<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Gawain Claridge - based heavily on sonyuserfiles/content/sony1/sonyNavigationV2.cfc
Date created:	2006/09/06

	Provides the base functionality for tree management.
		
	Syntax	  -	None
	
	Parameters - None required
				
	Return Codes - None 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2008/11/19 added cluster management
AJC 2008/12/19 Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories
NYB 2009-05-07 Lexmark Bug 2144 - added distinct to query in getProductSectionDetails function
AJC 2009/11/24 P-LEX039 - Added list price to the import
SSS 2010-04-27 LHID 3244 Added N throughout the file to fix kangi char bug
PPB 2011/06/08 LID6601/LID6694 add listprice column to insert
PPB 2011/06/13 LID5613 added some WITH (NOLOCK)s
WAB 2011/11/08 Major changes so that functions can be called across a cluster.  Use of applicationScope variable
IH	2012/10//31 Case 431490 changed cfparam cfsqltype of tmpTreeCategoryImageStatus from CF_SQL_INTEGER to CF_SQL_VARCHAR

2012-12-03 	PPB Case 432280 changed queryparam type from integer to varchar
2012-12-07 	PPB Case 432507 changed queryparam type from integer to varchar

--->

<CFCOMPONENT displayname="relayProductTree" hint="This provides housekeeping as well as navigation functions.">
<!---  
	
	The cfc pro

--->


<cffunction name="initialise">
	<cfargument name="applicationScope" default="#application#">
	<!--- DO NOT VAR THESE VARIABLES --->
	<cfset variables.applicationScope = arguments.applicationScope>
	
</cffunction>

<!--- Create default structure templates. --->
<cfscript>
	// Category Structure
	BlankCategory = structNew();
	BlankCategory.Title = "";
	BlankCategory.categoryName = "";
	BlankCategory.categoryID = "";
	BlankCategory.subCategories = structNew();
	BlankCategory.subCategories.categoryArray = arrayNew(1);	
	BlankCategory.Products = structNew();
	BlankCategory.Products.productArray = arrayNew(1);
	
	// Product Structure
	this.BlankProduct = structNew();
	this.BlankProduct.productTitle = "";
	this.BlankProduct.productID = "";
	this.BlankProduct.SummaryDataLoaded = false;
</cfscript>

<cffunction name="newProduct" returnType="Struct">
	<cfscript>
	 return Duplicate(this.BlankProduct);
	</cfscript>
</cffunction>
<!--- 

	TREE Management Functions

 --->
<cffunction name="insertTree" returntype="numeric">
	<cfargument name="title" required="no">
	<cfargument name="description" required="no">
	<cfargument name="active" required="no" default="0">
	<cfargument name="countryCode" required="Yes">
	<cfargument name="languageCode" required="Yes">
	<cfargument name="sourceid" required="Yes" type="numeric">
	<cfset var treeQry = "">
	<cfparam name="arguments.title" default="#arguments.languageCode# #arguments.countryCode# #arguments.sourceid#">
	<cfparam name="arguments.description" default="#arguments.languageCode# #arguments.countryCode# #arguments.sourceid#">
	<cfquery name="treeQry" datasource="#applicationScope.siteDataSource#">
		SELECT TreeID
			FROM PCMproductTree
			WHERE 1=1 
			<cfif len(trim(arguments.title))>
				AND  title =  <cf_queryparam value="#trim(arguments.title)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			</cfif>
			<cfif len(trim(arguments.description))>
				AND  description =  <cf_queryparam value="#trim(arguments.description)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			</cfif>
			<cfif len(arguments.countryCode)>
				AND  countryCode =  <cf_queryparam value="#arguments.countryCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			</cfif>
			<cfif len(arguments.languageCode)>
				AND  languageCode =  <cf_queryparam value="#arguments.languageCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			</cfif>
			<cfif len(arguments.sourceid)>
				AND sourceid = #arguments.sourceid#
			</cfif>
	</cfquery>
	<cfif not treeQry.recordcount>
		<!--- Insert new id --->
		<cfquery name="treeQry" datasource="#applicationScope.siteDataSource#">
			INSERT INTO PCMproductTree (
				title,
				Description,
				Active,
				countryCode,
				languageCode,
				lastupdatedBy,				
				sourceID) 
				VALUES
					(
					<cf_queryparam value="#trim(arguments.title)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#trim(arguments.description)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#arguments.Active#" CFSQLTYPE="cf_sql_float" >,
					<cf_queryparam value="#arguments.countryCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#arguments.languageCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#arguments.sourceid#" CFSQLTYPE="CF_SQL_INTEGER" >)
     			SELECT scope_identity() AS treeID
		</cfquery>			
	</cfif>
	<cfreturn treeQry.TreeID>
</cffunction>
<cffunction name="updateTree">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="title" required="Yes">
	<cfargument name="description" required="Yes">
	<cfargument name="active" required="Yes">
	<cfargument name="countryCode" required="Yes">
	<cfargument name="languageCode" required="Yes">
	<cfargument name="source" required="Yes">
	<cfargument name="filePath" required="Yes">
</cffunction>

<cffunction name="deleteTree">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="keepKeyData" default="false">
	<cfargument name="keepCategoryData" default="false">
	<cfargument name="deletePendingDeletes" default="false">
	
	<cfquery name="ClearTreeUpdateQry" datasource="#applicationScope.siteDataSource#">
		update PCMproductTree
		set clearTree=0
		where treeID = #arguments.treeID# 
	</cfquery>		
	
	<cfif deletePendingDeletes>
		<cfquery name="treeUpdQry" datasource="#applicationScope.siteDataSource#">
			delete from PCMproductCategoryProduct 
				where treeID = #arguments.treeID# 
				and  processStatus = 'PENDINGDELETE'
		</cfquery>
		<!--- GCC - 2005/07/01 - This is the last step of any import so set last updated date--->
		<cfquery name="treeUpdateQry" datasource="#applicationScope.siteDataSource#">
			update PCMproductTree
			set lastupdated=getdate(),
				lastupdatedby = 4
			where treeID = #arguments.treeID# 
		</cfquery>		
		<cfreturn>		
	</cfif>
	<cfif not keepKeyData>
		<!--- Delete all the key data --->
		<cfquery name="treeDel" datasource="#applicationScope.siteDataSource#">
			delete from PCMproductCategoryProduct where treeID = #treeID#
		</cfquery>
		<cfquery name="treeDel" datasource="#applicationScope.siteDataSource#">
			delete from PCMproductCategory where treeID = #treeID#
		</cfquery>
		<cfquery name="treeDel" datasource="#applicationScope.siteDataSource#">
			delete from PCMproductTreeTransformation where treeID = #treeID#
		</cfquery> 
		<cfquery name="treeDel" datasource="#applicationScope.siteDataSource#">
			delete from PCMproductTree where treeID = #treeID#
		</cfquery>
	</cfif>
	<!--- Delete the tree from all related tables. --->
	<cfif not keepCategoryData>
		<cfquery name="treeDel" datasource="#applicationScope.siteDataSource#">
			delete from PCMproductCategory where treeID = #treeID#
		</cfquery>
	</cfif>
	<!--- Mark the categories for deletion. --->
	<cfquery name="treeUpdQry" datasource="#applicationScope.siteDataSource#">
		UPDATE PCMproductCategory
			SET deleted = 1 where treeID = #arguments.treeID#
	</cfquery>
	<cfquery name="treeUpdQry" datasource="#applicationScope.siteDataSource#">
		UPDATE PCMproductCategoryProduct
			SET processStatus = 'PENDINGDELETE' where treeID = #arguments.treeID#
	</cfquery>	
	<!--- clear down for rebuilding --->
	<cfquery name="resetOrdering" datasource="#applicationScope.siteDataSource#">
		update PCMproductCategoryProduct 
		set userOrder = NULL
		where treeID = #arguments.treeID#
	</cfquery>
	<cfquery name="resetCategoryImages" datasource="#applicationScope.siteDataSource#">
		update PCMproductCategory 
		set localCategoryImageProductID = NULL
		where treeID = #arguments.treeID#
	</cfquery>
	<cfquery name="deleteProductAccessories" datasource="#applicationScope.siteDataSource#">
		DELETE FROM PCMproductAccessory
		WHERE treeID = #arguments.treeID#
	</cfquery>
</cffunction>

<cffunction name="selectTree">
	<cfargument name="treeID" required="No" default="">
	<cfargument name="countryCode" required="no" default="">
	<cfargument name="languageCode" required="no" default="">
	<cfargument name="sourcegroupID" required="no" default="">
	<cfargument name="sourceID" required="no" default="">
	<cfargument name="title" required="no" default="">
	
	<cfset var treeQry = "">
	<cfquery name="treeQry" datasource="#applicationScope.siteDataSource#">
		select * from PCMproductTree, PCMproductTreeSource
			WHERE PCMproductTree.sourceid=PCMproductTreeSource.sourceid
				<cfif len(arguments.title)>
					and  title =  <cf_queryparam value="#trim(arguments.title)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				</cfif>
				<cfif len(arguments.treeID)>
					and treeID =  <cf_queryparam value="#arguments.treeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
				<cfif len(arguments.countryCode)>
					and  countryCode =  <cf_queryparam value="#arguments.countryCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				</cfif>
				<cfif len(arguments.languageCode)>
					and  languageCode =  <cf_queryparam value="#arguments.languageCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				</cfif>
				<cfif len(arguments.sourcegroupID)>
					and PCMproductTree.sourceID IN (select sourceID from PCMProductTreeSource
					where SourceGroupID =  <cf_queryparam value="#arguments.sourcegroupID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</cfif>				
				<cfif len(arguments.sourceID)>
					and PCMproductTree.sourceID =  <cf_queryparam value="#arguments.sourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
	</cfquery>
	<cfreturn treeQry>
</cffunction>

<cffunction name="selectSource" returntype="query">
	<cfargument name="sourceID" required="No" default="">
	<cfargument name="source" required="no" default="">
	<cfargument name="sourceGroupID" required="no" default="">
	<cfset var SourceQry = "">
	
	<cfquery name="SourceQry" datasource="#applicationScope.siteDataSource#">
		select * from PCMproductTreeSource
		WHERE 1=1 
		<cfif len(arguments.source)>
			and  source =  <cf_queryparam value="#trim(arguments.source)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
		</cfif>
		<cfif len(arguments.sourceID)>
			and sourceID =  <cf_queryparam value="#arguments.sourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		<cfif len(arguments.sourceGroupID)>
			and sourceGroupID =  <cf_queryparam value="#arguments.sourceGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
	</cfquery>
	<cfreturn SourceQry>
	
</cffunction>

<cffunction name="getTreeSet" returntype="query">
	<cfargument name="SourcePrefix" default="">
	<cfargument name="ActiveOnly" default="">
	<cfargument name="treePrefix" default="">
	<cfargument name="sourcegroupid" default="">
	
	<cfquery name="treeQry" datasource="#applicationScope.siteDataSource#">
		SELECT TreeID,title,PCMproductTree.sourceID,LanguageCode,countryCode,Active,lastupdated,SourceGroupID,
			<!--- NJH 2008/12/22 CR-LEX579 added permittedLocales to display in the report --->
			permittedLoadLocales
			FROM PCMproductTree
			INNER JOIN PCMproductTreeSource on PCMproductTree.sourceid=PCMproductTreeSource.sourceid
			 WHERE 1=1
			<cfif len(arguments.SourcePrefix)>
				AND  Source  like  <cf_queryparam value="#arguments.SourcePrefix#%" CFSQLTYPE="CF_SQL_VARCHAR" >  
			</cfif>
			<cfif len(arguments.ActiveOnly)>
				AND Active =  <cf_queryparam value="#arguments.ActiveOnly#" CFSQLTYPE="cf_sql_float" > 
			</cfif>
			<cfif len(arguments.treePrefix)>
				AND  title  like  <cf_queryparam value="#arguments.treePrefix#%" CFSQLTYPE="CF_SQL_VARCHAR" >  					
			</cfif>
			<cfif len(arguments.sourcegroupID)>
				and SourceGroupID =  <cf_queryparam value="#arguments.sourcegroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfif>
			ORDER by PCMproductTree.sourceID,upper(title)
	</cfquery>
	<cfreturn treeQry>
</cffunction>

<!--- 

	CATEGORY Management Functions

 --->
<cffunction name="insertCategory" returntype="numeric" >
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="CategoryName" required="Yes">
	<cfargument name="ParentCategoryID" required="no" default="">
	<cfargument name="sourceTreeID" required="No" default="">
	<cfargument name="sourceCategoryID" required="No" default="">
	<cfargument name="RemoteCategoryID" required="No" default="">
	
	<!---  2005/07/27 GCC Category Ordering START --->
	<cfargument name="sequence" required="No" default="NULL">
	<!---  2005/07/27 GCC Category Ordering END --->
	<cfscript>
		var catQry = "";
		var catUpdQry = "";
		var baseCategoryName = "";
		var catIdx = "";
	
	</cfscript>
	
	<!--- Check to see if the category already exists. --->
	
	<cfquery name="catQry" datasource="#applicationScope.siteDataSource#" debug=no>
		select categoryID,deleted from PCMproductCategory 
			where treeID = #arguments.treeID#
				and  categoryname =  <cf_queryparam value="#arguments.CategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				<cfif len(arguments.ParentCategoryID)>
					and ParentCategoryID =  <cf_queryparam value="#arguments.ParentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				<cfelse>
					and ParentCategoryID is null
				</cfif>
				<cfif len(arguments.sourceTreeID)>
					and sourceTreeID =  <cf_queryparam value="#arguments.sourceTreeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
				<cfif len(arguments.sourceCategoryID)>
					and sourceCategoryID =  <cf_queryparam value="#arguments.sourceCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
				<cfif len(arguments.RemoteCategoryID)>
					and RemoteCategoryID =  <cf_queryparam value="#arguments.RemoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
	</cfquery>
	
	<!--- Return it. --->
	<cfif catQry.recordcount>
		<cfif catQry.deleted >
		<!---  2005/07/27 GCC Category Ordering START --->
			<cfquery name="catUpdQry" datasource="#applicationScope.siteDataSource#"  debug=no>
				update PCMproductCategory set deleted = 0				
				, sequence = <cf_queryparam value="#arguments.sequence#" CFSQLTYPE="CF_SQL_INTEGER" >
				,  categoryName =  <cf_queryparam value="#arguments.CategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				where treeID = #arguments.treeID#
					and  categoryID =  <cf_queryparam value="#catQry.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			</cfquery>
		<!---  2005/07/27 GCC Category Ordering END --->
		</cfif>
		<cfreturn catQry.categoryID>
	</cfif>
	
	<!--- Parent Category check. Stops stupid things happening. --->
	<cfif not len(arguments.ParentCategoryID) and arguments.CategoryName contains "|">
		<!--- Ok we need to determine the ParentCategoryID. --->
		<cfset baseCategoryName = "">
		<cfloop list="#arguments.CategoryName#" delimiters="|" index="catIdx">
			<cfscript>
			baseCategoryName = listappend(baseCategoryName,catIdx,"|");
			if (baseCategoryName neq arguments.CategoryName){
			// Ok to add this category.
			arguments.ParentCategoryID = insertCategory(
				treeid= arguments.treeID, 
				parentCategoryID=arguments.ParentCategoryID,
				CategoryName= baseCategoryName,
				sequence = arguments.sequence);	
			}	
			</cfscript>
		</cfloop>
	
	</cfif>
	<!--- Now double check as we may need to ensure that the category is not inserted now we know the parentCategoryID. --->
	<cfquery name="catQry" datasource="#applicationScope.siteDataSource#">
		select categoryID,deleted from PCMproductCategory 
			where treeID = #arguments.treeID#
				and  categoryname =  <cf_queryparam value="#arguments.CategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				<cfif len(arguments.ParentCategoryID)>
					and ParentCategoryID =  <cf_queryparam value="#arguments.ParentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				<cfelse>
					and ParentCategoryID is null
				</cfif>
				<cfif len(arguments.sourceTreeID)>
					and sourceTreeID =  <cf_queryparam value="#arguments.sourceTreeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
				<cfif len(arguments.sourceCategoryID)>
					and sourceCategoryID =  <cf_queryparam value="#arguments.sourceCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
				<cfif len(arguments.RemoteCategoryID)>
					and RemoteCategoryID =  <cf_queryparam value="#arguments.RemoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
	</cfquery>
	
	<!--- Return it. --->
	<cfif catQry.recordcount>
		<cfif catQry.deleted >
			<cfquery name="catUpdQry" datasource="#applicationScope.siteDataSource#">
				update PCMproductCategory set deleted  = 0
				, sequence = <cf_queryparam value="#arguments.sequence#" CFSQLTYPE="CF_SQL_INTEGER" >
				,  categoryName =  <cf_queryparam value="#arguments.CategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >  
					where treeID = #arguments.treeID#
					and  categoryID =  <cf_queryparam value="#catQry.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			</cfquery>
		</cfif>
		<cfreturn catQry.categoryID>
	</cfif>
	<!---  2005/07/27 GCC Category Ordering START --->
	<!--- As it is possible for the CategoryName to be a path to a  --->
	<cfquery name="catQry" datasource="#applicationScope.siteDataSource#">
		INSERT INTO PCMproductCategory (
			treeID,
			parentCategoryID,
			categoryName,
			lastupdatedBy,
			deleted,
			sourceTreeID,
			sourceCategoryID,
			sequence,
			RemoteCategoryID) 
			VALUES
				(
				<cf_queryparam value="#arguments.treeID#" CFSQLTYPE="cf_sql_integer" >,
				<cfif len(arguments.parentCategoryID)>
					<cf_queryparam value="#arguments.parentCategoryID#" CFSQLTYPE="cf_sql_integer" >,
				<cfelse>
					NULL,
				</cfif>
				<cf_queryparam value="#trim(arguments.categoryName)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
				0,
				<cfif len(arguments.sourceTreeID)>
					<cf_queryparam value="#arguments.sourceTreeID#" CFSQLTYPE="cf_sql_integer" >,
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.sourceCategoryID)>
					<cf_queryparam value="#arguments.sourceCategoryID#" CFSQLTYPE="cf_sql_integer" >
				<cfelse>
				NULL
				</cfif>,
				<cfif len(arguments.sequence)>
					<cf_queryparam value="#arguments.sequence#" CFSQLTYPE="cf_sql_integer" >
				<cfelse>
				NULL
				</cfif>,
				<cfif len(arguments.RemoteCategoryID)>
					<cf_queryparam value="#arguments.RemoteCategoryID#" CFSQLTYPE="cf_sql_integer" >
				<cfelse>
				NULL
				</cfif>)
    	SELECT scope_identity() AS categoryID
	</cfquery>		
	<!---  2005/07/27 GCC Category Ordering END --->
	<cfreturn catQry.categoryID>	
</cffunction>

<cffunction name="deleteCategory">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfset var childQry = "">
	<cfset var delQry = "">
	<!--- Get all the child categories --->
	<cfscript>
	childQry = selectCategoryChildCategories (treeID = #arguments.treeID#, categoryID = #arguments.categoryID#);
	</cfscript>
	<cfloop query="childQry">
		<CFSCRIPT>
		deleteCategory (treeID = #childQry.treeID#, categoryID = #childQry.categoryID#);
		</CFSCRIPT>
	</cfloop>
	
	<cfquery name="delQry" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductCategoryProduct 
			where treeID = #arguments.treeID# AND
				categoryID = #arguments.categoryID#	
	</cfquery>
	<cfquery name="delQry" datasource="#applicationScope.siteDataSource#">
		update PCMproductCategory set deleted = 1
			where treeID = #arguments.treeID# AND
				categoryID = #arguments.categoryID#	
	</cfquery>
</cffunction>

<cffunction name="selectCategory" returntype="query">
	<cfargument name="treeID" required="Yes" type="numeric">
	<!--- PJP 20/11/2012: CASE 432117: removed the numeric constraint as causing issues with blank catids --->
	<cfargument name="categoryID" required="Yes">
	<cfargument name="mode" required="Yes" default="standard">
	<!--- START: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
	<cfargument name="showDeleted" type="Boolean" required="No" default="true">
	<!--- END: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
	
	<cfset var catQry = "">
	<cfquery name="catQry" datasource="#applicationScope.siteDataSource#">
		select * from PCMproductCategory where treeID = #arguments.treeID# 
		<cfif arguments.mode eq "standard" and len(arguments.categoryID)>
			and categoryID  =  <cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="cf_sql_integer" >
		<cfelseif arguments.mode eq "remote" and len(arguments.categoryID)>
			and remoteCategoryID  =  <cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="cf_sql_integer" >
		</cfif>
		<!--- START: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
		<cfif not showDeleted>
			and deleted = 0
		</cfif>
		<!--- END: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
	</cfquery>
	<cfreturn catQry>	
</cffunction>

<cffunction name="selectCategoryChildCategories" returntype="query">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<!--- START: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
	<cfargument name="showDeleted" type="Boolean" required="No" default="true">
	<!--- END: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
	
	<cfset var catQry = "">
	<cfquery name="catQry" datasource="#applicationScope.siteDataSource#">
		select * from PCMproductCategory where treeID = #arguments.treeID# 
			<cfif arguments.categoryID neq 0>
				and ParentcategoryID = #arguments.categoryID#
			<cfelse>
				and ParentcategoryID IS NULL
			</cfif>
			<!--- START: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
			<cfif not showDeleted>
				and deleted = 0
			</cfif>
			<!--- END: Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
	</cfquery>
	<cfreturn catQry>	
</cffunction>

<cffunction name="selectCategoryProducts" returntype="query">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="getDeletedProducts" required="No" default="false">
	<cfargument name="orderBy" required="No">
	<cfargument name="baseModelsOnly" type="boolean" required="no" default="false">
	<cfset var catProductQry = "">
	
	<!--- START: 2010/05/07 AJC P-LEX039 - Restrict imported categories --->
	<cfquery name="catProductQry" datasource="#applicationScope.siteDataSource#">
		select pcp.productID,p.productKey,p.title, pcp.categoryID,pcp.processStatus,CASE WHEN PCP.userOrder is NULL THEN PCP.sequence ELSE PCP.userOrder END as sequence, p.countryCode,p.languageCode,p.sourceid,pcp.promotion,
		pc.sourcetreeID	
		<!--- START: AJC 2009-11-25 P-LEX039 --->
		,p.ListPrice
		<!--- END: AJC 2009-11-25 P-LEX039 --->
		from PCMproductCategoryProduct pcp WITH (NOLOCK)
		inner join PCMproduct p WITH (NOLOCK) on p.productID = pcp.productID
		inner join PCMproductCategory pc WITH (NOLOCK) ON pcp.categoryID = pc.categoryID
				<cfif arguments.baseModelsOnly>
					inner join PCMlexSearchBaseModels baseModels WITH (NOLOCK) on baseModels.productKey = p.productKey
				</cfif>
				where pcp.treeID = #arguments.treeID# and
					pcp.categoryID = #arguments.categoryID# and
					p.productID = pcp.productID
					<cfif arguments.getDeletedProducts>
						and pcp.processStatus = 'SUPPRESSED'
					<cfelse>
						and pcp.processStatus <> 'SUPPRESSED'
					</cfif>
					ORDER by 
					<cfif isDefined("arguments.orderBy")>
						#arguments.orderBy#
					<cfelse>
				CASE WHEN PCP.userOrder is NULL THEN PCP.sequence ELSE PCP.userOrder END, title
					</cfif>
	</cfquery>
	<!--- END: 2010/05/07 AJC P-LEX039 - Restrict imported categories --->
	<cfreturn catProductQry>	
</cffunction>

<cffunction name="updateCategoryName">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="categoryName" required="Yes">
	<cfset var catQry = "">
	<cfquery name="catQry" datasource="#applicationScope.siteDataSource#">
		update PCMproductCategory set 
				displayName =  <cf_queryparam value="#arguments.categoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >   
			where treeID = #arguments.treeID# and categoryID = #arguments.categoryID#
	</cfquery>	
</cffunction>


<cffunction name="insertProductById" >
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="sequence" required="No" default="">
	<cfargument name="processStatus" required="Yes">
	<cfset var prodQry = "">
	<cfset var updateProdQry = "">
	<cfset var insertProdQry = "">
	
	<!--- Check to see if the record exists in the current tree. --->
	<cfquery name="prodQry" datasource="#applicationScope.siteDataSource#">
		select 1 from PCMproductCategoryproduct 
			where treeid = #arguments.treeID#
			and categoryID = #arguments.categoryID#
			and productID = #arguments.productID#
	</cfquery>
	<cfif prodQry.recordcount >
		<!--- *** --->
		<!--- at this stage need to import the product at it's source tree status --->
		<!--- *** --->
		<cfquery name="updateProdQry" datasource="#applicationScope.siteDataSource#">
			UPDATE  PCMproductCategoryproduct 
				SET processStatus =  <cf_queryparam value="#arguments.processStatus#" CFSQLTYPE="CF_SQL_VARCHAR" > ,	<!--- 2012-12-03 PPB Case 432280 changed queryparam type from integer to varchar --->
				<cfif len(arguments.sequence)>
					sequence = <cf_queryparam value="#arguments.sequence#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>					
					sequence = NULL
				</cfif>
				where treeid = #arguments.treeID#
				and categoryID = #arguments.categoryID#
				and productID = #arguments.productID#
		</cfquery>		
	
	<cfelse>
		<!--- Insert --->
		<cfquery name="insertProdQry" datasource="#applicationScope.siteDataSource#">
			INSERT INTO  PCMproductCategoryproduct (treeid,categoryID,productID,processStatus,lastupdatedBy<cfif len(arguments.sequence)>,sequence</cfif>)
				VALUES(
				<cf_queryparam value="#arguments.treeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.processStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				#request.relayCurrentUser.personID#
				<cfif len(arguments.sequence)>,<cf_queryparam value="#arguments.sequence#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>	)	
		</cfquery>		
	</cfif>
</cffunction>

<!--- 

	PRODUCT Management Functions 

--->
 
<cffunction name="insertProductHeader" returntype="numeric" hint="This creates the initial set of information for a product" >
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="yes" >
	<cfargument name="title" required="Yes">
	<cfargument name="productKey" required="Yes">
	<cfargument name="model" required="no" default="">
	<cfargument name="countryCode" required="yes" >
	<cfargument name="languageCode" required="yes" >
	<cfargument name="sourceid" required="Yes" type="numeric">
	<cfargument name="sequence" required="no" default="">
	<cfargument name="productnumber" required="no" default="">
	<cfargument name="description" required="no" default="">
	<cfargument name="remoteCreatedDate" required="no" default="">
	<cfargument name="imageurl" required="no" default="">
	<cfargument name="thumbnailurl" required="no" default="">
	<cfargument name="outOfCatalogue" required="no" default="">
	<cfargument name="startdate" required="no" default="">
	<cfargument name="baseModel" required="no" default="">
	<!--- START: AJC 2009/11/24 P-LEX039 - Added list price to the import --->
	<cfargument name="ListPrice" required="no" default="">
	<!--- >AJC 2009/11/24 P-LEX039 - Added list price to the import --->
	<cfset var prodQry ="">
	<cfset var checkProdQry = "">
	<cfset var insertProdQry = "">
	
	<cfset var prodUpQry ="">
	<cfset var prodID = "">
	<cfset var prodInsQry = "">
	<cfset var UpdateProdQry = "">

	<cfif not len(arguments.model)>
		<!--- Ensure a model number exists. --->
		<cfset arguments.model = arguments.title>
	</cfif>
	<!--- check the product exists --->
	<!--- NJH 2009/04/23 Bug Fix Lexmark Support Issue 1604 - bring back description as well--->
	<cfquery name="checkProdQry" datasource="#applicationScope.siteDataSource#" maxrows=1>
		SELECT productID as productID, model, baseModel, title, productnumber, imageurl,thumbnailurl, suppressdate,description,listPrice FROM PCMproduct where 
			productKey =  <cf_queryparam value="#arguments.productKey#" CFSQLTYPE="CF_SQL_VARCHAR" >   AND 
			countryCode =  <cf_queryparam value="#arguments.countryCode#" CFSQLTYPE="CF_SQL_VARCHAR" >   AND 
			languageCode =  <cf_queryparam value="#arguments.languageCode#" CFSQLTYPE="CF_SQL_VARCHAR" >   AND
			sourceID =  <cf_queryparam value="#arguments.sourceID#" CFSQLTYPE="CF_SQL_VARCHAR" >  
		order by productid desc
	</cfquery>
	
	<cfif checkProdQry.recordcount neq 0>
	
		<cfset prodID = checkProdQry.productID>
		<!--- it does - get the category --->
		<cfquery name="prodQry" datasource="#applicationScope.siteDataSource#">
			SELECT	productID, processStatus
			FROM PCMproductCategoryProduct WHERE
				productID =  <cf_queryparam value="#prodID#" CFSQLTYPE="CF_SQL_INTEGER" >   AND
			treeID = #arguments.treeID# AND
			categoryID =  <cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<cfif prodQry.recordcount eq 0>
		<!--- 2009/01/29 GCC LID 1672 Category must have changed - move product to new category and reselect status --->
			<cfquery name="moveProductToNewCategory" datasource="#applicationScope.siteDataSource#">
				Update PCMproductCategoryProduct
					set categoryID =  <cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					where  productid =  <cf_queryparam value="#prodID#" CFSQLTYPE="CF_SQL_INTEGER" >   AND
						treeID = #arguments.treeID#
			</cfquery>
			<!--- collect new data--->
			<cfquery name="prodQry" datasource="#applicationScope.siteDataSource#">
				SELECT	productID, processStatus
				FROM PCMproductCategoryProduct WHERE
					productID =  <cf_queryparam value="#prodID#" CFSQLTYPE="CF_SQL_INTEGER" >   AND
				treeID = #arguments.treeID# AND
				categoryID =  <cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
		
		<!--- NJH 2009/04/23 Bug Fix Lexmark Support Issue 1604 - check if description has changed as well.
		GCC 2009/05/20 LID 2252 resurresction now works if gone from out pf catalogue to back in. --->
		<cfif (checkProdQry.model neq arguments.model) 
		or (checkProdQry.baseModel neq arguments.baseModel) 
		or (checkProdQry.title neq arguments.title) 
		or (checkProdQry.productnumber neq arguments.productnumber) 
		or (checkProdQry.imageurl neq arguments.imageurl) 
		or (checkProdQry.ListPrice neq arguments.ListPrice) 
		or (checkProdQry.thumbnailurl neq arguments.thumbnailurl) 
		or (checkProdQry.description neq arguments.description)
		or ((prodQry.processStatus is "SUPPRESSED" or prodQry.processStatus is "PENDINGDELETE") and arguments.outOfCatalogue is "no") 
		or ((prodQry.processstatus is "NEW" or prodQry.processstatus is "EXISTING") and arguments.outOfCatalogue is "yes")
		>
			<cfquery name="UpdateProdQry" datasource="#applicationScope.siteDataSource#">
				update PCMproduct 
				set  title =  <cf_queryparam value="#arguments.title#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
					model =  <cf_queryparam value="#arguments.model#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
				lastUpdated=#createodbcdatetime(now())#,
					productNumber =  <cf_queryparam value="#arguments.productNumber#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
					description =  <cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
					baseModel =  <cf_queryparam value="#arguments.baseModel#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				<cfif arguments.remoteCreatedDate is not "">,remoteCreatedDate=<cf_queryparam value="#arguments.remoteCreatedDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" ></cfif>
				<cfif arguments.imageurl is not ""> ,imageurl =  <cf_queryparam value="#arguments.imageurl#" CFSQLTYPE="CF_SQL_VARCHAR" >  </cfif>
				<cfif arguments.thumbnailurl is not ""> ,thumbnailurl =  <cf_queryparam value="#arguments.thumbnailurl#" CFSQLTYPE="CF_SQL_VARCHAR" >  </cfif>
				<cfif arguments.startdate is not "">,startdate=<cf_queryparam value="#arguments.startdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > </cfif>
				<cfif arguments.outOfCatalogue is "Yes">,suppressdate=#createodbcdatetime(DateAdd("d", 1, now()))#</cfif>
				<cfif arguments.outOfCatalogue is "No">,suppressdate=NULL</cfif>
				<!--- START: AJC 2009/11/24 P-LEX039 - Added list price to the import --->
				<cfif arguments.ListPrice is not "">,ListPrice =  <cf_queryparam value="#arguments.ListPrice#" CFSQLTYPE="cf_sql_numeric" ></cfif>
				<cfif arguments.ListPrice is "">,ListPrice=NULL</cfif>
				<!--- END: AJC 2009/11/24 P-LEX039 - Added list price to the import --->
				where  productid =  <cf_queryparam value="#prodID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			</cfquery>
		</cfif>
		<cfif (prodQry.processStatus is "SUPPRESSED" or prodQry.processStatus is "PENDINGDELETE") and arguments.outOfCatalogue is "no">
			<cfquery name="UpdateProdCatQry" datasource="#applicationScope.siteDataSource#">
				update PCMproductCategoryProduct
				set processstatus='EXISTING'
				where  productid =  <cf_queryparam value="#prodID#" CFSQLTYPE="CF_SQL_INTEGER" >   AND
				treeID = #arguments.treeID# AND
				categoryID =  <cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
		<cfreturn prodID>
	<cfelse>
		<!--- If we are here, then the product is new. --->
		<!--- 2011/06/08 PPB LID6601/LID6694 add listprice column to insert --->
		<cfquery name="insertProdQry" datasource="#applicationScope.siteDataSource#">
			insert into PCMproduct (title,productKey,model,countryCode,languageCode,sourceid,productNumber,description,baseModel<cfif arguments.remoteCreatedDate is not "">,remoteCreatedDate</cfif><cfif arguments.imageurl is not "">,imageurl</cfif><cfif arguments.thumbnailurl is not "">,thumbnailurl</cfif><cfif arguments.startdate is not "">,startdate</cfif><cfif arguments.outOfCatalogue is "Yes">,suppressdate</cfif><cfif arguments.ListPrice is not "">,listPrice</cfif>) VALUES (
				<cf_queryparam value="#arguments.title#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.productKey#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.model#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.countryCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.languageCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.sourceID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.productNumber#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.baseModel#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfif arguments.remoteCreatedDate is not "">,<cf_queryparam value="#arguments.remoteCreatedDate#" CFSQLTYPE="cf_sql_timestamp" ></cfif>
				<cfif arguments.imageurl is not "">,<cf_queryparam value="#arguments.imageurl#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
				<cfif arguments.thumbnailurl is not "">,<cf_queryparam value="#arguments.thumbnailurl#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
				<cfif arguments.startdate is not "">,<cf_queryparam value="#arguments.startdate#" CFSQLTYPE="cf_sql_timestamp" ></cfif>
				<cfif arguments.outOfCatalogue is "Yes">,#createodbcdatetime(DateAdd("d", 1, now()))#</cfif>
				<cfif arguments.ListPrice is not "">,<cf_queryparam value="#arguments.ListPrice#" CFSQLTYPE="cf_sql_numeric" ></cfif>
				 )
			SELECT scope_identity() as productID	
		</cfquery>
		<cfquery  name="prodUpQry" datasource="#applicationScope.siteDataSource#">
			insert into PCMproductCategoryProduct
				(treeID,
				categoryID,
				productID,sequence,processStatus)
				values
				(<cf_queryparam value="#arguments.treeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#insertProdQry.productID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cfif len(arguments.sequence)><cf_queryparam value="#arguments.sequence#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse>NULL</cfif>,'NEW')
		</cfquery>	
	<cfreturn insertProdQry.productID>
	</cfif>
	
</cffunction>

<cffunction name="updateProduct" >
	<cfargument name="productDataStruct" required="Yes">
	<cfset var prodQryUp = "">
	<cfset var userSectQry = "">
	<cfquery  name="prodQryUp" datasource="#applicationScope.siteDataSource#">
		UPDATE PCMproduct set
			<cfif len(arguments.productDataStruct.imageUrl)>
					imageUrl =  <cf_queryparam value="#arguments.productDataStruct.imageUrl#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				imageUrl = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.thumbNailUrl)>
					thumbNailUrl =  <cf_queryparam value="#arguments.productDataStruct.thumbNailUrl#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				thumbNailUrl = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.title)>
					title =  <cf_queryparam value="#arguments.productDataStruct.title#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				title = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.productKey)>
					productKey =  <cf_queryparam value="#arguments.productDataStruct.productKey#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				productKey = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.Model)>
					Model =  <cf_queryparam value="#arguments.productDataStruct.Model#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				Model = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.countryCode)>
					countryCode =  <cf_queryparam value="#arguments.productDataStruct.countryCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				countryCode = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.languageCode)>
					languageCode =  <cf_queryparam value="#arguments.productDataStruct.languageCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				languageCode = NULL,
			</cfif>
			<cfif len(arguments.productDataStruct.source)>
					sourceID =  <cf_queryparam value="#arguments.productDataStruct.source#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
			<cfelse>
				sourceID = NULL,
			</cfif>
				lastUpdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  ,
			lastUpdatedBy = #request.relayCurrentUser.personID#,
			<cfif len(arguments.productDataStruct.primaryModel)>
					primaryModel =  <cf_queryparam value="#arguments.productDataStruct.primaryModel#" CFSQLTYPE="CF_SQL_bit" >  ,
			<cfelse>
				primaryModel = 0 ,
			</cfif>
			<cfif len(arguments.productDataStruct.isRetired) and arguments.productDataStruct.isRetired eq "true">
				isRetired = 1
			<cfelse>
				isRetired = 0
			</cfif>
		WHERE  productID =  <cf_queryparam value="#arguments.productDataStruct.productID#" CFSQLTYPE="CF_SQL_INTEGER" >  
	</cfquery>
	<!--- Ok now we need to apply the retired flag to ALL trees. --->
	<cfif len(arguments.productDataStruct.isRetired) and arguments.productDataStruct.isRetired eq "true">
		<cfquery name="userSectQry" datasource="#applicationScope.siteDataSource#">
			update PCMproductCategoryProduct set processStatus = 'SUPPRESSED'  
				where  productID =  <cf_queryparam value="#arguments.productDataStruct.productID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		</cfquery>	
	</cfif>
	<!--- Before text data can just be inserted, we need to ensure we aren't overwriting user specified content. --->
	<cfquery name="userSectQry" datasource="#applicationScope.siteDataSource#">
		select sectionID from PCMproductText 
			where  productID =  <cf_queryparam value="#arguments.productDataStruct.productID#" CFSQLTYPE="CF_SQL_INTEGER" >  
				AND userdata = 1	
	</cfquery>
	<cfif userSectQry.recordcount>
		<cfset userSections = valuelist(userSectQry.sectionID)>
		<cfif not listfind(userSections,textID)>
			<cfscript>
				updateProductTextData (productID = arguments.productDataStruct.productID,
					sectionID = textID,
					textData = arguments.productDataStruct["text"][textID].textData,
					userData = arguments.productDataStruct["text"][textID].userdata);
			</cfscript>
		</cfif>
		
	<cfelse>
		<cfloop collection="#arguments.productDataStruct.text#" item="textID">
			<cfscript>
				// Update the text information for the product.
				updateProductTextData (productID = arguments.productDataStruct.productID,
					sectionID = textID,
					textData = arguments.productDataStruct["text"][textID].textData,
					userdata = arguments.productDataStruct["text"][textID].userdata);
			</cfscript>
		</cfloop>
	</cfif>
</cffunction>

<cffunction name="deleteProduct" returntype="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
</cffunction>

<cffunction name="updateProductTextData">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="sectionID" required="Yes">
	<cfargument name="textData" required="Yes">	
	<cfargument name="userData" required="Yes">	
	<cfset var prodQryUpdQry = "">
	<!--- Delete the original content. --->
	<cfquery  name="prodQryUpdQry" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductText 
		where productID = #arguments.productID# 
		AND  sectionID =  <cf_queryparam value="#sectionID#" CFSQLTYPE="CF_SQL_VARCHAR" >  	
	</cfquery>
	
	<cfquery  name="prodQryUpdQry" datasource="#applicationScope.siteDataSource#">
		insert into PCMproductText (
		productID,
		sectionID,
		textdata,
		userdata,
		lastupdatedby) VALUES (
		<cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#arguments.sectionID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
		<cfif len(arguments.textdata)>
			<cf_queryparam value="#arguments.textdata#" CFSQLTYPE="CF_SQL_VARCHAR" >,
		<cfelse>
			NULL,
		</cfif>
		<cf_queryparam value="#arguments.userData#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)
	</cfquery>
			
</cffunction>

<cffunction name="insertProductTextData" access="public">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="sectionAttributeID" required="Yes">
	<cfargument name="textData" required="Yes">	
	<cfargument name="userData" required="Yes">
	<cfargument name="textDataCount" required="Yes">	
	
	<cfset var qry_ins_producttext = "">
	
	<cfquery  name="qry_ins_producttext" datasource="#applicationScope.siteDataSource#" debug = no>
		insert into PCMproductText (
		productID,
		sectionAttributeID,
		textdata,
		userdata,
		lastupdatedby,
		textDataCount) VALUES (
		<cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#arguments.sectionAttributeID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
		<cfif len(arguments.textdata)>
			<cf_queryparam value="#arguments.textdata#" CFSQLTYPE="CF_SQL_VARCHAR" >,
		<cfelse>
			NULL,
		</cfif>
		<cf_queryparam value="#arguments.userData#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
		<cf_queryparam value="#arguments.textDataCount#" CFSQLTYPE="CF_SQL_INTEGER" >)
	</cfquery>
			
</cffunction>

<cffunction name="insertSection" returntype="numeric">
	<cfargument name="remotecategoryid" required="Yes">
	<cfargument name="masked" required="Yes">
	<cfargument name="sequence" required="Yes" type="numeric">
	<cfargument name="sectionTypeID" required="Yes">
	
	<cfset var qry_ins_section = "">
	
	<cfquery  name="qry_ins_section" datasource="#applicationScope.siteDataSource#" debug = no>
		insert into PCMproducttextsection (remotecategoryid,masked,sequence,sectionTypeID)
		values (<cf_queryparam value="#arguments.remotecategoryid#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.masked#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.sequence#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.sectionTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		SELECT scope_identity() AS sectionid
	</cfquery>
	
	<cfreturn qry_ins_section.sectionid>
</cffunction>

<!--- 2009/03/31 GCC Added remotesectionattributegroupid as it is soooo needed by the Lexmark LXPD import... --->
<cffunction name="insertSectionAttributeGroup" returntype="numeric">
	<cfargument name="sectionattributegroupname" required="Yes">
	<cfargument name="remotesectionattributegroupid" required="No" default="">
	
	
	<cfset var qry_ins_SectionAttributeGroup = "">
	
	<cfif arguments.remotesectionattributegroupid eq "">
		<cfquery  name="qry_ins_SectionAttributeGroup" datasource="#applicationScope.siteDataSource#" debug = no>
			insert into PCMproducttextsectionattributegroup (sectionattributegroupname)
			values (<cf_queryparam value="#arguments.sectionattributegroupname#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			SELECT scope_identity() AS sectionattributegroupid
		</cfquery>
	<cfelse>
		<cfquery  name="qry_ins_SectionAttributeGroup" datasource="#applicationScope.siteDataSource#" debug = no>
			insert into PCMproducttextsectionattributegroup (sectionattributegroupname,remotesectionattributegroupid)
			values (<cf_queryparam value="#arguments.sectionattributegroupname#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.remotesectionattributegroupid#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			SELECT scope_identity() AS sectionattributegroupid
		</cfquery>	
	</cfif>
	
	<cfreturn qry_ins_SectionAttributeGroup.sectionattributegroupid>
</cffunction>

<cffunction name="insertSectionAttribute" returntype="numeric">
	<cfargument name="sectionid" required="Yes">
	<cfargument name="sectionattributename" required="Yes">
	<cfargument name="remoteattributeid" required="Yes">
	<cfargument name="sequence" required="Yes" type="numeric">
	<cfargument name="masked" required="Yes">
	<cfargument name="sectionattributegroupID" required="Yes">
	
	<cfset var qry_ins_SectionAttribute = "">
	
	<cfquery  name="qry_ins_SectionAttribute" datasource="#applicationScope.siteDataSource#">
		insert into PCMproducttextsectionattributes (sectionid, sectionattributename,remoteattributeid,sequence,masked,sectionattributegroupID)
		values (<cf_queryparam value="#arguments.sectionid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.sectionattributename#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.remoteattributeid#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.sequence#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.masked#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.sectionattributegroupID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		SELECT scope_identity() AS sectionattributeid
	</cfquery>
	
	<cfreturn qry_ins_SectionAttribute.sectionattributeid>
</cffunction>

<cffunction name="insertProductAccessories">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="productKey" required="Yes">
	<cfargument name="accessoryProductKey" required="Yes">
	
	<cfset var qry_ins_ProductAccessories = "">
	
	<cfquery  name="qry_ins_ProductAccessories" datasource="#applicationScope.siteDataSource#">
		insert into PCMproductAccessory (treeID,productKey,accessoryProductKey)
		values (<cf_queryparam value="#arguments.treeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.productKey#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.accessoryProductKey#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</cfquery>
	
</cffunction>

<cffunction name="getProductHeader" returntype="query">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="productID" type="numeric">
	<cfargument name="productKey" type="numeric" required="No">
	<cfset var prodQry = "">
	<cfquery  name="prodQry" datasource="#applicationScope.siteDataSource#">
		select p.* from PCMproduct p WITH (NOLOCK) 
		inner join pcmProductCategoryProduct pcp WITH (NOLOCK) on pcp.productID = p.productID
		where 1=1 and pcp.treeID = #arguments.treeID#
		<cfif isDefined("productID")>
			and p.productID = #arguments.productID#
		</cfif>
		<cfif isDefined("productKey")>
			and p.productKey = #arguments.productKey#
		</cfif>
	</cfquery>
	<cfreturn prodQry>
</cffunction>

<cffunction name="getProductDataSections" returntype="string">
	
	<cfquery name="selectProductSectionsQry" datasource="#applicationScope.siteDataSource#"  cachedwithin="#createtimespan(1,0,0,0)#">
		select sectionId from PCMproductTextSection order by sectionId
	</cfquery>
	<cfreturn valuelist(selectProductSectionsQry.sectionId)>
	
</cffunction>


<!--- 

	This function loads the summary data for a product into the Nav structure.

 --->
<cffunction name="getProductSummaryData">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="product" required="Yes" type="struct">
	<cfquery name="selectProductInfoQry"  datasource="#applicationScope.siteDataSource#">
		select p.* from PCMProduct p inner join pcmProductCategoryProduct pcp
			on pcp.productID = p.productID
			where  p.productID =  <cf_queryparam value="#product.productID#" CFSQLTYPE="CF_SQL_INTEGER" >  
				and pcp.treeID = #arguments.treeID#
	</cfquery>
	<cfscript>
		if (product.summaryDataLoaded) {
		 return;
		}
		product.Title = selectProductInfoQry.Title;
		product.Model = selectProductInfoQry.Model;
		product.productNumber = selectProductInfoQry.ProductNumber;
		product.ProductKey = selectProductInfoQry.ProductKey;
		product.thumbnailurl = selectProductInfoQry.thumbnailurl;
		product.imageURL = selectProductInfoQry.imageURL;
		product.created = selectProductInfoQry.created;
		product.description = selectProductInfoQry.description;
		product.remoteCreatedDate = selectProductInfoQry.remoteCreatedDate;
		product.remoteFileID = selectProductInfoQry.remoteFileID;
		product.summaryDataLoaded = true;
	</cfscript>
	<cfquery name="selectProductSummaryQry"  datasource="#applicationScope.siteDataSource#">
		select textdata from PCMproductText pt inner join PCMproductTextSectionAttributes ptsa
			on pt.sectionAttributeID = ptsa.sectionAttributeID inner join PCMproductTextSection pts
			on pts.sectionID = ptsa.sectionID inner join PCMproductTextSectionType ptst
			on pts.sectionTypeID = ptst.sectionTypeID inner join pcmProductCategoryProduct pcp
			on pcp.productID = pt.productID
			where  pt.productID =  <cf_queryparam value="#product.productID#" CFSQLTYPE="CF_SQL_INTEGER" >   and ptst.sectionTypeName = 'Summary'
			and pcp.treeID = #arguments.treeID#	
	</cfquery>
	<cfset temp = structdelete(product,"Summary")>
	<cfloop query="selectProductSummaryQry">
		<cfset product["Summary"] = selectProductSummaryQry.textdata>		
	</cfloop>
</cffunction>

<!--- 

	Function builds the content 

--->
<cffunction name="getProductDetails" returntype="struct">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="maskID" required="No" default="0">
	<cfargument name="sectionID" required="No">
	
	<cfscript>
		var product = structNew();
		
		var productHeaderQry = getProductHeader(treeID = arguments.treeID,productID = arguments.productID);
	
		product.productKey = productHeaderQry.productKey;
		product.productID = productHeaderQry.productID;
		product.languageCode = productHeaderQry.languageCode;
		product.countryCode = productHeaderQry.countryCode;
		product.sourceID = productHeaderQry.sourceID;
		product.isretired = productHeaderQry.isretired;
		product.primaryModel = productHeaderQry.primaryModel;
		product.title = productHeaderQry.title;
		product.imageUrl = productHeaderQry.imageUrl;
		product.thumbnailUrl = productHeaderQry.thumbnailUrl;
		product.model = productHeaderQry.model;
		product.remoteCreatedDate = productHeaderQry.remoteCreatedDate;
		product.remoteFileID = productHeaderQry.remoteFileID;
		//product.text = getProductTextData(productID = arguments.productID);
		product.productNumber = productHeaderQry.productNumber;
		product.description = productHeaderQry.description;
		if (not isDefined("arguments.sectionID")) {
			product.sections = getProductSectionDetails(treeID=arguments.treeID, productID = arguments.productID,maskID=arguments.maskID);
		} else {
			product.sections = getProductSectionDetails(treeID=arguments.treeID, productID = arguments.productID,maskID=arguments.maskID,sectionID=arguments.sectionID);
		}
		product.nodata = false;
		
	</cfscript>
	<!--- Return the product. --->
	
	<cfreturn product>
</cffunction>



<cffunction name="getProductMask" returntype="string" hint="Returns the mask ID for a product. If none exists, it returns 0">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="crossPortals" required="no" default="false"><!--- NJH 2006/12/14 --->

	<cfset productMask = 0>
	<cfset getProductMask="">
	
	<cfquery name="getProductMask" datasource="#applicationScope.siteDataSource#">
		select maskid from pcmproduct p inner join pcmproductcategoryproduct pcp 
			on pcp.productid = p.productid inner join pcmproductcategory pc 
			on pc.categoryid = pcp.categoryid inner join pcmproductmask mask 
			on mask.remotecategoryid = pc.remotecategoryid 
			where p.productid = #arguments.productID# 
				and pc.treeID = #arguments.treeID#
				and mask.siteDefID in (<cfif crossPortals>0,</cfif>#request.currentSite.siteDefID#)
	</cfquery>
	<cfif getProductMask.recordCount gt 0>
		<cfset productMask = valueList(getProductMask.maskID)>
	</cfif>
	<cfreturn productMask>
</cffunction>

<cffunction name="getPCMProductMask" returntype="query" hint="Returns the mask details from the PCMProductMask table">
	<cfargument name="SiteDefID" required="No" default="">
	<cfargument name="SourceID" required="No" default="">
	<cfargument name="MaskID" required="No" default="">
	<cfargument name="RemoteCategoryID" required="No" default="">

	<cfset var qry_get_PCMProductMask="">
	
	<cfquery name="qry_get_PCMProductMask" datasource="#applicationScope.siteDataSource#">
		select * from PCMProductMask
		where 1=1
		<cfif SiteDefID is not "">AND SiteDefID =  <cf_queryparam value="#SiteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >  </cfif>
		<cfif SourceID is not "">AND SourceID =  <cf_queryparam value="#sourceid#" CFSQLTYPE="CF_SQL_INTEGER" >  </cfif>
		<cfif MaskID is not "">AND MaskID =  <cf_queryparam value="#maskID#" CFSQLTYPE="CF_SQL_INTEGER" >  </cfif>
		<cfif RemoteCategoryID is not "">AND RemoteCategoryID =  <cf_queryparam value="#RemoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  </cfif>
	</cfquery>
	
	<cfreturn qry_get_PCMProductMask>
</cffunction>

<cffunction name="insPCMProductMask" returntype="numeric" hint="Inserts the mask details into the PCMProductMask table">
	<cfargument name="SiteDefID" required="No" default="">
	<cfargument name="SourceID" required="No" default="">
	<cfargument name="RemoteCategoryID" required="No" default="">
	<cfargument name="MaskDescription" required="No" default="">

	<cfset var qry_ins_PCMProductMask="">
	
	<cfquery name="qry_ins_PCMProductMask" datasource="#applicationScope.siteDataSource#">
		insert into pcmProductMask 
			(remoteCategoryID,maskDescription,created,createdBy,updated,updatedBy,SiteDefID,SourceID)
		values
			(<cf_queryparam value="#RemoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#MaskDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#SiteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#SourceID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		SELECT scope_identity() AS MaskID
	</cfquery>
	
	<cfreturn qry_ins_PCMProductMask.MaskID>
</cffunction>

<cffunction name="getProductSectionDetails" returntype="query">
	<cfargument name="TreeID" required="No">
	<cfargument name="SourceID" required="No">
	<cfargument name="SiteDefID" required="No">
	<cfargument name="remoteCategoryID" required="No">
	<cfargument name="productID" required="No">
	<cfargument name="sectionTypeID" required="No">
	<cfargument name="maskID" required="No">
	
	<cfset qryProductSections = "">
	
	<cfif isDefined("arguments.remotecategoryID")>
		<cfquery name="getCategoryMask" datasource="#applicationScope.siteDataSource#">
			select distinct maskID from pcmProductMask pm 
			where remotecategoryID =  <cf_queryparam value="#arguments.remoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and pm.SourceID =  <cf_queryparam value="#arguments.SourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and pm.SiteDefID =  <cf_queryparam value="#arguments.SiteDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<cfset maskID = getCategoryMask.maskID>
	</cfif>
	
	<cfif not isDefined("maskID") or maskID eq "">
		<cfset maskID = 0>
	</cfif>
	
	<cfquery name="qryProductSections" datasource="#applicationScope.siteDataSource#">
		select 
		<cfif isDefined("arguments.remotecategoryID")>
			 distinct
			 '#maskID#' as maskID,
			 --SECTIONS
			 pts.masked as sectionMask, 
			 pts.sequence as sectionSequence,
			 pmtSectionMask.transformationvalue as sectionTransformationMask,
			 pmtSectionSequence.transformationvalue  as sectionTransformationSequence, 
			 --ATTRIBUTES
			 ptsa.masked as defaultAttributeMask,
			 ptsa.sequence as defaultAttributeSequence, 
			 pmtAttributeMask.transformationValue as attributeTransformationMask, 
			 pmtAttributeSequence.transformationValue as attributeTransformationSequence,
			 --ATTRIBUTE GROUPS
			 ptsag.masked as defaultAttributeGroupMask,
			 ptsag.sequence as defaultAttributeGroupSequence,
			 pmtAttributeGroupMask.transformationValue as attributeGroupTransformationMask, 
			 pmtAttributeGroupSequence.transformationValue as attributeGroupTransformationSequence,
			 

			 isNull (pmtSectionMask.transformationValue,pts.masked) as currentSectionMask,
			 isNull (pmtAttributeMask.transformationValue,ptsa.masked) as currentAttributeMask,
			 isNull (pmtAttributeGroupMask.transformationValue,ptsag.masked) as currentAttributeGroupMask,
		 <cfelse>
		 	<!--- NYB 2009-05-07 Lexmark Bug 2144 - added distinct: --->
			 distinct 
		 	 pt.textData,
			 pt.textDataCount,
		 </cfif>
		 --ORDERING DATA
		 isNull (pmtSectionSequence.transformationValue,pts.sequence) as currentSectionSequence,
		 isNull (pmtAttributeGroupSequence.transformationValue,ptsag.sequence) as currentAttributeGroupSequence,
		 isNull (pmtAttributeSequence.transformationValue,ptsa.sequence) as currentAttributeSequence,
 		 pts.remotecategoryid,
		 ptsa.remoteAttributeID,
		 ptsa.sectionID,
		 pt.sectionAttributeID,
		 ptst.sectionTypeID,
		 ptsag.sectionAttributeGroupID,
		 sectionTypeName,
		 sectionAttributeName,
		 sectionAttributeGroupName

 		 <cfif not isDefined("arguments.remotecategoryID")>
			 ,displaySuppliesLink = CASE WHEN sectiontypeName = 'pcm_sections_supplies' AND textdatacount = 1 AND textdata IN
                          (SELECT DISTINCT cast(PCMproduct.productkey AS varchar(255))
                            FROM          PCMproductCategoryProduct INNER JOIN
                                                   PCMproduct ON PCMproductCategoryProduct.productID = PCMproduct.productID
                            WHERE 1=1 		
								<cfif isDefined("arguments.treeID")>
									and pcp.treeID =  <cf_queryparam value="#arguments.treeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								</cfif> 
							AND processstatus <> 'suppressed') THEN 1 ELSE 0 END
		</cfif>
		 
		from 
		pcmProductCategoryProduct pcp
		
		inner join pcmProductCategory pc
		on pcp.categoryid=pc.categoryID
		
		inner join pcmProduct p
		on pcp.productid=p.productid
		
		  inner join 
			   
		 pcmProductText pt on pcp.productID = pt.productID
		  inner join 
		  
		 pcmProductTextSectionAttributes ptsa 
		  on pt.sectionAttributeID = ptsa.sectionAttributeID
     	  left join
		  
		 pcmProductTextSectionAttributeGroup ptsag
		  on ptsa.sectionAttributeGroupID = ptsag.sectionAttributeGroupID
		  inner join 
		  
		 pcmProductTextSection pts
		  on ptsa.sectionid = pts.sectionid
		  inner join 
		  
		 pcmProductTextSectionType ptst
		  on pts.sectionTypeID = ptst.sectionTypeID
		  left join
		  
		 pcmProductMaskTransformation pmtSectionMask
		  on ptsa.sectionid = pmtSectionMask.sectionid and pmtSectionMask.AttributeID is null  and  pmtSectionMask.maskid  in ( <cf_queryparam value="#arguments.maskID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and pmtSectionMask.transformationActionID = 1
		 
		  left join  
		 pcmProductMaskTransformation pmtSectionSequence
		  on ptsa.sectionid = pmtSectionSequence.sectionid and pmtSectionSequence.AttributeID is null and  pmtSectionSequence.maskid  in ( <cf_queryparam value="#arguments.maskID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and pmtSectionSequence.transformationActionID = 2
		 
		  left join  
		 pcmProductMaskTransformation pmtAttributeMask
		  on ptsa.remoteAttributeID = pmtAttributeMask.attributeID and pmtAttributeMask.sectionID = ptsa.sectionID and  pmtAttributeMask.maskid  in ( <cf_queryparam value="#arguments.maskID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and pmtAttributeMask.transformationActionID = 1
		  
		  left join  
		 pcmProductMaskTransformation pmtAttributeSequence
		  on ptsa.remoteAttributeID = pmtAttributeSequence.AttributeID and pmtAttributeSequence.sectionID = ptsa.sectionID and  pmtAttributeSequence.maskid  in ( <cf_queryparam value="#arguments.maskID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and pmtAttributeSequence.transformationActionID = 2
		  
		  left join  
		 pcmProductMaskTransformation pmtAttributeGroupMask
		  on ptsa.sectionAttributeGroupID = pmtAttributeGroupMask.sectionAttributeGroupID <!--- and pts.sectionID = pmtAttributeGroupMask.sectionID ---> and  pmtAttributeGroupMask.maskid  in ( <cf_queryparam value="#arguments.maskID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and pmtAttributeGroupMask.transformationActionID = 1

		 left join  
		 pcmProductMaskTransformation pmtAttributeGroupSequence
		  on ptsa.sectionAttributeGroupID = pmtAttributeGroupSequence.sectionAttributeGroupID <!--- and pts.sectionID = pmtAttributeGroupSequence.sectionID ---> and  pmtAttributeGroupSequence.maskid  in ( <cf_queryparam value="#arguments.maskID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and pmtAttributeGroupSequence.transformationActionID = 2
		
		where 1=1
		<cfif isDefined("arguments.treeID")>
			and pcp.treeID =  <cf_queryparam value="#arguments.treeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		
		<cfif isDefined("arguments.remoteCategoryID")>
			and pc.remotecategoryID =  <cf_queryparam value="#arguments.remoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
			p.languagecode='EN' and
			p.countryCode = 'GB'
		</cfif>
		
		<cfif isDefined("arguments.productID")>
			and pcp.productid =  <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			
			-- test whether section is masked in productTextSection table
			and pts.masked = 0
			-- test whether section is masked by transformation
			and (pmtSectionMask.transformationValue is null or pmtSectionMask.transformationValue = 0)
			 
			-- test whether attribute is masked in productTextSectionAttributes table
			and ptsa.masked = 0 
			-- test whether attribute is masked by transformation
			and (pmtAttributeMask.transformationValue is null or pmtAttributeMask.transformationValue = 0)
			
			-- test whether attributeGroup is masked in productTextSectionAttributeGroups table
			--and ptsag.masked = 0 
			-- test whether attributeGroup is masked by transformation
			and (pmtAttributeGroupMask.transformationValue is null or pmtAttributeGroupMask.transformationValue = 0)
		</cfif>
		
		<cfif isDefined("arguments.sectionTypeID")>
			and ptst.sectionTypeID =  <cf_queryparam value="#arguments.sectionTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		 
		order by
		 isNull (pmtSectionSequence.transformationValue,pts.sequence),
		 isNull (pmtAttributeGroupSequence.transformationValue,ptsag.sequence),
		 isNull (pmtAttributeSequence.transformationValue,ptsa.sequence)

	</cfquery>

	<cfreturn qryProductSections>
</cffunction>

<cffunction name="getProductTextData" returntype="struct">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfset var textData = structNew()>
	<cfset var selectTextQry = "">
	<cfquery name="selectTextQry" datasource="#applicationScope.siteDataSource#">
		select sectionAttributeID,textdata,userdata from PCMproductText 
			where productID = #arguments.productID#
	</cfquery>
	<cfloop list="#getProductDataSections()#" index="sectID">
			<cfset textData[sectID]["textdata"] = "">
			<cfset textData[sectID]["userdata"] = 0>
	</cfloop>
	<cfloop query="selectTextQry">
		<cfset textData[sectionAttributeID]["textdata"] = selectTextQry.textdata>
		<cfset textData[sectionAttributeID]["userdata"] = selectTextQry.userdata>
	</cfloop>
	<cfreturn textData>
</cffunction>

<cffunction name="getThumbnailImageUrl" returntype="struct">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="node" type="struct" required="Yes">
	<cfset var productUrlstruct = structNew()>
	<cfset var catidx="">
	<cfset var idx="">
	<cfset var temp ="">
	<cfset productUrlstruct.url = "">
	<cfset productUrlstruct.productID = "">

	<!--- Check the sub-nodes. --->
	<!--- Check the sub-nodes. --->
	<cfloop from="1" to="#arrayLen(node.subCategories.categoryArray)#" index="catidx">
		<cfset productUrlstruct = getThumbnailImageUrl(treeID = arguments.treeID,node= node.subCategories.categoryArray[catidx])>		
		<cfif len(productUrlstruct.url)><cfreturn productUrlstruct></cfif>
	</cfloop>
		<!--- Product Found --->
	<cfloop from="1" to="#arrayLen(node.Products.productArray)#" index="idx">
		<cfif not node.Products.productArray[idx].SummaryDataLoaded>
			<cfset temp = getProductSummaryData(treeID = arguments.treeID,product = node.Products.productArray[idx])>
		</cfif>		
		<cfif len(node.Products.productArray[idx].ThumbnailURL) AND node.Products.productArray[idx].ThumbnailURL NEQ "http://www.sonybiz.net/images/ui/X/Product-page/productdefault.jpg">
		<!--- valid image reference - check for image locally first --->	
			<cfif not fileexists("#request.productImagesDirectory#\#node.Products.productArray[idx].productID#tn.png")>		
				<cfhttp url="#request.remoteimagepath#/#node.Products.productArray[idx].ThumbnailURL#" method="get"  timeout="5"></cfhttp>
				<!--- successful get - use and exit --->
				<cfif cfhttp.statusCode eq "200 OK">					
					<cfset productUrlstruct.url = node.Products.productArray[idx].ThumbnailURL>
					<cfset productUrlstruct.productID = node.Products.productArray[idx].productID>
					<cfbreak>
				</cfif>
			<!--- use local image --->
			<cfelse>
				<cfset productUrlstruct.url = node.Products.productArray[idx].ThumbnailURL>
				<cfset productUrlstruct.productID = node.Products.productArray[idx].productID>
				<cfbreak>				
			</cfif>
		</cfif>
	</cfloop>
	<cfreturn productUrlstruct>
</cffunction>

<!--- 

	Product/Tree Management Functions

--->

<!--- 
	
	Tree Functions

--->

<cffunction name="buildNavigationTreeFromDB" >
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="updateCluster" type="string" required="no" default="true">
	
	<cfset var treeQry = "">
	<cfset var treeCategoriesQry = "">
	<cfset var productQry = "">
	<cfset var NewNavStruct = "">
	
	<cftry>
	
			<!--- 20060515 WAB & GCC 
				Added locking of trees when being rebuilt
				see comments later in template in function setupNavigation
			--->
	
		<cflock name="buildNavigationTreeFromDBLock_#arguments.treeID#" timeout="5" throwontimeout="Yes">
			
			<!--- Get the tree information. --->
			<cfset treeQry = selectTree(treeID = treeID)>
			<cfset treeCategoriesQry = selectTreeCategories(treeID= arguments.treeID)>
			<cfset productQry = selectTreeProducts(treeID)>
			<!--- Create a new navigation struct. This system assumes the product database is there. --->
			<cfset NewNavStruct = duplicate(BlankCategory)>
			
			<!--- Get the tree information --->
			<cfset NewNavStruct.TreeType = treeQry.sourcegroupid>
			<cfset NewNavStruct.LanguageCode = treeQry.LanguageCode>
			<cfset NewNavStruct.CountryCode = treeQry.CountryCode>
			<cfset NewNavStruct.title = treeQry.title>
			<cfloop query="treeCategoriesQry">
				<cfset temp = addNavigationNode(
					navigationTop = NewNavStruct,
					CategoryNameFullPath = treeCategoriesQry.CategoryName,
					CategoryID=treeCategoriesQry.CategoryID,
					ParentCategoryID=treeCategoriesQry.ParentCategoryID)>		
			</cfloop>
			<!--- Now add the product pointer.	 --->
			<cfloop query="productQry">
				<cfif productQry.processStatus neq "SUPPRESSED">
					<cfset success = assignProductToNavigationTree(
						navigationTop = NewNavStruct,
						CategoryID = productQry.CategoryID,
						productID = productQry.productID,
						promotion = productQry.promotion)>		
				</cfif>
			</cfloop>
			<!--- Now assign the newNavStruct. --->
			<cfset applicationScope.navstruct.navigation["#arguments.treeID#"]=duplicate(NewNavStruct)>
			
		</cflock>
	
		<cfcatch type="any">
			<cfrethrow>
		</cfcatch>
	
	</cftry>
	
		<!--- WAB 2008/11/19 --->
	<cfif updateCluster > 
			<cfset applicationScope.com.clusterManagement.updateCluster(updatemethod="buildNavigationTreeFromDB",treeID = #treeID#)>
	</cfif>  

	
</cffunction>

<cffunction name="getTreeFromTreeGroup" returntype="numeric">
	<cfargument name="groupID" required="Yes">
</cffunction>

<cffunction name="selectTreeCategories" returntype="query" >
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="returnQryAvailable"  default="No">
	<cfargument name="ParentCategoryID"  default="">
	<cfargument name="ParentCategoryName"  default="">

		
	<cfset var treeCategoriesQry = "">
	<cfset var emptyProductTempCategoryTable = "">
	<cfset var putCategoriesQry = "">
	<cfset var returnSortedQry = "">
	<cfset var tmpTreeCategoryImageStatus = "">
	<cfset var fileName = "">
	<cfif not arguments.returnQryAvailable>
		<cfset returnQry = QueryNew("TreeID,CategoryID,CategoryName,ParentCategoryID,categoryImageStatus,generation,sequence")>
		<!--- table pcmProductTempCategoryTable needs to be cleared out at the beginning of the run 
			could have tested ParentCategoryID being "", but think that not returnQryAvailable is synonymous
		--->
		<cfquery name="clearOut" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductTempCategoryTable where treeID = #arguments.TreeID#
		</cfquery>
	</cfif>

	<cfquery name="treeCategoriesQry" datasource="#applicationScope.siteDataSource#">
		SELECT TreeID,CategoryID, isNull(displayName,categoryName) as categoryName,displayName,ParentCategoryID,
		categoryImageStatus = case when localCategoryImageProductID is null then 'Automatic' ELSE 'Fixed' END,
		sequence = CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END
			from PCMproductCategory
				WHERE treeID = #arguments.treeID#
					<cfif len(arguments.ParentCategoryID)>
						and ParentCategoryID =  <cf_queryparam value="#arguments.ParentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					<cfelse>
						and ParentCategoryID is NULL						
					</cfif>
					and deleted = 0
					order by ParentCategoryID, CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END
	</cfquery>	
	<cfloop query="treeCategoriesQry">
		<cfset fileName = "#request.productCategoryImagesDirectory#\categoryImage_#arguments.treeID#_#categoryID#.png">
		<!--- <cfoutput>category image status :#treeCategoriesQry.categoryImageStatus# -  file exists: "#fileName#" : #fileExists(fileName)#<br></cfoutput> --->
		<cfscript>
		QueryAddRow( returnQry,1);
		QuerySetCell(returnQry,"TreeID",treeCategoriesQry.TreeID);
		QuerySetCell(returnQry,"CategoryID",treeCategoriesQry.CategoryID);
		QuerySetCell(returnQry,"CategoryName",listAppend(ParentCategoryName,treeCategoriesQry.CategoryName,"|"));
		QuerySetCell(returnQry,"ParentCategoryID",treeCategoriesQry.ParentCategoryID);
		if (fileExists(fileName)) {
			tmpTreeCategoryImageStatus = "Uploaded";
			QuerySetCell(returnQry,"categoryImageStatus","Uploaded");
		} else {
			tmpTreeCategoryImageStatus = treeCategoriesQry.categoryImageStatus;
			QuerySetCell(returnQry,"categoryImageStatus",treeCategoriesQry.categoryImageStatus);
		}
		QuerySetCell(returnQry,"generation",listlen(ParentCategoryName, "|") );
		QuerySetCell(returnQry,"sequence",treeCategoriesQry.sequence);
		//QuerySetCell(returnQry,"processStatus",treeCategoriesQry.processStatus);
		
		temp = selectTreeCategories (treeID = arguments.TreeID,
			returnQryAvailable = "Yes",
			ParentCategoryID = treeCategoriesQry.CategoryID,
			ParentCategoryName = listappend(ParentCategoryName,CategoryName,"|"));
		</cfscript>
		<cfquery name="putCategoriesQry" datasource="#applicationScope.siteDataSource#">
		insert into PCMproductTempCategoryTable (TreeID,CategoryID,CategoryName,ParentCategoryID,categoryImageStatus,generation,sequence)
		values (
			<cf_queryparam value="#treeCategoriesQry.TreeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<cf_queryparam value="#treeCategoriesQry.CategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<cf_queryparam value="#listAppend(ParentCategoryName,treeCategoriesQry.CategoryName,"|")#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cfif len(treeCategoriesQry.ParentCategoryID)><cf_queryparam value="#treeCategoriesQry.ParentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse>NULL</cfif>,
			<cf_queryparam value="#tmpTreeCategoryImageStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#listlen(ParentCategoryName, "|")#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<cfif len(treeCategoriesQry.sequence)><cf_queryparam value="#treeCategoriesQry.sequence#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse>NULL</cfif>)
		</cfquery>
	</cfloop>
	<cfquery name="returnSortedQry" datasource="#applicationScope.siteDataSource#">
		Select * from PCMproductTempCategoryTable
		where treeid = #arguments.treeID#
		order by generation,parentcategoryID,sequence
	</cfquery>
		
	<cfreturn returnSortedQry>
</cffunction>

<cffunction name="selectTreeProducts" returntype="query">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfset var treeProductsQry = "">
	<cfquery name="treeProductsQry" datasource="#applicationScope.siteDataSource#">
		SELECT PCP.TreeID,PCP.CategoryID,PCP.productID,CASE WHEN pc.userOrder is NULL THEN PCP.sequence ELSE pc.userOrder END as sequence,pcp.processStatus,pcp.promotion
			from PCMproductCategoryProduct PCP inner join PCMproductCategory PC on pc.categoryID = pcp.categoryID
			WHERE PCP.TreeID = #TreeID# 
			and pc.deleted = 0
			ORDER by pcp.categoryID,CASE WHEN pcp.userOrder is NULL THEN PCP.sequence ELSE pcp.userOrder END
	</cfquery>
	<cfreturn treeProductsQry>
</cffunction>

<cffunction name="addNavigationNode" returntype="boolean">
	<cfargument name="navigationTop" required="Yes" type="struct">
	<cfargument name="CategoryNameFullPath" required="Yes" type="string">
	<cfargument name="CategoryID" required="Yes" type="string">
	<cfargument name="ParentCategoryID" required="Yes" type="string">
	<!--- Define the local set of variables. --->		
	<cfset var resultset = structNew()>
	<cfset var currentCategory = "">
	<!--- Make sure the currentNode is pointing at the top Node. --->
	<cfset var currentNode = navigationTop>
	<cfset var newCategory = structNew()>
	
	<!--- Navigation can only be inserted if the current product has no products. --->
<!--- 	<cfif arraylen(currentNode.products.productArray)>
		<cfreturn false>
	</cfif>
 --->
 	<cfset resultSet = structFindValue(navigationTop,"#arguments.categoryID#","ALL")>
	<cfloop from="1" to="#arrayLen(resultSet)#" index="resIdx">
		<cfif structKeyExists(resultSet[#resIdx#].owner,"categoryID") AND resultSet[resIdx].owner.categoryID eq arguments.categoryID>
			<!--- Category already exists. --->
			<cfreturn true>
		</cfif>
	</cfloop>
	<!--- If we are here we need to add the category.  --->
	<cfif len(arguments.ParentCategoryID)>
 		<cfset resultSet = structFindValue(navigationTop,"#arguments.ParentCategoryID#","ALL")>
		<cfset resultNotFound = true>
		<cfloop from="1" to="#arrayLen(resultSet)#" index="resIdx">
		<cfif structKeyExists(resultSet[#resIdx#].owner,"categoryID") AND resultSet[resIdx].owner.categoryID eq arguments.ParentCategoryID>
			<!--- Parent found. --->
			<cfset currentNode = resultSet[#resIdx#].owner>
			<cfset resultNotFound = false>
			<cfbreak>
		</cfif>
		</cfloop>
		
		<cfif resultNotFound>
			<!--- Not found. --->
			<cfreturn false>
		</cfif>
	<cfelse>
		<cfset currentNode = arguments.navigationTop>
	</cfif>

	<!--- Now create a new one entry. --->
	<cfset newCategory = Duplicate (BlankCategory)>
	<cfset newCategory.title = listlast(arguments.CategoryNameFullPath,"|")>
	<cfset newCategory.categoryName = arguments.CategoryNameFullPath>
	<cfset newCategory.categoryID = arguments.categoryID>
	<cfset temp = arrayAppend(currentNode.subCategories.categoryArray,duplicate(newCategory))>	
	<cfreturn true>
</cffunction>


<cffunction name="assignProductToNavigationTree" returntype="boolean" >
	<cfargument name="navigationTop" required="Yes" type="struct">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="Promotion" required="Yes">
	<cfscript>
		var category = structNew();
		var product = duplicate(this.BlankProduct);
		var nodeFound = false;
		var resultSet = arrayNew(1);
		var prodIdx = 1;   
		// Populate the product.
		product.ProductID = arguments.ProductID;
		product.Promotion = arguments.Promotion;
		
	</cfscript>
	<cfset resultSet = structFindValue(navigationTop,"#CategoryID#","ALL")>
	<!--- Find the category to which the  --->
	<cfloop from="1" to="#arrayLen(resultSet)#" index="resIdx">
		<cfif structKeyExists(resultSet[#resIdx#].owner,"categoryID") AND resultSet[resIdx].owner.categoryID eq arguments.CategoryID>
			<cfset nodeFound = true>
			<cfset category = resultSet[#resIdx#].owner>
			<cfbreak>
		</cfif>			
	</cfloop>
	
	<cfif not nodeFound>
		<cfreturn false>
	</cfif>
	<!--- Now ensure no duplicate entries. --->
	<cfset nodeFound = false>
	<cfloop from="1" to="#arraylen(category.products.productArray)#" index="prodIdx">
		<cfif category.products.productArray[prodIdx].productID EQ arguments.productID>
		
			<cfreturn true>
		</cfif>
	</cfloop>
	<!--- Add the pointer IF the category has no subcategories. --->
	<cfset temp = arrayAppend(category.products.productArray,product)>
	<cfreturn true>
	<!--- <cfif arraylen(category.subCategories.categoryArray)>
		<cfreturn false>
	<cfelse>
	</cfif> --->

</cffunction>

<!--- 

	Tree Management Action functions

--->

<cffunction name="copyCategoryToTargetCategory" output="No">
	<cfargument name="CopyChildren" default="false" type="boolean">
	<cfargument name="CopyProducts" default="true" type="boolean">
	<cfargument name="SourceTreeId" type="numeric" required="Yes">
	<cfargument name="SourceCategoryId" type="numeric" required="Yes">
	<cfargument name="RemoteCategoryId" required="No" default="">
	<cfargument name="TargetTreeId" type="numeric" required="Yes">
	<cfargument name="TargetCategoryId" required="No" default="">
	
	<cfscript>
		
	// Check the Source exists.
	var sourceCatQry = selectCategory(treeId = arguments.SourceTreeId,categoryId = arguments.SourceCategoryId);
	// Get the target Category Information.
	var targetCatQry = selectCategory(treeId = arguments.TargetTreeId,categoryId = arguments.TargetCategoryId);
	var sourceCatChildrenQry = "";
	var sourceProductQry = "";
	var temp = "";
	var temp2 = "";
	
	// Determine the Key.
	var NewCategoryName =sourceCatQry.CategoryName;
	// Insert the new category. 
	var NewCategoryID = insertCategory(treeid= arguments.TargetTreeId, 
		ParentCategoryID = arguments.TargetCategoryId, 
		CategoryName= NewCategoryName, 
		sourceTreeID = arguments.sourceTreeID,
		sourceCategoryID = arguments.sourceCategoryID,
		RemoteCategoryId = arguments.RemoteCategoryId,
		sequence = sourceCatQry.sequence);
	</cfscript>
	
	<!--- Copy children is required. --->	
	<cfif arguments.CopyProducts>	
		<!--- Get the current set of products at the source point in the tree. --->
		<cfset sourceProductQry = selectCategoryProducts (treeid = arguments.SourceTreeId, categoryId = arguments.SourceCategoryId)>
		<cfloop query="sourceProductQry">
			<cfscript>
				temp2 = insertProductByID(
					treeid = arguments.TargetTreeId, 
					categoryId = NewCategoryID,
					productID = sourceProductQry.productID, 
					processStatus = sourceProductQry.processStatus,
					sequence = sourceProductQry.sequence);				
			</cfscript>			
		</cfloop>		
	</cfif>		
	
	<cfif arguments.CopyChildren>
		<cfset sourceCatChildrenQry = selectCategoryChildCategories(treeid = arguments.SourceTreeId,categoryId = arguments.SourceCategoryId)> 
		<cfloop query="sourceCatChildrenQry">
			<cfset temp = copyCategoryToTargetCategory(
				CopyChildren = arguments.CopyChildren,
				CopyProducts = arguments.CopyProducts,
				SourceTreeId = arguments.SourceTreeId,
				SourceCategoryId = sourceCatChildrenQry.CategoryID,
				TargetTreeId = arguments.TargetTreeId,
				TargetCategoryId = NewCategoryID)>	
		</cfloop>
	</cfif>

</cffunction>

<cffunction name="copyAllCategoryProductsAndChildrenToTargetCategory">
	<cfargument name="SourceTreeId" type="numeric" required="Yes">
	<cfargument name="SourceCategoryId" type="numeric" required="Yes">
	<cfargument name="TargetTreeId" type="numeric" required="Yes">
	<cfargument name="TargetCategoryId" type="numeric" required="Yes">
	<cfscript>
		var SourceChildCatQry = "";
		var SourceCatProdQry = "";
		var temp = "";
		var temp2 = "";
		SourceChildCatQry = selectCategoryChildCategories(treeID = arguments.SourceTreeId, 
			categoryID = arguments.SourceCategoryId) ;
		SourceCatProdQry = selectCategoryProducts (treeID = arguments.SourceTreeId, 
			categoryID = arguments.SourceCategoryId);
	</cfscript>
	
	<cfloop query="SourceCatProdQry">
		<cfscript>
		temp2 = insertProductById (treeID = arguments.TargetTreeId,
			categoryID = arguments.TargetCategoryId,
			productID = SourceCatProdQry.productID,
			sequence = SourceCatProdQry.sequence, 
			processStatus = SourceCatProdQry.processStatus);	
		</cfscript>	
	</cfloop>
	
	<cfloop query="SourceChildCatQry">
		<cfscript>
		temp = copyAllCategoryProductsAndChildrenToTargetCategory (
			SourceTreeId = arguments.SourceTreeId,
			SourceCategoryId = SourceChildCatQry.categoryID,
			TargetTreeId = arguments.TargetTreeId,
			TargetCategoryId = arguments.TargetCategoryId);
		</cfscript>
	</cfloop>	
</cffunction>

<!--- Navigation Hierarchy Functions --->
<cffunction name="setupNavigation" output="No">
	<cfargument name="treeID" required="No" default="">
	<cfargument name="countryCode" required="No" default="">
	<cfargument name="languageCode" required="No" default="">
	<!--- Check that the navigation exists. --->
	<cfif not isdefined("applicationScope.NAVSTRUCT")>
		<!--- get the set of trees. --->
		<cfscript>
		applicationScope.NAVSTRUCT = structNew();
		applicationScope.NAVSTRUCT.navigation = structNew();
		</cfscript>	
	</cfif>
	
	<!--- If the treeName has been defined, then check to see if it exists. --->
	<cfif len(arguments.treeID)>
	 		
		<!--- 20060515 WAB & GCC
			Implemented locking of the trees while being reloaded into memory
			Note that this lock is opened before the line testing for existance of the tree in memory; 
			There is also an IDENTICALLY named lock in the function buildNavigationTreeFromDB().
			buildNavigationTreeFromDB() is called from here, but also in other places; by having the locks of the same name we can prevent buildNavigationTreeFromDB() being called from here at the same time as it is called from anywhere else

			Note that a cftry has been put around the code which calls this function to allow the user to have a proper message
					
		 --->
		<cflock name="buildNavigationTreeFromDBlock_#arguments.treeID#" timeout="2" throwontimeout="Yes">
			<cfscript>
				if ( (not structKeyExists(applicationScope.navstruct.navigation,"#arguments.treeID#")) OR 
					 structIsEmpty (applicationScope.navstruct.navigation[arguments.treeID]) )
				{
					// Get the tree details and build.
					//treeDetailsQry = selectTree ( treeID = arguments.treeID);
					//if (val(treeDetailsQry.active)){
						buildNavigationTreeFromDB(treeID= arguments.treeID);	
						// All ok
						return true;				
					//}
					//else {
						// Tree is not good. 
					//	return false;
					//}
				} 
				
			</cfscript>
		</cflock>
		
	</cfif>
	<cfreturn true>

</cffunction>
<!---  



--->
<cffunction name="deleteMemoryStructures">
	<cfscript>
	structDelete (application,"navstruct");
	structDelete (application,"spiceXML");
	</cfscript>
</cffunction>

	
<cffunction name="numberOfProducts" access="public" returntype="numeric" >
	<cfargument name="node" type="struct" required="Yes">
	<cfset var productCount = 0>
	<cfset var catidx = "">   <!--- WAB var'ed this 2005-09-07 --->
	<!--- Check the sub-nodes. --->
	<cfloop from="1" to="#arrayLen(node.subCategories.categoryArray)#" index="catidx">
		<!--- WAB 2006-04-10 added in a check for corrupted trees --->
		<cfif isStruct(node.subCategories.categoryArray[catidx])>
			<cfset productCount = numberOfProducts(node.subCategories.categoryArray[catidx]) + productCount>		
		<cfelse>
			<cfmail from="errors@#cgi.http_host#" to="errors@foundation-network.com" subject ="Sony Product Tree Error" type = html>
				A tree appears to be corrupted
				<cfif isDefined("navid")>
					Navid = #navid#
				</cfif>
				<cfdump var="#node#">
				
			</cfmail>
		</cfif>
		
	</cfloop>
	<cfset productCount = productCount + arrayLen(node.Products.productArray)>
	<cfreturn productCount>
</cffunction>

<cffunction name="getSetOfPromotedProduct" access="public">
	<cfargument name="navigation" type="string" required="Yes">
	<cfargument name="node" type="struct" required="Yes">
	
	<cfscript>
		var childProdArr = arrayNew(1);
		var promoProductStruct = "";
	</cfscript>
<!--- 	
	<cfloop from="1" to="#arrayLen(node.Products.productArray)#" index="prodidx">
		<cfif val(node.Products.productArray[prodidx].promotion)>
			<cfset temp = arrayAppend(productArr,node.Products.productArray[prodidx])>		
		</cfif>
	</cfloop> --->	
		<!--- Put in current node --->
		<cfif len(node.categoryID) gt 0>
			<cfset temp = arrayAppend(childProdArr,#node.categoryID#)>
		</cfif>
		<!--- Check the child and grandchild nodes. --->
		<cfloop from="1" to="#arrayLen(node.subCategories.categoryArray)#" index="catidx">	
			<cfset temp = arrayAppend(childProdArr,#node.subCategories.categoryArray[catidx].categoryID#)>	
			<cfscript>
				catQry = selectCategoryChildCategories(treeID=navigation,categoryID=node.subCategories.categoryArray[catidx].categoryID);
			</cfscript>
			<cfloop query="catQry">
				<cfset temp = arrayAppend(childProdArr,#catQry.categoryID#)>
			</cfloop>
		</cfloop>
		<cfquery name = "promoProductStruct" datasource="#applicationScope.siteDataSource#">
		SELECT     
			PCMproduct.thumbNailUrl, 
			PCMproductTree.TreeID,
			PCMproductCategoryProduct.categoryID, 
			PCMproductCategoryProduct.productID,
			PCMproduct.Model			
		FROM        
			PCMproductCategoryProduct 
		INNER JOIN
	        PCMproductTree ON PCMproductCategoryProduct.treeID = PCMproductTree.TreeID
		INNER JOIN
	        PCMproduct ON PCMproductCategoryProduct.productID = PCMproduct.productID
		WHERE     
			(promotion = 1) 
		AND 
				(PCMproductTree.TreeID =  <cf_queryparam value="#navigation#" CFSQLTYPE="CF_SQL_VARCHAR" >  )
		<!--- Tree top - want all promotion products --->
 		<cfif len(url.nodeid) neq 0>
			AND
					PCMproductCategoryProduct.categoryID  in ( <cf_queryparam value="# arraytolist(childProdArr)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
		 </cfif>
		</cfquery>
		
	<cfreturn promoProductStruct>
</cffunction>

<cffunction name="forceDelete">
		<cfargument name="treeID" required="Yes" type="numeric">
	<!--- Delete ALL aspects of a tree. --->
	<cfquery name="delqry" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductText where productID in(select ProductID from PCMproductCategoryProduct where treeID = #arguments.treeID#)
	</cfquery>
	<cfquery name="prodIdsqry" datasource="#applicationScope.siteDataSource#">
		select ProductID from PCMproductCategoryProduct where treeID = #arguments.treeID#
	</cfquery>
	
	<cfquery name="delqry" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductCategoryProduct where treeID = #arguments.treeID#
	</cfquery>
	<cfloop query="prodIdsqry">
		<cfquery name="delqry" datasource="#applicationScope.siteDataSource#">
			delete from PCMproduct where  productID =  <cf_queryparam value="#prodIdsqry.productID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		</cfquery>
	</cfloop>
	
	<cfquery name="delqry" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductCategory where treeID = #arguments.treeID#
	</cfquery>
	<cfquery name="delqry" datasource="#applicationScope.siteDataSource#">
		delete from PCMproductTree where treeID = #arguments.treeID#
	</cfquery>
	
</cffunction>
<!--- 
	The transformation actions.	
--->

<!--- The following set of functions manage the transformation actions. --->
<cffunction name="insertTransformationAction" returntype="numeric">
	<cfargument name="actionToPerform"  type="string" required="Yes">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryId" required="no" default="">
	<cfargument name="categoryName" required="no" default="">
	<cfargument name="sourceTreeId" default="">
	<cfargument name="sourceCategoryId" default="">
	<cfargument name="sourceCategoryName" default="">
	<cfargument name="productID" default="">
	<cfargument name="userOrder" default="">
	<cfargument name="ExtraParameters" default="">

	<!--- Get the 'next' sequence and insert --->
	<cftransaction>
	<cfquery name="MaxSeqQry" datasource="#applicationScope.siteDataSource#">
		SELECT max(SequenceID) as MaxID
			FROM PCMproductTreeTransformation
			WHERE TreeID = #TreeID# 
	</cfquery>
	<cfset sequenceID = val(MaxSeqQry.MaxID) + 1>
	<cfquery name="ActInsQry"  datasource="#applicationScope.siteDataSource#">
		INSERT INTO PCMproductTreeTransformation (
			treeId,
			sequenceID,
			actionToPerform,
			categoryId,
			categoryName,
			sourceTreeId,
			sourceCategoryId,
			sourceCategoryName,
			productID,
			userOrder,
			ExtraParameters) VALUES (
				#treeID#,
				#sequenceID#,
				'#actionToPerform#',
				<cfif len(arguments.categoryId)>
					#arguments.categoryId#,
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.categoryName)>
					N'#arguments.categoryName#',
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.sourceTreeId)>
					#arguments.sourceTreeId#,
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.sourceCategoryId)>
					#arguments.sourceCategoryId#,
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.sourceCategoryName)>
					N'#arguments.sourceCategoryName#',
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.productID)>
					#arguments.productID#,
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.userOrder)>
					#arguments.userOrder#,
				<cfelse>
					NULL,
				</cfif>
				<cfif len(arguments.ExtraParameters)>
					'#arguments.ExtraParameters#')
				<cfelse>
					NULL)
				</cfif>
	</cfquery>		
	</cftransaction>
	<cfreturn sequenceID>
</cffunction>

<cffunction name="selectTreeTransformationActions" returntype="query">
	<cfargument name="treeID" required="Yes" type="numeric">
	<cfset var ActSelQry = "">
	<cfquery name="ActSelQry"  datasource="#applicationScope.siteDataSource#">
		SELECT
			treeId,
			sequenceID,
			actionToPerform,
			categoryId,
			categoryName,
			sourceTreeId,
			sourceCategoryId,
			sourceCategoryName,
			productID,
			userOrder,
			ExtraParameters
			FROM  PCMproductTreeTransformation 
				WHERE treeId = #treeID#
					ORDER by sequenceID			
	</cfquery>
	<cfreturn ActSelQry>
</cffunction>

<!--- This function is tied to the treeActions.cfm  --->
<cffunction name="generateNavigationTreeFromTransformationActions">
	<cfargument name="treeID" type="numeric" required="Yes">	
	<!--- Get the set of actions. --->
	<cfset var temp = "">
	<cfset var temp2 = "">
	<cfset var temp3 = "">
	<cfset var actionSetQry = selectTreeTransformationActions(treeid)>
	
	<!--- Clear out all data relating to the tree. --->
	<cfset temp = deleteTree(treeID = treeID,keepKeyData = true,keepCategoryData = true)>
	<!--- loop over each action and perform it in sequence--->
	<cfloop query="actionSetQry">
		<cfoutput>^</cfoutput>
		<cfset temp2 = runTransformationAction (
			actionToPerform = actionSetQry.actionToPerform,
			treeId = actionSetQry.treeId,
			categoryId = actionSetQry.categoryId,
			categoryName = actionSetQry.categoryName,
			sourceTreeId = actionSetQry.sourceTreeId,
			sourceCategoryId = actionSetQry.sourceCategoryId,
			sourceCategoryName = actionSetQry.sourceCategoryName,
			productID = actionSetQry.productID,
			userOrder = actionSetQry.userOrder,
			ExtraParameters = actionSetQry.ExtraParameters)>
	</cfloop>	
	<cfset temp3 = deleteTree(treeID = treeID,keepKeyData = true,keepCategoryData = true,deletePendingDeletes=true)>
</cffunction>

<!--- 2005/11/21 - GCC - CR_SNY564 Product Tree Copy  --->
<cffunction name="copyTreeTransformationActions">
	<cfargument name="sourceTreeID" type="numeric" required="Yes">	
	<cfargument name="targetTreeID" type="numeric" required="Yes">	
	
	<cftransaction>
		<!--- clear out target tree transformations not necessary because we are always calling with a clean tree. --->
		
		<!--- insert the source tree categories into the target tree categories. --->
		<cfquery name="buildTreeCategories" datasource="#applicationScope.siteDataSource#">
			SET IDENTITY_INSERT PCMproductCategory ON 
			insert into PCMproductCategory (
			TreeID,
			categoryID,
			CategoryName,
			ParentCategoryID,
			lastUpdated,
			lastUpdatedBy,
			processStatusOld,
			displayName,
			sourceTreeID,
			sourceCategoryID,
			deleted,
			localCategoryImageProductID,
			sequence,
			userOrder
			)
			select  
			<cf_queryparam value="#arguments.targetTreeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
			categoryID,
			CategoryName,
			ParentCategoryID,
			getdate(),
			lastUpdatedBy,
			processStatusOld,
			displayName,
			sourceTreeID,
			sourceCategoryID,
			deleted,
			localCategoryImageProductID,
			sequence,
			userOrder
			from PCMproductCategory where treeID = #arguments.sourceTreeID#
			SET IDENTITY_INSERT PCMproductCategory OFF
		</cfquery>
			<!--- insert the source tree category products into the target tree categories. --->
		<cfquery name="buildTreeCategoryProducts" datasource="#applicationScope.siteDataSource#">
			insert into PCMproductCategoryProduct 
			select  
			#arguments.targetTreeID#,
			categoryID,
			productID,
			suppressed,
			processStatus,
			sequence,
			#request.relaycurrentuser.personid#,
			getdate(),
			promotion,
			userOrder
			from PCMproductCategoryProduct where treeID = #arguments.sourceTreeID#
		</cfquery>
		<!--- insert the source tree transformations into the target tree transformations. --->
		<cfquery name="buildTreeActions" datasource="#applicationScope.siteDataSource#">
			INSERT Into PCMproductTreeTransformation 
			select  
			#arguments.targetTreeID#,
			sequenceID,
			categoryID,
			categoryName,
			sourceTreeID,
			sourceCategoryID,
			sourceCategoryName,
			actionToPerform,
			productID,
			extraParameters,
			userOrder	
			from PCMproductTreeTransformation where treeID = #arguments.sourceTreeID# order by sequenceID
		</cfquery>
	</cftransaction>
</cffunction>

<cffunction name="runTransformationAction" output="No">
	<cfargument name="actionToPerform"  type="string" required="Yes">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryId" required="no">
	<cfargument name="categoryName" type="string" required="no">
	<cfargument name="sourceTreeId" default="">
	<cfargument name="sourceCategoryId" default="">
	<cfargument name="sourceCategoryName" type="string" required="no">
	<cfargument name="productID" default="">
	<cfargument name="userOrder" default="">
	<cfargument name="ExtraParameters" type="string" default="">
	
	<cfset var CopyChildren = "">
	<cfset var CopyProducts = "">
	<cfset var temp = "">
	<cfset var temp2 = "">
	<cfset var NewCategoryID = "">
	<cfset var topLevelCategoryQuery = "">
	<cfset var loopCategoryID = "">
		
	<cfswitch expression="#actionToPerform#">
		<cfcase value="AddNewCategoryToWO">
			<!--- Remove illegal characters --->
			<cfset CategoryName = replace(CategoryName,"|"," ")>
			<cfset CategoryName = trim(CategoryName)>
			<cfif len(trim(CategoryName))>
				<!--- Add the node --->
				<cfset NewCategoryID = insertCategory (treeID= arguments.treeID,parentCategoryID = arguments.CategoryID, CategoryName=trim(CategoryName), sequence = 100)>
			</cfif>
		</cfcase>
		<cfcase value="editCategoryName">
			<cfscript>
				if (len(trim(CategoryName))){
					temp = updateCategoryName (treeId = arguments.treeID, categoryID = arguments.categoryID, categoryName = trim(CategoryName));				
				}
			</cfscript>
		</cfcase>
		<cfcase value="DeleteCategory">
			<cfset temp =  deleteCategory(treeID=treeID,CategoryID=CategoryID)>
		</cfcase>
		<cfcase value="AddWFCategoryToWO">
			<!--- Break down the Extraparmeters --->
			<cfloop list="#ExtraParameters#" index="parIdx">
				<cfset temp = evaluate("#parIdx#")>
			</cfloop>
			<cfif CopyChildren eq "false" and CopyProducts eq "true">
				<cfset temp2 = copyAllCategoryProductsAndChildrenToTargetCategory (
					SourceTreeId = sourceTreeID,
					SourceCategoryId = sourceCategoryID,
					TargetTreeId = treeID,
					TargetCategoryId= val(CategoryID))>			
			<cfelse>
				<!--- 2006/10/31 GCC special case to copy whole tree - if sourcecategoryID is 0 --->
				<cfif sourceCategoryId eq 0>
					<!--- get top level categories and loop over them performing the action for each --->
					<cfset topLevelCategoryQuery = selectCategoryChildCategories(treeId = arguments.sourceTreeId, categoryID = 0)>
					<cfloop query="topLevelCategoryQuery">
						<cfset loopCategoryID = topLevelCategoryQuery.categoryID[currentRow]>
						<cfset loopRemoteCategoryID = topLevelCategoryQuery.remoteCategoryID[currentRow]>
						<!--- Copy the category. --->
						<cfset temp2 = copyCategoryToTargetCategory (
							CopyChildren = CopyChildren,
							CopyProducts = CopyProducts,
							SourceTreeId = sourceTreeID,
							SourceCategoryId = loopCategoryID,
							RemoteCategoryId = loopRemoteCategoryID,
							TargetTreeId = treeID,
							TargetCategoryId= CategoryID)>	 
					
					</cfloop>
				<cfelse>
					<cfquery name="getRemoteCategory" datasource="#applicationScope.siteDataSource#">
						Select remoteCategoryID from PCMproductCategory 
						where treeID =  <cf_queryparam value="#arguments.sourceTreeId#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and categoryID =  <cf_queryparam value="#arguments.sourceCategoryId#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery> 
					<cfset RemoteCategoryId = getRemoteCategory.RemoteCategoryId>
					<!--- Copy the category. --->
					<cfset temp2 = copyCategoryToTargetCategory (
						CopyChildren = CopyChildren,
						CopyProducts = CopyProducts,
						SourceTreeId = sourceTreeID,
						SourceCategoryId = sourceCategoryID,
						RemoteCategoryId = RemoteCategoryId,
						TargetTreeId = treeID,
						TargetCategoryId= CategoryID)>	
					</cfif>	
			</cfif>	
		</cfcase>
		<cfcase value="changeProductStatus">
			<cfset temp = evaluate(ExtraParameters)>
			<cfset temp2 = changeProductStatus (
				treeID=arguments.treeID,
				CategoryID=arguments.CategoryID,
				productID = arguments.productID,
				status = status)>
		
		</cfcase>
		<cfcase value="changeProductPromotionStatus">
			<cfset temp = evaluate(ExtraParameters)>
			<cfset temp2 = changeProductPromotionStatus (
				treeID=arguments.treeID,
				CategoryID=arguments.CategoryID,
				productID = arguments.productID,
				promotion = promotion)>
		
		</cfcase>
		<cfcase value="copyProduct">
			<cfscript>
				/*copyProduct(
					TargetTreeId = treeID,
					TargetCategoryId= val(CategoryID),
					productKey = productKey
					);*/
			</cfscript>	
			copyProduct is disabled.
		</cfcase>
		<cfcase value="editCategoryImage">
			<cfset temp = setCategoryImage (
				treeID=arguments.treeID,
				nodeID=arguments.CategoryID,
				productID = arguments.productID)>
		</cfcase>
		<cfcase value="clearCategoryImage">
			<cfset temp = applicationScope.com.relayProductTree.clearCategoryImage(
			treeID=arguments.treeID,
			nodeID=arguments.CategoryID)>
		</cfcase>
		<cfcase value="changeUserOrder">
		<!--- GCC - well boy, here is your bit... --->
			<cfset temp = changeUserOrder (
				treeID=arguments.treeID,
				CategoryID=arguments.CategoryID,
				productID = arguments.productID,
				userOrder = arguments.userOrder)>
		</cfcase>
		<cfcase value="changeCategoryOrder">
			<cfset temp = changeCategoryOrder (
				treeID=arguments.treeID,
				CategoryID=arguments.CategoryID,
				userOrder = arguments.userOrder)>
		</cfcase>
		<cfdefaultcase>
		<cfoutput><br>Action #actionToPerform# is unknown.</cfoutput>
		</cfdefaultcase>	
	</cfswitch>
	<cfreturn>	
</cffunction>

<cffunction name="getTreeImages">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="topDirectory" default="#request.productCategoryImagesDirectory#">
	
	<cfif directoryExists("#topDirectory#\#treeID#")>
		<cfdirectory action="list" directory="#topDirectory#\#treeID#" filter="*.png" name="uploadImages" sort="datelastmodified ASC">
		<cfreturn uploadImages>
	<cfelse>
		<cfreturn 0>
	</cfif>
	
</cffunction>

<cffunction name="deleteTreeImage">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="imageFile" required="Yes">
	<cfargument name="imageDirectory" default="#request.productCategoryImagesDirectory#">
	
	<cffile action="delete" file="#arguments.imageDirectory#\#arguments.treeId#\#arguments.imageFile#">

</cffunction>

<cffunction name="uploadTreeImage">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="imageFile" required="Yes">
	<cfargument name="thumbSize" default="60">
	<cfargument name="imageDirectory" default="#request.productCategoryImagesDirectory#">
	
	<cfif structKeyExists(request,"productThumbSize")>
		<cfset arguments.thumbSize = request.productThumbSize>
	</cfif>	
	
	<cfif not directoryExists("#arguments.imageDirectory#\#treeId#")>
		<cfdirectory action="create" directory="#arguments.imageDirectory#\#treeId#">
	</cfif>
	
	<cffile action="upload" destination="#arguments.imageDirectory#\#treeId#" filefield="#arguments.imageFile#" nameconflict="overwrite">
	<cfset ImageUUID = createUUID()>
	<cfset nImage = applicationScope.com.relayImage.getImageComponent()>
	<cfscript>
		nImage.readimage(arguments.imageDirectory&"\"&arguments.treeId&"\"&cffile.serverFile);
		nImage.blur(nImage.getWidth()/350);
		if (nImage.getWidth()GT nImage.getHeight()){
		nImage.scaleWidth(arguments.thumbSize);	
		}	else {nImage.scaleHeight(arguments.thumbSize);}
						 
		nImage.writeImage(arguments.imageDirectory&"\"&arguments.treeId&"\treeImage_#ImageUUID#.png","png");
		nImage.readimage(arguments.imageDirectory&"\"&arguments.treeId&"\treeImage_#ImageUUID#.png");
		//height = nImage.getHeight();
	</cfscript>
		
	<cffile action="delete" file="#arguments.imageDirectory#\#treeId#\#serverFile#">
	
	<cfreturn "treeImage_#ImageUUID#.png">
</cffunction>

<cffunction name="assignUploadCategoryImage">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="imageFile" required="Yes">
	<cfargument name="thumbSize" default="60">
	<cfargument name="imageDirectory" default="#request.productCategoryImagesDirectory#">

	<cfif structKeyExists(request,"productThumbSize")>
		<cfset arguments.thumbSize = request.productThumbSize>
	</cfif>		

	<cfif fileExists("#arguments.imageDirectory#\#arguments.treeId#\#arguments.imageFile#")>
		<cfset newFileName = "#arguments.imageDirectory#\categoryImage_#arguments.treeId#_#arguments.categoryID#.png">
	
		<!--- <cffile action="copy" source="#arguments.imageDirectory#\#arguments.treeId#\#arguments.imageFile#" destination="#newFileName#" nameconflict="overwrite"> --->
		
		<cfset nImage = applicationScope.com.relayImage.getImageComponent()>
		<cfscript>
			nImage.readimage("#arguments.imageDirectory#\#arguments.treeId#\#arguments.imageFile#");
			nImage.writeImage("#newFileName#","png");
		</cfscript>
	</cfif>
</cffunction>

<cffunction name="deleteCategoryImage">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryId" required="Yes" default="">
	<cfargument name="imageDirectory" default="#request.productCategoryImagesDirectory#">
	
	<cffile action="delete" file="#arguments.imageDirectory#\categoryImage_#arguments.treeID#_#arguments.categoryID#.png">

</cffunction>

<cffunction name="setTreeActiveStatus">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="ActiveStatus" required="Yes">
	
	<cfquery name="treeStatUpdQry" datasource="#applicationScope.siteDataSource#">
		update PCMproductTree set Active =  <cf_queryparam value="#arguments.ActiveStatus#" CFSQLTYPE="cf_sql_float" >  where treeID = #arguments.treeID#
	</cfquery>
	
	<cfif arguments.ActiveStatus>
		<cfset temp = buildNavigationTreeFromDB(treeID = arguments.treeID)>
	<cfelse>
		<cfscript>
			structdelete(applicationScope.navstruct.navigation,arguments.treeid);
		</cfscript>
	</cfif>
</cffunction>

<cffunction name="changeProductStatus">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="status" required="Yes">
	<!--- Update the product status. --->
	<cfquery name="UpdateProductQry"  datasource="#applicationScope.siteDataSource#">
			update PCMproductCategoryProduct SET
				processstatus =  <cf_queryparam value="#arguments.status#" CFSQLTYPE="CF_SQL_VARCHAR" > <!--- 2012-12-07 PPB Case 432507 changed integer to varchar --->
				WHERE treeId = #arguments.treeId# 
					AND categoryID = #arguments.categoryID# 
					AND productID = #arguments.productID#
	</cfquery>
</cffunction>
<cffunction name="changeProductPromotionStatus">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="promotion" required="Yes" type="numeric">
	<!--- Update the product status. --->
	<cfquery name="UpdateProductQry"  datasource="#applicationScope.siteDataSource#">
			update PCMproductCategoryProduct SET
				promotion = #arguments.promotion#
				WHERE treeId = #arguments.treeId# 
					AND categoryID = #arguments.categoryID# 
					AND productID = #arguments.productID#
	</cfquery>
</cffunction>

<!--- 

	This function takes a tree and performs transformation actions on that tree. Then takes the list of dependent trees and run the process again.

--->
<cffunction name="propogateTreeChanges">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfset var treeQry = "">
	<!--- get all user trees that use this data tree to refresh them --->
	<cfquery name="treeQry"  datasource="#applicationScope.siteDataSource#">
		SELECT DISTINCT PCMproductTree_1.Active, PCMproductTree_1.TreeID
		FROM         PCMproductCategoryProduct INNER JOIN
                      PCMproduct ON PCMproductCategoryProduct.productID = PCMproduct.productID INNER JOIN
                      PCMproductTree t ON PCMproduct.countryCode = t.countryCode AND PCMproduct.languageCode = t.languageCode AND PCMproduct.sourceID = t.sourceID INNER JOIN
                      PCMproductTree PCMproductTree_1 ON PCMproductCategoryProduct.treeID = PCMproductTree_1.TreeID
		WHERE     (t.TreeID = #arguments.treeID#) and (PCMproductTree_1.sourceid in (select sourceid from PCMProducttreeSource where SourceGroupID=2))
	</cfquery>
	<cfif treeQry.recordcount gt 0>
		<!--- Loop over the query perfoming the transactions and refreshing the memory structures for active trees. --->
		<cfloop query="treeQry">
			<cfoutput>prop#htmleditformat(treeQry.treeID)# </cfoutput>
			<cfscript>
				generateNavigationTreeFromTransformationActions(
				treeID = "#treeQry.treeID#");
			</cfscript>
			<cfscript>
				if (treeQry.active eq 1){			
					buildNavigationTreeFromDB(
						treeID = "#treeQry.treeID#");
				}
			</cfscript>		
		</cfloop>
	</cfif>
</cffunction>

<cffunction name="getSourceTreesForTree" output="Yes">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfset var treeQry = "">
	<cfquery name="treeQry"  datasource="#applicationScope.siteDataSource#">
		select distinct (pt.sourceTreeID) as treeID, t.sourceID from PCMproductTreeTransformation pt, PCMproductTree t 
			where pt.TreeID = #arguments.treeID# AND (t.Active = 1)
				and t.treeID = pt.sourceTreeID
	</cfquery>
	<cfreturn treeQry>

</cffunction>

<cffunction access="public" name="getLinkableProducts" hint="Returns all product information to link to products in live product trees">
	<cfargument name="productID" type="numeric" required="No">
	<cfargument name="countryID" type="numeric" required="No">
	<cfscript>
		var qGetLinkableProducts = "";
	</cfscript>
	
	<!--- NJH 2007/04/30 added pid rather than pn as node, to get specific product id for quickLinks --->
	<cfquery name="qGetLinkableProducts" datasource="#applicationScope.siteDataSource#">
		Select distinct pid as node, Description as headline, pcc,pn,plc, EID
		from	vPCMproductLinks
		where 1=1 
		<cfif isdefined('arguments.productID')>
		and pid = '#arguments.productID#'
		</cfif>
		<cfif isdefined('arguments.countryID')>
		and countryID = #arguments.countryID#
		</cfif>
		order by Description
	</cfquery>	
	<cfreturn qGetLinkableProducts>
</cffunction>

<cffunction access="public" name="getLinkableNodes" hint="Returns all product information to link to products in live product trees">
	<cfargument name="nodeID" type="numeric" required="No">
	<cfargument name="countryID" type="numeric" required="No">
	<cfscript>
		var qGetLinkableNodes = "";
	</cfscript>
	
	<cfquery name="qGetLinkableNodes" datasource="#applicationScope.siteDataSource#">
		Select distinct nodeID as node, CategoryName as headline, navID,nodeID, EID 
		from	vPCMcategoryLinks
		where 1=1
		<cfif isdefined('arguments.nodeID')>
		and nodeID = #arguments.nodeID#
		</cfif>
		<cfif isdefined('arguments.countryID')>
		and countryID = #arguments.countryID#
		</cfif>
		order by categoryName
	</cfquery>	
	<cfreturn qGetLinkableNodes>
</cffunction>

<cffunction access="public" name="getLocalCategoryImage" hint="Returns image name of local category image">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="nodeID" type="numeric" required="Yes">
	<cfscript>
		var qGetLocalCategoryImage = "";
	</cfscript>
	<cfquery name="qGetLocalCategoryImage" datasource="#applicationScope.siteDataSource#">
		SELECT (select thumbnailURL from PCMproduct where productid = localCategoryImageProductID) as URL, localCategoryImageProductID as ProductID
		from PCMproductCategory
		WHERE treeID = #arguments.treeID# and categoryID = #arguments.nodeID#
	</cfquery>
	<cfreturn qGetLocalCategoryImage>
</cffunction>

<cffunction access="public" name="getAllCategoriesBelow" hint="Returns a list of All nodes below a node">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="nodeID" type="numeric" required="Yes">

	<cfscript>
		var qGetChildren = "";
		var result = "" ;
	</cfscript>
	
	<cfquery name="qGetChildren" datasource="#applicationScope.siteDataSource#">
		SELECT     categoryID
		FROM         PCMproductCategory
		WHERE     (treeID = #arguments.treeID#) AND (ParentCategoryID = #arguments.nodeID#) AND (deleted = 0)
	</cfquery>
	<cfif qGetChildren.recordcount gt 0>
		<cfloop query="qGetChildren">
				<cfset result  = listappend(result ,#qGetChildren.categoryID#)>
				<cfset result  = listappend(getAllCategoriesBelow(treeID = #arguments.treeID#, nodeID=#qGetChildren.categoryID#),result)>
		</cfloop>
	<cfelse>
	</cfif>
	<cfreturn result>
</cffunction>

<cffunction access="public" name="getProductImages" hint="Returns all images at or below a node">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="nodeList" type="string" required="Yes">
	
	<cfscript>
		var qGetProductImages = "";
	</cfscript>
		<cfquery name="qGetProductImages" datasource="#applicationScope.siteDataSource#">
			SELECT     distinct PCMproduct.productID, PCMproduct.thumbNailUrl,PCMproduct.title
			FROM         PCMproduct 
			INNER JOIN
				PCMproductCategoryProduct ON PCMproduct.productID = PCMproductCategoryProduct.productID
			WHERE     treeID = #arguments.treeID# and  (PCMproductCategoryProduct.categoryID  IN ( <cf_queryparam value="#arguments.nodeList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
			AND len(PCMproduct.thumbNailUrl) > 0
			AND PCMproduct.thumbNailUrl <> 'http://www.sonybiz.net/images/ui/X/Product-page/productdefault.jpg'
			AND processStatus <> 'SUPPRESSED'
			ORDER BY PCMproduct.title
	</cfquery>
	<cfreturn qGetProductImages>
</cffunction>

<cffunction access="public" name="setCategoryImage" hint="stores product image details against the node">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="nodeID" type="numeric" required="Yes">
	<cfargument name="productID" type="numeric" required="Yes">
	
	<cfscript>
		var qSetCategoryImage = "";
	</cfscript>	
	<cfquery name="qSetCategoryImage" datasource="#applicationScope.siteDataSource#">
		update PCMproductCategory
		SET localCategoryImageProductID = #arguments.ProductID#
		WHERE categoryID = #arguments.nodeID# and treeID = #arguments.treeID#
	</cfquery>
	<cfoutput>image updated #htmleditformat(arguments.nodeID)# #htmleditformat(arguments.ProductID)#<br></cfoutput>
	<cfreturn "Image Updated">
</cffunction>

<cffunction access="public" name="clearCategoryImage" hint="clears product image details for the node">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="nodeID" type="numeric" required="Yes">
	<cfscript>
		var qclearCategoryImage = "";
	</cfscript>
	
	<cfquery name="qclearCategoryImage" datasource="#applicationScope.siteDataSource#">
		update PCMproductCategory
		SET localCategoryImageProductID = NULL
		WHERE categoryID = #arguments.nodeID# and treeID = #arguments.treeID#
	</cfquery>
	<cfreturn "Image Data Cleared">
</cffunction>

<cffunction name="getCategoryDetails" returntype="query">
	<cfargument name="treeID" type="numeric" required="Yes">
	<cfargument name="nodeID" type="numeric" required="Yes">
	<cfscript>
		var qGetCategoryDetails = "";
	</cfscript>
	<cfquery name="qGetCategoryDetails" datasource="#applicationScope.siteDataSource#">
		select * from PCMproductCategory where categoryID = #arguments.nodeID# and treeID = #arguments.treeID#
	</cfquery>
	<cfreturn qGetCategoryDetails>
</cffunction>

<cffunction name="changeUserOrder" hint="Reorders the products in a category based on user selection">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="productID" required="Yes" type="numeric">
	<cfargument name="userOrder" required="Yes">
	<cfscript>
		var qGetPreviousProductOrderValue = "";
		var qGetPreviousProductOrder = "";
		var qGetProductOrder = "";
		var previousProductOrderValue = "";
		var previousProductOrder = "";
		var newOrderValue = "";
		var qchangeUserOrder = "";
	</cfscript>
	<!--- Get the previous products sort order value at the userOrder position. --->
	<cfquery name="qGetProductOrder"  datasource="#applicationScope.siteDataSource#">
		select productID, CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END as actualOrderValue
		from PCMproductCategoryProduct
		where treeId = #arguments.treeId# 
		and categoryID = #arguments.categoryID#
		and productID <> #arguments.productID#
		order by CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END
	</cfquery>
	
	<!--- 2006/08/29 GCC because this all relies on the sort order values being unique within a category
	 why not set it now for all of the products in the category? --->
	<cfset userOrderIndex = 10>
	<cfloop query="qGetProductOrder">
		<cfquery name="setSortOrder"  datasource="#applicationScope.siteDataSource#">
			update PCMproductCategoryProduct
			set  userOrder =  <cf_queryparam value="#userOrderIndex#" CFSQLTYPE="cf_sql_float" >  
			where  productID =  <cf_queryparam value="#qGetProductOrder.productID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			and treeID = #arguments.treeId#
			and categoryID = #arguments.categoryID#
		</cfquery>
		<cfset userOrderIndex = userOrderIndex + 10>
	</cfloop>
	
	<cfquery name="qGetPreviousProductOrderValue"  datasource="#applicationScope.siteDataSource#">
		select productID, userOrder as actualOrderValue
		from PCMproductCategoryProduct
		where treeId = #arguments.treeId# 
		and categoryID = #arguments.categoryID#
		and productID <> #arguments.productID#
		order by userOrder
	</cfquery>
	
	

	<cfif qGetPreviousProductOrderValue.recordcount gt 0 and arguments.userOrder gt qGetPreviousProductOrderValue.recordcount>
		<!--- they want the product put to the end --->
		<cfoutput query="qGetPreviousProductOrderValue" startrow="#qGetPreviousProductOrderValue.recordcount#" maxrows="1">
			<cfset newOrderValue = qGetPreviousProductOrderValue.actualOrderValue + 5>
		</cfoutput>
	<cfelse>		
		<cfif qGetPreviousProductOrderValue.recordcount gt 0>	
		
			<cfoutput query="qGetPreviousProductOrderValue" startrow="#arguments.userOrder#" maxrows="1">
				<cfset previousProductOrderValue = qGetPreviousProductOrderValue.actualOrderValue>
			</cfoutput>
		<cfelse>
			<!--- no products in this category any longer --->
			<cfreturn "Product not found">
		</cfif>
		<cfquery name="qGetPreviousProductOrder"  datasource="#applicationScope.siteDataSource#">
			select max(Sequence) as previousProductOrder from
			(select CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END as Sequence
			from PCMproductCategoryProduct
			where treeId = #arguments.treeID# 
			and categoryID = #arguments.categoryID#
			and productID <> #arguments.productID#) base
			where Sequence  <  <cf_queryparam value="#PreviousProductOrderValue#" CFSQLTYPE="cf_sql_numeric" >
		</cfquery>
		
		<cfif qGetPreviousProductOrder.previousProductOrder eq "">
		<!--- top of the category --->
			<cfset previousProductOrder = 0>
		<cfelse>
			<cfset previousProductOrder = qGetPreviousProductOrder.previousProductOrder>
		</cfif>
	
		<cfset newOrderValue = (previousProductOrder + previousProductOrderValue)/2>
	
	</cfif>
	
	<cfquery name="qchangeUserOrder"  datasource="#applicationScope.siteDataSource#">
			update PCMproductCategoryProduct SET
					userOrder =  <cf_queryparam value="#newOrderValue#" CFSQLTYPE="cf_sql_float" >  
				WHERE treeId = #arguments.treeId# 
				AND categoryID = #arguments.categoryID# 
				AND productID = #arguments.productID#
	</cfquery>
	<cfreturn "Product Order updated">
</cffunction>

<cffunction name="changeCategoryOrder" hint="Reorders the category based on user selection">
		<cfargument name="treeID" required="Yes" type="numeric">
	<cfargument name="categoryID" required="Yes" type="numeric">
	<cfargument name="userOrder" required="Yes">
	<cfscript>
		var qGetPreviousCategoryOrderValue = "";
		var qGetPreviousCategoryOrder = "";
		var qGetCategoryOrder = "";
		var previousCategoryOrderValue = "";
		var previousCategoryOrder = "";
		var newOrderValue = "";
		var qchangeUserOrder = "";
		var categoryDetails = "";
		categoryDetails = getCategoryDetails (treeID = arguments.treeID, nodeID = arguments.categoryID);
	</cfscript>
		
	<!--- Get the previous Categorys sort order value at the userOrder position. --->
	<cfquery name="qGetCategoryOrder"  datasource="#applicationScope.siteDataSource#">
		select categoryID, CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END as actualOrderValue
		from PCMproductCategory
		where treeId = #arguments.treeId# 
		<cfif categoryDetails.parentCategoryID eq "">
		and parentCategoryID is NULL
		<cfelse>
		and  parentCategoryID =  <cf_queryparam value="#categoryDetails.parentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		</cfif>		
		and categoryID <> #arguments.categoryID#
		and deleted = 0
		order by CASE WHEN userOrder is NULL THEN sequence ELSE userOrder END
	</cfquery>
	
	<!--- 2006/08/29 GCC because this all relies on the sort order values being unique within a category
	 why not set it now for all of the categories? --->
	<cfset userOrderIndex = 10>
	<cfloop query="qGetCategoryOrder">
		<cfquery name="setSortOrder"  datasource="#applicationScope.siteDataSource#">
			update PCMproductCategory
			set  userOrder =  <cf_queryparam value="#userOrderIndex#" CFSQLTYPE="cf_sql_float" >  
			where  categoryID =  <cf_queryparam value="#qGetCategoryOrder.categoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			and treeID = #arguments.treeId#
		</cfquery>
		<cfset userOrderIndex = userOrderIndex + 10>
	</cfloop>
	
	<cfquery name="qGetPreviousCategoryOrderValue"  datasource="#applicationScope.siteDataSource#">
		select categoryID, userOrder as actualOrderValue
		from PCMproductCategory
		where treeId = #arguments.treeId# 
		<cfif categoryDetails.parentCategoryID eq "">
		and parentCategoryID is NULL
		<cfelse>
		and  parentCategoryID =  <cf_queryparam value="#categoryDetails.parentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		</cfif>		
		and categoryID <> #arguments.categoryID#
		and deleted = 0
		order by userOrder
	</cfquery>
	
	<cfif qGetPreviousCategoryOrderValue.recordcount gt 0>

		<cfif  arguments.userOrder gt qGetPreviousCategoryOrderValue.recordcount>
			
			<!--- they want the Category put to the end --->
			<cfoutput query="qGetPreviousCategoryOrderValue" startrow="#qGetPreviousCategoryOrderValue.recordcount#" maxrows="1">
				<cfset newOrderValue = qGetPreviousCategoryOrderValue.actualOrderValue + 5>
			</cfoutput>
	
		<cfelse>	
			
			<cfoutput query="qGetPreviousCategoryOrderValue" startrow="#arguments.userOrder#" maxrows="1">
				<cfset previousCategoryOrderValue = qGetPreviousCategoryOrderValue.actualOrderValue>
			</cfoutput>
		
			<cfquery name="qGetPreviousCategoryOrder"  datasource="#applicationScope.siteDataSource#">
				select max(Sequence) as previousCategoryOrder from
				(select userOrder as Sequence
				from PCMproductCategory
				where treeId = #arguments.treeId# 
				<cfif categoryDetails.parentCategoryID eq "">
					and parentCategoryID is NULL
				<cfelse>
					and  parentCategoryID =  <cf_queryparam value="#categoryDetails.parentCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
				</cfif>
					and categoryID <> #arguments.categoryID#
					and deleted = 0) base
				where Sequence  <  <cf_queryparam value="#PreviousCategoryOrderValue#" CFSQLTYPE="cf_sql_numeric" >
			</cfquery>
			
			<cfif qGetPreviousCategoryOrder.previousCategoryOrder eq "">
			<!--- top of the category --->
				<cfset previousCategoryOrder = 0>
			<cfelse>
				<cfset previousCategoryOrder = qGetPreviousCategoryOrder.previousCategoryOrder>
			</cfif>
		
			<cfset newOrderValue = (previousCategoryOrder + previousCategoryOrderValue)/2>
		
		</cfif>
		
		<cfquery name="qchangeUserOrder"  datasource="#applicationScope.siteDataSource#">
				update PCMproductCategory SET
						userOrder =  <cf_queryparam value="#newOrderValue#" CFSQLTYPE="cf_sql_float" >  
					WHERE treeId = #arguments.treeId# 
					AND categoryID = #arguments.categoryID# 
		</cfquery>
		<cfreturn "Category Order updated">		
	<cfelse>
		<cfreturn "No Categories Found">
	</cfif>
</cffunction>

<cffunction name="getSourceTreeID" hint="Returns the source tree id for a product">
	<cfargument name="productID" required="Yes" type="numeric">
	
	<cfset var qGetSourceTreeID = "">
	
	<cfquery name = "qGetSourceTreeID" datasource="#applicationScope.siteDataSource#">
		SELECT     PCMproductTree.TreeID AS sourcetreeID
		FROM         PCMproduct p INNER JOIN
		                      PCMproductTree ON p.languageCode = PCMproductTree.languageCode AND p.countryCode = PCMproductTree.countryCode AND 
		                      p.sourceID = PCMproductTree.sourceID
		WHERE     (NOT PCMproductTree.sourceid IN (select sourceid from PCMProductTreeSource where sourcegroupid=2)) AND (p.productID = #arguments.productid#)
	</cfquery>
	<cfreturn qGetSourceTreeID>
</cffunction>

<cffunction name="getProductFlagGroupData_Inherited" hint="Returns the flagID of the category or its parents in the supplied flag group if it exists">
	<cfargument name="treeID" required="yes" type="numeric">
	<cfargument name="categoryID" required="yes" type="numeric">
	<cfargument name="flagGroupID" required="yes" type="numeric">
	
	<cfscript>
		var qGetCategoryFlagID = "";
		var categoryDetails = "";
		var qGetParentCategoryID = "";
	</cfscript>
	
	<cfquery name = "qGetCategoryFlagID" datasource="#applicationScope.siteDataSource#">
		SELECT     PCMproductCategoryFlag.flagID
		FROM         PCMproductCategory INNER JOIN
        	PCMproductCategoryFlag ON PCMproductCategory.categoryID = PCMproductCategoryFlag.CategoryID
		WHERE     (PCMproductCategoryFlag.CategoryID = #arguments.CategoryID#) AND (PCMproductCategory.TreeID = #arguments.TreeID#)
		ORDER BY PCMproductCategoryFlag.created
	</cfquery>
	
	<cfif qGetCategoryFlagID.recordcount gt 0>
		<cfif applicationScope.com.flag.getFlagStructure(qGetCategoryFlagID.flagID).flaggroupID eq arguments.flagGroupID>
			<cfreturn qGetCategoryFlagID.flagID>
		</cfif>
	</cfif>
	
	<cfscript>
		categoryDetails = getCategoryDetails (treeID = arguments.treeID, nodeID = arguments.categoryID);
	</cfscript>
	<cfif isnumeric(categoryDetails.ParentCategoryID) and categoryDetails.ParentCategoryID gt 0>
		<cfscript>
			return getProductFlagGroupData_Inherited(treeID = arguments.treeID, categoryID = categoryDetails.ParentCategoryID, flagGroupID = arguments.flagGroupID);
		</cfscript>
	</cfif>	
</cffunction>

	<cffunction name="getProductFlagData" hint="Returns the CategoryID of the first, highest level category related to the flagID">
		<cfargument name="treeID" required="yes" type="numeric">
		<cfargument name="flagID" required="yes" type="numeric">
		
		<cfscript>
			var qGetProductFlagData = "";
		</cfscript>
		
		<!--- 
		we are only expecting one category to be flagged but JIC the order by 
		is enough to ensure we get the highest point in the tree where more than one is flagged 
		--->
		<cfquery name = "qGetProductFlagData" datasource="#applicationScope.siteDataSource#" maxrows="1">
			SELECT     PCMproductCategory.categoryID
			FROM         PCMproductCategory INNER JOIN
            	PCMproductCategoryFlag ON PCMproductCategory.categoryID = PCMproductCategoryFlag.CategoryID
			WHERE     (PCMproductCategory.TreeID = #arguments.treeID#)  
			AND (PCMproductCategoryFlag.flagID = #arguments.flagID#)
			order by updated desc, PCMproductCategory.categoryID
		</cfquery>
		
		<cfreturn qGetProductFlagData.categoryID>
	</cffunction>
</cfcomponent>



