<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent hint="Functions to deal with given mappings" output="false">

	<!--- This function is used to set Parent Child Hierarchy --->
	<cffunction name="getParentAccount" access="public" returntype="string" output="false">
	    <cfargument name="direction" type="string" required="true">
		<cfargument name="locationID" type="numeric" required="true">
		<cfargument name="flagID" type="string" default="HQ">
		
		<cfset var result = "">
		<cfset var getLocationHQ = "">
		
		<cfif arguments.direction eq "RW2SF">
			
			<!--- If the location is a HQ itself then do nothing (it is a parent itself); otherwise, we need to get the parent for it as it is not an HQ --->
			<cfquery name="getLocationHQ">
				select case when l.locationID = ifd.data then null else HQ.crmLocID end as parentCrmID 
				from location l with (noLock) 
					left join integerFlagData ifd on ifd.EntityID = l.OrganisationID and ifd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID=arguments.flagID).flagID#" CFSQLTYPE="cf_sql_integer" >
					inner join location HQ on HQ.locationID = ifd.data and HQ.organisationID = ifd.entityID
				where l.LocationID = #arguments.locationID#
			</cfquery>
			
			<cfset result = getLocationHQ.parentCrmID>
		</cfif>
	    <cfreturn result>
		
	</cffunction>

</cfcomponent>