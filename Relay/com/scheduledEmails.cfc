<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	2013/05/02	NJH	2013 Roadmap - change the way that the sql is build.. make use of inner joins rather than a lot of selects in the where statements.. fixed a few bugs that I came across.
					Also added new rows for the flag modified date for the filters... have a convention of flagID_lastUpdated as the column name.

    2015/08/18  DAN - add support for is null/is not null in the conditional e-mails
    2015/08/20  DAN - add support for decimal flags in the conditional e-mails

 --->


<cfcomponent displayname="scheduledEmails" hint="Provides methods for managing scheduled emails." extends="relay.com.schedule">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getEmailRemindersToRun" hint="Rturns a list of the ids of eMail reminders to run">
		
		<cfset var getEmailRemindersToRun = "">
		<cfset var scheduledEmailsToRun = "">
		
		<cfquery name="getEmailRemindersToRun" datasource="#dataSource#">
			select * from eMailReminderSchedule
		</cfquery>
		
		<cfloop query="getEmailRemindersToRun">
			<cfoutput>#datediff(getEmailRemindersToRun.frequency,getEmailRemindersToRun.lastrun,now())# #htmleditformat(getEmailRemindersToRun.frequency)# since last run<br></cfoutput>
			<cfif datediff(getEmailRemindersToRun.frequency,getEmailRemindersToRun.lastRun,now()) gt 0>
				<cfset scheduledEmailsToRun = listAppend(scheduledEmailsToRun,getEmailRemindersToRun.eMailReminderScheduleID)>
			</cfif>
		</cfloop>
		
		<cfreturn scheduledEmailsToRun>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="populateReminders" hint="Inserts eMail reminders to run">
		<cfargument name="eMailReminderScheduleID" type="numeric" required="yes">
		
		<cfset var getEmailScheduleDetails = "">
		<cfset var returnVar = "">
		
		<cfquery name="getEmailScheduleDetails" datasource="#dataSource#">
			select * from eMailReminderSchedule 
			where eMailReminderScheduleID = #arguments.eMailReminderScheduleID#
		</cfquery>
		
		<cfif getEmailScheduleDetails.dataInsertMethod neq "">
			<cfinvoke method="#getEmailScheduleDetails.dataInsertMethod#" returnvariable="returnVar">
				<cfinvokeArgument name="emailDefID" value="#getEmailScheduleDetails.emailDefID#">
				<cfinvokeArgument name="dateFlagID" value="#getEmailScheduleDetails.dateFlagID#">
				<cfinvokeArgument name="intervalDays" value="#getEmailScheduleDetails.intervaldays#">
			</cfinvoke>
			<cfreturn returnVar>
		<cfelse>
			<cfreturn "Email reminders inserted">
		</cfif>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Sends an email using genericEmail/eMailSender                
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="sendGenericReminderEmail" hint="Sends an email using genericEmail/eMailSender">
		<cfargument name="eMailReminderScheduleID" type="numeric" required="yes">
		
		<cfset var getEmailScheduleDetails = "">
		<cfset var setEmailDateSent = "">
		
		<cfquery name="getEmailScheduleDetails" datasource="#dataSource#">
			select emailTextID,e.eMailDefID from eMailReminderSchedule es
			INNER JOIN eMailDef e ON e.eMailDefID = es.eMailDefID
			where eMailReminderScheduleID = #arguments.eMailReminderScheduleID#
		</cfquery>
		
		<cfmodule template="/genericEmails/emailSender.cfm" 
			emailTextID="#getEmailScheduleDetails.emailTextID#">
		
		<cfquery name="setEmailDateSent" datasource="#dataSource#">
			UPDATE eMailReminder
			SET dateSent = getDate()
			where eMailDefID =  <cf_queryparam value="#getEmailScheduleDetails.eMailDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>

		<cfquery name="setEmailDateSent" datasource="#dataSource#">
			UPDATE eMailReminderSchedule
			SET lastRun = getDate()
			where eMailReminderScheduleID = #eMailReminderScheduleID#
		</cfquery>

		<cfreturn "Email sent">
	</cffunction>	

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Tests the Send of an email using genericEmail/eMailSender    
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="testSendGenericReminderEmail" hint="Returns details of the email and destinations">
		<cfargument name="eMailReminderScheduleID" type="numeric" required="yes">
		
		<cfset var getEmailScheduleDetails = "">
		
		<cfquery name="getEmailScheduleDetails" datasource="#dataSource#">
			select emailTextID,e.eMailDefID from eMailReminderSchedule es
			INNER JOIN eMailDef e ON e.eMailDefID = es.eMailDefID
			where eMailReminderScheduleID = #arguments.eMailReminderScheduleID#
		</cfquery>
		
		<cfmodule template="/genericEmails/emailSender.cfm" 
			emailTextID="#getEmailScheduleDetails.emailTextID#"
			runType="Test">
		
		<cfreturn "Test Run">
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="insertAMsForPersonDateFlagID" access="private" hint="Inserts acc mngrs personids for a dateFlagID">
		<cfargument name="eMailDefID" type="numeric" required="yes">
		<cfargument name="dateFlagID" type="numeric" required="yes">
		<cfargument name="intervalDays" type="numeric" required="yes">
		
		<cfset var insertEmailReminders = "">
		
		<cfquery name="insertEmailReminders" datasource="#dataSource#">
			INSERT INTO eMailReminder ([eMailDefID], [personid])
			SELECT <cf_queryparam value="#arguments.eMailDefID#" CFSQLTYPE="CF_SQL_INTEGER" >, ifd.data
				FROM integerFlagData ifd
				INNER JOIN flag f ON f.flagID = ifd.flagID
				INNER JOIN person p ON ifd.data = p.personID
				WHERE f.flagTextID = 'AccManager'
				AND ifd.entityID in 
					(select p.organisationID /*GCC should this be personID? - depneds on how flag is set up*/
					--,data,DATEDIFF(day, getdate(), data) 
					from dateflagdata
					INNER JOIN person p ON entityID = p.personid
					WHERE flagid = #arguments.dateFlagID#
					AND DATEDIFF(day, getdate(), data) < #intervalDays#)
				AND p.personID NOT IN
					(SELECT personID
					from eMailReminder
					WHERE emailDefID = #arguments.emailDefID#
					and dateSent is not NULL) 
		</cfquery>
		<cfreturn "Email reminders inserted">
	</cffunction>



<!--- START: 2010-05-27 AJC P-FNL090 Email Scheduler --->

	<cffunction access="public" name="getEmailSchedule" hint="Returns details of schedule emails" returntype="Query">
		<cfargument name="scheduleID" type="numeric" required="false" default="0"/>
		<cfargument name="currentdatetime" type="any" required="false" default=""/>
		
		<cfset var qry_get_EmailScheduleDetails = ''>
		<cfset var dayOfTheWeek = "">
		<cfset var currentDateTimeInMinutes = 0>
		
		<cfif isdate(arguments.currentdatetime)>
			<cfset dayOfTheWeek = dayofweek(arguments.currentdatetime)>
			<cfset currentDateTimeInMinutes = (hour(arguments.currentdatetime)*60)+minute(arguments.currentdatetime)>
		</cfif>
		
		<cfquery name="qry_get_EmailScheduleDetails" datasource="#dataSource#">
			select s.ScheduleID
				,s.ScheduleName
				,s.Description
				,s.ScheduleTypeID
				,s.Tablename
				,s.StartDate
				,s.EndDate
				,cast(cast(s.StartTime/60 as int) as varchar) + ':'+cast( s.StartTime % 60 as varchar) as starttime
				,cast(cast(s.EndTime/60 as int) as varchar) + ':'+cast( s.EndTime % 60 as varchar) as endtime
				,s.DailyHours
				,s.DailyMon
				,s.DailyTue
				,s.DailyWed
				,s.DailyThur
				,s.DailyFri
				,s.DailySat
				,s.DailySun
				,s.scheduleQuery
				,ed.emailDefID
				,ed.eMailTextID 
				from schedule s
			inner join scheduleEmail se on s.ScheduleID=se.ScheduleID
			inner join emailDef ed on se.emailDefID = ed.emailDefID
			<cfif arguments.scheduleID neq 0>
				where s.ScheduleID = #arguments.scheduleID#
			</cfif>
			<cfif isdate(arguments.currentdatetime)>
				and #arguments.currentdatetime# between s.startdate and s.enddate
				<!--- IT'S USEFUL TO COMMENT THE FOLLOWING LINE OUT TEMPORARILY DURING DEV --->  				
				and s.Starttime between <cf_queryparam value="#currentDateTimeInMinutes#" CFSQLTYPE="CF_SQL_INTEGER" > -2 and <cf_queryparam value="#currentDateTimeInMinutes#" CFSQLTYPE="CF_SQL_INTEGER" >+2
		
				
				<!--- START: 2011/02/17 - AJC - LID 5351 Day of the week code was missing from query --->
				and 
					(
						(
						s.DailySun = 0
						and s.DailyMon = 0
						and s.DailyTue = 0
						and s.DailyWed = 0
						and s.DailyThur = 0
						and s.DailyFri = 0
						and s.DailySat = 0
						)
						or s.DailySun = <cfif dayOfTheWeek is 1>1<cfelse>0</cfif>
						or s.DailyMon = <cfif dayOfTheWeek is 2>1<cfelse>0</cfif>
						or s.DailyTue = <cfif dayOfTheWeek is 3>1<cfelse>0</cfif>
						or s.DailyWed = <cfif dayOfTheWeek is 4>1<cfelse>0</cfif>
						or s.DailyThur = <cfif dayOfTheWeek is 5>1<cfelse>0</cfif>
						or s.DailyFri = <cfif dayOfTheWeek is 6>1<cfelse>0</cfif>
						or s.DailySat = <cfif dayOfTheWeek is 7>1<cfelse>0</cfif>
					)
				<!--- END: 2011/02/17 - AJC - LID 5351 Day of the week code was missing from query --->
			</cfif>
		</cfquery>
		
		<cfreturn qry_get_EmailScheduleDetails />
	</cffunction>

	<cffunction access="public" name="addEmailSchedule" hint="Adds a scheduled email and returns the scheduleID" returntype="Numeric">
		<cfargument name="schedulename" type="String" required="true" />
		<cfargument name="description" type="String" required="true" />
		<cfargument name="tablename" type="String" required="true" />
		<cfargument name="startdate" type="Date" required="true" />
		<cfargument name="enddate" type="any" required="false" default="" />
		<cfargument name="frequencyDailyHours" type="Numeric" required="true"/>
		<cfargument name="frequencyStartTime" type="String" required="true"/>
		<cfargument name="frequencyEndTime" type="String" required="true"/>
		<cfargument name="frequencyDaySun" type="boolean" required="true" />
		<cfargument name="frequencyDayMon" type="boolean" required="true" />
		<cfargument name="frequencyDayTue" type="boolean" required="true" />
		<cfargument name="frequencyDayWed" type="boolean" required="true" />
		<cfargument name="frequencyDayThur" type="boolean" required="true" />
		<cfargument name="frequencyDayFri" type="boolean" required="true" />
		<cfargument name="frequencyDaySat" type="boolean" required="true" />
		<cfargument name="eMailDefID" type="Numeric" required="true" />
		<cfargument name="Filters" type="struct" required="false" default="#structnew()#"/>
		<cfargument name="Recipients" type="struct" required="false" default="#structnew()#"/>
		
		<!--- TODO: should be in a setting somewhere --->
		<cfset var ScheduleTypeID = "1" />
		<cfset var scheduleID = 0 />
		<cfset var filterKeyList = "" />
		<cfset var starttime = (listfirst(frequencyStartTime,":")*60)+listlast(frequencyStartTime,":") />
		<cfset var endtime = (listfirst(frequencyEndTime,":")*60)+listlast(frequencyEndTime,":") />
		<cfset var qry_ins_EmailSchedule = "" />
		<cfset var qry_upd_Schedule = "" />
		<cfset var SQLString = ""/>
		
		<cfset scheduleID = add(schedulename=arguments.schedulename
								,description=arguments.description
								,tablename=arguments.tablename
								,scheduleTypeID=ScheduleTypeID
								,startdate=arguments.startdate
								,enddate=arguments.enddate
								,frequencyDailyHours=arguments.frequencyDailyHours
								,frequencyStartTime=starttime
								,frequencyEndTime=endtime
								,frequencyDaySun=arguments.frequencyDaySun
								,frequencyDayMon=arguments.frequencyDayMon
								,frequencyDayTue=arguments.frequencyDayTue
								,frequencyDayWed=arguments.frequencyDayWed
								,frequencyDayThur=arguments.frequencyDayThur
								,frequencyDayFri=arguments.frequencyDayFri
								,frequencyDaySat=arguments.frequencyDaySat
								,scheduleQuery=''
								)>
		
		<cfquery name="qry_ins_EmailSchedule" datasource="#dataSource#">
			insert into scheduleEmail(scheduleID,emailDefID)
			VALUES (<cf_queryparam value="#scheduleID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.eMailDefID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		</cfquery>
				
		<cfif not StructIsEmpty(arguments.Filters)>
			<cfset saveFilters(scheduleID,arguments.Filters)>
		</cfif>
		
		<cfif not StructIsEmpty(arguments.Recipients)>
			<cfset saveRecipients(scheduleID,arguments.Recipients)>
		</cfif>
		
		<cfset SQLString = buildSQL(scheduleID) />
		
		<cfquery name="qry_upd_Schedule" datasource="#dataSource#">
			UPDATE schedule
			set scheduleQuery =  <cf_queryparam value="#SQLString#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			where scheduleID =  <cf_queryparam value="#scheduleID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfreturn scheduleID />
		
	</cffunction>
	
	<cffunction access="public" name="updateEmailSchedule" hint="Updates a scheduled email and returns the scheduleID" returntype="Numeric">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		<cfargument name="schedulename" type="String" required="true" />
		<cfargument name="description" type="String" required="true" />
		<cfargument name="tablename" type="String" required="true" />
		<cfargument name="startdate" type="Date" required="true" />
		<cfargument name="enddate" type="any" required="false" default="" />
		<cfargument name="frequencyDailyHours" type="Numeric" required="true"/>
		<cfargument name="frequencyStartTime" type="String" required="true"/>
		<cfargument name="frequencyEndTime" type="String" required="true"/>
		<cfargument name="frequencyDaySun" type="boolean" required="true" />
		<cfargument name="frequencyDayMon" type="boolean" required="true" />
		<cfargument name="frequencyDayTue" type="boolean" required="true" />
		<cfargument name="frequencyDayWed" type="boolean" required="true" />
		<cfargument name="frequencyDayThur" type="boolean" required="true" />
		<cfargument name="frequencyDayFri" type="boolean" required="true" />
		<cfargument name="frequencyDaySat" type="boolean" required="true" />
		<cfargument name="eMailDefID" type="Numeric" required="true" />
		<cfargument name="Filters" type="struct" required="false" default="#structnew()#"/>
		<cfargument name="Recipients" type="struct" required="false" default="#structnew()#"/>
		
		<!--- TODO: should be in a setting somewhere --->
		<cfset var ScheduleTypeID = "1" />
		<cfset var thisScheduleID = 0 />
		<cfset var filterKeyList = "" />
		<cfset var starttime = (listfirst(arguments.frequencyStartTime,":")*60)+listlast(arguments.frequencyStartTime,":") />
		<cfset var endtime = (listfirst(arguments.frequencyEndTime,":")*60)+listlast(arguments.frequencyEndTime,":") />
		<cfset var qry_ins_EmailSchedule = "" />
		<cfset var qry_upd_Schedule = "" />
		<cfset var SQLString = ""/>
		
		<cfset thisScheduleID = update(scheduleID=arguments.scheduleID
								,schedulename=arguments.schedulename
								,description=arguments.description
								,tablename=arguments.tablename
								,scheduleTypeID=ScheduleTypeID
								,startdate=arguments.startdate
								,enddate=arguments.enddate
								,frequencyDailyHours=arguments.frequencyDailyHours
								,frequencyStartTime=starttime
								,frequencyEndTime=endtime
								,frequencyDaySun=arguments.frequencyDaySun
								,frequencyDayMon=arguments.frequencyDayMon
								,frequencyDayTue=arguments.frequencyDayTue
								,frequencyDayWed=arguments.frequencyDayWed
								,frequencyDayThur=arguments.frequencyDayThur
								,frequencyDayFri=arguments.frequencyDayFri
								,frequencyDaySat=arguments.frequencyDaySat
								,scheduleQuery=''
								)>
		
		<cfquery name="qry_ins_EmailSchedule" datasource="#dataSource#">
			update scheduleEmail
			set emailDefID=#arguments.eMailDefID#
			where scheduleID=#arguments.scheduleID#
		</cfquery>
				
		<!--- <cfif not StructIsEmpty(arguments.Filters)> --->		<!--- 2011/06/14 PPB REL106 removed this condition so that we can remove the a filter if it is the only one in the list  --->
			<cfset saveFilters(arguments.scheduleID,arguments.Filters)>
		<!--- </cfif> --->
		
		<!--- <cfif not StructIsEmpty(arguments.Recipients)> --->	<!--- 2011/06/14 PPB REL106 removed this condition so that we can remove a recipient if it is the only one in the list  --->
			<cfset saveRecipients(arguments.scheduleID,arguments.Recipients)>
		<!--- </cfif> --->
		
		<cfset SQLString = buildSQL(arguments.scheduleID) />
		
		<cfquery name="qry_upd_Schedule" datasource="#dataSource#">
			UPDATE schedule
			set scheduleQuery =  <cf_queryparam value="#SQLString#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			where scheduleID = #arguments.scheduleID#
		</cfquery>
		
		<cfreturn scheduleID />
		
	</cffunction>
	
	<cffunction access="public" name="saveFilters" hint="">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		<cfargument name="Filters" type="Struct" required="true" />
		
		<cfset var filterKeyList = "" />
		<cfset var qry_ins_filter = "" />
		<cfset var qry_del_filter = "" />
		<cfset var fkey = "" />

		<cfquery name="qry_del_filter" datasource="#application.sitedatasource#">
			DELETE FROM ScheduleFilter
			WHERE scheduleID = #arguments.scheduleID#
		</cfquery>

		<cfif not StructIsEmpty(arguments.Filters)>
			<cfset filterKeyList = structkeylist(arguments.Filters)>
			<cfloop list="#filterKeyList#" index="fkey">
				<cfquery name="qry_ins_filter" datasource="#application.sitedatasource#">
					INSERT INTO ScheduleFilter(ScheduleID,ColumnName,Operator,FilterValue)
					VALUES(<cf_queryparam value="#arguments.scheduleID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.Filters[fkey].Column#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.Filters[fkey].operator#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.Filters[fkey].Value#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				</cfquery>
			</cfloop>
		</cfif>
		
	</cffunction>
	
	<cffunction access="public" name="getFilters" hint="" returntype="Query">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		
		<cfset var qry_get_filters_fields = "" />
		<cfset var qry_get_filters_flags = "" />
		<cfset var qry_get_filters = "" />
		<cfset var rowNumber = 1>
		<cfset var FriendlyNames = "">
		<cfset var ListElementFriendlyName = "">
		<cfset var entityDetails = "">
		<cfset var filterValueListElement = "">
		
		<cfquery name="qry_get_filters_fields" datasource="#dataSource#">
			select sf.columnname as columnname, sf.operator, sf.FilterValue, infosch.DATA_TYPE,
				'phr_'+s.tablename+'_'+sf.columnName as friendlyFieldName,
				'' as friendlyName,
				s.tablename + '.' + sf.columnname as prefixedColumnName		<!--- return prefixedColumnName to avoid conflict with the same column name (eg created) existing in the entity table and a flag --->
			from scheduleFilter sf
				inner join schedule s on s.scheduleID = sf.scheduleID
				inner join information_schema.columns infosch on sf.columnname = column_name and infosch.Table_name = s.tablename
			WHERE s.scheduleID = #arguments.scheduleID# 
		</cfquery>
		
		<!--- the filterValue MAY be a comma delimited list of ids and we want to display them in a friendly format, so we loop thro the list building up the friendlyName(s) and write it back to the query  --->		
		<cfloop query="qry_get_filters_fields">
			<cfset FriendlyNames = ''>
			<cfloop  list="#FilterValue#" index="filterValueListElement">
				<cfif FindNoCase("OrganisationId",columnName) gt 0 OR FindNoCase("OrgId",columnName) gt 0>
					<cfset ListElementFriendlyName = application.com.relayPLO.getOrgDetails(organisationID=filterValueListElement).organisationName> 
				<cfelseif FindNoCase("LocationId",columnName) gt 0 OR FindNoCase("LocId",#columnName#) gt 0>					
					<cfset ListElementFriendlyName = application.com.relayPLO.getLocDetails(locationID=filterValueListElement).siteName> 
				<cfelseif FindNoCase("PersonId",columnName) gt 0>
					<cfset ListElementFriendlyName = application.com.relayPLO.getPersonDetails(personID=filterValueListElement).fullname> 
				<cfelseif FindNoCase("CountryId",columnName) gt 0>
					<cfset ListElementFriendlyName = application.countryname[filterValueListElement]> 
				<cfelse>
					<cfset ListElementFriendlyName = FilterValue> 
				</cfif>
				
				<cfset FriendlyNames = FriendlyNames & ListElementFriendlyName  & "<BR/>">
			</cfloop>

			<cfset QuerySetCell(qry_get_filters_fields, "friendlyName", "#FriendlyNames#", rowNumber)>

			<cfset rowNumber = rowNumber + 1>
		</cfloop> 

		<!--- 2011/06/06 PPB REL106 cast filterValue as nvarchar throughout query else it crashed when the filter was a string
				2013/05/02	NJH	column name can be the flagId with _lastUpdated appended to it if it is the flag modified column
		 --->
		<cfquery name="qry_get_Filters_Flags" datasource="#dataSource#">
			select sf.columnname, sf.operator, sf.FilterValue, ft.name, 
				case when len(f.namePhraseTextID) > 0 then 'phr_'+f.namePhraseTextID else f.name end + case when right(sf.columnname,12) = '_lastUpdated' then ' Last Modified' else '' end as friendlyFieldName,
				case when ft.datatable = 'boolean' and right(sf.columnname,12) != '_lastUpdated' and cast(sf.filterValue as nvarchar)='0' then 'No'
					when ft.datatable = 'boolean' and right(sf.columnname,12) != '_lastUpdated' and cast(sf.filterValue as nvarchar)='1' then 'Yes'
					else sf.filterValue end
				as friendlyName,
				sf.columnname as prefixedColumnName
			from scheduleFilter sf
			inner join schedule s on s.scheduleID = sf.scheduleID
			inner join flag f on replace(sf.columnname,'_lastUpdated','') = cast(f.flagID as varchar)
			inner join flagGroup fg on f.flagGroupID = fg.flagGroupID
			inner join flagType ft on ft.flagTypeId = fg.flagTypeID
			inner join schemaTable st on st.entityTypeID = fg.entityTypeID and s.tablename = st.entityName
			WHERE s.scheduleID = #arguments.scheduleID# 
		</cfquery>
		
		<cfquery name="qry_get_Filters" dbtype="query">
			SELECT * FROM  qry_get_filters_fields
			UNION
			SELECT * FROM  qry_get_filters_flags
			ORDER BY 1 <!--- 2011/06/21 PPB the ORDER is important cos we use it to decide whether to use an AND or an OR between filters when building SQL (eg we want all filters for organisationId together) --->
		</cfquery>	
	
		<!--- <cf_translateQueryColumn query=#qry_get_Filters# columnName="friendlyFieldName"> --->
		
		<cfreturn qry_get_Filters />
	</cffunction>
	
	<cffunction access="public" name="saveRecipients" hint="">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		<cfargument name="Recipients" type="Struct" required="true" />
		
		<cfset var recipientKeyList = "" />
		<cfset var qry_del_recipient = "" />
		<cfset var qry_ins_recipient = "" />
		<cfset var rkey = "" />
		<cfset var qry_ins_filter = "">
		
		<cfquery name="qry_del_recipient" datasource="#application.sitedatasource#">
			DELETE FROM ScheduleEmailRecipient
			WHERE scheduleID = #arguments.scheduleID#
		</cfquery>
		
		<cfif not StructIsEmpty(arguments.Recipients)>
			
			<cfset recipientKeyList = structkeylist(arguments.Recipients)>
			
			<cfloop list="#recipientKeyList#" index="rkey">
				<cfquery name="qry_ins_filter" datasource="#application.sitedatasource#">
					INSERT INTO ScheduleEmailRecipient(ScheduleID,RecipientValue)
					VALUES(<cf_queryparam value="#arguments.scheduleID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.Recipients[rkey].Value#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				</cfquery>

			</cfloop>
		</cfif>
		
	</cffunction>
	
	<cffunction access="public" name="getRecipients" hint="" returntype="Query">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		
		<cfset var qry_get_Recipients = "" />
		
		<!--- 2011/06/07 PPB alse get RecipientDisplayValue --->
		<cfquery name="qry_get_Recipients" datasource="#dataSource#">
			select ser.*, 'phr_'+ CASE WHEN LEFT(ser.RecipientValue,5)='flag_' THEN '' ELSE s.tablename +'_' END +ser.RecipientValue AS RecipientDisplayValue	<!--- don't add the tablename for flags cos some flags will already use the default translation phr_flag_FLAGTEXTID --->
			from ScheduleEmailRecipient ser
			inner join Schedule s on s.ScheduleID = ser.ScheduleID
			where ser.scheduleID = #arguments.scheduleID#
		</cfquery>

		<cf_translateQueryColumn query=#qry_get_recipients# columnName="RecipientDisplayValue">
		
		<cfreturn qry_get_Recipients />
	</cffunction>

	<!--- 2011/07/10 REL106 PPB re-vamped buildSQL AND removed buildEntitySQL (which was buildOpportunitySQL) to avoid joins for each recipient
		2013/05/02 NJH 2013 Roadmap - re-worked the where clause for flags.. use joins instead of a where clause
	  --->
	<cffunction access="public" name="buildSQL" hint="generates the SQL to store in schedule table">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		
		<cfset var qry_schedule = getEmailSchedule(scheduleID) />
		<cfset var SQLStringSelect = "" />
		<cfset var qry_get_Filters = "" />
		<cfset var sqlOperator = "" />
		<cfset var currentColumnname = "">
		<cfset var sqlOperatorSign = "">
		<cfset var qry_get_Recipients = "">
		<cfset var sqlStringWhere = "WHERE 1=1">
		<cfset var joinStatement = "">
		<cfset var origColumnName = "">
		<cfset var joinFlagIDs = "">
		<cfset var thisColumnName = "">

		<cfset var entityIdString = "#qry_schedule.tablename#.#application.entityType[application.entityTypeID[qry_schedule.tablename]].UniqueKey#">

		<cfset var SQLStringSelect = "SELECT #entityIdString# as entityID FROM #qry_schedule.tablename#" />
		<!--- <cfset SQLString = SQLString & " FROM " & qry_schedule.tablename />
		<cfset SQLString = SQLString & " WHERE 1=1"> --->

		<cfset qry_get_Filters = getFilters(scheduleID) />
		
		<cfif qry_get_Filters.recordcount gt 0>
			<cfset currentColumnname = "">
			<cfset sqlStringWhere = sqlStringWhere & " AND (0=0">
			
			<cfoutput query="qry_get_Filters">
				<cfset origColumnName = columnname>
				<cfset thisColumnName = listFirst(columnname,"_")>
				
				<cfif origColumnName neq currentColumnname>
					<cfset sqlStringWhere = sqlStringWhere & ") AND (" />
					<cfif isNumeric(thisColumnName) and not listFind(joinFlagIDs,thisColumnName)>
						<cfset joinStatement = joinStatement & application.com.flag.getJoinInfoForFlag(flagID=thisColumnName,returnLinkedEntity=false,entityTableAlias=qry_schedule.tablename).join>
						<cfset joinFlagIDs = listAppend(joinFlagIDs,thisColumnName)>
					</cfif>
				<cfelse>
					<cfset sqlStringWhere = sqlStringWhere & " OR " />
				</cfif>
				<cfset currentColumnname = origColumnName>
				
				<cfset sqlOperatorSign = "" />
				
				<cfswitch expression="#operator#">
					<cfcase value="Equals">
						<cfset sqlOperator = "=" />
					</cfcase>
					<cfcase value="NotEquals">
						<cfset sqlOperator = "!=" />
					</cfcase>
					<cfcase value="GreaterThan">
						<cfset sqlOperator = ">" />
					</cfcase>
					<cfcase value="LessThan">
						<cfset sqlOperator = "<" />
					</cfcase>
					<cfcase value="NotIn">
						<cfset sqlOperator = "NOT IN" />
					</cfcase>
					<cfcase value="In">
						<cfset sqlOperator = "IN" />
					</cfcase>
                    <!--- 2015/08/18 DAN - add support for is null/is not null in the conditional e-mails --->
                    <cfcase value="Is">
                        <cfset sqlOperator = " IS " /> <!--- note the spaces here so we dont need to add extra spaces when building the query below --->
                    </cfcase>

					<cfcase value="NumDaysBeforeRun">
						<cfset sqlOperator = "=" />
						<cfset sqlOperatorSign = "-" />
					</cfcase>
					<cfcase value="NumDaysBeforeRunOrMore">
						<cfset sqlOperator = "<=" />
						<cfset sqlOperatorSign = "-" />
					</cfcase>
					<cfcase value="NumDaysAfterRun">
						<cfset sqlOperator = "=" />
						<cfset sqlOperatorSign = "+" />
					</cfcase>
					<cfcase value="NumDaysAfterRunOrMore">
						<cfset sqlOperator = ">=" />
						<cfset sqlOperatorSign = "+" />
					</cfcase>

				</cfswitch>

                <!--- 2015/08/18 DAN - add support for is null/is not null in the conditional e-mails --->
				<cfswitch expression="#data_type#">
					<cfcase value="bigint,int,bit,float,money,numeric,real,smallint,smallmoney,timestamp">
						<cfif listfind("In,NotIn",operator)>
							<cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & ' ' & sqloperator & ' ' & "(" & filtervalue & ")"  >
						<cfelse>
							<cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & sqloperator & filtervalue >
						</cfif>						
					</cfcase>
					
					<cfcase value="varchar,char,text">
						<cfif listfind("In,NotIn",operator)>
							<cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & ' ' & sqloperator & ' ' & "(" & listqualify(filtervalue,"'",",","ALL") & ")"  >
                        <cfelseif operator eq "Is">
                            <cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & sqloperator & filtervalue >
						<cfelse>
							<cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & sqloperator & "'" & filtervalue & "'"  >
						</cfif>	
					</cfcase>
					
					<cfcase value="nvarchar,nchar,ntext">
						<cfif listfind("In,NotIn",operator)>
							<cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & ' ' & sqloperator & ' ' & "(" & listqualify(filtervalue,"'",",","ALL") & ")"  >
                        <cfelseif operator eq "Is">
                            <cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & sqloperator & filtervalue >
						<cfelse>
							<cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & sqloperator & "N'" & filtervalue & "'"  >
						</cfif>	
					</cfcase>
					
					<cfcase value="datetime">
                        <cfif operator eq "Is">
                            <cfset sqlStringWhere = sqlStringWhere & prefixedColumnName & sqloperator & filtervalue >
                        <cfelse>
                            <cfset sqlStringWhere = sqlStringWhere & 'dbo.dateAtMidnight(' & prefixedColumnName & ') ' & sqloperator & ' dateadd(d,' & sqloperatorSign & filtervalue & ',dbo.dateAtMidnight(getdate()))' & ' ' >
                        </cfif>
					</cfcase>

					<cfcase value="radio,checkbox">		<!--- flags --->
						<!--- <cfset sqlStringWhere = sqlStringWhere & 'ISNULL((SELECT 1 FROM booleanFlagData WHERE flagID=#ColumnName# AND entityID= #entityIdString#),0)' & sqloperator & filtervalue & ' ' > --->

						<cfif listLast(origColumnName,"_") neq "lastUpdated">
                            <cfif operator eq "Is">
                                <cfset sqlStringWhere = sqlStringWhere & "flag_#thisColumnName#.flagID " & sqloperator & filtervalue >
                            <cfelse>
    							<cfset sqlStringWhere = sqlStringWhere & "flag_#thisColumnName#.flagID is ">
    							<cfif filterValue>
    								<cfset sqlStringWhere = sqlStringWhere & "not ">
    							</cfif>
    							<cfset sqlStringWhere = sqlStringWhere & "null">
    						</cfif>
						<cfelse>
                            <cfif operator eq "Is">
                                <cfset sqlStringWhere = sqlStringWhere & "flag_#thisColumnName#.lastUpdated " & sqloperator & filtervalue >
                            <cfelse>
    							<cfset sqlStringWhere = sqlStringWhere & "dbo.dateAtMidnight(flag_#thisColumnName#.lastUpdated) #sqloperator# dateadd(d,#sqloperatorSign##filtervalue#,dbo.dateAtMidnight(getdate()))" >
    						</cfif>
						</cfif>
					</cfcase>

                    <!--- 2015-08-20 DAN - add support for decimal flags in the conditional e-mails --->
					<cfcase value="integer,decimal">	<!--- NB this is an integer flag; a table field will have type = "int"  --->
					
						<cfif listLast(origColumnName,"_") neq "lastUpdated">
							<!--- <cfset sqlStringWhere = sqlStringWhere & 'ISNULL((SELECT data FROM integerFlagData WHERE flagID=' & thisColumnName & ' AND entityID=#entityIdString#' &  '),0)' > --->
								
							<cfif listfind("In,NotIn",operator)>
								<cfset sqlStringWhere = sqlStringWhere & "flag_#thisColumnName#.data #sqloperator# (#filtervalue#)">
							<cfelse>
								<cfset sqlStringWhere = sqlStringWhere & "flag_#thisColumnName#.data #sqloperator# #filtervalue#">
							</cfif>
						<cfelse>
                            <cfif operator eq "Is">
                                <cfset sqlStringWhere = sqlStringWhere & "flag_#thisColumnName#.lastUpdated " & sqloperator & filtervalue >
                            <cfelse>
    							<cfset sqlStringWhere = sqlStringWhere & "dbo.dateAtMidnight(flag_#thisColumnName#.lastUpdated) #sqloperator# dateadd(d,#sqloperatorSign##filtervalue#,dbo.dateAtMidnight(getdate()))" >
    						</cfif>
						</cfif>
					</cfcase>
	
					<cfdefaultcase>
						<cfset sqlStringWhere = sqlStringWhere & '1=1' />
					</cfdefaultcase>
					
				</cfswitch>
			</cfoutput>
			
			<cfset sqlStringWhere = sqlStringWhere & ")" />

			<cfset qry_get_Recipients = getRecipients(arguments.scheduleID) />
			<!--- 
			<cfset SQLString = SQLString & "AND 1=0" />
			<cfloop query="qry_get_Recipients">
				<cfset SQLString = SQLString & " OR NOT EXISTS (SELECT 1 FROM ScheduleEmailLog WHERE scheduleID = #arguments.scheduleID# AND entityID=#entityIdString# AND recipientValue='#qry_get_Recipients.recipientValue#')" />
			</cfloop>
			 --->
			<!--- to save looping we can check that the total number of "persons" who have received the mail equals the number of recipients against the conditional email --->
			<cfset sqlStringWhere = sqlStringWhere & " AND (SELECT COUNT(DISTINCT recipientValue) FROM ScheduleEmailLog WHERE scheduleID = #arguments.scheduleID# AND entityID=#entityIdString# AND recipientValue IN(#quotedValueList(qry_get_Recipients.recipientValue)#))<#qry_get_Recipients.recordCount#" />

		</cfif>
		
		<cfreturn "#SQLStringSelect# #joinStatement# #sqlStringWhere#">
		
	</cffunction>
	
		
	<cffunction access="public" name="addEmailScheduleLog" hint="">
		<cfargument name="scheduleID" type="Numeric" required="true" />
		<cfargument name="entityID" type="Numeric" required="true" />
		<cfargument name="personID" type="Numeric" required="true" />
		<cfargument name="recipientValue" type="string" required="true" />
		
		<cfset var qry_ins_emailScheduleLog = "" />
		<!--- 2011/07/09 PPB REL106 added the AND NOT EXISTS clause --->
		<cfquery name="qry_ins_emailScheduleLog" datasource="#application.sitedatasource#">
			INSERT INTO ScheduleEmailLog(ScheduleID,entityID,personID,scheduleEmailLogStatusID,recipientValue,emailDefID)
			(select #arguments.scheduleID#,#arguments.entityID#,#arguments.personID#,0,<cf_queryparam value="#arguments.recipientValue#" CFSQLTYPE="CF_SQL_VARCHAR" >,emailDefID 
			from scheduleEmail 
			where scheduleID=#arguments.scheduleID#
			AND NOT EXISTS(SELECT 1 FROM ScheduleEmailLog WHERE ScheduleID=#arguments.scheduleID# AND entityID=#arguments.entityID# AND personID=#arguments.personID#))
		</cfquery>

	</cffunction>
	
	
	<cffunction access="public" name="getScheduleEmailToSend" hint="" returntype="Query">
		<cfargument name="emailBatchAmount" type="numeric" required="false" default=""/>
		
		<cfset var qry_get_scheduleEmailToSend = '' />
		
		<cfquery name="qry_get_scheduleEmailToSend" datasource="#dataSource#">
			select top #emailBatchAmount# s.*, sel.scheduleEmailLogID, sel.entityID, sel.personID,ed.eMailTextID from scheduleEmailLog sel
			inner join schedule s on sel.scheduleID = s.scheduleID
			inner join scheduleEmail se on s.ScheduleID=se.ScheduleID
			inner join emailDef ed on se.emailDefID = ed.emailDefID
			where scheduleEmailLogStatusID = 0
			order by scheduleEmailLogID
		</cfquery>
		
		<cfreturn qry_get_scheduleEmailToSend />
	</cffunction>
	
	<cffunction access="public" name="setScheduledEmailLogAsSent" hint="">
		<cfargument name="scheduleEmailLogID" type="Numeric" required="true" />
		<cfargument name="scheduleEmailLogStatusID" type="Numeric" required="true" />
		
		<cfset var qry_upd_emailScheduleLog = "" />
		
		<cfquery name="qry_upd_emailScheduleLog" datasource="#application.sitedatasource#">
			update ScheduleEmailLog
			set scheduleEmailLogStatusID=#arguments.scheduleEmailLogStatusID#
			, actionDate = getDate()
			where scheduleEmailLogID = #arguments.scheduleEmailLogID#
		</cfquery>

	</cffunction>
	
<!--- END: 2010-05-27 AJC P-FNL090 Email Scheduler --->

	<!--- 
		function which will be run automatically when cfc is loaded into application.com
		needs to be able to run without application available
	 --->
	<cffunction access="public" name="initialise" hint="">
		<cfargument name="applicationScope" default = "#application#">	
		<!--- Note that this is a global variable, do not var - this is set as a variable so that scheduled emails can be run as a housekeeping task, which is not aware of application --->
		<!--- <cfset var datasource = ""> --->
		<cfset datasource = applicationScope.sitedatasource>
	</cffunction>

	<!--- 2014-09-18	RPW	No Back button on Triggered Emails Screen --->
	<cffunction access="public" name="deleteScheduledEmail" hint="">
		<cfargument name="scheduleID" type="Numeric" required="true" />

		<cfset var qry_dlt_emailSchedule = "" />

		<cftransaction>
			<cfquery name="qry_dlt_emailSchedule" datasource="#application.sitedatasource#">
				DELETE ScheduleEmail WHERE ScheduleID = <cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.scheduleID#">

				DELETE Schedule WHERE ScheduleID = <cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.scheduleID#">
			</cfquery>
		</cftransaction>

	</cffunction>

</cfcomponent>
