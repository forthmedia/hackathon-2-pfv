<!--- �Relayware. All Rights Reserved 2014 

2016-01-13 WAB	CFAdmin Password now stored in db, but leave option of providing password to these functions for cases when saved password is incorrect
				Added functions for switching debug on and off and finding how long it has been on for
2016-01-15	WAB	Added function to set a collection of properties in one go
--->
	
<cfcomponent >

	<cffunction name="instantiateAdminApi" access="public" returnType = "Struct">
		<cfargument name="module" >   
		<cfargument name="cfpassword" default="#getCFPassword()#">   

		
		<cfset var result = structNew()>
		<cfset var adminObj = createObject("component","cfide.adminapi.administrator")>
		<cfset var login =  adminObj.login(adminpassword = cfpassword)>

		<cfif login>
			<cfset result.isOK = true>
		   	<cfset result.Obj = createObject("component","cfide.adminapi.#module#")>		
		<cfelse>
			<cfset result.isOK  =false>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="callFunction" access="public" returnType = "Struct">
		<cfargument name="module" >  
		<cfargument name="method" > 
		<cfargument name="cfpassword" default="#getCFPassword()#">   		
		
		<cfset var result = {}>
		<cfset var argumentcollection = duplicate(arguments)>
		<cfset var api = "">
		<cfset var objectPointer = "">
		
		<cfset structDelete(argumentcollection,"password")> 
		<cfset structDelete(argumentcollection,"module")> 
		<cfset structDelete(argumentcollection,"method")> 

	
		<cfset api = instantiateAdminApi(cfpassword = cfpassword,module = module)>
		
		<cfif api.isOK>
			<cfset objectPointer = api.obj>
			<cfinvoke component="#objectPointer#" method="#method#" argumentCollection= #argumentCollection# returnvariable="result.result">
			
			<cfset result.isOK = true>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "Incorrect Password #cfpassword#">
		</cfif>
	
	<cfreturn result>
	
	</cffunction>
	
	<cffunction name="checkCFPassword" access="public" returnType = "boolean">
		<cfargument name="cfpassword" default="#getCFPassword()#">   

		<cfset var api = instantiateAdminApi(cfpassword = cfpassword,module = "base")>
		
		<cfreturn api.isOK>	
	</cffunction>
	

	<cffunction name="getCFPassword">
		<cfreturn application.com.settings.getSetting ("relaywareinc.cfadminpassword")>
	</cffunction>
	
	
	<cffunction name="setProperties" hint = "Sets a collection of properies" returnType="struct">
		<cfargument name="properties" default="#structNew()#">   
		<cfargument name="cfpassword" default="#getCFPassword()#">   
		
		<cfset var Result = {isOk=true, message="", updated = {}, unchanged = {}}>
		<cfset var property = "">
		<cfif checkCFPassword(cfpassword)>
			<cfloop collection="#properties#" item="property">
				<cfset var getSetting = application.com.cfadmin.callFunction(module="runtime",method="getRuntimeProperty",propertyName=property, propertyValue=properties[property], argumentCollection = arguments)>
				<cfif getSetting.result is properties[property]>
					<cfset result.unchanged[property] = properties[property]>
				<cfelse>
					<cfset var setSetting = application.com.cfadmin.callFunction(module="runtime",method="setRuntimeProperty",propertyName=property, propertyValue=properties[property], argumentCollection = arguments)>			
					<cfset result.updated[property] = properties[property]>
				</cfif>
				
			</cfloop>
		<cfelse>
			<cfset result = {isOK = false, message = "Incorrect Password"}>
		</cfif>
		
		<cfreturn result>
	</cffunction>	
	

	<cffunction name="setDebug">
		<cfargument name="debugOn" type="boolean">
		<cfargument name="setIPAddress" type="boolean" default = "false">
		<cfargument name="IPAddress" default = "#application.com.security.getRemoteAddress()#" >
		<cfargument name="cfpassword" default="#getCFPassword()#">   

		<cfset var result = structnew()>
		<cfset var getdebuggingObj = instantiateAdminApi ( module="debugging", cfpassword = cfpassword)>
		<cfset var debuggingObj = "">

		<cfif getdebuggingObj.isOK>
		   	<cfset debuggingObj = getdebuggingObj.obj>
			<cfset debuggingObj.setDebugProperty(propertyName="enableDebug", propertyValue=debugOn) >

			<cfif debugOn and setIPAddress>
				<cfset debuggingObj.setIP(IPAddress) > 
			</cfif>	

			<cfset result.debugOn = debuggingObj.getDebugProperty('enableDebug') >

			<cfif debugOn>
				<cfset server.debugLastSwitchedOn = now()>
			<cfelse>	
				<cfset structDelete(server,"debugLastSwitchedOn")>
			</cfif>

			<cfset result.isOK = true>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "Login Failed">
		</cfif>		

		<cfreturn result>
	</cffunction>
	

	<cffunction name="getDebug" returnType="boolean" hint="Note, returns false if password is wrong.  So use checkCFPassword()">
		<cfargument name="checkIPaddress" default = "false" >
		<cfargument name="IPAddress" default = "#application.com.security.getRemoteAddress()#" >
		<cfargument name="cfpassword" default="#getCFPassword()#">   

		<cfset var result = "">
		<cfset var getdebuggingObj = instantiateAdminApi ( module="debugging", cfpassword = cfpassword)>

		<cfif getdebuggingObj.isOK>
		   	<cfset var debuggingObj = getdebuggingObj.obj>
			<cfset result = debuggingObj.getDebugProperty('enableDebug') >

			<cfif result and checkIPaddress>
				<cfset result = result && (listfind(debuggingObj.getIPList(),ipaddress))?true:false>
			</cfif>
			
		<cfelse>
			<cfset result = false>
		</cfif>		

		<cfreturn result>
	</cffunction>
	

	<cffunction name="getMinutesSinceDebugLastSwitchedOn" returnType="Numeric" hint="Time since debug turned on.  -1 if not known">
		<cfset var result = -1>
		<cfif structKeyExists (server, "debugLastSwitchedOn")>
			<cfset result = datediff("n",server.debugLastSwitchedOn,now())>	
		</cfif>
		<cfreturn result>
	</cffunction>
	

	
</cfcomponent>	