<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="RelaydbInterface" hint="Manages default db table queries">
<!--- 
Features for consideration:

Based on ideas from http://www.digital-crew.com/index.cfm?action=dcDBEditorV2doc

Easy to use with built-in documentation 
Create complex interfaces with only a few lines of code 
Support for MySQL, Access, MSSQL and Oracle databases. 
Supports most standard form input mechanisms and more 
Foreign Key reference queries supported 
Reports mismatched foreign references in record chooser 
Not Null Integrity Checking before update submission 
Display/Value Lists Supported 
Number Ranges supported 
HTML WYSIWYG Support: Advanced Image insertion and editing Standard formatting options Familure Interface Toggle WYSIWYG to HTML 
Compound key support 
Simple to use interface 
Updates performed in background to reduce latency 
DHTML for speedy updating and advanced interface control 
Looks like real database application - familiar interfaces 
Error capture and reporting 
Dynamic (no refresh) column sorting 
Highly customisable 
Permission control Control New Record Permission Control Delete Permission Control Edit Permission 
Integrated security - ensures no URL bypass 
Advanced Image Support: Simple to use, interface created from few lines of code Image uploading Server-side image selection Secure Folder Traversal Auto image Resizing on upload option Advanced thumbnail generation Resizing Define thumbnail naming convention Optionally seperate thumbnail and image folders Thumbnail based image selection Will use generated thumbnails if they exist or else fake thumbnails from real images Thumbnail referential image integrity 
Optional Confirmation on Deletes 
Many Interface Types: number, boolean, boolean_yn, select_number, html, text, textbox, image, password, select_text 
Input Validation & Integrity Validation 

2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes

 --->
<cfset THIS.tableName="person">
<cfset THIS.dataSource="relayDB">
<cfset THIS.ColList = "">
<cfset THIS.numRowsToReturn = "">
<cfset THIS.whereClause="">
<!--- ==============================================================================
         Get the table column details from schemaTable                  
=============================================================================== --->

<!---  --->
	<cffunction access="public" name="getCols" output="true" returntype="query"  hint="Returns uniqueKey and dsiplayCols from schamaTable for THIS.table">
	<!--- get the columns for the given table --->
		<cfquery name="qGetCols" datasource="#application.siteDatasource#">
			select uniqueKey from schemaTable
				where entityName =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfquery>
		
		<cfset THIS.uniqueKey=qGetCols.uniqueKey>
		<cfset THIS.qGetCols = qGetCols>
		<cfreturn qGetCols>
	</cffunction>
	
<!--- ==============================================================================
                 getAllCols for THIS.tablename                                      
=============================================================================== --->
	<cffunction access="public" name="getAllCols" output="true" returntype="query" hint="Returns all of the columns for THIS.tableName">
		<!--- get all of the columns for given table information_SCHEMA.columns --->
		<cfparam name="colList" default="">
		<cfquery name="qGetAllCols" datasource="#THIS.dataSource#">
			select [Column_Name],Ordinal_Position, Column_Default, Is_Nullable, 
			Data_Type, Character_Maximum_Length 
			from information_SCHEMA.columns 
				where table_name =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				order by Ordinal_Position
				<cfif collist neq "">
					
				</cfif>
		</cfquery>
		<cfinvoke method="getCols" returnvariable="qCols"></cfinvoke>
		<cfset THIS.displayCols=THIS.ColList>
		<cfset THIS.uniqueKey=qCols.uniqueKey>
		<cfset THIS.qGetAllCols = qGetAllCols>
		<cfreturn qGetAllCols>
	</cffunction>

<!--- ==============================================================================
                 getAllRows for THIS.tablename                                      
=============================================================================== --->	
	<cffunction access="public" name="getAllRows" output="true" hint="Returns all of the rows from a table">
		<!--- this should be modified to work so that it can be passed 
				a) nothing - in this case it will get all columns
				b) a column list - in this case it will return less that the full list
		  --->
		<cfif THIS.ColList eq "">
			<cfinvoke method="getAllCols" returnvariable="qCols"></cfinvoke>
			<cfset thisColList = valueList(qCols.column_name)>
			<cfset thisColDatTypes = valueList(qCols.Data_Type)>
		<cfelse>
			<cfinvoke method="getAllCols" returnvariable="qCols">
				
			</cfinvoke>
			<cfset thisColList = valueList(qCols.column_name)>
			<cfset thisColDatTypes = valueList(qCols.Data_Type)>

		</cfif>
		<cfset colListConverted = "">
		<cfloop list="#thisColList#" index="listItem">
			<cfset colListConverted = listAppend(colListConverted, "[" & listItem & "]")>
		</cfloop>
		<cfquery name="qGetAllRows" datasource="#THIS.dataSource#">
			select <cfif THIS.numRowsToReturn neq "">top #THIS.numRowsToReturn#</cfif> #colListConverted# from [#THIS.tableName#]
			<cfif THIS.whereClause neq "">
				where 1=1
				<cfloop index="i" list="#THIS.whereClause#">
				and #i#
				</cfloop>
			</cfif>
			
		</cfquery>
		<!--- <cfreturn qGetAllRows> --->
		<cfset THIS.qGetAllRows = qGetAllRows>
		<cfset THIS.displayCols=thisColList>
		<cfset THIS.uniqueKey=listFirst(thisColList)>
	</cffunction>
	
	<cffunction access="public" name="update" output="true" hint="updates a row for this.tableName">
		<cfinvoke method="getAllCols" returnvariable="qAllCols"></cfinvoke>
		
		<cfquery name="qGetAllRows" datasource="#THIS.dataSource#">
			select top 10 #qCols.displayCols# from [#THIS.tableName#]
			<cfif THIS.whereClause neq "">
				where 1=1
				<cfloop index="i" list="#THIS.whereClause#">
				and #qCols.uniqueKey# = 1</cfloop>
			</cfif>
			
		</cfquery>
		<!--- <cfreturn qGetAllRows> --->
		<cfset THIS.qGetAllRows = qGetAllRows>
		<cfset THIS.displayCols=qCols.displayCols>
		<cfset THIS.uniqueKey=qCols.uniqueKey>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns queries containing filter rows on foreign keys        
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFilters" hint="Returns">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="filterColList" type="string" required="true">
		<cfquery name="getFilters" datasource="#dataSource#">
			select distinct #filterColList# from #THIS.tableName#
		</cfquery>
		<cfreturn filterColList>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   Returns query containing distinct column values for tablename        
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getDistinctColumnValues" hint="Returns distinct column values for tablename">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="columnName" type="string" required="true">
		<!--- 2007-04-22 SWJ - added chacheing for 5 mins by default so that the query is not re-executed every time the user pages through the data --->
		<cfargument name="cacheForXMins" type="numeric" default="5">
		<cfargument name="distinctCountColumnName" type="string" default="">		<!--- 2007-11-05 WAB - added a distinct count (for dataloads) --->

		
		<cfscript>
			var getDistinctColumnValuesQry = "";
		</cfscript>
		
		<cfquery name="getDistinctColumnValuesQry" datasource="#dataSource#" cachedwithin="#createTimeSpan(0,0,arguments.cacheForXMins,0)#">
			select [#columnName#] as Column_Value,count(*) as number_Of_Rows <cfif distinctCountColumnName is not "">, count(distinct [#distinctCountColumnName#]) as number_Of_Rows_distinct</cfif>
			from #tablename#
			group by [#columnName#]
			order by [#columnName#]
		</cfquery>
		<cfreturn getDistinctColumnValuesQry>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   Returns query containing distinct column values for tablename        
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getDistinctColumnCounts" 
		hint="Returns distinct column counts for tablename">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="tablename" type="string" required="true">
		
		<cfquery name="getAllCols" datasource="#dataSource#">
			select [Column_Name] , data_type
			from information_SCHEMA.columns 
				where table_name =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfquery>
			<cfset qColumnCounts = QueryNew("column_Name, nulls, other_values")>
		<cfloop query="getAllCols">
			<cfif data_type is not "text" and data_type is not "ntext"  and data_type is not "image" and data_type is not "uniqueidentifier"> <!--- WAB 2006-12-13 this query doesn't work for Text, ntext & image Type 2011/05/17 GCC extended to include uniqueidentifier--->
				<cfquery name="getColumnCounts" datasource="#dataSource#" timeout="120">
				select '#column_name#' as column_Name,
				(select count(*) from #tablename# where #Column_Name# is null) as nulls, 
				(select count(distinct #Column_Name#) from #tablename# where #Column_Name# is not null) as distinct_values
				</cfquery>
				<cfscript>

				// add some rows in the query and set the cells in the query 
				newRow  = QueryAddRow(qColumnCounts);
					temp = QuerySetCell(qColumnCounts, "column_Name", getColumnCounts.column_Name);
					temp = QuerySetCell(qColumnCounts, "nulls", getColumnCounts.nulls);
					temp = QuerySetCell(qColumnCounts, "other_values", getColumnCounts.distinct_values);
				</cfscript>
			<cfelse>
				<cfscript>
				// add some rows in the query and set the cells in the query 
				newRow  = QueryAddRow(qColumnCounts);
					temp = QuerySetCell(qColumnCounts, "column_Name", column_name);
					temp = QuerySetCell(qColumnCounts, "nulls", 0);
					temp = QuerySetCell(qColumnCounts, "other_values", "not available");
				</cfscript>
			
			</cfif>
		</cfloop>
		<cfreturn qColumnCounts>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    2005-08-30 SWJ Added to provide a way to see if a view exists       
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="viewExists" access="public" returntype="boolean" hint="Returns true if a view exists">
		<cfargument name="viewName" type="string" required="true">

		<cfscript>var checkView = "";</cfscript>
		<cfquery name="checkView" datasource="#application.SiteDataSource#">
			select * from INFORMATION_SCHEMA.VIEWS 
				where TABLE_NAME =  <cf_queryparam value="#arguments.viewName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfquery>

		<cfif checkView.recordCount eq 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>
	
	<cfscript>
		//2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes
		public string function GetDBUUID
		(
		
		)
		{
			var sql = "SELECT NEWID() AS NewUUID";
			var qObj = new query();
			qObj.setDatasource(application.SiteDataSource);
			var qGetDBUUID = qObj.execute(sql=sql).getResult();
			
			return qGetDBUUID.NewUUID[1];
		}
	</cfscript>
	
</cfcomponent>

