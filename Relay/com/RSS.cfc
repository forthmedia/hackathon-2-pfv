<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Functions for managing RSS feeds" hint="">

	<cffunction name="getFeeds" access="public">
		<cfargument name="countryList" type="string" required="no" default="#request.relaycurrentuser.countryList#">
		<cfargument name="languageEditRights" type="string" required="no" default="#request.relaycurrentuser.languageEditRights#">
		<cfargument name="feedID" type="numeric" required="no">

		<cfscript>
			var qGetFeeds = "";
		</cfscript>
		
		<cfquery name="qGetFeeds" datasource="#application.sitedatasource#">
			Select * from Feed
			where languageID  in ( <cf_queryparam value="#arguments.languageEditRights#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
			<!--- 2012-07-20 PPB P-SMA001 		and countryID  in ( <cf_queryparam value="#arguments.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) --->
			#application.com.rights.getRightsFilterWhereClause(entityType="feed").whereClause#	<!--- 2012-07-20 PPB P-SMA001 --->
			<cfif IsDefined("arguments.feedID") AND len(arguments.feedID)>
				AND feedID = #arguments.feedID#
			</cfif>
		</cfquery>
		<cfreturn qGetFeeds>
	</cffunction>
	
	<cffunction name="updateFeedEntry" access="public">
		<cfargument name="feedID" type="numeric" required="yes">
		<cfargument name="itemID" type="numeric" required="yes">
		<cfargument name="title" type="string" required="yes">
		<cfargument name="excerpt" type="string" required="yes">
		<cfargument name="link" type="string" required="yes">
		<cfargument name="status" type="boolean" required="yes">

		<cfscript>
			var qUpdateFeedEntry = "";
		</cfscript>
		
		<cfif arguments.status eq 0>
			<cfquery name="qUpdateFeedEntry" datasource="#application.sitedatasource#">
				if exists (select * from FeedEntries where feedID = #arguments.feedID# and itemID = #arguments.itemID#)
				Update FeedEntries
				set active = 0,
				updated = getdate(),
				updatedby = #request.relaycurrentuser.personID#
				where feedID = #arguments.feedID#
				and itemID = #arguments.itemID#
			</cfquery>			
		<cfelse>
			<cfquery name="qUpdateFeedEntry" datasource="#application.sitedatasource#">
				if not exists (select * from FeedEntries where feedID = #arguments.feedID# and itemID = #arguments.itemID#)
				Insert into FeedEntries (feedID,itemID,blogID,title,excerpt,link,created,createdby,updated,updatedby,active)
				values (#arguments.feedID#,#arguments.itemID#,#arguments.blogID#,'#arguments.title#','#arguments.excerpt#','#arguments.link#',getdate(),#request.relaycurrentuser.personID#,getdate(),#request.relaycurrentuser.personID#,1)
				else
				update feedEntries
				set active = 1,
				blogID =  <cf_queryparam value="#arguments.blogID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				title =  <cf_queryparam value="#arguments.title#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				excerpt =  <cf_queryparam value="#arguments.excerpt#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				link =  <cf_queryparam value="#arguments.link#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				where feedID = #arguments.feedID#
				and itemID = #arguments.itemID#
			</cfquery>			
		</cfif>
		<cfreturn "success">
	</cffunction>

	<cffunction name="getRSSFile" access="public">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="languageID" type="numeric" required="yes">	
		<cfargument name="feedType" type="string" required="yes">	
		<cfargument name="fileDirectory" type="string" required="no">#
		<cfargument name="FilterMethod" type="string" required="no" default="LanguageOnly">
		
		
		<cfif not isdefined('arguments.fileDirectory')>
			<cfset arguments.fileDirectory = application.userfilesabsolutepath & "\content\mxna\feeds\">
		</cfif>
		<cfscript>
			languageISOCode = lcase(application.com.commonQueries.getISOCodeFromID(entity='language',entityValue=arguments..languageID));
			countryISOCode = lcase(application.com.commonQueries.getISOCodeFromID(entity='country',entityValue=arguments.countryID));
		</cfscript>
		
		<cfswitch expression="#FilterMethod#">
			<cfcase value="LanguageOnly">
				<cfswitch expression="#languageISOCode#">
					<cfcase value="ES,DE,FR,NL,IT">
						<cfset RSSFile = languageISOCode & '_' & languageISOCode & '_' & arguments.feedType & ".xml">
					</cfcase>
					<cfdefaultcase>
						<cfset RSSFile = languageISOCode & '_GB_' & arguments.feedType & ".xml">
					</cfdefaultcase>
				</cfswitch>
			</cfcase>
			<cfdefaultcase>
				<cfset RSSFile = languageISOCode & '_' & countryISOCode & '_' & arguments.feedType & ".xml">
			</cfdefaultcase>
		</cfswitch>
		
		<cfset fullpath = arguments.fileDirectory & RSSFile>
		<cfif fileexists(#fullPath#)>			
			<cfreturn RSSFile>
		<cfelse>			
			<cfreturn "File #fullPath# not found">
		</cfif>		
	</cffunction>

	<cffunction name="createFeed" access="public">
		<cfargument name="feedName" type="string" required="yes">
		<cfargument name="feedType" type="string" required="yes">
		<cfargument name="languageID" type="numeric" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">

		<cfscript>
			var qCheckFeed = "";
			var qCreateFeed = "";
		</cfscript>
		
		<cfquery name="qCheckFeed" datasource="#application.sitedatasource#">
			select * from feed 
			where languageID = #arguments.languageID#
			and countryID = #arguments.countryID#
			and feedType =  <cf_queryparam value="#arguments.feedType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfquery>
		
		<cfif qCheckFeed.recordcount gt 0>
			<cfset result = -1>
		<cfelse>		
			<cfquery name="qCreateFeed" datasource="#application.sitedatasource#">
				INSERT into Feed (feedname,feedtype,languageID,countryID,created,createdby,updated,updatedby)
				values (<cf_queryparam value="#arguments.feedname#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.feedType#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.languageID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >,getdate(),<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,getdate(),<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >)
			</cfquery>
		<cfset result = 1>
		</cfif>
		<cfreturn result>
	</cffunction>	
</cfcomponent>
