<!--- �Relayware. All Rights Reserved 2014 --->
<!---
CFC providing functionality for managing files.

AR 2005-03-18 Added functionality to enable file uploading and sharing.

WAB 2006-02-21  added function for getting files a user has views rights to
2006-10-25		WAB		altered getfileList query to do countryscopes
2007-09-25		SSS		altered getFileList in order to display the size of the file
2008/10/07		SSS		File expired should not expire for internal people.
2009/05/27		WAB		LID2237
						Got secured files to work again
						Altered getfileList to bring back fileNameAndPath which is specially doctored to reflect the setting of 'secure'  (adds .cfm to end)
						Added new functions for dealing with creating and removing secured files and stubs
2009/07/14		NJH		Added support for securing related files.
2009/08/20		SSS		Altered functions in order to get the capbility of getting zipfiled courseware files to work.
2009-09-04		NYB		P-SNY081 - created addFileToDb function
2009/09/08		WAB		LID 2512 Lots of work on to deal with Zip files
2009/10/02		NJH		LID 2731
2009-10-06              NYB             multi lang support for filenames
2009/12/02		NJH		LID 2895
2010/04/12        WAB   For missing template handle, rcreated function getFileIDFromScriptName from code in manageFileSecurity function
2010/06			WAB		FNL088 changed to use new path variables and new secur file location, get rid of stubs
2010/09/14 		WAB		Removed code which assumed that file extensions have 3 characters, replace with list last
2011/01/12 		WAB 	Changes to handling of temporary files
						Temporary files from each instance are now put in their own sub directory
						Temporary file can be put in a non SSL directory (mainly for use on dev sites - where certificates are self signed.  Needs the filesTemp/notSSL directory to be opened to non SSL traffic)
2011/06/07		NYB 	LHID6807 create function getFileAttributes - only really useful for swf
							and image files atm, need building upon to be useful for other file types
2011/09/05		WAB	changes to getTemporaryFolderDetails() and tempSessionFileCleanup()
2011/11/14		WAB   Removed much of the code which used to deal with secure files and stubs
2011/12/01		RMB		P-REL101 added new field "DeliveryMethod" to getFileList alaso added new functions "AllDeliveryMethods", "ValidDeliveryMethods" and "getAllFileType"
2011/12/09		RMB		P-REL101 added CreatedBy and LastUpdatedBy to files table in Function "addFileToDb"
2012/02/16		RMB 	Added "directoryExists" check before directory creates in function "getTemporaryFolderDetails"
2012-02-05 		WAB 	P-MIC001 Added support for JOINs and TOP to getFileList
2012-05-09		STCR	P-REL109 Allow Courseware search on both name column value and filename-on-disk
2012-05-29		STCR	P-REL109 File listing filters
2012-05-30		STCR	P-REL109 CASE 428533 duplicate file check.
2012/06/21 		PPB 	Case 428815 add filter for owner
2012-09-06 		PPB 	Case 430058	redirect a logged out user to login and display the file if they hit a link to a secure file (2012Summer release)
2012-09-12		IH		Case 430243 Call getFilesAPersonHasCountryViewRightsTo for backward compatibility with customized code
2012-10-??		WAB/NJH	Fixed problems when secure zipped packages contained subdirectories - we were ignoring the subdirectory paths
2013-03-27 		WAB 	CASE 434530 tempSessionFileCleanup housekeeping task failing
2013-04-11 		PPB 	Case 434536 if isUnknownUser force login for access to secure file (previously if logged out with cookie it worked ok)
2013-04- 		WAB		When copying files to filesTemp, name the folder with the fileID.  Plan to use this in future to deal with 404 errors within filesTemp
2013-07-02		NYB 	Case 435893 added with(nolock)'s throughout getFileList query
2013/09/11		NJH		2013 R2 - use getFiles function to work out what files we have rights to. Meant a change in getFileList and any functions that call this function. Got rid of some functions that we weren't using
2013-02-24		IH		Change inner join on person to outer join in GetResults
2013-07-07		NYB 	Case 439373 changed getFilesAPersonHasEditRightsTo to accept user as having rights to a file if they have level3 fileTask
2014-07-07 		PPB 	Case 440468 rejigged GetResults query to avoid using a long list of parameters multiple times (causing it to hit max number of parameters)
15/07/2014 		DCC 	P-KAS031 added filter argument to getFilesAPersonHasCountryViewRightsTo
2014/09/24  	NJH 	CASE 442176 Deal with downloading files which are URLs but are within a secure fileType
2015-01-19 		WAB		CASE 443444 getFileIDFromScriptName function, fix to deal with apostrophes in filename
2015-02-02		ACPK	CRARB001 implemented first iteration of Top Downloaded Files widget
2015-02-05		ACPK	CRARB001 fixed Top Downloaded Files widget not returning all files which user has access to
2015-06-10  	DCC 	changed from just getfile.filename as not defined if no fileid supplied
2015-06-16      DAN     Case 444633 - fix for core error with external links referenced by Relay_File_List
2015-07-30		PYW		P-TAT005. Abstract file download notification from fileget.cfm. Create method notifyResourceDownload
2015-08-03		DXC 	no case ref: force update of lastUpdatedBy and lastUpdated for audit
2015-08-05      DAN     K-1314 - allow filtering products by multiple media groups
2015-09-10		ACPK	PROD2015-40	Fixed error caused by unsafe characters in image filenames by encoding filename in URLLocation
2015-10-26		ACPK	PROD2015-208 Encode filename in file URLs; Replace plus sign in filename with "plus" string
2015-10-26      ACPK    PROD2015-333 Replace all plus signs in clientfile/clientfilename values with "plus" strings
2015-04-02 		PPB 	Case 444129 allow FileList sortorder to be publishDate,revision and lastUpdated
2016-01-29		ESZ		case 446338 File error, rename file on disk from '+' on '_' and add message for user
2015-12-01		WAB PROD2015-290 Visibility Project. Change reference to fn_getCountryRightsV2() to getCountryScopeList()
2016-01-06      ACPK    BF-195 Removed duplicate column name which was preventing sorting by revision
2016-02-02		RJT		BF-433 edited query of queries in getFilesInUserCountry to use integers for the countryID (which is required to avoid the BF-433 relayError
2016-02-24		WAB 	PROD2015-291  Media Library Visibility - Files/FileTypes/FileTypeGroups now support extendedRecordRights (although only files have it implemented in UI)
2016-03-01 		WAB		PROD2016-509 Add watermarking to PDF files (based on FileType)
2016-06-09              NJH for katya  case 449051 enable fileid search if numeric search string
2016-07-18      DAN     Case 450838 - support for custom image/color types (non RGB), if its custom type JPEG we use JAI library instead of cfimage
2016-07-21      DAN     Case 450959 - fix for moving files to and from secure when one of the file is actually URL (null filename)
2016-05-10		YMA		PROD2016-2451 update cfimage to use physical location
2016-01-09		DAN		PROD2016-3174 - fix for file deletion related to the create thumbnails story (PROD2016-2470)
2017-01-31		WAB		RT-221 Problem with dataTypes in qGetUsersCountryAndCountryGroups().  Made dataType Explicit within get2ColForCountryLists()

--->

<cfcomponent displayname="fileManager" hint="Functions for file management">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFileTypes" hint="What does this function do" validValues="true">
		<cfargument name="type" type="string" required="false">
		<cfargument name="sortOrder" type="string" default="type">
		<!--- 2015-08-05 DAN K-1314 --->
		<cfargument name="fileTypeGroupID" type="ANY" required="false" default="0">
		<cfargument name="fileTypeID" type="numeric" required="false">
		<cfargument name="fileTypeGroup" type="string" required="false">
		<cfargument name="restrictToPermissionLevel" type="numeric" required="false">

		<cfset var getFileTypesQry = '' />

		<cfquery name="getFileTypesQry" datasource="#application.SiteDataSource#">
			declare @personid int = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			SELECT distinct ft.FileTypeID,
				   ft.Type,
				   ftg.Heading,
				   ft.autounzip,<!--- WAB 2009/09/09  LID 2512 --->
				   ft.DefaultDeliveryMethod ,
				   ftg.FileTypeGroupID,
				   ft.path
			FROM FileType AS ft inner join FileTypeGroup AS ftg
				on ft.FileTypeGroupID=ftg.FileTypeGroupID
				<cfif structKeyExists(arguments,"restrictToPermissionLevel") and arguments.restrictToPermissionLevel neq "" <!--- and not application.com.login.checkInternalPermissions(securityTask="fileTask",securityLevel="level3") --->>
				left join
					dbo.getRecordRightsTableByPerson (#application.com.relayEntity.getEntityType("fileTypeGroup").entityTypeId#,@personid) as rftg on rftg.recordid = ftg.fileTypeGroupid
				left join
					dbo.getRecordRightsTableByPerson (#application.com.relayEntity.getEntityType("fileType").entityTypeId#,@personid) as rft on rft.recordid = ft.fileTypeid
				</cfif>
			WHERE 1=1
				<cfif structKeyExists(arguments,"type")>
					and type =  <cf_queryparam value="#arguments.type#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif structKeyExists(arguments,"FileTypeGroupID")> <!--- NJH 2013/09/11 - added the and statement, because we want to bring back the fileTypegroup --->
					<!--- 2015-08-05 DAN K-1314 --->
                    <cfif arguments.FileTypeGroupID is not 0 and arguments.FileTypeGroupID is not "" and arguments.FileTypeGroupID is not "FileTypeGroupID">and ftg.FileTypeGroupID  IN ( <cf_queryparam value="#arguments.FileTypeGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				</cfif>
				<cfif structKeyExists(arguments,"FileTypeID")>
					and ft.FileTypeID =  <cf_queryparam value="#arguments.FileTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
				<cfif structKeyExists(arguments,"fileTypeGroup")and arguments.fileTypeGroup is not "">
					and ftg.heading =  <cf_queryparam value="#arguments.fileTypeGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

				<!--- NJH 2013/09/11 2013 R2 - only show fileTypes that one has rights to... sometimes we need to include a fileType you don't have rights to so that we don't lose it in the dropdown --->
				<cfif structKeyExists(arguments,"restrictToPermissionLevel")>
					and
					<cfif arguments.restrictToPermissionLevel eq 2>
						case <cfif structKeyExists(arguments,"showFileTypeID") and isNumeric(arguments.showFileTypeID)>when ft.fileTypeID = <cf_queryparam value="#arguments.showFileTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > then 1</cfif>
						 when (ft.hasRecordRightsBitMask & 2)/2 = 1
						 	then rft.level2
						 when (ftg.hasRecordRightsBitMask & 2)/2 = 1
						 	then rftg.level2
						 else 1 end = 1
					<cfelseif arguments.restrictToPermissionLevel eq 1>
						(ftg.hasRecordRightsBitMask & 1 = 0 OR rftg.level1 = 1)
							AND
						(ft.hasRecordRightsBitMask & 1 = 0 OR rft.level1 = 1)
					</cfif>
				</cfif>
			order by #arguments.sortOrder#
		</cfquery>

		<cfreturn getFileTypesQry>
	</cffunction>

	<!--- METHOD FOR DELETING FILES WHILE STEPPING AROUND THE IIS LOCKING ISSUE. --->
	<cffunction access="public" name="deleteFile" hint="deletes a file (manages IIS locking issue)">
		<cfargument name="fullFilePath">

		<cfset var tmpFileName = '' />
		<cfset var result = false />
		<cfset var errorStruct = structNew() />

		<cfif fileExists(fullFilePath)>
			<cfset tmpFileName = "#listDeleteAt(fullFilePath,listLen(fullFilePath,"\"),"\")#\#listFirst(listLast(fullFilePath,"\"),".")#_ForDelete.#listLast(listLast(fullFilePath,"\"),".")#">

			<!--- PROD2016-3174 - workaround fix for failing to rename and delete the file due to locks --->
			<cfset var tryUntilTime = getTickCount() + 10000 />
			<cfloop condition="getTickCount() LT tryUntilTime">
				<cftry>
					<cffile action="rename" source="#fullFilePath#" destination="#tmpFileName#" nameconflict="overwrite">
					<cfif fileExists(tmpFileName)>
						<cffile action="delete" file="#tmpFileName#">
						<cfset result = true> <!--- file deleted --->
					</cfif>
					<cfset tryUntilTime = 0 />
					<cfcatch>
						<cfset errorStruct = cfcatch>
						<cfset Sleep(500) />
					</cfcatch>
				</cftry>
			</cfloop>
		</cfif>

		<cfif not result and not structIsEmpty(errorStruct)>
			<cfset application.com.errorHandler.recordRelayError_Warning(type="Delete File",Severity="error",catch=errorStruct,WarningStructure=arguments)>
		</cfif>
		

		<cfreturn result>
	</cffunction>

	<!--- WAB 2009/09/14 from fileListV3.cfm
		deleteFile function already existed, so this has a longer name
	--->

	<cffunction access="public" name="deleteFileFromFileLibrary" hint="Deletes a file from File Library">
		<cfargument name="fileID" type="numeric">

		<cfset var result = {isOK="true",message=""}>
		<cfset var getFilePath = "">
		<cfset var useAbsPath = '' />
		<cfset var thisFile = '' />
		<cfset var thisImageFile = '' />
		<cfset var deleteFileQry = '' />
		<cfset var hasRightsToFile = getFilesAPersonHasEditRightsTo(personID=request.relayCurrentUser.personID,fileID=arguments.fileID)>

		<cfif hasRightsToFile.recordCount>
			<CFQUERY NAME="getFilePath" DATASOURCE="#application.SiteDataSource#">
				SELECT Path,fileName,secure,autounzip     <!--- WAb 2009/09/14 added autounzip --->
				FROM FileType inner JOIN files on files.fileTypeID = FileType.FileTypeID
				WHERE files.fileID = #arguments.fileID#
			</CFQUERY>

			<cfif getFilePath.secure is 1>
				<cfset useAbsPath = application.paths.secureContent>
			<cfelse>
				<cfset useAbsPath = application.paths.content>
			</cfif>

			<CFSET thisFile = "#useAbsPath#\#getFilePath.path#\#getFilePath.FileName#">
			<cfset thisImageFile = "#application.paths.content#\linkImages\files\thumb_#arguments.fileID#.png">

			<CFTRY>
				<cfif fileExists (thisFile)>
					<cfset deleteFile(thisFile)>
				</cfif>
				<cfif fileExists (thisImageFile)>
					<cfset  deleteFile(thisImageFile)>
				</cfif>

				<!--- NJH 2008/09/18 elearning 8.1  - a zipped file will have it's contents in a subdirectory. Delete this directory if it exists --->
				<cfif getFilePath.autounzip and directoryExists("#application.paths.content#\#getFilePath.path#\#arguments.FileID#")>
					<cfdirectory action="delete" directory="#application.paths.content#\#getFilePath.path#\#arguments.FileID#" recurse="true">
				</cfif>

				<!--- <cffile action="DELETE" file="thisFile"> --->
				<cfcatch type="any">
					<cfset result.isOK = false>
					<CFSET result.message = "#cfcatch.message#. ">
				</CFCATCH>
			</CFTRY>

			<!--- DXC 2015/08/03 START force update of lastUpdatedBy and lastUpdated for audit --->
			<cfquery name="deleteFileQry">
				update files set
					lastUpdated=getDate(),
					lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
					lastUpdatedByPerson = #request.relayCurrentUser.personID#
				where fileID =  <cf_queryparam value="#arguments.fileID#" CFSQLTYPE="CF_SQL_INTEGER" >

				delete from files where fileID =  <cf_queryparam value="#arguments.fileID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		<cfelse>
			<cfset result.isOk = false>
			<CFSET result.message = "Insufficient Rights.">
		</cfif>

		<cfreturn result>

	</cffunction>

	<!--- METHOD FOR RETRIEVING SECURED FILES.
	WAB 2009.05/27 altered so that always brings back a structure 	was getSecuredFile
NJH 2009/07/14 P-FNL069 deal with secure related files
	SSS 2009-08-21  deal with clusteredabsolutepath
	WAB 2009/09/07  Altered to use getTemporaryFolderDetails()
	WAB 2009/09/09  LID 2512 Added support for Autounzip column on the fileType table (changed some of SSS work)
			Added caching of details of files already served up in session
	--->

	<cffunction access="public" name="getSecuredFile" hint="retrieves details of a secured file for a user to download">
		<cfargument name="fileID" type="numeric">
		<cfargument name="fileLibraryName" type="string" default="FileLibrary"> <!--- NJH 2009/07/15 P-FNL069 --->
		<cfargument name="fileName" type="string" required="false"> <!--- SSS if it is a zip file use this file name --->

		<cfset var fileDetail = "">
		<cfset var securePathToFile = "">
		<cfset var result = {isOK=true,absoluteURL="",relativeURL=""}>
		<cfset var TemporaryFolderdetails = "">
		<cfset var fileIdentifier = '' />
		<cfset var zipFile = '' />
		<cfset var securePathToSubDirectory = '' />


		<cfif arguments.fileLibraryName eq "FileLibrary">
			<cfquery name="fileDetail" datasource="#application.sitedatasource#">
					select	f.fileid, f.filename, ft.path, autounzip, revision as lastupdated, f.url, ft.addWatermark
				from	files f		inner join
						filetype ft	on f.fileTypeID = ft.fileTypeID
				where	fileID = #arguments.fileID#
			</cfquery>

		<!--- NJH 2009/07/14 P-FNL069 - if the file doesn't exist in files table, check if it's a related file --->
		<cfelse>
			<cfquery name="fileDetail" datasource="#application.sitedatasource#">
				select rf.fileID, rf.filename, rfc.filePath as path, uploaded as lastupdated, 0 as autounzip, null as url, 0 as addWatermark
				from relatedFile rf inner join relatedFileCategory rfc
					on rf.fileCategoryID = rfc.fileCategoryID
				where
					rf.fileID = #arguments.fileID#
			</cfquery>
		</cfif>

		<cfif fileDetail.recordCount gt 0>

			<cfif fileDetail.filename neq "" and fileDetail.url eq "">
				<!--- So that multiple requests to the same file within a single session don't generate separate temporary files, we keep track of requests in a session variable --->

				<cfparam name="session.securedFileLocations" default="#structNew()#">
				<cfset fileIdentifier = "#fileLibraryName#_#fileID#">

				<!--- check if already accessed within this session (and not updated in the meantime) --->
				<cfif structKeyExists(session.securedFileLocations,fileIdentifier) and directoryExists (session.securedFileLocations[fileIdentifier].path) and session.securedFileLocations[fileIdentifier].lastupdated eq  fileDetail.lastupdated>
					<!--- already downloaded in this session so get the folder location from memory --->
					<cfset TemporaryFolderdetails = session.securedFileLocations[fileIdentifier]>
					<cfset result.isOK = true>
				<cfelse>
					<!--- first download in this session so create a temporary folder --->
					<cfset TemporaryFolderdetails = getTemporaryFolderDetails(uniqueFolderName = fileIdentifier)>

					<!--- check that required file exists --->
					<cfset securePathToFile = "#application.paths.secureContent#\#fileDetail.path#\#fileDetail.fileName#">
					<cfif fileExists(securePathToFile)>
						<cffile action="copy" source="#securePathToFile#" destination="#TemporaryFolderdetails.path#">

						<cfif fileDetail.addWatermark>
							<cfset applyWaterMark	(filePath = TemporaryFolderdetails.path & "\" & fileDetail.fileName, Text = getWaterMarkText(fileDetail = fileDetail))>
						</cfif>

					<!--- LHID 2512 sss 2009/08/20 if the file we are coping to the temp directory is a zip file we should extract it so that it can be worked with--->

						<cfif fileDetail.autounzip and findnocase(".zip", fileDetail.fileName) NEQ 0>
							<!--- WAB 2009/09/16 have now decided to store them in an unzipped state
							<cfset zipFile=unzipFile(fileAndPath="#securePathToFile#",destination="#TemporaryFolderdetails.path#",overwrite="true", zipfileID = arguments.fileID)>
							<cfif not zipFile.isOK>
								<cfoutput>#zipFile.message#</cfoutput><CF_ABORT>
							</cfif>
							--->
							<cfset securePathToSubDirectory = "#application.paths.secureContent#\#fileDetail.path#\#fileDetail.fileid#">
							<cfset copyDirectory (source="#securePathToSubDirectory#", destination="#TemporaryFolderdetails.path#", recurse = true)>
						</cfif>

						<cfset result.isOK = true>

						<!--- save tempfolder details in session for future use--->
						<cfset session.securedFileLocations[fileIdentifier] = TemporaryFolderdetails>
						<cfset session.securedFileLocations[fileIdentifier].lastupdated = fileDetail.lastupdated>
					<cfelse>
						<cfset result.Message = "The secured File does not exist.">
						<cfset result.isOK = false>
					</cfif>
				</cfif>
			</cfif>

		<cfelse>
			<cfset result.Message = "The fileID provided is invalid.">
			<cfset result.isOK = false>
		</cfif>


		<cfif result.isOK>
			<cfif structkeyExists(arguments,"fileName")>
				<cfset result.absoluteURL = TemporaryFolderdetails.path  & arguments.fileName>
	            <cfset result.relativeURL = TemporaryFolderdetails.webpath & URLEncodeFilePath(arguments.fileName)>

	        <!--- NJH 2014/09/24 CASE 442176 a file that is a url - shouldn't be secured, but handling the case when it is --->
			<!--- GCC 2015/07/17 CASE 445287 Some https URLs break flieexists checks down this line e.g. https://x.y.z.com/files/0/f/3478344242/1/f_29404488333 so return a valid absolutepath instead that does not exist--->
	        <cfelseif fileDetail.url neq "" and fileDetail.fileName eq "">
	        	<cfset result.absoluteURL = "c:\IXdoXnotXexistEVER.doc">
	            <cfset result.relativeURL = fileDetail.url>
			<cfelse>
				<cfset result.absoluteURL = TemporaryFolderdetails.path  & fileDetail.fileName>
	            <cfset result.relativeURL = TemporaryFolderdetails.webpath  & URLEncodeFilePath(fileDetail.fileName)>
			</cfif>
		</cfif>
		<cfreturn result>
</cffunction>


<!--- 2016-03-01 	WAB		PROD2016-509 Various watermarking functions --->
<cffunction name="getWaterMarkText" hint="gets WaterMark Text.  Override this function if required. File detail query provided in case it is helpful! ">
	<cfargument name="fileDetail">

	<cfreturn formatWaterMarkText(request.relayCurrentUser.person.email)>

</cffunction>


<cffunction name="formatWaterMarkText" hint="format WaterMark Text. Override this function if you want to add some css">
	<cfargument name="watermarkText" required="true">

	<cfreturn watermarkText>

</cffunction>


<cffunction name="applyWaterMark" hint="Apply Watermark to file.  Super this function if you want to customise size etc., all arguments are passed through to cfpdf even if not cfargument'ed">
	<cfargument name="filePath" required="true">
	<cfargument name="text" required="true">
	<cfargument name="opacity" default="2">
	<cfargument name="rotation" default="60">
	<cfargument name="height" default="50">

	<cfif listLast (filePath, ".") is "pdf">
		<cfset arguments.source = arguments.filepath>
		<cfset arguments.destination = arguments.filepath>
		<cfset structDelete (arguments,"filePath")>

		<cfpdf
			action="addWatermark"
		    overwrite="yes"
			attributeCollection = #arguments#
			showonprint = "true"
		    >

		<!--- Apply a random password to prevent the watermark being removed --->
		<cfpdf
			action="protect"
			source= #arguments.source#
    		permissions="AllowPrinting,AllowScreenReaders,AllowDegradedPrinting"
    		newownerpassword= #createUUID()#
		>

	</cfif>

</cffunction>

<cffunction name="URLEncodeFilePath" hint="Does a urlEncodedFormat but leaves / alone, needed because sometimes our filenames have a path in them as well">
	<cfargument name="string" required="true">
	<cfset var result = replacenocase(replacenocase(urlEncodedFormat(string),'%2F','/','ALL'),'%5C','\','ALL')>
	<cfreturn result>
</cffunction>
	<!---
		SSS 2009/08/20 LHID 2512 changed the way temp directories are handled so they are deleted when the session ends
		WAB 2009/09/07  Altered to use getTemporaryFolderDetails()
		Clears up any files from sessions which are no longer active - ie belt and braces
		WAB 2011/09/05 Added clean up of NotSSL directory
	--->
	<cffunction access="public" name="tempSessionFileCleanup" hint="Cleans up any file where the session has expired">
		<cfargument name="applicationScope" default="#application#">

		<cfset var tempDirectoryPath = "">
		<cfset var listOfActiveSessions = "">
		<cfset var tempDirectories = "">
		<cfset var dummySessionScope = {sessionid="dummy"}>  <!--- when this function is called by housekeeping we do not have a session scope, but we call a function that expects a sessionScope variable, we  actually need something from the function which is unrelated to session so I am just going to pass a dummy scope in --->
		<cfset var notSSL = "">
		<cfset var TemporaryFolderdetails = "">

		<cfloop list="1,0" index="notSSL">

			<cfset TemporaryFolderdetails = getTemporaryFolderDetails(createFolder = false, applicationScope = applicationScope, sessionScope = dummySessionScope,notSSL = notSSL)>

			<cfdirectory action="list" directory="#TemporaryFolderdetails.instancePath#" name="tempDirectories">

			<cfloop query="tempDirectories">
				<!--- WAB 2013-03-27 CASE 434530 housekeeping task failing - add applicationScope --->
				<cfset sessionResult = applicationScope.com.security.getSessionIDFromHash(sessionToken = name, applicationScope=applicationScope)>  <!--- this function retrieves the sessionid from the session hash.  If the session is no longer active then it will return .isOK = false --->
				<cfif not sessionResult.isOK>
					<!--- NJH 2009/12/02 LID 2895- put a try/catch if error occurs when attempting to delete directory linked to session that has expired. --->
					<cftry>
					<cfdirectory action="delete" directory="#directory#\#name#" recurse="yes">
						<cfcatch>
							<cfmail to="#applicationScope.errorsEmailAddress#" from="#applicationScope.errorsEmailAddress#" subject="Error attempting to delete temp dir" type="html">
								Attempting to delete folder: #directory#\#name#<br>
								Domain: #request.currentSite.domain#<br>
								<cfdump var="#cfcatch#">
							</cfmail>
						</cfcatch>
					</cftry>
				</cfif>
			</cfloop>
		</cfloop>


	</cffunction>

	<!--- WAB 2010/07/15
		Deletes temporary files for the current session
		Usually used in onSessionEnd
		WAB 2011/11/08 replaced cfmail in catch with recordRelayError_Warning()
	 --->
	<cffunction access="public" name="tempFileCleanupForCurrentSession" hint="Cleans up temporaryFiles for a specific session - can be called at log out">
		<cfargument name="sessionScope" default="#session#">
		<cfargument name="applicationScope" default="#application#">

		<cfset var TemporaryFolderdetails = "">
		<cfset var NotSSL = "">
		<cfloop list="1,0" index="notSSL">
			<cfset TemporaryFolderdetails = getTemporaryFolderDetails(createFolder = false, sessionScope = arguments.sessionScope, applicationScope = arguments.applicationScope,notSSL = notSSL)>
		 	<!--- NJH 2009/10/02 LID 2731- put a try/catch as error occurs when logging out if can't delete directory for some reason. --->

			<cftry>
				<cfif directoryExists(TemporaryFolderdetails.sessionpath)>
					<cfdirectory action="delete" directory="#TemporaryFolderdetails.sessionpath#" recurse="yes">
				</cfif>
				<cfcatch>
					 <cfset arguments.applicationScope.com.errorhandler.recordRelayError_Warning (type="File Housekeeping Error",message="Could not delete directory #TemporaryFolderdetails.sessionpath#. (filemanager.tempFileCleanupForCurrentSession())",catch=cfcatch, TTL=14)>
				</cfcatch>
			</cftry>
		</cfloop>

	</cffunction>



	<!--- WAB 2009/09/07 pulled this code out so that could be used elsewhere - ie for the creation of any on-the-fly temporary files which need accessing --->
	<!--- WAB 2010/07/15
		Changes required so that function can operate when called from onSessionEnd
		Still to be completed
	 --->
	<!--- WAB 2011/09/05
		added .webrootpath because I found a bit of code referring to it in dataTransfers.cfc (I must have managed to lose this change in a previous release) (actually changed from webpathroot to webrootpath)
		added changed .rootPath and changed .temporaryFolderPath to .instancePath to make more consistent, only referred to within this cfc so changed these instances
	--->
	<!--- RMB 2012/02/16 Added check before directory create to "getTemporaryFolderDetails"--->
	<cffunction access="public" name="getTemporaryFolderDetails" hint="creates and returns the name of a temporary folder in which to store a file which needs to be accessed via http only by the current user">
		<cfargument name="createFolder" default="true">
		<cfargument name="sessionScope" default="#session#">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="NotSSL" default="false">		<!--- set to true if it might be necessary to access the file over a non SSL link (mainly if we need to do CFHTTP on it)  --->
		<cfargument name="uniqueFolderName" default="Folder_#getTickCount()#">

		<cfset var result = structNew()>
		<cfset var filesTempRelativePath = "/content/filesTemp">
		<cfset var instanceIdentifier = "#applicationScope.instance.uniqueid#">
		<cfset var sessionIdentifier = "#applicationScope.com.security.getSessionHash(sessionscope = sessionScope,applicationScope = applicationScope)#">
		<cfset var userfilespath = "">

		<cfif notSSL>
			<cfset filesTempRelativePath = filesTempRelativePath & "/notSSL">
		</cfif>

		<!--- LHID 5214 2009-08-21 SSS This is to try and copy files to a set ipaddress in a cluster--->
		<!--- NJH 2010/08/02 8.3 - changed to session from request --->
		<cftry>
			<!--- WAB 2011/09 problem here when called onSessionEnd - it does not appear to be possible to set a session variable, so need a try --->
			<cfparam name="arguments.sessionScope.clusteredAbsolutePath" default="#arguments.applicationScope.paths.userfiles#">
			<cfset userfilespath = sessionScope.clusteredAbsolutePath>
			<cfcatch>
				<cfset userfilespath = arguments.applicationScope.paths.userfiles>
			</cfcatch>
		</cftry>

		<cfset result.rootPath = "#userfilespath##filesTempRelativePath#">
		<cfset result.instancePath = "#result.rootPath#\#instanceIdentifier#">
		<cfset result.sessionPath = "#result.instancePath#\#sessionIdentifier#">
		<cfset result.path = "#result.sessionPath#/#uniqueFolderName#\">

		<!--- NJH 2010/08/02 8.3 - I don't think that the request scope exists when this function is called in onSessionEnd --->
		<cfif isDefined("request") and isStruct(request) and structKeyExists(request,"currentSite")>
			<cfset result.webrootpath = "#request.currentsite.HTTPprotocol##request.currentsite.domain##filesTempRelativePath#">
			<cfset result.webpath = "#result.webrootpath#/#instanceIdentifier#/#sessionIdentifier#/#uniqueFolderName#/">
			<cfset result.webSessionPath = "#result.webrootpath#/#instanceIdentifier#/#sessionIdentifier#">
		</cfif>

		<cfif arguments.createFolder>
		<!--- 2012/02/16 - RMB - Added check before directory creates--->
			<cfif not directoryExists (result.path)>
				<cfdirectory action="create" directory="#result.path#">
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- CLEANS UP FILES TEMP DIRECTORY --->
	<cffunction access="public" name="tempFileCleanup" hint="cleans up downloads from the temp directory">

		<cfset var currentDateTime = now()>
		<cfset var TemporaryFolderdetails  = "">
		<cfset var tempDirectories  = "">
		<cfset var doDelete  = "">
		<cfset var NotSSL  = "">
		<cfset var directoryPath = '' />
		<cfset var directoryFiles = '' />

		<cfloop list="1,0" index="notSSL">

			<cfset TemporaryFolderdetails = getTemporaryFolderDetails(createFolder = false,notSSL = notSSL)>

			<cfdirectory action="list" directory="#TemporaryFolderdetails.instancePath#" name="tempDirectories">

			<cfloop query="tempDirectories">
				<cfif dateDiff("s","#dateLastModified#",currentDateTime) gt 20>  <!--- WAB LID 2237 mod, date was all wrong --->
					<!--- <cftry>
						<cfdirectory action="delete" directory="#directory#\#name#" recurse="yes">
						<cfcatch type="any">file is locked</cfcatch>
					</cftry> --->
					<cfset directoryPath = "#directory#\#name#">
					<cfset doDelete = true>
					<!--- <cftry>

						<cfdirectory action="list" directory="#directoryPath#" name="directoryFiles">
						<cfloop query="directoryFiles">
							<cffile action="delete" file="#directoryPath#\#name#">
						</cfloop>
						<cfcatch><cfset doDelete = false></cfcatch>
					</cftry> --->
					<cfif doDelete>
						<cftry>
						<cfdirectory action="delete" directory="#directory#\#name#" recurse="yes">
						<cfcatch><!--- <cfdirectory action="list" directory="#directoryPath#" name="directoryFiles"><cfdump var="#directoryFiles#"> ---></cfcatch>
						</cftry>
					</cfif>
				</cfif>
			</cfloop>

		</cfloop>

	</cffunction>

	<cffunction access="public" name="manageSecuredFileSecurity" hint="manages file security in application.cfm file used for secured files">
            <cfargument name="scriptName" default = "#cgi.SCRIPT_NAME#">
			<cfset var SecuredFile = "">
			<cfset var fileDetails = '' />

                  <cfset fileDetails = getFileIDFromScriptName (scriptName)>

                        <!--- 2007/05/22 GCC - tweaked to enable secure files with no rights to be accessible by all as long as they are logged in--->
							<!--- 2012-04-24 WAB removed Try/Catch here because was displaying a dump to the end user.  (In this case because of lack of disk space)--->
							<cfif fileDetails.isOK and (request.relaycurrentuser.isUnknownUser OR userCanViewFile(personID=request.relaycurrentuser.personid,fileID=fileDetails.fileID,fileLibraryName=fileDetails.fileLibraryName))>	<!--- 2013-04-11 PPB Case 434536 --->
								<cfif request.relaycurrentuser.isloggedin>	<!--- 2012-09-06 PPB Case 430058 if logged out and secure file redirect to login first --->
                                    <!--- SSS 2009/08/20 LHID 2512 if the file is a zip the actual file needs to be passed in because it is not the zip file we are trying to open--->
                                    <!--- NJH 2009/09/18 - Added fileLibraryName as that needs to get passed in for relatedFiles. --->
                                    <cfif not fileDetails.unzippedFile>
                                          <cfset SecuredFile = getSecuredFile(fileID=fileDetails.fileID,fileLibraryName=fileDetails.fileLibraryName)>
                                    <cfelse>
                                          <cfset SecuredFile = getSecuredFile(fileID=fileDetails.fileID,fileName=fileDetails.fileName,fileLibraryName=fileDetails.fileLibraryName)>
                                    </cfif>

                                    <cfif SecuredFile.isOK>
                                          <!--- This cleans up all old temp files , it may need to be in a separate housekeeping process, although it seems to be fairly quick--->
                                          <!--- <cfset tempFileCleanup ()> ---> <!--- SSS 2009-08-21 took this out files are now deleted on a schedule and session existsing --->
                                          <cflocation url="#SecuredFile.relativeURL#" addtoken=false> <!---  WAB 2009/09/09 added addtoken--->
                                    <cfelse>
                                          <cfoutput>#SecuredFile.Message#</cfoutput>
                                    </cfif>
								<cfelse>
									<!--- 2012-09-06 PPB Case 430058 if logged out and secure file redirect to login first --->
									<cflocation url="#application.com.security.encryptURL('/index.cfm?urlrequested=displayfile=true%26fileId=#fileDetails.fileID#')#" addtoken="false">
								</cfif>
                        <cfelse>
                              <cfoutput>Sorry, but you may not access this file.</cfoutput>
                        </cfif>

      </cffunction>


      <!---
            WAB 2010/04/12 split this function out from manageSecuredFileSecurity so that it could be used in the missing template handler
            WAB 2015-01-19 CASE 443444 The lCase() inside of cfqueryparam inside of cfquery was causing double encoding of apostrophes
      --->
	<cffunction access="public" name="getFileIDFromScriptName" hint="Returns the fileID and file library name, given a scriptName/File path ">
		<cfargument name="scriptName" default = "#cgi.SCRIPT_NAME#">

		<cfset var result = {isOK = true, fileID = 0, fileLibraryName="FileLibrary",unzippedFile=false}>
		<cfset var fileName = "">
		<cfset var fileTrueName = "">
		<cfset var fullFileTypePath = "">
		<cfset var fileTypePath = "">
		<cfset var qFileData = "">
		<cfset var fileLibraryName = "FileLibrary">
		<cfset var reFindFileIDResult = '' />
		<cfset var fileID = '' />


		<!--- Need to strip off the \content bit and pull out the path and the file name--->
		<cfset fileName = listLast(scriptName,"\/")>
		<cfif listLen(listLast(scriptName,"/"),".") gt 2 and listLast(listLast(scriptName,"/"),".") is "cfm"> <!--- if this is a stub file of the form   myFile.doc.cfm--->
			<cfset fileTrueName = listDeleteAt(fileName,listLen(fileName,"."),".")>
		<cfelse>
			<cfset fileTrueName = listLast(scriptName,"/")>
		</cfif>

		<cfset fullFileTypePath = reverse(listRest(reverse(scriptName),"/"))>
		<cfset fileTypePath = reReplaceNoCase(fullFileTypePath,"\A[/]*content/","")>
		<cfset result.fileName = fileTrueName>
        <cfset result.fileTypePath = fileTypePath>

		<!---
		Debug
		<cfoutput>
			scriptname : #scriptname# <BR>
			filename : #filename# <BR>
			filetruename : #fileTrueName# <BR>
			fullFileTypePath : #filename# <BR>
			fileTypePath : #fileTypePath# <BR>
		</cfoutput>
		--->

		<!--- First check for the file in the regular file library --->
		<cfquery name="qFileData" datasource="#application.sitedatasource#">
		select
				f.fileid
          	from	files f
					inner join
				filetype ft on f.filetypeid = ft.filetypeid
		where
				f.fileName =  <cf_queryparam value="#fileTrueName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and ft.path =  <cf_queryparam value="#fileTypePath#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif qFileData.recordCount neq 0>
			<cfset result.fileID = qFileData.fileID>
			<cfreturn result>
		</cfif>


		<!---
			Now check whether the file is part of a zipped up file
			check for path of form    xxxx/yyyy/#fileid#/wwww/zzzz.aaa
			if matches this path then we are accessing a file within a zip file, so need to check for rights to that file
		 --->

		<cfset groups= {1="TypePath",2="fileID",3="subpath"}>
		<cfset reFindFileIDResult = application.com.regexp.refindAllOccurrences('(.*)\/([0-9]+)(?:\/(.*))?',fileTypePath,groups)>
		<cfif arrayLen(reFindFileIDResult) is not 0>
			<cfset fileID = reFindFileIDResult[1].fileID>
			<cfif reFindFileIDResult[1].subpath is not "">
				<cfset result.filename = reFindFileIDResult[1].subpath & "/" & result.filename>
			</cfif>

			<!--- now check that the fileID is a zip file --->
			<cfquery name="qFileData" datasource="#application.siteDataSource#">
			select fileID from files where fileID =  <cf_queryparam value="#fileID#" CFSQLTYPE="CF_SQL_INTEGER" >  and filename like N'%.zip'
			</cfquery>
		</cfif>


        <cfif qFileData.recordCount neq 0>
        	<cfset result.unzippedFile = true>
            <cfset result.fileID = qFileData.fileID>

            <cfreturn result>
		</cfif>


		<!--- Finally check whether it is a related file. --->
		<cfquery name="qFileData" datasource="#application.siteDataSource#">
		select rf.fileID
		from
				relatedFile rf
					inner join
				relatedFileCategory rfc on rf.fileCategoryID = rfc.fileCategoryID
		where
			rf.filename =  <cf_queryparam value="#fileTrueName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
			replace(rfc.filePath,'\','/') =  <cf_queryparam value="#fileTypePath#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif qFileData.recordCount neq 0>
		      <cfset result.fileLibraryName = "RelatedFile">
		      <cfset result.fileID = qFileData.fileID>
		<cfelse>
		      <cfset result.isOK = false>
		</cfif>

		<cfreturn result>

	</cffunction>




	<!---
	************************
	Various functions for creating/moving secure files
	WAB 2009/05/28
	WAB 2011/11/14 Removed many of the functions (and renamed others)  [createSecureFile,removeSecureFile,createStubsForType]
				Much of the code was redundant in 8.3 (secure file dealt with differently)
				Really just left with functions which move files from secure to insecure location (or vice versa) if the file type is changed from secure to insecure
				No longer have to deal with "stub" files
				No longer need to deal with unzipping auto-unzip files [this is done in fileTask]- just need to move their subdirectory


	************************
	--->

	<cffunction name="checkFileInCorrectLocationSecureOrNot">
		<!---
		Checks that files are in the correct location (based on setting of file.secure)
 		This Function is called from templates\relayRecordManager_fileType.cfm (value of secure might have been changed in the front end)
		 --->
		<cfargument name="fileID" type="numeric">
		<cfargument name="fileTypeID" type="numeric">

		<cfset var getFileDetails = "">

			<cfquery name="getFileDetails" datasource="#application.sitedatasource#">
				select	isnull(secure,0) as secure,f.fileid, path, filename, autounzip
				from	files f inner join
						filetype ft on f.filetypeid = ft.filetypeid
				where
					filename is not null <!--- Case 450959 --->
					<cfif structKeyExists(arguments,"fileID")>and f.fileid = #fileid#</cfif>
					<cfif structKeyExists(arguments,"fileTypeID")>and f.fileTypeid = #fileTypeid#</cfif>
				order by Path
			</cfquery>

			<cfloop query="getFileDetails" >
				<cfset moveFileToOrFromSecure (toSecure = secure,filename = fileName, path =  path, autounzip = autounzip, fileid = fileid)>
			</cfloop>


	</cffunction>


	<cffunction name="moveFileToOrFromSecure">
		<cfargument name="toSecure" default="true">
		<cfargument name="fileName">
		<cfargument name="Path">
		<cfargument name="autounzip" type="boolean" default = "false">
		<cfargument name="fileid" default="">  <!--- needed for auto unzip--->

		<cfset fromPath = application.paths["#iif(toSecure,de(''),de('secure'))#Content"] & "\"  & Path>
		<cfset toPath = application.paths["#iif(toSecure,de('secure'),de(''))#Content"] & "\"  & Path>

		<!--- Check for file in the source directory --->
		<cfdirectory action="LIST" directory="#fromPath#" name="checkSourceFile" filter="#fileName#">

		<cfif checkSourceFile.recordcount>
			<!--- If file exists, move to other directory--->
			<cfif not directoryExists(toPath)>
				<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=toPath)>
			</cfif>
			<cffile action="MOVE" source="#fromPath#/#fileName#" destination="#toPath#">
		</cfif>

		<cfif autounzip and listlast(filename,".") is "zip" and fileid is not "">
			<!--- Need to move subdirectory if it exists --->
			<cfif directoryExists(fromPath & "\" & fileid)>
				<cfset moveDirectory(source = fromPath & "\" & fileid,destination = toPath & "\" & fileid ) >
			</cfif>
		</cfif>

	</cffunction>


	<!--- CHECKS IF USER HAS RIGHTS TO FILE --->

	<cffunction access="public" name="userCanViewFile" hint="checks if a user is allowed to view a file">
		<cfargument name="personid" required="yes">
		<cfargument name="fileid" required="yes">
		<cfargument name="fileLibraryName" type="string" default="FileLibrary"> <!--- NJH 2009/07/15 P-FNL069 --->

		<cfset var files = "">

		<cfif arguments.fileLibraryName eq "FileLibrary">
			<cfset files = getFilesAPersonHasCountryViewRightsTo(personid=arguments.personid,fileid=arguments.fileid)>

			<cfif files.recordcount gt 0>
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>
		<cfelseif arguments.fileLibraryName eq "RelatedFile">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>

	<cffunction name="getFileDetailsFromPath" access="public" returntype="struct">
		<cfargument name="filePath" type="string" required="true">

		<cfreturn application.com.regExp.getFileDetailsFromPath(inputString=arguments.filePath)>
	</cffunction>

	<!--- UNFINISHED METHOD --->
	<!--- NJH 2009/07/13 P-FNL069 - wrote uploadFile method to check file extension of uploaded file. It uploads the file to a temp dir. If the extension is valid,
		it is uploaded to the intended directory; otherwise it is deleted.
		NJH 2011/01/10 LID 4963 - added a few more arguments for the upload file.
		WAB 2011/04/19 LID 6316 - made maxSize a setting (and renamed)
		--->
	<cffunction access="public" name="uploadFile" hint="Uploads a specified file and checks that the file is valid." returntype="struct">
		<cfargument name="fileField" type="string" hint="Form field">
		<cfargument name="destination" type="string">
		<cfargument name="nameConflict" type="string" default="makeUnique">
		<cfargument name="maxSize_MB" type="numeric" default="#application.com.settings.getSetting('files.maxUploadSize_MB')#" hint="Maximum size of file in MB"> <!--- WAB 2011/04/19 LID 6316 changed to a setting, also changed from KB to MB (no sign of this argument actually being passed in anywhere)--->
		<cfargument name="renameFileTo" type="string" required="false" hint="Includes the file extension">
		<cfargument name="accept" type="string" default="" hint="What type of files to accept. It is used in conjuction with allowed file extension">

		<cfscript>
			var result = structNew();
			var fileUpload = "";
			var dotPos = "";
			var fileExtension = "";
			var uploadedFileExtension = "";
			var invalidFileType = "";
			var maxSize_bytes = arguments.maxSize_MB * 1024 * 1024;
			var filename = "";
			var fileDetails = getFileDetailsFromPath(filePath=arguments.fileField);
			var destinationPath = arguments.destination;
			var fileNameAndPath = "";
			var testFileUpload = "";

			result.isOk = false;
			result.message = "";
		</cfscript>


		<cfif structKeyExists(form,arguments.fileField) and form[arguments.fileField] neq "">
			<cftry>
				<cffile action="upload" fileField="#arguments.fileField#" destination="#application.paths.temp#" nameconflict="makeUnique" result="testFileUpload">
				<!--- NJH/SSS 2009/08/17 it appears that serverFileExt can be empty sometimes.. haven't figured out why, but use the client extension if it is found to be empty. --->
				<cfset uploadedFileExtension = testFileUpload.serverFileExt>
				<cfif testFileUpload.serverFileExt eq "">
					<cfset uploadedFileExtension = testFileUpload.clientFileExt>
				</cfif>

				<cfset filename = testFileUpload.serverFile>
				<cfset fileNameAndPath = destinationPath&"\"&filename>

				<cfif (structKeyExists(arguments,"renameFileTo") and arguments.renameFileTo neq "")>
					<cfset fileNameAndPath = destinationPath&"\"&arguments.renameFileTo>
				</cfif>

				<cfif testFileUpload.filesize gt maxSize_bytes>
					<cfset result.isOk = false>
					<cfset result.message = "File exceeds maximum size of #arguments.maxSize_MB#MB.">

				<cfelseif listFindNoCase(application.allowedFileExtensions,uploadedFileExtension)>

					<!--- if the file to be uploaded is a zip file, ensure that it contains only valid files --->
					<cfif uploadedFileExtension eq "zip">

						<!--- 2016/05/09 448379 - delete __MACOSX directory  /.DS_Store files if they exist --->
						<cfzip action="delete" file="#testFileUpload.serverDirectory#\#testFileUpload.serverFile#" >
							<cfzipparam entrypath="__MACOSX" />
							<cfzipparam filter=".DS_Store" />
						</cfzip>

						<!--- list the files and determine that they are all valid before unzipping them --->
						<cfzip action="list" file="#testFileUpload.serverDirectory#\#testFileUpload.serverFile#" name="zipFiles" recurse="yes">
						<cfloop query="zipFiles">
							<!--- WAB 2013-06-23 don't test files without an extension --->
							<cfif type eq "file" and listLen(name,".") GT 1 >   <!--- every type should be file as we have set recurse to true --->
								<!---
								WAB 2010/09/14 Crap code assumed that file extension had 3 characters, replace with list last
								<cfset dotPos = findNoCase(".",name,len(name)-5)>
								<cfset fileExtension = right(name,len(name)-dotPos)> --->
								<cfset fileExtension = listlast (name,".")>
								<cfif not listFindNoCase(application.allowedFileExtensions,fileExtension)>
									<cfset invalidFileType = fileExtension>
									<cfbreak>
								</cfif>
							</cfif>
						</cfloop>

					<!--- NJH 2011/10/19 bit of an issue with IE and jpgs that are actually pjpegs.. For some reason, when uploading such an image,the server rejects it. Setting the mime type
						in IIS didn't seem to solve the problem, but adding the "image/pjpeg" mime type in the accept attribute works. --->
					<cfelseif uploadedFileExtension eq "jpg" and structKeyExists(testFileUpload,"contentSubType") and testFileUpload.contentSubType eq "pjpeg">
						<cfset arguments.accept = listAppend(arguments.accept,"image/pjpeg")>
					</cfif>

					<cfif invalidFileType is "">
						<cfif not directoryExists("#arguments.destination#")>
							<cfset application.com.globalFunctions.CreateDirectoryRecursive(arguments.destination)>
						</cfif>
						<!--- 2016-01-29  ESZ   case 446338 File error, rename file '+' on 'plus' --->
						<cfif findnocase("+",fileName) gt 0>
							<cfset filename = replace(testFileUpload.serverFile,"+","_","ALL")>
							<cfset fileNameAndPath = destinationPath&"\"&filename>
							<cfset plussignerrormessageMessage = "phr_plussignerrormessage #testFileUpload.serverFile# -> #filename#">
							<cfoutput>
                                <div>#application.com.relayUI.message(message = plussignerrormessageMessage, type="warning")#</div>
							</cfoutput>
						</cfif>

						<!--- upload the file rather than move it so that we can get the response structure, which we then return --->
						<cffile action="upload" fileField="#arguments.fileField#" destination="#fileNameAndPath#" nameconflict="#arguments.nameConflict#" result="fileUpload" accept="#arguments.accept#">

						<!--- 2016/05/09 448379 - delete __MACOSX directory if it exists --->
						<cfif uploadedFileExtension eq "zip">
							<cfzip action="delete" file="#fileNameAndPath#" >
								<cfzipparam entrypath="__MACOSX" />
								<cfzipparam filter=".DS_Store" />
							</cfzip>
						</cfif>

						<!--- 2015-11-11  ACPK    PROD2015-333 Replace all plus signs in clientfile/clientfilename values with "plus" strings --->
                        <cfif findnocase("+",fileUpload.clientfile) gt 0>
                            <cfset fileUpload.clientfile=replace(fileUpload.clientfile,"+","plus","ALL")>
                            <cfset fileUpload.clientfilename=replace(fileUpload.clientfilename,"+","plus","ALL")>
                        </cfif>

						<cfset result.isOk = true>
						<cfset result.message = "File uploaded successfully.">
						<cfset structAppend(result,fileUpload)>

					<cfelse>
						<cfset result.isOk = false>
						<cfset result.message = "Zip file contains file of type #invalidFileType# which is not permitted. <BR>Permitted files are of type: #application.allowedFileExtensions#">
					</cfif>
				<cfelse>
					<cfset result.isOk = false>
					<cfset result.message = "File type #uploadedFileExtension# not permitted. <BR>Permitted files are of type: #application.allowedFileExtensions#">
				</cfif>

				<cffile action="delete" file="#testFileUpload.serverDirectory#\#testFileUpload.serverFile#">

				<cfcatch>
					<cfset result.isOk = false>
					<cfset result.message = cfcatch.message & cfcatch.detail>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="Uploading File",Severity="error",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfset result.isOk = false>
			<cfset result.message = "Please specify a file to upload.">
		</cfif>
		<cfreturn result>
	</cffunction>


	<!--- NJH 2009/07/13 P-FNL069 wrote file to unzip zip files. We check if the files inside the zip are allowed. If not, the zip is not unzipped. --->
	<cffunction access="public" name="unzipFile" hint="Unzips specified file and checks that the files contained are valid." returntype="struct">
		<cfargument name="fileAndPath" type="string" hint="Filename including path">
		<cfargument name="destination" type="string">
		<cfargument name="overwrite" type="boolean" default="false">

		<cfscript>
			var result = structNew();
			var fileExtension = "";
			var dotPos = "";
			var InvalidFileType = "";

			result.isOk = true;
			result.message = "";
		</cfscript>

		<cftry>
			<!--- list the files and determine that they are all valid before unzipping them --->
			<cfzip action="list" file="#arguments.fileAndPath#" name="zipFiles" recurse="yes">

			<cfloop query="zipFiles">
				<!--- WAB 2013-06-23 don't test files without an extension --->
				<cfif type eq "file" and listLen(name,".") GT 1>   <!--- every type should be file as we have set recurse to true --->
					<!---  WAB 2010/09/14 Crap code assumed that file extension had 3 characters, replace with list last
					<cfset dotPos = findNoCase(".",name,len(name)-5)>
					<cfset fileExtension = right(name,len(name)-dotPos)>
					--->
					<cfset fileExtension = listlast (name,".")>

					<cfif not listFindNoCase(application.allowedFileExtensions,fileExtension)>
						<cfset InvalidFileType = fileExtension>
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>

			<cfif InvalidFileType is "">
				<cfif not directoryExists("#arguments.destination#")>
					<cfset application.com.globalFunctions.CreateDirectoryRecursive(arguments.destination)>
				</cfif>

				<cfzip action="unzip" destination="#arguments.destination#" file="#arguments.fileAndPath#" overwrite="#arguments.overwrite#">

				<cfset result.isOk = true>
				<cfset result.message = "Successfully unzipped files.">
			<cfelse>
				<cfset result.isOk = false>
				<cfset result.message = "Zip File contains a file of type #InvalidFileType# which is not permitted. Permitted files are of type: #application.allowedFileExtensions#">


			</cfif>

			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>


	<!---
	WAB 2005-05-16

	general function for getting alist of files
	this is private so that it can be called by more specific functions within this cfc
	which set the correct filters  (these functionhave to know how this query is structured)

	2009/05/27  LID 2237  WAB added fileNameAndPath to query - this doctors the path for secure files by adding .cfm
				note that filename stays as the original file name (without the .cfm)
	2010/07/14 WAB added a checkFileExists argument and a physicalLocation field to the query
	WAB 2012-02-05 P-MIC001 added support for TOP and a set of joins (and also made sure that the functions calling this function can pass though a filter parameter correctly parameter
	STCR 2012-05-29 P-REL109 - enable users to filter by User Group
	NJH 2012/12/04 Roadmap 2013 Added support for keywords
	NJH 2013/09/11 2013 R2 - re-wrote to take into account new getFiles function which works out all the rights
	 --->
	 <!--- RMB 2011/12/02 - P-REL101 - Added DeliveryMethod to select query GetResults--->
	<cffunction access="private" name="getFileList" hint="" return="query">
		<cfargument name="personid" type="numeric" default = "0">
		<cfargument name="filetypegroupid" type="ANY" default = "0">
		<cfargument name="flagTextIDs" type="ANY" default = "">
		<!--- 2015-08-05 DAN K-1314 --->
		<cfargument name="filetypeid" type="ANY" default = "0">
		<cfargument name="fileid" type="numeric" default = "0">
		<cfargument name="filetype" type="string" default = "">
		<cfargument name="languageid" type="string" default = "">
		<cfargument name="filter" type="string" default = "">
		<cfargument name="having" type="string" default = "">
		<cfargument name="countryFilterList" type="string" default = "">
		<cfargument name="checkExpires" type="numeric" default="0">
		<cfargument name="orderby" type="string" default = "name">
		<cfargument name="returnCountryIDList" type="boolean" default = "false">
		<cfargument name="returnCountryISOList" type="boolean" default = "false">
		<cfargument name="checkFileExists" type="boolean" default="false">
		<cfargument name="uploadedBy" type="numeric"> <!--- NJH 2011/08/19 --->
		<cfargument name="uploadedFromDate" type="date"> <!--- NJH 2011/08/19 --->
		<cfargument name="uploadedToDate" type="date"> <!--- NJH 2011/08/19 --->
		<cfargument name="fileTypeGroup" type="string" default=""> <!--- NJH 2012/02/27 CASE 426874 --->
		<cfargument name="join" type="string" default="">
		<cfargument name="top" type="numeric" >
		<cfargument name="userGroupID" type="string" default="0"><!--- STCR 2012-05-29 P-REL109 - enable users to filter by User Group --->
		<cfargument name="ownerid" type="numeric" default = "0">	<!--- 2012/06/21 PPB Case 428815 add filter for owner --->
		<cfargument name="keywords" type="string" required="false">	<!--- NJH 2012/11/29 2013 Roadmap--->
		<cfargument name="source" type="string" required="false"> <!--- NJH 2012/11/29 2013 Roadmap--->
		<cfargument name="filterByRights" type="boolean" default="false">
		<cfargument name="returnColumns" type="string" default=""> <!--- NJH 2013/09/11 - return specific columns --->
		<cfargument name="returnAdditionalColumns" type="string" default=""> <!--- NJH 2013/09/11 - return any additional columns --->
		<cfargument name="searchString" type="string" default="">
		<cfargument name="searchTables" type="string" default="files,fileFamily,fileType,fileTypeGroup,keywords">
		<cfargument name="returnType" type="string" default="query">

		<cfset var countries = 0>
		<cfset var thisFileExists = 1>
		<cfset var GetResults = "">
		<cfset var filterByCountry="null">
		<cfset var indexedFiles = "">
		<cfset var fileListQry = "">

		<cfif request.relayCurrentUser.countryList neq "">
			<cfset countries = request.relayCurrentUser.countryList>
		</cfif>

		<!--- if it's a single value, it will be numeric.. otherwise it will be a string --->
		<cfif isNumeric(arguments.countryFilterList)>
			<cfset filterByCountry=arguments.countryFilterList>
		</cfif>

		<!--- NJH 2013/10/21 - Roadmap 2013 2 - add ability to search content for files as well --->
		<cfif arguments.searchString neq "">
			<cfset indexedFiles = application.com.search.getIndexedFiles(searchTextString=arguments.searchString)>
		</cfif>


		<cfsavecontent variable="fileListQry">
			<cfoutput>
				select * from (SELECT distinct
				<cfif structKeyExists (arguments,"top")>
					top #arguments.top#
				</cfif>
				<cfif arguments.returnColumns neq "">
					#preserveSingleQuotes(arguments.returnColumns)#
				<cfelse>
				f.fileid, f.fileFamilyID,
				f.name,
				f.filename,
				<!--- 2009/05/27 WAB LID2237    	--->
				case when len(isNull(url,'')) != 0 then url else 'content/' + fT.path + '/' + f.filename end as fileNameAndPath,
				case when isNull(fT.secure,0) = 1 then '#application.paths.secureContent#' else '#application.paths.content#' end + '/' + fT.path + '/' + f.filename as physicalLocation,
				f.revision, f.filesize/1024 as filesize,
				f.publishDate,f.lastUpdated,   <!--- 2015-04-02 PPB Case 444129 allow sorting on extra cols ---> <!--- 2016-01-06 ACPK BF-195 Removed duplicate column name --->
				f.filetypeid, f.sortOrder, f.familySortOrder, ft.filetypegroupid, f.created, fT.path,
				fT.secure, f.personid as OwnerID,P.FIRSTNAME + ' ' + P.LASTNAME AS OwnerName, fT.type,  emailownerOnDownload,
				ftg.intro, ftg.heading,
				hasEditRights AS Level2,
				hasViewRights AS Level1,
				f.hasRecordRightsBitMask & 1 as recordHasSomeLevel1Rights,
				<cfif personid is not 0>
				case WHEN F.PERSONID = #personid# THEN 1 ELSE 0 END
				<cfelse>
				0
				</cfif>
				AS isOwner,
				<cfif arguments.returnCountryIDList>
				(select isNull(dbo.getCountryScopeList (f.fileid,'files',1,0),37)) as countryIDs, <!--- NJH 2013/05/31 - case 435576 - if fn_getCountryRightsV2 returns null, then it's available for all countries..hope this won't break anything!! --->
				</cfif>
				<cfif arguments.returnCountryISOList>
				(select isNull(dbo.getCountryScopeList (f.fileid,'files',1,2),'')) as countryISOs,
				</cfif>
				<cfif checkFileExists>
				null as fileExists,
				</cfif>
			   P.Email, P.EmailStatus,
			   f.languageID, l.language, f.DeliveryMethod,f.url,
			   case when len(isNull(url,'')) != 0 then url else '#request.currentSite.protocolAndDomain#/content/' + fT.path + '/' + dbo.fn_urlencode(f.filename) end as fileURL, <!--- 2015-10-26	ACPK	PROD2015-208 Encode filename in file URLs --->
			   case when len(isNull(url,'')) != 0 then 'URL' else 'File' end as source,f.description,ff.title as fileFamilyName,
			   case when f.triggerType = 1 and f.triggerDate < getDate() then 1 else 0 end as expired
			   </cfif>
			  #preserveSingleQuotes(arguments.returnAdditionalColumns)#
					<cfif arguments.searchString neq "">
						,textsearch.weighting
					</cfif>
		 	<!--- START: 2013-07-02 NYB Case 435893 added with(nolock)'s throughout query: --->
			from getFiles(#personid#,#filterByCountry#,#IIF(arguments.filterByRights,1,0)#) gf
				inner join vfileswithboolean f with(nolock) on gf.fileID = f.fileID
				inner join filetype ft with(nolock) ON ft.filetypeid = f.filetypeid
				inner join filetypegroup ftg with(nolock) ON ft.filetypegroupid = ftg.filetypegroupid
				left join person p	with(nolock) on p.personid = f.personid
				inner join language l with(nolock) on f.languageID = l.languageID
				left join fileFamily ff on ff.fileFamilyID = f.fileFamilyID

		<!--- 2016-02-24 WAB 	PROD2015-291  Visibility Project , changed this filter to support extendedRecordRights
				Note that the UI only allows choise of a single usergroup, whereas it is possible that rights are defined based on membership of multiple usergroups (or by exclusion)
				This code already supports multiple user groups so OK if the UI is changed
		 --->
		<cfif arguments.userGroupID is not 0 and arguments.userGroupID neq ""><!--- STCR 2012-05-29 P-REL109 - enable users to filter by User Group --->
					left join
				dbo.getRecordRightsTableByUserGroup (#application.com.relayEntity.getEntityType("files").entityTypeId#,<cf_queryparam value="#arguments.userGroupID#" cfsqltype="cf_sql_varchar"> ) as rf on rf.recordid = f.fileid
					left join
				dbo.getRecordRightsTableByUserGroup (#application.com.relayEntity.getEntityType("fileTypeGroup").entityTypeId#,<cf_queryparam value="#arguments.userGroupID#" cfsqltype="cf_sql_varchar"> ) as rftg on rftg.recordid = ftg.fileTypeGroupid
					left join
				dbo.getRecordRightsTableByUserGroup (#application.com.relayEntity.getEntityType("fileType").entityTypeId#,<cf_queryparam value="#arguments.userGroupID#" cfsqltype="cf_sql_varchar">) as rft on rft.recordid = ft.fileTypeid
		</cfif>
		<cfif structKeyExists(arguments,"keywords") and arguments.keywords neq "" or findNoCase("t.data",arguments.filter) or (arguments.searchString neq "" and listFindNoCase(arguments.searchTables,"keywords"))>
			left join (textMultipleFlagData t
				inner join flag on flag.flagID = t.flagID
				inner join flagGroup fg on fg.flagGroupID = flag.flagGroupID and fg.flagGroupTextID = 'filesKeywords' and fg.entityTypeID =  <cf_queryparam value="#application.entityTypeID.files#" CFSQLTYPE="CF_SQL_INTEGER" > ) on f.fileID = t.entityID
		</cfif>
		<cfif join is not "">
			#join#
		</cfif>
		<cfif arguments.searchString neq "">
			inner join (
				<!--- YMA create a derived table of all the matches to the search string.  the search string is compared against many different potential match areas and
				 a weighting build up.  The more matches, the heighe rthe weighting and the higher up the search results they will appear. --->
				Select fileid, SUM(weighting) as weighting from (
				<cfset var firstPass = true>
				<cfif findnocase('"',DecodeforHTML(arguments.searchstring))>
					select f.FileID, 1.5 as weighting from files as f where
					CONTAINS(f.name,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
					UNION ALL
					select f.FileID, 1 as weighting from files as f where
					CONTAINS(f.*,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
				<cfelse>
					<!--- YMA instead of passing the full search string into the full text search contains method we are instead breaking each word and searching for it seperately.
								This is because we want to see how many parts of the search string are found accross the file name and all other columns of the files view and build up our weighting on it. --->
					<cfloop list="#replace(arguments.searchString,' ',',','all')#" delimiters="," index="listItem">
						<cfif not firstpass>
							UNION ALL
						</cfif>
						select f.FileID, 1.5 as weighting from files as f where
						CONTAINS(f.name,<cf_queryparam value='#listItem#' cfsqltype="CF_SQL_VARCHAR">)
							<cfset firstPass = false>
					</cfloop>
					<cfloop list="#replace(arguments.searchString,' ',',','all')#" delimiters="," index="listItem">
						UNION ALL
						select f.FileID, 1 as weighting from files as f where
						CONTAINS(f.*,<cf_queryparam value='#listItem#' cfsqltype="CF_SQL_VARCHAR">)
					</cfloop>
				</cfif>

				<cfif indexedFiles.recordCount>
					UNION ALL
					select f.FileID, 1 as weighting from files as f where f.fileID in (<cf_queryparam value="#valueList(indexedFiles.custom1)#" cfsqltype="cf_sql_integer" list="true">)
				</cfif>
				<cfif isNumeric(arguments.searchString)>
					UNION ALL
					select f.FileID, 1 as weighting from files as f where f.fileID = <cf_queryparam value="#arguments.searchString#" cfsqltype="cf_sql_integer">
				</cfif>

				<cfif listFindNoCase(arguments.searchTables,"fileType")>
					UNION ALL
					select f.FileID, 1 as weighting from files as f
					inner join filetype ft on f.filetypeID = ft.filetypeID where
					CONTAINS(ft.type,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
				</cfif>
				<cfif listFindNoCase(arguments.searchTables,"fileTypeGroup")>
					UNION ALL
					select f.FileID, 1 as weighting from files as f
					inner join filetype ft on f.filetypeID = ft.filetypeID
					inner join filetypegroup ftg on ft.filetypegroupID = ftg.filetypegroupID where
					CONTAINS(ftg.heading,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
					<!--- 2014-07-07 PPB Case 440468 removed test to see if fileid is in valueList(indexedFiles.custom1) here because we have already included those files above --->
				</cfif>
				<cfif listFindNoCase(arguments.searchTables,"fileFamily")>
					UNION ALL
					select f.FileID, 1 as weighting from files as f
					inner join fileFamily ff on ff.fileFamilyID = f.fileFamilyID where
					CONTAINS(ff.title,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
					<!--- 2014-07-07 PPB Case 440468 removed test to see if fileid is in valueList(indexedFiles.custom1) here because we have already included those files above --->
				</cfif>
				<cfif listFindNoCase(arguments.searchTables,"keywords")>
					UNION ALL
					select f.FileID, 1 as weighting from files as f
					inner join (textMultipleFlagData t
					inner join flag on flag.flagID = t.flagID
					inner join flagGroup fg on fg.flagGroupID = flag.flagGroupID and fg.flagGroupTextID = 'filesKeywords' and fg.entityTypeID =  <cf_queryparam value="#application.entityTypeID.files#" CFSQLTYPE="CF_SQL_INTEGER" > ) on f.fileID = t.entityID  where
					CONTAINS(t.data,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
					<!--- 2014-07-07 PPB Case 440468 removed test to see if fileid is in valueList(indexedFiles.custom1) here because we have already included those files above --->
				</cfif>
					<!--- 2016-07-16 - AJC - Check for boolean flags --->
					UNION ALL
					select f.FileID, 1 as weighting from files as f
					inner join (booleanFlagData t
					inner join flag fl on fl.flagID = t.flagID
					<!--- 2017-01-03	YMA	PROD2016-3075 only active flags in active flag groups should be considered in the results. --->
					inner join flagGroup fg on fg.flagGroupID = fl.flagGroupID and fg.active = 1 and fl.active = 1 and fg.entityTypeID =  <cf_queryparam value="#application.entityTypeID.files#" CFSQLTYPE="CF_SQL_INTEGER" > ) on f.fileID = t.entityID
					inner join phraselist pl on fl.NamePhraseTextID = pl.phraseTextID
					inner join phrases p on p.phraseID = pl.phraseID
					where
					CONTAINS(p.phraseText,<cf_queryparam value='#application.com.search.prepareSearchString(searchTextString=arguments.searchString,defaultConjunction="OR")#' cfsqltype="CF_SQL_VARCHAR">)
					<!---p.phraseText like <cf_queryparam value="%#arguments.searchString#%" cfsqltype="CF_SQL_VARCHAR">--->
				) as weightedTextSearch group by fileID
			) as textsearch  on textsearch.fileID = f.fileID
		</cfif>
			  		where 1 = 1
				<cfif trim(arguments.filter) is not "">
					and #preserveSingleQuotes(arguments.filter)#
				</cfif>
				<!--- 2015-08-05 DAN K-1314 --->
				<cfif filetypegroupid is not 0 and filetypegroupid is not "">and ft.filetypegroupid  IN ( <cf_queryparam value="#fileTypeGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				<cfif filetypeid is not 0 and filetypeid is not "">and ft.filetypeid IN ( <cf_queryparam value="#fileTypeID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				<cfif ownerid is not 0>and f.personid = #ownerID#</cfif>	<!--- 2012/06/21 PPB Case 428815 add filter for owner --->
				<cfif arguments.fileTypeGroup is not "">and ftg.heading = <cf_queryparam value="#fileTypeGroup#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
				<cfif filetype is not "">and ft.type =  <cf_queryparam value="#fileType#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
				<cfif fileID is not "0">and f.fileid = #fileID#</cfif>
				<cfif languageid is not "">and f.languageid  in ( <cf_queryparam value="#languageid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				<cfif listLen(arguments.countryFilterList) gt 1>and  gf.countryID  in ( <cf_queryparam value="#arguments.countryFilterList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) or gf.countryid is null)</cfif> <!--- the isNull will bring back a record if there are no countryscopes set on this file --->

				<!--- which user groups can see the files --->
				<cfif arguments.userGroupID is not 0 and arguments.userGroupID neq "">
					and (rf.level1 = 1 or f.hasRecordRightsBitMask & 1=0)
					and (rft.level1 = 1 or ft.hasRecordRightsBitMask & 1=0)
					and (rftg.level1 = 1 or ftg.hasRecordRightsBitMask & 1=0)
				</cfif>

				<cfif request.relaycurrentuser.IsInternal EQ 0><cfif checkExpires is not 0>and ((f.triggerType = 1 and f.triggerDate >  <cf_queryparam value="#createODBCDate(now())#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ) or (f.triggerType = 0) or (f.triggerType = 2) or (f.triggerType is NULL))</cfif></cfif><!--- added a a isinternal cfif so that files will not expire for internal users --->
				<cfif structKeyExists(arguments,"uploadedBy")> and f.personID = #arguments.uploadedBy#</cfif>
				<cfif structKeyExists(arguments,"uploadedFromDate")>
					and isNull(f.lastUpdated,f.created) >= #createODBCDateTime(arguments.uploadedFromDate)#
				</cfif>
				<cfif structKeyExists(arguments,"uploadedFromDate")>
					and isNull(f.lastUpdated,f.created) <= #createODBCDateTime(arguments.uploadedToDate)#
				</cfif>
				<cfif structKeyExists(arguments,"keywords")>
					<cfif arguments.keywords neq "">
						and t.data in (<cf_queryparam value="#arguments.keywords#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">)
					<!--- If keywords has been passed in but is empty, don't return any records --->
					<cfelse>
						and 1=0
					</cfif>
				</cfif>
				<cfif structKeyExists(arguments,"source") and arguments.source neq "">
					and f.url is <cfif arguments.source eq "url">not</cfif> null
				</cfif>
				<cfif structKeyExists(arguments,"flagTextIDs")>
					<cfloop list="#arguments.flagTextIDs#" index="flagTextID">
						and f.#flagTextID# = 1
					</cfloop>
				</cfif>

				) as f
				order by
				<!--- YMA when we are searching with a searchstring we have built a weighting that should override any other ordering clause to bring the most relevant results to the top. --->
				<cfif arguments.searchString neq "">
					weighting desc,
				</cfif>
				#preserveSingleQuotes(arguments.orderby)#
			</cfoutput>
		</cfsavecontent>

		<cfif arguments.returnType eq "query">
			<CFQUERY NAME="GetResults" DATASOURCE="#application.SiteDataSource#">
				#preserveSingleQuotes(fileListQry)#
		</cfquery>

		<cfif checkFileExists>
			<cfloop query="getResults">
				<cfset thisFileExists = 1>
				<!--- NJH 2013/01/18 - set fileExists to 1 if the file record is a url.. maybe verify by doing a cfhttp call at some point --->
				<cfif getResults.url[currentRow] eq "" and not fileExists(physicalLocation)>
					<cfset thisFileExists = 0>
				</cfif>
				<cfset querySetCell(getResults,"fileExists",thisFileExists,currentRow)>
			</cfloop>
		</cfif>
		<cfreturn  getResults>
		<cfelse>
			<cfreturn  fileListQry>
		</cfif>

	</cffunction>

	<!--- BEGIN STCR 2012-05-31 P-REL109 CASE 428533 --->
	<cffunction access="public" name="checkNameConflict" hint="Attempt to determine whether a file name or a filename conflicts with an existing file name or filename. Can either match on db column files.Name or on physical file" returntype="struct">
		<cfargument name="filetitle" type="string" default="" /><!--- Pass this to match on the column files.Name --->
		<cfargument name="filetypeid" type="numeric" default="0" />
		<cfargument name="currentfileid" type="numeric" default="0" /><!--- The current file on the edit / 're-upload' screen should not be considered as a conflict with itself! --->
		<cfargument name="filefield" type="string" default="" /><!--- Pass either the input file form field name (as filefield), or the physical filename (as filename), in order to match on the physical filename --->
		<cfargument name="filename" type="string" default="" />

		<cfset var result = {isOK=false,fileExists=false} />
		<cfset var testFileUpload = "" />
		<cfset var getFileMatches = queryNew("empty") />

		<cftry>
			<cfif arguments.filefield neq "">
				<cffile action="upload" fileField="#arguments.fileField#" destination="#application.paths.temp#" nameconflict="makeUnique" result="testFileUpload">
				<cfset arguments.filename = testFileUpload.clientFile /><!--- Get the original filename --->
			</cfif>
			<cfif arguments.filename neq "" and arguments.filetypeid neq 0>
				<cfquery name="getFileMatches" datasource="#application.SiteDataSource#">
					select isnull(f.name,'') AS columnFilename, isnull(f.fileid,0) AS fileid, 'content/' + fT.path + '/' + <cf_queryparam value="#arguments.filename#" cfsqltype="cf_sql_varchar" > as fileNameAndPath,
					case when isNull(fT.secure,0) = 1 then '#application.paths.secureContent#' else '#application.paths.content#' end + '/' + fT.path + '/' + <cf_queryparam value="#arguments.filename#" cfsqltype="cf_sql_varchar" > as physicalLocation
					from filetype fT
					left join files f with(nolock) on f.filename = <cf_queryparam value="#arguments.filename#" cfsqltype="cf_sql_varchar" > and f.filetypeid = fT.filetypeid
					where fT.filetypeid = <cf_queryparam value="#arguments.filetypeid#" cfsqltype="CF_SQL_INTEGER" >
				</cfquery>
			<cfelseif arguments.filetitle neq "">
				<cfquery name="getFileMatches" datasource="#application.SiteDataSource#">
					select filename, name, fileid, fileFamilyID, filetypeid from files
					where name = <cf_queryparam value="#arguments.filetitle#" cfsqltype="CF_SQL_VARCHAR" >
					<cfif arguments.currentfileid neq 0>and fileid <> <cf_queryparam value="#arguments.currentfileid#" cfsqltype="CF_SQL_INTEGER" ></cfif><!--- Do not match against self --->
					<!--- STCR the line below would check for name matches only within the same filetype, ie allowing the same name to be re-used for different file types. Currently not required according to Gawain 2012-06-01 --->
					<cfif arguments.filetypeid neq 0>and filetypeid = <cf_queryparam value="#arguments.filetypeid#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>
				</cfquery>
			</cfif>
			<cfif getFileMatches.recordCount gt 0>
				<cfif arguments.filename neq "">
					<cfset result.fileExists = fileExists(getFileMatches.physicalLocation) />
					<cfset result.clientFile = arguments.filename />
				<cfelseif arguments.filetitle neq "">
					<cfset result.fileExists = true />
				</cfif>
				<!--- STCR was thinking that fileIDMatchList could be used for matching to existing file entities,
					eg if we were implementing the requirement to switch from file-add to file-re-upload without the user needing to find the file in the listing first.
				--->
				<!--- <cfset result.fileIDMatchList = valueList(getFileMatches.fileid) /> --->
				<cfset result.isOK = true />
			<cfelse><!--- This should not be reached unless an invalid filetypeid was passed. --->
				<!--- NJH 2013/04/04 - not sure why this was set to false.. if we didn't have a match, then it still should be ok... --->
				<cfset result.isOK = true />
			</cfif>
			<cfcatch>
				<cfset result.isOK = false />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	<!--- END STCR 2012-05-31 P-REL109 CASE 428533 --->

	<!--- WAB 2012-02-05 P-MIC001 changed to pass through argument collection and supprt a filter being passed in --->
	<cffunction access="public" name="getFilesAPersonHasEditRightsTo" hint="" return="query">
		<cfargument name="personid" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="filetypegroupid" type="numeric" default = "0">
		<cfargument name="filetypeid" type="numeric" default = "0">
		<cfargument name="filetype" type="string" default = "">
		<cfargument name="fileid" type="numeric" default = "0">
		<cfargument name="filter" type="string" default = "(1 = 1)">

		<cfset var personFilter = ""><!--- 2014-05-07 NYB Case 439373 made blank --->
		<cfset var hasFileTaskLevel3 = application.com.login.checkInternalPermissions(securityTask="fileTask",securityLevel="level3")><!--- 2014-05-07 NYB Case 439373 added --->

		<!--- START: 2014-05-07 NYB Case 439373 wrapped in IF and setting personFilter in here --->
		<cfif hasFileTaskLevel3>
			<cfset arguments.filter = arguments.filter>
		<cfelse>
			<cfset personFilter = "hasEditRights = 1">
			<cfset arguments.filter = arguments.filter & " AND " & personFilter>
		</cfif>
		<!--- END: 2014-05-07 NYB Case 439373 --->
		<cfreturn getFileList(	argumentCollection = arguments )>
	</cffunction>


<!--- WAB 2006-02-21

note that a user has view permission either if
a) they are a member of a usergroup with view rights
b) no view permissions have been set up for anyone

--->
	<!--- WAB 2012-02-05 P-MIC001 changed to pass through argument collection --->
	<cffunction access="public" name="getFilesAPersonHasViewRightsTo" hint="" return="query" validvalues="true">
		<cfargument name="personid" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="filetypegroupid" type="ANY" default = "0">
		<cfargument name="filetypeid" type="ANY" default = "0">
		<cfargument name="fileid" type="numeric" default = "0">
		<cfargument name="filetype" type="string" default = "">
		<cfargument name="languageid" type="string" default = "">
		<cfargument name="checkFileExists" type="boolean" default = "false">
		<cfargument name="orderby" type="string" default = "f.name">

		<!--- 2012-09-12	IH	Case 430243 Call getFilesAPersonHasCountryViewRightsTo for backward compatibility with customized code  --->
		<cfreturn getFilesAPersonHasCountryViewRightsTo(argumentCollection = arguments)>	<!--- 2013-07-30 PPB Case 435716 changed from <cfset> to <cfreturn> on Avid,Kerio and Pembridge prod sites --->

	</cffunction>

	<!--- WAB 2012-02-05 P-MIC001 changed to pass through argument collection --->
	<cffunction access="public" name="getFilesAPersonHasCountryViewRightsTo" hint="" return="query">
		<cfargument name="personid" type="numeric" default="#request.relayCurrentUser.personID#">
		<!--- 2015-08-05 DAN K-1314 --->
		<cfargument name="filetypegroupid" type="ANY" default = "0">
		<cfargument name="filetypeid" type="ANY" default = "0">
		<cfargument name="fileid" type="numeric" default = "0">
		<cfargument name="filetype" type="string" default = "">
		<cfargument name="languageid" type="string" default = "">
		<cfargument name="orderby" type="string" default = "f.name">
		<cfargument name="returnCountryIDList" type="boolean" default = "false">
		<cfargument name="checkFileExists" type="boolean" default="false">
		<cfargument name="fileTypeGroup" type="string" default = ""> <!--- NJH 2012/02/27 CASE 426874 --->
		<cfargument name="filter" type="string" default = "(1 = 1)">	<!--- DCC P-KAS031 15/07/2014 added filter argument --->
		<cfargument name="keywords" type="string" required="false">

		<cfset arguments.filterByRights=true>

		<cfif personid IS request.relaycurrentuser.personid>
            <cfset arguments.countryFilterList = request.relaycurrentuser.content.showForCountryID>
		<cfelse>
            <cfset arguments.countryFilterList =  "select countryid from vpeople where personid = #personid#" >
		</cfif>

		<!--- NJH 2013/05/01 - 2013 Roadmap item 193 - only get files that have a publish date of less than today --->
		<!--- DCC P-KAS031 15/07/2014 added append to filter  --->
		<cfset arguments.filter &= " AND isNull(f.publishDate,getDate()) <= getDate()">


		<cfreturn getFileList(	argumentCollection = arguments )>

	</cffunction>


	<cffunction access="public" name="getFilesForFileManagerListing" hint="" return="query">
		<cfargument name="filetypegroupid" type="numeric" default = "0">
		<cfargument name="languageid" type="string" default = "">
		<cfargument name="searchString" type="string" default = "">
		<cfargument name="countryIDList" type="string">
		<cfargument name="uploadedBy" type="numeric">
		<cfargument name="uploadedFromDate" type="date">
		<cfargument name="uploadedToDate" type="date">
		<cfargument name="userGroupID" type="numeric" default="0"><!--- STCR 2012-05-09 P-REL109 - enable users to filter by User Group --->
		<!--- <cfargument name="searchColumns" type="string" required="false" default="name,url,filename,familyName,fileGroup,fileType,keywords"> --->

		<cfset var count =0>

<!--- 	NJH 2013/10/21 - Remove the searching of a search string in the 'getFileList' function
		<cfset arguments.filter = "">

		<cfif arguments.searchColumns neq "" and arguments.searchString neq "">
			<cfsavecontent variable="arguments.filter">
				<cfoutput>
					(
					<cfloop list="#arguments.searchColumns#" index="columnName">
						<cfset count ++>

							<cfif listFindNoCase("name,filename,url",columnName)>
								f.#columnName#
							<cfelseif columnName eq "familyName">
								ff.title
							<cfelseif columnName eq "fileGroup">
								ftg.heading
							<cfelseif columnName eq "fileType">
								ft.type
							<cfelseif columnName eq "keywords">
								t.data
							</cfif>
								like <cf_queryparam value="%#arguments.searchString#%" cfsqltype="cf_sql_varchar">
						<cfif count lt listLen(arguments.searchColumns)>OR</cfif>
					</cfloop>
					)
				</cfoutput>
			</cfsavecontent>
		</cfif>

		<!--- NJH 2013/06/14 - if the search string is numeric, then search fileID as well.. don't want to search just fileID, because the filenames, for example, could contain numbers that you may want to search on --->
		<cfif isNumeric(arguments.searchString)>
			<cfset arguments.filter = "f.fileID = #arguments.searchString# or #arguments.filter#">
		</cfif> --->

		<cfif structKeyExists(arguments, "countryIDList") and listlen(arguments.countryIDList) gt 0>
			<!--- STCR 2012-05-29 P-REL109 - getFileList already has country filtering support but uses a different argument name to that already specified on this function --->
			<cfset arguments.countryFilterList = arguments.countryIDList />
		</cfif>

		<cfreturn getFileList(personID=request.relayCurrentUser.personID,argumentCollection = arguments)>

	</cffunction>

<!--- 	<cffunction access="public" name="getFileCountries" hint="adds country data to files in query" return="query">
		<cfargument name="fileQuery" type="query" required="yes">
		<cfset var qReturn = queryNew(listAppend(fileQuery.columnList,"countryIDs"))>

		<cfloop query="fileQuery">
			<cfscript>
				queryAddRow(qReturn,1);
			</cfscript>
			<cfquery name="fileCountries" datasource="#application.sitedatasource#">
				select	countryID
				from	countryScope
				where	entityID = #fileID# and
						entity = 'FILES'
			</cfquery>
			<cfloop list="#qReturn.columnList#" index="col">

				<cfif compareNoCase(col,"countryIDs") eq 0>

					<cfscript>
						querySetCell(qReturn,col,valueList(fileCountries.countryID));
					</cfscript>
				<cfelse>
					<cfscript>
						querySetCell(qReturn,col,evaluate(col));
					</cfscript>
				</cfif>
			</cfloop>
		</cfloop>

		<cfreturn qReturn>
	</cffunction>

 --->
<!--- 	<cffunction access="public" name="getFileCountriesData" hint="gets country data for a fileID" return="query">
		<cfargument name="fileID" type="any" required="yes">
		<cfset var qReturn = "">

		<cfquery name="qReturn" datasource="#application.sitedatasource#">
			select	c.*
			from	countryScope cs inner join
					country	c		on cs.countryID = c.countryID
			where	cs.entityID = #fileID# and
					cs.entity = 'FILES'
		</cfquery>

		<cfreturn qReturn>
	</cffunction> --->

	<cffunction access="public" name="get2ColForCountryLists" hint="" return="query">
		<cfargument name="originalFileQuery" type="query" required="yes">

		<cfset var fileQuery = arguments.originalFileQuery>
		<!--- 2017-01-31	WAB	RT-221 Problem with dataTypes in qGetUsersCountryAndCountryGroups() so made explicit --->
		<cfset var qReturn = queryNew('fileID,countryID','integer,integer')>

		<cfloop query="fileQuery">
			<cfloop list="#countryIDs#" index="countryID">
				<cfscript>
					queryAddRow(qReturn);
					querySetCell(qReturn,'fileID',fileID);
					querySetCell(qReturn,'countryID',countryID);
				</cfscript>
			</cfloop>
		</cfloop>
		<cfreturn qReturn>
	</cffunction>

	<cffunction access="public" name="getFilesInUserCountry" hint="" return="query">
		<cfargument name="originalFileQuery" type="query" required="yes">
		<!--- 2008/05/08 GCC trialling users country countryGroups as well - groups include users country
			This introduces possible conflicts with files in the same family in the users country and in the country group but this case should be rare
			- last revision date would be used as the decider if language didn't differentiate
			- this could lead to the group file being shown rather than the country file - may need to enhance further if this becomes an issue
			--exclude all countries as this is the last chance saloon hardcoded in getMostSuited...
		 --->
		<cfset var fileQuery = get2ColForCountryLists(arguments.originalFileQuery)>
		<cfset var userCountryAndGroups = "">
		<cfset var qFilesInCountry = "">
		<cfset var qGetUsersCountryAndCountryGroups = "">

		<cfquery name="qGetUsersCountryAndCountryGroups" datasource="#application.siteDataSource#">
			select countryGroupID from countryGroup where countryMemberID =  <cf_queryparam value="#request.relayCurrentUser.content.showForCountryID#" CFSQLTYPE="cf_sql_integer" > and countrygroupID <> 37
		</cfquery>

		<cfset userCountryAndGroups = valuelist(qGetUsersCountryAndCountryGroups.countryGroupID)>
		<!--- 2017-01-31	WAB	RT-221 DataType now made explicit in get2ColForCountryLists() so can remove CAST --->
		<cfquery name="qFilesInCountry" dbtype="query">
			select	fileID
			from	fileQuery
			where	countryID in (#userCountryAndGroups#)
		</cfquery>

		<cfreturn valueList(qFilesInCountry.fileID)>
	</cffunction>

	<cffunction access="public" name="getMostSuitedFileFromFamily" hint="" return="query">
		<cfargument name="originalFileQuery" type="query" required="yes">
		<cfargument name="overrideSortOrder" default="familySortOrder, name">

<!--- 	now done in original query	<cfset var fileQuery = getFileCountries(arguments.originalFileQuery)> --->
		<cfset var fileQuery = arguments.originalFileQuery>
		<cfset var qReturn = "">
		<cfset var lFileFamilyIDs = queryNew("fileFamilyID")>
		<cfset var useFileID = "">
		<cfset var hasLanguage = "">
		<cfset var lFilesInCountry = getFilesInUserCountry(fileQuery)>

		<!--- get all the files not in families, we need to return all of these  --->
		<cfquery name="qReturn" dbtype="query">
			select 	*
			from 	fileQuery
			where 	cast(fileFamilyID as VARCHAR) = '0' or
						fileFamilyID IS NULL
		</cfquery>


		<!--- get all the files in families --->
		<cfquery name="getFileFamilyIDs" dbtype="query">
			select 	distinct fileFamilyID, cast(fileFamilyID as VARCHAR)
			from 	fileQuery
			where 	cast(fileFamilyID as VARCHAR) <> '0' and
					fileFamilyID is not null
		</cfquery>

		<cfset lFileFamilyIDs = valueList(getFileFamilyIDs.fileFamilyID)>
		<cfset userCountry = request.relayCurrentUser.content.showForCountryID>

		<cfset lLanguageIDs = request.relayCurrentUser.languageID>
		<!--- get list of language defaults for a country --->
		<cfquery name="getAllLanguages" datasource="#application.siteDataSource#">
			select 	defaultLanguageID
			from	country
			where	cast(countryID as INTEGER) =  <cf_queryparam value="#userCountry#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>

		<cfif getAllLanguages.recordCount gt 0>
			<cfset lLanguageIDs = listAppend(lLanguageIDs,valueList(getAllLanguages.defaultLanguageID))>
		</cfif>

		<cfloop list="#lFileFamilyIDs#" index="fileFamilyID">
			<cfset hasLanguage = "">
			<cfset useFileID = "">

			<CFIF lFilesInCountry IS NOT "">   <!--- wab 2007/05/22 HAD TO ADD THIS CHECK because sometimes lFilesInCountry is blank --->
				<cfquery name="getCountryMatch" dbtype="query">
				select 	fileID
				from 	fileQuery
				where	cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
						fileID in (#lFilesInCountry#)
				</cfquery>
			</CFIF>
			<!--- <cfquery name="getCountryMatch" dbtype="query">
				select 	fileID
				from 	fileQuery
				where	cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
						'#userCountry#' in (countryIDs)
			</cfquery> --->
			<!--- if we've found docs associated, check for country --->
			<cfif lFilesInCountry IS NOT "" AND getCountryMatch.recordCount gt 0>
				<cfloop list="#lLanguageIDs#" index="languageID">
					<cfquery name="getCountryLanguageMatch" dbtype="query">
						select 	fileID
						from 	fileQuery
						where	cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
								fileID in (#lFilesInCountry#) and
								languageID  = #languageID#
					</cfquery>
					<!--- <cfquery name="getCountryLanguageMatch" dbtype="query">
						select 	fileID
						from 	fileQuery
						where	cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
								'#userCountry#' in (countryIDs) and
								languageID  = #languageID#
					</cfquery> --->
					<cfif getCountryLanguageMatch.recordCount gt 0>
						<cfset hasLanguage = valueList(getCountryLanguageMatch.fileID)>
						<cfbreak>
					</cfif>
				</cfloop>
				<!--- more than 1, narrow down using file revision date --->
				<cfif listLen(hasLanguage) gt 1>
						<cfquery name="getCountryLanguageDateMatch" dbtype="query">
							select 		fileID
							from 		fileQuery
							where		cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
										fileID in (#lFilesInCountry#) and
										fileID in (#hasLanguage#)
							order by	revision desc
						</cfquery>
						<!--- <cfquery name="getCountryLanguageDateMatch" dbtype="query">
							select 		fileID
							from 		fileQuery
							where		cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
										'#userCountry#' in (countryIDs) and
										fileID in (#hasLanguage#)
							order by	revision desc
						</cfquery> --->
						<cfset useFileID = getCountryLanguageDateMatch.fileID>
				<!--- no records means a search for latest. --->
				<cfelseif listLen(hasLanguage) lt 1>
					<cfquery name="getCountryDateMatch" dbtype="query">
						select 		fileID
						from 		fileQuery
						where		cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
									fileID in (#lFilesInCountry#)
						order by	revision desc
					</cfquery>
					<!--- <cfquery name="getCountryDateMatch" dbtype="query">
						select 		fileID
						from 		fileQuery
						where		cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
									'#userCountry#' in (countryIDs)
						order by	revision desc
					</cfquery> --->
					<cfif getCountryDateMatch.recordCount gt 0>
						<cfset useFileID = getCountryDateMatch.fileID>
					</cfif>
				<cfelse>
					<cfset useFileID = hasLanguage>
				</cfif>
			<cfelse>

				<!--- Add English as a final default in the country --->
				<cfset lLanguageIDs = listAppend(lLanguageIDs,1)>

				<!--- check for language match --->
				<cfloop list="#lLanguageIDs#" index="languageID">
					<cfquery name="getLanguageMatch" dbtype="query">
						select 	fileID
						from 	fileQuery
						where	cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
								languageID  = #languageID#
								and ',' + countryIDs + ',' like '%,37,%'
					</cfquery>
					<cfif getLanguageMatch.recordCount gt 0>
						<cfset hasLanguage = valueList(getLanguageMatch.fileID)>
						<cfbreak>
					</cfif>
				</cfloop>
				<cfif listLen(hasLanguage) gt 1>
					<cfquery name="getLanguageDateMatch" dbtype="query">
						select 		fileID
						from 		fileQuery
						where		cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
									fileID in (#hasLanguage#)
									and ',' + countryIDs + ',' like '%,37,%'
						order by	revision desc
					</cfquery>
					<cfset useFileID = getLanguageDateMatch.fileID>
				<cfelseif listLen(hasLanguage) eq 1>
					<cfset useFileID = hasLanguage>
				</cfif>
			</cfif>
			<cfif useFileID neq "">
				<cfquery name="useFile" dbtype="query">
					select	*
					from	fileQuery
					where	cast(fileFamilyID as VARCHAR) = '#fileFamilyID#' and
							fileID = #useFileID#
				</cfquery>
				<cfif useFile.recordCount eq 1>
					<cfscript>
						queryAddRow(qReturn,1);
					</cfscript>
					<cfloop list="#useFile.columnList#" index="col">
						<cfscript>
							querySetCell(qReturn,col,evaluate("useFile.#col#"));
						</cfscript>
					</cfloop>
				</cfif>
			</cfif>
		</cfloop>

		<cfquery name="qReturnOrdered" dbtype="query">
			select *
			from qReturn
			order by #arguments.overrideSortOrder#
		</cfquery>

		<!--- <cfquery name="qReturnOrdered" dbtype="query">
			select *, case when sortOrder is not NULL then sortOrder as thisSortOrder else 99 as thisSortOrder end
			from qReturn
			order by  thisSortOrder, name
		</cfquery> --->

		<cfreturn qReturnOrdered>
	</cffunction>

	<cffunction access="public" name="setAllFileSizes" hint="loops through all files on system and writes their size to the db.">
		<cfargument name="dataSource" type="string" default="#application.SiteDataSource#">

		<cfquery name="getFolders" datasource="#arguments.dataSource#">
			select path
			from filetype
		</cfquery>

		<cfloop query="getFolders">
			<cfdirectory action="list" directory="#application.paths.content#\#path#" name="directories">
			<cfloop query="directories">
				<cfquery name="updateFileSize" datasource="#arguments.dataSource#">
					update 	files
					set		fileSize =  <cf_queryparam value="#size#" CFSQLTYPE="CF_SQL_INTEGER" >
					<!--- START: NYB 2009-10-06 LHID2554 - added N before '#name#' --->
					where 	filename =  <cf_queryparam value="#name#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<!--- END: NYB 2009-10-06 LHID2554 --->
				</cfquery>
				<cfoutput>Path: #getFolders.path# - filename: #name# - filesize set to: #size#<br></cfoutput>
			</cfloop>
		</cfloop>

		<!--- <cfquery name="getAllFiles" datasource="#arguments.dataSource#">
			SELECT DISTINCT f.fileid, ft.path, f.filename
			FROM      files f INNER JOIN
                      filetype ft ON f.filetypeid = ft.filetypeid INNER JOIN
                      filetypegroup ftg ON ft.filetypegroupid = ftg.filetypegroupid

		</cfquery>

		<cfloop query="getAllFiles">
			<cfset fullpath = "#application.paths.content#\#path#\#filename#">
			<!--- <cffile action="read" file="#fullpath#" variable="tmpFile"> --->
			<cfdirectory action="list" name="fileFromList" directory="#application.paths.content#\#path#" filter="#filename#">
			<cfif fileFromList.recordCount gt 0>
				<cfset thisFileSize = fileFromList.Size>
				<cfquery name="updateFileSize" datasource="#arguments.dataSource#">
					update 	files
					set		fileSize = #thisFileSize#
					where 	fileid = #fileID#
				</cfquery>
				<cfoutput>fileID: #fileID# - Path: #fullpath#, filesize set to: #thisFileSize#<br></cfoutput>
			</cfif>
		</cfloop> --->
	</cffunction>

	<cffunction access="public" name="setAllFileSortOrders" hint="loops through all files and assigns default sortOrder where none is specified.">
		<cfargument name="dataSource" type="string" default="#application.SiteDataSource#">

		<cfquery name="setSortOrders" datasource="#arguments.dataSource#">
			update files
			set sortOrder = 99
			where sortOrder is null
		</cfquery>

		<cfquery name="setSortOrders" datasource="#arguments.dataSource#">
			update files
			set familySortOrder = sortOrder
			where familySortOrder is null
		</cfquery>
	</cffunction>


	<!--- NYB	2009-09-04	P-SNY081 Created Function --->
	<cffunction access="public" name="addFileToDb" hint="Adds File to Files table in DB">
		<cfargument name="FileTypeID" type="numeric" required="true">
		<cfargument name="Name" type="string" required="true">
		<cfargument name="FileName" type="string" required="true">
		<cfargument name="PersonID" type="numeric" default="4">
		<cfargument name="LanguageID" type="numeric" default="1">
		<cfargument name="Created" type="date" default="#now()#">
		<cfargument name="CreatedBy" type="numeric" default="#request.relaycurrentuser.usergroupid#">
		<cfargument name="LastUpdated" type="date" default="#now()#">
		<cfargument name="LastUpdatedBy" type="numeric" default="#request.relaycurrentuser.usergroupid#">
		<cfargument name="LastUpdatedByPerson" type="numeric" default="#request.relaycurrentuser.personID#">
		<cfargument name="revision" type="date" default="#now()#">

		<cfquery name="addFileToDbQry" datasource="#application.SiteDataSource#">
			<!--- START: NYB 2009-10-06 LHID2554 - added N before filename --->
			if not exists (select * from files where FileTypeID='#FileTypeID#' and FileName =  <cf_queryparam value="#FileName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			<!--- END: NYB 2009-10-06 LHID2554 --->
				begin
					insert into files (#StructKeyList(arguments)#)
					values (
					<cfset iCount = 0>
					<cfloop index="i" list="#StructKeyList(arguments)#">
						<cfset iCount = iCount + 1>
						<cfif isNumeric(arguments[i]) or isDate(arguments[i])>
							#arguments[i]#
						<cfelse>
							<!--- START: NYB 2009-10-06 LHID2554 - added N before '#arguments[i]#' --->
							N'#arguments[i]#'
							<!--- END: NYB 2009-10-06 LHID2554 --->
						</cfif>
						<cfif iCount lt StructCount(arguments)>,</cfif>
					</cfloop>
					)
					select scope_identity() as fileid,'true' as added
				end
			else
				begin
					<!--- START: NYB 2009-10-06 LHID2554 - added N before the FileName value --->
					<!--- 2009/01/13 GCC added update to revision as well--->
					update files
						set LastUpdated = <cf_queryparam value="#LastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
							revision = <cf_queryparam value="#LastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
							LastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
						where FileTypeID='#FileTypeID#' and FileName =  <cf_queryparam value="#FileName#" CFSQLTYPE="CF_SQL_VARCHAR" >

					select fileid,'false' as added from files where FileTypeID='#FileTypeID#' and FileName =  <cf_queryparam value="#FileName#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<!--- END: NYB 2009-10-06 LHID2554 --->
				end
		</cfquery>
		<cfset application.com.search.updateFilesSearchCollectionInThread(fileid=addFileToDbQry.fileid)>
		<cfreturn addFileToDbQry>
	</cffunction>

	<!--- WAB 2009/09/16
	2010/10/21 added a movedirectory
	 --->
	<cffunction access="public" name="copyDirectory" hint="Copy A Directory Structure">
			<cfargument name="source" required="true">
			<cfargument name="destination" required="true">
			<cfargument name="recurse" default="true">

			<cfset moveOrCopyDirectory(source=source, destination = destination , recurse = recurse, action = "copy")>

	</cffunction>


	<cffunction access="public" name="moveDirectory" hint="Move A Directory Structure">
			<cfargument name="source" required="true">
			<cfargument name="destination" required="true">
			<cfargument name="recurse" default="true">

			<cfset moveOrCopyDirectory(source=source, destination = destination , recurse = recurse, action = "move")>

	</cffunction>

	<cffunction access="public" name="moveOrCopyDirectory" hint="Move or Copy A Directory Structure">
			<cfargument name="source" required="true">
			<cfargument name="destination" required="true">
			<cfargument name="Action" required="true">
			<cfargument name="recurse" default="true">

			<cfdirectory action="list" name="directoryQuery" directory="#source#" recurse = #recurse#>

			<!--- need to make sure all slashes are \ and there are no \\ (except as first two characters)--->
			<cfset arguments.source = cleanFilePath(arguments.source)>
			<cfset arguments.destination = cleanFilePath(arguments.destination)>

			<cfif not directoryExists (destination)>
				<cfdirectory action="create" directory="#destination#">
			</cfif>

			<cfloop query = "directoryQuery">
				<cfset destinationDirectory = replaceNoCase(directory,source,destination)>
				<cfif type is "dir" >
					<cfif not directoryExists (destinationDirectory & "\" & name)>
						<cfdirectory action="create" directory="#destinationDirectory#\#name#">
					</cfif>
				<cfelse>
					<cffile action="#arguments.action#" source = "#directory#\#name#" destination =#destinationDirectory# >
				</cfif>

			</cfloop>

			<cfif action is "Move">
				<!--- need to delete old directory structure --->
				<cftry>
					<cfdirectory action="delete" directory="#source#" recurse = #recurse#>
					<cfcatch>
						<!--- Can sometimes fail - especially during testing if someone has the folder open! --->
					</cfcatch>
				</cftry>
			</cfif>

	</cffunction>

	<!--- WAB 2011/11/14 was needed for the moveDirectoryFunction aboeve --->
	<cffunction name="cleanFilePath" access="public" hint="changes all / to \ and removes \\ (except first two characrers)">
		<cfargument name="path" required="true">
		<cfset var result = replace(path,"/","\","ALL")>
		<cfset result = rereplace(result,"(.)\\\\","\1\","ALL")>
		<cfreturn result>
	</cffunction>

	<cffunction name="readFile" access="public" hint="Reads a file">
		<cfargument name="fileID" type="numeric" required="true">

		<cfset var fileContents="">

		<cfset getFile = getFileList(fileID=arguments.fileID)> <!--- maybe need to check for rights here!! --->
		<cfif fileExists(getfile.physicalLocation)>
			<CFFILE ACTION="Read" FILE="#getfile.physicalLocation#" VARIABLE="fileContents">
		</cfif>
		<cfreturn fileContents>
	</cffunction>

	<cffunction name="convertAbsolutePathToRelative">
		<cfargument name="path" type="string" >

		<cfset var result = "">
		<!---
			make up a regular expression of the three absolute paths to above /relay, /code, /content
			need to escape the / characters, and only after that can we put in the |s (by changing the delimiters)
		--->
		<cfset var regExp = application.com.regExp.escapeRegExp("#application.paths.core#,#application.paths.abovecode#,#application.paths.userfiles#")>
		<cfset regExp = listChangeDelims(regExp,"|",",")>

		<cfset result = rereplaceNoCase(path,regexp,"")>

		<cfreturn result>
	</cffunction>


	<!--- 	created:  	2011/06/07 NYB
			for:		LHID6807
	--->
	<cffunction name="getFileAttributes" access="public" hint="Reads a file">
		<cfargument name="fileID" type="numeric" required="false">
		<cfargument name="physicalLocation" type="string" required="false">
		<cfargument name="maxImgWidth" type="numeric" default="800">
		<cfargument name="maxImgHeight" type="numeric" default="600">

		<cfscript>
			var fileDetails = structnew();
		</cfscript>
		<cfif structkeyexists(arguments,"fileID")>
			<cfset getfile = getFileList(fileid = arguments.fileID)>
			<cfif isDefined("getFile")>
                <cfif getFile.PHYSICALLOCATION EQ "" and getFile.SOURCE EQ "URL">
                     <!--- DAN - URL source with no physical location --->
                    <cfset fileDetails.PhysicalLocation = "">
                    <cfset securedFileResult = getSecuredFile(fileID=fileID)>
                    <cfif getFile.URL NEQ "">
                        <CFSET fileDetails.URLLocation = "#getFile.URL#">
                    <cfelse>
                        <CFSET fileDetails.URLLocation = "#request.currentSite.protocolAndDomain#/#getFile.fileNameAndPath#">
                    </cfif>
                <cfelseif getFile.secure and getFile.PHYSICALLOCATION NEQ "">
                    <cfset securedFileResult = getSecuredFile(fileID=fileID)>
                    <CFSET fileDetails.URLLocation = replacenocase(securedFileResult.relativeURL,"%2E",".")>
                    <CFSET fileDetails.PhysicalLocation = securedFileResult.ABSOLUTEURL>
                <cfelse>
					<!--- 2015-09-09	ACPK	PROD2015-40	Encoded the filename in URLLocation --->
                    <CFSET fileDetails.URLLocation = "#request.currentSite.protocolAndDomain#/#URLEncodeFilePath(getFile.fileNameAndPath)#">
                    <CFSET fileDetails.PhysicalLocation = replace(getFile.PHYSICALLOCATION,"/","\","all")>
                </cfif>
            </cfif>
		<cfelseif structkeyexists(arguments,"physicalLocation")>
			<CFSET fileDetails.physicalLocation = arguments.physicalLocation>
			<cfset x = findnocase("\Content",fileDetails.physicalLocation)>
			<CFSET fileDetails.URLLocation="">
			<cfif x neq 0>
				<CFSET fileDetails.URLLocation = "#request.currentSite.protocolAndDomain#/#Right(fileDetails.physicalLocation, len(fileDetails.physicalLocation)-x)#">
			</cfif>
		<cfelse>
			<cfthrow message="The getFileDetails function requires parameters fileID or physicalLocation to be passed">
		</cfif>
        <cfset fileDetails.doesFileExist = (fileDetails.physicalLocation NEQ "" and fileExists(fileDetails.physicalLocation))>
		<cfset fileDetails.extension = listlast(fileDetails.physicalLocation,".")>

		<cfif isDefined("getFile")>
			<!--- 2015/10/06 - DCC changed from just getfile.filename as not defined if no fileid supplied --->
		<cfset fileDetails.filename = getfile.filename>
		<cfelse>
			<cfset fileDetails.filename = listlast(fileDetails.physicalLocation,"\")>
		</cfif>

		<cfset fileDetails.IsImageFile = IsImageFile(fileDetails.physicalLocation)>

		<cfif fileDetails.doesFileExist>
			<cfif fileDetails.IsImageFile>
                <cfset imageData = ImageRead(fileDetails.physicalLocation)>
                <cfset imageType = imageGetBufferedImage(imageData).getType()>
                <cfif imageType NEQ 0>
    				<!--- 2015-09-09	ACPK	PROD2015-40	 Removed code obsoleted by encoding filename earlier --->
					<!--- 2016-05-10	YMA		PROD2016-2451 update cfimage to use physical location --->
					<cfimage source="#fileDetails.physicalLocation#" name="imageVar">
    				<cfset fileDetails.ImgHeight="#ImageGetHeight(imageVar)#">
    				<cfset fileDetails.ImgWidth="#ImageGetWidth(imageVar)#">
                <cfelse>
                    <!--- custom image type, use JAI to find the required image size attributes (JPEGs only) --->
                    <cfif listfindnocase("jpg, jpeg", fileDetails.extension)>
                        <cfscript>
                            try {
                                imageFile = createObject("java", "java.io.File").init(javaCast("string", fileDetails.physicalLocation));
                                fileStream = createObject("java", "com.sun.media.jai.codec.FileSeekableStream").init(imageFile);
                                parameterBlock = createObject("java", "java.awt.image.renderable.ParameterBlock").init();
                                parameterBlock.add(fileStream);
                                bufferedImage = createObject("java", "javax.media.jai.JAI").create(javaCast("string", "jpeg"), parameterBlock).getAsBufferedImage();
                            } catch (any e) {
                                writeLog(type="error", text="Error on reading image: #e.message# #e.detail# #e.stackTrace#", file="imageError.log" );
                            } finally {
                                if (isDefined("fileStream")) {
                                    fileStream.close();
                                }
                            }
                        </cfscript>
                    </cfif>

                    <cfset fileDetails.ImgHeight = isDefined("bufferedImage") ? bufferedImage.getHeight() : arguments.maxImgHeight>
                    <cfset fileDetails.ImgWidth = isDefined("bufferedImage") ? bufferedImage.getWidth() : arguments.maxImgWidth>
                </cfif>

		    <cfelse>
				<cfif fileDetails.extension eq "swf">
					<cfscript>
						fis = createObject("java", "java.io.FileInputStream").init(fileDetails.physicalLocation);
						decoder = createObject("java", "macromedia.swf.TagDecoder").init(fis);
						header = decoder.decodeHeader();
						fis.close();
						rect = header.size;
						fileDetails.IsCompressed=header.compressed;
						fileDetails.FrameCount=header.framecount;
						fileDetails.FrameRate=header.rate;
						fileDetails.FlashVersion=header.version;
						fileDetails.ImgHeight=rect.getHeight();
						fileDetails.ImgWidth=rect.getWidth();
						fileDetails.ImgHeight=fileDetails.ImgHeight/20;
						fileDetails.ImgWidth=fileDetails.ImgWidth/20;
					</cfscript>
				</cfif>
			</cfif>

			<cfif structkeyexists(fileDetails,"ImgWidth") and structKeyExists(fileDetails, "ImgHeight")>
				<cfif arguments.maxImgWidth lt fileDetails.ImgWidth>
					<cfset fileDetails.maxImgWidth=arguments.maxImgWidth>
					<cfset fileDetails.maxImgHeight=fileDetails.ImgHeight/(fileDetails.ImgWidth/fileDetails.maxImgWidth)>
				<cfelse>
					<cfset fileDetails.maxImgWidth=fileDetails.ImgWidth>
					<cfset fileDetails.maxImgHeight=fileDetails.ImgHeight>
				</cfif>

				<cfif arguments.maxImgHeight lt fileDetails.maxImgHeight>
					<cfset fileDetails.maxImgHeight=arguments.maxImgHeight>
					<cfset fileDetails.maxImgWidth=fileDetails.ImgWidth/(fileDetails.ImgHeight/fileDetails.maxImgHeight)>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn fileDetails>
	</cffunction>

	 <!--- RMB 2011/12/02 - P-REL101 - Delivery Method query. Sets options in "Admin Tool/Edit Setting Files/Files" --->
	<cffunction name="AllDeliveryMethods" access="public" hint=""  validValues="true">

		<cfset var getDeliveryMethods = "">

		<cf_querySim>
			<cfoutput>
			getDeliveryMethods
			display,Value
			Phr_Sys_DeliveryMethod_Local|Local
			Phr_Sys_DeliveryMethod_AkamaiCDN|AkamaiCDN
			</cfoutput>
		</cf_querySim>

		<cfreturn getDeliveryMethods>

    </cffunction>

	 <!--- RMB 2011/12/02 - P-REL101 - Returns only the selected options from Delivery Method from Admin Tool/Edit Setting Files/Files --->
	<cffunction name="ValidDeliveryMethods" access="public" hint="" validValues="true">

		<cfset var UseDeliveryMethods = AllDeliveryMethods()>
		<cfset var result = "">

		<cfquery name="result" dbtype="query">
		SELECT * FROM UseDeliveryMethods
		WHERE [value] IN (#listqualify(application.com.settings.getsetting('files.deliveryMethod'),"'")#)
		ORDER BY [Value]
		</cfquery>

		<cfreturn result>

    </cffunction>

	 <!--- RMB 2011/12/02 - P-REL101 - Returns the Default Delivery Method for a file type so when uploading a new file it defaults to the normal location --->
	<cffunction name="getAllFileType" access="public" hint="">

		<cfargument name="sortOrder" type="string" default="filetypeid ASC">

        <cfquery name="queryAllFileType" datasource="#application.siteDataSource#">
		SELECT [filetypeid]
		      ,[filetypegroupid]
		      ,[type]
		      ,[path]
		      ,[secure]
		      ,[autounzip]
		      ,[DefaultDeliveryMethod]
		FROM filetype
		ORDER BY <cf_queryObjectName value="#sortOrder#">
		</cfquery>

		<cfreturn queryAllFileType>

    </cffunction>

	<cffunction name="FindAndReplaceInDirFile" hint="" access="public">
		<cfargument name="Find" type="string" required="true">
		<cfargument name="Replace" type="string" required="true">
		<cfargument name="path" type="string" required="true">
		<cfargument name="filter" type="string" default = "*.*">
		<cfargument name="recurse" type="boolean" default="true">
		<cfargument name="type" type="string" default="file" hint="Search through files and/or directories.Valid values are: file,all,dir">

		<cfset var FindAndReplaceStruct = {find = find,replace = replace}>
		<cfset var FindAndReplaceArray = [FindAndReplaceStruct]>

		<cfreturn MultipleFindAndReplaceInDirFile(FindAndReplaceArray=FindAndReplaceArray,path=arguments.path,filter=arguments.filter,recurse=arguments.recurse,type=arguments.type)>

	</cffunction>


	<cffunction name="MultipleFindAndReplaceInDirFile" hint="" access="public">
		<cfargument name="FindAndReplaceArray" required="true">
		<cfargument name="path" required="true">
		<cfargument name="filter" default = "*.*">
		<cfargument name="recurse" default="true">
		<cfargument name="type" type="string" default="file" hint="Search through files and/or directories.Valid values are: file,all,dir">

		<cfset var result = {isOK=true,message="",error=""}>
		<cfset var files = "">
		<cfset var fileContent = "">
		<cfset var newFileContent = "">

		<cfset arguments.path = replace (arguments.path,"/","\","ALL")>
		<cfdirectory action="list" directory="#arguments.path#" filter="#arguments.filter#" recurse="#arguments.recurse#" name="files" type="#arguments.type#">

		<cfloop query="files">
			<cfif not findNoCase(".svn",directory)>

				<cfif type eq "file">
					<cffile action="read" file="#directory#\#name#" variable="fileContent">
					<cfset shortfilename ='#replaceNoCase(directory,path,"","ONCE")#\#name#'>

					<cfset newFileContent = fileContent>
					<cfloop array="#FindAndReplaceArray#" index="FindAndReplaceStruct">
						<cfset newFileContent = reReplaceNoCase(newFileContent,FindAndReplaceStruct.find,FindAndReplaceStruct.replace,"ALL")>
					</cfloop>

					<cfif fileContent neq newFileContent>
						<cffile action="write" file="#directory#\#name#" output="#newFileContent#" addNewLine="false">
						<cfset result.message = result.message & "#shortfilename# updated.<BR>">
					</cfif>
				</cfif>

				<cfif type eq "dir">
					<cfdirectory action="rename" directory="#directory#" newDirectory="#reReplaceNoCase(directory,FindAndReplaceStruct.find,FindAndReplaceStruct.replace,'ALL')#">
				</cfif>
			</cfif>

		</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- check whether a file exists (secure or unsecure...) --->
	<cffunction name="doesFileExist" access="public" returnType="struct">
		<cfargument name="filePath" type="string" required="true">

		<cfset var dir = "">
		<cfset var result = {exists=false,path=""}>
		<cfset var thisFilePath = replace(arguments.filePath,"/","\","ALL")>

		<cfloop list="secure,userfiles" index="dir">
			<cfif fileExists("#application.paths[dir]#\#thisFilePath#")>
				<cfset result.path = "#application.paths[dir]#\#thisFilePath#">
				<cfset result.exists = true>
				<cfreturn result>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- START: 2013-04-11 PPB Case 434536
				2013-05-01  WAB renamed and added a test for rights
	--->
	<cffunction name="isFileSecureOrHasRights" access="public" hint="Checks to see if a file is secure" returntype="boolean">
		<cfargument name="fileID" type="numeric" required="true">

		<cfset var getFile = getFileList(fileID = fileID)>
		<cfset result = getFile.recordCount AND (getFile.Secure OR getFile.recordHasSomeLevel1Rights)>

		<cfreturn  result>
	</cffunction>
	<!--- END: 2013-04-11 PPB Case 434536 --->


	<!--- NJH 2013/09/20 Sprint 1 Case 435830 - function to return allowedfiled types for the fileType editor --->
	<cffunction name="getAllowedFilesForType" access="public" validValues="true" returnType="query" output="false">
		<cfset var allowedfiles = listAppend(application.com.settings.getSetting("files.allowedFileExtensions"),"images,documents")>
		<cfset var allowedFilesForType = "">

		<cfoutput>
			<cf_querySim>
				allowedFilesForType
				display,value
				<cfloop list="#allowedFiles#" index="fileExt">#fileExt#|#fileExt##chr(10)##chr(13)#</cfloop>
			</cf_querySim>
		</cfoutput>

		<cfreturn allowedFilesForType>
	</cffunction>


	<!--- NJH 2013/09/20 - Sprint 1 case 435830 0- function to check whether file type is allowed. could be used by fileUpload and unzip functions
		Still needs support for
	 --->
	<cffunction name="checkFileExtension" access="public" returnType="struct">
		<cfargument name="fileTypeID" type="numeric" required="false">
		<cfargument name="filename" type="string" required="true">

		<cfset var allowedFileExtensions = application.com.settings.getSetting("files.allowedFileExtensions")>
		<cfset var result = {isOK=true,allowed=true,validExtensions=allowedFileExtensions}>
		<cfset var getAllowedFilesForType = "">
		<cfset var acceptType = {documents="pdf,docx,doc,txt",images="jpg,jpeg,png,gif,bmp,tiff"}>

		<cfif structKeyExists(arguments,"fileTypeID")>
			<cfquery name="getAllowedFilesForType" datasource="#application.siteDataSource#">
				select replace(replace(allowedFiles,'documents','#acceptType.documents#'),'images','#acceptType.images#') as allowedfiles
				from fileType where fileTypeID=<cf_queryparam value="#arguments.fileTypeID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfif getAllowedFilesForType.allowedFiles neq "">
				<cfset allowedFileExtensions = getAllowedFilesForType.allowedFiles>
				<cfset result.validExtensions=allowedFileExtensions>
			</cfif>
		</cfif>

		<cfif not listFindNoCase(allowedFileExtensions,listLast(arguments.filename,"."))>
			<cfset result.allowed = false>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getFileDisplayIcon" access="public" returnType="struct">
		<cfargument name="filename" required="true" type="string">

		<cfset var imgSrc = "blank.png">
		<cfset var fileExtension = listLast(arguments.filename)>
		<cfset var result = {src="/images/icons/#imgSrc#",imageTag = "<img src='/images/icons/blank.png' border=0>",iconExists=false}>

		<cfif listFindNoCase("pdf,doc,xls,ppt,zip,docx,xlsx,pptx,txt", fileExtension)>
			<cfset imgSrc = "#fileExtension#.gif">
			<cfif fileExtension eq "txt">
				<cfset imgSrc = "txt.png">
			</cfif>
			<cfset result.iconExists = true>
		</cfif>

		<cfset result.src = "/images/misc/#imgSrc#">
		<cfset result.imageTag = '<img src="#result.src#" border=0 title="#arguments.filename#">'>

		<cfreturn result>
	</cffunction>


	<cffunction name="updateSyndicatedContent" >
		<cfargument name="fileTypeID">

		<cfset var result = {isOK = true,message="",updated = #structNew()#, uptodate = #structNew()#, doNotExist = #structNew()#}>

		<cfset syndicateServiceURL = "http://192.168.168.61:8080/syndicatedContent.cfm"><!--- Note the internal URL.  The file will only accept requets from internal IP addresses --->

		<!--- get list of files in filegroup --->
		<cfset files = application.com.filemanager.getFilesForFileManagerListing (fileTypeID = arguments.fileTypeID)>

		<cfloop query="#files#">
			<!--- check the last modified date from the source server --->
			<cfset fullurl = "#syndicateServiceURL#?filename=#fileName#&filePath=#path#&relaywareDatabaseID=#application.instance.databaseID#">
			<cfhttp url="#fullURL#" method="head" >

			<cfset headers = cfhttp.ResponseHeader>

			<cfif not structKeyExists (headers,"notfound") and structKeyExists (headers,"last-Modified")>

				<cfset remoteDate = lsparsedatetime(headers["last-modified"],"English (UK)")>
				<cfif revision is "" or remoteDate GT revision>

					<cfset finalpath = reverse(listRest(reverse(replace(physicalLocation,"/","\","ALL")),"\"))>
					<cfset tempPath = application.paths.temp>
					<cfif not directoryExists(finalPath)>
						<cfdirectory action="create" directory="#finalPath#" recurse="true">
					</cfif>

					<cfhttp url="#fullURL#" method="get" path="#finalPath#" file="#fileName#"><!---  path="#path#"   --->

					<cfquery name="updateFile">
					update files set revision =  <cf_queryparam value="#remoteDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  where fileid =  <cf_queryparam value="#fileid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

					<cfset structInsert(result.updated,fileName,"")>
				<cfelse>
					<cfset structInsert(result.upToDate,fileName,"")>
				</cfif>

			<cfelse>

				<cfset structInsert(result.doNotExist,fileName,"")>

			</cfif>

		</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="deleteAllLinkedImages" access="remote">
		<cfargument name="entityID" required="true">
		<cfargument name="entityTypeID" required="true">

		<cfset var result={isOK="true",message="Record deleted."}>
		<cfset var directoryPath="#application.paths.content#\linkImages\#application.entitytype[arguments.entityTypeID].tablename#\#arguments.entityID#">
		<cfset var directoryFiles="">
		<cfset var subDirectories="">

		<!--- delete assets --->
		<cfif DirectoryExists(directoryPath)>
			<!--- get list of files in directory --->
			<cfdirectory
			    directory = "#directoryPath#"
			    action = "list"
			    listInfo = "all"
			    name = "directoryFiles"
			    recurse = "yes"
			    type = "file">
			<cftry>
				<!--- delete all files in directory --->
				<cfloop query="#directoryFiles#">
					<cfset deleteFile("#directory#\#name#")>
				</cfloop>
				<!--- delete directory --->
				<cfset DirectoryDelete("#directoryPath#", true) />

				<cfcatch>
					<cfset result={isOK="false",message=cfcatch.message}>
				</cfcatch>
			</cftry>
		</cfif>
		<cfreturn result>

	</cffunction>

	<!--- 2015-02-02 ACPK CRARB001 Returns the N most downloaded files, out of all files which the user has viewing rights to --->
	<cffunction name="gettopNdownloads" access="public" returntype="query">
		<cfargument name="norequested" required="false" default="5">
		<cfargument name="FileTypeGroupID" required="false" default="0">
		<cfargument name="fromDate" required="false" default="">
		<cfargument name="toDate" required="false" default="">
		<cfargument name="daysDuration" required="false" default="0">

		<cfset startDate = DateAdd("d", -daysDuration, Now())>
		<cfset filesUserHasViewRightsTo = getFilesAPersonHasViewRightsTo(personid=request.relaycurrentuser.personid,orderby="f.fileid",returnCountryIDList=true)>

		<cfquery name="qtopNdownloads" datasource="#application.siteDataSource#">
			SELECT TOP #arguments.norequested# vactivities.entityID AS 'fileID', files.name, COUNT(*) AS 'totalDownloads'
			FROM vactivities <!---Database of user activity--->
			INNER JOIN files <!---Database of uploaded files--->
				ON vactivities.entityID=files.fileID <!---Match up records using entity/file ID--->
			INNER JOIN filetype<!---Database of file types--->
				ON files.filetypeid = filetype.filetypeid <!---Match up records using file type ID--->
			INNER JOIN filetypegroup<!---Database of file type groups--->
				ON filetype.filetypegroupid = filetypegroup.filetypegroupid <!---Match up records using file type group ID--->
			WHERE vactivities.activitytype = 'FileDownloaded' <!---Downloaded files only--->
				<cfif FileTypeGroupID NEQ 0> <!---Specified file type groups only (default: returns all)--->
					AND filetypegroup.filetypegroupid in (#arguments.FileTypeGroupID#)
				</cfif>
				<cfif fromDate NEQ ""> <!---Consider activity after specified date (default: returns all)--->
					AND vactivities.timeOfActivity >= #arguments.fromDate#
				</cfif>
				<cfif toDate NEQ ""> <!---Consider activity before specified date (default: returns all)--->
					AND vactivities.timeOfActivity <= #arguments.toDate#
				</cfif>
				<cfif daysDuration NEQ 0> <!---Consider activity from N days before today (default: returns all)--->
					AND vactivities.timeOfActivity > #startDate#
				</cfif>
				AND vactivities.entityID in (#ArrayToList(getMostSuitedFileFromFamily(originalFileQuery=filesUserHasViewRightsTo)["fileid"])#) <!---Files which current user has access to only--->
			GROUP BY vactivities.entityID, files.name <!---Group by entity ID and name--->
			ORDER BY totalDownloads DESC <!---Sort from most downloaded to least--->
		</cfquery>

		<cfreturn qtopNdownloads/>
	</cffunction>


	<!--- P-TAT005. Abstract file download notification from fileget.cfm. Overide this method in custom cfc in you wish to use email template --->
	<cffunction name="notifyResourceDownload" access="public" returntype="void">
		<cfargument name="emailTo" required="true" type="string">
		<cfargument name="emailFrom" required="true" type="string">
		<cfargument name="filename" required="true" type="string">
		<cfargument name="siteTitle" required="true" type="string">
		<cfargument name="fullname" required="true" type="string">
		<cfargument name="fileID" required="true" type="numeric">
		<cfargument name="filePath" required="true" type="string">

		<cfset var mailmsg = {}>

		<cfset mailmsg = "The file #arguments.filename# was downloaded from #arguments.siteTitle# by #arguments.fullname#.">
		<cfset mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & "FileID: #arguments.fileID#" & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & "Filename: #arguments.filename#" & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & "User: #arguments.fullname#" & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & "Download Time: #DateFormat(Now(), "mmm-dd-yyyy")# #TimeFormat(Now(),"HH:MM")#" & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & "This is just for your information.">
		<cfset mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
		<cfset mailmsg = mailmsg & "Automated message from #arguments.filePath# (Message 1)">

		<cf_mail to="#arguments.emailTo#"
				from="#arguments.emailFrom#"
				subject="#arguments.filename# Filedownloaded."><cfoutput>#mailmsg#</cfoutput>
		</cf_mail>

	</cffunction>


	<cffunction access="public" name="getFileTypeGroups" hint="" return="query">
		<cfargument name="filetypegroupids" type="string" default = "">

		<cfquery name="getFileTypeGroups" datasource="#application.siteDataSource#">
			select ftg.fileTypeGroupID, ftg.heading, ftg.intro
			from fileTypeGroup ftg
			where 1=1
				<cfif arguments.fileTypeGroupIDs neq "">
					and ftg.fileTypeGroupID IN (<cf_queryparam value="#arguments.fileTypeGroupIDs#" cfsqltype="cf_sql_varchar" list="true">)
				</cfif>
			order by ftg.heading
		</cfquery>

		<cfreturn  getFileTypeGroups>
	</cffunction>

</cfcomponent>

