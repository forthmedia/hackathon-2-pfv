<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		realyLeads.cfc
Author:
Date started:

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-09-02			RPW			Add Lead Screen on Portal
2014-09-22			AXA 		CORE-655 make sure invalid lead types are not displayed
2014-09-22			AXA			CORE-654 allow users to manually inactivate leads
2014-11-18			RPW			CORE-97 Phase 1: Extend database fields for Leads
2014-11-20			RPW			CORE-97 Added function SaveConvertLeadData - Catered for dates in different locales.
2014-11-27			RPW			CORE-97 Phase 1: Extend database fields for Leads - Added contact to EndCustomerOppContacts when converting lead to opportunity
2014-12-01			RPW			CORE-97 Phase 1: Extend database fields for Leads - Added check that ContactPersonId and partnerContactID are numeric
2015-01-28			RPW			JIRA FIFTEEN-84 - Create a new relaytag to show all leads for a company
2015-02-23			RPW			Leads Listing Accepted by Partner to display Yes, No, New translations for 1,0,NULL
2015/09/30			NJH			PROD2015-35 - added TFQO filtering for getLeads data mode
2016-01-08 			PPB 		CYB CR-004 Distributor Assigned Leads - add Distributor to merge fields for emails
2016/06/16			NJH			JIRA PROD2016-1273 - check user rights for lead.
2016/09/25          DAN         PROD2016-2387 - filter leads by type (new tag parameter)
2016/09/27          DAN         PROD2016-2398 - add progressID aka Lead Status to the leads query so it can be used in "showTheseColumns" of the tags RELAY_LEADS and RELAY_PARTNERLEADS
2016/11/23          DAN         PROD2016-2790 - fix for access rights for primary contacts and lead partner managers


Possible enhancements:

 --->
<cfcomponent displayname="relayLeads" hint="Functions for used by simple lead table.">

<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Please note this component is not to be confused with leadManager which manages the much more
	  complex opportunity management system.  This provides functison for the simpler lead data structure
	  which is used to capture leads at a higher level.
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="updateLeadTypeStatus" hint="Updates the leadTypeStatusID">
		<cfargument name="leadTypeID" type="numeric" default="1" required="yes">
		<cfargument name="leadID" type="numeric" default="1" required="yes">
		<cfargument name="leadTypeStatus" type="string" default="NewStatus" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var updateLeadTypeStatus = "";
		</cfscript>

		<cfquery name="getLeadTypeStatus" datasource="#arguments.dataSource#">
			select LeadTypeStatusID from leadTypeStatus
			where leadTypeID = <cfqueryparam value="#arguments.leadTypeID#" cfsqltype="CF_SQL_INTEGER">
			and Status = <cfqueryparam value="#arguments.leadTypeStatus#" cfsqltype="CF_SQL_VARCHAR">
		</cfquery>


		<cfquery name="updateLeadTypeStatus" datasource="#arguments.dataSource#">
			update lead set LeadTypeStatusID =  <cf_queryparam value="#getLeadTypeStatus.LeadTypeStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >
			where leadID = <cfqueryparam value="#arguments.leadID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn "set leadID #arguments.leadID# to #getLeadTypeStatus.LeadTypeStatusID#">
	</cffunction>

	<cffunction access="public" name="getPersonFromLead" hint="Updates the leadTypeStatusID">
		<cfargument name="leadID" type="numeric" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var qGetPersonFromLead = "";
		</cfscript>

		<cfquery name="qGetPersonFromLead" datasource="#arguments.dataSource#">
			select createdBy as personID from lead
			where leadid = #arguments.leadID#
		</cfquery>

		<cfreturn qGetPersonFromLead.PersonID>
	</cffunction>


	<cffunction name="getLeadMergeFields" access="public" returntype="struct">
		<cfargument name="fieldName" type="string" default="">
		<cfargument name="leadID" type="numeric">
		<cfargument name="lead" required="false">

		<cfif not structKeyExists(arguments,"lead")>
			<cfset lead = application.com.structureFunctions.queryRowToStruct(query=getLead(arguments.leadID),row=1)>
			<cfset lead.fullname = lead.firstname & " " & lead.lastname>
			<cfif isNumeric(lead.sourceID)>
				<cfset lead.Source = application.lookup[lead.sourceID].itemText>
			</cfif>
			<cfif isNumeric(lead.leadTypeID)>
				<cfset lead.type = application.lookup[lead.leadTypeID].itemText>
			</cfif>
			<cfif isNumeric(lead.progressID)>
				<cfset lead.progress = application.lookup[lead.progressID].itemText>
			</cfif>
			<cfif isNumeric(lead.approvalStatusID)>
				<cfset lead.approvalStatus = application.lookup[lead.approvalStatusID].itemText>
			</cfif>
			<cfif isNumeric(lead.countryID)>
				<cfset lead.country = application.com.relayCountries.getCountryDetails(countryID=lead.countryID).countryDescription>
			</cfif>
		</cfif>

		<cfif structKeyExists(lead,"acceptedByPartnerDate") and lead.acceptedByPartnerDate neq "">
			<cfset lead.acceptedByPartnerDate = DateFormat(lead.acceptedByPartnerDate,"dd-mmm-yyyy")>
		</cfif>

		<cfif not structKeyExists(lead,"partner")>
			<cfif not isNumeric(lead.partnerSalesPersonID)>
				<cfset lead.partnerSalesPersonID = 0>
			</cfif>
			<cfset lead.partner = application.com.commonQueries.getFullPersonDetails(lead.partnerSalesPersonID)>
		</cfif>

		<!--- START: 2016-01-08 PPB CYB CR-004 Distributor Assigned Leads --->
		<cfif not structKeyExists(lead,"distributor")>
			<cfif not isNumeric(lead.distiContactPersonID)>
				<cfset lead.distiContactPersonID = 0>
			</cfif>
			<cfset lead.distributor = application.com.commonQueries.getFullPersonDetails(lead.distiContactPersonID)>
		</cfif>
		<!--- END: 2016-01-08 PPB CYB CR-004 Distributor Assigned Leads --->

		<cfif not structKeyExists(lead,"accountManager")>
			<cfif not isNumeric(lead.vendorAccountManagerPersonID)>
				<cfset lead.vendorAccountManagerPersonID = 0>
			</cfif>
			<cfset lead.accountManager = application.com.commonQueries.getFullPersonDetails(lead.vendorAccountManagerPersonID)>
		</cfif>

		<cfif not structKeyExists(lead,"opportunity") and (structKeyExists(lead,"convertedOpportunityID") and lead.convertedOpportunityID neq "")>
			<cfset lead.opportunity = application.com.opportunity.getOpportunityMergeFields(opportunityID=lead.convertedOpportunityID)>
		</cfif>

		<cfreturn lead>
	</cffunction>


	<!--- 2014-09-02	RPW	Add Lead Screen on Portal --->
	<cffunction name="getLead" access="public" returnType="query" output="false">
		<cfargument name="leadID" type="numeric" required="true">

		<cfset var getLeadQry = "">

		<cfquery name="getLeadQry">
			select leadID,leadTypeID,firstname,lastname,company,email,address1,address2,address3,address4,address5,postalCode,countryID,officePhone,mobilePhone,website,
				progressID,sourceID,vendorAccountManagerPersonID,partnerSalesPersonID,description,rejectionReason,acceptedByPartnerDate,annualRevenue,currency,approvalStatusID,partnerLocationID,distiContactPersonId,			<!--- 2016-01-08 PPB CYB CR-004 Distributor Assigned Leads add distiContactPersonId--->
				convertedOpportunityID
			from lead
				where leadID = <cf_queryparam value="#arguments.leadID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn getLeadQry>
	</cffunction>

	<cfscript>
		//2014-08-29	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
		//2014-09-02	RPW	Add Lead Screen on Portal
		//2014-09-22	AXA CORE-655 make sure invalid lead types are not displayed
		//2015-02-23	RPW	Leads Listing Accepted by Partner to display Yes, No, New translations for 1,0,NULL
		public query function GetLeads(
			required string resultType,
			string sortOrder,
			any filter,
			numeric leadTypeID
		)
		{
			//TODO: if rights are defined then they should be added to this cfc and included application.com.rights.getRightsFilterWhereClause(entityType="Lead",alias="l")
			//2015-01-28			RPW			JIRA FIFTEEN-84 - Added leadApprovalStatus and PartnerSalesPerson to query.
			// NJH 2016/06/16 JIRA PROD2016-1273 - implement user rights.
			// NJH 2017/02/28 RT-310 - added some new columns to the cory query (translations of some lookup fields). RingCentral had tried to add them in a custom function and it was timing out due to the large number of records and using a qoq to do the adding.

			var rights = application.com.rights.getRightsFilterQuerySnippet(entityType="Lead",alias="l");
			var leadTypePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippetsForTextID(tablename="lookupList", alias="ll_type",TextIDColumn="lookupTextID",phraseTextID="itemText",entityID="lookupListID",prefix="leadType");
			var leadProgressPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippetsForTextID(tablename="lookupList", alias="ll_progress",TextIDColumn="lookupTextID", phraseTextID="itemText",entityID="lookupListID",prefix="leadProgress");
			var leadSourcePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippetsForTextID(tablename="lookupList", alias="ll_source",TextIDColumn="lookupTextID", phraseTextID="itemText",entityID="lookupListID",prefix="leadSource");
			var sql = "";

			switch(arguments.resultType) {
				case "data":
					sql &= "
						select * from (SELECT
							leadID,
							l.firstname + ' '+ l.lastname as name,
							l.company,
							c.countryDescription as country,
							l.email,
							l.created,
							CASE
								WHEN acceptedByPartner = 1 THEN 'phr_Yes'
								WHEN acceptedByPartner = 0 THEN 'phr_No'
								ELSE 'phr_New'
							END AS 'acceptedByPartner',
							convert(bit,case when convertedOpportunityID is not null then 1 else 0 end) as converted,
							ll_approvalStatus.ItemText leadApprovalStatus,
							ll_progress.ItemText leadProgressStatus,
							(
								SELECT fullname FROM Person WHERE PersonID = L.PartnerSalesPersonID
							) PartnerSalesPerson,
							officePhone as phone,
							isNull(#preserveSingleQuotes(leadTypePhraseQuerySnippets.select)#,ll_type.itemText) as leadType,
							isNull(#preserveSingleQuotes(leadProgressPhraseQuerySnippets.select)#,ll_progress.itemText) as leadProgress,
							isNull(#preserveSingleQuotes(leadSourcePhraseQuerySnippets.select)#,ll_source.itemText) as leadSource
					";
					break;
				case "count":
					sql &= "SELECT COUNT(1) numberOfLeads ";
					break;
			}

			sql &= "
				FROM dbo.lead l
				INNER JOIN country c on l.countryID = c.countryID
				LEFT OUTER JOIN lookupList ll_type on l.leadTypeID = ll_type.LookupID AND ll_type.FieldName = 'leadTypeID'
				LEFT OUTER JOIN lookupList ll_approvalStatus on l.approvalStatusID = ll_approvalStatus.LookupID AND ll_approvalStatus.FieldName = 'leadApprovalStatusID'
				LEFT OUTER JOIN lookupList ll_progress on l.progressID = ll_progress.LookupID AND ll_progress.FieldName = 'leadProgressID'
				LEFT OUTER JOIN lookupList ll_source ON l.sourceID = ll_source.LookupID AND ll_source.FieldName = 'leadSourceID'
				#preservesinglequotes(leadTypePhraseQuerySnippets.join)#
				#preservesinglequotes(leadProgressPhraseQuerySnippets.join)#
				#preservesinglequotes(leadSourcePhraseQuerySnippets.join)#
			";

		    sql &= " " & rights.join & " ";

			sql &= " WHERE l.active = 1
					 AND (l.convertedOpportunityID = '' OR l.convertedOpportunityID IS NULL)";

			if (!request.relayCurrentUser.isInternal) {
				sql &= " AND (isNull(l.approvalStatusID,0) NOT IN (:closedLeadStages) AND isNull(l.progressID,0) NOT IN (:closedLeadStages))";
			}

            if (structKeyExists(arguments,"leadTypeID") && arguments.leadTypeID > 0) {
                sql &= " AND l.leadTypeID=:leadTypeID";
            } else {
                // current default. Partner leads are leads that are created as part of partner registration, so we don't want them appearing in the listing screen (at least not now in the partner's screen)
                sql &= " AND (l.leadTypeID IS NULL OR ll_type.lookupTextID <> 'Partner') ";
            }

			if (structKeyExists(arguments,"filter") && IsArray(arguments.filter) && ArrayLen(arguments.filter)) {
				for (var f=1;f <= ArrayLen(arguments.filter);f++) {
					sql &= " AND " & arguments.filter[f] & " ";
				}
			}

			if (structKeyExists(rights,'whereClause')) {
				sql &= " " & rights.whereClause & " ";
			}


			//NJH PROD2015-35 - added TFQO filtering when in data mode
			if (arguments.resultType eq 'data') {
				useWholeWordsOnly = false;
				savecontent variable="tfqoFilter" { include "../templates/tableFromQuery-QueryInclude.cfm"; }
				sql &= ") as baseData where 1=1 " & tfqoFilter;
			}

			switch(arguments.resultType) {
				case "data":
					sql &= " ORDER BY " & arguments.sortOrder;
					break;
			}

			var qObj = new query();
			qObj.setDatasource=(application.siteDataSource);
			qObj.setSql(sql);
			if (structKeyExists(arguments,"leadTypeID") && arguments.leadTypeID > 0) {
    			qObj.addParam(name="leadTypeID",value=arguments.leadTypeID,cfsqltype="cf_sql_integer");
			}

			var closedLeadStages = application.com.settings.getSetting("leadManager.closedLeadStages");
			qObj.addParam(name="closedLeadStages",value=closedLeadStages,cfsqltype="cf_sql_integer", list="yes");

			/*if (!request.relayCurrentUser.isInternal) {
				qObj.addParam(name="partnerSalesPersonID",cfsqltype="CF_SQL_INTEGER",value=request.relayCurrentUser.personid);
			}*/
			var qGetLeads = qObj.execute().getResult();

			return qGetLeads;
		}


		//2015-01-28	RPW	JIRA FIFTEEN-84 - Added function GetOrganisationLeads
		//2015-02-23	RPW	Leads Listing Accepted by Partner to display Yes, No, New translations for 1,0,NULL
		/* NJH 2016/11/25 - commented out as exact same as above function
		* public query function GetOrganisationLeads
		(
			required numeric organisationID,
			string sortOrder="leadID DESC",
			numeric leadTypeID
		)
		{
			var sql = "
				SELECT leadID
					,l.firstname + ' ' + l.lastname AS NAME
					,l.company
					,c.countryDescription AS country
					,l.email
					,l.created
					,CASE
						WHEN acceptedByPartner = 1 THEN 'phr_Yes'
						WHEN acceptedByPartner = 0 THEN 'phr_No'
						ELSE 'phr_New'
						END AS 'acceptedByPartner'
					,convert(BIT, CASE
							WHEN convertedOpportunityID IS NOT NULL
								THEN 1
							ELSE 0
							END) AS converted
					,lkas.ItemText leadApprovalStatus
					,lProgress.ItemText leadProgressStatus
					,(
						SELECT FirstName + ' ' + LastName
						FROM Person
						WHERE PersonID = L.PartnerSalesPersonID
						) PartnerSalesPerson
				FROM dbo.lead l
				INNER JOIN country c ON l.countryID = c.countryID
				LEFT JOIN lookupList lk ON l.leadTypeID = lk.LookupID
				LEFT JOIN lookupList lkas ON l.approvalStatusID = lkas.LookupID
				LEFT JOIN lookupList lProgress ON l.progressID = lProgress.LookupID
				WHERE 1 = 1
					AND active = 1
					AND (
						l.convertedOpportunityID = ''
						OR l.convertedOpportunityID IS NULL
						)
					AND partnerLocationID IN (SELECT locationid FROM location WHERE OrganisationID = :OrganisationID)
			";
			if (structKeyExists(arguments,"leadTypeID") && arguments.leadTypeID > 0) {
			    sql &= " AND l.leadTypeID=:leadTypeID";
			}

			sql &= "
				ORDER BY LeadID DESC
			";

			var qObj = new query();
			qObj.setDatasource=(application.siteDataSource);
			qObj.setSql(sql);
			qObj.addParam(name="OrganisationID",cfsqltype="CF_SQL_INTEGER",value=arguments.organisationID);
			if (structKeyExists(arguments,"leadTypeID") && arguments.leadTypeID > 0) {
                qObj.addParam(name="leadTypeID",value=arguments.leadTypeID,cfsqltype="cf_sql_integer");
            }

			var qGetLeads = qObj.execute().getResult();

			return qGetLeads;
		}*/

		//2014-09-02			RPW			Add Lead Screen on Portal
		public query function GetPerson(
			required numeric personID
		)
		{
			var sql = "
				SELECT FirstName,LastName,LocationId
				FROM Person
				WHERE PersonID = :PersonID
			";
			var qObj = new query();
			qObj.setDatasource=(application.siteDataSource);
			qObj.setSql(sql);
			qObj.addParam(name="PersonID",cfsqltype="CF_SQL_INTEGER",value=arguments.PersonID);

			var qGetPerson = qObj.execute().getResult();

			return qGetPerson;
		}

	</cfscript>
	<!--- 2014-09-22 AXA CORE-654 allow users to manually inactivate leads --->
	<cffunction name="setActiveLead" access="public" output="false" returntype="void" hint="CORE-654 allow users to manually inactivate leads">
		<cfargument name="leadIDs" type="string" required="true" />
		<cfif listLen(arguments.leadIDs) GT 0>
			<cfquery name="updActiveLead" datasource="#application.siteDataSource#">
				UPDATE lead
					set active = 0
				WHERE leadID IN (<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.leadIDs#" list="true">)
			</cfquery>
		</cfif>
	</cffunction>


	<cffunction name="convertLead" access="public" output="false" returnType="struct">
		<cfargument name="leadId" type="numeric" required="true">

		<cfset var result = {isOK=true,message="",opportunityID=0}>
		<cfset var leadMappingStruct = {person={},location={},opportunity={}}>

		<cfset leadMappingStruct.person = {firstname="firstname",lastname="lastname",email="email",salutation="salutation",notes="lead.description",officePhone="officePhone",mobilePhone="mobilePhone"}>
		<cfset leadMappingStruct.location = {sitename="company",address1="address1",address2="address2",address4="address4",address5="address5",postalCode="postalCode",countryId="countryID",accountTypeId=application.organisationType.endCustomer.organisationTypeID,specificUrl="website"}>
		<cfset leadMappingStruct.organisation = {organisationName="company",countryID="countryID",organisationTypeID=application.organisationType.endCustomer.organisationTypeID}>
		<!--- 2014-11-18	RPW	CORE-97 Fixed references to GetStageID and getOppTypeID --->
		<cfset leadMappingStruct.opportunity = {currentSituation="lead.description",countryID="countryID",distiLocationID="distiLocationID",partnerLocationID="partnerLocationID",partnerSalesPersonID="partnerSalesPersonID",currency="currency",vendorAccountManagerPersonID="vendorAccountManagerPersonID",stageID=application.com.opportunity.getStageID(stageTextID='oppStageLEAD').ID,oppTypeID=application.com.opportunity.getOppTypeID(oppTypeTextID='Deals'),campaignID="campaignID",sourceId="oppSource.opportunitySourceID"}>

		<cfset var usedFieldNames = "">
		<cfset var tempTable = "####leadToConvert">
		<cfset var entityType = "">
		<cfset var getLeadToEntityProfiles = "">


		<!--- Move the mapping of profiles over to a trigger so that profiles are moved when a lead is converted via the Connector
		<cfloop list="#structKeyList(leadMappingStruct)#" index="entityType">
			<cfquery name="getLeadToEntityProfiles">
				select leadFlags.flagGroupTextID as leadFlagGroup, entityFlags.flagGroupTextID as entityFlagGroup
					,leadFlags.flagTextID as leadFlag, entityFlags.flagTextID as entityFlag
					,entityFlags.dataTable
				from vFlagDef leadFlags
					inner join vFlagDef entityFlags on replace(leadFlags.flagTextID,'lead_','') = replace(entityFlags.flagTextID,'#left(entityType,3)#_','') and leadFlags.dataType = entityFlags.dataType
				where leadFlags.flagTextId like 'lead[_]%'
					and entityFlags.flagTextID like '#left(entityType,3)#[_]%'
			</cfquery>

			<!--- add any profiles to the mapping struct --->
			<cfloop query="getLeadToEntityProfiles">
				<cfif dataTable eq "boolean">
					<cfset leadMappingStruct[entityType][entityFlagGroup] = "replace(#leadFlagGroup#,'lead_','#left(entityType,3)#_')"> <!--- if a boolean flag, then the text IDs are in the field --->
				<cfelse>
					<cfset leadMappingStruct[entityType][entityFlag] = leadFlag>
				</cfif>
			</cfloop>
		</cfloop> --->

		<cfquery name="createTempTable">
			if object_id('tempdb..#tempTable#') is not null
			begin
				drop table #tempTable#
			end

			select
			<cfloop collection="#leadMappingStruct#" item="mappedEntity">
				<cfloop collection="#leadMappingStruct[mappedEntity]#" item="fieldname">
					<cfif not listFindNoCase(usedFieldNames,fieldname)>
						<cfset usedFieldnames = listAppend(usedFieldNames,fieldname)>
						<cfset var selectLeadField = leadMappingStruct[mappedEntity][fieldname]>
						<cfif not listLen(usedFieldNames) eq 1>,</cfif>#preserveSingleQuotes(selectLeadField)# as #fieldname#
					</cfif>
				</cfloop>
				,null as #mappedEntity#Id
			</cfloop>
			,null as contactPersonID
			,null as entityID
			,'Converted From Lead' as detail
			,dateAdd(month, 1, getDate()) as expectedCloseDate
			,<cf_queryparam value="#arguments.leadID#" cfsqltype="cf_sql_integer"> as leadID
			<!--- ,<cf_queryparam value="#arguments.leadID#" cfsqltype="cf_sql_integer"> as convertedFromLeadID ---> <!--- shouldn't need this column as there should be a trigger setting it --->
			into #tempTable#
			from vlead lead
				left join lookupList lsource on lsource.lookupId = lead.sourceID
				left join oppSource on rtrim(ltrim(oppSource.opportunitySource)) = rtrim(ltrim(lsource.itemText))
			where leadID = <cf_queryparam value="#arguments.leadID#" cfsqltype="cf_sql_integer">
				and convertedOpportunityID is null

			select * from #tempTable#
		</cfquery>

		<!--- matching!!! --->
		<cfset var additionalMatchColumns = "">
		<cfloop list="organisation,location,person" index="entityType">
			<cfset additionalMatchColumns = "">

			<!--- only match to end customer accounts --->
			<!--- NJH 2016/12/12 JIRA PROD2016-2888 - remove this condition. Let's be consistent and the user can add the field to the matching if necessary
			<cfif entityType eq "organisation">
				<cfset additionalMatchColumns = "organisationTypeID">
			<cfelseif entityType eq "location">
				<cfset additionalMatchColumns = "accountTypeID">
			</cfif>--->
			<cfset application.com.matching.matchEntityRecords(tablename=tempTable,entityType=entityType,uniqueIdColumnName="leadID",additionalMatchColumns=additionalMatchColumns)>
		</cfloop>

		<cfset var createEntity = "">

		<cfif createTempTable.recordCount neq 0>
			<cfquery name="createEntity">
				<!--- order is important, so doing a hard-coded list rather than structKeyList --->
				<cfloop list="organisation,location,person,opportunity" index="entityType">
				exec entityUpsert @upsertTablename=#tempTable#, @entityname='#entityType#',@lastUpdatedByPerson=#request.relayCurrentUser.personId#, @returnTableName=##leadToConvert_return <!--- have a return table so that we don't get any return from the SP --->

				<cfif entityType eq "organisation">
					update #tempTable# set entityID = organisationID
				<cfelseif entityType eq "person">
					update #tempTable# set contactPersonID = personID
				</cfif>

				<cfif entityType neq "organisation">
				update lead
					set converted#entityType#ID = t.#entityType#ID,
					lastUpdated = getDate(),
					lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupId#" cfsqltype="cf_sql_integer">
				from
					lead l inner join #tempTable# t on l.leadID = t.leadID
				</cfif>
				</cfloop>

				update lead
					set progressID = ll.lookupID,
					lastUpdated = getDate(),
					lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupId#" cfsqltype="cf_sql_integer">
				from
					lead l
						inner join #tempTable# t on l.leadID = t.leadID
						join lookupList ll on 1=1
				where
					rtrim(ltrim(ll.lookupTextId))='ClosedConverted'
					and ll.fieldname='leadProgressID'
			</cfquery>

			<cfquery name="createEntity">
				select convertedOpportunityID from lead where leadID=<cf_queryparam value="#arguments.leadID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfset result.opportunityID = createEntity.convertedOpportunityID>
			<cfset var addTabLink = "openNewTab('Edit Opportunity #result.opportunityID#','Edit Opportunity #result.opportunityID#','/opportunity/edit.cfm?opportunityID=#result.opportunityID#',{reuseTab:true,iconClass:'opportunity'}); return false;">
			<cfset result.message = "Successfully converted Lead (Converted OpportunityID: #result.opportunityID#)">
			<cfset var qLeadDetails = application.com.relayLeads.getLead(arguments.leadID) />
			<cfset var leadManagerID = qLeadDetails.vendorAccountManagerPersonID />
			<cfset var partnerContactID = qLeadDetails.partnerSalesPersonID />
			<cfset var mergeStruct = {leadID=arguments.leadId,opportunityid=result.opportunityID} /> <!---2016/07/20 GCC 450978 - add opportunityid so that opportunity merge fields in email work --->
			<cfif isNumeric(leadManagerID)>
				<cfset application.com.email.sendEmail(personID=leadManagerID,emailTextID="LeadConverted",mergeStruct=mergeStruct)>
			</cfif>
			<cfif isNumeric(partnerContactID)>
				<cfset application.com.email.sendEmail(personID=partnerContactID,emailTextID="LeadConverted",mergeStruct=mergeStruct)>
			</cfif>

			<!--- 2014-11-27	RPW	CORE-97 Phase 1: Extend database fields for Leads - Added contact to EndCustomerOppContacts when converting lead to opportunity --->
			<!--- 2014-12-01	RPW	CORE-97 Phase 1: Extend database fields for Leads - Added check that ContactPersonId and partnerContactID are numeric --->
			<cfscript>
				var opportunityDetails = application.com.opportunity.getOpportunityDetails(opportunityID=createEntity.convertedOpportunityID);
				if (IsNumeric(opportunityDetails.ContactPersonId[1]) && opportunityDetails.ContactPersonId[1] && IsNumeric(partnerContactID) && partnerContactID) {
					application.com.flag.setFlagData(flagId='EndCustomerOppContacts',entityid=opportunityDetails.ContactPersonId[1],data=partnerContactID);
				}
			</cfscript>

		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="delete" access="public" output="false" returntype="any">
		<cfargument name="leadId" type="numeric" required="true">

		<cfscript>
			local.stEntity = {};
			local.stEntity['entityID'] = arguments.leadId;
			local.stEntity['entityTypeID'] = application.entityTypeID['lead'];
			result = application.com.relayEntity.deleteEntity(argumentCollection=local.stEntity,hardDelete=false);
		</cfscript>

		<cfreturn result />
	</cffunction>

		<!--- 2014-11-18	RPW	CORE-97 Added function SaveConvertLeadData --->
		<cfscript>
		public struct function SaveConvertLeadData
		(
			string detail="",
			required date expectedCloseDate,
			required numeric OppCustomerTypeID,
			required numeric stageID,
			required numeric budget
		)
		{
			var result = {isOK=true,message="",opportunityID=0};

			if (IsDate(arguments.expectedCloseDate) && ListLen(arguments.expectedCloseDate,"/")==3) {
				try {
					// 2014-11-20	RPW	CORE-97 Added function SaveConvertLeadData - Catered for dates in different locales.
					var dateLocale = request.relaycurrentuser.localeInfo.dateType;

					switch(dateLocale) {
						case "US":
							var day = ListGetAt(arguments.expectedCloseDate,2,"/");
							var month = ListGetAt(arguments.expectedCloseDate,1,"/");
							var year = ListGetAt(arguments.expectedCloseDate,3,"/");
							break;
						case "EURO":
							var day = ListGetAt(arguments.expectedCloseDate,1,"/");
							var month = ListGetAt(arguments.expectedCloseDate,2,"/");
							var year = ListGetAt(arguments.expectedCloseDate,3,"/");
							break;
						default:
							var day = ListGetAt(arguments.expectedCloseDate,2,"/");
							var month = ListGetAt(arguments.expectedCloseDate,1,"/");
							var year = ListGetAt(arguments.expectedCloseDate,3,"/");
							break;
					}

					var ODBCexpectedCloseDate = CreateODBCDateTime(CreateDateTime(year,month,day,0,0,0));
				}
				catch(any excpt) {
					var ODBCexpectedCloseDate = "";
				}
			} else {
				var ODBCexpectedCloseDate = "";
			}

			var sql = "
				DECLARE @UpdateResult TABLE (
					opportunityID INT
				)

				UPDATE opportunity SET
				detail = :detail,
				expectedCloseDate = :expectedCloseDate,
				OppCustomerTypeID = :OppCustomerTypeID,
				stageID = :stageID,
				estimatedBudget = :budget
				OUTPUT INSERTED.opportunityID INTO @UpdateResult
				WHERE opportunityID = :opportunityID
				SELECT opportunityID FROM @UpdateResult
			";

			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="detail",cfsqltype="CF_SQL_VARCHAR",value=arguments.detail,null=!Len(arguments.detail));
			qObj.addParam(name="expectedCloseDate",cfsqltype="CF_SQL_TIMESTAMP",value=ODBCexpectedCloseDate,null=!IsDate(ODBCexpectedCloseDate));
			qObj.addParam(name="OppCustomerTypeID",cfsqltype="CF_SQL_INT",value=arguments.OppCustomerTypeID,null=!IsNumeric(arguments.OppCustomerTypeID));
			qObj.addParam(name="stageID",cfsqltype="CF_SQL_INT",value=arguments.stageID,null=!IsNumeric(arguments.stageID));
			qObj.addParam(name="budget",cfsqltype="CF_SQL_NUMERIC",value=arguments.budget,null=!Len(arguments.budget));
			qObj.addParam(name="opportunityID",cfsqltype="CF_SQL_INT",value=arguments.opportunityID,null=!IsNumeric(arguments.opportunityID));

			var updateResult = qObj.execute(sql=sql).getResult();

			if (updateResult.recordCount && updateResult.opportunityID) {

				result.opportunityID = updateResult.opportunityID[1];

			} else {
				result.isOK = false;
				result.message = "Data Fields not Updated";
			}

			return result;
		}
	</cfscript>


	<!--- NJH 2016/06/16 JIRA PROD2016-1273 - add function to check user rights. Deals with both internal and external users --->
	<cffunction name="getUserRightsForLead" access="public" output="false" returnType="struct" hint="Returns access rights for leads">
		<cfargument name="leadID" type="numeric" required="true" hint="The lead ID">
		<cfargument name="personID" type="numeric" required="true" hint="The person ID">

		<cfset var rightsStruct = {edit=true,view=true}>

		<cfset var filterArray = ["leadID="&arguments.leadID]>
		<cfset var leadQry = GetLeads(filter=filterArray,resultType="count")>
		<cfif leadQry.numberOfLeads eq 0>
			<cfset var rightsStruct = {edit=false,view=false}>
		</cfif>

		<cfreturn rightsStruct>
	</cffunction>

</cfcomponent>
