<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			xmlFunctions.cfc
Author:				WAB
Date started:		2009/06
	
Description:			
Some useful functions

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2010/07/14	Changed functioning of the lowercase attribute in readAndParseXMLFile - now lowercases the xmlname.  Reg Exp may need adjusting for some special cases - I have only done a simple document without CDATA
WAB 2011/11/03  added function sortXMLChildrenByAttributeValueOrXMLName()
Possible enhancements:
WAB 2011/12/05  Mods to runFunctionAgainstNodes() so calls function an extra time after a node's children (with mode = end)
WAB 2011/12/05  Also change to how additionalArgs was passed around - I think modified to how it was supposed to work
WAB 2015-03-10	added areXMLNodesIdentical function originally for use in iisAdmin.cfc
 --->

<cfcomponent >
<!--- 
WAB
Reads an XML File
 --->
 <cffunction name="readAndParseXMLFile" output="false" >
 	<cfargument name="fileNameAndPath" required = true>
 	<cfargument name="changeCase_Name" default = "">  <!--- make all xmlnames upper or lower case --->
 	<cfargument name="changeCase_Attribute" default = "">  <!--- make all xmlattribute names upper or lower case (note that changing the doc type encoding and version to upper case can break it)--->
	<cfargument name="saveMixedCaseNameAsAttribute" type="boolean" default = "false">
	<cfargument name="charset" default = "utf-8">
	
	<cfset var XMLContent = "">
	<cfset var result = {isOK=true,xml=""}>
	<cfset var lowerUpper = "">

		<cfif not fileExists (fileNameAndPath)>
			<cfset result.isok = false>
			<cfset result.message = "File #filenameAndPath# does not exist">
		<cfelse>
				
			<cftry>
				<!--- NYB ?temp patch --->
				<cftry>
					<cffile action="READ" file="#fileNameAndPath#" variable="XMLContent" charset="#charset#">
					<cfcatch>
						<cffile action="READ" file="#fileNameAndPath#" variable="XMLContent">
					</cfcatch>
				</cftry>

				<cfif changeCase_Name is not "">
					<!--- LID4086 NJH 2010/09/20 - added the ? after the !. This forces it to ignore anything with a ?
					 in it, such as the first line of the relay var xml '<?xml version="1.0" encoding="utf-16" ?>' --->
					<cfset lowerUpper = ucase(left(changeCase_Name,1))> <!--- this gives us U or L for use in the regExp --->
					<cfif saveMixedCaseNameAsAttribute>
						<cfset XMLContent = rereplace (XMLContent,"<([^/!?].*?)([ >])",'<\#lowerUpper#\1\E mixedCaseXMLName="\1"\2',"ALL")>
						<cfset XMLContent = rereplace (XMLContent,"<(/.*?)([ >])",'<\#lowerUpper#\1\2',"ALL")>		
					<cfelse>
						<cfset XMLContent = rereplace (XMLContent,"<(.*?)([ >])",'<\#lowerUpper#\1\2',"ALL")>		
					</cfif>
				</cfif> 
			
				<cfif changeCase_Attribute is not "">
					<cfset lowerUpper = ucase(left(changeCase_Attribute,1))> <!--- this gives us U or L for use in the regExp --->
					<cfset XMLContent = rereplace (XMLContent,' ([A-Za-z0-9]*?)="([^"]*?)"',' \#lowerUpper#\1\E="\2"',"ALL")>		
				</cfif>
		
				<cfset result.xml = xmlparse(XMLContent)>
				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.message = "Error reading XML document #fileNameAndPath#<BR>#cfcatch.Detail#">
				</cfcatch>
			</cftry>
		
		</cfif>

	<cfreturn result>

 </cffunction>

<cffunction name="saveXMLDoc" output="false">
 	<cfargument name="fileNameAndPath" required = true>
 	<cfargument name="XMLDOC" required = true>
	<cfargument name="charset" default = "utf-8">

	<cfset var result = {isOK = true}>
	<cfset var XMLString = toString(xmlDoc)>
	
	<cffile action="write" file="#fileNameAndPath#" output="#XMLString#" charset="#charset#">	

	<cfreturn result>

</cffunction>

 <!--- 
 WAB
 Works up through an XML doc making a list of the parents
 --->	
<cffunction name="getXMLParentList" output="false">
	<cfargument name = "XMLNode">
	<cfargument name = "delimiter" default = ",">
	<cfargument name = "stopAt" default = "##document">
	<cfargument name = "appendSelf" default = "false">
	
	
	<cfif structKeyExists (XMLNode, "xmlParent") and XMLNode.xmlParent.xmlName is not arguments.StopAt>
		<cfreturn listappend (getXMLParentList(xmlNode = XMLNode.xmlParent,stopat= arguments.stopat,delimiter= arguments.delimiter,appendSelf = false),XMLNode.xmlParent.xmlName,arguments.delimiter) & iif(appendSelf,de(delimiter),de("")) & iif(appendSelf,de(xmlNode.xmlname),de(""))>
	<cfelse>
		<cfreturn "">
	</cfif>
	
</cffunction>


<!---
WAB Settings Project
Searches from current node upwards until finds a node which matches the pattern and then returns that node
eg use: "self::node()[@myattribute='true']" to find the parent node with myattribute set to true

 --->
<cffunction name="XMLSearchParents" output="false">
	<cfargument name = "XMLNode">
	<cfargument name = "XMLSearchPattern">
	
	
		<cfset var searchResult = XMLSearch(XMLNode,XMLSearchPattern)>

		<cfif arrayLen(searchResult)>
			<cfreturn searchResult>
		<cfelseif structKeyExists (XMLNode, "xmlParent")>
			<cfreturn XMLSearchParents(XMLNode.xmlParent,xmlSearchPattern)>
		<cfelse>
			<cfreturn arrayNew(1)>
		</cfif>

	
</cffunction>


<!--- 
 WAB
 Works up through an XML doc until it reaches the top
 I often seemed to pass round bits of xml docs and then need to know what the parent doc was
 I'm sure that there must be a built in way of doing this, but I have not stumbled across it
 --->	
<cffunction name="getXMLDocObjectFromNode" output="false">
	<cfargument name = "XMLNode">
	
	
	<cfif XMLNode.xmlname is not "##document" >
		<cfreturn getXMLDocObjectFromNode(xmlnode.xmlParent)>
	<cfelse>
		<cfreturn XMLNode>
	</cfif>
	
</cffunction>


 <!--- 
*********************************
Library function
*********************************
 --->

 <cffunction name="ConvertXmlToStruct" access="public" returntype="struct" output="true"
				hint="Parse raw XML response body into ColdFusion structs and arrays and return it.">
	<cfargument name="xmlNode" type="string" required="true" />
	<cfargument name="str" type="struct" required="true" />
	<!---Setup local variables for recurse: --->
	<cfset var i = 0 />
	<cfset var axml = arguments.xmlNode />
	<cfset var astr = arguments.str />
	<cfset var bstr = structNew() />
	<cfset var n = "" />
	<cfset var tmpContainer = "" />
	<cfset var at_list = '' />
	<cfset var attrib_list = '' />
	<cfset var atr = '' />
	<cfset var attrib = '' />	

	<cfset axml = XmlSearch(XmlParse(arguments.xmlNode),"/node()")>
	<cfset axml = axml[1] />
	<!--- For each children of context node: --->
	<cfloop from="1" to="#arrayLen(axml.XmlChildren)#" index="i">
		<!--- Read XML node name without namespace: --->
		<cfset n = replace(axml.XmlChildren[i].XmlName, axml.XmlChildren[i].XmlNsPrefix&":", "") />
		<!--- If key with that name exists within output struct ... --->
		<cfif structKeyExists(astr, n)>
			<!--- ... and is not an array... --->
			<cfif not isArray(astr[n])>
				<!--- ... get this item into temp variable, ... --->
				<cfset tmpContainer = astr[n] />
				<!--- ... setup array for this item beacuse we have multiple items with same name, ... --->
				<cfset astr[n] = arrayNew(1) />
				<!--- ... and reassing temp item as a first element of new array: --->
				<cfset astr[n][1] = tmpContainer />
			<cfelse>
				<!--- Item is already an array: --->
				
			</cfif>
			<cfif arrayLen(axml.XmlChildren[i].XmlChildren) gt 0>
					<!--- recurse call: get complex item: --->
					<cfset astr[n][arrayLen(astr[n])+1] = ConvertXmlToStruct(axml.XmlChildren[i], structNew()) />
			<cfelse>
					<!--- else: assign node value as last element of array: --->
					<cfset astr[n][arrayLen(astr[n])+1] = axml.XmlChildren[i].XmlText />
			</cfif>
		<cfelse>
			<!---
				This is not a struct. This may be first tag with some name.
				This may also be one and only tag with this name.
			--->
			<!---
					If context child node has child nodes (which means it will be complex type): --->
			<cfif arrayLen(axml.XmlChildren[i].XmlChildren) gt 0>
				<!--- recurse call: get complex item: --->
				<cfset astr[n] = ConvertXmlToStruct(axml.XmlChildren[i], structNew()) />
			<cfelse>
				<cfif IsStruct(aXml.XmlAttributes) AND StructCount(aXml.XmlAttributes)>
					<cfset at_list = StructKeyList(aXml.XmlAttributes)>
					<cfloop from="1" to="#listLen(at_list)#" index="atr">
						 <cfif ListgetAt(at_list,atr) CONTAINS "xmlns:">
							 <!--- remove any namespace attributes--->
							<cfset Structdelete(axml.XmlAttributes, listgetAt(at_list,atr))>
						 </cfif>
					 </cfloop>
					 <!--- if there are any atributes left, append them to the response--->
					 <cfif StructCount(axml.XmlAttributes) GT 0>
						 <cfset astr['_attributes'] = axml.XmlAttributes />
					</cfif>
				</cfif>
				<!--- else: assign node value as last element of array: --->
				<!--- if there are any attributes on this element--->
				<cfif IsStruct(aXml.XmlChildren[i].XmlAttributes) AND StructCount(aXml.XmlChildren[i].XmlAttributes) GT 0>
					<!--- assign the text --->
					<cfset astr[n] = axml.XmlChildren[i].XmlText />
						<!--- check if there are no attributes with xmlns: , we dont want namespaces to be in the response--->
					 <cfset attrib_list = StructKeylist(axml.XmlChildren[i].XmlAttributes) />
					 <cfloop from="1" to="#listLen(attrib_list)#" index="attrib">
						 <cfif ListgetAt(attrib_list,attrib) CONTAINS "xmlns:">
							 <!--- remove any namespace attributes--->
							<cfset Structdelete(axml.XmlChildren[i].XmlAttributes, listgetAt(attrib_list,attrib))>
						 </cfif>
					 </cfloop>
					 <!--- if there are any atributes left, append them to the response--->
					 <cfif StructCount(axml.XmlChildren[i].XmlAttributes) GT 0>
						 <cfset astr[n&'_attributes'] = axml.XmlChildren[i].XmlAttributes />
					</cfif>
				<cfelse>
					 <cfset astr[n] = axml.XmlChildren[i].XmlText />
				</cfif>
			</cfif>
		</cfif>
	</cfloop>
	<!--- return struct: --->
	<cfreturn astr />
</cffunction>


<cfscript>
/**
* Merges one xml document into another
* Fix sent in by Scott Talmsa
* 
* @param xml1      The XML object into which you want to merge (Required)
* @param xml2      The XML object from which you want to merge (Required)
* @param overwriteNodes      Boolean value for whether you want to overwrite (default is true) (Optional)
* @return void (changes the first XML object) 
* @author Nathan Dintenfass (nathan@changemedia.com) 
* @version 2, October 15, 2008 
* WAB 2010/10/24 discovered that did not work for nodes where the XMLName started with _.  The structure notation did not work and had to replace all instances with XMLSearch Instead
*/
function xmlMerge(xml1,xml2){
    var readNodeParent = arguments.xml2;
    var writeNodeList = arguments.xml1;
    var writeNodeDoc = arguments.xml1;
    var readNodeList = "";
    var writeNode = "";
    var readNode = "";
    var nodeName = "";
    var ii = 0;
    var writeNodeOffset = 0;
    var toAppend = 0;
    var nodesDone = structNew();
    var tempxmlsearchResult = ""; 
    //by default, overwrite nodes
    var overwriteNodes = true;
    //if there's a 3rd arguments, that's the overWriteNodes flag 
	if (structKeyExists(arguments,"overwriteNodes"))
        overwriteNodes = arguments.overwriteNodes;
    //if there's a 4th argument, it's the DOC of the writeNode -- not a user pro`ded argument -- just used when doing recursion, so we know the original XMLDoc object
	if (structKeyExists(arguments,"writeNodeDoc"))
        writeNodeDoc = arguments.writeNodeDoc;
    //if we are looking at the whole document, get the root element
    if(isXMLDoc(arguments.xml2))
        readNodeParent = arguments.xml2.xmlRoot;
    //if we are looking at the whole Doc for the first element, get the root element
    if(isXMLDoc(arguments.xml1))
        writeNodeList = arguments.xml1.xmlRoot;    
    //loop through the readNodeParent (recursively) and override all xmlAttributes/xmlText in the first document with those of elements that match in the second document
    for(nodeName in readNodeParent){
        writeNodeOffset = 0;
        //if we haven't yet dealt with nodes of this name, do it
        if (NOT structKeyExists(nodesDone,nodeName)){
	        /* WAB 2010/10/24 replaced with XMLSearch Below    
	        readNodeList = readNodeParent[nodeName]; */
            readNodeList  = xmlsearch(readNodeParent,"./#nodeName#"); 

            //if there aren't any of this node, we need to append however many there are
	        /* WAB 2010/10/24 replaced with XMLSearch Below    
			 if  (NOT structKeyExists(writeNodeList,nodeName)){ */
            if  (NOT arraylen(xmlsearch(writeNodeList,"./#nodeName#"))) {             
                toAppend = arrayLen(readNodeList);
            }
            //if there are already at least one node of this name
            else{
                //if we are overwriting nodes, we need to append however many there are minus however many there were (if there none new, it will be 0)
                if(overWriteNodes){
	                    toAppend = arrayLen(readNodeList) - arrayLen(writeNodeList[nodeName]);
                }
                //if we are not overwriting, we need to add however many there are
                else{
                    toAppend = arrayLen(readNodeList);
                    //if we are not overwriting, we need to make the offset of the writeNode equal to however many there already are
                    writeNodeOffset = arrayLen(writeNodeList[nodeName]);
                }
            }
            //append however many nodes necessary of the name
            for(ii = 1; ii LTE toAppend; ii = ii + 1){
                arrayAppend(writeNodeList.xmlChildren,xmlElemNew(writeNodeDoc,nodeName));
            }
            //loop through however many of this nodeName there are, writing them to the writeNodes
            for(ii = 1; ii LTE arrayLen(readNodeList); ii = ii + 1){
                /*  WAB 2010/10/24 replaced with XMLSearch Below    
                writeNode = writeNodeList[nodeName][ii + writeNodeOffset]; */
				tempXMLSearchResult  = xmlsearch(writeNodeList,"./#nodeName#") ;
				writeNode = tempXMLSearchResult [ii + writeNodeOffset];
                readNode = readNodeList[ii];
                //set the xmlAttributes and xmlText to this child's values
                writeNode.xmlAttributes = readNode.xmlAttributes;                
                //deal with the CDATA scenario to properly preserve (though, if it contains CDATA and text nodes, this won't work!!
                if(arrayLen(readNode.xmlNodes) AND XmlGetNodeType(readNode.xmlNodes[1]) is "CDATA_SECTION_NODE"){
                    writeNode.xmlCData = readNode.xmlcdata;
                }
                else{
                    //modify to check to see if it's cData or not
                    writeNode.xmlText = readNode.xmlText;
                }
                //if this element has any children, recurse
                if(arrayLen(readNode.xmlChildren)){

                    xmlMerge(xml1=writeNode,xml2=readNode,overwritenodes=overwriteNodes,writeNodeDoc=writeNodeDoc);

					
                }
            }
            //add this node name to those nodes we have done -- we need to do this because an XMLDoc object can have duplicate keys
            nodesDone[nodeName] = true;
        }
    }
}
</cfscript>

<cffunction name="XmlAppend" access="public" returntype="any" output="false"  hint="Copies the children of one node to the node of another document.">

<!--- Define arguments. --->
	<cfargument name="NodeA" type="any" required="true" hint="The node whose children will be added to." />
 	<cfargument name="NodeB" type="any" required="true" hint="The node whose children will be copied to another document."/>
	<cfargument name="includeParent" type="any" default="true" hint="copies the parent as well"/>
	<cfargument name="position" type="numeric" default="0" hint="position to insert at"/>
 
 
 	<!--- Set up local scope. --->
	<cfset var LOCAL = StructNew() />

 
 	<cfif includeParent>

		<cfset local.nodeToCopy = ARGUMENTS.NodeB.cloneNode(JavaCast( "boolean", true ))>
		<cfset local.nodeToCopy = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(	local.nodeToCopy,	JavaCast( "boolean", true )		) >
		<cfset ARGUMENTS.NodeA.AppendChild(local.nodeToCopy) />

		<!--- seemed unable to insert at a position directly, so insert at end and then copy and delete if necessary --->
		<cfif position is not 0 and position lt arrayLen(ARGUMENTS.NodeA.XMLChildren)>
			<cfset arrayInsertAt(ARGUMENTS.NodeA.XMLChildren,position,ARGUMENTS.NodeA.XMLChildren[arrayLen(ARGUMENTS.NodeA.XMLChildren)]) /> 
			<cfset arrayDeleteAt (ARGUMENTS.NodeA.XMLChildren,arrayLen(ARGUMENTS.NodeA.XMLChildren))>
		</cfif>	
	<cfelse>
 

<!---
Get the child nodes of the originating XML node.
This will return both tag nodes and text nodes.
We only want the tag nodes.
--->
<cfset LOCAL.ChildNodes = ARGUMENTS.NodeB.GetChildNodes() />
 
<!--- Loop over child nodes. --->
	<cfloop index="LOCAL.ChildIndex" from="1" to="#LOCAL.ChildNodes.GetLength()#" step="1">

<!---
Get a short hand to the current node. Remember
that the child nodes NodeList starts with
index zero. Therefore, we must subtract one
from out child node index.
--->
		<cfset LOCAL.ChildNode = LOCAL.ChildNodes.Item(	JavaCast("int",		(LOCAL.ChildIndex - 1)		)	) />
 

<!---
Import this noded into the target XML doc. If we
		do not do this first, then ColdFusion will throw
an error about us using nodes that are owned by
another document. Importing will return a reference
to the newly created xml node. The TRUE argument
defines this import as DEEP copy.
--->
		<cfset LOCAL.ChildNode = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(	LOCAL.ChildNode,	JavaCast( "boolean", true )		) />
 

<!---
Append the imported xml node to the child nodes
of the target node.
--->
		<cfset ARGUMENTS.NodeA.AppendChild(LOCAL.ChildNode) />

</cfloop>
 
 	</cfif>

<!--- Return the target node. --->
<cfreturn ARGUMENTS.NodeA />

</cffunction>




	<!--- 
		NJH/WAB 2009/06/13 LID Pool 2358
	
		get position of node in xmlchildren 
		can't work out another way of doing it, but needed for deleting and adding before or after a given node
	--->
	<cffunction name="getPositionOfNode" access="public" hint="Returns the position of a node" output="false">
		<cfargument name="XMLNode" type="xml">
		<cfargument name="startAtIndex" type="numeric" default=1>	
		
		<cfset var nodeIdx = 0>
		<cfset var arrayLength = arrayLen(arguments.XMLNode.xmlParent.xmlChildren) />
	
			<cfloop index="nodeIdx" from = #arguments.startAtIndex# to = #arrayLength#>
				<cfif XMLNode.xmlParent.xmlChildren[nodeIdx] is arguments.XMLNode> <!--- originalNode --->
					<cfreturn nodeIdx>
				</cfif>
			</cfloop>
	
		<cfreturn 0>
	</cffunction>
	
	
	<!--- NJH/WAB 2009/06/13 LID Pool 2358 --->
	<cffunction name="deleteNode">
		<cfargument name="XMLNode" type="xml">
		<cfargument name="startSearchAtPosition" type="numeric" default=1>
		<cfset ArrayDeleteAt(arguments.XMLNode.xmlParent.xmlChildren, getPositionOfNode(XMLNode,arguments.startSearchAtPosition))>
	</cffunction>

	<cffunction name="deleteNodes">
		<cfargument name="XMLNodeArray" type="array">
		<cfset var i = 0>
		<cfset var arrayLength = arrayLen(XMLNodeArray)>

		<cfloop index="i" from = "1" to = "#arrayLength#">
			<cfset deleteNode (XMLNodeArray[i])>
		</cfloop>
		
	</cffunction>
	
	


	<!--- 
		NJH/WAB 2009/06/13 LID Pool 2358
		Pass in an array of nodes (such as anode.XMLChildren or result of an XMLSearch)
		loops through recursively running the named function against each node
		WAB 2010/06/07 added additionalArgs for Settings Project
	 --->
	<cffunction name="runFunctionAgainstNodes">
		<cfargument name="XMLNodeArray" >
		<cfargument name="function" >
		<cfargument name="level" default = "0" >
		<cfargument name="additionalArgs" default = "#structNew()#" >
		
		<cfset var result = "">
		<cfset var argumentStruct = structNew()>
		<cfset var functionresult = "">
		<cfset var thisNode = '' />
		<cfset var i = '' />
		<cfset var childrenArrayLength = arrayLen(XMLNodeArray) />

		<cfloop index = "i" from = "1" to = #childrenArrayLength#>
			<cfset thisNode = XMLNodeArray[i]>
			
			<!--- NJH 2010/07/28 - pass through arguments as argumentCollection --->
			<cfset argumentStruct.XMLNode = thisNode>
			<cfset argumentStruct.level=arguments.level>
			<cfset structAppend(argumentStruct,arguments.additionalArgs)>
			<!--- WAB 2011/12/05 I'm sure that additionalArgs was supposed to be passed through as itself, not converted into keys of the argument collection --->
			<cfset argumentStruct.additionalArgs  = additionalArgs>

						
			<cfif arrayLen(thisNode.xmlchildren) GT 0>	
				<!--- this node has children --->
				<!--- first run the function on this node, with mode = "start" --->
				<cfset functionResult = arguments.function(argumentCollection=argumentStruct,mode="start")>	
				<!--- if false is returned, we do not run the function on the children --->
				<cfif not(isdefined("functionResult")) or functionResult is true>
					<cfset runFunctionAgainstNodes (xmlnodeArray = thisNode.xmlchildren,function = arguments.function,level = arguments.level+1,additionalArgs = arguments.additionalArgs)>
					<!--- WAB 2011/12/05 added calling the function again after all the children with mode = end --->
					<!--- we then run the function again against this node, with mode ="end"  --->
					<cfset functionResult = arguments.function(argumentCollection=argumentStruct,mode="end")>
				</cfif>
			<cfelse>
				<!--- <cfset function(XMLNode = thisNode,level = arguments.level,additionalArgs = arguments.additionalArgs)> --->
				<cfset arguments.function(argumentCollection=argumentStruct)>			
			</cfif>

		</cfloop>
		
		<cfreturn result>
		
	</cffunction>
	
<!---
WAB 2010/06/14 for settings project
--->
	<cffunction name="runFunctionAgainstNodeAndParents">
		<cfargument name="XMLNode" >
		<cfargument name="function" >
		<cfargument name="level" default = "0" >
		<cfargument name="stopAt" default = "XMLRoot" >
		<cfargument name="additionalArgs" default = "#structNew()#" >
		
		<cfset var result = "">
			<cfif xmlnode.xmlname is not stopAt>
		
				<cfif structKeyExists (XMLNode,"XMLParent")>
					<cfset runFunctionAgainstParentNodes (XMLNode = XMLNode.XMLParent, function = function, level= level -1,additionalArgs = additionalArgs)>
				</cfif>
					<cfset arguments.function(xmlNode = thisNode, level = level,additionalArgs = additionalArgs)>			
			</cfif>
					
		<cfreturn result>
		
	</cffunction>


	
	<!--- 
		NJH/WAB 2009/06/13 LID Pool 2358
		
		Because each type of node has a different name/id field need a function to work out what the current node has --->
	<cffunction name="getNodeNameAttribute" output="false">
		<cfargument name="XMLNode" >	
		<cfset var nameAttribute ="">
				<cfif structKeyExists(XMLNode.xmlattributes,"Name")>
					<cfset nameAttribute = "Name">
				<cfelseif structKeyExists(XMLNode.xmlattributes,"id")>
					<cfset nameAttribute = "id">
				<cfelseif structKeyExists(XMLNode.xmlattributes,"tabtext")>
					<cfset nameAttribute = "TabText">
				<cfelse>
					<cfoutput>#XMLNode.xmlname# No name attribute<BR></cfoutput>
					<cfset nameAttribute="None">
					
				</cfif>
	
				<cfreturn nameAttribute>
	</cffunction>
	
<!--- 
2011/11/03 NJB & WAB for settings project
Reorders the items in an XML structure based on an attribute or the name
Basically rearranges the XMLChildren[] array
(WAB: I have to say that I didn't think that it was possible to doctor XML documents in this way, but it works!)

 --->	

<cffunction name="sortXMLChildrenByAttributeValueOrXMLName" output="false">
	<cfargument name = "XMLNode" default = ""> <!--- node whose children are to be ordered --->
	<cfargument name = "AttributeName" default = ""> <!--- leave blank to sort by XMLName --->
	<cfargument name = "SortType" default = "textnocase"> 
	<cfargument name = "SortOrder" default = "asc"> 
	
	<cfset var keyStructure = {}>	
	<cfset var childNode = "">	
	<cfset var sortArray = "">	
	<cfset var sortValue = "">	
	<cfset var childIndex = "">	
	<cfset var getFromPosition = "">	
	<cfset var xmlChildrenLength = arrayLen(xmlnode.xmlchildren)>
	<cfset var trackingArray = arraynew  (1)>  <!--- is going to keep track of where all the items have been swapped to --->

	<!--- Pull out the XML attribute which is going to be used for the sort, 
			store value in a structure keyed on the current position.  
			We will then sort this structure and use the result to re-arrange the xml
			An earlier version did have the ability to specify a path to the value to sort on but it wasn't really necessary for what we were doing
	--->
	<cfloop index = "childIndex" from = "1" to = #xmlChildrenLength#>
		<cfset childNode = xmlnode.xmlchildren[childIndex]>
		<cfif attributeName is not "">
			<cfset sortValue = childNode.xmlattributes[AttributeName]>
		<cfelse>
			<cfset sortValue = childNode.xmlName>
		</cfif>	
		<cfset keyStructure[childIndex] = sortValue>
		<cfset trackingArray[childIndex] = childIndex>
	</cfloop>

	<cfset sortArray = structSort(keyStructure,SortType,sortOrder,"")>
	<!--- Loop through the array swapping items - which gets one item into the correct place, 
			the other item will be in the wrong place but will be corrected by a later swap 
			we have to keep track of where the 'other' items have got to, which we do with the trackingArray
			Actually it does my head in trying to work out what is going on but it seems to work!
			I could have done this by taking a copy of the original array and just putting the items into the right place, but I felt that this would be inefficient (and coming from the days when computers only had 32K I don't like copying things unnecessarily!)
			Also, it did not seem to work to sort into a new array and then set xmlNode.xmlchildren = newArray. 
	--->
	<cfloop index="childIndex" from="1" to="#xmlChildrenLength#">
			<cfset getFromPosition = sortarray[childIndex]>
			<cfset arraySwap (XMLNode.xmlchildren,childIndex,trackingArray[getFromPosition])>
			<cfset arraySwap (trackingArray,arrayfind_(trackingArray,childIndex),getFromPosition)>
	</cfloop>

	<cfreturn true>  <!--- XML is passed by reference so we do not need to return anything --->
</cffunction>

<!--- ArrayFind function used by function above. There will be an arrayFind function in CF, so I have used a different name --->
<cffunction name="arrayFind_" output="false">
	<cfargument name="array">
	<cfargument name="value">

	<cfset var i = 0>	
	<cfloop from=1 to ="#arrayLen(array)#" index="i">
		<cfif array[i] is value>
			<cfreturn i>
		</cfif>
	</cfloop>
	<cfreturn 0>	
</cffunction>


<cffunction name="xmlSearchWithLock" access="public" output="false" returnType="array">
	<cfargument name="xmlDoc" type="xml" required="true">
	<cfargument name="xPathString" type="string" required="true">
	<cfargument name="lockName" type="string" required="true">
	<cfargument name="timeout" type="numeric" default="3">
	
	<cfset var resultArray = arrayNew(1)>
	
	<cflock name="#arguments.lockName#" timeout="#arguments.timeout#" throwOnTimeout=true>
		<cfset resultArray = xmlSearch(arguments.xmlDoc,arguments.xPathString)>
	</cflock>
	
	<cfreturn resultArray>
</cffunction>


	<cffunction name="XML2XMLString" access="public" output="false" returnType="String" hint="Ensures that a variable holds an XML string, not an XML doc">
		<cfargument name="XMLorString" >				

		<cfset var result = XMLorString>

		<cfif isXMLDoc (XMLorString)>
			<cfset result = toString(XMLorString)>
		</cfif>

		<cfreturn result>		
	</cffunction>



	<cffunction name="XMLString2XML" access="public" output="false" returnType="XML" hint="Ensures that a variable holds an XML string, not an XML doc">
		<cfargument name="XMLorString" >				

		<cfset var result = XMLorString>

		<cfif not isXMLDoc (XMLorString)>
			<cfif trim(XMLorString) is not "">
				<cfset result = xmlParse(XMLorString)>
			<cfelse>
				<cfset result = xmlNew()>
			</cfif>	
		</cfif>

		<cfreturn result>			
	</cffunction>


	<cffunction name="areXMLNodesIdentical" output="false" hint="Checks whether XML nodes are identical">
		<cfargument name="NodeA">
		<cfargument name="NodeB">
		<cfargument name="CaseSensitiveNames" default = "true">
		<cfargument name="CaseSensitiveValues" default = "false">
	
		<cfset var result = {identical = true,path=""}>
		<cfset var i = 0>
		<cfset var child = "">
		<cfset var attr = "">
		<cfset var areStringsEqual = application.com.globalFunctions.areStringsEqual>
	
		<!--- check name and attributes --->
		<cfif not areStringsEqual( nodeA.xmlName, nodeB.xmlName, caseSensitiveNames)>
			<cfset result.identical = false>
			<cfset result.message = "xmlNames #nodeA.xmlName# <> #nodeB.xmlName#">
		<cfelseif structCount(nodeA.xmlAttributes) is not structCount(nodeB.xmlAttributes)>
			<cfset result.identical = false>
			<cfset result.message = "Node #nodeA.xmlName# wrong number attributes. #structKeyList(nodeA.xmlAttributes)# vs #structKeyList(nodeB.xmlAttributes)#">
		<cfelse>
			<cfloop collection = #nodeA.xmlAttributes# item="attr">
				<cfif not structKeyExists (nodeB.xmlAttributes, attr) OR NOT application.com.globalFunctions.areStringsEqual( nodeB.xmlAttributes[attr], nodeA.xmlAttributes[attr], caseSensitiveValues )>
					<cfset result.identical = false>
					<cfset result.message = "Attribute #attr# different">
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
	
		<cfif result.identical is false>
			<cfreturn result>
		</cfif>
	
		<!--- check children --->
		<cfif arrayLen (nodeA.xmlChildren) is not arrayLen (nodeB.xmlChildren)>
			<cfset result.identical = false>	
			<cfset result.message = "Node #nodeA.XMLName# wrong number of children">		
		<cfelse>
			<cfloop index="i" from="1" to = "#arrayLen(nodeA.xmlChildren)#" >
				<cfset Result = areXMLNodesIdentical ( nodeA = nodeA.xmlChildren[i], nodeB = nodeB.xmlChildren[i], CaseSensitiveNames = CaseSensitiveNames, CaseSensitiveValues = CaseSensitiveValues )>
				<cfif Result.identical is false>
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
	
		<cfreturn result>
		
	</cffunction>

			
				

</cfcomponent>

