<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:		salesForce.cfc	
Author:			NJH  
Date started:	09-09-2010
	
Description:	SalesForce functions		

Amendment History:

Date 		Initials 	What was changed

2014-01-07 	PPB 	Case 436687 use isISODateString() instead of isDate() for testing dates from SF
2014-03-05	NJH		Case 439090 - check whether the value that we're looking for exists in the column list before deleting from it
2014-04-04 	PPB 	Case 439257	reset EntityId when upserting location
2014/04/29	NJH		Case 439537 - added cflock around the xmlsearch
2014-07-22 	PPB 	Case 439347 if there is an error code prefix it to the error message
2014-10-13	AHL		Case 442038 Opportunity not sync. Apostrophe SOQL Escape Characters

Possible enhancements:


 --->

<cfcomponent output="false">

<!--- 
		this function is one of the primary functions in the .cfc It takes a structure with details of a salesforce object or a relayware entity
		and then converts the structure to one containing fields that the other system can handle.
	--->
	
	<cffunction name="convertObject" access="private" returntype="struct" output="false" hint="Converts a given Relayware entity or SalesForce object into its reciprocal based on the mapping xml">
		<cfargument name="objectDetails" type="struct" required="true" hint="Can be Relayware entity or SalesForce object details. The keys are the column/field names">
		<cfargument name="object" type="string" required="true" hint="The name of the Relayware entity or SalesForce object">
		<cfargument name="opportunityID" type="string" required="false" default="" hint="The associated opportunityID. This is used for logging purposes">
		<cfargument name="direction" type="string" default="SF2RW" hint="The direction we are going. Valid values are SF2RW or RW2SF">
		<cfargument name="relatedObjectID" type="string" default="" hint="The associated object. This is used for logging purposes"> <!--- 2013 Release 2 --->
		<cfargument name="relatedObject" type="string" default="" hint="The associated objectID. This is used for logging purposes"> <!--- 2013 Release 2 --->
		
		<cfscript>
			var entityDetails = {isOK=true,fieldname=""};
			var objectKey = "";
			var thisValue = "";
			var salesForceID = 0;
			var createObjectResult = structNew();
			var entityID = 0;
			var accountID = 0;
			var createEntityResult = structNew();
			var tablename = "";
			var getPerson = "";
			var methodCallResult = structNew();
			var organisationID = 0;
			var rwOrgTypeArray = arrayNew(1);
			var tableObjectName = "table";
			var columnObjectName = "column";
			var mappedObjectName = "object";
			var mappedColumnName = "objectColumn";
			var getValue = "";
			var entityObjectList = arguments.object;
			var mappingArray = arrayNew(1);
			var mappedObjectList = "";
			var mappedObjectStruct = structNew();
			var mappingAttributes = structNew();
			var innerJoin = "";
			var columnName = "";
			var node = "";
			var currentObject = "";
			var objectStruct = structNew();
			var sfMappedObjectKey = "";
			var errorMessage = "";
			var messageTextID = "phr_salesForceError_convertObject";
			var mailTo="relayHelp@relayware.com";
			var mappedObjArgs = {object=arguments.object,direction=arguments.direction};
			var roleSearch = "";
			var mappedObjectColumnList = "";
			var entityObjectColumnList = "";
			var getUserStruct = structNew();
			var settingsStruct = structNew();
			var personStruct = "";
			var thisObjectID = "";
			var getOrganisationID = "";
			var exportEntity = "";
			var orgTypeSettings = structNew();
			var orgTypeSettingsNew = structNew();
			var entityUniqueKey = "";
			var errorArgs = {};
		</cfscript>

		<cfif arguments.direction eq "RW2SF">
			<cfif arguments.object eq "oppProducts">
				<cfset thisObjectID = arguments.objectDetails["oppProductID"]>
			<cfelse>
				<cfset thisObjectID = arguments.objectDetails["#arguments.object#ID"]>
			</cfif>
		<cfelse>
			<cfset thisObjectID = arguments.objectDetails.ID>
		</cfif>
		<cfset logDebugInformation(entityType=arguments.object,entityID=thisObjectID,additionalDetails=arguments,method="ConvertObject #arguments.object#(start)")>
		
		<cfif arguments.direction neq "RW2SF">
			<cfset tableObjectName = "object">
			<cfset columnObjectName = "objectColumn">
			<cfset mappedObjectName = "table">
			<cfset mappedColumnName = "column">
		</cfif>
		
		<!--- get the mappings for the given entity or object --->
		<cfif structKeyExists(arguments.objectDetails,"role")>
			<cfset mappedObjArgs.role = arguments.objectDetails.role>
			<cfset roleSearch="[translate(@role,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.objectDetails.role)#']">
		</cfif>
		<cfset mappedObjectStruct = getMappedObject(argumentCollection=mappedObjArgs)>

		<cfif arguments.direction eq "RW2SF">
			<cfset entityObjectList = mappedObjectStruct.tableList> <!--- this will be one or more Relayware entities --->
			<cfset mappedObjectList = mappedObjectStruct.objectList> <!--- this will be one or more SalesForce object(s)--->
			<cfset entityObjectColumnList = mappedObjectStruct.entityColumnList>
			<cfset mappedObjectColumnList = mappedObjectStruct.objectColumnList>
		<cfelse>
			<cfset entityObjectList = mappedObjectStruct.objectList> <!--- this will be one or more SalesForce object(s) --->
			<cfset mappedObjectList = mappedObjectStruct.tableList> <!--- this will be one or more Relayware entities --->
			<cfset entityObjectColumnList = mappedObjectStruct.objectColumnList>
			<cfset mappedObjectColumnList = mappedObjectStruct.entityColumnList>
		</cfif>

		<!--- go through each item of the incoming structure --->
		<cfloop collection="#arguments.objectDetails#" item="objectKey">

			<!--- loop through each object in the object list.. some mappings map to more than one object. (ie. contact-> person,location) --->
			<cfloop list="#entityObjectList#" index="currentObject">
				<!--- TO-DO: some jiggery-pokery - want to fix this when I have time, but for now this works. If we're mapped to a SF object that 
					has a child mapping, then the response query that we create has the additional object as object__r_fieldname.
					Here, I'm saying that if the column name contains __r_, then replace it with __r. as that should be in the mapping xml
				 --->
				<cfset sfMappedObjectKey = objectKey>
				<cfif arguments.direction eq "SF2RW" and findNoCase("__r_",sfMappedObjectKey)>
					<cfset sfMappedObjectKey = replaceNoCase(sfMappedObjectKey,"__r_","__r.")>
				</cfif>
				
				<cfif listFindNoCase(entityObjectColumnList,sfMappedObjectKey) or listFindNoCase("partner,opportunityContactRole",arguments.object)>
					<!--- find the object/column mapping in the mappings document. --->

					<cflock name="SalesforceXMLSearch" timeout="3">
						<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@#tableObjectName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(currentObject)#'][translate(@#columnObjectName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(sfMappedObjectKey)#']#roleSearch#")>
					</cflock>

					<!--- if we have a result... --->					
					<cftry>
						<cfloop array="#mappingArray#" index="node">
							<cfset mappingAttributes = node.xmlAttributes>
							
							<cfset tableName=mappingAttributes[mappedObjectName]> <!---  the name of the RW entity or SF object --->
							<cfset columnName=mappingAttributes[mappedColumnName]> <!--- the name of the RW column or SF field --->

							<!--- special case for partner and opportunity contact role objects --->
							<cfif listFindNoCase("opportunityContactRole,partner",tablename) and structKeyExists(mappingAttributes,"role")>
								<cfset tablename = tableName&application.delim1&mappingAttributes.role>
							</cfif>
							
							<!--- build up a structure of the mapped objects/entities.. If we convert a contact, for example, this
								entityDetails structure will contain two structures, one holding the person details and another holding 
								the location details 
							--->
							<cfif not structKeyExists(entityDetails,tableName)>
								<cfset entityDetails[tableName] = structNew()>
								<cfset entityDetails[tableName].fieldsToNull = ""> <!--- this will contain the list of fields that need to be set to null --->
							</cfif>
							
							<!--- get the current value in the incoming detail structure for the given field --->
							<cfset thisValue = arguments.objectDetails[objectKey]>
							
							<!--- only set the value if the value is not a 0 or an empty string. However, we want to be able to set the value if the value is 0.0... we need
								this for pricebookEntries that have a price of 0.0 --->
							<!--- NJH 2012/12/12 netGear SF issues.. now allow 0's to go through.. I think this if statement was from beginning days. We now check that the value is not 0 before trying to export a RW entity
								<cfif ((thisValue eq 0 and isValid("float",thisValue) and not isValid("integer",thisValue)) or thisValue neq 0) and thisValue neq ""> --->
							<cfif thisValue neq "">
	
								<!--- if it's a flag, then we basically want to return the column name. This will get passed to the  'setFlagData' function, so that we know which columns to look for --->
								<cfif structKeyExists(mappingAttributes,"flagID") or structKeyExists(mappingAttributes,"flagGroupID")>
									<cfset thisValue = sfMappedObjectKey>
								
								<!--- if there is a related table involved, we need to get data from another table --->
								<cfelseif structKeyExists(mappingAttributes,"relatedTable")>
									<!--- build up the join statement --->
									<cfset innerJoin = "">
									<cfif structKeyExists(mappingAttributes,"join")>
										<cfset innerJoin = mappingAttributes.join>
									</cfif>
									
									<cfif arguments.direction eq "RW2SF">
										
										<cfquery name="getValue" datasource="#application.siteDataSource#">
											select #preserveSingleQuotes(mappingAttributes.value)# as mappedValue from #mappingAttributes.relatedTable# #innerJoin# where #mappingAttributes.key# = '#thisValue#'
										</cfquery>
										
										<!--- if we're related to a user, get the user ID.. need to make a specific call as it's not stored on RW --->
										<cfif structKeyExists(mappingAttributes,"relatedObject") and mappingAttributes.relatedObject eq "User">
											<cfif getValue.recordCount eq 0>
												<cfset entityDetails.isOK = "false">
												<cfset entityDetails.message = "#mappingAttributes.column# does not exist as a user on Relayware">
											<cfelse>
												<cfset getUserStruct = getSalesForceUserID(PersonID=thisValue)>
												<cfif structKeyExists(getUserStruct,"ID")>
													<cfset thisValue = getUserStruct.ID>
												<cfelse>
													<cfset entityDetails.isOK = "false">
													<cfset entityDetails.message = getUserStruct.message>
												</cfif>
											</cfif>
										<cfelse>
										
											<!--- if it's a salesForceID that we're after, and we don't have one, then export the object and get the salesForce ID 
												NJH 2012/12/12 NetGear SF issue - now check that the value is not 0, as we let 0's get by
											--->
											<cfif (getValue.recordCount eq 0 or getValue.mappedValue eq "") and thisValue neq 0 and Left(mappingAttributes.value,3) eq "crm">
											
												<cfset exportEntity = mappingAttributes.relatedTable>
												<!--- if we're exporting a partner as part of an opportunity and the location fields hold locationIDs, we need to get the organisationIDs --->
												<cfif listFindNoCase("partnerLocationID,distiLocationID",objectKey) and mappingAttributes.relatedTable eq "location" and mappingAttributes.value eq "crmOrgID">
													<cfquery name="getOrganisationID" datasource="#application.siteDataSource#">
														select organisationID from location where locationID=#thisValue#
													</cfquery>
													<cfset exportEntity = "organisation">
													<cfset thisValue = getOrganisationID.organisationID>
												</cfif>
											
												<!--- thisValue may be an empty string if the query above returns 0 records --->
												<cfif thisValue neq ""> <!--- 2013 Release 2 --->
													<cfset createObjectResult = export(entityType=exportEntity,entityID=thisValue,opportunityID=arguments.opportunityID,relatedObjectID=thisObjectID,relatedObject=arguments.object)>
												<cfelse>
													<cfset createObjectResult.isOK = false>
												</cfif>
												
												<cfset entityDetails.isOK = createObjectResult.isOK>
												<cfif not entityDetails.isOK>
													<cfset entityDetails.message = "Could not export #application.com.relayEntity.getEntityType(entityTypeID=exportEntity).friendlyName# (ID: #thisValue#) which is needed to export #arguments.object# (ID: #thisObjectID#)">
												</cfif>
												<cfset thisValue = createObjectResult.salesForceID>
												
											<cfelse>
												<cfset thisValue = getValue.mappedValue>
												<cfif objectKey eq "organisationTypeID">
													<cfset settingsStruct = application.com.settings.getSetting("salesForce.mappings.organisationType")>
													<cfif structKeyExists(settingsStruct,thisValue)>
														<cfset thisValue = settingsStruct[thisValue]>
													<cfelse>
														<cfset entityDetails.isOK = false>
														<cfset entityDetails.message = "#thisValue# does not exist as an organisationType mapping on Relayware">
													</cfif>
												<cfelseif objectKey eq "pricebookID">
													<!--- a bit of a hack here as pricebook salesForceIds are not actually the ID in salesForce. We append the country/currency to them so that we can get a unique handle on them
														This was how it was originally designed. We could probably change this to store the actual id, but it hasn't yet been done. We just need time. 
														So, a pricebookID of 'ABCDEF12345' on salesforce will be 'ABCDEF12345_GB_GBP' on Relayware. So, we need to get the first bit of it for the actual id.
													--->
													<cfset thisValue = listFirst(thisValue,"_")>
												</cfif>
											</cfif>
										</cfif>
										
									<cfelse>
										<!--- SF to RW --->
										<!--- get the organisation type --->
										<cfif columnName eq "organisationTypeID" and tablename eq "organisation">
											<cfset orgTypeSettings = application.com.settings.getSetting("salesForce.mappings.organisationType")>

											<!--- NJH 2013/03/11 a bit of error handling to deal with case 434014 --->
											<cfif structCount(orgTypeSettings) eq 0>
												<cfset application.com.settings.populateSettingXMLNodeByVariableName(variablenames="salesForce.mappings.organisationType",updateCluster=false)>  
												<cfset orgTypeSettingsNew = application.com.settings.getSetting("salesForce.mappings.organisationType")>
												
												<cfmail to="william.bibby@relayware.com;nathaniel.hogeboom@relayware.com" from="errors@relayware.com" subject="missing setting" type="html" priority="urgent">
													<cfdump var="#orgTypeSettings#" label="original org type setting">
													<cfdump var="#orgTypeSettingsNew#" label="new org type setting">
												</cfmail>
												<cfset orgTypeSettings = orgTypeSettingsNew>
											</cfif>
											
											<cfset rwOrgTypeArray = structFindValue(orgTypeSettings,thisValue)>

											<cfif arrayLen(rwOrgTypeArray) gt 0>
												<cfset thisValue = rwOrgTypeArray[1].key>
											<cfelse>
												<cfmail to="william.bibby@relayware.com;nathaniel.hogeboom@relayware.com" from="errors@relayware.com" subject="missing setting" type="html" priority="urgent">
													<cfdump var="#orgTypeSettings#">
												</cfmail>
												
												<cfthrow message="Unable to find organization type mapping for value #thisValue#">
												<cfset thisValue = "EndCustomer">
											</cfif>

										</cfif>

										<!--- 
											special case for accountToID fields for partner roles.. In the mapping XML, it most likely is mapped to a location. So, we would end up getting a locationID.
											However, we want to have the orgId so that we can figure out what locationID it is that we actually want.
											This is a bit of hard-coding which may need to be altered.
										 --->
										<cfif objectKey eq "accountToID" and structKeyExists(mappingAttributes,"role")>
											<cfquery name="getValue" datasource="#application.siteDataSource#">
												select organisationID as mappedValue from organisation where crmOrgID = '#thisValue#'
											</cfquery>
										<cfelse>
											<cfquery name="getValue" datasource="#application.siteDataSource#">
												select #mappingAttributes.key# as mappedValue from #mappingAttributes.relatedTable# #innerJoin# where #preserveSingleQuotes(mappingAttributes.value)# = '#thisValue#'
											</cfquery>
										</cfif>

										<cfif structKeyExists(mappingAttributes,"relatedObject") and mappingAttributes.relatedObject eq "User" and getValue.recordCount eq 0>
											<cfset personStruct = getRelaywarePersonID(SalesForceID=thisValue)>
											<cfif personStruct.success>
												<cfif structKeyExists(personStruct,"personid") and personStruct.personid gt 0>
													<cfset thisValue = personStruct.personid>
												<cfelse>
													<cfset thisValue = getDefaultOwner(Country=personStruct.country)>
													<cfif thisValue eq 0>
														<cfset entityDetails.isOK = "false">
														<cfset entityDetails.message = "#mappingAttributes.column# does not exist as a user on Relayware. No person flagged as 'SFDCDefaultOwner' could be found for country '#personStruct.country#'.">
													<cfelse>
														<cftry>
															<cfset form["#arguments.object#name"] = arguments.objectdetails.name>
															<cfset application.com.email.sendEmail(emailTextID="SalesForceOpportunitySetToDefaultOwner",personID=thisValue)>
															<cfcatch type="any">
																
															</cfcatch>
														</cftry>
													</cfif>
												</cfif>
											<cfelse>
												<cfset entityDetails.isOK = "false">
												<cfset entityDetails.message = personStruct.message> <!--- 2013 Release 2 --->
											</cfif>
										<cfelse>
											<!--- if the saleForce object doesn't exist on Relayware, then create them --->
											<cfif (getValue.recordCount eq 0 or getValue.mappedValue eq "") and Left(mappingAttributes.value,3) eq "crm">
											
												<cfif not (mappingAttributes.column eq "pricebookID" and arguments.opportunityID neq "")>
													<cfset objectStruct = getMappedObject(object=mappingAttributes.relatedTable,direction="RW2SF")>
													<cfset createEntityResult = import(object=objectStruct.entity,objectID=thisValue,opportunityID=arguments.opportunityID,relatedObjectID=thisObjectID,relatedObject=arguments.object)>  <!--- 2013 Release 2 --->
													<cfset entityDetails.isOK = createEntityResult.isOK>
													<cfif not entityDetails.isOK>
														<cfset entityDetails.message = "Could not import #mappingAttributes.relatedTable# (ID: #thisValue#) which is needed to import #arguments.object# (ID: #thisObjectID#)">
													</cfif>
													<cfset thisValue = createEntityResult.entityID>
												
												<!--- LID 7258 - otherswise it's a pricebook that we're needing to import.. we don't want to import it when importing an opportunity, but rather when we're running the pricebook import routine, as it has rules that need to be followed --->
												<cfelse>
													<cfset entityDetails.isOK = false>
													<cfset entityDetails.message = "Pricebook with ID:#thisValue# does not exist on Relayware. Please have an administrator run the pricebook import routine to import the necessary pricebook.">
												</cfif>
											<cfelse>
												<cfif getValue.mappedValue eq "" and columnName eq "countryID">
													<cfset entityDetails.isOK = false>
													<cfset entityDetails.message = "Country (#thisValue#) not found for #arguments.object# (ID: #thisObjectID#)">
												</cfif>
												
												<cfset thisValue = getValue.mappedValue>
											</cfif>									
										</cfif>
											
									</cfif>
								
								<!--- if the value is retrieved by running a function --->	
								<cfelseif structKeyExists(mappingAttributes,"value") and left(mappingAttributes.value,5) eq "func:">
									<!--- 2012/01/17	NJH		setting mergeStruct.direction --->
									<cfset mergeStruct = structNew()>
									<cfset mergeStruct = duplicate(arguments.objectDetails)>
									<cfset mergeStruct.direction = arguments.direction>
									<cfset thisValue = runMappingFunction(functionName=mappingAttributes.value,mergeStruct=mergeStruct)>
								</cfif>
							</cfif>
							
							<!--- we don't want to keep getting values if we've had a problem. --->
							<cfif not entityDetails.isOK>
								<cfset entityDetails.fieldname = columnName>
								<cfbreak>
							</cfif>
							
							<!--- 2012/02/16 MS/NJH Extending the mapping attributes that we can pass in SF Mapping XML. New parameter called dataType to force data to be stored as a given type. Used to convert problematic data --->
							<cfif structKeyExists(mappingAttributes,"dataType") and thisValue neq "">
								<cfif mappingAttributes.dataType eq "integer">
									<cfset thisValue = int(thisValue)>
								<cfelseif mappingAttributes.dataType eq "numeric">		<!--- 2013-08-21 PPB Case 436649 numeric --->
									<cfset thisValue = val(thisValue)>					<!--- 2013-08-21 PPB Case 436649 numeric --->
								</cfif>
							</cfif>
							
							<!--- 2010/12/24 NJH setting some null fields --->
							<cfif direction eq "RW2SF" and structKeyExists(mappingAttributes,"sfValueToNull") and mappingAttributes.sfValueToNull eq thisValue>
								<cfset entityDetails[tablename].fieldsToNull = listAppend(entityDetails[tablename].fieldsToNull,columnName)>
								<cfset entityDetails[tablename][columnName] = thisValue>
	
							<!--- only set the value if the value is not a 0 or an empty string when going across to SalesForce. If a field is to get blanked, then the xml attribute 'sfValueToNull' should be set
								However, we want to be able to set the value if the value is 0.0... we need
								this for pricebookEntries that have a price of 0.0 We may need a better way of doing this 
								2013/08/13 NJH Case 436251	 - changed to convert the 0 to string and then do a string compare. 0 then doesn't equal 0.00, where as the old comparison (using isFloat and isInt) thought that they were the same
							--->
							<cfelseif direction eq "RW2SF" and ((thisValue eq 0 and compare(toString(int(thisValue)),toString(thisValue)) neq 0) or thisValue neq 0) and thisValue neq "">
								<cfif application.com.dateFunctions.isISODateString(thisValue)>		<!--- 2014-01-07 PPB Case 436687 use isISODateString instead of isDate --->
									<cfset thisValue = dateFormat(thisValue,"yyyy-mm-dd")&" "&timeFormat(thisValue,"HH:mm:ss")>
								</cfif>
								<cfset entityDetails[tablename][columnName] = thisValue>

							<cfelseif direction eq "SF2RW">
								<!--- if it's a flag mapping, then we want to return the column name. We build up a list of column names that hold flag IDs. This gets passed to the 'SetFlagData' function so 
									that we only get the xml records that we need to, rather than trolling through them all and setting the flags. This way, we only set flags for values that have changed. --->
								<cfif structKeyExists(mappingAttributes,"flagID") or structKeyExists(mappingAttributes,"flagGroupId")>
									<cfif not structKeyExists(entityDetails[tablename],"flagColumnList")>
										<cfset entityDetails[tablename].flagColumnList = "">
									</cfif>
									<cfset entityDetails[tablename].flagColumnList = listAppend(entityDetails[tablename].flagColumnList,mappingAttributes.objectColumn)>
								</cfif>
								<cfif columnName neq "">
									<cfset entityDetails[tablename][columnName] = thisValue>
								</cfif>
							</cfif>
						</cfloop>
					
						<!--- if we've had a problem, log the error and send an urgent email --->
						<cfcatch type="any">
							<cfset entityDetails.isOK = false>
							<cfset entityDetails.fieldname = columnName>
														
							<cfif errorMessage eq "" and messageTextId neq "">
								<cfset errorMessage = application.com.relayTranslations.translatePhrase(phrase=messageTextId)>
							<cfelse>
								<cfset errorMessage = cfcatch.message>
							</cfif>
							<cfset entityDetails.message = errorMessage>  <!--- 2013 Release 2 --->
							
							<cfif application.testSite neq 0>
								<cfset mailTo="nathaniel.hogeboom@relayware.com">
							</cfif>
							
							<cfmail to="#mailTo#" from="errors@relayware.com" subject="Error encountered Converting Object in SFDC Synch on #request.currentSite.domainAndRoot#" type="html" priority="urgent">
								An error was encountered at #request.requestTime# while attempting to <cfif arguments.direction eq "SF2RW">import #arguments.object# data into Relayware<cfelse>export #arguments.object# data to SalesForce</cfif>.
								<br>
								<br>
								<b>Arguments</b><br>
								<cfdump var="#arguments#" expand = "no"><br>
								
								<b>Error dump</b><br>
								<cfif structKeyExists(cfcatch,"sql")>SQL: <cfdump var="#cfcatch.sql#"><br></cfif>
								Message:<cfdump var="#cfcatch.message#"><br>
								Detail:<cfdump var="#cfcatch.detail#"><br>
								TagContext:<cfdump var="#cfcatch.tagContext#"><br>
								<br>
								Regards,<br>
								<br>
								RelayWare/SalesForce Synch
							</cfmail>
							
							<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce ConvertObject #arguments.object#",Severity="error",catch=cfcatch,WarningStructure=arguments)>
							
							<cfif arguments.direction eq "SF2RW">
								<cfset logSFError(object=arguments.object,opportunityID=arguments.opportunityID,objectID=thisObjectID,method="ConvertObject",message=errorMessage,fields=columnName,direction="I")>
							<cfelse>
								<!--- we do a list first as the table name could be partner#application.delim1#var/reseller and we want to log partner --->
								<cfset logSFError(object=listFirst(tablename,application.delim1),opportunityID=arguments.opportunityID,entityID=thisObjectID,method="ConvertObject",message=errorMessage,fields=columnName,direction="E",role=listLast(tablename,application.delim1))>
							</cfif>
							
							<!--- if we're in debug mode, then rethrow the error for developer purposes. There is another try/catch around the whole process that will handle this error. --->
							<cfif structKeyExists(url,"debug")>
								<cfrethrow>
							</cfif>
						</cfcatch>
					</cftry>
				</cfif>
				
				<cfif not entityDetails.isOK>
					<cfbreak>
				</cfif>
			</cfloop>
			
			<cfif not entityDetails.isOK>
				<cfbreak>
			</cfif>
		</cfloop>
		
		<!--- if we've been able to convert everything, then delete any errors message that may have been previously logged --->
		<cfif entityDetails.isOK>
			<cfif arguments.direction eq "SF2RW">
				<cfset deleteLogEntries(object=arguments.object,objectID=arguments.objectDetails.ID,method="ConvertObject",direction="I")>
			<cfelse>
				<cfset deleteLogEntries(object=listFirst(tablename,application.delim1),entityID=arguments.objectDetails[application.entityType[application.entityTypeID[arguments.object]].uniqueKey],method="ConvertObject",direction="E",role=listLast(tablename,application.delim1))>
			</cfif>
		<cfelse>
		
			 <!--- 2013 Release 2 --->
			<cfset errorArgs = {fields=columnName,opportunityID=arguments.opportunityID,relatedObjectID=arguments.opportunityID,method="ConvertObject",dataStruct=arguments.objectDetails}>
			<cfif structKeyExists(entityDetails,"message")>
				<cfset errorArgs.message = entityDetails.message>
			</cfif>
			<cfif arguments.relatedObject neq "" and arguments.relatedObjectID neq "">
				<cfset errorArgs.relatedObject = arguments.relatedObject>
				<cfset errorArgs.relatedObjectID = arguments.relatedObjectID>
			</cfif>
			<cfif arguments.direction eq "SF2RW">
				<cfset logSFError(object=arguments.object,objectID=arguments.objectDetails.ID,direction="I",argumentCollection=errorArgs)>
			<cfelse>
				<cfset entityUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=arguments.object).uniqueKey>
				<cfif listLen(tablename,application.delim1) gt 1>
					<cfset errorArgs.role = listLast(tablename,application.delim1)>
				</cfif>
				<cfset logSFError(object=listFirst(tablename,application.delim1),entityID=arguments.objectDetails[entityUniqueKey],direction="E",argumentCollection=errorArgs)>
			</cfif>
		</cfif>
	
		<!--- log the fact that we have completed the convertObject function and also its result --->
		<cfset logDebugInformation(entityType=arguments.object,entityID=thisObjectID,additionalDetails=entityDetails,method="ConvertObject #arguments.object#(end)")>

		<cfreturn entityDetails>
	</cffunction>
	
	
	<!--- function to import objects (opportunities, accounts, contacts) from salesForce into relayware --->
	<cffunction name="import" access="public" returntype="struct" output="false">
		<cfargument name="object" type="string" default="opportunity">
		<cfargument name="objectID" type="string" required="true">
		<cfargument name="opportunityID" type="string" required="false" default="">
		<cfargument name="objectDetails" type="struct" required="false">
		<cfargument name="additionalArgs" type="struct" default="#structNew()#">
		<cfargument name="objectEntityID" type="numeric" hint="The object entityID">
		<cfargument name="deleteEntity" type="boolean" default="false" hint="Does the object need to be deleted.">
		<cfargument name="relatedObjectID" type="string" default=""> <!--- 2013 Release 2 --->
		<cfargument name="relatedObject" type="string" default=""> <!--- 2013 Release 2 --->
		
		<cfscript>
			var methodCallResult = {success = true};
			var objectStruct = structNew();
			var childObjectStruct = structNew();
			var entityID = 0;
			var processEntity = "";
			var rwEntitiesStruct = structNew();
			var locationID = 0;
			var name = "";
			var thisOpportunityID = arguments.opportunityID;
			var mappedObjectInfo = getMappedObject(object=arguments.object,direction="SF2RW");
			var processOrder = mappedObjectInfo.processOrder;
			var entityType = mappedObjectInfo.entity;
			var matchEntityColumns = "";
			var childMethodCallResult = structNew();
			var setNewPOLDataAs = "";
			var getLocationFromPerson = "";
			var oppRoleDetails = structNew();
			var oppRoleStruct = structNew();
			var oppObjectList = "";
			var oppRelatedObject = "";
			var getOppTypeIDForLeads = "";
			var oppProductMappings = structNew();
			var deleteOppProducts = "";
			var thisObjectID = "";
			var exportOppProduct = structNew();
			var rolesArray = arrayNew(1);
			var roleNode = "";
			var role = "";
			var locationAdded = false;
			var tempProcessOrderList = processOrder;
			var flagColumnList = "";
			var locationExists = "";
			var functionArgs = structNew();
			var flagColumn = "";
			var tablename = "";
			var flagOrFlagGroup = "";
			var flagIDList = "";
			var rowCount = 0;
			var deleteSynchPendingRecord = "";
			var locationFields = "";
			var locationFieldsArray = "";
			var locationNode = "";
			var locationFieldName = "";
			var logStruct = structNew();
			var flagDataResult = structNew();
			var objectFieldList = "";
			var roleList = "";
			var queryFieldList = "";
			var locationFieldArray = "";
			var personLocationID = structNew();
			var fieldName = "";
			var oppProductsToImport = "";
			var oppProductsSynched = true;
			var synchResult = structNew();
			var doesOppHavePendingProducts = "";
			var sfIDColumn = getIDMappingInfo(entityType=mappedObjectInfo.entity).sfIDColumn;
			var fieldsToUpdate = "";
			var warningStructure = {};
			var oppRoleFieldList = "";
			var getEndCustomerCountry = "";
			var countryCurrencies = "";
			var methodName = "";
		</cfscript>

		<cfset logDebugInformation(entityID=arguments.objectID,entityType=arguments.object,method="Import #arguments.object#(start)",additionalDetails=arguments)>

		<cfset request.errorMsgPhraseTextPrefix = "salesForce">  <!--- phrasePrefix for entity error phraseTextID so that we can be specific about error messages --->
		
		<cfset methodCallResult.entityID=0>
		<cfif arguments.deleteEntity>
			
			<cftry>
				<cfset application.com.relayEntity.deleteEntity(entityTypeID=mappedObjectInfo.entity,entityID=arguments.objectEntityID,harddelete=true)>
				
				<cfset methodCallResult.isOk = true>
				<cfset entityID=arguments.objectEntityID>
				<cfcatch>
					<cfset methodCallResult.isOk = false>
				</cfcatch>
			</cftry>
			
		<cfelse>
		
			<!--- use retrieve method to get all the information about the object  --->
			<cfif not structKeyExists(arguments,"objectDetails")>
				<!--- if we know this object already exists on RW (ie. we have a salesforceID), then only get the synched fields; otherwise, get the lot --->
				<cfif not structKeyExists(arguments,"objectEntityID")>
					<cfset entityID = application.com.salesForce.doesEntityExist(object=arguments.object,objectID=arguments.objectID,useRWMatchingRules=false).entityID>
				<cfelse>
					<cfset entityID = arguments.objectEntityID>
				</cfif>
				<cfset queryFieldList = mappedObjectInfo.objectColumnList>
				<cfif entityID neq 0>
					<cfset queryFieldList = mappedObjectInfo.synchedObjectColumnList>
				</cfif>
				<!--- Might have to use queryString here rather than retrieve, so that we are consistent with the original query --->
				<cfset queryFieldList = application.com.salesforceAPI.setObjectFieldsForQuery(object=arguments.object,fieldlist=queryFieldList,useLabel="false")>  <!--- 2013 Release 2 --->
				<cfif queryFieldList neq "">
					<cfset methodCallResult = send(object=arguments.object,method="retrieve",fieldlist=queryFieldList,ids=arguments.objectID,opportunityID=thisOpportunityID)>
				</cfif>
			</cfif>
			
			<!--- 
				for each object:
				1. Check if it exists by checking for the salesForceID value. 
					If it does exist, update if arguments.updateExisting is true; otherwise not.
					If it does not exist, check if it exists by matching algorithm.
					If it does exist, set the sales ForceID. Update if arguments.updateExisting is true; otherwise not.
					If it does not exist, create them.
			  --->
	
			<!--- if it's successful and we have a record count where we are expecting it --->
			<cfif methodCallResult.success and ((not structKeyExists(arguments,"objectDetails") and structKeyExists(methodCallResult,"response") and methodCallResult.response.recordCount gt 0) or (structKeyExists(arguments,"objectDetails")))>
	
				<cfif not structKeyExists(arguments,"objectDetails")>
					<cfset objectStruct = application.com.structureFunctions.queryToStruct(query=methodCallResult.response,key="ID")>
					<!--- seem to need to do this, as an objectId of 0038000000ft54N will bring back an object with an ID of 0038000000ft54NAAQ --->
					<cfset arguments.objectID = structKeyList(objectStruct)>
				<cfelse>
					<cfset objectStruct[arguments.objectID] = duplicate(arguments.objectDetails)>
					<cfif not structKeyExists(arguments.objectDetails,"ID")>
						<cfset objectStruct[arguments.objectID]["ID"] = arguments.objectID>
					</cfif>
				</cfif>
				
				<cfif thisOpportunityID eq "" and arguments.object eq "opportunity">
					<cfset thisOpportunityID = arguments.objectID>
				</cfif>
	
				<cfset rwEntitiesStruct = convertObject(object=arguments.object,objectDetails=objectStruct[arguments.objectID],opportunityID=thisOpportunityID,relatedObject=arguments.relatedObject,relatedObjectID=arguments.relatedObjectID)>  <!--- 2013 Release 2 --->
	
				<!--- we may not always have a location and person, for example... if it's just a person field that has changed, we don't need to worry about processing location data --->
				<cfloop list="#tempProcessOrderList#" index="entityType">
					<cfif not structKeyExists(rwEntitiesStruct,entityType)>
						<cfset processOrder = listDeleteAt(processOrder,listFindNoCase(processOrder,entityType))>
					</cfif>
				</cfloop>
	
				<!--- if it's an opportunity, then we need to get the opportunityContacts and partners and their roles - create a new key on the fly for the opportunity table
					with the name of  contactID-role. We then need to check for this in convertObject function
					In Phase 2 V1, we are only concerned about importing partners; in V2, we will need to be concerned about importing distributors as well,
					so everything may need to be put into a function.
				 --->
				<cfif arguments.object eq "opportunity" and rwEntitiesStruct.isOK and request.synchLevel neq "field">
					<cfset oppObjectList = "">
					<cfif listFindNoCase(mappedObjectInfo.objectList,"opportunityContactRole")>
						<cfset oppObjectList = "opportunityContactRole">
					</cfif>
					<cfif listFindNoCase(mappedObjectInfo.objectList,"partner")>
						<cfset oppObjectList = listAppend(oppObjectList,"partner")>
					</cfif>
	
					<cfloop list="#oppObjectList#" index="oppRelatedObject">
						<cflock name="SalesforceXMLSearch" timeout="3">
							<cfset rolesArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(oppRelatedObject)#'][translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='opportunity']")>
						</cflock>
						<!--- re-set the Relayware roles, as we only pick up roles that have been set. If the roles are not brought back from salesForce, that is considered a deletion, which is the same
							as setting these fields to 0
						 --->
						<cfset roleList = "">
						<cfset oppRoleFieldList = "">
						<cfloop array="#rolesArray#" index="roleNode">
							<cfset role = roleNode.xmlAttributes.column>
							<cfset oppRoleFieldList = listAppend(oppRoleFieldList,role)>
							<!--- if it's a new opportunity, then set the roles to 0 by default --->
							<cfif entityID eq 0>
								<cfset rwEntitiesStruct.opportunity[role] = 0>
							</cfif>
							<cfset roleList = listAppend(roleList,roleNode.xmlAttributes.role)>
						</cfloop>
						
						<cfif roleList neq "">
							<cfset roleList = listQualify(roleList,"'")>
							<cfset childMethodCallResult = send(object=oppRelatedObject,method="query",queryString="select #roleNode.xmlAttributes.objectColumn#,role from #oppRelatedObject# where opportunityID='#arguments.objectID#' and role in (#roleList#)",opportunityID=thisOpportunityID,ignoreBulkProtection=true)>
							<cfif childMethodCallResult.success and listFindNoCase(childMethodCallResult.response.columnList,roleNode.xmlAttributes.objectColumn)>
								<cfloop query="childMethodCallResult.response">
									<cfset oppRoleDetails = application.com.structureFunctions.queryRowToStruct(query=childMethodCallResult.response,row=currentRow)>
									<cfset oppRoleStruct=convertObject(object=oppRelatedObject,objectDetails=oppRoleDetails,opportunityID=thisOpportunityID)>
									
									<cfif structKeyExists(oppRoleStruct,"opportunity") and oppRoleStruct.isOK>
										<cfset structAppend(rwEntitiesStruct.opportunity,oppRoleStruct.opportunity)>
									</cfif>
								</cfloop>
							</cfif>
						</cfif>
					</cfloop>
					
					<!--- NJH 2012/12/12 Only call setOppPartner for a mapping of type partner that is mapped to partnerLocationID. If one maps partnerLocationID to another field,
						then don't call this function. Deal with it in a custom function. --->
					<cfif listFindNoCase(oppObjectList,"partner") and listFindNoCase(oppRoleFieldList,"partnerLocationID")>
						<!--- Set the partner for the given opportunity --->
						<cfset setOppPartner(entityStruct=rwEntitiesStruct)>
					</cfif>
				</cfif>
	
				<cfset structAppend(rwEntitiesStruct[entityType],arguments.additionalArgs,true)>
				
				 <!--- 2013 Release 2 --->
				<cfset logStruct = {opportunityID=thisOpportunityID,object=arguments.object,objectID=arguments.objectID,name=name,direction="I",method="Import"}>
				<cfif structKeyExists(arguments,"relatedObjectID")>
					<cfset logStruct.relatedObjectID=arguments.relatedObjectID>
				</cfif>
				<cfif structKeyExists(arguments,"relatedObjectID")>
					<cfset logStruct.relatedObject=arguments.relatedObject>
				</cfif>

				<cfif rwEntitiesStruct.isOK>
					<cfloop list="#processOrder#" index="processEntity">
	
						<!--- If we've just added a location, then we don't want to see whether the person exists, because we know that they won't.. skip the call to see if they exist --->
						<cfif processEntity eq "person" and processOrder eq "location,person" and locationAdded>
							<!--- it may be, however, that a location has just been added because a contact's address details have changed. In this scenario, they will exist
								on Relayware... --->
							<cfset entityID = application.com.salesForce.doesEntityExist(object=arguments.object,objectID=arguments.objectID,useRWMatchingRules=false).entityID>
						<cfelse>
							<!---<cfset entityID = application.com.salesForce.doesEntityExist(object=arguments.object,objectID=arguments.objectID,objectDetails=objectStruct[arguments.objectID]).entityId> --->
							<cfset entityID = application.com.salesForce.doesEntityExist(object=arguments.object,objectID=arguments.objectID,entityDetails=rwEntitiesStruct).entityId>
						</cfif>
	
						<!--- TODO: not sure if we should be checking the result of the entityExists call to see whether it's ok.. For example, if a countryID isn't passed to check a location (person), it falls over
							So, we really don't want to then try to insert the location as that only introduces more errors...
						 --->
						<cfif entityType eq "person">
							<!--- we're processing a location for a person that does exist in relayware --->
							<cfif entityID neq 0 and processEntity eq "location">
								<!--- 
									if we're importing a contact, we have a location and a person struct.
									We process the location struct first as if we need to do a person create, we need to have the locationID.
									So, we check if the person exists. If so, grab the locationID from the person; otherwise we will have to 
										check whether it exists using standard matching rules.
								--->
								
								<!--- here we need to grab any location fields that we don't have in the incoming details, should we need to create a location. For example, if a contact's postal code
									has changed, and we decide that it's a new location, then we need all other address fields as well as the country. We can get these details from the current location
								 --->
								<cfset locationFields = "">
								<cflock name="SalesforceXMLSearch" timeout="3">
									<cfset locationFieldArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='location']/@column")>
								</cflock>
								
								<cfloop array="#locationFieldArray#" index="locationNode">
									<cfif not structKeyExists(rwEntitiesStruct["location"],locationNode.xmlValue)>
										<cfset locationFields = listAppend(locationFields,locationNode.xmlValue)>
									</cfif>
								</cfloop>
								
								<cfquery name="getLocationFromPerson" datasource="#application.siteDataSource#">
									select p.locationID, p.organisationID, <cfif locationFields neq "">#locationFields#,</cfif> (select count(1) from person where locationID=p.locationID) as numPeopleAtLocation
									from person p with (noLock) inner join location l with (noLock)
										on p.locationID = l.locationID
									where p.personID = #entityID#
								</cfquery>
								
								<!--- if there is more than one person at this location and the matching columns have been updated, then we want to create a new location--->
								<cfif getLocationFromPerson.numPeopleAtLocation gt 1>
									<cfloop list="#locationFields#" index="locationFieldName">
										<cfset rwEntitiesStruct["location"][locationFieldName] = getLocationFromPerson[locationFieldName][1]>
									</cfloop>
	
									<!--- need to get the organisationID for matching purposes 
									2012/11/27	NJH/WAB 	CASE 432014, changed from  getLocationFromPerson.organisationID[1]> 
									--->
									<cfset rwEntitiesStruct["location"]["organisationID"] = rwEntitiesStruct["person"]["organisationID"]>
									<cfset locationExists = application.com.relayEntity.doesLocationExist(locationDetails=rwEntitiesStruct["location"])>
									<cfset locationID = locationExists.entityID>
								<cfelse>
									<cfset locationID = getLocationFromPerson.locationID>
									<!--- NJH/WAB CASE 432014 - 2012/11/27 - if a person has moved organisation, we need to update the location's organisationID to point to the new organisationID
										This will create a new location at the organisation without doing any matching!!
									--->
									<cfset rwEntitiesStruct["location"]["organisationID"] = rwEntitiesStruct["person"]["organisationID"]>
								</cfif>
								<cfset entityID = locationID>
									
							<!--- if a person doesn't exist, we may have to insert a location. To do this, we need an organisationID which is currently in the person structure --->
							<cfelseif entityID eq 0 and processEntity eq "location">
								<cfset rwEntitiesStruct["location"]["organisationID"] = rwEntitiesStruct["person"]["organisationID"]>
								<!--- START 2014-04-04 PPB Case 439257 --->
								<cfset locationExists = application.com.relayEntity.doesLocationExist(locationDetails=rwEntitiesStruct["location"])>
								<cfset locationID = locationExists.entityID>
                                <cfset entityID = locationID>
								<!--- END 2014-04-04 PPB Case 439257 --->
									
							<!--- if a person does exist , but the locationId may have changed --->
							<cfelseif entityID neq 0 and locationID neq 0 and processEntity eq "person">
								<cfset rwEntitiesStruct[processEntity]["locationID"] = locationID>
							
							<!--- if a person does not exist and the locationID is not 0 and we're about to add the person record, then set the locationID
								for the person --->
							<cfelseif entityID eq 0 and locationID neq 0 and processEntity eq "person">
								<cfset rwEntitiesStruct[processEntity]["locationID"] = locationID>
							</cfif>
						</cfif>
						
						<cfset flagColumnList = "">	
						<!--- if this entity has flags that have changed, we have here the list of object columns mapped to flags.. this gets passed to the setFlagData function --->	
						<cfif structKeyExists(rwEntitiesStruct[processEntity],"flagColumnList")>
							<cfset flagColumnList = rwEntitiesStruct[processEntity].flagColumnList>
							<cfset structDelete(rwEntitiesStruct[processEntity],"flagColumnList")>
						</cfif>
						
						<cfset childMethodCallResult.isOK = true>
						<!--- If we have a organisationID, personID or a locationID, etc. do an update
							TO DO: might be useful to have an upsert function...
							
							I have added a check of fields to update which I hope is ok.. it's to prevent doing unnecessary updates where there are no fields.
								An empty struct, as this point, (in field updates) has two keys, salesforceID and fieldsToNull
						--->
						<cfset fieldsToUpdate = structKeyList(rwEntitiesStruct[processEntity])>
						<cfif listFindNoCase(fieldsToUpdate,"fieldsToNull")>
							<cfset fieldsToUpdate = listDeleteAt(fieldsToUpdate,listFindNoCase(fieldsToUpdate,"fieldsToNull"))>
						</cfif>
						
						<cfif listFindNoCase(fieldsToUpdate,sfIDColumn)>
							<cfset fieldsToUpdate = listDeleteAt(fieldsToUpdate,listFindNoCase(fieldsToUpdate,sfIDColumn))>
						</cfif>

						 <!--- 2013 Release 2 --->
						<cfset logStruct = {opportunityID=thisOpportunityID,object=arguments.object,objectID=arguments.objectID,name=name,direction="I"}>
						<cfif structKeyExists(arguments,"relatedObjectID")>
							<cfset logStruct.relatedObjectID=arguments.relatedObjectID>
						</cfif>
						<cfif structKeyExists(arguments,"relatedObjectID")>
							<cfset logStruct.relatedObject=arguments.relatedObject>
						</cfif>

						<!--- allow some pre processing --->
						<cfif structKeyExists(this,"preImport")>
							<cfset rwEntitiesStruct[processEntity] = preImport(entityDetails=rwEntitiesStruct[processEntity],object=arguments.object,objectID=arguments.objectID,entityID=entityID)>
						</cfif>

						<cfif entityID neq 0 and fieldsToUpdate neq "">
							<cftry>
								
								<cfset methodName="update#processEntity#">  <!--- 2013 Release 2 --->
								<cfset logStruct.entityID=entityID>  <!--- 2013 Release 2 --->
								
								<!--- here we are stripping out any fields where Relayware is the master --->
								<cfloop collection="#rwEntitiesStruct[processEntity]#" item="fieldname">
									<cfif fieldname neq "fieldsToNull" and not listFindNoCase(mappedObjectInfo.synchedObjectColumnList,getMappedField(object=arguments.object,table=processEntity,column=fieldname))>
										<!--- 
											the countryID on the opportunity is a bit of a special case, as we set it 'manually'  at the moment, based on the partner's country. So, an update to the partner should
											trigger an update to the country, even though it's not in the mappings document
											2012/11/27	NJH/WAB CASE 432014, had to add another exception for person.locationid
										--->
										<cfif not ((processEntity eq "opportunity" and fieldname eq "countryID") OR (processEntity eq "person" and fieldname eq "locationID"))>
											<cfset structDelete(rwEntitiesStruct[processEntity],fieldname)>
										</cfif>
									</cfif>
								</cfloop>
								
								<cfloop list="#flagColumnList#" index="flagColumn">
									<cfif not listFindNoCase(mappedObjectInfo.synchedObjectColumnList,flagColumn)>
										<cfset flagColumnList = listDeleteAt(flagColumnList,listFindNoCase(flagColumnList,flagColumn))>
									</cfif>
								</cfloop>
								
								<!--- TODO: probably need to check whether an update can be done.. for example, if we've matched using RW rules,
									then we'll be here, but if the person is not approved, for example, and the trigger point is approval, then
									they probably shouldn't be updated --->
								<cfif structKeyExists(application.com.relayEntity,"update#processEntity#Details")>
									<cfset functionArgs = structNew()>
									<cfset functionArgs["#processEntity#ID"]=entityID>
									<cfset functionArgs["#processEntity#Details"]=rwEntitiesStruct[processEntity]>
									
									<cfinvoke component=#application.com.relayEntity# method="update#processEntity#Details" argumentCollection=#functionArgs# returnvariable="childMethodCallResult">
								<cfelse>
									<cfset childMethodCallResult = application.com.relayEntity.updateEntityDetails(entityTypeID=application.entityTypeID[processEntity], entityID=entityID, entityDetails=rwEntitiesStruct[processEntity])>
								</cfif>
								<cfcatch>
									<cfset childMethodCallResult = {isOK = false,message = cfcatch.message&"<br>"&cfcatch.detail}>
									<!--- CASE 432014 Added logging --->
									<cfset warningStructure = {arguments = arguments,functionArgs = functionArgs}>
									<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce Entity Update",Severity="error",catch=cfcatch,WarningStructure=warningStructure)>
								</cfcatch>
							</cftry>
								
							<cfset childMethodCallResult.entityID = entityID>
							
						<!--- otherwise we're doing a create --->
						<cfelseif entityID eq 0>
							<cfset methodName = "insert#processEntity#"> <!--- 2013 Release 2 --->
							
							<!--- get the name of the object for logging purposes --->
							<cfset name="">
							<cfif processEntity eq "opportunity" and structKeyExists(rwEntitiesStruct[processEntity],"detail")>
								<cfset name = rwEntitiesStruct[processEntity]["detail"]>
							<cfelseif processEntity eq "organisation" and structKeyExists(rwEntitiesStruct[processEntity],"organisationName")>
								<cfset name = rwEntitiesStruct[processEntity]["organisationName"]>
							<cfelseif processEntity eq "person" and structKeyExists(rwEntitiesStruct[processEntity],"firstName") and structKeyExists(rwEntitiesStruct[processEntity],"lastName")>
								<cfset name = rwEntitiesStruct[processEntity]["firstName"] & " " & rwEntitiesStruct[processEntity]["lastName"]>
							</cfif>
							
							<cfif arguments.object eq "asset">
								<cfset matchEntityColumns = "crmAssetID">
								
							<cfelseif arguments.object eq "opportunity">
								<!--- set incoming opportunities from SalesForce as type "Lead" QUIPS Phase2 V1 (row 16) --->
								<cfquery name="getOppTypeIDForLeads" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,1,0,0)#">
									select oppTypeID from oppType where oppTypeTextID='Leads'
								</cfquery>
								<cfset rwEntitiesStruct.opportunity.oppTypeID=getOppTypeIDForLeads.oppTypeID>
								
								<!--- NJH 2013/06/20 Case 435723 - if country not set at this point, set the country of the opportunity to be that of the end customer country if they are set--->
								<cfif structKeyExists(rwEntitiesStruct.opportunity,"entityId") and (not structKeyExists(rwEntitiesStruct.opportunity,"countryID") or rwEntitiesStruct.opportunity.countryID eq 0 or rwEntitiesStruct.opportunity.countryID eq "")>
									<cfquery name="getEndCustomerCountry" datasource="#application.siteDataSource#">
										select countryId from organisation with (noLock) where organisationID = #rwEntitiesStruct.opportunity.entityID#
									</cfquery>
									<cfset rwEntitiesStruct.opportunity.countryID = getEndCustomerCountry.countryID[1]>
								</cfif>
								
								<!--- NJH 2013/06/20 Case 435723 - set the opp currency based on the opp country if it hasn't already been set--->
								<cfif (structKeyExists(rwEntitiesStruct.opportunity,"countryID") and isNumeric(rwEntitiesStruct.opportunity.countryID) and rwEntitiesStruct.opportunity.countryID neq 0) and (not structKeyExists(rwEntitiesStruct.opportunity,"currency") or rwEntitiesStruct.opportunity.currency eq "")>
									<cfset countryCurrencies = application.com.relayCountry.getCountryCurrencies(countryID=rwEntitiesStruct.opportunity.countryID)>
									<cfif countryCurrencies.recordCount>
										<cfset rwEntitiesStruct.opportunity.currency = listFirst(valueList(countryCurrencies.currencyISO))>
									</cfif>
								</cfif>
							</cfif>
			
							<!--- set organisation/person data to 'Applied' if creating --->
							<cfif listFindNoCase("organisation,person",processEntity)>
								<cfset setNewPOLDataAs = application.com.settings.getSetting("salesForce.setNewPOLDataAs")>
							</cfif>
			
							<!--- set some default values before inserting --->
							<cfset rwEntitiesStruct[processEntity] = setDefaultValues(direction="SF2RW",objectDetails=rwEntitiesStruct[processEntity],object=arguments.object,objectID=entityID)>
	
							<cftry>
								<cfset functionArgs = {setNewAs = setNewPOLDataAs,matchEntityColumns=matchEntityColumns,insertIfExists=true}> <!--- NJH 2011/03/29 - added insertIfExists=true because we've already checked that it doesn't exist --->
									
								<cfif structKeyExists(application.com.relayEntity,"insert#processEntity#")>
									<cfset functionArgs["#processEntity#Details"]=rwEntitiesStruct[processEntity]>
									<cfinvoke component=#application.com.relayEntity# method="insert#processEntity#" argumentCollection=#functionArgs# returnvariable="childMethodCallResult">
								<cfelse>
									<cfset childMethodCallResult = application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID[processEntity],entityDetails=rwEntitiesStruct[processEntity],argumentCollection=functionArgs)>
								</cfif>
								<cfcatch>
									<cfset childMethodCallResult = {isOK = false,message = cfcatch.message&"<br>"&cfcatch.detail}>
									
									<cfset warningStructure = {arguments = arguments,functionArgs = functionArgs}>
									<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce Entity Create",Severity="error",catch=cfcatch,WarningStructure=warningStructure)>
								</cfcatch>
							</cftry>
							
							<!--- if we couldn't insert, log the error --->
							<cfif not childMethodCallResult.isOK>
								
								<!--- if we failed to add a location, then don't bother with the person --->
								<cfif processEntity eq "location" and listSort(mappedObjectInfo.tableList,"text") eq "location,person">
									<cfset methodCallResult.isOK = false>
									<cfbreak>
								</cfif>
							<cfelse>
								<cfif processEntity eq "location" and listSort(mappedObjectInfo.tableList,"text") eq "location,person">
									<cfset locationID = childMethodCallResult.entityID>
									<cfset locationAdded = true>
									
									<!--- if only a contact's location's details have changed then we won't be processing the person. However, if we need to change the person's location
										because it ends up being a brand new location, we need to update the person record to put them into the new location --->
									<cfif not listFindNoCase(processOrder,"person")>
										<cfset personLocationID = {locationID = locationID}>
										<cfset application.com.relayEntity.updatePersonDetails(personID=application.com.salesForce.doesEntityExist(object=arguments.object,objectID=arguments.objectID,useRwMatchingRules=false).entityID,personDetails=personLocationID)>
									</cfif>
								<cfelse>
									<cfset entityID = childMethodCallResult.entityID>
									<cfset setSalesForceIDForEntity(entityID=entityID,salesForceID=arguments.objectId,entityType=processEntity)>
								</cfif>
							</cfif>
						</cfif>
						
						 <!--- 2013 Release 2 --->
						<cfset logStruct.method=methodName>
						
						<cfif not childMethodCallResult.isOK>
							<!--- log the error--->
							<!--- START 2014-07-22 PPB Case 439347 if there is an error code prefix it to the message (esp useful when there is no message) --->
							<cfif StructKeyExists(childMethodCallResult,"errorCode") and childMethodCallResult.errorCode neq "">
								<cfset logStruct.message=childMethodCallResult.errorCode & ": ">
							</cfif>
							<cfset logStruct.message=logStruct.message & childMethodCallResult.message>
							<!--- END 2014-07-22 PPB Case 439347 --->
							<cfset logStruct.dataStruct = duplicate(rwEntitiesStruct[processEntity])>
							<cfset logSFError(argumentCollection=logStruct)>
						<cfelse>
						
							<!--- allow some post processing --->
							<cfif structKeyExists(this,"postImport")>
								<cfset postImport(entityDetails=rwEntitiesStruct[processEntity],object=arguments.object,objectID=arguments.objectID)>
							</cfif>
						
							<!--- delete previous errors associated with the record--->
							<cfset structDelete(logStruct,"name")>
							<cfset structDelete(logStruct,"opportunityID")>
							<cfset deleteLogEntries(argumentCollection=logStruct)>
						</cfif>
						
						<cfset methodCallResult.isOK = childMethodCallResult.isOK>
						
						<!--- set any flags that may need setting (ie. custom fields that are stored in flags). Don't bother setting flags if the pol table hasn't updated successfully as it will have to get done again anyways. That would mean flags would potentially get set twice. --->
						<cfif entityID neq 0 and flagColumnList neq "" and childMethodCallResult.isOK>
							<cftry>
								<cfset flagDataResult = setFlagDataForObject(object=arguments.object,entityID=entityID,direction="SF2RW",objectDetails=objectStruct[arguments.objectID],entityType=processEntity,flagColumns=flagColumnList)>
								<cfcatch>
									<cfset flagDataResult = {isOK = false,message = cfcatch.message&"<br>"&cfcatch.detail}>
									<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce Import #arguments.object#",Severity="error",catch=cfcatch,WarningStructure=arguments)>
								</cfcatch>
							</cftry>
							
							<cfif not flagDataResult.isOK>
								<cfset logSFError(object=arguments.object,objectID=arguments.objectID,method="setFlagDataForObject",message=flagDataResult.message,direction="I",dataStruct=objectStruct[arguments.objectID])>
							<cfelse>
								<cfset deleteLogEntries(object=arguments.object,objectID=arguments.objectID,method="setFlagDataForObject",direction="I")>
							</cfif>
						</cfif>

						<!--- if we're importing an opportity and we have are using opportunity products, check if there are mappings for opportunity products in xml and do import/delete --->
						<cfif arguments.object eq "opportunity" and application.com.settings.getSetting("LeadManager.products.useOpportunityProducts") and methodCallResult.isOK>
						
							<cfif (request.synchLevel eq "field" and structKeyExists(arguments,"objectDetails") and structKeyExists(objectDetails,"Opportunity Products")) or request.synchLevel neq "field">
								<cfset oppProductMappings = getMappedObject(object="oppProducts",direction="rw2sf")>
							
								<cfif request.synchLevel neq "field">
									<cfset mappedObjectInfo = getMappedObject(object=oppProductMappings.entity,direction="SF2RW")>
									
									<cfset oppProductsToImport = "">
									<!--- manage opp products - do export and delete --->
									<cfset childMethodCallResult = send(object=oppProductMappings.entity,method="query",queryString="select #mappedObjectInfo.objectColumnList# from #oppProductMappings.entity# where opportunityID='#arguments.objectID#'",opportunityID=arguments.opportunityID,ignoreBulkProtection=true)>
									<cfif childMethodCallResult.isOK>
										
										<cfif structKeyExists(childMethodCallResult,"response") and listFindNoCase(childMethodCallResult.response.columnList,"ID")>
											<cfset childObjectStruct = application.com.structureFunctions.queryToStruct(query=childMethodCallResult.response,key="ID")>
											
											<cfset oppProductsToImport = structKeyList(childObjectStruct)>
											<cfloop collection="#childObjectStruct#" item="thisObjectID">
												<cfset exportOppProduct = import(object=oppProductMappings.entity,objectID=thisObjectID,objectDetails=childObjectStruct[thisObjectID],opportunityID=thisOpportunityID,relatedObjectID=thisOpportunityID,relatedObject="opportunity")>
												<!--- if a product fails, fail the opportunity as this is record level at this point --->
												<cfif not exportOppProduct.isOK>
													<cfset methodCallResult.isOK = false>
												</cfif>
											</cfloop>
										</cfif>
									</cfif>

										<!--- delete all products that we haven't just imported --->
										<cfquery name="deleteOppProducts" datasource="#application.siteDataSource#">
											update oppProducts set lastUpdated=GetDate(), lastUpdatedBy = #request.relayCurrentUser.userGroupId#,lastUpdatedByPerson=#request.relayCurrentUser.personID#
												where opportunityID = #entityID# 
												<cfif oppProductsToImport neq "">and isNull(#getIDMappingInfo(entityType="oppProducts").sfIDColumn#,'') not in (#listQualify(oppProductsToImport,"'")#)</cfif>
												
											delete from oppProducts where opportunityID = #entityID#
												<cfif oppProductsToImport neq "">and isNull(#getIDMappingInfo(entityType="oppProducts").sfIDColumn#,'') not in (#listQualify(oppProductsToImport,"'")#)</cfif>
										</cfquery>
									
								<!--- if doing field level opportunity synching, then do a record level synch on products --->
								<cfelse>
									<cfset synchResult = objectEntitySynch(dtdID=request.dtdID,entityTypeList="oppProducts",rwOpportunityID=entityID,sfOpportunityID=arguments.objectID)>
								</cfif>
							</cfif>
						</cfif>
						
						
					</cfloop>
				<cfelse>
					<cfset methodCallResult.isOK = false>
					<cfset logStruct.object = entityType>
					<cfset logStruct.dataStruct=duplicate(rwEntitiesStruct[entityType])>
					<cfset logStruct.message="Problem with #entityType# data">
					<!--- <cfset logSfError(argumentCollection=logStruct)> --->
				</cfif>
			
			<!--- if it's successful but we don't have a record where we are expecting one --->	
			<cfelseif methodCallResult.success and (not structKeyExists(arguments,"objectDetails") and methodCallResult.response.recordCount eq 0)>
				<cfset logSFError(object=arguments.object,objectID=arguments.objectID,method="Retrieve",message="No data returned from SalesForce.",direction="I")>
			</cfif>
		</cfif>
		
		<cfif not structKeyExists(methodCallResult,"isOK")>
			<cfset methodCallResult.isOK = methodCallResult.success>
		</cfif>
		
		<cfif arguments.object eq "opportunity" and request.synchLevel eq "field">
			<cfquery name="doesOppHavePendingProducts" datasource="#application.siteDataSource#">
				select count(1) as numProducts from salesForceSynchPending with (noLock) where opportunityID='#arguments.objectID#'
			</cfquery>
			
			<cfif doesOppHavePendingProducts.numProducts gt 0>
				<cfset oppProductsSynched = false>
			</cfif>
		</cfif>
		
		<cfset methodCallResult.entityID = entityID>
		
		<cfif methodCallResult.isOK>

			<!--- get rid of some unwanted keys --->
			<cfif structKeyExists(objectStruct,arguments.objectID)>
				<cfset structDelete(objectStruct[arguments.objectID],"ID")>
				<cfset objectFieldList = structKeyList(objectStruct[arguments.objectID])>
			</cfif>

			<cfif oppProductsSynched and arguments.object eq "opportunity">
				<cfset objectFieldList = listAppend(objectFieldList,"Opportunity Products")>
			<cfelse>
				<cfif listFindNoCase(objectFieldList,"Opportunity Products")>
					<cfset objectFieldList = listDeleteAt(objectFieldList,listFindNoCase(objectFieldList,"Opportunity Products"))>
				</cfif>
			</cfif>

			<cfquery name="deleteSynchPendingRecord" datasource="#application.siteDataSource#">
				delete from SalesForceSynchPending where objectID='#arguments.objectID#' 
					<cfif not oppProductsSynched>
					and fieldName not in ('Opportunity Products')
					</cfif>
					and ((object='#arguments.object#'
							<cfif objectFieldList neq "" and request.synchLevel eq "field">
							and fieldName in (#listQualify(objectFieldList,"'")#)
							</cfif>
							)
					<cfif structKeyExists(request,"sfSynch") and request.sfSynch>
						or (object <cfif listLen(processOrder) gt 1>in (#listQualify(processOrder,"'")#)<cfelse>= '#processOrder#'</cfif> and 
							(
						<cfset rowcount=0>
						
						<cfif request.synchLevel eq "field">
							<cfif not structIsEmpty(rwEntitiesStruct)>
								<cfloop list="#processOrder#" index="tablename">
									<!--- get rid of some unwanted keys --->
									<cfset structDelete(rwEntitiesStruct[tablename],"fieldsToNull")>
									<cfset structDelete(rwEntitiesStruct[tablename],"crm#left(tablename,3)#ID")>
									<cfif structKeyList(rwEntitiesStruct[tablename]) neq ""><cfif rowcount>or </cfif> (fieldname in (#listQualify(structKeyList(rwEntitiesStruct[tablename]),"'")#))<cfset rowCount++></cfif>
								</cfloop>
							</cfif>
							
							<cfif flagColumnList neq "">
								<cfset flagIDList = "">
								<cfloop list="#flagColumnList#" index="flagColumn">
									<cfset flagOrFlagGroup = getMappedField(object=arguments.object,table=mappedObjectInfo.entity,objectColumn=flagColumn)>
	
									<cfif listFirst(flagOrFlagGroup,".") eq "flagGroup">
										<!--- could be either a flagGroupID or a flagGroupTextID --->
										<cfif structKeyExists(request.sfFlagsInFlagGroup,listLast(flagOrFlagGroup,"."))>
											<cfset flagIDList = listAppend(flagIDList,request.sfFlagsInFlagGroup[listLast(flagOrFlagGroup,".")])>
										<cfelse>
											<cfset flagIDList = listAppend(flagIDList,request.sfFlagsInFlagGroup[application.com.flag.getFlagGroupStructure(flagGroupID=listLast(flagOrFlagGroup,".")).flagGroupTextID])>
										</cfif>
									<cfelse>
										<cfset flagIDList = listAppend(flagIDList,listLast(flagOrFlagGroup,"."))>
									</cfif>
								</cfloop>
								<cfif rowCount gt 0 and request.synchLevel eq "field">or </cfif>(fieldname like 'AnyFlag-%' and newValue in (#listQualify(flagIDList,"'")#))
							</cfif>
						</cfif>
						
						<cfif (rowCount eq 0 and flagColumnList eq "") or request.synchLevel neq "field">
							1=1
						</cfif>
							)
						)
					</cfif>
					)
			</cfquery>
			
			<cfif (listFindNoCase("organisation,location,person,pricebook,pricebookEntry,product",entityType) or (entityType eq "opportunity" and oppProductsSynched)) and entityID neq 0>
				<cfset flagEntityAsSynched(entityID=entityID,entityType=entityType,direction="import")> <!--- 2013 Release 2 --->
			</cfif>
			
			<!--- clear any log entries if we've successfully imported an object --->
			<cfset deleteLogEntries(object=arguments.object,objectID=arguments.objectID,direction="I")>
		</cfif>
		
		<cfset clearDebugInformation(entityID=arguments.objectID,entityType=arguments.object)>
		
		<cfreturn methodCallResult>
	</cffunction>
	
	
	<!--- function to export objects from relayware to salesforce --->
	<cffunction name="export" access="public" hint="Inserts/updates an object on SalesForce from Relayware" returntype="struct" output="false">
		<cfargument name="entityType" type="string" default="opportunity">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="opportunityID" type="string" required="false" default="">
		<cfargument name="entityDetails" type="struct" required="false">
		<cfargument name="deleteObject" type="boolean" default="false">
		<cfargument name="entityObjectID" type="string" required="false">
		<cfargument name="relatedObjectID" type="string" default="">  <!--- 2013 Release 2 --->
		<cfargument name="relatedObject" type="string" default="">  <!--- 2013 Release 2 --->
		
		<cfscript>
			var methodCallResult = {isOK=true,salesForceID=0};
			var objArray = arrayNew(1);
			var objectDetails = structNew();
			var salesForceID = 0;
			var ID = 0;
			var sfObject = "";
			var role = "";
			var sfObjectID = 0;
			var resultQry = "";
			var objDetailKey = "";
			var name = application.entityType[application.entityTypeID[arguments.entityType]].nameExpression;
			var uniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey;
			var thisOpportunityID = arguments.opportunityID;
			var mappedObjectInfo = getMappedObject(object=arguments.entityType);
			var objectType = mappedObjectInfo.entity;
			var baseEntity = getMappedObject(object=objectType,direction="SF2RW").entity;
			var baseEntityCreated = false;
			var processOrder = mappedObjectInfo.processOrder; // the list of objects in order of how they need to be processed. For example, a contact maps to person and location, be we need to deal with the location first
			var objectOwnerID = "";
			var accManArguments = structNew();
			var flagDetailsStruct = structNew();
			var mergeStruct = structNew();
			var oppStruct = structNew();
			var childMethodCallResult = structNew();
			var sfField = "";
			var queryString = "";
			var getEntityDetails = "";
			var getOppProductsToExport = "";
			var logStruct = structNew();
			var objectOwnerStruct = structNew();
			var ownerIDNotRqd = "";
			var getOppProductsToDelete = "";
			var flagIDList = "";
			var entityDetailStruct = structNew();
			var timeOffsetInSeconds = 0;
			var oppProductsMapping = structNew();
			var queryColumnList = mappedObjectInfo.entityColumnList;
			var deleteSynchPendingRecord = "";
			var object = "";
			var objectFieldList = "";
			var entityFieldList = "";
			var oppModifiedSinceDate = "";
			var columnName = "";
			var oppProductsToDeleteList = "";
			var crmOppProductID = "";
			var oppProductIDsToDelete = "";
			var oppProductsSynched = true;
			var doesOppHavePendingProducts = "";
			var synchResult = structNew();
			var dataStruct = structNew();
		</cfscript>
		
		<cfset logDebugInformation(entityID=arguments.entityID,entityType=arguments.entityType,method="Export #arguments.entityType#(start)",additionalDetails=arguments)>

		<cfset salesForceID = getSalesForceIDForEntity(entityID=arguments.entityID,entityType=arguments.entityType)>
		
		<!--- delete the SF object --->
		<cfif arguments.deleteObject>
			
			<cfset methodCallResult = send(method="delete",object=mappedObjectInfo.entity,ids=entityObjectID)>
			
		<cfelse>
		
			<cfif not structKeyExists(arguments,"entityDetails")>	
				<!--- if this record has a salesforceID, then it's an update. If doing updates, we only want to get fields where salesforce is not the master --->
				<cfif salesForceID neq 0>
					<cfset queryColumnList = mappedObjectInfo.synchedEntityColumnList>
				</cfif>
				
				<!--- if exporting a person and a contact is mapped to location and person, then we need to specify which organsationID, as they both have organisationID --->
				<cfif arguments.entityType eq "person" and listFindNoCase(mappedObjectInfo.tableList,"location")>
					<cfif listFindNoCase(mappedObjectInfo.entityColumnList,"organisationID") and listFindNoCase(queryColumnList,"organisationID")>
						<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,"organisationID")),"person.organisationID")>
					</cfif>
					<cfif listFindNoCase(mappedObjectInfo.entityColumnList,"countryID") and listFindNoCase(queryColumnList,"countryID")>
						<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,"countryID")),"location.countryID")>
					</cfif>
					<!--- case 433709 - dealt with active being ambiguous; Case 439090, check that active exists in queryColumnList before trying to delete it --->
					<cfif listFindNoCase(mappedObjectInfo.entityColumnList,"active") and listFindNoCase(queryColumnList,"active")>
						<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,"active")),"person.active")>
					</cfif>
				
				<cfelseif arguments.opportunityID neq "">
					<!--- remove ambiguous columns names, as we will be joining to org table again --->
					<cfif arguments.entityType eq "organisation">
						<cfloop list="#queryColumnList#" index="columnName">
							<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,columnName)),"#arguments.entityType#.#columnName#")>
						</cfloop>
					<cfelseif arguments.entityType eq "location">
						<cfif listFindNoCase(mappedObjectInfo.entityColumnList,"countryID") and listFindNoCase(queryColumnList,"countryID")>
							<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,"countryID")),"location.countryID")>
						</cfif>
						<cfif listFindNoCase(mappedObjectInfo.entityColumnList,"organisationID") and listFindNoCase(queryColumnList,"organisationID")>
							<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,"organisationID")),"location.organisationID")>
						</cfif>
					</cfif>
				</cfif>
			
				<cfif arguments.entityType eq "opportunity">
					<cfif listFindNoCase(mappedObjectInfo.entityColumnList,"entityID")>
						<cfset queryColumnList = listAppend(listDeleteAt(queryColumnList,listFindNoCase(queryColumnList,"entityID")),"opportunity.entityID")>
					</cfif>
				</cfif>
			
				<!--- get the opportunity/organisation/person details --->
				<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
					select #arguments.entityType#.#uniqueKey#,#queryColumnList#, <cfif name neq "">#arguments.entityType#.#preserveSingleQuotes(name)#<cfelse>''</cfif> as name
					<cfif listFindNoCase("organisation,location,person",arguments.entityType) and arguments.opportunityID neq ""> 
						,o2.organisationTypeID as OrgType
					</cfif>
					<cfif listFindNoCase("organisation,location,person,opportunity",arguments.entityType)> 
						,case when bfd.flagID is null then 0 else 1 end as suspended
					<cfelse>
						,0 as suspended
					</cfif>
					from #arguments.entityType# 
						<cfif arguments.entityType eq "person" and listFindNoCase(mappedObjectInfo.tableList,"location")> inner join location on person.locationID = location.locationID</cfif>
						<cfif listFindNoCase("organisation,location,person",arguments.entityType) and arguments.opportunityID neq ""> 
							inner join organisation o2 on #arguments.entityType#.organisationID = o2.organisationID
						</cfif>
						<cfif listFindNoCase("organisation,location,person,opportunity",arguments.entityType)> 
							left join booleanFlagData bfd with (noLock) on bfd.entityID = #arguments.entityType#.#uniqueKey# and bfd.flagID = #application.com.flag.getFlagStructure(flagID="#left(arguments.entityType,3)#SalesForceSynchSuspended").flagID#
						</cfif>
		 			where #arguments.entityType#.#uniqueKey# = #arguments.entityID#
				</cfquery>
		
				<!--- the entity no longer exists. Probably never ever get here --->
				<cfif getEntityDetails.recordCount neq 1>
					<cfset methodCallResult.isOK = false>
					<cfset logSFError(opportunityID=thisOpportunityID,object=objectType,entityID=arguments.entityID,method="create",message="Unable to retreive details for #arguments.entityType# with #uniqueKey# of #arguments.entityID#",direction="E")>
					<cfset methodCallResult.message = "Unable to retrieve details for #arguments.entityType# with #uniqueKey# of #arguments.entityID#">
					
				<!--- we arrive here when an opportunity tries to synch, for example, but it reguires an org that is currently suspended. Don't try to export the org if it's been suspended --->
				<cfelseif getEntityDetails.suspended[1]>
					<cfset methodCallResult.isOK = false>
					<cfset methodCallResult.message = "#arguments.entityType# #name# with #uniqueKey# of #arguments.entityID# has been suspended from synching. Please unsuspend to enable a synch.">
				<cfelse>
					<cfset entityDetailStruct = application.com.structureFunctions.queryRowToStruct(query=getEntityDetails,row=1)>
				</cfif>
	
				<cfset flagIDList = mappedObjectInfo.flagList>
			<cfelse>
			
				<!--- this is simply to get the entity name for logging purposes --->
				<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
					select <cfif name neq "">#arguments.entityType#.#preserveSingleQuotes(name)#<cfelse>''</cfif> as name
					from #arguments.entityType# 
					where #arguments.entityType#.#uniqueKey# = #arguments.entityID#
				</cfquery>
			
				<cfset entityDetailStruct = duplicate(arguments.entityDetails)>
				<cfset entityDetailStruct[uniqueKey] = arguments.entityID>
			</cfif>
	
			<cfif methodCallResult.isOK>
				<cfif arguments.entityType eq "opportunity">
					<cfset thisOpportunityID = arguments.entityID>
					<cfset oppProductsMapping = getMappedObject(object="oppProducts")>
				</cfif>
		
				<cfset objectDetails = convertObject(objectDetails=entityDetailStruct,object=arguments.entityType,direction="RW2SF",opportunityID=thisOpportunityID,relatedObject=arguments.relatedObject,relatedObjectID=arguments.relatedObjectID)>
		
				<!--- if the only fields that are passed throught are flag fields,then the objectType won't exist; so just create an empty one as the code below expects it --->
				<cfif not structKeyExists(objectDetails,objectType)>
					<cfset objectDetails[objectType] = structNew()>
				</cfif>

				 <!--- 2013 Release 2 --->
				<cfset logStruct = {direction = "E",opportunityID = thisOpportunityID,entityID = arguments.entityID,name=getEntityDetails.name[1],method="export"}>

				<cfif structKeyExists(arguments,"relatedObjectID")>
					<cfset logStruct.relatedObjectID=arguments.relatedObjectID>
				</cfif>
				<cfif structKeyExists(arguments,"relatedObjectID")>
					<cfset logStruct.relatedObject=arguments.relatedObject>
				</cfif>
				
				<cfif objectDetails.isOK>
					<cfset logStruct.entityDetails=entityDetailStruct>
				
					<cfloop list="#processOrder#" index="sfObject">
						<!--- get the name of the object for logging purposes --->
						<cfset name = getEntityDetails.name>
						
						<cfset mergeStruct = structNew()>
						<cfset mergeStruct = duplicate(entityDetailStruct)>
						<cfset structAppend(mergeStruct,objectDetails[objectType])>
						
						<!--- setting fields from flag values - if any flags have changes for an existing entity, then they will come through in the entityDetails struct as an 'anyFlag' field --->
						<cfif structKeyExists(entityDetailStruct,"anyFlag")>
							<cfset flagIDList = entityDetailStruct["anyFlag"]>
						</cfif>
						
						<cfif not structKeyExists(arguments,"entityDetails") or (structKeyExists(arguments,"entityDetails") and flagIDList neq "")>
							<cftry>
								<cfset flagDetailsStruct = setFlagDataForObject(object=arguments.entityType,objectDetails=mergeStruct,entityID=arguments.entityID,direction="RW2SF",entityType=arguments.entityType,flagIDList=flagIdList)>
								<cfcatch>
									<cfset flagDetailsStruct.isOK = false>
									<cfset flagDetailsStruct.message = cfcatch.message&"<br>"&cfcatch.detail>
									<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce Export #arguments.entityType#",Severity="error",catch=cfcatch,WarningStructure=arguments)>
								</cfcatch>
							</cftry>
							<cfif not flagDetailsStruct.isOK>
								<cfset logSFError(object=arguments.entityType,entityID=arguments.entityID,method="setFlagDataForObject",message=flagDetailsStruct.message,direction="E",dataStruct=mergeStruct)>
							<cfelse>
								<cfset deleteLogEntries(object=arguments.entityType,entityID=arguments.entityID,method="setFlagDataForObject",direction="E")>
							</cfif>
						</cfif>
		
						<!--- sometimes the struct won't exist..like opportunity and opportunityLineItem.... --->
						<cfif structKeyExists(objectDetails,sfObject)>
							<!--- we need to make sure that the original fieldsToNull do not get overwritten by the flags fieldsToNull --->
							<cfif structKeyExists(flagDetailsStruct,"fieldsToNull") and structKeyExists(objectDetails[sfObject],"fieldsToNull")>
								<cfset flagDetailsStruct.fieldsToNull = listAppend(objectDetails[sfObject].fieldsToNull,flagDetailsStruct.fieldsToNull)>
							</cfif>
							<cfset structAppend(objectDetails[sfObject],flagDetailsStruct)>
							<cfset logStruct.objectDetails=objectDetails[sfObject]>
						<cfelse>
							<cfset logStruct.objectDetails=objectDetails[objectType]>
						</cfif>
		
						<cfset dataStruct = duplicate(logStruct)> <!--- this captures the data that is going to be exported, in case it fails, so we can analyse it --->
		
						<!--- if we're dealing with the primary object --->
						<cfif sfObject eq objectType>
							
							<cfset objArray[1] = objectDetails[sfObject]>
							
							<!--- NJH 2013/10/23 Roadmap2 Release 2 Item 33 check if object exists on salesforce --->
							<cfif salesForceID eq 0>
								<cfset salesForceID = doesSFObjectExist(object=objectType,objectDetails=objArray[1])>
								<!--- map the RW entity to the matched SF object --->
								<cfif salesForceID neq 0>
									<cfset setSalesForceIDForEntity(entityID=arguments.entityID,salesForceID=salesForceID,entityType=arguments.entityType)>
								</cfif>
							</cfif>
		
							<cfset logStruct.object = objectType>
							<cfset logStruct.dataStruct=dataStruct>
							
							<!--- get the Account manager (or object owner) for a new SF object if it is required--->
							<cfif salesForceID eq 0>
								<cfset ownerIDNotRqd = "true">
								
								<!--- NYB 2010-12-23 - only get the Account Manager for objects that have OwnerID as a field in the [SF] table --->
								<cfset putSFDCObjectIntoMemory(object=objectType)>
								<cfif ListFindNoCase(StructKeyList(application.sfObjects['#objectType#'],','), 'OwnerId') gt 0>
									<cfset ownerIDNotRqd = "false">
									<cfset accManArguments = {entityType=arguments.entityType,entityID=arguments.entityID,object=objectType}>
									<cfif thisOpportunityID neq "">
										<cfset accManArguments.opportunityID = thisOpportunityID>
									</cfif>
									<cfset objectOwnerStruct = getAccountManagerForEntity(argumentCollection=accManArguments)>
									<cfset objectOwnerID = objectOwnerStruct.objectOwnerID>	
								</cfif>
								
								<cfif objectOwnerID neq "" or ownerIDNotRqd>
									<cfif not ownerIDNotRqd>
										<cfset objectDetails[sfObject]["ownerID"] = objectOwnerID>
										<cfset deleteLogEntries(object=objectType,entityID=arguments.entityID,method="AssignAccountManager",direction="E")>
									</cfif>
								<cfelse>
									<cfset methodCallResult.isOk = false>
									<cfset methodName="AssignAccountManager">
									<cfset methodCallResult.message=objectOwnerStruct.message>
								</cfif>
							</cfif>
							
							<cfif methodCallResult.isOK>
								<!--- allow some pre processing --->
								<cfif structKeyExists(this,"preExport")>
									<cfset objArray[1] = preExport(objectDetails=objArray[1],object=arguments.entityType,entityID=arguments.entityID,objectID=salesForceID)>
								</cfif>
							
								<!--- if a salesForceID exists for the entity, then do an update --->
								<cfif salesForceID neq 0>
									<cfset methodName = "update"> <!--- 2013 Release 2--->
									
									<cfset objectDetails[objectType]["ID"] = salesForceID> <!--- set the ID field for the update --->
									<!--- check if this structure has any fields to go across. If there is only a sinlge field, that will be the ID. So, we don't need to update the opportunity. We could be in this situation
										if the items which triggered the opportunity synch are the products or the roles
									 --->
									<cfif structCount(objArray[1]) gt 1>
										<cfset methodCallResult = send(object=objectType,method="update",objects=objArray,objectID=salesForceID,name=getEntityDetails.name,opportunityID=thisOpportunityID,entityID=arguments.entityID)>
										<cfset methodCallResult.isOK = methodCallResult.success>
									<cfelse>
										<cfset methodCallResult.isOK = true>
									</cfif>
									
								<!--- no salesForceID has been set. Assume it is a create --->
								<cfelse>
									
									<cfset methodName = "create"> <!--- 2013 Release 2--->
			
									<!--- set some default values when doing a create. --->
									<cfset objArray[1] = setDefaultValues(direction="RW2SF",objectDetails=objArray[1],object=arguments.entityType,entityID=arguments.entityID,mergeStruct=entityDetailStruct)>
	
									<cfset methodCallResult = send(object=objectType,method="create",objects=objArray,entityID=arguments.entityID,name=getEntityDetails.name,opportunityID=thisOpportunityID)>
									<cfif baseEntity eq arguments.entityType>
										<cfset baseEntityCreated = true>
									</cfif>
	
									<cfset methodCallResult.isOK = methodCallResult.success>
	
									<cfif methodCallResult.success>
										<cfset resultQry = methodCallResult.response>
										
										<cfif resultQry.recordCount eq 1 and resultQry.success[1]>
											<cfset salesForceID = resultQry.ID[1]>
											<cfset setSalesForceIDForEntity(entityID=arguments.entityID,salesForceID=salesForceID,entityType=arguments.entityType)>
										</cfif>
									<cfelse>
										<cfset logStruct.fields=objectDetails.fieldname>
										<cfset methodCallResult.message = "Unable to create #objectType# as error encountered setting field #objectDetails.fieldname#">
									</cfif>
								</cfif>
							</cfif>
							
							 <!--- 2013 Release 2 --->
							<cfif not methodCallResult.isOk>
								<cfset logStruct.method = methodName>
								<cfset logStruct.message = methodCallResult.message>
								<cfset logSFError(argumentCollection=logStruct)>
							<cfelse>
							
								<!--- run post export function if it exists. Meant as a hook for doing special things --->
								<cfif structKeyExists(this,"postExport")>
									<cfset postExport(objectDetails=objArray[1],object=arguments.entityType,entityID=arguments.entityID,objectID=salesForceID)>
								</cfif>
								<cfset deleteLogEntries(object=objectType,entityID=arguments.entityID,method=methodName,direction="E")>
							</cfif>
							
						<!--- it will be an object that is related to the main object that we are updating, like partner or opportunityContactRole which is needed for an opportunity --->
						<cfelse>
		
							<!--- if the base object wasn't successfully created, then don't bother added records that are dependant on it --->
							<cfif salesForceID neq 0>
								<cfif sfObject eq "opportunityLineItem" and application.com.settings.getSetting("LeadManager.products.useOpportunityProducts")>
								
									<!--- if the opportunity is set to record level synching, then we just get all the products of the opportunity to be exported --->
									<cfif request.synchLevel neq "field">
									
										<cfquery name="getOppProductsToExport" datasource="#application.siteDataSource#">
											select oppProductID from oppProducts where opportunityID=#arguments.entityID# and productOrGroup = 'P'
										</cfquery>
										
										<!--- export any products that need exporting --->
										<cfloop query="getOppProductsToExport">
											<cfset childMethodCallResult = export(entityType="oppProducts",entityID=oppProductID,opportunityID=thisOpportunityID,relatedObjectID=arguments.entityID,relatedObject="opportunity")>  <!--- 2013 Release 2 --->
											<cfif not childMethodCallResult.isOK>
												<cfset oppProductsSynched = false> <!--- if we had a problem creating one of the products, we don't want to mark the opportunity as 'synched' --->
												<cfset methodCallResult.message = "Error exporting oppProduct #oppProductId#.">
												<cfif structKeyExists(childMethodCallResult,"message")>
													<cfset methodCallResult.message = methodCallResult.message&" "&childMethodCallResult.message>
												</cfif>
											</cfif>
										</cfloop>
										
										<!--- delete any products that need deleting --->
										<cfquery name="getOppProductsToDelete" datasource="#application.siteDataSource#">
											select crmOppProductID from oppProductsDel where opportunityID=#arguments.entityID# and crmOppProductID is not null
										</cfquery>
										
										<cfif getOppProductsToDelete.recordCount gt 0>
											<cfset oppProductsToDeleteList = listQualify(valueList(getOppProductsToDelete.crmOppProductID),"'")>
											<cfset childMethodCallResult = send(method="query",object="opportunityLineItem",queryString="select ID from opportunityLineItem where ID in (#oppProductsToDeleteList#)",opportunityID=thisOpportunityID,ignoreBulkProtection=true)>
											<cfset oppProductIDsToDelete = "">
											<cfif childMethodCallResult.isOK and listFindNoCase(childMethodCallResult.response.columnList,"ID")>
												<cfset oppProductIDsToDelete = valueList(getOppProductsToDelete.crmOppProductID)>
												<cfloop list="#valueList(getOppProductsToDelete.crmOppProductID)#" index="crmOppProductID">
													<cfif not listFindNoCase(valueList(childMethodCallResult.response.ID),crmOppProductID)>
														<cfset oppProductIDsToDelete = listDeleteAt(oppProductIDsToDelete,listFindNoCase(oppProductIDsToDelete,crmOppProductID))>
													</cfif>
												</cfloop>
											</cfif>
											
											<cfif oppProductIDsToDelete neq "">
												<cfset childMethodCallResult = send(method="delete",ids=oppProductIDsToDelete,object="opportunityLineItem",opportunityID=thisOpportunityID)>
												<cfif not childMethodCallResult.isOK>
													<cfset oppProductsSynched = false> <!--- if we had a problem creating one of the products, we don't want to mark the opportunity as 'synched' --->
													<cfset methodCallResult.message = "Error deleting oppProducts on SalesForce.">
													<cfif structKeyExists(childMethodCallResult,"message")>
														<cfset methodCallResult.message = methodCallResult.message&" "&childMethodCallResult.message>
													</cfif>
												</cfif>
											</cfif>
										</cfif>
										
										<cfset methodCallResult.isOK = oppProductsSynched>
									
									<cfelse>
										<!--- if doing field level opportunity synching, then do a record level synch on products --->
										<cfset synchResult = objectEntitySynch(dtdID=request.dtdID,entityTypeList="oppProducts",rwOpportunityID=arguments.entityID,sfOpportunityID=salesForceID)>
									</cfif>
									
								<cfelseif sfObject neq "opportunityLineItem">
									<!--- 
										TODO: re-visit this as it's a bit hard-coded at the moment.
										loop through the list of structures in objectDetails.. for an opportunity object, this will include the partner and opportunityContactRole structures.. 
									--->
									<cfloop collection="#objectDetails#" item="objDetailKey">
										<cfif sfObject eq listFirst(objDetailKey,application.delim1)>
											<cfset role = listLast(objDetailKey,application.delim1)>
											<cfset objectDetails[objDetailKey]["role"] = role>
											
											<cfif sfObject eq "opportunityContactRole">
												<cfset sfField = "contactID">
											<cfelse>
												<cfset sfField = "accountToID">
											</cfif>
		
											<cfset ID ="">
											<cfif structKeyExists(objectDetails[objDetailKey],sfField)>
												<cfset ID = objectDetails[objDetailKey][sfField]>
											</cfif>
											<cfset objectDetails[objDetailKey]["opportunityID"] = salesForceID>
											<cfset objectDetails[objDetailKey]["role"] = role>
											<cfset queryString = "select ID,opportunityId,#sfField# from #sfObject# where opportunityID = '#salesForceID#' and role = '#role#'">
											
											<!--- if the accountmanager, contact person, sponsor, partnerLocationId or distiLocationID haven't been set in relayware,
												or if the opportunity wasn't successfully created, then don't bother inserting it in sales force --->
											<cfset sfObjectID = 0>
											<cfset childMethodCallResult = send(object=sfObject,method="query",queryString=queryString,role=role,opportunityID=thisOpportunityID,ignoreBulkProtection=true)>
											<cfset resultQry = childMethodCallResult.response>
											
											<cfif childMethodCallResult.success>
												<cfset objArray[1] = objectDetails[objDetailKey]>
												
												<!--- if no opportunity partner or opportunity Contact exists, then create one if one has been set on Relayware. --->
												<cfif listFindNoCase(resultQry.columnList,"size") and resultQry.size[1] eq 0 and ID neq "">
													<cfset childMethodCallResult = send(object=sfObject,method="create",objects=objArray,entityID=arguments.entityId,opportunityID=thisOpportunityID,role=role)>
												
												<!--- if the role or contact has changed.. we will get a query result --->	
												<cfelseif listFindNoCase(resultQry.columnList,"opportunityID") and resultQry.recordCount gt 0 and ID neq "">
													<!--- delete, then re-create as more may have been  --->
													<cfset childMethodCallResult = send(object=sfObject,method="delete",ids=valueList(resultQry.ID),entityID=arguments.entityId,opportunityID=thisOpportunityID,role=role)>
													<cfif childMethodCallResult.success>
														<cfset childMethodCallResult = send(object=sfObject,method="create",objects=objArray,entityID=arguments.entityId,opportunityID=thisOpportunityID,role=role)>
													</cfif>
													<!--- <cfset childMethodCallResult = send(object=sfObject,method="update",objectID=resultQry.ID,objects=objArray,opportunityID=arguments.opportunityID)> --->
													
												<!--- if a role has been set up on SalesForce but has been removed on Relayware, then remove them on SalesForce. Note: This will get rid of all roles
													of this type which have been set on SalesForce --->
												<cfelseif listFindNoCase(resultQry.columnList,"opportunityID") and resultQry.recordCount gt 0 and ID eq "">
													<cfset childMethodCallResult = send(object=sfObject,method="delete",ids=valueList(resultQry.ID),opportunityID=thisOpportunityID,role=role)>
												</cfif>
													
												<cfif not childMethodCallResult.success>
													<cfset methodCallResult.isOK = false>
													<cfset methodCallResult.message = "Error synchronising opportunity object #sfObject# for opportunity #arguments.entityId#">
													<cfif structKeyExists(childMethodCallResult,"message")>
														<cfset methodCallResult.message = methodCallResult.message&" "&childMethodCallResult.message>
													</cfif>
												<cfelse>
													<!--- delete any log entries hanging around. They won't get picked up for deleting in the send method as the data state could be changing, meaning that the errors could have 
														been created while trying to do a delete, but then the next call may be a call to a delete if the sponsor was removed, for example. --->
													<cfset deleteLogEntries(method="create",object=sfObject,entityID=arguments.entityID,opportunityID=thisOpportunityID,role=role)>
													<cfset deleteLogEntries(method="delete",object=sfObject,entityID=arguments.entityID,opportunityID=thisOpportunityID,role=role)>
												</cfif>
											</cfif>
										</cfif>
									</cfloop>
								</cfif>
							</cfif>
						</cfif>
					</cfloop>
	
					<!--- we need to set the "Synched" flag on the opportunity if ....
						1. The opp has a salesforce ID and had products which have all gone across ok or
						2. The opp has a salesforce ID and had no products to go across
					--->
					<cfif methodCallResult.isOK and listFindNoCase("opportunity,organisation,location,person,pricebook,pricebookEntry,product",arguments.entityType) and salesForceID neq 0>
						<cfset flagEntityAsSynched(entityID=arguments.entityID,entityType=arguments.entityType,direction="export")>  <!--- 2013 Release 2 --->
					</cfif>
					
				<!--- if object details is not ok, then return a false so that we don't keep attempting to insert everything for this entity. A way to cut down on unncessary processing power --->
				<cfelse>
					<cfset methodCallResult.isOK = false>
					<cfset logStruct.object = objectType>
					<cfset logStruct.dataStruct=dataStruct>
					<cfset logStruct.message="Problem with #objectType# data">
					<!--- <cfset logSfError(argumentCollection=logStruct)> --->
				</cfif>
		
			<!--- entity was suspended.. so don't try and export --->
			</cfif>
		</cfif>
		
		<!--- check if we have any products that are pending. If so, then we don't flag the opportunity as synched --->
		<cfif arguments.entityType eq "opportunity" and request.synchLevel eq "field">
			<cfquery name="doesOppHavePendingProducts" datasource="#application.siteDataSource#">
				select count(1) as numProducts from salesForceSynchPending with (noLock) where opportunityID='#arguments.entityID#'
			</cfquery>
			
			<cfif doesOppHavePendingProducts.numProducts gt 0>
				<cfset oppProductsSynched = false>
			</cfif>
		</cfif>
		
		
		<cfif methodCallResult.isOK>

			<cfset structDelete(entityDetailStruct,uniqueKey)>
			<!--- if a base entity has just been created, then we want to get rid of all pending records, so delete all keys in the structure. --->
			<cfif baseEntityCreated>
				<cfset structClear(entityDetailStruct)>
			</cfif>
			<cfset entityFieldList = structKeyList(entityDetailStruct)>
			<cfset objectFieldList = "">
			<cfloop list="#processOrder#" index="object">
				<cfif structKeyExists(objectDetails,object)>
					<cfset objectFieldList = listAppend(objectFieldList,structKeyList(objectDetails[object]))>
				</cfif>
			</cfloop>

			<!--- if all the opportuntiy products have synched, then delete the  Opportunity Products field; otherwise, remove it from the 
				list of field names to delete. --->
			<cfif oppProductsSynched and arguments.entityType eq "opportunity">
				<cfset entityFieldList = listAppend(entityFieldList,"Opportunity Products")>
			<cfelse>
				<cfif listFindNoCase(entityFieldList,"Opportunity Products")>
					<cfset entityFieldList = listDeleteAt(entityFieldList,listFindNoCase(entityFieldList,"Opportunity Products"))>
				</cfif>
			</cfif>

			<cfquery name="deleteSynchPendingRecord" datasource="#application.siteDataSource#">
				delete from SalesForceSynchPending where (entityID=#arguments.entityID# and <cfif listLen(mappedObjectInfo.tableList) gt 1>object in (#listQualify(mappedObjectInfo.tableList,"'")#)<cfelse>object='#mappedObjectInfo.tableList#'</cfif> and
					<cfif not oppProductsSynched>
					fieldName not in ('Opportunity Products') and
					</cfif>
					<cfif request.synchLevel eq "field" and (entityFieldList neq "" or flagIdList neq "")>
						(
						<cfif entityFieldList neq "">(fieldName in (#listQualify(entityFieldList,"'")#))</cfif>
						<cfif flagIdList neq ""><cfif entityFieldList neq "">or </cfif>(fieldName like 'AnyFlag-%' and newValue in (#listQualify(flagIDList,"'")#))</cfif>
						)
					<cfelse>
						1=1
					</cfif>
					)
				<cfif structKeyExists(request,"sfSynch") and request.sfSynch>
					<cfif salesForceID neq "">
						or (objectID='#salesForceID#' and object='#mappedObjectInfo.entity#' <cfif request.synchLevel eq "field" and objectFieldList neq "">and fieldname in (#listQualify(objectFieldList,"'")#)</cfif>)
					</cfif>
				</cfif>
			</cfquery>
			
			<!--- clear any log entries if we've successfully exported an object --->
			<cfset deleteLogEntries(object=arguments.entityType,entityID=arguments.entityID,direction="E")>
		</cfif>
		
		<cfset methodCallResult.salesForceID = salesForceID>
		
		<cfset clearDebugInformation(entityID=arguments.entityID,entityType=arguments.entityType)>

		<cfreturn methodCallResult>
	</cffunction>

	<cffunction name="flagModifiedEntitiesAsSynching" access="public" returnType="query" hint="Flags as synching any entities that have been modified so that they get picked up in an export" output="false">
		<cfargument name="entityTypeList" type="string" default="organisation,location,person"> <!--- if doing POL data, make sure that it is passed through as organsition,location,person! Order is important at the moment --->
		<cfargument name="modifiedFrom" type="date" required="true"> <!--- must be passed in as UTC --->
		<cfargument name="modifiedTo" type="date" required="true"> <!--- must be passed in as UTC --->
		
		<cfscript>
			var getModifiedEntities = queryNew("entityType,entityID,organisationID,approved");
			var entity = "";
			var mnemonic = "";
			var mappedObject = structNew();
			var crmID = "";
			var triggerPoint = "";
			var includeOppProducts = false;
			var oppProductMappings = structNew();
			var polSynchType = application.com.settings.getSetting("salesForce.ploSynchType");
			var timeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset();
			var selectFlaggedEntitiesSql = "";
			var setEntitiesAsSynching = "";
			var contactLocationPerson = false; //tells us whether a contact is mapped to a location and person
			var whereClauseStructure = application.com.settings.getSetting("salesForce.whereClause.entity");
			var whereClause = "";
			var createTempTable = "";
			var getModEntities = "";
			var baseEntity = "";
			var getOrgType = "";
			var fieldNameList = "";
			var baseEntityUniqueKey = "";
			var entityUniqueKey = "";
			var loopCount = 1;
			var synchedFlagIDList = "";
			var queryInfo = structNew();
			var baseJoin = "";
			var approvalEntity = "";
		</cfscript>
		
		<cfquery name="createTempTable" datasource="#application.siteDataSource#">
			if object_id('tempdb..##modifiedEntity') is not null
			begin
			   drop table ##modifiedEntity
			end
		</cfquery>

		<cftry>
			
			<cfset queryInfo = getModifiedEntriesQueryStatement(argumentCollection=arguments)>
			
			<cfquery name="getModifiedEntities" datasource="#application.siteDataSource#" result="getModEntities">
				select distinct * into ##modifiedEntity from (
				<cfloop list="#arguments.entityTypeList#" index="entity">
					
					<cfset whereClause = queryInfo.whereClause[entity]>
					<cfset baseJoin = queryInfo.baseJoin[entity]>
				
					<cfif not (listFindNoCase("organisation,location,person",entity) and polSynchType eq "None")>
					
						<cfset mappedObject = getMappedObject(object=entity)>
						<cfset synchedFlagIDList = mappedObject.synchedFlagList>
						<cfset baseEntity = entity>
						<cfset approvalEntity = "">
						
						<!--- in the standard SF implementation, a contact is mapped to a location and person and so we need to get modifications
							to both entities --->
						<cfif listFindNoCase("location,person",entity) and listSort(mappedObject.tableList,"text") eq "location,person">
							<cfset contactLocationPerson = true>
							<cfset baseEntity = "person">
						</cfif>
						
						<cfif baseEntity eq "person">
							<cfset approvalEntity = "person">
						<cfelseif listFindNoCase("organisation,location",baseEntity)>
							<cfset approvalEntity = "organisation">
						</cfif>
				
						<cfset mnemonic = left(entity,1)>
						<cfif entity eq "opportunity">
							<cfset mnemonic = "opp">
						</cfif>
						
						<cfset crmID = getIDMappingInfo(entityType=baseEntity).sfIDColumn>
						<cfset baseEntityUniqueKey = application.entityType[application.entityTypeID[baseEntity]].uniqueKey>
						<cfset entityUniqueKey = application.entityType[application.entityTypeID[entity]].uniqueKey>
	
						<!--- if we're exporting opportunities, we may need to look at oppProduct modifications (if it's been mapped!!) --->
						<cfif entity eq "opportunity">
							<cfset oppProductMappings =getMappedObject(object="oppProducts")>
							<cfif listLen(oppProductMappings.entityColumnList) gt 0>
								<cfset includeOppProducts = true>
							</cfif>
						<cfelse>
							<cfset includeOppProducts = false>
						</cfif>
						
						<cfif listFindNoCase("organisation,person",baseEntity)>
							<cfset synchedFlagIDList = listAppend(synchedFlagIDList,application.com.flag.getFlagStructure("#left(baseEntity,3)#Approved").flagID)>
						</cfif>
						
						<cfif loopCount neq 1>
							union
						</cfif>
				
						<!--- NJH 2012/04/03 CASE 427161 - reworked the modregister joins when dealing with opportunities and their products... put the entityType with the entityId when joining to modRegister --->
						select distinct #baseEntity#.#baseEntityUniqueKey# as entityID, '#baseEntity#' as entity
						<cfif listFindNoCase("organisation,location,person",baseEntity)>
						,#baseEntity#.organisationID,isNull(#approvalEntity#Approved.entityID,0) as approved
						,ot.typeTextID as orgType
						<cfelseif entity eq "opportunity">
						,opportunity.stageID, os.sortOrder
						</cfif>
						from 
							#entity# with (noLock)
							<cfif listFindNoCase("organisation,location,person",entity)>
								<cfif listFindNoCase("location,person",entity)>
								inner join organisation with (noLock) on organisation.organisationID = #entity#.organisationID
								</cfif>
								<cfif contactLocationPerson>
									<cfif entity eq "person">
									inner join location with (noLock) on person.locationID = location.locationID
									<cfelseif entity eq "location">
									inner join person with (noLock) on person.locationID = location.locationID
									</cfif>
								</cfif>
								inner join organisationType ot with (noLock) on organisation.organisationTypeID = ot.organisationTypeID	 
							<cfelseif entity eq "opportunity">
								inner join oppStage os with (noLock) on os.opportunityStageID = opportunity.stageID
								<cfif includeOppProducts>
								left join oppProducts op with (noLock) on op.opportunityID = opportunity.opportunityID and op.productOrGroup = 'P'
								left join oppProductsDel opd with (noLock) on opd.opportunityID = opportunity.opportunityID and opd.productOrGroup = 'P'
								</cfif>
							</cfif>
							<!--- #preserveSingleQuotes(baseJoin)# --->
							inner join modRegister mr with (noLock) on 
								((mr.recordID = #baseEntity#.#entityUniqueKey# and mr.entityTypeID = #application.entityTypeID[entity]#)<cfif includeOppProducts>or (mr.recordID = op.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#) or (mr.recordID = opd.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)</cfif>)
							left join ModEntityDef as med with (noLock) on 
								(med.TableName = '#entity#' <cfif includeOppProducts>or med.tableName = 'oppProducts' or med.tableName = 'oppProducts'</cfif>) and mr.ModEntityID = med.ModEntityID
							left join booleanFlagData synch with (noLock) on synch.entityID = #baseEntity#.#baseEntityUniqueKey# and synch.flagID in (#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynching").flagID#,#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynchSuspended").flagID#)
							<cfif listFindNoCase("organisation,person",approvalEntity)>
							left join booleanFlagData #approvalEntity#Approved with (noLock) on #approvalEntity#Approved.entityID = #approvalEntity#.#application.entityType[application.entityTypeID[approvalEntity]].uniqueKey# and #approvalEntity#Approved.flagID = #application.com.flag.getFlagStructure("#left(approvalEntity,3)#Approved").flagID#
							</cfif>
						where
							<!--- #preserveSingleQuotes(whereClause)#  --->
							mr.modDate between dateadd(s,-(#timeOffsetInSeconds#),#request.utcStartDateTime#) and dateadd(s,-(#timeOffsetInSeconds#),#request.utcEndDateTime#)
							and  (
									(
									<cfif (listFindNoCase("organisation,location,person",entity) and polSynchType eq "Full") or not listFindNoCase("organisation,location,person",entity)>
										<cfset fieldNameList = mappedObject.synchedEntityColumnList>
										<cfif includeOppProducts>
											<cfset fieldNameList = listAppend(fieldNameList,oppProductMappings.entityColumnList)>
										</cfif>
											(
											<cfif fieldNameList neq "">
												(
													med.FieldName in (#listQualify(fieldNameList,"'")#) 
													and
													(mr.Action='#mnemonic#M' <cfif includeOppProducts>or mr.Action = 'opppM'</cfif>)
												)
											or 
											</cfif>
											(mr.Action = '#mnemonic#A' <cfif includeOppProducts>or mr.Action in ('opppA','opppD')</cfif>)
											)
									<cfelseif mappedObject.synchedEntityColumnList neq "">
										med.FieldName in (#listQualify(mappedObject.synchedEntityColumnList,"'")#)
										and (mr.Action = '#mnemonic#M')
									<cfelse>
										1=0
									</cfif>
									)
								<cfif synchedFlagIDList neq "">
									or
									(mr.Action in ('FA','FD','FM')
										and mr.flagID in (#synchedFlagIDList#)
									)
								</cfif>
								)

						<cfif listFindNoCase("organisation,location,person",entity) and polSynchType neq "Full">and #crmID# is not null</cfif>
							and synch.entityID is null
							and isNull(actionByCF,#request.relayCurrentUser.userGroupID#) <> #request.relayCurrentUser.userGroupID#
						<cfif structKeyExists(whereClauseStructure,entity) and whereClauseStructure[entity] neq "">
							<cfset whereClause = whereClauseStructure[entity]>
							and #preserveSingleQuotes(whereClause)#
						</cfif>
						<cfif listFindNoCase("location,person",entity) and structKeyExists(whereClauseStructure,"organisation") and whereClauseStructure.organisation neq "">
							<cfset whereClause = whereClauseStructure.organisation>
							and #preserveSingleQuotes(whereClause)#
						</cfif>
						<cfif entity eq "person" and structKeyExists(whereClauseStructure,"location") and whereClauseStructure.location neq "">
							<cfset whereClause = whereClauseStructure.location>
							and #preserveSingleQuotes(whereClause)#
						</cfif>
					</cfif>
					
					<cfset loopCount++>
				</cfloop>
				) as base
		
			</cfquery>
			
			<cfcatch>
				<cfif structKeyExists(url,"showDebug")>
					<cfrethrow>
				</cfif>
				<cfdump var="#cfcatch#">
				<cfabort>
			</cfcatch>
		</cftry>
					
		<cfif getModEntities.recordCount>
			
			<cfif listFindNoCase("organisation,location,person",entity)>
				<cfquery name="getOrgType" datasource="#application.siteDataSource#">
					select distinct orgType from ##modifiedEntity
				</cfquery>
				
				<cfloop query="getOrgType">
					<cfset triggerPoint = application.com.settings.getSetting("salesForce.synchTriggerPoints.plo.#orgType#")>
					
					<cfsavecontent variable="selectFlaggedEntitiesSql">
						<cfoutput>
							select distinct t.entityID,#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynching").flagID# as flagID,#request.relayCurrentUser.userGroupID# as createUserGroup,'#request.requestTime#' as createTime,'#request.requestTime#' as updateTime,#request.relayCurrentUser.userGroupID# as updateUserGroup
							from ##modifiedEntity t
								left join booleanFlagData bfd with (noLock) on t.entityID = bfd.entityID and bfd.flagID = #application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynching").flagID#
							where bfd.flagID is null
								<cfif triggerPoint eq "Approval">and t.approved <> 0<cfelseif triggerPoint neq "creation">and 1=0</cfif>
							and t.orgType = '#orgType#'
						</cfoutput>
					</cfsavecontent>
					
					<cfquery name="setEntitiesAsSynching" datasource="#application.siteDataSource#">
						update booleanFlagData
							set lastUpdated = getDate(),
								lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
								lastUpdatedByPerson=#request.relayCurrentUser.personID#
						from booleanFlagData bfd
							inner join (#preserveSingleQuotes(selectFlaggedEntitiesSql)#) base
								on bfd.entityID = base.entityID
						where bfd.flagID in (#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynched_export").flagID#,#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynched_import").flagID#)
							
						delete booleanFlagData 
						from booleanFlagData bfd
							inner join (#preserveSingleQuotes(selectFlaggedEntitiesSql)#) base
								on bfd.entityID = base.entityID
						where bfd.flagID in (#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynched_export").flagID#,#application.com.flag.getFlagStructure("#left(baseEntity,3)#SalesForceSynched_import").flagID#)  <!--- 2013 Release 2 --->
						
						insert into booleanFlagData (entityID,flagID,createdBy,created,lastUpdated,lastUpdatedBy)
						#preserveSingleQuotes(selectFlaggedEntitiesSql)#
					</cfquery>

				</cfloop>

			<cfelseif entity eq "opportunity">
				
				<cfsavecontent variable="selectFlaggedEntitiesSql">
					<cfoutput>
						select distinct t.entityID,#application.com.flag.getFlagStructure("oppSalesForceSynching").flagID# as flagID,#request.relayCurrentUser.userGroupID# as createUserGroup,'#request.requestTime#' as createTime,'#request.requestTime#' as updateTime,#request.relayCurrentUser.userGroupID# as updateUserGroup
						from ##modifiedEntity t
							left join booleanFlagData bfd with (noLock) on t.entityID = bfd.entityID and bfd.flagID = #application.com.flag.getFlagStructure("oppSalesForceSynching").flagID#
						where bfd.flagID is null
							<!---and t.sortOrder >= #getOppStageOrder.sortOrder#--->
					</cfoutput>
				</cfsavecontent>
				
				<cfquery name="setEntitiesAsSynching" datasource="#application.siteDataSource#">
					update booleanFlagData
						set lastUpdated = getDate(),
							lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
							lastUpdatedByPerson=#request.relayCurrentUser.personID#
					from booleanFlagData bfd
						inner join (#preserveSingleQuotes(selectFlaggedEntitiesSql)#) base
							on bfd.entityID = base.entityID
					where bfd.flagID in (#application.com.flag.getFlagStructure("oppSalesForceSynched_export").flagID#,#application.com.flag.getFlagStructure("oppSalesForceSynched_import").flagID#)
							
					delete booleanFlagData
					from booleanFlagData bfd
						inner join (#preserveSingleQuotes(selectFlaggedEntitiesSql)#) base
							on bfd.entityID = base.entityID
					where bfd.flagID in (#application.com.flag.getFlagStructure("oppSalesForceSynched_export").flagID#,#application.com.flag.getFlagStructure("oppSalesForceSynched_import").flagID#) <!--- 2013 Release 2 --->

					insert into booleanFlagData (entityID,flagId,createdBy,created,lastUpdated,lastUpdatedBy)
					#preserveSingleQuotes(selectFlaggedEntitiesSql)#
				</cfquery>
			</cfif>
		</cfif>
		
		<cfquery name="getModEntities" datasource="#application.siteDataSource#">
			select * from ##modifiedEntity
		</cfquery>
		
		<cfreturn getModEntities>

	</cffunction>
	

	<cffunction name="getEntitiesFlaggedAsSynching" access="public" hint="Returns all the entities that have been flagged as 'Synching'" returntype="struct" output="false">
		<cfargument name="entityTypeList" type="string" required="true">
		<cfargument name="opportunityID" type="numeric" required="false" hint="Needed when getting opportunity products">
		<cfargument name="modifiedFrom" type="date" required="true"> <!--- must be passed in as UTC --->
		<cfargument name="modifiedTo" type="date" required="true"> <!--- must be passed in as UTC --->
		
		<cfscript>
			var getEntitiesFlaggedAsSynchingQuery = "";
			var entity = "";
			var mappedObject = structNew();
			var contactLocationPerson = false; //tells us whether a contact is mapped to a location and person
			var crmID = "";
			var whereClause = "";
			var polSynchType = application.com.settings.getSetting("salesForce.ploSynchType");
			var timeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset();
			var oppProductMappings = structNew();
			var includeOppProducts = false;
			var getSynchPendingEntities = "";
			var getAllSynchPendingEntities = "";
			var loopCount = 1;
			var synchedEntityColumns = "";
			var baseEntity = "";
			var fieldNameList = "";
			var uniqueBaseEntityKey = "";
			var uniqueEntityKey = "";
			var synchedFlagIDList = "";
			var queryResult = structNew();
			var result = structNew();
			var queryInfo = structNew();
			var baseJoin = "";
		</cfscript>

		<cfif not structKeyExists(url,"processPendingData")>
		
			<cfset queryInfo = getModifiedEntriesQueryStatement(argumentCollection=arguments)>
		
			<cfquery name="getEntitiesFlaggedAsSynchingQuery" datasource="#application.siteDatasource#" result="queryResult">
				<cfloop list="#arguments.entityTypeList#" index="entity">
				
					<cfset whereClause = queryInfo.whereClause[entity]>
					<cfset baseJoin = queryInfo.baseJoin[entity]>
					
					<cfset mappedObject = getMappedObject(object=entity)>
					<cfset synchedEntityColumns = mappedObject.synchedEntityColumnList>
					<cfset synchedFlagIDList = mappedObject.synchedFlagList>
					
					<!--- if we're exporting opportunities, we may need to look at oppProduct modifications (if opportunity products have been mapped!!) --->
					<cfif mappedObject.entity eq "opportunity" and application.com.settings.getSetting("LeadManager.products.useOpportunityProducts")>
						<cfset oppProductMappings =getMappedObject(object="oppProducts")>
						<cfif listLen(oppProductMappings.entityColumnList) gt 0>
							<cfset includeOppProducts = true>
						</cfif>
					<cfelse>
						<cfset includeOppProducts = false>
					</cfif>
					
					<cfif not (listFindNoCase("organisation,location,person",entity) and polSynchType eq "Update")>
						<cfset synchedEntityColumns = ListAppend(synchedEntityColumns,"Whole Record",",")>
					</cfif>
					
					<cfset baseEntity = entity>
					
					<cfif listFindNoCase("location,person",entity) and listSort(mappedObject.tableList,"text") eq "location,person">
						<cfset contactLocationPerson = true>
						<cfset baseEntity = "person">
					</cfif>
					<cfif listFindNoCase("organisation,person",baseEntity)>
						<cfset synchedFlagIDList = listAppend(synchedFlagIDList,application.com.flag.getFlagStructure("#left(baseEntity,3)#Approved").flagID)>
					</cfif>
					
					<cfset crmID = getIDMappingInfo(entityType=baseEntity).sfIDColumn>
					<cfif loopCount neq 1>
						union
					</cfif>
					
					<cfset fieldNameList = synchedEntityColumns>
					<cfif includeOppProducts>
						<cfset fieldNameList = listAppend(synchedEntityColumns,oppProductMappings.entityColumnList)>
					</cfif>
					
					<cfset uniqueBaseEntityKey = application.entityType[application.entityTypeID[baseEntity]].uniqueKey>
					<cfset uniqueEntityKey = application.entityType[application.entityTypeID[entity]].uniqueKey>
					
					<!---
						if there is a history object for this object in SalesForce, then we can look at the mod register for individual fields; otherwise we look at the entity as a whole 
						Note: If an oppProduct has changed, the fieldname is 'Opportunity Products' (which is important!!!).. this will pick up an opportunity for synching
							if the opportunity itself hasn't changed but one it's products has.
					--->
					
					<!--- NJH 2012/04/03 CASE 427161 - reworked the modregister joins when dealing with opportunities and their products.. put the entityType with the entityId when joining to modRegister --->
					<cfif request.synchLevel eq "field">
						<cfsavecontent variable="whereClause">
							<cfoutput>
								dateadd(s,#timeOffsetInSeconds#,mr.modDate) between '#dateFormat(request.utcStartDateTime,"yyyy-mm-dd")# #timeFormat(request.utcStartDateTime,"HH:mm:ss")#' and '#dateFormat(request.utcEndDateTime,"yyyy-mm-dd")# #timeFormat(request.utcEndDateTime,"HH:mm:ss")#'
								and isNull(actionByCF,#request.relayCurrentUser.userGroupID#) <> #request.relayCurrentUser.userGroupID#
								and (<cfif fieldNameList neq "">med.FieldName in (#listQualify(fieldNameList,"'")#)<cfelse>1=0</cfif>
									<cfif synchedFlagIDList neq "">
									 or (mr.Action in ('FA','FD','FM') and mr.flagID in (#synchedFlagIDList#))
									 </cfif>
									)
							</cfoutput>
						</cfsavecontent>
						
						select distinct case when mr.flagId is not null then cast(mr.flagID as varchar) else newVal end as newVal,
							#baseEntity#.#uniqueBaseEntityKey# as entityID, 
							case when med.tableName = 'oppProducts' and 1=#IIF(includeOppProducts,1,0)# then 'opportunity' else med.tableName end as entity,
							case when med.tableName = 'oppProducts' and 1=#IIF(includeOppProducts,1,0)# then 'Opportunity Products' when med.fieldName is null then 'AnyFlag-'+cast(mr.flagId as varchar) else med.fieldName end as fieldName,
							#crmID# as entityCrmID,	dateadd(s,#timeOffsetInSeconds#,modDate) as modDate
						from 
							#baseEntity# with (noLock)
							<cfif includeOppProducts>
								left join oppProducts op with (noLock) on op.opportunityID = opportunity.opportunityID and op.productOrGroup = 'P'
								left join oppProductsDel opd with (noLock) on opd.opportunityID = opportunity.opportunityID and opd.productOrGroup = 'P'
							</cfif>
								inner join modRegister mr with (noLock)
									on ((mr.recordID = #baseEntity#.#uniqueEntityKey# and mr.entityTypeID = #application.entityTypeID[entity]#)<cfif includeOppProducts>or (mr.recordID = op.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)or (mr.recordID = opd.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)</cfif>)
								left join modEntityDef med with (noLock)
									on (med.tablename = '#entity#' <cfif includeOppProducts>or med.tableName = 'oppProducts'</cfif>) and mr.modEntityID = med.modEntityID
								inner join booleanFlagData bfd on bfd.entityID = #baseEntity#.#baseEntity#ID and bfd.flagId = #application.com.flag.getFlagStructure(flagID="#left(baseEntity,3)#SalesForceSynching").flagID#
								inner join 
									(select max(mr.id) as ID from modRegister mr with (noLock) 
										inner join modEntityDef med with (noLock) on mr.modEntityID = med.modEntityID
									where 
										#preserveSingleQuotes(whereClause)#
									group by recordID, case when med.fieldname != null then mr.modEntityID else mr.flagID end
									) maxID on maxID.ID = mr.id
						where
							#preserveSingleQuotes(whereClause)#
							<cfif structKeyExists(arguments,"opportunityID") and entity eq "oppProducts"> and oppProducts.opportunityID = #arguments.opportunityID#</cfif>
							<cfif loopCount eq listLen(arguments.entityTypeList)>
							order by entity, entityID
							</cfif>
							
					<cfelse>
					
						<!--- we are not looking at field changes but whole records that have changed... 
							When looking at opportunity products, the query is slightly different as we have to pick up deletes as well as adds/edits. That means joining
							to the oppproductdel table as well as the oppproduct table.
						--->
						select distinct '' as newVal, 
							<cfif entity neq "oppProducts">#baseEntity#.#uniqueBaseEntityKey#<cfelse>mr.recordID</cfif> as entityId,
							'#baseEntity#' as entity, 
							case when mr.action = 'opppD' and #IIF(entity eq "oppProducts",1,0)# =1 then 'Deleted' else 'Whole Record' end as fieldName, 
							<cfif entity eq "oppProducts">case when mr.action = 'opppD' then opd.crmOppProductID else #baseEntity#.#crmID# end<cfelse>#baseEntity#.#crmID#</cfif> as entityCrmID, 
							max(dateadd(s,#timeOffsetInSeconds#,mr.modDate)) as modDate
						from 
							<cfif includeOppProducts or entity eq "oppProducts">
								opportunity with (noLock)
								left join oppProducts with (noLock) on oppProducts.opportunityID = opportunity.opportunityID and oppProducts.productOrGroup = 'P'
								left join oppProductsDel opd with (noLock) on opd.opportunityID = opportunity.opportunityID and opd.productOrGroup = 'P'
							<cfelse>
								#baseEntity# with (noLock)
							</cfif>
							inner join modRegister mr with (noLock)
								on ((mr.recordID = #baseEntity#.#uniqueBaseEntityKey# and mr.entityTypeID = #application.entityTypeID[entity]#)<cfif includeOppProducts>or (mr.recordID = oppProducts.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)</cfif><cfif includeOppProducts or entity eq "oppProducts"> or (mr.recordID = opd.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)</cfif>)
							left join modEntityDef med with (noLock) 
								on mr.modEntityID = med.modEntityID and (med.tablename = '#entity#'<cfif includeOppProducts> or med.tableName = 'oppProducts'</cfif>)
							<!--- #preserveSingleQuotes(baseJoin)# --->
							<cfif entity eq "oppProducts">
								<cfset baseEntity = "opportunity">
							</cfif>
							inner join booleanFlagData bfd with (noLock) on bfd.entityID = #baseEntity#.#baseEntity#ID and bfd.flagId = #application.com.flag.getFlagStructure(flagID="#left(baseEntity,3)#SalesForceSynching").flagID#
							<cfif entity eq "oppProducts">
								<cfset baseEntity = "oppProducts">
							</cfif>
						where
							<!--- #preserveSingleQuotes(whereClause)# --->
							isNull(actionByCF,#request.relayCurrentUser.userGroupID#) <> #request.relayCurrentUser.userGroupID#
							and mr.modDate between dateadd(s,-(#timeOffsetInSeconds#),#request.utcStartDateTime#) and dateadd(s,-(#timeOffsetInSeconds#),#request.utcEndDateTime#)
							and (<cfif fieldNameList neq "">med.FieldName in (#listQualify(fieldNameList,"'")#)<cfelse>1=0</cfif>
								<cfif synchedFlagIDList neq "">
								 or (mr.Action in ('FA','FD','FM') and mr.flagID in (#synchedFlagIDList#))
								</cfif>
								<cfif entity eq "oppProducts">
									or (mr.action = 'opppD')
								</cfif>
							)
							<cfif structKeyExists(arguments,"opportunityID") and entity eq "oppProducts"> and opportunity.opportunityID = #arguments.opportunityID#</cfif>
						group by <cfif entity neq "oppProducts">#baseEntity#.#uniqueBaseEntityKey#<cfelse>mr.recordID</cfif>,#baseEntity#.#crmID#,case when mr.action = 'opppD' and #IIF(entity eq "oppProducts",1,0)# =1 then 'Deleted' else 'Whole Record' end<cfif entity eq "oppProducts">,opd.#crmID#</cfif>
					
					</cfif>
					
					<cfset loopCount++>
				</cfloop>
			</cfquery>

			<cfset result = duplicate(queryResult)>

			<!--- when we get a person/location change, we can pick up the personID twice.. once in the location query and once in the person query.
				Here we get the distince records, so that the person is only picked up once.
			 --->
			<cfif loopCount gt 2>
				<cfquery name="getEntitiesFlaggedAsSynchingQuery" dbtype="query" result="queryResult">
					select distinct newVal,entityID,entity,fieldname,entityCrmID,max(modDate) as modDate from getEntitiesFlaggedAsSynchingQuery
						group by newVal,entityID,entity,fieldname,entityCrmID
				</cfquery>
				
				<cfset result.recordCount = getEntitiesFlaggedAsSynchingQuery.recordCount>
			</cfif>

		<cfelse>
		
			<!--- get the entity records from the pending table --->
			<cfloop list="#arguments.entityTypeList#" index="entity">
				<cfset getSynchPendingEntities = getSynchPendingRecords(entityType=entity)>
				
				<cfquery name="getAllSynchPendingEntities" dbType="query">
					select * from getSynchPendingEntities
					<cfif isQuery(getAllSynchPendingEntities)>
					union
					select * from getAllSynchPendingEntities
					</cfif>
				</cfquery>
			</cfloop>
			
			<cfset getEntitiesFlaggedAsSynchingQuery = getAllSynchPendingEntities>
			<cfset result.sql = "">
		</cfif>
		
		<cfif structKeyExists(url,"showDebug")>
			<cfdump var="#getEntitiesFlaggedAsSynchingQuery#">
		</cfif>
		
		
		<cfset result.recordSet = getEntitiesFlaggedAsSynchingQuery>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getModifiedEntriesQueryStatement" access="public" returnType="struct" output="false">
		<cfargument name="entityTypeList" type="string" required="true">
		<cfargument name="opportunityID" type="numeric" required="false" hint="Needed when getting opportunity products">
		
		<cfset var queryStatement = {}>
		<cfset var timeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset()>
		<cfset var mappedObject = {}>
		<cfset var synchedEntityColumns = "">
		<cfset var synchedFlagIDList = "">
		<cfset var oppProductMappings = {}>
		<cfset var includeOppProducts = false>
		<cfset var fieldNameList = "">
		<cfset var polSynchType = application.com.settings.getSetting("salesForce.ploSynchType")>
		<cfset var baseEntity = "">
		<cfset var uniqueBaseEntityKey = "">
		<cfset var whereClause = "">
		<cfset var baseJoin = "">
		<cfset var contactLocationPerson = false>
		<cfset var entityWhereClause = "">
		<cfset var whereClauseStructure = application.com.settings.getSetting("salesForce.whereClause.entity")>
		
		<cfset queryStatement.whereClause = {}>
		<cfset queryStatement.baseJoin = {}>
		
		<!--- TODO - set some request variables that hold the start/end time with the offset and in UTC, so that we know exactly what the dates are!! It will save passing them around as well --->
		
		<cfloop list="#arguments.entityTypeList#" index="entity">
			
			<!--- <cfset mappedObject = getMappedObject(object=entity)>
			<cfset synchedEntityColumns = mappedObject.synchedEntityColumnList>
			<cfset synchedFlagIDList = mappedObject.synchedFlagList>
			
			<cfif not (listFindNoCase("organisation,location,person",entity) and polSynchType eq "Update")>
				<cfset synchedEntityColumns = ListAppend(synchedEntityColumns,"Whole Record",",")>
			</cfif>
			
			<cfset fieldNameList = synchedEntityColumns>
			
			<cfset baseEntity = entity>
			
			<cfif listFindNoCase("location,person",entity) and listSort(mappedObject.tableList,"text") eq "location,person">
				<cfset contactLocationPerson = true>
				<cfset baseEntity = "person">
			</cfif>
			
			<!--- if we're exporting opportunities, we may need to look at oppProduct modifications (if opportunity products have been mapped!!) --->
			<cfset includeOppProducts = false>
			<cfif mappedObject.entity eq "opportunity" and application.com.settings.getSetting("LeadManager.products.useOpportunityProducts")>
				<cfset oppProductMappings =getMappedObject(object="oppProducts")>
				<cfif listLen(oppProductMappings.entityColumnList) gt 0>
					<cfset includeOppProducts = true>
				</cfif>
			</cfif>
			
			<cfif includeOppProducts>
				<cfset fieldNameList = listAppend(synchedEntityColumns,oppProductMappings.entityColumnList)>
			</cfif>
			
			<cfif listFindNoCase("organisation,person",baseEntity)>
				<cfset synchedFlagIDList = listAppend(synchedFlagIDList,application.com.flag.getFlagStructure("#left(baseEntity,3)#Approved").flagID)>
			</cfif>
			
			<cfset uniqueBaseEntityKey = application.entityType[application.entityTypeID[baseEntity]].uniqueKey>
			
			<cfsavecontent variable="whereClause">
				<cfoutput>
					isNull(actionByCF,#request.relayCurrentUser.userGroupID#) <> #request.relayCurrentUser.userGroupID#
					and mr.modDate between dateadd(s,-(#timeOffsetInSeconds#),#request.utcStartDateTime#) and dateadd(s,-(#timeOffsetInSeconds#),#request.utcEndDateTime#)
					and (<cfif fieldNameList neq "">med.FieldName in (#listQualify(fieldNameList,"'")#)<cfelse>1=0</cfif>
							<cfif synchedFlagIDList neq "">
							 or (mr.Action in ('FA','FD','FM') and mr.flagID in (#synchedFlagIDList#))
							</cfif>
							<cfif entity eq "oppProducts">
								or (mr.action = 'opppD')
							</cfif>
						)
						
					<!--- TODO: ask PS/Support question - should we include an organisation where clause for a loc or person? ie. should lower objects inherit the where clause of their parents --->
					<cfif structKeyExists(whereClauseStructure,entity) and whereClauseStructure[entity] neq "">
						<cfset entityWhereClause = whereClauseStructure[entity]>
						and #preserveSingleQuotes(entityWhereClause)#
					</cfif>
					<cfif listFindNoCase("location,person",entity) and structKeyExists(whereClauseStructure,"organisation") and whereClauseStructure.organisation neq "">
						<cfset entityWhereClause = whereClauseStructure.organisation>
						and #preserveSingleQuotes(entityWhereClause)#
					</cfif>
					<cfif entity eq "person" and structKeyExists(whereClauseStructure,"location") and whereClauseStructure.location neq "">
						<cfset entityWhereClause = whereClauseStructure.location>
						and #preserveSingleQuotes(entityWhereClause)#
					</cfif>
				</cfoutput>
			</cfsavecontent>
			
			<cfsavecontent variable="baseJoin">
				<cfoutput>
					<cfif includeOppProducts or entity eq "oppProducts">
						left join oppProducts op with (noLock) on op.opportunityID = opportunity.opportunityID and op.productOrGroup = 'P'
						left join oppProductsDel opd with (noLock) on opd.opportunityID = opportunity.opportunityID and opd.productOrGroup = 'P'
					</cfif>
					inner join modRegister mr with (noLock)
						on ((mr.recordID = #baseEntity#.#uniqueBaseEntityKey# and mr.entityTypeID = #application.entityTypeID[entity]#)<cfif includeOppProducts>or (mr.recordID = oppProducts.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)</cfif><cfif includeOppProducts or entity eq "oppProducts"> or (mr.recordID = opd.oppProductID and mr.entityTypeID = #application.entityTypeID.oppProducts#)</cfif>)
					left join modEntityDef med with (noLock) 
						on mr.modEntityID = med.modEntityID and (med.tablename = '#entity#'<cfif includeOppProducts> or med.tableName = 'oppProducts'</cfif>)
				</cfoutput>
			</cfsavecontent> --->
			
			<cfset queryStatement.whereClause[entity] = whereClause>
			<cfset queryStatement.baseJoin[entity] = baseJoin>
		</cfloop>
		
		<cfreturn queryStatement>
	</cffunction>
	
	
	<!--- a function that sets any custom fields that are stored in flags and vice versa --->
	<cffunction name="setFlagDataForObject" access="private" returntype="struct" output="false">
		<cfargument name="objectDetails" type="struct" default="#structNew()#">
		<cfargument name="object" type="string" required="true" hint="Will hold the name of a SF object or RW entityType">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="direction" type="string" default="SF2RW" hint="Valid values are SF2RW or RW2SF">
		<cfargument name="entityType" type="string" required="true" hint="The name of the Relayware entityType that we're currently dealing with">
		<cfargument name="flagIDList" type="string" default="" hint="List of flags that could be passed through to be set. Will be passed through when processing RW2SF changes.">
		<cfargument name="flagColumns" type="string" default="" hint="List of SF column names that will map to Relayware flags. Will be passed through when processing SF2RW changes.">
		
		<cfscript>
			var objectStruct = {isOK=true,message=""}; // structure to hold the flag data.. will be appended to the main object structure before going to SF.
			var mappedObjectStruct = getMappedObject(object=arguments.object,direction=arguments.direction);
			var mappedObjectList = mappedObjectStruct.tableList;
			var mappedObjectName = "table";
			var mappedColumnName = "column";
			var currentObject = "";
			var mappingArray = arrayNew(1);
			var mappingAttributes = structNew();
			var columnName = "";
			var com = application.com;
			var mergeStruct = structNew();
			var functionName = "";
			var flagType = structNew();
			var argCollection = structNew();
			var flagTextId = "";
			var node = "";
			var objectColumnName = "";
			var flagDataQry = "";
			var flagID = 0;
			var xmlSearchString = "";
			var tableCriteria = "";
			var flagColumn = "";
			var flagName = "";
			var xmlflagGroupIDs = "";
			var entityIDs = structNew();
			var thisEntityID = arguments.entityID;
		</cfscript>
		
		<cfif arguments.direction eq "RW2SF">
			<cfset mappedObjectList = mappedObjectStruct.objectList>
			<cfset mappedObjectName = "object">
			<cfset mappedColumnName = "objectColumn">
		</cfif>
		
		<!--- NJH 2013/10/09 - get any related entities. when exporting a contact, get locationIDs, for example, to get any flags set against a location...  --->
		<cfif arguments.direction eq "RW2SF">
			<cfset entityIDs[arguments.entityType] = arguments.entityId>
			<cflock name="SalesforceXMLSearch" timeout="3">
				<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/tableJoinMapping[translate(@#mappedObjectName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(mappedObjectStruct.entity)#'][translate(@primaryTable,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.entityType)#']/tableJoin")>
			</cflock>

			<cfloop array=#mappingArray# index="node">
				
				<cfquery name="getRelatedEntityID" datasource="#application.siteDataSource#">
					select #node.xmlAttributes.secondaryTable#.#application.com.relayEntity.getEntityType(entityTypeID=node.xmlAttributes.secondaryTable).uniqueKey# as ID 
					from #arguments.entityType#
						<cfif structKeyExists(node.xmlAttributes,"preJoin")>#node.xmlAttributes.preJoin#</cfif>
						inner join #node.xmlAttributes.secondaryTable# on #node.xmlAttributes.joinOn#
					where #arguments.entityType#.#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey# = #arguments.entityID#
				</cfquery>
				
				<cfset entityIDs[node.xmlAttributes.secondaryTable] = getRelatedEntityID.ID>
			</cfloop>
		</cfif>
		
		<!--- this will pass on the entityID to the flag function
			CASE 426355 - the order is important, as a new contact's entityID will be an empty space. So, either don't override existing struct values
				or set the entityID after the setting the merge struct with objectDetails
		 --->
		<cfset structAppend(mergeStruct,arguments.objectDetails)>
		<cfset mergeStruct["entityID"] = arguments.entityID>
		<cfset mergeStruct["direction"] = arguments.direction>

		<!--- if SF2RW, then mappedObjectList = tablelist; otherwise it is the object --->
		<cfloop list="#mappedObjectList#" index="currentObject">
			<!--- if a list of flagIds have been passed in, look for those in the xml 
				CASE 429891: check to see that the flagTextId or the flagGroupTextID does not equal an empty string before doing a search on it. An empty string
					brings back nodes that we do not want.
			--->
			<cfloop list="#arguments.flagIDList#" index="flagID">
				<cfset xmlSearchString = xmlSearchString & IIF(xmlSearchString neq "",DE(' or '),DE('')) & "@flagID='#flagID#'">
				<cfif application.com.flag.getFlagStructure(flagID=flagID).flagTextID neq "">
					<cfset xmlSearchString = xmlSearchString & " or translate(@flagID,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(application.com.flag.getFlagStructure(flagID=flagID).flagTextID)#'">
				</cfif>
				<!--- CASE 429891: if the flagGroupId is already in the search string, don't include it again.. more for tidyness then functionality --->
				<cfif not listFind(xmlflagGroupIDs,application.com.flag.getFlagStructure(flagID=flagID).flagGroup.flagGroupID)>
					<cfset xmlSearchString = xmlSearchString & " or @flagGroupID='#application.com.flag.getFlagStructure(flagID=flagID).flagGroup.flagGroupID#'">
					<cfif application.com.flag.getFlagStructure(flagID=flagID).flagGroup.flagGroupTextID neq "">
						<cfset xmlSearchString = xmlSearchString & " or translate(@flagGroupID,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(application.com.flag.getFlagStructure(flagID=flagID).flagGroup.flagGroupTextID)#'">
					</cfif>
					<cfset xmlflagGroupIDs = listAppend(xmlflagGroupIDs,application.com.flag.getFlagStructure(flagID=flagID).flagGroup.flagGroupID)>
				</cfif>
			</cfloop>
			
			<!--- if no flags have been passed, then look for entries with sf column names if they have been passed --->
			<cfif xmlSearchString eq "">
				<cfloop list="#arguments.flagColumns#" index="flagColumn">
					<cfset xmlSearchString = xmlSearchString & IIF(xmlSearchString neq "",DE('or '),DE('')) & "translate(@objectColumn,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz') = '#lcase(flagColumn)#'">
				</cfloop>
			</cfif>
			
			<!--- otherwise, just look for entries that have a flagId or flagGroupId. --->
			<cfif xmlSearchString eq "">
				<cfset xmlSearchString = "@flagGroupID or @flagID">
			</cfif>
			
			<!--- if we're exporting to SF, then we want a primary entity as well. For example, when building a contact, we first want location flags and then person flags --->
			<!--- <cfif arguments.direction eq "RW2SF">
				<cfset tableCriteria = "[translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.entityType)#']">
			</cfif> --->
			<cflock name="SalesforceXMLSearch" timeout="3">
				<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@#mappedObjectName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(currentObject)#']#tableCriteria#[#xmlSearchString#]")>
			</cflock>

			<cfloop array=#mappingArray# index="node">
				<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction eq arguments.direction>
					<cfset mappingAttributes = node.xmlAttributes>
					<cfset columnName = mappingAttributes[mappedColumnName]>
					<cfset thisEntityID = arguments.entityID>
					
					<cfif arguments.direction eq "RW2SF">
						<cfset thisEntityID = entityIDs[mappingAttributes.table]>
					</cfif>
					
					<!--- run the function if one has been defined for this mapping --->
					<cfif structKeyExists(mappingAttributes,"value") and listFirst(mappingAttributes.value,":") eq "func">
						<cfset functionName = mappingAttributes.value>
	
						<cfif structKeyExists(mappingAttributes,"flagID")>
							<cfset mergeStruct.flagID = mappingAttributes.flagID>
					<cfelseif structKeyExists(mappingAttributes,"flagGroupID")>		<!--- 2013-03-19 PPB Case 434331 belt and braces (on top of fix 429891); now we shouldn't get here without a flagGroupID   --->
							<cfset mergeStruct.flagGroupID = mappingAttributes.flagGroupID>
						</cfif>
						<cfset objectStruct[columnName] = runMappingFunction(functionName=functionName,mergeStruct=mergeStruct)>
					<cfelse>
	
						<!--- flags.. support boolean and text. Integer will work if it's just an integer getting stored. We currently don't do a lookup
							for linked entities 
						--->
						<cfif structKeyExists(mappingAttributes,"flagID")>
	
							<cfset flagType = application.com.flag.getFlagStructure(flagId=mappingAttributes.flagID).flagType>
							<!--- otherwise it's just a plain flag setting... We're currently assuming that the function above does the setting for us. --->
							<cfif arguments.direction eq "SF2RW">
								
								<cfset argCollection=structNew()>
								<cfset argCollection.flagTextID=mappingAttributes.flagID>
								<cfset argCollection.entityID=thisEntityID>
								<cfset argCollection.flagID=mappingAttributes.flagID>
								
								<cfif arguments.objectDetails[mappingAttributes.objectColumn] neq "">
									<cfif flagType.hasDataColumn>
										<cfset argCollection.data=arguments.objectDetails[mappingAttributes.objectColumn]>
									</cfif>
									
									<cfif flagType.name eq "radio">
										<cfset argCollection.deleteOtherRadiosInGroup=true>
									</cfif>
									
									<cftry>
										<cfif flagType.datatable eq "boolean">
											<cfif isBoolean(arguments.objectDetails[mappingAttributes.objectColumn]) and not arguments.objectDetails[mappingAttributes.objectColumn]>
												<cfset application.com.flag.deleteFlagData(flagID=mappingAttributes.flagID,entityID=thisEntityID)>
											<cfelse>
												<cfset application.com.flag.setBooleanFlag(argumentCollection=argCollection)>
											</cfif>
										<cfelse>
											<!--- if it's an integer flag, ensure that the value that is being inserted is an integer (ie. not a float) --->
											<cfif flagType.datatable eq "integer" and isNumeric(argCollection.data)>
												<cfset argCollection.data = int(argCollection.data)>
											</cfif>
											<cfset application.com.flag.setFlagData(argumentCollection=argCollection)>
										</cfif>
										<cfcatch>
											<cfset objectStruct.isOK = false>
											<cfset objectStruct.message = objectStruct.message&"Could not set flag #argCollection.flagTextID# for #mappingAttributes.table# (ID: #argCollection.entityID#)<br>">
										</cfcatch>
									</cftry>
									
								<!--- it's an empty value. We treat is as a delete --->
								<cfelse>
									<cftry>
										<cfset application.com.flag.deleteFlagData(flagID=mappingAttributes.flagID,entityID=thisEntityID)>
										<cfcatch>
											<cfset objectStruct.isOK = false>
											<cfset objectStruct.message = objectStruct.message&"Could not delete from flag #argCollection.flagTextID# for #mappingAttributes.table# (ID: #thisEntityID#)<br>">
										</cfcatch>
									</cftry>
								</cfif>
								
							<!--- RW2SF flags --->
							<cfelse>
								
								<cfset flagDataQry = application.com.flag.getFlagData(entityID=thisEntityID,flagId=mappingAttributes.flagID)>
								<cfif application.com.flag.getFlagStructure(mappingAttributes.flagID).flagType.hasDataColumn>
									<cfset objectStruct[columnName] = flagDataQry.data>
								<cfelseif flagType.datatable eq "boolean">
									<cfif flagDataQry.recordCount>
										<cfset objectStruct[columnName] = "true">
									<cfelse>
										<cfset objectStruct[columnName] = "false">
									</cfif>
								</cfif>
							</cfif>
							
						<!--- 2010-12-23 - NJH added support for flagGroupIds --->	
						<cfelseif structKeyExists(mappingAttributes,"flagGroupID")>
						
							<cfset flagType = application.com.flag.getFlagGroupStructure(flagGroupId=mappingAttributes.flagGroupID).flagType>
							
							<!--- otherwise it's just a plain flag setting... We're currently assuming that the function above does the setting for us. --->
							<cfif arguments.direction eq "SF2RW">
							
								<cfset objectColumnName = mappingAttributes.objectColumn>
								<cfif arguments.objectDetails[objectColumnName] neq "">
	
									<cfset argCollection=structNew()>
									<cfset argCollection.entityID=thisEntityID>
									<!--- 
										the flagTextId that we're looking for is the object column name plus the value. So, for example, if the column is account_type__c
											and it has values for 'yes' and 'no', then the flags will be account_type__c_yes and account_type__c_no
									--->
									
									<!--- only support for boolean flag groups at the moment --->
									<cfif flagType.datatable eq "boolean">
									
										<cftry>
											<cfif flagType.name eq "radio">
												<cfset argCollection.deleteOtherRadiosInGroup=true>
												<cfset argCollection.flagTextID=application.com.regExp.makeSafeColumnName(replace(objectColumnName,"__c","")&"_"&arguments.objectDetails[objectColumnName])>
												
												<cfif not application.com.flag.getFlagStructure(flagID=argCollection.flagTextID).isOK>
													<!--- if that flagTextID doesn't exist, we try to get the flagTextID by trying to find the flag
														in that flagGroup specified in the mappings with a name as defined by the column value. We put this in a structure to prevent multiple hits to the db. --->
													<cfif not structKeyExists(request,"flagNameMappings")>
														<cfset request.flagNameMappings = structNew()>
													</cfif>
													
													<!--- NJH 2013/01/15 - Case 433311 a bad copy and paste job.. changed flagID to be arguments.objectDetails[objectColumnName] as flagID does not exist --->
													<cfif not structKeyExists(request.flagNameMappings,"#mappingAttributes.flagGroupID#_#arguments.objectDetails[objectColumnName]#")>
													
														<cfquery name="getFlagID" datasource="#application.siteDataSource#">
															select flagTextID from flag f with (noLock) inner join flagGroup fg with (noLock)
																on f.flagGroupID = fg.flagGroupID
															where 
																<cfif isNumeric(mappingAttributes.flagGroupID)>fg.flagGroupID=#mappingAttributes.flagGroupID#
																<cfelse>fg.flagGroupTextID='#mappingAttributes.flagGroupID#'</cfif>
																 and f.name='#arguments.objectDetails[objectColumnName]#'
														</cfquery>
														
														<cfset request.flagNameMappings["#mappingAttributes.flagGroupID#_#arguments.objectDetails[objectColumnName]#"] = getFlagID.flagTextID>
													</cfif>
													
													<cfset argCollection.flagTextID = request.flagNameMappings["#mappingAttributes.flagGroupID#_#arguments.objectDetails[objectColumnName]#"]>
												</cfif>
												<cfset application.com.flag.setBooleanFlag(argumentCollection=argCollection)>	
												
											<cfelseif flagType.name eq "checkbox">
											
												<!--- we delete all flags, and then re-add the ones as necessary --->
												<cfset application.com.flag.deleteFlagGroupData(flagGroupID=mappingAttributes.flagGroupID,entityID=thisEntityID)>
												
												<cfloop list="#arguments.objectDetails[objectColumnName]#" index="flagID" delimiters=";">
													
													<!--- we first to get flagIds based on the columnName_columnValue --->
													<cfset argCollection.flagTextID=application.com.regExp.makeSafeColumnName(replace(objectColumnName,"__c","")&"_"&flagID)>
													
													<cfif not application.com.flag.getFlagStructure(flagID=argCollection.flagTextID).isOK>
													
														<!--- if that flagTextID doesn't exist, we try to get the flagTextID by trying to find the flag
															in that flagGroup specified in the mappings with a name as defined by the column value. We put this in a structure to prevent multiple hits to the db. --->
														<cfif not structKeyExists(request,"flagNameMappings")>
															<cfset request.flagNameMappings = structNew()>
														</cfif>
														
														<cfif not structKeyExists(request.flagNameMappings,"#mappingAttributes.flagGroupID#_#flagID#")>
														
															<cfquery name="getFlagID" datasource="#application.siteDataSource#">
																select flagTextID from flag f with (noLock) inner join flagGroup fg with (noLock)
																	on f.flagGroupID = fg.flagGroupID
																where 
																	<cfif isNumeric(mappingAttributes.flagGroupID)>fg.flagGroupID=#mappingAttributes.flagGroupID#
																	<cfelse>fg.flagGroupTextID='#mappingAttributes.flagGroupID#'</cfif>
																	 and f.name='#flagID#'
															</cfquery>
															
															<cfset request.flagNameMappings["#mappingAttributes.flagGroupID#_#flagID#"] = getFlagID.flagTextID>
														</cfif>
														
														<cfset argCollection.flagTextID = request.flagNameMappings["#mappingAttributes.flagGroupID#_#flagID#"]>
													</cfif>
													<cfset application.com.flag.setBooleanFlag(argumentCollection=argCollection)>
												</cfloop>
											</cfif>
											
											<cfcatch>
												<cfset objectStruct.isOK = false>
												<cfset objectStruct.message = objectStruct.message&"Could not set flag #argCollection.flagTextID# for #mappingAttributes.table# (ID: #argCollection.entityID#)<br>">
											</cfcatch>
										</cftry>
									</cfif>
									
								<cfelse>
									<!--- if it's an empty value, we treat that as no flags set for that flag group --->
									<cftry>
										<cfset application.com.flag.deleteFlagGroupData(flagGroupID=mappingAttributes.flagGroupID,entityID=thisEntityID)>
										<cfcatch>
											<cfset objectStruct.isOK = false>
											<cfset objectStruct.message = objectStruct.message&"Could not delete from flagGroup #mappingAttributes.flagGroupID# for #mappingAttributes.table# (ID: #thisEntityID#)<br>">
										</cfcatch>
									</cftry>
								</cfif>
							<cfelse>
								
								<cfif flagType.datatable eq "boolean">
									<cfset flagDataQry=application.com.flag.getFlagGroupData(flagGroupId=mappingAttributes.flagGroupID,entityID=thisEntityID)>
										 
									<cfif flagType.name eq "checkbox">
										<cfset flagName = listChangeDelims(valueList(flagDataQry.flagName),";",",")>
									<cfelse>
										<cfset flagName = flagDataQry.flagName>
									</cfif>
									<cfset objectStruct[columnName] = flagName>
								</cfif>
								
							</cfif>
						</cfif>
					</cfif>
					
					<cfif arguments.direction eq "RW2SF" and structKeyExists(mappingAttributes,"sfValueToNull") and mappingAttributes.sfValueToNull eq objectStruct[columnName]>
						<cfif not structKeyExists(objectStruct,"fieldsToNull")>
							<cfset objectStruct.fieldsToNull = "">
						</cfif>
						<cfset objectStruct.fieldsToNull = listAppend(objectStruct.fieldsToNull,columnName)>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>

		<cfreturn objectStruct>
	</cffunction>
	

	<cffunction name="getModifiedObjectsFromSalesForce" access="public" returntype="struct" output="true">
		<cfargument name="object" type="string" required="true">
		<cfargument name="modifiedFrom" type="date" required="true" hint="Passed in as UTC">
		<cfargument name="modifiedTo" type="date" required="true" hint="Passed in as UTC">
		<cfargument name="polData" type="boolean" default="false">
		<cfargument name="opportunityID" type="string">
		
		<cfscript>
			var queryFieldList = "ID,field,newValue,createdDate,#arguments.object#ID";
			var queryResponseStruct = structNew();
			var whereClauseStructure = application.com.settings.getSetting(IIF(structKeyExists(request,"sfOppTypeTextID"),"""salesForce.whereClause.leadSynch.object""","""salesForce.whereClause.object"""));
			var whereClause = "";
			var historyWhereClause = "";
			var historyObject = "";
			var sfModQuery = queryNew(queryFieldList);
			var getObjectFieldLastestChanges = "";
			var getSFRecordChanges = "";
			var rowCount = 0;
			var sfField = "";
			var assocDataChange = "";
			var updateResult = structNew();
			var updateQuery = "";
			var deleteResult =structNew();
			var deleteQuery = "";
			var dateWhereClause = "";
			var mappedObjectInfo = structNew();
			var errorMessage = "";
			var fieldList = "";
			var queryMethod = "query";
			var deleteArgStruct = structNew();
			var oppProductsID = "";
			var objectArray = arrayNew(1);
			var getSFOpportunityID = "";
			var dataTypeList = "";
			var i=0;
		</cfscript>

		<cfset historyObject = getHistoryObject(object=arguments.object)>
		
		<cfset putSFDCObjectIntoMemory(object=historyObject)>

		<cfif not structKeyExists(url,"processPendingData")>
			<!--- we need the salesforce user ID, so if a session has not yet been established, then create one --->
			<cfif not structKeyExists(application,"sfdcapi") or not structKeyExists(application.sfdcapi,"userID") or (structKeyExists(application.sfdcapi,"userID") and application.sfdcapi.userID eq "")>
				<cfset createSFDCSession()>
			</cfif>
			
			<cfif structKeyExists(whereClauseStructure,arguments.object) and whereClauseStructure[arguments.object] neq "">
				<cfset whereClause = " #whereClauseStructure[arguments.object]#">
			</cfif>
			
			<cfset mappedObjectInfo = getMappedObject(object=arguments.object,direction="SF2RW")>
			
			<!--- if we're looking at field history changed, then we need to build up our query slightly differently. We need to get changes
				for only those base objects that we are interested in. --->
			<cfif request.synchLevel eq "field">
			
				<cfif whereClause neq "">
					<cfset historyWhereClause = " #arguments.object#ID in (select ID from #arguments.object# where #whereClause#)">
				</cfif>
			
				<!--- if doing updates only, we don't want to get new records --->
				<cfif arguments.polData and application.com.settings.getSetting("salesForce.ploSynchType") eq "update">
					<cfset historyWhereClause = historyWhereClause & IIF(historyWhereClause neq "",DE('and '),DE('')) & "field != 'created'">
				</cfif>
				
				<!--- if we're dealing with opportunities (and we may need to extend this list in the future), then we do a call to get updated opportunities.
					The reason we do this is because if we just look at the opportunity field changes, things like contacts and roles being added/deleted
					don't flag the opportunity as being changed.
					We may not acutally need to do this as SalesForce has a trigger that updates the opportunity amount when an opportunity product changes
				 --->
				<cfif arguments.object eq "opportunity" and structKeyExists(request,"sfSynch") and request.sfSynch and application.com.settings.getSetting("LeadManager.products.useOpportunityProducts")>
					<cfset updateResult = send(object="opportunityLineItem",method="query",queryString="SELECT opportunityID,max(lastModifiedDate) lastModifiedDate from opportunityLineItem where lastModifiedDate >= #convertDateToSalesForceFormat(dateToConvert=arguments.modifiedFrom)# and lastModifiedDate <= #convertDateToSalesForceFormat(dateToConvert=arguments.modifiedTo)# and lastModifiedByID != '#application.sfdcapi.userID#' and opportunityID in (select ID from opportunity where isDeleted != TRUE #IIF(whereClause neq '',DE('and '),DE(''))# #whereClause#) group by opportunityID",throwErrorOn500Response=true)>

					<cfif not updateResult.isOK>
						<cfset errorMessage = updateResult.response>
					</cfif>
				</cfif>
				
				<!--- get only the fields that are in the mapping xml. We also need to get created fields and those that are textName... account.name field changes are stored as account.textName for some reason  --->
				<cfset whereClause = historyWhereClause & IIF(historyWhereClause neq "",DE('and '),DE('')) & " field in ('created','TextName',"&#listQualify(mappedObjectInfo.synchedObjectColumnList,"'")#&")">
				<cfset dateWhereClause = "createdDate >= #convertDateToSalesForceFormat(dateToConvert=arguments.modifiedFrom)# and lastModifiedDate <= #convertDateToSalesForceFormat(dateToConvert=arguments.modifiedTo)# and createdByID != '#application.sfdcapi.userID#'">
				
			<cfelse>
				<cfset historyObject = arguments.object>
				<!--- appending the last modified date so that we store the date when this record was last modified should it go in the synchPending table --->
				<cfset fieldList = listAppend(mappedObjectInfo.objectColumnList,"lastModifiedDate")>
				<cfset queryFieldList = application.com.salesForceAPI.setObjectFieldsForQuery(object=arguments.object,fieldlist=fieldList)>
				<cfset dateWhereClause = "lastModifiedDate >= #convertDateToSalesForceFormat(dateToConvert=arguments.modifiedFrom)# and lastModifiedDate <= #convertDateToSalesForceFormat(dateToConvert=arguments.modifiedTo)# and lastModifiedByID != '#application.sfdcapi.userID#'">
			</cfif>
	
			<cfset queryResponseStruct = send(object=historyObject,method=queryMethod,queryString="SELECT #queryFieldList# from #historyObject# where #dateWhereClause# #IIF(whereClause neq '',DE('and '),DE(''))# #whereClause#",throwErrorOn500Response=true)>
			<cfset sfModQuery = queryResponseStruct.response>
			
			<!--- here we have to get the opportunity product deletions. Ideally, we would like to use the queryAll method, but opportunity products do not get flagged as deleted
				until the whole opportunity has been deleted. Therefore, if you delete an opportunity product from an opportunity and then do a  'queryAll', it does not get picked up.
				So, it's back to the 'getDeleted' method, which means we have to do a little extra work to find out which SF opportunity they relate to (have to do this via RW) and find out
				if they are valid deletions on RW side (ie. are they products that we are interested in)
				We need to run this query for opportunities that are in field level synching mode (as we need to find out which opportunities may have modifications to import due to opportunity products
				being deleted) and then for the opportunity products themselves in record level synching mode. The first time it will get called for opportunities.
			--->
			<cfif ((arguments.object eq "opportunity" and request.synchLevel eq "field") or arguments.object eq "opportunityLineItem") and not structKeyExists(request,"deletedSFProducts")>
				<!--- <cfset deleteResult = send(object="opportunityLineItem",method="getDeleted",startDate=arguments.modifiedSince,endDate=application.com.dateFunctions.convertServerDateToUTC(dateAdd("n",1,request.requestTime)))> --->

				<!--- when this is called, we want to get the deleted opporutnity products, but we don't yet want to delete the ties to Relayware, as we need to know
					their entity IDs, which we get below. We need to know the entity IDs, as that is how they will be deleted. We need to remove the SF IDs as they are deleted, because
					an edit to the product on RW side will result in a 'invalid oppProductId' on SF when trying to do an update. So we need a handle on which products it is that we need to delete. --->
				<cfset deleteResult = UpdateDeletedObjects(entityTypes="oppProducts",deleteRWReferences=false)>

				<cfif not deleteResult.isOK>
					<cfset errorMessage = errorMessage&" "&deleteResult.deleteQuery>
				<cfelseif listFindNoCase(deleteResult.deleteQuery.columnList,"ID")>
					
					<cfset deleteQuery = deleteResult.deleteQuery>
					<cfset oppProductsID = getIDMappingInfo(entityType="oppProducts").sfIDColumn>
					
					<!--- find out the SalesForceID for these opportunity products by looking at the crmOppId as well as their RW entityIDs. These 
						entityIDs will be the handle to delete the opp products --->
					<cfquery name="getSFOpportunityID" datasource="#application.siteDataSource#">
						select #getIDMappingInfo(entityType="opportunity").sfIDColumn# as sfOppID, #oppProductsID# as sfOppProductId, cast(oppProductId as varchar) as entityId 
						from opportunity o with (noLock)
							inner join oppProducts op with (noLock) on op.opportunityId = o.opportunityID and op.productOrGroup = 'P'
						where #oppProductsID# in (#listQualify(valueList(deleteQuery.ID),"'")#)
					</cfquery>
					
					<!--- filter the delete query so that we only have products that we are interested in --->
					<cfquery name="deleteQuery" dbtype="query">
						select getSFOpportunityID.sfOppID as opportunityID,deletedDate as lastModifiedDate,ID,entityID
						from getSFOpportunityID,deleteQuery
						where deleteQuery.ID = getSFOpportunityID.sfOppProductId
					</cfquery>

					<cfset request.deletedSFProducts = deleteQuery>
				</cfif>
				
			<cfelseif structKeyExists(request,"deletedSFProducts") and not structKeyExists(request,"oppProductReferencesDeleted") and arguments.object eq "opportunityLineItem">
				<!--- now actually remove the tie between SF and RW for those deleted products..  --->
				<cfset UpdateDeletedObjects(entityTypes="oppProducts",startDate=arguments.modifiedFrom,convertToUTC=false)>
				<cfset request.oppProductReferencesDeleted = true>
			</cfif>

			<cfif not queryResponseStruct.isOK>
				<cfset errorMessage = queryResponseStruct.response&" "& errorMessage>
				
			<!--- if we are in modDataType='field' and we have a valid response with some data in it --->
			<cfelseif queryResponseStruct.isOK and listFindNoCase(sfModQuery.columnList,"#arguments.object#ID") and historyObject neq arguments.object>
	
				<!--- get the latest changes for a given object/field, as we could have picked up multiple changes for the same field --->
				<cfquery name="getObjectFieldLastestChanges" dbtype="query">
					select max(createdDate) as createdDate from sfModQuery
					group by field,#arguments.object#ID
				</cfquery>
				
				<!--- 
					get the full records of the fields that have changed...
					maybe put this into a temporary table... will that speed it up??
					Have added object and entityID columns so that we can pass in a whole query to the insertPendingRecords function which expects them.
				--->
				<cfquery name="getSFRecordChanges" dbType="query">
					select <cfloop list="#sfModQuery.columnList#" index="sfField"><cfif sfField eq "createdDate">sfModQuery.#sfField# as lastModifiedDate<cfelseif sfField eq "isDeleted">case when isDeleted=true then 'true' else 'false' end as deleted<cfelse>sfModQuery.#sfField#</cfif>,</cfloop>0 as processed, 0 as rowNum, '#arguments.object#' as object, '' as entityID
					from sfModQuery,
						getObjectFieldLastestChanges
					where sfModQuery.field = getObjectFieldLastestChanges.field
						and sfModQuery.#arguments.object#ID = getObjectFieldLastestChanges.#arguments.object#ID
						and cast(sfModQuery.createdDate as varchar) = cast(getObjectFieldLastestChanges.createdDate as varchar)
					order by sfModQuery.#arguments.object#ID, sfModQuery.field
				</cfquery>
			</cfif>
			
			<!--- if we had to run a query to getUpdated and we have a valid query with some data in it --->
			<cfif not structIsEmpty(updateResult) and updateResult.isOK and listFindNoCase(updateResult.response.columnList,"ID")>
				<cfset updateQuery = updateResult.response>
	
				<!--- Note, the term 'Opportunity Products' is important as we look for this in the opportunity import! If changing it here,
					you will need to change it there as well.
					
					Here we look for changes that occurred to an opportunity that don't come through the opportunityFieldHistory table. For example
					Adding a partner or role will not flag the opportunity as being changed in the opportunityFieldHistory. So, we do a query to get updated opportunities
					and then compare with those opportunities where fields have changed.
				--->

				<cfquery name="assocDataChange" dbtype="query">
					select distinct updateQuery.opportunityID as ID,'Opportunity Products' as field, '' as newValue,updateQuery.lastModifiedDate,updateQuery.opportunityID as #arguments.object#ID,0 as processed, 0 as rowNum
					from updateQuery
					<cfif isQuery(getSFRecordChanges)>
					union
						select opportunityID as ID,field,newValue,lastModifiedDate,opportunityID,0 as processed, 0 as rowNum
						from getSFRecordChanges
					</cfif>
				</cfquery>

				<cfset getSFRecordChanges = assocDataChange>
			</cfif>
			
 			<!--- if we had to run a query to getDeleted and we have a valid query with some data in it. This will only be for opportunities
				and opportunityLineItems --->
			<cfif structKeyExists(request,"deletedSFProducts")>
				<cfset deleteQuery = request.deletedSFProducts>

				<!--- Note, the term 'Opportunity Products' is important as we look for this in the opportunity import! If changing it here,
					you will need to change it there as well.
				--->

				<cfif arguments.object eq "opportunity">
					<!--- this query is used to identify which opportunities have changed as a result of opportunity products being deleted--->
					<cfquery name="assocDataChange" dbtype="query">
						select distinct opportunityID as ID,'Opportunity Products' as field,opportunityID as #arguments.object#ID,'' as newValue,cast (max(lastModifiedDate) as varchar) as lastModifiedDate,0 as processed, 0 as rowNum
						from deleteQuery
						group by opportunityID
						<cfif isQuery(getSFRecordChanges)>
						union
							select opportunityID as ID,field,opportunityID,newValue,lastModifiedDate,0 as processed, 0 as rowNum
							from getSFRecordChanges
						</cfif>
					</cfquery>
				<cfelse>

					<!--- this query is used to identity the opportunityLineItems that have changed when working to synch oppProducts --->
					<cfquery name="assocDataChange" dbtype="query">
						select distinct <cfloop list="#fieldList#" index="sfField"><cfif listFindNoCase("lastModifiedDate,ID",sfField)>#sfField#<cfelse>'' as #sfField#</cfif>,</cfloop>ID as #arguments.object#ID, entityID, 0 as processed, 0 as rowNum, 'Deleted' as field
						from deleteQuery
						<cfif structKeyExists(arguments,"opportunityID")> where opportunityID = '#arguments.opportunityID#'</cfif>
						<cfif isQuery(getSFRecordChanges)>
						union
							select #fieldList#,ID as #arguments.object#ID,'' as entityID, 0 as processed, 0 as rowNum,'' as field
							from getSFRecordChanges
						</cfif>
					</cfquery>
				</cfif>

				<cfset getSFRecordChanges = assocDataChange>
			</cfif>
			
			<!--- if we're not getting changed fields but rather full table fields, then create an 'objectID' field 
				as it won't exist, and yet most of the code expects it... Easier to add one here
			--->
			<cftry>
				<cfif request.synchLevel neq "field" and listFindNoCase(queryResponseStruct.response.columnList,"ID")>
					<cfquery name="getSFRecordChanges" dbType="query">
						select *,ID as #arguments.object#ID,'#arguments.object#' as object,'' as entityID,0 as processed, 0 as rowNum, 'Whole Record' as field, '' as newValue from queryResponseStruct.response
					</cfquery>
					
					<cfset queryResponseStruct.response = getSFRecordChanges>
				</cfif>
				<cfcatch>
					<cfmail to="nathaniel.hogeboom@relayware.com" from="errors@relayware.com" subject="response.columnList undefined in queryResponseStruct" type="html" priority="urgent">
						<cfdump var="#queryResponseStruct#">
					</cfmail>
					<cfrethrow>
				</cfcatch>
			</cftry>
			
			<cfif isQuery(getSFRecordChanges)>
				<!--- Tidy up the response query a bit. We have to set a  rowCount field, so that it gives us a unique identifier for each record when doing an update. 
					We want to set the processed field on it when doing the comparing between these records and RW records.
				--->
				<cfloop query="getSFRecordChanges">
					<!--- remove the T's and Z's in the lastModified date column --->
					<cfset querySetCell(getSFRecordChanges,"lastModifiedDate",replace(replace(lastModifiedDate,"T"," "),"Z"," "),currentRow)>
					<cfset querySetCell(getSFRecordChanges,"rowNum",currentRow,currentRow)>
					<!--- not sure why, but the account 'Name' field is stored as 'TextName' in the account History object.. I think it
						might be the fieldname for all 'Name' type fields --->
					<cfif request.synchLevel eq "field">
						<cfif field eq "TextName">
							<cfset querySetCell(getSFRecordChanges,"field","Name",currentRow)>
						<cfelseif field eq "created">
							<cfset querySetCell(getSFRecordChanges,"field","Whole Record",currentRow)>
						</cfif>
					</cfif>
				</cfloop>

			<cfelse>
				<!--- return an empty query if we don't have a response --->
				<cfloop from="1" to="#listLen(fieldList)#" index="i">
					<cfset dataTypeList = listAppend(dataTypeList,"varchar")>
				</cfloop>
				<cfset getSFRecordChanges = queryNew(fieldList,"#dataTypeList#")>
			</cfif>
			
			<!--- Standardise the response query. These are columns that should always exist. We need a field and newValue, as a bulk update will insert
				these records into the pending table with a fieldname and newValue column. Easier to set them here rather than check if they exist. --->
			<cfif not listFindNoCase(getSFRecordChanges.columnList,"entityID")>
				<cfset queryAddColumn(getSFRecordChanges,"entityID","varchar",arrayNew(1))>
			</cfif>
			<cfif not listFindNoCase(getSFRecordChanges.columnList,"object")>
				<cfset objectArray = arrayNew(1)>
				<cfif getSFRecordChanges.recordCount>
					<cfset arraySet(objectArray,1,getSFRecordChanges.recordCount,arguments.object)>
				</cfif>
				<cfset queryAddColumn(getSFRecordChanges,"object","varchar",objectArray)>
			</cfif>
			
			<cfif getSFRecordChanges.recordCount>
				<cfif not listFindNoCase(getSFRecordChanges.columnList,"field")>
					<cfset objectArray = arrayNew(1)>
					<cfset arraySet(objectArray,1,getSFRecordChanges.recordCount,"Whole Record")>
					<cfset queryAddColumn(getSFRecordChanges,"field","varchar",objectArray)>
				</cfif>
				<cfif not listFindNoCase(getSFRecordChanges.columnList,"newValue")>
					<cfset queryAddColumn(getSFRecordChanges,"newValue","varchar",arrayNew(1))>
				</cfif>
			</cfif>
			
			<cfset queryResponseStruct.response = getSFRecordChanges>		
			
		<cfelse>
			<!--- get SalesForce pending records --->
			<cfset queryResponseStruct.response = getSynchPendingRecords(object=arguments.object)>
			<cfset queryResponseStruct.success = true>
			<!--- setting some variables that we are looking for in objectEntitySynch...but not really used yet when process pending records --->
			<cfset queryResponseStruct.arguments = structNew()>
			<cfset queryResponseStruct.arguments.queryString = "">
		</cfif>

		<cfset queryResponseStruct.message = errorMessage>
		
		<cfif structKeyExists(url,"showDebug")>
			<cfdump var="#queryResponseStruct#">
		</cfif>
		
		<!--- we want to throw an error here so that the process doesn't keeping running "successfully".. we don't want to log the this 
			run in the dataTransferHistory table as we will then miss potential changes
		--->
		<cfif not queryResponseStruct.success>
			<cfthrow message="Error querying SalesForce for object #arguments.object#. #queryResponseStruct.message#">
		</cfif>
		
		<cfreturn queryResponseStruct>
	</cffunction>
	
	<!--- NJH 2013/10/23 Roadmap2 Release 2 Item 33 check if object exists on salesforce so that we don't insert duplicates--->
	<cffunction name="doesSFObjectExist" access="public" returnType="string" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objectDetails" type="struct" required="true">
		
		<cfset var salesForceID = 0>
		<cfset var queryString = "select ID from #arguments.object# where ">
		<cfset var result = structNew()>
		<cfset var matchFields = "">
		<cfset var columnName = "">
		<cfset var objectWhereClause = application.com.settings.getSetting("whereClause.object.#arguments.object#")> <!--- only select from objects that are included in the synch --->
		<cfset var columnValue = "">
		<cfset var firstLoop = true>
		<cfset var entityID = 0>
		<cfset var mappedObject= getMappedObject(object=arguments.object,direction="SF2RW")>
		<cfset var mappedField = "">
		<cfset var matchColumns = {organisation=structNew(),location=structNew(),person=structNew()}>
		<cfset var tablename = "">
		
		<!--- only check for POL data or SF users --->
		<cfif listFindNoCase("organisation,location,person",mappedObject.entity) or arguments.object eq "user">
		
			<!--- set columns that we use for matching --->
			<cfset matchColumns.organisation = "organisationName,organisationTypeID,countryID">
			<cfset matchColumns.location = "sitename,address1,address4,postalCode,organisationID,countryID">
			<cfset matchColumns.person = "firstname,lastname,email,locationID">
			
			<cfif objectWhereClause neq "">
				<cfset queryString = queryString & " " & objectWhereClause>
			</cfif>
	
			<cfif arguments.object eq "user">
				<cfset matchFields = "firstname,lastname,email">
			<cfelse>
				<cfloop list="#mappedObject.tablelist#" index="tablename">
					<cfset matchFields = listAppend(matchFields,matchColumns[tablename])>
				</cfloop>
				<cfif listFindNoCase(mappedObject.objectColumnList,"parentID")>
					<cfset matchFields = listAppend(matchFields,"parentID")>
				</cfif>
			</cfif>
			
			<cfloop list="#matchFields#" index="columnName">
				<cfset columnValue = "">
				
				<!--- we don't have a user mapping, so just use the column names. ParentID is a special 'field' that we append if we are using parent/child hierarchy as we want to search within a given parentID --->
				<cfif arguments.object eq "user" or columnName eq "parentID">
					<cfset mappedField = columnName>
				<cfelse>
					<cfset mappedField = getMappedField(object=arguments.object,table=mappedObject.entity,column=columnName)>	
				</cfif>
				
				<cfif mappedField neq "">
					<cfif structKeyExists(arguments.objectDetails,mappedField)>
						<cfset columnValue = application.com.salesForceAPI.escapeSoql(trim(arguments.objectDetails[mappedField]))>	<!--- 2014-10-13 AHL Case 442038 Opportunity not sync. Apostrophe soql  Escape --->
					</cfif>
					<cfset queryString = queryString & IIF(firstLoop and objectWhereClause eq "",DE(""),DE(" and ")) & "#mappedField# = '#columnValue#'">
					<cfset firstLoop=false>
				</cfif>
			</cfloop>
			
			<cfset queryString = queryString & " order by lastModifiedDate desc">
			
			<cfset result = send(method="query",object=arguments.object,queryString=queryString,ignoreBulkProtection=true)> <!--- set ignoreBulkProtection to true just in case the threshold is very low --->
			
			<cfif result.response.recordCount gte 1 and listFindNoCase(result.response.columnList,"ID")>
				<cfif arguments.object neq "user">
					<!--- we need to make sure that the salesForceID that we are going to update is not already mapped to an existing entity...Not yet sure what to do in the case that it is.. --->
					<cfloop query="result.response">
						<cfset entityID = doesEntityExist(object=arguments.object,objectID=ID,useRWMatchingRules=false).entityID>
						<cfif entityID eq 0>
							<cfset salesForceID = ID>
							<cfbreak>
						</cfif>
					</cfloop>
				<cfelse>
					<cfset salesForceID = result.response.ID[1]>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn salesForceID>
	</cffunction>
	
	
	<cffunction name="doesEntityExist" access="public" returntype="struct" hint="Checks if an entity exists in Relayware" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objectID" type="string" required="true">
		<cfargument name="useRWMatchingRules" type="boolean" default="true">
		<cfargument name="objectDetails" type="struct" required="false">
		<cfargument name="entityDetails" type="struct" required="false">
		
		<cfscript>
			var mappedObjectInfo = getMappedObject(object=arguments.object,direction="SF2RW");
			var rwEntity = mappedObjectInfo.entity;
			var entity="";
			var doesEntityWithSaleForceIDExist = "";
			var entityID = 0;
			var methodCallResult = structNew();
			var processOrder = mappedObjectInfo.processOrder;
			var organisationID = 0;
			var objectStruct = structNew();
			var rwEntitiesStruct = structNew();
			var foreignKeyInfo = getIDMappingInfo(entityType=rwEntity);
			var entityExistsStruct = structNew();
			var result = structNew();
			var thisObjectDetails = structNew();
			var locationID = 0;
			var functionArgs = structNew();
		</cfscript>
		
		<!--- we check whether an entity exists in two ways.
			1. If it has a salesForceID
			2. RW matching rules
		 --->

		<cfquery name="doesEntityWithSaleForceIDExist" datasource="#application.siteDataSource#">
			select #foreignKeyInfo.entityIDColumn# as entityID
				from #rwEntity#
				where #foreignKeyInfo.sfIDColumn# = '#arguments.objectID#'
		</cfquery>

		<cfif doesEntityWithSaleForceIDExist.recordCount gt 0>
			<cfset entityID = doesEntityWithSaleForceIDExist.entityID>
		</cfif>
		
		<cfset result.isOK = true>
		<cfset logDebugInformation(entityID=arguments.objectID,entityType=arguments.object,method="Does Entity Exist",additionalDetails=arguments)>
		
		<!--- if we haven't found an entity by ID, check by the matching rules.--->
		<!--- only doing matching rules on accounts and contacts (ie. organisations,locations and people) --->
		<cfif entityID eq 0 and listFindNoCase("organisation,location,person",rwEntity) and arguments.useRWMatchingRules>

			<!--- if we need to do some matching, we need to get more data than just the ID. 
				the details can be passed in; if not, they will have to be retrieved.
			 --->
			<cfif not structCount(arguments.entityDetails)>
				<cfif not structKeyExists(arguments,"objectDetails")>
					<cfset methodCallResult = send(object=arguments.object,method="retrieve",ids=arguments.objectID)>
					
					<cfif methodCallResult.success and listFindNoCase(methodCallResult.response.columnList,"ID")>
						<cfset objectStruct = application.com.structureFunctions.queryToStruct(query=methodCallResult.response,key="ID")>
						<cfset thisObjectDetails = objectStruct[objectID]>
					</cfif>
				<cfelse>
					<cfset thisObjectDetails = arguments.objectDetails>
				</cfif>
			</cfif>

			<cfif structCount(thisObjectDetails) gt 0 or structCount(arguments.entityDetails)>
				<cfif not structCount(arguments.entityDetails)>
					<cfset rwEntitiesStruct = convertObject(object=arguments.object,objectDetails=thisObjectDetails)>
				<cfelse>
					<cfset rwEntitiesStruct = arguments.entityDetails>
					<cfset rwEntitiesStruct.isOK = true>
				</cfif>
				
				<cfif rwEntitiesStruct.isOK>
					<cfloop list="#processOrder#" index="entity">
						<!--- for various entities, we're going to need to collect information on other entities that we are dependant on for matching.
							ie. to match a person, we need to know the locationID and the orgID, and for matching on a location, we need to know the orgID
						 --->

						<!--- need to set the orgID before checking for a location or person --->
						<cfif listFindNoCase("location,person",entity)>
						
							<!--- if we don't have an organisationID in the entity struct, then try and find the organisationID --->
							<cfif organisationID eq 0>
								<cfif not structKeyExists(rwEntitiesStruct.location,"organisationID") and not structKeyExists(rwEntitiesStruct.person,"organisationID")>
									<cfset organisationID = application.com.salesForce.doesEntityExist(object="account",objectID=thisObjectDetails["accountID"]).entityID>
								<cfelse>
									<cfif structKeyExists(rwEntitiesStruct.location,"organisationID")>
										<cfset organisationID = rwEntitiesStruct.location.organisationID>
									<cfelse>
										<cfset organisationID = rwEntitiesStruct.person.organisationID>
									</cfif>
								</cfif>
							</cfif>
							
							<cfset rwEntitiesStruct[entity]["organisationID"] = organisationID>
							
							<!--- if we're dealing with a person mapped to a contact (which is also mapped to a location), then set the locationID 
								which will have been set below because the location gets processed first...
							--->
							<cfif arguments.object eq "contact" and processOrder eq "location,person" and entity eq "person">
								<cfset rwEntitiesStruct[entity]["locationID"] = locationID>
							</cfif>
						</cfif>
						
						<cftry>
							<cfset functionArgs = structNew()>
							<cfset functionArgs["#entity#Details"] = rwEntitiesStruct[entity]>
							<cfset functionArgs.excludeSalesForceRecords = true>
							
							<!--- CASE 426374: NJH 2012-02-23 when we check for an organisation, we don't want to update the match name for the organisations in a given country
								for every organisation. If someone does a bulk update for a given country, we are refreshing the matchnames for that country every time and it's not needed
								as we update the matchname for the organisation when we insert the org... we may want to refresh this for the first org insert in a particular country.
							 --->
							<cfset functionArgs.updateMatchNamesForExistenceCheck = false>
								
							<cfif structKeyExists(application.com.relayEntity,"does#entity#Exist")>
								<cfinvoke component=#application.com.relayEntity# method="does#entity#Exist" argumentCollection=#functionArgs# returnvariable="entityExistsStruct">
							<cfelse>
								<cfset functionArgs.entityTypeID = entity>
								<cfset entityExistsStruct = application.com.relayEntity.doesEntityExist(argumentCollection=functionArgs)>
							</cfif>
							<cfcatch>
								<cfset entityExistsStruct = {isOK = false,message = cfcatch.message&"<br>"&cfcatch.detail}>
								<cfset result.isOK = false>
							</cfcatch>
						</cftry>
						
						<cfif not entityExistsStruct.isOK>
							<cfset logSFError(object=arguments.object,objectID=arguments.objectID,method="does#entity#Exist",message=entityExistsStruct.message,direction="I")>
							<cfbreak> <!--- if we haven't found a location, for example, then don't bother looking for a person --->
						<cfelse>
							<cfset deleteLogEntries(object=arguments.object,objectID=arguments.objectID,method="does#entity#Exist",direction="I")>
							
							<cfset entityID = entityExistsStruct.entityID>
							
							<cfif entityID neq 0>
								<!--- if we found a match, then set the salesForceID on this entity --->
								<cfif listLen(entityExistsStruct.entitiesMatchedList) gt 1>
									<cfset entityID = scoreAndMergeMatchedEntities(entityType=entity,entityIDList=entityExistsStruct.entitiesMatchedList).entityID>
								</cfif>
								
								<!--- if we have a locationId for a contact and the contact is mapped to a person and location, then we don't want to set the SalesForce ID on the location,
									but on the person --->
								<cfif not (arguments.object eq "contact" and processOrder eq "location,person" and entity eq "location")>
									<cfset setSalesForceIDForEntity(entityType=entity,entityID=entityID,salesForceID=arguments.objectID)>
								<cfelse>
									<cfset locationID = entityID>
								</cfif>
							<cfelse>
								<!--- if we haven't found a location, for example, then don't bother looking for a person --->
								<cfbreak>
							</cfif>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
			
		</cfif>
		
		<cfset result.entityID = entityID>
		<cfset result.entityType = rwEntity>

		<cfreturn result>
	</cffunction>
</cfcomponent>