<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="decryptMobileString" access="public" returnType="string">
		<cfargument name="string" type="string" required="true">
				
		<cfset var HexKey = application.com.settings.getSetting("mobile.secretKey")>
		<cfset var myKey = ToBase64(BinaryDecode(HexKey, "Hex"))>
		<cfset var Encrypted = arguments.hexText>
		<cfset var Decrypted = Decrypt( Encrypted, MyKey, 'AES' ,'Hex' )>
		
		<cfreturn Decrypted>
	</cffunction>

	<cffunction name="validateMobileRequest" access="public" returnType="numeric">
		
		<cfset var personID = 0>
		<cfset var mobileArgs = structNew()>
		<cfset var mobileCallArg = "">

		<!--- encrypted data for the mobile. Attempt to decrypt and set the variables into url scope to then be available to the callmethod service --->
		<cfif structKeyExists(url,"emd")>
			<cftry>
				<!--- <cfset decryptedData = urlDecode(application.com.mobile.decryptMobileString(string=url.emd))> --->
				<cfset decryptedData = urlDecode(application.com.encryption.decryptString(string=url.emd))>
				<cfset mobileArgs = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString=decryptedData,delimiter="&")>
				
				<cfcatch>
				</cfcatch>
			</cftry>
		</cfif>
		
		<cfif structKeyExists(request,"mobile") and structKeyExists(request.mobile,"args")>
			<cfset mobileArgs = request.mobile.args>
		</cfif>
		
		<cfif structKeyExists(mobileArgs,"username") and structKeyExists(mobileArgs,"password")> <!--- and structKeyExists(mobileArgs,"key") and mobileArgs.key eq application.com.settings.getSetting("mobile.publicKey") and structKeyExists(mobileArgs,"dateTimeStamp")>
			<cfif abs(dateDiff("n",mobileArgs.dateTimeStamp,application.com.dateFunctions.requestTimeUTC())) lte 5> --->
				<cfset checkLogin = application.com.login.authenticateUserV2(username=mobileArgs.userName,password=mobileArgs.password,showComplexityWidget=false)>
				
				<cfif checkLogin.isOK>
					<cfset personID=checkLogin.personID>
					<cfset request.mobile.validRequest = true> <!--- if this is set to true, then we can proceed with our mobile request --->
					
				<!--- an invalid username and password have been passed in... not entirely sure what to do here.. we need to reject the request --->
				<cfelse>
					
				</cfif>
			<!---</cfif>--->
		</cfif>
		
		<cfreturn personID>
		
	</cffunction>
	
	
	<cffunction name="trackRequest" access="public" output="false">
		<cfargument name="webserviceName" type="string" required="true">
		<cfargument name="methodName" type="string" required="true">
		<cfargument name="mobileArgs" type="struct" default="#structNew()#">
		
		<cfset var pageView = "">
		<cfset var insertRequest = "">
		<cfset var entityID = 0>
		<cfset var entityTypeID = 0>
		
		<!--- LOGIN --->
		<cfif arguments.webserviceName eq "login">
			<cfif arguments.methodName eq "authenticate">
				<cfset pageView = "tabletLoginMainView">
			</cfif>
		
		<!--- OPPORTUNITY --->	
		<cfelseif arguments.webserviceName eq "opportunity">
			<cfset entityTypeID = application.entityTypeID.opportunity>
			<cfif structKeyExists(arguments.mobileArgs,"opportunityID")>
				<cfset entityID = arguments.mobileArgs.opportunityID>
			</cfif>
			
			<cfif arguments.methodName eq "getOpportunities">
				<cfif structKeyExists(arguments.mobileArgs,"teamOpps") and arguments.mobileArgs.teamOpps>
					<cfset pageView = "groupTeamLeadsView">
				<cfelse>
					<cfset pageView = "leadsView">
				</cfif>
			<cfelseif arguments.methodName eq "insertOpportunity">
				<cfset pageView = "newLeadContactView">
			<cfelseif arguments.methodName eq "updateOpportunity">
				<cfset pageView = "leadContactEditView">
			</cfif>
			
		<!--- ELEARNING --->
		<cfelseif arguments.webserviceName eq "elearning">
			<cfset entityTypeID = application.entityTypeID.trngModule>
			<cfif structKeyExists(arguments.mobileArgs,"moduleID")>
				<cfset entityID = arguments.mobileArgs.moduleID>
			</cfif>
			
			<cfif arguments.methodName eq "mobileServiceCourses">
				<cfset pageView = "courseView">
			<cfelseif arguments.methodName eq "mobileServiceMod">
				<cfset pageView = "moduleView">
			<cfelseif arguments.methodName eq "mobileServiceModRated">
				<cfset pageView = "moduleRatingsView">
			<cfelseif arguments.methodName eq "mobileServiceFileRating" or arguments.methodName eq "getModuleRating">
				<cfset pageView = "singleModuleView">
			</cfif>
			
		<!--- NEWS --->
		<cfelseif arguments.webserviceName eq "news">
			<cfset entityTypeID = application.entityTypeID.element>
			<cfif arguments.methodName eq "getNewsHeadlines">
				<cfset pageView = "newNewsView">
			<cfelseif arguments.methodName eq "getNewsContent">
				<cfset pageView = "newsDetailsView">
				<cfif structKeyExists(arguments.mobileArgs,"contentID")>
					<cfset entityID = arguments.mobileArgs.contentID>
				</cfif>
			</cfif>
		</cfif>
		
		<cfif pageView neq "">
			<cfquery name="insertRequest" datasource="#application.siteDataSource#">
				insert into mobileTracking (visitID,viewTextID<cfif entityID neq 0>,entityID,entityTypeID</cfif>) values 
				(	<cf_queryparam value="#request.relayCurrentUser.visitID#" CFSQLType="CF_SQL_INTEGER">,
					<cf_queryparam value="#pageView#" CFSQLType="CF_SQL_VARCHAR">
					<cfif entityID neq 0>,<cf_queryparam value="#entityID#" CFSQLType="CF_SQL_INTEGER">,<cf_queryparam value="#entityTypeID#" CFSQLType="CF_SQL_INTEGER"></cfif>
				)
			</cfquery>
		</cfif>
	</cffunction>
</cfcomponent>