<!--- �Relayware. All Rights Reserved 2014 --->
<!--- dateFunctions.cfc

Author WAB

2005-04-20


A place to put date related functions


2007-10-17	WAB added functions for dealing with client times
2009/07/06	WAB mod to dontshowZeroHours behaviour of dateTimeFormat.  23:59:59 counts as a zero hours, and returns that day rather than next day
					ie   dateTimeFormat (date={ts '2009-07-31 23:59:59'},dontshowZeroHours=true ) gives 31 Jul 2009.  Previously gave 31 Jul 2009 23:59.   Doesn't give 1 Aug 2009  (might be argued that this would be better, maybe a switch required)
2009/12/08	WAB Internationalisation, added showAsLocalTime attribute to dateTimeFormat function 
2011-01-10 	NYB LHID5257 changed datetimeformat function
2011/09/26 	PPB	add ConvertIsoToDateTime function
2011/10/13	WAB Added function getDateTypeFormattingInfo()
2011/12/12	GCC Added function convertServerDateColumnToUTC()
2012-01-18	WAB extended DayDiffFromTodayInWords
2014-01-07 PPB Case 436687 new function isISODateString()
2015-03-02 AHL Case 443866. Long text with dates recognized as dates
2015-06-22 WAB During FIFTEEN-384 (Add time support to date Flags). Changed parameter type in combineDateFormFields() and moved call to applicationMain
2016-03-17	WAB During PROD2016-118  Housekeeping Improvements - added a getDbTime() function
2016-10-17	WAB During PROD2016-2542 Housekeeping problems when dbTime and CFTime are wildly different. Added functions to convert between the two
				 Also pulled out a DayDiffInWords() function from DayDiffFromTodayInWords
 --->

 
<cfcomponent displayname="Various Date Related functions" hint="Date Component">



	<!--- 
			getWorkDay ()
			
			WAB 2005-04-20
			
			By default brings you back the date of the next work day
			
			Can find the date of the next work day starting x days ahead
			NOTE:  this is not starting x Working Days ahead, but maybe it should 
			
			doesn't deal with holidays!!


			 --->

	<cffunction access="public" name="getWorkDay" returnType="Date" output="false">
		<cfargument NAME="date" type = "date" DEFAULT="#now()#">  <!--- date to start at --->
		<cfargument  NAME="daysAhead"type = "numeric" DEFAULT="1">  <!--- how many days ahead to start looking for a work day --->
 
					
				<!--- get rid of the time from the date passed --->
				<cfset var theDate = createODBCDate(dateformat(arguments.date,"yyyy-mm-dd"))>
				<cfset var dateToTry = dateAdd("d",arguments.daysAhead,theDate)>
				<cfset var dayOfWeek = dayOfWeek(dateToTry)>
					
				<cfloop condition = "dayOfWeek is 1 or dayOfWeek is 7">
					<cfset dateToTry = dateAdd("d",1,dateToTry)>	
					<cfset dayOfWeek = dayOfWeek(dateToTry)>
				</cfloop>
					
				<cfreturn dateToTry>

	</cffunction>

	<!--- i.e what is the date of this Saturday  getDateOfADayInThisWeek (now(),7)
		note that week begins on Sunday and goes to saturday
	--->
	
	<cffunction access="public" name="getDateOfADayInThisWeek" returnType="date" output="false">
			<cfargument NAME="date" type = "date" DEFAULT="#now()#">	
			<cfargument NAME="dayOfWeek" type = "numeric" DEFAULT="7">	
			
			<cfset var thisDay = datePart("w",date)>
			
			<cfset var result = dateAdd("d",dayOfWeek - thisDay,date)>
			<cfreturn result>
	
	</cffunction>
	
	<!--- 
	eg what is the date next Sunday getDateOfNextOccurenceOfADay(now(),1)
	 --->
	<cffunction access="public" name="getDateOfNextOccurenceOfADay" returnType="date" output="false">
			<cfargument NAME="date" type = "date" DEFAULT="#now()#">	
			<cfargument NAME="dayOfWeek" type = "numeric" DEFAULT="7">	
			
			<cfset var result ="">
			<cfset var thisDay = datePart("w",date)>
			<cfset var addDays = dayOfWeek - thisDay>

			<cfif addDays LTE 0 >
				<cfset addDays = addDAys + 7> 
			</cfif>
			
			<cfset result = dateAdd("d",addDays,date)>
			<cfreturn result>
	
	</cffunction>


	<!--- adds hours to a date but only in work hours (as defined by the parameters) --->
	<cffunction access="public" name="addWorkHours" returnType="Date" output="false">
		<cfargument NAME="date" type = "date" DEFAULT="#now()#">  <!--- date to start at --->
		<cfargument  NAME="hours"type = "numeric" DEFAULT="0">  <!--- how many hours to add --->
		<cfargument  NAME="workDayStart"type = "numeric" DEFAULT="8">		 
		<cfargument  NAME="workDayEnd"type = "numeric" DEFAULT="18">		 
		<cfargument  NAME="debug"type = "boolean" DEFAULT=false>		 							

		<cfset var dayOfWeek = '' />
		<cfset var currentTime = '' />
		<cfset var hoursToUse = '' />

		<cfset var hoursLeftToAdd = arguments.hours>
		<cfset var currentDate = arguments.date>
		<cfset var counter = 0>  <!--- this is just to prevent a runaway loop and could be removed if debugging is successful --->

		
	<cfloop condition ="hoursLeftToAdd gt 0 and counter lt 20">

			<cfset dayOfWeek = dayOfWeek(currentDate)>
			<cfset currentTime = removeDateFromTime (currentDate)>
									<cfif debug><cfoutput>Current Time = #currentTime#, hours left = #hoursLeftToAdd#<BR></cfoutput></cfif>

					
		<cfif dayOfWeek is 1 or dayOfWeek is 7>
			<!--- not a work day, so go to next day --->
			<cfset currentDate = dateAdd("d",1,removeTimeFromDate(currentDate))>	
									<cfif debug><cfoutput>A: date set to #currentDate#, hours left = #hoursLeftToAdd#<BR></cfoutput></cfif>

		<cfelse>	
	
			<cfif workDayStart/24 - currentTime gt 1/(24*60*60)>  <!--- time before start of day, so move date forward to start of day --->
				<cfset currentDate = dateAdd("h",workDayStart,removeTimeFromDate(currentDate))>
									<cfif debug><cfoutput>B: date set to #currentDate#, hours left = #hoursLeftToAdd#<BR></cfoutput></cfif>

			<cfelseif currentTime lt workDayEnd/24>		<!--- time is in middle of day so add as many hours as we can --->
				<cfif workDayEnd/24 - currentTime gte hoursLeftToAdd/24>   
					<cfset hoursToUse = hoursLeftToAdd>		<!--- if all the hours can be added within this day then add them--->
				<cfelse>	
					<cfset hoursToUse = workDayEnd - currentTime*24 >  <!--- NOT all the hours can be added within this day so ad as many as possible --->
				</cfif> 
	<!--- 				<cfset currentDate = dateAdd("h",hoursToUse,currentDate)> --->
					<cfset currentDate = hoursToUse/24 + currentDate>
					<cfset hoursLeftToAdd = hoursLeftToAdd - hoursToUse>
									<cfif debug><cfoutput>C: date set to #currentDate#, hours left = #hoursLeftToAdd#<BR></cfoutput></cfif>
			<cfelse>
					<cfset currentDate = dateAdd("d",1,removeTimeFromDate(currentDate))>				
									<cfif debug><cfoutput>D: date set to #currentDate#, hours left = #hoursLeftToAdd#<BR></cfoutput></cfif>
			</cfif>
		</cfif>
			
		<cfset counter = counter + 1>
		<cfset currentDate = createODBCDateTime(currentDate)>
		
	</cfloop>	
		
		<cfreturn currentDate>

	</cffunction>


	<cffunction access="public" name="getDBDateTime" output="false" hint="">		
		<cfset var qryDBDateTime = "">
		<cftry>
	 		<cfquery name="qryDBDateTime" datasource="#application.siteDataSource#">
		 		select getdate() as DBDateTime
			</cfquery>
			<cfreturn qryDBDateTime.DBDateTime>	
			<cfcatch type="any">
				<cfset ErrorMsgNotification(source="getDBDateTime", errorCatch=cfcatch)>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cffunction>
		
	
	<cffunction access="public" name="removeTimeFromDate" returnType="Date"output="false">
		<cfargument NAME="date" type = "date" DEFAULT="#now()#">  
 
			<!--- get rid of the time from the date passed --->
			<cfreturn createODBCDate(dateformat(arguments.date,"yyyy-mm-dd"))>
	</cffunction>

	<cffunction access="public" name="removeDateFromTime" returnType="date" output="false">
		<cfargument NAME="date" type = "date" DEFAULT="#now()#">  
 
			<!--- get rid of the time from the date passed --->
			<cfreturn createODBCTime(timeformat(arguments.date,"HH:mm:ss"))>
	</cffunction>


	<!--- 
	WAB 2009/12/08
	Internationalisation
	Added showAsLocalTime attribute
	Changed standard format to medium rather than dd mm yyyy and changed to use LSDateFormat
	 --->
	<!--- 	NYB 2011-01-10 LHID5257 - changed
			added a mask argument, so datetimes can now be formatted by just passing 
			through a date and mask argument.  The change is backward compatible.
			Change is java so the mask is case sensitive.  See documentation for options:
			http://download.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html
	 --->
	<cffunction access="public" name="dateTimeFormat" output="false">
		<cfargument NAME="date" type = "date" required="true"> 
		<cfargument NAME="mask" type = "string">   
		<cfargument NAME="dateMask" type = "string" default = "medium">   <!--- dd mmm yyyy --->
		<cfargument NAME="timeMask" type = "string" default = "HH:mm">  
		<cfargument NAME="separator" type = "string" default = " ">  
		<cfargument NAME="dontshowZeroHours" type = "boolean" default = "false">  
		<cfargument NAME="showAsLocalTime" type = "boolean" default = "false">    <!--- WAB 2009/12/08 --->
		<cfargument name="showIcon" type="boolean" default="true">
 
 		<cfset var result = "">
		<cfset var title = "phr_serverTime">
		<cfset var icon = "">
		<cfset var SDFclass = '' />
		<cfset var SDFinstance = '' />

		<cfif showAsLocalTime>
			<cfset arguments.date = convertServerDateTimeToClientDateTime(arguments.date)>
		</cfif>

		<cfif structkeyexists(arguments,"mask")>
			<cftry>
				<cfset SDFclass=CreateObject("java", "java.text.SimpleDateFormat") >
				<cfset SDFinstance = SDFclass.getInstance()>
				<cfset SDFinstance.applyPattern(mask)>
				<cfset result = SDFinstance.format(arguments.date)>
			<cfcatch type="any">
				<cfif Find(separator, mask) gt 0>
					<cfset arguments.dateMask = Left(mask, Find(separator, mask)-1)>
					<cfset arguments.timeMask = Right(mask, (len(mask)-Find(separator, mask)))>
				<cfelse>
					<cfset result = dateFormat(date,mask)>
				</cfif>
				<cfset structdelete(arguments,"mask")>
			</cfcatch>
			</cftry>
		</cfif>
		<cfif len(result) eq 0>
			<cfset result = lsdateFormat(date,dateMask)>
			<cfif dontshowZeroHours is false or (timeFormat(date,"HH:mm:ss") is not "00:00:00" and timeFormat(date,"HH:mm:ss") is not "23:59:59")>
				<cfset result = result & separator & timeFormat(date,TimeMask)>
			</cfif>
			<!--- NYB have placed the following inside "if len(result)" because I do not want 
			showIcon to be the default for my new functionality --->
		<cfif arguments.showIcon>
			<cfif arguments.showAsLocalTime>
				<cfset title="phr_localTime">
			</cfif>
			<cfset icon='<span title="#title#"><img src="/images/icons/time.png"></span>'>
			<cfset result = result&icon>
		</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- 
	******************************************
	takes a date and compares to todays date.  Returns the difference in words
	returns  x days ago / yesterday/today/tomorrow/ in x days
	WAB 2005-09-12
	WAB 2012-01-19 added doTime parameter for doing more detail when difference is small
	WAB 2016-03-20  Added "Now" when difference 0 minutes
	WAB 2016-10-17	Pulled main code out into a separate function which does compare to now()
	******************************************
	 --->

	<cffunction access="public" name="DayDiffFromTodayInWords" returnType="string" output="false">
		<cfargument NAME="date" type = "date">  
		<cfargument NAME="doTime" default="false"> 

		<cfreturn DayDiffInWords (date = date, dateFrom = now(), doTime = doTime )>

	</cffunction>


	<cffunction access="public" name="DayDiffInWords" returnType="string" output="false">
		<cfargument NAME="date" type = "date">  
		<cfargument NAME="dateFrom" type = "date">  
		<cfargument NAME="doTime" default="false"> 
		
			<cfset var daydiff = dateDiff("d",removeTimeFromDate(dateFrom),removeTimeFromDate(date))>
			<cfset var timediff = dateDiff("n",dateFrom,date)>						
			<cfset var abstimediff = abs(TimeDiff)>						
			<cfset var result = "">
			<cfset var plural = "s">
			<cfset var timeInPast = iif(timediff lte 0,true,false)>
			<cfset var units = "">
			
				<cfif doTime and abstimeDiff lt 60*24>
					<cfif abstimediff gt 59>
						<cfset units = "hour">
						<cfset abstimediff = int(abstimediff/60)>
					<cfelse>
						<cfset units = "minute">
					</cfif>
					<cfif abstimediff is 1>
						<cfset plural = "">
					</cfif>
					<cfif units is "minute" and abstimediff is 0>
						<cfset result = "Now">
					<cfelse>
					<cfset result = '#iif(timeInPast,de(""),de("In "))##abstimediff# #units##plural##iif(timeInPast,de(" ago"),de(""))#'>
					</cfif>
					
				<cfelseif daydiff is 0>
					<cfset result = "Today">
				<cfelseif daydiff is 1>
					<cfset result = "Tomorrow">
				<cfelseif daydiff is -1>
					<cfset result = "Yesterday">
				<cfelseif timeInPast>
					<cfset result = "#abs(daydiff)# days ago">
				<cfelseif not timeInPast>
					<cfset result = "in #abs(daydiff)# days">
				</cfif>
		
		
			<cfreturn result>		
 
	</cffunction>


	<!--- WAB 
	2006-02-01
	turns a timespan into words (only give to one unit of accuracy so just hours or minutes, not both)
	 --->
	<cffunction access="public" name="TimeSpanInWords" returnType="string" output="false">
		<cfargument NAME="timespan" >  
			<cfset var dateparts = "365,30,7,1,h,n,s">
			<cfset var datepartnames = "years,months,weeks,days,hours,minutes,seconds">
			<cfset var datepartnamesSingular = "year,month,week,day,hour,minute,second">
			<cfset var result = "">			
			<cfset var value = 0>
			<cfset  var i = 0>


			<cfif timespan is "">
				<cfset value = 0>				
			<cfelseif timespan lt 1>
				<cfset i = 4>
				<cfloop condition = "value is 0 and i lt 7">
					<cfset i = i + 1>
					<cfset value = datePart (listgetat(dateparts,i),timespan)>
				</cfloop>

			<cfelse>
			
				<cfloop condition = "value is 0 and i lt 5">			
					<cfset i = i + 1>
					<cfset value = int  (timespan/listgetat(dateparts,i))>
				</cfloop>
			
			

			</cfif>


				<cfif value is 0>
					<cfset result = "">
				<cfelseif value is 1>
					<cfset result = value & " " & listgetat(datepartnamessingular,i)>
				<cfelse>
					<cfset result = value & " " & listgetat(datepartnames,i)>
				</cfif>
		
			<cfreturn result>		
 
	</cffunction>	
	


	<!--- 
	******************************************
	Function used to join together date and time fields submitted as separate items on a form
	
	******************************************
	
		NJH 2014/04/25 - pass through the scope as the date can be passed in via url or form. Doing a form 'get' in this case for the connector dashboard...
	 --->
	<cffunction access="public" name="combineDateAndTimeFormFields" returnType="string" output="false">
		<cfargument NAME="baseVariableName" type = "string" required="true">  	
		<cfargument NAME="scope" type = "struct" required="true"> 	
		
		<cfset var theTime = '' />
		<cfset var date = "">
		
		<cfset var theScope = arguments.scope>
		
		<cfif structKeyExists(theScope,arguments.baseVariableName)>
			<cfset date = theScope[arguments.baseVariableName]>
		</cfif>

		<cfif date is not "">
			<cfif structKeyExists(theScope,"#baseVariableName#_hr")>
				<cfset date = dateAdd("h",theScope["#baseVariableName#_hr"],date)>
				<cfif structKeyExists(theScope,"#baseVariableName#_min")>
					<cfset date = dateAdd("n", theScope["#baseVariableName#_min"],date)>
				</cfif>
			<cfelseif structKeyExists(theScope,"#baseVariableName#_tm")>
				<cfset theTime = removeDateFromTime(parsedatetime(theScope["#baseVariableName#_tm"]))>
				<cfset date = date + theTime>
			</cfif>
		</cfif>

		<!--- If this variable exists, then this is a field which has been entered as a local time and we need to convert to server time --->
		<cfif structKeyExists(theScope,"#baseVariableName#_localTime")>
			<cfset date = application.com.datefunctions.convertClientDateTimeToServerDateTime (date)>
		</cfif>

		<cfreturn date>

	</cffunction>
	

<!--- 
function used to take various date form fields and return a single date value
WAB 2015-06-22 During FIFTEEN-384. Alteration so that this function is only called once - in applicationMain.  
				Changed Scope to be the actual scope rather than its name - bring in line with combineDateAndTimeFormFields
 --->

	<cffunction access="public" name="combineDateFormFields" returnType="string" output="false">
		<cfargument NAME="baseVariableName" type = "string" required="true">  	
		<cfargument NAME="scope" type = "struct" required="true">  	
		
		<cfset var theScope = scope>
		<cfset var result = "">
		<cfset var theDay = 1>
		<cfset var theMonth = 1>
		<cfset var theYear = 0>
		<cfset var fieldValue = '' />		

		<!--- if the base variable does not exist or is blank then look for individual form fields --->
		<cfif not structKeyExists(theScope,baseVariableName) or theScope[baseVariableName] is "">
	
			<!--- this bit handles form fields from drop downs (or individual day/month/year fields --->				
			<cfif (structKeyExists(theScope,"#baseVariableName#Day")) 
						or 
			 	(structKeyExists(theScope,"#baseVariableName#Month") ) 
						or 
				(structKeyExists(theScope,"#baseVariableName#Year") )>

					<cfif structKeyExists(theScope,"#baseVariableName#Day")>
						<cfset theDay = theScope["#baseVariableName#Day"]>
					</cfif>				
					<cfif structKeyExists(theScope,"#baseVariableName#Month")>
						<cfset theMonth = theScope["#baseVariableName#Month"]>
					</cfif>				
					<cfif structKeyExists(theScope,"#baseVariableName#Year")>
						<cfset theYear = theScope["#baseVariableName#Year"]>
					</cfif>				
					

					<cftry>
						<cfset result = CreateDateTime (theYear,theMonth,theDay,0,0,0)>
		
						<cfcatch>				
							<!--- if any of the form fields were blank, the above will fall over, so catch an return "" --->		
							<cfset result = "">
						</cfcatch>
					</cftry>
		
			<cfelse>
				<cfset result = "">
			</cfif>
		<cfelseif listlen(theScope[baseVariableName],"-") is 3 and len(trim(theScope[baseVariableName])) LTE 10>
				<!--- this bit handles a single form field containing a date in the format dd-mm-yyy --->
					<cfset fieldValue = theScope[baseVariableName]>
					<cfset result = CreateDateTime (listgetat(fieldValue,3,"-"),listgetat(fieldValue,2,"-"),listgetat(fieldValue,1,"-"),0,0,0)> 

		<cfelse>
			<cfset result = theScope[baseVariableName]>
		</cfif>
						
		<cfreturn result>	

	</cffunction>


	<cffunction name="requestTimeUTC" output="false">
		<cfreturn convertServerDateToUTC(request.requestTime)>
	</cffunction>


	<cffunction name="nowUTC" output="false">

		<cfreturn convertServerDateToUTC(now())>
	
	</cffunction>

	<cffunction name="convertServerDateToUTC" output="false">
		<cfargument NAME="date" type = "date" required="true">  			
		
		<cfreturn dateadd("s",gettimezoneinfo().utctotalOffset,date)>
		
	</cffunction>

	<cffunction name="nowClientTimeZone" output="false">

		<cfreturn 	convertServerDateTimeToClientDateTime (now())>
		
	</cffunction>
	
	<cffunction name="convertServerDateTimeToClientDateTime" returns="date" output="false">
		<cfargument NAME="date" type = "date" required="true">  			

		<cfreturn dateadd("n",-request.relaycurrentuser.localeInfo.clientServerTimeOffsetMinutes,date)>
	
	</cffunction>

	<cffunction name="convertClientDateTimeToServerDateTime" returns="date" output="false">
		<cfargument NAME="date" type = "date" required="true">  			

		<cfreturn dateadd("n",request.relaycurrentuser.localeInfo.clientServerTimeOffsetMinutes,date)>
	
	</cffunction>

	<cffunction name="convertServerDateColumnToUTC" returns="query" output="false">
		<cfargument NAME="queryObject" type="query" required="true">  
		<cfargument NAME="columnNameList" type="string" required="true" hint="comma separated">  
	
		<cfset var columnName="">
		<cfset var columnValue="">
		<cfset var dateInUTC="">
	
		<cfloop query="queryObject">
			<cfloop list="#arguments.columnNameList#" index="columnName" delimiters=",">
				<cfset columnValue = queryObject[columnName][currentrow]>
				<cfset dateInUTC = application.com.dateFunctions.convertServerDateToUTC(date=columnValue)>
				<cfset querysetcell(arguments.queryObject,columnName,dateInUTC,currentrow)>
			</cfloop>
		</cfloop>	
	
		<cfreturn arguments.queryObject>
	</cffunction>
	
	<cffunction access="public" name="getDateOfFirstOfMonth" returnType="date" output="false">
			<cfargument NAME="date" type = "date" DEFAULT="#now()#">	
			
			<cfset var result = createDate(datePart("yyyy",date),datePart("m",date),1)>

			<cfreturn result>
	
	</cffunction>

	<!--- WAB 2008/12/01 error in my function when we got to December! --->
	<cffunction access="public" name="getDateOfLastOfMonth" returnType="date" output="false">
			<cfargument NAME="date" type = "date" DEFAULT="#now()#">	
			<cfset var result = "">
						
			<cfset result = dateAdd("m",1,getDateOfFirstOfMonth(date))>
			<cfset result = dateAdd("d", -1,result)>
			
			<cfreturn result>
	
	</cffunction>
	
	<cffunction name="getUtcSecondsOffset" returntype="numeric" output="false">
		<cfreturn getUtcOffset(datepart="s")>
	</cffunction>

	<!--- WAB 2013-06-18 Added caching of the Offset - since doesn't change and I was doing 7000 in one request --->
	<cffunction name="getUtcOffset" returnType="numeric" output="false">
		<cfargument name="datepart" type="string" default="s">
		
		<cfset var getOffset = "">
		
		<cfparam name="variables.UTcOffset" default = "#structNew()#">

		<cfif not structKeyExists (variables.UTcOffset, datepart)>
			<cfquery name="getOffset" datasource="#application.siteDataSource#">
			select datediff(#arguments.datepart#,getdate(),getutcdate()) as offset
			</cfquery>
			<cfset variables.UTcOffset[datePart] = getOffset.offset>
		</cfif>	
		
		<cfreturn variables.UTcOffset[datePart]>
	</cffunction>
	
	
	<cffunction name="ConvertIsoToDateTime" access="public" returntype="date" output="false">
		<cfargument name="sDate" required="yes" type="string" hint="Properly formed ISO-8601 dateTime String">
		<cfargument name="sConvert" required="no" type="string" default="local" hint="utc|local">
		
		<!--- 2011/09/26 PPB function lifted from discussion @ http://www.bennadel.com/blog/811-Converting-ISO-Date-Time-To-ColdFusion-Date-Time.htm --->
		
		<cfset var sWork = "">
		<cfset var sDatePart = "">
		<cfset var sTimePart = "">
		<cfset var sOffset = "">
		<cfset var nPos = 0>
		<cfset var dtDateTime = CreateDateTime(1900,1,1,0,0,0)>
		<!--- Trim the inbound string; set it to uppercase in preparation for conversion --->
		<cfset sWork = UCase(Trim(Arguments.sDate))>
		<!--- if the rightmost character of the sting is "Z" (for Zulu meaning UTC), remove it and change the offset to positive-zero) --->
		<cfif Right(sWork,1) IS "Z">
		<cfset sWork = Replace(sWork,"Z"," +0000", "ONE")>
		</cfif>
		<!--- extract the "T" and split out the date --->
		<cfif sWork CONTAINS "T">
		<cfset sWork = Replace(sWork, "T", " ", "ONE")>
		</cfif>
		<cfset sDatePart = ListFirst(sWork, " ")>
		<cfset sTimePart = ListGetAt(sWork, 2, " ")>
		<!--- figure out where the offset begins in the time part --->
		<cfset nPos = REFind("[\+\-]",sTimePart)>
		<cfif nPos GT 0>
		<!--- split out the offset from the time part --->
		<cfset sOffset = Right(sTimePart,Len(sTimePart)-nPos+1)>
		<cfset sTimePart = Replace(sTimePart,sOffset,"","ONE")>
		</cfif>
		<!--- convert the parts into the formats that are needed for conversion to POP datetime format --->
		<cfset sDatePart = DateFormat(sDatePart,"ddd, dd mmm yyyy")>
		<cfset sTimePart = TimeFormat(sTimePart,"HH:mm:ss")>
		<cfset sOffset = Replace(sOffset,":","","ALL")>
		<!--- parse the date, time and offset parts as a POP datetime formatted string --->
		<cfset dtDateTime = ParseDateTime("#sDatePart# #sTimePart# #sOffset#","pop")>
		<cfdump var="#dtDateTime#">
		<!--- convert the date time to local time if required --->
		<cfif Arguments.sConvert IS "local">
		<cfset dtDateTime = DateConvert("utc2local",dtDateTime)>
		</cfif>
		<!--- return the date-time object, which allows the calling process to handle it however it likes --->
		<cfreturn dtDateTime>
	</cffunction>


	<!--- WAB 2011/10/13 a piece of code cut out of cf_relayDateField because I needed the info somewhere else 
			LID 7965
	--->
	<cffunction name="getDateTypeFormattingInfo" output="false">
		<cfargument name="dateType" default="#request.relaycurrentuser.localeInfo.dateType#" hint="EURO,US or ISO">
	
		<cfset var result = {}>
		<CFSWITCH expression="#ucase(arguments.dateType)#">
			<cfcase value="EURO">
				<cfset result.datemask = "dd/MM/yyyy">
				<cfset result.dateHiddenFieldSuffix = "EURODATE">
			</cfcase>
			<cfcase value="US">
				<cfset result.datemask = "MM/dd/yyyy">
				<cfset result.dateHiddenFieldSuffix = "DATE">
			</cfcase>
			<cfcase value="ISO">
				<cfset result.datemask = "yyyy/MM/dd">
				<cfset result.dateHiddenFieldSuffix = "DATE">
			</cfcase>
			<cfdefaultcase>
				<cfset result.datemask = "dd/MM/yyyy">
				<cfset result.dateHiddenFieldSuffix = "EURODATE">
			</cfdefaultcase>
		
		</CFSWITCH>

		<cfreturn result>

	
	</cffunction>

	<!---NJH 2012/11/04 Case 431491 --->
	<cffunction name="extractDateTimeFromUTCString" access="public" returnType="struct" output="false">
		<cfargument name="dateInUTCFormat" type="string" required="true">
		
		<cfset var result = {isOK=true,utcDateTime=arguments.dateInUTCFormat,message="",localDateTime=""}>
		<cfset var utcDateTime = arguments.dateInUTCFormat>
		<cfset var utcTimeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset()>
		
		<!--- looking for format of yyyy-mm-ddThh:mm:ssZ  --->
		<cfif find("T",arguments.dateInUTCFormat) eq 11 and right(arguments.dateInUTCFormat,1) eq "Z">
			<cfset utcDateTime = replace(replace(arguments.dateInUTCFormat,"T"," "),"Z","")>
			<cfset result.localDateTime = dateAdd("s",-utcTimeOffsetInSeconds,utcDateTime)>
			<cfset result.utcDateTime = utcDateTime>
		<cfelse>
			<cfset result.isOk = false>
			<cfset result.message = "Date not in UTC format">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="GetTimeZoneInfoDB" access="public" returntype="struct" output="false">
		<cfset var result = structNew()>
		
		<cfset result.utcTotalOffset = application.com.dateFunctions.getUtcOffset(datepart="ss")>
		<cfif result.utcTotalOffset lt 0>
			<cfset result.utcHourOffset = round(result.utcTotalOffset/3600)>
		<cfelse>
			<cfset result.utcHourOffset = int(result.utcTotalOffset/3600)>
		</cfif>
		
		<cfset result.utcMinuteOffset = (result.utcTotalOffset/60) mod 60>
		
		<cfreturn result>
	</cffunction>

	<!--- 2014-01-07 PPB Case 436687 this function exists because isDate() is unreliable eg isDate('12.5') returns true  --->
	<cffunction name="isISODateString" access="public" returntype="boolean" output="false">
		<cfargument name="dateString" type="string" required="true">
		
		<cfset var result = false>

		<cfif REFind("[0-9]{4}-[0-9]{2}-[0-9]{2}",dateString) gt 0 and len(dateString) lte 10 >			<!--- matching YYYY-MM-DD --->	<!--- 2015-03-02 AHL Case 443866. Long text with dates recognized as dates --->
			<cfset result = true>
		</cfif>
		
		<cfreturn result>
	</cffunction>

	<!--- 2015-05-12 AHL SAML SSO, Adding Date ISO Data String --->
	<cffunction name="dateToIsoDateString" access="public" returntype="string" output="false" >
		<cfargument name="date" type="date" required="true" default="#now()#">
		<cfreturn 	'#DateFormat(DateConvert('local2utc',arguments.date),'YYYY-MM-DDT')#	& #TimeFormat(DateConvert('local2utc',arguments.date()),'HH:mm:SSZ')#' />
	</cffunction>

	<!---
	 Functions used by the SAML SP developed by WAB
		START  2015-09-22 AHL
	 --->
	<cffunction name="getISOTimeString" output="false" hint="Makes a datetime string in ISO format">
		<cfargument name="datetime" type="date" default="#now()#">
		<cfargument name="convertToUTC" default = false>
		
		<cfset var offsetString  = "">
		<cfif convertToUTC>
			<cfset arguments.datetime =  dateConvert( "local2utc", datetime )>
			<cfset offsetString = "Z">
		<cfelse>
			<cfset offsetString = getTimeZoneOffsetString()>
		</cfif>

         <cfset var result = dateFormat(arguments.datetime,'yyyy-MM-dd') & "T" & timeFormat(arguments.datetime,'HH:mm:ss') & offsetString>
		
		<cfreturn result>		
	
	</cffunction>


	<cffunction name="getTimeZoneOffsetString" output="false" hint="Creates the time zone string for getISOTimeString()">
		<cfset var info = GetTimeZoneInfo()>

		<cfset var sign = "">		
		
		<cfif info.utcTotalOffset is 0>
			<cfset var result = "Z">
		<cfelse>	
			<cfset var mins = numberformat(abs((info.utcTotalOffset mod 3600)/60),"00")>
			<cfset var hours = numberformat(abs(int(info.utcTotalOffset/3600)),"00")>
			<cfif info.utcTotalOffset gt 0>
				<cfset sign = "-">
			<cfelse>	
				<cfset sign = "+">
			</cfif> 
			<cfset var result = sign & hours & ":" & mins>

		</cfif>
				
		<cfreturn result>	
	
	</cffunction>
	<!---
	 Functions used by the SAML SP developed by WAB
		END  2015-09-22 AHL
	 --->



	<cffunction name="getCFToDBTimeOffset" hint="Gets difference between CF Time - NOW() and dbTime getDate() in milliseconds.  Not sure whether I have got the sign logically correct, but it does the job">
		<cfargument name="refresh" default = "false">

		<cfif refresh or not structKeyExists (variables,"CFToDBTimeOffsetMilliSeconds")>
		
			<cfset var getCFToDBTimeOffset = "">
			<CFQUERY NAME="getCFToDBTimeOffset" >
			SELECT dateDiff(ms,GETDATE(),'#dateformat(now(),"yyyy-mm-dd")# #timeformat(now(),"HH:mm:ss.l")#') [milliseconds], getdate(), cast ('#dateformat(now(),"yyyy-mm-dd")# #timeformat(now(),"HH:mm:ss.l")#' as datetime)
			</CFQUERY>

			<cfset variables.CFToDBTimeOffsetMilliSeconds = getCFToDBTimeOffset.milliseconds>
			<cfset variables.NowOffLastRefreshed_Hour = datePart ("h",now())>
		</cfif>	

		<!--- WAB 2016-05-23 Ensure that offset is updated on the hour (which will also hopefully deal with any summer time issues) --->
		<cfif datePart ("h",now()) is not variables.NowOffLastRefreshed_Hour>
			<cfset getCFToDBTimeOffset (refresh = true )>
		</cfif>

		<cfreturn variables.CFToDBTimeOffsetMilliSeconds>
	
	</cffunction>
	

	<cffunction name="getDBTime" hint="Gets a dateTime which is the same as the db date() but without going to the db every time.">
		<cfargument name="refresh" default = "false">

		<cfreturn dateAdd ("l",- getCFToDBTimeOffset() ,now())>

	</cffunction>


	<cffunction name="convertDBTimeToCFTime" hint="Takes a database time and converts to a CFServer Time.  Used in situations when time must be CF time (such as scheduledTasks) but the time of running is determined by the db.  Difference is not usually important, but can be if CF server is in a different timeZone">
		<cfargument name="dbTime" required = true type="date">
		
		<cfreturn dateAdd ("l", getCFToDBTimeOffset() , dbTime)>
	
	</cffunction>


	<cffunction name="convertCFTimeToDBTime" hint="Takes a database time and converts to a CFServer Time.  Used in situations when time must be CF time (such as scheduledTasks) but the time of running is determined by the db.  Difference is not usually important, but can be if CF server is in a different timeZone">
		<cfargument name="CFTime" required = true type="date">
		
		<cfreturn dateAdd ("l", - getCFToDBTimeOffset() , CFTime)>
	
	</cffunction>


	<cffunction name="getMonthsOfYear" output="false" hint="Returns the months 1-12 and the translations for those months">
		<cfscript>
			
			if(!structKeyExists(this,"monthsOfYear")){
				var monthsOfYearArray=ArrayNew(1);
				monthsOfYearArray[1]={data=1, display="phr_sys_january"};
				monthsOfYearArray[2]={data=2, display="phr_sys_february"};
				monthsOfYearArray[3]={data=3, display="phr_sys_march"};
				monthsOfYearArray[4]={data=4, display="phr_sys_april"};
				monthsOfYearArray[5]={data=5, display="phr_sys_may"};
				monthsOfYearArray[6]={data=6, display="phr_sys_june"};
				monthsOfYearArray[7]={data=7, display="phr_sys_july"};
				monthsOfYearArray[8]={data=8, display="phr_sys_august"};
				monthsOfYearArray[9]={data=9, display="phr_sys_september"};
				monthsOfYearArray[10]={data=10, display="phr_sys_october"};
				monthsOfYearArray[11]={data=11, display="phr_sys_november"};
				monthsOfYearArray[12]={data=12, display="phr_sys_december"};
				
				this.monthsOfYear=application.com.structureFunctions.arrayOfStructuresToQuery(monthsOfYearArray);
			}	
			return this.monthsOfYear;		
		</cfscript>
	
	</cffunction>
	 
	<cffunction name="getAllDaysInMonth" output="false" hint="Returns the days (e.g. 1,2,3,4) as a query in the requested month IF NOT A LEAP YEAR">
		<cfargument NAME="monthID" type = "numeric" required="true" hint="1 for january etc">	
		<cfscript>
			
			
			if(!structKeyExists(this,"daysInMonthsOfYear")){
				this.daysInMonthsOfYear=structNew();
			}	
			
			if(!structKeyExists(this.daysInMonthsOfYear,"monthID")){
				var daysArray=ArrayNew(1);
				var numberOfDaysInMonth=daysInMonth(CreateDate(2001,arguments.monthID,1)); //2001 isn't a leap year (nor is 2000 but that may be unclear)
				
				for(var i=1;i<=numberOfDaysInMonth;i++){
					daysArray[i]={data=i, display=i};
				}
				
				
				this.daysInMonthsOfYear[monthID]=application.com.structureFunctions.arrayOfStructuresToQuery(daysArray);
			}	
			return this.daysInMonthsOfYear[monthID];		
		</cfscript>
	
	</cffunction>
	 
	 
	<cffunction name="clampDate" output="false" hint="clamps a date to be within a range">

		<cfargument name="date" required="true">
		<cfargument name="min" required="true">
		<cfargument name="max" required="true">
		DateCompare
		<cfif DateCompare(date,min) LT 0>
			<cfreturn min>
		<cfelseif DateCompare(date,max) GT 0>
			<cfreturn max>
		<cfelse>
			<cfreturn date>
		</cfif>
	</cffunction>
</cfcomponent>	

