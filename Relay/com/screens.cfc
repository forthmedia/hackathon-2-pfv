<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAb 2007/12/?     Added new functions to do with entity navigation
WAb 2007/? 2008/03/03	Function to bring back ist of screens a flag or group appears in
WAB 2008/03/26		Added countryid  and organisationcountryid  to getScreenList, this allows the list of screens to be countryspecific
					bit nasty to have two variables, but felt needed to be able to deal with locations in different country to location
WAB 2008/07/08   code to aid transition to new 3 pane view
AJC 2008/07/2008 Lenovo Support:779:PRM Error - Was missing the "s." prefix so was causing ambiguous error
WAB 2008/11/10   T-10 Bug Issue 857 removed hack for 3 pane view transition
WAB 2008/01/20   Lexmark Support Issue 1578 above didn't seem to have been done properly - too many nested comments and the critical line was still intact!  Now really removed
SSS 2009/09/03   Added a function to tell you if a screen Exists function name doesScreenExist
2014-08-11			RPW Create new profile type: "Financial"
NJH 2015/12/02	JIRA Fifteen-141 changed from inner join to usergroup to left join as entityId 0 was to mean all usergroups
WAB 2015-02-25	altered getScreenAndDefinition to use new dbo.getScreenItems() function
2015-12-01 	WAB Altered recordRights to use correct entity name 'Screens' not 'Screen'
2016-05-04	WAB BF-676 Knock on effect of PROD2016-878. Change getScreenList uses argument entityTypeID not entityType.  Prevents any issues with organiSation and organiZation.  Bring back correct label from the schema table
--->

<cfcomponent displayname="screens" hint="Screen functions">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getScreenList" hint="Get select box for screens entries" returntype="query">

		<cfargument name="EntityTypeID" type="numeric" required="yes">
		<cfargument name="organisationTypeID" type="string" required="no" default="0">
		<cfargument name="includeParentTypes" default=false>
		<cfargument name="personid" type="numeric" default="#request.relayCurrentUser.personid#">
		<cfargument name="usergroupid" default="">   <!--- WAB 2007/12/10 can pass in a usergroupid to use instead of a personid --->
		<cfargument name="countryid" default=""> <!--- WAB 2008/03/26 only show screens which are of the right country, cuold --->
		<cfargument name="organisationcountryid" default="#countryid#">    <!--- if different from location country then need to pass in separately --->
		<cfargument name="sortorder" type="string" default="GroupingSort asc, rtrim(s.ScreenName)"> <!--- NJH 2016/04/06 sugar 448818 - pass in sortorder so that the 'More' link in 3-pane view shows screen in alphabetical order. Have left old sortOrder in case it is still used --->

		<cfscript>
			var getScreenList = "";
			var entityTypeIDList = EntityTypeID ;
			var translatedGroup = structNew();

			if (includeParentTypes) {
				if (listFind(entityTypeIDList,0)) {
					entityTypeIDList = listappend(entityTypeIDList, 1) ;
				}
				if (listFind(entityTypeIDList,1)) {
					entityTypeIDList = listappend(entityTypeIDList, 2);
				}
			}
		</cfscript>

		<!--- if passed a usergroupid then find out whether it is a personal usergroupid or a group
			if it is a personal group, then it is easier for my queries if actually use the personid
		--->
		<cfif usergroupid is not "">
			<cfquery name="local.getUserGroup" dataSource="#application.siteDataSource#">
			select personid from usergroup where userGroupID =  <cf_queryparam value="#usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfif local.getUserGroup.personid is not "">
				<cfset arguments.personid = local.getUserGroup.personid>
				<cfset arguments.usergroupid = "">
			</cfif>
		</cfif>

		<!--- WAB 2012-04-18 CASE 427759, lower cased screenText ID (change to JSON encoding meant that it was no longer guaranteed to be lower case when used in javascript) --->
		<cfquery name="getScreenList" dataSource="#application.siteDataSource#">
			SELECT distinct 
				s.screenID, 
				rtrim(s.ScreenName) as screenname, 
				lower(s.screenTextID) as screenTextID, 
				s.sortorder, 
				s.entityTypeID,
				st.entityName as entityType,
				<!--- WAB 2007/11: A rather basic way of having grouping (just put group=My Group in the parameters field
					 probably needs something more robust when we get a better idea of what is needed
				--->
				case when parameters like '%group=%' then substring (parameters,charindex('group=',parameters)+6,charindex(',',parameters +',',charindex('group=',parameters)+6)-charindex('group=',parameters)-6)  else st.label + ' Screens' end as grp,
				case when parameters like '%group=%' then 99 else s.EntityTypeID end as GroupingSort,
				case when parameters like '%NotEntitySpecific%' then 1 else 0 end as nes    <!--- NotEntitySpecific  these are screens which are not really associated with the current entity eg: Add Organisation, Page Preferences and we don't want to show the entity name in the navigation and don't want to remember them as default pages --->
			from
				screens s
					inner join
				schemaTable st on s.entityTypeID = st.entityTypeID
					left join
				screenOrganisationType sot on s.screenid = sot.screenid
					left join
				(recordrights rr  inner join <cfif usergroupid is "">rightsgroup <cfelse> usergroup </cfif>g on rr.usergroupid = g.usergroupid )
					on rr.entity = 'screens' and rr.recordid = s.screenid and (rr.permission & 1) <> 0
				<cfif organisationcountryid is not "" or countryid is not "">
					left join
				countryGroup cg on s.scope = cg.countryGroupid
				</cfif>

			Where s.ScreenID <>0
			<!--- WAB 2008/07/08 this is a special hack to enable smooth release of new entity navigation
			The new nav has some new screen in the range 70 to 130
			includeParentTypes will only be set when this function is called from the new nav

			2008/10/27 turned out that this was a problem when this function was called from the defaultScreens.cfm page.
			This needs to bring back all the screens but doesn't set includeParentTypes
			 --->
			<!--- WAB 2008/11/10 T-10 Bug Issue 857 - now removed - remove this if statment for everyone on new 3 pane view
			<cfif not includeParentTypes   >
			and s.screenid not between 70 and 130
			</cfif>
			 --->
			and s.entityTypeID  in ( <cf_queryparam value="#EntityTypeIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and isNull(s.suite,0) = 1
			and s.active = 1
			and	(sot.screenid is null or sot.organisationTypeiD =  <cf_queryparam value="#val(organisationTypeID)#" CFSQLTYPE="CF_SQL_INTEGER" > )
			and (isnull(s.securitytask,'') in  ('','datatask') or <!--- WAB 2007/28/11 moving towards dropping reliance on security task - datatask has become the defacto default for all users so --->
					s.securitytask in (
							select distinct s.shortname
									from rights r
									join securitytype s on s.securitytypeid = r.securitytypeid
									where r.usergroupid in (
										<cfif usergroupid is  "">
											select distinct r.usergroupid
												from rightsgroup g
												join rights r on r.usergroupid = g.usergroupid
												where g.personid = #personid#
										<cfelse>
											#usergroupid#
										</cfif>

										)
							)
					 )

			<cfif usergroupid is "">
				and (rr.recordid is null or g.personid = #personid#)
			<cfelse>
				and (rr.recordid is null or g.usergroupid =  <cf_queryparam value="#usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfif>

			<cfif countryid is not "">
				and (s.entityTypeID not in  (1,0) or s.scope = 0 or cg.countryMemberID =  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfif>
			<cfif organisationcountryid is not "">
				and (s.entityTypeID not in  (2) or s.scope = 0 or cg.countryMemberID =  <cf_queryparam value="#organisationcountryid#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfif>


			ORDER by #arguments.sortOrder#
		</cfquery>


		<cfloop query="getScreenList">
			<cfif not structKeyExists(translatedGroup, getScreenList.entityType[currentRow])>
				<cfset translatedGroup[getScreenList.entityType[currentRow]] = getScreenList.GRP[currentRow]>
				<cfif application.com.relayTranslations.doesPhraseTextIDExist('Screens_' & getScreenList.entityType[currentRow])>
					<cfset translatedGroup[getScreenList.entityType[currentRow]] = application.com.relayTranslations.translatePhrase(phrase='Screens_' & getScreenList.entityType[currentRow])>
				</cfif>
			</cfif>
			<cfset querySetCell(getScreenList,"GRP",translatedGroup[getScreenList.entityType[currentRow]],currentRow)>
		</cfloop>

		<cfreturn getScreenList>

	</cffunction>

	<cffunction access="public" name="orderScreenList" hint="Select query of queries, ordering the original query alphabetically" returntype="query">

		<cfargument name="thisQuery" type="query" required="yes">

		<cfscript>
			var getScreenListOrdered = "";
		</cfscript>


		<cfquery name="getScreenListOrdered" dbtype="query">
			SELECT *
			FROM thisQuery
			ORDER by ScreenName
		</cfquery>


		<cfreturn getScreenListOrdered>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       SWJ: 2005-08-23 Added this function copied from screens/screen.cfm
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getScreen" access="public" returntype="query" hint="Returns the screen record as a query object for the screenID/screenTextID provided in CurrentScreenId.">
		<cfargument name="CurrentScreenId" type="string" required="yes">

		<cfscript>
			var getScreen = "";
		</cfscript>

		<CFQUERY NAME="getScreen" datasource="#application.siteDataSource#">
			SELECT * from screens
			where
			<CFIF isnumeric(CurrentScreenId)>
			screenid =  <cf_queryparam value="#arguments.CurrentScreenId#" CFSQLTYPE="CF_SQL_INTEGER" >
			<CFELSE>
			screentextid =  <cf_queryparam value="#arguments.CurrentScreenId#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		</cfquery>

		<cfreturn getScreen>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       SSS: 2009-03-09 added this function so that it van be used directly
	   instead of useing getscreen and then having to check the recordcount.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="doesScreenExist" access="public" returntype="boolean" hint="True or false for the existence of a screen">
		<cfargument name="CurrentScreenId" type="string" required="yes">

		<cfset var ScreenExist = getScreen(CurrentScreenId = arguments.CurrentScreenId)>

		<cfreturn iif(ScreenExist.recordcount is 0,false,true)>
	</cffunction>

	<!--- WAB 2016-11 FormRenderer add support for screenVersion 2 --->
	<cffunction name="getScreenAndDefinition" access="public" returntype="struct" hint="">
		<cfargument name="ScreenId" type="string" required="yes">
		<cfargument name="countryId" type="numeric" required="yes">
		<cfargument name="method" type="string" default="view">
		<cfargument name="readonlyfields" default="">   <!--- parameter can be passed eg: ='person_email','location_fax' --->
		<cfargument name="ForbiddenFields" default = "">
		<cfargument name="readonlywithhiddenfields" default="">
		<cfargument name="screenVersion" default="1">


			<cfset var result = structNew()>
			<cfset var getScreen = "">
			<cfset var getscreenDefinition = "">

			<CFQUERY NAME="getScreen" DATASOURCE="#application.sitedatasource#">
				SELECT 
					* ,
					case when viewEdit = 'view' then '1' when viewEdit in ( 'edit','') then '0' else viewEdit end as readOnly 
				from 
					screens
				where
					<CFIF not isnumeric (screenid)>
						screentextid =  <cf_queryparam value="#ScreenId#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<CFELSE>
						screenid =  <cf_queryparam value="#ScreenId#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFIF>
			</CFQUERY>

			<CFIF getScreen.RecordCount IS 0>
					<cfoutput>incorrect screen id</cfoutput>
				<CFSET result.ScreenId = 0>
			<CFELSE>
				<CFSET result.ScreenId = getScreen.screenid>
			</CFIF>

			<cfset result.screen = getScreen>

			<cfif getScreen.viewEdit is "view" and arguments.method is not "view">  <!--- WAB 2007-06-12 added - if screen is  view, then all items in it must be 	in view mode--->
				<cfset arguments.method = "view">
			</cfif>

			<!--- query now updated so that can find region specific items --->
			<!--- country specific items override region specific ones at the same sort position --->
			<!--- note that if a country is in two regions and lines are specified for each of these regions, then each will appear on screen--->

			<CFSET arguments.readonlyfields = listappend(arguments.readonlyfields,"person_lastUpdated,person_lastUpdatedby,person_created,person_lastUpdated,location_lastUpdatedby,location_created")>

			<!--- 	WAB 2015-02-25 Altered this query to use new getScreenItems function
					This uses a CTE to recurse throught the screendefinition to pick up screens within screen
					Means that I don't have to maintain the screenIndex table which was used previously to deal with multiple levels of screen within screen
			--->
			<CFQUERY NAME="getScreenDefinition" DATASOURcE="#application.SiteDataSource#">
			select
				itemid,
				CountryID ,
				<cfif screenVersion is 1>
					FieldSource,
					FieldLabel,
					EvalString ,
					CASE WHEN fieldsource+'_'+fieldtextid in (<cf_queryparam value="#arguments.readOnlyWithHiddenFields#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">) THEN 'viewwithhidden' WHEN '#arguments.method#' = 'view' or fieldsource+'_'+fieldtextid in (<cf_queryparam value="#readOnlyFields#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">) or fieldsource+'_all' in (<cf_queryparam value="#readOnlyFields#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">) THEN 'view' ELSE method END AS method,
					BreakLn,
				<cfelse>
					<!--- Items added for 'screensV2' --->
					case when fg.flagGroupID is not null then fg.entityTable when f.flagID is not null then f.entityTable when fieldSource = '##addressTable##' then 'location' else FieldSource end as fieldSource,
					case when FieldLabel <> ''  then FieldLabel when fg.flagGroupID is not null then isNull(fg.namePhraseTextID, fg.flagGroupTextID <!--- TBD need to add name to view --->) when f.flagID is not null then flag <!--- TBD  need translation ---> else '' end as Label,
					EvalString2 as evalString ,
					readOnly,
				</cfif>
				FieldTextID,
				TranslateLabel ,
				AllColumns,
				Maxlength,
				Size   ,
				specialformatting,
				UseValidValues ,
				lookup ,
				jsVerify,
				required,
				thisLineCondition
				

			from
				dbo.getScreenItems (<cf_queryparam value="#result.screenid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >) as x

				<cfif screenVersion is 2>
					left join 
				vflagGroup fg on fieldSource = 'flagGroup' and (fg.flagGroupTextID = fieldTextID OR convert(varchar,fg.flagGroupID) = fieldTextID) and fg.flagTypeID in (2,3)
					left join 
				vflagdef f on fieldSource = 'flag' and (f.flagTextID = fieldTextID OR convert(varchar,f.flagID) = fieldTextID) 
				</cfif>
			where
				fieldsource+'_'+isNull(fieldtextid,'') not in (<cf_queryparam value="#arguments.forbiddenFields#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			order by sortIndex
					</CFQUERY>

					<cfset result.Definition = getScreenDefinition>

		<cfreturn result>

	</cffunction>


<!--- Functions for creating screen definitions in memory without having to have the screen set up in the screens table
		very useful for default screens and for displaying occasional screens of flags
		first function creates a single row screen definition, second function used for adding another row
--->

<cffunction name="createScreenDefinition" access="public" returntype="query" hint="">
	<cfargument name="fieldsource" type="string" required="yes">
	<cfargument name="fieldtextID" type="string" required="yes">
	<cfargument name="evalString" type="string" default="#fieldSource#.#fieldtextid#">
	<cfargument name="fieldlabel" type="string" default="">
	<cfargument name="translatelabel" type="numeric" default="0">
	<cfargument name="Condition" type="string" default="">
	<cfargument name="allcolumns" type="numeric" default="0">
	<cfargument name="breakln" type="numeric" default="1">
	<cfargument name="maxLength" type="string" default = "">
	<cfargument name="size" type="string" default = "">
	<cfargument name="trClass" type="string" default="">
	<cfargument name="tdClass" type="string" default="">

	<cfargument name="UseValidValues" type="numeric" default="0">
	<cfargument name="lookup" type="numeric" default="0">
	<cfargument name="jsverify" type="string" default="">
	<cfargument name="required" type="numeric" default=0>
	<cfargument name="specialFormatting" type="string" default="">
	<cfargument name="method" type="string" default="edit">

	<cfargument name="screenDefinition" type="query" required="false">


	<cfquery name="local.generateScreenItem" datasource="#application.siteDataSource#">
	select
		0 as itemid,
		'#fieldsource#' as fieldsource,
		'#fieldtextid#' as fieldtextid,
		'#fieldlabel#' as fieldlabel,
		#translatelabel# as translatelabel,

		'#Condition#' as thisLineCondition,
		'#evalstring#' as evalstring,
		'#method#' as method, <!--- WAB 2007-02-26 changed from thisLineMethod. thisLineMethod becomes a variable set at top of showScreentable --->
		#required# as required,
		#AllColumns# as allColumns,
		#BreakLn# as breakln,
		'#maxlength#' as maxlength,
		'#size#' as Size   ,
		'#specialFormatting#' as specialformatting,
		#UseValidValues# as UseValidValues ,
		#lookup# as lookup ,
		'#trclass#' as TRClass ,
		'#tdclass#' TDClass ,
		'#jsVerify#' as jsverify,
		0 as isCollapsable,
		0 as isVisible,
		<cfif isDefined("screenDefinition")>
		#screenDefinition.recordCount# + 1 as sortOrder
		<cfelse>
		1 as SortOrder
		</cfif>
	</cfquery>

	<cfif isDefined("screenDefinition")>
		<cfquery name="local.generateScreenItem" dbtype="query">
		select * from screenDefinition
			union
		select * from local.generateScreenItem
		order by sortorder
		</cfquery>
	</cfif>

	<cfreturn local.generateScreenItem>
</cffunction>

<cffunction name="getScreensContainingFlag" access="public" returntype="query">
	<cfargument name="flagID" type="numeric" required="true">

	<cfset var getScreenDef= "">
	<cfquery name="getScreenDef" DATASOURCE="#application.SiteDataSource#">
		select distinct s.screenName, s.screenid from screenDefinition sd
			INNER JOIN Screens s on s.screenid = sd.screenid
			INNER JOIN flag f ON fieldSource = 'flag' and (f.flagTextID = sd.fieldTextID or convert(varchar,f.flagID) = sd.fieldTextID)
			where f.FlagID = #flagID#
	</cfquery>

	<cfreturn getScreenDef>
</cffunction>

<cffunction name="getScreensContainingFlagGroup" access="public" returntype="query">

	<cfargument name="flagGroupID" type="numeric" required="true">

	<cfset var getScreenDef= "">
	<cfquery name="getScreenDef" DATASOURCE="#application.SiteDataSource#">
		select distinct s.screenName, s.screenid from screenDefinition sd
			INNER JOIN Screens s on s.screenid = sd.screenid
			INNER JOIN flagGroup fg ON fieldSource = 'flagGroup' and (fg.flagGroupTextID = sd.fieldTextID or convert(varchar,fg.flagGroupID) = sd.fieldTextID)
			where FlagGroupID = #FlagGroupID#
	</cfquery>

	<cfreturn getScreenDef>

</cffunction>

<!--- WAb 2008/03/03 added this extra function --->
<cffunction name="getScreensContainingFlagsInFlagGroup" access="public" returntype="query">

	<cfargument name="flagGroupID" type="numeric" required="true">

	<cfset var getScreenDef= "">
	<cfquery name="getScreenDef" DATASOURCE="#application.SiteDataSource#">
		select distinct s.screenName, s.screenid, f.name as flagName from screenDefinition sd
			INNER JOIN Screens s on s.screenid = sd.screenid
			INNER JOIN flag f ON fieldSource = 'flag' and (f.flagTextID = sd.fieldTextID or convert(varchar,f.flagID) = sd.fieldTextID)
			INNER JOIN flaggroup fg on  fg.flaggroupid = f.flagGroupID
			where fg.FlagGroupID = #FlagGroupID#
			order by f.name
	</cfquery>

	<cfreturn getScreenDef>

</cffunction>

<cffunction name="AddRowToScreenDefinition" access="public" returntype="query" hint="">
	<cfargument name="screenDefinition" type="query" required="true">

	<cfreturn createScreenDefinition (argumentCollection = arguments)>

</cffunction>

<!--- function to determine how many days since a person has hit the save button on a screen recently

	NOTE:
	This function looks for a person editing THEIR OWN record (or their own location/organisation record)
	- doesn't count editing an other person's record (as an internal user or an External Business Admin might do)

	WAB 2007-03-14 reworked this query and made the function HasPersonAccessedScreenInLastXDays call this function

--->
<cffunction name="NumberofDaysSincePersonAccessedScreen" access="public" returntype="boolean" hint="">
	<cfargument name="screenID" type="string" required="true">
	<cfargument name="personID" type="numeric" required="true">

	<cfset var getScreenUsage = "">

	<cfquery name="getScreenUsage" datasource="#application.siteDataSource#">
	select DateDiff(day, max(st.created), getdate()) as numofdays
	from
		screenTracking st
			inner join
		screens s on s.screenid = st.screenid
			inner join
		person p
			on p.personid = st.personid
	where
		<cfif isNumeric(screenid)>
			s.screenid = <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelse>
			s.screentextid = <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		and st.personid = #personid#
		and
			(
				(st.entitytype = 'person' and entityId = p.personid)
					or
				(st.entitytype = 'location' and entityId = p.locationid)
					or
				(st.entitytype = 'organisation' and entityId = p.organisationid)
			)
	</cfquery>

	<cfif getScreenUsage.numofDays eq "">
		<cfreturn "999">
	<cfelse>
		<cfreturn getScreenUsage.numofdays>
	</cfif>

</cffunction>

<!--- function to determine whether a person has hit the save button on a screen recently --->
<cffunction name="HasPersonAccessedScreenInLastXDays" access="public" returntype="boolean" hint="">
	<cfargument name="screenID" type="string" required="true">
	<cfargument name="personID" type="numeric" required="true">
	<cfargument name="days" type="numeric" default=999>

	<cfset var LastAccessAgo = NumberofDaysSincePersonAccessedScreen(screenid = screenid, personid = personid)>

	<cfif LastAccessAgo is "" or LastAccessAgo gt days>
		<cfreturn false>
	<cfelse>
		<cfreturn true>
	</cfif>


</cffunction>


<!--- function to determine whether a person has hit the save button on a screen recently --->
<cffunction name="HasPersonFromThisOrganisationAccessedScreenInLastXDays" access="public" returntype="boolean" hint="">
	<cfargument name="screenID" type="string" required="true">
	<cfargument name="organisationID" type="numeric" required="true">
	<cfargument name="days" type="numeric" default=999>

	<cfset var getScreenUsage = "">

	<cfquery name="getScreenUsage" datasource="#application.siteDataSource#">
	select top 1 *
	from
		screens s
			inner join
		screenTracking st
			on s.screenid = st.screenid
			inner join
		person p on p.personid = st.personid
	where
		<cfif isNumeric(screenid)>
			s.screenid = <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelse>
			s.screentextid = <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		and p.organisationid = #organisationid#
		and st.created > getdate()- #days#
	</cfquery>

	<cfreturn getScreenUsage.recordcount>  <!--- will be 0 or 1 (true or false) --->

</cffunction>


<cffunction name="loadAddressFormatStruct" access="public" returntype="string" hint="">
	<cfargument name="countryID" type="string" required="true">
		<!---
		WAB 2007-04-12
		originally in screen\qryGet AddressFormat.cfm
		not used much (if at all) but I moved it here.
		used to take lots of time to load up all formats at application start up, but better to do it as and when required and call function evaluateAddressFormat

		 --->

		<!---
		Reads the address format for a given countryid
			produces a string (AddressFormat[countryid])which can be evaluated to give an address with commas between lines and blanks removed
			requires:  countryid
			returns  addressFormat[countryid]
			query which gets location data must be called location

			--->

		<!---  Mods
		WAB 2001-02-05	added CFPARAM bit in
		WAB 260199  Fixed problem with addresses with # in it.
		WAB 081100  Modified so that it will work with the addresses defined in terms of fieldSource = #addressTable#
		 --->


		<CFSET var screenid = "standardaddressformat">
		<CFSET var screenqueryname = "getAddressFormat">
		<cfset var getAddressFormat = application.com.screens.getScreenAndDefinition(
					screenid = screenid,
					countryid = countryid).definition>


		<CFSET var addressFormat= '"'>
		<CFSET var addressTable = "location">
		<CFSET var thisSeparator = "">  <!--- this is the separator from the previous line.  There is an assumption that address1 is not blank  --->
		<CFSET var fieldsDone = ''>
<!---  WAB 2008-04-30 mod to prevent multiple instances of same item appearing (which can happen if there are odd conditions on lines), added fields done variable"--->

		<CFLOOP query = "getAddressFormat">

			<CFIF fieldSource is "CFPARAM">

				<CF_evaluateNameValuePair nameValuePairs="#fieldTextID#" mode="param">

			<CFELSEIF fieldSource is "##addressTable##" and not listfindNoCase(fieldsDone,fieldTextID)>  <!--- bit of a hack to get rid of any superfulous stuff --->

				<cfset fieldsdone = listappend(fieldsDone,fieldTextiD)>
				<CFSET var thisEvalString = EvalString>
				<CFIF refind ("##[[:graph:]]*##",thisEvalString) is not 0>
					  <CFSET thisEvalString = evaluate("'#evalString#'")>
				</CFIF>

				<CFIF fieldTextID is "countryid">
					<CFSET thisEvalString = "countryName">
				</cfif>

				<!--- quite a lot of stuff here - including escaping single #s so that the evaluate does not fall over --->
				<CFSET addressFormat  = addressFormat &  "##iif(trim(#thisEvalString#) is not '', DE('#thisseparator# ##replace(#thisEvalString#,'####','########','ALL')##'), DE(''))##" >

				<CFSET thisSeparator = "##separator##">
				<cfif breakLn is 1>  <!--- this is keepWithNext (OK seems backwards) --->
					<cfset thisSeparator = "">
				</cfif>

			</cfif>

		</CFLOOP>

		<CFSET addressFormat = addressFormat & '"' >

		<CFIF not isdefined("application.addressFormat")>
			<CFSET application.addressFormat = structNew()>
		</CFIF>

		<cfset application.addressFormat[countryid] = addressFormat>

		<cfreturn addressFormat>
</cffunction>

<!---
evaluateAddressFormat

used to ouput an address in a country specific format as defined in the standard addressformat screen

eg:

application.com.screens.evaluateAddressFormat(location = locationQuery, separator = "<BR>")

argument
countryid			: defaults to location.countryid but can be passed
separator 			:  display on either one line (,) or multiple lines (<BR>) or whatever you want
showCountry    		: true/false
finalCharacter		: eg "." or ""

WAB 2013-07-03 Fix so that will work with location as either a query or a structure
 --->

<cffunction name="evaluateAddressFormat" access="public" returntype="string" hint="" output="false">
	<cfargument name="location"  required="true">
	<cfargument name="row" type="numeric" default="1"	hint ="row in location query">
	<cfargument name="countryid" type="numeric" >
	<cfargument name="showCountry" default="true"	>
	<cfargument name="separator" default=",">
	<cfargument name="finalCharacter" default=".">

	<cfset var result = "">
	<cfset var countryName = "">

	<cfif not structKeyExists (arguments,"countryid")>
		<cfif isQuery(Location)>
			<cfset arguments.countryid = location.countryid[row]>
		<cfelse>
			<cfset arguments.countryid = location.countryid>
		</cfif>
	</cfif>

	<cfif not isdefined("application.addressFormat") or not structKeyExists(application.addressFormat,countryid)>
		<cfset loadAddressFormatStruct(countryid = countryid)>
	</cfif>

	<cfif showCountry>
		<cfset countryName = application.CountryName[countryid]>
	</cfif>

	<cfif isQuery(Location)>
		<cfoutput query="location" startrow=#row# maxrows=1>
			<cfset result = evaluate(application.addressFormat[Countryid]) & finalCharacter>
		</cfoutput>
	<cfelse>
		<cfset result = evaluate(application.addressFormat[Countryid]) & finalCharacter>
	</cfif>

	<cfreturn result>

</cffunction>

<cffunction name="getScreenForOrgType" access="public" returntype="string" displayname="Get a Organisation Type Specific Screen" description="Tries to find an organisation type specifc screen for a given screentextID. If one cannot be found it then checks if the screentextID can also be found. Returns a  structure with exists(true,false), resultset(Screens table records)">

	<cfargument name="screentextID" type="string" required="true">
	<cfargument name="organisationID" type="string" required="false">
	<cfargument name="organisationtypeID" type="string" required="false">

	<cfscript>
		var qry_get_organisationType = "";
		var orgTypeScreenTextID = "";
		var tempScreenTextID = "";
		var qry_get_ScreenOrganisationTypes = "";
		var sotValueList = "";
		var qry_get_Screen = "";
		var qry_get_OrgTypeScreen = "";
	</cfscript>

	<!--- if an organisationtype is passed get the typetextID --->
	<cfif arguments.organisationtypeID neq "">

		<cfquery name="qry_get_organisationType" datasource="#application.siteDataSource#">
			select ot.typeTextID, ot.organisationtypeID from organisationType ot
			where ot.organisationTypeID =  <cf_queryparam value="#arguments.organisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	<!--- if an organisation is passed get its organisationtype typetextID --->
	<cfelseif arguments.organisationID neq "">

		<cfquery name="qry_get_organisationType" datasource="#application.siteDataSource#">
			select ot.typeTextID,ot.organisationTypeID from organisationType ot
			INNER JOIN organisation o on ot.organisationTypeID = o.organisationTypeID
			where o.organisationID =  <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cfif>
	<!--- if an organisation type is found --->

	<cfif qry_get_organisationType.recordcount neq 0>

		<!--- set a temporary name for the screentextid --->
		<cfset tempScreenTextID = "#arguments.screentextID#OrgType#qry_get_organisationType.typeTextID#">
		<!--- see if tempScreenTextID exists --->
		<cfquery name="qry_get_OrgTypeScreen" datasource="#application.siteDataSource#" maxrows="1">
			select *, '' as organisationTypes from Screens
			where screentextid =  <cf_queryparam value="#tempScreenTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<!--- if a screen exists --->
		<cfif qry_get_OrgTypeScreen.recordcount neq 0>
			<cfoutput query="qry_get_OrgTypeScreen">
				<!--- get the screens organisation types --->
				<cfquery name="qry_get_ScreenOrganisationTypes" datasource="#application.siteDataSource#">
					select organisationTypeID from screenOrganisationType
					where screenid =  <cf_queryparam value="#qry_get_OrgTypeScreen.screenID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<!--- get the value list of organisation types --->
				<cfset sotValueList = ValueList(qry_get_ScreenOrganisationTypes.organisationTypeID)>
				<!--- update the query column --->
				<cfset QuerySetCell(qry_get_OrgTypeScreen, "organisationTypes", sotValueList, qry_get_OrgTypeScreen.currentrow)>

			</cfoutput>
			<!--- if the screen has no organisation types assigned or the organisationtype has access to the screen --->
			<cfif (qry_get_OrgTypeScreen.organisationTypes is "") or ( listfind(qry_get_OrgTypeScreen.organisationTypes,"#qry_get_organisationType.organisationTypeID#") neq 0)>
				<!--- update the orgTypeScreenTextID to return --->
				<cfset orgTypeScreenTextID = tempScreenTextID>
			</cfif>
		</cfif>
	</cfif>

	<cfif orgTypeScreenTextID is "">

		<cfset tempScreenTextID = "#arguments.screentextID#">
		<!--- see if tempScreenTextID exists --->
		<cfquery name="qry_get_Screen" datasource="#application.siteDataSource#" maxrows="1">
			select *, '' as organisationTypes from Screens
			where screentextid =  <cf_queryparam value="#tempScreenTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<!--- if a screen exists --->
		<cfif qry_get_Screen.recordcount neq 0>
			<cfoutput query="qry_get_Screen">
				<!--- get the screens organisation types --->
				<cfquery name="qry_get_ScreenOrganisationTypes" datasource="#application.siteDataSource#">
					select organisationTypeID from screenOrganisationType
					where screenid =  <cf_queryparam value="#qry_get_Screen.screenID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<!--- get the value list of organisation types --->
				<cfset sotValueList = ValueList(qry_get_ScreenOrganisationTypes.organisationTypeID)>
				<!--- update the query column --->
				<cfset QuerySetCell(qry_get_Screen, "organisationTypes", sotValueList, qry_get_Screen.currentrow)>
				<!--- if the screen has no organisation types assigned or the organisationtype has access to the screen --->
				<cfif listlen(sotValueList) eq 0>
					<cfset orgTypeScreenTextID = tempScreenTextID>
				<cfelseif listfind(qry_get_Screen.organisationTypes,"#qry_get_organisationType.organisationTypeID#") neq 0>
					<cfset orgTypeScreenTextID = tempScreenTextID>
				</cfif>
			</cfoutput>

		</cfif>

	</cfif>

	<cfreturn orgTypeScreenTextID>

</cffunction>


	<!--- WAB 2007/11/26

	The default screens that a user sees can be specified at a system wide or  usergroup or personal usergroup level
	The values are stored in the preferences table in a hive called dataScreens
	The keys are of the form
		defaultscreen.Person.0	= "PersonDetail"
		defaultscreen.Location.1	= "ListLocations"
	The 0 and 1 above are the OrganisationTypeID (0 for any)

	I have crafted a query which works out what to show to a given user

	NJH 2015/12/02	changed from inner join to usergroup to left join as entityId 0 was to mean all usergroups
	--->
	<cffunction name="getDefaultScreen" access="public">
		<cfargument name="entityType">
		<cfargument name="organisationTypeID">
		<cfargument name="UserGroupID" default = "#request.relaycurrentuser.userGroupID#">
		<cfargument name="UserGroups" default = "#request.relaycurrentuser.userGroups#">

			<cfset var result = "">
			<cfset var getbestmatchScreen = "">

					<cfquery name="getbestmatchScreen"  datasource = "#application.sitedatasource#">
					select  top 1 p.entityid as usergroupid, subkey, data as screenid,
						case when p.entityid = 0 then 'All Users' else ug.name end as usergroupname,
						organisationtypeid,
						case when organisationtypeid is null then 'Any' else typetextid end as OrganisationType,
						 screenname
					from
						preferences p
							left join
						usergroup ug on ug.usergroupid = p.entityid
							left join
						organisationtype ot on convert(int,substring (reverse(subkey),1,charindex('.',reverse(subkey))-1)) = ot.organisationtypeid
							left join
						screens s on s.screenid = convert(int,data)

					where
							p.entitytypeid =  <cf_queryparam value="#application.entityTypeID.usergroup#" CFSQLTYPE="CF_SQL_INTEGER" >
						and hive ='datascreens'
						and (subkey =  <cf_queryparam value=".defaultscreen.#entityType#.0" CFSQLTYPE="CF_SQL_VARCHAR" >  <cfif structKeyExists(arguments,"organisationTypeID")> or subkey =  <cf_queryparam value=".defaultscreen.#entityType#.#organisationtypeid#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>)
						and p.entityID in (0,#UserGroupID#, #UserGroups#)

					order by
						case
							when p.entityid  in ( <cf_queryparam value="#UserGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) then 1
							when p.entityid  in ( <cf_queryparam value="#usergroups#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) then 2
							when p.entityid = 0 then 3
						 end ,
						subkey desc  -- this will get the organisationtype specific one ahead of the non specific one
					</cfquery>

		<cfreturn getbestmatchScreen>

	</cffunction>

	<!--- 2014-08-11			RPW Create new profile type: "Financial" --->
	<cffunction name="getCurrencies" access="public" returntype="query" output="false">
		<cfargument name="currencyISOCode" type="string" required="no">

		<cfquery name="local.qGetCurrencies" datasource="#application.sitedatasource#">
			SELECT
				currencyISOCode,
				currencyName,
				currencySign,
				currencyMso
			FROM Currency
			<cfif IsDefined("arguments.currencyISOCode")>
				WHERE currencyISOCode = <cf_queryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.currencyISOCode#" maxlength="3">
			</cfif>
			ORDER BY currencyName
		</cfquery>

		<cfreturn qGetCurrencies>
	</cffunction>

</cfcomponent>

