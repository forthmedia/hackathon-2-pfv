<!--- �Relayware. All Rights Reserved 2014 --->


<cfcomponent displayname="Common Report Queries" hint="Contains a number of commonly used Report Queries">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns reports for this module.                
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!--- 2012-02-03		WAB		Sprint Menu Changes.  Rationalise menu XML code.  Access menus through getMenuXMLDoc().  Client extensions dealt with automatically. --->
	<cffunction name="getReportsInModule" access="public" hint="Returns a query of reports within the menu XML structure that the user has access to">
		<cfargument name="module" required="yes" type="string">
		<cfset myReports = "">
		
		<cfset reportsInModule = XmlSearch(application.com.relayMenu.getMenuXMLDoc(),"/MenuData/MenuItem[@Module='#arguments.module#']/MenuAction[contains(@TabText,'Reports')]/SubMenuAction/")>
		<cfset relayMenuXML = StructNew()>
		
		<cfset size = ArrayLen(reportsInModule)>
		
		<cfset reportQuery = QueryNew("Name,URL,Grouping")>
		<cfif size gt 0>
		<cfset temp = QueryAddRow(reportQuery,#size#)>
		<cfloop index="reportIdx" from="1" to="#size#">
				<!--- WAB 2007-04-11 needed a way of not showing some reports on particular sites so added the concept of a Condition (not required)
					ideally we would need to be able to read the ini file for a particular group, but for now will have to use existing variables or request variables 
				--->
				<cfset showItem = true>
				<cfif structKeyExists (reportsInModule[reportIdx].XmlAttributes, "condition")>
					<cftry>
						<cfset showItem = evaluate(reportsInModule[reportIdx].XmlAttributes.condition)>
						<cfcatch>
							<cfset showItem =false><cfoutput> #reportsInModule[reportIdx].XmlAttributes.Name#</cfoutput>
						</cfcatch>
					</cftry>
				</cfif>

				<cfif showItem>
			<!--- NJH 2006/07/07 Do we have rights to see this reports? --->
					<cfset reportSecurityLevel = "reportTask:Level1">
					<cfif structKeyExists(reportsInModule[reportIdx].XmlAttributes,"Security")>
			<cfset reportSecurityLevel = reportsInModule[reportIdx].XmlAttributes.Security>
					</cfif>
			<cfif application.com.login.checkInternalPermissions(listfirst(reportSecurityLevel,":"),listlast(reportSecurityLevel,":"))>
				<cfset temp = QuerySetCell(reportQuery,"Name",#reportsInModule[reportIdx].XmlAttributes.Name#,#reportIdx#)>
				<cfset temp = QuerySetCell(reportQuery,"URL",#reportsInModule[reportIdx].XmlAttributes.URL#,#reportIdx#)>
				<cfset temp = QuerySetCell(reportQuery,"Grouping",#reportsInModule[reportIdx].XmlAttributes.Group#,#reportIdx#)>
			</cfif>
				</cfif>
		</cfloop>
		</cfif>
		
		<cfquery name="myReports" dbtype="query">
			select name, url, grouping from reportQuery where name is not null
		</cfquery>
		
		<cfreturn myReports>
	</cffunction>
	
	
	<cffunction name="getClientReports" access="public" hint="Returns query of reports from the client specific XML file that the user has access to">
		<cfargument name="module" required="yes" type="string">
		<!--- NYF 2008-06-09 replace 
		<cfset FileLocation=replace("#application.paths.content#\xml\InternalReportListExtension.xml","/","\")>
		with --->
		<cfargument name="FileLocation" required="no" type="string" default="#application.paths.abovecode#\xml\InternalReportListExtension.xml">

		<cfset var getClientReports = "">
		<cfset qryClientReports = QueryNew("Name,URL,Grouping","varchar,varchar,varchar")>

		
		<cfif not fileExists("#arguments.FileLocation#")>
			<cfset AttributesError="No InternalReportListExtension.xml file found; looking for #arguments.FileLocation#">
		</cfif>		
		
		<cftry>
			<cffile action="read"
		 			file="#arguments.FileLocation#"
		 			variable="clientxml">
			<cfcatch type="Any">
				<!--- XML file not found --->
			</cfcatch>
		</cftry> 

		<cfif isDefined("clientxml")>
			<cfset clientxmldoc = XmlParse(clientxml)>
			<cfset clientReports = XmlSearch(clientxmldoc, "//reportGroup[@module='#arguments.module#']/report")>
			<cfset reportXML = StructNew()>
			
			<cfset size = ArrayLen(clientReports)>
			<cfif size gt 0>
				<cfset reportQuery = QueryNew("Name,URL,Grouping")>
				<cfset temp = QueryAddRow(reportQuery,#size#)>
				<cfloop index="reportIdx" from="1" to="#size#">
					<!--- NJH 2006/07/07 Do we have rights to see this reports? --->
					<cfset reportSecurityLevel = "reportTask:Level1">
					<cfif structKeyExists(clientReports[reportIdx].XmlAttributes,"Security")>	
					<cfset reportSecurityLevel = clientReports[reportIdx].XmlAttributes.Security>
					</cfif>
					<cfif application.com.login.checkInternalPermissions(listfirst(reportSecurityLevel,":"),listlast(reportSecurityLevel,":"))>
						<cfset temp = QuerySetCell(reportQuery,"Name",#clientReports[reportIdx].XmlAttributes.Name#,#reportIdx#)>
						<cfset temp = QuerySetCell(reportQuery,"URL",#clientReports[reportIdx].XmlAttributes.URL#,#reportIdx#)>
						<cfset temp = QuerySetCell(reportQuery,"Grouping",#clientReports[reportIdx].XmlAttributes.Group#,#reportIdx#)>
					</cfif>
				</cfloop>
				
				<cfquery name="qryClientReports" dbtype="query">
					select name, url, grouping from reportQuery where name is not null
				</cfquery>
			</cfif>
		</cfif>
			
		<cfreturn qryClientReports>
		
	</cffunction>
	
</cfcomponent>

