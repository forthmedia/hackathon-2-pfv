<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	2004-12-21	JC	Added replaceCheckField trap - checks for the images that are used to
	replace check and radio fields when frmMethod = View
	2008/09/24  WAB altered convertNameValuePairStringToStructure to allow dotnotation also changed regexp to deal with nulls
	WAB 2008/10/07 changes CollectFormFieldsIntoNameValuePairs() during work on P-SOPHSprint5
	2009/01/21	SSS	makeSafeColumnName function added
	2009/03/03 	WAB bug in CollectFormFieldsIntoNameValuePairs leading to a trailing delimiter
	2009/04/27  WAB mod to CollectFormFieldsIntoNameValuePairs so that can pick up parameters where the parameter name is in a field
	2009/09/30   WAB/NJH LHID2706
	2010/02/04   WAB LID    LID 3064 Added function removeExtraCommasFromAList
	2010/05/26 WAB   Add removedNumericFormatting function
	2010/09/22	WAB	Mod to refindAllOccurrences to return group names
	2011-06-21	NYB	P-LEN024 - changed findHTMLTagsInString function to handled additional tags
	2011/11/07  WAB  Major work on convertNameValuePairsToStructure to deal with some special cases
	2011/11/17 	WAB	More work on convertNameValuePairsToStructure.  Now has a function to bring back list of keys in order (after NYB)
	2011/11/22 WAB  altered convertNameValuePairStringToStructure_ to deal with strings with repeated keys (eg x=1&x=2)
	2011/12/02	WAB Altered findHTMLTagsInString to handle any tag
	2011/12/02	WAB Added removeProtocol()
	2011/12/05	WAB Added camelCaseToSpaces
	2012-01-16	WAB Bug fix to convertNameValuePairStringToStructure_ to deal with delimiter of |  (or any other delimiter which has a special regExp meaning)
	2012-01		WAB While doing security upgrade added lots of functions needed for searching our code base
	JavaMatch() - uses java regular expression library to do similar things to reFindNoCase - but seems less likely to crash
	getComments() for finding/replacing comments in a string (dealing with nested comments )
	Functions for deciding whether a position in a string is within a comment
	removeWhiteSpace() - removes white space from code - protecting TEXTAREA and PRE
	2012-02-28	WAB	During CASE:426873  moved some functions into relayTranslations.cfc and got rid of others (related to doctoring phrase text used by wysiwyg editors)
	2012-03-19	WAB	Fixed Problems with parseRelayFunction if using unnamed parameters
	2012-06-22 	PPB Case 429018 add replaceNonAlphaNumericChars
	2013-01-06 	WAB CASE 432644 Alterations to getMatchedOpeningAndClosingTags() to return details of nested tags
	2013-01-06 	WAB CASE 432644 Alterations to how excludeArray is processed by refindAllOccurrences()
	2013-01-29 	WAB for CASE 433406 added unHTMLEditFormat() function
	2013-09-09  WAB fix an edgecase bug in convertNameValuePairStringToStructure_ (wouldn't deal with x&y=2&z=3 )
	2014-01-08	WAB	Fix the regExp for removing SQL comments, added a regExp for removing SQL single line comments
	2014-02-25 	WAB CASE 439005  Fix to CollectFormFieldsIntoStructure()
	2014-05-20 	WAB CASE 440398 added ` to list of allowed 'name' characters
	2014-09-30 	WAB added new type which includes all the characters from ID including a space.
	2014-11-25	WAB (For CASE 442080 - reinstate translation Ts). Added functions replaceMatchedItemsWithPlaceHolders() and reinstateMatchedItems() so could be used by relayTranslations for protecting DONOTTRANSLATE blocks
	2015-11-17	WAB PROD2015-406 SQL Injection Security. Added regExp to support cf_queryObjectName
	2016-04-20 	WAB CASE 449069 fix removeExtraCommasFromList() to deal with more than two successive commas
	2017-03-06 	PPB RT-325 SUSE page formatting - added function escapeNonHTMLChevrons developed by WAB
	--->
	<!---
	ColdFusion Reg Exp Tips
	Best place is http://www.regular-expressions.info/
	(?m) at beginning turns on multi - line mode so that ^ and $  will match at beginning and end of a line
	(?:regexp   )  prevents a group capturing (creating back-references)
	--->
<cfcomponent displayname="RegularExpressionFunctions" hint="various clever things with regular expressions">

	<!--- 2009/09/29 WAB/NJH LID 2706 looking at place to store standard regexps --->
	<cfset this.standardexpressions = structNew()>
	<cfset this.standardexpressions["NameCharacters"]="^([^\x00-\x1F\x21-\x26\x28-\x2c\x2e-\x40\x5b-\x5F\x7B-\xBF]+)$"> <!--- WAB 2014-05-20 CASE 440398 changed x60 to x59, this allows ` (which is a slightly odd character but ' gets converted to it by security.cfc so it does sometimes appear on forms) --->
	<cfset this.standardexpressions["NumericList"]="^([0-9\.]+,)*[0-9\.]+$">  <!--- WAB 2013-09-18 prevents non numeric and double/leading/trailing commas, but does not protect against multiple decimal points in a number [the regExp got too complicated and overflowed]  --->
	<cfset this.standardexpressions["ID"]="\A[0-9a-zA-Z_.,\-]*\Z">  <!--- WAB 2013-10-17 Will match numbers or TextIDs but no rubbish.   Note, never allow brackets.  WAB 2015-11-17 Add - for negative numbers --->
	<cfset this.standardexpressions["AlphaNumericList"]= this.standardexpressions["ID"]>
	<cfset this.standardexpressions["AlphaNumericListWithSpace"]=replace(this.standardexpressions.AlphaNumericList,"]","\s]")>  <!--- same as alphanumericlist but including space --->
	<cfset this.standardexpressions["queryObjectName"]=replace(this.standardexpressions.AlphaNumericListWithSpace,"]*","\]##\[]*")>  <!--- same as alphanumericlistwithspace but allowing square brackets and has --->

	<cffunction name="findAddressInText" access="public" returntype="array" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfset var regexp = "(\s|\A)((?:http://|www\.)[^\s]+)(\Z|\s)">
		<cfset var groupNames = {1="before",2="url",3="after"}>
		<cfreturn refindAllOccurrences(reg_expression= regExp,string = arguments.inputString, groupNames = groupNames)>
	</cffunction>

	<!--- function using regular expressions to convert a url entered by a user in plain text into an HREF
		note, it should ignore existing HREFs as long as they don't have whitespace around the URLs
		NJH 2013/02/19 Sprint 3 Item task 142 - added support for shortenURL
		--->

	<cffunction name="convertAddressToHref"  access="public" hint="" returntype="string" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="target" type="string" default="">
		<cfargument name="class" type="string" required="false">
		<cfargument name="shortenURL" type="boolean" default="false">
		<!--- finds:
			1. a whitespace  or the beginning of the string
			2. http://  or wwww.
			2a. anything which is not whitespace
			3. end of string or a whitespace of some sort
			--->
		<!--- 			<cfset regexp = "(.*?)(http://|www\.)([a-zA-Z0-9_%\-\+\?&=\.\\/]+)(\Z|\s.*)"> --->
		<!--- 			<cfset regexp = "(.*\s|\A)(http://|www\.)([^\s]+)(\Z|\s.*)"> --->
		<!--- <cfset var regexp = "(\s|\A)((?:http://|www\.)[^\s]+)(\Z|\s)"> --->
		<cfset var targetString = "">
		<cfset var classString = "">
		<cfset var result = arguments.inputString>
		<!--- <cfset var groupNames = {1="before",2="url",3="after"}> --->
		<cfset var theURL = "">
		<cfset var newURL = "">
		<cfset var findResult = "">
		<cfset var oldUrl = "">
		<cfset var urlResult = "">
		<!---
			replaces with
			backreference 1 the whitespace character
			+   <A HREF='
			+	back reference 2 and 3 (the url)
			+ 	'>
			+ 	back reference 2 and 3 (the url) again
			+	</A>
			+ 	back reference 4 the whitespace character
			--->
		<!--- <cfset findResult = refindAllOccurrences(reg_expression= regExp,string = inputString, groupNames = groupNames)> --->
		<cfset findResult = findAddressInText(inputString=arguments.inputString)>
		<cfif arguments.target neq "">
		<cfset targetString = " target='#arguments.target#'">
		</cfif>
		<cfif structKeyExists(arguments,"class") and arguments.class neq "">
		<cfset classString = " class='#arguments.class#'">
		</cfif>
		<cfloop array="#findResult#" index="urlResult">
			<cfset theURL = urlResult.url>
			<cfset oldUrl = theURL>
			<cfif arguments.shortenURL>
			<cfset theURL = application.com.social.shortenURL(url=theURL).url>
			</cfif>
			<cfset newURL = '<A HREF = "#theURL#" #targetString##classString#>#theURL#</A>'>
			<cfset result = replace(arguments.inputString,oldUrl,newURL,"ALL")>
		</cfloop>
		<cfreturn result>
	</cffunction>


	<cffunction name="parseAnchorTag"  access="public" hint="" returntype="struct" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfset var result = structNew()>
		<cfset var regexp = "<A(.*)>(.*)</A>">
		<cfset var x = refindNoCase(regExp,inputString,1,true)>
		<cfif x.pos[1] is not 0>
		<cfset result.params = mid(inputString,x.pos[2],x.len[2])>
		<cfset result.text = mid(inputString,x.pos[3],x.len[3])>
		<cfset result.params = convertNameValuePairStringToStructure(result.params)>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2008/04/27
		Returns an array of all instances which match a regular expression
		Note that this function uses the same array structure as refind - ie the first group is returned as item 2, second group as item 3.  Sorry!
		WAB 2010/09/21: You can now pass in a structure of names for your groups.  This uses proper indexing ie item 1 refers to the first group
		eg  {1="firstname",2="secondname"}
		WAB 2011/11/16 added support for a start attribute (mainly useful for refindSingleOccurrence)
		WAB 2012-01   Added options for returning lines numbers and for using the java regExp Library
		WAB 2013-01-06 	CASE 432644 Altered how excludeArray is processed
		--->

	<cffunction name="refindAllOccurrences"  access="public" hint="" returntype="array" output="false">
		<cfargument name="reg_expression" type="string" required="true">
		<cfargument name="string" type="string" required="true">
		<cfargument name="groupNames" type="Struct" default="#structNew()#">  <!--- allows the function to return named groups rather than numbered --->
		<cfargument name="start" type="numeric" default="1">
		<cfreturn refindOccurrences (argumentCollection = arguments)>
	</cffunction>


	<cffunction name="refindSingleOccurrence"  access="public" hint="" returntype="array" output="false">
		<cfargument name="reg_expression" type="string" required="true">
		<cfargument name="string" type="string" required="true">
		<cfargument name="groupNames" type="Struct" default="#structNew()#">  <!--- allows the function to return named groups rather than numbered --->
		<cfargument name="start" type="numeric" default="1">
		<cfargument name="useJavaMatch" type="boolean" default="false">
		<cfreturn refindOccurrences (argumentCollection = arguments,singleOccurrence = true)>
	</cffunction>


	<cffunction name="refindOccurrences"  access="public" hint="" returntype="array" output="false">
		<cfargument name="reg_expression" type="string" required="true">
		<cfargument name="string" type="string" required="true">
		<cfargument name="groupNames" type="Struct" default="#structNew()#">  <!--- allows the function to return named groups rather than numbered --->
		<cfargument name="singleOccurrence" type="boolean" default="false">
		<cfargument name="start" type="numeric" default="1">
		<cfargument name="getLineNumbers" type="boolean" default="false">
		<cfargument name="useJavaMatch" type="boolean" default="false">
		<cfargument name="excludeArray" type="array" >  <!--- exclude any matches which lie inside positions given in this array [which is an array of the type returned by refindOccurrences() or more usually getComments() or getMatchedOpenersAndClosers()] --->
		<cfargument name="excludeArrayReplacementCharacter" default=" " > <!--- actually excludeArray just replaces all characters with spaces so that they do not match anything.  In special circumstances you can replace with a different character which can allow you to remove characters which might confuse your regular expression, but still have them appear in the result --->
		<cfset var result = arrayNew(1)>
		<cfset var stringToSearch = arguments.string>
		<cfset var findresult = "">
		<cfset var thisResult = ""> <!---  --->
		<cfset var i = 0>
		<cfif useJavaMatch>
		<cfset result = javaMatch (regexp =reg_expression,string = arguments.string, groupnames= groupnames,getLineNumbers = getLineNumbers,start =start,singleOccurrence = singleOccurrence)>
	<cfelse>
		<cfif structCount (groupNames) is 0>
		<cfset arguments.groupNames = {1=2,2=3,3=4,4=5,5=6,6=7,7=8,8=9,9=10,10=11,11=12,12=13,13=14,14=15,15=16,16=17}> <!--- does limit to 9 groups in a regexp - hope enough --->
		</cfif>
		<cfif structKeyExists(arguments,"excludeArray")>
		<!---
			Replace characters specified by the exclude array with a placeholder of spaces of same length
			We will do the reFind on this string, but use the original string when pulling out the results, in this way we never have to worry about putting the placeholders back
			--->
		<cfloop from="#arraylen(arguments.excludeArray)#" to =1 index="i" step="-1">
		<cfset stringToSearch = removeChars(stringToSearch,arguments.excludeArray[i].position,arguments.excludeArray[i].length)>
		<cfset stringToSearch = insert(repeatString(arguments.excludeArrayReplacementCharacter,arguments.excludeArray[i].length),stringToSearch,arguments.excludeArray[i].position-1)>
		</cfloop>
		</cfif>
		<cfset findresult = refindnocase (reg_expression,stringToSearch,start,true)>
		<cfloop condition = "findresult.pos[1] is not 0">
		<cfset thisResult = structNew()>
		<cfset thisResult.String = mid(arguments.string,findresult.pos[1],findresult.len[1])>
		<cfloop index="i" from="2" to= "#arrayLen(findresult.pos)#">
		<cfif findresult.pos[i] is not 0>
		<cfset thisResult[groupNames[i-1]] = mid(arguments.string,findresult.pos[i],findresult.len[i])>
	<cfelse>
		<cfset thisResult[groupNames[i-1]] = "">
		</cfif>
		</cfloop>
		<cfset thisResult.Position = findresult.pos[1]>
		<cfset thisResult.Length = findresult.len[1]>
		<cfset thisResult.end = thisresult.Position + thisResult.length -1 >
		<cfif getLineNumbers>
		<cfset thisResult.LineNumber = getNumberOfLineBreaks(left(arguments.string,thisResult.Position)) + 1>
		</cfif>
		<cfset arrayAppend(result,thisResult)>
		<cfif singleOccurrence><cfbreak></cfif>
		<cfset findresult = refindnocase (reg_expression,stringToSearch,findresult.pos[1] + findresult.len[1],true)>
		</cfloop>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="removeHiddenField"  access="public" hint="" returntype="string" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="scope" type="string" default="ALL">
		<!--- remove hidden fields  --->
		<cfset var regexp = '<INPUT Type\=\"Hidden\"[^\>]*\>'>
		<cfreturn REreplacenocase(inputString,regexp,"",scope)>
	</cffunction>


	<cffunction name="replaceCheckField" access="public" hint="" returntype="string" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="scope" type="string" default="ALL">
		<!--- replace checkbox with appropriate display --->
		<cfset var regexp = '<IMG SRC\=\"/images/misc/flag-yes.gif\"[^\>]*\>'>
		<cfset arguments.inputString = REreplacenocase(arguments.inputString,regexp,"[X]",scope)>
		<cfset regexp = '<IMG SRC\=\"/images/misc/flag-no.gif\"[^\>]*\>'>
		<cfreturn REreplacenocase(inputString,regexp,"[ ]",scope)>
	</cffunction>

	<!--- WAB 2011/11/17 split code of convertNameValuePairStringToStructure into another function
		which brings back a bit more info - in particular the order of the names
		for backwards compatibility this function just calls the new function and returns the result key
		--->

	<cffunction name="convertNameValuePairStringToStructure" access="public" hint="" returntype="struct" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="delimiter" type="string" default = " ">
		<cfargument name="equalsSign" type="string" default="=">
		<cfargument name="DotNotationToStructure" type="boolean" default = "false">
		<cfargument name="addOrderedNameList" type="boolean" default = "false">
		<cfreturn convertNameValuePairStringToStructure_ (argumentCollection = arguments).result>
	</cffunction>


	<cffunction name="convertNameValuePairStringToStructure_" access="public" hint="" returntype="struct" output="false">
		<!---
			WAB 2006-05-17 added . as a valid character in a variable name  so a variable could be x.y.z
			WAB 2011/02/03 converted to refindalloccurrences
			WAB 2011/11/04 fell over if given a string such as href="12345"onclick="3456"  when no space between the parameters
			WAB 2011/11/16 Did another complete rewrite to try and deal with all the edge cases
			Now works gradually through the string picking things up as it goes, rather than trying to do it all in one go.
			Has the advantage that can probably tailored as edge cases arise
			Altered NYBs technique for returning order of names
			Now returns result within a structure (so that I can also return the list of names), so function renamed and original function calls this one
			WAB 2011/11/22	Added code to deal with duplicate keys like x=1&x=2.  No, by default, appends values
			WAB 2012-01-20  Altered regexp to deal with form   x=1 y z=1
			WAB 2012-02-15  Another edgecase x=1&y=2&z=
			WAB 2016-11-29	PROD2016-2768 Deal with stack overflows on very long strings.  Found an improved regExp which does not use |
			--->
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="delimiter" type="string" default = " ">
		<cfargument name="equalsSign" type="string" default="=">
		<cfargument name="DotNotationToStructure" type="boolean" default = "false">
		<cfargument name="appendDuplicateKeys" type="boolean" default = "#iif(delimiter is '&',true,false)#"> <!--- WAB 2011/11/22 Added this argument.  If there are duplicate keys then the values are appended.  So x=1&x=2  gives result.x = 1,2.   For backwards compatibility defaulted to false except for query strings (delimited by &) where true makes more sense --->
			<!--- takes a string of name value pairs, delimited by spaces (or whatever is defined) and converts to a structure of name value pairs
			can deal with quoted values
			--->
		<cfset var result = {result = structNew(),OrderedNameList = ""}>
		<cfset var delimiterRegExp = escapeRegExp(arguments.delimiter) />
		<cfset var tempdelimiter = delimiterRegExp />
		<cfset var regExp = '' />
		<cfset var findNameAndEquals = '' />
		<cfset var startPos = '' />
		<cfset var regExpToUseForValue = '' />
		<cfset var nameAndEqualsRegExp = '' />
		<cfset var findNameAndEquals_ = '' />
		<cfset var findValue = '' />
		<cfset var value = '' />
		<cfset var name = '' />
		<cfset var continueFrom = '' />
		<cfset var regExps = {}>
		<cfset var counter = 0>
		<cfset var groups = '' />
		<cfset var pointer = '' />
		<cfset var thisKey = '' />

		<cfif inputString is "">
			<cfreturn result>
		</cfif>

		<cfset arguments.inputString = trim(arguments.inputString)>

		<cfif tempdelimiter is " ">
			<cfset tempdelimiter = "">
		</cfif>

		<!--- find variable + whitespace + equals + 1 character --->
		<cfset regExp = "([^#delimiterRegExp##equalsSign#]+)\s*(#equalsSign#|#delimiterRegExp#|\Z)(\s*)(.|\Z)" >
		<cfset groups = {1="name", 2="equalsSign", 3="whitespace",4="possibleQualifier"}>
		<cfset RegExps.NameAndEquals = {regExp = regExp,groups=groups}>

		<!--- find value when double quote qualified --->
		<cfset regExp = '"((?:[^"]*("")?)*)"' >
		<cfset groups = {1="value", 2="2",3="3",4="4"}>
		<cfset RegExps.DoubleQuotedValue = {regExp = regExp,groups=groups,qualifier='"'}>

		<!--- find value when single quote qualified --->
		<cfset regExp = "'((?:[^']*('')?)*)'" >
		<cfset groups = {1="value", 2="2",3="3",4="4"}>
		<cfset RegExps.SingleQuotedValue = {regExp = regExp,groups=groups,qualifier="'"}>

		<!--- find value when not  qualified --->
		<cfif arguments.delimiter is " ">
			<cfset tempdelimiter = "\s">
		</cfif>
		<cfset regExp = "(|([^#tempdelimiter#]*?))(#tempdelimiter#|\Z)" >
		<cfset groups = {1="value", 2="2",3="3",4="4"}>
		<cfset RegExps.UnQualifiedValue = {regExp = regExp,groups=groups,qualifier=""}>

		<!--- special case for null value when no qualifier and space delimiter eg: finding b in  "a=1 b= c=3"
			also deals with a=1&b&c=3  and a=1&b=&c=3
			--->
		<cfset regExp = "(|\Z)" > <!--- WAB 2013-09-09 a bug had crept in here, probably 2012-02-15 when dealing with edgecase x=1&y=2&z=, no longer dealt with  x&y=2&z=3.  Needed to add the | so that it matches on nothing  --->
		<cfset groups = {1="value", 2="2",3="3",4="4"}>
		<cfset RegExps.Null = {regExp = regExp,groups=groups,qualifier=""}>
		<cfset findNameAndEquals = refindsingleoccurrence (reg_expression = regexps.NameAndEquals.regexp,string = inputstring,groupnames=regexps.NameAndEquals.groups)>
		<cfset counter = 0 >

		<cfloop condition="arrayLen(findNameAndEquals)">
			<cfset counter = counter + 1>
			<cfset startPos = findNameAndEquals[1].position + findNameAndEquals[1].length - len(findNameAndEquals[1].possibleQualifier)> <!--- len(findNameAndEquals[1].possibleQualifier) is usually 1, but occasionally (eg when we are on the last item of a list where there is no value eg: "a=1,b=2c="  ) it is 0 --->

			<cfif findNameAndEquals[1].equalsSign is not equalsSign>
				<!--- This is the case of a  "&y&x=1" where y is a variable without any value, similar to "&y=&x=1" dealt with below --->
				<cfset regExpToUseForValue = "null">
				<cfif findNameAndEquals[1].equalsSign is "">  <!--- might actually always be "" in this case --->
					<cfset startpos ++>
				</cfif>
			<cfelseif findNameAndEquals[1].possiblequalifier is '"' >
				<cfset regExpToUseForValue = "DoubleQuotedValue">
			<cfelseif  findNameAndEquals[1].possiblequalifier is "'">
				<cfset regExpToUseForValue = "SingleQuotedValue">
			<cfelse>
				<cfset regExpToUseForValue = "UnQualifiedValue">
				<!--- need a special test here for  a=1 b= c=3 --->
				<cfif delimiter is " " and findNameAndEquals[1].whitespace is not "">
					<cfset nameAndEqualsRegExp = "[a-zA-Z0-9_.]+#equalsSign#">
					<cfset findNameAndEquals_ =  refindsingleoccurrence (reg_expression = nameAndEqualsRegExp,string = inputstring,start = startPos)>
					<cfif arrayLen(findNameAndEquals_) and findNameAndEquals_[1].position is startPos>
						<!--- we have found a name and equals exactly where we might have expected a value, therefore this item must be a null --->
						<cfset regExpToUseForValue = "null">
						<!--- this is a null value --->
					</cfif>
				</cfif>
			</cfif>

			<cfset findValue = refindsingleoccurrence (reg_expression = regexps[regExpToUseForValue].regexp, string = inputstring, groupNames = regexps[regExpToUseForValue].groups, start = startPos, usejavaMatch = true)>

			<cfif arrayLen(findValue)>
				<cfset value = findValue[1].value>
				<cfset name = findNameAndEquals[1].name>
				<cfset continueFrom = findValue[1].position + findValue[1].length>
			<cfelseif (regExpToUseForValue is "Null" or regExpToUseForValue is "UnQualifiedValue") and startPos GT len(inputString)>
				<!--- Another odd situation.  This will occur processing the last item of a string like this
					"x=1,y=2,z"
					ie we have a name without a value or an equals sign
					or
					"x=1,y=2,z="
					--->
				<cfset value = "">
				<cfset name = findNameAndEquals[1].name>
				<cfset continueFrom = startPos> <!--- startPos is greater than length so loop will end --->
			<cfelse>
				<!--- this is rather an odd situation
					- suggests an unmatched qualifier
					lets try just finding an unqualified value
					--->
				<cfset findValue = refindsingleoccurrence (regexps["UnQualifiedValue"].regexp,inputstring,regexps["UnQualifiedValue"].groups,startPos)>
				<cfif arrayLen(findValue)>
					<cfset name = findNameAndEquals[1].name>
					<cfset value = findValue[1].value>
					<cfset continueFrom = findValue[1].position + findValue[1].length>
				<cfelse>
					<cfset name = "">
					<cfset value = "">
					<cfset continueFrom = startPos + 1>
				</cfif>
			</cfif>

			<!--- This line unescapes escaped quotes --->
			<cfif regexps[regExpToUseForValue].qualifier is not "">
				<cfset value = replace(value,regexps[regExpToUseForValue].qualifier & regexps[regExpToUseForValue].qualifier,regexps[regExpToUseForValue].qualifier,"ALL")>
			</cfif>

			<!--- WAB 2008/09/24 altered this part to allow dot notation in variable names to be converted into a structure of structures--->
			<cfset pointer = result.result>
			<cfif DotNotationToStructure>
				<cfloop condition="listlen(name,'.') gt 1">
					<cfset thisKey = listFirst(name,".")>
					<cfif not structKeyExists (Pointer,thisKey)>
						<cfset structInsert (pointer,thisKey,structNew())> <!--- structInsert preserves case --->
					</cfif>
					<cfset pointer =  pointer[thiskey]>
					<cfset name = listrest(name,".")>
				</cfloop>
			</cfif>

			<!--- WAB 2011/11/22 had problem with strings like x=1&x=2.
				modifed to append values together --->
			<cfset name = trim(name)> <!--- NJH 2012/11/14 Case 431805 --->
			<cfif name is not "">
				<cfif not structKeyExists(pointer,name)>
					<cfset structInsert (pointer,name,value)>  <!--- structInsert preserves case --->
					<cfset result.orderedNameList = listappend(result.orderedNameList,name)>   <!--- this will go a bit wrong with DotNotationToStructure --->
				<cfelseif appendDuplicateKeys>
					<cfset pointer[name] = listAppend(pointer[name],value)>
				<cfelse>
					<!---
					not much we can do in this case if there is already and item with this name and appendDuplicateKeys is false
					will end up with the first value
					--->
				</cfif>
			</cfif>

			<cfset findNameAndEquals = refindsingleoccurrence (regexps.NameAndEquals.regexp,inputstring,regexps.NameAndEquals.groups,continueFrom)>
			<cfif counter gt 100>
				<cfoutput>regexp error convertNameValuePairStringToStructure_ loop #inputstring# #continueFrom#</cfoutput><cfabort>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<!--- delete an item from a list of name value pairs, handles quoted strings
		2013-05-15 WAB CASE 435127 added equalssign argument
		--->

	<cffunction name="removeItemFromNameValuePairString" access="public" hint="" returntype="string" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="itemToDelete" type="string" required="true">
		<cfargument name="delimiter" type="string" default = " ">
		<cfargument name="equalsSign" type="string" default="=">
		<cfset var re= "">
		<cfset var result= "">
		<cfif delimiter is " ">
		<cfset re = "(\A|\s)\s*#itemToDelete#\s*#equalsSign#\s*(""[^""]*""|'[^']*'|\S+)(\s|\Z)">
	<cfelse><!--- delimiters tested , and & . Others would work, but some might need to be escaped--->
		<cfset re = "(\A|#delimiter#)\s*#itemToDelete#\s*#equalsSign#\s*(""[^""]*""|'[^']*'|[^""']?[^#delimiter#]*[^""']?)\s*(#delimiter#|\Z)">
		</cfif>
		<cfset result = reReplaceNoCase (inputString,re,"#delimiter#","ALL")>
		<cfif left (result,1) is delimiter>
		<cfset result = mid(result,2,len(result)-1)>
	<cfelseif right(result,1) is delimiter>
		<cfset result = mid(result,1,len(result)-1)>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="removeItemsFromNameValuePairString" access="public" hint="" returntype="string" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="itemsToDelete" type="string" required="true">
		<cfargument name="delimiter" type="string" default = " ">
		<cfargument name="equalsSign" type="string" default="=">
		<cfset var result = inputString>
		<cfset var item = "">
		<cfloop index="item" list = "#itemsToDelete#">
			<cfset result = removeItemFromNameValuePairString(inputString = result,itemToDelete = item,delimiter = delimiter,equalsSign = equalsSign)>
		</cfloop>
		<cfreturn  result>
	</cffunction>


	<cffunction name="getFileDetailsFromPath" access="public" hint="" returntype="struct" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfset var x= "">
		<cfset var re = "(\A|.*[\\/])([^\/\\]*)\.([^\.]*)\Z">   <!--- 2007-12-05 WAB added the \A to handle filename without any path --->
		<cfset var result = structNew()>
		<cfset x = reFind (re,inputstring,0,true)>
		<cfif x.pos[1] is not 0>
		<cfset result.extension = mid (inputString,x.pos[4],x.len[4])>
		<cfset result.name = mid (inputString,x.pos[3],x.len[3])>
		<cfset result.path = mid (inputString,x.pos[2],x.len[2])>
		<cfset result.fullfilename = result.name & "." & result.extension>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!---
		take a URL and return any parameters in a structure
		--->

	<cffunction name="getParametersFromURLString" access="public" hint="" returntype="struct" output="false">
		<cfargument name="URLString" type="string" required="true">
		<cfset var result = structNew()>
		<cfif listLen(URLString,"?") gt 1>
		<cfset result = convertNameValuePairStringToStructure(URLString,"&")>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2008-01-15 need to manually escape singlequote singlequote when putting into database because cf doesn't do this automatically
		written orginally for phrases where often appears in javascript functions--->

	<cffunction name="escapeDoubleSingleQuotes"  access="public" returns = "string" output="false">
		<cfargument name="inputString" required = true>
		<cfreturn  replaceNoCase(inputString,"''","''''","ALL")>
	</cffunction>

	<!---
		WAB 2011/11/18 now returns order of arguments
		--->

	<cffunction name="parseRelayFunction" access="public" hint="" returntype="struct" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfset var result = {name="",params=structNew(),paramListOrdered="",unnamedArguments=false}>
		<cfset var regexp = "\[\[(.*)\((.*)\)\]\]">
		<cfset var groups = {1="name",2="parametersString"}>
		<cfset var convertNameValuePairs = '' />
		<cfset var I = 0>
		<cfset var thisArgument="">
		<cfset var argumentsArray = '' />
		<cfset var argumentName = '' />
		<cfset var refindresult = refindSingleOccurrence(regExp,inputString,groups)>
		<cfif arrayLen(refindresult)>
		<cfset result = refindresult[1]>
		<cfset result.parametersString = trim(result.parametersString)>
		<!---
			WAB 2012-03-19
			Need to deal with unnamed parameters as well as named, so check for form xxxxx= in the parameter string--->
		<cfif result.parametersString is "" or refind("\A[A-Za-z_][A-Za-z0-9_ ]*=",result.parametersString)>
		<cfset convertNameValuePairs = application.com.regexp.convertNameValuePairStringToStructure_(inputString=result.parametersstring,delimiter=",")>
		<cfset result.params = convertNameValuePairs.result>
		<cfset result.paramListOrdered = convertNameValuePairs.orderedNameList>
	<cfelse>
		<!--- unnamed parameters
			assuming that there are no commas in the arguments then we can just loop over a list
			might need a better function if there are commas or escaped qualifiers
			--->
		<cfset argumentsArray = listToArray(result.parametersString)>
		<cfset result.unnamedArguments = true>
		<cfset result.paramListOrdered = "">
		<cfloop index="I" from = "1" to="#arrayLen(argumentsArray)#" >
		<cfset argumentName="unnamed_#I#">
		<cfset result.params[argumentName] = rereplace(argumentsArray[I],"\A([""']?)(.*)\1\Z","\2")>
		<cfset result.paramListOrdered = listAppend(result.paramListOrdered ,argumentName)>
		</cfloop>
		</cfif>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="parseRelayTag" access="public" hint="" returntype="struct" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfset var result = {name="",params=structNew()}>
		<cfset var regexp = application.com.relayTranslations.RegExp.relayTag>
		<cfset var groups = {1="name",2="parametersstring"}>
		<cfset var refindresult = refindSingleOccurrence(regExp,inputString,groups)>
		<cfif arrayLen(refindresult)>
		<cfset result = refindresult[1]>
		<cfset result.params = application.com.regexp.convertNameValuePairStringToStructure(inputString=result.parametersstring,delimiter=" ")>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2013-07-03 code pulled out of comms --->

	<cffunction name="parseURL">
		<cfargument name="url" required="true">
		<cfset var regExp = "^((((.*?)://)?([^?##/]*))([^:\s\?\##\&""']*))(\?([^##""'\s]*))?(##([^""'\s]*))?">
		<cfset var groups = {1="protocolDomainAndPath", 2="protocolAndDomain",3="protocolwithSlashes",4="protocol",5=  "domain",6="path",7="queryStringWithQuestionMark",8="queryString",9="fragmentWithHash",10="fragment"}>
		<cfset var result = reFindAllOccurrences(reg_expression = regExp,string = url, groupnames = groups)>
		<cfreturn result>
	</cffunction>


	<cffunction name="parseQuery" access="public" hint="" returntype="struct" output="false">
		<cfargument name="queryString" type="string" required="true">
		<!--- an idea of WABs to break up a query into its constituent bits
			fairly rough and ready.  Requires SELECT,FROM,WHERE,GROUP BY,ORDER BY in main query to be in capitals (and any other of these word to not be capitalised)
			--->
		<cfset var remaining = queryString>
		<cfset var query = structNew()>
		<cfset var re = "">
		<cfset var x = "">
		<cfset var y = "">
		<cfset var getCols = "">
		<cfset var getAllCols = "">
		<cfset var checkQuery = "">
		<cfset var col = "">
		<cfset var colname = "">
		<cfset query.orderby = "">
		<cfset query.groupBy = "">
		<cfset query.where = "">
		<cfset query.column = structNew()>
		<cfset query.alias = structNew()>
		<cfset re = "SELECT(.*?)FROM(.*?)(WHERE(.*?)){0,1}(GROUP BY(.*?)){0,1}(ORDER BY(.*?)){0,1}\Z">
		<cfset x = reFind (re,remaining,0,true)>
		<cfif x.pos[1] is not 0>
		<cfset query.select = mid (remaining,x.pos[2],x.len[2])>
		<cfset query.from = mid (remaining,x.pos[3],x.len[3])>
		<cfif x.pos[5] is not 0><cfset query.where  = mid (remaining,x.pos[5],x.len[5])></cfif>
		<cfif x.pos[7] is not 0><cfset query.groupby = mid (remaining,x.pos[7],x.len[7])></cfif>
		<cfif x.pos[9] is not 0><cfset query.orderby = mid (remaining,x.pos[9],x.len[9])></cfif>
		<cftry>
		<!--- get names of columns --->
		<cfquery name="getCols" datasource="#application.sitedatasource#" cachedWithin= #CreateTimeSpan(1,0,0,0)# debug = no>
						select #preserveSingleQuotes(query.select)# from  #preserveSingleQuotes(query.from)# where 1 = 0 <cfif query.groupBy is not "">group by #preserveSingleQuotes(query.groupby)#</cfif>
						</cfquery>
		<cfquery name="getAllCols" datasource="#application.sitedatasource#" cachedWithin= #CreateTimeSpan(1,0,0,0)# debug = no>
						select * from  #preserveSingleQuotes(query.from)# where 1 = 0
						</cfquery>
		<cfcatch>
		<!--- check whether the underlying query runs --->
		Problem Parsing Query <BR>
		<cftry>
		<cfquery name="checkQUERY" datasource="#application.sitedatasource#" debug = no>
								#preserveSingleQuotes(queryString)#
								</cfquery>
		<cfcatch>
		Your underlying query does not work!!
		<cfdump var="#query#">
		<cfrethrow>
		</cfcatch>
		</cftry>
		Make sure that SELECT, FROM, WHERE, GROUP BY and ORDER BY are in capitals in your main query
		and not in All Capitals in any subqueries
		<cfdump var="#query#">
		<cfrethrow>
		</cfcatch>
		</cftry>
		<cfset query.SelectedColumns = getCols.columnList>
		<cfset query.AllColumns = getAllCols.columnList>
		<!--- think I might be able to do something useful knowing what all the aliases are
			query.column['columnx'].alias  tells you what columnx is aliased as
			query.column['columnx'].actualname  tells you what columnx actual name is .  If columnx is an alias then this can be used for getting at the actual name
			--->
		<cfloop index="col" list="#query.AllColumns#">
		<cfif not structKeyExists(query.column,col)>
		<cfset query.column[col] = structNew()>
		<cfset query.column[col].actualname = col>
		<cfset query.column[col].alias = col>
	<cfelse>
		<!--- this column must appear more that once so would cause a problem if we needed to reference it
			really need to know table alias, but don't know how I could find that out
			as a hack I am going to doctor organisationid and assume that a single letter alias
			--->
		<cfif 'organisationid,locationid,personid' contains col >
		<cfset query.column[col].actualname = left(col,1) & "." &col>
		</cfif>
		</cfif>
		</cfloop>
		<cfloop index="col" list="#query.selectedColumns#">
		<cfif listFindNoCAse(query.AllColumns,col) is 0>
		<!--- column not in list so may be aliased --->
		<!--- this was a basic re which just went back to the comma before the "as xyz"
			<cfset re="(\A|,)([^,]*) as #col#[\s]*(,|\Z)">
			this doesn't work for complicated aliases (maybe a function which has a comma in it.
			for these you can put the whole expression in double brackets ((   ...   )) and this re will pick it up
			--->
		<cfset re="(\A|,)[\s]*([^\(][^,]*[^\)]|\(\(.*\)\))[\s]*as[\s]*#col#[\s]*(,|\Z)">
		<cfset y = reFindNoCase (re,query.select,0,true)>
		<cfif y.pos[1] is not 0>
		<!--- this is an aliased column --->
		<cfset colname = trim(mid (query.select,y.pos[3],y.len[3]))>
		<cfif not structKeyExists (query.column,colname)>
		<cfset query.column[colname] = structNew()>
		<cfset query.column[colname].actualname = colname>
		</cfif>
		<cfset query.column[colname].alias  = col>
		<cfset query.column[col]  = query.column[colname]>
		</cfif>
		</cfif>
		</cfloop>
	<cfelse>
		Bad query
		</cfif>
		<cfreturn query>
	</cffunction>

	<!---
		WAB 2011/06/15 Extended this function and converted to use refindAllOccurrences
		for use in communications to strip out and process links
		WAB 2011/12/01 extended so that would work for any tag, just tell it whether it has an endtag or not
		will still work for those a,script,form which were hardcoded as having end tags
		WAB 2012/01  Many more extensions made while doing security project and mass search and replace of tags
		--->

	<cffunction name="findHTMLTagsInString" access="public" hint="Finds particular HTML tags in a string and returns an array of structures with all instances of the tag and their attributes" returntype="array" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<cfargument name="htmlTag" type="string" required="Yes">
		<cfargument name="hasEndTag" type="boolean" >
		<cfargument name="attributeRegExp" type="string" default=""> <!--- code a regExp to pull out tags with a particular attribute, for example to find a hidden input field use attributeRegExp="type=[""']hidden[""']"   --->
		<cfargument name="getLineNumbers" type="boolean" default="false">
		<cfargument name="ignoreComments" type="boolean" default="false">
		<cfargument name="useSophisticatedRegExp" type="boolean" default="false">  <!--- see note below (2012-01-20) --->
		<cfargument name="useJavaMatch" type="boolean" default="true">

		<cfset var result = "">
		<cfset var thisresult = "">
		<cfset var regExp = "">
		<cfset var tagsWithEndTag = "a,script,form">
		<cfset var commentPositions = "">
		<cfset var i = 0>
		<cfset var argumentCollection = "">
		<cfset var groups = {1="openingTag",2="tagName",3 = "attributeString"}>

		<cfif not structKeyExists (arguments,"hasEndTag")>
			<cfset arguments.hasEndTag = iif(listfindnocase(tagsWithEndTag,htmlTag),true,false)>
		</cfif>

		<cfif hasEndTag>
			<cfset regexp = "(<(#arguments.htmlTag#)(?: (.*?)>|>))(.*?)(</\2>)">
			<!---	WAB 2012-01-10 updated so that has either space or > after the tag name - prevents conflict between say cfquery and cfqueryparam <cfset regexp = "<(#arguments.htmlTag#)(.*?)>(.*?)</\1>"> --->
			<cfset groups[4] = "innerHTML">
			<cfset groups[5] = "endTag">
		<cfelse>
			<!---
				WAB 2012-01-20 This regExp cannot deal with a nested tag inside a the tag
				Not an issue in pure HTML, but can be an issue when parsing CF pages, where a <CFIF can occur inside a another tag
				The Sophisticated regExp below ignores any < > inside the tag, but will fall over if you have something like <INPUT type="button" value="<<" >
				Was needed during security upgrade
				--->
			<cfif useSophisticatedRegExp>
				<cfset regExp = "(<(#arguments.htmlTag#)((?:<(?:<.*?>|.)*?>|.)*?)>)">
			<cfelse>
				<!--- WAB 2012-04-24 CASE 427705 had to replace .* with [^>]* because was being too greedy.  Suprised problem only just surfaced!  --->
				<cfset regExp = "(<(#arguments.htmlTag#)(?: ([^>]*?#attributeRegExp#.*?)>|>))">
			</cfif>
		</cfif>

		<cfset argumentCollection = {reg_expression = regExp,string = inputString,groupNames = groups, getLineNumbers = getLineNumbers, useJavaMatch=useJavaMatch}>

		<cfset result = refindAllOccurrences(argumentCollection = argumentCollection)>

		<cfloop array="#result#" index="thisResult">
			<cfset thisResult.attributes = convertNameValuePairStringToStructure(thisResult.attributeString & " ")> <!--- the trailing space deals with an edge case  x="1" y="2"z> - where we want to pickup z as an attribute with no value --->
		</cfloop>

		<cfif ignorecomments>
			<cfset commentPositions = getComments (inputString)>
			<cfloop from="#arraylen(result)#" to =1 index="i" step="-1">
				<cfif isPositionInsidePositionArray(commentPositions,result[i].position)>
					<cfset arrayDeleteAt(result,i)>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- WAB: Not quite sure why I put this function in this file, but couldn't find anywhere sensible, and the code which converts the namevaluepair lists to a structure is here  --->

	<cffunction name="CollectFormFieldsIntoNameValuePairs" access="public" hint="deals with any form fields which need to get shoved into a 'additionalParameters' field" returntype="string" output="false">
		<!--- WAB 2007-10-16  rationalising all the versions I have of this code used in elemenediting, screendefedit, flagedit, --->
		<cfargument name="fieldPrefix" type="string" default="">  <!--- prefix for all these fields - usually parameter or frmparameter .  assumes _ as separator--->
		<cfargument name="fieldSuffix" type="string" default="">  <!--- usually blank --->
		<cfargument name="appendTo" type="string" default=""> <!--- might be form.parameters --->
		<cfargument name="delimiter" type="string" default=",">
		<cfargument name="equalssign" required="no"  type="string" default="=" >
		<cfargument name="includeNulls" type="boolean" default="false">
		<cfset var tempstructure = "">
		<cfset var result = "">
		<cfset tempstructure = CollectFormFieldsIntoStructure (fieldPrefix = fieldPrefix, fieldSuffix = fieldsuffix, DotNotationToStructure = true, includeNulls = includeNulls)>
		<!--- WAB 2009/03/03 bug found here - amazing worked as well as it did, maybe something else changed
			if no items in tempstructure then the listAppend appended blank to the appendTo variable, which ended up giving a trailing delimiter
			--->
		<cfif structCount(tempstructure) is 0>
		<cfset result = appendTo>
	<cfelse>
		<cfset result = listappend(appendTo,application.com.structurefunctions.convertStructureToNameValuePairs(struct=tempstructure,delimiter = delimiter, equalssign = equalssign,recurse=true))>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2008/10/07 made this from CollectFormFieldsIntoNameValuePairs because I needed them in a nice structure
		could use it as an intermediate function called by CollectFormFieldsIntoNameValuePairs
		2012-04-24 WAB	Case 432049 changed so can handle Suffix or Prefix (or both).  Previously had to have a Prefix
		--->

	<cffunction name="CollectFormFieldsIntoStructure" access="public" hint="deals with any form fields which need to get shoved into a 'additionalParameters' field" returntype="struct" output="false">
		<cfargument name="fieldPrefix" type="string" default="">  <!--- prefix for all these fields - usually parameter or frmparameter .  assumes _ as separator--->
		<cfargument name="fieldSuffix" type="string" default="">  <!--- usually blank --->
		<cfargument name="DotNotationToStructure" type="boolean" default="true">
		<cfargument name="includeNulls" type="boolean" default="true">
		<cfset var result = structNew()>
		<cfset var field = "">
		<cfset var paramName= "">
		<cfset var paramValue= "">
		<cfset var pointer= "">
		<cfset var prefix = "">
		<cfset var suffix = "">
		<cfset var regExp = "">
		<cfset var findParam = "">
		<cfif fieldPrefix is not "">
		<cfset prefix = fieldprefix & "_">
		</cfif>
		<cfif fieldSuffix is not "">
		<cfset suffix = "_" & fieldSuffix >
		</cfif>
		<cfif fieldPrefix & fieldSuffix is "">
		<cfoutput>Must Supply Either fieldPrefix OR fieldSuffix</cfoutput>
		<cf_abort>
		</cfif>
		<cfset regExp = "\A#prefix#(.*?)#suffix#\Z">
		<!--- gather together any parameters into a form  --->
		<cfloop index="field" list = "#form.fieldnames#">
			<cfif refindNoCase (regExp,field)	>
			<cfset findParam= refindAllOccurrences(regExp,field)>
			<cfset paramName = findParam[1][2]>
			<!--- WAB 2009/04/27 new idea so that the name of the parameter can be stored in another field
				this is needed so that we can have users add their own parameters with a textbox for the name and a text box for the value
				so you might have fields
				frmParameter_New01
				and 	name_frmParameter_New01
				WAB 2014-02-25 CASE 439005 discovered that this piece of code was only really working correctly when using a suffix rather than a prefix.
				The 'name' part will be at the same end as the prefix/suffix (otherwise these name fields are picked up by the regExp as fields in their own right)
				--->
			<cfif fieldPrefix is not "" and structKeyExists(form,"name_#field#")>
			<cfset paramName = form ["name_#field#"]>
		<cfelseif fieldSuffix is not "" and structKeyExists(form,"#field#_name")>
			<cfset paramName = form ["#field#_name"]>
			</cfif>
			<cfset paramvalue = form[field]>
			<cfif includenulls or paramvalue is not "">
			<cfset pointer = result>
			<cfloop condition="listLen(paramName,'.') gt 1">
			<cfif not structKeyExists(pointer, listfirst(paramName,'.'))>
			<cfset pointer[listfirst(paramName,'.')] = structNew()>
			</cfif>
			<cfset pointer = pointer[listfirst(paramName,'.')] >
			<cfset paramName = listRest(paramName,'.')>
			</cfloop>
			<cfset pointer[paramName] = paramvalue>
			</cfif>
			</cfif>
		</cfloop>
		<cfreturn result>
	</cffunction>


	<cffunction name="makeSafeColumnName" access="public" hint="Makes fields passed into it safe to become database columns" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<cfset var regExp = "[^A-Za-z0-9_]">
		<cfset var result = REReplaceNoCase(inputString,regExp,"","ALL")>
		<cfif isNumeric(left(Result,1))>
		<cfset result = "_" & result>
		</cfif>
		<cfreturn Result>
	</cffunction>

	<!--- WAB 2011/09/26
		Function for making a TextID.
		Thought that there might have already been one in the system but couldn't find it
		Turned out that the rules are the same as makeSafeColumnName(), but thought it worth having a function with a better name
		Suppose that one could extend it to make a unique textID on a particular table (flags already has functions to do this)
		--->

	<cffunction name="makeSafeTextID" access="public" hint="Makes value passed in safe to be a textID" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<cfreturn makeSafeColumnName(inputString)>
	</cffunction>

	<!--- WAB 2011/09/26 --->

	<cffunction name="isTextIDSafe" access="public" hint="Makes value passed in safe to be a textID" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<!--- regExp for string only containing alphaNumeric and _, not starting with number --->
		<cfset var regExp = "\A[A-Za-z_][A-Za-z0-9_]*\Z">
		<cfreturn refindNoCase(regexp,inputString)>
	</cffunction>

	<!---
		WAB 2010/02/04
		Takes out any trailing, leading or double commas from a list
		useful before putting a list into an IN statement if the list has come from multiple form fields with same name, some of which may be blank
		used in flagTask.cfm for LID 3064
		WAB 2016-04-20 CASE 449069 This function failed when dealing with three or more commas in a row.  Adjusted to use a better regExp
		--->

	<cffunction name="removeExtraCommasFromList" output="false">
		<cfargument name = "list">
		<cfset arguments.list = reReplace(arguments.list,",(?=,)","","ALL")>
		<cfreturn reReplace(list,"\A,|,\Z","","ALL")>
	</cffunction>

	<!---
		WAB 2010/05/26
		Attempts to clean out commas and currency symbols from a formatted number
		Will deal with comma as decimal separator for up to 2 decimal places
		--->

	<cffunction name="removeNumberFormatting" output="false" >
		<cfargument name="inputString" type="string" required="Yes">
		<cfset var result = inputString>
		<!--- remove anything which not 0-9,. --->
		<cfset result = reReplaceNoCase(result,"[^0-9\.\-,\(\)]","","ALL")>
		<!--- replace ,nnn with nnn --->
		<cfset result = reReplaceNoCase(result,",([0-9]{3})","\1","ALL")>
		<!---
			deal with comma as the decimal separator
			replace ,n or ,nn at end of string with .n and .nn
			--->
		<cfset result = reReplaceNoCase(result,",([0-9]{0,2})\Z",".\1","ALL")>
		<!---
			deal brackets for negative
			--->
		<cfset result = reReplaceNoCase(result,"\((.*)\)","-\1","ALL")>
		<cfreturn result>
	</cffunction>


	<cffunction name="getFunctionCallFromString" access="public" returnType="array" output="false">
		<cfargument name="functionString" type="string" required="true">
		<cfset var groups ={1="component",2="method",3="argumentList"}>
		<cfset var regexp = "(.+?)\.([^\.]+?)(?:\Z|\((.*?)\))">
		<cfset var functionArray = refindAllOccurrences(regexp,arguments.functionString,groups)>
		<cfset var idx = 0>
		<cfloop from=1 to=#arrayLen(functionArray)# index="idx">
			<cfset functionArray[idx].argumentStruct = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = functionArray[idx].argumentList,delimiter=",")>
		</cfloop>
		<cfreturn functionArray>
	</cffunction>


	<cffunction name="removeProtocol" access="public" returnType="string" output="false">
		<cfargument name="String" type="string" required="true">
		<cfreturn rereplacenocase(string,"http(s)?://","")>
	</cffunction>


	<cffunction name="camelCaseToSpaces" access="public" returnType="string" output="false">
		<cfargument name="string">
		<!--- WAB 2014-06-17 small improvement when comes across multiple capital letters in a row --->
		<cfset var result	= rereplace(string,"([a-z])([A-Z])","\1 \2","ALL")> <!--- adds spaces between small and capital letters,  --->
		<cfset result	= rereplace(result,"([A-Z])([A-Z][a-z])","\1 \2","ALL")> <!--- adds if multiple capital letters in a row followed by small then adds space before last one --->
		<cfset result	= rereplace(rereplace(result,"\A(.)","\U\1","ONE"),"_"," ","ALL")> <!--- Captialises the first letter and replaces _ with blank --->
		<cfreturn result>
	</cffunction>

	<!--- NYB 2011-12-09 LHID8232 added function, primarily to remove script tags when opened in Word --->

	<cffunction name="replaceHTMLTagsInString" access="public" hint="Finds particular HTML tags in a string and returns an array of structures with all instances of the tag and their attributes" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<cfargument name="htmlTag" type="string" required="Yes">
		<cfargument name="replaceExp" type="string" default = "">
		<cfargument name="hasEndTag" type="boolean" default="true">
		<cfargument name="preserveLineNumbers" type="boolean"  default="false">
		<cfargument name="preserveLength" default="false">
		<cfset var result = inputString>
		<cfset var regExp = "">
		<cfset var replaceString = "">
		<cfset var numberOfLineBreaks = 0>
		<cfset var matches = '' />
		<cfset var match = '' />
		<cfif hasEndTag>
		<cfset regexp = "<(#arguments.htmlTag#)(.*?)>(.*?)</\1>">
	<cfelse>
		<cfset regExp = "<(#arguments.htmlTag#)(.*?)>">
		</cfif>
		<cfif not preserveLineNumbers and not preservelength>
		<cfset result = REreplaceNoCase(result,regexp,replaceExp, "ALL")>
	<cfelse>
		<cfset matches = refindAllOccurrences (regExp,inputString)>
		<cfloop array="#matches#" index="match">
		<cfif preservelength>
		<cfset replaceString = rereplace(match.string,"[^\n]"," ","ALL")>
	<cfelse>
		<cfset numberOfLineBreaks = arrayLen(rematch("\n",match.string))>
		<cfset replaceString = repeatString(chr(10),numberOfLineBreaks)>
		</cfif>
		<cfset result = replace(result,match.string,replaceString)>
		</cfloop>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="reReplaceWithBlank" access="public" hint="does a regular expression replace but can preserves length and line numbering" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<cfargument name="reg_expression" type="string" required="Yes">
		<cfargument name="preserveLineNumbers" type="boolean"  default="true">
		<cfargument name="preserveLength" type="boolean" default="true">
		<cfset var matches = refindAllOccurrences (reg_expression,inputString)>
		<cfset var result = replaceByPositionArray(inputString = inputString,positionArray=matches,replaceCharacter=" ",preserveLineNumberspreserveLineNumbers  = preserveLineNumbers,preserveLength = preserveLength)>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2016-01-26 Added for use in CodeCop --->

	<cffunction name="reReplaceNoCaseStartingAt" access="public" hint="Does reReplace starting at a particular position" output="false">
		<cfargument name="string" type="string" required="Yes">
		<cfargument name="reg_expression" type="string" required="Yes">
		<cfargument name="substring" type="string"  required="true">
		<cfargument name="scope" type="string" default="ONE">
		<cfargument name="startAt" type="numeric" default="0">
		<cfset var result = "">
		<cfset var length = len (string)>
		<cfif startAt GT length>
		<cfset result = string>
	<cfelse>
		<cfif startAt GT 1>
		<cfset result = left (string,startAt-1)>
		</cfif>
		<cfset result &= rereplaceNoCase (right(string,length - startAt + 1),reg_expression, substring, scope)>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="ReplaceByPositionArray" access="public" hint="Takes a String and a position Array (as created by reFindAllOccurrences) and replaces the positions defined in the position array with the replace character" output="false">
		<cfargument name="inputString" type="string" required="Yes">
		<cfargument name="positionArray" type="array" required="Yes">
		<cfargument name="preserveLength" type="boolean" default="true">
		<cfargument name="replacementCharacter" type="string" default=" "> <!--- only used when preserving length --->
		<cfargument name="preserveLineNumbers" type="boolean"  default="true">
		<cfset var result = inputString>
		<cfset var replaceString = "">
		<cfset var numberOfLineBreaks = 0>
		<cfset var match = '' />
		<cfset var i = 0 />
		<cfloop index="i" from="#arrayLen(positionArray)#" to = "1" step ="-1">
			<cfset match = positionArray[i]>
			<cfif preservelength>
			<cfset replaceString = rereplace(match.string,"[^\n]",replacementCharacter,"ALL")>
		<cfelse>
			<cfset numberOfLineBreaks = arrayLen(rematch("\n",match.string))>
			<cfset replaceString = repeatString(chr(10),numberOfLineBreaks)>
			</cfif>
			<cfset result = removeChars(result,match.position,match.length)>
			<cfif replaceString is not "">
			<cfset result = insert(replaceString,result,match.position-1)>
			</cfif>
		</cfloop>
		<cfreturn result>
	</cffunction>

	<!---
		WAB 2012-01-26
		To find position of all comments in a string
		Deals with nested comments
		In theory could be done with a regExp (<\! ---((?:<\! ---.*?--- >|.)*?)--- >), but this is too complicated and crashes the regExp engine
		Could be modified for any tag
		--->
	<cfset this.comments = {}>
	<cfset this.comments.CF = {opening="<!---",closing="--->"}>
	<cfset this.comments.SQL = {opening="/\*",closing="\*/"}>  <!--- WAB 2013-01-07 discovered that I had the wrong sort of slash in my comments, amazing! --->
	<cfset this.comments.SQLSingleLine = {opening="--",closing="\n"}>
	<cfset this.comments.JSSingleLine = {opening="\\\\",closing="\n"}>
	<cfset this.comments.HTML = {opening="<!--[^-]",closing="[^-]-->"}> <!--- ought to have negative lookahead on -  --->
	<cfset this.comments.brackets = {opening="\(",closing="\)"}> <!--- ought to have negative lookahead on -  --->

	<cffunction name="getComments" output="false">
		<cfargument name="string" type="string">
		<cfargument name="Type" type="string" default="CF"> <!--- CF, SQL for SQL --->
		<cfreturn getMatchedOpeningAndClosingTags(string=string, openingRegExp = this.comments[type].opening, closingRegExp = this.comments[type].closing)>
	</cffunction>

	<!---
		getMatchedOpeningAndClosingTags()  [previously getMatchedOpenersAndClosers()]
		WAB 2012-01 - during security project.
		A primitive parser which will find matching opening and closing tags - and not be led astray by nesting
		Developed to find comments in a string (so that they could be deleted or ignored)
		Returns an array of matches in a similar way to reFindAllOccurrences
		Later used to parse brackets in SQL statements
		And (CASE 432644) to parse <relay_if> </relayif> tags
		WAB 2013-01-05 CASE 432644
		Extended to return an array containing the positions of any 1st generation nested tags.
		In particular this allowed me to find relay_elseifs and relay_elses inside a relay_if without finding those inside nested relayifs
		Also added singleOccurrence parameter
		--->

	<cffunction name="getMatchedOpeningAndClosingTags" output="false">
		<cfargument name="string" type="string">
		<cfargument name="openingRegExp" type="string">
		<cfargument name="closingRegExp" type="string">
		<cfargument name="singleOccurrence" type="boolean" default="false">
		<cfset var result = []>
		<cfset var openCloseCount = 0>
		<cfset var thisResult = {}>
		<cfset var nestedTag = '' />
		<cfset var findStart = '' />
		<cfset var currentPos = '' />
		<cfset var findnext = '' />
		<!---
			find an opener
			find the next thing which either an opener or a closer
			keep count of whether we have closed all the openers , when all closed save result
			--->
		<cfset findStart = javaMatch(regexp = openingRegExp,string = string,singleoccurrence = true)>
		<cfloop condition = "arrayLen(findStart)">
			<cfset thisResult = {position = findStart[1].Position, hasNestedTag = false, startInner = findStart[1].Position + findStart[1].length, openingString = findStart[1].string,nestedtags = arraynew(1)}>
			<cfset openCloseCount ++>  <!--- openCloseCount should always be 1 after this increment --->
			<cfset currentPos = findStart[1].Position + 1>
			<cfloop condition="openCloseCount is not 0">
				<cfset findnext = javaMatch(regexp = "#openingRegExp#|#closingregExp#",string = string,start = currentPos,singleoccurrence = true)>
				<cfif arrayLen(findNext)>
				<cfif refindNoCase(openingRegExp,findNext[1].string) >
				<cfset openCloseCount ++>
				<cfset thisResult.hasNestedTag = true>
				<cfif openCloseCount is 2>
				<!--- keep track of where the nested tag starts (only first generation) --->
				<cfset nestedtag = {position = findnext[1].position - thisResult.position + 1}>
				</cfif>
			<cfelse>
				<cfset openCloseCount -- >
				<cfif openCloseCount is 1>
				<!--- keep track of where the nested tag ends --->
				<cfset nestedtag.end = findnext[1].end - thisResult.position + 1>
				<cfset nestedtag.length = nestedtag.end - nestedtag.position + 1>
				<cfset arrayAppend(thisResult.nestedtags,nestedtag)>
				</cfif>
				</cfif>
			<cfelse>
				<!--- should only get here if there are unmatched comments somewhere, so can just return the result that we have got so far--->
				<cfreturn result>
				</cfif>
				<cfset currentPos = findNext[1].Position + 1>
			</cfloop>
			<cfset thisResult.end = findNext[1].end>
			<cfset thisResult.length = thisResult.end - thisResult.position + 1>
			<cfset thisResult.string = mid(string,thisResult.position,thisResult.length)>
			<cfset thisResult.innerText = mid(string,thisResult.startInner,findNext[1].position - thisResult.startInner)>
			<cfset thisResult.closingString = findNext[1].string>
			<cfset arrayAppend(result,thisResult)>
			<cfif singleOccurrence><cfbreak></cfif>
			<cfset findStart = javaMatch(regexp = openingRegExp,string = string,start = thisResult.end + 1,singleoccurrence = true)>
		</cfloop>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2013-01-07 decided to change name of this function, this is for backwards compatibility--->

	<cffunction name="getMatchedOpenersAndClosers" output="false">
		<cfreturn getMatchedOpeningAndClosingTags(argumentCollection = arguments)>
	</cffunction>


	<cffunction name="removeComments" output="false">
		<cfargument name="string" type="string">
		<cfargument name="Type" type="string" default="CF"> <!--- blank for CF, SQL, HTML for SQL --->
		<cfargument name="preserveLineNumbers" type="boolean" default="false">
		<cfargument name="preserveLength" type="boolean" default="false">
		<cfset var result = string>
		<cfset var replaceString = "">
		<cfset var numberOfLineBreaks = 0>
		<cfset var foundItem = '' />
		<cfset var commentPositions = getComments(string = string,type = type)>
		<cfloop array="#commentPositions#" index="foundItem">
			<cfif not preserveLineNumbers and not preservelength>
			<cfset result = ReplaceNoCase (result,foundItem.string,"")>
		<cfelse>
			<cfif preservelength>
			<cfset replaceString = rereplace(foundItem.string,"[^\n]"," ","ALL")>
		<cfelse>
			<cfset numberOfLineBreaks = getNumberOfLineBreaks(foundItem.string)>
			<cfset replaceString = repeatString(chr(10),numberOfLineBreaks)>
			</cfif>
			<cfset result = replace(result,foundItem.string,replaceString)>
			</cfif>
		</cfloop>
		<cfreturn result>
	</cffunction>


	<cffunction name = "removeAllComments" output="false">
		<cfargument name="string" type="string">
		<cfargument name="preserveLineNumbers" type="boolean" default="false">
		<cfargument name="preserveLength" type="boolean" default="false">
		<cfset var result = string>
		<cfset result = removeComments(string = result,type = "HTML",preserveLineNumbers = preserveLineNumbers,preserveLength = preserveLength )>
		<cfset result = removeComments(string = result,type = "CF",preserveLineNumbers = preserveLineNumbers,preserveLength = preserveLength )>
		<cfset result = removeComments(string = result,type = "SQL", preserveLineNumbers = preserveLineNumbers,preserveLength =preserveLength )>
		<cfset result = removeComments(string = result,type = "SQLSingleLine", preserveLineNumbers = preserveLineNumbers,preserveLength =preserveLength )>
		<cfreturn result>
	</cffunction>

	<!---
		WAB 2012-01
		Function to replace refindAllOccurrences using the java library
		Java library seems less prone to falling over on complicated regExps
		Might need a bit more work around the flags
		--->

	<cffunction name="javaMatch" output="false">
		<cfargument name="regExp" type="string">
		<cfargument name="string" type="string">
		<cfargument name="dotall" type="boolean" default="true">  <!--- whether dot matches across a line break --->
		<cfargument name="groupnames" type="struct" default="#structNew()#">
		<cfargument name="start" type="numeric" default="1">
		<cfargument name="getLineNumbers" type="boolean" default="false">
		<cfargument name="singleOccurrence" type="boolean" default="false">
		<cfset var result = arrayNew(1)>
		<cfset var regExpObject = createObject("java", "java.util.regex.Pattern")>
		<cfset var flags = "">
		<cfset var pattern = "">
		<cfset var match = "">
		<cfset var thisResult = {}>
		<cfset var i = "">
		<cfset var whole = "">
		<cfset var thisGroup = "">
		<cfset var right = '' />
		<cfset var searchString = string />
		<cfset flags = bitOR(iif(dotall,regExpObject.DOTALL,0),regExpObject.CASE_INSENSITIVE)>
		<cfset pattern = regExpObject.compile("" &  regexp,flags )>
		<cfif start is 0>
		<cfset arguments.start = 1>
	<cfelseif start is not 1>
		<cfset right = len(searchString) - start + 1>
		<cfif right lte 0>
		<cfreturn result>
		</cfif>
		<cfset searchString = right(searchString, right)>
		</cfif>
		<cfset match = pattern.matcher(searchString)>
		<cfif structCount (groupNames) is 0>
		<cfset arguments.groupNames = {1=1,2=2,3=3,4=4,5=5,6=6,7=7,8=8,9=9,10=10,11=11,12=12,13=13,14=14,15=15,16=16}> <!--- does limit to 9 groups in a regexp - hope enough --->
		</cfif>
		<cfloop condition="match.find()">
			<cfset whole = match.group()>
			<cfset thisResult = {position = match.start() + start,end = start+match.end()-1,length = match.end() - match.start() ,string=whole}>
			<cfloop index="i" from="1" to="#match.groupcount()#">
				<cfset thisGroup = match.group(i)>
				<!--- groups with zero length match seem to return an undefined value --->
				<cfif not isDefined("thisGroup")>
				<cfset thisGroup = "">
				</cfif>
				<cfset thisResult[groupNames[i]] = thisGroup>
			</cfloop>
			<cfif getLineNumbers>
			<cfset thisResult.LineNumber = getNumberOfLineBreaks(left(searchString,thisResult.Position)) + 1>
			</cfif>
			<cfset arrayAppend(result,thisResult)>
			<cfif singleOccurrence>
			<cfbreak>
			</cfif>
		</cfloop>
		<cfreturn result>
	</cffunction>

	<!---
		WAB 2012-01
		This function is used primarily to decide whether a given position in a string is inside a comment
		The position array is an array of the type returned by getComments() (which is same as reFindAllOccurrences())
		--->

	<cffunction name="isPositionInsidePositionArray" output="false">
		<cfargument name="PositionArray" type="array">
		<cfargument name="Position" type="numeric">
		<cfset var item = "">
		<cfloop array = #PositionArray# index="item">
			<cfif position gte item.position and position lte item.end>
			<cfreturn true>
			</cfif>
		</cfloop>
		<cfreturn false>
	</cffunction>

	<!---
		WAB 2012-01
		getNumberOfLineBreaks()
		Returns number of linebreaks in a string (so number of lines is this number plus 1)
		Used primarily during security upgrade so we could replace all comments in a file, but retain the correct line numbering
		--->

	<cffunction name="getNumberOfLineBreaks" output="false">
		<cfargument name="string" type="string">
		<cfreturn arrayLen(rematch("\n",string))>
	</cffunction>

	<!---
		WAB 2012-01
		Adds line numbers to the beginning of each line of a string
		--->

	<cffunction name="AddLineNumbers" output="false">
		<cfargument name="string" type="string">
		<cfargument name="start" type="numeric" default = "1">
		<cfset var result = string>
		<cfset var regularExpressions = application.com.relayTranslations.regExp>
		<cfset var regExp = "(\n)(?![0-9]*:)">
		<cfset var counter = start>
		<cfset var find =  reFind(regExp,result)>
		<cfloop condition = "find is not 0">
			<cfset result = reReplaceNoCase (result,regExp,"#chr(10)##counter#:","ONE")>
			<cfset find =  reFind(regExp,result)>
			<cfset counter ++>
		</cfloop>
		<cfreturn result>
	</cffunction>

	<!---
		WAB 2012-01
		remove whitespace from a string
		Used in OnRequestEnd
		Removes any multiple line breaks and leading whitespace
		Protects <TEXTAREA> and <PRE> tags
		--->

	<cffunction name="removeWhiteSpace" output="false">
		<cfargument name="string" type="string">
		<cfargument name="BlankLines" type="boolean" default="true">
		<cfargument name="LeadingWhiteSpace" type="boolean" default="true">
		<cfargument name="AllLineBreaks" type="boolean" default="false">
		<cfset var result = string>
		<cfset var counter = 0>
		<cfset var protectedItem = "">
		<!--- find protected tags <TEXTAREA and <PRE> --->
		<cfset var regExpArguments = {reg_expression = "<(TEXTAREA|PRE).*?<\/\1>",string = result,useJavaMatch = true}>
		<cfset var findProtected = refindAllOccurrences(argumentCollection = regExpArguments)>
		<cfloop array="#findProtected#" index="protectedItem">
			<cfset counter ++>
			<cfset result = replace(result,protectedItem.string,"*PROTECTED#counter#*")>
		</cfloop>
		<cfif blankLines>
		<cfset result = rereplace(result,"\r\n\s*(?=\r\n)","","ALL")> <!--- This removes blank lines --->
		</cfif>
		<cfif LeadingWhiteSpace>
		<cfset result = rereplace(result,"(\n)\s*","\1","ALL")>  <!--- this removes leading whitespace --->
		</cfif>
		<cfif AllLineBreaks>
		<cfset result = rereplace(result,"\r\n","","ALL")> <!--- This removes all lines breaks --->
		</cfif>
		<cfset result = rereplace(result,"\A\r\n\Z","","ALL")> <!--- If we have ended up with a single line break then get rid of it --->
		<cfset counter = 0>
		<cfloop array="#findProtected#" index="protectedItem">
			<cfset counter ++>
			<cfset result = replace(result,"*PROTECTED#counter#*",protectedItem.string)>
		</cfloop>
		<cfreturn result>
	</cffunction>

	<!--- removes whitespace from all items in a structure (which are strings) --->

	<cffunction name="removeWhiteSpaceFromStructure" output="false">
		<cfargument name="structure" type="struct">
		<cfset var key = "">
		<cfloop collection="#structure#" item="key">
			<cfif isSimpleValue (structure[key])>
			<cfset arguments.structure[key] = removeWhiteSpace(structure[key])>
			</cfif>
		</cfloop>
		<cfreturn arguments.structure>
	</cffunction>


	<cffunction name="escapeRegExp" hint="escapes characters in a string so can be used in a regular expression (may not be complete)" output="false">
		<cfargument name="string" type="string">
		<!--- WAB 2014-02-24 added brackets and then 2014-03-26 ^--->
		<cfreturn reReplaceNoCase(string, "([<!>*.?:\\\|\(\)\{\}\^])","\\\1","ALL")>
	</cffunction>

	<!--- 2012-06-22 PPB Case 429018 add replaceNonAlphaNumericChars --->

	<cffunction name="replaceNonAlphaNumericChars" output="false" >
		<cfargument name="stringToSearch" type="string" required="true">
		<cfargument name="replacementString" type="string" required="false" default="">
		<cfreturn rereplace(stringToSearch,"[^a-zA-Z0-9]",replacementString,"ALL") > 	<!--- [^[:alnum:]] would do the same but less readable --->
	</cffunction>

	<!---NJH 2012/11/04 Case 431491 - imported a function that Will created to parse a where clause --->

	<cffunction name="parseWhereClause" access="public" returnType="array" output="false">
		<cfargument name="whereClause" type="string" required="true">
		<cfset var result = []>
		<!--- started with just one regexp, but eventually broke out into two, one hardcoded to find expressions in single quotes --->
		<cfset var regExps = {}>
		<cfset var findMatches = arrayNew(1)>
		<cfset var thisMatch = "">
		<cfset var thisResult = structNew()>
		<cfset var groups="">
		<cfset var betweenFind = "">
		<cfset var regExp = '' />
		<cfset var regExpItem = '' />
		<cfset regExps.int.groupNames = {1 = "openingWhiteSpace", 2 = "sqlvariable",3="operator",4="bracket",5="expression",6="endbracket",7="nextcharacter"}>
		<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
		<cfset regExps.int.regexp = "(\s|\A|,|\()([^\s=\(]*)\s*( NOT\s*IN| IN| BETWEEN|!=|<>|>|<|>=|<=|>|=)\s*(\()?\s*([^'\s]+?)(\))?(\s)">
		<cfset regExps.varchar.groupNames = {1 = "openingWhiteSpace", 2 = "sqlvariable",3="operator",4="bracket",5="N",6="quote",7="expression",8="endquote",9="endbracket",10="nextcharacter"}>
		<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
		<cfset regExps.varchar.regexp = "(\s|\A|,|\()([^\s=\(]*)\s*( NOT\s*IN| IN| NOT\s*LIKE| LIKE| BETWEEN|!=|<>|>|<|>=|<=|>|=)\s*(\()?\s*(N)?(')([^']*?)(')\s*?(\))?(.)? ">
		<cfset arguments.whereClause = arguments.whereClause & " ">  <!--- need an extra character on the end! --->
		<cfloop collection = #regExps# item="regExpItem">
			<cfset findMatches = reFindAllOccurrences(reg_Expression=regExps[regExpItem].regExp,string=arguments.whereClause,groupnames=regExps[regExpItem].groupnames)>
			<cfloop array = "#findMatches#" index="thisMatch">
				<!--- sometimes a closing bracket is picked up when there hasn't been an opening bracket - deal
					<cfif thisMatch.endBracket is not "" and thisMatch.Bracket is "">
					<cfset thisMatch.string = replace(thisMatch.string,"#thisMatch.beforeEndBracket##thisMatch.endBracket##thisMatch.nextcharacter#","#thisMatch.beforeEndBracket##thisMatch.nextcharacter#")>
					<cfset thisMatch.endBracket = "">
					--->
				<cfset thisResult = {column=thisMatch.sqlVariable,expression=thisMatch.expression,operator=thisMatch.operator,wholestring=thisMatch.string}>
				<cfset arrayAppend(result,thisResult)>
				<cfif trim(thisMatch.operator) is "BETWEEN">
				<cfset groups= {1="expression"}>
				<cfif structKeyExists(thisMatch,"quote")>
				<cfset regExp= "AND\s?'([^']*)'">
			<cfelse>
				<cfset regExp= "AND\s?([^\s]*)">
				</cfif>
				<cfset betweenFind = reFindAllOccurrences(reg_Expression=regExp,string=arguments.whereClause,groupnames=groups,start=thisMatch.end)>
				<cfif arrayLen(betweenFind)>
				<cfset thisResult = {column=thisMatch.sqlVariable,expression=betweenFind[1].expression,operator="BETWEENAND",wholestring=betweenFind[1].string}>
				<cfset arrayAppend(result,thisResult)>
				</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<cfreturn result>
	</cffunction>


	<cffunction name="isStringHTML" access="public" returntype="boolean" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfreturn reFindNoCase("<html>|<br>|<p>|<body>",arguments.inputString,1)>
	</cffunction>

	<!---
		unHTMLEditFormat()
		WAB 2013-01-29 for CASE 433406
		A rough and ready function for replacing HTML entities with original characters.
		Was needed to deal with links which were encoded by ckeditor (so that they displayed correctly in HTML) but which we then URL encoded to be passed to remote.cfm.  When un URLencoded we were left with a URL containing HTML entities and got the addresses wrong
		--->

	<cffunction name="unHTMLEditFormat" access="public" returntype="string" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfset var characters = "&,<,>,"",',">
		<cfset var HTMLEntities = htmlEditFormat(characters)>
		<cfset var result = replaceList(inputstring,htmlEntities,characters)>
		<cfreturn result>
	</cffunction>


	<cffunction name="proper" access="public" returnType="string" hint="Capitalises the first letter of a word/s" output="false">
		<cfargument name="inputString" type="string" required="true">
		<cfreturn reReplace(arguments.inputString,"(^[a-z]|\s+[a-z])","\U\1","ALL")>
	</cffunction>

	<!--- WAB 2014-11-25 Added for CASE 442080 --->

	<cffunction name="replaceMatchedItemsWithPlaceHolders" hint="Takes and array of matches (as returned by refindAllOccurrences or getComments) and replaces those items with a placeholder" output=false>
		<cfargument name="string" type="string" required="true" >
		<cfargument name="matchedItems" type="array" required="true" hint="An array as returned by refindAllOccurrences">
		<cfargument name="placeHolderString" type="string" default="~PH">
		<cfscript>
		var result = {string = string, placeholder = placeHolderString,replacements = []};
		var counter = 0;

		arrayEach(matchedItems,function (item){
			counter ++;
			var placeholder = "#placeHolderString#_#counter#";
			result.string = left (result.string,item.position -1)   & placeholder & repeatString(" ",item.length - len(placeholder)) & right (result.string,len(result.string) - item.end) ;
				arrayAppend(result.replacements,item.string);
		});

		</cfscript>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2014-11-25 Added for CASE 442080 --->

	<cffunction name="reinstatePlaceHolders" hint="To be used alongside replaceMatchedItemsWithPlaceHolders() to reinstate items removed by that function" output="false">
		<cfargument name="string" type="string" required="true">
		<cfargument name="placeHolderStructure" type="struct" required="true" hint="This is the structure returned by replaceMatchedItemsWithPlaceHolders">
		<cfscript>
		var counter = 0;
		arrayEach(placeHolderStructure.replacements,function (item){
			counter ++;
			string = replace(string,"#placeHolderStructure.placeholder#_#counter#",item);
		});
		</cfscript>
		<cfreturn string>
	</cffunction>


	<cffunction name="replaceCommentsWithPlaceHolders" hint="Replace comments with a placeholder.  A wrapper for replaceMatchedItemsWithPlaceHolders" output="false">
		<cfargument name="string" type="string">
		<cfargument name="Type"  default="CF">
		<cfargument name="placeHolderString" type="string" default="~PH">
		<cfscript>
		var matchedItems =  getComments (string = string, type= type );
		var result = replaceMatchedItemsWithPlaceHolders (string = string, matchedItems = matchedItems, placeHolderString = placeHolderString);
		</cfscript>
		<cfreturn result>
	</cffunction>

	<!--- WAB 2016-02-08 for BF-409
		Functions for testing a string against a coldfusion mask
		See data\persondetail.cfm for use
		see \test\unit\com\regexp\mask.cfc for unittest
		--->

	<cffunction name="convertMaskToRegularExpression">
		<cfargument name="mask">
		<cfset var regExp = mask>
		<cfset  regExp = escapeRegExp(mask)>
		<cfset 	regExp = replace (regExp,"9","\d","ALL")>
		<cfset 	regExp = replace (regExp,"A","[A-Za-z]","ALL")>
		<cfset 	regExp = replace (regExp,"X","\w","ALL")>
		<cfset 	regExp = replace (regExp,"\?",".","ALL")>
		<cfset 	regExp = "\A" & regExp & "\Z">
		<cfreturn regExp>
	</cffunction>


	<cffunction name="doesStringMatchMask">
		<cfargument name="string">
		<cfargument name="mask">
		<cfreturn refindNoCase(convertMaskToRegularExpression(mask),string)>
	</cffunction>

	<!--- 2017-03-06 PPB RT-325 SUSE page formatting - new func developed by WAB --->
	<cffunction name = "escapeNonHTMLChevrons" hint="HTMLEditFormats any opening chevrons which do not appear to be part of an HTML tag">
		<cfargument name="stringIn" type="string">

		<cfset var re = "<(?!(/)?([A-Z])[^<]*?>)">  <!--- <cfset re = "<(?!(/)?(BR|B|I|A|/)[^<]*?>)"> --->

		<cfset var result = rereplacenocase (arguments.stringIn, re, "&lt;", "ALL")>

		<cfreturn result>
	</cffunction>

</cfcomponent>
