<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-06-28 PPB Case 428165 switched from RIGHT to LEFT joins in getFlagCounts()  
--->

<cfcomponent displayname="relay" hint="componentFunction">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getFlagCounts" access="public" returntype="struct" hint="Returns a query containing profile attributes for supplied flagGroupID and the number of records">
		<cfargument name="flagGroupID" type="numeric" required="yes">
		<cfargument name="countryID" type="numeric" default="0">
		<cfargument name="cacheQueriesForMins" type="numeric" default="#application.cachedReportTimeShortInMins#">
		<cfargument name="IncludeUnpopulatedData" type="boolean" default="false">
				
		<cfscript>
			var getFlagCounts = "";
			var cachedForMins = arguments.cacheQueriesForMins;
			var strToReturn = structNew();
		</cfscript>
		
		<cfset getflagGroup = application.com.flag.getFlagGroupStructure(flaggroupid)>
		
		<CFIF not request.relayCurrentUser.isInternal or not arguments.IncludeUnpopulatedData> 
			<!--- Get GrandTotal of records for flagGroup--->
			<cfquery name="getFlagCountTotal" datasource="#application.sitedatasource#" cachedwithin="#CreateTimeSpan(0, 0, cachedForMins, 0)#">
				SELECT COUNT(fd.entityid) AS Total
					FROM  #getFlagGroup.flagtype.dataTableFullName# fd
					INNER JOIN flag f 
					ON f.flagid = fd.flagid 
					WHERE
						F.name is not null
						AND F.FlagGroupID=#arguments.flaggroupid#
						AND F.Active = 1
			</cfquery>
		<CFELSE>
			<CFQUERY NAME="getFlagCountTotal" datasource="#application.sitedatasource#" cachedwithin="#CreateTimeSpan(0, 0, cachedForMins, 0)#">
				SELECT COUNT(e.#getFlagGroup.entityType.UniqueKey#) AS Total
				FROM #getFlagGroup.entityType.tablename# e 
			</CFQUERY>
		</CFIF>

		<cfset strToReturn.Total = 	getFlagCountTotal.total>
		
		<cfif arguments.IncludeUnpopulatedData>
			<CFQUERY NAME="getFlagCounts" datasource="#application.sitedatasource#" cachedwithin="#CreateTimeSpan(0, 0, cachedForMins, 0)#">
				SELECT 
				isNULL(Flag.Name,'No Data') as Name, 
					Flag.FlagID, Flag.orderingIndex,
					Count(e.#getFlagGroup.entityType.UniqueKey#) AS Total,
					Fraction=cast(((Convert(Real, Count(e.#getFlagGroup.entityType.UniqueKey#))/#getFlagCountTotal.Total#)*100) as int)
				FROM #getFlagGroup.flagType.dataTableFullName# fdt 
				INNER JOIN Flag ON fdt.FlagID = Flag.FlagID and Flag.FlagGroupID=#arguments.flagGroupID#
				LEFT OUTER JOIN 	<!--- 2012-06-28 PPB Case 428165 switched from RIGHT to LEFT join  --->
				<!---2006/07/07 GCC tweaked - brackets borke single join --->
				<cfif getFlagGroup.entityType.tableName neq "person">
				#getFlagGroup.entityType.tableName# e  
				<cfelse>
				( #getFlagGroup.entityType.tableName# e  inner join location l on l.locationid = e.locationid)
				</cfif>				
				ON fdt.EntityID = e.#getFlagGroup.entityType.UniqueKey#
				<cfif arguments.countryID neq 0>
					AND countryID = #arguments.countryID#
				</cfif>
				GROUP BY Flag.orderingIndex, Flag.Name, flag.FlagID
				ORDER by Flag.orderingIndex
			</CFQUERY>
		<cfelse>
			<CFQUERY NAME="getFlagCounts" datasource="#application.sitedatasource#" cachedwithin="#CreateTimeSpan(0, 0, cachedForMins, 0)#">
				SELECT 
					isNULL(Flag.Name,'No Data') as Name, 
					Flag.FlagID, Flag.orderingIndex,
					Count(e.#getFlagGroup.entityType.UniqueKey#) AS Total,
					Fraction=cast(((Convert(Real, Count(e.#getFlagGroup.entityType.UniqueKey#))/#getFlagCountTotal.Total#)*100) as int)
				FROM #getFlagGroup.flagType.dataTableFullName# fdt 
				INNER JOIN Flag ON fdt.FlagID = Flag.FlagID and Flag.FlagGroupID=#arguments.flaggroupid#
				LEFT OUTER JOIN		<!--- 2012-06-28 PPB Case 428165 switched from RIGHT to LEFT join  ---> 
				<!---2006/07/07 GCC tweaked - brackets borke single join --->
				<cfif getFlagGroup.entityType.tableName neq "person">
				#getFlagGroup.entityType.tableName# e  
				<cfelse>
				( #getFlagGroup.entityType.tableName# e  inner join location l on l.locationid = e.locationid)
				</cfif>
				ON fdt.EntityID = e.#getFlagGroup.entityType.UniqueKey#
				WHERE Flag.Name is not null
				<cfif arguments.countryID neq 0>
					AND countryID = #arguments.countryID#
				</cfif>
				GROUP BY Flag.orderingIndex, Flag.Name, flag.FlagID
				ORDER by Flag.orderingIndex
			</CFQUERY>
		</cfif>
		<!--- add the query data to the structure to return --->
		<cfset strToReturn.queryData = 	getFlagCounts>
		
		<cfreturn strToReturn>
	</cffunction>

</cfcomponent>
