<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2010/10 For Version 8.3
This cfc is an attempt to rationalise all of our merge functionality and end up with a standard syntax
for communications, system emails and translations.
Functions should no longer need to be passed personids, since the code will be able to automatically get the currentUser or the email recipient personid
It will hopefully lead to easier use of these functions within the content editor
It also gets around the problems we have had with localisation of variables in previous merge code

This cfc has the core functionality
mergeFunctions.cfc has the actual merge functions

WAB 2010/05/05 	LID 6501 - changes to finish integrating emailMerge.cfc functions into the code (which I had forgotten to do)
WAB 2011/06/29  For Comms Merging, created a version of the checkForAndEvaluate.... function which brings back an error structure.  This can be used when sending comms to abort if there is a merge error of any sort
WAB 2011/07   	(with NJH) created a separate evaluateAString() function which is called in a couple of places
				Added in code to allow automatic calculation of ABCD.WXYZ by calling function getABCD() when ABCD.WXYZ does not exist
NYB 2011/08/19	P-LEN024 changed checkForAndEvaluateCFVariables_ReturningStructure to add OmnitureTracking
NYB	2011-08-25 	LHID7571 - added acceptHashes so the system can choice to allow hashes in email content
WAB 2011/10/10  moved omniture tracking code from here to tskcommsend
				managed to get rid of need for acceptHashes - now dealt with automatically
WAB 2011/11/08 doRelayLoop - deal with errors better
WAB 2011/11/16  evaluateAString() fixed problem looping out of control if a function and variable have same name
				getPerson() added ability to pass in a small person structure and have it extended if an extra field required
				seemed to be confusion as to whether communication.siteURL or communication.linksToSiteURL - came down on side of latter, since that is field in db
WAB 2011/12/05	make initialiseMergeStruct() return this
WAB 2012-03-14  Implement merge via CFM file
IH	2012/06/18	Case 428852 use collection loop to prevent issues when struct key contains comma
WAB	2012-10-03	knock on from CASE 431031 changed attribute dontSendEvaluationErrorWarningEmail to logMergeErrors (and NOT'ed)
WAB 2012-11-13  CASE 431561 - deal with infinite loop if merge field happens to return something which is not a string  (could occur for example if someone puts [[person]] as a merge field )
WAB 2013-01-06 	CASE 432644 Deal with Relay_IF regExp falling over if string very long.  Got rid of regExp and implemented some primitive parsing
WAB 2013-01-15	Comms Improvements. Added in identifyMergeFields() config option.  Used to work out what merge fields exist in a phrase. .  This allowed me to make sure that my commDetail query contained all the fields needed.  All are returned in the errorArray, and we don't do the automatic calling of the  getENTITYNAME function
								  	Added in evaluateAllOptions() config option.  Allows a merge to be 'tested', all branches of relay_IF statements are evaluated.
									the changes led me to to change the way that merge errors are passed around - now each function returns an array of merge errors rather than a single error string
WAB 2013-04-24 CASE 434850 add support for [[IF]] notation (both for merge by file and normal merge)
WAB 2013-06-11	Merge by file, replace CR and LF entities
PPB 2013-10-04 	Case 437262 defence against request.relaycurrentuser.person not existing
WAB 2013-10-23 	Log full CF error when error in a relayTag and added errorid to message
WAB 2013-11-28	2013RoadMap2 Item 25 Altered getCommunication() so that it can load a full communication given variables.commid
WAB 2013-11-28	Removed a bit of code labelled 2013-08-15  YMA   Case 436313.  Wouldn't have worked (contained an error) and requirement dealt with by above change
WAB	2014-09-03	Merging of Non POL Fields and Flags (building on some recent YMA changes).
				Added generic getEntity and getEntityID functions.
				Now supports [[entity.flagName]] notation.  Eventually we will hopefully go down a real object route.
WAB 2015-01-28	(committed 2015-03-11) Support for [[entity.flagName]] notation on all objects rather than just custom entities
WAB 2015-11-27 	During CASE 446665 was not working properly evaluating variables of form  AAAA.BBBB.CCCC if this was the first reference to AAAA
PYW	2015-12-23	P-TAT006. BRD 31 Add getTrngPersonCertification
NJH	2016/04/27	Sugar 449184 - replaced replaceNoCase with replace. Issue with Turkish translations.
DAN 2016-05-05  Case 449024 - add a function for fundActivity merge fields
WAB	2016-10-06	FormRenderer Add support for EntityObjects.   Including for relayCurrentUser
				Involves parsing out the merge strings and replacing    object.xxx.yyy with object.get('xxx.yyy')
WAB 2017-01-03	Need to check that object is an instance of entity.cfc
WAB 2017-01-30	Person/Location/Organisation now always entityObjects whether come from currentUser or build on the fly
				Some changes to getPerson/getLocation/getOrganisation to embed the logic of finding one given the other
--->

<cfcomponent cache="false">
	<cfset variables.merge = {}>

	<!--- because a copy of this object is cached in relayTranslations, if there are any changes to this object we need to tell relayTranslations to reload.  This will get picked up when reloadApplication components called - mainly of use on dev sites --->
	<cfif isdefined("application.com.relayTranslations")>
		<cfset application.com.relayTranslations.dropCachedMergeObject()>
	</cfif>

	<cfset this.config = {evaluateAllOptions = false, identifyMergeFields = false}>

	<!--- WAB - an idea for reusing the mergeStruct rather than recreating.  Could save a few 10's of ms when doing thousands of merges such as in comms --->
	<cffunction name="reinitialiseMergeStruct">
		<cfargument name  = "mergeStruct" default = "#structNew()#">

		<cfset var key = "">
		<!--- delete everything in variables scope and then initialise again --->
		<cfloop item="key" collection = "#variables#">
			<cfif not isCustomFunction (variables[key]) and key is not "this">
				<cfset structDelete(variables,key)>
			</cfif>
		</cfloop>
		<cfset initialiseMergeStruct (mergeStruct = mergeStruct)>
		<cfreturn this>
	</cffunction>

	<cffunction name="initialiseMergeStruct">
		<cfargument name  = "mergeStruct" default = "#structNew()#">

		<cfset var key = "">

		<cfparam name="arguments.mergeStruct.context" default = "">
		<cfset variables.merge = mergeStruct>

		<!--- alias func and user for easier syntax --->
		<!--- func just there for backwards compatibility --->
		<cfset variables.func =  this>
		<cfif StructKeyExists(request,"relaycurrentuser") >  
			<cfset variables.user = request.relaycurrentuser>
		</cfif>

		<!--- WAB don't set these yet, only get them when required
		<cfif StructKeyExists(request.relaycurrentuser,"person") AND mergeStruct.context is "">  <!--- so for emaildefs and communications person, location etc. come from elsewhere ---> <!--- 2013-10-04 PPB Case 437262 defence against request.relaycurrentuser.person not existing --->
			<!---  I'm doing this before the mergeStruct is localised so that these values can be overwritten if necessary --->
			<cfset variables.user = request.relaycurrentuser>
			<cfset variables.person = request.relaycurrentuser.person>
			<cfset variables.location = request.relaycurrentuser.location>
			<cfset variables.organisation = request.relaycurrentuser.organisation>
		</cfif>
		 --->

		<!--- localise any arguments which aren't our normal ones, not sure this works any more --->
		<cfloop item="key" collection = #arguments#>
			<cfif listfindnocase("phraseText,phraseTextID,mergeStruct,dontSendEvaluationErrorWarningEmail",key) is 0 >
				<cfset variables[key] = arguments[key]>
			</cfif>
		</cfloop>

		<!--- localise each key in the mergeStruct
		--->
			<cfloop item="key" collection = #mergeStruct#>
				<cfif listfindnocase("this",key) is 0 >
					<cfset variables[key] = mergeStruct[key]>
				</cfif>
			</cfloop>

		<cfreturn this>
	</cffunction>


	<cffunction name="appendMergeStruct">
		<cfargument name  = "mergeStruct" default = "#structNew()#">	

		<cfloop item="key" collection = #mergeStruct#>
			<cfif listfindnocase("this",key) is 0 >
				<cfset variables[key] = mergeStruct[key]>
			</cfif>
		</cfloop>
		
	</cffunction>


	<cffunction name="evaluateAllOptions">
		<cfargument name  = "truefalse" default = "true">

		<cfset this.config.evaluateAllOptions  = trueFalse>

	</cffunction>

	<!--- WAB	2014-09-03	Merging of Non POL Fields and Flags.  Added these generic functions  --->
	<cffunction name="getEntityID">
		<cfargument name="entityType">

		<cfset var entityTypeStructure = application.entityType[application.entityTypeID[entityType]]>
		<cfset var result = 0>
		<!--- WAB 2015-01-28 changed order of looking for entityid, look for entityName structure first, then entityTypeID/entityID pair, then in variables[entityUniqueKey]--->
		<cfif structKeyExists(variables,entityTypeStructure.tablename)>
			<cfset result =  variables[entityTypeStructure.tablename][entityTypeStructure.uniqueKey]>
		<cfelseif structKeyExists(variables,"entityTypeID") and variables.entityTypeID is entityTypeStructure.entityTypeID and structKeyExists(variables,"entityID") >
			<cfset result = variables.entityID>
		<cfelseif structKeyExists(variables,entityTypeStructure.uniqueKey)>
			<cfset result =  variables[entityTypeStructure.uniqueKey]>

		<!--- NJH 2016/09/20 JIRA PROD2016-379 - if in test mode (ie. testing sending of emails) then grab a 'dummy' ID to use for the population of merge fields. --->
		<cfelseif structKeyExists(variables,"test") and variables.test>

			<cfset var getMaxEntityID = "">
			<cfquery name="getMaxEntityID">
				select max(#entityTypeStructure.uniqueKey#) as maxID from #entityTypeStructure.tablename#
			</cfquery>
			<cfset result = getMaxEntityID.maxID>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getEntity" returntype="struct">
		<cfargument name="entityType" type="string" default="">
		<cfargument name="fieldName" type="string" default="">

		<cfset var result = {}>
		<cfset var entityStructure = {}>
		<cfset var fieldResult = "">

		<cfif not structKeyExists (variables,EntityType)>
			<!--- base entity structure does not exist, so get it.
				Either from a custom function or a generic one
			 --->
			<!---
				NJH 2016/09/21 - start of work where we set the uniqueKey via the getEntityId function. Not going to do it now as don't have time to test
			<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
			<cfif structKeyExists (variables,"get#entityTypeStruct.uniqueKey#")>
				<cfset var getEntityIDFunction = variables["get#entityTypeStruct.uniqueKey#"]>
				<cfset variables[entityTypeStruct.uniqueKey] = getEntityIDFunction (argumentCollection = arguments)>
			</cfif> --->

			<cfif structKeyExists (variables,"get#EntityType#")>
				<cfset var theEntityFunction = variables["get#EntityType#"]>
				<cfset entityStructure = theEntityFunction (argumentCollection = arguments)>
			<cfelse>
				<cfset entityStructure = getGenericEntityStructure(entityType = entityType, entityID = getEntityID(entityType))>
				<cfset variables[entityType] = entityStructure>
			</cfif>
		<cfelse>
			<cfset entityStructure = variables[entityType]>
		</cfif>

		<!--- WAB 2016-11 Add support for entityObjects --->
		<cfif isObject (entityStructure)and isInstanceOf (entityStructure,'entities.entity')>
			<!--- WAB TBD Actually may not even need to to a get here, should be done automatically now  --->
			<cfset entityStructure.get(fieldName)>
		<cfelse>
	
			<!--- now check that the required field exists, if not then it may be flag/flagGroup --->
			<cfif fieldname is not "" and not structKeyExists (entityStructure, fieldname)>
				<!--- field may be a flag - lets check --->
				<cfset var fieldStructure = application.com.relayEntity.getTableFieldStructure(tablename=entityType,fieldname=fieldname)>
				<cfif fieldStructure.isOK>
					<!--- WAB 2015-01-28 sometimes the entity structure is actually a query ( eg if it has come from relaycurrentuser.person), convert to structure so that can be appended to --->
					<cfif isQuery (entityStructure)>
						<cfset variables[entityType] = application.com.structureFunctions.queryRowToStruct (entityStructure,1)>
						<cfset entityStructure = variables[entityType]>
					</cfif>
					<cfif fieldStructure.fieldType is "flag">
						<cfset fieldResult = showProfileAttributeValue(fieldname)>
						<cfset entityStructure[fieldname] = fieldResult>
					<cfelseif fieldStructure.fieldType is "flaggroup">
						<cfset fieldResult = showProfileValue(fieldname)>
						<cfset entityStructure[fieldname] = fieldResult>
					<cfelseif fieldStructure.fieldType is "field">
						<!--- must be a core field which wasn't in the original structure --->
						<cfset var tempEntityStructure = getGenericEntityStructure(entityType = entityType, entityID = getEntityID(entityType))>
						<cfset structAppend(EntityStructure,tempEntityStructure,false)>
					</cfif>
	
				</cfif>
	
			</cfif>

		</cfif>

		<cfreturn entityStructure>

	</cffunction>


	<cffunction name="getGenericEntityStructure" returntype="struct">
		<cfargument name="entityType" type="string" default="">
		<cfargument name="entityID" type="string" default="">

			<cfset var EntityTypeStructure = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
			<cfset var entityStructure = {}>
			<cfset var getEntityDetails = "">
			<cfquery name="getEntityDetails">
				select
					e.*,
					case when p.personid is not null then isNull(p.firstname,'')+' '+isNull(p.lastname,'') else 'Unknown' end as lastUpdatedFullName
				from #EntityTypeStructure.tablename# e
					left join person p on p.personID = e.lastUpdatedByPerson
				where e.#EntityTypeStructure.uniqueKey# = <cf_queryparam value="#EntityID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfset entityStructure = application.com.structureFunctions.queryRowToStruct(query=getEntityDetails,row=1)>
			<cfset entityStructure.lastUpdatedBy = entityStructure.lastUpdatedFullName>
			<cfset entityStructure.lastUpdated = DateFormat(entityStructure.lastUpdated,"dd-mmm-yyyy")>
			<cfset entityStructure.created = DateFormat(entityStructure.created,"dd-mmm-yyyy")>

		<cfreturn entityStructure>

	</cffunction>

	<cffunction name="getPersonID">
		<cfif structKeyExists(variables,"personid")>
			<cfreturn variables.personid>
		<cfelse>
			<cfreturn user.personid>
		</cfif>
	</cffunction>

	<cffunction name="getLocationID">
		<cfif structKeyExists(variables,"locationid")>
			<cfreturn variables.locationid>
		<cfelseif structKeyExists(variables,"personid")>
			<cfquery name = "local.location">
			select Locationid from person where personid= #variables.personid#
			</cfquery>
			<cfreturn local.location.locationid>
		<cfelseif structKeyExists(variables,"person")>
			<cfreturn variables.person.locationid>
		<cfelse>
			<cfreturn user.locationid>
		</cfif>
	</cffunction>

	<cffunction name="getOrganisationID">
		<cfif structKeyExists(variables,"Organisationid")>
			<cfreturn variables.Organisationid>
		<cfelseif structKeyExists(variables,"Locationid")>
			<cfquery name = "local.organisation">
			select organisationid from location where locationid = #variables.locationid#
			</cfquery>
			<cfreturn local.organisation.Organisationid>
		<cfelseif structKeyExists(variables,"personid")>
			<cfquery name = "local.organisation">
			select organisationid from person where personid= #variables.personid#
			</cfquery>
			<cfreturn local.organisation.Organisationid>
		<cfelseif structKeyExists(variables,"person")>
			<cfreturn variables.person.organisationid>
		<cfelseif structKeyExists(variables,"location")>
			<cfreturn variables.location.organisationid>
		<cfelse>
			<cfreturn user.Organisationid>
		</cfif>
	</cffunction>

	<cffunction name="getCommunication">

		<cfif not structKeyExists(variables,"communication")>
			<!--- WAB 2013-11-28 2013RoadMap2 Item 25.  Added ability to load communication from variable.commid - used for merging of comm/message titles within contact history --->
			<cfif structKeyExists(variables,"commid")>
				<cfset variables.communication = application.com.communications.getCommunicationObject(variables.commid)>
			<cfelse>
				<cfset variables.communication ={commid=0,linkTositeURL=application.com.relaycurrentsite.getReciprocalExternalProtocolAndDomain(),test=0}>
			</cfif>

		</cfif>

		<cfif not structKeyExists (variables.communication,"linktoSiteURLWithProtocol")>
			<cfset variables.communication.linktoSiteURLWithProtocol = application.com.relaycurrentsite.getReciprocalExternalProtocolAndDomain()>
		</cfif>


		<cfreturn variables.communication>
	</cffunction>


	<!--- WAB 2015-12-03	CASE 446541 Added queryRowToStruct --->
	<cffunction name="getLocation">
		<cfset var entityStruct = "">
		<cfif not structKeyExists(variables,"location")>  
			<cfset var LocationID = getLocationid()>
			<cfif LocationID is user.location.locationid>
				<cfset variables.location = user.location>
			<cfelse>
				<cfset variables.location = application.com.entities.load ("location",locationid)>
			</cfif>
		</cfif>

		<cfreturn variables.location>
	</cffunction>


	<!--- WAB 2015-12-03	CASE 446541 Added queryRowToStruct --->
	<cffunction name="getOrganisation">
		<cfargument name="field" default="">

		<cfset var entityStruct = "">

		<cfif not structKeyExists(variables,"Organisation")>  <!--- set in email.cfc --->
			<cfset var organisationID = getOrganisationID()>
			<cfif organisationID is user.organisationID>
				<cfset variables.organisation = user.organisation>
			<cfelse>
				<cfset variables.Organisation = application.com.entities.load ('organisation',organisationID)>
			</cfif>
		</cfif>

		<cfreturn variables.Organisation>
	</cffunction>

	<cffunction name="getPerson">
		<cfargument name="field" default="">

		<!---
		WAB 2011/11/16 added test for person.partialStructure.
		This allows me to pass in from tskCommSend a small person structure with the most common fields in it
		If a less common field is then requested then we can go off and do a query.
		This improves performance by not having to do individual entity queries for each merge
		 --->
		<cfif structKeyExists(variables,"person")>
			<cfif structKeyExists (variables.person,"partialStructure")>  <!--- set in email.cfc --->
				<cfset var entityStruct = application.com.relayentity.getEntityStructure(entityTypeID = 0,entityid = getpersonid(),getEntityQueries=true)>
	
				<cfparam name="variables.person" default="#structNew()#">
				<cfset structAppend(variables.person,application.com.structureFunctions.queryRowToStruct(query = entityStruct.queries.person, row=1),false)>
				<cfset structDelete(variables.person,"partialStructure")>
	
				<cfparam name="variables.location" default="#structNew()#">
				<cfset structAppend(variables.location,application.com.structureFunctions.queryRowToStruct(query = entityStruct.queries.location, row=1),false)>
				<cfset structDelete(variables.location,"partialStructure")>
	
				<cfparam name="variables.organisation" default="#structNew()#">
				<cfset structAppend(variables.organisation,application.com.structureFunctions.queryRowToStruct(query = entityStruct.queries.organisation, row=1),false)>
				<cfset structDelete(variables.organisation,"partialStructure")>
			</cfif>
		
		<cfelse>
			<cfset var personid = getPersonID()>
			
			<cfif personid IS user.personid>
				<cfset variables.person = user.person>
			<cfelse>
				<cfset variables.person = application.com.entities.load ('person', personID)>
			</cfif>

		</cfif>

		<cfreturn variables.person>
	</cffunction>

	<cffunction name="getOpportunity" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.opportunityId = variables.opportunityID>
		<cfif structKeyExists(variables,"opportunity")>
			<cfset arguments.opportunity = variables.opportunity>
		</cfif>

		<cfset variables.opportunity = application.com.opportunity.getOpportunityMergeFields(argumentCollection=arguments)>
		<cfreturn variables.opportunity>
	</cffunction>

	<!--- 2015-08-05 AHL Incentives Merge Fields --->
	<cffunction name="getOrder">
		<cfargument name="field" default="">
		<cfargument name="order" required="false">

		<cfset arguments.orderId = variables.orderID>
		<cfif structKeyExists(variables,"order")>
			<cfset arguments.order = variables.order>
		</cfif>

		<cfset variables.order = application.com.relayOrder.getOrderMergeFields(argumentCollection=arguments)>

		<cfreturn variables.order>
	</cffunction>

	<cffunction name="getLead" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.leadID = variables.leadID>
		<cfif structKeyExists(variables,"lead")>
			<cfset arguments.lead = variables.lead>
		</cfif>

		<cfset variables.lead = application.com.relayLeads.getLeadMergeFields(argumentCollection=arguments)>
		
		<cfreturn variables.lead>
	</cffunction>


	<cffunction name="getQuizTaken" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.quizTakenID = variables.quizTakenID>
		<cfset variables.quizTaken = application.com.relayQuiz.getQuizTakenMergeFields(argumentCollection=arguments)>

		<cfreturn variables.quizTaken>
	</cffunction>


	<cffunction name="getEvent" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.eventFlagTextID = variables.flagID>
		<cfset variables.eventDetail = application.com.relayEvents.getEventDetails(argumentCollection=arguments)>

		<cfreturn variables.eventDetail>
	</cffunction>


	<cffunction name="getModule" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.moduleID = variables.moduleID>
		<cfset variables.module = application.com.relayElearning.getModuleMergeFields(argumentCollection=arguments)>

		<cfreturn variables.module>
	</cffunction>

	<cffunction name="getCertification" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.certificationID = variables.certificationID>
		<cfset variables.certification = application.com.relayElearning.getCertificationMergeFields(argumentCollection=arguments)>

		<cfreturn variables.certification>
	</cffunction>

	<!--- 2015-12-23 PYW P-TAT006. BRD 31 Add getTrngPersonCertification --->
	<cffunction name="getTrngPersonCertification" returntype="struct">
		<cfargument name="fieldName" type="string" default="">

		<cfset arguments.personCertificationID = variables.personCertificationID>
		<cfset variables.trngPersonCertification = application.com.relayElearning.getTrngPersonCertificationMergeFields(argumentCollection=arguments)>

		<cfreturn variables.trngPersonCertification>
	</cffunction>

<!---
checkForAndEvaluateCFVariables

A function used to check whether a phrase has any embedded cf variables and if so to evaluate the whole thing
This is called in a few places in cf_translate

WAB 2005-10-19
From code in cf_translate with modifications to handle [[ ]] notation


WAB 2006-02-20
checkForCFVariables
function to return true or false based on same regular expressions used in checkForAndEvaluateCFVariables
allows me to check phrases once and remember whether they need to be evaluated


WAB 2009/03/04 pulled out these regExp into variables
changed regExpHash from ##([a-zA-Z_][[:graph:]]*)##  to ##([a-zA-Z_][A-Za-z0-9_.]*)## to that only picks up valid CF variable names, not bits of HTML with all sorts of characters

 --->

	<cffunction name="checkForAndEvaluateCFVariables">
		<cfreturn checkForAndEvaluateCFVariables_ReturningStructure(argumentCollection = arguments).phraseText>
	</cffunction>


	<!--- WAB 2013/01/15 for Comms Work
	This function allows us to work out what merge fields are used in a phrase.
	This is useful in Communications because it allows us to query all merge fields and put then in the mergeStruct, rather than relying on the functions such as getPerson() to go off and get entity fields (which is slow)
	It uses a combination of recognising merge fields like [[xxx]] and picking up evaluation errors
	We have to pick up evaluation errors to deal with fields like [[relay_if person.sex is "M"]] (because we would not pick up person.sex, only [[person.sex]] )
	We also evaluate all cases within IF statements  (evaluateAllOptions)
	--->

	<cffunction name="identifyMergeFields">
		<cfset var result = {}>
		<cfset var errorArray = "">
		<cfset var i = 0 >

		<cfset this.config.identifyMergeFields  = true>
		<cfset evaluateAllOptions(true)>
		<cfset errorArray =  checkForAndEvaluateCFVariables_ReturningStructure(argumentCollection = arguments).errorArray>
			<CFLOOP array = #errorArray# index="i">
				<cfif structKeyExists (i,"fieldname") and not isBoolean(i.fieldname)>
					<cfset result[i.fieldname] = "">
				</cfif>
			</CFLOOP>
		<cfreturn result>

	</cffunction>

	<cffunction name="checkForAndEvaluateCFVariables_ReturningStructure">
		<cfargument name="phraseText" required="true">
		<cfargument name="phraseTextID" default = "">
		<cfargument name="logMergeErrors" default = "true">
		<cfargument name="executeRelayTags" default = "true">

		<cfset var result = "">
		<cfset var warningStructure = "">

		<cfset result = doRelayIFAndRelayLoopAndBracketsAndTags(phraseText = removeCFComments(phraseText), executeRelayTags = executeRelayTags)>

		<cfif logMergeErrors and arrayLen(result.errorArray)>
			<!--- Log a warning of evaluation errors 
					WAB 2017-01-24 RT-188 Add errorID to the result
			--->
			<cfset warningStructure= {Original_Text =phraseText,errors = result.errorArray, Final_Text = result,phrasetextID = phrasetextID,mergeStruct=merge}>
			<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning (Type="Merge Error",Message="Merge Error: " & phraseTextID,warningStructure=warningStructure,TTL=30 )>
		</cfif>
		<!--- <cfset result = executeRelayTags(phrasetext = result)> --->

		<cfreturn result>
	</cffunction>


	<!--- This removes anything between CF comments, handles nested comments.  Won't change the display, but could allow you to comment out code which is failing --->
	<cffunction name="removeCFComments">
		<cfargument name="phraseText">
		<cfset var result = phraseText>
		<cfset var regExp = application.com.relayTranslations.regExp>
		<cfset var find =  reFindNoCase(regExp.Comments,result,0,true)>
		<cfloop condition = "find.pos[1] is not 0">
			<cfset result = reReplace (result,regExp.Comments,"")>
			<cfset find =  reFindNoCase(regExp.Comments,result,0,true)>
		</cfloop>

		<cfreturn result>
	</cffunction>



	<cffunction name="doRelayIFAndRelayLoopAndBracketsAndTags">
		<cfargument name="phraseText">
		<cfargument name="executeRelayTags" default = "true">
		<cfargument name="debug" default="false">

		<cfset var result = {phraseText = phraseText,errorArray = arrayNew(1)}>
		<cfset var loopresult = {phraseText = phraseText}>
		<cfset var regExp = application.com.relayTranslations.regExp>
		<cfset var groups ={1="SquareBracketsExpression",2="hashExpression",3="LoopParams",4="LoopInnerText",5="relaytagname",6="relayTagParameters"}>
		<cfset var combinedRegExp =  "">
		<cfset var tempRegExpArgs = "">
		<cfset var find = "">
		<cfset var parsedRelayIF = "">
		<cfset var translatedPhrase = "">

		<cfset combinedRegExp =  "#regExp.SquareBrackets#|#regExp.hashedVariable#|#regExp.relayLoop#">
		<cfif executeRelayTags>
			<cfset combinedRegExp =  listAppend(combinedRegExp,RegExp.relayTag,"|")>
		</cfif>
		<cfset tempRegExpArgs = {reg_expression = combinedRegExp,string = result.phraseText,groupnames = groups,start = 1,usejavamatch=false}> <!--- WAB Note doesn't work properly using javamatch, haven't investigated yet--->
		<cfset find =  application.com.regExp.refindSingleOccurrence(argumentCollection = tempRegExpArgs)>

		<cfloop condition ="arrayLen(find)">
			<cfif debug><cf_log file="Merge" text="Start #left(find[1].squarebracketsexpression,30)#"></cfif>
			<!--- WAB 2013-04-24 add support for [[IF]] notation --->
			<cfif left(find[1].SquareBracketsExpression,3) is "IF " OR find[1].relayTagName is "relay_if">
				<cfset parsedRelayIF = parseRelayIF(result.phraseText)>
				<cfset loopresult = doRelayIF (phrasetext = result.phraseText,wholeTag = parsedRelayIF.wholeTag, tagsArray = parsedRelayIF.tagsArray)>
			<cfelseif find[1].SquareBracketsExpression is not "" >
				<cfset loopresult = doCFVariableOrFunction (phrasetext = result.phraseText,expression = find[1].SquareBracketsExpression,wholeTag = find[1].string)>
			<cfelseif find[1].hashExpression is not "" >
				<cfset loopresult = doCFVariableOrFunction (phrasetext = result.phraseText,expression = find[1].hashExpression,wholeTag = find[1].string)>
			<cfelseif find[1].loopParams is not "">
				<cfset loopresult = doRelayLoop (phrasetext = result.phraseText,params = find[1].LoopParams, InnerText = find[1].LoopInnerText,wholeTag = find[1].string)>
			<cfelseif find[1].relayTagName is not "">
				<cfset loopresult = doRelayTag (phrasetext = result.phraseText,tagname = find[1].relayTagName, parameters = find[1].relayTagParameters,wholeTag = find[1].string)>
			</cfif>

			<cfset result.phraseText = loopresult.phraseText>

			<!---
				Now we search for another merge field
				Question is, how do we deal with nested merge fields.
				We always end up evaluating the inner merge field first
				So if we continue searching from this position we will miss the outer merge field
				We could
				1. always start the search from position 1 - is this inefficient?
				2. get to the end and then do a final check

				2012-04-19 WAB CASE 426548 I am going to go for option 1
			--->
			<cfset tempRegExpArgs.string = result.phraseText>
			<cfset tempRegExpArgs.start = 1>  <!--- was find[1].position ie start from position of previous match, not position + length --->
			<cfif arrayLen(loopresult.errorArray)>
				<cfset result.errorArray = application.com.structureFunctions.arrayMerge(result.errorArray,loopresult.errorArray)>
			</cfif>
			<cfif debug><cf_log file="Merge" text="End #left(find[1].squarebracketsexpression,30)#"></cfif>

			<cfset find =  application.com.regExp.refindSingleOccurrence(argumentCollection = tempRegExpArgs)>

		</cfloop>

		<cfreturn result>
	</cffunction>



		<!---
		ParseRelayIF() WAB 2013-01-06 CASE 432644
		Implemented new method of dealing with relayIFs using primitive parsing
		WAB 2013-04-24 add support for [[IF]] notation
		--->

	<cffunction name="parseRelayIF">
		<cfargument name="string">

		<cfset var result ={}>
		<cfset var findStartAndEndOfIF = '' />
		<cfset var groupnames = '' />
		<cfset var parseIfandElses = '' />

		<!--- 	Finds the start and end of the relayIF block
				Also returns an array indicating where there are nested relayIFs.
				This array can be fed back into refindAllOccurrences when we are looking for the elseifs and else, so that we do not find any nested ones of these
		--->
		<cfset findStartAndEndOfIF = application.com.regexp.getMatchedOpeningAndClosingTags (string = string,openingRegExp = "<relay_if.*?>|\[\[IF.*?\]\]",closingRegExp = "</relay_if>|\[\[ENDIF\]\]", singleOccurrence = true)>
		<cfset result.wholeTag = findStartAndEndOfIF[1].string>

		<!---
			We now do a regExp search to pull out the IF, ELSEIF and ELSE blocks (and their conditions)
			These are returned in the correct order so can later be looped through until we find a condition which is satisfied (or we get to the else)
		--->
		<cfset groupnames = {1="tag", 2="condition",3="innerText"}>
		<cfset parseIfandElses = application.com.regexp.refindalloccurrences (reg_expression = "(?:<relay_|\[\[)(if|else(?:if)?)(.*?)(?:>|\]\])(.*?)(?=<relay_else|</relay_if|\[\[else|\[\[endif|\Z)", groupnames = groupnames, string =  findStartAndEndOfIF[1].string,excludearray = findStartAndEndOfIF[1].nestedTags)>

		<cfset result.tagsArray = parseIfandElses>

		<cfreturn result>

	</cffunction>


		<!---
		RELAY_IF RELAY_ELSE RELAY_ELSEIF /RELAY_IF

		WAB 2013-01-06 CASE 432644 Completely rewritten so can now handle nested IFs
		--->

	<cffunction name="doRelayIF">
		<cfargument name="phraseText">
		<cfargument name="wholeTag">
		<cfargument name="tagsArray"> <!--- see parseRelayIF for a description of what this is --->


		<cfset var result = {phraseText=phraseText,errorArray=arrayNew(1)}>
		<cfset var relayIfResult = "">
		<cfset var evalResult = "">
		<cfset var item = "">
		<cfset var condition = "">

		<!---
			Loop through the tagsarray ie the IF, and ELSEIFs and the ELSE
			Test each condition until we get a true one, save the inner text and break
			If no condition is satisfied and no ELSE then relayIfResult will remain as ""
		--->

		<cfloop array="#tagsArray#" index="item">
			<cfif item.tag is not "else">
				<cfset condition = fixEscapedQuotes(item.condition)>
			<cfelse>
				<!--- There is no condition on the ELSE, but if we have got this far then must be true--->
				<cfset condition = true>
			</cfif>

			<cfset evalResult = evaluateAString(trim(condition))>

			<cfif not evalResult.isOK>
				<cfset result.errorArray = application.com.structureFunctions.arrayMerge(result.errorArray, evalResult.errorArray)>
			</cfif>


			<cfif this.config.evaluateAllOptions>
				<!--- If evaluateAllOptions is set then we don't test for true/false, just assume true, and we don't break out of the loop --->
				<cfset relayIfResult = relayIfResult & " " & item.InnerText>
			<cfelseif evalResult.isOK>
				<cfif evalresult.result>
					<cfset relayIfResult = item.InnerText>
					<cfbreak>
				</cfif>
			<cfelse>
				<!--- if the evaluation fails then we don't display either the if or the else, but hide something in the content' --->
				<cfset relayIfResult = "<!-- Condition Failed #trim(condition)# #evalResult.errorArray[1].message#-->">
				<cfbreak>
			</cfif>
		</cfloop>

		<!--- Sugar 448909 - problem with Turkish Translation. Replaced replaceNoCase with replace--->
		<cfset result.phraseText = Replace (phraseText,wholeTag,relayIfResult,"ONE")>

		<cfreturn result>

	</cffunction>


	<cffunction name="doRelayLoop">
		<cfargument name="phraseText">
		<cfargument name="params">
		<cfargument name="InnerText">
		<cfargument name="wholeTag">

		<cfset var result = {phraseText=phraseText,errorArray=arrayNew(1)}>
		<cfset var relayLoopResult= "">
		<cfset var tempArgs = {}>
		<cfset var InnerTextEvaluationResult = "">
		<cfset var regExp = application.com.relayTranslations.regExp>
		<cfset var queryResult = "">

			<!---
				RELAY_LOOP
				Note cannot deal with nested loops
			--->

		<cfset var ParamsStruct = application.com.regExp.convertNameValuePairStringToStructure(params)>

		<!--- NJH 2012/08/16 remove any square brackets in the query name... doesn't look like it has been 'evaluated' yet --->
		<cfif structKeyExists(paramsStruct,"query")>
			<cfset paramsStruct.query = replace(replace(paramsStruct.query,"[","","ALL"),"]","","ALL")>
		</cfif>
		<!---
			WAB 2011/11/08 added some code to check that the query is defined and to deal with errors when evaluating the inner text
			NJH 2012/08/31 - if the loop is the first variable in the content being merged, then it may not yet be defined. So, here
				I try and do an evaluateAString to see if we can get the query. And then, failing that, we then have an error.
		--->
		<cfif not isDefined(paramsStruct.query) or not IsQuery(evaluate(paramsStruct.query))>
			<cfset queryResult = evaluateAString(expression=paramsStruct.query)>
			<cfif queryResult.isOK>
				<cfset variables[paramsStruct.query] = queryResult.result>
			</cfif>
		</cfif>

		<cfif not isDefined(paramsStruct.query) or not IsQuery(evaluate(paramsStruct.query))>
			<cfset var errorStruct = {expression="#paramsStruct.query#", message = "#paramsStruct.query# is not a query"}>
			<cfset arrayAppend(result.errorArray, errorStruct)>
			<!--- have to replace any [[ ]] within the inner text, otherwise will keep trying to evaluate --->
			<cfset result.phraseText = Replace (phraseText,wholeTag,reReplace(innerText,"(\[\[|\]\])","","ALL"),"ONE")>
		<cfelse>
			<cfloop query = "#paramsStruct.query#">
				<cfset tempArgs = {phraseText = InnerText}>
				<cfset InnerTextEvaluationResult = checkForAndEvaluateCFVariables_returningStructure (argumentCollection = tempArgs)>
				<cfset relayLoopResult = relayLoopResult & InnerTextEvaluationResult.phraseText>
				<cfif arrayLen(InnerTextEvaluationResult.errorArray) >
					<cfset arrayAppend(result.errorArray, InnerTextEvaluationResult.errorArray[1])> <!--- we will only remember error from last loop, they are likely to be all identical --->
				</cfif>
			</cfloop>
			<cfset result.phraseText = Replace(phraseText,wholeTag,relayLoopResult,"ONE")>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="doCFVariableOrFunction">
		<cfargument name="phraseText">
		<cfargument name="expression">
		<cfargument name="wholeTag">

		<cfset var result = {phraseText=phraseText,errorArray=arrayNew(1)}>

		<cfset var evaluateResult = evaluateAString(expression=expression)>

		<cfif not evaluateResult.isok>
			<cfset result.errorArray = evaluateResult.errorArray>
		</cfif>

		<cftry>
			<!--- although unlikely, it is possible for this line to error if the evaluation returned something other than a string (for example if a merge field like opportunity.insideRep.fullname has been written as opportunity.insideRep)--->
			<cfset result.phraseText = Replace (phraseText,wholeTag,evaluateResult.result,"ONE")>
			<cfcatch>
				<!--- WAB 2012-11-13 CASE 431561 Need to replace the [[ ]] expression, otherwise we go into an infinite loop --->
				<cfset result.phraseText = Replace (phraseText,wholeTag,ucase(expression),"ONE")>
				<cfset var errorStruct = {expression = expression,message = cfcatch.message}>
				<cfset arrayAppend(result.errorArray, errorStruct)>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>

	<cffunction name="doRelayTag" output="false">
		<cfargument name="phraseText">
		<cfargument name="tagName">
		<cfargument name="parameters">  <!--- effectively bit between the start and end tags--->
		<cfargument name="wholeTag">

			<cfset var matchfound = "">
			<cfset var theTag = "">
			<cfset var nspecificValues = "">
			<cfset var name = "">
			<cfset var relayTagContent_html = "">
			<cfset var i =  "">
			<cfset var cfoutputonlyCount =  "">

			<cfset var doRelayTagResult = {phraseText=phraseText,errorArray=arrayNew(1)}>
			<cfset var regExp = application.com.relayTranslations.regExp>
			<!--- Now need to check whether there are any parameters in the tag --->
			<cfset var parameterStructure = application.com.regExp.convertNameValuePairStringToStructure(parameters)>

				<cfif structKeyExists(application.RelayTagKeyedByTagName,tagname) >
					<cfset matchfound = true>
					<cfset theTag = application.RelayTagKeyedByTagName[tagname]>
					<CFSET nSpecificValues = #theTag.SpecificValues#>
					<CFIF nSpecificValues>
						<CFINCLUDE TEMPLATE="TagTemplates/#theTag.TagTagTextID#_Tag.cfm">
					</CFIF>

					<cftry>
						<cfsaveContent variable = "relayTagContent_html">
							<!--- WAB 2013-02-26 replaced enablecfoutputonly no/yes around this block, because was indiscriminate and sometimes left us in an unexpected cfoutputonly=yes state --->
							<cfset cfoutputonlyCount = application.com.request.disableCFOUTPUTOnly()>
								<cfif not theTag.fileExists><!---  2009/02/11 WAB added check--->
									<cfoutput>File for tag #htmleditformat(theTag.Tag)# does not exist <!--  #theTag.cfm# --><BR></cfoutput>
								<cfelseif theTag.isCustomTag>
									<cfmodule template="#theTag.tagPath#"  attributecollection="#parameterStructure#">
								<cfelse>
									<!--- 2012/06/18	IH	Case 428852 use collection loop to prevent issues when struct key contains comma --->
									<cfloop collection="#parameterStructure#" item="i">
										<cfset variables[i]	= parameterStructure[i]>
									</cfloop>
									<cfinclude template="#theTag.tagPath#">
									<cfloop collection="#parameterStructure#" item="i">
										<cfset structDelete(variables,i)>
									</cfloop>
								</cfif>
							<cfset application.com.request.enableCFOUTPUTOnly(cfoutputonlyCount)>
						</cfsavecontent>

						<cfcatch>
							<!--- TBD Ought to log a full error here because it won't just be a bad merge field but a full blown CF error
									WAB 2013-10-23 log full error and added errorid
							--->
							<cfset var errorStruct = {expression = wholeTag,message = cfcatch.message}>
							<cfset var errorID = application.com.errorHandler.recordRelayError_ColdFusion (error=cfcatch)>
							<cfset arrayAppend(doRelayTagResult.errorArray, errorStruct)>
							<!--- NJH 2013/11/28 - output error in a 'standard' fashion --->
							<cfset relayTagContent_html = application.com.relayUI.message(message="Error with #tagname#.  Error ID #errorID#",type="error")>
						</cfcatch>
					</cftry>

					<!--- NJH 2016/04/06 Sugar 448909 - timing out when dealing with relayTags on Turkish translation, due to a bug with replaceNoCase with high ASCII characters --->
					<cfset doRelayTagResult.phraseText = Replace (phraseText,wholeTag,relayTagContent_html,"one")>
				<cfelse>
					<cfset doRelayTagResult.phraseText = Replace (phraseText,wholeTag,application.com.relayUI.message(message="#tagname# Tag Not Found",type="error"),"one")>
				</cfif>

		<cfreturn doRelayTagResult >
	</cffunction>


	<!--- this function just executes relay tags and can be used by itself if relyatags have not been executed during the normal phrase process --->
	<cffunction name="executeRelayTags">
		<cfargument name="phraseText">

			<cfset var result_html = phraseText>
			<cfset var find = "">
			<cfset var regExp = application.com.relayTranslations.regExp>
			<cfset var groups ={1="relaytagname",2="relayTagParameters"}>
			<cfset var tempRegExpArgs = {reg_expression = RegExp.relayTag,string = result_html,groupnames = groups}>

			<cfset find =  application.com.regExp.refindSingleOccurrence(argumentCollection = tempRegExpArgs)>

			<cfloop condition ="arrayLen(find)">
				<cfset result_html = doRelayTag (phrasetext = result_html,tagname = find[1].relayTagName, parameters = find[1].relayTagParameters,wholeTag = find[1].string).phraseText>
				<cfset tempRegExpArgs.string = result_html>
				<cfset find =  application.com.regExp.refindSingleOccurrence(argumentCollection = tempRegExpArgs)>
			</cfloop>

			<cfreturn result_html>

	</cffunction>

	<cffunction name="fixEscapedQuotes">

		<cfargument name="string">
		<cfset var result = string>

		<cfset result = Replace(result,"&##39;","'","ALL")>
		<cfset result = Replace(result,"`","'","ALL")>
		<cfset result = ReplaceNoCase(result,"&quot;",'"',"ALL")>
		<cfreturn result>
	</cffunction>


	<cffunction name="evaluateAString">
		<cfargument name="expression">

		<cfset var result = {isOK = true,result="",errorArray = arrayNew(1)}>
		<cfset var tempExpression = replaceNoCase(arguments.expression,'application.com.',"","ALL")> <!--- Not allowed to run functions from application.com, otherwise anything could happen!--->
		<cfset var counter = 0>
		<cfset var lastError = "">
		<cfset var tryAgain = false>
		<cfset var originalCatch = "">
		<cfset var regExp = '' />
		<cfset var groups = '' />
		<cfset var regExpTest = arrayNew(1) />
		<cfset var stringToEvaluate = '' />
		<cfset var refindResult = '' />


		<cfset tempExpression = fixEscapedQuotes(tempExpression)>

		<!--- Rather than always catching an error and then going off and finding the object, let us see if we can, in simple cases get the object first --->
		<!--- Ought to be in a catch --->
			<cfset var regExp = "(?x)    					## 	ignore whitespace - ie allow commenting
								([a-z0-9_]*?)				##	variablename
								\.							## dot
								(							
									[a-z0-9_]*(?![a-z0-9_(])		## variablename	but not ending with (  - so not a function	
									(?:\.[a-z0-9_]*(?![a-z0-9_(]))*    ## 	zero or more of a dot followed by a variablename but not ending with (  - so not a function
								)				
								">

		<!--- remove anything inside quotes before looking for xxx.yyy --->
		<cfset var tempExpressionNoQuotes = reReplace (tempExpression,"('|"").*?(\1)","","ALL")>
		<cfset var refindResult = application.com.regexp.refindAllOccurrences(regExp, tempExpressionNoQuotes, {"1" = "object","2" = "field"})>

		<cfif arrayLen(refindResult)>
			<cfloop array = #refindResult# index="local.item">
				<cfif structKeyExists(variables,local.item.Object)>
					<cfif local.item.Object is "user">
						<!--- We have something like user.person.aField which we are going to replace with something like user.person.get('aField')--->
						<cfset local.thing = user [listFirst(local.item.field,".")]>
						<cfset local.item.field = listRest(local.item.field,".")>
					<cfelse>
						<cfset thing = variables[local.item.Object]>
					</cfif>

					<cfif isObject (thing) and isInstanceOf (thing,'entities.entity') and local.item.field is not "">
						<!--- For an entity object we can replace object.field with object.get('field') and object.object2.field with object.get('object2.field')--->
						<cfset tempExpression = replace (tempExpression,local.item.field, "get('#local.item.field#')" )>
					</cfif>
				<cfelseif structKeyExists (application.entityTypeID, local.item.Object) and not listFindNoCase ("application,user", local.item.Object)>
					<cftry>
						<cfset getEntityResult = getEntity(entityType= local.item.Object,fieldname= local.item.field)>
						<cfcatch>
							<!--- WAB 2017-03-14. PROD2016-3558  Bad merge field causing an error rather than a being logged and carrying on
									If getEntity above fails we will just carry on.  
										Either will reoccur in the loop below and get logged
										Or it won't recoccur and we needn't worry
							 --->
						</cfcatch>
					</cftry>	
				</cfif>	
			</cfloop>	
		</cfif>

		<cfloop condition="counter lt 10">	<!--- We should break out of the loop at much less then 10, but this is a backstop! --->
			<cfset counter ++>
			<CFTRY>
				<!--- See comments in identifyMergeFields() to understand what is going on here --->
				<cfif this.config.identifyMergeFields>
					<!--- is the expression of the form   xxxxx or xxxx.yyyy, if so we return the name of the field --->
					<cfset refindResult = application.com.regexp.refindAllOccurrences("\A[a-z0-9_]*(\.[a-z0-9_]*)?\Z",tempExpression)>
					<cfif arrayLen(refindResult)>
						<cfset var fieldname = refindResult[1].string>
						<cfif not isBoolean(fieldname)>
							<cfset var errorStruct = {expression = tempExpression, message= "" ,fieldname=fieldname}>
							<cfset arrayAppend(result.errorArray,errorStruct)>
							<cfset result.isOK = false>
							<cfif not isDefined(fieldname)>
								<cfset "variables.#fieldname#" = ucase (fieldname)>
							</cfif>
						</cfif>
					</cfif>

				</cfif>

				<CFSET  result.result = evaluate(tempExpression)>

				<cfbreak>  <!--- if evaluation successful then break out of loop --->
					<CFCATCH>

						<cfset tryAgain = false>
						<cfset originalCatch = cfcatch>
						<!--- if this a repeat of a previous error then don't retry, will break out later in loop --->
						<cfif cfcatch.message is not lastError>

							<cfset lastError = cfcatch.message>

							<!--- look for Invalid CFML construct, this usually means a single hash --->
							<cfif cfcatch.message contains "Invalid CFML construct">
								<cfset tempExpression = replace (tempExpression,"##","####","ALL")>
								<cfset tryAgain = true>
							<cfelse>

								<!--- look at error message and see if it matches "Variable ABCD.WXYZ is undefined".  --->
								<cfset regExp = "Variable ([^\.]*?)\.(.*?) is undefined">
								<cfset groups = {1="structure",2="key"}>
								<cfset regExpTest = application.com.regExp.reFindAllOccurrences(regExp,cfcatch.message,groups)>

								<cfif not arrayLen(regExpTest)>
									<!--- look at error message again and see if it matches "Element WXYZ is undefined in ABCD".  --->
									<cfset regExp = "Element (.*?) is undefined in (.*?)\.">
									<cfset groups = {1="key",2="structure"}>
									<cfset regExpTest = application.com.regExp.reFindAllOccurrences(regExp,cfcatch.message,groups)>
								</cfif>

								<cfif arrayLen(regExpTest)>


									<!--- try running function called getEntity(ABCD,WXYZ).WXYZ
											if the table and field are valid, this should populate variables.ABCD.WXYZ which will then be available on the next loop
									--->
									<cfif NOT this.config.identifyMergeFields>
										<cftry>

											<!--- WAB 2015-01-28
												realised that I could easily add support for merging flags using [[table.flagName]] format for all tables by just calling
												the getEntity function (which I added a few months ago) for all entities, not just those for which get#table# does not exist
												and it gets rid of an evaluate

												WAB 2015-11-27 BF-26 During CASE 446665
												Was not working properly for variables of form  AAAA.BBBB.CCCC  (only a problem if first reference to AAAA happens to be in this form)
												Had to revert to an evaluate statement
												And added in a cfbreak, since if evaluate succeeds we have got our result and if it fails then will go through catch.

												I think that actually there may be no need for the loop anymore

												Reinstated the looping when entityObjects introduced
											--->
											<cfset getEntity(entityType=regExpTest[1].structure,fieldname=regExpTest[1].key)>
											<cfset tryAgain = true>

											<cfCatch>
											</cfCatch>
										</cftry>
									<cfelse>
										<!--- See comments in identifyMergeFields() to understand what is going on here --->
										<!--- This is a special case for previewing/testing and evaluation.  We don't go off and get missing fields automatically, but instead raise and error - this is used in Comms so that all the required fields are passed in in the merge structure  --->
										<cfset variables[regExpTest[1].structure][regExpTest[1].key] = "#regExpTest[1].structure#.#regExpTest[1].key#">
										<cfset result.isok = false>
										<cfset tryAgain = true>
										<cfset errorStruct = {expression = tempExpression, message= originalCatch.message,fieldname="#regExpTest[1].structure#.#regExpTest[1].key#"}>
										<cfset arrayAppend(result.errorArray,errorStruct)>

									</cfif>

								<!--- NJH 2011/08/03 - probably a BIG hack!!! and I'll admit it, but not sure what else to do at the moment...
									Essentially, there is a variable with the same name as a function and it's trying to evaluate it...
									so, here we explicitly try and run the function in the 'this' scope
									WAB 2011/11/16 added a cfbreak, otherwise loop keeps running 10 times!
									Have probably got rid of this error anyway by renaming the merge functions
								--->
								<cfelseif cfcatch.message eq "Entity has incorrect type for being called as a function.">
									<cftry>
										<cfset result.result = evaluate("this.#tempExpression#")>
										<CFBREAK>
										<cfcatch>
										</cfcatch>
									</cftry>
								</cfif>

							</cfif>

						</cfif>


						<cfif not tryAgain>
							<CFSET  result.result = ucase(tempExpression)>
							<cfset result.isok = false>
							<cfset errorStruct = {expression = tempExpression, message= originalCatch.message}>
							<cfif arrayLen(regExpTest)>
								<cfset errorStruct.fieldname ="#regExpTest[1].structure#.#regExpTest[1].key#">
							</cfif>

							<cfset arrayAppend(result.errorArray,errorStruct)>

							<cfbreak>
						</cfif>

					</CFCATCH>
			</CFTRY>

			<!---
			TODO  Ought to log an error if counter reaches 10
			 --->

		</cfloop>

		<cfreturn result>

	</cffunction>

	<!--- WAB 2012-03-14 Code to do merging via a CFM file - for use in Comms
		One Function to take phraseText and create a temporary file
		One Function which uses that file to do a merge
		Note that the creation and use do not have to be by the same mergeObject.  Infact in general the file is created once and then used by many mergeObjects
	--->

	<cffunction name="createMergeFile">
		<cfargument name="phraseText">
		<cfargument name="fileNameRoot" default="merge">
		<cfargument name="charSet" default="UTF-8">

		<cfset var result = {isOK=true}>
		<cfset var fileName = fileNameRoot & "_#randRange(1,1000000)#.cfm">
		<cfset var fileContents =  "">
		<cfset var IFregExp = "(?:<(/)?relay|\[\[)_(if|else|elseif)(.*?)>">


		<cfset result.path = "\temp\#fileName#">
		<cfset result.expandedPath = expandPath("\temp\#fileName#")>

		<cfset fileContents = phrasetext>
		<!--- remove All CF tags just in case
		WAB 2013-04-24 434850, don't replace cfif and convert [[if]] to <cfif>
				Add support for [[IF]] syntax
		--->
		<cfset fileContents = rereplaceNocase(fileContents,"<CF(?!_executerelaytag|if|else|elseif)","<","ALL")>

		<!--- remove all references to application.com--->
		<cfset fileContents = replaceNocase(fileContents,"application.com","","ALL")>

		<!--- If there are relayTags in the content then need to pop a cf_executeRelayTag around them --->
		<cfset var regexp = "(\[\[|<)(RELAY_.*?)(\]\]|>)">
		<cfset var replaceexp = "<CF_executeRelayTag><\2></CF_executeRelayTag>">
		<cfset fileContents = rereplaceNocase(fileContents,regexp,replaceexp,"ALL")>

		<!--- Deal with RelayIF tags, replace with cfif equivalents --->
		<cfif not this.config.evaluateAllOptions>
			<cfset fileContents = reReplaceNocase(fileContents,IFregExp,"<\1CF\2\3>","ALL")>
		<cfelse>
			<cfset fileContents = reReplaceNocase(fileContents,IFregExp," ","ALL")>
		</cfif>
		<cfset fileContents = rereplaceNocase(fileContents,"\[\[ENDIF\]\]","</cfif>","ALL")>

		<!--- WAB 2013-06-11 Replace LF and CR entities, cause problems if in the middle of a merge field (which can happen!) --->
		<cfset fileContents = replace(fileContents,"&##13;","#chr(13)#","ALL")>
		<cfset fileContents = replace(fileContents,"&##10;","#chr(10)#","ALL")>

		<!--- escape any single # --->
		<cfset fileContents = replace(fileContents,"##","####","ALL")>

		<!--- change [[  ]] to #   #  --->
		<cfset fileContents = rereplace(fileContents,"\[\[|\]\]","##","ALL")>

		<cfset fileContents = "<cfoutput><cfprocessingdirective  pageencoding='#charSet#'>" & fileContents & "</cfoutput>">

		<!--- WAB 2013-05-29 CASE 435379 don't add a new line to the merge file --->
		<cffile action="write" output="#fileContents#" file="#result.expandedPath#" charset=#charSet# addNewLine=false>

		<cfreturn result>

	</cffunction>


	<cffunction name="mergeByFile">
		<cfargument name="filePath" required=true>

		<cfset var result = {phraseText="",errorArray = arrayNew(1)}>
		<cftry>
			<!--- WAB 2013-05-29 CASE 435380 Note that CFSaveContent must be hard up against the include to prevent stray spaces and carriage returns --->
			<cfsaveContent variable="result.phraseText"><cfinclude template="#filePath#"></cfsaveContent>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset var errorStruct = {expression="", message = cfcatch.message}>
				<cfset arrayAppend(result.errorArray, errorStruct)>

			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>

	<cffunction name="getVariablesScope">
		<cfreturn variables>
	</cffunction>

    <!--- Case 449024 --->
    <cffunction name="getFundActivity" returntype="struct">
        <cfargument name="fieldName" type="string" default="">

        <cfset arguments.fundRequestActivityID = variables.fundRequestActivityID>
        <cfset variables.fundActivity = application.com.relayFundManager.getFundActivityV2(activityID=arguments.fundRequestActivityID)>

        <cfif isQuery(variables.fundActivity) AND variables.fundActivity.recordCount EQ 1>
            <!--- convert to structure as expected by a caller --->
            <cfset variables.fundActivity = application.com.structureFunctions.queryRowToStruct(query = variables.fundActivity, row=1)>
        </cfif>

        <cfreturn variables.fundActivity>
    </cffunction>

</cfcomponent>

