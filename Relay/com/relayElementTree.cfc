<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Mods

WAB  2005-10-31 added function whichTreeIsThisNodeIn()

WAB   2006-06-26  modification to exactly where in the code application.elementTreeCache[elementid] is created

WAB 2007-03-20  added a check for application.ElementTreeDefByNameAndCountry being defined - it keeps disappearing!!
WAB 2007-03-20  altered where tidyElementTreeCache is called
WAB 2007-06-01 altered tidyelementtreecache process to be asynchronous
GCC 2007/08/30  Used ETIDs if exist over EID for the URLs used in navigation / breadcrumbs - see JvdW for rationale
WAB 2007/09/19  added function for outputting tree as a UL
WAB 2007/12  added function for outputting tree as a milonic
WAB 2008/01/09  added code to cache the details of the unknown user trees
WAB 2008/06    rewrote the tidyelementtreecache code so that it actually worked
WAB 2008/11/27  mod to tidyelementtreecache function to deal with CF7 not having a sleep function
18 Feb 2009		NYB 		Sophos Stargate 2 - added 2 new functions
18 Feb 2009		NYB 		Sophos Stargate 2 - added colLength and colLength check throughout insert to stop errors being thrown when a string beyond the size allowed by the table was passed
2009/03/12		WAB	 LID 1962 change to processSEIDValue to handle strings with guff added to end
2009/06 		WAB	LID 2361 altered the seid function to deal with test links
2009/10/05 		WAB 	LID 2725
2009/10/13 		WAB 	LID 2763 added http protocol variable to tidyElementTreeCache function
2009/10/13 		WAB 	More problems with tidyElementTreeCache function and cflock
2009/11/09		WAB 	altered http protocol in tidyElementTreeCacheTable to not use request.currentSite but calculated directly, because sometimes called from webservices\nosession where request.currentSite does not exist.  Did try adding request.currentSite to webservices\nosession at one point but this led to other problems
				also altered how a lock timeout is dealt with, so that fails more gracefully
2010/02/02		WAB 	LID 3058 altered the whyCantCurrentUserSeeThisElement function so that login is checked before rights.  This means that we can display a login screen if login is required and redirect to the home page if the user does not have rights.  Whereas if done the other way round we always get the answer that the user doe snot have rights the
2010/06			WAB 	Changed reference to instance
2012/01/18		RMB		P-LEN24 - CR066 Added validvalues = true to "getTreeTopNodes"
	WAB 2014-01-31		removed references to et cfm, can always just use / or /?
2015/09/30      DAN     PROD2015-77 - add character limit to the search input
2016/01/27		YMA		PROD2016-2463 added sortorder to getSecondaryNavigationBranchFromTree and convertTreeToBranch methods

 --->

<cfcomponent displayname="Relay Element Tree Component" hint="Retrieves data for Element Tree UI">
<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="remote" name="getTreeTopNodes" output="false" returntype="query" hint="Query to retrieve elements with this parentID." validValues="true">

		<cfset var qGetTreeTopNodes = "">
		<cfset var siteStructureTopNodes = '' />

		<!--- get top nodes from the sitestructure --->
		<CFQUERY NAME="siteStructureTopNodes" DATASOURCE="#application.SiteDatasource#" cachedwithin="#application.cachedContentTimeSpan#">
			IF exists (select 1 from sysobjects where name = 'siteDef' and xtype='u')
				BEGIN
				select distinct topid from elementTreeDef	where topid is not null
				END
			ELSE
				select 0 as topid
		</CFQUERY>

		<CFQUERY NAME="qGetTreeTopNodes" DATASOURCE="#application.SiteDatasource#" cachedwithin="#application.cachedContentTimeSpan#">
			-- #application.cachedContentVersionNumber# (this variable is used to flush the cache when the content is changed)
			SELECT Element.Headline, ElementType.Type, element.ID, element.elementTypeID
			FROM Element INNER JOIN
			     ElementType ON Element.ElementTypeID = ElementType.ElementTypeId
				 where (Element.isTreeRoot = 1 and Element.islive = 1 ) <cfif siteStructureTopNodes.recordcount is not 0>or id  in ( <cf_queryparam value="#valuelist(siteStructureTopNodes.topid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				Order by element.ID asc --ElementType.Type, Element.headline
		</CFQUERY>
		<cfreturn qGetTreeTopNodes>
	</cffunction>


	<!--- gets list of sites using a given element as tree top --->
	<cffunction name="getSitesForTopElementID">
		<cfargument name="topelementID" type="string" required="true" >

		<cfset var result = arrayNew(1)>
		<cfset var treeName = '' />
		<cfset var domain = '' />
		<cfset var siteid = '' />
		<cfset var countryid = '' />
		<!--- WAB have been having errors here where application.elementtreedefbynameandcountry seems to disappear from memory all of a sudden
		really don't understand it but will check for it anyway
		--->
		<cfif not structKeyExists(application ,"ElementTreeDefByNameAndCountry")>
				<cfset application.com.relayCurrentSite.loadElementTreeDefinitions()>
		</cfif>

		<!--- 	WAB 2010/10/12 removed references to application.site def and replaed with application.sitedefbyid--->
		<cfloop collection = #application.sitedefbyid# item = "siteid">
			<cfset treeName = application.sitedefbyid[siteid].elementTreeName>
			<cfloop collection = #application.elementtreedefbynameandcountry[treeName]# item = "countryid">
				<cfif application.elementtreedefbynameandcountry[treeName][countryid].topid is topelementID>
					<cfloop list="#application.sitedefbyid[siteid].domainlist#" index="domain">
						<cfset arrayAppend(result,domain)>
					</cfloop>
						<cfbreak>

				</cfif>

			</cfloop>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<!--- gets list of sites using a given element as tree top
		WAB 2010/10/12 removed references to application.site def and replaced with application.sitedefbyid
	--->
	<cffunction name="getSitesAndTopElements">

		<cfset var treeName = '' />
		<cfset var topid = '' />
		<cfset var domain = '' />
		<cfset var siteid = '' />
		<cfset var countryid = '' />
		<cfset var tempStruct = structNew()>
		<cfset var resultQuery = queryNew('siteDomain,topelementid')>

		<!--- loop through each siteDef and get name of its tree--->
		<cfloop collection = #application.sitedefbyid# item = "siteid">
			<cfset treeName = application.sitedefbyid[siteid].elementTreeName>

			<cfif structKeyExists(application.elementtreedefbynameandcountry,treename)>

				<cfloop collection = #application.elementtreedefbynameandcountry[treeName]# item = "countryid">
					<cfparam name="tempStruct[siteid]" default = #structNew()#>
					<cfset topid = application.elementtreedefbynameandcountry[treeName][countryid].topid>
					<cfif not structKeyExists(tempStruct[siteid],topid)>
						<cfset tempStruct[siteid][topid] = 1>
						<!--- Now have to loop through all the domains for that site--->
						<cfloop index="domain" list="#application.sitedefbyid[siteid].domainList#">
							<cfset queryAddRow(resultQuery)>
							<cfset querySetCell(resultQuery,'siteDomain',domain)>
							<cfset querySetCell(resultQuery,'topelementid',topid)>
						</cfloop>
					</cfif>
				</cfloop>
			</cfif>
		</cfloop>
		<!---2014/03/27 GCC removed order by siteDomain which was overriding the order set by maindomain desc in the domain list--->
		<cfquery name = "resultQuery" dbtype="query">
		select * from resultQuery
		</cfquery>


		<cfreturn resultQuery>
	</cffunction>


<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="remote" name="getTreeChildNodes" output="false" returntype="query" hint="Query to retrieve elements as Child nodes with this parentID.">
		<cfargument name="parentID" type="string" required="true" hint="ParentID to use in the query.">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">

		<cfset var qGetTreeChildNodes = "">
		<cfset var ErrorText = '' />
		<cfset var getElements = '' />

		<cfquery name="qGetTreeChildNodes" datasource="#dataSource#">
			SELECT headline AS nodeText, id AS nodeID
				FROM element where parentID =  <cf_queryparam value="#arguments.parentID#" CFSQLTYPE="CF_SQL_INTEGER" >
				ORDER BY sortOrder
		</cfquery>
		<cfreturn qGetTreeChildNodes>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="refreshElementBranch" hint="What does this function do">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="personID" type="numeric" required="yes">

		<cfset var ErrorText = '' />
		<cfset var getElements = '' />

			<cfstoredproc procedure="GetElementBranch" datasource="#dataSource#" returncode="Yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@ElementID" value="#ElementID#" null="No">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@personid" value="#personID#" null="No">
					<cfprocresult name="getElements" resultset="1">
				<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="ErrorText" dbvarname="@FNLErrorParam" null="Yes">
				<cfif frmelementstatus neq ''>
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" dbvarname="@Depth" value="0" null="No">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" dbvarname="@Status" value="#frmElementStatus#" null="No">
				</cfif>
			</cfstoredproc>
			<!--- Make the query object available in the application scope. --->
			<cflock timeout="120" throwontimeout="No" name="elementTree" type="EXCLUSIVE">
				<cfset application.elementTree = getelements>
			</cflock>
		<cfreturn ErrorText>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="cloneElementBranch" hint="This creates a clone of a particular.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="targetElementID" type="numeric" required="no" default=0>  <!--- NJH 2006/10/25 --->
		<cfargument name="IncludeTopElement" type="numeric" required="no" default=1>
		<cfargument name="copyRecordRights" type="numeric" required="no" default=1>
		<cfargument name="copyCountryScopes" type="numeric" required="no" default=1>
		<cfargument name="copyPhrases" type="numeric" required="no" default=1>
		<cfargument name="numberOfGenerations" type="numeric" required="no" default=99>

		<cfset var ErrorText = '' />
		<cfset var clonedElements = '' />

		<CFSTOREDPROC PROCEDURE="CloneElementBranch" DATASOURCE="#dataSource#" RETURNCODE="Yes">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ElementID" VALUE="#ElementID#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE="#personID#" NULL="No">

			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@targetElementid" VALUE="#targetElementID#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@includeTopElement" VALUE="#includeTopElement#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@copyRecordRights" VALUE="#copyRecordRights#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@copyCountryScopes" VALUE="#copyCountryScopes#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@copyPhrases" VALUE="#copyPhrases#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@numberOfGenerations" VALUE="#numberOfGenerations#" NULL="No">

				<CFPROCRESULT NAME="clonedElements" RESULTSET="1">
			<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="ErrorText" DBVARNAME="@FNLErrorParam" NULL="Yes">
		</CFSTOREDPROC>

		<cfreturn "Element branch cloned">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deleteElementBranch" hint="This deletes a particular element Branch.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="personID" type="numeric" required="yes">

		<cfset var ErrorText = '' />
		<cfset var clonedElements = '' />

		<CFSTOREDPROC PROCEDURE="CloneElementBranch" DATASOURCE="#dataSource#" RETURNCODE="Yes">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ElementID" VALUE="#ElementID#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE="#personID#" NULL="No">
				<CFPROCRESULT NAME="clonedElements" RESULTSET="1">
			<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="ErrorText" DBVARNAME="@FNLErrorParam" NULL="Yes">
		</CFSTOREDPROC>

		<cfreturn "Element branch deleted.">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setRoot" hint="It updates an element making isTreeRoot = 1.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="userGroupID" type="numeric" required="yes">

		<cfquery datasource="#dataSource#">
			update element set isTreeRoot = 1,
				lastUpdatedBy = <cfqueryparam value="#userGroupID#" cfsqltype="CF_SQL_INTEGER">,
				lastUpdated = getDate()
			where ID = <cfqueryparam value="#elementID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>

		<cfreturn "Set elementID #elementID# as a tree root.  You will now be able to edit the element tree in content trees.">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="UnSetRoot" hint="It updates an element making isTreeRoot = 1.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="userGroupID" type="numeric" required="yes">

		<cfquery  datasource="#dataSource#">
			update element set isTreeRoot = 0,
				lastUpdatedBy = <cfqueryparam value="#userGroupID#" cfsqltype="CF_SQL_INTEGER">,
				lastUpdated = getDate()
			where ID = <cfqueryparam value="#elementID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn "Set elementID #elementID# as a tree root.  You will now be able to edit the element tree in content trees.">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="CheckElementAdminRights" returnType="query"
		hint="description">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="personID" type="numeric" required="yes">

		<cfset var CheckElementAdminRights = '' />

		<CFQUERY NAME="CheckElementAdminRights" DATASOURCE="#application.dataSource#">
			SELECT UserGroup.UserGroupID,
				UserGroup.Name
			FROM UserGroup INNER JOIN RecordRightsGroup
			ON UserGroup.UserGroupID = RecordRightsGroup.UserGroupID
			WHERE RecordRightsGroup.PersonID=#personID#
		</CFQUERY>
		<cfreturn CheckElementAdminRights>
	</cffunction>

	<!--- WAB 2007/09/19 split  getFullTreeFromCache out into getFullTreeDetailsFromCache and a stub getFullTreeFromCache --->

	<cffunction access="public" name="getFullTreeFromCache" returnType="query"	output="no">
		<cfargument name="topElementID" type="numeric" required="yes">

		<cfreturn 	getFullTreeDetailsFromCache(topElementID).query>

	</cffunction>


	<cffunction access="public" name="getFullTreeDetailsFromCache" returnType="struct"	output="no">
		<cfargument name="topElementID" type="numeric" required="yes">


		<cfset var flagQuery = "">
		<cfset var fullTree = "">
		<cfset var rowIndexStruct = structNew()>
		<cfset var refreshrequired = "">
		<cfset var cacheName = "">
		<cfset var getElementTreeCacheID	 = "">
		<cfset var ElementTreeCacheID	 = "">
		<cfset var theNode	 = "">
		<cfset var thedate = '' />
		<cfset var getNextGoLiveDate = '' />
		<cfset var getNextExpiresDate = '' />

		<cfparam name="application.elementTreeCache" default = #structNew()#>

		<!---
			WAB 07/13/
			need to lock this bit of code so that for a given topelementid only one process can
			get the tree at a time (or more importantly only one process can refresh it at a time)
		 --->
		<cflock timeout = "60" throwontimeout="Yes" name="FullTree#topelementid#" type="EXCLUSIVE">

			<cfif not structKeyExists(application.elementTreeCache,topElementID) >
				<cfset refreshRequired = true>

			<cfelseif application.cachedContentVersionNumber is not application.elementTreeCache [topElementID].versionNumber>
				<cfset refreshRequired = true>
			<cfelseif application.elementTreeCache[topElementID].refreshrequireddate is not "" and application.elementTreeCache[topElementID].refreshrequireddate lt now()>
				<cfset refreshRequired = true>
<!--- 			<cfelseif true>
				<cfoutput>debug setting refresh on every load (relayelementtree.cfc)</cfoutput>
				<cfset refreshRequired = true>
 --->		<cfelse>
				<cfset refreshRequired = false>
			</cfif>

			<cfif refreshRequired>

					<cfset cacheName = "FullTree_#topElementID#">
					<cfquery name="fullTree" datasource="#application.sitedatasource#" >
					exec GetElementTree
						@ElementID=#topElementID#,
						@personid=-1,  <!--- tree regardless of rights --->
						@depth = 0,    <!--- full depth --->
						@statusid = -1,	<!--- any status --->
						@date = null ,  <!--- any date --->
						@cacheName =  <cf_queryparam value="#cacheName#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfquery>

					<!--- get cacheid --->
					<cfquery name="getElementTreeCacheID" datasource="#application.sitedatasource#">
					select elementtreecacheID from elementtreecache where cachename =  <cf_queryparam value="#cachename#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfquery>
					<cfset elementTreeCacheID = getElementTreeCacheID.elementtreecacheID>
					<cfset flagQuery = getElementFlagData(cacheID = elementTreeCacheID)>


					<!--- this function adds the resolvedHREF and resolvedTarget Columns --->
					<cfset fullTree = applyResolvedHREFToElementTree (fullTree)>


				<!--- join the flagdata to the tree --->
				<cfquery name="fullTree" dbtype="query">
				select
					*
				from
					fullTree , flagQuery
				where
					fulltree.node = flagQuery.id
					order by sortorder
				</cfquery>

				<!--- create a structure keyed on elementid giving the row number.  we can use this to access this cache directly (used for updating individual rows of the cache)
					Added WAB 2006-10-01 for caching flaggroup data
				--->
				<cfloop query = "fullTree">
					<cfset rowIndexStruct[node] = currentRow>
				</cfloop>


				<!--- find out when we will next need to refresh the cache  --->
				<cfquery name="getNextGoLiveDate" dbtype="query">
				select min(livedate) as thedate from fullTree where livedate is not null and livedate > '#dateformat(now(),"yyyy-mm-dd")# #timeformat(now(),"HH:mm:ss")#'
				</cfquery>
				<cfquery name="getNextExpiresDate" dbtype="query">
				select min(expiresdate) as thedate  from fullTree where expiresdate is not null and expiresdate > '#dateformat(now(),"yyyy-mm-dd")# #timeformat(now(),"HH:mm:ss")#'
				</cfquery>

				<!--- very long if - must be a better way but couldn't find it --->
				<cfif getNextGoLiveDate.theDate is "" and getNextExpiresDate.thedate is "">
					<cfset thedate = "">
				<cfelseif getNextGoLiveDate.theDate is "" and 	getNextExpiresDate.thedate is not "">
					<cfset thedate = getNextExpiresDate.thedate>
				<cfelseif getNextGoLiveDate.theDate is not "" and 	getNextExpiresDate.thedate is "">
					<cfset thedate = getNextGoLiveDate.theDate >
				<cfelse>
					<cfset thedate = min (getNextGoLiveDate.thedate, getNextExpiresDate.thedate)	>
				</cfif>


				<!--- WAB 2006-06-26 added this (moved the structNew() from higher up the code)
					trying to prevent a bug with application.elementTreeCache [topElementID].versionnumber not existing)
				 --->
				<cfif not structKeyExists (application.elementTreeCache,"topElementID")>
					<cfset application.elementTreeCache [topElementID] = structNew()>
				</cfif>
				<cfset application.elementTreeCache [topElementID].versionNumber = application.cachedContentVersionNumber>
				<cfset application.elementTreeCache [topElementID].query = fullTree>
				<cfset application.elementTreeCache [topElementID].dateCached = now()>
				<cfset application.elementTreeCache [topElementID].refreshrequireddate = theDate>
				<cfset application.elementTreeCache [topElementID].RowIndexStruct = rowIndexStruct>
				<cfset application.elementTreeCache [topElementID].elementTreeCacheID = elementTreeCacheID>
				<cfset application.elementTreeCache [topElementID].unknownPersonTrees = structNew()>

			</cfif>

		</cflock>

			<cfreturn 	application.elementTreeCache [topElementID]>

	</cffunction>


	<cffunction access="private" name="applyResolvedHREFToElementTree" >
		<cfargument name="ElementTree" type="query" required="yes">
			<cfset var theHref = "">
			<cfset var theNode = '' />
			<cfset var theTarget = "">
				<!--- WAB 2006-01-24
						add two columns so that don't always have to work out the required url
						still abit rough and ready - especially in the target area
						resolved Href
						resolvedTarget

						2008/01/10 broken out as a function
						WAB 2014-01-31		removed references to et cfm, can always just use / or /?
						--->

				<cfset queryAddColumn (ElementTree,"resolvedHREF",arrayNew(1))>
				<cfset queryAddColumn (ElementTree,"resolvedTarget",arrayNew(1))>
				<cfloop query="ElementTree">
					<CFIF isExternalFile EQ 1>
							<CFIF left(URL, 10) EQ "JavaScript">
								<cfset theHREF = #url#>
								<cfset theTarget = "_self">
							<cfelseif left(URL, 7) EQ "http://"><!--- link out to new window, actually should check if link is  --->
								<cfset theHREF = #url#>
								<cfset theTarget = "_blank">
							<cfelseif left(URL, 8) EQ "https://"><!--- link out to new window, actually should check if link is  --->
								<cfset theHREF = #url#>
								<cfset theTarget = "_blank">
							<CFELSEIF left(URL, 1) EQ "?">
								<cfset theHREF = "/#url#&fromeid=#node#">  <!--- if a click is going to a different page then I am passing the eid of the node on the menu, jsut incase it is required fror something--->
								<cfset theTarget = "">
							<CFELSEIF left(URL, 1) EQ "/" or left(URL, 6) EQ "et.cfm" >
								<cfset theHREF = "/#replaceNoCase(url,'et.cfm','')#&fromeid=#node#">
								<cfset theTarget = "">
							<CFELSE >
								<cfset theHREF = "http://#url#">
								<cfset theTarget = "">
							</CFIF>
						<CFELSE>
								<!--- 2007/08/30 GCC Used ETIDs if exist over EID for the URLs used in navigation / breadcrumbs - see JvdW for rationale --->
								<cfif elementTextID neq "">
									<cfset theNode = elementTextID>
								<cfelse>
									<cfset theNode = node>
								</cfif>

								<cfset theHREF = "/?eid=#theNode#">
								<cfset theTarget = "">
						</CFIF>
						<cfset querySetCell(ElementTree,"resolvedHREF",theHREF,currentRow)>
						<cfset querySetCell(ElementTree,"resolvedTarget",theTarget,currentRow)>
				</cfloop>

			<cfreturn elementTree>
	</cffunction>


	<!--- WAB 2008/01/09 just messing - wondering whether we can dynamically update the caches
		(currently just updating a single column)
		haven't dealt with what happens if multiple instances of a node in the tree!
	--->
	<cffunction access="public" name="updateAnElementInCache" >
		<cfargument name="ElementID" type="numeric" required="yes">

			<cfset var getElement = "">
			<cfset var rowid = "">
			<cfset var thisCache = "">
			<cfset var topElementID = '' />

			<cfquery name="getElement" datasource="#application.sitedatasource#" >
			select * from element where id = #elementID#
			</cfquery>
			<cfset getElement  = applyResolvedHREFToElementTree (getElement)>

			<!--- loop through all the caches --->
			<cfloop collection = "#application.elementTreeCache#" item="topElementID">
				<cfoutput><BR>checking tree #htmleditformat(topElementID)#<BR></cfoutput>
				<cfset thisCache = application.elementTreeCache[topElementID]>
				<!--- see whether this element appears in the cache --->
				<cfif structKeyExists(thisCache.RowIndexStruct,elementID)>
					<!--- this is the row in the query --->
					<cfset rowid = thisCache.RowIndexStruct[elementID]>
					<!---  check that this is the correct row --->
					<cfif elementID is thisCache.query.node[rowid]>
						<cfoutput><BR>#topElementID# #rowid# #thisCache.query.parameters[rowid]#</cfoutput>
						<cfset querySetCell(thisCache.query,"parameters",getElement.parameters,RowID)>
					<cfelse>
						<cfoutput>Incorrect Row</cfoutput>
					</cfif>
				</cfif>
			</cfloop>

	</cffunction>

	<cffunction access="public" name="getElementTreeCacheID" returnType="numeric"	output="no">
		<cfargument name="topElementID" type="numeric" required="yes">

		<cfset var useUnknownUserCache = '' />
		<cfset var insertIntoElementTreeCache = '' />

		<cfif not structKeyExists(application,"elementTreeCache") or not structKeyExists(application.elementTreeCache,topElementID) >
			<cfset getFullTreeFromCache	(topElementID)>
		<cfelseif not structKeyExists(application.elementTreeCache [topElementID],"elementTreeCacheID") or  application.elementTreeCache [topElementID].elementTreeCacheID is "">
			<cfset application.elementTreeCache[topElementID].refreshrequireddate = now() -1>  <!---  hack way of getting the cache refreshed (really should only occur during deployment of this code --->
			<cfset getFullTreeFromCache	(topElementID)>
		</cfif>
		<cfreturn application.elementTreeCache [topElementID].elementTreeCacheID>

	</cffunction>


	<cffunction access="public" name="getTreeDetailsForCurrentUserFromCache" returnType="struct">
			<cfargument name="topElementID" type="numeric" required="yes">
			<cfset var refreshrequired = true>
			<cfset var elementList = "">
			<cfset var additionalElementList = "">
			<cfset var weightingQuery = "">
			<cfset var getPersonElements = "">
			<cfset var getAdditionalElements = "">
			<cfset var cacheName = "SessionTree_#session.sessionid#_#request.relaycurrentuser.personid#_#topElementID#">
			<cfset var getElementTreeCacheID	 = "">
			<cfset var elementTreeCacheID = "">
			<cfset var fullTreeDetails = "">
			<cfset var fullTreeQuery= "">
			<cfset var useUnknownUserCache = '' />
			<cfset var insertIntoElementTreeCache = '' />


			<!--- have we a cache of this person's element ids? --->
			<cfparam name="session.elementTreeCache" default = #structNew()#>

			<!--- 2006-06-04 WAB added locking - get problems in frame sets where all three frames get tree at same time --->
			<cflock timeout = "60" throwontimeout="Yes" name="UserTree_#session.sessionid#_#topelementid#" type="EXCLUSIVE">

				<cfif not structKeyExists(session.elementTreeCache,topElementID) or not structKeyExists(session.elementTreeCache[topElementID],"applicationCachedContentVersionNumber")>
					<cfset session.elementTreeCache [topElementID] = structNew()>
					<cfset refreshRequired = true>
				<cfelseif	session.elementTreeCache[topElementID].applicationCachedContentVersionNumber is not application.CachedContentVersionNumber
						or	session.elementTreeCache[topElementID].date lt request.relayCurrentUser.content.refreshBeforeThisDate
						or  session.elementTreeCache[topElementID].isLoggedIn  is not request.relayCurrentUser.isLoggedIn
						>
						<cfset refreshRequired = true>
	<!--- 			<cfelseif true>
					<cfset refreshRequired = true>
					<cfoutput>debug: tree being refreshed on every load</cfoutput>
	 --->
				<cfelse>
					<cfset refreshRequired = false>
				</cfif>


				<cfif refreshRequired>

					<cfset elementTreeCacheID = "">

						<!---  WAB 2008/01/09
						wanted to cache the unknown user tree to prevent getElementTree being called every time site is hit by site monitors and things
						can only use the cached tree if this an unknown user without any 'twiddly bits' - hence large if statement
						--->
						<cfset useUnknownUserCache = false>
						<cfif request.relaycurrentuser.Content.showForPersonID is application.unknownPersonID and
								request.relaycurrentuser.Content.Status is 4 and
								not request.relaycurrentuser.Content.showForDifferentDate and
								request.RelayCurrentUser.Content.AlwaysShowTheseElementIDs is "" and
								request.RelayCurrentUser.Content.AlwaysShowTheseElementIDsIfStatusCorrect is "">
								<!--- this is plain unknown user so can use cached info if exists --->
								<cfset useUnknownUserCache = true>
								<cfset cacheName = "UnknownUserTree_#topelementid#">
						</cfif>

						<cfif useUnknownUserCache and (structKeyexists(application.elementTreeCache [topElementID],"unknownPersonTrees") and structKeyexists(application.elementTreeCache [topElementID].unknownPersonTrees,request.relaycurrentuser.Content.showForCountryID))>
								<!--- unknown user cache does exist so make use of it --->
									<cfset elementList = application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID].elementList>
									<cfset elementTreeCacheID = application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID].elementTreeCacheID>
									<cfset weightingQuery = application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID].weightingQuery>
						<cfelse>

							<cfquery name="getPersonElements" datasource="#application.sitedatasource#" >
							exec GetElementTree
								@ElementID=#topElementID#,
								@personid=#request.relaycurrentuser.Content.showForPersonID#,
								@countryid=#request.relaycurrentuser.Content.showForCountryID#,
								@Statusid=#request.relaycurrentuser.Content.Status#,
								@depth = 0 ,
								@isLoggedIn = #iif(request.relaycurrentUser.isloggedin,1,0)#,
								@justReturnNodes = 1
								<cfif request.relaycurrentuser.Content.showForDifferentDate>
								,@date  = #request.relaycurrentuser.Content.Date#
								</cfif>
								,@cacheName =  <cf_queryparam value="#cacheName#" CFSQLTYPE="CF_SQL_VARCHAR" >
								,@useTempRightsTable = 0
							</cfquery>
							<!--- get cacheid --->
							<cfquery name="getElementTreeCacheID" datasource="#application.sitedatasource#">
							select elementtreecacheID from elementtreecache where cachename =  <cf_queryparam value="#cachename#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfquery>

							<cfset elementTreeCacheID = getElementTreeCacheID.elementtreecacheID>
							<cfset elementList = valuelist(getPersonElements.node)>


								<cfif request.RelayCurrentUser.Content.AlwaysShowTheseElementIDs is not "" or request.RelayCurrentUser.Content.AlwaysShowTheseElementIDsIfStatusCorrect	is not "" >
									<!--- if there are any elements which are to be shown regardless of status then we need to make sure that these elements and their parents are added to this user's tree --->
									<cfset fullTreeDetails = getFullTreeDetailsFromCache (topElementID)>
									<cfset fullTreeQuery = fullTreeDetails.query>
									<cfquery name="getAdditionalElements" dbtype="query" >
									select * from fullTreeQuery where
									1= 0
									<cfif request.RelayCurrentUser.Content.AlwaysShowTheseElementIDs	is not "">
									 OR node in (#request.RelayCurrentUser.Content.AlwaysShowTheseElementIDs#)
									</cfif>
									<cfif request.RelayCurrentUser.Content.AlwaysShowTheseElementIDsIfStatusCorrect	is not "">
									 OR (node in (#request.RelayCurrentUser.Content.AlwaysShowTheseElementIDsIfStatusCorrect#) AND statusid >= #request.relaycurrentuser.Content.Status# and statusid <> 5)
									</cfif>
									</cfquery>

									<cfif getAdditionalElements.recordCount is not 0 >
										<!--- add nodes and parents to the element list (this will probably end up with duplicates, but not really a problem --->
										<!--- and add these extranodes into the elementTreeCache table --->
										<CFSET additionalElementList = listappend(valuelist(getAdditionalElements.parentIDList),valuelist(getAdditionalElements.node))>
											<cfquery name="insertIntoElementTreeCache" datasource="#application.sitedatasource#">
											insert into elementTreeCacheData (elementTreeCacheID,elementID, sortOrder)
											select distinct <cf_queryparam value="#elementTreeCacheID#" CFSQLTYPE="CF_SQL_INTEGER" >, ecdfull.elementid, ecdfull.SortOrder
											from elementTreeCacheData ecdFull left join elementTreeCacheData ecd on ecdfull.elementid = ecd.elementid and ecd.elementTreeCacheid =  <cf_queryparam value="#elementTreeCacheID#" CFSQLTYPE="CF_SQL_INTEGER" >
											where
												ecdFull.elementTreeCacheID =  <cf_queryparam value="#fullTreeDetails.elementTreeCacheID#" CFSQLTYPE="CF_SQL_INTEGER" >
												and ecdFull.elementid  in ( <cf_queryparam value="#additionalElementList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and ecd.elementid is null
											</cfquery>

										<cfset elementlist = listappend(elementlist,additionalElementList)>

									</cfif>
								</cfif>



								<!--- <cfset weightingQuery= application.com.relayElementTree.getElementWeightingsForCurrentUser (elementIdlist = elementlist)> --->
								<cfset weightingQuery= application.com.relayElementTree.getElementWeightingsForCurrentUser (cacheid = elementTreeCacheID)>


							<cfif useUnknownUserCache>
								<!--- cache this unknownuser tree into an application variable for future use --->
								<cflock timeout = "60" throwontimeout="Yes" name="FullTree#topelementid#" type="EXCLUSIVE">
									<cfset application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID] = structNew()>
									<cfset application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID].elementList = elementList>
									<cfset application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID].elementTreeCacheID = elementTreeCacheID>
									<cfset application.elementTreeCache [topElementID].unknownPersonTrees[request.relaycurrentuser.Content.showForCountryID].weightingQuery = weightingQuery>
								</cflock>
							</cfif>



					</cfif>



					<cfset session.elementTreeCache[topElementID].elementlist = elementList>
					<cfset session.elementTreeCache[topElementID].date = now()>
					<cfset session.elementTreeCache[topElementID].isLoggedIn = request.relayCurrentuser.isloggedin>
					<cfset session.elementTreeCache[topElementID].weightingQuery = weightingQuery>
					<cfset session.elementTreeCache[topElementID].applicationCachedContentVersionNumber = application.cachedContentVersionNumber>
					<cfset session.elementTreeCache[topElementID].elementTreeCacheID = elementTreeCacheID>



				</cfif>

			</cflock>

				<!--- at some point we need to clear up the ElementTreeCacheTable, I'll do it when a refresh has been required --->
<!---
	WAB commented out 2007-06-01 because it causes all sorts of timeout problems.
	Still need to do some tidying, but have an idea to get it done by a separate process
--->
			<cfif  refreshrequired >

				<!---
				Now done with housekeepingsheduler
				<cfset tidyElementTreeCacheTableCheck()>		<!--- moved outside of the lock and moved into the person function  2007-03-16 --->
				--->

			</cfif>

			<cfreturn session.elementTreeCache[topElementID]>

	</cffunction>

	<cffunction name="getTreeForCurrentUser" access="public" returntype="query" output="no">
			<cfargument name="topElementID" type="numeric" required="yes">
			<cfset var getElements = "">
			<!--- first get query of all elements in tree --->
			<cfset var fullTree = getFullTreeFromCache(topElementID)>

			<!--- now get details of user's own tree --->
			<cfset var userTreeDetails = getTreeDetailsForCurrentUserFromCache(topElementID)>

			<cfset var weightingQuery = userTreeDetails.weightingQuery>

			<!--- problem because the query below ends up reordering the fullTree query and mucking up the cache
				so I actually need to take a copy of it
				I could resort afterwards instead
			 --->
			<cfset var Tree = duplicate(fulltree)>

			<!--- this query now filters the whole query of elements by the list of elements stored for this person's session --->
			<cfquery name="getElements" dbtype="query">
			select
				<cfif request.relaycurrentuser.isLoggedIn>
					showOnMenuLoggedin as showOnMenu,
				<cfelse>
					showOnMenuLoggedOut as showOnMenu,
				</cfif>
					showOnMenu as unresolvedShowOnMenu,
				*
			from
				tree , weightingQuery
			where
				tree.node = weightingQuery.id
				order by tree.sortorder
<!---
				and fulltree.node in (#userTreeDetails.elementlist#)  <!--- actually don't need this because the join to weighting query does the filter --->
 --->			</cfquery>



<!--- 			<!--- the QoQ mucks up the order of the original query - needs to be resorted --->
			<cfquery name="fullTree" dbtype="query">
			select
				*
			from
				fulltree
			order by fulltree.sortorder
			</cfquery>

 --->
 			<!---  need to update the showonMenuColumn, QoQ does not seem to handle a case statement--->

			<!---
			Don't need to do this now, done in the sql stored procedure giving columns showOnMenuLoggedIn/Out
			<cfset getElements = resolveShowOnMenuColumn(getElements)>
			 --->


			<cfreturn getElements>

	</cffunction>

	<cffunction name="getElementTreeCacheIDForCurrentUser" access="public" returntype="string" output="no">
			<cfargument name="topElementID" type="numeric" required="yes">

		<cfif not structKeyExists(session,"elementTreeCache") or not structKeyExists(session.elementTreeCache,topElementID) >
			<cfset getTreeDetailsForCurrentUserFromCache	(topElementID)>
		</cfif>

		<cfreturn session.elementTreeCache [topElementID].elementTreeCacheID>

	</cffunction>


	<!--- WAB 2010/10/26 simplified and run by housekeeping --->
	<cffunction name="tidyElementTreeCacheTable" access="public"  output="yes" returntype="boolean">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="numberToDeletePerBatch" type="numeric" default = "100">

			<cfset var result = true>
			<cfset var deleteElementTreeCaches = '' />
			<cfset var checkElementTreeCaches = '' />
					<!--- also delete a number of session trees which are more than 2 days old --->
					<cfquery name="deleteElementTreeCaches" datasource="#applicationScope.sitedatasource#">
					DELETE from elementtreecache
					where
					elementtreecacheID in (
						select top #numberToDeletePerBatch# elementtreecacheID
						from elementtreecache
						where
						cachename like 'SessionTree%'
						and lastupdated < getdate()-2  -- more than two days old
					)
					</cfquery>


					<cfquery name="checkElementTreeCaches" datasource="#applicationScope.sitedatasource#">
					select 1 where exists (
						select 1
						from elementtreecache
						where
						cachename like 'SessionTree%'
						and lastupdated < getdate()-2  -- more than two days old
					)
					</cfquery>


		<cfreturn result>
	</cffunction>

	<cffunction name="tidyElementTreeCacheTableForASession" access="public"  output="yes" returntype="boolean">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="SessionID" type="string" >

			<cfset var result = true>
			<cfset var deleteElementTreeCaches = '' />

				<cfquery name="deleteElementTreeCaches" datasource="#applicationScope.sitedatasource#">
				DELETE from elementtreecache
				where
				cachename  like  <cfqueryparam value="SessionTree_#sessionid#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>




		<cfreturn result>
	</cffunction>



	<!--- takes a tree and resoves the items which have "Only showonMenu when logged in" and "only showOnMenu when logged Out" to show and not show  --->
	<cffunction access="private" name="resolveShowOnMenuColumn" returnType="query"	>
		<cfargument name="elementTree" type="query" required="yes">

			<cfloop query = elementTree>
				<cfswitch expression="showOnMenu" >
					<cfcase value = -1>
						<cfif request.relaycurrentUser.isloggedIn>
							<cfset querySetCell(elementTree,showOnMenu,0,currentRow)>
						<cfelse>
							<cfset querySetCell(elementTree,showOnMenu,1,currentRow)>
						</cfif>
					</cfcase>
					<cfcase value=2>
						<cfif request.relaycurrentUser.isloggedIn>
							<cfset querySetCell(elementTree,showOnMenu,1,currentRow)>
						<cfelse>
							<cfset querySetCell(elementTree,showOnMenu,0,currentRow)>
						</cfif>
					</cfcase>
				</cfswitch>
			</cfloop>


		<cfreturn elementTree>
	</cffunction>

	<cffunction access="public" name="getTopElementIDPartnerContent" returnType="string"	>
		<!--- WAB 2005-05-25
			this code cut out of displayBorder
			works out what the top id is for the current site
		 --->
		<cfset var ID = "">

			<!--- a request scope variable TopElementIDPartnerContent can be defined in RelayINI
			to override the one in the application scope  --->

			<cfif isDefined("request.TopElementIDPartnerContent")>
				<cfset ID = request.TopElementIDPartnerContent>
			<cfelse>
				<cfset ID = application.TopElementIDPartnerContent>
			</cfif>

		<cfreturn ID>


	</cffunction>

	<cffunction access="public" name="getCurrentUsersElementIDListForTree" returnType="string"	>
		<!--- returns the cached list of elements that the current user has rights to  --->
			<cfargument name="topElementID" type="numeric" default = "#application.com.relayElementTree.getTopElementIDPartnerContent()#">

			<cfreturn getTreeDetailsForCurrentUserFromCache(topElementID).elementList>

	</cffunction>


<!--- 	<cffunction access="public" name="isThisElementInCurrentUsersTree" returnType="string"	>
		<!--- returns the cached list of elements that the current user has rights to  --->
			<cfargument name="topElementID" type="numeric" default = "#application.com.relayElementTree.getTopElementIDPartnerContent()#">
			<cfargument name="ElementID" type="String" required = true>


	</cffunction>
 --->


	<!--- very simple function for checking whether an element is in a tree
		handles eid or etid which makes
	 --->
	<cffunction access="public" name="isElementInATree" returnType="boolean" hint="Checks a passed elementID is in the passed elementTree">
			<cfargument name="elementTree" type="query" required = true>
			<cfargument name="ElementID" type="String" required = true>

			<cfset var check = "">
			<cfset var result = false>

			<cfif elementID is not "">
				<cfquery name="check" dbtype="query">
					select 1 from elementTree where
					<cfif isNumeric(elementID)>
						node = #elementID#
					<cfelse>
						upper(elementTextID) = '#ucase(elementID)#'
					</cfif>

				</cfquery>

				<cfif check.recordCOunt is not 0 >
					<cfset result = true>
				</cfif>
			</cfif>

			<cfreturn result>

	</cffunction>


	<cffunction access="public" name="getBranchFromTree" returnType="query"
	hint ="Returns a branch from an existing element tree.  Generation column is updated to reflect position in new tree
	"

	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="topOfBranchID" type="numeric" required = "yes">
		<cfargument name="returnTopOfBranch" type="boolean" required = "no" default="true">

			<cfset var getElements = 	"">
			<cfset var getTopOfBranch = 	"">

			<cfquery name="getTopOfBranch" dbtype="query">
			select generation from elementTree where node = #topOfBranchID#
			</cfquery>

			<cfif getTopOfBranch.recordCount is not 0 >
				<cfquery name="getElements" dbtype="query">
				select generation + 1 - #getTopofBranch.generation# as generation,
					*
				from elementTree
				where
					(',' + parentIDlist + ',' like '%#topofbranchid#%'
					<cfif returnTopOfBranch>
						or node = #topOfBranchID#
					</cfif>
					)
				</cfquery>
			<cfelse>
				<!--- top of branch not found in tree so return a blank query --->
				<cfquery name="getElements" dbtype="query">
				select 0 as generation,
					*
				from elementTree
				where
					1 = 0
				</cfquery>
			</cfif>


		<cfreturn getElements>

	</cffunction>


	<cffunction access="public" name="convertTreeToMenuTree" returnType="query"
	hint ="Takes an element tree query and returns a query suitable for outputing on a menu.
		You specify what you want the menu to look like in terms of number of levels, whether to show siblings and children etc.
	"

	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="currentelementID" type="numeric" required="yes">
		<cfargument name="numberOfGenerations" type="numeric" default="2">
		<cfargument name="showCurrentElementBranch" type="boolean" default="true">
		<cfargument name="showCurrentElementSiblings" type="boolean" default="true">
		<cfargument name="showCurrentElementChildren" type="boolean" default="true">
		<cfargument name="showCurrentElementUncles" type="boolean" default="true">
		<cfargument name="includeTopElement" type="boolean" default="false">
		<cfargument name="numberOfGenerationsOfChildren" type="numeric" default="1">
		<cfargument name="maxGenerationOfCurrentElementBranch" type="numeric" default="99">

		<cfset var getElements = 	convertTreeToBranch (elementtree = elementtree,
												justShowMenuItems = true,
												topOfBranchId = iif(elementTree.node is not "",elementTree.node,0) ,  <!--- top id should be first in the query I hope! --->
												numberOfGenerations = numberOfGenerations,
												showCurrentElementBranch = showCurrentElementBranch,
												showCurrentElementSiblings = showCurrentElementSiblings,
												showCurrentElementChildren = showCurrentElementChildren,
												showCurrentElementUncles = showCurrentElementUncles,
												currentElementID = currentElementID,
												includeTopElement = includeTopElement ,
												numberOfGenerationsOfChildren = numberOfGenerationsOfChildren,
												maxGenerationOfCurrentElementBranch = maxGenerationOfCurrentElementBranch
												)>

		<cfreturn getElements>

	</cffunction>


	<cffunction access="public" name="convertTreeToMenuBranch" returnType="query"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="topOfBranchID" type="numeric" required = "yes">
		<cfargument name="currentelementID" type="numeric" required="yes">
		<cfargument name="numberOfGenerations" type="numeric" default="2">
		<cfargument name="showCurrentElementBranch" type="boolean" default="true">
		<cfargument name="showCurrentElementSiblings" type="boolean" default="true">
		<cfargument name="showCurrentElementChildren" type="boolean" default="true">
		<cfargument name="showCurrentElementUncles" type="boolean" default="true">
		<cfargument name="includeTopElement" type="boolean" default="false">
		<cfargument name="numberOfGenerationsOfChildren" type="numeric" default="1">
		<cfargument name="maxGenerationOfCurrentElementBranch" type="numeric" default="99">


		<cfset var getElements = 	convertTreeToBranch (elementtree = elementtree,
												justShowMenuItems = true,
												topOfBranchId = topOfBranchId,
												currentElementID = currentElementID,
												numberOfGenerations = numberOfGenerations,
												showCurrentElementBranch = showCurrentElementBranch,
												showCurrentElementSiblings = showCurrentElementSiblings,
												showCurrentElementChildren = showCurrentElementChildren,
												showCurrentElementUncles = showCurrentElementUncles,
												includeTopElement = includeTopElement ,
												numberOfGenerationsOfChildren = numberOfGenerationsOfChildren,
												maxGenerationOfCurrentElementBranch = maxGenerationOfCurrentElementBranch
												)>

		<cfreturn getElements>

	</cffunction>


	<cffunction access="public" name="getBranchWhichContainsCurrentElementStartingAtGenerationX" returnType="query"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="currentelementID" type="numeric" required="yes">
		<cfargument name="topOfBranchGeneration" type="numeric" required = "yes">

		<cfset var topOfBranchID = '' />
		<cfset var getCurrentElementInfo = '' />
		<cfset var result = '' />

			<cfquery name="getCurrentElementInfo" dbtype="query">
			select parentid,parentIDList, generation,node from elementTree where node = #currentelementID#
			</cfquery>

			<cfif listlen(getCurrentElementInfo.parentIDList) gte topOfBranchGeneration>
				<!--- have found the top of the branch somewhere above the current element--->
				<cfset topOfBranchID = listgetat(getCurrentElementInfo.parentIDList,topOfBranchGeneration)>
			<cfelseif listlen(getCurrentElementInfo.parentIDList) is topOfBranchGeneration -1>
				<!--- the current element is the top of the branch --->
				<cfset topOfBranchID = currentElementiD>
			<cfelseif getCurrentElementInfo.recordcount is 0>
				<!--- element does not appear to be in tree, return a blank query--->
				<cfquery name="result" dbtype="query">
				select * from elementTree where 1 = 0
				</cfquery>
				<cfreturn result>
			<cfelse>
				<!--- in this case the currentElement must be above the generation specified, not sure what to do! --->
				<cfset topOfBranchID = currentElementiD>
			</cfif>

			<cfreturn 	convertTreeToMenuBranch (argumentCollection=arguments,topOfBranchID = topOfBranchID)>


	</cffunction>


<!--- Added WAB 2007-03-14 to make use of showonsecondarynavigation--->
	<cffunction access="public" name="getSecondaryNavigationBranchFromTree" returnType="query"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="topOfBranchID" type="numeric" required = "yes">
<!--- 		<cfargument name="currentelementID" type="numeric" required="yes">						 --->
		<cfargument name="numberOfGenerations" type="numeric" default="2">
		<cfargument name="includeTopElement" type="boolean" default="false">
		<cfargument name="numberOfGenerationsOfChildren" type="numeric" default="1">
		<cfargument name="sortOrder" type="string" default="sortOrder">


		<cfset var getElements = 	convertTreeToBranch (elementtree = elementtree,
												justShowSecondaryNavigationItems = true,
												topOfBranchId = topOfBranchId,
												numberOfGenerations = numberOfGenerations,
												includeTopElement = includeTopElement,
												sortOrder = sortOrder
												)>


		<cfreturn getElements>

	</cffunction>


	<!---
	Gets various sorts of branches of a tree.

		all require
			elementTree query
			ID of the top of the branch

		Then you can use:
			NumberofGenerations  -  the number of generations to go down the tree.  Note that the top element counts as 1 generation

			justShowMenuItems   - just brings back items which are marked to show on the main navigation

			justShowSecondaryNavigationItems	- just brings back items marked to show on secondary navigation


		To get more complicated:
			Imagine that your primary navigation shows just 1 generation below the top (ie numberOfGenerations = 2)
			However say that your current element is in generation 4 and you want the display the tree down to that element
			you need to:

			pass in currentElementID
			set showCurrentElementBranch = true

			You can then decide whether you want to show the siblings, uncles and children of the current element and define numberOfGenerationsOfChildren
			You can also limit how many generations you show on the way down to the currentelement (maxGenerationOfCurrentElementBranch)




	eg:



	get branch of a tree down to the current element
	can then get children of the current element  - showCurrentElementChildren
	and siblings of current element - showCurrentElementSiblings
	and all the uncle and grand.. uncles  - showCurrentElementUncles




	numberOfGenerations works a bit oddly! if you set it to anything other than 0 you get all the siblings down the branch for that number of generations.


	WAB 2007-03-13 changed so that currentElementID is not required


		WAB 2007-12-18 had a problem when started using trees where sections dynamically appeared in more than one place in the tree
		meant that it was possible for the same item to appear more than once with the same parentid so filtering by parentid could give duplicate items,
		however the parentidlist (ie the path to a given element) is always unique so started to use that instead
		however there is actually no guarantee which path will be used (basically the first it comes across).  Think that this would only possibly be a problem when trying to do a breadcrumb
	 --->

	<cffunction access="public" name="convertTreeToBranch" returnType="query"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="topOfBranchID" type="numeric" required = "yes">
		<cfargument name="currentelementID" type="numeric" required="no">
		<cfargument name="numberOfGenerations" type="numeric" default="2">
		<cfargument name="showCurrentElementBranch" type="boolean" default="true">
		<cfargument name="showCurrentElementSiblings" type="boolean" default="true">
		<cfargument name="showCurrentElementChildren" type="boolean" default="true">
		<cfargument name="showCurrentElementUncles" type="boolean" default="true">
		<cfargument name="includeTopElement" type="boolean" default="false">
		<cfargument name="numberOfGenerationsOfChildren" type="numeric" default="1">
		<cfargument name="maxGenerationOfCurrentElementBranch" type="numeric" default="99">
		<cfargument name="justShowMenuItems" type="boolean" default="false">
		<cfargument name="justShowSecondaryNavigationItems" type="boolean" default="false">	<!--- WAB 2007-03-14 --->
		<cfargument name="sortOrder" type="string" default="sortOrder">
		
			<cfset var getCurrentElementInfo = "">
			<cfset var getTopOfBranch = "">
			<cfset var generationOffset = 0>
			<cfset var getElements = "">
			<cfset var topElementID = elementTree.node> <!--- topelement should be the first item in the list --->
			<cfset var topOfBranchParentIDList = '' />
			<cfset var maxgeneration = '' />

			<cfif isdefined("currentelementID")>
				<cfquery name="getCurrentElementInfo" dbtype="query">
				select parentid,parentIDList, generation,node from elementTree where node = #currentelementID#
				</cfquery>
			</cfif>

			<!--- need to work out how to convert generations in the original tree to generations in the branch--->
			<cfif topElementID is not topOfBranchID and topOfBranchID is not 0>
				<cfquery name="getTopofBranch" dbtype="query">
				select *  from elementTree where node = #topOfBranchID#
				</cfquery>
				<cfset topOfBranchParentIDList = getTopofBranch.parentIDList> <!--- WAB 2007-12-18 --->
				<cfif getTopofBranch.recordCount is not 0>
					<cfset maxgeneration = getTopofBranch.generation + numberofgenerations -1>   <!--- WAB 2005-10-24 added the -1, didn't seem to work otherwise! --->
					<cfset generationoffset =  1 - getTopofBranch.generation >
				<cfelse>
					<!--- WAB added this check 2007/12/03
					if no record returned for the top of branch then we should just return an empty query here
					as it happens getTopOfBranch is such a query!
					--->
					<cfreturn getTopOfBranch>

				</cfif>

			<cfelse>
				<cfset maxgeneration = #numberofgenerations#>
				<cfset topOfBranchParentIDList = elementTree.parentIDList>  <!--- WAB 2007-12-18 ie parent list of first item in query which is the top of branch required --->
			</cfif>

			<cfquery name="getElements" dbtype="query">
			select
				generation + #generationOffset# as generation,
				*
			from elementTree

			where
			1 = 1
			<cfif justShowMenuItems>
			and showOnMenu = 1
			</cfif>

			<cfif justShowSecondaryNavigationItems>
			and showOnSecondaryNav = 1
			</cfif>


			<cfif topElementID is not topOfBranchID>
				<!--- elements have to be children of the top of branch--->

		<!--- WAB altered 2007-12-18 see comment above
			and  (',' + parentIDlist + ',' like '%#topofbranchid#%' or node = #topOfBranchID#) --->
					and  (parentIDlist like '#topOfBranchParentIDList#,#topOfBranchID#%' or node = #topOfBranchID#)
			</cfif>

			<cfif not includeTopElement>
				<!--- don't include the top node if not wanted --->
				and node <> #topofbranchid#
			</cfif>

			and

			(
			<!--- get all the items at the top of the tree down to the required generation --->
			generation <= #maxgeneration#

			<cfif isdefined("currentelementID") and getCurrentElementInfo.recordcount is not 0>
				<cfif showCurrentElementBranch >
				or (
						(
						node in (#getCurrentElementInfo.parentidlist#)  <!--- gets parents of current record --->
						or node = #currentelementid# 			<!--- gets current record --->
						)
					and generation + #generationoffset# <= #maxGenerationOfCurrentElementBranch#
					)
				</cfif>

				<cfif showCurrentElementSiblings>
				or (
					parentid = #getCurrentElementInfo.parentid# 	 		<!--- gets siblings of current record --->
					)
				</cfif>

				<cfif showCurrentElementUncles>
				or (
					parentid in (#getCurrentElementInfo.parentidlist#)
					and
					parentid <> #getCurrentElementInfo.parentid# <!--- gets siblings of parents of current record --->
					)
				</cfif>

				<cfif showCurrentElementChildren>
				or  (
						(parentid = #currentelementid#      <!---  gets children  of current record --->
						<cfif numberOfGenerationsOfChildren gt 1>
						or ',' + parentIDlist + ',' like '%#getCurrentElementInfo.node#%'
						</cfif>
						)
						and generation <= #getCurrentElementInfo.generation# + #numberOfGenerationsOfChildren#
					)
				</cfif>
			</cfif>
			)

			order by #arguments.sortOrder#
			</cfquery>

		<cfreturn getElements>


	</cffunction>


	<!--- WAB 2007/12/03  function deletes all children of an item (and optionally the item itself) from a tree
		can pass in a list of branchIds
	--->
	<cffunction access="public" name="deleteBranchFromTree" returnType="query">
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="topOfBranchIdsToDelete" type="string" default="">		<!--- can be list --->
		<cfargument name ="deleteTopOfBranch" default = true>
		<cfargument name="topOfBranchIdsToKeep" type="string" default="">		<!--- can be list --->

		<cfset var getElements = "">
		<cfset var thisBranchID = '' />

		<cfif topOfBranchIdsToDelete is not "" and topOfBranchIdsToKeep is not "">
			<cfoutput>Either pass argument topOfBranchIdsToDelete or topOfBranchIdsToKeep, not both</cfoutput><CF_ABORT>
		</cfif>

			<cfquery name="getElements" dbtype="query">
			select * from
			elementtree

			where

				<cfif  topOfBranchIdsToDelete is not "">
					 (1=1
					<cfloop index="thisBranchID" list = "#topOfBranchIdsToDelete#">
						and ',' + parentIDlist + ',' not like '%#thisBranchID#%'
						<cfif deleteTopOfBranch>
						 and node <> #thisBranchID#
						 </cfif>
					 </cfloop>
					)
				<cfelseif topOfBranchIdsToKeep is not "">
					 (
						1 = 0
					<cfloop index="thisBranchID" list = "#topOfBranchIdsToKeep#">
						or ',' + parentIDlist + ',' like '%,#thisBranchID#,%'
						or node = #thisBranchID#
					 </cfloop>

					 )
				</cfif>

			</cfquery>

		<cfreturn getElements>

	</cffunction>


	<cffunction access="public" name="getElementFlagData" returnType="query"output="no"	>
		<cfargument name="cacheID" type="numeric" required="no">	<!--- one of cacheid or elementIDlist is required --->
		<cfargument name="elementIDList" type="string" required="no">

			<cfset var flagdata = "">
			<cfset var flagjoins = "">
			<cfset var i = "">
			<cfset var FullWeightingPeriod = '' />
			<cfset var mediumWeightingPeriod = '' />
			<cfset var mediumWeightingDecaysTo = '' />
			<cfset var lowWeightingPeriod = '' />
			<cfset var lowWeightingDecaysTo = '' />

			<cfparam name = "request.elementFlags"	default = "">

					<cfset flagjoins = application.com.flag.getJoinInfoForListOfFlags(request.elementFlags)>
					<cfset FullWeightingPeriod = 14.0>  <!--- days that weighting stays at 1 --->
					<cfset mediumWeightingPeriod = 14.0>  <!--- days that weighting decays from 1 to #mediumWeightingDecaysTo#--->
					<cfset mediumWeightingDecaysTo = .8>
					<cfset lowWeightingPeriod = 28.0>	<!--- days that weighting decays from #mediumWeightingDecaysTo#  to #lowWeightingDecaysTo#  --->
					<cfset lowWeightingDecaysTo = .1>

					<!--- this query could probably be built automatically from some of my flag code --->
					<CFQUERY NAME="flagdata" DATASOURCE="#application.SiteDataSource#">
						select
							distinct    <!--- WAB 2007/12/12 / 2008/01/07  had to add this distinct - in some cases an element can appear more than once in a tree (if using the getChildrenfromparentid feature such as on Lexmark) but we only every want an id to appear once in this query because if it appears twice then we can get duplications when joining to this query in QoQ--->
							<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
								#flagjoins[i].selectField# as  #flagjoins[i].alias# ,
							</cfloop>

						case
							<!--- full weighting period --->
							when getdate() - e.created <  <cf_queryparam value="#FullWeightingPeriod#" CFSQLTYPE="CF_SQL_integer" >  then 1.0

							<!--- medium decay period --->
							when getdate() - e.created <  <cf_queryparam value="#FullWeightingPeriod#" CFSQLTYPE="CF_SQL_integer" >  + #mediumWeightingPeriod#
									then 1- (datediff(d,e.created + #FullWeightingPeriod#,getdate()))/#mediumWeightingPeriod# * (1-#mediumWeightingDecaysTo#)

							<!--- low decay period --->
							when getdate() - e.created <  <cf_queryparam value="#FullWeightingPeriod#" CFSQLTYPE="CF_SQL_integer" >  + #mediumWeightingPeriod# + #lowWeightingPeriod#
									then #mediumWeightingDecaysTo# -
											(
												(#mediumWeightingDecaysTo#-#lowWeightingDecaysTo#)
													*
												(datediff(d , e.created + #FullWeightingPeriod# + #mediumWeightingPeriod#,getdate())
												  /#lowWeightingPeriod#)
											)

							<!--- low period --->
							else #lowWeightingDecaysTo# end


							as dateWeighting,

						 e.id

						 from
								element e
							<cfif structKeyExists(arguments,"cacheid")>
									inner join
								elementtreecachedata ecd
									 on ecd.elementtreecacheID = #cacheid# and e.id = ecd.elementid
							</cfif>

							<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
								#flagjoins[i].join#
							</cfloop>

							<cfif structKeyExists(arguments,"elementIDList")>
								where
								<cfif elementIDList is not "">
									e.id  in ( <cf_queryparam value="#elementIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
								<cfelse>
									1=0
								</cfif>

							</cfif>


					</CFQUERY>

				<cfreturn flagData>

	</cffunction>


	<cffunction access="public" name="getElementWeightingsForCurrentUser" returnType="query"	>
		<cfargument name="elementIDList" type="string" required="no">
		<cfargument name="cacheid" type="numeric" required="no">

		<cfreturn getElementWeightingsForAPerson (argumentCollection = #arguments#, personid = request.relaycurrentuser.personid)>

	</cffunction>



	<cffunction access="public" name="getElementWeightingsForAPerson" returnType="query"	>
		<cfargument name="elementIDList" type="string" required="no">
		<cfargument name="cacheid" type="numeric" required="no">
		<cfargument name="personid" type="numeric" required="yes">

				<cfset var flagdata = "">
				<cfset var personjoins = "">
				<cfset var elementjoins = "">
				<cfset var i = "">


				<cfparam name = "request.elementWeightingFlags.Person"	default = "">
				<cfparam name = "request.elementWeightingFlags.Element"	default = "">

				<cfset personjoins = application.com.flag.getJoinInfoForListOfFlags(request.elementWeightingFlags.Person)>
				<cfset elementjoins = application.com.flag.getJoinInfoForListOfFlags(request.elementWeightingFlags.Element)>

					<!--- flagweighting
						wab 2005-09-07 modified so that value ranges from 0.1 to 1
						having a zero value didn't work when it was multiplied by the date weighting (cause the answer was always 0)
					--->

					<CFQUERY NAME="flagdata" DATASOURCE="#application.SiteDataSource#">
						select
						distinct    <!--- WAB 2007/12//12 had to add this distinct - in some cases an element can appear more than once in a tree (if using the getChildrenfromparentid feature such as on Lexmark) but we only every want an id to appear once in this query because if it appears twice then we can get duplications when joining to this query in QoQ--->
						dbo.minimumValue (9,
						<cfif arrayLen(personjoins) is not 0 and arrayLen(personjoins) is arrayLen(elementjoins)>
							<cfloop index = i from = "1" to = "#arrayLen(personjoins)#">
								<cfif i is not 1>+</cfif>(isnull(#personjoins[i].selectField#,1) * #elementjoins[i].selectField#)
							</cfloop>
						<cfelse>
							0
						</cfif>
						) / 10 + .1
						as flagweighting,


<!--- 						<cfloop index = i from = "1" to = "#arrayLen(personjoins)#">
							isnull(#personjoins[i].selectField#,1) as flagPerson#personjoins[i].alias#, #elementjoins[i].selectField# as flagElement#elementjoins[i].alias#,
						</cfloop>
 --->
						case when exists (select 1 from entityTracking where entityid = e.id and entityTypeID =  <cf_queryparam value="#application.entityTypeID["Element"]#" CFSQLTYPE="CF_SQL_INTEGER" > ) then 1 else 0 end as visited,
						 e.id

						 from element e

							<cfif structKeyExists(arguments,"cacheid")>
									inner join
								elementtreecachedata ecd
									 on ecd.elementtreecacheID = #cacheid# and e.id = ecd.elementid
							</cfif>


						<cfloop index = i from = "1" to = "#arrayLen(elementjoins)#">
							#elementjoins[i].join#
						</cfloop>

						 , person p
						<cfloop index = i from = "1" to = "#arrayLen(personjoins)#">
							#personjoins[i].join#
						</cfloop>

							where
								p.personid = #personid#
						<cfif structKeyExists(arguments,"elementIDList")>

							<cfif elementIDList is not "">
								and  e.id  in ( <cf_queryparam value="#elementIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
							<cfelse>
								and 1 = 0
							</cfif>
						</cfif>

					</CFQUERY>

				<cfreturn flagData>

	</cffunction>




	<!---
	getElementsFlaggedWithParticularFlag

	WAB 2005-12-19 - added demotion of visited items

	--->

	<cffunction access="public" name="getElementsFlaggedWithParticularFlag" returnType="query"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="flagid" type="string" required="yes">
		<cfargument name="data" type="string" required="no" default = "1">
		<cfargument name="demoteVisitedElements" type="boolean" required="no" default = "false">

			<cfset var theFlagID = application.com.flag.getFlagStructure(flagID).flagid>
			<cfset var result = "">

			<cfif listfindNoCase(elementTree.columnlist,"FLAG_#theFlagID#") is 0 >
				<cfoutput>Flag #htmleditformat(flagid)# does not exist (or has not been cached - see request.elementFlags) </cfoutput>
				<cfquery name="result" dbtype="query">
					select * , 0 as overallweighting
					from
					elementTree
					where 1= 0
				</cfquery>
				<cfdump var="#result#">
			<cfelse>

				<cfquery name="result" dbtype="query">
					select * ,
					(flagweighting * dateweighting) as overallweighting

					from
					elementTree
					where
					FLAG_#theFlagID# = #data#

	 				order by <cfif demoteVisitedElements> visited , </cfif> overallweighting desc , sortorder asc  <!--- order by visited added 2005-12-19 --->
				</cfquery>


			</cfif>



			<cfreturn result>
	</cffunction>


	<!--- function to order an element tree (assumes that you have chosen some elements somehow and want them all at the same generation for a simple list --->
	<cffunction access="public" name="orderElementTree" returnType="query"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="orderBy" type="string" default="sortOrder" hint= "visited,overallweighting,sortorder">
		<cfset var result = "">

				<cfquery name="result" dbtype="query">
					select * ,
					(flagweighting * dateweighting) as overallweighting

					from
					elementTree

	 				order by <cf_queryObjectName value="#orderBy#">
				</cfquery>

			<cfreturn result>
	</cffunction>



	<cffunction access="public" name="getElementOfParticularGenerationFromCurrentBranch" returnType="numeric"	>
		<cfargument name="elementtree" type="query" required="yes">
		<cfargument name="currentelementID" type="numeric" required="yes">
		<cfargument name="generation" type="numeric" required="yes">

			<cfset var getCurrentElementInfo = "">
			<cfset var getElement = "">


			<cfquery name="getCurrentElementInfo" dbtype="query">
			select parentid,parentIDList from elementTree where node = #currentelementID#
			</cfquery>

			<cfquery name="getElement" dbtype="query">
			select node from elementTree where
			generation = #generation#
			and node in (#getCurrentElementInfo.parentIdList#,#currentelementID#)
			</cfquery>

		<cfif getElement.recordCOunt is 0 >
			<cfreturn 0>
		<cfelse>
			<cfreturn getElement.node>
		</cfif>


	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     2005-06-15 SWJ added for element linking
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLinkedElements" hint="Returns a query object containing entities linked to this element">
		<cfargument name="currentElementID" type="numeric" required="yes">
		<cfargument name="topElementID" type="numeric" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var getLinkedElements = "";
			var elementTree = getFullTreeFromCache(arguments.topElementID);
		</cfscript>

		<cfquery name="getLinkedElements" datasource="#arguments.dataSource#">
			select CASE WHEN HostElementID is not null THEN HostElementID ELSE entityID END as ElementID, el.entityID, elt.elementLinkType, el.elementLinkTypeTextID, el.additionalDescription, e.Headline
FROM         elementLink el INNER JOIN
                      elementLinkType elt ON el.elementLinkTypeTextID = elt.elementLinkTypeTextID LEFT OUTER JOIN
                      Element e ON el.entityID = e.id
			WHERE elementID = #arguments.currentelementID#
			order by el.elementLinkTypeTextID
		</cfquery>
		<cfreturn getLinkedElements>
	</cffunction>

	<cffunction access="public" name="incrementCachedContentVersionNumber" hint="Is called after elements are updated to blow caches">
		<cfargument name="updateCluster" type="string" required="no" default="true">
		<cfargument name="applicationScope" default="#application#">

			<cflock timeout="5" throwontimeout="No" name="incrementCachedContentVersionNumberLock" type="EXCLUSIVE">
				<cfset arguments.applicationScope.cachedContentVersionNumber = IncrementValue( applicationScope.cachedContentVersionNumber ) />
			</cflock>


		<cfif updateCluster >
			<cfset arguments.applicationScope.com.clusterManagement.updateCluster(updatemethod="incrementCachedContentVersionNumber",applicationScope =applicationScope)>
		</cfif>

		<cfreturn 	applicationScope.cachedContentVersionNumber>


	</cffunction>

	<cffunction access="public" name="cachedContentNeedsToBeBlown" hint="sets a variable so that we know that cache needs to be updated">
		<cfset session.CachedContentVersionNumberToBlow = application.cachedContentVersionNumber>
	</cffunction>

	<cffunction access="public" name="doesCachedContentNeedToBeBlown" returns="Boolean" hint="">
<!--- 		<CFOUTPUT>#session.CachedContentVersionNumberToBlow#</CFOUTPUT> --->
		<cfif isDefined("session.CachedContentVersionNumberToBlow") and session.CachedContentVersionNumberToBlow GTE application.cachedContentVersionNumber>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>



	<cffunction name="whyCantCurrentUserSeeThisElement" access="public" returntype="struct" output="no"
		hint = "returns struct.reason which can be NotInTree,NotLive,NoRights,NoCountryRights,LoginRequired,shouldbeOK"
	>
		<cfargument name="topElementID" type="numeric" required="yes">
		<cfargument name="ElementID" type="string" required="yes">

		<cfset var result = structNew()>
		<cfset var getElement = "">
		<cfset var checkStatus = "">
		<cfset var checkRights = "">
		<cfset var getelementviewrights = '' />

		<!--- 1.  is the element in the current tree? --->
		<cfset var elementTree = getFullTreeFromCache(topElementID)>

		<cfquery name="GetElement" dbtype="query">
		SELECT *,node as id,parent as parentid
		from elementTree
		where 1=1
		<cfif isNumeric(ElementID) >
			and node = #ElementID#
		<cfelseif elementID is not "">
			and upper(elementTextID) = '#ucase(ElementID)#'
		<cfelse>
			and 1 = 0
		</cfif>
		</cfquery>


		<cfif getElement.recordcount is 0 >
			<!--- Element not In current Tree --->
			<cfset result.reason   = "NotInTree">
			<cfreturn result>
		</cfif>


		<!---
		WAB 2010/02/02 LID 3058
		added the countryid - otherwise would get wrong answer on a Lexmark Portal (where countryid is related to the portal not the personid)
		also added parameter names (for clarity)
		 --->
		<cfset getelementviewrights = application.com.rights.Element(elementID = elementid,permission = 1,topID= TopElementID,personid = request.relaycurrentuser.Content.showForPersonID,countryid = request.relaycurrentuser.Content.showForCountryID)>
		<cfset result.rights = getelementviewrights>


		<!--- 2.  check for a draft status less than what is being currently requested--->
		<cfquery name="checkStatus" dbtype="query">
		select * from getelementviewrights.parentquery
		where ( statusid < #request.relaycurrentUser.content.Status# and statusid <4 ) or statusid = 5
		</cfquery>

		<cfif checkStatus.recordcount is not 0 >
			<!--- <cfoutput>Element #checkStatus.elementid# is not live</cfoutput> --->
			<cfset result.reason   = "NotLive">
			<cfreturn result>
		</cfif>


		<!--- WAB 2010/02/02 LID 3058
		moved Logged In test to be before the Rights test.
		Until person is logged in we don't necessarily know who they are so the rights test will often fail incorrectly.
 		So we need to give someone a "Please Log In" message before we give them a "Page Not Available" message.
		--->

		<!--- 3. check for need to be logged in --->
		<cfif getelementviewrights.loginRequired gt request.relaycurrentUser.isLoggedIn>
			<cfif getelementviewrights.loginRequired is 2 and request.relaycurrentUser.isLoggedIn is 1>
				<!--- <cfoutput>Must be logged in at level 2 to see this item</cfoutput> --->
				<cfset result.reason   = "LoginRequiredHigherLevel">
			<cfelse>
				<!--- <cfoutput>Must be logged in to see this item</cfoutput> --->
				<cfset result.reason   = "LoginRequired">
			</cfif>
			<cfreturn result>
		</cfif>

		<!--- 4. check for user rights and countryrights--->
		<cfif not getelementviewrights.hasrights>
			<cfquery name="checkRights" dbtype="query">
			select * from getelementviewrights.parentquery
			where recordrightsOK = 0
			</cfquery>

			<cfif checkRights.recordcount is not 0>
				<cfset result.reason   = "NoRights">
			<cfelse>
				<cfset result.reason   = "NoCountryRights">
			</cfif>
			<cfreturn result>

		</cfif>


		<cfset result.reason   = "shouldbeok">
		<cfreturn result>




	</cffunction>

<!--- WAB LID 2361 added test to the argument list so that tracking is done against the correct record--->
	<cffunction access="public" name="createSEIDQueryString" return="string" output="no">
			<cfargument name="LinkType" type="string" default="normal"><!--- preview,Unrestricted --->
			<cfargument name="elementid" type="string" required="true"><!--- can be id or textid --->
			<cfargument name="personid" type="numeric" default = 0>
			<cfargument name="commid" type="numeric" default = 0>
			<cfargument name="test" type="numeric" default = 0>   <!--- WAB added 2009/06/15, needed when tracking clicks from test communications --->
			<cfargument name="days" type="numeric" default="30">

			<cfset var x= "">
			<cfset var abbrev="">
			<cfset var result="">


			<cfswitch expression="#linkType#" >
				<cfcase value="preview,pr">
					<cfset abbrev="pr">
					<cfset result = elementid & "-" & abbrev>
				</cfcase>
				<cfcase value="unrestricted,se,secure">
					<cfset abbrev="se">
					<cfset x = elementid & "|" & abbrev & "|" & personid & "|" & commid & "|" & test & "|" & dateadd("d",days,now())>  <!--- Note that when adding test it was added infront of the date (to be the same as others, does mean that some existing links will fail--->
					<cfset x = application.com.encryption.encryptString(string=x)>
					<cfset result = elementid & "-" & abbrev & "-" & x>
				</cfcase>
				<cfcase value="tracked">
					<cfset abbrev="tr">
					<cfset x = elementid & "|" & abbrev & "|" & personid & "|" & commid & "|" & test>
					<cfset x = application.com.encryption.encryptString(string=x)>
					<cfset result = elementid & "-" & abbrev & "-" & x>
				</cfcase>
				<cfcase value="normal,e">
					<cfset result = elementid >
				</cfcase>

				<cfdefaultcase>
					<cfset result = elementid >
				</cfdefaultcase>
			</cfswitch>


			<cfreturn "seid="& result>

	</cffunction>



	<!---
	WAB 2009/03/13 LID 1962 mods to deal with SEID string having stuff on end
			factorised some of the code
	 --->
	<cffunction access="public" name="processSEIDValue" return="struct" output="yes">
		<cfargument name="theString" type="string" required="true">

		<cfset var result = structNew()>
		<cfset var plaintextlinkType = "">
		<cfset var plaintextelementID = "">
		<cfset var encrytedpart = "">
		<cfset var decrytedstring = "">
		<cfset var getElementID = "">
		<cfset var decryptedString = '' />

		<cfset result.elementid = 0>
		<cfset result.personid = 0>
		<cfset result.commid = 0>

		<!--- string is in the form  of either :
			elementid  - gives a normal link
			elementid-linktype   - some special sort of link which isn't secure
			elementid-linktype-encryptedpart - a secure link
		the encrypted bit is in the form elementid-linktype-personid-commid-validuntil
		--->


		<cfif listlen(thestring,"-") is 1>
			<!--- this is just a plain link, have to assume that it is OK --->
			<cfset result.linkisValid = true>
			<cfset result.elementid = listfirst(thestring,"-")>
			<cfreturn result>
		<cfelseif listlen(thestring,"-") gt 1>
			<cfset plaintextelementid = listgetat(thestring,1,"-")>
			<cfset plaintextlinktype = listgetat(thestring,2,"-")>
		<cfelse>
			<cfset result.linkisValid = false>
			<cfreturn "#result#">
		</cfif>

		<!--- WAB 2009/03/13 this section used to be repeated in every CFCASE, now in one place
		 --->
		<cfif listLen(theString,"-") gt 2>
			<CFSET encrytedpart = listgetat(thestring,3,"-/")>  <!--- WAB 2006-02-01 added in the / as a delimiter to fix a problem when someone had got a / at the end of their string -  won't cause problems because encrypted string can never have / in it--->
			<cfset decryptedString= application.com.encryption.decryptString(string=encrytedpart)><!--- WAb 2009/10/05 added name of the parameter LID 2725 --->
			<cfset result.elementid = listgetAt(decryptedString,1,"|")>
			<cfset result.linkType = listgetAt(decryptedString,2,"|")>
			<cfset result.personid = listgetAt(decryptedString,3,"|")>
			<cfset result.commid = listgetAt(decryptedString,4,"|")>
			<cfset result.test = listgetAt(decryptedString,5,"|")>
			<cfif plaintextlinktype is not result.linkType or plaintextelementID is not result.elementid>
					<!--- url has been tampered with --->
					<cfset result.linkisValid = false>
					<cfreturn result>
			</cfif>
		</cfif>

			<cfswitch expression="#plaintextlinkType#" >
				<cfcase value="pr">  <!--- preview link for relay internal user--->
						<!--- check that user is a relay internal user and if so add this element id to list of ids
						allow it to persist across logins
						--->
						<cfif request.relayCurrentUser.isValidRelayInternalUser and request.relayCurrentUser.isLoggedIn>
							<cfset application.com.relaycurrentuser.updateContentSettings( AdditionalshowelementIDs = plaintextelementid, ShowLinkToControlContentEvenIfILogOnAsSomeoneElse = true)>
							<cfset result.elementid = plaintextelementid>
							<cfset result.linkisValid = true>
						<cfelse>
							<cfset result.linkisValid = false>
						</cfif>

				</cfcase>
				<cfcase value="se"> <!--- unrestricted link to a secure page--->
					<cfset result.validUntil = listgetAt(decryptedString,6,"|")> <!--- WAB LID 2631 changed 5 to  when test added, could cause problem around release time with old links --->
					<cfif result.validUntil gt now()>
						<cfset result.linkisValid = true>
						<cfif not isNumeric(plaintextelementid)>
							<cfquery name = "getElementID" datasource = "#application.sitedatasource#">
								select id from element where elementtextid =  <cf_queryparam value="#plaintextelementid#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfquery>
							<cfset plaintextelementid = valueList(getElementID.ID)>
						</cfif>
						<cfset application.com.relaycurrentuser.updateContentSettings( AdditionalshowelementIDsIfStatusCorrect = plaintextelementid)>
					<cfelse>
						<cfset result.linkisValid = false>
					</cfif>
				</cfcase>
				<cfcase value="tr"> <!--- tracked link --->
						<cfset result.linkisValid = true>
				</cfcase>

				<cfdefaultcase>
					<cfset result.linkisValid = false>
				</cfdefaultcase>
			</cfswitch>

		<cfreturn "#result#">

	</cffunction>




<!---

	function whichTreeIsThisNodeIn
	returns the elementID of the tree that this node is in
	returns 0 if not found


	WAB 2005-10-31
--->

	<cffunction access="public" name="whichTreeIsThisNodeIn" return="numeric" output="yes">
		<cfargument name="elementid" type="numeric" required="true">

			<cfset	 var getParents = "">
			<cfset	 var PossibleTrees = "">
			<cfset	 var PossibleTreeIDs = "">
			<cfset	 var nodeID = "">

			<!--- get list of parents of the current node --->
			<cfquery name="getParents" datasource="#application.sitedatasource#" >
			exec getElementParents @elementID = #elementid#
			</CFQUERY>

			<!--- get list of all the possible trees --->
			<cfset possibleTrees = getTreeTopNodes()>
			<cfset possibleTreeIDs = valuelist(possibleTrees.id)>

			<!--- loop through until we find one of the parents in the list of trees --->
			<cfloop index="nodeid" list = "#valueList(getParents.elementid)#">
					<cfif listfindnocase(possibleTreeIDs, nodeid)>
							<cfreturn nodeid>
					</cfif>
			</cfloop>

			<!--- if not found, return the item at the top of the parent list --->
			<cfreturn getParents.elementid>

	</cffunction>



<!--- 2006/01/09 GCC  --->
	<cffunction access="public" name="getlinkBlockFlagID" hint="Returns the flagID from a specified flag group if it is related to 1. A product category or its parents (Sony1) or 2. the element or its parents">
		<cfargument name="eid" required="yes" type="numeric">
		<cfargument name="linkBlockFlagGroupID" required="yes" type="numeric">

		<cfscript>
			var linkBlockFlagID = "";
		</cfscript>
		<!--- if in a Sony product Tree --->
		<cfif structkeyexists(request,"productCategoryID") and request.productCategoryID neq "">
			<cfscript>
				linkBlockFlagID = application.com.sonyNavigationV2.getProductFlagGroupData_Inherited(treeID = request.productTreeID,categoryID = request.productCategoryID, flagGroupID=arguments.linkBlockFlagGroupID);
			</cfscript>
			<cfif isdefined('linkBlockFlagID')>
				<!--- check it is in flag group linkBlockFlagGroup --->
				<cfreturn linkBlockFlagID>
			</cfif>
		</cfif>
			<!--- not in a category or no flag found check the elements flag --->
			<cfscript>
				linkBlockFlagID = application.com.relayElementTree.getElementFlagGroupData_Inherited (elementid = arguments.eid,topelementID = request.TopElementIDPartnerContent,flagGroupID=arguments.linkBlockFlagGroupID);
			</cfscript>
			<cfif isdefined('linkBlockFlagID')>
				<cfreturn linkBlockFlagID>
			</cfif>
		<cfreturn>
	</cffunction>


<!---
WAB 2006-01-10

gets flagGroupData for a given element

initially written so that if no flag set on current element, would look for data in all parents until a result is found or we get to the parent

function looks for data in application.elementtreecache.  If cache has not been populated then gets data from database an populates cache

Originally used on a radio button, could work on checkboxes although recursing through parents until data is found might be slightly odd for checkboxes

 --->

	<cffunction access="public" name="getElementFlagGroupData_Inherited" return="string" output="yes">
		<cfargument name="elementid" type="numeric" required="true">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="topElementID" type="numeric" default = "#request.CurrentElementTree.node#">

		<cfreturn getElementFlagGroupData (elementID =elementID , flagGroupID = flagGroupID , topElementID = topElementID, useInheritence = true)>


	</cffunction>

	<cffunction access="public" name="getElementFlagGroupData" return="string" output="yes">
		<cfargument name="elementid" type="numeric" required="true">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="topElementID" type="numeric" default = "#request.CurrentElementTree.node#">
		<cfargument name="useInheritence" type="boolean" default="false">

		<cfset var flagGroupColumnName = "">
		<cfset var fullTree = getFullTreeFromCache(topElementID)>
		<cfset var rowIndexStruct = application.elementTreeCache[topElementID].rowIndexStruct>		<!--- bit nasty accessing application variable directly here --->
		<cfset var theDataQuery = "">
		<cfset var blankArray = arrayNew(1)>
		<cfset var flagDataForThisElement = '' />

		<!--- get flagGroupID --->
		<cfset arguments.flagGroupID = application.com.flag.getFlagGroupStructure(flagGroupID).flagGroupID>
		<cfset flagGroupColumnName = "flagGroup_#flagGroupID##iif(useInheritence,de('_I'),de(''))#">


		<!--- does the cached tree have this column --->
		<cflock timeout = "60" throwontimeout="Yes" name="FullTree#topelementid#" type="EXCLUSIVE">  <!--- WAB 2006-02-10 moved lock to outside the if--->
			<cfif not structKeyExists(fullTree,flagGroupColumnName)>
			<!--- create column in application cache --->
					<cfset queryAddColumn(fullTree,flagGroupColumnName,blankArray)>
			</cfif>
		</cflock>


			<!--- is there data for this element in the cache? --->
			<cfset flagDataForThisElement = fullTree[flagGroupColumnName][rowIndexStruct[elementID]] >

			<cfif flagDataForThisElement  is "" >  <!--- blank means that nothing has been cached yet, 0 is the value for cached but no data--->
			<!--- No --->

					<cfset thedataQuery = application.com.flag.getFlagGroupData (flagGroupID = flagGroupID, entityid = elementID)>
					<cfset flagDataForThisElement = valueList(thedataQuery.flagID)>


					<!--- is there any data? --->
					<cfif flagDataForThisElement is not "">
						<!--- yes - put in application cache --->
					<cfelseif elementID is not topElementID and useInheritence>
					<!--- no (and not top of tree) - recurse --->
						<cfset flagDataForThisElement = getElementFlagGroupData_Inherited (elementID =fullTree["parentID"][rowIndexStruct[elementID]], flagGroupID = flagGroupID , topElementID = topElementID)>
					<!--- no and top of tree - result = 0--->
					<cfelse>
						<cfset flagDataForThisElement = 0>
					</cfif>

					<cflock timeout = "60" throwontimeout="Yes" name="FullTree#topelementid#" type="EXCLUSIVE">
						<cfset querySetCell (fullTree,flagGroupColumnName,flagDataForThisElement,rowIndexStruct[elementID])>
					</cflock>


			</cfif>

			<cfreturn flagDataForThisElement>

	</cffunction>

	<cffunction access="public" name="createElementTypeStructure" return="string" output="yes">
		<cfargument name="applicationScope" default = "#application#">

		<cfset var tempTypeStruct = structNew()>
		<cfset var structureFunctions = '' />
		<cfset var regExp = '' />
		<cfset var thisType = '' />
		<cfset var inheritFrom = '' />
		<cfset var tempwizardControls = '' />
		<cfset var parametersStruct = '' />
		<cfset var labelStruct = '' />
		<cfset var wizardcontrolsStruct = '' />
		<cfset var tempStruct = '' />
		<cfset var childType = '' />
		<cfset var item = '' />
		<cfset var Types = '' />

		<CFQUERY NAME="Types" DATASOURCE="#applicationScope.SiteDataSource#">
		select *,
			case when isNull(allowedChildTypeIDLIst,'') = '' then convert(varchar,elementTypeID) else allowedChildTypeIDLIst END 	as allowedChildTypeIDList2
		from elementType
		order by inheritEditorSettingsFromTypeID,elementTypeID
		</CFQUERY>



		<cfset structureFunctions = createobject("component","relay.com.structurefunctions")>  <!--- this isn't necessary in application.com scope when cfc is initialised, so we will call as a regualr object --->
		<cfset regExp = createobject("component","relay.com.regExp")>  <!--- this isn't necessary in application.com scope when cfc is initialised, so we will call as a regular object --->


		<cfloop query = "Types">
			<cfset thisType = structureFunctions.queryRowToStruct(types,currentrow)>
			<cfset thisType.allowedChildTypesArray = listtoarray (allowedChildTypeIDList2)>
			<cfset thisType.allowedParentTypesArray = arrayNew(1)>
			<cfif thisType.inheritEditorSettingsFromTypeID is not "">
					<cfset inheritFrom = tempTypeStruct[thisType.inheritEditorSettingsFromTypeID]>
				<CFSET thisType.show = inheritFrom.show>
				<cfset thisType.label = inheritFrom.label>
				<cfset thisType.help = inheritFrom.help>
				<cfset tempwizardControls = inheritFrom.wizardcontrols>
				<cfif thisType.wizardPages is "">
					<cfset thisType.wizardPages = inheritFrom.wizardPages>
				</cfif>
			<cfelse>
				<CFSET thisType.show = structNew()>
				<cfset thisType.label = structNew()>
				<cfset thisType.help = structNew()>
				<cfset tempwizardControls = structNew()>

			</cfif>

			<!--- get items in the parameters column and merge them into the main structure--->
			<cfset parametersStruct = regexp.convertNameValuePairStringToStructure(thisType.parameters,",")>
			<cfset thisType = structureFunctions.Structmerge(thisType,parametersStruct)>

			<cfset labelStruct = regexp.convertNameValuePairStringToStructure(thisType.fieldlabels,",")>
			<cfset thisType.label = structureFunctions.Structmerge(thisType.label,labelStruct)>

			<cfset wizardcontrolsStruct = regexp.convertNameValuePairStringToStructure(thisType.wizardcontrols,",")>
			<cfset thisType.wizardControls =  structureFunctions.Structmerge(tempWizardControls,wizardcontrolsStruct)>

			<cfif not structKeyExists(thisType.wizardcontrols,"wysiwyg") and structKeyExists(thisType.wizardcontrols,"default")>
				<cfset thisType.wizardcontrols.wysiwyg = replaceNoCase(thisType.wizardcontrols.default,"textArea","WYSiWYG","ALL")>
			</cfif>
			<!--- pull out certain items into their own sub-structures   (those begining with SHOW or ending with HELP) --->
			<cfset tempStruct= structNew()>
			<cfloop collection="#thisType#" item="item">
				<cfif left(item,4) is "show" and item is not "SHOW">
					<cfset tempStruct[right(item,len(item)-4)] = thisType[item]>
					<!--- <cfset structDelete (tempStruct[elementTypeID],item)> --->
				</cfif>
			</cfloop>

			<cfset thisType.show =  structureFunctions.Structmerge(thisType.show,tempStruct)>

			<cfset tempStruct= structNew()>
			<cfloop collection="#thisType#" item="item">
				<cfif left(item,5) is "label" and item is not "label">
 					<cfset tempStruct[right(item,len(item)-5)] = thisType[item]>
					<!--- <cfset structDelete (tempStruct[elementTypeID],item)>  --->
				</cfif>
			</cfloop>
			<cfset thisType.label =  structureFunctions.Structmerge(thisType.label,tempStruct)>


			<cfloop collection="#thisType#" item="item">
				<cfif right(item,4) is "help" and item is not "help">
					<cfset tempStruct[left(item,len(item)-4)] = thisType[item]>
					<!--- <cfset structDelete (tempStruct[elementTypeID],item)> --->
				</cfif>
			</cfloop>
			<cfset thisType.help =  structureFunctions.Structmerge(thisType.help,tempStruct)>

			<cfset tempTypeStruct[elementTypeID]=thisType>
		</cfloop>
		<cfloop query = "Types">
			<cfloop index="childType" list = "#allowedChildTypeIDList2#">
				<cfset arrayappend (tempTypeStruct[childType].allowedParentTypesArray,elementTypeID)>
			</cfloop>
		</cfloop>



		<cfset arguments.applicationScope.elementType = tempTypeStruct	>

	</cffunction>

	<!--- 2006-05-19 AJC CR_SNY579 gets an elementID from a ElementTextID in a passed elementtree--->
	<cffunction access="public" name="getElementIDFromElementTextID" returnType="query" hint="Checks a passed elementID is in the passed elementTree">
			<cfargument name="elementTree" type="query" required = true>
			<cfargument name="ElementTextID" type="String" required = true>

			<cfset var getElementID = "">
			<cfset var parentID = '' />

			<cfquery name="getElementID" dbtype="query">
				select node as elementID from elementTree where
				upper(elementTextID) = '#ucase(ElementTextID)#'
			</cfquery>

			<cfreturn getElementID>

	</cffunction>

	<!--- 2015-12-01 WAB During PROD2015-290 Visibility Project.change some replaces to jsStringFormat --->
	<cffunction name="generateDTreeAddFunctions" access="public" output="false">
			<cfargument name="elementTree" type="query" required = true>
			<cfargument name="firstNodeIsRoot" type="boolean" default = true>
			<cfargument name="dtreeObjectName" type="string" default = "d">
			<cfargument name="editLink" default= "/elements/elementEdit.cfm">
			<cfargument name="showHAVetc" default= "false">

			<cfset var resultJS = "">
			<cfset var elementName = "">
			<cfset var additionalHTML = "">
			<cfset var parentID = '' />

			<cfsavecontent variable = "resultJS">
				<CFLoop Query="elementTree" >
					<cfif firstNodeIsRoot and currentrow is 1>
						<cfset parentID = -1>
					<cfelse>
						<cfset parentID = parent>
					</cfif>

					<cfset additionalHTML =''>
					<cfif showHAVetc>
							<cfset additionalHTML ='&nbsp;&nbsp;&nbsp;<a href="#editLink#?RecordID=#node#&frmWizardPage=WYSIWYGEditor" target="editFrame" title="Edit HTML" class="smalllink">H</a> <a href="#editLink#?RecordID=#node#&&frmWizardPage=attributes" target="editFrame" title="Edit content attributes" class="smalllink">A</a> <a href="#editLink#?RecordID=#node#&frmWizardPage=visibility" target="editFrame" title="Edit visibility and rights" class="smalllink">V</a> <a href="#editLink#?parentID=#node#" target="editFrame" title="Add child page" class="smalllink">+</a>'>
					</cfif>
						<!--- <cfoutput>#dtreeObjectName#.add('#node#','#parentID#','#replace(left(name,20),"'","\'","ALL")# <cfif statusid is not 4>(#status#)</cfif> <cfif editRights is not 1><img border = 0 src="/images/misc/padlock.gif"></cfif>','#JSStringFormat(editlink)#?RecordID=#node#','#replace(name,"'","\'","ALL")# <cfif statusid is not 4>(#status#)</cfif> <cfif recordRights is not "">RIGHTS: #replace(recordRights,"'","\'","ALL")# </cfif>EID: #node# &##10Right Click For Options','editFrame','','','','#lcase(elementTextID)#','#replace(additionalHTML,"'","\'","ALL")#',#iif(haschildrenNotInResultSet is 1 or haschildren is 1,de("true"),de("false"))#);</cfoutput>--->
						<!--- NJH 2011/06/21 - open element in new tab --->
						<cfset elementName = jsStringFormat(name)>
						<cfoutput>#dtreeObjectName#.add('#node#','#parentID#','#left(elementName,20)# <cfif statusid is not 4>(#status#)</cfif> <cfif editRights is not 1><span id="elementTreeLock" class="fa fa-lock"></span></cfif>','javascript:void(openNewTab(\'element_#node#\',\'#jsStringFormat(elementName)# (#IIF(elementTextID neq "",DE(elementTextID),DE(node))#)\',\'#editlink#?RecordID=#node#\',{iconClass:\'content\',reuseTab:true,reuseTabJSFunction: function () {return true}}))','#elementName# <cfif statusid is not 4>(#status#)</cfif> <cfif recordRights is not "">\rREQUIRED RIGHTS:\r #jsStringFormat(recordRights)# </cfif>\rEID: #node# &##10Right Click For Options','','','','','#lcase(elementTextID)#','#jsStringFormat(additionalHTML)#',#iif(haschildrenNotInResultSet is 1 or haschildren is 1,de("true"),de("false"))#);</cfoutput>
				</CFLoop>
			</cfsavecontent>

			<cfreturn resultJS>
	</cffunction>


<!--- WAb 2007/09/19 testing ideas --->
	<cffunction name="outputTreeAsUL" access="public" output=false>
		<cfargument name="elementTree" type="query" required = true>
		<cfargument name="currentElement" default="">
		<cfargument name="ULClass" default="">
		<cfargument name="ULID" default="">
		<cfargument name="useGenerationWithClass" type="boolean" default="false">

		<cfset var currentElementQry="">
		<cfset var selected="">
		<cfset var nextGeneration="">
		<cfset var currentElementParentIDList = '' />
		<cfset var previousgeneration = '' />
		<cfset var resultHTML = '' />
		<!--- START: Alex --->
		<cfset var qrytopGeneration="">
		<cfset var topGeneration="">


		<cfquery name="qrytopGeneration" dbtype="query">
			select min(generation) as topGeneration from elementTree
		</cfquery>

		<cfset topGeneration = qrytopGeneration.topGeneration>
		<!--- END: Alex --->
		<!--- get list of parents of current element so that we can select all the parents as well --->
		<!--- 2008/01/08 GCC
		1. If no results then use request.currentelement.parentIDlist
		2. pass currentelement=0 to show no  'selected' parent--->
		<cfquery name="currentElementQry" dbtype="query">
		select parentidlist from elementTree where node = #currentelement#
		</cfquery>

		<cfif currentelement neq 0 and currentElementQry.recordcount eq 0>
			<cfset currentElementParentIDList = request.currentElement.parentIDList>
		<cfelse>
			<cfset currentElementParentIDList = currentElementQry.parentidlist>
		</cfif>

		<cfsetting enablecfoutputonly = true>
		<cfsavecontent variable="resultHTML">
			<cfset previousgeneration = elementTree.generation>
			<cfoutput><ul class="#ULClass#" ID="#ULID#"></cfoutput>
				<cfloop query = "elementTree">
					<cfif currentRow is not elementTree.recordcount>
						<CFSET nextgeneration = elementTree.generation[currentrow+1]>
					<cfelse>
						<CFSET nextgeneration = 1>
					</cfif>
					<cfif generation GT previousgeneration>
						<cfoutput><ul class="<cfif useGenerationWithClass>#ULClass##generation#</cfif>"></cfoutput>
					</cfif>
					<cfif node is currentElement or listfind(currentElementParentIDList,node)><cfset selected = true><cfelse><cfset selected = false></cfif>
					<cfoutput><li class="<cfif useGenerationWithClass>#ULClass##generation#</cfif> <cfif currentRow is 1>first</cfif><cfif selected>selected</cfif>" ><A HREF="#resolvedHREF#" target="#resolvedTarget#" <!--- <cfif selected>class="selected"</cfif> ---> ></cfoutput>
					<!--- START: Alex --->
					<cfif topGeneration eq generation>
						<cfoutput><span>#application.com.security.sanitiseHTML(headline)#</span></cfoutput>
					<cfelse>
						<cfoutput>#application.com.security.sanitiseHTML(headline)#</cfoutput>
					</cfif>
					<!--- END: Alex ---><cfoutput></A></cfoutput>   <!--- this just while testing <cfif haschildren is 1>+</cfif>--->
					    <!--- note no space between "first" and "selected"   (IE 6 can't handle multiple classes) --->
					<cfif nextgeneration LTE generation ><cfoutput></li></cfoutput></cfif>
					<cfif nextgeneration LT generation><cfoutput>#repeatString("</ul></li>",generation - nextgeneration)#</cfoutput></cfif>
					<cfset previousgeneration =generation >
				</cfloop>
		 	<cfoutput></ul></cfoutput>
		</cfsavecontent>
		<cfsetting enablecfoutputonly = false>
		<cfreturn resultHTML>

	</cffunction>

	<cffunction name="outputTreeAsULStyled" access="public" output=true>
		<cfargument name="elementTree" type="query" required = true>
		<cfargument name="currentElement" default="">

		<cfset var currentElementQry="">
		<cfset var menuCounter = 1>
		<cfset var result = '' />
		<cfset var currentElementParentIDList = '' />
		<cfset var toggleItemsArray = '' />
		<cfset var marca = '' />
		<cfset var showArrow = '' />
		<cfset var x = '' />
		<cfset var qryTopLevelElements = '' />
		<cfset var qryGetChildren = '' />
		<cfset var qryGetGrandChildren = '' />


		<cfset result.items = 0>
		<cfset result.array = "">

		<cfquery name="currentElementQry" dbtype="query">
			select parentidlist from elementTree where node = #currentelement#
		</cfquery>

		<cfif currentelement neq 0 and currentElementQry.recordcount eq 0>
			<cfset currentElementParentIDList = request.currentElement.parentIDList>
		<cfelse>
			<cfset currentElementParentIDList = currentElementQry.parentidlist>
		</cfif>

		<cfsetting enablecfoutputonly = true>
		<cfsavecontent variable="result.menu_html">

				<cfset toggleItemsArray=ArrayNew(2)>

				<cfquery name="qryTopLevelElements" dbtype="query">
					select * from elementTree where generation = 2
				</cfquery>

				<cfoutput><ul></cfoutput>
					<cfloop index="x" from = "1" to = #qryTopLevelElements.recordcount#>
						<cfif qryTopLevelElements.node[x] is currentElement or listfind(currentElementParentIDList,qryTopLevelElements.node[x])>
							<cfset marca = "marca">
						<cfelse>
							<cfset marca = "">
						</cfif>
						<cfoutput>

							<li>

								<a href="#qryTopLevelElements.resolvedHREF[x]#" id="TopNav#x#" class="with-layer #marca#">#trim(qryTopLevelElements.headline[x])#</a>

								<cfquery name="qryGetChildren" dbtype="query">
									select * from elementTree where ACTUALPARENTID = #qryTopLevelElements.node[x]#
								</cfquery>

								<cfif qryGetChildren.recordcount GT 0>
									<cfset menuCounter = 1>
									<span style="clear:both;"></span>
									<span class="subMenu">
										<div id="layer-#x#">
											<div class="top-layer"></div>
											<div class="content-layer">
												<cfloop query="qryGetChildren">

													<!--- Does child have grandchildren? We're only going down to GrandChildren --->
													<cfquery name="qryGetGrandChildren" dbtype="query">
														select * from elementTree where ACTUALPARENTID = #qryGetChildren.node#
													</cfquery>
													<cfif qryGetGrandChildren.recordcount GT 0>
														<cfset showArrow = "true">
													<cfelse>
														<cfset showArrow = "false">
													</cfif>
													<h4><a href="#qryGetChildren.resolvedHREF#">#application.com.security.sanitiseHTML(qryGetChildren.headline)#</a><cfif showArrow><a href="##" id="title-#qryGetChildren.node#"><img src="/code/borders/images/back-downLiA.gif"></cfif></a></h4>

													<cfif qryGetGrandChildren.recordcount GT 0>
														<div id="module-#qryGetChildren.node#" style="padding-bottom: 5px;display:none;">
															<cfset toggleItemsArray[x][menuCounter] = qryGetChildren.node>
															<cfset menuCounter = menuCounter +1>
															<cfloop query="qryGetGrandChildren">
																<p><a HREF="#qryGetGrandChildren.resolvedHREF#" target="#qryGetGrandChildren.resolvedTarget#">- #application.com.security.sanitiseHTML(qryGetGrandChildren.headline)#</a></p>
															</cfloop>

														</div>
													</cfif>
												</cfloop>
											</div>
										</div>
									</span>
								</cfif>

							</li>

						</cfoutput>

					</cfloop>
					<cfoutput>
						<div style="float:right;">
							<li style="background-image:none;">
								<a class="contactUs" href="/?eid=928" style="padding-right:0px;">#application.com.relayTranslations.translateString("phr_contact_us")#</a>
							</li>
						</div>
					</cfoutput>
				<cfoutput></ul></cfoutput>
		</cfsavecontent>

		<cfset result.items = qryTopLevelElements.recordcount>
		<cfset result.array = toggleItemsArray>

		<cfsetting enablecfoutputonly = false>
		<cfreturn result>

	</cffunction>

	<!--- GCC 2007/10/04 stealing ideas --->
	<cffunction name="outputTreeAsSelectForm" access="public" output=false>
		<cfargument name="elementTree" type="query" required = true>
		<cfargument name="SelectFormName" default="linkListSelect">
		<cfargument name="SelectClass" default="">
		<cfargument name="NullText" default="">

		<cfset var nextgeneration = '' />
		<cfset var myIndex = '' />
		<cfset var resultHTML = '' />
			<!--- Gcc I have put the form in here to keep it as plug and play as possible... --->
			<!--- 2008/04/22 GCC Tweaked to use resolvedHref instead of eid to enable the use of redirects --->
			<cfsetting enablecfoutputonly = true>
			<cfsavecontent variable="resultHTML">
				<cfoutput>
				<script type="text/javascript">
					function setActionAndSubmit() {
						myForm = document.#jsStringFormat(arguments.SelectFormName)#form;
						myForm.action = myForm.formAction.value;
						myForm.submit();
					}
				</script>
				<Form id="#arguments.SelectFormName#form" method="post" action="/" name="#arguments.SelectFormName#form">
					<Select name="formAction" class="#arguments.SelectClass#" onChange="setActionAndSubmit()"></cfoutput>
				<cfif arguments.NullText neq "">
					<cfoutput><option value="/?eid=#request.currentelement.id#"><span class="#arguments.SelectClass#0">phr_#htmleditformat(arguments.NullText)#</span></cfoutput>
				</cfif>
				<cfloop query = "elementTree">
					<cfif currentRow is not elementTree.recordcount>
						<CFSET nextgeneration = elementTree.generation[currentrow+1]>
					<cfelse>
						<CFSET nextgeneration = 1>
					</cfif>
						<cfoutput><option value="#resolvedHref#" <cfif node eq request.currentelement.id>selected</cfif> class="#arguments.SelectClass##generation#"><cfloop from="1" to="#elementTree.generation[currentrow]#" index="myIndex"><cfif myIndex gt 1>&nbsp;</cfif></cfloop>#application.com.security.sanitiseHTML(headline)#</cfoutput>
				</cfloop>
				<cfoutput></Select></Form></cfoutput>
			</cfsavecontent>
			<cfsetting enablecfoutputonly = false>
		<cfreturn resultHTML>
	</cffunction>


<!---
	You will need something like this in your calling file!
						_menuCloseDelay=500;
						_menuOpenDelay=150;
						_subOffsetTop=2;
						_subOffsetLeft=-2;

						with(menuStyle=new mm_style()){
						fontfamily="arial, helvetica, sans-serif";
						fontsize="10px";
						fontstyle="normal";
						fontweight="bold";
						offbgcolor="#cdcec8";
						offcolor="#555555";
						onbgcolor="#eeeeee";
						oncolor="#555555";
						padding=4;
						subimage="images/none.gif";
						subimagepadding=2;
						itemwidth="200px";
						itemheight="35px";
						bgimage="images/off.gif";
						overbgimage="images/on.gif";
						onbgimage="images/on.gif";
						padding="0px 0px 7px 23px";
						}

						with(submenuStyle=new mm_style()){
						bordercolor="#ffffff";
						borderstyle="solid";
						borderwidth=1;
						fontfamily="arial, helvetica, sans-serif";
						fontsize="10px";
						fontstyle="normal";
						fontweight="bold";
						offbgcolor="#cdcec8";
						offcolor="#555555";
						onbgcolor="#eeeeee";
						oncolor="#555555";
						padding=4;
						separatorcolor="#ffffff";
						separatorsize=1;
						subimage="images/arrow.gif";
						subimagepadding=2;
						itemwidth="198px";
						}
--->

<!--- 	<cffunction name="outputTreeAsMilonic" access="public" >
		<cfargument name="elementTree" type="query" required = true>
		<cfargument name="currentElement" default="">
		<cfargument name="leftOffset" default="0">
		<cfargument name="topOffset" default="0">
		<cfargument name="menustyle" default="menustyle">
		<cfargument name="submenustyle" default="submenustyle">
		<cfargument name="screenposition" default="relative">

		<cfset var resultJS = "">
		<cfset var milonicDebug = '' />
		<cfset var milonicPath = '' />

		<cfquery name = "arguments.elementTree" dbtype="query">
		select * from elementTree order by parentid, sortorder
		</cfquery>

		<cfset milonicDebug=false>
		<cfset milonicPath = "/test/william/miloniclex">
		<cfset milonicPath = "/javascript/Milonic">

			<cf_includeJavascriptOnce template="#milonicPath#/milonic_src.js">
			<cf_includeJavascriptOnce template="#milonicPath#/mmenudom.js">
<!--- <cf_includeJavascriptOnce template="#milonicPath#/menu_data.js"> --->

<cfsavecontent variable="resultJS">
				<SCRIPT language=JavaScript >
					fixMozillaZIndex=true; //Fixes Z-Index problem  with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps



				<CFOUTPUT QUERY = "elementTree" GROUP = "PARENTID">

					with(new menuname("Menu#jsStringFormat(parentID)#")){
						screenposition="#jsStringFormat(screenposition)#";
					<CFIF GENERATION IS 2 or generation is 1> <!--- bit odd this! I think that I havemy generations confused a bit.  Generally for a milonic menu the top item is not used, so the first level is actually generation 2! --->
						style = #jsStringFormat(menustyle)#;
						alwaysvisible=1;
						left=#jsStringFormat(leftOffset)#;
						top=#jsStringFormat(topOffset)#;
						followscroll=0;
						orientation="horizontal";
					<cfelse>
						style=#jsStringFormat(submenustyle)#;
					</CFIF>

						<cfoutput>
							aI("text=#jsStringFormat(headline)#;url=#jsStringFormat(resolvedHREF)#;<cfif haschildren is 1>showmenu=Menu#jsStringFormat(ID)#;</cfif>pagematch=#jsStringFormat(resolvedHREF)#;");
						</cfoutput>
					}

				</cfoutput>

					var buildAllMenus = true  ;    // builds everything at load time

				  drawMenus()
				</SCRIPT>


		</cfsavecontent>

		<cfreturn resultJS>

	</cffunction> --->


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	      Get a page/element
	      2009-02-16 NYB Sophos Stargate 2
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getElement" hint="Returns a query object containing an Element">
		<cfargument name="elementID" type="string" required="true">

		<cfset var getElementQry = "">

		<CFQUERY NAME="getElementQry" DATASOURCE="#application.SiteDataSource#">
			select * from element where
			<cfif isNumeric(elementID)>
				id =  <cf_queryparam value="#elementID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				upper(elementTextID) =  <cf_queryparam value="#ucase(elementID)#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		</CFQUERY>
		<cfreturn getElementQry>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	      Creates a page/element
	      2009-02-16 NYB Sophos Stargate 2
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createPage" hint="Returns id of the page created" output="false">
		<cfargument name="Headline" type="string" required="true">
		<cfargument name="Summary" type="string" default="">
		<cfargument name="Detail" type="string" required="true">

		<cfargument name="resetCache" type="boolean" default="true">   <!--- if true, blows cache and resets Phrases Struct at the end of page creation --->
		<cfargument name="Language" type="string" default="1">	<!--- Accepts LangugageID or Language Name --->
		<cfargument name="Country" type="string" default="0">	<!--- Accepts CountryID or Country Name --->

		<cfargument name="RecordRights" type="string" required="false">
		<cfargument name="CountryScope" type="string" required="false">

		<cfargument name="UserID" type="numeric" default="0">
		<cfargument name="Created" type="date" default="#now()#">
		<cfargument name="LastUpdate" type="date" default="#Created#">
		<cfargument name="PageId" type="string" default="0">
		<cfargument name="LongSummary" type="string" required="false">

		<cfargument name="ElementTypeID" type="numeric" default="42">
		<cfargument name="ElementTypeGroupID" type="numeric" required="false">
		<cfargument name="ParentPage" type="string" default="0"> <!--- Accepts ElementID or ElementTextID --->
		<cfargument name="StatusID" type="numeric" default="4"><!--- defaults to live --->
		<cfargument name="SortOrder" type="numeric" default="1">
		<cfargument name="MasterOrder" type="numeric" required="false">
		<cfargument name="Date" type="numeric" required="false">
		<cfargument name="Image" type="numeric" required="false">
		<cfargument name="IsLive" type="numeric" default="1">
		<cfargument name="IsExternalFile" type="numeric" default="0">
		<cfargument name="ExpiresDate" type="date" required="false">
		<cfargument name="showonmenu" type="numeric" default="1">
		<cfargument name="SummaryInContents" type="numeric" default="0">
		<cfargument name="rate" type="numeric" default="0">
		<cfargument name="parameters" type="string" required="false">
		<cfargument name="LoginRequired" type="numeric" default="0">
		<cfargument name="elementTextID" type="string" required="false">
		<cfargument name="isTreeRoot" type="numeric" default="0">
		<cfargument name="hidden" type="numeric" default="0">
		<cfargument name="GetChildrenFromParentID" type="numeric" default="0">
		<cfargument name="GetContentFromID" type="numeric" default="0">
		<cfargument name="BorderSet" type="string" required="false">
		<cfargument name="stylesheets" type="string" required="false">
		<cfargument name="showSummary" type="numeric" default="0">
		<cfargument name="showLongSummary" type="numeric" default="0">
		<cfargument name="hideChildrenFromMenu" type="numeric" default="0">
		<cfargument name="showonSecondaryNav" type="numeric" default="1">

		<cfset var updateTime = now()>
		<cfset var languageid = "">
		<cfset var countryid = "">
		<cfset var getParent = "">
		<cfset var ParentID = "">
		<cfset var UserGroupID = "">
		<cfset var getRecord = "">
		<cfset var phraseTextIDList = "">
		<cfset var phraseTextID = "">
		<cfset var i = "">
		<cfset var x = "">
		<cfset var getLanguageID = "">
		<cfset var getCountryID = "">
		<cfset var getElementTableInfo = "">
		<cfset var insertNewRecord = "">


		<cfif isNumeric(Language)>
			<cfset LanguageID = Language>
		<cfelse>
			<CFQUERY NAME="getLanguageID" DATASOURCE="#application.SiteDataSource#">
				select LanguageID from [Language] where [Language] =  <cf_queryparam value="#Language#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</CFQUERY>
			<cfif getLanguageID.recordcount eq 0>
				<cfset LanguageID = 1>
			<cfelse>
				<cfset LanguageID = getLanguageID.LanguageID>
			</cfif>
		</cfif>

		<cfif isNumeric(Country)>
			<cfset CountryID = Country>
		<cfelse>
			<CFQUERY NAME="getCountryID" DATASOURCE="#application.SiteDataSource#">
				select CountryID from [Country] where CountryDescription =  <cf_queryparam value="#Country#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</CFQUERY>
			<cfif getCountryID.recordcount eq 0>
				<cfset CountryID = 1>
			<cfelse>
				<cfset CountryID = getCountryID.CountryID>
			</cfif>
		</cfif>

		<cfset getParent = application.com.relayElementTree.getElement(elementID=ParentPage)>

		<cfif getParent.recordcount eq 0>
			<cfset ParentID = 0>
		<cfelse>
			<cfset ParentID = getParent.ID>
		</cfif>

		<cfif UserID eq 0>
			<cftry>
				<cfset arguments.UserID=request.relayCurrentUser.usergroupid>
				<cfcatch type="any">
					<cfset arguments.UserID=0>
				</cfcatch>
			</cftry>
		</cfif>

		<cfset getRecord = application.com.relayElementTree.getElement(elementID=PageID)>

		<CFTRANSACTION>
			<cfif PageID eq 0 or getRecord.recordcount eq 0>
				<cfif not isDefined("request.colLength")>
					<CFQUERY NAME="getElementTableInfo" DATASOURCE="#application.SiteDataSource#">
						select c.Column_Name as ColName,c.CHARACTER_MAXIMUM_LENGTH as MaxLength from information_schema.tables t
						inner join information_schema.columns c on t.table_name=c.table_name
						where t.table_name='element'
						and t.table_type='BASE TABLE'
						and CHARACTER_MAXIMUM_LENGTH is not NULL
						order by c.ordinal_position
					</CFQUERY>
					<!--- NYB 2009-02-27 - added request.colLength and request.colLength check throughout insert to stop errors being thrown --->
					<cfset request.colLength = structNew()>
					<cfloop index ="i" from = "1" to = "#getElementTableInfo.recordcount#">
						<cfset x = application.com.structureFunctions.queryRowToStruct(query=getElementTableInfo,row=i,columns="MaxLength")>
						<cfset request.colLength[getElementTableInfo.ColName[i]] = x.MaxLength>
					</cfloop>
				</cfif>

				<CFQUERY NAME="InsertNewRecord" DATASOURCE="#application.SiteDataSource#">
					insert into element (
						ElementTypeID,
						<cfif structkeyexists(arguments,"ElementTypeGroupID")>ElementTypeGroupID,</cfif>
						ParentID,statusID,SortOrder,
						<cfif structkeyexists(arguments,"MasterOrder")>MasterOrder,</cfif>
						Headline,
						<cfif structkeyexists(arguments,"Date")>[Date],</cfif>
						<cfif structkeyexists(arguments,"Image")>Image,</cfif>
						isLive,isExternalFile,
						<cfif structkeyexists(arguments,"ExpiresDate")>ExpiresDate,</cfif>
						showonmenu,
						SummaryInContents,
						rate,
						hasCountryScope,
						<cfif structkeyexists(arguments,"parameters")>parameters,</cfif>
						hasRecordRights,
						LoginRequired,
						<cfif structkeyexists(arguments,"menuimage")>menuimage,</cfif>
						<cfif structkeyexists(arguments,"elementTextID")>elementTextID,</cfif>
						isTreeRoot,hidden,GetChildrenFromParentID,GetContentFromID,
						<cfif structkeyexists(arguments,"BorderSet")>BorderSet,</cfif>
						<cfif structkeyexists(arguments,"stylesheets")>stylesheets,</cfif>
						showSummary,
						showLongSummary,
						hideChildrenFromMenu,
						showonSecondaryNav,
						Created,CreatedBy,lastUpdated,LastUpdatedBy
						)
					values (
						<cf_queryParam value="#arguments.ElementTypeID#" cfsqltype="CF_SQL_INTEGER">,
						<cfif structkeyexists(arguments,"ElementTypeGroupID")><cf_queryParam value="#arguments.ElementTypeGroupID#" cfsqltype="CF_SQL_INTEGER">,</cfif>
						<cf_queryParam value="#ParentID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.statusID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.SortOrder#" cfsqltype="CF_SQL_INTEGER">,
						<cfif structkeyexists(arguments,"MasterOrder")><cf_queryParam value="#arguments.MasterOrder#" cfsqltype="CF_SQL_INTEGER">,</cfif>
						<cf_queryparam value="#left(arguments.Headline,request.colLength["Headline"])#" CFSQLTYPE="CF_SQL_VARCHAR">,
						<cfif structkeyexists(arguments,"Date")> <cf_queryparam value="#CreateODBCDateTime(arguments.Date)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,</cfif>
						<cfif structkeyexists(arguments,"Image")><cf_queryparam value="#arguments.Image#" CFSQLTYPE="CF_SQL_VARCHAR">,</cfif>
						<cf_queryParam value="#arguments.IsLive#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.IsExternalFile#" cfsqltype="CF_SQL_INTEGER">,
						<cfif structkeyexists(arguments,"ExpiresDate")><cf_queryparam value="#CreateODBCDateTime(arguments.ExpiresDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP">,</cfif>
						<cf_queryParam value="#arguments.showonmenu#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.SummaryInContents#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.rate#" cfsqltype="CF_SQL_INTEGER">,
						<cfif structkeyexists(arguments,"CountryScope")>1<cfelse>0</cfif>,
						<cfif structkeyexists(arguments,"parameters")><cf_queryparam value="#left(arguments.elementTextID,request.colLength["parameters"])#" CFSQLTYPE="CF_SQL_VARCHAR">,</cfif>
						<cfif structkeyexists(arguments,"RecordRights")>1<cfelse>0</cfif>,
						<cf_queryParam value="#LoginRequired#" cfsqltype="CF_SQL_INTEGER">,
						<cfif structkeyexists(arguments,"menuimage")><cf_queryparam value="#arguments.menuimage#" CFSQLTYPE="CF_SQL_VARCHAR">,</cfif>
						<cfif structkeyexists(arguments,"elementTextID")><cf_queryparam value="#left(arguments.elementTextID,request.colLength["elementTextID"])#" CFSQLTYPE="CF_SQL_VARCHAR">,</cfif>
						<cf_queryParam value="#arguments.isTreeRoot#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.hidden#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.GetChildrenFromParentID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.GetContentFromID#" cfsqltype="CF_SQL_INTEGER">,
						<cfif structkeyexists(arguments,"BorderSet")><cf_queryparam value="#left(arguments.BorderSet,request.colLength["BorderSet"])#" CFSQLTYPE="CF_SQL_VARCHAR">,</cfif>
						<cfif structkeyexists(arguments,"stylesheets")><cf_queryparam value="#left(arguments.stylesheets,request.colLength["stylesheets"])#" CFSQLTYPE="CF_SQL_VARCHAR"></cfif>
						<cf_queryParam value="#arguments.showSummary#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.showLongSummary#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.hideChildrenFromMenu#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.showonSecondaryNav#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryparam value="#CreateODBCDateTime(arguments.Created)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryParam value="#UserID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryparam value="#CreateODBCDateTime(arguments.LastUpdated)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryParam value="#UserID#" cfsqltype="CF_SQL_INTEGER">
						)
				</CFQUERY>

				<CFQUERY NAME="getRecord" DATASOURCE="#application.SiteDataSource#">
					select max(id) as id
					from element
					where parentID =  <cf_queryparam value="#ParentID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and created = #CreateODBCDateTime(arguments.Created)#
				</CFQUERY>
			</cfif>

			<cfset arguments.PageID = getRecord.id>
			<cfif structkeyexists(arguments,"Summary")>
				<cfset phraseTextIDList = "Headline,Summary,Detail">
			<cfelse>
				<cfset phraseTextIDList = "Headline,Detail">
			</cfif>


			<cfloop index="PhraseTextID" list="#phraseTextIDList#">
				<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID=PhraseTextID,userID=UserID,entitytypeid=10,entityid=PageID,LanguageID=LanguageID,countryID=countryID,phraseText=arguments[PhraseTextID],addMode="true",updateMode="overwrite",updateCluster=resetCache)>
			</cfloop>


			<cfif structkeyexists(arguments,"CountryScope")>
				<!--- WAB 2010/05/24 this looks very odd we were looping through countryScope but not using the index!
				I have therefore altered it so that the index i is passed in as the countryid to setCountryScope
				--->
				<cfloop index="i" list="#CountryScope#">
					<cfset application.com.rights.setCountryScope(entity="ELEMENT",entityID=PageID,entitytypeid=10,CountryID=I,Permission=1)>
				</cfloop>
			</cfif>

			<cfif structkeyexists(arguments,"RecordRights")>
				<cfloop index="userGroupID" list="#RecordRights#">
					<cfset application.com.rights.setRecordRights(entity="ELEMENT",UserGroupID=userGroupID,recordid=PageID,Level=1)>
				</cfloop>
			</cfif>
		 </cftransaction>

		<cfif resetCache>
			<cfset application.com.relayElementTree.incrementCachedContentVersionNumber()>
		</cfif>

		<cfreturn PageID>
	</cffunction>


	<!--- 2014/05/20	YMA	Bootstrap responsive portal --->
	<cffunction name="outputTreeAsBootstrapUL" access="public" output=false>
		<cfargument name="elementTree" type="query" required = true>
		<cfargument name="currentElement" default="">
		<cfargument name="desktopGenerations" default="3">
		<cfargument name="showLogoutInXSNav" default="true">
		<cfargument name="showSearchInXSNav" default="true">
		<cfargument name="searchResultsEID" default="searchresults">


		<cfset var firstGeneration = elementTree.generation[1]>
		<cfset var currentGeneration = elementTree.generation[1]>
		<cfset var resultHTML = "">
		<cfset var i = 0>

		<cfsetting enablecfoutputonly = true>
		<cfsavecontent variable="resultHTML">
		<cfoutput>
		<ul class="nav navbar-nav" id="mainNavigation">
			<cfloop query="#elementTree#">
				<cfif elementTree.generation lt currentGeneration>
					<cfset var generationDiff = currentGeneration - elementTree.generation>
					<cfloop from="1" to=#generationDiff# index="i">
						</ul>
					</li>
					</cfloop>
				</cfif>
				<cfif elementTree.haschildren gt 0 and elementtree.hidechildrenfrommenu eq 0 and (elementtree.generation[currentrow + 1] gt elementtree.generation)>
					<li class="dropdown<cfif generation gt firstGeneration>-submenu</cfif> <cfif id eq arguments.currentElement>active</cfif>">
						<a  <cfif generation eq firstGeneration>data-toggle="dropdown" class="dropdown-toggle js-activated"</cfif> href="#resolvedHREF#<!--- <cfif generation gt firstGeneration>#resolvedHREF#<cfelse>javascript:if(jQuery(window).width() >= 960){window.location = 'www.google.com';}else{return false;}</cfif> --->">#application.com.security.sanitiseHTML(headline)#<cfif generation eq firstGeneration><b class="moreMenu visible-xs hidden-sm hidden-md hidden-lg"></b><b class="caret hidden-sm  visible-sm visible-md visible-lg"></b></cfif>
							<cfif generation gt firstGeneration><i class="fa fa-caret-right" aria-hidden="true"></i></cfif>							
						</a>
						<ul <cfif generation eq firstGeneration>role="menu"</cfif> class="<!---cfif elementtree.generation gte arguments.desktopGenerations>visible-xs </cfif--->dropdown-menu">
							<cfif generation eq firstGeneration><li class="hidden-md hidden-lg"><a HREF="#resolvedHREF#">#application.com.security.sanitiseHTML(headline)# </a>
					</li>
				</cfif>
				<cfelse>
					<li class="<cfif id eq arguments.currentElement>active</cfif>">
						<a HREF="#resolvedHREF#" target="#resolvedTarget#">#application.com.security.sanitiseHTML(headline)#</a></li>
				</cfif>
				<cfset currentGeneration = elementTree.generation>
			</cfloop>
			<cfif firstGeneration lt currentGeneration>
				<cfset generationDiff = currentGeneration-firstGeneration>
				<cfloop from="1" to=#generationDiff# index="i">
					</ul>
				</li>
				</cfloop>
			</cfif>
			<cfif request.relayCurrentUser.isLoggedIn>
				<cfif showSearchInXSNav>
					<li class="visible-xs search">
					<div id="searchbar">
						<form id="searchform" method="post" action="#request.currentsite.protocolAndDomain#/et.cfm?eid=#searchResultsEID#" name="searchform">
							<input class="box" type="text" value="Search" name="frmCriteria" id="s" maxlength="255" onfocus="this.value=(this.value=='Search') ? '' : this.value;" onblur="this.value=(this.value=='') ? 'Search' : this.value;"/>
						</form>
					</div>
					</li>
				</cfif>
				<cfif showLogoutInXSNav><li class="bold logout visible-xs"><a href="/et.cfm?eid=logout" id="mobileLogout">phr_Logout</a></li></cfif>
			</cfif>
		</ul>
		</cfoutput>
		</cfsavecontent>
		<cfsetting enablecfoutputonly = false>
		<cfreturn resultHTML>

	</cffunction>


	<!--- Initialisation function
		This will be run automatically by our code when loaded into com scope
	--->
	<cffunction name="initialise">
		<cfargument name="applicationScope" default = "#application#">
		<cfif not structKeyExists(applicationScope,"elementType")>
			<cfset createElementTypeStructure	(applicationScope = applicationScope)>
		</cfif>
		<cfparam name="application.cachedContentVersionNumber" type="numeric" default="0">
		<cfset incrementCachedContentVersionNumber(updatecluster = false)>


	</cffunction>

	<!--- Add new functions above the initialisation functions--->
</cfcomponent>

