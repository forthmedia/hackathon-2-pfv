<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Mods


2005-06-28  WAB 	 Added smallint and bit data types to function writeBestMatchData
					added a few local variables (bit not all)
2007/07/18  WAB     added a bit of error handling to mergelocation needed for Trend
2007/10/29  WAB     added nvarchar N to function writeBestMatchData
2008/09/16  GCC    Added inner join on matchmethodid to all getfirstMatches queries to avoid dupes being filtered out by being in the idToDelete list of other matchmethodIDs
2009/04/30  WAB 	CR Lex 588  Added checkForMergeProtection function
2009/05/12 	NYB 	added Location dedupe code
2010/10/22	NJH 	P-FNL079 - added a list of entities to score in scoring functions. Added function for merge only method.
2013-09-19 	NYB		Case 436820 added option for dedupeMatchMethod 14 to getAllMatchMethods function
2013-11-26	WAB		Case 438202 - added a maxNumberToProcess parameter
2015-07-07	dxc 	case 445491/445434 - added ability to exclude computer columns to getColumnData and including in call
2015-08-21  DAN		Case 445491/445434 - fixes for auto-dedupe tool fails on submit
2016-01-12  DAN     Case 447173 - fix for merging locations caused by SQL error "failed to resume transaction"
2016-01-16	WAB		Security/CodeCop  Paramed some variables as type variablename to prevent codecop complaining.
					Then had to fix problem because didn't work when referring to query columns in plain structure notation, needed array notation
2016/09/01	NJH		JIRA PROD2016-1190 - replaced matching functions in dataloads with new calls to functions in matching.cfc. Primarily for updating matchnames
 --->

<cfcomponent displayname="dedupe" hint="Provide functions for matching and scoring duplicates">
	<!--- *********************************************************************************************************
		DEDUPING TOOLS
	********************************************************************************************************** --->

	<!--- will take dedupeMatchMethodID and run relevant deduping methods --->
	<cffunction access="public" name="dedupeRecordsFromView" hint="Runs the specified view and methods based on the specified type (definite or possible)">
		<cfargument name="dedupeMatchMethodID" type="string" required="true">
		<cfargument name="countryIDs" type="any">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var qRemoveOldPendingMerges = "">
		<cfset var viewDetail = '' />
		<cfset var passCountryIDs = '' />
		<cfset var methodChosen = '' />
		<cfset var possibleSuccess = '' />
		<cfset var x = '' />
		<cfset var countryID = '' />
		<cfset var allMatches = '' />
		<cfset var matchLogID = '' />

		<cfset viewDetail = getMatchMethodData(dedupeMatchMethodID)>

		<cfif isDefined("countryIDs") and countryIDs neq "">
			<cfset passCountryIDs = "true">
		<cfelse>
			<cfset passCountryIDs = "false">
		</cfif>

	<!--- 	<cfif viewDetail.dedupeViewType is "Definate"> --->
			<cfset methodChosen = getMethodDefiniteDupes(viewDetail)>

			<!--- 2007/09/24 GCC - removes old matches as a sledgehammer method to remove matches
			that are no longer valid because their matchkey has changed e.g. same org, email different names poss dupe but one of their email addresses
			have since been edited...still 'to do' but no longer valid.--->

			<cfquery name="qRemoveOldPendingMerges" datasource="#application.siteDataSource#">
			update dedupematches set mergestate = 'D'
			FROM         dedupeMatches dm INNER JOIN
                      dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID
			WHERE     (dml.dedupeMatchMethodID =  <cf_queryparam value="#arguments.dedupeMatchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (dm.mergeState = 't')
			<cfif passCountryIDs eq "true">
				and dm.countryID  in ( <cf_queryparam value="#countryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			</cfquery>

			<cfif passCountryIDs eq "true">
				<cfif viewdetail.dedupeentitytype eq "Organisation">
					<!--- GCC 2005/06/25 refresh the matchnames for those countries --->
					<cfscript>
						//NJH 2016/09/01 - JIRA PROD2016-1190 x = application.com.dbtools.updateOrgMatchname(countryIDlist = "#countryIDs#");
						application.com.matching.updateMatchFields(tablename="organisation",entityType="organisation",whereClause="countryID in (#countryIDs#)");
					</cfscript>
				<cfelseif viewdetail.dedupeentitytype eq "Location">
					<!--- NYB 2009/05/12 refresh the matchnames for those countries --->
					<cfscript>
						//NJH 2016/09/01 - JIRA PROD2016-1190 x = application.com.dbtools.updateLocationMatchname(countryIDlist = "#countryIDs#");
						application.com.matching.updateMatchFields(tablename="location",entityType="location",whereClause="countryID in (#countryIDs#)");
					</cfscript>
				<cfelseif viewdetail.dedupeentitytype eq "Person">
					<!--- NYB 2009/05/12 refresh the matchnames for those countries --->
					<cfscript>
						//NJH 2016/09/01 - JIRA PROD2016-1190 x = application.com.dbtools.updatePersonMatchname(countryIDlist = "#countryIDs#");
						application.com.matching.updateMatchFields(tablename="person",entityType="person");
					</cfscript>
				</cfif>
				<cfloop list="#countryIDs#" delimiters="," index="countryID">
					<cfinvoke method="#methodChosen#" viewDetail="#viewDetail#" countryID="#countryID#" returnvariable="allMatches">
					<cfinvoke method="writeDedupeMatches" viewDetail="#viewDetail#" dedupeEntities="#allMatches#" returnvariable="matchLogID">
				</cfloop>
			<cfelse>
				<cfif viewdetail.dedupeentitytype eq "Organisation">
					<!--- GCC 2008/02/04 refresh the matchnames for the whole DB - could be very sloooow --->
					<cfscript>
						//NJH 2016/09/01 - JIRA PROD2016-1190 x = application.com.dbtools.updateOrgMatchname();
						application.com.matching.updateMatchFields(tablename="organisation",entityType="organisation");
					</cfscript>
				</cfif>
				<cfinvoke method="#methodChosen#" viewDetail="#viewDetail#" returnvariable="allMatches">
				<cfinvoke method="writeDedupeMatches" viewDetail="#viewDetail#" dedupeEntities="#allMatches#" returnvariable="matchLogID">
			</cfif>

			<cfreturn "success">
<!--- 		<cfelse>
			<cfset possibleSuccess = managePossibleDupes()>

		</cfif> --->

	</cffunction>

	<!--- retrieve data from dedupeMatchMethod table based on passed ID --->
	<cffunction access="public" name="getMatchMethodData" hint="Retrieves the view data from dedupeMatchMethod table based on the dedupeMatchMethodID">
		<cfargument name="dedupeMatchMethodID" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var qMatchMethodData = '' />

		<cfquery name="qMatchMethodData" datasource="#datasource#">
			select *
			from dedupeMatchMethod
			where dedupeMatchMethodID =  <cf_queryparam value="#dedupeMatchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn qMatchMethodData>

	</cffunction>

	<!--- check the matchMethod data passed in and decide on which method to run --->
	<cffunction access="public" name="getMethodDefiniteDupes" hint="Returns string reference to method to run.">
		<cfargument name="viewDetail" type="query" required="true">

		<cfswitch expression="#viewDetail.dedupeEntityType#">
			<cfcase value="Person">
				<cfreturn "scorePerson">
			</cfcase>
			<cfcase value="Organisation">
				<cfreturn "scoreOrganisation">
			</cfcase>
			<cfcase value="Location">
				<cfreturn "scoreLocation">
			</cfcase>
		</cfswitch>

	</cffunction>

	<!---
		NJH 2008/02/08 Don't believe this function is called anymore but is superceded by the function above!!!

	<!--- check the matchMethod data passed in and decide on which method to run --->
	<cffunction access="public" name="manageDefiniteDupes" hint="Checks for relevant method based on entityType">
		<cfargument name="viewDetail" type="query" required="true">

		<cfset var allMatches = '' />

		<cfswitch expression="#viewDetail.dedupeEntityType#">
			<cfcase value="Person">
				<cfinvoke method="scorePerson" viewDetail="#viewDetail#" returnvariable="allMatches">
			</cfcase>
			<cfcase value="Organisation">
				<cfinvoke method="scoreOrganisation" viewDetail="#viewDetail#" returnvariable="allMatches">
			</cfcase>
			<cfcase value="Location">
				<cfinvoke method="scoreLocation" viewDetail="#viewDetail#" returnvariable="allMatches">
			</cfcase>
		</cfswitch>
		<cfreturn allMatches>
	</cffunction> --->

	<!--- retrieve all dudplicates for location table based on passed in view and score them. --->
	<cffunction access="public" name="scoreLocation" hint="Runs scoring method against Location table.">
		<cfargument name="viewDetail" type="query" required="false"> <!--- NJH 2010/10/23 - P-FNL079 was required.. can't pass in for merge only methods, but should be passed in for all other method types --->
		<cfargument name="countryID" type="any" required="false">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="locationIDList" type="string" required="false"> <!--- NJH 2010/10/22 P-FNL079 - score a list of locations --->

		<cfset var v="">
		<cfset var t="l">
		<cfset var getScoreForLocation = "">

		<cfif structKeyExists(arguments,"viewDetail")>
			<cfset v = viewDetail.viewAlias>
			<cfset t = viewDetail.tableAlias>
			<!--- This type checking prevents codeCop rejecting the query --->
			<cfparam name="v" type="variablename">
			<cfparam name="t" type="variablename">
			<cfparam name="viewDetail.dedupeViewName[1]" type="variablename">
		</cfif>

		<cfquery name="getScoreForLocation" datasource="#datasource#">
			select <cfif structKeyExists(arguments,"viewDetail")>#v#.*,</cfif> #t#.locationID as EntityID, #t#.lastUpdated, 'score' =
				case when #t#.address1 is null or #t#.address1 like '' then 0 else 10 end +
				case when #t#.address4 is null or #t#.address4 like '' then 0 else 10 end +
				case when #t#.PostalCode like '' then 0 else 10 end +
				case when #t#.Active = 0 or #t#.Active like '' then -50 else 5 end +
				case when #t#.telephone is null or #t#.telephone like '' then 0 else 5 end +
				case when #t#.fax is null or #t#.fax like '' then 0 else 5 end +
				case when #t#.sitename is null or #t#.sitename like '' then 0 else 5 end
			from location #t# with(nolock)
			<cfif structKeyExists(arguments,"viewDetail")>
				inner join #viewDetail.dedupeViewName# #v# with(nolock)
			on #preserveSingleQuotes(viewDetail.joinOnClause)#
			<cfif isDefined('arguments.countryID') and arguments.countryID neq "">
			where #v#.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfelse>
				where 1=1
			</cfif>
			<!--- NJH 2010/10/22 P-FNL079 - score a list of locations --->
			<cfif structKeyExists(arguments,"locationIDList") and arguments.locationIDList neq "">
				and #t#.locationID  in ( <cf_queryparam value="#arguments.locationIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			order by <cfif structKeyExists(arguments,"viewDetail")>#v#.matchKey,</cfif> score desc, #t#.lastUpdated desc
		</cfquery>

		<cfreturn getScoreForLocation>

	</cffunction>

	<!--- retrieve all dudplicates for Person table based on passed in view and score them. --->
	<cffunction access="public" name="scorePerson" hint="Runs scoring method against Person table.">
		<cfargument name="viewDetail" type="query" required="false"> <!--- NJH 2010/10/23 - P-FNL079 was required.. can't pass in for merge only methods, but should be passed in for all other method types --->
		<cfargument name="countryID" type="any" required="false">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="personIDList" type="string" required="false"> <!--- NJH 2010/10/22 P-FNL079 - score a list of people --->

		<cfset var v="">
		<cfset var t="p">
		<cfset var success="">
		<cfset var getScoreForPerson = "">

		<cfif structKeyExists(arguments,"viewDetail")>
			<cfset v = viewDetail.viewAlias>
			<cfset t = viewDetail.tableAlias>
			<!--- This type checking prevents codeCop rejecting the query --->
			<cfparam name="v" type="variablename">
			<cfparam name="t" type="variablename">
			<cfparam name="viewDetail.dedupeViewName[1]" type="variablename">
		</cfif>


		<!--- 2006/08/22 GCC Changed to exclude internal people from the match results --->
		<cfquery name="getScoreForPerson" datasource="#datasource#">
			select <cfif structKeyExists(arguments,"viewDetail")>#v#.*,</cfif> #t#.personID as EntityID, #t#.lastUpdated, 'score' = case when #t#.firstname is null or #t#.firstname like '' then 0 else 10 end +
				case when #t#.lastname is null or #t#.lastname like '' then 0 else 10 end +
				case when #t#.homephone is null or #t#.homephone like '' then 0 else 5 end +
				case when #t#.officephone is null or #t#.officephone like '' then 0 else 5 end +
				case when #t#.mobilephone is null  or #t#.mobilephone like '' then 0 else 5 end +
				case when #t#.faxphone is null or #t#.faxphone like '' then 0 else 5 end +
				case when #t#.email is null or #t#.email like '' then 0 else 10 end +
				case when #t#.username is null or #t#.username like '' then 0 else 15 end +
				case when #t#.password is null or #t#.password like '' then 0 else 15 end +
				case when #t#.firstTimeUser = 1 then 0 else 10 end +
				case when #t#.Active = 0 or #t#.Active like '' then -50 else 5 end +
				case when #t#.jobdesc is null or #t#.jobdesc like '' then 0 else 5 end<!---  +
				case when (select top 1 1 from usergroup where personID = #t#.personID) = 1 then 100 else 0 end --->
			from Person #t# with(nolock)
			<cfif structKeyExists(arguments,"viewDetail")>
			inner join #viewDetail.dedupeViewName# #v# with(nolock)
			on #preserveSingleQuotes(viewDetail.joinOnClause)#
			</cfif>
			where 1=1
			<cfif isDefined('arguments.countryID') and arguments.countryID neq "" and structKeyExists(arguments,"viewDetail")>
			and #v#.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<!--- NJH 2010/10/22 P-FNL079 - score a list of people --->
			<cfif structKeyExists(arguments,"personIDList") and arguments.personIDList neq "">
				and #t#.personID  in ( <cf_queryparam value="#arguments.personIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			and personID not in (SELECT DISTINCT PersonID
  				FROM UserGroup with(nolock)
 				WHERE PersonID = #t#.personID)
			order by <cfif structKeyExists(arguments,"viewDetail")>#v#.matchKey,</cfif> score desc, #t#.created
		</cfquery>

		<!--- <cfinvoke method="writeDedupeMatches" viewDetail="#viewDetail#" dedupeEntities="#getScoreForPerson#" returnvariable="success"> --->

		<cfreturn getScoreForPerson>

	</cffunction>

	<!--- retrieve all dudplicates for Person table based on passed in view and score them. --->
	<cffunction access="public" name="scoreOrganisation" hint="Runs scoring method against Organisation table.">
		<cfargument name="viewDetail" type="query" required="false"> <!--- NJH 2010/10/23 - P-FNL079 was required.. can't pass in for merge only methods, but should be passed in for all other method types --->
		<cfargument name="countryID" type="any" required="false">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="organisationIDList" type="string" required="false"> <!--- NJH 2010/10/22 P-FNL079 - score a list of organisations --->

		<cfset var v="">
		<cfset var t="o">
		<cfset var success="">
		<cfset var getScoreForOrganisation = "">

		<cfif structKeyExists(arguments,"viewDetail")>
			<cfset v = viewDetail.viewAlias>
			<cfset t = viewDetail.tableAlias>
			<!--- This type checking prevents codeCop rejecting the query --->
			<cfparam name="v" type="variablename">
			<cfparam name="t" type="variablename">
			<cfparam name="viewDetail.dedupeViewName[1]" type="variablename">
		</cfif>

		<cfquery name="getScoreForOrganisation" datasource="#datasource#">
			select <cfif structKeyExists(arguments,"viewDetail")>#v#.*,</cfif> #t#.organisationID as EntityID, #t#.lastUpdated, 'score' =
				 case when #t#.organisationName is null or #t#.organisationName like '' then 0 else 5 end +
				 case when #t#.Notes is null or #t#.Notes like '' then 0 else 5 end +
				 case when #t#.AKA is null or #t#.AKA like '' then 0 else 5 end +
				 case when #t#.DefaultDomainName is null or #t#.DefaultDomainName like '' then 0 else 5 end +
				 case when #t#.OrgURL is null or #t#.OrgURL like '' then 0 else 5 end +
				 case when #t#.Active = 0 or #t#.Active like '' then -50 else 5 end +
				 case when #t#.AccountMngrID is null or #t#.AccountMngrID like '' then 0 else 5 end +
				 case when #t#.AccountNumber is null or #t#.AccountNumber like '' then 0 else 5 end
			from organisation #t# with(nolock)
			<cfif structKeyExists(arguments,"viewDetail")>
			<cfif viewDetail.dedupeMatchMethodID eq 11>
				left outer join person with(nolock) on #t#.organisationID = person.OrganisationID
			</cfif>
			inner join #viewDetail.dedupeViewName# #v# with(nolock)
			on #preserveSingleQuotes(viewDetail.joinOnClause)#

			<cfif isDefined('arguments.countryID') and arguments.countryID neq "">
			where #v#.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfelse>
				where 1=1
			</cfif>
			<!--- NJH 2010/10/22 P-FNL079 - score a list of organisations --->
			<cfif structKeyExists(arguments,"organisationIDList") and arguments.organisationIDList neq "">
				and #t#.organisationID  in ( <cf_queryparam value="#arguments.organisationIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			order by <cfif structKeyExists(arguments,"viewDetail")>#v#.matchKey,</cfif> score desc, #t#.created
		</cfquery>

		<!--- <cfinvoke method="writeDedupeMatches" viewDetail="#viewDetail#" dedupeEntities="#getScoreForPerson#" returnvariable="success"> --->

		<cfreturn getScoreForOrganisation>

	</cffunction>

	<!--- loop through duplicates and write them to the dedupeMatches table. --->
	<cffunction access="public" name="writeDedupeMatches" hint="Writes duplicates to dedupeMatches table.">
		<cfargument name="viewDetail" type="query" required="true">
		<cfargument name="dedupeEntities" type="query" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<!--- WAB 2005-06-28 added some local variables to fix an apparently random bug --->
		<cfset var x = "">
		<cfset var i = "">
		<cfset var userKey = "">
		<cfset var itemsArray = "">
		<cfset var itemDupes = "">
		<cfset var incr = "">
		<cfset var indexToDelete = "">
		<cfset var highestElem 	= "">
		<cfset var checkUnique 	= "">
		<cfset var getDedupeMatchMethodID = "">
		<cfset var keepTest = "">
		<cfset var deleteTest = "">
		<cfset var IDToKeep = "">
		<cfset var IDToDelete = "">
		<cfset var validateStruct = "">
		<cfset var useKey = '' />
		<cfset var dupeArray = '' />
		<cfset var itemStruct = '' />
		<cfset var highIndex = '' />
		<cfset var dolog = '' />
		<cfset var col = '' />
		<cfset var distinctItems = '' />
		<cfset var updateDedupeMatches = '' />
		<cfset var dedupeMatchLogID = '' />


		<!--- log match and method --->
		<cfinvoke method="logDedupeMatch" viewDetail="#viewDetail#" returnvariable="dedupeMatchLogID">
		<!--- get the ID to filter out pending dupes generated using the same matchMethod --->
		<cfquery name="getDedupeMatchMethodID" datasource="#datasource#">
			select dedupeMatchMethodID  from dedupeMatchLog where dedupeMatchLogID =  <cf_queryparam value="#dedupeMatchLogID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<!--- get distinct set of entityID's from passed in duplicates query --->
		<cfquery dbtype="query" name="distinctItems">
			select distinct lower(matchKey)
			from dedupeEntities
		</cfquery>

		<!--- create an array of entities using a list of entityIDs --->
		<cfset itemsArray = listToArray(valueList(distinctItems.matchKey,"|"),"|")>

		<!--- ENABLE OUTPUTS ONLY FOR DEBUGGING PURPOSES --->
		<!--- <cfoutput>
		<table border="1">
			<tr>
				<td>ID</td>
				<td>SCORE</td>
				<td>LAST UPDATED</td>
			</tr>
		</cfoutput> --->
		<!--- loop through the array of duplicates entityIDs, retieve information from duplicate query for set of matches and store them in the array. --->
		<cfloop from="1" to="#arrayLen(itemsArray)#" index="i">
			<cfset useKey = itemsArray[i]>
			<cfquery dbtype="query" name="itemDupes">
				select *
				from dedupeEntities
				where lower(matchKey) = '#lcase(useKey)#'
				order by score DESC
			</cfquery>


			<cfif itemDupes.recordcount gt 1>
				<cfset incr = 1>
				<cfset dupeArray = arrayNew(1)>
				<cfloop query="itemDupes">
					<cfset itemStruct = structNew()>
					<cfloop list="#columnList#" index="col">
						<cfset itemStruct[col] = evaluate(col)>
					</cfloop>
					<cfset dupeArray[incr] = itemStruct>
					<cfset incr = incr + 1>
				</cfloop>

				<!--- Setup the highest scorer as element 1 in the array as this is how the return query was ordered --->
				<cfset highIndex = 1>
				<cfset highestElem = dupeArray[highIndex]>
				<!--- loop through the set of duplicates and check each for equal score against the highestElem.
					  If the score matches, check against lastUpdated field for the latest record.
					  Check that the match does not already exist in the dedupeMatches table. if not, write the match to the dedupeMatches table. --->
				<cfloop from="2" to="#arrayLen(dupeArray)#" index="x">
					<cfset dolog = "true">
					<cfset indexToDelete = x>
					<cfif highestElem.score eq dupeArray[x].score>
						<cfif highestElem.lastUpdated lt dupeArray[x].lastUpdated>
							<cfset indexToDelete = highIndex>
							<cfset highIndex = x>
							<cfset highestElem = dupeArray[highIndex]>
						</cfif>
					</cfif>

					<!--- 2009/05/05 P-LEX031 mopup -  This is the place to check against merge flags before continue - keepTest checked here becuase it can change within the loop --->
					<cfset IDToKeep = highestElem.EntityID>
					<cfset IDToDelete = dupeArray[indexToDelete].EntityID>
					<!--- NJH 2010/10/22 P-FNL079 - converted this into validateMergeCandidates function
					<cfset KeepTest = checkForMergeProtection(entityType=application.entitytypeid[viewDetail.dedupeEntityType],entityID=IDToKeep)>
					<cfset deleteTest = checkForMergeProtection(entityType=application.entitytypeid[viewDetail.dedupeEntityType],entityID=IDToDelete)>

					<cfif keepTest.isMergeProtected and deleteTest.isMergeProtected>
						<!--- set as not a dupe--->
						<cfset doLog = "false">
						<cfset x = application.com.dedupe.setAsNotADuplicate(idToKeep=IDToKeep,idToDelete=IDToDelete,tablename=viewDetail.dedupeEntityType)>
					<cfelseif not keepTest.isMergeProtected and deleteTest.isMergeProtected>
						<!--- swop IDs--->
						<cfset IDToKeep = dupeArray[indexToDelete].EntityID>
						<cfset IDToDelete = highestElem.EntityID>
					</cfif> --->

					<cfset validateStruct = validateMergeCandidates(IDToKeep=IDToKeep,IDToDelete=IDToDelete,entityType=application.entitytypeid[viewDetail.dedupeEntityType])>
					<cfset doLog = validateStruct.isDupe>
					<!--- <cfoutput>
					<tr style="background-color:##809FCE">
						<td style="color:white; font-weight:bold">#highestElem.EntityID#</td>
						<td style="color:white; font-weight:bold">#highestElem.score#</td>
						<td style="color:white; font-weight:bold">#highestElem.lastUpdated#</td>
					</tr>
					<tr style="background-color:##CC6600">
						<td style="color:white; font-weight:bold">#dupeArray[indexToDelete].EntityID#</td>
						<td style="color:white; font-weight:bold">#dupeArray[indexToDelete].score#</td>
						<td style="color:white; font-weight:bold">#dupeArray[indexToDelete].lastUpdated#</td>
					</tr>
					</cfoutput> --->

					<!--- log the dupe even if it has already been found in another match but hasn't yet been
					duped. RemoveNoLongerValidDupes will resolve the discrepancy when the other match is run
                                        2007/12/10 GCC added 'N' so that 'not a dupe' are never found again
										2008/05/16 GCC (Bug Fix Trend Issue 167) changed from 'in list (T,N)' to not in 'M' - don't log a dupe if it exists at any status other then merged
										2010/01/25 GCC LID3035 added 'D' because no longer valids may yet become valid again and require merging--->
					<cfif doLog>
						<cfquery name="checkUnique" datasource="#datasource#">
							select * from dedupeMatches inner join dedupeMatchLog on dedupeMatches.dedupeMatchLogID = dedupeMatchLog.dedupeMatchLogID
							where 	IDToKeep =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
									IDToDelete =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
									TableName =  <cf_queryparam value="#viewDetail.dedupeEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
									mergestate not in ('M','D') and dedupeMatchMethodID =  <cf_queryparam value="#getDedupeMatchMethodID.dedupeMatchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfquery>
						<cfif checkUnique.recordCount eq 0>
							<cfquery name="updateDedupeMatches" datasource="#datasource#">
								insert into dedupeMatches (tableName,IDToKeep,IDToDelete,countryID,duplicateType,mergeState,dedupeMatchLogID)
								values
								(<cf_queryparam value="#viewDetail.dedupeEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >,<cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >,<cf_queryparam value="#highestElem.CountryID#" CFSQLTYPE="CF_SQL_INTEGER" >,'D','T',<cf_queryparam value="#dedupeMatchLogID#" CFSQLTYPE="CF_SQL_INTEGER" >)
							</cfquery>
						</cfif>
					</cfif>
				</cfloop>
			<cfelse>
				<!--- <cfoutput>
					<tr style="background-color:##FF0000">
						<td style="color:white; font-weight:bold" colspan="3">NOT A DUPLICATE: #useKey#</td>
					</tr>
				</cfoutput> --->
			</cfif>
		</cfloop>

		<!--- <cfoutput><table></cfoutput> --->

		<cfreturn dedupeMatchLogID>
	</cffunction>


	<!--- NJH 2010/10/22 P-FNL079 --->
	<cffunction name="validateMergeCandidates" access="public" returntype="struct">
		<cfargument name="IDToKeep" type="numeric" required="true">
		<cfargument name="IDToDelete" type="numeric" required="true">
		<cfargument name="entityType" type="string" required="true">

		<cfset var result=structNew()>
		<cfset var keepID = arguments.IDToKeep>
		<cfset var deleteID = arguments.IDToDelete>
		<cfset var keepTest = checkForMergeProtection(entityType=arguments.entityType,entityID=keepID)>
		<cfset var deleteTest = checkForMergeProtection(entityType=arguments.entityType,entityID=deleteID)>

		<cfset result.isDupe=true>

		<cfif keepTest.isMergeProtected and deleteTest.isMergeProtected>
			<!--- set as not a dupe--->
			<cfset result.isDupe = "false">
			<cfset setAsNotADuplicate(idToKeep=keepID,idToDelete=deleteID,tablename=arguments.entityType)>
		<cfelseif not keepTest.isMergeProtected and deleteTest.isMergeProtected>
			<!--- swop IDs--->
			<cfset keepID = arguments.IDToDelete>
			<cfset deleteID = arguments.IDToKeep>
		</cfif>

		<cfset result.idToKeep = keepID>
		<cfset result.IDToDelete = deleteID>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2010/10/22 P-FNL079 --->
	<cffunction name="addMergeOnlyDupe" access="public" returntype="struct">
		<cfargument name="IDToKeep" type="numeric" required="true">
		<cfargument name="IDToDelete" type="numeric" required="true">
		<cfargument name="entityType" type="variablename" required="true">

		<cfset var insertMatchLog = "">
		<cfset var getCountryIdOfEntityToKeep = "">
		<cfset var getEntityMergeAlreadyExistsCount = "">
		<cfset var insertEntityIntoMergeQueue = "">
		<cfset var validateMergeStruct = validateMergeCandidates(argumentCollection=arguments)>
		<cfset var result = structNew()>

		<cfset var keepID = validateMergestruct.idToKeep>
		<cfset var deleteID = validateMergestruct.IDToDelete>
		<cfset var getMergeOnlyMatchMethodID = "">

		<cfset result.isOK = true>

		<cfif validateMergeStruct.isDupe>
			<cfquery name="getMergeOnlyMatchMethodID" datasource="#application.siteDataSource#">
				select dedupeMatchMethodID from dedupeMatchMethod
				where dedupeEntityType =  <cf_queryparam value="#arguments.entityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and dedupeTypeID = (select dedupeTypeID from dedupeType where name='mergeOnly')
			</cfquery>

			<cftransaction action = "begin">
				<cftry>
					<cfquery name="insertMatchLog" datasource="#application.siteDataSource#">
						insert into dedupeMatchLog (dedupeMatchMethodID) values	(<cf_queryparam value="#getMergeOnlyMatchMethodID.dedupeMatchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >)
						select scope_identity() as matchLogID
					</cfquery>

					<!--- Get the CountryId of the 'Entity To Keep' --->
					<cfquery name="getCountryIdOfEntityToKeep" datasource="#application.siteDataSource#">
						select countryId from #arguments.entityType#
							<cfif arguments.entityType eq "person">inner join location on location.locationID = person.locationID</cfif>
						where #arguments.entityType#Id =  <cf_queryparam value="#keepID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

					<!--- Make sure both values do not already appear in the queue for processing --->
					<cfquery name="getEntityMergeAlreadyExistsCount" datasource="#application.siteDataSource#">
						select count(IDToKeep) as existsCount
						from dedupeMatches
						where tableName =  <cf_queryparam value="#arguments.entityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and IDToKeep =  <cf_queryparam value="#keepID#" CFSQLTYPE="CF_SQL_INTEGER" >
							and IDToDelete =  <cf_queryparam value="#deleteID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

					<!--- Only do insert into merge queue if above check shows not already in merge queue --->
					<cfif getEntityMergeAlreadyExistsCount.ExistsCount eq 0>

						<!--- Insert data into Merge Queue for later processing --->
						<cfquery name="insertEntityIntoMergeQueue" datasource="#application.siteDataSource#">
							INSERT INTO dedupeMatches
							(	tableName,
								IDToKeep,
								IDToDelete,
								countryID,
								duplicateType,
								mergeState,
								dedupeMatchLogID,
								created
							)
							VALUES
							(	<cf_queryparam value="#arguments.entityType#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								<cf_queryparam value="#keepID#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#deleteID#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#getCountryIdOfEntityToKeep.CountryId#" CFSQLTYPE="CF_SQL_INTEGER" >,
								'D',
								'T',
								<cf_queryparam value="#insertMatchLog.matchLogID#" CFSQLTYPE="CF_SQL_INTEGER" >,
								GetDate()
							)
						</cfquery>
					</cfif>
					<cftransaction action="commit"/>

					<cfcatch type="Database">
						<cftransaction action="rollback"/>
						<cfset result.isOK = false>
					</cfcatch>
				</cftry>

			</cftransaction>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- Write a log of the match to the dedupeMatchLog table and return the new ID --->
	<cffunction access="public" name="logDedupeMatch" hint="Write a log of the match.">
		<cfargument name="viewDetail" type="query" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var writeDedupeMatchLog = '' />

		<cfquery name="writeDedupeMatchLog" datasource="#datasource#">
			insert into dedupeMatchLog (dedupeMatchMethodID)
			values
			(<cf_queryparam value="#viewDetail.dedupeMatchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >)
			select scope_identity() as newID
		</cfquery>

		<cfreturn writeDedupeMatchLog.newID>
	</cffunction>

	<!--- get duplicates from dedupeMatches table based on the dedupeMatchLogID and the mergeState. --->
	<cffunction access="public" name="getDedupeMatchesData" hint="Retrieves data from DedupeMatches based on matchID">
		<cfargument name="matchID" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var qMatchData = '' />

		<cfquery name="qMatchData" datasource="#datasource#">
			select *
			from dedupeMatches
			where dedupeMatchLogID =  <cf_queryparam value="#matchID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				  mergeState = 'T'
		</cfquery>

		<cfreturn qMatchData>

	</cffunction>

	<!--- Retrieves all match methods from dedupeMatchMethod table. --->
	<cffunction access="public" name="getAllMatchMethods" hint="Retrieves all match methods from dedupeMatchMethod table.">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<!--- <cfargument name="dedupeViewType" type="string" default=""> NJH 2008/02/06 TND510 Blocked/unBlocked: moved dedupeViewType to dedupeTypeID--->
		<cfargument name="dedupeTypeID" type="numeric" default=0>

		<cfset var qMatchMethods = '' />

		<cfquery name="qMatchMethods" datasource="#datasource#">
			select *
			from dedupeMatchMethod
			where	active <> 0
			<cfif arguments.dedupeTypeID neq 0>
				and dedupeTypeID = #arguments.dedupeTypeID#
			</cfif>
			<!--- <cfif arguments.dedupeViewType neq "">
					and dedupeViewType = '#arguments.dedupeViewType#'
			</cfif> NJH 2008/02/06 TND510 Blocked/unBlocked: moved dedupeViewType to dedupeTypeID --->
            order by dedupeMatchMethodDescription
		</cfquery>

		<cfreturn qMatchMethods>

	</cffunction>

	<!--- Get matches for coutries associated to user, seperated by duplicateType and tableName. --->
	<cffunction access="public" name="getMatchesForSelect" hint="Retrieves the match data for populating the select.">
		<cfargument name="countryIDs" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var selectMatches = '' />

		<CFQUERY NAME="selectMatches" DATASOURCE="#datasource#">
			SELECT dm.countryID, dm.duplicateType, dm.tableName, c.CountryDescription, count(dm.countryID) as numrecs, dmm.dedupeMatchMethodID, dmm.dedupeViewName,dmm.dedupeMatchMethodDescription,dt.name as dedupeViewType, dmm.dedupeTypeID
			FROM dedupeMatches dm
			INNER JOIN country c ON c.countryID = dm.countryID
			INNER JOIN dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID
			INNER JOIN dedupeMatchMethod dmm ON dml.dedupeMatchMethodID = dmm.dedupeMatchMethodID
			INNER JOIN dedupeType dt on dmm.dedupeTypeID = dt.dedupeTypeID
			WHERE c.CountryID IN (37,<cf_queryparam value="#countryIDs#" cfsqltype="cf_sql_integer" list=True>) AND
				  dm.mergeState = 'T' and
				  /* dmm.dedupeViewType <> 'Definate' */ <!--- NJH 2006/08/25 weed out definite dupes for the selection --->
				  dmm.dedupeTypeID = 1  <!--- NJH 2008/02/05 moved dedupeViewType to dedupeTypeID --->
			GROUP BY dm.tableName, dm.countryID, dm.duplicateType, c.countryDescription, dmm.dedupeMatchMethodID, dmm.dedupeViewName,dmm.dedupeMatchMethodDescription,dt.name,dmm.dedupeTypeID
			ORDER BY c.CountryDescription
		</CFQUERY>

		<cfreturn selectMatches>

	</cffunction>

<!--- ==============================================================================
       Based on tableName, retrieve actual data for matches.
===============================================================================
2013-11-26	WAB		Case 438202 - added a maxNumberToProcess parameter
--->

	<cffunction access="public" name="getMatchesForDisplay" hint="Retrieves the match data for display purposes." returnType="query">
		<cfargument name="tableName" type="any" required="true">
		<cfargument name="duplicateType" type="any" required="true">
		<cfargument name="countryID" type="string" required="true">
		<cfargument name="matchMethodID" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="maxNumberToProcess" type="numeric" default="5">

		<cfscript>
			var getMatches = queryNew("");
			var getFirstMatches = "";
			var getNotADupeList = "";
			var NotADupeList = "";
			var temp = "";
			temp = application.com.dedupe.RemoveNoLongerValidDupes(matchMethodID = arguments.matchMethodID,countryIDlist = arguments.countryID);
		</cfscript>

		<cfswitch expression="#matchMethodID#">
			<cfcase value="1">
				<cfquery name="getFirstMatches" datasource="#datasource#" maxrows="#maxNumberToProcess#">
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<CFQUERY NAME="getMatches" DATASOURCE="#datasource#">
					SELECT 		p.firstname, p.lastName, p.email, p.OrganisationID, dm.countryID, IDToKeep AS ID, 'keep' AS action, matchName =  p.matchname + '#application.delim1#' + CAST(p.OrganisationID AS varchar(50)), country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		p.firstname, p.lastName, p.email, p.OrganisationID, dm.countryID, IDToDelete AS ID, 'delete' AS action, matchName =  p.matchname + '#application.delim1#' + CAST(p.OrganisationID AS varchar(50)), country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	matchName, action desc
				</CFQUERY>
			</cfcase>
			<cfcase value="2">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		o.OrganisationName, o.OrganisationID, o.MatchName, dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = dm.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		o.OrganisationName, o.OrganisationID, o.MatchName, dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = dm.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	o.MatchName, action desc
				</CFQUERY>
			</cfcase>
			<cfcase value="3">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								l.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<!--- WAB 26-01-2011 was a small error in this query, fixed and also recrafted to use explicit JOINS --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 	distinct	LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID,
								<!--- START: NYB 2009-05-11 replaced:
								MatchName = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(l.Address1)), ' ', ''), '.', ''), ',', ''), '�', 'ss'), '?','oe'), '�','e'), '''','`') + '-' + REPLACE(RTRIM(LTRIM(l.PostalCode)), ' ', ''),
								WITH --->
								MatchName = cast(o.organisationid as varchar(255)) +'#application.delim1#'+ cast(l.MatchName as nvarchar(500)),
								<!--- END: NYB 2009-05-11 replaced --->
								dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription, o.organisationID, o.organisationName
					FROM		organisation o
								INNER JOIN
								location l ON o.organisationID = l.organisationID
								INNER JOIN
								country c ON c.countryID = l.countryID
								INNER JOIN
								dedupeMatches dm ON l.locationID = dm.IDToKeep
								INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>

					UNION
					SELECT	distinct	LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID,
								<!--- START: NYB 2009-05-11 replaced:
								MatchName = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(l.Address1)), ' ', ''), '.', ''), ',', ''), '�', 'ss'), '?','oe'), '�','e'), '''','`') + '-' + REPLACE(RTRIM(LTRIM(l.PostalCode)), ' ', ''),
								WITH --->
								MatchName = cast(o.organisationid as varchar(255)) +'#application.delim1#'+ cast(l.MatchName as nvarchar(500)),
								<!--- END: NYB 2009-05-11 replaced --->
								dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription, o.organisationID, o.organisationName
					FROM		organisation o
								INNER JOIN
								location l ON o.organisationID = l.organisationID
								INNER JOIN
								country c ON c.countryID = l.countryID
								INNER JOIN
								dedupeMatches dm ON l.locationID = dm.IDToKeep
								INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>
			<cfcase value="4">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								l.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<!--- WAB 2010/11/02 this query was failing because being ordered by l.matchname which was not in the query
					decided to order by l.address1, which is (roughly) what the matching was on
					decided not to repeat the replace statement again
					Then 2011/02/09 realised that I could order by an alias anyway
				 --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID,
								MatchName = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(l.Address1)), ' ', ''), '.', ''), ',', ''), '�', 'ss'), '?','oe'), '�','e'), '�','o'), '''','`') + '-' + CAST(l.OrganisationID AS varchar(50)),
								dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription, o.organisationID, o.organisationName, l.address1
					FROM		organisation o, country c, dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = l.countryID AND
								o.organisationID = l.organisationID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID,
								MatchName = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(l.Address1)), ' ', ''), '.', ''), ',', ''), '�', 'ss'), '?','oe'), '�','e'), '�','o'), '''','`') + '-' + CAST(l.OrganisationID AS varchar(50)),
								dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription, o.organisationID, o.organisationName, l.address1
					FROM		organisation o, country c, dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToDelete INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = l.countryID AND
								o.organisationID = l.organisationID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>
			<!--- 2006/06/22 GCC P_SNY039 fuzzy match of orgs with different names but the same non null vatnumber in the same country --->
			<cfcase value="6">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								o.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<!--- 2008/04/03 - GCC - Added VAT to ensure the string is treated as such otherwise CF thinks 000-1 is the same as 00000-1 in massdedupe2.cfm --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		o.OrganisationName, o.OrganisationID, MatchName = 'VAT' + cast(o.VATnumber as nvarchar(50)) + '-' + cast(o.CountryID as nvarchar(50)), dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		o.OrganisationName, o.OrganisationID, MatchName = 'VAT' + cast(o.VATnumber as nvarchar(50)) + '-' + cast(o.CountryID as nvarchar(50)), dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>

			<cfcase value="7">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								o.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		o.OrganisationName, o.OrganisationID, MatchName = orgURL, dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		o.OrganisationName, o.OrganisationID, MatchName = orgURL, dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>
								organisationid  not in ( <cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) AND
								dm.IDTokeep  in ( <cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>

			<cfcase value="8">
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- GCC 2008/10/09 Fixed bug with ordering of matches by removing firstnmae, lastname from the order by statement
					meant in cases whereby the deleter was alphabetically first the deleter was kept and the keeper was deleted despite scoring --->
				<CFQUERY NAME="getMatches" DATASOURCE="#datasource#">
					SELECT 		p.firstname, p.lastName, p.Email, MatchName = LTRIM(RTRIM(p.Email)), p.OrganisationID, dm.countryID, IDToKeep AS ID, 'keep' AS action, country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		p.firstname, p.lastName, p.Email, MatchName = LTRIM(RTRIM(p.Email)), p.OrganisationID, dm.countryID, IDToDelete AS ID, 'delete' AS action, country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>

			<cfcase value="9">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								l.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID, MatchName = REPLACE(RTRIM(LTRIM(l.PostalCode)), ' ', '') + '-' + CAST(l.OrganisationID AS varchar(50)), dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription, o.organisationID, o.organisationName
					FROM		organisation o, country c, dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = l.countryID AND
								o.organisationID = l.organisationID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID, MatchName = REPLACE(RTRIM(LTRIM(l.PostalCode)), ' ', '') + '-' + CAST(l.OrganisationID AS varchar(50)), dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription, o.organisationID, o.organisationName
					FROM		organisation o, country c, dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToDelete INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = l.countryID AND
								o.organisationID = l.organisationID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>

			<cfcase value="10">
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- GCC 2008/10/09 Fixed bug with ordering of matches by removing firstnmae, lastname from the order by statement
					meant in cases whereby the deleter was alphabetically first the deleter was kept and the keeper was deleted despite scoring --->
				<CFQUERY NAME="getMatches" DATASOURCE="#datasource#">
					SELECT 		p.firstname, p.lastName, p.email, MatchName = REPLACE(REPLACE(LTRIM(RTRIM(p.FirstName)),'�','ue'),'�','oe') + '-' + REPLACE(REPLACE(LTRIM(RTRIM(p.LastName)),'�','ue'),'�','oe') + '-' + CAST(p.OrganisationID AS varchar(50)), p.OrganisationID, dm.countryID, IDToKeep AS ID, 'keep' AS action, country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		p.firstname, p.lastName,p.email, MatchName = REPLACE(REPLACE(LTRIM(RTRIM(p.FirstName)),'�','ue'),'�','oe') + '-' + REPLACE(REPLACE(LTRIM(RTRIM(p.LastName)),'�','ue'),'�','oe') + '-' + CAST(p.OrganisationID AS varchar(50)), p.OrganisationID, dm.countryID, IDToDelete AS ID, 'delete' AS action, country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>
			<cfcase value="11">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								o.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		o.OrganisationName, o.OrganisationID, MatchName = cast(IDtoKeep as varchar(20)) , dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		o.OrganisationName, o.OrganisationID, MatchName = cast(IDtoKeep as varchar(20)) , dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>
								organisationid  not in ( <cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) AND
								dm.IDTokeep  in ( <cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>

			<!--- NJH 2008/02/01 CR-TND510 Blocked/Unblocked - added cases 12 and 13. --->
			<cfcase value="12">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								o.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
									dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		o.OrganisationName, o.OrganisationID, cast(dm.dedupeMatchID as varchar) as MatchName, dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToKeep
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		o.OrganisationName, o.OrganisationID, cast(dm.dedupeMatchID as varchar) as MatchName, dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription
					FROM		country c, dedupeMatches dm INNER JOIN
								organisation o ON o.organisationID = dm.IDToDelete
					WHERE		dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = o.countryID AND
								<cfif getFirstMatches.recordCount gt 0>
								organisationid  not in ( <cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) AND
								dm.IDTokeep  in ( <cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>
			<cfcase value="13">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep INNER JOIN
								dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								l.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
									dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID, cast(dm.dedupeMatchID as varchar) as MatchName, dm.countryID, IDToKeep AS ID, 'keep' AS action, c.countryDescription, o.organisationID, o.organisationName
					FROM		organisation o, country c, dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToKeep
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								c.countryID = l.countryID AND
								o.organisationID = l.organisationID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		LocationAddress = l.address1 + ' ' + l.address2 + ' ' + l.postalCode, l.locationID, cast(dm.dedupeMatchID as varchar) as MatchName, dm.countryID, IDToDelete AS ID, 'delete' AS action, c.countryDescription, o.organisationID, o.organisationName
					FROM		organisation o, country c, dedupeMatches dm INNER JOIN
								location l ON l.locationID = dm.IDToDelete
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								c.countryID = l.countryID AND
								o.organisationID = l.organisationID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>
			<!--- START: 2013-09-19 NYB Case 436820 - added: --->
			<cfcase value="14">
				<!--- get first 5 matches --->
				<cfquery name="getFirstMatches" datasource="#datasource#" >
					SELECT 		distinct TOP #arguments.maxNumberToProcess# dm.IDToKeep
					FROM		dedupeMatches dm
								INNER JOIN person p ON p.personID = dm.IDToKeep
								INNER JOIN location l ON p.locationID = l.locationID
								INNER JOIN dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								l.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								dm.IDToKeep not in (
									select IDToDelete
									FROM dedupeMatches dm INNER JOIN
									dedupeMatchLog dml ON dm.dedupeMatchLogID = dml.dedupeMatchLogID AND dml.dedupeMatchMethodID =  <cf_queryparam value="#matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE dm.tableName =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
									dm.mergeState = 'T'
									)
				</cfquery>
				<!--- get the items matched to the matches and their data. --->
				<cfquery name="getMatches" datasource="#datasource#">
					SELECT 		p.firstname, p.lastName, p.email, cast(dm.dedupeMatchID as varchar) as MatchName, p.OrganisationID, dm.countryID, IDToKeep AS ID, 'keep' AS action, country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToKeep
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType = <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' AND
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					UNION
					SELECT		p.firstname, p.lastName,p.email, cast(dm.dedupeMatchID as varchar) as MatchName, p.OrganisationID, dm.countryID, IDToDelete AS ID, 'delete' AS action, country = c.countryDescription
					FROM		location l, country c, dedupeMatches dm INNER JOIN
								person p ON p.personID = dm.IDToDelete
					WHERE		lower(dm.tableName) =  <cf_queryparam value="#lcase(tableName)#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.duplicateType =  <cf_queryparam value="#duplicateType#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
								dm.mergeState = 'T' and
								l.locationID = p.locationID AND
								c.countryID = l.countryID AND
								<cfif getFirstMatches.recordCount gt 0>dm.IDTokeep in (<cf_queryparam value="#valueList(getFirstMatches.IDTokeep)#" cfsqltype="cf_sql_integer" list=True>)<cfelse>dm.IDTokeep in (0)</cfif>
					ORDER BY	MatchName, action desc
				</CFQUERY>
			</cfcase>
			<!--- END: 2013-09-19 NYB Case 436820 --->
		</cfswitch>

		<cfreturn getMatches>

	</cffunction>

<!--- ==============================================================================
          Retrieves best data based on 2 entityIDs
=============================================================================== --->
	<cffunction access="public" name="getBestMatchData" hint="runs relevant methods and returns a result of the best data based on the 2 match IDs">
		<cfargument name="IDToKeep" type="any" required="true">
		<cfargument name="IDToDelete" type="any" required="true">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">


		<cfset var qDataToKeep = '' />
		<cfset var qDataToDelete = '' />
		<cfset var qFinalData = '' />

		<cfinvoke method="getEntityData" returnvariable="qDataToKeep" entityType="#tableName#" entityID="#IDToKeep#">
		<cfinvoke method="getEntityData" returnvariable="qDataToDelete" entityType="#tableName#" entityID="#IDToDelete#">
		<cfinvoke method="setBestMatchData" returnvariable="qFinalData" dataToKeep="#qDataToKeep#" dataToDelete="#qDataToDelete#">

		<cfreturn qFinalData>

	</cffunction>

<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="public" name="getEntityData" hint="get data for entity based on type and id">
		<cfargument name="entityID" type="any" required="true">
		<cfargument name="entityType" type="string" required="true">

			<cfset var entityData = '' />

			<cfswitch expression="#entityType#">
				<cfcase value="Person">
					<cfinvoke method="getPersonData" returnvariable="entityData" entityID="#entityID#">
				</cfcase>
				<cfcase value="Organisation">
					<cfinvoke method="getOrganisationData" returnvariable="entityData" entityID="#entityID#">
				</cfcase>
				<cfcase value="Location">
					<cfinvoke method="getLocationData" returnvariable="entityData" entityID="#entityID#">
				</cfcase>
			</cfswitch>

		<cfreturn entityData>

	</cffunction>

	<cffunction access="public" name="getPersonData" hint="get data for Person based on PersonID">
		<cfargument name="entityID" type="any" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var qPersonData = '' />

		<cfquery name="qPersonData" datasource="#datasource#">
			select *
			from Person
			where PersonID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn qPersonData>

	</cffunction>

	<cffunction access="public" name="getOrganisationData" hint="get data for organisation based on ID">
		<cfargument name="entityID" type="any" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var qOrganisationData = '' />

		<cfquery name="qOrganisationData" datasource="#datasource#">
			select *
			from organisation
			where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn qOrganisationData>

	</cffunction>

	<cffunction access="public" name="getLocationData" hint="get data for location based on ID">
		<cfargument name="entityID" type="any" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var qLocationData = '' />

		<cfquery name="qLocationData" datasource="#datasource#">
			select *
			from location
			where locationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn qLocationData>

	</cffunction>

	<cffunction access="public" name="setBestMatchData" hint="copies data from second record into first if there is null data in the first and not in the second.">
		<cfargument name="dataToKeep" type="query" required="true">
		<cfargument name="dataToDelete" type="query" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var dToKeepCol = '' />
		<cfset var dToDeleteCol = '' />
		<cfset var colName = '' />

		<cfset var colList = dataToKeep.columnList>
		<cfset var returnQuery = queryNew(colList)>
		<cfset queryAddRow(returnQuery,1)>

		<cfloop list="#colList#" index="colName">
			<cfset dToKeepCol = evaluate("dataToKeep.#colName#")>
			<cfset dToDeleteCol = evaluate("dataToDelete.#colName#")>
			<cfif dToKeepCol neq "">
				<cfscript>
					querySetCell(returnQuery,colName,dToKeepCol);
				</cfscript>
			<cfelse>
				<cfscript>
					querySetCell(returnQuery,colName,dToDeleteCol);
				</cfscript>
			</cfif>
		</cfloop>

		<cfreturn returnQuery>

	</cffunction>

<!--- ==============================================================================
       writeBestMatchData: Update table based on tableName with data provided

	   2009/05/01 GCC - fixed issue with incrementing without having output a statement and added float
=============================================================================== --->
	<cffunction access="public" name="writeBestMatchData" hint="Update table based on tableName with data provided">
		<cfargument name="goodData" type="query" required="true">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="entityID" type="any" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var isDeleted = '' />
		<cfset var sqlStatement = '' />
		<cfset var incr = '' />
		<cfset var col = '' />
		<cfset var qUpdateData = '' />
		<cfset var columnData = '' />

		<cfset var colList = goodData.columnList>
		<cfset var IDName = trim(tableName)&"ID">

		<!--- 2015-08-21 DAN case 445491/445434 - fix the typo on excludeComputedColumns --->
		<cfinvoke method="getColumnData" returnvariable="columnData" tableName="#tableName#" inCols="#colList#" excludeComputedColumns="true"><!--- dxc case 445491/445434 --->

		<cfset isDeleted = structDelete(columnData,IDName,true)>

		<cfif goodData.recordCount gt 0 and listContainsNoCase(colList,IDName) and structCount(columnData) gt 0>

			<cfset sqlStatement = "update #tableName# set ">
			<cfset incr = 1>
			<cfloop collection="#columnData#" item="col">
				<cfif listContainsNoCase(colList,col)>
					<cfswitch expression="#columnData[col]#">
						<cfcase value="char,varchar,nvarchar,datetime" delimiters=",">
							<cfif incr gt 1>
								<cfset sqlStatement = sqlStatement & ", ">
							</cfif>
                            <!--- 2015-08-21 DAN case 445491/445434 - START  --->
                            <cfset var evaluatedGoodData = trim(replace(evaluate("goodData."&col),"'","''","ALL"))>
                            <cfset var queryPart = " = N'" & evaluatedGoodData & "'">
                            <cfif col eq "crmPerID" and evaluatedGoodData eq "">
                                <!--- this is a hack to prevent SQL error about duplicated value for unique key column crmPerID, so we need to set it to NULL instead of an empty string --->
                                <cfset var queryPart = " = NULL">
                            </cfif>
							<cfset sqlStatement = sqlStatement & col & queryPart> <!--- 2007/10/29 WAB added N   - was losing nvarchar data, test suggest that doesn't matter  having N on non nvarchars!--->
							<!--- 2015-08-21 DAN case 445491/445434 - END  --->
							<cfset incr = incr + 1>
						</cfcase>
						<cfcase value="int,decimal,smallint,bit,float" delimiters=",">  <!--- 2005-06-28 WAB added smallint and bit --->
							<cfif trim(evaluate("goodData."&col)) neq "">
								<cfif incr gt 1>
									<cfset sqlStatement = sqlStatement & ", ">
								</cfif>
								<cfset sqlStatement = sqlStatement & col & " = " & trim(evaluate("goodData."&col))>
								<cfset incr = incr + 1>
							</cfif>
						</cfcase>
					</cfswitch>

				</cfif>
			</cfloop>
			<cfset sqlStatement = sqlStatement & " where " & IDName & " = " & entityID>
			<cfquery name="qUpdateData" datasource="#datasource#">
				#preserveSingleQuotes(sqlStatement)#
			</cfquery>
		<cfelse>
			<cfreturn false>
		</cfif>

		<cfreturn true>

	</cffunction>

<!--- ==============================================================================
       getColumnData: Returns column meta data for a given tablename
=============================================================================== --->
	<cffunction access="public" name="getColumnData" hint="Retrieve specific column data from table based on tableName">
		<cfargument name="tableName" type="variablename" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="excludeComputedColumns" default="false"><!--- dxc case 445491/445434 --->

		<cfset var rColumnData = '' />
		<cfset var qColumnData = '' />

<!--- START dxc case 445491/445434 --->
		<cfquery name="qColumnData" datasource="#datasource#">
			select c.COLUMN_NAME, c.DATA_TYPE
			from INFORMATION_SCHEMA.COLUMNS c
			<!--- 2015-08-21 DAN case 445491/445434 - fix the join with computed_columns --->
			left join sys.computed_columns cc on cc.name=c.column_name and cc.object_Id=OBJECT_ID('#tableName#')
			where c.table_name =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfif excludeComputedColumns>and cc.object_id is null</cfif>
		</cfquery>
<!--- END dxc case 445491/445434 --->
		<cfset rColumnData = structNew()>
		<cfloop query="qColumnData">
			<cfset rColumnData[column_name] = data_type>
		</cfloop>

		<cfreturn rColumnData>

	</cffunction>

<!--- ==============================================================================
       Merges two organisation records
=============================================================================== --->
	<cffunction access="public" name="mergeOrganisation" hint="Merge organisation based on IDToKeep and IDToDelete.">
		<cfargument name="IDToKeep" type="any" required="true">
		<cfargument name="IDToDelete" type="any" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var x = '' />
		<cfset var delDupeOrgDataSources = '' />
		<cfset var dedupeOrganisation = '' />
		<cfset var setMergeState = '' />
		<cfset var result = structNew()>
		<cfset result.isok = true>
		<cfset result.message = "Success">

		<cfquery name="delDupeOrgDataSources" datasource="#datasource#">
			delete OrgDataSource
			from
				OrgDataSource,  OrgDataSource as OrgDataSourceSave
			where
				OrgDataSource.organisationid =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_INTEGER" >
				and OrgDataSourceSave.organisationid =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_INTEGER" >
				and OrgDataSource.dataSourceID = OrgDataSourceSave.dataSourceID
				and OrgDataSource.remotedataSourceID = OrgDataSourceSave.remotedataSourceID
		</cfquery>

		<!--- <CFSTOREDPROC PROCEDURE="dedupeOrganisation" DATASOURCE="#datasource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RemoveOrganisationID" value="#IDToDelete#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RetainOrganisationID" value="#IDToKeep#" null="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Run" VALUE="Yes" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@createdByPersonID" value="#request.relaycurrentuser.personid#" null="No">
		</CFSTOREDPROC> --->
		<cftry>
			<cfquery name="dedupeOrganisation" datasource="#datasource#">
				exec dedupeOrganisation @RemoveOrganisationID =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_INTEGER" >  , @RetainOrganisationID =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_INTEGER" > , @Run="Yes" , @createdByPersonID=#request.relaycurrentuser.personid#
			</cfquery>

			<cfquery name="setMergeState" datasource="#datasource#">
				update dedupeMatches set
					mergeState = 'M'
				where
					IDToKeep =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
					IDToDelete =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
					lower(tableName) = 'organisation'
			</cfquery>
			<cfcatch type="any">
				<cfset result.isok = false>
				<cfset result.message = "Not Merged">
				<!--- 2009/05/06 - Any issues set as not a dupe - may need to be more specifc at a later date but enables auto dedupe to continue after a problem instead of jam
				could do with error message reporting in the same way as location --->
				<cfscript>
					x = application.com.dedupe.setAsNotADuplicate(idToKeep=IDToKeep,idToDelete=IDToDelete,tablename="organisation");
				</cfscript>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>

<!--- ==============================================================================
       Merges two person records
=============================================================================== --->
	<cffunction access="public" name="mergePerson" hint="Merge person based on IDToKeep and IDToDelete.">
		<cfargument name="IDToKeep" type="any" required="true">
		<cfargument name="IDToDelete" type="any" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var x = '' />
		<cfset var setMergeState = '' />
		<cfset var result = structNew()>
		<cfset result.isok = true>
		<cfset result.message = "Success">

		<cftry>
			<CFSTOREDPROC PROCEDURE="dedupePerson" DATASOURCE="#datasource#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RemovePersonID" value="#IDToDelete#" null="No">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RetainPersonID" value="#IDToKeep#" null="No">
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Run" VALUE="Yes" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@createdByPersonID" value="#request.relaycurrentuser.personid#" null="No">

			</CFSTOREDPROC>

			<cfquery name="setMergeState" datasource="#datasource#">
				update dedupeMatches set
					mergeState = 'M'
				where
					IDToKeep =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
					IDToDelete =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
					lower(tableName) = 'person'
			</cfquery>
			<cfcatch type="any">
				<cfset result.isok = false>
				<cfset result.message = "Not Merged">
				<!--- 2009/05/06 - Any issues set as not a dupe - may need to be more specifc at a later date but enables auto dedupe to continue after a problem instead of jam
				could do with error message reporting in the same way as location --->
				<cfset application.com.dedupe.setAsNotADuplicate(idToKeep=IDToKeep,idToDelete=IDToDelete,tablename="person")>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Dedupe",Severity="error",catch=cfcatch,warningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>

	<!--- Set the duplicate set to "not a duplicate". --->
	<cffunction access="public" name="setNotDupe" hint="Set duplicate in dedupeMatches to not a duplicate based on IDToKeep and IDToDelete and tableName.">
		<cfargument name="IDToKeep" type="any" required="true">
		<cfargument name="IDToDelete" type="any" required="true">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var setMergeState = '' />

		<cfquery name="setMergeState" datasource="#datasource#">
			update dedupeMatches set
				mergeState = 'R'
			where
				(
				IDToKeep =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
				IDToDelete =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >
				)
				or
				(
				IDToKeep =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
				IDToDelete =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >
				)
				and
				lower(tableName) =  <cf_queryparam value="#lcase(trim(tableName))#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

	</cffunction>

<!--- ==============================================================================
       Merges two location records
=============================================================================== --->
	<cffunction access="public" name="mergeLocation" hint="Merge location based on IDToKeep and IDToDelete.">
		<cfargument name="IDToKeep" type="any" required="true">
		<cfargument name="IDToDelete" type="any" required="true">
		<cfargument name="allowDedupeAcrossOrgs" type="boolean" default="no"> <!--- NJH TND510 Blocked/Unblocked. Need to dedupe locations across organisations--->
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

	<!---
	WAB 2005-10-26
	Changed to call dedupeLocation rather than dedupe entity.
	This stored procedure has some checking  - locations must be in same organisation and country
	I have added a very rough cftry to catch the errors

2007/07/18  changed to return a structure with an error message

	NJH/GCC 2008/05/16 defaulted the result.message to success, as we display the results to the user
	 --->

		<cfset var x = '' />
		<cfset var dedupeLocation = '' />
		<cfset var setMergeState = '' />
		<cfset var setNotAMergeState = '' />
		<cfset var result = structNew()>
		<cfset result.isok = true>
		<cfset result.message = "Success">

<cftry>

<!---
		WAB 2006-02-28 removed the cfstroedproc and replaced with a direct call.
			for some reason cfstorderproc did not work and I could not debug it

		<CFSTOREDPROC PROCEDURE="dedupeLocation" DATASOURCE="#datasource#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RemoveLocationID" value="#IDToDelete#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RetainLocationID" value="#IDToKeep#" null="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Run" VALUE="Yes" >
		</CFSTOREDPROC>
 --->

    <!--- START: 2016-01-12 DAN Case 447173 --->
    <cftransaction>
		<cfquery name="dedupeLocation" datasource="#datasource#">
            exec dedupeLocation
            @RemoveLocationID = <cf_queryparam value="#arguments.IDToDelete#" CFSQLTYPE="CF_SQL_INTEGER" >,
            @RetainLocationID = <cf_queryparam value="#arguments.IDToKeep#" CFSQLTYPE="CF_SQL_INTEGER" >,
            @Run="Yes",
            @createdByPersonID = <cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >,
            @AllowDedupeAcrossOrganisations = <cf_queryparam value="#arguments.allowDedupeAcrossOrgs#" CFSQLTYPE="CF_SQL_BIT" >
		</cfquery>

		<cfquery name="setMergeState" datasource="#datasource#">
			update dedupeMatches set
				mergeState = 'M'
			where
				IDToKeep =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
				IDToDelete =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
				lower(tableName) = 'location'
		</cfquery>
    </cftransaction>
    <!--- END: 2016-01-12 DAN Case 447173 --->

		<cfcatch type=database>
			<cfset result.isok = false>
			<cfif cfcatch.detail contains "Locations must be in same Country">
				<cfset result.message = "Locations must be in same Country to be merged">
				<!--- NJH 2008/02/26 set the merge state to be not a duplicate to prevent jamming as this will get picked up again as a possible duplicate... --->
				<cfscript>
					x = application.com.dedupe.setAsNotADuplicate(idToKeep=IDToKeep,idToDelete=IDToDelete,tablename="location");
				</cfscript>
				<cfoutput>Locations must be in same Country to be merged</cfoutput>
				<!--- <CF_ABORT> NJH 2008/02/26 removed the abort as that caused the transaction to be rolled back, therefore defeating the purpose of calling 'setAsNotADuplicate'--->
			<cfelseif cfcatch.detail contains "Locations must be in same Organisation">
				<!--- NJH 2008/02/26 set the merge state to be not a duplicate to prevent jamming as this will get picked up again as a possible duplicate... --->
				<cfscript>
					x = application.com.dedupe.setAsNotADuplicate(idToKeep=IDToKeep,idToDelete=IDToDelete,tablename="location");
				</cfscript>
				<cfoutput>Locations must be in same Organisation to be merged</cfoutput>
				<!--- <CF_ABORT> NJH 2008/02/26 removed the abort as that caused the transaction to be rolled back, therefore defeating the purpose of calling 'setAsNotADuplicate'--->
			<cfelseif cfcatch.detail contains "Dedupe Failed">   <!--- WAB 2007/07/18 added in for Trend (very last minute!) the list last is a hack to get the message after [sql server] in the sql server error message--->
				<cfset result.message = "#listlast(cfcatch.detail,"]")# ">
				<!--- 2007/11/27 GCC - remove those that have failed from the detected merge records - mainly for NavisionIDs at Trend but will affect all generic location merge errors... --->
				<!--- 2008/02/22 NJH
					This will set any dupes that are found by a 'Possible' or 'Definite' match method to no longer be a dupe
					but will leave merge only dupes still to be done
				--->
				<cfquery name="setNotAMergeState" datasource="#datasource#">
					update dedupeMatches set
						mergeState = 'N'
					from dedupeMatches dm inner join dedupeMatchLog dml
						on dm.dedupeMatchLogID = dml.dedupeMatchLogID inner join dedupeMatchMethod dmm
						on dmm.dedupeMatchMethodID = dml.dedupeMatchMethodID
					where
						dm.IDToKeep =  <cf_queryparam value="#IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
						dm.IDToDelete =  <cf_queryparam value="#IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
						lower(dm.tableName) = 'location' and
						dmm.dedupeTypeID <> 3
				</cfquery>
			<cfelse>
				<cfrethrow>
			</cfif>
		</cfcatch>

	</cftry>

<!--- 		<CFSTOREDPROC PROCEDURE="dedupeEntity" DATASOURCE="#datasource#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RemoveEntityID" value="#IDToDelete#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" dbvarname="@RetainEntityID" value="#IDToKeep#" null="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Entity" VALUE="location" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Run" VALUE="Yes" >
		</CFSTOREDPROC>
 --->
		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="setAsNotADuplicate" hint="Set a match as not being a duplicate">
		<cfargument name="IDToKeep" type="any" required="true">
		<cfargument name="IDToDelete" type="any" required="true">
		<cfargument name="tablename" type="string" required="true">

		<cfset var setNotAMergeState = '' />

		<cfquery name="setNotAMergeState" datasource="#application.siteDataSource#">
			update dedupeMatches set
				mergeState = 'N'
			from dedupeMatches dm inner join dedupeMatchLog dml
				on dm.dedupeMatchLogID = dml.dedupeMatchLogID inner join dedupeMatchMethod dmm
				on dmm.dedupeMatchMethodID = dml.dedupeMatchMethodID
			where
				dm.IDToKeep =  <cf_queryparam value="#arguments.IDToKeep#" CFSQLTYPE="CF_SQL_Integer" >  and
				dm.IDToDelete =  <cf_queryparam value="#arguments.IDToDelete#" CFSQLTYPE="CF_SQL_Integer" >  and
				lower(dm.tableName) =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

	</cffunction>

<!--- *********************************************************************************************************
		KEEP BELOW CODE FOR NOW, INCASE OF FUTURE NEED
	********************************************************************************************************** --->
	<cffunction access="public" name="updatePersonDataScore" hint="Inserts record into DataScore for each score">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="personID" type="any" required="true">
		<cfargument name="scoreGroupID" type="any" required="true">
		<cfargument name="score" type="any" required="true">

		<cfset var checkForDuplicate = '' />
		<cfset var logScore = '' />
		<cfset var OutOfScore = getWholeScore(datasource,scoreGroupID)>

		<cfquery name="checkForDuplicate" datasource="#datasource#">
			select DataScoreID
			from DataScore
			where ScoreGroupID =  <cf_queryparam value="#scoreGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				  PersonID =  <cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif checkForDuplicate.RecordCount gt 0>
			<cfquery name="logScore" datasource="#dataSource#">
				Update DataScore
				set	Score =  <cf_queryparam value="#score#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					OutOfScore =  <cf_queryparam value="#outOfScore#" CFSQLTYPE="cf_sql_integer" >,
					DateInserted = #createODBCDateTIme(now())#
				where DataScoreID =  <cf_queryparam value="#checkForDuplicate.DataScoreID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		<cfelse>
			<cfquery name="logScore" datasource="#dataSource#">
				insert into DataScore
				(PersonID,ScoreGroupID,Score,OutOfScore,DateScored)
				values
				(<cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#scoreGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#score#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#OutOfScore#" CFSQLTYPE="cf_sql_numeric" >,<cf_queryparam value="#createODBCDateTime(now())#" CFSQLTYPE="cf_sql_timestamp" >)
			</cfquery>
		</cfif>

		<cfreturn true>
		<!---
		update DataScore
			set datascore = x -- Nikki this is where you need to work out your approach
			were personid in (#personIDList#)
		--->

		<!--- <cfreturn message = "Records updated"> --->
	</cffunction>

<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="public" name="getScoreOnScoreGroup" hint="Runs the SQL statement to retrieve scores and then runs the update method">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="scoreGroupID" type="any" default="1">

		<cfset var loggedScore = '' />
		<cfset var getScores = '' />
		<cfset var SQLStatement = getFullCaseStatement(datasource,scoreGroupID)>

		<cfquery name="getScores" datasource="#datasource#">
			#preserveSingleQuotes(SQLStatement)#
		</cfquery>

		<cfloop query="getScores">
			<cfset loggedScore = updatePersonDataScore(datasource,personID,scoreGroupID,Score)>
		</cfloop>

		<cfquery name="getScores" datasource="#datasource#">
			select * from DataScore where ScoreGroupID =  <cf_queryparam value="#scoreGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn getScores>

	</cffunction>

<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="public" name="getWholeScore" hint="Gets The Entire Possible Score from the ScoreMethods table based on ScoreGroupID">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="scoreGroupID" type="any" required="true">

		<cfset var getOutOfScore = '' />

		<cfquery name="getOutOfScore" datasource="#datasource#">
			select SUM(HighScore) as WholeScore from ScoreMethod
			where ScoreGroupID =  <cf_queryparam value="#scoreGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn getOutOfScore.WholeScore>

	</cffunction>

<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="public" name="getScoreGroup" hint="Retrieves ScoreGroup and ScoreMethods for those groups based on ScoreGroupID">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="scoreGroupID" type="any" required="true">

		<cfset var getScoreGroupAndMethod = '' />

		<cfquery name="getScoreGroupAndMethod" datasource="#datasource#">
			select * from ScoreGroup sg, ScoreMethod sm
			where sg.ScoreGroupID =  <cf_queryparam value="#scoreGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				  sm.ScoreGroupID = sg.ScoreGroupID
		</cfquery>

		<cfreturn getScoreGroupAndMethod>

	</cffunction>

<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="public" name="createFullCaseStatement" hint="uses recordset of ScoreGroup and ScoreGroupMethods for a ScoreGroupID to create the sql select statement for scoring.">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="scoreGroupAndMethod" type="query" required="true">

		<cfset var incr = '' />
		<cfset var errCount = 0>
		<cfset var returnSQL = "Select">

		<cfif scoreGroupAndMethod.SelectFields neq "">
			<cfif findNoCase("PersonID",scoreGroupAndMethod.SelectFields)>
				<cfset returnSQL = returnSQL & " " & scoreGroupAndMethod.SelectFields & ", 'score' = ">
			<cfelse>
				<cfset returnSQL = returnSQL & " PersonID," & scoreGroupAndMethod.SelectFields & ", 'score' = ">
			</cfif>
		<cfelse>
			<cfset returnSQL = returnSQL & " PersonID, 'score' = ">
		</cfif>

		<cfset incr = 1>
		<cfloop query="scoreGroupAndMethod">
			<cfset returnSQL = returnSQL & CaseStatement>
			<cfif incr lt RecordCount>
				<cfset returnSQL = returnSQL & " + ">
			</cfif>
			<cfset incr = incr+1>
		</cfloop>

		<cfif scoreGroupAndMethod.RefTables neq "">
			<cfif findNoCase("Person",scoreGroupAndMethod.RefTables)>
				<cfset returnSQL = returnSQL & " from " & scoreGroupAndMethod.RefTables>
			<cfelse>
				<cfset returnSQL = returnSQL & " from Person," & scoreGroupAndMethod.RefTables>
			</cfif>
		<cfelse>
			<cfset returnSQL = returnSQL & " from Person">
		</cfif>

		<cfif scoreGroupAndMethod.OrderByFields neq "">
			<cfset returnSQL = returnSQL & "order by " & scoreGroupAndMethod.OrderByFields>
		</cfif>

		<cfreturn returnSQL>

	</cffunction>

	<cffunction access="public" name="getFullCaseStatement" hint="uses ScoreGroupID to get recordset from getScoreGroup() and pass it to createFullCaseStatement()">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="scoreGroupID" type="any" required="true">

		<cfreturn createFullCaseStatement(datasource,getScoreGroup(datasource,scoreGroupID))>

	</cffunction>

	<cffunction access="public" name="RemoveNoLongerValidDupes">
		<cfargument name="MatchMethodID" type="numeric" required="true">
		<cfargument name="countryIDlist" type="string" required="true">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<!--- GCC - 2005/09/29 - Marks dupes as state 'D' where ID to keep or delete no longer exists in the POL tables --->
		<cfscript>
			var getEntity = "";
			var qRemoveNoLongerValidDupes = "";
		</cfscript>

		<cfquery name="getEntity" datasource="#datasource#">
		Select DedupeEntityType as entity
		From dedupeMatchMethod
		where dedupeMatchMethodID = #arguments.matchMethodID#
		</cfquery>

		<!--- This type checking prevents codeCop rejecting the query --->
		<cfparam name="getEntity.entity[1]" type="variablename">

		<!--- 2012/02/14 GCC removed subqueries and unions and split into 3 separate queries for performance reasons--->
		<cfquery name="qRemoveNoLongerValidDupes" datasource="#datasource#">
			update dedupematches set mergestate = 'D'
			FROM         dedupeMatches dm with(nolock) LEFT OUTER JOIN
			                      #getEntity.entity# pol with(nolock) ON dm.IDToKeep = pol.#getEntity.entity#ID
			WHERE     (dm.tableName =  <cf_queryparam value="#getEntity.entity#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			and pol.#getEntity.entity#id is null
			and dm.countryid  in ( <cf_queryparam value="#arguments.countryIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and dm.mergestate ='T'

			update dedupematches set mergestate = 'D'
			FROM         dedupeMatches dm with(nolock) LEFT OUTER JOIN
			                      #getEntity.entity# pol with(nolock) ON dm.IDToDelete = pol.#getEntity.entity#ID
			WHERE     (dm.tableName =  <cf_queryparam value="#getEntity.entity#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			and pol.#getEntity.entity#id is null
			and dm.countryid  in ( <cf_queryparam value="#arguments.countryIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and dm.mergestate ='T'

			--2007/02/15 - GCC - MatchMethodID 11 causes this very rarely. Hack for now...
			update dedupematches set mergestate = 'D'
			FROM         dedupeMatches dm with(nolock)
			WHERE     IDToKeep = IDToDelete
			and tableName =  <cf_queryparam value="#getEntity.entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and dm.countryid  in ( <cf_queryparam value="#arguments.countryIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and dm.mergestate ='T'
		</cfquery>
		<cfreturn "Success">
	</cffunction>

	<!--- GCC - 2005/10/14 - P_SNY031 Data integrity
	2013-11-26	WAB		Case 438202 - added a maxNumberToProcess parameter
	--->
	<cffunction access="public" name="AutoDeDupe">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="duplicateType" type="string" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="matchMethodID" type="numeric" required="true">
		<cfargument name="maxNumberToProcess" type="numeric" default="5">

		<cfset var trueMatchAmount = '' />
		<cfset var lastMatch = '' />
		<cfset var matchAmount = '' />
		<cfset var keepID = '' />
		<cfset var incr = '' />
		<cfset var autoMergeData = '' />
		<cfset var getMerges = '' />

		<cfscript>
			var frmMatchData = tableName & "," & duplicateType & "," & countryID & "," & matchMethodID;
			var getMatchMaximum = "";
			var getMatchAmount = "";
			var getAllMatches = application.com.dedupe.getMatchesForDisplay(argumentCollection = arguments);
		</cfscript>

		<cfif getAllMatches.recordCount eq 0>
			<cfreturn 0>
		<cfelse>

			<cfquery name="getMerges" dbtype="query">
				select ID as matchID
				from getAllMatches
				where lower([action]) = 'keep'
			</cfquery>

			<!--- get the amount of actual matches displayed per page, based on the action 'keep'. --->
			<cfquery name="getMatchMaximum" dbtype="query">
				select distinct lower(MatchName)
				from getAllMatches
			</cfquery>
			<cfset trueMatchAmount = getMatchMaximum.recordCount>
			<cfset lastMatch = "">
			<cfset matchAmount = 0>
			<cfset keepID = "">
			<cfset incr = 1>
			<CFOUTPUT query="getAllMatches">
				<cfset autoMergeData = "">
				<cfif lastMatch neq "#matchName#">
					<cfquery name="getMatchAmount" dbtype="query">
						select ID as matchID
						from getAllMatches
						where lower(matchName) = '#lcase(matchName)#'
					</cfquery>
					<cfset matchAmount = getMatchAmount.recordCount>
					<cfset keepID = ID>
					<cfset lastMatch = "#matchName#">
					<cfloop query="getMatchAmount">
						<cfif matchID neq keepID>
							<cfif autoMergeData eq "">
								<cfset autoMergeData = keepID&"|"&matchID>
							<cfelse>
								<cfset autoMergeData = autoMergeData&","&keepID&"|"&matchID>
							</cfif>
						</cfif>
					</cfloop>
				</cfif>
				<cfif keepID eq ID>
					<cfset "form.frmMergeType#incr#" = autoMergeData>
					<cfset incr = incr+1>
				</cfif>
			</cfoutput>
			<cfoutput>
				<cfset form.frmMatchAmount = trueMatchAmount>
				<cfset form.frmMatchData = frmMatchData>
				<cfset form.frmRunMatches = "true">
				<cfset form.scheduledMatch = "true">
			</cfoutput>
			<cfinclude template="/dataTools/massdedupe2.cfm">
			<cfreturn getMerges.recordcount>
		</cfif>
	</cffunction>

	<cffunction access="public" name="AutoDeDupeLog">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="matchMethodID" type="numeric" required="true">
		<cfargument name="dedupeQuantity" type="numeric" required="true">

		<cfscript>
			var qInsertAutoDeDupeLog = "";
		</cfscript>

		<cfquery name="qInsertAutoDeDupeLog" datasource="#application.sitedatasource#">
		INSERT into deDupeAutomationLog (matchMethodID,countryID,entityType,deDupeQuantity)
		VALUES (<cf_queryparam value="#arguments.matchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.dedupeQuantity#" CFSQLTYPE="CF_SQL_Integer" >)
		</cfquery>
		<cfreturn "success">
	</cffunction>

	<cffunction access="public" name="GetOrganisationMatchCountryIDs">
		<cfscript>
			var qGetOrganisationMatchCountryIDs = "";
			var countryIDs = "";
		</cfscript>
		<cfquery name="qGetOrganisationMatchCountryIDs" datasource="#application.sitedatasource#">
			Select distinct countryid
			from country
			where countryid in (select distinct countryID from organisation)
			or countryid in (select distinct countryID from location)
			order by countryid
		</cfquery>
		<cfoutput query="qGetOrganisationMatchCountryIDs">
			<cfset countryIDs = listappend(countryIDs,countryid)>
		</cfoutput>
		<cfreturn countryIDs>
	</cffunction>


	<!---
	function to work out who a person is now!
	i.e. given an personid that has been deduped, will tell you who they are now
	returns 0 if not found

	 --->
	<cffunction access="public" name="whoHasThisPersonBeenDedupedTo">
		<cfargument name="personid" type="numeric" required="true">

   			<cfscript>
				var personidToCheck = personid ;
				var personrecord =  application.com.globalfunctions.CFQUERY(SQLString: "select personid from person where personid = #personIdToCheck#");

			   while (personrecord.recordcount is 0 and personIdToCheck is not "") {
					// personid is not valid, but check to see if person has been deduped
					// if person has been deduped then personidToCheck will contain the new personid
					// if no sign of this personid then personidToCheck will be ""
					personIdToCheck = application.com.globalfunctions.CFQUERY(SQLString: "select * from dedupeAudit where recordtype='person' and archivedid = #personIdToCheck#").newid;

					if (personIdToCheck is not "") {
						personrecord  = application.com.globalfunctions.CFQUERY(SQLString: "select * from person where personid = #personIdToCheck#");
					}
				}

		</cfscript>

		<cfif personRecord.recordcount is 1>
			<cfreturn personRecord.personid>
		<cfelse>
			<cfreturn 0>
		</cfif>

	</cffunction>

	<cffunction access="public" name="reMarkRejected" hint="This function will reset any dupes that have already been rejected but have been reintroduced">
		<cfargument name="tableName" type="string" required="true">
		<cfscript>
			var qResetRejected = "";
		</cfscript>
		<!---Using this function will mean once a dedupe has been set to not being a dedupe it will never be reconsidered--->
		<cfquery name="qResetRejected" datasource="#application.sitedatasource#">
			update dedupeMatches
				Set MergeState = 'R'
				where dedupeMatchID in
					(	select d1.dedupeMatchID from dedupeMatches d1
						inner join dedupeMatches d2 ON d1.IDToKeep = d2.IDToKeep
						and d1.IDToDelete = d2.IDToDelete
						where d1.tablename =  <cf_queryparam value="#arguments.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and d2.tablename =  <cf_queryparam value="#arguments.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and d1.mergeState = 'T'
						and d2.mergeState = 'R')
		</cfquery>
	</cffunction>

	<cffunction access="public" name="checkForMergeProtection">
		<cfargument name="entityType" type="string" required="true">	<!--- be an id or string--->
		<cfargument name="entityID" type="numeric" required="true">

		<cfset var result = structNew()>
		<cfset var checkMergeProtection = "">
		<cfset var thisEntityType = arguments.entityType>

		<cfif not isNumeric(arguments.entityType)>
			<cfset thisEntityType = application.entityTypeID[arguments.entityType]>
		</cfif>

		<cfquery name="checkMergeProtection" datasource="#application.sitedatasource#">
			exec dedupeEntityCheckProtection @entityTypeID =  <cf_queryparam value="#thisEntityType#" CFSQLTYPE="CF_SQL_INTEGER" > , @entityID = #entityID#
		</cfquery>

		<cfset result.isMergeProtected = checkMergeProtection.recordCount>  <!--- 0 false > 0 = true --->
		<cfset result.detail = checkMergeProtection>

		<cfreturn result>

	</cffunction>



</cfcomponent>

