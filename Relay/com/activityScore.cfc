<!---
  --- activityScore
  --- -------------
  --- 
  --- author: Richard.Tingle
  --- date:   10/03/16
	
	Note scheme and scoreRuleGrouping are the same thing

19/08/2016	RJT	PROD2016-1315 Added fuctionality to get score informnation on other entities under the same organisation
  --->
<cfcomponent accessors="true" output="true" persistent="false">


	<cffunction name="getActiveModulesWithActivities" access="public" output="false" returntype="any">
		
		<cfset var getActiveModules="">
		<cfquery name="getActiveModules" datasource="#application.SiteDataSource#">
				select 1 as primaryOrder, 'Any' as moduleTextID, 'Any' as modulename 
			union
				select distinct 2 as primaryOrder, relayModule.moduleTextID as moduleTextID , relayModule.name as modulename
				from relayModule
				join relayModuleEntityType on relayModule.moduleID=relayModuleEntityType.relayModuleID
				join activityType on relayModuleEntityType.entityTypeID=activityType.entityTypeID
				where relayModule.active=1 
				
			union 
			
				select 3 as primaryOrder, 'Other' as moduleTextID, 'Other' as modulename 
			order by primaryOrder, modulename
			
		</cfquery>
		
		<cfreturn getActiveModules/>
	</cffunction>
	
	
	<cffunction name="getActivitiesInModule" access="public" output="false" returntype="any">
		<cfargument name="moduleTextId" required="true" hint="In addition to a moduleTextId this can be 'Any' or 'Other', any does what you'd imagine, other returns any activity whose entity isn't attached to a particular module' ">
		<cfargument name="entityTypeIDForScoring" type="numeric" required="true" hint="Some activities don't have data on certain levels, this filters those out">
		
		<!--- Return activities whose entities aren't part of a module or activities whose entities don't have a module --->
		<cfset var getActivities="">
		<cfquery name ="getActivities" datasource="#application.SiteDataSource#">
			select activityType.activityTypeID as activityTypeID, activityType.description as description from activityType
			left join  relayModuleEntityType on activityType.entityTypeID=relayModuleEntityType.entityTypeID
			left join relayModule on relayModuleEntityType.relayModuleID=relayModule.moduleID
			
			<cfswitch expression="#moduleTextId#">
				<cfcase value="any">
					where (active=1 or active is null) <!--- i.e. its entity is in an active module or not in a module at all --->
				</cfcase>	
				<cfcase value="other">
					where active is null <!--- I.e. not attached to a module --->
				</cfcase>
				<cfdefaultcase> 
					where moduleTextID=<cf_queryParam value="#arguments.moduleTextId#" CFSQLType="CF_SQL_VARCHAR">
				</cfdefaultcase>
			</cfswitch>
			and activityType.availableForScoring = 1
			and activityType.minimumEntityTypeIDWithData <= <cf_queryParam value="#arguments.entityTypeIDForScoring#" CFSQLType="CF_SQL_INTEGER"> 
			group by activityType.activityTypeID, activityType.description
			order by activityType.description	 
		</cfquery>

		<cfreturn getActivities>
				
	</cffunction>

	<cffunction name="getScoringTechniquesForActivity" access="public" output="false" returntype="array" hint="Returns an array of structures of calculation methods for this activity">
		<cfargument name="activityTypeID" required="true">
		<cfscript>
			//we use reflection to find the appropriate methods based on annotations. See activityScoreCalculationFunctions for full details
			
			var functionMetaData=GetMetaData(application.com.activityScoreCalculationFunctions).functions;
			var appropriateFunctions=filterFunctionsForActivityType(functionMetaData,activityTypeID);

			var validMethodsArray=ArrayNew(1);
			ArrayAppend(validMethodsArray,{method="default",display="phr_score_pointsPerActivity"});
			ArrayAppend(validMethodsArray,{method="scoreCommunicationReadBasedOnEntity",display="phr_score_awards_based_on_emtity_field"});

			for(var i=1;i<=arrayLen(appropriateFunctions);i++){
				var methodNamePhrase=structkeyexists(appropriateFunctions[i],"phrase")?appropriateFunctions[i].phrase:appropriateFunctions[i].name;

				ArrayAppend(validMethodsArray,{method=appropriateFunctions[i].name,display=methodNamePhrase});
			}

			//the default method is always valid


			return validMethodsArray;
		</cfscript>

	</cffunction>
	
	
		
	
	
	<!---
	 * Runs a rule definition over that period, the rule itself will have a validity period, the actual period applied under will
	 * be the union of these periods
	--->
	<cffunction name="runScoreRule" access="public" output="false">
		<cfargument name="ruleVersionID" required="true" type="numeric">
		<cfargument name="ruleDefinition" required="true" type="Struct">
		<cfargument name="startTime_exclusive" required="true">
		<cfargument name="endTime_inclusive" required="true">
		<cfargument name="capStartTime_exclusive" required="true">
		<cfargument name="capEndTime_inclusive" required="true">




			<cfscript>
				var ruleRunReport={isOk=true, scoreRuleVersersionID=ruleVersionID,ruleDefinition=ruleDefinition,startTime_exclusive=startTime_exclusive,endTime_inclusive=endTime_inclusive,capStartTime_exclusive=capStartTime_exclusive,capEndTime_inclusive=capEndTime_inclusive };
				

				//we'll be able to run this rule as a single batch
				var filterDefinition=ruleDefinition.filterDefinition;
				
				var filterSQL=application.com.filterSelectAnyEntity.getSQLForFilterDefinition(filterDefinition);
				
				var entityAssociatedWithRule_TypeId=getAssociatedEntityTypeIDForActivity(ruleDefinition.activityTypeID);
				
				var entityAssociatedWithRule_Table=application.entityType[entityAssociatedWithRule_TypeId].tableName;
				var entityAssociatedWithRule_UniqueKey=application.entityType[entityAssociatedWithRule_TypeId].UniqueKey;
			
				var entityCollectingScore_entityTypeID=getEntityTypeIDOfEntityCollectingScore(ruleVersionID);
			
				//Because we have both a period start/stop and a rule start/stop we want the tighter of these restrictions to apply
				var scoreStart_exclusive=(DateCompare(startTime_exclusive,ruleDefinition.validFrom)==1)?startTime_exclusive:ruleDefinition.validFrom;
				var scoreEnd_inclusive=((not isDate(ruleDefinition.validTo)) OR DateCompare(ruleDefinition.validTo,endTime_inclusive)==1)?endTime_inclusive:ruleDefinition.validTo;

				if (scoreEnd_inclusive==""){
					scoreEnd_inclusive=now();
				}
			</cfscript>
			
			<cfif entityCollectingScore_entityTypeID EQ Application.entityTypeId.person>
				<cfset var collectingEntityTableLocation="Activity.personID">
			<cfelseif entityCollectingScore_entityTypeID EQ Application.entityTypeId.location>	
				<cfset var collectingEntityTableLocation="Activity.locationID">
			<cfelseif entityCollectingScore_entityTypeID EQ Application.entityTypeId.organisation>	
				<cfset var collectingEntityTableLocation="Activity.organisationID">
			<cfelse>
				<cfthrow  message="EntityTypeID of entity type trying to collect scores (#entityCollectingScore_entityTypeID#) is not supported">
			</cfif>

			<cftry>
				<cfset var applyScores="">
				<cfset var result="">
				<cfquery result="result" name="applyScores" datasource="#application.sitedatasource#">
					
					declare @scoreRuleID int
					select @scoreRuleID=scoreRuleID from scoreRuleVersion where scoreRuleVersionID=<cf_queryparam value = "#ruleVersionID#" CFSQLType="CF_SQL_INTEGER">
					
					declare @schedualedTaskPerson int
					select @schedualedTaskPerson=personID from Person where FirstName='Scheduled' and LastName='Task'
					
					declare @scoreCap int
					select @scoreCap=<cf_queryparam value = "#ruleDefinition.scoreCap#" CFSQLType="CF_SQL_INTEGER">
								
					declare @uncappedNewScores TABLE 
						   ( 
						   id int IDENTITY(1,1) primary key NOT NULL, 
						   triggeringScoreRuleVersionID int, 
						   triggeringActivityID int, 
						   score int,
						   obtainingEntityID int, <!---denormalise for performance (will be a person, organisation or location)--->
						   remainingCap int <!---this could be calculated in the first run but is calculated on the second run for similicity--->
						   )
					<cfif ruleDefinition.SCORECALCULATIONMETHOD EQ "default">   
						insert into @uncappedNewScores (triggeringScoreRuleVersionID,triggeringActivityID,score,obtainingEntityID)
						select 
							<cf_queryparam value = "#ruleDefinition.scoreRuleVersionID#" CFSQLType="CF_SQL_INTEGER">,
							activityID as triggeringActivityID,
							<cf_queryparam value = "#ruleDefinition.scoreValue#" CFSQLType="CF_SQL_NUMERIC">,
							#collectingEntityTableLocation#
					
						from Activity
						join #entityAssociatedWithRule_Table# on Activity.entityID=#entityAssociatedWithRule_Table#.#entityAssociatedWithRule_UniqueKey# <!---used by filter--->
						#PreserveSingleQuotes(filterSQL)#
						where Activity.activityTypeID=<cfqueryparam value = "#ruleDefinition.activityTypeID#" CFSQLType="CF_SQL_INTEGER">
						and activityTime > <cf_queryparam value = "#scoreStart_exclusive#" CFSQLType="CF_SQL_TIMESTAMP"> 
						and activityTime <= <cf_queryparam value = "#scoreEnd_inclusive#" CFSQLType="CF_SQL_TIMESTAMP">

					<cfelse>
						<!--- This uses a CF function to score the entities --->
						<cfset var customScores=getCustomScores(ruleDefinition,scoreStart_exclusive,scoreEnd_inclusive,collectingEntityTableLocation,entityAssociatedWithRule_Table,entityAssociatedWithRule_UNIQUEKEY  )>
						<cfif arrayLen(customScores) GTE 1>
							INSERT INTO  @uncappedNewScores (triggeringScoreRuleVersionID,triggeringActivityID,score,obtainingEntityID) VALUES
							<cfset var i=1>
							<cfloop from=1 to = "#arrayLen(customScores)#" index="i" >
								<cfif i neq 1>
								,
								</cfif>
								(
								<cf_queryparam value = "#ruleDefinition.scoreRuleVersionID#" CFSQLType="CF_SQL_INTEGER">,
								<cfoutput>
									#customScores[i].triggeringActivityID#,
	                            <!---The score shown is the floor is the score shown, not rounded mathematically. [#2460]--->
	                            	round(CAST('#customScores[i].score#' as REAL),0),
									#customScores[i].obtainingEntityID#
								</cfoutput>
								)
							</cfloop>
						</cfif>
					</cfif>
	
					<cfif ruleDefinition.scoreCap NEQ "">
	
					<!--- Now that we've calculated the new scores if there were to be no cap we need to cap them, 
					first we need to know how much score the person has already scored in the current capping period
					for this score rule (NOT SCORE RULE VERSION).
					We store this in a table variable for later use
					--->
					declare @previousScoresByCollectingEntity TABLE 
						   ( 
						   entityID int primary key NOT NULL, 
						   totalScore  numeric(9,3) NOT NULL
						   )
	
					insert into @previousScoresByCollectingEntity(entityID,totalScore)
					select 
						#collectingEntityTableLocation#
						,
						sum(score)
					from score	
					left join scoreRuleVersion on score.triggeringScoreRuleVersionID=scoreRuleVersion.scoreRuleVersionID
					join activity on score.triggeringActivityID=activity.activityID
					where 
						scoreRuleVersion.scoreRuleID = @scoreRuleID
						and activityDate > <cf_queryparam value = "#capStartTime_exclusive#" CFSQLType="CF_SQL_TIMESTAMP">
						and activityDate <= <cf_queryparam value = "#capEndTime_inclusive#" CFSQLType="CF_SQL_TIMESTAMP">
						and #collectingEntityTableLocation# in (select obtainingEntityID from @uncappedNewScores)
					group by #collectingEntityTableLocation#
	
	
					<!--- 	We now have to find how much of the cap is left for each score, this is a little bit of a 
							nasty query, sql server 2012 apparently has the OVER keyword which would avoid the nasty subquery
					--->			
					
					update cappedUNS
					set cappedUNS.remainingCap=
						@scoreCap
						- ISNULL(previousScores.totalScore,0)
						- ISNULL((
						select sum(score) 
						from @uncappedNewScores runningUNS 
						where runningUNS.ID < cappedUNS.ID 
						and runningUNS.obtainingEntityID=cappedUNS.obtainingEntityID
						),0)
					from @uncappedNewScores cappedUNS
					left join @previousScoresByCollectingEntity previousScores on previousScores.entityID=cappedUNS.obtainingEntityID
					
					<!--- Apply the cap --->
					update @uncappedNewScores
					set score = (case 
									when score < remainingCap then score
									when remainingCap>0 then remainingCap
									else 0
								end)	
	
					</cfif>
					
					
					<!--- Insert the new scores --->
					Insert into score(triggeringScoreRuleVersionID, triggeringActivityID, score, lastUpdatedByPerson, lastUpdatedBy, createdBy)
					select triggeringScoreRuleVersionID,triggeringActivityID,score,@schedualedTaskPerson,@schedualedTaskPerson,@schedualedTaskPerson 
					from @uncappedNewScores
				
					select count(*) as scoresApplied from  @uncappedNewScores
				
				</cfquery>


				<cfset ruleRunReport.newScoresApplied=applyScores.scoresApplied>
			
				<cfcatch> 
					<cfset ruleRunReport.isOk=false>
					<cfset ruleRunReport.errorID=application.com.errorHandler.recordRelayError_CFCATCH(catch=cfcatch)>
				</cfcatch>
			</cftry>
				
		
		<cfreturn ruleRunReport>
	</cffunction>
	 
	<cffunction name="getCustomScores" access="public" output="false" returnType="array">
		<cfargument name="ruleDefinition" required="true" type="struct">
	 	<cfargument name="scoreStart_exclusive" required="true">
	 	<cfargument name="scoreEnd_inclusive" required="true">
	 	<cfargument name="collectingEntityTableLocation" required="true">
	 	<cfargument name="entityAssociatedWithRule_Table" required="true">
	 	<cfargument name="entityAssociatedWithRule_UNIQUEKEY" required="true">

	 		
	 		<cfset var filterDefinition=ruleDefinition.filterDefinition>
			<cfset var filterSQL=application.com.filterSelectAnyEntity.getSQLForFilterDefinition(filterDefinition)>
				
			<!--- grab the activities to be manually scored--->
			<cfset var getActivitiesToBeScored="">
	 		<cfquery name="getActivitiesToBeScored" datasource="#application.sitedatasource#">
		 		select 
				Activity.activityID as triggeringActivityID,
				Activity.entityID
				from Activity
				join #entityAssociatedWithRule_Table# on Activity.entityID=#entityAssociatedWithRule_Table#.#entityAssociatedWithRule_UniqueKey# <!---used by filter--->
				#PreserveSingleQuotes(filterSQL)#
				where Activity.activityTypeID=<cfqueryparam value = "#ruleDefinition.activityTypeID#" CFSQLType="CF_SQL_INTEGER">
				and activityTime > <cf_queryparam value = "#scoreStart_exclusive#" CFSQLType="CF_SQL_TIMESTAMP"> 
				and activityTime <= <cf_queryparam value = "#scoreEnd_inclusive#" CFSQLType="CF_SQL_TIMESTAMP"> 
			</cfquery>
			
			<cfscript>
				var resultsArray=ArrayNew(1);
				var staticArguments=deserializeJSON(ruleDefinition.nonStandardParameters);
				
				for(var i=1;i<=getActivitiesToBeScored.recordCount;i++){
					var methodArguments=structCopy(staticArguments);
					methodArguments.entityID=getActivitiesToBeScored["entityID"][i];
					methodArguments.activityID=getActivitiesToBeScored["triggeringActivityID"][i];
					methodArguments.entityTable=entityAssociatedWithRule_Table;
					var score=invoke(application.com.activityScoreCalculationFunctions, "#ruleDefinition.SCORECALCULATIONMETHOD#", methodArguments);
					var newResult={
						triggeringActivityID=getActivitiesToBeScored["triggeringActivityID"][i],
						obtainingEntityID=getActivitiesToBeScored["entityID"][i],
						score=score
					};
					
					arrayAppend(resultsArray,newResult);
				}
				
				return resultsArray;
				
			</cfscript>
			
	</cffunction>		

	<cffunction name="getScoresForEntityInSchemeByRule" access="public" output="false" returnType="query">
		<cfargument name="entityID" required="true" type="numeric">
		<cfargument name="schemeID" required="true" type="numeric">
		<cfargument name="startDate_exclusive" required="true">
		<cfargument name="endDate_inclusive" required="true">
		<cfargument name="includeCurrentRulesEvenIfEmpty" type="numeric" default="false">
		<cfargument name="entityTypeID" type="numeric" default="getScoreRuleGrouping(schemeID).recordSet.entityTypeID" hint="The entityTypeID (person, location or organisation that the passed entityID is). Defaults to the scheme's entity level">
		

		<cfscript>
			var namePhraseSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRule", phraseTextID = "name");
		</cfscript>


		
		<cfquery name="getActivitiesToBeScored" datasource="#application.sitedatasource#">
			Declare @actualScores table(
			scoreRuleID int,
			
			ActivitiesCompleted int,
			CurrentScore int,
			scoreRuleName nVarChar(max),
			activityTypeDescription nVarChar(max) null, <!--- this data can in principle change over the period, the most up to date is put in as the final step --->
			pointsPerActivity nVarChar(max)  null, <!--- this data can in principle change over the period, the most up to date is put in as the final step --->
			scoreCap nVarChar(max)  null <!--- this data can in principle change over the period, the most up to date is put in as the final step --->
			)
			
			insert into @actualScores(scoreRuleID,ActivitiesCompleted,CurrentScore,scoreRuleName)
			select 
			scoreRuleID,
			count(*) as ActivitiesCompleted,
			SUM(score) as CurrentScore,
			isNull(scoreRuleNameTranslation,'phr_ManualAdjustment') as scoreRuleName
			from vActivityScore
			where ScoreRuleGroupingID=<cf_queryparam value = "#arguments.schemeID#" CFSQLType="CF_SQL_INTEGER">
			and activityTime > <cf_queryparam value = "#startDate_exclusive#" CFSQLType = "CF_SQL_TIMESTAMP" > 
			and activityTime <= <cf_queryparam value = "#endDate_inclusive#" CFSQLType = "CF_SQL_TIMESTAMP" > 
			
			<cfif entityTypeID EQ application.entityTypeID.person>
				and personID=<cf_queryparam value = "#arguments.entityID#" CFSQLType="CF_SQL_INTEGER">
			<cfelseif entityTypeID EQ application.entityTypeID.location>
				and locationID=<cf_queryparam value = "#arguments.entityID#" CFSQLType="CF_SQL_INTEGER">
			<cfelseif entityTypeID EQ application.entityTypeID.organisation>
				and organisationID=<cf_queryparam value = "#arguments.entityID#" CFSQLType="CF_SQL_INTEGER">
			<cfelse>
				<cfthrow message="#entityTypeID# is not a supported entity type ID"> 
			</cfif>
			
			group by scoreRuleID,scoreRuleNameTranslation,activityTypeDescription
			
			
			<!--- Add the zero row entrys --->
			<cfif includeCurrentRulesEvenIfEmpty>
				insert into @actualScores (scoreRuleID,ActivitiesCompleted,CurrentScore,scoreRuleName)
				select distinct scoreRule.scoreRuleID,0 as ActivitiesCompleted, 0 as  CurrentScore, #namePhraseSnippet.select# as scoreRuleNameTranslation
				from scoreRule 
				#preserveSingleQuotes(namePhraseSnippet.join)#
				inner join scoreRuleVersion on scoreRule.scoreRuleID=scoreRuleVersion.scoreRuleID and validFrom <= <cf_queryparam value = "#endDate_inclusive#" CFSQLType = "CF_SQL_TIMESTAMP" > and <cf_queryparam value = "#startDate_exclusive#" CFSQLType = "CF_SQL_TIMESTAMP" > <= isNull(validTo,getDate())and active= 1 <!---filter to only active rules during the period --->
				where ScoreRuleGroupingID=<cf_queryparam value = "#arguments.schemeID#" CFSQLType="CF_SQL_INTEGER">
				and scoreRule.scoreRuleID not in (select scoreRuleID from @actualScores  where scoreRuleID is not null) <!---nulls really upset not ins --->
			</cfif>	
			
			<!---Fill in variable data (i.e. data where all the scores may not have actually had the same data but we just want the most recent--->
			update inputActualScores
			set 
				inputActualScores.activityTypeDescription = mostRecentVersion.Description ,
				inputActualScores.pointsPerActivity=mostRecentVersion.scoreValue,
				inputActualScores.scoreCap=mostRecentVersion.scoreCap
			from @actualScores inputActualScores
			join 
				(
				Select scoreRuleVersion.scoreRuleID,
				activityType.Description,
				case 
					when nonStandardMethod is null then cast(scoreValue as nVarChar(30))
					else 'phr_custom'
				End as scoreValue,
				case 
					when nonStandardMethod is null then cast(scoreCap as nVarChar(30))
					else 'phr_custom'
				End as scoreCap
				from scoreRuleVersion
				inner join
					(
						select MAX(validFrom) as validFrom, scoreRuleID
						from scoreRuleVersion
						where validFrom <= <cf_queryparam value = "#endDate_inclusive#" CFSQLType = "CF_SQL_TIMESTAMP" > and <cf_queryparam value = "#startDate_exclusive#" CFSQLType = "CF_SQL_TIMESTAMP" > <= isNull(validTo,getDate())
						and scoreRuleVersion.active = 1
						group by scoreRuleID
					) mostRecentFilter on scoreRuleVersion.scoreRuleID = mostRecentFilter.scoreRuleID and scoreRuleVersion.validFrom=mostRecentFilter.validFrom 
				join activityType on scoreRuleVersion.activityTypeID=activityType.activityTypeID
				) mostRecentVersion on mostRecentVersion.scoreRuleID=inputActualScores.scoreRuleID

			select scoreRuleID,activityTypeDescription,ActivitiesCompleted,CurrentScore,scoreRuleName,pointsPerActivity,scoreCap from 
				@actualScores
			
		</cfquery>
	
		<cfreturn getActivitiesToBeScored>
	
	</cffunction>

	<cffunction name="getScoresForEntitiesWithinOrganisationInSchemeByRule" access="public" output="false" returnType="struct" hint="In a particular organisation returns a report of the total score (in a period) for entities within; entities being whatever level the score is collected on">
		<cfargument name="schemeID" required="true" type="numeric">
		<cfargument name="organisationID" required="true" type="numeric">
		<cfargument name="startTime_exclusive" required="true" default="#CreateDate(1970, 1, 1)#">
		<cfargument name="endTime_inclusive" required="true" default="#CreateDate(Year(request.requestTimeODBC)+2,1,1)#"> <!--- This is just a date that is garanteed in the future and changes rarely (so the caching can be added at a later date) --->
		
		<cfscript>
			var schemeData=getScoreRuleGrouping(schemeID).recordSet;
			var returnStruct={scoringEntityTypeID=schemeData.entityTypeID,entityLevelReport=queryNew("")};
			var uniqueKey=application.entitytype[schemeData.entityTypeID].uniqueKey;
			var entityTable=application.entitytype[schemeData.entityTypeID].tableName;
		</cfscript>
		
		<cfquery name = "returnStruct.entityLevelReport" result="returnStruct.entityLevelResult" dataSource = "#application.siteDataSource#" > 
			<!--- duplicates uniqueKey so it can be read either way --->
			Declare @scoreReport Table
			(
				#uniqueKey# int ,
				entityID int,
				score int
			)
			
			insert into @scoreReport (#uniqueKey#,entityID,score)
			select #uniqueKey# ,#uniqueKey# as entityID , SUM(score) as score from 
			vActivityScore
			where scoreRuleGroupingID= <cf_queryParam value="#SchemeID#" CFSQLType = "CF_SQL_INTEGER"> 
			and organisationID=  <cf_queryParam value="#request.relayCurrentUser.organisationID#" CFSQLType = "CF_SQL_INTEGER">
			and activityTime > <cf_queryParam value="#startTime_exclusive#" CFSQLType = "CF_SQL_TIMESTAMP">
			and activityTime <= <cf_queryParam value="#endTime_inclusive#" CFSQLType = "CF_SQL_TIMESTAMP">
			group by #uniqueKey#
		
			<!--- Fill in any entities with score zero --->
		
			insert into @scoreReport (#uniqueKey#,entityID,score)
			select #uniqueKey# ,#uniqueKey# as entityID , 0
			from #entityTable#
			where organisationID = <cf_queryParam value="#organisationID#" CFSQLType = "CF_SQL_INTEGER">
			and active = 1
			and #uniqueKey# not in (select entityID from @scoreReport)
			
			select #uniqueKey#, entityID, score from @scoreReport 
		
		</cfquery>
		
		<cfreturn returnStruct>
		
	</cffunction>

	<cfscript>
	
		public Query function getActiveSchemesForValidValues() validValues="true"{
			var translationSnippets= application.com.relayTranslations.getPhrasePointerQuerySnippets(entityType="scoreRuleGrouping", phraseTextID="title");
			
			sql="select #translationSnippets.select# as title,scoreRuleGroupingID "&
				"from scoreRuleGrouping "&
				"#preserveSingleQuotes(translationSnippets.join)# " &
				"where active=1";
				
			var schemeQuery=new Query();
			schemeQuery.setDatasource(application.sitedatasource);
			
			return schemeQuery.execute(sql=sql).getResult();
			
		}
		public Query function getScoreDisplayOptionsForValidValues() validValues="true"{
			//as far as I'm aware the validValues on relayTags only support a seperate phase and data value if a query is used.
			//So this is created a fake query
			
			var displayOptions=ArrayNew(1);
			displayOptions[1]={display="phr_AllTime", data="AllTime"};
			displayOptions[2]={display="phr_CurrentPeriod", data="Current"};
			displayOptions[3]={display="phr_PreviousPeriod", data="Previous"};
			
			return application.com.structureFunctions.arrayOfStructuresToQuery(theArray=displayOptions,addArrayIndex=false);
			
		}
	
		/**
		 * Returns a score report (which can be queried for specific entityIDs).
		 * 
		 * The entityTypeIDForReporting defines what entity ID is to be expected by the report, will be person, organisation or location.
		 * 
		 * If the collectingEntityTypeID is equal to the level on which the score is collected the score in that score is returned
		 * 
		 * If the collectingEntityTypeID is higher than the collecting level then the score is "rolled up" from all the children
		 * 
		 * if the collectingEntityTypeID is lower than the collecting level then an exception is raised
		 * 
		 * if start date and enddate are ommitted the report is for all time (up to now)
		 * 
		 **/
		public score.reporting.ScoreReport function getScoreReportForGrouping(required numeric scoreRuleGroupingID, required numeric entityTypeIDForReporting, startDate_exclusive, endDate_inclusive){
			arguments.includeRollup=true;
			return new score.reporting.ScoreReportForAnything(argumentCollection=arguments);
		}
	
		/**
		 * Returns a score report (which can be queried for specific entityIDs).
		 * 
		 * includeRollUp specifies if (for example) an organisations score includes all of its location and person childrens scores
		 * 
		 * if start date and enddate are ommitted the report is for all time  (up to now)
		 * 
		 **/
		public score.reporting.ScoreReport function getScoreReportForAllGroupings(required numeric entityTypeIDForReporting, required boolean includeRollUp, startDate_exclusive, endDate_inclusive){
			return new score.reporting.ScoreReportForAnything(argumentCollection=arguments);
		}
	
		public numeric function getNumberOfActiveGroupings(){
			var versionQuery=new Query();
			
			versionQuery.setDatasource(application.sitedatasource);
			
			var queryReturn=versionQuery.execute(
				sql='select count(*) as numberActive from ScoreRuleGrouping where active=1'
			);
			
			return queryReturn.getResult().numberActive;
		}
	
		/**
		 * Returns a struct containing the data for the version of the rule that is current (even if it is inactive)
		 * A rule version, once set, is never changed, only a new version created
		 **/
		public struct function getMostRecentVersionOfRule(required numeric ruleID){
			var versionQuery=new Query();
			versionQuery.setDatasource(application.sitedatasource);
			versionQuery.addParam(name="ruleID",value=ruleID,cfsqltype="CF_SQL_INTEGER"); 
			var queryReturn=versionQuery.execute(
				sql='select scoreValue, scoreCap,nonStandardMethod,nonStandardParameters,filterDefinition,activityTypeID,active from scoreRuleVersion where scoreRuleID=:ruleID and validTo is null'
			);
			var metaData=queryReturn.getPrefix();
			var result=queryReturn.getResult();
			
			if (metaData.recordCount!=1){
				throw(message="No active version of score rule " & ruleID);
			}else{
				var versionStruct= application.com.structureFunctions.queryRowToStruct(result);
				
				return convertRawDatabaseRuleDefinitionToRuleDefinition(versionStruct);

			}
			
		}
		
		public boolean function activityGroupingHasActiveRules(required numeric scoreRuleGroupingID){
			
			var activeRulesQuery=new Query();
			activeRulesQuery.setDatasource(application.sitedatasource);
			activeRulesQuery.addParam(name="scoreRuleGroupingID",value=arguments.scoreRuleGroupingID,cfsqltype="CF_SQL_INTEGER");
			var queryReturn=activeRulesQuery.execute(sql=
				"select count(*) as activeRules from scoreRuleVersion " &
				"join scoreRule on scoreRuleVersion.scoreRuleID=scoreRule.scoreRuleID " &
				"where scoreRule.scoreRuleGroupingID = :scoreRuleGroupingID and scoreRuleVersion.validTo is null " &
				"and scoreRuleVersion.active=1 "
			);
			
			return queryReturn.getResult().activeRules>0;
			
		}
		
		/**
		 * Creates a new rule and the first version of it. Returns its ruleID
		 **/
		public numeric function createNewRule(required numeric scoreRuleGroupingID, required struct ruleDetails){
			
			var queryResponse=application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID.scoreRule, entityDetails={scoreRuleGroupingID=scoreRuleGroupingID}, insertIfExists="true");
		
			if (queryResponse.isOk){		
				saveNewVersionOfRule(queryResponse.entityID,ruleDetails);
				return queryResponse.entityID;
			}else{
				var message=structKeyExists(queryResponse,"ERRORCODE")?queryResponse.ERRORCODE:"No error code";
				var errorID=structKeyExists(queryResponse,"errorID")?queryResponse.errorID:"No Error ID";
				throw(message="Error while creating new rule, failed with error code [#message#] and errorID [#errorID#] ");
			}

		}
		
		/**
		 * Inactivates the old version of the rule (if it exists) and creates a new version with the updated details
		 **/
		public void function saveNewVersionOfRule(required numeric scoreRuleID, required struct ruleDetails){
			
			if (ruleDetails.SCORECALCULATIONMETHOD EQ "default"){
				ruleDetails.nonStandardMethod="";
			}else{
				ruleDetails.nonStandardMethod=ruleDetails.SCORECALCULATIONMETHOD;
			}
		
			//create new versions
			var changeTime=now();
			var versionDetails=StructCopy(ruleDetails);
			versionDetails.scoreRuleID=arguments.scoreRuleID;
			versionDetails.validFrom=changeTime;

			var queryResponse=application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID.scoreRuleVersion, entityDetails=versionDetails, insertIfExists="true");
			if (queryResponse.isOk){
				var newVersionID= queryResponse.entityID;
			}else{
				var message=structKeyExists(queryResponse,"ERRORCODE")?queryResponse.ERRORCODE:"No error code";
				var errorID=structKeyExists(queryResponse,"errorID")?queryResponse.errorID:"No Error ID";
				throw(message="Error while saving New Version Of Rule, failed with error code [#message#] and errorID [#errorID#] ");
			}
			
			//deactivate old version (if it exists)
			
			var versionQuery=new Query();
			versionQuery.setDatasource(application.sitedatasource);
			versionQuery.addParam(name="ruleID",value=scoreRuleID,cfsqltype="CF_SQL_INTEGER"); 
			versionQuery.addParam(name="newRuleVersionID",value=newVersionID,cfsqltype="CF_SQL_INTEGER"); 
			versionQuery.addParam(name="changeTime",value=changeTime,cfsqltype="cf_sql_timestamp"); 
			var queryReturn=versionQuery.execute(
				sql='update scoreRuleVersion set validTo=:changeTime where scoreRuleId=:ruleID and validTo is null and scoreRuleVersionID !=:newRuleVersionID' 
			);
		}

		public array function getRequiredParametersForActivityScoringMethod(required string scoringMethodName){
			var functionMetadata=GetMetaData(application.com.activityScoreCalculationFunctions[scoringMethodName]);
			var parameters=functionMetadata.PARAMETERS;
			var requiredParameterData=ArrayNew(1);
			for(var i=1;i<=arrayLen(parameters);i++){
				if (parameters[i].name != "entityId" and parameters[i].name != "activityId" and parameters[i].name != "entityTable"){
					var phrase=structKeyExists(parameters[i],"phrase")?parameters[i].phrase:parameters[i].name;
					arrayAppend(requiredParameterData, {parameterName=parameters[i].name, parameterPhrase=phrase, parameterType=parameters[i].type, parameterRequired=parameters[i].required});
				}
			}

			return requiredParameterData;
		}

		/**
		 * gets the entityTypeID of the entity associated with an activity (e.g. a lead approval activity would return the EntityTypeID of lead
		 **/
		public numeric function getAssociatedEntityTypeIDForActivity(required numeric activityTextID){
			var entityTypeIDQuery=new Query();
			entityTypeIDQuery.setDatasource(application.siteDatasource);
			entityTypeIDQuery.setcachedwithin(CreateTimeSpan(0, 1, 0, 0)); //realistically the result of this query will never change, but give it a 1 hour timeout just in case
			entityTypeIDQuery.addParam(name="activityTextID",value=activityTextID,cfsqltype="cf_sql_varchar");
			var result=entityTypeIDQuery.execute(sql="SELECT entityTypeID from ActivityType where activityTypeID= :activityTextID").getResult(); 
			
			return result.entityTypeID;
			
		}
		
		/**
		 * Will return either a person, location or organisation EntityTypeID. Whichever type of entity is collecting the score
		 **/
		public numeric function getEntityTypeIDOfEntityCollectingScore(required numeric scoreRuleVersionID){
			var entityTypeIDQuery=new Query();
			entityTypeIDQuery.setDatasource(application.siteDatasource);
			entityTypeIDQuery.addParam(name="scoreRuleVersionID",value=scoreRuleVersionID,cfsqltype="cf_sql_varchar");
			
			var result=entityTypeIDQuery.execute(sql="select entityTypeID from scoreRuleGrouping " &
			"join scoreRule on scoreRuleGrouping.scoreRuleGroupingID=scoreRule.scoreRuleGroupingID " &
			"join scoreRuleVersion on scoreRule.scoreRuleID=scoreRuleVersion.scoreRuleID " &
			"where scoreRuleVersionID=:scoreRuleVersionID ");
			
			return result.getResult().entityTypeID;
		}
		
		/**
		 * This method is provided to use instead of relayEntity.getEntity because these methods are used 
		 * inside housekeeping tasks where there is no relaycurrent user.
		 **/		
		private struct function getScoreRuleGroupings(){
			var returnStruct={recordCount=0,recordSet=""};
			var scoreRuleGroupingQuery=new Query();
			scoreRuleGroupingQuery.setDatasource(application.siteDatasource);
			
			var queryResult=scoreRuleGroupingQuery.execute(sql="select scoreRuleGroupingID,entityTypeID, cappingPeriodTypeTextID, cappingPeriodResetMonthOfYear, cappingPeriodResetDayOfMonth  from scoreRuleGrouping");
			
			returnStruct.recordCount=queryResult.getPrefix().recordCount;
			returnStruct.recordSet=queryResult.getResult();
			
			return returnStruct;
		}
		
		
		/**
		 * This method is provided to use instead of relayEntity.getEntity because these methods are used 
		 * inside housekeeping tasks where there is no relaycurrent user.
		 **/
		public struct function getScoreRuleGrouping(required numeric groupingID){
			var returnStruct={recordCount=0,recordSet=""};
			var scoreRuleGroupingQuery=new Query();
			scoreRuleGroupingQuery.setDatasource(application.siteDatasource);
			scoreRuleGroupingQuery.addParam(name="groupingID",value=groupingID,cfsqltype="cf_sql_integer");
			scoreRuleGroupingQuery.setCachedWithin(CreateTimeSpan(0, 0, 0, 2));
			var queryResult=scoreRuleGroupingQuery.execute(sql="select 'phr_title_scoreRuleGrouping_' + cast(scoreRuleGroupingID as nVarChar(30)) as title_translation, scoreRuleGroupingID,entityTypeID, cappingPeriodTypeTextID, cappingPeriodResetMonthOfYear, cappingPeriodResetDayOfMonth  from scoreRuleGrouping where scoreRuleGroupingID = :groupingID");
			
			returnStruct.recordCount=queryResult.getPrefix().recordCount;
			returnStruct.recordSet=queryResult.getResult();
			
			return returnStruct; 
		}
		
		public struct function awardNewScores(){
			var reportStruct={overallIsOk="true", groupingByGroupingReport=[]};
			
			var startTime=getLastActivityScoreAwardTime();
			
			reportStruct.startTime=startTime;
			
			//we want to update the scores up till the least updated activity (to avoid any 
			//possibility of an activity slipping through during the update)
			
			var endTimeQuery=new Query();
			endTimeQuery.setDatasource(application.siteDatasource);
			var queryResult=endTimeQuery.execute(sql="select MIN(lastRefreshed) as oldestRefresh from activityType").getResult();
			var endTime=queryResult.oldestRefresh;
			
			reportStruct.endTime=endTime;
			
			var scoreRuleGroupings=getScoreRuleGroupings();
			
			for(var i=1;i<=scoreRuleGroupings.recordCount;i++){
				var groupingReport=awardNewScores_forAGrouping(scoreRuleGroupings.recordSet["scoreRuleGroupingID"][i],startTime,endTime);
				if (!groupingReport.overallIsOk){
					reportStruct.overallIsOk=false;
				}
				ArrayAppend(reportStruct.groupingByGroupingReport,groupingReport);
				
			}
			
			//log any problems
			
			if (!reportStruct.overallIsOk){
				application.com.errorHandler.recordRelayError_Warning(Severity="error", Type="ActivityScoringError", message="ActivityScoringError", WarningStructure=reportStruct);
			}
			
			return reportStruct;
		}
		
		/**
		 * Returns the start_exclusive=start_exclusive,end_inclusive of a capping period relative to relativeDate (now by default) relative by relativePeriod (this period by default)
		 * 
		
		public struct function getRelativeCappingPeriod(required string scoreRuleGroupingID, relativeDate=now(), relativePeriod=0){
			
			var cappingPeriodTypeTextID=
			var dateToConsider="";
			switch(cappingPeriodTypeTextID){
				case "quarterly":
					testDate=DateAdd("m",3*relativePeriod,relativeDate);
					break;
				case "Annually":
					testDate=DateAdd("yyyy",relativePeriod,relativeDate);
					break;
				case "Monthly":
					testDate=DateAdd("m",relativePeriod,relativeDate);
					break;
				default:
					throw(message="Capping period #reportingRegionType# not supported");
			}
			
			return 
		}
		 **/
		public struct function awardNewScores_forAGrouping(required numeric scoreRuleGroupingID,required startTime, required endTime){
			var reportStruct={overallIsOk="true", periodByPeriodReport=[], groupingID=scoreRuleGroupingID};

			//relevantCappedPeriods are non neccissarily WHOLE capping periods, but they span at most one
			relevantCappedPeriods=divideIntoReportingRegions(scoreRuleGroupingID,startTime,endTime);
					

			for(var i=1;i<=arrayLen(relevantCappedPeriods);i++){
				var period=relevantCappedPeriods[i];
				
				var periodReport=awardNewScores_forOneCappingRegion(scoreRuleGroupingID,period.start_exclusive,period.end_inclusive);

				if (!periodReport.overallIsOk){
					reportStruct.overallIsOk=false;
				}
				arrayAppend(reportStruct.periodByPeriodReport,periodReport);
			}
			return reportStruct;
		}
		
		
		/**
		 * Returns as a query the capping period types and the phrases that give the friendly name
		 **/
		public query function getCappingPeriodType(){
			var periodTypes=ArrayNew(1);
			
			periodTypes[1]={type="allTime", phrase="phr_score_period_allTime", needsMonthOfYear=false, needsDayOfMonth=false};
			periodTypes[2]={type="annually", phrase="phr_score_period_annually", needsMonthOfYear=true, needsDayOfMonth=true};
			periodTypes[3]={type="quarterly", phrase="phr_score_period_quarterly", needsMonthOfYear=false, needsDayOfMonth=false};
			periodTypes[4]={type="monthly", phrase="phr_score_period_monthly", needsMonthOfYear=false, needsDayOfMonth=true};
			
			return application.com.structureFunctions.arrayOfStructuresToQuery(periodTypes);
		}
		
		
		
		/**
		 * Runs the score awarding from the last known score up to the current time for rules in the specified grouping, awarding scores for any activities that have occured in the 
		 * mean time.
		 * 
		 * Period must not span multiple capping periods
		 **/
		private struct function awardNewScores_forOneCappingRegion(required numeric scoreRuleGroupingID, required startTime, required endTime ){
			var reportStruct={overallIsOk="true", ruleByRuleReport=[], period="#startTime# - #endTime#"};
			
			var cappingPeriod=getTimeCappingPeriodContainingDate(scoreRuleGroupingID,dateAdd("s",1,startTime));
			
			var allRulesToBeApplied=getAllActiveScoreRulesActiveInAPeriod(scoreRuleGroupingID,startTime,endTime);
			
			for(var index=1;index<=arrayLen(allRulesToBeApplied);index++){

				
				var ruleApplyReport=runScoreRule(allRulesToBeApplied[index].scoreRuleVersionID, allRulesToBeApplied[index],startTime,endTime,cappingPeriod.start_exclusive,cappingPeriod.end_inclusive);
				if (!ruleApplyReport.isOk){
					reportStruct.overallIsOk=false;
				}

				arrayAppend(reportStruct.ruleByRuleReport,ruleApplyReport);
			}
			
			return reportStruct;
		}
		

		private any function getLastActivityScoreAwardTime(){
			var lastScoreQuery=new Query();
			lastScoreQuery.setDatasource(application.siteDatasource);
			var queryResult=lastScoreQuery.execute(sql="select MAX(created) as lastScore from score").getResult();
			
			var lastUpdated=queryResult.lastScore;
			
			if (lastUpdated==""){
				//we're looking from the beginning so its the earliest activity we care about 
				//(not the beginning of time as we'll be breaking the period up into reporting periods)
				
				queryResult=lastScoreQuery.execute(sql="select min(activityTime) as lastScore from Activity").getResult();
				
				var lastUpdated=queryResult.lastScore;
				
				if (lastUpdated==""){
					lastUpdated=now(); ///i.e. do nothing
				}
			}
			
			return lastUpdated;
		}
		
		/**
		 * Returns all the score rules that were active during a period and part of the specified grouping. It does not garantee that they were valid for the entire 
		 * period and:
		 * IT MAY RETURN MULTIPLE VERSIONS OF THE SAME RULE if multiple versions of the same rule existed in the same period.
		 **/
		private Array function getAllActiveScoreRulesActiveInAPeriod(required numeric scoreRuleGroupingID, required startTime, required endTime ){
			var lastScoreQuery=new Query();
			lastScoreQuery.setDatasource(application.siteDatasource);
			lastScoreQuery.addParam(name="startTime",value=startTime,cfsqltype="cf_sql_timestamp"); 
			lastScoreQuery.addParam(name="endTime",value=endTime,cfsqltype="cf_sql_timestamp"); 
			lastScoreQuery.addParam(name="scoreRuleGroupingID",value=scoreRuleGroupingID,cfsqltype="cf_sql_integer"); 
			queryResult=lastScoreQuery.execute(
				sql='select scoreRuleVersionID,scoreRule.scoreRuleID, scoreValue,scoreCap,nonStandardMethod,nonStandardParameters,filterDefinition,validFrom,validTo, activityTypeID ' &
					'from scoreRuleVersion ' &
					'join scoreRule on scoreRule.scoreRuleID=scoreRuleVersion.scoreRuleID ' &
					'where  '&
					'active=1 and ('&
						'((:startTime <= validTo)  and  (:endTime >= validFrom)) ' & //i.e. an overlap exists in the ranges'
						'OR ((validTo is null) and (:endTime >= validFrom)) ' & //special case for current rules (by special case I mean most will be like this)'
					') and ' &
					'scoreRule.scoreRuleGroupingID = :scoreRuleGroupingID ' &
					'order by validFrom' //so where there are multiple versions of the same rule the oldest is applied first, important for caps
				);
			
			
			var ruleDefinitions_raw=application.com.structureFunctions.QueryToArrayOfStructures(queryResult.getResult());
			
			var ruleDefinitions=[];
			for(var i=1;i<=arrayLen(ruleDefinitions_raw);i++){
				ruleDefinitions[i]=convertRawDatabaseRuleDefinitionToRuleDefinition(ruleDefinitions_raw[i]);
			}
			
			return ruleDefinitions;

		}
		
		private array function filterFunctionsForActivityType(required Array functionMetaData,required numeric activityTypeID){
			var filteredFunctionMetaData=ArrayNew(1);
			var activityTypeTextID=getActivityTextIDForID(activityTypeID);
			
			for(var i=1;i<=arrayLen(functionMetaData);i++){
				var activityTypesList=(structkeyexists(functionMetaData[i],"activityTypes")?functionMetaData[i].activityTypes:"" );
				
				if (ListContainsNoCase(activityTypesList,activityTypeTextID)){
					ArrayAppend(filteredFunctionMetaData,functionMetaData[i]);
				}
				
			}
			
			return filteredFunctionMetaData;
		}
	
		private string function getActivityTextIDForID(required numeric activityTypeID){
			var activityTypeIDQuery=new Query();
			activityTypeIDQuery.setDatasource(application.siteDatasource);
			activityTypeIDQuery.setcachedwithin(CreateTimeSpan(0, 1, 0, 0)); //realistically the result of this query will never change, but give it a 1 hour timeout just in case
			activityTypeIDQuery.addParam(name="activityTypeID",value=activityTypeID,cfsqltype="cf_sql_integer");
			var queryResult=activityTypeIDQuery.execute(sql="select activityType from activityType where activityTypeID=:activityTypeID");
			
			if (queryResult.getPrefix().recordCount!=1){
				throw(message="No active type of id " & activityTypeID);
			}else{
				var result=queryResult.getResult();
				return result.activityType;
			}
			
		}
	
		private struct function convertRawDatabaseRuleDefinitionToRuleDefinition(required struct rawDefinition){
			var versionStruct =structCopy(rawDefinition);
			if (versionStruct.nonStandardMethod EQ ""){
				versionStruct.SCORECALCULATIONMETHOD="default";
			}else{
				versionStruct.SCORECALCULATIONMETHOD=versionStruct.nonStandardMethod;
			}
			structDelete(versionStruct,"nonStandardMethod");
			return versionStruct;
		}
		
		/**
		 * Breaks the period into regions that are within the same reporting period
		 **/
		private Array function divideIntoReportingRegions(numeric scoreRuleGroupingID, required start_exclusive, required end_inclusive){
			

			var scoreRuleGroupingData=getScoreRuleGrouping(scoreRuleGroupingID).recordSet;
			
			var reportingRegionType=scoreRuleGroupingData.cappingPeriodTypeTextID;
			
			
			var regions=ArrayNew(1);
			
			
			switch(reportingRegionType){
				case "allTime":
					return [{start_exclusive=start_exclusive,end_inclusive=end_inclusive}];
					break;
				case "quarterly":
				case "Annually":
				case "Monthly":
					var testDate=start_exclusive;
					

					
					while(DateCompare(testDate,end_inclusive)<0){
						
						var cappingRegion=getTimeCappingPeriodContainingDate(scoreRuleGroupingID,testDate);
						

						
						//clamp the region to the permitted region
						cappingRegion.start_exclusive=application.com.dateFunctions.clampDate(cappingRegion.start_exclusive,start_exclusive,end_inclusive);
						cappinRegion.end_inclusive=application.com.dateFunctions.clampDate(cappingRegion.end_inclusive,start_exclusive,end_inclusive);


						if (DateCompare(cappingRegion.start_exclusive,cappingRegion.end_inclusive) NEQ 0){

							//i.e. if the clamping hasn't collapsed the region entirely
							arrayAppend(regions,cappingRegion);

						}
						
						switch(reportingRegionType){
							case "quarterly":
								testDate=DateAdd("s",1,DateAdd("m", +3, cappingRegion.start_exclusive));
								break;
							case "Annually":
								testDate=DateAdd("s",1,DateAdd("yyyy", +1, cappingRegion.start_exclusive));
								break;
							case "Monthly":
								testDate=DateAdd("s",1,DateAdd("m", +1, cappingRegion.start_exclusive));
								break;
							default:
								throw(message="Capping period #reportingRegionType# not supported");
						}
					}
					
					
					break;	
				default:
					throw(message="Capping period #reportingRegionType# not supported");	
			}
			
			return regions;
		}
	
		
	
		public struct function getTimeCappingPeriodContainingDate(numeric scoreRuleGroupingID,required date){
			var scoreRuleGroupingData=getScoreRuleGrouping(scoreRuleGroupingID).recordSet;
			
			
			var reportingRegionType=scoreRuleGroupingData.cappingPeriodTypeTextID;
			switch(reportingRegionType){
				case "allTime":
					return  {start_exclusive=CreateDateTime(1970,1,1,0,0,0),end_inclusive=now()};
				
				case "Annually":
					var resetMonth=scoreRuleGroupingData.cappingPeriodResetMonthOfYear;
					var resetDay=scoreRuleGroupingData.cappingPeriodResetDayOfMonth;
					
					var lastYear=CreateDateTime(year(date)-1, resetMonth, resetDay, 0, 0, 0);
					var thisYear=CreateDateTime(year(date), resetMonth, resetDay, 0, 0, 0);
					var nextYear=CreateDateTime(year(date)+1, resetMonth, resetDay, 0, 0, 0);
					
					if (int(lastYear)<int(date) && int(date)<int(thisYear)){
						return {start_exclusive=lastYear,end_inclusive=thisYear};
					}else{
						return {start_exclusive=thisYear,end_inclusive=nextYear};
					}
					
				case "quarterly":
				
					var quarterOfYear=quarter(date); //returns a quarter between 1 and 4
					
					var quarterStart=createDate(year(date),(quarterOfYear-1)*3+1,1);
					var quarterEnd=DateAdd("m",3,date); //because end date is inclusive and start is exclusive it is correct that an end is equal to a start
					
					return {start_exclusive=quarterStart,end_inclusive=quarterEnd};
					
					break;
				case "Monthly":
					var resetDay=scoreRuleGroupingData.cappingPeriodResetDayOfMonth;
					
					var thisMonth=CreateDateTime(year(date), month(date), resetDay, 0, 0, 0);
					var nextMonth=DateAdd("m", +1, thisMonth);
					var lastMonth=DateAdd("m", -1, thisMonth);
					
					
					if (int(lastMonth)<int(date) && int(date)<int(thisMonth)){
						return {start_exclusive=lastMonth,end_inclusive=thisMonth};
					}else{
						return {start_exclusive=thisMonth,end_inclusive=nextMonth};
					}
					
					break;
				default:
					throw(message="Capping period #reportingRegionType# not supported");	
			}
		}
		
		
	
	</cfscript>

</cfcomponent>
