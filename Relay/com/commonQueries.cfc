<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2008/06/30     SSS      Added Nvarchar Support to advanced search
2009/06/04	   GCC      Added with(nolock) to getUserAccounts last contact date
2011/09/07 	   PPB		added getCountryIdFromDescription()
2012/03/05 	   PPB 		P-REL115 API added getOrganisationType
2012/05/29	   PPB 		P-MIC001 getOrgDetailsForEmailMerge
2013-01-21	   PPB 		Case 432524 return location details from getOrgDetailsForEmailMerge
2013-05-07	   PPB 		Case 434976 return crmPerId from getAccManager
2014-07-17	   REH 		Case 440898 return VAT number from getFullPersonDetails and getOrgDetailsForEmailMerge

--->

<cfcomponent displayname="Common Relay Queries" hint="Contains a number of commonly used Relay Queries">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns accManager details for THIS.personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getMyAccManagerDetails" hint="Returns accManager details for THIS.personID.">
		<cfscript>
			var getPersonsAccManagerDetails = "";
		</cfscript>
		<CFQUERY NAME="getPersonsAccManagerDetails" DATASOURCE="#THIS.dataSource#">
			select p.firstname + ' '+ p.lastname as Accountmanager, p.officephone, i.entityid
			from integerflagdata i
			join person p on p.personid = i.data
			join flag f on f.flagid = i.flagid
			join person p1 on p1.organisationid = i.entityid
			where p1.personid =  <cf_queryparam value="#THIS.personID#" CFSQLTYPE="CF_SQL_INTEGER" >  and f.flagtextid = 'AccManager'
		</CFQUERY>
		<CFSET THIS.AccountManager = AccManager.AccountManager>
		<CFSET THIS.ACofficephone = AccManager.officephone>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns accManager details for a person as a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getPersonsAccManagerQuery" access="public" returntype="query" hint="Returns an accManagers details as a query">
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="additionalColumns" type="string" default="">
		<cfargument name="flagId" type="string" default="AccManager">

		<cfscript>
			var getPersonsAccManagerQuery="";
		</cfscript>

		<cfquery name="getPersonsAccManagerQuery" datasource="#application.siteDataSource#">
			select p.firstname + ' '+ p.lastname as Account_Manager, p.officephone as office_phone,
			p.personid as account_Manager_ID,
			i.entityid
			<cfloop index="col" list="#additionalColumns#">,p.#col#</cfloop>
			from integerflagdata i
			join person p on p.personid = i.data
			join flag f on f.flagid = i.flagid
			join person p1 on p1.organisationid = i.entityid
			where p1.personid=#arguments.personID# and f.flagtextid =  <cf_queryparam value="#arguments.flagId#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn getPersonsAccManagerQuery>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns accManager details for a PL or O as a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getAccManager" access="public" returntype="query" hint="Returns an accManagers details as a query">
		<cfargument name="entityID" type="numeric" required="yes">
		<cfargument name="additionalColumns" type="string" default="">
		<cfargument name="entityType" type="string" default="organisation">

		<cfset var constantAdditionalColumns = "firstname,lastname,email,personID,crmPerId">	 <!--- 2013-05-07 PPB Case 434976 --->
		<cfset var flagId = "AccManager">

		<cfif additionalColumns neq "">
			<cfset constantAdditionalColumns = "#additionalColumns#,#constantAdditionalColumns#">
		</cfif>

		<cfif arguments.entityType eq "location">
			<cfquery name="getOrgID" datasource="#application.siteDataSource#">
				select organisationid from location where locationid=#arguments.entityID#
			</cfquery>
			<cfif getOrgID.recordcount eq 1>
				<cfset arguments.entityID = getOrgID.organisationid>
				<cfset arguments.entityType = "organisation">
			</cfif>
		</cfif>

		<cfif arguments.entityType eq "organisation">
			<cfset getAccountManagerDetails = application.com.flag.getFlagData(flagId=flagId,entityID=arguments.entityID,additionalColumns=constantAdditionalColumns)>
		<cfelseif arguments.entityType eq "person">
			<cfset getAccountManagerDetails = getPersonsAccManagerQuery(personID=arguments.entityID,additionalColumns=constantAdditionalColumns)>
		</cfif>

		<cfreturn getAccountManagerDetails>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the person details for for THIS.personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPersonDetails" hint="Returns various person details for THIS.personID.">

		<cfset var getPersonDetails ="">

		<CFQUERY name="getPersonDetails" datasource="#THIS.dataSource#">
			SELECT p.personid, p.firstname, p.lastname, p.firstname+' '+p.lastname as fullname, p.salutation,
				p.email, p.officephone as directLine, l.telephone as switchboard, o.organisationName,
				l.countryid, l.locationID, o.organisationID
			FROM Person p
			INNER JOIN Organisation o ON p.Organisationid = o.OrganisationID
			INNER JOIN Location l ON p.locationid = l.locationID
			WHERE personID =  <cf_queryparam value="#THIS.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<CFSET THIS.salutation=getPersonDetails.salutation>
		<CFSET THIS.firstname=getPersonDetails.firstname>
		<CFSET THIS.lastname=getPersonDetails.lastname>
		<CFSET THIS.fullname=getPersonDetails.fullname>
		<CFSET THIS.email=getPersonDetails.email>
		<CFSET THIS.directLine=getPersonDetails.directLine>
		<CFSET THIS.switchboard=getPersonDetails.switchboard>
		<CFSET THIS.organisationName=getPersonDetails.organisationName>
		<CFSET THIS.country=getPersonDetails.countryid>
		<CFSET THIS.person=getPersonDetails.personid>
		<CFSET THIS.personID=getPersonDetails.personid>
		<CFSET THIS.PersonLocID = getPersonDetails.locationID>
		<CFSET THIS.PersonOrgID = getPersonDetails.organisationID>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns query object with full person details for personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getFullPersonDetails" access="public" returntype="query" hint="Returns query object containing person details for personID.">
		<cfargument name="personID" type="numeric" required="yes">
		<cfscript>
			var getFullPersonDetails="";
		</cfscript>

		<CFQUERY name="getFullPersonDetails" datasource="#application.SiteDataSource#">
			SELECT p.PersonID, p.FirstName, p.LastName, p.FirstName + ' ' + p.LastName AS fullname, p.Salutation, p.Email, p.Password,
					  p.OfficePhone AS directLine, p.officephone, p.mobilephone, p.faxphone, p.jobdesc,
                      l.Telephone AS switchboard, o.OrganisationName, l.sitename, l.Fax, l.Address1, l.Address2, l.Address3, l.Address4, l.Address5,
					  l.PostalCode, l.CountryID, l.LocationID, o.OrganisationID, o.OrgURL, c.CountryDescription, o.vatNumber					<!--- 2014-07-17 REH Case 440898 return VAT number for an org --->
			FROM Person AS p INNER JOIN
                      organisation AS o ON p.OrganisationID = o.OrganisationID INNER JOIN
                      Location AS l ON p.LocationID = l.LocationID INNER JOIN
                      Country AS c ON l.CountryID = c.CountryID
			WHERE personID = #arguments.personID#
		</CFQUERY>

		<cfreturn getFullPersonDetails>

	</cffunction>

	<!---
	2012/05/29 PPB P-MIC001 this method is specifically to return OrgDetails for the case where there isn't a contactPersonID (on the opp)
	we want to ensure that the column names are the same as those used if there IS a contactPersonID as retreived by getFullPersonDetails above (and hence I don't use any other "Get Org Details" function)
	so that the same merge field can be used either way
	--->
	<cffunction name="getOrgDetailsForEmailMerge" access="public" returntype="query" hint="Returns query object containing organisation details">
		<cfargument name="organisationId" type="numeric" required="yes">

		<cfset var qryOrgDetails="">

		<!--- START 2013-01-21 PPB Case 432524 return location details for an org too --->
		<cfquery name="qryOrgDetails" datasource="#application.SiteDataSource#">
 			SELECT TOP 1 o.OrganisationName, o.OrganisationID, o.OrgURL, o.vatNumber, oc.CountryDescription, l.SiteName, l.address1, l.address2, l.address3, l.address4, l.address5, l.PostalCode, lc.CountryId, lc.CountryDescription AS LocationCountryDescription 			<!--- 2014-07-17 REH Case 440898 return VAT number for an org --->
			FROM organisation o
			INNER JOIN Country oc ON o.CountryID = oc.CountryID
			LEFT JOIN (location l INNER JOIN Country lc ON l.CountryID = lc.CountryID) ON l.organisationId = o.organisationId
			WHERE o.OrganisationID = <cf_queryparam value="#arguments.organisationId#" CFSQLTYPE="CF_SQL_INTEGER" >
			order by l.created
		</cfquery>
		<!--- END 2013-01-21 PPB Case 432524 --->


		<cfreturn qryOrgDetails>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the person details for for THIS.personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPersonID" hint="Returns personID from username">
		<cfargument name="username" required="yes">
		<CFQUERY name="getPersonIDForUsername" datasource="#application.SiteDataSource#">
			SELECT personID from person where username =  <cf_queryparam value="#arguments.username#" CFSQLTYPE="CF_SQL_VARCHAR" >  or
			(firstname =  <cf_queryparam value="#listfirst(arguments.username," ")#" CFSQLTYPE="CF_SQL_VARCHAR" >  and lastname =  <cf_queryparam value="#listlast(arguments.username," ")#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>
		<cfreturn getPersonIDForUsername.personID>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns user details for THIS.userID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUserDetails" hint="Returns user details for THIS.userID.">
		<CFQUERY NAME="getUserDetails" DATASOURCE="#THIS.dataSource#">
			select p.userName, ug.name as userGroupName, ug.userGroupID, p.personID
			from userGroup ug
			join person p on p.personid = ug.personid
			where ug.userGroupID =  <cf_queryparam value="#THIS.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<CFSET THIS.userName = getUserDetails.userName>
		<CFSET THIS.userGroupName = getUserDetails.userGroupName>
		<CFSET THIS.userGroupID = getUserDetails.userGroupID>
		<CFSET THIS.personID = getUserDetails.personID>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns user details for THIS.userID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getUserGroupDetails" access="public" returntype="query" hint="Returns user details for userGroupID.">
		<cfargument name="userGroupID" type="numeric" required="yes">

		<cfscript>
			var getUserGroupDetails = "";
		</cfscript>

		<CFQUERY NAME="getUserGroupDetails" DATASOURCE="#application.SiteDataSource#">
			select p.userName, ug.name as userGroupName, ug.userGroupID, p.personID
			from userGroup ug
			join person p on p.personid = ug.personid
			where ug.userGroupID=#arguments.userGroupID#
		</CFQUERY>

		<cfreturn getUserGroupDetails>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns user details for THIS.userID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getTextFlagValue" hint="Returns text flag value for the combination of THIS.FlagtextID, THIS.flagGroupTextID and THIS.entityID.">
		<CFQUERY NAME="getTextFlagValue" DATASOURCE="#THIS.dataSource#">
			SELECT tfd.Data
				FROM TextFlagData tfd
				INNER JOIN flag f ON f.flagID=tfd.flagid
				INNER JOIN flagGroup fg ON fg.flagGroupID = f.flagGroupID
				where f.flagtextid =  <cf_queryparam value="#THIS.FlagtextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				AND fg.flagGroupTextID =  <cf_queryparam value="#THIS.flagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				AND entityID =  <cf_queryparam value="#THIS.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<CFSET THIS.textFlagData = getTextFlagValue.Data>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOrgDistis" hint="Returns details of distis for a give organisationID via vDistiList">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="organisationid" type="numeric" default="1" required="yes">

		<cfscript>
			var getOrgDistis = "";
		</cfscript>

		<CFQUERY NAME="getOrgDistis" DATASOURCE="#dataSource#">
			SELECT DISTINCT d.organisationId, d.organisationName,
				d.CountryDescription, d.specificURL, d.telephone, d.Fax
			FROM dbo.vDistiList d
				INNER JOIN countryGroup cg on d.countryid = cg.countrymemberid
				INNER JOIN organisation o
				on o.countryid = cg.countrygroupid
			WHERE
				o.organisationid = #organisationid#

			union

			select 0,'Phr_Ext_OtherDisti','zzALL','','',''

			ORDER BY countryDescription, d.organisationName
		</CFQUERY>
		<cfreturn getOrgDistis>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns users accounts for THIS.userID see homepage/myAccounts for usage.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUserAccounts" hint="Returns THIS.userGroupID's accounts.">
		<cfparam name="THIS.FILTERSELECTVALUE" default="">
		<cfparam name="THIS.FilterSelect" default="#THIS.FILTERSELECTVALUE#">
		<cfparam name="THIS.FILTERSELECTVALUE2" default="">
		<cfparam name="THIS.FilterSelect2" default="#THIS.FILTERSELECTVALUE2#">
		<cfparam name="THIS.radioFilterValue" default="">
		<cfparam name="THIS.radioFilterName" default="#THIS.radioFilterValue#">
		<cfparam name="THIS.alphabeticalIndexColumn" default="">
		<cfparam name="THIS.alphabeticalIndex" default="">
		<cfparam name="THIS.frmAccountMngrID" type="numeric" default="0">
		<cfparam name="THIS.flagGroupTextID" default="mainOrgType">
		<cfparam name="THIS.sortOrder" default="o.OrganisationName, c.ISOCode, o.OrganisationID">
		<cfparam name="THIS.AccountMngrFlagTextID" default="AllocationSalesTechnical">

		<cfif not application.com.flag.doesFlagGroupExist(#THIS.AccountMngrFlagTextID#)>
			<CFSET THIS.qUserAccounts = "">
			<cfreturn THIS.qUserAccounts>
		</cfif>

		<CFQUERY NAME="getUserAccounts" DATASOURCE="#THIS.dataSource#">
			SELECT DISTINCT o.organisationName as account,
					o.OrganisationID,
					LEFT( UPPER( o.organisationName ), 1 ) as alphabeticalIndex,
					(select f.name from flag f
						INNER JOIN booleanFlagData bfd on f.flagID = bfd.flagID
						INNER JOIN flagGroup fg ON f.flagGroupID = fg.flagGroupID
						WHERE entityID = o.OrganisationID
						AND flagGroupTextID =  <cf_queryparam value="#THIS.flagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ) as account_type,
				   o.LastUpdated as last_updated,
				   c.ISOCode as country,
				   (select max(dateSent) from commDetail cd with(nolock)
					   INNER JOIN location l with(nolock) ON cd.LocationID = l.locationID
					   where l.OrganisationID = o.OrganisationID) as last_contact_date
			  FROM organisation o
			  	INNER JOIN Country c ON o.CountryID = c.CountryID
			  	inner join rights r on r.countryID = o.countryID and userGroupID=<cf_queryparam value="#this.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			  	<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
				  	inner join rightsGroup rg on r.UserGroupID = rg.UserGroupID AND r.Permission > 0 AND rg.PersonID = <cf_queryparam value="#this.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				  	inner join SecurityType s on s.SecurityTypeID = r.SecurityTypeID and s.shortname='RecordTask'
			  	</cfif>
			  	<CFIF THIS.frmAccountMngrID neq 0>
				  	<cfset accountManagerFlagGroupSructure = application.com.flag.getFlagGroupStructure(THIS.AccountMngrFlagTextID)>
				  	inner join #accountManagerFlagGroupSructure.FlagType.DataTableFullName# d on d.entityID = o.organisationID and
				  		<cfif accountManagerFlagGroupSructure.FlagType.hasDataColumn>data<cfelse>d.entityID</cfif> = #this.personID#
				  	inner join flag f on f.flagID = d.flagID and f.flagGroupID =  <cf_queryparam value="#accountManagerFlagGroupSructure.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			  	</cfif>
			  	<cfif THIS.FilterSelect eq "Account_type">
				  	INNER JOIN booleanFlagData bfd on bfd.entityID = o.organisationID
				  	inner join flag fa on bfd.flagID = fa.flagID and fa.flagGroupID =  <cf_queryparam value="#application.com.flag.getFlagGroupStructure(THIS.flagGroupTextID).flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			  	</cfif>
			WHERE 1=1
				<cfif THIS.FilterSelect eq "Account_type">
					and fa.name = <cf_queryparam value="#THIS.FilterSelectValues#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelseif THIS.FilterSelect neq "">
					AND #THIS.FilterSelect# =  <cf_queryparam value="#THIS.FilterSelectValues#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

				<!--- o.CountryID IN (select countryID
				from rights where usergroupID =  <cf_queryparam value="#this.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > ) --->
			<!--- <cfif THIS.FilterSelect neq "">
				<cfif THIS.FilterSelect eq "Account_type">
					AND (select f.name from flag f
						INNER JOIN booleanFlagData bfd on f.flagID = bfd.flagID
						INNER JOIN flagGroup fg ON f.flagGroupID = fg.flagGroupID
						WHERE entityID = o.OrganisationID
						AND flagGroupTextID =  <cf_queryparam value="#THIS.flagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ) =  <cf_queryparam value="#THIS.FilterSelectValues#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					AND #THIS.FilterSelect# =  <cf_queryparam value="#THIS.FilterSelectValues#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
			</cfif> --->
			<cfif THIS.alphabeticalIndex neq "">
				AND LEFT( UPPER( #THIS.alphabeticalIndexColumn# ), 1 ) =  <cf_queryparam value="#THIS.alphabeticalIndex#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>

			<!--- <cfif isDefined("countryScopeOrganisationRecords") and countryScopeOrganisationRecords eq 1> --->
			<!--- <cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
			  	and o.countryID in (
				 SELECT r.CountryID FROM Rights AS r, RightsGroup AS rg, Country as c
					WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e
						WHERE e.ShortName = 'RecordTask')
					AND r.UserGroupID = rg.UserGroupID
					AND c.countryid = r.countryid
					AND rg.PersonID =  <cf_queryparam value="#this.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND r.Permission > 0
				 )
			 </cfif> --->

			<!--- <CFIF THIS.frmAccountMngrID neq 0>
				<cfset accountManagerFlagGroupSructure = application.com.flag.getFlagGroupStructure(THIS.AccountMngrFlagTextID)>
	 		AND o.OrganisationID IN (select entityID from #accountManagerFlagGroupSructure.FlagType.DataTableFullName#
				where
				<cfif accountManagerFlagGroupSructure.FlagType.hasDataColumn>
					data
				<cfelse>
					entityID
				</cfif>
				in (#this.personID#)
				and FlagID IN (select f.flagID from flagGroup fg
					inner join flag f on fg.flagGroupID=f.flagGroupID
					where flagGroupTextID =  <cf_queryparam value="#THIS.AccountMngrFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
			</CFIF> --->
			ORDER BY #THIS.sortOrder#
		</CFQUERY>
		<CFSET THIS.qUserAccounts = getUserAccounts>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      getCurrentSystemUsers
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCurrentSystemUsers" hint="Returns all users for the countries the current user has rights to.">
		<cfscript>
			var getCurrentSystemUsers = "";
		</cfscript>

		<CFQUERY NAME="getCurrentSystemUsers" DATASOURCE="#application.sitedataSource#">
			SELECT firstname,lastname,p.personid, ISOCode, email,
			firstName + ' ' + lastname  + ' (' + ISOCode + ')' as displayValue
			FROM person p
				inner join location l on p.locationid = l.locationid
				inner join country c ON c.countryID = l.countryID
				inner join userGroup ug on ug.personid = p.personid
				where l.countryid in (select countryid from usergroupCountry where usergroupID = #request.relayCurrentUser.usergroupid#)
				--and p.organisationID in (select relayVarValue from relayVar where relayVarName = 'actionUserOrgIDs')
				and p.loginExpires > getDate()
				order by firstname
		</CFQUERY>
		<CFSET THIS.qCurrentSystemUsers = getCurrentSystemUsers>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      insertTrackSearch
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertTrackSearch" hint="Insert a record into the trackSearch table.">
		<cfargument name="searchType" default="D">
		<cfargument  name="searchString" required = "yes">
		<cfargument name="personid" required = "yes">  <!--- added 2006-01-25  --->
		<cfargument name="datasource" default="#application.sitedataSource#">

		<cfset var insertTrackSearch = "">
		<CFQUERY NAME="insertTrackSearch" DATASOURCE="#datasource#">
			INSERT INTO [trackSearch](
				[searchString],
				[searchType],
				[personID]
				)
<!--- 	WAB 2005-03-22 removed cfqueryparams - getting very strnage intermittent errors		VALUES (<cfqueryparam value="#left(searchString,254)#" cfsqltype="CF_SQL_VARCHAR" maxlength="255">,
					<cfqueryparam value="#searchType#" cfsqltype="CF_SQL_VARCHAR" maxlength="1">,
					<cfqueryparam value="#userGroupID#" cfsqltype="CF_SQL_INTEGER">
				)
 --->
<!--- IH 2013-07-19 Changed it back to cfqueryparam because cf_queryparam somehow doubled the apostrophes in search string --->
 			VALUES (<cfqueryparam value="#left(searchString,254)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#searchType#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >)

 		</CFQUERY>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      insertHelpSystemSearch     2006-03-03 SWJ added to fix problem with tracking help searches
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertHelpSystemSearch" hint="Insert a record into the trackSearch table.">
		<cfargument name="searchType" default="D">
		<cfargument  name="searchString" required = "yes">
		<cfargument name="userGroupID" type="numeric" required="yes">

		<cfset var insertHelpSystemSearch = "">
		<CFQUERY NAME="insertHelpSystemSearch" DATASOURCE="#application.siteDataSource#">
			INSERT INTO [trackSearch](
				[searchString],
				[searchType],
				[userGroupID]
				)
 			VALUES (<cfqueryparam value="#left(searchString,254)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#searchType#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >)
 		</CFQUERY>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      listSearches
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="listSearches" hint="List searches in the trackSearch table.">
		<cfargument name="dataSource" type="string" required="no" default="#application.siteDataSource#">
		<CFQUERY NAME="insertTrackSearch" DATASOURCE="#arguments.dataSource#">
			SELECT searchString
			FROM trackSearch
			group by searchString
			order by searchString
		</CFQUERY>
		<cfset THIS.qTrackSearch = insertTrackSearch>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      return p number for auto authentication
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getP" hint="Get user's p number">

		<cfargument name="personID" required="yes" type="numeric">
		<cfargument name="dataSource" type="string" required="no" default="#application.siteDataSource#">

		<cfscript>
			var getP = "";
		</cfscript>

		<CFQUERY NAME="getP" DATASOURCE="#arguments.dataSource#">
			select convert(varchar,(personid)) + '-' + convert(varchar,(personid & 1023)
			* (locationid & 1023)) as p from person
			WHERE personID = #arguments.personID#
		</CFQUERY>

		<cfreturn getP.p>

	</cffunction>



	<cffunction access="public" returntype="query" name="getLiveProjects" hint="Get deliverables for which the user has read permission">
		<cfargument name="personID" required="yes" type="numeric">
			<cfscript>
			  var getLiveProjects = "";
		    </cfscript>
			<CFQUERY NAME="getLiveProjects" dataSource="#application.siteDataSource#" >
			    SELECT Project.ProjectID, Project.ProjectName,
			   		SUM((RecordRights.Permission/1) % 2) AS allow_read,
					SUM((RecordRights.Permission/2) % 2) AS allow_edit,
			 		SUM((RecordRights.Permission/4) % 2) AS allow_add
				FROM RecordRightsGroup INNER JOIN (RecordRights INNER JOIN (projectStatus INNER JOIN Project
					ON projectStatus.ProjectStatusId = Project.ProjectStatusId)
					ON RecordRights.RecordID = Project.ProjectID)
					ON RecordRightsGroup.UserGroupID = RecordRights.UserGroupID
				WHERE projectStatus.IsLive=1
					AND RecordRightsGroup.PersonID=#arguments.personID#
					AND RecordRights.Entity='projects'
				GROUP BY Project.ProjectID, Project.ProjectName
				HAVING SUM((RecordRights.Permission/1) % 2) > 0
				ORDER BY Project.ProjectName
			</CFQUERY>
	 <cfreturn getLiveProjects>
	</CFFUNCTION>



	<CFFUNCTION name="StatusDesc" hint="This returns all action status types that are assigned">
		<CFARGUMENT name="statusID" type="numeric" required="true">
		    <cfscript>
			  var StatusDesc = "";
		    </cfscript>
		<CFQUERY NAME="getAssigned" DATASOURCE="#application.SiteDataSource#">
			SELECT actionStatusID, status from actionStatus WHERE Assigned = #statusID#
		</CFQUERY>
		<cfreturn StatusDesc>
	</CFFUNCTION>


	<CFFUNCTION name="getNotes" returntype="query" hint="This returns all notes associated with an action">
	  <CFARGUMENT name="actionID" type="numeric" required="true">
		    <cfscript>
			  var getNotes = "";
		    </cfscript>
		<CFQUERY NAME="getNotes" DATASOURCE="#application.SiteDataSource#">
			SELECT 	n.*,
				s.status as newStatusText,
				p.firstName+' '+p.lastName as newOwnerName,
				ug.name as initials
			FROM actionnote n
				INNER JOIN userGroup ug ON ug.userGroupID=n.creatorid
				INNER JOIN person p ON p.personid=n.newowner
				INNER JOIN actionStatus s ON s.actionStatusid = n.newstatus
			WHERE actionid=#arguments.actionid#
			ORDER BY n.created ASC
	  </CFQUERY>

	  <cfreturn getNotes>
  </cffunction>


	<CFFUNCTION name="getOwners" hint="all the entitled owners">
	  <CFARGUMENT name="actionOrgs" required="true" type="string">
	        <cfscript>
			  var getOwners = "";
		    </cfscript>
		<CFQUERY NAME="getOwners" dataSource="#application.siteDataSource#">
				SELECT p.PersonID as ownerPersonID, p.firstName+' '+p.lastName as fullname,
					o.OrganisationName
				FROM person p
					inner join organisation o on p.OrganisationID = o.OrganisationID
				where o.organisationID  in ( <cf_queryparam value="#arguments.actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and p.active = 1
				ORDER BY p.firstName
		</CFQUERY>

	 <cfreturn getOwners>
	</CFFUNCTION>


  <CFFUNCTION name="getCountries" hint="all countries">
  	<cfargument name="countrygroupid" required="no" type="string" default="">
	<cfargument name="restrictToUsersCountries" type="boolean" default="false">

    <cfscript>
	  var qGetCountries = "";
    </cfscript>

	<CFQUERY NAME="qGetCountries" dataSource="#application.siteDataSource#">
		SELECT DISTINCT Country.CountryID, Country.CountryDescription AS country
		FROM         CountryGroup RIGHT OUTER JOIN
                     Country ON CountryGroup.CountryMemberID = Country.CountryID
		WHERE 1=1
		<cfif arguments.restrictToUsersCountries>
			AND countryID  in ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="cf_sql_integer"  list="true"> )
		</cfif>
		<cfif len(arguments.countryGroupID)>
			AND     (Country.ISOCode IS NOT NULL)
			AND CountryGroup.CountryGroupID =  <cf_queryparam value="#arguments.countryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		ORDER BY Country.CountryDescription
	</CFQUERY>

	<cfreturn qGetCountries>
  </CFFUNCTION>

  <CFFUNCTION name="getStatus" hint=" get all status types">
	        <cfscript>
			  var getStatus = "";
		    </cfscript>

	  <CFQUERY NAME="getStatus" dataSource="#application.siteDataSource#">
		SELECT actionStatus.actionStatusID, actionStatus.Status, actionStatus.status_order
			FROM actionStatus
			ORDER BY actionStatus.status_order
	  </CFQUERY>

   <CFRETURN getStatus>
  </CFFUNCTION>


   <CFFUNCTION name="getActionTypes" hint=" get all action types">
            <cfscript>
			  var getActionTypes = "";
		    </cfscript>

		    <cfquery name="getActionTypes" dataSource="#application.siteDataSource#">
		        select actionTypeID, actionType from actionType order by actionType
	        </cfquery>

   <CFRETURN getActionTypes>
  </CFFUNCTION>



   <CFFUNCTION name="getActionDetails" hint=" get all action details per ID">
            <cfscript>
			  var getActionDetails = "";
		    </cfscript>
	    <CFQUERY NAME="getActionDetails" DATASOURCE="#application.SiteDataSource#">
	      SELECT a.*
	      FROM actions a
	      WHERE a.actionid =  <cf_queryparam value="#actionid#" CFSQLTYPE="CF_SQL_INTEGER" >
        </CFQUERY>

   <CFRETURN getActionDetails>
  </CFFUNCTION>

	<!--- 2005-04-27 - AJC - Get Country Details --->
	<cffunction name="getCountry" returntype="query" hint="Gets the country table details by countryID">

		<cfargument name="countryid" required="yes" type="numeric">

		<cfscript>
			var getCountryDetails = "";
		</cfscript>

		<CFQUERY NAME="getCountryDetails" DATASOURCE="#application.SiteDataSource#">
			SELECT *
			FROM Country
			WHERE countryid = #countryid#
		</CFQUERY>
		<!--- 17-10-2007 Gad: this code ensures system doesnt break for ANY SITE that doesnt
		have the FIELD TELEPHONEFORMAT in the COUNTRY TABLE, a new column i s created in the QRY and this is polulated and returned as blank--->
		<cfif getCountryDetails.recordcount GT 0>
			<cfif NOT isdefined('getCountryDetails.telephoneformat')>

				<cfset TmpTelArray = ArrayNew(1)>
				<cfloop from="1" to="#getCountryDetails.recordcount#" index="ann">
					<cfset TmpTelArray[ann] = "">
				</cfloop>
				<cfset addNewQryCol = queryAddColumn(getCountryDetails,'telephoneformat','varchar',TmpTelArray)>
			</cfif>

		</cfif>

  	<cfreturn getCountryDetails>
	</cffunction>

	<!--- 2011/09/07 PPB LID7679 added function to get id for 'All Countries'; rather than return full details from here, the result can easily be passed into getCountry if reqd --->
	<cffunction name="getCountryIdFromDescription" returntype="numeric" hint="Gets the country id sending in the country description">
		<cfargument name="countryDescription" required="yes" type="string">

		<cfscript>
			var getCountry = "";
		</cfscript>

		<cfquery name="getCountry" datasource="#application.SiteDataSource#">
			SELECT CountryId
			FROM Country
			WHERE CountryDescription =  <cf_queryparam value="#countryDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

  		<cfreturn getCountry.CountryId>
	</cffunction>


	<cffunction name="getLanguages" returntype="query">
		<cfscript>
			var qGetLanguages = "";
		</cfscript>

		<CFQUERY NAME="qGetLanguages" DATASOURCE="#application.SiteDataSource#">
			SELECT languageID,Language
			FROM Language
			ORDER BY Language
		</CFQUERY>
  	<cfreturn qGetLanguages>
	</cffunction>

	<!--- PPB 2010-04-08 --->
	<cffunction name="getCurrencies" returntype="query">
		<cfset var qGetCurrencies = "">

		<cfquery name="qGetCurrencies" datasource="#application.sitedatasource#">
			select
				currencyISOCode,currencyName,currencySign
			from
				currency
			order by
				currencyISOCode
		</cfquery>

		<cfreturn qGetCurrencies>
	</cffunction>

	<cffunction name="getISOCodeFromID">
		<cfargument name="entity" required="yes" type="variableName">
		<cfargument name="entityValue" required="yes" type="numeric">

		<cfscript>
			var qGetCode = "";
		</cfscript>

		<cfquery name="qGetCode" datasource="#application.sitedatasource#">
			select ISOCode from #arguments.entity# where #arguments.entity#ID = #arguments.entityValue#
		</cfquery>
		<cfreturn qGetCode.IsoCode>
	</cffunction>

	<cffunction name="getIDFromISOCode">
		<cfargument name="entity" required="yes" type="string">
		<cfargument name="entityValue" required="yes" type="string">

		<cfscript>
			var qGetID = "";
		</cfscript>

		<cfquery name="qGetID" datasource="#application.sitedatasource#">
			select #arguments.entity#ID as ID from #arguments.entity# where ISOCode =  <cf_queryparam value="#arguments.entityValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfreturn qGetID.ID>
	</cffunction>

  <cffunction name="makeRandomString" returnType="string" output="false">
		   <cfset var chars = "23456789ABCDEFGHJKMNPQRSTUVWXYZ"> <!--- deliberately not including 1,I,L,l,0,O,o abcdefghijkmnpqrstuvwxyzas it gets confusing and frustrating for the user --->
		   <cfset var length = randRange(4,5)>
		   <cfset var result = "">
		   <cfset var i = "">
		   <cfset var char = "">

		   <cfscript>
		   for(i=1; i <= length; i++) {
		      char = mid(chars, randRange(1, len(chars)),1);
		      result&=char;
		   }
		   </cfscript>

		   <cfreturn result>
		</cffunction>

	<!--- 2009/07/31 SSS captcha release functions start --->
	 <cffunction name="drawCaptcha" returntype="String" access="public">
		<cfargument name="captchaerror" required="yes" type="string" default="false">

		<cfset var resultHTML="">
		<!--- <cfinclude template="../../customtags/relayforms/defaultRelayFormFieldAttributes.cfm">
		<cfparam name="attributes.height" type="numeric" default="40">
		<cfparam name="attributes.width" type="numeric" default="300">

		<cfparam name="attributes.label" type="string" default="captcha">

		<cfparam name="attributes.DISABLED" type="string" default="">
		<cfparam name="attributes.ONVALIDATE" type="string" default=""> --->
		<cfparam name="attributes.FIELDNAME" type="string" default="frmcaptcha">
		<cfparam name="attributes.CURRENTVALUE" type="string" default="">


		<cfset captchaText = application.com.commonQueries.makeRandomString()>

		<cfsavecontent variable="resultHTML">
			<div class="form-group row">
				<div class="col-xs-12 col-sm-9 col-sm-offset-3">
					<cfimage action="captcha" width="300" height="40" text="#captchaText#">
				</div>
			</div>
			<cfif captchaerror>
				<div class="form-group row">
					<div class="col-xs-12">
						<cfoutput>#application.com.relayui.message(message="phr_ext_captchaerror",type="error")#</cfoutput>
					</div>
				</div>
			</cfif>
			<div class="form-group row">

				<div class="col-xs-12 col-sm-9 col-sm-offset-3">
					<input class="form-control" type="Text" name="frmcaptcha">
				</div>
				<cfset attributes.currentValue = hash(ucase(captchaText))>
				<cfset attributes.fieldname = attributes.fieldname &"_hash">
				<cfoutput><CF_INPUT type="hidden" name="frmcaptcha_hash" value="#attributes.currentValue#"></cfoutput>
			</div>
		</cfsavecontent>

		<cfreturn resultHTML>

	</cffunction>

	 <cffunction name="validatecaptcha" returntype="String">
	 	 <cfargument name="userResponse" required="yes" type="string">
		 <cfargument name="captchaResponse" required="yes" type="string" >

		 <cfset hashuserresponce = hash(ucase(arguments.userResponse))>
		 <cfif hashuserresponce EQ arguments.captchaResponse>
		 	<cfreturn true>
		 <cfelse>
		 	<cfreturn false>
		 </cfif>
	 </cffunction>
	<!--- 2009/07/31 SSS captcha release functions finish --->

	<!--- 2012/03/05 PPB P-REL115 API  --->
	<cffunction name="getOrganisationType" access="public" returntype="query" >
		<cfargument name="organisationTypeId"  type="any" required="false">			<!--- if not sent or it is an empty string, return all rows; if sent and non-numeric it is assumed that a TypeTextId has been passed in --->
		<cfargument name="columnList" type="string" required="false">

		<cfset var qryOrganisationType = "">

		<cfquery name="qryOrganisationType" datasource="#application.sitedatasource#">
			<cfif StructKeyExists(arguments,"columnList")>
				select #arguments.columnList#
			<cfelse>
				select *
			</cfif>
			from OrganisationType
			where 1=1
			<cfif not StructKeyExists(arguments,"organisationTypeId") or arguments.OrganisationTypeId eq "" >
				<!--- organisationTypeId not sent in so don't filter --->
			<cfelseif val(OrganisationTypeId) gt 0>
				and OrganisationTypeId = <cfqueryparam CFSQLType="cf_sql_integer" value="#arguments.OrganisationTypeId#">
			<cfelse>
				and TypeTextId = <cfqueryparam CFSQLType="cf_sql_varchar" value="#arguments.OrganisationTypeId#">
			</cfif>
		</cfquery>

		<cfreturn qryOrganisationType>
	</cffunction>


</cfcomponent>

