<!--- �Relayware. All Rights Reserved 2014 --->

<cfcomponent displayname="Common SOD Queries" hint="Contains a number of commonly used SOD Queries">

	<cffunction name="addSODgroup">
		<!--- add a new group if one doesn't already exist with that name --->
		<cfargument name="groupName" required="yes" type="string">
		
		<cfquery name="addSODgroup" datasource="#application.sitedatasource#">
			declare @groupID int
			
			select @groupID = productHierarchyGroupID from SODhierarchyGroup
				where productHierarchyGroupName = rtrim(ltrim(<cf_queryparam value="#arguments.groupName#" CFSQLTYPE="CF_SQL_VARCHAR" >	))

			if (@groupID is null)
				begin
					insert into SODhierarchyGroup (productHierarchyGroupName, groupTypeID) values (RTRIM(LTRIM(<cf_queryparam value="#arguments.groupName#" CFSQLTYPE="CF_SQL_VARCHAR" >)),2)
	
					select @groupID = max(productHierarchyGroupID) from SODhierarchyGroup
				end
			else
				begin
					select @groupID = -1 from SODhierarchyGroup
				end
				
			select SODgroupID = @groupID
			
		</cfquery>
		
		<cfreturn addSODgroup.SODgroupID>
	</cffunction>
	
	
	<cffunction name="updateSODgroup">
		<!--- update a name of a product group --->
		<cfargument name="newGroupName" required="yes" type="string">
		<cfargument name="groupID" required="yes" type="string">
		
		<cfquery name="updateSODgroup" datasource="#application.sitedatasource#">
		
			declare @groupID int
			
			select @groupID = productHierarchyGroupID from SODhierarchyGroup
				where productHierarchyGroupName = rtrim(ltrim(<cf_queryparam value="#arguments.groupName#" CFSQLTYPE="CF_SQL_VARCHAR" >	))
			
			if (@groupID is null)
				begin
					update SODhierarchyGroup  set productHierarchyGroupName = rtrim(ltrim(<cf_queryparam value="#arguments.groupName#" CFSQLTYPE="CF_SQL_VARCHAR" >	))
						where productHierarchyGroupID =  <cf_queryparam value="#arguments.groupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				end
			else
				begin
					select @groupID = -1 from SODhierarchyGroup
				end
			
			select SODgroupID = @groupID
				
		</cfquery>
		<cfreturn updateSODgroup.SODgroupID>
	</cffunction>
	
	
	
	<cffunction name="deleteSODgroup">
		<cfargument name="groupName" required="yes" type="string">
		
		<cfquery name="deleteSODgroup" datasource="#application.sitedatasource#">
			delete from SODhierarchyGroup where productHierarchyGroupName =  <cf_queryparam value="#arguments.groupName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>
	</cffunction>
	
	
	
	<cffunction name="addSODhierarchy">
		<!--- add a new hierarchy if one doesn't already exist with that name in the product group --->
		<cfargument name="groupID" required="yes" type="numeric">
		<cfargument name="level1" required="yes" type="string">
		<cfargument name="level2" required="yes" type="string">
		<cfargument name="level3" required="yes" type="string">
		<cfargument name="level4" required="yes" type="string">
		<cfargument name="level5" required="yes" type="string">
		
		<cfquery name="addSODhierarchy" datasource="#application.sitedatasource#">
			declare @hierarchyID int

			select @hierarchyID = productHierarchyID from SODhierarchy
			where 
				level1 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level1#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level2 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level2#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level3 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level3#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level4 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level4#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level5 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level5#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				productHierarchyGroupID = <cf_queryparam value="#arguments.groupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			
			if (@hierarchyID is null)
				begin
					insert into SODhierarchy (productHierarchyGroupID,level1,level2,level3,level4,level5) values 
					(
						<cf_queryparam value="#arguments.groupID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level1#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
						UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level2#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
						UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level3#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
						UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level4#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
						UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level5#" CFSQLTYPE="CF_SQL_VARCHAR" >)))
					)
					
					select newHierarchyID = scope_identity() from SODhierarchy
				end
			else
				begin
					select newHierarchyID = @hierarchyID
				end
		</cfquery>
		<cfreturn addSODhierarchy.newHierarchyID>
	</cffunction>
	

	<cffunction name="moveSODhierarchy">
		<!--- edit a hierarchy. If a hierarchy already exists with that name, move the products from
			the old hierarchy to the new one --->
		<cfargument name="groupID" required="yes" type="numeric">
		<cfargument name="hierarchyID" required="yes" type="numeric">
		<cfargument name="level1" required="yes" type="string">
		<cfargument name="level2" required="yes" type="string">
		<cfargument name="level3" required="yes" type="string">
		<cfargument name="level4" required="yes" type="string">
		<cfargument name="level5" required="yes" type="string">
		
		<cfquery name="moveSODhierarchy" datasource="#application.sitedatasource#">
			
				declare @existingHierarchyID int
			
				select @existingHierarchyID = productHierarchyID from SODhierarchy
			where 
				level1 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level1#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level2 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level2#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level3 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level3#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level4 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level4#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				level5 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level5#" CFSQLTYPE="CF_SQL_VARCHAR" >))) and
				productHierarchyGroupID = <cf_queryparam value="#arguments.groupID#" CFSQLTYPE="CF_SQL_INTEGER" >
						
				if (@existingHierarchyID is null)
					begin
						update SODhierarchy set 
							level1 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level1#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
							level2 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level2#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
							level3 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level3#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
							level4 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level4#" CFSQLTYPE="CF_SQL_VARCHAR" >))),
							level5 = UPPER(RTRIM(LTRIM(<cf_queryparam value="#arguments.level5#" CFSQLTYPE="CF_SQL_VARCHAR" >)))
						where productHierarchyID = #arguments.hierarchyID#
					end
				else
					begin
						update SODproductHierarchy set 
							productHierarchyID = @existingHierarchyID
							
						delete from SODhierarchy where productHierarchyID = #arguments.hierarchyID#
					end

		</cfquery>
	</cffunction>
	
	
	<cffunction name="deleteSODhierarchy">
		<!--- delete a hierarchy. If it contains products, delete them as well --->
		<cfargument name="hierarchyID" required="yes" type="numeric">
		
		<cfquery name="deleteSODhierarchy" datasource="#application.sitedatasource#">
			if exists
				(select ProductCode from SODproductHierarchy where productHierarchyID=#arguments.hierarchyID#)
			begin
				delete from SODproductHierarchy where productHierarchyID = #arguments.hierarchyID#
			end
			
			delete from SODhierarchy where productHierarchyID = #arguments.hierarchyID#
		</cfquery>
	</cffunction>
	
	
	
	<cffunction name="getSODhierarchies">
		<cfargument name="groupID" required="yes" type="numeric">
		
		<cfset getSODhierarchies = "">
		<cfquery name="getSODhierarchies" datasource="#application.sitedatasource#">
			select distinct cast(productHierarchyID as bigint) as productHierarchyID, level1 + '|' + level2 + '|' + level3 + '|' + level4 + '|' + level5  as productHierarchy,
			level1 + '-' + level2 + '-' + level3 + '-' + level4 + '-' + level5  as productHierarchyDisplay,
			level1, level2, level3, level4, level5 from SODhierarchy where productHierarchyGroupID = #arguments.groupID#
			order by productHierarchy
		</cfquery>
		<cfreturn getSODhierarchies>
	</cffunction>
	
	
	
	<cffunction name="addSODproduct">
		<cfargument name="productCode" required="yes" type="string">
		<cfargument name="hierarchyID" required="yes" type="numeric">
		
		<cfquery name="addSODproduct" datasource="#application.sitedatasource#">
			if not exists (
				select ProductCode from SODproductHierarchy
				where ProductCode =  <cf_queryparam value="#arguments.productCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
					productHierarchyID = #arguments.hierarchyID#
			)
			begin
				insert into SODproductHierarchy (ProductCode,productHierarchyID) values 
					(<cf_queryparam value="#arguments.productCode#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#arguments.hierarchyID#" CFSQLTYPE="cf_sql_integer" >)
			end
		</cfquery>
	</cffunction>
	
				
	<cffunction name="deleteSODproduct">
		<!--- delete SOD products... if a hierarchy is empty after a product deletion, delete it as well --->
		<cfargument name="productCode" required="yes" type="string">
		<cfargument name="hierarchyID" required="yes" type="numeric">
		
		<cfquery name="deleteSODproduct" datasource="#application.sitedatasource#">
			delete from SODproductHierarchy where ProductCode =  <cf_queryparam value="#arguments.productCode#" CFSQLTYPE="CF_SQL_VARCHAR" >  and productHierarchyID=#arguments.hierarchyID#
		
			if not exists
				(select ProductCode from SODproductHierarchy where productHierarchyID=#arguments.hierarchyID#)
			begin
				delete from SODhierarchy where productHierarchyID = #arguments.hierarchyID#
			end
		</cfquery>
	</cffunction>
	
	
	<cffunction name="getSODproducts">
		<cfargument name="groupID" required="yes" type="numeric">
		<cfargument name="hierarchyID" required="no" type="numeric" default="-1">
		
		<cfset getSODproducts = "">
		<cfquery name="getSODproducts" datasource="#application.sitedatasource#">
			select distinct sph.ProductCode, sph.productHierarchyID,
				sh.level1, sh.level2, sh.level3, sh.level4, sh.level5, 
				level1 + '-' + level2 + '-' + level3 + '-' + level4 + '-' + level5  as productHierarchy
				from SODproductHierarchy sph inner join SODhierarchy sh on
				sph.productHierarchyID = sh.productHierarchyID
				where sh.productHierarchyGroupID = #arguments.groupID#
				<cfif arguments.hierarchyID neq -1>
					and sph.productHierarchyID = #arguments.hierarchyID#
				</cfif>
				order by productHierarchy, ProductCode
		</cfquery>
		<cfreturn getSODproducts>
	</cffunction>
	
	
	<cffunction name="getPossibleSODlocationMatchesInCM">
		<!--- bring back locations in CM that possibly match a company within SOD --->
		<cfargument name="searchOrgName" required="yes" type="string">
		<cfargument name="countryID" required="yes" type="numeric">
		<cfargument name="searchMode" required="no" type="string" default="tight">
		
		<cfset getPossibleSODlocationMatchesInCM = "">
		<cfquery name="getPossibleSODlocationMatchesInCM" datasource="#application.sitedatasource#">
			select distinct o.organisationID, o.organisationName, 
			l.sitename, l.locationID, isNull(l.address1,'') as address1, isNull(l.Address5,'') as city, isNull(l.postalcode,'') as postalCode
			from Organisation o inner join
			Location l on o.organisationID = l.organisationID
			where o.countryID = #arguments.countryID# and 
			<cfif searchMode eq "loose">
				DIFFERENCE(o.organisationName, <cf_queryparam value="#arguments.searchOrgName#" CFSQLTYPE="CF_SQL_VARCHAR" >) >= 4 and
			<cfelseif searchMode eq "tight">
				o.organisationName  like  <cf_queryparam value="#arguments.searchOrgName#%" CFSQLTYPE="CF_SQL_VARCHAR" >  and
			</cfif>
			l.locationID not in (select locationID from SODloadLocation)
			order by o.organisationName, l.sitename
		</cfquery>
		<cfreturn getPossibleSODlocationMatchesInCM>
	</cffunction>
	
	
	<cffunction name="getPossibleOrganisationMatchesInSOD">
		<!--- bring back locations in SOD that possibly match an organisation --->
		<cfargument name="searchOrgName" required="yes" type="string">
		<cfargument name="countryID" required="yes" type="numeric">
		<cfargument name="searchMode" required="no" type="string" default="tight">
		
		<cfset getPossibleOrganisationMatchesInSOD = "">
		<cfquery name="getPossibleOrganisationMatchesInSOD" datasource="#application.sitedatasource#">
			select distinct thirdPartyForeignKey, companyName as organisationName, companyName as sitename, 
				isNull(address1,'') as address1, isNull(city,'') as city, postalCode, thirdPartyForeignKey as locationID
			from SODinitialLoad 
			where thirdPartyForeignKey not in
				(select distinct thirdPartyForeignKey from SODloadLocation) and
			<cfif searchMode eq "loose">
				DIFFERENCE(companyName, <cf_queryparam value="#arguments.searchOrgName#" CFSQLTYPE="CF_SQL_VARCHAR" >) >= 4 and

			<cfelseif searchMode eq "tight">
				companyName  like  <cf_queryparam value="#arguments.searchOrgName#%" CFSQLTYPE="CF_SQL_VARCHAR" >  and
			</cfif>
				countryID = #arguments.countryID#
			order by companyName
		</cfquery>
		<cfreturn getPossibleOrganisationMatchesInSOD>
	</cffunction>
	
	
	<cffunction name="createMatch">
		<!--- create a match between an CM organisation and a company in the SOD --->
		<cfargument name="thirdPartyForeignKey" required="yes" type="numeric">
		<cfargument name="locationID" required="yes" type="numeric">
		
		<cfquery name="createMatch" datasource="#application.sitedatasource#">
			insert into SODloadLocation (thirdPartyForeignKey,locationID,matchMethod,created) values
				(<cf_queryparam value="#arguments.thirdPartyForeignKey#" CFSQLTYPE="cf_sql_varchar" >, <cf_queryparam value="#arguments.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >,'Manual',<cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
		</cfquery>
		
		<cfset populateloadTable(thirdPartyForeignKey=#arguments.thirdPartyForeignKey#)>
	</cffunction>
	
	
	<cffunction name="populateloadTable">
		<cfargument name="thirdPartyForeignKey" required="no" type="string" default="">
		<cfargument name="loadTableName" required="no" type="string" default="SODload">
	
	
		<cfset var qPopulateloadTable="">
	
		<!--- load automatic org and location matches and any manual ones --->
		<!--- should we be loading new locations at matched organisations? --->
		<cfquery name="qPopulateloadTable" datasource="#application.siteDataSource#">
			Insert into #loadTableName#
			Select 
				ThirdPartyForeignKey,
				CompanyName,
				DistiName,
				PostalCode,
				CountryID,
				ProductCode,
				Quantity,
				cast(UnitPrice as money),
				cast(TotalPrice as money),
				SalesDate,
				datepart(wk,SalesDate),
				datepart(mm,SalesDate),
				datepart(qq,SalesDate),
				datepart(yyyy,SalesDate)
			from SODinitialLoad
			where 	
				<cfif arguments.thirdPartyForeignKey neq "">	
					ThirdPartyForeignKey =  <cf_queryparam value="#arguments.ThirdPartyForeignKey#" CFSQLTYPE="cf_sql_varchar" > 
				<cfelse>
				locationAction = 'Locations matched on city, country, orgid and postal code'
			or
				ThirdPartyForeignKey in (select distinct ThirdPartyForeignKey from SODloadLocation)
				</cfif>
		</cfquery>
	
	</cffunction>
	
	  <CFFUNCTION access="public" name="getUnMatchedSODCountries">
  		    <cfscript>
			  var getCountriesQry = "";
		    </cfscript>
			<!--- <CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
				SELECT c.countryDescription + ' (' + cast(count(distinct sil.ThirdPartyForeignKey) as varchar(255)) + ')' as country, c.countryID, c.ISOcode, count(distinct sil.ThirdPartyForeignKey) as counttpfk FROM 
				country c left outer join SODinitialLoad sil
				on c.countryID = sil.countryID
				WHERE c.countryID IN (#request.relayCurrentUser.countryList#)
				and sil.ThirdPartyForeignKey not in (select distinct ThirdPartyForeignKey from SODloadLocation)
				group by country, c.countryID, c.ISOcode, c.countryDescription
				HAVING (count(distinct sil.ThirdPartyForeignKey)) > 0
				ORDER BY country
			</CFQUERY> --->
			
			<CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
				SELECT c.countryDescription as country, c.countryID, c.ISOcode FROM 
				country c left outer join SODinitialLoad sil
				on c.countryID = sil.countryID
				WHERE c.countryID IN (#request.relayCurrentUser.countryList#)
				and sil.ThirdPartyForeignKey not in (select distinct ThirdPartyForeignKey from SODloadLocation)
				group by country, c.countryID, c.ISOcode, c.countryDescription
				ORDER BY country
			</CFQUERY>
	    
		<cfreturn getCountriesQry>
	  </CFFUNCTION>
	  
	   <CFFUNCTION access="public" name="getCountryCount">
  		   <cfargument name="countryID" required="Yes" type="numeric">
	
			<cfset var getCountriesCnt = "">
	
			<CFQUERY NAME="getCountriesCnt" dataSource="#application.siteDataSource#">
				SELECT		count(distinct ThirdPartyForeignKey) as countryCount, countryID
				FROM		SODinitialLoad
				WHERE		countryID = #countryID# AND
							ThirdPartyForeignKey not in (select distinct ThirdPartyForeignKey from SODloadLocation)
				group by	countryID
			</CFQUERY>
			
				    
			<cfreturn getCountriesCnt.countryCount>
	  </CFFUNCTION>
	  
	  <CFFUNCTION access="public" name="setApplicationSODCountryData">
  			<cfset userCountries = getUnMatchedSODCountries()>
			<cfset application.SODCountryData = structNew()>
			<cfloop query="userCountries">
				<cfset application.SODCountryData[countryID] = structNew()>
				<cfset application.SODCountryData[countryID].Country = country>
				<cfset application.SODCountryData[countryID].CountryCount = getCountryCount(countryID)>
			</cfloop>
	  </CFFUNCTION>
	  
	  
	  <CFFUNCTION access="public" name="getUnMatchedOrganisationLocations">
	  	<cfargument name="organisationID" required="Yes" type="numeric">
		    <cfscript>
			  var getCountriesQry = "";
		    </cfscript>
			<CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
				SELECT countryDescription + ' (' + cast((select count(locationID) from location where countryID = c.countryID and organisationID = #arguments.organisationID# and locationID not in (select locationID from SODloadLocation)) as varchar(255)) + ')' as country, countryID, ISOcode FROM 
				country c
				WHERE countryID IN (#request.relayCurrentUser.countryList#)
				and countryID in (select distinct location.countryID from location where organisationID = #arguments.organisationID#)
				ORDER BY country
			</CFQUERY>
	    
		<cfreturn getCountriesQry>
	  </CFFUNCTION>
	
</cfcomponent>
