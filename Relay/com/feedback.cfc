<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	
	

--->

<cfcomponent displayname="Lifestyle Feedback" hint="This cfc manages the feedback facility for Lifestyle">



<!--- 

	This determines if somebody is allowed to provide feedback for a person. 

--->
<cffunction name="postingFeedbackAllowed" returntype="boolean" output="Yes">
	<cfargument name="personID" required="Yes">
	<cfargument name="posterID" required="Yes">
	<cfargument name="feedbackGroupID" required="Yes">
	<cfargument name="relationshipFlagID" required="Yes">
	<cfargument name="relationshipMode" required="No" default="Entity">
	<cfargument name="timeWindowBlock" required="No" default="#dateAdd('M',-3,now())#">

	<cfscript>
		// Check the relationship.
		if (relationshipMode eq "Entity"){
			relationshipQry = application.com.flag.getFlagData(
				flagId = arguments.relationshipFlagID,
				entityID = arguments.personID,
				data = arguments.posterID);
		}
		else {
			relationshipQry = application.com.flag.getFlagData(
				flagId = arguments.relationshipFlagID,
				entityID = arguments.posterID,
				data = arguments.personID);
		}
		// You can only leave feedback if you are in a valid relationship.
		if (relationshipQry.recordcount eq 0){
			return false;
		}
		
		// Now get current results.
		lastRating = getPosterFeedback (
			personID = arguments.personID,
			posterID = arguments.posterID,
			feedbackGroupID = arguments.feedbackGroupID);
			
		if (len(lastRating.lastUpdated) and lastRating.lastUpdated gt timeWindowBlock) {
				// User is trying to post feedback when they have done within the allowed time window..
			return false;
		}
		else {
			// User has never posted feedback or is outside of the timewindow.
			return true;
		}		
	</cfscript>

</cffunction>

<!--- 

	This determines if somebody is allowed to provide feedback for a person. This assumes that the user has confirmed that posting feedback is allowed.

--->
<cffunction name="postFeedback">
	<cfargument name="posterID" required="Yes">
	<cfargument name="feedbackGroupID" required="Yes">
	<cfargument name="formStruct" required="Yes">
	<cfargument name="feedbackrange" default="5" required="No">
	
	<!--- get the child groups. --->
	<cfscript>
	
		childFlagGroupsQry = application.com.flag.getFlagGroupChildGroups(
			ParentFlagGroupId= application.com.flag.getFlagGroupStructure(arguments.feedbackGroupID).flaggroupID);

	</cfscript>
	<cfloop query="childFlagGroupsQry">
		<!--- We need to clear down the set of values for the poster. --->
		<cfloop from="1" to="#feedbackrange#" index="rangeIdx">
			<cfscript>
				application.com.flag.deleteFlagdata (
					flagId = application.com.flag.getFlagStructure("#childFlagGroupsQry.FlagGroupTextID##rangeIdx#").flagID,
					entityid = arguments.formStruct.personID,
					data = arguments.posterID);
			</cfscript>		
		</cfloop>
		<!--- Now add sthe correct value. --->
		<cfscript>
			application.com.flag.setFlagData(
				flagId =application.com.flag.getFlagStructure("#childFlagGroupsQry.FlagGroupTextID##form[childFlagGroupsQry.FlagGroupTextID]#").flagID,
				entityID = arguments.formStruct.personID,
				data = arguments.posterID);			
		</cfscript>
	</cfloop>
</cffunction>

<!--- 

	This function returns the value 

--->
<cffunction name="getPosterFeedback" returntype="struct" output="no">
	<cfargument name="personID" required="Yes">
	<cfargument name="posterID" required="Yes">
	<cfargument name="feedbackGroupID" required="Yes">
	<cfargument name="feedbackrange" default="5" required="No">
	<cfscript>
		var summaryResults = structNew();
		summaryResults.lastUpdated = "";
		childFlagGroups = application.com.flag.getFlagGroupChildGroups(
			ParentFlagGroupId= application.com.flag.getFlagGroupStructure(feedbackGroupID).flaggroupID);
	</cfscript>
	<cfloop query="childFlagGroups">
		<cfscript>
			// Default the value.
			summaryResults[childFlagGroups.FlagGroupTextId] = "";
		</cfscript>
		<cfloop from="1" to="#feedbackrange#" index="rangeIdx">
			<cfscript>	
				// Determine which 'value' has been set.
				flagData = application.com.flag.getFlagData(
					flagId = "#childFlagGroups.FlagGroupTextId##rangeIdx#",
					entityID = arguments.personID,
					data = arguments.posterID);
				if (flagData.recordcount) {
					summaryResults[childFlagGroups.FlagGroupTextId] = rangeIdx;
					summaryResults.lastUpdated = flagData.lastUpdated;
				}
			</cfscript>
		
		
		</cfloop>
		
	</cfloop>
	<cfreturn summaryResults>
	
</cffunction>
<!--- 
	
	This function returns a structure that contains the set of information for each feedback item and a summary set of information.

--->
<cffunction name="getFeedBackRating" returntype="struct" output="Yes">
	<cfargument name="personID" required="Yes">
	<cfargument name="feedbackGroupID" required="Yes">
	<cfargument name="feedbackrange" default="5" required="No">

	<cfscript>
		// Build the result structure.
		var summaryResults = structNew();
		summaryResults.resultset = ArrayNew(1);
		summaryResults.summaryPercentResult = "";
		summaryResults.summarySampleSize = 0;
		childFlagGroups = application.com.flag.getFlagGroupChildGroups(
		ParentFlagGroupId= application.com.flag.getflaggroupStructure(feedbackGroupID).flaggroupID);	
	</cfscript>
	
	<!--- Get the set of feedback areas. --->
	<cfset sumtotal = 0>
	<cfset sampleSize = 0>
	<cfset lastFeedbackDate = "">
	<cfloop query="childFlagGroups">
		<cfscript>
			// Initialise the values the totals.
			summaryResults.resultSet[childFlagGroups.currentRow] = structNew();
			sampleSize = 0;
			total = 0;
		</cfscript>
		<cfloop from="1" to="#arguments.feedbackrange#" index="rangeIdx">
			<cfscript>
			// Get the feedback group elements
			flagData = application.com.flag.getFlagData(
				flagId = "#childFlagGroups.FlagGroupTextId##rangeIdx#",
				entityID = arguments.personID);
			// Get the totals.
			total = total + (flagData.recordcount * rangeIdx);
			sampleSize = sampleSize + flagData.recordcount;
			lastFeedbackDate = flagData.lastUpdated;
			</cfscript>
			
		</cfloop>
		<cfscript>
		// Now work out the total for the group.
		if (sampleSize neq 0){
			summaryResults.resultSet[childFlagGroups.currentRow].percentResult = total/(sampleSize*arguments.feedbackrange) * 100;
		}
		else {
			summaryResults.resultSet[childFlagGroups.currentRow].percentResult = 0;
		}
		summaryResults.resultSet[childFlagGroups.currentRow].sampleSize = sampleSize;
		summaryResults.resultSet[childFlagGroups.currentRow].FLAGGROUPTEXTID = childFlagGroups.FlagGroupTextId;
		// Add the total to the final total.
		sumtotal = sumtotal + total;			
		</cfscript>		
	</cfloop>
	
	<cfscript>
	// Now get the summary.
	if (sampleSize neq 0){
		summaryResults.summaryPercentResult = sumtotal/(sampleSize * arguments.feedbackrange * childFlagGroups.recordcount) * 100;
		summaryResults.sumtotal = sumtotal;
	}
	else {
		summaryResults.summaryPercentResult = "";
	}
	
	// sampleSize should be the same across all feedback, so picking the last one is fine.
	summaryResults.summarySampleSize = sampleSize;
	summaryResults.lastFeedbackDate = lastFeedbackDate;
	</cfscript>
	<cfreturn summaryResults>
</cffunction>

</cfcomponent>

