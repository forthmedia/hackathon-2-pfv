<!--- �Relayware. All Rights Reserved 2014 

File name: RelayEcommerceAdmin.cfc

2014-01-17 PPB Case 438548 added p.deleted filter to getCatalogueList()

 --->


<cfcomponent displayname="RelayEcommerceAdmin" hint="Admin functions for Relay eCommerce">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCatalogueList" hint="Returns a query object containing catalogues">
		<cfquery name="getCatalogueList" datasource="#this.dataSource#">
			SELECT distinct 
				promotion.CampaignID, 
				promotion.PromoName as catalogue, 
				(select count(*) from product p 
					where p.campaignID = promotion.campaignID and p.deleted=0) as products_in_catalogue,			<!--- 2014-01-17 PPB Case 438548 added p.deleted filter  --->
				(select count(*) from orders as o 
					where o.promoid = promotion.promoid ) as orders_placed,
				(select count(distinct locationid) from orders as o 
					where o.promoid = promotion.promoid ) as customers, 
				promotion.ShowDownloadLink,
				'Price Books' AS pricebookLink		
			FROM promotion 
			WHERE 
			<!--- WAB LID 6369 2011/04/20 added test for blank this.CampaignIDAllowed --->
			<cfif this.CampaignIDAllowed is not "">
			Promotion.CampaignID  IN ( <cf_queryparam value="#this.CampaignIDAllowed#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfelse>
			1=0
			</cfif>			

			ORDER BY promotion.PromoName
		</cfquery>
		<cfset this.qGetCatalogueList = getCatalogueList>
	</cffunction>

</cfcomponent>
