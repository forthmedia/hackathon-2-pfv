<!--- �Relayware. All Rights Reserved 2015 --->
<!---
File name:			relayOrder.cfc
Author:				AHL
Date started:		2015

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed
Possible enhancements:
2016-03-18 AHL Sugar 448590 Formating payment Date properly

 --->

<cfcomponent hint="Incentive Order" displayname="order Component">

	<cffunction name="getOrderHeader" access="public" returntype="query">
		<cfargument name="orderID" required="true" />
		<cfquery name="qryOrderHeader" datasource="#application.sitedatasource#">
			select 	
				o.* ,
				promotion.*,
				p.firstname,
				p.lastname,
				p.email,
				l.sitename,
				c.countrydescription,
				c.countrycurrency,
				c.countryid,
				c.AllowVATReduction, c.VATFormat,
				c.vatRequired,
				ug.name as CreatedByName,
				pug.email as CreatedByEmail,
				delCountry.countryDescription as delCountryName,
				invCountry.countryDescription as invCountryName
			from
				orders as o
					left join 
				location as l			on  o.locationid=l.locationid
					left join 
				country as c			on l.countryid=c.countryid
					left join 
				person as p				on o.personid=p.personid 
					inner join  
				promotion				on o.promoid=promotion.promoid
					left join
				country as delCountry	on delCountry.countryID = delCountryID
					left join
				country as invCountry	on invCountry.countryID = invCountryID
					left join
				usergroup as ug			on ug.usergroupid=o.createdby
					left join 
				person as pug			on ug.personid=pug.personid 
					
			where
			o.orderid =  <cf_queryparam value="#OrderID#" cfsqltype="cf_sql_integer" > 
		</cfquery>
		<cfreturn qryOrderHeader/>
	</cffunction>

	<cffunction name="getOrderItems" access="public" returntype="query">
		<cfargument name="orderID" required="true" />
		<cfquery name="qryOrderItems" datasource="#application.sitedatasource#">
			Select 	
				*
			FROM 
				vOrderItems
			WHERE	
				orderid =  <cf_queryparam value="#orderID#" cfsqltype="cf_sql_integer" > 	
			AND 
				active = 1
		</cfquery>
		<cfreturn qryOrderItems/>
	</cffunction>

	<cffunction name="getOrderValue" access="public" returntype="query">
		<cfargument name="orderID" required="true" />
		<cfquery name="qryOrderValue" datasource="#application.sitedatasource#">			
			select 
				sum(quantity * unitdiscountprice) as totalvalue,
				sum(quantity * unitlistprice) as totallistvalue,
				sum(quantity * (unitlistprice * (1 - isnull(DistiDiscount,0)))) as TotalDistiValue,
				sum(totaldiscountprice) as totalvalue1,
				sum(totallistprice) as totallistvalue1,
				sum(prodweight) as totalWeight
			from 
				vorderitems as i
			where	
				i.orderid =  <cf_queryparam value="#orderID#" cfsqltype="cf_sql_integer" > 
				and	i.active=1
		</cfquery>
		<cfreturn qryOrderValue/>
	</cffunction>

	<cffunction name="getOrderMergeFields" access="public" returntype="struct">
		<cfargument name="fieldName" type="string" default="">
		<cfargument name="orderID" type="numeric">
		<cfargument name="order" required="false">

		<cfset var qryOrderValue = "">
		
		<cfif not structKeyExists(arguments,"order")>
			<cfset order = application.com.structureFunctions.queryRowToStruct(query=application.com.relayOrder.getOrderHeader(orderId=arguments.orderID),row=1)>
		</cfif>
		
		<!--- START 2016-03-18 AHL Sugar 448590 Formating payment Date properly --->
		<cfif structKeyexists(order,'paymentDate')>
				<cfset order.paymentDate = lsDateFormat(order.paymentDate,"medium") />
		</cfif>
		<!--- END 2016-03-18 AHL Sugar 448590 Formating payment Date properly --->

		<cfif not structKeyExists(order,"orderItems")>
			<cfset order.orderItems = application.com.relayOrder.getOrderItems(orderId=arguments.orderID)>
		</cfif>
		
		<cfif not structKeyExists(order,"orderTotals")>
			<cfset order.orderTotals = StructNew() />
		</cfif>
		
		<cfset qryOrderValue = application.com.relayOrder.getOrderValue(orderId=arguments.orderID)/>
		
		<cfif not structKeyExists(order.orderTotals,"totalvalue")>
			<cfset order.orderTotals.totalvalue = qryOrderValue.totalvalue/>
		</cfif>
		<cfif not structKeyExists(order.orderTotals,"totallistvalue")>
			<cfset order.orderTotals.totallistvalue = qryOrderValue.totallistvalue/>
		</cfif>
		<cfif not structKeyExists(order.orderTotals,"TotalDistiValue")>
			<cfset order.orderTotals.TotalDistiValue = qryOrderValue.TotalDistiValue/>
		</cfif>
		<cfif not structKeyExists(order.orderTotals,"totalvalue1")>
			<cfset order.orderTotals.totalvalue1 = qryOrderValue.totalvalue1/>
		</cfif>
		<cfif not structKeyExists(order.orderTotals,"totallistvalue1")>
			<cfset order.orderTotals.totallistvalue1 = qryOrderValue.totallistvalue1/>
		</cfif>
		<cfif not structKeyExists(order.orderTotals,"totalWeight")>
			<cfset order.orderTotals.totalWeight = qryOrderValue.totalWeight/>
		</cfif>

		<!---
		AHL instead of using a hook, I encourage customization by overriding this method using super
		<cf_include template="/code/cftemplates/orderEmailMergeBuilder_extension.cfm" checkifexists="true">
		--->

		<cfreturn order>
		
	</cffunction>

</cfcomponent>
