<!---  �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction access="public" name="RenewAsset" hint="Creates an Opportunity from the Asset">
		<cfargument name="asset" required="yes">

		<cfscript>
			var merge = structnew();
			var GetAssetProduct = "";
			var GetRenewalProduct = "";
			var renewalprice = 0;
			var productToUse = "";
			var Tier = "";
			var result = "";
			var oppStruct = structNew();
			var vendor = "";
			var OrgID = "";
			var contact = "";
			var getMainContactInfo = "";
			var unitPrice = 0;
			var contactQry = "";
		</cfscript>

		<!--- get the Asset Product info --->
		<cfset qryRenewalProduct = application.com.opportunity.getProduct(productID=arguments.asset.productID,countryID=arguments.asset.countryID)>

		<!--- default to the product/price from the asset to start --->
		<cfif arguments.asset.quantity eq 0>
			<cfset unitPrice = 0>
		<cfelse>
			<cfset unitPrice = val(arguments.asset.price)/arguments.asset.quantity>
		</cfif>
		<cfset productToUse = arguments.asset.productID>
		<cfset lookupPrice = false>

		<!--- if the product has a RenewalSKU look up the price from the pricebook OR if the asset price is 0 then look up the price from the pricebook --->
		<cfif qryRenewalProduct.RenewalSKU neq "" OR arguments.asset.price eq "" OR arguments.asset.price eq 0>

			<cfif qryRenewalProduct.RenewalSKU neq "">
				<cfset qryRenewalProduct = getProduct(SKU=qryRenewalProduct.renewalSKU,countryID=arguments.asset.countryID)>
			</cfif>

			<cfif qryRenewalProduct.recordcount EQ 1>
				<cfset lookupPrice = true>				<!--- set a flag to lookup the price later cos getOppProductPrice (which does a lot of the hard work esp applying 'on pricebook'-type promotions) requires opportunityID and we haven't inserted the opp yet --->
			</cfif>
		</cfif>

		<!--- create the opportunity --->
		<cfset oppStruct.entityID = arguments.asset.entityID>
		<cfset oppStruct.detail = "Renewal: #qryRenewalProduct.Description#">
		<cfset oppStruct.currency = arguments.asset.currencyISOCode>
		<cfset oppStruct.stageID = 1>
		<cfset oppStruct.expectedCloseDate = arguments.asset.EndDate>
		<cfset oppStruct.contactPersonID = arguments.asset.contactPersonID>
		<cfset oppStruct.countryID = arguments.asset.countryID>

		<!--- Get the primary contact for the partner Org --->

		<!--- If the Asset is being Manually Renewed use the Current User --->
		<cfif structKeyExists(url,"assetid")>
			<cfset oppStruct.partnerSalesPersonID = request.relaycurrentuser.personid>
			<cfset oppStruct.partnerLocationId = request.relaycurrentuser.locationid>
			<cfset vendor = application.com.flag.getflagdata(flagid="accManager",entityid=request.relaycurrentuser.organisationid)>
			<cfif vendor.data NEQ "">
				<cfset oppStruct.vendorAccountManagerPersonID = #vendor.data#>
			</cfif>
		<cfelse>
			<!--- If the Asset is being Renewed by a Scheduled Task use EntityIDs --->
			<cfif GetAssets.partnerEntityTypeID EQ 2> <!--- If the EntityID is an Org get the primary contact --->
				<cfset contactQry = application.com.flag.getflagdata(flagid="keyContactsPrimary",entityid=arguments.asset.partnerEntityID)>
				<cfset vendor = application.com.flag.getflagdata(flagid="accManager",entityid=arguments.asset.partnerEntityID)>
				<cfset orgHQ = application.com.flag.getFlagdata(flagID="HQ",entityID=arguments.asset.partnerEntityID)>

				<!--- LID 3872 get the organisation's headquarter location --->
				<cfif orgHQ.data neq "">
					<cfset oppStruct.partnerLocationID = orgHQ.data>
				<cfelse>

					<!--- LID 3872 if it doesn't have a headquarters, grab the first location created --->
					<cfquery name="getFirstCreatedLocation" datasource="#application.siteDataSource#">
						select top 1 locationID from location where organisationID =  <cf_queryparam value="#arguments.asset.partnerEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >  order by created desc
					</cfquery>
					<cfset oppStruct.partnerLocationID = getFirstCreatedLocation.locationID>
				</cfif>
			<cfelse>
				<!--- If the partnerEntityTypeID is 1 - a Location get the OrgID --->
				<cfset OrgID = application.com.RelayPLO.getLocationOrg(locationid=arguments.asset.partnerEntityID)>
				<cfset contactQry = application.com.flag.getflagdata(flagid="PrimaryContacts",entityid=arguments.asset.partnerEntityID)>
				<cfset vendor = application.com.flag.getflagdata(flagid="accManager",entityid=OrgID)>
				<cfset oppStruct.partnerLocationId = arguments.asset.partnerEntityID>
			</cfif>
			<cfif vendor.data NEQ "">
				<cfset oppStruct.vendorAccountManagerPersonID = vendor.data>
			</cfif>

			<cfset contact = valueList(contactQry.data)>

			<!--- if there is no primary contact at that organisation, set the partner sales person to be the vendor organisation's primary contact --->
			<cfif contact eq "">
				<cfset contact = application.com.flag.getflagdata(flagid="keyContactsPrimary",entityid="2").data>
			</cfif>

			<!--- NJH 2016/07/28 JIRA PROD2016-351 - dealing with multiple primary contacts. Don't assign as partner if more than 1 primary contact at org/location --->
			<cfif contact neq "" and listLen(contact) eq 1>
				<cfset oppStruct.partnerSalesPersonID = contact>
			</cfif>
		</cfif>

		<cfparam name="oppStruct.DistiLocationId" default="0">
		<cfif GetAssets.distiEntityTypeID EQ 1>
			<cfset oppStruct.DistiLocationId = arguments.asset.distiEntityID>
		<cfelseif GetAssets.distiEntityTypeID EQ 2 and GetAssets.distiEntityID neq "">
			<cfset distilocation = application.com.RelayPLO.getLocations(actionOrgs=arguments.asset.distiEntityID)>
			<cfset oppStruct.DistiLocationId = distilocation.locationID>
		</cfif>

		<!--- gets the opptype from the db  --->
		<cfquery name="qrygetOpportunityType" datasource="#application.siteDataSource#">
			select oppTypeID
			from OppType
			Where oppTypeTextID = 'Renewal'
		</cfquery>

		<cfset oppStruct.oppTypeID = qrygetOpportunityType.oppTypeID>

		<!--- Add the opportunity --->
		<cfset oppid = application.com.relayEntity.insertOpportunity(opportunityDetails=oppStruct)>

		<!--- update the Asset with the Renewal opportunity ID --->
		<CFQUERY NAME="UpdateAsset" DATASOURCE="#application.siteDataSource#">
			UPDATE Asset
				set renewalOppID =  <cf_queryparam value="#oppid.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
				lastUpdated =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			WHERE assetID =  <cf_queryparam value="#arguments.asset.assetID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>


		<cfif lookupPrice>
			<cfset result = application.com.opportunity.getOppProductPrice(opportunityId=oppid.entityID,productID=qryRenewalProduct.productID)>

			<cfif result.unitprice GT 0>
				<cfset unitprice = result.unitprice>
				<cfset productToUse = qryRenewalProduct.productID>
			</cfif>
		</cfif>

		<!--- create the opportunity Product --->
		<cfset product = application.com.opportunity.addProductToOpportunity(opportunityId=oppid.entityID,productID=productToUse,quantity=arguments.asset.quantity,unitPrice=unitPrice)>

		<!--- send the email --->
		<cfset merge.opportunityID = oppid.entityID>
		<cfset merge.assetName = qryRenewalProduct.Description>
		<cfset merge.assetEndDate = arguments.asset.enddate>
		<cfset merge.detail = oppStruct.detail>

		<cfif oppStruct.contactPersonID eq "">		<!--- i don't think this should happen, but i put this in as a trap against dodgy data while running the scheduled task --->
			<cfset merge.mainContact = "">
		<cfelse>
			<cfquery name="getMainContactInfo" datasource="#application.siteDataSource#">
				select firstname+' '+lastname as mainContact from person where personID =  <cf_queryparam value="#oppStruct.contactPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfset merge.mainContact = getMainContactInfo.mainContact>
		</cfif>

		<!--- NJH 2016/07/28 JIRA PROD2016-351 - dealing with multiple primary contacts. loop over contacts... the partnerSalesPersonId won't be assigned if list of (primary) contacts is more than 1 --->
		<cfset var contactPersonID = "">
		<cfloop list="#contact#" index="contactPersonID">
			<cfset emailsent = application.com.email.sendEmail(emailtextid = "AssetRenewal",personid = contactPersonID, mergeStruct = merge)>
		</cfloop>

		<cfreturn oppid.entityID>

	</cffunction>


	<cffunction access="public" name="getAssets" hint="Retrieves the Assets from the Asset table" returntype="query">
		<cfargument name="organisationID" 	required="no" type="numeric">
		<cfargument name="assetID" 			required="no" type="numeric">
		<cfargument name="sortOrder" 		required="no" default="EndDate">
		<cfargument name="resellerOrgID" 	required="no" type="numeric">
		<cfargument name="includeRenewed" required="no" type="boolean" default="false">

		<cfset var qGetAssets = "">

		<cfset partnerRenewalTimeSpan = application.com.settings.getSetting("leadManager.partnerRenewalTimeSpan")>


		<CFQUERY NAME="qGetAssets" DATASOURCE="#application.siteDataSource#">

			SELECT a.*
				,'Renew' AS RenewButton, showRenewButton=case when (datediff(d,getdate(),enddate) >= 0) AND renewable=1 AND renewalOppID IS NULL then 1 else 0 end
			<cfif structKeyExists(arguments,"organisationID") and arguments.organisationID NEQ 0>
				,p.description as description,
			<cfelse>
				,a.name as description,
			</cfif>
			org.OrganisationName,
			person.FirstName + ' ' + person.LastName as PersonFullName
			FROM Asset a
			<cfif structKeyExists(arguments,"organisationID")and arguments.organisationID NEQ 0>
				inner join product p on a.productid = p.productID
			</cfif>
			inner join country c on c.countryid = a.countryid
			inner join organisation org on org.organisationId = a.entityId
			left outer join person on person.personId = a.contactPersonId
		    WHERE 1=1
		    <cfif arguments.includeRenewed eq false>		<!--- PPB 2010-07-12 show it in sales history even if it has been renewed (but don't include it in scheduled renewal --->
		    	AND renewalOppID is NULL
			</cfif>
			<cfif structKeyExists(arguments,"resellerOrgID") and arguments.resellerOrgID NEQ 0>
				AND a.partnerEntityID = <cf_queryparam value="#arguments.resellerOrgID#" cfsqltype="cf_sql_integer">
			</cfif>
			<cfif (structKeyExists(arguments,"organisationID") and arguments.organisationID NEQ 0)>
				<cfif structKeyExists(arguments,"organisationID") and arguments.organisationID NEQ 0>
					AND a.entityID = <cf_queryparam value="#arguments.organisationID#" cfsqltype="cf_sql_integer">
				</cfif>
			<cfelseif structKeyExists(arguments,"assetID") and arguments.assetID NEQ 0>
				AND a.assetID = <cf_queryparam value="#arguments.assetID#" cfsqltype="cf_sql_integer">
			</cfif>
			<!--- to 'remove' date filter and show all assets set partnerRenewalTimeSpan in settings to be a very large number of days! --->
			AND (DATEDIFF(dd, GETDATE(), a.EndDate)) <=

			<cfif isdefined('renewalTimeSpan')>
			(CASE
				<CFLOOP COLLECTION="#renewalTimeSpan.IS0Code#" ITEM="ISOCODE">
					WHEN c.isocode =  <cf_queryparam value="#isocode#" CFSQLTYPE="CF_SQL_VARCHAR" >  then #renewalTimeSpan.IS0Code[isocode]#
				</CFLOOP>
				else #partnerRenewalTimeSpan#
			END)
			<cfelse>
				#partnerRenewalTimeSpan#
			</cfif>

			ORDER BY #arguments.sortOrder#
		</CFQUERY>

		<cfreturn qGetAssets>

	</cffunction>

</cfcomponent>