<!---

WAB 2009/06

Core of the Relayware Access Control Engine

WAB 2009/06/29  implemented special case security="None"
WAB 2009/06/30	came across boot up bug if XML file was corrupt.
				Added a test for boot failure and show warning message.
				Also modified some variable names itemToTest / directoryToTest

WAB 2009/07/14 changed name of fileAndDirectorySecurity.cfc to security.cfc
WAB 2009/09/14   LID 2625 - modified encryptHiddenFields to allow easier exclusion of fiels
WAB 2009/09/15   Mod to encrypt query string
WAB 2009/09/15   Added function encryptURL
WAB 2009/09/15   Added function confirmFieldsHaveBeenEncrypted
WAB 2009/10/15   Added otherServersInCluster to the isIPAddressTrusted function, used to add security to manageclusterws.cfc
WAB 2009/10/28   Mod to above to deal with ports in the otherserversincluster variable
WAB 2009/10/28   Added some extra var'ing to deal with intermittent security problem when loading a frame set
WAB 2010/02/27   Added some output=false to encryption functions
WAB 2010/04/26	 Problem discovered on Panda Project, encryptURL was outputting whitespace, have set all functions to output = false
WAB 2010/04/19 	 Fixed problem with encryptHiddenFields - regexp running out of control
WAB 2010/06/14   FNL069 support for CODE Mapping.   So is now synonymous with CONTENT - need to be sure that this will work!
WAB 2010/06/14   Support for a devsite specific security XML which is merged into the main XML
WAB 2010/06		 test on otherserversincluster changed to function
WAB 2010/06		Use New application.path variables
WAB 2011/06/09 	Came across a major flaw in the code to check whether current IPAddress was trusted.  If the regExp was blank then always returned a true result
WAB 2011/06 		Added single variable  encryption
WAB 2011/06/15 		Added code to allow certain variables to escape XSS checks
2011-07-04	NYB	P-LEN024 - changed sqlInjectionReFindResult to handle results as arrays - such as what is passed by cfgrid
WAB 2011/07/06 		Added code to handle virtual root folders (for want of a better word)
WAB 2011/07/08 	(but not committed until later).  Altered the security code so that a file/directory can be LoginRequired OR Trusted
WAB 2011/10/18  Added an attribute which will force a file/directory to be run with debug off (see cf_forceNodebug)
WAB 2011/11/15  confirmFieldsHaveBeenEncryptedWithFeedback() mod to use regExp and support regExp of fields which do not need encrypting
				added support for attributes encryptedVariables and nonencryptedVariables in the XML
				testCFCMethodSecurity() added support for testing argument encryption in webservice CFC methods
				new function encryptQueryColumn()
WAB 2012-01		Security Project.
				Code to add session tokens to all forms
				OnRequestEnd Processing
				Add random IV to formstate encryption

WAB 2012-02-21 Altered isIPAddressTrusted() to deal with requests which are NAT'ed by the datacenter firewall
WAB 2012-03-20	Beefed up the XSS protection to protect against more tags, all events and some keywords
WAB	2012-05-10	Mods to isIPAddressTrusted() - move some code into getHostAddress()
WAB 2012-05-16  Code to protect against CFFORM putting unencoded cgi.query _string onto page
IH	2012-06-07	Case 428652 Case 428652 trim value in function "queryParam"
WAB 2012-06-11  Case 428652 Case 428652 move location of above trim - don't trim data going in to db.  Just needed for testing whether string is a select statement
WAB 2012-06-13	Major reorganisation of security code to improve handling of CFC method security
				Moved Testing for blind post, injection and encryption from applicationMain into applySecurityRequirements() function.  Can now be called from elsewhere (such as webservices\callWebService.cfc)
				Improved functions for adding specific security requirements/exclusions to CFC Methods
				Added hasOwnSecurity attribute to securityXML - currently just used by callWebService.cfc
WAB 2012-09-19 CASE 430667 Added some locking to loading of the securityXML
WAB 2012-10-03 CASE 431028 Store formstate encryption key in settings - with consideration for future support for a rotating the keys automatically
				2012-10-03 had to change the delimiter I was using from - to |
WAB 2012-10-17 CASE 431347 encryptQueryString() - Prevent double encryption of formstate variables in a query string
WAB 2012-10-18 CASE 431359 QueryParam() was not allowing negative numbers as integers!
WAB 2012-11-14 CASE 432005 QueryParam() was not allowing brackets before a select statement
WAB 2012-11-29 CASE 432326 added isJSON_() function and use it in checkForSQLInjectionAndXSS() to deal with cases when isJSON() crashes
WAB 2013-01-24 Problem with JSON encoding long numbers in exponent format in the formstate structure. prepend a space before encoding and then trim afterwards
WAB 2013-01-09  CASE 433123 Do not worry about XSRF attacks when the user is not logged in, so we can allow blind POSTS when a user is logged out.  Prevents problems with login pages timing out
WAB 2013-02-11 Fix problem with queryparam() when date of format 'monthName, dd yy hh:mm'
WAB 2013-03-25  CASE 433419 alter getCFCMethodSecurityRequirements() to support XSSSafeVariables, SQLSafeVariables etc as attributes of a method
WAB 2013-05-02  Prevent injection into request.query_string by generating it from url structure (which ColdFusion has already sanitised)
WAB 2013-06-24 Added proper bit support to queryParam()
PPB 2013-09-03 Case 436271 change dangerousSQL RegEx to search for UPDATE followed by a space and SET followed by a space
WAB 2013-10-02	Case 437290 Cache the formStatePassKeys to reduce load on getSetting
WAB 2013-10-14	ESAPI now standard part of CF, so can remove various tests
WAB 2013-10-29	CASE 437623 Problem with <CF_input> inside <encryptHiddenFields> (especially with new encodeForHtmlAttribute). Spaces get encoded and put into the formstate structure but never decoded.  Added a canonicalize()
WAB 2013-11-11	Alter how session token created - due to changes to CF10 sessionids
WAB 2013-11-13  queryParam() Added support for null=true parameter
WAB 2013-09-11  CASE 436745 Security Project.  Block any URL containing %00 - a sure sign of trouble
WAB 2013-12-04	CASE 438291  Add functions to deal with load balancer and cgi.remote_addr not reflecting the client IP.  Added a function getRemoteAddress()
WAB 2013-01-10	Change variable XMLSearch to XMLSearchResult so isn't name of built in function
WAB 2014-02	Added locking around XMLSearches to try and prevent random errors and corruptions
NYB 2014-07-23 Case 441030 changed "alter." to "alter\s"
WAB 2014-09-09  Add ability to easily specify network ranges in the TrustedIPs attribute of the security XML.  Use CIDR notation (eg 67.52.136.52/30 specifies 4 addresses 67.52.136.52 - 67.52.136.55 )
WAB 2014-11-10 CASE 442598 deal with multiple formstate variables when encrypting querystrings - combine into one
WAB 2014-12-18 CASE 443133 Alteration of checkIPAddressInRangeCIDR() to support list/array of addresses
WAB 2015-01-15 CASE 443444 Problems with the missingTemplateHandler and filenames containing & with a space, caused an invalid variable name security error
WAB 2015-03-17 CASE 444099 Attempt to automatically detect and deal with with security XML getting corrupted.
WAB 2015-05-19 CASE 444754 - Rotation of SessionID on Login.  During rotation the value of session.SessionID will be incorrect.  Need to get the underlying session ID from Java
DAN 2015-07-17 Case 445264 - fix for Parameter Error due to false report of SQL/XSS injection - update keyword search for select to be followed by a space
WAB 2015-11-17 PROD2015-406 SQL Injection Security Added queryObjectName function to support cf_queryObjectName
				Also change to #this.injectionRe# change 'space' to 'nonword character'
WAB/DAN 2015-11-24 Case 446240 - using replace instead of replaceNoCase due to CF bug related to using 'replaceNoCase' with certain unicode chars (see: https://issues.jboss.org/browse/RAILO-2825)
WAB 2016-01-11	queryParam - allow getDate() as a valid sql_timeStamp
WAB 2016-01-12	Alterations to support security in more mapped directories than just 'code'.  So can now have testbox, unittests etc virtual directories
				Replaced the dev site specific security file with support for a testSite attribute.  This allows the unit test directories to be easily made available on test anddev sites
WAB 2016-01-15	PROD2015-518 Amend <cfthrow> in queryParam so that it reports the file and line where the <cf_queryparam> was actually called
WAB 2016-02-26	Further Attempts to  deal with XML corruption.  Implemented a better test for corruption and put called it from an extra place
				Got rid of virtual rooot concept - had only been required for backwards compatibility for Lexmark
				cache result of getTopLevelDirectoryNames()
ESZ 2016-04-05 case 448769 Fix Error in encryptQueryString()
WAB 2016-06-17	PROD2016-1280 More on XML Corruption.  Split code into a number of functions and add more tests for corruption - although still don't have a definitive test
				Added a housekeeping task, and as a result had to get rid of a few references to request.relayCurrentUser.isInternal which should have been using arguments.isInternal
WAB 2016-03-18	PROD2016-779 Implement a way to a template or function to only accept posts
WAB 2016-06-17  PROD2016-1276 XSRF Protection for ajax webservice calls, even if they are gets.  Implement requiresXSRFToken attribute to replace allowBlindPOST.
WAB	2016-06-27	During PROD2016-1334 CallRelayRecordManager security.
				encryptQueryColumn() did not work if column specificed in arguments.updatecolumnName did not exist
				Add support for arguments.singleSession in encryptQueryString()
PROD2016-1334 callRelayRecordManager security.  Prevent tampering with
PROD2016-1380  Deal with settings corruptions. isIpAddressRelayWareInc()  Don't get whole relaywareInc setting structure - just get items needed
WAB 2016-07-15	PROD2016-1418  Deal with securityXML Corruption.  Put a lock around whole of getFileSecurityRequirements
WAB 2016-09-20	PROD2016-1418	Still getting corruptions during testing.  Add a futher lock around isXMLCorrupted
				I think that I may have at last managed to prevent corruptions occurring, so the next thing to do would be to remove all the code related to checking for corruptions, because (as in this case) that code may itself cause problems
WAB 2016-11-03  Added ::1 to trusted IPaddresses (it is a loopback address on my CF2016 on windows 10 install)
NJH/WAB 2017/02/22 - RT-271 - check whether the string caught by the injection checker is actually runnable as sql, and therefore dangerous. If not, let it through.
WAB	2017-02-28	And bug fix - fell over on dates 

Possible enhancements:
dangerousSQL RegEx to search for keyword followed by a space (done for UPDATE and SET) OR a comment

--->

<cfcomponent>

	<cfset variables.formStatePassKeys = structNew()>
	<!---

	TODO permanent application variable and make sure is is blown when app variables are blown

	 --->
	<cfset xmlSearchLockName="xmlSearchLock">

	<cffunction name="loadXML" output="false">
		<cfargument name="refresh" default = "false">

		<cfset var xmlObj = "">
		<cfset var xmlFile = "">
		<cfset var coreXMLFilePath = "">
		<cfset var coreSecurityXMLResult = '' />
		<cfset var coreSecurityXML = "">
		<cfset var clientXMLFilePath = "">
		<cfset var clientSecurityXMLResult = '' />
		<cfset var clientSecurityXML = "">
		<cfset var clientContentNode = "">
		<cfset var devSiteXMLFilePath = '' />
		<cfset var devSiteSecurityXMLResult = '' />
		<cfset var coreContentNode = "">
		<cfset var result = structNew()>
		<cfset var root = '' />

		<cfset result.isOK = true>


		<cfif not structKeyExists (variables.applicationScope, "XMLDocs")>
			<cfset variables.applicationScope.XMLDocs = structNew()>
		</cfif>

		<cfif not structKeyExists (variables.applicationScope.XMLDocs, "Security")>
			<cfset variables.applicationScope.XMLDocs.security = structNew()>
			<cfset variables.applicationScope.XMLDocs.security.reload = true>
		</cfif>

		<cfif applicationScope.XMLDocs.security.reload or arguments.refresh>


			<cftry>
				<!--- WAB 2012-09-19 CASE 430667 and others
				 Added some locking - after some problems with random corruption of the security XML in memory.
				 However I think fairly unlikely that problem was in this area since this process is very quick --->
				<cflock name="loadSecurityXML" timeout="0" throwOnTimeOut=true>
					<cfset xmlObj = createObject("component","com.xmlfunctions")>  <!--- must be done as a create object to overcome boot strap issues --->

					<!--- read core XML --->
					<cfset XMLFile = "directorySecurity.xml">
					<cfset coreXMLFilePath = "#applicationScope.paths.relayware#/xml/" & XMLFile>
					<cfset coreSecurityXMLResult = xmlObj.readAndParseXMLFile (fileNameAndPath = coreXMLFilePath,changeCase_Name="lower")>
					<cfif coreSecurityXMLResult.isOK>
						<cfset coreSecurityXML = coreSecurityXMLResult.XML>
					<cfelse>
						<cfset result.isOK = false>
						<cfset result.message = coreSecurityXMLResult.message>
						<cfreturn result>
					</cfif>

					<!--- read UserFile XML --->
					<cfset clientXMLFilePath = "#applicationScope.paths.abovecode#/xml/" & XMLFile>
					<cfif fileExists (clientXMLFilePath)>
						<cfset clientSecurityXMLResult = xmlObj.readAndParseXMLFile (fileNameAndPath = clientXMLFilePath,changeCase_Name="lower")>
						<cfif clientSecurityXMLResult.isOK>
							<cfset clientSecurityXML = clientSecurityXMLResult.XML>
						<cfelse>
							<cfset result.isOK = false>
							<cfset result.message = clientSecurityXMLResult.message>
							<cfreturn result>
						</cfif>


						<cfset  xmlObj.XmlMerge (xml1=coreSecurityXML.directorysecurity.code,xml2=clientSecurityXML.directorysecurity.code,overwriteNodes=true,writeNodeDoc=coreSecurityXML)>

					</cfif>

					<!--- WAB 2016-01-12 removed support for a separate dev site security file, now done with a testSite attribute --->
					<cfset removeNodesFromXMLBasedOnTestSite (coreSecurityXML,application.testSite)>

					<cflock name="#xmlSearchLockName#" timeout="5" throwOnTimeOut=true>  <!--- Further lock to prevent searches taking place at the very moment when the XML is being updated --->
						<cfset variables.applicationScope.XMLDocs.security = {	Doc = coreSecurityXML
																				,reload = false
																				,lastLoaded = now()
																				}>
						<cfset result.message = "Reloaded #now()#">
						<cflog file="boot" text="SecurityXML loaded">
					</cflock>

				</cflock>

				<cfcatch>
					<cfset result.isOK = false>
					<cfif cfcatch.message is "An error occured while Parsing an XML document.">
						<cfset result.message = "XML Parse Error: " & cfcatch.detail>
					<cfelseif cfcatch.type is "lock" and cfcatch.lockname is "loadSecurityXML"> <!--- --->
						<cfif applicationScope.XMLDocs.security.reload is "TRUE">
							<!--- this is the situation of a lock during the first load of the XML structure, can't continue because will create further errors, so need to throw a real error --->
							<cfthrow type="ApplicationInitialising">
						<cfelse>
							<cfset result.message = "XML document locked">
						</cfif>
					<cfelse>
						<cfset result.message = cfcatch.message>
					</cfif>

				</cfcatch>

			</cftry>

		<cfelse>

			<cfset result.message = "Already In Memory">

		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- WAB 2015-03-17 CASE 444099
	Attempt to automatically detect and deal with with security XML getting corrupted.
	Usually manifests itself as all requests getting a directoryNotRegisted error
	This function is called from testFileSecurity() if it returns failure code directoryNotRegistered

	WAB 2016-02-26 Modified test for corruption - just see if index.cfm returns an isOK.  If it doesn't then something is amiss
					Added some locking to prevent multiple reloads

	--->

	<cffunction name="checkForCorruptedXMLAndReloadIfNecessary">
		<cfargument name="reference" default="">

		<cfset var result = "">

		<cfif isXMLCorrupted()>
			<cflock name="checkForCorruptionLock" timeout="10">
				<cfif isXMLCorrupted()>
					<cfset reloadCorruptedXML (reference)>
				<cfelse>
					<cflog file="boot" text="SecurityXML corruption fixed by other task (#reference#)">
				</cfif>
				<cfset result = "Reloaded">
			</cflock>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="reloadCorruptedXML">
		<cfargument name="reference" default="">
		<cfargument name="debugStruct" default={}>

		<cfset loadXML(refresh = true)>
		<cfmail subject="#CGI.server_name#: Directory Security Reload. #reference#" to="serverSupport@relayware.com" from = "serverSupport@relayware.com" type="html">
			<cfdump var="#debugStruct#">
		</cfmail>
		<cflog file="boot" text="SecurityXML corruption fixed (#reference#)">

	</cffunction>

	<!--- 	WAB 2016-06-16
			Still not got to bottom of XML corruptions and not detecting them all
			Pull the detection out into own function and add a few extra tests
			WAB 2016-09-20	PROD2016-1418	add a lock around this function (same lock as in getFileSecurityRequirements()).  Was getting race condition when doing load testing
	 --->
	<cffunction name="isXMLCorrupted">
		<cfset var result = false>
		<!--- 	Functional Tests
				A few files which should always be available, if they are not then there is a problem
				So testFileSecurity should return isOK.
		--->
		<cflock name="getFileSecurityRequirements" timeout="10" throwontimeout="true">

			<cfset var checkList = "index.cfm,webservices/user.cfc">
			<cfloop list = "#checkList#" index="local.file">
				<cfset var securityResult = testFileSecurity(scriptname = local.file, checkForCorruption = false, isLoggedIn = false, isInternal = false)>

				<cfif not securityResult.isOK>
					<cfset result = true>
					<cfbreak>
				</cfif>
			</cfloop>

			<!--- 	Lower Level Tests
					Some xPaths which should bring back a result and not error
			--->
			<cfset var checkList = "//relay,//code,//relay/webservices">
			<cfloop list = "#checkList#" index="local.path">
				<cftry>
					<cflock name="#xmlSearchLockName#" timeout="5" throwOnTimeOut=true>
						<cfset var XMLSearchResult = xmlsearch (application.XMLDocs.security.Doc.xmlroot,path)>
					</cflock>
					<cfif not arrayLen (XMLSearchResult)>
						<cfset result = true>
						<cfbreak>
					</cfif>
					<cfcatch>
						<cfset result = true>
						<cfbreak>
					</cfcatch>
				</cftry>
			</cfloop>
		</cflock>

		<cfreturn result>
	</cffunction>



	<!--- WAB 2016-01-12
	Removes any nodes from the XML if they have a testSiteAttribute which does not contain the current value
	--->
	<cffunction name="removeNodesFromXMLBasedOnTestSite">
			<cfargument name="xmlDoc" required="true" type="xml">
			<cfargument name="testSite" default = "#applicationScope.testSite#">

			<cfset var node = "">
			<cfset var xmlfunctions = createObject("component","com.xmlfunctions")>  <!--- must be done as a create object to overcome boot strap issues --->
			<cfset var XMLSearchResult = xmlsearch (xmlDoc,"//*[@testsite]")>

			<cfloop array="#XMLSearchResult#" index="node">
				<cfif not listFind(node.xmlAttributes.testSite,testSite)>
					<cfset xmlfunctions.deleteNode(node)>
				</cfif>
			</cfloop>

	</cffunction>


	<cffunction name="dealWithSlashes" output="false">
		<cfargument name="path">

		<cfreturn replace(replace(path,"\","/","ALL"),"//","/","ALL")>

	</cffunction>


	<cffunction name="convertScriptNameToStandardForm" output="false">
		<cfargument name="scriptName">

		<!---
			WAB 2016-01-12 Modified to use new function getTopLevelDirectoryNames() rather than a hard coded list of conditions
		--->

		<cfset var adjustedscriptName = dealWithSlashes(arguments.scriptName)>
		<cfset var topLevelDirectoryNames = getTopLevelDirectoryNames()>

		<!--- NJH 2010/08/24 LID 3920 - if scriptname doesn't come in with a leading slash and it's just a filename, then we end up with relayscriptname, rather then relay/scriptname --->
		<cfif left(adjustedscriptName,1) neq "/">
			<cfset adjustedscriptName = "/"&adjustedscriptName>
		</cfif>

		<cftry>
			<cfset adjustedscriptName = replacenocase(adjustedscriptName,dealWithSlashes(application.path),"")>
			<cfset adjustedscriptName = replacenocase(adjustedscriptName,dealWithSlashes(application.userfilesabsolutepath),"")>

			<cfif arrayFindNoCase(topLevelDirectoryNames,listFirst(adjustedscriptName,"/")) >
				<cfset adjustedscriptName = adjustedscriptName>
			<cfelseif listlen (adjustedscriptName,"/") gt 1 and arrayFindNoCase(topLevelDirectoryNames,listgetAt(adjustedscriptName,2,"/")) and listgetAt(adjustedscriptName,2,"/") is not  "relay">
				<cfset adjustedscriptName = listrest(adjustedscriptName,"/")>
			<cfelse>
				<cfset adjustedscriptName = "relay" & adjustedscriptName>
			</cfif>

			<!--- remove any leading / which might have crept in (for example if scriptName /relay/test/blah.cfm was passed in  --->
			<cfset adjustedscriptName = reReplaceNoCase(adjustedscriptName,"\A\/","")>

			<cfcatch>
				<cfoutput>#adjustedscriptName#</cfoutput>			<cfabort>
			</cfcatch>
		</cftry>

		<cfreturn lcase(adjustedscriptName)>
	</cffunction>

	<!--- 	WAB 2016-01-12 gets Array of the names of the top level directories (eg [relay,code,unittests,test ...]
			This is used in convertScriptNameToStandardForm() - where we used to have a hard coded list
			WAB 2016-02-26 Caching the array in same structure as XML
	--->
	<cffunction name="getTopLevelDirectoryNames" output="false" returnType = "array">
		<cfif not structKeyExists (application.XMLDocs.security,"topLevelDirectories")>
		<cfset var result = arrayNew(1)>
		<cfset var item = "">
			<cflock name="#xmlSearchLockName#" timeout="5" throwOnTimeOut=true>
		<cfloop array = #application.XMLDocs.security.Doc.xmlroot.xmlchildren# index = "item">
			<cfset arrayAppend (result,item.xmlname)>
		</cfloop>
			</cflock>
			<cfset application.XMLDocs.security.topLevelDirectories = result>
		</cfif>
		<cfreturn application.XMLDocs.security.topLevelDirectories>
	</cffunction>


	<cffunction name="makeSafeXMLSearchString" output="false">
		<cfargument name="searchString">
		<cfset var result = "">

		<cfset result = rereplaceNocase(searchString,"[^a-zA-Z0-9_\.\/]","_","ALL")>
		<!--- my xml format can't deal with file names with spaces, brackets, etc in them, have to be replaced with _ in the XML file --->
		<cfset result = rereplaceNocase(result,"\/\.","/_","ALL")>
		<!--- my xml format can't deal with file names starting with . so search for /. and replace   with /_--->

		<cfset result = rereplaceNocase(result,"\/([0-9])","/_\1","ALL")>


		<cfreturn result>

	</cffunction>


	<!---
	WAB 2011/11/15  added support for attributes encryptedVariables and nonencryptedVariables in the XML
	--->

	<cffunction name="getFileSecurityRequirements" output="false">
		<cfargument name="scriptName">
		<cfargument name="checkForCorruption" default = "true" hint="used to prevent infinite recursion when called by checkForCorruptedXMLAndReloadIfNecessary()">

		<cfset var directory = "">
		<cfset var directoryToTest = "">
		<cfset var fileNameToTest = "">
		<cfset var fileContent= "">
		<cfset var XMLSearchResult = "">
		<cfset var result = structNew()>
		<cfset var xmlnode = "">
		<cfset var xmlattributes = "">
		<cfset var done = {
							  login = false
							, internal=false
							, security=false
							, noaccess=false
							, nodebug=false
							, requiresXSRFToken=false
							, requiresPost=false
							}>
		<cfset var registeredChildren = "">
		<cfset var includeFile = "">
		<cfset var x = "">
		<cfset var filename = "">
		<cfset var xmlObj = "">
		<cfset var xPath = "">

		<cfset result = {
						  isOK= true
						, fileSpecific= false
						, allowTrusted = false
						, LoginRequired = 2
						, InternalRequired = true
						, security = ""
						, includeFiles = arrayNew(1)
						, XSSSafeVariables = ""
						, SQLSafeVariables = ""
						, encryptedVariables = ""
						, nonencryptedVariables = ""
						, checkVariableNames = "true"   <!--- WAB 2015-01-15 CASE 443444 Added support for new attribute --->
						, nodebug = "false"
						, noAccess = "false"
						, requiresXSRFToken = (cgi.request_method is "POST")?true:false
						, requiresPost = "false"
						, hasOwnSecurity = "false"
						, hasChildFolders =  false
						, hasChildFiles =  false

						, scriptName = convertScriptNameToStandardForm(scriptname)
					}>

		<cfset fileNameToTest = makeSafeXMLSearchString(result.scriptName)>
		<cfset directoryToTest = listAllButLast(fileNameToTest,"/")>
		<cfset result.thisDir = reReplaceNoCase(directoryToTest,"\ARelay\/","/")>

		<cftry>

			<cflock name="getFileSecurityRequirements" timeout="10" throwontimeout="true">

				<!--- Search for the actual file in the xml structure --->
				<cfset xPath = "/directorysecurity/#fileNameToTest#">
				<cflock name="#xmlSearchLockName#" timeout="5" throwOnTimeOut=true>
					<cfset XMLSearchResult = xmlsearch (application.XMLDocs.security.Doc.xmlroot,xPath)>
				</cflock>

				<cfif arrayLen(XMLSearchResult) is 0> <!--- file does not exist in the xml structure--->
					<!--- Now look for the directory --->

					<cfset xPath = "/directorysecurity/#directoryToTest#">
					<cflock name="#xmlSearchLockName#" timeout="5" throwOnTimeOut=true>
						<cfset XMLSearchResult = xmlsearch (application.XMLDocs.security.Doc.xmlroot,xPath)>
					</cflock>

					<cfif arrayLen(XMLSearchResult) is not 0>
						<cfset xmlnode = XMLSearchResult[1]>
						<cfset xmlattributes = xmlnode.xmlattributes>
						<cfif structKeyExists (xmlattributes,"namedFilesOnly") and xmlattributes.namedFilesOnly>
							<cfset result.isoK = false>
							<cfset result.message = "File Not Registered">
							<cfset result.failureCode = "FileNotRegistered">
						</cfif>
						<!--- check for child files and folders --->
						<cfset registeredChildren = getRegisteredFilesAndFoldersInANode(xmlnode)>
						<cfset result.childFilesArray = registeredChildren.filesArray>
						<cfset result.childFoldersArray = registeredChildren.foldersArray>
						<cfset result.hasChildFolders =  iif (arrayLen(result.childFilesArray),true,false)>
						<cfset result.hasChildFiles =  iif (arrayLen(result.childFoldersArray),true,false)>


					<cfelse>
						<!--- loop until find something --->
						<cfloop condition= "arrayLen(XMLSearchResult) is  0 and xPath is not '/directorysecurity'">
							<cfset xPath = listAllButLast(xPath,"/")>
							<cflock name="#xmlSearchLockName#" timeout="5" throwOnTimeOut=true>
								<cfset XMLSearchResult = xmlsearch (application.XMLDocs.security.Doc.xmlroot,xPath)>
							</cflock>
						</cfloop>

						<cfif arrayLen(XMLSearchResult) is not 0>
							<!--- a parent directory found, does it allow unregistered directories?--->
							<cfset xmlnode = XMLSearchResult[1]>
							<cfset xmlattributes = xmlnode.xmlattributes>
							<cfif not structKeyExists (xmlattributes,"allowAllSubdirectories") or not xmlattributes.allowAllSubdirectories >
								<cfset result.isoK = false>
								<cfset result.message = "Directory Not Registered">
								<cfset result.failureCode = "DirectoryNotRegistered">
							</cfif>

						<cfelse>
							<!--- Nothing found --->
								<cfset result.isoK = false>
								<cfset result.message = "Directory Not Found">
								<cfset result.failureCode = "DirectoryNotRegistered">

						</cfif>



					</cfif>

				<cfelse><!--- Named file found in xml structure , carry on --->
					<cfset result.fileSpecific= true>
				</cfif>

				<!---
				Special code to deal with secure file library
				These stub files are in directories which are not registered but need to be accessible.
				All rights are dealt with by the secure file library functions
				However they all have names of form   xxxxxxxxxx.abc.cfm
				We can also check that all that is in them is comments, so no possibility of slipping in a real file into a secured directory
				If it
				--->
				<cfif not result.isOK and result.failureCode is "DirectoryNotRegistered">
					<cfset filename = listlast(scriptname,"/")>
					<!--- 2009/08/14 GCC was "is 3" which meant race inefered in file library if a secure file had a dot in the name other than before the extension... --->
					<cfif listLen(filename,".") gte 3 and fileExists(scriptname)>
						<!--- read the content --->
						<cffile file="#scriptname#" action="read" variable = "filecontent">
						<!--- strip out the comments --->
						<cfset filecontent = rereplacenocase (filecontent,"<!---.*?--->","","ALL")>
						<cfset filecontent = rereplacenocase (filecontent,"\s","","ALL")>
						<cfif len(filecontent) lt 2> <!--- 2 is a random small number just in case some odd character not filtered out --->
							<cfset result.isoK = true>
							<cfset result.LoginRequired = 0>
							<cfset result.internalRequired = false>
							<cfset result.failurecode = "">
						</cfif>
					</cfif>
				<cfelse>

					<cfif arrayLen(XMLSearchResult) is  0>
						<cfset result.isoK = false>
						<cfset result.message = "Directory Not Registered">
						<cfset result.failureCode = "DirectoryNotRegistered">

					<cfelseif structKeyExists(XMLSearchResult[1].xmlattributes,"hasOwnSecurity")>
						<cfset result.hasOwnSecurity = true>

					<cfelse>
						<cfset xmlnode = XMLSearchResult[1]>

						<cfloop condition= "xmlnode.xmlname is not 'directorysecurity'">  <!--- this is the top of the xml --->
							<cfset xmlattributes = xmlnode.xmlattributes>
							<cfif structKeyExists (xmlnode.xmlattributes,"allowTrusted") and xmlattributes.allowTrusted>
								<cfset result.allowTrusted = true>
								<cfif structKeyExists (xmlnode.xmlattributes,"trustedIPs")>
									<cfset result.trustedIPs = xmlnode.xmlattributes.trustedIPs>
								<cfelse>
									<cfset result.trustedIPs = "">
								</cfif>
								<!---  WAB 2011/07/08 seeing whether we can have allowTrusted OR Logged In
									while there was a bug in the isTrusted code we didn't notice that this was the case
									NEEDS FULL TESTING
								<cfset result.LoginRequired= 99> <!--- this would prevent any logged in user accessing the directory --->
								<cfset Done.Login = true>
								<cfbreak>
								--->
							</cfif>

							<cfif not Done.NoAccess and structKeyExists (xmlnode.xmlattributes,"noAccess") >
								<cfif xmlattributes.noAccess>
									<cfset result.noAccess = true>
									<!---
									<cfset result.isOK = false>
									<cfset result.message = "No Access Allowed">
									<cfset result.failureCode = "NoAccess">
									<cfbreak>
									--->
								<cfelse>
									<cfset Done.NoAccess = true>  <!--- this allows NoAccess on a directory to be overriden by a child page--->
								</cfif>
							</cfif>

							<cfif structKeyExists (xmlnode.xmlattributes,"XSSSafeVariables") >
								<cfset result.XSSSafeVariables = listappend(result.XSSSafeVariables,xmlnode.xmlattributes.XSSSafeVariables,"|")>
							</cfif>

							<cfif structKeyExists (xmlnode.xmlattributes,"SQLSafeVariables") >
								<cfset result.SQLSafeVariables = listappend(result.SQLSafeVariables,xmlnode.xmlattributes.SQLSafeVariables,"|")>
							</cfif>

							<cfif structKeyExists (xmlnode.xmlattributes,"encryptedVariables") >
								<cfset result.encryptedVariables = listappend(result.encryptedVariables,xmlnode.xmlattributes.encryptedVariables,"|")>
							</cfif>

							<cfif structKeyExists (xmlnode.xmlattributes,"nonencryptedVariables") >
								<cfset result.nonencryptedVariables = listappend(result.nonencryptedVariables,xmlnode.xmlattributes.nonencryptedVariables,"|")>
							</cfif>

							<!--- WAB 2015-01-15 CASE 443444 Added support for new attribute --->
							<cfif structKeyExists (xmlnode.xmlattributes,"checkVariableNames") and isBoolean(xmlnode.xmlattributes.checkVariableNames) >
								<cfset result.checkVariableNames = xmlnode.xmlattributes.checkVariableNames>
							</cfif>

							<cfif not Done.Login and structKeyExists (xmlnode.xmlattributes,"LoginRequired")>
								<cfset result.LoginRequired= xmlattributes.LoginRequired>
								<cfset Done.Login = true>
							</cfif>
							<cfif not done.Internal and structKeyExists (xmlnode.xmlattributes,"internalUser")>
								<cfset result.InternalRequired = xmlattributes.internalUser>
								<cfset done.Internal = true>
							</cfif>
							<cfif not done.security and structKeyExists (xmlattributes,"security") and xmlattributes.security is not "">
								<cfset result.security = xmlattributes.security>
								<cfset done.security = true>
							</cfif>
							<cfif not done.nodebug and structKeyExists (xmlattributes,"nodebug") and xmlattributes.nodebug is not "">
								<cfset result.nodebug = xmlattributes.nodebug>
								<cfset done.nodebug = true>
							</cfif>

							<cfif not done.requiresXSRFToken and structKeyExists (xmlattributes,"requiresXSRFToken")>
								<cfset result.requiresXSRFToken = xmlattributes.requiresXSRFToken>
								<cfset done.requiresXSRFToken = true>
							</cfif>

							<cfif not done.requiresPost and structKeyExists (xmlattributes,"requiresPost")>
								<cfset result.requiresPost = xmlattributes.requiresPost>
								<cfset done.requiresPost = true>
							</cfif>

							<cfif structKeyExists (xmlattributes,"include") and xmlattributes.include is not "">
								<cfset includeFile = xmlattributes.include>
								<!--- if an absolute path then will start with / or \ otherwise we need to add on the path to where we are --->
								<cfif refindNoCase ("\A[\\\/]",includeFile)>

								<cfelse>
									<cfset xmlObj = application.com.xmlfunctions>
									<cfset x = xmlObj.getXMLParentList(xmlnode = xmlnode,delimiter = "/", stopat= "directorysecurity", appendSelf=true)>
									<cfset includefile = "/" & x & "/" & includefile>
								</cfif>
								<cfset arrayAppend(result.includeFiles,includeFile)>
							</cfif>

							<cfset xmlnode = xmlnode.xmlparent>
						</cfloop>

					</cfif>
				</cfif>

			</cflock>

			<cfcatch>
				<!--- 	WAB 2016-02-26
						Modified 2016-06-16 to always reload on the XML error
						If we get an error, log it
						If it is the known XML error then do a reload, otherwise check for corruption
						Then try again (once)
				--->
				<cfset application.com.errorhandler.recordRelayError_ColdFusion(error=cfcatch)>
				<cfif checkForCorruption> <!--- this prevents infinite loop --->
					<cfif cfcatch.message contains "Unable to process the result of the XMLSearch">
						<cfset reloadCorruptedXML (reference = scriptname, debugStruct = cfcatch)>
					<cfelse>
						<cfset checkForCorruptedXMLAndReloadIfNecessary(reference = scriptname, debugStruct = cfcatch)>
					</cfif>
						<cfset result = getFileSecurityRequirements (argumentCollection = arguments, checkForCorruption = false)>
				<cfelse>
				<cfset result.isoK = false>
					<cfset result.message = "Test Failed #cfcatch.message#">
					<cfset result.failureCode = "ErrorOccured">
				</cfif>

			</cfcatch>
		</cftry>



		<cfreturn result>
	</cffunction>


	<cffunction name="testFileSecurity" output="false">
		<cfargument name="scriptName">
		<cfargument name="isLoggedIn" default = "#request.relayCurrentUser.isLoggedin#">
		<cfargument name="isInternal" default = "#request.relayCurrentUser.isInternal#">
		<cfargument name="testSecurityLevel" default = "true">
		<cfargument name="checkForCorruption" default = "true" hint="used to prevent infinite recursion">

		<cfset var result = "">
		<cfset var fileSecurityRequirements = getFileSecurityRequirements (scriptName = scriptName, checkForCorruption = checkForCorruption)>

		<cfset result = applySecurityRequirements(securityRequirements = fileSecurityRequirements,argumentCollection = arguments)>


		<!--- 	WAB 2015-03-17 CASE 444099
				Attempt to automatically detect and deal with with security XML getting corrupted.
				If we get a failureCode "DirectoryNotRegistered" then check for XML corruption
		--->
		<cfif result.isOK is false and checkForCorruption and (result.failureCode is "DirectoryNotRegistered" or result.failureCode is "FileNotRegistered")>
			<cfset var checkResult = checkForCorruptedXMLAndReloadIfNecessary(scriptname)>
			<cfif checkResult is "reloaded">
				<cfset result = testFileSecurity (argumentCollection = arguments, checkForCorruption = false)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="applySecurityRequirements" output="false">
		<cfargument name="securityRequirements">
		<cfargument name="isLoggedIn" default = "#request.relayCurrentUser.isLoggedin#">
		<cfargument name="isInternal" default = "#request.relayCurrentUser.isInternal#">
		<cfargument name="testSecurityLevel" default = "true">

		<cfset var securityTask = "">
		<cfset var securityLevel = "">
		<cfset var result = securityRequirements>
		<cfset var encryptionCheckResult = "">
		<cfset var warningStruct = {}>
		<cfset var injectionCheckResult = "">


		<cfif securityRequirements.hasOwnSecurity>
			<cfreturn result>
		</cfif>
		<cfif result.isOK>
			<!--- check noAccess --->
			<cfif result.noAccess >
				<cfset result.isOK = false>
				<cfset result.message = "No Access Allowed">
				<cfset result.failureCode = "NoAccess">
			</cfif>
		</cfif>


		<cfif result.isOK>
			<!--- check internal--->
			<cfif result.InternalRequired and not isInternal>
				<cfset result.isOK = false>
				<cfset result.message = "Must Be Internal User">
				<cfset result.failureCode = "InternalUserRequired">
			</cfif>
		</cfif>


		<cfif result.isOK>
			<!--- check login --->
			<cfif result.loginRequired gt isLoggedin>
				<cfif isLoggedin is 1>
					<cfset result.isOK = false>
					<cfset result.message = "Full Login Required">
					<cfset result.failureCode = "FullLoginRequired">
				<cfelse>
					<cfset result.isOK = false>
					<cfset result.message = "Login Required">
					<cfset result.failureCode = "LoginRequired">
				</cfif>
			</cfif>
		</cfif>

		<cfif result.isOK>
			<!--- check security
			Note that this is currently coded for a single security test.
			For backwards compatability we make the checkPermission query available to the called template
			We could have security expressions (ie ANDS or ORS) but in this case would not bring back the checkpermission query
			--->
				<cfif result.security is not "" and result.security is not "none">
					<cfset securityTask = listFirst (result.security,":")>
					<cfset securityLevel = listLast (result.security,":")>
					<cfset result.checkPermission = application.com.login.checkInternalPermissions (securityTask,"",securityLevel)>
					<cfif testSecurityLevel and result.checkPermission.RecordCount IS 0>
						<cfset result.isoK = false>
						<cfset result.message = "Must have rights #result.security#">
						<cfset result.failureCode = "InsufficientRights">
					</cfif>
				</cfif>

		</cfif>

		<!--- if we have failed then test whether trusted is allowed --->
		<!--- WAB 2012-12-04 CASE 438291 Deal change to use getRemoteAddress () --->
		<cfif not result.isOK and result.AllowTrusted>
			<!--- will it allow trusted? --->
			<!--- WAB 2011/06/08 Found a major flaw here, if result.TrustedIPs was blank, the reFind always returned 1 (ie true), so all IP addresses were considered trusted --->
			<!--- WAB 2014-09-09  Add support for TrustedIPs  to hold list of network ranges in CIDR format --->
			<cfif (isIpAddressTrusted() or  (result.TrustedIPs is not "" and testTrustedIPsString(result.TrustedIPs,getRemoteAddress()) ))>
				<!--- it's ok its a trusted site --->
				<cfset result.isOK = true>
			<!---
			I now allow loggedin OR trusted so this is not required
			For trusted ONLY then use attributes NOACCESS and ALLOWTRUSTED together
			<cfelseif result.loginRequired is 99>
				<!--- for a trusted site we have set login required to be 99 to prevent a logged in user getting here
					if this is the reason for failure, we need to change the failure code to notTrusted
				 --->
				<cfset result.message = "Not Trusted">
				<cfset result.failureCode = "NotTrusted">
			--->
			</cfif>
		</cfif>

		<cfif result.isOK>
			<!--- Check XSRF token
				WAB 2013-01-09 CASE 433123 Do not need to check for blind POSTing if the user is not logged in since there is no XSRF danger (XSRF relies on hijacking an already authenicated session for nefarious purposes)
			--->
			<cfif (result.requiresXSRFToken AND isLoggedIn and NOT isXSRFTokenPresentAndValid())  >
				<cfset result.isOK = false>
				<cfset result.message = "Session Expired (2)">
				<cfset result.failureCode = "XSRFTokenFailure">
				<cfif structKeyExists(form,"_rwSessionToken") and application.testSite is "2">
					<cfset result.message = result.message  & "<br />#form._rwSessionToken# should be #getsessionToken()#.">
				</cfif>

			</cfif>
		</cfif>

		<cfif result.isOK>
			<!--- Check Requires Post
			--->
			<cfif (result.requiresPost and cgi.request_method is not "POST") >
				<cfset result.isOK = false>
				<cfset result.message = "Invalid Request (1)">
				<cfset result.failureCode = "RequiresPost">
			</cfif>
		</cfif>

		<cfif result.isOK>
			<!--- Check Encryption --->
			<cfset encryptionCheckResult = confirmFieldsHaveBeenEncryptedWithFeedback(fieldnames = result.encryptedVariables, nonEncryptedFieldNames = result.nonEncryptedVariables, regexp = true)>

			<cfif not encryptionCheckResult.isOK>
				<cfset result.isOK = false>
				<cfset result.message = "Parameter Error 1">
				<cfset result.failureCode = "EncryptionError">
				<cfset result.encryptionCheckResult = encryptionCheckResult>

			</cfif>
		</cfif>

		<!--- SQL/XSS Injection Checks --->
		<cfif result.isOK>

			<!---
			WAB 2015-01-15 CASE 443444 Problems with missingTemplateHandler because the queryString gives dodgy variables in the URL collection
			Replace our previous hack (which failed when the queryString contained an & and generated more than one variable in the URL collection)
			Instead, added a new checkVariableNames attribute
			--->

			<cfset injectionCheckResult = checkForSQLInjectionAndXSS(XSSSafeVariables = result.XSSSafeVariables,SQLSafeVariables = result.SQLSafeVariables, checkVariableNames = result.checkVariableNames, isInternal = arguments.isInternal)>

				<cfif not injectionCheckResult.isOK>
					<cfset result.isOK = false>
					<cfset result.message = "Parameter Error">
					<cfset result.failureCode = "InjectionError">
					<cfset result.injectionCheckResult = injectionCheckResult>
					<cfset warningStruct.xssResult = result>
					<cfset warningStruct.script_name = cgi.script_name>
					<cfset application.com.errorhandler.recordRelayError_Warning(severity="SECURITY",Type="Injection",message = "Injection Problem.",WarningStructure = warningStruct)>

					<!--- NJH 2009/06/29 - turn debug when error encountered, as the debug can cause a script to be run --->
					<cfsetting showdebugoutput="false">
				</cfif>

		</cfif>




		<!---  no good including files here because variables go into the wrong scope--->
		<cfreturn result>
	</cffunction>

	<cffunction name="getCFCMethodSecurityRequirements" output="false">
		<cfargument name="methodPointer">

		<cfset var result = {}>
		<cfset var methodMetaData = getMetaData (methodPointer)>
		<cfset var fullKey	= '' />
		<cfset var key	= '' />
		<cfset var thisParameter	= '' />
		<!---
		Look for
			allowTrusted
			LoginRequired
			InternalRequired (from internalUser)
			security
			requiresXSRFToken
			requiresPost

			XSSSafeVariables
			SQLSafeVariables
			encryptedVariables
			nonencryptedVariables
		 --->
			<cfloop list="LoginRequired,internalRequired,security,requiresXSRFToken,requiresPost" index="key">
				<cfif structKeyExists (methodMetaData,key)>
					<cfset result[key] = methodMetaData[key]>
				</cfif>
			</cfloop>

			<!---
				this loop looks for XSSSafeVariables,SQLSafeVariables etc as attributes of the method (ie identical syntax to that in directorySecurity.xml)
				WAB 2013-03-25 Case 433419 Added this loop so that variables which were nots arguments could be allowed tested

			<cfloop list="XSSSafe,SQLSafe,encrypted,nonencrypted" index="key">
				<cfset fullKey = '#key#Variables'>
				<cfif structKeyExists(methodMetaData,fullKey)>
					<cfparam name="result[fullKey]" default = "">
					<cfset result[fullKey] = listappend(result[fullKey],methodMetaData[fullKey],"|")>
				</cfif>
			</cfloop>
			--->

			<!--- this loop looks for XSSSafe,SQLSafe etc as attributes of individual arguments --->
			<cfloop index="thisParameter" array="#methodMetaData.parameters#">
				<cfloop list="XSSSafe,SQLSafe,encrypted,nonencrypted" index="key">
					<cfif structKeyExists(thisParameter,key)>
						<cfset fullKey = '#key#Variables'>
						<cfparam name="result[fullKey]" default = "">
						<cfset result[fullKey] = listappend(result[fullKey],thisParameter.name,"|")>
					</cfif>
				</cfloop>
			</cfloop>

			<cfset result.methodname = methodMetaData.name>

		<cfreturn result>

	</cffunction>


	<cffunction name="getCFCFileAndMethodSecurityRequirements" output="false">
		<cfargument name="componentPath">
		<cfargument name="methodPointer">

		<cfset var fullKey	= '' />
		<cfset var key	= '' />
		<cfset var methodSecurityRequirements = getCFCMethodSecurityRequirements (methodPointer = methodPointer)>
		<cfset var securityRequirement = getFileSecurityRequirements(scriptname = componentPath)>

		<cfloop list="LoginRequired,internalRequired,security,requiresXSRFToken,requiresPost" index="key">
			<cfif structKeyExists (methodSecurityRequirements,key)>
				<cfset securityRequirement[key] = methodSecurityRequirements[key]>
			</cfif>
		</cfloop>

		<cfloop list="XSSSafe,SQLSafe,encrypted,nonencrypted" index="key">
			<cfset fullKey = '#key#Variables'>
			<cfif structKeyExists (methodSecurityRequirements,fullkey)>
				<cfset securityRequirement[fullkey]= listappend(securityRequirement[fullkey],methodSecurityRequirements[fullkey])>
			</cfif>
		</cfloop>

		<cfset securityRequirement.methodname = methodsecurityRequirements.methodname>

		<cfreturn securityRequirement>

	</cffunction>


	<!---  WAB 2011/10/19 added this new method
		looks at the metadata of a CFC function and checks for security
		currently just checks security, but could do login as well
		used by webservices\callWebservice.cfc
		WAB 2011/11/16 added support for testing encryption of arguments
		WAb 2012-02-22 Added support for internalUser and LoginRequired
	--->
	<cffunction name="testCFCMethodSecurity">
		<cfargument name="methodPointer">
		<cfargument name="isLoggedIn" default = "#request.relayCurrentUser.isLoggedin#">
		<cfargument name="isInternal" default = "#request.relayCurrentUser.isInternal#">


		<cfset var securityTask = "">
		<cfset var securityLevel = "">
		<cfset var checkPermission = "">
		<cfset var result = {isOK = true,message="",failureCode=""}>
		<cfset var parametersWhichNeedEncrypting = "">
		<cfset var testEncryption = "">
		<cfset var thisParameter= "">
		<cfset var methodMetaData = getMetaData (methodPointer)>

		<cfif structKeyExists (methodMetaData,"LoginRequired")>
			<cfif arguments.isLoggedIn lt methodMetaData.LoginRequired>
				<cfset result.isoK = false>
				<cfset result.message = "Login Required">
				<cfset result.failureCode = "LoginRequired">

			</cfif>
		</cfif>

		<cfif result.isoK and structKeyExists (methodMetaData,"internalUser")>
			<cfif methodMetaData.internalUser and not arguments.isInternal>
				<cfset result.isoK = false>
				<cfset result.message = "Must Be Internal User">
				<cfset result.failureCode = "InternalUserRequired">
			</cfif>
		</cfif>

		<cfif result.isoK and structKeyExists (methodMetaData,"security")>
				<cfif methodMetaData.security is not "" and methodMetaData.security is not "none">
					<cfset securityTask = listFirst (methodMetaData.security,":")>
					<cfset securityLevel = listLast (methodMetaData.security,":")>
					<cfset checkPermission = application.com.login.checkInternalPermissions (securityTask,"",securityLevel)>
					<cfif checkPermission.RecordCount IS 0>
						<cfset result.isoK = false>
						<cfset result.message = "You do not have sufficient rights">
						<cfset result.failureCode = "InsufficientRights">
					</cfif>
				</cfif>
		</cfif>

		<cfif result.isOK and structKeyExists (methodMetaData,"requiresPost")>
			<cfif cgi.request_method is not "POST" >
				<cfset result.isOK = false>
				<cfset result.message = "Invalid Request (2)">
				<cfset result.failureCode = "RequiresPost">
			</cfif>
		</cfif>

		<cfif result.isoK >
			<cfloop index="thisParameter" array="#methodMetaData.parameters#">
				<cfif structKeyExists(thisParameter,"encrypted")>
					<cfset parametersWhichNeedEncrypting = listappend(parametersWhichNeedEncrypting,thisParameter.name)>
				</cfif>
			</cfloop>

			<cfset testEncryption = confirmFieldsHaveBeenEncryptedWithFeedback(fieldnames = parametersWhichNeedEncrypting, regExp = false)>
			<cfif not testEncryption.isOK>
				<cfset result.isOK = false>
				<cfset result.message = "Parameter Error 3">
				<cfset result.failureCode = "ParameterError">
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

	<!--- Needed to be able to run include files which set all sorts of local variables --->
	<cffunction name="doIncludeFiles">
		<cfargument name="includeFilesArray">
		<cfargument name="variablesScope">

		<cfset var i = 0>
		<cfset var thisIncludeFile = "">
		<cfset var variableName = "">

		<cfloop index="i" from = "1" to = #arrayLen(includeFilesArray)#>
				<cfset thisIncludeFile = includeFilesArray[i]>
				<cftry>
						<cfinclude template = "#thisIncludeFile#">
						<cfcatch>
						</cfcatch>
				</cftry>
		</cfloop>

		<cfloop item="variableName" collection = "#variables#">
			<cfset arguments.variablesScope[variableName] = variables[variableName]>
		</cfloop>

	</cffunction>


	<cffunction name="getRegisteredFilesAndFoldersInADirectory" access="public" returntype="struct" output="false">
		<cfargument name="scriptName">

		<cfset var result = structNew()>
		<cfset var i = 0>
		<cfset var itemToTest = "">
		<cfset var XMLSearchResult = "">


		<cfset result.scriptName = convertScriptNameToStandardForm(scriptname)>
		<cfset result.filesArray = arrayNew(1)		>
		<cfset result.foldersArray = arrayNew(1)		>

		<cfset itemToTest = makeSafeXMLSearchString(result.scriptName)>
			<!--- Search for the actual file in the xml structure --->
		<cftry>
			<cfset XMLSearchResult = xmlsearch (application.XMLDocs.security.Doc.xmlroot,"//#itemToTest#")>
			<cfif arrayLen(XMLSearchResult) is not 0>
					<cfset result = getRegisteredFilesAndFoldersInANode (XMLSearchResult[1])>
			</cfif>
			<cfcatch>
				<cfoutput>#itemToTest#</cfoutput>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>


	<cffunction name="getRegisteredFilesAndFoldersInANode" access="public" returntype="struct" output="false">
		<cfargument name="XMLNode">

		<cfset var result = structNew()>
		<cfset var i = 0>
		<cfset result.filesArray = arrayNew(1)>
		<cfset result.foldersArray = arrayNew(1)>

				<cfloop index="i" from = "1" to = #arrayLen(xmlnode.xmlchildren)#>
					<cfif xmlnode.xmlchildren[i].xmlname contains ".c">
						<cfset arrayAppend(result.filesArray,xmlnode.xmlchildren[i].xmlname)>  <!--- scriptName & "/" & --->
					<cfelse>
						<cfset arrayAppend(result.foldersArray,xmlnode.xmlchildren[i].xmlname)>
					</cfif>
				</cfloop>

		<cfreturn result>

	</cffunction>

	<!--- WAB 2012-12-04 CASE 438291 Deal change to use getRemoteAddress () --->
	<cffunction name="isIpAddressTrusted" access="public" returntype="boolean" output="false">
		<cfargument name="IPAddress" default="#getRemoteAddress()#">

		<!--- WAB 2012-02-21 added a check for server calling itself, but the remote_addr has been NAT'ed by the datacentre firewall
			WAB 2012-05-10 moved getHostAddress code to another function with proper error handling
		--->

			<cfif
					isIpAddressRelayWareInc (ipaddress = arguments.ipaddress)
						or
					arguments.ipaddress is getServerIPAddress() <!--- calling itself --->
 						or
					arguments.ipaddress is cgi.http_host <!--- call itself --->
						or
					arguments.ipaddress is getHostAddress(cgi.http_host) <!--- This is a call which has gone to the datacentre firewall and come back with a NAT'ed address--->
						or
					applicationscope.com.clusterManagement.isThisIPAddressInCluster(ipaddress = arguments.ipaddress,applicationscope = applicationscope) <!--- WAB 2009/10/15 added other servers in cluster, 2009/10/27 modified listfind to listcontains so that can handle port numbers in otherservers in cluster--->
						or
					arguments.ipaddress is "::1" <!--- WAB 2016-11-03 a call itself (found on a CF2016 install)--->

				>
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>

	</cffunction>


	<!--- WAB 2014-09-09  New function to test whether an IPAddress lies in range specified in the TrustedIPs string
		TrustedIps string may now be either a plain regular expression or a list of ranges in CIDR format
 	--->
	<cffunction name="testTrustedIPsString" access="public" returntype="boolean" output="false">
		<cfargument name="trustedIPs" required="true">
		<cfargument name="IPAddress" required="true">

		<cfset var result = false>
		<cfset var trustedIPRange = "">

		<!--- TrustedIps can be a regular expression or a list of IP/CIDRs
			If contains number/number then must be a list of ranges in in CIDR format
		--->
		<cfif refindNoCase ("[0-9]/[0-9]",trustedIPs)>
			<!--- loop over each range until we find one that is contains the given IP address, then break --->
			<cfloop list="#trustedIPs#" index="trustedIPRange" delimiters="|,">
				<cfset result = checkIPAddressInRangeCIDR(IpAddress,trustedIPRange)>
				<cfif result>
					<cfbreak>
				</cfif>
			</cfloop>

		<cfelse>
			<!--- TrustedIPs is a regular expression --->
			<cfset result = reFindNoCase(TrustedIPs,IpAddress)>
		</cfif>

		<cfreturn result>

	</cffunction>



	<!--- WAB 2012-05-10, Code to get IPAddress of a host with proper handling of non existent hosts --->
	<cffunction name="getHostAddress" access="public" returntype="string" output="false" hint="returns IP Address of a hostname.  Blank returned if not found">
		<cfargument name="hostname" required="true">

		<cfset var iaddrClass = createObject("java","java.net.InetAddress")>
		<cfset var result = "">
		<cftry>
			<cfset result = iaddrClass.getByName(arguments.hostname).getHostAddress()>
			<cfcatch>
				<!--- If host is not found (or is an IP address) then call will result in an error --->
				<!--- If we were passed an IPAddress then return it, otherwise return blank --->
					<cfif refind("\A[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\Z",hostname)>
						<cfset result = hostname>
					</cfif>
			</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>


	<!--- WAB 2012-12-04 CASE 438291 Deal change to use getRemoteAddress () --->
	<cffunction name="isIpAddressRelayWareInc" access="public" returntype="boolean" output="false" hint="decides whether an IPAddress is a Relayware Inc IP">
		<cfargument name="IPAddress" default="#getRemoteAddress()#">

		<cfset var relayWareBreakOutRegExp = applicationScope.com.settings.getSetting(variablename = "relaywareInc.breakoutRegexp")>

		<cfif applicationScope.testsite>
			<cfset relayWareBreakOutRegExp = listAppend(relayWareBreakOutRegExp,	applicationScope.com.settings.getSetting(variablename = "relaywareInc.internalNetworkRegExp"),"|")>
		</cfif>

		<cfreturn reFind(relayWareBreakOutRegExp,ipaddress)>

	</cffunction>


	<cffunction name="getServerIPAddress" access="public" returntype="string" output="false">

				<cfset var iaclass=CreateObject("java", "java.net.InetAddress") >
				<cfset  var addr=iaclass.getLocalHost()>
				<cfreturn  addr.getHostAddress()>

	</cffunction>


	<cffunction name="areIpAddressesOnSameSubnet" access=private returnType="boolean">
		<cfargument name = "IPAddress1" required="true">
		<cfargument name = "IPAddress2" required="true">
		<cfargument name = "subnetMask" default="255.255.255.0">

		<cfset var result = true>
		<cfset var i = 0>

			<cfloop index="i" from ="1" to = "4">
				<cfif bitAND (listgetat(ipaddress1,i,"."), listgetat(subNetMask,i,".")) is not bitAND (listgetat(ipaddress2,i,"."), listgetat(subNetMask,i,".")) >
					<cfset result = false>
					<cfbreak>
				</cfif>
			</cfloop>

		<cfreturn result>

	</cffunction>

	<!---
	checkIPAddressInRange
	WAB 2011/06/29 (with eddie)
	Given a networkAddress and a mask, works out whether the given address is on the subnet
	(the networkAdress is the first address in the range)

	#checkIPAddressInRange ("195.212.199.35","195.212.199.32","255.255.255.224")#
	#checkIPAddressInRange ("195.212.199.31","195.212.199.32","255.255.255.224")#
	#checkIPAddressInRange ("195.212.199.65","195.212.199.32","255.255.255.224")#
	#checkIPAddressInRange ("195.212.199.63","195.212.199.32","255.255.255.224")#

	 --->

	<cffunction name="checkIPAddressInRange">
		<cfargument name="IPaddress">
		<cfargument name="networkAddress">
		<cfargument name="subnetmask">

		<cfset var result = true>
		<cfset var ipaddress_bits = '' />
		<cfset var networkAddress_bits = '' />
		<cfset var subnetmask_bits = '' />
		<cfset var counter = '' />

		<cfloop index="counter" from="1" to ="4">
			<cfset ipaddress_bits = listgetat(ipaddress,counter,".")>
			<cfset networkAddress_bits= listgetat(networkAddress,counter,".")>
			<cfset subnetmask_bits = listgetat(subnetmask,counter,".")>

			<cfif networkAddress_bits is not  bitand (ipaddress_bits,subnetmask_bits) >
				<cfset result = false>
				<cfbreak>
			</cfif>

		</cfloop>


		<cfreturn result>
	</cffunction>

	<!--- 	WAB 2014-09-09
		Rather a hurried function to deal with network ranges specified in CIDR notation
		Only deals with CIDR > 24 (but that should be fine for us)
		Just calls the existing checkIPAddressInRange function (which in reality probably immeadiately reverses some of the calculations!)
		Later converted to operate on a list or array of network addresses (released for WAB CASE 443133 2014-12-18)
	 --->
	<cffunction name="checkIPAddressInRangeCIDR">
		<cfargument name="IPaddress">
		<cfargument name="networkAddressAndCIDR" hint="Network address (or list or array of them) in CIDR notation  000.000.000.000/nn, or just a single IP address">

		<cfset var result = false>
		<cfset var range = "">

		<cfif isSimpleValue (networkAddressAndCIDR)>
			<cfset arguments.networkAddressAndCIDR = listToArray (networkAddressAndCIDR,",|")>
		</cfif>

		<cfloop array = "#networkAddressAndCIDR#" index="range" >
			<cfset var networkAddress = listFirst(range,"/")>
			<cfif listLen (range,"/") GT 1>
				<cfset var CIDR = listLast(range,"/")>
			<cfelse>
				<!--- if there is no CIDR then just set to 32 - a single IP address --->
				<cfset var CIDR = 32>
			</cfif>

			<cfif CIDR GT 32 OR CIDR LT 24>
				<cfset result = false>
			<cfelse>
				<cfset var LastQuartetMask = 256 - (2 ^ (32 - CIDR))>
				<cfset result = checkIPAddressInRange (ipAddress,networkAddress, "255.255.255." & LastQuartetMask)>
				<cfif result is true>
					<cfbreak>
				</cfif>
			</cfif>

		</cfloop>

		<cfreturn result>
	</cffunction>



	<!---
	Code used for protecting from form tampering
	Used in cf_encryptHiddenFields
	2009/09/14 WAB for LID 2625 added a dontRemoveRegExp attribute and a excludeVariableNamesRegExp to make it easier to deal with encrypting all fields except a few defined items
	2012-02-08 WAB for Security Project, changed regular expression to use one of my standard ones in com.regExp - could no longer guarantee that the attributes would appear in the order required for my existing regExps
	 --->
	<cfset this.dontRemoveHiddenFieldsStandardRegExp = "_required|_date|_eurodate">

	<cffunction name="encryptHiddenFields" access="public" returntype="struct" output="false">
		<cfargument name="contentString" required=true>
		<cfargument name="variableNamesRegExp" default = ""><!--- if only want certain variables encrypted then define a regular expression of their names --->
		<cfargument name="excludeVariableNamesRegExp" default = ""> <!--- can exclude certain variables from the encryption --->
		<cfargument name="remove" default = "true">
		<cfargument name="removeRegExp" default = "">  <!--- if only want certain variables removed from the source then define a regExp --->
		<cfargument name="dontRemoveRegExp" default = ""> <!--- don't want these moved   WAb 2009/09/14--->
		<cfargument name="debug" default = "false">
		<cfargument name="warningMode" default = "false"><!--- doesn't prevent a page running, but issues a warning by email --->
		<cfargument name="useformstate" default = "true"> <!--- when false it leaves the fields on the page but encrypts the values using the --->
		<cfargument name="useonce" default = "false">
		<cfargument name="singleSession" default = "true">
		<cfargument name="ttl_minutes" default = "0">


		<cfset var result = {contentChanged = false}>
		<cfset var dontRemoveFieldsRegExp = listappend(dontRemoveRegExp,this.dontRemoveHiddenFieldsStandardRegExp,"|")>
		<cfset var alteredContent = "">
		<cfset var formStateStructure = structNew()>
		<cfset var name = "">
		<cfset var value = "">
		<cfset var wholeString = "">
		<cfset var i = "">
		<cfset var foundArray = "">
		<cfset var regExpArray = ArrayNew(1)>
		<cfset var newValue = '' />
		<cfset var newString = '' />
		<cfset var hiddenFieldsArray = '' />
		<cfset var hiddenField = '' />
		<cfset var newhiddenField = '' />

		<cfset hiddenFieldsArray = findAllHiddenFields (contentString)>

		<cfset formStateStructure.fields = structNew()>
		<cfset formStateStructure.removed = "">
		<cfset alteredContent = contentString>


			<cfloop array="#hiddenFieldsArray#" index="hiddenField">
				<cfset name = hiddenField.attributes.name>
				<cfif structKeyExists (hiddenField.attributes,"value")>
					<!--- CASE 437623 add canonicalize --->
					<cfset value = canonicalize(hiddenField.attributes.value,false,false)>
					<cfif (arguments.variableNamesRegExp is "" or refindNoCase(arguments.variableNamesRegExp,name)) and (arguments.excludeVariableNamesRegExp is "" or not refindNoCase(arguments.excludeVariableNamesRegExp,name)) and not isValueEncrypted(value)>
						<cfset result.contentChanged = true>
						<cfif useformstate>
							<cfif not structKeyExists(formStateStructure.fields,name) or formStateStructure.fields[name] is "">
								<!--- WAB 2013-01-24 Problem encrypting strings which are numeric and longer than 10 or so digits, Get converted to exponent format in JSON
									I am going to have to add a space at the beginning and strip it off when decrytping
								--->
								<cfif isNumeric(value) and len(value) GT 10>
									<cfset value = " " & value>
								</cfif>
								<cfset formStateStructure.fields[name] = value>
							<cfelseif value is not "">
								<cfset formStateStructure.fields[name] = listappend(formStateStructure.fields[name],value)>
							</cfif>
							<cfif arguments.remove and (arguments.removeRegExp is "" or refindNoCase(arguments.removeRegExp,name)) and  refindNoCase(dontRemoveFieldsRegExp,name) is 0>
								<cfset alteredContent = replaceNocase (alteredContent,hiddenField.string,"")>
								<cfset formStateStructure.removed = listAppend(formStateStructure.removed,name)>
							</cfif>
						<cfelse> <!--- encrypt values individually --->
								<cfset newValue = encryptVariableValue (name=name,value=value,singleSession=singleSession)>
								<cfset newHiddenField = reReplaceNoCase(hiddenField.string,"(value=['""]+)#value#([""']+)",'VALUE="#newValue#"')>
								<cfset alteredContent = replaceNocase (alteredContent,hiddenField.string,newHiddenField,"ONCE")>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>

		<cfif structCount(formStateStructure.fields)>

			<cfset result.FormState = createFormStateVariable (formStateStructure = formStateStructure,warningMode = warningMode,ttl_minutes=ttl_minutes,singlesession=singlesession,useonce=useonce)>

			<cfset result.alteredContentString = 	alteredContent & '<INPUT TYPE="HIDDEN" NAME="formState" VALUE="#Result.formState#">'>
		<cfelse>
			<cfset result.alteredContentString = 	alteredContent >
		</cfif>
		<cfreturn result>

	</cffunction>

	<cffunction name="findAllHiddenFields">
		<cfargument name="content" required=true>
		<cfset var result = application.com.regExp.findHTMLTagsInString(inputString=content,htmlTag="INPUT",hasendtag=false,attributeRegExp="type=[""']hidden[""']")>

		<cfreturn result>
	</cffunction>

	<cffunction name="AddFormTokensToContent" access="public" returntype="string" output="false">
		<cfargument name="content" required=true>

		<cfset var result = content>
		<cfset var findForms = "">
		<cfset var thisForm = "">
		<cfset var startCount = getTickCount()>
		<cfset var newFormInnerHTML = '' />
		<cfset var newForm = '' />
		<cfset var formChanged = false />
		<cfset var actionToTest = "" />
		<cfset var argumentCollection = {inputString = content,htmltag="form",useJavaMatch = false, useSophisticatedRegExp = false}>

		<cfset findForms = application.com.regExp.findHTMLTagsInString(argumentCollection  = argumentCollection )>

		<cfloop array = "#findForms#" index="thisForm">
			<!--- add tokens to POST forms (not GET) --->
			<cfset newForm = thisForm.string>
			<cfset formChanged = false>
			<cfif structKeyExists(thisForm.attributes,"method") and thisForm.attributes.method is "POST" AND thisForm.innerHTML does not contain 'name="_rwSessionToken"'>
				<cfif not structKeyExists(thisForm.attributes,"action") or refindNoCase("http(s)*://(?!#request.currentsite.Domain#)",thisForm.attributes.action) is 0>
				<cfset newFormInnerHTML = chr(10) & createSessionTokenFormFieldHTML() & thisForm.innerHTML>
				<cfset newForm = replace(newForm,">" & thisForm.innerHTML & "<",">" & newFormInnerHTML & "<")> <!--- 2015-11-24 WAB/DAN Case 446240 --->
				<cfset formChanged = true>
			</cfif>
			</cfif>

			<!--- WAB 2012-05-16
				Discovered that if CFFORM does not have an action attribute (or it is blank)
				it automatically sets action to #cgi.script_name#?#cgi.query_string#
				This can cause XSS attacks if cgi.query_string is dodgy
				We could put action = ?#request.query_sting# on all cfforms (but this is a pain)
				So this piece of code looks for action="#cgi.script_name#?#cgi.query_string#" and removes it
				Note that in the code below I can't look at the value of thisForm.attributes.action because a XSS attack will usually have " and > in it - and these confuse my convertNameValuePairsToStructure
			--->
			<cfset actionToTest = 'action="#cgi.script_name#?#cgi.query_string#"'>
			<cfif structKeyExists(thisForm.attributes,"action") and thisForm.string contains actionToTest>
				<cfset newForm = replace(newForm,actionToTest, "")> <!--- 2015-11-24 WAB/DAN Case 446240 --->
				<cfset formChanged = true>
			</cfif>

			<cfif formChanged>
				<cfset result = replace(result,thisForm.string, newForm)> <!--- 2015-11-24 WAB/DAN Case 446240 --->
			</cfif>

		</cfloop>

		<cfreturn result>

	</cffunction>

<!---
These form Token functions not used at the moment
just have a session token, but in future we might want some sort of form specifi token
	<cffunction name="createFormToken" access="public" returntype="string" output="false">
		<cfargument name="pageURL" default="#cgi.http_host##cgi.script_name#">

			<cfset var result = hash(session.sessionid & pageURL)>
		<cfreturn result>

	</cffunction>

	<cffunction name="testFormToken" access="public" returntype="boolean" output="false">
		<cfargument name="token" >

			<cfset var result = false>
			<cfset var referer = application.com.regExp.removeProtocol(listfirst(cgi.http_referer,"?"))>
			<cfset var correctToken = createFormToken(referer)>



			<cfif correctToken is token>
				<cfset result = true>
			</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="createFormTokenField" access="public" returntype="string" output="false">

			<cfset var result = '<input type="hidden" name="_rwFormToken" value="#createFormToken()#">'>
		<cfreturn result>

	</cffunction>
--->


	<!--- 	WAB 2015-05-19 CASE 444754 - Rotation of SessionID on Login.
			During rotation the value of session.SessionID will be incorrect.
			Need to get the underlying session ID from Java so use application.com.request.getSessionID()
	--->
	<cffunction name="getSessionToken" access="public" returntype="string" output="false">
		<cfargument name="sessionID" default="#application.com.request.getSessionID()#">

		<cfset var result  = "">

		<!--- 	I add on a few actual characters because I need to be able to look at a list of session Tokens (which are used as temporary directorynames)
				and quickly work out which are active sessions, by having a few characters I don't have to test every single active sessionid, just a few
				WAB 2013-11-11 CF10 sessionids are somewhat different, they all end in .#instancename# and ids from individual instances do not all start with the same 4 characters so I can use the first 4 characters to filter them down
		--->
		<cfset result = left (arguments.sessionid,4) & hash(arguments.sessionID) >

		<cfreturn result>


	</cffunction>

	<cffunction name="testSessionToken" access="public" returntype="boolean" output="false">
		<cfargument name="sessionToken" required=true>
		<cfargument name="sessionID" default="#application.com.request.getSessionID()#">

		<cfreturn iif(getSessionToken(sessionid = sessionID) is sessionToken,true,false)>

	</cffunction>

	<!--- WAB 2012-06-13 if request is a POST checks whether correct token is present.  Returns TRUE if the post is blind (ie no token) --->
	<cffunction name="isBlindPost" access="public" returntype="boolean" output="false">
		<cfset var result = false>

 			<cfif cgi.request_method is "POST" and isdefined("form")>  <!---  Need to test for form as well because posts to webservices do not have form scope--->
				<cfset result = NOT isXSRFTokenPresentAndValid()>
			</cfif>
		<cfreturn result>
	</cffunction>


	<!--- --->
	<cffunction name="isXSRFTokenPresentAndValid" access="public" returntype="boolean" output="false">
		<cfset var result = false>

		<cfif cgi.request_method is "POST" and isdefined("form")>  <!---  Need to test for form as well because posts to webservices do not have form scope--->
			<cfif structKeyExists(form,"_rwSessionToken") AND testsessionToken(form._rwSessionToken) >
				<cfset result = true>
			</cfif>
		<cfelseif cgi.request_method is "GET">
			<cfif structKeyExists(url,"_rwSessionToken") AND testsessionToken(url._rwSessionToken) >
				<cfset result = true>
			</cfif>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!---
		The session hash is a simple hash of the sessionid (plus a few characters).  We use it for uniquely naming a temporary directory, where it is a security risk to use the sesisonid itself
		It is different from the sessionToken (and we don't use the sesssion token for this purpose), because we don't mind the hash being on a URL
		Not sure why I didn't just use an encrypted string which would be much easier to reverse engineer
	--->
	<cffunction name="getSessionHash" access="public" returntype="string" output="false">
		<cfargument name="sessionID" default="">
		<cfargument name="sessionScope" default="#session#">

		<cfset var result  = "">

		<cfif arguments.sessionID is "">
			<cfset arguments.sessionid = sessionScope.sessionid>
		</cfif>

		<!--- I add on a few actual characters because I need to be able to look at a list of sessionTokens(directorynames) and quickly work out which are active sessions, by having a few characters I don't have to test every single active sessionid, just a few --->
		<!--- WAB 2013-11-11 Changed way token created (to take into account how CF10 sessionids are built up). Now look like this 397BA9067A4CF5CF5460127A3E2C9CF0.cfusion . So they end with the name of the instance and the first 4 characters now change (whereas previously they generally stayed the same on a particular instance)	  --->
		<cfset result = left (arguments.sessionid,4) & hash(arguments.sessionID) >
		<cfreturn result>


	</cffunction>

	<cffunction name="getSessionIDFromHash" access="public" returntype="struct" output="false">
		<cfargument name="sessionToken" required=true>
		<cfargument name="sessionIDList" default="">
		<cfargument name="applicationScope" default="#application#">

		<!--- given a hash we can find which session it is for (as long as we have a list of possible sessions)
			could test all the sessionid, but because we have left the last 4 characters of the session id unencrypted we can use
			them to narrow the search
		 --->
		<cfset var findArray = "">
		<cfset var find = "">
		<cfset var result = {isOK = false, sessionid = ""}>
		<cfset var first4 = left(sessionToken,4)>
		<cfset var regExp = "(?:\A|,)(#first4#[^,]*?)(?:\Z|,)">  <!--- WAB 2013-11-11 Changed way token created (to take into account how CF10 sessionids are built up) --->
		<cfset var groupNames = {1="sessionid"}>
		<cfset var sessionid = "">

		<cfif sessionIDList is "">
			<cfset arguments.sessionIDList= applicationScope.com.sessionManagement.getListOfSessionIDs()>
		</cfif>

		<cfset findArray = applicationScope.com.regExp.refindAllOccurrences(regExp,sessionIDList,groupnames)>

		<cfset find = "">
		<cfloop array = "#findArray#" index="find">
			<cfset sessionID = find.sessionid>
			<cfif testSessionToken(sessionToken,sessionid)>
				<cfset result.isOK = true>
				<cfset result.sessionid = sessionid>
				<cfbreak>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<cffunction name="createSessionTokenFormFieldHTML" access="public" returntype="string" output="false">

			<cfset var result = '<input type="hidden" name="_rwsessiontoken" value="#getsessionToken()#">'> <!--- note that lower case is critical (for when we check its existence in JS)--->
		<cfreturn result>

	</cffunction>


	<!---
	WAB

	Function for encrypting a query string
	uses all the same parameters as the encryptHiddenFields customTag

	 --->
	<cffunction name="encryptQueryString" access="public" returntype="string" output="false">
		<cfargument name="queryString" required=true>
		<cfargument name="variableNamesRegExp" default = "">  <!--- regExp to define which variables to encrypt - leave blank for all --->
		<cfargument name="excludeVariableNamesRegExp" default = "">  <!--- WAB added 2009/09/15 --->
		<cfargument name="remove" default = "true">
		<cfargument name="removeRegExp" default = "">  <!--- it is possible to use this to encypt a variable, but also leave it in the query string - of more use on forms, but left here for compatibility --->
		<cfargument name="dontRemoveRegExp" default = ""> <!--- don't want these moved   WAb 2009/09/14--->
		<cfargument name="debug" default = "false">
		<cfargument name="warningMode" default = "false"><!--- doesn't prevent a page running, but issues a warning by email --->
		<cfargument name="useformstate" default = "true">
		<cfargument name="singleSession" default = "false">

		<cfset var dontRemoveFieldsRegExp = listappend(dontRemoveRegExp,"_required|_date|_eurodate","|")> <!--- actually wouldn't find these on a url --->
		<!--- 		<cfset var regExp = "[?&\A](([A-Za-z0-9_]*?)=([^&\Z]*))">  WAB 2009/09/15 altered regExp - wasn't dealing with start of string properly, had to change indexes at the same time --->
		<!--- <cfset var regExp = "((\A|&|\?)([A-Za-z0-9_]*?)=([^&\Z]*))"> --->
		<cfset var regExp = "([A-Za-z0-9_]*?)=([^&$]*)(\&|$)">


		<cfset var alteredContent = "">
		<cfset var formStateStructure = structNew()>
		<cfset var found = "">
		<cfset var idx = "">
		<cfset var i = "">
		<cfset var variablename = "">
		<cfset var foundArray = "">
		<cfset var groups = {1="name",2="value",3="_"}>

		<!---
			This regexp searches for name value pairs with & delimiter   xxxx=yyyy&
		 --->
	 	<cfset foundArray = application.com.regexp.refindAllOccurrences(regExp,arguments.queryString,groups)>
		<cfset alteredContent= arguments.queryString>

		<cfset formStateStructure.fields = structNew()>
		<cfset formStateStructure.removed = "">

		<cfloop index="found" array=#foundArray#>
			<!--- 	WAB 2012-10-17 CASE 431347, if the query string already contained a formstate variable, it was getting encoded again
						Don't re-encrypt a formstate.  Possibly might be better to decode and re-encode with the other variables
					WAB 2014-11-10 CASE 442583/442412  Have actually gone ahead and done what I suggested 2 years ago - decode and re-encode any existing formstates
						Turns out that the other end of the process (decrypting) couldn't handle multiple formstates anyway
			--->

			<cfif (arguments.variableNamesRegExp is "" or refindNoCase(arguments.variableNamesRegExp,found.name)) and (arguments.excludeVariableNamesRegExp is "" or not refindNoCase(arguments.excludeVariableNamesRegExp,found.name))>
				<cfif found.name is "formState">
					<cfset var formStateDecrypted = decryptFormStateVariable (found.value)>
					<cfset structAppend (formStateStructure.fields,formStateDecrypted.result)>
				<cfelseif not structKeyExists(formStateStructure.fields,found.name) or formStateStructure.fields[found.name] is "">
					<!--- variable not already in structure, so add --->
					<cfset formStateStructure.fields[found.name] = found.value>
                <!--- case 448769 ESZ 2016-04-05 	Was referencing value rather than found.value --->
				<cfelseif found.value is not "">
					<!--- variable already in structure so append --->
					<cfset formStateStructure.fields[found.name] = listappend(formStateStructure.fields[found.name],found.value)>
				</cfif>

				<!--- remove variable and value from query string if necessary--->
				<cfif arguments.remove and (arguments.removeRegExp is "" or refindNoCase(arguments.removeRegExp,found.name)) and  refindNoCase(dontRemoveFieldsRegExp,found.name) is 0>
					<cfset alteredContent = replaceNocase (alteredContent,found.String,"")> <!--- WAB 2009/09/15 changed from reReplace--->
					<cfset formStateStructure.removed = listAppend(formStateStructure.removed,found.name)>
				</cfif>


			</cfif>

		</cfloop>


		<cfif useFormState>
			<cfif structCount(formStateStructure.fields)>
				<cfset alteredContent = listappend(alteredContent, "formState=" & urlencodedformat(createFormStateVariable (formStateStructure = formStateStructure.fields, warningMode = arguments.warningMode, singleSession = arguments.singleSession)),"&")>
			</cfif>
		<cfelse>
			<cfloop collection = #formStateStructure.fields# item="variablename">
				<cfset formStateStructure.fields[variablename] = encryptVariableValue (name = variablename, value = formStateStructure.fields[variablename], singleSession = arguments.singleSession)>
			</cfloop>
			<cfset alteredContent = listappend(alteredContent,application.com.structureFunctions.convertStructureToNameValuePairs(struct = formStateStructure.fields,delimiter="&"))>
		</cfif>
		<cfreturn alteredContent>

	</cffunction>


	<!--- WAB 2009/09/15 function to encrypt whole url in one go, easier way of accessing encryptQueryString --->
	<cffunction name="encryptURL" access="public" returntype="string" output="false">
		<cfargument name="url" required=true>
		<!--- for documentation see encryptQueryString above --->
		<cfargument name="variableNamesRegExp" default = "">
		<cfargument name="remove" default = "true">
		<cfargument name="removeRegExp" default = "">
		<cfargument name="debug" default = "false">
		<cfargument name="warningMode" default = "false">
		<cfargument name="singleSession" default = "false">

		<cfset var queryString = listRest (url,"?")>
		<cfset var baseURL = listFirst (url,"?")>


		<cfreturn  baseURL & "?" & encryptQueryString(queryString = queryString , argumentCollection = arguments)>

	</cffunction>


	<!---
	WAB 2011/06/01
	Needed some way of being able to pass around encrypted variable values (for example in javascript)
	The formstate idea doesn't really work in this case because you can't pull out the variable you want
	My idea goes like this:
		i) we need to be able to automatically decrypt the variable, so we need to be able to identify it.
			there are two options:
			a) [my initial idea] change the name of the variable to xxxxx_enc and automatically decrypt an put in variable called xxxxx
			b) [perhaps a better idea ]put a specific set of characters at the beginning or end of the string, followed by the encrypted part
				This has the advantage that no code has to be changed to deal with names form fields and variables being changed
		ii) We can't just encrypt the value by itself because then personid=1 would have same encryption as locationid=1 - which would be rather dangerous
			Therefore need to use the name of the field itself somewhere in the encryption
			So how about encrypting the name of the variable and using this as the key to encrypt the value?
	--->

	<cffunction name="getEncryptionSuffix" access="public" returntype="string" output="false">
		<cfargument name="singleSession" default = "false">

		<cfif not structKeyExists(this,"encryptionSuffix")>
			<cfset this.encryptionSuffix = hash(application.instance.databaseid)> <!--- doesn't matter what this is, looks best as Capital Hex --->
		</cfif>

		<cfif singleSession>
			<cfreturn "S-" & this.encryptionSuffix>
		<cfelse>
			<cfreturn this.encryptionSuffix>
		</cfif>

	</cffunction>

	<cffunction name="createEncryptVariablePassKey" access="public" returntype="string" output="false">
		<cfargument name="name" required=true>
		<cfargument name="singleSession" default = "false">

		<cfset var passkey = "">
		<cfif singleSession>
			<cfset passkey = application.com.encryption.encryptString(string=hash(ucase(name)),passkey=right(session.sessionid,20))>
		<cfelse>
			<cfset passkey = application.com.encryption.encryptString(string=hash(ucase(name)))>
		</cfif>

		<cfreturn LEFT(passkey,20)>

	</cffunction>

	<cffunction name="encryptVariableValue" access="public" returntype="string" output="false">
		<cfargument name="name" required=true>
		<cfargument name="value" required=true>
		<cfargument name="singleSession" default = "false">

			<cfset var passkey = "">
			<cfset var encryptedvalue = "">

		<cfif value is not "">
			<cfset passkey = createEncryptVariablePassKey(name = name,singlesession = singlesession)>
			<cfset encryptedvalue = application.com.encryption.encryptStringcf_cryp(string=value,passkey= passkey)>
			<cfset encryptedvalue = encryptedvalue & "-" & getEncryptionSuffix(singlesession = singlesession) >
			<cfreturn encryptedvalue>
		<cfelse>
			<cfreturn value>
		</cfif>
	</cffunction>

	<!---
	WAB 2011/11/16 New Function
	Probably needs a bit of modification to automatically deal with column datatypes.
	Column being encrypted is often integer but the result is of course not
	--->
	<cffunction name="encryptQueryColumn" access="public" returntype="query" output="false">
		<cfargument name="query" required=true>
		<cfargument name="columnName" required=true>
		<cfargument name="updatecolumnName" default="#arguments.columnName#">
		<cfargument name="variableName" default="#arguments.columnName#">
		<cfargument name="singleSession" default = "false">

		<!---  Add a check to make sure we don't double encrypt'--->
		<cfset var alreadyEncryptedCheckDone = false>

		<cfif listFindNoCase (query.columnList,updatecolumnName) is 0>
			<cfset queryAddColumn (query,updatecolumnName,arrayNew(1))>
		</cfif>

		<cfloop query="query">
			<cfif not alreadyEncryptedCheckDone and query[columnName][currentRow] is not "">
				<cfif isValueEncrypted(query[columnName][currentRow])>
					<cfbreak> <!--- this query has already been encrypted --->
				<cfelse>
					<cfset alreadyEncryptedCheckDone = true>
				</cfif>
			</cfif>
			<cfset querySetCell(query,updatecolumnName,encryptVariableValue(variablename,query[columnName]),currentRow)>
		</cfloop>
		<cfreturn query>
	</cffunction>

	<!--- WAB 2011/11/16 modified so can handle lists of encrypted variables (eg if posted from a multiple select) --->
	<cffunction name="decryptVariableValue" access="public" returntype="string" output="false">
		<cfargument name="name" required=true>
		<cfargument name="value" required=true>

		<cfset var encryptedPart = "">
		<cfset var result = ""	>
		<cfset var passkey = "">
		<cfset var thisValue = "">
		<cfset var singleSession = false	>

		<cfloop list="#value#" index="thisvalue" >
			<cfset encryptedPart = listAllButLast(thisvalue,"-")>
			<cfif listLast(encryptedPart,"-") is "S">
				<cfset singleSession = true	>
				<cfset encryptedPart = listAllButLast(encryptedPart,"-")>
			</cfif>
			<cfif passKey is ""><!--- only need to calcuate the passkey once in the loop --->
				<cfset passkey = createEncryptVariablePassKey(name=name,singlesession =singlesession )>
			</cfif>
			<cfset result = listappend(result,application.com.encryption.decryptStringcf_cryp(string=encryptedpart,passkey= passkey))>
		</cfloop>



		<cfreturn result>

	</cffunction>

	<cffunction name="isValueEncrypted" access="public" returntype="boolean" output="false">
		<cfargument name="value" required=true>
		<cfset var result = false>
				<cfif refindNocase("-#getEncryptionSuffix()#\Z",value)>
					<cfset result = true>
				</cfif>
		<cfreturn result>

	</cffunction>



	<cffunction name="createFormStateVariable" access="public" returntype="string" output="false">
		<cfargument name="formStateStructure">
		<cfargument name="useonce" default = "false">
		<cfargument name="singleSession" default = "false">
		<cfargument name="ttl_minutes" default = "0">

		<cfset var formStateWDDX = "">
		<cfset var encryptedformState = "">

			<cfif useOnce>
				<cfset arguments.formStateStructure.useOnce = true>
				<cfset arguments.formStateStructure.uniqueid = createUUID() >
				<cfparam name="session.encryptedForms" default="#structNew()#">
				<cfset session.encryptedForms[formStateStructure.uniqueid]  = true>
			</cfif>
			<cfif singleSession>
				<cfset arguments.formStateStructure.sessionid = session.sessionid>
				<cfset arguments.formStateStructure.singleSession = true>
			</cfif>
			<cfif ttl_minutes is not 0 >
				<cfset arguments.formStateStructure.formDate = now()>
				<cfset arguments.formStateStructure.ttl_minutes = ttl_minutes>
			</cfif>

		<cfset encryptedformState = encryptFormStateStructure(formStateStructure)>

		<cfreturn encryptedformState>

	</cffunction>


	<cffunction name="encryptFormStateStructure" access="public" returntype="string" output="false">
		<cfargument name="formStateStructure">

		<cfset var formStateWDDX = SerializeJSON(formStateStructure)>
		<cfset var passKeyInfo = getCurrentFormStatePassKey()>
		<!--- NJH 2009/07/28 removed the passkey from the encrypt function as it's now stored in the application variable and no longer needed. --->
		<cfset var result = application.com.encryption.EncryptStringRandomIV(string=formStateWDDX,passkey=passKeyInfo.passkey,modifiedEncoding=true)>
		<cfset result = result & "|" & passKeyInfo.version>

		<cfreturn result>
	</cffunction>

	<!--- WAB 2012-10-02 Altered these two functions so that the passKey is cached within this cfc, so reducing number of calls to getSetting
		Note that the idea is that the passkeys should change on a regular (?daily basis).
		We would cycle through version 01 to 10 and then start over again

	--->
	<cffunction name="getCurrentFormStatePassKey" access="public" returntype="struct" output="false">
		<cfset var result = {}>

		<cfset result.version = "01">
		<cfset result.passkey = getFormStatePassKeyByVersion(result.version)>

		<cfreturn result>

	</cffunction>

	<cffunction name="getFormStatePassKeyByVersion" access="public" returntype="string" output="false">
		<cfargument name="version" >

		<cfset var result = "">

		<!--- WAB 2013-10-04 CASE 437290 Added a check for the version being a valid pattern, otherwise causes all sort of problems in settings --->
		<cfif refind("\A[0-9][0-9]\Z",version) is 1 >
			<cfif not structKeyExists (variables.formStatePassKeys,version)>
				<cfset variables.formStatePassKeys[version] = application.com.settings.getSetting('security.formstatepasskeys.#version#')>
			</cfif>

			<cfset result = variables.formStatePassKeys[version]>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="decryptFormStateVariable" access="public" returntype="struct" output="false">
		<cfargument name="formStateString">


		<cfset var result = {isOK=true,result = structNew()}>
		<cfset var formStateWDDX = "">
		<cfset var passKeyVersion = "">
		<cfset var passKey = "">
		<cfset var fieldName = "">

		<!--- Deal with case of formstate not being urldecoded - probably only happens if trying to decrypt value in cgi.HTTP_REFERER --->
		<cfif formStateString contains "%7C">
			<cfset arguments.formStateString = urlDecode(arguments.formStateString)>
		</cfif>

		<cfset passKeyVersion = listLast(formStateString,"|")>
		<cfset passKey = getFormStatePassKeyByVersion(passKeyVersion)>

		<!--- WAB 2013-10-04 CASE 437290 Added check for passKey being blank (which may now happen if the formstate string has been truncated or otherwise been tamperered with)--->
		<cfif passKey is not "">
			<cfset formStateWDDX = application.com.encryption.deCryptStringRandomIV(string=listAllButLast(formStateString,"|"),passkey=passKey,modifiedEncoding=true)>
		</cfif>

		<cfif formStateWDDX is "">
			<cfset result.isOK = false>
		<cfelse>
			<cfset result.result = deSerializeJSON(formStateWDDX)>
			<!--- WAB 2013-01-24 Problem with JSON encoding long numbers.  Have to add a space infront, which needs removing --->
			<cfif structKeyExists(result.result,"fields")>
				<cfloop item="fieldName" collection = #result.result.fields#>
					<cfif isNumeric(result.result.fields[fieldname]) and len(result.result.fields[fieldname]) GT 9>
						<cfset result.result.fields[fieldname] = trim (result.result.fields[fieldname])>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- WAB 2009/09/15 from an idea by NJH --->
	<cffunction name="confirmFieldsHaveBeenEncrypted" access="public" returntype="string" Hint="Makes sure that fields have been passed in as encrypted strings and haven't just been bolted onto the url" output="false">
		<cfargument name="fieldNames" required="true">
		<cfargument name="regExp" default="false">

		<cfreturn confirmFieldsHaveBeenEncryptedWithFeedback(fieldnames = fieldnames, regexp = regExp).isOK>
	</cffunction>

	<!--- WAB 2011/06/13 created this function from confirmFieldsHaveBeenEncrypted so that we could return the name of the field which had failed the encryption test --->
	<!--- WAB 2011/11/15 changed the way this function worked, so could accept a regular expression (meant looping over the form and url variables rather than looping over the list of names to check)
						then had to add regExp parameter
	--->
	<cffunction name="confirmFieldsHaveBeenEncryptedWithFeedback" access="public" returntype="struct" Hint="Makes sure that fields have been passed in as encrypted strings and haven't just been bolted onto the url" output="false">
		<cfargument name="fieldNames" required="true">
		<cfargument name="nonEncryptedFieldNames" default="">
		<cfargument name="regExp" default="false">

		<cfset var result = {isOK = true,failedFieldNames=""}>
		<cfset var scope = "">
		<cfset var scopes = "">
		<cfset var var = "">

		<cfparam name="form" default="#structNew()#">
		<cfset scopes = {form = form,  url = url}>

		<cfif arguments.fieldNames is "">
			<cfreturn result>
		</cfif>

		<!---
				for backwards compatibility (and perhaps to make it easier for programmers)
				arguments.fieldnames can just be a comma separated list, not a regExp
				but we need to convert it to a regexp, so comma needs replacing with |
				but also need to put in \A and \Z to prevent substrings matching
		--->

		<cfif not regExp>
			<cfset arguments.fieldNames = "\A" & replace(arguments.fieldnames,",","\Z|\A","ALL") & "\Z"> <!--- make into a regexp --->>
			<cfif arguments.nonEncryptedFieldNames  is not "">
				<cfset arguments.nonEncryptedFieldNames = "\A" & replace(arguments.nonEncryptedFieldNames,",","\Z|\A","ALL") & "\Z"> <!--- make into a regexp --->
			</cfif>
		</cfif>

		<cfloop collection="#scopes#" item="scope">
			<cfloop item="var" collection ="#scopes[scope]#">
				<cfif refindNocase(fieldNames,var) and (nonEncryptedFieldNames is "" or not refindNocase(nonEncryptedFieldNames,var)) and not structKeyExists(request.encryption.encryptedFields,var)>
					<cfset result.failedFieldNames = listappend(result.failedFieldNames,var)>
					<cfset result.ISOK = false>
				</cfif>
			</cfloop>
		</cfloop>

		<cfreturn result>
	</cffunction>



	<!---
		WAB 2015-01-15 CASE 443444
		Added checkVariableNames parameter, replaces hardcoded hack in checkForValidURLVariableName() for
		Problems with missingTemplateHandler because the queryString gives dodgy variables in the URL collection
		Replace our previous hack (which failed when the queryString contained an & and generated more than one variable in the URL collection)
	--->

	<cffunction name="checkForSQLInjectionAndXSS" access="private">
			<cfargument name="XSSSafeVariables" default = "">
			<cfargument name="SQLSafeVariables" default = "">
			<cfargument name="checkVariableNames" default = "true">
			<cfargument name="isInternal" default = "#request.relayCurrentUser.isInternal#">

					<!---
					Code has been brought from sqlInjectionChecker.cfm
					Author:				WAB
					Date started:		2007-04-16

					Description:
					Checks form and url scopes for dodgy bits of sql

					Amendment History:

					Date (DD-MMM-YYYY)	Initials 	What was changed
					2008/07/08    WAB     <script> tag added
					2008/07/21    GCC     Tweaked to move <script to the end so that it isn't expected to be followed by a space>
					2008/07/22    GCC     Added message to list - better solution medium term would be to change this behaviour completely and reinstate 'message' checking- not ideal but it is passed as URL variable in comms (commfileadd) - <script>alert('Some links in your email will not be tracked')</script>
					2008/09/30    PPB	  Added filetext_Text to fieldsToIgnore so that relay/FileManagement/editFileContents.cfm saves
					2008/10/27	  NJH	  Bug Fix All Sites Issue 1206 - added noteText to fields to ignore so that action notes can save. Changed the insert/update to use cfqueryParam
					2008/11/07	  SWJ	  Took message out of the list and made a mod to commfileadd and commFile as this was introducing a xss attack option
					2008/11/25    WAB     Moved javascript to the end so that it isn't expected to be followed by a space
					2009/03/23		WAB		changed variable name x to sqlInjectionReFindResult   (less likely to be used as a miscellaneous variable)
					2009/04/21		GCC		Extended to make safe any HTML submitted in URL or FORM variables from a non-internal site.
					2009/05/13		GCC		Testing some further SQL checks - create trigger and select from.
					2009/07/16		NJH		P-FNL069 - check that variables coming through have valid names. Replace single quotes with `.
					2009/07/08		WAB 	altered variable names for the regular expression find (was causing contamination)
					2009/11/18		NJH		P-FNL080 - don't replace single quotes with ` if the variable is a valid date.
					2010				Added exception for the missingtemplate handler
					2012-02-16		WAB		separated out SQL and XSS regExp so that XSSSafeVariables could pass the XSS test but still be pulled up by the SQL test
					Possible enhancements:


					 --->
				<!--- 	<cfset var fieldsToIgnore = "SEARCHTEXTSTRING,FRMMSG_HTML,FRMMSG_TXT,filetext_Text,NoteText"> WAB 2012-02-16 removed these and added to XSSSafeVariables, couldn't do noteText.  TODO searchTextString--->
 				<cfset var result = structNew()>
				<cfset var scope = "">
				<cfset var thecollection = '' />
				<cfset var fieldValue = '' />
				<cfset var sqlInjectionReFindResult = '' />
				<cfset var field = '' />
				<cfset var invalidVariableNameCheck= {isOK = true}>
				<cfset var injectionCheck = {isOK = true} />

				<cfset result.isOK = true>
				<cfset result.errors = arrayNew(1)>

				<!--- WAB 2013-09-13 reject any string which has %00 in it --->
				<cfif refindNocase ("%00",cgi.query_string)>
					<cfset result.isOK = false>
					<cfset arrayAppend(result.errors,"SQL/XSS injection problem URL contains %00" )>
				</cfif>

				<cfloop index = "scope" list = "url,form">
					<!--- 2009/06/18 GCC scope is not defined if you go to a cfc with ?wsdl and this check stops it from breaking --->
					<cfif isdefined(scope)>
						<cfset thecollection = evaluate(scope)>
						<cfloop item="field" collection="#thecollection#">
								<cfset fieldValue = thecollection[field]>
								<cfset injectionCheck = {isOK=true}>

								<cfif SQLSafeVariables is "" or refindNoCase(SQLSafeVariables,field) is 0>
									<!--- Note when adding item to this list that there are really two sections - 1st section has mainly sql stuff which looks for words with spaces before and after.  The second section is just a set of words to block regardless of spaces --->

									<cfset injectionCheck  = checkStringForInjection(fieldValue,"dangerousSQL")>
									<!--- additional SQL injection checks for non internal users 		--->
									<!--- 2009/06/18 GCC noted restricting "select from" this breaks editing of valid value queries - made non internal only--->
									<cfif injectionCheck.isOK  and not arguments.isInternal >
										<cfset injectionCheck  = checkStringForInjection(fieldValue,"SQLSelect")>
									</cfif>
								</cfif>

								<!--- specific XSS checks unless variable is on the XSSSafeVariables list --->
								<cfif injectionCheck.isOK and (XSSSafeVariables is "" or refindNoCase(XSSSafeVariables,field) is 0)>
									<cfset injectionCheck  = checkStringForInjection(fieldValue,"XSS")>
								</cfif>

								<!--- NJH 2009/06/29 - P-FNL069 added the reg exp to check for a valid variable names - should only be alphanumeric plus an underscore and a dot	--->
								<!--- WAB 2015-01-15 CASE 443444 Added support for new checkVariableNames argument --->
								<cfif injectionCheck.isOK and checkVariableNames>
									<cfset invalidVariableNameCheck = checkForValidURLVariableName(field)>
								</cfif>

								<cfif not injectionCheck.isOK or not invalidVariableNameCheck.isOK>
									<cfif not injectionCheck.isOK >
										<cfset result.textInjected = injectionCheck.string>
										<cfset result.type = injectionCheck.type>
										<cfset arrayAppend(result.errors,"#field#: SQL/XSS injection problem (#result.type#): #result.textInjected#" )>
										<cfset result.FullTextInjected = fieldValue>
									<cfelse>
										<cfset arrayAppend(result.errors,"'#field#' is not a valid variable name")>
										<cfset result.textInjected = field>
										<cfset result.FullTextInjected = field&"="&fieldValue>
									</cfif>
									<cfset result.field = field>
									<cfset result.variableScope = scope>
									<cfset result.isOK = false>
									<cfset result.personid = request.relayCurrentUser.personid>
									<cfset result.fullname = request.relayCurrentUser.fullname>
									<cfset result.xsssafevariables = xsssafevariables>
									<cfset result.SQLsafevariables = SQLsafevariables>
								</cfif>


								<!--- GCC 2009/04/21 extended to HTML encode all URL and FORM params submitted from non Internal sites but to leave & alone
								Alters <,> to HTML safe alternatives--->
								<cfif XSSSafeVariables is "" or refindNoCase(XSSSafeVariables,field) is 0>

									<!--- WAB 2011/07/05 don't replace quotes if the string is a valid JSON string (eg in a coldfusion ajax call) --->
									<cfif not arguments.isInternal>
										<cfset theCollection[field] = replace(theCollection[field],">","&gt;","ALL")>
										<cfset theCollection[field] = replace(theCollection[field],"<","&lt;","ALL")>
										<!--- 2009/05/28 GCC further check to replace single quote with  safe' single quote --->`
										<!--- 2009/06/19 GCC disovered this breaks date time stamps in portal e.g. orders\orderItemsTask.cfm query at line 101 ish> --->
										<!--- 2009/07/09 I have re-instated this. Any timestamps that are passed (which I think is very few) can be changed back to use single quotes. Actually, the timestamp in question
											is now encrypted which gets by the issue at the moment.
												2009/11/18 NJH P-FNL080 - check if the variable is a date. If not, then replace single quotes; else, leave it be.
										--->
										<cfif not isDate(theCollection[field])>
											<cfset theCollection[field] = replace(theCollection[field],"'","`","ALL")>
										</cfif>
										<!--- WAB 2012-11-29 CASE 432326 isJSON() occasionally crashes (eg when field is 1000e), so use own isJSON_() function with a catch --->
										<cfif not isJSON_ (theCollection[field])>
											<cfset theCollection[field] = replace(theCollection[field],"""","&quot;","ALL")>
										</cfif>
									</cfif>
								</cfif>

						</cfloop>
					</cfif>
				</cfloop>

				<cfreturn result>
			</cffunction>


		<!--- WAB 2012-11-29 CASE 432326 added this function because isJSON() was falling over (eg testing 1000e).
				Any error is caught and function will return false
		 --->
		<cffunction name="isJSON_" hint="isJSON function wrapped in a try/catch to deal with cases when isJSON() crashes">
			<cfargument name="string">
			<cfset var result = false>
			<cftry>
				<cfset result = isJSON(string)>
				<cfcatch>
				</cfcatch>
			</cftry>
			<cfreturn result>
		</cffunction>

	<cfset tagFilter = "script,object,applet,embed,form,input,layer,ilayer,frame,iframe,frameset,param,meta,base,style">
	<cfset jsEventFilter = "onLoad,onClick,onDblClick,onKeyDown,onKeyPress,onKeyUp,onMouseDown,onMousemove,onMouseOut,onMouseUp,onMouseOver,onBlur,onChange,onFocus,onSelect,onabort,onerror,onresize,onscroll,onunload,onreset,onsubmit">
	<cfset jswordFilter	="javascript:,vbscript:,\.cookie,\.toString,:expr,:expression,\.fromCharCode,String\.,alert\s*\(">
	<cfset tagFilterRegExp = "(<(#replace(tagFilter,",","|","ALL")#).*?>)">
	<cfset jsEventFilterRegExp = "(#replace(jsEventFilter,",","|","ALL")#)\s*=">
	<cfset jswordFilterRegExp = "(#replace(jswordFilter,",","|","ALL")#)">

	<!---
		This function takes any bit of HTML and removes anything which might allow javascript to run
		You can use it to santise any bit of HTML of unknown origin, but obviously it is of no use if you actually want some javascript to run!

	--->
	<cffunction name="sanitiseHTML" output="false">
			<cfargument name="string">

			<cfset var result = string>
			<cfset result = reReplaceNoCase(result,tagFilterRegExp,"","ALL")>
			<cfset result = reReplaceNoCase(result,jsWordFilterRegExp,"","ALL")>
			<cfset result = reReplaceNoCase(result,jsEventFilterRegExp,"","ALL")>

			<cfreturn result>

	</cffunction>

	<!--- 2014-07-23 NYB Case 441030 changed "alter." to "alter\s"
		2015-10-20 WAB changed opening and closing spaces to \W (ie NOT a word character)
						Shutdown
	--->
	<cfset this.injectionRe = {
		dangerousSQL = "(?<=\A|[\W])(create.+?trigger|drop.+?table|drop.+?database|delete.+?from|alter\s+?table|alter\s+?view|alter\s+?database|alter\s+?function|alter\s+?trigger|truncate.+?table|update\s.+?\sset\s|\.\./|shutdown|exec.*\()([\W]|\Z|;)",
        sqlSelect = "(?<=\A|[\W])(\(\s*)?(select\s.+?from|if.+?select|select.+?\(|select.+?\@|\.\./)([\W]|\Z|;)", <!--- DAN 2015-07-17 Case 445264 --->
		XSS = "#jswordFilterRegExp#|#jsEventFilterRegExp#|#tagFilterRegExp#"

	}>

	<cffunction name="checkStringForInjection" access="public">
		<cfargument name="String">
		<cfargument name="Type">

		<cfset var result = {isOK = true}>

		<cfif not getMetadata(string).isInstance("java.lang.CharSequence")> 
			<!--- numerics and booleans are by definition safe 
				they also break some of the regular expression function which us java matching - which only works on actual strings 
				so can just return 
			--->
			<cfreturn result>
		</cfif>

		
		<cfset var stringNoComments = string>
		<!--- strip all comments from SQL statements --->
		<cfif findNoCase("sql",arguments.type) >
			<cfset var stringNoComments = application.com.regExp.removeComments(string = application.com.regExp.removeComments(string = string,type = "SQL", preserveLength = true ),type = "SQLSingleLine" ) >
		</cfif>


		<cfset var InjectionReFindResult = application.com.regExp.reFindAllOccurrences(reg_expression=this.injectionRe[type],string=stringNoComments,useJavaMatch=true)>

		<cfif arrayLen(InjectionReFindResult)>
			<cfif !findNoCase("sql",arguments.type)>
				<cfset result.isOK = false>
				<cfset result.string = InjectionReFindResult[1].string>
				<cfset result.type = type>
			<cfelse>
			
				<cfloop array="#InjectionReFindResult#" index="injectionItem">
					<cfset stringToTest = right (stringNoComments, len (stringNoComments) - InjectionItem.position  + 1)>

					<!--- NJH/WAB 2017/02/22 - RT-271 - check whether the string is actually runnable as sql, and therefore dangerous --->
					<cfif isStringRunnableAsSql(stringToTest)>
						<cfset result.isOK = false>
						<cfset result.string = stringToTest>
						<cfset result.type = type>
						<cfbreak>
					</cfif>
				
				</cfloop>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- NJH/WAB 2017/02/22 RT-271 - added a function to work out whether the string caught in the sql injection is actually runnable or not. Here
		we are running it in 'test' mode by turning noexec on --->
	<cffunction name="isStringRunnableAsSql" access="public" output="false" returnType="boolean">
		<cfargument name="String" type="string" required="true">

		<!--- remove any instances of 'set noexec off' --->
		<cfset var stringToTest = reReplaceNoCase(arguments.string,"set\s+noexec\s+off\s*;","","all")>

		<cfquery name="local.runStringAsSql">
			declare @SQL nvarchar(max) = N'SET NOEXEC ON; #stringToTest#; SET NOEXEC OFF;'
			BEGIN TRY
				exec sp_executesql @sql
				select 1 as valid
			END TRY
			BEGIN CATCH
				select 0 as valid
			END CATCH
		</cfquery>

		<cfreturn local.runStringAsSql.valid>

	</cffunction>

	<!---
		WAB 2015-01-15 CASE 443444
		Remove hardcoded hack to deal with peculiarities of the missing template handler,
		now handled further up process so that this function is not called when missing template handler involved
		WAB 2016-11 FormRenderer  Added '-' as a valid character in a field name.  Needed to for defining attributes of form   data-xxxx
	--->
	<cffunction name="checkForValidURLVariableName" access="public">
		<cfargument name="variableName">

		<cfset var result = {isOK = true}>
		<cfset var invalidVariableNameRegExp = "[^0-9a-zA-Z_\-\.#application.delim1#*\|\[\]]">    <!--- The dot was added as an image(??) comes through with coordindates.. loginButton.X and loginButton.Y   NJH 2011/06/24 - also added square brackets for ckfinder - when moving/copying images from one folder to another ---> <!--- WAB allowed a * for his settings project --->
		<cfif refindnocase(invalidVariableNameRegExp,variablename)>
			<cfset result.isOK = false>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- WAB 2011/02/01
		takes a set of name value pairs and does an HTMLEditFormat on all the values
		Use when cgi.query_string is put into a form's action to prevent XSS attacks

		WAB 2011/11/22 Wondering why I didn't just take the URL scope and encode that
		One reason would be that the URL parameters might have already been decrypted
		So if I were to do that then this function would need to be called before decryption is done
		I would also lose the order of parameters (which admittedly I have only just added!)
		Anyhow, this it the code which could be used
		<cfset request.query_String = application.com.structureFunctions.convertStructureToNameValuePairs(struct = URL,delimiter="&",encode="URL")>

		WAB 2013-05-02 (code done previously).  Changed to encoding the URL structure.  Closes an injection avenue.  the URL structure has already been sanitised by CF
	 --->

	<cffunction name="make_cgi_query_String_safe" output="false">
		<cfset var keylist = application.com.regExp.convertNameValuePairStringToStructure_(inputString = cgi.query_string, delimiter = "&").orderedNameList>
		<cfset request.query_String = application.com.structureFunctions.convertStructureToNameValuePairs(struct = URL,delimiter="&",encode="URL", keylist = keylist)>
		<cfset request.query_string_and_script_name = cgi.script_name & "?" & request.query_String>
	</cffunction>

	<cffunction name="makeQueryStringSafe" output="false">
		<cfargument name="queryString">

		<cfreturn application.com.structureFunctions.encodeNameValuePairString (inputstring=queryString,delimiter="&",encode="URL")>

	</cffunction>

<!--- strips out anything dodgy from HTML
	allows us to pass simple HTML messages around without worrying
--->




			<cffunction name="blockSQLInjectionAndXSS">
				<cfargument name="XSSSafeVariables" default = "">
				<cfargument name="SQLSafeVariables" default = "">

				<cfset var warningStruct = {}>
				<cfset var result = checkForSQLInjectionAndXSS(XSSSafeVariables = XSSSafeVariables,SQLSafeVariables = SQLSafeVariables)>

				<cfif not result.isOK>
					<cflog text="SQL Injection attempt by #result.fullname# (#result.personID#) via #result.variableScope#.#result.field# on template: #CGI.script_name# Type: #result.textInjected# FullTextInjected: #result.fullTextInjected#"
						log="APPLICATION" file="relaySecurity" type="Warning" application="yes">

					<cfset warningStruct.xssResult = result>
					<cfset warningStruct.script_name = cgi.script_name>
					<cfset application.com.errorhandler.recordRelayError_Warning(severity="SECURITY",Type="Injection",message = "Injection Problem.",WarningStructure = warningStruct)>

					<cfmail from="relayAdmin@foundation-network.com" to="relayErrors@foundation-network.com"
						subject = "SQL Injection on #CGI.server_name#" type="html">
						<cfdump var="#result#">
						<!--- <cfdump var="#request.relayCurrentUser#"> --->
					</cfmail>

					<!--- NJH 2009/06/29 - turn debug when error encountered, as the debug can cause a script to be run --->
					<cfsetting showdebugoutput="false">

					<!--- WAB --->
					<cfif application.testSite is 2>
						<cfoutput>Injection Error (This message only appears on Dev Site)</cfoutput>
						<cfdump var="#result#">
					</cfif>

					<cfabort>
				</cfif>


			</cffunction>



<!---
queryParam - used by CF_QueryParam for validating datatype
Note that it is primarily for security purposes rather than strict data type checking
So, for example, when testing numeric types we do not actually check that the input is a valid number; just that it is not 'dangerous'.
So we allow 1.1.1 through as numeric even though it is not a valid number
 --->

<cffunction name="queryParam" returns="string" output="false">
	<cfargument name="value" default = "">
	<cfargument name="list" default = "false">
	<cfargument name="cfsqltype" default = "CF_SQL_CHAR">
	<cfargument name="allowSelectStatement" default = "false">
	<cfargument name="isQoQ" default = "false">

	<cfset var ok= false>
	<cfset var result = arguments.value>
	<cfset var regExp = "">
	<cfset var simpleDataType = application.dataTypeLookup[arguments.cfsqltype].simple>
	<cfset var N = 'N'>

	<cfif structKeyExists(arguments,"null") and arguments.null>
		<!--- WAB 2013-11-13 added support for the null attribute as supported by cfqueryparam (for date and numeric fields you can actually just use value="null") --->
		<cfset result = "null">
		<cfset ok = true>
	<cfelse>
		<cfif isQoQ>
			<cfset N = "" >
		</cfif>


		<cfswitch expression = "#simpleDataType#">
			<cfcase value="Numeric">
				<!--- 	WAB 2012-10-18 CASE 431359 Added the negative sign duh!
						WAB 2015-05-17 BF-710 Allow spaces anywhere (primarily leading and trailing, but once you have a list they could be anywhere)
				--->
				<cfset regExp = "\A([0-9\s\.,-]*|null)\Z">
				<cfif refindNoCase(regExp,arguments.value)>
					<cfset ok = true>
				<cfelseif cfsqltype is "CF_SQL_Bit" and isBoolean(arguments.value)> <!--- WAB 2013-06-24 Added support for converting Yes/No to bit 1/0 --->
					<cfset result = iif(result,1,0)>
					<cfset ok = true>
				<cfelseif allowSelectStatement>
					<!--- WAB 2012-11-14 432005 allow white space or open brackets before SELECT --->
					<cfif refindNoCase("\A(\s|\()*select",value) is not 0 and checkStringForInjection(Value,"dangerousSQL").isOK>
						<!--- this makes sure that there are no updates/drops etc in the statement, but cannot check for 1=1 type conditions --->
						<cfset ok = true>
					</cfif>
				</cfif>
			</cfcase>
			<!--- NJH 2016/12/01 JIRA PROD2016-2608 - brought out bit as it's own simple data type.--->
			<cfcase value="bit">
				<cfif isBoolean(arguments.value)>
					<cfset result = iif(result,1,0)>
					<cfset ok = true>
				</cfif>
			</cfcase>

			<cfcase value="text">
				<cfset ok = true>
				<cfset result= escapeSingleQuotes(arguments.value)>
				<cfif arguments.list>
					<cfset result = ListChangeDelims(result,"',#N#'")>
				</cfif>

				<cfset result = "#N#'" & result & "'">

			</cfcase>

			<cfcase value="Date">
				<cfif arguments.value is "" or arguments.value is "null" or arguments.value is "getdate()">
					<cfset ok = true>
				<cfelse>
					<cfif isdate(arguments.value)>
						<cfset ok = true>
						<cfif not isDateOrTimeStamp(arguments.value)>
							<!--- WAB 2013-02-11 add in test for arguments list, other wise fails for dates which have commas in them, eg 'monthName, dd yy hh:mm' --->
							<cfif arguments.list>
								<cfset result = listQualify(arguments.value,"'")>
							<cfelse>
								<cfset result = "'" & arguments.value & "'">
							</cfif>
						</cfif>
					</cfif>
				</cfif>

			</cfcase>

			<cfdefaultcase>

			    CF_SQL_BLOB
			    CF_SQL_CLOB
			    CF_SQL_IDSTAMP
			    CF_SQL_REFCURSOR

			</cfdefaultcase>
		</cfswitch>
	</cfif>

	<cfif not ok>
		<!--- WAB 2016-01-15 PROD2015-518 alter throw to report file and line--->
		<cfset var callingTemplate = application.com.globalfunctions.getFunctionCallingFunction(ignoreFunction = "queryParam.cfm")>
		<cfthrow detail="Data type error ""#arguments.value#"" is not #arguments.cfsqltype#. File:#callingTemplate.templatePath#. Line:#callingTemplate.linenumber#" message = "Data type error #arguments.value# #arguments.cfsqltype# #SimpleDataType#.">
	</cfif>

	<cfreturn result>

</cffunction>

<!--- WAB 2015-11-17 Added this function to support cf_queryObjectName
validates that value is has a pattern which could be a valid object name
basically alphnumeric plus _ . [ ]
--->
<cffunction name="queryObjectName" returns="string" output="false" hint="validates that value could be a valid object name.  Prevents injection into table and column names ">
	<cfargument name="value" required="true">

	<cfif not isValid("regex",arguments.value,application.com.regexp.standardexpressions["queryObjectName"])>
		<cfset var getCallingTemplateDetails = application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction="validsqlname.cfm")>
		<cfthrow detail="Data type error ""#arguments.value#"" is not a valid sql object name. Template #getCallingTemplateDetails.templatePath# line #getCallingTemplateDetails.linenumber#" message = "Data type error ""#arguments.value#"" is not a valid sql object name.">
	</cfif>

	<cfreturn arguments.value>

</cffunction>


<cffunction name="escapeSingleQuotes" output="no">
		<cfargument name="string">
		<cfreturn replaceNoCase(string,"'","''","ALL")>
</cffunction>

<cffunction name="isDateOrTimeStamp" output="no">
		<cfargument name="date">
		<cfset var regexp = "{(ts|d)[0-9 -:',]*?}"> <!--- comma for list --->
		<cfreturn refind(regExp,date)>
</cffunction>






	<cffunction name="listFileSecurity" >
		<cfargument name="topdirectory">  <!--- absolute path --->

		<cfset var root = "">
		<cfset var dir = "">
		<cfset var fileSecurity = "">
		<cfset var thisrelativePath = "">
		<cfset var thislevel = "">
		<cfset var checkTypes = "">
		<cfset var checkType = "">
		<cfset var filename = "">
		<cfset var requirements = '' />
		<cfset var test = '' />

		<cfset arguments.topdirectory = dealWithSlashes(topdirectory)>
		<cfset root = topdirectory>
		<cfset root = replacenocase(root,dealWithSlashes(application.path),"")>
		<cfset root = replacenocase(root,dealWithSlashes(application.userfilesabsolutepath),"")>
		<cfdirectory action="LIST" directory="#topdirectory#" name="Dir" recurse="Yes" filter="*.cfm|*.cfc">
		<cfquery name="dir" dbtype="query">
		select *,  directory + '\' + name as fullpath from dir order by directory,type,name <!--- -- fullpath --->
		</cfquery>

<!--- 	<cfoutput>Directory: #topdirectory# <BR>Root #root# <BR></cfoutput>
 --->
		<cfset fileSecurity = queryNew("type,level,relativePath,name,accessible,loginrequired,internalrequired,securityList,currentUserIsOK,fileSpecific")>

		<cfoutput query="dir" group="directory" >
			<cfset thisrelativePath = replaceNoCase(dealWithSlashes(dir.directory),dealWithSlashes(topdirectory),"")>
			<cfset thislevel = listLen (thisrelativePath,"/")>
			<cfset checkTypes = "dir">
			<cfoutput>
				<cfset checkTypes = listappend(checkTypes,"file")>
				<cfloop index="CheckType" list = "#checkTYpes#">
					<cfset queryAddRow(fileSecurity)>
					<cfif checkType is "dir">
						<cfset fileName = root & thisrelativePath >
						<cfset querySetCell(fileSecurity,"level",thislevel)>
						<!--- Directory : #fileName#<BR> --->
					<cfelse>
						<cfset fileName = root & thisrelativePath & "\" & name>
						<!--- File : #fileName#<BR> --->
						<cfset querySetCell(fileSecurity,"name",name)>
						<cfset querySetCell(fileSecurity,"level",thislevel+1)>
					</cfif>
					<cfset requirements = getFileSecurityRequirements(scriptname=fileName)>
					<cfset test = testFileSecurity(scriptname=filename, includefiles=false)>
					<cfset querySetCell(fileSecurity,"type",checkType)>
					<cfset querySetCell(fileSecurity,"relativePath",thisrelativePath)>
					<cfset querySetCell(fileSecurity,"accessible",requirements.isOK)>
					<cfset querySetCell(fileSecurity,"loginRequired",requirements.LoginRequired)>
					<cfset querySetCell(fileSecurity,"fileSpecific",requirements.fileSpecific)>
					<cfset querySetCell(fileSecurity,"internalRequired",requirements.internalRequired)>
					<cfset querySetCell(fileSecurity,"securityList",requirements.security)>
					<cfset querySetCell(fileSecurity,"currentUserIsOK",test.isOK)>

				</cfloop>
				<cfset checkTypes = "">
			</cfoutput>

		</cfoutput>
<!--- 		<cfdump var="#filesecurity#">  --->
		<cfoutput>Root: #root#</cfoutput>
		<table class="withborders">
			<tr><th></th><th>Is Potentially<BR>Accessible</th><th>Login Required</th><th>Internal Required</th><th>Security List</th><th>Accessible to Current User</th><th></th></tr>

		<cfoutput query="fileSecurity">
			<cfif (type is "dir" or accessible) and left(relativePath,6) is not "/test/">
				<cfif type is "dir" or fileSpecific>
					<tr><td>#repeatString("&nbsp;",(level)*5)# <A href="#root##relativePath#/#name#" target="TestFile"><cfif type is "dir"><font color="<cfif not accessible>green</cfif>">#relativePath#</font><cfelse><font color="red">#name#</font></cfif> </a></td><td>#Accessible#</td><td>#loginrequired#</td><td>#internalrequired#</td><td>#securityList#</td><td>#currentUserIsOK#</td><td></td></tr>
				</cfif>
			</cfif>
		</cfoutput>

		</table>



	</cffunction>



	<cffunction name="listFileSecurityV2" output="false">
		<cfargument name="topdirectory">  <!--- absolute path --->
		<cfargument name="refresh" default = false>

		<cfset var root = "">
		<cfset var allDirectories = "">
		<cfset var fileSecurity = "">
		<cfset var thisrelativePath = "">
		<cfset var thislevel = "">
		<cfset var checkTypes = "">
		<cfset var checkType = "">
		<cfset var filename = "">
		<cfset var x= "">
		<cfset var dummy = "">
		<cfset var previousAccessible = "">
		<cfset var previousLevel = "">
		<cfset var previousHasRegisteredChildren = "">
		<cfset var thisDirRelativePath = "">
		<cfset var filesAndFoldersInDirectory = "">
		<cfset var filesInDirectoryArray = "">
		<cfset var firstpass = "">
		<cfset var thistype = "">
		<cfset var requirements = "">
		<cfset var test = "">
		<cfset var colourscheme = "">
		<cfset var thisfile = "">
		<cfset var afile = "">

	<cfif not structKeyExists(application,"securityReport")>
		<cfset application.securityReport = structNew()>
	</cfif>


	<cfset arguments.topdirectory = dealWithSlashes(arguments.topdirectory)>

	<cfset root = topdirectory>

	<cfset root = replacenocase(root,dealWithSlashes(application.userfilesabsolutepath),"")>
	<cfset root = replacenocase(root,dealWithSlashes(replaceNocase(application.path,"relay","")),"")>
	<cfif refresh or not structKeyExists(application.securityReport,topDirectory) or datediff("m",now(),application.securityReport[topDirectory].lastRefreshed) gt 60>

		<cfdirectory action="LIST" directory="#topdirectory#" name="allDirectories" recurse="Yes" >

		<cfset dummy=querynew("dummy")>
		<cfset queryAddRow(dummy)>

		<cfquery name="allDirectories" dbtype="query">
		select directory,name,type, directory + '\' + name as fulldirectoryname from allDirectories where type='Dir'
		union
		select '#topdirectory#','','Dir','#topdirectory#'		from dummy
		order by fulldirectoryname
		</cfquery>

		<cfloop query="allDirectories">
			<cfset querySetCell(allDirectories,"directory",lcase(directory),currentrow)>
			<cfset querySetCell(allDirectories,"name",lcase(name),currentrow)>
			<cfset querySetCell(allDirectories,"fulldirectoryname",lcase(fulldirectoryname),currentrow)>
		</cfloop>


		<cfquery name="allDirectories" dbtype="query">
		select * from allDirectories
		order by fulldirectoryname
		</cfquery>

<!--- 	<cfoutput>Directory: #topdirectory# <BR>Root #root# <BR></cfoutput>
 --->
		<cfset fileSecurity = queryNew("type,level,relativePath,name,accessible,loginrequired,internalrequired,securityList,allowTrusted,currentUserIsOK,fileSpecific,accessibleToUnknownUser,accessibleToPortalUser")>

		<cfset previousAccessible = true>
		<cfset previousLevel = 0>
		<cfset previousHasRegisteredChildren = true>

		<cfloop query="allDirectories" <!--- endrow = "100"  --->>

			<cfset thisdirrelativePath = lcase(replaceNoCase(dealWithSlashes(fulldirectoryname),dealWithSlashes(topdirectory),""))>
			<cfset thislevel = listLen (thisdirrelativePath,"/")>


			<cfif previousAccessible is false and previousHasRegisteredChildren is false and previousHasRegisteredChildren is false and thisLevel gt previousLevel>
				<!--- Don't bother about this directory, it is the child of a directory that has no access with no children, so is itelf of no importance --->

			<cfelse>

				<cfset filesAndFoldersInDirectory = getRegisteredFilesAndFoldersInADirectory(root & thisdirrelativePath)>
				<cfset filesInDirectoryArray = filesAndFoldersInDirectory.filesArray>

					<cfset firstPass = true>
					<cfloop index="thisFile" list = " ,#arrayToList(filesInDirectoryArray)#">
						<cfset queryAddRow(fileSecurity)>

						<cfif firstPass>
							<cfset querySetCell(fileSecurity,"level",thislevel)>
							<cfset thisType = "Dir">
							<cfset fileName = fulldirectoryname & "/*.*">
							<!--- Directory : #fileName#<BR> --->
							<!--- get an example file in this directory so that we can use it in the link in the report--->
							<cfdirectory action="LIST" directory="#fulldirectoryname#" name="aFile" recurse="no" filter="*.cfm">
							<cfloop query= "aFile">
								<cfif name does not contain "application" and listfindnocase (arrayToList(filesInDirectoryArray),name) is 0>
									<cfset querySetCell(fileSecurity,"name",name)>
									<cfbreak>
								</cfif>
							</cfloop>

						<cfelse>
						 	<cfset fileName = fulldirectoryname & "/" & thisFile>
							<!--- File : #fileName#<BR> --->
							<cfset querySetCell(fileSecurity,"name",thisFile)>
							<cfset querySetCell(fileSecurity,"level",thislevel+1)>
							<cfset thisType = "File">
						</cfif>
						<cfset requirements = getFileSecurityRequirements(scriptname=fileName)>
						<cfset test = testFileSecurity(scriptname=filename, includefiles=false)>
						<cfset querySetCell(fileSecurity,"type",thisType)>
						<cfset querySetCell(fileSecurity,"relativePath",thisdirrelativePath)>
						<cfset querySetCell(fileSecurity,"accessible",requirements.isOK)>
						<cfset querySetCell(fileSecurity,"loginRequired",requirements.LoginRequired)>
						<cfset querySetCell(fileSecurity,"fileSpecific",requirements.fileSpecific)>
						<cfset querySetCell(fileSecurity,"internalRequired",requirements.internalRequired)>
						<cfset querySetCell(fileSecurity,"securityList",requirements.security)>
						<cfset querySetCell(fileSecurity,"currentUserIsOK",test.isOK)>
						<cfset querySetCell(fileSecurity,"accessibleToUnknownUser",iif(requirements.loginRequired is 0 and not requirements.internalRequired and requirements.isOK,true,false))  >
						<cfset querySetCell(fileSecurity,"accessibleToPortalUser",iif(requirements.loginRequired gt 0  and not requirements.internalRequired and requirements.isOK,true,false))  >
						<cfset querySetCell(fileSecurity,"AllowTrusted",requirements.AllowTrusted)>

						<cfif firstPass>
							<cfset previousAccessible = requirements.isOK>
							<cfset previousLevel = thisLevel>
							<cfset previousHasRegisteredChildren = arrayLen(filesAndFoldersInDirectory.filesArray) or arrayLen(filesAndFoldersInDirectory.foldersArray) >
						</cfif>

						<cfset firstPass = false>

					</cfloop>

			</cfif>



		</cfloop>


		<cfset application.securityReport[topDirectory] = structNew()>
		<cfset application.securityReport[topDirectory].lastRefreshed = now()>
		<cfset application.securityReport[topDirectory].query = fileSecurity>

	<cfelse>

		<cfset fileSecurity = application.securityReport[topDirectory].query  >

	</cfif>







<cfset colourScheme = structNew()>
<cfset colourScheme["AccessibleToUnknownUser"] = {colour="red",title="Accessible to Unknown User"}>
<cfset colourScheme["AccessibleToPortalUser"] = {colour="orange",title="Accessible to Portal User"}>
<cfset colourScheme["AccessibleToInternalUser"] = {colour="blue",title="Accessible to Internal User"}>
<cfset colourScheme["AccessibleToTrustedIP"] = {colour="green",title="Accessible to Trusted IP"}>
<cfset colourScheme["NotAccessible"] = {colour="grey",title="Not Accessible to Anyone"}>

<!--- 		<cfquery name="filesecurity" dbtype="query">
		select * from filesecurity
		order by relativePath,type,name
		</cfquery>
 --->

<!--- <cfdump var="#filesecurity#">   --->

<div>
<div style="float: left">
		<table class="withborders">
			<cfoutput><tr><th>#root#</th><th>Is Potentially<BR>Accessible</th><th>Login Required</th><th>Internal Required</th><th>Security List</th><th>Accessible to Current User</th><th></th></tr></cfoutput>

		<cfoutput query="fileSecurity">
			<cfif (type is "dir" or accessible) and left(relativePath,6) is not "/test/">
				<cfif type is "dir" or fileSpecific>
					<tr><td>#repeatString("&nbsp;",(level)*5)# <A href="<cfif root is not "relay">/#root#</cfif>#relativePath#/#name#" target="TestFile"><font color="<cfif accessibleToUnknownUser>#colourScheme.AccessibleToUnknownUser.colour#<cfelseif accessibleToPortalUser>#colourScheme.AccessibleToPortalUser.colour#<cfelseif not accessible>#colourScheme.NotAccessible.colour#<cfelseif  allowTrusted>#colourScheme.AccessibleToTrustedIP.colour#<cfelseif internalRequired>#colourScheme.AccessibleToInternalUser.colour#</cfif>"> <cfif type is "dir">#root##relativePath#<cfelse>#name#</cfif> </a></font></td><td>#Accessible#</td><td>#loginrequired#</td><td>#internalrequired#</td><td>#securityList#</td><td>#currentUserIsOK#</td><td></td></tr>
				</cfif>
			</cfif>
		</cfoutput>

		</table>
</div>
<div style="float: left">

<table>
<tr><Th>Key</TH><th>&nbsp;<BR>&nbsp;</th></tr>
<cfloop item="x" collection = #colourScheme#>
	<cfoutput><tr><td><font color="#colourScheme[x].colour#">#colourScheme[x].title#</font></td></tr></cfoutput>
</cfloop>
</table>

</div>
</div>

	</cffunction>

<!---
ESAPI CALLS
WAB 2012-02
These functions call objects in the ESAPI library
Starting with some encoding functions, not really sure how much different they are from HTMLEditFormat,
but OWASP seems to think that it is very important to encode correctly for the given "context"

To prevent catastrophic failure on systems without ESAPI I am providing fall back functions
As of CF9, ESAPI is a standard part of cold fusion, so can get rid of all the catches

--->
	<!---
		Load up the ESAPI object when cfc if first loaded
		hope it is OK to use a single instance of it
	--->
	<cfset this.esapiObj = CreateObject("java", "org.owasp.esapi.ESAPI")>

	<!--- Not being used yet, but could be useful in the Injection checking --->
	<cffunction name="canonicalize">
		<cfargument name="string" default = "">

		<cfset var result = "">

		<cfset result =this.esapiObj.encoder().canonicalize(JavaCast("string", string))>

		<cfreturn result>



	</cffunction>

	<!--- This is really HTMLEditFormat but might encode / --->
	<cffunction name="encodeForHTML">
		<cfargument name="string" default = "">

		<cfset var result = "">

		<cfset result =  this.esapiObj.encoder().encodeForHTML(JavaCast("string", string))>

		<cfreturn result>

	</cffunction>

	<!--- this is HTMLEdit format for tag attributes, think that main difference from HTMLEditFormat is that ' is encoded
		I am calling this from structureFunctions.convertStructureToNameValuePairs() when encode is set to HTMLAttribute (eg from CF_INPUT)
	--->
	<cffunction name="encodeForHTMLAttribute">
		<cfargument name="string" default = "">

		<cfset var result = "">

		<cfset result = this.esapiObj.encoder().encodeForHTMLAttribute(JavaCast("string", string)) >

		<cfreturn result>


	</cffunction>


	<!--- WAB 2012-10-03 keep having to get all but the last item in a list, so here is a function for it --->
	<cffunction name="listAllButLast">
		<cfargument name="list" required= "true">
		<cfargument name="delimiter" default= ",">
		<cfreturn  reverse(listRest(reverse(list),delimiter))>
	</cffunction>

	<!--- WAB 2012-12-04 CASE 438291 Deal with LoadBalancer --->
	<cffunction name="getRemoteAddress" output="false">
		<cfset var result = cgi.REMOTE_ADDR>
		<cfif structKeyExists (cgi,"HTTP_CLIENT_IP")>
			<cfset result = cgi.HTTP_CLIENT_IP>
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="initialise">
		<cfargument name="applicationScope" default = "#application#">

		<cfset var loadresult = "">
		<cfset variables.applicationScope = applicationScope>

		<cfset loadResult = loadXML(refresh = true)>
		<cfif not loadResult.isOK >
			<cfoutput><font color="red">Security XML Load Failure #loadresult.message#</font></cfoutput>
			<cfif not structKeyExists (applicationScope.xmldocs.security,"DOC")>
				<cfabort>
			</cfif>
		</cfif>


	</cffunction>

</cfcomponent>


