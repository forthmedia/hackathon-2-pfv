<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB 2009/09/07 LID 2515 Removed #application. userfilespath# from function generateCFChartSWFAndJPGfileAndURL 
		added calls to filemanager to get temp folder - but needs testing
--->

<cfcomponent
             hint="I provide methods to manage dashboards" name="relayDashboard"
             access="public">

<!--- ==============================================================================
      2006-04-14 SWJ - started to flesh out the main pats of the cfc                                                   
=============================================================================== --->
<cffunction name="getChartWidgets" output="false" description="Returns a query object which lists the charts to show in a dashboard" 
					access="public" returntype="query">

	<cf_querySim>
	qChartWidgets
	chartSet,chartDataMethod,chartName,chartTitle,chartType
	Lead Pipeline|getLeadPipelineStatusData|leadPipelineStatusChart|Pipeline Status|horizontalBar
	Lead Pipeline|getLeadPipelineRegionData|leadPipelineRegionsChart|Pipeline By Region|pie
	Lead Pipeline|getLeadPipelineMonthData|leadPipelineMonthChart|Pipeline by Month|horizontalBar
	Lead Metrics|getLeadsByStatusData|leadsByStatusChart|Leads by Status|horizontalBar
	Lead Metrics|getLeadsByRegionData|leadsByRegionsChart|Leads By Region|pie
	Lead Metrics|getLeadsByMonthData|leadsByMonthChart|Leads by Month|horizontalBar
	Lead Source|getPipelineByIndustryValue|getPipelineByIndustryValueChart|Pipeline Value By Industry|cone
	Lead Source|getPipelineBySource|getPipelineBySourceChart|Pipeline Value By Source|pie
	Lead Source|getPipelineByIndustryVolume|getPipelineByIndustryVolumeChart|Pipeline Numbers by Industry|line-Bar
	Sales Out|getHistoricSOD|getHistoricSOD|Sales Out by Product Group|horizontalBar
	Sales Out|getRegionalSOD|getRegionalSOD|Sales Out by Country|Bar
	Sales Out|getInventoryByDisti|getInventoryByDisti|Inventory by Distributor|Bar
	Incentive|getAverageIncentiveClaimValue|getAverageIncentiveClaimValue|Average Incentive Claim|horizontalBar
	Incentive|getIncentiveLiabilityByCountry|getIncentiveLiabilityByCountry|Liability by Country|Bar
	Incentive|getIncentiveByDisti|getIncentiveByDisti|Incentive Claims by Distributor|Bar
	eMarketing|getClickThruRates|getClickThruRates|Average Click Thru Rates|pie
	eMarketing|getCommsSent|getCommsSent|Communications Sent|Bar
	eMarketing|geteMailAddressesByCountry|geteMailAddressesByCountry|Email Address Quality|Bar
	</cf_querySim>
	
	<cfreturn qChartWidgets>
</cffunction>

<cffunction name="getDashboardWidgets" output="false" description="Returns a query object which lists the charts to show in a dashboard" 
					access="public" returntype="query">

	<cf_querySim>
	qDashboardWidgets
	dashboardFile,title
	displayChartSet|Lead Pipeline
	displayChartSet|Lead Metrics
	displayChartSet|Lead Source
	displayChartSet|Sales Out
	displayChartSet|Incentive
	displayChartSet|eMarketing
	metrics|Metrics
	</cf_querySim>
	<!--- myContacts|My Contacts
	leadManagerPipeline|Pipeline
	actionsMyActions|My Actions --->

	<cfreturn qDashboardWidgets>
</cffunction>

<!--- ==============================================================================
                                                         
=============================================================================== --->

	<cffunction access="public" name="generateCFChartSWFfile" hint="returns a path to a swf for a given chart">
		<cfargument name="chartDataMethod" type="string" required="yes">
		<cfargument name="chartName" type="string" required="yes">
		<cfargument name="chartTitle" type="string" required="yes">
		<cfargument name="chartType" type="string" default="horizontalbar">
		<cfargument name="chartFormat" type="string" default="flash">
		<cfargument name="chartData" type="any" required="false">
		
		<cfset var chartFormatExtension = "swf">
		<!--- WAB 2009/09/07 LID 2515 --->
		<cfset var TemporaryFolderdetails = application.com.filemanager.getTemporaryFolderDetails()>
		
		<cfif arguments.chartFormat is not "flash">
			<cfset chartFormatExtension = arguments.chartFormat>
		</cfif>
		
		
		<cftry>
			<!--- create a temp file location to store the generated cfchart --->

			<cfset tempFileName = "#TemporaryFolderdetails##arguments.chartName#.#chartFormatExtension#">
			<!--- escape the \ slashes as these need to be escaped to be used in a cfinput field --->
			<cfset tempFileName = replace(tempfileName,"\","\\","all")>
			
			<!--- get the data to display --->
			<cfif isDefined("arguments.chartData")>
				<cfset qChartData = chartData>
			<cfelse>
				<cfinvoke component="relay.com.relayDashboardData" method="#arguments.chartDataMethod#" returnvariable="qChartData"></cfinvoke>
			</cfif>
			
			<!--- generate the chart swf.  Note: using the name argument puts the swf binary into a variable of that 
				name so yu can write it to disk in the cfWrite--->
			
			<cf_chart format="#arguments.chartFormat#" chartheight="#request.chartHeight#" style="#request.chartStyle#" title="#arguments.chartTitle#" chartwidth="#request.chartWidth#" seriesplacement="default" labelformat="number" show3d="yes" xoffset=".04" yoffset=".04" tipstyle="mouseOver" pieslicestyle="sliced" name="#arguments.chartName#">
				<cfif isQuery(qChartData)>
					<!--- here were just displaying a single chart --->
					<cf_chartseries type="#arguments.chartType#" query="qChartData" itemcolumn="chart_Label" valuecolumn="value" 
						serieslabel="Status" paintstyle="#request.chartPaintstyle#"></cf_chartseries>
				<cfelse>
					<cfif isStruct(qChartData)>
						<!--- here we're displaying more than one chart delivered as a structure of queries which we're looping over 
						to pull out each data series --->
						<cfset loopCounter = 1>
						<cfloop collection="#qChartData#" item = "dataSet">
						<!--- <cf_chartseries type="line" query="qChartData.dataSet" itemcolumn="chart_Label" 
							valuecolumn="value" serieslabel="Target" seriescolor="##4F4F4F" datalabelstyle="columnLabel" paintstyle="plain"></cf_chartseries> --->
							<cfif listLen(arguments.chartType,"-") gt 1>
								<cfset thisChartType = arguments.chartType>
								<!--- if there is a list of chartTypes delimited by "-" then set this var so we can test for it in the loop in a sec --->
								<cfif listLen(arguments.chartType,"-") lte structCount(qChartData)>
									<cfset thisChartType = listGetAt(arguments.chartType,loopCounter,"-")>
								<cfelse>
									<cfset thisChartType = listGetAt(arguments.chartType,listLen(arguments.chartType,"-"),"-")>
								</cfif>
							<cfelse>
								<cfset thisChartType = arguments.chartType>
							</cfif>
							<cfset thisSeriesData = structFind(qChartData,dataSet)>
							<cfif thisChartType eq "line">
								<cf_chartseries type="line" query="thisSeriesData" itemcolumn="chart_Label" 
									valuecolumn="value" serieslabel="#dataSet#" seriescolor="##4F4F4F" datalabelstyle="columnLabel" paintstyle="plain"></cf_chartseries>
							<cfelseif thisChartType eq "bar">
								<cf_chartseries type="#thisChartType#" query="thisSeriesData" itemcolumn="chart_Label" valuecolumn="value" 
									serieslabel="#dataSet#" paintstyle="#request.chartPaintstyle#" datalabelstyle="rowlabel"></cf_chartseries>
							<cfelse>
								<cf_chartseries type="#thisChartType#" query="thisSeriesData" itemcolumn="chart_Label" valuecolumn="value" 
									serieslabel="#dataSet#" paintstyle="#request.chartPaintstyle#"></cf_chartseries>
							</cfif>
							<cfset loopCounter = loopCounter + 1>
						</cfloop>
					</cfif>
				</cfif>
			</cf_chart>
			
			<cffile action="write" file="#tempFileName#" output="#evaluate(arguments.chartName)#">
		<cfcatch type="any"><cfreturn 0></cfcatch>
		</cftry>
		<!--- return the file location so the calling file can use the swf --->
		<cfreturn tempFileName>
	</cffunction>
	
	<cffunction access="public" name="generateCFChartSWFAndJPGfileAndURL" hint="returns an url to a swf for a given chart">
		<cfargument name="chartDataMethod" type="string" required="yes">
		<cfargument name="chartName" type="string" required="yes">
		<cfargument name="chartTitle" type="string" required="yes">
		<cfargument name="chartType" type="string" default="horizontalbar">
		<cfset var qChartData = "">
		<!--- create a temp file location to store the generated cfchart --->
		<cfinvoke component="relay.com.relayDashboardData" method="#arguments.chartDataMethod#" returnvariable="qChartData"></cfinvoke>
		
		<cfscript>
			writeChartSWF = generateCFChartSWFfile(arguments.chartDataMethod,arguments.chartName,arguments.chartTitle,arguments.chartType,"flash",qChartData);
			writeChartJPG = generateCFChartSWFfile(arguments.chartDataMethod,arguments.chartName,arguments.chartTitle,arguments.chartType,"jpg",qChartData);
		</cfscript>
		
		<!--- WAB 2009/09/07 LID 2515 Removed #application. userfilespath#, actually decided to generate URL by replacing stuff in the absolute filename (ensures all in synch) --->		
		<!--- 	<cfset tempFileURL = "#application. UserFilesPath#/temp/#arguments.chartName#-#request.relayCurrentUser.personid#">		 --->		
		<cfset tempFileURL = replaceNoCase (writeChartJPG,application.UserFilesAbsolutePath,"","ONCE")>
		<cfset tempFileURL = replaceNoCase (writeChartJPG,"\","/","ALL")>
		<cfset tempFileURL = replaceNoCase (writeChartJPG,".jpg","","ONCE")>
		
		
		
		<cfreturn tempFileURL>
	</cffunction>

<!--- ==============================================================================
  DRAW A CFCHART                                                       
=============================================================================== --->

	<cffunction access="public" name="generateCFChart" hint="returns a chart">
		<cfargument name="chartDataMethod" type="string" required="yes">
		<cfargument name="chartName" type="string" required="yes">
		<cfargument name="chartTitle" type="string" required="yes">
		<cfargument name="chartType" type="string" default="horizontalbar">
		
		<cfargument name="organisationID" type="numeric" required="no">
		<cfargument name="numOfMonths" type="numeric" required="no">
		<cfargument name="productCode" type="string" required="no">
		
		<cfset args = StructNew()>
		<cfif isDefined("arguments.organisationID")>
			<cfset args.organisationID = "#arguments.organisationID#">
		</cfif>
		<cfif isDefined("arguments.numOfMonths")>
			<cfset args.numOfMonths = "#arguments.numOfMonths#">
		</cfif>
		<cfif isDefined("arguments.productCode")>
			<cfset args.productCode = "#arguments.productCode#">
		</cfif>
				
		<!--- get the data to display --->
		<cfinvoke component="relay.com.relayDashboardData" method="#arguments.chartDataMethod#" returnvariable="qChartData" argumentCollection="#args#"></cfinvoke>
		
		<!--- generate the chart swf.  Note: using the name argument puts the swf binary into a variable of that 
			name so yu can write it to disk in the cfWrite--->
		<cf_chart format="flash" chartheight="#request.chartHeight#" style="#request.chartStyle#" title="#arguments.chartTitle#" chartwidth="#request.chartWidth#" seriesplacement="default" labelformat="number" show3d="yes" xoffset=".04" yoffset=".04" tipstyle="mouseOver" pieslicestyle="sliced">
			<cfif isQuery(qChartData)>
				<!--- here were just displaying a single chart --->
				<cf_chartseries type="#arguments.chartType#" query="qChartData" itemcolumn="chart_Label" valuecolumn="value" 
					serieslabel="Status" paintstyle="#request.chartPaintstyle#"></cf_chartseries>
			<cfelse>
				<cfif isStruct(qChartData)>
					<!--- here we're displaying more than one chart delivered as a structure of queries which we're looping over 
					to pull out each data series --->
					<cfset loopCounter = 1>
					<cfloop collection="#qChartData#" item = "dataSet">
					<!--- <cf_chartseries type="line" query="qChartData.dataSet" itemcolumn="chart_Label" 
						valuecolumn="value" serieslabel="Target" seriescolor="##4F4F4F" datalabelstyle="columnLabel" paintstyle="plain"></cf_chartseries> --->
						<cfif listLen(arguments.chartType,"-") gt 1>
							<cfset thisChartType = arguments.chartType>
							<!--- if there is a list of chartTypes delimited by "-" then set this var so we can test for it in the loop in a sec --->
							<cfif listLen(arguments.chartType,"-") lte structCount(qChartData)>
								<cfset thisChartType = listGetAt(arguments.chartType,loopCounter,"-")>
							<cfelse>
								<cfset thisChartType = listGetAt(arguments.chartType,listLen(arguments.chartType,"-"),"-")>
							</cfif>
						<cfelse>
							<cfset thisChartType = arguments.chartType>
						</cfif>
						<cfset thisSeriesData = structFind(qChartData,dataSet)>
						<cfif thisChartType eq "line">
							<cf_chartseries type="line" query="thisSeriesData" itemcolumn="chart_Label" 
								valuecolumn="value" serieslabel="#dataSet#" seriescolor="##4F4F4F" datalabelstyle="columnLabel" paintstyle="plain"></cf_chartseries>
						<cfelseif thisChartType eq "bar">
							<cf_chartseries type="#thisChartType#" query="thisSeriesData" itemcolumn="chart_Label" valuecolumn="value" 
								serieslabel="#dataSet#" paintstyle="#request.chartPaintstyle#" datalabelstyle="rowlabel"></cf_chartseries>
						<cfelse>
							<cf_chartseries type="#thisChartType#" query="thisSeriesData" itemcolumn="chart_Label" valuecolumn="value" 
								serieslabel="#dataSet#" paintstyle="#request.chartPaintstyle#"></cf_chartseries>
						</cfif>
						<cfset loopCounter = loopCounter + 1>
					</cfloop>
				</cfif>
			</cfif>
		</cf_chart>
		
	</cffunction>

<!--- =============================================================================
	functions to manage display of grid data
============================================================================== --->

<cffunction access="public" name="getGridData" hint="returns a structure with a query and other grid data">
	<cfargument name="chartData" type="any" required="yes">
	
	<cfset var sGridData = structNew()>
	<cfset var returnStruct = structNew()>
	
	<cfif isQuery(chartData)>
		<cfset sGridData["qchartData"] = chartData>
	<cfelse>
		<cfset sGridData = chartData>
	</cfif>
	<cfset incr = 1>
	<cfloop collection="#sGridData#" item="sk">
		<cfset qColValues = sGridData[sk]>
		<cfif incr eq 1>
			<cfset queryCol = "#listFirst(sGridData[sk].columnList)#">
			
			<CFSET nColList = evaluate("valueList(qColValues.#queryCol#)")>
			<cfset nColNames = "">
			<cfloop from="1" to="#listLen(nColList)#" index="x">
				<cfset nColNames = listAppend(nColNames,"col#x#")>
			</cfloop>
			<cfscript>
				qGridData = queryNew(nColNames);
			</cfscript>
			<!--- <cfloop from="1" to="#listLen(nColNames)#" index="cIncr">
				<cfscript>
					querySetCell(qGridData,listGetAt(nColNames,cIncr),listGetAt(nColList,cIncr));
				</cfscript>
			</cfloop> --->
		</cfif>
		
		<cfloop list="#listDeleteAt(sGridData[sk].columnList,1)#" index="col">
			
			<cfset nColValues = evaluate("valueList(qColValues.#col#,'|')")>
			<cfscript>
				queryAddRow(qGridData,1);
			</cfscript>
			
			<cfloop from="1" to="#listLen(nColNames)#" index="cIncr">
				<cfset cValue = listGetAt(nColValues,cIncr,"|")>
				<cfif isNumeric(cValue)>
					<cfset cValue = numberFormat(cValue,",")>
				</cfif>
				<cfscript>
					querySetCell(qGridData,listGetAt(nColNames,cIncr),cValue);
				</cfscript>
			</cfloop>
		</cfloop>
		<cfset incr = incr+1>
	</cfloop>
	
	<cfset returnStruct.qData = qGridData>
	<cfset returnStruct.colNames = nColNames>
	<cfset returnStruct.colList = nColList>
	
	<cfreturn returnStruct>
	
</cffunction>

<cffunction name="manageDashboardDefaults" access="public" hint="Sets a user's prefs according to defaults if not already set.">
	<cfargument name="xmlFile" required="no" type="string" default="RelayDashboardList.xml">

	<cfset var RelayDashboardFile=replace("#application.paths.relayware#\xml\#arguments.xmlFile#","/","\")>
	<cfset var qTabItems = "">
	<cfset var dashboardTabs = "">
	<cfset var tabSecurityLevel = "">
	<cfset var userTabPrefs = "">
	<cfset var myxmldoc = "">
	<cfset var size = 0>
	<cfset var tabID = 0>
	<cfset var tabIdx = 0>
	
	<cfif not fileExists("#RelayDashboardFile#")>
		<cfset AttributesError="No #arguments.xmlFile# file found; looking for #RelayDashboardFile#">
	</cfif>		
	
	<cftry>
		<cffile action="read"
				file="#application.paths.relayware#\xml\#arguments.xmlFile#"
				variable="myxml">
		<cfcatch type="Any">XML file read error</cfcatch>
	</cftry> 
	
	<cfset 	myxmldoc = XmlParse(myxml)>
	<cfset dashboardTabs = XmlSearch(myxmldoc,"/Dashboard/DashboardTab")>
	
	<cfset size = ArrayLen(dashboardTabs)>
	<cfif size gt 0>
		<cfloop index="tabIdx" from="1" to="#size#">
			<!--- NJH 2006/07/07 Do we have rights to see this reports? --->
			<cfset tabSecurityLevel = dashboardTabs[tabIdx].XmlAttributes.Security>
			<cfif application.com.login.checkInternalPermissions(listfirst(tabSecurityLevel,":"),listlast(tabSecurityLevel,":"))>
				<cfset tabID = dashboardTabs[tabIdx].XmlAttributes.id>
				<cfset userTabPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#")>
				<cfset qTabItems = getTabItemsInModule(dashboardTabs[tabIdx].XmlAttributes.module,arguments.xmlFile)>
				<cfif isQuery(qTabItems)>
					<cfloop query="qTabItems">
						<cfif not structKeyExists(userTabPrefs,"chart#id#") or not structKeyExists(userTabPrefs["chart#id#"],"data_customisation")>
							<cfset userTabItemPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#.chart#id#")>
							<cfset userTabItemPrefs.data_customisation = structNew()>
							<cfset userTabItemPrefs.data_customisation.custom1 = structNew()>
							<cfset userTabItemPrefs.data_customisation.custom1.chartTitle = chartTitle>
							<cfset userTabItemPrefs.data_customisation.custom1.chartName = chartName>
							<cfif isDefault>
								<cfset userTabItemPrefs.data_customisation.custom1.display = true>
							<cfelse>
								<cfset userTabItemPrefs.data_customisation.custom1.display = false>
							</cfif>
							<cfset application.com.preferences.saveUserPreference(hive="dashboard",key="tab#tabID#.chart#id#",data=userTabItemPrefs)>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
		</cfloop>
	</cfif>
	<!--- <cfset userTabPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="")>
	<cfdump var="#userTabPrefs#"> --->
</cffunction>

<cffunction name="getDashboardTabs" access="public" hint="Returns a query of tabs within the dashboard XML structure that the user has access to">
	<cfargument name="xmlFile" required="no" type="string" default="RelayDashboardList.xml">
	<cfset var dashboardTabs = "">
	<cfset var tabQuery = "">
	<cfset var tabSecurityLevel = "">
	<cfset var myxmldoc = "">
	<cfset var size = 0>
	<cfset var temp = "">
	<cfset RelayDashboardFile=replace("#application.paths.relayware#\xml\#arguments.xmlFile#","/","\")>
	
	<cfif not fileExists("#RelayDashboardFile#")>
		<cfset AttributesError="No #arguments.xmlFile# file found; looking for #RelayDashboardFile#">
	</cfif>		
	
	<cftry>
		<cffile action="read"
				file="#application.paths.relayware#\xml\#arguments.xmlFile#"
				variable="myxml">
		<cfcatch type="Any">XML file read error</cfcatch>
	</cftry> 
	
	<cfset 	myxmldoc = XmlParse(myxml)>
	<cfset dashboardTabs = XmlSearch(myxmldoc,"/Dashboard/DashboardTab")>
	
	<cfset size = ArrayLen(dashboardTabs)>
	<cfif size gt 0>
		<cfset tabQuery = QueryNew("id,Module,DisplayFile,Security")>
		<cfset temp = QueryAddRow(tabQuery,#size#)>
		<cfloop index="tabIdx" from="1" to="#size#">
			<!--- NJH 2006/07/07 Do we have rights to see this reports? --->
			<cfset tabSecurityLevel = dashboardTabs[tabIdx].XmlAttributes.Security>
			<cfif application.com.login.checkInternalPermissions(listfirst(tabSecurityLevel,":"),listlast(tabSecurityLevel,":"))>
				<cfset temp = QuerySetCell(tabQuery,"id",#dashboardTabs[tabIdx].XmlAttributes.id#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabQuery,"Module",#dashboardTabs[tabIdx].XmlAttributes.Module#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabQuery,"DisplayFile",#dashboardTabs[tabIdx].XmlAttributes.DisplayFile#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabQuery,"Security",#dashboardTabs[tabIdx].XmlAttributes.Security#,#tabIdx#)>
			</cfif>
		</cfloop>
		
		<cfquery name="myTabs" dbtype="query">
			select * from tabQuery where id is not null
		</cfquery>
		
		<cfreturn myTabs>
	<cfelse>
		<cfreturn false>
	</cfif>
</cffunction>

<cffunction name="getUserTabItemsInModule" access="public" hint="Returns a query of tabs items within the dashboard XML structure that the user has access to">
	<cfargument name="module" required="yes" type="string">
	<cfargument name="xmlFile" required="no" type="string" default="RelayDashboardList.xml">
	<cfset var qTabItems = getTabItemsInModule(arguments.module,arguments.xmlFile)>
	<cfset var finalTabItems = queryNew(listAppend(qTabItems.columnList,"customID"))>
	<cfset var userChartPrefs = "">
	<cfset var tabID = getTabID(arguments.module,arguments.xmlFile)>
	<cfset var finalIDList = "">

	<cfloop query="qTabItems">
		<cfset userChartPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#.chart#id#")>
		<cfif structKeyExists(userChartPrefs,"data_customisation") and structCount(userChartPrefs.data_customisation) gt 0>
			<cfloop collection="#userChartPrefs.data_customisation#" item="chartKey">
				<cfif structKeyExists(userChartPrefs.data_customisation[chartKey],"display")>
					<cfif userChartPrefs.data_customisation[chartKey].display>
						<cfscript>
							queryAddRow(finalTabItems,1);
							querySetCell(finalTabItems,"customID",chartKey);
						</cfscript>
						<cfloop list="#columnList#" index="col">
							<cfscript>
								querySetCell(finalTabItems,col,evaluate(col));
							</cfscript>
						</cfloop>
					</cfif>
				</cfif>
			</cfloop>
			
		</cfif>
	</cfloop>
	<cfif finalTabItems.recordCount gt 0>
		<cfset finalTabItems = setChartUserData(finalTabItems,"chartTitle",tabID)>
		<cfset finalTabItems = setChartUserData(finalTabItems,"chartName",tabID)>
	</cfif>
	
	<cfreturn finalTabItems>
</cffunction>

<cffunction name="getAllUserTabItemsInModule" access="public" hint="Returns a query of tabs items within the dashboard XML structure that the user has access to">
	<cfargument name="module" required="yes" type="string">
	<cfargument name="xmlFile" required="no" type="string" default="RelayDashboardList.xml">
	<cfset var qTabItems = getTabItemsInModule(arguments.module,arguments.xmlFile)>
	<cfset var finalTabItems = queryNew(listAppend(qTabItems.columnList,"customID"))>
	<cfset var userChartPrefs = "">
	<cfset var tabID = getTabID(arguments.module,arguments.xmlFile)>
	<cfset var finalIDList = "">

	<cfloop query="qTabItems">
		<cfset userChartPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#.chart#id#")>
		<cfif structKeyExists(userChartPrefs,"data_customisation") and structCount(userChartPrefs.data_customisation) gt 0>
			<cfloop collection="#userChartPrefs.data_customisation#" item="chartKey">
				<cfscript>
					queryAddRow(finalTabItems,1);
					querySetCell(finalTabItems,"customID",chartKey);
				</cfscript>
				<cfloop list="#columnList#" index="col">
					<cfset value = evaluate(col)>
					<cfif lCase(col) eq "chartname">
						<cfset value = userchartPrefs.data_customisation[chartKey].chartName>
					</cfif>
					<cfscript>
						querySetCell(finalTabItems,col,value);
					</cfscript>
				</cfloop>
			</cfloop>
		</cfif>
	</cfloop>
	<cfif finalTabItems.recordCount gt 0>
		<cfset finalTabItems = setChartUserData(finalTabItems,"chartTitle",tabID)>
	</cfif>
	
	<cfreturn finalTabItems>
</cffunction>

<cffunction name="setChartUserData" access="public" hint="Returns a query of tabs items with user data instead of xml data.">
	<cfargument name="qTabItems" required="yes" type="query">
	<cfargument name="colName" required="yes" type="string">
	<cfargument name="tabID" required="true" type="any">
	<cfset var qReturn = queryNew(qTabItems.columnList)>
	<cfset var userPrefs = "">
	
	<cfloop query="qTabItems">
		<cfset userPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#arguments.tabID#.chart#id#.data_customisation.#customID#")>
		<cfscript>
			queryAddRow(qReturn,1);
		</cfscript>
		<cfloop list="#columnList#" index="col">
			<cfif lCase(col) eq lCase(arguments.colName) and structKeyExists(userPrefs,arguments.colName)>
				<cfscript>
					querySetCell(qReturn,col,userPrefs[arguments.colName]);
				</cfscript>
			<cfelse>
				<cfscript>
					querySetCell(qReturn,col,evaluate(col));
				</cfscript>
			</cfif>
		</cfloop>
	</cfloop>
	
	<cfreturn qReturn>
</cffunction>

<cffunction name="getTabItemsInModule" access="public" hint="Returns a query of tabs items within the dashboard XML structure that the user has access to">
	<cfargument name="module" required="yes" type="string">
	<cfargument name="xmlFile" required="no" type="string" default="RelayDashboardList.xml">
	<cfset var itemsInTabModule = "">
	<cfset var tabItemsQuery = "">
	<cfset var tabSecurityLevel = "">
	<cfset var myxmldoc = "">
	<cfset var size = 0>
	<cfset var temp = "">
	<cfset RelayDashboardFile=replace("#application.paths.relayware#\xml\#arguments.xmlFile#","/","\")>
	
	<cfif not fileExists("#RelayDashboardFile#")>
		<cfset AttributesError="No #arguments.xmlFile# file found; looking for #RelayDashboardFile#">
	</cfif>		
	
	<cftry>
		<cffile action="read"
				file="#application.paths.relayware#\xml\#arguments.xmlFile#"
				variable="myxml">
		<cfcatch type="Any">XML file read error</cfcatch>
	</cftry> 
	
	<cfset 	myxmldoc = XmlParse(myxml)>
	<cfset itemsInTabModule = XmlSearch(myxmldoc,"/Dashboard/DashboardTab[@module='#arguments.module#']/DashboardItem")>
	
	<cfset size = ArrayLen(itemsInTabModule)>
	<cfif size gt 0>
		<cfset tabItemsQuery = QueryNew("id,isDefault,ChartDataMethod,ChartName,ChartTitle,ChartType,Security")>
		<cfset temp = QueryAddRow(tabItemsQuery,#size#)>
		<cfloop index="tabIdx" from="1" to="#size#">
			<!--- NJH 2006/07/07 Do we have rights to see this reports? --->
			<cfset tabSecurityLevel = itemsInTabModule[tabIdx].XmlAttributes.Security>
			<cfif application.com.login.checkInternalPermissions(listfirst(tabSecurityLevel,":"),listlast(tabSecurityLevel,":"))>
				<cfset temp = QuerySetCell(tabItemsQuery,"id",#itemsInTabModule[tabIdx].XmlAttributes.id#,#tabIdx#)>
				<cfif structKeyExists(itemsInTabModule[tabIdx].XmlAttributes,"isDefault")>
					<cfset temp = QuerySetCell(tabItemsQuery,"isDefault",#itemsInTabModule[tabIdx].XmlAttributes.isDefault#,#tabIdx#)>
				<cfelse>
					<cfset temp = QuerySetCell(tabItemsQuery,"isDefault",false,#tabIdx#)>
				</cfif>
				<cfset temp = QuerySetCell(tabItemsQuery,"ChartDataMethod",#itemsInTabModule[tabIdx].XmlAttributes.ChartDataMethod#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabItemsQuery,"ChartName",#itemsInTabModule[tabIdx].XmlAttributes.ChartName#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabItemsQuery,"ChartTitle",#itemsInTabModule[tabIdx].XmlAttributes.ChartTitle#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabItemsQuery,"ChartType",#itemsInTabModule[tabIdx].XmlAttributes.ChartType#,#tabIdx#)>
				<cfset temp = QuerySetCell(tabItemsQuery,"Security",#itemsInTabModule[tabIdx].XmlAttributes.Security#,#tabIdx#)>
			</cfif>
		</cfloop>
		
		<cfquery name="myTabData" dbtype="query">
			select * from tabItemsQuery where id is not null
		</cfquery>
		
		<cfreturn myTabData>
	<cfelse>
		<cfreturn false>
	</cfif>
</cffunction>

<cffunction name="getTabID" access="public" hint="Returns an ID for a tab based on tab module">
	<cfargument name="module" required="yes" type="string">
	<cfargument name="xmlFile" required="no" type="string" default="RelayDashboardList.xml">
	<cfset var tabDetail = "">
	<cfset var myxmldoc = "">
	<cfset var size = 0>
	<cfset RelayDashboardFile=replace("#application.paths.relayware#\xml\#arguments.xmlFile#","/","\")>
	
	<cfif not fileExists("#RelayDashboardFile#")>
		<cfset AttributesError="No #arguments.xmlFile# file found; looking for #RelayDashboardFile#">
	</cfif>		
	
	<cftry>
		<cffile action="read"
				file="#application.paths.relayware#\xml\#arguments.xmlFile#"
				variable="myxml">
		<cfcatch type="Any">XML file read error</cfcatch>
	</cftry> 
	
	<cfset myxmldoc = XmlParse(myxml)>
	<cfset tabDetail = XmlSearch(myxmldoc,"/Dashboard/DashboardTab[@module='#arguments.module#']")>
	
	<cfset size = ArrayLen(tabDetail)>
	<cfif size gt 0>
		<cfreturn tabDetail[1].XmlAttributes.id>
	<cfelse>
		<cfreturn false>
	</cfif>
</cffunction>

<!--- ==============================================================================
   The functions provide dummy data for illustration                                                      
=============================================================================== --->
<cffunction name="getAll" output="false" description="Returns all the contacts in the database" 
					access="public" returntype="query">
	
	<cfscript>
	var memberList = queryNew("id, photo, firstName, lastName, email, phone, fax,  cell,  
								address, zip, city, state,  company, job, website, comments");

	queryAddRow(memberList);
		querySetCell(memberList,'id','1');
		querySetCell(memberList,'firstName','Alan');
		querySetCell(memberList,'lastName','Williams');
		querySetCell(memberList,'email','alan@adobe.com');
		querySetCell(memberList,'phone','212-253-4000');
		querySetCell(memberList,'fax','212-253-4000');
		querySetCell(memberList,'cell','212-256-2546');
		querySetCell(memberList,'photo','alan.jpg');
		querySetCell(memberList,'address','250 5th Street');
		querySetCell(memberList,'zip','42654');
		querySetCell(memberList,'city','New York');
		querySetCell(memberList,'state','NY');
		querySetCell(memberList,'company','Adobe');
		querySetCell(memberList,'job','CEO');
		querySetCell(memberList,'website','http://www.adobe.com');
		querySetCell(memberList,'comments','IBM on Friday detailed an initiative intended to accelerate the business benefits of an SOA. The Business Integration Adoption Model');
		
	queryAddRow(memberList);
		querySetCell(memberList,'id','6');
		querySetCell(memberList,'firstName','Tom');
		querySetCell(memberList,'lastName','Smith');
		querySetCell(memberList,'email','tom@macromedia.com');
		querySetCell(memberList,'photo','tom.jpg');
		querySetCell(memberList,'phone','310-253-4586');
		querySetCell(memberList,'fax','310-253-0000');
		querySetCell(memberList,'cell','310-256-2546');
		querySetCell(memberList,'address','20 Alvarado St');
		querySetCell(memberList,'zip','42054');
		querySetCell(memberList,'city','Los Angeles');
		querySetCell(memberList,'state','CA');
		querySetCell(memberList,'company','Macromedia');
		querySetCell(memberList,'job','Manager');
		querySetCell(memberList,'website','http://www.macromedia.com');
		querySetCell(memberList,'comments','IBM on Friday detailed an initiative intended to accelerate the business benefits of an SOA. The Business Integration Adoption Model');
	queryAddRow(memberList);
		querySetCell(memberList,'id','8');
		querySetCell(memberList,'firstName','Andy');
		querySetCell(memberList,'lastName','Sailor');
		querySetCell(memberList,'email','andy@earthlink.net');
		querySetCell(memberList,'photo','andy.jpg');
		querySetCell(memberList,'phone','949-253-4586');
		querySetCell(memberList,'fax','310-253-4548');
		querySetCell(memberList,'cell','949-256-2546');
		querySetCell(memberList,'address','250 Cozumel');
		querySetCell(memberList,'zip','92677');
		querySetCell(memberList,'city','Laguna Niguel');
		querySetCell(memberList,'state','CA');
		querySetCell(memberList,'company','Blue Instant');
		querySetCell(memberList,'job','Cold Fusion Developer');
		querySetCell(memberList,'website','http://www.asfusion.com');
		querySetCell(memberList,'comments','IBM on Friday detailed an initiative intended to accelerate the business benefits of an SOA. The Business Integration Adoption Model');
		
	queryAddRow(memberList);
		querySetCell(memberList,'id','6');
		querySetCell(memberList,'firstName','Alex');
		querySetCell(memberList,'lastName','Peterson');
		querySetCell(memberList,'email','alex@yahoo.com');
		querySetCell(memberList,'photo','alex.jpg');
		querySetCell(memberList,'phone','310-253-4586');
		querySetCell(memberList,'fax','310-253-0000');
		querySetCell(memberList,'cell','310-256-2546');
		querySetCell(memberList,'address','20 Alvarado St');
		querySetCell(memberList,'zip','42004');
		querySetCell(memberList,'city','Los Angeles');
		querySetCell(memberList,'state','CA');
		querySetCell(memberList,'company','Macromedia');
		querySetCell(memberList,'job','Manager');
		querySetCell(memberList,'website','http://www.macromedia.com');
		querySetCell(memberList,'comments','IBM on Friday detailed an initiative intended to accelerate the business benefits of an SOA. The Business Integration Adoption Model');
		
	queryAddRow(memberList);
		querySetCell(memberList,'id','6');
		querySetCell(memberList,'firstName','Tim');
		querySetCell(memberList,'lastName','Johnson');
		querySetCell(memberList,'email','tim@ibm.com');
		querySetCell(memberList,'photo','');
		querySetCell(memberList,'phone','310-253-4586');
		querySetCell(memberList,'fax','310-253-0000');
		querySetCell(memberList,'cell','310-256-2546');
		querySetCell(memberList,'address','20 Mountain View');
		querySetCell(memberList,'zip','95644');
		querySetCell(memberList,'city','Los Angeles');
		querySetCell(memberList,'state','CA');
		querySetCell(memberList,'company','IBM');
		querySetCell(memberList,'job','CFO');
		querySetCell(memberList,'website','http://www.ibm.com');
		querySetCell(memberList,'comments','IBM on Friday detailed an initiative intended to accelerate the business benefits of an SOA. The Business Integration Adoption Model');
		
		return memberList;
</cfscript>

</cffunction>

<cffunction name="getContactsShort" output="false" description="Returns all the contacts in the database" 
					access="public" returntype="query">
	
	<cfscript>
	memberList = queryNew("id,name,age,gender,department");
	queryAddRow(memberList);
		querySetCell(memberList,'id','1');
		querySetCell(memberList,'name','Adams');
		querySetCell(memberList,'age','23');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','6');
		querySetCell(memberList,'name','Alan');
		querySetCell(memberList,'age','30');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','2');
	queryAddRow(memberList);
		querySetCell(memberList,'id','7');
		querySetCell(memberList,'name','Albert');
		querySetCell(memberList,'age','25');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','3');
	queryAddRow(memberList);
		querySetCell(memberList,'id','8');
		querySetCell(memberList,'name','Alex');
		querySetCell(memberList,'age','40');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','4');
	queryAddRow(memberList);
		querySetCell(memberList,'id','9');
		querySetCell(memberList,'name','Alexander');
		querySetCell(memberList,'age','38');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','5');
	queryAddRow(memberList);
		querySetCell(memberList,'id','10');
		querySetCell(memberList,'name','Alicia');
		querySetCell(memberList,'age','23');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','11');
		querySetCell(memberList,'name','Alfred');
		querySetCell(memberList,'age','26');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','12');
		querySetCell(memberList,'name','Alison');
		querySetCell(memberList,'age','31');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','3');
	queryAddRow(memberList);
		querySetCell(memberList,'id','2');
		querySetCell(memberList,'name','Arnold');
		querySetCell(memberList,'age','50');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','4');
	queryAddRow(memberList);
		querySetCell(memberList,'id','3');
		querySetCell(memberList,'name','Ann');
		querySetCell(memberList,'age','55');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','5');
	queryAddRow(memberList);
		querySetCell(memberList,'id','24');
		querySetCell(memberList,'name','Annie');
		querySetCell(memberList,'age','35');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','4');
		querySetCell(memberList,'name','Tom');
		querySetCell(memberList,'age','54');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','2');
	queryAddRow(memberList);
		querySetCell(memberList,'id','5');
		querySetCell(memberList,'name','Timoty');
		querySetCell(memberList,'age','26');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','3');
	queryAddRow(memberList);
		querySetCell(memberList,'id','5');
		querySetCell(memberList,'name','Jim');
		querySetCell(memberList,'age','23');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','4');
	queryAddRow(memberList);
		querySetCell(memberList,'id','13');
		querySetCell(memberList,'name','Barbara');
		querySetCell(memberList,'age','37');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','5');
	queryAddRow(memberList);
		querySetCell(memberList,'id','14');
		querySetCell(memberList,'name','Cameron');
		querySetCell(memberList,'age','35');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','15');
		querySetCell(memberList,'name','Dan');
		querySetCell(memberList,'age','39');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','2');
	queryAddRow(memberList);
		querySetCell(memberList,'id','16');
		querySetCell(memberList,'name','Eddie');
		querySetCell(memberList,'age','28');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','3');
	queryAddRow(memberList);
		querySetCell(memberList,'id','17');
		querySetCell(memberList,'name','Frank');
		querySetCell(memberList,'age','40');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','4');
	queryAddRow(memberList);
		querySetCell(memberList,'id','18');
		querySetCell(memberList,'name','Gale');
		querySetCell(memberList,'age','29');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','4');
	queryAddRow(memberList);
		querySetCell(memberList,'id','19');
		querySetCell(memberList,'name','Harry');
		querySetCell(memberList,'age','32');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','20');
		querySetCell(memberList,'name','Issac');
		querySetCell(memberList,'age','38');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','2');
	queryAddRow(memberList);
		querySetCell(memberList,'id','21');
		querySetCell(memberList,'name','Jackie');
		querySetCell(memberList,'age','23');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','3');
	queryAddRow(memberList);
		querySetCell(memberList,'id','22');
		querySetCell(memberList,'name','Karen');
		querySetCell(memberList,'age','42');
		querySetCell(memberList,'gender','female');
		querySetCell(memberList,'department','1');
	queryAddRow(memberList);
		querySetCell(memberList,'id','23');
		querySetCell(memberList,'name','Larry');
		querySetCell(memberList,'age','50');
		querySetCell(memberList,'gender','male');
		querySetCell(memberList,'department','3');
		
		return memberList;
</cfscript>

</cffunction>

<cffunction name="getDepartments" output="false" description="Returns all the contacts in the database" 
					access="public" returntype="query">
	
<cf_querySim>
departments
id,name
1|Customer Service
2|Human Resources
3|IT
4|Administration
5|Accounting
</cf_querySim>

	<cfreturn departments />
</cffunction>


</cfcomponent>

