<!---
File name:			clustermanagement.cfc
Author:
Date started:			/xx/07

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

WAB 2008/11/11		work to deal with cold fusion clustering
			application.serversInCluster can now take a port as well
WAB 2008/11/19		Better error handling, refresh WSDL if function appears not to exist and ignore timeouts
WAB 2009/01/12		Code to read files across cluster using remoteRelayAdmin
WAB 2009/02/18      Above code didn't work when actually used on a live server!  So modified and replaced with generic code which will run any function across the cluster (but wait fro the response)
WAB 2009/03/03		Added code to reduce number of failure warning emails.  Sends first 5 then every 10 then every 100
WAB 2009/06/30 		Added doesLockExistWithinCluster function
WAB 2009/10/27 		runfunctionOnCluster not dealing properly with ports
WAB 2010/06			Major Changes -Moved to new method of dealing with clusters - Register EachInstance in the db, removed emailing about errors
WAB 2011/05/10		Threaded cluster calls to speed everything up on systems with large number of instances (eg Lexmark)
WAB 2011/12/14		Added switch to switch to a non threaded version of cluster code - primarily yo keep Lexmark Up and Running
WAB 2012-01-18		Implementation of CF_INVOKE to speed up calling of cluster functions
WAB 2012-10-18 		Extended getClusterQuery to include statistics
WAB 2012-12-05 		CASE 430659 altered getRelayWareVersionInfo () so that always returns the svncommit key - to be used as the anticaching number in includejavascriptonce
WAB 2013-11-01		Changes related to CF10.
						The directory mapping is done completely differently
						The CFUSION instance does not by defaultl have a built in webserver port, so we have to add one
RJT 2015-12-04		Change to Version.cfm to hold a struct rather than a string as we parse it to create one anyway
WAB 2016-02-24 		Record version changes to the releaseHistory table
WAB 2016-03-09		On Dev Sites use git info to populate version structure.
					Add a year to the version info
WAB 2016-03-17		PROD2016-118  Housekeeping Improvements - the withoutThreading threading function which has not been used for a while had not been updated to reflect new location of instance directory

TDB
There is some code in a previous revision commented as follows
AJC/WAB 2010/04/28 Added http call to check the server exists and is running relayware
Hasn't been added to latest code because new cluster management code might be able to automatically deal with machines leaving the cluster, but might need to be

 --->

 <cfcomponent displayname="Code for updating a cluster" hint="">

	<!--- to switch between threading and non threading in emergencies
		Added WAB 2012/12/14
	 --->
	<cfset this.useThreading = true>
	<cfset variables.xmlFunctions = createObject("component", "com.xmlFunctions")>
	<cfset variables.regExpObj = createObject("component","com.regExp")>


	<cffunction access="public" name="initialise" output="yes">

		<cfif structKeyExists (arguments,"applicationScope")>
			<cfset variables.applicationScope = arguments.applicationScope>
		</cfif>

		<cfset this.instancepath = "instance/clusterManagement">
		<cfset this.heartbeatServerAndProtocol = "https://heartbeat.relayware.com"> <!--- WAB 2012-04-13 remove www from address - certificate doesn't have it --->
		<cfif cgi.http_host contains "dev6-">
				<cfset this.heartbeatServerAndProtocol = "http://dev6-relayAdmin/heartbeat">
		</cfif>

		<cfreturn this>

	</cffunction>


	<!---
	Runs a function on all the other servers/instances in the cluster, but excludes the current instance
	It is a fire and forget function - primarily used for blowing application caches when underlying data changes
	 --->
	<cffunction access="public" name="updateCluster" output="yes">
		<cfargument name="updateMethod" type="string" required="true">

		<cfreturn runFunctionOnCluster (functionname=updatemethod,includeCurrentServer=false,waitforresponse=false,argumentCollection=arguments)>

	</cffunction>


	<cffunction access="public" name="updateInstanceJsessionID" output="yes">
			<cfset var registerInstanceJsessionid = "">
			<cfquery name="registerInstanceJsessionid" datasource = "#application.sitedatasource#">
			update ColdFusionInstance_AutoPopulated set  jsessionid =  <cfqueryparam value="#session.sessionid#" CFSQLTYPE="CF_SQL_VARCHAR" >  where  id =  <cfqueryparam value="#application.instance.ColdFusionInstanceid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

	</cffunction>


	<cffunction name="getInstanceDetails" access="public" returntype="struct">
		<cfset var result = {}>

		<cfset result.IPAddress = CreateObject("java", "java.net.InetAddress").getLocalHost().getHostAddress()> <!--- was serverIPAddress --->
		<cfset result.Port = getInstancePort()>
		<cfset result.UniqueID = right(hash(result.IPAddress & result.Port),10)> <!---was serverInstanceUniqueID  WAB 2009/10/21, wanted a way of uniquely identifying a server and port (for example for creating temporary files which are linked to a specific instance).  Needed to obscure the address so hashed it, and just took the end of it to make it a bit shorter - can't believe will get contention! --->
		<cfset result.name = getInstanceName()>
		<cfset result.servername = CreateObject("java", "java.net.InetAddress").getLocalHost().getHostName()>

		<cfreturn result>

	</cffunction>
	<!--- this was a very nasty method for getting the current proxy port, bit of a mess as well
	basically looks in the configuration XML for the port setting
	wish could find a good one
	also in webservices\remoteRelayAdmin\remoteRelayAdminWS.cfc

	Modified for CF10
	Note that in CF10 not all instances have a Port to the internal web server (in particular the cfusion instance)
	--->
	<cffunction name="getInstancePort" access="remote" returntype="string">

		<cfset var result = "">
		<cfset var xmldoc = '' />
		<cfset var XMLresult = '' />

		<cfset xmldoc = getServerXMLdoc()>

		<cfset XMLresult = getConnectorNodeFromXML(xmlDoc)>

		<cfif arrayLen(XMLresult)>
			<cfset result = XMLresult[1].xmlattributes.port>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getInstanceName" access="remote" returntype="string">

		<cfreturn createobject("component","CFIDE.adminapi.runtime").getinstancename()>

	</cffunction>

	<cffunction name="getIpaddressesAndSubnetsFromIPConfig" access=private returnType="array">

		<cfset var result = arrayNew(1)>
		<cfset var regExpResultArray = "">
		<cfset var IPConfigString = "">
		<cfset var tempStruct = "">
		<cfset var i = "">

		<cfexecute
			name = "C:\WINDOWS\system32\ipconfig.exe"
			variable ="IPConfigString"
			timeout = "5"
		/>

		<cfset regExpResultArray = regExpObj.refindAllOccurrences ("Address.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}).*?Mask.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})",IPConfigString)>

			<cfloop array="#regExpResultArray#" index="i">
				<cfset tempStruct = {address=i[2],mask=i[3]}>
				<cfset arrayAppend (result,tempStruct)>
			</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction name="getIpaddressArray" access=private returnType="array">
		<cfif not structKeyExists (server,"ipAddressArray")>
			<cfset server.ipAddressArray = getIpaddressesAndSubnetsFromIPConfig()>
		</cfif>

		<cfreturn server.ipAddressArray>

	</cffunction>

	<cffunction name="ConvertIPAddressArrayToList" access=private returnType="string">
		<cfargument name="ipAddressArray" type="array" required="true">
		<cfset var result = "">
		<cfset var item = "">

		<cfloop array="#ipAddressArray#" index="item">
			<cfset result = listappend(result,item.address)>
		</cfloop>

		<cfreturn result>

	</cffunction>



<!---
	WAB 2009/01/12 and 2009/02/18
	Runs a function on all servers/instances.
	Pass in the name of the function and any function specific parameters
	Optionally runs the function on the current instance
	NB - This function waits for the answer
	Originally written for Nat to read the scheduled task XML files on all servers

	Wab 2009/10/09 added varing while debuggin an intermittent error
	WAB 2010/07 rewritten using some of the new clustering stuff.
		All cluster calls go through a single calLWebService function which deals with application name
		Also changed so that a single function can do the job of runFunctionOnCluster and updateCluster
		(There is a switch to specify whether to wait for a response or do fire and forget)
	 --->

	<cffunction access="public" name="runFunctionOnCluster" output="yes">
		<cfif this.useThreading>
			<cfreturn runFunctionOnClusterWithThreading(argumentCollection = arguments)>
		<cfelse>
		<cfreturn runFunctionOnClusterWithoutThreading(argumentCollection = arguments)>
		</cfif>

	</cffunction>

	<cffunction access="public" name="runFunctionOnClusterWithThreading" output="yes">
		<cfargument name="functionName" type="string" required="true">
		<cfargument name="includeCurrentServer" default = "false">
		<cfargument name="waitForResponse" default = "true">
		<cfargument name="timeOut" default = "10">
		<cfargument name="instanceID" default = ""> <!--- to run on a specific instance --->


			<!---  <cfset var thread = '' /> This variable deliberately not var'ed, this line fools varScoper --->
			<cfset var result = structNew()>
			<cfset var args = structCopy(arguments)>
			<cfset var thisresult = "">
 			<cfset var theURL = "">
 			<cfset var instanceQuery = "">
			<cfset var argswddx = '' />
			<cfset var feed  = "">
			<cfset var feedList  = "">
			<cfset var threadname = "">
			<cfset var timeoutMilliseconds  = 0>
			<cfset var threadNameToUniqueIDStruct = {}>

			<cfif not waitForResponse>
				<cfset arguments.timeout = 1> <!--- timeout of zero does not work --->
			</cfif>

			<cfset timeoutMilliseconds  = arguments.timeOut * 1000>

			<cfset structDelete (args,"functionName")>
			<cfset structDelete (args,"updateMethod")>  <!--- this one comes from the updateCluster function --->
			<cfset structDelete (args,"includeCurrentServer")>
			<cfset structDelete (args,"timeout")>
			<cfset structDelete (args,"waitForResponse")>
			<cfset structDelete (args,"applicationScope")>
			<cfset structDelete (args,"instanceid")>

			<cfif instanceID is "">
				<cfset instanceQuery = getClusterQuery (includeCurrentServer = includeCurrentServer)>
			<cfelse>
				<cfset instanceQuery = getClusterQuery (instanceid = instanceid)> 					<!--- don't want to do a cluster update if this machine IP is the same as the IP of the machine we are are being told to update--->
			</cfif>

			<cfif instanceQuery.recordcount gt 0>
	 			<cfloop query = "instanceQuery">
					<cfset feed = application.com.structureFunctions.queryRowToStruct(query=instanceQuery,row=currentrow)>
							<cfset feed.theurl = "http://#instanceIPAddress#:#instancePort#/#this.instancepath#/callClusterWebService.cfc?wsdl">
							<cfset feed.timeout = timeout>
							<cfset feed.args = args>
							<cfset feed.functionName = functionName>
							<cfset feed.instanceUniqueID= instanceUniqueID>
							<cfset feed.threadName = createUUID() >
							<cfset threadNameToUniqueIDStruct[feed.threadName] = feed.instanceUniqueID>
							<cfset feedlist = listappend(feedlist,feed.threadName)>
							<cfset feed.waitForResponse = waitForResponse>

		  				<cfthread action="run" name="#feed.threadName#" feed=#feed# >

							<cfset thisResult = structNew()>
							<cftry>
								<cf_invoke
									webservice="#feed.theURL#"
									timeout="#feed.timeout#"
									method = "callWebService"
									webServiceName = "clusterFunctions"
									methodName = "#feed.functionName#"
									appname = "#feed.cfappname#"
									returnVariable = "thisresult"
									additionalArguments = #feed.args#
									refreshWSDL = false
								/>

								<cfcatch type="any">
									<cfif not feed.waitForResponse and cfcatch.detail contains "Read timed out">
										<!--- don't worry about a time-out, assume all will be OK --->
										<cfset thisResult = {isOK=true,message="Function Called"}>
									<cfelse>
										<cfset thisResult = {isOK=false,message=cfcatch.detail}>
									</cfif>
								</cfcatch>

							</cftry>
							<cfset thisResult.URL = cf_invoke.urlCalled>
							<cfset thisResult.instanceUniqueID = feed.instanceUniqueID>
							<cfset thread.result = thisResult>
						</cfthread>


				</cfloop>


	  			<cfthread action="join" name="#feedList#" timeout="#timeoutMilliseconds#" />

	 			<cfloop list= "#feedList#" index="threadName">

					<cfif cfthread[threadName].status is not "COMPLETED">
							<cfthread action="terminate" name="#threadName#"/>
							<cfif cfthread[threadName].status is "NOT_STARTED">
								<cfset thisResult = {isOK=false,message="Problem: NOT_STARTED",instanceUniqueID = threadNameToUniqueIDStruct[threadName]}>
							<cfelseif not waitForResponse >
								<cfset thisResult = {isOK=true,message="Function Called",instanceUniqueID = threadNameToUniqueIDStruct[threadName]}>
							<cfelse>
								<cfset thisResult = {isOK=false,message="Thread Timed Out",instanceUniqueID = threadNameToUniqueIDStruct[threadName]}>

							</cfif>
					<cfelse>
						<cfset thisResult = cfthread[threadName].result>
					</cfif>
							<cfset result[thisresult.instanceUniqueID] = thisresult>
				</cfloop>
			</cfif>


		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="runFunctionOnClusterWithoutThreading" output="yes">
		<cfargument name="functionName" type="string" required="true">
		<cfargument name="includeCurrentServer" default = "false">
		<cfargument name="waitForResponse" default = "true">
		<cfargument name="timeOut" default = "10">
		<cfargument name="instanceID" default = ""> <!--- to run on a specific instance --->


			<!---  <cfset var thread = '' /> This variable deliberately not var'ed, this line fools varScoper --->
			<cfset var result = {}>
			<cfset var args = structCopy(arguments)>
			<cfset var thisresult = "">
 			<cfset var theURL = "">
 			<cfset var instanceQuery = "">
			<cfset var theurlAndParameters = '' />
			<cfset var feed  = "">
			<cfset var feedList  = "">
			<cfset var threadname = "">
			<cfset var timeoutMilliseconds = 0>
			<cfset var webservice = "">
			<cfset var webserviceargs = {}>
			<cfset var threadNameToUniqueIDStruct = {}>
			<cfset args.randomID = createUUID()>


			<cfif not waitForResponse>
				<cfset arguments.timeout = 1> <!--- timeout of zero does not work --->
			</cfif>

			<cfset timeoutMilliseconds  = timeOut * 1000>

			<cfset structDelete (args,"functionName")>
			<cfset structDelete (args,"updateMethod")>  <!--- this one comes from the updateCluster function --->
			<cfset structDelete (args,"includeCurrentServer")>
			<cfset structDelete (args,"timeout")>
			<cfset structDelete (args,"waitForResponse")>
			<cfset structDelete (args,"applicationScope")>
			<cfset structDelete (args,"instanceid")>

			<cfif instanceID is "">
				<cfset instanceQuery = getClusterQuery (includeCurrentServer = includeCurrentServer)>
			<cfelse>
				<cfset instanceQuery = getClusterQuery (instanceid = instanceid)> 					<!--- don't want to do a cluster update if this machine IP is the same as the IP of the machine we are are being told to update--->
			</cfif>

			<cfif instanceQuery.recordcount gt 0>
	 			<cfloop query = "instanceQuery">
					<cfset feed = application.com.structureFunctions.queryRowToStruct(query=instanceQuery,row=currentrow)>
							<cfset feed.theurl = "http://#instanceIPAddress#:#instancePort#/#this.instancepath#/callClusterWebService.cfc?wsdl">
							<cfset feed.args = args>
							<cfset feed.timeout = timeout>
							<cfset feed.functionName = functionName>
							<cfset feed.instanceUniqueID= instanceUniqueID>
							<cfset feed.threadName = createUUID() >
							<cfset threadNameToUniqueIDStruct[feed.threadName] = feed.instanceUniqueID>
							<cfset feedlist = listappend(feedlist,feed.threadName)>
							<cfset feed.waitForResponse = waitForResponse>

							<cfset thisResult = structNew()>

							<cftry>

								<cf_invoke
									webservice="#feed.theURL#"
									timeout="#feed.timeout#"
									method = "callWebService"
									webServiceName = "clusterFunctions"
									methodName = "#feed.functionName#"
									appname = "#feed.cfappname#"
									returnVariable = "thisresult"
									additionalArguments = #feed.args#
									refreshWSDL = false
								/>


								<cfcatch type="any">
									<cfif not feed.waitForResponse and cfcatch.detail contains "Read timed out">
										<!--- don't worry about a time-out, assume all will be OK --->
										<cfset thisResult = {isOK=true,message="Function Called"}>
									<cfelseif cfcatch.detail is "Error: 404 Not Found." >
										<cfset thisResult = {isOK=false,message=cfcatch.detail}>
									<cfelse>
										<cfset thisResult = {isOK=false,message=cfcatch.detail}>
									</cfif>
								</cfcatch>

							</cftry>
							<cfset thisResult.URL = cf_invoke.urlCalled>
							<cfset thisResult.instanceUniqueID = feed.instanceUniqueID>
							<cfset thread.result = thisResult>
					<!--- 	</cfthread> --->
						<cfset result[thisresult.instanceUniqueID] = thisresult>

				</cfloop>


			</cfif>


		<cfreturn result>

	</cffunction>


		<!---
			Register / Update Registration  of instance in the db
			Needs all these params because sometimes called before the cfapplication is started
			--->
		<cffunction name="registerInstance">
			<cfargument name="cfAppName"  default = "#application.applicationname#">
			<cfargument name="testSite"  default = "#application.TestSite#">
			<cfargument name="datasource"  default = "#application.sitedatasource#">
			<cfargument name="updateApplicationVariable"  default = "true">
			<cfargument name="cf_mappings"  default = "#application.cf_mappings#">


			<cfset var instance	 = getInstanceDetails ()>
			<CFSET var versionInfo = getRelayWareVersionInfo()>
			<cfset var registerInstance = '' />
			<cfset var result = '' />
			<cfset var relayInstanceMetaData = '' />
			<cfset var coldFusionInstanceMetaData = '' />
			<cfset var args = '' />
			<cfset var recordDatabaseID = '' />

			<cfset var structureFunctions = createObject("component","com.StructureFunctions")>
			<cfset var machineMetaData = {ipaddressArray = getIpaddressArray ()}>
			<cfset var ipAddressList = ConvertIPAddressArrayToList(machineMetaData.ipaddressarray)>
			<!---
			create table
				ColdFusionInstance_AutoPopulated
				 (DatabaseIdentifier varchar(100),
						CFAppname varchar(100),
						InstanceIPAddress varchar (20),
						InstancePort varchar(5),
						InstanceName varchar(50)
						ServerName varchar(50)
						testsite int,
						LastUpdated datetime)

			--->

			<cfquery name="registerInstance" datasource = "#datasource#">
			declare @databaseID  varchar (50)
			select @databaseID  = @@serverName + '.' + db_name()

			if exists (select 1 from ColdFusionInstance_AutoPopulated where DatabaseIdentifier = @databaseID and  cfappname =  <cfqueryparam value="#cfappname#" CFSQLTYPE="CF_SQL_VARCHAR" >  and  instanceIPAddress =  <cfqueryparam value="#instance.IPAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instancePort =  <cfqueryparam value="#instance.Port#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				BEGIN
					update ColdFusionInstance_AutoPopulated
					set lastupdated = getdate(), testsite=#arguments.testsite#, active =1,  instanceName =  <cfqueryparam value="#instance.Name#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,  servername =  <cfqueryparam value="#instance.ServerName#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,relaywareVersion =  '#versionInfo.version#',allIPAddresses =  <cfqueryparam value="#ipAddressList#" CFSQLTYPE="CF_SQL_VARCHAR" >
					where DatabaseIdentifier = @databaseID and  cfappname =  <cfqueryparam value="#cfappname#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instanceIPAddress =  <cfqueryparam value="#instance.IPAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instancePort =  <cfqueryparam value="#instance.Port#" CFSQLTYPE="CF_SQL_VARCHAR" >
				END
			ELSE
				BEGIN
					insert into 	ColdFusionInstance_AutoPopulated
						(DatabaseIdentifier,CFAppname,instanceIPAddress,instancePort,instanceName,TestSite,LastUpdated,active,serverName,relaywareVersion ,allIPAddresses )
						values (@databaseID,'#cfappname#','#instance.IPAddress#','#instance.Port#','#instance.name#',#arguments.testsite#,getdate(), 1,'#instance.ServerName#','#versionInfo.version#','#ipAddressList#')

				END

				select
					id, servername, instancename,instanceIPAddress, instancePort,cfappname, @@serverName as databaseserver, db_name() as databaseName,I.testsite,databaseid,'' AS relayInstanceID, (select top 1 value from settings where name = 'theClient.clientName') as clientName
				from ColdFusionInstance_AutoPopulated I
				where DatabaseIdentifier = @databaseID and  cfappname =  <cfqueryparam value="#cfappname#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instanceIPAddress =  <cfqueryparam value="#instance.IPAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instancePort =  <cfqueryparam value="#instance.Port#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset result = structureFunctions.queryRowToStruct(registerInstance,1)>
			<cfset result.relayWareVersionInfo = versionInfo>
			<cfset result.instanceUniqueID = instance.Uniqueid>

			<!---
			Don't bother with this now, get info straight from db each time
			<cfif updateApplicationVariable>
				<!--- now tell all other instances to update their list of instances --->
				<cfset updateserversInClusterVariable (updatecluster = true)>
			</cfif>
			--->

			<cfset relayInstanceMetaData = {versionInfo = versionInfo,cf_mappings = cf_mappings}>
			<cfset coldFusionInstanceMetaData = {remoteAdminLocation="/instance/remoteAdmin"}> <!--- remoteAdminLocation to manage change of code from webservices directory  --->

								<!--- WAB 2010/09/27
						We are now going to tell HQ that this site is up and running
						Need to pass
							MachineName
							InstanceIP
							InstancePort
							InstanceName
							DatabaseServer & dataBaseName
							CFApplicationName
							Internal/External
							HTTPS
					 --->
				<cftry>

					 <cfset args = {servername = registerInstance.serverName,
					 				instanceName = registerInstance.InstanceName,
					 				instanceIPAddress = registerInstance.instanceIPAddress,
					 				InstancePort=registerInstance.Instanceport,
					 				cfappname=registerInstance.cfappname,
					 				ColdFusionProductVersion = server.coldfusion.ProductVersion,
					 				databaseServer= registerInstance.databaseServer,
					 				databaseName = registerInstance.databaseName,
					 				testSite = registerInstance.testSite,
					 				relayPath = '#versionInfo.relayPath#',
					 				relaywareVersion = '#versionInfo.version#',
					 				machineMetaData = machineMetaData,
					 				coldFusionInstanceMetaData = coldFusionInstanceMetaData,
					 				clientName = '#registerInstance.clientName#',
					 				relayInstanceMetaData = relayInstanceMetaData
					 						}>
					<cfinvoke
						webservice="#this.heartbeatServerAndprotocol#/WebServices/registration.cfc?wsdl"
						method = "registerRelayInstance"
						timeout = "5"
						args = 	"#args#"
						returnVariable = "heartBeatResult"
					>

					<!--- we are going to store the unique databaseid back to the ColdFusionInstance_AutoPopulated table (shouldn't every change actually but need to store it somewhere) --->
					<cfquery name="recordDatabaseID" datasource = "#datasource#">
					update
						ColdFusionInstance_AutoPopulated
					set
						databaseid =  <cfqueryparam value="#heartBeatResult.databaseid#" CFSQLTYPE="CF_SQL_INTEGER" >
					where
						id =  <cfqueryparam value="#registerInstance.ID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>



					<cfset result.databaseid =  heartBeatResult.databaseid>
 					<cfset result.RelayInstanceID = heartBeatResult.RelayInstanceID>

					<cfcatch>
						<!--- if the heartbeat call fails then we will return the databaseid already in the database (already in result)--->
					</cfcatch>
				</cftry>

				<!---
					WAB 2016-02-24 record new versions
					If version number has changed then record the change
				--->
				<cfset var versionToRecord = "#result.relayWareVersionInfo.versionData.year#.#result.relayWareVersionInfo.versionData.major#.#result.relayWareVersionInfo.versionData.minor#.#result.relayWareVersionInfo.versionData.maintenance#">
				<cfquery datasource = "#datasource#">
				if not exists (select 1 from releaseHistory where type = 'version' and revision = <cfqueryparam value = "#versionToRecord#" CFSQLType="CF_SQL_VARCHAR">)
				BEGIN
					insert into releaseHistory (type, revision, metaData)
					values ('version' , <cfqueryparam value = "#versionToRecord#" CFSQLType="CF_SQL_VARCHAR">, <cfqueryparam value = "#serializeJSON (result.relayWareVersionInfo.versionData)#" CFSQLType="CF_SQL_VARCHAR">)
				END
				</cfquery>


			<cfreturn result>

		</cffunction>

		<cffunction name="deRegisterInstance">


			<cfset var args = '' />
			<cfset var deRegisterInstance = '' />
			<cfset var instance	 = getInstanceDetails ()>

			<cfquery name="deRegisterInstance" datasource = "#applicationScope.sitedatasource#">
			declare @databaseID  varchar (50)
			select @databaseID  = @@serverName + '.' + db_name()

					update ColdFusionInstance_AutoPopulated
					set lastupdated = getdate(), active =0
					where DatabaseIdentifier = @databaseID
						and  cfappname =  <cfqueryparam value="#applicationScope.applicationName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and  instanceIPAddress =  <cfqueryparam value="#instance.IPAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and  instancePort =  <cfqueryparam value="#instance.Port#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

				<cftry>
					<!--- WAB 2010/09/27
						We are now going to tell HQ that this site is shut down
						Need to pass
							MachineName
							InstanceName
							CFApplicationName
					 --->
					 <cfset args = {servername = applicationScope.Instance.serverName,
					 				instanceName = applicationScope.Instance.Name,
					 				cfappname=applicationScope.applicationname}>
					<cfinvoke
						webservice="#this.heartbeatServerAndProtocol#/WebServices/registration.cfc?wsdl"
						method = "deRegisterRelayInstance"
						timeout = "5"
						args = 	"#args#"
						returnVariable = "x"
					>
					<cfcatch>
					</cfcatch>
				</cftry>

			<cfreturn true>
		</cffunction>

		<cffunction name="registerSiteToInstance">
			<cfargument name="instanceID" >
			<cfargument name="hostname" >
			<cfargument name="datasource"  default = "#application.sitedatasource#">

			<cfset var registerAndGetSiteInstance = "">
			<cfset var heartBeatResult = "">
			<cfset var result = {}>
			<cfset var args ="">

			<cfquery name="registerAndGetSiteInstance" datasource = "#datasource#">
			declare @instanceID int, @hostname varchar(100)
			select   @instanceID =  <cfqueryparam value="#instanceID#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,  @hostname =  <cfqueryparam value="#hostname#" CFSQLTYPE="CF_SQL_VARCHAR" >
			if exists (select 1 from ColdFusionInstance_Host_AutoPopulated where ColdFusionInstance_AutoPopulatedID =  @instanceID and hostname = @hostname)
			BEGIN
				update ColdFusionInstance_Host_AutoPopulated
					set lastupdated = getdate()
				where
					ColdFusionInstance_AutoPopulatedID =  @instanceID
					and hostname = @hostname
			END
			ELSE
			BEGIN
				insert into ColdFusionInstance_Host_AutoPopulated
					(ColdFusionInstance_AutoPopulatedID,
					hostname,
					lastupdated  )
				values (
					 @instanceID,
					@hostname,
					getdate()
				)

			END

				select 	servername, instancename,instanceIPAddress, instancePort,isInternal,sdd.sitedefid, cfappname, @@serverName as databaseserver, db_name() as databaseName,I.testsite
				from ColdFusionInstance_AutoPopulated I
					inner join
					ColdFusionInstance_Host_AutoPopulated H on I.ID= h.ColdFusionInstance_AutoPopulatedID
					inner join
					sitedefdomain sdd on sdd.domain = h.hostname
					inner join sitedef sd on sd.sitedefid = sdd.sitedefid
				where i.id =  @instanceID and h.hostname = @hostname

			</cfquery>

					<!--- WAB 2010/09/27
						We are now going to tell HQ that this site is up and running
						Need to pass
							MachineName
							InstanceIP
							InstancePort
							InstanceName
							DatabaseServer & dataBaseName
							CFApplicationName
							HostName
							Internal/External
							HTTPS
					 --->
				<cftry>

					 <cfset args = {servername = registerAndGetSiteInstance.serverName,
					 				instanceName = registerAndGetSiteInstance.InstanceName,
					 				cfappname=registerAndGetSiteInstance.cfappname,
					 				databaseServer= registerAndGetSiteInstance.databaseServer,
					 				databaseName = registerAndGetSiteInstance.databaseName,
					 				hostname = hostname,
					 				isInternal = registerAndGetSiteInstance.isInternal,
					 				sitedefid = registerAndGetSiteInstance.sitedefid}>
					<cfinvoke
						webservice="#this.heartbeatServerAndprotocol#/WebServices/registration.cfc?wsdl"
						method = "registerRelaySiteToInstance"
						timeout = "5"
						args = 	"#args#"
						returnVariable = "heartBeatResult"
						refreshwsdl = true
					>


					<cfcatch>

					</cfcatch>
				</cftry>

			<cfreturn result>

		</cffunction>

	<cffunction name="removeClusterInstance">
		<cfargument name="id" >

		<cfset var delete = "">
		<cfquery name="delete" datasource = "#application.sitedatasource#">
		delete from ColdFusionInstance_AutoPopulated where  id in ( <cfqueryparam value="#id#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> )
		</cfquery>

	</cffunction>

	<!--- WAB 2012-10-18 Extended to include statistics --->
	<cffunction name="getClusterQuery">
		<cfargument name="includeInactive" default = "false">
		<cfargument name="includeHosts" default = "false">
		<cfargument name="includeStats" default = "false">
		<cfargument name="instanceid" default = "">
		<cfargument name="includeCurrentServer" default = "false">

			<cfset var getCluster	 = "">
			<cfset var instance	 = getInstanceDetails ()>
			<cfset var recentlyUpdatedPeriod	 = 3> <!--- days, needs to be longer than the application timeout really --->

				<cfquery name="getCluster" datasource = "#applicationScope.sitedatasource#">
			select
					instance.id,
					instanceIPAddress,
					instancePort,
					cfappname,
					instanceName,
					serverName,
					active,
					case when instance.lastUpdated > getdate() - #recentlyUpdatedPeriod# then 1 else 0 end as recentlyupdated ,
					case when  instanceIPAddress =  <cf_queryparam value="#instance.IPAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instancePort =  <cf_queryparam value="#instance.Port#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  cfappname =  <cf_queryparam value="#applicationScope.applicationname#" CFSQLTYPE="CF_SQL_VARCHAR" >   then 1 else 0 end as currentInstance,
					instanceIPAddress + case when isnull(instancePort,'') <> '' then ':' + instancePort else '' end + '|' + cfappname as instanceUniqueID,
					<cfif includeHosts>
					hostname,
					(select count(1) from ColdFusionInstance_host_AutoPopulated host where instance.id = host.ColdFusionInstance_AutoPopulatedID) as numberHosts,
					</cfif>
					<cfif includeStats>
					stats.*,
					</cfif>
					instance.lastupdated,
					allIPAddresses

			from
					ColdFusionInstance_AutoPopulated instance
					<cfif includeHosts>
					left join	ColdFusionInstance_host_AutoPopulated host	on instance.id = host.ColdFusionInstance_AutoPopulatedID
					</cfif>
					<cfif includeStats>
					left join
						(
						select id,
							max(visitstarteddate) as lastSessionStarted,
							case when max(lastRequestDate) < max(visitstarteddate) then max(visitstarteddate) else max(lastRequestDate) end as lastRequest,
							sum (case when visitStarteddate between getdate() - (1.0/24.0) and getdate()  then 1 else 0 end) as lastHour,
							sum (case when datepart(dy,visitStarteddate) =  datepart(dy,getdate())   then 1 else 0 end) as Today,
							sum (case when datepart(dy,visitStarteddate) =  datepart(dy,getdate())-1   then 1 else 0 end) as yesterday,
							sum (case when visitStarteddate  > getdate()-7 then 1 else 0 end)  as LastWeek
						from
							coldfusioninstance_autopopulated
							left join visit on coldfusioninstance_autopopulated.id = coldfusioninstanceid
									and visitStartedDate > getdate() -7   <!--- get stats for last 7 days --->
						group by id
						) stats
						on stats.id = instance.id
					</cfif>


			where
				databaseIdentifier = @@serverName + '.' + db_name()
				<cfif instanceid is not "">
					and  instance.id in ( <cfqueryparam value="#instanceid#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> )
				<cfelse>
					<cfif not includeInactive>
					and active = 1
					and instance.lastUpdated > getdate() - #recentlyUpdatedPeriod#
					</cfif>
					<cfif not includeCurrentServer>
					and case when  instanceIPAddress =  <cf_queryparam value="#instance.IPAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  instancePort =  <cf_queryparam value="#instance.Port#" CFSQLTYPE="CF_SQL_VARCHAR" >   and  cfappname =  <cf_queryparam value="#applicationScope.applicationname#" CFSQLTYPE="CF_SQL_VARCHAR" >   then 1 else 0 end = 0
					</cfif>
				</cfif>

			order by instance.id
			</cfquery>

		<cfreturn getCluster>

	</cffunction>


	<cffunction name="getClusterAndHostQuery">
		<cfargument name="includeInactive" default = "false">
		<cfargument name="includeCurrentServer" default = "false">
		<cfargument name="includeStats" default = "false">

		<cfreturn getClusterQuery (includeInactive  = includeInactive, includeHosts = true, includeCurrentServer = includeCurrentServer,includeStats = includeStats)>
	</cffunction>

	<cffunction name="isThisSiteClustered">
		<cfreturn getClusterQuery().recordcount>
	</cffunction>

	<cffunction name="isThisIPAddressInCluster">
		<cfargument name="ipaddress" default = "true">

		<cfset var checkIP ="">
		<cfset var clusterQuery = getClusterQuery(includeCurrentServer = true)>

		<cfquery name="checkIP" dbtype="query">
		select 1 from clusterQuery where
			InstanceipAddress =  <cfqueryparam value="#ipaddress#" CFSQLTYPE="CF_SQL_VARCHAR" >
		or allIPAddresses like '%#ipaddress#%'
		</cfquery>

		<cfreturn checkIP.recordcount>

	</cffunction>


	<cffunction name="isServerActive" >
		<cfargument name="serverName" default = "true">

		<cfset var getServer ="">
		<cfquery name="getServer" datasource = "#applicationScope.sitedatasource#">
		select 1 from ColdFusionInstance_AutoPopulated
		where servername in (#listqualify(servername,"'")#)
		and active = 1
		and lastupdated > getdate() - .2  -- 4 hrs!
		</cfquery>

		<cfreturn getserver.recordcount>

	</cffunction>

	<cffunction name="getPathToInstanceDirectory" >

		<cfset var instancePath  = application.paths.relay & "\instance">
		<cfset instancePath  = replace(instancePath,"/","\","ALL")>
		<cfset instancePath  = getCorrectCaptalisationForDirectory (instancePath).directory>

		<cfreturn instancePath >

	</cffunction>


	<cffunction name="hasInstanceMappingBeenSetUpCorrectly" hint="Checks if the instance mapping has been set up">
		<cfset var result = {isOK = true, message = ""}>
		<cfset var xmlDocResult = '' />
		<cfset var connectorSearchResult = '' />
		<cfset var xmlSearchResult = '' />
		<cfset var aliasValue = '' />
		<cfset var contextNode = '' />

		<cfset var xmlDoc = getServerXMLDoc()>

			<!--- check for a connector node, if there isn't one then isOK = false --->
			<cfset connectorSearchResult = getConnectorNodeFromXML(xmlDoc)>

			<cfif not arrayLen(connectorSearchResult)>
				<cfset result.isOK = false>
				<cfset result.Message = result.Message & "<BR>Connector Needs Adding">
			</cfif>

			<!--- Search for context node and Add/Update it --->
			<cfset xmlSearchResult = getContextNodeFromXML(xmldoc)>

			<cfset aliasValue =  "/instance=#getPathToInstanceDirectory()#">

			<cfif not arrayLen(xmlSearchResult)>
				<cfset result.isOK = false>
				<cfset result.Message = result.Message & "<BR>Mapping Needs Adding">
			<cfelse>
				<cfset contextNode = xmlSearchResult[1]>
				<cfif not structKeyExists(contextNode.XMLAttributes,"aliases") or contextNode.XMLAttributes.aliases is not aliasValue>
					<cfset result.isOK = false>
					<cfset result.Message = result.Message & "<BR>Mapping Needs Updating (currently #aliasValue#)">
				</cfif>
			</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="getPortsForAllServersOnMachine" hint="">
		<cfset var result = structNew()>
		<cfset var ColdFusionDir = reverse(listrest(reverse(server.coldFusion.rootDir),"\"))>

		<!-- Read the instances.xml file to get details of all the instance on this machine -->
		<cfset var instancesDocXMLResult = variables.xmlFunctions.readAndParseXMLFile (ColdFusionDir & "\config\instances.xml")>

		<!-- Search for all the server Nodes - gives us the path to the instance -->
		<cfset serversResult = XMLSearch (instancesDocXMLResult.xml,"/servers/server")>

		<!-- For Each Instance, read the serverXML and get the port numbers -->
		<cfloop array="#serversResult#" index="thisServer">

			<cfset serverXML = getServerXML(rootDir = trim(thisServer.directory.xmltext)).xml>
			<cfset connectorNodeResult = getConnectorNodeFromXML(serverXML)>
			<cfif arrayLen(connectorNodeResult)>
				<cfset result [thisServer.name] = {port = connectorNodeResult[1].XMLAttributes.port, redirectPort = connectorNodeResult[1].XMLAttributes.redirectPort}>
			<cfelse>
				<cfset result [thisServer.name] = {port = "", redirectPort = ""}>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="getNextFreePort" hint="">
		<cfset allPorts = getPortsForAllServersOnMachine()>

		<cfset MaxPort = 8500 -1 ><!--- Bit of a hack, but CF10 generally uses ports starting at 8500 --->
		<cfscript>
			structEach (allPorts, function (key, value) {if (value.port is not "") {maxPort = max(value.port,maxPort);}} );
		</cfscript>

		<cfreturn maxPort + 1>
	</cffunction>


	<cffunction name="getConnectorNodeFromXML">
		<cfargument name="xmlDoc" >

		<cfreturn XmlSearch(xmldoc, "/Server/Service/Connector[contains(@protocol,'http')]")>
	</cffunction>


	<cffunction name="getContextNodeFromXML">
		<cfargument name="xmlDoc" >

		<cfreturn xmlsearch (xmldoc,"/Server/Service/Engine/Host/Context")>

	</cffunction>


	<cffunction name="getServerXML" hint="">
		<cfargument name="rootDir" default = "#server.coldfusion.rootDir#">

		<cfset var result = variables.xmlFunctions.readAndParseXMLFile (getServerXMLPath(rootDir))>

		<cfreturn result>
	</cffunction>


	<cffunction name="getServerXMLDoc" hint="">
		<cfargument name="rootDir" default = "#server.coldfusion.rootDir#">

		<cfreturn getServerXML(rootDir).xml>

	</cffunction>


	<cffunction name="writeServerXML" hint="">
		<cfargument name="xmlDoc" >

		<cfset xmlstring = tostring(xmldoc)>
		<!--- A hack to put elements on separate lines, but doesn't do indenting'
		<cfset xmlString = rereplace(xmlstring,">([\s]*)<",">#chr(13)##chr(10)#\1<","ALL")>
		--->

		<cffile action="write" file="#getServerXMLPath()#" output="#xmlstring#">

		<cfreturn true>
	</cffunction>


	<cffunction name="getServerXMLPath" hint="">
		<cfargument name="rootDir" default = "#server.coldfusion.rootDir#">

		<cfset var result = "#rootDir#/runtime/conf/server.xml">
		<cfreturn result>
	</cffunction>


	<cffunction name="AddHttpConnectorToServerXMLDoc" hint="Adds HTTP Connector if one not there">
		<cfargument name="XMLDoc" default = "false">  <!--- used for testing without doing the final update --->

		<cfset var xmlSearchResult = '' />
		<cfset var newNode = '' />

		<cfset xmlSearchResult = getConnectorNodeFromXML(xmlDoc)>

		<cfif not arrayLen(xmlSearchResult)>
			<cfset newNode = XmlElemNew(xmldoc,"Connector")>
			<!-- Note quotes round keys keep case -->
			<cfset nodeAttributes = {"protocol"="org.apache.coyote.http11.Http11Protocol", "connectionTimeout"="20000", "executor"="tomcatThreadPool"}>
			<cfset nodeAttributes["redirectPort"]="8443" >
			<cfset nodeAttributes["port"]=getNextFreePort() >
			<cfset newNode.XMLAttributes = nodeAttributes>
			<cfset ArrayAppend(xmldoc.Server.Service.XmlChildren, newNode)>
		</cfif>

		<cfreturn XMLDoc>

	</cffunction>

	<cffunction name="AddInstanceMapping" hint="Adds Mapping - replaces existing one if there">
		<cfargument name="testMode" default = "false">  <!--- used for testing without doing the final update --->

		<cfset var result = {isOK=true,message=""}>
		<cfset var xmlDocResult = '' />
		<cfset var xmldoc = '' />
		<cfset var xmlSearchResult = '' />
		<cfset var instancePath = '' />
		<cfset var checkDirectory = '' />
		<cfset var xmlstring = '' />
		<cfset var newNode = '' />
		<cfset var updateRequired = false />

			<cfset xmldoc = getServerXMLDoc()>

			<!--- check for a connector node, if there isn''t one then add it --->
			<cfset connectorSearchResult = getConnectorNodeFromXML(xmlDoc)>

			<cfif not arrayLen(connectorSearchResult)>
				<cfset AddHttpConnectorToServerXMLDoc(xmlDoc)>
				<cfset updateRequired = true>
			</cfif>

			<!--- Search for context node and Add/Update it --->
			<cfset xmlSearchResult = getContextNodeFromXML (xmldoc)>

			<cfset aliasValue =  "/instance=#getPathToInstanceDirectory()#">

			<cfif not arrayLen(xmlSearchResult)>
				<cfset contextNode = XmlElemNew(xmldoc,"Context")>
				<!-- Note quotes round keys - keeps case -->
				<cfset nodeAttributes = {"path"="/","docBase"="#server.coldFusion.rootdir#\wwwroot", "WorkDir"="#server.coldFusion.rootdir#\runtime\conf\Catalina\localhost\tmp", "aliases" = aliasValue}>
				<cfset contextNode.XMLAttributes = nodeAttributes>
				<cfset ArrayAppend(xmldoc.Server.Service.Engine.Host.XmlChildren, contextNode)>
				<cfset updateRequired = true>
			<cfelse>
				<cfset contextNode = xmlSearchResult[1]>
				<cfif not structKeyExists(contextNode.XMLAttributes,"aliases") or contextNode.XMLAttributes.aliases is not aliasValue>
					<cfset updateRequired = true>
					<cfset contextNode.XMLAttributes["aliases"]= aliasValue>
				</cfif>
			</cfif>


				<!---
					Can't set up a mapping with UNC Paths
					If application.paths.relay points to UNC path then we will have to doa bit of a hack
				--->

				<cfif left (instancePath,2) is "\\">
					<!--- We are going to have to have the code put locally, maybe it already is
						assume mapping of form \\xxxx\web\.....
						am going to see if d:\web exists, if so then use it
					--->
					<cfset result.message = "Can't do mapping to UNC path.<BR>">
					<cfset instancePath = "D:\" & listrest (instancePath,"\")>
  					<cfset checkDirectory = getCorrectCaptalisationForDirectory (instancePath)>
					<cfif not checkDirectory.isOK>
						<cfset result.isOK = false>
						<cfset result.message = result.message  & "You could copy code instancemanagement directory to #instancePath#">
						<cfreturn result>
					</cfif>
				<cfelse>
					<!--- Check capitalisation of directory name --->
  					<cfset checkDirectory = getCorrectCaptalisationForDirectory (instancePath)>
				</cfif>

				<cfif not updateRequired >
					<cfset result.message = result.message  & "Mapping Exists" >
				<cfelseif not testMode>
					<cfset writeServerXML(xmlDoc)>
					<cfset result.message = result.message  & "Mapping Added: " & checkDirectory.directory & ". You will need to restart your server">
				<cfelse>
					<cfset result.message = result.message  & "Mapping Would Be Added...: " & checkDirectory.directory>
				</cfif>

		<cfreturn result>

	</cffunction>

	<!--- Java needs the directory name to be in correct case, surely there is a way of doing this, but I had to use cfdirectory recursively to get the actual capitalisation.
	Drive letter has to be correctly capitalised to start with
	If directory does not exist then return false
	--->
	<cffunction name="getCorrectCaptalisationForDirectory">
		<cfargument name="directory" >
		<cfset var result = {isOK=true}>
		<cfset var slash = "\">
		<cfset var restOfPath = '' />
		<cfset var nextDirectory = '' />
		<cfset var dirList = '' />

		<cfset arguments.directory = replace(arguments.directory,"/",slash,"ALL")>

		<!--- Loop gradually along the path using cfdirectory to search for each successive directory --->
		<!--- are we using a
			UNC path - need to start with first two bits
			or
			a directory - need to start with first bit

		--->
		<cfif left (directory,2) is "\\">
			<cfset result.directory = "\\" & listFirst(directory,slash)>
			<cfset restOfPath = listRest(directory,slash)>
			<cfset result.directory = result.directory & slash & listFirst(restOfPath,slash)>
			<cfset restOfPath = listRest(restOfPath,slash)>
		<cfelse>
			<cfset result.directory = listFirst(directory,slash)>
			<cfset restOfPath = listRest(directory,slash)>
		</cfif>




		<cfloop index="nextDirectory" list="#restOfPath#" delimiters = "#slash#">
			<cfdirectory action="LIST" directory="#result.directory#" name="dirList" filter="#nextDirectory#" type="dir">
			<cfif dirList.recordCount>
				<cfset result.directory = result.directory & slash & dirList.name>
			<cfelse>
				<cfset result.isOK = false>
				<cfbreak>
			</cfif>

		</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- 2012-12-05 WAB CASE 430659 altered so that always returns the svncommit key - to be used as the anticaching number in includejavascriptonce --->
	<cffunction name="getRelayWareVersionInfo">
		<cfset var result = {version = 0, svnCommit = 0}>
		<cfset var gitBranch = '' />
		<cfset var gitRevision = '' />
		<cfset var relaywareVersionNumber = "">

		<cfinclude template = "\version.cfm"> <!---Where relaywareVersionNumber comes from  --->

		<cfset result.versionData = relaywareVersionNumber>

		<cfif result.versionData.major EQ ""> <!---i.e not set --->
			<!--- 	This is probably a dev site so see if we can find a git branch name
					We imagine that the branch might be named in the format Year_Major_Minor_Description
					If it is then we are able to pull out the year/major/minor and we will throw the rest of the name on to the .branch
					WAB 2016-04-07 BF-564
			--->
			<cfset var  gitHeadFile = "\..\.git\head">
			<cfset gitHeadFile = expandPath(gitHeadFile)>
			<cfif fileExists (gitHeadFile)>
				<cffile action="read"  file="#gitHeadFile#" variable="gitBranch">
				<cfset result.versionData.branch = listLast(gitBranch,"/")>
				<!--- This reg exp will always bring back something since all the items are optional! --->
				<cfset var YearMajorMinorRegExp = "\A(\d+)?_?(\d+)?_?(\d+)?_?(\d+)?_?(.*)?\Z">
				<cfset var groupNames = {1 = "year", 2="Major", 3="Minor", 4="maintenance", 5="Branch"}>
				<cfset var match = regExpObj.refindAllOccurrences (YearMajorMinorRegExp, result.versionData.branch, groupnames)>
				<cfset result.versionData = match[1]>

				<!--- set a random revision for time being - used as a cache buster --->
				<cfset result.versionData.revision = int (rand () * 10000) >

			</cfif>

		</cfif>

		<cfset result.svnCommit = right(relaywareVersionNumber.revision,6)>

		<cfset result.version="#relaywareVersionNumber.year#.#relaywareVersionNumber.major#.#relaywareVersionNumber.minor#.#relaywareVersionNumber.maintenance#.#relaywareVersionNumber.build#.#relaywareVersionNumber.revision#">

		<!--- get the path as well --->
		<cfset result.relayPath = reverse(listrest(listrest(reverse(getcurrenttemplatepath()),"\"),"\"))>

		<cfreturn result>
	</cffunction>


<!---

	<cfif isDefined("application.sitedatasource")>
		<cfset updateserversInClusterVariable(false)>
	</cfif>
 --->

 </cfcomponent>


