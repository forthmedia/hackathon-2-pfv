<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayEvents.cfc	
Author:			WAB  
Date started:	??
	
Description:	Events functions		

Amendment History:

Date 		Initials 	What was changed
2015-01-30	AHL		Case 443215 Complete status for Events
2016-01-12	WAB		Visibility Project (and BF-226).  Change recordRights queries to refer to the actual entity (which is eventDetail rather than event)
2015-11-25  ACPK    PROD2015-289 Fix for eFolder: If date exists as URL parameter, return Events for this date only

Possible enhancements:
 --->

<cfcomponent displayname="relayEvents" hint="Provides functions for Relay Event Manager">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                   
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getEventList" access="public" returntype="query" hint="What does this function do">
		
		<cfargument name="EventGroupID" type="numeric" default="0">
		<cfargument name="filterByCountryIDList" type="string">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="filterByUserGroupids" type="string" default="">
		<cfargument name="filterByVisibility" type="boolean" default="0">
		<cfargument name="limitToTeamEvents" type="boolean" default="false">
		
		
		<cfscript>
			var getEventList="";
			
			getEventList=getEventListUnknownUser(EventGroupID="#EventGroupID#",filterByCountryIDList="#filterByCountryIDList#",datasource="#datasource#",filterByUserGroupids="#filterByUserGroupids#",filterByVisibility="#filterByVisibility#",limitToTeamEvents=limitToTeamEvents);
		</cfscript>

		<cfreturn getEventList>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getEventListUnknownUser" hint="Returns all events when the user is unknown">
		<!--- Depreciated, use getEventList instead --->
		<cfargument name="EventGroupID" type="numeric" default="0">
		<cfargument name="filterByCountryIDList" type="string">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="filterByUserGroupids" type="string" default="">
		<cfargument name="filterByVisibility" type="boolean" default="0">
	
		<cfscript>
			var getEventListUnknownUser = "";
		</cfscript>

		<cfquery name="getEventListUnknownUser" datasource="#arguments.dataSource#">
				Select 	e.flagid,
					e.countryid, 
					'phr_register' as regStatus,
					COUNT(distinct efd.entityid) as NumberRegistered,
					eg.eventName,
					e.title,
					e.eventstatus, 
					isnull(e.AllocatedStartDate, e.PreferredStartDate) as startDate, 
					e.allocatedenddate, 
					e.location, 
					e.venueRoom, 
					e.SortOrder,
					f.flagtextid,
					e.EstAttendees as EstAttendees, 
					ef.Sex, 
					ef.fieldValue, 
					ef.MinDateofBirth, 
					ef.MaxDateofBirth
				from eventDetail as e
				inner join eventGroup eg on e.eventGroupID = eg.eventGroupID
				inner join flag f on f.flagid = e.flagid
				inner join eventCountry ec ON f.flagID = ec.flagID 
				left outer join eventFilters ef on e.FlagID = ef.FlagID
				left outer join eventflagdata efd on efd.flagid = e.flagid and efd.regstatus <> 'cancelled'
				where 1=1
				<CFIF IsDefined("arguments.EventGroupID") AND arguments.EventGroupID GT 0>
				 AND e.eventGroupID = #arguments.EventGroupID#
				</CFIF>
				<cfif isDefined("arguments.filterByCountryIDList") and len(arguments.filterByCountryIDList) GT 0>
				and ec.countryID  in ( <cf_queryparam value="#arguments.filterByCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
				<cfif isDefined("arguments.limitToTeamEvents") and arguments.limitToTeamEvents>
				and (e.maxTeamSize is not null or e.maxTeamSize > 1)
				</cfif>
				and e.flagid in (select ed.flagID 
					from eventDetail ed 
					INNER JOIN eventflagData efd ON ed.flagID = efd.flagID
					where <!--- efd.entityID = #request.relayCurrentUser.personid# 
					and ---> ed.invitationType = 'invOnly' 
					UNION
					select ed.flagID 
					from eventDetail ed
					where ed.invitationType = 'open')
				
				<cfif filterByUserGroupids is not "">
				and e.flagid in 
				(select recordid as flagids from recordrights
				where usergroupid  in ( <cf_queryparam value="#filterByUserGroupids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and entity='EventDetail'
				UNION
				select flagid as flagids from eventdetail 
				where flagid not in (select recordid from recordrights where entity='EventDetail'
				)
				)
				</cfif>
				
				<!--- 
					and (e.eventstatus = 'Agreed') 
					WAB - removed this line and replaced with below
					This allows full events to show if the person is already entered  (confirmed or not)
					GCC - 2005/02/15 - Changed to hide initial events and correct a logic error
					--->
				 and ((e.eventstatus = 'Agreed') <!--- or (e.flagid in (select distinct flagid from eventflagdata where regstatus in ('RegistrationStarted','RegistrationSubmitted','confirmed') and eventstatus <> 'initial' and entityid = #request.relayCurrentUser.personid#)))
			 --->
			    <!--- 2015-11-25     ACPK   PROD2015-289 Fix for eFolder: If date exists as URL parameter (i.e. after clicking date in Event Widget), return Events for this date only  --->
			    <CFIF IsDefined("url.Event")>
				    and isnull(e.AllocatedStartDate, e.PreferredStartDate) = <cf_queryparam value="#url.Event#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			    <CFELSE>
				and isnull(e.AllocatedStartDate, e.PreferredStartDate) > getDate())
				</CFIF>
				<CFIF IsDefined("sex")>
					and (ef.sex is null or ef.sex =  <cf_queryparam value="#sex#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				</CFIF>
				<CFIF IsDefined("fieldValue")>
					and (ef.fieldValue is null or (ef.fieldValue =  <cf_queryparam value="#fieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
				</CFIF>
				<CFIF IsDefined("DateOfBirth")>
					and convert(datetime,'#DateOfBirth#',111) Between isnull(ef.maxDateOfBirth,'1766/10/14') and isnull(ef.minDateOfBirth,'2149/07/20')
				</CFIF>
				group by e.CountryID, e.FlagID, eg.EventName, e.title, e.EventStatus, e.AllocatedStartDate, e.PreferredStartDate, e.AllocatedEndDate, e.Location, 
			                      e.VenueRoom, f.FlagTextID, e.EstAttendees, ef.Sex, ef.MinDateofBirth, ef.MaxDateofBirth, ef.fieldValue, e.SortOrder
				order by  eg.eventName, e.SortOrder, isnull(e.AllocatedStartDate, e.PreferredStartDate)

		</cfquery>
		<cfreturn getEventListUnknownUser>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description                            
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getEventDetails" access="public" returntype="query" hint="Returns the events for the supplied eventFlagTextID">
		<cfargument name="eventFlagTextID" type="string" required="yes">
		
		<cfscript>
			var getEventDetailsQry = "";
		</cfscript>
		
		<cfquery name="getEventDetailsQry" datasource="#application.siteDataSource#">
			select * from eventdetail 
			where flagid  = 
			<cfif isNumeric(arguments.eventFlagTextID)>
				<cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
			(select flagid from flag 
				where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			</cfif>
		</cfquery>
		
		<cfreturn getEventDetailsQry>
	</cffunction>	
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      SWJ: 2005-08-26 Modified the query to only allow                                                                     
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateEventRegStatus" hint="Updates the regStatus to regStatus for personID and eventFlagTextID">
		
		<cfargument name="eventFlagTextID" type="string" required="yes">
		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="regStatus" type="string" default="RegistrationSubmitted">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
				
		<cfscript>
			var updateEventRegStatus = "";
			var getRegDetails = "";
			var getEventDetails = "";
			var mergeStruct = structNew();
			var getPersonName = "";
		</cfscript>
		
		<cfquery name="getEventDetails" datasource="#application.siteDataSource#">
			select title,flagID,share from eventDetail where flagID = (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>
		
		<cfquery name="updateEventRegStatus" datasource="#arguments.datasource#">		
		IF exists (select * from EventFlagData WHERE flagid = (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			AND entityid = #arguments.personid#)
		   BEGIN 
			Update EventFlagData 
			Set RegStatus =  <cf_queryparam value="#arguments.RegStatus#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			WHERE flagid = (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			AND entityid = #arguments.personid#
		   END
		ELSE
		   BEGIN 
		   	IF exists (select * from EventDetail WHERE flagid = (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					AND invitationType = 'Open')
				BEGIN
					INSERT INTO EventFlagData
					(EntityID, FlagID, FirstName, LastName, SiteName, CountryID, 
						Country, Email, regDate, LastUpdated, RegStatus, Wave)
						SELECT personid, (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ), 
							p.FirstName, p.LastName, l.SiteName, c.CountryID, 
							c.CountryDescription, p.email, getDate(), getDate(), '#arguments.RegStatus#','A'
					FROM Person AS p, Location AS l, Country AS c
						WHERE p.PersonID = #arguments.personid#
						AND p.LocationID = l.LocationID
						AND l.CountryID = c.CountryID
						AND NOT EXISTS (SELECT * FROM EventFlagData WHERE EntityID = #arguments.personid# AND 
						flagid = (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
				END
		   END
		</CFQUERY>
		
		<!--- NJH 2012/10/23 Social CR - check if event is shareable --->
		<cfif arguments.regstatus eq "RegistrationSubmitted" and application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.eventDetail.registered") and getEventDetails.share>
			<cfset mergeStruct.event = getEventDetails.title>
			
			<cfif arguments.personID neq request.relayCurrentUser.personID>
				<cfquery name="getPersonName" datasource="#application.siteDataSource#">
					select firstname,lastname from person where personID=#arguments.personID#
				</cfquery>
				
				<cfset mergeStruct.person = {firstname=getPersonName.firstname,lastname=getPersonName.lastname}>
			</cfif>
			<cfset application.com.service.addRelayActivity(action="registered",performedOnEntityID=getEventDetails.flagId,performedOnEntityTypeID=application.entityTypeID["eventDetail"],performedByEntityID=arguments.personID,mergeStruct=mergeStruct,sendShare=true)>
		</cfif>
		
		<cfquery name="getRegDetails" datasource="#arguments.dataSource#">
			SELECT efd.firstName + ' ' + efd.lastname as fullname, 
				e.title as eventName, RegStatus
			FROM EventFlagData efd
				INNER JOIN eventDetail e ON efd.flagid = e.flagid
				WHERE EntityID = #arguments.personid# AND 
				efd.flagid = (select flagid from flag where flagtextid =  <cf_queryparam value="#arguments.eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>
		
		<cfreturn getRegDetails>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    SWJ: 2005-08-26 Added                                                 
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="validateEventFlagTextID" hint="Checks if the eventFlagTextID is a valid event">
		<cfargument name="eventFlagTextID" type="string" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		
		<cfscript>
			var checkEventFlagTextID = "";
		</cfscript>
		
		<cfquery name="checkEventFlagTextID" datasource="#arguments.dataSource#">
			select FlagID,EventGroupID,Location,CountryID,EventStatus 
				from eventDetail 
			where flagID = (select flagid from flag 
				where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>

		<cfif checkEventFlagTextID.recordCount eq 1>
			<cfreturn checkEventFlagTextID>
		<cfelse>
			<cfreturn "The eventFlagTextID [#arguments.eventFlagTextID#] that you have set in the tag is not associated with a valid event.  Please chack you have the correct eventFlagTextID.">
		</cfif>
	</cffunction>
	
	<!--- NJH 2012/10/25 - Social CR - add event merge fields --->
	<cffunction name="getEventMergeFields" access="public" returntype="struct">
		<cfargument name="eventFlagTextID" type="string" required="yes">
		
		<cfset var eventQry = queryNew("startDate,endDate,location,title")>
		
		<cftry>
			<cfquery name="eventQry" datasource="#application.siteDataSource#">
				select location,title,preferredStartDate as startDate,preferredEndDate as endDate
				from eventDetail 
				where flagid  = 
				<cfif isNumeric(arguments.eventFlagTextID)>
					<cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>
				(select flagid from flag 
					where flagtextid =  <cf_queryparam value="#arguments.eventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				</cfif>
			</cfquery>
			
			<cfcatch>
				<cfset queryAddRow(eventQry,1)>
			</cfcatch>
		</cftry>
		
		<cfreturn application.com.structureFunctions.queryRowToStruct(query=eventQry,row=1)>
	</cffunction>



	<!--- 2015-01-30 AHL Case 443215 Complete status for Events (whole function) --->
	<cffunction name="updateCompletedEvents" access="public" returntype="struct">
		<cfargument name="nMaxUpdates" type="numeric" required="false" default="0" />	<!--- 0 will update all events --->
		
		<cfset local.result = {isOK = true}>
		
		<cfquery  name="qryUpdateCompletedEvents" datasource="#application.sitedatasource#">
			UPDATE 
			<cfif arguments.nMaxUpdates gt 0>
				TOP (<cfqueryparam value="#arguments.nMaxUpdates#" cfsqltype="CF_SQL_INTEGER" />)
			</cfif>
			dbo.[EventDetail]
			SET [EventStatus]='Complete'
			WHERE [PreferredEndDate] <= GETDATE()
				AND [EventStatus] NOT IN ('Complete','Cancelled')
		</cfquery>
		
		<cfreturn local.result />
		
	</cffunction>

</cfcomponent>

