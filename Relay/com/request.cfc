<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			request.cfc
Author:				WAB
Date started:		2012-02-16

Description:
Created during security project to hold "request" code
In particular the code for postprocessing every request.
Also added the code for handling requestTimeOut


Amendment History:
Date 		Initials 	What was changed
2012-02-09	WAB 		Mods to requestTimeOut functions so that no longer need to use getDate() [which can't be used if we are in a requestTimeoutSituation]
2012-04-18 WAB	Added function for adding attributes to body tag
2012-06-15	WAB	altered code for adding all the HTML tags so that it could be called in the middle of a request (specifically by CFFLUSH)
2012-07-11	WAB	429147 Made a Major change to how the content buffer was cleared (hopefully no major effect though)
2012-07-31	IH add code to commit transactions that hasn't been committed
2012-09-12  PPB Case 430249 create new global js variable localeInfo
2012-10-25	IH	Case 431391 Force compatibility mode on IE9 to support context menu on internal pages
2012-10-25 	IH 	Case 431499 Add X-UA-Compatible to external pages too to get budget groups drop down populated on IE9.
2013-01-28	WAB	Sprint 15&42 Comms.  Only clear up processLocks when request ends abnormally (ie error or CF_ABORT), so remove from here
2013-02-26	WAB Added functions disable/enableCFOUTPUTOnly()
2013-03-20	WAB	Add code to translate the HTMLHeadBuffer
2013-03-13 	WAB Added isAjax() function to be used to decide whether to display correct HTTP error codes
2013-03-26 	WAB Added loadScript() javascript function (could be moved to some other js file if necessary).
2013-05-20 	WAB Added hideFooter() function
2013-05-28 	WAB Added support for not removing leading whitespace from html
2013-05-29	WAB Deal with script tags in cf_layout/cfajax requests
2014-01-08	WAB Add postProcessContentStructure() (for use by callWebService)
2014-03-20 	AXA Case 439089 need to output a whitespace character to clear the buffer
2014-06-10 	WAB Pull out code to rollback open transactions into a separate function which can then be called from errorHandler.cfc
2014-06-19	WAB CASE 440695 Moved code to collect client time zone from relaycurrentuser, so that it can only appear when we are doing the  <head> tag
2014-09-08	NJH Fix postProcessContentStructure() which was recursing to a wrongly named function
2014-09-23	WAB Changed loadScript code to allow a callback and synchronous loading option
2014-11-25	WAB	CASE 442080 - reinstate translation Ts and logging.  include doTranslation.js if required
2014-12-10	NJH	Add stylesheets dynamically from currentSite.stylsheet.Load them here rather than displayBorder.
2015/02/10	NJH	Fifteen-133 - wider implementation of a previous bug fix (task 834). when we have framesets on a page, then when clicking on the tab, resize the iframe to force IE to re-render content.
2015-05-13  WAB CASE 443300/444527 Add x-frame-options http header to prevent click jacking
2015-11-04 	WAB	Added support for ui messages in request scope, and removed div if message not present
2015-11-27  WAB PROD2015-458 added allowFramingByAllSites()
2015-11-30  WAB PROD2015-458 Automatically allow site to be framed by another domain associated with this Relayware ins
2015-05-19 	WAB CASE 444754 Rotate Session Code
2015-11-20 	WAB CASE 444754 Rotate Session Code, deal with login via webservice.  allow checkForRotateSessionOnRequestEnd() to be called from callwebservice.cfc
2016-01-22	WAB Altered calls to cf_includeJavascriptOnce - renamed attributes.inhead to attributes.useHTMLHead (and negated) because previous name was awful (my fault!)
2016-01-27	WAB BF-385 Security Problems in jQuery-migrate
			WAB	Log JS errors, even on dev sites
2016-02-24 	WAB Added support for request.relayCurrentUser.content.doNotTranslate - can switch off onRequestEnd translation for debugging	purposes
2016-03-10	WAB	Internal Stylesheets were appearing on the Portal
2016-07-01	RJT PROD2016-1353 Added appendToBodyAttribute() function
2016/11/18	NJH	Screens V2 - moved includeOnce custom tags to functions
2017-02-16	DBB	RT-233: Setting compatibility mode so that site UI does not break across IE browsers.
--->
<cfcomponent output="false">

	<cffunction name="doOnRequestEnd" access="public" returntype="boolean" output="true">

		<cfset var content = "">
		<cfset var pos = "">
		<cfset var start = "">
		<cfset var string = "">
		<cfset var args = "">

		<cfset checkForRotateSessionOnRequestEnd()>

		<cfset var functionPointers = [addHTMLDocumentTags,insertHeaderBufferIntoContent,postProcessContentString]>

		<cfparam name="request.runOnRequestEnd" default="true">

		<!--- 2013-05-29 WAB If the request comes from cf_layout we doctor the script tags --->
		<cfif structKeyExists (url,"_cf_containerId")>
			<cfset arrayAppend(functionPointers,putScriptTagsInAjaxOnLoad)>
		</cfif>

		<cfset rollBackOpenTransactions()>

		<!---
		2013-01-28 WAB  Sprint 15&42 Comms
		Remove this deletion of locks
		We will do it if there is a CF_ABORT, or if there is an error, but we will not do it on a normally finishing request
		This allows for fire and forget threads to be spawned which create locks.
		<cfset deleteProcessLocks()>
		 --->
		<cfif request.runOnRequestEnd>
			<cfset updateContentBuffer(functionPointers)>
		</cfif>


		<cfreturn true>
	</cffunction>

	<!---
	Single Function to do all post processing of content
	Either called from onRequestEnd
	Or can be called when content is returned from ajax webServices
	 --->
	<cffunction name="postProcessContentString" access="public" returntype="string" output="false">
		<cfargument name="content">
		<cfargument name="debug" default="false">

		<cfset var result = content>
		<cfset var pos = "">
		<cfset var start = "">
		<cfset var string = "">
		<cfset var args = "">
		<cfset var functionPointer = "">
		<cfset var functionPointerArray = [removeWhiteSpace,AddFormTokensToContent,translateContent]> <!---  --->
		<cfset var tempContent = "">
		<cfset var tickArray = [getTickCount()]>
		<cfset var i = "">

		<cfloop array="#functionPointerArray#" index="functionPointer">
			<cfset args = {content = result}> <!---  top prevent appearance in debug--->
			<cfset result = functionPointer(argumentCollection = args)>
			<cfset arrayAppend(tickArray,getTickCount())>
		</cfloop>

		<cfif debug and not structKeyExists(url,"wsdl") and not (cgi.script_name contains 'webservices')>
				<cfset result = result & "Post Processed">
				<cfloop index="i" from = "2" to="#arrayLen(tickArray)#">
					<cfset result = result & "#numberformat((tickArray[i] - tickArray[i-1])/1000,'0.000')#, ">
				</cfloop>
				<cfset result = result & "Total:#numberformat((tickArray[arrayLen(tickArray)] - tickArray[1])/1000,'0.000')#">
		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- Loop recursively over a structure doing postprocessing (translation/whitespace removal etc.) --->
	<cffunction name="postProcessContentStructure" access="public" returntype="struct" output="false">
		<cfargument name="Structure">

		<cfset var key = "">

		<cfloop collection="#arguments.structure#" item="key">
			<cfset var content = arguments.structure[key]>
			<cfif isStruct(content)>
				<cfset postProcessContentStructure(content)>
			<cfelseif isValid("string",content)>
				<cfif not isNumeric(content) and not isDate(content) >  <!--- don't process numeric or dates, because a) they don't need processing and b) the java pattern matcher falls over on non strings --->
					<cfset arguments.structure[key] = postProcessContentString(content)>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn arguments.structure>
	</cffunction>

	<cffunction name="getGeneratedContent">
		<cfreturn getPageContext().getCFOutput().getBuffer().tostring()>
	</cffunction>


	<!--- WAB late 2012.  To allow us to translate anything which has been put into the <HEAD> by ColdFusion (eg from CFFORM), we--->
	<cffunction name="insertHeaderBufferIntoContent">
		<cfargument name="content">

		<cfset var result = "">
		<cfset var headerBuffer = getHeaderBuffer()>

		<cfif refindnocase ("</head>",content) is 0>
			<cfset result = headerBuffer & chr(10) & content>
		<cfelse>
			<cfset result = rereplaceNocase(content,"(</head>)", "#headerBuffer#\1" )>
		</cfif>



		<cfset clearHeaderBuffer()>
		<cfreturn result>

	</cffunction>
	<!---
		code from www.codersrevolution.com/index.cfm/2009/10/20/Taming-The-Header-Output-Of-CFHTMLHead-and-CFAjaxProxy
		which can access and clear the CFHTMLHead Buffer
	 --->
	<cffunction name="clearHeaderBuffer" output="false" returntype="void">
      <cfscript>
	         var local = StructNew();
          local.out = getPageContext().getOut();
          while (getMetaData(local.out).getName() Is 'coldfusion.runtime.NeoBodyContent')
              { local.out = local.out.getEnclosingWriter(); }
          local.method = local.out.getClass().getDeclaredMethod('initHeaderBuffer', ArrayNew(1));
          local.method.setAccessible(true);
          local.method.invoke(local.out, ArrayNew(1));
      </cfscript>
	  </cffunction>


  <cffunction name="getHeaderBuffer" output="false" returntype="any">
     <cfscript>
         var local = StructNew();
         local.out = getPageContext().getOut();
         while (getMetaData(local.out).getName() Is 'coldfusion.runtime.NeoBodyContent')
             { local.out = local.out.getEnclosingWriter(); }

         local.field =
         local.out.getClass().getDeclaredField("prependHeaderBuffer");
         local.field.setAccessible(true);
         local.buffer = local.field.get(local.out);
         local.output =
         iif(StructKeyExists(local,'buffer'),"local.buffer.toString()",de(""));

         local.field =
         local.out.getClass().getDeclaredField("appendHeaderBuffer");
         local.field.setAccessible(true);
         local.buffer = local.field.get(local.out);

         return local.output &
         iif(StructKeyExists(local,'buffer'),"local.buffer.toString()",de(""));

      </cfscript>
  </cffunction>




	<cffunction name="updateContentBuffer" access="public" returntype="boolean" output="true">  <!--- output must be true, otherwise the clear buffer does not work --->
		<cfargument name="functionPointerArray">

		<cfsetting enablecfoutputonly="true">

		<cfset var newContent = "">
		<cfset var functionPointer = "">
		<cfset var tickArray = [getTickCount()]>
		<cfset var args = "">

		<!---
		WAB: This blank CFOUPUT is essential
		Without it I had the incredibly weird effect of the last query which had been run appearing in the output (assuming that nothing else had been outputted - enablecfoutputonly set to YES)
		It would seem as if it sits in the CFoutput buffer until the next thing is cfoutputted
		DO NOT DELETE
		vvvvvvvvvvvvvvvvvvvvvv  --->

		<cfoutput>#chr(32)#</cfoutput> <!--- 2014-03-20 AXA Case 439089 need to output a whitespace character to clear the buffer --->
		<cfset newcontent = getGeneratedContent()>

		<cfloop array="#functionPointerArray#" index="functionPointer">
			<cfset args = {content = newcontent}> <!---  to prevent appearance in debug--->
			<cfset newContent = functionPointer(argumentCollection = args)>
		</cfloop>

		<!---
			WAB 2012-07-10 429147
			Had problems with clearing the buffer using this command

				<cfset getPageContext().getOut().clearBuffer()>

			If there was a CF_ABORT somewhere between an opening custom tag and a closing custom tag, and the opening custom tag had generated some content,
			then that piece of content did not seem to get cleared from the buffer (or more likely the buffer was cleared and then it was reoutputted to the buffer).
			So that piece of content ended up being duplicated
			Possibly similar to the problem mentioned above which necessitated a blank CFOUTPUT a few lines up.
			Anyhow I discovered that <cfcontent reset=true> would clear the buffer correctly,
			except if there had been an <cfsetting enablecfoutputonly=true> in the earlier code then my <cfoutput>#newContent#</cfoutput>  did not work
			So I ended up having to set enablecfoutputonly=false (as many times as needed!)

			[There is a file BufferProblem.cfm somewhere (WAB test directories) which reproduces and demonstrates the problem]

		--->
		<!--- have to reverse out all the enablecfoutputonly=true   (see comment above) --->
		<cfset disableCFOUTPUTOnly()>

		<!--- Clear the current buffer before outputting my doctored content, keep on same line --->
		<cfcontent reset=true><cfoutput>#newContent#</cfoutput>

		<cfsetting enablecfoutputonly="false">

		<cfreturn true>

	</cffunction>

	<!--- WAB 2013-02-26, move this code to a function and added an enableCFOUTPUTOnly() function which is called in merge.cfc --->
	<cffunction name="disableCFOUTPUTOnly" access="public" returntype="numeric" output="false">

		<cfset var i = 0>
		<cfset var enablecfoutputonlyCount =   getPageContext().getCFOutput().getDisableCount()>
		<cfloop index="i" from ="1" to = "#enablecfoutputonlyCount#">
			<cfset getPageContext().getCFOutput().enablecfoutputonly(false)>
		</cfloop>

		<cfreturn enablecfoutputonlyCount>
	</cffunction>

	<cffunction name="enableCFOUTPUTOnly" access="public" returntype="void" output="false">
		<cfargument name="count" default="1">

		<cfloop index="local.i" from ="1" to = "#count#">
			<cfset getPageContext().getCFOutput().enablecfoutputonly(true)>
		</cfloop>

		<cfreturn >
	</cffunction>


	<cffunction name="removeWhiteSpace" access="public" returntype="string" output="false">
		<cfargument name="content">
		<cfset var args = {string = content}>
		<cfset var leadingWhiteSpace = false>

		<cfif structKeyExists(request,"relaycurrentuser")>
			<cfset leadingWhiteSpace = not(request.relaycurrentuser.errors.showLeadingWhiteSpace) >
		</cfif>

		<cfreturn application.com.regExp.removeWhiteSpace(argumentCollection = args,leadingwhitespace=leadingWhiteSpace)>
	</cffunction>

	<cffunction name="translateContent" access="public" returntype="string" output="false">
		<cfargument name="content">

		<cfset var result = content>
		<cfset var args = {string = result}>

		<cfif structKeyExists (request,"relayCurrentUser") and not request.relayCurrentUser.content.doNotTranslate> <!--- occasionally requests end before we have had a chance to set relaycurrentuser - eg when calling webservices/nosession/amiup.cfm --->
			<cfset result = application.com.relaytranslations.translateStringWithAutoEncode(argumentcollection = args)>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!---
	WAB 2013-05-29
	To sort out problems with cf_layout the way that cfajax processes javascript
	Take all script blocks in the content and put into cfajax's onload function
	--->
	<cffunction name="putScriptTagsInAjaxOnLoad" access="public" returntype="string" output="false">
		<cfargument name="content">

		<cfset var allScriptsString = "">
		<cfset var thisTag = "">
		<cfset var thisScriptString = "">
		<cfset var scriptTags = application.com.regexp.findHTMLTagsInString(inputString = content, htmltag="script",hasendtag= true)>

		<cfloop array="#scriptTags#" index="thisTag">
			<cfset thisScriptString = thisTag.innerHTML>
			<cfset thisScriptString = rereplaceNocase(thisScriptString,"<!--(.*?)//-->","\1","ALL")> <!--- replace the comments put around script by things like CFFORM --->
			<cfset allScriptsString = listappend(allScriptsString,thisScriptString,"#chr(10)#")>
			<cfset arguments.content = replace(arguments.content,thisTag.string,"")>
		</cfloop>

		<cfset AjaxOnLoad("function() {#allScriptsString#}")>

		<cfreturn content>
	</cffunction>

	<cffunction name="AddFormTokensToContent" access="public" returntype="string" output="false">
		<cfargument name="content">
		<cfset var args = {content = content}>
		<cfreturn application.com.security.AddFormTokensToContent(argumentcollection = args)>
	</cffunction>



	<!---
	WAB 2011-04-26 a set of functions for dealing with requestTimeouts
	Prompted by timeouts in dataloads, but something which was at the back of my mind anyway
	We tend to get round timeout problems by just setting enormously large timeouts, but I don't think that this is ideal since real problems with long running requests are just hidden
	I think that it is preferable to only increase timeouts when we actually need to
	So I have created a function extendRequestTimeOutIfRequired() which effectively says
	"I need x Seconds to run this function, make sure that there is at least x seconds of this request still available"
	This is particularly useful if some long running function is being called multiple times in a request,
	rather than guessing the maximum number of times it might be called and how long each call is going to take and ending up with a very large timeout which might then hide some other problem later in the request.
	--->

	<cffunction name="getTimeOut">
		<cfreturn createObject("java", "coldfusion.runtime.RequestMonitor").getRequestTimeout()>
	</cffunction>

	<cffunction name="getElapsedTime" hint="Number of seconds the current request has been running for">
		<cfargument name="applicationScope" default="#application#">
		<!---
			get currentTime from the DB since this is where request.requestTime comes from
			I feel that there is probably a better way of getting this from CF
			WAB 2012-02-08 worked out a way of doing it without going to the db, by storing an offset between db time and webserver time
			This was essential for extending timeouts for error handling
		<cfset var requestLength = "">
		<CFQUERY NAME="requestLength" DATASOURCE="#applicationScope.SiteDataSource#">
			SELECT datediff(s,#createOdbcdatetime(request.requesttime)#,getdate()) as secs
		</CFQUERY>
		--->

		<cfset var result = datediff("s",request.requestTime,application.com.dateFunctions.getDBTime()) >


		<cfreturn result>
	</cffunction>


	<cffunction name="extendTimeOut" hint="add x seconds to the requestTimeOut">
		<cfargument name="seconds" required="yes" type="numeric">

		<cfset var newTimeout = getTimeOut() + seconds>
		<cfsetting requestTimeout = #newTimeout#>

	</cffunction>

	<cffunction name="getTimeLeft" hint="how long is left until request times out (negative if already past it!)">

		<cfreturn getTimeOut() - getElapsedTime()>

	</cffunction>


	<cffunction name="extendTimeOutIfNecessary" hint ="makes sure that there is at least x seconds of request left">
		<cfargument name="seconds" required="yes" type="numeric">

		<cfset var amountToExtend = seconds - getTimeLeft()>

		<cfif amountToExtend gt 0>
			<cfset extendTimeOut(amountToExtend)>
		</cfif>

	</cffunction>


	<cffunction name="initialiseDocumentVars" access="public" output="false">

		<cfset request.document = structNew()>
		<cfset request.document.addHTMLDocumentTags = showHTMLDocumentTags()>
		<cfset request.document.addBodyTag = request.document.addHTMLDocumentTags>  <!--- in some situations need to be able to turn off body tag (eg for framesets, although we are going to do that automatically) --->
		<cfset request.document.openingTagsDone = false>
		<cfset request.document.title = request.currentSite.title>
		<cfset request.document.body = structNew()>
		<cfset request.document.showFooter = true>
		<cfset request.document.docType = "<!DOCTYPE html>">
		<cfset request.document.metaTags = structNew()>
		<cfset request.document.metaTags["Content-Type"] = "text/html; charset=UTF-8">
		<cfset request.document.metaTags["X-UA-Compatible"] = "IE=Edge"> <!--- force IE to default to standards mode.. otherwise, we have browser mode and document mode in different versions which causes many problems --->
		<cfset request.document.metaTags["viewport"] = "width=device-width, user-scalable=no, initial-scale=1,maximum-scale=1">
		<!--- 	WAB 2015-01-06 CASE 443300/444527 add anti-clickjacking header
					Note that adding items to the headers structure will automatically add it to the request (as long as addHTMLDocumentTags is set)
				WAB 2015-11-27 PROD2015-458 Got rid of the setting security.injection.x_frame_options because it just didn't do what I had hoped
					and fixed typo in request.document.httpHeaders = {} (which luckily hadn't mattered)
		--->
		<cfset request.document.httpHeaders = {}>
		<cfset request.document.httpHeaders["X-Frame-Options"] = "SAMEORIGIN"> <!--- to allow framing by all sites use allowFramingByAllSites() --->
		<cfset request.document.httpHeaders["X-UA-Compatible"] = "IE=9; IE=8; IE=7; IE=EDGE"><!---DBB RT-233--->
		
		<!--- WAB 2015-11-30 PROD2015-458 Check for cases where we might allow framing --->
		<cfif getReferringHost() is not "" and getReferringHost() is not cgi.http_host>
			<!--- Allow to be framed by any of our own sites --->
			<cfif application.com.relayCurrentSite.isRelayDomain (getReferringHost())>
					<cfset request.document.httpHeaders["X-Frame-Options"] = "ALLOW-FROM #getReferringHost(withProtocol = true)#"> <!--- to allow framing by all sites use allowFramingByAllSites() --->
			</cfif>
		</cfif>

		<cfset request.relayFormDisplayStyle = "HTML-div">

		<cfif request.relayCurrentUser.isInternal>
			<cfset request.topHead = structNew()>
			<!--- WAB HTML-div all the way!<cfset request.relayFormDisplayStyle = "HTML-table">--->
			<cfset request.document.h1 = {show=true,text="",id=""}>
		</cfif>

		<cfsavecontent variable="request.document.stylesheets">
			<cfoutput>
				<cfloop list="#request.currentSite.stylesheet#" index="local.stylesheet">
					<cf_includeCssOnce template="#local.stylesheet#" checkIfExists="true" useHTMLHead="false">
				</cfloop>
			</cfoutput>
		</cfsavecontent>

		<cfif structKeyExists (request.relayCurrentUser.errors, "showdebug")>  <!--- If statement only required during migration --->
			<cfsetting showDebugOutput = "#request.relayCurrentUser.errors.showDebug#">
		</cfif>


	</cffunction>

	<!--- Use in CF_BODY --->
	<cffunction name="setBodyAttribute" access="public" output="false">
		<cfargument name="Attribute" required="true">
		<cfargument name="Value" required="true">

		<!--- Append event attributes, all others overright--->
		<cfif structKeyExists (request.document.body,attribute) and left(attribute,2) is "on">
			<cfset listAppend(request.document.body[attribute],attribute,";")>
		<cfelse>
			<cfset request.document.body[attribute] = value>
		</cfif>

	</cffunction>

	<!--- Appends to an attribute on the body. Note it does not add any deliminators by default --->
	<cffunction name="appendToBodyAttribute" access="public" output="false">
		<cfargument name="attribute" required="true">
		<cfargument name="value" required="true">
		<cfargument name="delimiter" default="">

		<cfif structKeyExists (request.document.body,attribute)>
			<cfset request.document.body[attribute] &= delimiter & value>
		<cfelse>
			<cfset setBodyAttribute(Attribute,Value)>
		</cfif>

	</cffunction>

	<!--- Function to decide whether request needs all the tags added --->
	<cffunction name="showHTMLDocumentTags" access="private" returnType="boolean">
		<cfset var addHTMLDocumentTags = true>

		<cfif structKeyExists(url,"wsdl")
			or (cgi.script_name contains 'webservices')
			or isAjax()
			or structKeyExists(url,"openAsExcel")
			or structKeyExists(url,"openAsWord")
			or structKeyExists(url,"suppressOutput")>
			<cfset addHTMLDocumentTags = false>
		</cfif>

		<cfreturn addHTMLDocumentTags>
	</cffunction>


	<cffunction name="isAjax" access="public" returnType="boolean" output="false">

		<cfset var result = false>
		<cfif 	(isDefined("returnFormat") and (returnFormat is "JSON" or returnFormat is "PLAIN"))>
			<cfset result = true>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="doNotAddHTMLDocTags" access="public">
		<cfset request.document.addHTMLDocumentTags = false>
	</cffunction>


	<!--- 	WAB 2015-11-27  PROD2015-458
			This function can be called on any page which should be allowed to be framed by any third party site
	--->
	<cffunction name="allowFramingByAllSites" access="public">
		<cfset request.document.httpHeaders["X-Frame-Options"] = "ALLOWALL">
	</cffunction>


	<cffunction name="setMetaTag" access="public">
		<cfargument name="name" type="string" required="true">
		<cfargument name="value" type="string" required="true">

		<cfset request.document.metaTags[arguments.name] = arguments.value>
	</cffunction>


	<cffunction name="emulateIEVersion" access="public">
		<cfargument name="IEVersion" type="numeric" required="true">

		<cfset setMetaTag(name="X-UA-Compatible",value="IE=EmulateIE#arguments.IEVersion#")>
	</cffunction>


	<!--- Gets all DocType, HTML, HEAD & BODY Tags and javascript required at the beginning of a document --->
	<cffunction name="getHTMLHeader" access="private">
		<cfargument name="content" type="string" required="true">

		<cfset var htmlHeader = "">
		<cfset var lang = lcase(IIF(request.relayCurrentUser.languageISOCode neq "",DE(request.relayCurrentUser.languageISOCode),DE("en")))>
		<cfset var currentDir = "">
		<cfset var topHead = "">

		<!--- Body tags break framesets.  Could leave off all the extra tags, except that the current loading icon code looks for one of the JS variables set below --->
		<cfif content contains "<frameset">
			<cfset request.document.addBodyTag = false>
		</cfif>


		<cfif request.document.addHTMLDocumentTags>

			<!--- 2015-01-06 WAB CASE 443300/444527 add httpHeaders (specifically x-frame-options)
			--->
			<cfloop collection="#request.document.httpHeaders#" item="local.header">
				<cfheader name="#local.header#" value="#request.document.httpHeaders[local.header]#">
			</cfloop>

			<cfsavecontent variable="htmlHeader"><cfoutput>#request.document.docType#
					<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="#lang#" lang="#lang#">
						<!---
							2012-10-25 IH Case 431499 Add X-UA-Compatible to external pages too to get budget groups drop down populated on IE9. This line must be before the head tag otherwise won't work (cfform incompatibility)
							2013-04-30 WAB Case 434590  Removed
							context menus now compatible with IE9/10
							Other solutions available for the <P><FORM> issue
							<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
						--->
						<head>

							<title>#htmlEditFormat(request.document.title)#</title>
							<cfloop collection="#request.document.metaTags#" item="local.metaTag">
							<meta name="#local.metaTag#" content="#request.document.metaTags[local.metaTag]#"/>
							</cfloop>
							<!---
							WAB 2013-03-26 loadScript() function allows scripts to be loaded via prototype or cfform ajax calls (where <script src="xxx"> does not work)
											It tests so that scripts are only loaded once (which makes it worth using on jQuery calls as well)
											This script could go in some .js file which we load on every request
							WAB 2014-09-23	Added support for a callback function, which allows for ondemand loading of js libraries.
											Supports both asynch (default) and synch loading.
											Synch not ideal but sometimes unavoidable within our framework (for example within our form validation which is not asynchronous)
							 --->
							<script>
							loadScript = function (src,callback,synchronous) {
								head = $$('head')[0];
								if (head)	{
									<!--- search for existing scripts with same name. Most scripts will have the src attribute, but those loaded using the synchronous capability of this function will not have a src attribute, but we will have added a data-src attribute --->
									testForExistingScript = $$('script[src="'+ src +'"]').length || $$('script[data-src="'+ src +'"]').length
									if (!testForExistingScript) {
										if (synchronous) {
											<!--- get some kind of XMLHttpRequest --->
											var xhrObj = new XMLHttpRequest();
											<!--- open and send a synchronous request --->
											xhrObj.open('GET', src, false);
											xhrObj.send('');
											<!--- add the returned content to a newly created script tag --->
									    	script = new Element('script', { type: 'text/javascript', 'data-src': src });
									    	script.text = xhrObj.responseText
											head.appendChild(script);
											if (callback) {callback ()}
										} else {
									    	script = new Element('script', { type: 'text/javascript', src: src });
									    	if (callback) {script.onload = callback}
										    head.appendChild(script);
										}
									} else {
										if (callback) {callback ()}
									}
								}
							}
							</script>

							<script type="text/javascript">
								var domainAndRoot = location.protocol + '//' + location.hostname; <!--- WAB 2011-02-02 - thought might as well get it via JS "#request.currentSite.protocolAndDomain#"; --->
								<!--- 2009-06-23 WAB as part of security review, only show this variable if person is a valid internal user.  No need for this variable to be accessed unless this is the case (probably only used for links for content editors and translators) --->
								<cfif request.relayCurrentUser.isValidRelayInternalUser>
									<cfif request.currentSite.isInternal>
										var iDomainAndRoot = domainAndRoot;
									<cfelse>
										var iDomainAndRoot = "#jsStringFormat(application.com.relayCurrentSite.getSiteProtocol(request.currentsite.internalDomain))##request.currentsite.internalDomain#";
									</cfif>
								</cfif>
								<cfif not request.relayCurrentUser.isInternal>
									var elementID = #structKeyExists(request,"currentElement")?request.currentElement.ID:0#;
								</cfif>

								<!--- TODO Are any of these still used?  Really date from late 1990s when I wasn't very good with JS dates! --->
								var DBServerDate = "#DateFormat(request.requestTime,"dd/mm/yyyy")#";
								var DBServerTime = "#jsStringFormat(request.requestTime)#";
								var WEBServerDate = "#DateFormat(now(),"dd/mm/yyyy")#";
								var WEBServerTime = "#jsStringFormat(now())#";
								var _rw = {sessiontoken : '#jsStringFormat(application.com.security.getsessionToken(sessionID = getSessionID()))#'} <!--- note sessiontoken lower case--->
								var phr = new Array(); <!--- this is used for javascript translations --->
								var localeInfo =  {decimalPoint : '#jsstringformat(request.relaycurrentuser.LocaleInfo.decimalInfo.decimalPoint)#'};		<!--- 2012-09-12 PPB/WAB Case 430249  --->
								var isInternal = #request.currentSite.isInternal?true:false#;

								<!--- 2009-03-03 WAB added for debugging instances.  Needs relaycurrentsite update
									2009-04-01 GCC for security reasons only show if cfdebug mode is on and only to showsitetotheseIPs
									2009-09-09 WAB added application.testSite is 2
								--->

								<!--- NJH 2008-05-21 P-TND066 1.1.3 --->
								<cfif structKeyExists(url,"mediaMode") and url.mediaMode eq "print">
										window.onload = function() {
											window.print();
										}
								</cfif>
							</script>

								<!--2015-04-08 SB BOOTSTRAP CSS-->
								<cfset includeCssOnce(template = "/javascript/bootstrap/css/bootstrap.min.css", useHTMLHead="false")>
								<!--2016-03-10 WAB These Styles should only be on Back Office-->
								<cfif request.relayCurrentUser.isInternal>
									<cfset includeCssOnce(template = "/styles/relayware-mobile.css", useHTMLHead="false")>
									<cfset includeCssOnce(template = "/styles/relayware-tablet.css", useHTMLHead="false")>
									<cfset includeCssOnce(template = "/styles/relayware-desktop-medium.css", useHTMLHead="false")>
									<cfset includeCssOnce(template = "/styles/relayware-desktop-large.css", useHTMLHead="false")>
								</cfif>
								<!--2016-08-11 TWR Font Awesome for icons v4.7.0 -->
								<cfset includeCssOnce(template = "/styles/fontAwesome/css/font-awesome.min.css", useHTMLHead="false")>

								<!--Other JS-->
								<cfset includeJavascriptOnce(template = "/javascript/lib/prototype.min.js", useHTMLHead="false")>
								<cfset includeJavascriptOnce(template = "/javascript/lib/jquery/jquery.min.js", useHTMLHead="false")>
								<cfset includeJavascriptOnce(template = "/javascript/lib/jquery/jquery-migrate-rw.js", useHTMLHead="false")>
								<cfset includeJavascriptOnce(template = "/javascript/lib/jquery/jquery-ui.min.js", useHTMLHead="false")>
								<cfset includeJavascriptOnce(template = "/javascript/lib/modernizr/modernizr-custom.js", useHTMLHead="false")>
								<cfset application.com.request.includeJavascriptOnce(template="/javascript/lib/jquery/spinner.js", useHTMLHead="false")>

								<!--2015-04-08 SB BOOTSTRAP JS-->
								<cfset includeJavascriptOnce(template = "/javascript/bootstrap/js/bootstrap.js", useHTMLHead="false")>

								<!--- WAB 2014-06-19 CASE 440695 Code moved from relayCurrentUser --->
								<cfif not request.relayCurrentUser.localeInfo.clientUTCreceived>
									<cfset includeJavascriptOnce(template = "/javascript/getClientTimeZone.js", useHTMLHead="false")>
								</cfif>

								<!--- WAB 2014-11-25 CASE 442080 (reinstate Ts and logging)  --->
								<cfif application.com.relayTranslations.showTranslationLinks() or structKeyExists(cookie,"translator") and cookie.translator is not "" >
									<cfset includeJavascriptOnce(template = "/javascript/doTranslation.js", useHTMLHead="false")>
								</cfif>

								<!--- WAB 2014-09-15 attempt to implement single scrollbar --->
								<!--- <cfif request.CurrentSite.isInternal and request.script_name neq "/index.cfm">
									<cf_includeJavascriptOnce template = "/javascript/iframeResizer/iframeResizer.contentWindow.js">
								</cfif> --->

								<!--- <script>
									jQuery.extend({ alert: function (message, title) {
										jQuery("<div></div>").dialog( {
										    buttons: { "Ok": function () {jQuery(this).dialog("close"); } },
										    close: function (event, ui) { jQuery(this).remove(); },
										    resizable: false,
										    title: title,
										    modal: true
										  }).text(message);
										}
									});
								</script> --->

								<cfset includeJavascriptOnce(template = "/javascript/jsErrorHandler.js", useHTMLHead="false")>
								<cfset includeJavascriptOnce(template="/javascript/lib/fancybox/jquery.fancybox.pack.js", useHTMLHead="false")>
								<cfset includeCssOnce(template="/javascript/lib/fancybox/jquery.fancybox.css",useHTMLHead="false")>

								#request.document.stylesheets#

								<!--- WAB 2004-06-03 load translation javascript if translator cookie is set
									should be able to removed this from all the individual templates it is in
									added test for relayCurrentUser 2005-07-05
								--->

								<!--- <CFIF request.relayCurrentUser.Content.showTranslationLinks or structKeyExists(cookie,"translator") and cookie.translator is not "" >
									<cf_includejavascriptonce template = "/javascript/doTranslation.js">
								</cfif> --->

								<cfif ((application.testSite is 2 or reFindNoCase(listAppend(application.com.settings.getSetting("relaywareInc.breakoutRegexp"),"127\.0\.0\.1","|"),CGI.REMOTE_ADDR)) and isDebugMode() )>
								<!-- #request.currentsite.instance.name# #request.currentsite.instance.IPAddress# #request.currentsite.instance.Port# #request.currentSite.IPAddressOfSwitch# 	-->
								</cfif>

								<!--- NJH 2014/10/14 Task Core 834/ Fifteen-133 (2015/02/10) a bit of a hack for an IE 11 problem where a page with framesets. For some reason, moving away from a page that had framesets
									caused the page to not be displayed when returning to it. Only after changing the height of the div containing the iframe which contained
									the frameset caused the page to display again. This can be removed once we get rid of the framesets.
									 --->
								<cfif content contains "<frameset" and request.relayCurrentUser.isInternal>
									<script>
									var pid = jQuery(window.frameElement).parent().parent().parent().attr('id');

									jQuery('##relayTabPanel__'+pid,parent.document).click(function() {
										var parentHeight = jQuery(window.frameElement).parent().height();

										jQuery(window.frameElement).parent().height(parentHeight-1);
										setTimeout(function() {jQuery(window.frameElement).parent().height(parentHeight);},100); <!--- resetting the height back to the original --->

										<!--- now deal with the 3-pane view. Go through the frames within the iframe and then check if any of those frames have a frameset called entityNavigationFrameset. Not a particularly elegant
											solution, but this is not an elegant problem. Once we have the frameset, we slightly change the rows attribute which forces a rerender of the content. Ideally we want to only run this
											if running in IE (> 11) and if on a page with the entityNav frameset on it.--->
										jQuery('frame',jQuery(window.frameElement).document).each(function(index) {
										 	var entityNavFrameset = jQuery(jQuery(this)[0].contentDocument).find('##entityNavigationFrameset');
										 	if (entityNavFrameset.length) {
										 		var origRowsAttribute = entityNavFrameset.attr('rows');
										 		rowHeight = parseInt(origRowsAttribute.split(',')[0]);
										 		var rowHeightInPx = rowHeight+'px';
										 		entityNavFrameset.attr('rows',rowHeightInPx+',*')
										 		entityNavFrameset.attr('rows',rowHeight+',*')
										 	}
										 })
									});
									</script>
								</cfif>
								<!--- SB 2015/02/10 Added Google Analytics --->
								<script>
									  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
									  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
									  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
									  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

									  ga('create', 'UA-50943852-2', 'auto');
									  ga('send', 'pageview');
								</script>
						</head>
						<cfif request.document.addBodyTag>
							<body #application.com.structureFunctions.convertStructureToNameValuePairs(struct=request.document.body, encode="HTMLAttribute",includeNulls=false,qualifier='"',delimiter=" ")#>
							<!--- SB 2015/02/10 Added Google Analytics --->
							<!-- Google Tag Manager -->
							<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5W5SN7"
							height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
							<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
							new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
							j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
							'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
							})(window,document,'script','dataLayer','GTM-5W5SN7');</script>
							<!-- End Google Tag Manager -->

							<!--- WAB 2015-07-03 add isLoggedIn test before including topHead, some such as productsTopHead cause errors when not logged in --->
							<cfif request.relayCurrentUser.isInternal >
								<div class="internalBodyPadding">
								<cfif request.relayCurrentUser.isLoggedIn>
									<cf_includeTopHead>
								</cfif>
								<!---  2015-11-04 WAB	Added support for messages in request scope, and removed div if message not present --->
								<cfset var message = application.com.relayUI.getMessage(scope="session,request")>
								<cfif message is not ""><div id="message">#message#</div></cfif>

							</cfif>
						</cfif>

				 </cfoutput>
			</cfsavecontent>
		</cfif>

		<cfreturn htmlHeader>
	</cffunction>


	<cffunction name="getHTMLFooter" access="private">

		<cfset var htmlFooter  = "">

		<cfif request.document.addHTMLDocumentTags>
			<cfsavecontent variable="htmlFooter">
				<cfoutput>
						<cfif request.relayCurrentUser.isInternal and request.document.showFooter>
							<cfinclude template="/templates/InternalFooter.cfm">
						</cfif>
						<cfif request.document.addBodyTag><cfif request.relayCurrentUser.isInternal><!--- internal body padding---></div></cfif></body></cfif>
					</html>
				</cfoutput>
			</cfsavecontent>
		</cfif>

		<cfreturn htmlFooter>

	</cffunction>

	<!--- This function is called by CF_FLUSH --->
	<cffunction name="haveHTMLDocumentOpeningTagsBeenAdded" access="public">
		<cfreturn request.document.openingTagsDone>
	</cffunction>


	<!--- Adds both Opening Tags to content (for use mid request - eg for CF_FLUSH)--->
	<cffunction name="addHTMLDocumentOpeningTags" access="public">
		<cfargument name="content" type="string" required="true">

		<cfset var htmlHeader = "">

		<cfif not request.document.openingTagsDone>
			<cfset htmlHeader = getHTMLHeader(content)>
			<cfset request.document.openingTagsDone = true>
		</cfif>

		<cfreturn htmlHeader & arguments.content>
	</cffunction>

	<!--- Adds Opening and Closing tags to content --->
	<cffunction name="addHTMLDocumentTags" access="private">
		<cfargument name="content" type="string" required="true">

		<cfset var htmlHeader = "">
		<cfset var htmlFooter = "">

		<cfif structKeyExists (request,"document")>  <!--- protects against this variable not being set - not common but can happen if there is a custom application.cfc which still uses our standard error handler  --->
			<cfset htmlFooter = getHTMLFooter()>

			<cfif not request.document.openingTagsDone>
				<cfset htmlHeader = getHTMLHeader(content = content)>
			</cfif>
		</cfif>

		<cfreturn htmlHeader & arguments.content & htmlFooter>
	</cffunction>

	<cffunction name="deleteProcessLocks" access="private">
		<cfset application.com.globalFunctions.deleteThisRequestsProcessLocks()>
	</cffunction>


	<cffunction name="setTopHead" access="public">
		<cfloop collection="#arguments#" item="local.arg">
			<cfset request.topHead[local.arg] = arguments[local.arg]>
		</cfloop>
	</cffunction>

	<cffunction name="hideFooter" access="public">
		<cfset request.document.showFooter = false>
	</cffunction>


	<cffunction name="rollBackOpenTransactions" access="public">
		<cfif structKeyExists(request,"openTransaction") and request.openTransaction is 1>
			<cfquery name="local.tran" datasource = "#application.sitedatasource#">
				IF @@TRANCOUNT > 0 ROLLBACK tran
			</cfquery>
			<cfset structDelete(request,"openTransaction") >
		</cfif>
	</cffunction>


	<cffunction name="getReferringHost" access="public">
		<cfargument name="withProtocol" default = "false">

		<cfset var regExp = "(http(?:s)?://)(.*?)/.*">
		<cfset var replacement = "\2">
		<cfif withProtocol>
			<cfset var replacement = "\1\2">
		</cfif>

		<cfset var referringHost = reReplaceNoCase (cgi.http_referer,regExp,replacement)>

		<cfreturn referringHost>

	</cffunction>


	<cffunction name="setDocumentH1" access="public">
		<cfargument name="show" type="boolean" required="false">
		<cfargument name="text" type="string" default="">

		<cfloop collection="#arguments#" item="local.arg">
			<cfif structKeyExists(arguments,local.arg)>
				<cfset request.document.h1[local.arg] = arguments[local.arg]>
			</cfif>
		</cfloop>
	</cffunction>


	<cffunction name="showDocumentH1" access="public" returnType="boolean">
		<cfreturn request.document.h1.show>
	</cffunction>


	<cffunction name="getDocumentH1" access="public" returnType="string">
		<cfreturn request.document.h1.text>
	</cffunction>

	<cffunction name="outputDocumentH1" access="public">
		<cfif showDocumentH1()>
			<cfoutput><h1 #application.com.structureFunctions.convertStructureToNameValuePairsForHTMLOutput(struct=request.document.h1,includeKeysRegexp="id")#>#application.com.security.sanitiseHTML(request.document.h1.text)#</h1></cfoutput>
		</cfif>
	</cffunction>


	<!---
		WAB 2015-05-19 CASE 444754 Rotate Session Code


	--->

	<cffunction name="rotateSession" access="public">
		<cfargument name="delay" default="true" hint="delay to end of request - this allows session variables to continue to be set">
		<cfargument name="clear" default="true" hint="clear out everything in the current session">

		<cfif clear>
			<!--- before clearing the session scope we take a copy of the existing sessionid.  We are going to need this later - so that when this session expires we have the correct sessionid --->
			<cfset var oldSessionInfo = {sessionID = session.sessionid}>
			<!--- clear the session scope and then put back the old session id in a temporary location --->
			<cfset structClear (session)>
			<cfset session.tempSessionBeforeRotation = oldSessionInfo>
			<!--- Now set a temporary sessionid.  We cannot create the new java session yet because as soon as we do this we cannot put anything new into the session scope (until the next request) --->
			<cfset session.sessionID = "rotatedFrom #oldSessionInfo.sessionID#">

		</cfif>

		<cfif delay>
			<!--- when delay is set, we will pick up this variable onRequestEnd  at which point we will actually create the new session --->
			<cfset request.rotateSession =  true>
		<cfelse>
			<!--- remember the sessionid before creating the new java session.  It will be a temporary session id of the form rotatedFrom xxxxxx --->
			<cfset var temporarySessionID = session.sessionID>
			<cfset var newSessionID = jeeRotateSession()>
			<cfset structDelete (request,"rotateSession")>

			<!--- Update Visit Table to reflect correct (new) sessionid --->
			<cfquery name="local.updateVisit">
			update visit set jsessionid =  <cf_queryparam value="#newSessionID#" CFSQLTYPE="CF_SQL_VARCHAR" > where jSessionID =  <cf_queryparam value="#temporarySessionID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

		</cfif>

	</cffunction>


	<cffunction name="checkForRotateSessionOnRequestEnd" output="false" access="public">

		<cfif structKeyExists (	request, "rotateSession")>
			<cfset rotateSession(delay=false,clear=false)>
			<cfset structDelete (	request, "rotateSession")>
		</cfif>

	</cffunction>


	<cffunction name="jeeRotateSession" output="false" returntype="string" access="private">
		<!---
		Courtesey of https://www.petefreitag.com/item/829.cfm
		 --->
		<cfset var sessionScopeToPassToNewSession = Duplicate(session)>
		<cfset var req = getPageContext().getRequest()>
		<cfset StructDelete(sessionScopeToPassToNewSession, "sessionid")>
		<cfset StructDelete(sessionScopeToPassToNewSession, "urltoken")>

		<!--- 	When we expire the old session it needs to have the correct sessionid.  At the moment it has a temporary sessionid 'rotatedfrom xxxx'
				We have already taken a copy of the new Session variables to pass to the new session
				So we can now clear the session scope and pop the old information back into it
		--->

		<cfif structKeyExists (session,"tempSessionBeforeRotation")>
			<cfset structClear(session)>
			<cfset structAppend(session,sessionScopeToPassToNewSession.tempSessionBeforeRotation)>
			<cfset StructDelete(sessionScopeToPassToNewSession, "tempSessionBeforeRotation")>
		</cfif>

		<!--- invalidate and timeout old session.
				Give a few seconds to allow current request to finish - just in case.
			Note that req.getSession().setMaxInactiveInterval() does not work
		--->

		<cfset session.setMaxInactiveInterval(6)>
		<cfset req.getSession().invalidate()>
		<!--- create a new JEE session --->
		<cfset local.newSession = req.getSession(true)>
		<cfset var newID = local.newSession.getId()>

		<!--- copy the session scope that we want to be passed into the new session into into a temporary key in the new java session,
			copying this into the coldFusion session is handled in onSessionStart --->
		<cfif NOT StructIsEmpty(sessionScopeToPassToNewSession)>
			<cfset local.newSession.setAttribute("jeeRotateSessionOldSession", sessionScopeToPassToNewSession)>
		</cfif>

		<cfreturn newID>
	</cffunction>


	<cffunction name="getSessionID" access="public" output="false" hint="Get sessionID directly from Java - need to use this if in process of session rotation ">
		<cfreturn getPageContext().getRequest().getSession().getID()>
	</cffunction>


	<cffunction name="checkForRotateSessionOnSessionStart" output="false" returntype="string" >

		<cfset local.oldSession = getPageContext().getRequest().getSession().getAttribute("jeeRotateSessionOldSession")>
		<cfif NOT IsNull(local.oldSession) AND NOT structIsEmpty(local.oldSession)>
			<cfset structAppend (session,local.oldSession,false)>
			<cfset getPageContext().getRequest().getSession().removeAttribute("jeeRotateSessionOldSession")>
		</cfif>
	</cffunction>


	<cffunction name="includeJavascriptOnce" access="public" output="true" returnType="void">
		<cfargument name="template" type="string" required="true">
	    <cfargument name="translate" type="boolean" default="false">
	    <cfargument name="mergeStruct" default="#structNew()#" type="struct"/>
	    <cfargument name="useHTMLHead" default="true" type="boolean"/>
	    <cfargument name="callback" default="" type="string">
	    <cfargument name="synchronousCallback" default="false" type="boolean"/> <!--- If you have an Ajax request makes calls this tag twice for the same js file with two callbacks, then the second callback will fail.  Until I recode completely to collect all the calls and output them at the end, we have to go with a synchronous load --->
		<cfargument name="includeCustom" type="boolean" default="#not listContainsNoCase(arguments.template,'code','/')? true:false#"/> <!--- don't include if this is a custom file already --->

		<cfset var jsTemplateList = arguments.template>
		<cfif arguments.includeCustom and left(arguments.template,1) eq "/">
			<cfset jsTemplateList = listAppend(jsTemplateList,"/code"&arguments.template)>
		</cfif>

		<cfset var scriptsToLoad  = []>
		<cfset var allPhraseScriptJS = "">
		<cfset var jsTemplate = "">
		<cfloop list="#jsTemplateList#" index="jsTemplate">
			<cfparam name="request.includeJavascriptOnce" default = "#structNew()#">

			<!---<cfset isCustom = false>2015/12/03 GCC/DAN/NJH 446496 --->
			<cfset var scriptTag = "">

	       <cfif not structKeyExists(request.includeJavascriptOnce, jsTemplate) >
	      	    <cfset  request.includeJavascriptOnce[jsTemplate] = "true">

				<!--- if dealing with a template that doesn't exist, then set includeJavascriptOnce to false so we don't include non-existent files
					NJH 2016/03/01 - changed to check all files exist if file is local.
				 --->
				<cfif left(jsTemplate,4) neq "http">
				<!--- 	<cfset isCustom = true> 2015/12/03 GCC/DAN/NJH 446496  --->
					<cfif not fileExists(expandPath(jsTemplate))>
						<cfset request.includeJavascriptOnce[jsTemplate] = "false">
					</cfif>
				</cfif>

				<cfif request.includeJavascriptOnce[jsTemplate]>
					<cfif arguments.translate>

					<!---
					WAB 2010/03/25 for doing translations within js files
					use phr.phraseTextID to output a phrase in a .js file
					note that it is case sensitive (in that you must always use the same case on a single page)
					Creates a JS array called phr with translations
					--->
						<cfif not structKeyExists(application.phrasesInJavascript,jsTemplate)>
							<cfset var isCustom = (listContainsNoCase(jsTemplate,'code','/'))> <!---2015/12/03 GCC/DAN/NJH 446496 added to enable a custom template to be loaded and translated even if it isn't overriding a core js file--->
							<cfset var filePath = (not isCustom? application.path: application.paths.aboveCode) & jsTemplate>
							<cffile action="read" file="#filepath#" variable="jsFileString">
							<cfset var argumentCollection = {string = jsFileString}>  <!--- to prevent content showing in debug --->
							<cfset application.phrasesInJavascript[jsTemplate] = application.com.relayTranslations.extractListOfJavascriptPhrasesFromAString(argumentCollection = argumentCollection)>
						</cfif>

						<cfset var thisPhraseScripts = "">
						<cfset var phrase = "">
						<cfsavecontent variable="thisPhraseScripts">
							<cfoutput>
								<!--- note that the phr array is created in applicationMain.cfc --->
								<!--- WAB 2012-10-02 added logMergeErrors - we were getting a problem with rwFormValidation.js where there are phrases which have [[MAX]] and [[MIN]] in them.  These are not merged in at translate time, but later in javascript.
										However we end up logging an error on each occasion that they are called.  So don't bother log merge errors unless there is a merge struct.  Bit nasty but will work
									  WAB 2013-09-18 modified so that we don't even try to evaluateVariables unless there is a mergeStruct.  Prevents exceptions in the debug.
								--->
								<cf_translate encode="javascript" mergeStruct=#arguments.mergeStruct# evaluateVariables=#structCount(arguments.mergeStruct)#>
										<cfloop index="phrase" list = "#application.phrasesInJavascript[jsTemplate]#">phr['#jsStringFormat(phrase)#'] = 'phr_#jsStringFormat(phrase)#';
									</cfloop>
								</cf_translate>
							</cfoutput>
						</cfsavecontent>

						<cfset allPhraseScriptJS = listAppend(allPhraseScriptJS,thisPhraseScripts,chr(10))>
					</cfif>


					<!---
					WAB 2011/05/10 - just use relative paths
					WAB 2012-12-05 CASE 430659 added anticaching number
					--->

					<cfset arrayAppend (scriptsToLoad, "#jsTemplate#?v=#application.instance.relaywareversioninfo.svnCommit#")>

				</cfif>

			</cfif>

		</cfloop>


		<cfif arrayLen (scriptsToLoad) or arguments.callBack is not "">
			<cfsavecontent variable="scriptTag">
				<cfif not application.com.request.isAjax()>
					<cfset var script = "">
					<cfloop array = "#scriptsToLoad#" index="script">
							<cfoutput><script type="text/javascript" src="#script#"></script></cfoutput>
					</cfloop>
					<cfif arguments.callback is not "">
						<cfoutput><script>jQuery( document ).ready(function () { #arguments.callBack# })</script></cfoutput>
					</cfif>
				<cfelse>
					<!---
							WAB 2013-03-26 Alter the way scripts are loaded in ajax requests so that will be loaded by prototype and cfform ajax calls
										Note reliance on loadScript function which is automatically added to the parent document (currently in request.cfc)
					--->
					<cfset arguments.useHTMLHead = false>
					<cfoutput>
					<script>
						<cfif arrayLen(scriptsToLoad)>
							<cfset var loopCount = 0>
							<cfloop array = "#scriptsToLoad#" index="script">
								<cfset loopCount ++>
								loadScript ('#script#' <cfif loopcount is arrayLen(scriptsToLoad) and arguments.callBack is not "">, function () {#arguments.callBack#} ,#arguments.synchronousCallback#</cfif>)
		 					</cfloop>

						<cfelseif arguments.callBack is not "">
							#arguments.callBack#
						</cfif>
					</script>
					</cfoutput>
				</cfif>
				<cfif allPhraseScriptJS is not "">
					<cfoutput><script>#allPhraseScriptJS#</script></cfoutput>
				</cfif>
			</cfsavecontent>

			<cfif arguments.useHTMLHead>
				<cfhtmlhead text="#scriptTag#">
			<cfelse>
				<!--- this is used when we are already in the head, and we want to output the script tag in the place that we have called this custom tag, or for ajax calls --->
				<cfoutput>
					#scriptTag#
				</cfoutput>
			</cfif>

		</cfif>
	</cffunction>


	<cffunction name="includeCSSOnce" access="public" output="true">
		<cfargument name="template" type="string" required="true"/>
	    <cfargument name="useHTMLHead" default="true" type="boolean"/>
		<cfargument name="checkIfExists" default="false" type="boolean"/>
		<cfargument name="includePortalcss" type="boolean" default="#not request.relayCurrentUser.isInternal and not listContainsNoCase(arguments.template,'code','/') and left(arguments.template,1) eq '/'?true:false#"/>
		<cfargument name="rel" type="string" default="stylesheet">

		<cfparam name="request.includeCssOnce" default = "#structNew()#">

		<cfif not structKeyExists(request.includeCssOnce, arguments.template)>

			<cfset  request.includeCssOnce[arguments.template] = "true">

			<cfset var cssToInclude = arguments.template>
			<cfif arguments.includePortalcss>
				<cfset arguments.checkIfExists = true>
				<cfset cssToInclude = listAppend(cssToInclude,"/code#arguments.template#")>
			</cfif>

			<cfloop list="#cssToInclude#" index="cssFile">
				<cfset var cssExists = true>

				<cfif arguments.checkIfExists>
					<cfset cssExists = fileExists(expandpath (cssFile))>
				</cfif>

				<cfif not arguments.checkIfExists or cssExists>
					<cfset var css = "">
					<cfsavecontent variable="css">
						<cfoutput>
							<link href="#cssFile#?v=#application.instance.relaywareversioninfo.svnCommit#" type="text/css" #application.com.structureFunctions.convertStructureToNameValuePairsForHTMLOutput(struct=arguments,includeKeysRegexp="rel|title")#/>
							<!--- <style type="text/css" media="screen,print">@import url("#cssFile#");</style> --->
						</cfoutput>
					</cfsavecontent>

					<!--- this is used when we are already in the head, and we want to output the script tag in the place that we have called this custom tag --->
					<cfif arguments.useHTMLHead and not application.com.request.isAjax()>
						<cfhtmlhead text="#css#">
					<cfelse>
						<cfoutput>
							#css#
						</cfoutput>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
	</cffunction>
</cfcomponent>