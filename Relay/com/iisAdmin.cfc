<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
IISAdmin.cfc
WAB
Code for interacting with IIS
Two main uses
	Getting paths to virtual directories  (this can be done on both IS6 and IIS7)
	Adding Settings such as 404 handler (this can only be done on IIS7)
	

2012-03-06	WAB Reinstated reading virtual directory information from IIS during boot up (serverInitialisation.cfc)
				Some changes done at this time
2012-03-27 WAB deal with sites bound using wildcard	
2012-04-25 WAB Fixes after released to an IIS6 site (Lexmark stg)
2012-05-16 WAB change references to http_host to server_name, because sometime the former has a port appended to it
2015-03-10 WAB	Changes to functions for inserting nodes into web.config
				Will now run off a piece of XML
				Added specific code for adding URL rewriting
				Use URL rewriting for dealing with missing templates, get rid of custom 404 httpError, but add passThrough of existing response
2015-03-17	WAB CASE 444176 URL rewriting was causing CF graphs to not display
2015-09-24 	RJT Added support for running on windows 8 (which seems to work) and allows RW to at least try to work on higher versions of windows				
2016-01-12	WAB Added new function, addVirtualDirectory(),  to add a virtual directory (rather than updating it).  setVirtualDirectories() will call it automatically if a set fails.  used by serverInitialisation.cfc
2016-04-19	WAB CASE 449163 mimeType Test needs to be case insensitive
2017-01-23 	WAB RT-150 MissingTemplte re-write rule altered (encode the Request_URI)
 --->

<cfcomponent>

	 <cfset system32Path = "c:\windows\system32\">
	 <cfset cscriptEXE = system32Path & "cscript.exe">
	 <cfset appcmdEXE = system32Path & "inetsrv\appcmd.exe">
	 <cfset regexpobject = createObject("component","com.regexp")>
	 <cfset xmlfunctions = createobject("component","com.xmlfunctions")>
	

	<cffunction name="getIISVersion" output="false">

		<cfscript>
			/*a regex would probably be better than this
			* 6 is our minimum supported version, things will start going
			* wrong from windows 60 (or if IIS change their naming scheme),
			* but thats an improvement over the previous limit of windows 7 -RJT
			*/
			for(var i=6;i<59;i++){
				if (cgi.server_software contains "Microsoft-IIS/" & i){
					return i;
				}
			}
			return 0;
		</cfscript>

		
	</cffunction>
	


	<cffunction name="getIISSetting">
		<cfargument name="section" default = "">
		<cfargument name="xpath" default = "">
		<cfargument name="Root" default = "false">
		<cfargument name="debug" default = "false">
		
		<cfset var result = "">
		<cfset var IISsiteName = "">
		<cfset var sectionXML = "">
		<cfset var appCMDResult = "">		
		
		<cfif not Root>
			<cfset IISsiteName = getCurrentIISSiteName()>
		</cfif>
		
		<cfset appCMDResult = runAppCMD (command="LIST",object="CONFIG",parameters=" #IISsiteName# /section:#section#",debug=debug)>

		<cfif appCMDResult.isOK>
			<cfset sectionXML = appCMDResult.xml>
			<cfif xpath is not "">
				<cfset result = xmlsearch(sectionXML,"#xpath#")>
			<cfelse >	
				<cfset result = xmlsearch (sectionXML,'//CONFIG/*')> 
			</cfif>
		<cfelse>
			<cfset result = appCMDResult.String>  <!--- actually don't have a way of returning an error, so just return the string returned from IIS.  Sorry bit lame --->
		</cfif>
		
		<cfreturn result>
		
	</cffunction>
	
	<cffunction name="upsertIISNode">
		<cfargument name="section" default = "">
		<cfargument name="attributes" default = ""> <!---  --->
		<cfargument name="uniqueKey" default = "">
		<cfargument name="collectionItem" default = "#iif(uniqueKey is not "",true,false)#">
		<cfargument name="pathToParent" default = "">
		<cfargument name="position" default = "-1" hint="0 based index">
		<cfargument name="applyToRoot" default = "false">
		<cfargument name="debug" default = "false">
		<cfargument name="update" default = "false">

		<!--- If we are adding an item to a collection we need to check whether it already exists --->
		<cfset var nodeExists = false>
		<cfset var attributesIdentical = false>
		<cfset var iisSiteName = "">
		<cfset var uniqueKeyPath = "">
		<cfset var uniqueKeyXPath = "">
		<cfset var commandString = "">
		<cfset var result = {isOK = true, results = [], message = "", inserted = false, updated = false}>
		<cfset var tempResult = "">
		<cfset var key = "">
		<cfset var existingCollection = "">

		<cfif collectionItem>
			<cfset existingCollection = getIISSetting (section = section)>
		</cfif>

		<cfif uniqueKey is not "">
			<cfset uniqueKeyPath = "[#uniquekey#='#attributes[uniqueKey]#']">
			<cfset uniqueKeyXPath = "//*" & pathToXpath(uniqueKeyPath)>
			<cfset var checkExistingNode = xmlSearch (existingCollection[1], uniqueKeyXPath)>
			<cfset nodeExists = arrayLen(checkExistingNode)>
			<cfif nodeExists>
				<!--- node exists, check for being identical--->
				<cfset attributesIdentical = application.com.structureFunctions.areStructuresIdentical (checkExistingNode[1].xmlattributes, attributes)>
			</cfif>
		</cfif>	

		<cfif not applyToRoot>
			<cfset IISsiteName = getCurrentIISSiteName()>
		</cfif>
		
		<cfif attributesIdentical>
			<cfset result.message = "Unchanged" >
		
		<cfelseif collectionItem and not nodeExists>
			<!--- Adding a collection item if it doesn't exist --->
			
			<cfif pathToParent is not "">
				<cfset commandString = pathToParent & ".">
			</cfif>

			<cfset var attributeString = "">
			<cfif arguments.position is not -1>
				<cfset local.position = min (arguments.position,arrayLen(existingCollection[1].xmlchildren))>
				<cfset attributeString = "@#local.position#">			
			</cfif>

			<cfloop collection = #attributes# item="key">
				<cfset attributeString = listAppend(attributeString,"#key#='#attributes[key]#'")>
			</cfloop>
			<cfset commandString &= "[" & attributeString & "]">
			
			<cfset tempResult = runAppCMD ( command="SET", object="CONFIG", parameters=' #IISsiteName# /section:#section# /+"#commandString#" ', xml=false, debug=debug)>

			<cfset arrayAppend(result.results, tempResult)>
			<cfset result.isOK = tempResult.isOK>
			<cfset result.inserted = true>
			<cfset result.message = "inserted">

		<cfelse>

			<cfloop collection = #attributes# item="key">
				<!--- update all attributes unless it is the unique key --->
				<cfif uniqueKey is "" or key is not uniqueKey>
					<cfset commandString = "">
	
					<cfif pathToParent is not "">
						<cfset commandString &= pathToParent & ".">
					</cfif>
	
					<cfif uniqueKeyPath is not "">
						<cfset commandString &= uniqueKeyPath & ".">
					</cfif>
					<cfset commandString = commandString  & "#key#:#attributes[key]#">
					<cfset tempResult = runAppCMD ( command="SET", object="CONFIG", parameters=' #IISsiteName# /section:#section# /"#commandString#" ', xml=false, debug=debug)>
					<cfset arrayAppend(result.results,tempResult)>
					<cfset result.isOK = result.isOK && tempResult.isOK>
					<cfset result.message = "updated">
					<cfset result.updated = true>
				</cfif>	
			</cfloop>


		</cfif>

		<cfreturn result>
	
	</cffunction>

	

	<cffunction name="clearIISCollection">
		<cfargument name="section" default = "">
		<cfargument name="pathToCollection" default = "">
		<cfargument name="applyToRoot" default = "false">
		<cfargument name="debug" default = "false">
		
		<cfset var commandString = "">
		<cfset var key = "">
		<cfset var result = arrayNew(1)>
		<cfset var thisresult = "">
		<cfset var IISsiteName = "">
		
		<cfif not applyToRoot>
			<cfset IISsiteName = getCurrentIISSiteName(host)>
		</cfif>

		<cfset var commandString = "/-">

		<cfif pathToCollection is not "">
			<cfset commandString &= "#pathToCollection#">
		</cfif>
						

		<cfset thisResult = runAppCMD (command="SET",object="CONFIG",parameters=' #IISsiteName# /section:#section# "#commandString#" ',xml=false,debug=debug)>
		<cfset arrayAppend(result,thisResult)>
		
		<cfreturn result>
		
	</cffunction>

	<!--- WAB 2014-09-23 Added this function for CASE 441814 
		For an example of how to use it see setUrlRewrite	()	
	--->
	
	<cffunction name="setIISNodeByXML">
		<cfargument name="xml" required> 			<!---  Some XML such as <rule name="api"><match url="*.cfm"/><action type="rewrite" url="/index.cfm" /></rule> --->
		<cfargument name="section" default = ""> 	<!--- section within the IIS configuration to update --->
		<cfargument name="collectionsAndKeys" default = {}>	<!--- the attribute of the parent node which uniquely defines it, such as 'name' in my example --->
		<cfargument name="position" default = "-1">
		<cfargument name="overwrite" default = "false">
		<cfargument name="debug" default = "false">

		<cfset var Result = {isOK = true, results = [], message = "", inserted = false, updated = false }>
		<cfset var childNodePath = "">
		<cfset var xpath = "">
		<cfset var childNode = "">
		<cfset var grandChildNode = "">
		<cfset var thisResult = "">

		<cfif not isXMLDoc (xml)>
			<cfset arguments.xml = xmlparse (xml)>
		</cfif>

		<!--- If we are adding an item to a collection we need to check whether it already exists --->
		<cfif structKeyExists(collectionsAndKeys,xml.xmlroot.xmlName)>
			<cfset var uniqueKey = collectionsAndKeys[xml.xmlroot.xmlName]>
			<cfset var path = "[#uniquekey#='#xml.xmlroot.xmlattributes[uniqueKey]#']">
			<cfset xPath = "/*" & pathToXpath (path)>
		</cfif>	
		
		<cfset var checkExisting = getIISSetting (section = section, xpath = xPath)>
		
		<cfif arrayLen(checkExisting) and not overwrite>
			<cfset result.isOK = true>
			<cfset result.message = "Already Exists">
		<cfelse>
			
			<cfset var upsertResult = upsertIISNode(section = section, uniquekey= uniqueKey, debug=debug, attributes = xml.xmlroot.xmlattributes, position = position)>						

			<cfset arrayAppend(result.results,upsertResult.results,true)>
			<cfset result.inserted = result.inserted || upsertResult.inserted>
			<cfset result.updated = result.updated || upsertResult.updated>
	
			<cfloop array="#xml.xmlroot.xmlchildren#" index="childNode">
				<cfif arrayLen(childNode.xmlChildren)>
					<!--- a collection, does it have a unique key, if not then we can't update individual items so would need to delete all items and add again --->
					<cfset var collectionUniqueKey = collectionsAndKeys[childNode.xmlName]>
					<cfif collectionUniqueKey is "">
						<!--- Delete Collection --->
						<cfset xPath = "//*" & replace(path,"[","[@","ALL") & "/" & childNode.xmlname>
						<cfset var checkExistingCollection = getIISSetting(section,xpath)>
						<cfif arrayLen(checkExistingCollection)>
							<cfset clearIISCollection (section = section, pathToCollection = path & "." & childNode.xmlname)>
						</cfif>
						
					</cfif>
					
					<cfloop array="#childNode.xmlchildren#" index="grandChildNode">
						<cfset upsertResult = upsertIISNode(section = section, pathToParent = path & "." & childNode.xmlname, collectionItem = true, debug=debug, attributes = grandChildNode.xmlattributes)>
						<cfset arrayAppend(result.results,upsertResult.results,true)>
						<!---  adding/changing children will always be recorded as an update --->
						<cfset result.updated = result.updated || upsertResult.updated || upsertResult.inserted>
					</cfloop>

				<cfelse>
					<cfset upsertResult = upsertIISNode(section = section, pathToParent = path & "." & childNode.xmlname, debug=debug, attributes = childNode.xmlattributes)>
					<cfset arrayAppend(result.results,upsertResult.results,true)>
					<!---  adding/changing children will always be recorded as an update --->
					<cfset result.updated = result.updated || upsertResult.updated || upsertResult.inserted>

				</cfif>
				
			</cfloop>
		
			<cfloop array="#result.results#" index="thisResult">
				<cfset result.isOK = result.isOK AND thisResult.isOK>				
			</cfloop>
		
		</cfif>
		
		<cfreturn result>
	
	</cffunction>
	
	<cffunction name="pathToXpath">
		<cfargument name="path" required="true">
		<cfreturn replace(path,"[","[@","ALL")>
	</cffunction>
	

	<cffunction name="runAppCMD">
		<cfargument name="command" default = "" > <!--- LIST, sET --->
		<cfargument name="object" default = "" ><!--- SITE,APP, CONFIG etc --->
		<cfargument name="parameters" default = "" >
		<cfargument name="debug" default = "false" >
		
		<cfargument name="XML" default = "true" >
	
		<cfset var result = {isOK=true,command="",result=""}>
		<!--- command to be run --->
		<cfset var query = "#command# #object# #parameters# ">
		<cfif xml>	
			<cfset query = query  & " /xml">
		</cfif>
		
		<cfset result.query = query>
		<cfif debug>
			<cfoutput>#appcmdEXE# #query#</cfoutput>
		</cfif>
		<!--- execute command --->
		<cfexecute name="#appcmdEXE#" arguments="#query#" timeout="5" variable="result.String" />
	
		
		<cfif xml and isXML (result.String)>
			<cfset result.xml = xmlparse (result.String, false)>
		<cfelseif result.String contains "ERROR">  <!--- A slightly lame test! --->
			<cfset result.isOK = false>
		</cfif>
	
		<cfreturn result>
	</cffunction>


	<cffunction name="readWebConfig">
		<cfset var path = "#application.paths.relay#/web.config">
		<cfset var content = "">	
		<cfif fileExists (path)>
			<cffile action="read" file="#path#" variable="content">
		<cfelse>
			<cfset content = "<configuration></configuration>">
		</cfif>	
		<cfreturn xmlParse (content)>
	</cffunction>


	<cffunction name="writeWebConfig">
		<cfargument name="XML">

		<cfset var content = xmlfunctions.XML2XMLString(XML)>
		<cfset var path = "#application.paths.relay#/web.config">

		<cffile action="write" file="#path#" output="#content#" charset="UTF-8">

		<cfreturn true>
	</cffunction>


	<cffunction name="get404NodeArray">
		
		<cfset var result = getIISSetting('httpErrors',"//*[@statusCode='404']")>
		<cfreturn result>

	</cffunction>


	<cffunction name="MissingTemplateHandlerNode">
		<cfset var result = {statusCode= 404,path = "/errorHandler/IISMissingTemplateHandler.cfm",responseMode='ExecuteURL'}> <!--- ,existingResponse="PassThrough" --->
		<cfreturn result>
	</cffunction>


	<!--- 	WAB 2015-03-17 CASE 444176 URL rewriting was causing CF graphs to not display 
			Add an exclusion for files with paths starting /CFIDE 		
			CF uses some 'virtual' filenames (such as /CFIDE/graphData.cfm) which do not actually exist but should not be processed as missing templates
			I'm not going to worry about logging actual missing files in the /CFIDE directory
			WAB 2017-01-23 RT-150 rewrite fails if filename contains ampersands etc.  Need to URL encode the Request_URI
	--->

	<cffunction name="getMissingTemplateRewriteRuleXML">
		<cfargument name="secure" type="numeric" required="true" hint="0 or 1">
		
		<cfset var protocol = "HTTP" & iif(secure,de("S"),de(""))>
		<cfset var xmlString = "">

		<cfsavecontent variable="XMLString">
			<cfoutput>
				<rule name="404 #protocol#" enabled="true" stopProcessing="true">
					<match url="(.*)" />
					<conditions>
						<add input="{REQUEST_URI}" negate="true" pattern="(CFFileServlet|CFIDE)/(.*)" />
						<add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
						<add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
						<add input="{SERVER_PORT_SECURE}" pattern="#secure#" />
					</conditions>
					<serverVariables/> <!--- Not actually used, but if not here my update routines always think that rule needs updating --->
					<action type="Rewrite" url="/errorHandler/IISMissingTemplateHandler.cfm?404;#protocol#://{HTTP_HOST}:{SERVER_PORT}{UrlEncode:{REQUEST_URI}}" />
				</rule>
			</cfoutput>	
		</cfsavecontent>

		<!---  This line just removes the tabs from the beginning of the lines, makes it look nicer if outputted as a string, will fail of course if the code is reformatted! --->
		<cfset xmlString = reReplace (xmlString,"(\n)\t{4}","\1","ALL")>

		<cfset var result = xmlParse (XMLString)>
		
		<cfreturn result>
	</cffunction>
				
		
	<cffunction name="isMissingTemplateHandlerSetUp">
		<cfreturn checkAndSetUpMissingTemplateHandler (update = false)>
	</cffunction>


	<cffunction name="setUpMissingTemplateHandler">
		<cfreturn checkAndSetUpMissingTemplateHandler (update = true)>
	</cffunction>


	<cffunction name="checkAndSetUpMissingTemplateHandler">
		<cfargument name="update" default="false">

		<cfset var result = {isOK = true,message=""}>
		

		<cfif getIISVersion() LT 7>
			<cfset result.message = "This test can only be done on IIS 7">
			<cfreturn result>
		</cfif>


		<!--- check for new style rewrite nodes, both https and http 
			Postion is 1 (which should be after the API rewrite)
		--->
		<cfset var secure = 0>
		<cfloop index="secure" list = "0,1">
			<cfset var rewriteNode = getMissingTemplateRewriteRuleXML (secure = secure)>
			<cfset var tempResult = checkAndSetupURLRewriting(rewriteNode = rewriteNode, position = 1, update = update)>
			<cfset result.message &= tempResult.message & "<BR>">
			<cfset result.isOK = tempResult.isOK && result.isOK>
		</cfloop>


		<!--- check for existence of old style httpError node which needs to be removed --->
		<cfset var array404 = get404NodeArray()>

		<cfif arrayLen(array404) and structKeyExists (array404[1].xmlattributes,"responseMode") and array404[1].xmlattributes.responseMode is "ExecuteURL">
			<cfif update>

				<cfset runAppCMD (command="SET",object="CONFIG",parameters=" #getCurrentIISSiteName()# -section:system.webServer/httpErrors /-[statusCode='404'] ",xml=false)>
				<cfset result.message &= 'httpErrors: Error node statusCode="404" removed<br />'>					
				<!--- now search again because the <remove> node will not have been updated, but this should happen in the next section --->
				<cfset array404 = get404NodeArray()>
			<cfelse>
				<cfset result.isOK= false>
				<cfset result.message &= 'httpErrors: Error node statusCode="404" to be removed <br />'>					
			</cfif>
		</cfif>

		<cfif not arrayLen(array404)>
			<cfset var webConfigXML = readWebConfig ()>
			<cfset var searchForRemoveNode = xmlSearch (webConfigXML,"//httpErrors/remove[@statusCode='404']")>

			<cfif arrayLen(searchForRemoveNode)>
				<cfif update>
					<!--- no way of deleting this node with AppCmd, resort to tampering with XML --->
					<cfset xmlfunctions.deleteNode(searchForRemoveNode[1])>
					<cfset writeWebConfig (webConfigXML)>
					<cfset result.message &= 'httpErrors: Remove Node statusCode="404" removed<br />'>
				<cfelse>
					<cfset result.isOK= false>
					<cfset result.message &= 'httpErrors: Remove Node statusCode="404" to be removed<br />'>
				</cfif>
			</cfif>
			<!--- OK --->
		</cfif>
		

		<!--- Check that passThrough is set on httpErrors --->
		<cfset var checkForExistence = getIISSetting(section = 'httpErrors',xpath = "//*[@existingResponse='PassThrough']")>
		<cfif not arrayLen(checkForExistence)>
			<cfif update>
				<cfset upsertIISNode(section="httpErrors",attributes={existingresponse="PassThrough"})>
				<cfset result.message &= 'existingResponse=PassThrough added to httpErrors <br />'>								
			<cfelse>
				<cfset result.isOK= false>
				<cfset result.message &= 'httpErrors needs existingResponse=PassThrough<br />'>					
			</cfif>
		<cfelse>
			<cfset result.message &= 'httpErrors.existingResponse=PassThrough correct<br />'>							
		
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="checkAndSetupURLRewriting">
		<cfargument name="rewriteNode" required="true">
		<cfargument name="position" default="-1">
		<cfargument name="update" default="false">

		<cfset var checkForExistence = getIISSetting ( section = 'rewrite/rules', xpath = "//*[@name='#rewriteNode.xmlroot.xmlattributes.name#']" )>
		<cfset var identical = true>
		<cfset var result = {}>

		<cfif arrayLen (checkForExistence)>
			<!--- check XML Identical --->
			<cfset var checkIdentical = xmlFunctions.areXMLNodesIdentical (rewriteNode.xmlroot,checkForExistence[1])>
			<cfset identical = checkIdentical.identical>
		</cfif>

		<cfif not arrayLen (checkForExistence) or not identical>
			<cfif update>
				<cfset var upsertResult = setIISNodeByXML(
														xml = rewriteNode,
														section = "rewrite/rules",
														collectionsAndKeys = {
															rule = "name",
															conditions="" 
														},
														position = position,	
														overwrite = true
				)>
				<cfset result.isOK = upsertResult.isOK>
				<cfset result.message = 'Rewrite rule: #rewriteNode.xmlroot.xmlattributes.name# #iif( upsertResult.inserted, de("inserted"), de(iif( upsertResult.updated, de("Updated"), de("OK") ) ) )#'>

			<cfelse>	
				<cfif not identical>
					<cfset result.message = "Rewrite rule: #rewriteNode.xmlroot.xmlattributes.name# exists but is not correct.  #checkIdentical.message#<br />">
				<cfelse>
					<cfset result.message = "Rewrite rule: #rewriteNode.xmlroot.xmlattributes.name# does not exist<br />">
				</cfif>
				<cfset result.isOK = false>
					
			</cfif>
		<cfelse>
			<cfset result.isOK = true>
			<cfset result.message = "Rewrite rule: #rewriteNode.xmlroot.xmlattributes.name# correct">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getVirtualDirectoriesXML" returns="struct">
		<cfset var executeResultString = "">

		<cfreturn runAppCMD (command="LIST",object="VDIR")>	
	
	</cffunction>


	<cffunction name="getVirtualDirectoriesForThisSite" returns="struct">
		<cfreturn getVirtualDirectoryXMLForSite(getCurrentIISSiteName())>
	</cffunction>


	<cffunction name="getVirtualDirectoryXMLForSite" returns="struct">
		<cfargument name="siteName" default = "#getCurrentIISSiteName()#">
		
		<cfset var xml = getVirtualDirectoriesXML().xml>
		<cfset var result = "">
		<cfset var iisSiteName = "">
				
		<cfset result = xmlsearch (xml,"//VDIR[@APP.NAME='#siteName#/']")>
		
		<cfreturn result>
	</cffunction>


	<cffunction name="getVirtualDirectoryStructureForSite" returns="struct">
		<cfargument name="siteName" default = "#getCurrentIISSiteName()#">
		<cfset var result = {isok=true,vdirs=structNew()}>
		
		<cfif getIISVersion() GTE 7>
			<cfset var XML = getVirtualDirectoryXMLForSite(sitename)>
			<cfset var dir = "">
			<cfloop array="#xml#" index="dir">
				<cfset result.vdirs[dir.xmlattributes.path] = dir.xmlattributes.physicalPath>
			</cfloop>
			
		<CFELSE>
			<cfset result.vdirs = getIISSiteVirtualDirectoriesIIS6(siteName)>
			<!--- IIS6 does not report the root directory as one the virtual directories, so we will have to add it manually - rather relies on the name and location of this CFC --->
			<cfset result.vdirs["/"] = replaceNoCase(getmetadata(this).path,"\com\iisadmin.cfc","")>			

		</cfif>

		
		<cfreturn result>
	</cffunction>


	<cffunction name="setVirtualDirectories" returns="struct">
		<cfargument name="vdir">
		<cfargument name="physicalPath">
		
		<cfset var result = "">
		<cfset var thisArg ="">
		<cfset var parameters = " #getCurrentIISSiteName()#/#vdir# ">
		<cfloop collection="#arguments#" item="thisArg">
			<cfif thisArg is not "vdir">
				<cfset parameters = parameters & ' -#thisArg#:"#arguments[thisARG]#" '>
			</cfif>
		</cfloop>

		<cfset result = runAppCMD (command="SET",object="VDIR",parameters =parameters,xml=false)>

		<!--- rather a nasty way of doing upsert, shoudl really check for existence --->
		<cfif not result.isOK and result.string contains "Must use exact identifer">
			<cfset result = addVirtualDirectory (argumentCollection = arguments)>
		</cfif>
		
		<cfreturn result>	
	
	</cffunction>


	<cffunction name="addVirtualDirectory" returns="struct">
		<cfargument name="vdir">
		<cfargument name="physicalPath">
		
		<cfset var result = "">
		<cfset var thisArg ="">
		<cfset var parameters = "">
		<cfset var parametersStruct = {"app.name" = getCurrentIISSiteName() & "/",
								"path" = "/" & vdir,
								"physicalPath" = physicalPath
									
					}>

		<cfloop collection="#parametersStruct#" item="thisArg">
			<cfset parameters &= ' /#thisArg#:"#parametersStruct[thisARG]#" '>
		</cfloop>

		<cfset result = runAppCMD (command="ADD",object="VDIR",parameters =parameters,xml=false)>

		<cfreturn result>	
	
	</cffunction>


	<cffunction name="getrequestFiltering">
	
		<cfset var result = getIISSetting(section = 'requestFiltering',debug=true)>
		
		<cfreturn result>

	</cffunction>

	<cffunction name="getMaxFileSizeXML">
	
		<cfset var result = getIISSetting(section = 'requestFiltering',xpath = "//requestLimits",debug=false)>
		
		<cfreturn result>

	</cffunction>

	<cffunction name="getMaxFileSize">

		<cfset var result = 4194304> <!--- IIS 6 default upload file size?? --->
		<cfset var XMLArray = getMaxFileSizeXML()>
		
		<cfif getIISVersion() is not 6>
			<cfset result = 30000000> <!--- IIS 7 default upload file size?? --->
		</cfif>
		
		<cfif arrayLen(XMLArray) and structKeyExists(XMLArray[1].xmlattributes,"maxAllowedContentLength")>
			<cfset result = XMLArray[1].xmlattributes.maxAllowedContentLength>
		</cfif>

		<cfreturn result>	
	</cffunction>


	<cffunction name="setMaxFileSize">
		<cfargument name="size">

		<cfset var result = "">
		<cfset var attributes = {}>
		<cfset attributes["requestLimits.maxAllowedContentLength"] = size>

		<cfset result = upsertIISNode(section = 'requestFiltering',attributes=attributes,applyToRoot=true,debug=false)>
		
		<cfreturn result>

	</cffunction>


	<cffunction name="setMimeType">
		<cfargument name="fileextension">
		<cfargument name="mimeType">

		<cfset var result = "">

		<cfif left(fileextension,1) is not ".">
			<cfset arguments.fileextension = "." & arguments.fileextension>
		</cfif>
		<cfset arguments.fileextension = lcase(arguments.fileextension)>
		<cfset arguments.mimeType = lcase(arguments.mimeType)>

		<cfset result = upsertIISNode(section = 'staticContent', collectionItem=true, uniqueKey = "fileExtension", attributes=arguments, applyToRoot = false,debug=true)>
		
		<cfreturn result>

	</cffunction>

	<cffunction name="getMimeType">
		<cfargument name="fileextension">
		
		<cfset var result = "">

		<cfif left(fileextension,1) is not ".">
			<cfset arguments.fileextension = "." & arguments.fileextension>
		</cfif>

		<cfset result = getIISSetting(section = 'staticContent',xpath = "//mimeMap[translate(@fileExtension,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(fileextension)#']",debug=false)>
		
		<cfreturn result>

	</cffunction>


	<cffunction name="getMimeTypes">
	
		<cfset var result = "">

		<cfset result = getIISSetting(section = 'staticContent',debug=false)>
		
		<cfreturn result>

	</cffunction>


	<cffunction name="getSitesAndBindingsXML">
		<cfset var executeResultString = "">

		<cfreturn runAppCMD (command="LIST",object="SITE",parameters="/state:started").xml>	
			
	</cffunction>
	 

	<cffunction name="getIISSiteName">
		<cfargument name="host" required="true">
		<cfargument name="port" required="true">
		<cfif getIISVersion() GTE 7>
			<cfreturn getIISSiteNameIIS7(host=host,port=port)>
		<cfelse>
			<cfreturn getIISSiteNameIIS6(host=host,port=port)>
		</cfif>

	</cffunction>


	<cffunction name="getCurrentIISSiteName">

		<cfset var result = getIISSiteName(host = cgi.server_name,port = cgi.server_port)>
		<cfif result is "">
			<cfset result = getIISSiteName(host = CreateObject("java", "java.net.InetAddress").getLocalHost().getHostAddress(),port = cgi.server_port)>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="getIISSiteNameIIS7" access="private">
		<cfargument name="host" required="true">
		<cfargument name="port" required="true">

		<cfset var result ="">
		<cfset var xml = getSitesAndBindingsXML()>
		
		<!--- We may have to do a few similar xpaths with this complicated case translation, so I am building that bit once and will just replace the SEARCHSTRING --->
		<cfset var GenericxPath = "//SITE[contains(translate(@bindings,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'SEARCHSTRING')] ">
		
		<!--- first search is for the hostname as a hostheader (with port infront) --->
		<cfset var xPath = replace(GenericxPath,"SEARCHSTRING","#port#:#lcase(host)#")>
		<cfset var searchresult =xmlsearch(XML,xpath)> 
		<cfif not arrayLen(searchResult)>
			
			<!--- If HostHeader name is not found then look for IPAddress (with port following) --->
			<cfset var IPAddress = CreateObject("java", "java.net.InetAddress").getByName(host).getHostAddress()>
			<cfset xPath = replace(GenericxPath,"SEARCHSTRING","#IPAddress#:#port#")>

		<cfset searchresult =xmlsearch(XML,xpath)> 
			<cfif not arrayLen(searchResult)>
				<!--- IPAddress not found, look for * (with port following) --->
				<cfset xPath = replace(GenericxPath,"SEARCHSTRING","*:#port#")>

				<cfset searchresult =xmlsearch(XML,xpath)> 
			</cfif>

		
		</cfif>
		
		<cfif arrayLen (searchresult)>
			<cfset result = searchresult[1].xmlattributes['site.name']>
		</cfif>
		
		<cfreturn result>
		
	</cffunction>


	<cffunction name="getIISSiteNameIIS6">
		<cfargument name="host" default = "#cgi.server_name#">		
		<cfargument name="port" required="true">		

			<cfset var sitesAndBinding = getAllIISSitesAndBindings()>
			<cfset var IISSite = "">

			<cfquery name = "IISSite" dbtype="query">
			select distinct servername from sitesAndBinding
			where lower(host) = '#lcase(host)#' and port = #port#
			</cfquery>
		<cfreturn IISSite.serverName>
	
	</cffunction>


	<cffunction name="getAllIISSitesAndBindings">
		
		<cfset var iisVersion=getIISVersion()>

		<cfif iisVersion GTE 7>
			<cfreturn getAllIISSitesAndBindingsIIS7()>
		<cfelseif iisVersion EQ 6>
			<cfreturn getAllIISSitesAndBindingsIIS6()>
		<cfelse>
			<cfoutput>IIS Server Version Not Supported</cfoutput>
			<CFABORT> <!--- do not change to CF_ABORT custom tags path not necessarily set at this point --->
		</cfif>

	</cffunction>


	<cffunction name="getAllIISSitesAndBindingsIIS7">
		<cfargument name="states" default="started">
		
		<cfset var query = "">
		<cfset var executeResultString = "">
		<cfset var result = queryNew("siteName,serverID,state,ipaddress,port,hostname")>
		<cfset var array = "">
		<cfset var i  = "">
		<cfset var regExp  = "">
		<cfset var regExpFind  = "">
		<cfset var serverName = "">
		<cfset var bindings = "">
		<cfset var state= "">
		<cfset var row = "">
		
	
		<cfset var siteXML = getSitesAndBindingsXML()>

				<!--- loop over rows, parsing --->
				<cfloop index="i" from = "1" to = "#arrayLen(siteXML.xmlroot.xmlchildren)#">
						<cfset var node = siteXML.xmlroot.xmlchildren[i]>
						<cfset state = node.xmlattributes.state>
						<cfif listFindNoCase(arguments.states,state)>
							<!--- now find each binding from a string looking like this
								http/*:80:dev6-cfint,http/*:80:dev6-cfext
							--->						
							<cfset servername= node.xmlattributes["site.name"]>
							<cfset bindings = node.xmlattributes.bindings>
							
							<cfset regexp = '/(.*?):(.*?):(.*?)(,|\Z)'>
							<cfset var groupNames = {1="ipaddress",2="port",3="hostname",4="notused"}>
							<cfset regExpFind = regexpobject.refindalloccurrences(regexp,bindings & ",",groupnames)>  <!--- comma is a hack 'csue couldn't get my regexp to see end of string with \Z! --->
							<cfloop index="i" from = "1" to = "#arrayLen(regExpFind)#">
								<cfset queryAddRow(result)>
								<cfset querySetCell (result,"siteName",servername,result.recordcount)>
								<cfset querySetCell (result,"serverID","?",result.recordcount)>
								<cfset querySetCell (result,"state",state,result.recordcount)>
	
								<cfset querySetCell (result,"ipaddress",regExpFind[i].ipaddress,result.recordcount)>
								<cfset querySetCell (result,"port",regExpFind[i].port,result.recordcount)>
								<cfset querySetCell (result,"hostname",regExpFind[i].hostname,result.recordcount)>
							</cfloop>

						</cfif>
											
				</cfloop>
	
		<cfreturn result> 
	
	</cffunction>


	<cffunction name="getIISSiteVirtualDirectoriesIIS7" returns="struct">
		<cfargument name = "serverName" default="#getIISSiteName()#">	
	
		<cfset var executeResultString = "">
		<cfset var result = structNew()>
		<cfset var i  = "">
		<cfset var regExp  = "">
		<cfset var array = "">
		<cfset var regExpFind  = "">
		<cfset var row = "">
		
		<!--- command to be run --->
		<cfset var query = 'LIST VDIR /app.name:"#servername#/"'>

		<!--- execute command --->
		<cfexecute name="#appcmdEXE#" arguments="#query#" timeout="30" variable="executeResultString" />

							<!--- 
						VDIR "cfdevWeb2/" (physicalPath:d:\web2\relayv9)
						VDIR "cfdevWeb2/content" (physicalPath:d:\WEB2\ATIRelayUserFiles\Content)							
							--->

		<cfset array = listtoarray(executeResultString,chr(10))>
					<cfset regexp = 'VDIR "#servername#(.*?)" \(physicalPath\:(.*?)\)'>
	
				<!--- loop over rows, parsing --->
				<cfloop index="i" from = "1" to = "#arrayLen(array)#">
					<cfset row = array[i]>
					<cfset regExpFind = regexpobject.refindalloccurrences(regexp,row)>

					<cfif arrayLen(regExpFind)>
						<cfset result[regExpFind[1][2]] = regExpFind[1][3]>
					</cfif>
				</cfloop>
		<cfreturn 	result>	
		
	</cffunction>


	<cffunction name="getIISSiteVirtualDirectoriesIIS6" returns="struct">
		<cfargument name = "serverName" default="#getIISSiteName()#">	
	
		<cfset var query = "">
		<cfset var executeResultString = "">
		<cfset var result = structNew()>
		<cfset var array = "">
		<cfset var row = "">
		<cfset var regexp = "">
		<cfset var groupNames= "">
		<cfset var regexpfind= "">
		<cfset var siterow = "">
		<cfset var i = "">
									
		<!--- command to be run --->
		<cfset query = system32Path & "iisvdir.vbs /query #servername#">

		<!--- execute command --->
		<cfexecute name="#cscriptEXE#" arguments="#query#" timeout="30" variable="executeResultString" />

							<!--- 
							Microsoft (R) Windows Script Host Version 5.6
							Copyright (C) Microsoft Corporation 1996-2001. All rights reserved.
							
							Connecting to server ...Done.
							Alias                              Physical Root
							==============================================================================
							/content                           D:\web2\Relaywareuserfiles
							
							--->


		<cfset executeResultString = removeGuffFromResult (executeResultString)>  

		<cfset array = listtoarray(executeResultString,chr(10))>
	
				<!--- loop over rows, parsing --->
				<cfloop index="i" from = "1" to = "#arrayLen(array)#">
					<cfset row = array[i]>
					<cfset regexp = "([^\s]*)\s*([^\s]*)">
					<cfset groupNames = {1="dir",2="path"}	>	
					<cfset regExpFind = regexpobject.refindalloccurrences(regexp,row,groupnames)>
					<cfif arrayLen(regExpFind)>
						<cfset result[regExpFind[1].dir] = regExpFind[1].path>
					</cfif>
				</cfloop>
					
		<cfreturn 	result>	
		
	</cffunction>
		
	
	<cffunction name="getAllIISSitesAndBindingsIIS6">
		<cfargument name="states" default="started">
		
		<cfset var query = "">
		<cfset var executeResultString = "">
		<cfset var result = queryNew("siteRow,serverName,serverID,state,ipaddress,port,host")>
		<cfset var array = "">
		<cfset var row = "">
		<cfset var regexp = "">
		<cfset var regexpfindArray= "">
		<cfset var regexpfound= "">
		<cfset var siterow = "">
		<cfset var i = "">
		<cfset var state = "">
				
		<!--- command to be run --->
		<cfset query = system32Path & "iisweb.vbs /query">

		<!--- execute command --->
		<cfexecute name="#cscriptEXE#" arguments="#query#" timeout="30" variable="executeResultString" />
	
							<!---
							This script brings back a result like this

							Microsoft (R) Windows Script Host Version 5.6
							Copyright (C) Microsoft Corporation 1996-2001. All rights reserved.
							Connecting to server ...Done.
							Site Name (Metabase Path)                     Status  IP              Port  Host
							==============================================================================
							Default Web Site (W3SVC/1)                    STARTED ALL             80    N/A
							Rndcfdev (W3SVC/712725001)                    STARTED ALL             80    RnD-cfint
							                                                      ALL             80    RnD-cfext
							rnd2cfdev (W3SVC/809162139)                   STARTED ALL             80    rnd2-cfint
							                                                      ALL             80    rnd3-cfint
							--->
	
		<!--- remove all the guff at beginning, ends with === newline --->
		<cfset executeResultString = removeGuffFromResult (executeResultString)>  
		<!--- Convert to array of lines --->
		<cfset array = listtoarray(executeResultString,chr(10))>
	
				<!--- loop over rows, parsing --->
				<cfloop index="i" from = "1" to = "#arrayLen(array)#">
					<cfset row = array[i]>
					<!--- this looks for the row which has the site name --->
					<cfset var groupNames = {1="servername",2="serverid",3="state",4="ipaddress",5="port",6="host"}>
					<cfset regexp = "(.*?)\((.*?)\)\s*([^\s]*)\s*([^\s]*)\s*([^\s]*)\s*([^\s]*)">
					<cfset regExpFindArray = regexpobject.refindalloccurrences(regexp,row,groupnames)>
					<cfif arrayLen(regExpFindArray)>
						<cfset siteRow = true>
					<cfelse>
						<cfset siteRow = false>
						<!--- not a row with site name, so look for row  without--->
						<cfset regexp = "( )( )( )\s*([^\s]*)\s*([^\s]*)\s*([^\s]*)">
						<cfset regExpFindArray = regexpobject.refindalloccurrences(regexp,row,groupnames)>
					</cfif>
					
			
					<cfif arrayLen(regExpFindArray)>
						<cfset regexpfound = regExpFindArray[1]>
						<cfif siteRow>
							<cfset state = regExpFindArray[1].state>
						<cfelse>
							<cfset state = result["state"][result.recordcount]> <!--- we get the state from the previous row --->
						</cfif>

						<cfif listfindNocase(states,state)>
						<cfset queryAddRow(result)>
						<cfif siteRow>
							<cfset querySetCell (result,"siteRow","1",result.recordcount)>
								<cfset querySetCell (result,"serverName",regexpfound.servername,result.recordcount)>
								<cfset querySetCell (result,"serverID",regexpfound.serverid,result.recordcount)>
								<cfset querySetCell (result,"state",state,result.recordcount)>

						<cfelse>
							<cfset querySetCell (result,"siteRow","0",result.recordcount)>
							<cfset querySetCell (result,"serverName",result["serverName"][result.recordcount-1],result.recordcount)>
							<cfset querySetCell (result,"serverID",result["serverID"][result.recordcount-1],result.recordcount)>
								<cfset querySetCell (result,"state",state,result.recordcount)>
							
						</cfif>
							<cfset querySetCell (result,"ipaddress",regexpfound.ipaddress,result.recordcount)>
							<cfset querySetCell (result,"port",regexpfound.port,result.recordcount)>
							<cfset querySetCell (result,"host",regexpfound.host,result.recordcount)>
						</cfif>
					</cfif>
				
				</cfloop>
	
		<cfreturn result> 
	
	</cffunction>


	<cffunction name="removeGuffFromResult" access="private">
		<cfargument name="string">
		<!--- remove all the guff at beginning, ends with === newline --->		
		<cfreturn rereplace (string,"\A.*====\r","")>  
	</cffunction>



</cfcomponent>