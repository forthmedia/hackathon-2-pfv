<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			FundApprovalStatus.cfc
Author:				AJC
Date started:		2011-03-15
	
Description:		For the MDF section. Contains
functions related to Fund Approval Status.	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

 --->
<cfcomponent>

	<cffunction access="public" name="getFundApprovalStatus" hint="Returns Fund Approval Status information">
		<cfargument name="sortOrder" type="string" default="sortOrder">
		
		<cfscript>
			var qry_get_FundApprovalStatus = "";
			var statusTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "FundApprovalStatus", phraseTextID = "title");
		</cfscript>
		
		<cfquery name="qry_get_FundApprovalStatus" datasource="#application.siteDataSource#">
			select FundApprovalStatusID
			,#statusTitlePhraseQuerySnippets.select# as FundApprovalStatus
			,FundStatusTextID
			,active
			,sortorder
			from FundApprovalStatus
				#preservesinglequotes(statusTitlePhraseQuerySnippets.join)#
			order by #arguments.sortOrder#
		</cfquery>
		
		<cfreturn qry_get_FundApprovalStatus>
	</cffunction>
	
	<cffunction access="public" name="getFundApprovalStatusForDropDown" hint="Returns lenovo Co-Funders for populating a dropdown">
		<cfscript>
			var qry_get_FundApprovalStatus = "";
			var FundApprovalStatusPhraseQuerySnippets = "";
		</cfscript>
		<cfset FundApprovalStatusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "FundApprovalStatus", phraseTextID = "title")>
		<cfquery name="qry_get_FundApprovalStatus" datasource="#application.siteDataSource#">
			select FundApprovalStatusID as dataValue ,<!--- 'title_lenovoFundCoFundingEntity_' + convert (varchar, lenovoFundCoFundingEntityID) ---> #preservesinglequotes(FundApprovalStatusPhraseQuerySnippets.select)# as displayValue
			 from FundApprovalStatus
			 #preservesinglequotes(FundApprovalStatusPhraseQuerySnippets.join)# 
			 where active = 1 
			 order by sortorder
		</cfquery>
		
		<cfreturn qry_get_FundApprovalStatus>
	</cffunction>

</cfcomponent>
