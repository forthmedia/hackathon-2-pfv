<!--- 
File name:			serverInformation.cfc
Author:				WAB			
Date started:			2010/05
	
Description:			
	The code was created to get rid of the awful setSiteDetails file

	Idea is that when a CF Instance Starts We	
	a) 	Interogate IIS for IIS Sites
	b) 	Map each http_host to an IIS Server
	c) 	Find the virtual directories for each IISSite
	d) 	Read An XML file in the userfiles directory (/XML/datasource.xml) which defines the datasource
	e) 	POP all the info into some structures

	On each request we lookup the HTTP_HOST in the structures and are able to return details of all mappings, datasource etc.

	The first time a request comes for a specific HTTP_HOST we check that that datasource has the http_host set up in the sitedef (this gives us the value of testSite as well)	
	If the http_host is not in the sitedef then it can be added interactively	

	Will run on IIS7, IIS6 (may need testing 2010-06-30) 
	
	On Remote Cluster Instances we have to use a different technique because we don't have IIS on the remote Box
	(Infact this method could have been used throughout, but the IIS one came first!)
	We have to work out the virtual directories by making a request to a special file which we know exists in each virtual directory and which returns details of where it is located (using getTemplatePath)
	(This turned out to be unreliable.  So we resorted to just interrogating IIS - Turned out that cluster slaves generally have a copy of IIS anyway)


	TO BE DONE
	Take a look at the second initialise() in getSiteForThisDomain - probably needs done better


	
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-03-06	WAB		Did some major backtracking and reverted to using IIS technique to get virtual directories.  
					This was after an outage on Lenovo when servers just couldn't get to initialise.cfc
					In reality, even in coldfusion muliti machine clusters we always have IIS set up correctly even if not being used.  So IIS technique is OK
2014-01-07	WAB		Correct problem with the "/Test" mapping (was just "test")
2015-05-06	WAB		Added CFQUERYPARAM to query to prevent injection (noticed error during Acunetix scan)
2016-01-12 	WAB 	Add support for Unit Testing virtual directories
2016-01-22	WAB 	Add wirebox to the system
2016-01-22	WAB 	Add a mapping to an examples directory
2017-03-08 	WAB		PROD2016-3543 Prevent unknown host exception triggering throwing a full blown error (which ends up with an email being sent, because it occurs before the error handling even exists)
Possible enhancements:



 --->

<cfcomponent cache="false">


	 <cfset regexpobject = createObject("component","com.regexp")>
	 <cfset xmlfunctions = createobject("component","com.xmlfunctions")>
	 <cfset clusterManagementObj = createObject ("component","com.clusterManagement").initialise()>
	
	<cffunction name="initialise" returns="string">

			<!--- Need Locking --->
			<cfset var vdirs ="">
			<cfset var distinctIISSites ="">
			<cfset var mappingsEtc = "">
			<cfset this.structures = {}>
			<cfset this.structures.relayApplicationsStruct = {}>
			<cfset this.structures.hostNameToRelayApplicationsStruct = {}>
			<cfset this.dateLoaded = now()>
			
<!--- Note.
	In theory we could do without the IIS technique and use the initialise.cfc technique throughout
	In which case we would comment out the rest of this initialisation function
--->	

	</cffunction >
	

	<cffunction name="getSiteForThisDomain" returns="string">

		<cfset var thisIISSite = "">	
		<cfset var vdirs = structNew()>	
		<cfset var dirs = "">	
		<cfset var dir = "">	
		<cfset var mappingsEtc = "">	
		<cfset var tempSiteName = "">	
		<cfset var theURL = "">	
		<cfset var theResult = "">	
		<cfset var check = "">	
		<cfset var registersite= "">	
		
		<cfif not structKeyExists(this.structures,"relayApplicationsStruct")>
			<cfset initialise ()>
		</cfif>

		<cfif not structKeyExists (this.structures.hostNameToRelayApplicationsStruct,CGI.SERVER_NAME)>
				<!--- this host hasn't been put into the structure yet, so we need to find out about it--->
				<!--- gets details of the virtual directories--->
				<cftry>
					<cfset var getvdirs = getVirtualDirectories ()>
					<!--- PROD2016-3543 WAB Add a catch here in the case of an unknown host, prevents server support being spammed with error emails --->
					<cfcatch>
						<cfif cfcatch.type is "java.net.UnknownHostException">
							<cfoutput>Unknown Host: #cfcatch.message#<br />You may need to add it to your server hosts file</cfoutput>
							<cfabort>
						</cfif>
						<cfrethrow>
					</cfcatch>
				</cftry>	
				<cfif getvdirs.isOK>
					<cfset vdirs = getvdirs.vdirs>
				<cfelse>
					<cfoutput>#getvdirs.message#</cfoutput><cfabort>
				</cfif>
				
				<!--- work out all the mappings and the datasource, get value of testSite for all cf instances running on this database--->
				<cfset mappingsEtc = getCFMappingsAndDataSourceFromVirtualDirectories (vdirs)>
				<cfif not mappingsEtc.isOK>
					<cfoutput>#mappingsEtc.reason#</cfoutput>
					<cfabort>
				</cfif>

				<!--- check that domain is in our db and that 'testsite' is the same for this domain as for all the other instances--->
				<cfset var checkDomain = checkDomainIsInSiteDef(domain=CGI.SERVER_NAME,datasource=mappingsEtc.datasource,currentValueOfApplicationTestSite=mappingsEtc.testSite)>
				<cfif not checkDomain.isOK>
					<cfoutput>#checkDomain.message#</cfoutput>
					<cfabort>
				<cfelse>
					<cfset mappingsEtc.testSite = checkDomain.testSite>
				</cfif>

				<!--- special dev site code for plumbing in a test directory mapping --->
				<cfif mappingsEtc.testSite is 2>
					<!--- <cfset getvdirs = getVirtualDirectories ("/test")> --->
					<cfif structKeyExists (getvdirs.vdirs,"/test")>
						<cfset mappingsEtc.cf_mappings["/test"] = getvdirs.vdirs["/test"]> <!--- WAB 2013-12-16 added the / infront of test, cf mapping wasn't working properly. --->
						<cfset mappingsEtc.paths.test = getvdirs.vdirs["/test"]>
					</cfif>
				</cfif>


				<cfset var instance = clusterManagementObj.registerInstance (cfappname = mappingsEtc.cfappname,  datasource=mappingsEtc.datasource, jsessionid = "",testSite= mappingsEtc.testSite, updateApplicationVariable=false,cf_mappings = mappingsEtc.cf_mappings) >
				<cfset registersite = clusterManagementObj.registerSiteToInstance (instanceID = instance.id,hostname = CGI.SERVER_NAME,datasource = mappingsEtc.datasource)>
				<cfset mappingsEtc.instance = {ipaddress = instance.instanceIPAddress,name = instance.instancename, servername = instance.servername,port=instance.instanceport,coldFusionInstanceID = instance.id,databaseid = instance.databaseid,relaywareVersionInfo = instance.relaywareVersionInfo,uniqueid = instance.instanceuniqueid}>
				<!--- all OK, so store cfapp info and a pointer to it in the object--->
				<cfset  this.structures.relayApplicationsStruct[mappingsEtc.cfappname] = mappingsEtc>  <!--- this key may already exist from another domain running on this instance, but will not mind being overwritten--->
				<cfset this.structures.hostNameToRelayApplicationsStruct[cgi.server_name] = {cfappname=mappingsEtc.cfappname}>


		</cfif>


		<cfif structKeyExists (this.structures.hostNameToRelayApplicationsStruct,CGI.SERVER_NAME)>
			<cfset var thisCFAppname = this.structures.hostNameToRelayApplicationsStruct[CGI.SERVER_NAME].CFAppname>

			<cfset getSiteByCFAppname (thisCFAppname)>
			
			<cfif not this.structures.relayApplicationsStruct[thisCFAppname].isOK>
				<cfoutput>#cgi.server_name#.  Not OK.  #this.structures.relayApplicationsStruct[thisCFAppname].reason#</cfoutput>
				<cfabort>
			</cfif>

		<cfelse>
			<cfoutput>#cgi.server_name# Not Found As Relay Site</cfoutput>		
		</cfif>
				

		<cfreturn this.structures.relayApplicationsStruct[thisCFAppname]>

	</cffunction>	



	<cffunction name="getCFMappingsAndDataSourceFromVirtualDirectories" returns="string">
		<cfargument name = "virtualDirectoriesStructure" required="yes">

		<cfset var tempCustomtagsPath ="">
		<cfset var tempRelaywarePath ="">
		<cfset var xmlFileName ="">
		<cfset var datasourceXML ="">
		<cfset var checkDatasource ="">
		
		<cfset var result = {isOK = false, virtualDirectories = virtualDirectoriesStructure,testSite="",datasource="unknown",uniqueCodeIdentifier = ""}>
				<cfif structKeyExists (virtualDirectoriesStructure,"/content") >

						<!---  work out custom tags and relayware directories
						Looks for directories named like relay   (ie if relay_XX then look for customTags_XX
						If these don't exist then assume called customTags and relayware in the same web directory as relay
						code path defaults to same as content, but overriden if specific path exists
						
						Infact we have now fairly much standardised on having relay/customtags/relayware all in a subdirectory with a name of the form relayCore_xxxx
						So some of this jiggery pokery is unnecessary
						--->
						<cfset result.paths = {
							relay = virtualDirectoriesStructure["/"],
							content = virtualDirectoriesStructure["/content"],
							code = virtualDirectoriesStructure["/content"]
						}>

						<cfif structKeyExists (virtualDirectoriesStructure,"/code")>
							<cfset result.paths.code= virtualDirectoriesStructure["/code"]>
						</cfif>

						<!--- can't think of a good name for directory above code --->
						<cfset result.paths.abovecode = getParentDirectory(result.paths.code)> <!--- one above code --->

						<cfset result.paths.core = getParentDirectory(result.paths.relay)> <!--- one above relay --->
						<cfset result.paths.web= getParentDirectory(result.paths.core)> <!--- one above relay --->
						<cfset result.paths.root =  result.paths.relay>


						<cfset tempCustomtagsPath = replacenocase(result.paths.relay ,"relay","customtags")>
						<cfif directoryExists (tempCustomtagsPath)>
							<cfset result.customtagsPath = tempCustomtagsPath>
						<cfelse>	
							<cfset result.customtagsPath = result.paths.core & "\customtags">
						</cfif>
						<cfset result.Paths.customtags = result.customtagsPath>
						
						<cfset tempRelaywarePath = replacenocase(result.paths.relay ,"relay","relayware")>
						<cfif directoryExists (tempRelaywarePath)>
							<cfset result.Paths.relayware = tempRelaywarePath>
						<cfelse>	
							<cfset result.Paths.relayware = result.paths.core & "\relayware">
						</cfif>

						<cfset result.paths.userfiles = replacenocase(result.paths.content,"\content","","ONCE")> <!--- ie directory above content---> 
						<cfset result.paths.secure = result.paths.userfiles  & "\secure">						
						<cfset result.paths.secureContent = result.paths.userfiles  & "\secure\content">						
												
						<cfset result.CF_mappings = {}>   
							<cfset result.CF_mappings["/"] = result.paths.relay>
							<cfset result.CF_mappings["/relay"] = result.paths.relay>
							<cfset result.CF_mappings["/userFiles"] = result.paths.userfiles>
							<cfset result.CF_mappings["/content"] = result.paths.content>
							<cfset result.CF_mappings["/code"] = result.paths.code>
							<!--- Note that /temp mapping is done further down --->
							<cfset result.CF_mappings["/taffyresources"]= result.paths.relay & "/taffy/resources">  
							<cfset result.CF_mappings["/wirebox"] = result.paths.core & "/wirebox">
						
						
					<cfset xmlfilename = result.paths.userfiles & "/xml/datasource.xml">
					<cfif fileExists(xmlfilename)>
						<!--- WAB 2012-02-16 changed from structure notation to xPath search - allowed for a neater document when I added a master key field to the XML file--->
						<cfset var datasourceXMLResult = xmlfunctions.readAndParseXMLFile(xmlfilename)>
						<cfif datasourceXMLResult.isOK>
							<cfset var dataSourceXMLSearch = xmlSearch(datasourceXMLResult.XML,"//datasource")>
							<cfset result.datasource = dataSourceXMLSearch[1].xmltext>
							<cftry>
								<cfquery name = "checkDataSource" dataSource = "#result.datasource#">
								select 1
								</cfquery>
								
								<!--- I am going to look at the coldFusionInstance_AutoPopulated Directory to 
									a) clear out any records which are from old instances of the database [ie if this db has just been moved from live to dev we get rid of the old stuff]
									b) get the value of testsite for the current instances.  We need to ensure that a db is only run in one 'mode' at a time
								--->
								<cfquery name = "local.getValueOfTestSite" dataSource = "#result.datasource#">
								delete from coldFusionInstance_AutoPopulated
								where databaseIdentifier <> @@serverName + '.' + db_name()
								
								select distinct testSite From coldFusionInstance_AutoPopulated
								</cfquery>

								<!--- WAB 2016-01-11 PROD2016-2635 on a brand new database there will not be a record in coldFusionInstance_AutoPopulated 
													This was not a problem until I added the unitTest mappings which are only get added when testSite in (2,3)
													This meant that the unit test mappings were only created the second time the site was flushed - which was not good for automated testing
													Adding an extra query to get the value of testsite from siteDefDomain if there is no record in coldFusionInstance_AutoPopulated sorts the problem
								--->
								<cfif local.getValueOfTestSite.recordCount is 0>
									<cfquery name = "local.getValueOfTestSite" dataSource = "#result.datasource#">
									select testsite from siteDefDomain where domain = <cfqueryparam value="#CGI.SERVER_NAME#" CFSQLTYPE="CF_SQL_VARCHAR" >
									</cfquery>
								</cfif>

								<cfset result.testsite = local.getValueOfTestSite.testSite>
								<cfset result.isOK = true>

								<!--- 
								Define the name of the CFApplication
								It is theoretically possible that an instance is running more than one code version against the same database (This probably would only happen on a dev site).
								So we need to come up with a unique name for the cf_application.
								It should be the datasource name plus something to uniquely identify the code version being run
								Am going to take all the cfmappings, concatentate them, do a hash and take the few characters	
								 --->

								<!--- 
								Change of plan 1, 
								lets just take the name of the relay directory, 
								strip off relay / relay_ and see what we are left with
								Will either be blank, or something like 8_3 - which will be fine as a suffix for naming the cfapplication
									 <cfset var uniqueCodeIdentifier = "">
									 <cfloop item="local.x" collection="#result.cf_mappings#">
										 <cfset uniqueCodeIdentifier = uniqueCodeIdentifier & result.cf_mappings[x]>
									 </cfloop>
									 <cfset result.uniqueCodeIdentifier = left (hash(uniqueCodeIdentifier),10)>

								--->	
								
								<!--- 
								Change of plan 2
								We are now putting relay,customtags & relayware in their own numbered directory of form relayCore_xxxx
								so the _xxxx bit can be our unique identifier
								 --->
									<!--- If the relay Directory has a suffix then this will find it --->
									<cfset result.uniqueCodeIdentifier = reReplaceNocase(listLast (result.paths.relay,"/\"),"relay[_]?","","ONE")>

									<cfif result.uniqueCodeIdentifier is "">
										<!--- If the relay directory did not have a suffix then this will find the suffix to the relayCore directory (which could itself be blank)--->			
										<cfset result.uniqueCodeIdentifier = reReplaceNocase(listgetAt (result.paths.relay,listlen(result.paths.relay,"/\")-1,"/\"),"relaycore[_]?","","ONE")>
									</cfif>
								
								<cfcatch>
									<cfif refindNocase(cfcatch.detail,"Datasource.*could not be found" )>
										<cfset result.reason = 'Datasource #result.datasource# not set up on instance #clusterManagementObj.getInstanceName()#'>						
									<cfelse>
										<cfset result.reason = "Database Error: #cfcatch.detail#">															
									</cfif>
									
									

								</cfcatch>
							</cftry>
						<cfelse>
							<cfset result.reason = 	datasourceXMLResult.message>					
						</cfif>	
						

						<cfif result.isOK >

							<!--- 
								WAB 2016-01-12 Add support for Unit Testing
								If we are on a test or dev site then make sure that there is a TestBox virtual directory and that it is pointing to the correct place 
								Then add virtual directory for unitTests directory
								Also, if there is a TestBox folder on any other site (such as a staging site), then we						
							--->
							<cfset var TestBoxDirectory = result.paths.core & "\TestBox">
							<cfset var UnitTestDirectory = result.paths.core & "\_support\tests\unit">
							<cfset var ExamplesDirectory = result.paths.core & "\_support\tests\examples">
							<cfset var IISAdminObj = createObject("component","IISAdmin")>
	
							<cfif listFind("2,3",result.testsite) or structKeyExists (virtualDirectoriesStructure,"\testBox")>
								<cfif directoryExists (TestBoxDirectory)> 
									<cfif not structKeyExists (virtualDirectoriesStructure,"\testBox") or virtualDirectoriesStructure["\testBox"] is not TestBoxDirectory>						
										<cfset IISAdminObj.setVirtualDirectories(vdir= "testBox", PhysicalPath = TestBoxDirectory)> 
									</cfif>						
									<cfset result.CF_mappings["/testbox"] = TestBoxDirectory>
									<cfset result.CF_mappings["/mxunit"] = TestBoxDirectory & "\system\compat" >
								</cfif>
							</cfif>
	
							<cfif structKeyExists(result.CF_mappings, "/testbox") and directoryExists (UnitTestDirectory)>
							  		<cfset result.CF_mappings["/unittests"] = UnitTestDirectory>
									<cfset IISAdminObj.setVirtualDirectories(vdir= "unittests", PhysicalPath = UnitTestDirectory)> 
							</cfif>	
	
							<cfif listFind("2,3",result.testsite)>
							  		<cfset result.CF_mappings["/examples"] = ExamplesDirectory>
									<cfset IISAdminObj.setVirtualDirectories(vdir= "examples", PhysicalPath = ExamplesDirectory)>
							</cfif>

						</cfif>

					<cfelse>
						<cfset result.reason = "No datasource XML file at #xmlfilename#">
					</cfif>	

			<cfelse>
				<cfset result.reason = "No /content or /code virtual directory">
				<cfreturn result>
			</cfif>			

			<!--- WAB 2010-03-09 this needs to be a valid variable name, otherwise can't access the CFAdmin API Object' --->
			<cfset result.CFAppname	= rereplace(listappend(result.datasource,result.uniqueCodeIdentifier,"_"),"[^A-Za-z0-9_]","","ALL")>

			<!--- WAB 2011-03-10 make a temporary directory which is not synchronised and application specific  --->
			<cfset result.paths.temp = result.paths.web & "\UnSynchedTemp\" >   <!--- This is a temporary directory which is not accessible from the web, does not get synched and has a coldfusion mapping --->
			<!--- make sure both directories exist. bit nasty doing it twice, but my createdirectoryrecursive function is not available --->
			<cfif not directoryexists(result.paths.temp)>
				<cfdirectory action="CREATE" directory="#result.paths.temp#">
			</cfif>
			<cfset result.paths.temp = result.paths.temp & result.CFAppname>   
			<cfif not directoryexists(result.paths.temp)>
				<cfdirectory action="CREATE" directory="#result.paths.temp#">
			</cfif>
			
			<cfset result.CF_mappings["/temp"] = result.paths.temp>


					
		<cfreturn result>
	
	</cffunction>
	
	<cffunction name="getParentDirectory">
		<cfargument name = "directory" required="true">		
		
		<cfreturn reverse(listRest(reverse(directory),"\")) >
	</cffunction>
	
	<cffunction name="checkDomainIsInSiteDef" returns="string">
		<cfargument name = "domain" required="true">		
		<cfargument name = "datasource" required="true">		
		<cfargument name = "currentValueOfApplicationTestSite" default = "">	
		
		<cfset var getSites =  "">
		<cfset var chkTestSite =  "">
		<cfset var testSiteValue =  "">
		<cfset var tempTestSite =  "">
		<cfset var addSite =  "">		
		<cfset var fieldname =  "">		
		<cfset var result  = {isOK = true}>
						
						<!--- process adding form if necessary - under development - no security or anything--->
						<cfif isDefined("addToSiteDef")>
							<cfif currentValueOfApplicationTestSite is not "">
								<cfset tempTestSite = currentValueOfApplicationTestSite>
							<cfelse>
								<cfset tempTestSite = addToSiteDefTestSite>
							</cfif>

							<cfquery name = "addSite" dataSource = "#datasource#">
								if not exists (select 1 from siteDefDomain where  domain =  <cfqueryparam value="#Domain#" CFSQLTYPE="CF_SQL_VARCHAR" > ) 
								BEGIN
									insert into siteDefDomain (domain,sitedefid,testsite,mainDomain)
									values (<cfqueryparam value="#Domain#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cfqueryparam value="#addToSiteDef#" CFSQLTYPE="CF_SQL_INT" >,<cfqueryparam value="#tempTestSite#" CFSQLTYPE="CF_SQL_INT" >,0)
								END	
							</cfquery>
						</cfif>
						
						<!--- check datasource, work out value of testSite--->
						<cfquery name = "local.checkForDomain" dataSource = "#datasource#">
						select * from siteDefDomain 
						where  domain =  <cfqueryparam value="#domain#" CFSQLTYPE="CF_SQL_VARCHAR" >  
						</cfquery>

						<cfif local.checkForDomain.recordcount is 0 >
							<!--- domain does not exist, offer to add it --->
							<cfquery name = "getSites" dataSource = "#datasource#">
							select * from sitedef
							</cfquery>
							
							<cfsaveContent variable="result.message">
							<cfoutput>
							Domain #domain# not found.  <BR>Which site is it connected with
								<!--- bit of an experiment.  Probably only get here if the first hit to a site is from an unregistered host, in which case we don't know value of testSite --->
							<form  method="post">
								<select name="addToSiteDef">
									<cfloop query = "getSites">
										<option value="#siteDefID#">#Name# (#iif(isInternal,de("Internal"),de("Portal"))#)
									</cfloop>
								</select> 
								<cfif currentValueOfApplicationTestSite is "">
								<!--- This cfapplication has not started up yet and the first url used to hit it is not in the sitedefs, therefore we don't know whether it is running as a test site or not 
									2013-02-25	WAB	Add testSite value 4 - Demo 
								--->
								<BR><select name="addToSiteDefTestSite">
										<option value="2">Dev Site
										<option value="4">Demo Site
										<option value="3">Test Site
										<option value="1">Staging Site
										<option value="0">Live Site
								</select>								
								</cfif>
								
								<BR><input type="submit">
							</form>
							</cfoutput>
							</cfsaveContent>
							<cfset result.isOK = false>
							
						<cfelseif currentValueOfApplicationTestSite is "">
							<!--- first domain on this site/database to be accessed, so take the value of 'testSite' which comes from sitedef as the correct one --->
							<cfset result.testSite = local.checkForDomain.testSite>
						<cfelseif local.checkForDomain.testSite is currentValueOfApplicationTestSite>
							<cfset result.testSite = local.checkForDomain.testSite>
						<cfelse>
							<cfset result.isOK = false>
							<cfset result.message = "Value of testSite for #CGI.SERVER_NAME# is not same as other domains on this database">
						</cfif>
			<cfreturn result>
	
	</cffunction>
	
	

	<cffunction name="getSiteByCFAppname">
		<cfargument name="CFAppname"	>
			<cfreturn  this.structures.relayApplicationsStruct[CFAppname]>
	</cffunction>



	<cffunction name="getVirtualDirectories">
			<cfargument name="dirs" default="/,/content,/code">

			<cfset var result = {isOK = true, vdirs = structNew(),message = ""}>	

			<cfreturn getVirtualDirectoriesWithIIS(argumentCollection = arguments)>	

	</cffunction>

	<!--- 
	For sites on a remote cluster we don't have IIS to interogate, 
	therefore we are going to try another technique to get the virtualDirectories.
	We make a request to a file in each of the content and code directories.  This file returns its location.
	Note that in a clustered enviroment we can't actually be sure that it is the same machine answering.  
	Therefore each machine in a cluster will have to use the same code locations for a given domain. (so all sites serving abc.com have to use the same directories, but def.com on the same cluster could use different directories)
	--->

	<cffunction name="getVirtualDirectoriesWithoutIIS">
		<cfargument name="dirs" default="/,/content,/code">
	
		<cfset var result = {isOK = true, vdirs = structNew(),message = ""}>
		<cfset var wsresult = "">
		<cfset var dir = "">
		<cfset var theURL = "">
		<cfset var wsHttpProtocol = "http://">
		<cfset var intialisationFile = "\initialise\initialise.cfc">
		
		
<!--- 
				<cfif structKeyExists(form,"SSLCertificate")>
					<cffile action="upload" fileField="SSLCertificate" nameConflict="makeunique" destination="d:\web">
					<cfoutput>#cffile.serverFileName#</cfoutput>
					<cfabort>					
				
				</cfif>
 --->

				<cfif cgi.SERVER_PORT_SECURE>
					<cfset wsHttpProtocol = "https://">
				</cfif>	
				<!--- Need to find virtual mappings for 3 directories, so do a loop --->
				<!--- NJH 2010-08-03 - RW8.3 - increased the timeout from 1 to 5. It was timing out reading the wsdl on application startup
					which meant that the site would have to be hit twice for it to be loaded. The issue is probably the fact that the application
					takes too long to start and that really should be fixed rather than increasing the timeout, although a timeout of 1 sec isn't that long.
					 --->

				<!--- root mapping can be found from the local of this cfc,
					 so don't need to have this in the loop 
					actually no it can't, can go wrong if have multiple code versions on same instance and request comes in from a content or code directory 
					actually (2) it is possible that getPageContext().getServletContext().getRealPath("/") would get the right answer
				
				<cfset vdirs["/"] = replaceNoCase(getCurrentTemplatePath(),"\com\serverinitialisation.cfc","","ONCE")>
				--->
				
				<cfloop index="dir" list = "#dirs#">
					<cftry>
						<cfset var SSLProblem = false>
							<cftry>
								<cfinvoke 
									webservice="#wsHttpProtocol##cgi.server_name##dir##intialisationFile#?wsdl" 
									timeout="5" 
									method = "getDetails"
									returnVariable = "wsresult"
									refreshwsdl = true
									/>
								<cfcatch>
									<cfif cfcatch.detail contains "peer not authenticated" or cfcatch.detail contains "SSLException">
										<!--- fall back to http and try again --->
										<cfset SSLProblem = true>
										<cfset var originalCatch = cfcatch>
										<cfinvoke 
											webservice="http://#cgi.server_name##dir##intialisationFile#?wsdl" 
											timeout="5" 
											method = "getDetails"
											returnVariable = "wsresult"
											refreshwsdl = true
											/>
									<cfelse>
										<cfrethrow>  <!--- Note that this rethrow shows as a second exception in debug (which had confused me!) --->
									</cfif>		
								</cfcatch>		
							</cftry>	

						<cfif wsresult.isOK>
							<cfset result.vdirs[dir] = replaceNoCase(wsresult.path,intialisationFile,"","ONCE")>
						<cfelse>
							<cfset result.isOK = false>
							<cfset result.message = "Unable to access #dir##intialisationFile# (Not Trusted)">						
							<cfbreak>	
						</cfif>	

							<cfcatch>
								<cfif cfcatch.detail contains "404 Not Found" and not SSLProblem>
									<cfset result.isOK = false>
									<cfset result.message = "Initialisation File has not been placed at #wsHttpProtocol##cgi.server_name##dir##intialisationFile#">
								<cfelse>  <!--- probably 403 Forbidden --->
									<cfset result.isOK = false>
									<cfsavecontent variable = "result.message">
										<cfoutput>
										Unable to read Initialisation File at #wsHttpProtocol##cgi.server_name##dir##intialisationFile#
										<cfif SSLProblem>
											<BR>This is probably a Certificate Error
											<BR>#originalCatch.detail#
											<BR>You may need to allow non SSL requests to the \code\initialise directory<BR>
											<cfif originalCatch.detail contains "peer not authenticated">
											<!--- WIP form to upload certificate - not finished - may not be required anyway
											<form action="/test/certificateUpload/certificateUpload.cfm" method="post" ENCTYPE="multipart/form-data">
												<input type="file" name="SSLCertificate">
												<input type="submit">
											</form>
											--->
										</cfif>	
										<cfelse >
											<cfoutput><BR>#cfcatch.detail#</cfoutput>
										</cfif>	
										</cfoutput>
									</cfsavecontent>
								</cfif>
								<cfbreak>
							</cfcatch>
						</cftry>		
					
				</cfloop>

		<cfreturn result>
	
	</cffunction>


<!--- 
This sections gets all the details of sites and bindings from IIS and pops them into a structure --->

	<cffunction name="getVirtualDirectoriesWithIIS">
		<cfargument name="dirs" default="/,/content,/code">

			<cfset var IISAdminObj = createObject("component","IISAdmin")>
			<cfset var result = IISAdminObj.getVirtualDirectoryStructureForSite ()>

			<cfreturn result>

	</cffunction>



	<!--- Returns the structure which corresponds to a particular databaseid
		being used by bounceback processor to confirm which datasource to use to update a comm record
	 --->
	<cffunction name="getRelayApplicationStructuresForDatabaseID">
		<cfargument name="databaseid">
		
		<cfset var result = arrayNew(1)>
		<cfset var appname = "">
		<cfloop collection=#this.structures.relayApplicationsStruct# item="appname">
			<cfif this.structures.relayApplicationsStruct[appname].instance.databaseid is arguments.databaseid>
				<cfset arrayAppend(result,this.structures.relayApplicationsStruct[appname])>
			</cfif>
		</cfloop>
	
		<cfreturn result>
		
	</cffunction>
	
	<cffunction name="getDataSourceNameForDatabaseID">
		<cfargument name="databaseid">
		
		<cfset var result = "">
		<cfset var applicationStructuresArray = getRelayApplicationStructuresForDatabaseID(databaseid)>
		<cfif arrayLen(applicationStructuresArray)>
			<cfset result = applicationStructuresArray[1].datasource >
		
		</cfif>
		
		<cfreturn result>
		
	</cffunction>




	
</cfcomponent>

