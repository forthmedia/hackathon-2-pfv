<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayui.cfc
Author:			NJH
Date started:	13-02-2015

Description:	Framework for managing messages

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/02/13			NJH 		Jira fifteen-147 if type is passed through as an empty string, then treat it as success
2015-11-04 			WAB			Changed getMessage to support list of scopes and got rid of multiple evaluates throughout

Possible enhancements:


 --->


<cfcomponent output="false">

<!---
WAB 2013-05-29 Added support for a callback function which will fire after the message is closed  (I needed it to resize something in comms)
				Listener now needs to be added on each message individually rather than globally, so added a random ID
WAB 2013-05-29 Added support for closing the message automatically after a set amount of time. Some debate as whether this is philosophically a good idea - so may be blackballed
YMA	2013-06-21 Added option to NOT sanatise HTML so I can pass I frames into my preview results message in GenericEmails/emails.cfm
2014-09-10	RPW		Add Lead Screen on Portal
2014-09-12	RPW		Add Lead Screen on Portal - Locked scope in case session scope accessed
2014-10-06	RPW		Lead screen not saving and displaying message
2014-10-28	RPW		Changed Messages functionality to take closeAfter parameter on Set rather than get
--->
	<cffunction name="message" output="false" access="public">
		<cfargument name="message" type="string" required="true">
		<cfargument name="type" type="string" default="success" hint="Accepts: success, error, info, warning or define your own class">
		<cfargument name="showClose" type="boolean" default="true">
		<cfargument name="id" type="string" default="message_#replacenocase(rand(),".","")#">
		<cfargument name="callback" type="string" default="">
		<cfargument name="closeAfter" type="numeric" default="0">
		<cfargument name="doNotSanatiseHTML" type="boolean" default="false">

		<cfset var classname = arguments.type neq ""?arguments.type:"successblock"> <!--- NJH 2015/02/13 Jira fifteen-147 if type is an empty string, then treat it as success --->
		<cfset var returnVal = "">

		<cfif trim(arguments.message) neq "">
			<cfif request.relayCurrentUser.isInternal>
				<cfif listFindnoCase("success,info,warning,error",arguments.type)>
					<cfset classname = "#lcase(arguments.type)#block">
				</cfif>
			</cfif>

			<cfsavecontent variable="returnVal">
				<cfoutput><div id="#arguments.id#" class="#classname#"><cfif arguments.showClose><a id="close_#arguments.id#" class="close"></a></cfif><cfif arguments.doNotSanatiseHTML>#arguments.message#<cfelse>#application.com.security.sanitiseHTML(arguments.message)#</cfif></div>
				<cfif arguments.showClose or arguments.closeAfter is not 0>
					<script>
					jQuery(document).ready(function (){
						jQuery('##close_#arguments.id#').click(
							function () {
								jQuery(this).parents('.success, .error, .warning, .info, .infoblock, .warningblock, .successblock, .errorblock').hide(600,'linear',
									function(){
										this.remove();
										<cfif arguments.callback is not "">
											#arguments.callback#
										</cfif>
									})
						})
					})
		 			</script>
				</cfif>

				<cfif closeAfter is not 0>
					<script>window.setTimeout(function () {jQuery('##close_#arguments.id#').trigger('click')},#arguments.closeAfter# * 1000)</script>
				</cfif>
				</cfoutput>
			</cfsavecontent>

		</cfif>

		<cfreturn returnVal>
	</cffunction>


	<!--- 2014-09-10	RPW		Add Lead Screen on Portal --->
	<!--- 2014-10-28	RPW	Changed Messages functionality to take closeAfter parameter on Set rather than get --->
	<cffunction name="setMessage" access="public" returntype="void">
		<cfargument name="message" type="string" required="true">
		<cfargument name="type" type="string" default="success">
		<cfargument name="scope" type="string" default="session">
		<cfargument name="closeAfter" type="string" default="0">

		<cfscript>
			var scopeStruct = Evaluate(arguments.scope);
			//	2014-09-12	RPW		Add Lead Screen on Portal - Locked scope in case session scope accessed
			lock timeout = 1 scope = arguments.scope throwOnTimeout = true type = "exclusive" {

				if (!StructKeyExists(scopeStruct,"messages")) {
					StructInsert(scopeStruct,"messages",[]);
				}

				ArrayAppend(scopeStruct.messages,arguments);

			}
		</cfscript>

	</cffunction>


	<!---2014-09-10	RPW		Add Lead Screen on Portal 
		 2014-10-28	RPW	Changed Messages functionality to take closeAfter parameter on Set rather than get 
		 2015-11-04 WAB	Changed to support list of scopes and got rid of multiple evaluates
	--->
	<cffunction name="getMessage" access="public" returntype="string">
		<cfargument name="scope" type="string" default="session">
		<cfargument name="deleteMessage" type="string" default="true">

		<cfscript>
			var rtnString = "";
			arrayEach (listToArray(scope), function (thisScope){
				var scopeStruct = Evaluate(thisScope);
				//	2014-09-12	RPW		Add Lead Screen on Portal - Locked scope in case session scope accessed
				lock timeout = 1 scope = thisScope throwOnTimeout = false type = "exclusive" {
	
					if (StructKeyExists(scopeStruct,"messages")) {
						for (var messageStruct IN scopeStruct.messages) {
							rtnString &= message(argumentCollection=messageStruct);
						}
						if (deleteMessage) {
							StructDelete(scopeStruct,"messages");
						}
					}
	
				}
			});	
		</cfscript>

		<cfreturn rtnString>

	</cffunction>


	<!--- 2014-10-28	RPW	Changed Messages functionality to take closeAfter parameter on Set rather than get --->
	<cffunction name="displayMessage" access="public">
		<cfargument name="scope" type="string" default="session">
		<cfargument name="deleteMessage" type="string" default="true">

		<cfoutput>#getMessage(argumentCollection=arguments)#</cfoutput>
	</cffunction>

</cfcomponent>