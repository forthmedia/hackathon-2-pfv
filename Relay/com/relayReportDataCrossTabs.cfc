<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="relay" hint="componentFunction">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getCrossTabDefinition" access="public" returntype="query" hint="Returns a query containing a cross tab report's definition">
		<cfargument name="reportCrossTabID" type="numeric" required="yes">
		<cfargument name="cacheQueriesForMins" type="numeric" default="#application.cachedReportTimeShortInMins#">
				
		<cfscript>
			var getCrossTabDefinition = "";
			var cachedForMins = arguments.cacheQueriesForMins;
		</cfscript>
		
		<cfquery name="getCrossTabDefinition" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, cachedForMins, 0)#">
			SELECT * from reportCrossTab where reportCrossTabID = #arguments.reportCrossTabID#
		</cfquery>
		
		<cfreturn getCrossTabDefinition>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getCrossTabData" access="public" returntype="query" hint="Returns a query containing the data for a specified cross tab.">
		<cfargument name="reportCrossTabID" type="numeric" required="yes">
		<cfargument name="cacheQueriesForMins" type="numeric" default="#application.cachedReportTimeShortInMins#">
				
		<cfscript>
			var getCrossTabData = "";
			var cachedForMins = arguments.cacheQueriesForMins;
			var getReportDefinition = application.com.relayReportDataCrossTabs.getCrossTabDefinition(reportCrossTabID=arguments.reportCrossTabID);
		</cfscript>
		
		
		<cfquery name="getCrossTabData" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, cachedForMins, 0)#">
			SELECT distinct #getReportDefinition.reportRows# as reportRows, #getReportDefinition.reportCols# as reportcolumns, 
				count(#getReportDefinition.reportUnits#) as reportUnits
			FROM #getReportDefinition.tables#
			WHERE #preserveSingleQuotes(getReportDefinition.whereClause)#
			group by #getReportDefinition.reportRows#, #getReportDefinition.reportCols#
			order by #getReportDefinition.reportRows#, #getReportDefinition.reportCols#
		</cfquery>
	
		<cfreturn getCrossTabData>
	</cffunction>
</cfcomponent>