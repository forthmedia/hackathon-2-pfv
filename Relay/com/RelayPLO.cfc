<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2008/02/19	WAB 	mods to createDataSearchQueryFromFormVariables to do left joins so that empty orgs can be found
2008/02/24  WAB		more mods to createDataSearchQueryFromFormVariables including adding a human readable description of the query, making a structure of the fields used
2008/07/30 	NYF		Bug Fix T-10 Issue 583: Added support for creating country screens
					which were always possible to create, but never supported properly throughout the code
2008/09/03	PPB		added method emailAddressExists
2009/01/14	WAB		Bug 1286, speed up getPersList by using a new view without contact history vPersList v2
2009/01/21	WAB 	Mods to createDataSearchQueryFromFormVariables () to bring back a mini version of the criteria in words for the left nav
2009/01/14	WAB		Bug 1286 continued.  Decided to make it a switch so ended up going back to old view (modified slightly) and bringing back coumns selectively
2009/05/28	NJH		P-SNY063 - added functions to insert/update entity and POL data.
2009/09/25	NJH		LHID 2236
2010/04/22  NAS		P-PAN002 Added Function to get the Location from an OrgID
2010/05/25 	NAS		P-CLS002 - added a function to retfrieve Org, Loc or Per IDs from Foreign Key
2011-03-07	NYB  	rewrote getUserGroups
2011-03-11	NYB  	added Order by c.countryDescription to getOrgCountries
2011-03-14 	NYB 	LHID5528 created getLocDetails function
2011/05/25	NYB		REL106 moved processing code of getEntityStructure to relayEntity.getEntityStructure
2011/06/30	NAS		P-LEX053 - Add VAT Search
2011/07/04 	PPB 	LID7082 added isDefined("request.showDeletedRecords")
2011/07/20	PPB		LEN024 added showInActiveRecords to getPersList
2012/01/13	RMB		Added short address to display border on demo opp
2012/01/12 	PPB 	P-REL112 connect to linkedIn
2012/01/13	RMB		Added short address to display border on demo opp
2013/01/02	IH		Changed = searches to IN searches
2013-03-06 	NYB		Case 434039 made changes to getOrgLocations function to allow it to be used for getting locations by persondetail.cfm
2014-02-26	PPB 	Case 439001 exclude person records flagged for deletion in isEmailAddressUnique()
2014-09-18	WAB		CASE 441742 var whole cfc.  Prompted by bug due to lack of var on siteName in createDataSearchQueryFromFormVariables()
2014-11-20	AXA		P-PGI001 added function getLocationForOrg()
2014-11-25	RPW		CORE-97 Phase 1: Extend database fields for Leads - Set the countryID to be that of the partner if it is selected
2014-12-08 	PPB 	ABS001 change to isEmailAddressUnique to avoid undefined variable error
2015-10-13  DAN     Case 446180 - editing of a user email address, fix for scenarios when accountType is NULL/empty or non-numeric
2016/02/10	NJH		JIRA Prod2015-343 - added domain matching function.
2016/06/09	NJH		JIRA PROD2016-342 - changes to createDataSearchQueryFromFormVariables function to support more location searching, such as countryID and url.
2016-10-07	WAB 	During CASE 450971/ PROD2016-2654 change formatAddress() to support in a location query/structure instead of just a locationid

 --->

<cfcomponent displayname="RelayPLO" hint="A collection of Person, Location and Organisation">

	<CFFUNCTION name="getOwners" hint="all the entitled owners">
	  	<CFARGUMENT name="actionOrgs" required="true" type="string">

        <cfscript>
		  var getOwners = "";
	    </cfscript>

		<CFQUERY NAME="getOwners" dataSource="#application.siteDataSource#">
			SELECT p.PersonID as ownerPersonID, p.firstName+' '+p.lastName as fullname,
				o.OrganisationName
			FROM person p
				inner join organisation o on p.OrganisationID = o.OrganisationID
			where o.organisationID  in ( <cf_queryparam value="#arguments.actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				and p.active = 1
			ORDER BY p.firstName
		</CFQUERY>

	 <CFRETURN 	getOwners>
	</CFFUNCTION>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the person details for THIS.personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<!--- AR 2005-05-09 Changed the personID field to be dealt with as a list added a more complete record set. (Country Information) --->
	<cffunction access="public" name="getPersonDetails" hint="Returns various person details for THIS.personID.">
		<cfargument name="personID" required="Yes">

		<cfset var getPersonDetailsQry = "">

		<CFQUERY name="getPersonDetailsQry" datasource="#application.SiteDataSource#">
			SELECT p.personid, p.firstname, p.lastname, p.firstname+' '+p.lastname as fullname, p.salutation,
				p.email, p.officephone as directLine, l.telephone as switchboard, o.organisationName, p.mobilePhone,
				l.countryid, l.locationID, o.organisationID , c.countryDescription,
				p.lastUpdated
			FROM Person p
			INNER JOIN Organisation o ON p.Organisationid = o.OrganisationID
			INNER JOIN Location l ON p.locationid = l.locationID
			INNER JOIN country c ON l.countryID = c.countryID
			WHERE personID  in ( <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			order by CountryDescription,fullname
		</CFQUERY>
		<cfreturn getPersonDetailsQry>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Returns the location details for locationid
			2011-03-14 NYB LHID5528 created
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLocDetails" hint="Returns location details for locationid passed.">
		<cfargument name="LocationID" required="Yes">

		<cfset var getLocationDetailsQry = "">

		<CFQUERY name="getLocationDetailsQry" datasource="#application.SiteDataSource#">
			SELECT l.*, o.organisationName, c.countryDescription as country
			FROM Location l
			INNER JOIN Organisation o ON l.Organisationid = o.OrganisationID
			INNER JOIN country c ON l.countryID = c.countryID
			WHERE l.LocationID  in ( <cf_queryparam value="#arguments.LocationID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			order by CountryDescription,l.SiteName
		</CFQUERY>
		<cfreturn getLocationDetailsQry>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns user details for THIS.userID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUserDetails" hint="Returns user details for THIS.userID.">

			<cfset var getUserDetails = "">

			<CFQUERY NAME="getUserDetails" DATASOURCE="#THIS.dataSource#">
			select p.userName, ug.name as userGroupName, ug.userGroupID, p.personID
			from userGroup ug
			join person p on p.personid = ug.personid
			where ug.userGroupID =  <cf_queryparam value="#THIS.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<CFSET THIS.userName = getUserDetails.userName>
		<CFSET THIS.userGroupName = getUserDetails.userGroupName>
		<CFSET THIS.userGroupID = getUserDetails.userGroupID>
		<CFSET THIS.personID = getUserDetails.personID>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns all countries
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getCountries" hint="all countries">
  		<cfargument name="countryIds" default="" required="No">
		<!--- AR 2005-04-25 Modified this to return a more flexible set of country information. --->
		    <cfscript>
			  var getCountriesQry = "";
		    </cfscript>
		<CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
			SELECT countryDescription as country, country.countryID, ISOcode FROM country
				<cfif len(arguments.countryIds)>
				WHERE country.countryID  IN ( <cf_queryparam value="#arguments.countryIds#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
				ORDER BY country
		</CFQUERY>

	<cfreturn getCountriesQry>
  </CFFUNCTION>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns Locations
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getLocations" hint="all locations">
	<CFARGUMENT name="actionOrgs" required="true" type="string">
	<CFARGUMENT name="LocationID" required="false" type="string" hint="location id">
		<cfscript>
		  var getLocations = "";
	    </cfscript>
		<CFQUERY NAME="getLocations" dataSource="#application.siteDataSource#">
	         SELECT SiteName, LocationID, countryID
		     FROM location
			 WHERE organisationID  in ( <cf_queryparam value="#actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfif isdefined('arguments.LocationID')>
				AND LocationID = <cfqueryparam value="#arguments.LocationID#" cfsqltype="cf_sql_integer">
			</cfif>
		     ORDER BY SiteName
        </CFQUERY>

	<cfreturn getLocations>
  </CFFUNCTION>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns Organisation Locations Details & Address
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getOrgLocations" hint="all locations" returntype="query" >
	<CFARGUMENT name="OrganisationID" required="true" type="any">
	<CFARGUMENT name="excludeFlaggedForDeletion" default="false" type="boolean"><!--- NYB 2013-03-06 Case 434039 added --->
	<CFARGUMENT name="additionalFilter" default="" type="string"><!--- NYB 2013-03-06 Case 434039 added --->

		<cfset var qryOrgLocations = "">

		<CFQUERY NAME="qryOrgLocations" datasource="#application.siteDataSource#">
			SELECT isnull(l.SiteName,'')
				+ case WHEN ltrim(isNull(l.Address1,'')) = '' then '' else ', ' + l.Address1 end
				+ case WHEN ltrim(isNull(l.Address4,'')) = '' then '' else ', ' + l.Address4 end
				+ case WHEN ltrim(isNull(l.Address5,'')) = '' then '' else ', ' + l.Address5 end
				+ case WHEN ltrim(isNull(l.postalCode,'')) = '' then '' else ', ' + l.postalCode end
				+ case WHEN ltrim(isNull(c.ISOCode,'')) = '' then '' else ', ' + c.ISOCode end
				+ case WHEN ltrim(isNull(l.locationID,'')) = '' then '' else ', ID:' + cast(l.locationID as nvarchar(50)) end
			AS outputAddress,
			  case WHEN ltrim(l.Address1) is null then '' else l.Address1 end
			  + case WHEN ltrim(l.Address4) is null then '' else ', ' + l.Address4 end
			  + case WHEN ltrim(l.Address5) is null then '' else ', ' + l.Address5 end
			  + case WHEN ltrim(c.ISOCode) is null then '' else ', ' + c.ISOCode end
			AS outputShortAddress,
			<!--- START: NYB 2013-03-06 Case 434039 removed - looked like this was introduce (twice) in error as part of a project merge: ---
			  case WHEN ltrim(l.Address1) is null then '' else l.Address1 end
			  + case WHEN ltrim(l.Address4) is null then '' else ', ' + l.Address4 end
			  + case WHEN ltrim(l.Address5) is null then '' else ', ' + l.Address5 end
			  + case WHEN ltrim(c.ISOCode) is null then '' else ', ' + c.ISOCode end
			AS outputShortAddress,
			!--- END: NYB 2013-03-06 Case 434039 --->
				l.locationID, l.SiteName, l.countryID

			<!--- CoBranding --->
			,Address1 ,Address2 ,Address3 ,Address4 ,Address5, postalCode ,note ,telephone, fax, locEmail, specificUrl
			FROM Location L
				INNER JOIN country c ON l.countryID = c.countryID
				<!--- START: NYB 2013-03-06 Case 434039 added: --->
				<cfif excludeFlaggedForDeletion>
					left join booleanFlagData locDelete with (noLock) on locDelete.entityID = l.locationId and locDelete.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID="deleteLocation").flagID#" CFSQLTYPE="cf_sql_integer" >
				</cfif>
				<!--- END: NYB 2013-03-06 Case 434039 --->
			WHERE l.organisationID = <cfqueryparam value="#val(OrganisationID)#" cfsqltype="cf_sql_integer">
				<!--- START: NYB 2013-03-06 Case 434039 added: --->
				<cfif excludeFlaggedForDeletion>
					and locDelete.entityID is null
				</cfif>
				#additionalFilter#
				<!--- END: NYB 2013-03-06 Case 434039 --->
		</CFQUERY>


	<cfreturn qryOrgLocations>
  </CFFUNCTION>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns countries for a Organisation Locations  2007-10-11 Gad
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getOrgCountries" hint="returns countries for a org" returntype="query" >
	<CFARGUMENT name="OrganisationID" required="true" type="any">

		<cfset var qryOrgCountries = "">

		<CFQUERY NAME="qryOrgCountries" datasource="#application.siteDataSource#">
			SELECT distinct l.countryID,c.countryDescription    <!--- WAB added the distinct --->
			FROM Location L
			INNER JOIN country c ON l.countryID = c.countryID
			WHERE l.organisationID = <cfqueryparam value="#val(OrganisationID)#" cfsqltype="cf_sql_integer">
			<!--- 2012-07-26 PPB P-SMA001 commented out
			and c.countryid in (#request.relaycurrentuser.countrylist#)
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->

			Group by c.countryDescription, l.countryID
			Order by c.countryDescription /*NYB 2011-03-11 added*/
		</CFQUERY>


	<cfreturn qryOrgCountries>
  </CFFUNCTION>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns Organisations
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getOrganisations" hint="returns all organisations">
		<CFARGUMENT name="actionOrgs" required="true" type="string">

		<cfscript>
		  var getOrganisations = "";
	    </cfscript>

		<CFQUERY NAME="getOrganisations" dataSource="#application.siteDataSource#">
		    SELECT OrganisationID, OrganisationName
			FROM organisation
			WHERE organisationID  in ( <cf_queryparam value="#actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			ORDER BY OrganisationName
	   </CFQUERY>

	<cfreturn getOrganisations>
  </CFFUNCTION>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	NYB 2011-03-07 - rewrote to use existing function
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getUserGroups" hint="retrieves user groups for the current user.">
   		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">
   		<cfargument name="UserGroups" type="string" required="false">
   		<cfargument name="UserGroupIDs" type="string" required="false">
   		<cfargument name="IncludePersonUserGroups" type="boolean" default="false">

		<cfset var getUserGroups = application.com.relayUserGroup.getUserGroupAllocation(argumentCollection=arguments)>

	<cfreturn getUserGroups>
  </CFFUNCTION>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	SWJ: 2005-08-17 Created this new function
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="updatePersonEmailStatus" hint="Updates the person's email status to -0.5 which is the know good email value by default.">
   		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="emailStatus" type="numeric" default="-0.5">

		<cfset var updateEmailStatus = "">

		<CFQUERY NAME="updateEmailStatus" dataSource="#application.siteDataSource#">
		     update person
			 	set emailStatus = #arguments.emailStatus#
		     WHERE personID = #arguments.personid# and emailStatus <> #arguments.emailStatus#
	   </CFQUERY>

	<cfreturn "Email status updated">
  </CFFUNCTION>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	SWJ: 2006-03-16 Created this to be used in data/perslist.cfm
WAB 2008/01 Mods Bug 1286 added getLastContactDate switch
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
  <CFFUNCTION access="public" name="getPersList" hint="Returns a list of people from vPersList.">
		<cfargument name="organisationID" type="numeric" required="yes">
		<cfargument name="FirstName" type="string" default="">
		<cfargument name="LocationIDs" type="string" default="">
		<cfargument name="LastName" type="string" default="">
		<cfargument name="fullname" type="string" default="">
		<cfargument name="PersonID" type="string" default="">
		<cfargument name="Email" type="string" default="">
		<cfargument name="SelectionID" type="string" default="0">
		<cfargument name="sortOrder" type="string" default="p.firstName,p.lastName">
		<cfargument name="getLastContactDate" default = "false">
		<cfargument name="showInActiveRecords" type="boolean" default = "true">		<!--- 2011/07/20 PPB LEN024; Lenovo will by default send in false --->
		<cfargument name="showDeletedRecords" type="boolean" default="false">

		<cfscript>
		  var getPersList = "";
		  var columnList= "*";
		  var thissortorder= arguments.sortorder;
		  var linkedInEnabled = false;
		  var getPersListDef = "";
	    </cfscript>

		<!--- getLastContactDate switch allows us to not bring back lastcontactdate [for quick response]
			can't use select * and view might change over time,
			so get list of columns and delete appropriate ones
		--->
		<CFQUERY NAME="getPersListDef" DATASOURCE="#application.SiteDataSource#">
			select * from vPersList where 1 = 0
		</CFQUERY>
		<cfset columnList = getPersListDef.columnList>

		<cfif not getLastContactDate>
			<!--- remove lastcontactdate column and check not in the sort order --->
			<cfset columnList = listdeleteAt(columnList ,listfindnocase(columnList,"lastContactDate"))>
			<cfif listContainsNoCase(sortOrder,"lastcontactdate")>
				<cfset thissortorder = listdeleteat (arguments.sortorder,listContainsNoCase(arguments.sortOrder,"lastcontactdate"))>
			</cfif>
		<cfelse>
			<!--- remove the alternative column hasContactHistory (a slightly slow column, so don't make things worse by bringing it back --->
			<cfset columnList = listdeleteAt(columnList ,listfindnocase(columnList,"hascontacthistory"))>
		</cfif>

		<cfset columnList = listdeleteAt(columnList ,listfindnocase(columnList,"lastUpdated"))>			<!--- 2012/01/12 PPB P-REL112 removed from list cos the columnname is ambiguous with that from the serviceEntity/service table which I am now joining --->
		<cfset columnList = listdeleteAt(columnList ,listfindnocase(columnList,"lastUpdatedBy"))>		<!--- 2012/01/12 PPB P-REL112 removed from list cos the columnname is ambiguous with that from the serviceEntity/service table which I am now joining --->
		<cfset LinkedInEnabled = application.com.settings.getSetting("socialMedia.enableSocialMedia") and (ListFindNoCase(application.com.settings.getSetting("socialMedia.services"),'LinkedIn') gt 0)>  	<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->

		<CFQUERY NAME="getPersList" DATASOURCE="#application.SiteDataSource#">
			SELECT #columnList#
			<cfif LinkedInEnabled>								<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->
				,se_li.publicProfileURL AS linkedInProfileURL
			</cfif>
			from vPersList p
			<CFIF arguments.SelectionID IS NOT 0>
			 	INNER JOIN (select entityID from selectionTag where SelectionID =  <cf_queryparam value="#arguments.SelectionID#" CFSQLTYPE="CF_SQL_INTEGER" > ) as st
				on p.personid = st.entityID
			</CFIF>
			<cfif LinkedInEnabled>						<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->
				LEFT OUTER JOIN (dbo.serviceEntity se_li with (noLock) INNER JOIN dbo.service s_li ON s_li.serviceID = se_li.serviceID AND s_li.name='LinkedIn') ON se_li.entityTypeID=0 AND se_li.entityID = p.personID
			</cfif>

			WHERE p.organisationID = #arguments.organisationID#
			<CFIF len(arguments.LocationIDs) gt 0>AND p.LocationID  in ( <cf_queryparam value="#arguments.LocationIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</CFIF>
			<CFIF len(arguments.FirstName) gt 0>AND ltrim(rtrim(p.FirstName))  like  <cf_queryparam value="#arguments.Firstname#%" CFSQLTYPE="CF_SQL_VARCHAR" >  </CFIF>
			<CFIF len(arguments.LastName) gt 0>AND ltrim(rtrim(p.LastName))  like  <cf_queryparam value="#arguments.Lastname#%" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF>
			<CFIF len(arguments.PersonID) gt 0>AND p.personid IN (<cf_queryparam value="#arguments.PersonID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)  </CFIF>
			<CFIF len(arguments.fullName) gt 0>AND ltrim(rtrim(p.FirstName)) + ' ' + ltrim(rtrim(p.LastName))  like  <cf_queryparam value="#arguments.Fullname#%" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF><!--- WAB 2008/04/28 added full name to because this comes from quick search --->
			<CFIF len(arguments.Email) gt 0>AND ltrim(rtrim(p.Email))  like  <cf_queryparam value="#arguments.Email#%" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF>  <!--- NJH 2007/02/12 added the % --->
			<cfif not arguments.showDeletedRecords>
				and deleted = 0
			</cfif>
			<!--- 2011/09/19 LID7745 previously showed inactive AND active if showInActiveRecords=1; also switched the logic around so that if showInActiveRecords is not defined it defaults to active --->
			<cfif arguments.showInActiveRecords>	<!--- 2011/07/20 PPB LEN024; normally InActive records are displayed --->
				and active = 0
			<cfelse>
				and active = 1
			</cfif>

			<cfif thissortorder is not "">
				ORDER BY <cf_queryObjectName value="#thissortorder#">
			</cfif>


		</CFQUERY>
  	<cfreturn getPersList>

	</CFFUNCTION>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       2006-03-18 SWJ created function to streamline perslist.cfm
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="countPeopleInOrg" hint="Returns a the number of people in org with organisationID">
		<cfargument name="OrganisationID" type="numeric" required="yes">

		<cfscript>
			var countPeopleInOrg = "";
		</cfscript>

		<cfquery name="countPeopleInOrg" datasource="#application.siteDataSource#">
			select count(*) as numbPers from person where organisationID = #arguments.OrganisationID#
		</cfquery>

		<cfreturn countPeopleInOrg.numbPers>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       2006-03-18 SWJ created function to streamline perslist.cfm
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="countLocsInOrg" hint="Returns a the number of people in org with organisationID">
		<cfargument name="OrganisationID" type="numeric" required="yes">

		<cfscript>
			var countLocsInOrg = "";
		</cfscript>

		<cfquery name="countLocsInOrg" datasource="#application.siteDataSource#">
			select count(*) as numLocs from location where organisationID = #arguments.OrganisationID#
		</cfquery>

		<cfreturn countLocsInOrg.numLocs>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       2006-03-18 SWJ created function to streamline perslist.cfm & orgdetail.cfm
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOrgDetails" hint="Returns a the number of people in org with organisationID">
		<cfargument name="OrganisationID" type="numeric" required="yes">
		<cfargument name="columnList" type="string" required="no">			<!--- 2012/01/20 PPB P-REL112 send in columnList while I'm at it --->

		<cfscript>
			var getOrgDetails = "";
			var LinkedInEnabled = "";
		</cfscript>

		<cfset LinkedInEnabled = application.com.settings.getSetting("socialMedia.enableSocialMedia") and (ListFindNoCase(application.com.settings.getSetting("socialMedia.services"),'LinkedIn') gt 0)>  	<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->


		<CFQUERY NAME="getOrgDetails" datasource="#application.siteDataSource#">
			SELECT DISTINCT
					<cfif StructKeyExists(arguments,"columnList")>
						#arguments.columnList#
					<cfelse>
						o.*, ugb.Name AS LastUpdatedBy, o.LastUpdated, uga.Name AS CreatedBy
					</cfif>
					<cfif LinkedInEnabled>								<!--- 2012/01/20 PPB P-REL112 connect to linkedIn --->
						, se_li.ServiceEntityID AS LinkedInOrgId
					<cfelse>
						, '' AS LinkedInOrgId
					</cfif>
				FROM organisation o LEFT OUTER JOIN
			        UserGroup ugb ON o.LastUpdatedBy = ugb.UserGroupID LEFT OUTER JOIN
			        UserGroup uga ON o.CreatedBy = uga.UserGroupID
					<cfif LinkedInEnabled>								<!--- 2012/01/20 PPB P-REL112 connect to linkedIn --->
						LEFT OUTER JOIN (dbo.serviceEntity se_li with (noLock) INNER JOIN dbo.service s_li ON s_li.serviceID = se_li.serviceID AND s_li.name='LinkedIn') ON se_li.entityTypeID=2 AND se_li.entityID = o.organisationID
					</cfif>

				WHERE o.organisationID = #arguments.OrganisationID#
				  	<!--- <cfif isDefined("request .country ScopeOrganisationRecords") and request .country ScopeOrganisationRecords eq 1> --->
				  	<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
						<!--- only if countryScopeOrganisationRecords is set to 1 in content/CFTemplates/relayINI.cfm --->
			 			<!--- 2012-07-26 PPB P-SMA001 commented out
			 			AND o.CountryID IN (#request.relayCurrentUser.CountryList#)
			 			 --->
			 			#application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
					</cfif>
		  </CFQUERY>

		<cfreturn getOrgDetails>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	SSS: 2007-07-04 Copied this from movelocationtoneworganisation.cfm to
	put into a function so that it can be used in multiple places.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getMoveLocToNewOrg" hint="moves a location to a new organisation">
		<cfargument name="LocationID" type="numeric" required="yes">

		<cfset var getLocation = "">
		<cfset var oldOrgID = "">
		<cfset var status = "">
		<cfset var insOrgDataSource = "">
		<cfset var updateLocation = "">
		<cfset var updatePerson = "">

		<CFQUERY NAME="GetLocation" datasource="#application.siteDataSource#">
			select sitename,countryID,organisationID from location
			where locationid = <cf_queryparam value="#arguments.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<cfset oldorgID = getLocation.organisationid>
		<CFIF getLocation.recordCount is 1>

			<cfset var orgDetails={CountryID=getLocation.countryid,	organisationName=getLocation.sitename,Active=1}>
			<cf_transaction>
				<cfscript>
					var newLocOrgID = application.com.relayEntity.insertOrganisation(organisationDetails=orgDetails,insertIfExists=true).entityID;
				</cfscript>

				<!--- <CFQUERY NAME="insOrgDataSource" DATASOURCE=#application.SiteDataSource#>
					INSERT INTO OrgDataSource(OrganisationID, DataSourceID, RemoteDataSourceID)
										VALUES(<cf_queryparam value="#newLocOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
				</CFQUERY> --->

				<!--- update organisationid of person and locations --->
				<CFQUERY NAME="updateLocation" DATASOURCE=#application.SiteDataSource#>
					update location set organisationid =  <cf_queryparam value="#newLocOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
					where organisationid =  <cf_queryparam value="#oldOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and locationid = #arguments.LocationID#
				</CFQUERY>

				<!--- handled by trigger on location table
				<CFQUERY NAME="updatePerson" DATASOURCE=#application.SiteDataSource#>
					update person set organisationid =  <cf_queryparam value="#newLocOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
					where organisationid =  <cf_queryparam value="#oldOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and locationid = #arguments.LocationID#
				</CFQUERY> --->
			</cf_transaction>

			<!--- NJH 2009/01/29 Bug Fix All Sites Issue 1696 - Added the organisation name and ID to the status --->
			<cfset status = "#getLocation.sitename# moved to new Organisation '#getLocation.sitename#' (ID:#newLocOrgID#)">
			<cfreturn status>
		<cfelse>
			<cfset status ="Location Not Found">
			<cfreturn status>
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	SSS: 2007-09-27 This function below will move a location from 1 org to a exsiting org.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getMoveLocToExistingOrg" hint="moves a location to a existing organisation">
		<cfargument name="LocationID" required="yes" type="numeric">
		<cfargument name="neworgID" required="yes" type="numeric">
		<cfargument name="LastUpdatedBy" default="#request.relaycurrentuser.usergroupID#" type="numeric">
		<cfargument name="LastUpdated" default="#CreateODBCDate(now())#" type="date">

		<cfset var updateLocation = "">
		<cfset var updatePerson = "">

		<cftransaction>

			<!--- update organisationid of person and locations --->
			<CFQUERY NAME="updateLocation" DATASOURCE=#application.SiteDataSource#>
				update location
					set organisationid 	= 	#arguments.neworgID#,
						LastUpdatedBy	=	#arguments.LastUpdatedBy#,
						LastUpdated		=	#arguments.LastUpdated#
				where locationid = #arguments.LocationID#
			</CFQUERY>

			<CFQUERY NAME="updatePerson" DATASOURCE=#application.SiteDataSource#>
				update person
					set organisationid = 	#arguments.neworgID#,
						LastUpdatedBy	=	#arguments.LastUpdatedBy#,
						LastUpdated		=	#arguments.LastUpdated#
				where locationid = #arguments.LocationID#
			</CFQUERY>
		</cftransaction>
		<cfreturn arguments.neworgID>
	</cffunction>


<!---
this function is used in the function below
because we are building a query outside of CFQUERY we can't rely on CF to do its clever stuff with excaping singlequotes
 --->

	<cffunction name="escapeSingleQuotes" output="no">
			<cfargument name="string">
			<cfreturn replaceNoCase(string,"'","''","ALL")>
	</cffunction>


<!---
	2007/10/03		WAB/NH	took this query out of orglistdrilldownv2 /qrysearchorgs
	2009/01/22		WAB Mods to the criteria in words to bring back a mini version for the left nav
--->

	<cffunction access="public" name="createDataSearchQueryFromFormVariables" returns="struct">
		<cfargument name="additionalJoinsNotRequiredForTheseTables" default="">  <!--- list of tables which the caller already has joins for --->
		<cfargument name="queryString" default=""> <!--- used if need to pass in the variables in the form of a queryString --->
		<!--- <cfargument name="showDeletedItems" default="#iif(isDefined('request.showDeletedOrgs') and request.showDeletedOrgs,true,false)#"> --->  <!--- NJH 2007/03/07 filter out records flagged for deletion , moved to this function 2008-04-29 WAB--->

		<!--- NJH 2008/05/30 added with (noLock) to the selects --->

		<cfset var result = structNew()>
		<cfset var showDeletedRecords = false>
		<cfset var getFlagTypes = "">
		<cfset var frmPersonFlagIDs = "">
		<cfset var frmLocationFlagIDs = "">
		<cfset var frmOrganisationFlagIDs = "">
		<cfset var queryStringStructure = {}>
		<cfset var tempCriteria = "">
		<cfset var field = "">
		<cfset var entity = "">
		<cfset var flagid = "">
		<cfset var getAccountManager = "">

		<cfset result.ok = true>
		<cfset result.error = "">
		<cfset result.hasPersonCriteria = false>
		<cfset result.hasLocationCriteria = false>
		<cfset result.hasOrganisationCriteria = false>
		<cfset result.fieldsUsed = structNew()>
		<cfset result.criteriaInWordsStruct = structNew()>
		<cfset result.criteriaInWordsMiniStruct = structNew()>
		<cfset result.CriteriaInWords = "">
		<cfset result.CriteriaInWordsMini = "">

		<cfif isDefined("frmDeleted") and isBoolean(frmDeleted) and frmDeleted>
			<cfset showDeletedRecords = true>
		</cfif>

		<!--- if we pass in the variables as a structure rather than in form or url  then this is a (hack) way of making them visible to this code --->
		<cfloop collection = #arguments# item="field">
			<cfif listFindNoCase("additionalJoinsNotRequiredForTheseTables,queryString",field) is 0>
				<cfset url[field] = arguments[field]>
			</cfif>
		</cfloop>

		<!--- WAB added 2009/01/21.
			Function had accepted the queryString parameter for a long time, but never been coded
			Note that have to delete these at the end of function (sometimes function called more tha once in a request)
		 --->
		<cfif queryString is not "">
				<cfset queryStringStructure = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = queryString, Delimiter = "&")>
				<cfloop collection = #queryStringStructure# item="field">
					<cfset url[field] = queryStringStructure[field]>
				</cfloop>
		</cfif>



		<cfif isDefined("frmFlagIDs") and frmFlagIDs is not 0>
			<cfloop index="entity" list="person,location,organisation">
				<CFQUERY NAME="getFlagTypes" DATASOURCE="#application.SiteDataSource#">
					select flagid
					from flaggroup fg with (noLock) inner join flag f with (noLock) on fg.flaggroupid = f.flaggroupid
					where
						flagid  in ( <cf_queryparam value="#frmFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						and
						entityTypeID =  <cf_queryparam value="#application.EntityTypeID[entity]#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfset "frm#entity#FlagIDs" = valuelist(getflagTypes.Flagid)>
			</cfloop>
		</cfif>


		<!--- Deal with submission from the left menu --->
		<cfif isDefined("searchFieldSelector") and isDefined("searchTextString")>
			<cfset url[searchFieldSelector] = searchTextString >
			<cfset url["frm#searchFieldSelector#"] = searchTextString > <!--- WAB 2008/07/15 for some backwards compatability during change of left menus search, bit nasty--->
		</cfif>

		<!--- build up the where first statement and work out whether there are any person criteria --->
		<cfsavecontent variable = "result.whereStatement_sql">
			<cfoutput>
				<!---<cfset result.hasLocationCriteria = true> NJH 2012/07/30 - due to Will's changes below, I set this to true so that the location table is included in the joins, as the rights clause expects
					to join to the location table. --->
			<!---
			WAB 2012-06-30	During P-SMA001
			Having found out roughly what countryScopeOrganisationRecords does, I can't see that it should affect searching.
			It should be possible (says Jenny) to find Locations in 'your' country, even if the organisation is in a country that you do not have rights to, but the way it is currently coded prevents this
			So I removed this if statement while implementing rights changes

			<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
				<!--- 11 July 2003 modified to show all orgs unless  countryScopeOrganisationRecords is set to 1 in content/CFTemplates/relayINI.cfm --->
			  	and o.countryID in (<cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)
			<cfelse>
				and (
					l.CountryID IN (<cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)
						<!--- WAB 2008/02/19  this line allows us to find empty orgs in relevant countries--->
						or (l.CountryID is null and o.CountryID IN (<cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">))
					)
			</cfif>
			--->

<!--- 2012-07-30 PPB SMA001 - I HAVE TEMPORARLY COMMENTED THIS OUT COS L.COUNTRYID IS NOT DEFINED WHEN SEARCH FOR AN ORG IN 3-PANE VIEW   --->
<!---
				and (


					 (1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location", alias = "l").whereClause#)

 						or

					 (l.CountryID is null #application.com.rights.getRightsFilterWhereClause(entityType="organisation", alias = "O").whereClause#)

					)
 --->

			<cfif not showDeletedRecords>
			and orgDeleted.flagID is null
			</cfif>


			<CFIF isDefined("frmFlagIDs") and frmFlagIDs IS NOT 0>
					<cfset result.fieldsUsed["frmFlagIDs"] = frmFlagIDs>
				AND
				(
		 		<CFIF frmOrganisationFlagIDs IS NOT "">
					<cfset result.hasOrganisationCriteria = true>
					o.ORGANISATIONid IN (select entityID from BooleanFlagData with (noLock) where FlagID IN (<cf_queryparam value="#frmOrganisationFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">))
						OR
				</cfif>
		 		<CFIF frmPersonFlagIDs IS NOT "">
					<cfset result.hasPersonCriteria = true>
					p.Personid
							IN(select entityID from BooleanFlagData with (noLock) where FlagID IN (<cf_queryparam value="#frmPersonFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">))
						OR
				</cfif>
		 		<CFIF frmLocationFlagIDs IS NOT "">
					<cfset result.hasLocationCriteria = true>
					l.Locationid
							IN(select entityID from BooleanFlagData with (noLock) where FlagID IN (<cf_queryparam value="#frmLocationFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">))
						OR
				</cfif>
						 (1=0)
				)
				<!--- criteria in words --->
				<cfif listLen(frmFlagIDs) is 1>
					<cfset result.criteriaInWordsStruct["frmFlagIDs"] = application.com.flag.getFlagStructure(frmFlagIDs).translatedName & " is set">
					<cfset result.criteriaInWordsMiniStruct["frmFlagIDs"] =  application.com.flag.getFlagStructure(frmFlagIDs).translatedName>
				<cfelse>
					<cfset tempCriteria = "">
					<cfloop index = "flagId" list = #frmFlagIDs#>
						<cfset tempCriteria = tempCriteria & iif (tempCriteria is not "", de(" or "), de("")) & application.com.flag.getFlagStructure(flagid).translatedName >
					</cfloop>
						<cfset result.criteriaInWordsMiniStruct["frmFlagIDs"] = tempCriteria>
						<cfset tempCriteria = tempCriteria & " are set">
						<cfset result.criteriaInWordsStruct["frmFlagIDs"] = tempCriteria>

				</cfif>

			</CFIF>

 			<CFIF isDefined("frmAccountMngrID") and frmAccountMngrID GT 0>
					<cfset result.hasLocationCriteria = true>
					<cfset result.hasOrganisationCriteria = true>
					<cfset result.fieldsUsed.frmAccountMngrID = frmAccountMngrID>  <!--- NJH 2008/05/02 Lexmark Bug 49 - was <cfset result.fieldsUsed["frmAccountMngrID"].value = frmAccountMngrID> --->
					<!--- Changes to inner join below
				AND o.ORGANISATIONid IN (select entityID from #application.com.flag.getFlagGroupStructure('AllocationSalesTechnical').FlagType.DataTableFullName# with (noLock)
					where data in (<cf_queryparam value="#frmAccountMngrID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">) and FlagID IN (
							select f.flagID from flagGroup fg with (noLock)
							inner join flag f with (noLock) on fg.flagGroupID=f.flagGroupID
							where flagGroupTextID = 'AllocationSalesTechnical')) --->
					and (ifd_org.entityID is not null or ifd_loc.entityID is not null)
					<!--- get Account Manager Name --->
					<CFQUERY NAME="getAccountManager" DATASOURCE="#application.SiteDataSource#">
					select firstName + ' ' + lastname as name from person with (noLock) where personid  in ( <cf_queryparam value="#frmAccountMngrID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</CFQUERY>
					<cfset result.criteriaInWordsStruct["frmAccountMngrID"] = "Account Manager is " & valueList(getAccountManager.name)>
					<cfset result.criteriaInWordsMiniStruct["frmAccountMngrID"] = "AM: " & valueList(getAccountManager.name)>


			</CFIF>
			<!--- JIRA PROD2016-342 NJH 2016/06/09 - add location country in search. Also allow searching for multiple countries --->
			<CFIF isDefined("frmCountryID") and frmCountryID IS NOT 0>
					<cfset result.hasCountryCriteria = true>
					<cfset result.hasLocationCriteria = true>
					<cfset result.fieldsUsed["frmCountryID"] = frmCountryID>
			 	AND (o.CountryID in (<cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">) or l.countryID in (<cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">))
			</CFIF>
			<CFIF isDefined("frmFax") and frmFax IS NOT "">
				<cfset result.fieldsUsed["frmFax"] = frmFax>
				<cfset result.hasLocationCriteria = true>
			 	AND ltrim(rtrim(fax)) like <cf_queryparam value="#trim(frmFax)#" CFSQLTYPE="CF_SQL_VARCHAR">
			</CFIF>
			<CFIF isDefined("frmEmail") and frmEmail IS NOT "">
				<cfset result.fieldsUsed["frmEmail"] = frmEmail>
				<cfset result.hasPersonCriteria = true>
			 	AND ltrim(rtrim(Email)) like <cf_queryparam value="#trim(frmEmail)#%" CFSQLTYPE="CF_SQL_VARCHAR"> <!--- NJH 2009/09/25 Bug Fix Security Project Issue 2236 - remove any single quotes that might be used for sql injection as sql injection checker isn't catching this on the internal site yet. --->
			</CFIF>
			<CFIF isDefined("frmAddress") and frmAddress IS NOT "">
				<cfset result.fieldsUsed["frmAddress"] = frmAddress>
				<cfset result.hasLocationCriteria = true>
		    	AND (ltrim(rtrim(Address3)) like <cf_queryparam value="#trim(frmAddress)#%" CFSQLTYPE="CF_SQL_VARCHAR"> or ltrim(rtrim(Address4)) like <cf_queryparam value="#trim(frmAddress)#%" CFSQLTYPE="CF_SQL_VARCHAR"> or ltrim(rtrim(Address5)) like <cf_queryparam value="#trim(frmAddress)#%" CFSQLTYPE="CF_SQL_VARCHAR">)
			</CFIF>
			<CFIF isDefined("frmPostalCode") and frmPostalCode IS NOT "">
				<cfset result.fieldsUsed["frmPostalCode"] =frmPostalCode >
				<cfset result.hasLocationCriteria = true>
			 	AND PostalCode like <cf_queryparam value="#trim(frmPostalCode)#%" CFSQLTYPE="CF_SQL_VARCHAR">
			</CFIF>
			<CFIF isDefined("frmFirstName") and frmFirstName IS NOT "">
				<cfset result.fieldsUsed["frmFirstName"] = frmFirstName>
				<cfset result.hasPersonCriteria = true>
			 	AND ltrim(rtrim(firstName)) like <cf_queryparam value="#trim(frmFirstName)#%" CFSQLTYPE="CF_SQL_VARCHAR">
			</CFIF>
			<CFIF isDefined("frmLastName") and frmLastName IS NOT "">
				<cfset result.fieldsUsed["frmLastName"] = frmLastName>
				<cfset result.hasPersonCriteria = true>
			 	AND ltrim(rtrim(LastName)) like <cf_queryparam value="#trim(frmLastName)#%" CFSQLTYPE="CF_SQL_VARCHAR">
			</CFIF>
			<!--- WAB 2008/02/20 added full name search to make search from left menu work if first name has a space in it --->
			<CFIF isDefined("frmFullName") and frmFullName IS NOT "">
				<cfset result.fieldsUsed["frmFullName"] = frmFullName>
				<cfset result.hasPersonCriteria = true>
			 	AND ltrim(rtrim(firstName)) + ' ' + ltrim(rtrim(LastName)) like <cf_queryparam value="#trim(frmFullName)#%" CFSQLTYPE="CF_SQL_VARCHAR">
			</CFIF>

			<CFIF isDefined("frmTelephone") and frmTelephone IS NOT "">
				<cfset result.fieldsUsed["frmTelephone"] = frmTelephone>
				<cfset result.hasLocationCriteria = true>
				<cfset result.hasPersonCriteria = true>
				<!--- and (dbo.CleanChars(Telephone,'0-9') like '#frmTelephone#' or dbo.CleanChars(Fax,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or dbo.CleanChars(officePhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or dbo.CleanChars(mobilePhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or dbo.CleanChars(homePhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or dbo.CleanChars(faxPhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or dbo.CleanChars(homeFax,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > ) --->
				and ((isNull(telephone,'') <> '' and dbo.CleanChars(Telephone,'0-9') like <cf_queryparam value="#trim(frmTelephone)#" CFSQLTYPE="CF_SQL_VARCHAR">)
					 or (isNull(Fax,'') <> '' and dbo.CleanChars(Fax,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 or (isNull(officePhone,'') <> '' and dbo.CleanChars(officePhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 or (isNull(mobilePhone,'') <> '' and dbo.CleanChars(mobilePhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 or (isNull(homePhone,'') <> '' and dbo.CleanChars(homePhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 or (isNull(faxPhone,'') <> '' and dbo.CleanChars(faxPhone,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 or (isNull(homeFax,'') <> '' and dbo.CleanChars(homeFax,'0-9')  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
			</CFIF>
			<CFIF isDefined("frmsrchpersonID") and frmsrchpersonID IS NOT "">
				<cfset result.fieldsUsed["frmsrchpersonID"] =frmsrchpersonID >
				<cfset result.hasPersonCriteria = true>
				AND p.personID IN (<cf_queryparam value="#frmsrchpersonID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
			</CFIF>
			<CFIF isDefined("frmsrchlocationID") and frmsrchlocationID IS NOT "">
				<cfset result.fieldsUsed["frmsrchlocationID"] = frmsrchlocationID>
				<cfset result.hasLocationCriteria = true>
			 	AND l.locationID in (<cf_queryparam value="#frmsrchlocationID#" CFSQLTYPE="CF_SQL_INTEGER" list="true" >)
			</CFIF>
			<CFIF isDefined("frmNoContacts") and  frmNoContacts IS NOT 0>
				<cfset result.hasOrganisationCriteria = true>
				<cfset result.fieldsUsed["frmNoContacts"] = frmNoContacts>
			 	AND o.ORGANISATIONid IN (select distinct organisationid
					from organisation with (noLock) where organisationid not in (select
					distinct organisationid from person with (noLock)))
			</CFIF>
			<CFIF isDefined("frmsrchOrgID") and frmsrchOrgID is not "" and frmsrchOrgID is not "0">   <!--- WAB 2000-10-13 added this line, sometimes being called with this variable set  and bringing back whole database --->
				<cfif not isStringNumericOrNumericList(frmsrchOrgID)>
					<cfset result.ok = false>
					<cfset result.error = "OrganisationID must be Numeric or List">
				<cfelse>
					<cfset result.hasOrganisationCriteria = true>
					<cfset result.fieldsUsed["frmsrchOrgID"] =frmsrchOrgID >
				 	AND o.organisationID  in ( <cf_queryparam value="#frmsrchOrgID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  <!--- WAb 2008/02/19 changed to IN --->
					</cfif>

			</CFIF>
			<!--- JIRA PROD2016-342 NJH 2016/06/09 - search for multiple organisation types. Also add location accountTypes in search --->
			<cfif isDefined("frmorganisationTypeID") and isNumeric(frmorganisationTypeID) and frmOrganisationTypeID IS NOT 0 ><!--- SSS 2007-06-14 Organsation is passed as 0 not "" --->
				<cfset result.fieldsUsed["frmorganisationTypeID"] = frmorganisationTypeID>
				<cfset result.criteriaInWordsStruct["frmorganisationTypeID"] = "Org Type: #application.organisationType[frmorganisationTypeID].name#">
				<cfset result.criteriaInWordsMiniStruct["frmorganisationTypeID"] = "">	<!---  do show org type in mini--->
				AND (o.organisationTypeID in (<cf_queryparam value="#frmorganisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">) or l.accountTypeID in (<cf_queryparam value="#frmorganisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">))
			</cfif>
			<cfif isDefined("frmCommID") and frmCommID IS NOT "0">
				<cfset result.hasOrganisationCriteria = true>
				<cfset result.fieldsUsed["frmCommID"] = frmCommID>
			 	AND commID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			<!--- JIRA PROD2016-342 NJH 2016/06/09 - add location urls in search --->
			<cfif isDefined("frmOrgURL") and frmOrgURL is not "">  <!--- NJH 2007/10/03 search by org url --->
				<cfset result.hasOrganisationCriteria = true>
				<cfset result.hasLocationCriteria = true>
				<cfset result.fieldsUsed["frmOrgURL"] =frmOrgURL >
				AND (orgURL  like  <cf_queryparam value="#frmOrgURL#%" CFSQLTYPE="CF_SQL_VARCHAR" > or l.specificURL like <cf_queryparam value="#frmOrgURL#%" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfif>

			<cfif isDefined("frmLastContactDate") and frmLastContactDate is not "">  <!--- PKP: 2008/02/08 search by last contact date --->
				<cfset result.fieldsUsed["frmLastContactDate"] = frmLastContactDate>
				<cfset result.haslocationCriteria = true>
				<!--- NJH 2008/06/23 Bug Fix Lexmark Issue 42 - added brackets for the 'or' statement --->
				and (cd.DateSent is NULL
				or cd.DateSent <=  <cf_queryparam value="#dateformat(frmLastContactDate,"yyyy-mm-dd")# #timeformat(frmLastContactDate,"hh:mm:ss")#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
			</cfif>

			<CFIF isDefined("frmSiteName") and Trim(frmSiteName) IS NOT "">
				<cfset result.fieldsUsed["frmSiteName"] =frmSiteName >
				<cfset result.hasLocationCriteria = true>
				<cfset result.hasOrganisationCriteria = true>
				<CFIF isDefined("frmLooseMatch") and frmLooseMatch IS NOT 1>
					<cfset result.fieldsUsed["frmLooseMatch"] = frmLooseMatch>
					AND (DIFFERENCE(OrganisationName, N'#escapeSingleQuotes(frmSiteName)#') >= 4
						or difference(siteName,N'#escapeSingleQuotes(frmSiteName)#') >= 4
					)
				<CFELSE>
					<cfset var trimSiteName = trim(frmSiteName)>
					<cfif len(trimSiteName) is 1>
						AND (organisationName like N'#trimSiteName#%'
							or aka like N'#trimSiteName#%'
							or siteName like N'#trimSiteName#%' <!--- NJH 2008/02/08 Bug Fix All sites 10: Advanced search for Organisation/site name not finding sites --->
						)
					<cfelse>
						<cfif isDefined("frmNext") and frmNext eq "dataSearch">
						<!--- 2005-04-13 SWJ after investigating I found this search adds 5 seconds to each search so I've changed it to only execute when using advanced search  --->
						AND (replace(replace(replace(organisationName,' ',''),'.',''),',','')
								like replace(replace(replace(N'%#escapeSingleQuotes(trimSiteName)#%',' ',''),'.',''),',','')
						or replace(replace(replace(aka,' ',''),'.',''),',','')
								like replace(replace(replace(N'%#escapeSingleQuotes(trimSiteName)#%',' ',''),'.',''),',','')
						or replace(replace(replace(siteName,' ',''),'.',''),',','')
								like replace(replace(replace(N'%#escapeSingleQuotes(trimSiteName)#%',' ',''),'.',''),',','') <!--- NJH 2008/02/08 Bug Fix All sites 10: Advanced search for Organisation/site name not finding sites --->
							)
						<cfelse>
							AND (organisationName like replace(replace(replace(N'%#escapeSingleQuotes(trimSiteName)#%',' ','%'),'.','%'),',','%')
								or aka like replace(replace(replace(N'%#escapeSingleQuotes(trimSiteName)#%',' ','%'),'.','%'),',','%')
								or siteName like replace(replace(replace(N'%#escapeSingleQuotes(trimSiteName)#%',' ','%'),'.','%'),',','%') <!--- NJH 2008/02/08 Bug Fix All sites 10: Advanced search for Organisation/site name not finding sites --->
								)

						</cfif>
					</cfif>
					<!--- AND (OrganisationName like '#trimSiteName#%' or aka like '#trimSiteName#%') --->
				</CFIF>
			 </CFIF>

			 <!--- 2011/06/30			NAS			P-LEX053 - Add VAT Search --->
			 <cfif isDefined("frmVAT") and frmVAT is not "">
				<cfset result.hasOrganisationCriteria = true>
				<cfset result.fieldsUsed["frmVAT"] =frmVAT >
				AND (
					o.VatNumber  like  <cf_queryparam value="#trim(frmVAT)#%" CFSQLTYPE="CF_SQL_VARCHAR" >
					)
			</cfif>

			<CFIF isDefined("frmSelectionID") and isNumeric(frmSelectionID) and frmSelectionID IS NOT 0>
				<cfset result.fieldsUsed["frmSelectionID"] = frmSelectionID>
				<cfset result.hasPersonCriteria = true>
					and st.SelectionID  in ( <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFIF>

			<!--- <cfif isDefined("request.showDeletedOrgs") and request.showDeletedOrgs>  <!--- NJH 2007/03/07 filter out records flagged for deletion --->
				and deleted = 0
			</cfif> --->

			<cfif isDefined("frmFlagIDfromFlagCount") and frmFlagIDfromFlagCount neq "0" and isDefined("frmEntityType") and frmEntityType neq "0" and isDefined("frmdataTableFullName") and frmdataTableFullName neq "0" >
			<!--- SWJ: 2002-03-02 this section is designed to deal with queries coming from FlagGroupCounts.cfm report
					whcih can pass any flag entity Type to this template.  Currently we only support orgs, locs and people --->
				<CFIF frmentityType eq "organisation">
					<cfset result.hasOrganisationCriteria = true>
					AND o.organisationID IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
				<cfelseif frmentityType eq "Person">
					and p.Personid IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
					<cfset result.hasPersonCriteria = true>
				<cfelseif frmentityType eq "Location">
					AND l.Locationid IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
					<cfset result.hasLocationCriteria = true>
				</CFIF>
			</cfif>

			<cfif not showDeletedRecords and ((result.hasLocationCriteria or result.hasPersonCriteria
				or not application.com.settings.getSetting("plo.countryScopeOrganisationRecords"))
				and listFindNoCase(additionalJoinsNotRequiredForTheseTables,"location") is 0)
			>
				and locDeleted.entityID is null
			</cfif>
			<cfif not showDeletedRecords and (result.hasPersonCriteria and listFindNoCase(additionalJoinsNotRequiredForTheseTables,"person") is 0)>
				and perDeleted.entityID is null
			</cfif>

			</cfoutput>

		</cfsavecontent>


		<!--- creating the joins needed --->
		<cfsavecontent variable = "result.additionaljoins_sql">
			<!--- WAB 2008/02/19 changed these to left joins so that can find empty organisations --->
			<cfif (result.hasLocationCriteria or result.hasPersonCriteria
				or not application.com.settings.getSetting("plo.countryScopeOrganisationRecords"))
				and listFindNoCase(additionalJoinsNotRequiredForTheseTables,"location") is 0
			>
				<!--- note that if request .country ScopeOrganisationRecords is not set then we always need to filter by location country so have to add location in, even though we don't actually strictly speaking have location criteria--->
			left join location l with (noLock) on O.organisationid = l.organisationid
				<cfif not showDeletedRecords>
					left join booleanFlagData as locDeleted with (noLock) on locDeleted.entityID = l.locationID and locDeleted.flagID = <cfoutput>#application.com.flag.getFlagStructure(flagID="DeleteLocation").flagID#</cfoutput>
				</cfif>
			</cfif>

			<cfif result.hasPersonCriteria 	and listFindNoCase(additionalJoinsNotRequiredForTheseTables,"person") is 0>
			left  join person p with (noLock) on l.locationid = p.locationid
				<cfif not showDeletedRecords>
					left join booleanFlagData as perDeleted with (noLock) on perDeleted.entityID = p.personID and perDeleted.flagID = <cfoutput>#application.com.flag.getFlagStructure(flagID="DeletePerson").flagID#</cfoutput>
				</cfif>
			</cfif>

			<cfif not showDeletedRecords>
			left join booleanflagdata orgDeleted with (noLock) on orgDeleted.flagid = <cfoutput>#application.com.flag.getFlagStructure("deleteOrganisation").flagid#</cfoutput> and orgDeleted.entityID = o.organisationid
			</cfif>


			<CFIF isDefined("frmSelectionID") and isNumeric(frmSelectionID) and frmSelectionID IS NOT 0>
			INNER JOIN selectionTag as st with (noLock) on status<>0 and p.personid = st.entityID
	 		</CFIF>

			<cfif isdefined("frmLastContactDate") and frmLastContactDate is not ''>
				inner join commdetail cd with (noLock) on l.locationID = cd.locationID
			</cfif>

			<cfif isDefined("frmCommID") and frmCommID IS NOT "0">
				inner join phoneAction pa with (noLock) on pa.organisationID = o.organisationID
			</cfif>

			<CFIF isDefined("frmAccountMngrID") and frmAccountMngrID GT 0>
				left join (integerFlagData ifd_org inner join flag f_org on f_org.flagID = ifd_org.flagID and f_org.flagTextID='accManager') on ifd_org.entityID = o.organisationID and ifd_org.data in (<cf_queryparam value="#frmAccountMngrID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)
				left join (integerFlagData ifd_loc inner join flag f_loc on f_loc.flagID = ifd_loc.flagID and f_loc.flagTextID='locAccManager') on ifd_loc.entityID = l.locationID and ifd_loc.data in (<cf_queryparam value="#frmAccountMngrID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)
			</CFIF>

		</cfsavecontent>


		<!--- clear up temporary URL variables --->
		<cfif queryString is not "">
				<cfloop collection = #queryStringStructure# item="field">
					<cfset structDelete (url,field)>
				</cfloop>
		</cfif>



		<cfset result.fieldsUsedQueryString = application.com.structureFunctions.convertStructureToNameValuePairs (struct = result.fieldsUsed,delimiter = "&")>

		<!--- just a hack at the moment to build up a criteria string , need to think about and maybe do similar to selections --->
		<cfloop item =  "field" collection = #result.fieldsUsed#>
			<cfif not structKeyExists (result.criteriaInWordsStruct, field)>
				<cfset result.criteriaInWordsStruct[field] = "phr_search_#field# = #result.fieldsUsed[field]#">
			</cfif>
			<cfif not 	structKeyExists (result.criteriaInWordsMiniStruct, field)>
				<cfset result.criteriaInWordsMiniStruct[field] = result.fieldsUsed[field]>
			</cfif>
		</cfloop>
		<cfloop item =  "field" collection = #result.fieldsUsed#>
				<cfset result.CriteriaInWords = listappend (result.CriteriaInWords,result.criteriaInWordsStruct[field],'|')>
			<cfif result.criteriaInWordsMiniStruct[field] is not ""><cfif result.CriteriaInWordsMini is not ""><cfset result.CriteriaInWordsMini = result.CriteriaInWordsMini & "; "></cfif><cfset result.CriteriaInWordsMini = result.CriteriaInWordsMini & result.criteriaInWordsMiniStruct[field]></cfif> <!--- rather nasty ifs to deal with possible blank item plus wanting to delimit with ; + space --->
		</cfloop>
		<cfset result.CriteriaInWords = replaceNocase (result.CriteriaInWords , "|","<BR>","ALL")>

		<cfreturn result>


	</cffunction>

	<cffunction name="isStringNumericOrNumericList" >
		<cfargument name="string" required="yes">

		<cfset var item = "">

		<cfloop list="#string#" index="item">
			<cfif not isNumeric(Item)><cfreturn false></cfif>
		</cfloop>

		<cfreturn true>
	</cffunction>


	<!--- WAB 2008-04-25
	Spend my life writing same query to find out things about an entity such as its organisationid, countryid etc
	This is a generic POL function which returns the salient details of an entity and its parents.
	Based to some extent on customTags\getRecord and code in webservices\entityNavigation.cfc
	At its simplest just gives the parent entityIDs, names and countries
	but can get whole records as well
	 --->

	<!--- NYB REL106 moved contents to RelayEntity --->
	<cffunction name="getEntityStructure">
		<cfargument name="entityTypeID" type="numeric" required="yes">
		<cfargument name="entityID" type="numeric" required="yes">
		<cfargument name="getEntityQueries" type="boolean" default="no">

		<cfset var result = structNew()>
		<cfset result = application.com.RelayEntity.getEntityStructure(argumentCollection=arguments)>

		<cfreturn result>
	</cffunction>


	<cffunction name="emailAddressExists" >
		<cfargument name="emailAddress" required="yes">

		<cfset var getEmailAddress = "">

		<cfquery name="getEmailAddress" datasource="#application.SiteDataSource#">
			SELECT personId FROM person WHERE email =  <cf_queryparam value="#emailAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn (getEmailAddress.recordcount gt 0)>
	</cffunction>

	<!---

this bit of code cut out of orglist,drilldownV3.  Wasn't doing anything there, but may need to be ressurected
<!--- If the selection is a search criteria, then we need to limit
	the locations to the owner's countries as well
--->
<CFIF frmSelectionID IS NOT 0>

	<CFQUERY NAME="getOwnerCountries" DATASOURCE="#application.SiteDataSource#">
		SELECT r.CountryID
		  FROM Rights AS r,
			   RightsGroup AS rg,
			   Selection AS s,
			   UserGroup AS ug
		 WHERE s.SelectionID = #frmSelectionID#
		   AND rg.PersonID = ug.PersonID
		   AND ug.UserGroupID = s.CreatedBy
		   AND rg.UserGroupID = r.UserGroupID
		   AND r.Permission > 0
		   AND r.SecurityTypeID =
					(SELECT e.SecurityTypeID
					 FROM SecurityType AS e
					 WHERE e.ShortName = 'RecordTask')
		</CFQUERY>

		<CFSET OwnerCountries = 0>

		<CFIF getOwnerCountries.RecordCount GT 0>
			<CFIF getOwnerCountries.CountryID IS NOT "">
				<!--- top record (or only record) is not null --->
				<CFSET OwnerCountries = ValueList(getOwnerCountries.CountryID)>
			</CFIF>
		</CFIF>
</CFIF>


	 --->

	<!--- NJH 2010/06/02 - commented this out as it has been moved to relayEntity.cfc
	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="updateEntityDetails" access="public" hint="Updates an entity's details" returntype="struct">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" required="false">
		<cfargument name="table" type="string" required="false">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="treatEmtpyFieldAsNull" type="boolean" required="false" default="false">

		<cfscript>
			var updateDetails = "";
			var fieldCount = 0;
			var result = structNew();
			var fieldValue = "";
			var column = "";
			var field = "";
			var firstRun = true;
			var thisNullValue = "''";

			var preparedEntityStruct = prepareEntityStruct(method="update",argumentCollection=arguments);
			var entityDetailsStruct = preparedEntityStruct.entityDetailsStruct;
			var tableDetailsStruct = preparedEntityStruct.tableDetailsStruct;

			result.message = preparedEntityStruct.message;
			result.isOk = preparedEntityStruct.isOK;
		</cfscript>

		<cfif not result.isOk>
			<cfreturn result>
		</cfif>

		<!--- matchname is currently not updated automatically!! --->
		<cfquery name="updateDetails" datasource="#application.siteDataSource#">
			update #preparedEntityStruct.tablename#
			set
				<cfloop collection="#entityDetailsStruct#" item="field">
					<cfset fieldCount = fieldCount+1>
					<cfset fieldValue = entityDetailsStruct[field]>
					#field# =
					<cfif len(fieldValue) eq 0 and arguments.treatEmtpyFieldAsNull and tableDetailsStruct[field].isNullable>
						null
					<cfelseif len(fieldValue) eq 0>
						<cfswitch expression="#tableDetailsStruct[field].data_type#">
							<cfcase value="bit,int,float,smallint,numeric">0</cfcase>
							<cfdefaultcase>''</cfdefaultcase>
						</cfswitch>
					<cfelseif len(fieldValue) gt 0>
						<cfswitch expression="#tableDetailsStruct[field].data_type#">
							<cfcase value="nvarchar,ntext">N'#fieldValue#'</cfcase>
							<cfcase value="datetime">#createODBCDateTime(fieldValue)#</cfcase>
							<cfcase value="bit,int,float,smallint,numeric">#fieldValue#</cfcase>
							<cfdefaultcase>'#fieldValue#'</cfdefaultcase>
						</cfswitch>
					</cfif>

					<cfif fieldCount lt structCount(entityDetailsStruct)>,</cfif>
				</cfloop>
			where #preparedEntityStruct.uniqueKey# = #arguments.entityID#
				and (
				<cfloop collection="#entityDetailsStruct#" item="field">
					<!--- don't check if updated dates are different --->
					<cfif not listFindNoCase(preparedEntityStruct.crudColumns,field)>
						<cfif not firstRun>
							or
						</cfif>
						<cfset fieldValue = entityDetailsStruct[field]>
						<cfswitch expression="#tableDetailsStruct[field].data_type#">
							<cfcase value="bit,int,float,smallint,numeric"><cfset thisNullValue = 0></cfcase>
							<cfdefaultcase><cfset thisNullValue = "''"></cfdefaultcase>
						</cfswitch>
						isNull(#field#,#thisNullValue#) <>
							<cfif len(fieldValue) eq 0 and arguments.treatEmtpyFieldAsNull and tableDetailsStruct[field].isNullable>
								null
							<cfelseif len(fieldValue) eq 0>
								#thisNullValue#
							<cfelseif len(fieldValue) gt 0>
								<cfswitch expression="#tableDetailsStruct[field].data_type#">
									<cfcase value="nvarchar,ntext">N'#fieldValue#'</cfcase>
									<cfcase value="datetime">#createODBCDateTime(fieldValue)#</cfcase>
									<cfcase value="bit,int,float,smallint,numeric">#fieldValue#</cfcase>
									<cfdefaultcase>'#fieldValue#'</cfdefaultcase>
								</cfswitch>
							</cfif>
							<cfset firstRun = false>
					</cfif>
				</cfloop>
				)
		</cfquery>

		<cfreturn result>

	</cffunction>
	--->

	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="updatePersonDetails" access="public" returntype="struct" hint="Updates a persons details">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="personDetails" type="struct" required="true">

		<cfreturn application.com.relayEntity.updatePersonDetails(argumentCollection=arguments)>

	</cffunction>


	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="updateLocationDetails" access="public" returntype="struct" hint="Updates a locations details">
		<cfargument name="locationID" type="numeric" required="true">
		<cfargument name="locationDetails" type="struct" required="true">

		<cfreturn application.com.relayEntity.updateLocationDetails(argumentCollection=arguments)>

	</cffunction>



	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="updateOrganisationDetails" access="public" returntype="struct" hint="Updates an organisation's details">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="organisationDetails" type="struct" required="true">

		<cfreturn application.com.relayEntity.updateOrganisationDetails(argumentCollection=arguments)>

	</cffunction>


	<cffunction name="updateOpportunityDetails" access="public" returntype="struct" hint="Updates an opportunity's details">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="opportunityDetails" type="struct" required="true">

		<cfreturn application.com.relayEntity.updateEntityDetails(entityID=arguments.opportunityID,entityTypeID=application.entityTypeID["opportunity"],entityDetails=arguments.opportunityDetails)>

	</cffunction>


	<!--- NJH 2009/05/27 P-SNY063 --->
	<cffunction name="insertPerson" access="public" hint="Insert a person record" returntype="struct">
		<cfargument name="personDetails" type="struct" required="true">
		<cfargument name="setPassword" type="boolean" default="false">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="matchPersonColumns" type="string" default="firstname,lastname,email,locationID">

		<cfreturn application.com.relayEntity.insertPerson(argumentCollection=arguments)>
	</cffunction>



	<!--- NJH 2009/05/27 P-SNY063 --->
	<cffunction name="insertLocation" access="public" hint="Insert a location record" returntype="struct">
		<cfargument name="locationDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="updateMatchName" type="boolean" default="true">
		<cfargument name="matchLocationColumns" type="string" default="sitename,address1,address4,postalCode,organisationID,countryID">

		<cfreturn application.com.relayEntity.insertLocation(argumentCollection=arguments)>
	</cffunction>



	<!--- NJH 2009/05/27 P-SNY063 --->
	<cffunction name="insertOrganisation" access="public" hint="Insert an organisation record" returntype="struct">
		<cfargument name="organisationDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="updateMatchName" type="boolean" default="true">
		<cfargument name="matchOrganisationColumns" type="string" default="organisationName,countryID">

		<cfreturn application.com.relayEntity.insertOrganisation(argumentCollection=arguments)>
	</cffunction>



	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="doesPersonExist" access="public" returntype="struct">
		<cfargument name="personDetails" type="struct" required="true">
		<cfargument name="columnsToUse" type="string" default="firstname,lastname,email,locationID">

		<cfreturn application.com.relayEntity.doesPersonExist(argumentCollection=arguments)>
	</cffunction>


	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="doesLocationExist" access="public" returntype="struct">
		<cfargument name="locationDetails" type="struct" required="true">
		<cfargument name="columnsToUse" type="string" default="sitename,address1,address4,postalCode,organisationID,countryID">

		<cfreturn application.com.relayEntity.doesLocationExist(argumentCollection=arguments)>
	</cffunction>


	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="doesOrganisationExist" access="public" returntype="struct">
		<cfargument name="organisationDetails" type="struct" required="true">
		<cfargument name="columnsToUse" type="string" default="organisationName,countryID">

		<cfreturn application.com.relayEntity.doesOrganisationExist(argumentCollection=arguments)>
	</cffunction>



	<!--- NJH 2009/05/22 P-SNY063 --->
	<cffunction name="deleteEntityOverride" access="public" returntype="boolean" hint="Should NOT be used for standard deletions, but only by processes where third party systems dictate that an entity must be deleted">
		<cfargument name="entityID" type="numeric">
		<cfargument name="entityTypeID" type="numeric">
		<cfargument name="harddelete" type="boolean" default="true">

		<cfreturn application.com.relayEntity.deleteEntityOverride(argumentCollection=arguments).entityDeleted>
	</cffunction>

	<!--- NAS 2010/04/22 P-PAN002 --->
	<cffunction name="GetLocationOrg" access="public" returntype="query">
		<cfargument name="locationID" type="numeric" required="true">

		<cfset var GetLocationOrg = "">

		<!--- Get The Location OrgID --->
		<!--- 2014-11-25	RPW		CORE-97 Phase 1: Extend database fields for Leads - Returned countryID aswell --->
		<cfquery name="GetLocationOrg" datasource="#application.sitedatasource#">
			select organisationID,countryID
			from location
			where locationID = #arguments.locationID#
		</cfquery>

		<cfreturn GetLocationOrg>

	</cffunction>

	<!--- NAS 2010/05/25 P-CLS002 --->
	<cffunction name="getEntityByForeignKey" access="public" returntype="query">
		<cfargument name="Entity" 		type="string" required="true">
		<cfargument name="ColumnName" 	type="string" required="true">
		<cfargument name="ColumnData" 	type="string" required="true">

		<cfset var getEntity=application.com.RelayEntity.getEntityByForeignKey(argumentcollection=arguments)>

		<cfreturn getEntity>

	</cffunction>

	<!--- NAS 2010/05/25 P-CLS002 --->
	<cffunction name="getPersonByName" access="public" returntype="query">
		<cfargument name="Name" 		type="string" required="true">
		<cfargument name="Organisation" 	type="string" required="true">

		<cfset var getPersonID="">

		<cfquery name="getPersonID" datasource="#application.sitedatasource#">
			select top 1 personID
			from person
			where ltrim(rtrim(firstName)) + ' ' + ltrim(rtrim(LastName))  like  <cf_queryparam value="#trim(arguments.Name)#%" CFSQLTYPE="CF_SQL_VARCHAR" >
			and organisationID in (select organisationid from organisation Where organisationname  like  <cf_queryparam value="#arguments.organisation#%" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>

		<cfreturn getPersonID>

	</cffunction>


	<cffunction access="public" name="getOrganisationType" hint="Returns the type of organisation" returntype="string">
		<cfargument name="OrganisationID" type="numeric" required="yes">

		<cfset var qryOrganisationType = "">

		<cfquery name="qryOrganisationType" datasource="#application.siteDataSource#">
			SELECT ot.TypeTextId
				FROM organisation o INNER JOIN
			        OrganisationType ot ON ot.OrganisationTypeId = o.OrganisationTypeId
				WHERE o.organisationID = #arguments.OrganisationID#
		  </cfquery>

		<cfreturn qryOrganisationType.TypeTextId>
	</cffunction>

	<cffunction name="isUniqueEmailValidationOn" access="public" returnType="struct">

		<cfset var uniqueEmailStruct = {isOn=false,uniqueEmailMessage="phr_sys_formValidation_UniqueEmailRequired"}>

		<cfif application.com.settings.getSetting("plo.uniqueEmailValidation")>
			<cfset uniqueEmailStruct.isOn = true>
			<cfset var uniqueEmailFieldStruct = application.com.relayEntity.getTableFieldStructure(tablename="person",fieldname="email")>
			<cfif structKeyExists(uniqueEmailFieldStruct.cfFormParameters,"uniqueEmailMessage") and uniqueEmailFieldStruct.cfFormParameters.uniqueEmailMessage neq "">
				<cfset uniqueEmailStruct.uniqueEmailMessage = uniqueEmailFieldStruct.cfFormParameters.uniqueEmailMessage>
			</cfif>
		</cfif>

		<cfreturn uniqueEmailStruct>
	</cffunction>


	<cffunction name="isEmailAddressUnique" access="public" returnType="struct">
		<cfargument name="emailAddress" type="string" required="yes">
		<cfargument name="personid" type="numeric" required="yes"><!--- pass in 0 for a new record --->
		<cfargument name="orgType" type="string" default="reseller"> <!--- NJH 2010/09/21 --->

		<cfset var orgTypeTextID = arguments.orgType>
		<cfset var result = {isOK=true,feedback=""}>
		<cfset var getEmailAddress = "">
		<cfset var getPersonAccountType = "">
		<cfset var accountType = arguments.orgType>

		<!--- START 2014-12-08 PPB ABS001 rejigged the conditions to avoid orgTypeTextID is undefined error when referencing organisationType[orgTypeTextID] --->
		<cfif arguments.personID neq 0 and isnumeric(arguments.personID)>
			<cfquery name="getPersonAccountType" datasource="#application.siteDataSource#">
				select accountTypeID
				from location l with (noLock)
					inner join person p with (noLock) on l.locationID = p.locationID
				where p.personID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfset accountType = getPersonAccountType.accountTypeID>
        </cfif>
        <!--- END 2014-12-08 PPB ABS001 --->

        <!--- START: 2015-10-13 DAN Case 446180 --->
		<cfif not isNumeric(orgTypeTextID) OR not isNumeric(accountType)>
			<cfset accountType = application.organisationType[orgTypeTextID].organisationTypeID>		<!--- CASE 426500 NJH 2012/03/08 - val'd the orgTypeTextId because it could be 1.0 (from the webservice) ; note: val() removed for PartnerCloud but don't know if deliberate --->
		</cfif>
        <!--- END: 2015-10-13 DAN Case 446180 --->

		<!---  if emailAddress is blank then return true - if email is blank then that will be dealt with by 'required' code if necessary--->
		<cfif trim(arguments.emailAddress) is not "" and orgTypeTextID neq "EndCustomer">

			<cfset var excludedEmailsFromUniqueValidation = application.com.settings.getSetting("plo.excludedEmailsFromUniqueValidation")>
			<cfif not listFindNoCase(excludedEmailsFromUniqueValidation,arguments.emailAddress)>

				<!--- now check for email already in database --->
				<cfquery name="getEmailAddress" datasource="#application.SiteDataSource#">
					declare @personEmail as personEmailTableType

					insert into @personEmail (personID, email,accountTypeID)
					select <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.emailAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#accountType#" CFSQLTYPE="CF_SQL_INTEGER">

					select 1 from dbo.getNonUniqueEmails(@personEmail)
				</cfquery>

				<cfif getEmailAddress.recordcount gt 0>
					<cfset result.isOK = 0>
					<cfset result.feedback = "#emailAddress# already Exists">  <!--- needs to be a phrase --->
				<cfelse>
					<!--- could now do other tests --->
				</cfif>
			</cfif>
		</cfif>
		<cfreturn result>
	</cffunction>



	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	       2013-02-20 YMA Format Address for string display
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="formatAddress" hint="Returns a formatted address for string display" output="false">
		<cfargument name="locationID" type="numeric" required="false">
		<cfargument name="showAddressOnly" type="boolean" default="false">
		<cfargument name="location" type="any" required="false" hint="Pass in a arguments.location structure or query containing all the fields needed rather than arguments.locationID. Saves running a query">
		
		<cfif not structKeyExists (arguments,"Location") and not structKeyExists (arguments,"LocationID")>
			<cfthrow detail="formatAddress function requires either Location or LocationID argument">
		</cfif>

		<cfscript>
			var formattedAddress = "";
		</cfscript>

		<cfif not structKeyExists (arguments,"Location")>
			<cfquery name="arguments.location" datasource="#application.siteDataSource#">
				select * from location
				INNER JOIN Country ON Location.CountryID = Country.CountryID
				where locationID = #arguments.locationID#
			</cfquery>
		</cfif>

			<cfif not arguments.showAddressOnly>
				<cfif (len(location.sitename) gt 20) >
					<cfset formattedAddress = left(location.sitename,20) & "..">
				<cfelse>
					<cfset formattedAddress = location.sitename>
				</cfif>
				<cfset formattedAddress = formattedAddress & " (">		<!--- PPB 2008-10-13 issue 970 removed comma --->
			</cfif>

			<cfif (len(location.address1) gt 15) >
				<cfset formattedAddress = formattedAddress & left(location.address1,15) & "..">
			<cfelse>
				<cfset formattedAddress = formattedAddress & location.address1>
			</cfif>
			<cfset formattedAddress = formattedAddress & ", ">

			<cfif (len(location.address4) gt 10) >
				<cfset formattedAddress = formattedAddress & left(location.address4,10) & "..">
				<cfelse>
				<cfset formattedAddress = formattedAddress & location.address4>
				</cfif>
				<cfset formattedAddress = formattedAddress & ", ">

			<cfif (len(location.address5) gt 2) >
				<cfset formattedAddress = formattedAddress & left(location.address5,2) & "..">
				<cfelse>
				<cfset formattedAddress = formattedAddress & location.address5>
			</cfif>
			<cfset formattedAddress = formattedAddress & " ">

			<cfif len(location.postalcode) gt 10>
				<cfset formattedAddress = formattedAddress & left(location.postalcode,10) & "..">
			<cfelse>
				<cfset formattedAddress = formattedAddress & location.postalcode>
			</cfif>
			<cfset formattedAddress = formattedAddress & ", ">

			<cfset formattedAddress = formattedAddress & left(location.ISOCode,2)>

			<cfif not arguments.showAddressOnly>
				<cfset formattedAddress = formattedAddress & ")">
			</cfif>


		<cfreturn formattedAddress>

	</cffunction>

	<!--- 2014-11-20 AXA P-PGI001 added function getLocationForOrg() --->
	<cffunction access="public" name="getLocationForOrg" output="false" hint="Get Locatons for a persons organisation" returntype="query">
		<cfargument name="flagTextID" required="false" type="string" hint="flagTextID to filter location data on">
		<cfargument name="includeHQ" required="false" type="boolean" default="true" hint="used to bring back locations flagged as HQ">
		<cfargument name="myStoreOnly" required="false" type="boolean" default="false" hint="limits the resultset to loggedIn users location only">
		<cfargument name="showAll"		required="false" type="boolean" default="true" hint="ignore all flags and hq logic and return all locations.  basically reverts back to original query without filters.">
		<cfscript>
		var returnQuery = QueryNew('temp');
		var locationList = "";
		var getHeadQuarterLocationID = QueryNew('locationID');
		if (NOT arguments.showAll){
			if(NOT application.com.flag.isFlagSetForCurrentUser(flagID = 'HQLocation')){
				if(arguments.myStoreOnly AND arguments.includeHQ){
					locationList = "#request.relaycurrentuser.LocationID#";
					getHeadQuarterLocationID = getLocationForOrg(flagTextID = 'HQLocation', includeHQ = false, myStoreOnly = false, showAll = false); //Make the arguments includeHQ & myStoreOnly to stop the fuction looping
					locationList = ListAppend(locationList, ValueList(getHeadQuarterLocationID.locationID));
				} else if (arguments.myStoreOnly AND NOT arguments.includeHQ){
					locationList = request.relaycurrentuser.LocationID;
				}
			}
		}
		</cfscript>

		<cfquery name="returnQuery" datasource="#application.SiteDataSource#">
			SELECT DISTINCT
				L.locationID,
				L.siteName,
				L.Address1,
				L.Address2,
				L.Address3,
				L.Address4,
				L.Address5,
				L.PostalCode,
				C.ISOCode,
				C.countryID,
				CASE WHEN LEN(L.siteName) > 20 THEN LEFT(L.siteName, 20) + '..' ELSE ISNULL(L.siteName, '') END +
				' (' + CASE WHEN LEN(L.Address1) > 15 THEN LEFT(L.Address1, 15) + '..' ELSE ISNULL(L.Address1, '') END +
				', ' + CASE WHEN LEN(L.Address4) > 10 THEN LEFT(L.Address4, 10) + '..' ELSE ISNULL(L.Address4, '') END +
				', ' + CASE WHEN LEN(L.Address5) > 2 THEN LEFT(L.Address5, 2) + '..' ELSE ISNULL(L.Address5, '') END +
				' ' + CASE WHEN LEN(L.PostalCode) > 10 THEN LEFT(L.PostalCode, 10) + '..' ELSE ISNULL(L.PostalCode, '') END +
				' ' + CASE WHEN LEN(SUBSTRING(C.ISOCode, 4, 100)) > 2 THEN LEFT(SUBSTRING(C.ISOCode, 4, 100), 2) + '..' ELSE ISNULL(SUBSTRING(C.ISOCode, 4, 100), '') END + ')' AS 'formattedAddress'
			FROM
				location AS L
				<cfif NOT arguments.showAll>
				LEFT OUTER JOIN vBooleanFlagData AS F ON
					F.entityID = L.locationID AND
					F.FlagGroupTextID IN ('LocationType','AddressType')
				</cfif>
				INNER JOIN country AS C ON
					C.countryID = L.countryID
			WHERE
				L.organisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.organisationID#">
				<cfif NOT arguments.showAll>
					<cfif StructKeyExists(arguments, "flagTextID") AND arguments.flagTextID NEQ ''>
						AND ISNULL(F.flagTextID, '') IN (<cf_queryparam value="#arguments.flagTextID#" cfsqltype="cf_sql_varchar" list="true">)
					</cfif>
					<cfif locationList NEQ ''>
						AND L.locationID IN (<cf_queryparam value="#locationList#" cfsqltype="cf_sql_integer" list="true">)
					</cfif>
				</cfif>
		</cfquery>

		<cfreturn returnQuery>
	</cffunction>


	<!--- JIRA PROD-343 NJH 2016/01/06 - Domain Matching --->
	<cffunction name="getDomainMatch" access="public" output="false" hint="Find approved domains for a given email address" returnType="struct">
		<cfargument name="emailAddress" type="string" required="true">
		<cfargument name="countryId" type="numeric" required="false">

		<cfset var getDomainMatches = "">
		<cfset var emailDomain = trim(right(arguments.emailAddress, len(arguments.emailAddress) - find("@",arguments.emailAddress)))>
		<cfset var flagEntityType = application.com.flag.getFlagStructure(flagID="AuthorizedDomain").entityType.tablename>
		<cfset var entityUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=flagEntityType).uniqueKey>
		<cfset var result = {locationIDList=0,domainMatched=false}>

		<!--- spaces in authorised domain are treated as delimiters
			NJH 2017/02/20 - JIRA RT-287 - had to add OPTION (FORCE ORDER) so that the regexp would only run against the rows returned by the joins before it (rather than the whole of the textFlagdata table)
		 --->
		<cfquery name="getDomainMatches">
			select locationID
			from #flagEntityType# e
				<cfif flagEntityType eq "organisation">
				inner join location l on l.organisationID = e.organisationID
				</cfif>
				inner join vBooleanFlagData app on app.entityID = e.#entityUniqueKey# and app.flagTextID = '#left(flagEntityType,3)#Approved'
				inner join vFlagDef f on f.flagTextID='AuthorizedDomain'
				inner join textFlagData tfd on tfd.flagID = f.flagID and tfd.entityID = e.#entityUniqueKey#
			where
				isNull(accountTypeID,1) in (#application.organisationType.reseller.organisationTypeID#,#application.organisationType.distributor.organisationTypeID#)
				and dbo.RegExIsMatch(N'(^|,)#replace(emailDomain,".","\.")#($|,)',dbo.RegExReplace(replace(data,' ',','),'(^|,)([^@,]*?)@','$1'),1) = 1
					<!--- (dbo.RegExReplace(data,'(^|,)([^@,]*?)@','$1') = <cf_queryparam cfsqltype="cf_sql_varchar" value="#emailDomain#"/> <!--- equals --->
						or dbo.RegExReplace(data,'(^|,)([^@,]*?)@','$1') like <cf_queryparam cfsqltype="cf_sql_varchar" value="#emailDomain#,%"/> <!--- match beginnging --->
						or dbo.RegExReplace(data,'(^|,)([^@,]*?)@','$1') like <cf_queryparam cfsqltype="cf_sql_varchar" value="%,#emailDomain#"/> <!--- match end --->
						or dbo.RegExReplace(data,'(^|,)([^@,]*?)@','$1') like <cf_queryparam cfsqltype="cf_sql_varchar" value="%,#emailDomain#,%"/> <!--- match middle --->
					) --->
				<cfif structKeyExists(arguments,"countryID") and arguments.countryID neq 0>
				and #flagEntityType eq "organisation"?"l":"e"#.countryID = <cf_queryparam value="#arguments.countryID#" cfsqltype="cf_sql_integer">
				</cfif>
				OPTION (FORCE ORDER)
		</cfquery>

		<cfset result.domainMatched = getDomainMatches.recordCount>
		<cfif getDomainMatches.recordCount>
			<cfset result.locationIDList = valueList(getDomainMatches.locationID)>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="isPersonAPrimaryContact" access="public" output="false" hint="Returns whether a person is a primary contact or not" returnType="boolean">
		<cfargument name="personID" type="numeric" default="#request.relaycurrentUser.personID#" hint="The ID of the person">

		<!--- returns true if person is primary contact or org or location level --->
		<cfset var result = false>
		<cfset var primaryFlag = getPrimaryContactFlag()>

		<cfif primaryFlag neq "" and application.com.flag.getFlagData(flagID=primaryFlag,data=arguments.personID).recordCount>
			<cfset result = true>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!--- NJH 2016/07/27 JIRA PROD2016-351/599 this is a function to get the primary contacts while we are in transition between org and location level, with org being a single contact and location level being multiple --->
	<cffunction name="getPrimaryContactsForEntity" access="public" output="false" hint="Returns a list of primary contacts for an organisation or location" returnType="string">
		<cfargument name="entityID" type="numeric" required="true" hint="Either an organisation or location ID depending on whether the flag is a location or organisation level. Default is a locationID">
		<!--- <cfargument name="entityType" type="string" default="location" hint="Either organisation or location"> --->
		<cfargument name="primaryContactFlag" type="string" default="PrimaryContacts" hint="Location or organisation level Primary Contact flag. Defaulted to location primary contact flag">

		<cfset var primaryContactPersonIds = "">
		<cfset var primaryContactFlagStruct = application.com.flag.getFlagStructure(flagID=arguments.primaryContactFlag)>

		<cfif primaryContactFlagStruct.isOK and primaryContactFlagStruct.flagActive>
			<cfset var primaryContactQry = application.com.flag.getFlagData(flagid=primaryContactFlagStruct.flagID, entityID=arguments.entityId)>
			<cfset primaryContactPersonIds = valueList(primaryContactQry.data)>
		</cfif>
		<cfreturn primaryContactPersonIds>
	</cffunction>


	<!--- NJH 2016/07/27 JIRA PROD2016-351/599 this is a function to get the primary contact flag while we are in transition between org and location level, with org being a single contact and location level being multiple --->
	<cffunction name="getPrimaryContactFlag" access="public" output="false" hint="Returns the primary contact flag" returnType="string">

		<cfset var primaryContactFlag = "">

		<cfloop list="PrimaryContacts,KeyContactsPrimary" index="primaryContactFlag">
			<cfset var primaryContactFlagStruct = application.com.flag.getFlagStructure(flagID=primaryContactFlag)>

			<cfif primaryContactFlagStruct.isOK and primaryContactFlagStruct.flagActive>
				<cfreturn primaryContactFlag>
			</cfif>
		</cfloop>

		<cfreturn primaryContactFlag>
	</cffunction>
</cfcomponent>
