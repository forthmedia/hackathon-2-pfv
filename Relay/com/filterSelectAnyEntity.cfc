<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			filterSelectAnyEntity.cfc
Author:				YMA
Date started:		01/08/2014

Description:		functions for filtering.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:

 --->
<cfcomponent displayname="filterSelectAnyEntity" hint="logic behind entity filtering">
	
	<cfset this.oEntityFields = {}>

	<cffunction name="init">
		<cfreturn this />
	</cffunction>

	<cffunction name="getFilterCriteria" access="public" returnType="query" output="false">
		<cfargument name="filterCriteriaID" required="false">

		<cfset var getFilterCriteria = "">

		<cfquery name="getFilterCriteria" datasource="#application.siteDataSource#">
			select * from filterCriteria fc
			<cfif structkeyexists(arguments,"approvalEngineApproverID")>
				where fc.filterCriteriaID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.filterCriteriaID#">
			</cfif>
		</cfquery>

		<cfreturn getFilterCriteria>
	</cffunction>
											

	<cffunction name="getFilterCriteriaForEntityTypeID" access="public" returnType="query" output="false">
		<cfargument name="relatedEntityTypeID" required="true">

		<cfset var getFilterCriteria = "">

		<cfquery name="getFilterCriteria" datasource="#application.siteDataSource#">
			select * from filterCriteria fc
			where fc.entityTypeID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relatedEntityTypeID#">
		</cfquery>

		<cfreturn getFilterCriteria>
	</cffunction>
											

	<cffunction name="getFilterCriteriaForEntity" access="public" returnType="query" output="false">
		<cfargument name="relatedEntityID" required="true">
		<cfargument name="relatedEntityTypeID" required="true">

		<cfset var getFilterCriteria = "">

		<cfquery name="getFilterCriteria" datasource="#application.siteDataSource#">
			select * from filterCriteria fc
			where fc.entityID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relatedEntityID#">
			and fc.entityTypeID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relatedEntityTypeID#">
		</cfquery>

		<cfreturn getFilterCriteria>
	</cffunction>

	<!---Returns a json string equivalen to "no filter" for a particular entityType filter --->
	<cffunction name="getStarterFilter" access="public" returnType="string" output="false">
		<cfargument name="entityTypeID" type="numeric" required="true">
	
		<cfscript>
			var filterStructure={entityTypeID=entityTypeID, filter=[]};
		
			return SerializeJSON(filterStructure);
		</cfscript>
	
	</cffunction>

	<!--- Equivalent functionality to getFilterCriteriaForEntity but accepts a json string holding the current filter--->
	<cffunction name="interpretCurrentFilterForDisplay" access="public" returnType="query" output="false">
		<cfargument name="currentFilterJSON" type="String" required="true">
		<cfscript>
			if (currentFilterJSON ==""){
				return queryNew("field,operator,comparator");
				
			}else{
				var filters=deserializejson(currentFilterJSON).filter;
		
				//loop over the structure to grab just the display parts
				var filterDisplayData=arrayNew(1);
				for(var i=1;i<=arrayLen(filters);i++){
					filterDisplayData[i]=filters[i].filterDisplay;
				}
		
				//this will be a structure of the form [{field="field",operator="operator", comparatorB="comparison",comparatorC="" },etc]
				
				var convertedQuery=application.com.structureFunctions.arrayOfStructuresToQuery(filterDisplayData);
			
				return convertedQuery;
			}
			
			
		</cfscript>
		
		
	</cffunction>	


	<cffunction name="runFilterAgainstEntity" access="public" returnType="query" output="false">
		<cfargument name="filterEntityID" required="true">
		<cfargument name="filterEntityTypeID" required="true">
		<cfargument name="relatedEntityID" required="true">
		<cfargument name="relatedEntityTypeID" required="true">
		
		<cfset var getFilterCriteriaForEntity = getFilterCriteriaForEntity(relatedEntityID=arguments.filterEntityID,relatedEntityTypeID=arguments.filterEntityTypeID)>
		<cfset var runFilterAgainstEntity = "">
		
		<cfquery name="runFilterAgainstEntity" datasource="#application.siteDataSource#">
				select * from #application.entityType[arguments.relatedEntityTypeID].TABLENAME#
			<cfloop query="getFilterCriteriaForEntity">
					#replace(getFilterCriteriaForEntity.compiledSQL,"''","'","all")#
				</cfloop>
				where #application.entityType[arguments.relatedEntityTypeID].TABLENAME#.#application.entityType[arguments.relatedEntityTypeID].uniqueKey# = #arguments.relatedEntityID#
		</cfquery>
		
		<cfreturn runFilterAgainstEntity>
	</cffunction>
	
	
	<cffunction name="getCurrentEntityFields" access="public" hint="stores current entity fields struct in component" returntype="struct" output="false">
		<cfargument name="entityTypeID" required="true" type="numeric">
		<cfargument name="tablename" required="true" type="string">
		
		<cfset var field="">
		<cfset var tempStruct = {}>
		
		<!--- lock in case the cache is currently filling oEntityFields --->
		<cflock scope="Application" timeout="10" type="exclusive">
			<!--- only fill the cache if the entitytypeID is not currently defined within the struct --->
			<cfif not structKeyExists(this.oEntityFields,"#arguments.entityTypeID#")>
				<!--- get all the fields available to the entity --->
				<cfset tempStruct = application.com.relayentity.getEntityMetaData(entityTypeID=arguments.entityTypeID,countryIDPerspective=0)[arguments.tablename]>
				
				
				<cfloop collection=#tempStruct# item="field">
					<cfset tempStruct[field].fieldDescriptor = application.com.relayentity.getTableFieldStructure(tablename=arguments.tablename,fieldname=field)>
				</cfloop>
				
				<cfset this.oEntityFields[arguments.entityTypeID] = tempStruct>
			</cfif>
		</cflock>
		
		<cfreturn this.oEntityFields[arguments.entityTypeID]>
	</cffunction>
	
	<cffunction name="retrieveEntityField" access="public" hint="stores current entity fields struct in component" returntype="struct" output="false">
		<cfargument name="entityTypeID" required="true" type="numeric">
		<cfargument name="field" required="true" type="string">
		
		<!--- only fill the cache if the entitytypeID is not currently defined within the struct --->
		<cfif not structKeyExists(this.oEntityFields,"#arguments.entityTypeID#")>
			<cfset getCurrentEntityFields(entityTypeID=arguments.entityTypeID,tablename=application.entityType[arguments.entityTypeID].tablename)>
		</cfif>
		
		<cfreturn this.oEntityFields[arguments.entityTypeID][arguments.field]>
	</cffunction>
	
	<cffunction name="loadOperator" access="public">
		<cfargument name="field" required="true" type="string">
		<cfargument name="entityTypeID" required="true">
		<cfargument name="comparisonContainerId" default="comparisonContainer">
		
		<cfset var dataType = "">
		<cfset var result="">
		<cfset var fieldStruct = retrieveEntityField(entityTypeID=arguments.entityTypeID,field=arguments.field)>
		
		<cfif fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flag">
			<cfset dataType = fieldStruct.fielddescriptor.flagtype.datatable>
		<cfelseif fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flagGroup">
			<cfset dataType = fieldStruct.fielddescriptor.flagtype.datatable>
		<cfelse>
			<cfset dataType = fieldStruct.fielddescriptor.data_type>
		</cfif>
		
		<cfsavecontent variable="result">
			<cfoutput>
			<select id="operator" name="operator" class="operator"
				<cfif (not listfindnocase('bit,boolean',dataType)) or (fieldStruct.hasvalidvalues 
						and not fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flag")>
					onchange="loadComparison('#arguments.field#',this.value,#entityTypeID#, jQuery('###comparisonContainerId#'))"
				</cfif>
				>
				<option value="">phr_ext_selectavalue</option>
				<cfif fieldStruct.hasvalidvalues and not fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flag">
					<option value="|a| = '|b|'">=</option>
					<option value="|a| != '|b|'">!=</option>
					<option value="|a| in (|b|)">one of</option>
					<option value="|a| not in (|b|)">not one of</option>
				<cfelse>
					<cfswitch expression="#dataType#">
						<cfcase value="int,integer,numeric,decimal">
							<option value="|a| = |b|">=</option>
							<option value="|a| > |b|">></option>
							<option value="|a| >= |b|">>=</option>
							<option value="|a| < |b|"><</option>
							<option value="|a| <= |b|"><=</option>
							<option value="|a| between |b| and |c|">between</option>
							<option value="|a| in (|b|)">in ()</option>
							<option value="|a| not in (|b|)">not in ()</option>
						</cfcase>
						<cfcase value="varchar,nvarchar,text">
							<option value="|a| = '|b|'">=</option>
							<option value="|a| like('|b|%')">starts with</option>
							<option value="|a| like('%|b|')">ends with</option>
							<option value="|a| like('%|b|%')">contains</option>
						</cfcase>
						<cfcase value="boolean,bit">
							<option value="|a| = 1">true</option>
							<cfif not fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flag">
								<option value="|a| = 0">false</option>					
							</cfif>
						</cfcase>
						<cfcase value="date,datetime">
							<option value="|a| > '|b|'">after</option>
							<option value="|a| < '|b|'">before</option>
							<option value="|a| between '|b|' and '|c|'">between</option>
						</cfcase>
						<cfdefaultcase>
							<option value="|a| = '|b|">#dataType# operators not defined</option>
						</cfdefaultcase>
					</cfswitch>
				</cfif>
			</select>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="loadComparison" access="public">
		<cfargument name="field" required="true" type="string">
		<cfargument name="operator" required="true" type="string">
		<cfargument name="entityTypeID" required="true">
		
		<cfset var result="">
		<cfset var fieldStruct = retrieveEntityField(entityTypeID=arguments.entityTypeID,field=arguments.field)>
		
		<cfsavecontent variable="result">
			<cfoutput>
				<cfif fieldStruct.hasvalidvalues and not fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flag">
					<select <cfif findNoCase(' in ',arguments.operator)>multiple="true"</cfif> name="comparisonB" id="comparisonB" class="comparisonB">
						<cfif isquery(fieldStruct.validvalues)>
							<cfloop query="#fieldStruct.validvalues#">
								<option value="#datavalue#">#displayvalue#</option>
							</cfloop>
						<cfelseif fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flagGroup">
							<cfloop query="#application.com.flag.getFlagGroupFlags(FlagGroupId=fieldStruct.FIELD)#">
								<option value="#FLAGID#">#translatedName#</option>
							</cfloop>
						</cfif>
					</select>
				<cfelse>
					<cfif fieldStruct.datatype eq "date" or fieldStruct.datatype eq "dateTime" >
						<cf_relayDateField label="" fieldname="comparisonB" class="comparisonB">
						<cfif findNoCase('|c|',arguments.operator)>
						 and <cf_relayDateField label="" fieldname="comparisonC" class="comparisonC">
						</cfif>
					<cfelseif ListContainsNoCase("int,integer,numeric,decimal",fieldStruct.datatype)>
						<input type="number" name="comparisonB" id="comparisonB" class="comparisonB"/>
						<cfif findNoCase('|c|',arguments.operator)>
						 and <input type="number" name="comparisonC" id="comparisonC" class="comparisonC" />
						</cfif>
					<cfelse>
						<input type="text" name="comparisonB" id="comparisonB" class="comparisonB"/>
						<cfif findNoCase('|c|',arguments.operator)>
						 and <input type="text" name="comparisonC" id="comparisonC" class="comparisonC" />
						</cfif>
					</cfif>
				</cfif>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="saveFilter" access="public">
		<cfargument name="filterField" required="true" type="string">
		<cfargument name="operatorDisplay" required="true" type="string">
		<cfargument name="operator" required="true" type="string">
		<cfargument name="comparisonB" required="true">
		<cfargument name="comparisonC" required="false">
		<cfargument name="RELATEDENTITYID" required="true">
		<cfargument name="RELATEDENTITYTYPEID" required="true">
		<cfargument name="filterEntityTypeID" required="true">
		
		<cfset var compiledSQL = "">
		<cfset var i = "">
		<cfset var comparison = arguments.comparisonB>
		<cfset var result = {isOk=true,html="",message="",errorCode="",filterCriteriaID=0}>  

		<cfset var joinInfo = "">
		
		<cfif structKeyExists(arguments,"comparisonC")>
			<cfset comparison = "#comparison# and #arguments.comparisonC#">		
		</cfif>
		
		<cftry>
			<cf_transaction action="begin">

			<cfquery name="insertFilterCriteria" datasource="#application.siteDataSource#">
				insert into filterCriteria
				values(
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.filterEntityTypeID#">,
					<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.filterField#">,
					<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.operatorDisplay#">,
					<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#comparison#">,
					'',
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.RELATEDENTITYID#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.RELATEDENTITYTYPEID#">)
				select scope_identity() as insertedID
			</cfquery>
			
			
			<!--- work out compiled SQL --->
			<cfif structKeyExists(arguments,"comparisonC") >
				<cfset compiledSQL=getCompiledSQLForFilter(filterField=filterField,operator=operator,comparisonB=comparisonB,comparisonC=comparisonC,filterEntityTypeID=filterEntityTypeID,operatorDisplay=operatorDisplay)>
			<cfelse>
				<cfset compiledSQL=getCompiledSQLForFilter(filterField=filterField,operator=operator,comparisonB=comparisonB,filterEntityTypeID=filterEntityTypeID,operatorDisplay=operatorDisplay)>
			</cfif>

			<cfquery name="updateFilterCriteriaCompiledSQL" datasource="#application.siteDataSource#">
				update filterCriteria
				set compiledSQL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#compiledSQL#">
				where filterCriteriaID = <cfqueryparam cfsqltype="cf_sql_integer" value="#insertFilterCriteria.insertedID#">
			</cfquery>

			<cfset result.filterCriteriaID=insertFilterCriteria.insertedID>
			<cfset result.errorCode = "ENTITY_INSERTED">

			<cf_transaction action="commit"/>

			</cf_transaction>
			<cfcatch>
				<cf_transaction action="rollback"/>

				<cfset result.isOK = false>
				<cfset result.errorCode = "INSERT_ERROR">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>
				
		<cfsavecontent variable="result.html">
			<cfoutput>
				<tr id="#result.filterCriteriaID#">
					<td>#arguments.filterField#</td>
					<td>#arguments.operatorDisplay#</td>
					<td>#arguments.comparisonB#<cfif structKeyExists(arguments,"comparisonC")> and #arguments.comparisonC#</cfif></td>
					<td><a href="##" onclick="deleteFilter(#result.filterCriteriaID#)">delete</a></td>
				</tr>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn result>
	</cffunction>
	
	<!--- Used with filterLocallyAnyEntity, this interprets the filter definition into a piece of sql that can go in a join statement --->
	<cffunction name="getSQLForFilterDefinition" >
		<cfargument name="filterDefinitionJSON" required="true">
		
		<cfscript>
			var filterDefinition=deserializeJSON(filterDefinitionJSON);
			var filterEntityTypeID=filterDefinition.entityTypeID;
			var filterDefinitionRows=filterDefinition.Filter;
			
			var filterSQL=""; //each "row" of the filter gets compiled independantly then joined together
			for(var i=1;i<=arrayLen(filterDefinitionRows);i++ ){
				var field=filterDefinitionRows[i].filterData.field;
				var operator=filterDefinitionRows[i].filterData.operator;
				var comparisonB=structKeyExists(filterDefinitionRows[i].filterData,"comparatorB")?filterDefinitionRows[i].filterData.comparatorB:"";
				var comparisonC=structKeyExists(filterDefinitionRows[i].filterData,"comparatorC")?filterDefinitionRows[i].filterData.comparatorC:"";
				var operatorDisplay=filterDefinitionRows[i].filterDisplay.operator;
				var filterSQL&= " " & application.com.filterSelectAnyEntity.getCompiledSQLForFilter(field,operator,comparisonB,comparisonC,filterEntityTypeID,operatorDisplay);
			}
	
		
			return filterSQL;
		</cfscript>
		
				
	
	
	</cffunction>
	
	<cffunction name="getCompiledSQLForFilter" access="public">
		<cfargument name="filterField" required="true">
		<cfargument name="operator" required="true" type="string">
		<cfargument name="comparisonB" required="true">
		<cfargument name="comparisonC">
		<cfargument name="filterEntityTypeID" required="true">
		<cfargument name="operatorDisplay" required="true" type="string">
		<cfargument name="filterUniqueIdentifier" default="#Replace(CreateUUID(),'-','','all')#" hint="what this is isn't important, its just used as an alias, is should be unique though">

		<cfset var fieldStruct = retrieveEntityField(entityTypeID=arguments.filterEntityTypeID,field=arguments.filterField)>
		<cfif FindNoCase(operator,"in")> 
			<cfset var whereClause = replace(replace(arguments.operator,'|a|',arguments.filterField),'|b|',listqualify(arguments.comparisonB,"'",",","all"))> <!--- 446596 2015/12/16 GCC & RJT quote the list - ok for numbers too --->
		<cfelse>	
			<cfset var whereClause = replace(replace(arguments.operator,'|a|',arguments.filterField),'|b|',arguments.comparisonB)>
		</cfif>
		
		<cfdump var="#whereClause#">

		<cfset comparison=arguments.comparisonB>
		<cfif structKeyExists(arguments,"comparisonC") and comparisonC NEQ "">
			<cfset comparison = "#comparison# and #arguments.comparisonC#">
			
			<cfset whereClause =  replace(whereClause,'|c|',arguments.comparisonC)>
			
		</cfif>
		
		
		
		<cfsavecontent variable="compiledSQL">
					<cfoutput>
						INNER JOIN (
						select #application.entityType[arguments.filterEntityTypeID].uniqueKey# from #application.entityType[arguments.filterEntityTypeID].TABLENAME#
						
						<cfif fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flagGroup">
							where (
								<cfloop list="#arguments.comparisonB#" index="i">
									<cfif not listFirst(arguments.comparisonB) eq i>
										OR
									</cfif>
									#application.entityType[arguments.filterEntityTypeID].uniqueKey# in (
										select #application.entityType[arguments.filterEntityTypeID].TABLENAME#.#application.entityType[arguments.filterEntityTypeID].uniqueKey# from #application.entityType[arguments.filterEntityTypeID].TABLENAME#
											#application.com.flag.getJoinInfoForFlag(
											flagID = i,
											jointype = 'inner',
											entityTableAlias=application.entityType[arguments.filterEntityTypeID].tablename
											).join#
									)
								</cfloop>
								
							)
						<cfelseif fieldStruct.FIELDDESCRIPTOR.FIELDTYPE eq "flag">
							<cfset joinInfo = application.com.flag.getJoinInfoForFlag(
										flagID = arguments.filterField,
										jointype = 'inner',
										entityTableAlias=application.entityType[arguments.filterEntityTypeID].tablename
										)>
							#joinInfo.join#
							<cfif listFindNoCase("in (),not in ()",arguments.operatorDisplay) gt 0>
                                where #structKeyExists(joinInfo,"selectFieldRaw") ? joinInfo.selectFieldRaw : joinInfo.selectField# <!--- Case 448461 --->
								<cfif left(arguments.operatorDisplay,3) eq "not">
									not
								</cfif>
									in (#comparison#) 
                            <cfelseif listFindNoCase("=,>,<,>=,<=", arguments.operatorDisplay)> <!--- 08/03/2016 DAN Case 448461 - add missing where clause for flags --->
                                where #structKeyExists(joinInfo,"selectFieldRaw") ? joinInfo.selectFieldRaw : joinInfo.selectField# #arguments.operatorDisplay# #comparison#
							</cfif>
						<cfelse>
							 where #whereClause# 
						</cfif>
						) as filter#filterUniqueIdentifier#
						on filter#filterUniqueIdentifier#.#application.entityType[arguments.filterEntityTypeID].uniqueKey# 
						= #application.entityType[arguments.filterEntityTypeID].TABLENAME#.#application.entityType[arguments.filterEntityTypeID].uniqueKey#
					</cfoutput>
			</cfsavecontent>
		<cfreturn compiledSQL>
	</cffunction>
	
	
	
	<cffunction name="deleteFilter" access="public">
		<cfargument name="filterCriteriaID" required="true">
		
		<cfset var result = {isOk=true,html="",message="",errorCode="",filterCriteriaID=arguments.filterCriteriaID}>
		
		<cftry>
			<cf_transaction action="begin">

			<cfquery name="deleteFilterCriteria" datasource="#application.siteDataSource#">
				delete filterCriteria where filterCriteriaID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.filterCriteriaID#">
			</cfquery>
			
			<cfset result.errorCode = "ENTITY_DELETED">

			<cf_transaction action="commit"/>

			</cf_transaction>
			<cfcatch>
				<cf_transaction action="rollback"/>

				<cfset result.isOK = false>
				<cfset result.errorCode = "DELETE_ERROR">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>
		
		<cfreturn result>
	</cffunction>
	
</cfcomponent>