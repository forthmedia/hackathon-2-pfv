<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Adam Reynolds
Date created:	2005-04-25

	This provides the set of functionality currency conversion for the Relay system

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/03/11			NJH			P-PAN002 - added currency fields to the where clause in the convert function. Return two new fields.. fromCurrencySign and toValueNumeric
2010/04/27			PPB			added getCurrencyByISOCode
2013-03-20 			NYB 		Case 431040 created Function localeCurrencyFormat
2013-10-11 			PPB 		Case 437226 defend against a currency not existing for the supplied isocode
--->

<cfcomponent displayname="currency" hint="Provides facilities to enable currency conversion">

<!--- 
*************
Note:  WAB 2009/09/22 I have commented this out on dev because the initialisation always fails, and it takes 20 seconds to do so
*************

<!--- Set of variables used to maintain currency. --->
<cfscript>
	// Initialise on start up.
	if (not structKeyExists(application,"currencyConversionTable")){
		tmp = this.initLocal();
	}
</cfscript>
 --->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Get the latest version of the 'local' version of the conversion.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="globalInit" output="No" returntype="string">
	<!--- 2006/11/23 - GCC - removed xmlDownloadUrl to stop hitting xe.com since we stopped the subscription --->
	<!--- http://www.xe.com/dfs/datafeed2.cgi?foundationnetworkltd --->
	<cfscript>
		var XMLdownloadUrl = ""; 
		var XMLfilepath = "#getdirectoryfrompath(getcurrentTemplatePath())#\..\CurrencyConverter\currencies.xml";
		var XMLfiledata = "";
		var XMLCurrencies = "";
	</cfscript>
	<!--- Read in the 'current' file. --->
	<cftry>
		<cffile action="READ" file="#getdirectoryfrompath(getcurrentTemplatePath())#\..\CurrencyConverter\currencies.xml" variable="XMLfiledata" charset="UTF-8">
	<cfcatch type="any">
		
		<cfhttp method="GET" url="#XMLdownloadUrl#" timeout="20" throwonerror="Yes"  charset="UTF-8" >
		
		<cffile action="WRITE" file="#XMLfilepath#" output="#cfhttp.fileContent#" addnewline="Yes"  charset="UTF-8">
		<cffile action="READ" file="#XMLfilepath#" variable="XMLfiledata" charset="UTF-8">
	</cfcatch>
	</cftry>
	
	<cftry>
		<!--- Check to see when the next download is needed. --->
		<cfset XMLCurrencies = xmlparse(XMLfiledata)>
		<cfset downloadStatus = XMLSearch(XMLCurrencies,"/xe-datafeed/header[hname='Status']")>
		<cfif downloadStatus[1].hvalue.xmltext neq "OK">
			<cfreturn XMLfilepath>
		</cfif>
		<cfset timestampResult = XMLSearch(XMLCurrencies,"/xe-datafeed/header[hname='UTC Time of Your Next Update']")>
		<cfset currencyStruct.nextUpdateTime = dateadd("h",23,timestampResult[1].hvalue.xmltext)>
		<!--- Check that the data is ok to transmit. --->
		<cfif currencyStruct.nextUpdateTime lte now()>
			<!--- Get the new file. --->
			<cfhttp method="GET" url="#XMLdownloadUrl#" timeout="20" throwonerror="Yes"  charset="UTF-8" >
			</cfhttp>
			
			<!--- Now parse the text. --->
			<cfif cfhttp.statusCode EQ "200 OK">
				<cfset XMLCurrencies = xmlparse(cfhttp.fileContent)>
				<cfset downloadStatus = XMLSearch(XMLPotentialCurrencies,"/xe-datafeed/header[hname='Status']")>
				<cfif downloadStatus[1].hvalue.xmltext eq "OK">
					<!--- Now archive the old file. --->
					<cffile action="MOVE" source="#XMLfilepath#" destination='#getdirectoryfrompath(getcurrentTemplatePath())#\..\CurrencyConverter\currencies#dateformat(now(),"YYYYMMDD")##timeformat(now(),"HHSS")#.xml'>
					<!--- Write away the NEW data. --->
					<cffile action="WRITE" file="#XMLfilepath#" output="#cfhttp.fileContent#" addnewline="Yes"  charset="UTF-8">
					<cfreturn XMLfilepath>
				</cfif>
			</cfif>		
		<cfelse>
			<cfreturn XMLfilepath>
		</cfif>
	<cfcatch type="Any">
		<!--- Fall back to the current file. ---> 
		<cfreturn XMLfilepath>
	</cfcatch>
	</cftry>

</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Initialises the conversion.          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="initLocal" output="Yes" returntype="string">
	<cfscript>
		// Please note that this URL won't work in development, but live boxes can see this URL, so when they do a currency request, the file will update.
		var XMLdownloadUrl = "http://dev.foundation-network.com/sony/currencyConverter/Collecter.cfm";
		var XMLfilepath = "#getdirectoryfrompath(getcurrentTemplatePath())#\..\CurrencyConverter\currencies.xml";
		var XMLfiledata = "";
		var XMLCurrencies = "";
		var currencyStruct = structNew();
		
		currencyStruct.nextUdpdateTime = "";
		currencyStruct.UTCTimestamp = "";		
		currencyStruct.currencies = structNew();
	</cfscript>
	
	<!--- Read in the data file. --->
	<!--- Get the 'New' file from http://www.xe.com/dfs/sample-eur.xml ---> 
	<cftry>
		<cffile action="READ" file="#XMLfilepath#" variable="XMLfiledata" charset="UTF-8">
	<cfcatch type="any">

		<cfhttp method="GET" url="#XMLdownloadUrl#" timeout="20" throwonerror="Yes"  charset="UTF-8" >
		<cffile action="WRITE" file="#XMLfilepath#" output="#cfhttp.fileContent#" addnewline="Yes"  charset="UTF-8">
		<cffile action="READ" file="#XMLfilepath#" variable="XMLfiledata" charset="UTF-8">
	</cfcatch>
	</cftry>
	
	<cftry>
		<!--- Check to see when the next download is needed. --->
		<cfset XMLCurrencies = xmlparse(XMLfiledata)>
		<cfset downloadStatus = XMLSearch(XMLCurrencies,"/xe-datafeed/header[hname='Status']")>
		<cfif downloadStatus[1].hvalue.xmltext neq "OK">
			<cfreturn "Download of currency data failed.">
		</cfif>
		<cfset timestampResult = XMLSearch(XMLCurrencies,"/xe-datafeed/header[hname='UTC Time of Your Next Update']")>
		<cfset currencyStruct.nextUdpdateTime = dateadd("h",2,timestampResult[1].hvalue.xmltext)>
		<cfif currencyStruct.nextUdpdateTime lte now()>
			<!--- Get the new file. --->
			<cfhttp method="GET" url="#XMLdownloadUrl#" timeout="20" throwonerror="Yes"  charset="UTF-8" >
			</cfhttp>
			
			<!--- Now parse the text. --->
			<cfif cfhttp.statusCode EQ "200 OK">
				<cfset XMLPotentialCurrencies = xmlparse(cfhttp.fileContent)>
				<cfset downloadStatus = XMLSearch(XMLPotentialCurrencies,"/xe-datafeed/header[hname='Status']")>
				<cfif downloadStatus[1].hvalue.xmltext eq "OK">
					<!--- Now archive the old file. --->
					<cffile action="MOVE" source="#XMLfilepath#" destination='#getdirectoryfrompath(getcurrentTemplatePath())#\..\CurrencyConverter\currencies#dateformat(now(),"YYYYMMDD")##timeformat(now(),"HHSS")#.xml'>
					<!--- Write away the NEW data. --->
					<cffile action="WRITE" file="#XMLfilepath#" output="#cfhttp.fileContent#" addnewline="Yes"  charset="UTF-8">
					<cfset XMLCurrencies = XMLPotentialCurrencies>
				</cfif>
			</cfif>		
			<!--- Now make sure we don't check it for another 12 hours. --->
			<cfset currencyStruct.nextUdpdateTime = dateadd("h",12,now())>	
		</cfif>
	<cfcatch type="Any">
		<!--- Fall back to the current file. --->
		<cffile action="READ" file="#XMLfilepath#" variable="XMLfiledata" charset="UTF-8"> 
		<cfset XMLCurrencies = xmlparse(XMLfiledata)>
		
	</cfcatch>
	</cftry>
	<!--- Get the currencies. --->
	<cfset resultSet = XMLSearch(XMLCurrencies,"/xe-datafeed/currency")>
	<cfscript>
	for (i=1; i lte arrayLen(resultSet); i = i+1){
		currencyStruct.currencies[#resultSet[i].csymbol.XMLText#] = structNew();
		currencyStruct.currencies[#resultSet[i].csymbol.XMLText#].rate = resultSet[i].crate.XMLText;
		currencyStruct.currencies[#resultSet[i].csymbol.XMLText#].name = resultSet[i].cname.XMLText;
		currencyStruct.currencies[#resultSet[i].csymbol.XMLText#].symbol = resultSet[i].csymbol.XMLText;
	}
	// Get the timestamp for the next download of the file.
	timestampResult = XMLSearch(XMLCurrencies,"/xe-datafeed/header[hname='UTC Time of Your Next Update']");
	currencyStruct.nextUdpdateTime = dateadd("h",2,timestampResult[1].hvalue.xmltext);
	if (currencyStruct.nextUdpdateTime lt now()){currencyStruct.nextUdpdateTime = dateadd("h",12,now());}
	// Get the time stamp on the file.
	timestampResult = XMLSearch(XMLCurrencies,"/xe-datafeed/header[hname='UTC Timestamp']");
	currencyStruct.UTCTimestamp = timestampResult[1].hvalue.xmltext;
	
	// Assign the currency.
	application.currencyConversionTable = currencyStruct;
	</cfscript>
	<cfreturn "OK">
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the structure containing all the           
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<!--- NJH 2010/07/29 - P-PAN002 Rollup Reporting - added resultstruct.toValueNumeric as toValue was a money field and couldn't be formatted in TFQO when over 999.
		Also returned the fromCurrency sign.
 --->

<cffunction name="convert">
	<cfargument name="fromValue" required="Yes" type="numeric">
	<cfargument name="fromCurrency" required="Yes">
	<cfargument name="toCurrency" default="" >
	<cfargument name="method" default="1">
	<cfargument name="date" required="No">
	<cfset var resultStruct = structNew()>
	
	
	<cfswitch expression="#arguments.method#">
		<cfcase value="1">				
			<cfscript>
				// Initialise if struct no longer exists or the timestamp is out.
				if (not structKeyExists(application,"currencyConversionTable") OR  application.currencyConversionTable.nextUdpdateTime lte now()){
					if (this.initLocal() neq "OK"){
					resultStruct.fromValue = arguments.fromValue;
					resultStruct.fromCurrency = arguments.fromCurrency;
					resultStruct.toCurrency = arguments.toCurrency;		
					resultStruct.conversionRate = '';
					resultStruct.toValue = '';
					resultStruct.fromCurrencyName = '';
					resultStruct.toCurrencyName = '';
					resultStruct.toValueNumeric = '';
					return resultStruct;}
					
				}
				// Make sure that the tocurrency is defined.
				if (not len(arguments.toCurrency)){
					countryDetailsQry = application.com.relayCountries.getCountryDetails(countryID = request.relayCurrentUser.countryID);
					arguments.toCurrency = countryDetailsQry.CountryCurrency;
				}
				
				resultStruct.fromValue = val(arguments.fromValue);
				resultStruct.fromCurrency = arguments.fromCurrency;
				resultStruct.toCurrency = arguments.toCurrency;
				resultStruct.UTCTimestamp = application.currencyConversionTable.UTCTimestamp;
				
				if (not structKeyExists(application,"currencyConversionTable")){tmp = this.initLocal();}
				if (structKeyExists(application.currencyConversionTable.currencies,arguments.fromCurrency) AND structKeyExists(application.currencyConversionTable.currencies,arguments.toCurrency)){
					resultStruct.conversionRate = application.currencyConversionTable.currencies[arguments.toCurrency].rate/ application.currencyConversionTable.currencies[arguments.fromCurrency].rate  ;
					resultStruct.toValue = val(arguments.fromValue) * resultStruct.conversionRate;
					// Assign the currency names.
					resultStruct.fromCurrencyName = application.currencyConversionTable.currencies[arguments.fromCurrency].name;
					resultStruct.toCurrencyName = application.currencyConversionTable.currencies[arguments.toCurrency].name;
				}
				else {
					resultStruct.conversionRate = '';
					resultStruct.toValue = '';
					resultStruct.fromCurrencyName = '';
					resultStruct.toCurrencyName = '';
				}
				resultStruct.toValueNumeric = resultStruct.toValue;
				return resultStruct;
			</cfscript>
		</cfcase>
		<cfcase value="2">
			
			<!--- WAB 2012-01-11 security project.  Made some mods so that cf_queryparam replacements did not fall over.  Not even sure if code is used --->
			<cfif structkeyexists(arguments,"fromValue") and arguments.fromValue neq "">
				<cfquery name="resultStructure" datasource="#application.sitedatasource#">
					declare @resultDate datetime
					select @resultDate  = <cfif structkeyexists(arguments,"date") and arguments.date neq ""><cf_queryparam value="#arguments.date#" CFSQLTYPE="CF_SQL_TIMESTAMP"><cfelse>getdate()</cfif>
					
					select top 1
						<cf_queryparam value="#arguments.fromValue#" CFSQLTYPE="CF_SQL_INTEGER" > as fromValue,
						<cf_queryparam value="#arguments.fromCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > as fromCurrency,
						<cf_queryparam value="#arguments.toCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > as toCurrency,		
						case when '#arguments.fromCurrency#' =  <cf_queryparam value="#arguments.toCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 1 else exchangeRate end as conversionRate,
						isNull(convert(varchar(30),cast((case when '#arguments.fromCurrency#' =  <cf_queryparam value="#arguments.toCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 1 else exchangeRate end * #arguments.fromValue#) as money),1),'--') as toValue,
						isNull(cast(case when '#arguments.fromCurrency#' =  <cf_queryparam value="#arguments.toCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 1 else exchangeRate end * #arguments.fromValue# as decimal(18,2)),0) as toValueNumeric,
						cFrom.currencyName as fromCurrencyName,
						cTo.currencyName as toCurrencyName,
						cTo.currencySign as toSign,
						cFrom.currencySign as fromSign
					from
						currency cTo left join exchangeRate on exchangeRate.toCountryCurrency = cTo.currencyISOCode
						and <!--- NJH 2010/07/29 - changed from where... add the conditions to the join, so that we can return the currency data ---> 
							startdate <= @resultDate 
						and
							enddate >= @resultDate 
						<!--- NJH 2010/03/11 P-PAN002 - added the currency fields in the where clause
						and toCountryCurrency = '#arguments.toCurrency#' --->
						and fromCountryCurrency =  <cf_queryparam value="#arguments.fromCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						inner join currency cFrom on cFrom.currencyISOCode =  <cf_queryparam value="#arguments.fromCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					where cTo.currencyIsoCode =  <cf_queryparam value="#arguments.toCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					order by startdate desc
				</cfquery>
				
				<cfreturn resultStructure>				
			<cfelse>
				<cfscript>
					resultStruct.fromValue = '';
					resultStruct.fromCurrency = '';
					resultStruct.toCurrency = '';
					resultStruct.conversionRate = '';
					resultStruct.toValue = '';
					resultStruct.fromCurrencyName = '';
					resultStruct.toCurrencyName = '';
					resultStruct.toSign = '';
					resultStruct.fromSign = '';
					resultStruct.toValueNumeric = '';
				</cfscript>
				<cfreturn resultStruct>
			</cfif>

		</cfcase>
	</cfswitch>
	
	
</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the set of available currencies as a query.    
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="getCurrencies" returntype="query" output="Yes">
	<cfscript>
	
	var currencyQry = QueryNew("symbol,name,ratetoBase");
	var currencyItem = "";
	// Make sure the data is valid.
	if (not structKeyExists(application,"currencyConversionTable") OR  application.currencyConversionTable.nextUdpdateTime lte now()){
		tmp = this.initLocal();
	}
	arrayOfkeys = StructKeyArray(application.currencyConversionTable.currencies);
	tmp = ArraySort(arrayOfkeys, "textnocase", "asc");
	</cfscript>
	
	<cfloop from="1" to="#arrayLen(arrayOfkeys)#" index="itemIdx">
		
		<cfscript>
			tmp = QueryAddRow(currencyQry);
			tmp = QuerySetCell(currencyQry,"symbol",application.currencyConversionTable.currencies[arrayOfkeys[itemIdx]].symbol);
			tmp = QuerySetCell(currencyQry,"name",application.currencyConversionTable.currencies[arrayOfkeys[itemIdx]].name);
			tmp = QuerySetCell(currencyQry,"ratetoBase",application.currencyConversionTable.currencies[arrayOfkeys[itemIdx]].rate);		
		</cfscript>
	</cfloop>
	<cfreturn currencyQry>
</cffunction>

<cffunction name="getCurrencyByISOCode" returntype="query" output="No">
	<cfargument name="currencyISOCode" required="yes" type="string">
	
	<cfset var qryCurrency = "">
	
	<cfquery name="qryCurrency" datasource="#application.siteDataSource#">
		select * from currency where currencyISOCode =  <cf_queryparam value="#arguments.currencyISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	
	<!--- WAB Case 431103, if no excel format set, then use the no symbol style --->
	<cfif qryCurrency.recordCount gt 0 and qryCurrency.currencyMso is "">					<!--- 2013-10-11 PPB Case 437226 added recordcount condition --->
		<cfset querySetCell(qryCurrency,"currencyMSO",'xl25')>
	</cfif>
	
	<cfreturn qryCurrency>
</cffunction>


<!--- 2013-03-20 NYB Case 431040 created Function: --->
<cffunction name="localeCurrencyFormat" returntype="string" output="No">
	<cfargument name="countryid" required="yes">
	<cfargument name="languageid" required="yes">
	<cfargument name="amount" required="yes" type="string">
	
	<cfset var JavaLocale = "">
	<cfset var JavaNumberFormatter = "">
	<cfset var javaFormatterInstance = "">
	<cfset var countryISO = "">
	<cfset var languageISO = "">
	<cfset var valueFormatted = "">
	
	<cfobject name="JavaLocale" action="Create" type="Java" class="java.util.Locale">
	<cfobject name="JavaNumberFormatter" action="Create" type="Java" class="java.text.NumberFormat">

	<cfset countryISO = UCase(application.countryiso[arguments.countryid])>
	<cfset languageISO = LCase(application.languageisocodelookupstr[arguments.languageid])>

	<cfset JavaLocale.init(javacast("string",languageISO), javacast("string",countryISO))>
	<cfset javaFormatterInstance = JavaNumberFormatter.getCurrencyInstance(JavaLocale)>
	<cfset valueFormatted = javaFormatterInstance.format(javacast("double",arguments.amount))>
	
	<cfreturn valueFormatted>
</cffunction>

</cfcomponent>

