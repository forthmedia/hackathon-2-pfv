<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Opportunity Foreign Keys" hint="Provides the queries for the opportunity table foreign keys">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description                            
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="functionName" returnType="query"
		hint="description">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="dataSource" type="string" required="true">
		
		<cfquery name="functionName" datasource="#dataSource#">
		
		</cfquery>
		<cfreturn functionName>
	</cffunction>	

</cfcomponent>