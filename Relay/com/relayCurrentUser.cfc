<!--- �Relayware. All Rights Reserved 2014 --->
<!---
relay/com/relayCurrentUser.cfc

WAb 2005-06-21ish   added lots of cflocking
WAb 2005-06-29   		altered how language is set, removed dependence on cookie.defaultlanguageid
					check added to get possible country of the unknown user


WAB  2006-02-07	did a bit of work towards getting rid of the session cookie and putting it all in a structure
					- not finished

WAb 2006-02-22 		tried to deal with case of people arriving back at site to find that they have been deduped.
					also made sure that session cookies are killed if the personid changes for some reason

WAB 2006-06-19		added browser detection

WAB 2007-07-04 		added updating of visit table on each request

WAB 2008-01-22		added  request.relaycurrentuser.showerrors

WAB 2008-01-29		updatePersonaldata now updates usergroups at the same time

WAb 2008-01-31          added checks for lots of bots, kill sessions and don't record visits

WAB 2008-02-05  code for SSO, included new functions for verifying users and adding impersonatedbyperson and sitedefid to visit and usage tables
				note ended up using  impersonatedbyperson rather than impersonatedbypersonid because large number of rows are created which we don't actually need to have deduplicated
GCC 2008-07-08 added with (nolock)

WAb 2008-09-16   WAB increased timeout on locks

WAB 2009-01-06   Added PRTG Network Monitor to list of bots (formerly IPCHECK I think); and BOCC (their uptime monitor)
WAB 2009-01-06   Added sparc-sun-solaris to list of bots This is Sony's call to our relaywebservice and needn't be recorded (takes up lots of space) would be good to haev a way of defining these better on a site by site basis
WAB 2009-05-18   Secure the user cookie against tampering
NYB 2009-05-29 	LHID 2306 Kill session on logout
NYB 2009-07-14 	P-FNL069 - changed setLoggedOut, added removeUserCookie arg, if true it deletes the User & DeJaVu cookies
GCC 2009-08-05  populate isocode for unknownuser, based on possible countryid
WAB 2009-09-14	delete temporary files on logout
WAB 2009-09-24	fix GCC 2009-08-05  to deal with possible countryid = 0 (eg bots)
WAB 2009-11-30	LID2890 Sony1, translation country changing when user profile updated
WAB 2010-06-14   P-FNL069 removed call to cf_browser
		Prevent clientTime javascript being put into output stream if request is JSON.  Since the first request in a session is now often an Ajax login, I now have to check that the timezone has been received
WAB 2010/06		change reference to serverIPAddress to instance.ipaddress
WAB 2010-10-06 removeUserCookie parameter replaced with setting security.internal\external.rememberUserName
WAB 2011-11-08  Major Varing
WAB 2012-02-09 Added switch to switch off using cfqueryparam for debugging
WAB 2012-06-30 Added Support for custom data to be added to relayCurrentUser during initialisation

2012-09-12 	PPB Case 430249 don't allow entry of commas in a numeric field (unless the users locale expects them as a decimal seperator)
2012-09-18	WAB	Case 430249 set session.RelayCurrentUserStructure.LocaleInfo.decimalInfo

2012-10-03  WAB Case 431022 RememberMe not working.  Discovered that some critical references to cookie.user within refresh() function had been changed to request.relayCurrentUser.personid during the security project.  Changed them back
2012-10-23	WAB	Case 431440 If the value of content.showForCountryID is updated then update the content cache as well
2013-01-15	NJH 2013Roadmap - added support for connections. Currently have linkedIn,facebook and organisation connections. Stored in the current user structure
2013-01-17  WAB P-LEX075 Reworked all the SSO type code in refresh() so that it is easily expandable.
2013-03-07	WAB	CASE 432630 Added countryName to relayCurrentUser.location & person - so can be used in merge
2013-03-28 NYB Case 434551 replaced 'if countrylist="" then make countrylist=countryid' with 'if countrylist does not contain countryid then add countryid to countrylist'
2013-03-?	WAB	Minor alteration to AuthenticationFilter_URLLogout
2013-05-28	WAB added a showLeadingWhiteSpace switch (which can used when debugging HTML)
2013-10-29 	NYB Case 437178 added checkForCountryOnFORM to set the country that displays to the one selected on the addContactForm
2013-11-06 NYB Case 437178 changed StructCopy of sessionScope.RelayCurrentUserStructure to request.RelayCurrentUser to Duplicate, and update Country based on 'not logged in', not isUnknown
2013-11-28 NYB Case 437968 put checkForCountryOnFORM before checkForLanguageOnFORM, and removed setShowContentForPersonID(personID=404) from checkForCountryOnFORM
2013-12-04	WAB CASE 438291 Replace cgi.remote_addr with call to com.security.getRemoteAddress - to deal with loadBalancer
2014-02-26 WAB remove cfid and cftoken from the visit table.  We don't use them and in CF10 they sometimes seem to no longer be of type int
2014-03-19	PPB	Case 439151 save the keysToPersist lists before clearing session so mechanism can be used repeatedly
2014-06-19	WAB CASE 440695 Moved code to collect client time zone to request.cfc, so that it can only appear when we are doing the  <head> tag
2015-10-07 WAB PROD2015-165 Security Scan.  Add queryParam to visit table insert query
2015-11-03	WAB Alter code which decided whether errors are dumped to screen to use the isTrusted() function
2015-11-04 DAN Case 446442 - add isMobile to the browser structure determined by HTTP_USER_AGENT
2015-12-01	WAB Perfomance improvement.  Don't increment visit.numberOfRequests for Ajax and MissingTemplateHandler
2015-05-19 	WAB CASE 444754 Rotate Session on Login/Logout
2016-02-24 	WAB Added 'doNotTranslate' key to the relayCurrentUser.content structure. Used to switch off onRequestEnd translation for debugging purposes. (note that it is in the negative so that OFF/FALSE is the default state - in the same way as debugging is usually OFF)
14/03/2016 DAN Case 448481 - fix for the visit insert SQL to use correct value for siteDefDomainID
2016-10-19	WAB	Minor change to debugging so that debugging/error handling settings are persisted during login
2016-11-15 WAB Move to using Entity objects for user.person/location/organisation
--->

<cfcomponent displayname="relayCurrentUser" hint="Used for accessing and updating the relayCurrentUser Structure">
	<cfset this.doNotKeepSessionOpenForThesePages = "amiup.cfm">
	<cfset this.BotList = "sparc-sun-solaris,PRTG Network Monitor,BOCC,IPCheck,IPSentry,feedburner,PHP,ISC Systems,Googlebot,topicblogs,slurp,feedparser,Mediapartners-Google,BuzzTracker,AOLserver,baiduspider,Spider,Galaxybot,Intelliseek,technorati,Feedfetcher,MSIECrawler,Msnbot,Nutch,YahooSeeker,ColdFusion,MXNA,YahooFeedSeeker,NewsGatorOnline,GreatNews,Snarfer,ShopWiki,Java/1,Moreoverbot,Python-urllib,DAUM RSS Robot,megite,twiceler,Windows Live Writer,cfschedule,Morfeus,echoping,surveybot">

	<cffunction access="public" name="refresh">


				<cfset var checkValid = '' />
				<cfset var existingSession = false />
				<cfset var previousIsLoggedIn = '' />
				<cfset var currentLoginStatus = '' />
				<cfset var filters = '' />
				<cfset var filterFunction = '' />
				<cfset var filterResult = '' />
				<cfset var statusChanged = '' />
				<cfset var filter = '' />
				<cfset var item = '' />
				<cfset var thisEntityID = "">
				<cfset var thisEntityTypeID = "">
				<cfset var serviceEntityID = "">

				<!--- load up the site structure --->
				<cfset application.com.relaycurrentsite.setcurrentSite()>

				<cflock name = "#getLockName()#" timeout="10" throwontimeout="Yes" type="EXCLUSIVE" >  <!--- WAB 2008-09-16 Increased timeout to 10 from 3--->

				<!---
					WAB 2013-01-17 This code was completely rewritten to allow for easy expansion for custom SSO type functions.
					The series of IF statements has been replaced by lots of individual functions with name AuthenticationFilter_xxxx
					These functions are called in turn and each checks for its own 'Token'
					See Wiki
				--->

				<!--- Check whether we are starting a new session (strictly speaking whether we already have a relaycurrentuser) or not --->
				<cfif (structKeyExists(session,"RelayCurrentUserStructure") and structKeyExists (session.RelayCurrentUserStructure,"personid")) >
					<cfset existingSession = true>

							<cfif not structKeyExists(cookie,"user")>
								<!---
									2006-05-22
									this is an odd case which we are trying to debug
									the session is all there but the cookie has disappeared (maybe temporarily)
									I am going to send myself an email and see what happens
									but for time being just refresh the user structure -
									may need to recreate the user cookie

									happens after using reset.cfm
								 --->

									<!---
									WAB 2009-05-18  replaced with function call
									<cfset application.com.globalfunctions.cfcookie (	NAME="USER"  ,VALUE="#cookieValue#-#cookiecheck#"  ,EXPIRES="#application.com.globalFunctions.getCookieExpiryDays()#"	)>
									--->
									<cfset 	setUserCookie (personid = session.RelayCurrentUserStructure.personid, usergroupid = session.RelayCurrentUserStructure.UserGroupID,isInternal = session.RelayCurrentUserStructure.isInternal)>

							<cfelseif listfirst(cookie.user,"-") is not session.RelayCurrentUserStructure.personid >
									<!--- cookie is not same as personid,
									most likely another frame has done a login or something
									so I am going to set the cookie to the new values and continue with refresh
									 --->

									<!---
									WAB 2009-05-18  replaced with function call
									<cfset application.com.globalfunctions.cfcookie (	NAME="USER"  ,VALUE="#cookieValue#-#cookiecheck#"  ,EXPIRES="#application.com.globalFunctions.getCookieExpiryDays()#"	)>
									--->
									<cfset 	setUserCookie (personid = session.RelayCurrentUserStructure.personid, usergroupid = session.RelayCurrentUserStructure.UserGroupID,isInternal = session.RelayCurrentUserStructure.isInternal)>



							</cfif>


						<cfset previousIsLoggedIn = session.RelayCurrentUserStructure.isLoggedIn>
						<cfset refreshIsLoggedInStatus()>
						<!--- TBD WAB 2013/01/22 - think about this, can it be done later --->
						<cfif previousIsLoggedIn is not session.RelayCurrentUserStructure.isLoggedIn >
							<!--- ie the login status has changed since the previous request, need to refresh contentTree --->
							<cfset refreshcontenttree () >
						</cfif>

						<cfset currentLoginStatus = {personid = session.RelayCurrentUserStructure.personID,level = session.RelayCurrentUserStructure.isLoggedIn}>

				<cfelse>
						<cfset existingSession = false>
						<cfset currentLoginStatus = {personid = -1,level = -1,isInternal = request.currentSite.isInternal}>
				</cfif>

				<!--- Loop through all the filters until one is used
				--->
				<cfset filters = getAuthenticationFilters(existingSession = existingSession)>
					<cfloop array="#filters#" index="filter">
						<cfset filterFunction = this[filter.name]>
						<cfset filterResult = filterFunction(argumentCollection = currentLoginStatus)>  <!--- currentPersonID = session.RelayCurrentUserStructure.personID,currentIsLoggedIn = session.RelayCurrentUserStructure.isLoggedIn,isInternal = session.RelayCurrentUserStructure.isInternal  --->
						<cfif filterResult.filterUsed>
								<cfbreak>
						</cfif>
					</cfloop>


				<!---
					make use of the result of the filter to initialise the user, log them in etc.
					For an existing session a filter may not have been activated,
					But for a new session we always have a fallback filter (AuthenticationFilter_UnknownUser) which will cause relaycurrentuser to be initialised
				--->
				<cfif filterResult.filterUsed>

					<!--- check whether anything has changed, if not then we don't have to do anything--->
					<cfset statusChanged = false>
					<cfloop collection=#currentLoginStatus# item="item">
						<cfif structKeyExists (filterResult,item) AND currentLoginStatus[item] is not filterResult[item]>
							<cfset statusChanged = true>
							<cfbreak>
						</cfif>
					</cfloop>

					<cfif statusChanged>

						<cfif currentLoginStatus.personid is not filterResult.personid>
							<!--- if the personid has changed for an existing session then we need to reinitialise - strictly speaking we ought to have a new sessionID, but no way of doing that without a reload of some sort
								for a new session (the personid will have changed) just initialise
							--->
							<cfif existingSession>
								<cfset reinitialise(filterResult.personid)>
							<cfelse>
								<cfset session.RelayCurrentUserStructure = init(personid = filterResult.personid)>
							</cfif>
						</cfif>

						<cfif filterResult.level is 0 and currentLoginStatus.level GT 0>
							<!--- if level has been changed from >0 to 0 then it is a logout--->
							<cfset application.com.relaycurrentuser.setLoggedOut()>
						<cfelseif filterResult.level GT 0 and filterResult.personid is not 0>
							<!--- level > 0 need to to a login, but need to validate person on this site (but mustn't log in personid 0 )--->
							<cfset checkValid = application.com.login.isPersonAValidUserOnTheCurrentSite (filterResult.personID)>
							<cfif not checkValid.isValidUser>
								<cfset filterResult.level = 0>  <!--- so won't get logged in, but we can set the user cookie --->
							</cfif>
								<cfset setLoggedInPrivate(argumentCollection = filterResult,doreinitialisation=false)>
								<cfif session.RelayCurrentUserStructure.isInternal>

								<cfelse>
									<!--- external site --->
									<cfset refreshcontenttree () >

								</cfif>

						</cfif>

					</cfif>
				</cfif>




				<cfif existingSession>
	 					 	<!--- if the user changed their browser language then we update the language variables --->
							<cfset checkForBrowserLanguageChange () >

							<cfset fnUpdateVisit()>  <!--- added WAB 2007-07-04 personid  --->

							<!--- GCC 2005-08-10 added to cater for language picker - /relaytagtemplates/ChangeRelayLanguage.cfm --->
							<!--- START: 2013-11-28 NYB Case 437968 swapped the order round: --->
							<cfset checkForCountryOnFORM () >  <!--- 2013-10-29 NYB Case 437178 added --->
							<cfset checkForLanguageOnFORM () >
							<!--- END: 2013-11-28 NYB Case 437968 swapped the order round --->
							<cfset checkForLanguageOnURL () >

							<!---  WAB 2014-06-19 CASE 440695 moved to request.cfc
							<cfif not session.RelayCurrentUserStructure.localeInfo.clientUTCreceived>
								 <cfset outputGetClientTimezoneCode()>
							</cfif>
							--->

							<cfset arrayprepend (session.RelayCurrentUserStructure.previousPages, duplicate(session.RelayCurrentUserStructure.currentPage)) >  <!---  --->
							<cfif arrayLen (session.RelayCurrentUserStructure.previousPages) gt 5> <!--- WAB 2012-10-17 reduced from 10 to 5, takes less space when saved in error structure and straw poll suggests not used much --->
								<cfset arrayDeleteAt(session.RelayCurrentUserStructure.previousPages,arrayLen (session.RelayCurrentUserStructure.previousPages))>
							</cfif>

							<cfset session.RelayCurrentUserStructure.currentPage = duplicate(getCurrentPageDetails ())>

							<!--- save time of the current Request --->
							<cfset session.RelayCurrentUserStructure.requestTime = now() >
							<cfset session.RelayCurrentUserStructure.PageCount = session.RelayCurrentUserStructure.PageCount + 1>

							<cfif (structKeyExists(url,"openedby"))> <!--- this is a variable I introduced to try and allow cross session control of content - so the opening session is allowed to update sessions it has opened --->
								<cfset session.RelayCurrentUserStructure.sessionOpenedBy = url.openedby >
							</cfif>


				<cfelse>
					<!--- this is the case of a brand new session --->
					<cfset application.com.clusterManagement.updateInstanceJsessionID()>

				</cfif>

				<cfset copyToRequestScope ()>

				<!---
					This is a case where someone has logged into Relayware and is linking their RW account with LinkedIn. We will
					have a token and verifier from LinkedIn, and we will set the link between RW and LinkedIn.

					I think this will only be the case for LinkedIn, as there will only be a 'LinkYourAccount' button with linkedIn.

					if the user has logged in and we have an accessToken, store the token in the db for future reference
				--->
				<cfif request.RelayCurrentUser.isLoggedIn and ((structKeyExists(url,"oauth_token") and structKeyExists(url,"oauth_verifier")) or (structKeyExists(url,"code"))) and not structKeyExists(filterResult,"loginWithService")>

					<!--- NJH 2013/02/20 part of spring 4 item 13 - pass through entityID and entityTypeID if they are defined as it may not always be the current user (ie. twitter account) --->
					<cfset thisEntityID = request.relayCurrentUser.personID>
					<cfset thisEntityTypeID = application.entityTypeID.person>
					<cfif structKeyExists(url,"entityID") and structKeyExists(url,"entityTypeID")>
						<cfset thisEntityID = url.entityID>
						<cfset thisEntityTypeID = url.entityTypeID>
					</cfif>

					<!--- the accessToken is defined up earlier in this function --->
					<cfset CreateObject("component", "oauth.oauthtoken").setAccessToken(accessToken=filterResult.accessToken,serviceID=filterResult.serviceID,entityID=thisEntityID,entityTypeID=thisEntityTypeID)>

					<!--- set the link between the user and the service.. store the user's service ID --->
					<cfif not application.com.service.hasEntityBeenLinked(serviceID=filterResult.serviceID,entityID=thisEntityID,entityTypeID=thisEntityTypeID).linkEstablished>
						<cfset serviceEntityID = application.com.service.getProfile(serviceID=filterResult.serviceID,fieldList="id",entityID=thisEntityID,entityTypeID=thisEntityTypeID).profile.ID>

						<cfif not application.com.service.doesEntityExistWithServiceEntityID(serviceID=filterResult.serviceID,ServiceEntityID=serviceEntityID,entityTypeID=thisEntityTypeID).entityExists>
							<cfset application.com.service.linkEntityToService(serviceID=filterResult.serviceID,entityID=thisEntityID,entityTypeID=thisEntityTypeID)>
						<cfelse>
							<cfset url.message = "phr_social_accountHasAlreadyBeenConnected">
							<cfset request.errorMessage = url.message>
						</cfif>
					</cfif>
				</cfif>

			</cflock>

				<!--- load up the definition of the current tree --->
				<cfset application.com.relaycurrentsite.setcurrentTree()>

	</cffunction>

	<cffunction name="getAuthenticationFilters">
		<cfargument name="existingSession" type="boolean" required>

		<cfset var type = "all">
		<cfset var allFilters = "">
		<cfset var existingSessionFilters = "">
		<cfset var i = "">

		<cfif existingSession>
			<cfset type="existingSession">
		</cfif>

		<cfif not structKeyExists(variables,"authenticationFilters")>
			<cfset allfilters = application.com.globalfunctions.getArrayOfCFCMethods(this,"authenticationFilter_","Index","100")>

			<!--- Remove Items where Active=FALSE --->
			<cfloop from="#arrayLen(allfilters)#" to="1" step="-1" index="i">
				<cfif structKeyExists(allfilters[i],"Active") and not allfilters[i].Active>
					<cfset arrayDeleteAt(allfilters,i)>
				</cfif>
			</cfloop>

			<cfset variables.authenticationFilters = {all = allfilters}>
			<cfset existingSessionFilters = duplicate(allfilters)>

			<!--- Remove Items where Active=FALSE --->
			<cfloop from="#arrayLen(existingSessionFilters)#" to="1" step="-1" index="i">
				<cfif structKeyExists(existingSessionFilters[i],"newSessionOnly") and existingSessionFilters[i].newSessionOnly>
					<cfset arrayDeleteAt(existingSessionFilters,i)>
				</cfif>
			</cfloop>

			<cfset variables.authenticationFilters.existingSession = existingSessionFilters>

		</cfif>

		<cfreturn variables.authenticationFilters[type]>

	</cffunction>

	<cffunction access="public" name="reinitialise" >
		<cfargument name="personid" type="numeric" required="true">

		<cfset	var temp =structNew()>
		<cfset	var sessionExisted = false >
		<cfset	var keyname = ''>
		<cfset	var i = ''>

		<cflock name = "#getLockName()#" timeout="10" throwontimeout="Yes" type="EXCLUSIVE" >
		<cfscript>

			temp.time = now() ;
			temp.PageCount	= 0 ;
			temp.sessionid = session.sessionid; // WAB 2009-06-09
			// if session.RelayCurrentUserStructure already exists and is valid (which it usually will) we save the session started time and a few other bits
			if (structKeyExists(session,"RelayCurrentUserStructure") and structKeyExists (session.RelayCurrentUserStructure,"personid")) {
				sessionExisted = true;
				temp.Time = session.RelayCurrentUserStructure.sessionStarted;
				temp.PageCount = session.RelayCurrentUserStructure.pageCount;
				temp.CurrentPersonID = session.RelayCurrentUserStructure.personid;
				temp.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse = session.RelayCurrentUserStructure.Content.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse ;
				temp.ShowTranslationLinks = session.RelayCurrentUserStructure.Content.ShowTranslationLinks;
				temp.ItemsToPersistAcrossLogins = structNew() ;
				temp.ItemsToPersistAcrossLoginsAndUsers = {errors = session.RelayCurrentUserStructure.errors} ;  /* WAB 2016-10-19  Aids debugging of logins and not a security issue since ultimately controlled by IPAddress */
				for (i=1;i lte listlen(session.RelayCurrentUserStructure.keysToPersistAcrossLoginsAndUsers);i=i+1)
					{
						keyname = listgetat(session.RelayCurrentUserStructure.keysToPersistAcrossLoginsAndUsers,i);
						if (structKeyExists(session.RelayCurrentUserStructure,keyname)) {
							temp.ItemsToPersistAcrossLoginsAndUsers [keyname] = Duplicate(session.RelayCurrentUserStructure[keyname]) ;
						}
					}
				for (i=1;i lte listlen(session.RelayCurrentUserStructure.keysToPersistAcrossLogins);i=i+1)
					{
						keyname = listgetat(session.RelayCurrentUserStructure.keysToPersistAcrossLogins,i);
						if (structKeyExists(session.RelayCurrentUserStructure,keyname)) {
							temp.ItemsToPersistAcrossLogins [keyname] = Duplicate(session.RelayCurrentUserStructure[keyname]) ;
						}
					}

				// START 2014-03-19 PPB Case 439151 save the keysToPersist so mechanism can be used more than once
				temp.ItemsToPersistAcrossLogins ['keysToPersistAcrossLogins'] = Duplicate(session.RelayCurrentUserStructure['keysToPersistAcrossLogins']) ;
				temp.ItemsToPersistAcrossLoginsAndUsers ['keysToPersistAcrossLoginsAndUsers'] = Duplicate(session.RelayCurrentUserStructure['keysToPersistAcrossLoginsAndUsers']) ;
				// END 2014-03-19 PPB Case 439151

				temp.localeInfo = structCopy (session.RelayCurrentUserStructure.localeInfo) ;
			}

			/* 2015-05-19 	WAB CASE 444754 Rotate the Session.
							also clears current session scope
							New session is actually created at end of request (otherwsie we would not be able to set values into the session Scope.  Doing it this way allows values to be set and carried over to the new session)
			 */
			application.com.request.rotateSession ( delay = true, clear = true) ;  // WAB 2009-06-09, clear out all existing stuff from the session
			session.RelayCurrentUserStructure = init(personid);

			if (sessionExisted) {
				session.RelayCurrentUserStructure.sessionStarted = temp.Time ;
				session.RelayCurrentUserStructure.PageCount = temp.PageCount + 1;

				if (temp.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse) {
					session.RelayCurrentUserStructure.content.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse = true ;
					session.RelayCurrentUserStructure.content.ShowLinkToControlContent = true;
					session.RelayCurrentUserStructure.Content.ShowTranslationLinks = temp.ShowTranslationLinks ;  // handy to remember across logins when content editors are testing logins and registrations
				}


				if (temp.CurrentPersonID is session.RelayCurrentUserStructure.personid) {
					for (keyname in temp.ItemsToPersistAcrossLogins)
					{
						session.RelayCurrentUserStructure[keyname] = temp.ItemsToPersistAcrossLogins [keyname];
					}
				}

				for (keyname in temp.ItemsToPersistAcrossLoginsAndUsers)
					{
						session.RelayCurrentUserStructure[keyname] = temp.ItemsToPersistAcrossLoginsAndUsers [keyname];
					}

//				session.RelayCurrentUserStructure.ItemsToPersistAcrossLogins = temp.ItemsToPersistAcrossLogins ;

				if (temp.CurrentPersonID is not session.RelayCurrentUserStructure.personid) {
					// session.RelayCurrentUserStructure.ItemsToPersistAcrossLogins.previousPersonIDsInThisSession = listappend (temp.ItemsToPersistAcrossLogins.previousPersonIDsInThisSession,temp.CurrentPersonID);
					session.RelayCurrentUserStructure.previousPersonIDsInThisSession = listappend (session.RelayCurrentUserStructure.previousPersonIDsInThisSession,temp.CurrentPersonID);
				}
				// these variables only come via javascript after a page has been rendered, so we save the ones which we already have
				session.RelayCurrentUserStructure.localeInfo= temp.localeinfo;

			}

			copyToRequestScope () ;
		</cfscript>
		</cflock>

			<!--- WAB 2006-11-01 decided that after a reinitialisation we should re-run relayINI --->
		<!--- <cf_include template="\code\cftemplates\relayINI.cfm" checkIfExists="true"> --->
		<!--- <cfif fileexists("#application.paths.code#\cftemplates\relayINI.cfm")>
			<!--- relayINI is used to over-ride default global application variables and params --->
			<cfinclude template="/code/cftemplates/relayINI.cfm">
		</cfif> --->


	</cffunction>



	<cffunction access="public" name="setShowContentForPersonID" >
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="sessionScope" default="#session#">

		<cfscript>
			var data = sessionScope.RelayCurrentUserStructure ;
			var query = application.com.globalfunctions.CFQUERY("select countryid from location l inner join person p on p.locationid = l.locationid where personid = #personid#");
			data.Content.ShowForPersonID = personid ;
			data.Content.ShowForCountryID = query.countryid ;
			if (query.recordcount is 0 or personid is 0) {
				data.Content.ShowForCountryID = 0 ;		// will give content where there are no country rights set
			} else if (personid is -1){
				data.Content.ShowForCountryID = -1 ;    // will give content regardless of country rights
			} else {
				data.Content.ShowForCountryID = query.countryid ;
			}

			copyToRequestScope (sessionscope = sessionscope) ;
		</cfscript>
	</cffunction>


	<cffunction access="public" name="setShowContentForCountryID" >
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="sessionScope" default="#session#">

		<cfscript>
			var data = sessionScope.RelayCurrentUserStructure ;
			var currentValue = data.Content.ShowForCountryID;
			data.Content.ShowForCountryID = countryid ;

			// WAB 2012-10-23 CASE 431440   if countryid has changed then refresh the content tree
			if (currentValue is not countryid) {refreshContentTree();}

			copyToRequestScope (sessionscope = sessionscope) ;
		</cfscript>
	</cffunction>

	<cffunction access="public" name="setLanguage" return= "numeric" hint ="Set LanguageID (for Translation) can be either ID, ISOCode or Language Name.  Returns LanguageID set or 0 if language not valid">
		<cfargument name="Language" type="string" required="yes">
		<cfargument name="sessionScope" default="#session#">


		<cfscript>
			var data = sessionScope.RelayCurrentUserStructure ;
			var languageIDLookupStr = structnew();
			var languagelookupfromidstr = structnew();
			var languageIDLookupFRomNameStr = structnew();
			var x = "";
			var countryLanguageList = "";

			if (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList) {
				countryLanguageList = application.com.relayTranslations.getCountryLanguages(countryID = data.content.showForCountryID);
				languageIDLookupStr = request.languageIDLookupStr;
				languagelookupfromidstr = request.languagelookupfromidstr;
				languageIDLookupFRomNameStr = request.languageIDLookupFRomNameStr;
			} else {
				languageIDLookupStr = application.languageIDLookupStr;
				languagelookupfromidstr = application.languagelookupfromidstr;
				languageIDLookupFRomNameStr = application.languageIDLookupFRomNameStr;
			}

			if (isNumeric(Language) and arraylen(structfindvalue(languagelookupfromidstr,language)) is not 0 ) {
				data.languageid = Language;

			} else if (len(language) is 2 and structKeyExists(languageidlookupstr,language) ) {
				data.languageid = languageidlookupstr[language] ;

			} else if (arraylen(structfindvalue(languageIDLookupFRomNameStr,language)) is not 0  ) {
					x = structfindvalue(languageIDLookupFRomNameStr,language);
					data.languageid = languageIDLookupFRomNameStr[x[1].key];

			} else {
				if (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList) {
					data.languageid = listfirst(countryLanguageList,",");
					//grab the country default
				} else {
				// language not recognised so nothing changed
					return 0  ;
				}
			}

			// having set a languageid, set the language isocode as well
			data.languageisocode = application.languageisocodelookupstr[data.languageid];

			copyToRequestScope (sessionscope = sessionscope) ;
			return data.languageid ;
		</cfscript>

	</cffunction>


	<!--- 	2016-02-24 	WAB Added 'doNotTranslate' key
			2016-03-04	WAB	But somehow the code to do the update got lost, now restored!
	--->
	<cffunction access="public" name="updateContentSettings"  hint ="alter various settings for showing different content">
			<cfargument name="Status" type="numeric">
			<cfargument name="countryid" type="numeric">
			<cfargument name="ShowTranslationLinks" type="boolean">
			<cfargument name="doNotTranslate" type="boolean">
			<cfargument name="contentdate" type="string">
			<cfargument name="showElementIDs" type="string">
			<cfargument name="additionalshowElementIDs" type="string">
			<cfargument name="additionalshowElementIDsIfStatusCorrect" type="string">
			<cfargument name="ShowLinkToControlContentEvenIfILogOnAsSomeoneElse" type="boolean">
			<cfargument name="ShowLinkToControlContent" type="boolean" >
			<cfargument name="sessionScope" default="#session#">

			<cfset var x = "">
			<cfset var item = "">
			<cfset var data = "" >

			<cflock timeout="10" throwontimeout="Yes" name="#getLockName()#" type="EXCLUSIVE">
				<cfset data = sessionScope.RelayCurrentUserStructure >

			<cfscript>

				if (isDefined('Status')) {
					if (status gte 0 and status lte 5 ) {
						data.Content.Status = status ;
					}
				}

				if (isDefined('countryid')) {
					data.Content.ShowForCountryID = countryid ;
				}

				if (isDefined('ShowTranslationLinks')) {
					data.Content.ShowTranslationLinks = ShowTranslationLinks ;
				}

				if (isDefined('arguments.doNotTranslate')) {
					data.Content.doNotTranslate = doNotTranslate ;
				}

				if (isDefined('contentDate')) {
					data.Content.date = contentDate ;
					if (contentdate is "") {
						data.Content.showForDifferentDate = false ;
						} else {
						data.Content.showForDifferentDate = true ;
					}
				}

				if (isDefined('ShowElementIDs')) {

					data.Content.AlwaysShowTheseElementIDs = ShowElementIDs ;
				}


				if (isDefined('AdditionalShowElementIDs')) {
					for (x=1;x lte listlen(AdditionalshowElementIDs); x=x+1) {
						item = listgetat(AdditionalshowElementIDs,x) ;
						if (not listfind(data.Content.AlwaysShowTheseElementIDs,item))
						{
								data.Content.AlwaysShowTheseElementIDs  = listappend(data.Content.AlwaysShowTheseElementIDs ,item) ;
						}
					}
				}

				if (isDefined('AdditionalshowElementIDsIfStatusCorrect')) {
					for (x=1;x lte listlen(AdditionalshowElementIDsIfStatusCorrect); x=x+1) {
						item = listgetat(AdditionalshowElementIDsIfStatusCorrect,x) ;
						if (not listfind(data.Content.AlwaysShowTheseElementIDsIfStatusCorrect,item))
						{
								data.Content.AlwaysShowTheseElementIDsIfStatusCorrect  = listappend(data.Content.AlwaysShowTheseElementIDsIfStatusCorrect ,item) ;
						}
					}
				}

				if (isDefined('ShowLinkToControlContentEvenIfILogOnAsSomeoneElse')) {
					data.Content.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse  = ShowLinkToControlContentEvenIfILogOnAsSomeoneElse ;
				}

				if (isDefined('ShowLinkToControlContent')) {
					data.Content.ShowLinkToControlContent = ShowLinkToControlContent ;
				}


				data.Content.refreshBeforeThisDate = now() ;

				copyToRequestScope (sessionscope = sessionscope) ;


		</cfscript>
		</cflock>







	</cffunction>




	<cffunction access="public" name="refreshContentTree"  hint = "use if you want this user's content tree to be refreshed">
		<!--- use application.com.relaycurrentuser.refreshContentTree() --->
		<cfset	var data = session.RelayCurrentUserStructure >
		<cflock timeout="10" throwontimeout="Yes" name="#getLockName()#" type="EXCLUSIVE">
		<cfscript>
				data.refreshContentBeforeThisDate = now() ;   // old variable to be removed
				data.refreshContentTree =true ; // old variable to be removed
				data.Content.refreshBeforeThisDate = now() ;
				copyToRequestScope() ;
		</cfscript>
		</cflock>

	</cffunction>

<!---
	<cffunction access="public" name="ContentTreeRefreshed"  hint = "use when the user's content tree has been refreshed">
		<!--- use application.com.relaycurrentuser.ContentTreeRefreshed() --->
		<cfscript>
				var data = session.RelayCurrentUserStructure ;
				data.refreshContentTree = 	false;
				copyToRequestScope() ;
		</cfscript>

	</cffunction>
 --->

	<!---
	2009-11-30 LID2890  WAB
	 --->

	<cffunction access="public" name="updatePersonalData" >
		<cfset var data = session.RelayCurrentUserStructure >
		<cfset var savedLanguage = data.person.language > <!---  save language so that we can detect a change  --->
		<cfset var savedShowForCountryID = data.content.showforcountryid > <!---  LID2890  WAB --->
		<cfset var savedCountryID = data.location.countryid> <!---  LID2890  WAB --->


		<cflock timeout="10" throwontimeout="Yes" name="#getLockName()#" type="EXCLUSIVE">
		<cfscript>

			data = setPersonalData(data) ;
			// if profile language has changed then change language
			if (data.person.language is not savedLanguage) {
				setLanguage(data.person.language) ;
			}

			/* 2009-11-30 Bug LID2890  WAB
			  Bug with Sony webservices.  A showforcountryid has been set for the session by using a locale,
			  but then the user is taken to a page to update their profile and the showforcountryid was getting reset to the value from the db

			 So if the user data has been updated we don't want the showforcountryid to be if it has been changed deliberately in an earlier request
			 [except maybe if the user has changed their country (actually quite unlikely in real world scenario)]
			*/

			if (savedShowForCountryID is not data.content.showforcountryid  and data.location.countryid IS savedCountryID) {
				data.content.showforcountryid = savedShowForCountryID ;
			}


		</cfscript>
		</cflock>

		<cfset refreshContentTree () >  <!--- incase flags and things have changed --->
		<cfset copyToRequestScope () >
	</cffunction>

	<cffunction access="private" name="copyToRequestScope" >
		<cfargument name="sessionScope" default="#session#"	>
		<cflock timeout="10" throwontimeout="Yes" name="#getLockName(sessionScope = sessionScope)#" type="EXCLUSIVE">
			<cfscript>
			request.RelayCurrentUser = Duplicate(sessionScope.RelayCurrentUserStructure);  //2013-11-06 NYB Case 437178 changed from StructCopy to Duplicate
			// set the locale for the whole request
			setlocale (sessionScope.RelayCurrentUserStructure.LocaleInfo.Locale);
			</cfscript>
		</cflock>
	</cffunction>


	<cffunction access="private" name="refreshIsLoggedInStatus" >
			<cfset var timeoutMinutes = application.com.settings.getSetting("security.#iif(session.RelayCurrentUserStructure.isInternal,de('internalSites'),de('externalSites'))#.sessionTimeout_Minutes")>
			<cfif session.RelayCurrentUserStructure.isLoggedIn>
				<!--- if this request is within #application. TimeOut#  minutes of the last request then the session remains active--->
				<cfif session.RelayCurrentUserStructure.isLoggedInLastRefreshTime is not "" and DateDiff("n",session.RelayCurrentUserStructure.isLoggedInLastRefreshTime,Now()) LE timeoutMinutes >
					<cfif not structKeyExists(url,"notickle")>
					<CFSET session.RelayCurrentUserStructure.isLoggedInLastRefreshTime = now()>
					</cfif>
				<cfelse>
					<CFSET session.RelayCurrentUserStructure.isLoggedIn = 0>
					<CFSET session.RelayCurrentUserStructure.isLoggedInLastRefreshTime = "">
					<cfset session.RelayCurrentUserStructure.isLoggedInExpired = true>
				</cfif>
			</cfif>
	</cffunction>

	<cffunction access="private" name="checkForBrowserLanguageChange">
			<cfscript>
				// has the user changed their browser language?
				// if so we update their language setting
				if (structKeyExists(cgi,"HTTP_ACCEPT_LANGUAGE") and cgi.HTTP_ACCEPT_LANGUAGE is not "") {

					if (cgi.HTTP_ACCEPT_LANGUAGE is not session.RelayCurrentUserStructure.HTTP_ACCEPT_LANGUAGE ) {
						// note that the actual language may not have changed, or the language selected may not be live, but we try and change it anyhow
						setLanguage(application.com.relayTranslations.getUsersPreferredLangFromBrowser())  ;
						session.RelayCurrentUserStructure.HTTP_ACCEPT_LANGUAGE	= cgi.HTTP_ACCEPT_LANGUAGE ;

						// WAB 2007-11
						// if the language has changed then may need to change the locale as well
						session.RelayCurrentUserStructure.LocaleInfo.Locale = getUserLocale (session.RelayCurrentUserStructure.content.showForCountryID) ;
						session.RelayCurrentUserStructure.LocaleInfo.dateType = getDateTypeOfLocale (session.RelayCurrentUserStructure.LocaleInfo.Locale) ;
						session.RelayCurrentUserStructure.LocaleInfo.decimalInfo = getLocaleDecimalInfo (session.RelayCurrentUserStructure.LocaleInfo.Locale) ; 	//2012-09-18 WAB added line
					}
				}
			</cfscript>
	</cffunction>

	<cffunction access="private" name="checkForLanguageOnURL">
			<cfscript>

				var theLang = "" ;
				if (structKeyExists(URL,"lang")) {
					if (listlen(url.lang,"-") eq 2) {
						theLang = listFirst(URL.lang,"-");
					} else {
						theLang = URL.Lang;
					}
					setLanguage(theLang) ;
				}
			</cfscript>
	</cffunction>

	<cffunction access="private" name="checkForLanguageOnFORM">
		<cfscript>
			// Have they chosen a language from a language selector?
			// if so we update their language setting
			if (isDefined("FORM.frmShowLanguageID") and isNumeric(FORM.frmShowLanguageID) AND FORM.frmShowLanguageID is not "") {
				setLanguage(FORM.frmShowLanguageID) ;
			}
		</cfscript>
	</cffunction>

	<!--- START: 2013-10-29 NYB Case 437178 created function: --->
	<cffunction access="private" name="checkForCountryOnFORM">
		<cfscript>
			if (not session.relaycurrentuserStructure.ISLOGGEDIN) {
				if (isDefined("FORM.insCountryID") and isNumeric(FORM.insCountryID) AND FORM.insCountryID is not "") {
					setShowContentForCountryID(FORM.insCountryID) ;
					//setShowContentForPersonID(personID=404) ;  //2013-11-28 NYB Case 437968 removed
				}
			}
		</cfscript>
	</cffunction>
	<!--- END: 2013-10-29 NYB Case 437178 created function: --->

	<!--- WAB 2006-02-07 this function underdevelopment --->
	<cffunction access="public" name="setLoggedInPrivate">
		<cfargument name="personid" type="numeric" required=true>
		<cfargument name="level" required=true>
		<cfargument name="doReinitialisation" default = "true">
		<cfargument name="impersonatedByPerson" default = "0"><!---  WAB 2008-02-05  added argument  --->
		<cfargument name="source" type="string" default = ""> <!--- NJH 2012/01/20 Social Media --->

		<cfset var updateVisit = '' />

		<!---  re-initialise the user structure--->
		<cflock timeout="10" throwontimeout="Yes" name="#getLockName()#" type="EXCLUSIVE">
			<cfif doReinitialisation>
				<cfset reinitialise (personid)>
			</cfif>

			<cfif personid is not application.unknownpersonid and  personid is not 0 and (level is 1 or level is 2)>
				<!--- belt and braces check to make sure that only a valid internal user gets into the internal site  --->
					<cfif (session.relaycurrentuserstructure.isInternal and not session.relaycurrentuserstructure.isValidRelayInternalUser) >
						<cfset session.RelayCurrentUserStructure.isLoggedIn = 0>
						<cfset session.RelayCurrentUserStructure.isLoggedInLastRefreshTime = "">

					<cfelse>
						<cfset session.RelayCurrentUserStructure.isLoggedIn = level>
						<cfset session.RelayCurrentUserStructure.isLoggedInExpired = false>
						<cfset	session.RelayCurrentUserStructure.isLoggedInLastRefreshTime = now() >
						<cfset application.com.login.insertUsage(personid = PersonID,level = level, impersonatedByPerson = impersonatedByPerson,source=arguments.source,visitID=session.relaycurrentuserstructure.visitid)>
						<!---2015/05/13 PYW Case 444667 - extend cfcookie to set httponly flag --->
						<!--- <cfset application.com.globalFunctions.cfcookie(name="relayLogin" value="#application.applicationname#"  httponly="true")> --->
						<!--- NJH 2016/11/10 - hackathon project.. changes to Yan's initial work.. set showLink to true if user is in RW organisation --->
						<cfif session.RelayCurrentUserStructure.organisationID eq 1>
							<cfset session.RelayCurrentUserStructure.errors.showLink = true>
						</cfif>

						<cfset application.com.globalFunctions.cfcookie(name="relayLogin", value="#application.applicationName#")>

						<cfquery name="updateVisit" datasource="#application.sitedatasource#">
						update visit
							set
							numberOfLogins	= numberOfLogins + 1,
							impersonatedbyPerson  =  <cfqueryparam value="#impersonatedByPerson#" CFSQLTYPE="cf_sql_integer" >
							where visitID =  <cfqueryparam value="#session.relaycurrentuserstructure.visitid#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfquery>

					</cfif>
			</cfif>
		</cflock>
	</cffunction>


	<!--- WAB March 2006 --->
	<cffunction access="public" name="setLoggedIn">
		<cfargument name="personid" type="numeric" required=true>
		<cfargument name="level" required=true>
		<cfargument name="impersonatedByPerson" default = "0"> <!---  WAB 2008-02-05  added argument  --->
		<cfargument name="source" type="string" default = ""> <!--- NJH 2012/01/20 Social Media --->

		<cflock name = "#getLockName()#" timeout="10" throwontimeout="Yes" type="EXCLUSIVE" >
			<cfset setLoggedInPrivate (personid = personid, level = level, impersonatedByPerson = impersonatedByPerson,source=arguments.source )>
			<cfset  copyToRequestScope () >
		</cflock>
	</cffunction>


	<cffunction access="public" name="setLoggedOut">
		<!--- START: NYB 2009-07-14 P-FNL069 - added --->
		<cfargument name="cookieList" default="language,session,partnersession,previousSearches,appname,pid,cfid,cftoken,jsessionid,defaultLANGUAGEid,locatorLocation">
		<!--- WAB 2010-10-06 removeUserCookie parameter replaced with setting security.internal\external.rememberUserName_days
		<cfargument name="removeUserCookie" type="boolean" default="false">
		--->

		<cfset var rememberUserNameDays = application.com.settings.getSetting("security.#iif(request.currentsite.isInternal,de('internalSites'),de('externalSites'))#.rememberUserName_days")>
		<cfset var jasperServerUrl = "">
		<cfset var jasperServerFunctions = "">
		<cfset var updateVisit = '' />

		<CFIF not rememberUserNameDays>
			<cfset application.com.login.unSetRememberMeCookie()>
			<cfset arguments.cookieList = ListAppend(arguments.cookieList, "User")>
		</CFIF>
		<!--- END: NYB 2009-07-14 P-FNL069 - added --->

		<cflock name = "#getLockName()#" timeout="10" throwontimeout="Yes" type="EXCLUSIVE" >
			<cfscript>
				session.RelayCurrentUserStructure.isLoggedIn = 0;
				session.RelayCurrentUserStructure.isLoggedInLastRefreshTime = "" ;
			</cfscript>

				<cfquery name="updateVisit" datasource="#application.sitedatasource#">
				update visit
					set
					numberOfLogouts	= numberOfLogouts + 1
					where visitID =  <cf_queryparam value="#session.relaycurrentuserstructure.visitid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

			<cfset application.com.globalFunctions.cfcookie(name="relayLogin", value="", expires="now")>
			<!--- <cfset application.com.globalFunctions.cfcookie(name="relayLogin" value="" expires="NOW")> --->

			<!--- NJH 2010-11-30 - if we're logged in to JasperServer, then logout when logging out of Relayware --->
			<cfif session.relayCurrentUserStructure.jasperServerIsLoggedIn>
				<cfset jasperServerFunctions =  createobject("component","relay.com.jasperServer")>
				<cfset jasperServerUrl = "#jasperServerFunctions.getJasperServerPath()#/exituser.html">
				<cfhtmlhead text="<img src='#jasperServerUrl#' width=0 height=0/>">
				<cfset setJasperServerIsLoggedIn(isLoggedIn=false)>
			</cfif>

			<!--- WAB 2009-09-14 as part of LID 2512 - Sophos secure files --->
			<cfset application.com.filemanager.tempFileCleanupForCurrentSession()>

			<!--- START:  NYB 2009-05-29 LHID 2306
			WAB comment - how about clearing all cookies except ones which we specifically need to keep.  Not sure how many of these are used these days
			--->
			<cfset  clearCookies (cookieList = "dejavu")> <!--- WAB 2010-12-06 RememberMe cookie must be cleared at logout - otherwise user is just logged in again and we end up in a vicious circle --->
				<cfset  clearCookies (cookieList = cookieList)>  <!--- NYB 2009-07-14 P-FNL069 - replaced list with variable --->
				<cfset  deleteThisSession() >
			<!--- WAB this is an alternative approach which allows us to persist particular settings between logins (allows content editors to change content settings and then login as someone else (such as a test account) who isn't a content editor but retain rights
				reinitialise recreates a new relaycurrentuser and now clears out the session structure and
				<cfset reinitialise (session.relaycurrentuserstructure.personid)>
			--->

			<!--- 	this does not work, cause session.sessionid is referenced later on <cfset StructClear(session)> --->

			<!--- END:  2009-05-29 LHID 2306 --->

			<cfset  copyToRequestScope () >
		</cflock>
	</cffunction>


	<cffunction access="private" name="init" returntype="struct">
		<cfargument name="personid" type="numeric" required="true">
		<cfscript>
			var queryResult = '';
			var getCountries = '';
			var getRegions = '';
			var personRecord = '';
			var dedupedPerson = '';
			var x = '';
			var browserLang = '';
			var locale = '';
			var countryDefaultLanguageid = '';
			var changeSessionTimeout = 0;
			var tempStruct = structNew() ;
			var botarray = arrayNew(1);
			var botarrayLength = 0 ;
			var dontRecordVisit =  false;
			var browser = '';
			var i = '' ;
			var query = '' ;
			var usergroups = '' ;
			var showErrorIDLinkToTheseIPsRegExp = '' ;
			var getLastUsage = '' ;
			var getPreviousUsage = '' ;
			var checkVisitTable = '' ;
			var insertVisit = '' ;
			var updateVisit = '' ;
			var languageIDLookupStr = "";
			var languagelookupfromidstr = "";


			tempStruct.isInternal = false;
			tempStruct.isUnknownUser = false;
			tempStruct.isLoggedIn = 0;
			tempStruct.isLoggedInExpired = false ;
			tempStruct.tempSessionCookieTime = '';
			tempStruct.refreshContentTree = true ;
			tempStruct.keysToPersistAcrossLogins = '' ;
			tempStruct.keysToPersistAcrossLoginsAndUsers = 'previousPages,PreviousPersonIDsInThisSession,UserDataKeys' ;
			tempStruct.itemsToPersistAcrossLogins = structNew() ;
			tempStruct.ItemsToPersistAcrossLogins.previousPages = arrayNew(1);
			tempStruct.LocaleInfo = structNew ();
			tempStruct.LocaleInfo.clientUTCOffsetMinutes = gettimezoneinfo().utctotalOffset /60 ;// set equal to server time to start with
			tempStruct.LocaleInfo.clientServerTimeOffsetMinutes = 0			;
			tempStruct.LocaleInfo.clientUTCreceived = false ;
			tempStruct.IPAddress = application.com.security.getRemoteAddress() ;  // WAB 2012-12-04 CASE 438291 change to use getRemoteAddress ()
			tempStruct.isLoggedIn = 0;
			tempStruct.isLoggedInLastRefreshTime = "";
			tempStruct.PreviousPersonIDsInThisSession = '' ;
			tempStruct.isABot = false;
			tempStruct.impersonatedByPerson = 0; // WAB 2008-02-05  added impersonation
			tempStruct.previousPages = arrayNew(1) ; // WAB 2009-06-09
			tempStruct.jasperServerIsLoggedIn=false; // NJH 2010-11-30
			tempStruct.isApiUser=false;  //NJH 2012/05/15
			tempStruct.isJasperServerLoginConfirmed=false; // NJH 2013-02-12


				// Check for pages and browsers which we don't want to keep sessions open for
				// WAB 2008-03-01 was a bug here had this.BotList instead of this.doNotKeepSessionOpenForThesePages
				if (listfindNoCase(this.doNotKeepSessionOpenForThesePages,listlast(cgi.script_name,"/")) is not 0) {
						changeSessionTimeout =  10 ;
						dontRecordVisit =  true ;
				} 	else {
						//Check for bots - give a short lifespan
						if (Len(CGI.HTTP_USER_AGENT)) {
								botArray = listToArray(this.BotList);
								 botarrayLength= arrayLen(botArray);
								for (i=1;i lte botarrayLength;i = i+1) {
									if (findNoCase(botArray[i], CGI.HTTP_USER_AGENT)) {
										changeSessionTimeout =  60 ; // seconds, make it a minute - this allows the ip check monitor to do a bit of navigating around in a single session
										dontRecordVisit =  true ;
										tempStruct.isABot = true ;
										break;
									}
								}
						}
				}

				if (changeSessionTimeout) {
					session.setMaxInactiveInterval (changeSessionTimeout) ;
				}



			// if there is a cookie.relaylogin then we know that within this browser session there was a login
			if(structKeyExists(cookie,"relaylogin") and cookie.relaylogin is application.applicationname) {
				tempStruct.isLoggedInExpired = true ;
			} else {
				tempStruct.isLoggedInExpired = false ;
			}


			// this will force content to be reloaded on relogin

			// decide whether we are accessing an internal or an external site
	 		tempStruct.isInternal = request.currentsite.isInternal;

				tempStruct.siteDomain = cgi.HTTP_Host ;
				tempStruct.siteDomainAndRoot = tempStruct.siteDomain;


			// check that this personid is valid
			// (usually this will have already been done somewhere, but just checking!
				personRecord = application.com.globalfunctions.CFQUERY("select personid from person where personid = #Personid#  ");


			// are they the unknown user
			if (personid is 0 or personid is application.unknownpersonid or personRecord.recordCount is 0) {
				tempStruct.isUnknownUser = true ;
				tempStruct.Personid = application.unknownpersonid ;
			} else {
				tempStruct.Personid = personid;
				tempStruct.isUnknownUser = false ;
			}



			// is person an internal relay user (may be usefulif they are accessing the external site
			if (not tempStruct.isUnknownUser) {
				tempStruct.isValidRelayInternalUser = application.com.login.isPersonAValidRelayInternalUser(tempstruct.personid) ;
			} else {
				tempStruct.isValidRelayInternalUser = false ;
			}


			//writeoutput (tempStruct.Personid );

			// set usergroupid - if the person doesn't have one, then set to the unknownperson usergroupid
			// it is possible that it should always be the unknownperson usergroupid for an external site

	//			query = application.com.globalfunctions.CFQUERY("select usergroupid from usergroup where personid = #tempStruct.Personid#  union  select usergroupid from usergroup where personid = #application.unknownpersonid# and not exists (select * from person where personid = #tempStruct.Personid#)");
	//			if (query.recordcount is not 0) {
	//				tempStruct.usergroupid = query.usergroupid;
	//			} else {
	//				tempStruct.usergroupid = 0;
	//			}

				// WAB: 2006-01-25 made a mod here - said "not exists (select 1 from person where personid = #tempStruct.Personid#)" - which can't be true since always exists!
				queryResult = application.com.globalfunctions.CFQUERY("select usergroupid from usergroup where personid = #tempStruct.Personid#  union  select usergroupid from usergroup where personid = #application.unknownpersonid# and not exists (select 1 from usergroup where personid = #tempStruct.Personid#)");
				tempStruct.usergroupid = queryResult.usergroupid;


			</cfscript>
<!--- 		WAB 2007-06-18 removed this test, now happens on internal and external sites
	<cfif not tempstruct.isInternal> --->
		<!---
		2008-01-29 WAB removed and put into the update personal information function
		<CF_createUserGroups personid = "#tempStruct.personID#">

		--->
<!--- 			</cfif> --->

			<cfscript>
			// list of user groups
			usergroups = application.com.login.getUserGroups(tempStruct.PersonID);
			tempStruct.usergroups = valuelist(usergroups.usergroupid);


			// populate the personal data (person,location, organisation queries)
				tempStruct  = setPersonalData (tempStruct) ;


			// set a languageid
			browserLang = application.com.relayTranslations.getUsersPreferredLangFromBrowser(defaultLang="") ;
			countryDefaultLanguageid = application.com.relayTranslations.getDefaultLanguageForACountry (tempStruct.countryid) ;

			</cfscript>

				<cfif structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList>

					<cfset x = application.com.relayTranslations.getCountryLanguages(countryID = tempstruct.countryID)>
					<cfset languageIDLookupStr = request.languageidlookupstr>
					<cfset languagelookupfromidstr = request.languagelookupfromidstr>

				<cfelse>

					<cfset languageIDLookupStr = application.languageidlookupstr>
					<cfset languagelookupfromidstr = application.languagelookupfromidstr>

				</cfif>

			<cfscript>


				if (structKeyExists(URL,"lang") and structKeyExists(languageidlookupstr,url.lang)) {
					//if there is a url.lang variable defined and valid then use it
					tempStruct.languageid = languageidlookupstr[url.lang] ;
//				}	else if (isDefined("cookie.defaultLanguageid")){
//					// use cookie language if set
//					tempStruct.languageid = cookie.defaultLanguageid ;
				} 	else if (not tempStruct.isUnknownUser  and tempStruct.person.language is not "" and structKeyExists(application.languageidlookupfromnamestr,tempStruct.person.language) and  listfindNocase (request.currentsite.livelanguageids,application.languageidlookupfromnamestr[tempStruct.person.language]) ) {
					// WAB 2007-12-11 person language has to be one of the live languages for this site
					// if language is set in the database then use that
					tempStruct.languageid = application.languageidlookupfromnamestr[tempStruct.person.language];
				} else if (browserLang is not "" and structKeyExists(application.languageidlookupstr,browserLang)) {
					// use browser language if set and exists in our list
					tempStruct.languageid = application.languageidlookupstr[browserLang] ;
				} 	else if (not tempStruct.isUnknownUser  and listfind(request.currentsite.liveLanguageIds,listFirst(countryDefaultLanguageid))) {				// WAb 2007-12-11 change livelangaugeids from applicationscope to currentsite
					// set to default language for this country
						tempStruct.languageid = listFirst(countryDefaultLanguageid);
				} else {
					tempStruct.languageid = 1 ;
				}


			// having set a languageid, set the language isocode as well
				tempStruct.languageisocode = application.languageisocodelookupstr[tempStruct.languageid];

			// store the current value of cgi.HTTP_ACCEPT_LANGUAGE so that we can tell if it has changed
			if (structKeyExists(cgi,"HTTP_ACCEPT_LANGUAGE")) {
				tempStruct.HTTP_ACCEPT_LANGUAGE = cgi.HTTP_ACCEPT_LANGUAGE ;
			} else {
				tempStruct.HTTP_ACCEPT_LANGUAGE = '' ;
			}



				tempStruct.Content = structNew() ;
				tempStruct.Content.showForPersonid = tempStruct.personid;
				// WAB 2007-02-05 now use guessed country for the unknown user
					if (tempStruct.isUnknownUser) {
						tempStruct.Content.showForCountryID = tempStruct.possiblecountryidforunknownuser;
					} else {
						tempStruct.Content.showForCountryID = tempStruct.countryid;
					}

				tempStruct.Content.Status = 4;   // live
				tempStruct.Content.showForDifferentDate = false;
				tempStruct.Content.showDraftTranslations = false;
				tempStruct.Content.showTranslationLinks = false;
				tempStruct.Content.doNotTranslate = false; // WAB 2016-02-24 added
				tempStruct.Content.AlwaysShowTheseElementIDs = "";
				tempStruct.Content.AlwaysShowTheseElementIDsIfStatusCorrect = "";
				tempStruct.Content.refreshBeforeThisDate = now() ;

				tempStruct.LocaleInfo.Locale = getUserLocale (tempStruct.Content.showForCountryID) ;
				tempStruct.LocaleInfo.dateType 	= getDateTypeOfLocale (tempStruct.LocaleInfo.Locale) ;
				tempStruct.LocaleInfo.decimalInfo = getLocaleDecimalInfo (tempStruct.LocaleInfo.Locale) ; 		//2012-09-12 PPB/WAB Case 430249


				// decide whether this user gets the link for controlling content.
				// note that this might be called before a cookie is set so I check that we aren't dealing withthe unknown user before checking for rights
				queryResult = application.com.globalfunctions.CFQUERY("	SELECT 1 FROM rightsgroup r INNER JOIN (SELECT userGroupID FROM usergroup 	WHERE Name = 'Content Editor') u	ON r.userGroupID = u.userGroupID AND r.personid= #tempStruct.Personid#");

				if (not tempStruct.isUnknownUser and queryResult.recordcount is not 0 ) {
					tempStruct.Content.ShowLinkToControlContent = true;
					tempStruct.Content.AllowedToSeeLinkToControlContent = true;
				} else {
					tempStruct.Content.ShowLinkToControlContent = false;
					tempStruct.Content.AllowedToSeeLinkToControlContent = false;
				}

					tempStruct.Content.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse = false;




			// pNumber
			tempStruct.pNumber  = application.com.commonQueries.getP(tempStruct.PersonID);



			// store details of current page
			tempStruct.currentPage = getCurrentPageDetails();
			tempStruct.requestTime = now() ;
			tempStruct.pageCount = 1 ;

			// set user cookie
			/* 	WAB 2009-05-18  replaced with function call
			application.com.globalfunctions.cfcookie (	NAME="USER"  ,VALUE="#tempStruct.Personid#-#tempStruct.UserGroupID#"  ,EXPIRES="#application.com.globalFunctions.getCookieExpiryDays()#"	)	;
			*/
			setUserCookie (personid = tempStruct.Personid, usergroupid = tempStruct.UserGroupID,isInternal = tempStruct.isInternal);
			// -#tempStruct.UserGroups#" no longer added

			// does this person get to see CF errors on screen
			// and expanded for a few other items as well - broadly linked to debugging
			tempStruct.errors = {
				showDump=false,
				showLink=false,
				queryParamDebug=false,
				showLeadingWhiteSpace = false,
				showdebug = false
			} ;

			/*
			WAB 2015-10-29 change to use isTrusted() function

				// Create regular expression (showErrorIDLinkToTheseIPsRegExp) to test the client IP address against.
				// Show errors to requests from the Relayware breakout
				// WAB 2011-05-31 added support for testSite eq 3, also re-arranged code and added some comments
				showErrorIDLinkToTheseIPsRegExp = listAppend(application.com.settings.getSetting("relaywareInc.breakoutRegexp"),"127\.0\.0\.1","|") ;

				// Show errors to test & staging sites on our internal subnet
				if (application.com.relayCurrentSite.isEnvironment('test,staging,demo') ) {
					showErrorIDLinkToTheseIPsRegExp = listAppend(showErrorIDLinkToTheseIPsRegExp,"192.168.0.[0-9]{1,3}","|") ;
				}
			*/

			// Special case for client IPAddresses where we have turned on CF debug using our remote admin tools
			if (structKeyExists(server,"ShowErrorsToTheseIPAddressesRegExp")) {
				showErrorIDLinkToTheseIPsRegExp = listAppend(showErrorIDLinkToTheseIPsRegExp,server.ShowErrorsToTheseIPAddressesRegExp,"|") ;
			}

			if 	(application.com.security.isIpAddressTrusted() OR (showErrorIDLinkToTheseIPsRegExp is not "" and reFind(showErrorIDLinkToTheseIPsRegExp,application.com.security.getRemoteAddress()))) {
				tempStruct.errors.showLink = true;
			}

			// For dev sites we show the error dump in-line
			if (application.com.relayCurrentSite.isEnvironment('Dev')) {
				tempStruct.errors.showDump = true ;
				tempStruct.errors.showLink = true;
				tempStruct.errors.showDebug = true;
			}

		</cfscript>


			<!--- list of countries the user has rights to --->
			<cfset tempStruct.countryList = application.com.rights.getCountryRightsIDList(tempStruct.personid)>

			<!---
				WAB 2005-05-11
			if the user doesn't have any country rights, we are going to add their own country to the list
				this was in response to a problem on the sony site when we took countryrights away from the 404 usergroup
			 --->
			 <!--- START: 2013-03-28 NYB Case 434551 replaced 'if countrylist="" then make countrylist=countryid' with 'if countrylist does not contain countryid then add countryid to countrylist' --->
			<cfif ListFind(tempStruct.countryList, tempStruct.countryid) eq 0>
				<cfset tempStruct.countryList = listappend(tempStruct.countryList,tempStruct.countryid)>
			</cfif>
			 <!--- END: 2013-03-28 NYB Case 434551  --->

			<cfif tempStruct.countryList is not "" >
				<!--- list of regions the user has rights to all countries of--->
				<cfquery name="getRegions" datasource="#application.sitedatasource#" >
				select c.countryid as RegionID
				from country c inner join countrygroup cg on c.countryid = cg.countrygroupid
				where isnull(isocode,'') = ''
				and countrymemberid  in ( <cf_queryparam value="#tempStruct.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				group by c.countryid, c.countrydescription
				HAVING (COUNT(1) = (select count(1) from countrygroup cg2 inner join country c2 on c2.countryId = cg2.countryMemberId  where cg2.countrygroupid = c.countryid))
				</cfquery>

				<cfif getregions.recordcount is not 0 >
					<cfset tempStruct.RegionList = valuelist(getregions.RegionID)>
				<cfelse>
					<cfset tempStruct.RegionList = "0">
				</cfif>


				<!--- list of regions the user has rights to at least one country of--->
				<cfquery name="getRegions" datasource="#application.sitedatasource#" >
				select distinct c.countryid as RegionID
				from country c inner join countrygroup cg on c.countryid = cg.countrygroupid
				where isnull(isocode,'') = ''
				and countrymemberid in (<cf_queryparam value="#tempStruct.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfquery>

				<cfif getregions.recordcount is not 0 >
					<cfset tempStruct.RegionListSomeRights = valuelist(getregions.RegionID)>
				<cfelse>
					<cfset tempStruct.RegionListSomeRights = "0">
				</cfif>
			<cfelse>
					<cfset tempStruct.RegionList = "0">
					<cfset tempStruct.RegionListSomeRights = "0">
			</cfif>


			<cfset tempStruct.LanguageEditRights = application.com.relayTranslations.getPersonLanguageRights(tempStruct.personid)>
			<cfif tempStruct.LanguageEditRights  is "">
				<cfset tempStruct.LanguageEditRights  = "0">
			</cfif>


		<!--- get last logged in info --->
			<cfquery name="getLastUsage" datasource="#application.sitedatasource#" >
			select max(logindate) as time from usage with(nolock)
			where personid =  <cf_queryparam value="#tempStruct.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			and siteDefID =  <cf_queryparam value="#request.CurrentSite.siteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<!--- 	if user is logged in then this is the login time of the current session,
					if they aren't logged in work out login time--->
			<!--- WAB 2006-03-28 I don't think this is working correctly anymore - some probably needs to go in the setloggedin function --->

			<cfif tempStruct.isLoggedIn>
				<cfset tempStruct.CurrentLoginTime = getLastUsage.time>
				<cfif getLastUsage.time is not "" >
					<!--- Now get the previous login time --->
					<cfquery name="getPreviousUsage" datasource="#application.sitedatasource#" >
					select max(logindate) as time from usage with(nolock)
					where personid =  <cf_queryparam value="#tempStruct.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and siteDefID =  <cf_queryparam value="#request.CurrentSite.siteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 				and  abs(datediff(ss,#createodbcdatetime(getLastUsage.time)#,logindate)) > 1
					</cfquery>
					<cfset tempStruct.PreviousLoginTime = getPreviousUsage.time>
				<cfelse>
					<cfset tempStruct.PreviousLoginTime = "">
				</cfif>

			<cfelse>
					<cfset tempStruct.CurrentLoginTime = "">
					<cfset tempStruct.PreviousLoginTime = getLastUsage.time>
			</cfif>


			<!---
			WAB 2010/06 removed call to <CF_Browser> (out of  date an annoying)
			Then discovered place where we test for it (filedisplay)
			so added back a very rudimentatry test of my own --->
			<cfset tempStruct.Browser = browserDetect()>

			<!--- stores id of session which opened this session --->
			<cfif structKeyExists(url,"openedby")>
				<cfset tempStruct.sessionOpenedBy = url.openedby>
			<cfelse>
				<cfset tempStruct.sessionOpenedBy = "">
			</cfif>

				<cfset tempStruct.sessionStarted = now()>



			<!--- add an entry into the visit table --->
			<!--- is this a completely new session or has the person changed --->
			<!--- 2013/10/14 NJH 2013 release 2 Sprint 2 - store the siteDefDomainID rather than the actual domain/site--->

				<cfif 	not structKeyExists(session,"RelayCurrentUserStructure")
						or not isdefined("session.RelayCurrentUserStructure.visitid")
						or (session.RelayCurrentUserStructure.personid is not application.unknownPersonid and session.RelayCurrentUserStructure.personid is not tempStruct.personid)>

					<cfif structKeyExists(url,"a") and url.a is "t"><cfset browser='EmailTracker'><cfelse><cfset browser = Left(CGI.HTTP_USER_AGENT,100)></cfif>
					<!--- <cfif browser = "" and  --->

					<cfif not dontRecordVisit and not cgi.SCRIPT_NAME contains "amiup.cfm">  <!--- WAB 2008-01-30 added the don't record visit --->

						<!--- 2015-10-07 WAB PROD2015-165 Security Scan.  Add queryParam to this query --->
						<cfset var referrer = Left(cgi.http_referrer,100)>
						<cfif structKeyExists(url,"a") and url.a is "t">
							<cfset referrer = "EmailTracker">
						</cfif>

						<cfset var metaData = "">
						<cfif structKeyExists(url,"openedBy")>
							<cfset metaData = "openedBy=#url.openedBy#">
						</cfif>

						<!--- WAB 2014-02-26 remove cfid and cftoken from the visit table.  We don't use them and in CF10 they sometimes seem to no longer be of type int --->
						<cfquery name="insertVisit" datasource="#application.sitedatasource#">
							insert into visit(
								jsessionid,
								personid,
								browser,
								ipaddress,
								siteDefDomainID,
								numberOfLogins,
								arrivedAsUnknownUser,
								visitstartedDate,
								referrer,
								impersonatedbyPerson,
								siteDefID,
								coldfusionInstanceid,
								metadata
							)
							values
							(
								<cf_queryparam value="#session.sessionid#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								<cf_queryparam value="#tempStruct.personid#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#Left(CGI.HTTP_USER_AGENT,100)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								<cf_queryparam value="#application.com.security.getRemoteAddress()#" CFSQLTYPE="CF_SQL_VARCHAR" >,  <!---  WAB 2012-12-04 CASE 438291 change to use getRemoteAddress () --->
								<cf_queryparam value="#request.currentSite.siteDefDomainID#" CFSQLTYPE="CF_SQL_INTEGER" >, <!--- 14/03/2016 DAN Case 448481 --->
								<cf_queryparam value="#iif(tempStruct.isLoggedIn,1,0)#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#iif(tempStruct.isUnknownUser,1,0)#" CFSQLTYPE="CF_SQL_INTEGER" >,
								getdate(),
								<cf_queryparam value="#referrer#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								<cf_queryparam value="#tempStruct.impersonatedbyperson#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#request.currentSite.siteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#application.instance.coldfusioninstanceid#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cf_queryparam value="#metaData#" CFSQLTYPE="CF_SQL_VARCHAR" >
							)
							SELECT scope_identity() as visitID

						</cfquery>

						<cfset tempStruct.VisitID = insertVisit.visitID			>
					<cfelse>
						<cfset tempStruct.VisitID = 0>
					</cfif>

				<cfelse>

					<!--- 	if we have recognised a previously unknown user then update the current visit record --->
					<!--- WAB 2015-05-19 took out reference to cftoken which is no longer in the table - not sure why hadn't errored before, but did after my session rotate code implemented --->
					<cfif session.RelayCurrentUserStructure.personid is application.unknownPersonid	and	tempStruct.personid is not application.unknownPersonid>

						<cfquery name="updateVisit" datasource="#application.sitedatasource#">
						update visit
							set
							personid =  <cf_queryparam value="#tempStruct.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
						where visitID =  <cf_queryparam value="#session.relaycurrentuserstructure.visitid#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfquery>

					</cfif>

					<cfset tempStruct.VisitID = session.relaycurrentuserstructure.visitid			>

				</cfif>

					<!--- creates the structure of a user's rights (tasks) in memory --->
					<cfset application.com.login.initUserSession(tempstruct.personID)>
					<cfset structDelete (session,"flagGroupRights")>  <!--- clear out this cached rights structure WAB 2007-02-23 --->


					<!--- when the session starts, lets get the users timezoneoffset from the browser
							has to be done via a bit of javascript
						WAB 2010-06-14 don't do if return format is JSON - otherwise problems on Ajax login, but need to check that timezone is set in some other way

					 --->

					 <!--- WAB 2014-06-19 CASE 440695 Moved to request.cfc
					 <cfset outputGetClientTimezoneCode()>
					 --->

			<cfreturn tempStruct>
	</cffunction>

	<!---
	WAB 2014-06-19 CASE 440695 Moved this code to request.cfc, so only ever gets output when there is a <HEAD>
	<cffunction name="outputGetClientTimezoneCode">
		<!---
			can only output this script tag for non cfc calls
			WAB 2011-06-20 added test .cfc  - was previously looking for wsdl in the URL
		--->
		<cfif (not structKeyExists(request,"suppressApplicationCfmOutput") or not request.suppressApplicationCFMOutput) and not structKeyExists(url,"returnformat")  and not structKeyExists(url,"wsdl") and listlast (SCRIPT_NAME,".") is not "cfc" >
			<cf_includejavascriptOnce template = "/javascript/getClientTimeZone.js" useHTMLHead="false">
		</cfif>


	</cffunction>
	--->


	<!--- rudimentary detection to replace cf_browser --->
	<cffunction name="browserDetect">
		<cfargument name="browserString"  default = "#cgi.HTTP_USER_AGENT#">

			<cfset var regExp = '' />
			<cfset var regExpResult = '' />
			<cfset var result = {name = browserString,type="",version=0,os="", isMobile=false}>
            <!--- 2015-11-04 DAN Case 446442 --->
            <cfset var regExp = '(Android|webOS|iPhone|iPad|BlackBerry|Windows Phone)' />
            <cfset result.isMobile = refindNocase(regExp, browserString) GT 0 />

			<!--- Then discovered place where we test for it (filedisplay)
			so added back a very rudimentatry test of my own --->

				<cfif browserString contains "Opera">
					<cfset result.type = "OPERA">
				<cfelseif browserString contains "webkit" or browserString contains "khtml">
					<cfset result.type = "Safari">
				<cfelseif browserString contains "MSIE">
					<cfset result.type = "MSIE">
					<cfset regExp = "MSIE (.*?);">
					<cfset regExpResult = application.com.regExp.refindAllOccurrences(regExp,browserString)>
					<cfif arrayLen (regExpResult)>
						<cfset result.version = regExpResult[1][2]>
					</cfif>

				<cfelseif browserString contains "Gecko">
					<cfset result.type = "Gecko">
				</cfif>

				<cfif browserString contains "windows" or browserString contains "win32">
				 	<cfset result.OS = "Windows">
				<cfelseif browserString contains "macintosh" or browserString contains "mac os x">
				 	<cfset result.OS = "MAC">
				<cfelseif browserString contains "linux" >
				 	<cfset result.OS = "Linux">
				</cfif>

		<cfreturn result>
	</cffunction>


	<!--- for existing session, update the visit record--->
	<cffunction access="private" name="fnUpdateVisit"  hint="">

		<cfset var updateVisit = '' />

		<cfif not application.com.request.isAjax() and cgi.script_name is not "/errorHandler/IISMissingTemplateHandler.cfm">
			<cfquery name="updateVisit" datasource="#application.sitedatasource#">
			update visit
				set numberOfRequests = numberOfRequests + 1,
				lastRequestDate =  <cfqueryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			where visitID =  <cfqueryparam value="#session.relaycurrentuserstructure.visitid#" CFSQLTYPE="CF_SQL_INTEGER" >

			<cfif structKeyExists (url,"openedby")>
			update visit
				set metaData = isnull(metaData,'') + case when isnull(metaData,'') <> '' then ',' else '' end + <cf_queryparam value="openedby=#url.openedby#" CFSQLTYPE="CF_SQL_VARCHAR" >

			where visitID =  <cf_queryparam value="#session.relaycurrentuserstructure.visitid#" CFSQLTYPE="CF_SQL_INTEGER" >
				and metadata  not like  <cf_queryparam value="%openedby=#url.openedby#%" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			</cfquery>
		</cfif>

	</cffunction>

	<cffunction access="public" name="storeUserData" returntype="boolean" hint="any programmer can store data within relaycurrentuser and can decide whether it persists if the user changes or there is a login/logout">
		<cfargument name="keyName" type="string" required=true>
		<cfargument name="data"  required=true>
		<cfargument name="keepAcrossLoginsAndLogouts"  default = "true">
		<cfargument name="keepAcrossUserChanges"  default = "false">

		<!--- store list of any keys added through this method --->
		<cfparam name="session.relaycurrentUserStructure.UserDataKeys" default= "">
		<cfif listfindNoCase(session.relaycurrentUserStructure.UserDataKeys,keyname) is 0>
			<cfset session.relaycurrentUserStructure.UserDataKeys = listappend (session.relaycurrentUserStructure.UserDataKeys, keyname)>
		</cfif>

		<cfset session.relaycurrentUserStructure[keyname] = data>

		<cfif keepAcrossLoginsAndLogouts>
			<cfif listfindNoCase(session.relaycurrentUserStructure.keysToPersistAcrossLogins,keyname) is 0>
				<cfset session.relaycurrentUserStructure.keysToPersistAcrossLogins = listappend (session.relaycurrentUserStructure.keysToPersistAcrossLogins, keyname)>
			</cfif>
		</cfif>
		<cfif keepAcrossUserChanges>
			<cfif listfindNoCase(session.relaycurrentUserStructure.keysToPersistAcrossLoginsAndUsers,keyname) is 0>
				<cfset session.relaycurrentUserStructure.keysToPersistAcrossLoginsAndUsers = listappend (session.relaycurrentUserStructure.keysToPersistAcrossLoginsAndUsers, keyname)>
			</cfif>
		</cfif>

		<cfset copyToRequestScope()>

		<cfreturn true>

	</cffunction>


	<cffunction access="public" name="deleteUserData" returntype="boolean" hint="delete data stored by above function ">
		<cfargument name="keyName" type="string" required=true>

		<cfif listfindNoCase(session.relaycurrentUserStructure.UserDataKeys,keyname) is not 0>
			<cfset structDelete (session.relaycurrentUserStructure,keyname)>
			<cfset copyToRequestScope()>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>





	</cffunction>



	<cffunction access="private" name="getCurrentPageDetails" returntype="struct" hint="">
			<cfset var pageStruct = structNew() >

			<cfset pageStruct.script_Name = cgi.script_name>
			<cfif isDefined("url")>
				<cfset pageStruct.url = duplicate(url)>  <!--- found that I needed to use duplicate to get it to work --->
			</cfif>
			<cfif isDefined("form")>
				<cfset pageStruct.form = duplicate(form)>
			</cfif>
			<cfset pageStruct.timeLoaded = request.requestTime>
			<cfset pageStruct.saveGeneratedContent = false>

		<cfreturn pageStruct>
	</cffunction>

	<!--- 2008-07-08 GCC added with (nolock) so this doesnt have to wait behind locks on POL tables to get POL data
		2013-03-07	WAB	CASE 432630 Added countryName to relayCurrentUser.location & person - so can be used in merge
	--->
	<cffunction access="private" name="setPersonalData" returntype="struct" hint="updates parts of the user structure relating to personal data, can be used if a person has updated their own record">
		<cfargument name="tempStruct" type="struct" required="yes">


			<!---  <cfset var tempStruct = '' /> this line fools varScoper --->
			<cfscript>
				var usergroups = '';
				var emptyConnectionQuery = queryNew("ID,fullname,personID");
				var connections={linkedIn=emptyConnectionQuery,facebook=emptyConnectionQuery,organisation=emptyConnectionQuery};
				var serviceArray = arrayNew(1);
				var i=0;

				application.com.login.addPersonToUserGroupsByProfile(tempStruct.PersonID) ;
				usergroups = application.com.login.getUserGroups(tempStruct.PersonID);
				tempStruct.usergroups = valuelist(usergroups.usergroupid);
				tempStruct.connections = connections;


			if (tempStruct.isUnknownUser) {
				/* 	things for the unknown user
				 	null queries for person,location,organisation
				 	WAB 2017-02-01 RT-227  (Error if person merge field used on a logged out page)
						Should move these POL queries to a null entity object but I don't have one yet.  Have instead converted them to structures.  
						This prevents some errors in the merge system where returnType=struct (or object)
				*/ 
				tempStruct.person = application.com.structureFunctions.queryRowToStruct (new query (sql ="select * from vperson with (nolock) where personid = 0").execute().getResult()); 
				tempStruct.location = application.com.structureFunctions.queryRowToStruct (new query (sql ="select *, '' as countryname from vlocation with (nolock) where locationid = 0").execute().getResult());
				tempStruct.organisation = application.com.structureFunctions.queryRowToStruct (new query (sql ="select *, '' as countryname from vorganisation with (nolock) where organisationid = 0").execute().getResult());
				tempStruct.locationid = 0 ; // hope this makes sense!
				tempStruct.organisationid = 0 ; // hope this makes sense!
				tempStruct.countryid = 0 ;
				// WAB added this guess at the unknown users country, but haven't updated .countryid yet
				tempStruct.possiblecountryidforunknownuser = application.com.relayTranslations.getPossibleCountryFromBrowser();
				//2009-08-05 GCC using poss countryid for isocode for unknown user. changed from ''
				//2009-09-24 WAB sometimes possiblecountryid is 0 so have to add a check
				if (tempStruct.possiblecountryidforunknownuser is not 0) {
					tempStruct.countryisocode = application.CountryISO[tempStruct.possiblecountryidforunknownuser];
				} else {
					tempStruct.countryisocode = '';
				}
				tempStruct.content.showforcountryid = tempStruct.possiblecountryidforunknownuser ;
				tempStruct.fullname = 'Unknown User';
				tempStruct.supportEmailAddress = application.SupportEmailAddress;
				tempStruct.picture = "";

			}  else {
				// things for the known user
				// queries for person,location,organisation details
				// WAB 2016-11-15 Move to using Entity objects
				tempStruct.person = application.com.entities.load ('person', tempStruct.Personid);  //application.com.globalfunctions.CFQUERY("select v.*,p.* from person p inner join vPersonPublicProfile v on p.personID = v.personId where p.personId=#tempStruct.Personid#");
				tempStruct.location = tempStruct.person.getLocation(); //application.com.globalfunctions.CFQUERY("select l.*, c.countryDescription as countryName from location l with (nolock)  inner join country c with (nolock) ON l.countryID = c.countryid  where l.locationid = #tempStruct.person.locationid#");
				tempStruct.organisation = tempStruct.person.getOrganisation(); //application.com.globalfunctions.CFQUERY("select o.*,c.countryDescription as countryName from organisation o with (nolock) inner join country c with (nolock) ON o.countryID = c.countryid  where o.organisationid = #tempStruct.person.organisationid#");
				tempStruct.locationid = tempStruct.person.locationid;
				tempStruct.organisationid = tempStruct.person.organisationid;
				tempStruct.countryid = tempStruct.location.countryid;
				tempStruct.countryisocode = application.CountryISO[tempStruct.countryid];
				tempStruct.content.showforcountryid = tempStruct.location.countryid;
				tempStruct.fullname = tempStruct.person.firstname & ' ' & tempStruct.person.lastname;
				tempStruct.supportEMailAddress = application.com.globalfunctions.CFQUERY("select case when isNull(supportEmail,'') = '' then '#application.supportEmailAddress#' else supportEmail end as supportEmail from country where countryid = #tempStruct.countryid#").supportEmail;

				/* WAB 2016-11-15 Entity Objects: This is rather naughty updating the object directly, but it does what is required without saving the data to the db.  Also added a getPictureURL to the  */
				if (tempStruct.person.pictureURL eq "") {
					tempStruct.person.pictureURL = "/images/social/icon_no_photo_80x80.png";
				}
 				// 2013/01/29	YMA		Changed image size
				tempStruct.picture = "<img src='#tempStruct.person.pictureURL#' id='userPicture'/>";
				
			}

			if (application.testSite is not 0) {
				tempStruct.supportEMailAddress = 'devHelp2@relayware.com' ;
			}

			</cfscript>

			<!--- WAB 2012-06-30 added functionality to set custom data during initialisation --->
			<cfset structAppend(tempStruct,setCustomData(duplicate(tempStruct)),false)>

		<cfreturn tempStruct>

	</cffunction>

	<!---
	WAB 2012-06-30
	This function is designed to be overwritten by custom code.
	Returns a structure which will get appended into the relayCurrentUser structure [cannot overwrite existing keys]
	 --->
	<cffunction name="setCustomData" hint="This function is designed to be overwritten by custom code.  Returns a structure which will get appended into the relayCurrentUser structure [cannot overwrite existing keys]">
		<cfargument name="tempStruct" type="struct" required="yes">
		<!--- DO NOT PUT ANY CODE IN THIS FUNCTION.  IT IS DESIGNED TO BE EXTENDED WITHOUT A CALL TO super.SetCustomData ()--->
		<cfreturn structNew()>
	</cffunction>

	<!---
		WAB 2006-03-28
		given the clients time offset from gmt, put information into the user structure
			note that these offsets seem to be 'backwards'  ie GMT +1 is utcOffset -60
	 --->

	<cffunction access="public" name="setClientTimeOffset"  hint="">
		<cfargument name="clientutcOffsetMinutes" type="numeric" required="yes">
			<cfset var localeInfo  = "">
			<cfset var ServerUTCOffsetMinutes = gettimezoneinfo().utctotalOffset /60>

			<cflock timeout="10" throwontimeout="Yes" name="#getLockName()#" type="EXCLUSIVE">
				<cfset localeInfo = session.RelayCurrentUserStructure.localeInfo >

				<cfset localeInfo.clientUTCOffsetMinutes = clientutcOffsetMinutes>
				<cfset localeInfo.clientServerTimeOffsetMinutes = ClientUTCOffsetMinutes - ServerUTCOffsetMinutes>
				<cfset localeInfo.clientUTCreceived = true>  <!--- really just for debugging to see if worked --->

			</cflock>
		<cfreturn>
	</cffunction>

	<cffunction access="public" name="getValidLocaleFromBrowser"  hint="">

		<cfset var templocale = "">
		<cfset var result = "">

		<cfloop index="templocale" list = "#cgi.HTTP_ACCEPT_LANGUAGE#">
			<cfset tempLocale = replacenocase(listfirst(templocale,";"),"-","_")>
			<cfif listfindNoCase (server.ColdFusion.SupportedLocales, templocale)>
					<cfset result = tempLocale>
<!--- 				<cfset result = listgetat(server.ColdFusion.SupportedLocales, listfindNoCase (server.ColdFusion.SupportedLocales, templocale))> --->
				<cfbreak>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="getDateTypeOfLocale"  hint="">
		<cfargument name="locale" type="string" >

			<CFSCRIPT>
				var dateType = '';
				var tempLocale = setLocale (locale) ;
				var dateTest = lsdateformat(createdatetime(2007,10,12,0,0,0),"short") ;

				if (left(dateTest,2) is "12" )  { // eurodate
					dateType = 'EURO' ;
				} else if (left(dateTest,2) is "07")  {  // japanese date
					dateType = 'ISO' ;
				} else if (left(dateTest,2) is "10" )  {  // us date
					dateType = 'US'			;
				}

				setLocale (tempLocale) ;

			</CFSCRIPT>
		<cfreturn dateType>
	</cffunction>

	<!--- 2012-09-12 PPB/WAB Case 430249 new function --->
	<cffunction access="public" name="getLocaleDecimalInfo"  hint="">
		<cfargument name="locale" type="string" >

			<CFSCRIPT>
				var result = {};
				var tempLocale = setLocale (locale) ;
				var Test = lsnumberformat(1234.5,"9,999.9") ;

				result.decimalPoint = mid(test,6,1) ;
				result.thousandsseparator = mid(test,2,1) ;

				setLocale (tempLocale) ;

			</CFSCRIPT>
		<cfreturn result>
	</cffunction>



	<!--- this function works out what locale to use.  If we decide to change the way this is chosen then make changes here --->
	<cffunction access="public" name="getUserLocale"  hint="" >
		<cfargument name="countryid" type="string" >

		<cfset var countryLocaleQry = "">
		<cfset var result = "">

			<cfscript>
			countryLocaleQry = application.com.globalfunctions.CFQUERY("	SELECT defaultLocale FROM country where countryid =  #countryid#");
				if (countryLocaleQry.defaultLocale is not "")  {
					result   = countryLocaleQry.defaultLocale ;
				} else {
					// getlocale from browser
					result = getValidLocaleFromBrowser () ;
					if (result is  "") {
						result = getLocale ();
					}
				}
			</cfscript>

		<cfreturn result>
	</cffunction>

	<!---
	2009-05-18  WAB
	Added some checking to the user cookie to prevent it being tampered with (which allows another user name to appear in the login box)
	Added a third item to the cookie which is a encryption of the first two items
	New functions here to set and check the user cookie

	 --->

	<cffunction access="private" name="setUserCookie"  >
		<cfargument name="personid" type="numeric" >
		<cfargument name="userGroupID" type="numeric" >
		<cfargument name="isInternal" type="boolean" default="#request.relayCurrentSite.isInternal#" >

		<cfset var cookieValue = "#personid#-#userGroupID#">
		<cfset var cookiecheck = application.com.encryption.encryptString (encryptType = "standard", string = cookieValue)>
		<!--- WAB 10-01-2011 set cookie to expire at midnight rather than at exactly x days in future.  This makes behaviour the same as other security settings --->
		<cfset application.com.globalfunctions.cfcookie (	NAME="USER"  ,VALUE="#cookieValue#-#cookiecheck#"  ,EXPIRES="#application.com.globalFunctions.getCookieExpiryDate(isInternal = isInternal)#")>

	</cffunction>


	<cffunction access="private" name="checkUserCookie"  return="boolean">

		<cfset var cookiecheckdigits = "" >
		<cfset var decryptedcookiecheck = "" >

		<cfif listLen(cookie.user,"-") lte 2>
			<cfreturn false>
		<cfelse>
			<cfset cookiecheckdigits = listGetAt(cookie.user,3,"-") >
			<cfset decryptedcookiecheck = application.com.encryption.decryptString (encryptType = "standard",string = cookiecheckdigits) >

			<cfif listFirst (cookie.user,"-") is listFirst (decryptedcookiecheck,"-")>
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>
		</cfif>

	</cffunction>


<!--- 	<!--- this is an old function --->
	<cffunction access="public" name="contentTreeRefreshed" returntype="boolean" hint="">

			<cfset  session.relayCurrentUserStructure.refreshContentTree = false >
			<cfset  copyToRequestScope () >

		<cfreturn true>

	</cffunction>
 --->
	<!--- this creates a lock name which is unique to this session
		this means that doing this lock doesn't lock up the whole of this session, just access to session.relaycurrentuserstructure
	--->
	<cffunction access="private" name="getLockName" returntype="String" hint="">
		<cfargument name="sessionScope" default="#session#">

		<cfreturn "relayCurrentUser#sessionScope.sessionid#">

	</cffunction>

	<cffunction access="public" name="onrequestEnd"  hint="">

		<cfif session.relaycurrentuserstructure.currentPage.saveGeneratedContent or (structKeyExists(session.relaycurrentuserstructure,"saveGeneratedContent")  and session.relaycurrentuserstructure.saveGeneratedContent)>
			<cfset saveGeneratedContent()>
		</cfif>

		<cfif structKeyExists(session.relaycurrentuserstructure.currentPage,"deleteSessionAtEndOfRequest") and session.relaycurrentuserstructure.currentpage.deleteSessionAtEndOfRequest>
			<cfset deleteThisSession()>
		</cfif>

	</cffunction>

	<cffunction access="public" name="saveGeneratedContentAtEndOfRequest"  hint="">
		<cfargument name="YesOrNo" type="boolean" default="true">
			<cfset session.relaycurrentuserstructure.currentpage.saveGeneratedContent = YesOrNo>
		<cfreturn>
	</cffunction>

	<cffunction access="public" name="deleteSessionAtEndOfRequest"  hint="">
		<cfargument name="YesOrNo" type="boolean" default="true">
		<cfset session.relaycurrentuserstructure.currentpage.deleteSessionAtEndOfRequest = YesOrNo>
	</cffunction>

	<cffunction access="public" name="saveGeneratedContent"  hint="">
		<cfset session.relaycurrentuserstructure.currentpage.generatedContent = getPageContext().getOut().getString()>
	</cffunction>

	<cffunction access="private" name="deleteThisSession"  hint="">
		<!--- 	WAB 2009-06-08 LHID 2306 replaced this with killcurrentsession, which seems to work better for the logout code, because doesn't take effect for a second or two
			<cfset application.com.sessionmanagement.deleteASession(application.applicationname,session.sessionid)>
		--->
		<cfset application.com.sessionmanagement.killCurrentSession(time=2)>
	</cffunction>

	<cffunction access="public" name="setShowErrors"  hint="">
		<cfargument name="showDump" default="1">
		<cfargument name="showLink" default="1">
		<cfargument name="sessionScope" default="#session#">

		<cfset arguments.sessionScope.relayCurrentUserStructure.errors.showDump = showDump>
		<cfset arguments.sessionScope.relayCurrentUserStructure.errors.showLink = showLink>

		<cfset copyToRequestScope (sessionscope = sessionscope) >

		<cfreturn sessionScope.relayCurrentUserStructure.errors>

	</cffunction>

	<cffunction access="public" name="setQueryParamDebug"  hint="">
		<cfargument name="QueryParamDebug" default="1">
		<cfargument name="sessionScope" default="#session#">

		<cfset arguments.sessionScope.relayCurrentUserStructure.errors.QueryParamDebug = QueryParamDebug>

		<cfset copyToRequestScope (sessionscope = sessionscope) >

		<cfreturn sessionScope.relayCurrentUserStructure.errors>

	</cffunction>

	<cffunction access="public" name="setShowLeadingWhiteSpace"  hint="">
		<cfargument name="ShowLeadingWhiteSpace" default="1">
		<cfargument name="sessionScope" default="#session#">

		<cfset arguments.sessionScope.relayCurrentUserStructure.errors.ShowLeadingWhiteSpace= ShowLeadingWhiteSpace>

		<cfset copyToRequestScope (sessionscope = sessionscope) >

		<cfreturn sessionScope.relayCurrentUserStructure.errors>

	</cffunction>

	<cffunction access="public" name="setShowDebug"  hint="">
		<cfargument name="debugOn" default="1">
		<cfargument name="cfpassword" required = false>

		<cfif debugOn>
			<cfset application.com.cfadmin.setDebug(argumentcollection = arguments, setIPAddress = true)>
		</cfif>
		<cfset session.relayCurrentUserStructure.errors.showDebug = debugOn>

		<cfset copyToRequestScope () >

		<cfreturn session.relayCurrentUserStructure.errors>

	</cffunction>


	<cffunction access="public" name="getShowDebug"  hint="">
		<cfargument name="checkServerDebug" default="true">

		<cfset var result = true>
		<cfif checkServerDebug>
			<cfset result = application.com.cfadmin.getDebug()>
		</cfif>

		<cfset result = result && session.relayCurrentUserStructure.errors.showDebug>

		<cfreturn result>

	</cffunction>

	<!--- 2016-10-24 PPB CF_DUMP --->
	<cffunction access="public" name="setDebugSessionSetting">
		<cfargument name="settingName" required="true">
		<cfargument name="settingValue" required="true">
		<cfargument name="sessionScope" default="#session#">

		<cfset arguments.sessionScope.relayCurrentUserStructure.errors[arguments.settingName] = arguments.settingValue>
		
		<cfset copyToRequestScope (sessionscope = sessionscope)>
	</cffunction>

	<cffunction access="public" name="getCurrentUserStructure"  hint="">
		<cfargument name="sessionScope" default="#session#">

		<cfreturn sessionScope.relayCurrentUserStructure>
	</cffunction>

	<!--- NYB 2009-05-29 LHID 2306 - created function --->
	<cffunction access="public" name="clearCookies"  hint="Taken from Reset.cfm">
		<cfargument name="cookieList" default="language,session,partnersession,previousSearches,appname,pid,user,cfid,cftoken,jsessionid,defaultLANGUAGEid,dejavu,locatorLocation">
		<cfargument name="outputFeedback" default="false"> <!---  WAB added so that didn't output stuff when used in logout function--->

		<cfset var i = "">

		<CFLOOP INDEX="i" LIST=#cookieList# DELIMITERS=",">
			<CFOUTPUT>
				<CFIF structKeyExists(cookie,i)>
					<cfif outputFeedback>Cookie #htmleditformat(i)# <BR></cfif>
					<cfset application.com.globalFunctions.cfcookie(name=i,value="0",expires="0")>
					<!--- <cfset application.com.globalFunctions.cfcookie(NAME=#i# VALUE="0" EXPIRES=0)> --->
				<CFELSE>
					<cfif outputFeedback>No #htmleditformat(i)# cookie set<BR></cfif>
				</cfif>
			</CFOUTPUT>
		</CFLOOP>
	</cffunction>

	<cffunction access="public" name="setJasperServerIsLoggedIn" >
		<cfargument name="isLoggedIn" type="boolean" default="true">
		<cfargument name="sessionScope" default="#session#">

		<cfscript>
			var data = sessionScope.RelayCurrentUserStructure ;

			//	NJH 2011-03-08 - P-FNL075 Report Designer - populate translations for current user, should they be accessing a report that requires the cache to be populated
			if (arguments.isLoggedIn and not structKeyExists(data,"jasperServerIsLoggedIn") ) {
				application.com.relayTranslations.populatePhrasePointerCacheForCurrentUser();
			}
			if (not arguments.isLoggedIn) {
				setIsJasperServerLoginConfirmed(loginConfirmed=false);
			}
			data.jasperServerIsLoggedIn = arguments.isLoggedIn ;
			copyToRequestScope (sessionscope = sessionscope) ;
		</cfscript>
	</cffunction>

	<!--- NJH 2013/02/12 variable to be used to let us know that we have sent off a request to JasperServer to be logged in... we still really don't know if we have successfully logged in --->
	<cffunction access="public" name="setIsJasperServerLoginConfirmed" >
		<cfargument name="loginConfirmed" type="boolean" default="true">
		<cfargument name="sessionScope" default="#session#">

		<cfscript>
			var data = sessionScope.RelayCurrentUserStructure ;

			data.isJasperServerLoginConfirmed = arguments.loginConfirmed ;
			copyToRequestScope (sessionscope = sessionscope) ;
		</cfscript>
	</cffunction>


	<cffunction name="setPersonImage" access="public">
		<cfargument name="sessionScope" default="#session#">
		<cfargument name="pictureURL" type="string" required="false">

		<cfset var personProfileImage = "">
		<cfset var updatePersonPictureURL = "">

		<cfif not structKeyExists(arguments,"pictureURL")>
			<cfset personProfileImage = application.com.linkedIn.getProfile(entityID=sessionScope.RelayCurrentUserStructure.personID,entityTypeID=0,fieldlist="picture-url").profile.pictureURL>
		<cfelse>
			<cfset personProfileImage = arguments.pictureURL>
		</cfif>

		<!--- this call is made when a user connects their RW account with LinkedIn. Only update their picture url if it has not already been set --->
		<!--- 2013/01/29	YMA		Dont update image if we've already got an image for this user --->
		<cfquery name="updatePersonPictureURL" datasource="#application.siteDataSource#">
			update person set
				pictureURL =  <cf_queryparam value="#personProfileImage#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				lastUpdated=getDate(),
				lastUpdatedBy =  <cf_queryparam value="#sessionScope.RelayCurrentUserStructure.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				lastUpdatedByPerson =  <cf_queryparam value="#sessionScope.RelayCurrentUserStructure.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			where personID =  <cf_queryparam value="#sessionScope.RelayCurrentUserStructure.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and len(isNull(pictureURL,'')) = 0
		</cfquery>

	</cffunction>

	<!--- NJH 2013/01/13 - 2013Roadmap - set person connections --->
	<cffunction name="getPersonConnections" access="public" returnType="struct">

		<cfset var serviceArray = '' />
		<cfset var service = '' />

		<cfif request.relayCurrentUser.connections.organisation.recordCount eq 0>
			<cfif (application.com.settings.getSetting("socialMedia.enableSocialMedia")) >
				<cfset serviceArray=listToArray(application.com.settings.getSetting("socialMedia.services"))>
				<cfloop array="#serviceArray#" index="service">
					<cfset session.relayCurrentUserStructure.connections[service] = application.com.service.getConnections(personID=request.relayCurrentUser.PersonID,serviceID=service).person>
				</cfloop>
			</cfif>
			<cfset session.relayCurrentUserStructure.connections.organisation = application.com.globalFunctions.cfQuery("select personId,firstname+' '+lastname as fullname from vPersList where organisationID = #request.relayCurrentUser.organisationID#")>

			<cfset copyToRequestScope()>
		</cfif>

		<cfreturn request.relayCurrentUser.connections>
	</cffunction>

	<cffunction name="AuthenticationFilter_URLLogout" index="10" >
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=personID,Level=0,filterUsed=false}>

		<!--- Case 435375 NJH 2013/05/22 - added personId neq -1... as we shouldn't be processing this if the user is already logged out.. this was actually Will's solution --->
		<cfif structKeyExists(url,"logout") and arguments.personID neq -1>
			<cfset result.filterUsed = true>
		</cfif>

		<cfreturn result>

	</cffunction>

	
	<cffunction name="AuthenticationFilter_URL_ES" index="20">
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var linkInfo = "">
		<cfset var result = {PersonID=0,Level=0,filterUsed=false}>

		<cfif structKeyExists(url,"es")>

			<cfset linkInfo = application.com.login.verifySSOLink (url.es) >
			<cfif (linkInfo.isValidLink) >
				<cfset result = {filterUsed = true,personid = linkInfo.personid,level = 2,impersonatedbyperson = linkInfo.currentuser}>
			<cfelse>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="SSO",Severity="debug",warningStructure=linkInfo)>
			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="AuthenticationFilter_Social" index="20">
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=arguments.personid,Level=arguments.level,filterUsed=false}>
		<cfset var callbackResult = "">
		<cfset var serviceID = "">
		<cfset var doesUserExistWithServiceEntityID = "">

		<cfif structKeyExists(url,"s") and application.com.settings.getSetting("socialMedia.enableSocialMedia")> <!--- TBD WAB and isInternal --->

			<cfset result.filterused = true>
			<cfset serviceID = application.com.encryption.decryptString(encryptType="standard",string=url.s)>
			<cfif serviceID eq "">
				<cfset serviceId = url.s>
			</cfif>
			<cfset result.serviceID = serviceID>

			<cfset callbackResult = application.com.service.handleCallback(serviceID=serviceID)>
			<cfset result.accessToken = callbackResult.accessToken>
			<!--- this is the situation where a user has not logged in yet and is authenticating. The callback result will tell
				us if it's ok to authenticate --->
			<cfif not Level and callbackResult.authenticateUser and callbackResult.isOK>
				<cfset session.accessToken = callbackResult.accessToken>
				<cfset doesUserExistWithServiceEntityID = application.com.service.authenticateUserViaService(serviceID=serviceID)>

				<!--- a link has been established already, so we log them in. --->
				<cfif doesUserExistWithServiceEntityID.validUser>
					<cfset result.personid = doesUserExistWithServiceEntityID.personID>
					<cfset result.level = 2>
					<cfif serviceID eq "linkedIn">
						<cfset setPersonImage()><!--- WAB TBD check that correct personid known here --->
					</cfif>
					<cfset result.loginWithService = true>
				<cfelse>
					<!--- A link has not yet been established between RW and the service, but we're setting the user's service ID in request scope so that we can link
					them in callback.cfm  --->
					<cfset request.serviceEntityID = doesUserExistWithServiceEntityID.serviceEntityID>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="AuthenticationFilter_Mobile" index="30" >
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=0,Level=0,filterUsed=false}>
		<cfset var mobilePersonID = application.com.mobile.validateMobileRequest()>

		<cfif mobilePersonID is not 0 and ((mobilePersonID is session.RelayCurrentUserStructure.personid and not session.relayCurrentUserStructure.isLoggedIn) OR mobilePersonID is not session.RelayCurrentUserStructure.personid) >
			<cfset result = {PersonID=mobilePersonID,Level=2,filterUsed=true,source="mobile"}>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="AuthenticationFilter_P" index="30" newSessionOnly="true">
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=0,Level=0,filterUsed=false}>

			<cfif (not isInternal and structKeyExists(url,"p") and structKeyExists(url,"a") and application.com.login.checkMagicNUmber(p) is not 0) >
			<!---
				// this is a link coming in from an communuication or such like
				// and the p number is valid  (note that the checkmagic number function called here isn't quite as good at checking for dedupes than the one called by remote.cfm [but I used it because it was in a cfc!]
			--->
					<cfset result = {personid = application.com.login.checkMagicNUmber(p), level = 0, filterUsed = true}>

					<cfif structKeyExists(cookie,"dejavu") and cookie.dejavu is result.personid>
						<cfset result.level = 1>
					</cfif>

			</cfif>
		<cfreturn result>

	</cffunction>

	<cffunction name="AuthenticationFilter_DejaVu" index="30" newSessionOnly="true">
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=0,Level=0,filterUsed=false}>

			<cfif not isInternal and structKeyExists(cookie,"dejavu") and isNumeric(cookie.dejavu) and (not structKeyExists(cookie,"user") or cookie.dejavu is listfirst(cookie.user,"-")) and application.com.dedupe.whoHasThisPersonBeenDedupedTo(cookie.dejavu) is not 0 >
	<!--- 							// if there is a dejavu cookie and it is the same as the user cookie (or there isn't a user cookie then we will use that  --->
				<cfset result.PersonID = application.com.dedupe.whoHasThisPersonBeenDedupedTo(cookie.dejavu)>
				<cfset result.Level = 1>
				<cfset result.filterUsed = true>
	<!---
								// does the current site have a security profile?  If so then we need to check that this person is valid
								// WAB 2008-02-05  modified to use new function for testing validity - now below

								isValidUser = application.com.login.isPersonAValidUserOnTheCurrentSite (thepersonid).isValidUser;
								if (isValidUser) {
									loginLevel = 1 ;
								}
	 --->
			</cfif>

		<cfreturn result>


	</cffunction>

	<cffunction name="AuthenticationFilter_Cookie" index="998" newSessionOnly="true" >
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=0,Level=0,filterUsed=false}>


			<cfif structKeyExists(cookie,"user") and isNumeric(listfirst(cookie.user,"-")) and listfirst(cookie.user,"-") is not application.unknownpersonid>
	<!---
								// there is a user cookie and it isn't the unknown person
								// WAB 2009-05-18  added a check for the user cookie not being tampered with
	 --->

				<cfif checkUserCookie()>
					<cfset result.filterUsed = true>
					<cfset result.PersonID = listfirst(cookie.user,"-")>
					<!--- 	// check that this is a real personID, or could be a deduped personid	 --->
					<cfset result.PersonID = application.com.dedupe.whoHasThisPersonBeenDedupedTo (result.PersonID)>
				</cfif>
			</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="AuthenticationFilter_SAMLCore" index="60" >
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID = personID, Level = Level, filterUsed=false} />

		<cfif isDefined("form") and structKeyExists (form,"samlResponse") and Level LT 2>
			<cfscript>
				var samlAuthenticationFilter=new singleSignOn.SAML.SP.SAMLAuthenticationFilterPlugin();

				result=samlAuthenticationFilter.runFilter(personID,true,level);


			</cfscript>


		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="AuthenticationFilter_OAuth2Core" index="70">
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID = personID, Level = Level, filterUsed=false} />

        <cfscript>
            var oAuthFilter = new singleSignOn.OAuth2.client.OAuth2AuthenticationFilterPlugin();

            result = oAuthFilter.runFilter(personID,true,level);
        </cfscript>

		<cfreturn result />

	</cffunction>

	<!--- For a new session, the fallback position is setting the user as the unknown user --->
	<cffunction name="AuthenticationFilter_UnknownUser" index="999" newSessionOnly="true" >
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=0,Level=0,filterUsed=true}>

		<cfreturn result>

	</cffunction>


	<!--- For a new session, the fallback position is setting the user as the unknown user
	<cffunction name="AuthenticationFilter_Test" index="999" >
		<cfargument name="personID">
		<cfargument name="isInternal">
		<cfargument name="Level">

		<cfset var result = {PersonID=1,Level=0,filterUsed=true}>

		<cfreturn result>

	</cffunction>
	 --->


</cfcomponent>