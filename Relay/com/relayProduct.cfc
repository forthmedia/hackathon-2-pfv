<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		relayProduct.cfc

2014-05-14 PPB Case 439562
2014-08-27	RPW	Can't view High Res product image in product listing
2015/07/10	NJH	add sort column in qoq so order is case sensitive for some of the product picker queries...
2015/08/05  DAN K-1314 - allow filtering products by multiple media groups
2015-11-12  DAN PROD2015-364 - products country restrictions

--->

<cfcomponent displayname="relayProduct" hint="This provides functions for products in the product cataloge.">

	<cffunction name="InsertProductGroup" access="public" returntype="numeric" hint="Adds a product group if it doesn't exist and returns the product group ID">
		<cfargument name="productGroupDesc" type="string" required="yes">

		<cfscript>
			var getProductGroupID="";
		</cfscript>

		<cfquery name="getProductGroupID" datasource="#application.siteDataSource#">
			if not exists (select 1 from productGroup where description =  <cf_queryparam value="#arguments.productGroupDesc#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					insert into productGroup (description) values (<cf_queryparam value="#arguments.productGroupDesc#" CFSQLTYPE="CF_SQL_VARCHAR" >)
					select scope_identity() as productGroupID
				end
			else
				begin
					select productGroupID from productGroup where description =  <cf_queryparam value="#arguments.productGroupDesc#" CFSQLTYPE="CF_SQL_VARCHAR" >
				end
		</cfquery>

		<cfreturn getProductGroupID.productGroupID>
	</cffunction>




	<cffunction name="InsertProductCategory" access="public" returntype="numeric" hint="Adds a product category if it doesn't exist and returns the product category ID">
		<cfargument name="productCategoryName" type="string" required="yes">

		<cfscript>
			var getProductCategoryID="";
		</cfscript>

		<cfquery name="getProductCategoryID" datasource="#application.siteDataSource#">
			if not exists (select 1 from productCategory where productCategoryName =  <cf_queryparam value="#arguments.productCategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					insert into productCategory (productCategoryName,productCategoryFactor) values (<cf_queryparam value="#arguments.productCategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >,0)
					select scope_identity() as productCategoryID
				end
			else
				begin
					select productCategoryID from productCategory where productCategoryName =  <cf_queryparam value="#arguments.productCategoryName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				end
		</cfquery>

		<cfreturn getProductCategoryID.productCategoryID>
	</cffunction>



	<cffunction name="InsertProductGroupCategory" access="public" hint="">
		<cfargument name="productCategoryID" type="numeric" required="yes">
		<cfargument name="productGroupID" type="numeric" required="yes">

		<cfscript>
			var insertProductGroupCategoryIfDoesNotExist="";
		</cfscript>

		<cfquery name="insertProductGroupCategoryIfDoesNotExist" datasource="#application.siteDataSource#">
			if not exists (select 1 from productGroupCategory where productCategoryID = #arguments.productCategoryID# and productGroupID = #arguments.productGroupID#)
				begin
					insert into productGroupCategory (productGroupID,productCategoryID) values (<cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.productCategoryID#" CFSQLTYPE="cf_sql_integer" >)
				end
		</cfquery>
	</cffunction>


	<cffunction name="InsertProduct" access="public" hint="Inserts a product into the product table if it doesn't exist. Otherwise the product is updated if the 'updateExisting' argument is set to true.">
		<cfargument name="campaignID" type="numeric" required="yes">
		<cfargument name="SKU" type="string" required="yes">
		<cfargument name="deleted" type="boolean" default="false">
		<cfargument name="description" type="string" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="discountPrice" type="numeric">
		<cfargument name="listPrice" type="numeric">
		<cfargument name="PriceISOCode" type="string">
		<cfargument name="includeImage" type="boolean" default="0">
		<cfargument name="productGroupID" type="numeric" required="yes">
		<cfargument name="ProductAvailability" type="string">
		<cfargument name="accrualMethod" type="string" default="M">
		<cfargument name="updateExisting" type="boolean" default="false">

		<cfscript>
			var insertProductIfDoesNotExist="";
			var suppressed = 0;
			var GetproductID = "";
		</cfscript>

		<cfif arguments.deleted>
			<cfset suppressed = 1>
		<cfelse>
			<cfset suppressed = 0>
		</cfif>

		<cfquery name="insertProductIfDoesNotExist" datasource="#application.siteDataSource#">
			if not exists (select 1 from product where
					campaignID = #arguments.campaignID# and
					SKU =  <cf_queryparam value="#arguments.SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
					countryID = #arguments.countryID#
				)
				begin
					insert into product (
						campaignID,
						sku,
						deleted,
						description,
						countryID,
						<cfif isdefined("arguments.discountPrice")>discountPrice,</cfif>
						<cfif isdefined("arguments.listPrice")>listPrice,</cfif>
						<cfif isdefined("arguments.PriceISOCode")>PriceISOCode,</cfif>
						productGroupID,
						accrualMethod,
						includeImage,
						<cfif isdefined("arguments.ProductAvailability")>ProductAvailability,</cfif>
						created,
						createdBy,
						lastUpdated,
						lastUpdatedBy)
					values (
						<cf_queryparam value="#arguments.campaignID#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#arguments.SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#suppressed#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#arguments.countryID#" CFSQLTYPE="cf_sql_integer" >,
						<cfif isdefined("arguments.discountPrice")><cf_queryparam value="#arguments.discountPrice#" CFSQLTYPE="cf_sql_numeric" >,</cfif>
						<cfif isdefined("arguments.listPrice")><cf_queryparam value="#arguments.listPrice#" CFSQLTYPE="cf_sql_numeric" >,</cfif>
						<cfif isdefined("arguments.PriceISOCode")><cf_queryparam value="#arguments.PriceISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
						<cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#arguments.accrualMethod#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#arguments.includeImage#" CFSQLTYPE="CF_SQL_bit" >,
						<cfif isdefined("arguments.ProductAvailability")><cf_queryparam value="#arguments.ProductAvailability#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
						GetDate(),
						<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
						GetDate(),
						<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)
				end
			<cfif updateExisting>
			else
				begin
					update product set
						deleted =  <cf_queryparam value="#suppressed#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						description =  <cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						<cfif isdefined("arguments.discountPrice")>discountPrice = #arguments.discountPrice#,</cfif>
						<cfif isdefined("arguments.listPrice")>listprice = #arguments.listPrice#,</cfif>
						<cfif isdefined("arguments.PriceISOCode")>PriceISOCode = '#arguments.PriceISOCode#',</cfif>
						productGroupID = #arguments.productGroupID#,
						accrualMethod =  <cf_queryparam value="#arguments.accrualMethod#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						includeImage =  <cf_queryparam value="#arguments.includeImage#" CFSQLTYPE="CF_SQL_bit" > ,
						<cfif isdefined("arguments.ProductAvailability")>ProductAvailability='#arguments.ProductAvailability#',</cfif>
						lastUpdated = GetDate(),
						lastUpdatedBy = #request.relayCurrentUser.personID#
					where countryID = #arguments.countryID# and
						SKU =  <cf_queryparam value="#arguments.SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
						campaignID = #arguments.campaignID#
				end
			</cfif>
		</cfquery>
			<cfquery name="GetproductID" datasource="#application.siteDataSource#">
				select *
				from product
				where countryID = #arguments.countryID# and
					  SKU =  <cf_queryparam value="#arguments.SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
					  campaignID = #arguments.campaignID#
			</cfquery>

			<cfreturn GetproductID.productID>
	</cffunction>


	<cffunction name="InsertProductFlag" access="public" hint="Inserts a product flag for a given product">
		<cfargument name="productID" type="numeric" required="yes">
		<cfargument name="flagID" type="numeric" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">

		<cfscript>
			var insertProductFlagIfDoesNotExist="";
		</cfscript>

		<cfquery name="insertProductFlagIfDoesNotExist" datasource="#application.siteDataSource#">
			if not exists (select 1 from productFlag where productID = #arguments.productID# and flagID = #arguments.flagID# and countryID = #arguments.countryID#)
				begin
					insert into productFlag (productID,flagID,countryID) values (<cf_queryparam value="#arguments.productID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.flagID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.countryID#" CFSQLTYPE="cf_sql_integer" >)
				end
		</cfquery>
	</cffunction>

	<!--- NJH 2013Roadmap 2012/12/05 --->
	<cffunction name="getProductCurrencies" access="public" returnType="query" validValues="true">

		<cfset var getCurrencies = "">
		<cfset var currencyList = "">

		<cfset getCurrencies = application.com.relayCountries.getCountryCurrencies()>

		<cfif application.com.relayMenu.isModuleActive(moduleID="Incentive")>
			<cfset queryAddRow(getCurrencies,1)>
			<cfset querySetCell(getCurrencies,"currencyISO","RWP")>
			<cfset querySetCell(getCurrencies,"currencyName","Incentive Points")>
		</cfif>

		<cfreturn getCurrencies>
	</cffunction>


	<cffunction name="getProductGroups" access="public" returnType="query" validvalues="true">
		<cfargument name="productCategoryID" type="numeric" required="false">

		<cfset var getProductGroupsQry="">

		<cfquery name="getProductGroupsQry" datasource="#application.siteDataSource#">
			select pg.productGroupID,pg.description_defaultTranslation as description,pg.title_defaultTranslation as Title,
				pc.title_defaultTranslation as productCategoryTitle
			from productGroup pg
				left join productCategory pc on pg.productCategoryID = pc.productCategoryID
			where isNull(pg.active,1) = 1
				and isNull(pc.active,1) = 1
			<cfif structKeyExists(arguments,"productCategoryID")>
				and pg.productCategoryID = <cf_queryparam value="#arguments.productCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			order by pc.title_defaultTranslation,description
		</cfquery>

		<cfreturn getProductGroupsQry>
	</cffunction>


	<cffunction name="getProductCategories" access="public" returnType="query" validValues="true">

		<cfset var getProductCategoriesQry="">

		<cfquery name="getProductCategoriesQry" datasource="#application.siteDataSource#">
			select productCategoryID,title_defaultTranslation as productCategory
			from productCategory
			order by title_defaultTranslation
		</cfquery>

		<cfreturn getProductCategoriesQry>
	</cffunction>


	<cffunction name="getProductCampaigns" access="public" returnType="query" validValues="true">

		<cfset var getProductCampaignsQry="">

		<cfquery name="getProductCampaignsQry" datasource="#application.siteDataSource#">
			select promoName as campaign,campaignID from promotion order by promoName
		</cfquery>

		<cfreturn getProductCampaignsQry>
	</cffunction>


	<cffunction name="getProductTypes" access="public" returnType="query" validValues="true">

		<cfset var getProductTypesQry="">

		<cfquery name="getProductTypesQry" datasource="#application.siteDataSource#">
			select name, ID as productTypeID from productType order by name
		</cfquery>

		<cfreturn getProductTypesQry>
	</cffunction>

	<!--- 2013-12-17	WAB	CASE 438446 join to country rights was not taking into account that product.countryid = 0 should be accessible to all countries --->
	<!--- 2014-05-09 	PPB Case 439562 added OR p.countryid = 37 --->
	<cffunction name="getProductSelectorProducts" access="public" returntype="query">
		<cfargument name="productGroupID" type="numeric" required="false">
		<cfargument name="productCategoryID" type="numeric" required="false">
		<cfargument name="productID" type="numeric" required="false">
		<cfargument name="additionalCols" type="string" required="false">
		<cfargument name="productCatalogueCampaignID" type="numeric" default="#application.com.settings.getSetting('products.productCatalogueCampaignID')#">
        <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

		<!--- 2014-08-27	RPW	Can't view High Res product image in product listing --->
		<cfset var getProductsQry = "">
		<cfset var Col = "">
		<!--- <cfset var rightsFilter = application.com.rights.getRightsFilterQuerySnippet(entityType="product",alias="p",useJoins=false)> --->
		<cfset var productTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="product", phraseTextID = "title",baseTableAlias="p")>
		<cfset var productDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="product", phraseTextID = "description",baseTableAlias="p")>
		<cfset var productGroupDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="productGroup", phraseTextID = "description",baseTableAlias="pg")>
		<cfset var productLongDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="product", phraseTextID = "LongDescription",baseTableAlias="p")>
		<cfset var productGroupTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="productGroup", phraseTextID = "title",baseTableAlias="pg")>
		<cfset var productGroupPortalTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="productGroup", phraseTextID = "portalTitle",baseTableAlias="pg")>
		<cfset var productCategoryDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="productCategory", phraseTextID = "description",baseTableAlias="pc")>
		<cfset var productCategoryTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType="productCategory", phraseTextID = "title",baseTableAlias="pc")>

		<!--- 2014-08-27	RPW	Can't view High Res product image in product listing --->
		<cfquery name="getProductsQry" datasource="#application.siteDataSource#">
			select distinct pc.productCategoryID,pc.productCategoryName,
				pg.productGroupID,
				p.productID,p.sku,
				<cfif structKeyExists(arguments,"additionalCols")>
					<cfloop list="#arguments.additionalCols#" index = "Col">
						p.#Col#,
					</cfloop>
				</cfif>
				#preserveSingleQuotes(productTitlePhraseQuerySnippets.select)# as title,
				#preserveSingleQuotes(productDescPhraseQuerySnippets.select)# as description,
				#preserveSingleQuotes(productGroupDescPhraseQuerySnippets.select)# as productGroupDescription,
				#preserveSingleQuotes(productLongDescPhraseQuerySnippets.select)# as productLongDescription,
				#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)# as productGroupTitle,
				#preserveSingleQuotes(productGroupPortalTitlePhraseQuerySnippets.select)# as productGroupPortalTitle,
				#preserveSingleQuotes(productCategoryDescPhraseQuerySnippets.select)# as productCategoryDescription,
				#preserveSingleQuotes(productCategoryTitlePhraseQuerySnippets.select)# as productCategoryTitle,
				'#application.paths.content#\linkImages\product\'+ cast(p.productID as varchar)+'\Thumb_'+cast(p.productID as varchar)+'.jpg' as productThumbnailPath,
				'#request.currentSite.protocolAndDomain#/content/linkImages/product/'+ cast(p.productID as varchar)+'/Thumb_'+cast(p.productID as varchar)+'.jpg' as productThumbnailUrl,
				'#application.paths.content#\linkImages\product\'+ cast(p.productID as varchar)+'\highRes_'+cast(p.productID as varchar)+'.jpg' as productHighResPath,
				'#request.currentSite.protocolAndDomain#/content/linkImages/product/'+ cast(p.productID as varchar)+'/highRes_'+cast(p.productID as varchar)+'.jpg' as productHighResUrl,
				'#application.paths.content#\linkImages\productGroup\'+ cast(pg.productGroupID as varchar)+'\Thumb_'+cast(pg.productGroupID as varchar)+'.jpg' as productGroupThumbnailPath,
				'#request.currentSite.protocolAndDomain#/content/linkImages/productGroup/'+ cast(pg.productGroupID as varchar)+'/Thumb_'+cast(pg.productGroupID as varchar)+'.jpg' as productGroupThumbnailUrl,
				'#application.paths.content#\linkImages\productCategory\'+ cast(pc.productCategoryID as varchar)+'\Thumb_'+cast(pc.productCategoryID as varchar)+'.jpg' as productCategoryThumbnailPath,
				'#request.currentSite.protocolAndDomain#/content/linkImages/productCategory/'+ cast(pc.productCategoryID as varchar)+'/Thumb_'+cast(pc.productCategoryID as varchar)+'.jpg' as productCategoryThumbnailUrl
			from product p
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
				inner join productGroup pg on p.productGroupID = pg.productGroupID
				<!--- #RightsFilter.join# --->
				inner join promotion pr on pr.campaignID = p.campaignID
				left join productCategory pc on pc.productCategoryID = pg.productCategoryID
				#preservesinglequotes(productTitlePhraseQuerySnippets.join)#
				#preservesinglequotes(productDescPhraseQuerySnippets.join)#
				#preservesinglequotes(productGroupDescPhraseQuerySnippets.join)#
				#preservesinglequotes(productLongDescPhraseQuerySnippets.join)#
				#preservesinglequotes(productGroupTitlePhraseQuerySnippets.join)#
				#preservesinglequotes(productCategoryDescPhraseQuerySnippets.join)#
				#preservesinglequotes(productCategoryTitlePhraseQuerySnippets.join)#
				#preservesinglequotes(productGroupPortalTitlePhraseQuerySnippets.join)#
			where
				isNull(deleted,0) != 1
				and isNull(p.active,1) = 1
				and isNull(pg.active,1) = 1
				and isNull(pc.active,1) = 1
					and p.campaignID = <cf_queryparam value="#arguments.productCatalogueCampaignID#" cfsqltype="CF_SQL_INTEGER">
				<!--- and ((1=1 #RightsFilter.whereClause#) OR p.countryid = 0 OR p.countryid = 37) --->			<!--- 2014-05-09 PPB Case 439562 added OR p.countryid = 37 --->
                and (p.hascountryScope = 0 or vcs.countryid is not null)
				<cfif structKeyExists(arguments,"productCategoryID")>
					and pc.productCategoryID = <cf_queryparam value="#arguments.productCategoryID#" cfsqltype="CF_SQL_INTEGER">
				</cfif>
				<cfif structKeyExists(arguments,"productGroupID")>
					and pg.productGroupID = <cf_queryparam value="#arguments.productGroupID#" cfsqltype="CF_SQL_INTEGER">
				</cfif>
				<cfif structKeyExists(arguments,"productID")>
					and p.productID = <cf_queryparam value="#arguments.productID#" cfsqltype="CF_SQL_INTEGER">
				</cfif>
			order by #preserveSingleQuotes(productCategoryTitlePhraseQuerySnippets.select)#,#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)#,p.sku
		</cfquery>

		<cfloop query="getProductsQry">
			<cfif not fileExists(productThumbnailPath)>
				<cfset querySetCell(getProductsQry,"productThumbnailPath","",currentRow)>
				<cfset querySetCell(getProductsQry,"productThumbnailURL","",currentRow)>
			</cfif>
			<cfif not fileExists(productGroupThumbnailPath)>
				<cfset querySetCell(getProductsQry,"productGroupThumbnailPath","",currentRow)>
				<cfset querySetCell(getProductsQry,"productGroupThumbnailUrl","",currentRow)>
			</cfif>
			<cfif not fileExists(productCategoryThumbnailPath)>
				<cfset querySetCell(getProductsQry,"productCategoryThumbnailPath","",currentRow)>
				<cfset querySetCell(getProductsQry,"productCategoryThumbnailUrl","",currentRow)>
			</cfif>
		</cfloop>

		<cfreturn getProductsQry>
	</cffunction>


	<cffunction name="getProductAssets" access="remote">
		<cfargument name="productID" type="numeric" required="true">
		<!--- 2015-08-05 DAN K-1314 --->
		<cfargument name="FileTypeGroupID" type="ANY" required="false" default="0">
		<cfargument name="FileTypeID" type="ANY" required="false" default="0">
		<cfargument name="fuzzyProductMatch" type="boolean" required="false" default="yes">

		<!--- 2016-11-18 SSPS-80 Depending on fuzzyProductMatch parameter match assests strictly by SKU or use fuzzy matching --->
		<cfset var fields = not arguments.fuzzyProductMatch ? "sku" : "sku,title_defaultTranslation,description_defaultTranslation,description">
		<cfset var keywords = application.com.search.getKeywordsForEntity(entityType="product",entityID=arguments.productID,entityFields=fields, fuzzyMatch=arguments.fuzzyProductMatch)>
		<cfset var getRelatedMediaAssets = application.com.filemanager.getFilesAPersonHasViewRightsTo(personid=request.relayCurrentUser.personID,checkExpires = true,keywords=valueList(keywords.data),filetypegroupid=arguments.FileTypeGroupID,filetypeid=arguments.FileTypeID)>

		<cfquery name="getRelatedMediaAssets" dbtype="query">
			select name,fileUrl as url, type, fileID,revision,filesize,filename,path, fileNameAndPath, HEADING,source from getRelatedMediaAssets order by type,name
		</cfquery>

		<cfreturn getRelatedMediaAssets>
	</cffunction>


	<!--- NJH 2015/07/10 - add sort column as in qoq so order is case sensitive...--->
	<cffunction name="getProductSelectorCategories" access="public" returntype="query">

		<cfset var getProductCategoriesQry = "">
		<cfset var productsQry = getProductSelectorProducts(argumentCollection=arguments)>

		<cfquery name="getProductCategoriesQry" dbtype="query">
			select distinct productCategoryTitle as title, productCategoryDescription as description, productCategoryID, productCategoryThumbnailURL as thumbnailURL,
				lower(productCategoryTitle) as productCategoryTitleSort
			from productsQry
			where  productCategoryTitle <> '' OR productCategoryDescription <> ''		<!--- 2014-05-14 PPB Case 439562 avoid blank row in dropdown in RWPromotions.cfm --->
			order by productCategoryTitleSort
		</cfquery>

		<cfreturn getProductCategoriesQry>
	</cffunction>


	<!--- NJH 2015/07/10 - add sort column as in qoq so order is case sensitive. Also changed column that order is done on as productSelector outputs title, not description..--->
	<cffunction name="getProductSelectorGroups" access="public" returntype="query">
		<cfargument name="productCategoryID" type="numeric" required="false">

		<cfset var getProductGroupsQry = "">
		<cfset var productsQry = getProductSelectorProducts(argumentCollection=arguments)>

		<cfquery name="getProductGroupsQry" dbtype="query">
			select distinct productGroupTitle as title, productGroupPortalTitle as portalTitle, productGroupDescription as description, productGroupID, productGroupThumbnailURL as thumbnailURL,
				lower(productGroupTitle) as productGroupTitleSort
			from productsQry
			where 1=1
			<!--- START 2014-06-17 PPB Case 439562 when used with a TwoSelects control then we don't want the selected items in the From list; getProducts() also specifically filters the selected items for the To list but that doesn't seem necessary here --->
			<cfif structKeyExists(arguments,"list") and (structKeyExists(arguments,"selected") and arguments.selected neq '')>
				and productGroupID not in(#arguments.selected#)
			</cfif>
			<!--- END PPB Case 439562 --->
			order by productGroupTitleSort
		</cfquery>

		<cfreturn getProductGroupsQry>
	</cffunction>

	<cffunction name="getProductKeywords" access="public" validValues="true" returnType="query">

		<cfset var getProductKeywordsQry = "">

		<cfquery name="getProductKeywordsQry" datasource="#application.siteDataSource#">
			select distinct sku as dataValue, sku as displayValue from product
			where campaignID=<cf_queryparam value="#application.com.settings.getSetting('products.productCatalogueCampaignID')#" cfsqltype="CF_SQL_INTEGER">
				and active != 0
				and deleted != 1
			order by sku
		</cfquery>

		<cfreturn getProductKeywordsQry>
	</cffunction>
</cfcomponent>
