﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name: communications.cfc

2005-02-22		WAB		Added function getCommDetailRecord to deal with long feedback
2005-09-07		WAB		correction to bug in prepareEmailMergeText
2005-09-21		WAB		CR_SNY529 Item 3: Added gatekeeper role information to the gatekeeper query
Oct 05			WAB		Changed to work with MX7 - couldn't save a query within a query so the getCommunication had to be changed to return a structure
2006-04-24		WAB		changed function getCommunicationCountryIDList to ignore orgs in vOrgsToSupressFromReports
2006-06-19		WAB		added code to deal with <NOBR> tags which devedit mangles
2008/07/08		WAB		introduce commselection sent status of 3 - which means sending.  And rudimentary a test for broken comms
2008/07/11 		WAB		Deal with performance issues on stats queries.  Rejigged views, indexes, triggers and a query
2008/09/10 		WAB		Alter a stats query to deal with commstatusid is null (basically items which have broken down)
2008/10/08		WAB		Alter stats clickthru query to correct problem with the number of distinct clicks - problem with union on query and deduped people
2009/01/14 		WAB  	Performance probs query getCommCOuntries1
2009/06/00 		WAB  	LID 2361 New Functions created taking code from tskcommsend and templates/qryGetCommToSend.cfm
2009/02/11  	WAB  	LID  2816 - problem when template caching is turned on - make comm file names unique
2009/11/17		NAS		LID 2835 added 'with (NoLock)' to 'clickThroughsUniquepeople'
2010/10/05		WAB		replaced application.userfiles absolutepath & application.files directory with application.paths.content
2010/10/11 		WAB/SKP  LID 4107  dateDiff was using "m" rather than "n" for minutes with rather bad results
2010/11/17		WAB		LID 4864 Don't disallow "application." in emails, but disallow "application.com." instead
2011-11-20 		NYB		LHID 4918 - added support for CommEmailFormat being db driven
2011-03-25 		NYB		added a default to CommEmailFormatID in getCommEmailFormat fn
2011-03-28 		NYB		LHID5664: only change LastKnownisApproved from 1 to 0 if it hasn't already been sent, or due to be sent in the next 5 minutes
2011/06/   		WAB   		Moved communication content to the db and do merging with the merge object
2011-07-04 		NYB		P-LEN024
2011-07-04 		NYB		P-LEN024 changed getValidTemplates to only return valid filetypes - ie, txt, html and htm
2011-07-04 		NYB		P-LEN024
2011-08-25 		NYB		P-LEN024 changed track reg expression - was too strict
2011-08-26 		NYB 	LHID7595 changes to prepareEmailForMerge - added concept of containsBodyTag and containsHTMLTag - and the code around them
2111-08-31		PPB		REL109 changes to getCommsSentForPerson for My Mailbox on portal
2011/09/06		WAB		Fix problems with track attribute on links in comms
							RegExp problem when some links tracked and some not
							Problems with track=No  (code wasn't written to deal with a negative!)
							LID 7702 Problems with mailTo:  - should not be tracked at all
2011/09/06		WAB		LID 7729 Fix problem when email content has :: in
2011/10/11		PPB		NET005 created getCommTypesLID: moved function getCommType from commHeader.cfm to allow Netgear to override
2011/10/06		WAB		Moved all omniture tracking code from merge.cfc into here.
						Major varing of all functions
						LID 7454
2011/11/01 		WAB		LID8078 problem with bounceback processing (dates with timezone info in them)
2011/11/07		WAB		created function saveEmailTemplate()
						created function updateContentLastUpdated() which is called by saveEmailContent() and other files
						pass htmlcontent to varioius functions in an argument collection to prevent appearing in debug (on dev sites)
2011/11/07		WAB 	LID 8103 problem with prepareEmailForMerge () if anchor.href was blank
2012-01-30  	WAB		CRM426370 function prepareEmailForMerge(). 'LinkToSiteURL' not being used to work out the base URL
2012-02-01 		NYB 	Case 425248: added hasRightsToSend and hasRightsToSchedule to getCommunication
2012-03-14		WAB 	Improve Comms Speed by going back to using a .cfm file (but now within our merge object)
2012/04/02		IH		Case 426955 reformat table in function formatTskCommSendIndividualResultStructureV1 not to use "colspan"
2012/04/19		IH		Case 424774 commented out query "clickThroughsUniquepeople" and used the previous query to get sum of "people" and "clicks"
2012-11-06 		WAB 		CASE 431771 GetCommContent() fell over if there was no content!
2012-11-08 		WAB 	CASE 426507. Improved performance of getCommunicationStatisticsQuerys()
2013-01-10 		PPB 	Case 432207 remove the comments pair <!--     --> from within a <style> tag

2013-01			WAB		Major communication works - many functions moved to communicate\communication.cfc
						Still more work which could be done
2013-04-04 		NYB 	Case 434535 (Netgear) added some with(nolocks) to queries in getCommunicationStatisticsQuerys
2013-05-08		WAB		CASE 433974 Added standard functions for converting emailStatus to words
2013-08-15		YMA		Case 436313 in various places replaced fromDisplayName with fromDisplayNameID and made relevant joins to new table to store valid values.
2013-10-15		WAB		During CASE 437315 Added function for cancelling a communication during sending
2013-11-01		WAB 	CASE 437307 - Functions setting/reading metadata on communication (for download)
2013-11-27		AXA		CASE 438178 add condition to CommsSent query
2014-08-13		RPW		Create ability to choose template for Standard Emails
2014-09-29		WAB		Get bounceback phrases from emailPhrase (not relayserver.dbo.emailphrases)
2014-10-30 		WAB 	CASE 442416 Automatically initialise the bounceback server
2015-03-11		WAB		CASE 444102 Bouncebacks failing because some code from 2013_2  had not come through
2014-09-29 WAB During CASE 439422.  Function parseBounceBack(). Change way matches are found in the emailPhrases table, (also do away with regExp column which was not being used)
2015-10-13		WAB 	CASE PROD2015-203
2016-01-20		WAB 	BF-32 Reverted changes of PROD2015-203.  Instead improved code which removes unique variables from URLs
2016-01-29		SB		Fixed table col
2016-06-06		WAB		BF-960 Fix problem saving template (personid being used instead of usergroupid)
--->


<cfcomponent>


	<cfset this.Constants = {externalSelectLID = 0,approverSelectLID = 11,internalSelectLID = 12,excludeSelectLID = 13,failureSelectLID = 10}>


	<cffunction access="public" name="getCommunication" returnType="struct" hint="Gets the main details for a communication." ouput="false">
		<cfreturn getCommunicationObject(argumentCollection = arguments )>
	</cffunction>

	<cffunction access="public" name="getCommunicationObject" returnType="struct" hint="Gets the main details for a communication." ouput="false">
		<cfargument name="commid" type="string" required="true">
		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">

			<cfset var communicationObject = createObject ("component","communicate.communication")>
			<cfset communicationObject = communicationObject.init(commid = Commid,personid = personid)>

		<cfreturn communicationObject>
	</cffunction>

	<cffunction access="public" name="getCommSendResultObject" returnType="any" hint="Recreates a CommResultObject from a database record" ouput="false">
		<cfargument name="commSendResultId" type="string" required="true">

			<cfset var result = {}>

			<cfset result.object =  createObject ("component","communicate.commSendResult")>
			<cfset result.isOK = result.object.initialiseFromDB(commSendResultId = commSendResultId)>

		<cfreturn result >
	</cffunction>


	<cffunction name="createAttachmentsTable" >
		<cfargument name="attachmentsQuery" required="true">
		<cfargument name="showDeleteLink" default="false">

		<cfset var result_html = "">

			<cfsavecontent variable = "result_html">
					<cfoutput>
					<TABLE>
					<cfloop query="attachmentsQuery">
						<TR><TD >#htmleditformat(OriginalFileName)# </td><td>(#htmleditformat(FileSize)#KB) </td><td><A HREF="javascript:void(openWin('CommPreviewAttachment.cfm?path=#jsstringformat(relativePath)#','','width=800,height=800'))">Phr_Sys_view</a></td><td> <cfif showDeleteLink><a href="##" onclick="void(deleteAttachment(#frmCommID#,#commFileID#,this))">phr_sys_delete</a></cfif></TD></TR>

					</cfloop>
					</TABLE>
					</cfoutput>
			</cfsavecontent>

		<cfreturn result_html>
	</cffunction>




<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      	Gets the CommEmailFormats available
		NYB 2011-01-20 added as part of LHID5833
		NYB 2011-03-25 added a default to CommEmailFormatID
		DO NOT RELEASE WITHOUT FIRST RUNNING:
		_support/sql/Relay/Updats/LHID5833 Unable ToCreateHTMLTextComm.sql
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCommEmailFormats" returnType="query">
		<cfargument name="CommEmailFormatID" required="false" type="numeric">

		<cfset var getCommEmailFormats = '' >

		<cfif structkeyexists(arguments,"CommEmailFormatID") and not isNumeric(arguments.CommEmailFormatID)>
				<cfset arguments.CommEmailFormatID = 1>
		</cfif>

		<CFQUERY NAME="getCommEmailFormats" DATASOURCE="#application.SiteDataSource#">
			select * from CommEmailFormat
			<cfif structkeyexists(arguments,"CommEmailFormatID")>
				where CommEmailFormatID = #arguments.CommEmailFormatID#
			</cfif>
			order by CommEmailFormatID
		</CFQUERY>
		<cfreturn getCommEmailFormats>
	</cffunction>




	<!---
	getCommunicationApprovalDetails
	has been cut out of getCommunication as a separate function
	but can be called by itself if required

	 --->


	<!--- WAB 2011/10/19 cut this code from applicationCommunicateINI--->
	<cffunction access="public" name="isUserACommunicationsAdministrator" hint="" returnType = "boolean">
		<cfset var result = false>
		<cfif application.com.login.checkInternalPermissions("CommTask","Level4") IS 1 or application.com.login.checkInternalPermissions("AdminTask","Level1")>
			<cfset result  = true>
		</cfif>
		<cfreturn result>
	</cffunction>


<!--- returns a list of personids of the owners of a communication
	WAB 2005-04-27
	written with a view to there being a possibility of sharing communications so they have multiple owners
 --->
	<cffunction access="public" name="getCommunicationOwners" hint="" returnType = "string">
			<cfargument name="commid" type="numeric" required="true">

			<cfset var getOwners = "">

			<CFQUERY NAME="getOwners" DATASOURCE="#application.SiteDataSource#">
			select
				personid
			from
				communication c
					inner join
				usergroup ug on ug.usergroupid = c.createdby
			where
				c.commid = #arguments.commid#
			</cfquery>

			<cfreturn valuelist(getOwners.personid)>

	</cffunction>

	<cffunction access="public" name="getCommunicationOwnersCountryIDList" hint="" returnType = "string">
			<cfargument name="commid" type="numeric" required="true">

			<cfreturn application.com.rights.getCountryRightsIDListForMultiplePeople (getCommunicationOwners(#CommID#))>

	</cffunction>





<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns a list of campaigns
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCampaigns" returnType="query"
		hint="Returns a list of campaigns">

		<cfset var getCampaigns = '' />

		<CFQUERY NAME="getCampaigns" DATASOURCE="#application.siteDataSource#">
			select campaignID, campaignName
			from campaign order by campaignName
		</CFQUERY>
		<cfreturn getCampaigns>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns a list of commDetailStatus records
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCommDetailStatus" returnType="query"
		hint="Returns a list of getCommDetailStatus records">

		<cfset var getCommDetailStatus = "">

		<CFQUERY NAME="getCommDetailStatus" DATASOURCE="#application.siteDataSource#">
			select commStatusID, Description
			from getCommDetailStatus order by Description
		</CFQUERY>
		<cfreturn qGetCommDetailStatus>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns a commdetail record
			WAB 2013-11-21 Feedback is now nvarchar(max).  Do not have to deal with feedback longer than 4000 characters (previously spread over multiple records)
			WAB 2013-12-03 2013RoadMap2 Item 25 Contact History.  Store Subject/Body in new CommDetailContent table rather than in the feedback column
							(no longer need to worry about size of feedback column either)
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCommDetailRecord" returnType="query" hint="Returns a  getCommDetail record">
		<cfargument name="commdetailid" type="numeric" required="true">
		<cfargument name="getMessage" type="boolean" default="false"> <!--- NJH 2013/02/28 - Case 433940 - provide support for comm messages - a message is part of a comm --->

			<cfset var theFeedback = "">
			<cfset var Feedbacks = "">
			<cfset var detail = "">

			<CFQUERY name="Detail" datasource="#application.siteDataSource#">
				SELECT p.firstName+' '+p.lastname AS fullName,
					  CD.PersonID,
					  (SELECT OrganisationName
					      FROM organisation o
			        INNER JOIN location l ON l.organisationid = o.organisationid
			        	 WHERE l.locationid = cd.locationid) AS orgName,

					  CD.Feedback,
					  CD.DateSent,
					isNull(cd.lastupdated,cd.datesent) as lastupdated,
				   cd.commTypeId,
				   cd.commReasonId,
				   cd.commStatusId,
				   cdr.name as CommReason,
					  CD.CommDetailID,
					  cdt.showInAddComm,
					  CD.CommID,
				   	<cfif not arguments.getMessage>
					   	CASE WHEN cdc.subject is not null then cdc.subject else comm.Title end as Title,
					   	cdc.body ,
					   	cdt.name as commType,
					   	cds.name as commStatus,
					<cfelse>
						title.phraseText as title,
						'' as body,
						'Message' as commType,
						'Sent' as commStatus,
					</cfif>
					dbo.Campaign.campaignName as commCampaign
				  FROM commdetail cd
				<cfif arguments.getMessage>
					inner join vPhrases title on title.entityID = cd.commID and title.entityTypeID =  <cf_queryparam value="#application.entityTypeID.communication#" CFSQLTYPE="CF_SQL_INTEGER" >  and title.phraseTextID='messageTitle'
				</cfif>
			LEFT JOIN CommDetailContent CDC ON cd.commdetailID = cdc.commdetailid
			LEFT OUTER JOIN Person p ON CD.PersonID = p.PersonID
			LEFT OUTER JOIN Communication comm ON CD.CommID = comm.CommID
			     LEFT JOIN commDetailStatus cds ON cds.commStatusId = cd.commStatusId
			   	LEFT JOIN commDetailReason cdr ON cdr.commReasonId = cd.commReasonId
			     LEFT JOIN commDetailType cdt ON cdt.commTypeId = cd.commTypeId
 LEFT OUTER JOIN dbo.Campaign ON cd.CommCampaignID = dbo.Campaign.campaignID
			     WHERE cd.commdetailid =  #arguments.commdetailid#
			</CFQUERY>


		<cfreturn Detail>
	</cffunction>



	<!---
		WAB 2011/11/07 changed to return whether content updated (ie whether there was a change in the content)
						Then realised that I should update within this function
	--->

	<!---
		 WAB 2011/11/07 moved this code from commFileAdd.cfm
		WAB 2011/10 this template section is somewhat anachronistic now that email content is stored in phrases
			Templates still use the code which combines the HTML and TEXT versions of the email into the same file
	 --->

	<cffunction name="saveEmailTemplate">
		<cfargument name="HTMLBody" type="string" >
		<cfargument name="TextBody" type="string" >
		<cfargument name="TemplateName" type="string" >

		<cfset var checkfileexists = "">
		<cfset var result = {isOK = true}>
		<cfset var combinedfilecontent = combineHTMLandText(HTMLBody,TextBody)>
		<cfset var addFileResult = '' />

		<!--- User wishes to save email text for future use --->
		<CFSET var doctoredTemplateName =  rereplace(TemplateName,'[\\\/:\*\?"<>\|]',"","ALL")>  <!--- added to remove illegal characters, probably ought to do more --->
		<CFSET var TemplateFileName =  doctoredTemplateName & #request.relayCurrentUser.userGroupID# & "-" & DateFormat(Now(), "yyyymmdd") & TimeFormat(Now(), "HHmmss") & ".txt">
		<CFSET var fullname = application.paths.content & "\emailtemplates\" & TemplateFileName>

		<!--- save file - 2008/02/14 GCC force into UTF-8 and read later as UTF-8 --->
		<CFFILE ACTION="WRITE" FILE="#fullname#" OUTPUT="#combinedfilecontent#" charset="utf-8">

		<!--- check save successful --->
		<CFDIRECTORY ACTION="list"
			DIRECTORY="#application.paths.content#\emailtemplates\"
			FILTER="#TemplateFileName#"
			NAME="checkfileexists">


			<CFIF checkfileexists.recordcount IS NOT 0>

				<cfset addFileResult = application.com.fileManager.addFileToDb(fileTypeID = 5,
																				name = doctoredTemplateName,
																				filename = TemplateFileName,
																				personid = request.relayCurrentUser.personID
																				)>


				<!--- set permissions for this file to this user 
					2016-06-06	WAB	BF-960 Fix problem with this query - personid being used instead of usergroupid
				--->
					<CFQUERY DATASOURCE="#application.SiteDataSource#">
					INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
							values( <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_Integer" >,'files',<cf_queryparam value="#addFileResult.fileid#" CFSQLTYPE="CF_SQL_INTEGER" >,'3')
					</cfquery>
					<cfset result.fileid = addFileResult.fileid>

			<cfelse>
					<cfset result.isOK = false>

			</cfif>

		<cfreturn result>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Combine HTML and Text versions of an email into a single variable for saving as a file
		NOTE: WAB 2011/06  This Code now only used for saving email templates (and ought to be replaced anyway!)
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="combineHTMLandText" returnType="string"
		hint="Combine HTML and Text versions of an email into a single variable for saving as a file">
		<cfargument name="HTML" type="string" required="true">
		<cfargument name="Text" type="string" required="true">

		<cfset var temp = "">
		<cfset arguments.html = doctorHTMLbeforeSave(html)>

		<CFIF trim(text) is not "" and trim(html) is not "">
		 	<cfset temp = trim(HTML) & "<!--TEXTVERSION--" & Text & "--ENDTEXTVERSION-->" >
		<cfelse>
			<cfset temp = trim(HTML) & Text>
		</cfif>

		<cfreturn temp>
	</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             extract HTML and Text versions of an email from a single variable
		NOTE: WAB 2011/06  This Code now only used for reading email templates and an upgrade script
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="extractHTMLandText" returnType="struct"
		hint="extract HTML and Text versions of an email from a single variable ">
		<cfargument name="Content" type="string" required="true">

				<cfset var result = structnew()>
				<cfset var pos = 0>
				<CFSET result.HTML= "">
				<CFSET result.text= "">
				<CFSET result.MultiPartMessage= false>
				<CFSET result.SinglePartMessageFormat = "">

			<cfset pos = findnocase("<!--TEXTVERSION--",content)>

		<CFIF pos is 0 >
			<!--- just a single format in the file --->
			<CFIF findnocase("<HTML",content) is 0 >
				<CFSET result.Text = content>
				<CFSET result.SinglePartMessageFormat = "Text">
				<CFSET result.MessageFormat = "Text">
			<cfelse>
				<CFSET result.HTML = content>
				<CFSET result.SinglePartMessageFormat = "HTML">
				<CFSET result.MessageFormat = "HTML">
			</cfif>

		<CFELSE>
			<CFIF pos is not 1> <!--- should never be 1, but was once while debugging --->
				<cfset result.html = left (content, pos-1)>
			</CFIF>
			<cfset result.text =rereplacenocase(content,".*(<!--TEXTVERSION--)(.*)(--ENDTEXTVERSION-->)","\2")>
			<CFSET result.MultiPartMessage= true>
			<CFSET result.MessageFormat = "HTML,TEXT">
		</CFIF>


		<cfreturn result>
	</cffunction>


	<cffunction access="public" name="doctorHTMLbeforeSave" returnType="string">
		<cfargument name="HTML" type="string" required="true">

		<cfset var result = html>
		<!--- remove junk HREF stuff put in by devedit --->
		<CFSET result = stripDevEditHRefStuff (result)>
		<CFSET result = stripDevEditAbsoluteImagePath (result)>
		<CFSET result = stripDevEditMetaGeneratorStuff (result)>
		<CFSET result = replaceDevEditApostrophes (result)>  <!--- WAB 2006-04-12 replaces &#039 with ' --->
		<CFSET result = devEditOtherReplacementsBeforeSave(result)>


		<cfreturn result>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        Strip out http://sitedomain/urlroot bits which have been put into HREFs by devedit
		probably a better way of doing this (maybe preventing devedit putting them there in the first place, but could work out a way

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="stripDevEditHRefStuff" returnType="string"
		hint="Strip out request.currentSite.httpProtocol sitedomain/urlroot bits which have been put into HREFs by devedit">
		<cfargument name="HTML" type="string" required="true">
		<cfset var result ="">
		<cfset var re ="">


<!--- 		2005-04-21 WAB changed regular expression to solve a problem - had a habit of running away and gobbling up whole documents!
		<cfset re = "#request.currentSite.protocolAndDomain#.*?\[\[">
--->
		<cfset re = "#request.currentSite.protocolAndDomain#[[:graph:]]*?\[\[">
		<CFSET result = reReplace(HTML,re,"[[","ALL")>


		<cfreturn result>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        Strip out http://sitedomain from images to leave a relative path
		We add back an absolute path to an external site before sending
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!--- WAB 2011/06/14 --->
	<cffunction access="public" name="stripDevEditAbsoluteImagePath" returnType="string"
		hint="Strip out request.currentSite.httpProtocol sitedomain/urlroot bits which have been put into IMGs by devedit">
		<cfargument name="HTML" type="string" required="true">
		<cfset var result ="">
		<cfset var re ="">


		<cfset re = '(src=")https?://#request.currentsite.domainandroot#'>
		<CFSET result = reReplace(HTML,re,'\1',"ALL")>

		<cfreturn result>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        Strip out multiple <meta content="MSHTML 6.00.2900.2802" name="GENERATOR"/> bits which have been put in by devedit
		WAB 2005-12-19
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="stripDevEditMetaGeneratorStuff" returnType="string"
		hint="Strip out <meta name='GENERATOR' content='MSHTML 8.00.6001.18975' /> bits which have been put into HREFs by devedit">
		<cfargument name="HTML" type="string" required="true">
		<cfset var result ="">
		<cfset var re ="">

		<cfset re = "<meta name=""GENERATOR"" content=""MSHTML.*?\/>">
		<CFSET result = reReplaceNoCASE(HTML,re,"","ALL")>   <!--- WAB 2011/06/14 add no case --->

		<cfreturn result>
	</cffunction>



	<cffunction access="public" name="devEditOtherReplacementsBeforeSave" returnType="string"
		hint="">
		<cfargument name="HTML" type="string" required="true">
		<cfset var result = html>


		<!--- replaces instances of &#60;NOBR&#62; and &#60;/NOBR&#62; with <nobr> </nobr>
			devEdit automatically
		--->
		<cfset result = replaceNoCase (result,"&##60;NOBR&##62;","<NOBR>","ALL")>
		<cfset result = replaceNoCase (result,"&##60;/NOBR&##62;","</NOBR>","ALL")>

		<!--- 2013-01-10 PPB Case 432207 remove the comments pair <!-- --> from within a <style> tag cos the style wasn't being applied in the editor --->
		<cfset result = reReplaceNoCase (result,"(<style.*?>\s*)<!--(.*?)-->(\s*</style>)","\1\2\3","ALL")>

		<cfreturn result>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		WAB 2006-04-12
		dev edit falls over if text includes &#039 (apostrophes).  Strip them out before saving
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="replaceDevEditApostrophes" returnType="string"
		hint="replaces instances of &##039; with ' (note actually a single hash)">
		<cfargument name="HTML" type="string" required="true">
		<cfset var result ="">

		<cfset result = replaceNoCase (HTML,"&##039;","'","ALL")>

		<cfreturn result>
	</cffunction>



<!---WAB Lenovo 2012-03-03 put blank hrefs in all anchors - otherwise ckeditor can't edit them --->
	<cffunction name="dealWithAnchorsWithNoHREF" returntype="string">
		<cfargument name="Content" type="string" required="true">

		<cfset var allAnchors = application.com.regExp.findHTMLTagsInString (content,"A")>
		<cfset var result = Content>
		<cfset var anchor = "">
		<cfset var newLink = "">

		<cfloop array="#allanchors#" index="anchor">
			<cfif not StructKeyExists(anchor.attributes,"href")  >
				<cfset newLink = replaceNoCase(anchor.string,"<A",'<A href="" ',"ONE")>
				<cfset result = replace(result,anchor.string,newlink)>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>



<!---
WAB 2011/06/21 replaced loop in this function with a single function call, changed return type (and changed the places where it is called)
--->
	<cffunction name="checkForNonTrackedLinksInEmail" returntype="array">
		<cfargument name="Content" type="string" required="true">

		<cfset var allAnchors = application.com.regExp.findHTMLTagsInString (content,"A")>
		<cfset var anchor="">
		<!--- look for HREFs which aren't being tracked in any way
		basically looks for any href which doesn't start with [
			also ignores bookmarks #
			WAB 2011/09/06 LID 7702 - ignore mailTo: links
			WAB 2011/09/06  deal with track=no
		--->
		<cfset var result = arrayNew(1)>

		<cfloop array="#allanchors#" index="anchor">
			<cfif (not StructKeyExists(anchor.attributes,"track")  or not anchor.attributes.track)
					and structKeyExists(anchor.attributes,"href")
					and left(anchor.attributes.href,1) is not "##"
					and anchor.attributes.href does not contain "[[" and anchor.attributes.href does not contain "mailto:"
			>
				<cfset arrayAppend (result,anchor)>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="replaceNonTrackedLinksInEmail" returntype="string">
		<cfargument name="Content" type="string" required="true">
		<cfargument name="LinkToSiteURL" type="string" required="false" >

		<!--- look for HREFs which aren't being tracked in any way --->
		<CFSET var Result = content>
		<cfset var link = "">
		<CFSET var nonTrackedLinks = "">

		<!--- NYB P-LEN024 2011-07-04 replaced - didn't work properly
		<CFSET var re2 = 'HREF="([^"##\[]*)#LinkToSiteURL#.*?\?([^"\[]*)eid=([^"\[&]*)([^"\[]*)"'>
		<CFSET var replacestring2 = 'HREF="[[RelayPageLink(''\3'')]]&\2\4"'>
		--->
		<!--- looks for eid items and replaces	--->
		<CFSET var re2 = '(<A)([^>]*)(HREF=")([^"##\[ '']*)#LinkToSiteURL#([^"\[ >]*)[?]eid=([1-9a-zA-Z]*)([^&]*)([^" '']*)(["''])'>
		<CFSET var replacestring2 = "\1\2\3[[RelayPageLinkV2(elementid=\6,additionalParameters='\8')]]">

		<!--- looks for seid items - result of [[RelayPageLinkV2]] merge - and replaces	--->
		<CFSET var re4 = '(<A)([^>]*)(HREF=")([^"##\[ '']*)#LinkToSiteURL#([^"\[ >]*)\?seid=([^\-]*)\-tr([^&]*)([^" '']*)(["''])'>
		<CFSET var replacestring4 = "\1\2\3[[RelayPageLinkV2(elementid=\6,additionalParameters='\8')]]">

		<!--- looks for seid items - result of [[SecurePageLink]] merge - and replaces	--->
		<CFSET var re5 = '(<A)([^>]*)(HREF=")([^"##\[ '']*)#LinkToSiteURL#([^"\[ >]*)\?seid=([^\-]*)\-se([^&]*)([^"''\)]*)(["''])'>
		<CFSET var replacestring5 = "\1\2\3[[SecurePageLink(elementid=\6,days=30,additionalParameters='\8')]]">

		<CFSET var re6 = '(<A)([^>]*)(HREF\=)(["''])([^"'']*)http[s]*://#LinkToSiteURL#/remote.cfm([^\&])*([\&])([^\&])*([\&])([^\&])*([\&])([^\&])*([\&])fid=(#application.flagid["NoCommstypeall"]#)([^"'']*)'>
		<CFSET var replacestring6 = "\1\2\3[[Unsubscribe]]">

		<CFSET var re7 = '(<A)([^>]*)(HREF\=)(["''])([^"'']*)http[s]*://#LinkToSiteURL#/remote.cfm([^\&])*([\&])([^\&])*([\&])([^\&])*([\&])([^\&])*([\&])fid=([^"'']*)'>
		<CFSET var replacestring7 = "\1\2\3[[UnsubscribeThisType]]">

		<!--- looks for eeid items and replaces--->
		<CFSET var re3 = 'HREF="([^"##\[]*)#LinkToSiteURL#.*?\?([^"\[]*)eeid=([^"\[&]*)([^"\[]*)"'>
		<CFSET var replacestring3 = 'HREF="[[RelayPageLinkV2(''\3'')]]&\2\4"'>

		<CFSET var re8 = '(<A)([^>]*)(HREF\=)(["''])([^"'']*)http[s]*://#LinkToSiteURL#/remote.cfm\?t=([0-9]*)&c=([0-9]*)&a=d&p=([0-9]*)-([0-9]*)&f=([0-9]*)'>
		<CFSET var replacestring8 = "\1\2\3[[Download]]&##38;f=\10">

		<cfset Result = rereplacenocase(Result,re3,replaceString3,"ALL")>
		<cfset Result = replacenocase(Result,"&amp;","&","ALL")>
		<cfset Result = rereplacenocase(Result,re4,replaceString4,"ALL")>
		<cfset Result = rereplacenocase(Result,re5,replaceString5,"ALL")>
		<cfset Result = rereplacenocase(Result,re6,replaceString6,"ALL")>
		<cfset Result = rereplacenocase(Result,re7,replaceString7,"ALL")>
		<cfset Result = rereplacenocase(Result,re2,replaceString2,"ALL")>
		<cfset Result = rereplacenocase(Result,re8,replaceString8,"ALL")>

		<cfset nonTrackedLinks = checkForNonTrackedLinksInEmail(result)>
		<cfloop array="#nonTrackedLinks#" index="link">
			<cfset result = replacenocase(result,link.attributeString,link.attributeString & ' track="true"',"ALL")>
		</cfloop>

<!--- WAB 2011/09/06 I don't think that this piece of code below is necessary
	as far as I can tell LINK tags aren't touched by the tracking code
--->
		<!--- START:  NYB LEN024 2011-06-20 - remove tracking put into LINK tags --->
		<cfset Result = rereplacenocase(Result,'(<LINK.*?HREF=["''][[]*trackedLink)([(]["''])(.*?)(["''][)][]]*)','\1\2<-REMOVETRACKING\3\5',"all")>
		<cfset Result = replacenocase(Result,'[[trackedLink("<-REMOVETRACKING','',"all")>
		<cfset Result = replacenocase(Result,"[[trackedLink('<-REMOVETRACKING",'',"all")>
		<!--- END:  NYB LEN024 2011-06-20 --->
		<cfreturn result>

	</cffunction>



<!---
adds omniture tracking to a piece of content
assumes that you have already tested whether omniture tracking is on (using application.com.settings.getSetting('Communications.Tracking.AllowOmnitureTracking'))

 --->


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             getValidTemplates
			 Returns the set of email templates available to a user.

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<!--- 2014-08-13		RPW		Create ability to choose template for Standard Emails --->
<cffunction name="getValidTemplates" returntype="query" validValues="true">
	<cfset var GetEmailTemplatesQry = "">
	<CFQUERY NAME="GetEmailTemplatesQry" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT 1 as priority, f.fileid,fT.path, f.filename, f.name as description
		FROM
			FILES AS F INNER JOIN FILETYPE AS FT ON F.FILETYPEID = FT.FILETYPEID
		WHERE FT.path = 'emailtemplates'
		AND F.personID = #request.relayCurrentUser.personid#
		<!--- NYB 2011-08-18 P-LEN024 added this AND to limit to valid filetypes --->
		AND (f.filename like '%.txt' or f.filename like '%.htm' or f.filename like '%.html')
		UNION
		SELECT DISTINCT 2 as priority, f.fileid,fT.path, f.filename, f.name as description
		FROM
			FILES AS F INNER JOIN FILETYPE AS FT ON F.FILETYPEID = FT.FILETYPEID
			LEFT JOIN

				(SELECT RECORDID,PERSONID, ENTITY,	Convert(bit,sum(permission & 1)) AS Level1
					FROM RECORDRIGHTS RR
							INNER JOIN
						RIGHTSgROUP RG
							ON RR.USERGROUPID = RG.USERGROUPID
					GROUP BY RECORDID, PERSONID, ENTITY)
					AS RIGHTS

			ON F.FILEID = RIGHTS.RECORDID	AND RIGHTS.ENTITY = 'FILES' AND RIGHTS.PERSONID  = #request.relayCurrentUser.personid#
		WHERE FT.path = 'emailtemplates'
			AND F.personID <> #request.relayCurrentUser.personid#
			AND (RIGHTS.PERSONID IS NOT NULL OR F.PERSONID = #request.relayCurrentUser.personid#   )
			AND (f.filename like '%.txt' or f.filename like '%.htm' or f.filename like '%.html')
		ORDER BY priority, F.name
	</CFQUERY>
	<cfreturn GetEmailTemplatesQry>
</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             insertCommunication
			 Inserts the communication returning the commId

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="insertCommunication" returntype="numeric">
	<cfargument name="frmProjectRef" required="Yes">
	<cfargument name="frmCampaignID" required="Yes">
	<cfargument name="frmTitle" required="Yes">
	<cfargument name="frmDescription" required="Yes">
	<cfargument name="frmReplyTo" required="Yes">
	<cfargument name="frmfromAddress" required="Yes">
	<cfargument name="frmCommFromDisplayNameID" required="Yes">
	<cfargument name="frmCommTypeLID" required="Yes">
	<cfargument name="frmCommFormLID" required="Yes">
	<cfargument name="frmtrackemail" required="Yes">
	<cfargument name="frmNotes" required="Yes">
	<cfargument name="FreezeDate" required="no" default="#now()#">

	<cfset var insertCommQry = '' />

		<CFQUERY NAME="insertCommQry" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO Communication (
						ProjectRef,
						campaignID,
						Title,
						Description,
						ReplyTo,
						fromAddress,
						commFromDisplayNameID,
						CommTypeLID,
						CommFormLID,
						TrackEmail,
						Notes,
						SendDate,
						Sent,
						TestFlag,

						CreatedBy, Created, LastUpdatedBy, LastUpdated)
			VALUES (
						<cf_queryparam value="#arguments.frmProjectRef#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#arguments.frmCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#arguments.frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#arguments.frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#arguments.frmReplyTo#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<!--- I do not update these variables if they are still the same as the default application variables
							this means that if the application variables are changed then we aren't left with old (or wrong) details in communications which haven't been sent out yet --->
						<cfif frmfromAddress is not "#application.emailFrom#@#application.mailfromdomain#">  <cf_queryparam value="#arguments.frmfromAddress#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse>null</cfif>,
						<cfif isNumeric("#arguments.frmCommFromDisplayNameID#")><cf_queryparam value="#arguments.frmCommFromDisplayNameID#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse>null</cfif>,
						<cf_queryparam value="#arguments.frmCommTypeLID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#arguments.frmCommFormLID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#arguments.frmtrackemail#" CFSQLTYPE="CF_SQL_bit" >,
						<cf_queryparam value="#arguments.frmNotes#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						null,
						0,
						0,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#arguments.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#arguments.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)

						select scope_identity() as commid
		</CFQUERY>

	<cfreturn insertCommQry.CommID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             getValidTemplates
			 Returns the set of email templates available to a user.

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="insertCommunicationFile">
	<cfargument name="commID" required="Yes">
	<cfargument name="frmCommFormLID" required="Yes">
	<cfargument name="frmselectid" required="Yes">
	<cfargument name="frmMsg_Txt" required="No" default="">
	<cfargument name="frmMsg_HTML" required="No" default="">
	<cfargument name="FreezeDate" default="#now()#">
</cffunction>


<cffunction name="getCommunicationStatisticsQuerys">
	<cfargument name="CommID" required="Yes" type="numeric">
	<cfargument name="refresh" default = "false">
	<cfargument name="countryID" default="">
	<cfargument name="filterCode" required="Yes">
	<cfargument name="debug" default = false>


	<cfset var result = structNew()>
	<cfset var filter = "">
	<cfset var updateMostSig = "">
	<cfset var EmailStatistics	= "">
	<cfset var EmailStatisticsResult	= "">
	<cfset var clickThroughs= "">
	<cfset var clickThroughsResult = "">
	<cfset var clickThroughsUniquepeople= "">
	<cfset var clickThroughsUniquepeopleResult= "">
	<cfset var TotalSent = '' />
	<cfset var TotalReceived = '' />
	 <cfset var CommCounts = '' />


	<cfset filter = getCommunicationFilter(countryid=countryid, filtercode = filtercode)>

		<!--- WAB 2008/07/11 new mostsignificantcommstatusid  column added.  This code back populatesit for historical data--->
		<!--- CASE: 426507 : PJP 17/09/2012:  Comment out this query update as no longer needed, advice from WAB --->
		<!--- <CFQUERY name="updateMostSig" datasource="#application.sitedatasource#" >
		if exists (select 1 from information_schema.columns where table_name = 'commdetail' and column_name = 'mostsignificantcommstatusid')
			BEGIN
			update commdetail set mostsignificantcommstatusid = null
			where commid = #commid# and mostsignificantcommstatusid is null and commstatusid is not null
			END
		</CFQUERY> --->
		<!--- WAB 2012-11-08 CASE 426507 added NOLOCK and don't need to use vCommDetailMostSignificantStatus--->
				<CF_QUERYWithCache name="EmailStatistics" datasource="#application.sitedatasource#" cachedWithin = "#createTimeSpan(0,0,20,0)#" refreshcachenow = "#refresh#">
					<cfoutput>

				select
				commstatusid as status,
				statsReport,statsReportOrder,
				StatusGroup,
				case when StatusGroup = 'NotReceived' then 'Failed' when StatusGroup = 'Sent' then 'Unconfirmed' when StatusGroup = 'Received' then 'Successful' when StatusGroup = 'NotSent' then 'Not Sent' else 'Unclassified' end as Result ,
				isnull(mostSignificant.countOfPersonid,0) as MostSignificantCount,
				isnull(allx.countOfPersonid,0) as AllStatusCount

				from
				commDetailStatus cds with (NOLOCK)

				left join (

				select
				 MostSignificantCommstatusid as status,
				count(distinct cd.personid) as countOfPersonid
				 from
					Commdetail cd with (NOLOCK)
					#filter.country#
					where
					cd.commid = #commid#
					and #filter.TestInternalExternal#
				group by MostSignificantCommstatusid
				) as mostSignificant on isnull(mostSignificant.status,0) = cds.commstatusid  <!--- WAB 2008/09/10 added isNull to get the null items (Not processed yet) into the statistics --->


				left join (
				select
				 commstatusid as status,
				count(distinct cd.personid) as countOfPersonid
				 from
					commdetailHistory cd with (NOLOCK)
					#filter.country#
					where
					cd.commid = #commid#
					and #filter.TestInternalExternal#
					and commstatusid > 10
				group by commstatusid
				) as Allx on allx.status = cds.commstatusid

				where mostSignificant.countOfPersonid is not null or Allx.countOfPersonid is not null
				order by
					case when StatusGroup = 'NotReceived' then 4 when StatusGroup = 'Sent' then 2 when StatusGroup = 'Received' then 1 when StatusGroup = 'NotSent' then 5 else 6 end  ,
					significance desc

					</cfoutput>
					</CF_QUERYWithCache>
		<cfset result.emailStatistics = 	EmailStatistics>

		<cfquery name="TotalSent" dbtype="query">
		select sum(MostSignificantCount) as number from EmailStatistics where statusGroup not in ('NotSent')
		</cfquery>
		<cfquery name="TotalReceived" dbtype="query">
		select sum(MostSignificantCount) as number  from EmailStatistics where statusGroup in ('Received')
		</cfquery>

		<cfif TotalSent.number is "" or TotalSent.number is 0 or totalReceived.number is "">
			<cfset result.ReadToSent =0>
		<cfelse>
			<cfset result.ReadToSent = 	int (100 * totalReceived.number/TotalSent.number )>
		</cfif>


	<cfquery name="CommCounts" datasource="#application.sitedatasource#">
		SELECT count(distinct cd.personid) as people, count(distinct cd.locationid) as locations
		FROM 	CommDetail as cd with (NOLOCK)
				#filter.country#
		WHERE cd.CommID=#commid#
		and #filter.TestInternalExternal#
	</cfquery>

		<cfset result.commcounts = 	commcounts>



<!---
	FROM commCheckDetailsClickThrus.cfm
	WAB 2004-07-21 	a first stab at click through reporting
	WAB 2005-09-04  added A HREF to the click through list
					discovered that wasn't filtering by internal/external/test and corrected
	WAB 2010/10/12 	Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
					I do have some worries about this, see comment of same date in commheader.cfm
	WAB 2012-11-08 	CASE 426507.  Improved performance by removing the union which seemed redundant and adding NOLOCK
	WAB 2013-04-30	Added domain to the address of a file
	WAB 2015-10-13	CASE PROD2015-203 Removed toLoc from this query because was causing duplication of rows if there were user specific parameters in the URL.
					Have added some items to list of parameters to remove from URL before saving (entityFunctions.cfc:addEntityTrackingUrl())
	WAB 2016-01-20	BF-32 Reverted changes of PROD2015-203.  Instead improved code which removes unique variables from URLs
 --->

	<CF_QUERYWithCache name="clickThroughs" datasource="#application.sitedatasource#" cachedWithin = "#createTimeSpan(0,0,20,0)#" refreshcachenow = "#refresh#">
		<cfoutput>
		declare @linkToSiteURL varchar(100)
		<!--- WAB 2012-11-08 slight improvement to this line, so that can handle linktoSiteURL being blank as well as null)  --->
		select @linkToSiteURL  = isNull(linktoSiteURL,'#application.com.relayCurrentSite.getReciprocalExternalDomain()#') from communication with (NOLOCK) where commid = #commid#


		<!---
		WAB 2008/07/11 performance issues with this query (of mine) having odd joins in it
		recraft as a union.
		SELECT
		case when e.id is not Null	then e.headline
			 when f.fileid is not null then f.name
			else toLoc end as Name,
		case when e.id is not Null	then @linkToSiteURL + '/?seid='+ convert(varchar,e.id)+'-pr'
		else toLoc end as address,
		et.entityTypeID,
		et.entityID,
		toLoc,
		count(distinct cd.personid) as people, count(1) as clicks
		from
			commdetail cd

				#filter.country#

				inner join
			entityTracking et on (cd.commid = et.commid  or (et.entityTypeID = #application.entityTypeID['Communication']# and et.entityid = #frmCommid#))
								and cd.personid = et.personid
				left join
			Element E  on ET.EntityID = e.id and et.entityTypeid = 10
				left join
			Files F  on ET.EntityID = f.fileid and et.entityTypeid = 47

		where
			cd.CommID=#commid#


			and #filter.TestInternalExternal#


		group by 	 e.id, e.headline,toloc, f.name, f.fileid, 	et.entityTypeID,	et.entityID,toLoc
		--->
		<!--- WAB 2012-11-08 CASE 426507
		This query had two bits unioned together, but my tests show that the second part does not pick up anything that has not been picked up by the first part
		I am therefore going to get rid of the second part, 'cause it make it run much faster.
		(I wrote the query myself a long time ago - don't know what I was thinking!)

		SELECT Name, address, entityTypeID, entityID, toLoc, count(distinct personid) as people, count(1) as clicks
		from
		(

				SELECT
				case when e.id is not Null	then e.headline
					 when f.fileid is not null then f.name
					else toLoc end as Name,
				case when e.id is not Null	then @linkToSiteURL + '/?seid='+ convert(varchar,e.id)+'-pr'
				else toLoc end as address,
				et.entityTypeID,
				et.entityID,
				toLoc,
				cd.personid,
				et.entityTrackingID   <!--- WAb 2008/10/08 added et.entityTrackingID.  This prevents the UNION effectively distincting the two parts of the query resulting in us losing distinct clicks, by adding in a entityTrackingID then every item row becomes distinct --->
				from
					commdetail cd

						#filter.country#

						inner join
					entityTracking et on (cd.commid = et.commid  )
										and cd.personid = et.personid
						left join
					Element E  on ET.EntityID = e.id and et.entityTypeid = 10
						left join
					Files F  on ET.EntityID = f.fileid and et.entityTypeid = 47

				where
					cd.CommID=#commid#


					and #filter.TestInternalExternal#

			union

				SELECT
				case when e.id is not Null	then e.headline
					 when f.fileid is not null then f.name
					else toLoc end as Name,
				case when e.id is not Null	then @linkToSiteURL + '/?seid='+ convert(varchar,e.id)+'-pr'
				else toLoc end as address,
				et.entityTypeID,
				et.entityID,
				toLoc,
				cd.personid,
				et.entityTrackingID
				from
					commdetail cd

						#filter.country#

						inner join
					entityTracking et on ((et.entityTypeID = #application.entityTypeID['Communication']# and et.entityid = #arguments.Commid#) and cd.commid <> et.commid )    <!--- WAB 2008/10/08 added cd.commid <> et.commid .  This prevents this part of the union bringing back the same rows as the first half (not essential since the union does remove duplicates, but might be more efficient --->
										and cd.personid = et.personid
						left join
					Element E  on ET.EntityID = e.id and et.entityTypeid = 10
						left join
					Files F  on ET.EntityID = f.fileid and et.entityTypeid = 47

				where
					cd.CommID=#commid#


					and #filter.TestInternalExternal#

		) as x
		group by name, address, entityTypeID, entityID, toLoc
		--->

		<!--- WAB 2012-11-08 CASE 426507
		Took Union out of query - second part did not find anything extra
		Also swapped order of tables
		Added WITH (NOLOCK)
		--->

		SELECT
			case when e.id is not Null	then e.headline
				 when f.fileid is not null then f.name
				else toLoc end as Name,
			case when e.id is not Null	then @linkToSiteURL + '/?seid='+ convert(varchar,e.id)+'-pr'
				when f.fileid is not Null and isNull(f.url,'') = ''	then @linkToSiteURL + '/' + toLoc
				else toLoc end as address,
			et.entityTypeID,
			et.entityID,
			toLoc,
			count(distinct et.personid) as people, count(1) as clicks
		from
		entityTracking et with(nolock)
			inner join
		commdetail cd with(nolock) on cd.commid = et.commid and cd.personid = et.personid

		#filter.country#

			left join
		Element E with (NOLOCK) on ET.EntityID = e.id and et.entityTypeid = 10
			left join
		Files F with (NOLOCK) on ET.EntityID = f.fileid and et.entityTypeid = 47

	where
		et.CommID=#commid#
		and #filter.TestInternalExternal#
	group by
		entityTypeID, entityID, toLoc,e.id,f.fileid,f.url, headline,f.name
	</cfoutput>
	</CF_QUERYWithCache >

	<cfset result.clickThroughs = 	clickThroughs>
	<!---
	<CF_QUERYWithCache name="clickThroughsUniquepeople" datasource="#application.sitedatasource#" cachedWithin = "#createTimeSpan(0,0,20,0)#" refreshcachenow = "#refresh#">
		<cfoutput>
		SELECT
			count(distinct cd.personid) as people, count(distinct entityTrackingID) as clicks
		from
			<!--- 2009/11/17		NAS		LID 2835 added 'with (NoLock)' --->
			commdetail cd with(nolock)

			#filter.country#

				inner join
			<!--- 2009/11/17		NAS		LID 2835 added 'with (NoLock)' --->
			entityTracking et with(nolock) on cd.personid = et.personid and (cd.commid = et.commid or (et.entityTypeID = #application.entityTypeID['Communication']# and et.entityid = #arguments.Commid#))
		where
			cd.CommID=#commid#
		and #filter.TestInternalExternal#

		</cfoutput>
		</CF_QUERYWithCache> --->

	<cfset clickThroughsUniquepeople = {people=0,clicks=0}>

	<cfloop query="clickThroughs">
		<cfset clickThroughsUniquepeople.people += people>
		<cfset clickThroughsUniquepeople.clicks += clicks>
	</cfloop>

	<cfset result.clickThroughsUniquepeople = 	clickThroughsUniquepeople>

	<cfif debug>
		<cfdump var="#result#">
	</cfif>

	<cfreturn result>

</cffunction>

<cffunction name="formatCommunicationStatisticsQuery">
	<cfargument name="CommID" required="Yes">
	<cfargument name="filterCode" required="Yes">   <!--- E,I,T --->
	<cfargument name="countryID" default="">
	<cfargument name="refresh" default = "false">
	<cfargument name="debug" default = false>

	<cfset var helpTextColumn1 = '' />
	<cfset var helpTextColumn2 = '' />
	<cfset var MostSignificantCounter = '' />
	<cfset var groupidString = '' />
	<cfset var idString = '' />
	<cfset var ago = '' />
	<cfset var helpTextColumn1Total = '' />
	<cfset var helpTextColumn2Total = '' />
	<cfset var result_html = '' />
	<cfset var stats = getCommunicationStatisticsQuerys(argumentCollection = arguments)>

	<cfsaveContent variable="result_html">
		<cfoutput>
		<div class="EmailStatisticsRefreshArea form-group">
			<cfif stats.EmailStatistics.querycachedAgo is not "">
				<label>Cached #htmleditformat(stats.EmailStatistics.querycachedAgo)# ago</label>
			</cfif>
			<a href="javascript:loadEmailStats(true)" class="btn btn-primary">Refresh</a>
		</div>
		<TABLE class="table table-hover table-striped">

		<cfset helpTextColumn1 = "A person is only counted in a single category in this column. <BR> If more than one action has been recorded, the person is counted in the topmost category">
		<cfset helpTextColumn2 = "If more than one action has been recorded against a person, <BR> that person will be counted in more than one category in this column">
		<tr>
			<td colspan=2>
				<b>Email Results:</b>
			</td>
			<td>
				## People Distinct<BR>(click number for options) <img src="/images/misc/iconhelpsmall.gif" border="0" onmouseover="javascript:pop('#helpTextColumn1#','',event)" onmouseout="javascript:kill()">
			</td>
			<td>
				## People Total<BR>(click number for options) <img src="/images/misc/iconhelpsmall.gif" border="0" onmouseover="javascript:pop('#helpTextColumn2#','',event)" onmouseout="javascript:kill()">
			</td>
		</tr>

		</cfoutput>

					<CFOUTPUT query="stats.EmailStatistics" group = "result">
					<cfset MostSignificantCounter = 0>
					<cfset groupidString = "frmCommid=#arguments.Commid#&filtercode=#filtercode#&frmCountryID=#CountryID#">

		 	<TR>
				<td align="left" COLSPAN=4><b>#htmleditformat(stats.EmailStatistics.result)#</b></td>
			</tr>

					<cfoutput>
							<cfset MostSignificantCounter = MostSignificantCounter + MostSignificantCount>
							<cfset idString = groupidString & "&status=#status#">
					<TR>
						<TD colspan="2">
							Phr_Sys_Numberof #htmleditformat(StatsReport)#</td>
						<TD>
							<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" onContextMenu="return false" contextmenu = "commEmailStats" id = "#idString#&statusview=MostSignificant" CLASS="smallLink">
							#htmleditformat(MostSignificantCount)#
							</a>
						</TD>

						<TD>
							<cfif AllStatusCount is not 0>
							<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)"   onContextMenu="return false" contextmenu = "commEmailStats" id = "#idString#&statusview=All" CLASS="smallLink">
							#htmleditformat(AllStatusCount)#
							</a>
							<cfelse>
							&nbsp;
							</cfif>

							</TD>
					</TR>
					</cfoutput>
		 	<TR>
				<td colspan=2><B><!--- Total ---><!--- decided to leave out word total - looked a bit cluttered ---></B> <!--- #result# ---></td>
				<td>
					<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" onContextMenu="return false" contextmenu = "commEmailStats" id = "#groupidString#&statusGroup=#statusGroup#&statusview=MostSignificant" CLASS="smallLink" style="font-weight: bold;">
							#htmleditformat(MostSignificantCounter)#
					</A>
				</td>

				<td>&nbsp;<!--- don't show, doesn't make sense to total <B>#AllStatusCounter#</B> ---></td>
			</tr>

			<!---
			<cfif clickThroughs.querycachedAt is not "" or emailStatistics_MostSignificant.querycachedAt is not "">
				<cfif clickThroughs.querycachedAt lt emailStatistics_MostSignificant.querycachedAt >
					<cfset ago = clickThroughs.querycachedAgo>
				<cfelse>
					<cfset ago = emailStatistics_MostSignificant.querycachedAgo>
				</cfif>
					<cfoutput>
					<TR>
					<TD>Statistics were cached #ago# ago <a href="?frmcommid=#frmCommid#&filtercode=#filtercode#&frmCountryID=#frmCountryID#&refreshCommStats=true">Refresh</a></TD>
					</TR>
					</cfoutput>

			</cfif>		 --->

		</CFOUTPUT>

		<cfoutput>
		<tr><TD colspan=4>&nbsp;</TD></tr>
		<tr><TD colspan=2>Total Number of People:</TD><TD><A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" onContextMenu="return false" contextmenu = "commResultsAll" id = "frmCommid=#arguments.Commid#&filtercode=#filtercode#&frmCountryID=#CountryID#" CLASS="smallLink" style="font-weight: bold;"> #htmleditformat(stats.CommCounts.people)#</a></TD><TD>					&nbsp;<!--- <A HREF="commResultsAll.cfm?frmCommID=#frmcommid#&filtercode=#filtercode#">Phr_Sys_Report</A> ---> </TD><TD><!--- <A HREF="CommResultsAllDownload.cfm?frmCOmmID=#frmcommid#&filtercode=#filtercode#">Phr_Sys_Download</a> ---></TD></tr>
		<tr><TD colspan=2>Total Number of Locations: </TD><TD>#htmleditformat(stats.CommCounts.Locations)#</TD><td>&nbsp;</td></tr>
		<tr><TD colspan=2>Read to Sent %: </TD><TD>#htmleditformat(stats.ReadToSent)#%</TD><td>&nbsp;</td></tr>
		</cfoutput>

			<cfif stats.clickThroughs.recordcount is not 0>
				<!--- NJH 2009/02/23 Bug Fix Trend Support Issue 1284 - Added helpTextColumn1Total and helpTextColumn2Total as some explanatory text for the click throughs--->
				<cfset helpTextColumn1 = "Multiple Clicks by the same person to the same page are NOT counted in this column">
				<cfset helpTextColumn1Total = "This is the total number of people who have clicked through to pages.<br>If a person has clicked to more than one page, they are NOT counted twice">
				<cfset helpTextColumn2 = "Multiple Clicks by the same person to the same page ARE counted in this column">
				<cfset helpTextColumn2Total = "This is the total number of Clicks.  Multiple clicks are counted">

				<cfoutput>

				<tr><TD colspan="4"><HR></TD></tr>
				<TR class="label">
					<TD colspan="2"><B>phr_sys_comm_Summary_TrackedLinksTitle</B></td>
					<TD>## People Distinct<BR>
								<img src="/images/misc/iconhelpsmall.gif" border="0" onmouseover="javascript:pop('#helpTextColumn1#','',event)" onmouseout="javascript:kill()">
					</TD>
					<TD>Total ## Clicks<BR>
							<img src="/images/misc/iconhelpsmall.gif" border="0" onmouseover="javascript:pop('#helpTextColumn2#','',event)" onmouseout="javascript:kill()">
					</TD>
				</TR>

				<!--- 2015-10-13 WAB CASE PROD2015-203 Removed reference to toLoc
						2016-01-20 WAB BF-32 reverted this change
				 --->
				<cfloop query = "stats.clickThroughs">
				<TR>
					<td>&nbsp;</td>
					<!--- WAB 2011/10/10 LID 7454, was using request.currentsite.httprotocol (so sometimes using https) which didn't work for URLs on other sites.  Suppose could check for links to our own portals incase those are https!--->
					<TD ><A HREF="#iif(address contains "://",de(""),de("http://"))##address#" target="preview">#htmleditformat(name)#</A></TD>
					<TD>
						<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)"   onContextMenu="return false" contextmenu = "commClickThroughs" id = "frmCommID=#arguments.Commid#&frmentityID=#entityID#&frmEntityTypeID=#EntityTypeID#&frmName=#urlencodedformat(name)#&frmToLoc=#urlencodedformat(toLoc)#&FilterCode=#FilterCode#&frmCountryID=#Countryid#" CLASS="smallLink">
						#htmleditformat(people)#
						</A>
					</TD>


					<TD>#htmleditformat(clicks)#</TD>
				</TR>

				</cfloop>

				<!--- NJH 2009/02/23 Bug Fix Trend Support Issue 1284 - Added some explanatory text for the click throughs--->
				<TR>
					<TD colspan=2>phr_sys_comm_Summary_TrackedLinksTotal</TD>
					<TD>
						<img src="/images/misc/iconhelpsmall.gif" border="0" onmouseover="javascript:pop('#helpTextColumn1Total#','',event)" onmouseout="javascript:kill()">
						<B>#htmleditformat(stats.clickThroughsUniquepeople.people)#</B>
					</TD>
					<TD>
						<img src="/images/misc/iconhelpsmall.gif" border="0" onmouseover="javascript:pop('#helpTextColumn2Total#','',event)" onmouseout="javascript:kill()">
						<B>#htmleditformat(stats.clickThroughsUniquepeople.clicks)#</B>
					</TD>
				</TR>

				</cfoutput>
			</cfif>
		<cfoutput></TABLE></cfoutput>
	</cfsavecontent>

	<cfreturn result_html>

</cffunction>


<cffunction name="getCommunicationFilter">
	<cfargument name="filtercode" required="Yes">
	<cfargument name="countryid" required="Yes">


	<cfset var result = structNew()>
	<cfset var getCountries = "">
	<cfset result.Message = "">


			<cfset result.TestInternalExternal = "(">
			<cfif filtercode contains "T">
				<cfset result.TestInternalExternal = result.TestInternalExternal & "(cd.Test = 1) or ">
				<cfset result.Message = listappend (result.Message, " As Tests")>
			</cfif>
			<cfif filtercode contains "I">
				<cfset result.TestInternalExternal = result.TestInternalExternal & "(cd.Internal = 1 and cd.test = 0) or ">
				<cfset result.Message = listappend (result.Message, " Internally")>
			</cfif>
			<cfif filtercode contains "E">
				<cfset result.TestInternalExternal = result.TestInternalExternal & "(cd.Internal = 0 and cd.test = 0) or ">
				<cfset result.Message = listappend (result.Message, " Externally")>
			</cfif>
			<cfset result.TestInternalExternal = result.TestInternalExternal & "1=0)">
			<cfset result.Message = "Contains items sent" & result.Message >


		<cfset result.country = "">
		<cfif CountryID is not "">
			<cfset result.country = "
							inner join
					person px on px.personid = cd.personid
						inner join
					location l on px.locationid = l.locationid and l.countryid in (#frmCountryID#)

			">
			<cfquery name="getCountries" datasource="#application.sitedatasource#">
			select countryDescription from country where countryid  in ( <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
			<cfset result.Message = result.Message  & " to " & valuelist(getCountries.countryDescription)>

		</cfif>

		<cfreturn result>

</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             getCommunications
			 Returns the set of communications available to a user.

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="getCommunications">
	<cfargument name="personID" required="Yes">
	<cfargument name="SearchBy" default="">
	<cfargument name="selectfieldtype" default="">
	<cfargument name="CommFormLID" default="">
	<cfargument name="ShowTest" default="">
	<cfargument name="ShowOwn" default="">
	<cfargument name="ShowStatus" default="all">
	<cfargument name="UserGroups" default="#request.relayCurrentUser.usergroupid#">

	<cfset var CommunicationsQry = "">

	<cfquery name="CommunicationsQry" datasource="#application.sitedatasource#">
		SELECT
		c.CommID, c.Title, c.Description,
		c.CommTypeLID, c.CommFormLID,LookupList.Itemtext as commform,
		c.Replyto, c.SendDate, c.Sent as sent,
		c.created,
		c.CreatedBy,UserGroup.Name, Location.SiteName,
		Location.CountryID,Country.CountryDescription, CommFile.FileName, CommFile.Directory,
		(select distinct 1 from commselection cs where cs.commid = c.commid and cs.commselectlid =  <cf_queryparam value="#this.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  and cs.sent = 1) as hasBeenSentExternally

		FROM Communication as c INNER JOIN ((UserGroup INNER JOIN Person ON UserGroup.PersonID = Person.PersonID)
			INNER JOIN Location ON Person.LocationID = Location.LocationID) ON c.CreatedBy = UserGroup.UserGroupID
			INNER JOIN Country ON Location.CountryID = Country.CountryID
			INNER JOIN  lookuplist on c.CommFormLID = LookupList.LookupID
				left JOIN commFile ON CommFile.CommID = c.CommID and commfile.commformlid = c.commformlid and commfile.isparent = commfileid


		where 1=1
		<cfif arguments.selectfieldtype is not "">
		and <cf_queryObjectName value="#arguments.SearchBy#">  like  <cf_queryparam value="#arguments.selectfieldtype#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>


		<cfif not (arguments.selectfieldtype is not "" and arguments.Searchby is "c.commid")>
			<!--- if there is a search by commid then we override all the other filters --->
			<cfif arguments.CommFormLID is not "">
				and c.commformlid  in ( <cf_queryparam value="#arguments.CommFormLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>

			<cfif arguments.ShowTest is 0>
				and commTypeLid <> 27
			</cfif>

			<cfif arguments.ShowOwn is 1>
				<!--- Left for completeness --->
				and c.createdby = #request.relayCurrentUser.usergroupid#
			</cfif>
			<cfif len(arguments.UserGroups)>
				and c.createdby  in ( <cf_queryparam value="#arguments.UserGroups#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>

			<cfif arguments.ShowStatus is not "all">
				and sentExternally
				<cfif arguments.ShowStatus is "sent">
					= 1
				<cfelseif arguments.ShowStatus is "unsent">
					= 0
				</cfif>
			</cfif>
			and c.lastupdated > getdate() -60
		</cfif>
		order by c.commid desc
	</cfquery>
	<cfreturn CommunicationsQry>

</cffunction>

<cffunction name="copyCommunication" returntype="numeric">
	<cfargument name="commID" required="Yes" type="numeric">
	<cfargument name="NewTitle" required="No" default="">
	<cfargument name="fromAddress" required="No" default="">
	<cfargument name="commFromDisplayNameID" required="No" default="">
	<cfargument name="ReplyTo" required="No" default="">
	<cfargument name="CopySelections" required="No">
	<cfargument name="CopyContent" required="Yes">

	<cfset var NewCommID = '' />
	<cfset var sDirGetEmailTemplates = '' />
	<cfset var NewCommFileID = '' />
	<cfset var insertComm = '' />
	<cfset var getCommIDQry = '' />
	<cfset var ContentCommFileDetailsQry = '' />
	<cfset var AttachmentsCommFileDetailsQry = '' />
	<cfset var insertContentQry = '' />
	<cfset var getCommFileIDQry = '' />
	<cfset var insertContentFileQry = '' />
	<cfset var copyCommFileAttachmentsQry = '' />
	<cfset var FreezeDate = now()>
	<!---
		There is an argument for CopyAttachment, but for the time being this is not
		deemed as needed. Attachments will be linked in.
	--->
	<!--- DB Model Information
			Communications - Comm information
				CommDetails - Who the comm was sent to. (ignored
				CommSelection - The selections associated with the comm.
				CommFile - The set of attachments
	 --->
	<CFQUERY NAME="insertComm" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO Communication (
			ProjectRef,
			campaignID,
			Title,
			Description,
			ReplyTo,
			fromAddress,
			commFromDisplayNameID,
			CommTypeLID,
			CommFormLID,
			TrackEmail,
			Notes,
			SendDate,
			Sent,
			TestFlag,
			CreatedBy,
			Created,
			LastUpdatedBy,
			LastUpdated)
	 		SELECT
				ProjectRef,
				campaignID,
				<cfif len(arguments.NewTitle)>
					<cf_queryparam value="#arguments.NewTitle#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					Title
				</cfif>,
				Description,
				<cfif len(arguments.ReplyTo)>
					<cf_queryparam value="#arguments.ReplyTo#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					ReplyTo
				</cfif>,
				<cfif len(arguments.fromAddress)>
					<cf_queryparam value="#arguments.fromAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					fromAddress
				</cfif>,
				commFromDisplayNameID,
				CommTypeLID,
				CommFormLID,
				TrackEmail,
				Notes,
				NULL as SendDate,
				0 as Sent,
				TestFlag,
				#request.relayCurrentUser.usergroupid# AS CreatedBy,
				#FreezeDate# AS Created,
				#request.relayCurrentUser.usergroupid# AS LastUpdatedBy,
				#FreezeDate# AS LastUpdated
				FROM Communication
				where commID = #arguments.commID#
	</CFQUERY>

	<CFQUERY NAME="getCommIDQry" DATASOURCE="#application.SiteDataSource#">
		SELECT Max(CommID) AS NewCommID
		  FROM Communication
		 WHERE LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		 AND LastUpdatedBy=#request.relayCurrentUser.usergroupid#
	</CFQUERY>

	<CFSET NewCommID = getCommIDQry.NewCommID>

	<cfif CopySelections>
		<CFQUERY NAME="getCommIDQry" DATASOURCE="#application.SiteDataSource#">
			insert into CommSelection (
				CommID,
				SelectionID,
				CommSelectLID,
				sent,
				sentDate,
				Title_IfOriginalDeleted)
				SELECT
					<cf_queryparam value="#NewCommID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					SelectionID,
					CommSelectLID,
					sent,
					sentDate,
					Title_IfOriginalDeleted
					from CommSelection where commid = #arguments.commid#
		</cfquery>
	</cfif>
	<CFSET sDirGetEmailTemplates = application.paths.content &  "\emailtemplates\">
	<cfif CopyContent>
		<CFQUERY name="ContentCommFileDetailsQry" datasource="#application.sitedatasource#">
			SELECT *
				FROM CommFile
				WHERE CommFile.CommID=#arguments.commid#
				and commformlid = 2 and commfileid = isparent
		</CFQUERY>
		<cfloop query="ContentCommFileDetailsQry">
			<!--- Email Content. Get the set of attachments for the commID --->
			<CFQUERY name="AttachmentsCommFileDetailsQry" datasource="#application.sitedatasource#">
				SELECT *
					FROM CommFile
					WHERE CommFile.CommID=#arguments.commid#
					and commformlid = 2 and commfileid <> isparent
					AND commfileid =  <cf_queryparam value="#ContentCommFileDetailsQry.commfileid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
			<!--- file Copy the content file, then attach the attachments. --->
			<cffile  action="COPY"
 source="#application.userFilesAbsolutePath##Trim(ContentCommFileDetailsQry.Directory)#\#Trim(ContentCommFileDetailsQry.FileName)#" destination="#application.userFilesAbsolutePath##Trim(Directory)#\#NewCommID#-#ContentCommFileDetailsQry.CommFormLID#.txt" nameconflict="MAKEUNIQUE">

			<cfquery name="insertContentQry" datasource="#application.sitedatasource#">
				insert into CommFile (
					CommID,
					CommFormLID,
					Directory,
					FileName,
					OriginalFileName,
					IsParent,
					FileSize,
					CreatedBy,
					Created,
					LastUpdatedBy,
					LastUpdated)
					VALUES (
						<cf_queryparam value="#NewCommID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#ContentCommFileDetailsQry.CommFormLID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#ContentCommFileDetailsQry.Directory#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#NewCommID#-#ContentCommFileDetailsQry.CommFormLID#.txt" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#ContentCommFileDetailsQry.OriginalFileName#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						NULL,
						<cf_queryparam value="#ContentCommFileDetailsQry.FileSize#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					)
			</cfquery>

			<!--- Unsure if this step can be done in one step, but we still need to find the new comm id. --->
			<cfquery name="getCommFileIDQry" datasource="#application.sitedatasource#">
				SELECT Max(CommFileID) AS NewCommFileID
					FROM CommFile
					WHERE LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					AND LastUpdatedBy=#request.relayCurrentUser.usergroupid#
			</cfquery>
			<cfset NewCommFileID = getCommFileIDQry.NewCommFileID>
			<!--- Now update the parent ID.  --->
			<cfquery name="insertContentFileQry" datasource="#application.sitedatasource#">
				update CommFile set
					IsParent =  <cf_queryparam value="#NewCommFileID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE CommFileID =  <cf_queryparam value="#NewCommFileID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<!--- now work on the attachments --->

			<cfquery name="copyCommFileAttachmentsQry" datasource="#application.sitedatasource#">
				insert into CommFile (
					CommID,
					CommFormLID,
					Directory,
					FileName,
					OriginalFileName,
					IsParent,
					FileSize,
					CreatedBy,
					Created,
					LastUpdatedBy,
					LastUpdated)
					SELECT
						<cf_queryparam value="#NewCommID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						CommFormLID,
						Directory,
						FileName,
						OriginalFileName,
						<cf_queryparam value="#NewCommFileID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						FileSize,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
						FROM CommFile
						WHERE IsParent =  <cf_queryparam value="#ContentCommFileDetailsQry.CommFileID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND CommFileID <> IsParent
			</cfquery>
		</cfloop>

	</cfif>

	<cfreturn newCommID>
</cffunction>




<cffunction name="shouldCommunicationWarningBeSent" returntype="boolean">
	<cfargument name="commID">
	<cfargument name="Warning">
	<cfargument name="lastupdated">
	<cfargument name="hoursBetweenwarnings" default = "24">

	<cfparam name = "application.commWarningsSent" default=#structNew()#>

	<cfif not structKeyExists(application.commWarningsSent,commid)>
		<cfreturn true>
	<cfelseif not structKeyExists(application.commWarningsSent[commid],warning)>
		<cfreturn true>
	<cfelseif dateAdd("h",hoursBetweenWarnings,application.commWarningsSent[commid][warning].dateSent) lt now()>
		<cfreturn true>
	<cfelseif dateAdd("h",hoursBetweenWarnings,application.commWarningsSent[commid][warning].dateSent) lt lastUpdated>
		<cfreturn true>
	<cfelse>
		<cfreturn false>
	</cfif>

</cffunction>


<cffunction name="communicationWarningSent" >
	<cfargument name="commID">
	<cfargument name="Warning">

	<cfparam name = "application.commWarningsSent" default=#structNew()#>
	<cfparam name = "application.commWarningsSent[commid]" default=#structNew()#>
	<cfparam name = "application.commWarningsSent[commid][warning]" default=#structNew()#>


	<cfset application.commWarningsSent[commid][warning].dateSent = now()>

	<cfreturn>

</cffunction>


<!---

	The following function sends the set of

--->
<cffunction name="sendCommunication" >
	<cfargument name="commID">
	<CF_ABORT>




</cffunction>


<!---
WAB 2013-12-03 2013RoadMap2 Item 25 Contact History.  Get Subject from new CommDetailContent table rather than in the feedback column
--->
<cffunction name="getCommsSentForPerson" access="public" hint="Returns the communications sent for a person" returntype="query">
	<cfargument name="personID" type="numeric" required="yes">
	<cfargument name="commTypeIDs" type="string" required="no">								<!--- 2011/08/31 PPB REL109 My Mailbox on Portal --->
	<cfargument name="sortOrder" type="string" required="no" default="datesent desc">		<!--- 2011/08/31 PPB REL109 My Mailbox on Portal --->

	<cfscript>
		var CommsSent = "";
	</cfscript>

	<CFQUERY name="CommsSent" dataSource="#application.SiteDataSource#">
		SELECT
			case when cdc.commdetailid is not null then case when isnull(cdc.subject,'') <> '' then cdc.subject else left(cdc.body,100) end  else c.Title end as title,
				c.commid,
				cd.dateSent as sendDate,
				CDT.Name as FormText,
				cd.commstatusid as status,
				case when cd.commTypeID = 11 then '' else LL.ItemText end as TypeText,
				s.name as statustext,
				cd.commStatusID as feedback,
				cd.fax as addressused

		FROM CommDetail cd
			INNER JOIN Communication c on  cd.CommID = c.CommID
			LEFT JOIN CommDetailContent cdc on cd.commdetailid = cdc.commdetailid
			INNER JOIN LookupList LL on  c.CommTypeLID = LL.LookupID
			INNER JOIN commdetailType cdt on cd.CommTypeID = CDT.commtypeid
			INNER JOIN commDetailStatus s on cd.commStatusID=s.commStatusID
		WHERE cd.PersonID=#arguments.personID#
			AND cd.dateSent is not null
			AND cd.test <> 1
			AND c.CommFormLID <>4
			AND c.CommTypeLID <>50
			AND c.title <> 'AdHocComms' <!--- 2013-11-27 AXA CASE 438178 add condition to CommsSent query --->
			<cfif structKeyExists(arguments,"commTypeIDs")>
				AND cd.commTypeID  IN ( <cf_queryparam value="#arguments.commTypeIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		ORDER BY <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>

	<cfreturn CommsSent>
</cffunction>



<!---
	Formats the result structure returned by tskcommsend.cfm
	uses formatTskCommSendIndividualResultStructure to do the formatting
--->

<cffunction name="formatTskCommSendResultStructure" >
	<cfargument name="ResultStructure">
	<cfargument name="funcVersion" default="1">
	<cfargument name="emailsAttempted" default="">

	<cfset var result = {}>
	<cfset var thisComm = "">
	<cfset var thisCommResult = "">
	<cfset var key = "">


	<cfif not ResultStructure.isOK>
		<cfset result.LongTableHTML = ResultStructure.ErrorMessage & "<BR>">
		<cfset result.shortTableHTML = ResultStructure.ErrorMessage & "<BR>">
	</cfif>

	<cfloop item="thisComm" collection="#ResultStructure.comms#">
		<cfset thisCommResult = formatTskCommSendIndividualResultStructure(ResultStructure.comms[thisComm],funcVersion,arguments.emailsAttempted)>
		<cfloop item="key" collection="#thisCommResult#">
			<cfif not structkeyexists(result,key)>
				<cfset result[key] =  thisCommResult[key]>
			<cfelse>
				<cfif isNumeric(result[key])>
					<cfset result[key] = result[key] + thisCommResult[key]>
			<cfelse>
					<cfset result[key] = result[key] & thisCommResult[key]>
				</cfif>

			</cfif>
		</cfloop>
	</cfloop>
	<cfreturn  result>
</cffunction>




<!---
WAB 2009/06 LID 2361

	This query taken from templates\qryGetCommToSend.cfm
	Returns query of communications to be sent
	Used in tskcommsend and elsewhere

Can be be filtered to get those which are due to go out now
or can include those communications which are due to go out in the future
 --->
<cffunction name="getCommunicationsScheduledForNow">

	<cfreturn getCommToSendQuery (sendDate=now(),frmCommID="",overrideSendDate=false)>

</cffunction>


<cffunction name="getCommunicationsScheduledForNowAndFuture">

	<cfreturn getCommToSendQuery (frmCommID="",overrideSendDate=true)>

</cffunction>


<cffunction name="getCommToSendQuery">

<!--- this query returns the communications that will be processed
	 The order should be :
	 Smallest communications first (to ensure they get out quickly) - no longer really relevant - we are sending throughout the day, not at a single time in the evening
	 Fax before email if communication is in more than one form.
---->

<!--- Amendment History
	WAB 2007-06-27  changed join to person to be on createdby, not lastupdated by
	WAB 2005-04-27  removed the bit where enumerate all used to get round ODBC error, since SQL has moved on abit since 1999
	WAB 2001-03-23 Added overrideSendDate so that test communications can be sent out
	WAb 2000-02-07  added check for frmCommID being both defined and not blank - turned out that some other template was using frmCOmmID and this was causing problems
	WAb 2000-12-22 Changed name of variable for bringing back a singleCommunication (from justsendthese to frmCOmmID)
	WAB 1999-02-16	Added	 c.commtypelid to query
	LW	1999-04-09	Added the possibility of "enumerateall"
				lastUpdated, createdBy field
	KT 1999-11-01	query returns FlagID - this is needed later
				if the communication is about an event.

NB enumerateall as specifies which fields to select/group
	which is necessary to avoid the following ODBC error:
	"The current query would require an index on a
	work table to be built with 15 keys.
	The maximum allowable number of keys is 16.	"
	while ensuring that the query used
	to display AND send communications is the same
 --->



	<!--- CreateODBCDate(Now()) this is midnightof the day --->
	<CFargument  NAME="sendDate" DEFAULT="#Now()#" type="date">
	<CFargument  name = "overridesenddate" default = "false">
	<CFargument  name = "CommID" default = "">

	<cfset var getCommInfo = "">


	<!--- All communications that go out based on date--->

<!--- WAB pulled out these statements because used in both the select and group and difficult to keep in synch --->
<cfset var caseStatementForWholeCommOverridingSelection = "cs.selectionid is not null and (c.SendDate is  null or cs.senddate < c.senddate) ">
<cfset var titleAndSelectionStatement = "c.Title + case when #caseStatementForWholeCommOverridingSelection# then ' (Selection ' + convert(varchar,cs.selectionid) +')' else '' end ">
<cfset var sendDateStatement = "case when #caseStatementForWholeCommOverridingSelection# then cs.senddate else c.senddate end ">
<cfset var selectionIDStatement = "case when #caseStatementForWholeCommOverridingSelection# then cs.selectionid else null end">
<cfset var selectionNameStatement = "case when #caseStatementForWholeCommOverridingSelection# then s.title else null end">

	<CFQUERY NAME="getCommInfo" DATASOURCE=#application.siteDataSource#>
	SELECT c.CommID,
	  c.FlagID,  <!--- added KT 1999-11-01 --->
	  c.Title,
	  #preserveSingleQuotes(titleAndSelectionStatement)# as titleAndSelection,
	 #preserveSingleQuotes(selectionIDStatement)# as selectionid,
	  #preserveSingleQuotes(selectionNameStatement)# as Selection,
	  c.Description,
	#preserveSingleQuotes(sendDateStatement)# as sendDate,
	  c.CommFormLID AS MainCommFormLID,
	  c.commtypelid,		<!--- added WAB 1999-02-16 --->
		c.locked,
		c.sent,
	c.trackemail,
	c.lastKnownIsApproved,
	  c.lastUpdated,
	isNull(c.FromAddress,'#application.emailFrom#@#application.mailfromdomain#') as fromaddress,
	  isNull(cdm.displayName,'#application.EmailFromDisplayName#') as fromdisplayName,
	  c.commFromDisplayNameID,
  	  cf.Directory,
	  cf.FileName,
  	  c.lastUpdatedBy,
  	  c.contentLastUpdated,
	  c.CreatedBy,
	  c.linkToSiteURL,
	  cf.CommFileID,
	  sum(cfb.FileSize) AS FileSize,
		<!--- WAB 2000-11-13 added some columns to work out how large an email communication will end up being
			If it is too big then it cannot be sent during office hours --->
		(select ISNULL(sum(filesize),0) from commfile where commfile.commid = c.commid and commfile.commformlid = 2) as emailfilesize,
		(SELECT COUNT(1)  from commdetail where commdetail.commid = c.commid ) as numberofPeople,
	  cf.CommFormLID,
	  p.personid,
	  p.Email,
	  p.FirstName,
	  p.LastName,
	  c.replyto,
	  c.CCto,
	  convert(integer,c.requestReceipt) as requestReceipt,
	  (select CommConfEmail from country as c,location as l where c.countryid = l.countryid and l.locationid = p.locationid) as supportEmail

	  FROM Communication AS c
	  		left outer join
	  	commFromDisplayName AS cdm on c.commFromDisplayNameID = cdm.commFromDisplayNameID
	  		inner join
		CommFile AS cf on c.CommID = cf.CommID and cf.CommFileID = cf.IsParent
	  		inner join
		CommFile AS cfb	on cfb.CommID = c.CommID
			inner join
	  UserGroup AS ug on c.CREATEDBy = ug.UserGroupID    <!--- wab 2007-06-27 changed to createdby from lastupdated by - makes more sense to send things to the creator --->
  			inner join
	  Person AS p on p.PersonID = ug.PersonID
	<!--- WAB  adding scheduling of comms on a per selection basis --->
	 	left join
			(commselection cs
				inner join
			selection s on cs.selectionid = s.selectionid )
			on 		cs.commid = c.commid

				<cfif commid is "">
				and ((cs.sent in (2,3) <!--- 2=queued,3=partially sent --->	and   cs.senddate is not null <CFIF not overrideSendDate> and cs.senddate <#senddate#</CFIF>)
					or
					(c.sendDate is not null and c.sent = 0 and cs.sent <> 1 <CFIF not overrideSendDate>and c.senddate < #senddate#</cfif>)
					)
				-- and (c.SendDate is null or cs.senddate < c.senddate)
				<!--- WAB 2009/06/09 added extra test so selection only joins if it is scheduled to be sent before the main comm --->
				</cfif>


	 WHERE
	 1 = 1

	 AND (

	 		(c.SendDate IS NOT null and c.sent = 0 <CFIF not overrideSendDate >and c.SendDate < #senddate# </CFIF> )
			<cfif commid is "">
	 			 or
			(cs.senddate is not null and cs.sent in (2,3) <CFIF not overrideSendDate >and cs.senddate <#senddate# </cfif> )

			</cfif>
		)

		 AND c.TestFlag = 0 <!--- false --->

<!--- 	 AND ((c.CommFormLID IN (2,3) AND c.CommFormLID = cf.CommFormLID)
	 		OR (c.CommFormLID = 0)) <!--- 2=email, 3=fax, 0= look to CommFile---> --->

	 AND c.CommFormLID IN (2,3,5,6,7) <!--- exclude downloads (4) and 0 --->

	 <CFIF CommID is not "">
	 AND c.CommID  in ( <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 </CFIF>

	GROUP BY c.CommID,
	  c.FlagID,
	  c.Title,
	  c.Description,
	  c.SendDate,
	  c.CommFormLID,
	  c.commtypelid,
	  c.lastUpdated,
	  c.locked,
	  c.sent,
      c.trackemail,
	  c.lastKnownIsApproved,
	  cf.Directory,
	  cf.FileName,
	  c.lastUpdatedBy,
  	  c.contentLastUpdated,
	  c.CreatedBy,
  	  c.linkToSiteURL,
	  cf.CommFileID,
  	  cf.CommFormLID,
	  p.locationid,
	  p.personid,
	  p.Email,
	  p.FirstName,
	  p.LastName,
	  c.replyto,
	  c.FromAddress,
	  c.commFromDisplayNameID,
	  cdm.displayName,
	  c.ccto,
 	  convert(integer,c.requestReceipt)
	,#preserveSingleQuotes(titleAndSelectionStatement)#
	,#preserveSingleQuotes(sendDateStatement)#
 	,#preserveSingleQuotes(selectionIDStatement)#
	,#preserveSingleQuotes(selectionNameStatement)#
	<!--- for viewing ALL pending comms --->	<!--- as to be processed by tskcommsend --->
	ORDER BY
			#preserveSingleQuotes(sendDateStatement)# ,

	sum(cfb.FileSize) ASC, c.CommID, cf.CommFormLID DESC

	</CFQUERY>

	<cfreturn getCommInfo>
</cffunction>






	<!--- START: 10-NOV-2010		AJC		P-LEN022 - CR-LEN011 Created function to remove temporary files --->
	<cffunction name="deleteTemporaryFiles" returntype="boolean">
		<cfargument name="communicationStructure" type="Struct" required="true">

		<cfset var resultOK="true">

		<cftry>

			<cfif structKeyExists (communicationStructure.content.email, "htmlincludefilenameAbsolute") and fileExists (communicationStructure.content.email.htmlincludefilenameAbsolute)>
				<cffile action="delete" file="#communicationStructure.content.email.htmlincludefilenameAbsolute#">
			</cfif>

			<cfif structKeyExists (communicationStructure.content.email, "textincludefilenameAbsolute") and  fileExists (communicationStructure.content.email.textincludefilenameAbsolute)>
				<cffile action="delete" file="#communicationStructure.content.email.textincludefilenameAbsolute#">
			</cfif>

			<cfcatch><cfset resultOK="false"></cfcatch>

		</cftry>

		<cfreturn resultOK>

	</cffunction>


	<cffunction name="runBounceBackProcess">
		<cfargument name="maxRows" default = "50">
		<cfargument name="debug" default = "false">

		<cfset var result = {isOK = true,itemsProcessedArray = arrayNew(1),message="",moreToProcess = false,numberProcessed= 0}>
		<cfset var timeout = 30 + (maxRows * 3)>
		<cfset var cfPopParams = {}>
		<cfset var POPMessages = "">
		<cfset var parsedDate = "">
		<cfset var tempPOPMessages = "">
		<cfset var bouncebackSettings = applicationScope.com.settings.getSetting("communications.bouncebacks")>
		<cfset var body = '' />
		<cfset var messageResult = '' />
		<cfset var bouncebacklog = '' />
		<cfset var mostRecentBouncebackFlag = 'mostRecentBounceback' />
		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

		<cfset timeout = max(timeout,500)>
		<cfsetting requesttimeout="#timeout#">


		<cfset cfpopParams = {
				username="user_#applicationScope.instance.databaseid#.#applicationScope.instance.databaseid#",
				password="#applicationScope.instance.databaseid#",
				server=bouncebackSettings.server,
				timeout = timeout,
				maxrows = maxrows
					}>

		<!--- usually the username and password are just the default settings above, but could be overridden in settings if necesary --->
		<cfif bouncebackSettings.username is not "">
			<cfset cfpopParams.username  = bouncebackSettings.username>
		</cfif>
		<cfif bouncebackSettings.password is not "">
			<cfset cfpopParams.password  = bouncebackSettings.password>
		</cfif>


				<cftry>
				<cfpop action="GETALL"
		    	   name="POPMessages"
			    	attributeCollection = #cfpopParams#
			    	>

				   <cfcatch>


						<cfif cfcatch.message contains "An exception occurred when setting up mail server parameters">
							The POP server is not running or cannot be accessed
								<cfif debug><cfdump var="#cfpopparams#"><CF_ABORT></cfif>
							<cfrethrow>
						<cfelseif cfcatch.message contains "Logon failure: unknown user name or bad password">
							Your log on details are incorrect.
							<!--- WAB 2014-10-30 CASE 442416
								Logon failure means that the bounceback server has not been initialised, so send an initialisation email
							 --->
							<cfset initialiseBouncebackServer()>
							<cfset result.isOK = false>
							<cfset result.message = "Incorrect Login Details. Will attempt to initialise bounceback server">
							<cfreturn result>
						</cfif>
						<!--- This is rather a nasty section which deals with occasional problems we have with the POP tag and problem emails--->

						<!--- if there is a problem with a message, we need to step back until we get the actual row --->
						<cfif arguments.maxrows is not 1>
							<cfset arguments.maxrows = int(arguments.maxrows/2)>
							<cfoutput>Problem reading a message trying #maxrows# rows<BR></cfoutput>
							<cfset runBounceBackProcess (maxrows)>


						<cfelse><!--- ie one row returned --->

							<!--- OK, now we have found the offending item, we are going to process it without getting its body. --->

							<cftry>
								<cfpop action="GETHEADERONLY"
							       name="tempPOPMessages"
							       attributeCollection = #cfpopParams#
							       >

									<cfcatch>
										<cfif cfcatch.detail contains "StringIndexOutOfBoundsExceptio">

											<cfpop action="DELETE"
										       messagenumber="1"
									         attributeCollection = #cfpopParams#>

										<cfelse>
											<cfrethrow>
										</cfif>

									</cfcatch>
							</cftry>


								<!--- add some columns to the query to make it look as if we have retrieved the body , when we haven't
								will then be processed as normal and be deleted.
								the bounceback process should then continue happily
								 --->

								<CFQUERY dbtype="query" name = "POPMessages">
								select *, '' as htmlbody, '' as textbody from tempPOPMessages
								</CFQUERY>


						</cfif>
				   </cfcatch>
			</cftry>

		<!--- We now hopefully have a query called POPMessages --->


		<CFOUTPUT>Processing #POPMessages.recordCount# messages <BR></CFOUTPUT>

			<CFLOOP QUERY="POPMessages">

				<!--- This function parses the text and updated commdetail and person if necessary --->
				<cfif HTMLBody is not "">
					<cfset body= htmlbody>
				<cfelse>
					<cfset body= textbody>
				</cfif>
				<cfset messageResult = parseBounceBack (to = to,subject=subject,body=body,header= header)>
				<cfset arrayAppend(result.itemsProcessedArray,messageResult)>

				<cfif messageResult.detailsRecognised>
					<cfoutput>
					Person = #messageResult.details.personid#<BR>
					Communication = #messageResult.details.commid# <BR>
					</cfoutput>
				</cfif>


				<!--- if the details were recognised (commid/personid) but there were no recognised phrases then we save the details to the db --->
				<CFIF messageResult.detailsRecognised  and not messageResult.phraseFound >
					<!--- Returned email does not match any key phrases
						Pop in a table for later processing (TBD)
						WAB 2011/11/01 fixed an error by using parseDateTime() rather than createODBCDate() which did not deal with dates with timezones attached
						WAB 2011/11/28 having problems with my DE() - replaced with longhand.  Also trimmed emailaddresses
					--->
					<cfset body = HTMLBody>
					<cfif body is "">
						<cfset body = textBody>
					</cfif>

					<!--- 2012-09-10 WAB 430574, falls over if date is null, which seems to happen very occasionally --->
					<cftry>
						<cfset parsedDate = parseDateTime(date,"pop")>
						<cfcatch>
							<cfset parsedDate = "null">
						</cfcatch>
					</cftry>

					<CFQUERY DATASOURCE=#messageResult.datasourcename# NAME="bouncebacklog">
						Insert into commUnParsedBounceBacks
						(commid, personid, subject,body,fromAddress,toAddress,received)
						values (<cf_queryparam value="#messageResult.details.commid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#messageResult.details.personid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#Subject#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#body#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#left(from,250)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#left(to,250)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#parsedDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
					</cfquery>
					<cfoutput>No Phrase Found.  Logged</cfoutput>

				</CFIF>

				<cfif messageResult.detailsRecognised>
					<cfif application.com.flag.doesFlagExist(mostRecentBouncebackFlag)>
						<cfset application.com.flag.setFlagData(entityid = messageResult.details.personid, flagid = mostRecentBouncebackFlag,data = left("#Subject#<BR> #body#",4000))>
					</cfif>
				</CFIF>

				<cfpop action="DELETE"
				       uid = #uid#
			        attributeCollection = #cfpopParams#>

				<cfoutput>#messageResult.message#<BR><BR></cfoutput>


			</CFLOOP>

			<cfset result.itemsProcessed=POPMessages.recordCount>

		<cfif POPMessages.recordCount is maxRows>
			<!--- This means that there are probably more bouncebacks to process --->
			<cfset result.moreToProcess=true>
		</cfif>

		<cfreturn result>

	</cffunction>





	<!---
		Parse Email Bounceback
		WAB 2011/02 Moved from custom tags
		Given a to/subject/body/header it
		i) looks for our encoding of personid and commid
		ii) looks for recognised phrases in the body of the email
		iii) updates commdetail record accordingly

		2014-09-29 WAB	(But didn't get into 2014_2/2015_1 until 2015-03-11
						During CASE 439422, Change way matches are found in the emailPhrases table

	 --->

	<cffunction name="parseBounceBack">
		<cfargument name="to">
		<cfargument name="subject">
		<cfargument name="body">
		<cfargument name="header">

		<cfset var result = {detailsRecognised = false,message="",phraseFound = false, action=""}>

		<!--- Extract Person ID and CommID from address --->
		<!--- define a re to extract serverid-commid-personid-commdetailid   (commdetailid is optional) --->
		<cfset var regularExpression = "BB([0-9]+)-([0-9]+)-([0-9]+)(?:-([0-9]+))?@">
		<cfset var groups = {1="databaseid",2="commid",3="personid",4="commdetailid"}>
		<cfset var refindArray = "">
		<cfset var newEmailStatus_SQL = '' />
		<cfset var entityID = '' />
		<cfset var value = '' />
		<cfset var forwardResult = '' />
		<cfset var itemToSearch = '' />
		<cfset var getComm = '' />
		<cfset var findPhraseMatch = '' />
		<cfset var getUnsubscribeFlag = '' />
		<cfset var getFlag = '' />
		<cfset var getPerson = '' />
		<cfset var updateFeedback = '' />
		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

			<!--- loop through to,header, body until we find a match --->
			<cfloop list="to,header,body" index="itemToSearch">
				<CFSET reFindArray = applicationScope.com.regexp.refindSingleOccurrence(regularexpression,arguments[itemToSearch],groups)>
				<CFIF arrayLen(reFindArray) is not 0>
					<cfbreak>
				</CFIF>
			</cfloop>


			<CFIF arrayLen(reFindArray) is 0>
				<!--- TBD
				This is likely to be an out of office being returned to the From Address
				We do not encode the numbers into the from address because Clients to not like to see numbers in the from Address
				However we could look at the FROM email address and mark that email address as good
				we could also look at the subject line which usually includes the subject of the original communication
				 --->
				<cfset result.message = "No Person or CommID found">

			<cfelse>
				<cfset result.details = refindArray[1]>
				<cfset result.detailsRecognised = true>

				<!--- This is still coded so that it is possible to run bounceback processing for more than one datasource on the same scheduled process
					In reality this should no longer be necessary, since bouncebacks for different dbs now come into their own mail box
				 --->
				<cfset result.datasourceName = server.serverInitialisationObj.getDataSourceNameForDatabaseID (result.details.databaseid)>

				<cfif result.datasourceName is "">
					<CFSET result.message = "Database #result.details.databaseid#.  Not on this server" >
					<cfset result.detailsRecognised = false>

				<cfelse>

					<!--- Check that the commid exists --->
						<CFQUERY DATASOURCE=#result.datasourcename# NAME="getComm">
						Select c.commid, cd.personid, cd.commdetailid
						from Communication c left join commdetail cd on c.commid = cd.commid and cd.personid =  <cf_queryparam value="#result.details.personid#" CFSQLTYPE="CF_SQL_INTEGER" >  <cfif result.details.commdetailid is not ""> and cd.commDetailid =  <cf_queryparam value="#result.details.commDetailid#" CFSQLTYPE="CF_SQL_INTEGER" >  </cfif>
						where c.commid =  <cf_queryparam value="#result.details.commid#" CFSQLTYPE="CF_SQL_INTEGER" >
						</CFQUERY>

						<cfif getComm.recordCount is 0 >
							<CFSET result.message = "Could not find communication with CommID = #result.details.commid#" >
							<CFset result.detailsRecognised = FALSE>
						<cfelseif getComm.commdetailid is "" >
							<CFSET result.message = "Could not find person with PersonID = #result.details.personid# not found in Communication #result.details.commid#." >
							<CFset result.detailsRecognised = FALSE>
						<cfelse>
							<CFset result.detailsRecognised = true>
						</cfif>

				</cfif>

			</CFIF>


				<!--- *********************************************

				  			Identify Key Phrases in Content

			      ********************************************* --->
			<CFIF result.detailsRecognised >

						<!--- 	WAB 2014-09-29 changed to point to main copy of emailPhrases, not relayServer copy
								At same time change way this works, rather than looping through the phrases manually testing each against the email, run a single query against the table
								Got rid of the regExp column which was not being used; % can now be used as a wild card in the phrase


						 --->
						<cfquery name="findPhraseMatch" datasource="#applicationScope.sitedatasource#" timeout="60">
						declare
							@subject nvarchar (max) = <cf_queryparam value = "#arguments.subject#" CFSQLTYPE="CF_SQL_VARCHAR" >,
							@body nvarchar (max) = <cf_queryparam value = "#arguments.body#" CFSQLTYPE="CF_SQL_VARCHAR" >

							select top 1 * from emailPhrases
							where
								active = 1
								AND
									(
										(@subject like '%' + phrase + '%')
											OR
										(subjectLineOnly = 0 and @body like '%' + phrase + '%')
									)
						order by sortorder
						</cfquery>

							<CFIF findPhraseMatch.recordCount is not 0>

								<cfset result.phraseFound = true>
								<cfset result.phraseRecord = applicationScope.com.structureFunctions.queryRowToStruct(findPhraseMatch,1)>

						<!--- *********************************************

				  			Act According to Key Phrases Discovery

					      ********************************************* --->

								<!--- If action contains updatecommstatus then update commdetail--->
								<CFIF findnocase("updatecommstatus",findPhraseMatch.action) is not 0>

									<CFQUERY DATASOURCE=#result.datasourcename# result="updateFeedback">
									UPDATE CommDetail
									SET CommDetail.Feedback =  <cf_queryparam value="#findPhraseMatch.feedback#. #findPhraseMatch.Phrase#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
										CommDetail.commstatusid =  <cf_queryparam value="#findPhraseMatch.commstatus#" CFSQLTYPE="CF_SQL_INTEGER" >
									WHERE CommDetail.CommID =  <cf_queryparam value="#result.details.commid#" CFSQLTYPE="CF_SQL_INTEGER" >
									AND CommDetail.PersonID =  <cf_queryparam value="#result.details.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
									<cfif result.details.commdetailid is not "">
									and commDetailid =  <cf_queryparam value="#result.details.commDetailid#" CFSQLTYPE="CF_SQL_INTEGER" >
									</cfif>
									AND commtypeid=2
									</CFQUERY>

									<cfif updateFeedback.recordCount>
										<CFSET result.message = listappend(result.message,"Updated commstatus: #findPhraseMatch.commstatus#")>
									</cfif>

								</CFIF>


								<!--- if action contains  incrementemailstatus or setemailstatusok
								then update the person emailstatus field--->
								<CFIF findnocase("incrementemailstatus",findPhraseMatch.action) is not 0 or findnocase("setemailstatusok",findPhraseMatch.action) is not 0 >
									<CFIF findnocase("incrementemailstatus",findPhraseMatch.action) is not 0>
										<cfset newEmailStatus_SQL = 'isnull(EmailStatus,0) + 1'>
									<cfelseIF findnocase("setemailstatusok",findPhraseMatch.action) is not 0 >
										<cfset newEmailStatus_SQL = -0.5>
									</cfif>


									<!--- Update Person Email Status for this message --->
									<CFQUERY DATASOURCE="#result.datasourcename#" >
									UPDATE Person
										SET EmailStatus =  #newEmailStatus_SQL#
									WHERE PersonID =  <cf_queryparam value="#result.details.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
										and emailStatus <>  #newEmailStatus_SQL#
									</CFQUERY>

									<CFSET result.message = listappend(result.message,"Updated email status")>


								</cfif>

								<!--- WAB 2011/02
									I do not think that we do any of this unsubscribing and flagging via emails
									But I have left the code in anyway.
									Would need checking before reuse
								--->
								<!--- unsubscribe from this type of communication --->
								<CFIF findnocase("unsubscribeThisType",findPhraseMatch.action) is not 0  >
									<!--- find out the flag for this type of communication --->
									<CFQUERY DATASOURCE=#result.datasourcename# NAME="getUnsubscribeFlag">
									Select * from flag
									where flagTextID =  <cf_queryparam value="NoCommsType#getComm.CommTypeLID#" CFSQLTYPE="CF_SQL_VARCHAR" >
									</CFQUERY>

									<CFIF getUnsubscribeFlag.RecordCount is 0>
										<cfset applicationScope.com.errorHandler.recordRelayError_Warning (Type="BounceBack Processing Error", message="BounceBack Processing Error, No Unsubscribe Flag for CommType #getComm.CommTypeLID#", TTL=30)>

									<CFELSE>
										<CF_setFlag flagid = 	"#getUnsubscribeFlag.flagid#"
													entityid=	"#thisemail.personid#"
													value=		"1"
													SiteDataSource= "#result.datasourcename#"
													entityType="person"
													userid = "0">

										<CFSET result.message = listappend(result.message,"flag NoCommsType#getComm.CommTypeLID# (#getUnsubscribeFlag.flagid#)")>
									</cfif>
								</CFIF>

								<CFIF findnocase("setFlag",findPhraseMatch.action) is not 0 or findnocase("unsetFlag",findPhraseMatch.action) is not 0 >
								<!--- set a Flag--->
									<!--- find out what type of flag it is --->
									<CFQUERY DATASOURCE=#result.datasourcename# NAME="getFlag">
									Select fet.tablename, flagid
									from flag as f, flaggroup as fg, flagentityType as fet
									where f.flaggroupid = fg.flaggroupid
									and fg.EntityTypeID = fet.EntityTypeID
									and f.flagTextID =  <cf_queryparam value="#flagtextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
									</CFQUERY>


									<CFIF getFlag.RecordCount is 0 or listFindNoCase("Person,Location",getFlag.deleteName) is 0>
										<cfset applicationScope.com.errorHandler.recordRelayError_Warning (Type="BounceBack Processing Error", message="BounceBack Processing Error, No Flag #flagtextID#", TTL=30)>

									<CFELSE>

										<CFIF getFlag.tableName is "location">
											<CFQUERY DATASOURCE=#result.datasourcename# NAME="getPerson">
											Select * from Person where personid =  <cf_queryparam value="#thisemail.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
											</CFQUERY>
											<CFSET entityID = getPerson.locationid>
										<CFELSEIF getFlag.tableName is "Person">
											<CFSET entityID = thisemail.personid>
										<CFELSE>
											<CFSET entityID = 0>
											<CFOUTPUT> error wrong type of flag	 </CFOUTPUT>

										</cfif>

										<CFIF findnocase("setFlag",findPhraseMatch.action) is not 0>
											<CFSET value = 1>
											<CFSET forwardResult = forwardResult & "A flag has been set #lb#">
										<CFELSEIF findnocase("unSetFlag",findPhraseMatch.action) is not 0>
											<CFSET value = 0>
											<CFSET forwardResult = forwardResult & "An flag has been unSet #lb#">
										</cfif>


										<CF_setFlag flagid = 	"#getFlag.flagid#"
													entityid=	"#entityID#"
													value=		"#value#"
													SiteDataSource= "#result.datasourcename#"
													entityType="#getFlag.tableName#"
													userid = "0">
											<CFSET result.message = listappend(result.message,"flag #findPhraseMatch.flagtextid# (#getFlag.flagid#) set to #value#")>
									</cfif>


								</CFIF>

							</CFIF>




		<CFELSE><!--- details not recognised --->

		</CFIF>




		<cfreturn result>


	</cffunction>

	<!--- WAB 2014-10-30 CASE 442416
		Long overdue function.
		Sends a dummy email to the bounceback server, to initialise it  - ie create the account for this DB so that the logon does not fail
	--->
	<cffunction name="initialiseBouncebackServer">
		<cfset var mailFailToDomain = application.com.settings.getSetting("communications.mailFailToDomain")>
		<cfset var dummyEmailAddress = "test-BB#application.instance.databaseid#-0-0@#mailFailToDomain#">

		<cfmail from="serverSupport@relayware.com" to="#dummyEmailAddress#" Subject="Initialisation Email">
		Initialisation
		</cfmail>

		<cfreturn dummyEmailAddress>
	</cffunction>


	<cffunction name="initialise">
		<cfargument name="applicationScope" default="#application#">
		<!--- DO NOT VAR THESE VARIABLES --->
		<cfset variables.applicationScope = arguments.applicationScope>

	</cffunction>


	<!--- NYB 2011/06/21 P-LEN024 - added function: --->
	<cffunction name="getSiteValidValues" type="public" returntype="query" validvalues="true">
		<cfset var siteQry = "">

		<cfquery name="siteQry" datasource = "#application.sitedatasource#">
			select domain as displayValue, domain as dataValue,
			<!--- this line is mainly of importance on dev sites where we have lots of domains, if there is a specific reciprocal then it should appear at top of list --->
			case when domain =  <cf_queryparam value="#application.com.relayCurrentSite.getReciprocalExternalDomain()#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 1 else 0 end as currentreciprocaldomain
			 from
			sitedef sd inner join
			sitedefdomain sdd on sd.sitedefid = sdd.sitedefid
			where isinternal = 0
			and active = 1
			and testsite =  <cf_queryparam value="#application.testsite#" CFSQLTYPE="CF_SQL_INTEGER" >
			order by currentreciprocaldomain desc, maindomain desc, sd.sitedefid asc, domain asc
		</cfquery>

		<cfreturn siteQry>
	</cffunction>

	<!--- NAS 2011-08-02 P-LEN024 - added function: --->
	<cffunction name="createStartingTemplateCommLink" type="public" returntype="string" >
		<cfargument name="startingTemplate" default="">
		<cfargument name="queryString" default="">

		<cfset var StartingTemplateLink = "">
		<cfset var internalremoteStub = "">

		<cfset internalremoteStub =  "#application.com.relayCurrentSite.getReciprocalInternalProtocolAndDomain()#/index.cfm?" >
		<cfset StartingTemplateLink = '#internalremotestub##arguments.queryString#&a=i&st=#application.startingTemplateLookupStr['#startingTemplate#']#'>

		<cfreturn StartingTemplateLink>
	</cffunction>


	<!--- END: 10-NOV-2010		AJC		P-LEN022 - CR-LEN011 Created function to remove temporary files
 --->

	<!--- 2011/10/11 PPB NET005 new fn (moved from commheader) --->
	<cffunction name="getCommTypesLID" type="public" returntype="query" >
		<cfset var getCommTypes = "">

		<CFQUERY NAME="getCommTypes" DATASOURCE="#application.SiteDataSource#">
			SELECT IsDefault,
				  LookupID,
				  ItemText
			 FROM LookupList
			 WHERE FieldName = 'CommTypeLID'
			 AND ItemText <> 'Download' and LookupID not in (98,99,4)  <!--- 98, 99 are adhoc and system emails --->
			 AND IsParent = 0 <!--- false --->
			 AND IsLive = 1
			 ORDER BY ItemText
			</CFQUERY>

		<cfreturn getCommTypes>
	</cffunction>


	<cffunction name="getCommunicationLockName">
		<cfargument name="commid" required="true">
		<cfargument name="test" required="true">

		<cfset var result = "sendCommunication-#commid#">

		<cfif test>  <!--- can send as many tests as you want simultaneously --->
			<cfset result = result & "-Test-" & createUUID()>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getCommunicationLockLikeString">
		<cfargument name="commid" required="true">

		<cfset var result = "sendCommunication-#commid#%">

		<cfreturn result>

	</cffunction>


	<cffunction name="getCommunicationsBeingSent">

		<cfset var lockName = getCommunicationLockLikeString(commid = '%')>
		<cfset var result = application.com.globalFunctions.getMatchingProcessLocks(lockname)>

		<cfreturn result>

	</cffunction>


	<cffunction name="getCommunicationLockDetails">
		<cfargument name="commid" required="true">
		<cfargument name="commSendResultID" >

		<cfset var lockName = getCommunicationLockLikeString(commid = commid)>
		<cfset var result = application.com.globalFunctions.getMatchingProcessLocks(lockname)>
		<cfset var i = 0>

		<cfif structKeyExists(arguments,"commSendResultID")>
			<!--- need to look for the lock with that ID --->
			<cfloop index="i" from="#arrayLen(result)#" to="1" step="-1">
				<cfif result[i].metadata.commSendResultID is not arguments.commSendResultID>
					<cfset arrayDeleteAt (result,i)>
				</cfif>
			</cfloop>

		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="isCommunicationBeingSent">
		<cfargument name="commid" default="">

		<cfreturn getCommunicationLockDetails(commid).lockExists>

	</cffunction>

	<!--- WAB 2013-10 Added during CASE 437315 --->
	<cffunction name="cancelCommunicationSend">
		<cfargument name="commid" default="">

		<cfset var newMetaData = {cancel=true}>
		<cfset var result = {isOK = true}>
		<cfset var lockresult = "">

		<!--- set cancel metadata on lock --->
		<cfset application.com.globalFunctions.updateProcessLock(lockname= getCommunicationLockName(commid = commid, test = 0), metadata = newMetadata, ThirdPartyUpdate=true )>

		<!--- check that it worked--->
		<cfset lockResult = application.com.globalFunctions.getProcessLock(lockname= getCommunicationLockName(commid = commid, test = 0))>

		<cfif not lockResult.lockExists>
			<cfset result = {isOK = false, message = "Could not find communication"}>
		<cfelseif not structKeyExists (lockResult.metadata, "cancel")>
			<cfset result = {isOK = false, message = "Could not cancel, try again"}>
		</cfif>

		<cfreturn result >

	</cffunction>



	<cffunction name="getOmnitureTrackableURLs" output="false">
		<cfset var OmnitureTrackableUrls= "">

			<cfset OmnitureTrackableUrls= application.com.settings.getSetting('Communications.Tracking.OmnitureTrackableUrls')>

			<!--- clean up data --->
			<cfset OmnitureTrackableUrls = rereplace(OmnitureTrackableUrls,chr(10),"","all")>
			<cfset OmnitureTrackableUrls = replace(OmnitureTrackableUrls,chr(13),",","all")>
			<cfset OmnitureTrackableUrls = replace(OmnitureTrackableUrls," ",",","all")>
			<cfset OmnitureTrackableUrls = rereplace(OmnitureTrackableUrls, ",(?=,)" , "" , "all")> <!--- replaces multiple commas with a single one  --->

		<cfreturn OmnitureTrackableURLs>

	</cffunction>


	<cffunction name="getOmnitureTrackableURLsRegExp" output="false">

		<cfset var result = replace(getOmnitureTrackableURLs(),",","|","ALL")>

		<cfreturn result>

	</cffunction>



	<!--- WAB 2013-05-08 CASE 433974 Added these functions to standardise how emailStatus is described --->
	<cffunction name="emailStatusInWords">
		<cfargument name="emailStatus" required = "true">

		<cfset var result = "">

		<cfif emailStatus IS -0.5>
			<cfset result = "Confirmed Good">
		<cfelseif emailStatus IS 0>
			<cfset result = "Unconfirmed">
		<cfelseif emailStatus GT 0 >
			<cfset result = round(emailstatus) & " Bounceback(s)">
			<cfif emailStatus is not int(emailStatus)>
				<cfset result = result & ", previously Confirmed Good">
			</cfif>
		<cfelse>
			<cfset result = "Unknown">
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="emailStatusInWordsSQLSnippet">
		<cfargument name="personAlias" default = "">

		<cfset var emailStatus = "emailStatus">
		<cfset var result = "">

		<cfif personAlias is not "">
			<cfset emailStatus = personAlias & "." & emailStatus>
		</cfif>

		<cfset result = "CASE WHEN #emailStatus# = -0.5 THEN 'Confirmed Good' WHEN #emailStatus# = 0 THEN 'Unconfirmed' WHEN #emailStatus# >0 THEN convert(varchar,round(emailStatus,0)) + ' Bounceback(s)'  + CASE WHEN emailStatus <> round(emailstatus,0) THEN ', previously Confirmed Good' ELSE '' END ELSE 'Unknown' END ">

		<cfreturn result>

	</cffunction>

	<cffunction name="useMessage">

		<!--- variable used to determine whether to display messages or not.. currently based on whether the activity stream has been place in content. --->
		<cfset var result = application.com.relayTags.isRelayTagInElement(relayTagName="relay_activity_Stream")>

		<cfreturn result>
	</cffunction>

	<cffunction access="public" name="isUserAllowedToSendNow" returnType="boolean" output="false">
		<cfset var result = false>

		<cfset var checkPermission = application.com.login.checkInternalPermissions(securityTask = "commtask")>

		<cfif checkPermission.Level3 IS 1 or checkPermission.Level4 IS 1>
			<cfset result = true>
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction access="public" name="disableCommFromNameID" output="false">
		<cfargument name="CommFromNameID" default = "">

		<cfset var disableCommFromNameID = "">
		<cfset var result = {isOK="true",message=""}>

		<CFTRY>
			<CFQUERY NAME="disableCommFromNameID" DATASOURCE="#application.SiteDataSource#">
				update commFromDisplayName
				set active = 0
				where commFromDisplayNameID = <cf_queryparam value="#arguments.CommFromNameID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<CFCATCH type="any">
				<cfset result.isOK = false>
				<CFSET result.message = "#cfcatch.message#. ">
			</CFCATCH>
		</CFTRY>

		<cfreturn result>

	</cffunction>

	<!--- 2013-11-01 WAB CASE 437307 - Functions setting/reading metadata on communication (download) --->
	<cffunction name="getCommMetaData" output="false">
		<cfargument name="commid" required="true">

		<cfset var getMetaDataQRY = '' />
		<cfset var result = {} />

		<cfquery name="getMetaDataQRY" datasource="#application.siteDataSource#">
		select parameters
		from communication
		where commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_Integer" >
		</cfquery>

		<cfif getMetaDataQRY.parameters is not "">
			<cfif isJSON(getMetadataQRY.parameters)>
				<cfset result = deserializeJSON(getMetaDataQRY.parameters)>
			<cfelse>
				<cfset result = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = getMetaDataQRY.parameters, delimiter = ",")>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="setCommMetaData" output="false">
		<cfargument name="commid" required="true">
		<cfargument name="name" required="true">
		<cfargument name="value" required="true">

		<cfset var currentMetaData = getCommMetaData(commid)>

		<cfset currentMetaData[name] = value>

		<cfquery name="updateMetadata" datasource="#application.siteDataSource#">
		update communication
		set parameters =  <cf_queryparam value="#serializeJSON(currentMetaData)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		where
		commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_Integer" >
		</cfquery>

		<cfreturn currentMetaData>

	</cffunction>


	<cffunction name="deleteCommMetaData" output="false">
		<cfargument name="commid" required="true">
		<cfargument name="name" required="true">

		<cfset var updateMetadata = '' />
		<cfset var currentMetaData = getCommMetaData(commid)>

		<cfset structDelete(currentMetaData,name)>

		<cfquery name="updateMetadata" datasource="#application.siteDataSource#">
		update communication
		set parameters =  <cf_queryparam value="#serializeJSON(currentMetaData)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		where
		commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_Integer" >
		</cfquery>

		<cfreturn currentMetaData>

	</cffunction>




</cfcomponent>

