<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Change Log:

2014-09-24	 AHL 	Case 441699 Creating Element Element Link Image Thumbnails
2015-11-10  ACPK    PROD2015-24 Updated convertPdfToImage to save images using specified filename
2015-11-11  ACPK    PROD2015-24 Added missing default value/removed required attribute from filename parameter in convertPdfToImage()
2015-12-02  ACPK    PROD2015-468 Added new removeTopMargin argument to resizeImage(); if Yes, image is placed at top of thumbnail
2016-05-04 	WAB 	CASE 448873 Add support for a custom version of jPedal.PdfDecoder
2016-01-09	DAN		PROD2016-2470 - add support for TIFFs and BMPs when creating thumbnails and few fixes related

 --->

<cfcomponent displayname="Relay Image Manager" hint="Methods for managing images">

	<!--- WAB 2010/10/20 replace lots of createObjects with a single call --->
	<cffunction name="getImageComponent">
		
		<cfreturn createObject("component","com.image").setKey(application.com.settings.getSetting("imageComponentLicenceKey"))>	

	</cffunction>

	<cffunction name="getImageWidth" hint="returns image width">
		<cfargument name="imageFilePath" type="string" required="yes" hint="absolutePath">
				
		<cfscript>
			var thisImage = "";
		</cfscript>
						
		<!--- <cfscript>
			nImage = getImageComponent();
			nImage.readimage(imageFilePath);
			imageWidth = nImage.getWidth();
		</cfscript> --->
		<cfimage name="thisImage" action="read" source="#arguments.imageFilePath#">
		
		<cfreturn thisImage.width>
	</cffunction>
	
	<cffunction name="getImageHeight" hint="returns image height">
		<cfargument name="imageFilePath" type="string" required="yes" hint="absolutePath">
				
		<cfscript>
			var thisImage = "";
		</cfscript>
						
		<!--- <cfscript>
			nImage = getImageComponent();
			nImage.readimage(imageFilePath);
			imageHeight = nImage.getHeight();
		</cfscript> --->
		
		<cfimage name="thisImage" action="read" source="#arguments.imageFilePath#">
		
		<cfreturn thisImage.height>
	</cffunction>
	
	
	<cffunction name="resizeImageOneSide" hint="resizes the width or height based on which is greater.">
		<cfargument name="imageFilePath" type="string" required="yes" hint="absolutePath">
		<cfargument name="targetWidthOrHeight" type="numeric" required="yes" hint="pixels">

		<cfscript>
			nImage = getImageComponent();
			nImage.readimage(imageFilePath);
			if (nImage.getWidth() gte nImage.getHeight()) {
				nImage.scaleWidth(arguments.targetWidthOrHeight);
			} else {
				nImage.scaleHeight(arguments.targetWidthOrHeight);
			}
		</cfscript>
		
		<cfreturn "success">
	</cffunction>

	<!--- 2014-02-03	YMA	Case 436524 re-written to remove cropping problems.  We now resize the image (with ImageScaleToFit) and center it 
	over a transparent background. --->
	<cffunction name="resizeImage" hint="resizes an image with a crop if neccesary to target dimensions">
		<cfargument name="targetWidth" type="numeric" required="yes" hint="pixels">
		<cfargument name="targetHeight" type="numeric" required="yes" hint="pixels">
		<cfargument name="sourcePath" type="string" required="yes" hint="absolutePath">
		<cfargument name="sourceName" type="string" required="yes">
		<cfargument name="targetPath" type="string" required="no" hint="absolutePath - default to source">
		<cfargument name="targetName" type="string" required="no" hint="extension will dictate what the file is saved as - default to source">
		<cfargument name="removeTopMargin" type="boolean" required="no" default="no"> <!--- 2015-12-02  ACPK    PROD2015-468 Added new removeTopMargin argument; if Yes, image is placed at top of thumbnail --->
		
		<cfscript>
			var thisImage = "";
			var marginX = 0;
			var marginY = 0;
			var background = "";
		</cfscript>
			
		<cfif not structKeyExists(arguments,"targetPath")>
			<cfset arguments.targetPath = arguments.sourcePath>
		</cfif>
		<cfif not structKeyExists(arguments,"targetName")>
			<cfset arguments.targetName = arguments.sourceName>
		</cfif>
		
		<cfif not directoryExists(arguments.targetPath)>
			<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=arguments.targetPath)>
		</cfif>		
		<cfimage name="thisImage" action="read" source="#arguments.sourcePath#\#arguments.sourceName#"> <!--- 2014-09-24 AHL Case 441699 Creating Element Element Link Image Thumbnails --->
		<cfset ImageSetAntialiasing(thisImage)>
		<cfset ImageScaleToFit(thisImage,arguments.targetWidth,arguments.targetHeight)>
		
		<cfset background = ImageNew("",arguments.targetWidth,arguments.targetHeight,"argb")>
				
		<!--- 2015-12-02  ACPK    PROD2015-468 Added new removeTopMargin argument; if Yes, image is placed at top of thumbnail --->
		<cfif removeTopMargin>
			<cfset marginX = Ceiling((arguments.targetWidth-thisImage.width)/2)>
			<cfset marginY = 0>
		<cfelse>
			<cfset marginX = Ceiling((arguments.targetWidth-thisImage.width)/2)>
			<cfset marginY = Ceiling((arguments.targetHeight-thisImage.height)/2)>
		</cfif>
		
		<cfset ImagePaste(background,thisImage,marginX,marginY)>
		
		<cfimage source="#background#" action="write" destination="#arguments.targetPath#\#arguments.targetName#" overwrite="yes">
		
		<cfset thisImage = background>
		
		<cfreturn thisImage>
		
	</cffunction>
	
	
	<cffunction name="convertImage" hint="convert Image to other format">
		<cfargument name="sourcePath" type="string" required="yes" hint="absolutePath">
		<cfargument name="sourceName" type="string" required="yes">
		<cfargument name="targetPath" type="string" default="#sourcePath#" hint="absolutePath - default to source">
		<cfargument name="targetName" type="string" default="#sourceName#" hint="extension will dictate what the file is saved as - default to source">

		<cfset var thisImage = "">
		<cfimage action="convert" source="#arguments.sourcePath#\#arguments.sourceName#" destination="#arguments.targetPath#\#arguments.targetName#" overwrite="yes" name="thisImage">	
		<cfreturn thisImage>
	</cffunction>
	
	<cffunction name="createPlaceholderImage" hint="Creates an image to be used as a placeholder">
		<cfargument name="imagePath" type="string" required="Yes">
		<cfargument name="imageHeight" type="numeric" required="yes">
		<cfargument name="imageWidth" type="numeric" required="yes">
		<cfargument name="overwrite" type="boolean" required="no" default="false">
		<cfargument name="imageText" type="string" required="no" default="">
		
		<cfif fileexists(arguments.imagePath) and arguments.overwrite eq "false">
			<cfreturn "Image already exists">
		<cfelse>
			<cfset nImage = getImageComponent()>
			<cfscript>
				nImage.createImage(arguments.imageWidth,arguments.imageHeight);
				white = nImage.getColorByName("white");
				black = nImage.getColorByName("black");
				nImage.setBackgroundColor(white);
				nImage.setFill(white);
				nImage.setStroke(1,black);
				nImage.drawRectangle(0,0,arguments.imageWidth-1,arguments.imageHeight-1);
				nImage.setFill(black);
				timesNewRoman = nImage.loadSystemFont("Arial", 12,"Normal");
				nImage.drawSimpleString(arguments.imageText,3,15,timesNewRoman);
				nImage.writeImage(arguments.imagePath,"png");
			</cfscript>
			<cfreturn "Created Successfully">
		</cfif>
		
	</cffunction>

	
	<cffunction name="getRemoteImage" access="public" returntype="boolean" hint="Retrieves a remote image and saves it in a local directory">
		<cfargument name="remoteImagePath" type="string" required="Yes">
		<cfargument name="remoteFileName" type="string" required="yes">
		<cfargument name="localImagePath" type="string" required="Yes">
		<cfargument name="localFileName" type="string" required="yes">
		<cfargument name="overwrite" type="boolean" required="no" default="false">
		<cfargument name="imageType" type="string" required="no" default="standard">
		<cfargument name="scaleWidth" type="numeric" required="no" default=60 hint="pixels">
		<cfargument name="scaleHeight" type="numeric" required="no" default=60 hint="pixels">

		<!--- NJH 2009/07/06 P-SNY655 var'd some variables --->
		<cfscript>
			var nImage = "";
			var extension = "";
		</cfscript>
		
		<cfif fileexists("#arguments.localImagePath#\#arguments.localFileName#") and arguments.overwrite eq "false">
			<cfreturn false>
		<cfelse>
			<!--- NJH 2009/07/06 P-SNY655 fixed bug.. should be getting file if overwrite is true --->
			<cfif not fileexists("#localImagePath#\#arguments.localFileName#") or arguments.overwrite>	
				<cfhttp url="#arguments.remoteImagePath#/#arguments.remoteFileName#" method="get" timeout="3"></cfhttp>
				<cfif cfhttp.statusCode eq "200 OK">
					<cftry>
						<cfset nImage = getImageComponent()>
						<cfscript>
							nImage.readImageFromURL("#arguments.remoteImagePath#/#arguments.remoteFileName#");
							
							if (arguments.imageType eq "thumbnail") {
								if (nImage.getWidth() gt nImage.GetHeight()){
									nImage.scaleWidth(arguments.scaleWidth); //thumbsize);
								} else {
									nImage.scaleHeight(arguments.scaleHeight); //thumbsize);
								}
							}
							extension = listlast(arguments.localFileName,".");
							nImage.writeImage("#arguments.localImagePath#\#arguments.localFileName#",extension);
						</cfscript>
						
						<cfcatch>
						<!--- The most likely problem is an error while reading an image. This is probably due to the image being a progressive image --->
						</cfcatch>
						
					</cftry>
					<cfreturn true>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn false>
		
	</cffunction>
		
	
	<cffunction name="CreateThumbnail" hint="Creates a square thumbnail image" returnType="struct">
		<cfargument name="square" type="boolean" default="true" hint="If true a transparent background will be added.">
		<cfargument name="targetSize" type="numeric" default="80" hint="pixels">
		<cfargument name="maxSize_MB" type="numeric" default="#application.com.settings.getSetting('files.maxUploadSize_MB')#" hint="Maximum size of file in MB">
		<cfargument name="targetFilepath" type="string" required="yes" hint="in theory could be passed as absolute or relative. Puts everything in the content directory at the moment">
		<cfargument name="targetFileName" type="string" default="" hint="If blank, keep the original filename">
		<cfargument name="targetFileExtension" type="string" default="" hint="If blank, keep the original file extension">
		<cfargument name="sourceFile" type="string" required="true">
		
		<cfset var image = "">
		<cfset var sourceImage = "">
		<cfset var relativeDestination = "">
		<cfset var pos = 0>
		<cfset var result = {isOK=true,message="",imagePath="",imageURL=""}>
		<cfset var destination=arguments.targetFilepath>
		<cfset var cffile  = {isOk=false}>
		<cfset var absoluteDestPath = "">
		<cfset var fileDest = "">
		<cfset var fileExt = "">
		<cfset var fileNameAndExt = "">
		<cfset var fileExistsResult = structNew()>
		<cfset var filterFileExtension="*">
		<cfset var filterFilename="*">
		<cfset var fileExists = false>
		<cfset var thumbnailFiles = "">
		<cfset var imageWidth = "">
		<cfset var imageHeight = "">
		<cfset var targetWidth = "">
		<cfset var targetHeight = "">
				
		<!--- if the path doesn't already have content..  --->
		<cfif left(destination,8) neq "\content" and left(destination,7) neq "content" and left(destination,2) neq "\\" and mid(destination,2,2) neq ":\">
			<cfset destination = application.paths.content & destination>
		</cfif>
		<!--- assume it's a relative path, so convert to absolute --->
		<cfif left(destination,2) neq "\\" and mid(destination,2,2) neq ":\">
			<cfset absoluteDestPath = expandPath(destination)>
		<cfelse>
			<cfset absoluteDestPath = destination>
		</cfif>
		
		<cfif arguments.targetFileName neq "">
			<cfset filterFilename = arguments.targetFileName>
		</cfif>
		<cfif arguments.targetFileExtension neq "">
			<cfset filterFileExtension = arguments.targetFileExtension>
		</cfif>
		
		<cfif not structKeyExists(form,arguments.sourceFile)>
			<cfset fileExistsResult = application.com.filemanager.doesFileExist(filePath=arguments.sourceFile)>
			<cfset fileExists = fileExistsResult.exists>
			<!--- 2013-06-21	YMA	If we are uploading a file that already existed on the server we need to expand its path. The doesFileExist only checks userfiles directories and in this case, the file was in relay--->
			<cfif fileExists eq false>
				<cfif fileExists(ExpandPath(arguments.sourceFile))>
					<cfset fileExists=true>
					<Cfset fileExistsResult.Path = ExpandPath(arguments.sourceFile)>
				</cfif>
			</cfif>
		<cfelseif form[arguments.sourceFile] neq "">
			<cfset fileExists = true>
		</cfif>
		
		<cfdirectory name="thumbnailFiles" action="list" directory="#destination#" filter="#filterFilename#.#filterFileExtension#">
		<cfif structKeyExists(form,arguments.sourceFile) and form[arguments.sourceFile] neq "">
			<cfset cffile = application.com.fileManager.uploadFile(fileField=trim(arguments.sourceFile),maxSize_MB=maxSize_MB,destination=destination,nameConflict="Overwrite",accept="image/jpeg,image/gif,image/jpg,image/png,image/bmp,image/x-ms-bmp,image/tiff")>
			
			<cfif cffile.isOK>
				<cfset fileDest = cffile.serverdirectory>
				<cfset fileExt = cffile.serverFileExt>
				<cfset fileNameAndExt = cffile.serverFile>
				
				<!--- if we're going to be doing something (ie.a file is going to be uploaded or copied), then delete any existing files... --->
				<cfif fileExists>
					<cfif lcase(thumbnailFiles.directory & "\" & thumbnailFiles.name) neq lcase(fileDest & "\" & fileNameAndExt)>
						<cfloop query="thumbnailFiles">
							<cffile action="delete" file="#thumbnailFiles.directory#\#thumbnailFiles.name#">
						</cfloop>
					</cfif>
				</cfif>
			</cfif>
		<cfelse>
			
			<cfif fileExists>
				<cfif not directoryExists(absoluteDestPath)>
					<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=absoluteDestPath)>
				</cfif>
				<cftry>
					<cffile action="copy" source="#fileExistsResult.path#" destination="#absoluteDestPath#">
					<cfset fileDest = absoluteDestPath>
					<cfset fileExt = listLast(arguments.sourceFile,".")>
					<cfset fileNameAndExt = listLast(arguments.sourceFile,"/")>
					<cfset cffile.isOk = true>
					
					<cfcatch>
						<cfset cffile.isOk = false>
					</cfcatch>
				</cftry>
			</cfif>
		</cfif>
		
		<cfif cffile.isOK>

			<cfif arguments.targetFileName eq "">
				<cfset arguments.targetFileName = listFirst(fileNameAndExt,".")>
			</cfif>
			<cfset targetImage = arguments.targetFileName & "." & fileExt> <!--- e.g. Thumb_XX.bmp --->
			
			<cffile action ="rename" source = "#fileDest#\#fileNameAndExt#" destination = "#fileDest#\#targetImage#">
			
			<cfset imageWidth = application.com.relayImage.getImageWidth("#fileDest#\#targetImage#")>
			<cfset imageHeight = application.com.relayImage.getImageHeight("#fileDest#\#targetImage#")>
			
			<!--- 2014-02-03	YMA	Case 436524 If square targetHeight and targetWidth are both targetSize. --->
			<cfset targetWidth = arguments.targetSize>
			<cfif arguments.square>
				<cfset targetHeight = arguments.targetSize>	
			<cfelse>
				<cfset targetHeight = ceiling(imageHeight*(targetWidth/imageWidth))>	
			</cfif>	
			
			<cftry>
				<!--- if the file extension has been specified, and it's not the same as the file --->		
				<cfif fileExt neq arguments.targetFileExtension and arguments.targetFileExtension neq "">
					<!--- note: thumbnails are in PNG format by default --->
					<cfset Image = application.com.relayImage.convertImage(
							sourcePath=fileDest,
							sourceName=arguments.targetFileName & "." & fileExt ,
							targetName=arguments.targetFileName & "." & arguments.targetFileExtension
							)>
					<cfset application.com.fileManager.deleteFile(fileDest & "\" & arguments.targetFileName & "." & fileExt)> <!--- delete the original file --->
				<cfelseif arguments.targetFileExtension eq "">
					<!--- done so that we can return the proper filename and extension --->
					<cfset arguments.targetFileExtension = fileExt>
				</cfif>
				
				<!--- resize image to required width/height --->
				<!--- moved it here so resizing is done after the potential conversion to PNG above --->
				<cfset Image = application.com.relayImage.resizeImage(
						targetWidth=targetWidth,
						targetHeight=targetHeight,
						sourcePath=fileDest,
						sourceName=arguments.targetFileName & "." & arguments.targetFileExtension,
						targetName=arguments.targetFileName & "." & arguments.targetFileExtension 
						)>

				<cfset result.imagePath="#fileDest & "\" & arguments.targetFileName & "."& arguments.targetFileExtension#">
				<cfset relativeDestination = replace(destination,"\","/","ALL")>
				<cfset pos = findNoCase("/content",relativeDestination)>
	
				<cfif pos gt 0>
					<cfset relativeDestination = right(relativeDestination,len(relativeDestination)-pos+1)>
				</cfif>
				<cfset result.imageURL="#relativeDestination#/#arguments.targetFileName#.#arguments.targetFileExtension#">
				
				<cfcatch>
					<cfset result.message = "Problem creating thumbnail.">
					<cfset result.isOK = false>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="Creating Thumbnail",Severity="error",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfset structAppend(result,cffile,true)>
		</cfif>
		<cfreturn result>
	</cffunction>

	<!--- 	2016-05-04 WAB CASE 448873 
			Problems converting PDF to Image traced to jPedal V4.  These problems are broadly solved by jPedalV6, but replacing jPedal.jar causes other problems in CFDocument
			We have repackaged jPedalV6 into a custom jar file.  
			So, when we want the pdfDecoder, we will check for the existence of the custom jar file and load the custom path
	--->
	<cffunction name="getpdfDecoder" access="public" returntype="any" output="false" hint="">

		<cfif fileExists (server.coldFusion.rootDir & "\lib\jpedalCustom.jar")  >
			<cfset var jPedal =  createObject("java", "custom.org.jpedal.PdfDecoder").init(javaCast("boolean", true))>
		<cfelse>
			<cfset jPedal =  createObject("java", "org.jpedal.PdfDecoder").init(javaCast("boolean", true))>
		</cfif>
		
		<cfreturn jPedal>
	</cffunction>


	<cffunction name="convertPdfToImage" access="public" returntype="string" output="false" hint="Attempts to create a thumbnail from a file.">
	    <!---// define the arguments //--->
	    <cfargument name="source" required="true" hint="Full filepath to source file" />
	    <cfargument name="destination" type="string" required="true" hint="Destination folder for image" />
	    <cfargument name="filename" type="string" default="convertedImage" hint="Filename for image" /> <!--- 2015-11-11  ACPK    PROD2015-24 Added optional filename parameter --->
	    <cfargument name="page" type="numeric" default="1" hint="Page number in the PDF to convert to an image" />
	    <cfargument name="type" type="string" default="png" hint="Type of image to create (i.e. PNG, JPG, etc)" />
	    <cfargument name="width" hint="Width of image (specifying both a width and height for the image to scale-to-fit, otherwise the image is fullsize)" />
	    <cfargument name="height" hint="Height of image (specifying both a width and height for the image to scale-to-fit, otherwise the image is fullsize)" />
	    <cfargument name="minWidth" hint="minimum width we can save the image to" />
	    <cfargument name="minHeight" hint="minimum height we can save the image to" />
	    <cfargument name="highResolution" type="boolean" default="true" hint="Indicates whether or not to use high quality rendering" />
	    <cfargument name="interpolation" type="string" default="highestQuality" hint="Interpolation method used for resampling" />
	    <cfargument name="quality" type="numeric" default="1" hint="Defines the JPEG quality used to encode the image" />
	    <!---// declare variables //--->
	    <cfset var pdfDecode = "" />
	    <cfset var ConvertPagesToHiResImages = "" />
	    <cfset var pdfImage = "" />
	    <cfset var imageToSave = "" />
	    <cfset var scalingFactor = 1 />
	    <!--- 2015-11-10  ACPK    PROD2015-24 Use specified filename for images --->
		<cfset var newFileName = arguments.filename & '_page_' & arguments.page & "." & arguments.type />
		<cfset var newFile = arguments.destination & newFileName />
	    <cfset var hashMap = createObject("java","java.util.HashMap").init() />
	    
		<cftry>
	        <cfscript>
				pdfDecode = getpdfDecoder();
	            // the version of PdfDecoder in cf8 supports showing annotations
	            if( structKeyExists(pdfDecode, "showAnnotations") ) pdfDecode.showAnnotations = javaCast("boolean", false);
	            pdfDecode.useHiResScreenDisplay(javaCast("boolean", arguments.highResolution));
	            pdfDecode.setExtractionMode(javaCast("int", 0));
	           	if(IsSimpleValue(arguments.source)){
	            	pdfDecode.openPdfFile(javaCast("String", arguments.source));
	           	}else{
	            	pdfDecode.openPdfArray(arguments.source);
	           	}
	            // if a password has been supplied, use the password
	            if( structKeyExists(arguments, "password") )
	                pdfDecode.setEncryptionPassword(javaCast("String", arguments.password));
	           
	           /* set width and height to open PDF as */
				if((structKeyExists(arguments,"width") && arguments.width != "") && (structKeyExists(arguments,"height") && arguments.height != "")){
					/* check if width and height meat their minimum otherwise apply a scaling factor */
					if(structKeyExists(arguments,"minWidth") && arguments.width < arguments.minWidth){
						scalingFactor = arguments.minWidth/arguments.width;
					}
					if(structKeyExists(arguments,"minHeight") && arguments.height < arguments.minHeight){
						scalingFactor = arguments.minHeight/arguments.height;
					}
				}else{
					/* get the pdf's cropbox height and width and store to the arguments.height variable */
					arguments.height = pdfDecode.getPdfPageData().getCropBoxHeight(1);
					arguments.width = pdfDecode.getPdfPageData().getCropBoxWidth(1);
					
					/* If min height and width are defined then check they are correct in the pdfs crop box */
					if(structKeyExists(arguments,"minWidth") && arguments.width < arguments.minWidth){
						scalingFactor = arguments.minWidth/arguments.width;
					}
					if(structKeyExists(arguments,"minHeight") && arguments.height < arguments.minHeight){
						scalingFactor = arguments.minHeight/arguments.height;
					}
				}
				
				/* apply scaling factor and make sure the rounded value is not lower than scaled value */
				arguments.width = scalingFactor*arguments.width;
				if(arguments.width>round(arguments.width)){
					arguments.width=round(arguments.width)+1;
				}
				arguments.height = scalingFactor*arguments.height;
				if(arguments.height>round(arguments.height)){
					arguments.height=round(arguments.height)+1;
				}
				
				/* set the pdf to open in a specific width and height */
				hashMap.put(javaCast("int", 10),javaCast("string[]",[javaCast("string", "#arguments.width#"),javaCast("string", "#arguments.height#")]));
				
				/* something to do stopping this being very low resolution */
				hashMap.put(javaCast("int", 11),javaCast("boolean",true)) ;
				
	            imageToSave = createObject("java", "java.awt.image.BufferedImage");
	            imageToSave = pdfDecode.getPageAsHiRes(javaCast("int", page),hashMap);
	            // close the PDF file
	            pdfDecode.closePdfFile();
	            /*
	             * go back to native CF functions
	             */
	            // create a native CF image from the BufferedImage
	            pdfImage = imageNew(imageToSave);
	            // write the image to disk
	            imageWrite(pdfImage, newFile, arguments.quality);
	        </cfscript>
	        <!---// if an error has occured, just return an empty string to indicate we couldn't process the PDF //--->
			 <cfcatch type="any">
	       		<cfreturn "" />
	        </cfcatch>
	    </cftry>
	    <cfreturn newFileName />
	</cffunction>

</cfcomponent>