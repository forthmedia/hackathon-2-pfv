// �Relayware. All Rights Reserved 2014
/*

	phraseService.cfc.  Service for providing phrase functionality

	@author Richard.Tugwell
	@date 25/08/16

	Created initially to serve API phrase functionality

*/

component {

	variables.requiredfields = [ "countryID","languageID","phraseText" ];

//	GET stuff ///////////////////////////////////////////////////

	public struct function getPhrase( required string phraseID ) {

		if ( !phraseExists(  arguments.phraseID ) ) {
			return { "data" : {"phraseID" : arguments.phraseID } , "errorcode" : "INVALID_PHRASE" };
		}

		var phrases = getPhraseTranslations( arguments.phraseID );

		if ( !phrases.success ) {
			return { "data" : {"phraseID" : arguments.phraseID } , "errorcode" : "INVALID_PHRASE" };
		}

		var resp = {
			"data" : {
		    	"phraseID" : arguments.phraseID ,
		    	"phraseTextID" : getPhraseTextID( arguments.phraseID ),
				"recordcount" : phrases.data.recordcount,
		    	"translations" : []
		    },
			"errorcode" : ""
		};

	    for ( phrase in phrases.data ) {
			var tempStruct = {};
	 		tempStruct["countryID"] = phrase.countryID;
	 		tempStruct["languageID"] = phrase.languageID;
	 		tempStruct["language"] = phrase.language;
	 		tempStruct["phraseText"] = phrase.phraseText;
	    	arrayAppend( resp.data.translations , tempStruct );
		};

		return resp;

	}

	public any function getPhraseTranslations( required string phraseID ) {

	    var resp = { "success" : true , "data" : "" };
	    var sql = "";
	    var qry = new Query();

	    sql &= "select a.countryID , a.phraseID , a.languageID , a.phraseText , b.phraseTextID , c.language ";
	    sql &= "from phrases a , phraselist b , language c ";
	    sql &= "where 1 = 1 ";
	    sql &= "and a.phraseID = b.phraseid ";
	    sql &= "and a.languageID = c.languageID ";
	    sql &= "and a.phraseID = :phraseID ";

		qry.setSQL( sql );
		qry.addParam( name = "phraseID" , value = arguments.phraseID , CFSQLTYPE = "CF_SQL_INTEGER");

	    try {
			resp.data = qry.execute().getResult();
		}
		catch( any e )
		{
			resp.success = false;
		};

		return  resp;

	}

	public string function getPhraseTextID( required string phraseID ) {

	    var resp = "";
	    var sql = "";
	    var qry = new Query();

	    sql &= "select phrasetextID from phraselist where phraseID = :phraseID";

		qry.setSQL( sql );
		qry.addParam( name = "phraseID" , value = arguments.phraseID , CFSQLTYPE = "CF_SQL_INTEGER");

	    try {
			resp = qry.execute().getResult();
		}
		catch( any e )
		{
			return  "UNKNOWN DB ERROR";
		};

		return resp.phrasetextID;

	}

// Process an array of phrases, validate and decide what to do with them (insert / update )

	public struct function doPhraseTranslations( required struct phraseObject , required string ID ) {

		var phraseID = arguments.phraseObject.phraseID;
		var userID = arguments.ID;
		var resp = { "errorcode" : "" , "data" : phraseObject };

		if ( !structKeyExists( arguments.phraseObject , "phraseID") OR !phraseExists(  arguments.phraseObject.phraseID ) ) {
			resp.errorcode = "INVALID_PHRASE";
			return resp;
		}

		if ( !structKeyExists( arguments.phraseObject ,  "translations" )  OR  !isArray( arguments.phraseObject.translations ) ) {
			resp.errorcode = "INVALID_PHRASE_TRANSLATIONS";
			return resp;
		}

		arrayEach( arguments.phraseObject.translations , function( a ) {
			var result = "";
			var validation = validateTranslationObject(a);
			a["success"] = validation.success;
			a["errors"] = validation.errors;

			if ( validation.success ) {
				var phraseData = { phraseID : phraseID , languageID : a.languageID , countryID : a.countryID , phraseText : a.phraseText };
				//a["result"] = application.com.relaytranslations.addNewPhraseAndTranslationV2( argumentCollection = phraseData ); Not working as expected...
				if ( !phraseTranslationExists( phraseData ) ) {
					result = insertPhraseTranslation( phraseData , userID );
				}
				else {
					result = updatePhraseTranslation( phraseData = phraseData , userID = userID  );
 				}
				if ( !result.success ) {
					a["success"] = result.success;
					a["errors"] = result.errors;
					}
			};
		}
	);

		return resp;

	}


//	DATABASE stuff ///////////////////////////////////////////////////

public struct function updatePhraseTranslation( required struct phraseData , required string userID  ) {

		var resp = { "success" : true , "errors" : [] };
		var sql = "";
		var qry = new Query();

		sql &= "update phrases set";
		sql &= " phrasetext = :phraseText,";
		sql &= " lastupdatedby = :lastupdatedBy,";
		sql &= " lastupdated = GETDATE()";
		sql &= " where 1 = 1";
		sql &= " and phraseID = :phraseID";
		sql &= " and countryID = :countryID";
		sql &= " and languageID = :languageID";

		qry.setSQL( sql );
		qry.addParam( name = "phraseText" , value = phrasedata.phraseText , CFSQLTYPE = "CF_SQL_VARCHAR");
		qry.addParam( name = "lastUpdatedBy" , value = userID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "phraseID" , value = phrasedata.phraseID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "languageID" , value = phrasedata.languageID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "countryID" , value = phrasedata.countryID , CFSQLTYPE = "CF_SQL_INTEGER");

		try {
			qry.execute();
		}
		catch( any e )
		{
			arrayAppend( resp.errors , "Unknown error updating DB. languageID: " & phrasedata.languageID & " countryID: " & phrasedata.countryID );
			resp.success = false;
		};

		return resp;
};

public struct function insertPhraseTranslation(  required struct phraseData , required string userID   ) {

		var resp = { "success" : true , "errors" : [] };
		var sql = "";
		var qry = new Query();

		sql &= "insert into phrases ";
		sql &= " (phraseID , countryID , languageID , phraseText , lastupdatedBy , lastupdated , createdBy , created )";
		sql &= " VALUES";
		sql &= " ( :phraseID , :countryID , :languageID , :phraseText , :lastupdatedBy , GETDATE() , :createdBy ,  GETDATE() )";

		qry.setSQL( sql );
		qry.addParam( name = "phraseID" , value = phrasedata.phraseID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "languageID" , value = phrasedata.languageID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "countryID" , value = phrasedata.countryID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "phraseTExt" , value = phrasedata.phraseText , CFSQLTYPE = "CF_SQL_VARCHAR");
		qry.addParam( name = "lastUpdatedBy" , value = userID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "createdBy" , value = userID , CFSQLTYPE = "CF_SQL_INTEGER");

		try {
			qry.execute();
		}
		catch( any e )
		{
			arrayAppend( resp.errors , "error inserting into DB. languageID: " & phrasedata.languageID & " countryID: " & phrasedata.countryID );
			resp.success = false;
		};

		return resp;
};



//	VALIDATE stuff ///////////////////////////////////////////////////

	public boolean function phraseExists( required string phraseID ) {

	    var resp = "";
	    var sql = "";
	    var qry = new Query();

	    sql &= "select phraseID from phraselist where phraseID = :phraseID";

		qry.setSQL( sql );
		qry.addParam( name = "phraseID" , value = arguments.phraseID , CFSQLTYPE = "CF_SQL_INTEGER");

	    try {
			resp = qry.execute().getResult();
		}
		catch( any e )
		{
			//TODO: Do something...
		};

		return resp.recordcount gt 0;

	}

	public boolean function phraseTranslationExists( required struct phraseData ) {

	    var resp = "";
	    var sql = "";
	    var qry = new Query();

	    sql &= "select phraseID from phrases";
	    sql &= " where 1 = 1";
	    sql &= " and phraseID = :phraseID";
	    sql &= " and countryID = :countryID";
	    sql &= " and languageID = :languageID";

		qry.setSQL( sql );
		qry.addParam( name = "phraseID" , value = arguments.phraseData.phraseID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "languageID" , value = arguments.phraseData.languageID , CFSQLTYPE = "CF_SQL_INTEGER");
		qry.addParam( name = "countryID" , value = arguments.phraseData.countryID , CFSQLTYPE = "CF_SQL_INTEGER");

	    try {
			resp = qry.execute().getResult();
		}
		catch( any e )
		{
			return false;
			//TODO: Do something...
		};

		return resp.recordcount gt 0;

	}

	public any function validateTranslationObject( translation  ) {
		var resp = { "success" : true , "errors" : [] };

		arrayEach( requiredFields , function(f) {
				if ( !structKeyExists( translation , f ) ) {
					arrayAppend( resp.errors , "required param " & f & " not found");
					resp.success = false;
				}
			}
		);

		if ( ( translation.countryID != 0 ) && !application.com.relayCountries.isValidCountry( translation.countryID  ) ) {
			arrayAppend( resp.errors , translation.countryID & " is not a valid countryID");
			resp.success = false;
		}

		if ( !application.com.relayCountries.isValidCountryLanguage( countryID = translation.countryID , languageID = translation.languageID  ) ) {
			arrayAppend( resp.errors ,  translation.languageID & " is not a valid languageID for country " & translation.countryID);
			resp.success = false;
		}

		return resp;

	}
}
