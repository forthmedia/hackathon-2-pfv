﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WARNING WARNING WARNING
DO NOT USE references to application.  YOU MUST USE variables.applicationScope
WARNING WARNING WARNING --->
<!--- com/errorHandler.cfc

Mods
NYB	2011-03-14	LHID5528 created emailErrorNotification function
WAB 2011/04    Lots of varing
WAB 2011/04    Modify code for setting/checking sitewideErrorHandler
NYB	2011-04-14	LHID5915 added dataStruct.cfError as I need more info logged to diagnose
WAB 2011/04/18	LID6336 Major Problem if Heartbeat Server down, spawned an infinite number of errors!
WAB 2011/04/19  More Work to reduce the size of the errorStructure and make sure that we do not try to serialise objects
WAB 2011/04/20  Work to allow optional sending of email in recordRelayError_Warning (replaces emailErrorNotification)
				Get rid of recordRelayError_cfCatch, since all functions done by recordRelayError_Warning
WAB 2011/09		Modified code which looks for blocking spids to run asynchronously.  Hoped that it might work better and give better info, but actually did not seem to make much difference.
WAB 2011/10/04	Noticed a hardcoded cfadmin password
WAB 2011/10/31	Added code to delete certain items from error logs after a given period
WAB 2011/11/08  Alterations so that can be called during cluster calls (when session scope not available)
WAB 2012-07-06 Removed the 500 status Code because of conflicts with IIS
WAB 2012-09-19	Added special check for errors of type ApplicationInitialising (which I throw when we get locks during flushes etc.)
WAB 2012-09-24	Reworked checkForSpecialActions(), now called runCustomChecks().  Now dynamically looks for functions to run, so can be extended easily
WAB 2012-09-26 	Improved javascript error handling - save the offending lines to the db, either by reading the DOM with js, or by reading the source .JS file on the server
WAB 2012-09-27	Add TTL (Time To Live) column to relayError table,
WAB 2012/12/05 	Altered sendSingleErrorToHQAsynchronously() to use a CFTHREAD rather than the Asynchronous gateway
WAB 2013/01		further mod to sendSingleErrorToHQAsynchronously() to deal with cases when function is called when we are already in a CFTHREAD
WAB 2013/01/28	var'ing

2013-02-19	PPB		Case 433662  use phrases for error messages
2013-02-20 	PPB 				 remove reference to custom error handler - no longer needed
2013-03-13 	WAB Reinstated correct HTTP status code for Ajax requests - we think that it will be OK to have the IIS header added
2013-04-25	WAB	A new attempt to fix corruptions in the settings XML document
2013-06-04	WAB	Fix a long standing error when a timeout occurs in the webservices/noSession directory (which often happens with comms tracking links). Request.currentSite.domainAndRoot not defined
2013-07-01	WAB	Improved CustomCheck_MissingSetting() - has been to seen to actually work on a real corruption!
2013-12-04	WAB	Replace all instances of application.  with variables.applicationScope.  This is required for the code to work properly when called by the siteWideErrorHandler which does not run application.cfc
2014-02-10	WAB Fix issue in CustomCheck_SettingsError, tagcontext undefined. Not sure how it had ever worked, needed to be error.tagContext
2014-06-10 	WAB Rollback open transactions if before recording a ColdFusion error
2014-11-05 	WAB Only send Coldfusion Errors to HQ immediately.  Was a problem with too many errors clogging things up and using all the available threads.
2014-11-25	WAB	Added output of error.detail to outputErrorWithDebug()
2015-02-13  GCC Add noLock to query in copyErrorsToHQ()
2015-06-22	WAB	Alter the way that errors are recorded back to scheduledTaskLog
2015-10-05 	WAB JIRA Prod2015-150 Return correct 500 status code when errors occur
2015-11-10 	WAB	Improve performance of sending errors back to heartbeat by
				a) removing lots of data from the dataStruct which is sent - we had stopped saving them because struct was so big.
				b) Remove code for sending single errors immediately, should prevent thread errors
2015-12-01	WAB Fix to above - was failing when dataStruct contained undefined items
2016-01-13	WAB	CFAdmin Password now stored in db, so change a couple of function to make cfpassword not required
2016-01-13	WAB	Add visitID to error table so that can easily see errors in particular session
2016-01-15 	WAB PROD2015-518 Mods so that errors can be thrown specifying a specific filename/line (useful in cf_queryparam, cf_param) where the place the throw occurs is unrelated to the place where the error occured
2016-06-10	WAB Added an extendRequestTimeOut() to recordRelayError_warning function (was falling over when called by housekeeping)
2017-02-21	WAB PROD2016-3475 PushErrorsToHQ - send most critical errors first, tell heartbeat if no errors to be sent
--->



<cfcomponent>

<!--- This structure is used to work out the 'type' of an error, the list and numbers come from old relayware versions --->
<cfset this.ErrorType = structNew()>
<cfset this.ErrorType[1] = {type= "Other", match = ""}>
<cfset this.ErrorType[2] = {type= "Log file full", match = "log file for database .* is full"}>
<cfset this.ErrorType[3] = {type= "Data truncation", match = "String or binary data would be truncated"}>
<cfset this.ErrorType[4] = {type= "Violation of PRIMARY KEY", match = "Violation of PRIMARY KEY constraint"}>
<cfset this.ErrorType[5] = {type= "Request timeout", match = "The request has exceeded the allowable time limit"}>
<cfset this.ErrorType[6] = {type= "Lock timeout", match = "A timeout occurred while attempting to lock"}>
<cfset this.ErrorType[11] = {type= "Deadlock victim", match = "Deadlock victim"}>
<cfset this.ErrorType[12] = {type= "invalid XML character", match = "invalid XML character"}>
<cfset this.ErrorType[13] = {type= "CFML structure missing an element", match = "Element .* is undefined in a CFML structure"}>
<cfset this.ErrorType[14] = {type= "Database syntax error", match = "Incorrect syntax near the keyword|Error Executing Database Query"}>
<cfset this.ErrorType[15] = {type= "Missing Template", match = "Missing template|Could Not find the included template"}>
<cfset this.ErrorType[17] = {type= "Database object missing", match = "[SQLServer]Invalid object name"}>
<cfset this.ErrorType[18] = {type= "Undefined Variable", match = "Variable .* is undefined"}>
<cfset this.ErrorType[19] = {type= "Could not find prepared statement", match = "Could not find prepared statement"}>


<!---
	this structure is used to define how errors are aged
	a bit primitive, but might keep the size of error logs down a bit
	ideally need a way of updating from HQ!
	remember that all errors are copied to HQ as well (where they are also aged)
	CF errors kept a long time, other errors kept much less
 --->


<!--- this will be overridden by the initialise function --->
<cfset variables.applicationScopeAvailable = false>


<cffunction name="initialise">
	<cfargument name="applicationScope" default="#application#">
	<!--- DO NOT VAR THESE VARIABLES --->
	<cfset variables.applicationScope = arguments.applicationScope>
	<cfset variables.applicationScopeAvailable = true>

</cffunction>

<!--- Just cut and pasted from sitewideerrorHandler.cfm at the moment --->
<cffunction name="handleError" >
	<cfargument name="exception">

	<cfset var errorID = 0>
	<cfset var currentuser = "">
	<cfset var messageDisplayed = false>  <!--- if we get into the cfcatch block for any reason, this tells us whether the error message has already been displayed on screen --->

	<!--- WAB 2012-09-19 Trying to improve locking during initialisation and prevent race conditions
			Added this special line to catch "ApplicationInitialising" errors which I now throw in these cases
	--->
	<cfif structKeyExists (exception,"type") and exception.type is "ApplicationInitialising">
		<cfoutput>Application Initialising.  Please Wait..</cfoutput>
		<cfabort>
	</cfif>



	<!---
	WAB Return correct 500 statusCode
	Unfortunately this gives problems because IIS adds its own 500 message to our message
	In theory this can be corrected configuring IIS with existingResponse="passThrough"
	However this then leads to our 404 error handling not working.
	This was added for Early versions of RW2012, but we decided to remove it again (2012-07-06) because of the above problems
	WAB 2013-03-13 Reinstated just for ajax requests
	WAB 2015-10-05 JIRA Prod2015-150 We have now sorted out the IIS pass through issue (by using the url rewriting module to deal with missing templates)
					So we can return the correct status code in all instances
	--->

	<cfif areapplicationvariablesloaded() >
	<cftry>
		<!--- Add a CFTRY because this cfheader will sometimes fail - eg if someone has used a cflush in their code --->
		<cfheader statusCode = "500"   statusText = "Server Error">
		<cfcatch>
		</cfcatch>
	</cftry>
	</cfif>

	<cftry>
		<cfset extendRequestTimeOut()>

		<cfset currentUser = getCurrentUserStruct ()>

		<!--- record the error in the database--->
		<cfset errorID = recordRelayError_ColdFusion (error = exception,userStruct = currentUser)>

		<!--- NJH 2008/10/13 CR-TND562 - log the error ID in the scheduledTaskLog table if the error thrown was from a scheduled task
				WAB 2015-06-22 Modified and moved.
				Only do this update for an uncaught coldfusion error,
				The IDs of all other errors/warnings which are logged (and there may be more than one) are collected in an array and are thrown into scheduledTaskLog.result
				This is normally done at end of cf_scheduled task, but of course this won't run if there is an uncaught error
		--->
		<cfif isDefined("request.scheduledTask")>
			<cfset var scheduleResult = {}>
			<!---  not interested in this errorid (recorded elsewhere), so remove --->
			<cfset arrayDelete (request.scheduledTask.errorsLogged, errorID)>
			<cfif arrayLen (request.scheduledTask.errorsLogged)>
				<cfset scheduleResult.errorsLogged = request.scheduledTask.errorsLogged>
			</cfif>

		    <cfset var updateScheduledTaskLog = "">
			<cfquery name="updateScheduledTaskLog" datasource="#applicationScope.SiteDataSource#">
				update	scheduledTaskLog
			    set 	errorID = #ErrorID#,
					    <cfif structCount (scheduleResult)>result =  <cf_queryparam value="#serializeJSON(scheduleResult)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,</cfif>
			        	endTime = GetDate(),
			        	lastUpdated = GetDate(),
			        	lastUpdatedBy = #currentUser.userGroupId#
				where 	scheduledTaskLogID = #request.scheduledTask.scheduledTaskLogID#
			</cfquery>
		</cfif>

		<!--- WAB 2015-06-22 Add support for sending an email from an uncaught error.  Designed for use on scheduled tasks, but could be used elsewhere
				request.onError is set using errorHandler.setOnError()
		 --->
		<cfif structKeyExists (request,"onError")>
			<cfset var args = {errorID = errorID, error = exception, message = request.onError.message, emailTo = request.onError.EmailTo}>
			<cfset sendErrorEmail (argumentCollection = args)>
		</cfif>

		<!--- if this user is to be shown the error then output it to screen--->
		<cfif currentUser.errors.showDump>
			<cfoutput>#outputErrorWithDebug(error = exception)#</cfoutput>
		<cfelse>
			<cfoutput>#outputErrorMessage(exception = exception, errorid = errorid, currentUser=currentUser)#</cfoutput>
		</cfif>
		<cfset messageDisplayed = true>

		<cfset runCustomChecks (error = exception,errorid = errorid)>

		<cfcatch>
			<!---
			WAB 2011/03/17 Deal slightly better if there is an error in the errorHandler itself.
			On dev sites:
				Gives a dump,
			On other sites
				Gives a plain message (if the error occurs before the message has been displayed)
				Sends an email to server support
			--->

			<cfif areapplicationvariablesloaded() and applicationScope.testSite is 2>
				<cfset getPageContext().getOut().clearBuffer()>
				<cfoutput>
				<B><BR>Error In Error Handler</B><BR>
				(This dump only shown on Dev Sites)<BR>
				Error Handler Error
				<cfdump expand="false" var="#cfcatch#">
				Original Error
				<cfdump expand="false" var="#exception#">
				Current User
				<cfdump expand="false" var="#currentUser#">
				</cfoutput>

			<cfelse >
				<cfif not messageDisplayed>
					<!--- If error occurred before message displayed then
					clear all content already generated and display a simple message
						Sorry this is a rather weak message, hopefully it won't appear very often! --->

					<cfset getPageContext().getOut().clearBuffer()>
					<cfoutput>An Error Has Occurred</cfoutput>
				</cfif>

				<cfmail to="serverSupport@relayware.com"  from="serverSupport@relayware.com" subject="Error in Error Handler #cgi.server_Name#" type="html">
					Error attempting to handle the following error for:<br />
					#currentUser.person.firstname#
					#currentUser.person.lastname# (personID #currentUser.personID#) <br />
					on #cgi.http_host#:<br>
					<cfdump var="#exception#">
					<cfdump var="#cfcatch#">
					<cfdump var="#CGI#">
					<cfif isdefined("URL")>
						<cfdump var="#URL#">
					</cfif>
				</cfmail>

			</cfif>


		</cfcatch>
	</cftry>

	<cfreturn errorID>

</cffunction>

	<!--- this code copied from com.globalFunctions, but put here so available even if com object have not been loaded--->
	<cffunction name="extendRequestTimeOut" hint="extends request timeout by 60 seconds so that actual timeouts get handled without error">

		<cfset var newTimeout = createObject("java", "coldfusion.runtime.RequestMonitor").getRequestTimeout() + 60>
		<cfsetting requestTimeout = #newTimeout#>

	</cffunction>


<!---
  This function takes the CFERROR struct and puts an debug/error message on screen in the format of the standard CF error message
    --->

  <cffunction name="outputErrorWithDebug">
    <cfargument name="error">
    <cfargument name="noDump" default = "false">  <!--- allows us to use this code to send an email --->

	<cfset var errorLinesStruct = {isOK = false}>

	<cfif structCount(error)>   <!--- because this code can now be used to send emails when some warnings are recorded, I will check that the error structure has something in it--->

		<cfset var templateInfo = extractTemplateInfoFromErrorStructure(error)>

		<cfif templateInfo.fileName is not "" >
			<cfset errorLinesStruct  = getLinesFromAFile(filename = templateInfo.fileName, line = templateInfo.line, linesbefore=5, linesafter=3)>
		</cfif>

		<cfif isdefined("returnFormat")>
			<!--- This is a cfc request, output a smaller error--->
			<cfoutput>
				<cfset getPageContext().getOut().clearBuffer()>
				#error.message# <BR>
				<cfif errorLinesStruct.isOK>
	                  #errorLinesStruct.filename#: line #errorLinesStruct.line#<br>
		        </cfif>
		        <cfif structKeyExists (error,"SQL")>
		              #error.SQL#
		        </cfif>
			</cfoutput>


		<cfelse>

	      <cfoutput>
			<!--- need these incase error is in the title block, probably need others --->
				</title></head>


	        <table width="500" cellpadding="0" cellspacing="0" border="0">
	            <tr>
	                <td id="tableProps2" align="left" valign="middle" width="500">
	                    <h1 id="textSection1" style="COLOR: black; FONT: 13pt/15pt verdana">
	                      #error.message#
	                    </h1>
	                </td>
	            </tr>

				<!--- WAB 2014-11-25, for some reason we weren't outputting the detail here - must have been removed at some point but have reinstated it with an IF --->
				<CFIF structKeyExists (error, "DETAIL")>
	            <tr>
	                <td id="tablePropsWidth" width="400" colspan="2">
	                    <font style="COLOR: black; FONT: 8pt/11pt verdana">
	                      #error.DETAIL#
	                    </font>
	                </td>
	            </tr>
	            </CFIF>

	            <tr>
	                <td height>&nbsp;</td>
	            </tr>

			<cfif errorLinesStruct.isOK>
	              <tr>
	                <td width="400" colspan="2">
	                <font style="COLOR: black; FONT: 8pt/11pt verdana">
	                  The error occurred in <b>#errorLinesStruct.filename#: line #errorLinesStruct.line#</b><br>
	                  </td>
	              </tr>

	          <!--- Output the lines near where the error ocurred--->

	            <tr>
	                <td colspan="2">
	              <pre>#outputErrorLines(errorLinesStruct)#
	              </pre>
	              </td>
	          </tr>

			</cfif>

	        <cfif structKeyExists (error,"SQL")>
	        <tr>
	            <td >
	          SQL:  <BR>

	              <cfoutput><PRE>#error.SQL#</PRE></cfoutput>
	            </td>
	          </tr>

	        </cfif>

		   	<cfif not noDump>
			   <tr>
		            <td >
		          CFError Dump (Click to Expand):

		              <cfdump var="#error#" expand="no">
		            </td>
		          </tr>


			</cfif>


	              <tr>
	            <td colspan="2">
	              <hr color="##C0C0C0" noshade>
	            </td>
	          </tr>
	      </table>

	      </cfoutput>

    </cfif>

    </cfif>

  </cffunction>


<cffunction name="getLinesFromAFile">
	<cfargument name="filename">
	<cfargument name="line">
	<cfargument name="linesbefore">
	<cfargument name="linesafter">
	<cfargument name="trimto" default = "0">

		<cfset var sourceFileContent = "">
		<cfset var sourceFileArray = "">
		<cfset var result = {isOK = true, filename = filename}>
		<cfset var fileArrayIndex = 0>
		<cfset var text = "">

		<cftry>
	  		<cffile action = "read" file="#fileName#" variable="sourceFileContent">

			<!--- replace all chr(13) since I am going to array to list on delimiter of chr(10) --->
			<cfset sourceFileContent = Replace(sourceFileContent, Chr(13), "", "all")>

			<!--- if a line has no content then add a space --->
			<cfset sourceFileContent = reReplace(sourceFileContent, "\A\n", " " & Chr(10), "all")>

			<!--- similar to above, should be able to do better with a zero width regexp --->
		    <cfloop condition="findNocase(Chr(10) & Chr(10),sourceFileContent)">
				<cfset sourceFileContent = Replace(sourceFileContent, Chr(10) & Chr(10), Chr(10) & " " & Chr(10), "all")>
		    </cfloop>

			<cfset sourceFileArray = ListToArray(sourceFileContent,  Chr(10))>
			<cfset result.line = line>
		    <cfset result.startLine = max(1,line-linesbefore)>
	       	<cfset result.endLine = min(arrayLen(sourceFileArray),line+linesafter)>
			<cfset result.lines = {}>

				<cfloop index="fileArrayIndex" from = "#result.startLine#" to = "#result.endLine#">
					<cfset text = sourceFileArray[fileArrayIndex]>
					<cfif trimto is not 0 and len(text) gt trimto>
						<cfset text = left (text,trimto) & " ...">
					</cfif>
					<cfset result.lines[fileArrayIndex] = text>
				</cfloop>
			<cfcatch>
				<cfset result.isOK = false>
			</cfcatch>
		</cftry>

		<cfreturn result>

</cffunction>

<cffunction name="getErrorFileAndLineFromTagContext">
	<cfargument name="tagContext">
	<cfset var result = {filename = "", line = 0}>
	<cfset var tagContextIndex = 0>

			<cfloop index="tagContextIndex" from ="1" to ="#arrayLen(tagcontext)#">
				<cfif fileExists (tagcontext[tagContextIndex].template)>
					<cfset result.fileName = tagcontext[tagContextIndex].template>
					<cfset result.line = tagcontext[tagContextIndex].line>
							<cfbreak>
				</cfif>
			</cfloop>

			<cfif areapplicationvariablesloaded()>
				<cfset result.relativeFileName = replaceNocase(replaceNocase(replaceNocase(result.fileName,applicationScope.paths.abovecode,""),applicationScope.paths.userfiles,""),applicationScope.paths.core,"")>
			<cfelse>
				<cfset result.relativeFileName = result.fileName>
			</cfif>

	<cfreturn result>

</cffunction>

  <cffunction name="extractTemplateInfoFromErrorStructure">
	<cfargument name="errorStructure">

		<cfset var result = "">
		<cfset var tagContext = arrayNew(1)>

	      <!--- NJH 2010/08/09 check also if tagcontext is a key, as it doesn't appear that rootcause always exists 
	      	      WAB 2016-10-19 (During PROD2016-2415) and sometimes rootCause does not have a tagContext either
	      --->
		<cfif structKeyExists (errorStructure,"rootcause") and structKeyExists (errorStructure.rootcause,"tagcontext") >
			<cfset tagContext = errorStructure.rootcause.tagcontext>
		<cfelseif structKeyExists (errorStructure,"tagcontext")>
			<cfset tagContext = errorStructure.tagcontext>
		</cfif>

		<!--- 2016-01-15 WAB PROD2015-518
			Look for File and Line information in the detail.
			This is used if we thrown an error, but want to report a different file/linenumber
		--->
		<cfset var regExp = "File:(.*). Line:([0-9]*)">
		<cfif structKeyExists (errorStructure,"detail") and refindNoCase(regExp,errorStructure.detail)>
			<cfset var regExpResult = application.com.regExp.refindAlloccurrences(regExp,errorStructure.detail, {1 = "relativefilename", 2 = "line"})[1]>
			<cfset result = {line=regExpResult.line, relativefilename=regExpResult.relativefilename, filename = expandPath (regExpResult.relativefilename)}>
		<cfelse>
			<cfset result = getErrorFileAndLineFromTagContext (tagcontext)>
		</cfif>

		<cfreturn result>

  </cffunction>


<cffunction name="outputErrorLines" >
	<cfargument name="errorLinesStruct">

	<cfset var lineList = "">
	<cfset var lineNumber = "">

	<cfif errorLinesStruct.isOK>
		<cfset lineList = listsort(structKeyList(errorLinesStruct.lines),"numeric")>
		<cfloop list="#linelist#" index="lineNumber"><cfoutput><cfif lineNumber is errorLinesStruct.Line><b></cfif>#lineNumber# :#htmleditformat(errorLinesStruct.lines[lineNumber])# <cfif lineNumber is errorLinesStruct.Line></b></cfif><BR></cfoutput>	</cfloop>
	</cfif>

	<cfreturn>

</cffunction>

  <cffunction name="outputUserDetails">
    <cfargument name="userStruct">

	<cfoutput>

	<table>
		<cfif structKeyExists (userStruct,"fullname")>
		<tr>
			<td>User Name</td>
			<td>#userStruct.fullname#</td>
		</tr>
		</cfif>
		<cfif structKeyExists (userStruct,"person") and structKeyExists (userStruct.person,"email")>
		<tr>
			<td>Email</td>
			<td>#userStruct.person.Email#</td>
		</tr>
		</cfif>
		<cfif structKeyExists (userStruct,"personid")>
		<tr>
			<td>PersonID</td>
			<td>#userStruct.personid#</td>
		</tr>
		</cfif>
		<cfif structKeyExists (userStruct,"countryid")>
		<tr>
			<td>Country / Language</td>
			<td>#userStruct.CountryID# / #userStruct.LanguageID#</td>
		</tr>
		</cfif>


	</table>

	</cfoutput>


</cffunction>




	<cffunction name="outputErrorMessage">
		<cfargument name="exception">
		<cfargument name="errorID">
		<cfargument name="currentUser" type="struct" required="true">

		<cfset var ErrorMessageText = "">
		<cfset var ErrorMessageTranslateResult = "">
		<cfset var ErrorMessage = structNew()>
		<cfset ErrorMessage.ShowSpecificErrorMessage = False>   <!--- set this to true to override the standard text --->
		<cfset ErrorMessage.showTryAgainLink = false>

		<!--- Search for specific errors for which we can display specific messages --->
		<cfif refindNoCase("Transaction.*was deadlocked",exception.message) is not 0>
			<cfset ErrorMessage.ShowSpecificErrorMessage = True>
			<cfset ErrorMessage.Title = "phr_sys_error_TransactionDeadlocked">					<!--- 2013-02-19 PPB Case 433662 use phrase --->
			<cfset ErrorMessage.Message = "phr_sys_error_TransactionDeadlockedMessage">			<!--- 2013-02-19 PPB Case 433662 use phrase --->
			<cfset ErrorMessage.showTryAgainLink = true>
		<cfelseif refindNoCase("exceeded the allowable time limit",exception.message) is not 0 or exception.message contains "Execution timeout expired">
			<cfset ErrorMessage.ShowSpecificErrorMessage = True>
			<cfset ErrorMessage.Title = "phr_sys_error_RequestTimedOut">						<!--- 2013-02-19 PPB Case 433662 use phrase --->
			<cfset ErrorMessage.Message = "phr_sys_error_RequestTimedOutMessage">				<!--- 2013-02-19 PPB Case 433662 use phrase --->
			<cfset ErrorMessage.showTryAgainLink = true>

		<cfelseif refindNoCase("some other error message ",exception.message) is not 0>
			<cfset ErrorMessage.ShowSpecificErrorMessage = True>
			<cfset ErrorMessage.Title = "Some specific error">
			<cfset ErrorMessage.Message = "some other error message">
			<cfset ErrorMessage.showTryAgainLink = true>

		</cfif>


		<!--- Give the user a message
			either a default message, or one defined in content\errors\customError_display.cfm

		 --->

		<!--- Clear all content already generated --->
		<cfset getPageContext().getOut().clearBuffer()>


		<cfif areApplicationVariablesLoaded() and fileexists("#applicationScope.paths.content#\errors\customError_display.cfm")>

<!--- 2013-02-20 PPB remove reference to custom error handler - no longer needed
			<cftry>
				<cfinclude template = "/content/errors/customError_display.cfm">
				<cfcatch type="any">
				</cfcatch>
			</cftry>
 --->
		<cfelseif not areApplicationVariablesLoaded() >
			<!--- Mainly for during dev--->
			<cfoutput>
				<cfif cgi.remote_host contains "192.168.0">
					This error dump only appears on internal network <BR>
					(#ErrorID#)<BR>
					<cfdump var="#exception#">
				</cfif>
			</cfoutput>

		<cfelse>
			<cfsaveContent variable="ErrorMessageText">
				<cfoutput>
	<!---
		WAB 2012-06 Removed the HTML tags, these are now added by the onRequestEnd processing
		<HTML>
				<HEAD>
				<TITLE><cfif ErrorMessage.ShowSpecificErrorMessage>#ErrorMessage.Title#<cfelse>Template Unavailable</cfif></TITLE>
				<!--- NJH 2009/01/05 - Bug Fix All Sites Issue 1561 - use the stylesheet loaded in relay\variables.applicationScope..cfm
				<LINK REL='stylesheet' HREF='#request.currentSite.httpProtocol##CGI.http_host#/code/styles/DefaultPartnerStyles.css'> --->
				</HEAD>


				<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			--->

				<table width="500" border="0" cellspacing="0" cellpadding="5" align="center">
					<TR><td align="center"><cfif ErrorMessage.ShowSpecificErrorMessage>#ErrorMessage.Title#</cfif></td></TR>
				 	<TR>
				  		<TD ALIGN="CENTER">
							<cfif ErrorMessage.ShowSpecificErrorMessage>
								#ErrorMessage.Message#
							<cfelse>
								phr_sys_error_message
							  <!---  Sorry the template you are trying to reach is not available.
							   An automatic email has been generated and sent to the support team. They will deal
							   with this as soon as possible and will email you to let you know of their progress. --->
							</cfif>
								<BR>
								phr_sys_error_contactSupport
							   <!--- If you would like to contact the support team, please
							   send E-Mail to: <A HREF="mailto:#currentUser.SupportEmailAddress#">#currentUser.SupportEmailAddress#</A>. --->
								<BR>
								(<cfif currentUser.errors.showLink>
									<a href="#getURLOfErrorDetailsFile()#?errorID=#errorID#" target="errorWindow">#ErrorID#-#applicationScope.instance.databaseid#</a><cfelse>#ErrorID#-#applicationScope.instance.databaseid#</cfif>)
						</TD>
					</TR>

					<cfif ErrorMessage.showTryAgainLink>
					<TR>
						<TD align="center">
							<A HREF="javascript:window.location.reload()"><FONT size "+2"><B>Try Again</b></font></a>
						</TD>
					</tr>
					</cfif>

					<TR>
						<TD align="center">
							<A HREF="javascript:history.go(-1)"><FONT size "+2"><B>phr_sys_error_GoBack</b></font></a>
						</TD>
					</tr>
				</TABLE>

<!---				</BODY>
				</HTML> --->
				</cfoutput>
			</cfsaveContent>

			<!--- WAB 2011/03/09 need to use this function rather than cf_translate because the customtags path is not always available --->
			<cfset ErrorMessageTranslateResult = applicationScope.com.relayTranslations.translateStringComplicated(string = ErrorMessageText, languageID=arguments.currentUser.languageID, countryID=arguments.currentUser.countryID)>
			<cfoutput>#ErrorMessageTranslateResult.string#</cfoutput>

		</cfif>



	</cffunction>

  <cffunction name="recordRelayError_Javascript">
  	<cfargument name="pageURL" required="true">  <!--- URL of the calling page --->
  	<cfargument name="fileURL" required="true">  <!--- URL of the file the error was in (could be a JS file) --->
  	<cfargument name="msg" required="true">
  	<cfargument name="line" required="true">
  	<cfargument name="dataStruct" default=#structNew()#>


		<cfset var template = replace (arguments.pageurl,"#request.currentSite.httpProtocol##cgi.http_host#","")>
		<cfset var filename = replace (arguments.fileURL,"#request.currentSite.httpProtocol##cgi.http_host#","")>
		<cfset var absolutePath = '' />

		<cfset arguments.dataStruct.JSError = {url = arguments.pageurl,msg = msg, line = line,filename = filename}>

		<!--- if error was in a local JS file which we can read and get the offending line --->
		<cfif fileURL contains #cgi.http_host# and listlast(fileURL,".") is "js">
			<cfset absolutePath = expandPath(filename)>
			<cfset arguments.datastruct.sourcefile = getLinesFromAFile(filename = absolutePath,line = line, linesbefore = 5,linesafter= 3, trimto = 100)>
		</cfif>

		<!--- Look for the original request in relaycurrent user, and then pull out the form variables--->



		<!--- WAB 2012-04-16, occasionally line comes through as undefined--->
		<cfif arguments.line is "undefined">
			<cfset arguments.line = "null">
		</cfif>

		<cfset insertRelayErrorRecord (source="JAVASCRIPT", template = template, diagnostics = msg, filename = filename, line = line ,dataStruct=dataStruct,addToDataStruct="user",TTL=60)>

  </cffunction>

  <cffunction name="recordRelayError_ColdFusion">
    <cfargument name="error">
    <cfargument name="userStruct" default="#getCurrentUserStruct()#">

      <!---


      For a real error we have

      Browser    = cgi.HTTP_USER_AGENT
      datetime
      diagnostics  (derived from root cause = rootcause.message plus a line number)
      generatedcontent
      httpreferrer  = cgi.HTTP_REFERER
      mailto
      message     = rootcause.message
      querystring   = cgi.QUERY_ STRING
      remoteaddress = cgi.REMOTE_ADDR
      rootcause
      stacktrace
      tagcontext
      Template   from tagContext.template
      Type

      --->

      	<cfset var errorID= 0>
	  	<cfset var dataStruct = {}>


		<cfset var templateInfo = extractTemplateInfoFromErrorStructure(error)>

		<cfset var errorType = getErrorType(error)>
		<!--- I want to serialise the error struct, but need to remove some keys which are too large or can't be duplicated --->
		<cfset dataStruct.cferror = CopyErrorOrCatch (error)>

		<cfif templateInfo.fileName is not "" >
			<cfset dataStruct.sourceFile = getLinesFromAFile(filename = templateInfo.fileName, line = templateInfo.line, linesbefore=5, linesafter=3)>
		</cfif>

		<!---
			WAB 2012-10-17 remove cf_mappings from error structure - we never have problems in that area!
			<cfif areApplicationVariablesLoaded()>
				<cfset dataStruct.cf_mappings = applicationScope.cf_mappings>
			</cfif>
		--->
		<cfif areApplicationVariablesLoaded()>
   			<cfset applicationScope.com.request.rollBackOpenTransactions()>
		</cfif>

	     <cfset errorid = insertRelayErrorRecord (source="COLDFUSION",userStruct = userStruct, diagnostics = error.message, filename = templateInfo.relativefilename, line = templateInfo.line, dataStruct=dataStruct,addToDataStruct="form,url,user,previouspages",type=errorType,TTL=400)>

		<cfreturn errorid>

  </cffunction>



	<cffunction name="getErrorType">
		<cfargument name="errorStructure">

		<cfset var type="Other">
		<cfset var matchResult = "">
		<cfset var ErrorTypeKey = "">
		<cfset var thisErrorType = "">
		<cfset var textToSearch = errorStructure.message>
		<!--- WAB 2011/11/08 added .detail to search --->
		<cfif structKeyExists (errorStructure,"detail")>
			<cfset textToSearch = textToSearch  & errorStructure.detail>
		</cfif>

		<cfloop item="ErrorTypeKey" collection = "#this.errorType#">
			<cfset thisErrorType = this.errorType[ErrorTypeKey]>
			<cfif thisErrorType.Match is not "">
				<cfset matchResult = refindNoCase(thisErrorType.Match,textToSearch)>
				<cfif matchResult is not 0>
					<cfset type = thisErrorType.Type>
					<cfbreak>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn type>
	</cffunction>


  <cffunction name="recordRelayError_Warning">
		<cfargument name="Severity" required="true" default="WARNING" >  <!--- one of ERROR,WARNING,INFORMATION,DEBUG--->
		<cfargument name="Type" default = "">
		<cfargument name="Message" default="">
		<cfargument name="WarningStructure" default = "#structNew()#">
		<cfargument name="catch" default="#structNew()#"> <!---DEPRECATED (as cant't be used in cfscript, use caughtError instead)' RJT --->
	    <cfargument name="caughtError" default = "#catch#">
	    <cfargument name="addToDataStruct" default = "form,url">
	    <cfargument name="addUserToDataStruct" default = "false">
	    <cfargument name="sendEmail" default = "false">
	    <cfargument name="emailSubject" default = "">
		<cfargument name="TTL" >  <!--- Time to Live (days) --->
		<cfargument name="emailTo" default = ""> <!--- 2014/11/12 GCC added --->


		<cfset var errorID = 0>
		<cfset var args = {}>
		<cfset var diagnostics = message>

		<cfset extendRequestTimeOut ()>

		<cfif not structKeyExists(arguments,"TTL")>
			<cfswitch expression="#severity#">
				<cfcase value="Error"><cfset arguments.ttl = 400></cfcase>
				<cfcase value="Warning"><cfset arguments.ttl = 90></cfcase>
				<cfcase value="Information"><cfset arguments.ttl = 45></cfcase>
				<cfcase value="Debug"><cfset arguments.ttl = 7></cfcase>
				<cfdefaultcase ><cfset arguments.ttl = 90></cfdefaultcase>
			</cfswitch>
		</cfif>

		<cfif not listFindNoCase("error,warning,information,debug",severity)>
			<cfset arguments.severity = "WARNING">
		</cfif>

		<cfif structCount(caughtError)>
			<cfset arguments.WarningStructure.cferror = CopyErrorOrCatch(caughtError)>
		</cfif>

		<cfif type is "">
			<cfif structCount(caughtError)>
				<cfset arguments.type = getErrorType(caughtError)>
			<cfelse>
				<cfset arguments.type = "Other">
			</cfif>
		</cfif>

		<cfif diagnostics is "" and structCount(caughtError)>
			<cfset diagnostics = caughtError.message>
		</cfif>

		<cfif addUserToDataStruct>
			<cfset arguments.addToDataStruct = listappend(arguments.addToDataStruct,"user")>
		</cfif>

		<cfset errorID = insertRelayErrorRecord (source="#ucase(severity)#", type=type,diagnostics = diagnostics, dataStruct=WarningStructure,addToDataStruct=addToDataStruct,TTL=TTL)>

		<cfparam name="request.Errors" default="#structNew()#">

		<cfset request.Errors[errorID] = {message = Message,severity = severity}>

		<cfif sendEmail>
			<cfset args = {errorID = errorID,error = caughtError,message = message}>
			<cfif structKeyExists (arguments,"emailTo")>
				<cfset args.emailTo = emailTo>
			</cfif>
			<cfset sendErrorEmail (argumentCollection = args)>
		</cfif>

		<cfreturn errorid>

  </cffunction>

  <cffunction name="recordRelayError_CFCATCH">
    <cfargument name="catch" required="true">
    <cfargument name="type" default = "">
	 <cfargument name="TTL" default = "90">

	<!---
	WAB 2011/04/20
	recordRelayError_warning does the same job as this function but better,
	For backwards compatibility replaced with call to recordRelayError_Warning.
	But ideally do not use this function
	--->
      <!---
      For cfcatch we have

      detail
      errnumber
      message
      stacktrace
      tagcontext
      type
      name
      These are actually all in the rootCause Structure of a real error
      --->
		<cfset var dataStruct = {}>
		<cfset var argumentCollectionStruct = {source="Error",diagnostics = catch.message,filename = catch.tagcontext[1].template,line = catch.tagcontext[1].line,TTL=TTL}>
		<!--- START 2011-04-14 NYB LHID5915 added dataStruct.cfError as I need more info logged to diagnose --->
		<cfif isdefined("arguments")>
			<cfif structkeyexists(arguments,"catch")>
				<cfset dataStruct.cfError = catch>
				<cfset StructInsert(argumentCollectionStruct, "dataStruct", dataStruct)>
			</cfif>
			<cfif structkeyexists(arguments,"type")>
				<cfset StructInsert(argumentCollectionStruct, "type", arguments.type)>
			</cfif>
		</cfif>

      	<cfset errorID=insertRelayErrorRecord (argumentCollection=argumentCollectionStruct)>
		<!--- END 2011-04-14 NYB LHID5915 added dataStruct.cfError as I need more info logged to diagnose --->
		
		<cfreturn errorID> 
  </cffunction>


	<!--- take a copy of an error structure, removing any keys which won't serialise or are too big

			was originally inline with these comments
			NJH 2010/08/05 had to duplicate the error in this way, as you can't wddx objects, which this structure contained. However,
			we couldn't delete any keys from this structure as it was still considered a java object..
			http://www.coldfusionjedi.com/index.cfm/17-09-2009/Interesting-tidbits-on-ColdFusion-Exceptions
			 WAB 2010/10/05 filter out some of the keys which make the wddx package too big or indeed cause everything to crash
	--->


	<cffunction name="CopyErrorOrCatch">
		<cfargument name="errorStructure">

		<cfset var result = {}>
		<cfset var key="">
		<cfset var arrayIndex = "">
		<cfset var maxLengthOfTagContext = 10 >
		<cfset var deleteTo = 0 >

		<cfset var keysToDelete ="variabletype,objectType,sourceType,type,cause">
				<!--- am not saving stacktrace and variabletype
				will get stackTrace, TagContext,detail,message,resolvedname,errnumber,type,element,sql
				and any others I don't know about - thought it better to filter out known ones rather than only include known ones (since we would lose the ones I don't know about.
				If any more cause errors we can add them to list)
				also get rid of any objects, since these will not serialise
				--->

			<cfloop item="key" collection="#arguments.errorStructure#">
				<cfif listfindNoCase(keysToDelete,KEY) is 0 and not isObject(arguments.errorStructure[key])>
					<cfset result[key] = duplicate(arguments.errorStructure[key])>
				</cfif>
			</cfloop>

		<cfif structKeyExists (result,"rootcause")>
			<cfif structKeyExists (result.rootcause,"nextException")  >
				<cfset structDelete (result, "rootcause")>
				<cfset result.rootcause = "This Key has been deleted due to space limitations">
			</cfif>

			<!--- get rid of rootcause.tagcontext - if duplicated --->
			<cfif structKeyExists (result,"tagContext") and structKeyExists (result.rootcause,"tagContext")>
				<cfset structDelete(result.rootcause,"tagContext")>
			</cfif>

			<!--- get rid of any unwanted keys in rootcause --->
			<cfloop item="key" collection="#result.rootcause#">
				<cfif listfindNoCase(keysToDelete,KEY) or isObject(result.rootcause[key])>
					<cfset structDelete (result.rootcause,key) >
				</cfif>
			</cfloop>

		</cfif>

		<!--- cut down the size of the tagContext Array--->
		<cfif structKeyExists (result,"tagContext") and arrayLen(result.tagContext) gt maxLengthOfTagContext>
			<cfset deleteTo = maxLengthOfTagContext + 1>
			<cfloop index="arrayIndex" from = #arrayLen(result.tagContext)# to = #deleteTo# step = -1>
				<cfset arrayDeleteAt (result.tagContext,arrayIndex)>
			</cfloop>
		</cfif>

		<cfreturn result>

	</cffunction>

<!--- WAB 2012-09-2012 renamed this function
	pulled out each check into a separate function with name customCheck_XYZ, and use introspection to run each one
	Makes easier to add new checks here and can also be extended with customer specific checks.
--->
<cffunction name="runCustomChecks">
	<cfargument name="error">
	<cfargument name="errorid" default=0>

		<!--- get array of all function named  customCheck_XYZ --->
		<cfset var functionPointerArray =  getCFCFunctionsByName(this,"customCheck_")>
		<cfset var thisFunctionPointer = '' />

		<cfloop array="#functionPointerArray#" index="thisFunctionPointer">
			<cftry>
				<cfset thisFunctionPointer(error=error,errorid=errorid)>
				<cfcatch>
					<!--- don't want any error in these functions causing problems displaying the error, so ignore.  Probably ought to log something though --->
				</cfcatch>
			</cftry>
		</cfloop>

</cffunction>

<cffunction name="getCFCFunctionsByName" return="array" hint="Returns an array of function pointers where name matches a regexp">
	<cfargument name="component" required="true">
	<cfargument name="regExp" required="true">

		<cfset var functionPointerArray =  []>
		<cfset var functionName = "">

		<!--- get array of all function named  customCheck_XYZ --->
		<cfloop collection="#component#" item="functionName">
			<cfif refindnocase(regExp,functionName)>
				<cfset arrayappend(functionPointerArray,component[functionName])>
			</cfif>
		</cfloop>

	<cfreturn functionPointerArray >
</cffunction>


<cffunction name="CustomCheck_PreparedStatement">
	<cfargument name="error">
	<cfargument name="errorid" default=0>

	<!--- check for particular error types which need particular action --->
	<CFIF (structKeyExists(error,"message") and ERROR.MESSAGE CONTAINS "Could not find prepared statement")	>

				<CFMAIL
					TO="serverSupport@relayware.com"
					FROM="errors@#cgi.http_host#"
					SUBJECT="SQL 'Could not find prepared statement' Error on #cgi.http_host#"
					type="html">

						An error has occurred which is indicative of a problem between CF and SQL <BR>

						The current way of fixing this is to go into CF Admin and reset the datasource Max Pooled Statements from 1000 to 0 and back again, although we don't know exactly what this does (it may be that changing any setting would have the same effect)<BR>

						<cfdump var="#error#">

						<A HREF="http://#cgi.http_host#/errorHandler/etails.cfm?errorid=#ErrorID#" target="error#ErrorID#">Full Details of this Error</A> <BR>

				</CFMAIL>


	</CFIF>

</cffunction>

<cffunction name="CustomCheck_BlockingSPIDs">
	<cfargument name="error">
	<cfargument name="errorid" default=0>

	<!--- check for blocking spids --->

		<CFIF (structKeyExists(error,"message") and ERROR.MESSAGE CONTAINS "exceeded the allowable time limit"  AND ERROR.MESSAGE CONTAINS "query")
				or error.message contains "Execution timeout expired"
				or error.message contains "timeout occurred"
				or refindNoCase("Transaction.*was deadlocked",error.message)
				>


				<cfset checkForDatabaseBlockAsynchronously(errorid = errorid,pageRequested = "#cgi.SCRIPT_NAME#?#cgi.query_string#")>


		</CFIF>

</cffunction>


<cffunction name="CustomCheck_InvalidSession">
	<cfargument name="error">

		<!---
		WAB 2011/03/08
		We occasionally get Session is Invalid Errors reason is unknown
		By resetting the jsessionid cookie, the next request will work OK
		--->
	<cfif (structKeyExists(error,"message") and ERROR.MESSAGE CONTAINS "Session is Invalid")>
		<cfset application.com.globalFunctions.cfcookie(name="JSESSIONID", value="" )>
	</cfif>


</cffunction>

<!--- WAB 2012-09-24
After continuing problems with settings randomly disappearing from memory I try to test for this error and reload the appropriate setting.
 --->
<cffunction name="CustomCheck_MissingSetting">
	<cfargument name="error">
	<cfargument name="errorID" deault="0">

		<cfset var groups={1="key",2="collection"}>
		<cfset var refindResult ="">
		<cfset var parentSettingName ="">
		<cfset var findNodesArray ="">
		<cfset var settingNode ="">
		<cfset var settingName ="">
		<cfset var validateResult ="">
		<cfset var feedback = {}>

		<cfif structKeyExists (error,"message") and isDefined("application")>

			<!--- regExp to look for   element XXX is undefined in YYY., and pull out XXX and YYY --->
			<cfset refindResult = variables.applicationScope.com.regExp.refindAllOccurrences("element (.*) is undefined in (.*)\.",error.message,groups)>

			<cfif arrayLen(refindResult) and not listFindNoCase("attributes,variables,arguments,form,url",refindResult[1].collection)>
				<cfset settingName = refindResult[1].key>
				<!--- look in settings XML for setting with that name --->
				<cfset findNodesArray = xmlsearch(variables.applicationScope.settings.masterxml,"//#lcase(settingname)#")> <!--- search in master XML because settingXML may be corrupted --->
				<cfif arrayLen(findNodesArray)>

					<cfloop array = #findNodesArray# index="settingNode"> <!--- might be more than one node since we just searched for a node name, not a full path) --->
						<cfset var parentName = variables.applicationScope.com.settings.getFullVariableNameFromNode(settingNode.xmlparent)>
						<cfset validateResult = variables.applicationScope.com.settings.validateSettingsXMLByName(parentName)>
					</cfloop>

					<cfset var toAddress = "serverSupport@relayware.com">
					<cfif variables.applicationScope.testsite is 2>
						<cfset toAddress = "William@relayware.com">
					</cfif>

						<cfmail from="serverSupport@relayware.com" to="#toAddress#" subject="Possible Setting Problem: #cgi.server_name#. #settingName#" type="HTML">
							<cfoutput>
							A possible missing setting has been detected!<br />
							Error Message: #error.message#<br />
							Error ID: #getURLOfErrorDetailsFile()#?errorID=#errorID#<br />
							Setting #parentSettingName# has been Verified with the following result<br />
							#validateResult#
							</cfoutput>
						</cfmail>
				</cfif>
			</cfif>

		</cfif>

</cffunction>

<cffunction name="CustomCheck_SettingsError">
	<cfargument name="error">
	<cfargument name="errorID" deault="0">

		<cfset var groups={1="key",2="collection"}>
		<cfset var refindResult ="">
		<cfset var parentSettingName ="">
		<cfset var findNodesArray ="">
		<cfset var settingNode ="">
		<cfset var settingName ="">
		<cfset var validateResult ="">
		<cfset var feedback = {}>

		<cfif structKeyExists (error,"message")  and error.message is ""  and getErrorFileAndLineFromTagContext(error.tagcontext).filename is "\Relay\com\settings.cfc">

			<cfset validateResult = variables.applicationScope.com.settings.validateSettingsXML()>

			<cfset var toAddress = "serverSupport@relayware.com">
			<cfif variables.applicationScope.testsite is 2>
				<cfset toAddress = "William@relayware.com">
			</cfif>
						<cfmail from="serverSupport@relayware.com" to="#toAddress#" subject="Error in Settins.cfc: #cgi.server_name#." type="HTML">
							<cfoutput>
							An error has been detected in settings.cfc!<br />
							Error Message: #error.message#<br />
							Error ID: #getURLOfErrorDetailsFile()#?errorID=#errorID#<br />
							The XML has been validated with the following result<br />
							#validateResult#
							</cfoutput>
						</cfmail>
		</cfif>

</cffunction>

<!--- WAB 2012-09-24
After continuing problems with security document getting randomly corrupted I try to test for this error and reload the XML document.
Error always seems to be .internalRequired in FileSecurityRequirement for index.cfm.
 --->

<cffunction name="CustomCheck_SecurityXMLCorruption">
	<cfargument name="error">
	<cfargument name="errorID" deault="0">

	<cfif structKeyExists(error,"tagContext") and arrayLen(error.tagContext) and error.tagContext[1].raw_trace contains "APPLYSECURITYREQUIREMENTS">

			<cfset variables.applicationScope.com.security.loadXML(refresh=true)>

					<cfmail from="serverSupport@relayware.com" to="serverSupport@relayware.com" subject="Security XML Automatically Reloaded: #cgi.server_name#" type="HTML">
					<cfoutput>
					Error Message: #error.message#<br />
					Error ID: #errorID#<br />
					Security XML has been Reloaded
					</cfoutput>
					</cfmail>


	</cfif>

</cffunction>

<cffunction name="checkForDatabaseBlockAsynchronously">
     <cfargument name="errorid" default="0">
      <cfargument name="pageRequested" default="">

	<cfset var aSynchArgs = {appname=applicationScope.applicationname,componentName="errorHandler",methodname="checkForDatabaseBlock",protocolAndDomain=getProtocolAndDomain(),errorid=errorid,pageRequested=pageRequested}>
				<cftry>
					<cfset SendGatewayMessage("relayAsynchronousGateway",aSynchArgs)>
					<cfcatch>
						<!--- TODO - maybe need a error logged here --->
					</cfcatch>
				</cftry>
</cffunction>

<cffunction name="checkForDatabaseBlock">
    <cfargument name="errorid" default="0">
    <cfargument name="pageRequested" default="">
	<!--- need these twoarguments because we don't have request.currentsite when function called asynchronously--->
	<cfargument name="protocolAndDomain" default="#getProtocolAndDomain()#">

		<cfset var blockdetails = "">

		<cftry>

			<cfset blockdetails = applicationScope.com.dbtools.getDatabaseBlocks(datasource=applicationScope.sitedatasource)>

			<cfif blockDetails.hasBlock>


					<CFMAIL
						TO="serverSupport@foundation-network.com"
						FROM="errors@#hostname#"
						SUBJECT="SQL Block #hostname#"
						type="html">
						Page requested = #pageRequested# <BR>
						ErrorID <a href="#getURLOfErrorDetailsFile(protocolAndDomain=protocolAndDomain)#?errorID=#errorID#" target="errorWindow">#ErrorID#-#applicationScope.instance.databaseid#</a> <BR>
						<cfdump var="#BlockDetails#">
					</CFMAIL>

			</cfif>
		<cfcatch>

					<CFMAIL
						TO="serverSupport@foundation-network.com"
						FROM="errors@#hostname#"
						SUBJECT="Problem testing for SQL Block #hostname#"
						type="html">
						Page requested = #pageRequested#			<BR>
						ErrorID <a href="#getURLOfErrorDetailsFile(protocolAndDomain = protocolAndDomain)#?errorID=#errorID#" target="errorWindow">#ErrorID#-#applicationScope.instance.databaseid#</a><BR>
						<cfdump var="#cfcatch#">
					</CFMAIL>



		</cfcatch>
	</cftry>

</cffunction>




  <cffunction name="getCurrentUserStruct">

    <cfset var currentUser = "">
    <cfset var personStruct = "">
    <cfset var errorStruct = "">

    <cfif structKeyExists(request,"relayCurrentUser")>
      <cfset currentUser = request.relayCurrentUser>
    <cfelseif doesSessionRelayCurrentUserExist()>
		<!--- error must have occurred before request.currentuser created, but we know that there is an equivalent session structure--->
      <cfset currentUser = session.relayCurrentUserStructure>
    <cfelse>
		<!--- This error has occurred very early in the boot sequence. We have to create a dummy current user structure --->
		<cfset personStruct = {firstname="Unknown", lastname="User",languageId=1,countryID=0,personid = 0, person= structNew(),email=""}>
		<cfset personStruct.fullName = personStruct.firstName & " " &  personStruct.lastName >
		<cfset personStruct.person.email = "">
      	<cfset errorStruct = {showDump = false,showLink = false}>
      	<cfif areApplicationVariablesLoaded() and applicationScope.testsite is 2>
     	     <cfset errorStruct.showDump = true>
     	</cfif>
      	<cfset currentUser = {personid=0, errors = errorStruct, SupportEmailAddress =  "errors@foundation-network.com", userGroupId=0, person=personStruct, languageid = 1, countryid = 0,fullname =  personStruct.fullName, visitid = 0}>

	 </cfif>

    <cfreturn currentUser>

  </cffunction>

  <cffunction name="getPreviousPagesArray">
	<cfargument name="userStruct" default = "#getCurrentUserStruct()#">
	<cfargument name="maxPages" default = "5">

		<cfset var result = {}>
		<cfset var to = maxPages + 1>
		<cfset var i = '' />

			<cfif structKeyExists (userStruct,"previouspages")>
				<cfset result.previouspages = userStruct.previouspages>
				<cfloop index="i" to ="#to#" from="#arrayLen(result.previouspages)#" step="-1">
					<cfset arrayDeleteat(result.previousPages,i)>
				</cfloop>
			</cfif>

	<cfreturn result>

  </cffunction>

  <cffunction name="makeSmallUserStruct">
	<cfargument name="userStruct" default = "#getCurrentUserStruct()#">

		<cfset var result = {}>
		<cfset var key = "">
		<cfset var i = 0>
		<cfset var KeysToKeep = "fullname,HTTP_ACCEPT_LANGUAGE,ISLOGGEDIN,LANGUAGEID,LANGUAGEISOCODE,locationid,personid,organisationid,visitid,countryid,countryisocode,currentPage">

		<cfloop list="#KeysToKeep#" index="key">
			<cfif structKeyExists (userStruct,key)>
				<cfset result[key] = userStruct[key]>
			</cfif>
		</cfloop>


			<cfset result.person = {}>
			<cfset result.location = {}>
			<cfset result.organisation = {}>

			<cfif structKeyExists (userStruct,"person") and structKeyExists (userStruct.person,"email")>
				<cfset result.person.email = userStruct.person.email>
			</cfif>
			<cfif structKeyExists (userStruct,"location") >
				<cfset result.location.sitename = userStruct.location.sitename>
			</cfif>
			<cfif structKeyExists (userStruct,"organisation") >
				<cfset result.organisation.organisationName = userStruct.organisation.organisationName>
			</cfif>


			<cfif structKeyExists (userStruct,"content")>
				<cfset result.content = {}>
				<cfset result.content.showforcountryid = userStruct.content.showforcountryid>
			</cfif>

		<cfreturn result>

  </cffunction>


	<cffunction name="doesSessionRelayCurrentUserExist">
	<!--- WAB need a separate function to do this because if session scope is not enabled (eg when called from clustermanagement) then falls over and an error needs to be caught --->

		<cfset var result = false>

		<cftry>
			<cfif isdefined("session") and structKeyExists(session,"relayCurrentUserStructure")>
				<cfset result = true>
			</cfif>
			<cfcatch>
				<cfset result = false>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>



  <cffunction name="insertRelayErrorRecord">
      <cfargument name="source" default="">
      <cfargument name="Type" default="">
      <cfargument name="userStruct" default="#getCurrentUserStruct()#">
	  <cfargument name="template" default = "#cgi.SCRIPT_NAME#?#cgi.query_string#">
	  <cfargument name="referrer" default = "#cgi.HTTP_REFERER#">
      <cfargument name="diagnostics" default = "">
      <cfargument name="fileName" default = "">
      <cfargument name="line" default = "0" type="numeric">  <!--- WAB 2014-11-03 CASE 442481 Add type, SQL injection problem --->
      <cfargument name="dataStruct" default = "#StructNew()#">
      <cfargument name="AddToDataStruct" default = "form,url,user">  <!--- List from FORM,URL,USER --->
      <cfargument name="sendEmail" default = "false">
	 <cfargument name="TTL" default = "60">

      <cfset var dataString = "">
      <cfset var insert = "">
      <cfset var updateScheduledTaskLog = "">
      <cfset var errorID = 0>


		<!--- Add useful things to the dataStruct  --->
		<cfif listFindNoCase(AddToDataStruct,"form") and isDefined("form") and structCount(form)>
			<cfset arguments.dataStruct.form = form>
		</cfif>
		<cfif listFindNoCase(AddToDataStruct,"url") and isDefined("url") and structCount(url)>
			<cfset arguments.dataStruct.url = url>
		</cfif>
		<cfif listFindNoCase(AddToDataStruct,"user")>
			<cfset arguments.dataStruct.user = makeSmallUserStruct(userStruct)>
		</cfif>
		<cfif listFindNoCase(AddToDataStruct,"previousPages")>
			<cfset arguments.dataStruct.previousPages = getPreviousPagesArray(userStruct)>
		</cfif>


		<cfif isStruct(dataStruct)>
        	<!--- WAB 2012-10-16 change to JSON encoding (much smaller) <CFWDDX ACTION="CFML2WDDX" INPUT="#dataStruct#" OUTPUT="dataString"> --->
			<cfset dataString = serializeJSON(dataStruct)>
    	</cfif>

      <cfif isdefined("ApplicationScope") and structKeyExists (ApplicationScope,"siteDataSource")>
	        <cftry>
	          <CFQUERY name = "insert"  datasource="#applicationScope.SiteDataSource#">
		       <!--- WAB 2014-11-03 CASE 442481 Add CFQUERYPARAM, SQL injection problem on LineNumber field but did whole query --->
	          insert into RelayError
	          (Personid,RemoteAddress,Browser,domain,Template,Referer,Diagnostics,filename,linenumber,dataStruct,serverName,instanceName,source,type,relayversion,TTL,visitid)
	          values (
	            <cfqueryparam value="#userStruct.Personid#" CFSQLTYPE="CF_SQL_INTEGER" >,
	            <cfqueryparam value="#cgi.Remote_Addr#" CFSQLTYPE="CF_SQL_VARCHAR">,
	            <cfqueryparam value="#left(cgi.http_user_agent,100)#" CFSQLTYPE="CF_SQL_VARCHAR">,
	            <cfqueryparam value="#left(cgi.http_host,100)#" CFSQLTYPE="CF_SQL_VARCHAR">,
	            <cfqueryparam value="#left(template,150)#" CFSQLTYPE="CF_SQL_VARCHAR">,
	            <cfqueryparam value="#left(referrer,200)#" CFSQLTYPE="CF_SQL_VARCHAR">,
	            <cfqueryparam value="#left(Diagnostics,4000)#" CFSQLTYPE="CF_SQL_VARCHAR">,
	            <cfqueryparam value="#left(filename,200)#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#line#" CFSQLTYPE="CF_SQL_INTEGER" >  ,
	            <cfqueryparam value="#dataString#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#applicationScope.instance.servername#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#applicationScope.instance.name#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#arguments.source#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#arguments.type#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#left(applicationScope.instance.relaywareVersionInfo.version,100)#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cfqueryparam value="#arguments.TTL#" CFSQLTYPE="CF_SQL_INTEGER">,
				<cfqueryparam value="#userStruct.visitid#" CFSQLTYPE="CF_SQL_INTEGER">
				)
	          select scope_identity() as relayErrorID
	          </cfquery>

			<cfset errorID = insert.relayErrorID>

	          <cfcatch>
					<cfif applicationScope.testsite is 2>
						<cfdump label="This only appears on Dev Site!" var="#cfcatch#">
					<cfelse>

						<cfmail to="serverSupport@relayware.com"  from="serverSupport@relayware.com" subject="Error in Error Handler #cgi.server_Name#" type="html">
							The following Error Occurred
							<cfdump var="#cfcatch#">
							When trying to process this error
							<cfdump var="#dataStruct#">
						</cfmail>

					</cfif>
					<!--- will just continue with errorid = 0 --->

	          </cfcatch>
	        </cftry>

	          <!--- NJH 2008/10/13 CR-TND562 - log the error ID in the scheduledTaskLog table if the error thrown was from a scheduled task
	          	WAB 2015-06-22 Modified code to update scheduledTaskLog
	          						in case of uncaught error is now done in handleError() function
	          					and in case of caught errors is done in cf_scheduledTaskLog
	          					here we just record the ids of the errors/warnings caught during the request
	          --->
	          <cfif structKeyExists(request,"scheduledTask")>
				<cfset arrayAppend(request.scheduledTask.errorsLogged, errorid)>
	          </cfif>


			<!--- 2014-11-05 WAB only send CF errors immediately - try and reduce the load on the thread system.
					May be this isn't worth doing at all - no one ever looks at them!
					2015-11-10 WAB removed entirely
			<cfif errorID is not 0 and source is "COLDFUSION">
				<cfset sendSingleErrorToHQASynchronously (relayErrorid = ErrorID)>
			</cfif>
			 --->


          	<cfreturn ErrorID>

        <cfelse>

			<cfmail subject="Relayware Server Startup Error: #cgi.http_host#" from ="serverSupport@relayware.com" to ="serverSupport@relayware.com" type="html">
				This error occured while application variables were not available
				<cfdump var="#dataStruct#">
				<cfdump var="#cgi#">
				<cfdump var="#createObject('component','com.clustermanagement').getInstanceDetails()#">
			</cfmail>

          <cfreturn 0>

        </cfif>



  </cffunction>

	<cffunction name="sendErrorEmail">
		<cfargument name="emailTo" default = "errors@relayware.com">   <!--- This ought to be a setting with different defaults for dev/test sites (can't do this yet!))--->
		<cfargument name="errorID" required="true">
		<cfargument name="error" required="true">
		<cfargument name="message" default="">

		<cfset var subject = "">
		<cfset var currentuser = "">

		<cfif applicationScope.TestSite>
			<cfset arguments.emailTo = "sandbox@relayware.com">
		</cfif>

		<cfset subject = "Error #cgi.http_host#. ">

		<cfif message is not "">
			<cfset subject = subject & message>
		<cfelse>
			<cfset subject = subject & extractTemplateInfoFromErrorStructure (error).relativeFileName>
		</cfif>

		<cfset currentUser = getCurrentUserStruct ()>

			<CFMAIL
				TO="#arguments.emailTo#"
				FROM="errors@#cgi.http_host#"
				SUBJECT="#subject#"
				type="html">
				<A HREF="#getURLOfErrorDetailsFile()#?errorid=#errorID#" target="error#errorID#">Full Details on Local Machine</A>
				<BR>
				<A HREF="http://relayAdmin/relayRemoteAdmin/viewerror.cfm?errorid=#errorID#&databaseid=#applicationScope.instance.databaseid#" target="error#errorID#">Full Details Relayware</A>

				#outputUserDetails(currentUser)#

				#outputErrorWithDebug(error = error,noDump = true)#

			</cfmail>

	</cffunction>


	<!--- WAB 2015-06-22 	Added functionality to set an email address to which to send details of on any error in a request
							Initially used in cf_scheduledTask, but generally applicable
	--->
	<cffunction name="setOnError">
		<cfargument name="EmailTo" required="true" type="string">
		<cfargument name="Message" required="false" default = "" type="string">

		<cfset request.OnError = arguments>

	</cffunction>


	<cffunction name="areApplicationVariablesLoaded">

		<!--- for errors which occur very early in the boot sequence (which shouldn't happen but does sometimes) we may not even have application variables --->
		<!--- assume if this doesn't exist then the other ones won't exist --->
		<cfreturn iif(isDefined("applicationScope.paths"),true,false)>

	</cffunction>


	<cffunction name="getProtocolAndDomain">

		<cfset var ProtocolAndDomain = "">
		<cfif isDefined("request.currentsite")>
			<cfset ProtocolAndDomain = request.currentsite.protocolAndDomain>
		<cfelse>
			<cfset ProtocolAndDomain = "http" & iif(SERVER_PORT_SECURE,de("s"),de("")) & "://" & http_host>
		</cfif>

		<cfreturn ProtocolAndDomain>

	</cffunction>


	<cffunction name="getURLOfErrorDetailsFile">
		<cfargument name="protocolAndDomain" type="String" default = "#getProtocolAndDomain()#">

		<!--- WAB 2012-10-05 Problem with links to errorDetails on external sites giving 'must be internal user'. Link needs to go to internal domain, not sure whether request.currentsite.internalDomain always defined at this point, so doing an isdefined
			  WAB 2012-10-10 No it doesn't!  ErrorsDetails.cfm has its own security, so answer to the problem is to open up ErrorsDetails.cfm in the directory security
		 --->

		<cfset var result = ProtocolAndDomain & "/errorHandler/errorDetails.cfm">
		<cfreturn result>

	</cffunction>


	<cffunction name="sendSingleErrorToHQAsynchronously">
		<cfargument name="relayErrorID" type="numeric" required = "true">

			<!--- WAB 2012-12-13 no longer need to use the asynchronous gateway - use a thread instead.
			However, since threads cannot create threads, this cannot be done if we are already in a thread.  In this case, just skip the code, the error will be sent to HQ by the regular housekeeping task which mops up unsent errors.
			 --->
			<cfset var feed = {relayErrorID = relayErrorID}>

			<cfif not isDefined("thread")>

				<cfthread action="run" name="errorThread#createuuid()#"  feed=#feed#>
					<cfset copyErrorsToHQ(relayErrorID = feed.relayErrorID)>
				</cfthread>

			</cfif>

	</cffunction>

	<cffunction name="copyErrorsToHQ">
		<cfargument name="numberToSend" default="10">
		<cfargument name="relayErrorID" type="numeric" default="0">


		<cfset var getErrors = "">
		<cfset var updateErrors = "">
		<cfset var errorsWDDX = "">
		<cfset var getDBDetails = "">
		<cfset var args = "">
		<cfset var tempRelayErrorID = "">
		<cfset var result = {}>

			
		<!--- 2015-02-13 GCC Added with(nolock) 
			2017-02-21 WAB 	PROD2016-3475  Push more serious errors first
		--->
		<cfquery name="getErrors" datasource ="#applicationScope.siteDataSource#">
		select top #numberToSend# *
		from relayError with(nolock)
		where remoteerrorID is null
		<cfif relayErrorID is not "0">
			and relayErrorID = #relayErrorID#
		</cfif>
		ORDER BY 
			CASE WHEN source IN ('COLDFUSION','ERROR') THEN 1 ELSE 0 END desc
			, errordatetime desc
		</cfquery>

		<cfquery name="getDBDetails" datasource ="#applicationScope.siteDataSource#">
		select @@serverName as databaseServer, db_name()as databaseName

		</cfquery>

		<cfif getErrors.recordCount>
				<!--- WAB 2015-11-10 minimise dataStruct before sending to heartbeat --->
				<cfloop query="getErrors">
					<cfset querySetCell (getErrors,"dataStruct",minimiseDataStructJSON(dataStruct),currentRow)>
				</cfloop>

				<cfwddx action="cfml2wddx" input="#getErrors#" output="errorsWDDX">
								 <cfset args = {
						 				databaseServer= getDBDetails.databaseServer,
						 				databaseName = getDBDetails.databaseName,
						 				errorsWDDX = errorsWDDX	}>
						<!--- WAB 2011/04/18 LID6336 needed to add a try, otherwise if heartbeat is down, we get a cascade of errors which brings the server to its knees --->
						<cftry>
							<cfinvoke
								webservice="#applicationScope.com.clusterManagement.heartbeatServerAndProtocol#/WebServices/registration.cfc?wsdl"
								method = "reportErrors"
								timeout = "15"
								args = 	"#args#"
								returnVariable = "result"
									refreshwsdl = "true"
							>


							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.message = "Error Calling Heartbeat Server. " & cfcatch.message>
							</cfcatch>
						</cftry>

							<cfif result.isOK>
								<cfif structcount(result.remoteid)>
									<cfquery name="updateErrors" datasource ="#applicationScope.siteDataSource#">
										<cfloop item="tempRelayErrorID" collection="#result.remoteid#">
										update relayError set remoteErrorID = 	#result.remoteid[tempRelayErrorID]# where relayErrorID = #tempRelayErrorID#
										</cfloop>
									</cfquery>
								</cfif>
							</cfif>




		<cfelse>
			<!---	2017-02-21 WAB 	PROD2016-3475 tell heartbeat that there are no errors to report --->
			<cftry>
				 <cfset args = {
			 				databaseServer= getDBDetails.databaseServer,
			 				databaseName = getDBDetails.databaseName}
			 	>
			 				
				<cfinvoke
					webservice="#applicationScope.com.clusterManagement.heartbeatServerAndProtocol#/WebServices/registration.cfc?wsdl"
					method = "reportNoErrors"
					timeout = "15"
					args = 	"#args#"
					returnVariable = "result"
					refreshwsdl = "true"
				>

				<cfcatch>
					<!--- Not really a worry if this fails --->
				</cfcatch>
			</cftry>

			<cfset result={isOK=true,message="No Items to Process"}>

		</cfif>

	<cfreturn result>

	</cffunction>

	<!--- WAB 2015-11-10 Minimise the size of the datastruct so that we can continue to send some information back to heartbeat without overloading the system --->
	<cffunction name="minimiseDataStructJSON">
		<cfargument name="dataStructJSON" type="string" hint="serialised dataStructString">

		<cfset var result = "">
		<cfset var keysToDelete  = "previouspages,user,sourcefile">
		<cfset var key1 = "">
		<cfset var key2 = "">
		<cfset var value = "">

		<cfif dataStructJSON is not "" and isJSON (dataStructJSON)>
			<cfset var dataStruct = deserializeJSON (dataStructJSON)>

			<cfloop collection ="#dataStruct#" item = "Key1">
				<cfif listfindnocase (keysToDelete,Key1)>
					<cfset structDelete (dataStruct,Key1)>
				<cfelseif Key1 is "cferror">
					<cfset dataStruct[key1] = minimiseErrorStruct (dataStruct[key1])>
				<cfelse>
					<!--- sometimes we get 'undefined' value which throw errors here, so catch and delete --->
					<cftry>
						<cfset value = dataStruct[key1]>
						<cfcatch>
							<cfset value = "">
							<cfset structDelete (dataStruct,Key1)>
						</cfcatch>
					</cftry>
					<cfif isStruct (value)>
						<!--- loop through collection deleting anything more than 1 level down or more than 100 characters --->
						<cfloop collection ="#value#" item = "Key2">
							<cfif not isValid ("String" ,dataStruct[key1][key2]) >
								<cfset dataStruct[key1][key2] = "deleted">
							<cfelseif len(dataStruct[key1][key2]) GT 100>
								<cfset dataStruct[key1][key2] = left (dataStruct[key1][key2],100) & " ...">
							</cfif>
						</cfloop>
					</cfif>
				</cfif>

			</cfloop>

			<cfset result = serializeJSON (dataStruct)>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="minimiseErrorStruct" hint="minimises error structure - for sending to Heartbeat, called by minimiseDataStructJSON">
		<cfargument name="errorStruct" type="struct" hint="">

		<cfset var keysToDelete = "suppressed,column,knowncolumn,knownline">
		<cfset var key = "">

			<cfloop collection ="#errorStruct#" item = "Key">
				<cfif listfindnocase (keysToDelete,Key)>
					<cfset structDelete (errorStruct,Key)>
				<cfelseif key is "tagContext" and arrayLen(errorStruct.tagContext) gt 0>  <!--- NJH 2016/01/14 - check that tag context has an element 1 before getting it. --->
					<cfset errorStruct[key] = errorStruct.tagContext[1]>
				<cfelseif not isDefined ("errorStruct.#key#") >
					<cfset errorStruct[key] = "Deleted - Not Defined">
				<cfelseif not isValid ("String" , errorStruct[key]) >
					<cfset errorStruct[key] = "Deleted">
				<cfelseif len(errorStruct[key]) GT 100>
					<cfset errorStruct[key] = left (errorStruct[key],100) & " ...">
				</cfif>
			</cfloop>

		<cfreturn errorStruct>

	</cffunction>


	<cfset variables.siteWideErrorHandler = {mapping = "/sitewideerrorhandler",relativePath="/errorHandler",filename="sitewideerrorhandler.cfm"}>

	<!--- 2016-01-13 WAB	CFAdmin Password now stored in db, so cfpassword argument now optional --->
	<cffunction name="checkSitewideErrorHandlerMappingAndSetting">
		<cfargument name="cfpassword" required="false">

		<cfset var result = {isOK = false,message="OK"}>
		<cfset var checkMapping = "">
		<cfset var checkSetting = "">

		<cfset checkMapping = checkSitewideErrorHandlerMapping (argumentCollection = arguments)>

		<!--- now check setting --->
		<cfset checkSetting = checkSitewideErrorHandlerSetting (argumentCollection = arguments)>

		<cfset result.isOK = checkMapping.isOK AND checkSetting.isOK>
		<cfset result.message = checkMapping.message & "<BR>" & checkSetting.message >

		<cfreturn result>

	</cffunction>


	<cffunction name="checkSitewideErrorHandlerMapping">
		<cfargument name="cfpassword" required="false">
		<cfset var result = {isOK = false,message="OK"}>
		<cfset var path = "">
		<cfset var checkMapping = applicationScope.com.cfadmin.callFunction(module = "extensions",method="getmappings", argumentCollection = arguments)>
		<cfif checkmapping.isOK>
			<cfif structKeyExists (checkMapping.result,siteWideErrorHandler.mapping)>
				<!--- check directory and that file exists, remember that because we can only have one mapping per CF instance, the template may not actually be in the core directories for this particular application, but as long as the file exists we are OK--->
				<cfset path = checkMapping.result[siteWideErrorHandler.mapping] & "/" & siteWideErrorHandler.filename>
				<cfif fileExists (path)>
					<cfset result.isOK = true>
					<cfset result.message = "Mapping #siteWideErrorHandler.mapping# set to #checkMapping.result[siteWideErrorHandler.mapping]#">
				<cfelse>
					<cfset result.message= "Mapping Exists, but file #path# does not exist">
				</cfif>
			<cfelse>
				<cfset result.message = "No mapping for #siteWideErrorHandler.mapping#">
			</cfif>
		<cfelse>
			<cfset result.message = checkMapping.message>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getCorrectSiteWideErrorHandlerValue">
		<cfreturn variables.siteWideErrorHandler.mapping & "/" & variables.siteWideErrorHandler.filename>
	</cffunction>

	<cffunction name="getCorrectSiteWideErrorHandlerMappingPath">
		<cfreturn applicationScope.paths.relay & siteWideErrorHandler.relativepath>
	</cffunction>


	<cffunction name="checkSitewideErrorHandlerSetting">
		<cfargument name="cfpassword" required="false">
				<cfset var result = {isOK = false,message="OK"}>
				<cfset var checkSetting = applicationScope.com.cfadmin.callFunction(module = "runtime",method="GETRUNTIMEPROPERTY",propertyName="SiteWideErrorHandler",argumentCollection = arguments)>

					<cfif not checkSetting.isOK>
						<cfset result.isOK = false>
						<cfset result.message = checkSetting.message>
					<cfelseif checkSetting.result is getCorrectSiteWideErrorHandlerValue() >
						<cfset result.isOK = true>
						<cfset result.message = "Site-Wide Error Handler set to #checkSetting.result#">
					<cfelse>
						<cfset result.message= "Site-Wide Error Handler set to #checkSetting.result#.  Should be #getCorrectSiteWideErrorHandlerValue()#">
					</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="setSitewideErrorHandlerMappingAndSetting">
		<cfargument name="cfpassword" required="false">

		<cfset var result = "">
		<cfset var checkSetting = "">
		<cfset var checkMapping = checkSitewideErrorHandlerMapping (argumentCollection = arguments)>

		<cfif not checkMapping.isOK>
			<cfset variables.applicationScope.com.cfadmin.callFunction(module = "extensions",method="setmapping",mapname=siteWideErrorHandler.mapping,mappath=getCorrectSiteWideErrorHandlerMappingPath(),argumentCollection = arguments)>
		</cfif>

		<cfset checkSetting = checkSitewideErrorHandlerSetting (argumentCollection = arguments)>
		<cfif not checkSetting.isOK>
			<cfset variables.applicationScope.com.cfadmin.callFunction(module = "runtime",method="sETRUNTIMEPROPERTY",propertyName="SiteWideErrorHandler",	propertyValue = getCorrectSiteWideErrorHandlerValue())>
		</cfif>


		<cfset result = checkSitewideErrorHandlerMappingAndSetting (argumentCollection = arguments)>

		<cfreturn result>

	</cffunction>



<cffunction name="cleanUpErrorLogs">

	<cfset var numberDeleted = -1>
	<cfset var deleteRecords = -1>
	<cfset var maxLoopsPerRequest = 10>
	<cfset var maxToDeletePerLoop = 1000>
	<cfset var counter = 0>
	<cfset var thisItem = "">
	<cfset var totalNumberDeleted = 0>
	<cfset var thisErrorCleanUp = "">
	<cfset var result = {isOK = true,numberDeleted = 0}>
	<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

		<cfset numberDeleted = -1>
		<cfset counter = 0>
		<cfloop condition="numberDeleted is not 0 and counter lt maxLoopsPerRequest">
			<cfset counter ++>
			<cfset applicationScope.com.request.extendTimeOutIfNecessary(seconds = 120,applicationscope = applicationscope)><!--- WAB 2012-09-20 Fix here - I had moved this function to a different CFC --->
			<cfquery name="deleteRecords" datasource="#applicationScope.SiteDataSource#">
				delete relayerror from relayerror
				inner join (
						select top #maxToDeletePerLoop# relayerrorid from relayerror
						where
						 datediff (d,errorDateTime,Getdate()) > TTL
						) as x
				on x.relayerrorid  = relayerror.relayerrorid
				select @@rowcount as deleted
			</cfquery>
			<cfset numberDeleted = deleteRecords.deleted>
			<cfset result.NumberDeleted = result.NumberDeleted  + NumberDeleted >
		</cfloop>


	<cfreturn result>

</cffunction>


</cfcomponent>

