<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB May 2006

Functions for creating the request.currentsite structure
	and the currentTree

2007-03-14	WAB rejig the memory structures
2007-03-14	WAB work out the internal style sheet
2007-04-07	WAB continue to rejig
2007-05-31	WAB corrected a bug loading sitedefinitions (firstpass not being set to false after first pass!)
2007-06-06	WAB added suffix to the name of the site (dev/test)
2007-07-16 	WAB testing https login
2007/12/10	WAB fixed a bug in the code which ensures backwards compatability of sitedefs.  Was assuming that the maximum sitedefid was getsitedefid.recordcount (which ignored the fact that rows might be missing in the sitedef table   ie max(ident) <> recordcount
2007/12/11	WAB currentsite.livelanguageids defaults to request.currentSite.liveLanguageIDs and create currentsite.livelanguages from it
2008/01/25	WAB add httpProtocol to currentSiteStructure
2008/02/05	WAB added some functions for accessing sitedef structure (other than current one)
2008/07/17 	WAB Fixed Problem with internalDomain being set wrongly - caused problems when JS popups used iDomainAndRoot variable
2008/11/26 	WAB added getSitedefs function for use in SSO
2009/02/03 	WAB LID 1745  mod to qryGetSiteDefs, error when no domains for a particular site - only apparent on SSO page
2009/03/03	WAB added server port to the site structure
2009/03/03 	WAB/NJH  Lighthouse Issue All Sites 1739 - getExternalSiteURL function
2009/06/23	WAB LID 2398 Problem with .internalDomain not getting set.  Fixed and removed all code dealing with backwards compatibilty to non-sitedef dbs
2009/06/24	WAB improved so that didn't casue an exception during boot up 		got rid of references to application.com
2009/09/07	WAB SOPHOS LID 2512 store the IP address that CGI.SERVER_NAME resolves to as IPAddressOfSwitch  [OK, there won't always be a switch, but is only really applicable if there is one]
2009/10/21	WAB added site.serverInstanceUniqueID to structure
2013-03-12	WAB	Deal with a long standing issue which crops up when dbs are moved from live to staging sites.  The external URLs references in comms are not valid on the non live site and causes problems when trying to get the protocol.
2013-04-30	WAB	Improve behaviour when trying to get siteDef for a domain which turns out not to be a relayware one - don't keep reloading structure into memory unnecessarily
2013-05-20 	WAB add a generic noReply email address to the siteDef structure
2013-10-14 	WAB Modified so that request.currentSite structure contains the siteDefDomainID.  This involved changing the structure of application.siteDefIndex
2013-10-28	WAB Fixed an error introduced when doing the above - reinstate domains field returned from getSiteDefs()
2015-12-04	RJT Provision of methods to provide UI friendly version strings and changes related to the change to Version.cfm to hold a struct rather than a string as we parse it to create one anyway
--->


<cfcomponent displayname="relayCurrentSite" hint="">

	<cffunction name="loadSiteDefinitions" access="public" hint="load siteDefinitions into memory">
		<cfargument name="updateCluster" type="string" required="no" default="true">
		<cfargument name="applicationScope" default="#application#">

		<cfset var tempSiteStruct = structNew()>
		<cfset var tempSiteStructBySiteId = structNew()>
		<cfset var tempSiteDefIndex = structNew()>
		<cfset var tempSiteDefIndexByName = structNew()>

		<cfset var checkForSiteDefs = "">
		<cfset var parametersStruct = "">
		<cfset var domain = "">
		<cfset var site = "">
		<cfset var row= "">
		<cfset var firstpass = true>
		<cfset var SiteDefs  = "">
		<cfset var thisSiteDefID  = 0>
		<cfset var getLiveLanguages = '' />
		<cfset var structureFunctionsObj = createObject("component","relay.com.structureFunctions")> <!---  WAB 2009/06/24	 allows this function to run during boot up when com not yet in memory --->
		<cfset var regExpFunctionsObj = createObject("component","relay.com.regExp")>
		<cfset var noReplyDomain = "">
		<cfset var qryGetDomains = "">

		<cflock timeout="5" throwontimeout="Yes" name="loadSiteDefinitions_#applicationScope.applicationname#" type="EXCLUSIVE">


				<!--- WAB 2008/11/26 converted query to function --->
				<cfset SiteDefs = getSiteDefs (applicationScope = applicationScope)>



			<!---
			WAB 2009/06/23 LID 2398 all sites now on sitedef code
			WAB 2009/06/24	 got rid of references to application.com
			<cfif SiteDefs.recordcount is not 0>
			--->
			<!---
				<cfparam name="application.internal UserDomains" default = "">
				<cfparam name="application.external UserDomains" default = "">
			--->

				<cfloop query="SiteDefs">
					<cfset row = structureFunctionsObj.queryRowToStruct (query = SiteDefs,row=currentrow,excludecolumns="domains,domainsdev,domainslive,domainstest,parameters")>
					<cfset row = structureFunctionsObj.convertFlatStructureToStructureOfStructures (row,"_")>


					<cfset parametersStruct = regExpFunctionsObj.convertNameValuePairStringToStructure(inputstring=parameters,delimiter=",")>
					<cfset parametersStruct = structureFunctionsObj.convertFlatStructureToStructureOfStructures (parametersStruct,"_.")>

					<cfset row = structureFunctionsObj.structMerge (row,parametersStruct)>


					<cfset site = createDefaultSiteDef(isInternal = SiteDefs.isInternal,applicationScope = applicationScope)>
					<cfset site = structureFunctionsObj.structMerge (struct1 = site, struct2 = row, dontMergeNulls = true)>
					<!--- set stylesheet if blank --->
					<cfif not site.isInternal and site.stylesheet is "">
						<cfset site.stylesheet = calculateStyleSheetFromBorderSet(borderset = site.borderset,applicationScope = applicationscope)>
					</cfif>

					<!---  For external sites, need to work out reciprocal internal site--->
					<cfif not site.isInternal>
						<cfif site.reciprocalSite is "">
							<cfoutput>Reciprocal site not set in siteDef table</cfoutput><CF_ABORT>
						</cfif>
							<cfset site.internalDomain = listFirst(tempSiteStructBySiteId[site.reciprocalSite].domainList)>
					</cfif>

					<!--- WAB added these suffixes 2007-06-06
						2013-02-25	WAB Add test for Demo Sites, and don't show suffix in title
					--->

					<cfif applicationScope.TestSite and not isEnvironment('Demo')>
						<cfset Site.Title = Site.Title & " | " & getEnvironmentAbbreviation(applicationScope=applicationScope)>
					</cfif>

					<cfset Site.domainAndRoot = Site.domain>
					<cfset Site.protocolAndDomain = site.httpprotocol & Site.domain>

					<!--- 2013-05-2013 WAB add a generic noReply email address, uses the mainDomain of the site (except on devSites) (so that emails from scheduled processes don't come from IPAddresses or other oddly named servers) --->
					<cfset noReplyDomain = site.maindomain>
					<cfif isEnvironment('Dev')>
							<cfset noReplyDomain = Site.domain>
					</cfif>
					<cfset Site.systemEmail = "Relayware<noReply@" & noReplyDomain & ">">

					<!--- need to update site.liveLanguages is set (from site.livelanguageids) --->
					<cfif site.liveLanguageids is not "">
						<CFQUERY NAME="getLiveLanguages" DATASOURCE="#applicationScope.sitedatasource#">
						select isocode from language where languageid  in ( <cf_queryparam value="#site.liveLanguageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						</CFQUERY>
						<CFSET site.liveLanguages =valuelist(getLiveLanguages.isocode) >
					</cfif>

					<cfset tempSiteStructBySiteId[siteDefID] = site>
					<!--- WAB 2009/03/03 Lighthouse Issue All Sites 1739 - added --->
					<cfset tempSiteDefIndexByName[name] = siteDefID>



					<cfset firstpass = true>

					<!---
					WAB 2008/07/07 There is bug somewhere in here which occurs if the #domains# list contains a duplicate item.
					For some reason #request.currentsite.internalDomain# disappears
					Haven't worked it out though - ended up just removing the duplicate
					WAB 2009/06/23 Note: I think that application.sitedef is virtually redundant.  Most of the code uses the much lighter weight sitedefByID and siteDefIndex.  application.sitedef could be got rid of [one reference in loadElementTreeDefinitions() could be replaced]
					 --->

					<!--- for backwards compatibilty populate the domains field--->
						<cfquery name="qryGetDomains" datasource="#applicationScope.siteDataSource#">
						select domain, ID
						from
							siteDefDomain
						where
								siteDefID =  <cf_queryparam value="#siteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
							and testSite =  <cf_queryparam value="#applicationScope.testSite#" CFSQLTYPE="CF_SQL_INTEGER" >
							and active = 1
						order by mainDomain desc, id
						</cfquery>

						<cfset site.domainList = valuelist (qryGetDomains.domain)>
					<cfloop query="qryGetDomains">
						<!--- <cfset tempSiteStruct[domain] = site> --->
						<cfset tempSiteDefIndex[qryGetDomains.domain] = {siteDefID = SiteDefs.SiteDefID , siteDefDomainID = id}>

						<!--- make sure that this domain is in the #application.internal/externaluserdomains# for backwards compatability
							WAB2007-02-28
						--->
						<!---
						<cfif site.isInternal and listFindNoCase (application.internal UserDomains, domain) is 0>
							<cfset application.internal UserDomains = listappend(application.internal UserDomains, domain)>
						<cfelseif not site.isInternal and listFindNoCase (application.externa lUserDomains, domain) is 0>
							<cfset application.external UserDomains = listappend(application.external UserDomains, domain)>
						</cfif>
						--->
					</cfloop>


				</cfloop>

			<!---
			WAB 2009/06/23 LID 2398 all sites now on sitedef code
			</cfif>
			--->

			<!--- <cfset application.siteDef = tempSiteStruct> --->
			<cfset applicationScope.siteDefById = tempSiteStructBySiteId>
			<cfset applicationScope.siteDefIndex = tempSiteDefIndex>
			<!--- WAB 2009/03/03 Lighthouse Issue All Sites 1739 - added --->
			<cfset applicationScope.siteDefIndexByName = tempSiteDefIndexByName>

			<cfif updateCluster >
				<cfset applicationScope.com.clusterManagement.updateCluster(updatemethod="loadSiteDefinitions")>
			</cfif>

		</cflock>

		<cfreturn true>

	</cffunction>

	<cffunction name="getEnvironmentAbbreviation">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="testSite" default="#applicationScope.testSite#">

		<cfreturn applicationScope.EnvironmentLookUp[testSite].abbr>
	</cffunction>

	<cffunction name="getEnvironmentName">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="testSite" default="#applicationScope.testSite#">


		<cfreturn applicationScope.EnvironmentLookUp[testSite].name>
	</cffunction>

	<cffunction name="isLiveEnvironment" hint="Returns whether Current Site is Live (as opposed to one of the Test types)">
		<cfargument name="applicationScope" default="#application#">

		<cfreturn NOT (applicationScope.testSite)>
	</cffunction>

	<cffunction name="isEnvironment" hint="Pass in a list of any of Live,Test,Dev,Staging.  Returns whether current site is if that type">
		<cfargument name="TypeList" required="true">
		<cfargument name="applicationScope" default="#application#">

		<cfreturn iif(listFindNoCase(arguments.TypeList,applicationScope.EnvironmentLookUp[applicationScope.testSite].abbr),true,false)>
	</cffunction>



	<!--- WAB 2008/11/26 split out this code to be used here and in SSO --->
	<cffunction name="getSiteDefs" access="public" returns="query" >
		<cfargument name="internal" default="true">
		<cfargument name="external" default="true">
		<cfargument name="applicationScope" default="#application#">

		<cfset var qryGetSiteDefs = "">
		<cfset var qryGetDomains = '' />

			<!---
			WAB 2009/02/03 LID1745
			An odd error cropped up when one of the domains was blank, so changed to not bring it back
			WAB 2013-05-20 added main domain, for use in generating the noReply email address
			 --->

				<cfquery name="qryGetSiteDefs" datasource="#applicationScope.siteDataSource#">
					select
						'' as Domains,
						(select top 1 domain from sitedefdomain sdd where sdd.siteDefID = sd.siteDefID and testSite = <cf_queryparam value="#applicationScope.testSite#" CFSQLTYPE="CF_SQL_INTEGER" >) as mainDomain,
						*
						from
						siteDef sd
						where 1 = 1
						<cfif not internal>and isInternal = 0</cfif>
						<cfif not external>and isInternal = 1</cfif>
				order by isInternal desc <!--- WAB 2009/06/23 added order so that guarantee that internal processed first --->
				</cfquery>

				<!--- populate the domains field 	--->
				<cfloop query="qryGetSiteDefs">
					<cfquery name="qryGetDomains" datasource="#applicationScope.siteDataSource#">
					select domain
					from
						siteDefDomain
					where
							siteDefID =  <cf_queryparam value="#siteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and testSite =  <cf_queryparam value="#applicationScope.testSite#" CFSQLTYPE="CF_SQL_INTEGER" >
					order by mainDomain desc, id
					</cfquery>
					<cfset querySetCell (qryGetSiteDefs,"domains",valuelist(qryGetDomains.domain),currentrow)>
				</cfloop>


		<cfreturn qryGetSiteDefs>

	</cffunction>

	<cffunction name="getExternalSiteDefs" access="public" returns="query" >
		<cfreturn getSiteDefs(internal = false)>
	</cffunction>

<!---
This function creates a struct with some default values defining a site
it is then merged with values from the database to create the real site structure
 --->

	<cffunction name="createDefaultSiteDef" access="private" >
		<cfargument name="isInternal" required = true>
		<cfargument name="applicationScope" default="#application#">

		<cfset var clusterManagementObj = createObject("component","relay.com.clusterManagement")> 			<!--- WAB 2009/06/24	 got rid of references to application.com --->

		<cfset var Site = structNew()>
		<cfset site.isOK = true>
		<cfset site.title = "">
		<cfset site.name  = "">
		<cfset Site.domain = "">
		<cfset Site.urlRoot = "">
		<cfset site.domainList = "">
		<cfset Site.securityProfile = "">
		<cfset Site.allowRememberMe = 0>
		<cfset Site.isInternal = isInternal>
		<cfset Site.siteDefID = 0>
		<cfset site.hasLoginInBorder = false> <!--- not sure whether here is where this should be - really needs to be connected to a border set --->
		<cfset site.unsubscribeHandler = "">    <!--- options  SingleClick,Screen,Element --->
		<cfset site.unsubscribeHandlerID = "unsubscribeHandler">  <!--- The id of the element or screen to go to --->
<!--- 		<cfset site.test = structNew()> --->
		<!--- wab testing nagging --->
		<cfset site.liveLanguageIDs= "1">   <!--- did come from application. livelanguageids but now populated in sitedef table--->
		<cfset site.nagging = structNew()>
		<cfset site.nagging.isOn = false>
		<cfset site.nagging.pageList = "">
		<cfset site.nagging.processid = 0>
		<cfset site.stylesheet = "">
		<cfset site.useSecureLogin = false>  <!--- 2007-07-16 WAB testing https login  --->
		<cfset site.siteOffLine = false>
		<cfset Site.httpProtocol =  "http://">


		<!--- WAB added 2007-04-19 get IP address of server - useful for clusters with virtual servers
		2009/03/03 Moved from in setCurrentSite function so only done once, not on every request. Since the structure is stored in application scope, the ip address is not going to change
				Added getting the port
		--->
		<!---
			changed to a structure site.instance
			instance.ipaddress  was  serverIPAddress
			instance.port was serverPort
			instance.UniqueID was serverInstanceUniqueID
			instance.name
			--->

		<cfset site.instance = clusterManagementObj.getInstanceDetails()>

		<cfif isInternal>
			<cfset site.borderset = "">
			<cfset Site.elementTreeName = "">
			<!--- this bit copied from templates\loadStylesheets.cfm
				WAB 2007-03-14
			 --->
			<cfif fileexists("#applicationscope.paths.content#\styles\relayWareStyles.css")>
				<cfset site.stylesheet  = "/code/styles/relayWareStyles.css">
			<cfelse>
				<!--- don't know version number until run time --->
				<cfset site.stylesheet  = "/styles/relayWareStylesV2.css">
			</cfif>

		<cfelse>
			<cfset site.borderset = "relayBorders">
			<cfset Site.elementTreeName = "default">
		</cfif>



		<cfreturn site>
	</cffunction>


	<!---
	WAB 2008/02/05
	given a domain, gets the site defid
	broken out as a function from existing code
	--->
	<cffunction name="getSiteDefIndexItem" access="public">
		<cfargument name="domain" required= true>
		<cfargument name="abortOnNotFound" default=false>

		<cfset var nullResult = {siteDefID = 0, siteDefDomainID = 0 }>

		<cflock timeout="5" throwontimeout="Yes" name="loadSiteDefinitions_#application.applicationname#" type="EXCLUSIVE">
			<cfif not structKeyExists(application ,"siteDefIndex")>
				<cfset loadSiteDefinitions(updateCluster = false)>
			</cfif>
		</cflock>

		<cfif not(structKeyExists(application, "siteDefIndex") AND structKeyExists(application.siteDefIndex,Domain))>
			<!--- WAB 2013-04-30 Add isRelayDomain test here, prevents unnecessary reloading of siteDef structure --->
			<cfif isRelayDomain (domain)>
				<!--- site doesn't appear to be in the siteDef, so try loading again from db  - this gets round bootstrap problems --->
				<cfset loadSiteDefinitions(updateCluster = false)>
				<cfif not structKeyExists(application.siteDefIndex,domain)>
					<cfif abortOnNotFound>
						<!--- If still not there then exit non-gracefully --->
						<cfoutput>No Site Set up for #CGI.SERVER_NAME#<cfdump var="#application.siteDefByID#"><cfdump var="#application.siteDefIndex#"></cfoutput>
						<CF_ABORT>
					<cfelse>
						<cfreturn nullResult>
					</cfif>
				</cfif>
			<cfelse>
				<cfreturn nullResult>
			</cfif>
		</cfif>

		<cfreturn application.siteDefIndex[domain]>

	</cffunction>

	<!---
	WAB 2008/02/05
	Two functions to give access to the sitedef structure
	--->
	<cffunction name="getSiteDefStructureByID" access="public">
		<cfargument name="siteDefID" required= true>
		<cfreturn application.siteDefById[siteDefID]>
	</cffunction>

	<cffunction name="getSiteDefStructureByDomain" access="public">
		<cfargument name="domain" required= true>
		<cfargument name="forceSecure" default="false">

		<cfset var iaddrClass = '' />
		<cfset var siteDefIndexItem = getSiteDefIndexItem(domain = domain)>
		<cfset var siteDefID = siteDefIndexItem.siteDefID>
		<cfset var siteDefDomainID = siteDefIndexItem.siteDefDomainID>
		<cfset var siteDef = "">

		<cfif SiteDefID is 0>
			<cfset siteDef ={isOK = false}>
			<cfreturn siteDef>
		</cfif>

		<cfset siteDef = duplicate(application.siteDefById[siteDefID])>

		<cfset siteDef.domainAndRoot = domain & siteDef.urlroot>
		<cfset siteDef.domain = domain >

		<!---  WAB 2008/07/17 added this bit, 2010/11/23 WAB - can't think what for--->
		<cfif siteDef.isInternal>
			<cfset siteDef.internalDomain = siteDef.domain>
		</cfif>

		<!--- 2008/01/25 WAB changes for https login being 'fully' https 
			 2016-03-16 WAB isSecure was not being set to true when 'secureness' was detected from the protocol (when forceSecure is set) 
			 				rather than from the isSecure setting in the db (which is use when system is behind a load balancer which does the SSL and the request comes to CF insecure)
		--->
		<cfif forceSecure or siteDef.isSecure>
			<cfset siteDef.httpProtocol =  "https://">
			<cfset siteDef.isSecure =  1>
		</cfif>

		<cfset siteDef.protocolAndDomain = siteDef.httpprotocol & siteDef.domain>
		<cfset siteDef.siteDefDomainID = siteDefDomainID >


		<!---
			SOPHOS LID 2512
				store the IP address that CGI.SERVER_NAME resolves to
				- should allow us to detect which switch/cluster manager a request has come through
				This can then be used in any way you want - initially to be used for making sure that secure files are copied to the correct server
		--->
			<cftry>
				<CFOBJECT NAME="iaddrClass"  CLASS="java.net.InetAddress" TYPE="JAVA" ACTION="CREATE">
				<cfset sitedef.IPAddressOfSwitch = iaddrClass.getByName(domain).getHostAddress()>
				<cfcatch>
					<cfset sitedef.IPAddressOfSwitch ="Unknown">
				</cfcatch>
			</cftry>

		<cfreturn sitedef>

	</cffunction>


	<!--- WAB 2013-03-2103 - used by comms to check that the value defined in linksToSiteURL is valid (which it may not be if a db has moved from live to staging)--->
	<cffunction name="isSiteDefDomainValid" access="public">
		<cfargument name="domain" required= true>

		<cfset var siteDefID = getSiteDefIndexItem(domain = domain).siteDefID>

		<cfif siteDefID is 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>

	</cffunction>

	<!--- WAB 2013-04-30 quick check in the memory and in db to see if a domain is a Relay one.
			saves having to reload application variable every time I want to test whether a domain is a valid Relay one
			Would be better if I could guarantee that the correct things were always in memory (not sure why I can't!)
	 --->
	<cffunction name="isRelayDomain" access="public">
		<cfargument name="domain" required= true>

		<cfset result = true>

		<cfif not (structKeyExists(application ,"siteDefIndex") AND structKeyExists (application.siteDefIndex,domain))>

			<cfquery name="local.checkDomain" datasource = "#application.sitedatasource#">
			select 1 from
			sitedefdomain sdd
			where sdd.testsite =  <cf_queryparam value="#application.testsite#" CFSQLTYPE="CF_SQL_INTEGER" >
			and domain = <cf_queryparam value="#domain#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset result = local.checkDomain.recordcount>

		</cfif>

		<cfreturn result>

	</cffunction>



	<cffunction name="setCurrentSite" access="public">

			<cfset  request.currentSite = getSiteDefStructureByDomain(domain = cgi.server_name,forceSecure = cgi.SERVER_PORT_SECURE)>

	</cffunction>


	<!--- To functions to replace listFirst(application.internal userdomains) (and externalUserDomains)
		Haven't really worked out any better logic but here is it anyway
	--->
	<cffunction name="getReciprocalExternalorInternalSite" output="true">
		<cfargument name="internal">
		<cfargument name="reciprocalTo" default =  "">

		<cfset var result = "">

		<cfif internal>
			<cfset result = getInternalSite()>
		<cfelse>
			<cfset result = getExternalSite()>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getReciprocalExternalDomain" output="false">
		<cfreturn getReciprocalExternalorInternalSite (internal = false).Domain>
	</cffunction>

	<cffunction name="getReciprocalInternalDomain" output="false">
		<cfreturn getReciprocalExternalorInternalSite (internal = true).domain>
	</cffunction>

	<cffunction name="getReciprocalExternalProtocolAndDomain" output="false">
		<cfreturn getReciprocalExternalorInternalSite (internal = false).protocolAndDomain>
	</cffunction>

	<cffunction name="getReciprocalInternalProtocolAndDomain" output="false">
		<cfreturn getReciprocalExternalorInternalSite (internal = true).protocolAndDomain>
	</cffunction>


	<!--- 2009/03/03 WAB/NJH Lighthouse Issue All Sites 1739
		2013-03-12	WAB	Deal with the site or siteDefID not existing, just default back to main external site
	--->

	<cffunction name="getExternalSiteURL">
		<cfargument name="site" default =  "">	  <!--- name of the site, or sitedefid, or if blank we just get the first external site --->

		<cfset var externalSite = getExternalSite(site)>

		<cfif not externalSite.isOK>
			<cfset externalSite = getExternalSite()>
		</cfif>

		<cfreturn externalSite.ProtocolAndDomain>

	</cffunction>


	<cffunction name="getInternalSite" output="true">
		<cfset var result = '' />
		<cfset var siteID = '' />

		<cfif request.currentSite.isInternal>
			<cfset result = request.currentSite>
		<cfelse>
			<cfloop item="siteID" collection="#application.siteDefById#">
				<cfif application.siteDefById[siteID].isInternal>
					<cfset result = application.siteDefById[siteID]>
					<!--- TBD now need to get the correct host though! Currently just getting the first one--->
					<cfset result.domain = listFirst(result.domainList) >
					<cfset result.protocol = iif(result.isSecure,de("https://"),de("http://")) >
					<cfset result.domainAndRoot =  result.domain>
					<cfset result.ProtocolAndDomain =  result.protocol & result.domain>

					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="getExternalSite" output="true">
		<cfargument name="site" default =  "">	  <!--- name of the site, or sitedefid, or if blank we just get the first external site --->
		<cfargument name="reciprocalTo" default =  "">

		<cfset var result = '' />
		<cfset var getReciprocal = '' />
		<cfset var getMainSite = '' />

		<cfif reciprocalTo  is "" and request.currentSite.isInternal>
			<!--- If we are on an internal site then we want to get a site which is reciprocal to this one --->
			<cfset arguments.reciprocalTo = request.currentSite.domain>
		</cfif>

		<cfif reciprocalTo is not "">
			<cfquery name="getReciprocal" datasource = "#application.sitedatasource#">
				select
					sdd.domain,sdd.reciprocalid,sdd.maindomain --,reciprocalTo.*
				from
					sitedef sd
						inner join
					sitedefdomain sdd on sd.sitedefid = sdd.sitedefid
						inner join
							(select id, sitedefid from
							sitedefdomain where domain =  <cf_queryparam value="#reciprocalTo#" CFSQLTYPE="CF_SQL_VARCHAR" >
							) as reciprocalTo
					on reciprocalTo.id = sdd.reciprocalid
						or reciprocalTo.sitedefid = sd.reciprocalsite
				where sdd.testsite =  <cf_queryparam value="#application.testsite#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!---GCC 2011/01/21 needed to filter to correct site when more than one portal exists --->
					<cfif site is not "">
						<cfif isNumeric(site)>
						and sd.sitedefid =  <cf_queryparam value="#site#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfelse>
						and sd.name =  <cf_queryparam value="#site#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
					</cfif>
				order by
					case when reciprocalTo.id = sdd.reciprocalid then 1 else 0 end desc,  -- this puts a site which has a 'personal' reciprocal arrangement with this site at the top of the list
					sdd.maindomain desc,   -- this gets the main domain first
					<!--- WAB LID 5680
					for systems with multiple external sitedefs, this gets the one with the lowest number first (idealy we would have an ordering column of some sort on the sitedef table)
					--->
					sd.sitedefid
			</cfquery>

			<cfif getReciprocal.recordCount>
				<cfset result = getSiteDefStructureByDomain(domain = getReciprocal.domain)>
				<cfreturn result>
			</cfif>

		</cfif>

		<cfif not request.currentSite.isInternal and (site is "" or site is request.currentSite.name)>
			<cfset result = request.currentSite>
		<cfelse>
			<cfquery name="getMainSite" datasource = "#application.sitedatasource#">
				select *
				from
					sitedef sd
						inner join
					sitedefdomain sdd on sd.sitedefid = sdd.sitedefid
				where
					isInternal = 0
				<!---GCC 2011/01/21 needed to filter to correct environment or you get a mix of dev,staging and prod right? --->
					and sdd.testsite =  <cf_queryparam value="#application.testsite#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif site is not "">
						<cfif isNumeric(site)>
						and sd.sitedefid =  <cf_queryparam value="#site#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfelse>
						and sd.name =  <cf_queryparam value="#site#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
					</cfif>
				order by
					sdd.maindomain desc   -- this gets the main domain first
			</cfquery>
			<cfset result = getSiteDefStructureByDomain(domain = getMainSite.domain)>

		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="getListOfAllDomains" output="true">
		<cfargument name="internal">
		<cfset var result = "">
		<cfset var siteID = '' />

			<cfloop item="siteID" collection="#application.siteDefById#">
				<cfif application.siteDefById[siteID].isInternal is arguments.internal>
					<cfset result= listappend(result,application.siteDefById[siteID].domainList)>
				</cfif>
			</cfloop>
		<cfreturn result>
	</cffunction>

	<cffunction name="setCurrentTree" access="public">
		<cfset request.currentSite.ElementTreeDef = duplicate(getTreeDefByNameAndCountry(request.currentSite.elementtreename,request.relaycurrentuser.countryid))>
	</cffunction>


	<cffunction name="updateCurrentSiteKey" access="public">
			<cfargument name="Key" required = "true">
			<cfargument name="value" required = "true">

			<cfset request.currentSite[Key] = value>

			<cfswitch expression = key>
				<cfcase value = "elementtreeName">
					<cfset setCurrentTree()>
				</cfcase>
				<cfcase value = "borderset">
					<cfset request.currentSite.stylesheet = calculateStyleSheetFromBorderSet(request.currentSite.borderset)>
				</cfcase>


			</cfswitch>


	</cffunction>

	<cffunction name="calculateStyleSheetFromBorderSet" access="private">
		<cfargument name="borderset" required = "true">
		<cfargument name="applicationScope" default="#application#">


			<cfset var stylesheetname = "/code/borders/" & BorderSet & "_stylesheet_display.css">
			<cfset var userFilesStyleSheetPath = applicationScope.paths.userFiles & stylesheetname>

			<cfif fileExists(userFilesStyleSheetPath)>
				<cfreturn  stylesheetname >
			<cfelse>
				<cfreturn "/code/styles/DefaultPartnerStyles.css" >
			</cfif>


	</cffunction>


	<cffunction name="loadElementTreeDefinitions" access="public" hint="loads tree definitions into application variables">
		<cfargument name="updateCluster" type="string" required="no" default="true">
		<cfargument name="applicationScope" default="#application#">

		<!--- creates
					application.ElementTreeDefByNameAndCountry
					application.ElementTreeDefByID
		 --->
			<cfset var parametersStruct = '' />
			<cfset var thisTreeDef = '' />
			<cfset var x = '' />
			<cfset var topid = '' />
			<cfset var treename = '' />
			<cfset var thisElementTreeDefID = '' />
			<cfset var sitename = '' />
			<cfset var id = '' />
			<cfset var site = '' />
			<cfset var tempStructByNameAndCountry = structnew()>
			<cfset var tempStructByID = structnew()>

			<cfset var checkForTreeDefs = "">
			<cfset var getTreeDefs = "">
			<cfset var getTreeDefs2 = "">
			<cfset var row = "">
			<cfset var defaultTreeStruct = "">
			<cfset var parameterStruct= "">
			<cfset var thisTreeName = "">


			<!--- first we load up all the ones which aren't inheriting from anything --->
			<cfquery name="getTreeDefs" datasource="#applicationScope.siteDataSource#">
			select * from elementTreeDef
			where inheritfrom = 0
			</cfquery>


			<cfloop query = "getTreeDefs">
				<!--- get row from database and pop into a structure --->
				<cfset row = applicationScope.com.structurefunctions.queryRowToStruct (getTreeDefs,currentrow)>
				<!--- get the items in the additional parameters field, and merge them into the main strucuture--->
				<cfset parametersStruct = applicationScope.com.regexp.convertNameValuePairStringToStructure(parameters,",")>
				<cfset row = applicationScope.com.structurefunctions.structMerge (row,parametersStruct)>
				<cfset defaultTreeStruct = createDefaultElementTreeDef(applicationScope = applicationScope)>
				<cfset row = applicationScope.com.structurefunctions.structMerge (struct1=defaultTreeStruct,struct2=row, dontmergenulls = true)>

				<!--- put resulting structure into a structure keyed on name and country --->
				<cfset tempStructByNameAndCountry[elementTreeName][countryid] = row>
				<!--- and into a structure referenced by ID (needed for the inheritence which is done by elementTreeDefID --->
				<cfset tempStructByID[elementTreeDefID] = tempStructByNameAndCountry[elementTreeName][countryid]>
			</cfloop>


			<!--- then we load up all the ones which are inheriting from another tree --->
			<cfquery name="getTreeDefs2" datasource="#applicationScope.siteDataSource#">
			select * from
				elementTreeDef
			where inheritFrom <> 0
			order by inheritFrom, elementTreeDefID
			</cfquery>

			<cfloop query = "getTreeDefs2">
				<!--- get row from database and pop into a structure and add the additional parameters--->
				<cfset row = applicationScope.com.structurefunctions.queryRowToStruct (getTreeDefs2,currentrow)>
				<cfset parametersStruct = applicationScope.com.regexp.convertNameValuePairStringToStructure(parameters,",")>
				<cfset row = applicationScope.com.structurefunctions.structMerge (row,parametersStruct)>

				<!--- if the name of the tree is specified then we use it, otherwise we get the name from the parent --->
				<cfif ElementTreeName is "">
					<cfset thisTreeName = tempStructByID[inheritfrom].elementTreename >
				<cfelse>
					<cfset thisTreeName = elementTreeName  >
				</cfif>

				<!--- now we merge the parent and the child together
					any values set in the child record override the values in the parent record
				 --->
				<cfset tempStructByNameAndCountry[thistreename][countryid] = applicationScope.com.structurefunctions.structMerge(struct1 = tempStructByID[inheritFrom], struct2 = row, dontMergeNulls = true, mergeToNewStruct = true)>
				<cfset tempStructByID[elementTreeDefID] = tempStructByNameAndCountry[thistreename][countryid]>
			</cfloop>

			<!--- Now loop through all the resulting definitions applying any special rules such as filling in the blanks --->
			<cfloop item="thiselementTreeDefID" collection = #tempStructByID#>
				<cfset thisTreeDef = tempStructByID[thiselementTreeDefID]>
				<cfif thisTreeDef.loginPage is "">
					<cfset thisTreeDef.loginPage = thisTreeDef.defaultPageLoggedOut>
				</cfif>

			</cfloop>


		<!--- save as an application variable --->
		<cfset applicationScope.ElementTreeDefByNameAndCountry = tempStructByNameAndCountry>
		<cfset applicationScope.ElementTreeDefByID = tempStructByID>

		<!--- create a structure to allow working out the site from a treeTopID
		WAB: doesn't look as if I ever used this since x isn't saved in appication scope

		<cfset x = structNew()>
		<cfloop collection = "#application.ElementTreeDefByID#" item="id">
			<cfset topid = application.ElementTreeDefByID[id].topid>
			<cfset treename = application.ElementTreeDefByID[id].elementtreename>
			<cfset sitename = "">

			<cfloop collection = "#application.sitedef#" item = "site">
				<cfif application.site def[site].elementtreename is treename>
					<cfset sitename = application.site def[site].name>
				</cfif>
			</cfloop>

			<cfset x[topid] = structNew()>
			<cfset x[topid].treename = treename>
			<cfset x[topid].sitename = sitename>
			<cfset x[topid].treename = treename>

		</cfloop>
			--->

		<cfif updateCluster >
			<cfset applicationScope.com.clusterManagement.updateCluster(updatemethod="loadElementTreeDefinitions",applicationScope = applicationScope)>
		</cfif>

		<cfreturn true>
	</cffunction>

	<cffunction name="dumpTreeDetails" access="public">
		<cfdump var="#application.ElementTreeDefByNameAndCountry#">

	</cffunction>

	<cffunction name="getTreeDefByNameAndCountry" access="public" hint="returns the correct tree for a given name and country">
			<cfargument name="name" required = "true">
			<cfargument name="countryid" required = "true">

			<cfset var TreeStructure = structNew()>
			<cfset var result = "">

			<cfif not structKeyExists(application ,"ElementTreeDefByNameAndCountry")>
				<cfset loadElementTreeDefinitions()>
			</cfif>


			<cfif name is "">
				<cfreturn structNew()>
			</cfif>

			<cfif structKeyExists (application.ElementTreeDefByNameAndCountry,name)>
				<cfset TreeStructure = application.ElementTreeDefByNameAndCountry[name]>
			<cfelse>
				<cfoutput>Tree #name# does not exist</cfoutput>
			</cfif>

			<!--- look for a tree for the given country, if doesn't exist then go for countryid = 0 --->
			<cfif structKeyExists(TreeStructure,countryid)>
				<cfset result = TreeStructure[countryid]>
			<cfelseif structKeyExists(TreeStructure,0)>
				<cfset result = TreeStructure[0]>
			<cfelse>
				<cfoutput>Default country does not exist for tree #name#</cfoutput>
			</cfif>

			<cfreturn result>

	</cffunction>


	<cffunction name="outputTreeDetails" access="remote">
		<cfargument name="id" required = "true">

			<cfset var thisTree = '' />
			<cfset var defaultTree = '' />
			<cfset var parentTree = '' />
			<cfset var field = '' />
			<cfset var defFromDatabase = '' />

				<cfquery name="defFromDatabase" datasource="#application.siteDataSource#">
				select * from elementTreeDef
				where elementTreeDefID  =  <cf_queryparam value="#id#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>

				<cfset thisTree = application.ElementTreeDefByID[id]>
				<cfset defaultTree = createDefaultElementTreeDef()>
				<cfif thisTree.inheritFrom is not 0>
					<cfset parentTree = application.ElementTreeDefByID[thisTree.inheritFrom]>
				</cfif>

<cfoutput>
<table class="table table-hover table-striped">
	<tr><td colspan=4><b>Tree #thisTree.elementTreeName#</b></td></tr>
	<tr>
		<th>Field</th>
		<th>Default</th>
		<th><cfif thisTree.inheritFrom is not 0><a href="?treeid=#thisTree.inheritFrom#">Inherited</a></cfif></th>
		<th>Value</th>
	</tr>

	<cfloop item="field" collection = #thisTree#>
		<tr>
			<td>#htmleditformat(field)#</td>
				<cfif isDefined("defFromDatabase.#field#") and defFromDatabase[field][1] is "" and thisTree[field] is not "">
					<cfif structkeyExists(defaultTree,field) and defaultTree[field] is thisTree[field]>
						<td>*</td><td></td>
					<cfelse>
					    <td></td><td>*</td>
					</cfif>
				<cfelse>
					<td></td><td></td>
				</cfif>

			<td>#thisTree[field]#</td>
		</tr>
	</cfloop>
</table>
</cfoutput>

	</cffunction>

	<cffunction name="createDefaultElementTreeDef" access="private" >
		<cfargument name="applicationScope" default="#application#">

		<cfset var treeDef = structNew()>
		<cfset treeDef.countryid = 0>
		<cfset treeDef.elementTreeName = "default">
		<cfset treeDef.topID = applicationScope.TopElementIDPartnerContent>
		<cfif isDefined("applicationScope.defaultExternalElementETID")>
			<cfset treeDef.defaultPageLoggedOut = applicationScope.defaultExternalElementETID>
		<cfelse>
			<cfset treeDef.defaultPageLoggedOut = "externalHome">
		</cfif>
		<cfif isDefined("applicationScope.defaultInternalElementETID")>
			<cfset treeDef.defaultPageLoggedIn = applicationScope.defaultInternalElementETID>
		<cfelse>
			<cfset treeDef.defaultPageLoggedIn = "portalHomePage">
		</cfif>
			<cfset treeDef.LoginPage = "">
			<cfset treeDef.showBreadcrumbLoggedIn = true>
			<cfset treeDef.hideTheseEIDsFromBreadCrumb = "">

		<cfreturn treeDef>
	</cffunction>


	<cffunction name="isSiteSecure" returntype="boolean" output="false">
		<cfargument name="sitename" type="string" required="true">

		<cfif structKeyExists(application.siteDefIndex,arguments.sitename)>
			<cfreturn application.sitedefById[application.sitedefIndex[arguments.sitename].siteDefID].isSecure>
		</cfif>

		<cfreturn false>
	</cffunction>

	<cffunction name="getSiteProtocol" returnType="string" output="false">
		<cfargument name="sitename" type="string" required="true">

		<cfif isSiteSecure(sitename=arguments.sitename)>
			<cfreturn "https://">
		<cfelse>
			<cfreturn "http://">
		</cfif>

	</cffunction>


<!---
This refreshes the site structure if cfcs are refreshed
but if run at boot time won't work (because some dependent objects not loaded yet, so has a CFTRY around it)
  --->
	<cftry>
		<cfset loadSiteDefinitions(updateCluster=false)>
		<cfcatch>
		</cfcatch>
	</cftry>


	<!--- DEPRECATED Use getSiteVersionData --->
	<cffunction name="getSiteVersion" output="false" returnType="struct" access="public">

		<cfreturn getSiteVersionData()>
	</cffunction>

	<cffunction name="getSiteVersionData" output="false" returnType="struct" access="public">

		<cfreturn application.instance.relaywareVersionInfo.versionData>
	</cffunction>
	
	<cffunction name="getSiteFullVersionString" output="false" returnType="string" access="public">
		<cfscript>
			var siteData=getSiteVersionData();
			return "#getSiteFriendlyVersionString()#.#siteData.build#.#siteData.revision#";
		</cfscript>
	</cffunction>
	
	<cffunction name="getSiteFriendlyVersionString" output="false" returnType="string" access="public">
		<!--- WAB 2016-03-09	Add a year to the version info
			WAB 2016-04-07 BF-564 Add the branch info on Test/dev sites
		 --->
		<cfscript>
			var siteData=getSiteVersionData();
			var result = "";
			var items = ["year","major","minor","maintenance"];
			arrayEach (items, function (item) {
				if (sitedata[item] is not "") {
					result = listAppend(result,sitedata[item],".");
				}
			});
			
			if (application.com.relayCurrentSite.isEnvironment('dev,test')) {
				result &= " " & application.instance.relaywareversioninfo.versiondata.branch;
			}
			

			return result;
		</cfscript>
	</cffunction>
	



</cfcomponent>


