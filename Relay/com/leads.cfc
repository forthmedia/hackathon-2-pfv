/*
 *
 *
 *
 * 2015/03/31		NJH		Add new function to set JS variables in lead.cfm called getJavascriptVariables. Added so we could add custom JS variables on the page.
 * 2015-06-18		MS		P-CML001 Added missing Address fields to sort order - If you set Address1,Address2 or Address4 display true then make sure you add them to the sort order as well.
 * 2015/07/17		NJH		Filter out hidden fields as needing to be passed through to the order function.
 * 2015/11/10       DAN     Case 446321 - make firstname and lastname required fields
 * 2015/11/12 		RJT 	PROD2015-374 - Allowed leads to be converted on portal
 * 2016/01/28		RJT		BF-393 - Resolved issue where a user didn't have rights to the fields used to check if a field could be converted on the portal. RightsCheck is now turned off
 * 2016/11/02		SBW		Added updatePartnerViewedDate method to update Partner Viewed Date the first time a lead is viewed in portal.
 **/
component output=false {

	public struct function GetLeadsFormFieldDisplayOptions
	(
		required boolean isInternal,
		required string mode,
		required numeric leadID
	)
	{
		var fieldDisplay = {};

		switch (arguments.mode) {
			case "add":
				fieldDisplay.sortOrder = ["salutation","EndCustomerName","firstname","lastname","company","countryID","countryID_Display","email","phonemask","officePhone","website","progressID","leadTypeID","leadTypeID_Display","enquiryType","description","sourceID","annualRevenue","currency","ApprovalStatusID","rejectionReason","partnerLocationID","partnerSalesPersonID","partnerSalesManagerPersonID","distiContactPersonID","vendorAccountManagerPersonID"];
				break;
			case "edit":
				fieldDisplay.sortOrder = ["salutation","EndCustomerName","firstname","lastname","company","countryID","countryID_Display","email","phonemask","officePhone","website","progressID","leadTypeID","leadTypeID_Display","enquiryType","description","sourceID","annualRevenue","currency","ApprovalStatusID","ApprovalStatusID_Display","rejectionReason","acceptedByPartner","Contacts","partnerLocationID","partnerLocationID_Display","partnerSalesPersonID","partnerSalesManagerPersonID","distiContactPersonID","vendorAccountManagerPersonID","vendorAccountManagerPersonID_orig","vendorAccountManagerPersonID_Display"];
				break;
		}

		fieldDisplay.salutation.required = false;
		fieldDisplay.endCustomerName.required = true;
		fieldDisplay.firstname.required = true;
		fieldDisplay.lastname.required = true;
		//START: 2015-06-18	MS	Added missing address fields
		fieldDisplay.Address1.required = false;
		fieldDisplay.Address2.required = false;
		fieldDisplay.Address4.required = false;
		//END  : 2015-06-18	MS	Added missing address fields
		fieldDisplay.Address5.required = false;
		fieldDisplay.countryID.required = false;
		fieldDisplay.officePhone.required = false;
		fieldDisplay.website.required = false;
		fieldDisplay.leadTypeID.required = false;
		fieldDisplay.enquiryType.required = false;
		fieldDisplay.description.required = false;
		fieldDisplay.sourceID.required = false;
		fieldDisplay.annualRevenue.required = false;
		fieldDisplay.currency.required = false;
		fieldDisplay.ApprovalStatusID.required = true;
		fieldDisplay.acceptedByPartner.required = false;
		fieldDisplay.partnerLocationID.required = false;
		fieldDisplay.partnerSalesPersonID.required = false;
		fieldDisplay.partnerSalesManagerPersonID.required = false;
		fieldDisplay.sponsorPersonID.required = false;
		fieldDisplay.distiLocationID.required = false;
		fieldDisplay.distiContactPersonID.required = false;
		fieldDisplay.vendorAccountManagerPersonID.required = false;
		fieldDisplay.vendorSalesManagerPersonID.required = false;

		fieldDisplay.salutation.display = true;
		//START: 2015-06-18	MS	Added missing address fields - If you set Address1,Address2 or Address4 display true then make sure you add them to the sort order as well.
		fieldDisplay.Address1.display = false;
		fieldDisplay.Address1.readonly = false;
		fieldDisplay.Address2.display = false;
		fieldDisplay.Address2.readonly = false;
		fieldDisplay.Address4.display = false;
		fieldDisplay.Address4.readonly = false;
		//END  : 2015-06-18	MS	Added missing address fields - If you set Address1,Address2 or Address4 display true then make sure you add them to the sort order as well.
		fieldDisplay.countryID.readonly = false;
		fieldDisplay.website.display = true;
		fieldDisplay.progressID.display = true;
		fieldDisplay.enquiryType.display = true;
		fieldDisplay.leadTypeID.display = true;
		fieldDisplay.leadTypeID.readonly = false;
		fieldDisplay.currency.display = true;
		fieldDisplay.statusID.display = true;
		fieldDisplay.statusID.readonly = false;
		fieldDisplay.acceptedByPartner.display = true;
		fieldDisplay.contacts.display = true;
		fieldDisplay.partnerLocationID.readonly = false;
		fieldDisplay.partnerLocationID.display = true;
		fieldDisplay.partnerSalesPersonID.display = true;
		fieldDisplay.partnerSalesManagerPersonID.display = true;
		fieldDisplay.sponsorPersonID.display = true;
		fieldDisplay.distiLocationID.display = true;
		fieldDisplay.distiLocationID.readonly = false;
		fieldDisplay.distiContactPersonID.display = true;
		fieldDisplay.vendorAccountManagerPersonID.display = true;
		fieldDisplay.vendorAccountManagerPersonID.readonly = false;
		fieldDisplay.vendorSalesManagerPersonID.display = true;
		fieldDisplay.relatedProvince.display = false;
		fieldDisplay.sourceID.display = true;
		fieldDisplay.annualRevenue.display = true;

		if (!arguments.isInternal && arguments.mode=="add") {
			fieldDisplay.countryID.readonly = true;
			fieldDisplay.leadTypeID.readonly = true;
			fieldDisplay.currency.display = false;
			fieldDisplay.statusID.display = false;
			fieldDisplay.acceptedByPartner.display = false;
			fieldDisplay.contacts.display = false;
			fieldDisplay.partnerLocationID.display = false;
			fieldDisplay.partnerSalesPersonID.display = false;
			fieldDisplay.partnerSalesManagerPersonID.display = false;
			fieldDisplay.sponsorPersonID.display = false;
			fieldDisplay.distiLocationID.display = false;
			fieldDisplay.distiContactPersonID.display = false;
			fieldDisplay.vendorAccountManagerPersonID.display = false;
			fieldDisplay.vendorSalesManagerPersonID.display = false;
		}
		if (!arguments.isInternal && arguments.mode!="add") {
			fieldDisplay.countryID.readonly = true;
			fieldDisplay.leadTypeID.readonly = true;
			fieldDisplay.currency.display = false;
			fieldDisplay.statusID.display = true;
			fieldDisplay.statusID.readonly = true;
			fieldDisplay.acceptedByPartner.display = true;
			fieldDisplay.partnerLocationID.display = true;
			fieldDisplay.partnerLocationID.readonly = true;
			fieldDisplay.partnerSalesPersonID.display = true;
			fieldDisplay.partnerSalesManagerPersonID.display = false;
			fieldDisplay.sponsorPersonID.display = false;
			fieldDisplay.distiLocationID.display = false;
			fieldDisplay.distiContactPersonID.display = false;
			fieldDisplay.vendorAccountManagerPersonID.display = true;
			fieldDisplay.vendorAccountManagerPersonID.readonly = true;
			fieldDisplay.vendorSalesManagerPersonID.display = false;
		}

		return fieldDisplay;
	}


	public query function GetCountries
	(
		boolean withCurrencies=true,
		boolean showCurrentUsersCountriesOnly=true
	)
	validValues=true
	{
		var result = application.com.relayCountries.getCountries(
			withCurrencies=arguments.withCurrencies,
			showCurrentUsersCountriesOnly=arguments.showCurrentUsersCountriesOnly
		);

		return result;
	}


	public string function GetCustomFields
	(
		required boolean isInternal,
		required string mode
	)
	{
		var rtnString = '';

		return rtnString;
	}


	public any function OrderXmlSource
	(
		required string xmlSource,
		required array sortOrder
	)
	{

		var xmlDoc = XmlParse(arguments.xmlSource);
		var xmlItems = xmlDoc.editors.editor.XmlChildren;

		var missingFields = [];

		var xmlFields = [];
		for (var i=1;i <= ArrayLen(xmlItems);i++) {
			switch(xmlItems[i].XmlName) {
				case "field":
					if (!structKeyExists(xmlItems[i].XmlAttributes,"control") or xmlItems[i].XmlAttributes.control neq "hidden") {ArrayAppend(xmlFields,xmlItems[i].XmlAttributes.name);};
					break;
				case "lineItem":
				case "group":
					for (var f=1;f <= ArrayLen(xmlItems[i].XmlChildren);f++) {
						ArrayAppend(xmlFields,xmlItems[i].XmlChildren[f].XmlAttributes.name);
					}
					break;
			}

		}

		for (var f=1;f <= ArrayLen(xmlFields);f++) {
			if (!ArrayFindNoCase(arguments.sortOrder,xmlFields[f])) {
				ArrayAppend(missingFields,xmlFields[f]);
			}
		}

		if (ArrayLen(missingFields)) {
			throw("The following fields are missing from the sortOrder: " & ArrayToList(missingFields));
		}

		var orderedXmlDoc = XmlNew();
		orderedXmlDoc.xmlRoot = XmlElemNew(orderedXmlDoc,"editors");
		orderedXmlDoc.editors.XmlChildren[1] = XmlElemNew(orderedXmlDoc,"editor");
		for (var attribute IN xmlDoc.editors.editor.XmlAttributes) {
			orderedXmlDoc.editors.editor.XmlAttributes[attribute] = StructFind(xmlDoc.editors.editor.XmlAttributes,attribute);
		}

		var fieldStruct = {};
		for (var s=1;s <= ArrayLen(arguments.sortOrder);s++) {
			var field = XmlSearch(xmlDoc,"//field[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[s])#""]");
			if (!ArrayLen(field)) {
				var field = XmlSearch(xmlDoc,"//lineItem[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[s])#""]");
			}
			if (!ArrayLen(field)) {
				var field = XmlSearch(xmlDoc,"//group[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[s])#""]");
			}
			if (!ArrayLen(field)) {
				throw("Field """ & arguments.sortOrder[s] & """ not found in XML");
			}
			StructInsert(fieldStruct,arguments.sortOrder[s],field);
		}

		var xmlLineCount = 1;
		for (var s=1;s <= ArrayLen(arguments.sortOrder);s++) {
			var fieldObj = StructFind(fieldStruct,arguments.sortOrder[s]);
			var fieldTypeName = fieldObj[1].XmlName;

			var parentLineItem = XmlSearch(xmlDoc,"//lineItem/field[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[s])#""]");
			var parentGroup = XmlSearch(xmlDoc,"//group/field[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[s])#""]");

			if (!ArrayLen(parentLineItem) && !ArrayLen(parentGroup)) {

				var attributes = StructFind(fieldStruct,arguments.sortOrder[s])[1].XmlAttributes;
				orderedXmlDoc.editors.editor.XmlChildren[xmlLineCount] = XmlElemNew(orderedXmlDoc,fieldTypeName);

				for (var attribute IN attributes) {
					orderedXmlDoc.editors.editor.XmlChildren[xmlLineCount].XmlAttributes[attribute] = StructFind(attributes,attribute);
					orderedXmlDoc.editors.editor.XmlChildren[xmlLineCount].xmlText = fieldObj[1].xmlText;
				}
				xmlLineCount++;

			}

			switch(fieldTypeName) {
				case "lineItem":
					var lineItem = StructFind(fieldStruct,arguments.sortOrder[s])[1];
					var lineItemXmlLineCount = 1;
					for (var c=1;c <= ArrayLen(arguments.sortOrder);c++) {
						var childLineItem = XmlSearch(lineItem,"//lineItem/field[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[c])#""]");

						if (ArrayLen(childLineItem)) {

							orderedXmlDoc.editors.editor.lineItem.XmlChildren[lineItemXmlLineCount] = XmlElemNew(orderedXmlDoc,"field");

							var attributes = childLineItem[1].XmlAttributes;
							for (var attribute IN attributes) {
								orderedXmlDoc.editors.editor.lineItem.XmlChildren[lineItemXmlLineCount].XmlAttributes[attribute] = StructFind(attributes,attribute);
							}
							lineItemXmlLineCount++;

						}

					}
					break;
				case "group":
					var group = StructFind(fieldStruct,arguments.sortOrder[s])[1];
					var groupXmlLineCount = 1;
					for (var c=1;c <= ArrayLen(arguments.sortOrder);c++) {
						var childGroup = XmlSearch(group,"//group/field[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=""#lcase(arguments.sortOrder[c])#""]");

						if (ArrayLen(childGroup)) {

							orderedXmlDoc.editors.editor.group.XmlChildren[groupXmlLineCount] = XmlElemNew(orderedXmlDoc,"field");

							var attributes = childGroup[1].XmlAttributes;
							for (var attribute IN attributes) {
								orderedXmlDoc.editors.editor.group.XmlChildren[groupXmlLineCount].XmlAttributes[attribute] = StructFind(attributes,attribute);
							}
							groupXmlLineCount++;

						}

					}
					break;
			}


		}

		return orderedXmlDoc;
	}


	public boolean function leadIsConverted(string leadID){
		var convertedQuery=new Query();
		convertedQuery.setName("qCheckConvertedLead");
		convertedQuery.addParam(name="leadID", value=leadID,cfsqltype="cf_sql_varchar");
		result = convertedQuery.execute(sql="SELECT leadID from dbo.lead l where convertedOpportunityID IS NULL and leadID = :leadID ");

    	return result.getPrefix().recordcount EQ 0;

	}


	public boolean function personCanConvertLead(numeric personID=request.relayCurrentUser.personID){

		var validFlags=application.com.settings.getSetting("leadManager.LeadsConvertibleOnPortalBy");


		if (ListLen(validFlags)>0){

			var personData=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.person, entityID=personID, fieldList=validFlags, testLoggedInUserRights=false);

			for(var row in personData.recordSet){
				for(var key in row){

					if (row[key]){
						//we have one of the required flags
						return true;
					}

				}
			}

			return false; //got to end and disn't find a flag to give us permission
		}else{
			return true; // no flags, all permitted
		}

	}

	/**
	 * Checks the lead state (not the logged in users permissions) and returns if the lead can be converted from its current state.
	 **/
	public boolean function leadCanBeConverted(required string leadID){
		var approvalStatusesAtWhichCanBeConverted=application.com.settings.getSetting("leadManager.leadsCanBeConvertedOnPortalAtApprovalStatus");
		var progressStageAtWhichCanBeConverted=application.com.settings.getSetting("leadManager.leadsCanBeConvertedOnPortalAtProgressStage");

		var canBeConverted=arguments.leadId neq 0?true:false; //start by assuming it can be converted, try to prove otherwise

		if (canBeConverted) {
			var queryData=application.com.relayEntity.getEntity(EntityID=leadID, EntityTypeID=application.EntityTypeID.lead, fieldList="progressID,approvalStatusID", testLoggedInUserRights=false).recordSet;

			if (canBeConverted AND ListLen(approvalStatusesAtWhichCanBeConverted)>0){
				canBeConverted=ListContains(approvalStatusesAtWhichCanBeConverted,queryData.approvalStatusID);
			}
			if (canBeConverted AND ListLen(progressStageAtWhichCanBeConverted)>0){
				canBeConverted=ListContains(progressStageAtWhichCanBeConverted,queryData.progressID);
			}
		}

		return canBeConverted;
	}

	public string function getJavascriptVariables(){
		var rtnString = 'var rejectID = #application.com.relayForms.getLookupList(fieldname="leadApprovalStatusID",textID="Rejected").lookupID#;
						var bInternal = #request.relayCurrentUser.isInternal#;
						var requestElementID  = #NOT request.relayCurrentUser.isInternal?request.currentElement.id:0#;';

		return rtnString;
	}

	/**
	 * SBW: Set the partnerViewedDate when lead is viewed on portal, if not set already.
	**/
	public void function updatePartnerViewedDate(string leadID,numeric partnerSalesPersonId){

		try{
			var updQuery=new Query();
			//updQuery.setName("updPartnerViewedDate");
			updQuery.addParam(name="leadID", value=arguments.leadID, cfsqltype="cf_sql_integer");
			updQuery.addParam(name="partnerSalesPersonId", value=arguments.partnerSalesPersonId, cfsqltype="cf_sql_integer");

			updQuery.execute(sql="UPDATE lead SET viewedByPartnerDate = getDate() WHERE leadID = :leadID and viewedByPartnerDate IS NULL and partnerSalesPersonID = :partnerSalesPersonId");
 		}
		catch(any e){
			// no op
		}
	}

}