<!--- �Relayware. All Rights Reserved 2014 --->

<!---
NJH 2013/04/26 - normalised the userlink table.. take out the personID and put into separate table which holds an entity/entityTypeID. userLink now just stores a link and entityUserLink
				stores links against people or usergroups. Altered queries below to take that into account

 --->

<cfcomponent displayname="Common myRelay Queries" hint="Contains a number of commonly used Relay Queries">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns links for THIS.personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getMyLinks">
		<cfargument name="personID" default="#request.relayCurrentUser.personID#" type="numeric">

		<cfset var myLinks = "">
		<cfset var personUserGroups = application.com.relayUserGroup.getUserGroupAllocation(personID=arguments.personID)>

		<!--- get links that are my active personal links or that are defaulted to me as a result of me being in a usergroup. Check that I have not 'opted out'
		2016/07/11 GCC added distinct 449907 --->
		<cfquery name="myLinks" datasource="#application.SiteDataSource#">
			select distinct uLink.linkID, uLink.linkLocation, uLink.linkDescription, uLink.linkAttribute1, uLinkType.linkType, moduleTextID as module
			from userLink uLink with (noLock)
				inner join userLinkType uLinkType with (noLock) on uLink.linkTypeID = uLinkType.linkTypeID
				inner join entityUserLink eul on uLink.linkId = eul.linkID
			where
				(
					(eul.entityId=#arguments.personID# and eul.entityTypeID =  <cf_queryparam value="#application.entityTypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" >  and active=1)
					or
					(eul.entityId  in ( <cf_queryparam value="#valueList(personUserGroups.userGroupID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and eul.entityTypeID =  <cf_queryparam value="#application.entityTypeID.userGroup#" CFSQLTYPE="CF_SQL_INTEGER" >  and not exists
						(select 1 from entityUserLink where linkID=uLink.linkID and entityId=#arguments.personID# and entityTypeID =  <cf_queryparam value="#application.entityTypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" >  and active=0)
					)
				)
			order by uLinkType.linkType, linkAttribute1, linkDescription
		</cfquery>
		<cfreturn myLinks>
	</cffunction>


	<cffunction name="addLink">
		<cfargument name="LinkType" type="string" required="true">
		<cfargument name="Location" type="string" required="true">
		<cfargument name="Description" type="string" required="true">
		<cfargument name="Attribute1" type="string" default = "">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.PersonID#">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">
		<cfargument name="moduleTextID" type="string" default="">

		<cfset var addUserLink = "">

		<cfquery name="addUserLink" datasource="#application.sitedatasource#">
			declare @linkTypeID int
				<cfif isNumeric(linkType)>
					select @linkTypeID =  <cf_queryparam value="#arguments.linkType#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>
					select @linkTypeID =  linkTypeID from userLinkType where linkType = <cf_queryparam value="#arguments.linkType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

			if not exists(select 1 from userlink where
				linkLocation =  <cf_queryparam value="#arguments.location#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
				linkTypeID = @linkTypeID

			)
			INSERT INTO UserLink (linkLocation,linkDescription,linkAttribute1,linkTypeID,moduleTextID)
			  VALUES (<cf_queryparam value="#arguments.location#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#arguments.attribute1#" CFSQLTYPE="CF_SQL_VARCHAR" >, @linkTypeID, <cf_queryparam value="#arguments.moduleTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >)

			select linkID from userlink where
				linkLocation =  <cf_queryparam value="#arguments.location#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
				linkTypeID = @linkTypeID
		</cfquery>

		<cfset assignLinkToEntity(entityId=arguments.entityID,entityTypeId=arguments.entityTypeID,linkID=addUserLink.linkID)>
	</cffunction>


	<cffunction name="assignLinkToEntity" access="public">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">
		<cfargument name="linkID" type="numeric" required="true">

		<cfquery name="insertLink" datasource="#application.siteDataSource#">
			if not exists(select 1 from entityUserLink where linkID=#arguments.linkID# and entityID=#arguments.entityID# and entityTypeId=#arguments.entityTypeID#)
			insert into entityUserLink (entityId,entityTypeID,linkID,createdBy,lastUpdatedBy)
			values (#arguments.entityId#,#arguments.entityTypeID#,#arguments.linkID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#)
			else
			update entityUserLink set active=1 where linkID=#arguments.linkID# and entityID=#arguments.entityID# and entityTypeId=#arguments.entityTypeID#
		</cfquery>

	</cffunction>


	<cffunction name="isLinkInMyLinks" access="public">
		<cfargument name="personID" default="#request.relayCurrentUser.personID#" type="numeric">
		<cfargument name="LinkType" type="string" required="true">
		<cfargument name="Location" type="string" required="true">
		<cfargument name="Attribute1" type="string" required="false">

		<cfset var myLinks = "">
		<cfset var linkID = getLink(argumentCollection=arguments)>
		<cfset var personUserGroups = application.com.relayUserGroup.getUserGroupAllocation(personID=arguments.personID)>

		<cfquery name="myLinks" datasource="#application.SiteDataSource#">
			select * from entityUserLink where
				linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				(
					(entityId=#arguments.personID# and entityTypeID =  <cf_queryparam value="#application.entityTypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" >  and active=1)
					or
					(entityId  in ( <cf_queryparam value="#valueList(personUserGroups.userGroupID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and entityTypeID =  <cf_queryparam value="#application.entityTypeID.userGroup#" CFSQLTYPE="CF_SQL_INTEGER" >  and not exists
						(select 1 from entityUserLink where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityId=#arguments.personID# and entityTypeID =  <cf_queryparam value="#application.entityTypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" >  and active=0)
					)
				)
		</cfquery>

		<cfif not myLinks.recordCount>
			<cfreturn false>
		</cfif>

		<cfreturn true>
	</cffunction>


	<cffunction name="getLink" returntype="numeric">
		<cfargument name="LinkType" required="true">
		<cfargument name="Location" required="true">
		<cfargument name="Attribute1" default = "">

		<cfset var getUserLink = "">
		<cfset var userLink = 0>

		<cfquery name="getUserLink" datasource="#application.sitedatasource#">
			declare @linkTypeID int
				<cfif isNumeric(linkType)>
					select @linkTypeID =  <cf_queryparam value="#arguments.linkType#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>
					select @linkTypeID =  linkTypeID from userLinkType where linkType = <cf_queryparam value="#arguments.linkType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

			select linkID from userlink where
				linkLocation =  <cf_queryparam value="#arguments.location#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
				<!--- <cfif arguments.attribute1 neq "">linkAttribute1 = <cf_queryparam value="#arguments.attribute1#" CFSQLTYPE="CF_SQL_VARCHAR" > AND</cfif> --->
				linkTypeID = @linkTypeID
		</cfquery>

		<cfif getUserLink.recordCount>
			<cfset userLink = getUserLink.linkID>
		</cfif>
		<cfreturn userLink>

	</cffunction>


	<cffunction name="deleteLink">
		<cfargument name="LinkType" required="true">
		<cfargument name="Location" required="true">
		<!--- <cfargument name="Attribute1" default = ""> --->
		<cfargument name="entityID" default="#request.relayCurrentUser.PersonID#">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">

		<cfset var removeLinkFromMyFavourite = "">

		<!---
			if it's a person - check if person is in any usergroup that has this link.. if so, set their link to be inactive. Otherwise, just delete the link
			if it's a usergroup, check if any people in the usergroup have the link inactive/deleted - if so, delete the person link as well.
		 --->
		<cfset var linkID = getLink(LinkType=arguments.LinkType,Location=arguments.Location)>
		<cfset var personUserGroups = "">
		<cfset var getUserGroupsWithLink = "">
		<cfset var deleteUserLink = "">
		<cfset var peopleInGroup = "">
		<cfset var getPeopleWithInactivatedLink = "">
		<cfset var removeLink = "">

		<cfif arguments.entityTypeID eq application.entityTypeId.person>
			<cfset personUserGroups = application.com.relayUserGroup.getUserGroupAllocation(personID=arguments.entityID)>

			<cfquery name="getUserGroupsWithLink" datasource="#application.siteDataSource#">
				select entityID from entityUserLink where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityID  in ( <cf_queryparam value="#valueList(personUserGroups.userGroupID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>

			<!--- assign it so that we can then mark it as inactive --->
			<cfif getUserGroupsWithLink.recordCount>
				<cfset assignLinkToEntity(entityID=arguments.entityID,entityTypeID=arguments.entityTypeID,linkID=linkId)>
			</cfif>

			<cfquery name="deleteUserLink" datasource="#application.siteDataSource#">
				<cfif getUserGroupsWithLink.recordCount>
				update entityUserLink set active=0,lastUpdatedBy=#request.relayCurrentUser.userGroupID#,lastUpdated=getDate()
				<cfelse>
				delete from entityUserLink
				</cfif>
				where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityId =  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="cf_sql_integer" > and entityTypeId=#arguments.entityTypeId#
			</cfquery>

		<cfelseif arguments.entityTypeID eq application.entityTypeId.userGroup>

			<cfset peopleInGroup = application.com.relayUserGroup.getUserGroupAllocation(userGroupID=arguments.entityID)>

			<cfquery name="getPeopleWithInactivatedLink" datasource="#application.siteDataSource#">
				select entityID from entityUserLink where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityID  in ( <cf_queryparam value="#valueList(peopleInGroup.personID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and active=0
			</cfquery>

			<cfquery name="deleteUserLink" datasource="#application.siteDataSource#">
				delete from entityUserLink
				where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
					(
						(entityId =  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="cf_sql_integer" > and entityTypeId=#arguments.entityTypeId#)
						<cfif getPeopleWithInactivatedLink.entityID neq "">
						or
						(entityID  in ( <cf_queryparam value="#valueList(getPeopleWithInactivatedLink.entityID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and entityTypeID =  <cf_queryparam value="#application.entityTypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" > )
						</cfif>
					)
			</cfquery>
		</cfif>

		<cfquery name="removeLink" datasource="#application.siteDataSource#">
			if not exists(select 1 from entityUserLink where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			delete from userLink where linkID =  <cf_queryparam value="#linkID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>

	<cffunction name="displayLink" access="public" hint="Displays a link with the 'my favourites' icon functionality.">
		<cfargument name="linkLocation" type="string" required="true">
		<cfargument name="linkName" type="string" required="true">
		<cfargument name="linkAttribute1" type="string" required="true">
		<cfargument name="linkType" type="string" default="report">
		<cfargument name="linkTarget" type="string" default="">
		<cfargument name="target" type="string" default="">
		<cfargument name="href" type="string" default="">
		<cfargument name="module" type="string" default="">

		<cfset var class="notIsFavorite">
		<cfset var functionName = "addToFavourites">
		<cfset var thisLinkName = application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=arguments.linkName)>
		<cfset var options = "reuseTab:true">
		<cfset var title = "phr_sys_myLinks_AddToMyLinks">

		<cfif application.com.myRelay.isLinkInMyLinks(linkType=arguments.linkType,location=arguments.linkLocation)>
			<cfset class="isFavorite">
			<cfset functionName = "removeFromFavourites">
			<cfset title = "phr_sys_myLinks_removeFromMyLinks">
		</cfif>
		<cfif arguments.href eq "">
			<cfset arguments.href=arguments.linkLocation>
		</cfif>

		<cfif arguments.module neq "">
			<cfset options = listAppend(options,"iconClass:'#lcase(arguments.module)#'")>
		</cfif>

		<cfif arguments.linkType eq "jasperReport">
			<cfset arguments.href = application.com.jasperServer.getJasperServerReportUnitPath()&arguments.href>
		</cfif>

		<cfoutput>
			<a href="##" class="#class#" onclick="#functionName#(this);" linkLocation="#arguments.linkLocation#" linkType="#arguments.linkType#" linkName="#arguments.linkName#" linkAttribute1="#arguments.linkAttribute1#" module="#htmlEditFormat(arguments.module)#" title="#htmlEditFormat(title)#"></a>
			<cfif arguments.target eq "">
				<A HREF="##" TARGET="#arguments.target#" CLASS="smallLink" onClick="javascript:openNewTab('#thisLinkName#','#arguments.linkName#','#arguments.href#',{#options#});return false;">#htmleditformat(thisLinkName)#</A>
			<cfelse>
				<A HREF="#arguments.href#" TARGET="#arguments.target#" CLASS="smallLink">#htmleditformat(thisLinkName)#</A>
			</cfif>
		</cfoutput>

	</cffunction>

</cfcomponent>
