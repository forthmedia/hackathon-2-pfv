<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="bulkSMS" hint="I interface with bulksms.co.uk to manage SMS">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="sendMessage" hint="I send an SMS message.">
		<cfargument name="message" type="string" required="yes">
		<cfargument name="msisdn" type="string" required="yes">
		<cfargument name="username" type="string" default="simonwj">
		<cfargument name="password" type="string" default="foundat1on">
		
		
		<cfscript>
			var sendMessage = "";
		</cfscript>
		
		<cfhttp url="http://www.bulksms.co.uk:5567/eapi/submission/send_sms/2/2.0"
        method="GET"
        result="thisResult">
			<cfhttpparam type="URL" name="username" value="#arguments.username#">
			<cfhttpparam type="URL" name="password" value="#arguments.password#">
			<cfhttpparam type="URL" name="message" value="#arguments.message#">
			<cfhttpparam type="URL" name="msisdn" value="#arguments.msisdn#">
		</cfhttp> 
		
		<cfreturn thisResult>
	</cffunction>

</cfcomponent>