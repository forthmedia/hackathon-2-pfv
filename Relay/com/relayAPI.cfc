<!--- �Relayware. All Rights Reserved 2014

2015-01-21  WAB	CASE 443542	Fix problem in getStatusCodeAndMessage(), name of required field not being returned
2015-02-17	RPW	FIFTEEN-137 - Relayware API returns "Malformed JSON" when updating a record that makes no change
2015-02-23  RJT	Added hook code for custom OnAPIAccess code, allows for custom behaviour when the API is used (e.g. sending emails)
2015-02-23	RPW	FIFTEEN-219 - API - [DELETE] command is flagging an organisation for deletion even when the org has an [HQ] location associated with it.
2015-01-29	GCC	mods to cleanUpApiSessions()
2015-12-14  AHL Displaying the entity ID on 'ENTITY_EXISTS' ERRORs
2015-12-14  RJT Give response to delete request rather than empty message
2016-01-11	RJT Additional validation checks to ensure that required fields the API user doesn't have access to are still checked against
2016-01-13	RJT Organisations special case API delete to do all the rights checks up front to avoid "half deleting" an organsation
2016-09-19	RJT PROD2016-2339 Added the rights session initialisation. Needed for deleting organisations (which checks rights)
2016-10-13 	PPB PROD2016-2512 syntax issue - moved a bracket
--->


<cfcomponent output="false">

	<cfset this.sessionInactivityInMinutes = 15> <!--- any session that is inactive after this interval should be classed as expired --->

	<cffunction name="createUserSession" access="public" returnType="string" output="false">
		<cfargument name="personID" type="numeric" required="true">

		<cfset var insertUserSession = "">

		<cfquery name="insertUserSession" datasource="#application.siteDataSource#">
			declare @sessionID varchar(50)
			select @sessionID = replace(newID(),'-','');
			insert into apiSession (personId,sessionID) values (#arguments.personID#,@sessionID)

			select @sessionID as sessionID
		</cfquery>

		<!--- if a user has logged in with a new session, then let's expire their earlier sessions so that they only ever have one active session at a time--->
		<!---2015/06/11 GCC after discussions with WAB and NJH removing this as it stops customers using clusters / multiple servers wth the same credentials from interfacing and
		causes them each to keep invalidating each others sessions! Security risks are minimal --->
		<!---<cfquery name="expireOldSessions" datasource="#application.siteDataSource#">
			update apiSession
				set expired=getDate()
			where personID = <cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER">
				and sessionId != <cf_queryparam value="#insertUserSession.sessionID#" cfsqltype="CF_SQL_VARCHAR">
				and expired is null
		</cfquery>

		<cfset cleanUpApiSessions(personID=arguments.personId)>--->

		<cfreturn insertUserSession.sessionID>

	</cffunction>


	<!--- NJH 2012/03/22 - check whether the existing sessionId that is passed through is valid. We check to see that it has not expired
		and that the last request made on the token was made within the last #this.sessionInactivityInMinutes# minutes.
		Also check that we're below the number of allowed hits in a day.
		We accept a session of it is less than 24 hours old, or if it's older than 24 hours, but there is activity against that sessionID in the last 15 minutes.
		We don't want to kill a session in the middle of the apiUser doing some major processing
	--->
	<cffunction name="isSessionValid" access="public" returnType="struct" output="false">
		<cfargument name="sessionID" type="string" required="true">

		<cfset var result = {isValid=false,personID=404,errorCode=""}>
		<cfset var getNumberOfRequestsForToday = "">
		<cfset var doesSessionExist = "">

		<cfquery name="doesSessionExist" datasource="#application.siteDataSource#">
			select personID from apiSession s with (noLock)
				left join (
					select r.sessionID from apiSession s with (noLock)
						inner join apiRequest r with (noLock) on s.sessionID=r.sessionID and s.expired is null
					group by r.sessionID
					having datediff(n,max(r.startTime),getDate())  <=  <cf_queryparam value="#this.sessionInactivityInMinutes#" CFSQLTYPE="cf_sql_integer" > ) r
				on r.sessionID = s.sessionID
			where s.sessionID=<cf_queryparam value="#arguments.sessionID#" cfsqltype="CF_SQL_VARCHAR">
				and s.expired is null
				and (
					(datediff(hh,s.created,getDate()) <= 24)
						or
					(datediff(hh,s.created,getDate()) > 24 and r.sessionID is not null)
					)
		</cfquery>

		<cfset result.isValid = doesSessionExist.recordCount>

		<cfif result.isValid>
			<cfquery name="getNumberOfRequestsForToday" datasource="#application.siteDataSource#">
				select count(1) as numRequests from apiRequest with (noLock)
				where personID =  <cf_queryparam value="#doesSessionExist.personID#" CFSQLTYPE="CF_SQL_INTEGER" >  and startTime > dateAdd(dd,0,dateDiff(dd,0,getDate()))
			</cfquery>

			<cfif getNumberOfRequestsForToday.numRequests lte application.com.settings.getSetting("api.maxRequests")>
				<cfset result.personID = doesSessionExist.personID>
			<cfelse>
				<cfset result.errorCode = "NUM_REQUESTS_EXCEEDED">
			</cfif>
		<cfelse>
			<cfset result.errorCode = "INVALID_SESSIONID">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="initialiseApiUser" access="public" output="false">
		<cfargument name="sessionID" type="string" required="true">
		<cfargument name="personID" type="numeric" required="false">

		<cfset request.relayCurrentUser.sessionID=arguments.sessionID>

		<cfif not structKeyExists(arguments,"personID")>
			<cfquery name="getPersonID" datasource="#application.siteDataSource#">
                           select personID from apiSession with (noLock)
                           where sessionID = <cf_queryparam value="#arguments.sessionID#" cfsqltype="CF_SQL_VARCHAR">
			</cfquery>

			<cfset arguments.personID = getPersonID.personID>
		</cfif>

		<cfquery name="getPersonDetails" datasource="#application.siteDataSource#">
                     select firstname+' '+lastname as name, countryID, p.locationid from person p with (noLock) inner join location l with (noLock)
                           on p.locationID = l.locationId
                     where p.personID = #arguments.personID#
		</cfquery>

		<cfset request.relayCurrentUser.isInternal = true>
		<cfset request.relayCurrentUser.personID = arguments.personID>
		<cfset request.relayCurrentUser.locationID = getPersonDetails.locationID>
		<cfset request.relayCurrentUser.userGroupID = application.com.relayUserGroup.getUserGroupAllocation(personId=arguments.personID,UserGroups="API").userGroupID>
		<cfset request.relayCurrentUser.CountryList = application.com.rights.getCountryRightsIDList(arguments.personID)>
		<cfif request.relayCurrentUser.CountryList eq "">
			<cfset request.relayCurrentUser.CountryList = 0>
		</cfif>
		<cfset request.relayCurrentUser.isApiUser = true>
		<cfset request.relayCurrentUser.languageID=1> <!--- for the error handler --->
		<cfset request.relayCurrentUser.countryID=getPersonDetails.countryID>
		<cfset request.relayCurrentUser.fullName = getPersonDetails.name>
		<cfset application.com.login.initUserSession(request.relaycurrentuser.personid)>
	</cffunction>

	<cffunction name="cleanUpApiSessions" access="public" returnType="struct" output="false">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="applicationScope" default="#application#"> <!--- WAB added 2012-10-05 need because run as a housekeeping task --->

		<cfset var expireAPISessions = "">
		<cfset var deleteSessionsOlderThanAYear = "">
		<cfset var dropExpiredSessionTables = "">
		<cfset var result = {isOk=true}>

		<!--- If the session is more than a day old and there hasn't been activity in the last 15 minutes, we expire the session --->
		<cftry>
			<cfquery name="expireAPISessions" datasource="#arguments.dataSource#">
				update apiSession
					set expired=getDate()
				from apiSession s with (noLock)
					left join (
						select r.sessionID from apiSession s with (noLock)
							inner join apiRequest r with (noLock) on s.sessionID=r.sessionID and s.expired is null
						group by r.sessionID
						having datediff(n,max(r.startTime),getDate())  >  <cf_queryparam value="#this.sessionInactivityInMinutes#" CFSQLTYPE="cf_sql_integer" >) r
					on r.sessionID = s.sessionID
				where datediff(hh,created,getDate()) >= 24
					and expired is null
					and (r.sessionID is not null or datediff(n,s.created,getDate())  >  <cf_queryparam value="#this.sessionInactivityInMinutes#" CFSQLTYPE="cf_sql_integer" > )
			</cfquery>

			<cfquery name="deleteSessionsOlderThanAYear" datasource="#arguments.dataSource#">
				delete apiRequest
				from apiRequest r inner join apiSession s
					on r.sessionID = s.sessionID
				where datediff(d,s.created,getDate()) >= 365

				delete from apiSession
				where datediff(d,created,getDate()) >= 365
			</cfquery>

			<cfquery name="dropExpiredSessionTables" datasource="#arguments.dataSource#">
				declare @tablename varchar(250)
				declare @dropTableSql varchar(500)

				declare tempTableCursor cursor for select tablename from apiSession s with (noLock)
					inner join cachedQuery c with (noLock) on s.sessionID = c.sessionID
					where s.expired <= getDate() --2015/02/11 GCC get everything expired in the past, not just last 10 seconds
						<cfif structKeyExists(arguments,"personID")>and s.personID=<cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER"></cfif>

				open tempTableCursor
				fetch next from tempTableCursor into @tablename
				while @@fetch_status = 0
				begin
					set @dropTableSql = '
					if object_ID(''tempdb..'+@tablename+''') is not null
					Begin
						drop table '+@tablename+'
					End
					delete from cachedQuery where tablename='''+@tablename+''''

					exec(@dropTableSql)

					fetch next from tempTableCursor into @tablename
				end
				close tempTableCursor
				deallocate tempTableCursor
			</cfquery>

			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.message = cfcatch.message&" "&cfcatch.detail>
				<cfset warningStruct = arguments><cfset structdelete (warningStruct,"applicationScope")>
				<cfset applicationScope.com.errorHandler.recordRelayError_Warning(type="relayAPI Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=warningStruct)>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="logRequestStart" access="public" output="false" hint="Logs the beginning of the request">
		<cfargument name="verb" type="string" required="true"/>
		<cfargument name="cfc" type="string" required="true"/>
		<cfargument name="requestArguments" type="struct" default="#structNew()#"/>
		<cfargument name="personID" type="numeric" required="true"/>
		<cfargument name="sessionID" type="string" required="true"/>

		<cfif structKeyExists(requestArguments,"password")>
			<cfset arguments.requestArguments=StructCopy(arguments.requestArguments)> <!--- We can't touch the actual arguments--->
			<cfset arguments.requestArguments.password="********"> <!---Scrub passwords from log --->
		</cfif>

		<cfquery name="insertRequest" datasource="#application.siteDataSource#">
			insert into apiRequest (
				method,
				component,
				arguments,
				startTime,
				personID,
				IP,
				sessionID)
			values (
				<cf_queryparam value="#arguments.verb#" cfsqltype="CF_SQL_VARCHAR">,
				<cf_queryparam value="#arguments.cfc#" cfsqltype="CF_SQL_VARCHAR">,
				<cf_queryparam value="#application.com.structureFunctions.convertStructureToNameValuePairs(struct=arguments.requestArguments)#" cfsqltype="CF_SQL_VARCHAR">,
				getDate(),
				<cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER">,
				<cf_queryparam value="#cgi.REMOTE_ADDR#" cfsqltype="CF_SQL_VARCHAR">,
				<cf_queryparam value="#arguments.sessionID#" cfsqltype="CF_SQL_VARCHAR">)

			select scope_identity() as requestID
		</cfquery>

		<cfset request.apiRequestID = insertRequest.requestID>
	</cffunction>


	<cffunction name="updateRequestLog" access="public" output="false" hint="Updates an api request">
		<cfargument name="data" required="true">
		<cfargument name="statusCode" required="true">

		<cfset var updateRequestLog = "">

		<cfquery name="updateRequestLog" datasource="#application.siteDataSource#">
			update apiRequest
				set data = <cf_queryparam value="#SerializeJSON(arguments.data)#" cfsqltype="CF_SQL_VARCHAR">,
					statusCode = <cf_queryparam value="#arguments.statusCode#" cfsqltype="CF_SQL_INTEGER">
			where ID = <cf_queryparam value="#request.apiRequestID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
	</cffunction>


	<cffunction name="logRequestEnd" access="public" output="false" hint="Logs the end of the request">

		<cfset var updateRequestLog = "">

		<cfquery name="updateRequestLog" datasource="#application.siteDataSource#">
			update apiRequest
				set endTime=getDate()
			where ID = <cf_queryparam value="#request.apiRequestID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
	</cffunction>


	<!--- function to handle the error messages and status codes for all api responses --->
	<cffunction name="prepareApiResponse" access="public" returnType="struct" output="false">
		<cfargument name="result" type="struct" required="true">
		<cfargument name="errorCode" type="string" required="true">

		<cfset var response = structNew()>
		<cfset var validReturnFields = "message,detailMessage,field,recordSet,inserted,updated,deleted,entityID,sessionID,hasMore,queryId,recordCount,totalRecordCount,organization,location,person,opportunity,opportunityProduct,lead,oppStage,oppStatus,country,product,productGroup,pricebook,pricebookEntry,productCategory,productFamily,files,SODMaster,trngUserModuleProgress,trngModule,phraseID,phrasetextID,translations">
		<!--- RJT2 Added these as part of API phrases extension PROD2016 -2198 --->
		<cfset listAppend( validReturnFields , "phraseID,phrasetextID,translations" )>
		<cfset var returnField = "">
		<cfset var message = "">
		<cfset var statusCodeAndMessage = structNew()>

		<cfset response.result = arguments.result>

		<cfset statusCodeAndMessage = getStatusCodeAndMessage(errorCode=arguments.errorCode,result=response.result)>
		<cfset message = statusCodeAndMessage.message>
		<cfset response.result.errorCode = arguments.errorCode>

		<cfset response.statusCode = statusCodeAndMessage.statusCode>

		<cfif listFindNoCase("ENTITIES_UPDATED,ENTITY_UPDATED,ENTITY_INSERTED,ENTITIES_INSERTED",arguments.errorCode)>
			<cfset validReturnFields = "inserted,updated,entityID">
		</cfif>

		<!--- there is a problem so we return the errorcode and a message --->
		<cfif arguments.errorCode neq "" and not listFind("201,204,200",response.statusCode)>
			<cfset response.result = structNew()>
			<cfset response.result.errorCode = arguments.errorCode>
			<cfset response.result.message = message>

			<cfif structKeyExists(arguments.result, "detailMessage")>
				<cfset response.result.detailMessage=arguments.result.detailMessage>
			</cfif>
			<cfif structKeyExists(arguments.result, "reason")>
				<cfset response.result.reason=arguments.result.reason>
			</cfif>

			<!--- BEGINING 2015-12-14 AHL Displaying the entity ID on 'ENTITY_EXISTS' ERRORs --->
			<cfif arguments.errorCode eq 'ENTITY_EXISTS'>
				<cfif StructKeyExists(result,'entitiesMatchedList')>
					<cfset response.result.entitiesMatchedList = result.entitiesMatchedList/>
				</cfif>
			</cfif>
			<!--- END 2015-12-14 AHL Displaying the entity ID on 'ENTITY_EXISTS' ERRORs --->

		<!--- remove message and errorCode if everything is fine --->
		<cfelseif structKeyExists(response.result,"errorCode")>
			<!--- <cfif response.result.errorCode neq ""> --->
				<cfloop list="#structKeyList(response.result)#" index="returnField">
					<cfif not listFindNoCase(validReturnFields,returnField)>
						<cfset structDelete(response.result,returnField)>
					</cfif>
				</cfloop>
			<!--- </cfif> --->
		</cfif>

		<cfreturn response>

	</cffunction>


	<cffunction name="getEntity" access="public" returntype="struct" output="true">
		<cfargument name="entityID" type="string" required="false">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="fieldList" type="string" default="">
		<cfargument name="queryID" type="string" required="false">
		<cfargument name="filter" type="string" required="false">

		<cfset var result = {errorCode=""}>
		<cfset var qryResult = "">
		<cfset var whereClause = "">
		<cfset var cachedQueryTableNamePrefix = "####cachedQuery">
		<cfset var cacheResultsIfOverThreshold = false>

		<!--- check if numeric entityID passed, give a friendly error message if it isn't numeric --->
		<cfif structKeyExists(arguments,"entityID") and not isNumeric(arguments.entityID)>
			<cfset result.errorCode = "INVALID_DATA_TYPE">
			<cfset result.field = application.entityType[arguments.entityTypeID].uniqueKey>
			<cfset result.fieldValue = arguments.entityID>
		</cfif>

		<cfif result.errorCode eq "">
			<cftry>
				<!--- if we're getting a single entity, don't bother with the where clause --->
				<cfif not structKeyExists(arguments,"entityID")>
					<cfif structKeyExists(arguments,"filter")>
						<cfset whereClause = arguments.filter>
						<cfif not application.com.security.checkStringForInjection(string=whereClause,type="dangerousSql").isOK>
							<cfset result.errorCode = "INVALID_SQL">
						</cfif>
					</cfif>
					<cfset cacheResultsIfOverThreshold = true>
				</cfif>

				<cfif result.errorCode eq "">
					<!--- if no columns have been passed through, then return all the columns available for that given object --->
					<cfif arguments.fieldList eq "">
						<cfset arguments.fieldList = application.com.rights.getEntityFieldsForPersonAndMethod(haveRightsForMethod="view",entityTypeID=arguments.entityTypeID,returnApiName=true)>
					</cfif>

					<cfset structAppend(result,validateColumnsForMethod(columnList=arguments.fieldList,entityTypeID=arguments.entityTypeID),true)>

					<cfif result.errorCode eq "">
						<!--- if we're doing a bulk get and a queryId has been passed through, then get the records from the cached query table --->
						<cfif structKeyExists(arguments,"queryID") and not structKeyExists(arguments,"entityID")>
							<cfset qryResult = application.com.dbTools.getRecordsFromCachedQuery(tempTable=cachedQueryTableNamePrefix&arguments.queryID)>
						<cfelse>
							<cfset qryResult = application.com.relayEntity.getEntity(argumentCollection=arguments,useFriendlyName=true,cacheResultsIfOverThreshold=cacheResultsIfOverThreshold,whereClause=whereClause,pageSize=application.com.settings.getSetting("api.queryBatchSize"),testUserEntityRights=true)>
						</cfif>

						<cfset result.errorCode = qryResult.errorCode>
						<cfif qryResult.isOk>
							<cfset result.recordSet = qryResult.recordSet>

							<!--- if we're dealing with a collection, then add different things to the result, such as recordCount,hasmore and the queryId --->
							<cfif not structKeyExists(arguments,"entityID")>
								<cfset result.recordCount = qryResult.recordCount>
								<cfset result.totalRecordCount = qryResult.totalRecordCount>
								<cfif structKeyExists(qryResult,"tablename")>
									<cfset result.queryId = replaceNoCase(qryResult.tablename,cachedQueryTableNamePrefix,"")>
								</cfif>
								<cfif structKeyExists(qryResult,"hasMore")>
									<cfset result.hasMore = qryResult.hasMore>
								</cfif>
							</cfif>
						<cfelse>
							<cfset structAppend(result,qryResult,true)>
						</cfif>
					</cfif>
				</cfif>

				<cfcatch type="any">
					<!--- TODO: need to put in better error code and log relayError --->
					<cfset result.errorCode = "UNKNOWN_EXCEPTION">
					<cfset result = cfcatch>
					<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayAPI Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn prepareApiResponse(result=result,errorCode=result.errorCode)>
	</cffunction>
	
	<cffunction name="upsertFile" access="public" output="false" returntype="struct">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="entityID" type="string" required="false">
		<cfargument name="method" type="string" default="insert" hint="Insert or Update">
		<cfscript>
			if (structKeyExists(entityDetails,"fileName") AND Find("+",entityDetails.fileName) NEQ 0){
				var result = {errorCode="INVALID_DATA", message="+ is not a valid character in the filename"};
				return {result=result, statusCode=422};
			}
		</cfscript>
		


		<cfreturn upsertEntity(argumentCollection=arguments)>
		
	</cffunction>

	<cffunction name="upsertEntity" access="public" returntype="struct" output="false" hint="Can be used to insert or update an entity">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="entityID" type="string" required="false">
		<cfargument name="method" type="string" default="insert" hint="Insert or Update">

		<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeId=arguments.entityTypeId)>
		<cfset var result = {errorCode=""}>
		<cfset var methodResult = structNew()>
		<cfset var methodArgs = structNew()>
		<cfset var structField = "inserted">
		<cfset var entityMethodName = "">

		<!--- check if numeric entityID passed, give a friendly error message if it isn't numeric --->
		<cfif structKeyExists(arguments,"entityID") and (not isNumeric(arguments.entityID))>
			<cfset result.errorCode = "INVALID_DATA_TYPE">
			<cfset result.field = application.entityType[arguments.entityTypeID].uniqueKey>
			<cfset result.fieldValue = arguments.entityID>
		</cfif>

		<cfif result.errorCode eq "">
			<cftry>
				<!--- getting rid of some unwanted keys when dealing with functions below --->
				<cfset structDelete(arguments.entityDetails,"entityID")>
				<cfif structKeyExists(arguments.entityDetails,"fieldNames")>
					<cfset structDelete(arguments.entityDetails,"fieldNames")> <!--- passed in on a post (update) --->
				</cfif>

				<!--- we need to remove undefined struct keys as it poses a problem when returning the data in xml format, but also
					when arguments get returned --->
				<cfset application.com.structureFunctions.removeUndefinedStructKeys(data=arguments.entityDetails)>
				<cfset structAppend(result,validateColumnsForMethod(columnList=structKeyList(arguments.entityDetails),entityTypeID=arguments.entityTypeID),true)>

				<cfif result.isOK and arguments.method eq "insert">
					<cfset structAppend(result,doRequiredColumnsExistForEntity(columnList=structKeyList(arguments.entityDetails),entityTypeID=arguments.entityTypeID),true)>
				</cfif>

				<cfif result.isOk>
					<cfset entityMethodName = "#arguments.method##entityTypeStruct.tablename#">
					<cfif arguments.method eq "update">
						<cfset entityMethodName = "#entityMethodName#Details">
					</cfif>

					<cfif structKeyExists(application.com.relayEntity,entityMethodName)>
						<cfset methodArgs["#entityTypeStruct.tablename#Details"] = arguments.entityDetails>
						<cfset methodArgs.testUserEntityRights = true>
						<cfif arguments.method eq "update">
							<cfset methodArgs[entityTypeStruct.uniqueKey] = arguments.entityID>
						<cfelse>
							<cfset methodArgs.insertIfExists = false>
						</cfif>

						<cfinvoke component=#application.com.relayEntity# method=#entityMethodName# argumentCollection=#methodArgs# returnvariable="methodResult">
					<cfelse>

						<cfif arguments.method eq "insert">
							<cfset methodResult = application.com.relayEntity.insertEntity(argumentCollection=arguments,testUserEntityRights=true)>
						<cfelse>
							<cfset methodResult = application.com.relayEntity.updateEntityDetails(argumentCollection=arguments,testUserEntityRights=true)>
						</cfif>
					</cfif>

					<cfset structAppend(result,methodResult,true)>
					<!--- on a successful insert, stick the url to the new entityID in the response header --->
					<cfif methodResult.isOK and arguments.method eq "insert">
						<cfheader name="location" value="/api/v#request.version#/#request.entity#/#methodResult.entityID#">
					</cfif>
				</cfif>

				<cfcatch type="any">
					<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayAPI Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
					<cfset result.errorCode = "UNKNOWN_EXCEPTION">
				</cfcatch>
			</cftry>
		</cfif>

		<cfscript>
			customBehaviourHandler=new RelayAPI.CustomPostAPIBehaviourHandler();
			switch (LCase(method)) {
				case "insert":
					if (result.ISOK AND structKeyExists(result,"entityID")){
						customBehaviourHandler.handleCustomBehaviour_insert(entityTypeStruct.tablename,result.entityID,result.ISOK, entityDetails);
					}else{
						//we won't have an entityID if the result isn't ok.
						// 2015-12-14 AHL Previous assumption is wrong for 'ENTITY_EXISTS' ERRORs
						customBehaviourHandler.handleCustomBehaviour_insert(entityTypeStruct.tablename,-1,result.ISOK, entityDetails);
					}

					break;
				case "update":
					if (result.ISOK){
						customBehaviourHandler.handleCustomBehaviour_update(entityTypeStruct.tablename,result.entityID,result.ISOK, entityDetails);
					}
					break;
			}

		</cfscript>

		<cfreturn prepareApiResponse(result=result,errorCode=result.errorCode)>
	</cffunction>


	
	<cffunction name="upsertEntities" access="public" output="false" returntype="struct">
		<cfargument name="entitiesArray" type="any" required="true"> <!--- we need an array, although it could be passed through as xml or json.. so, allow anything in and then check its type --->
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="method" type="string" default="update">

		<cfset var entityType = application.com.relayEntity.getEntityType(entityTypeId=arguments.entityTypeId).tablename>
		<cfset var result = {errorCode="ENTITY_UPSERTED"}>
		<cfset var columnList = "">
		<cfset var entityStruct = structNew()>
		<cfset var entityField = "">

		<cftry>
			<cfif isJson(arguments.entitiesArray)>
				<cfset arguments.entitiesArray = deserializeJSON(arguments.entitiesArray)>
			<cfelseif isWddx(arguments.entitiesArray)>
				<cfwddx action="wddx2cfml" input="#arguments.entitiesArray#" output="arguments.entitiesArray">
			</cfif>

			<cfif isArray(arguments.entitiesArray)>

				<cfif arrayLen(arguments.entitiesArray) lte application.com.settings.getSetting("api.upsertBatchSize")>
					<!--- build up a list of distinct column names --->
<!--- 					<cfloop array="#arguments.entitiesArray#" index="entityStruct">
						<cfloop collection="#entityStruct#" item="entityField">
							<cfif not listFindNoCase(columnList,entityField)>
								<cfset columnList = listAppend(columnList,entityField)>
							</cfif>
						</cfloop>
					</cfloop>
					<!--- ensure that the columns are valid column names.. not checking here whether they are updateable.. that is done later --->
					<cfset structAppend(result,validateColumnsForMethod(columnList=columnList,method="update",entityTypeID=arguments.entityTypeID,returnApiName=true),"true")>

					<cfif result.isOk>--->
						<cfset result = application.com.relayEntity.upsertEntities(argumentCollection=arguments,method=arguments.method,returnApiName=true)>
					<!--- </cfif> --->

				<cfelse>
					<cfset result.errorCode = "DATA_LIMIT_EXCEEDED">
				</cfif>
			<cfelse>
				<cfset result.errorCode = "INVALID_DATA_TYPE">
			</cfif>

			<cfcatch type="any">
				<cfset result.errorCode = "UNKNOWN_EXCEPTION">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayAPI Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn prepareApiResponse(result=result,errorCode=result.errorCode)>
	</cffunction>


	<cffunction name="deleteEntity" access="public" output="false" returntype="struct">
		<cfargument name="entityID" type="string" required="true">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="harddelete" type="boolean" default="false">

		<cfset var result = {errorCode="", deleted=false}>

		<!--- check if numeric entityID passed, give a friendly error message if it isn't numeric --->
		<cfif not isNumeric(arguments.entityID)>
			<cfset result.errorCode = "INVALID_DATA_TYPE">
			<cfset result.field = application.entityType[arguments.entityTypeID].uniqueKey>
			<cfset result.fieldValue = arguments.entityID>
		</cfif>

		<cfscript>
			if (result.errorCode eq ""){
				var deletionReport=application.com.relayEntity.canEntityBeDeleted(entityID=entityID,entityTypeID=entityTypeID );
				if (not deletionReport.canBeDeleted){
					result.errorCode='ENTITY_CANNOT_BE_DELETED';
					result.detailMessage=arrayToList(deletionReport.reasons_friendlyText);
				}

			}

		</cfscript>



		<cfif result.errorCode eq "">
			<cfset result = application.com.relayEntity.deleteEntity(entityid=arguments.entityID,entityTypeID=arguments.entityTypeID,hardDelete=arguments.hardDelete)>
			<cfset result.deleted=result.entityDeleted> <!---This is consident with the other approved terms --->
		</cfif>

		<cfif structKeyExists(result,"isOk") AND result.isOk>
			<cfset customBehaviourHandler=new RelayAPI.CustomPostAPIBehaviourHandler()>
			<cfset customBehaviourHandler.handleCustomBehaviour_delete(application.entityType[arguments.entityTypeID].tablename,entityID,result.ISOK)>
		</cfif>



		<cfif result.deleted EQ true>
			<cfset result.message="Entity #entityID# deleted">
		<cfelseif structKeyExists(result, "errorID")>
			<cfset result.message="Error occured: #result.errorID#">
		</cfif>


		<cfreturn application.com.relayAPI.prepareApiResponse(result=result,errorCode=result.errorCode)>
	</cffunction>


	<cffunction name="validateColumnsForMethod" type="public" output="false" returntype="struct" hint="Determine whether the fields that have been passed through are allowed.">
		<cfargument name="columnList" type="string" required="true">
		<cfargument name="entityTypeID" type="string" required="true">

		<cfset var result = {isOK=true,errorCode="",invalidFields=""}>
		<cfset var column = "">
		<cfset var sortedColumnList = listSort(arguments.columnList,"textNoCase","asc")>
		<cfset var getAllowedColumnsForMethod = "">
		<cfset var entityType = application.com.relayEntity.getEntityType(entityTypeId=arguments.entityTypeId)>
		<cfset var allowedFields = "">

		<cftry>
			<cfset fieldStruct = application.com.rights.getEntityFieldRights(entityTypeID=arguments.entityTypeId,returnApiName=true)>
			<cfset allowedFields = structKeyList(fieldStruct)>
			<cfloop list="#sortedColumnList#" index="column">
				
				<cfif not listFindNoCase(allowedFields,column)>
					<cfset result.errorCode = "INVALID_FIELD">
					<cfset result.isOk = false>
					<cfset result.invalidFields = listAppend(result.invalidFields,column)>
					<!--- <cfbreak> --->
				</cfif>
			</cfloop>

			<cfcatch>
				<cfset result = cfcatch>
				<cfset result.errorCode = "UNKNOWN_EXCEPTION">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayAPI Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="doRequiredColumnsExistForEntity" type="public" output="false" returntype="struct" hint="Determine whether all required fields have been passed through.">
		<cfargument name="columnList" type="string" required="true">
		<cfargument name="entityTypeID" type="string" required="true">

		<cfset var result = {isOK=true,errorCode="",missingRequiredFields = ""}>
		<cfset var column = "">
		<cfset var entityType = application.com.relayEntity.getEntityType(entityTypeId=arguments.entityTypeId)>
		<cfset var uniqueKey=application.entityType[arguments.entityTypeID].uniqueKey>
		<cfset var table=application.entityType[arguments.entityTypeID].tablename>
		<cfset var entityMetaData=application.com.relayEntity.getEntityFieldMetaData(tableName=entityType.tablename)>
		<cfset var entityFields = "">


		<!---Because the behaviour of RelayEntity is quite complex we have to check column requirement both with and without
			privilage checks.

			A user is required to:
			- provide values for all required values without defaults
			- and provide values for all required values which the user has access to irrespective of defaults.

			This seems strange and is something I'd like to clear up in future versions.
		 --->

		<cfscript>


			var missingFieldsSet=arrayNew(1);

			var entityMetaData = application.com.relayEntity.getEntityMetaData(entityTypeId=arguments.entityTypeId,returnApiName=true)["#table#"];


			//check with rights
			for (key in entityMetaData){
				var entry=entityMetaData[key];
				if (not entry.isNullable and entry.edit){


					if (not ListContainsNoCase(arguments.columnList,key)){
						//we don't have the field but we should
						missingFieldsSet.add(key);
						result.isOk = false;
						result.errorCode = "REQUIRED_DATA_MISSING";
					}

				}
			}


			//check without rights (i.e. fields we can't even see but must set anyway - i.e. we can't create entities)

			var requiredFieldsQuery=new query();

		    requiredFieldsQuery.setDatasource(application.siteDataSource);
		    requiredFieldsQuery.setName("requiredFieldsQuery");
		    requiredFieldsQuery.addParam(name="tablename",value=table,cfsqltype="cf_sql_varchar");
		    requiredFieldsQuery.addParam(name="uniqueKey",value=uniqueKey,cfsqltype="cf_sql_varchar");
			requiredFieldsQuery.addParam(name="crudColumns",value=application.com.relayEntity.getCrudColumns(),cfsqltype="cf_sql_varchar", list="true");

			queryResult = requiredFieldsQuery.execute(sql="select name,apiName "&
													  "from vFieldMetaData "&
													  "where tablename = :tablename " &
													  "and IsNullable=0 " &
													  "and defaultValue is null " &
													  "and name != :uniqueKey " &
													  "and name not in (:crudColumns) "
				);

			metaInfo = queryResult.getPrefix();
			resultSet=queryResult.getResult();


			for(var i=1; i<=resultSet.recordCount; i=i+1){
				//check we passed all the required records
				var testColumnName=resultSet["apiName"][i] EQ ""?resultSet["name"][i] :resultSet["apiName"][i];
				if (not ListContainsNoCase(arguments.columnList,testColumnName)){
					//we don't have the field but we should

					var alreadyKnow=ArrayFindNoCase(missingFieldsSet,testColumnName);

					if (not alreadyKnow){
						missingFieldsSet.add(testColumnName);
						result.isOk = false;
						result.errorCode = "REQUIRED_DATA_MISSING";


						param name="result.detailMessage" default="You do not have permission for some of the required fields. You will need to request access to then to create new entrys. Note you will need to reauthenticate after these fields are authorised. These fields are:";
						result.detailMessage=result.detailMessage & " #testColumnName#";
					}

				}
			}

 



			if (len(result.missingRequiredFields)>0){																										/* 2016-10-13 PPB PROD2016-2512 syntax issue - moved a bracket */
				//we definately have missing fields, they may also be not available to us. We validate this to avoid giving a confusing error message
				var entityMetaData = application.com.relayEntity.getEntityMetaData(entityTypeId=arguments.entityTypeId,returnApiName=true)["#table#"];

				var missingRequiredFieldsArray=listToArray(result.missingRequiredFields);
				for(var i=1;i<=arrayLen(missingRequiredFieldsArray);i++){
					if (not structKeyExists(entityMetaData,missingRequiredFieldsArray[i])){
						//we don't even have access

						param name="result.detailMessage" default="You do not have permission for some of the required fields. You will need to request access to then to create new entrys. Note you will need to reauthenticate after these fields are authorised. These fields are:";

						result.detailMessage=result.detailMessage & " #missingRequiredFieldsArray[i]#";
					}
				}

			}

			if (not missingFieldsSet.isEmpty()){

				result.missingRequiredFields=arrayToList(missingFieldsSet);
			}

		</cfscript>



		<cfreturn result>
	</cffunction>


	<cffunction name="getStatusCodeAndMessage" access="public" returnType="struct">
		<cfargument name="errorCode" type="string" required="true">
		<cfargument name="result" type="struct" default="#structNew()#">

		<cfset var statusCodeAndMessage = {statusCode="",message=""}>
		<cfset var statusCode = 200>
		<cfset var message = "">
		<cfset var fieldMessage = "">
		<cfset var fieldValue = "">
		<cfset var errorStatusCode = 500>

		<cfswitch expression="#arguments.errorCode#">
			<cfcase value="NON_UNIQUE_EMAIL">
				<cfset statusCode = errorStatusCode>
				<cfset message = "The email address is not unique.">
			</cfcase>
			<cfcase value="ENTITY_EXISTS"> <!--- used when inserting --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "The entity already exists">
			</cfcase>
			<cfcase value="NONEXISTENT_ENTITY"> <!--- used when updating --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "The entity does not exist">
			</cfcase>
			<cfcase value="INSUFFICIENT_RIGHTS">
				<cfset statusCode=403>
				<cfset fieldMessage = "">
				<cfif structKeyExists(arguments.result,"field")>
					<cfset fieldMessage = " on field '#arguments.result.field#'">
				<cfelseif structKeyExists(arguments.result,"entityID")>
					<cfset fieldMessage = " on record with ID '#arguments.result.entityID#'">
				</cfif>
				<cfset message = "Insuffient rights#fieldMessage#">
			</cfcase>
			<cfcase value="INVALID_REFERENCE_ID"> <!--- invalid foreign key --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "Invalid foreign key">
				<cfif structKeyExists(result,"field")>
					<cfset message = message& " for field '#arguments.result.field#'">
				</cfif>
			</cfcase>
			<cfcase value="INVALID_API_USER">
				<cfset statusCode=401>
				<cfset message = "The user is not in the API usergroup">
			</cfcase>
			<cfcase value="INVALID_LOGIN_CREDENTIALS">
				<cfset statusCode=401>
				<cfset message = "The login credentials are not recognised">
			</cfcase>
			<cfcase value="NONEXISTENT_CACHED_QUERY">
				<cfset statusCode = errorStatusCode>
				<cfset message = "The queryID specified is not valid">
			</cfcase>
			<cfcase value="INVALID_FIELD">
				<cfset statusCode = errorStatusCode>
				<cfif structKeyExists(arguments.result,"field")>
					<cfset fieldMessage = "The field '#arguments.result.field#' is not a valid fieldname.">
				<cfelseif structKeyExists(arguments.result,"invalidFields") and arguments.result.invalidFields neq "">
					<cfset fieldMessage = "The following fields are invalid: '#arguments.result.invalidFields#'.">
				</cfif>
				<cfset message = "#fieldMessage# Please refer to the documentation for valid fieldnames.">
			</cfcase>
			<cfcase value="MISSING_UNIQUEID"> <!--- used when doing a bulk update --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "Missing the unique key field">
			</cfcase>
			<cfcase value="DATA_LIMIT_EXCEEDED"> <!--- bulk insert/update error --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "The number of records to be inserted/updated has exceeded the limit">
			</cfcase>
			<cfcase value="NUM_REQUESTS_EXCEEDED">
				<cfset statusCode=429>
				<cfset message = "The number of hits has exceeded the allowable limit">
			</cfcase>
			<cfcase value="INVALID_SESSIONID">
				<cfset statusCode = errorStatusCode>
				<cfset message = "An invalid sessionID has been passed.">
			</cfcase>
			<cfcase value="INVALID_PROTOCOL">
				<cfset statusCode=505>
				<cfset message = "Only HTTPS Version supported">
			</cfcase>
			<cfcase value="NOT_AUTHENTICATED">
				<cfset statusCode=401>
				<cfset message = "You must be authenticated to access this resource">
			</cfcase>
			<cfcase value="INVALID_ENTITY"> <!--- used when passing in an invalid entity to the describe entity function --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "The entity is not valid. Please refer to the documentation for valid entities.">
			</cfcase>
			<cfcase value="INVALID_SQL"> <!--- used when dangerous sql is passed through on a where clause --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "The sql statement used to filter the record set is invalid.">
			</cfcase>
			<cfcase value="ENTITY_DELETED"> <!--- updating an entity that does not exist --->
				<!--- 2015-02-23	RPW	FIFTEEN-219 - API - Changed response code to 202 for deletion from 500. --->
				<cfset statusCode = 202>
				<cfset message = "The entity has been deleted.">
			</cfcase>
			<cfcase value="ENTITY_CANNOT_BE_DELETED">
				<cfset statusCode = 202>
				<cfset message = "The entity cannot be deleted. See reason for more details">
			</cfcase>
			<cfcase value="INVALID_DATA_TYPE"> <!--- data contains wrong data type --->
				<cfset statusCode = errorStatusCode>
				<cfif structKeyExists(arguments.result,"field")>
					<cfif structKeyExists(arguments.result,"fieldValue")>
						<cfset fieldValue = " '#arguments.result.fieldValue#'">
					</cfif>
					<cfset message = "The value#fieldValue# for field '#arguments.result.field#' is not of the correct type.">
				<cfelse>
					<cfset message = "Incorrect data type."> <!--- TODO: need a better error message here.. this occurs when the argument type is not of array, for example --->
				</cfif>
			</cfcase>
			<cfcase value="EXCEEDS_MAX_LENGTH"> <!--- data exceeds the maximum length specified for the field --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "The value '#arguments.result.fieldValue#' for field '#arguments.result.field#' exceeds the maximum length specified for the field.">
			</cfcase>
			<cfcase value="REQUIRED_DATA_MISSING"> <!--- no data passed for a field that is not nullable --->
				<cfset statusCode = errorStatusCode>

				<!--- WAB 2015-01-21 CASE 443542 added test for not being blank (value is being defaulted to blank somewhere) --->
				<cfif structKeyExists(arguments.result,"missingRequiredFields") and arguments.result.missingRequiredFields is not "">
					<cfset message = "Required data missing for the following fields: #arguments.result.missingRequiredFields#.">
				<cfelse>
					<cfset message = "Required data missing for field '#arguments.result.field#'. Please refer to the documentation for the list of required fields for this entity.">
				</cfif>
			</cfcase>
			<cfcase value="INVALID_VALID_VALUE"> <!--- value not in list of valid values --->
				<cfset statusCode = errorStatusCode>
				<cfif structKeyExists(arguments.result,"fieldValue")>
					<cfset fieldValue = " '#arguments.result.fieldValue#'">
				</cfif>
				<cfset message = "The value#fieldValue# for field '#arguments.result.field#' is not a valid value. Please use the entity method to view a list of valid values for this field.">
			</cfcase>
			<cfcase value="INVALID_DATA"> <!--- value not in list of valid values - bulk update/insert--->
				<cfset statusCode = errorStatusCode>
				<cfset message = "Some of the records have invalid data in the field '#arguments.result.field#'">
			</cfcase>
			<cfcase value="UNKNOWN_EXCEPTION"> <!--- an unknown error in query,insert or update functions --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "An unknown exception has occurred. Please refer an administrator to this errorID (#arguments.result.errorID#)">
			</cfcase>
			<cfcase value="ENTITIES_UPDATED,ENTITY_UPDATED"> <!--- a successful update --->
				<cfset statusCode = 200> <!--- was 204 --->
			</cfcase>
			<cfcase value="ENTITY_INSERTED,ENTITIES_INSERTED"> <!--- a successful insert --->
				<cfset statusCode=201>
			</cfcase>
			<cfcase value="NO_VALID_FIELDS"> <!--- no valid columns have been passed --->
				<cfset statusCode = errorStatusCode>
				<cfset message = "No valid fieldnames have been passed.">
			</cfcase>
			<cfcase value="ENTITY_NOTUPDATED"> <!--- no change detected, so record not updated --->
				<!--- 2015-02-17	RPW	FIFTEEN-137 - Changed status code from 204 to 203. 204 will return no content. --->
				<cfset statusCode = 203>
				<cfset message = "The entity has not been updated. This may be because you entered data identical to existing data">
			</cfcase>
			<cfcase value="DELETE_FAILED">
				<cfset statusCode = errorStatusCode>
				<cfset message = "The record could not be deleted for the following reasons:#arguments.result.reason#">
			</cfcase>
			<cfcase value="DELETE_ERROR,QUERY_ERROR,UPDATE_ERROR,INSERT_ERROR,UPSERT_ERROR"> <!--- no valid columns have been passed.. might get this if variables not url-encoded --->
				<cfset statusCode = errorStatusCode>
				<cfif structKeyExists(arguments.result,"sqlMessage")>
					<!--- here, we get a string back from SQL server, saying  "Invalid column name 'invalidColumn'.". Extract the column name and hand it to our invalid_column error message--->
					<cfif left(arguments.result.sqlMessage,21) eq "Invalid column name '">
						<cfset tempFieldName = right(arguments.result.sqlMessage,len(arguments.result.sqlMessage)-21)>
						<cfset arguments.result.field = left(tempFieldName,len(tempFieldName)-2)>
						<cfset arguments.errorCode = "INVALID_FIELD">
						<cfset message = getStatusCodeAndMessage(errorCode="INVALID_FIELD",result=arguments.result).message>
					<cfelse>
						<cfset message = arguments.result.sqlMessage>
					</cfif>
				<cfelse>
					<cfset message = "An unhandled query error has occurred. Please refer an administrator to this errorID (#arguments.result.errorID#)">
				</cfif>
			</cfcase>
			<!---  NJH 2012/11/04 Case 431491 - invalid date format.. must be yyyy-mm-ddThh:mm:ssZ
					WAB PROD2016-728 2016-09-28 add some help text to show correct date format
			 --->
			<cfcase value="INVALID_DATE_FORMAT">
				<cfset statusCode = errorStatusCode>
				<cfif structKeyExists(arguments.result,"fieldValue")>
					<cfset fieldValue = " '#arguments.result.fieldValue#'">
				</cfif>
				<cfset message = "The value#fieldValue# for field '#arguments.result.field#' must be in UTC format. (YYYY-MM-DDThh:mm:ssZ)">
			</cfcase>
			<cfcase value="LEAD_CONVERTED_TO_OPPORTUNITY">
				<cfset statusCode = errorStatusCode>
				<cfset message = "Converted leads cannot be updated.">
			</cfcase>
			<!---  RJT2 2016/08/25 JIRA 2198 - add error codes for Phrase API --->
			<cfcase value="INVALID_PHRASE">
				<cfset statusCode = errorstatuscode>
				<cfif structKeyExists(arguments.result,"phraseid")>
					<cfset message = "Phrase with PhraseID " & arguments.result.phraseID & " does not exist.">
				<cfelse>
					<cfset message = "Unknown error processing phrase.">
				</cfif>
			</cfcase>
			<cfcase value="INVALID_PHRASE_TRANSLATIONS">
				<cfset statusCode = errorStatusCode>
				<cfset message = "Phrase translations array missing or invalid.">
			</cfcase>

			<cfdefaultcase>
			</cfdefaultcase>
		</cfswitch>

		<cfset statusCodeAndMessage.statusCode = statusCode>
		<cfset statusCodeAndMessage.message = message>

		<cfreturn statusCodeAndMessage>
	</cffunction>

	<!--- 2015-02-23	RPW	FIFTEEN-219 - API - Changed to check organisation, locations and people before deleting. --->
	<cfscript>

		public struct function deleteOrganisation
		(
			required numeric entityID,
			required numeric entityTypeID,
			required string datasource,
			required numeric relayCurrentUserPersonid,
			required numeric relayCurrentUserUsergroupid
		)
		{
			var response = {};
			var result = {};
			result.errorCode = "";
			var isOkToDelete = true;

			var getPeople = GetOrganisationPeople(organisationID=arguments.entityID,datasource=arguments.datasource);
			var getLocations = GetOrganisationLocations(organisationID=arguments.entityID,datasource=arguments.datasource);
			var checkLocationDelete = CheckLocationForDeletion(locationIDs=ValueList(getLocations.locationID),datasource=arguments.datasource);

			if (!checkLocationDelete.isOkToDelete) {
				isOkToDelete = false;
				result.reason = checkLocationDelete.reason;
				result.errorCode = checkLocationDelete.errorCode;
			}

			if (isOkToDelete) {
				var checkOrganisationDelete = CheckOrganisationForDeletion(organisationID=arguments.entityID,datasource=arguments.datasource,relayCurrentUserPersonid=arguments.relayCurrentUserPersonid,relayCurrentUserUsergroupid=arguments.relayCurrentUserUsergroupid);
				if (!checkOrganisationDelete.isOkToDelete) {
					isOkToDelete = false;
					result.reason = checkOrganisationDelete.reason;
					result.errorCode = checkOrganisationDelete.errorCode;
				}
			}

			if (isOkToDelete) {

				for (p=1;p <= getPeople.recordCount;p++) {
					application.com.flag.setBooleanFlag(flagTextID='DeletePerson', entityID=getPeople.personID[p], useRequestTime=true);
				}

				for (l=1;l <= getLocations.recordCount;l++) {
					application.com.flag.setBooleanFlag(flagTextID='DeleteLocation', entityID=getLocations.locationID[l], useRequestTime=true);
				}
				application.com.flag.setBooleanFlag(flagTextID='DeleteOrganisation', entityID=arguments.entityID, useRequestTime=true);
				result.DELETED=true;
				result.message="Entity #entityID# deleted";
			}

			response = application.com.relayAPI.prepareApiResponse(result=result,errorCode=result.errorCode);

			return response;

		}


		private struct function CheckOrganisationForDeletion
		(
			required numeric organisationID,
			required string datasource,
			required numeric relayCurrentUserPersonid,
			required numeric relayCurrentUserUsergroupid
		)
		{
			var response = {};
			response.isOkToDelete = true;

			var deletionReport=application.com.relayEntity.canEntityBeDeleted(entityID=organisationID,entityTypeID=application.entityTypeID.organisation );
			if (not deletionReport.canBeDeleted){
				response.isOkToDelete = false;
				response.errorCode='ENTITY_CANNOT_BE_DELETED';
				response.reason=arrayToList(deletionReport.reasons_friendlyText);
				response.detailMessage=response.reason;
			}

			if (response.isOkToDelete){
				var flagTextID = "DeleteOrganisation";

				var flagID = GetFlagID(FlagTextID=flagTextID,datasource=arguments.datasource).flagID[1];

				var flagStructure = application.com.flag.getFlagStructure(flagTextID);
				var typeData = flagStructure.flagType.datatable;
				var entityTable = flagStructure.entityType.tablename;
				var flagGroupID = flagStructure.flagGroupID;

				var flagRight = "";
				var userHasPersonDeleteRights = application.com.rights.getPersonDeleteRights();
				var userHasLocationDeleteRights = application.com.rights.getLocationDeleteRights();
				var userHasOrganisationDeleteRights = application.com.rights.getOrganisationDeleteRights();
				if (userHasPersonDeleteRights && userHasLocationDeleteRights && userHasOrganisationDeleteRights) {
					flagRight = "SystemTask";

					var qGetFlagRights = GetFlagRights(personID=arguments.relayCurrentUserPersonid,flagGroupID=flagGroupID,flagRight=flagRight,datasource=arguments.datasource);

					if (not qGetflagrights.recordcount) {
						response.isOkToDelete = false;
						response.entityId = arguments.organisationID;
						response.errorCode = "INSUFFICIENT_RIGHTS";
						response.reason="Insufficient rights on the DeleteOrganisation flag";
					}

				}else{
					response.isOkToDelete = false;
					response.entityId = arguments.organisationID;
					response.errorCode = "INSUFFICIENT_RIGHTS";
					response.reason="Require rights to delete organisations, locations and people, actually have; organisation: #userHasOrganisationDeleteRights#, location: #userHasLocationDeleteRights#, person: #userHasPersonDeleteRights# ";

				}



			}


			if (response.isOkToDelete){

				var sql = "
					SELECT entityid FROM booleanflagdata bfd
					INNER JOIN flag f ON bfd.flagid = f.flagid
					WHERE flagtextid = 'deleteOrganisation'
					AND entityID = :entityID
				";
				var qObj = new query();
				qObj.setDatasource(arguments.datasource);
				qObj.setSql(sql);
				qObj.addParam(name="entityID",cfsqltype="CF_SQL_INTEGER",value=arguments.organisationID);

				var getOrganisationDeleteFlags = qObj.execute().getResult();

				if (getOrganisationDeleteFlags.recordCount) {
					response.isOkToDelete = false;
					response.reason = "Organisation is already marked for deletion";
					response.errorCode = "DELETE_FAILED";
				}
			}

			return response;

		}


		private struct function CheckLocationForDeletion
		(
			required string locationIDs,
			required string datasource
		)
		{
			var response = {};
			response.isOkToDelete = true;

			var checkLocationDeletion = CheckOrganisationLocationsDeletion(locationIDs=arguments.locationIDs,datasource=arguments.datasource);

			if (checkLocationDeletion.recordCount) {
				response.isOkToDelete = false;
				response.reason = "Contains Locations That Cannot Be Deleted";
				response.errorCode = "DELETE_FAILED";
			}

			return response;

		}





		private query function GetOrganisationPeople
		(
			required numeric organisationID,
			required string datasource
		)
		{
			var sql = "
				SELECT  organisation.OrganisationID, organisation.OrganisationName, Person.Salutation,
						Person.FirstName, Person.LastName, person.personID,
						Person.FirstName+' '+Person.LastName as PersonName,
						location.locationID,location.SiteName as LocationName
				FROM Person
					INNER JOIN location ON Person.locationID = location.locationID
					INNER JOIN organisation ON location.OrganisationID = organisation.OrganisationID
				WHERE Person.OrganisationID = :organisationID
					AND (Person.PersonID NOT IN
			                (
								SELECT entityid FROM booleanflagdata bfd
								INNER JOIN flag f ON bfd.flagid = f.flagid
			                   	WHERE flagtextid = 'deleteperson'
							)
					)
				ORDER BY Person.OrganisationID, organisation.OrganisationName
			";
			var qObj = new query();
			qObj.setDatasource(arguments.datasource);
			qObj.setSql(sql);
			qObj.addParam(name="organisationID",cfsqltype="CF_SQL_INTEGER",value=arguments.organisationID);

			var getPeople = qObj.execute().getResult();

			return getPeople;
		}


		private query function GetOrganisationLocations
		(
			required numeric organisationID,
			required string datasource
		)
		{
			var sql = "
				SELECT *
				FROM location
				WHERE organisationID = :organisationID
				AND LocationID NOT IN
					(
						SELECT entityid
						FROM booleanflagdata bfd
						INNER JOIN flag f ON bfd.flagid = f.flagid
						WHERE flagtextid = 'deleteLocation'
					)
				ORDER BY OrganisationID, LocationID
			";
			var qObj = new query();
			qObj.setDatasource(arguments.datasource);
			qObj.setSql(sql);
			qObj.addParam(name="organisationID",cfsqltype="CF_SQL_INTEGER",value=arguments.organisationID);

			var getLocations = qObj.execute().getResult();

			return getLocations;
		}


		private query function CheckOrganisationLocationsDeletion
		(
			required string locationIDs,
			required string datasource
		)
		{
			var sql = "
				select locationID as entityID into ##deleteEntityIDTable
				FROM location
				WHERE locationID IN ( :locationIDList )

				Create table ##NotDeletable(EntityID int,Reason varchar(50),extendedInfo varchar(max))

				exec deleteEntityCheck @entityTypeID=1

				select d.*, l.SiteName
				from ##NotDeletable d inner join location l on d.entityID = l.locationID
				order by SiteName, reason

				drop table ##NotDeletable
				drop table ##deleteEntityIDTable
			";
			var qObj = new query();
			qObj.setDatasource(arguments.datasource);
			qObj.setSql(sql);
			qObj.addParam(name="locationIDList",cfsqltype="CF_SQL_INTEGER",value=arguments.locationIDs,list=true);

			var checkLocationDeletion = qObj.execute().getResult();

			return checkLocationDeletion;
		}


		private query function GetFlagID
		(
			required string FlagTextID,
			required string datasource
		)
		{
			var sql = "
				SELECT flagID
				FROM Flag
				WHERE FlagTextID = :FlagTextID
			";
			var qObj = new query();
			qObj.setDatasource(arguments.datasource);
			qObj.setSql(sql);
			qObj.addParam(name="FlagTextID",cfsqltype="CF_SQL_VARCHAR",value=arguments.FlagTextID);

			var getFlagID = qObj.execute().getResult();

			return getFlagID;
		}


		private query function GetFlagRights
		(
			required numeric personID,
			required numeric flagGroupID,
			required string flagRight,
			required string datasource
		)
		{
			var flagMethod = "edit";
			var sql = "
				SELECT 1
				from flaggroup as fg
				where fg.flaggroupid = :flagGroupID
				AND (	(fg.#flagMethod#AccessRights = 0 AND fg.#flagMethod# <> 0)
						OR
						(fg.#flagMethod#AccessRights <> 0	AND EXISTS (SELECT 1
																			FROM vFlagGroupRights as fgr
																			WHERE fg.FlagGroupID = fgr.FlagGroupID
																			and fgr.personid = :personID
																			AND fgr.#flagMethod# <> 0)
						OR '#arguments.flagRight#' = 'SystemTask'
						)
					)
			";
			var qObj = new query();
			qObj.setDatasource(arguments.datasource);
			qObj.setSql(sql);
			qObj.addParam(name="personID",cfsqltype="CF_SQL_INTEGER",value=arguments.personID);
			qObj.addParam(name="flagGroupID",cfsqltype="CF_SQL_INTEGER",value=arguments.flagGroupID);

			var getFlagRights = qObj.execute().getResult();

			return getFlagRights;
		}

	</cfscript>

</cfcomponent>
