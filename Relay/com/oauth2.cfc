/**
* @author Martin Elgie
* @date 2016-12-05 (ymd)
* Functions for accessing OAuth2 Servers for RelayTags
*/


component accessors=true output=true persistent=false {

	public query function getValidServers(boolean activeOnly=false, String sortOrder="serverName")  validvalues="true"{
		var authQuery=new Query();
		authQuery.addParam(name="activeOnly",value=arguments.activeOnly,cfsqltype="cf_sql_bit");
		authQuery.addParam(name="internalID", value=application.com.relayCurrentSite.getInternalSite().SITEDEFID, cfsqltype="cf_sql_varchar");
		
		var sqlSafe_identityProviderName=application.com.security.queryObjectName(arguments.sortOrder);
		
		var result = authQuery.execute(sql="select serverName, clientIdentifier "&
											"from OAuth2Server "&
											"where (active=1 or :activeOnly = 0) "&
											"and authorisedSites not like '%:internalID%' "&
											"order by #sqlSafe_identityProviderName# " 
											); 
											
		return result.getResult();
		
	}
}