<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayRating.cfc
Author:			AJC Englex
Date started:	19/07/2012

Description:	Functions for entity ratings

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:

The queries in the functions below were largly ported over from the PS project P-LEN030 CR031 CSAT and should be reviewed
 --->

<cfcomponent displayname="relayRating" hint="Provides functionality to manage the entity ratings">
	<cffunction access="public" name="getRatings" hint="Returns rating data by entity and entitytype">
		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="entitytypeID" required="yes" type="numeric">

		<cfscript>
			var getRatings = "";
		</cfscript>

		<cfquery name="getRatings" datasource="#application.siteDataSource#">
			select  (Sum(Rating)*1.0/(count(personID))) as avgRating,
			FLOOR((Sum(Rating)*1.0/(count(personID)))) as fullStars,
			count(personID) as totalRatings
			from Rating
			where entityID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
			and entitytypeID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entitytypeID#">
		</cfquery>

		<cfreturn getRatings>
	</cffunction>

	<cffunction name="getRatingsDiv" access="public" hint="This function should be called by getRatingsWidget function, and not really called directly">
		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="entitytypeID" required="yes" type="numeric">
		<cfargument name="dontAllowRatings" required="no" type="boolean">

		<cfset var getRatings = application.com.relayRating.getRatings(entityID=entityID,entitytypeID=entitytypeID) />
		<cfset var starRatings = "">

		<cfsavecontent variable="starRatings">
			<cfoutput>
				<cfif getRatings.totalRatings gt 0>
					<cfloop index = "LoopCount" from = "1" to = #getRatings.fullStars#>
						<img src="/Images/eLearning/star_full.gif" />
					</cfloop>
					<CFSET halfStars = getRatings.avgRating - getRatings.fullStars>
					<cfif halfStars gte 0.5 and halfStars lt 1>
						<img src="/Images/eLearning/star_half.gif" />
						<cfset emptyStars = 4 - getRatings.fullStars>
					<CFELSE>
						<cfset emptyStars = 5 - getRatings.fullStars>
					</CFIF>
					<cfloop index = "LoopCount" from = "1" to = #emptyStars#>
						<img src="/Images/eLearning/star_empty.gif" />
					</cfloop>
				<cfelse>
					<cfloop index="LoopCount" from=1 to=5>
						<img src="/Images/eLearning/star_empty.gif" />
					</cfloop>
				</cfif>
				<cf_translate>
				<br />#getRatings.totalRatings# phr_TotalRatings
				<cfif not structKeyExists(arguments,"dontAllowRatings") or not arguments.dontAllowRatings>
					<a href="##" onclick="jQuery('##box#arguments.entityID#').slideToggle();return false;">phr_rate</a>
				</cfif>
				</cf_translate>
			</cfoutput>
		</cfsavecontent>

		<cfreturn starRatings>
	</cffunction>

	<cffunction name="getRatingsWidget" access="public" returnType="string" >
		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="entitytypeID" required="yes" type="numeric">
		<cfargument name="dontAllowRatings" required="no" type="boolean" default="false">

		<cfset var ratings = "">
		<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityTypeID)>

		<cfsavecontent variable="ratings">
			<cfoutput>
				<!--- importing Javascript libraries for rating widget --->
				<cf_includejavascriptonce template = "/javascript/ratings.js">
				<cf_includejavascriptonce template = "/javascript/lib/jquery/jquery.rating.js">
				<!--- importing CSS sheets for rating widget--->
				<cf_includeCSSonce template = "/javascript/lib/jquery/css/jquery.rating.css">
			<div class="ratings">
				<div id="starRatings#arguments.entityID#">
					#application.com.relayRating.getRatingsDiv(argumentCollection=arguments)#
				</div>
				<cfif not arguments.dontAllowRatings>
					<div class="rate" id="box#arguments.entityID#" style="display:none;">
						<div class="rate_content" id="subBox#arguments.entityID#">
							<div id="RatingSubmit">
								<form name="contact" id="starForm#arguments.entityID#" action="" arg="#arguments.entityID#">
									 <div id="stars-wrapper#arguments.entityID#">
									<input type="hidden" name = "#entityTypeStruct.uniqueKey#" id = "#entityTypeStruct.uniqueKey#" value = "#arguments.entityID#">
								   <fieldset>
									   phr_#entityTypeStruct.tablename#_rateThis#entityTypeStruct.tablename#<BR>
										<!--- 2013-07-17	YMA	Case 436216 added to support cancellation of rating --->
										<INPUT TYPE=hidden NAME="entityScore" class="rating-cancel" VALUE=1 id="#arguments.entityTypeID#_#arguments.entityID#_0">
										<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=1 id="#arguments.entityTypeID#_#arguments.entityID#_1">
										<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=2 id="#arguments.entityTypeID#_#arguments.entityID#_2">
										<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=3 id="#arguments.entityTypeID#_#arguments.entityID#_3">
										<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=4 id="#arguments.entityTypeID#_#arguments.entityID#_4">
										<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=5 id="#arguments.entityTypeID#_#arguments.entityID#_5">
								     <br /><br />
								   </fieldset>
								   </div>
								 </form>
							</div>
						</div>
					</div>
				</cfif>
			</div>
		</cfoutput>
	</cfsavecontent>

	<cfreturn ratings>

	</cffunction>

</cfcomponent>
