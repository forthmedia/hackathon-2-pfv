<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2010/06  onward
	File which runs system tests
	The actual tests are functions in cfcs in the systemTests directory
	All functions must return a structure containing isOK = true/false

--->

	<!--- 
		TODO
		Add test for cf scriptpath
	--->

<cfcomponent >

	<cffunction name="runTest">
		<cfargument name="componentname">
		<cfargument name="methodname">
		<cfargument name="cfpassword">
		<cfargument name="applicationScope" default="#application#">
				
		<cfset var componentObj = createObject("component",componentName)>
		<cfset var theMethod = componentObj[methodname]>
		
		<cfset var result = theMethod (applicationScope = applicationScope,cfpassword = cfpassword)>
		
		<cfreturn result>
		
	</cffunction>

	<cffunction name="runTests">
		<cfargument name="runAllTests" default="false">
		<cfargument name="runTestsList" default="">
		<cfargument name="cfpassword" default="">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="testsArray" >
				
		<cfset var result = {}>
		<cfif not structKeyExists (arguments,"testsArray")>
			<cfset arguments.testsArray = getTestsArray (applicationScope = applicationScope)>
		</cfif>
		

			<cfloop index="component" array = "#testsArray#">
				<cfloop index="method" array = "#component.Functions#">
					<cfif runAllTests or listfindnocase(runTestsList,"#component.name#.#method.name#")>
						<cftry>
							<cfset testResult = runTest(componentname =component.name, methodname = method.name, applicationScope = applicationScope,cfpassword = cfpassword)>
							<cfcatch>
								<cfset testResult = {isOK = false, message = cfcatch.message}>
								<cfif structKeyExists (cfcatch,"TagContext")>
									<cfset testResult.message = testResult.message & " Line " & cfcatch.TagContext[1].line & " " & cfcatch.TagContext[1].template >
								</cfif>

							</cfcatch>
						</cftry>	
						<cfset result ["#component.name#.#method.name#"] = testResult>
					</cfif>	
				</cfloop>
			</cfloop>

		<cfreturn result>


		
	</cffunction>


	<cffunction name="getArrayOfFunctionsInAComponent">
		<cfargument name="component" default="false">	
		<cfargument name="filterPrivate" default="true">	
		
			<cfset var unOrderedFunctionsStructure = {}>
			<cfset var resultArray = arrayNew(1)>
			<cfset var componentMetaData  = getMetaData(Component)>


			<!--- Run functions in order specified by an INDEX attribute, 
				this gets me an array that points to the functions in the right order 
				also filters out any private functions
			--->
			
			<cfloop index="functionIDX" from=1 to = #arrayLen(componentMetaData.functions)#>
				<cfset theFunctionMetaData	= componentMetaData.functions[functionIDX]>
					<cfif (not filterPrivate) or (not StructKeyExists (theFunctionMetaData,"access")) or (theFunctionMetaData.access is not "private")>
						<cfset unOrderedFunctionsStructure[functionIDX] = {name = theFunctionMetaData.name}>
						<cfif structKeyExists (theFunctionMetaData,"index")>
							<cfset unOrderedFunctionsStructure[functionIDX].index = theFunctionMetaData.index>
						<cfelse>
							<cfset unOrderedFunctionsStructure[functionIDX].index = 10000 + functionIDX>
						</cfif>
					</cfif>
			</cfloop>	

			<cfset FunctionsInOrderArray = structSort(unOrderedFunctionsStructure,"numeric","ASC","index")>

			<cfloop index="orderIDX" from=1 to = #arrayLen(FunctionsInOrderArray)#>
				<cfset theFunctionMetaData	= componentMetaData.functions[FunctionsInOrderArray[orderIDX]]>
					<cfset resultArray[orderIDX] = theFunctionMetaData>
			</cfloop>	

		<cfreturn resultArray>

	</cffunction>

	<cffunction name="getTestsArray">
		<cfargument name="applicationScope" default="#application#">
		
		<cfset var result = arrayNew(1)>
		<cfset var tempresult = "">
		<cfset var method = "">
		<cfset var thisComponent = "">
		


		<cfdirectory name = "coreComponents" directory = "#applicationScope.paths.relay#/systemTests"  filter="*.cfc">


		<cfloop query = "coreComponents">
			<cfset objectName = replace(replace(replace(replace("#directory#\#name#",".cfc",""),applicationScope.path,""),applicationScope.userfilesabsolutepath,""),"\",".","ALL")>
			<cfset objectName = rereplace(objectName,"\A\.","")>

			<cftry>
				<cfset thisComponent = createObject("component",objectName)>
				<cfcatch>
					<cfoutput>Unable to load object #objectName#</cfoutput>
					<cfdump var="#cfcatch#">
					<CF_ABORT>
				</cfcatch>

			</cftry>


			<cfset thisComponentMetaData = getMetaData (thisComponent)>			
			<cfset thisComponentFunctions = getArrayOfFunctionsInAComponent (thisComponent)>
			<cfset thisResult = {name  = objectName,functions = thisComponentFunctions}>
			<cfif structKeyExists(thisComponentMetaData,"hint")>
				<cfset thisResult.hint = thisComponentMetaData.hint>
			<cfelse>
				<cfset thisResult.hint = listLast(objectName,".")>
			</cfif>
			
			<cfset arrayAppend(result,thisResult)>						
		
		</cfloop>
	
		<cfreturn result>
	</cffunction>	


	
	<cffunction name = "getAllRowsHTML">	
		<cfargument name="runAllTests" default="false">
		<cfargument name="runTestsList" default="">
		<cfargument name="cfpassword" default="">
		<cfargument name="applicationScope" default="#application#">

		<cfset var result = {needsCFPassword = false}>
		<cfset testsArray = getTestsArray (applicationScope = applicationScope)>
		<cfset testResults = runTests (argumentCollection = arguments,testsArray = testsArray)>

		<cfsaveContent variable = "Result.rowsHTML">
			<cfloop index="component" array = "#testsArray#">
				<cfoutput><TR><TH colspan="2">#application.com.security.sanitiseHTML(component.hint)#</TH></tr></cfoutput>
				<cfloop index="method" array = "#component.Functions#">
					<cfoutput>
					<TR><TD>&nbsp;&nbsp;</TD><TD><a onClick="runTest(this,'#component.name#','#method.name#',{<cfif arrayLen(StructFindValue(method,'cfpassword'))>cfpassword:true<cfset result.needsCFPassword = true></cfif>})">
					<cfif structKeyExists (method,"hint")>
						#application.com.security.sanitiseHTML(method.hint)#
					<cfelse>
						#application.com.security.sanitiseHTML(method.name)#
					</cfif>
						</a>
					</TD></TR>
					</cfoutput>
					<cfif structKeyExists (testResults,"#component.name#.#method.name#")>
						<tr><td></td><td>
						<cfset outputTestResult (testResults["#component.name#.#method.name#"])>
						</td></tr>
					</cfif>
				</cfloop>
			</cfloop>

			<script>
				var cfpassword = <cfoutput>'#jsStringFormat(cfpassword)#'</cfoutput> ;
				function runTest(obj,componentName,functionName,options) {
					
					parameters = 'runTest='  + componentName + '.' + functionName
					if(options.cfpassword ){
						parameters += '&cfpassword=' + getCFPassword()
								}
					// eventually ought to make this an ajax call like release scripts
					window.location.search	 = parameters				
				
				}

				function getCFPassword() { 
				
						if (cfpassword == '') {
							cfpassword = prompt('Please Enter the CF Admin Password')
						}
					return 	cfpassword
				}
				
				

				function runAllTests() { 
					
					parameters = 'runAllTests=true'
					<cfif result.needsCFPassword>
						parameters += '&cfpassword=' + getCFPassword()
					</cfif >
					window.location.search	 = parameters				
					
				}				
				
			</script>


		</cfsaveContent> 

		<cfreturn result>
	</cffunction>
	
	<cffunction name="outputTestResult">
		<cfargument name="TestResult">
	
		<cfif testResult.isOK>
			<cfset class = "successblock">
		<cfelse>
			<cfset class = "errorblock">
		</cfif>
		<cfoutput><div class="#class#">#application.com.security.sanitiseHTML(testResult.message)#</div></cfoutput>
	
	</cffunction>



	
</cfcomponent>