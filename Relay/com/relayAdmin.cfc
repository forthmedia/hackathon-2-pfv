<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2009/03/03 added some var'ing to try and fix a unexplainable error

NJH 2010/01/11 LID 2423 - check if structure key of 'isOk' exists
NJH 2016/09/07	JIRA PROD2016-2232 - re-wrote getCFScheduledTasks as it appeared not to be working. Now use <cfschedule action="list" result="scheduledTasks">
 --->

<cfcomponent displayname="Relay Admin Functions" hint="Contains a number of commonly used Relay Admin Queries">

	<!--- <cffunction name="getCFScheduledTasks" access="public" returntype="query" hint="Returns the ColdFusion Scheduled Tasks">
		<cfscript>
			var columnList = "paused,interval,start_Time,end_Time,url,start_date,disabled,task";
			var columnListWithDataTypes = "paused:bit,interval:varchar,start_Time:date,end_Time:date,url:varchar,start_date:date,disabled:bit,task:varchar,nextRun:date";
			var allCfScheduledTaskQry=QueryNew("#columnList#");
			var cfScheduleStruct = structNew();
			var thisServer = "";  //NJH Bug Fix All Sites Issue 1929
			var result = ""; //NJH Bug Fix All Sites Issue 1929
			var thisServerInfo = structNew();
			var directory = "";
			var scheduledTaskFile = "";
			var filecontent = "";
			var colName = "";
			var cfScheduledTaskQry = "";
		</cfscript>

		<cftry>
			<cfset directory = server.coldfusion.rootDir>

			<!--- read the scheduled tasks from the COLDFUSION file that stores the cfAdmin scheduled tasks
				If a server is running multiple instances, the directories actually have a '.' rather than a '-'
			 --->
			<cfif fileExists("#directory#\cfusion-ear\cfusion-war\WEB-INF\cfusion\lib\neo-cron.xml")>
				<cfset scheduledTaskFile = "\cfusion-ear\cfusion-war\WEB-INF\cfusion\lib\neo-cron.xml">
			<cfelseif fileExists("#directory#\cfusion.ear\cfusion.war\WEB-INF\cfusion\lib\neo-cron.xml")>
				<cfset scheduledTaskFile = "\cfusion.ear\cfusion.war\WEB-INF\cfusion\lib\neo-cron.xml">
			</cfif>

 			<cfset result = application.com.clusterManagement.runFunctionOnCluster(functionName="readTemplate",relativeTo="serverRoot",template=scheduledTaskFile)>

			<cfloop item= "thisServer" collection = #result#>
				<cfset thisServerInfo = result[thisServer]>
				<!--- LID 2423 NJH 2010/01/11 - we're assuming that if isOk is not defined, than it's ok, as any error in calling the
					webservice will result in 'isOk' being set to false --->
				<cfif (structKeyExists(thisServerInfo,"isOK") and thisServerInfo.isOK) or (not structKeyExists(thisServerInfo,"isOK"))>
					<cfset filecontent = result[thisServer].content>
					<cfwddx action="wddx2cfml" input="#filecontent#" output="cfScheduledTasksArray">

					<!--- NJH 2009/02/18 - need to look through each structure and find if each key (column) exists as they are not guaranteed to exist.
						For example, if there is only a single scheduled task set up that is set up to run daily, then the endtime is not specified.
						cfScheduleStruct is a structure of structures. Each structure is a scheduled task containing a structure of the details.
					--->
					<cfset cfScheduleStruct = cfScheduledTasksArray[1]>
					<cfloop collection="#cfScheduleStruct#" item="structKey">
						<cfloop list="#columnList#" index="colName">
							<cfif not structKeyExists(cfScheduleStruct[structKey],colName)>
								<cfset cfScheduleStruct[structKey][colName] = "">
							</cfif>
						</cfloop>
					</cfloop>
					<cfset cfScheduledTaskQry = application.com.structureFunctions.structToQuery(struct=cfScheduleStruct,defaultColumnsAndDataTypes = columnListWithDataTypes)>

					<!--- need to set brackets around interval as it appears to be a reserved word for qoq. --->
					<cfset columnList = replace(columnList,"interval","[interval]")>

					<cfquery name="allCfScheduledTaskQry" dbtype="query">
						select #columnList# from cfScheduledTaskQry
						<cfif isDefined("allCfScheduledTaskQry") and allCfScheduledTaskQry.recordCount gt 0>
							union
							select #columnList# from allCfScheduledTaskQry
						</cfif>
					</cfquery>

					<!--- need to remove the brackets to check if the name exists up above in the structure. --->
					<cfset columnList = replace(columnList,"[interval]","interval")>
				</cfif>
			</cfloop>

			<cfcatch type="any">
				<cfmail to="errors@foundation-network.com,gawain@foundation-network.com,nathanielh@foundation-network.com" from="errors@foundation-network.com" subject="Error while trying to read scheduledTaskFile on #cgi.http_host#" type="html">
					Error in relay\com\relayAdmin.cfc - function getCFScheduledTasks<br>
					#cfcatch.detail#<br>
					#cfcatch.message#<br>

					<cfif isDefined("thisServer")>
						Server:#thisServer#<br>
					</cfif>
					<cfdump var="#cfcatch#" label="Error dump"><br>
					<cfif isDefined("cfScheduleStruct")>
						<cfdump var="#cfScheduleStruct#"><br>
					</cfif>
					<cfif isDefined("cfScheduledTaskQry")>
						<cfdump var="#cfScheduledTaskQry#" label="Scheduled Task Query for #thisServer#">
					</cfif>
						<cfdump var="#result#" label="Cluster Result">

				</cfmail>
			</cfcatch>
		</cftry>

		<cfreturn allCfScheduledTaskQry>

	</cffunction> --->


	<cffunction name="getCFScheduledTasks" access="public" returntype="query" hint="Returns the ColdFusion Scheduled Tasks">

		<cfset var allScheduledTasks = application.com.clusterManagement.runFunctionOnCluster(functionName="getScheduledTasks",includeCurrentServer=true)>
		<cfset var allScheduledTasksQry = queryNew("task,url,status")>
		<cfset var instanceResponse = "">

		<cfloop collection="#allScheduledTasks#" item="instanceResponse">
			<cfif allScheduledTasks[instanceResponse].isOK>
				<Cfset var instanceScheduledTasks =  allScheduledTasks[instanceResponse].scheduledTasks>

				<cfif instanceScheduledTasks.recordCount gt 0>
					<cfquery name="allScheduledTasksQry" dbType="query">
						select * from instanceScheduledTasks
						<cfif allScheduledTasksQry.recordCount>
							union
							select * from allScheduledTasksQry
						</cfif>
						order by task
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn allScheduledTasksQry>
	</cffunction>


	<cffunction name="getScheduledTaskByName" access="public" output="true" returnType="struct">
		<cfargument name="name" type="string" required="true">

		<cfset var scheduledTasks = getCFScheduledTasks()>
		<cfset var result = {isOK=false}>
		<cfset var getScheduledTask = "">

		<cfquery name="getScheduledTask" dbType="query">
			select * from scheduledTasks where lower(task)='#lcase(arguments.name)#'
		</cfquery>

		<cfif getScheduledTask.recordCount>
			<cfset result = application.com.structureFunctions.queryRowToStruct(query=getScheduledTask)>
			<cfset result.isOk = true>
		</cfif>

		<cfreturn result>
	</cffunction>

</cfcomponent>
