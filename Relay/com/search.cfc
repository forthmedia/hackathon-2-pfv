<!--- �Relayware. All Rights Reserved 2014
2014-04-03	NYB	Case 439302 added line to exclude compression files from being included in the index
2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
2014-09-04 	WAB Case 441567 Fix to getFiles()
2015-06-03 	WAB Case 443965 alter prepareSearchString to deal with Quoted search strings
2015-07-08  DAN 445045 - results ordering for pages on portal search - sort alphabetically
2015/09/11	NJH	JIRA PROD2015-54 - remove any double spaces in the search string when getting the indexed files. Any double space causes a parse exception error in apache.lucene.queryParser
2015-09-11	ACPK	FIFTEEN-401	Updated getIndexedFiles to remove hyphens and quotation marks from search text string
2015-09-14	ACPK	FIFTEEN-401	Added asterisk, question mark, and comma to character blacklist; replaced replaceList() with REREplace()
2015-09-15	ACPK	FIFTEEN-401	Fixed regular expression used for blacklisting characters
2015-11-12  DAN PROD2015-364 - products country restrictions
2015-11-25  DAN Case 446654 - fix for search error due to no countryid on product
2015-12-07	WAB		BF-60 Shouldn't be removing double quotes from searchstring before passing to cfsearch
2015-12-14	WAB		But needs to deal with unmatched quotes
2016-02-16	RJT		PROD2015-607 Reorder the results such that items with an exact match in the title come first, then partial match in title, then everything else, ordered by Name alphabetically within those groups
2016-02-16	RJT		PROD2015-527 When searching for files associated with a product search the product's searchable fields for the files keywords AS WHOLE WORDS, i.e. EPA15 matches "EPA15 Awesome Product" but EPA1 doesn't'
2016/06/09	NJH		JIRA PROD2016-342 - add locations to search results. Left organisation searching alone at this point.
2016/08/12  GCC/DBB 451057 getCourses function - added joins to getRecordRightsTableByPerson to filter to courses/ modules the user has rights to see only in search results --->

<cfcomponent output="false">

	<cffunction name="getKeywordsForEntity" access="public">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityFields" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="fuzzyMatch" type="boolean" required="false" default="yes">

		<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var getKeywordsForEntityQry = "">
		<cfset var wordSeperators="\s_\.\-\:\,"> <!--- What represents seperation between "words". Note will go into regex so characters may need to be escaped (e.g. \s is space and \: is :) --->

		<cfset var count = 1>
		<cfset var fieldListLength = listLen(arguments.entityFields)>

		<cfquery name="getKeywordsForEntityQry" datasource="#application.siteDataSource#">
			select data from textMultipleFlagData t
				inner join flag f on f.flagID = t.flagID
				inner join flagGroup fg on fg.flagGroupID = f.flagGroupID and fg.flagGroupTextID like '%Keywords'
				inner join #arguments.entityType# e on
			<cfloop list="#arguments.entityFields#" index="local.field">
				<!--- 2016-11-18 SSPS-80 Match assests strictly by SKU depending on configuration option --->
				<cfif arguments.fuzzyMatch>
					dbo.RegExIsMatch('([#wordSeperators#]|^)' +data +'([#wordSeperators#]|$)',e.#local.field#,1) <> 0
				<cfelse>
					data = e.#local.field#
				</cfif>
				<cfif count lt fieldListLength>
					or
				</cfif>
				<cfset count++>
			</cfloop>
			where e.#entityTypeStruct.uniqueKey# = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
			union
			select data from textMultipleFlagData t
				inner join flag f on f.flagID = t.flagID
				inner join flagGroup fg on fg.flagGroupID = f.flagGroupID and fg.flagGroupTextID like '%Keywords'
				inner join vPhrases v on v.entityTypeID =  <cf_queryparam value="#application.entityTypeID[arguments.entityType]#" CFSQLTYPE="cf_sql_integer" > and charIndex(data,v.phraseText) <> 0
			where v.entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
				and v.phraseTextID in (<cf_queryParam value="#arguments.entityFields#" cfsqltype="CF_SQL_VARCHAR" list="true">)
		</cfquery>

		<cfreturn getKeywordsForEntityQry>
	</cffunction>

	<!--- JIRA PROD2016-342 - add locations to search results. Left organisation searching alone at this point. Removed 'tablesToSearch' from query as redundant, and made entityType return the actual entityType, rather than the friendly version. --->
	<cffunction name="getSiteSearchResults">
		<cfargument name="tablesToSearch" default="Person,Location,Organisation">
		<cfargument name="searchTextString" default="">
		<cfargument name="maxRows" default="500">
		<cfargument name="extraColumns" default="">
		<cfargument name="searchApprovedPOLOnly" type="boolean" default="false">
		<cfargument name="countryID" default="">

		<cfset var qSearch=queryNew('dummy')>
		<cfset var addUnion=false>
		<cfset var doListSearch=false>

		<cfif listLen(arguments.tablesToSearch)>
			<cfif reFindNoCase(application.com.regExp.standardexpressions.numericList,arguments.searchTextString)>
				<cfset doListSearch=true>
			</cfif>
			<cfquery name="qSearch">

				<cfif listFindNoCase(arguments.tablesToSearch,"Organisation")>
					SELECT entityID, entityName, entityLink, entityType, min(rank) as rank,3 as sortOrder<cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
					FROM (
						SELECT top #arguments.maxRows+1# o.OrganisationID as entityID, o.OrganisationName as entityName, o.OrgURL as entityLink, 'Organisation' as entityType, <cfif doListSearch>1 as rank<cfelse>ct.rank</cfif><cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
						FROM Organisation o
						<cfif not doListSearch>
						INNER JOIN CONTAINSTABLE(vOrganisationSearch,*,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">) as ct ON o.organisationID = ct.[key]
						</cfif>
						#application.com.rights.getRightsFilterQuerySnippet(entityType="organisation",alias="o").join#
						<cfif arguments.searchApprovedPOLOnly>
							inner join booleanFlagData bfdApprov on bfdApprov.entityID = o.organisationID and bfdApprov.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='orgApproved').flagID#" CFSQLTYPE="cf_sql_integer" >
						</cfif>
						left join booleanFlagData bfd on bfd.entityID = o.organisationID and bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='deleteOrganisation').flagID#" CFSQLTYPE="cf_sql_integer" >
						WHERE o.active = 1
							and bfd.flagID is null
						<!--- #application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause# --->
						<cfif isNumeric(arguments.searchTextString) and isValid("integer",arguments.searchTextString)>
							and o.OrganisationID = <cf_queryparam value="#arguments.searchTextString#" cfsqltype="CF_SQL_INTEGER">
						<cfelseif doListSearch>
							and o.OrganisationID IN (<cf_queryparam value="#arguments.searchTextString#" cfsqltype="CF_SQL_INTEGER" list="true">)
						</cfif>
						<cfif not (isNumeric(arguments.searchTextString) and isValid("integer",arguments.searchTextString)) or not doListSearch>
						UNION
						SELECT top #arguments.maxRows+1# o.OrganisationID as entityID, o.OrganisationName as entityName, o.OrgURL as entityLink, 'Organisation' as entityType, ct.rank<cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
						FROM Organisation o
						INNER JOIN Location l ON o.OrganisationID = l.OrganisationID
						INNER JOIN CONTAINSTABLE(vLocationSearch,*,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">) as ct ON l.locationID = ct.[key]
						#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l").join#
						<cfif arguments.searchApprovedPOLOnly>
							inner join booleanFlagData bfdApprov on bfdApprov.entityID = o.organisationID and bfdApprov.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='orgApproved').flagID#" CFSQLTYPE="cf_sql_integer" >
						</cfif>
						left join booleanFlagData bfd on (bfd.entityID = l.locationID and bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='deleteLocation').flagID#" CFSQLTYPE="cf_sql_integer" > ) or (bfd.entityID = o.organisationID and bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='deleteOrganisation').flagID#" CFSQLTYPE="cf_sql_integer" > )
						WHERE o.active = 1
						AND l.active = 1
						and bfd.flagID is null
						<cfif structKeyExists(arguments,'countryID') and isNumeric(arguments.countryID)>
							and o.countryID = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
						</cfif>
						<!--- #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# --->
						</cfif>
					) AS o
					GROUP BY entityID, entityName, entityLink, entityType<cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
					<cfset addUnion=true>
				</cfif>

				<cfif listFindNoCase(arguments.tablesToSearch,"Location")>
					<cfif addUnion>UNION</cfif>
					SELECT entityID, entityName, entityLink, entityType, min(rank) as rank, 2 as sortOrder<cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
					FROM (
						SELECT top #arguments.maxRows+1# l.locationID as entityID, l.sitename as entityName, l.specificURL as entityLink, 'Location' as entityType, <cfif doListSearch>1 as rank<cfelse>ct.rank</cfif><cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
						FROM Location l
						<cfif not doListSearch>
						INNER JOIN CONTAINSTABLE(vLocationSearch,*,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">) as ct ON l.locationID = ct.[key]
						</cfif>
						#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l").join#
						<!--- if in location aware mode and we're searching approved POL data, then join to locApproved; otherwise we just return all locations at approved orgs. --->
						<cfif arguments.searchApprovedPOLOnly and application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")>
							inner join booleanFlagData bfdApprov on bfdApprov.entityID = l.locationID and bfdApprov.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='locApproved').flagID#" CFSQLTYPE="cf_sql_integer" >
						</cfif>
						left join booleanFlagData bfd on bfd.entityID = l.locationID and bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='deleteLocation').flagID#" CFSQLTYPE="cf_sql_integer" >
						WHERE l.active = 1
							and bfd.flagID is null
						<cfif isNumeric(arguments.searchTextString) and isValid("integer",arguments.searchTextString)>
							and l.locationID = <cf_queryparam value="#arguments.searchTextString#" cfsqltype="CF_SQL_INTEGER">
						<cfelseif doListSearch>
							and l.locationID IN (<cf_queryparam value="#arguments.searchTextString#" cfsqltype="CF_SQL_INTEGER" list="true">)
						</cfif>
						<cfif structKeyExists(arguments,'countryID') and isNumeric(arguments.countryID)>
							and l.countryID = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
						</cfif>
					) AS l
					GROUP BY entityID, entityName, entityLink, entityType<cfif arguments.extraColumns is not "">,#arguments.extraColumns#</cfif>
					<cfset addUnion=true>
				</cfif>

				<cfif listFindNoCase(arguments.tablesToSearch,"Person")>
					<cfif addUnion>UNION</cfif>
					SELECT top #arguments.maxRows+1# p.PersonID as entityID, (p.FirstName + ' ' + p.LastName + ', ' + o.organisationName) as entityName, p.Email as entityLink, 'Person' as entityType, <cfif doListSearch>1 as rank<cfelse>ct.rank</cfif>,1 as sortOrder<cfif arguments.extraColumns is not "">,#preserveSingleQuotes(arguments.extraColumns)#</cfif>
					FROM Person p
					INNER JOIN organisation o on (p.organisationID = o.organisationid)
					<cfif not doListSearch>
					INNER JOIN CONTAINSTABLE(vPersonSearch,*,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">) as ct ON P.PersonID = ct.[key]
					</cfif>
					<cfif arguments.searchApprovedPOLOnly>
						inner join booleanFlagData bfdApprov on bfdApprov.entityID = p.personID and bfdApprov.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='perApproved').flagID#" CFSQLTYPE="cf_sql_integer" >
					</cfif>
					#application.com.rights.getRightsFilterQuerySnippet(entityType="person",alias="p").join#
					left join booleanFlagData bfd on bfd.entityID = p.personID and bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='deletePerson').flagID#" CFSQLTYPE="cf_sql_integer" >
					WHERE p.active = 1
						and o.active = 1
						and bfd.flagID is null
					<!--- #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# --->
					<cfif isNumeric(arguments.searchTextString) and isValid("integer",arguments.searchTextString)>
						and p.personID = <cf_queryparam value="#arguments.searchTextString#" cfsqltype="CF_SQL_INTEGER">
					<cfelseif doListSearch>
						and p.personID IN (<cf_queryparam value="#arguments.searchTextString#" cfsqltype="CF_SQL_INTEGER" list="true">)
					</cfif>
					<cfif structKeyExists(arguments,'countryID') and isNumeric(arguments.countryID)>
						and location.countryID = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
					</cfif>
				</cfif>

				ORDER BY sortOrder ASC, rank DESC
			</cfquery>

		</cfif>

		<cfreturn qSearch>

	</cffunction>


	<!--- 2015-06-03 WAB Case 443965 alter prepareSearchString to deal with Quoted search strings
		Previously "AAA BBB" would be converted to "AAA*" AND "BBB*"
		Now should stay as "AAA BBB"
		Whereas AAA BBB (no quotes) should still be converted to "AAA*" AND "BBB*"

		<cffunction name="prepareSearchString" output="false" access="public">
			<cfargument name="searchTextString" required="true">
			<cfset var returnVar = "">

			<cfloop list="#trim(arguments.searchTextString)#" index="local.i" delimiters='" '>
				<cfset returnVar &= "#trim(local.i)# ">
			</cfloop>

			<cfreturn '"#Replace(trim(returnVar),' ','*" AND "','all')#*"'>
		</cffunction>

	--->

	<cffunction name="prepareSearchString">
		<cfargument name="searchTextString" required="true">
		<cfargument name="addWildCard" default="true">
		<cfargument name="defaultConjunction" default="AND" required="false" />

		<cfscript>
			/* WAB 2015-12-14 remove any unmatched quotes */
			arguments.searchTextString = dealWithUnmatchedQuotes (string = searchTextString, replacementString = '');
			/* GCC 2016-04-07 if email is being used don't add wildcard*/
			var isValidEmail = isValid("email",searchTextString);
			if (isValidEmail) {
				arguments.addWildCard = false;
			}

			/* RegExp finds either a quoted string or a string delimited by a space  */
			var regexp = '(".*?"|[^"\s]+?(\s|\Z))';

			var parsedStringArray = application.com.regexp.reFindAllOccurrences (regExp, searchTextString);
			var result = "";
			var conjunctionAdded = true;

			arrayeach (parsedStringArray, function (item) {
				var trimmedText = trim (item.string);

				if (listfindNoCase ("OR,AND,NOT,NEAR",trimmedText)) {
					// If the word we find is a 'conjunction' then just add it to the result, and we won't need to add a conjunction before the next word
					result &= ' ' & trimmedText & ' ';
					conjunctionAdded = true;
				} else {
					// Add a conjunction
					if (!conjunctionAdded) {
						result &= ' ' & defaultConjunction & ' ';
					}
					/* remove quotes (we will replace them later, but this allows us to add the wildcard easily) */
					trimmedText = replace (trimmedText,'"','',"ALL");
					if (addWildCard) {
						trimmedText &= "*";
					}
					// Add quotes
					trimmedText = '"' & trimmedText & '"';
					result &= trimmedText;
					conjunctionAdded = false;
				}

			});

			return result;
		</cfscript>

	</cffunction>


	<cffunction name="getPortalSearchResults" output="true">
		<cfargument name="portalSearchID" default="">
		<cfargument name="searchTextString" default="">
		<cfargument name="languageid" default="#request.relaycurrentuser.languageid#">

		<cfset var qReturn=QueryNew("path,keyPath,category,type,Key,Title,URL,custom1,portalSearchID","VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar")>
		<cfset var qThisSearch = "">
		<cfset var stArgs = structCopy(arguments)>
		<cfset var qSources = getPortalSearchSources(portalSearchID=arguments.portalSearchID,languageid=arguments.languageID)>
		<cfset var qNormalized = "">
		<cfset structDelete(stArgs,"searchSources")>
		<cfloop query="#qSources#">
			<cfset stArgs["portalSearchID"]=qSources.portalSearchID>
			<cfset stArgs["path"]=qSources.path>
			<cfset stArgs["languageID"]=qSources.languageID>
			<cfset qThisSearch = evaluate("#searchMethodName#(argumentCollection=stArgs)")>

			<cfif qThisSearch.recordCount>
				<cfquery name="qNormalized" dbtype="query" >
					SELECT
						[path],keyPath,category,[type],[Key],Title,URL,custom1,portalSearchID
					FROM
						qThisSearch
				</cfquery>
				
				<cfset application.com.globalFunctions.queryAppend(qReturn, qNormalized)>
			</cfif>
		</cfloop>


		<!--- RJT PROD2015-527 we reorder the results such that items with an exact match in the title come first, then partial match in title, then everything else, ordered by Name alphabetically within those groups --->
		<cfscript>
			var columnNames=qReturn.getColumnNames();
			var columnNames_sanitised=ArrayNew(1);
			for(var i=1;i<=arrayLen(columnNames);i++){
					arrayAppend(columnNames_sanitised, "[#columnNames[i]#]"); //[ ] are in case of column names like Key, note that this is not a security measure (which isn't required as they are column names produced by a previous query)
			}

		</cfscript>


		<!--- RJT These unions are simulating case statements which are not available in queryOfQueries --->
		<cfquery dbtype="query" name="qReturn">
			<cfoutput>
					select #arrayToList(columnNames_sanitised)#,
					1 as searchQuality
					from qReturn
					where LOWER(Title)=LOWER(<cf_queryparam value="#searchTextString#" cfsqltype="cf_sql_varchar" >)
				union
					select #arrayToList(columnNames_sanitised)#,
					2 as searchQuality
					from qReturn
					where LOWER(Title) like LOWER(<cf_queryparam value="%#searchTextString#%" cfsqltype="cf_sql_varchar" >)
					and LOWER(Title)!=LOWER(<cf_queryparam value="#searchTextString#" cfsqltype="cf_sql_varchar" >)
				union
					select #arrayToList(columnNames_sanitised)#,
					3 as searchQuality
					from qReturn
					where Not (LOWER(Title) like LOWER(<cf_queryparam value="%#searchTextString#%" cfsqltype="cf_sql_varchar" >))
					and LOWER(Title)!=LOWER(<cf_queryparam value="#searchTextString#" cfsqltype="cf_sql_varchar" >)

				order by searchQuality, Title
			</cfoutput>
		</cfquery>

		<cfreturn qReturn>
	</cffunction>


	<cffunction name="getPortalSearchSources" output="false" returntype="query">
		<cfargument name="languageid" default="#request.relaycurrentuser.languageid#">
		<cfargument name="portalSearchID" default="">
		<cfset var qSources = "">

		<!--- dynamically get the collection name so that we don't have problems with moving dbs, etc. Also, getIndexedFiles is using the db name + files, so basing it off that. --->
		<cfquery name="qSources" datasource="#application.sitedatasource#" cachedwithin="#createTimeSpan(0,0,0,5)#">
			select ps.portalSearchID, ps.displayName, ps.searchMethodName, ps.indexMethodName, /*ps.collectionName, */
				case when indexMethodName is not null then  db_name() +replace(searchMethodName,'get','') else null end as collectionName,
			ps.path, ps.languageid, l.language
			from portalSearch ps
			left join language l on ps.languageid = l.languageid
			where ps.active = 1
			<cfif isNumeric(arguments.languageid)> and (ps.languageid = <cfqueryparam value="#arguments.languageid#" cfsqltype="cf_sql_integer"> or ps.languageid is null)</cfif>
			<cfif listLen(arguments.portalSearchID)> and (ps.portalSearchID in (<cfqueryparam value="#arguments.portalSearchID#" cfsqltype="cf_sql_integer" list="true">))</cfif>
			order by ps.sortOrder, ps.displayName
		</cfquery>
		<cfreturn qSources>
	</cffunction>


	<cffunction name="getPages" output="false" returntype="query">
		<cfargument name="searchTextString" default="">
		<cfargument name="maxRows" type="numeric" default="500">
		<cfargument name="path" default="">
		<cfargument name="portalSearchID" default="">

		<cfset var qPages = queryNew("dummy")>

		<cfif arguments.searchTextString neq "">
			<cfquery name="qPages" datasource="#application.siteDataSource#" maxrows="#arguments.maxRows#">
				SELECT distinct e.id as keyPath,
					--convert(varchar(12),e.id) + '_' + convert(varchar(12),h.languageid) + '_' +convert(varchar(12),h.phraseid) + '_' + convert(varchar(12),isnull(d.phraseid,0)) + '_' + convert(varchar(12),isnull(s.phraseid,0)) + '_' + convert(varchar(12),isnull(ls.phraseid,0)) as [key],
					convert(varchar(12),e.id) + '_' + convert(varchar(12),h.languageid) + '_' +convert(varchar(12),h.phraseid) as [key],
					isnull(h.phrasetext,'') as Title,
					isnull(s.phrasetext,'') as custom1,
					'' as URL,
					'pages' as type,
					'' as category,
					'#arguments.path#' + CONVERT(nvarchar(32), e.id) as path,
					'#arguments.portalSearchID#' as portalSearchID
				FROM element e
				INNER JOIN vphrasePointerCache h on h.phrasetextid = 'headline'
					and h.entitytypeid =  <cf_queryparam value="#application.entitytypeID.Element#" CFSQLTYPE="cf_sql_integer" >
					and h.entityid = e.id
					and h.countryid = #request.relayCurrentUser.countryid#
					and h.languageid = #request.relayCurrentUser.languageid#
				LEFT JOIN vphrasePointerCache d on d.phrasetextid = 'detail'
					and d.entitytypeid =  <cf_queryparam value="#application.entitytypeID.Element#" CFSQLTYPE="cf_sql_integer" >
					and d.entityid = e.id
					and d.Languageid = h.Languageid
					and d.countryid = h.countryid
				LEFT JOIN vphrasePointerCache s on s.phrasetextid = 'Summary'
					and s.entitytypeid =  <cf_queryparam value="#application.entitytypeID.Element#" CFSQLTYPE="cf_sql_integer" >
					and s.entityid = e.id
					and s.Languageid = h.Languageid
					and s.countryid = h.countryid
				LEFT JOIN vphrasePointerCache ls on s.phrasetextid = 'LongSummary'
					and ls.entitytypeid =  <cf_queryparam value="#application.entitytypeID.Element#" CFSQLTYPE="cf_sql_integer" >
					and ls.entityid = e.id
					and ls.Languageid = h.Languageid
					and ls.countryid = h.countryid
				WHERE e.id in
					(
						SELECT pl.entityid
							FROM PhraseList pl
							INNER JOIN phrases p ON pl.phraseid = p.phraseid
							INNER JOIN phrasepointercache pp on pp.phrase_ident = p.ident
							INNER JOIN vPhrasesSearch ps on ps.ident = p.ident
					      WHERE pl.entityTypeID =  <cf_queryparam value="#application.entitytypeID.element#" CFSQLTYPE="cf_sql_integer" >
							and CONTAINS(ps.phraseText,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">)
							and pp.languageid = #request.relayCurrentUser.languageid#
							and pp.countryid =  <cf_queryparam value="#request.relayCurrentUser.content.showForCountryID#" CFSQLTYPE="cf_sql_integer" >
							and pl.EntityID in (Select elementID from elementTreeCacheData where elementTreeCacheid =  <cf_queryparam value="#request.currentelementtreecacheid#" CFSQLTYPE="cf_sql_integer" > )
					)
                ORDER BY Title <!--- 2015-07-08  DAN 445045 - results ordering for pages on portal search - sort alphabetically --->
			</cfquery>
		</cfif>
		<cfreturn qPages>
	</cffunction>


	<cffunction name="getProducts" output="false">
		<cfargument name="searchTextString" default="">
		<cfargument name="maxRows" type="numeric" default="500">
		<cfargument name="path" default="">
		<cfargument name="portalSearchID" default="">
        <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">
        <cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var qProducts = queryNew("dummy")>

		<cfif arguments.searchTextString neq "">

			<!--- 2015-11-25  DAN Case 446654 --->
			<cfquery name="qProducts" datasource="#application.siteDataSource#" maxrows="#arguments.maxRows#">
				select distinct cast(d.CountryID as varchar) + '_' + cast(d.languageid as varchar) + '_' + cast(p.ProductID as varchar) + '_' + cast(p.campaignid as varchar) as [key],
					d.phraseText as title,
					p.sku as custom1,
					'#arguments.path#searchresults' as URL,
					'products' as type,
					'' as category,
					p.productid as keyPath,
					'#arguments.path#' + CONVERT(nvarchar(32), p.productid) as path,
					'#arguments.portalSearchID#' as portalSearchID
				from product p
                    left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid
					inner join vphrasePointerCache d on d.phrasetextid = 'title'
						and d.entitytypeid =  <cf_queryparam value="#application.entitytypeID.Product#" CFSQLTYPE="cf_sql_integer" >
						and d.entityid = p.productid
						and d.languageid = #request.relayCurrentUser.languageid#
						and d.countryid = #request.relayCurrentUser.countryid#
					left join vphrasePointerCache ld on ld.phrasetextid = 'description'
						and ld.entitytypeid =  <cf_queryparam value="#application.entitytypeID.Product#" CFSQLTYPE="cf_sql_integer" >
						and ld.entityid = p.productid
						and d.languageid = ld.languageid
				where 1=1 <!--- p.CountryID is not null --->
                    and (p.hascountryScope = 0
                        <cfif request.relayCurrentUser.isInternal> <!--- just in case - possibly this function is also used on the internal site --->
                            OR vcs.countryid in (select countryid FROM vCountryRights vcr with (noLock) WHERE vcr.personid = <cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER"> )
                        <cfelse>
                            OR vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
                        </cfif>
                    )
					and p.Deleted = 0
					and p.active = 1
					and (p.productid in (
							SELECT pl.entityid
								FROM PhraseList pl
								INNER JOIN phrases p ON pl.phraseid = p.phraseid
								INNER JOIN phrasepointercache pp on pp.phrase_ident = p.ident
						      WHERE pl.entityTypeID = 24
								AND CONTAINS(p.phraseText,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">)
								and pp.languageid = #request.relayCurrentUser.languageid#
								and pp.countryid =  <cf_queryparam value="#request.relayCurrentUser.content.showForCountryID#" CFSQLTYPE="cf_sql_integer" >
							)
							or CONTAINS(p.*, <cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">)
						)
			</cfquery>


		</cfif>
		<cfreturn qProducts>
	</cffunction>


	<cffunction name="getCourses" output="false">
		<cfargument name="searchTextString" default="">
		<cfargument name="maxRows" type="numeric" default="500">
		<cfargument name="path" default="">
		<cfargument name="portalSearchID" default="">

		<cfset var qCourses = queryNew("dummy")>

		<cfif arguments.searchTextString neq "">
			<!--- 451057 - GCC / DBB added joins to getRecordRightsTableByPerson to filter to courses/ modules the user has rights to see only in search results--->
			<cfquery name="qCourses" datasource="#application.siteDataSource#" maxrows="#arguments.maxRows#">
				SELECT DISTINCT 'e_c_' + CONVERT(varchar(10), tc.courseId) AS [key],
				  	CONVERT(nvarchar(255), courseTitle.phrasetext) AS Title,
				  	CONVERT(nvarchar(4000), courseDescription.phrasetext) AS custom1,
					'' as URL,
					'courses' as type,
					'' as category,
					tc.courseId as keyPath,
					'#arguments.path#' + CONVERT(nvarchar(32), tc.courseId) as path,
					'#arguments.portalSearchID#' as portalSearchID
				FROM trngCourse tc
				  INNER JOIN vphrasePointercache courseTitle ON courseTitle.entitytypeID =  <cf_queryparam value="#application.entitytypeID.trngcourse#" CFSQLTYPE="cf_sql_integer" >
				  	AND tc.courseId = courseTitle.entityID
				  	AND courseTitle.phrasetextid = 'title'
				  left JOIN vphrasePointercache courseDescription ON courseDescription.entitytypeID =  <cf_queryparam value="#application.entitytypeID.trngcourse#" CFSQLTYPE="cf_sql_integer" >
				  	AND tc.courseId = courseDescription.entityID
				  	AND courseDescription.phrasetextid = 'description'
				  	AND courseTitle.languageID= courseDescription.languageID
				  	AND courseTitle.countryID = courseDescription.countryID
				  	left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer">) as rr on rr.recordid = tc.courseId
				where tc.publishedDate <= GETDATE()
					and rr.level1 = 1
					AND tc.courseId in
					(
						SELECT pl.entityid
							FROM PhraseList pl
							INNER JOIN phrases p ON pl.phraseid = p.phraseid
							INNER JOIN phrasepointercache pp on pp.phrase_ident = p.ident
					      WHERE pl.entityTypeID =  <cf_queryparam value="#application.entitytypeID.trngcourse#" CFSQLTYPE="cf_sql_integer" >
							and CONTAINS(p.phraseText,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">)
							and pp.languageid = #request.relayCurrentUser.languageid#
							and pp.countryid = #request.relayCurrentUser.content.showForCountryID#
					)
					AND (courseTitle.languageID = #request.relayCurrentUser.languageid# OR courseTitle.languageID = 0)

				UNION

				SELECT DISTINCT 'e_m_' + CONVERT(varchar(10), tm.moduleId) + '_' + CONVERT(varchar(10), tmc.courseId) AS [key],
				  	CONVERT(nvarchar(255), moduleTitle.phrasetext) AS Title,
				  	CONVERT(nvarchar(4000), moduleDescription.phrasetext) AS custom1,
				 	'' as URL,
				 	'courses' as type,
				 	'' as category,
				 	tm.moduleid as keyPath,
				 	'#arguments.path#' + CONVERT(nvarchar(32), tmc.courseId) + '&moduleID=' + CONVERT(nvarchar(32), tm.moduleid) as path,
				 	'#arguments.portalSearchID#' as portalSearchID
				FROM trngModule tm
					INNER JOIN trngModuleCourse tmc ON tmc.moduleID = tm.moduleID
				 	INNER JOIN vphrasePointercache moduleTitle ON moduleTitle.entitytypeID =  <cf_queryparam value="#application.entitytypeID.trngModule#" CFSQLTYPE="cf_sql_integer" >
				 		AND tm.moduleId = moduleTitle.entityID
				 		AND moduleTitle.phrasetextid = 'title'
				  	INNER JOIN vphrasePointercache moduleDescription ON  moduleDescription.entitytypeID =  <cf_queryparam value="#application.entitytypeID.trngModule#" CFSQLTYPE="cf_sql_integer" >
				  		AND tm.moduleId = moduleDescription.entityID
				  		AND moduleDescription.phrasetextid = 'description'
				  		AND moduleTitle.languageID= moduleDescription.languageID
				  		AND moduleTitle.countryID = moduleDescription.countryID
				  	INNER JOIN vphrasePointercache trngCourseTitle ON trngCourseTitle.entitytypeID =  <cf_queryparam value="#application.entitytypeID.trngCourse#" CFSQLTYPE="cf_sql_integer" >
				  		AND tmc.courseId = trngCourseTitle.entityID
				  		AND trngCourseTitle.phrasetextid = 'title'
				  		AND moduleTitle.languageID= trngCourseTitle.languageID
				  		AND moduleTitle.countryID = trngCourseTitle.countryID
				  		left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer">) as rr on rr.recordid = tm.moduleid
				where tm.publishedDate <= GETDATE()
					and rr.level1 = 1
					AND tm.moduleid in
					(
						SELECT pl.entityid
							FROM PhraseList pl
							INNER JOIN phrases p ON pl.phraseid = p.phraseid
							INNER JOIN phrasepointercache pp on pp.phrase_ident = p.ident
					      WHERE pl.entityTypeID =  <cf_queryparam value="#application.entitytypeID.trngModule#" CFSQLTYPE="cf_sql_integer" >
							AND CONTAINS(p.phraseText,<cf_queryparam value='#prepareSearchString(arguments.searchTextString)#' cfsqltype="CF_SQL_VARCHAR">)
							and pp.languageid = #request.relayCurrentUser.languageid#
							and pp.countryid = #request.relayCurrentUser.content.showForCountryID#
					)
					AND (moduleTitle.languageid = #request.relayCurrentUser.languageid# OR moduleTitle.languageID = 0)
			</cfquery>

		</cfif>
		<cfreturn qCourses>
	</cffunction>

	<cffunction name="getIndexedFiles" access="public" returnType="query">
		<cfargument name="searchTextString" default="">
		<cfargument name="portalSearchID" default="">
		<cfargument name="maxRows" type="numeric" default="500">

		<cfset var preFilter = "">
		<cfset var collectionName = "">
		<cfset var getDBName = "">
		<cfset var qFiles = querynew("dummy")>

		<cfset var preparedSearchString = prepareSearchStringForCFSearch(arguments.searchTextString)>
		<cfif arguments.portalSearchId neq "">
			<cfset collectionName = getPortalSearchSources(portalSearchID=arguments.portalSearchID).collectionName>
		<cfelse>
			<cfquery name="getDBName">
				select db_name() as dbname
			</cfquery>
			<cfset collectionName = "#getDBName.dbname#Files">
		</cfif>
		<cfif listFind(CGI.SERVER_NAME,"servicemobile",".")>
			<cfset preFilter = "+LST M ">
		</cfif>
		<cfif preparedSearchString is not "">
			<cfsearch name="qFiles" collection="#collectionName#" criteria="#preFilter# + #preparedSearchString#"  maxrows="#arguments.maxRows#">
		</cfif>
		<cfreturn qFiles>
	</cffunction>


	<cffunction name="prepareSearchStringForCFSearch" returnType="string">
		<cfargument name="searchTextString" required="true">

		<!--- 	2015-09-11	ACPK	FIFTEEN-401	Updated getIndexedFiles to remove hyphens and quotation marks from search text string
				2015-09-14	ACPK	FIFTEEN-401	Added asterisk, question mark, and comma to character blacklist; replaced replaceList() with REREplace()
				2015-09-15	ACPK	FIFTEEN-401	Fixed regular expression used for blacklisting characters
				2015-12-07	WAB		Move code into its own function
				2015-12-07	WAB		BF-60 Shouldn't be removing double quotes, since they are used to specify that words have to be next to each other.
				2015-12-14	WAB		But need to be able to deal with someone entering unmatched quotes
									And can escape the special characters, don't have to remove them

				NOTE:  This is still fairly simplistic, and doesn't support some of the sophisticated stuff that Solr can do
						The replacing all spaces with ' +' is a bit brute force, but oddly it does seem to work properly if there is an OR in the search string

		--->

		<cfset var result = unEncodeQUOT ( searchTextString )>
		<cfset result = dealWithUnmatchedQuotes (string = result, replacementString = '\"')>
		<cfset result = REReplace(result ,"([!^+\-:~\[\]{}()'*?,])","\\\1","ALL")>
		<cfset result = trim(lcase (result))> <!--- by passing in an all lower case string, Solr does a case insensitive search --->
		<cfset result = REReplace( result,' {2,}',' ','ALL')> <!--- NJH replace double empty string with single empty string --->
		<cfset result = replace(result, ' ', ' +', 'all')>  <!--- The plus makes for an AND between words--->

		<cfreturn result>
	</cffunction>


	<cffunction name="unEncodeQUOT" returnType="string" hint="Returns &quot; to double quote">
		<cfargument name="String" required="true">

		<cfreturn replaceNoCase(string, "&quot;", '"', "ALL")>

	</cffunction>


	<cffunction name="dealWithUnmatchedQuotes" returnType="string" hint="">
		<cfargument name="String" required="true">
		<cfargument name="replacementString" required="true"> <!--- for full text search we can escape, for SQL we delete --->

		<!--- find all quotes which are not already escaped --->
		<cfset var result = string>
		<cfset var regExp = '(?<!\\)"'>
		<cfset var find = application.com.regexp.refindOccurrences(reg_expression = regExp, string = string, usejavamatch = true)>

		<cfif arrayLen(find) MOD 2 is not 0>
				<!---
				could try to get very sophisticated about what to do now, but lets just keep it simple
				so we will escape one of the quotes, might as well be the middle one!
				--->

				<cfset var quoteToEscape = int(arrayLen(find)/2) + 1>
				<!--- rwReplaceNoCasecan't do a replace starting at a particular location, but it will do --->
				<cfset result = application.com.globalFunctions.rwReplaceNoCase (string = result, substring1 = '"', substring2 = replacementString , start = find[quoteToEscape].position, scope = "ONE") >
		</cfif>

		<cfreturn result>

	</cffunction>


	<!---
	WAB 2014-09-04 CASE 441567.  Bug in this area had been fixed wrongly by uncommenting some old code.  Problem was actually in the variable being returned.  Have fixed removed all the old code.
	--->
	<cffunction name="getFiles" output="true">
		<cfargument name="searchTextString" default="">
		<cfargument name="maxRows" type="numeric" default="500">
		<cfargument name="path" default="">
		<cfargument name="portalSearchID" default="">

		<cfset var args =structNew()>
		<cfset var getResults=QueryNew("dummy")>

		<cfif arguments.searchTextString neq "">
			<!--- WAB 2016-09-27 During PROD206-2393 a change to sql getFile() function required the filter in this call to be aliased properly --->
			<cfset args = {personid=request.relaycurrentuser.personid,returnCountryIDList=false,checkFileExists=false,checkExpires=true,languageid=request.relayCurrentUser.languageid,returnColumns="f.fileID as [key],f.name as title,'' as path, '' as keyPath, '' as category, 'files' as type, '' as url, '' as custom1, '#arguments.portalSearchID#' as portalSearchID",searchTables="files,keywords",searchString=arguments.searchTextString,filter="portalSearch = 1 and DefaultDeliveryMethod = 'Local' and ft.path is not null and f.filename is not null",orderby="title"}>
			<cfset getResults = application.com.filemanager.getFilesAPersonHasCountryViewRightsTo(argumentCollection = args)>
		</cfif>

		<cfreturn getResults>
	</cffunction>


	<cffunction name="updateFilesSearchCollectionInThread">
		<cfargument name="collectionName" default="">
		<cfset var qSources = "">
		<cfset var qSource = "">
		<!--- 2014/11/12 GCC 442218 Fix solr search not refreshing on file change START - including <cfset var qSource = ""> --->

		<cfif arguments.collectionName is "">
			<cfset qSources = getPortalSearchSources()>
			<cfquery dbtype="query" name="qSource" maxrows=1>
				select collectionName
				from qSources
				where collectionName is not null
			</cfquery>

			<cfif qSource.recordCount>
				<cfset arguments.collectionName = qSource.collectionName>
			</cfif>
		</cfif>

		<cfthread name="updateFilesSearchCollectionInThread" feed=#arguments#>
			<cftry>
				<cfset updateFilesSearchCollection(argumentCollection=feed)>
				<cfcatch>
					<cfset application.com.errorHandler.recordRelayError_warning(catch = cfcatch,severity="error",Type="SOLR Portal Search",sendEmail="true",emailSubject="SOLR PORTAL SEARCH Update Error",emailTo="relayhelp@relayware.com")>
				</cfcatch>
			</cftry>
		</cfthread>

		<!--- 2014/11/12 GCC 442218 Fix solr search not refreshing on file change END--->
	</cffunction>


	<cffunction name="updateFilesSearchCollection">
		<cfargument name="collectionName" default="">
		<cfargument name="fileID" default="">

		<cfset var returnVar=false>
		<cfset var searchAction="refresh">
		<cfset var qFiles="">
		<cfset var ignoreFileExtensionList="zip,cab,%ar,tgz,wim,gz,kgb,lz%,pak%,pq%,ai">

		<cfif listLen(arguments.fileID)>
			<cfset var searchAction="update">
		</cfif>

		<cfquery name="qFiles" datasource="#application.sitedatasource#">
			select '/content/' + t.path + '/' + f.filename as urlPath, '#application.paths.content#\' + t.path + '\' + f.filename as fileKey, f.fileid
			<!--- (Select ' ' + f1.data  From textMultipleFlagData f1 Where f1.entityid = f.fileid For XML Path('')) as keywords --->
			from files f
			inner join filetype t on f.filetypeid = t.filetypeid
			inner join filetypegroup g on t.filetypegroupid = g.filetypegroupid
			where t.DefaultDeliveryMethod = 'Local'
			and t.path is not null
			and f.filename is not null
			<!--- 2014-04-03 NYB Case 439302 added: --->
			<!--- and f.filename NOT like '%.zip%' and f.filename NOT like '%.cab' and f.filename NOT like '%.%ar' and f.filename NOT like '%.tgz' and f.filename NOT like '%.wim' and f.filename NOT like '%.gz' and f.filename NOT like '%.kgb' and f.filename NOT like '%.lz%' and f.filename NOT like '%.pak%' and f.filename NOT like '%.pq%' --->
			<cfloop list="#ignoreFileExtensionList#" index="fileExt">
				and f.filename not like '%.#fileExt#'
			</cfloop>
			<cfif searchAction is "update">
				and f.fileid <cfif isNumeric(arguments.fileid)>= <cfqueryparam value="#arguments.fileid#" cfsqltype="cf_sql_integer"><cfelse>in (<cfqueryparam value="#arguments.fileid#" cfsqltype="cf_sql_integer" list="true">)</cfif>
			</cfif>
			group by '/content/' + t.path + '/' + f.filename, '#application.paths.content#\' + t.path + '\' + f.filename, f.fileid
		</cfquery>


		<cfindex collection="#arguments.collectionName#"
		    query="qFiles"
			action="#searchAction#"
		   	type="file"
		    key="fileKey"
		    custom1="fileid"
			urlpath="urlPath">

		<cfif searchAction is "refresh">
			<cfquery name="qFiles" datasource="#application.sitedatasource#">
				update portalSearch
				set lastIndexed = getDate()
				where collectionName = <cfqueryparam value="#arguments.collectionName#" cfsqltype="cf_sql_varchar">
			</cfquery>
		</cfif>
		<cfset returnVar=true>

		<cfreturn returnVar>
	</cffunction>

	<!--- 2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
	<cfscript>
		public query function getRecentSearches
		(
			required numeric top
		)
		{
			var sql = "
				SELECT TOP #arguments.top#
					searchString,
					MAX(searchdate)
				FROM tracksearch  WITH (NOLOCK)
				WHERE searchType = 'D'
				AND personid = :personid
				AND searchDate > DATEADD(year,-1,GETDATE())
				GROUP BY searchString
				ORDER BY MAX(searchdate) DESC
			";
			qObj = new query();
			qObj.setDatasource(application.sitedatasource);
			qObj.addParam(name="personid",cfsqltype="CF_SQL_INTEGER",value=request.relaycurrentuser.personid);
			var qGetRecentSearches = qObj.execute(sql=sql).getResult();

			return qGetRecentSearches;
		}
	</cfscript>


</cfcomponent>
