<!---  �Relayware. All Rights Reserved 2014 --->
<!--- OVERVIEW:
File name:			dbTools.cfc
Author:				???
Date started:		??/??/????

Description:

Possible enhancements:

Available Functions:
	sp_rename - public, output: true, returntype: string, purpose:Uses sp_rename to rename a column
	getOrgMatchNameReplaceStrings - public, purpose: Creates the org match name replace string
	getPersonMatchname - public, purpose: Creates the person match name replace string
	updateOrgMatchname - public, purpose: Creates the org match name
	updatePersonMatchname - public, purpose: Creates the person match name
	checkOrgMatchname - public, purpose: checks the org match name
	updateColumnValue - public, returntype: string
	trimData - public, purpose:Trims data and removes leading and trailing spaces.
	getJobList - public, purpose:Gets the most recent executions of the jobs for this server
	getTableDetails - public, purpose:Returns details of this table from sysTables
	dropColumn - public, purpose:Deletes a column from table.
	addColumn - public, purpose:Adds a column to a table.
	getDatabaseBlocks - public, purpose:Gets Info on Blocking Transactions
	DeleteRecords - public, purpose:Delete Records from table.
	flipflop - purpose:Puts the source table live and renames the live table to the source table
	TableExists - purpose:Checks to see if a user table of this name exists in the database

--->

<!--- Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-10-04  		    WAB 		Mod to checkOrgMatchname, updateorgmatchname for unicode
2008-05-06			SSS			Added a function called flipflop changes table names to make on table live and another table ready to add info
2008/05/28			WAB			modification to the blocking detection to get some more info
2008/08/29	 		NYB			added optional argument returnColumns to table getTableDetails to return
					 			column details - making it fully backward compatible
2008/11/28 			NJH			added a series of Archiving Functions
2009-04-22 			NJH			Sophos - added argument columnName and column sc.length as Column_Length to function getTableDetails - only used if returnColumns set to true
2009-04-30 			NJH			Sophos - created checkLoc & checkPer functions - called from relay\remoteAddTask.cfm
2009-05-12 			NJH			added Location dedupe code
2009-05-18 			NJH			added getColumnSize function
2009/05/28			NJH			P-SNY063 - altered function getTableDetails to return some extra columns such as isIdentity,isPrimaryKey, isNullable, etc. Also update org matchname for single org
								if orgId is passed through.
2009/06/25 			NJH			Bug Fix Sophos Support Issue 2331 - added lenOfReplaceString to result set to getReplaceStrings query
2009-09-28 			NYB 		LHID2672
2009/11/04 			NJH			P-fnl079 SFDC Integration
2010/02/15 			WAB/NAS		LID3095
2010-05-28 			AJC 		P-FNL090 Email Scheduler
NYB 2011-02-02 LHID5321 added caching to all getTableDetails queries, to 1 day
NYB 2011-02-08 LHID5321 made a change to getTableDetails to fix bug introduced by FNL079 in 2009
2011/05/25	NYB		REL106 added getTableDetailsStruct function
2011/08/05			NJH			Added 'excludeSalesForceRecords' when searching for matches on organisations/locations.
2011-08-16			STCR			LID7257
2011/10/04 			PPB 		NET005 changed getOrgMatchNameReplaceStrings to be compatible with how location works
2011-10-20 			NYB 		LHID7283 added Preservesinglequotes to query
2013-10-28			WAB			Renamed Functions getLocationMatchName() and getOrgMatchName() to getLocationMatchNameReplaceStrings() and getOrgMatchNameReplaceStrings()
								In preparation for functions with the same names which actually do return a matchname
2013/09/12			NJH			2013 Roadmap 2 / Case 438724 Improve matchname update to only run when matchnames are null
2013-12-20 			PPB 		Case 438460 added DESC to make longer strings take priority when creating matchnames
2014-03-19			AXA			Case 439176: removed arguments list, as they are optional, and may vary. Loop through the argument collection passed in to build the column list for the temp table
2014-03-31 			PPB 		Case 439252 replace all full-stops first so eg PLC and PLC. are replaced without having to specify both
2014/05/13			NJH			Added a number of new generic matching functions based on some rules set in the this scope. This is a stake in the ground to get the new matching
								functionality working, so ideally these functions should be used/improved upon going forward.
2014-11-10 			DXC 		Case 442434 Increased trim length of matchname to 100 chars to deal with Russian names
2014/12/03			NJH			Removed a number of datasource arguments
2015-06-23          DAN         Case 445081 - changed random chars (1J5?c REPLACE ME 1981) to not contain special characters as it caused SQL query to crash, use random UUID instead (b8fc445619be11e5b60b1697f925ec7b)
19/02/2016          DAN         Case 447952 - allow to check for organisation type when matching organistation names
2016/03/31			NJH			Case ??? kaspersky upgrade. increase in org match name. Lengthened orgMatchName to 255 characters. Also, in connector testing, found out that we were matching
								Admin companies/people. Now remove those from potential match candidates.
 --->

<cfcomponent displayname="RelaydbTools" hint="Various database functions">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             renames a colum
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="sp_rename" output="true"
		returntype="string"  hint="Uses sp_rename to rename a column">
			<cfargument name="tableName" type="string" required="true">
			<cfargument name="dataSource" type="string" required="true">
			<cfargument name="oldColumnName" type="string" required="true">
			<cfargument name="newColumnName" type="string" required="true">
			<cfset newColumnName = replace(newColumnName," ","","ALL")>
			<cfquery name="renameCol" datasource="#dataSource#">
				EXEC sp_rename '#tableName#.[#oldColumnName#]', '#newColumnName#', 'COLUMN'
			</cfquery>
			<cfreturn "#tableName#.[#oldColumnName#] changed to #tableName#.#newColumnName#">
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Sets a column in a table to a new value
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="updateColumnValue" access="public" returntype="string"
		hint="Sets columnName in tableName to newColumnValue where primaryKey in primaryKeyIDList">

		<cfargument name="primaryKeyIDList" required="yes" type="string">
		<cfargument name="newColumnValue" required="yes" type="string">
		<cfargument name="tableName" required="yes" type="string">
		<cfargument name="primaryKey" required="yes" type="string">
		<cfargument name="columnName" type="string" default="search" required="yes">

		<cfquery name="getColData" datasource="#application.SiteDataSource#">
			select data_type from information_schema.columns
				where column_name =  <cf_queryparam value="#arguments.columnName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and table_name =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<!--- build where clause --->
		<cfif listLen(primaryKey,"|") neq listLen(primaryKeyIDList,"|")>
			<cfset primaryKey = listGetAt(primaryKey,1,"|")>
			<cfset primaryKeyIDList = listGetAt(primaryKeyIDList,1,"|")>
		</cfif>

		<cfif listLen(primaryKey,"|") eq listLen(primaryKeyIDList,"|")>
			<cfset whereClause = "">
			<cfset incr = 1>

			<cfloop list="#primaryKey#" index="x" delimiters="|">
				<cfquery name="getPrimaryColData" datasource="#application.SiteDataSource#">
					select data_type from information_schema.columns
						where column_name =  <cf_queryparam value="#x#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and table_name =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>
				<!--- <cfoutput><script>alert("#x# - #getPrimaryColData.data_type#");</script></cfoutput> --->
				<cfif whereClause eq "">
					<cfset whereClause = "#x# IN ">
				<cfelse>
					<cfset whereClause = "#whereClause# AND #x# IN ">
				</cfif>

				<cfswitch expression="#getPrimaryColData.data_type#">
					<cfcase value="int,double">
						<cfset whereClause = "#whereClause#(#listGetAt(primaryKeyIDList,incr,'|')#)">
					</cfcase>
					<cfdefaultcase>
						<cfset whereClause = "#whereClause#('#listGetAt(primaryKeyIDList,incr,"|")#')">
					</cfdefaultcase>
				</cfswitch>

				<cfset incr = incr + 1>
			</cfloop>
		</cfif>


		<CFQUERY NAME="updateColumnValue" DATASOURCE="#application.SiteDataSource#">
			update #arguments.tableName#
				set #arguments.columnName# =
					<cfswitch expression="#getColData.data_type#">
						<cfcase value="date">
							<cfif isDate(trim(column))>
									<cf_queryparam value="#dateformat(trim(arguments.newColumnValue),"dd-mmm-yyyy")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
							<cfelse>
									<cf_queryparam value="#trim(arguments.newColumnValue)#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfif>
						</cfcase>
						<cfcase value="int">#trim(arguments.newColumnValue)#</cfcase>
						<cfdefaultcase>'#trim(arguments.newColumnValue)#'</cfdefaultcase>
					</cfswitch>
			where #whereClause#
		</cfquery>

	<cfreturn "Values changed">

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Trims all of the columns in the data table
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="trimData" hint="Trims data and removes leading and trailing spaces.">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="columnList" type="string" required="true">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">

		<cfscript>
			if (tablename eq "person")
				modTriggerName = "PM";
			else if (tablename eq "location")
				modTriggerName = "LM";
			else if (tablename eq "organisation")
				modTriggerName = "OM";
		</cfscript>

		<cfif isDefined("modTriggerName")>
			<cfquery name="trimData" datasource="#arguments.dataSource#">
				Update modTrigger set ISON = 0 WHERE NAME =  <cf_queryparam value="#modTriggerName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
		</cfif>

		<cfset thisRow = "1">
		<cfquery name="trimData" datasource="#arguments.dataSource#">
			Update #arguments.tableName#
				Set
				<cfloop index="i" list="#arguments.columnList#">
					#i# = Rtrim(Ltrim(#i#))<cfif listLen(columnList) neq thisRow>,</cfif>
					<cfset thisRow = thisrow + 1>
				</cfloop>
		</cfquery>

		<cfif isDefined("modTriggerName")>
			<cfquery name="trimData" datasource="#arguments.dataSource#">
				Update modTrigger set ISON = 1 WHERE NAME =  <cf_queryparam value="#modTriggerName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
		</cfif>

		<cfreturn "Leading and trailing spaces removed from #arguments.tablename#: #arguments.columnList#">
	</cffunction>


<!---

alter table actions drop column actionID
alter table actionNote drop column actionNoteID

alter table dataLoadCol add relayColumn varchar(50) NULL



/* ADD COLUMNS TO DATABASE*/
alter table element add elementTextID varchar(30) NULL
alter table element add isTreeRoot int NULL
alter table files add securityCode varchar(30) NULL
alter table [actions] add actionID INT NOT NULL IDENTITY PRIMARY KEY CLUSTERED
ALTER TABLE doc_exc ADD column_b VARCHAR(20) NULL CONSTRAINT exb_unique UNIQUE
alter table actionNote add actionNoteID int not null constraint completeDflt default 0

-- you can't alter an identity so drop and add col
alter table actionNote drop column actionNoteID
alter table actionNote add actionNoteID int not null IDENTITY PRIMARY KEY CLUSTERED


/* CONSTRAINTS */
ALTER TABLE doc_exc ADD column_b VARCHAR(20) NULL CONSTRAINT exb_unique UNIQUE
--add a unique constraint to an existing table
alter table files NOCHECK CONSTRAINT securityCode_UN UNIQUE(securityCode)
-- Disable a constraint using NOCHECK.
ALTER TABLE cnst_example NOCHECK CONSTRAINT salary_cap
-- Reenable a constraint using CHECK.
ALTER TABLE cnst_example CHECK CONSTRAINT salary_cap


/* TRIGGERS */
-- Disable the trigger.
ALTER TABLE trig_example DISABLE TRIGGER trig1
-- Re-enable the trigger.
ALTER TABLE trig_example ENABLE TRIGGER trig1


/* Add a nullable column with default values
This example adds a nullable column with a DEFAULT definition,
and uses WITH VALUES to provide values for each existing row in the table.
If WITH VALUES is not used, each row has the value NULL in the new column. */

ALTER TABLE MyTable
ADD AddDate smalldatetime NULL
CONSTRAINT AddDateDflt
DEFAULT getdate() WITH VALUES
 --->
<cffunction access="public" name="getJobList" hint="Gets the most recent executions of the jobs for this server">
	<cfquery name="getJobList" datasource="#datasource#">
	<!--- needs modifciation to return the most recent --->
	use msdb
	GO
	SELECT DISTINCT
	       SUBSTRING(j.name,1,60) AS JobName
	      ,SUBSTRING(CAST(h.run_date AS VARCHAR(8)),5,2) + '/' +
	       SUBSTRING(CAST(h.run_date AS VARCHAR(8)),7,2) + '/' +
	       SUBSTRING(CAST(h.run_date AS VARCHAR(8)),1,4) AS run_date

	      ,SUBSTRING(RIGHT( '000000' + CAST(RTRIM(h.run_time) AS varchar(6)), 6),1,2)  + ':' +
	       SUBSTRING(RIGHT( '000000' + CAST(RTRIM(h.run_time) AS varchar(6)), 6),3,2)  + ':' +
	       SUBSTRING(RIGHT( '000000' + CAST(RTRIM(h.run_time) AS varchar(6)), 6),5,2) AS run_time

	      ,SUBSTRING(RIGHT( '000000' + CAST(RTRIM(h.run_duration) AS varchar(6)), 6),1,2)  + ':' +
	       SUBSTRING(RIGHT( '000000' + CAST(RTRIM(h.run_duration) AS varchar(6)), 6),3,2)  + ':' +
	       SUBSTRING(RIGHT( '000000' + CAST(RTRIM(h.run_duration) AS varchar(6)), 6),5,2) AS run_duration
	  FROM sysjobhistory h WITH (READUNCOMMITTED)
	  JOIN sysjobs j
	    ON (h.job_id = j.job_id)
	   AND h.run_date > '20030207'                --rundate here
	   --AND h.run_time BETWEEN 003000 AND 123000   --times here
	 ORDER BY run_time  ASC
	</cfquery>
	<cfreturn getJobList>
</cffunction>

	<!--- START 2011/05/25 NYB REL106 added getTableDetailsStruct function --->
	<cffunction name="getTableDetailsStruct" access="public" returntype="struct">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="returnColumns" type="boolean" required="false" default="false">
		<cfargument name="excludeAutoGen" type="boolean" required="false"><!--- NYB 2009-10-02 LHID2719 - removed default="false" --->
		<cfargument name="columnName" type="string" required="false">
		<cfargument name="returnPK" type="boolean" required="false"> <!--- NYB 2009-10-02 LHID2719 - added --->
		<cfargument name="excludeColumn" type="string" required="false" default="">

		<cfset var entityTableDetails = getTableDetails(argumentCollection=arguments)>
		<cfset var tableDetailsStruct = application.com.structureFunctions.queryToStruct(query=entityTableDetails,key="column_name")>

		<cfreturn tableDetailsStruct>
	</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Returns columnNames from a query
			columnName argument accept one or more entries.  multiple column names passed as "eg1,eg2" - they do not need ' put around them
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!--- WAB 2010/10/04 problem when exclude column was passed - wasn't getting quoted or something.
			Modified so that listqualifies were all in the query, rather than doing list qualify followed by preserve single quotes
	 --->
	<cffunction access="public" name="getTableDetails" hint="Returns details of this table from sysTables" output="false">
		<!--- NYB 2008-12-03 - FNL008 Archive Project:  gave dataSource a default --->
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="returnColumns" type="boolean" required="false" default="false">
		<cfargument name="excludeAutoGen" type="boolean" required="false"><!--- NYB 2009-10-02 LHID2719 - removed default="false" --->
		<!--- NYB 2009-04-22 Sophos - added argument: columnName --->
		<cfargument name="columnName" type="string" required="false">
		<cfargument name="returnPK" type="boolean" required="false"> <!--- NYB 2009-10-02 LHID2719 - added --->
		<!--- START: 2010-05-28 AJC P-FNL090 Email Scheduler --->
		<cfargument name="excludeColumn" type="string" required="false">
		<!--- END: 2010-05-28 AJC P-FNL090 Email Scheduler --->

		<cfif returnColumns or structkeyexists(arguments,"columnName")>
			<!--- using system tables here as information_schema views don't indicate AutoGen columns --->
			<!--- NJH 2009/05/27 P-SNY063 - added isNullable, hasDefault, isIdentity and isPrimaryKey
				Also cached the query as I was hitting it a lot, and the table details really aren't going to change all that much.
			--->
			<!--- START: NYB 2009-10-02 LHID2719 - added IF and contents of ELSE - as IF taking too long to return data, so only used when necessary --->
			<cfif structkeyexists(arguments,"excludeAutoGen") or structkeyexists(arguments,"returnPK")>
				<cfquery name="getTableDetailsQry" cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#">
					<!--- NYB 2009-09-28 LHID2672 - changed sc.length to sc.prec --->
					select so.name as Table_Name, sc.name as Column_Name, st.name as Data_Type,sc.prec as Column_Length,sc.isNullable,
						case when sc.cdefault > 0 then 1 else 0 end as hasDefault,
						case when columnproperty(so.ID, sc.name, 'IsIdentity') <> 1 then 0 else 1 end as isIdentity,
						case when tc.constraint_name is null then 0 else 1 end as isPrimaryKey
				from syscolumns sc
				inner join systypes st on sc.xtype=st.xtype
				inner join sysobjects so on sc.id=so.id
				left join (information_schema.table_constraints tc inner join
					information_schema.constraint_column_usage ccu on tc.constraint_name = ccu.constraint_name and constraint_Type = 'Primary Key')
					on  tc.table_name = so.name and ccu.column_name = sc.name
				where so.name  in ( <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				and st.name != 'sysname'
					<cfif structkeyexists(arguments,"excludeAutoGen") and arguments.excludeAutoGen>
						and columnproperty(so.ID, sc.name, 'IsIdentity') <> 1
					</cfif>
				<!--- START:  NYB 2009-04-22 Sophos - added: --->
					<cfif structkeyexists(arguments,"columnName") and arguments.columnName neq ""> <!--- NJH 2009/05/22 P-SNY063 - added neq "" and changed from like to in --->
					and sc.name in  (<cf_queryparam value="#arguments.columnName#" CFSQLTYPE="CF_SQL_VARCHAR" list="true"> )
					</cfif>
				<!--- END:  NYB 2009-04-22 Sophos --->
				order by so.name desc, sc.Colid asc
				</cfquery>
			<cfelse>
				<!--- this runs much faster: --->
				<cfquery name="getTableDetailsQry" cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#">
					select Table_Name,Column_Name,Data_Type,isNULL(Character_Maximum_Length,0) as Column_Length,
					case when IS_NULLABLE='No' then 0 else 1 end as IsNullable,
					case when COLUMN_DEFAULT IS NULL then 0 else 1 end as hasDefault
					 from <cfif left(arguments.tablename,1) eq "##">tempdb.</cfif>information_schema.columns
					 where table_name  in ( <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					<cfif structkeyexists(arguments,"columnName") and arguments.columnName neq ""> <!--- NJH 2009/05/22 P=-SNY063 - added neq "" and changed from like to in --->
						and Column_Name  in ( <cf_queryparam value="#arguments.columnName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					</cfif>
					<cfif structkeyexists(arguments,"excludeColumn") and arguments.excludeColumn neq "">
						and Column_Name  not in ( <cf_queryparam value="#arguments.excludeColumn#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					</cfif>
					order by ORDINAL_POSITION
				</cfquery>
			</cfif>
			<!--- END: NYB 2009-10-02 LHID2719 --->
		<cfelse>
			<cfquery name="getTableDetailsQry" cachedwithin="#CreateTimeSpan(0, 0, 5, 0)#">
				select table_name from information_schema.tables
				where table_Name =  <cf_queryparam value="#arguments.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
		</cfif>

		<cfreturn getTableDetailsQry>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Returns column size as a struct
			struct keys are the names of the columns.  columnName can contain multiple values - not necessary to surround with 's
            NYB 2009-05-18 - added after if was required in 2 independent areas of core code
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getColumnSize">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="returnColumns" type="boolean" required="false" default="false">
		<cfargument name="excludeAutoGen" type="boolean" default="false">
		<cfargument name="columnName" type="string" required="false">

		<cfset tableDetailsQry = application.com.dbTools.getTableDetails(argumentCollection=arguments)>
		<cfquery name="tableDetailsQry" dbtype="query">
			select COLUMN_NAME,COLUMN_LENGTH from tableDetailsQry
		</cfquery>
		<cfset tableDetails = structNew()>
		<cfloop index ="i" from = "1" to = "#tableDetailsQry.recordcount#">
			<cfset x = application.com.structureFunctions.queryRowToStruct(query=tableDetailsQry,row=i,columns="COLUMN_LENGTH")>
			<cfset tableDetails[tableDetailsQry.COLUMN_NAME[i]] = x.COLUMN_LENGTH>
		</cfloop>
		<cfreturn tableDetails>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             NJH 2009/01/28 Returns identity Column from a table if it exists
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getIdentityColumn" hint="Returns the identity column of this table if it exists">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="tableName" type="string" required="true">

		<cfscript>
			var getIdentityColumnQry = "";
			var identityColumn = "";
		</cfscript>

		<cfquery name="getIdentityColumnQry" datasource="#arguments.dataSource#">
			select sc.name as identity_column
			from syscolumns sc
			inner join systypes st on sc.xtype=st.xtype
			inner join sysobjects so on sc.id=so.id
			where so.name  in ( <cf_queryparam value="#arguments.tableName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				and st.name != 'sysname'
				and columnproperty(so.ID, sc.name, 'IsIdentity') = 1
			order by so.name desc, sc.Colid asc
		</cfquery>

		<cfif getIdentityColumnQry.recordCount>
			<cfset identityColumn = getIdentityColumnQry.identity_column>
		</cfif>

		<cfreturn identityColumn>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="dropColumn" hint="Deletes a column from table.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="columnName" type="string" required="true">
		<cfquery name="dropColumn" datasource="#dataSource#">
			alter table #tablename# drop column #columnName#
		</cfquery>
		<cfreturn "Column #columnName# dropped">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addColumn" hint="Adds a column to a table.">
		<cfargument name="dataSource" type="string" required="false">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="newColumnName" type="string" required="true">
		<cfargument name="columnDataType" type="string" required="true">
		<cfargument name="columnDataLength" type="string" required="false" default="50">
		<cfargument name="initialValue" type="string" required="false">
		<cfargument name="whereClause" type="string" required="false">

		<cfquery name="local.addColumn">
		if not exists (select * from information_schema.columns
				where column_name =  <cf_queryparam value="#newColumnName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and table_name =  <cf_queryparam value="#tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		BEGIN
			alter table #tablename# add [#newColumnName#]
				[#columnDataType#] <cfif columnDataType eq "varchar">(#columnDataLength#)</cfif> NULL
		END
		</cfquery>

		<cfif isDefined("initialValue") and initialValue neq "">
			<cfquery name="initialiseColumnValue">
				update #tablename#
					<cfif columnDataType eq "varchar">
						set #newColumnName# =  <cf_queryparam value="#initialValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelse>
						set #newColumnName# = #initialValue#
					</cfif>

					<cfif isDefined("whereClause") and whereClause neq "">
						where #whereClause#
					</cfif>
			</cfquery>
		</cfif>
		<cfreturn "Column #newColumnName# added">
	</cffunction>


	<!--- WAB 2007-09-26 for error handling
		NJH 2012/03/01 Modified slightly to get data from sysprocesses and get all top blocking spids,
			rather than just the first one.
	--->
	<cffunction access="public" name="getDatabaseBlocks" hint="Gets Info on Blocking Transactions">
		<cfargument name="dataSource" type="string" required="true">

		<cfset var result = structNew()>
		<cfset var allSpids = "">
		<cfset var blockedSpids = "">
		<cfset var blockingSpid = "">
		<cfset result.hasBlock = false>


		<!--- get all connections --->
		<cfquery name="allSpids" datasource="#dataSource#">
			select * from master.dbo.sysprocesses
		</cfquery>

		<cfquery name="blockedSpids" dbtype="query">
		select * from allSpids where blocked <> 0
		</cfquery>

		<!--- find only spid which is in the list of blockers but isn't blocked itself --->
		<cfif blockedSpids.recordCount is not 0>
			<cfquery name="blockingSpid" dbtype="query">
			select * from allSpids
			where blocked = 0 and spid in (#valuelist(blockedSpids.blocked)#)
			</cfquery>

			<cfif blockingSpid.recordCount is not 0>

				<cfset result.hasBlock = true>
				<cfset result.blockingSpids = structNew()>

				<cfloop query="blockingSpid">
					<cfquery name="getSpidDetail" datasource="#dataSource#">
						dbcc inputbuffer(#blockingSpid.spid#)
					</cfquery>

					<cfset result.blockingSpids[blockingSpid.spid] = structNew()>
					<cfset result.blockingSpids[blockingSpid.spid].blockingSpidDetails = getSpidDetail>
					<cfset result.blockingSpids[blockingSpid.spid].blocked = structNew()>

					<cfquery name="getSpidsBlockedBySpid" dbtype="query">
						select * from blockedSpids where blocked = #blockingSpid.spid#
					</cfquery>

					<cfloop query="getSpidsBlockedBySpid">
						<cfquery name="getSpidDetail" datasource="#dataSource#">
							dbcc inputbuffer(#spid#)
						</cfquery>
						<cfset result.blockingSpids[blockingSpid.spid].blocked[getSpidsBlockedBySpid.spid] =  getSpidDetail.eventInfo>
					</cfloop>

				</cfloop>
			</cfif>
			<!---

				<cfquery name="getSpidDetail" datasource="#dataSource#">
				dbcc inputbuffer(#blockingSpid.spid#)
				</cfquery>

				<cfset result.blockingSpid = blockingSpid.spid>
				<cfset result.blockingSpidDetails = getSpidDetail>
				<cfset result.blocked = structNew()>

				<cfloop query="blockedSpids">
					<cfquery name="getSpidDetail" datasource="#dataSource#">
					dbcc inputbuffer(#spid#)
					</cfquery>
					<cfset result.blocked[spid] =  getSpidDetail.eventInfo>
				</cfloop>

			 --->

		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- 2011-10-20 NYB LHID7283 added Preservesinglequotes to query --->
	<cffunction access="public" name="DeleteRecords" hint="Delete Records from table.">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="whereClause" type="string" default=""><!--- NJH 2008/05/15 - CR-LEN507 --->

		<cfscript>
			var DeleteTableRecords = "";
		</cfscript>

		<cfquery name="DeleteTableRecords" datasource="#application.siteDataSource#">
			DELETE FROM #arguments.tablename#
			<cfif arguments.whereClause neq "">
				where #Preservesinglequotes(arguments.whereClause)#
			</cfif>
		</cfquery>
	</cffunction>

	<cffunction access="public" name="TruncateRecords" hint="Truncate Records from table.">
		<cfargument name="tablename" type="string" required="true">

		<cfscript>
			var TruncateTableRecords = "";
		</cfscript>

		<!--- NJH 2008/10/06 CR-LEN503 SPMDataLoad --->
		<cfif tableExists(tableName=arguments.tableName)>
			<cfquery name="TruncateTableRecords" datasource="#application.siteDataSource#">
				Truncate Table #arguments.tablename#
			</cfquery>
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     SSS 2008-05-06
	 Changes one table for another so that reports are not of line while a table is getting info put into it
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	 <cffunction name ="flipflop" hint="Puts the source table live and renames the live table to the source table">
		<cfargument name="sourceTable" required="true">
		<cfargument name="liveTable" required="true">

		<cfscript>
			var switchTables = "";
		</cfscript>

		<!--- flip flop the new data into the live table --->
		<cfquery name="switchTables" datasource="#application.siteDataSource#">
			EXEC sp_rename '#liveTable#', '#liveTable#Switch'
			EXEC sp_rename '#sourceTable#', '#liveTable#'
			EXEC sp_rename '#liveTable#Switch', '#sourceTable#'
		</cfquery>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     SSS 2008-06-24
	 Checks for the existance of a table in the database
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name ="TableExists" hint="Checks to see if a user table of this name exists in the database">
 		<cfargument name="TableName" required="true">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">

 		<cfquery name="doesTableExist" datasource="#arguments.dataSource#">
			SELECT * FROM sysobjects WHERE type='u' and name =  <cf_queryparam value="#arguments.TableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfif doesTableExist.recordcount eq 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>

<!--- ***********************************************************************
	NYB 02-01-2009
	gets the URLs associated with a defined datasource from within setSiteDetails.cfm in RelayWare
  *********************************************************************** --->
	<cffunction name="getURLsFromDataSource">
		<cfargument name="datasource" required="true">
		<cfargument name="setSiteFileName" default="setSiteDetails.cfm">
		<cfset sites = "">
		<cfoutput>
			<!--- add siteSiteDetails.cfm filename var - for dev testing --->
			<cfset fileName = "#ReplaceNoCase(application.path, '\Relay', '')#\RelayWare\#arguments.setSiteFileName#">   <!--- testing - on dev --->
			<cffile action = "read" file="#fileName#" variable = "siteDetailsFile">

			<!--- Find <cfif CGI.SERVER_NAME   or  <cfswitch expression="#CGI.SERVER_NAME#" --->
			<cfset REFoundPosition = REFindNoCase("\<(cfif|cfswitch)(\ +expression[\=\ ]+\""\##|\ )CGI\.SERVER_NAME", siteDetailsFile, 0)>
			<!--- wipe everything from file before: <cfif CGI.SERVER_NAME   or  <cfswitch expression="#CGI.SERVER_NAME#" --->
			<cfset siteDetailsFile = Right(siteDetailsFile,len(siteDetailsFile)-(REFoundPosition-1))>

			<!--- Find "SiteDataSource = #arguments.datasource#"... --->
			<cfset REFoundPosition = REFindNoCase("SiteDataSource(\ +\=|\=)\ +\""#arguments.datasource#\""", siteDetailsFile, 0)>

			<CFIF REFoundPosition eq 0>
				<P>Database <b>#arguments.datasource#</b> not found in <b>#fileName#</b>.</P>
			<CFELSE>
				<!--- if datasource found, find the following "cfset"... --->
				<cfset FoundPosition = FindNoCase("<cfset", siteDetailsFile, REFoundPosition)>
				<!--- ...then delete everything after that from file --->
				<cfset siteDetailsFile = Left(siteDetailsFile,FoundPosition-1)>

				<!--- Find the first space, then set cfmType to the preceding contents should be either cfswitch (dev) or cfif (live) --->
				<cfset FoundPosition = Find(" ",siteDetailsFile)>
				<cfset cfmType = Trim(Left(siteDetailsFile,FoundPosition))>

				<!--- set the Regular Expression to search for based on cfmType --->
				<cfif cfmType eq "<cfswitch">
					<cfset searchRE = "\<cfcase">
				<cfelse> <!--- assumes cfmType="cfif" --->
					<cfset searchRE = "(\<cfelseif|\<cfif)\ +CGI\.SERVER_NAME"> <!---  \ +CGI.SERVER_NAME\ +eq\ +\""\>   --->
				</cfif>

				<!--- find the last Regular Expression match in file - being the one just before datasource - and delete everything before it--->
				<cfset a = REFindNoCase(searchRE, siteDetailsFile, 0)>
				<cfloop condition="a gt 0">
					<cfif a neq 0>
						<cfset REFoundPosition = a>
						<cfset a = REFindNoCase(searchRE, siteDetailsFile, a+1)>
					</cfif>
				</cfloop>
				<cfset siteDetailsFile = Right(siteDetailsFile,len(siteDetailsFile)-(REFoundPosition-1))>

				<!--- delete everything after the CGI.SERVER_NAME tag - after which this is all we're left with --->
				<cfset FoundPosition = Find(">", siteDetailsFile, 0)>
				<cfset siteDetailsFile = Left(siteDetailsFile,FoundPosition)>

				<!--- delete everything not in "'s and put in a list --->
				<cfset toV = 1>
				<cfset fromV = 1>
				<cfloop condition="fromV gt 0">
					<cfif fromV neq 0>
						<cfset fromV = FindNoCase("""", siteDetailsFile, toV+1)>
						<cfset a = Right(siteDetailsFile, len(siteDetailsFile)-fromV)>
						<cfset toV = FindNoCase("""", a, 0)>
						<cfif toV eq 0 or fromV eq 0>
							<cfset fromV = 0>
						<cfelse>
							<cfset a = Left(a, toV-1)>
							<cfset sites = ListAppend(sites, a)>
							<cfset toV = FindNoCase("""", siteDetailsFile, fromV+1)>
						</cfif>
					</cfif>
				</cfloop>
			</CFIF>
		</cfoutput>
		<cfreturn sites>
	</cffunction>

<!--- ***********************************************************************
	Created NYB 20-01-2009   -  for Archive project
	Deletes specified table if it exists, returns true if successful, returns false if not
  *********************************************************************** --->
	<cffunction access="public" name="DeleteTable" output="true" hint="Deletes specified table if it exists">
		<cfargument name="TableName" required="true">
		<cfargument name="datasource" default="#application.siteDataSource#">
		<cftry>
	 		<cfquery name="deleteTable" datasource="#datasource#">
		 		if exists (select 1 from information_schema.tables where TABLE_TYPE='BASE TABLE' and TABLE_NAME =  <cf_queryparam value="#TableName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					drop table #TableName#
			</cfquery>
			<cfreturn true>
		<cfcatch type="any">
			<cfreturn false>
		</cfcatch>
		</cftry>
	</cffunction>


<!--- ***********************************************************************
	Created NYB 28-01-2011 P-NET002
	Converts "#description# (#sku#)" to "description+' ('+sku+')+'"
  *********************************************************************** --->
	<cffunction access="public" name="ConvertMaskToSqlEntry" output="true" hint="Deletes specified table if it exists">
		<cfargument name="MaskString" required="true">
		<cfargument name="separator" default="##">

		<cfif Left(MaskString, 1) eq separator>
			<cfset MaskString = Right(MaskString,len(MaskString)-1)>
		<cfelse>
			<cfset MaskString = "'#Right(MaskString,len(MaskString))#">
		</cfif>
		<cfif Right(MaskString, 1) eq separator>
			<cfset MaskString = Left(MaskString,len(MaskString)-1)>
		<cfelse>
			<cfset MaskString = "#Left(MaskString,len(MaskString))#'">
		</cfif>

		<cfset start = 1>
		<cfset found = Find(separator, MaskString)>
		<cfloop condition = "found gt 0">
			<cfif start>
				<cfset MaskString = Replace(MaskString, separator, "+'",'one')>
				<cfset start = 0>
			<cfelse>
				<cfset MaskString = Replace(MaskString, separator, "'+",'one')>
				<cfset start = 1>
			</cfif>
			<cfset found = Find(separator, MaskString)>
		</cfloop>

		<cfreturn MaskString>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     NYB 2008-12-02
	 START Archive Functions
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<!--- *********************        START Archive Utility Functions        ********************* --->
	<cffunction access="private" name="createLogStruct" output="false" hint="">
 		<cfargument name="ArchiveDefID" type="numeric" required="true">
		<cftry>
			<cfset LogStruct = StructNew()>
			<cfset x = StructInsert(LogStruct,"ArchiveDefID",ArchiveDefID)>
			<cfset x = StructInsert(LogStruct,"TableArchived","")>
			<cfset x = StructInsert(LogStruct,"StartTime",getDBDateTime())>
			<cfset x = StructInsert(LogStruct,"EndTime",LogStruct.startTime)>
			<cfset x = StructInsert(LogStruct,"SelectDuration",0)>
			<cfset x = StructInsert(LogStruct,"ArchiveDuration",0)>
			<cfset x = StructInsert(LogStruct,"RowsMoved",0)>
			<cfset x = StructInsert(LogStruct,"Status","Successful")>
			<cfset x = StructInsert(LogStruct,"ErrorMsg","")>
			<cfreturn LogStruct>
			<cfcatch type="any">
				<cfset x = ErrorMsgNotification(source="createLogStruct", errorCatch=cfcatch)>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="RelationshipTable" output="false" hint="">
 		<cfargument name="PrimaryTable" type="string" required="false">
 		<cfargument name="includeDependents" type="boolean" default="true">
		<cftry>
	 		<cfquery name="RelationshipQry" datasource="#application.siteDataSource#">
				select * from (
					select 'Person' as PrimaryTable,'PersonID' as PrimaryKey,'' as ForeignTable,'' as ForeignKey union
					select 'Usergroup' as PrimaryTable,'UsergroupID' as PrimaryKey,'' as ForeignTable,'' as ForeignKey union
					select 'EntityTracking' as PrimaryTable,'EntityTrackingID' as PrimaryKey,'EntityTracking' as ForeignTable,'' as ForeignKey union
					select 'DedupeAutomationLog' as PrimaryTable,'DedupeAutomationLogID' as PrimaryKey,'DedupeAutomationLog' as ForeignTable,'' as ForeignKey union
					select 'Opportunity' as PrimaryTable,'OpportunityID' as PrimaryKey,'Opportunity' as ForeignTable,'' as ForeignKey union
					select 'OppProducts' as PrimaryTable,'OppProductID' as PrimaryKey,'Opportunity' as ForeignTable,'opportunityID' as ForeignKey union
					select 'CommDetail' as PrimaryTable,'CommDetailID' as PrimaryKey,'CommDetail' as ForeignTable,'' as ForeignKey union
					select 'CommDetailHistory' as PrimaryTable,'CommDetailHistoryID' as PrimaryKey,'CommDetail' as ForeignTable,'CommDetailID' as ForeignKey union
					select 'CommDetailHistory' as PrimaryTable,'CommDetailHistoryID' as PrimaryKey,'CommDetailHistory' as ForeignTable,'' as ForeignKey union
					select 'TempSelectionTag' as PrimaryTable,'SelectionID,EntityID' as PrimaryKey,'TempSelectionTag' as ForeignTable,'' as ForeignKey union
					select 'TempSelectionCriteria' as PrimaryTable,'SelectionID,SelectionField' as PrimaryKey,'TempSelectionCriteria' as ForeignTable,'' as ForeignKey union
					select 'Selection' as PrimaryTable,'SelectionID' as PrimaryKey,'' as ForeignTable,'' as ForeignKey union
					select 'SelectionCriteria' as PrimaryTable,'SelectionID,SelectionField' as PrimaryKey,'Selection' as ForeignTable,'SelectionID' as ForeignKey union
					select 'SelectionGroup' as PrimaryTable,'SelectionID,UsergroupID' as PrimaryKey,'Selection' as ForeignTable,'SelectionID' as ForeignKey union
					select 'SelectionTag' as PrimaryTable,'SelectionID,EntityID' as PrimaryKey,'Selection' as ForeignTable,'SelectionID' as ForeignKey union
					select 'CommSelection' as PrimaryTable,'CommID,SelectionID' as PrimaryKey,'Selection' as ForeignTable,'SelectionID' as ForeignKey
				) as d
				where 1=1
				<cfif structkeyexists(arguments,"PrimaryTable")>
					and (PrimaryTable =  <cf_queryparam value="#PrimaryTable#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfif includeDependents>
						or ForeignTable =  <cf_queryparam value="#PrimaryTable#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					)
				</cfif>
			</cfquery>
			<cfreturn RelationshipQry>
			<cfcatch type="any">
				<cfset x = ErrorMsgNotification(source="RelationshipTable", errorCatch=cfcatch)>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="updateArchiveStatus" output="false" hint="">
 		<cfargument name="ArchiveDefID" type="string" required="true">
 		<cfargument name="Status" type="string" required="true">
		<cftry>
	 		<cfquery name="qryUpdateStatus" datasource="#application.siteDataSource#">
		 		update ArchiveDef set Status =  <cf_queryparam value="#Status#" CFSQLTYPE="CF_SQL_INTEGER" >  where ArchiveDefID =  <cf_queryparam value="#ArchiveDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfcatch type="any">
				<cfset x = ErrorMsgNotification(source="getDBDateTime", errorCatch=cfcatch)>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="getDBDateTime" output="false" hint="">
		<cftry>
	 		<cfquery name="qryDBDateTime" datasource="#application.siteDataSource#">
		 		select getdate() as DBDateTime
			</cfquery>
			<cfreturn qryDBDateTime.DBDateTime>
			<cfcatch type="any">
				<cfset x = ErrorMsgNotification(source="getDBDateTime", errorCatch=cfcatch)>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="createLogEntry" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
		<cftry>
			<cfset cols = StructKeyList(DataStruct,",")>
			<cfset strVals = "">
			<cfoutput>
				<cfloop index="col" list="#cols#">
					<cfset strVals = listAppend(strVals,iif(isNumeric(evaluate("DataStruct.#col#")),evaluate("DataStruct.#col#"),de("'" & evaluate("DataStruct.#col#") & "'")))>
				</cfloop>
		 		<cfquery name="qryCreateLogEntry" datasource="#application.siteDataSource#">
					insert into ArchiveLog (#PreserveSingleQuotes(cols)#) values (#PreserveSingleQuotes(strVals)#)
				</cfquery>
			</cfoutput>
			<cfcatch type="any">
				<cfset x = ErrorMsgNotification(source="createLogEntry", errorCatch=cfcatch)>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="ErrorMsgNotification" output="true" hint="">
 		<cfargument name="errorCatch" required="true">
 		<cfargument name="source" type="string" default="Not Specified">
		<cfoutput>
			<p>Error #htmleditformat(errorCatch.ErrorCode)#:  Occured in #htmleditformat(source)# on line #errorCatch.TagContext[1].line#.<br/>
			#htmleditformat(errorCatch.Message)#
			<cfif isDefined("errorCatch.Sql")>
				#htmleditformat(errorCatch.cause.Message)#</p>
				<table border="0">
					<tr>
						<td></td>
						<td style="padding-left: 20px;">#htmleditformat(errorCatch.Sql)#</td>
					</tr>
				</table>
				<p>
			</cfif>
			</p>
			<cfmail
				to = "errors@foundation-network.com"
				from = "do-not-reply@foundation-network.com"
				bcc = "relayhelpcc@foundation-network.com"
				subject = "Error In Scheduled process: ArchiveProcess "
				type = "html" >
				<p>Error #errorCatch.ErrorCode#:  Occured in #source# on line #errorCatch.TagContext[1].line#.<br/>
				#errorCatch.Message#
				<cfif isDefined("errorCatch.Sql")>
					#errorCatch.cause.Message#</p>
					<table border="0">
						<tr>
							<td></td>
							<td style="padding-left: 20px;">#errorCatch.Sql#</td>
						</tr>
					</table>
					<p>
				</cfif>
				</p>
			</cfmail>
		</cfoutput>
	</cffunction>

<!--- *********************        END Archive Utility Functions        ********************* --->

	<cffunction access="public" name="StandardArchiveFunction" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
 		<cfargument name="verbose" type="boolean" default="false">

		<cfscript>
			var identityColumn = ""; // NJH 2009/01/28 data archive testing
			var LogStruct = "";
			var rowCount = 0;
		</cfscript>

		<cfset RunTime = RunTime * 1000> <!--- as milliseconds--->

		<cftry>
			<cfset runStart = GetTickCount()>
			<!--- START - get path of Active datasource --->
			<cfquery name="getArchiveDB" datasource="#application.sitedatasource#Archive">
				select top 1 TABLE_CATALOG + '.' + TABLE_SCHEMA + '.' AS dbPath from information_schema.tables
			</cfquery>
			<cfset ArchiveDbPath = getArchiveDB.dbPath>
			<!--- END - get path of Active datasource --->

			<!--- START - gets table info, ie, relationships, all dependents of this table, to archive them also - and in the correct order) --->
			<cfset getRelationships = RelationshipTable(PrimaryTable=DataStruct.PrimaryTable)>

			<cfif getRelationships.recordcount eq 0>
				<!--- if table name isn't in getRelationships then just archive entries for this 1 table
					  create dummy table to use same processing loop as for cfelse/getRelationships.recordcount_gt_0
				--->
				<cfquery name="getTablesOrdered" datasource="#application.sitedatasource#">
					select 1 as archiveOrder, '#DataStruct.PrimaryTable#' as PrimaryTable,'#DataStruct.PriKeyCol#' as PrimaryKey,'' as ForeignTable,'' as ForeignKey
				</cfquery>
			<cfelse>
				<!--- puts table relationships in the most appropriates 'delete' order - depending on entries in the RelationshipTable(query) --->
				<cfset archiveOrder = 1>
				<!--- get main/primary table --->
				<cfquery name="getTablesOrdered" dbtype="query">
					select *, #archiveOrder# as archiveOrder from getRelationships where ForeignTable='' or ForeignTable=' ' or ForeignTable=PrimaryTable
				</cfquery>

				<cfset getTables = getTablesOrdered>

				<!--- get number of dependents --->
				<cfset cntRmng = getRelationships.recordcount - getTablesOrdered.recordcount>
				<cfloop condition="cntRmng gt 0">
				<!--- ie, loop thru dependents and place in correct (archive) order --->
					<cfset archiveOrder = archiveOrder + 1>

					<cfquery name="getTables" dbtype="query">
						select *, #archiveOrder# as archiveOrder from getRelationships where UPPER(ForeignTable)=UPPER('#getTables.PrimaryTable#') and UPPER(ForeignKey)=UPPER('#getTables.PrimaryKey#')
					</cfquery>

					<cfquery name="getTablesOrdered" dbtype="query">
						select * from getTablesOrdered
						union
						select * from getTables
						order by archiveOrder desc
					</cfquery>
					<cfset cntRmng = iif(getTables.recordcount gt 0,cntRmng - getTables.recordcount,0)>
				</cfloop>
			</cfif>
			<!--- END - gets table info --->

			<cfset x = updateArchiveStatus(ArchiveDefID=DataStruct.ArchiveDefID, Status="Running")>

			<!--- START - force timeout --->
			<cfset runEnd = GetTickCount()>
			<cfif (runEnd - runStart) gt RunTime>
				<cfset rowcount = 0>
			<cfelse>
				<cfset rowcount = 1>
			</cfif>
			<!--- END - force timeout --->

		<!--- START - main archive processing area --->
			<!--- NYB 2009-01-20 - swapped <cfloop query="getTablesOrdered"> with <cfloop condition="rowcount gt 0"> --->
			<cfloop query="getTablesOrdered">
				<cfloop condition="rowcount gt 0">
					<!--- NJH 2009/01/29 re-initialise the structure for every iteration --->
					<cfset LogStruct = createLogStruct(ArchiveDefID=DataStruct.ArchiveDefID)>

					<cfset LogStruct.TableArchived = PrimaryTable>
					<cfset tickStart = GetTickCount()>  <!--- start ArchiveLog.SelectDuration count --->

					<!--- remove temp table - used for FlipFlop process --->
					<cfset x = application.com.dbTools.DeleteTable(TableName="#PrimaryTable#_Temp")>

			 		<cfquery name="removeTempTable" datasource="#application.siteDataSource#">
				 		if exists (select 1 from information_schema.tables where TABLE_TYPE='BASE TABLE' and TABLE_NAME =  <cf_queryparam value="#PrimaryTable#_Temp" CFSQLTYPE="CF_SQL_VARCHAR" > )
							drop table #PrimaryTable#_Temp
					</cfquery>

					<cfset TableColumnsQry = getTableDetails(tableName=PrimaryTable, returnColumns=true, excludeAutoGen=false)>
					<cfset TableColumns = ValueList(TableColumnsQry.Column_Name,",")>

			 		<!--- START - create a join statement to add to delete dependents *
									only run for multi-table archive - outlined RelationshipTable --->
			 		<cfif archiveOrder gt 1>
						<cfquery name="createJoinStmt" dbtype="query" maxrows="1">
							select 'inner join ' + PrimaryTable + ' on ' + PrimaryTable + '.' + PrimaryKey +  '=#PrimaryTable#.#ForeignKey#' as JoinStatement, * from getTablesOrdered where PrimaryTable='#ForeignTable#'
						</cfquery>
				 		<cfset cntRmng = getTablesOrdered.recordcount - createJoinStmt.recordcount>
						<cfset createJoinStmtTemp = createJoinStmt>
						<cfloop condition="cntRmng gt 1 and createJoinStmt.archiveOrder gt 1">
							<cfquery name="createJoinStmtTemp" dbtype="query" maxrows="1">
								select 'inner join ' + PrimaryTable + ' on ' + PrimaryTable + '.' + PrimaryKey +  '=#createJoinStmtTemp.ForeignTable#.#createJoinStmtTemp.ForeignKey#' as JoinStatement, * from getTablesOrdered where PrimaryTable='#createJoinStmtTemp.ForeignTable#'
							</cfquery>
							<cfquery name="createJoinStmt" dbtype="query">
								select * from createJoinStmtTemp
								union
								select * from createJoinStmt
							</cfquery>
							<cfset cntRmng = iif(createJoinStmtTemp.archiveOrder gt 2,2,1)>
						</cfloop>
					</cfif>
					<!--- END - create a join statement --->
					<!--- start - Added for ExpiredUsersSelections --->
					<cfif structKeyExists(DataStruct,"DateColIn") and structKeyExists(DataStruct,"ViaTables") and structKeyExists(DataStruct,"ThroughCols")>
						<cfset paramTableStruct = StructNew()>
						<cfloop index="i" list="#DataStruct.ViaTables#">
							<cfset x = RelationshipTable(PrimaryTable=i,includeDependents=false)>
							<cfset y = StructInsert(paramTableStruct,i,x.PrimaryKey)>
						</cfloop>
						<cfset x = RelationshipTable(PrimaryTable=DataStruct.DateColIn,includeDependents=false)>
						<cfset y = StructInsert(paramTableStruct,DataStruct.DateColIn,x.PrimaryKey)>
					</cfif>
					<!--- end - Added for ExpiredUsersSelections --->

					<!--- START - copy entries from Primary Table to #PrimaryTable#_Temp - on Live datasource --->
					<cfquery name="copyEntries" datasource="#application.siteDataSource#">
						SELECT
							 top #DataStruct.MovePerRun#
						<cfloop index="i" from="1" to="#ListLen(TableColumns)#">#PrimaryTable#.#ListGetAt(TableColumns,i)#<cfif i lt ListLen(TableColumns)>,</cfif></cfloop>
						INTO #PrimaryTable#_Temp
				 		from #PrimaryTable#
				 		<cfif archiveOrder gt 1>  <!--- * add join statement --->
							#createJoinStmt.JoinStatement#
						</cfif>
						<!--- start - Added for ExpiredUsersSelections --->
						<cfif (structKeyExists(DataStruct,"DateColIn") and structKeyExists(DataStruct,"ViaTables") and structKeyExists(DataStruct,"ThroughCols")) or (structKeyExists(DataStruct,"WhereException"))>
							<cfif structKeyExists(DataStruct,"WhereException")>
								#DataStruct.WhereException#
							<cfelse>
								inner join #ListGetAt(StructKeyList(paramTableStruct),1)#
								on #ListGetAt(StructKeyList(paramTableStruct),1)#.#evaluate("paramTableStruct.#ListGetAt(StructKeyList(paramTableStruct),1)#")#=#DataStruct.PrimaryTable#.#ListGetAt(DataStruct.ThroughCols,1)#
								<cfloop index="i" from="2" to="#ListLen(StructKeyList(paramTableStruct))#" step="1">
									inner join #ListGetAt(StructKeyList(paramTableStruct),i)#
									on #ListGetAt(StructKeyList(paramTableStruct),i-1)#.#ListGetAt(DataStruct.ThroughCols,i)#=#ListGetAt(StructKeyList(paramTableStruct),i)#.#evaluate("paramTableStruct.#ListGetAt(StructKeyList(paramTableStruct),i)#")#
								</cfloop>
						 		where #DataStruct.DateColIn#.#DataStruct.DateCol# < #CreateODBCDateTime(DateAdd(DataStruct.AgePart, DataStruct.AgeValue * -1, request.requestTime))#
					 		</cfif>
						<cfelse>
						<!--- end - Added for ExpiredUsersSelections --->
					 		where #DataStruct.PrimaryTable#.#DataStruct.DateCol# < #CreateODBCDateTime(DateAdd(DataStruct.AgePart, DataStruct.AgeValue * -1, request.requestTime))#
						<!--- start - Added for ExpiredUsersSelections --->
						</cfif>
						<cfif structKeyExists(DataStruct,"WhereAddOn") and (not structKeyExists(DataStruct,"WhereException"))>
							#DataStruct.WhereAddOn#
						</cfif>
						<!--- end - Added for ExpiredUsersSelections --->
				 		order by
				 		<cfloop index="i" from="1" to="#ListLen(PrimaryKey)#">#PrimaryTable#.#ListGetAt(PrimaryKey,i)#<cfif i lt ListLen(PrimaryKey)>,</cfif></cfloop>
					</cfquery>
					<!--- END - copy entries from Primary Table to #PrimaryTable#_Temp --->

					<cfset tickEnd = GetTickCount()> <!--- end ArchiveLog.SelectDuration count --->
					<cfset LogStruct.SelectDuration = (tickEnd - tickStart)>  <!--- add to struct - removed LogStruct.SelectDuration + - as now logs after each run --->

					<!--- START - IF more then 1 entry was selected then run archive process --->
					<cfquery name="getEntries" datasource="#application.siteDataSource#">
						SELECT count(*) as RecC FROM #PrimaryTable#_Temp
					</cfquery>

					<!--- START - force timeout ---
					<cfset runEnd = GetTickCount()>
					<cfif (runEnd - runStart) gt RunTime>
						<cfbreak>
					</cfif>
					!--- END - force timeout --->

					<cfset rowcount = iif(getEntries.recordcount gt 0, getEntries.RecC, 0)>
					<cfif rowcount gt 0>

						<cfset identityColumn = getIdentityColumn(dataSource="#application.sitedatasource#Archive",tablename=PrimaryTable)>

						<cfset tickStart = GetTickCount()>  <!--- start ArchiveLog.ArchiveDuration count --->
						<cftransaction>
							<cftry>
								<!--- START - move entries from #PrimaryTable#_Temp (on Live) to #PrimaryTable# on Archive --->
						 		<cfquery name="addEntriesToArchive" datasource="#application.sitedatasource#">
							 		if exists (select 1 from #ArchiveDbPath#sysobjects where name =  <cf_queryparam value="#PrimaryTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  and xtype='U')
										begin
											<cfif identityColumn neq ""> <!--- NJH 2009/01/28 added --->
							 					SET IDENTITY_INSERT #ArchiveDbPath##PrimaryTable#  ON;
							 				</cfif>
											INSERT INTO #ArchiveDbPath##PrimaryTable# (#TableColumns#)
											SELECT #TableColumns#
											FROM #PrimaryTable#_Temp;
											<cfif identityColumn neq "">
								 				SET IDENTITY_INSERT #ArchiveDbPath##PrimaryTable#  OFF;
								 			</cfif>
										end
									else
							 			begin
								 			SELECT #TableColumns#
								 			INTO #ArchiveDbPath##PrimaryTable#
								 			FROM #PrimaryTable#_Temp;
											ALTER TABLE #ArchiveDbPath##PrimaryTable# ADD Archived datetime NOT NULL DEFAULT getdate();
								 		end
								</cfquery>

								<cfcatch type="any">
									<cfdump var="#cfcatch#">
							 		<cfquery name="addEntriesToArchive" datasource="#application.sitedatasource#">
								 		if exists (select 1 from #ArchiveDbPath#sysobjects where name =  <cf_queryparam value="#PrimaryTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  and xtype='U')
											begin
												INSERT INTO #ArchiveDbPath##PrimaryTable# (#TableColumns#)
												SELECT #TableColumns#
												FROM #PrimaryTable#_Temp;
											end
										else
								 			begin
									 			SELECT #TableColumns#
									 			INTO #ArchiveDbPath##PrimaryTable#
									 			FROM #PrimaryTable#_Temp;
												ALTER TABLE #ArchiveDbPath##PrimaryTable# ADD Archived datetime NOT NULL DEFAULT getdate();
									 		end
									</cfquery>
								</cfcatch>
								<!--- END - move entries from #PrimaryTable#_Temp (on Live) to #PrimaryTable# on Archive --->
							</cftry>

							<!--- delete archived entries from main table --->
					 		<cfquery name="RemoveArchiveEntries" datasource="#application.siteDataSource#">
								delete #PrimaryTable#
								from #PrimaryTable#_Temp ptt
								inner join #PrimaryTable# pt
								on pt.#ListGetAt(PrimaryKey,1)#=ptt.#ListGetAt(PrimaryKey,1)#
						 		<cfloop index="i" from="2" to="#listlen(PrimaryKey,",")#" step="1">
									 and pt.#ListGetAt(PrimaryKey,i)#=ptt.#ListGetAt(PrimaryKey,i)#
								</cfloop>
							</cfquery>

							<!--- delete temp table  --->
							<cfset x = application.com.dbTools.DeleteTable(TableName="#PrimaryTable#_Temp")>
						</cftransaction>
						<cfset tickEnd = GetTickCount()>  <!--- end ArchiveLog.ArchiveDuration count --->
						<cfset LogStruct.ArchiveDuration = (tickEnd - tickStart)> <!--- add to struct - removed: LogStruct.ArchiveDuration +   as now logs after each run --->
						<cfset LogStruct.RowsMoved = rowcount> <!--- removed: LogStruct.RowsMoved +  --->

					</cfif>  <!--- rowcount gt 0 --->
					<!--- END - IF more then 1 entry was selected then run archive process --->

					<!--- NJH 2009/01/28 -data archiving testing - moved to before the cfbreak; it was after the cfbreak so the last iteration didn't get logged. --->
					<!--- START 15 - moved from outside loop - now logs after every iteration--->
					<cfset LogStruct.EndTime = getDBDateTime()>
					<cfset x = createLogEntry(DataStruct=LogStruct)>
					<!--- END 15 --->

					<!--- START - force timeout --->
					<cfset runEnd = GetTickCount()>
					<cfif (runEnd - runStart) gt RunTime>
						<cfbreak>
					</cfif>
					<!--- END - force timeout --->

					<cfset sleep(3000)>  <!--- 3 secs --->

				</cfloop> <!--- condition="rowcount gt 0" --->

				<!--- START - force timeout --->
				<cfset x = application.com.dbTools.DeleteTable(TableName="#PrimaryTable#_Temp")>
				<cfif (runEnd - runStart) gt RunTime>
					<cfbreak>
				<cfelse>
					<cfset rowcount = 1>
				</cfif>
				<!--- END - force timeout --->

			</cfloop> <!--- query="getTablesOrdered"  --->
		<!--- END - main archive processing area --->
			<cfset x = updateArchiveStatus(ArchiveDefID=DataStruct.ArchiveDefID, Status="Ready")>
			<cfreturn LogStruct.Status>

			<cfcatch type="any">
				<cfdump var="#cfcatch#">
				<cftry>
					<cfset x = ErrorMsgNotification(source="StandardArchiveFunction for #DataStruct.CFFunction#", errorCatch=cfcatch)>
					<cfset LogStruct.Status = "Failed">
					<cfset x = updateArchiveStatus(ArchiveDefID=DataStruct.ArchiveDefID, Status="Ready")>
					<cfset LogStruct.EndTime = getDBDateTime()>
					<cfset LogStruct.ErrorMsg = "Error #cfcatch.ErrorCode#:  #cfcatch.Message#">
					<cfif isDefined("tickEnd")>
						<cfset x = createLogEntry(DataStruct=LogStruct)>
					</cfif>
					<cfreturn LogStruct.Status>
					<cfcatch type="any">
						<cfdump var="#cfcatch#">
						<cfset x = ErrorMsgNotification(source="StandardArchiveFunction for #DataStruct.CFFunction#", errorCatch=cfcatch)>
						<cfreturn LogStruct.Status>
						<CF_ABORT>
					</cfcatch>
				</cftry>
			</cfcatch>
		</cftry>
	</cffunction>

<!--- +++++++++++++++++++++        START Named Functions        +++++++++++++++++++++ --->
	<cffunction access="public" name="ArchiveModRegister" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveEntityTracking" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveDedupeAutomationLog" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveCommDetailHistory" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveCommDetail" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveOpportunity" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveTempSelectionEntries" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset DataStruct.PrimaryTable = "TempSelectionTag">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfset DataStruct.PrimaryTable = "TempSelectionCriteria">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<cfreturn runStatus>
	</cffunction>

	<cffunction access="public" name="ArchiveSelectionsOfExpiredUsers" output="true" hint="">
 		<cfargument name="DataStruct" type="struct" required="true">
 		<cfargument name="RunTime" type="numeric" default="3">
		<cfset runStatus = StandardArchiveFunction(DataStruct=DataStruct,RunTime=RunTime)>
		<!---   cfset runStatus = "Failed - CF Function still under construction"   --->
		<cfreturn runStatus>
	</cffunction>
<!--- +++++++++++++++++++++        END Named Functions        +++++++++++++++++++++ --->

	<cffunction name="dbCacheQuery" access="public" output="false" returnType="struct">
		<cfargument name="pageSize" type="numeric" default="300">
		<cfargument name="tempTable" type="string" default="####cachedQuery#replace(CreateUUID(),'-','','ALL')#">
		<cfargument name="queryString" type="string" required="true">

		<cfset var result = {isOk=true}>
		<cfset var qryString = arguments.queryString>
		<cfset var orderByClause = "">
		<cfset var orderByPosition = 0>
		<cfset var queryResult = "">

		<cfset var findAllBrackets = application.com.regexp.getMatchedOpeningAndClosingTags (string = qryString, openingRegExp="\(", closingRegExp="\)")> <!--- WAB 2013-01-07 changed name of this function (part of 432644)--->
		<cfset var findFrom = application.com.regExp.refindAllOccurrences(reg_expression = "\sfrom\s", string = qryString, excludeArray= findAllBrackets)>

		<cfset qryString = left(qryString,findFrom[1].position) & " into #arguments.tempTable# from " & right(qryString,len(qryString)-findFrom[1].end)>

		<cfset orderByPosition = findNoCase("order by",qryString)>
		<cfif orderByPosition>
			<cfset orderByClause = right(qryString,len(qryString)-orderByPosition+6)>
		<cfelse>
			<cfthrow message="The query needs to be ordered">
		</cfif>
		<cfset qryString= reReplaceNoCase(qryString,"select\s","select Row_Number() over (#orderByClause#) as RowIndex, ")>

		<cfset orderByPosition = findNoCase("order by",arguments.queryString)>
		<cfif orderByPosition eq 0>
			<cfset orderByPosition = len(arguments.queryString)>
		</cfif>

		<cftry>
			<cfquery name="getRecords" datasource="#application.siteDataSource#" result="queryResult">
				#preserveSingleQuotes(qryString)#

				CREATE UNIQUE CLUSTERED INDEX idx_rowIndex ON #arguments.tempTable#(rowIndex);
			</cfquery>

			<cfquery name="setCachedQueryMetaData" datasource="#application.siteDataSource#">
				insert into cachedQuery(tablename,queryString,sessionID,pageSize,recordCount,lastPageAccessed,created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson)
				values (
					<cf_queryparam value="#arguments.tempTable#" CFSQLTYPE="CF_SQL_VARCHAR">,
					<cf_queryparam value="#left(arguments.queryString,orderByPosition)#" CFSQLTYPE="CF_SQL_VARCHAR">,
					<!--- sessionID is stored in currentUser structure for the api user--->
					<cfif request.relaycurrentUser.isApiUser>
						<cf_queryparam value="#request.relayCurrentUser.sessionID#" CFSQLTYPE="CF_SQL_VARCHAR">,
					<cfelse>
						<cf_queryparam value="#session.sessionID#" CFSQLTYPE="CF_SQL_VARCHAR">,
					</cfif>
					<cf_queryparam value="#arguments.pageSize#" CFSQLTYPE="CF_SQL_INTEGER">,
					<cf_queryparam value="#queryResult.recordCount#" CFSQLTYPE="CF_SQL_INTEGER">,
					0,
					getDate(),
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER">,
					getDate(),
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER">,
					<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER">
				)
			</cfquery>

			<cfset result = getRecordsFromCachedQuery(tempTable=arguments.tempTable)>

			<cfif queryResult.recordCount lte arguments.pageSize>
				<cfquery name="dropTempTable" datasource="#application.siteDataSource#">
					drop table #arguments.tempTable#
				</cfquery>
			<cfelse>
				<cfset result.tablename = arguments.tempTable>
			</cfif>

			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="dbTools Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				<cfset result.sqlMessage = replaceNoCase(cfcatch.detail,"[Macromedia][SQLServer JDBC Driver][SQLServer]","")>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2012/03/28 function to collect records from the cached query --->
	<cffunction name="getRecordsFromCachedQuery" access="public" returnType="struct">
		<cfargument name="pageNumber" type="numeric" required="false">
		<cfargument name="tempTable" type="string" required="true">

		<cfset var getRecords = "">
		<cfset var result = {hasMore=false,errorCode="",isOK=true,recordSet=queryNew("")}>
		<cfset var startIndex = 0>
		<cfset var startRow = 0>
		<cfset var endRow = 0>
		<cfset var columnList = getReturnableColumnNamesForCachedQuery(tempTable=arguments.tempTable)>
		<cfset var pageSize = 0>
		<cfset var getPageSize = "">
		<cfset var setLastAccessed = "">
		<cfset var pageNum = 0>
		<cfset var checkIfTempTableExists = "">
		<cfset var getTotalRecordCount = "">

		<cfquery name="checkIfTempTableExists" datasource="#application.siteDatasource#">
			select isNull(object_ID('tempdb..#arguments.tempTable#'),0) as tableExists
		</cfquery>

		<cfif checkIfTempTableExists.tableExists>

			<cfquery name="getPageSize" datasource="#application.siteDataSource#">
				select pageSize,lastPageAccessed from cachedQuery where tablename = <cf_queryparam value="#arguments.tempTable#" CFSQLTYPE="CF_SQL_VARCHAR">
			</cfquery>

			<cfset pageSize = getPageSize.pageSize>

			<cfif structKeyExists(arguments,"pageNumber")>
				<cfset pageNum = arguments.pageNumber>
			<cfelse>
				<cfset pageNum = getPageSize.lastPageAccessed+1>
			</cfif>

			<cfset startIndex = (pageNum-1)*pageSize>
			<cfset startRow = startIndex+1>
			<cfset endRow = startIndex+pageSize>

			<cfquery name="getRecords" datasource="#application.siteDatasource#">
				select #columnList#
				from #arguments.tempTable#
				where RowIndex >= <cf_queryparam value="#startRow#" cfsqltype="CF_SQL_INTEGER">
					and RowIndex <= <cf_queryparam value="#endRow#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>

			<cfquery name="setLastAccessed" datasource="#application.siteDataSource#">
				update cachedQuery
					set lastAccessed = getDate(),
						lastUpdated = getDate(),
						lastPageAccessed = <cf_queryparam value="#pageNum#" CFSQLTYPE="CF_SQL_INTEGER">,
						lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER">,
						lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER">
				where tablename = <cf_queryparam value="#arguments.tempTable#" CFSQLTYPE="CF_SQL_VARCHAR">
			</cfquery>

			<cfquery name="getTotalRecordCount" datasource="#application.siteDataSource#">
				select recordCount as numRecords from cachedQuery where tablename = <cf_queryparam value="#arguments.tempTable#" CFSQLTYPE="CF_SQL_VARCHAR">
			</cfquery>

			<cfset result.recordCount = getRecords.recordCount>
			<cfset result.totalRecordCount = getTotalRecordCount.numRecords>
			<cfset result.recordSet = getRecords>

			<cfquery name="getRecords" datasource="#application.siteDatasource#">
				select count(1) as recordsLeft from #arguments.tempTable#
				where RowIndex > <cf_queryparam value="#endRow#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>

			<cfif getRecords.recordsLeft>
				<cfset result.hasMore = true>
				<cfset result.tablename=arguments.tempTable>
			</cfif>

		<cfelse>
			<cfset result.isOk = false>
			<cfset result.errorCode = "NONEXISTENT_CACHED_QUERY">
			<cfset application.com.errorHandler.recordRelayError_Warning(type="dbTools Error",Severity="Coldfusion",WarningStructure=arguments)>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2013/01/15 2013Roadmap - function to delete cached query --->
	<cffunction name="deleteCachedQuery" access="public" hint="Deletes a cached query">
		<cfargument name="tempTable" type="string" required="true">

		<cfquery name="dropTempTable" dataSource="#application.siteDataSource#">
			if object_ID('tempdb..#arguments.tempTable#') is not null
			drop table #arguments.tempTable#

			delete from cachedQuery where tablename=<cf_queryparam value="#arguments.tempTable#" cfsqltype="cf_sql_varchar">
		</cfquery>
	</cffunction>

	<!--- NJH 2012/03/28 function to remove the rowIndex column from the result set--->
	<cffunction name="getReturnableColumnNamesForCachedQuery" returntype="string">
		<cfargument name="tempTable" type="string" required="true">

		<cfset var getColumns = "">

		<cfquery name="getColumns" datasource="#application.siteDataSource#">
			select name from tempdb.sys.columns
			where object_id = object_id('tempdb..#arguments.tempTable#')
				and name != 'rowIndex'
		</cfquery>

		<cfreturn valueList(getColumns.name)>
	</cffunction>


	<cffunction name="validateWhereClause" access="public" returnType="struct">
		<cfargument name="whereClause" type="string" required="true">

		<cfreturn application.com.security.checkStringForInjection(string=arguments.whereClause,type="dangerousSQL,sqlSelect")>
	</cffunction>

	<!--- function that returns the columns in a query in the correct order and case. used instead of columnList --->
	<cffunction name="getColumnListInCorrectOrder" returnType="string" output="false">
		<cfargument name="queryObject" type="query" required="true">

		<cfset var metaData = getMetaData(arguments.queryObject)>
		<cfset var columnAttributes = "">
		<cfset var columnList = "">

		<cfloop array="#metaData#" index="columnAttributes">
			<cfset columnList = listAppend(columnList,columnAttributes.name)>
		</cfloop>

		<cfreturn columnList>
	</cffunction>

</cfcomponent>

