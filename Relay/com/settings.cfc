<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>

	<cfset this.passwordString = "***Password***">
<!---
WAB 2010/06

Mods subsequent to 2010/10
WAB 2010-11-01	separate out function for evaluating strings
				some varing
				support for applicationScope
				support for settings where values are appended to the default
				support for nodes being active by moduleid or by display Attribute
	2010/11/11	dump of settings can be done on a per section basis
WAB/NJH	2011/02  Lots of changes to allow variants
				Also reworked and factorised some of the edit displaying code
				TODO More on Variants - only usergroup and person really finished
				TODO More on variant layout
				TODO Settings which must be setup separately for different values of #application.testsite#
				TODO The listing screens are not variant aware
				TODO Control rights to editing
				TODO More efficient updates by using _orig
2011/07/11 	NYB		P-LEN024 - changed isNodeActive to also accept 'active' attribute as the identifer as
					to whether a module should be displayed - as 'display' was causing entries
					that use validvalues to error because it was ending up as an attribute value passed to
					relaySelectField.cfm, which then turned the value into attributes.displayValueColumn
					it then passed to the displayValidValues code.
2011/11/03  WAB Order Settings Alpabetically by Label
2011/11/08	WAB	Resolve default values if a validValues query specified (fixed a problem)
2011/11/30	WAB removed bulk of createStandardRelayFormElementAttributeStructure to relayForms.getRelayFormAttributesFromNode()
2011/12/05 WAB use regExp.camelCaseToSpaces() for labels
			WAB changes to all functions being called by xmlFunctions.runFunctionAgainstNodes so that make use of the mode=star/end attribute
2012/02		WAB/NJH	Changes to allow for support of encryption of any passwords using the system master key
2012/04/27	WAB Fix some outstanding issues with password encryption - particularly when using dynamic keys
				Improvements to dynamic keys.  Note that the validValues for a dynamic key are now specified using attribute "dynamicKeyValidValues"
2012-09-18	WAB	Added some locking when loading settings into memory
2012-09-25	WAB Attempt to deal with settings randomly falling out of memory.  Added code to deal with settings which aren't in memory when we look for them
2012-10-03 	WAB Some changes in preparation for CASE 431028 (storing of rotating encryption key in settings).
				Allow free entry of dynamic keys (rather than by valid value) (not particularly important).
				If decryption of a password setting fails I now catch the error and pass the the database value through.
2012-12-05	WAB	CASE 432508	getVariableNodeArray_Setting() and getVariableNodeArray_Master() could bring back wrong node setting name is a substring of another setting (Eg 'Products' and 'SalesForce.Products')
2012-12-13	WAB	Settings with Personal variants not working correctly - alter getMainSettingValueFromDataBase() function
2013/02/26	NJH 	Provide support for uploading files for settings. Really only done for image files (with a fixed height and width of 80px.)
2013-04-24	WAB	Added code to validate XML in memory (looks for one of the symptoms of corruption)
				Will also verify the current setting in memory against db/default
				This required pulling out some repeated code into a new function getMainSettingValueFromDataBaseAndDefault()
2013-04-25	WAB	Discovered a bug when editing items with dynamic keys - multiple items appear on page.  Fix problem I introduced to getSettingKeysFromDataBase()
2013-04-24 	WAB CASE 434803 insertUpdateDeleteSetting(), fix problems with nvarchar(max) and cfqueryparam
2013-06-17	WAB	Add exclusive CFLOCK whenever we do XML searches of the settingsXML or insert nodes.  May
2013-10-02	WAB CASE 437290 Major overhaul to try and prevent XML corruptions.  Added a layer of caching for static settings
2014-04-22	WAB Following CASE 437290, was referring to application.settings when should have been passing arguments.settingsStructure
03/10/2014  SB	Added heading for edit settings
2015-05-13	WAB	Some work to support variants better. (During CASE 444527) In particular settings which change depending upon the value of TestSite and those which vary by sitedef
2015-10-27	WAB	Added support for a delimiter when appendToDefault attribute is used
2015-11-09	WAB	Fixed problem with above appendToDefault code.  Somehow I had managed to make the default delimiter ' rather than ,
2016-03-23	WAB	BF-602 Problem with Password settings which have variants (such as the CFAdminPassword) - the actual password was being output in the HTML source
2016-04-04	WAB	PROD2016-823 Added support for country variants / got existing code to work
2016-04-05	WAB BF-246 Get UI related to setting variants to work again in the DIV world
2016-04-05	WAB Change the way that I indicate the default value of a setting
				The input box now shows the correct value whether it is the default or not
				There is a hover over title which indicates what the default value is (if the current value is not the default)
				I would like some UI indication to show which items have been updated - there is a style/class hook available.
2016-06-29	WAB	Settings Corruption
				Put a lock around the whole of getSetting to try to reduce the chances of corruption
2017/02/20	NJH/WAB - JIRA PROD2016-3416 and PROD2016-3417. Have an "orig" field for every setting that gets passed through to the settings functions. Update the xml if a setting function is called so that the setting can be accessed
				inside the function call. Also only process now when settings have changed, rather than for every update that may not have included any changes.
--->

<!---
First Section Has Functions for getting Settings
Second Section has Functions for editing Settings
At the end there are functions or managing the upgrade path (now in a separate cfc admin.release.settingsMigration)
 --->

<cffunction name="loadVariablesIntoApplicationScope" output="false">
	<!---
		Reads all the XML files and merges together
		Loops through the XML getting the current values of each setting
		At end of process puts everything into application scope
	--->

	<cfset var result = {isOK=true,message="Settings Loaded"}>
	<cfset var coreSettingFiles = "">
	<cfset var clientSettingFiles = "">
	<cfset var SettingFiles = "">
	<cfset var xmldoc = "">
	<cfset var firstPass= "true">
	<cfset var xmlfunctions = "">
	<cfset var tempSettings = "">
	<cfset var tempxmldocResult = "">
	<cfset var additionalArgs = "">
	<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

	<!--- WAB 2012-09-17 CASE 430667
		Added some locking
	--->
	<cftry>
	<cflock name="loadSettingsXML" timeout=0 throwOnTimeout=true>

		<cfif not isDefined("applicationScope.com.xmlFunctions")>
			<cfset xmlFunctions = createObject("component","com.xmlfunctions")>
		<cfelse>
			<cfset xmlFunctions = applicationScope.com.xmlfunctions>
		</cfif>
		<!--- load up all setting Files --->
		<cfdirectory action="LIST" directory="#applicationScope.paths.relayware#/XML/settings" filter="*.xml" name="coreSettingFiles">
		<cfdirectory action="LIST" directory="#applicationScope.paths.abovecode#/XML/settings" filter="*.xml" name="clientSettingFiles">

		<cfquery name = "SettingFiles" dbtype="query">
			select directory, name, 1 as orderIndex from coreSettingFiles
			union
			select directory, name,2 as orderIndex from clientSettingFiles
			order by orderIndex, name

		</cfquery>
		<cfloop query = "SettingFiles">
			<cfset tempxmldocResult = xmlFunctions.readAndParseXMLFile(fileNameAndPath = directory & "\" & name,changeCase_name="lower",changeCase_attribute="lower", saveMixedCaseNameAsAttribute=true)>
			<cfif tempxmldocResult.isOK>
				<cfif firstPass>
					<cfset xmlDoc = tempxmldocResult.XML>
					<cfset firstPass = false>
				<cfelse>
					<cfset xmlFunctions.xmlmerge (xmldoc,tempxmldocResult.XML)>
				</cfif>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = result.message  & tempxmldocResult.message & "<BR>">
				<cfoutput>#tempxmldocResult.message#</cfoutput>
				<!--- Not sure of what to do if XML fails --->
			</cfif>
		</cfloop>

		<!--- create structure to hold all the information --->
		<cfset tempSettings = structNew()>
		<cfset tempSettings.masterXML = xmldoc>  <!--- this is the xml doc as read from the files, it is used for primarily for displaying the edit pages --->
		<cfset tempSettings.settingXML =  XmlNew()>  <!--- this is created from the masterDoc and has all the information required to get the value of any setting--->
		<cfset tempSettings.settingXML.xmlroot = XmlElemNew(tempSettings.settingXML,"settings")>
		<cfset tempSettings.conditions = structNew()> <!--- any setting which have conditions attached to them have the conditions stored here [not not used liev yet, may change!] --->
		<cfset tempSettings.sites = structNew()>
		<cfset tempSettings.staticCache = structNew()>
		<cfset tempSettings.staticCacheMembers = structNew()>

		<!--- populate the settingXML from the masterXML.  Not that this is all done within a temporaray structure to prevent problems when settings are being reloaded --->
			<cfset additionalArgs = {settingStructure = tempSettings, usingTemporarySettingStructure = true}>
			<cfset runFunctionAgainstNodes(
						XMLNodeArray = tempSettings.masterXML.settings,
						function = populateSettingXMLNodeWithCurrentValues,
							additionalArgs = additionalArgs)>

			<!--- 2011/11/08 WAB/NJB reorder the settings alphabetically --->
			<cfset xmlfunctions.sortXMLChildrenByAttributeValueOrXMLName(XMLNode = tempSettings.masterXML.settings,AttributeName = "Label")>

			<cflock name="#variables.lockname#" type="exclusive" timeout=1 throwOnTimeout=true>
				<cfset applicationScope.settings = tempSettings>  <!--- could do a lock around this and getSetting --->
			</cflock>

	</cflock>

	<cfcatch>
		<cfif cfcatch.type is "lock">
			<cfif structKeyExists (applicationScope,"settings")>
				<!--- settings are already loaded, this is a reload, so can just return a false. --->
				<cfset result.isOK = false>
				<cfset result.message = "Settings being loaded by another process">
			<cfelse>
				<!--- this must be a lock during boot, need to throw an error --->
				<cfthrow type="ApplicationInitialising">
			</cfif>
		<cfelse>
			<cfrethrow>
		</cfif>

	</cfcatch>
	</cftry>
	<cfreturn result>

</cffunction>



<!---
Used when loading up the SettingXML
Gets the Main setting value (if there is one) and information about variants
 --->
<cffunction name="getMainSettingValueFromDataBase" output="false">
	<cfargument name="variableName" default = "">
		<cfset var getSetting = "">

		<!--- get the setting Values from the table, ready to populate the XML structure
			WAB 2015-05-13 Alter to deal with testSite variants (make sure that variant is chosen in preference to other setting)
		--->
		<CFQUERY NAME="getSetting" datasource="#applicationScope.sitedatasource#">
			select
				s.name as VarName,
				s2.value ,
				s2.testSite,
				case when s2.value is null then 0 else 1 end hasOverride,
				sum(case when isnull(s.sitedefid,'') <> '' then 1 else 0 end) as hasSiteVariant,
				sum(case when isnull(s.condition,'') <> '' then 1 else 0 end) as hasCondition,
				sum(case when s.countryid is not null then 1 else 0 end) as hasCountryVariant,
				sum(case when s.entityTypeID = 0 /*TBDapplication.entityTypeID.person*/ then 1 else 0 end) as hasPersonVariant, <!--- NJH 2011/02/17 --->
				sum(case when s.entityTypeID = 68 /*TBDapplication.entityTypeID.userGroup*/ then 1 else 0 end) as hasUserGroupVariant <!--- NJH 2011/02/17 --->
			from settings s left join
				settings s2  on
								s.name = s2.name
								and (s2.testSite is null or dbo.listfind(s2.testSite, #applicationScope.testsite#) <> 0)
								and (isnull(s2.sitedefid,'') = '' and isnull(s2.condition,'') = '' and s2.entityTypeID is null and s2.countryid is null)  <!--- 2012-12-13 WAB had to add the and s2.entityTypeID is null, otherwise could bring back a random personal setting as the main setting--->
			where
			(s.testSite is null or dbo.listfind(s.testSite, #applicationScope.testsite#) <> 0)
			<cfif variablename is not "">
			AND s.name =  <cf_queryparam value="#arguments.variableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			group by s.name, s2.Value, s2.testSite
			order by s.Name, isnull (s2.testSite,-1) desc
		</CFQUERY>

	<cfreturn getSetting >
</cffunction>

<!---
WAB 2013-04-24 Pulled this code which was repeated in 3 places into a function
	Combines information from database with default in master XML to come up with a setting value
	(also returns the setting query which contains information about variant settings)
 --->
<cffunction name="getMainSettingValueFromDataBaseAndDefault" output="false">
	<cfargument name="variableName" default = "">
	<cfargument name="masterXMLNode" default = "">
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">

		<cfset var result = {query = getMainSettingValueFromDataBase (variablename = arguments.variablename),value = arguments.masterXMLNode.xmlText }>

			<cfif result.query.recordcount and result.query.hasOverRide>
				<cfif isAttributeTrue (masterXMLNode.xmlattributes,"appendToDefault")>
					<cfset var delimiter = ",">
					<cfif structKeyExists(masterXMLNode.xmlattributes,"appendDelimiter")>
						<cfset delimiter = masterXMLNode.xmlattributes.appendDelimiter>
					</cfif>
					<!--- This is a special case where the database settings are appended to the default --->
					<cfset result.value= listappend (masterXMLNode.xmlText,result.query.Value,delimiter )>
				<cfelse>
					<cfset result.value	= result.query.Value>
				</cfif>
			</cfif>

			<cfset result.value = evaluateString(stringToEvaluate = result.value, settingStructure = arguments.settingStructure )>
			<cfset result.isPassword = isMasterNodeAPassword(arguments.masterxmlnode)>

	<cfreturn result>

</cffunction>

<!---
	gets distinct list of keys beneath a given variable
	used for dynamic keys
	returns a query with all the keys and (if there is a valid value query) the correct display values in the correct order
	WAB 2013-04-25 discovered bug introduced when I added the lastupdatedby column to this query - resulted in multiple rows appearing in editor when using dynamic keys.  Just take the max lastupdated instead
--->
<cffunction name="getSettingKeysFromDataBase" output="false">
	<cfargument name="variableName" default = "">
	<cfargument name="validValues" default = "">

		<cfset var getStructureKeys = "">
		<cfset var getValidValues = "">
		<cfset var validValueQuery = "">

				<cfquery name = "getStructureKeys" datasource = "#applicationScope.sitedatasource#">
				declare @startPos int
				declare @variablename varchar (200)
				select @variablename =  <cf_queryparam value="#variableName#." CFSQLTYPE="CF_SQL_VARCHAR" >
				select @startPos = len (@variablename) + 1
				select
					substring(name,@startPos,case when charindex('.',name,@startPos) <> 0 then charindex('.',name,@startPos) else len(name) + 1 end - @startPos) as dataValue,
					substring(name,@startPos,case when charindex('.',name,@startPos) <> 0 then charindex('.',name,@startPos) else len(name) + 1 end - @startPos) as displayValue,
					max(lastupdated) as lastupdated

				from settings s
				where Name like  @variablename+'%'
				and isnull(s.condition,'') = ''
				and isnull(s.siteDefID,'') = ''
				group by
					substring(name,@startPos,case when charindex('.',name,@startPos) <> 0 then charindex('.',name,@startPos) else len(name) + 1 end - @startPos),
					substring(name,@startPos,case when charindex('.',name,@startPos) <> 0 then charindex('.',name,@startPos) else len(name) + 1 end - @startPos)
				</cfquery>

				<cfif validvalues is not "">
					<cfset validValueQuery = application.com.relayForms.getValidValuesFromString(string = validvalues)>

					<!--- order structure keys in same way --->
					<cfquery name="getStructureKeys" dbtype= "query">
						select getStructureKeys.dataValue, validValueQuery.displayValue
							from validValueQuery , getStructureKeys
						where cast (validValueQuery.datavalue as varchar) = getStructureKeys.dataValue
					</cfquery>

				</cfif>

	<cfreturn getStructureKeys>
 </cffunction>

<!---
	populates the settingXML structure with the correct value of the setting,
	or the queries to get site and other variants
	Also standardises the masterXML nodes so that they all have a Label
--->
<cffunction name="populateSettingXMLNodeWithCurrentValues" output="false">
	<cfargument name = "XMLNode">
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">
	<cfargument name = "mode" default="">
	<cfargument name = "usingTemporarySettingStructure" default="false"> <!--- When acting on a temporary XML structure (ie when calling loadVariablesIntoApplicationScope() has been called ) we don't have to worry about the CF Locking --->

			<cfset var result = true>
			<cfset var parentnodeArrayInSettingXML = "">
			<cfset var newElement = "">
			<cfset var settingXMLNode = "">
			<cfset var searchForThisNode = "">
			<cfset var fullname = "">
			<cfset var nodeArrayInSettingXML = '' />
			<cfset var thisSettingQry = '' />
			<cfset var parentSettingXMLNode = '' />
			<cfset var parentFullName = '' />
			<cfset var keys = '' />
			<cfset var thisKeyXMLNode = '' />
			<cfset var thisName = '' />
			<cfset var thisSettingXMLNode = '' />
			<cfset var childIndex = '' />
			<cfset var getSiteVariants = '' />
			<cfset var getVariants = '' />
			<cfset var tempLabel = '' />
			<cfset var variableToSet = '' />
			<cfset var i = '' />
			<cfset var thisLockName = #iif(usingTemporarySettingStructure,rand(),DE(variables.lockname))# />
			<cfset var thisSetting = "">


			<cfset var masterXMLNode = XMLNode>  <!--- just for easier readability --->

			<!--- look for units in the name --->
			<cfset var groupNames = {1="units"}>
			<cfset var unitsTest = applicationScope.com.regexp.refindAlloccurrences(reg_expression= "_(seconds|minutes|hours|days|months|years)\Z",string = xmlnode.xmlname,groupNames = groupNames)>
			<cfif arrayLen(unitsTest)>
				<cfset masterXMLNode.xmlattributes.units = unitstest[1].units>
			</cfif>

			<!--- If there isn't a label the create one from the original XMLName (with some added spaces and thingd) --->
			<cfif not StructKeyExists (masterXMLNode.xmlattributes,"label")>
				<cfset tempLabel = masterXMLNode.xmlattributes.mixedCaseXMLName>
				<cfif arrayLen(unitsTest)>
					<cfset tempLabel = replaceNoCase (tempLabel,unitstest[1].units,"","ONE")>
				</cfif>
				<cfset masterXMLNode.xmlattributes.label = applicationScope.com.regExp.camelCaseToSpaces(tempLabel)>
			</cfif>

			<cfif not isAttributeTrue (masterxmlnode.xmlattributes,"dynamicKey")>
				<!--- hasDynamicKey is a special case dealt with later --->

				<!--- Does this node already exist in the settingsXML --->
					<cfset nodeArrayInSettingXML = getCurrentNodeInSettingXML (node = masterXMLNode,settingStructure = arguments.settingStructure)>
				<cfif arrayLen(nodeArrayInSettingXML) >
					<!--- Node Exists --->
					<cfset settingXMLNode = nodeArrayInSettingXML[1]>
				<cfelse>
					<!--- Add this node to the settings XML --->
					<!--- find parent node in the settings XML --->
					<cflock name="#thisLockName#" type="exclusive" timeout=1>
						<cfset parentnodeArrayInSettingXML =getCurrentNodeInSettingXML (node = masterXMLNode.xmlparent, settingStructure = settingStructure)>
						<cfset newElement = XMLElemNew(settingStructure.settingXML,masterxmlnode.xmlname)>
						<cfset arrayAppend(parentnodeArrayInSettingXML[1].xmlchildren,newElement)>
						<cfset searchForThisNode = xmlsearch(parentnodeArrayInSettingXML[1],"./#masterxmlnode.xmlname#")>
						<cfset settingXMLNode = searchForThisNode [1]>
					</cflock>

				</cfif>

			</cfif>

			<!--- If there are not XML children then this is a node with a 'value' --->
			<cfif arrayLen(masterXMLNode.xmlchildren) is 0 and not isAttributeTrue (masterxmlnode.xmlattributes,"dynamicKey")>
				<!--- work out the name in dot notation so that we can check in the relay var query--->
				<cfset fullname = getFullVariableNameFromNode(masterXMLNode)>
				<cfset masterXMLNode.xmlattributes.overridden	= false>  <!--- this will be set to true if the default value is overRidden by a value from the db --->

				<!---
				WAB 2013-04-24 consolidated some code into a single function
				Now get Main setting value from the db, along with info about variants--->
				<cfset thisSetting = getMainSettingValueFromDataBaseAndDefault (variablename = fullname,masterXMLNode = masterXMLNode,settingStructure = settingStructure )>



				<cfset settingXMLNode.xmltext = thisSetting.value>
				<cfset masterXMLNode.xmlattributes.overridden	= thisSetting.Query.hasOverRide> <!--- ie true --->
				<!--- NJH 2012-02-08 - put in xml whether setting is a password or not --->
				<cfset settingXMLNode.xmlAttributes.isPassword = thisSetting.isPassword>


				<cfif thisSetting.query.recordCount>

					<!--- deal with site variants and conditions --->
					<!--- WAB 2011/03 TBD site variants and conditions may need to be done differently now that we have worked out a way of doing person and UG variants --->
					<cfif thisSetting.query.hasSiteVariant>
						<cfset settingXMLNode.xmlAttributes.hasSiteVariant = true>
						<cfquery name="getSiteVariants" datasource="#applicationScope.sitedatasource#">
						select sd.sitedefid,value
						from settings s
							inner join siteDef sd on dbo.listfind(s.sitedefid,sd.sitedefid) <> 0
							where s.name =  <cf_queryparam value="#fullname#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and isnull(s.condition,'') = ''
							and isnull(s.siteDefID,'') <> ''
						</cfquery>

						<cfset arguments.settingStructure.sites [fullname] = applicationScope.com.structureFunctions.queryToStruct(query=getSiteVariants,key="sitedefid")>

					</cfif>

					<cfif thisSetting.query.hasCondition>
						<cfset settingXMLNode.xmlAttributes.hasCondition = true>
						<cfquery name="getVariants" datasource="#applicationScope.sitedatasource#">
						select * from settings
							where name =  <cf_queryparam value="#fullname#" CFSQLTYPE="CF_SQL_VARCHAR" >  and isnull(condition,'') <> ''
						</cfquery>
						<cfset arguments.settingStructure.conditions [fullname] = getVariants>
					</cfif>

					<!--- NJH 2011/02/17 --->
					<cfif thisSetting.query.hasPersonVariant>
						<cfset settingXMLNode.xmlAttributes.hasPersonVariant = true>
					</cfif>

					<cfif thisSetting.query.hasUserGroupVariant>
						<cfset settingXMLNode.xmlAttributes.hasUserGroupVariant = true>
					</cfif>

					<cfif thisSetting.query.hasCountryVariant>
						<cfset settingXMLNode.xmlAttributes.hasCountryVariant = true>
					</cfif>

				</cfif>

				<!--- NJH 2011/10/26 - added ability to include files.. used cfinclude rather than cf_include as I want the setting node to be available --->
				<cfif structKeyExists(masterXMLNode.xmlAttributes,"include") and fileExists(expandpath(masterXMLNode.xmlAttributes.include))>
					<cfinclude template = "#masterXMLNode.xmlAttributes.include#">
				</cfif>
				<!--- <cfdump var="#settingXMLNode#"><cfdump var="#masterXMLNode#"><CF_ABORT> --->

				<!--- Check if the value needs evaluating, variables with [[]] in them  --->
				<cfset settingXMLNode.xmltext = evaluateString(stringToEvaluate = settingXMLNode.xmltext, settingStructure = settingStructure )>

				<!--- For backwards compatibility for some variables we set the original application variable as well--->
				<cfif structKeyExists (masterXMLNode.xmlattributes,"setVariable")>
					<!--- WAB 2011/03/10 need to replace application. with applicationScope. so that this will work when called remotely--->
					<cfset variableToSet = replaceNocase(masterXMLNode.xmlattributes.setVariable,"application.","applicationScope.","ONE")>
					<cfset "#variableToSet#" = settingXMLNode.xmltext>
				</cfif>


			<cfelseif isAttributeTrue (masterxmlnode.xmlattributes,"dynamicKey")>
				<!--- This is a node which defines a structure of multiple items --->
				<!--- first get the parent node from the settings doc --->
				<cfset parentSettingXMLNode = getCurrentNodeInSettingXML (node = masterXMLNode.xmlparent, settingStructure = settingStructure )>
				<cfset parentSettingXMLNode = parentSettingXMLNode[1]>
				<cfset parentSettingXMLNode.xmlattributes["dynamicKey"] = "true">
				<cfset parentFullName = getFullVariableNameFromNode(masterxmlnode.xmlParent)>

				<!--- get list of keys from the db --->
				<cfset keys =getSettingKeysFromDataBase(variablename = getXMLParentList(masterxmlnode))>

				<!--- need to delete any existing keys from the settings XML where the key no longer exists in the db --->
				<cflock name="#thisLockName#" type="exclusive" timeout=1>
					<cfloop index="i" from = "#arrayLen(parentSettingXMLNode.xmlChildren)#" to = "1" step = -1>
						<cfif not listFind(valueList(keys.datavalue),rereplace(parentSettingXMLNode.xmlChildren[i].xmlname,"\A_",""))>
							<cfset xmlfunctions.deleteNode(parentSettingXMLNode.xmlChildren[i])>
						</cfif>
					</cfloop>
				</cflock>

				<!--- for each key need to:
					 loop through the defaults,
					 look in the db,
					 add element to the XML

					 TBD - Not dealing with variants here

					 --->


				<cfloop query="keys">
					<!--- add this key in the settingsXML if necessary --->
					<cflock name="#thisLockName#" type="exclusive" timeout=1>
						<cfset thisKeyXMLNode = addNodeIfDoesntExist (settingStructure.settingXML,parentSettingXMLNode,dataValue)>
					</cflock>

					<!--- WAB 2013-04-24 consolidated some code in this area into a single function getMainSettingValueFromDataBaseAndDefault --->
					<cfif arrayLen(masterxmlnode.xmlchildren)>
						<!--- dynamicKey has children --->
						<cfloop index="childIndex" from="1" to="#arrayLen(masterxmlnode.xmlchildren)#">
							<cfset thisName = parentFullName & "." & dataValue & "." & getVariableNameFromNode(masterxmlnode.xmlchildren[childIndex])>

							<cfset thisSetting = getMainSettingValueFromDataBaseAndDefault (variablename = thisname,masterXMLNode = masterxmlnode.xmlchildren[childIndex],settingStructure = settingStructure )>

							<cflock name="#thisLockName#" type="exclusive" timeout=1>
								<cfset thisSettingXMLNode = addNodeIfDoesntExist (settingStructure.settingXML,thisKeyXMLNode,masterxmlnode.xmlchildren[childIndex].xmlname)>
								<cfset thisSettingXMLNode.xmltext = thisSetting.value>
								<cfset thisSettingXMLNode.xmlAttributes.isPassword = thisSetting.isPassword>
							</cflock>

						</cfloop>

					<cfelse>
						<!--- dynamicKey does not have children --->
							<cfset thisName = parentFullName & "." & dataValue>
							<cfset thisSetting = getMainSettingValueFromDataBaseAndDefault (variablename = thisName,masterXMLNode = masterxmlnode,settingStructure = settingStructure )>

							<cfset thisKeyXMLNode.xmltext	= thisSetting.Value>
							<cfset thisKeyXMLNode.xmlAttributes.isPassword = thisSetting.isPassword>

					</cfif>
				</cfloop>
				<cfset result = false>  <!--- don't keep going down --->
			<cfelse>
				<!---  Node has children so return --->

			</cfif>


		<cfreturn result>

</cffunction>

<cffunction name="addNodeIfDoesntExist" hint="and return new node" output="false">
	<cfargument name="XMLDoc" >
	<cfargument name="XMLNode" >
	<cfargument name="XMLName" >

	<cfset var xmlNodeName = reReplace(XMLName,"\A([0-9])","_\1","ONE")>
	<cfset var xmlSearchResult = xmlSearch (XMLNode,"./#xmlNodeName#")>
	<cfif not arrayLen(xmlSearchResult)>
		<!--- Add this node to XML --->
		<cfset arrayAppend(xmlNode.xmlchildren,XMLElemNew(xmlDoc,xmlNodeName))>
		<cfset xmlSearchResult = xmlSearch (XMLNode,"./#xmlNodeName#")>

	</cfif>
	<cfreturn xmlSearchResult[1]>

</cffunction>

<!--- gets the value of a setting given then variable name --->
<cffunction name="getSetting" output="false" >
	<cfargument name= "variablename" >
	<cfargument name= "settingStructure" default = "#applicationScope.settings#">
	<cfargument name= "personID" type="numeric" required="false">
	<cfargument name= "personVariant" default="true" type="boolean" hint="This is a switch to let us know what the person's default would be if they did not have a person variant set. This value is to display on the personal edit setting page">
	<cfargument name= "countryid" type="numeric" default="#(structKeyExists(request,"relaycurrentUser") and structKeyExists(request.relaycurrentUser,"countryid")?request.relaycurrentUser.countryid:0)#">
	<cfargument name= "reloadOnNull" type="boolean" default="true"> <!--- If a setting is not found in memory we do a reload, but to prevent infinite looping we only try once (just in case there is some other reason for it not being there) --->

	<cfset var result = "">
	<cfset var xmlsearchResultArray = "">
	<cfset var getForPersonID = 0>
	<cfset var checkMasterXML = 0>
	<cfset var tries = 0>
	<cfset var maxTries = 2>
	<cfset var numberOfErrors = 0>
	<cfset var lastError = "">
	<cfset var resultWithMetaData = "">
	<cfset var checkCache = "">

	<cfif not structKeyExists(arguments,"personID") and structKeyExists(request,"relayCurrentUser")>
		<cfset getForPersonID = request.relayCurrentUser.personID>
	<cfelseif structKeyExists(arguments,"personID")>
		<cfset getForPersonID = arguments.personID>
	</cfif>

	<!--- 2013-10-02 WAB CASE 437290 Added code to store static values (those which do not have variants of any sort) in a separate memory cache, to try an prevent problems of XML corruption---->

	<!--- Look for the setting value in our static cache, if it is defined then use it--->
	<cfset checkCache = getSettingFromCache (variablename=arguments.variablename,settingStructure = arguments.settingStructure)>

	<cfif isDefined("checkCache")>
		<cfreturn checkCache>
	</cfif>


	<cflock name="#variables.lockname#" timeout="10" throwontimeout="true">

		<!--- 2013-10-02 WAB CASE 437290 try to trap the regular XML error (which has a blank message)
						and immediately try again to get setting value
						May not be necessary if the staticCache turns out to be successful
		 --->
		<cfset xmlsearchResultArray = getVariableNodeArray_Setting(variablename=arguments.variablename,settingStructure = arguments.settingStructure)>

		<cfloop condition="tries lt maxTries">
			<cfset tries ++>
			<cftry>

				<!--- For debugging purposes - deliberately throw an error
				<cfif tries lt maxTries><cfthrow message="" detail="This is a test error"></cfif>
				--->

				<cfif arrayLen(xmlsearchResultArray) gt 0 >
					<!--- The required node has been found in the XML document, so we now have to extract the setting from it - which may need recursing down a few levels --->
					<cfset resultWithMetaData = getSettingFromNode_ReturnsMetaData(xmlnode = xmlsearchResultArray[1], settingStructure = arguments.settingStructure,personID=getForPersonID,personVariant = arguments.personVariant, countryid = arguments.countryid)>
					<!--- If the value (or structure) which is returned does not contain any items with Person/User/Country variants then we can cache it
							We also won't cache any item which is a password (I just don't really want these floating around in application scope)
					--->
					<cfif resultWithMetaData.metaData.static and NOT resultWithMetaData.metaData.hasPassword>
						<cfset addSettingToCache(variablename = variablename, value = resultWithMetaData.settingValue, settingStructure = arguments.settingStructure)>
					</cfif>

					<cfset result = resultWithMetaData.settingValue>
				<cfelse>
					<!---
						2012-09-25	WAB
						if no node found, we are going to do a check that this setting hasn't fallen out of memory
						see whether it exists in the master document, if so do a reload and another getsetting
					 --->
					<cfif reloadOnNull>
						<cfset checkMasterXML = getVariableNodeArray_Master(variablename=arguments.variablename,settingStructure = arguments.settingStructure)>

						<cfif arrayLen(checkMasterXML)>
							<cfset populateSettingXMLNodeByVariableName(variablenames = arguments.variablename, updateCluster = false)>
							<cfset result = getSetting(argumentCollection = arguments,reloadOnNull=false)>
						</cfif>
					</cfif>
				</cfif>

				<!--- If we have got this far then there hasn't been an error and we can pop out of the loop --->
				<cfbreak>

				<cfcatch>
					<cfset numberOfErrors ++>
					<cfset lastError = cfcatch>
					<cfif cfcatch.message is "" and tries eq 1>
						<!--- On first Blank Error send a message, will then go around the loop and try again --->
						<cfmail from="william.bibby@relayware.com" to="william.bibby@relayware.com" subject="#cgi.server_Name#.  Error getting setting #variablename#.  Will Try again" type="html">
							<cfdump var="#lastError#">
						</cfmail>
					<cfelseif cfcatch.message is not "" or tries gte maxTries >
						<!--- If it isn't a regular blank error, or we have come to the end of our loop of attempts, then send a message and rethrow--->
						<cfmail from="william.bibby@relayware.com" to="william.bibby@relayware.com" subject="#cgi.server_Name#.  Error getting setting #variablename#.  FAILED after #NumberOfErrors# Attempts" type="html">
							<cfdump var="#lastError#">
						</cfmail>
						<cfrethrow>
					<cfelse>
						<!--- Will go around the loop and Try again--->
					</cfif>
				</cfcatch>

			</cftry>

		</cfloop>

		<cfif numberOfErrors is not 0>
			<cfmail from="william.bibby@relayware.com" to="william.bibby@relayware.com" subject="#cgi.server_Name#.  Error getting setting #variablename#.  SUCCEEDED after #NumberOfErrors# Attempts"  type="html">
				<cfdump var="#lastError#">
			</cfmail>
		</cfif>

	</cflock>

	<cfreturn result>

</cffunction>


<cffunction name="deleteSettingFromCache">
	<cfargument name="variableName" >

	<cfset var name = "">

	<cfset application.com.structureFunctions.structDelete_DotNotation(application.settings.staticCache,variablename)>
	 <!--- We don't actually really need to delete the key from the staticCache,
	 		but rather we do need to delete from the staticCacheMembers any item which is a Parent, Child or self of this setting --->
	<cfloop collection="#application.settings.staticCacheMembers#" item="name">
		<cfif name contains variablename OR variablename contains name>
			<cfset structDelete (application.settings.staticCacheMembers,name)>
		</cfif>
	</cfloop>

</cffunction>


<cffunction name="getSettingFromCache">
	 <cfargument name="variableName" >
	<cfargument name= "settingStructure" default = "#applicationScope.settings#">

	 <cfset var variableInCache = false>
 	 <cfset var i = "">
	 <cfset var lookforvariable = variableName>

	<!--- If we have already loaded this variable or one of its parents into the static cache then we can read our value directly from it
		so loop along looking for the full variable and then each parent in turn until we find it (or not in which case it will have to be loaded)
	--->
	 <cfloop from="1" to="#listLen(variablename,".")#" index="i">
		<cfif structKeyExists (settingStructure.staticCacheMembers,lookForVariable)>
			<cfset variableInCache = true>
		</cfif>
		<cfset lookForVariable = application.com.globalFunctions.listAllButLast(lookForVariable,".")>
	 </cfloop>

	<cfif variableInCache>
		 <cfreturn duplicate(application.com.structureFunctions.structFind_DotNotation(settingStructure.staticCache,variablename))>
	<cfelse>
		<cfreturn>
	</cfif>

</cffunction>

<cffunction name="addSettingToCache">
	 <cfargument name="variableName" >
	 <cfargument name="value" >
	<cfargument name= "settingStructure" default = "#applicationScope.settings#">

	 <cfset settingStructure.staticCacheMembers[variableName] = "">
	 <cfreturn application.com.structureFunctions.structInsert_DotNotation(settingStructure.staticCache,variablename,value)>

</cffunction>



<!--- gets the default value of a setting given then variable name --->
<cffunction name="getDefaultSetting" output="false">
	<cfargument name="variablename" >
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">

	<cfset var result = "">
	<cfset var xmlsearchResultArray = getVariableNodeArray_Master(variablename=variablename,settingStructure = settingStructure)>

	<cfif arrayLen(xmlsearchResultArray) gt 0 >
		<cfset result = xmlsearchResultArray[1].xmltext>
	<cfelse>
		<cfset result = "">
	</cfif>

	<cfreturn result>

</cffunction>

<cffunction name="isSettingDefault" output="false">
	<cfargument name="variablename" >

	<cfreturn iif(getSetting(variablename) is getDefaultSetting(variableName),true,false) >

</cffunction>


<cffunction name="isSettingAPassword" output="false">
	<cfargument name="variablename" >

	<cfset var nodeArray = getVariableNodeArray_Setting(variablename)>
	<cfset var result = false>

	<cfif arrayLen(nodeArray) and nodeArray[1].xmlAttributes.isPassword>
		<cfset result = true>
	</cfif>

	<cfif not arrayLen(nodeArray)>
		<!---  may be brand and dynamic node --->
		<cfset nodeArray =  getVariableNodeArray_Master(variablename)>
		<cfif arrayLen(nodeArray)>
			<cfset result = isMasterNodeAPassword(nodeArray[1])>
		</cfif>
	</cfif>

	<cfreturn result >

</cffunction>

<cffunction name="isMasterNodeAPassword" output="false">
	<cfargument name="XMLnode" >

	<cfset var result = false>

	<cfif structKeyExists(XMLNode.xmlattributes,"relayFormElementType") and XMLNode.xmlattributes.relayFormElementType eq "password">
		<cfset result = true>
	</cfif>

	<cfreturn result >

</cffunction>



<!--- returns the result of the search for a particular variable in the SettingXML (as an array so that you can easily check for not found) --->
<cffunction name="getVariableNodeArray_Setting" output="false">
	<cfargument name="variablename" >
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">

	<!--- variables starting with numbers are in the xml with a leading underscore --->
	<cfset var xPath = reReplaceNoCase (variableName,"([\A\.])([0-9])","\1_\2","ALL")>
	<cfset var result = "">
	<!--- replace dot with slash --->
	<!--- 2012-12-05	WAB	CASE 432508 correct to use absolute path from top of document --->
	<cfset xPath = "/" & replace(lcase(xpath),".","/","ALL")>
	<cfif xpath is not "/settings">
		<cfset xpath = '/settings' & xpath>
	</cfif>

		<!--- <cflock name="#variables.lockname#" type="exclusive" timeout=1 throwOnTimeout=true>
			<cfset result = xmlsearch (settingStructure.settingXML,xPath )>

			<cfset result = xmlsearch (settingStructure.settingXML,xPath )>
		</cflock> --->
		<cfset result = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=settingStructure.settingXML,xPathString=xPath,lockName="settings",timeout=1)>

	<cfreturn result>

</cffunction>

<!--- returns the result of the search for a particular variable in the masterXML (as an array so that you can easily check for not found) --->
<cffunction name="getVariableNodeArray_Master" output="false">
	<cfargument name="variablename" >
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">
	<cfargument name= "GetParentIfThisNodeDoesNotExist" default = false>  <!--- This is a special case when we are dealing with variables where one of the keys is dynamic.  The variable will not exist in the master XMLDoc so we look for the nearest node we can get --->

	<cfset var dynamicKeyFilter = '' />
	<cfset var originalxPath = '' />
	<cfset var i = '' />
	<cfset var result = "">

	<!--- variables starting with numbers are in the xml with a leading underscore --->
	<cfset var xPath = reReplaceNoCase (variableName,"(\A|\.)([0-9])","\1_\2","ALL")>
	<!--- replace dot with slash --->
	<cfset xPath = "/" & replace(lcase(xpath),".","/","ALL")>
	<!--- 2012-12-05	WAB	CASE 432508 correct to use absolute path from top of document --->
	<cfif xpath is not "/settings">
		<cfset xpath = '/settings' & xpath>
	</cfif>

	<cfset result = xmlsearch (settingStructure.masterXML,xPath )>

	<cfif not arrayLen(result) and GetParentIfThisNodeDoesNotExist>
		<cfset result = getVariableNodeArray_Master(variablename = reverse(listrest(reverse(variablename),".")),settingStructure = settingStructure, GetParentIfThisNodeDoesNotExist = GetParentIfThisNodeDoesNotExist)>
	</cfif>


	<!--- No result found, perhaps there is a dynamic key in there somewhere --->
	<cfif not arrayLen(result) and listLen(variableName,".") gt 1>
		<!---  alter the xpath to search for a dynamic key.   so //setting1/_10/name  becomes //setting1/*[@dynamickey='true']/name
			assumes that the dynamic key is a node with one set of children, if it works we can be cleverer
		--->
		<cfset dynamicKeyFilter = "*[@dynamickey='true']">
		<cfset originalxPath = xpath>
		<cfloop index="i" from = #listLen(xpath,"/")# to="1" step="-1">
			<cfset xpath = listdeleteat(listInsertAt(originalxPath,i,dynamicKeyFilter,"/"),i+1,"/")>
			<cfset result = xmlsearch (settingStructure.masterXML,xPath )>
			<cfif arrayLen(result)>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>



	<cfreturn result>

</cffunction>

<!--- pass in a node (probably in the masterXML doc)  and returns the equivalent node in the settingXML doc --->
<cffunction name="getCurrentNodeInSettingXML" output="false">
	<cfargument name="node" >
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">

	<cfreturn getVariableNodeArray_Setting (variablename = getFullVariableNameFromNode(node), settingStructure = settingStructure)>

</cffunction>

<!--- pass in a node (probably in the masterXML doc)  and returns the equivalent node in the settingXML doc --->
<cffunction name="getCurrentNodeInMasterXML" output="false">
	<cfargument name="node" >
	<cfargument name = "settingStructure" default = "#applicationScope.settings#">

	<cfreturn getVariableNodeArray_master (variablename = getFullVariableNameFromNode(node), settingStructure = settingStructure)>

</cffunction>




<cffunction name="getXMLParentList" output="false">
	<cfargument name = "XMLNode">
	<cfif not isXMLRoot(XMLNode) and XMLNode.xmlParent.xmlName is not "settings">
		<cfreturn listappend (getXMLParentList(XMLNode.xmlParent),getVariableNameFromNode(XMLNode.xmlParent),".")>
	<cfelse>
		<cfreturn "">
	</cfif>

</cffunction>


<!--- given a node gets the full variable name in dot notation--->
<cffunction name="getFullVariableNameFromNode" output="false" access="public">
	<cfargument name="XMLNode" >

	<cfreturn listappend(getXMLParentList(XMLNode),getVariableNameFromNode(XMLNode),".")>	  <!--- leading underscore is removed --->

</cffunction>

<cffunction name="getVariableNameFromNode" output="false" access="public">
	<cfargument name="XMLNode" >

	<!--- leading underscore is removed --->
	<cfreturn rereplace(XMLNode.xmlname,"\A_","","ONE")>

</cffunction>


<!--- given a node gets the full label of a node (ie with all parent labels)--->
<cffunction name="getFullLabelFromNode" output="false" access="public">
	<cfargument name="XMLNode" >
	<cfif not isXMLRoot(XMLNode) and XMLNode.xmlName is not "settings">
		<cfreturn listappend (getFullLabelFromNode(XMLNode.xmlParent),getLabelFromNode(XMLNode),".")>
	<cfelse>

		<cfreturn "">
	</cfif>

</cffunction>

<cffunction name="getLabelFromNode" output="false" access="public">
	<cfargument name="XMLNode" >

	<!--- leading underscore is removed --->
	<cfreturn XMLNode.xmlattributes.label>

</cffunction>


<!--- Given a node gets the setting for that node
	if the node has children then returns all the child settings as a structure
--->
<cffunction name="getSettingFromNode" output="false">
	<cfreturn getSettingFromNode_ReturnsMetaData(argumentCollection = arguments).settingValue>
</cffunction>

<cffunction name="getSettingFromNode_ReturnsMetaData" output="false">
	<cfargument name="XMLNode" >
	<cfargument name="personid" default="#request.relaycurrentuser.personid#" type="numeric">
	<cfargument name="countryid" default=0 type="numeric">
	<cfargument name="settingStructure" default="#applicationScope.settings#" type="struct">
	<cfargument name="PersonVariant" default="true" type="boolean" hint="This is a switch to let us know what the person's default would be if they did not have a person variant set. This value is to display on the personal edit setting page">

	<cfset var metaData = {depth=0,static=true,hasPassword=false}>
	<cfset var result = {settingValue = "",metaData = metaData}>
	<cfset var settingFound = false>
	<cfset var fullname= "">
	<cfset var thequery = '' />
	<cfset var checkCondition = '' />
	<cfset var i = '' />
	<cfset var VariantQry = '' />
	<cfset var childrenArrayLength = arrayLen (arguments.XMLNode.xmlChildren) />
	<cfset var settingname= "">
	<cfset var childXMLNode = "">
	<cfset var childResult = "">
	<cfset var maxChildDepth = 0>

	<cfif childrenArrayLength gt 0  or (isAttributeTrue(arguments.XMLNode.xmlattributes,"dynamicKey")) >
		<cfset result.settingValue = structNew()>
		<cfloop index = "i" from = "1" to = "#childrenArrayLength#">
			<cfset childXMLNode = arguments.XMLNode.xmlChildren[i]>
			<cfset settingname = getVariableNameFromNode(childXMLNode)>
			<cfset childResult = getSettingFromNode_returnsMetaData(XMLNode = childXMLNode,settingStructure = arguments.settingStructure,personid = arguments.personid,PersonVariant = arguments.PersonVariant, countryid = arguments.countryid)>
			<cfset result.settingValue[settingname] = childResult.settingValue>
			<cfset result.metaData.static = (result.metaData.static AND childResult.metaData.static)>
			<cfset result.metaData.hasPassword = (result.metaData.hasPassword OR childResult.metaData.hasPassword)>
			<cfset maxChildDepth = max(maxChildDepth,childResult.metaData.depth)>
		</cfloop>
			<cfset result.metaData.depth = result.metaData.depth + maxChildDepth + 1>

	<cfelse>
		<cfset settingFound = false>
		<cfset fullname = getFullVariableNameFromNode(arguments.XMLNode)>

		<!--- TBD work needed here to deal with different variants, only really does person/usergroup at moment --->
		<cfif (isAttributeTrue(arguments.XMLNode.xmlattributes,"hasPersonVariant")) or (isAttributeTrue(arguments.XMLNode.xmlattributes,"hasUserGroupVariant")) or (isAttributeTrue(arguments.XMLNode.xmlattributes,"hasCountryVariant"))>
			<cfset result.metaData.static = false>
			<cfset VariantQry = getVariant(variableName=fullname, personID=arguments.personID, personVariant = arguments.PersonVariant, countryid = arguments.countryid, countryVariant = isAttributeTrue(arguments.XMLNode.xmlattributes,"hasCountryVariant"))>

			<cfif VariantQry.recordcount>
				<cfset settingFound = true>
				<cfset result.settingValue = VariantQry.value>
				<cfreturn result>
			</cfif>
		</cfif>



		<!--- NJH 2010/07/28 - added structKeyExists --->
		<!--- WAB TBD 2011/03  In light of work on person variants we may want to do conditions and siteVariants differently--->
		<cfif isAttributeTrue(arguments.XMLNode.xmlattributes,"hasCondition")>
			<!--- more standard variables could be set here --->
			<cfset result.metaData.static = false>
			<cfset thequery = arguments.settingStructure.conditions[fullname]>
			<cfloop query="theQuery">
				<cftry>
					<cfset checkCondition = evaluate(condition)>

					<cfif checkCondition >
						<cfset result.settingValue = Value>
						<cfset settingfound = true>
						<cfbreak>
					</cfif>
					<cfcatch>
						<cfoutput>#fullname#<BR> Condition Error  #condition# <BR> #cfcatch.message#<BR></cfoutput>
					</cfcatch>
				</cftry>
			</cfloop>

		</cfif>

		<!--- NJH 2010/07/28 - added structKeyExists --->
		<cfif settingFound is false and isAttributeTrue(XMLNode.xmlattributes,"hasSiteVariant") >
			<cfset result.metaData.static = false>
			<cfif structKeyExists (arguments.settingStructure.sites [fullname],request.currentsite.sitedefid)>
				<cfset result.settingValue = arguments.settingStructure.sites [fullname][request.currentsite.sitedefid].value>
				<cfset settingfound = true>
			</cfif>

		</cfif>

		<cfif settingFound is false>
			<cfset result.settingValue =  arguments.XMLNode.xmlText>
		</cfif>

		<!--- NJH 2012-02-08 - decrypt the password --->
		<cfif arguments.XMLNode.xmlAttributes.isPassword and result.settingValue neq "">
			<!--- WAB 2012-10-03 add a try catch.  If the decrypt fails then just return the string.
				This means that we can put an unencrypted value into the db during the build but can still start up the system in order to run the encryption function.
				Could have automatically done an encryption here, but could wreak havoc if someone uses a database with the wrong masterkey (under content/XML/)
			--->
			<cftry>
				<cfset result.settingValue = application.com.encryption.decryptWithMasterKey(result.settingValue)>
				<cfcatch>
					<cfset result.settingValue = result.settingValue>
				</cfcatch>
			</cftry>
			<cfset result.metadata.hasPassword = true>
		</cfif>

	</cfif>

	<cfreturn result>

</cffunction>




<cffunction name="getVariant" access="public" hint="Gets variants of a setting, ordered so that first row is the setting you want. " returnType="query" output="false">
	<cfargument name="variableName" type="string" required="true">
	<cfargument name="allVariants" type="boolean" default="true">
	<cfargument name="personVariant" type="boolean" default="#allVariants#">
	<cfargument name="usergroupVariant" type="boolean" default="#allVariants#">
	<cfargument name="countryVariant" type="boolean" default="#allVariants#">

	<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
	<cfargument name="countryID" type="numeric" default="#request.relayCurrentUser.countryID#">
	<cfargument name="sitedefid" type="numeric" default="#request.CurrentSite.sitedefid#">

	<cfset var getVariant = "">
	<cfset var result = structNew()>

	<cfquery name="getVariant" datasource="#application.siteDataSource#">
		select top 1 * from settings
		where name =  <cf_queryparam value="#arguments.variableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and
		(
			1=0
			<cfif personVariant>
			or (entityTypeID =  <cf_queryparam value="#application.entitytypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityID = #arguments.personID#)
			</cfif>
			<cfif usergroupVariant>
			or (entityTypeID =  <cf_queryparam value="#application.entitytypeID.usergroup#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityID in (select userGroupID from rightsGroup where personID = #arguments.personID#))
			</cfif>
			<cfif countryVariant>
			or (countryid=#countryid#)
			or (countryid in (select countrygroupid from countrygroup where countrymemberid=#countryid#))
			</cfif>

		)


		<cfif not personVariant>
			and  (entityTypeID<>#application.entitytypeID.person#)
		</cfif>
		<cfif not usergroupVariant>
			and  (entityTypeID<>#application.entitytypeID.usergroup# )
		</cfif>
		<cfif not countryVariant>
			and (countryid is null)
		</cfif>

		and (testsite is null or testsite =  <cf_queryparam value="#application.testSite#" CFSQLTYPE="CF_SQL_INTEGER" > )
		and (sitedefid is null or sitedefid = #sitedefid#)

		order by
			case when entityTypeID =  <cf_queryparam value="#application.entitytypeID.person#" CFSQLTYPE="CF_SQL_INTEGER" >  then 1
				when entityTypeID =  <cf_queryparam value="#application.entitytypeID.usergroup#" CFSQLTYPE="CF_SQL_INTEGER" >  then 2
			end
	</cfquery>

	<cfreturn getVariant>
</cffunction>


<!--- given a variable name, updates the node in the setting XML--->
<cffunction name="populateSettingXMLNodeByVariableName" output="false">
	<cfargument name="variableNames" >
	<cfargument name="updateCluster" default="true" >

	<cfset var variablename = "">
	<cfset var XMLNodeArray 	= "">
	<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->


	<cfloop list = "#variableNames#" index="variablename">
		<cfset deleteSettingFromCache(variablename)>
		<cfset XMLNodeArray = getVariableNodeArray_Master (variablename = variableName, GetParentIfThisNodeDoesNotExist = true)>
		<cfset runFunctionAgainstNodes(
					XMLNodeArray = XMLNodeArray,
					function = populateSettingXMLNodeWithCurrentValues)>
	</cfloop>

	<cfif updateCluster>
			<cfset applicationScope.com.clusterManagement.updateCluster(updatemethod="updateSettingXMLNode",variablenames = variablenames)>
	</cfif>



</cffunction>

<!--- finds any required variables which do not have a value set --->
<cffunction name="checkForBlankRequiredVariables" output="false">
	<!--- TBD this function isn't right, it needs to search the masterXML required fields (which are active) and then check their values in the settingsXML --->
	<cfset var xmlSearchResult =xmlsearch(applicationScope.settings.masterXML,"//node()[text()='' and @required='1']")>

	<cfreturn xmlSearchResult>

</cffunction>


<cffunction name="evaluateString" output="false">
	<cfargument name="stringToEvaluate" >
	<cfargument name="settingStructure" default = "#applicationScope.settings#" >

	<cfset var result = "">
	<cfset var tmpStringToEvaluate = stringToEvaluate>

	<cfif listFirst(stringToEvaluate,":") eq "func">
		<cfset var functionStruct = application.com.relayForms.getFunctionPointerFromString(function=listLast(stringToEvaluate,":"))>
		<cfset var fieldFunction = functionStruct.functionPointer>
		<cfset stringToEvaluate = fieldFunction(argumentCollection=functionStruct.args)>
	</cfif>


			<cfif refind("\[\[",tmpStringToEvaluate)>
				<cfset tmpStringToEvaluate = reReplaceNoCase(tmpStringToEvaluate,"\[\[getSetting\((.*?)\)\]\]","##getSetting(variablename = \1, settingStructure = arguments.settingStructure)##","ALL")>
				<cfset tmpStringToEvaluate = reReplaceNoCase(tmpStringToEvaluate,"\[\[(.*?)\]\]","##\1##","ALL")>
				<cftry>
					<cfset result = evaluate (tmpStringToEvaluate)>
					<cfcatch>
						<!--- If evaluation failure then just return the original string --->
						<!--- Thought that I could automatically try adding quotes, but won't at moment--->
						<cfset result = stringToEvaluate>
					</cfcatch>
				</cftry>

			<cfelse>
				<cfset result = stringToEvaluate>
			</cfif>





	<cfreturn result>

</cffunction>


<cffunction name="removeSettingFromMemory" hint="This function is only used for testing how we deal with missing settings, of no use in a live environment">
	<cfargument name="variablename">

	<cfset var reversed = reverse(variablename)>
	<cfset var parentName = reverse(listrest(reversed,"."))>
	<cfset var childName = reverse(listfirst(reversed,"."))>
	<cfset var getParentNode = "">
	<cfset var i = "">

	<!--- remove a setting from memory --->
	<cfset getParentNode = application.com.settings.getVariableNodeArray_Setting(parentname)>
	<cfloop index="i" from = "1" to = "#arraylen(getParentNode[1].xmlchildren)#">
		<cfif getParentNode[1].xmlchildren[i].xmlname is childname>
			<cfset arrayDeleteAt(getParentNode[1].xmlchildren,i)>
			<cfbreak>
		</cfif>
	</cfloop>
</cffunction>




<!---
	WAB 2013-04-22
	We seem to get random corruptions of the settings XML document
	I have found that one of the corruptions can be detected by exporting the XML of individual nodes using toString() and then re-parsing and comparing the keys in the exported and original XML
	Once a corrupt node is detected I can delete the node and reload
	Very odd and I don't understand it, but it seems to work
	Result structure a bit of a hack!
 --->

<cffunction name="validateSettingNode" output="false" hint="For use with runFunctionAgainstNodes">
	<cfargument name = "XMLNode">
	<cfargument name = "mode" default="">

	<cfset var numberofXMLChildren = arrayLen(arguments.XMLNode.XMLChildren)>
	<cfset var numberofXMLChildren_new = 0>
	<cfset var xmlAsString = "">
	<cfset var tempXMLDoc = "">
	<cfset var fullvariablename = getFullVariableNameFromNode(xmlNode)>
	<cfset var thisSetting = "">
	<cfset var thismessage = "">
	<cfset var nodeOK = true>
	<cfset var i = 0>
	<cfset var currentSetting = '' />
	<cfset var newSetting = '' />
	<cfset var masterXMLNodeArray = '' />

	<cfif mode is "start">
		<cfif numberofXMLChildren is not 0>
			<!--- we are not in on a leaf node, so export, re-parse and check for the number of children --->
			<cfset xmlAsString = toString (XMLNode)>
			<cfset tempXMLDoc = XMLParse (xmlAsString)>
			<cfset numberofXMLChildren_new = arrayLen(tempXMLDoc.xmlroot.xmlchildren)>
			<cfset nodeOK = true>
			<cfif numberofXMLChildren_new is not NumberOfXMLChildren>
				<cfset nodeOK = false>
			<cfelse>
				<!--- Loop through checking that names of node are what we expect --->
				<cfloop index = "i" from="1" to = "#numberofXMLChildren#">

					<cfif tempXMLDoc.xmlroot.xmlchildren[i].xmlname is not XMLNode.xmlchildren[i].xmlname>
						<cfset nodeOK = false>
						<cfbreak>
					</cfif>
				</cfloop>
			</cfif>


			<cfif NOT nodeOK>
				<cfset currentSetting = getSetting (fullvariablename)>
				<cfset fullvariablename = application.com.settings.getFullVariableNameFromNode (xmlnode)>
				<cfset application.com.xmlfunctions.deleteNode (xmlNode)>
				<cfset application.com.settings.populateSettingXMLNodeByVariableName(variablenames = fullvariablename, updateCluster = false)>
				<cfset newSetting = getSetting (fullvariablename)>

				<cfmail from="serverSupport@relayware.com" to="serverSupport@relayware.com" subject="Setting Corruption Detected #cgi.server_name#" type="html">
					Corruption Detected in setting #fullvariablename#.
					Current Setting: <cfdump var="#currentSetting#">
					New Setting: <cfdump var="#newSetting#">
				</cfmail>

				<cfset arguments.message = arguments.message & "Node for #fullvariablename# deleted and reloaded. <BR>">

			</cfif>
		</cfif>
	<cfelseif mode is "" and not structKeyExists(xmlNOde.xmlAttributes,"dynamicKey")>
		<!--- a leaf node, check value --->
		<cfset masterXMLNodeArray = getCurrentNodeInMasterXML (xmlnode)>
		<cfset thisSetting = getMainSettingValueFromDataBaseAndDefault (variablename = fullvariablename,masterXMLNode = masterXMLNodeArray[1] )>

		<cfif thisSetting.value is not xmlNode.xmltext>
			<cfset currentSetting = xmlNode.xmltext>
			<cfset application.com.settings.populateSettingXMLNodeByVariableName(variablenames = fullVariableName, updateCluster = false)>
			<cfset newSetting = xmlNode.xmltext>
			<cfset thisMessage = "
				Setting #fullVariableName# in memory does not match database<BR>
				Database Value:#thisSetting.value#. <BR>
				Memory Value:#CurrentSetting#.<BR>
				Memory Value After Reload:#NewSetting#.
			">

			<cfif application.testsite is not 2>
				<cfmail from="serverSupport@relayware.com" to="serverSupport@relayware.com" subject="Incorrect Setting Detected in Memory #cgi.server_name#" type="html">
					<cfoutput>
						#thisMessage#
					</cfoutput>
					<cfdump var="#XMLNode#">
					<cfdump var="#thisSetting#">
				</cfmail>
			</cfif>
				<cfset arguments.message = arguments.message & thisMessage & "<BR>">

		</cfif>

	</cfif>

</cffunction>


<cffunction name="validateSettingsXML" output="true">

	<cfreturn validateSettingsXMLByNodeArray (xmlNodeArray = application.settings.settingXML.xmlroot.xmlchildren)>

</cffunction>

<cffunction name="validateSettingsXMLByName" output="true">
	<cfargument name="variableName">

	<cfreturn validateSettingsXMLByNodeArray (xmlNodeArray = getVariableNodeArray_Setting(variablename))>

</cffunction>

<cffunction name="validateSettingsXMLByNodeArray" output="true">
	<cfargument name="XMLNodeArray">
	<cfset var result = {message=""}>

		<cfset runFunctionAgainstNodes(
					XMLNodeArray = xmlnodeArray,
					function = validateSettingNode,
					additionalArgs = result	)>
	<cfreturn result.message>
</cffunction>



<!---
*********************************************
This section to do with editing the variables
*********************************************
 --->
<cffunction name="getRelayFormAttributesFromNode" output="false">
	<cfargument name="XMLNode">

	<cfset var attributesStruct = XMLNode.XMLAttributes>
	<cfset var tempQuery = "">
	<cfset var result = duplicate(attributesStruct)>
	<cfset var attributesToRemove = '' />
	<cfset var item = '' />

		<cfif not structKeyExists(result,"fieldname")>
			<cfset result.fieldname = getFullVariableNameFromNode(xmlnode)>
			<cfset structDelete (result,"name")>
		</cfif>

		<cfset result = applicationScope.com.relayForms.createStandardRelayFormElementAttributeStructure (result, applicationScope)>

		<!--- RemoveTheseAttributes --->
		<cfset attributesToRemove="hassitevariant,migratefrom,description,hascondition,overridden,group,setvariable,mixedcasexmlname,allowedvariants,haspersonvariant">
		<cfloop index="item" list = "#attributesToRemove#">
			<cfset structDelete (result,item)>
		</cfloop>

	<cfreturn result>
</cffunction>



<!--- Outputs a  --->
<cffunction name="outputSetting" output="false">
	<cfargument name = "XMLNode">
	<cfargument name = "level">
	<cfargument name = "mode">

	<cfset var parent = "">
	<cfset var value = "">
	<cfset var title = "">
	<cfset var defaultValue = "">

		<cfif arrayLen(XMLNode.xmlchildren) gt 0>
			<!--- This is a group with children --->
			<cfif mode is "start">
				<cfoutput>
					<tr><td colspan="2">#doIndentHTML(level)#<B><a href="editSetting.cfm?variabletoedit=#parent##iif(parent is not "",de("."),de(""))##XMLNode.XMLName#">#XMLNode.xmlattributes.label#</a></B> </td><td></td><td></td></tr>
					<cfif structKeyExists(XMLNode.xmlattributes,"description")><tr><td colspan=4>#doIndentHTML(level)#&nbsp;&nbsp;#XMLNode.xmlattributes.description#</td></tr></cfif>
				</cfoutput>
				<cfif isAttributeTrue (XMLNode.xmlattributes,"dynamicKey")>
					<cfreturn false>
				</cfif>
			</cfif>

		<cfelse>
			<!--- This is an actual variable--->
			<cfif structKeyExists(XMLNode.xmlattributes,"setvariable")>
				<cfset title = "aka: " & XMLNode.xmlattributes.setvariable>
			<cfelse>
				<cfset title = "">
			</cfif>

			<cfset defaultvalue = resolveValidValues(XMLNode = xmlnode, value = XMLNode.xmlText)>
			<cfset value = resolveValidValues(XMLNode = xmlnode, value = getSetting (variablename = getFullVariableNameFromNode(XMLNode),allVariants = false))>

			<cfif structKeyExists (XMLNode.xmlattributes,"relayFormElementType") and XMLNode.xmlattributes.relayFormElementType is "password">
				<!--- <cfset value = reReplaceNoCase (value,".","*","ALL")> --->
				<cfset value = this.passwordString> <!--- NJH 2012-02-08 - don't give away the length of the password --->
			</cfif>


			<cftry>

				<cfif not isAttributeTrue (XMLNode.xmlattributes,"dynamicKey")>

					<cfoutput>
						<tr>
							<td>#doIndentHTML(level)#</td>
							<td><a href="editSetting.cfm?variabletoedit=#parent##iif(parent is not "",de("."),de(""))##XMLNode.XMLName#" title="#title#">#XMLNode.xmlattributes.label#</a> </td>
							<td >#value# <cfif XMLNode.xmlattributes.overridden>(#defaultValue#)</cfif></td>
						</tr>
						<cfif structKeyExists(XMLNode.xmlattributes,"description")>
							<tr>
								<td></td>
								<td colspan=3>#doIndentHTML(level)#&nbsp;&nbsp;#XMLNode.xmlattributes.description#</td>
							</tr>
						</cfif>

					</cfoutput>
				<cfelse>
								<cfdump var="#xmlnode#"><CF_ABORT>
					<cfreturn false>

				</cfif>

				<cfcatch>
					<cfdump var="#xmlnode#"><CF_ABORT>
				</cfcatch>
			</cftry>

		</cfif>

		<cfreturn true>
</cffunction>


<cffunction name="outputSettingSection" output="true">
	<cfargument name = "XMLNode">
	<cfargument name = "level">
	<cfargument name = "mode" default="">

	<cfif isNodeActive(XmlNode)>
		<CFIF arrayLen(XMLNode.XMLchildren)>
			<cfif mode is "start">
				<cfoutput>
				<li class="#listClassDeclaration(level)#">
					<a href="/admin/settings/editSetting.cfm?variabletoedit=#getFullVariableNameFromNode(xmlnode)#">#xmlnode.xmlattributes.Label#</a>
				</li>
				</cfoutput>
			</cfif>
		</cfif>
		<cfreturn true>
	<cfelse>
		<cfreturn false> <!--- returns false then none of the children get displayed either (which is what we want) --->
	</cfif>



</cffunction>

<cffunction name="isNodeActive" output="false">
	<cfargument name = "XMLNode">

	<cfset var result = true>

	<!--- check if this section is attached to a module and only display if module is active --->
	<cfif structKeyExists (XMLNode.xmlattributes,"Module")>
		<cfset result = applicationScope.com.relayMenu.isModuleActive(XMLNode.xmlattributes.Module)>
	</cfif>

	<cfif result and structKeyExists (XMLNode.xmlattributes,"Display")>
		<!--- check that there isn't a display attribute --->
		<cfset result = evaluateString (XMLNode.xmlattributes.Display)>
	</cfif>

	<cfif result and structKeyExists (XMLNode.xmlattributes,"Active")>
		<!--- check that there isn't a display attribute --->
		<cfset result = evaluateString (XMLNode.xmlattributes.Active)>
	</cfif>

	<cfreturn result>

</cffunction>


<cffunction name="dumpSettingSections" >
	<cfargument name = "section" default = "">

	<cfset var XMLNodeArray = "">

	<cfif section is not "">
		<cfset XMLNodeArray = getVariableNodeArray_master (section)>
	<cfelse>
		<cfset XMLNodeArray = applicationScope.settings.masterXML.settings.xmlchildren>
	</cfif>
	<h2>Edit Settings</h2>
	<ul id="settingsTable1">
	<cfset runFunctionAgainstNodes (XMLNodeArray = XMLNodeArray, function = outputSettingSection)>
	</ul>
</cffunction>


<cffunction name="dumpSettings">
	<cfargument name = "section" default = "">

	<cfset var XMLNodeArray = "">

	<cfif section is not "">
		<cfset XMLNodeArray = getVariableNodeArray_master (section)>
	<cfelse>
		<cfset XMLNodeArray = applicationScope.settings.masterXML.settings.xmlchildren>
	</cfif>


	<TABLE id="settingsTable2">
		<cfoutput><tr><tH></th><tH>Variable</th><tH align="right">Current Value<BR>(Default if Different in brackets)</th></th></tr></cfoutput>

	<cfset runFunctionAgainstNodes (XMLNodeArray = XMLNodeArray, function = outputSetting)>
	</TABLE>
</cffunction>


<!--- if a node has valid values (or a Bit datatype) then we should be able to resolve a data value to its datavalue  --->
<cffunction name="resolveValidValues" output="false">
	<cfargument name = "XMLNode">
	<cfargument name = "value">
		<cfset var result = value>
		<cfset var validValueQuery = "">
		<cfset var tempValueList = "">
		<cfset var getdefault = "">
		<cfset var displayColumnName = "displayValue">
		<cfset var dataColumnName = "dataValue">

		<cfif arguments.value is "">
			<!--- can't resolve a blank value --->
		<cfelseif structKeyExists (xmlNode.xmlattributes,"validValues")>
			<cfset validValueQuery = application.com.relayForms.getValidValuesFromString(xmlNode.xmlattributes.validValues)>
				<cfif applicationScope.com.relayforms.doesColumnNeedQuotes(validvalueQuery,"datavalue")>
					<cfset tempValueList = listQualify(arguments.value,"'")>
				<cfelse>
					<cfset tempValueList = arguments.value>
				</cfif>

				<cfif structKeyExists (xmlNode.xmlattributes,"displayValue") and listFindNoCase(validValueQuery.columnList,xmlNode.xmlattributes.displayValue)>
					<cfset displayColumnName=xmlNode.xmlattributes.displayValue>
				</cfif>
				<cfif structKeyExists (xmlNode.xmlattributes,"dataValue") and listFindNoCase(validValueQuery.columnList,xmlNode.xmlattributes.dataValue)>
					<cfset dataColumnName=xmlNode.xmlattributes.dataValue>
				</cfif>


				<cftry>
					<cfquery name ="getDefault" dbType="Query">
					select #displayColumnName# as displayValue from
					 validvalueQuery
					where #dataColumnName#  in (#preserveSingleQuotes(tempValueList)#)
					</cfquery>
					<cfif getDefault.recordcount>
						<cfset result = valueList(getDefault.displayValue)>
					</cfif>

					<cfcatch>
						<!--- we are just going to have to ignore errors
						value will be returned as result
						--->

					</cfcatch>
				</cftry>

		<cfelseif structKeyExists (XMLNode.xmlattributes,"dataType") and XMLNode.xmlattributes.dataType is "bit">
			<cfset result = iif(value,de("Yes"),de("No"))>
		</cfif>
	<cfreturn result>
</cffunction>

<cffunction name="getEditorRowsForSetting" output="false">
	<cfargument name = "variableName">
	<cfargument name = "getWholeSection" default = "true">
	<cfargument name = "personMode" default = "false">

	<cfset var thisNodeArray = getVariableNodeArray_Master(variableName)>
	<cfset var resultStructure = {string=""}>
	<cfset var additionalArgs = {resultStructure=resultStructure}>
	<cfset var editSectionNode = '' />
	<cfset var startAtNode = '' />
	<cfset var temparray = '' />

	<cfif arrayLen(thisNodeArray)>
		<cfif getWholeSection>
			<!--- We look up the tree until we find a node which has the attribute editSection=true--->
			<cfset editSectionNode = applicationScope.com.xmlfunctions.XMLSearchParents (thisNodeArray[1],"self::node()[@editSection='true']")>
		<cfelse>
			<cfset editSectionNode = thisNodeArray[1]>
		</cfif>


		<cfif arrayLen(editSectionNode)>
			<cfset startAtNode = editSectionNode[1]>
		<cfelse>
			<!--- no edit section.
			If this node has children we can start from this node
			If there are no children then we should start from the parent level
			--->
			<cfif arrayLen(thisNodeArray [1].xmlchildren)>
				<cfset startAtNode = thisNodeArray [1]>
			<cfelse>
				<cfset startAtNode = thisNodeArray [1].xmlparent> <!--- TBD this one is not right --->
			</cfif>
		</cfif>
		<cfset temparray = [startatnode]>

		<cfif not personMode>
			<cfset runFunctionAgainstNodes(
									xmlNodeArray = temparray,
							function = getEditorRowForNode,
							additionalArgs =additionalArgs)>
		<cfelse>
			<cfset additionalArgs.personid = arguments.personid>
			<cfset runFunctionAgainstNodes(
									xmlNodeArray = temparray,
							function = outputPersonEditorRowForNode,
							additionalArgs =additionalArgs	)>

		</cfif>

	</cfif>

	<cfreturn additionalArgs.resultStructure.string>

</cffunction>

<!---
THIS IS THE NEW FUNCTION FOR OUTPUTING SETTINGS!
 --->
<cffunction name="getEditorRowForNode" hint="Given a leaf node, outputs the row (or rows) which are used to edit that variable" output="false">
	<cfargument name="XMLNode" type="xml" required="true">
	<cfargument name="level" type="numeric">
	<cfargument name="mode" default = "">
	<cfargument name="condition" default="" type="string">
	<cfargument name="resultStructure" default="" type="struct" required="true">
	<cfargument name="showVariants" type="boolean" default="true">

	<!--- 	<cfset var fieldname  = XMLNode.xmlattributes.name> --->
	<cfset var attributeCollection = XMLNode.xmlattributes>
	<cfset var newAttributeCollection = "">
	<cfset var reverseLevel = min (1,3-level)>
	<cfset var title = '' />
	<cfset var groupNames = '' />
	<cfset var defaultValue = '' />
	<cfset var getMainSettingValue = '' />
	<cfset var getDefault = '' />
	<cfset var getVariantSettingValue = '' />
	<cfset var valueListWithQuotesIfNecessary = "">
	<cfset var returnValue = true>
	<cfset var ResultHTML = "">
	<cfset var thisResultHTML = "">
	<cfset var newKey_HTML = "">
	<cfset var variableNameDotNotation = '' />
	<cfset var structureKeys = '' />
	<cfset var structureKeyFieldName = '' />
	<cfset var thisVariableName = '' />
	<cfset var childresultHTML = '' />
	<cfset var newKeyFieldName = '' />
	<cfset var dynamicKeyvalidValues = '' />
	<cfset var dynamicKeyvalidValueQuery = '' />
	<cfset var label = '' />
	<cfset var parentvariable = '' />
	<cfset var childXMLNode = '' />
	<cfset var childResult = '' />

	<cfset variableNameDotNotation = getFullVariableNameFromNode(xmlnode)>
	<cfif arrayLen (XMLNode) is 0>
		<cfreturn "">
	<cfelseif not isNodeActive(XMLNode)>
		<cfreturn false> <!--- do not need to continue down this branch --->
	<cfelseif isAttributeTrue (XMLNode.xmlattributes,"dynamicKey")>
			<!---
				This is a Structure.
				So the keys don't appear in the XML,
				They only exist when they are created in the db
				Values of keys controlled by a valid value query (could be extented to allow a key entered by hand)
			--->
				<!--- then we need to see what values of the array there are in the database --->
				<cfif structKeyExists (XMLNode.xmlattributes,"dynamicKeyvalidvalues")>
					<cfset dynamicKeyvalidValues = arguments.XMLNode.xmlattributes.dynamicKeyvalidvalues >
				</cfif>
				<cfset structureKeys = getSettingKeysFromDataBase (getXMLParentList(xmlnode),dynamicKeyvalidvalues)>

			<cfsavecontent variable="thisResultHTML">
				<cfloop query = "StructureKeys">
					<cfif arrayLen(XMLNode.xmlChildren)>
						<cfoutput><tr><td colspan="3">#doIndentHTML(level)##application.com.security.sanitiseHTML(displayvalue)#</td></tr></cfoutput>
						<cfloop index="childXMLNode" array= "#XMLNode.xmlChildren#">
							<cfset structureKeyFieldName = getFormFieldNameForNode(childXMLNode)>
							<cfset thisVariableName = replace(structureKeyFieldName,".#xmlnode.xmlname#",".#datavalue#","ALL")>
							<cfset childresult = getRowsForLeafNode(xmlnode = childxmlNode, level = level + 1,variableNameDotNotation = thisVariableName,showvariants = showvariants)>
							<cfoutput>
								#childresult#
							</cfoutput>
						</cfloop>
					<cfelse>
						<cfset thisVariableName = replace(variableNameDotNotation,".#xmlnode.xmlname#",".#datavalue#","ALL")>
						<cfoutput>#getRowsForLeafNode(label_HTMl=displayvalue, variableNameDotNotation = thisVariableName,xmlnode = xmlNode, level = level,showvariants = showvariants)#</cfoutput>


					</cfif>
				</cfloop>
				<!--- Now we need to output an add link --->
						<cfset newKeyFieldName = "." & getFormFieldNameForNode(XMLNode) & ".">
						<!--- ought to remove the current key values from this valid value drop down--->
						<cfif structKeyExists (XMLNode.xmlattributes,"dynamicKeyvalidvalues")>
							<cfset dynamicKeyvalidValueQuery = application.com.relayForms.getValidValuesFromString(string = dynamicKeyvalidvalues,excludevalues = valuelist(structurekeys.datavalue))>
							<cfset newKey_HTML = application.com.relayForms.validValueDisplay(name=newKeyFieldName,query=dynamicKeyvalidValueQuery,shownull=true,nullText="Add New " & xmlnode.xmlattributes.label)>
						<cfelse>
							<cfset newKey_HTML = 'New #xmlnode.xmlattributes.label#<BR><INPUT TYPE="TEXT" NAME="#newKeyFieldName#" SIZE="15">'>
						</cfif>
					<cfif arrayLen(XMLNode.xmlChildren)>
						<tr>
							<td colspan = "3">
								<cfset newKeyFieldName = "." & getFormFieldNameForNode(XMLNode) & ".">
								<!--- ought to remove the current key values from this valid value drop down--->
								<cfset dynamicKeyvalidValueQuery = application.com.relayForms.getValidValuesFromString(string = XMLNode.xmlattributes.dynamicKeyvalidvalues,excludevalues = valuelist(structurekeys.datavalue))>
								<cfoutput>#doIndentHTML(level)##newKey_HTML#</cfoutput>
							</td>
						</tr>
						<cfloop index="childXMLNode" array= "#XMLNode.xmlChildren#">
							<cfset structureKeyFieldName = getFormFieldNameForNode(childXMLNode)>
							<cfset thisVariableName = replace(structureKeyFieldName,".#xmlnode.xmlname#.",".#newKeyFieldName#.","ALL")>
							<cfset childresultHTML = getRowsForLeafNode(xmlnode = childxmlNode, level = level + 1,variableNameDotNotation = thisVariableName,showvariants = showvariants)>
							<cfoutput>
								#childresultHTML#
							</cfoutput>
						</cfloop>

					<cfelse>
						<!--- no XML children--->
						<cfset structureKeyFieldName = getFormFieldNameForNode(XMLNode)>
						<cfset thisVariableName = replace(structureKeyFieldName,".#xmlnode.xmlname#",".#newKeyFieldName#","ALL")>

						<cfset resultHTML = getRowsForLeafNode(label_html = newKey_HTML,xmlnode = XMLNode, level = level + 1,variableNameDotNotation = "#thisVariableName#",showvariants = showvariants)>
						<cfoutput>#resultHTML#</cfoutput>

					</cfif>


			</cfsavecontent>


			<cfset returnValue = false> <!--- ie don't go further down the tree --->

	<cfelseif arrayLen(xmlnode.xmlchildren)><!--- This is a section --->
		<cfif mode is "start">
			<cfset label = XMLNode.xmlAttributes.label>
			<cfif level is 0 >
				<!--- this is the first item, provide link upwards --->
				<cfset parentvariable = getXMLParentList(xmlnode)>
				<cfif parentvariable  is not "">
					<cfset label = '<a href="editSetting.cfm?variableToEdit=#getXMLParentList(xmlnode)#">#getFullLabelFromNode(xmlnode.xmlparent)#</a>.#getLabelFromNode(xmlnode)#' >
				</cfif>
			<cfelseif structKeyExists(XMLNode.xmlAttributes,"editSection") and XMLNode.xmlAttributes.editSection>
				<!--- This is an editSection then just need to provide a link to edit it --->
				<cfset label = '<a href="editSetting.cfm?variableToEdit=#variableNameDotNotation#">#XMLNode.xmlAttributes.label#</a>'>
				<cfset returnValue = false>
			</cfif>

			<cfsavecontent variable="thisResultHTML">
			<cfoutput>
			<tr>
				<td colspan="3">#doIndentHTML(level)#<h2>#application.com.security.sanitiseHTML(label)#</h2></td>
			</tr>
			<cfif structKeyExists (XMLNode.xmlAttributes,"description")>
				<tr>
					<td colspan=""></td>
					<td colspan="4">#XMLNode.xmlAttributes.description#</td>
				</tr>
			</cfif>
			</cfoutput>
			</cfsavecontent>
		</cfif>
	<cfelse><!--- This is a value --->
		<cfset thisResultHTML = getRowsForLeafNode(xmlnode = xmlNode, level = level,showvariants = showvariants)>
	</cfif>
	<cfset arguments.resultStructure.string = arguments.resultStructure.string & thisResultHTML  >
	<cfreturn returnValue>

</cffunction>

<cffunction name="listClassDeclaration" access="private" output="false" hint="Returns a class declaration to add to li tag on settings page" returntype="string">
	<cfargument name="level" type="numeric" required="true" />
	<cfset var classTag = "" />
	<cfset classTag = "settings-level-#arguments.level#" />
	<cfreturn classTag />
</cffunction>

<cffunction name="doIndentHTML" output="false" returntype="string">
	<cfargument name="level" type="numeric">
	<cfreturn repeatString("",level)>
</cffunction>

<!--- 	2014-09-20 WAB Change so that current value is show in input whether or not it is the default value.
		If value is not the default value then show the default value below
 --->
<cffunction name="getRowsForLeafNode" hint="Given a node, outputs the main value row " output="false">
	<cfargument name="XMLNode" type="xml" required="true">
	<cfargument name="level" type="numeric">
	<cfargument name="showVariants" type="boolean" default="true">
	<cfargument name="variableNameDotNotation" type="string" default="#getFullVariableNameFromNode(xmlnode)#"> <!--- this is usually just getFullVariableNameFromNode(xmlnode), but when dealing with dynamic keys it is passed in  --->
	<cfargument name="label_html" type="string" default="#application.com.security.sanitiseHTML(xmlnode.xmlattributes.label)#"> <!--- this is usually just xmlnode.xmlattributes.label, but when dealing with dynamic keys it can be passed in  --->


		<cfset var readOnlyNode = '' />
		<cfset var defaultvalue = '' />
		<cfset var currentValue = '' />
		<cfset var thisResultHTML = '' />
		<cfset var getMainSettingValue = '' />
		<cfset var fileExists = false>
		<cfset var fileRequired = false>
		<cfset var isDefaultValue = false>

		<!--- Attributes for relayFormElement --->
		<cfset var relayFormAttributes = getRelayFormAttributesFromNode(XMLNode)>
		<cfset relayFormAttributes.fieldname = variableNameDotNotation>
		<!--- Check for any parents which are readonly --->
		<cfset readOnlyNode = applicationScope.com.xmlfunctions.XMLSearchParents (XMLNode.xmlparent,"self::node()[@readonly='true']")>
		<cfif arrayLen(readOnlyNode)>
			<cfset relayFormAttributes.readonly = true>
		</cfif>
		<cfset request.fieldsList  = listAppend(request.fieldsList,relayFormAttributes.fieldname)>
		<cfset defaultvalue = XMLNode.xmlText>
		<cfset defaultvalue_resolved = resolveValidValues(XMLNode = xmlnode, value = defaultvalue)>

		<cfquery name = "getMainSettingValue" datasource = "#applicationScope.sitedatasource#">
			select  Name, Value, condition,sitedefid
			from settings s
			where Name =  <cf_queryparam value="#variableNameDotNotation#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and s.TestSite is null
			and isnull(s.condition,'') = ''
			and isnull(s.siteDefID,'') = ''
			and entityTypeID is null
			and countryid is null
			and entityID is null
		</cfquery>
		<cfset currentValue = getMainSettingValue.Value> <!--- getMainSettingValue.Value--->

		<!---
			If there isn't a current value in the database then we display the default value.
			WAB 2016-04-20 However there is a special case for items where the value in the db is appended to the default value
			We only want to display the value that is in the database
		 --->
		<cfif currentValue is "" and not structKeyExists (XMLNode.xmlattributes,"appendToDefault")>
			<cfset currentValue = defaultValue>
		</cfif>

		<cfif currentValue is defaultValue>
			<cfset isDefaultValue = true>
		</cfif>



		<!--- NJH 2012-02-08 - don't display the password value --->
		<cfif relayFormAttributes.relayFormElementType eq "password">
			<cfset currentValue = this.passwordString>
		</cfif>

		<cfsavecontent variable="thisResultHTML">
			<cfoutput>
				<div class="setting" id="setting_#variableNameDotNotation#">
					<h3 title="#variableNameDotNotation#" class="labeSettings">#label_html#</h3><cfif structKeyExists (XMLNode.xmlattributes,"description")><label class="label01">#XMLNode.xmlattributes.description#</label></cfif>
					<div class="setting-value <cfif not isdefaultValue>setting-value-changed</cfif>" <cfif not isdefaultValue and defaultvalue_resolved is not "">title= "Default: #htmleditformat(defaultvalue_resolved)#"</cfif>>
			</cfoutput>
				<cfif isdefined("XMLNode.XmlAttributes.file")>
					<cfinclude template="#XMLNode.XmlAttributes.file#">
				<cfelse>
					<cfset fileExists = false>
					<cfset fileRequired=false>

					<cfif relayFormAttributes.relayFormElementType eq "file">
						<cfif fileExists(expandPath(currentValue))>
							<cfset fileExists = true>
							<!--- if an image has already been uploaded and the setting is required, then don't make the field required as we currently have a 'value' set--->
							<cfif structKeyExists(relayFormAttributes,"required") and relayFormAttributes.required>
								<cfset relayFormAttributes.required=false>
								<cfset fileRequired=true>
							</cfif>
						</cfif>
					</cfif>
					<cf_relayFormElement attributeCollection = #relayFormAttributes# currentValue = #currentvalue# usecfform=false keeporig = false> <!--- WAB CASE 437330 added keepOrig (needed since a relayFormElement started using relayforms.cfc to display validvalues) --->
					<cf_input type="hidden" name="#relayFormAttributes.fieldName#_orig" value="#currentValue#">
				</cfif>
				<cfoutput>
					</div >
					<cfif structKeyExists (XMLNode.xmlattributes,"units")>(#XMLNode.xmlattributes.units#)</cfif>
					<!--- <cfif not isDefaultValue><p>Default:<cfif defaultvalue is not "">#htmleditformat(defaultvalue_resolved)#<cfelse>null</cfif></p></cfif> --->
					<cfif structKeyExists (XMLNode.xmlattributes,"appendToDefault")>
						<p>Values are <B>Appended</B> to the default list:</p>
						<p>#defaultValue#</p>
					</cfif>
					<cfif structKeyExists(XMLNode.xmlattributes,"appendDelimiter")>
						<p>Delimit items with #XMLNode.xmlattributes.appendDelimiter#</p>
					</cfif>

				</cfoutput>
		</cfsavecontent>
		<!--- Now look for variants (if they are to be displayed in line)--->
		<cfif structKeyExists(xmlnode.xmlattributes,"allowedVariants") >
			<cfif showVariants >
				<cfset thisResultHTML &= getVariantRowsForNode (xmlnode= xmlnode,level = level)>
			<cfelse>
				<cfset thisResultHTML &= '<a onclick="javascript:loadVariantRows (this,''#variableNameDotNotation#'');return false">Show Variants</a>'>
			</cfif>

		</cfif>

		<cfset thisResultHTML &= "</div>" >

	<cfreturn thisResultHTML>

</cffunction>


<cffunction name="getVariantRowsForNode" hint="Given a node, outputs the row (or rows) which are used to edit variants" output="false">
	<cfargument name="XMLNode" type="xml" required="true">
	<cfargument name="level" type="numeric" default=1>

	<cfset var resultHTML = "">
	<cfset var variableNameDotNotation = getFullVariableNameFromNode(xmlnode)>
	<cfset var relayFormAttributeCollection = getRelayFormAttributesFromNode(XMLNode)>
	<cfset var indentStringHTML = "">
	<cfset var getVariantSettingValue = '' />

	<cfset relayFormAttributeCollection.required = false> <!--- variants cannot be required since they need to be deletable --->

	<!--- WAB 2015-05-13 add full support for sitedef variants --->
	<cfquery name = "getVariantSettingValue" datasource = "#applicationScope.sitedatasource#">
		select ID, s.Name, Value, entityTypeID, entityID, condition, s.sitedefid, countryID,
			u.name as userGroupName, testSite, sd.name as siteDefName
		from settings s
			left join userGroup u on u.userGroupID = s.entityID and s.entityTypeID =  <cf_queryparam value="#application.entityTypeID.userGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
			left join siteDef sd on sd.sitedefid = s.sitedefID
		where s.Name =  <cf_queryparam value="#variableNameDotNotation#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and (isnull(s.condition,'') <> ''
			or isnull(s.siteDefID,'') <> ''
			or countryid is not null
			or (entityTypeID =  <cf_queryparam value="#application.entityTypeID.userGroup#" CFSQLTYPE="CF_SQL_INTEGER" >  and isNull(entityID,0) <> 0)
			or s.testSite is not null
			)
	</cfquery>

	<cfset indentStringHTML = doIndentHTML(arguments.level)>

	<cfsaveContent variable="resultHTML">
		<cfoutput>
		<div class="setting-variants-addvariantlink">
		Variants <a href="##" onclick="javascript:loadAddVariantRow(this,'#variableNameDotNotation#')"><img src="/images/icons/add.png" title="Add New Variant"></a>
		</div>
		</cfoutput>

		<cfif getVariantSettingValue.recordCount is not 0>
		<div class="settings-variants">
			<cfloop query = "getVariantSettingValue">
				<cfset local.value = getVariantSettingValue.Value>
				<cfif structKeyExists (XMLNode.xmlattributes,"relayFormElementType") and XMLNode.xmlattributes.relayFormElementType is "password">
					<cfset local.value = this.passwordString>
				</cfif>

				<cfoutput>
				<div class="settings-variant">
					<cfset relayFormAttributeCollection.fieldName = variableNameDotNotation&"|settingID#application.delim1##id#">

					<div class="settings-variant-description">
						<cfif countryID neq "">#indentStringHTML#<b>Country</b>: <cfif countryID neq 0 >#application.countryName[countryID]#<cfelse>All Countries</cfif><br></cfif>
						<cfif userGroupName neq "">#indentStringHTML#<b>UserGroup</b>: #userGroupName#<br></cfif>
						<cfif siteDefID is not "">#indentStringHTML#<b>Site</b>: #siteDefName#</cfif>
						<cfif testsite is not "">#indentStringHTML#<b>Environment</b>: #application.com.relayCurrentSite.getEnvironmentName(testSite = testSite)#</cfif>
					</div>

					<div class="settings-variant-value">
						<cf_relayFormElement attributeCollection = #relayFormAttributeCollection# currentValue = #local.value# usecfform=false>
						<cf_input type="hidden" name="#relayFormAttributeCollection.fieldName#_orig" value="#local.value#">
					</div>
				</div>
				</cfoutput>
			</cfloop>
		</div>
		</cfif>
	</cfsaveContent>

	<cfreturn resultHTML>
</cffunction>


	<cffunction name="getAddVariantRowForNode" output="false">
		<cfargument name="XMLNode" type="string" required="true">

		<cfset var variantSettingDisplay = "">
		<cfset var resultHTML = "">
		<cfset var ID = '' />
		<cfset var getCountries = '' />
		<cfset var getUserGroups = '' />
		<cfset var variableNameDotNotation = getFullVariableNameFromNode(xmlnode)>
		<cfset var relayFormAttributeCollection = getRelayFormAttributesFromNode(XMLNode)>
		<cfset relayFormAttributeCollection.required = false> <!--- variants cannot be required since they need to be deletable --->

		<cfif isVariantAllowed(xmlnode.xmlattributes,"testsite")>
			<cfoutput>
				<cf_querySim>
					getTestSites
					name,ID
					<cfloop collection="#application.EnvironmentLookUp#" item="ID">#application.EnvironmentLookUp[ID].name#|#ID##chr(10)##chr(13)#</cfloop>
				</cf_querySim>
			</cfoutput>
		</cfif>

		<cfif isVariantAllowed(xmlnode.xmlattributes,"country")>
			<cfquery name="getCountries" datasource="#application.siteDataSource#">
				select '0' as countryID, 'All Countries' as countryDescription, 1 as sortIdx
				union
				select countryID, countryDescription, 2 as sortIdx
				from country
				where isoCode is not null
					and countryid not in (select countryid from settings where name =  <cf_queryparam value="#variableNameDotNotation#" CFSQLTYPE="CF_SQL_VARCHAR" > and countryid is not null)
				order by sortIdx,countryDescription
			</cfquery>
		</cfif>

		<cfif isVariantAllowed(xmlnode.xmlattributes,"usergroup")>
			<cfquery name="getUserGroups" datasource="#application.siteDataSource#">
				select '0' as userGroupID, 'All UserGroups' as name, 1 as sortIdx
				union
				select userGroupID, name, 2 as sortIdx from userGroup where userGroupType <> 'Personal'
					and userGroupId not in (select entityID from settings where name =  <cf_queryparam value="#variableNameDotNotation#" CFSQLTYPE="CF_SQL_VARCHAR" >  and entityTypeID =  <cf_queryparam value="#application.entityTypeID.userGroup#" CFSQLTYPE="CF_SQL_INTEGER" > )
				order by sortIdx,name
			</cfquery>
		</cfif>

		<cfif isVariantAllowed(xmlnode.xmlattributes,"sitedef")>
			<cfquery name="getSiteDefs" datasource="#application.siteDataSource#">
				select sitedefID, name, sitedefID as sortIdx from sitedef
					 where sitedefID not in (select sitedefID from settings where name =  <cf_queryparam value="#variableNameDotNotation#" CFSQLTYPE="CF_SQL_VARCHAR" > and sitedefID is not null )
				order by sortIdx
			</cfquery>
		</cfif>

			<cfsavecontent variable = "resultHTML">
			<cfoutput>
				<div class="setting-variants-addvariant" id="#variableNameDotNotation#_AddVariant">
						<cfif isVariantAllowed(xmlnode.xmlattributes,"country")>
							<cf_relayFormElement relayFormElementType="select" fieldname="frmCountry" query="#getCountries#" display="countryDescription" value="countryID" label="" nulltext="Choose Country" shownull=true currentValue="" usecfform=false onChange="javascript:setFieldName(this,'#variableNameDotNotation#_value','countryID');"><br>
						</cfif>
						<cfif isVariantAllowed(xmlnode.xmlattributes,"usergroup")>
							<cf_relayFormElement relayFormElementType="select" fieldname="frmUserGroup" query="#getUserGroups#" display="name" value="userGroupID" label="" nulltext="Choose a usergroup" shownull=true currentValue="" usecfform=false onChange="javascript:setFieldName(this,'#variableNameDotNotation#_value','usergroupID');"><br>
						</cfif>
						<cfif isVariantAllowed(xmlnode.xmlattributes,"testSite")>
							<cf_relayFormElement relayFormElementType="select" fieldname="frmTestSite" query="#getTestSites#" display="name" value="ID" label="" currentValue="#application.testSite#" usecfform=false onChange="javascript:setFieldName(this,'#variableNameDotNotation#_value','testSite');">
						</cfif>
						<cfif isVariantAllowed(xmlnode.xmlattributes,"siteDef") >
							<cf_relayFormElement relayFormElementType="select" fieldname="frmSiteDef" query="#getSiteDefs#" display="name" value="siteDefID" label="" nulltext="Choose Site" shownull=true currentValue="" usecfform=false onChange="javascript:setFieldName(this,'#variableNameDotNotation#_value','siteDefID');">
						</cfif>
					<!--- <td>#application.com.settings.outputEditorField(variableName=variableNameDotNotation,currentValue="")#</td> --->
						<cf_relayFormElement id="#variableNameDotNotation#_value" fieldname="#variableNameDotNotation#|" attributeCollection = #relayFormAttributeCollection# currentValue = "" usecfform=false>
				</div>
			</cfoutput>
			</cfsavecontent>
		<cfreturn resultHTML>
	</cffunction>


<cffunction name="outputPersonEditorRowForNode" hint="Given a node, outputs the row (or rows) which are used to edit that variable" output="false">
	<cfargument name="XMLNode" type="xml" required="true">
	<cfargument name="level" type="numeric">
	<cfargument name="resultStructure" default="" type="struct" required="true">


		<!--- 	<cfset var fieldname  = XMLNode.xmlattributes.name> --->
			<cfset var attributeCollection = XMLNode.xmlattributes>
			<cfset var newAttributeCollection = "">
			<cfset var reverseLevel = min (1,3-level)>
			<cfset var title = '' />
			<cfset var groupNames = '' />
			<cfset var defaultValue = '' />
			<cfset var getMainSettingValue = '' />
			<cfset var getDefault = '' />
			<cfset var getVariantSettingValue = '' />
			<cfset var valueListWithQuotesIfNecessary = "">
			<cfset var thisResultHTML = "">
			<cfset var returnValue = true>
			<cfset var variableNameDotNotation = '' />
			<cfset var label = '' />
			<cfset var parentvariable = '' />
			<cfset var params = '' />
			<cfset var parent = '' />
			<cfset var readOnlyNode = '' />
			<cfset var personVariantQry = '' />
			<cfset var currentValue = '' />
			<cfset var default = '' />
			<cfset var thisResult = '' />

	<cfset variableNameDotNotation = getFullVariableNameFromNode(xmlnode)>
	<cfif arrayLen (XMLNode) is 0>
		<cfreturn "">
	<cfelseif not isNodeActive(XMLNode)>
		<cfreturn false> <!--- do not need to continue down this branch --->
	<cfelseif isAttributeTrue (XMLNode.xmlattributes,"dynamicKey")>
			<!---
				This is a Structure.
				So the keys don't appear in the XML,
				They only exist when they are created in the db
				Values of keys controlled by a valid value query (could be extented to allow a key entered by hand)
			--->
			<cfset thisresult = "dynamic keys to be done here">

			<cfreturn false>

	<cfelseif arrayLen(xmlnode.xmlchildren)>
			<!--- This is a section --->
			<cfset label = XMLNode.xmlAttributes.label>
			<cfif level is 0 >
					<!--- this is the first item, provide link upwards --->
					<cfset parentvariable = getXMLParentList(xmlnode)>
					<cfif parentvariable  is not "">
						<cfset label = '<a href="editSetting.cfm?variableToEdit=#getXMLParentList(xmlnode)#">#getFullLabelFromNode(xmlnode.xmlparent)#</a>.#getLabelFromNode(xmlnode)#' >
					</cfif>
			<cfelseif structKeyExists(XMLNode.xmlAttributes,"editSection") and XMLNode.xmlAttributes.editSection>
				<!--- This is an editSection then just need to provide a link to edit it --->
				<cfset label = '<a href="editSetting.cfm?variableToEdit=#variableNameDotNotation#">#XMLNode.xmlAttributes.label#</a>'>
				<cfset returnValue = false>
			</cfif>

		<cfsavecontent variable="thisResultHTML">
		<cfoutput>
		<tr><td colspan="3">#doIndentHTML(level)#<b>#htmleditformat(label)#</b></td></tr>
					<cfif structKeyExists (XMLNode.xmlAttributes,"description")>
						<tr><td colspan=""></td><td colspan="4">&nbsp;&nbsp;&nbsp;#XMLNode.xmlAttributes.description#</td></tr>
					</cfif>
		</cfoutput>
		</cfsavecontent >


	<cfelse>
		<!--- This is a leaf - ie a value --->
		<!--- First, is this setting which can have person variants --->
		<cfif not isVariantAllowed(xmlnode.xmlattributes,"person")>
			<cfset returnValue = false>
		<cfelse>

			<cfset params = XMLNode.xmlAttributes>
			<cfset parent = getXMLParentList(XMLNode)>
			<!--- Check for any parents which are readonly --->
			<cfset readOnlyNode = applicationScope.com.xmlfunctions.XMLSearchParents (XMLNode.xmlparent,"self::node()[@readonly='true']")>
			<cfif arrayLen(readOnlyNode)>
				<cfset params.readonly = true>
			</cfif>

			<cfset params.name = getFormFieldNameForNode(XMLNode)>  <!--- remove leading underscore --->

			<cfset newattributeCollection = getRelayFormAttributesFromNode(XMLNode)>
			<cfset newattributeCollection.required = false> <!--- variants cannot be required since they need to be deletable --->
			<cfset request.fieldsList  = listAppend(request.fieldsList,newattributeCollection.fieldname)>


			<cfset personVariantQry = getVariant(variableName=variableNameDotNotation,allVariants=false,personVariant=true,personID=arguments.personID)>
			<cfset currentValue = personVariantQry.Value> <!--- getMainSettingValue.Value--->

			<!--- get setting if there were no person variant --->
			<cfset default = getSetting (variableName=variableNameDotNotation,personvariant = false)>

			<cfsavecontent variable="thisResultHTML">
			<cfoutput>
					<tr>
						<td valign="top">#doIndentHTML(level)#</td>
						<td valign="top"><span title="#params.name#" class="label">#htmleditformat(params.label)#</span><cfif structKeyExists (params,"description")><br><i>#htmleditformat(params.description)#</i></cfif></td>
						<td valign="top"><cf_relayFormElement attributeCollection = #newattributeCollection# currentValue = #currentvalue# usecfform=false>
						<cfif structKeyExists (XMLNode.xmlattributes,"units")>(#XMLNode.xmlattributes.units#)</cfif>
						<BR>[#htmleditformat(default)#]
						<cfif isAttributeTrue (XMLNode.xmlattributes,"appendToDefault")>
						<BR>Values are <B>Appended</B> to the default
						</cfif>
						</td>
					</tr>
					</cfoutput>
			</cfsavecontent >

		</cfif>


	</cfif>

		<cfset arguments.resultStructure.string = arguments.resultStructure.string & thisResultHTML  >

		<cfreturn returnValue>


</cffunction>


<cffunction name="getFormFieldNameForNode" output="false">
	<cfargument name="XMLNode" >
	<cfreturn getFullVariableNameFromNode(XMLNode)>

</cffunction>




<cffunction name="ProcessSettingFormUpdate" output="false">
	<cfargument name="updateCluster" default="true">

	<cfset var variableValue = '' />
	<cfset var structKeyVariableReFind = '' />
	<cfset var structKeyvalue = '' />
	<cfset var variable = '' />
	<cfset var variablename = '' />
	<cfset var listOfVariablesToUpdate = '' />
	<cfset var variableToBeUpdatedInMemory = '' />
	<cfset var XMLNodeArray = '' />
	<cfset var argCollection = structNew()>
	<cfset var variantStruct = structNew()>
	<cfset var regExp = '' />
	<cfset var regExpFind = '' />
	<cfset var formField = '' />
	<cfset var variableRoot = '' />
	<cfset var regExpItem = '' />

	<cfset var groups={1="variable",2="variantInfo"}>
	<cfloop index = "variableRoot" list = "#form.SettingFieldList#">
		<!--- Look for any fields with name starting like this --->
		<cfset regExp = "(#variableRoot#)(?:\|(.*?))?(?=,|$)">
			<cfset regExpFind = application.com.regExp.refindalloccurrences(regExp,ListRemoveDuplicates(form.fieldnames),groups)>

		<cfloop array=#regExpFind# index="regExpItem">
			<cfset formField = regExpItem.string>

			<cfif right(formField,5) neq "_orig">
				<cfset variable = regExpItem.variable>
				<cfif regExpItem.variantInfo is not "">
					<cfset variantStruct = application.com.structurefunctions.convertNameValuePairStringToStructure(inputString = regExpItem.variantInfo,delimiter="|",equalssign="#application.delim1#")>
				<cfelse>
					<cfset variantStruct = {}>
				</cfif>

				<cfif left(variable,2) is not "..">   <!--- This is a dynamic key --->
					<cfif structKeyExists (form, formField)>
						<cfset variableValue = form[formfield] >
					<cfelse>
						<cfset variableValue = "">
					</cfif>

					<cfif variable contains "..">
						<!--- this is a new structure item
								need to replace the ..xxxxxxx.. with the value of .xxxxxxxx.
						 --->
						 <cfset structKeyVariableReFind = applicationScope.com.regexp.refindAllOccurrences("\.(\..*\.)(\.|\Z)",variable)>
						<!--- look for value of form variable with this name --->
						<cfset structKeyvalue = form[structKeyVariableReFind[1][2]]>
						<cfif structKeyvalue is not "">
						<!--- replace ..xxxxxxx.. with this value --->
							<cfset variable = replacenocase (variable,structKeyVariableReFind[1][2],structKeyvalue)>
						<cfelse>
							<!--- No item has been chosen from select box, so don't do update --->
							<cfset variable = "">
						</cfif>
					</cfif>

					<cfif variable is not "" and form [variable & "_orig"] NEQ variableValue>
						<cfset argCollection = {variablename = variable, variableValue = variableValue,updateXML = false, oldVariableValue = form [variable & "_orig"]}>
						<cfset structAppend(argCollection,variantStruct)>
						<cfif structKeyExists(form,"setSettingForPersonID")>
							<cfset argCollection.personID = form.setSettingForPersonID>
						</cfif>
						<cfset insertUpdateDeleteSetting(argumentCollection=argCollection)>
						<cfset XMLNodeArray = getVariableNodeArray_Master (variablename = variable, GetParentIfThisNodeDoesNotExist = true)>
						<cfset variableToBeUpdatedInMemory = getFullVariableNameFromNode(XMLNodeArray[1])>
						<cfset listOfVariablesToUpdate = listappend(listOfVariablesToUpdate,variableToBeUpdatedInMemory)>
					</cfif>

				</cfif>
			</cfif>
		</cfloop>
	</cfloop>

	<cfif listOfVariablesToUpdate is not "">
	<!--- now update in memory --->
	<cfset listOfVariablesToUpdate = applicationScope.com.globalfunctions.removeDuplicates(listOfVariablesToUpdate)>
	<cfset populateSettingXMLNodeByVariableName(variablenames = listOfVariablesToUpdate, updateCluster = updateCluster)>
	</cfif>

</cffunction>

		<!--- NOTE can't deal with conditions
			WAB 2015-05-13 Add support for site variants
		--->

<cffunction name="InsertUpdateDeleteSetting" output="true">
		<cfargument name="variableName" required="true" type="string">
		<cfargument name="variableValue" >
		<cfargument name="oldVariableValue" default = "">
		<cfargument name="condition" default="" type="string">
		<cfargument name="personID" default="0" type="numeric">
		<cfargument name="userGroupID" default="0" type="numeric">
		<cfargument name="countryID" default="null" type="string">
		<cfargument name="siteDefID" default="null" type="string">
		<cfargument name="testSite" default="null" type="string">
		<cfargument name="settingID" default="0" type="numeric">
		<cfargument name="updateXML" default = "true" > <!--- can set to false and do the update of the XML yourself - useful if dealing with dynamic keys, because otherwise there is a tendency to repeat the same update lots of times --->
		<cfargument name="updatedByPersonID" default="#request.relayCurrentUser.personid#" type="numeric">


		<cfset var variableNodeArray = getVariableNodeArray_Master (variablename = variablename,GetParentIfThisNodeDoesNotExist = false)>		<!--- The GetParentIfThisNodeDoesNotExist parameter allows us to update variables which include a dynamic key.  It will be a bit inefficient because we will end up updating the settings for every key  under that node, but hope not a problem --->
		<cfset var additionalArgs = "">
		<cfset var deleteSetting = ""/>
		<cfset var updateSetting = ""/>
		<cfset var whereClauseSQL =  ""/>
		<cfset var setForEntity = false/>
		<cfset var entityTypeID = 0>
		<cfset var entityID = 0>
		<cfset var dontUpdate = false>
		<cfset var fileExtension = "">
		<cfset var filename = "">
		<cfset var settingsFiles = "">
		<cfset var filepath = "">
		<cfset var createThumbnailResult = '' />

		<cfif arguments.personID is not 0>
			<cfset entityTypeID = application.entityTypeID.person>
			<cfset entityID = arguments.personID>
			<cfset setForEntity = true>
		</cfif>
		<cfif arguments.userGroupID is not 0>
			<cfset entityTypeID = application.entityTypeID.userGroup>
			<cfset entityID = arguments.userGroupID>
			<cfset setForEntity = true>
		</cfif>

		<cfif isSettingAPassword(arguments.variablename)>

			<cfif arguments.variableValue eq this.passwordString>
				<cfset dontUpdate = true>
			<cfelseif arguments.variableValue neq "">
				<cfset arguments.variableValue = application.com.encryption.encryptWithMasterKey(arguments.variableValue)>
			</cfif>

		<!--- provide support for uploading files in settings... only does thumbnails at the moment.. also need validation that the file is a valid image file... --->
		<cfelseif (arrayLen(variableNodeArray) eq 1 and structKeyExists(variableNodeArray[1].xmlAttributes,"control") and variableNodeArray[1].xmlAttributes.control eq "file")>

			<cfset dontUpdate = true>
			<cfif structKeyExists(variableNodeArray[1].xmlAttributes,"fileExtension")>
				<cfset fileExtension = variableNodeArray[1].xmlAttributes.fileExtension>
			</cfif>
			<cfif structKeyExists(variableNodeArray[1].xmlAttributes,"filename")>
				<cfset filename = variableNodeArray[1].xmlAttributes.filename>
			</cfif>

			<cfset filepath = variableNodeArray[1].xmlAttributes.filepath>

			<cfif form[arguments.variablename] neq "" or structKeyExists(form,arguments.variablename&"_delete")>
				<cfset dontUpdate = false>
				<cfdirectory name="settingsFiles" action="list" directory="#filePath#" filter="#filename#.*">
				<cfloop query="settingsFiles">
					<cffile action="delete" file="#settingsFiles.directory#\#settingsFiles.name#">
				</cfloop>
			</cfif>

			<cfif form[arguments.variablename] neq "">
				<cfset createThumbnailResult = application.com.relayImage.CreateThumbnail(targetSize=80,sourceFile=arguments.variableName,targetFilepath=filepath,targetFileName=filename,targetFileExtension=fileExtension)>
				<cfset arguments.variableValue = createThumbnailResult.imageURL>
			</cfif>
		</cfif>

		<cfsavecontent variable="whereClauseSQL">
			<cfoutput>
			Name =  <cf_queryparam value="#arguments.variablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfif arguments.settingID eq 0>
							and siteDefID <cfif arguments.siteDefID is "null">is null <cfelse> = <cf_queryparam value="#arguments.siteDefID#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
							and testsite  <cfif arguments.testsite is "null">is null <cfelse> = <cf_queryparam value="#arguments.testSite#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
							and isnull(condition,'') =  <cf_queryparam value="#arguments.condition#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and countryID <cfif arguments.countryid is "null">is null <cfelse>= #arguments.countryID#	</cfif>
						<cfif setForEntity>
							and entityTypeId =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
							and entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfelse>
							and isNull(entityTypeID,'') = ''
							and isNull(entityID,'') = ''
						</cfif>
					<cfelse>
						and id = #arguments.settingID#
					</cfif>
			</cfoutput>
		</cfsavecontent>


		<cfif variablename is not "" and not dontUpdate>
			<cfif trim(variableValue) is "">
				<cfquery name = "deleteSetting" datasource = "#applicationScope.sitedatasource#">
				update settings
				set lastupdatedbyperson = #arguments.updatedByPersonID#,
					lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
					lastUpdated = GetDate()
				where
				#preserveSingleQuotes(whereClauseSQL)#

				delete from Settings
				where
				#preserveSingleQuotes(whereClauseSQL)#
				</cfquery>

			<cfelse>
				<!--- 2013-04-24 WAB CASE 434803, problems with nvarchar(max) and cfqueryparam, add useCFVersion = false --->
				<cfquery name = "updateSetting" datasource = "#applicationScope.sitedatasource#">
				if exists (select 1 from settings where #preserveSingleQuotes(whereClauseSQL)#)
					BEGIN
						update settings
						set
							Value =  <cf_queryparam value="#arguments.variableValue#" CFSQLTYPE="CF_SQL_VARCHAR" useCFVersion = false> ,
							-- condition = '#arguments.condition#',
							lastupdatedbyPerson = #arguments.updatedByPersonID#,
							lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
							lastupdated = getdate()
						where
							#preserveSingleQuotes(whereClauseSQL)#
							and Value <>  <cf_queryparam value="#arguments.variableValue#" CFSQLTYPE="CF_SQL_VARCHAR" useCFVersion = false>
					END
				ELSE
					BEGIN
						insert into settings (Name, Value, condition,lastupdatedbyPerson, lastUpdatedBy, lastupdated,countryID,testSite, siteDefID <cfif setForEntity>,entityTypeID,entityID</cfif>)
						values (
								<cf_queryparam value="#variablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
								,<cf_queryparam value="#variableValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
								,<cf_queryparam value="#condition#" CFSQLTYPE="CF_SQL_VARCHAR" >
								,<cf_queryparam value="#arguments.updatedByPersonID#" CFSQLTYPE="cf_sql_integer" >
								,<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >
								,getdate(),<cf_queryparam value="#arguments.countryID#" CFSQLTYPE="cf_sql_integer" >
								,<cf_queryparam value="#arguments.testSite#" CFSQLTYPE="cf_sql_integer" >
								,<cf_queryparam value="#arguments.siteDefID#" CFSQLTYPE="cf_sql_integer" >
								<cfif setForEntity>
									,<cf_queryparam value="#entityTypeID#" CFSQLTYPE="cf_sql_integer" >
									,<cf_queryparam value="#entityID#" CFSQLTYPE="cf_sql_integer" >
								</cfif>)
					END

				</cfquery>


			</cfif>


			<cfset var onSaveMethod = "onSave#replace(variablename,'.','_','ALL')#">

			<!--- NJH 2014/08/13 - call an onSaveMethod if it exists --->
			<cfif arraylen(structFindValue(getMetaData(application.com.settingsFunctions),onSaveMethod))>
				<cfset populateSettingXMLNodeByVariableName(variableNames = variableName, updateCluster = true)> <!--- need to update in memory regardless of value of updateXML, since the onSaveMethod is likely to call getSetting()--->
				<cfinvoke component="#application.com.settingsFunctions#" method="#onSaveMethod#" argumentcollection="#arguments#">
			<cfelseif updateXML>
				<cfset populateSettingXMLNodeByVariableName(variableNames = variableName, updateCluster = true)>
			</cfif>

		</cfif>


</cffunction>



<!---
*********************************************
End of section to do with editing the variables
*********************************************
 --->



<cffunction name="resetCurrentValues">
	<cfargument name="variableName">


	<cfset var xmlsearchResultArray = getVariableNodeArray_Master(variablename)>

		<cfset runFunctionAgainstNodes(
				XMLNodeArray = xmlsearchResultArray,
				function = populateSettingXMLNodeWithCurrentValues)>

</cffunction>


<cffunction name="isAttributeTrue">
	<cfargument name="attributes" type="struct">
	<cfargument name="key" type="string">
	<cfif structKeyExists (attributes,key) and attributes[key]>
		<cfreturn true>
	<cfelse>
		<cfreturn false>
	</cfif>

</cffunction>

<cffunction name="isVariantAllowed">
	<cfargument name="attributes" type="struct">
	<cfargument name="value" type="string">
	<cfif structKeyExists (attributes,"allowedVariants") and listfindnocase(attributes.allowedVariants,value)>
		<cfreturn true>
	<cfelse>
		<cfreturn false>
	</cfif>

</cffunction>


<cffunction name="getSettingForMergeField" access="public" output="false" returnType="string">
	<cfargument name="variablename" type="string" required="true">

	<cfset var result = "">
	<cfset var setting = "">

	<cfif not isSettingAPassword(variableName=arguments.variableName)>
		<cfset setting = getSetting(variableName=arguments.variableName)>
		<cfif not isStruct(setting)>
			<cfset result = setting>
		</cfif>
	</cfif>

	<cfreturn result>

</cffunction>

	<!--- NJH/WAB function to return the default valid values for a node - used in the relay tag editor to get the list of valid value for the embedly plugin method --->
	<cffunction name="getValidValuesForSettings" output="false" validValues="true">
	     <cfargument name="variablename" >

	      <cfset var result = queryNew('dummy')>
	      <cfset var masterXMLNodeArray = getVariableNodeArray_Master(variableName)>
	      <cfset var masterXMLNode = "">

	      <cfif arrayLen(masterXMLNodeArray)>
	      	<cfset masterXMLNode = masterXMLNodeArray[1]  >
	            <cfif structKeyExists (masterXMLNode.xmlattributes,"validValues")>
	            	<cfset result = application.com.relayForms.getValidValuesFromString(masterXMLNode.xmlattributes.validValues)>
	            </cfif>
	      </cfif>
	      <cfreturn result>
	</cffunction>


	<cffunction name="getAllSettingNames" hint="Returns an array of setting names.  Used for random load testing">

		<cfset var resultArray = []>
		<cfscript>
			myFunction = function (xmlNode)  {
				if (not arrayLen(xmlNode.xmlChildren)) {
					arrayAppend(resultArray,getFullVariableNameFromNode(xmlnode));
				};
				return true;
			};

		</cfscript>

		<cfset xmlnodeArray = applicationScope.settings.masterXML.settings.xmlchildren>
		<cfset runFunctionAgainstNodes(
					XMLNodeArray = xmlnodeArray,
					function = myFunction	)>


		<cfreturn resultArray>
	</cffunction>

<!---
	This initialises the settings in memory
	But relies on com.xmlfunction being in memory so will fail during boot up
	Is called when application variables are loaded anyway


	<cfif isDefined("application.com")>
		<cftry>
			<cfset loadVariablesIntoApplicationScope ()>
			<cfcatch>
			</cfcatch>

		</cftry>
	</cfif>
--->

	<cffunction name="initialise">
		<cfargument name="applicationScope" default="#application#">

		<!--- DO NOT VAR THESE VARIABLES --->
		<cfset variables.applicationScope = arguments.applicationScope>
		<cfset variables.lockname = variables.applicationScope.applicationName & "_SettingsLock">


	</cffunction>


	<!--- We want to use the runFunctionAgainstNodes function in XMLFunctions
		however if we call it as applicationScope.com.xmlfunctions.runFunctionAgainstNodes we seem to get scoping issues because some of the functions we use are in this cfc
	 --->
	<cfset variables.xmlFunctions = createObject("component","relay.com.xmlfunctions")>
	<cfset variables.runFunctionAgainstNodes = xmlFunctions.runFunctionAgainstNodes>



</cfcomponent>





