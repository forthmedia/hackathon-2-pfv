<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relaySOD.cfc	
Author:				NJH
Date started:		
	
Description:		SOD functions

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code		What was changed
2009/12/03			NJH			P-LEX041	Added parameter that, if set to true, will expire the points if the account that has just
											been given points is deleted while processing SOD claims.

2010/03/09 			PPB 			TND096 		TREND send the DistributorNo(ie NavisionID) thro to R1DistiID --->
Possible enhancements:
 --->

<cfcomponent displayname="Relay SOD" hint="Provides functions used for SOD.">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getSODSources" hint="Gets SOD sources" returnType="query">
		
		<cfscript>
			var SODSourceQry = "";
		</cfscript>
		
		<cfquery name="SODSourceQry" datasource="#application.siteDataSource#">
			SELECT sourceID, description AS source FROM SODsource
		</cfquery>

		<cfreturn SODSourceQry>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getSourceDetails" hint="Gets details for a SOD source" returnType="query">
		<cfargument name="sourceID" type="numeric" required="Yes">
		
		<cfscript>
			var SourceDetailsQry = "";
		</cfscript>
		
		<cfquery name="SourceDetailsQry" datasource="#application.siteDataSource#">
			SELECT * FROM SODsource WHERE sourceID = #arguments.sourceID#
		</cfquery>

		<cfreturn SourceDetailsQry>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="validateDataLoadTable" hint="Validates that a dataload table can be loaded" returnType="boolean">
		<cfargument name="sourceID" type="numeric" required="Yes">
		
		<cfscript>
			var SourceDetailsQry = getSourceDetails(sourceID=arguments.sourceID);
		</cfscript>
		
		<cfquery name="isDataValid" datasource="#application.siteDataSource#">
			SELECT * 
			FROM #SourceDetailsQry.dataSource#
			WHERE 
				#SourceDetailsQry.location# IS NULL
				OR #SourceDetailsQry.product# IS NULL
				OR#SourceDetailsQry.quantity# IS NULL
		</cfquery>
		
		<cfif isDataValid.recordCount>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addControlColumns" hint="Add control columns on the dataload table">
		<cfargument name="dataloadTable" type="string" required="Yes">
		
		<cfscript>
			var addColumns = "";
		</cfscript>
		
		<cfquery name="addColumns" datasource="#application.siteDataSource#">
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='sourceID')
				ALTER TABLE #arguments.dataloadTable# add sourceID int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='countryID')
				ALTER TABLE #arguments.dataloadTable# add countryID int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='locationID')
				ALTER TABLE #arguments.dataloadTable# add locationID int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='productID')
				ALTER TABLE #arguments.dataloadTable# add productID int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='rowID')
				ALTER TABLE #arguments.dataloadTable# add rowID int IDENTITY (1, 1) NOT NULL
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='points')
				ALTER TABLE #arguments.dataloadTable# add points int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='RWCompanyAccountID')
				ALTER TABLE #arguments.dataloadTable# add RWCompanyAccountID int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='RWPersonAccountID')
				ALTER TABLE #arguments.dataloadTable# add RWPersonAccountID int null
				
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#arguments.dataloadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='matchDate')
				ALTER TABLE #arguments.dataloadTable# add matchDate datetime null
		</cfquery>

	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateControlColumns" hint="Updates the control columns in the dataload table">
		<cfargument name="sourceID" type="numeric" required="Yes">
		<cfargument name="tableName" type="string" required="No" default="SODMaster">
		<cfargument name="campaignID" type="numeric" required="No" default=4>
		<cfargument name="batchDate" type="date" required="false">  <!--- NJH 2008/03/10 added batch date to filter the data being updated to help speed up the queries. --->
		<cfargument name="startInvoiceDate" type="date" required="false"> <!--- NJH 2008/10/10 added invoice date range to help filter matching --->
		<cfargument name="endInvoiceDate" type="date" required="false">
		
		<cfscript>
			var updateColumns = "";
			var SourceDetailsQry = getSourceDetails(sourceID=arguments.sourceID);
			var additionalWhereClause = "";
		</cfscript>
		
		<!--- This is used to reduce the number of rows that we're trying to match --->
		<cfif isDefined("arguments.startInvoiceDate") and isDefined("arguments.endInvoiceDate")>
			<cfset additionalWhereClause = "and d.invoiceDate >= #createODBCDateTime(arguments.startInvoiceDate)# and d.invoiceDate <= #createODBCDateTime(arguments.endInvoiceDate)# ">
		</cfif>
		<cfif isDefined("arguments.batchDate")>
			<cfset additionalWhereClause = additionalWhereClause & "and d.created = #createODBCDateTime(arguments.batchDate)#">
		</cfif>
		
		<!--- <cfif arguments.tablename eq "SODMaster"> --->
		
			<!--- update the SOD Master table - used when reprocessing records--->
			<cfquery name="updateColumns" datasource="#application.siteDataSource#">
				UPDATE d set d.countryID = c.countryID
				FROM
					#arguments.tablename# d WITH (NOLOCK)
					INNER JOIN countryAlternatives ca WITH (NOLOCK) ON d.country = ca.alternativeCountryDescription 
					INNER JOIN Country c WITH (NOLOCK) ON ca.relayCountryDescription = c.countryDescription
				WHERE d.CountryID IS NULL
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
				
				
				UPDATE d set locationID = sll.locationID 
				FROM #arguments.tablename# d WITH (NOLOCK)
					INNER JOIN SodLoadLocationLookup sll WITH (NOLOCK) ON sll.thirdPartyForeignKey = d.thirdPartyForeignKey
				WHERE d.locationID IS NULL
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
				
								
				UPDATE d set locationID = slvl.locationID 
				FROM #arguments.tablename# d WITH (NOLOCK)
					INNER JOIN SODloadVatLookup slvl WITH (NOLOCK) ON slvl.thirdPartyVatNumber = d.VatNumber
				WHERE d.locationID IS NULL
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
				
				
				UPDATE d set locationID = slsl.locationID 
				FROM #arguments.tablename# d WITH (NOLOCK)
					INNER JOIN SODloadSiteNameLookup slsl WITH (NOLOCK) ON slsl.thirdPartySiteName = d.resellerName
				WHERE d.locationID IS NULL
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
				
				
				UPDATE d set productID = p.productID 
				FROM #arguments.tablename# d WITH (NOLOCK)
					INNER JOIN product p WITH (NOLOCK) ON p.SKU = d.productCode 
                    <!--- and d.countryID = p.countryID not used anymore --->
				WHERE d.productID IS NULL
					AND p.deleted = 0
					AND p.campaignID=#arguments.campaignID#
					AND p.accrualMethod = 'a'
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
					
				<!--- NJH 2008/12/17 CR-SNY661 calculate points based on original transaction (if it exists) --->
				UPDATE d set points = 
					( case when abs(sm.points/sm.quantity) <> p.discountPrice then abs(sm.points/sm.quantity) else p.discountPrice end * 
						d.quantity * 
						-- NJH 2008/04/14 Bug Fix Sony Issue 82
						case when (d.orderValue < 0 and d.quantity > 0) then -1 when (d.orderValue = 0 or d.quantity = 0) then 0 else 1 end
					)
				FROM #arguments.tablename# d WITH (NOLOCK)
					INNER JOIN product p WITH (NOLOCK) ON p.productID = d.productID
					LEFT OUTER JOIN SODMaster sm WITH (NOLOCK) on d.foreignTransactionID = sm.foreignTransactionID and sm.created = (select min(created) from SODMaster sm1 WITH (NOLOCK) where sm1.foreignTransactionID = d.foreignTransactionID and sm1.productID = d.productID and sm1.points <> 0 and sm1.matchDate is not null)
				WHERE d.points IS NULL
					AND d.quantity IS NOT NULL
					AND p.deleted = 0
					AND p.campaignID=#arguments.campaignID#
					<cfif arguments.tablename eq "SODMaster">
						#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
				
				<!--- UPDATE d set points = 
					(p.discountPrice * 
						d.quantity * 
						-- NJH 2008/04/14 Bug Fix Sony Issue 82
						case when (d.orderValue < 0 and d.quantity > 0) then -1 when (d.orderValue = 0 or d.quantity = 0) then 0 else 1 end
					) 
				FROM SODMaster d WITH (NOLOCK)
					INNER JOIN product p WITH (NOLOCK) ON p.productID = d.productID
				WHERE d.points IS NULL
					AND d.quantity IS NOT NULL
					AND p.deleted = 0
					AND p.campaignID=#arguments.campaignID#
					#PreserveSingleQuotes(additionalWhereClause)# --->
				
				
				UPDATE d set RWCompanyAccountID = ca.accountID 
				FROM rwCompanyAccount ca WITH (NOLOCK)
					INNER JOIN location l WITH (NOLOCK) ON l.organisationID = ca.organisationID 
					INNER JOIN #arguments.tablename# d WITH (NOLOCK) ON d.locationID = l.locationID
				WHERE d.matchDate IS NULL
					AND accountDeleted = 0
					AND testAccount = 0
					AND ca.accountID IN
					(SELECT accountID FROM rwPersonAccount WITH (NOLOCK) WHERE testAccount = 0 AND accountDeleted = 0
						GROUP BY accountID
						HAVING COUNT(distinct personID) = 1)
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
				
				<!--- update those records for which we only have one person account per company account--->
				UPDATE d SET rwPersonAccountID = pa.rwPersonAccountID 
				FROM rwPersonAccount pa WITH (NOLOCK)
					INNER JOIN #arguments.tablename# d WITH (NOLOCK) ON d.rwCompanyAccountID = pa.accountID
				WHERE d.matchDate IS NULL
					AND pa.accountID IN
					(SELECT accountID FROM rwPersonAccount WITH (NOLOCK) WHERE testAccount = 0 AND accountDeleted = 0
						GROUP BY accountID
						HAVING COUNT(distinct personID) = 1)
					AND pa.testAccount = 0 and pa.accountDeleted=0
					<cfif arguments.tablename eq "SODMaster">
					#PreserveSingleQuotes(additionalWhereClause)#
					</cfif>
			</cfquery>
		
		
		<!--- <cfelse>
		
			<!--- updating the dataload table - used prior to loading intial data into SODmaster --->
			<cfquery name="updateColumns" datasource="#application.siteDataSource#">
				UPDATE #arguments.tablename# 
				SET sourceID = #arguments.sourceID# 
				WHERE sourceID IS NULL
				
				UPDATE d 
				SET d.countryID = c.countryID
				FROM
					#arguments.tableName# d 
					INNER JOIN countryAlternatives ca ON d.#SourceDetailsQry.countryID# = ca.alternativeCountryDescription 
					INNER JOIN Country c ON ca.relayCountryDescription = c.countryDescription
				WHERE d.CountryID IS NULL
				
				UPDATE d
				SET locationID = sll.locationID
				FROM 
					#arguments.tablename# d
					INNER JOIN SodLoadLocationLookup sll ON sll.thirdPartyForeignKey = cast(d.#SourceDetailsQry.location# as varchar)
				WHERE d.locationID IS NULL
				
				UPDATE d set locationID = slvl.locationID FROM #arguments.tablename# d
					INNER JOIN SODloadVatLookup slvl ON slvl.thirdPartyVatNumber = d.#SourceDetailsQry.VatNumber#
				WHERE d.locationID IS NULL
				
				UPDATE d set locationID = slsl.locationID FROM #arguments.tablename# d
					INNER JOIN SODloadSiteNameLookup slsl ON slsl.thirdPartySiteName = d.#SourceDetailsQry.resellerName#
				WHERE d.locationID IS NULL
				
				UPDATE d set productID = p.productID FROM #arguments.tableName# d
					INNER JOIN product p ON p.SKU = d.#SourceDetailsQry.product# and d.#SourceDetailsQry.countryID# = p.countryID
				WHERE d.productID IS NULL
					AND p.deleted = 0
					AND p.campaignID = #arguments.campaignID#
					AND p.accrualMethod = 'a'
				
				UPDATE d set points = 
					(discountPrice * 
						d.#SourceDetailsQry.quantity# * 
						--case when d.#SourceDetailsQry.orderValue# < 0 when d.#SourceDetailsQry.orderValue# = 0 then 0 then -1 else 1 end
						case when (d.#SourceDetailsQry.orderValue# < 0 and d.#SourceDetailsQry.quantity# > 0) then -1 when (d.#SourceDetailsQry.orderValue# = 0 or d.#SourceDetailsQry.quantity# = 0) then 0 else 1 end
					)	
				FROM #arguments.tableName# d
					INNER JOIN product p ON p.productID = d.productID
				WHERE d.points IS NULL
					AND d.#SourceDetailsQry.quantity# is not null
					AND p.deleted = 0
					AND p.campaignID = #arguments.campaignID#
					AND p.accrualMethod = 'a'
			
				UPDATE d set RWCompanyAccountID = ca.accountID FROM rwCompanyAccount ca 
					INNER JOIN location l ON l.organisationID = ca.organisationID 
					INNER JOIN #arguments.tableName# d ON d.locationID = l.locationID
				WHERE d.matchDate IS NULL
					AND accountDeleted = 0
					AND testAccount = 0
					AND ca.accountID IN
					(SELECT accountID FROM rwPersonAccount WHERE testAccount = 0 AND accountDeleted = 0
						GROUP BY accountID
						HAVING COUNT(distinct personID) = 1)
				
				<!--- update those records for which we only have one person per company account--->
				UPDATE d
				SET
					rwPersonAccountID = pa.rwPersonAccountID
				FROM
					rwPersonAccount pa 
					INNER JOIN #arguments.tableName# d ON d.rwCompanyAccountID = pa.accountID
				WHERE d.matchDate IS NULL
					AND pa.accountID IN
					(	SELECT accountID FROM rwPersonAccount WHERE testAccount=0 AND accountDeleted = 0
						GROUP BY accountID
						HAVING COUNT(distinct personID) = 1
					)
					AND pa.testAccount = 0
					AND pa.accountDeleted = 0
			</cfquery>
		
		</cfif> --->
		
		<cfquery name="setMatchDate" datasource="#application.siteDataSource#">
			UPDATE #arguments.tableName#
			SET 
				matchDate = #createODBCDateTime(request.requestTime)#
				<cfif arguments.tablename eq "SODMaster">
					,lastUpdatedBy = #request.relayCurrentUser.personID#,
					lastUpdated = #createODBCDateTime(request.requestTime)#
				</cfif>
			WHERE
				locationID  IS NOT NULL
				AND productID  IS NOT NULL
				AND rwCompanyAccountID  IS NOT NULL
				AND rwPersonAccountID  IS NOT NULL
				AND points  IS NOT NULL
				AND matchDate IS NULL
				<cfif arguments.tablename eq "SODMaster" and isDefined("arguments.batchDate")>
					AND created = #arguments.batchDate#
				</cfif>
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="moveToSODMaster" hint="Appends records from SOD dataload to SOD master">
		<cfargument name="sourceID" type="numeric" required="Yes">
		<cfargument name="dataLoadTable" type="string" required="Yes">
		<cfargument name="batchDate" type="date">
		<cfargument name="useUniqueIdentifier" type="boolean" default="false"> 	<!--- NJH 2008/01/16 Sony needs this column, but we can't insert a null into it. So, if not using it, don't select it from the table/view into SODmaster --->
		<cfargument name="useR1DistiID" type="boolean" default="false"> 		<!--- PPB 2010/03/09 TND096 TREND send the DistributorNo(ie NavisionID) thro to R1DistiID --->
		
		<cfscript>
			var insertIntoSODMaster = "";
			var SourceDetailsQry = getSourceDetails(sourceID=arguments.sourceID);
		</cfscript>
		
		<!---
		2008-02-21 Michael Roberts
			Added new field 'NoOfUsers'
			NOTE: Translation table used for select field has also been changed.
			This has been done due to customer requirement 'P-TND065 1.1.2'
			
		2010/03/09 PPB TND096 added R1DistiID
		--->
		<cfquery name="insertIntoSODMaster" datasource="#application.siteDataSource#">
			INSERT INTO SODMaster
			(	sourceID,
				batchRowID,
				invoiceNumber,
				invoiceDate,
				quantity,
				NoOfUsers,
				points,
				orderValue,
				orderType,
				productCode,
				productID,
				locationID,
				thirdPartyForeignKey,
				resellerName,
				RWCompanyAccountID,
				RWPersonAccountID,
				EndUserCompanyName,
				VatNumber,
				country,
				countryID,
				matchDate,
				<cfif useUniqueIdentifier>foreignTransactionID,</cfif>
				<cfif useR1DistiID>R1DistiID,</cfif>
				createdBy,
				created,
				lastUpdatedBy,
				lastUpdated
			)
			SELECT
				sourceID,
				rowID,
				<cf_queryparam value="#SourceDetailsQry.invoiceNumber#" CFSQLTYPE="cf_sql_varchar" >,
				<cf_queryparam value="#SourceDetailsQry.invoiceDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#SourceDetailsQry.quantity#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#SourceDetailsQry.NoOfUsers#" CFSQLTYPE="cf_sql_integer" >,
				points,
				<cf_queryparam value="#SourceDetailsQry.orderValue#" CFSQLTYPE="cf_sql_numeric" >,
				<cf_queryparam value="#SourceDetailsQry.orderType#" CFSQLTYPE="cf_sql_varchar" >,
				<cf_queryparam value="#SourceDetailsQry.product#" CFSQLTYPE="cf_sql_varchar" >,
				productID,
				locationID,
				<cf_queryparam value="#SourceDetailsQry.location#" CFSQLTYPE="cf_sql_varchar" >,
				<cf_queryparam value="#SourceDetailsQry.resellerName#" CFSQLTYPE="CF_SQL_varchar" >,
				rwCompanyAccountID,
				rwPersonAccountID,
				<cf_queryparam value="#SourceDetailsQry.EndUserCompanyName#" CFSQLTYPE="cf_sql_varchar" >,
				<cf_queryparam value="#SourceDetailsQry.VatNumber#" CFSQLTYPE="cf_sql_varchar" >,
				<cf_queryparam value="#SourceDetailsQry.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				countryID,
				matchDate,
				<cfif useUniqueIdentifier><cf_queryparam value="#SourceDetailsQry.transactionID#" CFSQLTYPE="cf_sql_integer" >,</cfif>
				<cfif useR1DistiID><cf_queryparam value="#SourceDetailsQry.R1DistiID#" CFSQLTYPE="cf_sql_integer" >,</cfif>
				<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#arguments.batchDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#arguments.batchDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			FROM #arguments.dataLoadTable#
		</cfquery>
		
		<cfif useUniqueIdentifier>
			<!--- insert dummy transactions into SODMaster for transactions where the resellers has been modified --->
			<cfscript>
				application.com.relaySOD.insertDummyTransIntoSODMaster(batchDate=arguments.batchDate);
			</cfscript>
		</cfif>

	</cffunction>
	
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertDummyTransIntoSODMaster" hint="">
		<cfargument name="batchDate" type="date" required="true">
		
		<!--- When a known transaction comes through (ie. where we have been given a foreign transaction ID) and the reseller
			in that transaction has changed, we need to find the latest transaction that was matched belonging to the old
			reseller, so that we can remove the points from the old reseller and give it to the new reseller. We are creating a dummy
			transaction into SODMaster that will reflect this. We set the created date of this transaction to be slightly older than the
			transaction just inserted belonging to the new reseller so that the next time a bunch of transactions are loaded, we don't 
			think that the reseller has changed. 
			
			NJH 2009/02/25 Bug Fix Issue Sony 1895 - changed the created date inserted for the dummy transactions to be slightly newer than the created date of the last transaction
				that it relates to where the transaction is not already null. #fudgedBatchDate#	was formerly used, but primary key constraints
				were violated as we attempted to load two transactions with the same sourceID, batchRowID and created date. This was possible
				because there were transactions that need modifying in SODmaster that happened to have the same batchRowID. lastUpdated is now the handle
				for these records.
		--->
		<cfset fudgedBatchDate = createODBCdateTime(arguments.batchDate-.0001)>
	
		<cfquery name="insertDummyTransactions" datasource="#application.siteDataSource#">
			INSERT INTO SODMaster
			(	sourceID,
				batchRowID,
				invoiceNumber,
				invoiceDate,
				quantity,
				NoOfUsers,
				points,
				orderValue,
				orderType,
				productCode,
				productID,
				locationID,
				thirdPartyForeignKey,
				resellerName,
				RWCompanyAccountID,
				RWPersonAccountID,
				EndUserCompanyName,
				VatNumber,
				country,
				countryID,
				matchDate,
				foreignTransactionID,
				createdBy,
				created,
				lastUpdatedBy,
				lastUpdated
			)
			SELECT
				sm.sourceID,
				sm.batchRowID,
				sm.invoiceNumber,
				sm.invoiceDate,
				sm.quantity,
				sm.NoOfUsers,
				0,
				sm.orderValue,
				sm.orderType,
				sm.productCode,
				sm.productID,
				sm.locationID,
				sm.thirdPartyForeignKey,
				sm.resellerName,
				sm.RWCompanyAccountID,
				sm.RWPersonAccountID,
				sm.EndUserCompanyName,
				sm.VatNumber,
				sm.country,
				sm.countryID,
				<cf_queryparam value="#fudgedBatchDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				sm.foreignTransactionID,
				4,
				<!--- NJH 2009/02/25 Bug Fix Issue Sony 1895 - changed created from #fudgedBatchDate# to the query below --->
				(select max(created)+.0001 from SODMaster sm2 where sm2.foreignTransactionID = sm.foreignTransactionID and sm2.matchDate is not null and created < #arguments.batchDate#),
				4,
				#fudgedBatchDate#
			FROM SODMaster sm WITH (NOLOCK) INNER JOIN SodMaster sm1 on sm1.foreignTransactionID = sm.foreignTransactionID and sm1.thirdPartyForeignKey <> sm.thirdPartyForeignKey 
				and sm1.created = #arguments.batchDate# and sm.created = (select max(created) from SODMaster sm2 where sm2.foreignTransactionID = sm.foreignTransactionID and sm2.matchDate is not null and created < #arguments.batchDate#)
				<!--- don't insert any records that are already sitting in SOD Claim --->
			LEFT JOIN SODClaim sc WITH (NOLOCK) ON sm.created = sc.batchDate
					AND sm.sourceID = sc.sourceID
					AND sm.batchRowID = sc.batchRowID
			WHERE
				<!--- NJH/AJC 2008-03-14 Seemed to be excluding the dummy trans from inserting    sc.sourceID IS NULL
				AND  --->sm.matchDate  IS NOT NULL
				AND sm.foreignTransactionID IS NOT NULL
				AND sm.sourceID = 1
			ORDER By sm.created desc
		</cfquery>
		
		<!--- move the records we have just created into SOD claim. We can do this because we know that the reseller has been matched
			because the points that we're removing from the old reseller is based on the last matched record of theirs. 
		
			NJH 2009/02/25 Bug Fix Issue Sony 1895 - added batchDateColumn to function below. lastUpdated is now the handle for these dummy transactions which moveMatchedRecordsToSODClaim will use.
		--->
		<cfscript>
			application.com.relaySOD.moveMatchedRecordsToSODClaim(batchDate=fudgedBatchDate,reprocessedRecords=false,ignorePointsRecalculation=false,batchDateColumn="lastUpdated");
		</cfscript>
		
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="moveMatchedRecordsToSODClaim" hint="Populates the SOD claim table with unprocessed and matching records">
		<cfargument name="sourceID" type="numeric" required="No">
		<cfargument name="batchDate" type="date" required="No">
		<cfargument name="reprocessedRecords" type="boolean" default="true">
		<cfargument name="ignorePointsRecalculation" type="boolean" default="false"> <!--- NJH an override switch that tells us to just put the points into SODClaim as they appear in SODMaster, rather than trying to calculate points differences --->
		<cfargument name="batchDateColumn" type="string" default="created">
		
		<!--- NJH 2009/02/25 Bug Fix Issue Sony 1895- added batchDateColumn argument as the handle for a batch can now be either the created date or the lastUpdated date.
			In most cases, it will be the created date, but in the case of dummy transactions where we are doing points recalculation based on a foreignTransactionID,
			it will be the lastUpdated date. This is because we couldn't created dummy transations with the same created date as there were times
			when the dummy transactions contained the same batchRowID.
			For example. trans 123 is loaded on day 1 with a batchRowID of 1
						 trans ABC is loaded on day 2 with a batchRowID of 1
						 In a new load, we need to create dummy transactions for both 123 and ABC as they have changed. We now have to set the created
						 dates from each other to enforce the primary key in SODMaster of sourceID, batchRowID and created as sourceID and batchRowID are the same.
		 --->
		
		<!---
			2008-02-21 Michael Roberts
				Added new field 'NoOfUsers'
		--->
		<cfquery name="populateSODclaim" datasource="#application.siteDataSource#">
			INSERT INTO SODclaim
			(	sourceID,
				batchRowID,
				batchDate,
				organisationID,
				invoiceNumber,
				invoiceDate,
				personID,
				productID,
				quantity,
				NoOfUsers,
				endUserCompanyName,
				rwCompanyAccountID,
				points,
				rwPromotionID
			)
			SELECT
				sm.sourceID,
				sm.batchRowID,
				sm.created,
				l.organisationID, 
				invoiceNumber,
				invoiceDate, 
				pa.personID,
				productID, 
				quantity,
				NoOfUsers,
				EndUserCompanyName,
				rwCompanyAccountID,
				<cfif arguments.ignorePointsRecalculation>
				points,
				<cfelse>
				<!--- AJC/NJH 2008/02/25 CR-SNY631 points adjustment based on an existing transaction --->
				points-isnull((select top 1 points from SODMaster sm2 where sm2.created < sm.created and sm2.foreignTransactionID=sm.foreignTransactionID and sm2.thirdPartyForeignKey = sm.thirdPartyForeignKey and sm2.matchDate is not null order by created desc),0) as points,
				</cfif>
				<cfif isdefined("request.SODPointsAdjustmentPromoID") and not arguments.ignorePointsRecalculation>	
					case when isnull(convert(varchar,(select top 1 points from SODMaster sm2 where sm2.created < sm.created and sm2.foreignTransactionID=sm.foreignTransactionID and sm2.thirdPartyForeignKey = sm.thirdPartyForeignKey and sm2.matchDate is not null order by created desc)),'isNew') = 'isNew' then null else #request.SODPointsAdjustmentPromoID# END as rwPromotionID
				<cfelse>
					null
				</cfif>
			FROM   
				SODMaster sm WITH (NOLOCK) INNER JOIN location l WITH (NOLOCK) ON sm.locationID = l.locationID
				INNER JOIN rwPersonAccount pa WITH (NOLOCK) ON pa.rwPersonAccountID = sm.rwPersonAccountID
				<!--- if we're reprocessing records, only move into SOD claim records that are in the failures table --->
				<cfif arguments.reprocessedRecords>
					INNER JOIN SODFailure sf WITH (NOLOCK) ON sm.created = sf.batchDate
					AND sm.sourceID = sf.sourceID
					AND sm.batchRowID = sf.batchRowID
				</cfif>
			WHERE
				sm.processDate IS NULL
				AND sm.matchDate  IS NOT NULL
				<!--- AJC/NJH 2008/02/25 CR-SNY631 we don't want to process old records that have just been matched if a newer version exists. This is only relevant where the client is giving us a transactionID --->
				AND (sm.foreignTransactionID is null or sm.foreignTransactionID not in
					(select foreignTransactionID from SODMaster sm3 with (noLock)
					 where sm3.foreignTransactionID = sm.foreignTransactionID and sm3.created > sm.created and sm3.matchDate is not null))
				  <cfif isDefined("arguments.batchDate")>AND sm.#arguments.batchDateColumn# = #arguments.batchDate#</cfif> <!--- NJH 2009/02/25 - changed sm.created to sm.#arguments.batchDateColumn# --->
				  <cfif isDefined("arguments.sourceID")>AND sm.sourceID = #arguments.sourceID#</cfif>
			ORDER BY sm.created
			/*GROUP BY
				sm.sourceID,
				sm.batchRowID,
				sm.created,
				l.organisationID,
				invoiceNumber,
				invoiceDate,
				pa.personID,
				productID,
				EndUserCompanyName,
				rwCompanyAccountID*/
		</cfquery>
		
		<!--- remove from the SOD failures table if we're moving matched re-processed records into SOD claims--->
		<cfif arguments.reprocessedRecords>
			<cfquery name="removeProcessedRecordsFromSODFailure" datasource="#application.siteDataSource#">
				DELETE FROM SODFailure
				FROM
					SODFailure sf WITH (NOLOCK) INNER JOIN SODMaster sm WITH (NOLOCK) ON sm.created = sf.batchDate
					AND sm.sourceID = sf.sourceID
					AND sm.batchRowID = sf.batchRowID
				WHERE
					sm.productID IS NOT NULL
					AND sm.processDate IS NULL
					AND rwPersonAccountID  IS NOT NULL
					AND rwCompanyAccountID  IS NOT NULL
					AND quantity  IS NOT NULL
					AND points  IS NOT NULL
				  <cfif isDefined("arguments.batchDate")>AND created = #arguments.batchDate#</cfif>
				  <cfif isDefined("arguments.sourceID")>AND sm.sourceID = #arguments.sourceID#</cfif>
			</cfquery>
		</cfif> 

	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="moveUnMatchedRecordsToSODFailure" hint="Inserts unmatched records into the failures table">
		<cfargument name="sourceID" type="numeric" required="No">
		<cfargument name="batchDate" type="date" required="No">
		
		<cfscript>
			var insertSODFailures = "";
		</cfscript>
		
		<cfquery name="insertSODFailures" datasource="#application.siteDataSource#">
			INSERT INTO SODFailure (sourceID,batchRowID,batchDate)
			SELECT
				sm.sourceID,
				sm.batchRowID,
				sm.created 
			FROM
				SODMaster sm WITH (NOLOCK) LEFT OUTER JOIN SODFailure sf WITH (NOLOCK) ON sm.created = sf.batchDate
				AND sm.batchRowID = sf.batchRowID
				AND sm.sourceID = sf.sourceID
			WHERE
			(	locationID IS NULL
				OR productID IS NULL
				OR rwPersonAccountID IS NULL
				OR rwCompanyAccountID IS NULL
				OR quantity IS NULL
				OR points IS NULL
			)
				<cfif isDefined("arguments.batchDate")>AND sm.created = #arguments.batchDate#</cfif>
				<cfif isDefined("arguments.sourceID")>AND sm.sourceID = #arguments.sourceID#</cfif>
				AND sf.batchRowID IS NULL
		</cfquery>

	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="processSODclaims" hint="Process SOD claims">
		<cfargument name="numOfClaimsToProcess" type="numeric" default="5">
		<cfargument name="sourceID" type="numeric" required="Yes">
		<cfargument name="batchDate" type="date" required="No">
		<cfargument name="rwPromotionID" type="numeric" required="Yes">
		<cfargument name="expirePointsForDeletedAccounts" type="boolean" default="false"> <!--- NJH 2009/12/03 P-LEX041 --->

		<cfscript>
			var getSODclaimTransactions = "";
			var SourceDetailsQry = getSourceDetails(sourceID=arguments.sourceID);
			var getActivePromotions = "";
			var promotionID = arguments.rwPromotionID; // NJH 2008/05/23 created new variable called promotionID, as rwPromotionID wasn't a good choice... too
														//many variables with this name, and there was a bug due to scope issues.
		</cfscript>
		
		<!--- This key defines what a transaction is based on.
			if it's a single invoice, this should be set to 'invoiceNumber'
			if it's a single row, it should be set to SODClaimID
		 --->
		<cfset transactionKey = SourceDetailsQry.transactionKey>
		
		<!--- get the transactions --->
		<!--- only get sod claims that still have valid company accounts --->
		<!--- 2008/03/17 GCC - piloting Sony CR-SNY639 --->
		<!--- NJH 2008/04/17 added invoiceDate to query to be passed into the getActivePromotions function below --->
		<cfquery name="getSODclaimTransactions" datasource="#application.siteDataSource#">
			SELECT DISTINCT TOP #numOfClaimsToProcess# sodclaim.organisationID,rwCompanyAccountID,
				CAST(#transactionKey# AS varchar) AS #transactionKey#, sodclaim.personID,batchDate,location.countryid,invoiceDate
			FROM         sodclaim WITH (NOLOCK) INNER JOIN
                      Person WITH (NOLOCK) ON sodclaim.PersonID = Person.PersonID INNER JOIN
                      Location WITH (NOLOCK) ON Person.LocationID = Location.LocationID
			WHERE
				sourceID = #arguments.sourceID#
				AND rwCompanyAccountID IN (SELECT accountID FROM rwCompanyAccount)
			<cfif isDefined("arguments.batchDate")>
				AND batchDate = #arguments.batchDate#
			</cfif>
			ORDER BY batchDate
		</cfquery>
		
		<!--- create a transaction and insert transactionItems --->
		<cfloop query="getSODclaimTransactions">
			
			<!--- if a company account exists, create the transaction. --->
			<cfif rwCompanyAccountID gt 0>
				<cfscript>
					transactionID = application.com.relayIncentive.RWAccruePoints(AccountId=rwCompanyAccountID,AccruedDate=CreateODBCDateTime(request.requestTime),PointsAmount=0,PersonID=personID);
				</cfscript>
				
				<cftransaction action = "begin">
					<cftry>
						<!--- get the items for the particular transaction --->
						<cfquery name="getSODTransactionItems" datasource="#application.siteDataSource#">
							SELECT productID, quantity, NoOfUsers, points, invoiceNumber, invoiceDate, 
								endUserCompanyName, batchRowID, sourceID, batchDate, isNull(rwPromotionID,0) as rwPromotionID
							FROM SODclaim WITH (NOLOCK)
							WHERE
								cast(#transactionKey# as varchar) =  <cf_queryparam value="#evaluate(transactionKey)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
								AND organisationID =  <cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								AND rwCompanyAccountID =  <cf_queryparam value="#rwCompanyAccountID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								AND personID =  <cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								AND sourceID = #arguments.sourceID#
								<cfif isDefined("arguments.batchDate")>
									AND batchDate = #arguments.batchDate#
								</cfif>
						</cfquery>
						
						<!--- add items to the transaction --->
						<cfloop query="getSODTransactionItems">
						
							<cfset promotionID = getSODTransactionItems.rwPromotionID>
							
							<cfif points lt 0>
							
							<!--- NJH 2008/03/06 if SOD claim doesn't have a promotion assigned with it and the points is a negative accrual, 
								use the rwPromotionID passed in through the arguments. 
								A SOD claim will have a promotionID with it if it's a points adjustment based on a a transaction changing
								where we are importing SOD with a foreignTransactionID --->
								
								<cfif getSODTransactionItems.rwPromotionID eq 0>
									<cfset promotionID = arguments.rwPromotionID>
								</cfif>
							
								<cfscript>
									RWTransactionItemsID = application.com.relayIncentive.AddRWTransactionItems
									(	RWTransactionID=transactionID,
										ItemID=productID,
										quantity=quantity,
										NoOfUsers=NoOfUsers,
										points=points,
										invoiceNo=invoiceNumber,
										invoiceDate=invoiceDate,
										endUserCompanyName=endUserCompanyName,
										pointsType='PP',
										RWpromotionID=promotionID
									);
								</cfscript>
							<cfelse>

								<cfscript>
									RWTransactionItemsID = application.com.relayIncentive.AddRWTransactionItems
									(	RWTransactionID=transactionID,
										ItemID=productID,
										quantity=quantity,
										NoOfUsers=NoOfUsers,
										points=points,
										invoiceNo=invoiceNumber,
										invoiceDate=invoiceDate,
										endUserCompanyName=endUserCompanyName,
										RWpromotionID=promotionID
									);
								</cfscript>
							</cfif>
							
							<!--- update the processDate row in the SOD Master table --->
							<!--- 2008-02-28 Michael Roberts
								RWTransactionsItemId column was added so that their is a reference WHERE
								RWTransactionItems.RWTransactionsItemId = SODMaster.RWTransactionsItemId
							--->
							<cfif RWTransactionItemsID neq 0>
								<cfquery name="setProcessedDateInSODMaster" datasource="#application.siteDataSource#">
									UPDATE SODMaster
									SET 
										RWTransactionItemsID =  <cf_queryparam value="#RWTransactionItemsID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
										processDate =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
										lastUpdatedBy = #request.relayCurrentUser.personID#,
										lastUpdated =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
									WHERE
										batchRowID =  <cf_queryparam value="#batchRowID#" CFSQLTYPE="CF_SQL_INTEGER" > 
										AND created = '#batchDate#'
										AND sourceID = #sourceID#
								</cfquery>
							</cfif>
						</cfloop>
						<cftransaction action="commit">
						
						<cfcatch type="Database"> 
							<cfoutput> 
							     <h3>Error processing SOD transactions</h3> 
							     <p>Message :#htmleditformat(cfcatch.message)#</p> 
							     <p>Type :#htmleditformat(cfcatch.type)#</p>
								 <p>Detail :#htmleditformat(cfcatch.detail)#</p>
							     <p><i>Rolling back the attempted transaction</i></p>
							     <cftransaction action="rollback"/>
							</cfoutput> 
						</cfcatch>
					</cftry>
				</cftransaction>
				
				<!--- totals the points for the transaction--->
				<cfscript>	
					// 2008/03/17 GCC - piloting Sony CR-SNY639				
					getActivePromotions = application.com.relayIncentive.getActivePromotions( 
							countryID = countryID,
							triggerPoint = 'claim',
							pointsPassed = 0,
							nowDate = invoiceDate,
							personID = personID,
							RWTransactionID = TransactionID);
									
					totalPointsClaimed = application.com.relayIncentive.submitClaim(
							RWTransactionID = transactionID,
							RWTransactionTypeID = "AC",
							distiID = 0); 
				</cfscript>
				
				<!--- NJH 2009/12/03 P-LEX041 expiring points that have just been earned for deleted accounts. START --->
				<cfif arguments.expirePointsForDeletedAccounts>
					<cfquery name="isPersonAccountDeleted" datasource="#application.siteDataSource#">
						select 1 from rwCompanyAccount ca with (noLock) inner join
							rwPersonAccount pa with (noLock) on ca.accountID = pa.accountID
						where ca.accountID =  <cf_queryparam value="#rwCompanyAccountID#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and pa.personID =  <cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and pa.accountDeleted = 1
					</cfquery>
					
					<cfif isPersonAccountDeleted.recordCount gt 0>
						<!--- the RWExpirePoints procedure now accepts an expiry date, so that points due to expire in the future can be 
							expired now. I have set the date to be 10 years in the future, so that should catch everything  --->
						<cfquery name="expireAllPoints" datasource="#application.siteDataSource#">
							exec RWExpirePoints @personID =  <cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" > , @expiryDate=#dateAdd("yyyy",10,request.requestTime)#
						</cfquery>
					</cfif>
				</cfif>
				<!---  NJH 2009/12/03 P-LEX041 END --->
				
			</cfif>
			
		</cfloop>
		
		<!--- remove processed records from the SODclaim table --->
		<cfquery name="removeProcessedRecordsFromSODclaim" datasource="#application.siteDataSource#">
			DELETE FROM SODclaim 
				FROM 
					SODclaim sc INNER JOIN SODMaster sm ON sm.batchRowID = sc.batchRowID 
					AND sm.sourceID = sc.sourceID 
					AND sm.created = sc.batchDate
				WHERE sm.processDate IS NOT NULL
		</cfquery>
		
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="verifySuccessfulLoad" hint="Returns whether a load into the SOD tables was successful" returntype="boolean">
		<cfargument name="sourceID" type="numeric" required="Yes">
		<cfargument name="batchDate" type="date" required="No">
		
		<cfscript>
			var SourceDetailsQry = getSourceDetails(sourceID=arguments.sourceID);
		</cfscript>
		
		<cfif not SourceDetailsQry.useView>
			<cfset queryDataTable = SourceDetailsQry.dataSource>
		<cfelse>
			<cfset queryDataTable = "v#SourceDetailsQry.dataSource#">
		</cfif>
		
		<cfquery name="getDataloadCount" datasource="#application.siteDataSource#">
			SELECT COUNT(*) AS numRecords FROM #queryDataTable#
		</cfquery>
		
		<cfquery name="getNumLoadedIntoSODMaster" datasource="#application.siteDataSource#">
			SELECT COUNT(*) AS numRecords 
			FROM SODMaster WITH (NOLOCK)
			WHERE 
				created = #arguments.batchDate# 
				--AND sourceID = #arguments.sourceID#
		</cfquery>
		
		<cfif getDataloadCount.numRecords neq getNumLoadedIntoSODMaster.numRecords>
			<cfreturn false>
		</cfif>
		
		<!--- NJH 2008/02/29 only check the match date if we're pulling the data from a table rather than a view as we only 
			are matching if the data is in a holding table first. --->
		<cfif not SourceDetailsQry.useView>
			<cfquery name="getUnMatchedDataloadCount" datasource="#application.siteDataSource#">
				SELECT COUNT(*) AS numRecords FROM #SourceDetailsQry.datasource# WHERE matchDate IS NULL
			</cfquery>
			
			<cfquery name="getNumLoadedIntoSODFailure" datasource="#application.siteDataSource#">
				SELECT 
					COUNT(*) AS numRecords
				FROM
					SODFailure sf WITH (NOLOCK) INNER JOIN SODMaster sm WITH (NOLOCK) ON sf.batchDate = sm.created
					AND sf.batchRowID = sm.batchRowID 
					AND sf.sourceID = sm.sourceID
				WHERE
					sf.batchDate = #arguments.batchDate#
					--and sf.sourceID = #arguments.sourceID#
			</cfquery>
			
			<cfif getUnMatchedDataloadCount.numRecords neq getNumLoadedIntoSODFailure.numRecords>
				<cfreturn false>
			</cfif>
			
			<cfquery name="getMatchedDataloadCount" datasource="#application.siteDataSource#">
				SELECT COUNT(*) AS numRecords FROM #SourceDetailsQry.datasource# WHERE matchDate IS NOT NULL
			</cfquery>
			
			<cfquery name="getNumLoadedIntoSODClaim" datasource="#application.siteDataSource#">
				SELECT COUNT(*) AS numRecords
				FROM 
					SODClaim sc WITH (NOLOCK) INNER JOIN SODMaster sm WITH (NOLOCK) ON sc.batchDate = sm.created
					AND sc.batchRowID = sm.batchRowID
					AND sc.sourceID = sm.sourceID
				WHERE
					sc.batchDate = #arguments.batchDate#
					--AND sc.sourceID = #arguments.sourceID# 
			</cfquery>
			
			<cfif getMatchedDataloadCount.numRecords neq getNumLoadedIntoSODClaim.numRecords>
				<cfreturn false>
			</cfif>
		</cfif>
		
		<cfreturn true>

	</cffunction>
	
	<!--- NJH 2007/11/02 these functions were moved from sod.cfc as relaySOD.cfc will be the .cfc used for SOD --->
	
	<cffunction name="getPossibleSODlocationMatchesInCM">
		<!--- bring back locations in CM that possibly match a company within SOD --->
		<cfargument name="searchOrgName" required="yes" type="string">
		<cfargument name="countryID" required="yes" type="numeric">
		<cfargument name="searchMode" required="no" type="string" default="tight">
		
		<cfset getPossibleSODlocationMatchesInCM = "">
		<cfquery name="getPossibleSODlocationMatchesInCM" datasource="#application.sitedatasource#">
			SELECT
				distinct o.organisationID,
				o.organisationName, 
				l.sitename,
				l.locationID,
				isNull(l.address1,'') AS address1,
				isNull(l.Address5,'') AS city,
				isNull(l.postalcode,'') AS postalCode
			FROM 
				Organisation o INNER JOIN Location l ON o.organisationID = l.organisationID
			WHERE
				o.countryID = #arguments.countryID# AND 
				<cfif searchMode eq "loose">
					DIFFERENCE(o.organisationName, '#arguments.searchOrgName#') >= 4 AND
				<cfelseif searchMode eq "tight">
					o.organisationName  LIKE  <cf_queryparam value="#arguments.searchOrgName#%" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
				</cfif>
				l.locationID NOT IN (SELECT locationID FROM SODloadLocationLookup)
			ORDER BY
				o.organisationName,
				l.sitename
		</cfquery>
		<cfreturn getPossibleSODlocationMatchesInCM>
	</cffunction>
	
	
	<cffunction name="getPossibleOrganisationMatchesInSOD">
		<!--- bring back locations in SOD that possibly match an organisation --->
		<cfargument name="searchOrgName" required="yes" type="string">
		<cfargument name="countryID" required="yes" type="numeric">
		<cfargument name="searchMode" required="no" type="string" default="tight">
		
		<cfset getPossibleOrganisationMatchesInSOD = "">
		<cfquery name="getPossibleOrganisationMatchesInSOD" datasource="#application.sitedatasource#">
			SELECT DISTINCT thirdPartyForeignKey, companyName AS organisationName, companyName AS sitename, 
				isNull(address1,'') AS address1, isNull(city,'') AS city, postalCode, thirdPartyForeignKey AS locationID
			FROM SODinitialLoad 
			WHERE thirdPartyForeignKey NOT IN
				(SELECT distinct thirdPartyForeignKey FROM SODloadLocationLookup) AND
			<cfif searchMode eq "loose">
				DIFFERENCE(companyName, '#arguments.searchOrgName#') >= 4 AND
			<cfelseif searchMode eq "tight">
				companyName  LIKE  <cf_queryparam value="#arguments.searchOrgName#%" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
			</cfif>
				countryID = #arguments.countryID#
			ORDER BY companyName
		</cfquery>
		<cfreturn getPossibleOrganisationMatchesInSOD>
	</cffunction>
	
	
	<cffunction name="createMatch">
		<!--- create a match between an CM organisation and a company in the SOD --->
		<cfargument name="thirdPartyForeignKey" required="yes" type="numeric">
		<cfargument name="locationID" required="yes" type="numeric">
		
		<cfquery name="createMatch" datasource="#application.sitedatasource#">
			insert into SODloadLocationLookup (thirdPartyForeignKey,locationID,matchMethod,created) values
				(<cf_queryparam value="#arguments.thirdPartyForeignKey#" CFSQLTYPE="cf_sql_varchar" >, <cf_queryparam value="#arguments.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >,'Manual',<cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
		</cfquery>
		
		<cfset populateloadTable(thirdPartyForeignKey=#arguments.thirdPartyForeignKey#)>
	</cffunction>
	
	
	<cffunction name="populateloadTable">
		<cfargument name="thirdPartyForeignKey" required="no" type="string" default="">
		<cfargument name="loadTableName" required="no" type="string" default="SODload">
	
	
		<cfset var qPopulateloadTable="">
	
		<!--- load automatic org and location matches and any manual ones --->
		<!--- should we be loading new locations at matched organisations? --->
		<cfquery name="qPopulateloadTable" datasource="#application.siteDataSource#" timeout="3600">
			Insert into #loadTableName#
			Select 
				ThirdPartyForeignKey,
				CompanyName,
				DistiName,
				PostalCode,
				CountryID,
				ProductCode,
				Quantity,
				NoOfUsers,
				cast(UnitPrice as money),
				cast(TotalPrice as money),
				SalesDate,
				datepart(wk,SalesDate),
				datepart(mm,SalesDate),
				datepart(qq,SalesDate),
				datepart(yyyy,SalesDate),
				transactionID
			FROM SODinitialLoad
			WHERE 	
				<cfif arguments.thirdPartyForeignKey neq "">	
					ThirdPartyForeignKey =  <cf_queryparam value="#arguments.ThirdPartyForeignKey#" CFSQLTYPE="cf_sql_varchar" > 
				<cfelse>
				(locationAction = 'Locations matched ON city, country, orgid AND postal code'
			or
				ThirdPartyForeignKey in (SELECT distinct ThirdPartyForeignKey FROM SODloadLocationLookup))
				AND badrow IS NULL
				</cfif>
		</cfquery>
	
	</cffunction>
	
	  <CFFUNCTION access="public" name="getUnMatchedSODCountries">
  		    <cfscript>
			  var getCountriesQry = "";
		    </cfscript>
			<!--- <CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
				SELECT c.countryDescription + ' (' + cast(count(distinct sil.ThirdPartyForeignKey) as varchar(255)) + ')' as country, c.countryID, c.ISOcode, count(distinct sil.ThirdPartyForeignKey) as counttpfk FROM 
				country c left outer join SODinitialLoad sil
				on c.countryID = sil.countryID
				WHERE c.countryID IN (#request.relayCurrentUser.countryList#)
				and sil.ThirdPartyForeignKey not in (select distinct ThirdPartyForeignKey FROM SODloadLocation)
				group by country, c.countryID, c.ISOcode, c.countryDescription
				HAVING (count(distinct sil.ThirdPartyForeignKey)) > 0
				ORDER BY country
			</CFQUERY> --->
			
			<CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
				SELECT
					c.countryDescription AS country,
					c.countryID,
					c.ISOcode
				FROM country c left outer join SODinitialLoad sil ON c.countryID = sil.countryID
				WHERE
					c.countryID  IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="cf_sql_integer"  list="true"> )
					AND sil.ThirdPartyForeignKey NOT IN (SELECT distinct ThirdPartyForeignKey FROM SODloadLocationLookup)
				GROUP BY
					country,
					c.countryID,
					c.ISOcode,
					c.countryDescription
				ORDER BY country
			</CFQUERY>
	    
		<cfreturn getCountriesQry>
	  </CFFUNCTION>
	  
	   <CFFUNCTION access="public" name="getCountryCount">
  		   <cfargument name="countryID" default="" required="Yes">
			<cfset var getCountriesCnt = "">
			<CFQUERY NAME="getCountriesCnt" dataSource="#application.siteDataSource#">
				SELECT
					COUNT(DISTINCT ThirdPartyForeignKey) AS countryCount, countryID
				FROM SODinitialLoad
				WHERE
					countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND ThirdPartyForeignKey NOT IN (SELECT DISTINCT ThirdPartyForeignKey FROM SODloadLocationLookup)
				GROUP BY countryID
			</CFQUERY>
			
				    
			<cfreturn getCountriesCnt.countryCount>
	  </CFFUNCTION>
	  
	  <CFFUNCTION access="public" name="setApplicationSODCountryData">
  			<cfset userCountries = getUnMatchedSODCountries()>
			<cfset application.SODCountryData = structNew()>
			<cfloop query="userCountries">
				<cfset application.SODCountryData[countryID] = structNew()>
				<cfset application.SODCountryData[countryID].Country = country>
				<cfset application.SODCountryData[countryID].CountryCount = getCountryCount(countryID)>
			</cfloop>
	  </CFFUNCTION>
	  
	  
	  <CFFUNCTION access="public" name="getUnMatchedOrganisationLocations">
	  	<cfargument name="organisationID" default="" required="Yes">
		    <cfscript>
			  var getCountriesQry = "";
		    </cfscript>
			<CFQUERY NAME="getCountriesQry" dataSource="#application.siteDataSource#">
				SELECT 
					countryDescription + ' (' + cast((SELECT COUNT(locationID) 
														FROM location 
														WHERE countryID = c.countryID
														AND organisationID =  <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  
														AND locationID NOT IN (SELECT locationID FROM SODloadLocationLookup)) AS varchar(255)) + ')' AS country, 
					countryID, ISOcode 
					FROM country c
					WHERE countryID  IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="cf_sql_integer"  list="true"> )
					AND countryID IN (SELECT DISTINCT location.countryID FROM location WHERE organisationID =  <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				ORDER BY country
			</CFQUERY>
	    
		<cfreturn getCountriesQry>
	  </CFFUNCTION>

	  
	  	<!--- NJH 2008/12/16 CR-SNY661 --->
		<cffunction name="getSodLoadLocationLookupData" access="public" returntype="query" hint="Returns values in the SODLoadLocationLookUp table">
			<cfargument name="showEntityTypeIDData" type="numeric" required="false">
			<cfargument name="sortOrder" type="string" default="ThirdPartyForeignKey desc">
			
			<cfscript>
				var qryGetSODLoadLocationLookup = "";
			</cfscript>
			
			<cfquery name="qryGetSODLoadLocationLookup" datasource="#application.siteDataSource#">
				select *,
					<cfif arguments.showEntityTypeIDData eq 1>
						l.sitename
					<cfelseif arguments.showEntityTypeIDData eq 2>
						o.organisationID, o.organisationName
					</cfif>
				from sodLoadLocationLookup slll with (noLock)
					<cfif listFind("1,2",arguments.showEntityTypeIDData)>
						inner join location l with (noLock) on l.locationID = slll.locationID
					</cfif>
					<cfif arguments.showEntityTypeIDData eq 2>
						inner join organisation o with (noLock) on o.organisationID = l.organisationID
					</cfif>
					<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by #arguments.sortOrder#
			</cfquery>
			
			<cfreturn qryGetSODLoadLocationLookup>
		</cffunction>

	
	<!--- NJH 2009/02/02 CR-SNY661 --->
	<cffunction name="updateMergedLocationsInLookup" access="public" hint="Update SODLoadLocationLookUp table based on third party table holding merged location relationships">
		<cfargument name="mergedResellerTable" type="string" required="true">
		<cfargument name="masterColumnName" type="string" required="true">
		<cfargument name="mergedColumnName" type="string" required="true">
		
		<cfscript>
			var qryUpdateMergedLocations = "";
		</cfscript>
		
		<cfquery name="qryUpdateMergedLocations" datasource="#application.siteDataSource#">
			<!--- update slll
				set thirdPartyForeignKey =  <cf_queryparam value="#arguments.masterColumnName#" CFSQLTYPE="cf_sql_varchar" > ,
					lastUpdated = GetDate(),
					lastUpdatedBy = #request.relayCurrentUser.userGroupID#
			from sodLoadLocationLookup slll with (noLock)
				inner join #arguments.mergedResellerTable# mr with (noLock) on #arguments.mergedColumnName# = slll.thirdPartyForeignKey
			 --->
			
			begin tran
			/* put into temp table the new updated records for all records that will cause a primary key contraint in sodLoadLocationLookup when doing the update statement.
			   we need to delete these entries from sodLoadLocationLookup, and then insert the disinct new value
			   
			   ie. if the lookup table has location 1 mapping to birchID 2 & 3, and the merge table tells us that both 2&3 have been merged into 4,
			   then we can't have two rows of 1 mapping to 4. So, the temp table holds the 1-4 mapping, and then the 1-2 and 1-3 mapping in sodLoadLocationLookup gets deleted.
			   1-4 then gets inserted into sodLoadLocationLookup
			   
			   Then do the bulk update of all other values that won't cause an issue.
			*/
			
			declare @tempTable table (locationID int, thirdPartyForeignKey int)
			
			insert into @tempTable (locationID, thirdPartyForeignKey)
			select locationID,<cf_validateSQLObject value="#arguments.masterColumnName#">
				from sodLoadLocationLookup slll with (noLock)
					inner join #arguments.mergedResellerTable# mr with (noLock) on mr.#arguments.mergedColumnName# = slll.thirdPartyForeignKey
				group by <cf_validateSQLObject value="#arguments.masterColumnName#">,locationID 
				having count(locationID) > 1
			
			-- delete these entries
			-- select slll.locationID,mergedID
			delete sodLoadLocationLookup
						from sodLoadLocationLookup slll with (noLock)
							inner join #arguments.mergedResellerTable# mr with (noLock) on mr.#arguments.mergedColumnName# = slll.thirdPartyForeignKey
							inner join @tempTable t on slll.locationID = t.locationID
			
			insert into SodLoadLocationLookup (locationID,thirdPartyForeignKey,matchMethod,lastUpdatedBy)
			select locationID,thirdPartyForeignKey,'Automatic',#request.relayCurrentUser.userGroupID# from @tempTable
			
			commit tran
			
			/* delete all entries in SODLoadLocationLookup that will result in a duplicate getting created where a record already exists
				in which with the master value of the merged value already is mapped to a location.
				for example. locID 140722 maps to 183624 and 243501. The merge table tells us that 183624 is merged with 243501. So, we get rid of 
				the 140722 to 183624 mapping in SODLoadLocationLookup
			 */
			--select slll.* 
			delete slll
				from sodLoadLocationLookup slll with (noLock)
					inner join #arguments.mergedResellerTable# mr with (noLock) on mr.#arguments.mergedColumnName# = slll.thirdPartyForeignKey
					inner join sodLoadLocationLookup slll2 with (noLock) on mr.#arguments.masterColumnName# = slll2.thirdPartyForeignKey
			
			update slll
				set thirdPartyForeignKey =  <cf_queryparam value="#arguments.masterColumnName#" CFSQLTYPE="cf_sql_varchar" >,
					lastUpdated = GetDate(),
					lastUpdatedBy = #request.relayCurrentUser.userGroupID#
			from sodLoadLocationLookup slll with (noLock)
				inner join #arguments.mergedResellerTable# mr with (noLock) on mr.#arguments.mergedColumnName# = slll.thirdPartyForeignKey
		</cfquery>
		
	</cffunction>

</cfcomponent>
