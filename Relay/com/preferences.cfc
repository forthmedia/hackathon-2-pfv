<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 

WAB 2004-11-04

preferences.cfc


This piece of code is used to store non critical data structures in the database
In particular it is going to be used to store user preferences


2007/12/10   WAB  blank data not stored (deleted)

 --->

<cfcomponent displayname="Functions for saving user preferences" hint="">

	<cffunction access="public" name="getUserPreference">

		<cfargument name="hive" type="string" required="true">
		<cfargument name="key" type="string" required="false">
		<cfargument name="defaultStructure" type="struct" default=#structNew()#>
	
			<cfset var memoryStructPointer = getPointerToKeyInMemoryStructure (hiveAndKey="#hive#.#key#")>

			<!--- if no data in the key then go off to get some from the database --->
			<cfif not isStruct(memoryStructPointer)>
				<!--- pointing at an individual bit of data not a structure, so will just return the  data--->
			<cfelseif structCount(memoryStructPointer) is 0>
				<!--- structure has not been populated yet so go to database to get it --->
				<cfset memoryStructPointer = loadPreferenceDataIntoMemory (hive=hive,key=key,defaultStructure=defaultStructure)>
			<cfelse>
				<!--- already loaded from database so no need to go and get it--->
			</cfif>
			
		<cfreturn duplicate(memoryStructPointer)>
	
	</cffunction>


	<cffunction access="public" name="saveUserPreference">

		<cfargument name="hive" type="string" required="true">
		<cfargument name="key" type="string" required="true">
		<cfargument name="data" type="struct" required="true">
		<cfargument name="saveToDatabase" type="boolean" default="true">


			<cfset var hiveAndKey = "#hive#.#key#">
			<cfset var hiveAndKeywithoutLast = listdeleteat(hiveandkey,listlen(hiveandkey,"."),".")>
			<cfset var lastKey = listLast(hiveAndKey,".")>
			<cfset var memoryStructPointer = getPointerToKeyInMemoryStructure (hiveAndKey=hiveAndKeywithoutLast)>
	
			<cfset memoryStructPointer[lastKey] = data>			

			<cfif saveToDatabase>
				<cfset savePreferenceDataToDataBase1 (hive=hive,entityTypeID = 0,entityID = #request.relaycurrentuser.personid#,data = data, key=key )>
			</cfif>
				
		<cfreturn memoryStructPointer[lastKey]>
	
	</cffunction>

	<cffunction access="public" name="deleteUserPreference">

		<cfargument name="hive" type="string" required="true">
		<cfargument name="key" type="string" required="false">

			<cfset var hiveAndKey = "#hive#.#key#">
			<cfset var hiveAndKeywithoutLast = listdeleteat(hiveandkey,listlen(hiveandkey,"."),".")>
			<cfset var lastKey = listLast(hiveAndKey,".")>
			<cfset var memoryStructPointer = getPointerToKeyInMemoryStructure (hiveAndKey=hiveAndKeywithoutLast)>
			
			<cfset structDelete (memoryStructPointer,lastKey)>

			<cfset deletePreferenceDataFromDataBase (hive=hive,entityTypeID = 0,entityID = #request.relaycurrentuser.personid#, key=key )>
		
	</cffunction>
		
		
	<cffunction access="private" name="getPointerToKeyInMemoryStructure" >
			<cfargument name="hiveAndKey" type="string" required="true">
			<cfset var memoryStructPointer = "">
			
			<!--- create userPreference session structure if not exists --->
			<cfif not structKeyExists(session,"userPreferences")>
				<cfset session.userPreferences = structNew()>
			</cfif>
		
			<!--- pointer into structure --->
			<cfset memoryStructPointer = session.userPreferences>
						
			<!--- create all the keys down to the level required --->
			<cfloop index = "thisKey" list = "#hiveAndKey#" delimiters="."> 
				<cfif not structKeyExists (memoryStructPointer,thisKey)>
					<cfset memoryStructPointer[thiskey] = structNew()>					
				</cfif>
				<cfset memoryStructPointer = memoryStructPointer[thiskey]>
			</cfloop>						
		
			<cfreturn memoryStructPointer>
	
	</cffunction>
	
	
	<cffunction access="public" name="saveUserPreferenceToSession">
			<cfargument name="hive" type="string" required="true">
			<cfargument name="key" type="string" required="true">
			<cfargument name="data" type="struct" required="true">
	
			<cfreturn saveUserPreference(argumentCollection = arguments, savetodatabase = false)>
			
	</cffunction>
	
	<cffunction access="public" name="saveUserPreferenceToSessionAndDatabase">
			<cfargument name="hive" type="string" required="true">
			<cfargument name="key" type="string" required="true">
			<cfargument name="data" type="struct" required="true">
	
			<cfreturn saveUserPreference(argumentCollection = arguments, savetodatabase = true)>
			
	</cffunction>


	<cffunction access="public" name="deletePreferenceDataFromSessionAndDataBase">
			<cfargument name="hive" type="string" required="true">
			<cfargument name="key" type="string" required="true">
	

			<cfset deletePreferenceDataFromDataBase (hive=hive,entityTypeID = 0,entityID = #request.relaycurrentuser.personid#, key=key )>

			<cfreturn >
			
	</cffunction>

	

	<cffunction access="public" name="loadPreferenceDataIntoMemory" >
		<cfargument name="hive" type="string" required="true">
		<cfargument name="key" type="string" required="true">
		<cfargument name="entityTypeID"  default="0">	
		<cfargument name="entityID"  default="#request.relaycurrentuser.personid#">	
		<cfargument name="defaultStructure" type="struct" default = #structNew()#>


			<cfset var hiveAndKey = "#hive#.#key#">
			<cfset var hiveAndKeywithoutLast = listdeleteat(hiveandkey,listlen(hiveandkey,"."),".")>
			<cfset var lastKey = listLast(hiveAndKey,".")>
			<cfset var memoryStructPointer = getPointerToKeyInMemoryStructure (hiveAndKey=hiveAndKeywithoutLast)>

			<cfset memoryStructPointer[lastKey] = getPreferenceDataFromDatabase(argumentCollection = arguments)>

		<cfreturn memoryStructPointer[lastKey]>

	</cffunction>
	
	
	<!--- 
	returns a structure of all the data in the given hive/key
	if key requested actually points to single bit of data rather than a structure then will return a string
	 --->
	<cffunction access="public" name="getPreferenceDataFromDatabase" >
		<cfargument name="hive" type="string" required="true">
		<cfargument name="key" type="string" required="true">
		<cfargument name="entityTypeID"  required="true" type="numeric">	
		<cfargument name="entityID"  required="true" type="numeric">	
		<cfargument name="defaultStructure" type="struct" default = #structNew()#>

				<cfset var getData = "">
				<cfset var dataStruct = duplicate(defaultStructure)>

				<cfset var dataStructPointer = structNew()>
		
				 <cfquery name = "getdata"  datasource = "#application.sitedatasource#">
				 select data , subkey, stuff(subkey,1,len('#key#'),'') as subkey_
				from 
					preferences
				 where 
				 	 entityTypeid = #entityTypeID#
				 and entityid = #entityID#
				 and hive =  <cf_queryparam value="#hive#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				 <cfif key is not ""> <!--- If key is blank then bring back whole hive --->
				 and (subkey  like  <cf_queryparam value="#key#.%" CFSQLTYPE="CF_SQL_VARCHAR" >  or subkey =  <cf_queryparam value="#key#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				 </cfif>
				 order by subkey
				 </cfquery>
				
				<cfloop query = "getdata" >
					<cfset dataStructPointer = dataStruct>
					<cfif listLen(subkey_,".") is 0>
						<!--- the key requested is a piece of data rather than a structure --->
						<cfreturn data>
					<cfelse>
						<!--- this loop creates each key in the structure if it does not already exist
							could actually do
							<cfset "dataStruct#subKey_#" =  data>
							which would be quicker but couldn't handle subkeys which aren't valid CF variable names  such a report.1
							I hoped to recreate arrays, but can't because arrays aren't passed by reference
						--->					
 						<cfset listLengthMinus1 = listlen(subkey_,".")-1>
 						<cfloop index="I"	from="1" to= "#listLengthMinus1#">
							<cfset thisKey = listGetAt(subkey_,I,".")>
							<cfif not structKeyExists (dataStructPointer,thisKey)>
								<cfset dataStructPointer[thiskey] = structNew()>					
							</cfif>
							<cfset dataStructPointer =  dataStructPointer[thiskey]>
						</cfloop>
						<cfset dataStructPointer[listlast(subkey_,".")] = data>
					</cfif>

				</cfloop>
							
				<cfreturn dataStruct>

	</cffunction>
	
	<cffunction name="savePreferenceDataToDataBase1">
			<cfargument name="hive" type="string" required="true">
			<cfargument name="key" type="string" required="true">	
			<cfargument name="data"  required="true">	
			<cfargument name="entityTypeID"  required="true" type="numeric">	
			<cfargument name="entityID"  required="true" type="numeric">	
			
			<cfset var itemsupdated = savePreferenceDataToDataBase (argumentCollection=arguments)>
			

			 <cfquery name = "deleteOlddata"  datasource = "#application.sitedatasource#">
				delete from 
					preferences where entityTypeid = #entitytypeID# and entityid = #entityID# and hive =  <cf_queryparam value="#hive#" CFSQLTYPE="CF_SQL_VARCHAR" >  
					 <cfif key is not ""> <!--- If key is blank then dealing with whole hive --->
					 and (subkey  like  <cf_queryparam value="#key#.%" CFSQLTYPE="CF_SQL_VARCHAR" >  or subkey =  <cf_queryparam value="#key#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 </cfif>
					 <cfif itemsupdated is not "">
					and subkey  not in ( <cf_queryparam value="#itemsupdated#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					</cfif>
				 </cfquery>


	</cffunction>		


		<cffunction name="savePreferenceDataToDataBase">
			<cfargument name="hive" type="string" required="true">
			<cfargument name="key" type="string" required="true">	
			<cfargument name="data"  required="true">	
			<cfargument name="entityTypeID"  required="true" type="numeric">	
			<cfargument name="entityID"  required="true" type="numeric">	
			<cfset var itemsupdated = "">

			<cfif isStruct(data)>

				<cfloop collection="#data#" item="thiskey">		
					<cfset x = savePreferenceDataToDataBase (hive=hive,entityTypeID = entityTypeID,entityID = entityID,data = data[thisKey], key="#key#.#thisKey#" )>
					<cfset itemsupdated = listappend(itemsupdated,x)>
				</cfloop>
				
			<cfelseif isArray(data)>   <!--- bit odd but can handle arrays (although currently get converted back to structures on reload! --->
				<cfloop index="thisKey" from = "1" to = "#arraylen(data)#">
					<cfset x = savePreferenceDataToDataBase (hive=hive,entityTypeID = entityTypeID,entityID = entityID,data = data[thisKey], key="#key#.#thisKey#" )>
					<cfset itemsupdated = listappend(itemsupdated,x)>
				</cfloop> 
				
			<cfelse>
				<cfif data is not ''>
				 <cfquery name = "updatedata"  datasource = "#application.sitedatasource#">
				declare @olddata varchar(200), @newdata varchar(200)
					select @newdata =  <cf_queryparam value="#data#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					select @olddata = data  from preferences where entityTypeid = #entitytypeID# and entityid = #entityID# and hive =  <cf_queryparam value="#hive#" CFSQLTYPE="CF_SQL_VARCHAR" >  and subkey =  <cf_queryparam value="#key#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				 IF @@rowcount = 1 
				 	BEGIN
						IF @newdata <> @olddata
							update 	preferences set data = @newdata, lastupdated = getdate(), lastupdatedby = #request.relaycurrentuser.personid# where entityTypeid = #entitytypeID# and entityid = #entityID# and hive =  <cf_queryparam value="#hive#" CFSQLTYPE="CF_SQL_VARCHAR" >  and subkey =  <cf_queryparam value="#key#" CFSQLTYPE="CF_SQL_VARCHAR" > 		
					END
				ELSE 
				 	BEGIN
						insert into preferences (entityTypeID, entityid, hive, subkey, data,created,createdby,lastupdated, lastupdatedby  ) values (<cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#hive#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#key#" CFSQLTYPE="CF_SQL_VARCHAR" >,@newdata,getdate(),<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,getdate(),<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >) 		
					END
				 </cfquery>
			
				<cfset itemsupdated = key>
				</cfif>

			</cfif>
		
				<cfreturn itemsupdated>
		
		</cffunction>

	<cffunction access="public" name="deletePreferenceDataFromDatabase" >
		<cfargument name="hive" type="string" required="true">
		<cfargument name="key" type="string" required="true">
		<cfargument name="entityTypeID"  required="true" type="numeric">	
		<cfargument name="entityID"  required="true" type="numeric">	


				 <cfquery name = "deleteData"  datasource = "#application.sitedatasource#">
					delete
				from 
					preferences
				 where 
				 	 entityTypeid = #entityTypeID#
					 and entityid = #entityID#
					 and hive =  <cf_queryparam value="#hive#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					 <cfif key is not ""> <!--- If key is blank then delete whole hive --->
						 and (subkey  like  <cf_queryparam value="#key#.%" CFSQLTYPE="CF_SQL_VARCHAR" >  or subkey =  <cf_queryparam value="#key#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					 </cfif>
				 </cfquery>

	</cffunction>		
	
	<cffunction access="public" name="clearPreferencesFromMemory" >
			<cfset  structDelete(session,"userPreferences")>
	</cffunction>
	
</cfcomponent>

