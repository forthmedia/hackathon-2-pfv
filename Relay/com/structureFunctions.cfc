<!--- �Relayware. All Rights Reserved 2014 --->
<!---

	2013/09/25 Roadmap Sprint 1  - added convertStructureToNameValuePairsForHTMLOutput which speeds up the output of input fields (used by convertTagAttributeStructToValidAttributeList in relayForms.cfc). It makes use of some new CF10 functions.
	2015-03-10 	WAB Added areStructuresIdentical function - (with optional caseSensitivity)
 --->

<cfcomponent displayname="structureFunctions" hint="">

		<!---
		converts the given row in a query to a structure
		WAB 2006-02-28

		WAB May 2006 added feature to allow a special character (probably underscore) to mean that this field is actually part of a structure
		WAB 2008/10/07 changes to convertStructureToNameValuePairs() during work on P-SOPHSprint5,added a recurse function
		WAB 2008/12/03  add convertStructureToJSArray as part of BUG-1440
	njh 2009/03/04  added var'ing to structtoquery
	WAB	2009/03/10 added defaultColumnsAndDataTypes parameter to stuctToQuery
2009/03 WAB added function queryToTwoDimensionalStruct
2009/03 WAB added function QueryAggregateList
	NJH 2009-04-07 - S-01062 - fixed the structToQuery function to use defaultColumnsAndDataTypes correctly :)
	WAB	2009/03/10 added defaultColumnsAndDataTypes parameter to stuctToQuery
		 WAB 2009/04/29  Added a StructPop functions
		WAB 2009/05/11 altered convertStructureToNameValuePairs to be able to always use a specific qualifier
		 NJH 2009/10/05	P-FNL079 - added transposeMappingStructure function
		WAB	2010/11/09				Added htmleditformat encoding to convertStructureToNameValuePairs()
		WAB 2011/11/08   varing (based on error seen on live bug report)
		WAB 2011/11/17 convertStructureToNameValuePairs new argument KeyList
		WAB 2013-01-17  P-LEX075 Added orderStructureByAttribute() to help with my CFC introspection functions.
		WAB 2013-04-30  Add arrayOfStructsSort function
		WAB 2013-10-02  For CASE 437290, added functions for accessing a structure by dot notation
		WAB 2013-10		During CASE 437315 added an output=false
		NJH 2016/10/10	Added lowerCaseKeys for a Jira ticket that I can't find at the moment. Effectively, we wanted to convert a query to a structure with lower case key, as it was going to be used as a JSON structure.
						Created new argument to support that.

		WAB 2016-11-11 During PROD2016-2731 Added listToStruct()
--->

		<cffunction name="queryRowToStruct" returntype="struct" hint = "if row = 0 you get a blank structure" output="false">
			<cfargument name="query" type="query" required="true">
			<cfargument name="row" type="numeric" default="1" hint="usually myquery.currentrow">
			<cfargument name="columns" type="string" default="#query.columnlist#" hint = "leave blank for all columns">
			<cfargument name="excludecolumns" type="string" default="">
			<cfargument name="lowerCaseKeys" type="boolean" default="false">

			<cfset var result = structNew()>
			<cfset var position = 0 >
			<cfset var theColumn= ""  >
			<cfset var removecolumn = '' />

			<cfif excludeColumns is not "">
				<cfloop index="removecolumn" list = "#excludeColumns#">
					<cfset position = listfindnocase(columns,removecolumn)>
					<cfif position is not 0>
						<cfset arguments.columns = listdeleteat(arguments.columns,position)>
					</cfif>
				</cfloop>
			</cfif>

			<cfloop index="theColumn" list = "#arguments.columns#">
					<cfset var theKey = arguments.lowerCaseKeys?lcase(theColumn):theColumn>

					<cfset result[theKey] = arguments.query[theColumn][arguments.row]>
			</cfloop>

			<cfreturn result>

		</cffunction>



		<!---
			WAB June 2006
			take a flat structure with keynames with (say) _ notation  (eg house_colour, house_Name)
			and convert to a structure of structures house.colour, house.name
			--->

		<cffunction name="convertFlatStructureToStructureOfStructures" returntype="struct" hint = "" output="false">
			<cfargument name="structure" type="struct" >
			<cfargument name="structureDelimiter" type="string" default="_" hint="eg  '_'  - any columns with this in the name will end up as nested structure.  eg xyz_abc will become result.xyz.abc ">

				<cfset var arrayOfKeys = '' />
				<cfset var temp = '' />
				<cfset var x = '' />
				<cfset var i = '' />
				<cfset var key = '' />

				<cfloop collection = #structure# item = "key">
					<cfif listlen(key,structureDelimiter) gt 1>
						<cfset arrayOfKeys = listToArray (key,structureDelimiter)>
						<cfset temp = structure>
						<cfset x = arraylen(arrayOfKeys)-1>
						<cfloop index = "i" from = 1 to = "#x#">
							<cfif not structKeyExists(temp,arrayOfKeys[i])>
								<cfset temp[arrayOfKeys[i]] = structNew()>
							</cfif>
								<cfset temp = temp[arrayOfKeys[i]]>
						</cfloop>
						<cfset temp[arrayOfKeys[arraylen(arrayOfKeys)]] = structure[key]>
						<cfset structDelete(structure,key)>
					</cfif>
				</cfloop>

				<cfreturn structure>

	</cffunction>



		<cffunction name="queryToStruct" returntype="struct" hint = "" output="false">
			<cfargument name="query" type="query" required="true">
			<cfargument name="key" type="string" required="true">
			<cfargument name="columns" type="string" default="#query.columnlist#" hint = "leave blank for all columns">
			<cfargument name="excludecolumns" type="string" default="">

			<cfset var result = structNew()>
			<cfset var i = 0>

				<cfloop index ="i" from = "1" to = "#query.recordcount#">
					<cfset result[query[key][i]] = queryRowToStruct(query,i,columns,excludecolumns)>
				</cfloop>

			<cfreturn result>

		</cffunction>


<!---
WAB 2009/03 as part of the edit phrase widget

--->
		<cffunction name="queryToTwoDimensionalStruct" returntype="struct" hint = "" output="false">
			<cfargument name="query" type="query" required="true">
			<cfargument name="key1" type="string" required="true">
			<cfargument name="key2" type="string" required="true">
			<cfargument name="valueColumn" type="string" default = "">  <!--- if you just want result[key1][key2] = valueColumn as opposed to result[key1][key2] = aStructure of all the columns in the query--->
			<cfargument name="columns" type="string" default="#query.columnlist#" hint = "leave blank for all columns">
			<cfargument name="excludecolumns" type="string" default="">

			<cfset var result = structNew()>
			<cfset var i = 0>

				<cfloop index ="i" from = "1" to = "#query.recordcount#">
					<cfparam name = "result[query[key1][i]]" default = "#structNew()#">
					<cfif valueColumn is not "">
						<cfset result[query[key1][i]][query[key2][i]] = query[valueColumn][i]>
					<cfelse>
						<cfset result[query[key1][i]][query[key2][i]] = queryRowToStruct(query,i,columns,excludecolumns)>
					</cfif>
				</cfloop>

			<cfreturn result>

		</cffunction>

		<!--- WAB 2011/09/28 for MS
		same principle as queryToTwoDimensionalStruct, just more of it
		--->
		<cffunction name="queryToFourDimensionalStruct" returntype="struct" hint = "" output="false">
			<cfargument name="query" type="query" required="true">
			<cfargument name="key1" type="string" required="true">
			<cfargument name="key2" type="string" required="true">
			<cfargument name="key3" type="string" required="true">
			<cfargument name="key4" type="string" required="true">
			<cfargument name="valueColumn" type="string" default = "">  <!--- if you just want result[key1][key2] = valueColumn as opposed to result[key1][key2] = aStructure of all the columns in the query--->
			<cfargument name="columns" type="string" default="#query.columnlist#" hint = "leave blank for all columns">
			<cfargument name="excludecolumns" type="string" default="">

			<cfset var result = structNew()>
			<cfset var i = 0>

				<cfloop index ="i" from = "1" to = "#query.recordcount#">
					<cfparam name = "result[query[key1][i]]" default = "#structNew()#">
					<cfparam name = "result[query[key1][i]][query[key2][i]]" default = "#structNew()#">
					<cfparam name = "result[query[key1][i]][query[key2][i]][query[key3][i]]" default = "#structNew()#">
					<cfif valueColumn is not "">
						<cfset result[query[key1][i]][query[key2][i]][query[key3][i]][query[key4][i]] = query[valueColumn][i]>
					<cfelse>
						<cfset result[query[key1][i]][query[key2][i]][query[key3][i]][query[key4][i]] = queryRowToStruct(query,i,columns,excludecolumns)>
					</cfif>
				</cfloop>

			<cfreturn result>

		</cffunction>

		<!--- 2011/12/05 PPB/MS extended queryToFourDimensionalStruct 	--->
		<cffunction name="queryToFiveDimensionalStruct" returntype="struct" hint = "" output="false">
			<cfargument name="query" type="query" required="true">
			<cfargument name="key1" type="string" required="true">
			<cfargument name="key2" type="string" required="true">
			<cfargument name="key3" type="string" required="true">
			<cfargument name="key4" type="string" required="true">
			<cfargument name="key5" type="string" required="true">
			<cfargument name="valueColumn" type="string" default = "">  <!--- if you just want result[key1][key2] = valueColumn as opposed to result[key1][key2] = aStructure of all the columns in the query--->
			<cfargument name="columns" type="string" default="#query.columnlist#" hint = "leave blank for all columns">
			<cfargument name="excludecolumns" type="string" default="">

			<cfset var result = structNew()>
			<cfset var i = 0>

				<cfloop index ="i" from = "1" to = "#query.recordcount#">
					<cfparam name = "result[query[key1][i]]" default = "#structNew()#">
					<cfparam name = "result[query[key1][i]][query[key2][i]]" default = "#structNew()#">
					<cfparam name = "result[query[key1][i]][query[key2][i]][query[key3][i]]" default = "#structNew()#">
					<cfparam name = "result[query[key1][i]][query[key2][i]][query[key3][i]][query[key4][i]]" default = "#structNew()#">
					<cfif valueColumn is not "">
						<cfset result[query[key1][i]][query[key2][i]][query[key3][i]][query[key4][i]][query[key5][i]] = query[valueColumn][i]>
					<cfelse>
						<cfset result[query[key1][i]][query[key2][i]][query[key3][i]][query[key4][i]][query[key5][i]] = queryRowToStruct(query,i,columns,excludecolumns)>
					</cfif>
				</cfloop>

			<cfreturn result>

		</cffunction>


		<cffunction name="QueryToArrayOfStructures" returntype="array" hint = "" output="false">
			<cfargument name="query" type="query" required="true">
			<cfargument name="columns" type="string" default="#query.columnlist#" hint = "leave blank for all columns">
			<cfargument name="excludecolumns" type="string" default="">
			<cfargument name="lowerCaseKeys" type="boolean" default="false">

			<cfset var result = ArrayNew(1)>
			<cfset var i = 0>

				<cfloop index ="i" from = "1" to = "#query.recordcount#">
					<cfset result[i] = queryRowToStruct(query,i,columns,excludecolumns,lowerCaseKeys)>
				</cfloop>

			<cfreturn result>

		</cffunction>


		<!--- WAB 2007-03-12
			2009/03/10 added defaultColumnsAndDataTypes parameter for Nat's scheduled task handler
		--->
		<cffunction name="structToQuery" returntype="query" hint = "takes a simple structure and creates a query out of it" output="false">
			<cfargument name="struct" type="struct" required="true">
			<cfargument name="defaultColumnsAndDataTypes" type="string" default=""><!--- pass in a list of columns which must be returned.  List of columnName:DataType,columnName:DataType,columnName:DataType --->
			<cfargument name="defaultDataType" type="string" default="varchar">

			<!--- NJH 2009/03/04 Bug Fix All sites Issue 1929- var'ing variables as we're getting strange behaviour --->
			<cfscript>
				var theQuery = "";
				var thisItem = "";
				var keys = "";
				var item = "";
				var thisKey = "";
				var column = "";
				var columnName = "";
				var columnDataType = "";
			</cfscript>
			<!--- <cfset var theQuery = queryNew("")> --->

			<cfif defaultColumnsAndDataTypes is not "">
				<cfloop index = "column" list = "#defaultColumnsAndDataTypes#">
					<!--- NJH 2009-04-07 S-01062 was -- <cfset columnName = listFirst(column,":") > --->
					<cfset columnName = listAppend(columnName,listFirst(column,":"))>

					<!--- NJH 2009-04-07 S-01062  was --<cfif listLen(column,":") GT 1>
						<cfset columnDataType = listLast(column,":")>
					<cfelse>
						<cfset columnDataType = defaultDataType>
					</cfif> --->
					<cfif listLen(column,":") GT 1>
						<cfset columnDataType = listAppend(columnDataType,listLast(column,":"))>
					<cfelse>
						<cfset columnDataType = listAppend(columnDataType,defaultDataType)>
					</cfif>
				</cfloop>
				<!--- NJH 2009-04-07 S-01062 was -- <cfset queryAddColumn(theQuery,thiskey,columnDataType,arrayNew(1))> --->
				<cfset theQuery=queryNew(columnName,columnDataType)>
			<cfelse>
				<cfset theQuery=queryNew("")>
			</cfif>

			<cfloop item ="item" collection = #arguments.struct#>
				<cfset thisitem = arguments.struct[item]>
				<cfset keys = structKeyList(thisitem)>
					<cfset queryAddRow(theQuery)>
					<cfloop index = "thisKey" list = "#keys#">
						<cfif isSimpleValue(thisItem[thiskey])>
							<cfif listFindNoCase (theQuery.columnList,thisKey) is 0>
								<cfset queryAddColumn(theQuery,thiskey,arrayNew(1))>
							</cfif>
							<cfset querySetCell (theQuery, thisKey,thisItem[thiskey])>
						</cfif>
					</cfloop>

				</cfloop>

			<cfreturn theQuery>

		</cffunction>


		<cffunction name="structToQuerySimple" returntype="query" hint = "takes a simple structure and creates a query out of it">
			<cfargument name="struct" type="struct" required="true">

			<cfset var key = "">
			<cfset var structQuery = queryNew(structKeyList(arguments.struct))>

			<cfset queryAddRow(structQuery,1)>

			<cfloop collection="#arguments.struct#" item="key">
				<cfif structKeyExists(arguments.struct,key)>
				<cfset querySetCell(structQuery, key, arguments.struct[key])>
				<cfelse>
					<cfset querySetCell(structQuery, key, "")>
				</cfif>
			</cfloop>

			<cfreturn structQuery>

		</cffunction>


		<cffunction name="convertNameValuePairStringToStructure" access="public" hint="" returntype="struct" output="false">
			<cfargument name="inputString" type="string" required="true">
			<cfargument name="delimiter" type="string" default = " ">
			<cfargument name="equalsSign" type="string" default="=">

			<cfreturn application.com.regexp.convertNameValuePairStringToStructure (inputString = inputString, delimiter = delimiter,equalsSign = equalsSign )>

		</cffunction>


	<cffunction name="convertStructureToQueryString" access="public" output="no" returnType="string" hint="A wrapper function that converts a structure to a query string">
		<cfargument name="struct" required="yes" type="struct" hint="The struct to convert.">

		<cfreturn application.com.structureFunctions.convertStructureToNameValuePairs(struct = arguments.struct,delimiter="&",encode="URL")>
	</cffunction>

	<!--- -------------------------------------------------- --->
	<!--- WAB 2007/11/21 adapted from a structToList function
	WAB 2008/10/06 added in a recurse argument to recurse a whole structure and return values in dot notation
					added includeNulls
	WAB 2009/05/13 added a qualifier parameter if a qualifier has to be used (eg for XML)  (actually for XML would need to convert some characters as well)
	WAB 2011/11/17 added a KeyList parameter so that you can determine the order of the name value pairs
	WAB 2012-02		HTMLEdit Encoding renamed to HTMLAttribute and uses ESAPI encoding if available (removed a handful of references to HTMLEdit )
	WAB 2013-03-20	Deal with structures where there is an item in the structKeyList but it is not actually defined (which can happen if you pass an argument collection which contains an optional argument)
	--->
	<cffunction name="convertStructureToNameValuePairs" access="public" output="no" returntype="string" hint="Converts a struct to list.">

		<!--- Function Arguments --->
		<cfargument name="struct"    required="yes" type="struct"             hint="The struct to convert.">
		<cfargument name="delimiter" required="no"  type="string" default="," hint="Character(s) that separate list elements.">
		<cfargument name="equalssign" required="no"  type="string" default="=" hint="can replace the equals sign with something else">
		<cfargument name="recurse" required="no"  type="boolean" default=false hint="recurse through the structure">
		<cfargument name="recursePrefix" required="no"  type="string" default="" >
		<cfargument name="includeNulls" type="boolean" default="true">
		<cfargument name="qualifier" type="string" default=""> <!--- WAB 2009/05/13 --->
		<cfargument name="excludeKeys" type="string" default="">   <!--- WAB 2010/06 can specifically exclude certain items --->
		<cfargument name="excludeKeysRegexp" type="string" default="#replace(excludeKeys,',','|','ALL')#"> <!--- WAb2011/03/29 replaced above with a regexp --->
		<cfargument name="includeKeys" type="string" default="">   <!--- WAB 2010/06 can specifically only include certain items --->
		<cfargument name="includeKeysRegexp" type="string" default="#replace(includeKeys,',','|','ALL')#">
		<cfargument name="encode" type="string" default="">  <!--- eg XML WAB 2010/09/22--->
		<cfargument name="KeyList" type="string" default="#structKeyList(arguments.struct)#">  <!--- WAB 2011/11/17 can force the order of the keys--->

		<cfscript>

			/* Default variables */
//			var i = 0;
//			var key  = StructKeyArray(arguments.struct);
			var item = '';
			var list = "";
			var quote = '';
			var value = '';
			var i = '';

			/* Loop over struct elements */
//			for(i=1; i LTE ArrayLen(key); i=i+1)
//				list = ListAppend(list, key[i] & equalssign & arguments.struct[key[i]], arguments.delimiter);
//			for (item in arguments.struct) {
			for(i=1;i lte listLen(keyList);i=i+1) {
				item=listGetAt(keyList,i);
				if ((excludeKeysRegExp is "" or refindNoCase(excludeKeysRegExp,item) is 0) and (includeKeysRegExp is "" or refindNoCase(includeKeysRegExp,item) is not 0)) {
					if (structKeyExists(struct,item) AND isSimpleValue(struct[item])) {
						value = arguments.struct[item] ;
						if (value is not "" or includeNulls) {
							switch (encode) {
								case "XML":
								{
								value = XMLFormat(value);
								break;
								}
								case "HTMLAttribute":
								{
								value = application.com.security.encodeForHTMLAttribute(value);
								break;
								}

								case "URL":
								{
								value = URLEncodedFormat(urldecode(value));  // doing a decode and an encode prevents doubling encoding % signs.  Would only fall over if you were really trying to encode something of the form %nn but OK for dealing with query_ string encoding
								break;
								}
							}
							// WAB 2009/05/13 added code for using a qualifier	- this could be done somewhat better!
							if(qualifier is "") {
							 	quote='';
								if (value contains delimiter) {
									if (value contains '"') {quote = "'";} else {quote = '"';}
								}
							} else {
								quote = qualifier;
								value = replace(value,qualifier,qualifier&qualifier,"all");
							}
							list = ListAppend(list, recursePrefix & lcase(item) & equalssign & quote & value & quote, arguments.delimiter);
						}
					} else if (recurse){
						list = ListAppend(list, convertStructureToNameValuePairs(struct =struct[item], delimiter = delimiter, equalssign = equalssign, recurse = recurse, recursePrefix = item & ".",includenulls = includenulls) , arguments.delimiter);
					}
				}
			}

			/* Return list */
			return list;

		</cfscript>

	</cffunction>


	<cffunction name="convertStructureToNameValuePairsForHTMLOutput" output="false">
		<cfargument name="struct"    required="yes" type="struct"             hint="The struct to convert.">
		<cfargument name="includeKeysRegexp" type="string" default="">

		<cfscript>
			var quote='"';
			var list = "";
			var filteredStruct = structNew();


			filteredStruct = structFilter(arguments.struct,function(structKey,structValue) {
			 		 if (refindNoCase(includeKeysRegexp,arguments.structKey) neq 0 and structKeyExists(arguments,"structValue") and isSimpleValue(arguments.structValue) and arguments.structValue neq "") {
						return true;
			 		 } else {
						return false;
			 		}
			 });

			/*	2013-10-11	YMA	Case 437343 encodeForHTMLAttribute treats backslash as escape character so use our own function that uses esapi. */
			structEach(filteredStruct,function(key,value) {list= ListAppend(list, lcase(key) & "=" & quote & application.com.security.encodeForHTMLAttribute(value) & quote," ");});

			/* Return list */
			return list;

		</cfscript>
	</cffunction>

	<!---
		WAB 2011/02/01
		encodeNameValuePairString
		Takes a set of name value pairs and encode all the values (by default using HTMLEditFormat)
		It is called by security.make_safe_query_ String to encode request.query_string so that it is safe to put into a form's action attribute to prevent XXS attacks
	--->

	<cffunction name="encodeNameValuePairString" output="false">
		<cfargument name="inputString">
		<cfargument name="delimiter" default="&">
		<cfargument name="encode" default="URL">

		<cfset var structure = application.com.structurefunctions.convertNameValuePairStringToStructure(inputstring=inputString,delimiter=delimiter)>
		<cfset var result = application.com.structurefunctions.convertStructureToNameValuePairs(struct = structure,delimiter=delimiter,encode=encode)>

		<cfreturn result>
	</cffunction>


<!---
WAB 2008/12/08 created this function from the one above to create strings of the form {name:'myname',colour:'blue'}
 --->
	<cffunction name="convertStructureToJSArray" access="public" output="no" returntype="string" hint="Converts a struct to JSArray">

		<!--- Function Arguments --->
		<cfargument name="struct"    required="yes" type="struct"             hint="The struct to convert.">
		<cfargument name="recurse" required="no"  type="boolean" default=false hint="recurse through the structure">

		<cfscript>

			/* Default variables */
			var item = '';
			var list = "";
			var quote = '';
			var value = '';

			/* Loop over struct elements */
			for (item in arguments.struct) {
				if (not isStruct(struct[item])) {
					value = arguments.struct[item] ;
					list = ListAppend(list, "#item#:'#jsstringformat(value)#'",",");
				} else if (recurse){
					list = ListAppend(list, "{#convertStructureToJSArray(struct =struct[item], recurse = true )#}",",");
			}



			}
			list = "{#list#}";
			/* Return list */
			return list;

		</cfscript>

	</cffunction>


<!---
 Merge two simple structures in one
 keys in second overwrite keys in first.
 Can specify whether new keys are to be created

 Turns out that this is similar to structAppend which I didn't know about!

 struct1 	 The first struct. (Required)
 struct2 	 The second struct. (Required)
allownewkeys      true/false
outputwarnings     true/false  (only relevant if allownewkeys = false)


WAB

 Returns a struct (which is struct1 - unlessmergetoNewStruct is set)

--->

<cffunction name="structMerge" output="true">
	<cfargument name="struct1" type="struct" required="true">
	<cfargument name="struct2" type="struct" required="true">
	<cfargument name="mergeToNewStruct" type="boolean" default=false>
	<cfargument name="dontMergeNulls" type="boolean" default=false>
	<cfargument name="allowNewKeys" type="boolean" default=true>
	<cfargument name="outputWarnings" type="boolean" default=false>

	<cfset var key = "" />
	<cfset var resultStruct = structNew()>

	<cfif mergeToNewStruct>
		<cfset resultStruct = duplicate(struct1)>
	<cfelse>
		<cfset resultStruct = struct1>
	</cfif>

	<cfif allowNewKeys>
		<cfloop collection="#arguments.struct2#" item="key">
			<cfif isStruct (arguments.struct2[key])>
					<cfif not structKeyExists(resultStruct,key)>
							<cfset resultStruct[key] = structNew()>
					</cfif>

				<cftry>
					<cfset resultStruct[key] = structMerge(struct1 = resultStruct[key],struct2 = arguments.struct2[key],mergeToNewStruct = mergeToNewStruct, dontMergeNulls = dontMergeNulls, allowNewKeys = allowNewKeys)>
					<cfcatch>
						<CFRETHROW>
					</cfcatch>
				</cftry>
			<cfelse>
				<cfif not dontMergeNulls or arguments.struct2[key] is not "" or not structKeyExists(resultStruct,key)>
					<cfset resultStruct[key] = arguments.struct2[key]>
				</cfif>
			</cfif>
		</cfloop>
	<cfelse>
		<cfloop collection="#arguments.struct2#" item="key">
			<cfif structKeyExists (resultStruct,key)>
				<cfif isStruct (arguments.struct2[key])>
						<cfset resultStruct[key] = structMerge(struct1 = resultStruct[key],struct2 = arguments.struct2[key],mergeToNewStruct = mergeToNewStruct, dontMergeNulls = dontMergeNulls, allowNewKeys = allowNewKeys)>
				<cfelse>
					<cfif not dontMergeNulls or arguments.struct2[key] is not "">
						<cfset resultStruct[key] = arguments.struct2[key]>
					</cfif>
				</cfif>
			<cfelseif outputWarnings>
				<cfoutput>key #key# does not exist<BR></cfoutput>
			</cfif>
		</cfloop>
	</cfif>

	<cfreturn resultStruct />
</cffunction>



<cffunction name="arrayOfStructuresToQuery">
	<cfargument name="theArray" required = true>
	<cfargument name="addArrayIndex" default=true>
<cfscript>
	// from dataManipulationLib.cfm modified by WAB to deal with structures which might not all have the same fields in them
	var colNames = "";
	var theQuery = queryNew("");
	var i=0;
	var j=0;
	var queryColNames = "";
	//if there's nothing in the array, return the empty query
	if(NOT arrayLen(theArray))
		return theQuery;
	//get the column names into an array =
	colNames = structKeyArray(theArray[1]);
	if (addArrayIndex) {
		arrayappend( colNames, "arrayIndex") ;
	}
	//build the query based on the colNames
	theQuery = queryNew(arrayToList(colNames));
	//add the right number of rows to the query
	queryAddRow(theQuery, arrayLen(theArray));
	//for each element in the array, loop through the columns, populating the query
	for(i=1; i LTE arrayLen(theArray); i=i+1){
		structFieldNames = structKeyArray(theArray[i]);
		queryColNames = theQuery.columnlist;
		for(j=1; j LTE arrayLen(structFieldNames); j=j+1){
			if (listfindnocase(queryColNames,structFieldNames[j]) is 0){
			queryaddcolumn(theQuery,structFieldNames[j],arrayNew(1));
			}
			querySetCell(theQuery, structFieldNames[j], theArray[i][structFieldNames[j]], i);
		}
		if (addArrayIndex) {
			querySetCell(theQuery, "arrayIndex", i, i);
 		}
	}
	return theQuery;
</cfscript>
</cffunction>

<cfscript>
function structureToQuery(theStructure){
	var array = arrayNew(1) ;
	array[1] =theStructure ;
	return (arrayOfStructuresToQuery (theArray = array,addArrayIndex = false)) ;

}
</cfscript>



<!---

WAB 2009/03 as part of the edit phrase widget
same code also used in download\downheader.cfm but hasn't been modded to use this function
QueryAggregateList

Can't think of a sensible name for this function
Also couldn't think where to put it so ended up here in structure functions

It is an aggregate function (like Max,Min,Sum,Count) which makes a comma separated list

You can think of it with an example
Imagine you have a query
CountryName, CountyName
England		Oxfordshire
England		Wiltshire
USA		Washington
USA		Iowa
Canada		Quebec

And you want to get a query
England		Oxfordshire,Wiltshire
USA		Washington,Iowa
Canada		Quebec

You could imagine the sql would be
select CountryName , List (CountyName)
from Regions
Group by CountryName

But since this SQL function does not exist, I have written this QoQ function which does it for you


This function groups up a query and makes the ungrouped field into a comma separated list --->
<cffunction name="QueryAggregateList">

	<cfargument name ="Query" type="query">
	<cfargument name ="groupBy" type="string" >
	<cfargument name ="Field" type="string">
	<cfargument name ="delimiter" type="string" default=",">

	<cfset var columnList = Query.columnList>
	<cfset var result = "">
	<cfset var keepThisRowID = "">

	<cfset queryAddColumn(Query,"keepThis","integer",arrayNew(1))>

	<!--- loop through query --->
	<cfoutput query ="query" group="#groupBy#">
		<cfset keepThisRowID = currentRow>
		<cfset querySetCell (Query,"keepthis",1, keepThisRowID)>
		<cfoutput>
			<cfif currentRow is not keepThisRowID>
				<cfset querySetCell (Query,field,Query[field][keepThisRowID] & delimiter & Query[field][currentRow],keepThisRowID)>
			</cfif>
		</cfoutput>
	</cfoutput>

	<cfquery name = "result" dbtype="query">
	select [#replace(columnList,",","],[","ALL")#]    <!--- nasty method of putting [] around easch field name --->
	from Query
	where  keepthis = 1
	</cfquery>

	<cfreturn result>

</cffunction>


	<!--- -------------------------------------------------- --->
	<!--- XmlToStruct --->
<!--- *        Application: newsight DataTypeConvert Component
*          File Name: DataTypeConvert.cfc
* CFC Component Name: DataTypeConvert
*            Support: ColdFusion MX 6.0, ColdFusion MX 6.1, ColdFusion MX 7
*         Created By: Artur Kordowski - info@newsight.de
*            Created: 02.06.2005
 --->
	<cffunction name="XmlToStruct" access="public" output="yes" returntype="struct" hint="Converts a XML document object or a XML file to struct.">

		<!--- Function Arguments --->
		<cfargument name="xmlObj"  required="no" type="any"                    hint="Parsed XML document object. Required if argument file is not set.">
		<cfargument name="file"    required="no" type="string"                 hint="Pathname or URL of the XML file to read. Required if argument xmlObj is not set.">
		<cfargument name="charset" required="no" type="string" default="UTF-8" hint="The character encoding in which the XML file contents is encoded.">

		<cfscript>

			/* Default variables */
			var i = 0;
			var n = 0;
			var struct     = StructNew();
			var xmlData    = "";
			var childLen   = "";
			var childList  = "";
			var childData  = "";
			var parentName = "";
			var parentText = "";
			var data       = "";
			var name       = "";
			var text       = "";

			if(IsDefined("arguments.file")) xmlData = XmlParseFile(arguments.file, arguments.charset);
			else                            xmlData = arguments.xmlObj;

			if(StructKeyExists(xmlData, "xmlRoot"))
				StructAppend(struct, XmlToStruct(xmlData.xmlRoot));

			else
			{
				childLen  = ArrayLen(xmlData.xmlChildren);
				childList = StructKeyList(xmlData);

				/* Set parent text to struct */
				parentName = UCase(RemoveInvalidChar(xmlData.xmlName));
				parentText = Trim(xmlData.xmlText);

				if(Len(parentText) AND childLen)
					struct[parentName & "_XMLTEXT"] = parentText;

				/* Loop over children */
				for(i=1; i LTE childLen; i=i+1)
				{
					data      = xmlData.xmlChildren[i];
					name      = UCase(RemoveInvalidChar(data.xmlName));
					text      = Trim(data.xmlText);
					childData = XmlToStruct(data);

					if(ListValueCount(childList, data.xmlName) GT 1)
					{
						n=n+1;

						if(StructIsEmpty(childData)) struct[name][n] = text;
						else                         struct[name][n] = childData;
					}

					else
					{
						if(StructIsEmpty(childData)) struct[name] = text;
						else                         struct[name] = childData;
					}
				}

				/* Loop over attributes */
				for(attr IN xmlData.xmlAttributes)
					struct[UCase(RemoveInvalidChar(attr))] = xmlData.xmlAttributes[attr];
			}

			/* Return struct */
			return struct;

		</cfscript>

	</cffunction>


	<!--- -------------------------------------------------- --->
	<!--- RemoveInvalidChar --->
	<cffunction name="RemoveInvalidChar" access="private" output="no" returntype="string" hint="Replace all non-ascii characters from XML name.">

		<!--- Function Arguments --->
		<cfargument name="string" required="yes" type="string"  hint="String with the XML name.">

		<cfscript>

			arguments.string = Replace(arguments.string, ":", "_", "ALL");             // Replace character before prefix
			arguments.string = REReplace(arguments.string, "[^[:ascii]]", "_", "ALL"); // Replace all non-ascii character

			/* Return string */
			return arguments.string;

		</cfscript>

	</cffunction>


<!---
WAB 2009/04/29 StructPOP
gets item from structure and the deletes the key from the structure
Was created when displaying parameters in forms.
Wanted to be able to look for keys in the structure, use the value and then at the end know what items there were left which I hadn't used
--->
<cffunction name="structPop" output="false">
	<cfargument name="struct">
	<cfargument name="key">

	<cfset var result = "">

	<cfif structKeyexists(struct,Key)>
		<cfset result = struct[Key]>
		<cfset structDelete (struct,Key)>
	</cfif>
		<cfreturn result>
</cffunction>


	<!--- NJH 2009/20/05 P-FNL079 - function that accepts a mapping structure and returns the inverse.
		ie person.personID is passed in with a a value of "contact.ID", you will get a contact structure returned with a key of ID and a value of "person.personID"
	 --->
	<cffunction name="transposeMappingStructure" access="public" returntype="struct" hint="Returns a mapping structure with the inverse mappings." output="false">
		<cfargument name="entityMappingStructure" type="struct" required="true"><!--- a structure containing various mapping structures. The values in the mapping structure should be of the following notation = struct.key --->

		<cfscript>
			var structureName = "";
			var structureKey = "";
			var entityMappingKey = "";
			var entity = "";
			var transposedEntityMapping = "";
			var entityStruct = structNew();
			var transposedMappingStruct = structNew(); // structure that holds all structure with their mappings
		</cfscript>

		<cfloop collection="#arguments.entityMappingStructure#" item="entity">
			<cfif isStruct(arguments.entityMappingStructure[entity])>
				<cfset entityStruct = arguments.entityMappingStructure[entity]>
				<cfloop collection="#entityStruct#" item="entityMappingKey">
					<cfset transposedEntityMapping = entityStruct[entityMappingKey]>
					<cfif listlen(transposedEntityMapping,".") eq 2>
						<cfset structureName = listFirst(transposedEntityMapping,".")>
						<cfset structureKey = listLast(transposedEntityMapping,".")>
						<cfif not structKeyExists(transposedMappingStruct,structureName)>
							<cfset transposedMappingStruct[structureName] = structNew()>
						</cfif>
						<cfset transposedMappingStruct[structureName][structureKey] = entity&"."&entityMappingKey>
					</cfif>
				</cfloop>
			</cfif>
		</cfloop>

		<cfreturn transposedMappingStruct>
	</cffunction>


<!---
WAB 2010/12/01
OK Not a structure function but didn't feel like creating an arrayFunctions cfc
 --->
<cffunction name="arrayMerge" output="false">
	<cfargument name="array1">
	<cfargument name="array2">

	<cfset var arrayItem = "">

	<cfloop array="#array2#" index="arrayItem">
		<cfset arrayAppend(array1,arrayItem)>
	</cfloop>

	<cfreturn array1>

</cffunction>

	<!--- arrayOfStructuresFindKeyValue
		WAB 2010/11/30
		searches an array Of Structures looking for a key with a particular value
		returns the index of the item containing the key
	 --->
	<cffunction name="arrayOfStructuresFindKeyValue" output="false">
		<cfargument name="array" required = "true">
		<cfargument name="key" required = "true">
		<cfargument name="value" required = "true">

		<cfset var result = 0>
		<cfset var i = 0>
		<cfloop index = "i" from = "1" to = "#arrayLen(array)#">
			<cfif structKeyExists (array[i],key)>
				<cfif array[i][key] is value>
					<cfset result = i>
					<cfbreak>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="getStructureKeysByRegExp" returntype="string" output="false">
		<cfargument name="struct" type="struct" required="true">
		<cfargument name="regExp" type="string" required="true">

		<cfscript>
			var result = "";
			var structKey = "";
		</cfscript>

		<cfloop list="#structKeyList(arguments.struct)#" index="structKey">
			<cfif reFindNoCase(arguments.regexp,structKey)>
				<cfset result = listAppend(result,structKey)>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

		<cffunction name="changeStructureKeys" output="false">
			<cfargument name = "structure" required="true" type="struct">
			<cfargument name = "mappingstructure" required="true" type="struct">

			<cfset var oldKey = "">

			<cfloop collection="#mappingStructure#" item="oldKey">
				<cfif structKeyExists (arguments.structure,oldKey)>
					<cfset arguments.structure[mappingstructure[oldKey]] = arguments.structure[oldKey]>
					<cfset structDelete (arguments.structure,oldKey)>
				</cfif>
			</cfloop>

			<cfreturn arguments.structure>

		</cffunction>


	<!--- NJH 2012/05/03 API - Function to remove undefined keys that may exist in a structure.. it removes them recursively..
		this is needed in the api because we pass things through alot by arguments.. and although an argument may be undefined, it's key will still exist.
		So, looping through arguments and trying to get the values may cause problems, because the value may be undefined
		http://www.bennadel.com/blog/1430-ColdFusion-ARGUMENTS-Keys-Always-Exist-Even-When-Not-Required.htm
	--->
	<cffunction name="removeUndefinedStructKeys" access="public" returntype="any" output="false">
		<cfargument name="data" type="any" required="true">

		<cfset var structKey = "">

		<cfif isStruct(arguments.data)>
			<cfloop collection="#arguments.data#" item="structKey">
				<cfif not structKeyExists(arguments.data,structKey)>
					<cfset structDelete(arguments.data,structKey)>
				<cfelseif isStruct(arguments.data[structKey])>
					<cfset removeUndefinedStructKeys(data=arguments.data[structKey])>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn>
	</cffunction>

	<!--- WAB 2013-01-17 during P-LEX075
		A function which does a structSort, but then creates an array based on the result of structSort
		I created it for manipulating CFC metadata - ordering functions by an index attribute, 	It is called from globalFunctions.getArrayOfCFCMethods()
		It might be useful for something else, so here it is!

		Example, imagine a structure

			{
			William = {age = 48, lastname = Bibby},
			Nathaniel = {age = 31, lastname = Hogeboom},
			Imre = {lastname= Haraszti }
			}

		calling orderStructureByAttribute (struct,"age","40")

		would return the array
			[
				{age = 31, lastname = Hogeboom},
				{age = 40, lastname= Haraszti },
				{age = 48, lastname = Bibby}

			]

		I note that in this exampe you lose the first name.  In my use case the name was also a member of the structure so it didn't matter.
		I added the default index option because I could not guarantee that the index attribue would be present, I guess that in most use cases it would be

	--->

	<cffunction name="orderStructureByAttribute" output="false">
		<cfargument name="structure" required="true">
		<cfargument name="pathToSubElement" required="true">
		<cfargument name="defaultValue" default=""> <!--- note, at moment only supported at top level - not if path to subelement contains . --->

		<cfset var method = '' />
		<cfset var FunctionsInOrderArray = '' />
		<cfset var result = '' />
		<cfset var methodName = '' />

		<cfif listLen(pathToSubElement,".") is 1 and defaultValue is not "">
			<cfloop collection="#structure#" item="methodName">
				<cfset method = structure[methodName]>
				<cfif not structKeyExists (method,pathToSubElement)>
					<cfset method[pathToSubElement] = defaultValue>
				</cfif>
			</cfloop>
		</cfif>

		<cfset FunctionsInOrderArray = structSort(structure,"numeric","ASC",pathToSubElement)>

		<cfset result = arrayNew(1)>

		<cfloop index="methodName" array= #FunctionsInOrderArray#>
			<cfset arrayAppend(result,structure[methodname])>
		</cfloop>

		<cfreturn result>

	</cffunction>



		<!---
		Sorts an array of structures based on a key in the structures.
		WAB 2013-04-30
		(from some code in the internet)
		Suprised that we don't already have a similar function, but can't find one
		 --->

	<cffunction name="arrayOfStructsSort" output="false">
		<cfargument name="arrayOfStructs" required="true">
		<cfargument name="Key" required="true" hint="Key to sort by">
		<cfargument name="SortOrder" default="asc" hint="asc|desc">
		<cfargument name="SortType" default="numeric" hint="Text|textnocase|numeric">

		<cfscript>
				var sortResult = "";
		        //make an array to hold the sort stuff
		        var sortStruct = {};
		        //make an array to return
		        var returnArray = arraynew(1);
		        //grab the number of elements in the array (used in the loops)
		        var count = arrayLen(arrayOfStructs);
		        //make a variable to use in the loop
		        var ii = 1;
		        //loop over the array of structs, building the sortStruct
		        for(ii = 1; ii lte count; ii = ii + 1)
		            sortStruct[ii] = arrayOfStructs[ii][key] ;
		        //now sort the structure
		        sortResult = structSort(sortStruct,sortType,sortOrder);
		        //now build the return array
		        for(ii = 1; ii lte count; ii = ii + 1) {
		            returnArray[ii] = arrayOfStructs[sortResult[ii]];
				}

		</cfscript>

		<cfreturn returnArray>

	</cffunction>


	<!--- WAB 2013-10-02  For CASE 437290
		A set of functions for accessing items in a structure by dot notation

		So rather than doing
		<cfset keyName = "xx.yy.zz">
		<cfset value = evaluate ("myStructure.#keyName#")>
		Which is rather nasty

		You can do
		<cfset value = structFind_DotNotation (myStructure,keyName)>

	---->

	<cffunction name="structFind_DotNotation" output="false">
	    <cfargument name="structure" >
	    <cfargument name="key" >

		<cfset var thisKey = '' />
		<cfset var pointer = structure>


		<cfloop index="thisKey" list="#key#" delimiters=".">
			<cfif structKeyExists (pointer,thisKey)>
				<cfset pointer = pointer[thisKey]>
			<cfelse>
				<cfreturn>
			</cfif>
		</cfloop>

		<cfreturn pointer>
	</cffunction>

	<cffunction name="structDelete_DotNotation" output="false">
	    <cfargument name="structure" >
	    <cfargument name="key" >

		<cfset var thisKey = '' />
		<cfset var pointer = structure>

		<cfloop index="thisKey" list="#application.com.globalFunctions.listAllButLast(key,".")#" delimiters=".">
			<cfif structKeyExists (pointer,thisKey)>
				<cfset pointer = pointer[thisKey]>
			<cfelse>
				<cfreturn>
			</cfif>
		</cfloop>

		<cfset structDelete (pointer,listLast(Key,"."))>

		<cfreturn >
	</cffunction>


	<cffunction name="structInsert_DotNotation" output="false">
	     <cfargument name="structure" >
	     <cfargument name="key" >
	     <cfargument name="value" >

		<cfset var thisKey = '' />
		<cfset var pointer = structure>

		<cfloop index="thisKey" list="#application.com.globalFunctions.listAllButLast(key,".")#" delimiters=".">
			<cfif not structKeyExists (pointer,thisKey)>
				<cfset pointer[thisKey] = {}>
			</cfif>
				<cfset pointer = pointer[thisKey]>
		</cfloop>

		<cfset pointer[listLast(key,".")] = value>

		<cfreturn >
	</cffunction>


	<!--- 2014-03-10 WAB
			created for use in iisAdmin.cfc, but of general use
	--->
	<cffunction name="areStructuresIdentical" hint="Does what its name suggests" output="false">
		<cfargument name="struct1">
		<cfargument name="struct2">
		<cfargument name="caseSensitive" default = "true">

		<cfset var result = true>
		<cfset var attr = "">

		<cfif structCount (struct1) is not structCount (struct2)>
			<cfset result = false>
			<cfreturn result>
		</cfif>

		<cfloop collection = "#struct1#" item="attr">
			<cfif not structKeyExists (struct2,attr) or not application.com.globalFunctions.areStringsEqual( struct1[attr], struct2[attr], caseSensitive)>
				<cfset result = false>
				<cfbreak>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>


	<!--- WAB 2016-11-11 I was sure that I must already have a function that does this, but can't find one anywhere.  
		Just makes a blank structure from a list --->	
	<cffunction name="listToStruct" output="false" hint="Takes a list and makes each item a key in a  structure.  Value defaults to blank but can be set to any contstant - such as true">
		<cfargument name="list" required="true"> 
		<cfargument name="delimiter" default=","> 
		<cfargument name="value" default=""> 
	
		<cfset local.result = {}>
		<cfloop index="local.i" list = "#list#" delimiters = "#delimiter#">
			<cfset result[i] = value>
		</cfloop>
		
		<cfreturn result>

	</cffunction>

</cfcomponent>

