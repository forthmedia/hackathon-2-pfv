/* Relayware. All Rights Reserved 2014 */
/*
File name:		versionAnnoucement.cfc
Author:			RJT
Date started:	01-03-2016

Description:	A CFC for handling the internal version announcement (i.e. displaying a "whats new" pdf when a user logs in for the first time)

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


*/

component accessors=true output=true persistent=false {


	public struct function checkLoggedInUserForVersionAnnouncement(){
		
		var returnStruct={needsInclude=false, include=""};
		if ( NOT structKeyExists(session,"versionAnnouncementCheckMade") or NOT session.versionAnnouncementCheckMade){
			session.versionAnnouncementCheckMade=true;
			var previousLogin=request.relayCurrentUser.PreviousLoginTime;
		
			if (previousLogin==""){ //if never logged in before
				return returnStruct; //no announcemetn if we've never logged in before
			}
		
			var versionQuery=new Query();
			versionQuery.setDatasource(application.sitedatasource);
			versionQuery.addParam(name="previousLogin",value=previousLogin,cfsqltype="CF_SQL_TIMESTAMP"); 
			var resultUnannounced = versionQuery.execute(sql="SELECT metadata from releasehistory where type='version' and DATEDIFF(second,created,:previousLogin)<=0 order by created ");  //
			var resultAnnounced = versionQuery.execute(sql="SELECT top 1 metadata from releasehistory where type='version' and DATEDIFF(second,created,:previousLogin)>0 order by created desc");  // the most recent announced version
			
			var versionAnnounced = eliminateNonNumericVersionsFromQuery(resultAnnounced); //the most recent announced version (or no version at all);
			
			if (arrayLen(versionAnnounced)==0){
				versionAnnounced[1]={yearVersion=0,majorVersion=0,minorVersion=0}; //the beginning of time
			}
			
			
			
			var versions = eliminateNonNumericVersionsFromQuery(resultUnannounced); 

			if (ArrayLen(versions)>0){
				//mostOldVersion and mostRecentVersion may be the same
				var mostRecentOldVersion=versionAnnounced[1];
				mostRecentOldVersion.minorVersion++; //incriment by the smallest version incriment to get an inclusive range
				var mostRecentVersion=versions[ArrayLen(versions)];
				
				checkForMoreRecentVersionRequest(mostRecentVersion); //potentially blow the cache
				
				
				var announcmentData=getAnnouncableVersion(mostRecentOldVersion,mostRecentVersion  );
				if (announcmentData.anyAnnouncements){
					returnStruct.needsInclude=true;
					returnStruct.include=announcmentData.ANNOUNCEMENTURL;
				}
			}
		}
		
		
		return returnStruct;

	}

	private Array function eliminateNonNumericVersionsFromQuery(required any versionQuery){
		var metaInfo = versionQuery.getPrefix();
		var resultSet = versionQuery.getResult(); 
		
		var versions=ArrayNew(1);
		
		for(var i=1;i<=metaInfo.recordcount;i++){
			var versionData=DeserializeJSON(resultSet.metadata[i]);
			if (versionData.minor EQ ""){
				versionData.minor=0;
			}
			if (isNumeric(versionData.year) && isNumeric(versionData.major) && isNumeric(versionData.minor)  ){
				ArrayAppend(versions, {yearVersion=versionData.year,majorVersion=versionData.major,minorVersion=versionData.minor});
			}
		}
		return versions;
	}

	public struct function getAnnouncableVersion(required struct startVersion, required struct endVersion){
		var returnStruct={anyAnnouncements=false, yearVersion="",majorVersion="",minorVersion="", announcementURL=""};
		
		var announcableVersionData=getAnnouncableVersionData();
		
		//run backwards through the announcableVersion list, first one we find if the announcement if its in range
		
		var scanCompleted=false;
		var versionIndex=ArrayLen(announcableVersionData);
		while(!scanCompleted){
			
			if (versionIndex<=0 OR version_LT_Version(announcableVersionData[versionIndex],startVersion)){
				//too far back, there is no announcement
				scanCompleted=true;
			}else if (version_LTE_Version(announcableVersionData[versionIndex], endVersion)){
				//found an annoucement
				scanCompleted=true;
				returnStruct.anyAnnouncements=true;
				returnStruct.yearVersion=announcableVersionData[versionIndex].yearVersion;
				returnStruct.majorVersion=announcableVersionData[versionIndex].majorVersion;
				returnStruct.minorVersion=announcableVersionData[versionIndex].minorVersion;
				returnStruct.announcementURL=announcableVersionData[versionIndex].announcementURL;
				
			}
			
			versionIndex--;
		}
		
		
		
		return returnStruct;
	}
	
	
	public boolean function version_LTE_Version(required struct version1,required struct version2 ){
		return (version1.yearVersion==version2.yearVersion && version1.majorVersion==version2.majorVersion && version1.minorVersion==version2.minorVersion)
			OR
			version_LT_Version(version1,version2)
		;
	}
	
	public boolean function version_LT_Version(required struct version1,required struct version2 ){
		return version1.yearVersion<version2.yearVersion
		OR
		(version1.yearVersion==version2.yearVersion AND
			(version1.majorVersion<version2.majorVersion OR
				(version1.majorVersion==version2.majorVersion AND version1.minorVersion<version2.minorVersion)
			)
		);
		
		
	}
	
	/**
	 * Blows the cache if a more recent version than ever asked for before is asked for. Since this almost certainly
	 * indicates a release
	 **/
	public void function checkForMoreRecentVersionRequest(required Struct version){
		if (!isDefined("application.announcableVersionData.mostRecentVersionQueried")){
			triggerVersionDataRefetch();
		}else if (version_LT_Version(application.announcableVersionData.mostRecentVersionQueried,version)){
			application.announcableVersionData.mostRecentVersionQueried=version;
			triggerVersionDataRefetch();
		}
		
	}
	
	public void function triggerVersionDataRefetch(){
		application.announcableVersionData.needsUpdating=true;
	}

	public Array function getAnnouncableVersionData(){
		if (!structKeyExists(application,"announcableVersionData") or !structKeyExists(application.announcableVersionData,"needsUpdating")){
			application.announcableVersionData.needsUpdating=true;
		}
		
		
		if (application.announcableVersionData.needsUpdating){
			lock scope="application" type="exclusive" timeout="30" {
				if (application.announcableVersionData.needsUpdating){ //if both inside and outside the lock in case several updates pile up against the lock
					var versionStruct={};
					var versionList=arrayNew(1);
					var xmlObj = new com.xmlfunctions();
					
					var XMLFile = "versionAnnouncement.xml";
					var coreXMLFilePath = application.paths.relayware & "/xml/" & XMLFile;
					
					var versionsData=xmlObj.readAndParseXMLFile (fileNameAndPath = coreXMLFilePath,changeCase_Name="lower");
					
					
					if (isDefined("versionsData.xml.announcedversions.version")){ //may not be if no announcable versions
						for(var i=1;i<=arrayLen(versionsData.xml.announcedversions.version);i++){
							var versionData=versionsData.xml.announcedversions.version[i];
							versionList[i]={yearVersion=versionData.XmlAttributes.year,majorVersion=versionData.XmlAttributes.major,minorVersion=versionData.XmlAttributes.minor, announcementURL=versionData.XmlAttributes.announcementURL};
						}
					}
					application.announcableVersionData.versionList=versionList;
					application.announcableVersionData.needsUpdating=false;	
				}
			}
			
		}
		return application.announcableVersionData.versionList;
		
	}
	
}