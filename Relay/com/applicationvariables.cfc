<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			applicationVariables.cfc
Author:
Date started:			2007-04-17

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-12-11 		WAB modified creation of application variables so that can handle structures in dot notation
2008-02-20 		WAB moved code from setApplicationComponentsV2.cfm into function loadApplicationComponents
2008-08-18	SWJ 	Modified call to componentUtils to avoid the need to call CFIDE version of these functions as this was causing
					a security issue and we have now locked down CFIDE/componentUtils from external access.

2008-08-24	WAB		Mods to application.fieldinfo to  add cfFormParameters field CR-TND561 Req8
2009-02-02	WAB		Mod to setApplicationComponents to show line number on failed items (and put message after name of file)
2009-06-23	WAB		added some functions with standardised names for reloading bits of the app
2009-06-24	WAB		var'ed  variable name in loadApplicationComponents
2009-07-15 	WAB changed name of fileanddirectory.cfc to security.cfc
2009-09-22	WAB		Mods to setApplicationComponents function to improve performance (componetutils change)
 	 	 	 	and moved locking inside the function
2009-09-27	WAB		deal with request timeouts in setApplicationComponents during boot up
2010-08-02	NJH		RW8.3 - don't load cfcs from userFiles unless they are in CfTemplates dir. Also, don't load application.cfc into memory.
2011-05-10	WAB 	added handling of a timeout as an attribute of a reload function
2010-11-26  PPB  	delete fundRequestActivityXML from application structure so it gets reloaded on next use
2011-07-27	NYB		P-LEN024 changed commcheckDetails.cfm to commDetailsHome.cfm
2011-11-10	WAB		Add logging of errors loading CFCs into application.com
2013-02-25	WAB		Add testSite value 4 - Demo (looks like a live site, but still has email redirection)
2013-11-25 	PPB 	Case 438158 add isComputed
2013-09-10	WAB		Case 441626 reload_ApplicationComponents (just )Reload changed Application components was not giving an error message if a cfc failed (worked if there was also a successful load)
2014-10-22	NJH		P-NEX001 use ascii chr 172 for broken pipe, to avoid endemic file corruption
2015-10-06 	WAB 	Jira PROD2015-41 alter getFieldInfo query in getTableInfoStruct() to add Add Max/Min columns to define ranges on numeric fields.
2015-11-04	WAB 	Fixed problem with format of cfdirectory lastmodifed in resetServerInitialisationObj ().  User server.locale()
2016-01-12	WAB		created a function setEntityTypeVariables() in which to put an existing piece of code
2016-14-04  DAN     Case 449078 - support for "datetime" data type for the API data type so it's not get set to "date" instead
2016-09-30	RJT		PROD2016-2402 Ensure that "uniqueIdentifier" (aka guid) is properly validated when coming into API
Possible enhancements:



 --->

<cfcomponent displayname="ApplicationVariables" hint="A single cfc where all functions which set/reset various application variables">

	<cffunction name="updateRelayTagStructure">
		<cfargument name="applicationScope" default="#application#">

		<cfset var relayTagObj = createObject("component","com.relayTags")>
		<cfreturn relayTagObj.updateRelayTagStructure(applicationScope = applicationScope)>

	</cffunction>



	<cffunction name="reLoadAllVariables">
			<cfargument name="applicationScope" default="#application#">
			<cfset reLoadAllVariables_BeforeCom(applicationScope = applicationScope)>
			<cfset reLoadAllVariables_AfterCom(applicationScope = applicationScope)>

			<cfreturn true>

	</cffunction>

	<!--- These are variables which need to be loaded before the com components are initialised --->
	<cffunction name="reLoadAllVariables_BeforeCom">
			<cfargument name="applicationScope" default="#application#">

			<cfset var tempStruct = "">

			<cfset setDataTypeVariables(applicationScope = applicationScope)>

			<cfset tempStruct = {}>
			<cfset tempStruct[0] =  {name= "Live",abbr = "Live"}>
			<cfset tempStruct[1] =  {name= "Customer Staging",abbr = "Staging"}>
			<cfset tempStruct[2] =  {name= "Development",abbr = "Dev"}>
			<cfset tempStruct[3] =  {name= "Internal Test",abbr = "Test"}>
			<cfset tempStruct[4] =  {name= "Demo",abbr = "Demo"}>
			<cfset applicationScope.EnvironmentLookUp = tempStruct>

			<cfreturn true>

	</cffunction>


	<cffunction name="reLoadAllVariables_AfterCom">
			<cfargument name="applicationScope" default="#application#">

			<cfset var sfMappingsfilePathAndName = '' />
			<cfset var i = '' />
			<cfset var column = '' />
			<cfset var getAllEntityTypes = '' />
			<cfset var tempRowStruct = '' />
			<cfset var GetOrderStatii = '' />
			<cfset var GetGenuineOrderStatii = '' />
			<cfset var GetFileTypes = '' />
			<cfset var getWebUserID = '' />
			<cfset var mappingXmlFile = '' />
			<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

			<cf_log file="boot" Text="reLoadAllVariables Start">
			<cfsetting requestTimeout = "300">


			<!--- Set some miscellaneous constant application variables --->
			<cfset applicationScope.delim1 = chr(172)> <!--- 2014-10-22	NJH		P-NEX001 use ascii chr 172 for broken pipe, to avoid endemic file corruption --->


			<cfset setCountryVariables(applicationScope = applicationScope)>

			<cfset setEntityTypeVariables (applicationScope = applicationScope)>

			<cfquery name="getLookupList">
				select lookupId, lookupTextID, fieldname, itemText from lookupList where lookupTextID is not null
			</cfquery>

			<cfset applicationScope.lookupId = {}>
			<cfset applicationScope.lookup = {}>

			<cfloop query="getLookupList">
				<cfset applicationScope.lookupId[fieldname][lookupTextID] = lookupID>
				<!--- <cfset applicationScope.lookup[lookupID] = lookupTextID> --->
				<cfset tempRowStruct = structNew()>
				<cfloop index="column" list = "#getLookupList.columnlist#">
					<cfset tempRowStruct[column] = getLookupList[column][currentrow]>
				</cfloop>
				<cfset applicationScope.lookup[lookupID] = temprowstruct >
			</cfloop>

				<cf_log file="boot" Text="FieldInfo Start">

				<!--- WAB 2008-05-21 new field structure
					NJH 2012/03/26 - moved to relayEntity and only call it when it is needed
				--->
				<cfset application.fieldInfo = structNew()>
				<cfset loadfieldAliasStructure(applicationScope = applicationScope)>

				<!--- <cfset updateFieldInfoStructure(applicationScope = ApplicationScope,updatecluster=false)> ---> <!--- WAB 2011-03-15 changed to false - reloading application variables across a cluster is dealt with separately --->

				<cf_log file="boot" Text="FieldInfo End">

				<cfset structDelete(applicationScope,"addressFormat")>

				<cfset applicationScope.com.relayELementTree.createElementTypeStructure	(applicationScope = applicationScope)>

				<!--- reset the structure for holding table definitions - this is populated as required by template\getTableDefinition.cfm --->
				<cfset 	applicationScope.tableDefinition = structNew()>


				<cf_log file="boot" Text="Flags Start">
				<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			      initialise the flagstructure for first time
				 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
				 <cfset applicationScope.com.flag.loadApplicationStructures(applicationScope = applicationScope)>
				<cf_log file="boot" Text="Flags End">


						<!--- create order status description array (WAB changed to struct 2006-06-26 since lots of blanks, all other code can handle struct instead of array)--->
						<CFSET applicationScope.StatusDescriptions = structNew()>

						<CFQUERY NAME="GetOrderStatii" DATASOURCE="#applicationScope.siteDataSource#">
							SELECT statusId
							      ,description
								  ,genuine
							  FROM status
						  ORDER BY statusId
						</CFQUERY>

						<CFLOOP QUERY="GetOrderStatii">
							<CFSET applicationScope.StatusDescriptions[statusId] = Description>
						</CFLOOP>

						<!--- create genuineorderstatuses list --->
						<CFQUERY NAME="GetGenuineOrderStatii" DATASOURCE="#applicationScope.siteDataSource#">
							SELECT statusID
							  FROM status
							 WHERE genuine = 1
						  ORDER BY statusID
						</CFQUERY>

						<CFSET applicationScope.GenuineOrderStatii = valuelist(GetGenuineOrderStatii.statusId)>

						<!--- filetypes structure --->
						<CFQUERY NAME="GetFileTypes" DATASOURCE="#applicationScope.siteDataSource#">
							SELECT fileTypeID, Type,path
							  FROM filetype
						</CFQUERY>
						<cfscript>
							applicationScope.filetypeid = structnew() ;

							for (i=1;i lte GetFileTypes.recordcount; i=i+1) {
								applicationScope.filetypeid[getfiletypes.type[i]] = getfiletypes.filetypeid[i] ;
							}


						</cfscript>

						<!--- cachedQueryControls --->
						<CFHEADER NAME="cache-control" value=3600>
						<cfparam name="applicationScope.cachedContentVersionNumber" type="numeric" default="0"> <!--- used by the content tool --->
						<cfparam name="applicationScope.cachedContentTimeSpan" type="string" default="#CreateTimeSpan( 0, 0, 20, 0 )#">
						<cfparam name="applicationScope.cachedProcessVersionNumber" type="numeric" default="0"> <!--- used by the redirector/process actions --->
						<cfparam name="applicationScope.cachedSelectScreenVersionNumber" type="numeric" default="0"> <!--- used by the content tool --->
						<cfparam name="applicationScope.cachedSecurityVersionNumber" type="numeric" default="0"> <!--- updated by security rights module (admin/usermanagement) and tested for in com.login.checkInternalPermissions--->
						<cfparam name="applicationScope.cachedReportTimeShortInMins" type="numeric" default="1"> <!--- 2006-07-10 SWJ added to control how long relayReports.cfc's cache the report data for --->
						<cfparam name="applicationScope.cachedReportTimeLongInDays" type="numeric" default="1"> <!--- 2006-07-10 SWJ added to control how long relayReports.cfc's cache the report data for --->

						<cf_log file="boot" Text="Settings Start">
						<!--- NJH 2011/02/28 P-FNL075 reload settings now before setting the following app variables that are dependant on settings. --->
						<cfset applicationScope.com.settings.loadVariablesIntoApplicationScope()>
						<cf_log file="boot" Text="Settings End">

						<!--- set the variables defining the emailmergefields --->
						<cfset setEmailMergeVariables(applicationScope = applicationScope)>

						<!--- set the variables defining the application starting templates used by remote.cfm and index.cfm --->
						<cfset setApplicationStartingTemplates(applicationScope = applicationScope)>

						<!--- setup language structure to use when working out the current language ID from lang in et.cfm --->
						<cfset setLanguageVariables(applicationScope = applicationScope)>

						<!--- WAB 2008-07-15  structure for easy access to organisation types, to be stored as an application variable
							keyed both on numeric id and text id
						--->
						<cfset setOrganisationTypeStructure(applicationScope = applicationScope)>

						<!--- 2013-10-16	YMA	Custom entities, store list in application variables--->
						<cfset setCustomEntitiesStructure(applicationScope = applicationScope)>

						<!--- WAB 2009-01-27 clear out the menuXMLDoc  (used in relayMenu.cfc) --->
						<cfset structDelete(applicationScope,"menuXMLdoc")>

						<!--- NJH 2010-04-07 P-PAN002 - should probably loop through various tables that may have an xml document defined --->
						<cfset structDelete(applicationScope,"opportunityXML")>

						<!--- PPB 2010-11-26 to force reload next time its used --->
						<cfset structDelete(applicationScope,"fundRequestActivityXML")>

						<!--- NJH 2014/06/02 - Connector - old salesforce connector variables. These can be removed once the old salesforce code is no longer used --->
						<!--- <cfif applicationScope.com.settings.getSetting("versions.salesforce") neq 2>
							<cfset sfMappingsfilePathAndName="#applicationScope.paths.relayware#\xml\sfMappings.xml">

							<cfif fileExists("#applicationScope.paths.abovecode#\xml\sfMappings.xml")>
								<cfset sfMappingsfilePathAndName="#applicationScope.paths.abovecode#\xml\sfMappings.xml">
							</cfif>

							<cffile action="read" file="#sfMappingsfilePathAndName#" variable="mappingXmlFile"> <!--- NJH 2010-04-12 P-PAN002 hold the sales force mappings in application scope --->
							<cfset applicationScope.sfMappingXML = xmlParse(mappingXmlFile)>

							<cfset structDelete(applicationScope,"sfObjects")> <!--- NJH 2010-04-12 P-PAN002 - structure that holds meta data for salesforce objects --->
							<cfset structDelete(applicationScope,"SFDCAPI")>
						</cfif> --->

						<cfset applicationScope.connector = {}> <!--- NJH 2015/12/17 reset the structure rather then delete it. --->
						<cfset structDelete(application,"componentLoaded")> <!--- NJH 2015/12/22 this will cause a reload of all components loaded through getObject --->

						<!--- reload the relayTag structure and table --->
						<cfset updateRelayTagStructure(applicationScope = applicationScope)>

						<!--- WAB 2016-01-29 Added this item --->
						<cfset applicationScope.com.relayTranslations.resetPhraseStructure()>

					<!--- when reloading application variables, also reload site structure--->
					<cfset applicationScope.com.relaycurrentsite.loadSiteDefinitions(updatecluster=false,applicationScope = applicationScope )>
					<cfset applicationScope.com.relaycurrentsite.loadElementTreeDefinitions(updatecluster=false,applicationScope = applicationScope )>

			<!--- NJH 2010-10-12 set the webuserid based on the unknownPersonId --->
			<cfquery name="getWebUserID" datasource="#applicationScope.siteDataSource#">
				select userGroupId from userGroup where personID =  <cf_queryparam value="#applicationScope.com.settings.getSetting(variablename = "miscellaneous.unknownpersonid", applicationScope = applicationScope)#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset applicationScope.webuserid = getWebUserID.userGroupId>


			<!--- 2016/03/14 GCC 448328 now replaced with a call in reload application variables as refreshing every request is causing 404 issues when under load --->
			<!--- This will only work when on internal URL as external URL access is blocked by directorySecurity - ignored for now --->
			<cfhttp url="#cgi.HTTP_HOST#/api/v1/?docs&reload=true" timeout="30" result="resultOutput">

			<CFSET applicationScope.appVariables = true>
			<CFSET applicationScope.appVariablesLoadedDate = now()>

			<cfset application.com.versionAnnouncement.triggerVersionDataRefetch()>
		<cfreturn true>

	</cffunction>


	<!---
		WAB 2012-01-10 create a structure that can be used to get information about datatypes
		Originally used for automatic upgrading of queries to cf_queryparam
		Creates a structure application.dataTypeLookup,	which is keyed on both sql names and cf_sql_datatype names
		each subStructure contains .sql_type, .cf_sql_type and .simple_Type
		simple_Type just has options numeric,text,date,binary  - and saves long winded case statements
		simple_Type  might need more granularity in future - not too sure about some types such as image and binary
		I could have created a stucture manually, but decided to do it a bit more dynamically querying sql directly for its datatypes
	 --->
	<cffunction name="setDataTypeVariables">
		<cfargument name="applicationScope" default="#application#">

		<cfset var dataTypes = "">
		<cfset var structureFunctions = createObject ("component","com.structureFunctions")>
		<cfset var cf_sql_dataTypes = "BIT,BIGINT,DECIMAL,FLOAT,INTEGER,NUMERIC,SMALLINT,TINYINT,DOUBLE,REAL,MONEY,MONEY4,CHAR,VARCHAR,LONGVARCHAR,TIME,TIMESTAMP,DATE,BLOB,CLOB,IDSTAMP,REFCURSOR'">

		<!--- NJH 2016/12/01 JIRA PROD2016-2608 - brought out bit as it's own simple data type. not entirely sure of the impact, but we shall see!! :) --->
		<cFQUERY NAME="dataTypes" datasource="#applicationScope.siteDataSource#">
			select
			name as sql,
			case
			when name = 'bit' then 'bit'
			when (name like '%date%' or name like '%time%') then 'DATE'
			when precision <> 0 then 'numeric'
			when name like '%binary%' then 'binary'
			when precision = 0 then 'text'
			else '' end as simple,
			'CF_SQL_' + case when name in (<cfqueryparam cfsqltype="cf_sql_varchar" value="#cf_sql_dataTypes#" list="true">) then name
			when name like '%char%' then 'VARCHAR'
			when name like '%text%' then 'LongVARCHAR'
			when name like '%binary%' then '??'
			when name like '%int%' then 'Integer'
			when name like '%date%' then 'TIMESTAMP'
			when name like '%uniqueidentifier%' then 'IDSTAMP'
			when name like '%image%' then 'blob'
			else '' end as cfsql
			 from sys.types where is_assembly_type = 0
		</cFQUERY>


		<cfset applicationScope.dataTypeLookup = structureFunctions.queryToStruct(query=dataTypes,key="sql")>
		<cfset structAppend(applicationScope.dataTypeLookup,structureFunctions.queryToStruct(query=dataTypes,key="cfsql"))>

	</cffunction>



	<cffunction name="setEntityTypeVariables">
		<cfargument name="applicationScope" default="#application#">

		<!--- WAB 2004-07-26 this is really the same as the flagEntityType query (now in flag.cfc) except it get all entity types
			not just those with flags.  Found that I needed to reference these and didn't want to tamper
			with the existing view
			NJH 2015/01/05 - removed the friendlyname column from the schemaTable view. Replaced with label
			NJH 2016/06/09	JIRA PROD2016-342 - add label_plural so that we can output the friendly plural name for an entity. Used in search results at the moment.
			WAB 2016/11		FormRenderer Added flagsExist into structure
		 --->
		<CFQUERY NAME="getAllEntityTypes" DATASOURCE="#applicationScope.sitedatasource#"> <!--- WAB 2010-09-15 changed datasource, schematable is now a view in the main db --->
			select 
				entitytypeid
				, entityname as tablename
				, nameexpression,uniquekey
				, label
				, [label_plural]
				, description
				, isNull ((select 1 from information_schema.columns where column_name = 'countryid' and table_name = schemaTable.EntityName),0) as hasCountryIDColumn
				, flagsExist
			FROM 
				schemaTable
			where
				(entityTypeID = 0 and entityname ='person')
				or
				entityTypeID <> 0
			order by EntityTypeid

		</CFQUERY>


		<cfset applicationScope.EntityTypeID = structNew()>
		<cfset applicationScope.EntityType = structNew()>		<!--- WAB 2005-03-02 store all info on entities --->
		<cfloop query="getAllEntityTypes">
			<cfset applicationScope.EntityTypeID[tablename] = EntityTypeid>
			<cfset tempRowStruct = structNew()>
			<cfloop index="column" list = "#getAllEntityTypes.columnlist#">
				<cfset tempRowStruct[column] = getAllEntityTypes[column][currentrow]>
			</cfloop>
			<cfset applicationScope.EntityType[entityTypeID] =temprowstruct >	<!--- WAB sept 2006 I did try to key this by entityName as well, but it caused some bugs in existing code where it was looping through the structure and ending up with twice as many items as expected--->

		</cfloop>

	</cffunction>

	<!--- Set all the country application variables
		moved from setApplicationVariables.cfm WAB 2007-05-21
	 --->
	<cffunction name="setCountryVariables">
		<cfargument name="applicationScope" default="#application#">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->
		<cfset var getcountries = "">
		<cfset var getCountryGroups = "">
		<cfset var tempCountryList = "">
		<CFSET var tempCountryNameArray = arrayNew(1)>
		<CFSET var tempCountryISOArray = arrayNew(1)>
		<CFSET var tempCountryLookUpFromIsoCodeStr = structNew()>
		<CFSET var tempCountryIDLookUpFromIsoCodeStr = structNew()>
		<CFSET var tempCountriesInCountryGroup = arrayNew(1)>
		<CFSET var tempCountryGroupsBelongedTo = arrayNew(1)>

		<!--- create country arrays  CountryName [I] and CountryISO [i] --->
		<CFQUERY NAME="getCountries" datasource="#applicationScope.siteDataSource#">
			Select countryDescription, CountryID, ISOcode from country
		</CFQUERY>


		<CFLOOP query="getCountries">
			<CFSET tempCountryNameArray[countryid] = countryDescription>
			<CFSET tempCountryISOArray[countryid] = ISOCode>
			<cfif isocode is not "">
				<cfset tempCountryLookUpFromIsoCodeStr[isocode] = countryDescription>
				<cfset tempCountryIDLookUpFromIsoCodeStr[isocode] = countryID>
			</cfif>
		</cfloop>

		<CFSET applicationScope.CountryName = tempCountryNameArray>
		<CFSET applicationScope.CountryISO = tempCountryISOArray>
		<CFSET applicationScope.CountryLookUpFromIsoCodeStr = tempCountryLookUpFromIsoCodeStr>
		<CFSET applicationScope.CountryIDLookUpFromIsoCodeStr = tempCountryIDLookUpFromIsoCodeStr>



			<cFQUERY NAME="getCountryGroups" datasource="#applicationScope.siteDataSource#">
			select countryMemberid, cg.countrygroupid
			from country c inner join countrygroup as cg on c.countryid = cg.countryMemberid
			where isNUll(c.isocode,'') <> ''
			order by countryMemberid
			</CFQUERY>
			<cfoutput query = "getCountryGroups" group = "countryMemberid">
				<cfset tempCountryList = "">
				<cfoutput>
					<cfset tempCountryList = listappend(tempCountryList,countryGroupID)>
				</cfoutput>
				<CFSET tempCountryGroupsBelongedTo[countryMemberID] = tempCountryList	>
			</cfoutput>
				<cfset applicationScope.CountryGroupsBelongedTo = tempCountryGroupsBelongedTo>

		<!--- get the list of countries in each region--->
		<!---
		WAB 2007-04-12
		WAB replaced with single query and a cfoutput with group - much quicker
		 --->
			<cFQUERY NAME="getCountries" datasource="#applicationScope.siteDataSource#">
				select cg.countrygroupid , cg.countrymemberid
				from countrygroup as cg inner join country c on c.countryid = cg.countrymemberid
				where cg.countrygroupid  <> 0
				order by  cg.countrygroupid
			</CFQUERY>

			<cfoutput query = "getCountries" group = "countrygroupid">
				<cfset tempCountryList = "">
				<cfoutput>
					<cfset tempCountryList = listappend(tempCountryList,countryMemberID)>
				</cfoutput>
				<CFSET tempCountriesInCountryGroup[countryGroupID] = tempCountryList	>
			</cfoutput>
				<CFSET applicationScope.CountriesInCountryGroup = tempCountriesInCountryGroup>

		<cfreturn true>
	</cffunction>

	<cffunction name="setLanguageVariables">
		<cfargument name="applicationScope" default="#application#">

			<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->
			<cfset var getLanguages = '' />

			<CFQUERY NAME="getLanguages" DATASOURCE="#applicationScope.siteDataSource#">
				Select Language, languageid, buttonid, ISOCode, localLanguageName
				from Language order by languageID
			</CFQUERY>

			<cfscript>
				applicationScope.languageIDLookupStr = structNew();
				applicationScope.languageISOCodeLookupStr = structNew();
				applicationScope.languageLookupFromISOCodeStr = structNew();
				applicationScope.languageLookupFromIDStr = structNew();
				applicationScope.languageIDLookupFRomNameStr = structNew();   /* PPB corrected typo (was languageIDLookupStr again)*/
			</cfscript>

			<cfoutput query="getLanguages">
				<cfset applicationScope.languageIDLookupStr[ISOCode] = languageid>
				<cfset applicationScope.languageISOCodeLookupStr[languageid] = ISOCode>
				<cfset applicationScope.languageLookupFromISOCodeStr[ISOCode] = language>
				<cfset applicationScope.languageLookupFromIDStr[languageid] = language>
				<cfset applicationScope.languageIDLookupFRomNameStr[language] = languageid>
			</cfoutput>


			<cfreturn true>
		</cffunction>


	<cffunction name="resetServerInitialisationObj" >
		<cfargument name="onlyIfChanged" default="false">
		<cfargument name="applicationScope" default="#application#">

		<cfset var directoryToSearch = "#applicationScope.paths.relay#\com">
		<cfset var dir = "">
		<cfset var dateLastModified = "">

		<cfdirectory name="dir" directory="#directoryToSearch#" filter="serverinitialisation.cfc">

		<!---
		WAB 2012-02-09
		Problems with the date returned from cfdirectory.
		The format seems to be immune to changes in locale and is coming back in DD/MM format on most of our dev boxes
		Was just doing lsparseDateTime, but this was screwing up if browser was set to EN-US (which would set the server locale to EN-US and make lsparsedatetime bring back the wrong answer)
		Am going to have to hard code to EN-UK until I find out more.

		Also seemed to be a problem with my comaring dates using LT, so have changed to use datediff

		WAB 2015-11-04
		Have worked out what is going on.  cfdirectory always uses trhe underlying server locale.  So if I store details of that locale in server scope (applicationMain.cfc) then all should be fine
		 --->
		<cfset dateLastModified = lsparsedatetime(dir.datelastmodified,server.locale)>

		<cfif (not OnlyIfChanged or (structKeyExists(server,"serverInitialisationObj") and datediff("s",server.serverInitialisationObj.dateLoaded,dateLastModified) gt 0 ))>
			<cfset structDelete (server,"serverInitialisationObj")>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>

	<cffunction name="loadApplicationComponents" >
		<cfargument name="onlyIfChanged" default="false">
		<cfargument name="doServerInitialisationObj" default="false">	<!--- not done when there is a flush=, because then it would happen twice in a request--->
		<cfargument name="verbose" default="false">
		<cfargument name="applicationScope" default="#application#">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

		<cfscript>
		var tempComStruct = structNew() ;
		var applicationComExists = structKeyExists(applicationScope,"com");
		var resultStruct = {lockerror =false,numberfailed = 0,numberLoaded = 0,message="",verboseMessage="",loadedArray = arrayNew(1),errorsArray = arrayNew(1),notcachedArray = arrayNew(1) };
		var cfcs = "";
		var userOrCore = "";
		var cfcStruct = "";
		var check = "" ;
		var cfcname = "" ;
		var starttime = now ();
		var lastUpdated = '';
		var componentPath = '' ;
		var tempObject = '' ;
		var tempObjectMetaData = '' ;
		var tempErrorStruct = '' ;
		var cfcatchArray = arrayNew(1) ;
		var thisCatch = '' ;
		var error = '' ;
		var applockname = "LoadComponents_#application.applicationName#" ; // WAb 2009-07-14 added applicationname to the lock name to prevent locking across apps
		</cfscript>

		<cfif structKeyExists(arguments,"refreshCache")>
			<cfset arguments.onlyIfChanged = not arguments.refreshCache>  <!--- for backwards compatibilty with remote admin tools--->
		</cfif>

		<cfset structDelete(application,"componentLoaded")> <!--- NJH 2015/12/22 this will cause a reload of all components loaded through getObject --->

		<cfsetting requestTimeout = "300">

		<cfif not applicationComExists or not structKeyExists (applicationScope.com,"LastSuccessfulLoadTime")> <!--- TBD second part of if only for when this code first wrote, can be removed --->
			<cfset arguments.onlyIfChanged = false>
			<cfset lastUpdated = 0>
		<cfelse>
			<cfset lastUpdated = applicationScope.com.LastSuccessfulLoadTime>
		</cfif>

		<cfset structDelete(application,"componentLoaded")> <!--- NJH 2015/12/22 this will cause a reload of all components loaded through getObject --->

		<cftry>
			<cflock timeout="0"  throwontimeout="Yes" name="#applockname#" type="EXCLUSIVE">
				<cftry>
					<cfset cfcs = {core = getcfcs(relativePath = "relay\com",applicationScope = applicationScope),user = getcfcs(relativePath = "code\cftemplates",applicationScope = applicationScope)}>

						<!--- loop through all items--->
						<cfloop index="userOrCore" list = "user,core">
							<cfloop item="cfcname" collection = "#cfcs[userOrCore]#">
								<cfset cfcStruct = cfcs[userOrCore][cfcname]>
								<cfif userOrCore is "User">
								<!--- look for a core item with the same name
									if there is one then mark it as being extended, so we don't over write the user one
									We do end up loading it twice at the moment, which isn't a big issue I don't think
								--->
									<cfset check = structFindValue(cfcs.core,cfcs.user[cfcname].name)>
									<cfif arrayLen(check)>
										<cfset check[1].owner.extendedby = cfcs.user[cfcname].path>
									</cfif>
								</cfif>

								<cfif (not onlyIfChanged or cfcStruct.lastupdated gt applicationScope.com.LastSuccessfulLoadTime) and cfcStruct.name is not "application">
									<cfif structKeyExists (cfcStruct,"extendedby")>
										<cfset componentPath = cfcStruct.extendedby>
									<cfelse>
										<cfset componentPath = cfcStruct.path>
									</cfif>

									<cftry>
										<cfset tempObject = createObject("component",componentPath)>
										<cfset tempObjectMetaData = getMetaData(tempObject)>
										<!---  If component has attribute cache="false" then we don't load --->
										<cfif (not structKeyExists(tempObjectMetaData,"cache") or tempObjectMetaData.cache is true)>
											<!--- Now we check for an intialise function, if there is one it is run with parameter application scope --->
											<cfif arrayLen(structfindvalue(tempObjectMetaData,"initialise"))>
												<cfset tempObject.initialise(applicationScope = applicationScope)>
											</cfif>

											<cfset tempComStruct[cfcStruct.name] = tempObject>
											<cfset resultStruct.numberLoaded ++>
											<cfset arrayAppend(resultStruct.loadedArray,componentPath)>
										<cfelse>
											<cfset arrayAppend(resultStruct.notcachedArray,componentPath)>
										</cfif>

										<cfcatch>
											<!---  WAB 2009-10-27 special case for a timeout, don't want application.com to be part built so do a rethrow --->
											<cfif (findnocase("The request has exceeded the allowable time limit",cfcatch.message ) ) >
												<cfrethrow >

											<cfelse>
												<cfset resultStruct.numberfailed = resultStruct.numberfailed +1 >
												<cfset tempErrorStruct = {component = componentPath, message = cfcatch.message, detail = cfcatch.detail}>
												<cfif (structKeyExists(cfcatch,"line"))>
													<cfset tempErrorStruct.line = cfcatch.line>
												</cfif>
												<cfset arrayAppend(resultStruct.errorsArray,tempErrorStruct)>
												<cfset arrayAppend(cfcatchArray,cfcatch)>
													<!---
													 WAB 2007-09-17 On a live site possibly we should just completely fall over, otherwise we may be left with 1/2 an application!
													Don't release this until this has been discussed widely
													writeoutput("Application Failed to Load. (#componentToLoad#)")
													some abort function abort ;
													--->
											</cfif>
										</cfcatch>
									</cftry>

									<cfset doBootLog("#componentPath#  done")>
								</cfif>


							</cfloop>

						</cfloop>

				<cfif (not applicationComExists)>
			 		<cfset 	applicationScope.com = structNew()>
		 			<cfset applicationScope.com.LastSuccessfulLoadTime = 0>
				</cfif>


				<!--- copy temp objects into application scope--->
				<cfloop item="cfcname" collection = "#tempComStruct#">
					<cfset applicationScope.com[cfcname] = tempComStruct[cfcname]>
				</cfloop>
				<!--- only reset the load time if all items have loaded successfully --->
				<cfif not arrayLen(resultStruct.errorsArray)>
					<cfset applicationScope.com.LastSuccessfulLoadTime = startTime>
				</cfif>

				<!---  WAB 2011-11-14 any caught errors need to be logged --->
				<cfloop index="thisCatch" array="#cfCatchArray#">
					<cfset applicationScope.com.errorHandler.recordRelayError_Warning(severity="ColdFusion",catch=thisCatch,Type="com Component Load Error")>
				</cfloop>


				<cfif doServerInitialisationObj>
					<cfif resetServerInitialisationObj (applicationScope = applicationScope, onlyIfChanged = "true")> <!--- always only reload if changed - otherwise  gets reloaded all the time and clustering fails until such time as that server is hit again --->
						<cfset arrayAppend(resultStruct.loadedArray,"serverInitialisationObj reset")>
					</cfif>
				</cfif>

				<cfif arrayLen(resultStruct.errorsArray)>
					<cfset resultStruct.message= resultStruct.message & "<font color=red>" >
					<cfloop index="error" array="#resultStruct.errorsArray#">
						<cfset resultStruct.message= resultStruct.message & "#error.component# Failed<BR>">
						<cfif applicationScope.testSite is 2>
										<cfset resultStruct.message = resultStruct.message & "&nbsp;&nbsp;#error.message# <BR>&nbsp;&nbsp;#error.detail#<BR> ">
								<cfif (structKeyExists(error,"line"))>
										<cfset resultStruct.message = resultStruct.message & " Line: #error.line#<BR>">
								</cfif>
						</cfif>
					</cfloop>
					<cfset resultStruct.message= resultStruct.message & "</font>" >
				</cfif>
				<cfif verbose>
					<cfloop index="componentPath" array="#resultStruct.loadedArray#">
						<cfset resultStruct.message= resultStruct.message & "#componentPath#<BR>">
					</cfloop>
					<cfloop index="componentPath" array="#resultStruct.notCachedArray#">
						<cfset resultStruct.message= resultStruct.message & "#componentPath# not cached<BR>">
					</cfloop>
				<cfelse >
					<cfif arrayLen(resultStruct.loadedArray)>
						<cfset resultStruct.message = resultStruct.message  & "#arrayLen(resultStruct.loadedArray)# Loaded<BR>">
					</cfif>

				</cfif>



					<!--- WAB 2006-06-14
						wanted to have a way that particular functions could be made available for evaluation within translations and other places
						where we want to evaluate things.
						I block anything in application.com being evaluated to prevent accidental catastophes
						I am going to alias the cfcs which contain allowed functions into another application scope (application.FunctionsAllowedInPhrases)
						and then before evaluation is done I will alias this as "function"

						I have coded it so that we can merge together a number of different cfcs into this group (not sure if it will be of any use)
					 --->

			 		<cfset applicationScope.com.FunctionsAllowedInPhrases = applicationScope.com.structureFunctions.structMerge(applicationScope.com.FunctionsAllowedInPhrases,applicationScope.com.redirectorAllowedFunctions)>

					<cfcatch>
						<!--- whole load failure so send a warning email and rethrow--->
				<!--- 		<cf_log file="boot" Text="Com Fail"> --->
						<cfset 	sendWarningEmail (subject="Components Load Error",dump = cfcatch)>
						<cfrethrow>
					</cfcatch>
				</cftry>

					<cfset 	resultStruct.loadTime = datediff("s",startTime,now())>

					<cfif resultStruct.numberFailed is not 0 and not applicationComExists>   <!--- send warnings if there is a failure at boot up --->
						<!--- partial load failure during boot up, send a warning email
						could limit this to live sites
						--->
						<cfset 	sendWarningEmail (subject="Component Failed to Load ",dump = resultStruct,applicationScope = applicationScope)>
					</cfif>

			</cflock>

			<cfcatch>
				<cfif cfcatch.type is "lock" and cfcatch.lockName is applockname>
						<cfset resultStruct.lockError = true>
				<cfelse>
					<cfrethrow>
				</cfif>
			</cfcatch>
		</cftry>


		<cfreturn resultStruct>

	</cffunction>

	<cffunction name="getCFCs">

		<!---
		gets Structure of all the cfcs in a particular directory
		Structure has
			lastupdated -
			path - relative path to cfc in dot notation  eg: content.cftemplates.login.  This is also the key
			name - without the .cfc    eg: login
		--->

		<cfargument name="relativepath">  <!--- code\cftemplates or relay\com --->
		<cfargument name="updatedSince" default = "">
		<cfargument name="applicationScope" default = "#application#">

		<cfset var dir = "">
		<cfset var cfcPath = "">
		<cfset var result={}>
		<cfset var firstPartOfPath = listfirst(relativepath,'\')>  <!--- eg code or relay --->
		<cfset var restOfPath = listRest(relativepath,'\')>  <!--- eg com, cftemplates --->

		<cfset var directoryToSearch = "#applicationScope.paths[firstPartOfPath]#\#restOfPath#">


		<cfdirectory name="dir" directory="#directoryToSearch#" filter="*.cfc" sort="name asc" recurse="true">

		<cfloop query="dir">
			<!--- convert the directory to a relative path --->
			<cfset cfcPath =  replace(replace(directory & "\" ,applicationScope.paths[firstPartOfPath],firstPartOfPath),"\",".","ALL") & replace(name,".cfc","","ALL")>
			<cfset cfcPath = rereplacenocase(cfcpath,"\ARelay.","","ONE")>  <!--- remove the relay. bit, this is superfulous and the relay mapping could be removed--->
			<cfset result[cfcpath] = {path = cfcpath,name=replace(name,".cfc","","ALL"),lastupdated = datelastmodified}>
		</cfloop>

		<cfreturn result>
	</cffunction>


<!--- WAB 2009-10-27 CFScript can't do rethrow, so need own function --->
	<cffunction name="rethrow">
			<cfargument name="exception" default="">
			<cfthrow object="#arguments.exception#">
	</cffunction>

		<cffunction name="sendWarningEmail">
			<cfargument name="subject" default="">
			<cfargument name="Body" default="">
			<cfargument name="dump" default="#structNew()#">
			<cfargument name="applicationScope" default="#application#">

			<cfmail from="william@foundation-network.com" to="william@foundation-network.com" subject = "#subject#  #cgi.server_name# (#applicationScope.applicationname#) #cgi.script_name# #now()#" type="html">
				<cfoutput>#body# <p></cfoutput>
					<cfdump var="#dump#">
				<cfdump var="#cgi#">
			</cfmail>


		</cffunction>

	<cffunction name="setBlacklistVar">

		<cfset var counter = '' />
		<cfset var getBlackList = '' />

		<CFQUERY NAME="getBlackList" DATASOURCE="#application.siteDataSource#">
			select fieldname, data
				from BlackListDef inner join
				Blacklist on blacklistdef.FieldID = Blacklist.FieldID
				order by fieldname
		</CFQUERY>

		<cfscript>
			application.Blackliststr = structNew();
		</cfscript>
		<cfset counter = 0>
		<cfoutput query="getBlackList">
			<cfset counter = counter + 1>
			<cfset application.Blackliststr.fieldname.counter = data>
		</cfoutput>
	</cffunction>

	<!--- 	fieldinfostructure -
			being developed to allow a single place to store information about fields
			currently just used for blacklisting, but could store details of masks, jsVerification, default sizes etc.

			2012-04-16 WAB Major modifications to support API project
			Perhaps ought to be moved into com.relayEntity.cfc
	 --->

	<cffunction name="loadfieldAliasStructure">
		<!--- 	Load alias structure into memory.
				Aliases are defined in modEntityAlias table
				This allows us to quickly look up aliases and find which table/field they refer to.
				Use getFieldInfoStructureByAlias() function to actually access the fieldInfoStructure
				Aliases in the form #fieldname#_#tablename# are dealt with automaticaly in getFieldInfoStructureByAlias()
				Actually I think that the only place we use this is to alias email as insEmail!
		 --->
		<cfargument name="applicationScope" default="#application#">

		<cfset var tempStruct = {}>
		<cfset var getFieldAliases = "">

		<CFQUERY NAME="getFieldAliases" DATASOURCE="#application.siteDataSource#">
			select
				f.tablename ,
				f.fieldname ,
				modentityFieldAlias
			from
				modentitydef f
					inner Join
				modentityFieldAlias a	on f.modentityid = a.modentityid
			where
				fieldname <> 'Whole Record'
			order by f.tablename , f.fieldname
		</CFQUERY>

		<cfloop query = "getfieldAliases">
			<cfset tempStruct[modentityFieldAlias] = {tableName=getfieldAliases.tablename, fieldName=getfieldAliases.fieldname}>
		</cfloop>

		<cfset application.FieldAlias = tempStruct>

	</cffunction>


	<cffunction name="getFieldInfoStructureByAlias">
		<cfargument name="alias" type="string" required="true">

		<cfset var result = {isOK = false}>

		<!--- first look in alias Structure (which is loaded at boot time), then look for --->
		<cfif structKeyExists (application.FieldAlias,alias)>
			<cfset result = getFieldInfoStructure(tablename = application.FieldAlias[alias].tablename,fieldname = application.FieldAlias[alias].fieldname)>
		<cfelseif listlen(alias,"_") gt 1 and structKeyExists(application.EntityTypeID,listfirst(alias,"_"))>
			<cfset result = getFieldInfoStructure(tablename = listFirst(alias,"_"),fieldname = listRest(alias,"_"))>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="getFieldInfoStructure">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="fieldName" default="" required="true">

		<cfset var result = '' />

		<!--- Get the table structure (will either come from application scope or be loaded from db if required) --->
		<cfset var tableStruct = getTableInfoStructure (tablename)>

		<!--- if this field exists in the table structure then return it, otherwise returns isOK = false --->
		<cfif structKeyExists (tableStruct,fieldname)>
			<cfset result = tableStruct[fieldname]>
		<cfelse>
			<cfset result = {isOK = false}>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!---
		creates and returns the tableInfo structure
		structure contains a substructure for each field in the table [keyed on both actual database fieldname and APIname]
		each field structure has database information (length, datatype, nullable etc) and metadata stored in the modEntityDef table (eg cfformparameters)
	--->

	<cffunction name="getTableInfoStructure">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="reload" type="boolean" default="false">

		<cfset var getFieldInfo = "">
		<cfset var updateAll = false>
		<cfset var tempTableStruct = {} />
		<cfset var item = '' />
		<cfset var result = '' />
		<cfset var column = '' />
		<cfset var getFieldAliases = '' />
		<cfset var getBlackListItems = '' />
		<cfset var regExpobject =  CreateObject("component","com.regexp")>  <!--- 2008-09-29 WAB needed when booting --->
		<cfset var apiDataType = "">

		<cfif not structKeyExists (application.fieldInfo,tablename) or reload>

			<!---
				NJH 2012/03/26 changed fieldname to name so that we are consistent with the flag structure
			 	WAB 2012-04-12 altered so that doesn't rely on existence of field in the modEntitydef table, works off information_schema
			 	WAB 2015-10-06 Jira PROD2015-41  Add Max/Min columns to define ranges on numeric fields.  Also adjusted the maxLength of numeric types to allow for decimal point and negative sign
			--->
			<CFQUERY NAME="getFieldInfo" DATASOURCE="#application.siteDataSource#">
				declare @ten DECIMAL(38) = 10, @two DECIMAL(38) = 2
				select
					c.table_name as tablename,
					c.column_name as name,
					c.table_name +  '.' + c.column_name as fullfieldname,
					case when blacklistTypeId is not null then 1 else 0 end as hasBlackList,
					blacklistTypeId,
					c.data_Type,
					case
						when c.numeric_precision is not null then
							c.numeric_precision +
							case when data_Type = 'tinyint' then 0 else 1 end +   /* add one for the negative sign (except tinyint) */
							case when c.numeric_Scale = 0 then 0 else 1 end			/* add one for the decimal point */
						else
							isNull(c.character_Maximum_Length,0)
						end
					as MaxLength,
					<!--- max/min values. I could have hard coded them by data type, but I rather like calculating it!
						Note that returned as a varchar because some items have decimals and some items do not
					--->
					case
						when data_Type like 'tinyint' then
							'0'
						when data_Type like '%int' then
							CONVERT(VARCHAR (40), - (power (@two,(8*st.length)-1) ) )
						when numeric_precision is not null then
							CONVERT(VARCHAR (40), -(POWER (@ten , (NUMERIC_PRECISION - NUMERIC_SCALE)) -1))  + CASE WHEN NUMERIC_SCALE IS NOT NULL THEN '.' + CONVERT(VARCHAR (40), POWER (@ten,NUMERIC_SCALE) -1) ELSE '' END

						ELSE
							NULL

						end
					as [min],
					case
						when data_Type like 'tinyint' then
							CONVERT(VARCHAR (40), power (@two,(8*st.length)) -1 )
						when data_Type like '%int' then
							CONVERT(VARCHAR (40), power (@two,(8*st.length)-1) -1 )
						when numeric_precision is not null then
							CONVERT(VARCHAR (40), (POWER (@ten , (NUMERIC_PRECISION - NUMERIC_SCALE)) -1))  + CASE WHEN NUMERIC_SCALE IS NOT NULL THEN '.' + CONVERT(VARCHAR (40), POWER (@ten,NUMERIC_SCALE) -1) ELSE '' END
						end
					as [max],
					case when exists (select 1 from validFieldValues where fieldname = c.table_name+'.'+c.column_name) then 1 else 0 end  as hasValidValues,
					case when exists (select 1 from lookupList where fieldname in (c.column_name,c.table_name+c.column_name)) then 1 else 0 end as hasLookupList,
					dbo.getForeignKeyForTableColumn(c.table_name,c.column_name) as foreignKey,
					CFFormParameters,
					apiName,
					'field' as fieldType,
					isOK = 1,
					case when IS_NULLABLE='No' then 0 else 1 end as IsNullable,
					case when COLUMN_DEFAULT IS NULL then 0 else 1 end as hasDefault,
					case when COLUMN_DEFAULT like '%getDate()%' then 'getDate()' else replace(replace(replace(COLUMN_DEFAULT,'(',''),')',''),'''','') end as defaultValue,
					COLUMNPROPERTY(object_id(c.TABLE_NAME), c.COLUMN_NAME, 'IsIdentity') as isIdentity,
					'cf_sql_'+case when c.data_type = 'int' then 'integer' when c.data_type in ('nvarchar','ntext','text','uniqueidentifier') then 'varchar' when c.data_type='datetime' then 'timestamp' else c.data_type end as cfsqltype,
					COLUMNPROPERTY(object_id(c.TABLE_NAME), c.COLUMN_NAME, 'IsComputed') as isComputed				<!--- 2013-11-25 PPB Case 438158 --->
				from
					information_schema.columns  c
						inner join
					sysTypes st on c.data_type = st.name
						left Join
					modentitydef f on f.tablename= c.table_name and f.fieldname = c.column_name
						left join
						(information_schema.table_constraints tc
							inner join
						information_schema.constraint_column_usage ccu on tc.constraint_name = ccu.constraint_name and constraint_Type = 'Primary Key')
						on  tc.table_name =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >  and ccu.column_name = c.column_name
				where
					c.table_name =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</CFQUERY>

			<cfloop query = "getfieldInfo">

				<cfset item = structNew()>
				<cfloop index="column" list = "#getfieldInfo.columnlist#">
					<cfset item[column] = getfieldInfo[column][currentrow]>
				</cfloop>
				<cfset item.simpleDataType = application.dataTypeLookup[data_type].simple>

				<cfset apiDataType = "text">

				<cfif data_Type contains "int">
					<cfset apiDataType = "integer">
				<cfelseif data_Type eq "datetime"> <!--- Case 449078 --->
				    <cfset apiDataType = "datetime">
				<cfelseif data_Type contains "date">
					<cfset apiDataType = "date">
				<cfelseif data_Type eq "bit">
					<cfset apiDataType = "boolean">
				<cfelseif data_Type eq "decimal">
					<cfset apiDataType = "float">
				<cfelseif data_Type eq "uniqueidentifier">
					<cfset apiDataType = "uniqueidentifier">
				</cfif>
				<cfset item.apiDataType = apiDataType>

				<cfif blackListTypeID is not "">
					<CFQUERY NAME="getBlackListItems" DATASOURCE="#application.siteDataSource#">
					select data from blacklist
					where blackListTypeID =  <cf_queryparam value="#blackListTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>

					<cfset item.blacklist = structNew()>
					<cfset item.blacklist.query  = getBlackListItems>

				</cfif>

				<!--- WAB 2008-09-24 --->
				<cfset item.cfFormParameters = regExpobject.convertNameValuePairStringToStructure(cfformparameters,",")>

				<cfset tempTableStruct[getFieldInfo.name] = item>
				<cfif item.apiName neq "">
					<cfset tempTableStruct[getFieldInfo.apiName] = item>
				</cfif>
			</cfloop>

			<cfset application.fieldInfo[getFieldInfo.tablename]= tempTableStruct>

		</cfif>


		<cfreturn application.fieldInfo[tablename]>

	</cffunction>


<!--- WAB 2008-07-15  structure for easy access to organisation types, to be stored as an application variable
	keyed both on numeric id and text id
--->

	<cffunction name="setOrganisationTypeStructure" >
		<cfargument name="applicationScope" default="#application#">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->
		<cfset var result = "">
		<cfset var organisationTypes= "">
		<cfset var structureFunctionsObj = '' />

		<cfquery name="organisationTypes"  datasource = "#applicationScope.sitedatasource#">
		select 'phr_sys_'+TypeTextID as Name, TypeTextID, organisationTypeID
			 from organisationType
		</cfquery>

		<cfset structureFunctionsObj = createObject("component","relay.com.structureFunctions")>

		<cfset result = structureFunctionsObj.queryToStruct(query=organisationtypes, key = "organisationTypeID")>
		<cfset  structAppend (result, structureFunctionsObj.queryToStruct(query=organisationtypes, key = "TypetextID"))>


		<cfset applicationScope.organisationType = result>

		<cfreturn true>


	</cffunction>


<!--- WAB 2011/11 this function probably redundant with new merge functionality and wysiwyg --->
	<cffunction name="setEmailMergeVariables" description="" access="public">
		<cfargument name="applicationScope" default="#application#">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->
		<cfset var userFilesXMLPath = '' />
		<cfset var emailMergeXmlUserFiles = '' />
		<cfset var myXML = '' />
		<cfset var XMLstruct = '' />
		<cfset var mergeFieldsArray = '' />
		<cfset var emailMergeFieldsHTML = '' />
		<cfset var HTMLcounter = '' />
		<cfset var emailMergeFieldsHTMLLink = '' />
		<cfset var HTMLLinkcounter = '' />
		<cfset var emailMergeFieldsText = '' />
		<cfset var Textcounter = '' />
		<cfset var i = '' />
		<cfset var uniquemergeFields = '' />
		<cfset var nonuniquemergeFields = '' />
		<cfset var refind = '' />
		<cfset var insertcode = '' />
		<cfset var x = '' />
		<cfset var thisField = '' />
		<cfset var isFunction = '' />
		<cfset var possibleeditorName = '' />
		<cfset var emailMergeXml = '' />


		<!--- set application variables for the email merge fields


		WAB 2005-07-20   major modifications to the format and creating another application variable to hold details of valid merge fields
		WAB 2007-10-30  change to accomodate client specific merge fields (in userfiles directory)  when released must remove the XML tag from top of xml documents


		--->
				<cftry>
					<cffile action="read"
			  			file="#applicationScope.paths.relayware#\xml\emailmergefields.xml"
			  			variable="emailMergeXml">
					<cfcatch type="Any">Email Merge Field XML file not found in #applicationScope.paths.relayware#\xml\</cfcatch>
				</cftry>

				<cfset userFilesXMLPath = applicationScope.paths.userfiles & "\xml\emailmergefieldsExtension.xml">
				<cfif fileexists (userFilesXMLPath)>
					<cffile action="read"
			  			file="#userFilesXMLPath#"
			  			variable="emailMergeXmlUserFiles">
				<cfelse>
					<cfset emailMergeXmlUserFiles = "">
				</cfif>

				<!--- amazingly this seems to work to join the two xml files together!! WAB --->
				<cfset myXML = "<dummy>
								#emailMergeXml#
								#emailMergeXmlUserFiles#
								</dummy>">


				<CFSET XMLstruct = XmlParse(myxml)>
				<cfset mergeFieldsArray = XmlSearch(XMLstruct, "//fields/fielditem")	>


				<cfscript>
					// make structure for devEdit
					emailMergeFieldsHTML = structnew() ;
					HTMLcounter = 0 ;
					// make structure for devEditLink
					emailMergeFieldsHTMLLink = structnew() ;
					HTMLLinkcounter = 0 ;
					// make structure for text Edit
					emailMergeFieldsText = structnew() ;
					Textcounter = 0 ;


					// loop though all items
					for (i=1; i LTE arrayLen(mergeFieldsArray);i=i+1)
					{
						thisMergeFieldStruct = mergeFieldsArray[i] ;

						if (thisMergeFieldStruct.xmlattributes.showindevedit is "true")
						// items to show in devedit
						{
							HTMLcounter = HTMLcounter + 1 ;
							emailMergeFieldsHTML[HTMLcounter] = structnew() ;
							emailMergeFieldsHTML[HTMLcounter].insertname = thisMergeFieldStruct.title.xmltext ;
							emailMergeFieldsHTML[HTMLcounter].inserthtmlcode = thisMergeFieldStruct.insertcode.xmltext ;
						}

						if (thisMergeFieldStruct.xmlattributes.showintextedit is "true")
						// items to show in textedit
						{
							Textcounter = Textcounter + 1 ;
							emailMergeFieldsText[Textcounter] = structnew() ;
							emailMergeFieldsText[Textcounter].title = thisMergeFieldStruct.title.xmltext ;
							emailMergeFieldsText[Textcounter].insertcode = thisMergeFieldStruct.insertcode.xmltext ;
						}

						if (structKeyExists(thisMergeFieldStruct.xmlattributes,"showindeveditlinkManager") and thisMergeFieldStruct.xmlattributes.showindeveditlinkManager is "true")
						// links to show in devedit link manager
						{
							HTMLLinkcounter = HTMLLinkcounter + 1 ;
							emailMergeFieldsHTMLLink[HTMLLinkcounter] = structnew() ;
							emailMergeFieldsHTMLLink[HTMLLinkcounter].title = thisMergeFieldStruct.title.xmltext ;
							emailMergeFieldsHTMLLink[HTMLLinkcounter].insertcode = thisMergeFieldStruct.insertcode.xmltext ;
						}



					}


					// create a structure of all the valid fields which is then used for validating communications
					//
					// the name of each field is the bit between [[ and ]] , unless it is a function of the form [[afunction(x=1y=2)]] in which case the name is the bit before the (

						uniquemergeFields = ""  ;
						nonuniquemergeFields = ""  ;
						applicationScope.emailMergeFields = structNew ();
						refind = "\[\[(.*?)\]\]" ;   // regular expression to extract bit between the [[ and ]]

					for (i=1; i LTE arrayLen(mergeFieldsArray);i=i+1)
					{
						thisMergeFieldStruct = mergeFieldsArray[i] ;

						insertcode = thisMergeFieldStruct.insertcode.xmltext ;
						// extract a name from the insertcode
						x = refindNoCase(reFind, insertcode, 1, true)  ;

						if (x.pos[1] is not 0)  {  // if [[xxx]] is found

							thisField = mid(insertcode,x.pos[2],x.len[2]) ;    // bit betwen the [[ and ]]
							if (listLen(thisField,'(') gt 1) {
								thisField = listfirst(thisField,"(")	;		// if the insertcode has a "(" in it (ie it is a function of some sort) then the name is the bit before the (
								isFunction =true ;
							} else {
								isFunction =false ;
							}

							applicationScope.emailMergeFields[thisField] =structNew();
							applicationScope.emailMergeFields[thisField].sortIndex = i;

								// if the xml defines an refind thenit is used, otherwise we derive a standard one
								if (structKeyExists(thisMergeFieldStruct,"reFind")) {
									applicationScope.emailMergeFields[thisField].reFind = thisMergeFieldStruct.reFind.xmltext ;
								} else if (isFunction){
									applicationScope.emailMergeFields[thisField].reFind = thisField & "\((.*?)\)"   ;
								} else {
									applicationScope.emailMergeFields[thisField].reFind = thisField ;
								}


								if (structKeyExists(thisMergeFieldStruct,"reReplace")) {
									applicationScope.emailMergeFields[thisField].reReplace = thisMergeFieldStruct.reReplace.xmltext ;
								} else if (isFunction){
									applicationScope.emailMergeFields[thisField].reReplace = "application.com.emailmerge." & thisField & "(\1)" ;
								} else {
									applicationScope.emailMergeFields[thisField].reReplace = thisField ;
								}


								// if the reFind isn't looking for the [[ and ]] we add them to the beginning and end
								// and then add ## to the beginning and end of the replace
								//
								if (applicationScope.emailMergeFields[thisField].reFind does not contain "\]\]"){
									applicationScope.emailMergeFields[thisField].reFind = "\[\[" & applicationScope.emailMergeFields[thisField].reFind & "\]\]" ;
									applicationScope.emailMergeFields[thisField].reReplace = "##" & applicationScope.emailMergeFields[thisField].reReplace & "##" ;
								}


								applicationScope.emailMergeFields[thisField].insertCode = thisMergeFieldStruct.insertCode.xmltext ;
								applicationScope.emailMergeFields[thisField].unique = thisMergeFieldStruct.xmlattributes.unique ;

								if (structKeyExists(thisMergeFieldStruct,"description")) {
									applicationScope.emailMergeFields[thisField].description = thisMergeFieldStruct.description.xmltext ;
								}	else {
									applicationScope.emailMergeFields[thisField].description = '' ;
								}


								if (thisMergeFieldStruct.xmlattributes.unique is "true")
								{
									uniquemergeFields = listappend(uniquemergeFields, thisfield) ;

								} else {

									nonuniquemergeFields = listappend(nonuniquemergeFields,thisfield) ;
								}

								// wab trying to make similar to the relaytagstructure
								applicationScope.emailMergeFields[thisField].editorPath = '/relayTagEditors/relayFunctionEditor.cfm';
								applicationScope.emailMergeFields[thisField].cleanHTMLCode = applicationScope.emailMergeFields[thisField].insertCode ;
								applicationScope.emailMergeFields[thisField].insertname = thisfield;

								if (structKeyExists(thisMergeFieldStruct.xmlattributes,"showindeveditlinkManager") and thisMergeFieldStruct.xmlattributes.showindeveditlinkManager is "true")  {
									applicationScope.emailMergeFields[thisField].showInLinkManager = true ;
								} else {
									applicationScope.emailMergeFields[thisField].showInLinkManager = false ;
								}

								possibleeditorName = "function_#thisField#_editor.cfm" ;
								if (fileExists("#applicationScope.path#\relayTagEditors\#possibleeditorName#")) {
									applicationScope.emailMergeFields[thisField].editorExists = true;
									applicationScope.emailMergeFields[thisField].EDITORINSERTPATH = "\relayTagEditors\#possibleeditorName#";
								} else {
									applicationScope.emailMergeFields[thisField].editorExists = false;
								}

		//						if (thisField is 'TrackThisEmail') {
		//
								// special case need insert code in a variable for tskcommsend - bit of a hack I'm afraid
		//							applicationScope.TrackThisEmailCode = thisMergeFieldStruct.insertcode.xmltext  ;
		//							applicationScope.emailMergeFields[thisField].insertCode = insertcode;

		//						}

						}

					}



					applicationScope.uniquemergefields = uniquemergefields ;
					applicationScope.nonuniquemergefields = nonuniquemergefields ;
					applicationScope.emailMergeFieldsHTML =	emailMergeFieldsHTML ;
					applicationScope.emailMergeFieldsHTMLLink =	emailMergeFieldsHTMLLink ;
					applicationScope.emailMergeFieldsTEXT =	emailMergeFieldsTEXT ;



				</cfscript>
		</cffunction>


	<cffunction name="setApplicationStartingTemplates" description="" access="public">
		<cfargument name="applicationScope" default="#application#">
		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

		<!---
		File name:			setApplicationStartingTemplates.cfm
		Author:				AH
		Date started:		2004-06-01

		Description:		This sets up an application structure used by remote.cfm in conjuction
							with index.cfm to provide a method for login into a relay and going to the
							page specified in the structure.

							Example string: remote.cfm?a=i&p=#session.p#&st=003&ActionID=#actionID#

		Amendment History:

		Date (DD-MMM-YYYY)	Initials 	What was changed
		28-Oct-2008			NJH			T-10 Bug Fix Issue 1. Now redirecting to a page in relayware and opening the page using tabs. We try to use
										the tab name if the page that we're trying to access is in the xml menu structure, but if not, use the url and a tabName (separated by a pipe).

		Possible enhancements:


		 --->


		<cfscript>
			applicationScope.startingTemplateStr = structNew();
			applicationScope.startingTemplateLookupStr = structNew();
			applicationScope.startingTemplateStr[001] = "templates/subframe.cfm?module=home&goTo=homepage/myActions.cfm";
			applicationScope.startingTemplateStr[002] = "leadManager/specialPriceEdit.cfm";
			applicationScope.startingTemplateStr[003] = "templates/subframe.cfm?module=home&goTo=homepage/myActions.cfm?srchPersonID=&srchActionLive=on&srchdatetype=Started";
			applicationScope.startingTemplateStr[004] = "templates/subframe.cfm?module=Communicate&goTo=communicate/commdetailsHome.cfm";
			applicationScope.startingTemplateLookupStr['commcheckdetails'] = "004";
			applicationScope.startingTemplateStr[005] = "templates/subframe.cfm?module=Communicate&goTo=communicate/commCheck.cfm";
			applicationScope.startingTemplateLookupStr['commcheck'] = "005";
			applicationScope.startingTemplateStr[006] = "templates/subframe.cfm?module=incentive&goTo=incentive/reportRWClaimsDetailed.cfm";
			applicationScope.startingTemplateStr[007] = "templates/subframe.cfm?module=incentive&goTo=incentive/reportRWRegApproval.cfm";
			applicationScope.startingTemplateStr[008] = "templates/subframe.cfm?module=homepage&goTo=actions/ActionEdit.cfm";
			applicationScope.startingTemplateStr[009] = "templates/reportFrame.cfm";
			applicationScope.startingTemplateStr[010] = "templates/subframe.cfm?module=leadManager&goTo=leadManager/opportunityEdit.cfm";
			applicationScope.startingTemplateStr[011] = "templates/subframe.cfm?module=Approvals";
			applicationScope.startingTemplateStr[012] = "templates/subframe.cfm?module=Funds&goTo=funds/fundRequestActivities.cfm";
			applicationScope.startingTemplateStr[013] = "templates/subframe.cfm?module=Incentive&goTo=incentive/ApproveOrderStatus.cfm";
			applicationScope.startingTemplateStr[014] = "Communicate/commdetailsHome.cfm";
			applicationScope.startingTemplateLookupStr['commdetailsHome'] = "014";

			/*
			Version 2 of this structure is for use with the new UI
			Either give the name item in the relayware menu structure
			or give path to required template and a name for the tab (optional) separated by a pipe
			*/
			applicationScope.startingTemplateStrV2 = structNew();
			applicationScope.startingTemplateStrV2[001] = "homepage/myActions.cfm";
			applicationScope.startingTemplateStrV2[003] = "homepage/myActions.cfm?srchPersonID=&srchActionLive=on&srchdatetype=Started";
			applicationScope.startingTemplateStrV2[004] = "communicate/commdetailsHome.cfm|Communication";
			applicationScope.startingTemplateStrV2["commcheckdetails"] = applicationScope.startingTemplateStrV2[004];
			applicationScope.startingTemplateStrV2[005] = "Communications";
			applicationScope.startingTemplateStrV2[006] = "incentive/reportRWClaimsDetailed.cfm";
			applicationScope.startingTemplateStrV2[007] = "incentive/reportRWRegApproval.cfm";
			applicationScope.startingTemplateStrV2[008] = "actions/ActionEdit.cfm";
			applicationScope.startingTemplateStrV2[010] = "leadManager/opportunityEdit.cfm";
			applicationScope.startingTemplateStrV2[011] = "Approvals";
			applicationScope.startingTemplateStrV2[012] = "funds/fundRequestActivities.cfm";
			applicationScope.startingTemplateStrV2[013] = "Prizes";
		</cfscript>

		<!--- START 2010-10-19	NAS	LID4397: Leads - email hyperlinks --->
		<!--- Hook to be able to Extend the list for Client Specific Modules. Start the number at 900 --->
		<cf_include checkIfExists="true" template = "/code/cftemplates/setApplicationStartingTemplatesExtension.cfm">
		<!--- STOP 2010-10-19	NAS	LID4397: Leads - email hyperlinks --->

	</cffunction>

	<!---
	WAB 2009-06-23
	Trying a generic naming convention for updating various application structures.
	Means we can have a single template to handle them all
	Could even have some introspection

	2011-05-10 WAB added handling of a timeout as an attribute of a function
	 --->

	<cffunction name="doReload" description="" access="public">
		<cfargument name="item" >
		<cfargument name="updateCluster" default="true" >
		<cfargument name="waitForClusterResponse" default="false" >
		<cfargument name="applicationScope" default="#application#" >

		<cfset var method = evaluate("reload_#item#")>
		<cfset var result = {}>
		<cfset var methodresult = "">
		<cfset var args = {}>


		<cftry>
			<cfset methodresult = method(applicationScope = applicationScope)>
			<!---
			2010-01-11  WAB
			deal with function returning various different types, need to standardise to a structure with isOK in it
			--->
			<cfif not isDefined("methodresult")>
				<cfset result = {isOK = true}>
			<cfelseif isBoolean (methodresult)>
				<cfset result = {isOK = methodresult}>
			<cfelseif IsSimpleValue  (methodresult)>
				<cfset result = {isOK = true, message = methodresult}>
			<cfelse>
				<cfset result =	methodresult	>
				<cfif not structKeyExists(result,"isOK")>  <!--- add an isOK if not there - basically assume that a returned structure without an isOK must be OK! --->
					<cfset result.isOK = true>
				</cfif>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
				<cfif isDefined("cfcatch.tagcontext")>
						<cfset result.message = result.message & "  " & cfcatch.tagcontext[1].template & " line " & cfcatch.tagcontext[1].line>
				</cfif>

			</cfcatch>
		</cftry>

		<cfset result.FunctionMetaData = getMetaData(method)>

		<cfif updateCluster and structKeyExists(applicationScope,"com")>
			<cfset args = {functionName="doReload", waitForResponse = waitForClusterResponse,item=item, includeCurrentServer = false}>
			<cfif structKeyExists(result.FunctionMetaData,"timeout")>
				<cfset args.timeout = result.FunctionMetaData.timeout>
			</cfif>
	       		<cfset result.clusterResult = applicationScope.com.clusterManagement.runFunctionOnCluster(argumentCollection = args)>
		<cfelse>
			<cfset result.clusterResult = {}>
  		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction index = "30" name="reload_SecurityXML" description="Reload File and Directory Security XML Document" access="public">
		<cfargument name="applicationScope" default="#application#">
		<!--- WAB 2009-07-15 changed fileanddirectory to security  --->
		<cfreturn applicationScope.com.security.loadXML (refresh=true,applicationScope=applicationScope)>
	</cffunction>

	<cffunction index = "5" name="reload_ServerInitialisationObject" description="Reset Server Initialisation Object <font color='red'>(Only use when necessary)</font>" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn resetServerInitialisationObj(onlyIfChanged = false,applicationScope=applicationScope)>
	</cffunction>

	<cffunction index = "10" name="reload_ApplicationComponents" description="Reload Changed Application Components " access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result = loadApplicationComponents(onlyIfChanged = true,verbose=true,applicationScope=applicationScope,doServerInitialisationObj=true)>
		<cfif result.lockError>
			<cfset result.message = "Locked">
		<cfelseif result.numberLoaded + result.numberFailed is 0> <!--- 2013-09-10 WAB	Case 441626 added result.numberfailed to the test, otherwise a single failure and gave 'No Items To Load' message  --->
			<cfset result.message = "No Items to Load">
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction index = "15" name="reload_ApplicationComponentsWithReload" description="Reload All Application Components " access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result = loadApplicationComponents(onlyIfChanged  = false,verbose=true,applicationScope=applicationScope,doServerInitialisationObj=true)>
		<cfif result.lockError>
			<cfset result.message = "Locked">
		<cfelseif result.numberLoaded is 0>
			<cfset result.message = "No Items to Load">
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction index = "55" name="reload_RelayTagStructure" description="Reload Relay Tag Structure" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn updateRelayTagStructure(applicationScope=applicationScope)>
	</cffunction>

	<cffunction index = "40" name="reload_RelayMenus" description="Reload Application Menus" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn applicationScope.com.relayMenu.loadRelayMenu(applicationScope=applicationScope)>
	</cffunction>


	<cffunction index = "45" name="reload_PhrasesStruct" description="Reset Phrases Structure" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn applicationScope.com.relaytranslations.resetPhraseStructure(applicationScope=applicationScope,updatecluster = false) >
	</cffunction>

	<cffunction index = "50" name="reload_FlagStructures" description="ReSet Flag Structures" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn applicationScope.com.flag.loadApplicationStructures(applicationScope=applicationScope,updatecluster = false) >  <!--- clustering dealt with by parent function --->
	</cffunction>

	<cffunction index = "20" name="reload_ApplicationVariables" description="Reload Application Variables" access="public" timeout = 60>
		<cfargument name="applicationScope" default="#application#">
		<cfset var result = reLoadAllVariables(applicationScope=applicationScope)>
		<cfreturn  result>
	</cffunction>

	<cffunction index = "25" name="reload_Settings" description="Reload Settings Structure" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn applicationScope.com.settings.loadVariablesIntoApplicationScope(applicationScope=applicationScope) >
	</cffunction>

	<cffunction index = "60" name="reload_EmailMergeVariables" description="Reload Email Merge Variables" access="public">
		<cfargument name="applicationScope" default="#application#">
		<cfreturn setEmailMergeVariables(applicationScope=applicationScope) >
	</cffunction>

	<cffunction index = "65" name="reload_CompileAndLoadJava" description="Reload (and Compile) All Java Classes" access="public">
		<cfargument name="applicationScope" default="#application#">

		<cfset resultStruct=applicationScope.com.java.compileAndLoad_forReload(applicationScope)>



		<cfreturn resultStruct >
	</cffunction>

	<cffunction index = "70" name="reload_CompileAndLoadJavaBootstrap" description="Reload Java Compilation Bootstrap" access="public">
		<cfthrow message="not yet implemented">
	</cffunction>

	<cffunction name="getReloadFunctions">
		<cfset var thisFunction = '' />
		<cfset var functionsArray = getmetadata(this).functions>
		<cfset var result = {functions = structNew()}>
		<cfloop array = #functionsArray# index = "thisFunction">
			<cfif left(thisFunction.name,7) is "reload_">
				<cfset result.functions[thisFunction.name] = thisFunction>
			</cfif>
		</cfloop>

		<cfset result.orderedArray = structSort (result.functions,"numeric","asc","index")>

		<cfreturn result>

	</cffunction>

	<!--- for debugging --->
	<cffunction name="doBootLog" description="" access="public">
		<cfargument name="text" default = "">
		<cftry>
	 		<cf_log file="boot" text = #text#>
			<cfcatch>
				<!--- problems wih customtags paths when using clustering --->
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- 2013-10-16	YMA	Custom entities, store list in application variables--->
	<cffunction name="setCustomEntitiesStructure" >
		<cfargument name="applicationScope" default="#application#">

		<cfset var result = "">
		<cfset var customEntities = "">
		<cfset var structureFunctionsObj = '' />

		<CFQUERY NAME="customEntities" DATASOURCE="#application.SiteDataSource#">
			select ce.*, st.entityTypeID as referencedEntityTypeID from
				(select distinct s.entityTypeID,EntityName,description,UniqueKey,referencedtable
				from schemaTable s
				join INFORMATION_SCHEMA.COLUMNS isc on s.entityname = isc.TABLE_NAME
				JOIN
				(SELECT
					fk.name,
					OBJECT_NAME(fk.parent_object_id) 'Parent table',
					c1.name 'Parent column',
					OBJECT_NAME(fk.referenced_object_id) 'Referencedtable',
					c2.name 'Referenced column'
				FROM
					sys.foreign_keys fk
				INNER JOIN
					sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
				INNER JOIN
					sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
				INNER JOIN
					sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id) as FK
				ON s.entityName = FK.[Parent table] and isc.column_name = FK.[referenced column]
				where s.screensExist = 1 and s.flagsExist = 1 and s.uniqueKey is not null and s.customentity = 1) as ce
			join schematable as st on ce.referencedtable = st.entityname
			order by referencedtable, st.entityName
		</CFQUERY>

		<cfset structureFunctionsObj = createObject("component","relay.com.structureFunctions")>

		<cfset result = structureFunctionsObj.queryToStruct(query=customEntities, key = "ENTITYNAME")>


		<cfset applicationScope.customEntities = result>

		<cfreturn true>


	</cffunction>

</cfcomponent>
