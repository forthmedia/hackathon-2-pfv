<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="populateCalendarQuarters" hint="Creates the calendarQuarters for calenderID">
		<cfargument name="startingYear" type="numeric" default="2004">
		<cfargument name="startingMonth" type="numeric" default="1">
		<cfargument name="CalendarID" type="numeric" default="1">
		<cfargument name="clearPreviousEntries" type="boolean" default="false">
		
		<cfscript>
			var clearCalendarQuarter = "";
			var getCalendarQuarters = "";
			var SetCalQtrs = "";
		</cfscript>
		
		<cfif arguments.clearPreviousEntries>
			<cfquery name="clearCalendarQuarter" datasource="#application.siteDataSource#">
				delete from CalendarQuarter where calendarID = #arguments.CalendarID#
			</cfquery>
		</cfif>
		
		<cfset yearList = startingYear>
		<cfloop from="1" to="5" index="t">
			<cfset nextYear = arguments.startingYear + t>
			<cfset yearList = listappend(yearList,nextYear)>
		</cfloop>

		<cfloop list="#yearList#" index="CalendarYear">
			<cfloop list="1,2,3,4" index="qtr">
				<cfswitch expression="#qtr#">
					<cfcase value="1">
						<cfset thisMonth = arguments.startingMonth>
					</cfcase>
					<cfcase value="2">
						<cfset thisMonth = arguments.startingMonth + 3>
					</cfcase>
					<cfcase value="3">
						<cfset thisMonth = arguments.startingMonth + 6>
					</cfcase>
					<cfcase value="4">
						<cfset thisMonth = arguments.startingMonth + 9>
					</cfcase>
				</cfswitch>
				
				<cfset monthDate = CreateDate(CalendarYear, thisMonth, 1)>
				
				<cfquery name="SetCalQtrs" datasource="#application.siteDataSource#">
					INSERT INTO [CalendarQuarter]
				           ([CalendarID]
				           ,[CalendarYear]
				           ,[QuarterOfCalendarYear]
				           ,[StartDate]
				           ,[EndDate]
				           ,[CalendarQuarterDesc])
				     VALUES
				           (<cf_queryparam value="#CalendarID#" CFSQLTYPE="CF_SQL_INTEGER" >
				           ,<cf_queryparam value="#CalendarYear#" CFSQLTYPE="CF_SQL_Integer" >
				           ,<cf_queryparam value="#qtr#" CFSQLTYPE="CF_SQL_Integer" >
				           ,<cf_queryparam value="#monthDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				           ,<cf_queryparam value="#dateAdd("d",-1,dateAdd("m",3,monthDate))#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="Q#qtr#" CFSQLTYPE="CF_SQL_VARCHAR" >) 
				</cfquery>
			</cfloop>
		</cfloop>

		<cfquery name="getCalendarQuarters" datasource="#application.siteDataSource#">
			select * from CalendarQuarter
		</cfquery>
		
		<cfreturn getCalendarQuarters>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getCalendarQuarters" access="public" hint="Returns a query containing the calendar quarters for calenderID" returntype="Query">
		<cfargument name="CalendarID" type="numeric" default="1">
		
		<cfscript>
			var getCalendarQuarters = "";
		</cfscript>
		
		<cfquery name="getCalendarQuarters" datasource="#application.siteDataSource#">
			select * from CalendarQuarter
		</cfquery>

		<cfreturn getCalendarQuarters>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getDayInQuarter" hint="Returns a specific date for the current quarter based on dateInQtr" returntype="date">
		<cfargument name="dayExpression" type="string" required="yes" hint="A SQL expression for the date required in the quarter.">
		<cfargument name="dateInQtr" type="date" default="now()" hint="Any date in the quarter required which is defaulted to current date.">
		
		<cfscript>
			var getDayInQyarter = ""; 
		</cfscript>

		<cfswitch expression="#arguments.dayExpression#">
			<cfcase value="day21"><cfset dayExpression = "dateAdd([day],21,startDate)"></cfcase>
			<cfcase value="OneMonthIntoQtr"><cfset dayExpression = "dateAdd([month],1,startDate)"></cfcase>
			<cfcase value="lastDateOfQtr"><cfset dayExpression = "endDate"></cfcase>
			<cfcase value="OneMonthAfterQtr"><cfset dayExpression = "dateAdd([month],1,endDate)"></cfcase>
		</cfswitch>
		
		<cfquery name="getDayInQyarter" datasource="#application.siteDataSource#">
			select #arguments.dayExpression# as dateInQtr from calendarQuarter where CalendarQuarterId = 
			(select CalendarQuarterId from calendarQuarter where '#arguments.dateInQtr#' between StartDate and endDate)
		</cfquery>
		
		<cfreturn getDayInQyarter.dateInQtr>
	</cffunction>


</cfcomponent>