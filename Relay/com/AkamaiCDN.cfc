<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			RMB
Date created:	2011/11/25

Provides secure token and url for Akamai CDN

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
--->

<cfcomponent displayname="AkamaiCDN" hint="Provides secure token and url for Akamai CDN">

<!--- 2012-05-18 - RMB - P-REL101 - Returns the URL token or Generates a Cookie. --->
<cffunction name="GetAkamaiToken" output="false">
	<cfargument name="sUrl" type="string" required="true">
	<cfargument name="fileID" type="numeric" required="true">

	<cfset var queryAllFileType = "">
	<cfset var res = "">
	<cfset var useUrl = "">

	<cfquery name="FileType" datasource="#application.siteDataSource#">
	SELECT TOP 1
		T1.[path],
		'/content/' + T1.path + '/' + F1.filename as fileNameAndPath,
		'/content/' + T1.path + '/' + CONVERT(VARCHAR(1000), F1.fileID) +  '/*' as folderPath		
	FROM
	  files F1
	  INNER JOIN filetype T1 ON T1.filetypeid = F1.filetypeid
	WHERE
		F1.fileID = <cf_queryparam value="#arguments.fileID#" CFSQLType="CF_SQL_INTEGER" />
	</cfquery>

	<cfif FileType.path NEQ 'CourseWare'>
		<cfset res = urlauth_gen_url(sUrl = arguments.sUrl)>
	<cfelse>
	
		<cfif FileType.fileNameAndPath NEQ arguments.sUrl>
			<cfset useUrl = arguments.sUrl>
			<cfset arguments.sUrl = FileType.folderPath>
		<cfelse>
			<cfset useUrl = arguments.sUrl>
		</cfif>
		
		<cfset res = urlauth_gen_cookie(sUrl = arguments.sUrl, fileID = arguments.fileID, useUrl = useUrl)>
	
	</cfif>

	<cfreturn res>

</cffunction>

<!--- 2011/12/02 - RMB - P-REL101 - Returns the cookie for authorization with Akamai. --->
<cffunction name="urlauth_gen_cookie" output="false">
	<cfargument name="sUrl" type="string" required="true">
	<cfargument name="useUrl" type="string" required="true">
	<cfargument name="fileID" type="numeric" required="true">

	<cfset var AkamaiPartOne = application.com.settings.getsetting('files.Akamai.AkamaiPrivateRoot')>	
	<cfset var cookiename = application.com.settings.getsetting('files.Akamai.AkamaiParam')>
	<cfset var res = "">	
	<cfset var UseRelayDomain = "">

	<cfquery name="UseRelayDomain" datasource="#application.siteDataSource#">	
	SELECT TOP 1
		CASE WHEN sd.isSecure = 0 THEN  'HTTP://' ELSE  'HTTPS://' END + sdd.[domain] AS 'protocolAndDomain'
	FROM
		siteDefDomain sdd
		INNER JOIN siteDef sd ON sdd.siteDefID = sd.sitedefid
	WHERE
		sdd.testsite =  <cf_queryparam value="#application.testsite#" CFSQLTYPE="CF_SQL_INTEGER" >  AND
		sdd.[domain] like '%.relayware.com' and
		sdd.[domain] not like '%reports.%' -- JIC exclude report server
	ORDER BY
		sdd.maindomain DESC,
		sd.isInternal ASC,
		sdd.[domain] desc
	</cfquery>
	
	<cfset encryptedUrlVariables = application.com.security.encryptQueryString(queryString="cookieName=#urlencodedformat(cookieName)#&akamaiDomain=#urlencodedformat(AkamaiPartOne)#&accessPath=#urlencodedformat(useUrl)#&accessPathList=#urlencodedformat(sUrl)#")>
<cfset res = "#UseRelayDomain.protocolAndDomain#/webservices/nosession/akamai.cfm?#encryptedUrlVariables#">

	<cfreturn res>	
	
</cffunction>

<!--- 2011/12/02 - RMB - P-REL101 - Returns the URL path with the authorization token appended. --->
<cffunction name="urlauth_gen_url" output="false">
	<cfargument name="sUrl" default = "" type="string">
	<cfargument name="sExtract" default = "" type="string">
	<cfargument name="nTime" default = "">

	<cfset var sParam = application.com.settings.getsetting('files.Akamai.AkamaiParam')>
	<cfset var nWindow = application.com.settings.getsetting('files.Akamai.AkamaiWindow')>
	<cfset var sSalt = application.com.settings.getsetting('files.Akamai.AkamaiSaltKey')>
	<cfset var AkamaiPartOne = application.com.settings.getsetting('files.Akamai.AkamaiPrivateRoot')>

	<cfset var javaEpoch = request.requestTime>
	
	<cfset var sToken = urlauth_gen_token(sUrl, nWindow, sSalt, sExtract, nTime, javaEpoch)>
	<cfset var nExpires = "">
	<cfset var res = "">
	
		<cfif (sToken EQ '')>
			<CFSET MESSAGE = "sToken 1 ERROR">
			<cfreturn MESSAGE>
		</cfif>
		
		<cfif ((LEN(sParam) LT 5) OR (LEN(sParam) GT 12))>
			<CFSET MESSAGE = "sParam 1 ERROR">
			<cfreturn MESSAGE>
		</cfif>

		<cfif ((nWindow LT 0) OR (NOT IsNumeric(nWindow)))>
			<CFSET MESSAGE = "nWindow 1 ERROR">
			<cfreturn MESSAGE>
		</cfif>

		<cfif ((nTime LTE 0) OR (NOT IsNumeric(nTime)))>
			<cfset nTime = #int(javaEpoch.getTime()/1000)#>
		</cfif>

		<cfset nExpires = nWindow + nTime>

		<cfif (Find("?", sUrl) EQ 0)>
			<cfset res = sUrl & "?" & sParam & "=" & nExpires & "_" & sToken>
		<cfelse>
			<cfset res = sUrl & "&" & sParam & "=" & nExpires & "_" & sToken>
		</cfif>

	<cfreturn AkamaiPartOne & '/' & res>
	
</cffunction>

<!--- 2011/12/02 - RMB - P-REL101 - Returns the hash portion of the token. This function should not be called directly --->
<cffunction name="urlauth_gen_token" output="false">
	<cfargument name="sUrl" default = "" type="string">
	<cfargument name="nWindow" default = "" type="numeric">
	<cfargument name="sSalt" default = "" type="string">
	<cfargument name="sExtract" default = "" type="string">
	<cfargument name="nTime" default = "">
	<cfargument name="javaEpoch" default = "" type="numeric">

		<cfif ((sUrl EQ "") OR (IsNumeric(sUrl)))>
			<cfreturn FALSE>
		</cfif>

		<cfif ((nWindow LT 0) OR (NOT IsNumeric(nWindow)))>
			<cfreturn FALSE>
		</cfif>

		<cfif ((sSalt EQ "") OR (IsNumeric(sSalt)))>
			<cfreturn FALSE>
		</cfif>

		<cfif (IsNumeric(sExtract))>
			<cfset sExtract = "">
		</cfif>

		<cfif ((nTime LTE 0) || (NOT IsNumeric(nTime)))>
			<cfset nTime = #int(javaEpoch.getTime()/1000)#>
		</cfif>

		<cfset nExpires = nWindow + nTime>

		<cfset sExpByte1 = Chr(BitAnd(nExpires, 255))>
		<cfset sExpByte2 = chr(BitAnd(BitSHRN(nExpires, 8), 255))>
		<cfset sExpByte3 = chr(BitAnd(BitSHRN(nExpires, 16), 255))>
		<cfset sExpByte4 = chr(BitAnd(BitSHRN(nExpires, 24), 255))>

		<cfset sData = sExpByte1 & sExpByte2 & sExpByte3 & sExpByte4 & sUrl & sExtract & sSalt>

		<cfset sHash = _unHex(LCASE(Hash(sData)))>

	<cfset sToken = LCASE(Hash(sSalt & sHash))>
	
	<cfreturn sToken>
</cffunction>

<cffunction name="_unHex" output="false">
	<cfargument name="str" default = "">
		<cfset var res = "">
		<cfset var iloop = 0>
		<cfloop from="1" to="#LEN(str)#" index="iloop" step="2">

			<cfset res = res & chr(InputBaseN(MID(str, iloop, 2), 16))>

		</cfloop>
	<cfreturn res>
</cffunction>


<cffunction name="AkamaiPurgeData">

	<cfset var AkamaiPublicRoot = application.com.settings.getsetting('files.Akamai.AkamaiPublicRoot')>
	<cfset var AkamaiPrivateRoot = application.com.settings.getsetting('files.Akamai.AkamaiPrivateRoot')>
	<cfset var FilesArray = "">
	
	<cfquery name="PurgeData" datasource="#application.siteDataSource#">
	SELECT
		(CASE WHEN ft1.secure = 0 THEN 'http://#AkamaiPublicRoot#/content/' ELSE 'http://#AkamaiPrivateRoot#/content/' END)
		+ LTRIM(RTRIM(ft1.[path])) + '/' + LTRIM(RTRIM(f1.[filename])) as 'listfile'
	FROM files f1,
		filetype ft1,
		filetypegroup fg1
	WHERE
		LTRIM(RTRIM(f1.DeliveryMethod)) = 'AkamaiCDN' AND
		ft1.filetypeid = f1.filetypeid AND
		fg1.filetypegroupid = ft1.filetypegroupid AND
		DATEDIFF(hour, f1.revision, GETDATE()) BETWEEN 1 AND 24
	ORDER BY secure, f1.revision DESC
	</cfquery>

	<cfif PurgeData.recordcount GTE 1>

		<cfset FilesArray = ListToArray(ValueList(PurgeData.listfile)) />	
		<cfset purgeType = "arl">
		<cfset purgeAction = "invalidate">
		<cfinvoke component="AkamaiCDN" method="AkamaiPurgeService" returnvariable="AkamaiPurgeResult" PurgeFiles="#PurgeData#" FilesArray="#FilesArray#" purgeType="#purgeType#" purgeAction="#purgeAction#">

		<cfreturn AkamaiPurgeResult>
		
	<cfelse>

		<cfreturn false>

	</cfif>
	
</cffunction>

<cffunction name="AkamaiPurgeService">
	<cfargument name="PurgeFiles" required="true" type="query">
	<cfargument name="FilesArray" required="true" type="array">
	<cfargument name="purgeType" required="true" type="string">
	<cfargument name="purgeAction" required="true" type="string">

	<cfset var oppArry = "">	
	<cfset var uriArry = "">
	<cfset var Mystruct = "">
	<cfset var WSObject = "">
	<cfset var resultStruct = "">
	<cfset var result = "">
	<cfset var useUserName = "">
	<cfset var usePassword = "">
	
	<cfset useUserName = application.com.settings.getsetting('files.Akamai.AkamaiPurgeUserName')>
	<cfset usePassword = application.com.settings.getsetting('files.Akamai.AkamaiPurgePassword')>

	<cfobject webservice="https://ccuapi.akamai.com/ccuapi-axis.wsdl" name="WSObject">
	
	<cfscript>
	oppArry = ['type=#purgeType#','action=#purgeAction#'];
	uriArry = FilesArray;
	Mystruct = {name='#useUserName#', pwd='#usePassword#', network='', opt=oppArry, uri=uriArry};
	</cfscript>

	<cfinvoke webservice="#WSObject#" method="purgeRequest" attributecollection="#Mystruct#" returnvariable="result">

	<cfreturn result>

</cffunction>

</cfcomponent>