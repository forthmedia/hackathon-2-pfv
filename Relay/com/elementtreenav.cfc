<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="elementtreenav" hint="Relay content tree navigation">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getNodeParent" output="true" hint="" returntype="string">

		<cfargument name="nodeID" type="string" required="yes">


		<cfscript>
			var getParents = "";
		</cfscript>

		<CFQUERY NAME="getParents" datasource="#application.sitedatasource#">
			SELECT parentID
			FROM Element
			where ID =  <cf_queryparam value="#arguments.nodeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<cfreturn getParents.parentID>

	</cffunction>

	<cffunction name="getParentTree" access="public" returntype="string" hint="Finds the entire parent tree for a category">

		<cfargument name="nodeID" type="numeric">


		<cfscript>
			session.catIDList = "";
			session.catList = "";
			returnParentTree(arguments.nodeID);
		</cfscript>

		<cfreturn session.catList>

	</cffunction>

	<cffunction name="returnParentTree" access="public" returntype="string" hint="Finds the entire parent tree for a category">

		<cfargument name="nodeID" type="numeric">


		<cfscript>
			var parentID = getNodeParent(arguments.nodeID);
			session.catIDList = listAppend(session.catIDList,arguments.nodeID);

			if(parentID){
				returnParentTree(parentID);
			}

			//now invert the list
			session.catList="";
			for(i=listLen(session.catIDList); i gte 1; i=i-1){
				session.catList = listAppend(session.catList,listGetAt(session.catIDList,i));
			}
		</cfscript>



		<cfreturn session.catList>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      2005-06-15 SWJ added for element linking
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getElementLinkTypes" hint="Returns a query object containing elementLinkTypes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var getElementLinkTypes = "";
		</cfscript>

		<cfquery name="getElementLinkTypes" datasource="#arguments.dataSource#">
			select * from ElementLinkType where active = 1  <!--- NJH 2007/04/30 added active --->
		</cfquery>

		<cfreturn getElementLinkTypes>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      2005-06-15 SWJ added for element linking
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setElementLink" hint="Insert an elementLink">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="entityID" type="numeric" required="yes">
		<cfargument name="elementLinkTypeTextID" type="string" required="yes">
		<cfargument name="hostElementID" type="numeric" required="no">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="countryID" type="numeric" required="no"> <!--- NJH 2007/04/30 --->

		<cfscript>
			var qSetElementLink = "";
			var elementLinkSchemaID = "";
			var linkParam1 = "";
			var linkParam2 = "";
			var linkParam3 = "";
			var additionalDescription = "";
		</cfscript>

		<!--- NJH 2007/05/01 setting this request variable here if it's not already defined. It should be set in relayINI --->
		<cfif not isDefined("request.productCFC")>
			<cfset request.productCFC = "relayProductTree">
		</cfif>

		<cfif arguments.elementLinkTypeTextID eq "Advert">
			<cfscript>
				hostElementID = hostElementID;
			</cfscript>
		</cfif>

		<cfif arguments.elementLinkTypeTextID eq "Element">
			<cfscript>
				hostElementID = arguments.entityID;
			</cfscript>
		</cfif>

		<cfif arguments.elementLinkTypeTextID eq "Product">
			<!--- 2005/06/17 - GCC - Bit of a Sony specific include - should be to the relayware standard
			product data with this as a Sony specific override--->
			<!--- NJH 2007/05/01 created switch depending on which cfc is being used. --->
			<cfscript>
				elementLinkSchemaID = 1;
				if (request.productCFC eq "sonyNavigationV2") {
					productData = evaluate("application.com.sonyNavigationV2.getLinkableProducts(productID = entityID)");
				linkParam1 = productData.navID;
				linkParam2 = productData.nodeID;
				linkParam3 = productData.productID;
				} else if (request.productCFC eq "relayProductTree") {
					productData = evaluate("application.com.relayProductTree.getLinkableProducts(productID = entityID,countryID = countryID)");
					linkParam1 = productData.pcc;
					linkParam2 = productData.plc;
					linkParam3 = productData.pn;
				}
				hostElementID = productData.EID;
				additionalDescription = productData.headline;
			</cfscript>
		<cfelseif arguments.elementLinkTypeTextID eq "Category">
			<!--- 2005/06/17 - GCC - Bit of a Sony specific include - should be to the relayware standard
			product data with this as a Sony specific override--->
			<cfscript>
				productData = evaluate("application.com.#request.productCFC#.getLinkableNodes(nodeID = entityID)");
				elementLinkSchemaID = 2;
				if (request.productCFC eq "sonyNavigationV2") {
				linkParam1 = productData.navID;
				linkParam2 = productData.nodeID;
				} else if (request.productCFC eq "relayProductTree") {
					linkParam1 = productData.navID;
					linkParam2 = productData.nodeID;
				}
				hostElementID = productData.EID;
				additionalDescription = productData.headline;
			</cfscript>
		</cfif>

			<cfquery name="qSetElementLink" datasource="#arguments.dataSource#">
				INSERT INTO
					[elementLink]
					(
					[elementID],
					[entityID],
					[elementLinkTypeTextID],
					[hostElementID]
					<cfif elementLinkSchemaID is not "">
						,[elementLinkSchemaID]
					</cfif>
					<cfif linkParam1 is not "">
						,[elementLinkParam1]
					</cfif>
					<cfif linkParam2 is not "">
						,[elementLinkParam2]
					</cfif>
					<cfif linkParam3 is not "">
						,[elementLinkParam3]
					</cfif>
					<cfif additionalDescription is not "">
						,[additionalDescription]
					</cfif>
					)
				SELECT
					<cf_queryparam value="#arguments.elementID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#arguments.elementLinkTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#hostElementID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif elementLinkSchemaID is not "">
						,<cf_queryparam value="#elementLinkSchemaID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>
					<cfif linkParam1 is not "">
						,<cf_queryparam value="#linkParam1#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					<cfif linkParam2 is not "">
						,<cf_queryparam value="#linkParam2#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					<cfif linkParam3 is not "">
						,<cf_queryparam value="#linkParam3#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					<cfif additionalDescription is not "">
						,<cf_queryparam value="#additionalDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
			where not exists (select * from elementLink
				WHERE elementID = #arguments.elementID#
				AND entityID = #arguments.entityID#
				AND elementLinkTypeTextID =  <cf_queryparam value="#arguments.elementLinkTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				AND hostElementID = #hostElementID#
				<cfif linkParam1 is not "">
					AND elementLinkParam1 =  <cf_queryparam value="#linkParam1#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif linkParam2 is not "">
					AND elementLinkParam2 =  <cf_queryparam value="#linkParam2#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif linkParam3 is not "">
					AND elementLinkParam3 =  <cf_queryparam value="#linkParam3#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif additionalDescription is not "">
					AND additionalDescription =  <cf_queryparam value="#additionalDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>)
			</cfquery>
		<cfreturn SetElementLink>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      2005-06-15 SWJ added for element linking
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deleteElementLink" hint="Insert an elementLink">
		<cfargument name="elementID" type="numeric" required="yes">
		<cfargument name="entityID" type="numeric" required="yes">
		<cfargument name="elementLinkTypeTextID" type="string" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var deleteElementLink = "";
		</cfscript>

		<cfquery name="deleteElementLink" datasource="#arguments.dataSource#">
			delete from [elementLink]
				WHERE elementID = #arguments.elementID#
				AND entityID = #arguments.entityID#
				AND elementLinkTypeTextID =  <cf_queryparam value="#arguments.elementLinkTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn "Link deleted">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      2013-01-18 YMA Added to return element and all child pages and their family.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getArchivePageFamily">
		<cfargument name="id" type="numeric" required="yes">

		<cfscript>
			var archiveList = "";
		</cfscript>

		<cfquery name="archiveList" datasource="#application.sitedatasource#" >
			exec getElementTree
				@elementID =  <cf_queryparam value="#id#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				@statusid =  6 ,
				@justReturnNodes = 1

		</cfquery>

		<cfreturn archiveList>
	</cffunction>
</cfcomponent>


