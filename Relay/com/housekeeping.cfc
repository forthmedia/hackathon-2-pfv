<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2010
Individual Housekeeping functions
See http://relayware.wikidot.com/housekeeping

Use HouseKeeping.cfc for your individual housekeeping functions

WAB 2011/10/31 added clearOutErrorLogs()
2014-11-10	RPW	CORE-881 Social updates are not seen - Changed function SendSocialShares parameter runEveryDatePart from "d" (day) to "n" minute
2014-11-14	AXA CORE-947 Twitter messages duplicating when scheduled, added variable scoping
2014-11-27 AHL Case 442668 geo-data population task not running
2015-01-30 AHL Case 443215 Complete status for Events
2015-04-01	WAB CASE 444268 fixes to deleteOldProcessLocks()
2016/01/12	NJH	JIRA BF-94 - removed cf_queryparam in favour of cfqueryparam as attributes were not defined in cf_queryparam when called from housekeeping... don't know why... have changed to cfqueryparam for now
2016-03-17	WAB	Altered some items which were set to run every minute to run slightly less often
2016-06-17	WAB PROD2016-1280 Add validateSecurityXML()
2016-10-14	RJT	PROD2016-2531 removed unhelpful error handler so the proper error handler records an actual message
--->


	<cfcomponent extends="HousekeepingCore">

	<!--- Add your own functions here


		 <cffunction
			name="myFunction"
			runOnEach="Instance|Database"
			runEveryDatePart="n|h|d|m|y"
			runEvery = "aNumber"

			Not implemented Yet!
			OutOfOfficeHours = ""
			startTime = ""
			endTime = ""
					>

	 --->


	<cffunction name="reRegisterInstance" runOnEach="Instance" runEveryDatePart="n" runEvery="15">
		<cfargument name="applicationScope" default="#application#">
			<cfset var result = {isOK = true}>
			<cfset arguments.applicationscope.com.clusterManagement.registerInstance(cfAppName = applicationscope.applicationname,datasource = applicationscope.sitedatasource, testsite=applicationscope.testsite,updateApplicationVariable = false,cf_mappings=applicationScope.cf_mappings)>
		<cfreturn result>
	</cffunction>

	<cffunction name="deActivateInstances" runOnEach="database" runEveryDatePart="h" runEvery="1">
		<cfargument name="applicationScope" default="#application#">
			<cfset var result = {isOK = true}>
			<cfquery >
			update coldFusionInstance_autopopulated set active = 0 where active = 1 and lastupdated < dateadd(hh,-1,getdate())
			</cfquery>

		<cfreturn result>
	</cffunction>

	<cffunction name="pushErrors" runOnEach="database" runEveryDatePart="n" runEvery="10">
		<cfargument name="applicationScope" default="#application#">

			<cfset var result = applicationscope.com.errorHandler.copyErrorsToHQ(numbertosend = 50, applicationScope = applicationScope)>

		<cfreturn result>
	</cffunction>

	<cffunction name="tidyElementTreeCacheTable" runOnEach="database" runEveryDatePart="n" runEvery="30" hint="">
		<cfargument name="applicationScope" default="#application#">
			<cfset var result = {isOK = true}>

			<cfset arguments.applicationScope.com.relayelementTree.tidyElementTreeCacheTable (applicationScope = applicationScope) >

			<cfreturn result>

	</cffunction>


	<cffunction name="tidySessionElementTreeCacheTable" runOnEach="sessionEnd" hint="">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="sessionScope" default="#session#">
			<cfset var result = {isOK = true}>

			<cfset arguments.applicationscope.com.relayElementTree.tidyElementTreeCacheTableForASession (applicationscope = arguments.applicationscope, sessionid = arguments.sessionScope.sessionid)>

			<cfreturn result>

	</cffunction>


	<cffunction name="tempFileCleanupForCurrentSession" runOnEach="sessionEnd" hint="">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="sessionScope" default="#session#">
			<cfset var result = {isOK = true}>

			<cfset arguments.applicationscope.com.filemanager.tempFileCleanupForCurrentSession(sessionScope = arguments.sessionScope, applicationScope = arguments.applicationScope) >

			<cfreturn result>
	</cffunction>


	<!--- removes temp files which have for some reason been left by sessions which are no longer active --->
	<cffunction name="tempSessionFileCleanup" runOnEach="instance" runEveryDatePart="h" runEvery="1">
		<cfargument name="applicationScope" default="#application#">

			<cfset var result = {isOK = true}>

			<cfset arguments.applicationscope.com.filemanager.tempSessionFileCleanup(applicationScope = arguments.applicationScope) >

		<cfreturn result>
	</cffunction>


	<cffunction name="tempFileCleanup" runOnEach="instance" runEveryDatePart="d" runEvery="1">
		<cfargument name="hoursOld" default="24">
		<cfargument name="applicationScope" default="#application#">

			<cfset var result = {isOK = true,count = 0}>
			<cfset var tempFiles = "">

			<cfdirectory name="tempFiles" directory="#applicationScope.paths.temp#" recurse="yes">

			<cfloop query="tempFiles">
				<cfif datediff("h",datelastmodified,now()) gt hoursOld>
					<cftry>
						<cffile action="delete" file="#directory#/#name#">
						<cfset result.count = result.count + 1>
						<cfcatch>
						</cfcatch>
					</cftry>
				</cfif>
			</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="runPLODeletion" runOnEach="database" runEveryDatePart="n" runEvery="10" hint="">
		<cfargument name="applicationScope" default="#application#">

			<cfset var result = {isOK = true}>
			<cfset var doDelete = "">

			<cfquery name="doDelete" datasource = "#applicationScope.siteDataSource#">
			exec deleteFlaggedPeople
			exec deleteFlaggedLocations
			exec deleteFlaggedOrganisations
			</cfquery>

			<cfreturn result>

	</cffunction>


	<cffunction name="bounceBackProcessing" runOnEach="database" runEveryDatePart="n" runEvery="10" hint="">
		<cfargument name="applicationScope" default="#application#">

		<cfset var result = applicationScope.com.communications.runBounceBackProcess(maxrows = 50)>

		<cfreturn result>

	</cffunction>


	<!--- WAB 2011/10/31 --->
	<cffunction name="clearOutErrorLogs" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="">
		<cfargument name="applicationScope" default="#application#">

			<cfset var result = "">
<!--- WAB 2012-09-20
				Set these request variables
				needed later on and not automatically set in housekeeping process (normally gets time from db, but this will be fine)
			--->
			<cfparam name="request.requestTime" default="#now()#">
			<cfparam name="request.NowOffsetSeconds" default="0">

			<cfset result = applicationScope.com.errorHandler.cleanUpErrorLogs()>

			<cfreturn result>


	</cffunction>

	<!--- NJH 2012/04/26 expire any api sessions that need expiring --->
	<cffunction name="cleanUpApiSessions" runOnEach="database" runEveryDatePart="n" runEvery="10" hint="Expire any api sessions that need expiring" returntype="struct">
		<cfargument name="applicationScope" default="#application#">

		<cfreturn applicationScope.com.relayAPI.cleanUpApiSessions(datasource=applicationScope.siteDataSource, applicationScope=applicationScope)>
	</cffunction>

	<!---
		WAB 2012/09/20
		2015-04-01	WAB CASE 444268 locks were being deleted incorrectly.
					Fixed function in globalFunctions and send a warning email if a lock is deleted
		2015-07-02	WAB Moved core of this function to globalFunctions
	--->
	<cffunction name="deleteOldProcessLocks" runOnEach="database" runEveryDatePart="n" runEvery="5" returntype="struct">
		<cfargument name="applicationScope" default="#application#">

		<cfset var result = applicationScope.com.globalFunctions.deleteOldProcessLocksAndSendWarnings(applicationScope=applicationScope)>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2012/12/04 - Roadmap 2013 - moved from the elearning scheduled tasks --->
	<cffunction name="updateTrainingCertifications" runOnEach="database" runEveryDatePart="n" runEvery="10">
		<cfset var result = {isOK = true}>

		<cfset application.com.relayElearning.expireInvalidCertifications()>
		<cfset application.com.relayElearning.passValidPersonCertifications()> <!--- this is run in case there is a db update done --->
		<cfset application.com.relayElearning.expireInvalidPersonCertifications()>

		<cfreturn result>
	</cffunction>


	<cffunction name="updateTrainingSpecialisations" runOnEach="database" runEveryDatePart="n" runEvery="10">
		<cfset var result = {isOK = true}>

		<cfset application.com.relayElearning.insertEntitySpecialisations()> <!--- excludeEntityWithFlags was not ported over, as it's not being used.. and it would require a new setting.. so, add it when required --->
		<cfset application.com.relayElearning.expireInvalidEntitySpecialisations()>

		<cfreturn result>
	</cffunction>

	<cffunction name="cleanUpCachedQueriesForSession" runOnEach="sessionEnd" hint="Delete any cached queries that need deleting" returntype="struct">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="sessionScope" default="#session#">

		<cfset var result = {isOK = true}>
		<cfset var deleteCachedQueries = "">

		<cfquery name="deleteCachedQueries" datasource = "#applicationScope.siteDataSource#">
			delete from cachedQuery where sessionID =  <cfqueryparam value="#sessionScope.sessionID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2013/02/20 Sprint 4 - Item 13 - send tweets
	<cffunction name="sendScheduledTweets" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="Send any tweets that have been scheduled" returntype="struct">

		<cfset var result = {isOK = true}>
		<cfset var getScheduledTweets = "">
		<cfset var shareResult = "">
		<cfset var setSentDate = "">

		<cfquery name="getScheduledTweets" datasource = "#application.siteDataSource#">
			select b.broadcastID, m.text_defaultTranslation, se.serviceEntityID, b.broadcastaccountID, m.url, m.image
			from broadCast b
			inner join broadcastaccount ba on ba.broadcastaccountID = b.broadcastaccountID
			left join serviceEntity se on se.entityID = ba.broadcastaccountID and se.entityTypeID=#application.entityTypeID.broadcastAccount#
			inner join message m on b.messageID = m.messageID
			where
				m.sendDate between getDate()-2 and getDate()
				and b.sentDate is null
		</cfquery>

		<cfloop query="getScheduledTweets">
			<cfif serviceEntityID neq "">
				<cfset shareResult = application.com.twitter.share(comment=text,entityID=broadcastaccountID,entityTypeID=application.entityTypeId.broadcastAccount, image = image)>

				<cfif shareResult.isOK>
					<cfquery name="setSentDate" datasource="#application.siteDataSource#">
						update broadcast set sentDate = getDate(),
							lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
							lastUpdated = getDate(),
							lastUpdatedByPerson = #request.relayCurrentUser.personID#
						where broadCastID = <cf_queryParam value="#broadcastID#" cfsqltype="cf_sql_integer">
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>	 --->


	<cffunction name="sendScheduledBroadcasts" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="Send any page posts to facebook that have been scheduled" returntype="struct">

		<cfset var result = {isOK = true}>
		<cfset var getScheduledBroadcasts = "">
		<cfset var shareResult = {}>
		<cfset var setSentDate = "">

		<cfquery name="getScheduledBroadcasts" datasource = "#application.siteDataSource#">
			select b.broadcastID, m.text_defaulttranslation, se.entityTypeID, se.serviceEntityID, b.broadcastaccountID, m.url, m.image, ba.accessToken, s.serviceTextID, ba.serviceAccountID
			from broadCast b
				inner join broadcastaccount ba on ba.broadcastaccountID = b.broadcastaccountID
				inner join service s on ba.serviceID = s.serviceID
				left join serviceEntity se on se.entityID = ba.broadcastaccountID and se.entityTypeID =  <cfqueryparam value="#application.entityTypeID.broadcastAccount#" CFSQLTYPE="CF_SQL_INTEGER" >
				inner join message m on b.messageID = m.messageID
			where m.sendDate between getDate()-2 and getDate()
				and b.sentDate is null
				and s.serviceTextID in ('twitter','facebook')
		</cfquery>

		<cfloop query="getScheduledBroadcasts">
			<cfset shareResult.isOk = false>

			<cfif serviceEntityID neq "" and serviceTextID eq "twitter">
				<cfset shareResult = application.com.twitter.share(comment=getScheduledBroadcasts.text_defaulttranslation,entityID=getScheduledBroadcasts.broadcastaccountID,entityTypeID=application.entityTypeId.broadcastAccount,image=getScheduledBroadcasts.image,link=getScheduledBroadcasts.url)> <!--- 2014-11-14	AXA CORE-947 Twitter messages duplicating when scheduled, added variable scoping --->
			<cfelseif accessToken neq "" and serviceAccountID neq "" and serviceTextID eq "facebook">
				<cfset shareResult = application.com.facebook.postPageData(pageID=serviceAccountID,access_token=accessToken,message=text_defaulttranslation, picture=image,link=url)>
			</cfif>

			<cfif shareResult.isOK>
				<cfquery name="setSentDate" datasource="#application.siteDataSource#">
					update broadcast set sentDate = getDate(),
						lastUpdatedBy=<cfqueryParam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
						lastUpdated = getDate(),
						lastUpdatedByPerson = <cfqueryParam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
					where broadCastID = <cfqueryParam value="#getScheduledBroadcasts.broadcastID#" cfsqltype="cf_sql_integer"> <!--- 2014-11-14	AXA CORE-947 Twitter messages duplicating when scheduled, added variable scoping --->
				</cfquery>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="refreshSelections" runOnEach="database" runEveryDatePart="d" runEvery="1" hint="Refresh any selections that need refreshing." returntype="struct">
		<cfset var result = {isOK = true}>
		<cfset result = application.com.selections.refreshSelections()>

		<cfreturn result>
	</cffunction>


	<!--- 2014-11-10	RPW	CORE-881 Social updates are not seen - Changed function SendSocialShares parameter runEveryDatePart from "d" (day) to "n" minute --->
	<cffunction name="SendSocialShares" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="Send any social shares that need sharing" returntype="struct">
		<cfset var result = {isOK = true}>
		<cfset application.com.service.sendActivityShares()>

		<cfreturn result>
	</cffunction>


	<!--- WAB 2013-04-24 validate settings XML every hour --->
	<cffunction name="validateSettingXML" runOnEach="instance" runEveryDatePart="h" runEvery="1">

			<cfset var result = {isOK = true}>

			<cfset result.message = application.com.settings.validateSettingsXML() >

		<cfreturn result>
	</cffunction>


	<!--- 2016-06-17	WAB PROD2016-1280 Add validateSecurityXML() --->
	<cffunction name="validateSecurityXML" runOnEach="instance" runEveryDatePart="n" runEvery="2">

			<cfset var result = {isOK = true}>

			<cfset result.message = application.com.security.checkForCorruptedXMLAndReloadIfNecessary() >

		<cfreturn result>
	</cffunction>


	<cffunction name="tskCommSend" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="">

			<cftry>

			<cfmodule template = "/communicate/send/tskcommsend.cfm" userid = 0>

			<cfset var result = tskCommSend.resultStruct>

			<cfcatch>
				<cfmail from="william@relayware.com" to="william@relayware.com" subject = "housekeeping error" type="html">
					<cfdump var="#cfcatch#">
				</cfmail>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>


	<cffunction name="updateActivities" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="">
		<cfset var result = {isOK = true,message=""}>


		<cfset application.com.metrics.refreshActivities()>
		<cfset report=application.com.activityScore.awardNewScores()>
		<cfif report.OVERALLISOK EQ false>
				<cfset result.isOk=false>
				<cfset result.report=report>
		</cfif>



		<cfreturn result>
	</cffunction>


	<cffunction name="resetSynchAttemptForModifiedRecords" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="Resets the synch attempt to 0 for any records where errored fields have been modified in RW">
		<cfset var result = {isOK = true,message=""}>

		<cftry>
			<cfset application.getObject("connector.com.connector").resetSynchAttemptForModifiedRecords()>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2014/05/07 - done as part of the new connector work. New feature to delete empty orgs that don't have any locations --->
	<cffunction name="deleteEmptyOrgs" runOnEach="database" runEveryDatePart="d" runEvery="1" hint="Deletes any empty organisations">

		<cfset var result = {isOK = true,message=""}>
		<cfset var flagEmptyOrgsForDeletion = "">
		<cfset var deletedOrgFlagID = application.com.flag.getFlagStructure(flagID="deleteOrganisation").flagID>

		<cftry>
			<cfquery name="flagEmptyOrgsForDeletion">
				insert into booleanFlagData (flagID,entityId,created,createdBy,lastUpdated,lastUpdatedBy)
				select
					<cfqueryparam value="#deletedOrgFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					o.organisationID,
					getDate(),
					<cfqueryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,
					getDate(),
					<cfqueryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >
				from
						organisation o
					left join location l on l.organisationID = o.organisationID
					left join booleanFlagData bfd on bfd.entityID = o.organisationID and bfd.flagID =  <cfqueryparam value="#deletedOrgFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
				where
					bfd.entityID is null
				group by o.organisationID
				having count(l.locationID) = 0
			</cfquery>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<!--- remove any connector log files that are more than a day old --->
	<cffunction name="connectorLogCleanup" runOnEach="instance" runEveryDatePart="d" runEvery="1" hint="Removes any log files and debug tables that are more than 24 hours old">
		<cfargument name="hoursOld" default="24">
		<cfargument name="applicationScope" default="#application#">

			<cfset var result = {isOK = true,count = 0}>
			<cfset var logFiles = "">

			<cfdirectory name="logFiles" directory="#applicationScope.paths.content#\filesTemp\connector" recurse="yes">

			<cfloop query="logFiles">
				<cfif datediff("h",dateLastModified,now()) gt hoursOld and type neq "dir">
					<cftry>
						<cffile action="delete" file="#directory#/#name#">
						<cfset result.count = result.count + 1>

						<cfcatch>
							<cfset result.isOK = false>
							<cfset result.message = cfcatch.message>
						</cfcatch>
					</cftry>

					<!--- clean up any debug tables. This will look for tablenames with the given 'runID' --->
					<cfset application.getObject("connector.com.connectorUtilities").cleanUpTempTables(tableSuffix=listFirst(listLast(name,"_"),".")&"_ConnectorDebug")>
				</cfif>
			</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- keep the connectorResponse table tidy - only keep the last 5000 records --->
	<cffunction name="connectorResponseCleanup" runOnEach="instance" runEveryDatePart="h" runEvery="1" hint="Remove all but the last 20000 connectorResponse records to keep the table small.">

		<cfset var result = {isOK = true}>

		<cftry>
			<!--- Sugar 448248 NJH 2016/03/18 - don't delete connector response records that are also in the connectorQueueError table --->
			<cfquery name="deleteConnectoResponseRecords">
				delete top (10000) connectorResponse
				from connectorResponse r
					left join connectorQueue q on r.ID = q.connectorResponseID
					left join connectorQueueError qe on r.ID = qe.connectorResponseID
					left join connectorQueue qd on r.ID = qd.dataConnectorResponseID
				where
					q.connectorResponseID is null and qe.connectorResponseID is null and qd.dataConnectorResponseID is null
					and r.ID not in (select top 20000 ID from connectorResponse order by id desc)
			</cfquery>

			<!--- NJH 2016/05/11 JIRA PROD2016-934 - remove the environment setting on production if it's set, so that it doesn't get moved down to staging in a refresh 
					WAB 2016-12-12 fixed reference to com.setting (without the 'S')
			--->
			<cfif application.testsite eq 0 and application.com.settings.getSetting("connector.salesforce.synchWithEnvironment") neq "">
				<cfset var deleteConnectorEnvironmentSetting = "">

				<cfquery name="deleteConnectorEnvironmentSetting">
					delete from settings where name='connector.salesforce.synchWithEnvironment'
				</cfquery>
			</cfif>

			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="checkIfConnectorIsRunning" runOnEach="database" runEveryDatePart="n" runEvery="5">

		<!--- NJH 2015/09/17 removed reset from the url, as it now doesn't overrun the process lock, therefore no longer having the affect it had --->
		<cftry>
			<cfset var result = {isOK = true,message=""}>
			<cfset var getLastSynchAndConnectorType = application.getObject("connector.com.connector").getLastSuccessFulSynch()>
			<cfset var connectorType = getLastSynchAndConnectorType.connectorType[1]>

			<cfif not application.com.globalFunctions.doesLockExist(lockname="#connectorType# Connector")>
				<cfif dateDiff("n",getLastSynchAndConnectorType.lastSuccessfulSynch,request.requestTime) gt (application.com.settings.getSetting("connector.scheduledTask.waitInMinutesBeforeNextRun") + 2) and not application.com.settings.getSetting("connector.scheduledTask.pause")>
					<!--- <cfschedule action="update" task="#getLastSynchAndConnectorType.connectorType# Connector" url="#request.currentSite.protocolAndDomain#/scheduled/connectorSynch.cfm?connectorType=#connectorType#" interval="once" operation="HTTPRequest" startDate="#dateFormat(now(),'mm/dd/yy')#" startTime="#dateAdd('s',5,now())#"> --->
					<!--- NJH 2016/09/06 - JIRA PROD2016-2232 - call function to scheduled connector --->
					<cfset var scheduled = application.getObject("connector.com.connectorUtilities").scheduleConnectorSynch(connectorType=connectorType)>
					<cfset result.message = scheduled?"Connector Rescheduled":"Connector Not Rescheduled">
				</cfif>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<!--- delete any queue items where the data no longer exists... --->
	<cffunction name="deleteQueueItemForNonExistentRecords" runOnEach="database" runEveryDatePart="n" runEvery="5" hint="Delete records in the queue where they have no data (ie. they have been deleted) and where they are not a dependency">

		<cftry>
			<cfset var result = {isOK = true,message=""}>
			<cfset application.getObject("connector.com.connector").deleteQueueItemForNonExistentRecords()>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<!--- 2015-01-30 AHL Case 443215 Complete status for Events (whole function) --->
	<cffunction name="completeFinishedEvents" runOnEach="Database" runEveryDatePart="h"	runEvery = "12">
		<cfset var result = {isOK = false}>
		<cfset result = application.com.relayEvents.updateCompletedEvents() />

		<cfreturn result>
	</cffunction>


	<!--- 2014-11-27 AHL Case 442668 geo-data population task not running --->
	<cffunction name="updateLocationGoogleCoordinates" runOnEach="database" runEveryDatePart="n" runEvery="10" hint="Updating Geo-data for modified locations">
		<cfset var result = application.com.relayLocator.updateLocationGoogleCoordinates()>
		<cfreturn result>
	</cffunction>
	<!--- 2014-11-27 AHL Case 442668 geo-data population task not running --->



	<!--- WAB 2015-02-24 CASE 443944 Add function to clean out oAuth_Token table --->
	<cffunction name="deleteOauthTokens" runOnEach="database" runEveryDatePart="d" runEvery="1" hint="Delete old oAuth Tokens">
		<cfset var oDAO = CreateObject("component", "oauth.oauthtokendao")>
		<cfset var result = {isOK = true}>
		<cfset result.itemsDeleted = oDAO.deleteOldTokens()>
		<cfreturn result>
	</cffunction>

	<cffunction name="deleteExpiredTokens" runOnEach="database" runEveryDatePart="d" runEvery="1" hint="Delete old tokens (which includes OAuth2 tokens)">
		<cfset var result = {isOK = true}>
			<cfquery name="deleteExpiredTokens" datasource = "#applicationScope.siteDataSource#">
				delete from token where expiryDate< GETDATE()
			</cfquery>
		<cfreturn result>

	</cffunction>


	<!--- WAB 2015-02-07 (actually written long time ago but not committed) --->
	<!--- NJH 2016/11/22 JIRA PROD2016-1190 - removed the need for assigned entity Ids
	<cffunction name="cleanUpAssignedEntityIDsTable" runOnEach="database" runEveryDatePart="d" runEvery="1" hint="">
		<!---
		This table only needs one record per entityType, so might as well delete all the excess records
		 --->

		<cfset var result = {isOK = true}>

		<cfquery name="deleteAssignedEntityIDsRecords" datasource = "#applicationScope.siteDataSource#">
			delete assignedEntityIDs
			from assignedEntityIDs a
			inner join
				(
				select entityTypeID, max(newSeedValue) as maxValue
				from assignedEntityIDs
				group by entityTypeID
				) as maxValues
			ON a.entityTypeID = maxValues.entityTypeID
			and a.newSeedValue < maxValues.maxValue
		</cfquery>

		<cfreturn result>

	</cffunction>--->


	<!--- 2015-01-30 AHL Case 443215 Complete status for Events (whole function) --->
	<cffunction name="switchOffDebug" runOnEach="Instance" runEveryDatePart="n"	runEvery = "5">
		<cfset var result = {isOK = true}>
		<cfset var switchOffAfter_min = 30>

		<cfif NOT application.com.relayCurrentSite.isEnvironment('dev')>
			<!--- is debug on --->
			<cfif application.com.cfadmin.checkCFPassword ()>
				<cfif application.com.cfadmin.getDebug()>
					<!--- if so, when was it last switched on --->
					<cfset var timeOn = application.com.cfadmin.getMinutesSinceDebugLastSwitchedOn()>
					<cfif timeOn gt switchOffAfter_min or timeOn IS -1>
						<!--- Switch off if more than 30 mins ago --->
						<cfset application.com.cfadmin.setDebug(false)>
						<cfset result.message = "Debug Turned Off">
					<cfelse>
						<cfset result.message = "Debug Has only been on for #timeOn# minutes">
					</cfif>
				<cfelse>
					<cfset result.message = "Debug Not On">
				</cfif>
			<cfelse>
				<cfset result = {isOK = false, message = "Incorrect CFAdmin Password"}>
			</cfif>
		<cfelse>
			<cfset result.message = "Dev Site - debug left on">
		</cfif>

		<cfreturn result>
	</cffunction>


    <cffunction name="runDeactivateExpiredModules" hint="I run the task to make expired modules inactive" runOnEach="database" runEveryDatePart="d" runEvery="1">
        <cfscript>
            var result = {};

            result = application.com.relayElearning.deactivateExpiredModules();

            return result;
        </cfscript>
    </cffunction>


    <cffunction name="runRemindPersonCertificationExpiry" hint="I run the task to remind person certification expiry" runOnEach="database" runEveryDatePart="d" runEvery="1">
        <cfset var result = {isOK = true}>
        <cfset var noticePeriod = application.com.settings.getSetting("elearning.personCertificationExpiryNoticePeriod")>

        <cfif noticePeriod NEQ "" and isNumeric(noticePeriod) and noticePeriod GT 0>
            <cfset result = application.com.relayElearning.sendPersonCertificationExpiryAlerts(noticePeriod)>
        </cfif>

        <cfreturn result>
    </cffunction>


    <cffunction name="runRemindModuleExpire" hint="I run the task to alert modules that will expire after the given notice period" runOnEach="database" runEveryDatePart="d" runEvery="1">
        <cfset var result = {isOK = true}>
        <cfset var noticePeriod = application.com.settings.getSetting("elearning.moduleExpiryNoticePeriod")>

        <cfif noticePeriod NEQ "" and isNumeric(noticePeriod) and noticePeriod GT 0>
            <cfset result = application.com.relayElearning.sendModuleExpiryAlerts(noticePeriod)>
        </cfif>

        <cfreturn result>
    </cffunction>


</cfcomponent>