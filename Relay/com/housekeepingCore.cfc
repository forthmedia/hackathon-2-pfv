<!--- 
WAB 2010
Controls all Housekeeping requests
See http://relayware.wikidot.com/housekeeping

Use HouseKeeping.cfc for your individual housekeeping functions


WAB 2011/10/31 	Added code so that elapsed time of is recorded for each function run
WAB 2012/12/05 	Worked out a way of firing off asynchronous processes using CFTHREAD rather than needing an Asynchronous gateway
WAB 2016-01-16	Added visitid to the dummy user struct - now needed if an error is thrown during housekeeping
WAB 2016-03-17	PROD2016-118  Housekeeping Improvements
				Completely reworked so does not run in a thread at end of request
				Threading was causing some problems (when trying to turn off debug and when trying to change locale when sending emails) and is difficult to debug
				Now has its own automatic/self created scheduled task which actually runs \scheduled\housekeeping.cfm
WAB	2016-04-06	Bootstrapping issue - application.com.globalFunctions not available at the time I run initialise()
WAB 2016-06-10  BF-974 Problems handling Errors.
				If housekeeping process times out we were not able to log the error or continue
				ErrorHandler expected a few request variables which were not available
				Make sure that each housekeeping task has atleast 60 seconds available
				Also improved the Debugging EMail (wasn't telling me which site had errored)
				request.currentSite expected by some housekeeping Tasks
WAB 2016-10-17	PROD2016-2542 Problems when dbTime and CFTime are wildly different.  Make sure that CFschedule is set using CFTime
WAB 2016-10-31	PROD2016-2542 continued, dependency issue with above
--->	

	<cfcomponent cache="false">

	<cfset variables.CheckScheduledTaskIfOverdue_mins = 1>

	<CFFUNCTION name="initialise" output="false">
		<cfargument name="applicationScope" default="#application#">
		
		<cfset variables.applicationScope = applicationScope>
		<cfset variables.scheduledTaskName = "HouseKeeping_#applicationScope.applicationName#">
		<cfset variables.dateFunctions = createObject ("component","com.dateFunctions")>
		

		<cfset initialiseHousekeepingTable ()>
		<cfset setUpHouseKeepingScheduledTask (dateAdd("m",0,getTimeNextNeedsRunning()))>  <!--- Add a minute to  --->
		<cfset removeExpiredScheduledTasks ()>

		<cfset this.lastRunTimeThisInstance = getTimeLastRunThisInstance()>  

		<cfreturn this>

	</CFFUNCTION>


	<CFFUNCTION name="initialiseHousekeepingTable" hint="This adds any new methods to the db (and updates the runOnEach, just in case it has been changed)" output="false">

		<cfset var housekeepingFunctions = getHouseKeepingFunctionsArray()>
		<cfset var firstPass = true>
		<cfset var FunctionMetaData = "">
		<cfset var Insert = "">
		
		<cfquery name = "insert">
		MERGE into housekeeping
		USING
			(values
				<cfloop array="#housekeepingFunctions#" index="functionMetaData">
					<cfif not firstPass >,<cfelse><cfset firstPass = false></cfif>
					(<cf_queryparam value = "#functionMetaData.name#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value = "#functionMetaData.runOnEach#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				</cfloop>
			) as functions (methodName,runoneach)	
		ON functions.methodName = housekeeping.methodName
		WHEN MATCHED AND functions.runOnEach <> housekeeping.runOnEach THEN
			UPDATE SET 	runOnEach = functions.runOnEach
		WHEN NOT MATCHED THEN
			INSERT (methodName, runOnEach)
			VALUES (functions.methodName,functions.runOnEach );

		</cfquery>
			
	</CFFUNCTION>


	<cffunction name="doesHouseKeepingNeedRunning" hint="uses getMinutesUntilNextNeedsRunning()" output="false">

		<cfset var result = false>

		<cfif getMinutesUntilNextNeedsRunning() LT 0>	
			<cfset result = true>	
		</cfif>	
	
		<cfreturn result>
	</cffunction>


	<cffunction name="getTimeSincelastRunTime" returntype="Numeric" hint="Difference between now and lastRunTime" output="false">
		<cfreturn datediff("n",this.lastRunTimeThisInstance, dbTime())>
	</cffunction>


	<cffunction name="getMinutesUntilNextNeedsRunning" returntype="Numeric" output="false">
			<cfreturn dateDiff ("n", dbTime(), getTimeNextNeedsRunning())>
	</cffunction>


	<cffunction name="getTimeNextNeedsRunning" returntype="date" output="false">
		<cfargument name="runOnEach" default = "">
		
		<cfset var getNextRun = "">

		<cfquery name="getNextRun">
		select min (isNull(nextRunTime,getdate()-.01)) as [time]
		from vhousekeepinglog
		where 
			FilterOnThisColumnToRemoveDuplicateRunOnEachDatabaseItems=1 
			and methodName in (#listQualify(getFunctionNamesList(),"'")#)
			and
			(	runOnEach = 'database' 
				or 
				(runOnEach = 'instance' and coldfusioninstanceid = #application.instance.coldfusionInstanceId#)
			)	
		</cfquery>

		<cfset var result = getNextRun.Time>

		<cfreturn result>
	</cffunction>
	

	<cffunction name="getTimeLastRunThisInstance" returntype="date" output="false">

		<cfset var getLastRun = "">
		<cfquery name="getLastRun">
		select isNull(max (lastRunTime),0) as [time]
		from vhousekeepinglog
		where 
			coldfusioninstanceid = #application.instance.coldfusionInstanceId#
		</cfquery>

		<cfreturn getLastRun.time>		

	</cffunction>


	<cffunction name="getTimeFunctionNextNeedsRunning" returntype="date" output="false">
		<cfargument name="FunctionMetaData" required="true">

		<cfset var getNextRunTime = "">

		<cfquery name="getNextRunTime">
		select methodname, max (nextRunTime) as nextRunTime
		from vhousekeepinglog
		where 
			FilterOnThisColumnToRemoveDuplicateRunOnEachDatabaseItems=1 
			and methodName = '#FunctionMetaData.Name#' 
			and
			(	runOnEach = 'database' 
				or 
				(runOnEach = 'instance' and coldfusioninstanceid = #application.instance.coldfusionInstanceId#)
			)	
		group by methodname
		</cfquery>
		
		<cfset var result = getNextRunTime.nextRunTime>
		
		<cfif result is "">
			<cfset result = getnextRunTimeFromlastRunTime (FunctionMetaData,0)>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="doesFunctionNeedRunning" returntype="Boolean" output="false">
		<cfargument name="FunctionMetaData" required="true">
	
		<cfset var result = false>
		<cfif getTimeFunctionNextNeedsRunning(FunctionMetaData) lte dbTime()>
			<cfset result = true>
		</cfif>
		<cfreturn result>

	</cffunction>


	<cffunction name="getMinutesUntilFunctionNeedsRunning" returntype="Numeric" output="false"> 
		<cfargument name="FunctionMetaData" required>
	
		<cfreturn dateDiff ("n", getTimeFunctionNextNeedsRunning(FunctionMetaData),dbTime())>

	</cffunction>


	<cffunction name="isHouseKeepingRunning" hint="" returnType="boolean" output="false">
		<cfset var result = true>
		<cflock name="housekeeping" throwOntimeout="false" timeout = "0">
			<cfset result = false>
		</cflock>

		<cfreturn result>	
	</cffunction>


	<cffunction name="checkHouseKeepingScheduledTask" hint="" returntype="string" output="false">
		<cfset var result = "">
	
		<cfif dateDiff ("n", this.scheduledToRunAtCFTime, now() ) GT variables.CheckScheduledTaskIfOverdue_mins  >

			<!--- looks like a problem (but might be something long running --->			
			<!--- check whether there is an outer lock --->
			<cfif not isHouseKeepingRunning ()>
				<!--- Leave a debug email in while testing --->
				<cfmail from="william.bibby@relayware.com" to="william.bibby@relayware.com" subject="Housekeeping Not Run When Expected.  #cgi.server_name#" type="html">
					Scheduled to Run at: #this.scheduledToRunAtCFTime# CFTime<BR>
					Now is #now()#<BR>
					#dateDiff ("n", this.scheduledToRunAtCFTime, now() )# GT #variables.CheckScheduledTaskIfOverdue_mins#<br />
				</cfmail>
				<cf_log file="housekeeping" text="Housekeeping Check - Scheduled Task Not Scheduled">
				<cfset result = setUpHouseKeepingScheduledTask()>
			</cfif>
			
		</cfif>
	
	</cffunction>


	<cffunction name="callHousekeepingOnInstancesWhereHousekeepingIsNotRunning" hint="" returntype="struct" output="true">
	
		<cfset var result = {}>
		<cfset var getNonRunningInstances = getInstancesWhereHousekeepingIsNotRunning()>

		<cfloop query= "getNonRunningInstances">
			<cfset result[Instance] = callHouseKeepingOnAnotherInstance (coldfusionInstanceID)>	
		</cfloop>

		<cfreturn result>
	
	</cffunction>


	<cffunction name="getInstancesWhereHousekeepingIsNotRunning" hint="" returntype="query" output="false">
	
		<cfset var getNonRunningInstances = "">
		<cfquery name="getNonRunningInstances">
		select coldfusionInstanceID, instance,  Min(nextRunTime), max(lastRunTime) , datediff (n, Min(nextRunTime), getdate() ) 
		from vhousekeepingLog			
		where runOneach = 'instance'
		group by coldfusionInstanceID, instance
		having (datediff (n, Min(nextRunTime), getdate() ) >= 5 or Min(nextRunTime) is null)
		</cfquery>


		<cfreturn getNonRunningInstances>
	
	</cffunction>
	

	<cffunction name="callHouseKeepingOnAnotherInstance" hint="" returntype="struct" output="true">
		<cfargument name="coldfusionInstanceID" required="true" >		
		<cfargument name="houseKeepingMethodName" default="" >	
		<cfargument name="force" default="false" >	

		<cfset var callresult = application.com.clustermanagement.runFunctionOnClusterWithOutThreading (instanceID = coldfusionInstanceID,functionName = "runHousekeeping",houseKeepingMethodName=houseKeepingMethodName, force = force)>
	
		<cfset var result =  callresult[listfirst (structKeyList(callresult))]>

		<cfreturn result>
	
	</cffunction>
	
		
	<cffunction name="setUpHouseKeepingScheduledTask" returntype="string" output="false">
		<cfargument name="datetime" required="true" default="#dateAdd("n",1,getTimeNextNeedsRunning())#">
		
		<cfset var result = "">

		<cfset dateTimeCFTime = variables.dateFunctions.convertDBTimeToCFTime (datetime)>
		
		<!--- Make sure not in past --->
		<cfif dateTimeCFTime lt dateAdd("n",1, now ())>
			<cfset dateTimeCFTime = dateAdd("n",1, now ())>
		</cfif>
		
		<!--- Schedule for next whole minute (I just find it easier to read and I don't get my minutes and seconds confused when debugging) --->
		<cfset dateTimeCFTime = dateAdd("s",60-datepart("s",dateTimeCFTime),dateTimeCFTime)>
		
		<cfif not structKeyExists (this, "scheduledToRunAt") OR this.scheduledToRunAtCFTime is NOT dateTimeCFTime>
			<cf_log file="housekeeping" text="setUpHouseKeepingScheduledTask #dateTime#">
			<cfschedule 
						action="update" 
						task="#variables.scheduledTaskName#" 
						url="#listFirst(cgi.server_protocol,"/")#://#cgi.server_name#/scheduled/houseKeeping.cfm?" 
						interval="once" 
						operation="HTTPRequest" 
						startDate="#dateFormat(dateTimeCFTime,'mm/dd/yy')#" 
						startTime="#timeFormat(dateTimeCFTime,"HH:mm")#" >
	
			<cfset this.scheduledToRunAtCFTime = dateTimeCFTime>
	
			<cfset result =  "Schedule Set to run at #dateTimeCFTime# (CFTime)">
		<cfelse>

			<cfset result =  "Already Scheduled to run at #dateTime# (CFTime)">
		</cfif>		

		<cfreturn result>
	
	</cffunction>


	<cffunction name="getScheduledTask" returntype="query" output="false">

		<cfset var scheduledTasks = "">
		<cfset var scheduledTask = "">
		
		<cfschedule action="list" result = "scheduledTasks">
		<cfquery dbtype="query" name="scheduledTask">
		select task, [interval], remaining_count, status, url, startdate, starttime from scheduledTasks
		where lower(Task) = '#lcase(variables.scheduledTaskName)#'
		</cfquery>
	
		<cfreturn scheduledTask>
	</cffunction>

	
	<cffunction name="isScheduledTaskSetUp" returntype="boolean" output="false" >
		<cfset var scheduledTaskQuery = getScheduledTask()>
		
		<cfset var result = (scheduledTaskQuery.recordCount is 1 and scheduledTaskQuery.status is "running")>
		
		<cfreturn result>
	
	</cffunction>


	<cffunction name="removeExpiredScheduledTasks" returntype="void">

		<cfset var scheduledTasks = "">
		
		<cfschedule action="list" result = "scheduledTasks">
		<cfquery dbtype="query" name="scheduledTasks">
		select task, status
		from scheduledTasks
		where (lower(Task) like 'housekeeping%' and lower(Task) <> '#lcase(variables.scheduledTaskName)#')
		and status in ('expired')
		</cfquery>

		<cfloop query="scheduledTasks">
			<cfschedule task ="#task#" action = "delete">
		</cfloop>

		<cfreturn >
	</cffunction>


	<cffunction name="createDummyRequestStructure" returntype="struct" hint="creates a dummy one and returns the old one" output="false">
	
		<cfset var oldRequest = {}>

		<cfif not structKeyExists (request,"createDummyRequestStructure")>
			
			<cfset var oldRequest = duplicate (request)>
	 		<cfset var scheduledTaskPerson = application.com.globalFunctions.getScheduledTaskPerson()>
	 		
			<cfset var tempRequest = {
							requestTime = '#dateFormat(dbTime(),"yyyy-mm-dd")# #timeFormat(dbTime(),"HH:mm:ss")#',
							requestTimeODBC = dbTime(), 
							requestid = structKeyExists(request,"requestid")?request.requestid:createUUID(), 
							currentSite = oldRequest.currentSite,
							housekeeping = true }>
			<!--- WAB 2013-12-10 CASE 438036 added locationid and organisationid to the dummy relaycurrentuser structure, a piece of code in flag.cfc (called when merging a comm) was expecting the keys to exist --->
			<cfset tempRequest.relayCurrentUser = 
						{userGroupID = scheduledTaskPerson.userGroupID, 
						personid = scheduledTaskPerson.personID, 
						errors = {queryParamDebug=false, showDump=false, showLink = false},
						languageid = 0, 
						content= {showForCountryID = 0, doNotTranslate = false},
						isloggedin = 0,
						locationid = 0, 
						organisationid = 0, 
						visitid = 0, 
						fullName = "Housekeeping",
						person = {firstname = "Housekeeping", lastname = "Task"}
					} > 

			<cfset structClear(request)>   <!--- don't want anything relaycurrentuser ish --->
			<cfset structAppend(request,tempRequest)>

		</cfif>

		<cfreturn oldRequest>

	</cffunction>


	<cffunction name="reinstateRequestStructure" output="false">
		<cfargument name="oldRequest" required>
		
		<cfif structCount (oldRequest)>
			<cfset structClear(request)>   <!--- don't want anything relaycurrentuser ish --->
			<cfset structAppend(request,oldRequest)>
		</cfif>
		
	</cffunction>


	<cffunction name="runHouseKeepingIfRequired" returntype="struct" output="false">

		<cfset var result = {isok=true,needsRunning = doesHousekeepingNeedRunning()}>

		<cf_log file="housekeeping" text="Start">
		<cfif result.needsRunning and not isHouseKeepingRunning() >
			<cflock name="housekeeping" throwOntimeout="false" timeout = "0">
				<cfset var oldRequest = createDummyRequestStructure ()>
		 		<cfset result.result = runHouseKeeping()>
				<cfset reinstateRequestStructure(oldRequest)>

				<cfset result.otherInstances = callHousekeepingOnInstancesWhereHousekeepingIsNotRunning ()>  
			</cflock>
		<cfelse>
			<cfset result.timeUntilnextRunTime = getMinutesUntilNextNeedsRunning()>
		</cfif>

		<cfset setUpHouseKeepingScheduledTask ()>
		<cf_log file="housekeeping" text="End">

		<cfreturn result>	
	</cffunction>


	<!--- 
		Gets all the functions in the CFC
		When I started I hadn't realised that there was more than
		1 array of functions within the metadata.
		Turns out that when the cfc is extended you get two separate arrays
		Had wanted to append the arrays together by CF told me that it could not do an array merge (structurefunctions.arrayMerge) on that particular object type
		So have ended up with a weird array of arrays
	 --->
	<cffunction name="getArraysOfFunctionsInCFC" returntype="array" output="false">

		<!--- WAB	2016-04-06	Bootstrapping issue - application.com.globalFunctions not available at the time I run initialise() which calls this.  So create own object - this function only called twice because the result is cached --->
		<cfset var globalfunctions = createObject("com.globalFunctions")>
		<cfreturn globalfunctions.getArrayOfCFCMethods(this)>
	
	</cffunction>
	
	<cffunction name="getHouseKeepingFunctionsArray" returntype="array" output="false">
		
		<cfif not structKeyexists (this,"houseKeepingFunctionsArray")>

			<cfset  var functionArray = getArraysOfFunctionsInCFC()>

			<cfset this.houseKeepingFunctionsArray = arrayNew(1)>
	
				<cfset var FunctionMetaData = "">
				<cfloop index="FunctionMetaData" array="#functionArray#">
					<cfif structKeyExists (FunctionMetaData,"runOnEach") and listfindnocase ("instance,database",FunctionMetaData.runOnEach)>
						<cfif not structKeyExists (FunctionMetaData,"minutesBetweenRuns")>
							<cfset FunctionMetaData.minutesBetweenRuns = datediff("n",0,dateadd(FunctionMetaData.runEveryDatePart,FunctionMetaData.runEvery,0))>
						</cfif>

						<cfset arrayAppend(this.houseKeepingFunctionsArray,FunctionMetaData)>
					</cfif>	
				</cfloop>		
		</cfif>

		<cfreturn 	this.houseKeepingFunctionsArray>

	</cffunction>
	

	<cffunction name="getFunctionNamesList" returntype="string" output="false" hint="Often need to filter queries by this list because it is theoretically possible (and very possible on dev sites) to have different housekeeping tasks on each instance">
		<cfset var result = "">
		<cfset var FunctionMetaData = "">
		
		<cfloop array=#getHouseKeepingFunctionsArray()# index="FunctionMetaData">
			<cfset result = listAppend (result, FunctionMetaData.name )>
		</cfloop>
		<cfreturn result>
	</cffunction>

	
	<cffunction name="getHouseKeepingFunctionMetaData" output="false">
		<cfargument name="FunctionName" required>		

		<cfset var functionMetaData = "">
		<cfloop array = #getHouseKeepingFunctionsArray()# index="functionMetaData">
			<cfif functionMetaData.name is FunctionName>
				<cfreturn functionMetaData>
			</cfif>
		</cfloop>

		<cfreturn []>

	</cffunction>


	<cffunction name="getnextRunTimeFromlastRunTime" output="false">
		<cfargument name="FunctionMetaData" required>
		<cfargument name="lastRunTime" required>
		
		<cfset var nextRunTime = "">
		<!--- calculate the minutes between runs - used to order the functions to run --->
		<cfif not structKeyExists (FunctionMetaData,"minutesBetweenRuns")>
			<cfset arguments.FunctionMetaData.minutesBetweenRuns = datediff("n",0,dateadd(FunctionMetaData.runEveryDatePart,FunctionMetaData.runEvery,0))>
		</cfif>

		<cfif lastRunTime is 0>
			<cfset nextRunTime = dbTime()> 
		<cfelse>
			<cfset nextRunTime = dateAdd("n",FunctionMetaData.minutesBetweenRuns,lastRunTime) >
			<!--- If running every day then we will set for the middle of the night around 3am (with a slightly random minute element) --->
			<cfif FunctionMetaData.runEveryDatePart is "d">
				<cfset nextRunTime = dateAdd("h", - datePart("h",nextRunTime) + 3 ,nextRunTime)>
			</cfif>
		</cfif>


		<cfreturn nextRunTime>
	</cffunction>
	

	<cffunction name="getSessionHouseKeepingFunctionsArray" output="false">

		<cfset var functionArray = "">
		<cfset var FunctionMetaData = "">

		<cfif not structKeyexists (this,"sessionHouseKeepingFunctionsArray")>

			<cfset functionArray = getArraysOfFunctionsInCFC()>
	
			<cfset this.sessionHouseKeepingFunctionsArray = arrayNew(1)>

			<cfloop index="FunctionMetaData" array="#functionArray#">
				<cfif structKeyExists (FunctionMetaData,"runOnEach") and listfindnocase ("sessionEnd",FunctionMetaData.runOnEach)>
					<cfset arrayAppend(this.sessionHouseKeepingFunctionsArray,FunctionMetaData)>
				</cfif>	
			</cfloop>		

		</cfif>
		
		<cfreturn this.sessionHouseKeepingFunctionsArray>

	</cffunction>
	

	<cffunction name="runSessionEndHouseKeeping" output="false">
		<cfargument name="SessionScope" required="true">

		<cfset var functionObj = "">
		<cfset var FunctionMetaData = "">
		<cfset var thisResult = "">

		<cfloop index="FunctionMetaData" array="#getSessionHouseKeepingFunctionsArray()#">

			<cfset functionObj = this[FunctionMetaData.name]>						
			<cftry>
				<cfset thisResult = functionObj(applicationScope = variables.applicationScope, sessionScope = sessionScope)>
				<cfcatch>
					<cfset variables.applicationScope.com.errorHandler.recordRelayError_warning (catch = cfcatch,type="HouseKeeping Error",TTL=14)>
				</cfcatch>
			</cftry>
		</cfloop>

	</cffunction>


	<cffunction name="getHouseKeepingFunctionsDueToBeRun" output="false">

		<cfset var result = []>
		<cfset var allFunctions = getHouseKeepingFunctionsArray()>
		<cfset var FunctionMetaData = "">

		<cfloop index="FunctionMetaData" array="#allFunctions#">
		
			<cfif doesFunctionNeedRunning (FunctionMetaData)>
				<cfset arrayAppend(result,FunctionMetaData)>
			</cfif>
		
		</cfloop>

		<cfset result = application.com.structureFunctions.arrayOfStructsSort(arrayOfStructs = result,key="minutesBetweenRuns", sortType="numeric")>

		<cfreturn result>	
	
	</cffunction>


	<cffunction name="runHouseKeeping" output="false">
		<cfargument name="houseKeepingMethodName" default="" hint="to run a single task, give its name">
		<cfargument name="force" default="false" hint="Run Whether Required Or Not">
		<cfargument name="debug" default="false">


		<cfset var overallresult = {isOK=true, message="", results = {}}>

		<cfset var oldRequest = createDummyRequestStructure()>
		
		<cfif houseKeepingMethodName is not "">
			<cfset var ArrayOfFunctionsToRun = [getHouseKeepingFunctionMetaData(houseKeepingMethodName)]>
		<cfelse>
			<cfset ArrayOfFunctionsToRun = getHouseKeepingFunctionsDueToBeRun()>
		</cfif>

		<!--- set this at beginning so that other threads don't try to get here and then hit the lock --->
		<cfset this.lastRunTimeThisInstance = now ()>

		<cfset var FunctionMetaData = "">
		<cfloop index="functionMetaData" array="#ArrayOfFunctionsToRun#">

			<cfset var thisResult = runSingleHouseKeepingTask(functionMetaData = functionMetaData, force = force)>
			<cfset overallresult.results[functionMetaData.name] = thisResult>

			<cfif not StructKeyExists (thisResult,"notRun")>
				<cfset overallresult.message &= functionMetaData.name & ": #iif(thisResult.isOK,de('OK'),de('Failed'))#. <BR>">
			<cfelse>
				<cfset overallresult.message &= functionMetaData.name & ": #thisResult.message#<BR>">							
			</cfif>	
			
		</cfloop>
		
		<cfset reinstateRequestStructure(oldRequest)>

		<cfif houseKeepingMethodName is "">
			<cfset setUpHouseKeepingScheduledTask ()>
		</cfif>	
	
		<cfreturn overallresult>
	
	</cffunction>


	<cffunction name="runSingleHouseKeepingTask" output="false">
		<cfargument name="functionMetaData" required="true" >
		<cfargument name="force" default="false" hint="Run Whether Required Or Not">

		<cfset var insertHouseKeepingLog = "">
		<cfset var updateHouseKeepingLog = "">
		<cfset var thisresult = "">
		<cfset var thisresultJSON = "">
		<cfset var dataProcessingServer = '' />

		<cfset var okToRun = false>
		<cfset var thisResult = {isOK = true, message = ""}>

		<cf_lock name="#functionMetaData.name##functionMetaData.runOnEach is 'instance'?'_#application.instance.coldfusionInstanceId#':''#" throwOnTimeout="false">

			<cfif force or doesFunctionNeedRunning(functionMetaData)>
				<cfset okToRun = true>
			<cfelse>
				<cfset thisResult.notRun  = true>
				<cfset thisResult.message = "Not Due for #getMinutesUntilFunctionNeedsRunning(functionMetaData)#"  >
			</cfif>

			<cfif okToRun and functionMetaData.runOnEach is "database">
				<!--- If is a process which needs to be run on a database then it should be run on a dataprocessing server if there is one --->
				<cfset var dataProcessingServers = application.com.settings.getSetting(variablename="system.dataProcessingServers",personid = 0)>
				
				<cfif listfindnocase(dataProcessingServers,application.instance.servername)>
					<!--- we are the dp server, so OK--->
				<cfelseif dataprocessingservers is not "">
					<!--- is this dp server active --->
					<cfset var dpActive = false>
					<cfloop index="dataProcessingServer" list="#dataProcessingServers#">		
						<cfset dpactive = application.com.clustermanagement.isServerActive(servername=dataprocessingserver)>
						<cfif dpactive><cfbreak></cfif>
					</cfloop>

					<cfif dpactive>
						<cfset oktorun = false ><!--- leave it to the dp server--->
						<cfset thisResult.notRun  = true>
						<cfset thisResult.message = "Will be run on DP Server "  >
					</cfif>

				<cfelse>
				
					<!--- No dp server so ok to continue--->
				</cfif>						

			</cfif>
				
	
	
			<cfif OKToRun>
				<cfset application.com.request.extendTimeOutIfNecessary(60)>

				<cfquery name="insertHouseKeepingLog" datasource = "#application.sitedatasource#">
					declare @methodName sysname = <cf_queryparam value = "#functionMetaData.name#" CFSQLTYPE="CF_SQL_VARCHAR" >
					declare @coldFusionInstanceID int = <cf_queryparam value = "#application.instance.coldfusionInstanceId#" CFSQLTYPE="CF_SQL_INTEGER" >
					declare @housekeepingID int 

					select @housekeepingid = housekeepingid from housekeeping where methodname = @methodName
					
					IF NOT EXISTS (select 1 from houseKeepingLog_ where housekeepingid = @housekeepingid and coldfusionInstanceId = @coldFusionInstanceID)
					BEGIN
						insert into housekeepingLog_ 
						(housekeepingid,coldfusionInstanceId)
						values 	(@housekeepingid, @coldfusionInstanceId)
					END
				</cfquery>

				<cfset var functionObj = this[functionMetaData.name]>						
				<cftry>

					<cfset var startTime=dbTime()>
					<cfset thisResult = functionObj()>
					<cfcatch>
						<cfset application.com.errorHandler.extendRequestTimeOut ()>
						<cfset var errorid = application.com.errorHandler.recordRelayError_warning (severity="Error",catch = cfcatch,type="HouseKeeping Error",TTL=14)>
						<cfset var fileAndLine = application.com.errorHandler.extractTemplateInfoFromErrorStructure(cfcatch)>
						<cfset thisResult = {isOK = false, message="Error: #cfcatch.message# #fileAndLine.relativefilename# #fileAndLine.line#", errorID = errorid}>
					</cfcatch>
				</cftry>

				<cfset thisResult.elapsedTimeSeconds = datediff("s",startTime,dbTime())>					
				<!--- <cfset updateFunctionRunMetaData (functionMetaData,dbTime())> --->
				
				<cfset thisResultJSON = serializeJSON (thisResult)>		

				<!--- update housekeepingLog table --->
				<cfquery name="updateHouseKeepingLog" datasource = "#application.sitedatasource#">
					declare @methodName sysname = <cf_queryparam value = "#functionMetaData.name#" CFSQLTYPE="CF_SQL_VARCHAR" >
					declare @coldFusionInstanceID int = <cf_queryparam value = "#application.instance.coldfusionInstanceId#" CFSQLTYPE="CF_SQL_INTEGER" >
					declare @housekeepingID int 

					select @housekeepingid = housekeepingid from housekeeping where methodname = @methodName

					update 
						housekeepingLog_ 
					set 
						lastRunTime = #dbTime()#,
						nextRunTime = #getnextRunTimeFromLastRunTime(functionMetaData,dbTime())#,
						isOK = #iif(thisResult.isOK,de(1),de(0))#,
						metadata = '#thisResultJSON#'
					where 
						coldfusionInstanceId = @coldFusionInstanceID and housekeepingid = @housekeepingid 
				</cfquery>
				
			</cfif>

		</cf_lock>	

		<cfif cf_lock.timedOut>
			<cfset thisResult.notRun  = true>
			<cfset thisResult.message = "Already running">
		</cfif>


		<cfreturn thisResult>

	</cffunction>


	<cffunction name="dbTime">
		<cfreturn variables.dateFunctions.getDBTime()>
	</cffunction>


</cfcomponent>
