<!--- �Relayware. All Rights Reserved 2014 --->
<!---
sessionManagement.cfc

A few procedures for accessing all the sessions on a server

getApplications - gets list of the cfapplications on the server
getSessions - gets the details of all the sessions for a given application
setSessionVariable - sets a sessionvariable in a particular session

WAB   	2009/08/25 LID 2512 getListOfSessionIDs function made cluster aware
WAB 	2009/10/26   getListOfSessionIDs very slow if large number of sessions in existence.  Modify
WAB 	2016-10-04	PROD2016-2419 Added killListOfSessions() and killSessionsByPersonID
 --->


<cfcomponent displayname="sessionManagement" hint="Used for accessing user sessions">

	<cffunction access="public" name="getApplications" return="array">

		<cfset var appName = ArrayNew (1)>
		<cfset var appObj = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
		<cfset var apps = appObj.getApplicationKeys()>

		<cfloop condition="#apps.hasMoreElements()#">

			<cfset arrayAppend(appName,apps.nextElement())>
		</cfloop>

		<cfreturn appName>

	</cffunction >


	<cffunction access="public" name="getApplicationScope" return="struct">
		<cfargument name="applicationName" type="string" required="yes">

		<cfset var appObj = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
		<cfreturn appObj.getApplicationScope(applicationName)>

	</cffunction >

	<cffunction access="public" name="getSessions" return="struct">
		<cfargument name="applicationName" type="string" required="yes">

		<cfset var tracker = createObject("java", "coldfusion.runtime.SessionTracker")>
		<cfset var sessions = tracker.getSessionCollection(applicationName)>

		<cfreturn sessions>

	</cffunction >

	<cffunction access="public" name="getASession" return="struct">
		<cfargument name="applicationName" type="string" required="yes">
		<cfargument name="SessionID" type="string" required="yes">
		<cfset var result = structNew()>

		<cfset var tracker = createObject("java", "coldfusion.runtime.SessionTracker")>
		<cfset var sessions = tracker.getSessionCollection(applicationName)>

		<cfif sessionid does not contain applicationname> <!--- in this structure sessions are keyed by applicationname_sessionid --->
			<cfset arguments.sessionid = applicationname & "_" & sessionid>
		</cfif>

		<cfset result = sessions[SessionID]>

		<cfreturn result>

	</cffunction >

	<!--- WAB 2009/08/25 added cluster awareness--->
	<cffunction access="public" name="getListOfSessionIDs" return="string">
		<cfargument name="applicationName" type="string" default="#application.applicationname#">
		<cfargument name="includeCluster" type="boolean" default="false">
		<cfargument name="applicationScope" default="#application#">

		<cfset var sessionList = "">
		<cfset var clusterresult = "">
		<cfset var instance = "">
		<cfset var key = "">

		<cfset var sessions = getSessions(applicationname)>

<!---
		2009/10/26
		WAB   This loop was very slow if there were lots of sessions (3000+)
		Replaced with something quicker
		<cfloop collection = #sessions# item = "key">
			<cftry>
			<cfset sessionList = listappend(sessionList,sessions[key].sessionid)>
				<cfcatch>
					<!--- WAB: 2007-04-09
						sometimes it seems as if the sessions have expired in the time between getting the keys and accessing the collection and it throws an error here
						might as well just ignore it and carry on.
						 --->
				</cfcatch>
			</cftry>
		</cfloop>
 --->

<!---
		2009/10/26	WAB
		The keys to the sessionStructure are the sessionids prefixed with #applicationname#_
		Therefore can get the list much quicker by just doing a replace
--->

	 <cfset sessionList = replaceNoCase(structKeyList(sessions),"#applicationName#_","","ALL")>



		<!--- WAB 2009/08/25 added cluster awareness--->
		<cfif includeCluster>

			<cfset clusterResult = applicationScope.com.clustermanagement.runFunctionOnCluster(functionName = "getListOfSessionIDs",includeCurrentServer = false,applicationScope=applicationScope)>
			<cfloop item = "instance"  collection = #clusterresult#>
				<cfif clusterResult[instance].isOK>
					<cfset sessionlist = listappend (sessionList,clusterResult[instance].sessionids)>
				</cfif>

			</cfloop>

		</cfif>

		<cfreturn sessionList>

	</cffunction >


	<cffunction access="public" name="deleteASession" >
		<cfargument name="applicationName" type="string" required="yes">
		<cfargument name="SessionID" type="string" required="yes">

		<cfset var tracker = createObject("java", "coldfusion.runtime.SessionTracker")>
		<cfset var sessions = tracker.getSessionCollection(applicationName)>
		<cfset sessions[sessionid].setMaxInactiveInterval (0)>   <!--- TRYING HARD TO GET RID OF SESSION! --->

		<cfreturn structDelete (sessions,sessionid,true)>

	</cffunction >


	<cffunction access="public" name="getSessionsWithKeyValue" return="struct">
		<cfargument name="keyPath" type="string" required="yes">
		<cfargument name="keyValue" type="string" required="yes">

		<cfset var returnsessions = structNew()>

		<cfset var keyName = listlast(keyPath,".")>
		<cfset var path = left(keypath,len(keypath) - len(keyname)-1)>
		<cfset path = "thisSession." & path>



			<cfset var applications = application.com.sessionManagement.getApplications()>

			<cfloop index = "local.I" from= "1" to = "#arraylen(applications)#">

				<cfset var appname = applications[local.I]>
				<cfset var sessions = application.com.sessionManagement.getSessions(appname) >

				<cfloop collection = #sessions# item = "local.thiskey">
					<cfset var thisSession = sessions[local.thisKey]>

					<cfif isDefined("#path#.#keyname#")>
						<cfset var x = structget(path)>
						<cfif x[keyname] is keyvalue>
							<cfset returnsessions[local.thisKey] = thisSession>
						</cfif>
					</cfif>
				</cfloop>

			</cfloop>

		<cfreturn returnSessions>

	</cffunction>



	<cffunction access="public" name="getSessionsOpenedByCurrentUser" return="struct">

		<cfreturn getSessionsWithKeyValue("relayCurrentUserStructure.sessionOpenedBy","#application.ApplicationName#_#session.sessionid#")>

	</cffunction >




	<cffunction access="public" name = "setSessionVariable" >
		<cfargument name = "applicationname" type="string" required = "yes">
		<cfargument name = "sessionid" type="string" required = "yes">
		<cfargument name = "variableName" type="string" required = "yes">
		<cfargument name = "variableValue"  type="string" required = "yes">


		<cfset var sessions = getSessions(applicationName)>

		<cfset "sessions.#sessionid#.variableName" = variableValue>

	</cffunction>


	<cffunction access="public" name = "killCurrentSession" >
		<cfargument name = "time" type="numeric" default="1">
			<!--- 			<cfset getPageContext().getSession().invalidate(time)> --->
			<!--- 	<cfset StructClear(Session)> --->
			<!---  could try session.setMaxInactiveInterval(1 [seconds]) --->
			<!--- can use getPageContext().getSession().invalidate(), but doesn't actually clear the memory --->
			<cfset session.setMaxInactiveInterval (time) >
			<cfset application.com.globalFunctions.cfcookie(NAME="jsessionid", VALUE="0", EXPIRES=0)>

	</cffunction>


	<cffunction access="public" name = "getActiveSessions" >

		<cfquery name="local.getSessions" datasource="#application.sitedatasource#">
		select firstname + ' '+ lastname as name, isInternal, v.jsessionid,site,visitstarteddate,
			cf.instancename,
			case when v.jsessionid =  <cf_queryparam value="#session.sessionid#" CFSQLTYPE="CF_SQL_VARCHAR" >  then '*' else '' end as currentSession,
			case when v.metadata  like  <cf_queryparam value="%openedby=#session.sessionid#%" CFSQLTYPE="CF_SQL_VARCHAR" >  then '*' else '' end as openedByThisSession

		from
			visit v
				inner join
			person p on p.personid = v.personid
				inner join
			sitedef sd  on sd.sitedefid = v.sitedefid
				inner join
			coldfusioninstance_autopopulated cf	on cf.id = v.coldfusioninstanceid
		where
				v.sessionendeddate is null
			and v.coldfusioninstanceid is not null
			and lastrequestdate > getdate() - .2
		order by isInternal,site

		</cfquery>

		<cfreturn local.getSessions>

	</cffunction>


	<!--- WAB 	2016-10-40	PROD2016-2419 Added killListOfSessions() and killSessionsByPersonID, to be used when user changes their password and we wish to invalidate any other sessions --->
	<cffunction access="public" name = "killListOfSessions" >
		<cfargument name = "SessionIDs" type="string" required="true">
		<cfargument name="includeCluster" type="boolean" default="false">

		<cfset var listOfActiveSessionIDs = getListOfSessionIDs (includeCluster = false)>
		
		<cfset var sessionsDeleted = "">

		<cfloop index="sessionID" list = "#sessionIDs#">	
			<cfif listFind (listOfActiveSessionIDs, SessionID)>
				
				<cfset var theSession = getASession (applicationName = application.applicationname, sessionID = SessionID)>			

				<cfif structKeyExists (theSession, "RelayCurrentUserStructure")>
					<cfset theSession.RelayCurrentUserStructure.isLoggedIn = 0>			
				</cfif>

				<cfset theSession.setMaxInactiveInterval (1) >
				<cfset sessionsDeleted = listAppend (sessionsDeleted,sessionID)>
				
			</cfif>	
		</cfloop>


		<cfif includeCluster>

			<cfset clusterResult = application.com.clustermanagement.runFunctionOnCluster(functionName = "killListOfSessions", includeCurrentServer = false, sessionIDs = sessionIDs)>

			<cfloop item = "instance"  collection = #clusterresult#>
				<cfif clusterResult[instance].isOK>
					<cfset sessionsDeleted = listappend (sessionsdeleted,clusterResult[instance].sessionsdeleted)>
				</cfif>
			</cfloop>

		</cfif>

		<cfreturn sessionsDeleted>
	</cffunction>


	<cffunction access="public" name = "killSessionsByPersonID">
		<cfargument name = "personID" type="numeric" required="true">
		<cfargument name = "excludeSessionIDs" type="string" default="">

		<cfset var allExistingSessionIDsList = application.com.sessionManagement.getListOfSessionIDs(includeCluster = true)>
		<cfset var sessionsDeleted = "">

		<cfquery name = "local.SessionsToKill">
		select 
			distinct jsessionid, personid
		from 
			visit 
		where 
			jsessionid in (<cf_queryparam value=#allExistingSessionIDsList# cfsqltype="cf_sql_varchar" list="true">)
			and personid = <cf_queryparam value=#arguments.personid# cfsqltype="cf_sql_varchar" list="true">
			<cfif excludeSessionIDs is not "">
			and jsessionid not in ( <cf_queryparam value=#excludeSessionIDs# cfsqltype="cf_sql_varchar" list="true"> ) 
			</cfif>
		</cfquery>


		<cfif local.SessionsToKill.recordCount is not 0>
			<cfset sessionsDeleted = killListOfSessions (SessionIDs = valueList (local.SessionsToKill.jsessionID), includeCluster = true)>
		</cfif>

		<cfreturn sessionsDeleted>
	
	</cffunction>


</cfcomponent>

