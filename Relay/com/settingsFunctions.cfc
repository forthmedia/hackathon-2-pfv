<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		settingsFunctions.cfc
Author:			NJH
Date started:	03-02-2015

Description:	Functions to be run on setting changes

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	2015/02/03		NJH			Moved unique email validation here.

Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="onSavePartnerPacks_Marketing" access="public" output="false">
		<cfset savePartnerPacks(partnerPack="marketing",argumentCollection=arguments)>
	</cffunction>

	<cffunction name="onSavePartnerPacks_Training" access="public" output="false">
		<cfset savePartnerPacks(partnerPack="training",argumentCollection=arguments)>
	</cffunction>

	<cffunction name="onSavePartnerPacks_Sales" access="public" output="false">
		<cfset savePartnerPacks(partnerPack="sales",argumentCollection=arguments)>
	</cffunction>

	<cffunction name="onSavePartnerPacks_Connector" access="public" output="false">
		<cfset savePartnerPacks(partnerPack="connector",argumentCollection=arguments)>
	</cffunction>


	<cffunction name="savePartnerPacks" access="private" output="false">
		<cfargument name="partnerPack" type="string" required="true">
		<cfargument name="variableValue" type="string" required="true">

		<!--- partner packs setting --->
		<cfset var setPartnerPackModulesOnOff = "">
		<cfset var setPartnerPackElementsOnOff = "">
		<cfset var setPartnerPackScreensOnOff = "">
		<cfset var partnerPackStruct = {marketing="communicate,incentive,funds",sales="leadManager",training="elearning,events",connector="connector"}>

		<cfif listFindNoCase(structKeyList(partnerPackStruct),arguments.partnerPack)>
			<!--- <cfif structKeyExists(form,"partnerPacks.#arguments.partnerPack#") and structKeyExists(form,"partnerPacks.#arguments.partnerPack#_orig")> --->

				<cfif listFindNoCase("marketing,sales",partnerPack)>
					<cfif application.com.settings.getSetting("partnerPacks.#arguments.partnerPack#") or (not application.com.settings.getSetting("partnerPacks.marketing") and not application.com.settings.getSetting("partnerPacks.sales"))>
						<cfset partnerPackStruct[arguments.partnerPack] = listAppend(partnerPackStruct[arguments.partnerPack],"products")>
					</cfif>
				</cfif>

				<!--- <cfif (form["partnerPacks.#arguments.partnerPack#"] neq form["partnerPacks.#arguments.partnerPack#_orig"])> --->
					<cfquery name="setPartnerPackModulesOnOff">
						update relayModule
							set active = <cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_bit">
						where moduleTextID in (<cf_queryparam value="#partnerPackStruct[arguments.partnerPack]#" cfsqltype="cf_sql_varchar" list="true">)
							and active != <cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_bit">
					</cfquery>

					<!--- reload the relayTag structure so that any relay tags associated with the modules are shown/hidden as required --->
					<cfset application.com.applicationVariables.updateRelayTagStructure()>

					<!--- 2014-10-30	RPW	CORE-912 If I turn the partner packs off in [test.future] the pages are set to draft and not re-instated when I turn the partner packs back on. --->
					<cfset elementStatus = IIF(arguments.variableValue,4,0)>

					<cfquery name="setPartnerPackElementsOnOff">
						update element
							set statusID = <cf_queryparam value="#elementStatus#" cfsqltype="cf_sql_integer">
						where parameters like '%partnerPack=#arguments.partnerPack#%'
							and statusID != <cf_queryparam value="#elementStatus#" cfsqltype="cf_sql_integer">
					</cfquery>

					<!--- need to blow the elementTreeCache if the status changes --->
					<!--- 2014-10-30	RPW	CORE-912 If I turn the partner packs off in [test.future] the pages are set to draft and not re-instated when I turn the partner packs back on. --->
					<cfset application.com.relayElementTree.incrementCachedContentVersionNumber(updateCluster = true)>
					<cfset application.com.relayElementTree.cachedContentNeedsToBeBlown()>

					<cfquery name="setPartnerPackScreensOnOff">
						update screens
							set active = <cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_bit">
						where parameters like '%partnerPack=#arguments.partnerPack#%'
							and active != <cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_bit">
					</cfquery>
				<!--- </cfif>
			</cfif> --->
		</cfif>

	</cffunction>


	<cffunction name="onSaveConnector_Salesforce_ApiUser_Username" access="public" output="false">
		<cfargument name="variableValue" type="string" required="true">

		<!--- if for some reason the username is updated on it's own, then test new connection details. Otherwise, it needs to be done in conjunction with the password changes --->
		<cfif not structKeyExists(form,"CONNECTOR.SALESFORCE.APIUSER.PASSWORD") or form["CONNECTOR.SALESFORCE.APIUSER.PASSWORD"] eq "***Password***">
			<cfset var sfPassword = application.com.encryption.decryptWithMasterKey(application.com.settings.getMainSettingValueFromDataBase("connector.salesforce.apiUser.password").value)>
			<cfset testSalesforceLoginCredentials(username=arguments.variableValue,password=sfPassword)>
		</cfif>
	</cffunction>


	<cffunction name="onSaveConnector_Salesforce_ApiUser_Password" access="public" output="false">
		<cfargument name="variableValue" type="string" required="true">

		<cfset testSalesforceLoginCredentials(username=application.com.settings.getMainSettingValueFromDataBase("connector.salesforce.apiUser.username").value,password=application.com.encryption.decryptWithMasterKey(arguments.variableValue))>
	</cffunction>


	<cffunction name="testSalesforceLoginCredentials" output="false" access="private">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">

		<cfset var oldSessionDetails = {username="",password=""}>
		<cfif structKeyExists(application.connector,"api")>
			<cfset oldSessionDetails = duplicate(application.connector.api)>
		</cfif>
		<cfset var connectorApiObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorApiObject(connectorType="Salesforce")>

		<cfif not connectorApiObject.areLoginCredentialsValid(username=arguments.username,password=arguments.password)>
			<cfset application.connector.api = oldSessionDetails>
			<cfthrow message="Could not connect to Salesforce with user credentials">
		</cfif>

	</cffunction>


	<cffunction name="onSavePLO_uniqueEmailValidation" access="public" output="false">
		<cfargument name="variableValue" type="boolean" required="true" hint="The value of uniqueEmailValidation">

		<cfset var turnValidationOnOff = "">

		<cfif structKeyExists(form,"plo.uniqueEmailValidation") and structKeyExists(form,"plo.uniqueEmailValidation_orig")>
			<cfif (form.plo.uniqueEmailValidation neq form.plo.uniqueEmailValidation_orig)>

				<cfquery name="turnValidationOnOff">
					declare @sql varchar(250)

					<!--- 2015/01/29	YMA	Case 443670 settingXMLNode is no longer available to this file so get the value of the setting from the form instead. --->
					<cfif arguments.variableValue>
						update modentitydef
							set cfformparameters = 'onValidate=validateEmail,uniqueEmail=true,uniqueEmailMessage=phr_sys_formValidation_UniqueEmailRequired'
						where tablename = 'person' and fieldname = 'email'

						update screendefinition
							set jsverify = <cf_queryparam value='"verifyEmail(form.##thisLineFormField##,''phr_sys_formValidation_ValidEmailAddress'')"' CFSQLTYPE="CF_SQL_VARCHAR" >
						where fieldtextid = 'email' and fieldsource = 'person'

						set @sql = 'ENABLE TRIGGER person_uniqueEmailTrig on person'
					<cfelse>
						update modentitydef
							set cfformparameters = null
						where tablename = 'person' and fieldname = 'email'

						update screendefinition
							set jsverify = null
						where fieldtextid = 'email' and fieldsource = 'person'

						set @sql = 'DISABLE TRIGGER person_uniqueEmailTrig on person'
					</cfif>

					exec(@sql)
				</cfquery>
			</cfif>
		</cfif>

	</cffunction>

	<!--- 	WAB 2016-04-22 During CASE 449163 (case sensitivity of mime type checks)
			Warn if any file extensions which are added to the AllowedFileExtensions setting aren't supported by IIS
			Just raise a UI message - seems to work OK.
	--->
	<cffunction name="onSaveFiles_AllowedFileExtensions" access="public" output="false">

		<cfset var noMimeTypeList = "">
		<cfloop index="extension" list="#arguments.variableValue#">
			<cfif not ArrayLen(application.com.iisAdmin.getMimeType (extension))>
				<cfset noMimeTypeList = listAppend(noMimeTypeList, extension)>
			</cfif>
		</cfloop>

		<cfif noMimeTypeList is not "">
			<cfset application.com.relayUI.setMessage (Message = "There is no IIS Mime Type set for these file types: #noMimeTypeList#.<BR>It will not be possible to view these files on the system ", type="warning")>
		</cfif>


	</cffunction>


	<cffunction name="onSaveConnector_Type" access="public" output="false">
		<cfargument name="variableValue" type="string" required="true">

		<cfquery name="updateConnectorPersonAndUserGroup">
			update person set firstname=<cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_varchar"> where firstname=<cf_queryparam value="#form['connector.type_orig']#" cfsqltype="cf_sql_varchar"> and lastname='API'

			update usergroup set name = replace(name,<cf_queryparam value="#form['connector.type_orig']#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_varchar">)
			where name like <cf_queryparam value="#form['connector.type_orig']#%" cfsqltype="cf_sql_varchar">
				and (name like '% API' or name like '%ProxyUser')
		</cfquery>
	</cffunction>


	<!--- NJH 2016/09/06 - JIRA PROD2016-2232 - when unpausing connector, set it going --->
	<cffunction name="onSaveConnector_ScheduledTask_Pause" access="public" output="false">
		<cfargument name="variableValue" type="string" required="true">

		<cfif not arguments.variableValue>
			<cfset application.getObject("connector.com.connectorUtilities").scheduleConnectorSynch(scheduleTaskPaused=false,reset=true)>
		</cfif>
	</cffunction>


	<!--- NJH 2016/08/03 JIRA PROD2016-351 - update the primary contact flag based on location-aware setting	--->
	<cffunction name="onSavePLO_useLocationAsPrimaryPartnerAccount" access="public" output="false">

		<cfset var setPrimaryContactFlag = "">

		<cfquery name="setPrimaryContactFlag">
			update flag set active=<cf_queryparam value="#arguments.variableValue#" cfsqltype="cf_sql_bit"> where flagTextId='PrimaryContacts'
			update flag set active=<cf_queryparam value="#not(arguments.variableValue)#" cfsqltype="cf_sql_bit"> where flagTextId='KeyContactsPrimary'

			<cfif arguments.variableValue>
			update userGroup set conditionSQL = replace(conditionSql,'person.IS_KeyContactsPrimary = 1','person.IS_PrimaryContacts = 1') where name='Primary Contact'
			<cfelse>
			update userGroup set conditionSQL = replace(conditionSql,'person.IS_PrimaryContacts = 1','person.IS_KeyContactsPrimary = 1') where name='Primary Contact'
			</cfif>
		</cfquery>

		<cfset application.com.flag.updateFlagStructure(flagId="PrimaryContacts")>
		<cfset application.com.flag.updateFlagStructure(flagId="KeyContactsPrimary")>

		<cfset application.com.relayUI.setMessage(message="Please ensure that all screens,workflows and usergroup conditions that deal with the primary contact are amended accordingly when changing the useLocationAsPrimaryPartnerAccount setting",type="warning")>

	</cffunction>


	<!--- NJH 2017/02/13 JIRA PROD2016-3417 - when matching fields change, add them to the connector data tables and recreate the views as the views are used for matching. --->
	<cffunction name="createMatchFieldsOnConnectorDataTables" access="private" output="false">
		<cfargument name="entityType" type="string" required="true">

		<cfset var rwObject = arguments.entityType>

		<cfif application.com.relayMenu.isModuleActive("connector")>
			<!--- if we've changed the organisation matching columns, then we need to regenerate the view for the location object if we're not mapping organisations --->
			<cfif arguments.entityType eq "organisation" and not application.getObject("connector.com.connector").isObjectMapped(object_relayware=arguments.entityType)>
				<cfset rwObject = "location">
			</cfif>

			<cfset var connectorObjQry = application.getObject("connector.com.connector").getPrimaryConnectorObjects(object=rwObject)>
			<cfif connectorObjQry.recordCount and connectorObjQry.import[1]>
				<cfset application.getObject("connector.com.connectorMapping").recreateConnectorViews(object_relayware=rwObject,direction="import")>
			</cfif>
		</cfif>

	</cffunction>


	<cffunction name="onSaveMatching_MatchFields_Person" access="public" output="false">
		<cfset createMatchFieldsOnConnectorDataTables("person")>
	</cffunction>


	<cffunction name="onSaveMatching_MatchFields_Location" access="public" output="false">
		<cfset createMatchFieldsOnConnectorDataTables("location")>
	</cffunction>


	<cffunction name="onSaveMatching_MatchFields_Organisation" access="public" output="false">
		<cfset createMatchFieldsOnConnectorDataTables("organisation")>
	</cffunction>

</cfcomponent>