<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2008/06/30     	SSS     Added Nvarchar Support
2010/10/08		NAS		LID4280: Actions report is not showing the correct time.

--->

<cfcomponent displayname="relayActions" hint="A set of functions and methods for Relay Action management.">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addAction" hint="Adds an action to the database">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="entityID" type="numeric" required="yes"> <!--- which entity the action is related to --->
		<cfargument name="actionStatusID" type="numeric" default="2">
		<cfargument name="entityTypeID" type="numeric" required="yes">
		<cfargument name="deadline" type="numeric">
		<cfargument name="notes" type="string">
		<cfargument name="description" type="string" default="none">
		<cfargument name="priority" type="numeric">
		<cfargument name="actionTypeID" type="numeric">
		<cfargument name="ownerPersonID" type="numeric">
		<cfargument name="FreezeDate" type="date" default="#Now()#">
		<cfargument name="checkMultipleActions" type="string" default="no">
		
		<cfscript>
			var insertAction = "";
		</cfscript>
		
		<cfquery name="insertAction" datasource="#application.SiteDataSource#">
			INSERT INTO actions(ownerPersonID, CreatedBy, Description,actionStatusID, 
				created, Deadline, Notes, priority,
				actionTypeID,entityID,entityTypeID)
			VALUES(<cf_queryparam value="#arguments.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.actionStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#arguments.deadline#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#arguments.notes#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.priority#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.actionTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >)	
		</cfquery>
		
		<CFIF arguments.ownerPersonID neq request.relayCurrentUser.personid>
			<!--- email owner of new action asigned to him/her --->
			<cfset actionID = thisActionID>
			<cfinclude template="/actions/ActionEmail.cfm">	
		</CFIF>
		
		<cfreturn functionname>
	</cffunction>
	
	<cffunction access="public" name="getTopics" hint="gets topics for action notes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfquery name="topics" datasource="#arguments.datasource#">
			select		distinct topic
			from		actionnote
			order by	topic
		</cfquery>
		
		<cfreturn topics>
	</cffunction>
	
	<cffunction access="public" name="getInitiators" hint="gets initiators for action notes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfquery name="initiators" datasource="#arguments.datasource#">
			select		distinct initiator
			from		actionnote
			order by	initiator
		</cfquery>
		
		<cfreturn initiators>
	</cffunction>
	
	<cffunction access="public" name="getTypes" hint="gets types for action notes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		
		<cfquery name="types" datasource="#arguments.datasource#">
			select		distinct type
			from		actionnote
			order by	type
		</cfquery>
		
		<cfreturn types>
	</cffunction>
	
	<cffunction access="public" name="getActionNoteDetail" hint="gets action note and/or action details">s
		<cfargument name="actionNoteID" default="">
		<cfargument name="actionID" default="">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		
		<cfset var actionNote = "">
		
		<cfif arguments.actionNoteID neq "">
			<cfquery name="actionNote" datasource="#arguments.datasource#">
				select	an.*, a.description
				from	actionnote an, actions a
				where	an.actionNoteID =  <cf_queryparam value="#arguments.actionNoteID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
						a.actionid = an.actionid
			</cfquery>
		<cfelseif arguments.actionid neq "">
			<cfquery name="action" datasource="#arguments.datasource#">
				select	actionid, description
				from	actions
				where	actionid =  <cf_queryparam value="#arguments.actionid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			<cfscript>
				actionNote = queryNew("actionid,description,summary,noteText,timeTakenI,calls,emails,dateStart,dateEnd,topic,initiator,type");
				queryAddRow(actionNote,1);
				if (action.recordcount gt 0) {
					querySetCell(actionNote,"actionid",action.actionid);
					querySetCell(actionNote,"description",action.description);
				} else {
					querySetCell(actionNote,"actionid",0);
					querySetCell(actionNote,"description","");
				}
				querySetCell(actionNote,"summary","");
				querySetCell(actionNote,"noteText","");
				querySetCell(actionNote,"timeTakenI","");
				querySetCell(actionNote,"calls","");
				querySetCell(actionNote,"emails","");
				querySetCell(actionNote,"dateStart","");
				querySetCell(actionNote,"dateEnd","");
				querySetCell(actionNote,"topic","");
				querySetCell(actionNote,"initiator","");
				querySetCell(actionNote,"type","");
			</cfscript>
		</cfif>
		
		<cfreturn actionNote>
	</cffunction>
	
	<cffunction access="public" name="updateActionNote" hint="update action note details">
		
		<cfargument name="formObject">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		
		<!--- <cfscript>
			formObject.summary = replacenocase(formObject.summary,"'","''","all");
			formObject.noteText = replacenocase(formObject.noteText,"'","''","all");
			formObject.topic = replacenocase(formObject.topic,"'","''","all");
			formObject.initiator = replacenocase(formObject.initiator,"'","''","all");
			formObject.type = replacenocase(formObject.type,"'","''","all");
		</cfscript> --->
		
		<cfquery name="updateActionNote" datasource="#arguments.datasource#">
			update	actionnote
			set		
					lastUpdated = #createODBCDateTime(now())#,
					summary = <cf_queryparam value="#formObject.summary#" cfsqltype="cf_sql_varchar">,
					<!--- NJH 2008/10/27 Bug Fix All Site Issue 1206 - changed to queryparam as am allowing this field through the sql injection checker --->
					noteText = <cf_queryparam value="#formObject.noteText#" cfsqltype="cf_sql_varchar">,
					timeTakenI = <cfif formObject.timeTakenI eq "">0<cfelse>#formObject.timeTakenI#</cfif>,
					calls = <cfif formObject.calls eq "">0<cfelse>#formObject.calls#</cfif>,
					emails = <cfif formObject.emails eq "">0<cfelse>#formObject.emails#</cfif>,
					<!--- START 2010/10/08		NAS		LID4280: Actions report is not showing the correct time. --->
					<cfif formObject.dateStart neq "">
						<cfset StartTime = '#createODBCTime(Now())#'>
						<cfset NoteDateStarted=(DateFormat(formObject.dateStart) &' '& timeformat(StartTime))>
						dateStart = #createODBCDateTime(NoteDateStarted)#,
					</cfif>
					<cfif formObject.dateEnd neq "">
						<!--- 2013-03-08 - RMB - 434115 - Added NEW 'NewEndTime' and Changed 'EndTime' to use 'NewEndTime' --->
						<cfset NewEndTime = DateAdd("n", formObject.timeTakenI, now())>
						<cfset EndTime = CreateTime(TimeFormat(NewEndTime, "HH"), TimeFormat(NewEndTime, "mm"), TimeFormat(NewEndTime, "ss"))>
						<!--- <cfset EndTime = CreateTime(TimeFormat(now(), "HH"), evaluate(TimeFormat(now(), "mm") + formObject.timeTakenI), TimeFormat(now(), "ss"))> --->
						<cfset NoteDateEnded=(DateFormat(formObject.dateEnd) &' '& timeformat(EndTime))>
						dateEnd = #createODBCDateTime(NoteDateEnded)#,
					</cfif>
					<!--- STOP 2010/10/08		NAS		LID4280: Actions report is not showing the correct time. --->
					initiator = <cf_queryparam value="#formObject.initiator#" cfsqltype="cf_sql_varchar">,
					type = <cf_queryparam value="#formObject.type#" cfsqltype="cf_sql_varchar">,
					topic = <cf_queryparam value="#formObject.topic#" cfsqltype="cf_sql_varchar">
					
			where	actionNoteID =  <cf_queryparam value="#formObject.actionNoteID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
	</cffunction>
	
	<cffunction access="public" name="createActionNote" hint="creates action note">
		
		<cfargument name="formObject">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		
		<cfset creationTime = now()>
		
		<cfif formObject.timeTakenI eq "">
			<cfset formObject.timeTakenI = 0>
		</cfif>
		
		<cfquery name="updateActionNote" datasource="#arguments.datasource#">
			insert into	actionnote (
									actionid,
									creatorid,
									created,
									lastUpdated,
									newowner,
									newstatus,
									summary,
									noteText,
									timeTakenI,
									calls,
									emails,
									dateStart,
									<cfif formObject.dateEnd neq "">dateEnd,</cfif>
									initiator,
									type,
									topic
									)
			values		(
						<cf_queryparam value="#formObject.actionID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#request.relaycurrentuser.usergroupid#" CFSQLTYPE="CF_SQL_Integer" >,
						<cf_queryparam value="#createODBCDateTime(creationTime)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#createODBCDateTime(creationTime)#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="CF_SQL_Integer" >,
						5,
						<cf_queryparam value="#formObject.summary#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<!--- NJH 2008/10/27 Bug Fix All Site Issue 1206 - changed to queryparam as am allowing this field through the sql injection checker --->
						<cfqueryparam value="#formObject.noteText#" cfsqltype="cf_sql_varchar">,
						--N'#formObject.noteText#',
						<cf_queryparam value="#formObject.timeTakenI#" CFSQLTYPE="CF_SQL_float" >,
						<cfif formObject.calls eq "">0<cfelse><cf_queryparam value="#formObject.calls#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
						<cfif formObject.emails eq "">0<cfelse><cf_queryparam value="#formObject.emails#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
						<!--- START 2010/10/08		NAS		LID4280: Actions report is not showing the correct time. --->
						<cfif formObject.dateStart neq "">
							<cfset StartTime = '#createODBCTime(Now())#'>
							<cfset NoteDateStarted=(DateFormat(formObject.dateStart) &' '& timeformat(StartTime))>
							<cf_queryparam value="#createODBCDateTime(NoteDateStarted)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						</cfif>
						<cfif formObject.dateEnd neq "">
							<!--- 2013-03-08 - RMB - 434115 - Added NEW 'NewEndTime' and Changed 'EndTime' to use 'NewEndTime' --->
							<cfset NewEndTime = DateAdd("n", formObject.timeTakenI, now())>
							<cfset EndTime = CreateTime(TimeFormat(NewEndTime, "HH"), TimeFormat(NewEndTime, "mm"), TimeFormat(NewEndTime, "ss"))>
							<!--- <cfset EndTime = CreateTime(TimeFormat(now(), "HH"), evaluate(TimeFormat(now(), "mm") + formObject.timeTakenI), TimeFormat(now(), "ss"))> --->
							<cfset NoteDateEnded=(DateFormat(formObject.dateEnd) &' '& timeformat(EndTime))>
							<cf_queryparam value="#createODBCDateTime(NoteDateEnded)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						</cfif>
						<!--- STOP 2010/10/08		NAS		LID4280: Actions report is not showing the correct time. --->
						<cf_queryparam value="#formObject.initiator#" CFSQLTYPE="CF_SQL_VARCHAR" >,						
						<cf_queryparam value="#formObject.type#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#formObject.topic#" CFSQLTYPE="CF_SQL_VARCHAR" >
						)
			select		scope_identity() as newActionNoteID
		</cfquery>
		
		<cfreturn updateActionNote.newActionNoteID>
	</cffunction>

</cfcomponent>

