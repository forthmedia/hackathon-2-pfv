<!--- �Relayware. All Rights Reserved 2016 --->
<!---
	2016/01/06	JIRA BF-171  - show inactive training paths
	2016/01/14	WAB JIRA BF-268	 - but don't show inactive training paths to users
	2016-05-12	WAB During PROD2016-778 correct spelling of getRecordRightsFilterQuerySnippet()  (which was missing the T in get!)
--->

<cfcomponent accessors="false" output="false" persistent="false">

	<!--- 17/04/2014 YMA Lenovo CR 434305 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="getTrainingPathList" access="public" returnType="query">
		<cfargument name="sortOrder" type="string" required="false" default="TrainingPath">
		<cfargument name="restrictToUser" type="boolean" default="false">
		<cfargument name="personID" required="false" default="#request.relaycurrentuser.personID#">
		<cfargument name="showOnlyAssignedTrainingPath" default="false">
		<cfargument name="showOnlyUnAssignedTrainingPath" default="false">
		<cfargument name="returnCoursesAsCommaList" required="false" default="false" hint="Return a single row for courses?">
		<cfargument name="TrainingPathId" type="numeric" required="false">
		<cfargument name="showModules" type="boolean" default="false">
		<cfargument name="active" type="boolean" default="true">

		<cfset var qryTrainingPathList = "">
		<cfset var countryIDs = "">
		<cfset var userCountryAndGroups = "">
		<cfset var recordRights = {join="",whereClause=""}>
		<cfset var countryScope = {join="",whereClause=""}>

		<cfif arguments.restrictToUser>
			<cfset recordRights = application.com.rights.getRecordRightsFilterQuerySnippet(entityType="trngTrainingPath",alias="tp",personID=arguments.personID)>
			<cfset countryScope = application.com.rights.getCountryScopeFilterQuerySnippet(entityType="trngTrainingPath",alias="tp",personID=arguments.personID)>
		</cfif>

		<cfquery name="qryTrainingPathList">
			select * from (
				SELECT tp.TrainingPathTextID, tp.ID as TrainingPathID, tp.title_defaultTranslation as TrainingPath,
					tp.Created,tp.CreatedBy,tp.LastUpdated,tp.LastUpdatedBy, tp.ignoreAssignmentRules,
			<cfif arguments.showModules>
					m.module_code, m.moduletitle_defaulttranslation,m.passdecidedby,m.solution_area,m.fileID,m.quizdetailID,m.associated_quiz,
			</cfif>

			<cfif arguments.returnCoursesAsCommaList>
					STUFF((SELECT ', '+ cast(tc.title_defaultTranslation as nvarchar) as CourseTitle
						from  trngTrainingPathCourse tcc
							left outer join trngCourse tc on tcc.courseID = tc.courseID
						where  tcc.TrainingPathID  = tp.ID
						GROUP BY tc.title_defaultTranslation
					FOR XML PATH(''), TYPE).value('.','NVARCHAR(max)'), 1, 1, '') as CourseTitle
				from trngTrainingPath tp
			<cfelse>
					tc.courseID,
					tc.title_defaultTranslation as CourseTitle
				from trngTrainingPath tp
				left outer join trngTrainingPathCourse tpc on tp.ID = tpc.TrainingPathID <cfif structKeyExists(arguments,"TrainingPathId")>and tp.ID = <cf_queryparam value="#arguments.TrainingPathID#" cfsqltype="cf_sql_integer"></cfif>
				left outer join trngCourse tc on tpc.courseID = tc.courseID
			</cfif>

			<cfif arguments.showModules>
				left outer join vTrngModules as m on tc.courseID = m.courseID and m.active = 1
			</cfif>
			#preserveSingleQuotes(recordRights.join)#
			#preserveSingleQuotes(countryScope.join)#

			<cfif arguments.showOnlyAssignedTrainingPath or arguments.showOnlyUnAssignedTrainingPath>
			left Join trngTrainingPathPerson tpp on tp.ID = tpp.TrainingPathID and tpp.personID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="cf_sql_integer" >
			</cfif>
			where
				tp.active = <cf_queryparam value="#arguments.active#" cfsqltype="cf_sql_bit">
				#recordRights.whereClause#
				#countryScope.whereClause#
			<cfif  arguments.showOnlyUnAssignedTrainingPath>
				and tpp.trainingPathID is null
				and ignoreAssignmentRules != 1
			<cfelseif arguments.showOnlyAssignedTrainingPath>
				and (tpp.trainingPathID is not null	or ignoreAssignmentRules = 1)
			</cfif>

			) as trainingPathList where 1=1

			<cfif structKeyExists(arguments,"TrainingPathId")>
				and TrainingPathID = <cf_queryparam value="#arguments.TrainingPathID#" cfsqltype="cf_sql_integer">
			</cfif>

			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			Order By #arguments.sortorder#
		</cfquery>

		<cfreturn qryTrainingPathList>
	</cffunction>


	<!--- 	17/04/2014 YMA Lenovo CR 434305 Suggest a TrainingPath path for a user to follow. 
			2016/01/14	WAB JIRA BF-268	 don't show inactive training paths to users
			WAB 2016-06-22 PROD-778  Fix rights when entityType = organisation
	--->
	<cffunction name="getTrainingPathListForUsers" access="public" returnType="query">
		<cfargument name="sortOrder" type="string" required="true" default="TrainingPath">
		<cfargument name="showForPersonID" required="true" default="#request.relaycurrentuser.personID#"> <!--- current users rights. Who is viewing the data? --->
		<cfargument name="entityID" required="false" default="#request.relaycurrentuser.personID#"> <!--- get data for this entity.... org level or person level. The training contact will get all training paths for all people at their org--->
		<cfargument name="entityType" required="false" default="person"> <!--- the entityType tied to the entityId - only expecting POL - most times its 'person' but when viewing as training contact, it is organisation  --->
		<cfargument name="showOnlyAssignedTrainingPath" required="false" default="false">
		<cfargument name="showOnlyUnAssignedTrainingPath" required="false" default="false">
		<cfargument name="showOnlyActiveTrainingPath" required="false" default="true">
		<cfargument name="TrainingPathId" required="false">

		<cfset var qryTrainingPathList = queryNew("")>
		<cfset var recordRights = application.com.rights.getRecordRightsFilterQuerySnippet(entityType="trngTrainingPath",alias="tp",personID=arguments.showForPersonID)>
		<cfset var countryScope = application.com.rights.getCountryScopeFilterQuerySnippet(entityType="trngTrainingPath",alias="tp",personID=arguments.showForPersonID)>
		<cfset var getEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var titlePhraseQuerySnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingPath", phraseTextID = "title",baseTableAlias="tp")>

		<!--- if we're getting training paths for people at an organisation, then we need to filter out people that don't have access to these training paths --->
		<cfif arguments.entityType eq "organisation">
			<cfset var countryScopeForLocations = application.com.rights.getCountryScopeFilterQuerySnippet(entityType="trngTrainingPath",alias="tp",countryID="l.countryId")>
		</cfif>

		<cfif arguments.entityID neq "">
			<cfquery name="qryTrainingPathList">
				select * from
				(
					select tp.TrainingPathTextID,
							tp.ID as TrainingPathID,
							#titlePhraseQuerySnippet.select# as TrainingPath,
							tp.Created,tp.CreatedBy,
							tp.LastUpdated,tp.LastUpdatedBy,
							tp.ignoreAssignmentRules,
							p.organisationID,
							p.locationID,
							p.personId,
							p.fullname,
							tcp.ID as TrainingPathPersonID
					from trngTrainingPath tp
						#preserveSingleQuotes(titlePhraseQuerySnippet.join)#
						join person p on 1=1
						<cfif arguments.entityType eq "organisation">
							inner join location l on l.locationID = p.locationID
							#preserveSingleQuotes(countryScopeForLocations.join)#
						</cfif>
						#preserveSingleQuotes(recordRights.join)#
						#preserveSingleQuotes(countryScope.join)#
					<cfif not arguments.showOnlyAssignedTrainingPath>left outer<cfelse>inner</cfif> join trngTrainingPathPerson tcp on tp.ID= tcp.TrainingPathID and tcp.personID = p.personID
									<!--- and (
										(tce.entitytypeID = 0 and tce.entityID = p.personId)
											or
										(tce.entitytypeID = 1 and tce.entityID = p.locationId)
											or
										(tce.entitytypeID = 2 and tce.entityID = p.organisationId)
									) --->
					where
						1 = 1
						<cfif showOnlyActiveTrainingPath>
						and tp.active = 1
						</cfif>
						and p.#getEntityType.uniqueKey# in (<cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer" list="true">)
						#preserveSingleQuotes(countryScope.whereClause)#
						#preserveSingleQuotes(recordRights.whereClause)#
						<cfif arguments.entityType eq "organisation">
							<!--- WAB 2016-06-22 PROD-778  New visibility rights code was not compatible with the join + where clause that this query orignally had. 
									 Removed join and replaced where clause with a function which tests rights of a single person on a single record 
							--->
							and (tp.hasRecordRightsBitMask & power (2,1-1) = 0 OR dbo.checkRecordRights (#getEntityType.entityTypeID#,p.PersonID,tp.ID,1) = 1)
							#preserveSingleQuotes(countryScopeForLocations.whereClause)#
						</cfif>
					<cfif arguments.showOnlyUnAssignedTrainingPath>
						and tcp.ID is null
					</cfif>
					<cfif structKeyExists(arguments,"TrainingPathId")>
						and tp.ID= <cf_queryparam value="#arguments.TrainingPathId#" cfsqltype="cf_sql_integer">
					</cfif>
				) as qryTrainingPathList

				order By #arguments.sortorder#
			</cfquery>
		</cfif>

		<cfreturn qryTrainingPathList>
	</cffunction>


	<!--- 24/04/2014 YMA Lenovo CR 434305 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="addRemoveUsersToTrainingPath" access="public" returnType="struct">
		<cfargument name="addPersonIDs" required="true">
		<cfargument name="removePersonIDs" required="true">
		<cfargument name="TrainingPathID" type="numeric" required="true">

		<cfset var result1={isOK=true,message=""}>
		<cfset var result2={isOK=true,message=""}>
		<cfset var result={isOK=true,message="",TrainingPathID=arguments.TrainingPathID}>

		<!--- assign users --->
		<cfset result1 = assignUsersToTrainingPath(personIDs = arguments.addPersonIDs, TrainingPathID = arguments.TrainingPathID)>

		<!--- unAssign users --->
		<cfset result2 = unAssignUsersToTrainingPath(personIDs = arguments.removePersonIDs, TrainingPathID = arguments.TrainingPathID)>

		<cfset result.message = result1.message & ' ' & result2.message>

		<cfif not result1.isOK or not result2.isOK>
			<cfset result.isOK = false>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- 24/04/2014 YMA Lenovo CR 434305 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="unAssignUsersToTrainingPath" access="public" returnType="struct">
		<cfargument name="personIDs" required="true">
		<cfargument name="TrainingPathID" type="numeric" required="true">

		<cfset var result = {isOk=true,message="phr_elearning_TrainingPath_noPeople phr_elearning_TrainingPath_unAssignToTheTrainingPath"}>
		<cfset var usersAvailableToUnAssign = getTrainingPathListForUsers(TrainingPathID=arguments.TrainingPathID,entityID=arguments.personIDs,showOnlyAssignedTrainingPath=true)>
		<cfset var peopleRemoved = "">

		<cfif usersAvailableToUnAssign.recordcount gt 0>
			<cftry>

				<!--- <cfquery name="deleteTrainingPathEntity">
					delete from trngTrainingPathEntity
					where TrainingPathID = #arguments.TrainingPathID#
					and entityID in (<cf_queryparam value="#valuelist(usersAvailableToUnAssign.personID)#" cfsqltype="cf_sql_integer" list="true">)
					and entityTypeID = #application.entitytypeID["person"]#
				</cfquery> --->
				<cfset result = deleteTrainingPathPerson(personID=valuelist(usersAvailableToUnAssign.personID),trainingPathID=arguments.TrainingPathID)>

				<cfquery name="peopleRemoved">
					select fullname from person where personID in (<cf_queryparam value="#valuelist(usersAvailableToUnAssign.personID)#" cfsqltype="cf_sql_integer" list="true">)
				</cfquery>

				<cfset result.message = valuelist(peopleRemoved.fullname,", ") & " phr_elearning_TrainingPath_unAssignToTheTrainingPath">

				<cfcatch>
					<cfset result.isOk = false>
					<cfset result.message = cfcatch.message>
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- 24/04/2014 YMA Lenovo CR 434305 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="assignUsersToTrainingPath" access="public" returnType="struct">
		<cfargument name="personIDs" required="true">
		<cfargument name="TrainingPathID" type="numeric" required="true">

		<cfset var result = {isOk=true,message="phr_elearning_TrainingPath_noPeople phr_elearning_TrainingPath_assignToTheTrainingPath"}>
		<cfset var usersAvailableToAssign = getTrainingPathListForUsers(TrainingPathID=arguments.TrainingPathID,entityID=arguments.personIDs,showOnlyUnAssignedTrainingPath=true)>
		<cfset var peopleAdded = "">

		<cfif usersAvailableToAssign.recordcount gt 0>
			<cftry>
<!---
				<cfquery name="insertTrainingPathEntity" datasource="#application.sitedatasource#">
					insert into trngTrainingPathEntity
					select
					#arguments.TrainingPathID#,
					#application.entitytypeID['person']#,
					personID,
					getdate(),
					#request.relaycurrentuser.personID#,
					getdate(),
					#request.relaycurrentuser.personID#,
					<cfif structKeyExists(request.relaycurrentuser,"usergroupID")>#request.relaycurrentuser.usergroupID#<cfelse>null</cfif>
					from person where personID in (<cf_queryparam value="#valuelist(usersAvailableToAssign.personID)#" cfsqltype="cf_sql_integer" list="true">)
				</cfquery> --->
				<cfset result = insertTrainingPathPerson(personID=valuelist(usersAvailableToAssign.personID),trainingPathID=arguments.TrainingPathID)>

				<cfquery name="peopleAdded">
					select fullname from person where personID in (<cf_queryparam value="#valuelist(usersAvailableToAssign.personID)#" cfsqltype="cf_sql_integer" list="true">)
				</cfquery>

				<cfset result.message = valuelist(peopleAdded.fullname,", ") & " phr_elearning_TrainingPath_assignToTheTrainingPath">

				<cfcatch>
					<cfset result.isOk = false>
					<cfset result.message = cfcatch.message>
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- only used for people!!!??! --->
	<cffunction name="insertTrainingPathPerson" access="public" hint="Insert a training path person" returnType="struct">
		<cfargument name="TrainingPathID" type="numeric" required="true">
		<cfargument name="personID" type="string" required="true" hint="Can be a list of person IDs">
		<!--- <cfargument name="entityId" type="string" required="true" hint="Can be a list of entity (person) IDs">
		<cfargument name="entityType" type="string" required="true"> --->

		<cfset var result = {isOK=true,message="Training Path assigned."}>
		<cfset var insertTrainingPathPersonQry = "">
		<!--- <cfset var getEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)> --->

		<cftry>
			<cfquery name="insertTrainingPathPersonQry">
				insert into trngTrainingPathPerson (trainingPathID,personID,createdBy,lastUpdatedBy,lastUpdatedByPerson)
				select
					<cf_queryparam value="#arguments.trainingPathId#" cfsqltype="cf_sql_integer">,
					p.personID,
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
				from person p
					left join trngTrainingPathPerson tpp on p.personID = tpp.personID and tpp.trainingPathID = <cf_queryparam value="#arguments.trainingPathId#" cfsqltype="cf_sql_integer">
				where p.personID in (<cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer" list="true">)
					and tpp.ID is null
			</cfquery>

			<!--- <cfquery name="insertTrainingPathEntityQry">
				insert into trngTrainingPathEntity (trainingPathID,entityTypeID,entityID,createdBy,lastUpdatedBy,lastUpdatedByPerson)
				select
					<cf_queryparam value="#arguments.trainingPathId#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#application.entitytypeID[arguments.entityType]#" cfsqltype="cf_sql_integer">,
					#getEntityType.uniqueKey#,
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
				from #getEntityType.tablename# e
					left join trngTrainingPathEntity t on e.#getEntityType.uniqueKey# = t.entityID and t.entityTypeID=<cf_queryparam value="#application.entitytypeID[arguments.entityType]#" cfsqltype="cf_sql_integer"> and t.trainingPathID = <cf_queryparam value="#arguments.trainingPathId#" cfsqltype="cf_sql_integer">
				where #getEntityType.uniqueKey# in (<cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer" list="true">)
					and t.trainingPathEntityID is null
			</cfquery> --->

			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="deleteTrainingPathPerson" access="public" hint="Delete a training path person record" returnType="struct">
		<cfargument name="TrainingPathID" type="numeric" required="true">
		<cfargument name="personID" type="string" required="true" hint="Can be a list of person IDs">
		<!--- <cfargument name="entityId" type="string" required="true" hint="Can be a list of entity (person) IDs">
		<cfargument name="entityType" type="string" required="true"> --->

		<cfset var result = {isOK=true,message="Training Path un-assigned."}>
		<cfset var deleteTrainingPathPersonQry = "">

		<cftry>
			<cfquery name="deleteTrainingPathPersonQry">
				delete from trngTrainingPathPerson
				where trainingPathID = <cf_queryparam value="#arguments.trainingPathId#" cfsqltype="cf_sql_integer">
					and personID in (<cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer" list="true">)
			</cfquery>

			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


</cfcomponent>