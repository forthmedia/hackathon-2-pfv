<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

This code from

http://jasperforge.com/plugins/espforum/view.php?group_id=112&forumid=102&topicid=26597

TO DO: create images path settings variable


WAB 2012-07-09 Mods to query which brings back list of reports.  
				Added support for linking reports to Modules and Groups

2013-03-08 PPB Case 433303 added try/catch around first hit of UserObject.findRoles to help debug	
2013-03-21 PPB Case 433303 added extra condition to ensure the WebService object is loaded as recommended by NJH

 --->


 
<cfcomponent displayname="JasperServerService" hint="Jasper Server Service" output="false" extends="jasperServer">


	<cfset this.repositoryPath  = "/services/repository">
	<cfset this.adminPath  = "/services/UserAndRoleManagementService">
	<cfset webServiceObjects = {}>


<cffunction name="initWebService" access="public"  hint="Constructor.">
	<cfargument name="type" default = "user">
	
		<cfset var objectResult = "">
		<cfset var username = listRest(getJasperServerLoginParamsForCurrentUser(refreshUser=false),"=")>
		<cfset var password  = getJasperServerWebServicePassword()>
		<cfset var wsdlURL = "">
		<cfset var result = {isOK = true}>
		<cfset var warningStruct = "">
		<cfset var path = '' />
		<!--- NOTE This item deliberately not var'ed <cfset var webserviceobjects = '' /> --->
		
		<cfswitch expression="#arguments.type#">		<!--- 2013-03-21 PPB Case 433303 scope --->
			<cfcase value="repository">
				<cfset path = "/services/repository">
			</cfcase>
			<cfcase value="user">
				<cfset path = "services/UserAndRoleManagementService">
			</cfcase>
			<cfcase value="tenant">
				<cfset path = "services/OrganizationManagementService">
			</cfcase>
			<cfcase value="permission">
				<cfset path = "services/PermissionsManagementService">
			</cfcase>
		</cfswitch>
		
		<cfset wsdlURL = "#getjasperServerPath()#/#path#?wsdl">
		
		<cftry>
			<!--- <cfobject name="objectResult" webservice="#wsdlURL#" username="#username#" password="#password#" refreshwsdl="true">
	
			<cfcatch>
				<cftry> --->
					<cfset username="superuser">
					<cfset password = application.com.settings.getSetting("reports.superUserpassword")>
					
					<cfobject name="objectResult" webservice="#wsdlURL#" username="#username#" password="#password#" refreshwsdl="true">
					<cfcatch>
						<cftry>
							<cfset password = "superuser">
							<cfobject name="objectResult" webservice="#wsdlURL#" username="#username#" password="#password#" refreshwsdl="true">
							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.message=cfcatch.message>
								<cfset result.URLCalled= "#wsdlURL#&username=#username#&password=#password#">
								<cfset warningStruct = {arguments = arguments}>
								<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Webservice",Severity="error",catch=cfcatch,WarningStructure=warningStruct,TTL=30)>
							</cfcatch>
						</cftry>
					</cfcatch>
				<!--- </cftry>
			</cfcatch> --->
		</cftry>

		<cfset result.object = objectResult>
		<cfset webserviceobjects[arguments.type] = objectResult>		<!--- 2013-03-21 PPB Case 433303 scope --->

		<cfreturn result>
</cffunction>	

<cffunction name="getWebServiceObject">
	<cfargument name="type" default = "user">
	
	<cfif not structKeyExists(webserviceObjects,arguments.type) or webserviceObjects[arguments.type] eq "">		<!--- 2013-03-21 PPB Case 433303 added condition --->
		<cfset initWebService(type=arguments.type)>																<!--- 2013-03-21 PPB Case 433303 scope --->
	</cfif>
	
	<cfreturn webserviceObjects[arguments.type]>			<!--- 2013-03-21 PPB Case 433303 scope --->
</cffunction>



<!--- Private Functions --->
<cffunction name="dumpvar" access="private">
	<cfargument name="daVar">
	<cfargument name="abort" default="true">
		
		<cfdump var="#daVar#">
		<cfif arguments.abort>
			<CF_ABORT>
		</cfif>
</cffunction>

 
<cffunction name="getMimeTypes" access="private" returntype="any" output="false" hint="list mime types.">
	<cfargument name="listIterator" required="true">
	<cfscript>
		var iterator = arguments.listIterator;
		var mimeTypes = structNew();
		var header = "";
		while (iterator.hasNext()) {
		header = iterator.next();
		//dumpvar(iterator.next());
		mimeTypes[header.getName()] = header.getValue();
		}
		return mimeTypes;
	</cfscript>
</cffunction>

<cffunction name="outputContent" access="private" returntype="any" output="false" hint="output content.">
	<cfargument name="mimetype" required="true">
	<cfargument name="content" required="true">
	<cfargument name="filename" default="report">

	<cfif mimetype eq "application/pdf">
		<cfheader name="Content-Disposition" value="attachment;filename=#arguments.filename#.pdf">
	<cfelseif mimetype eq "application/rtf">
		<cfheader name="Content-Disposition" value="attachment;filename=#arguments.filename#.rtf">
	</cfif>

	<cfif listFirst(mimeTypes["Content-Type"],"/") neq "image">
		<cfcontent type="#arguments.mimetype#" variable="#arguments.content#" reset="true">
	<cfelse>
		<cfif not directoryExists("d:\web\relay\images\jasperserver")>
			<cfset application.com.globalFunctions.createDirectoryRecursive(fullPath="d:\web\relay\images\jasperserver")>
		</cfif>
		<cffile action="write" file="d:\web\relay\images\jasperserver\#arguments.filename#.#listLast(arguments.mimetype,'/')#" output="#arguments.content#">
	</cfif>

</cffunction>


<cffunction name="outputAttachments" access="public" returntype="any" output="false" hint="output Attachments.">
	<cfargument name="attachments" required="true">
	<cfargument name="reportfilename" default="">
	
	<cfset var filSize = '' />
	<cfset var ins = '' />
	<cfset var arr = '' />
	<cfset var mimeTypes = '' />
	<cfset var wee = '' />
	<cfset var xmlDataBuffer = '' />
	
	<cfscript>
		var allAttachments = arguments.attachments;
		var attachment = "";
		var i = 0;
		var byteClass = createObject("java", "java.lang.Byte");
		var byteArrayObj = createObject("java","java.lang.reflect.Array");
		var byteArray = "";
		//dumpvar(allAttachments);
		for (i = 1; i lte arrayLen(allAttachments); i = i+1) {
			attachment = allAttachments[i];   // WAB correction here added index
			//dumpvar(attachment);
			filSize = attachment.getSize();
			byteArray = byteArrayObj.newInstance(byteClass.TYPE, filSize);
			ins = attachment.getActivationDataHandler().getInputStream();
			arr = ins.read(byteArray,0,filSize);
			mimeTypes = getMimeTypes(attachment.getAllMimeHeaders());
			dumpvar(mimeTypes,false);
			if (listFirst(mimeTypes["Content-Type"],"/") eq "image") {
				reportfilename=mimeTypes["Content-Id"];
			}
			outputContent(mimeTypes["Content-Type"],byteArray,reportfilename);
		
			if (listFirst(mimeTypes["Content-Type"],"/") neq "image") {
				flvoutstream.write(byteArray);
				flvoutstream.flush();
				response.reset(); // reset the feed to the browser
				flvoutstream.close(); // close the stream to flash
			
				//dumpvar(byteArray);
				//mimeTypes = getMimeTypes(attachment.getAllMimeHeaders());
				//dumpvar(mimeTypes,false);
				wee = attachment.getActivationDataHandler().getTransferDataFlavors();
				ins = attachment.getActivationDataHandler().getInputStream();
				xmlDataBuffer = CreateObject("java","java.lang.String").init(ins);
			
				//dumpvar(xmlDataBuffer);
		
				// dumpvar(mimeTypes);
				if (mimeTypes["Content-Type"] eq "application/pdf"){
					outputContent(mimeTypes["Content-Type"],attachment.getContent());
				}
				else {
					outputContent(mimeTypes["Content-Type"],attachment.getAttachmentFile());
				}
			}
		}
		return attachment;
	</cfscript>
	
</cffunction>


<cffunction name="getAttachments" access="public" returntype="any" output="false" hint="output Attachments.">
	<cfargument name="attachments" required="true">
	<cfargument name="reportfilename" default="">
	

   	<cfset var filSize = '' />
	<cfset var ins = '' />
	<cfset var arr = '' />
	<cfset var mimeTypes = '' />
	<cfset var inputStream = '' />
	<cfset var objIn = '' />
	<cfset var com = '' />
	
	<cfscript>
		var allAttachments = arguments.attachments;
		var result = arrayNew(1) ;
		var attachment = "";
		var i = 0;
		var byteClass = createObject("java", "java.lang.Byte");
		var byteArrayObj = createObject("java","java.lang.reflect.Array");
		var byteArray = "";
		//dumpvar(allAttachments);
		for (i = 1; i lte arrayLen(allAttachments); i = i+1) {
			result[i] = structNew() ;
			attachment = allAttachments[i];   // WAB correction here added index
			filSize = attachment.getSize();
			byteArray = byteArrayObj.newInstance(byteClass.TYPE, filSize);
			ins = attachment.getActivationDataHandler().getInputStream();
			arr = ins.read(byteArray,0,filSize);
			mimeTypes = getMimeTypes(attachment.getAllMimeHeaders());

			result[i]["Content-Type"] = mimeTypes["Content-Type"] ;
			result[i].mimeTypes = mimeTypes ;
			result[i].byteArray = byteArray; 
			result[i].string = ToString( byteArray); 
			
			
			if (result[i].mimeTypes["content-id"] is "jasperPrint" ) {
			
				inputStream = CreateObject("java", "java.io.ByteArrayInputStream") ;
   				objIn = CreateObject("java", "java.io.ObjectInputStream") ;
   				com = "" ;
   				inputStream.init(toBinary(byteArray)) ;
	   			objIn.init(inputStream) ;
   				com = objIn.readObject() ;
   				objIn.close();
				result[i].jrprintobject = com ;
			}
				

//			outputContent(mimeTypes["Content-Type"],byteArray,reportfilename);

	/*		flvoutstream.write(byteArray);
			flvoutstream.flush();
			response.reset(); // reset the feed to the browser
			flvoutstream.close(); // close the stream to flash
			dumpvar(byteArray);
			mimeTypes = getMimeTypes(attachment.getAllMimeHeaders());
			dumpvar(attachment.getContent());
			wee = attachment.getActivationDataHandler().getTransferDataFlavors();
			ins = attachment.getActivationDataHandler().getInputStream();
			xmlDataBuffer = CreateObject("java","java.lang.String").init(ins);
		
//			dumpvar(xmlDataBuffer);
	*/
			// dumpvar(mimeTypes);

		/*		
			if (mimeTypes["Content-Type"] eq "application/pdf"){
				//outputContent(mimeTypes["Content-Type"],attachment.getContent());
				result[i].getContent = attachment.getContent();
			}
			else {
				result[i].getAttachmentFile = attachment.getAttachmentFile() ;
				//outputContent(mimeTypes["Content-Type"],attachment.getAttachmentFile());
			}
			
		*/	
		}
		return result;
	</cfscript>

	
</cffunction>

<!--- Public Functions --->
<cffunction name="list" access="public" returntype="any" output="true" hint="list jasper server resources.">
	<cfargument name="uriString" default="/">
	<cfargument name="wsType" default="folder">

	<cfset var listXML = "">
	<cfset var list = "listXML">
	<cfset var WebServiceObject = '' />

	<cfsavecontent variable="listXML">
		<request operationName="list" locale="en">
		<resourceDescriptor name="" wsType="<cfoutput>#arguments.wsType#</cfoutput>" uriString="<cfoutput>#arguments.uriString#</cfoutput>" isNew="false">
		<label>null</label>
		</resourceDescriptor>
		</request>
	</cfsavecontent>
	 	
	<!--- <cfscript>
	list = this.webservice.List(listXML);
	</cfscript> --->
	<cfset WebServiceObject	= getWebServiceObject("repository")>
	<cfset list = WebServiceObject.List(listXML)>
	
	<cfreturn list>
</cffunction>

<!--- Public Functions --->
<cffunction name="listAll" access="public" returntype="any" output="true" hint="list all jasperserver resources below parent">
	<cfargument name="uriString" default="/">
	<cfargument name="Type" default="reportUnit">

	<cfset var listXML = "">
	<cfset var list = "listXML">
	<cfset var WebServiceObject = '' />

	<cfsavecontent variable="listXML">
		<cfoutput>
		<request operationName="list" locale="en">
			<argument name="LIST_RESOURCES"/>
			<argument name="RESOURCE_TYPE">#Type#</argument>
			<argument name="PARENT_DIRECTORY">#uriString#</argument>
		</request>
		</cfoutput>
	</cfsavecontent>

	<cfset WebServiceObject	= getWebServiceObject("repository")>

	<CFSET list = WebServiceObject.List(listXML)>
	
	<cfreturn list>
</cffunction>


<!--- Public Functions --->
<cffunction name="get" access="public" returntype="any" output="false" hint="list jasper server resources.">
	<cfargument name="uriString" default="/">
	<cfargument name="wsType" default="folder">
	<cfargument name="reportParams" default="#structNew()#">  <!--- WAb mod --->
	
	<cfset var getXML = "">
	<cfset var result = "">
	<cfset var WebServiceObject = '' />
	<cfset var param = '' />	

	<cfsavecontent variable="getXML">
		<cfoutput>
		<request operationName="get" locale="en">
		<cfloop collection="#arguments.reportParams#" item="param">
		<argument name="#ucase(param)#"><cfoutput>#arguments.reportParams[param]#</cfoutput></argument>
		</cfloop> 
		<resourceDescriptor name="" wsType="#arguments.wsType#" uriString="#arguments.uriString#" isNew="false">
		<label>null</label>
		</resourceDescriptor>
		</request>
		</cfoutput>
	</cfsavecontent>

	<cfset WebServiceObject	= getWebServiceObject("repository")>
	<cfset result = WebServiceObject.Get(getXML)>
	
	<cfreturn result>
</cffunction>


<cffunction name="move" access="public" returntype="any" output="false" hint="move jasper server resources.">
	<cfargument name="uriString" default="/">
	<cfargument name="wsType" default="folder">
	<cfargument name="destinationURI" type="string" required="true">
	
	<cfset var moveXML = "">
	<cfset var result = "">
	<cfset var WebServiceObject = '' />
	
	<cfsavecontent variable="moveXML">
		<cfoutput>
		<request operationName="move" locale="en">
		<argument name="DESTINATION_URI"><cfoutput>#arguments.destinationURI#</cfoutput></argument>
		<resourceDescriptor name="" wsType="#arguments.wsType#" uriString="#arguments.uriString#">
		</resourceDescriptor>
		</request>
		</cfoutput>
	</cfsavecontent>

	<cfset WebServiceObject	= getWebServiceObject("repository")>
	<cfset result = WebServiceObject.Move(moveXML)>
	
	<cfreturn result>
</cffunction>


<cffunction name="delete" access="public" returntype="any" output="false" hint="delete jasper server resources.">
	<cfargument name="uriString" default="/">
	<cfargument name="wsType" default="folder">
	
	<cfset var deleteXML = "">
	<cfset var result = "">
	<cfset var WebServiceObject = '' />

	<cfsavecontent variable="deleteXML">
		<cfoutput>
		<request operationName="delete" locale="en">
		<resourceDescriptor name="" wsType="#arguments.wsType#" uriString="#arguments.uriString#">
		</resourceDescriptor>
		</request>
		</cfoutput>
	</cfsavecontent>

	<cfset WebServiceObject	= getWebServiceObject("repository")>
	<cfset result = WebServiceObject.Delete(deleteXML)>

	<cfreturn result>
</cffunction>


<cffunction name="runReport" access="public" returntype="any" output="true" hint="runs a jasper report.">
	<cfargument name="uriString" default="/">
	<cfargument name="reportParams" default="#structNew()#">  <!--- WAb mod --->

	<cfset var resultXMLDoc = '' />
	<cfset var reportAttachements = '' />
	<cfset var attachments = '' />
	<cfset var jasperPrint = '' />
	<cfset var getPages = '' />
	<cfset var getElements = '' />
	<cfset var getElements2 = '' />
	<cfset var pp = '' />	<cfset var outputFormat = "PDF">
	<cfset var imagesUri = "/images/jasperserver/">
	<cfset var reportXML = "">
	<cfset var report = "reportXML">
	<cfset var reportFileName = listlast(uriString,"/")>
	<cfset var webServiceObject = getWebServiceObject("repository")>

	
	<cfif structKeyExists(arguments.reportParams,"outputFormat")>
		<cfset outputFormat = arguments.reportParams.outputFormat>
	</cfif>

	<cfif structKeyExists(arguments.reportParams,"reportFileName")>
		<cfset reportFileName = arguments.reportParams.reportFileName>
	</cfif>
	
	<cfsavecontent variable="reportXML">
		<request operationName="runReport" locale="en">
		<argument name="IMAGES_URI"><cfoutput>#imagesUri#</cfoutput></argument>
		<argument name="RUN_OUTPUT_FORMAT"><cfoutput>#outputFormat#</cfoutput></argument>
		<resourceDescriptor name="" wsType="" uriString="<cfoutput>#arguments.uriString#</cfoutput>" isNew="false">
		<label>null</label>
		<cfloop list="#structKeyList(arguments.reportParams)#" index="pp">
		<parameter name="<cfoutput>#lcase(pp)#</cfoutput>" <cfif isdefined("inputcontrol_#pp#_isList")>isListItem="true" </cfif>><cfoutput>#arguments.reportParams[pp]#</cfoutput></parameter>
		</cfloop>
		<!---
		<parameter name="EmployeeID">emil_id</parameter>
		<parameter name="TEST_LIST" isListItem="true">A &amp; L Powers Engineering, Inc</parameter>
		<parameter name="TEST_LIST" isListItem="true">A &amp; U Jaramillo Telecom, Inc</parameter>
		<parameter name="TEST_LIST" isListItem="true">A &amp; U Stalker Telecom, Inc</parameter>
		--->
		</resourceDescriptor>
		</request>
	</cfsavecontent>
	
<!--- 	<cfdump var="#xmlparse(reportXML)#"><CF_ABORT>  --->
	<cfscript>

	//report = this.webservice.runReport(reportXML);
	report = webServiceObject.runReport(reportXML);
	resultXMLDoc = xmlParse(report);
	//dumpVar(resultXMLDoc,false);
	if(resultXMLDoc.operationResult.returncode.xmltext is not 0) {
		dumpVar(resultXMLDoc) ;
	} else {
	 //outputAttachments(this.webservice.getAttachments(),reportFileName);

		reportAttachements=webServiceObject.getAttachments();

		 attachments = getAttachments(reportAttachements,reportFileName);
		
	if (attachments[1].mimeTypes["content-id"] is "jasperPrint" ) {
		jasperPrint = attachments[1].jrprintobject ;
//		dumpvar (jasperPrint, false);
		getPages = jasperPrint.getPages() ;
		 dumpvar (getmetadata(getPages[1]), false);
		writeoutput (getPages[1].toString()) ;
//		traverseJRPrintElements (getPages[1].getElements());
//		getElements = getPages[1].getElements();
//		getElements2 = getElements[1].getElements();
//		dumpvar (getElements2[1].gettext(), false);
		//writeoutput (getPages[1]);
		
		
	}	else {
	
		//writeoutput (attachments[1].string) ;
	}
	
	outputAttachments(reportAttachements,reportFileName);
		return  ;
	}
	

	</cfscript>

</cffunction>

<cffunction name="traverseJRPrintElements">
	<cfargument name="elementsArray" >
	
	<cfscript>
		var index = 0 ;
		var element = "";
		
		for (index= 1; index <=arrayLen(elementsArray); index++) {
			element = elementsArray[index];
			if (false and element.classname is "net.sf.jasperreports.engine.fill.JRTemplatePrintFrame") {
				traverseJRPrintElements (element.getElements()); 
			} else if (structKeyExists(element,"gettext")) {
				writeoutput (element.getText()) ;
			} else {
					dumpvar (getmetadata(element).methods,false) ;
					
			}
		
		}
		
		return  ;
			
	</cfscript>

	
</cffunction>

<!--- This function uses the webService ListAll Operation to return all reports a user has rights to --->
<cffunction name="getReportsV2" output="false">
	<cfargument name="root" default = "">

	<cfset var listXML = listAll(uristring = "#root#")>
	<cfset var result = xmlparse (listXML )>
	
	<cfreturn result>
	
</cffunction>

<!--- This function uses the webService List Operation to recursively return all reports a user has rights to --->
<cffunction name="getReportsV1" output="false">
	<cfargument name="root" default = "">


	<cfset var i = 0>
	<cfset var x = "">
	<cfset var reportsXML = "">
	<cfset var foldersXML = "">
	<cfset var listXML = "">
	<cfset var listxmldoc = "">


	
	<cfoutput>Get Folder #htmleditformat(root)# <BR></cfoutput>
	<cfset listXML = list(uristring = "#root#", wstype="folder")>
	<cfset listxmldoc = xmlparse(listXML)>
	

	
	<cfif structKeyExists (listxmldoc, "operationresult") and structKeyExists (listxmldoc.operationresult, "resourceDescriptor")>
	
		<cfset reportsXML = xmlsearch(listxmldoc,"//resourceDescriptor[@wsType='reportUnit']") >  

		<!--- <cfoutput>#arrayLen(reportsXML)# reports found<BR></cfoutput> --->
	
		<cfset foldersXML = xmlsearch(listxmldoc,"//resourceDescriptor[@wsType='folder']") >  
		<cfloop index= "i" from = 1 to = #arrayLen(foldersXML)#>
			<cfset x = getReportsV1(foldersXML[i].xmlattributes.uriString)>
			<cfif isXML (x)>
				<cfset arrayAppend(foldersXML[i].xmlchildren,xmlelemnew(listxmldoc,"children"))>
				<cfset application.com.xmlfunctions.xmlappend (foldersXML[i].children,x,false)>
			</cfif>
		</cfloop>
	
			<cfreturn listxmldoc.operationresult>			

		
	<cfelse>
		<cfreturn "">
	
	</cfif>


</cffunction>

<!--- Outputs List Of reports from XML returned by getReportsV1 --->
<cffunction name="outputReportsV1" >
	<cfargument name="XML">

	<cfset var node = '' />
	<cfset var link = '' />
	<cfset var i = "">
	
	<UL>
	<cfloop index = "i" from= "1" to = "#arrayLen(XML.xmlchildren)#">
		<cfset node = XML.xmlchildren[i]>
		<cfif node.xmlname is "resourceDescriptor">
			
			<cfif node.xmlattributes.wsType is "folder">
				<cfoutput><LI>#node.label.xmlText#	</cfoutput>
				<cfif structKeyExists (node,"children")>
					<cfset outputReportsV1 (node.children)>
				</cfif>
				<cfoutput></LI></cfoutput>
			<cfelseif node.xmlattributes.wsType is "reportUnit">
		 				<cfset link = "#getjasperserverpath()#/flow.html?j_username=#request.relaycurrentuser.person.username#&j_password=#request.relaycurrentuser.person.password#&_flowId=viewReportFlow&reportUnit=#node.xmlattributes.uristring#&decorate=no">
						<cfoutput><LI><a onclick = "openNewTab('Report','#node.label.xmltext#','#link#');return false">#node.label.xmlText#</a> <a href="#link#" target="_new">New window</a>  </LI></cfoutput>  
			</cfif>
				
		</cfif>
		
	
	</cfloop>
	</UL>
</cffunction>


<!--- Outputs List Of reports from XML returned by getReportsV2 --->
<cffunction name="outputReportsV2" >
	<cfargument name="XML">

	<cfset var i = "">
	<cfset var node = '' />
	<cfset var parenturi = '' />
	<cfset var reportsArray = xmlsearch (XML,"//resourceDescriptor")>
	<cfset var name ="">
	
	<UL>
	<cfloop index = "i" from= "1" to = "#arrayLen(reportsArray)#">
		<cfset node = reportsArray[i]>
			
			<cfset name = reverse(listfirst (reverse(node.xmlattributes.uristring),"/"))>
			<cfset parenturi = reverse(listrest (reverse(node.xmlattributes.uristring),"/"))>
			<cfset parenturi = replace (parenturi,"/","","ONCE")>			<!--- remove leading / --->
			<cfif application.com.jasperServer.doesUserHaveAccessToObject(uriString="/"&parenturi,method="database")>
				<cfoutput><LI><a href="#getJasperServerReportUnitPath()##parenturi#/#name#">#node.label.xmlText#</a> <!--- <a href="#link#" target="_new">New window</a> --->  </LI></cfoutput>
			</cfif>
	</cfloop>
	</UL>

</cffunction>


<cffunction name="put" access="public" returntype="any" output="true" hint="creates or updates jasper server resources.">
	<cfargument name="uriString" default="/">
	<cfargument name="wsType" default="folder">
	<cfargument name="label" default="">
	<cfargument name="description" default="">
	<cfargument name="name" default="">

	<cfset var putXML = "">
	<cfset var WebServiceObject = '' />
	<cfset var result = '' />

	<cfsavecontent variable="putXML">
		<request operationName="put" locale="en">
		<resourceDescriptor name="<cfoutput>#arguments.name#</cfoutput>" wsType="<cfoutput>#arguments.wsType#</cfoutput>" uriString="<cfoutput>#arguments.uriString#</cfoutput>" isNew="true">
		<label><cfoutput>#arguments.label#</cfoutput></label>
		<description><cfoutput>#arguments.description#</cfoutput></description>
		</resourceDescriptor>
		</request>
	</cfsavecontent>
	 
	<cfset WebServiceObject	= getWebServiceObject("repository")>
	<cfset result = WebServiceObject.Put(putXML)>
	
	<cfreturn result>
</cffunction>


<cffunction name="getPermissionsForObject" access="public" returntype="any" output="true" hint="creates or updates jasper server resources.">
	<cfargument name="uriString" type="string" default="">
	<cfargument name="getInheritedPermissions" type="boolean" default="true">
	
	<cfset var permissionsArray = arrayNew(1)>
	<cfset var permissionObj = "">
	<cfset var roles = "">
	<cfset var parentRoles = "">
	<cfset var parentRole = "">
	<cfset var roleName = '' />
	<cfset var parentURI = '' />	
	<cfset var WebServiceObject	= getWebServiceObject("permission")>

	<cfset permissionsArray = WebServiceObject.getPermissionsForObject(arguments.uriString)>
	
	<cfloop array=#permissionsArray# index="permissionObj">
		<cfset roleName = permissionObj.getPermissionRecipient().getRoleName()>
		<cfif roleName neq "" and not listFindNoCase(roles,roleName) <!--- and roleName contains "ROLE_MOD_"  --->>
			<cfset roles = listAppend(roles,roleName)>
		</cfif>
	</cfloop>
	
	<!--- this webservice doesn't give any inherited permissions, so you have to traverse back to the root to get all the permissions --->
	<cfif listLen(arguments.uriString,"/") neq 1>
		<cfset parentURI = listDeleteAt(arguments.uriString,listLen(arguments.uriString,"/"),"/")>
		<cfset parentRoles = getPermissionsForObject(uriString=parentURI)>
		<cfloop list="#parentRoles#" index="parentRole">
			<cfif not listFindNoCase(roles,parentRole)>
				<cfset roles = listAppend(roles,parentRole)>
			</cfif>
		</cfloop>
	</cfif>

	<cfreturn roles>
</cffunction>


<cffunction name="runJasperserverWS">
	<cfargument name="method" type="string" required="true">
	<cfargument name="wstype" type="string" default="repository">
	
	<cfset var startXML = "">
	<cfset var endXML = "">
	<cfset var functionXML = "">
	<cfset var xmlToSend = "">
	<cfset var WebServiceObject = '' />
	<cfset var result = '' />
	
	<cfsavecontent variable="startXML">
		<request operationName="<cfoutput>#arguments.method#</cfoutput>" locale="en">
	</cfsavecontent>
	
	<cfsavecontent variable="endXML">
		</request>
	</cfsavecontent>
	
	<cfset functionXML = evaluate(method&"(argumentCollection=arguments)")>
	<cfset xmlToSend = startXML&functionXML&endXML>
	<cfset WebServiceObject	= getWebServiceObject(arguments.wstype)>
	<cfset result = evaluate("WebServiceObject."&arguments.method&"(xmlToSend)")>
</cffunction>


<!--- <cffunction name="runReport" access="public" returntype="any" output="true" hint="Runs a report from jasper server.">
	<cfargument name="uriString" default="/">

	<cfset var runReportXML = "">

	<cfsavecontent variable="runReportXML">
		<resourceDescriptor name="" wsType="" uriString="<cfoutput>#arguments.uriString#</cfoutput>" isNew="false">
		<label></label>
		</resourceDescriptor>
	</cfsavecontent>
	
	<cfreturn runReportXML>
</cffunction> --->


<cffunction name="findUsers" access="public" returntype="any" output="true" hint="Finds users on jasperserver.">
	<cfargument name="user">
	<cfargument name="tenantID">
	
	<cfset var UserObject = getWebServiceObject("user")>
	<cfset var userObjCrit = createObject("component","jasperServer.WSUserSearchCriteria")>
	
	<cfset userObjCrit.tenantID = arguments.tenantID>
	<cfif arguments.user neq "">
		<cfset userObjCrit.name = arguments.user>
	</cfif>
	<cfset userObjCrit.maxRecords = 0>
	<cfset userObjCrit.includeSubOrgs = true>
	
	<cfreturn userObject.findUsers(userObjCrit)>
</cffunction>


<cffunction name="setPermission" access="public" returntype="struct">
	<cfargument name="uriString" type="string">
	<cfargument name="permissionMask" type="numeric" default="32">
	<cfargument name="role" type="string" default="">
	<cfargument name="user" type="string" default="">
	
	<cfset var UserObject = "">
	<cfset var WebServiceObject = "">
	<cfset var users = arrayNew(1)>
	<cfset var roles = arrayNew(1)>
	<cfset var roleCriteria = "">
	<cfset var userCriteria = "">
	<cfset var permission = "">
	<cfset var jsUser = "">
	<cfset var setPermission = false>
	<cfset var result = {isOK=true,message="Permission set"}>
	<cfset var warningStruct = "">
	
	<cfset var permissionObj = createObject("component","jasperServer.WSObjectPermission")>
	<cfset permissionObj.uri="repo:"&arguments.uriString>
	<cfset permissionObj.permissionMask=arguments.permissionMask>
	
	<cfset WebServiceObject	= getWebServiceObject("permission")>
	<cfset UserObject = getWebServiceObject("user")>

	<cfif arguments.role neq "">
		<cfset roleCriteria = createObject("component","jasperServer.WSRoleSearchCriteria")>
		<cfset roleCriteria.rolename=arguments.role>
		<!--- START 2013-03-08 PPB Case 433303 added try/catch --->
		<cftry>
			<cfset roles = UserObject.findRoles(roleCriteria)>
			<cfcatch>
				<cfset warningStruct = {arguments = arguments}>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Webservice",Severity="error",catch=cfcatch,WarningStructure=warningStruct,TTL=30)>
			</cfcatch>
		</cftry>
		<!--- END 2013-03-08 PPB Case 433303 --->
		<cfif arrayLen(roles) gt 0>
			<cfset permissionObj.permissionRecipient=roles[1]>
			<cfset setPermission = true>
			<cfoutput>set permission for #roles[1].getRoleName()# on #arguments.uriString#<br></cfoutput>
		</cfif>
		
	<cfelseif arguments.user neq "">
		<cfset userCriteria = createObject("component","jasperServer.WSUserSearchCriteria")>
		<cfset userCriteria.name=arguments.user>
		<cfset userCriteria.tenantID = getTenantID()>
		
		<cftry>
			<cfset users = UserObject.findUsers(userCriteria)>
			<cfcatch>
				<cfset warningStruct = {arguments = arguments}>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Webservice",Severity="error",catch=cfcatch,WarningStructure=warningStruct,TTL=30)>
			</cfcatch>
		</cftry>
		
		<cfif arrayLen(users) gt 0>
			<!--- NJH not sure why, but for some reason the webservice result does a like.. so, searching for 2_int brings back 1032_int as well --->
			<cfloop array="#users#" index="jsUser">
				<cfif jsUser.getUsername() eq arguments.user>
					<cfset permissionObj.permissionRecipient=jsUser>
					<cfset setPermission = true>
					<cfoutput>set permission for #jsUser.getUserName()# on #arguments.uriString#<br></cfoutput>
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
	</cfif>

	<cfif setPermission>
		<cftry>
			<cfset permission = WebServiceObject.putPermission(permissionObj)>
			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.message="#cfcatch.message# #cfcatch.detail#">
				<cfset warningStruct = {arguments = arguments}>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Webservice",Severity="error",catch=cfcatch,WarningStructure=warningStruct,TTL=30)>
			</cfcatch>
		</cftry>
	</cfif>

	<cfreturn result>
</cffunction>


<cffunction name="deleteUser" returntype="struct">
	<cfargument name="user" type="string" required="true">
	<cfargument name="tenantID" type="string" default="#getTenantID()#">
	
	<cfset var UserObject = getWebServiceObject("user")>
	<cfset var userObj = createObject("component","jasperServer.WSUser")>
	<cfset var result = {isOK=true,message="User deleted"}>
	<cfset var warningStruct = "">
	
	<cfset userObj.tenantID = arguments.tenantID>
	<cfset userObj.username = arguments.user>
	
	<cftry>
		<cfset userObject.deleteUser(userObj)>
		<cfcatch>
			<cfset result.isOK=false>
			<cfset result.message="#cfcatch.message# #cfcatch.detail#">
			<cfset warningStruct = {arguments = arguments}>
			<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Webservice",Severity="error",catch=cfcatch,WarningStructure=warningStruct,TTL=30)>
		</cfcatch>
	</cftry>
	
	<cfreturn result>
</cffunction>


<cffunction name="deleteRole" returntype="struct">
	<cfargument name="role" type="string" required="true">
	<cfargument name="tenantID" type="string" required="true">
	
	<cfset var UserObject = getWebServiceObject("user")>
	<cfset var roleObj = createObject("component","jasperServer.WSRole")>
	<cfset var result = {isOK=true,message="Role deleted"}>
	<cfset var warningStruct = "">
		
	<cfset roleObj.tenantID = arguments.tenantID>
	<cfset roleObj.roleName = arguments.role>
	
	<cftry>
		<cfset userObject.deleteRole(roleObj)>
		<cfcatch>
			<cfset result.isOK=false>
			<cfset result.message="#cfcatch.message# #cfcatch.detail#">
			<cfset warningStruct = {arguments = arguments}>
			<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Webservice",Severity="error",catch=cfcatch,WarningStructure=warningStruct,TTL=30)>
		</cfcatch>
	</cftry>
	
	<cfreturn result>
</cffunction>


<cffunction name="findRoles">
	<cfargument name="role">
	<cfargument name="tenantID">
	
	<cfset var UserObject = getWebServiceObject("user")>
	<cfset var roleObjCrit = createObject("component","jasperServer.WSRoleSearchCriteria")>
	
	<cfset roleObjCrit.tenantID = arguments.tenantID>
	<cfif arguments.role neq "">
		<cfset roleObjCrit.roleName = arguments.role>
	</cfif>
	<cfset roleObjCrit.maxRecords = 0>
	<cfset roleObjCrit.includeSubOrgs = true>
	
	<cfreturn userObject.findRoles(roleObjCrit)>
</cffunction>


<cffunction name="putUser">
	<cfargument name="userName" type="string" required="true">
	
	<cfset var UserObject = getWebServiceObject("user")>
	<cfset var userObj = createObject("component","jasperServer.WSUser")>
	<cfset var roleObj = createObject("component","jasperServer.WSRole")>
	
	<cfset userObj.name = arguments.userName>
	<cfset userObj.tenantID = getTenantID()>
	<!--- <cfset userObj.roles = roleObj> --->
	<cfdump var="#userObj#"><Cfdump var="#UserObject#">
	<cfset UserObject.putUser(userObj)>
	<cfdump var="#userObj#"><Cfdump var="#UserObject#"><cfabort>
</cffunction>

</cfcomponent> 