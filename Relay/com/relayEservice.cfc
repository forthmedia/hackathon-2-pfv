<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="RelayEservice" hint="Functions for Relay eService Module">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProdContract" hint="Returns Product Contract Details">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="ProdContractID" type="string" required="true">
		<CFQUERY NAME="getProdContract" DATASOURCE="#datasource#">
			select * from ProdContract
			  <CFIF ProdContractID EQ 0>
			   	where 1=100
			  <cfelse>
			  	where ProdContractID =  <cf_queryparam value="#ProdContractID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			  </CFIF>
		</CFQUERY>
		<cfreturn getProdContract>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default Insert Query                           
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setProdContract" hint="Returns Product Contract Details">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="ProdContractID" type="string" required="true">
		<cfargument name="ContractNo" type="string" required="true">
		<cfargument name="ContractStartDate" type="date" required="yes">
		<cfargument name="ContractEndDate" type="date" required="yes">
		<cfargument name="OrganisationID" type="numeric" required="yes">
		<cfargument name="ProdContractTypeID" type="numeric" required="yes">
		<cfargument name="reseller" type="string" required="true">
		
		<cfif productContractID eq 0>
			<CFQUERY NAME="insertProdContract" DATASOURCE="#datasource#">
			INSERT INTO ProdContract (
				ContractNo, 
				ContractStartDate,
				ContractEndDate,
				OrganisationID,
				ProdContractTypeID,
				Reseller)
			values (
				<cf_queryparam value="#ContractNo#" CFSQLTYPE="CF_SQL_varchar" >, 
				<cf_queryparam value="#ContractStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#ContractEndDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				<cf_queryparam value="#prodContractTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#Reseller#" CFSQLTYPE="CF_SQL_VARCHAR" >)			
		</CFQUERY>
	
		<cfelse>
			<CFQUERY NAME="setProdContract" DATASOURCE="#datasource#">
				select * from ProdContract
				  <CFIF ProdContractID EQ 0>
				   	where 1=1
				  <cfelse>
				  	where ProdContractID =  <cf_queryparam value="#ProdContractID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				  </CFIF>
			</CFQUERY>
	</cfif>
		<cfreturn "Product contract updated">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default Update Query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateProdContract" hint="Updates Product Contract Details">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="ProdContractID" type="string" required="true">
		<cfargument name="ContractNo" type="string" required="true">
		<cfargument name="ContractStartDate" type="date" required="yes">
		<cfargument name="ContractEndDate" type="date" required="yes">
		<cfargument name="OrganisationID" type="numeric" required="yes">
		<cfargument name="ProdContractTypeID" type="numeric" required="yes">
		<cfargument name="reseller" type="string" required="true">
		
		<cfif productContractID neq 0>
			<CFQUERY NAME="updateProdContract" DATASOURCE="#datasource#">
			UPDATE ProdContract SET
				[ContractNo] =  <cf_queryparam value="#ContractNo#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				[ContractStartDate] = #ContractStartDate#
				[ContractEndDate] = #ContractEndDate#
				[OrganisationID] = #OrganisationID#
				[ProdContractTypeID] = #prodContractTypeID#
				[Reseller] =  <cf_queryparam value="#Reseller#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			where ProdContractID =  <cf_queryparam value="#ProdContractID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		<cfset message = "ContractID #ProdContractID# updated">
		</cfif>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getContractProducts" hint="Returns Contract Product Details">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="ProdContractID" type="string" required="true">
		<CFQUERY NAME="getContractProducts" DATASOURCE="#datasource#">
			select * from ProdContractProducts
			  <CFIF ProdContractID EQ 0>
			   	where 1=100
			  <cfelse>
			  	where ProdContractID =  <cf_queryparam value="#ProdContractID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			  </CFIF>
		</CFQUERY>
		<cfreturn getContractProducts>
	</cffunction>

	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             LOAD DATA INTO PRODCONTRACT TABLES                                             
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="loadProductContractData" hint="This process resizes the columns in a table">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		
		<cfscript>
			//check whether all fields present in the load table
			application.com.relayDataLoad.alterLoadTable(arguments.tableName, application.siteDataSource);
		</cfscript>
		
		<!---Get all contract type names--->
		<cfquery name="getDistinctNetSupportColumns" datasource="#arguments.dataSource#">
			INSERT INTO ProdContractType(ContractType,Active)
			SELECT DISTINCT NetCoverSupport, 1
			FROM #arguments.tableName#
			WHERE NetCoverSupport NOT IN (SELECT ContractType FROM prodContractType)
		</cfquery>
		
		<!---And update load table's column ProdContractID--->
		<cfquery name="updateLoadTable" datasource="#dataSource#">
			UPDATE dl
			SET dl.ProdContractTypeID = pct.prodContractTypeID
			FROM #arguments.tableName# dl
			INNER JOIN prodContractType pct ON pct.ContractType = dl.NetCoverSupport
		</cfquery>
			
		<cfquery name="loadContracts" datasource="#arguments.dataSource#">
			INSERT INTO prodContract(ContractNo,ContractStartDate,ContractEndDate,OrganisationID,ProdContractTypeID,Reseller,PersonID)
			SELECT DISTINCT c.ContractNo, c.ContractStartDate, c.ContractEndDate, c.orgID, prodContractTypeID, c.VendorName, c.personID
			FROM  #arguments.tableName# c
			WHERE c.contractNo IS NOT NULL
			AND c.ContractStartDate IS NOT NULL
			AND c.ContractEndDate IS NOT NULL
			AND c.orgID IS NOT NULL
			AND c.personID IS NOT NULL
			AND c.contractNo NOT IN (SELECT contractNo FROM prodContract)
		</cfquery>
			
		<cfquery name="setContractIDsInTheLoadTable" datasource="#dataSource#">
			UPDATE dl 
			set dl.ProdContractID = pc.ProdContractID
			FROM #arguments.tablename# dl
			INNER JOIN prodContract pc ON pc.contractNo = dl.contractNo
				AND pc.ContractStartDate = dl.ContractStartDate
				AND pc.ContractEndDate = dl.ContractEndDate
				AND pc.organisationID = dl.orgID
				AND pc.personID = dl.personID
				AND pc.ProdContractTypeID = dl.ProdContractTypeID
			WHERE pc.contractNo IS NOT NULL
				AND pc.ContractStartDate IS NOT NULL
				AND pc.ContractEndDate IS NOT NULL
				AND pc.organisationID IS NOT NULL
				AND pc.personID IS NOT NULL
		</cfquery>
			
			
		<cfquery name="setProductIDsInTheLoadTable" datasource="#dataSource#">
			UPDATE dl 
			set dl.productID = p.productID
			FROM #arguments.tablename# dl
			INNER JOIN product p ON dl.Part990 = p.RemoteProductID
		</cfquery>
		
		<cfquery name="moveDataToProdContractProductsTable" datasource="#dataSource#">
			INSERT INTO ProdContractProducts(ProdContractID,ProductID,SerialNo)
			SELECT ProdContractID, ProductID, SerialNumber
			FROM #arguments.tableName#
			WHERE ProdContractID IS NOT NULL
			AND ProductID IS NOT NULL
			AND SerialNumber IS NOT NULL
		</cfquery>
		
	</cffunction>
	
</cfcomponent>