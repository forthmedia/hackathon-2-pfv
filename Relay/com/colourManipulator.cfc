<cfcomponent displayname="Functions for changing shade of a colour" hint="">

	<!---
	Returns a hexadecimal color value an amount lighter or darker than the provided color.
	@param hexColor 	 starting hex color (Required)
	@param shade 	 amount hexColor should be incremented(+)/decremented(-) (Required)
	@return returns a string 
	--->
	<cffunction name="ColorShade" returntype="string">
		<cfargument name="hexColor" type="string" required="yes">
		<cfargument name="shade" type="numeric" required="yes">
		<cfif left(hexColor,1) eq "##">
			<cfset hexColor = RemoveChars(hexColor, 1, 1)>
		</cfif>
		<cfset var red = "">
		<cfset var green = "">
		<cfset var blue = "">
		<cfset var s = ARGUMENTS.shade>
		<cfset var incrementedColor = ""> 
		<cfset red = Left(ARGUMENTS.hexColor, 2)>
		<cfset green = Mid(ARGUMENTS.hexColor, 3, 2)>
		<cfset blue = Right(ARGUMENTS.hexColor, 2)>
		<cfset red = NumberFormat(InputBaseN(red, 16), 00)>
		<cfset green = NumberFormat(InputBaseN(green, 16), 00)>
		<cfset blue = NumberFormat(InputBaseN(blue, 16), 00)>	
		<cfset red = IIF(s LT 0, red * (s + 100) / 100, red + (255 - red) * s / 100)>
		<cfset green = IIF(s LT 0, green * (s + 100) / 100, green + (255 - green) * s / 100)>
		<cfset blue = IIF(s LT 0, blue * (s + 100) / 100, blue + (255 - blue) * s / 100)>
		<cfset red = UCase(FormatBaseN(red, 16))>
		<cfset green = UCase(FormatBaseN(green, 16))>
		<cfset blue = UCase(FormatBaseN(blue, 16))>
		<cfset red = IIF(Len(red) LT 2, DE(0&red), DE(red))>
		<cfset green = IIF(Len(green) LT 2, DE(0&green), DE(green))>
		<cfset blue = IIF(Len(blue) LT 2, DE(0&blue), DE(blue))>
		<cfset incrementedColor = UCase(red&green&blue)>
		<cfset incrementedColor = "##" & incrementedColor>
		<cfreturn incrementedColor>
	</cffunction>
	
	<cffunction name="HexToRGB" returntype="string" hint="hex to struct r,g,b,a">
    <cfargument name="hex" type="string" required="true" hint="3 or 6 digit hex value (valid examples: ##FFF, FFF, ##FFFFFF, FFFFFF)">
		<cfset var retVal = "">
		<cfset var rVal = "">
		<cfset var gVal = "">
		<cfset var bVal = "">

		<cfset var i = 0>
		<cfset var o = 0>
		<cfset var offset = 0>
    <cfif mid(arguments.hex, 1, 1) EQ chr(35)>
	    <cfset offset = 1>
		</cfif>
    <cfloop from="#1 + offset#" to="#len(arguments.hex)#" index="i">
	    <cfif find(mid(arguments.hex, i + o, 1), '0123456789ABCDEFabcdef') EQ 0>
  		  <cfset arguments.hex = removeChars(arguments.hex, i + o, 1)>
    		<cfset o = o - 1>
    	</cfif>
		</cfloop>
		<cfif len(arguments.hex) EQ (3 + offset)>
			<cfset rVal = InputBaseN(mid(arguments.hex, 1 + offset, 1) & mid(arguments.hex, 1 + offset, 1), 16)>
			<cfset gVal = InputBaseN(mid(arguments.hex, 2 + offset, 1) & mid(arguments.hex, 2 + offset, 1), 16)>
			<cfset bVal = InputBaseN(mid(arguments.hex, 3 + offset, 1) & mid(arguments.hex, 3 + offset, 1), 16)> 
		<cfelse> 
			<cfset rVal = InputBaseN(mid(arguments.hex, 1 + offset, 2), 16)>
			<cfset gVal = InputBaseN(mid(arguments.hex, 3 + offset, 2), 16)>
			<cfset bVal = InputBaseN(mid(arguments.hex, 5 + offset, 2), 16)>
		</cfif>
		<cfset retVal = rVal & "," & gval & "," & bVal>
		<cfreturn retVal>
	</cffunction>

</cfcomponent>	



