<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approvalEngine.cfc
Author:				YMA
Date started:		11/07/2014

Description:		Functions and business logic for approval engine.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2015-05-12 			YMA 		added filter to prevent blank records without approvers being returned in query
2015-08-21          DAN         Case 445348 - apply filter criteria for approval engine being queried
2016-02-18	    YMA		(2015-11-27 by Yan, commited by RJT) P-PAN001 CR001 Updated approval engine to handle approver identified by integerflagdata
01/04/2016      DAN Case 448345 - fixes for approval engine to fitler out requests to match related entity account manager
2016-10-14      DAN PROD2016-2532/Case 451145 - approval engine fixes so business plans can be approved by users matching specified criteria
2016-12-29		SBW Case 453682 - Query fails when more than 2100 parameters.  Added workaround.
2017-01-10		WAB Case 453682/RT-127	rewrote getApprovalRequestsForPerson() as a Single piece of SQL rather than reading all records into memory and processing one by one
2017-02-27		WAB	RT-249 Following on from RT-127.  Panduit have a customisation which adds another column to getApprovalRequestsForPerson, so TFQO filters cannot be applied here
								Added option to delay applying TFQO filters.  Also needed to a select * from (..) where
Possible enhancements:


 --->
<cfcomponent output="false">

	<!--- Data Access Methods --->
	<cffunction name="getApprovalEngine" access="public" returnType="query" output="false">
		<cfset var getApprovalEngine = "">

		<cfquery name="getApprovalEngine" datasource="#application.siteDataSource#">
			select * from vApprovalEngine
		</cfquery>

		<cfreturn getApprovalEngine>
	</cffunction>

	<cffunction name="getApprovalEngineIDByEntityType" access="public" returntype="any" output="false">
		<cfargument name="entityTypeID">
		<cfargument name="entityID">

		<cfset var qApprovalEngine = queryNew('')>
		<cfset var result = "">

		<cfquery name="qApprovalEngine">
			SELECT approvalEngineID
			FROM approvalEngine
			WHERE entityTypeID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityTypeID#">
		</cfquery>

		<cfloop query="qApprovalEngine">
			<cfset var getFilterCriteriaForEntityTypeID = application.com.filterSelectAnyEntity.runFilterAgainstEntity(
							filterEntityID=qApprovalEngine.approvalEngineID,
							filterEntityTypeID=application.entityTypeID['approvalEngine'],
							relatedEntityID=arguments.entityID,
							relatedEntityTypeID=arguments.entityTypeID
							)>
			<cfif getFilterCriteriaForEntityTypeID.recordcount gt 0>
				<cfset result = listAppend(result,qApprovalEngine.approvalEngineID)>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<cffunction name="getApprovalEngineSummary" access="public" returnType="query" output="false">
		<cfargument name="approvalEngineID" required="false">
		<cfset var getApprovalEngineSummary = "">

		<cfquery name="getApprovalEngineSummary" datasource="#application.siteDataSource#">
			select * from vApprovalEngineSummary
			where 1=1
			<cfif structKeyExists(arguments,"approvalEngineID")>
				and approvalEngineID =  <cf_queryparam value="#arguments.approvalEngineID#" CFSQLTYPE="CF_SQL_Integer" >
			</cfif>

			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		</cfquery>

		<cfreturn getApprovalEngineSummary>
	</cffunction>

	<!--- 2015-11-27 YMA P-PAN001 CR001 Updated approval engine to handle approver identified by integerflagdata --->
	<!--- NJH 2015/01/05 - replaced friendlyname with label in query below --->
	<cffunction name="getApprovalEngineApprovers" access="public" returnType="query" output="false">
		<cfargument name="approvalEngineID" required="false">

		<cfset var getApprovalEngineApprovers = "">

		<cfquery name="getApprovalEngineApprovers" datasource="#application.siteDataSource#">
			select
				aea.approvalEngineID,
				aea.approvalEngineApproverID,
				aea.approverEntityID,
				case
					when st.entityName = 'person'
						then (select FirstName + ' ' + Lastname from person where personID = aea.approverEntityID)
					when st.entityName = 'usergroup'
						then (select name from usergroup where usergroupID = aea.approverEntityID)
					when st.entityName = 'flag'
						then (select f.name from flag f where f.flagID = aea.approverEntityID)
					when st.entityName = 'integerflagdata'
						then (select f.name from flag f where f.flagID = aea.approverEntityID)
				end as approverEntity,
				aea.approverLevel,
				aea.entityTypeID,
				st.label as approverType,
				aea.created,
				(select name from usergroup where usergroupID = aea.createdby) as createdBy,
				aea.lastupdated,
				(select name from usergroup where usergroupID = aea.lastupdatedby) as lastUpdatedBy
			from approvalEngineApprover aea
			join schematable st on aea.entityTypeID = st.entityTypeID
			<cfif structkeyexists(arguments,"approvalEngineID")>
				where aea.approvalEngineID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalEngineID#">
			</cfif>
			order by aea.approverLevel
		</cfquery>

		<cfreturn getApprovalEngineApprovers>
	</cffunction>

	<!--- 2015-11-27 YMA P-PAN001 CR001 Necessary to pass approvalEngineID to function to work out approver related to an integerflagdata data value --->
	<cffunction name="getApprovalEngineApproverPersonIDs" access="public" returnType="array" output="false">
		<cfargument name="approverEntityID" required="true">
		<cfargument name="approver" required="true">
		<cfargument name="approvalEngineID" required="true">
		<cfargument name="approvalEntityID" required="true">

		<cfset var approverPersonIDs = arrayNew(1)>
		<cfset var approversTaggedByFlag = "">
		<cfset var approversInUserGroup = "">
		<cfset var approvalEngineEntityType = "">
		<cfset var approvalEngineRelatedEntityDetail = queryNew("entityTable,uniqueKey")>

		<cfswitch expression="#approver.approverEntityType#">
			<cfcase value="UserGroup">
				<cfset approversInUserGroup = application.com.relayUserGroup.getUserGroupAllocation(UserGroupIDs=arguments.approverEntityID)>
				<cfloop query="#approversInUserGroup#">
					<cfset ArrayAppend(approverPersonIDs, approversInUserGroup.personID)>
				</cfloop>
			</cfcase>
			<cfcase value="Flag">
				<cfquery name="approversTaggedByFlag" datasource="#application.siteDataSource#">
					select bfd.entityID from booleanflagdata bfd
					join person p on bfd.entityID = p.personID
					where bfd.flagID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approverEntityID#">
				</cfquery>
				<cfloop query="#approversTaggedByFlag#">
					<cfset ArrayAppend(approverPersonIDs, approversTaggedByFlag.entityID)>
				</cfloop>
			</cfcase>
			<cfcase value="IntegerFlagData">
				<cfset approvalEngineEntityType = getApprovalEngineSummary(approvalEngineID=arguments.approvalEngineID).approvalEngineEntityType>
				<cfquery name="approvalEngineRelatedEntityDetail">
					SELECT
						OBJECT_NAME(fk.referenced_object_id) 'entityTable',
						c2.name 'uniqueKey'
					FROM
						sys.foreign_keys fk
					INNER JOIN
						sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
					INNER JOIN
						sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
					WHERE OBJECT_NAME(fk.parent_object_id) = '#approvalEngineEntityType#'
				</cfquery>
				<cfquery name="approversTaggedByFlag" datasource="#application.siteDataSource#">
					select ifd.data from integerflagdata ifd
						join flag f on ifd.flagID = f.flagID
						join #approvalEngineRelatedEntityDetail.entityTable# t on ifd.entityID = t.#approvalEngineRelatedEntityDetail.uniqueKey#
					where ifd.flagID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approverEntityID#">
						and ifd.entityID = (select #approvalEngineRelatedEntityDetail.uniqueKey# from #approvalEngineEntityType#
										where #application.entityType[application.entityTypeID[approvalEngineEntityType]].uniqueKey# = #arguments.approvalEntityID#)
				</cfquery>
				<cfloop query="#approversTaggedByFlag#">
					<cfset ArrayAppend(approverPersonIDs, approversTaggedByFlag.data)>
				</cfloop>
			</cfcase>
			<cfdefaultcase>
				<cfset approverPersonIDs[1] = arguments.approverEntityID>
			</cfdefaultcase>
		</cfswitch>

		<cfreturn approverPersonIDs>
	</cffunction>


    <cffunction name="getApprovalRequestsForPerson" access="public" returnType="query" output="false">
              <cfargument name="personID" required="true" type="numeric">
              <cfargument name="applyTFQOFilter" default="true" type="boolean" hint="For some customisations extra columns get added to this query at a later stage, so filtering has to be done later">
  
              <cfset var getDistinctEntityTypes = "">
              <cfset var qDistinctApproversWithRelatedEntity = "">
              <cfset var qForeignKeyName = "">
              <cfset var getApprovalRequests = "">
              <cfset var approverIDsForPerson = "">
              <cfset var getAllApprovalRequests = queryNew("approverID,approvalEngineID,approvalEngineEntityTypeID,approvalStatusRelatedEntityID,currentLevel,approvalstatusTypeText,approvalstatusTypeTextID,approvedStatusCreated,entityTypeName,entityName")>
			  <cfset  var translatedStatusSnippet = application.com.relayTranslations.getPhrasePointerQuerySnippetsForTextID (
                                                                             tableName = "approvalStatusType",
                                                                             Prefix = "approvalListing",
                                                                             textIDColumn = "approvalstatusTextID"
                                                                                     )>

        <!--- WAB 2017-01-10 Case 453682/RT-127 Major chanages 
			What we are trying to do is Bring back a query of all the approvals which a person is assigned to.
			Assignment can be done in a number of ways
				1. By PersonID
				2. By UserGroup
				3. By an integer flag on the location (usually) which the entity to be approved is related to				
                4. The Person is tagged as an approver using a boolean flag    
		
			The problem comes with Item 3 because we need to join out to the entityTable before we can work out whether the person has rights 
			Since each entity type needs a different join to get that locationid, we need to do a big union before we can even start working out rights

        --->
		 <cfquery name="getDistinctApprovalEngines" datasource="#application.siteDataSource#">
			select distinct approvalEngineID, approvalEngineEntityTypeID
			from vapprovalengine
			where approverID is not null <!--- 2015-05-12 YMA added filter to prevent blank records without approvers being returned in query --->
			</cfquery>

		 <cfquery name="getDistinctEntityTypes" dbType="query">
			select distinct approvalEngineEntityTypeID
			from getDistinctApprovalEngines
		 </cfquery>

		<!--- Drop temp tables (should only be there if query has broken half way, but very useful during dev and easier than using random names)--->	
		 <cfquery name="dropTempTables" >
			if object_id ('tempdb..##allApprovalRecords')	is not null
				drop table  ##allApprovalRecords
			if object_id ('tempdb..##personsApprovalRecords')	is not null			
				drop table  ##personsApprovalRecords
			if object_id ('tempdb..##translatedStatus')	is not null			
				drop table  ##translatedStatus

		 </cfquery>
		 
		 <!--- 
			now lets get the approval request details with entity details for those approval requests with an approver 
			WAB 2017-01-10 Alter to be a single query rather than lots of queries with some CF loops
		 --->
        <cfquery name="getApprovalRequests">

			select 	
					  approvalStatusTypeID
					,  approvalstatusTextID as approvalstatusTypeTextID
					, #translatedStatusSnippet.select# as approvalstatusTypeText
			into ##translatedStatus
			from 
				approvalStatusType
                #preserveSingleQuotes(translatedStatusSnippet.join)#

			<!--- Make temporary table of all the approval requests with, if necessary the foreignEntityID which is needed to do rights Item 3 mentioned above, also throw in the locationid for later use --->
            select * 
			into ##allApprovalRecords
			from (
            <cfloop query="getDistinctApprovalEngines">
                <cfset var tblName = "#application.entityType[approvalEngineEntityTypeID].TABLENAME#">
                 <!--- DAN 21/08/2015    Case 445348 - Get any filter criteria for the approval engine being queried --->
		        <cfset var getFilterCriteriaForEntity = application.com.filterselectanyentity.getFilterCriteriaForEntity(relatedEntityID=APPROVALENGINEID,relatedEntityTypeID=application.entityTypeID['approvalEngine'])>
                <!--- find out the name of the foreign entity key (for example locationId, organisationId, etc.) --->
                <!--- WAB 2017-01-25 Added order by key_index_id which I seem to have lost when converting this code  
                      Also noticed that the orignal query was returning the name of the referenced column rather than the parent column (and it is the parent table that we are dealing with),  
                      Wasn't a problem until we came across a table where the column names were different (location.convertedFromLeadID <--> lead.leadid)  (note that this column only cropped up because we were missing the order by)
                --->
                <cfquery name="qForeignKeyName" datasource="#application.siteDataSource#">
                    select c1.name as value from sys.foreign_keys fk INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
                    INNER JOIN sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
                    INNER JOIN  sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
                    where OBJECT_NAME(fk.parent_object_id) = '#tblName#' order by key_index_id 
                </cfquery>
                <cfquery name="hasLocationIDColumn" datasource="#application.siteDataSource#">
				select * from information_schema.columns where table_name = '#tblName#' and column_name = 'locationid'				
				</cfquery>

                select 
					  app.approvalEngineID
					, app.approverEntityType
					, app.approverEntityID
					, app.approvalEngineEntityTypeID
					, app.approvalStatusRelatedEntityID
					, #tblName#.#qForeignKeyName.value# as foreignEntityID <!--- include foreignEntityID so later we can filter on it --->
					, '#application.entityType[approvalEngineEntityTypeID].label#' as entityTypeName
					 <cfif hasLocationIDColumn.recordcount >
						,l.locationid as locationid
					<cfelse>
						, null as locationid
					</cfif>
                from 
					vapprovalengine app 
						join 
					#tblName# as #tblName# on app.approvalStatusRelatedEntityID = #tblName#.#application.entityType[approvalEngineEntityTypeID].UNIQUEKEY#
					<cfif hasLocationIDColumn.recordcount >
							join location l on #tblName#.locationID = l.locationID
					</cfif>
					
                    <!--- DAN 21/08/2015    Case 445348 - Apply any filter for the approval engine being queried --->
                    <cfif getFilterCriteriaForEntity.recordcount gt 0>
                        <cfloop query="getFilterCriteriaForEntity">
                            #replace(getFilterCriteriaForEntity.compiledSQL,"''","'","all")#
                        </cfloop>
                    </cfif>

                where 
					app.approverID is not null
					and app.currentLevel is not null


                <cfif currentrow neq getDistinctApprovalEngines.recordcount>
                    union
                </cfif>
            </cfloop>
                ) as _
                
				
			<!--- Filter down the above table to give table with only those items person has rights to  --->
			declare @personid int = <cfqueryparam cfsqltype="cf_sql_integer" value="#personID#">

			select * 
			into ##personsApprovalRecords
			from (
						<!--- Rights by Person --->
						select 
							app.approvalEngineID, app.approvalEngineEntityTypeID, app.approvalStatusRelatedEntityID, entityTypeName, locationid
						from 
							##allApprovalRecords  app
						where 
							approverEntityType = 'person' and approverEntityID = @personid

						union

						<!--- Rights by User Group --->
						select 
							app.approvalEngineID, app.approvalEngineEntityTypeID, app.approvalStatusRelatedEntityID, entityTypeName, locationid
						from 
							##allApprovalRecords  app
								inner join 
							rightsGroup on approverEntityID = usergroupid 
						where 
							approverEntityType = 'usergroup' and rightsGroup.personid = @personid

						union

						<!--- Rights by Flag on Location (or other related entity) --->
						select 
							app.approvalEngineID, app.approvalEngineEntityTypeID, app.approvalStatusRelatedEntityID, entityTypeName, locationid
						from 
							##allApprovalRecords  app
								inner join 
							integerflagdata on flagid = approverEntityID 
						where 
							approverEntityType = 'integerflagdata' and entityid = app.foreignEntityID and data = @personid

						union

						<!--- Rights by Person Flag --->
						select 
							app.approvalEngineID, app.approvalEngineEntityTypeID, app.approvalStatusRelatedEntityID, entityTypeName, locationid
						from 
							##allApprovalRecords  app
								inner join 
							booleanflagdata on flagid = approverEntityID 
						where 
							approverEntityType = 'flag' and entityid = @personid
					)  as _
			

			<!--- Now another loop over entities to get names (perhaps could be got more efficiently from vEntityName, although Panduit custom version wants country as well).  
				Could also be combined into the very first loop
			 --->
					select * from 
					(

						select 
							  p.approvalEngineEntityTypeID
							, p.approvalStatusRelatedEntityID
							, latestApprovalRecord.APPROVERID
							,latestApprovalRecord.APPROVALENGINEID
							, latestApprovalRecord.APPROVEDSTATUSCREATED
							, latestApprovalRecord.CURRENTLEVEL
							, status.approvalstatusTypeText
							, status.approvalstatusTypeTextID
                            , entityTypeName
                            , e.name  + case when entityTypeName = 'person' then l.sitename    else '' end as entityname
							, l.sitename
							, c.countrydescription as country
                        from 
							##personsApprovalRecords p
								inner join
									(select *
									 from
										 (select 
											approvalEngineID
											, approvalEngineApproverID as approverid
											, entityid as approvalStatusRelatedEntityID
											, approvalengineapproverid
											, created as APPROVEDSTATUSCREATED
											,currentlevel
											, approvalstatustypeID
										 ,ROW_NUMBER () over (partition By approvalEngineID, EntityID order by Created desc) as _rank
										  from 
										 approvalstatus) _
									 where _rank = 1) as latestApprovalRecord 
									 ON
										latestApprovalRecord.approvalEngineID = p.approvalEngineID and latestApprovalRecord.approvalStatusRelatedEntityID = p.approvalStatusRelatedEntityID 
								join 
							##translatedStatus status on status.approvalStatusTypeID = latestApprovalRecord.approvalStatusTypeID 
								left join		
							vEntityName e on e.entityTypeID = p.approvalEngineEntityTypeID and e.entityid = p.approvalStatusRelatedEntityID	
								left join
							location l on p.locationid = l.locationid
								left join
							country c on c.countryid = l.countryid	
                         ) as _          
                        where 1=1
						<cfif applyTFQOFilter>
							<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
						</cfif>
				
				drop table  ##allApprovalRecords
				drop table  ##personsApprovalRecords
				drop table  ##translatedStatus

				
	        </cfquery>


              <cfreturn getApprovalRequests>
       </cffunction>


	<!--- 2015-11-27 YMA P-PAN001 CR001 Updated approval engine to handle approver identified by integerflagdata --->
	<cffunction name="getEntitiesForEntityType" access="remote">
		<cfargument name="entityTypeID" required="false">
		<cfargument name="approvalEngineID" required="false">

		<cfset var getEntitiesForEntityType = queryNew("display,value")>
		<cfset var approvalEngineEntityType = "">
		<cfset var approvalEngineRelatedEntityTypeID = queryNew("approvalEngineRelatedEntityTypeID")>
		<cfif entityTypeID neq "">
			<cfswitch expression="#application.entitytype[arguments.entityTypeID].tablename#">
				<cfcase value="person">
					<cfquery name="getEntitiesForEntityType" datasource="#application.siteDataSource#">
						select distinct p.FirstName + ' ' + p.Lastname as display, p.personID as value from person p
						join rightsgroup rg on p.personID = rg.personID
						join rights r on rg.usergroupID = r.usergroupID
						join SecurityType st on r.securityTypeID = st.securityTypeID
						and st.shortname = 'approvalTask'
						order by p.FirstName + ' ' + p.Lastname asc
					</cfquery>
				</cfcase>
				<cfcase value="usergroup">
					<cfquery name="getEntitiesForEntityType" datasource="#application.siteDataSource#">
						select ug.name as display, ug.usergroupID as value from usergroup ug
						join rights r on ug.usergroupID = r.usergroupID
						join SecurityType st on r.securityTypeID = st.securityTypeID
						and st.shortname = 'approvalTask'
						where ug.personID is null
						order by ug.name asc
					</cfquery>
				</cfcase>
				<cfcase value="flag">
					<cfquery name="getEntitiesForEntityType" datasource="#application.siteDataSource#">
						select distinct f.name as display, f.flagID as value from person p
						join rightsgroup rg on p.personID = rg.personID
						join rights r on rg.usergroupID = r.usergroupID
						join SecurityType st on r.securityTypeID = st.securityTypeID
						join booleanflagdata bfd on p.personID = bfd.entityID
						join flag f on bfd.flagID = f.flagID
						join flaggroup fg on f.flaggroupID = fg.flaggroupID
						where fg.entitytypeID = 0
						and st.shortname = 'approvalTask'
						order by f.name asc
					</cfquery>
				</cfcase>
				<cfcase value="integerFlagData">
					<cfset approvalEngineEntityType = getApprovalEngineSummary(approvalEngineID=arguments.approvalEngineID).approvalEngineEntityType>
					<cfquery name="approvalEngineRelatedEntityTypeID">
						SELECT
							OBJECT_NAME(fk.referenced_object_id) as 'approvalEngineRelatedEntityTypeID'
						FROM
							sys.foreign_keys fk
						INNER JOIN
							sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
						WHERE OBJECT_NAME(fk.parent_object_id) = '#approvalEngineEntityType#'
					</cfquery>
					<cfquery name="getEntitiesForEntityType" datasource="#application.siteDataSource#">
						select distinct f.name as display, f.flagID as value
							from flag f
							join flaggroup fg on f.flagGroupID = fg.flagGroupID and f.useValidValues = 1
								and f.linksToEntityTypeID = 0 and fg.entityTypeID = #application.entitytypeID[approvalEngineRelatedEntityTypeID.approvalEngineRelatedEntityTypeID]#
							join flagtype ft on fg.flagtypeID = ft.flagtypeID and ft.datatablefullname = 'integerFlagData'
						order by f.name asc
					</cfquery>
				</cfcase>
				<cfdefaultcase>
				</cfdefaultcase>
			</cfswitch>
		</cfif>

		<cfreturn getEntitiesForEntityType>

	</cffunction>

	<!--- get a struct detailing approvalEngine approver level, and approvers in that level. --->
	<cffunction name="getApprovalEngineApproverDetails" access="public" returnType="struct" output="false">
		<cfargument name="approvalEngineID" required="true">
		<cfargument name="approvalStatusRelatedEntityID" required="false" type="numeric">

		<cfset var getApprovalEngineApproverDetails = structNew()>
		<cfset var getApprovalEngineStatusSummary = "">
		<cfset var getPreviousApprovers = "">
		<cfset var getNextApprovers = "">
		<cfset var triggeringCriteria = "">
		<cfset var isNextLevelRequested = "">

		<cfquery name="getApprovalEngineStatusSummary" datasource="#application.siteDataSource#">
			select
				vAES.approvalEngineID,
				vAES.title,
				vAES.approvalEngineEntityTypeID,
				vAES.approvalEngineEntityType,
				vAES.approvalEngineRejectedWorkflowID,
				vAES.approvalEngineRejectedWorkflow,
				vAES.approvalEngineApprovedWorkflowID,
				vAES.approvalEngineApprovedWorkflow,
				vAES.approvalEngineRevisionWorkflowID,
				vAES.approvalEngineRevisionWorkflow,
				vAES.approvalRequestEmailID,
				vAES.approvalRequestEmail,
				vAES.maxLevel,
				isnull(max(aSt.currentLevel), 0) as currentLevel,
				#arguments.approvalStatusRelatedEntityID# as approvalStatusRelatedEntityID
			from vApprovalEngineSummary vAES
			LEFT OUTER JOIN (
							select aSt.approvalEngineID, aST.entityID, aST.currentLevel from approvalStatus aSt
			INNER JOIN approvalStatusType as aStT on aSt.approvalStatusTypeID = aStT.approvalStatusTypeID and aStT.approvalStatusTextID != 'approvalRequested'
							) as aSt on aSt.approvalEngineID = vAES.approvalEngineID and aST.entityID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalStatusRelatedEntityID#">
			WHERE vAES.approvalEngineID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalEngineID#">
			group by
				vAES.approvalEngineID,vAES.title,vAES.approvalEngineEntityTypeID,vAES.approvalEngineEntityType,vAES.approvalEngineRejectedWorkflowID,
				vAES.approvalEngineRejectedWorkflow,vAES.approvalEngineApprovedWorkflowID,vAES.maxLevel,
				vAES.approvalEngineApprovedWorkflow,vAES.approvalRequestEmailID,vAES.approvalRequestEmail,
				vAES.approvalEngineRevisionWorkflowID,vAES.approvalEngineRevisionWorkflow
		</cfquery>
		<cfset getApprovalEngineApproverDetails = application.com.structureFunctions.queryRowToStruct(query=getApprovalEngineStatusSummary, row=1)>

		<cfloop query="getApprovalEngineStatusSummary">
			<!--- Check if approval engine triggering criteria are met. --->
			<cfset triggeringCriteria = application.com.filterSelectAnyEntity.runFilterAgainstEntity(
									filterEntityTypeID= application.entityTypeID["approvalEngine"],
									filterEntityID=arguments.approvalEngineID,
									relatedEntityID=arguments.approvalStatusRelatedEntityID,
									relatedEntityTypeID=getApprovalEngineStatusSummary.APPROVALENGINEENTITYTYPEID
									)>
			<cfif triggeringCriteria.recordcount gt 0>
				<cfset getApprovalEngineApproverDetails['approvalCriteriaMet'] = true>
			<cfelse>
				<cfset getApprovalEngineApproverDetails['approvalCriteriaMet'] = false>
			</cfif>

			<!--- check if next level approval status is approvalRequested. --->
			<cfquery name="isNextLevelRequested" datasource="#application.siteDataSource#">
				select 1
				from approvalStatus aSt
				join approvalStatusType aStT on aSt.approvalStatusTypeID = aStT.approvalStatusTypeID
											and aStT.approvalStatusTextID = 'approvalRequested'
											and aSt.currentLevel = <cfqueryparam cfsqltype="cf_sql_integer" value="#getApprovalEngineStatusSummary.currentLevel#"> + 1
			</cfquery>
			<cfif isNextLevelRequested.recordcount gt 0>
				<cfset getApprovalEngineApproverDetails['approvalRequested'] = true>
			<cfelse>
				<cfset getApprovalEngineApproverDetails['approvalRequested'] = false>
			</cfif>
		</cfloop>


		<!--- previous approvers --->
		<cfif structKeyExists(arguments,"approvalStatusRelatedEntityID")>
			<cfquery name="getPreviousApprovers" datasource="#application.siteDataSource#">
				select
					approverID, approverLevel, approverEntityID, approverEntity,approverEntityTypeID, approverEntityType,
					approvalStatusID, currentLevel, approvalStatusTypeID, approvalStatusTypeTextID, approvalStatusRelatedEntityID, approvedStatusCreated, reason
				from vApprovalEngine
				where approvalStatusRelatedEntityID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalStatusRelatedEntityID#">
				and approvalEngineID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalEngineID#">
				order by approverLevel asc
			</cfquery>
			<cfset getApprovalEngineApproverDetails.currentStatusID = getPreviousApprovers.approvalStatusTypeID[getPreviousApprovers.recordcount]>
			<cfset getApprovalEngineApproverDetails.currentStatusTextID = getPreviousApprovers.approvalStatusTypeTextID[getPreviousApprovers.recordcount]>
			<cfset getApprovalEngineApproverDetails.previousLevelApprovers = application.com.structureFunctions.queryToTwoDimensionalStruct(query=getPreviousApprovers, key1='approverLevel', key2='approverId')>
		</cfif>

		<!--- next approvers --->
		<cfloop from="#getApprovalEngineStatusSummary.currentLevel+1#" to="#getApprovalEngineStatusSummary.maxLevel#" index="i">
			<cfquery name="getNextApprovers" datasource="#application.siteDataSource#">
				select distinct
					approverID,	approverLevel,	approverEntity,	approverEntityID,
					approverEntityType,	approverEntityTypeID
				from vApprovalEngine
				where approverLevel = <cfqueryparam cfsqltype="cf_sql_integer" value="#i#">
				and approvalEngineID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalEngineID#">
				order by approverLevel asc
			</cfquery>
			<cfset getApprovalEngineApproverDetails.nextLevelApprovers[#i#] = application.com.structureFunctions.QueryToArrayOfStructures(query=getNextApprovers)>
		</cfloop>

		<cfreturn getApprovalEngineApproverDetails>
	</cffunction>

	<!--- 2015-11-27 YMA P-PAN001 CR001 Necessary to pass approvalEngineID to getApprovalEngineApproverPersonIDs function to work out approver related to an integerflagdata data value --->
	<cffunction name="sendApprovalRequestEmails" access="public" returnType="string" output="false">
		<cfargument name="approvalEngineApproverDetails" required="true">
		<cfargument name="tierToRequestApproval" required="true">
		<cfargument name="approvalStatusRelatedEntityID" required="true">
		<cfargument name="approvalEngineID" required="true" type="numeric">

		<cfset var result = structNew()>
		<cfset var errorStruct = structNew()>
		<cfset var approver = "">
		<cfset var approverPersonID = "">
		<cfset var filterCriteria = "">
		<cfset var emailSent = "false">

		<!--- loop through approvers who are being sent approval requests --->
		<cfloop array=#arguments.approvalEngineApproverDetails.nextLevelApprovers[arguments.tierToRequestApproval]# index="approver">


			<cfset filterCriteria = application.com.filterSelectAnyEntity.runFilterAgainstEntity(
											filterEntityTypeID= application.entityTypeID["approvalEngineApprover"],
											filterEntityID=approver.approverID,
											relatedEntityID=arguments.approvalStatusRelatedEntityID,
											relatedEntityTypeID=arguments.approvalEngineApproverDetails.ApprovalEngineEntityTypeID
											)>

			<cfif filterCriteria.recordcount gt 0>
				<!--- 2015-11-27 YMA P-PAN001 CR001 Necessary to pass approvalEngineID to getApprovalEngineApproverPersonIDs function to work out approver related to an integerflagdata data value --->
				<cfloop array=#getApprovalEngineApproverPersonIDs(approverEntityID=approver.approverEntityID,approver=approver,approvalEngineID=arguments.approvalEngineID,approvalEntityID=arguments.approvalStatusRelatedEntityID)# index="approverPersonID">
					<!--- send emails --->
					<cfset var approvalEngineEntityStructure = application.entityType[arguments.approvalEngineApproverDetails.ApprovalEngineEntityTypeID]>

					<!--- check if out of office flag exists --->
					<cfif application.com.flag.doesFlagExist("outOfOffice")>
						<!--- check if approverPersonID is out of office --->
						<cfif application.com.flag.getFlagDataForPerson(flagID="outOfOffice",personID=approverPersonID).isset eq 1>
							<!--- check if user has delegated approvals --->
							<cfif application.com.flag.getFlagDataForPerson(flagID="outOfOfficeDelegateTo",personID=approverPersonID).data neq "">
								<!--- Is out of office: change approverPersonID to be that of the delegated person and CC original approver. --->
									<cfset approverPersonID=application.com.flag.getFlagDataForPerson(flagID="outOfOfficeDelegateTo",personID=approverPersonID).data>
							</cfif>
						</cfif>
					</cfif>

					<!--- WAB 2016-01-26 CASE 447755.  Merge not working correctly.  Removed entityTypeID item from merge struct, it was confusing the merge system --->
					<cfset var mergeStruct = {	#approvalEngineEntityStructure.uniqueKey#=approvalStatusRelatedEntityID,
											approvalStatusRelatedEntityID=approvalStatusRelatedEntityID,
											approverID=#approver.approverID#,
											approvalEngineID=arguments.approvalEngineID,
											tierToRequestApproval=arguments.tierToRequestApproval,
											encryptedApprovalLink=application.com.security.encryptURL(application.com.relaycurrentsite.getReciprocalExternalProtocolAndDomain()&'/?'&application.com.relayelementtree.createSEIDQueryString(elementid='approvalEngine',personID=approverPersonID,linktype="secure")&'&approvalEngineID='&arguments.approvalEngineID&'&entityTypeID='&arguments.approvalEngineApproverDetails.ApprovalEngineEntityTypeID&'&approvalStatusRelatedEntityID='&approvalStatusRelatedEntityID&'&approverID='&#approver.approverID#)
										}>

					<cfset saveApprovalDecision(responseForm=approver,
												entityTypeID=arguments.approvalEngineApproverDetails.ApprovalEngineEntityTypeID,
												approvalStatusRelatedEntityID=approvalStatusRelatedEntityID,
												approvalEngineID=arguments.approvalEngineID,
												level=arguments.tierToRequestApproval,
												approvalDecision='approvalRequested'
												)>

					<cfif listfind("0,1,2",arguments.approvalEngineApproverDetails.ApprovalEngineEntityTypeID)>
						<cfset result = application.com.email.sendEmail(recipientAlternativePersonID=approverPersonID,personID=request.relaycurrentuser.personID,emailTextID=arguments.approvalEngineApproverDetails.approvalRequestEmail,mergeStruct=mergeStruct)>
					<cfelse>
						<cfset result = application.com.email.sendEmail(personID=approverPersonID,emailTextID=arguments.approvalEngineApproverDetails.approvalRequestEmail,mergeStruct=mergeStruct)>
					</cfif>
					<cfif structKeyExists(arguments.approvalEngineApproverDetails,"approvalCopyRequestEmail") and arguments.approvalEngineApproverDetails.approvalCopyRequestEmail neq "">
						<cfset result &= application.com.email.sendEmail(personID=approverPersonID,emailTextID=arguments.approvalEngineApproverDetails.approvalCopyRequestEmail,mergeStruct=mergeStruct)>
					</cfif>
					<cfset emailSent = true>
				</cfloop>
			</cfif>
		</cfloop>
			<cfif not emailSent>
				<cfset errorStruct.isOk = false>
				<cfset errorStruct.message = "no valid approver to send to.">
				<cfset errorStruct.errorCode = "approverError">
				<cfset errorStruct.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayApprover Error",Severity="Approval Engine",catch=errorStruct,WarningStructure=arguments)>
				<cfset result = "{'isOk':'false','errorID':'#errorStruct.errorID#'}">
			</cfif>

		<cfreturn result>
	</cffunction>

	<!--- Business Logic / Processing / Functional handlers --->
	<cffunction name="approvalRuleHandler" access="public" output="false" returntype="any" hint="Checks if an entity has approval rules.  On request mode, sends approval emails. On response mode, triggers desired actions for an approver.">
		<cfargument name="mode" type="string" required="true" hint="when called from an entity, or on an entity submission this should be request / when an approver hits this page mode should be respond">
		<cfargument name="entityTypeID" type="numeric" required="false" hint="entityTypeID of the calling object">
		<cfargument name="approvalStatusRelatedEntityID"required="false" default="101" hint="entity ID of the record just submitted"> <!--- this is a hardcoded example of opportunityID 101--->
		<cfargument name="responseForm" type="struct" required="false" hint="Form detailing approvers response">
		<cfargument name="approvalEngineID" required="false" hint="Form detailing approvers response">

		<cfset var result=structNew()>
		<!--- Check if the entity has approval rules to run --->
		<cfif structKeyExists(arguments,"approvalEngineID") and arguments.approvalEngineID neq "">
			<cfset local.approvalEngineID = arguments.approvalEngineID>
		<cfelse>
			<cfset local.approvalEngineID = getApprovalEngineIDByEntityType(entityTypeID=arguments.entityTypeID,entityID=arguments.approvalStatusRelatedEntityID)>
		</cfif>

		<cfif listLen(local.approvalEngineID) GT 0>
			<cfset var approvalEngine = "">
			<cfloop list="#local.approvalEngineID#" index="approvalEngine">
				<!--- If current entity has approval rules to run, then do the following --->
				<!--- First get the approver details --->
				<cfset local.approvalEngineApproverDetails = getApprovalEngineApproverDetails(
															approvalEngineID				= approvalEngine,
															approvalStatusRelatedEntityID	= arguments.approvalStatusRelatedEntityID
				)>
				<cfset local.currentApprovedTier = local.approvalEngineApproverDetails.currentLevel>
				<cfset local.finalApprovalTier = local.approvalEngineApproverDetails.maxLevel>
				<cfset local.tierToRequestApproval = local.currentApprovedTier + 1>

				<cfswitch expression="#arguments.mode#">
					<!--- SEND APPROVAL REQUEST EMAIL TO APPROVER --->
					<cfcase value="request">
					<!--- which tier to send to --->
							<!--- Check if approval engine triggering criteria are met. --->
							<cfif local.approvalEngineApproverDetails.approvalCriteriaMet>
								<!--- Check if the tier requested is not over the maximum tier --->
								<cfif local.tierToRequestApproval lte local.finalApprovalTier>

									<!--- check the approval request has not already been sent or it was revisionNeeded --->
									<cfif local.approvalEngineApproverDetails.currentStatusTextID eq '' or local.approvalEngineApproverDetails.currentStatusTextID eq 'revisionNeeded'>
										<!--- get approvers and send them the email --->
										<cfset result = sendApprovalRequestEmails(
															approvalEngineApproverDetails 	= local.approvalEngineApproverDetails,
															tierToRequestApproval			= local.tierToRequestApproval,
															approvalStatusRelatedEntityID	= arguments.approvalStatusRelatedEntityID,
															approvalEngineID 				= approvalEngine
										)>
										<!--- <cfreturn result.result> --->
									</cfif>

									<!--- if the status was revisionNeeded then the status should be set back to approvalRequested --->
									<cfif local.approvalEngineApproverDetails.currentStatusTextID eq 'revisionNeeded'>
										<cfset approvalRuleHandler(
															mode="respond",
															entityTypeID=local.approvalEngineApproverDetails.APPROVALSTATUSRELATEDENTITYID,
															approvalStatusRelatedEntityID=local.approvalEngineApproverDetails.APPROVALENGINEENTITYTYPEID
															)>
									</cfif>

								<cfelse> <!--- final approval has already been made --->
									<!--- route them into the approved workflow or rejected workflow depending on last approval result --->
								</cfif>
							</cfif>
					</cfcase>

					<!--- APPROVER APPROVAL RESPONSE --->
					<cfcase value="respond">
						<!--- digest responseForm, save it to the DB then if approve re-fire this function in request mode. --->

						<cfset result.level = local.tierToRequestApproval>
						<cfif responseForm.approvaldecision eq 1><!--- approved --->
							<cfset result.approvalStatus = "partialyApproved">
							<cfset result.level = result.level>
							<cfif local.tierToRequestApproval eq local.finalApprovalTier>
								<cfset result.approvalStatus = "approved">
							</cfif>
						<cfelseif responseForm.approvaldecision eq -1>
							<cfset result.approvalStatus = "revisionNeeded">
							<cfset result.level = result.level - 1>
						<cfelse><!--- rejected --->
							<cfset result.approvalStatus = "rejected">
						</cfif>
						<cfset result.updateResult = saveApprovalDecision(
														responseForm=responseForm,
														entityTypeID=entityTypeID,
														approvalStatusRelatedEntityID=approvalStatusRelatedEntityID,
														approvalEngineID=approvalEngine,
														level=result.level,
														approvalDecision=result.approvalStatus
														)>

						<cfset result.approvaldetails = getApprovalEngineApproverDetails(
															approvalEngineID				= approvalEngine,
															approvalStatusRelatedEntityID	= arguments.approvalStatusRelatedEntityID
															)>

						<!--- if reject then update status and enter rejected flow  --->
						<cfswitch expression="#result.approvalStatus#">
							<cfcase value="rejected">
								<cfset result.workflowID = local.approvalEngineApproverDetails.approvalEngineRejectedWorkflowID>
							</cfcase>
							<cfcase value="revisionNeeded">
								<cfset result.workflowID = local.approvalEngineApproverDetails.approvalEngineRevisionWorkflowID>
							</cfcase>
							<cfcase value="partialyApproved">
								<!--- email next level approver. --->
									<cfset result.email = sendApprovalRequestEmails(
														approvalEngineApproverDetails 	= local.approvalEngineApproverDetails,
														tierToRequestApproval			= result.approvaldetails.currentLevel + 1,
														approvalStatusRelatedEntityID=arguments.approvalStatusRelatedEntityID,
														approvalEngineID 				= approvalEngine
												)>
							</cfcase>
							<cfcase value="approved">
								<cfset result.workflowID = local.approvalEngineApproverDetails.approvalEngineApprovedWorkflowID>
							</cfcase>
						</cfswitch>
					</cfcase>
				</cfswitch>
			</cfloop>
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="checkDupeApprovalDecision" access="public" output="false" returntype="boolean">
		<cfargument name="entityID" required="true" hint="entity ID of the record just submitted">
		<cfargument name="entityTypeID" required="true" hint="entity Type ID of the record just submitted">
		<cfargument name="approverID" required="true">
		<cfargument name="approvalEngineID" required="true">

		<cfset var qApprovalStatus = queryNew('')>
		<cfset local.bCanApprove = false>
		<cfset local.currentLevel = val(getApprovalEngineApproverDetails(approvalEngineID=arguments.approvalEngineID, approvalStatusRelatedEntityID=arguments.entityID).currentLevel)>
		<cfset var qApproverLevels = "">

		<cfquery name="qApproverLevels">
			select approverLevel
			FROM [approvalEngineApprover]
			where approvalEngineApproverID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.approverID#"> AND
					approvalEngineID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.approvalEngineID#">
		</cfquery>

		<cfquery name="qApprovalStatus">
			SELECT ast.approvalStatusID, ast.currentLevel, astt.approvalStatusTextID
			FROM approvalStatus ast
			JOIN approvalStatusType astt on ast.approvalStatusTypeID = astt.approvalStatusTypeID
			WHERE ast.entityID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
				AND ast.approvalEngineApproverID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.approverID#">
				AND ast.currentLevel = <cf_queryparam cfsqltype="cf_sql_integer" value="#local.currentLevel#">
				AND astt.approvalStatusTextID != 'approvalRequested'
		</cfquery>
		<cfif qApproverLevels.recordCount GT 0>
			<cfset local.validApprovalLevels = valueList(qApproverLevels.approverLevel)>
		<cfelse>
			<cfset local.validApprovalLevels = 0>
		</cfif>
		<cfset local.nextLevel = local.currentLevel + 1>

		<cfif qApprovalStatus.recordCount EQ 0>
			<cfif ListFindNoCase(local.validApprovalLevels,nextLevel) NEQ 0>
				<cfset local.bCanApprove = true>
			</cfif>
		<cfelse>
			<cfset local.qApprovalStatusStruct = application.com.structureFunctions.queryToStruct(query=qApprovalStatus,key="currentLevel",columns="approvalStatusTextID")>

			<cfif local.qApprovalStatusStruct[local.currentLevel].approvalStatusTextID eq "revisionNeeded">
				<cfset local.bCanApprove = true>
			</cfif>
		</cfif>

		<cfreturn local.bCanApprove>
	</cffunction>

	<!--- Data Access Methods --->
	<cffunction name="saveApprovalDecision" access="public" returnType="struct" output="false">
		<cfargument name="responseForm" type="struct" required="true" hint="Form detailing approvers response">
		<cfargument name="entityTypeID" type="numeric" required="true" hint="entityTypeID of the calling object">
		<cfargument name="approvalStatusRelatedEntityID" required="true" default="101" hint="entity ID of the record just submitted"> <!--- this is a hardcoded example of opportunityID 101--->
		<cfargument name="approvalEngineID" type="numeric" required="true" hint="the approval engine ID">
		<cfargument name="level" type="numeric" required="true" hint="the level approved">
		<cfargument name="approvalDecision" type="string" required="false" hint="Approval Decision">

		<cfset var result = {isOk=true,message="",errorCode="",approvalStatusID=0}>
		<cfset var insertApprovalDecision = "">
		<cftry>
			<cf_transaction action="begin">

			<cfquery name="insertApprovalDecision" datasource="#application.siteDataSource#">
				insert into approvalStatus
				values(
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalEngineID#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.level#">,
					(select approvalStatusTypeID from approvalStatusType where approvalStatusTextID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.approvalDecision#">),
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.approvalStatusRelatedEntityID#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.responseForm.approverID#">,
					<cfif structKeyExists(arguments.responseForm,"reason")>
						<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.responseForm.reason#">,
					<cfelse>
						null,
					</cfif>
					getdate())
				select scope_identity() as insertedID
			</cfquery>

			<cfset result.entityID=insertApprovalDecision.insertedID>
			<cfset result.errorCode = "ENTITY_INSERTED">

			<cf_transaction action="commit"/>

			</cf_transaction>
			<cfcatch>
				<cf_transaction action="rollback"/>

				<cfset result.isOK = false>
				<cfset result.errorCode = "INSERT_ERROR">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>


		<cfreturn result>
	</cffunction>

</cfcomponent>