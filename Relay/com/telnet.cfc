<!--- �Relayware. All Rights Reserved 2014 --->

<cfcomponent displayname="componentName" hint="componentFunction">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="openTelnet" hint="Opens a telnet session to the specified remote address">
		<!--- <cfargument name="remoteHost" type="string" required="true"> --->
		
		<cfobject type="COM" name="telnetObj" class="Intrafoundation.TCPClient" action="CREATE">
		
		<cfset this.TelnetObj = telnetObj.Open("#this.remoteHost#","23")>
	
		<!--- <cfreturn functionName> --->
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="CheckCMD" access="public" returntype="boolean" hint="This will return false if a command has been blocked, else return true">
		<cfargument name="vCmd" type="string" required="yes">
		<cfargument name="dSource" type="string" required="yes">
		<cfargument name="vEquipmentID" type="string" required="yes">
		
		<cfset vCmdAllow=true>
		<cfif vCmd neq "">
			<cfquery name="checkType" datasource="#dSource#" maxrows="1">
				select blockOnlyCommandsSpecified
				from Equipment e 
				INNER JOIN equipmentType et ON et.equipmentTypeID=e.equipmentTypeID
				where e.EquipmentID =  <cf_queryparam value="#val(vEquipmentID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>	
			<cfquery name="checkCommand" datasource="#dSource#" maxrows="1">
				select 1
				from Commands c
				inner join EquipmentTypeCommands etc ON etc.CommandID=c.CommandID
				inner join Equipment e ON etc.equipmentTypeID=e.equipmentTypeID
				where c.commandstring =  <cf_queryparam value="#trim(lcase(cmd))#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				and e.EquipmentID =  <cf_queryparam value="#val(vEquipmentID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>		

			<cfif checkType.blockOnlyCommandsSpecified eq "1">
				<!--- this equipment type will block only specified commands --->
				<cfif checkCommand.recordcount eq 1>
					<!-- command entered is blocked for this equipment type --->
					<cfset vCmdAllow=false>
					<cfset temp=setVariable("session.msgToOut#vequipmentID#","<p style=font: bold; color: Red;>" & cmd & " is a blocked command.</p>")>
				</cfif>
			<cfelse>
				<!--- this equipment type will block all commands unless specified --->
				<cfif checkCommand.recordcount eq "0">
					<!-- command entered is blocked for this equipment type --->
					<cfset vCmdAllow=false>
					<cfset temp=setVariable("session.msgToOut#vequipmentID#","<p style=font: bold; color: Red;>" & cmd & " is a blocked command.</p>")>
				</cfif>		
			</cfif>
		</cfif>		
		
		<cfreturn vCmdAllow>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Replace bad chars coming back from the remote machine        
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="CheckString" access="public" returntype="string" Hint="Replaces single or paired replacement strings.">
		<cfargument name="vThisString" type="string" required="yes">
		<cfargument name="vFinders" type="string" required="yes">
		<cfargument name="vReplacers" type="string" required="yes">
		
		<cfset finder = ListToArray(vFinders,"|")>
		<cfset replacer = ListToArray(vReplacers,"|")>
		
		<cfset instring = vThisString>
		
		<cfloop from="1" to="#ArrayLen(finder)#" index="item">
			<cfset instringpasscount = 0>
			<cfloop condition="Find(finder[#item#],instring) gt 0">
				<cfif listlen(replacer[#item#]) gt 1 and instringpasscount IS 1>
					<cfset instring = Replace(instring, finder[#item#], Listlast(replacer[#item#]) , "ONE")>
					<cfset instringpasscount = 0>
				<cfelse>
					<cfset instring = Replace(instring, finder[#item#], Listfirst(replacer[#item#]) , "ONE")>
					<cfset instringpasscount = 1>
				</cfif>
			</cfloop>
		</cfloop>
		<CFRETURN instring>
	</cffunction>

	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="telnetConnectionErrorText" 
		hint="Returns the error text for a given error code for Distinct Telnet object.">
		<cfargument name="errCode" type="string" required="yes">
		<cfswitch expression="#trim(errCode)#">
			<cfcase value="0"><cfset errText="The connection was successful."></cfcase>
			<cfcase value="1"><cfset errText="License error"></cfcase>
			<cfcase value="2"><cfset errText="General socket error."></cfcase>
			<cfcase value="3"><cfset errText="You are trying to receive data but you are not currently connected to the Telnet Server."></cfcase>
			<cfcase value="5"><cfset errText="A timeout error occurred while connecting or waiting for a prompt."></cfcase>
			<cfcase value="6"><cfset errText="Internal Telnet error"></cfcase> 
			<cfcase value="7"><cfset errText="The remote telnet host name is not defined."></cfcase>
			<cfcase value="8"><cfset errText="A terminal type has not been defined."></cfcase>
			<cfcase value="10"><cfset errText="You set the user name but did not specify a login prompt."></cfcase>
			<cfcase value="11"><cfset errText="You set the password but the password prompt is missing."></cfcase>
			<cfcase value="12"><cfset errText="The file to place the received data in, could not be opened."></cfcase>
			<cfcase value="13"><cfset errText="The file to place the received data in, could not be written to."></cfcase>			<cfcase value="14"><cfset errText="The file that is to contain the received data could not be created."></cfcase> 
			<cfcase value="15"><cfset errText="The hostname or IP address is invalid."></cfcase>
			<cfcase value="16"><cfset errText="The maximum number of concurrent Telnet connections has been reached."></cfcase>
			<cfdefaultcase><cfset errText = "Error code " & errCode & "  - Reason unknown"></cfdefaultcase>
		</cfswitch>
		<cfreturn errText>
	</cffunction>

</cfcomponent>