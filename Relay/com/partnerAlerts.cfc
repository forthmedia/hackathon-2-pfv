<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent hint="Gets sales tasks summary including pending renewals and approvals" displayname="Sales tasks summary">

<!--- 
	these functions have been moved to pandauserfiles and could be removed from here; this file must exist cos the userfiles version extends it 
			
	<cffunction name="getApprovals" output="yes" returntype="string" hint="Returns number of approvals">
		<cfset qApprovals = application.com.oppCustomers.getAllOpportunites(resellerOrgID=arguments.resellerOrgID,opportunityTypes='ALL')>
		
		<cfreturn qApprovals.recordcount>
	</cffunction>
	
	<cffunction name="getRenewals" output="yes" returntype="string" hint="Returns number of renewals expiring in current month">
		
		<cfargument name="resellerOrgID" required="yes" type="numeric">
		<cfset var renewals = "" />
		
		<cfset renewals = application.com.oppCustomers.getCustomersWithDealsAndRenewals(resellerOrgID=arguments.resellerOrgID,opportunityTypes='ALL')>
		
		<cfquery name="renewals" dbtype="query">
			SELECT 	*
			FROM 	renewals
			WHERE 	oppTypeTextID = 'Renewal'
			AND 	closeMonth = #month(now())#
			AND 	closeYear = #year(now())#
		</cfquery>
			
		<cfreturn renewals.recordCount>
	</cffunction>
	
	<cffunction name="getOpportunities" output="yes" returntype="query" hint="Returns number of opportunities in given month">
		
		<cfargument name="resellerOrgID" required="yes" type="numeric">
		<cfargument name="l_month" required="no" type="numeric" default=#month(now())#>
		<cfargument name="l_year" required="no" type="numeric" default=#year(now())#>

		<cfset var qOpps = "" />
		
		<!--- PPB 2010-07-16 i have switched the logic so that we no longer send in a list stages to show (some of which are user defined) but i use the existing hideOppStagesFromOppList ini setting instead --->			
		<cfset qOpps = application.com.oppCustomers.getAllOpportunites(resellerOrgID=arguments.resellerOrgID,showStagePlaceOrder='false')>
		
		
		<cfquery name="qOpps" dbtype="query">
			SELECT 	*
			FROM 	qOpps
			WHERE 	closeMonth = #l_month#
			AND 	closeYear = #l_year#
			AND  	opptypeID = 1
		</cfquery>
		<!--- oppTypeID 1 = Deals (see OppType table)--->
		<cfreturn qOpps>
	</cffunction>
	
	<cffunction access="public" name="getOppProductsForGivenDate" returnType="query" hint="Returns pipeline or forecast for given date.">
		
		<cfargument name="resellerOrgID" required="yes" type="numeric">
		<cfargument name="l_pipeline" required="no" type="numeric" default="1">
		<cfargument name="l_month" required="no" type="numeric" default=#month(now())#>
		<cfargument name="l_year" required="no" type="numeric" default=#year(now())#>
		<cfargument name="sortOrder" required="no" type="string" default="org.organisationID,  o.expectedCloseDate">
		
		<cfset var qryOppProducts = "" />
		
		<cfquery name="qryOppProducts" datasource="#application.siteDataSource#">
			SELECT     o.opportunityID, 
					   o.expectedCloseDate, 
					   o.actualCloseDate, 
					   o.detail AS opportunityDescription, 
					   SUM(ISNULL(op.subtotal, 0)) AS TotalValue, 
					   o.currency, 
					   ot.oppTypeTextID,
					   org.organisationName,
					   org.organisationID,
					   OppStatus.statusTextID 
			FROM       opportunity AS o 
			INNER JOIN organisation org ON o.entityid = org.organisationid
			INNER JOIN OppStatus 	ON  o.statusID = o.StatusID
			INNER JOIN OppStage 	ON  OppStage.OpportunityStageID = o.stageID
			INNER JOIN OppType ot 	ON  ot.OppTypeID = o.OppTypeID
			LEFT OUTER JOIN  oppProducts AS op ON o.opportunityID = op.OpportunityID
			INNER JOIN location on location.locationID = o.partnerLocationID
				AND location.organisationID = #arguments.resellerOrgID# 
			WHERE  (OppStage.stageTextID NOT IN ('oppStageClosed'))	
			AND (OppStage.LiveStatus = 1)	
			AND ((ot.oppTypeTextID = 'Deals' AND oppStatus.statusTextID <cfif arguments.l_pipeline EQ 1>!</cfif>= 'DealRegApproved') 
				OR (ot.oppTypeTextID = 'Renewal'))	
			AND month(o.expectedCloseDate) = #l_month#
			AND year(o.expectedCloseDate) = #l_year#			
			GROUP BY  oppStatus.statusTextID, org.organisationID, org.organisationName, o.opportunityID, o.expectedCloseDate, o.actualCloseDate, o.detail, o.currency, ot.oppTypeTextID
			ORDER BY #arguments.sortOrder#
		</cfquery>
		
		<cfreturn qryOppProducts>
		
	</cffunction>

 --->
</cfcomponent>

