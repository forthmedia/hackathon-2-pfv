﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayHomePage.cfc
Author:			
Date created:	

Amendment History:

DD-MMM-YYYY	Initials 	What was changed
11/01/2012		RMB		P-LEN024 - CR066 - Added and expanded SQL for "GetMyActions", "GetMyNewActions, "GetMyAccounts" and "GetMyAccountPlans"
16/01/2012		RMB		P-LEN024 - CR066 - Added new functions "DashboardNumberOptions", "DashboardDisplayOptions", "getReportDashboard" and "getDefaultHPStruct"
2012-03-20		RMB		varing and fix to GetMyNewActions query
--->

<!--- 
File name:		relayHomePage.cfc
Author:			
Date created:	

Amendment History:

YYYY/MM/DD Initials 	What was changed
May 2005 - Nikki/WAB created function recentAndQueuedComms to bring back queued and sent communications for the users country(ies)
2008/03/10  WAB MyActions bringing back actions with deleted entitues, see commment in code

2012/01/11      RMB		P-LEN024 - CR066 - Added and expanded SQL for "GetMyActions", "GetMyNewActions, "GetMyAccounts" and "GetMyAccountPlans"
2012/01/16	RMB		P-LEN024 - CR066 - Added new functions "DashboardNumberOptions", "DashboardDisplayOptions", "getReportDashboard" and "getDefaultHPStruct"
2012/02/20 	PPB Case 426740 use same query to get My Accounts total to show on homepage as is used by MyAccounts.cfm list
2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
2015/02/16	GCC GetMyOpportunities() stopped using vLeadListingCalculatedBudget as performance was bad--->
--->


<cfcomponent displayname="Relay Home Page" hint="Relay Home Page Queries">
<!--- ==============================================================================
                                                         
=============================================================================== --->
	<cffunction name="GetWidgetQuery" access="public" returntype="query" >
		<cfargument name="HomePageType" default = "" required="false">
		
		<cfset var qryHomepageWidget = queryNew("widgetData,displayRows,widgetParams,widgetDisplay,headerText,iconImgPath,moreLink")>
		<cfset var LPNSite = "#application.com.settings.getsetting('homePageSettings.latestPartnerNewsSiteTree')#">
		<cfset var UseFuncLPNSite = "NewsItems">
				
			<cfif LPNSite NEQ ''>
				<cfset UseFuncLPNSite = "FromTreeNewsItems">
			</cfif>

		<cfscript>
			// build the query object of widgets to show - in the future this can be made more sophiticated 
			// and controled by business group
			
			queryAddRow(qryHomepageWidget,1);
			querySetCell(qryHomepageWidget,"widgetData","MyRelay");
			querySetCell(qryHomepageWidget,"displayRows","5");
			querySetCell(qryHomepageWidget,"widgetDisplay","List");
			querySetCell(qryHomepageWidget,"headerText","Phr_Sys_My_Relay");
			querySetCell(qryHomepageWidget,"iconImgPath","/images/misc/icon_MyRelay.jpg");
			querySetCell(qryHomepageWidget,"moreLink","");
			
			
			queryAddRow(qryHomepageWidget,1);
			querySetCell(qryHomepageWidget,"widgetData","EmailShots");
			querySetCell(qryHomepageWidget,"displayRows","6000");
			querySetCell(qryHomepageWidget,"widgetParams","displayRows=5");
			querySetCell(qryHomepageWidget,"widgetDisplay","List");
			querySetCell(qryHomepageWidget,"headerText","phr_Sys_Latest_emailshots");
			querySetCell(qryHomepageWidget,"iconImgPath","/images/misc/icon_EmailShots.jpg");
			querySetCell(qryHomepageWidget,"moreLink","javascript:void(openNewTab('phr_Sys_Latest_emailshots','phr_Sys_Latest_emailshots','/communicate/commCheck.cfm',{reuseTab:true,iconClass:'communicate'}));");
	
			queryAddRow(qryHomepageWidget,1);
			querySetCell(qryHomepageWidget,"widgetData","#UseFuncLPNSite#");
			querySetCell(qryHomepageWidget,"displayRows","6");
			querySetCell(qryHomepageWidget,"widgetDisplay","List");
			querySetCell(qryHomepageWidget,"headerText","phr_Sys_Latest_partner_news");
			querySetCell(qryHomepageWidget,"iconImgPath","/images/misc/icon_NewsItems.jpg");
			querySetCell(qryHomepageWidget,"moreLink","");
		</cfscript>
		
		<cfif HomePageType IS ''>
		<cfscript>		
			queryAddRow(qryHomepageWidget,1);
			querySetCell(qryHomepageWidget,"widgetData","Promotions");
			querySetCell(qryHomepageWidget,"displayRows","6");
			querySetCell(qryHomepageWidget,"widgetDisplay","List");
			querySetCell(qryHomepageWidget,"headerText","phr_Sys_Latest_partner_promotions");
			querySetCell(qryHomepageWidget,"iconImgPath","/images/misc/icon_Promotions.jpg");
			querySetCell(qryHomepageWidget,"moreLink","");
		</cfscript>
		</cfif>		

		<cfscript>						
			queryAddRow(qryHomepageWidget,1);
			querySetCell(qryHomepageWidget,"widgetData","HintsAndShortcuts");
			querySetCell(qryHomepageWidget,"displayRows","6");
			querySetCell(qryHomepageWidget,"widgetDisplay","List");
			querySetCell(qryHomepageWidget,"headerText","phr_Sys_Helpful_hints_and_shortcuts");
			querySetCell(qryHomepageWidget,"iconImgPath","/images/misc/icon_HintsAndShortcuts.jpg");
			querySetCell(qryHomepageWidget,"moreLink","");
		</cfscript>
		
		<cfif HomePageType IS ''>
		<cfscript>				
			queryAddRow(qryHomepageWidget,1);
			querySetCell(qryHomepageWidget,"widgetData","KeyStatistics");
			querySetCell(qryHomepageWidget,"displayRows","6");
			querySetCell(qryHomepageWidget,"widgetDisplay","List");
			querySetCell(qryHomepageWidget,"headerText","phr_Sys_Key_statistics");
			querySetCell(qryHomepageWidget,"iconImgPath","/images/misc/icon_KeyStatistics.jpg");
			querySetCell(qryHomepageWidget,"moreLink","javascript:void(openNewTab('phr_Sys_Key_statistics','phr_Sys_Key_statistics','/homepage/keyStats.cfm',{reuseTab:true}));");
		</cfscript>
		</cfif>

		
		<cfreturn qryHomepageWidget>
	
	</cffunction>
	
<!--- ==============================================================================
                                                         
=============================================================================== --->
	<cffunction name="GetMyActions" access="public" returntype="query" >
		
		<cfset var GetMyActions = "">
		
		<!--- WAB 2008/03/10 added joins to organisation and person tables to assure that actions are with entities which exist --->
		<CFQUERY NAME="GetMyActions" DATASOURCE="#application.siteDataSource#">
			SELECT count(*) as numberOfActions,
       		ISNULL(SUM((CASE WHEN Deadline < GETDATE() THEN 1 ELSE 0 END)), 0) AS 'numberOfOdActions'
			from actions a
			INNER JOIN actionStatus ast ON ast.actionStatusID = a.actionStatusID
			left join person p on a.entityTypeid =0 and p.personid = a.entityid
			left join organisation o on a.entityTypeid =2 and o.organisationid = a.entityid
			WHERE ownerPersonID = #request.relayCurrentUser.personid#
			AND a.actionStatusID not in (SELECT actionStatusID from actionStatus where assigned = 0)
			and ((a.entityTypeid =0 and p.personid is not null) or (a.entityTypeid =2 and o.organisationid is not null) or (a.entityTypeid  not in (0,2)) )
		</CFQUERY>	
		
		<cfreturn GetMyActions>
	
	</cffunction>

<!--- ==============================================================================
                                                       
=============================================================================== --->

	<cffunction name="GetMyNewActions" access="public" returntype="query" >
		
		<cfset var GetMyNewActions = "">
		
		<CFQUERY NAME="GetMyNewActions" DATASOURCE="#application.siteDataSource#">
			SELECT count(*) as newActions,
       		ISNULL(SUM((CASE WHEN Deadline < GETDATE() THEN 1 ELSE 0 END)), 0) AS 'numberOfOdActions'
			from actions a
			INNER JOIN actionStatus ast ON ast.actionStatusID = a.actionStatusID
			left join person p on a.entityTypeid =0 and p.personid = a.entityid
			left join organisation o on a.entityTypeid =2 and o.organisationid = a.entityid
			WHERE ownerPersonID = #request.relayCurrentUser.personid#
			AND a.actionStatusID not in (SELECT actionStatusID from actionStatus where assigned = 0)
			and datediff(day,a.created,getDate()) < 5 
			and (a.lastupdatedby <> #request.relayCurrentUser.usergroupid# or a.lastUpdatedBy is null)
		</CFQUERY>
		
		<cfreturn GetMyNewActions>
	
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="GetMyAccounts" access="public" returntype="query" >
		<cfargument name="frmAccountMngrID" default="0">								<!--- 2012/02/20 PPB Case 426740 retained the "frm" variable name for compatibility with commonQueries.getUserAccounts() in case we merge the calls --->
		<cfargument name="AccountMngrFlagTextID" default="AllocationSalesTechnical">	<!--- 2012/02/20 PPB Case 426740 --->
		
		<cfset var GetMyAccounts = "">
		<cfset var myObject	= '' />

		<!--- START: 2012/02/20 PPB Case 426740 
			there was a problem such that a user could see My Accounts = 2 on the home page but only 1 would list when the user clicked thro
			I now use have emulated the call to commonQueries (as used by myAccounts.cfm) to ensure the queries run are the same 
		 ---> 

<!--- 		
		<CFQUERY NAME="GetMyAccounts" DATASOURCE="#application.siteDataSource#">
			select count(*) as numberOfAccounts from organisation 
			inner join integerFlagData on entityID=organisationID and flagID in
			(select flagID from flag where flagGroupID 
				in (select flagGroupID from flagGroup 
			where flagGroupTextID in ('AllocationSalesTechnical')))
			where data = #request.relayCurrentUser.personid#
			<!--- WAB LID5353 2011/01/25  added this to deal with unlikely (but happened on Lenovo) case of a person being marked as the account manager for organisations in countries which they did not have rights to --->
			and countryid in (#request.relaycurrentuser.countrylist#)
		</CFQUERY>
 --->

		<cfset var roleScopeMyAccounts = application.com.settings.getSetting("accounts.roleScopeMyAccounts")>

		<cfif roleScopeMyAccounts 
			and not application.com.login.checkInternalPermissions("dataManagerTask","level3")>
				<cfset arguments.frmAccountMngrID = request.relaycurrentuser.personID>
		</cfif>

		<cfscript>
		// create an instance of the object
		myObject = createObject("component","relay.com.commonQueries");
		myObject.dataSource = application.siteDataSource;
		myObject.personid = request.relayCurrentUser.personid;
		myObject.userGroupID = request.relayCurrentUser.usergroupid;
		myObject.frmAccountMngrID = frmAccountMngrID;
		myObject.AccountMngrFlagTextID = AccountMngrFlagTextID; 
		//myObject.flagGroupTextID = myAccountflagGroupTextIDToShow;
		//myObject.alphabeticalIndexColumn = alphabeticalIndexColumnUnaliased;
		//myObject.alphabeticalIndex = "";
		//myObject.sortOrder = sortOrder;
		myObject.getUserAccounts();
		</cfscript>

		<cfquery name="GetMyAccounts" dbtype="query">
			select count(*) as numberOfAccounts from myObject.qUserAccounts 
		</cfquery>

		<!--- END: 2012/02/20 PPB Case 426740 ---> 

		<cfreturn GetMyAccounts>
	
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="GetMyOpportunities" access="public" returntype="query" >
		
		<cfset var GetMyOpportunities = "">
		<!--- 2015/02/16 GCC stopped using vLeadListingCalculatedBudget as performance was bad--->
		<CFQUERY NAME="GetMyOpportunities" DATASOURCE="#application.siteDataSource#">
			SELECT count(*) AS numberOfOpps
			FROM opportunity o with(nolock)
			inner join OppStage os with(nolock) on o.stageid = os.OpportunityStageID
			WHERE os.STATUS <> 'delete' and (
					vendorAccountManagerPersonID =  #request.relayCurrentUser.personid#
					OR opportunityid IN (
						SELECT recordid
						FROM recordRights with(nolock)
								WHERE UserGroupID = #request.relayCurrentUser.usergroupid# 
							AND Entity = 'opportunityManagement'
						)
					)
		</CFQUERY>

		
		<cfreturn GetMyOpportunities>
	
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="GetNewsItems" access="public" returntype="query" >
		
		<cfset var GetNewsItems = "">
		
		<CFQUERY NAME="GetNewsItems" DATASOURCE="#application.siteDataSource#">
			SELECT top 3 e.ID 
			FROM Element e 
			INNER JOIN BooleanFlagData b ON e.id = b.entityid
			INNER JOIN flag f ON f.flagid = b.flagid
			WHERE f.flagtextid='PartnerHomepage_News_Item'
		</CFQUERY>

		
		<cfreturn GetNewsItems>
	
	</cffunction>
	
	<cffunction name="GetTreeNewsItems" access="public" returntype="query" >
		
		<cfset var GetNewsItems = "">
		<cfset var LPNSite = "#application.com.settings.getsetting('homePageSettings.latestPartnerNewsSiteTree')#">

		<CFQUERY NAME="GetNewsItems" DATASOURCE="#application.siteDataSource#">
			exec GetElementTree
				@ElementID =  <cf_queryparam value="#LPNSite#" CFSQLTYPE="CF_SQL_INTEGER" > , 
				@personid=#request.relaycurrentuser.personid#,
				@Statusid=4
		</CFQUERY>
		
		<cfreturn GetNewsItems>
	
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="GetPromos" access="public" returntype="query" >
		
		<cfset var GetPromos = "">
		
		<CFQUERY NAME="GetPromos" DATASOURCE="#application.siteDataSource#">
			SELECT top 3 e.ID
			FROM Element e 
			INNER JOIN BooleanFlagData b ON e.id = b.entityid
			INNER JOIN flag f ON f.flagid = b.flagid
			WHERE f.flagtextid='PartnerHomepage_Promo_Item'
		</CFQUERY>

		<cfreturn GetPromos>
	
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="GetKeyStats" access="public" returntype="query" >
		
		<cfset var GetKeyStats = "">
		
		<CFQUERY NAME="GetKeyStats" DATASOURCE="#application.siteDataSource#">
			SELECT top 10 country +' '+ statistic as description, value, path,last_updated as lastUpdated
			FROM vKeyStatsList 
			where last_Updated = (SELECT max(last_Updated) FROM vKeyStatsList)
			and (countryID in (select countryID 
			from rights 
			where usergroupID = #request.relayCurrentUser.usergroupid#))
			ORDER by countryID, statistic
		</CFQUERY>

		<cfreturn GetKeyStats>
	
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="CheckTestRecord" access="public" >
		
		<cfset var CheckTestRecord = "">
		
		<CFQUERY NAME="CheckTestRecord" DATASOURCE="#application.SiteDatasource#">
			if not exists (select * from lookupList where lookupid = 27)
			 BEGIN
				INSERT INTO [LookupList]([LookupID], [FieldName], [ItemText], [ExtraInfo], [Description], [IsParent], [IsDefault], [InputType], [IsLive], [Locked], [CreatedBy], [Created], [LastUpdated], [LastUpdatedBy])
				VALUES(27, 'commTypeLid','Test Communication', 0,'Used for test', 0, 1,'S', 1, 0, 100, getdate(), getDate(), 100) 
			END
		</CFQUERY>

	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="recentComms" access="public" >
		
		<cfargument name="countryList" default="">
		
		<cfset var recentComms = "">
		
		<CFQUERY NAME="recentComms" DATASOURCE="#application.siteDataSOurce#">
			SELECT c.CommID, c.Title, c.SendDate

			FROM 
				Communication c 
					INNER JOIN 
				UserGroup on c.CreatedBy = UserGroup.UserGroupID
					INNER JOIN  
				Person ON UserGroup.PersonID = Person.PersonID
					INNER JOIN 
				Location ON Person.LocationID = Location.LocationID
					<!--- INNER JOIN 
				commFile ON CommFile.CommID = c.CommID--->
					INNER JOIN 
				Country ON Location.CountryID = Country.CountryID
				, lookuplist
			WHERE 
				c.CommFormLID = LookupList.LookupID
			and c.commformlid in (2,3,5,6,7)
			<cfif len(arguments.countryList)>
				and Location.countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			and commTypeLid <> 27
			<!--- test for whether sent externally --->
			and (select distinct 1 from commselection where commselection.commid = c.commid and commselectlid = 0 and sent = 1) = 1
			order by c.SendDate desc
		</CFQUERY>

		<cfreturn recentComms>
		
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="recentAndQueuedComms" access="public" >
		
		<cfargument name="countryList" default="">
		
		<cfset var comms = structNew()>
		<cfset var recentComms	= '' />
		<cfset var recentQueuedComms	= '' />

				
		<CFQUERY NAME="recentComms" DATASOURCE="#application.siteDataSOurce#">
			select c.commid, c.Title, 'sent', max(cs.SentDate) as SendDate
			from communication c
			inner join country cty on ',' + c.countryidlist + ',' like '%,' + convert(varchar,cty.countryid) + ',%'  <!--- WAB: rather a nasty  construction to link to a list of countryids, also tried my own function dbo.listfind (c.countryidlist,cty .countryid) <> 0 --->--
			inner join commselection cs on cs.commid = c.commid and cs.commselectlid = 0  and cs.sent in (1)
			where 
			c.CommFormLID IN (2, 3, 5, 6, 7)
			<cfif len(arguments.countryList)>
			and cty.countryid  in ( <cf_queryparam value="#arguments.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			and c.CommTypeLID <> 27
			and DATEDIFF(day, cs.SentDate, GETDATE()) <= 90
 			group by c.commid, c.Title
			Order by max(cs.SentDate) desc
		</CFQUERY>
		<cfset comms.recentComms = recentComms>
		
		<CFQUERY NAME="recentQueuedComms" DATASOURCE="#application.siteDataSOurce#">
			select c.commid, c.Title, 'Queued', min(cs.SendDate) as SendDate
			from communication c
			inner join country cty on ',' + c.countryidlist + ',' like '%,' + convert(varchar,cty.countryid) + ',%'  <!--- dbo.listfind (c.countryidlist,cty .countryid) <> 0 --->
			inner join commselection cs on cs.commid = c.commid and cs.commselectlid = 0  and cs.sent in (2)
			where 
			c.CommFormLID IN (2, 3, 5, 6, 7)
			<cfif len(arguments.countryList)>
			and cty.countryid  in ( <cf_queryparam value="#arguments.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			and c.CommTypeLID <> 27
			and DATEDIFF(day, cs.SentDate, GETDATE()) <= 90
			group by c.commid, c.Title
			Order by min(cs.SendDate) asc
		</CFQUERY>
		<cfset comms.recentQueuedComms = recentQueuedComms>
		
		<cfreturn comms>
		
	</cffunction>
<!--- ==============================================================================
                                                       
=============================================================================== --->	

	<cffunction name="GetLastWhatsNewDate" access="public" >
		
		<cfset var GetLastWhatsNewDate = "">
		
		<CFQUERY NAME="GetLastWhatsNewDate" DATASOURCE="#application.SiteDatasource#">
			select distinct element.id from element left outer join
			(select entityid from entityTracking where entitytracking.entitytypeid=10
			and entitytracking.personid=#request.relayCurrentUser.personid#) as et
			on element.id=et.entityid where et.entityid is null and element.id IN (SELECT id
			FROM Element inner join elementType on element.elementTypeID = elementtype.elementTypeID
				WHERE Element.isLive = 1
			and elementtype.Type = 'Whats New'
			and element.date>getdate())
		</CFQUERY>

		<cfreturn GetLastWhatsNewDate>
		
	</cffunction>

<!--- ==============================================================================
P-LEN024 - CR066 - RMB - Dashboard allowed by type query                                                  
=============================================================================== --->	
	<cffunction name="DashboardNumberOptions" access="public" hint="" output="false" validValues="true">
		<cfargument name="DashType" required="true">
			<cfset var getDashNumOptions = "">

			<cfif DashType EQ '' OR DashType EQ 'Standard'>			
			<cf_querySim>
				getDashNumOptions
				display,Value
				0|0
			</cf_querySim>
			<!--- 2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
			<cfelseif DashType EQ 'Advanced'>
			<cf_querySim>
				getDashNumOptions
				display,Value
				0|0
			</cf_querySim>
			<cfelseif DashType EQ 'FullScreen'>
			<cf_querySim>
				getDashNumOptions
				display,Value
				1|1
				2|2
				3|3
				4|4
				5|5
				6|6
			</cf_querySim>
			<cfelseif DashType EQ 'SplitScreen'>
			<cf_querySim>
				getDashNumOptions
				display,Value
				1|1
				2|2
				3|3
			</cf_querySim>
			</cfif>
			
			<cfreturn getDashNumOptions>
	
	</cffunction>

<!--- ==============================================================================
P-LEN024 - CR066 - RMB - Dashboard Types query                                                  
=============================================================================== --->		
	<cffunction name="DashboardDisplayOptions" access="public" hint="" validValues="true">
	
			<cfset var getDashDisOptions = "">
			
			<!--- 2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
			<cf_querySim>
				getDashDisOptions
				display,Value
				Phr_Sys_HomePageTypes_Standard|Standard
				Phr_Sys_HomePageTypes_FullScreenReports|FullScreen
				Phr_Sys_HomePageTypes_SplitScreenReports|SplitScreen
				Phr_Sys_HomePageTypes_Advanced|Advanced
			</cf_querySim>
			
			<cfreturn getDashDisOptions>
	
	</cffunction>
	
<!--- ==============================================================================
P-LEN024 - CR066 - RMB - Get a users Dashboard Option                                                  
=============================================================================== --->	
	<cffunction name="getReportDashboard" access="public" returntype="string">
		<cfargument name="UserGroupID" default = "#request.relaycurrentuser.userGroupID#">
		<cfargument name="UserGroups" default = "#request.relaycurrentuser.userGroups#">
		<cfargument name="subkey" required="true">
		
			<cfset var qGetReportDashboard = "">
			
					<cfquery name="qGetReportDashboard"  datasource = "#application.sitedatasource#">
					select  top 1 data as result,
					p.entityid as usergroupid, subkey, data as result, case when p.entityid = 0 then 'All Users' else ug.name end as usergroupname
					from 
						preferences p with(nolock) 
							inner join 
						usergroup ug with(nolock) on ug.usergroupid = p.entityid 
					where 
							p.entitytypeid = 0
						and hive ='reportDashboard'
						and (subkey =  <cf_queryparam value="#arguments.subkey#" CFSQLTYPE="CF_SQL_VARCHAR" > )
						and p.entityID in (0,#UserGroupID#, #UserGroups#)
					 
					order by
						case 
							when p.entityid  in ( <cf_queryparam value="#UserGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) then 1
							when p.entityid  in ( <cf_queryparam value="#usergroups#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) then 2
							when p.entityid = 0 then 3
						 end , 
						subkey asc
					</cfquery>

		<cfreturn qGetReportDashboard.result>
				
	</cffunction>
	
<!--- ==============================================================================
P-LEN024 - CR066 - RMB - Default Struct for home page options                                                       
=============================================================================== --->	
	<cffunction name="getDefaultHPStruct" access="public" returntype="Struct">
		<cfset var homepageStruct	= {}>
		<cfscript>	
		homepageStruct.homepage.DashboardType = '' ;
		homepageStruct.homepage.DashboardsToDisplay = '' ;
		homepageStruct.homepage.FullScreenReport1 = '' ;
		homepageStruct.homepage.FullScreenReport2 = '' ;
		homepageStruct.homepage.FullScreenReport3 = '' ;
		homepageStruct.homepage.FullScreenReport4 = '' ;
		homepageStruct.homepage.FullScreenReport5 = '' ;
		homepageStruct.homepage.FullScreenReport6 = '' ;
		homepageStruct.homepage.SplitScreenReport1 = '' ;
		homepageStruct.homepage.SplitScreenReport2 = '' ;
		homepageStruct.homepage.SplitScreenReport3 = '' ;		
		</cfscript>
		
		<cfreturn homepageStruct>
				
	</cffunction>


	<!--- 2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
	<cfscript>
		public query function GetMyNewAccountApprovals()
		{
			var sql = "
				SELECT COUNT(1) numberOfNewAccountApprovals
				FROM
					Person p with(noLock)
					INNER JOIN Organisation o with(noLock) ON P.Organisationid = o.Organisationid
					INNER JOIN organisationType ot ON ot.organisationTypeID=o.organisationTypeID AND ot.TypeTextID != 'EndCustomer'
					INNER JOIN (booleanflagdata bp with(noLock)
								INNER JOIN flag fp with(noLock) ON fp.flagid = bp.flagid
								INNER JOIN flaggroup fgp with(noLock) ON fgp.flaggroupid = fp.flaggroupid AND fgp.flaggrouptextid = 'PerApprovalStatus'
								) ON bp.entityid = p.personID
					INNER JOIN (booleanflagdata bo with(noLock)
								 INNER JOIN flag fo with(noLock) ON fo.flagid = bo.flagid
								 INNER JOIN flaggroup fgo with(noLock) ON fgo.flaggroupid = fo.flaggroupid AND fgo.flaggrouptextid = 'orgApprovalStatus'
								) ON bo.entityid = o.organisationid
					INNER JOIN country c with(noLock) ON o.countryid = c.countryid
					LEFT JOIN booleanFlagData bfpd with (noLock) on bfpd.entityID = p.personID and bfpd.flagID = :deletePersonflagID
					LEFT JOIN booleanFlagData bfod with (noLock) on bfod.entityID = o.organisationID and bfod.flagID = :deleteOrganisationflagID
				WHERE
					1=1
					#application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause#

					AND bfpd.entityID is null
					AND bfod.entityID is null

					AND o.countryid = :countryid
					AND fp.flagTextID = 'PerApplied'
			";
			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="deletePersonflagID",cfsqltype="CF_SQL_INTEGER",value=application.com.flag.getFlagStructure("deletePerson").flagID);
			qObj.addParam(name="deleteOrganisationflagID",cfsqltype="CF_SQL_INTEGER",value=application.com.flag.getFlagStructure("deleteOrganisation").flagID);
			qObj.addParam(name="countryid",cfsqltype="CF_SQL_INTEGER",value=request.relayCurrentUser.countryid);
			var qGetMyNewAccountApprovals = qObj.execute(sql=sql).getResult();

			return qGetMyNewAccountApprovals;
		}


		public query function GetMyLeads()
		{
			var qGetLeadCount = application.com.relayLeads.GetLeads("count");

			return qGetLeadCount;
		}


		public query function GetMyFundManager()
		{
			var myObject = createObject("component","relay.com.commonQueries");
			myObject.dataSource = application.siteDataSource;
			myObject.personid = request.relayCurrentUser.personID;
			myObject.userGroupID = request.relayCurrentUser.userGroupID;
			myObject.frmAccountMngrID = request.relaycurrentuser.personID;
			myObject.getUserAccounts();

			var sql = "
				SELECT COUNT(1) numberOfFundManagers
				FROM vFundRequestList fr
				INNER JOIN fundCompanyAccount fca ON fca.accountID = fr.accountID
				INNER JOIN organisation o ON o.organisationID = fca.organisationID
			";
			if (myObject.qUserAccounts.recordcount) {
				sql &= " WHERE o.organisationID  IN ( :organisationIDList )";
			} else {
				sql &= " WHERE 1=0 ";
			}
			sql &= " AND count_requests > 0";

			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			if (myObject.qUserAccounts.recordcount) {
				qObj.addParam(name="organisationIDList",cfsqltype="CF_SQL_INTEGER",value=valueList(myObject.qUserAccounts.organisationId),list=true);
			}
			var qGetMyFundManager = qObj.execute(sql=sql).getResult();

			return qGetMyFundManager;
		}


		public query function GetMySelections()
		{

			var sql = "
				SELECT COUNT(1) numberOfSelections
				FROM Selection AS s
				INNER JOIN SelectionGroup AS sg ON sg.SelectionID = s.SelectionID
				INNER JOIN RightsGroup rg on sg.usergroupid = rg.usergroupid
				INNER JOIN usergroup ug on s.createdby = ug.usergroupid
				WHERE rg.personID = :personID
			";

			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="personID",cfsqltype="CF_SQL_INTEGER",value=request.relaycurrentuser.personid);
			var qGetMySelections = qObj.execute(sql=sql).getResult();

			return qGetMySelections;
		}


		public query function GetMyFavouriteReports()
		{

			var qryReports = application.com.myrelay.getMyLinks(personID=request.relayCurrentUser.personID);

			var qGetMyFavouriteReports = QueryNew("numberOfFavouriteReports");
			QueryAddRow(qGetMyFavouriteReports);
			QuerySetCell(qGetMyFavouriteReports, "numberOfFavouriteReports", qryReports.recordCount);

			return qGetMyFavouriteReports;
		}


		public query function GetMyReports()
		{

			var reportCount = 0;

			var subMenuNavItem = "reports";
			var module = "MyFavorites";

			var qrySubMenuNavItem = application.com.relayMenu.getSubMenuNavItems(module=module,subMenuNavItem=subMenuNavItem);
			reportCount += qrySubMenuNavItem.recordCount;

			var qryClientReports = application.com.relayReports.getClientReports(module=module);
			reportCount += qryClientReports.recordCount;

			if (application.com.relayMenu.isModuleActive(moduleID="ReportManager")) {
				var qryJasperReports = application.com.jasperServer.getReportsQuery(module=module);
				reportCount += qryJasperReports.recordCount;
			}

			var reportsFile = application.paths.relayware & "\xml\ReportList.xml";
			if (FileExists(reportsFile)) {
				var myxml = FileRead(reportsFile);
				var myxmldoc = XmlParse(myxml);
				var selectedElements = XmlSearch(myxmldoc, "//reportGroup[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#LCase(module)#']/report");
				reportCount += ArrayLen(selectedElements);
			}

			var qGetMyReports = QueryNew("numberOfReports");
			QueryAddRow(qGetMyReports);
			QuerySetCell(qGetMyReports, "numberOfReports", reportCount);

			return qGetMyReports;
		}
	</cfscript>

</cfcomponent>

