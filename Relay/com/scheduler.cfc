<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Common Relay Schedule Queries" hint="Contains a number of commonly used Relay Schedule Queries">

	<cffunction name="runNextJob" access="public" returntype="numeric" hint="Runs the next job in the queue">
		<cfscript>
			var getNextJob="";
		</cfscript>
		
		<cfquery name="getNextJob" datasource="#application.siteDataSource#">
			select top 1 jobID, (datediff(s,j.lastrun,'#request.requestTime#')-jt.executionInterval*60) as timeWaiting
			from job j inner join jobType jt on
			j.jobTypeID = jt.jobTypeID where
			datediff(s,j.lastrun,'#request.requestTime#') > jt.executionInterval*60 
			and j.completed is null
			order by timeWaiting desc, lastrun, jobID asc
		</cfquery>
		
		<!--- if we have job ready to go, run it --->
		<cfif getNextJob.recordCount gt 0>
			
			<cfset x= application.com.jobs.runJob(#getNextJob.jobID#)>
			<cfreturn getNextJob.jobID>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
</cfcomponent>