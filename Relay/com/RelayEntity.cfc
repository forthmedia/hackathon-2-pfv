<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		RelayEntity.cfc
Author:			NJH
Date started:	12-04-2010

Description:	A list of generic entity functions

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011-05-25	NYB		REL106 added support for non PLO entities, and added getEntityByForeignKey & getEntityStructure functions
2011-05-19			NAS			LID5947/8 & 6329 - to be kept in line with code in \Relay\remoteAddTask.cfm
2011-08-05			NJH			Added 'excludeSalesForceRecords' when searching for matches on entities (particularly pol data).
2012-02-03			NYB			Case 424794 Added additional error reporting to insertEntity function
2012-02-12			NJH			CASE 426374: Added updateMatchNamesForExistenceCheck - ignore updating orgMatchNames in a bulk insert
2012-02-23			NJH			CASE 426744 - added server side unique email validation check
2012-03-01 			PPB/NJH 	Case 426946 - bipass email validation if running doesEntityExist to avoid error
2012-07-31			IH			Case 429379 change to cf_transaction
2012-09-13			PPB			Case 430249 - revert fix for Case 429813
2012-09-25			PPB			LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED
2013/06/05			NJH			Added support for setting the default translations for entity phrase fields (ie. title_defaultTranslation). Attempt to find the default translation and update it.
2013-06-18 			PPB 		Case 435741 subtotal is a computed column on the db so should never be inserted
2013-11-25 			PPB 		Case 438158 don't attempt to insert/update a Computed column
2014-01-21 			YMA 		Case 438616 allow text fields to save as empty string
2014-02-26 			PPB 		Case 439001 new param to excludeRecordsFlaggedForDeletion in doesEntityExist();
2014-07-10			PPB			Case 440579 defend against adding flagdata inadvertently because the flagTextId is "" AND added EntityName param to doesFlagGroupExist and doesFlagExist calls
2014-08-14			AHL			Case 440859 SFDC sync; MISSING_REQUIRED_FIELD error
2014-09-03			WAB 		Merging of Non POL Fields and Flags.  Small change to getTableFieldStructure - look for flags before flagGroup
2014-10-07			AHL/YMA		Case 441383 Empty/null values not updating;
2014-11-06			AHL			Case 442134 better report errors for sf.
2015-01-22			WAB			CASE 443542 Problems when updateEntityDetails called from API.
									1.	Sometimes didn't detect an update when it happened due to matching wrong lastupdatedby and date being inaccurate
									2.	Sometime failed to get commas correct in update SET statement
									3.	Problems with updating nullable date fields.
									4.	Opportunity.probability set to zero if an update done without specifying stageID
								Altered setProfileData() to support deleting boolean flags by passing in 0
2015-02-03			RJT			Inculded a post rights check check object - contains code for any custome checks which are run after the column rights have been checked
2015/02/05			NJH			Moved the setting of the EndCustomerOppContacts flag into the opportunity IU trigger. Any opps being created through the connector were not getting these flags set. Also, now just done in one place.
2015-02-11			RJT			Bug fix (no case number) inclusion of Entity Specific method for Leads broke customPostRightsCheck because those methods were called differently from API. customPostRightsCheck now included within the updateLeadEntity method so no longer an issue
2015-03-02			RPW			FIFTEEN-231 - API - unable to update profile information - Arbor
2015-04-29			RJT			Correction of bug where Organisation___ID (e.g. OrganisationTypeID) would be identified as OrganisationID for foreign keys
2015-05-06			NJH			Add createdByPerson as a field to check for and set when creating a record.
2015-09-17			RJT			Added option on getEntity to not check the logged in users credentials
2015-09-17			AHL			API - The previous change will not work if the accountTypeColumn is empty
2015-10-22			ESZ			case 446050: Unable to rename Question Pool
2015-11-20          ACPK        PROD2015-441 Fixed checkIfPersistanceIsNeeded not being correctly defined
2016-01-07			RJT			Dates are no longer converted (for the API) to UTC, this can't be done because without a time it can't be said when the 2nd Nov US time is UK time other than also 2nd Nov'
2016-01-13			RJT			Added a functional way in to the deleteEntityCheck stored proceedure for ease of use
2016-01-14			RJT			ValidateData now uses the countryID of the entity (if it has one) to validate valid values rather than all the countrys the current user has rights to. This resolves the issue where some countries have valid values and some don't and the user has access to both
2016/09/01			NJH			JIRA PROD125 - removed the call to get next assignedEntityId for POL data for insertEntity. Also replaced old matching code with new.
2016-09-20			RJT			Fixed bug in which if a field called SomethingID where something wasn't an entity would cause an exception
2016-09-30			RJT			PROD2016-2402 Ensure that "uniqueIdentifier" (aka guid) is properly validated when coming into API
2016-10-0e			RJT			PROD2016-2402 Don't attempt to "lower case" uniqueIdentifier datatype (as it doesn't support that)
2016-07-11			DCC for ESZ 		case 450687 Quiz answer score
2016-10-17 			PPB 		JAB001 added validateForeignKeys exception for product.campaignId
2016-10-20			WAB			PROD2016-2508 default value for countryIDPerspective and function establishCountryIDForEntity() could give a non numeric result (#countryIDList#\) which screwed up code which demanded that countryID be an integer

Possible enhancements:

 --->

<cfcomponent displayname="RelayEntity" hint="A collection of entity functions">

	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="updateEntityDetails" access="public" hint="Updates an entity's details" returntype="struct" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" required="false">
		<cfargument name="table" type="string" required="false">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="treatEmtpyFieldAsNull" type="boolean" required="false" default="true">
		<cfargument name="testUserEntityRights" type="boolean" default="false"> <!--- default to false at the moment as I don't know what implications this is going to have --->
		<cfargument name="updateDeleted" type="boolean" default="false">
		<cfargument name="customPostRightsCheck" type="relay.interfaces.RelayEntityPostRightsCheck" hint="An object implementing relayEntityPostRightsCheck passed to provide specific checks (which can only be done after column rights checking). Can be left unset if no check is required" >

		<cfscript>
			var updateDetails = "";
			var updateDetailsResult = "";
			var fieldCount = 0;
			var result = {isOk=true,message="",errorCode="",entityID=0};
			var fieldValue = "";
			var column = "";
			var field = "";
			var firstRun = true;
			var thisNullValue = "''";
			var preparedEntityStruct = structNew();
			var entityDetailsStruct = structNew();
			var tableDetailsStruct = structNew();
			var thisEntityType = "";
			var doesEntityExist = "";
			var rightsStruct = {edit=true,view=true};
			var flagList = "";
			var flagGroupList = "";
			var entityTypeStruct = structNew();
			var getUpdatedEntity = "";
			var tempResult = structNew();
			var translatedColumns = "";
			var updated = false;
			var lastUpdated = "";
		</cfscript>



		<cfif not structKeyExists(arguments,"table") and structKeyExists(arguments,"entityTypeID")>
			<cfset thisEntityType = application.entityType[arguments.entityTypeID].tablename>
		<cfelse>
			<cfset thisEntityType = arguments.table>
		</cfif>

		<cfset entityTypeStruct = getEntityType(entityTypeID=thisEntityType)>
		<cfif not structKeyExists(arguments,"entityTypeID")>
			<cfset arguments.entityTypeID = entityTypeStruct.entityTypeID>
		</cfif>

		<cfif not arguments.updateDeleted>
			<cfset tempResult = isEntityAvailableForGetUpdateOrDelete(entityID=arguments.entityID,entityTypeID=arguments.entityTypeID,entityDetails=arguments.entityDetails,testUserEntityRights=arguments.testUserEntityRights)>
			<cfset structAppend(result,tempResult,true)>
			<cfif not result.isOk>
				<cfreturn result>
			</cfif>
		</cfif>

		<!---The column rights check occures within prepareEntityStruct  --->
		<cfset preparedEntityStruct = prepareEntityStruct(method="update",rightsStruct=rightsStruct,argumentCollection=arguments)>
		<!--- <cfset structAppend(result,preparedEntityStruct,true)> --->

		<cfif not preparedEntityStruct.isOk>
			<cfset result.errorCode = preparedEntityStruct.errorCode>
			<cfif structKeyExists(preparedEntityStruct,"field")>
				<cfset result.field = preparedEntityStruct.field>
			</cfif>
			<cfif structKeyExists(preparedEntityStruct,"fieldValue")>
				<cfset result.fieldValue = preparedEntityStruct.fieldValue>
			</cfif>
			<cfset result.isOK = false>
			<cfset result.message = preparedEntityStruct.errorCode>
			<cfreturn result>
		</cfif>

		<!---The column rights check occures within prepareEntityStruct so by this point the user definately has rights to update the entity, we can now make any specific checks that were passed RJT--->
		<cfscript>
			if (structKeyExists(Arguments,"customPostRightsCheck")){
				var tempResult=makePostRightsCheckCustomCheck(
					customPostRightsCheck,
					entityTypeStruct.tablename,
					entityTypeStruct.uniqueKey,
					arguments.entityID
				);
				if (!tempResult.isOK){
					structAppend(result,tempResult,true);
					return result;
				}

			}
		</cfscript>




		<cfset entityDetailsStruct = preparedEntityStruct.entityDetailsStruct>
		<cfset tableDetailsStruct = preparedEntityStruct.tableDetailsStruct>
		<cfset flagList = preparedEntityStruct.flagList>
		<cfset flagGroupList = preparedEntityStruct.flagGroupList>
		<cfset translatedColumns = preparedEntityStruct.translatedColumns>

		<cf_transaction action="begin">
			<cftry>

				<cfif listLen(translatedColumns)>
					<cfset setDefaultTranslations(entityID=arguments.entityID,entityDetails=entityDetailsStruct,translatedColumns=translatedColumns,entityTypeID=arguments.entityTypeID,mode="update")>
				</cfif>

				<!--- if the tableDetailsStruct structCount is 3, that means we only have the lastupdated columns that we're setting... so, there is no update on the core entity table --->
				<cfif checkIfPersistanceIsNeeded(tableDetailsStruct)> <!---2015-10-22 ESZ case 446050 --->
					<!--- matchname is currently not updated automatically!! --->
					<cfquery name="updateDetails" datasource="#application.siteDataSource#" result="updateDetailsResult">
						update #entityTypeStruct.tablename#
						set
							<cfloop collection="#entityDetailsStruct#" item="field">
								<cfif structKeyExists(tableDetailsStruct,field)>
									<!--- WAB 2015-01-22 CASE 443542, replaced rather odd technique for putting in the comma.  Would break down if the last field processed failed the structKeyExists(tableDetailsStruct,field) test --->
									<cfif not firstRun>,<cfelse><cfset firstRun = false></cfif>
									<cfset fieldValue = entityDetailsStruct[field]>
									#trim(field)# =
									<cfif len(fieldValue) eq 0 and tableDetailsStruct[field].isNullable and (arguments.treatEmtpyFieldAsNull or listFindNoCase("datetime,date,timestamp",tableDetailsStruct[field].data_type))>
										null
									<cfelseif len(fieldValue) eq 0>
										<cfswitch expression="#tableDetailsStruct[field].data_type#">
											<cfcase value="bit,int,float,smallint,numeric,decimal">0</cfcase>
											<cfdefaultcase>''</cfdefaultcase>
										</cfswitch>
									<cfelseif len(fieldValue) gt 0>
                                        <!---11/07/2016 ESZ case 450687 Quiz answer score added sczle="3" --->
										<cf_queryparam value="#fieldValue#" cfsqltype="#tableDetailsStruct[field].cfsqltype#"  scale="3">
									</cfif>
								</cfif>
							</cfloop>
						where #entityTypeStruct.uniqueKey# = #arguments.entityID#
							and (
							<!--- 2015-03-02	RPW	FIFTEEN-231 - Added 1 = 0 as API calls do not send in any fields if a flag only is updated. --->
								1 = 0
							<!--- This WHERE section makes sure that the CRUD columns are not updated unless there is actually a modification to one of the fields --->
							<cfloop collection="#entityDetailsStruct#" item="field">
								<cfif structKeyExists(tableDetailsStruct,field)>
									<!--- don't check if updated dates are different --->
									<cfif not listFindNoCase(preparedEntityStruct.crudColumns,field)>
										OR
										<cfset fieldValue = entityDetailsStruct[field]>

											<cfif len(fieldValue) gt 0>
												(
													#field#
													<!--- do case sensitive comparison where necessary --->
													<cfif tableDetailsStruct[field].simpleDataType is "text" AND  tableDetailsStruct[field].DATA_Type NEQ "uniqueidentifier">
														COLLATE Latin1_General_CS_AS
													</cfif>
													<>	<cf_queryparam value="#fieldValue#" cfsqltype="#tableDetailsStruct[field].cfsqltype#">

													OR #field# is null
												)
											<cfelse>
												<!--- Now deal with updating to null/blank.  This closely follows the logic in the SET clause --->
												<cfif len(fieldValue) eq 0 and tableDetailsStruct[field].isNullable and (arguments.treatEmtpyFieldAsNull OR tableDetailsStruct[field].simpleDataType is "date" )>
													<!--- for fields which are nullable or date fields, check against null --->
													#field# is not null
												<cfelse>
													<!--- for other fields check against '' or 0 --->
													<cfswitch expression="#tableDetailsStruct[field].simpleDataType#">
														<cfcase value="bit,numeric"><cfset thisNullValue = 0></cfcase>
														<cfdefaultcase><cfset thisNullValue = "''"></cfdefaultcase>
													</cfswitch>
													isNull(#field#,#thisNullValue#) <> #thisNullValue#
												</cfif>
											</cfif>

									</cfif>
								</cfif>
							</cfloop>
							)
					</cfquery>

					<cfset updated = updateDetailsResult.recordCount>
				</cfif>



				<cfif listLen(flagList) or listLen(flagGroupList)>
					<!--- Update Profile Data --->

					<cfif not updated>

						<!--- 	need to be able to work out whether any of the profiles have caused an update,
								so need to know the current value of lastUpdated
								but don't need to worry about it if the base entity has already been updated
						--->
						<cfset var lastUpdatedQry = new Query (sql = "select lastUpdated from #entityTypeStruct.tablename# where #entityTypeStruct.uniqueKey# = :uniqueKey", name="getLastUpdated")>
						<cfset lastUpdatedQry.addParam (name="uniqueKey",cfSqlType="cf_sql_integer",value=arguments.entityID)>
						<cfset lastUpdated = lastUpdatedQry..execute().getResult().lastUpdated>
					</cfif>

					<cfset setProfileData(entityID=arguments.entityID,flagList=flagList,flagGroupList=flagGroupList,entityDetails=entityDetailsStruct)>

					<cfif not updated>
						<!--- work out whether setProfileData has caused any updates ---->

						<!--- 	WAB 2015-01-22 CASE 443542
								The technique for working out whether the record has just been updated was flakey when called by the API
								It would be nice to just use updateDetailsResult, but this does not take into account flag updates
								So I now use updateDetailsResult to check for updates on the base, but still need to use this query to test for flag updates (if there have been no updates on the base table)

								Previous problems with this query were

								1.	it was using dateDiff(s,lastUpdated,getDate()) < 5 which is somewhat hit and miss
									The lastUpdated being used in the update of the base table is request.requesttime
									The lastUpdated set on the base table when flags are updated is getDate()
									There could be huge discrepancies here
									I decided (after trying a few approaches) to get the current value of lastUpdated before updating the flags and check whether it had changed)

								2.	request.relaycurrentuser.personid/usergroupid did not always seem to reflect what lastUpdatedByPerson and lastUpdatedBy are set to in entityDetails struct
									However I cannot explain why this would be, but I know that flags would always be updated using relayCurrentUser (ie wold ignore what was set in entityDetails struct)
									So maybe bets to test against both sets of values (with an IN statement)
						--->

						<cfquery name="getUpdatedEntity" datasource="#application.siteDataSource#">
							select
								1
							from
								#entityTypeStruct.tablename#
							where
								#entityTypeStruct.uniqueKey# = #arguments.entityID#

								and lastUpdatedBy IN (<cf_queryparam value="#entityDetailsStruct.lastupdatedby#" cfsqltype="cf_sql_integer">,<cf_queryparam value="#request.relaycurrentuser.usergroupid#" cfsqltype="cf_sql_integer">)
								<cfif getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname="lastUpdatedByPerson").isOk>and lastUpdatedByPerson IN (<cf_queryparam value="#entityDetailsStruct.lastupdatedbyPerson#" cfsqltype="cf_sql_integer">,<cf_queryparam value="#request.relaycurrentuser.personid#" cfsqltype="cf_sql_integer">)</cfif>
								and lastUpdated > <cf_queryparam value="#lastUpdated#" cfsqltype="timeStamp">
						</cfquery>

						<cfset updated = getUpdatedEntity.recordCount>

					</cfif>


				</cfif>

				<!--- NJH 2013/01/29 - if updating your personal details, update the currentUser structure --->
				<cfif (entityTypeStruct.tablename eq "person" and arguments.entityID eq request.relaycurrentUser.personID) or (entityTypeStruct.tablename eq "location" and arguments.entityID eq request.relaycurrentUser.locationID) or (entityTypeStruct.tablename eq "organisation" and arguments.entityID eq request.relaycurrentUser.organisationID)>
					<cfset application.com.relayCurrentUser.updatePersonalData()>
				</cfif>

				<cf_transaction action="commit">

				<cfcatch>
					<cf_transaction action="rollback">
					<cfset result.isOK = false>
					<cfset result.errorCode = "UPDATE_ERROR">
					<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>
		</cf_transaction>

		<cfif result.isOK>

			<cfif updated>
				<cfset result.entityId = arguments.entityID>
				<cfset result.errorCode = "ENTITY_UPDATED">
			<cfelse>
				<cfset result.errorCode = "ENTITY_NOTUPDATED">
				<cfset result.entityID = "0">
			</cfif>

			<cfif arguments.entityTypeID is application.entityTypeID.files><!--- 2014/0915 GCC changed from file which does not exist in 2014/1 schema --->
				<cfset application.com.search.updateFilesSearchCollectionInThread(fileid=arguments.entityID)>
			</cfif>

			<cfset result.approvalDetails = application.com.approvalEngine.approvalRuleHandler(mode='request', entityTypeID=arguments.entityTypeID, approvalStatusRelatedEntityID=arguments.entityID) />
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="makePostRightsCheckCustomCheck" access="private" hint="Runs within update and delete methods for any custom checks that are passed">
		<cfargument name="customPostRightsCheck" type="relay.interfaces.RelayEntityPostRightsCheck" required="true" hint="An object implementing relayEntityPostRightsCheck passed to provide specific checks (which can only be done after column rights checking). Can be left unset if no check is required" >
		<cfargument name="tableName" type="string" required="true" hint="The entity table that is having the check run against it">
		<cfargument name="uniqueKey" type="string" required="true" hint="The name of unique key in the table used to identify the entity">
		<cfargument name="entityID" type="string" required="true" hint="The value of the entityID itself">
		<cfscript>
			var result = {isOk=true,message="",errorCode="",entityID=0};

			if (structKeyExists(Arguments,"customPostRightsCheck")){
				var requiredFields=customPostRightsCheck.getRequiredFields();
				var requiredFields_SQLFormat="";
				var i=1;

				for(i=1;i LTE ArrayLen(requiredFields);i=i+1){
					// SQL requires variablename, variablename, variablename, this produces that
					requiredFields_SQLFormat=requiredFields_SQLFormat & (i GT 1?", ":"") & requiredFields[i];
				}

				var queryForCheck = new query();
				queryForCheck.setDatasource("#application.siteDataSource#");

				var queryResult=queryForCheck.execute(sql= "Select #requiredFields_SQLFormat# FROM #tablename# WHERE #uniqueKey# = #entityID#");
				var checkReturnsOK=customPostRightsCheck.postRightsCheckFunction(queryResult);

				if (!checkReturnsOK){
					result.isOK = false;
					result.errorCode=customPostRightsCheck.getFailureError();
					result.message=customPostRightsCheck.getFailureMessage();
				}
				return result;
			}
		</cfscript>

	</cffunction>


	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="updatePersonDetails" access="public" returntype="struct" hint="Updates a persons details" output="false">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="personDetails" type="struct" required="true">
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfreturn updateEntityDetails(entityID=arguments.personID,entityDetails=arguments.personDetails,entityTypeID=application.entityTypeID["person"],testUserEntityRights=arguments.testUserEntityRights)>
	</cffunction>


	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="updateLocationDetails" access="public" returntype="struct" hint="Updates a locations details" output="false">
		<cfargument name="locationID" type="numeric" required="true">
		<cfargument name="locationDetails" type="struct" required="true">
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfreturn updateEntityDetails(entityID=arguments.locationID,entityDetails=arguments.locationDetails,entityTypeID=application.entityTypeID["location"],testUserEntityRights=arguments.testUserEntityRights)>
	</cffunction>


	<!--- NJH 2009-05-22 P-SNY063 --->
	<!--- YMA 2014-01-21 Case 438616 allow text fields to save as empty string --->
	<cffunction name="updateOrganisationDetails" access="public" returntype="struct" hint="Updates an organisation's details" output="false">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="organisationDetails" type="struct" required="true">
		<cfargument name="testUserEntityRights" type="boolean" default="false">
		<cfargument name="treatEmtpyFieldAsNull" type="boolean" required="false" default="true">

		<cfreturn updateEntityDetails(entityID=arguments.organisationID,entityDetails=arguments.organisationDetails,entityTypeID=application.entityTypeID["organisation"],testUserEntityRights=arguments.testUserEntityRights,treatEmtpyFieldAsNull=arguments.treatEmtpyFieldAsNull)>
	</cffunction>


	<cffunction name="updateLeadDetails" access="public" returntype="struct" hint="Updates a lead's details" output="false">
		<cfargument name="leadID" type="numeric" required="true">
		<cfargument name="leadDetails" type="struct" required="true">
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfset var oppUpdateResult = structNew()>

		<cfset oppUpdateResult = updateEntityDetails(entityID=arguments.leadID,entityDetails=arguments.leadDetails,entityTypeID=application.entityTypeID["lead"],testUserEntityRights=arguments.testUserEntityRights, customPostRightsCheck=new leadConvertedChecker())>

		<cfreturn oppUpdateResult>

	</cffunction>

	<cffunction name="updateOpportunityDetails" access="public" returntype="struct" hint="Updates an opportunity's details" output="false">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="opportunityDetails" type="struct" required="true">
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfset var oppUpdateResult = structNew()>
		<cfset var getCurrentStage = "">

		<!--- get probability from OppStage and update opportunityDetails structure --->
		<cfif structKeyExists(arguments.opportunityDetails,"STAGEID")>
			<cfset arguments.opportunityDetails.probability = application.com.opportunity.getOppProbability(StageID=arguments.opportunityDetails.STAGEID)>
		<cfelse>
			<!--- WAB CASE 443542 Problems when called from API
				We should not set probability to 0 if stageID is not passed in - this has the effect of resetting the probability if just a single field is updated.
				<cfset arguments.opportunityDetails.probability = 0>
			--->
		</cfif>

		<!--- <cfif structKeyExists(arguments.opportunityDetails,"stageID")>
			<cfquery name="getCurrentStage" datasource="#application.siteDataSource#">
				select stageID from opportunity where opportunityID=<cf_queryParam value="#arguments.opportunityID#" cfSqlType="cf_sql_integer">
			</cfquery>

			<cfif arguments.opportunityDetails.stageID neq getCurrentStage.stageID>
				<cfset application.com.service.addRelayActivity(action="stageChanged",performedOnEntityID=arguments.opportunityID,performedOnEntityTypeID=application.entityTypeID.opportunity,share=false,url="#application.com.relayCurrentSite.getExternalSiteURL()#/?eid=oppEdit&opportunityID=#arguments.opportunityID#",insertIfExists=true)>
			</cfif>
		</cfif> --->

		<cfset oppUpdateResult = updateEntityDetails(entityID=arguments.opportunityID,entityDetails=arguments.opportunityDetails,entityTypeID=application.entityTypeID["opportunity"],testUserEntityRights=arguments.testUserEntityRights)>

		<!--- 2015/02/05 NJH Moved the setting of the EndCustomerOppContacts flag into the opportunity IU trigger.
		<cfif oppUpdateResult.isOK>
			<cfif structKeyExists(arguments.opportunityDetails,"ContactPersonId") and arguments.opportunityDetails.ContactPersonId neq 0 and isNumeric(arguments.opportunityDetails.ContactPersonId) and structKeyExists(arguments.opportunityDetails,"PartnerSalesPersonId") and arguments.opportunityDetails.PartnerSalesPersonId neq 0 and isNumeric(arguments.opportunityDetails.PartnerSalesPersonId)>
				<cfset application.com.flag.setFlagData(flagId='EndCustomerOppContacts',entityid=arguments.opportunityDetails.ContactPersonId,data=arguments.opportunityDetails.PartnerSalesPersonId)>
			</cfif>
		</cfif> --->

		<cfreturn oppUpdateResult>

	</cffunction>

	<cffunction name="getCrudColumns" returnType="string">
		<cfreturn "created,createdBy,createdByPerson,lastUpdated,lastUpdatedBy,lastupdatedbyPerson">
	</cffunction>


	<!--- 2016-10-20	WAB	PROD2016-2508  this function was returning non numeric answers
s							a) it was sometimes returning relayCurrentUser.countryList
							b) if entityDetails contained a countryID key which was blank (not sure why this would be, but the salesforce/API t)
		--->
	<cffunction name="establishCountryIDForEntity" returnType="numeric" hint="Returns the ''best guess' at what country should be used for validation of an entity. It may be a single country or a list of possibles" >
		<cfargument name="entityTypeID" type="numeric" required="false">
		<cfargument name="entityID" type="numeric" default=0> <!--- May be zero for updates--->
		<cfargument name="entityDetails" type="struct" required="true">


		<cfscript>

			if (structKeyExists(entityDetails, "countryID") and isNumeric(entityDetails.countryID)){
				return 	entityDetails.countryID;
			}else{
				if (entityID!=0){
					//go to the db to find out the ID
					var existingEntity =getEntity(entityTypeID=entityTypeID,entityID=entityID, fieldList="countryID" );


					if (existingEntity.isOk and structKeyExists(existingEntity.recordSet,"countryID")){
						return existingEntity.recordSet.countryID != ""?existingEntity.recordSet.countryID:0;
					}else if (isDefined("request.relayCurrentUser.countryID")){
						return request.relayCurrentUser.countryID;
					}else{
						return 0; //a fall back for very early interactions in the request cycle
					}
				}else if (isDefined("request.relayCurrentUser.countryID")){
					return request.relayCurrentUser.countryID;
				}else{
					return 0; //a fall back for very early interactions in the request cycle
				}

			}

		</cfscript>

	</cffunction>

	<!--- This function cleans up and prepares the entity structure for insert/update --->
	<cffunction name="prepareEntityStruct" access="public" returntype="struct" output="false">
		<cfargument name="entityTypeID" type="numeric" required="false">
		<cfargument name="table" type="string" required="false">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="method" type="string" default="insert">
		<cfargument name="columnsToUse" type="string" default="">
		<cfargument name="rightsStruct" type="struct" default="#structNew()#"> <!--- this is for field level rights --->
		<cfargument name="testUserEntityRights" type="boolean" default="false"> <!--- this is record level rights.. checked for insert and update --->
		<cfargument name="testLoggedInUserRights" type="boolean" default="true" hint="By default only the records the logged in user has rights to see are returned, if false is set in this parameter all relevant records are returned">
		<cfargument name="entityID" type="numeric" default=0> <!--- passed in when doing an update.. used for checking rights on a given field. For example, can we change the given users password --->

 		<cfscript>
			var result = {errorCode="",message="",isOK=true};
			var crudColumns = getCrudColumns();
			var tablename = "";
			var entityDetailsStruct = transposeEntityStructToCoreStruct(entityTypeID=arguments.entityTypeID,entityDetails=arguments.entityDetails);
			var tableDetailsStruct = structNew();
			var entityTableDetails = "";
			var uniqueKey = "";
			var field = "";
			var column = "";
			var friendlyFieldName = "";
			var messageTextId = "phr_entity_insertError";
			var mergeStruct = structNew();
			var message = "";
			var rights = arguments.rightsStruct;
			var functionArgs = structNew();
			var uniqueEmailValidation =structNew();
			var getOrgType = "";
			var columnList = "";
			var flagList = "";
			var flagGroupList = "";
			var tableFieldStruct = "";
			var flagDetailsStruct = "";
			var flagGroupDetailsStruct = "";
			var permission = "view";
			var returnApiName = false;
			var entityDetailKeys = "";
			var translatedColumns = "";

			var countryIDToUseForValidation=application.com.relayEntity.establishCountryIDForEntity(EntityTypeID=entityTypeID, entityID=entityID, entityDetails=entityDetails);

			if (not structKeyExists(rights,"edit")) {
				rights.edit = true;
			}
			if (not structKeyExists(rights,"view")) {
				rights.view = true;
			}

			/* WAB 2012-04-12 in passing, added lastUpdatedByPerson crudcolumn */
			if (arguments.method eq "update") {
				crudColumns = "lastUpdated,lastUpdatedBy,lastUpdatedByPerson";
			}

			if (not structKeyExists(arguments,"entityTypeID") and not structKeyExists(arguments,"table")) {
				result.message = "Either an entityTypeID or a table name must be passed in";
				result.isOK = false;
				return result;
			} else if (structKeyExists(arguments,"entityTypeID")) {
				tablename = application.entityType[arguments.entityTypeID].tablename;
			} else {
				tablename = arguments.table;
			}

			if (structKeyExists(application.entityTypeID,tablename)) {
				uniqueKey = application.entityType[application.entityTypeID[tablename]].uniqueKey;
			}

			/*this is a place holder for when rights are checked before updating an entity... not sure what they need to be at the moment, so at the moment,
				anyone has rights to edit the entity
				Currently only needed for insert as update/delete and get are done in another function.
			*/
			if (arguments.testUserEntityRights and arguments.method eq "insert") {
				rights = checkEntityRights(entityId=arguments.entityID,entityDetails=arguments.entityDetails,entityTypeID=tablename);

				result.isOkToEdit = rights.edit;
				if (not result.isOkToEdit) {
					result.errorCode = "INSUFFICIENT_RIGHTS";
					result.isOk = false;
					result.entityID = arguments.entityID;
					return result;
				}
			}

			//entityTableDetails = application.com.dbTools.getTableDetails(tableName=tablename,returnColumns=true,returnPK=true,columnName=arguments.columnsToUse);
			//tableDetailsStruct = application.com.structureFunctions.queryToStruct(query=entityTableDetails,key="column_name");
			columnList = arguments.columnsToUse;
			if (columnList eq "") {
				columnList = structKeyList(entityDetailsStruct);
			}
			/* WAB 2012-04-12 CASE 427623 errors inserting records - no crud columns, so added crudcolumns to list of fieldnames.  Hope best place need to ask Nat */
			tableFieldStruct = getTableFieldStructure(tablename=tablename,fieldname=listappend(columnList,crudColumns),returnWrapperStruct=true);
			tableDetailsStruct = tableFieldStruct.table;
			flagDetailsStruct = tableFieldStruct.flag;
			flagGroupDetailsStruct = tableFieldStruct.flagGroup;
		</cfscript>

		<cfif listFindNoCase("update,insert",arguments.method)>
			<cfset permission = "edit">
		</cfif>

		<cftry>
			<cfif arguments.method neq "doesEntityExist">
				<!--- if a field that is passed through isn't a valid column name or if the field is the primary key, then delete the field --->
				<cfset entityDetailKeys = structKeyList(entityDetailsStruct)>

				<cfloop list="#entityDetailKeys#" index="field">

					<cfif right(field,19) eq "_defaultTranslation">
						<cfset translatedColumns = listAppend(translatedColumns,field)>
					</cfif>

					<!--- if it's not a valid column or profile, get rid of the column --->
					<cfif not structKeyExists(tableDetailsStruct,field)>
						<cfif structKeyExists(flagDetailsStruct,field)>
							<cfset flagList = listAppend(flagList,field)>
						<cfelseif structKeyExists(flagGroupDetailsStruct,field)>
							<cfset flagGroupList = listAppend(flagGroupList,field)>
						<cfelse>
							<cfset result.isOK = false>
							<cfset result.field = field>
							<cfset result.errorCode = "INVALID_FIELD">
							<cfreturn result>
						</cfif>

					</cfif>

					<!--- Check person rights on field level for selects and inserts/updates --->
					<cfif testLoggedInUserRights and field neq uniqueKey and not application.com.rights.doesUserHaveRightsToEntityField(entityType=tablename,entityID=arguments.entityID,permission=permission,field=field,entityRights=rights,method=method)>
						<cfset result.errorCode = "INSUFFICIENT_RIGHTS">
						<cfset result.isOK = false>
						<cfset result.field = field>
						<cfif request.relayCurrentUser.isApiUser>
							<cfset result.field = getTableFieldStructure(tablename=tablename,fieldname=field).apiName>
						</cfif>
						<cfreturn result>
					</cfif>

					<cfif arguments.method neq "select">

						<!--- remove the primary key as we don't want to update or insert a primary key --->
						<cfif field eq uniqueKey>
							<cfset structDelete(entityDetailsStruct,field)>
						</cfif>

						<!--- if the incoming field is empty but it is required, then delete it --->
						<cfif structKeyExists(entityDetailsStruct,field) and len(entityDetailsStruct[field]) eq 0 and structKeyExists(tableDetailsStruct,field) and not tableDetailsStruct[field].isNullable and not listFindNoCase("text,ntext,varchar,nvarchar",tableDetailsStruct[field].data_type)>
							<cfset structDelete(entityDetailsStruct,field)>
						</cfif>

						<!--- START 2013-11-25 PPB Case 438158 if the incoming field is a Computed column, then delete it --->
						<cfif structKeyExists(tableDetailsStruct,field) and structKeyExists(tableDetailsStruct[field],"isComputed") and tableDetailsStruct[field].isComputed>
							<cfset structDelete(entityDetailsStruct,field)>
						</cfif>
						<!--- END 2013-11-25 PPB Case 438158 --->

						<!--- if we're doing an update but we don't have rights to edit the field, then remove the field from the incoming structure --->
						<!--- This is now done above...
						<cfif arguments.method eq "update">
							<cfif not application.com.rights.doesUserHaveRightsToEntityField(entityType=tablename,entityID=arguments.entityID,permission="edit",field=field,entityRights=rights)>
								<cfset structDelete(entityDetailsStruct,field)>
							</cfif>
						</cfif> --->
					</cfif>
				</cfloop>
			</cfif>

			<cfif structCount(entityDetailsStruct) eq 0>
				<cfset result.message = "No valid fields have been passed.">
				<cfset result.isOK = false>
				<cfset result.errorCode = "NO_VALID_FIELDS">
				<cfreturn result>
			</cfif>

			<!--- NJH 2012/03/23 - to do a select or a doesEntityExist, we just want to get the columns --->
			<cfif not listFindNoCase("doesEntityExist,select",arguments.method)>

				<!--- CASE 426744: NJH 2012-02-23 Unique Email Validation Check --->
				<cfif tablename eq "person" and structKeyExists(entityDetailsStruct,"email") and entityDetailsStruct.email neq "" and arguments.method neq "doesEntityExist">		<!--- 2012-03-01 PPB/NJH Case 426946 added arguments.method neq "doesEntityExist"  --->
					<cfset uniqueEmailValidation = application.com.relayPLO.isUniqueEmailValidationOn()>

					<cfif uniqueEmailValidation.isOn>

						<cfset functionArgs.emailAddress = entityDetailsStruct.email>
						<cfif arguments.method eq "update">
							<cfset functionArgs.personID = arguments.entityID>
						<cfelse>
							<cfset functionArgs.personID = 0>
						</cfif>

						<cfquery name="getOrgType" datasource="#application.siteDataSource#">
							select organisationTypeId from organisation o with (noLock)
							<cfif arguments.method eq "update">
									inner join person p with (noLock) on p.organisationID = o.organisationID
								where p.personID =  <cf_queryparam value="#functionArgs.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
							<cfelse>
								<cfif structKeyExists(entityDetailsStruct,"organisationID")>
									where o.organisationID =  <cf_queryparam value="#entityDetailsStruct.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
								<cfelseif structKeyExists(entityDetailsStruct,"locationID")>
									inner join location l on l.organisationID = o.organisationID
									where l.locationID =  <cf_queryparam value="#entityDetailsStruct.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfif>
							</cfif>
						</cfquery>
						<cfset functionArgs.orgType = getOrgType.organisationTypeID>

						<cfif not application.com.relayPLO.isEmailAddressUnique(argumentCollection=functionArgs).isOK>
							<cfset result.message = replace(application.com.relayTranslations.translatePhrase(phrase=uniqueEmailValidation.uniqueEmailMessage),"\n","")>
							<cfset result.errorCode = "NON_UNIQUE_EMAIL">
							<cfset result.isOK = false>
							<cfreturn result>
						</cfif>
					</cfif>
				</cfif>

				<cfloop collection="#tableDetailsStruct#" item="column">
					<!--- set values for the crud columns if they exist in the table but not in the incoming structure --->
					<cfif listFindNoCase(crudColumns,column) and not structKeyExists(entityDetailsStruct,column)>
						<cfswitch expression="#column#">
							<cfcase value="created,lastUpdated">
								<cfset entityDetailsStruct[column] = request.requestTime>
							</cfcase>
							<cfcase value="createdBy,lastUpdatedBy">
								<cfif isDefined("request.relayCurrentUser.userGroupID")>
									<cfset entityDetailsStruct[column] = request.relayCurrentUser.userGroupID>
								<cfelse>
									<cfset entityDetailsStruct[column] = 404>
								</cfif>
							</cfcase>
							<!--- WAB 2012-04-12 in passing, added lastUpdatedByPerson crudcolumn --->
							<cfcase value="lastUpdatedByPerson,createdByPerson">
								<cfif isDefined("request.relayCurrentUser.PersonID")>
									<cfset entityDetailsStruct[column] = request.relayCurrentUser.PersonID>
								<cfelse>
									<cfset entityDetailsStruct[column] = 404>
								</cfif>
							</cfcase>
						</cfswitch>
					</cfif>

					<!--- if we're inserting, we need to make sure that all required fields that do not have a default are passed through --->
					<cfif (arguments.method eq "insert")>
						<cfif (not tableDetailsStruct[column].isNullable and
								not tableDetailsStruct[column].hasDefault and
								column neq uniqueKey and
								(not structKeyExists(entityDetailsStruct,column) or (structKeyExists(entityDetailsStruct,column) and len(entityDetailsStruct[column]) eq 0)) and
								not tableDetailsStruct[column].isIdentity)>

							<cfset friendlyFieldName = getFriendlyNameForField(table=tablename,fieldname=column)>
							<cfset mergeStruct.fieldname=friendlyFieldName>
							<cfset mergeStruct.tablename=tablename>

							<cfif structKeyExists(request,"errorMsgPhraseTextPrefix") and application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID="#request.errorMsgPhraseTextPrefix#_entity_insertError")>
								<cfset messageTextId = "phr_#request.errorMsgPhraseTextPrefix#_entity_insertError">
							</cfif>
							<!--- START 2011-05-25 NYB REL106 added --->
							<cfset message = application.com.relayTranslations.translatePhrase(phrase=messageTextId,mergeStruct=mergeStruct)>
							<cfset message = "#message#. Could not insert #tablename# record as the #friendlyFieldName# has not been set.">
							<!--- END 2011-05-25 NYB REL106 --->
							<cfset result.message = message>
							<cfset result.errorCode = "MISSING_REQUIRED_FIELD">
							<cfset result.field = column>
							<cfset result.isOK = false>
							<cfreturn result>
						</cfif>
					</cfif>

					<!--- delete the identity column so that we don't attempt to insert it. --->
					<cfif tableDetailsStruct[column].isIdentity>
						<cfset structDelete(entityDetailsStruct,column)>
					</cfif>
				</cfloop>

				<!--- NJH 2012/05/02 do some data validation - mainly done for the API. This checks that if we're updating the location/organsiationID that they point to valid --->
				<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser>
					<cfset returnApiName = true>

					<cfset structAppend(result,validateData(entityDetails=entityDetailsStruct,entityTypeID=tableName,returnApiName=returnApiName,method=arguments.method, countryIDPerspective=countryIDToUseForValidation),true)>
					<cfif not result.isOK>
						<cfreturn result>
					</cfif>

					<cfset structAppend(result,validateForeignKeys(entityDetails=entityDetailsStruct,returnApiName=returnApiName,entityTypeId=arguments.entityTypeID),true)>
					<cfif not result.isOK>
						<cfreturn result>
					</cfif>
				</cfif>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				<cfset result.errorCode = "UNKNOWN_EXCEPTION">
			</cfcatch>
		</cftry>

		<cfset result.entityDetailsStruct = entityDetailsStruct>
		<cfset result.tableDetailsStruct = tableDetailsStruct>
		<cfset result.flagList = flagList>
		<cfset result.flagGroupList = flagGroupList>
		<cfset result.crudColumns = crudColumns>
		<cfset result.translatedColumns = translatedColumns>

		<cfreturn result>

	</cffunction>



	<!--- NJH 2009-05-27 P-SNY063
		NJH 2016/09/01 JIRA PROD125 - removed the call to get next assignedEntityId for POL data
	 --->
	<cffunction name="insertEntity" access="public" hint="Insert an entity" returntype="struct" output="false">
		<cfargument name="entityTypeID" type="numeric" required="false">
		<cfargument name="table" type="string" required="false">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="matchEntityColumns" type="string" default="">
		<cfargument name="updateMatchNamesForExistenceCheck" type="boolean" default="true"> <!--- CASE:426374 NJH 2012-02-23 - don't update orgmatchnames on a bulk insert --->
		<cfargument name="testUserEntityRights" type="boolean" default="false">
		<cfargument name="testLoggedInUserRights" type="boolean" default="true">
		<cfscript>
			var insertData = "";
			var result = {entityID=0,exists=false,entitiesMatchedList="",isOk=true,message="",errorCode=""};
			var valueToInsert = "";
			var firstField = 0;
			var entityExistsStruct = structNew();
			//var newID = 0;
			var preparedEntityStruct = structNew();
			var entityDetailsStruct = structNew();
			var tableDetailsStruct = structNew();
			var flagList = "";
			var flagGroupList = "";
			var field = "";
			var functionArgs = {columnsToUse=arguments.matchEntityColumns,updateMatchNamesForExistenceCheck=arguments.updateMatchNamesForExistenceCheck};
			var isOkToEdit = true;
			var rightsStruct = {edit=true,view=true};
			var entityTypeStruct = structNew();
			var thisEntityType = "";
			var translatedColumns = "";
		</cfscript>

		<!--- test the user rights on the entity --->
		<!--- <cfif arguments.testUserRights>
			<cfset rightsStruct = checkEntityRights(entityId=0,entityDetails=arguments.entityDetails,entityTypeID=preparedEntityStruct.tablename)>
			<cfset isOkToEdit = rightsStruct.edit>
		</cfif>

		<cfif isOkToEdit> --->
			<cfif not structKeyExists(arguments,"table") and structKeyExists(arguments,"entityTypeID")>
				<cfset thisEntityType = application.entityType[arguments.entityTypeID].tablename>
			<cfelse>
				<cfset thisEntityType = arguments.table>
			</cfif>
			<cfset entityTypeStruct = getEntityType(entityTypeID=thisEntityType)>
			<cfif not structKeyExists(arguments,"entityTypeID")>
				<cfset arguments.entityTypeID = entityTypeStruct.entityTypeID>
			</cfif>

			<cfset preparedEntityStruct = prepareEntityStruct(method="insert",argumentCollection=arguments,testUserEntityRights=arguments.testUserEntityRights,testLoggedInUserRights=arguments.testLoggedInUserRights)>

			<cfif not preparedEntityStruct.isOk>
				<cfset result.errorCode = preparedEntityStruct.errorCode>
				<cfif structKeyExists(preparedEntityStruct,"field")>
					<cfset result.field = preparedEntityStruct.field>
				</cfif>
				<cfif structKeyExists(preparedEntityStruct,"fieldValue")>
					<cfset result.fieldValue = preparedEntityStruct.fieldValue>
				</cfif>
				<cfset result.isOK = false>
				<!--- Start 2014-08-14 AHL Case 440859 SFDC sync; MISSING_REQUIRED_FIELD error --->
				<cfif trim(preparedEntityStruct.message) neq "">
					<cfset result.message = preparedEntityStruct.message>
				<cfelse>
					<cfset result.message = preparedEntityStruct.errorCode>
				</cfif>
				<!--- End 2014-08-14 AHL Case 440859 --->
				<cfreturn result>
			</cfif>

			<cfset entityDetailsStruct = preparedEntityStruct.entityDetailsStruct>
			<cfset tableDetailsStruct = preparedEntityStruct.tableDetailsStruct>
			<cfset flagList = preparedEntityStruct.flagList>
			<cfset flagGroupList = preparedEntityStruct.flagGroupList>
			<cfset translatedColumns = preparedEntityStruct.translatedColumns>

			<cfif not arguments.insertIfExists>
				<cftry>
					<!--- NJH 2012-02-23 Changed to use cfinvoke rather than evaluate --->
					<cfif structKeyExists(application.com.relayEntity,"does#entityTypeStruct.tablename#Exist")>
						<cfset functionArgs["#entityTypeStruct.tablename#Details"] = entityDetailsStruct>
						<cfinvoke component=#application.com.relayEntity# method="does#entityTypeStruct.tablename#Exist" argumentCollection=#functionArgs# returnvariable="entityExistsStruct">
					<cfelse>
						<cfset functionArgs.EntityTypeID = arguments.entityTypeID>
						<cfset functionArgs.EntityDetails = entityDetailsStruct>
						<cfset entityExistsStruct = doesEntityExist(argumentCollection=functionArgs)>
					</cfif>

					<cfcatch>
						<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
						<cfset result.isOK = false>
					</cfcatch>
				</cftry>

				<cfif structKeyExists(entityExistsStruct,"entityID") and entityExistsStruct.entityID neq 0>
					<cfset result.exists = true>
				</cfif>
			</cfif>

			<cfif not result.exists or arguments.insertIfExists>

				<cf_transaction action="begin">

					<!--- if we're adding date for the POL tables, get the ID via the assigned entityIDs table. --->
					<!--- <cfif listFind("0,1,2",arguments.entityTypeID)>
						<cfset newID = application.com.relayDataload.getAndSetMaxIDForEntity(entityTypeStruct.tablename)>
					</cfif> --->

					<cftry>
						<cfquery name="insertData" datasource="#application.siteDataSource#">
							insert into #entityTypeStruct.tablename#
								 (	<!--- <cfif newID neq 0>#entityTypeStruct.uniqueKey#,</cfif> --->
								 	<cfset firstField = 1>
								 	<cfloop collection="#entityDetailsStruct#" item="field">
									 	<cfif structKeyExists(tableDetailsStruct,field)>
										 	<cfif not firstField>,</cfif>#trim(field)#
										 	<cfset firstField = 0>
										 </cfif>
									</cfloop>
								)
							values
								(	<!--- <cfif newID neq 0>#newID#,</cfif> --->
								<cfset firstField = 1>
								<cfloop collection="#preparedEntityStruct.entityDetailsStruct#" item="field">
									<cfif structKeyExists(tableDetailsStruct,field)>
										<cfif not firstField>,</cfif>
										<cfset firstField = 0>
										<cfset valueToInsert = preparedEntityStruct.entityDetailsStruct[field]>
										<cfif len(valueToInsert) eq 0 and not tableDetailsStruct[field].hasDefault and tableDetailsStruct[field].isNullable>
											null
										<cfelseif len(valueToInsert) eq 0>
											<cfswitch expression="#tableDetailsStruct[field].data_type#">
												<cfcase value="bit,int,float,smallint,numeric,decimal,tinyint">0</cfcase>
												<cfdefaultcase>''</cfdefaultcase>
											</cfswitch>
										<cfelseif len(valueToInsert) gt 0>
											<!--- NJH 2014/09/16 - use non-CFVersion, as the system hanged when trying to insert an entity that errored on a trigger
												in this case, we were inserting a person with a location that was not in the org specified... Haven't figured out why it was causing a problem when using cfqueryparam' --->
											<cf_queryparam value="#valueToInsert#" cfsqltype="#tableDetailsStruct[field].cfsqltype#" useCFVersion="false">
										</cfif>
									</cfif>
								</cfloop>
								)

							<!--- <cfif newID eq 0> --->
								select scope_identity() as insertedID
							<!--- </cfif> --->
						</cfquery>


						<!--- have to check that insertedID is not an empty string. If there is no unique ID (or identity column), then this will be empty.  --->
						<cfset result.entityID=insertData.insertedID neq ""?insertData.insertedID:0>
						<cfset result.errorCode = "ENTITY_INSERTED">

						<cfif listLen(translatedColumns)>
							<cfset setDefaultTranslations(entityID=result.entityID,entityDetails=entityDetailsStruct,translatedColumns=translatedColumns,entityTypeID=arguments.entityTypeID,mode="insert")>
						</cfif>

						<cfset setProfileData(entityID=result.entityID,flagList=flagList,flagGroupList=flagGroupList,entityDetails=entityDetailsStruct)>

						<cf_transaction action="commit">

						<cfcatch>
							<cfset result.isOK = false>
							<cfset result.message = "Unable to insert #entityTypeStruct.tablename# record. #cfcatch.Message#"><!--- 2014-11-06 AHL Case 442134 better report errors for sf --->
							<cfset result.errorCode = "INSERT_ERROR">

							<cf_transaction action="rollback">

							<cfif isdefined("cfcatch.TagContext") and cfcatch.TagContext[1].ID eq "CFTHROW">
								<cfset result.message = result.message & "  " & cfcatch.message>
							</cfif>
							<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
						</cfcatch>
					</cftry>
					<!--- checks if the entity has approval rules and runs them. --->
					<cfif result.isOK>
						<cfset result.approvalDetails = application.com.approvalEngine.approvalRuleHandler(mode='request', entityTypeID=arguments.entityTypeID, approvalStatusRelatedEntityID=result.entityID) />
					</cfif>
				</cf_transaction>

			<cfelse>

				<!--- the entity already exists; pass the entityID back --->
				<cfset result.message = entityExistsStruct.message>
				<cfset result.entityID = entityExistsStruct.entityID>
				<cfset result.isOK = entityExistsStruct.isOK>
				<cfset result.errorCode = "ENTITY_EXISTS">
				<cfset result.entitiesMatchedList = entityExistsStruct.entitiesMatchedList>
			</cfif>

		<!--- don't have rights to insert this record --->
		<!--- <cfelse>
			<cfset result.isOK = false>
			<cfset result.errorCode = "INSUFFICIENT_RIGHTS">
			<cfset result.message = "You do not have sufficient rights to insert this record.">
		</cfif> --->

		<cfreturn result>

	</cffunction>



	<!--- NJH 2009-05-27 P-SNY063 --->
	<cffunction name="insertPerson" access="public" hint="Insert a person record" returntype="struct" output="false">
		<cfargument name="personDetails" type="struct" required="true">
		<cfargument name="setPassword" type="boolean" default="false">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="matchPersonColumns" type="string" default="firstname,lastname,email,locationID">
		<cfargument name="setNewAs" type="string" default="None">
		<cfargument name="testUserEntityRights" type="boolean" default="false">
		<cfargument name="testLoggedInUserRights" type="boolean" default="true">

		<cfscript>
			var result = structNew();
			var userName = "";
			var password = "";
			var getLocationCountry = "";
			var regApprovers = "";
			var getOrganisationType = "";
			var insPerDataSource = '';
			var orgIdColumn = getTableFieldNameForCurrentUser(tablename="person",fieldname="organisationID");
			var locIdColumn = getTableFieldNameForCurrentUser(tablename="person",fieldname="locationID");
			var getOrganisationIDFromLocation = "";

			// setting some required fields if they don't exist.
			if (not structKeyExists(arguments.personDetails,"active")) {
				arguments.personDetails.active=1;
			}
			//if (not structKeyExists(arguments.personDetails,"salutation")) {
			//	arguments.personDetails.salutation=" ";
			//}
			if (not structKeyExists(arguments.personDetails,"passwordDate")) {
				arguments.personDetails.passwordDate=request.requestTime;
			}
			if (not structKeyExists(arguments.personDetails,"firstTimeUser")) {
				arguments.personDetails.firstTimeUser=1;
			}

			if ((not structKeyExists(arguments.personDetails,"firstname")) or (structKeyExists(arguments.personDetails,"firstname") and len(arguments.personDetails.firstname) eq 0)) {
				arguments.personDetails.firstName=" ";
			}

			// if the username and password exist in the incoming structure, use them when creating the username and password
			if (structKeyExists(arguments.personDetails,"username")) {
				username = arguments.personDetails.username;
			}
			if (structKeyExists(arguments.personDetails,"password")) {
				password = arguments.personDetails.password;
			}
		</cfscript>

		<cfif not structKeyExists(arguments.personDetails,orgIdColumn) and structKeyExists(arguments.personDetails,locIdColumn)>
			<cfquery name="getOrganisationIDFromLocation" datasource="#application.siteDataSource#">
				select organisationID from location where locationID = <cf_queryparam value="#arguments.personDetails[locIdColumn]#" CFSQLTYPE="CF_SQL_INTEGER">
			</cfquery>

			<cfif getOrganisationIDFromLocation.recordCount>
				<cfset arguments.personDetails[orgIdColumn] = getOrganisationIDFromLocation.organisationID>
			</cfif>
		</cfif>

		<cfset result = insertEntity(entityTypeID=0,entityDetails=arguments.personDetails,insertIfExists=arguments.insertIfExists,matchEntityColumns=arguments.matchPersonColumns,testUserEntityRights=arguments.testUserEntityRights,testLoggedInUserRights=arguments.testLoggedInUserRights)>

		<cfif arguments.setPassword and result.entityID neq 0 and result.isOK and not result.exists>
			<cf_createUserNameAndPassword personId="#result.entityID#" username="#username#" password="#password#">
		</cfif>

		<cfif not result.exists and result.isOK and result.entityID neq 0>

			<cfquery name="insPerDataSource" datasource="#application.siteDataSource#">
				insert into PersonDataSource(PersonID, DataSourceID, RemoteDataSourceID)
				values(<cf_queryparam value="#result.entityId#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</cfquery>

			<cfif arguments.setNewAs neq "None">
				<cfquery name="getOrganisationType" datasource="#application.siteDataSource#">
					select typeTextId from organisationType ot inner join organisation o
						on ot.organisationTypeId = o.organisationTypeId
					where organisationID =  <cf_queryparam value="#personDetails.organisationId#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfif getOrganisationType.recordCount eq 1 and getOrganisationType.typeTextId neq "EndCustomer">
					<cfset application.com.flag.setFlagData(entityID=result.entityId,flagID='per#arguments.setNewAs#')>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>



		<!--- NJH 2009-05-27 P-SNY063
			NJH 2014/05/14 - Salesforce connector - default the accountTypeId for locations to default to the organisation type.
		--->
	<cffunction name="insertLocation" access="public" hint="Insert a location record" returntype="struct" output="false">
		<cfargument name="locationDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="updateMatchName" type="boolean" default="true">
		<cfargument name="matchLocationColumns" type="string" default="sitename,address1,address4,postalCode,organisationID,countryID">
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfscript>
			var insLocDataSource = "";
			var result = {message="",isOk=true};
			var getOrganisationDetails = "";
			var organisationID = 0;
			var orgIdColumn = getTableFieldNameForCurrentUser(tablename="location",fieldname="organisationID");
			var siteNameColumn = getTableFieldNameForCurrentUser(tablename="location",fieldname="sitename");
			var accountTypeColumn = getTableFieldNameForCurrentUser(tablename="location",fieldname="accountTypeID");
		</cfscript>

		<cfif ((not structKeyExists(arguments.locationDetails,siteNameColumn) or not structKeyExists(arguments.locationDetails,accountTypeColumn)) and structKeyExists(arguments.locationDetails,orgIdColumn))>
			<cfquery name="getOrganisationDetails" datasource="#application.siteDataSource#">
				select organisationName,organisationTypeID from organisation where organisationID =  <cf_queryparam value="#arguments.locationDetails[orgIdColumn]#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfif not structKeyExists(arguments.locationDetails,siteNameColumn)>
				<cfset arguments.locationDetails[siteNameColumn] = getOrganisationDetails.organisationName>
			</cfif>

			<!--- NJH 2014/04/07 Salesforce connector - get accountType from organisation if it is not passed in --->
			<!--- AHL 2015/09/17 API - The previous change will not work if the accountTypeColumn is empty --->
			<cfif accountTypeColumn neq "" AND  not structKeyExists(arguments.locationDetails,accountTypeColumn)>
				<cfset arguments.locationDetails[accountTypeColumn] = getOrganisationDetails.organisationTypeID>
			</cfif>
		</cfif>

		<cfset result = insertEntity(entityTypeID=1,entityDetails=arguments.locationDetails,insertIfExists=arguments.insertIfExists,matchEntityColumns=arguments.matchLocationColumns,testUserEntityRights=arguments.testUserEntityRights)>

		<cfif result.entityID neq 0 and result.isOK and not result.exists>
			<cfquery name="insLocDataSource" datasource="#application.siteDataSource#">
				insert into LocationDataSource(LocationID, DataSourceID, RemoteDataSourceID)
				values(<cf_queryparam value="#result.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</cfquery>

			<cfif arguments.updateMatchName>
				<cfset application.com.matching.updateMatchFields(entityType="location",tablename="location",whereClause="organisationID="&arguments.locationDetails[orgIdColumn])>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>



	<!--- NJH 2009-05-27 P-SNY063 --->
	<cffunction name="insertOrganisation" access="public" hint="Insert an organisation record" returntype="struct" output="false">
		<cfargument name="organisationDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="updateMatchName" type="boolean" default="true">
		<cfargument name="matchOrganisationColumns" type="string" default="organisationName,countryID">
		<cfargument name="setNewAs" type="string" default="None">
		<cfargument name="updateMatchNamesForExistenceCheck" type="boolean" default="true"> <!--- CASE:426374 NJH 2012-02-23 --->
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfscript>
			var insOrgDataSource = "";
			var result = "";
			var getOrganisationType = "";
			var thisOrganisationTypeID = "";
		</cfscript>

		<cfset result = insertEntity(entityTypeID=2,entityDetails=arguments.organisationDetails,insertIfExists=arguments.insertIfExists,matchEntityColumns=arguments.matchOrganisationColumns,updateMatchNamesForExistenceCheck=arguments.updateMatchNamesForExistenceCheck,testUserEntityRights=arguments.testUserEntityRights)>

		<cfif result.entityID neq 0 and result.isOK and not result.exists>
			<cfquery name="insOrgDataSource" datasource="#application.siteDataSource#">
				insert into OrgDataSource(organisationID, DataSourceID, RemoteDataSourceID)
				values(<cf_queryparam value="#result.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</cfquery>

			<cfif arguments.updateMatchName>
				<!---<cfset updateExistingOrgs = application.com.dbTools.updateOrgMatchname(organisationID=result.entityID)>--->
				<cfset application.com.matching.updateMatchFields(tablename="organisation",entityType="organisation",entityID=result.entityID)>
			</cfif>

			<cfif arguments.setNewAs neq "None">

				<cfif structKeyExists(arguments.organisationDetails,"organisationTypeID") and arguments.organisationDetails.organisationTypeId neq 0 and arguments.organisationDetails.organisationTypeId neq "">
					<cfquery name="getOrganisationType" datasource="#application.siteDataSource#">
						select typeTextId from organisationType where organisationTypeID =  <cf_queryparam value="#arguments.organisationDetails.organisationTypeId#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

					<cfif getOrganisationType.recordCount eq 1 and getOrganisationType.typeTextId neq "EndCustomer">
						<cfset application.com.flag.setFlagData(entityID=result.entityId,flagID="org#arguments.setNewAs#")>
					</cfif>
				</cfif>
				<!--- NJH commented this out below as apparently the ToList for this email is a phrase; so only need to send once --->
				<!--- <cfset regApprovers = application.com.rights.getFlagGroupRightsByCountry(flagID='orgApprovalStatus',countryId=arguments.organisationDetails.countryId)>

				<cfloop query="regApprovers">
					<cfset application.com.email.sendEmail(personID=personID,emailTextId="RegistrationNewOrgNotification",mergeStruct=arguments.organisationDetails)>
				</cfloop> --->
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="insertLead" access="public" hint="Insert a lead record" returntype="struct" output="false">
		<cfargument name="leadDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="true">
		<cfargument name="matchOpportunityColumns" type="string" default="">
		<cfargument name="testUserEntityRights" type="boolean" default="false">



		<cfscript>
			var result=insertEntity(entityTypeID=application.entityTypeID["lead"],entityDetails=arguments.leadDetails,insertIfExists=arguments.insertIfExists,matchEntityColumns=arguments.matchOpportunityColumns,testUserEntityRights=arguments.testUserEntityRights);
		</cfscript>

		<cfreturn result>
	</cffunction>


	<cffunction name="insertOpportunity" access="public" hint="Insert an opportunity record" returntype="struct" output="false">
		<cfargument name="opportunityDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="true">
		<cfargument name="matchOpportunityColumns" type="string" default="">
		<cfargument name="testUserEntityRights" type="boolean" default="false">

		<cfscript>
			var opportunityInsResult = structNew();

			// default some fields
			if (not structKeyExists(arguments.opportunityDetails,"oppTypeID")) {
				arguments.opportunityDetails.oppTypeID = 1;
			}

			// get probability from OppStage and update opportunityDetails structure
			if (structKeyExists(arguments.opportunityDetails,"STAGEID")) {
				arguments.opportunityDetails.probability = application.com.opportunity.getOppProbability(StageID=arguments.opportunityDetails.STAGEID);
			}
			else
			{
				arguments.opportunityDetails.probability = 0;
			}

			opportunityInsResult = insertEntity(entityTypeID=application.entityTypeID["opportunity"],entityDetails=arguments.opportunityDetails,insertIfExists=arguments.insertIfExists,matchEntityColumns=arguments.matchOpportunityColumns,testUserEntityRights=arguments.testUserEntityRights);
		</cfscript>

		<!--- 2015/02/05 NJH Moved the setting of the EndCustomerOppContacts flag into the opportunity IU trigger.
		<cfif opportunityInsResult.isOK>
			<cfif structKeyExists(arguments.opportunityDetails,"ContactPersonId") and arguments.opportunityDetails.ContactPersonId neq 0 and isNumeric(arguments.opportunityDetails.ContactPersonId) and structKeyExists(arguments.opportunityDetails,"PartnerSalesPersonId") and arguments.opportunityDetails.PartnerSalesPersonId neq 0 and isNumeric(arguments.opportunityDetails.PartnerSalesPersonId)>
				<cfset application.com.flag.setFlagData(flagId='EndCustomerOppContacts',entityid=arguments.opportunityDetails.ContactPersonId,data=arguments.opportunityDetails.PartnerSalesPersonId)>
			</cfif>
		</cfif> --->

		<cfreturn opportunityInsResult>
	</cffunction>


	<cffunction name="insertProduct" access="public" hint="Insert a product record" returntype="struct" output="false">
		<cfargument name="productDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="false">
		<cfargument name="matchProductColumns" type="string" default="campaignID,countryID,SKU">

		<cfscript>
			var productInsResult = structNew();

			productInsResult = insertEntity(entityTypeID=application.entityTypeID["product"],entityDetails=arguments.productDetails,insertIfExists=arguments.insertIfExists,matchEntityColumns=arguments.matchProductColumns);
		</cfscript>

		<cfreturn productInsResult>
	</cffunction>


	<!--- NAS 2010-05-25 P-CLS002 --->
	<cffunction name="insertOppProduct" access="public" hint="Insert an opportunity Product record" returntype="struct" output="false">
		<cfargument name="ProductDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="true">

		<cfscript>
			var oppProductInsResult = structNew();

			// default some fields
			if (not structKeyExists(arguments.ProductDetails,"ProductORGroup")) {
				arguments.ProductDetails.ProductORGroup = 'P';
			}

			/* START 2013-11-25 PPB Case 438158 no longer required - done generically
			// 2013-06-18 PPB Case 435741 subtotal is a computed column on the db so should never be inserted
			if (structKeyExists(arguments.ProductDetails,"subtotal")) {
				structDelete(arguments.ProductDetails,"subtotal");
			}
			END 2013-11-25 PPB Case 438158 */

			oppProductInsResult = insertEntity(entityTypeID=application.entityTypeID["opportunityProduct"],entityDetails=arguments.ProductDetails,insertIfExists=arguments.insertIfExists);
		</cfscript>

		<cfreturn oppProductInsResult>
	</cffunction>


	<!--- NYB 2011-01-04.  Created - required function to be named in format insert#tableName# to work from salesforce.cfc --->
	<cffunction name="insertOppProducts" access="public" hint="Insert an opportunity Product record" returntype="struct" output="false">
		<cfargument name="OppProductsDetails" type="struct" required="true">
		<cfargument name="insertIfExists" type="boolean" default="true">

		<cfset var oppProductInsResult = "">
		<cfset arguments.ProductDetails = arguments.OppProductsDetails>

		<cfset oppProductInsResult = insertOppProduct(argumentcollection=arguments)>

		<cfreturn oppProductInsResult>
	</cffunction>

	<!--- NJH 2009-05-22 P-SNY063
		NJH 2010-10-22 P-FNL079 - altered to get all matches, although we set the entityID to be the lastUpdated.
	--->
	<cffunction name="doesEntityExist" access="public" returntype="struct" output="false">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="columnsToUse" type="string" default="#structKeyList(entityDetails)#">
		<cfargument name="excludeSalesForceRecords" type="boolean" default="false">
		<cfargument name="excludeRecordsFlaggedForDeletion" type="boolean" default="false">		<!--- 2014-02-26 PPB Case 439001 --->

		<cfscript>
			var getEntityDetails = "";
			var field = "";
			var fieldValue = "";
			var preparedEntityStruct = prepareEntityStruct(method="doesEntityExist",testUserEntityRights=false,argumentCollection=arguments);
			var entityDetailsStruct = structNew();
			var tableDetailsStruct = structNew();
			var list = false;
			var result = structNew();
			var entityTypeStruct = getEntityType(entityTypeID=arguments.entityTypeID);
			var excludeRecordsFlaggedForDel = "";	//2014-02-26 PPB Case 439001

			result.entityID=0;
			result.isOK = true;
			result.exists = false;
			result.entitiesMatchedList = "";
			result.columnsUsedInMatch=columnsToUse;
		</cfscript>

		<cfif not preparedEntityStruct.isOk>
			<cfset structAppend(result,preparedEntityStruct)>
			<cfreturn result>
		</cfif>

		<!--- if an empty string passed through, then match on every column --->
		<cfif arguments.columnsToUse eq "">
			<cfset arguments.columnsToUse = application.com.globalFunctions.listMinusList(structKeyList(entityDetails),"created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson,#entityTypeStruct.uniqueKey#")>
		</cfif>

		<cfset excludeRecordsFlaggedForDel = (arguments.excludeRecordsFlaggedForDeletion AND ListFindNoCase("Organisation,Location,Person","#entityTypeStruct.tablename#")) >			<!--- 2014-02-26 PPB Case 439001 assign it to a vble to save doing the ListFind twice --->

		<cfset entityDetailsStruct = preparedEntityStruct.entityDetailsStruct>
		<cfset tableDetailsStruct = preparedEntityStruct.tableDetailsStruct>

		<cftry>
			<!--- Entity Details struct is a structure with only the columnsToUse as keys --->
			<cfif structCount(EntityDetailsStruct)>
				<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
					select #entityTypeStruct.uniqueKey# as entityID
					from #entityTypeStruct.tablename#
					<!--- START 2014-02-26 PPB Case 439001 --->
					<cfif excludeRecordsFlaggedForDel>
						left outer join (booleanflagdata bfd inner join flag f on f.flagID = bfd.flagID and f.flagTextID =  <cf_queryparam value="Delete#entityTypeStruct.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > ) on bfd.entityID =  <cf_queryparam value="#entityTypeStruct.uniqueKey#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>
					<!--- END 2014-02-26 PPB Case 439001 --->
					where 1=1
						<cfloop list="#arguments.columnsToUse#" index="field">
							<cfif structKeyExists(tableDetailsStruct,field)>
								<!--- 2011-05-25 NYB REL106 added if len(fieldValue) --->
								<cfset fieldValue = trim(EntityDetailsStruct[field])>
								<cfset list = false>
								<cfif listLen(fieldValue) gt 1>
									<cfset list = true>
								</cfif>
								<cfif len(fieldValue)>
									and ltrim(rtrim(#field#)) <cfif list>in (<cfelse>=</cfif>
									<!--- <cfswitch expression="#tableDetailsStruct[field].data_type#">
										<cfcase value="int"><cfset sqlType = "cf_sql_integer"></cfcase>
										<cfcase value="nvarchar,text,ntext"><cfset sqlType = "cf_sql_varchar"></cfcase>
										<cfcase value="bit"><cfif fieldValue>1<cfelse>0</cfif><cfset sqlType = "cf_sql_bit"></cfcase>
										<cfdefaultcase><cfset sqlType = "cf_sql_#tableDetailsStruct[field].data_type#"></cfdefaultcase>
									</cfswitch> --->
									<cf_queryparam value="#fieldValue#" cfsqlType="#tableDetailsStruct[field].cfsqlType#" list="#list#">
									<cfif list>)</cfif>
								</cfif>
							</cfif>
						</cfloop>
						<cfif arguments.excludeSalesForceRecords>
							AND (len(crm#left(entityTypeStruct.tablename,3)#ID) = 0 or crm#left(entityTypeStruct.tablename,3)#ID is null)  /* this at the moment assumes that the salesForceId column is in the format of crmPerId, crmLocId, etc.. */
						</cfif>
						<!--- START 2014-02-26 PPB Case 439001 --->
						<cfif excludeRecordsFlaggedForDel>
							AND f.FlagId IS NULL
						</cfif>
						<!--- END 2014-02-26 PPB Case 439001 --->
					order by
					/*NYB 2011-05-25 REL106: added if - because assuming lastUpdated exists in all tables renders this query ungeneric*/
					<cfif structkeyexists(EntityDetailsStruct,"lastUpdated ")>
						lastUpdated desc
					<cfelse>
						#entityTypeStruct.uniqueKey# asc
					</cfif>
				</cfquery>

				<cfif getEntityDetails.recordCount>
					<cfset result.entityID = getEntityDetails.entityID[1]>
					<cfset result.message = "#entityTypeStruct.tablename# exists based on #arguments.columnsToUse#">
					<cfset result.exists = true>
					<cfset result.entitiesMatchedList = valueList(getEntityDetails.entityID)>
				</cfif>
			</cfif>

			<cfcatch>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>

	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="doesPersonExist" access="public" returntype="struct" output="false">
		<cfargument name="personDetails" type="struct" required="true">
		<cfargument name="columnsToUse" type="string" default="firstname,lastname,email,locationID">
		<cfargument name="excludeSalesForceRecords" type="boolean" default="false">
		<cfargument name="excludeRecordsFlaggedForDeletion" type="boolean" default="false">		<!--- 2014-02-26 PPB Case 439001 --->

		<!--- <cfreturn doesEntityExist(entityTypeID=0,entityDetails=arguments.personDetails,columnsToUse=arguments.columnsToUse,excludeSalesForceRecords=arguments.excludeSalesForceRecords,excludeRecordsFlaggedForDeletion=arguments.excludeRecordsFlaggedForDeletion)> --->
		<cfset var result = {entityID=0,isOK = true,exists = false,entitiesMatchedList = ""}>
		<cfset var personMatches = application.com.matching.getMatchesForEntityDetails(entityType="person",argumentCollection=arguments.personDetails)>
		<cfif personMatches.recordCount>
			<cfset result = {entityID=personMatches.personID[1],isOK = true,exists = true,entitiesMatchedList = valueList(personMatches.personID),message="Person Exists"}>
		</cfif>
		<cfreturn result>
	</cffunction>


	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="doesLocationExist" access="public" returntype="struct" output="false">
		<cfargument name="locationDetails" type="struct" required="true">
		<!---<cfargument name="columnsToUse" type="string" default="#application.com.dbTools.getLocationMatchnameEntries(stringDelimiter=',')#">--->
		<cfargument name="excludeSalesForceRecords" type="boolean" default="false">

		<cfscript>
			var getOrganisationName = "";
			var result = structNew();
			var updateExistingLocs = "";
			var checkLoc = "";
			var columnName = "";
			var argStruct = structNew();

			//NJH 2012/09/12 use this location struct to do all the checking, as the api user is going to pass through details with different column names. This method returns us a structure that we can work with
			var rwLocationDetails = prepareEntityStruct(method="doesEntityExist",testUserEntityRights=false,entityTypeID=application.entityTypeID.location,entityDetails=arguments.locationDetails).entityDetailsStruct;

			result.entityID = 0;
			result.isOK = true;
			result.exists = false;
			result.entitiesMatchedList = 0;
			result.message = "Location does not exist";

			/*result.columnsUsedInMatch=columnsToUse & ",organisationID";
			result.columnsUsedInMatch=listAppend(result.columnsUsedInMatch,structKeyList(application.com.globalFunctions.checkReqdFunctionArgumentsExist(functionPath="application.com.dbTools.checkLoc",argumentStruct={})));
			result.columnsUsedInMatch=ListRemoveDuplicates(result.columnsUsedInMatch,",",true);*/
			result.columnsUsedInMatch = application.com.matching.getEntityMatchFields(entityType="location");
		</cfscript>

		<cfif structKeyExists(rwLocationDetails,"organisationID") and rwLocationDetails.organisationID neq 0 and rwLocationDetails.organisationID neq "">
			<cfif not structKeyExists(rwLocationDetails,"sitename") and listFindNoCase(arguments.columnsToUse,"siteName")>
				<cfquery name="getOrganisationName" datasource="#application.siteDataSource#">
					select organisationName from organisation where organisationID =  <cf_queryparam value="#rwLocationDetails.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfset rwLocationDetails.sitename = getOrganisationName.organisationName>
			</cfif>

			<!--- <cfreturn doesEntityExist(entityTypeID=1,entityDetails=arguments.locationDetails,columnsToUse=arguments.columnsToUse)> --->

			<!--- NJH 2012/09/12 - API project
				we may need a better way of doing this (matching probably needs a re-write, but address1 and postalCode aren't required fields in the api as the db columns
				are not required. So, if they don't exist at this point, then set them to an empty string so that at least we go into the matching function)
			 --->
			<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser>
				<cfif not structKeyExists(rwLocationDetails,"address1")>
					<cfset rwLocationDetails.address1 = "">
				</cfif>
				<cfif not structKeyExists(rwLocationDetails,"postalCode")>
					<cfset rwLocationDetails.postalCode = "">
				</cfif>
			</cfif>

			<!--- NJH 2016/09/01 JIRA PROD1190 - replaced with below
			<cfset updateExistingLocs = application.com.dbTools.updateLocationMatchname(organisationIDlist=rwLocationDetails.organisationID)>
			<cfif StructCount(application.com.globalFunctions.checkReqdFunctionArgumentsExist(functionPath="application.com.dbTools.checkLoc",argumentStruct=rwLocationDetails)) eq 0>
 --->
				<cfset argStruct = duplicate(rwLocationDetails)>
				<cfset argStruct.getAllMatches=true>

				<!--- <cfset checkLoc = application.com.dbTools.checkLoc(argumentCollection=argStruct)> --->
				<cfset checkLoc = application.com.matching.getMatchesForEntityDetails(entityType="location",argumentCollection=argStruct)>
				<cfif checkLoc.recordcount gt 0>
					<cfset result.entityID = checkLoc.locationID>
					<cfset result.exists = true>
					<cfset result.message = "Location exists">
					<cfset result.entitiesMatchedList = valueList(checkLoc.locationID)>
				</cfif>
			<!--- </cfif> --->
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="doesOrganisationExist" access="public" returntype="struct" output="false">
		<cfargument name="organisationDetails" type="struct" required="true">
		<cfargument name="columnsToUse" type="string" default="organisationName,countryID">
		<cfargument name="excludeSalesForceRecords" type="boolean" default="false">
		<cfargument name="updateMatchNamesForExistenceCheck" type="boolean" default="true"> <!--- CASE:426374 NJH 2012-02-23 On a SalesForce bulk import, we don't want to refresh the org matchname for the country every time. Takes a lot of time and is unnecessary --->

		<cfset var orgMatchKey="">
		<cfset var result = structNew()>
		<cfset var countryDetails = "">
		<cfset var updateExistingOrgs = "">
		<cfset var checkOrg = "">
		<cfset var orgMatchReplace = "">
		<cfset var orgArgs = structNew()>

		<!--- NJH 2012/09/12 use this organisation struct to do all the checking, as the api user is going to pass through details with different column names. This method returns us a structure that we can work with --->
		<cfset var rwOrganisationDetails = prepareEntityStruct(method="doesEntityExist",testUserEntityRights=false,entityTypeID=application.entityTypeID.organisation,entityDetails=arguments.organisationDetails).entityDetailsStruct>

		<cfset result.entityID = 0>
		<cfset result.isOK = true>
		<cfset result.exists = false>
		<cfset result.entitiesMatchedList = 0>
		<cfset result.columnsUsedInMatch=columnsToUse>

		<!--- NJH 2016/09/01 JIRA PROD1190 - replaced with below
		<cfif not structKeyExists(rwOrganisationDetails,"countryID") or (structKeyExists(rwOrganisationDetails,"countryID") and (rwOrganisationDetails.countryID eq 0 or rwOrganisationDetails.countryID eq ""))>
			<cfset result.message = "Country must be passed in when matching organisations">
			<cfset result.isOK = false>
		<cfelseif not structKeyExists(rwOrganisationDetails,"organisationName")>
			<cfset result.message = "organisationName must be passed in when matching organisations">
			<cfset result.isOK = false>
		<cfelse>

			<cfset countryDetails = application.com.commonQueries.getCountry(countryid=rwOrganisationDetails.countryID)>

			<cfif countryDetails.VATrequired>
				<cfif not StructKeyExists(rwOrganisationDetails,"vatNumber")>
					<cfset result.message = "Vat Number must be passed in when matching organisations">
					<cfset result.isOK = false>
				</cfif>
				<cfset orgMatchKey = rwOrganisationDetails.organisationName & application.delim1 & rwOrganisationDetails.vatNumber>
			<cfelseif structKeyExists(request,"orgMatchNameMethodID") and request.orgMatchNameMethodID eq 2>
				<cfif not structKeyExists(rwOrganisationDetails,"OrgURL")>
					<cfset result.message = "Organisation URL must be passed in when matching organisations">
					<cfset result.isOK = false>
				</cfif>
				<cfset orgMatchKey = replace("#rwOrganisationDetails.OrgURL#","http://","")>
			<cfelse>
				<cfset orgMatchKey = rwOrganisationDetails.organisationName>
			</cfif>

			<!--- test orgname for duplicates --->
			<!--- NJH 2007-03-16 replaced insSiteName with orgMatchKey in the call to checkOrgMatchname --->
			<cfscript>
				if (arguments.updateMatchNamesForExistenceCheck) {
					updateExistingOrgs = application.com.dbTools.updateOrgMatchname(countryIDlist=rwOrganisationDetails.countryID);
				}
				orgArgs = {orgName=orgMatchKey,countryID=rwOrganisationDetails.countryID,excludeSalesForceRecords=arguments.excludeSalesForceRecords};
				// 2011-05-19	NAS	LID5947/8 & 6329
				if (structKeyExists(request,"orgMatchNameMethodID") AND (request.orgMatchNameMethodID EQ "3"))
				{
					orgArgs.organisationTypeID=rwOrganisationDetails.OrganisationTypeID;
				}

				checkOrg = application.com.dbTools.checkOrgMatchname(argumentCollection=orgArgs);
			</cfscript> --->

			<cfset checkOrg = application.com.matching.getMatchesForEntityDetails(entityType="organisation",argumentCollection=rwOrganisationDetails)>
			<cfif checkOrg.recordcount gte 1>
				<cfset result.entityID = checkOrg.organisationID>
				<cfset result.exists = true>
				<!--- <cfset result.message = "Organisation exists with matchKey #orgMatchKey# and countryID #rwOrganisationDetails.countryID#"> --->
				<cfset result.message = "Organisation exists">
				<cfset result.entitiesMatchedList = valueList(checkOrg.organisationID)>
			</cfif>
		<!--- </cfif> --->

		<cfreturn result>
	</cffunction>

	<cffunction name="canEntityBeDeleted" access="public" returnType="struct" output="false">
		<cfargument name="entityID" type="numeric">
		<cfargument name="entityTypeID" type="numeric">

		<cfset returnStruct={canBeDeleted=true, reasons_friendlyText=ArrayNew(1), fullReasons=ArrayNew(1)}>

		<cfquery name="checkCanEntityBeDeleted" datasource="#application.sitedatasource#">
			exec deleteEntityCheck @EntityTypeID=<cf_queryparam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">, @EntityID=<cf_queryparam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>

		<cfloop query = "checkCanEntityBeDeleted">
			<cfscript>
				returnStruct.canBeDeleted=false;
				ArrayAppend(returnStruct.fullReasons,{entityID=checkCanEntityBeDeleted.entityID, EXTENDEDINFO=checkCanEntityBeDeleted.EXTENDEDINFO, REASON=checkCanEntityBeDeleted.REASON});
				ArrayAppend(returnStruct.reasons_friendlyText, "#checkCanEntityBeDeleted.REASON# : #checkCanEntityBeDeleted.EXTENDEDINFO#");
			</cfscript>
		</cfloop>

		<cfreturn returnStruct>

	</cffunction>

	<!--- NJH 2009-05-22 P-SNY063 --->
	<cffunction name="deleteEntityOverride" access="public" returntype="struct" hint="Should NOT be used for standard deletions, but only by processes where third party systems dictate that an entity must be deleted" output="false">
		<cfargument name="entityID" type="numeric">
		<cfargument name="entityTypeID" type="numeric">

		<cfscript>
			var getProtectedFlags = "";
			var checkCanEntityBeDeleted="";
			var getEntityFailureReasons="";
			var entityDeleted = "false";
			var result = {isOK=true,errorCode="",reason=""};
		</cfscript>

		<!--- Remove protected flags before deleting --->
		<cfquery name="getProtectedFlags" datasource="#application.sitedatasource#">
			select flagID from flag f with (noLock) inner join flagGroup fg with (noLock) on f.flagGroupID = fg.flagGroupID
			where fg.entityTypeID = <cf_queryparam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER"> and f.protected = 1
		</cfquery>

		<cf_transaction>
			<cfloop query="getProtectedFlags">
				<!--- delete protected flags for the entity--->
				<cfset application.com.flag.deleteFlagData(flagId=getProtectedFlags.flagid,entityid = arguments.entityID)>
			</cfloop>

			<cfquery name="checkCanEntityBeDeleted" datasource="#application.sitedatasource#">
				exec deleteEntityCheck @EntityTypeID=<cf_queryparam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">, @EntityID=<cf_queryparam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>

			<cfquery name="getEntityFailureReasons" dbtype="query">
				select reason, extendedInfo from checkCanEntityBeDeleted where reason <> '' order by reason
			</cfquery>

			<!--- If the org has reasons why it can't be deleted, rollback the flag removal and don't flag for deletion --->
			<cfif getEntityFailureReasons.recordCount gt 0>
				<cf_transaction action="rollback"/>
				<cfset entityDeleted = false>
				<cfset result.isOK = false>
				<cfset result.reason = valueList(getEntityFailureReasons.reason)>
				<cfset result.errorCode = "DELETE_FAILED">
			<cfelse>
				<cfset application.com.flag.setFlagData(flagID='Delete#application.entityType[arguments.entityTypeID].tablename#',entityID=arguments.entityID)>
				<cf_transaction action="commit"/>
				<cfset result.reason = "">
				<cfset entityDeleted = true>
			</cfif>
		</cf_transaction>

		<cfset result.entityDeleted = entityDeleted>
		<cfreturn result>

	</cffunction>


	<cffunction name="getFriendlyNameForField" access="public" hint="Returns the friendly name for a field. Used for messages" output="false">
		<cfargument name="entityTypeID" type="numeric" required="false">
		<cfargument name="table" type="string" required="false">
		<cfargument name="fieldname" type="string" required="true">

		<cfset var thisTableName = "">
		<cfset var friendNameStruct = structNew()>

		<cfset var personStruct = {locationID="location",organisationID="organisation"}>
		<cfset var locationStruct = {organisationID="organisation",countryID="country"}>
		<cfset var organisationStruct = {organisationID="organisation",countryID="country"}>
		<cfset var opportunityStruct = {partnerSalesPersonID="partner",entityID="end customer organisation",contactID="end customer contact",vendorAccountManagerID="account manager",partnerLocationID="partner location"}>
		<cfset var assetStruct = {partnerEntityID="partner",countryID="country",distiEntityId="distributor",entityID="end customer organisation",productID="product",currencyIsoCode="currency"}>

		<cfset friendNameStruct.person = personStruct>
		<cfset friendNameStruct.location = locationStruct>
		<cfset friendNameStruct.organisation = organisationStruct>
		<cfset friendNameStruct.opportunity = opportunityStruct>
		<cfset friendNameStruct.asset = assetStruct>

		<cfif not structKeyExists(arguments,"entityTypeID") and not structKeyExists(arguments,"table")>
			<cfreturn arguments.fieldname>
		</cfif>

		<cfif structKeyExists(arguments,"entityTypeID") and not structKeyExists(arguments,"table")>
			<cfset thisTableName = application.entityType[arguments.entityTypeID].tablename>
		<cfelse>
			<cfset thisTableName = arguments.table>
		</cfif>

		<cfif structKeyExists(friendNameStruct,thisTablename) and structKeyExists(friendNameStruct[thisTablename],arguments.fieldName)>
			<cfreturn friendNameStruct[thisTablename][arguments.fieldName]>
		</cfif>

		<cfreturn arguments.fieldname>
	</cffunction>


	<!--- 2011-05-25 NYB REL106 added getEntityByForeignKey function
		2012-04-16 WAB Don't know what this function is used for (an it appeared not to work anyway) but tidied up some nasty evaluates
	--->
	<cffunction name="getEntityByForeignKey" access="public" returntype="query" output="false">
		<cfargument name="Entity" 		type="string" required="true">
		<cfargument name="ColumnName" 	type="string" required="false">
		<cfargument name="ColumnData" 	type="string" required="true">

		<cfset var getEntity="">
		<cfif not structkeyexists(arguments,"ColumnName")>
			<cfset arguments.ColumnName = application.entitytype[application.entitytypeid[arguments.entity]].UniqueKey>
		</cfif>
		<cfif len(arguments.ColumnName) eq 0>
			<cfset arguments.ColumnName = "#arguments.entity#ID">
		</cfif>

		<cfquery name="getEntity" datasource="#application.sitedatasource#">
			select *
			from #arguments.entity# with (noLock)
			where #arguments.ColumnName# = <cfqueryparam CFSQLType="cf_sql_integer" value="#arguments.ColumnData#" null="false">
		</cfquery>

		<cfreturn getEntity>
	</cffunction>



	<!--- WAB 2008-04-25
	Spend my life writing same query to find out things about an entity such as its organisationid, countryid etc
	This is a generic POL function which returns the salient details of an entity and its parents.
	Based to some extent on customTags\getRecord and code in webservices\entityNavigation.cfc
	At its simplest just gives the parent entityIDs, names and countries
	but can get whole records as well
	 --->
		<cffunction name="getEntityStructure" output="false">
		<cfargument name="entityTypeID" type="numeric" required="yes">
		<cfargument name="entityID"  required="yes">
		<cfargument name="getEntityQueries" type="boolean" default="no">

		<cfset var result = {queries=structNew()}>
		<cfset var getEntityIDs= "">
		<cfset var entityTypeStruct = application.entityType[entityTypeID]>
		<cfset var entityPK = entityTypeStruct.UNIQUEKEY>
		<cfset var tempentityTypeStruct = {}>
		<cfset var getRecord = "">
		<cfset var tempEntityTypeID = 0>
		<cfset var query1 = "">

		<cfif len(entityPK) eq 0>
			<cfset entityPK = "#entityTypeID#id">
		</cfif>

		<cfif not isNumeric(entityID)>
			<!--- WAB 2011-09-27 TND109 Added ability to create blank records of POL data
				does it rather peculiarly (sorry) especially since there is only one defaults structure which is used to make P,O and L records.  Perhaps could have done it nicer with defaults.person,defaults.location etc
			 --->
			<cfset arguments.defaults["#entityTypeStruct.tablename#id"] = entityid>
			<cfset query1 = createDefaultRecord(entityType = entityTypeStruct.tablename,defaults = arguments.defaults)>

			<cfif EntityTypeID is 0>
				<cfset result = getEntityStructure(entitytypeid = 1,entityid = query1.locationid,defaults=arguments.defaults,getEntityQueries=true)>
				<cfset result.queries.person = query1>
			</cfif>

			<cfif EntityTypeID is 1>
				<cfset result = getEntityStructure(entitytypeid = 2,entityid = query1.organisationid,defaults=arguments.defaults,getEntityQueries=true)>
				<cfset result.queries.location = query1>
			</cfif>

			<cfif EntityTypeID is 2>
				<cfset result.queries.organisation = query1>
			</cfif>

			<cfset result.entityType = entityTypeStruct.tablename>
			<cfset result.entityTypeID = EntityTypeID>
			<cfset result.entityID = EntityID>

			<cfreturn result>

		<cfelse>

			<cfif EntityTypeID lte 2>
				 <cfquery name="getEntityIDs" datasource = "#application.sitedatasource#">
					select
						<cfif EntityTypeID lte 1>l.locationid as ID_location,l.locationid as ID_1,sitename as name_location,l.countryID as countryids_location,</cfif>
						<cfif EntityTypeID lte 0>p.personid as ID_person,p.personid as ID_0,firstname + ' ' + lastname as name_person,l.countryID as countryids_person,</cfif>
						o.organisationid as ID_organisation, o.organisationid as ID_2,organisationname as name_organisation,o.countryid as countryids_organisation,
						o.organisationTypeID
					from
						organisation o
						<cfif EntityTypeID lte 1>inner join location l on l.organisationid = o.organisationid</cfif>
						<cfif EntityTypeID IS 0>inner join person p on p.locationid = l.locationid</cfif>
					where
						#left(entityTypeStruct.tablename,1)#.#entityTypeStruct.tablename#ID  =  <cf_queryparam value="#entityID#" CFSQLTYPE="cf_sql_integer" >
				 </cfquery>
			<cfelse>
				 <cfquery name="getEntityIDs" datasource = "#application.sitedatasource#">
					select
						#entityPK# as ID_#entityTypeStruct.TableName#, #entityPK# as ID_2
						<cfif entityTypeID eq 3>,CountryDescription as name_country, countryid as countryids_country </cfif>
						<cfif entityTypeStruct.tablename eq "LocatorDef">,CountryGroupid as countryids_#entityTypeStruct.tablename# </cfif>
					from
						#entityTypeStruct.TableName#
					where
						#entityPK#  =  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="cf_sql_integer" >
				 </cfquery>
			</cfif>

			<cfset result = application.com.structurefunctions.convertFlatStructureToStructureOfStructures(application.com.structurefunctions.queryrowToStruct(query=getEntityIDs,row=1),"_")>
			<cfset result.entityType = entityTypeStruct.tablename>
			<cfset result.entityTypeID = EntityTypeID>
			<cfset result.entityID = EntityID>

			<cfif structkeyexists(result,"name")>
				<cfset result.entityName = result.name[result.EntityType]>
			</cfif>
			<cfset result.countryID =0>
			<cfif structKeyExists(result,"countryIDs")>
				<cfset result.countryid = result.countryids[result.EntityType]>
			</cfif>


			<cfif getEntityQueries>
				<cfset result.queries = structNew()>

				<!--- NYF 2008-07-30 Bug 583: added -> --->
				<cfif EntityTypeID lte 2>
					<cfloop index= tempEntityTypeID from="2" to = "#entityTypeID#" step = -1>

						<cfset tempentityTypeStruct = application.entityType[tempentityTypeID]>

							<cfquery NAME="getRecord" datasource="#application.SiteDataSource#">
					 		SELECT
								#preserveSingleQuotes(tempentityTypeStruct.NameExpression)# as name,
								ugu.name as lastupdatedbyName,
								ugc.name as createdbyName,
								<cfif tempentityTypeID gt 0>countryDescription as countryName,</cfif>
								<CFIF tempentityTypeID is 0>#result.countryids.person# as countryid,</cfif>
								<CFIF tempentityTypeID lte 2>(select count(1) from location where e.Organisationid = location.Organisationid) as NumberLocationInOrganisation,</cfif>
								<CFIF tempentityTypeID lte 2>(select count(1) from person where e.Organisationid = person.Organisationid) as NumberPersonInOrganisation,</cfif>
								<CFIF tempentityTypeID lte 1>(select count(1) from person where e.locationid = person.Locationid) as NumberPersonInLocation,</cfif>
								<cfif listFind("2,0",tempEntityTypeID)>'#application.com.login.getMagicNumber(entityID=result.id[tempentitytypeid],entityTypeID=tempEntityTypeID)#' as magicNumber,</cfif>
						 	e.*
							from #tempentityTypeStruct.TableName# as e
							<cfif tempentityTypeID gt 0>
								inner join country c with (noLock) on e.countryID = c.countryID
							</cfif>
							left outer join usergroup ugc on e.createdby = ugc.usergroupid
							left outer join usergroup ugu on e.lastupdatedby = ugu.usergroupid
							where e.#tempentityTypeStruct.UniqueKey# = #result.id[tempentitytypeid]#
							</CFQUERY>

						<cfset result.queries[tempentityTypeStruct.tablename]= getRecord>

					</cfloop>
				<cfelse>
					<cftry>
						<cfquery NAME="getRecord" datasource="#application.SiteDataSource#">
							SELECT
							<cfif entityTypeStruct.tablename eq "Country">CountryDescription as name, </cfif>
							ugu.name as lastupdatedbyName, ugc.name as createdbyName,
							e.*
							from #result.entityType# as e left outer join usergroup ugc on e.createdby = ugc.usergroupid left outer join usergroup ugu on e.lastupdatedby = ugu.usergroupid
							where e.#entityPK#  =  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="cf_sql_integer" >
						 </cfquery>
						 <cfcatch type="any">
							<cfquery NAME="getRecord" datasource="#application.SiteDataSource#">
								SELECT e.* from #result.entityType# as e
								where e.#entityPK#  =  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="cf_sql_integer" >
							</cfquery>
						 </cfcatch>
					</cftry>

	 				<cfset result.queries[result.entityType]= getRecord>
				</cfif>
			</cfif>

		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- WAB 2011-09-27 TND109
		This function creates a mainly blank query with a few items defaulted to whatever is passed in in the defaults structure
		Because sometimes the defaults aren't of quite the right data type (for example personid = newPerson) I have to do something a bit nasty!
	--->
	<cffunction name="createDefaultRecord" output="false">
		<cfargument name="entityType" required="yes">
		<cfargument name="defaults" default="#structNew()#">

		<cfset var blankRecord = "">
		<cfset var field = "">

		<cfquery name="blankRecord" datasource = "#application.sitedatasource#">
			select <cfloop collection="#defaults#" item="field">'' as #field#,</cfloop>*  from #entityType# where 1=0
		</cfquery>
		<cfset queryaddrow(blankRecord)>
		<cfloop collection="#defaults#" item="field">
			<cfset querysetcell(blankRecord,field,defaults[field])>
		</cfloop>

		<cfreturn blankRecord>

	</cffunction>



	<!--- This is a function to delete entities.
		BEWARE!!!! It does not currently do any checking and should be used with extremem caution.
		I'm wanting it to delete oppProducts. So, if it's pol data that may need to be deleted, we will probably need to create/call specific
		functions to do that. --->
	<cffunction name="deleteEntity" access="public" hint="Deletes an entity" output="false" returntype="struct">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="hardDelete" type="boolean" default="false">

		<cfset var deleteEntityQry = "">
		<cfset var entityType = getEntityType(entityTypeID = arguments.entityTypeID)>
		<cfset var result = {errorCode="",isOK=true,reason="",entityDeleted=true}>
		<cfset var qSetDeleted = "">
		<cfset var entityExists = getEntity(entityID=arguments.entityID,entityTypeID=entityType.entityTypeID,testUserEntityRights=true)>
		<cfset var tempResult = structNew()>

		<cftry>
			<cfset tempResult = isEntityAvailableForGetUpdateOrDelete(entityID=arguments.entityID,entityTypeID=entityType.entityTypeID,testUserEntityRights=true)>
			<cfset structAppend(result,tempResult,true)>
			<cfif not result.isOk>
				<cfset result.entityDeleted = false>
				<cfreturn result>
			</cfif>

			<!--- POL data is deleted by marking them as deleted --->
			<cfif listFindNoCase("person,organisation,location",entityType.tablename)>
				<cfset structAppend(result,deleteEntityOverride(entityID=arguments.entityID,entityTypeID=entityType.entityTypeID))>

			<!--- if the table has a deleted column and we're not doing a hard delete, then perform a soft delete --->
			<cfelseif not arguments.hardDelete and getTableFieldStructure(tablename=entityType.tablename,fieldname="deleted").isOK>
				<cfquery name="qSetDeleted" datasource="#application.siteDataSource#">
					update #entityType.tablename#
					set deleted=1,
						lastUpdated = getDate(),
						lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson=#request.relayCurrentUser.personID#
					where #entityType.uniqueKey# = #arguments.entityID#
				</cfquery>

			<!--- we're doing a hard delete --->
			<cfelse>
				<cfquery name="deleteEntityQry" datasource="#application.siteDataSource#">
					update #entityType.tablename#
						set lastUpdated = getDate(), lastUpdatedBy = #request.relayCurrentUser.userGroupID#,lastUpdatedByPerson=#request.relayCurrentUser.personID#
					where
						#entityType.uniqueKey# = #arguments.entityID#

					delete from #entityType.tablename# where #entityType.uniqueKey# = #arguments.entityID#
				</cfquery>
			</cfif>

			<cfcatch>
				<cfset result.reason = cfcatch.message>
				<cfset result.errorCode = "DELETE_ERROR">
				<cfset result.entityDeleted = false>
				<cfset result.isOk = false>
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2012/03/23 - function to get an entity record --->
	<cffunction name="getEntity" access="public" hint="Returns entity records" returnType="struct" output="false">
		<cfargument name="entityID" type="numeric" required="false">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="fieldList" type="string" default="">
		<cfargument name="useFriendlyName" type="boolean" default="false">
		<cfargument name="whereClause" type="string" default=""> <!--- NOTE: this currently assumes that the where clause has been sanitised!! --->
		<cfargument name="cacheResultsIfOverThreshold" type="boolean" default="false">
		<cfargument name="pageSize" type="numeric" default="300">
		<cfargument name="orderBy" type="string" default="">
		<cfargument name="testUserEntityRights" type="boolean" default="false" hint="Used if an entityID is passed so that we can pass an insufficient rights message.">
		<cfargument name="testLoggedInUserRights" type="boolean" default="true" hint="By default only the records the logged in user has rights to see are returned, if false is set in this parameter all relevant records are returned">
		<cfargument name="returnApiName" type="boolean" default="false">

		<cfset var entityType = getEntityType(entityTypeID = arguments.entityTypeID)>
		<cfset var result = {isOK=true,recordCount=0,recordSet="",errorCode=""}>
		<cfset var entityDetailsStruct = structNew()>
		<cfset var preparedEntityStruct = structNew()>
		<cfset var tableDetailsStruct = structNew()>
		<cfset var entityDetails = "">
		<cfset var column = "">
		<cfset var listCount = 0>
		<cfset var fieldStruct = structNew()>
		<cfset var entityColumnList = "">
		<cfset var tableAlias = left(entityType.tablename,1)>
		<cfset var orderByClause = "#tableAlias#.#entityType.uniqueKey#">
		<cfset var getRecords_sql = "">
		<cfset var flagGroupArray = "">
		<cfset var tableFieldStruct = structNew()>
		<cfset var validWhereClause = arguments.whereClause>
		<cfset var tempString = arguments.whereClause>
		<cfset var replaceWith = ""/>
		<cfset var getEntityDetails = ""/>
		<cfif testLoggedInUserRights>
			<cfset var rightsFilter = application.com.rights.getRightsFilterQuerySnippet(entityType=entityType.tablename,alias=tableAlias,useJoins=true)>
		</cfif>
		<cfset var prepareArgStruct = structNew()>
		<cfset var deletedFlagWhere="">
		<cfset var tempResult = structNew()>
		<cfset var prefix = "">
		<cfset var suffix = "">
		<cfset var utcTimeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset()>
		<cfset var whereColumnList = "">
		<cfset var parseWhereClauseArray = arrayNew(1)>
		<cfset var whereColumnElement = structNew()>

		<cfif arguments.orderBy neq "">
			<cfset orderByClause = arguments.orderBy>
		</cfif>

		<cfif structKeyExists(arguments,"entityID")>
			<cfset tempResult = isEntityAvailableForGetUpdateOrDelete(entityID=arguments.entityID,entityTypeID=arguments.entityTypeID,testUserEntityRights=arguments.testUserEntityRights)>
			<cfset structAppend(result,tempResult,true)>
			<cfif not result.isOk>
				<cfreturn result>
			</cfif>
		</cfif>

		<!--- getting the where clause ready for finding and replacing friendly names with valid where clause statements; at the moment,
			we are only supporting ands, ins and ors although we aren't stopping anything else coming through at the moment --->
		<cfif arguments.whereClause neq "">

			<!--- don't allow a sub-select at the moment... a bit too complicated with aliasing column names, etc.--->
			<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser and findNoCase("select ",arguments.whereClause) or findNoCase("select*",arguments.whereClause)>
				<cfset result.isOk = false>
				<cfset result.errorCode = "INVALID_SQL">
				<cfreturn result>
			</cfif>

			<!--- remove the statement after the in... we know that it's not a subselect, so it's not a column name --->
			<!---
				NJH 2012/11/04 Case 431491 - commented out the code below as I am now making use of Will's function that parses a where clause.. neater than this

			<cfset tempString = reReplaceNoCase(tempString," in(.*?)\((.*?)\)"," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,")"," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,"("," ","ALL")>

			<cfset tempString = replaceNoCase(tempString," and "," ","ALL")>
			<cfset tempString = replaceNoCase(tempString," or "," ","ALL")>

			<cfloop list="#operatorList#" index="operator">
				<cfset tempString = replaceNoCase(tempString,operator," ","ALL")>
			</cfloop>
			<!--- <cfset tempString = replaceNoCase(tempString," in "," ","ALL")>
			<cfset tempString = replaceNoCase(tempString," between "," ","ALL")>
			<cfset tempString = replaceNoCase(tempString," like "," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,"select "," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,"!="," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,"="," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,"<>"," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,">"," ","ALL")>
			<cfset tempString = replaceNoCase(tempString,"<"," ","ALL")> --->
			<cfset tempString = reReplaceNoCase(tempString,"'(.*?)'"," ","ALL")> <!--- remove anything in single quotes as they won't be variable names --->

			<!--- <cfset whereClauseColumnList = application.com.regexp.convertNameValuePairStringToStructure_(inputString=tempString,appendDuplicateKeys=true,equalsSign="in").orderedNameList> --->
			<!--- get rid of any duplicates in the list. Put into struct and then loop over structKeys. --->
			<cfset whereClauseColumnList = replace(tempString," ","|","ALL")>
			<cfloop list="#whereClauseColumnList#" index="column" delimiters="|">
				<cfif not structKeyExists(whereClauseStruct,column) and column neq "" and not isNumeric(column)>
					<cfset whereClauseStruct[trim(column)] = "">
				</cfif>
			</cfloop>

			<cfloop list="#structKeyList(whereClauseStruct,"|")#" index="column" delimiters="|"> --->
			<cfset parseWhereClauseArray = application.com.regExp.parseWhereClause(whereClause=arguments.whereClause)>
			<cfloop array="#parseWhereClauseArray#" index="whereClauseElement">

				<cfset tableFieldStruct = getTableFieldStructure(tablename=entityType.tablename,fieldname=whereClauseElement.column)>
				<cfset prefix = "">
				<cfset suffix = "">

				<cfif tableFieldStruct.isOK and structKeyExists(tableFieldStruct,"fieldType")>

					<!---NJH 2012/11/04 Case 431491 - convert date field to UTC --->
					<!--- RJT 2016/1/07 BF-201 Note mustn't try to convert a date, only datetimes as dates don't have a time so can't really be converted--->
					<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser and (tableFieldStruct.fieldType eq "field" and listFindNoCase("datetime,timestamp",tableFieldStruct.apiDataType)) or (listFindNoCase("flag,flagGroup",tableFieldStruct.fieldType) and tableFieldStruct.flagType.name eq "date")>
						<cfset prefix = "dateadd(s,#utcTimeOffsetInSeconds#,">
						<cfset suffix = ")">

						<cfif not application.com.dateFunctions.extractDateTimeFromUTCString(dateInUTCFormat=whereClauseElement.expression).isOK>
							<cfset result.field = whereClauseElement.column>
							<cfset result.isOK = false>
							<cfset result.fieldValue = whereClauseElement.expression>
							<cfset result.errorCode = "INVALID_DATE_FORMAT">
							<cfreturn result>
						</cfif>
					</cfif>

					<!--- when the api user, the column names for the where clause should be the api name --->
					<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser and whereClauseElement.column neq tableFieldStruct.apiName>
						<cfset result.isOk = false>
						<cfset result.errorCode = "INVALID_FIELD">
						<cfset result.field = whereClauseElement.column>
						<cfreturn result>
					</cfif>

					<cfif tableFieldStruct.fieldType eq "field">
						<cfset replaceWith = tableAlias&"."&tableFieldStruct.name>
					<cfelseif tableFieldStruct.fieldType eq "flag">
						<cfset replaceWith = application.com.flag.getJoinInfoForFlag(flagID=whereClauseElement.column,returnLinkedEntity=false).selectField>
					<cfelseif tableFieldStruct.fieldType eq "flagGroup">
						<cfset flagGroupArray = application.com.flag.getJoinInfoForFlagGroup(flagGroupID=whereClauseElement.column,showValueForFlag="flagTextID")>
						<cfset replaceWith = flagGroupArray[1].selectField>
					</cfif>

					<cfset whereColumnList = listAppend(whereColumnList,whereClauseElement.column)><!--- getting a list of the columns used in the where clause --->
					<cfset replaceWith = prefix&replaceWith&suffix>
					<!--- have to add the operator in the comparison, as sometimes a field value gets replaced when it's the same as a column name..
						not sure how to replace anything that is not in single quotes --->
					<cfset validWhereClause=rereplaceNoCase(validWhereClause,"(\s|\A|\()(#whereClauseElement.column#)([\s!=<]|\Z)","\1#replaceWith#\3","all")>
					<!--- <cfloop list="#operatorDelims#" index="operatorDelim">
						<cfset validWhereClause=rereplaceNoCase(validWhereClause,"#column#\#operatorDelim#","#replaceWith##operatorDelim#","all")>
					</cfloop> --->
				<!--- <cfelse>
					<cfset result.isOk = false>
					<cfset result.errorCode = "INVALID_COLUMN">
					<cfset result.field = column>
					<cfreturn result>--->
				</cfif>
			</cfloop>
		</cfif>

		<!--- initialise the entityDetailsStruct which is needed for prepareEntityStruct. The column can be used in the incoming struct or the api name.. Initialise with the fieldname --->
		<cfloop list="#arguments.fieldList#" index="column">
			<cfset entityDetailsStruct[column] = "">
		</cfloop>

		<cfset prepareArgStruct.method="select">
		<cfset prepareArgStruct.testLoggedInUserRights=arguments.testLoggedInUserRights>
		<cfset prepareArgStruct.entityDetails=entityDetailsStruct>
		<cfset prepareArgStruct.entityTypeID=arguments.entityTypeID>
		<cfset prepareArgStruct.columnsToUse=arguments.fieldList>

		<!--- check if we have rights on this entityID.. used primarily for the API, so that we can return an  "Insufficient Rights" message rather than just an empty record..
			to be consistent with update/insert
		--->
		<!--- <cfif arguments.testUserEntityRights and structKeyExists(arguments,"entityID")>
			<cfset prepareArgStruct.testUserEntityRights = true>
			<cfset prepareArgStruct.entityID = arguments.entityID>
		</cfif> --->

		<cfset preparedEntityStruct = prepareEntityStruct(argumentCollection=prepareArgStruct)>
		<cfset structAppend(result,preparedEntityStruct,"true")>

		<cfif result.isOk>

			<cfset tableDetailsStruct = preparedEntityStruct.tableDetailsStruct>
			<cfset entityColumnList = structKeyList(preparedEntityStruct.entityDetailsStruct)>

			<cfprocessingdirective suppresswhitespace="yes">
				<cfsaveContent variable="getRecords_sql">
					<cfoutput>
						select
						<cfloop list="#entityColumnList#" index="column">
							<cfset listCount++>
							<cfset tableFieldStruct = getTableFieldStructure(tablename=entityType.tablename,fieldname=column)>
							<cfset prefix = "">
							<cfset suffix = "">

							<!--- NJH 2012/11/04 Case 431491 - convert date field to UTC if the api user--->
							<!--- RJT 2016/1/07 BF-201 Note mustn't try to convert a date, only datetimes as dates don't have a time so can't really be converted--->
							<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser and (tableFieldStruct.fieldType eq "field" and listFindNoCase("datetime,timestamp",tableFieldStruct.apiDataType)) or (listFindNoCase("flag,flagGroup",tableFieldStruct.fieldType) and tableFieldStruct.flagType.name eq "date")>
								<cfset prefix = "convert(varchar(19),dateadd(s,#utcTimeOffsetInSeconds#,"> <!--- use varchar(19) so that we do not return milliseconds --->
								<cfset suffix = "),127)+'Z'">
                            <cfelseif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser and tableFieldStruct.fieldType eq "field" and tableFieldStruct.apiDataType eq "date">
                            <!--- Case 449078 - as done for BF-201 dont try to add utcTimeOffsetInSeconds for just "date" types but still convert to varchar so it's not get auto-formatted later --->
                                <cfset prefix = "convert(varchar(19),">
                                <cfset suffix = ",127)">
							</cfif>

							<cfif tableFieldStruct.fieldType eq "field">
								#prefix##tableAlias#.#column##suffix#
							<cfelseif tableFieldStruct.fieldType eq "flag">
								#prefix##application.com.flag.getJoinInfoForFlag(flagID=column,returnLinkedEntity=false).selectField##suffix#
							<cfelseif tableFieldStruct.fieldType eq "flagGroup">
								<cfset flagGroupArray = application.com.flag.getJoinInfoForFlagGroup(flagGroupID=column,showValueForFlag="flagTextID")>
								#prefix##flagGroupArray[1].selectField##suffix#
							</cfif>

							<cfif arguments.useFriendlyName and tableFieldStruct.apiName neq "">as [#tableFieldStruct.apiName#]</cfif>
							<cfif listLen(entityColumnList) gt listCount>,</cfif>
						</cfloop>
						from #entityType.tablename# #tableAlias# with (noLock)
						<!--- if we're getting a person, then we need to join to the location to get only people in countries that we've got rights to --->
						<cfif testLoggedInUserRights>#RightsFilter.join#</cfif>
						<!--- need to append any columns used in the where clause, as we may need to join to extra tables, such as flag tables --->
						<cfset entityColumnList = listAppend(entityColumnList,whereColumnList)>
						<!---RJT - case 445456 - ListRemoveDuplicates to avoid joining the same profile multiple times when the same flag is required in multiple contexts (e.g. in fieldlist and filter from API)--->
						<cfloop list="#ListRemoveDuplicates(entityColumnList,',',true)#" index="column">
							<cfset tableFieldStruct = getTableFieldStructure(tablename=entityType.tablename,fieldname=column)>
							<cfif tableFieldStruct.fieldType eq "flag">
								#application.com.flag.getJoinInfoForFlag(flagID=column,returnLinkedEntity=false).join#
							<cfelseif tableFieldStruct.fieldType eq "flagGroup">
								<cfset flagGroupArray = application.com.flag.getJoinInfoForFlagGroup(flagGroupID=column)>
								#flagGroupArray[1].join#
							</cfif>
						</cfloop>
						<cfif getTableFieldStructure(tablename=entityType.tablename,fieldname="deleted").isOK>
							<cfset deletedFlagWhere="and #tableAlias#.deleted != 1">
						<cfelseif listFindNoCase("person,organisation,location",entityType.tablename)>
							<!--- GCC 2015-01-29 Added No Lock --->
							left join booleanflagdata bfd with (noLock) on (#tableAlias#.#entityType.tablename#ID = bfd.entityID) and (bfd.flagID  = #application.com.flag.getFlagStructure(flagID="Delete#entityType.tablename#").flagID#)
							<cfset deletedFlagWhere="and bfd.entityID is null">
						</cfif>
						where 1=1
							<cfif testLoggedInUserRights>#rightsFilter.whereClause#</cfif>
							<cfif structKeyExists(arguments,"entityID")>and #tableAlias#.#entityType.uniqueKey# = <cf_queryparam value="#arguments.entityID#" cfSqlType="CF_SQL_INTEGER"></cfif>
							<cfif validWhereClause neq "">and (#validWhereClause#)</cfif>
							#deletedFlagWhere#
						order by <cf_queryObjectName value="#orderByClause#">
					</cfoutput>
				</cfsaveContent>
			</cfprocessingdirective>

			<cftry>
				<cfif not arguments.cacheResultsIfOverThreshold>
					<cfquery name="getEntityDetails" datasource="#application.siteDataSource#" result="entityDetails">
						#preserveSingleQuotes(getRecords_sql)#
					</cfquery>
					<cfset result.recordCount = entityDetails.recordCount>
					<cfset result.recordSet = getEntityDetails>
				<cfelse>
					<cfset result = application.com.dbTools.dbCacheQuery(queryString=getRecords_sql,pageSize=arguments.pageSize)>
					<cfif not result.isOK>
						<cfset result.recordCount = 0>
						<cfset result.errorCode = "QUERY_ERROR">
					</cfif>
				</cfif>
				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.recordCount = 0>
					<cfset result.errorCode = "QUERY_ERROR">
					<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- a function to return a structure with the core field names as keys, as it could be keyed on API name as well --->
	<cffunction name="transposeEntityStructToCoreStruct" access="private" returntype="struct" output="false">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="entityDetails" type="struct" required="true">

		<cfset var result = structNew()>
		<cfset var entityType = getEntityType(entityTypeID = arguments.entityTypeID)>
		<cfset var fieldStruct = {}>
		<cfset var column = "">
		<cfset var translationColumn = "">

		<cfloop collection="#arguments.entityDetails#" item="column">

			<cfset fieldStruct = getTableFieldStructure(tablename=entityType.tablename,fieldname=column)>

			<cfif fieldStruct.isOK and fieldStruct.fieldType neq "field">
				<cfset result[fieldStruct.apiName] = entityDetails[column]>
			<cfelseif fieldStruct.isOK>
				<cfset result[fieldStruct.name] = entityDetails[column]>
			<cfelse>

				<!--- NJH 2013/06/04 - if we are setting a translated field (such a product title), then change the column to _defaultTranlsation --->
				<cfset translationColumn = column&"_defaultTranslation">
				<cfif getTableFieldStructure(tablename=entityType.tablename,fieldname=translationColumn).isOK>
					<cfset result[translationColumn] = entityDetails[column]>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- function to return field/flag information for a given table.. --->
	<cffunction name="getTableFieldStructure" access="public" returnType="struct" output="false">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="fieldname" type="string" required="true"> <!--- can be a list of fieldnames --->
		<cfargument name="returnWrapperStruct" type="boolean" default="false"> <!--- puts an entity wrapper struct around the returned structure.. so it becomes location.sitename.maxLength rather than sitename.maxLength.. this is more for backwards compatibility --->

		<cfset var result = {isOK = false}>
		<cfset var entityStruct = {isOK=true,table=structNew(),flag=structNew(),flagGroup=structNew()}>
		<cfset var fieldStruct = {isOK=false}>
		<cfset var field = "">

		<cfset arguments.tablename = trim(arguments.tablename)>
		<cfset arguments.fieldname = trim(arguments.fieldname)>

		<cftry>
			<cfif listLen(arguments.fieldName) eq 1>

				<cfset fieldStruct = application.com.applicationVariables.getFieldInfoStructure(tablename=tablename,fieldname=fieldname)>

				<!--- WAB 2014-09-03 changed order to look for flags before flaggroups.  Often flagGroups are given the same name as the flag in them, but it is the flag that we are interested in. --->
				<cfif fieldStruct.isOK>
					<cfset entityStruct.table[fieldStruct.name] = fieldStruct>
				<cfelseif application.com.flag.doesFlagExist(flagID=arguments.fieldname,entityName=tablename)>				<!--- 2014-07-10 PPB Case 440579 added EntityName param --->
					<cfset fieldStruct = application.com.flag.getFlagStructure(flagID=arguments.fieldname)>
					<cfset entityStruct.flagGroup[fieldStruct.apiName] = fieldStruct>
				<cfelseif application.com.flag.doesFlagGroupExist(flagGroupID=arguments.fieldname,entityName=tablename)>	<!--- 2014-07-10 PPB Case 440579 added EntityName param --->
					<cfset fieldStruct = application.com.flag.getFlagGroupStructure(flagGroupID=arguments.fieldname)>
					<cfset entityStruct.flagGroup[fieldStruct.apiName] = fieldStruct>
				<cfelse>
					<cfset entityStruct.isOK=false>
				</cfif>

				<!--- this will return the result within an entity structure, where the entityname is the key --->
				<cfif arguments.returnWrapperStruct>
					<cfreturn entityStruct>
				<cfelse>
					<cfreturn fieldStruct>
				</cfif>

			<cfelse>

				<cfloop list="#arguments.fieldname#" index="field">
					<cfif field neq "">
						<cfset fieldStruct = getTableFieldStructure(tablename=arguments.tablename,fieldname=field)>
						<cfif fieldStruct.isOK>
							<!--- if it's a core field, then return the actual fieldname as the key; otherwise, return the flagTextId (which is the apiName) --->
							<cfif fieldStruct.fieldType eq "field">
								<cfset entityStruct.table[fieldStruct.name] = fieldStruct>
							<cfelseif fieldStruct.fieldType eq "flag">
								<cfset entityStruct.flag[field] = fieldStruct>
							<cfelse>
								<cfset entityStruct.flagGroup[field] = fieldStruct>
							</cfif>
						<cfelse>
							<!--- <cfset entityStruct = false> --->
						</cfif>
					</cfif>
				</cfloop>

				<cfreturn entityStruct>
			</cfif>

			<cfcatch>
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				<cfreturn result>
			</cfcatch>
		</cftry>

	</cffunction>


	<!--- function to return some basic information about a given entityType...
		WAB 2012-07-03 modified to bring back whole of entityType structure, not suer why only getting bits of it
	--->
	<cffunction name="getEntityType" access="public" returnType="struct" output="false">
		<cfargument name="entityTypeID" type="string" required="true">

		<cfset var result = {entityTypeID=0,tablename="",uniqueKey=""}>

		<cfif isNumeric(arguments.entityTypeID)>
			<cfset result  = application.entityType[arguments.entityTypeID]>
		<cfelse>
			<cfset result  = application.entityType[application.entityTypeID[arguments.entityTypeID]]>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- This function is used to set profile data for a given entity.. the entity details struct will contain the profiles and their values.
		The keys will be flagTextIds or flagGroupTextIDs... the list of flags or flaggroups to update is passed through... this saves looping
		through the structure and checking each element.. maybe there is a better way to do this..

		WAB 2015-01-27 CASE 443542 Added support for deleting boolean flags by passing in value 0
		I would like to reverse PPB Case 440579 - which is preventing un-setting all the flags in a flagGroup by passing in a blank value

	 --->
	<cffunction name="setProfileData" access="private" returnType="struct" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="flagList" type="string" default="">
		<cfargument name="flagGroupList" type="string" default="">

		<cfset var result = {isOk=true}>
		<cfset var flagTextID = "">
		<cfset var flagGroupTextID = "">
		<cfset var getFlagGroupFlags = "">
		<cfset var flagArgStruct = structNew()>
		<cfset var flagStructure = "">

		<cfloop list="#arguments.flagList#" index="flagTextID">

			<!--- if it's an integer flag, then the flagTextId will be a key.. else, if it's a boolean flag, then the flagTextID will be a value --->
			<cfif structKeyExists(arguments.entityDetails,flagTextID)>
				<cfset flagArgStruct = structNew()>
				<cfset flagArgStruct.flagID = flagTextID>
				<cfset flagArgStruct.entityId = arguments.entityId>
				<cfset flagStructure = application.com.flag.getFlagStructure(flagId=flagTextID)>

				<!--- WAB 2015-01-27 CASE 443542 Added support for deleting boolean flags by passing in value 0 --->
				<cfif NOT (arguments.entityDetails[flagTextID] eq "" OR (flagStructure.flagType.dataTable is "Boolean" and arguments.entityDetails[flagTextID] eq "0"))>
					<cfif structKeyExists(arguments.entityDetails,flagTextID)>
						<cfset flagArgStruct.data = arguments.entityDetails[flagTextID]>
					</cfif>
					<cfset application.com.flag.setFlagData(argumentCollection=flagArgStruct)>
				<cfelse>
					<cfset application.com.flag.deleteFlagData(argumentCollection=flagArgStruct)>
				</cfif>
			</cfif>
		</cfloop>

		<cfloop list="#arguments.flagGroupList#" index="flagGroupTextID">
			<cfif structKeyExists(arguments.entityDetails,flagGroupTextID)>

				<cfif application.com.flag.getFlagGroupStructure(flagGroupId=flagGroupTextID).flagType.dataTable eq "boolean">

					 <cfif trim(arguments.entityDetails[flagGroupTextID]) neq "">	<!--- 2014-07-10 PPB Case 440579 defend against this being empty --->
						<cfquery name="getFlagGroupFlags" datasource="#application.siteDataSource#">
							select flagID from flag f with (noLock) inner join flagGroup fg with (noLock) on f.flagGroupId = fg.flagGroupID
								where fg.flagGroupTextId = <cf_queryparam value="#flagGroupTextID#" cfsqltype="CF_SQL_VARCHAR">
								and flagTextID in (<cf_queryparam value="#arguments.entityDetails[flagGroupTextID]#" cfsqltype="CF_SQL_VARCHAR" list="true">)
						</cfquery>
						<cfset application.com.flag.setFlagGroupData(flagGroupID=flagGroupTextID,entityID=arguments.entityId,flagIDList=valueList(getFlagGroupFlags.flagID))>
					 </cfif>
				<cfelse>
					<cfquery name="getFlagGroupFlags" datasource="#application.siteDataSource#">
						select flagTextID from flag f with (noLock) inner join flagGroup fg with (noLock) on f.flagGroupId = fg.flagGroupID
							where fg.flagGroupTextId = <cf_queryparam value="#flagGroupTextID#" cfsqltype="CF_SQL_VARCHAR">
					</cfquery>

					<cfset setProfileData(entityId=arguments.entityID,entityDetails=arguments.entityDetails,flagList=valueList(getFlagGroupFlags.flagTextID),flagGroupId=flagGroupTextID)>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="checkEntityRights" access="private" returnType="struct" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="entityDetails" type="struct" required="true">

		<cfset var rightsStruct = {edit=true,view=true}>
		<cfset var argStruct = duplicate(arguments)>
		<cfset var getCountryID = "">
		<cfset var entityTypeStruct = getEntityType(entityTypeID=arguments.entityTypeId)>
		<cfset var runRights = true>
		<cfset var recordID = arguments.entityID>
		<cfset var uniqueKey = entityTypeStruct.uniqueKey>

		<cfset argStruct.entityType = entityTypeStruct.tablename>

		<!--- if updating pol data, check the country rights --->
		<cfif listFindNoCase("organisation,location,person",entityTypeStruct.tablename)>
			<cfif arguments.entityID eq 0 and entityTypeStruct.tablename eq "person" and structKeyExists(arguments.entityDetails,"locationID")>
				<cfset recordId = arguments.entityDetails.locationID>
				<cfset uniqueKey = "locationID">
			</cfif>

			<cfquery name="getCountryID" datasource="#application.siteDataSource#">
				select countryId from <cfif listFindNoCase("location,person",entityTypeStruct.tablename)>location<cfelse>organisation</cfif>
					<cfif entityTypeStruct.tablename eq "person" and arguments.entityID neq 0>inner join person on person.locationID = location.locationID</cfif>
				where #uniqueKey# = <cf_queryparam value="#recordId#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>

			<cfif getCountryID.recordCount>
				<cfset argStruct.countryId = getCountryID.countryID[1]>
			<cfelse>
				<!--- the entity doesn't exist, so don't bother running a rights check on it.. --->
				<cfset runRights = false>
			</cfif>
		</cfif>

		<cfif runRights>
			<cfset rightsStruct = application.com.rights.getUserRightsForEntity(argumentCollection=argStruct)>

			<!--- case for pol data - check that the user has rights of the country that they are moving the entity to --->
			<cfif rightsStruct.edit and listFindNoCase("organisation,location,person",entityTypeStruct.tablename) and structKeyExists(arguments.entityDetails,"countryID") and arguments.entityDetails.countryID neq "">
				<cfset argStruct.countryID = arguments.entityDetails.countryID>
				<cfset rightsStruct = application.com.rights.getUserRightsForEntity(argumentCollection=argStruct)>
			</cfif>
		</cfif>

		<cfreturn rightsStruct>
	</cffunction>


	<cffunction name="upsertEntities" access="public" returnType="struct" hint="Function to bulk insert or update entities">
		<cfargument name="entitiesArray" type="array" required="true" hint="This is an array of entity structures">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="method" type="string" default="update">
		<cfargument name="returnApiName" type="boolean" default="false"> <!--- used in error reporting.. give the apiName as the column names, etc. --->
		<cfargument name="processDespiteErrors" type="boolean" default="true"> <!--- process all the ones that we can. Ones with errors are ignored --->

		<cfset var columnList = "">
		<cfset var columnName = "">
		<cfset var entityTypeStruct = getEntityType(entityTypeID=arguments.entityTypeId)>
		<cfset var result = {isOK=true,errorCode=""}>
		<cfset var tmpTable = "####tmpTable#replace(CreateUUID(),'-','','ALL')#">
		<cfset var uniqueKeyFound = false>
		<cfset var entityStruct = structNew()>
		<cfset var entityField = "">
		<cfset var createTempTable = "">
		<cfset var listCount = 1>
		<cfset var arrayCount = 1>
		<cfset var insertIntoTempTable = "">
		<cfset var updateEntityRecords = "">
		<cfset var insertEntityRecords = "">
		<cfset var columnListLen = 0>
		<cfset var coreColumnList = "">
		<cfset var tableFieldStruct = structNew()>
		<cfset var updateableColumns = application.com.rights.getEntityFieldsForPersonAndMethod(entityTypeID=arguments.entityTypeID,haveRightsForMethod="edit")>
		<cfset var getUpdatedRecords = "">
		<cfset var flagList = "">
		<cfset var flagGroupList = "">
		<cfset var fieldList = "">
		<cfset var setBooleanProfiles = "">
		<cfset var entityID = 0>
		<cfset var removeRecordsThatUserDoesNotHaveRightsTo = "">
		<cfset var setNonBooleanProfiles = "">
		<cfset var flagGroupStruct = structNew()>
		<cfset var flagStruct = structNew()>
		<cfset var structField = "updated">
		<cfset var RightsFilter = application.com.rights.getRightsFilterQuerySnippet(entityType=entityTypeStruct.tablename,useJoins=false,alias="e")>
		<cfset var name = "">
		<cfset var fieldMetaData = structNew()>
		<cfset var tableFieldMetaData = structNew()>
		<cfset var validValues = "">
		<cfset var uniqueKeyInTable = "">
		<cfset var resultField = structNew()>

		<cfset var countryIDToUseForValidation=application.com.relayEntity.establishCountryIDForEntity(EntityTypeID=entityTypeID, entityID=entityID, entityDetails=entityDetails)>


		<cftry>
			<!--- build up a list of distinct column names --->
			<cfloop array="#arguments.entitiesArray#" index="entityStruct">
				<cfset uniqueKeyFound = false>
				<cfloop collection="#entityStruct#" item="entityField">
					<!--- <cfif not listFindNoCase(columnList,entityField)>
						<cfset columnList = listAppend(columnList,entityField)>
					</cfif> --->
					<cfset tableFieldStruct = getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=entityField)>
					<cfif listFindNoCase("#tableFieldStruct.name#,#tableFieldStruct.apiName#",entityTypeStruct.uniqueKey)>
						<cfset uniqueKeyFound = true>
						<cfset uniqueKeyInTable = entityField>
					</cfif>
				</cfloop>

				<!--- if doing an update, every structure needs to have the primary key of the entity --->
				<cfif not uniqueKeyFound and arguments.method eq "update">
					<cfset entityStruct.error = "MISSING_UNIQUEID">
					<!--- <cfset result.errorCode = "MISSING_UNIQUEID">
					<cfset result.isOK = false> --->
				</cfif>

				<cfloop list="#structKeyList(entityStruct)#" index="column">
					<cfif column neq "error">
						<cfset tableFieldStruct = getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=column)>
						<cfset var fieldStruct = application.com.rights.getEntityFieldRights(entityTypeID=arguments.entityTypeId,returnApiName=true)>

						<!--- if it's not a valid entity field or profile, then throw invalid column error --->
						<cfif not listFindNoCase("#tableFieldStruct.name#,#tableFieldStruct.apiName#",column) or (request.relayCurrentUser.isApiUser and not listFindNoCase(structKeyList(fieldStruct),column))>
							<cfset entityStruct.error = "INVALID_COLUMN">
							<cfset entityStruct.errorField = column>
							<!--- <cfset result.errorCode = "INVALID_COLUMN">
							<cfset result.field = column>
							<cfset result.isOK = false> --->
						</cfif>


						<cfif tableFieldStruct.fieldType eq "field">
							<cfset name = tableFieldStruct.name>
						<cfelseif tableFieldStruct.fieldType eq "flag">
							<cfset name = application.com.flag.getFlagStructure(flagID=column).flagTextID>
						<cfelseif tableFieldStruct.fieldType eq "flagGroup">
							<cfset name = application.com.flag.getFlagGroupStructure(flagGroupID=column).flagGroupTextID>
						</cfif>

						<!--- check if we have rights to update the column. If it's the primary key and we're doing an update, then ignore this check as we need the primary key to actually
							perform the update. For an insert, it's different --->
						<cfif not listFindNoCase(updateableColumns,name) and ((name neq entityTypeStruct.uniqueKey and arguments.method eq "update") or arguments.method eq "insert")>
							<cfset result.errorCode = "INSUFFICIENT_RIGHTS">
							<cfset result.errorField = column>
							<!--- <cfset result.errorCode = "INSUFFICIENT_RIGHTS">
							<cfset result.field = name>
							<cfset result.isOK = false> --->
						<cfelse>
							<cfif tableFieldStruct.fieldType eq "field" and not listFindNoCase(fieldList,name)>
								<cfset fieldList = listAppend(fieldList,name)>
							<cfelseif tableFieldStruct.fieldType eq "flag" and not listFindNoCase(flagList,name)>
								<cfset flagList = listAppend(flagList,name)>
							<cfelseif tableFieldStruct.fieldType eq "flagGroup" and not listFindNoCase(flagGroupList,name)>
								<cfset flagGroupList = listAppend(flagGroupList,name)>
							</cfif>
							<cfif not listFindNoCase(coreColumnList,name)>
								<cfset coreColumnList = listAppend(coreColumnList,name)>
							</cfif>
						</cfif>

					</cfif>
				</cfloop>
			</cfloop>

			<!--- <cfif not result.isOK>
				<cfreturn result>
			</cfif> --->

			<!--- loop through list and find out if any are invalid --->
			<!--- <cfloop list="#columnList#" index="column">
				<cfset tableFieldStruct = getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=column)>
				<!--- if it's not a valid entity field or profile, then throw invalid column error --->
				<cfif not listFindNoCase("#tableFieldStruct.name#,#tableFieldStruct.apiName#",column)>

					<!--- <cfset result.errorCode = "INVALID_COLUMN">
					<cfset result.field = column>
					<cfset result.isOK = false> --->
				<cfelse>

					<cfif tableFieldStruct.fieldType eq "field">
						<cfset name = tableFieldStruct.name>
					<cfelseif tableFieldStruct.fieldType eq "flag">
						<cfset name = application.com.flag.getFlagStructure(flagID=column).flagTextID>
					<cfelseif tableFieldStruct.fieldType eq "flagGroup">
						<cfset name = application.com.flag.getFlagGroupStructure(flagGroupID=column).flagGroupTextID>
					</cfif>

					<!--- check if we have rights to update the column. If it's the primary key and we're doing an update, then ignore this check as we need the primary key to actually
						perform the update. For an insert, it's different --->
					<cfif not listFindNoCase(updateableColumns,name) and ((name neq entityTypeStruct.uniqueKey and arguments.method eq "update") or arguments.method eq "insert")>
						<cfset result.errorCode = "INSUFFICIENT_RIGHTS">
						<cfset result.field = name>
						<cfset result.isOK = false>
					<cfelse>
						<cfif tableFieldStruct.fieldType eq "field">
							<cfset fieldList = listAppend(fieldList,name)>
						<cfelseif tableFieldStruct.fieldType eq "flag">
							<cfset flagList = listAppend(flagList,name)>
						<cfelseif tableFieldStruct.fieldType eq "flagGroup">
							<cfset flagGroupList = listAppend(flagGroupList,name)>
						</cfif>
						<cfset coreColumnList = listAppend(coreColumnList,name)>
					</cfif>
				</cfif>
			</cfloop>

			<cfif not result.isOK>
				<cfreturn result>
			</cfif> --->

			<cfset columnListLen = listLen(coreColumnList)>

			<cfquery name="createTempTable" datasource="#application.siteDataSource#">
				if object_id('tempdb..#tmpTable#') is not null
					begin
					   drop table #tmpTable#
					end
				create table #tmpTable#
				(
				<cfset listCount = 1>
				<cfloop list="#coreColumnList#" index="columnName">
					#columnName# nvarchar(1000),
				</cfloop>
					entityID int,
					error varchar(500),
					errorField varchar(100)
				)
			</cfquery>

			<cfquery name="insertIntoTempTable" dataSource="#application.siteDataSource#">
				insert into #tmpTable# (#coreColumnList#,entityID,error,errorField)
				<cfloop array="#arguments.entitiesArray#" index="entityStruct">
					select
					<cfset listCount = 1>
					<cfset entityID = 0>
					<cfif arguments.method eq "insert" and listFindNoCase("organisation,location,person",entityTypeStruct.tableName)>
						<cfset entityId = application.com.relayDataload.getAndSetMaxIDForEntity(entityTypeStruct.tableName)>
					</cfif>
					<cfloop list="#coreColumnList#" index="columnName">
						<cfif listFindNoCase("#uniqueKeyInTable#,#entityTypeStruct.uniqueKey#",columnName) and arguments.method eq "update">
							<cfset entityID = entityStruct[columnName]>
						</cfif>
						<cfset var apiName = getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=columnName).apiName>
						<cfif structKeyExists(entityStruct,columnName)>'#entityStruct[columnName]#'<cfelseif structKeyExists(entityStruct,apiName)>'#entityStruct[apiName]#'<cfelse>null</cfif><cfif listCount neq columnListLen>,</cfif>
						<cfset listCount++>
					</cfloop>
					,#entityID#
					,<cfif structKeyExists(entityStruct,"error")>'#entityStruct.error#'<cfelse>null</cfif>
					,<cfif structKeyExists(entityStruct,"errorField")>'#entityStruct.errorField#'<cfelse>null</cfif>
					<cfif arrayCount lt arrayLen(arguments.entitiesArray)>union</cfif>
					<cfset arrayCount++>
				</cfloop>
			</cfquery>

			<!--- if all the columns are valid, then go through and do some data validation based on the meta data --->
			<cfset structAppend(result,validateData(tablename=tmpTable,entityTypeId=arguments.entityTypeId,returnApiName=arguments.returnApiName,method=arguments.method,processDespiteErrors=arguments.processDespiteErrors, countryIDPerspective=countryIDToUseForValidation),true)>
			<cfif result.errorCode neq "" and not arguments.processDespiteErrors>
				<cfreturn result>
			</cfif>

			<cfset structAppend(result,validateForeignKeys(tablename=tmpTable,returnApiName=arguments.returnApiName,processDespiteErrors=arguments.processDespiteErrors,entityTypeID=arguments.entityTypeID),true)>
			<cfif result.errorCode neq "" and not arguments.processDespiteErrors>
				<cfreturn result>
			</cfif>

			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.errorCode = "UPSERT_ERROR">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				<cfreturn result>
			</cfcatch>
		</cftry>

		<cf_transaction action="begin">
			<cftry>

				<!--- <cfquery name="removeErroredRecords" datasource="#application.siteDataSource#">
					delete from #tmpTable# where error is not null
				</cfquery> --->

				<cfif arguments.method eq "update">

					<!--- remove any records that a user doesn't have country rights to --->
					<cfif rightsfilter.whereClause neq "">
						<cfquery name="removeRecordsThatUserDoesNotHaveRightsTo" datasource="#application.siteDataSource#">
							update #tmpTable#
								set error = 'INSUFFICIENT_RIGHTS'
							from #tmpTable# t inner join #entityTypeStruct.tablename# e
									on t.entityId = e.#entityTypeStruct.uniqueKey#
							where NOT (1=1 #rightsfilter.whereClause#)
						</cfquery>
					</cfif>

					<cfquery name="updateEntityRecords" datasource="#application.siteDataSource#">
						update #entityTypeStruct.tablename#
							set
								<cfloop list="#fieldList#" index="columnName"><cfif columnName neq entityTypeStruct.uniqueKey>#columnName# = isNull(t.#columnName#,e.#columnName#),</cfif></cfloop>
								lastUpdated = getDate(),
								lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
								lastUpdatedByPerson = #request.relayCurrentUser.personID#
						from
							#entityTypeStruct.tablename# e inner join #tmpTable# t on e.#entityTypeStruct.uniqueKey# = t.#entityTypeStruct.uniqueKey#
								and (
									<cfset listCount=1>
									<cfloop list="#fieldList#" index="columnName">e.#columnName# <> isNull(t.#columnName#,e.#columnName#) <cfif listCount neq listLen(fieldList)>or </cfif><cfset listCount++></cfloop>
								)
						where t.error is null
					</cfquery>

				<cfelseif arguments.method eq "insert">

					<cfquery name="insertEntityRecords" datasource="#application.siteDataSource#">
						insert into #entityTypeStruct.tablename#
							(#fieldList#,created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson<cfif listFindNoCase("organisation,location,person",entityTypeStruct.tablename)>,#entityTypeStruct.uniqueKey#</cfif>)
						select #fieldList#,getDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,getDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,#request.relayCurrentUser.personId#<cfif listFindNoCase("organisation,location,person",entityTypeStruct.tablename)>,entityID</cfif>
						from #tmpTable#
						where error is null
					</cfquery>
				</cfif>

				<!--- set the flagGroups flags where are going to be boolean flags --->
				<cfset var flagGroupTextID = "">
				<cfloop list="#flagGroupList#" index="flagGroupTextID">
					<cfset flagGroupStruct = application.com.flag.getFlagGroupStructure(flagGroupId=flagGroupTextID)>

					<cfif flagGroupStruct.flagType.dataTable eq "boolean">
						<cfquery name="setBooleanProfiles" datasource="#application.siteDataSource#">
							exec setBooleanFlagGroup @tablename =  <cf_queryparam value="#tmpTable#" CFSQLTYPE="CF_SQL_VARCHAR" > ,@flagGroupId=#flagGroupStruct.flagGroupID#,@columnname =  <cf_queryparam value="#flagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,@userGroupID=#request.relayCurrentUser.userGroupID#,@personID=#request.relayCurrentUser.personID#
						</cfquery>
					</cfif>
				</cfloop>

				<!--- set the flags list which are going to be non-boolean flags --->
				<cfset var flagTextID = "">
				<cfloop list="#flagList#" index="flagTextID">
					<cfset flagStruct = application.com.flag.getFlagStructure(flagId=flagTextID)>

					<cfif listFindNoCase("text,integer,date",flagStruct.flagType.dataTable)>
						<cfquery name="setNonBooleanProfiles" datasource="#application.siteDataSource#">
							exec setDataFlag @tablename =  <cf_queryparam value="#tmpTable#" CFSQLTYPE="CF_SQL_VARCHAR" > ,@flagId=#flagStruct.flagID#,@columnname =  <cf_queryparam value="#flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,@userGroupID=#request.relayCurrentUser.userGroupID#,@personID=#request.relayCurrentUser.personID#
						</cfquery>
					</cfif>
				</cfloop>

				<cf_transaction action="commit">

				<cfcatch>
					<cf_transaction action="rollback">

					<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
					<cfset result.isOK = false>
					<cfset result.errorCode = "UPSERT_ERROR">
					<cfreturn result>
				</cfcatch>
			</cftry>
		</cf_transaction>

		<cfquery name="getUpdatedRecords" datasource="#application.siteDataSource#">
			select #entityTypeStruct.uniqueKey# as ID from #entityTypeStruct.tablename#
				where lastUpdatedBy=#request.relayCurrentUser.userGroupId#
					and lastUpdatedByPerson = #request.relayCurrentUser.personID#
					and dateDiff(s,lastUpdated,getDate()) < 5
		</cfquery>

		<cfif arguments.method eq "insert">
			<cfset structField = "inserted">
		</cfif>
		<cfset var updatedEntities = valueList(getUpdatedRecords.ID)>

		<cfset result.errorCode = "ENTITIES_UPDATED">
		<cfif arguments.method eq "insert">
			<cfset result.errorCode = "ENTITIES_INSERTED">
		</cfif>

		<cfset var getAllRecords = "">
		<cfquery name="getAllRecords" datasource="#application.siteDataSource#">
			select entityID,error,errorField from #tmpTable#
		</cfquery>

		<cfset result[structField] = arrayNew(1)>
		<cfloop query="getAllRecords">
			<cfset result[structField][currentRow] = structNew()>
			<cfif arguments.method eq "update">
				<cfset result[structField][currentRow][entityTypeStruct.uniqueKey] = getAllRecords.entityID[currentRow]>
			</cfif>

			<cfif getAllRecords.error[currentRow] eq "" and listFindNoCase(updatedEntities,getAllRecords.entityID[currentRow])>
				<cfset result[structField][currentRow].success = true>
				<!--- if we successfully inserted a record, then return the entityID --->
				<cfif arguments.method eq "insert">
					<cfset result[structField][currentRow][entityTypeStruct.uniqueKey] = getAllRecords.entityID[currentRow]>
				</cfif>
			<cfelse>
				<cfset result[structField][currentRow].success = false>
			</cfif>

			<!--- get the problem field for the error message if it is needed --->
			<cfset result[structField][currentRow].error = getAllRecords.error[currentRow]>
			<cfset resultField = structNew()>
			<cfset resultField.field = getAllRecords.errorField[currentRow]>
			<cfset result[structField][currentRow].message = application.com.relayAPI.getStatusCodeAndMessage(errorCode=getAllRecords.error[currentRow],result=resultField).message>
		</cfloop>

		<cfset var dropTempTable = "">
		<cfquery name="dropTempTable" datasource="#application.siteDataSource#">
			drop table tempdb..#tmpTable#
		</cfquery>

		<cfreturn result>
	</cffunction>


	<cffunction name="getEntityMetaData" access="public" returntype="struct" output="false">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="returnApiName" type="boolean" default="false">
		<cfargument name="countryIDPerspective" type="string" default="#request.relayCurrentUser.countryid#" hint="The country ID from whose perspective to validate data">


		<cfset var entityType = getEntityType(entityTypeID=arguments.entityTypeId).tablename>
		<cfset var fieldStruct = structNew()>
		<cfset var entityStruct = structNew()>
		<cfset var tableFieldStruct = structNew()>
		<cfset var entityField = "">
		<cfset var validValues = "">
		<cfset var isNullable = true>
		<cfset var hasValidValues = true>
		<cfset var maxLength = 0>
		<cfset var dataType = "text">
		<cfset var validValueQry = "">
		<cfset var result = structNew()>
		<cfset var getValidValuesForDisplay = "">
		<cfset var properName = "">
		<cfset var fieldMetaData = structNew()>
		<cfset var getFlagGroupFlags = "">
		<cfset var getValidValues = "">

		<cfset entityStruct[entityType] = structNew()>

		<cfset fieldStruct = application.com.rights.getEntityFieldRights(entityTypeID=entityType,returnColumns="field,[view],edit",returnApiName=arguments.returnApiName,haveRightsForMethod="view")>

		<cfloop collection="#fieldStruct#" item="entityField">
			<cfset tableFieldStruct = getTableFieldStructure(tablename=entityType,fieldname=entityField)>

			<!--- initialise some values --->
			<cfset isNullable = true>
			<cfset hasValidValues = false>
			<cfset maxLength = 0>
			<cfset validValues = "">

			<cfif tableFieldStruct.isOK>
				<!--- dealing with a table field --->
				<cfif tableFieldStruct.fieldType eq "field">
					<cfset dataType = tableFieldStruct.apiDataType>
					<cfset hasValidValues = tableFieldStruct.hasValidValues>
					<cfset maxLength = tableFieldStruct.maxLength>
					<cfset isNullable = tableFieldStruct.isNullable>

					<cfif hasValidValues>
						<!--- TODO: work out what to do when we have an error in here --->
						<cftry>
							<cfset validValueQry = application.com.relayForms.getValidValues(fieldname=entityType&"."&entityField,countryId=countryIDPerspective)>

							<cfquery name="getValidValuesForDisplay" dbtype="query">
								select dataValue,displayValue from validValueQry
							</cfquery>

							<cfset validValues = getValidValuesForDisplay>
							<cfcatch>
								<cfset application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
							</cfcatch>
						</cftry>
					<cfelseif entityType eq "organisation" and tableFieldStruct.name eq "organisationTypeID">

						<cfquery name="getValidValuesForDisplay" datasource="#application.siteDataSource#">
							select organisationTypeID as dataValue, typeTextID as displayValue from organisationType with (noLock)
						</cfquery>

						<cfset validValues = getValidValuesForDisplay>
						<cfset hasValidValues = true>

					<cfelseif entityType eq "opportunity">

						<cfif tableFieldStruct.name eq "statusID">
							<cfquery name="validValues" datasource="#application.siteDataSource#">
								select statusID as dataValue, status as displayValue from oppStatus with (noLock)
							</cfquery>

						<cfelseif tableFieldStruct.name eq "stageID">
							<cfquery name="validValues" datasource="#application.siteDataSource#">
								select opportunityStageID as dataValue, opportunityStage as displayValue from oppStage with (noLock)
							</cfquery>

						<cfelseif tableFieldStruct.name eq "currency">
							<cfquery name="validValues" datasource="#application.siteDataSource#">
								select currencyISOCode as dataValue, currencyName as displayValue from currency with (noLock)
							</cfquery>
						</cfif>

						<cfif isQuery(validValues)>
							<cfset hasValidValues = true>
						</cfif>
					</cfif>

				<!--- dealing with flags/flagGroups --->
				<cfelseif listFindNoCase("flag,flagGroup",tableFieldStruct.fieldType)>

					<cfif structKeyExists(tableFieldStruct,"useValidValues")>
						<cfset hasValidValues = tableFieldStruct.useValidValues>
					</cfif>

					<cfswitch expression="#tableFieldStruct.flagType.name#">
						<cfcase value="integer">
							<cfset dataType = "integer">
						</cfcase>
						<cfcase value="text">
							<cfset maxLength = "4000">
							<cfset dataType = "text">
						</cfcase>
						<cfcase value="date">
							<cfset dataType = "date">
						</cfcase>
						<cfcase value="checkbox">
							<cfset dataType = "text">
							<cfset hasValidValues = true>
						</cfcase>
						<cfcase value="radio">
							<cfset dataType = "text">
							<cfset hasValidValues = true>
						</cfcase>
					</cfswitch>

					<!--- this is currently only used for the api user, in which case a boolean flag has valid values.. the flagTextIds of the flags
						in the flagGroup. If this is to be used elsewhere, this may need to be modified --->
					<cfif tableFieldStruct.fieldType eq "flagGroup" and tableFieldStruct.flagType.datatable eq "boolean">
						<cfquery name="getFlagGroupFlags" datasource="#application.siteDataSource#">
							select flagTextID,(select max(len(flagTextID)) from flag with (noLock)
								where flagGroupId =  <cf_queryparam value="#tableFieldStruct.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and len(isNull(flagTextID,'')) > 0) as maxLength
							from flag
							where flagGroupID =  <cf_queryparam value="#tableFieldStruct.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and len(isNull(flagTextID,'')) > 0
						</cfquery>

						<cfquery name="getValidValues" dbtype="query">
							select flagTextID as dataValue,flagTextID as displayValue from getFlagGroupFlags
						</cfquery>
						<cfset validValues = getValidValues>

						<cfset maxLength = getFlagGroupFlags.maxLength>
					</cfif>
				</cfif>

				<cfset properName = ucase(left(entityField, 1))&right(entityField,len(entityField)-1)>
				<cfset fieldMetaData[properName] = structNew()>
				<cfset fieldMetaData[properName].dataType = lcase(dataType)>
				<cfset fieldMetaData[properName].hasValidValues = IIF(hasValidValues,DE("true"),DE("false"))>
				<cfset fieldMetaData[properName].field = properName>
				<cfset fieldMetaData[properName].maxLength = maxLength>
				<cfset fieldMetaData[properName].isNullable = IIF(isNullable,DE("true"),DE("false"))>
				<cfset fieldMetaData[properName].view = IIF(fieldStruct[entityField].view,DE("true"),DE("false"))>
				<cfset fieldMetaData[properName].edit = IIF(fieldStruct[entityField].edit,DE("true"),DE("false"))>
				<cfif hasValidValues>
					<cfset fieldMetaData[properName].validValues = validValues>
				</cfif>
			</cfif>
		</cfloop>

		<cfset entityStruct[entityType] = fieldMetaData>
		<cfset structAppend(result,entityStruct,"true")>

		<cfreturn result>
	</cffunction>


	<cffunction name="validateForeignKeys" access="public" returnType="struct">
		<cfargument name="entityDetails" type="struct" required="false"> <!--- passed in when updating/inserting a single entity.. --->
		<cfargument name="tablename" type="string" required="false"> <!--- name of temp table to perform validation on.. used for bulk inserting/updating --->
		<cfargument name="entityTypeID" type="string" required="true">

		<cfset var result = {errorCode = "",isOk=true}>
		<cfset var doesForeignKeyExist = "">
		<cfset var column = "">
		<cfset var foreignEntity = "">
		<cfset var columnList = "">
		<cfset var foreignUniqueKey = "">
		<cfset var invalidKeys = false>
		<cfset var columnName = "">
		<cfset var entityStruct = getEntityType(entityTypeID=arguments.entityTypeID)>
		<cfset var getTableColumns = "">

		<cfif structKeyExists(arguments,"entityDetails")>
			<cfset columnList = structKeyList(arguments.entityDetails)>
		<cfelseif structKeyExists(arguments,"tablename")>

			<cfquery name="getTableColumns" datasource="#application.siteDataSource#">
				select * from #arguments.tablename# where 1=0
			</cfquery>

			<cfset columnList = getTableColumns.columnList>
		<cfelse>
			<cfreturn result>
		</cfif>

		<cfloop list="#columnList#" index="column">
			<cfif right(column,2) eq "ID">
				<cfif structKeyExists(arguments,"tablename") or arguments.entityDetails[column] neq "" or arguments.entityDetails[column] neq 0>
					<cfset foreignEntity = left(column,len(column)-2)>

					<!--- having to hard-code some foreign entity keys... primarily done for the opportunity table --->
					<cfif foreignEntity.toLowerCase().endsWith("person")>
						<cfset foreignEntity = "person">
					<cfelseif foreignEntity.toLowerCase().endsWith("location")>
						<cfset foreignEntity = "location">
					<cfelseif foreignEntity.toLowerCase().endsWith("organisationType")>
						<cfset foreignEntity = "organisationType">
					<cfelseif foreignEntity.toLowerCase().endsWith("organisation")>
						<cfset foreignEntity = "organisation">
					<cfelseif entityStruct.tablename eq "opportunity">
						<cfif foreignEntity eq "status">
							<cfset foreignEntity = "oppStatus">
						<cfelseif foreignEntity eq "stage">
							<cfset foreignEntity = "oppStage">
						<cfelseif foreignEntity eq "entity">
							<cfset foreignEntity = "organisation">
						</cfif>
					<cfelseif entityStruct.tablename eq "product">
						<cfif foreignEntity eq "campaign">										<!--- 2016-10-17 PPB JAB001 added exception --->
							<cfset foreignEntity = "promotion">
					</cfif>
					</cfif>

					<cfif structKeyExists(application.entityTypeID,foreignEntity)>

						<cfset uniqueKey=getEntityType(entityTypeID=foreignEntity).uniqueKey>

						<!---RJT Overrides for "wierd" entities who don't have their IDs as the official unique keys --->
						<cfif foreignEntity EQ "language">
							<cfset uniqueKey="languageID">
						</cfif>

						<cfset foreignUniqueKey = getEntityType(entityTypeID=foreignEntity).uniqueKey>
						<cfset columnName = column>
						<cfif request.relayCurrentUser.isApiUser>
							<cfset columnName = getTableFieldStructure(tablename=getEntityType(entityTypeID=foreignEntity).tablename,fieldname=foreignUniqueKey).apiName>
						</cfif>

						<!--- this table is a temp table that holds an error column that can be updated. --->
						<cfif structKeyExists(arguments,"tablename")>
							<cfquery name="doesForeignKeyExist" datasource="#application.siteDataSource#">
								update #arguments.tablename#
									set error = 'INVALID_REFERENCE_ID',errorField =  <cf_queryparam value="#columnName#" CFSQLTYPE="CF_SQL_VARCHAR" >
								from #arguments.tablename# t with (noLock) left join #foreignEntity# f with (noLock)
									on t.#column# = f.#foreignUniqueKey#
								where f.#foreignUniqueKey# is null
									and t.#column# is not null
								and error is null
							</cfquery>

							<cfquery name="doesForeignKeyExist" datasource="#application.siteDataSource#">
								select * from #arguments.tablename# where error = 'INVALID_REFERENCE_ID'
							</cfquery>

							<cfif doesForeignKeyExist.recordCount>
								<cfset invalidKeys = true>
							</cfif>

						<cfelse>
							<cfquery name="doesForeignKeyExist" datasource="#application.siteDataSource#">
								select 1 from #foreignEntity# with (noLock) where #uniqueKey# = <cf_queryparam value="#arguments.entityDetails[column]#" cfSqlType="CF_SQL_INTEGER">
							</cfquery>

							<cfif not doesForeignKeyExist.recordCount>
								 <cfset invalidKeys  = true>
							</cfif>
						</cfif>

						<cfif invalidKeys>
							<cfset result.errorCode = "INVALID_REFERENCE_ID">
							<cfset result.field = column>
							<cfif not structKeyExists(arguments,"tablename")>
								<cfset result.fieldValue = arguments.entityDetails[column]>
							</cfif>
							<cfset result.isOK = false>
							<cfreturn result>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="validateData" access="public" returnType="struct">
		<cfargument name="entityDetails" type="struct" required="false">
		<cfargument name="tablename" type="string" required="false"> <!--- a temp table which has 2 columns that are expected.. error and errorField.. these will get updated --->
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="returnApiName" type="boolean" default="false">
		<cfargument name="method" type="string" default="insert">
		<cfargument name="processDespiteErrors" type="boolean" default="false">
		<cfargument name="countryIDPerspective" type="string" default="#request.relayCurrentUser.countryID#" hint="The country ID from whose perspective to validate data">

		<cfset var entityTypeStruct = getEntityType(entityTypeID=arguments.entityTypeId)>
		<cfset var result = {errorCode = "",isOk=true}>
		<cfset var column = "">
		<cfset var columnList = "">
		<cfset var tableFieldMetaData = getEntityMetaData(entityTypeID=arguments.entityTypeID,countryIDPerspective=countryIDPerspective )>
		<cfset var fieldMetaData = structNew()>
		<cfset var invalidData = false>
		<cfset var fieldValue = "">
		<cfset var errorCode = "">
		<cfset var columnName = "">
		<cfset var caseStatement_SQL = "">
		<cfset var updateBooleanFields = "">
		<cfset var sortedFieldsFromMetaData = "">
		<cfset var flagTextID = "">
		<cfset var validValues = "">
		<cfset var field = "">
		<cfset var getTableColumns = "">
		<cfset var setErrorColumn = "">
		<cfset var isDataValid = "">
		<cfset var utcDateStruct = structNew()>


		<!---
			It is important to understand that this function, if passed in a tablename, treats nulls where data has not been supplied.
			If someone has passed in orgHQ as an empty value, then it will appear as an empty string. Different to nulls.

		 --->

		<cfif structKeyExists(arguments,"entityDetails")>
			<cfset columnList = listSort(structKeyList(arguments.entityDetails),"textNoCase","asc")>
		<cfelseif structKeyExists(arguments,"tablename")>

			<cfquery name="getTableColumns" datasource="#application.siteDataSource#">
				select * from <cf_queryObjectName value="#arguments.tablename#"> where 1=0
			</cfquery>

			<cfset columnList = listSort(getTableColumns.columnList,"textnocase","asc")>
		<cfelse>
			<cfreturn result>
		</cfif>

		<!--- check that all required fields have been passed through --->
		<cfif arguments.method eq "insert">
			<cfset fieldMetaData = tableFieldMetaData[entityTypeStruct.tablename]>
			<cfset sortedFieldsFromMetaData = listSort(structKeyList(fieldMetaData),"textNoCase","asc")>

			<cfloop list="#sortedFieldsFromMetaData#" index="field">
				<cfif not fieldMetaData[field].isNullable and fieldMetaData[field].edit and not listFindNoCase(columnList,field) and field neq entityTypeStruct.uniqueKey>

					<cfset columnName = field>
					<cfif arguments.returnApiName>
						<cfset columnName = getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=field).apiName>
					</cfif>

					<cfif structKeyExists(arguments,"tablename")>
						<cfquery name="setErrorColumn" datasource="#application.siteDataSource#">
							update <cf_queryObjectName value="#arguments.tablename#"> set error = 'REQUIRED_DATA_MISSING',errorField =  <cf_queryparam value="#columnName#" CFSQLTYPE="CF_SQL_VARCHAR" >
							where error is null
						</cfquery>
					</cfif>

					<cfif not arguments.processDespiteErrors>
						<cfset result.errorCode = "REQUIRED_DATA_MISSING">
						<cfset result.isOk = false>
						<cfset result.field = columnName>
						<cfreturn result>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>

		<cfloop list="#columnList#" index="column">
			<cfif structKeyExists(tableFieldMetaData[entityTypeStruct.tablename],column)>
				<cfset fieldMetaData = tableFieldMetaData[entityTypeStruct.tablename][column]>

				<cfset columnName = column>
				<cfif arguments.returnApiName>
					<cfset columnName = getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=column).apiName>
				</cfif>

				<cfif structKeyExists(arguments,"tablename")>
					<!--- have an issue with trying to validate validValues... so if we have an error, just continue and move on --->
					<cftry>

						<cfsavecontent variable="caseStatement_SQL">
							<cfoutput>
								case
									<cfif not fieldMetaData.isNullable and arguments.method eq "insert" and not listFindNoCase("text,ntext,varchar,nvarchar,string",fieldMetaData.dataType)>when #column# is null then 'REQUIRED_DATA_MISSING'</cfif>
									<cfswitch expression="#fieldMetaData.dataType#">
										<cfcase value="integer,float,numeric">when isNumeric(isNull(#column#,1)) != 1 then 'INVALID_DATA_TYPE'</cfcase>
										<cfcase value="date,datetime">when isDate(isNull(#column#,getDate())) != 1 then 'INVALID_DATA_TYPE'</cfcase>
										<cfcase value="boolean,bit">when isNull(#column#,'1') not in ('yes','no','true','false','1','0') then 'INVALID_DATA_TYPE'</cfcase>
									</cfswitch>
									<cfif fieldMetaData.maxLength neq 0>when len(#column#) > #fieldMetaData.maxLength# then 'EXCEEDS_MAX_LENGTH'</cfif>
									<cfif fieldMetaData.hasValidValues and structKeyExists(fieldMetaData,"validValues")>
										<cfset validValues = fieldMetaData.validValues>
										<cfif isQuery(validValues)>
											<cfset validValues = valueList(validValues.dataValue)>
										</cfif>
										<cfif validValues neq "">
											when len(#column#) > 0 and #column# not in (
												<cf_queryparam value="#validValues#" cfSqlType="CF_SQL_VARCHAR" list="true">)
											then 'INVALID_VALID_VALUE'
										</cfif>
									</cfif>
								else null end
							</cfoutput>
						</cfsavecontent>

						<cftry>

							<!--- update if we are processing a boolean field, as we can expect yes/no, true/false and 1/0 --->
							<cfif listFindNoCase("boolean,bit",fieldMetaData.dataType)>
								<cfquery name="updateBooleanFields" datasource="#application.siteDataSource#">
									update <cf_queryObjectName value="#arguments.tablename#"> set #column# = case when #column# in ('yes','true','1') then '1' when #column# in ('no','false','0') then '0' else #column# end
								</cfquery>
							</cfif>

							<cfquery name="isDataValid" datasource="#application.siteDataSource#">

								<cfif arguments.processDespiteErrors>
									update <cf_queryObjectName value="#arguments.tablename#"> set error = #preserveSingleQuotes(caseStatement_SQL)#,errorField =  <cf_queryparam value="#columnName#" CFSQLTYPE="CF_SQL_VARCHAR" >
								<cfelse>
									select <cfif listFindNoCase(columnList,entityTypeStruct.uniqueKey)>#entityTypeStruct.uniqueKey# <cfif arguments.returnApiName>as #getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=entityTypeStruct.uniqueKey).apiName#</cfif>,</cfif> #column# as value,
										 #preserveSingleQuotes(caseStatement_SQL)# as error,'#columnName#' as errorField
									from <cf_queryObjectName value="#arguments.tablename#">
								</cfif>
									where (1=0
										<cfif not fieldMetaData.isNullable and arguments.method eq "insert" and not listFindNoCase("text,ntext,varchar,nvarchar,string",fieldMetaData.dataType)>or #column# is null</cfif>
										<cfswitch expression="#fieldMetaData.dataType#">
											<cfcase value="integer,float,numeric">or isNumeric(isNull(#column#,1)) != 1</cfcase>
											<cfcase value="date,datetime">or isDate(isNull(#column#,getDate())) != 1</cfcase>
											<cfcase value="boolean,bit">or isNull(#column#,'1') not in ('yes','no','true','false','1','0')</cfcase>
										</cfswitch>
										<cfif fieldMetaData.maxLength neq 0>o r len(#column#) >  <cf_queryparam value="#fieldMetaData.maxLength#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
										<cfif fieldMetaData.hasValidValues and structKeyExists(fieldMetaData,"validValues")>
											<cfset validValues = fieldMetaData.validValues>
											<cfif isQuery(validValues)>
												<cfset validValues = valueList(validValues.dataValue)>
											</cfif>
											<cfif validValues neq "">or len(#column#) > 0 and #column# not in (<cf_queryparam value="#validValues#" cfSqlType="CF_SQL_VARCHAR" list="true">)
											</cfif>
										</cfif>
									)
									<!--- update only error fields that don't already have an error --->
									<cfif arguments.processDespiteErrors>
										and error is null
									</cfif>
							</cfquery>

							<cfcatch>
								<cfset application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
							</cfcatch>
						</cftry>

						<cfif not arguments.processDespiteErrors and isDataValid.recordCount>
							<cfset result.errors = isDataValid>
							<cfset result.errorCode = "INVALID_DATA">
							<cfset result.field = columnName>
						</cfif>

						<cfcatch>
							<cfset application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
						</cfcatch>
					</cftry>

				<!--- working through the argument details struct --->
				<cfelse>

					<cfset fieldValue = arguments.entityDetails[column]>
					<cfif fieldValue neq "">
						<cfif listFindNoCase("integer,float,numeric",fieldMetaData.dataType) and not isNumeric(fieldValue)>
							<cfset errorCode = "INVALID_DATA_TYPE">
						<cfelseif listFindNoCase("date,datetime",fieldMetaData.dataType) and not listFindNoCase("created,lastUpdated",column)>
							<!--- NJH 2012/11/04 Case 431491 - check if date is in utc format
								the created/lastUpdated fields have been set in prepareEntityStruct, and are not fields that the user can manipulate --->
							<cfset utcDateStruct = application.com.dateFunctions.extractDateTimeFromUTCString(dateInUTCFormat=fieldValue)>
							<cfif utcDateStruct.isOK>
								<cfset arguments.entityDetails[column] = utcDateStruct.localDateTime>
							<cfelse>
								<cfif not isDate(replace(replace(fieldValue,"T"," "),"Z",""))>
									<cfset errorCode = "INVALID_DATA_TYPE">
								<cfelse>
									<cfset errorCode = "INVALID_DATE_FORMAT">
								</cfif>
							</cfif>
						<cfelseif listFindNoCase("bit,boolean",fieldMetaData.dataType) and not isBoolean(fieldValue)>
							<cfset errorCode = "INVALID_DATA_TYPE">
						<cfelseif listFindNoCase("uniqueidentifier",fieldMetaData.dataType) and not isValid("guid",fieldValue)>
							<cfset errorCode = "INVALID_DATA_TYPE">
						<cfelseif fieldMetaData.maxLength neq 0 and fieldMetaData.maxLength lt len(fieldValue)>
							<!--- if it's a flag group, then the value will be a list of flagTextIDs, so loop over the list and ensure that each is under the maximum length --->
							<cfif getTableFieldStructure(tablename=entityTypeStruct.tablename,fieldname=column).fieldType eq "flagGroup">
								<cfloop list="#fieldValue#" index="flagTextID">
									<cfif fieldMetaData.maxLength lt len(flagTextID)>
										<cfset errorCode = "EXCEEDS_MAX_LENGTH">
										<cfbreak>
									</cfif>
								</cfloop>
							<cfelse>
								<cfset errorCode = "EXCEEDS_MAX_LENGTH">
							</cfif>
						<cfelseif fieldMetaData.hasValidValues and isQuery(fieldMetaData.validValues) and fieldMetaData.validValues.recordCount>
							<cfset validValues = fieldMetaData.validValues>
							<cfif isQuery(validValues)>
								<cfset validValues = valueList(validValues.dataValue)>
							</cfif>
							<cfif not listFindNoCase(validValues,fieldValue)>
								<cfset errorCode = "INVALID_VALID_VALUE">
							</cfif>
						</cfif>
					<cfelseif not fieldMetaData.isNullable and arguments.method eq "insert" and not listFindNoCase("text,ntext,varchar,nvarchar,string",fieldMetaData.dataType) and column neq entityTypeStruct.uniqueKey>
						<cfset errorCode = "REQUIRED_DATA_MISSING">
					</cfif>

					<cfif errorCode neq "">
						<cfset result.field = columnName>
						<cfset result.fieldValue = fieldValue>
						<cfset result.errorCode = errorCode>
						<cfset result.isOK = false>
						<cfreturn result>
					</cfif>

				</cfif>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="getTableFieldNameForCurrentUser" access="public" hint="Returns the expected fieldname for the current user." returnType="string">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="fieldname" type="string" required="true">

		<cfset var tableFieldStruct = getTableFieldStructure(tablename=arguments.tablename,fieldname=arguments.fieldname)>
		<cfif isDefined("request.relayCurrentUser.isApiUser") and request.relayCurrentUser.isApiUser>
			<cfreturn tableFieldStruct.apiName>
		<cfelse>
			<cfreturn tableFieldStruct.name>
		</cfif>
	</cffunction>


	<cffunction name="hasEntityBeenDeleted" access="public" hint="Returns whether an entity has been deleted. A return of false may just mean that the entity does not exist." returntype="boolean" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" required="true">

		<cfset var entityType = getEntityType(entityTypeID = arguments.entityTypeID)>
		<cfset var result = false>
		<cfset var entityDeleted = "">
		<cfset var isDeleted = false>

		<cfquery name="entityDeleted" datasource="#application.siteDataSource#">
			if exists (select 1 from sysobjects where name =  <cf_queryparam value="#entityType.tablename#Del" CFSQLTYPE="CF_SQL_VARCHAR" >  and type='U')
				select 1 as deleted from <cf_queryObjectName value="#entityType.tablename#Del"> with (noLock) where <cf_queryObjectName value="#entityType.uniqueKey#"> = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer">
			else
				select 0 as deleted
		</cfquery>

		<cfset isDeleted = entityDeleted.deleted>
		<cfif isDeleted eq "">
			<cfset isDeleted = false>
		</cfif>

		<!--- check in the core table if it exists --->
		<cfif not isDeleted>
			<cfquery name="entityDeleted" datasource="#application.siteDataSource#">
				select 1 from <cf_queryObjectName value="#entityType.tablename#">  with (noLock)
				<cfif listFindNoCase("person,organisation,location",entityType.tablename)>
					inner join booleanFlagData bfd with (noLock) on bfd.entityID =  <cf_queryObjectName value="#entityType.uniqueKey#">  and bfd.flagID = #application.com.flag.getFlagStructure(flagID="delete#entityType.tablename#").flagID#
				</cfif>
				where <cf_queryObjectName value="#entityType.uniqueKey#"> = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer">
				<cfif getTableFieldStructure(tablename=entityType.tablename,fieldname="deleted").isOK>
					and deleted = 1
				<!--- if it's not a pol record and it doesn't have a deleted column, then we can't tell whether it's been deleted, so we will just return a false --->
				<cfelseif not listFindNoCase("person,organisation,location",entityType.tablename)>
					and 1=0
				</cfif>
			</cfquery>

			<cfset isDeleted = entityDeleted.recordCount>
		</cfif>

		<cfreturn isDeleted>
	</cffunction>


	<!---
	Checks whether
		a) An entity exists
		b) Is not marked/flagged as deleted
		c) User has rights
	--->

	<cffunction name="isEntityAvailableForGetUpdateOrDelete" access="private" hint="Works out whether the entity is deleted or whether it exists" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="entityDetails" type="struct" default="#structNew()#">
		<cfargument name="testUserEntityRights" type="boolean" default="true">


		<cfset var entityType = getEntityType(entityTypeID = arguments.entityTypeID)>
		<cfset var result = {isOK=true,errorCode="",message=""}>
		<cfset var doesEntityExist = "">
		<cfset var rightsStruct = structNew()>

		<cfquery name="doesEntityExist" datasource="#application.siteDataSource#">
			select 1 from #entityType.tablename# where #entityType.uniqueKey# = <cf_queryparam value="#arguments.entityID#" cfSqlType="CF_SQL_INTEGER">
		</cfquery>

		<cfif not doesEntityExist.recordCount>
			<cfset result.isOK = false>
			<cfset result.errorCode = "NONEXISTENT_ENTITY">
		<cfelse>
			<cfif arguments.testUserEntityRights>
				<cfset rightsStruct = checkEntityRights(entityId=arguments.entityID,entityDetails=arguments.entityDetails,entityTypeID=entityType.tablename)>
				<cfif not rightsStruct.view>
					<cfset result.errorCode = "INSUFFICIENT_RIGHTS">
					<cfset result.isOk = false>
				</cfif>
			</cfif>
			<cfif result.isOk>
				<cfif hasEntityBeenDeleted(argumentCollection=arguments)>
					<cfset result.isOK = false>
					<cfset result.errorCode = "ENTITY_DELETED">
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="setDefaultTranslations" access="private" returnType="struct">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="translatedColumns" type="string" required="true">
		<cfargument name="entityDetails" type="struct" required="true">
		<cfargument name="mode" type="string" default="update">

		<cfset var getOldTranslations = "">
		<cfset var oldTranslations = structNew()>
		<cfset var updateDefaultTranslation = "">
		<cfset var entityTypeStruct = getEntityType(entityTypeID=arguments.entityTypeID)>
		<cfset var phraseTextID = "">
		<cfset var oldPhraseText = "">
		<cfset var newPhraseText = "">
		<cfset var result = {isOK=true}>
		<cfset var column = "">

		<cftry>
			<cfquery name="getOldTranslations" datasource="#application.siteDataSource#">
				select #arguments.translatedColumns# from #entityTypeStruct.tablename# where #entityTypeStruct.uniqueKey# = #arguments.entityID#
			</cfquery>

			<cfset oldTranslations = application.com.structureFunctions.queryRowToStruct(query=getOldTranslations,row=1)>

			<cfloop collection="#oldTranslations#" item="column">

				<cfset phraseTextID=replaceNoCase(column,"_defaultTranslation","")>
				<cfset oldPhraseText = trim(oldTranslations[column])>
				<cfset newPhraseText = trim(arguments.entityDetails[column])>

				<cfif arguments.mode eq "update">
					<cfquery name="updateDefaultTranslation" datasource="#application.siteDataSource#">
						update phrases set phraseText =  <cf_queryparam value="#newPhraseText#" CFSQLTYPE="CF_SQL_VARCHAR" > ,lastUpdated=getDate(),lastUpdatedBy=#request.relayCurrentUser.personID#
						where ident =
							(
								select top 1 ident from (
							 	 	select distinct phraseText,p.ident, defaultForThisCountry, countryID from phrases p
							 	 	inner join phraseList pl on p.phraseId = pl.phraseID
							 			where pl.entityTypeID=#arguments.entityTypeID# and pl.entityId = #arguments.entityID# and contains(phraseText,<cf_queryparam value="""#oldPhraseText#""" CFSQLTYPE="CF_SQL_VARCHAR" >)
							 		and phraseTextID =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
							 	)
							 	as filtered
							 	where phraseText =  <cf_queryparam value="#oldPhraseText#" CFSQLTYPE="CF_SQL_VARCHAR" >
							 	order by defaultForThisCountry desc, countryID asc
						 	)
					</cfquery>
				<cfelse>
					<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID=phraseTextID,entityTypeID=arguments.entityTypeID,entityid=arguments.entityID,phraseText=newPhraseText)>
				</cfif>
			</cfloop>

			<cfcatch>
				<cfset result.isOK = false>
				<cfrethrow>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<cffunction name="getEntityRelatedTables" access="public">
		<cfargument name="entityTypeID" required="false">
		<cfargument name="filterOnlyScreensAndFlags" type="boolean" required="false">

		<cfset var entityTypeStruct = getEntityType(entityTypeID=arguments.entityTypeId)>
		<cfset var entityRelatedTables = "">


		<cfquery name="entityRelatedTables" datasource="#application.siteDataSource#">
			select ce.*, st.entityTypeID as referencedEntityTypeID from
				(select distinct s.entityTypeID,EntityName,description,UniqueKey,referencedtable
				from schemaTable s
				join INFORMATION_SCHEMA.COLUMNS isc on s.entityname = isc.TABLE_NAME
				JOIN
				(SELECT
					fk.name,
					OBJECT_NAME(fk.parent_object_id) 'Parent table',
					c1.name 'Parent column',
					OBJECT_NAME(fk.referenced_object_id) 'Referencedtable',
					c2.name 'Referenced column'
				FROM
					sys.foreign_keys fk
				INNER JOIN
					sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
				INNER JOIN
					sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
				INNER JOIN
					sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id) as FK
				ON s.entityName = FK.[Parent table] and isc.column_name = FK.[referenced column]
				<cfif structKeyExists(arguments,"filterOnlyScreensAndFlags") and arguments.filterOnlyScreensAndFlags>where s.screensExist = 1 and s.flagsExist = 1 and s.uniqueKey is not null</cfif>
				) as ce
			join schematable as st on ce.referencedtable = st.entityname
			<cfif structKeyExists(arguments,"entityTypeID")>where ce.entityname = <cf_queryparam value="#getEntityType(entityTypeID=arguments.entityTypeId).tablename#" cfsqltype="CF_SQL_VARCHAR"></cfif>
			order by referencedtable, st.entityName
		</cfquery>

		<cfreturn entityRelatedTables>
	</cffunction>

	<cffunction name="recursiveListRelatedTables" access="public">
		<cfargument name="tableName" required="true" type="string">
		<cfargument name="filterOnlyScreensAndFlags" type="boolean" required="false">

		<cfset var referencedTableList = "">
		<cfset var getEntityRelatedTables = getEntityRelatedTables(entityTypeID = tablename, filterOnlyScreensAndFlags = filterOnlyScreensAndFlags)>

		<cfloop query="getEntityRelatedTables">
			<cfif not listFind(referencedTableList,REFERENCEDTABLE)>
				<cfset referencedTableList = listappend(referencedTableList,"#REFERENCEDTABLE#")>
				<cfset arguments.tableName = REFERENCEDTABLE>
				<cfset var referencedTableListToAdd = recursiveListRelatedTables(argumentCollection=arguments)>
				<cfif referencedTableListToAdd neq "">
					<cfset referencedTableList = listappend(referencedTableList,"#referencedTableListToAdd#")>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn referencedTableList>
	</cffunction>

	<!---
	WAB 2013-12-03 2013RoadMap2 Item 25 Contact History
	Function for accessing new relatedEntities Table - first used for storing relationship between systemEmails and what entityID they relate to
	--->
	<cffunction name="getRelatedEntities" access="public" returnType="query">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">

		<cfset var getRelatedEntitiesQry = "">

		<cfquery name="getRelatedEntitiesQry">
		select
			re.relatedEntityTypeID as entityTypeID, re.relatedEntityID as entityID, en.name, s.entityName
		from
			relatedEntity re
				left join
			ventityName en on re.relatedEntityTypeID = en.EntityTypeID AND re.relatedEntityID = en.EntityID
				left join
			schemaTable s on s.entityTypeID = re.relatedEntityTypeID
		where re.entityTypeID =  <cf_queryparam value="#application.entityTypeID[entityType]#" CFSQLTYPE="CF_SQL_INTEGER" >  AND re.entityID = #entityID#

		UNION

		select
			re.EntityTypeID, re.EntityID, en.name, s.entityName
		from
			relatedEntity re
				left join
			ventityName en on re.EntityTypeID = en.EntityTypeID AND re.EntityID = en.EntityID
				left join
			schemaTable s on s.entityTypeID = re.EntityTypeID
		where re.relatedEntityTypeID =  <cf_queryparam value="#application.entityTypeID[entityType]#" CFSQLTYPE="CF_SQL_INTEGER" >  AND re.relatedEntityID = #entityID#

		</cfquery>

		<cfreturn getRelatedEntitiesQry>

	</cffunction>


	<cffunction name="getEntityFieldMetaData" access="public" output="false" returnType="query" hint="Returns meta data for a given entity. This should be replacing getEntityMetaData!">
		<cfargument name="tablename" type="string" required="true" hint="The relayware entity">
		<cfargument name="dataType" type="string" required="false" hint="Filter by a given datatype">
		<cfargument name="column" type="string" required="false" hint="The table column name">

		<cfset var getFieldMetaData = "">

		<cfquery name="getFieldMetaData">
			select * from vFieldMetaData
			where
			<!--- add a 1=0 if tablename is not valid as we don't want to bring back everything, as it's quite a hefty view --->
			<cfif arguments.tablename neq "">tablename=<cf_queryparam value="#getEntityType(entityTypeID=arguments.tablename).tablename#" cfsqltype="cf_sql_varchar"><cfelse>1=0</cfif>
			<cfif structKeyExists(arguments,"column") and arguments.column neq "">
				and name = <cf_queryparam value="#arguments.column#" cfsqltype="cf_sql_varchar">
			</cfif>
			<cfif structKeyExists(arguments,"dataType")>
				and dataType = <cf_queryparam value="#arguments.dataType#" cfsqltype="cf_sql_varchar">
			</cfif>
			order by name
		</cfquery>

		<cfreturn getFieldMetaData>
	</cffunction>

	<!---2015-10-22 ESZ case 446050--->
	<!--- 2015-11-20   ACPK    PROD2015-441 Fixed checkIfPersistanceIsNeeded not being correctly defined --->
    <cffunction name="checkIfPersistanceIsNeeded"  access="public" output="false" returnType="boolean" hint="Returns true if passed strucure contains fields eligible for persist ( not from core entity table ) ">
        <cfargument name="tableDetailStruct" type="struct" required="true">

        <cfset crudList = "LastUpdated,LastUpdatedBy,LastUpdatedByPerson">
        <cfloop collection="#tableDetailStruct#" item="key">
            <cfif ListFindNoCase(crudList, key) EQ 0>
                <cfreturn true>
            </cfif>
        </cfloop>
        <cfreturn false>
    </cffunction>

</CFCOMPONENT>