<!--- �Relayware. All Rights Reserved 2014 --->
 <!---
File name:			rights.cfc
Author:				WAB
Date started:		2004/11/04
Description:

Allows functions that find out what rights users have in the system

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
01-April-2006 		WAB			altered to handle different login levels (0,1,2)
08-May-2006			WAB			corrected bug which meant that wrong list (blank) of user groups coming back when permissions other then 1 were requested
01-May-2008			WAB			added functions for determining rights to POLs
23-July-2008		SSS			Function that tells you for a certain usergroup who has what rights to which country
18 Feb 2009			NYB 		Sophos Stargate 2 - added 2 new functions
14 Jul 2009			NJH			P-FNL069 - added new functions to check a user's rights on a particular entity
2010/04/21			NJH			P-PAN002 - give temporary rights to a person for a given organisation. As specified by request.rightsOverride.organisation
2010/04/21 			NJH/WAB 	P-PAN002 - provide temporary rights to an organisation
2011/09/27			WAB 		P-TND109 removed required for entityID to be numeric to allow for rights to add new records
2011/10/25 			NYB			LHID7963 added getDeleteRights functions
2012-02-01 			NYB 		Case 425248 created checkInternalPermissionsForPerson.  Ended up not using it but left it in because it's a handy function to have
2013-10-17 			NYB 		Case 437263 changed getEntityFieldRights to return a duplicate copy of session.rights[entityTypeStruct.tablename] instead of a pointer/reference to this
								- to stop session.rights.person.email.edit from being updated by getUserRightsToEntityField
2015-01-21 			WAB/NJH 	CASE 443542 problem in getEntityFieldRights(), caching single query regardless of arguments passed to function
2015-03-11			RPW			Altered filter on crm fields to allow them to be edited if Connector is turned off.
2015/11/11          DAN         fix resolving country rights for portal user to check his country as opposite to internal user country rights
2015/12/01			WAB			PROD2015-290 Visibility - element() function.  getElementParents now brings back formatted lists of rights, so removed some code which was doing that
2016-01-19			WAB			Var'ing to deal with BF-199
					RJT			Added returnColumns to getEntityFieldRights cache key (was ignoring the returnColumns for second request)
2016-01-25			RJT			Made getRightsFilterQuerySnippet safe for housekeeping tasks (was causing CF error previously)
2016/04/26			NJH			Sugar 448757 - part of performance enhancements - added query snippet in getRightsFilterQuerySnippet function for dealing with opportunities.
2016-05-12			WAB 		During PROD2016-778 correct spelling of getRecordRightsFilterQuerySnippet()  (which was missing the T in get!)
								Also altered function to use new style dbo.getRecordRightsTableByPerson and hasRecordRightsBitMask
								Seems to be only called for one entity (trngTrainingPath), so have updated that to have a hasRecordRightsBitMask column
2016/06/15			RJT			PROD2016-1194 Made countryRights filter also support countryGroups
2016/06/16			NJH			JIRA PROD2016-1273 - add rights snippets for leads
2016/09/24          DAN         PROD2016-2388/Case 451338 - also check for rights of disti sales person/location when accessing opportunity
2016-10-25			WAB			PROD2016-2586 Problems with getRightsFilter() when called from housekeeping task sending communications.  Was asking for request.relayCurrentUser.isInternal - which doesn't exist
								Recode to take an isInternal argument which can be defaulted to request.relayCurrentUser.isInternal, but passed in specifically when called from comms
2016-11-24          DAN         PROD2016-2790 - fix for access rights for primary contacts and lead partner managers


Possible enhancements:
For the userGroup table there should be a usergroup textID that will enable us to get unique names for usergroups at the moment the name is used so if 2 usergroups have the same name or the name is changed for some reason it will break the code.


 --->



<cfcomponent displayname="Functions for getting rights to objects" hint="">

	<cffunction access="public" name="Element" returnType="Struct">

		<cfargument name="elementid" type="string" required="true">
		<cfargument name="permission" type="numeric" required="false" default = "1">
		<cfargument name="topid" type="numeric" required="false" default = "0">
		<cfargument name="personid" type="numeric" required="false" default = "#request.relayCurrentUser.personid#">
		<cfargument name="countryid" type="string" required="false" default = "">

		<!--- WAB 2005-04-18 corrected bug with working out whether user needs to be logged in or not logged in (I had got it all backwards)

			WAB 2005-12-21 if unknown person and unkown country then set countryid to 0 so that only give records with no country rights

		--->
		<cfset var getElement = '' />
		<cfset var getElementRights = '' />
		<cfset var rights = '' />
		<cfset var getGenerationGivingRights = '' />
		<cfset var getStatus = '' />
		<cfset var getLogin = '' />
		<cfset var getBorderSet = '' />
		<cfset var checktop = '' />


		<cfif not isNumeric(elementid)>
			<cfset var getElement = "">
			<cfquery name="getElement" datasource = "#application.sitedatasource#">
			select id from element where elementtextid =  <cf_queryparam value="#elementid#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
			<cfset arguments.elementid = getElement.id>
		</cfif>

		<cfif countryid is "" and personid is application.unknownpersonid>
			<cfset arguments.countryid = 0>
		</cfif>


		<cfquery name="getElementRights" datasource = "#application.sitedatasource#" cachedwithin="#application.cachedContentTimeSpan#">
		<!--- WAB added caching 2009/05/08 ---> -- #application.cachedContentVersionNumber# (this variable is used to flush the cache when the content is changed)
		exec getElementParents @ElementID =  <cf_queryparam value="#elementid#" CFSQLTYPE="CF_SQL_INTEGER" >  , @topid = #topid#, @personid=#personid#, @permission = #permission#, @countryid =  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

			<cfset var result = structnew()>
			<cfset result.parentQuery = getElementRights>
			<cfset result.hasrights = false>
			<cfset result.usergroups = "">
			<cfset result.usergroupsOfParents = "">
			<cfset result.countries = "">
			<cfset result.countriesOfParents = "">
			<cfset result.elementexists = false>
			<cfset result.elementintree = false>
			<cfset result.islive = false>
			<cfset result.LoginRequired = 0>
			<cfset result.notLoggedInRequired = false>

			<!--- if no records are returned then there isn't an element --->
			<cfif getElementRights.recordcount is not 0>
				<cfset result.elementexists = true>
			</cfif>

			<!--- check for rights
				different logic for viewing and other rights
			 --->
			<cfif permission is 1>
				<!--- are there any items in tree which person doesn't have permissions to
					if so they don't have rights to this element
					only for view rights is countryscope an issue
				  --->
				<cfquery name = "rights"   dbtype="query" debug="No" >
				select * from getElementRights
				where recordrightsOK = 0 or countryscopeOK = 0
				</cfquery>

				<cfif rights.recordcount is 0 >
					<cfset result.hasrights = true>
				</cfif>

				<!--- make a nice list of rights with ands and ors --->
				<cfloop query="getelementrights">
					<cfif countryscope is not "">
						<cfset result.countries &= iif (result.countries is not "",DE(" AND "), DE("")) & countryscope>
					</cfif>

					<cfif recordrights is not "">
						<cfset result.usergroups &= iif (result.usergroups is not "",DE(" AND "), DE("")) & recordrights>
					</cfif>

					<cfif currentRow is getElementRights.recordcount -1>
						<cfset result.usergroupsofParents = result.usergroups>
						<cfset result.countriesofParents = result.countries>
					</cfif>
				</cfloop>

			<cfelse>
				<!---
				rights other than permission 1
				rights are determined by first element up the tree which has rights set --->
				<cfquery name = "getGenerationGivingRights"   dbtype="query" debug="No">
				select generation , recordrightsOK, recordrights
				from getElementRights
				where recordrights <> ''
				order by generation desc
				</cfquery>

				<cfif getGenerationGivingRights.recordcount is 0>
					<!--- no rights set on this tree so user has rights to access it at this level--->
					<cfset result.hasrights = true>
				<cfelse>
					<cfset result.usergroups = listchangedelims(getGenerationGivingRights.recordrights," or ")>
					<cfif getGenerationGivingRights.recordrightsok is 1>
					<cfset result.hasrights = true>
					<cfelse>
						<cfset result.hasrights = false>
					</cfif>
				</cfif>

			</cfif>

			<!--- for element to be live then all the parents must be status 4 --->
			<cfquery name = "getStatus"   dbtype="query" debug="No">
			select 1
			from getElementRights
			where statusid <> 4
			</cfquery>

			<cfif getStatus.recordcount is 0>
				<cfset result.islive = true>
			</cfif>

			<!---  if any elements have LoginRequired set to > 0  then login is required--->
			<cfquery name = "getLogin"   dbtype="query" debug="No">
			select max(loginRequired) as maxLogin
			from getElementRights
			where loginRequired > 0
			</cfquery>

			<cfif getLogin.recordcount is not 0>
				<cfset result.loginRequired = getLogin.maxLogin>
			</cfif>

			<!---  if any elements have LoginRequired set to < 0  then not logged is required--->
			<cfquery name = "getLogin"   dbtype="query" debug="No">
			select 1
			from getElementRights
			where loginRequired < 0
			</cfquery>

			<cfif getLogin.recordcount is not 0>
				<cfset result.notLoggedInRequired = true>
			</cfif>



		<!--- trying to get borderset
		comes from first element up tree which has a borderset set
		--->
		<cfquery name = "getBorderSet"   dbtype="query" debug="No">
		select borderset
		from getElementRights
		where borderset <> ''
		order by generation desc
		</cfquery>

		<cfset result.borderset = getBorderSet.borderset>




		<!--- work out whether this element is in the current tree --->
		<cfif topid is not 0>
			<cfquery name = "checktop"   dbtype="query" debug="No">
			select * from getElementRights
			where elementid = #topid#
			</cfquery>

			<cfif checktop.recordcount is not 0>
				<cfset result.elementintree = true>
			</cfif>
		</cfif>



	 <cfreturn result>
	</cffunction>


<!---
getCountryRightsIDList

gets list of countryids that a person has rights to (based on the recordTask)
Doesn't worry about what level of permission - any is enough

 --->

	<cffunction access="public" name="getCountryRightsIDList" returnType="string">
			<cfargument name="personid" type="numeric" required="true">

			<cfreturn getCountryRightsIDListForMultiplePeople(personid)>

	</cffunction>


	<cffunction access="public" name="getCountryRightsIDListForMultiplePeople" returnType="string">
			<cfargument name="personidlist" type="string" required="true">
			<cfset var getCountries = "">

			<!--- WAB 2012-07-03 altered to use new vCountryRights view --->
			<CFQUERY NAME="getCountries" DATASOURCE="#application.SiteDataSource#">
			SELECT distinct r.CountryID
			FROM vCountryRights AS r
			WHERE r.PersonID  in ( <cf_queryparam value="#personidList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			AND r.Permission > 0
			</CFQUERY>

			<cfreturn valuelist(getCountries.countryid)>

	</cffunction>


	<!--- returns the rights from the recordrights table for a given person and entity
		if level is passed then returns a boolean, otherwise returns query with all levels

	--->
	<cffunction name="getRecordRights">
		<cfargument name="entity" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="personid" type="numeric" required="true">
		<cfargument name="level" type="string" default="">

		<cfset var GetResults = '' />

		<!--- WAB 2012-07-03 altered to use new vRecordRights view --->
		<CFQUERY NAME="GetResults" DATASOURCE="#application.SiteDataSource#">
		SELECT
			level1,level2,level3,level4,level5, permission
		from
			vRecordRights rr
		where
				rr.entity =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and rr.recordid = #entityID#
			and rr.personid = #personid#
		</CFQUERY>

		<cfif level is not "">
			<cfif getResults["level#level#"][1] is 1>
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>
		</cfif>

		<cfreturn getResults>

	</cffunction>


	<cffunction name="getCountrySpecificRights">
		<cfargument name="securitytask" type="string" required="true">
		<cfargument name="personid" type="numeric" required="true">
		<cfargument name="countryid" type="numeric" >

			<cfset var findCountries = "">

			<!--- WAB 2012-07-03 altered to use new vRights view --->
			<CFQUERY NAME="findCountries" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- 2009/09/23 WAB LID 2663, not actually used in the end.  added caching for a few seconds - ie a single request --->
				SELECT r.CountryID,
						convert(varchar(3),r.countryid) + '#application.delim1#' +  c.countryDescription as CountryIDandName,
						Level4,
						Level3,
						Level2,
						Level1,
						Level3 AS AddOkay,
						Level2 AS UpdateOkay,
						Level2 AS Edit,
						Level1 AS [view]

				FROM vRights r
					inner join Country as c on c.countryid = r.countryid
					WHERE r.shortname = <cf_queryparam value="#securitytask#" CFSQLTYPE="CF_SQL_VARCHAR" >
					AND r.PersonID = #personid#
					<CFIF structKeyExists(arguments,"countryid") >
						AND r.CountryID = #CountryID#
					</CFIF>
				ORDER BY CountryDescription
			<!--- 	HAVING Sum((r.Permission\1) MOD 2) > 0	<!--- at least read permissions ---> --->
			</CFQUERY>

		<cfreturn findCountries>

	</cffunction>
	<cffunction access="public" name="GetCountryApproversForUsergroup" hint="This will get all the approvers for a usergroup for a certain country">
		<cfargument name="UserGroupName" type="string" required="Yes"> <!--- pass in a user group or name or ID --->
		<cfargument name="CountryID" type="numeric" required="Yes">

		<cfset var FindPeople = '' />

		<!--- WAB 2012-07-03 altered to use new vCountryRights view --->
		<CFQUERY NAME="FindPeople" DATASOURCE="#application.SiteDataSource#">
			SELECT DISTINCT r.PersonID
				FROM	vCountryRights r
				WHERE     CountryID IN (#arguments.countryid#)
						and r.personid in (
							select rg.personid from rightsgroup rg with (nolock) INNER JOIN
							UserGroup ug with (nolock) ON rg.UserGroupID = ug.UserGroupID where ug.name =  <cf_queryparam value="#arguments.userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >
							)
		</CFQUERY>

		<cfreturn FindPeople>
	</cffunction>


	<!--- WAB 2012-07-03 P-SMA001 altered to take account of new rights filtering code --->
	<cffunction name="getPOLRights">
		<cfargument name="entityStruct" type="struct" >
		<cfargument name="entityTypeID" type="string" >
		<cfargument name="entityID" >
		<cfargument name="countryID" type="numeric" default = 0>
		<cfargument name="personid" type="numeric" default = "#request.relayCurrentUser.PersonID#">
		<cfargument name="debug" type="boolean" default = false>

		<cfset var entitydetails = "">
		<cfset var countryRights = "">
		<cfset var theCountryID = arguments.countryID>
		<cfset var entityType = "">
		<cfset var col = "">
		<cfset var taskRight = "">

		<!--- 2012/08/21 NJH CRM430214 defence against entityTypeID not in arguments but in arguments.entityStruct   --->
		<cfif not structKeyExists(arguments,"entityTypeID") and structKeyExists(arguments.entityStruct,"entityTypeID")>
			<cfset arguments.entityTypeID = arguments.entityStruct.entityTypeID>
		</cfif>
		<cfset entityType = application.com.relayEntity.getEntityType(entityTypeID)>
		<!---
		Get details of this entity
		 --->
		<cfif structKeyExists(arguments,"entityStruct")>
			<cfset entitydetails = entityStruct>
			<cfset arguments.entityTypeid = entitydetails.entityTypeid >
			<cfset arguments.entityid = entitydetails.entityid >
		<cfelse>
			<cfif not isNumeric(arguments.entityTypeID)>
				<cfset arguments.entityTypeID=application.entityTypeID[arguments.entityTypeID]>
			</cfif>
			<cfset entitydetails = application.com.relayplo.getEntityStructure(entityTypeID=entityTypeID,entityID = entityID)>
		</cfif>

		 <!---
		 Get recordRights for that country
		  --->
		<!--- NJH 2011/06/15 - if adding a new person, then just use the countryID passed through as the entityDetails countryIds will be blank --->
		<cfif entityID eq 0 OR entitydetails.countryids[application.entityType[entityTypeID].tablename] eq "">
			<cfset countryRights = getCountrySpecificRights	(securityTask = "recordTask", countryid = countryID, personid = request.relaycurrentuser.personid)>
		<cfelse>
			<!--- WAB 2012-07-05 P-SMA001 Custom Rights Filtering, can no longer just use getCountrySpecificRights() function, have to call getRightsFilterQuerySnippet() and then link to the entity table  --->
			<!---2012/08/20 GCC CRM430214  corrected 'as updateOK' to 'as updateOKay' --->
			<!---2012/09/19 WAB updateOkay is actually Level2, and need addOkay as well --->
			<cfset var filter = getRightsFilterQuerySnippet(entityType=entityType.tablename,alias="x")>
			<cfquery name="countryRights" datasource = "#application.sitedatasource#">
			select #filter.rightsTableAlias#.* , #filter.rightsTableAlias#.level1 as [view], #filter.rightsTableAlias#.level2 as edit, #filter.rightsTableAlias#.level2 as updateOkay, #filter.rightsTableAlias#.level3 as addOkay
			from #entityType.tablename# x
				#filter.join#
			where x.#entityType.uniqueKey# = #entityID#
			</cfquery>
		</cfif>


		<!--- If no rights at all we get a zero row query,
			this function is designed to always return a single row even if all the values are 0
			so we add in a row with values set to zero
			 --->
		<cfif countryRights.recordcount is 0>
			<cfset queryaddrow(countryRights)>
			<cfloop index="col" list=#countryrights.columnlist#>
				<cfset querysetcell(countryRights,col,0) >
			</cfloop>
		</cfif>


			<!--- check if record is in user's own organisation,
			if so we need to do some additional checks' --->

		<cfif entityDetails.id.organisation is request.relaycurrentuser.organisationid>

			<cfset var getOwnRecordTaskRights = application.com.login.checkInternalPermissions('ownRecordTask')>

			<cfif countryRights.recordcount is 0 or not countryRights.view or not countryRights.edit>
				<!--- doesn't have rights by dint of country, so check for own record rights --->
				<cfset OKToView = false>

				<cfif entityDetails.id[entityTypeID] is request.relaycurrentuser["#application.entityType[entityTypeID].uniquekey#"]>
					<!--- either own person/location/organisation record--->
					<cfif debug><cfoutput>own #application.entityType[entityTypeID].tablename# record</cfoutput></cfif>
					<cfset var OKToView = true>

				<cfelseif entityTypeID is 0 and (( getOwnRecordTaskRights.ownLocationPeople and entityDetails.id.location is request.relaycurrentuser.locationid ))>
					<!--- hasrights to whole location and entity is in this location--->
					<cfif debug><cfoutput>person at own location with ownlocationpeople rights</cfoutput>		</cfif>
					<cfset OKToView = true>

				<cfelseif (entityTypeID is 0 or entityTypeID is 1)and (( getOwnRecordTaskRights.ownCountryPeople and entityDetails.countryid is request.relaycurrentuser.countryid ))>
					<!--- hasrights to whole location and entity is in this location--->
					<cfif debug><cfoutput>#application.entityType[entityTypeID].tablename# in own country with owncountrypeople rights</cfoutput>					</cfif>
					<cfset OKToView = true>

				<cfelseif ( getOwnRecordTaskRights.ownOrganisationPeople )>
					<!--- hasrights to whole org--->
					<cfif debug><cfoutput>#application.entityType[entityTypeID].tablename# in own org  ownOrganisationPeople rights</cfoutput>		</cfif>
					<cfset OKToView = true>
				</cfif>

				<cfif oktoview>
					<cfset countryRights.view = 1>
					<cfset countryRights.edit = 1>
					<cfset countryRights.updateokay = 1>
					<cfset countryRights.level1 = 1>
					<cfset countryRights.level2 = 1>
				</cfif>

			<cfelse>
				<!---
					This is someone who has rights to the whole country
					Therefore they must have
						ownCountryPeople
						ownLocationPeople
						(actually this assumes that their country rights are for their own country - ?unlikely? to have rights to another country without rights to their own country)
					However in theory they might not have rights to locations in other countries [ownOrganisationPeople], so we should get that from 	getOwnRecordTaskRights
					Therefore update getOwnRecordTaskRights.ownCountryPeople and getOwnRecordTaskRights.ownLocationPeople to be definitely true
				--->

				<cfset getOwnRecordTaskRights.ownCountryPeople = 1>
				<cfset getOwnRecordTaskRights.ownLocationPeople = 1>
			</cfif>

			<!--- P-PAN002 2010/04/21 NJH/WAB - add rights from the getOwnRecordTaskRights structure --->
			<cfloop collection = "#getOwnRecordTaskRights#" item="taskRight">
				<cfset var rightsArray = [getOwnRecordTaskRights[taskRight]]>
				<cfif not structKeyExists(countryRights,taskRight)>
					<cfset QueryAddColumn(countryRights, taskRight, "bit",rightsArray)>
				</cfif>
			</cfloop>

		</cfif>

		<!--- P-PAN002 2010/04/21 NJH/WAB - provide temporary rights to a person for a given organisation --->
		<cfif
				structKeyexists (session,"rightsOverride")
			and structKeyexists (session.rightsOverride, "organisation")
			and structKeyexists (session.rightsOverride.organisation,entityDetails.id.organisation)
		>
			<cfset countryRights.view = 1>
			<cfset countryRights.edit = 1>
			<cfset countryRights.updateokay = 1>
			<cfset countryRights.level1 = 1>
			<cfset countryRights.level2 = 1>
			<cfset var tempRightsArray = [1]>
			<!--- if it doesn't exist, create it; otherwise just overwrite it --->
			<cfloop list="ownOrganisationPeople,ownCountryPeople,ownLocationPeople" index="taskRight">
				<cfif not structKeyExists(countryRights,taskRight)>
					<cfset QueryAddColumn(countryRights, taskRight, "bit",tempRightsArray)>
				<cfelse>
					<cfset QuerySetCell(countryRights,taskRight,tempRightsArray[1])>
				</cfif>
			</cfloop>
		</cfif>



		<cfreturn countryRights>

	</cffunction>

	<cffunction name="giveTemporaryRights">
		<cfargument name="entityType" type="string">
		<cfargument name="entityID" type="string">

		<cfparam name="session.rightsOverride" default = "#structNew()#">
		<cfparam name="session.rightsOverride[entityType]" default = "#structNew()#">

		<cfset session.rightsOverride[entityType][entityID] = true>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	      Set Country Scope
	      2009-02-18 NYB
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="setCountryScope">
		<cfargument name="entity" type="string">
		<cfargument name="entityID" type="numeric">
		<cfargument name="entityTypeID" type="numeric">
		<cfargument name="CountryID" type="numeric">
		<cfargument name="Permission" type="numeric" default="1">
		<cfargument name="Update" type="boolean" default="true">

		<cfset var insertCountryScope = '' />
		<CFQUERY NAME="insertCountryScope" DATASOURCE="#application.SiteDataSource#">
			if not exists (select 1 from countryScope where entity =  <cf_queryparam value="#entity#" CFSQLTYPE="cf_sql_varchar" >  and entityID=#entityID# and entityTypeID=#entityTypeID# and CountryID=#CountryID#)
				begin
					<cfif Update>
						if not exists (select 1 from countryScope where entity =  <cf_queryparam value="#entity#" CFSQLTYPE="cf_sql_varchar" >  and entityID=#entityID# and entityTypeID=#entityTypeID# and CountryID=#CountryID# and permission=#permission#)
							update countryScope set permission=#permission# where entity =  <cf_queryparam value="#entity#" CFSQLTYPE="cf_sql_varchar" >  and entityID=#entityID# and entityTypeID=#entityTypeID# and CountryID=#CountryID#
						else
					</cfif>
					insert into countryScope (entity,entityID,entityTypeID,CountryID,permission) values (<cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#entityID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#entityTypeID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#CountryID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#permission#" CFSQLTYPE="CF_SQL_Integer" >)
				end
		</CFQUERY>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	      Set Record Rights
	      2009-02-18 NYB
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="setRecordRights">
		<cfargument name="entity" type="string" required="true">
		<cfargument name="UserGroupID" type="numeric" required="true">
		<cfargument name="recordid" type="numeric" required="true">
		<cfargument name="Level" type="numeric" default="1">

		<cfset var insertRecordRights = '' />
		<CFQUERY NAME="insertRecordRights" DATASOURCE="#application.SiteDataSource#">
			declare @level int
			select @level = #level#

			if not exists (select 1 from recordRights where entity =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >  and UserGroupID=#UserGroupID# and recordid=#recordid#)
				insert into recordRights (UserGroupID,entity,recordid,permission)
				values (#UserGroupID#,'#entity#',#recordid#,POWER (2,(@level-1)))
			else
				begin
					update recordRights
					set permission=permission | (POWER (2,(@level-1)))
					where entity =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and UserGroupID=#UserGroupID#
					and recordid=#recordid#
				end
		</CFQUERY>
	</cffunction>

	<cffunction name="removeRecordRights">
		<cfargument name="entity" type="string">
		<cfargument name="UserGroupID" default="">
		<cfargument name="recordid" type="numeric">
		<cfargument name="Level" type="numeric" default="1">

		<cfset var removeRecordRights = "">
		<CFQUERY NAME="removeRecordRights" DATASOURCE="#application.SiteDataSource#">
			declare @level int
			select @level = #level#

			update recordRights
			set permission = permission & ~ (POWER (2,(@level-1)))
			where entity =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfif isNumeric(UserGroupID)>and UserGroupID =  <cf_queryparam value="#UserGroupID#" CFSQLTYPE="CF_SQL_Integer" > </cfif>
			and recordid=#recordid#

			delete from recordRights
			where permission = 0
			and entity =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfif isNumeric(UserGroupID)>and UserGroupID =  <cf_queryparam value="#UserGroupID#" CFSQLTYPE="CF_SQL_Integer" > </cfif>
			and recordid=#recordid#
		</CFQUERY>
	</cffunction>

	<!--- NJH 2009/07/14 P-FNL069 - function to determine whether a user has access to a particular entity and what that access is --->
	<cffunction name="doesUserHaveRightsForEntity" access="public" returnType="boolean">
		<cfargument name="entityType">
		<cfargument name="entityID" >
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="permission">

		<cfscript>
			var permissionStruct = {1="view",2="edit"};
			var rightsStruct = getUserRightsForEntity(entityID=arguments.entityID,entityType=arguments.entityType,personID=arguments.personID);
			var thisPermission = arguments.permission;

			if (isNumeric(arguments.permission)){
				thisPermission = permissionStruct[arguments.permission];
			}
		</cfscript>

		<cfreturn rightsStruct[thisPermission]>

	</cffunction>

	<!--- NJH 2009/07/15 - P-FNL069 function to return the rights a user has to a particular entity --->
	<cffunction name="getUserRightsForEntity" access="public" returntype="struct">
		<cfargument name="entityType" required="true">
		<cfargument name="entityID" required="true">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfscript>
			var thisEntityType = arguments.entityType;
			var result = structNew();
			var entityRights = structNew();

			result.edit = true;
			result.view = true;
		</cfscript>

		<cfif isNumeric("arguments.entityType")>
			<cfset thisEntityType = application.entityType[arguments.entityType]>
		</cfif>

		<!--- NJH 2016/06/16 check that entityID is not 0. Assume that 0 is a new record so the person can create it. --->
		<cfif arguments.entityID neq 0>
		<!--- add call to entity specific function here --->
		<cfswitch expression="#thisEntityType#">
			<cfcase value="trngPersonCertification,trngUserModuleProgress,entitySpecialisation,quizTaken">
				<cfset entityRights = application.com.relayElearning.getUserRightsForElearning(argumentCollection=arguments)>
			</cfcase>
			<cfcase value="cashbackClaim">
				<cfset entityRights = application.com.relayCashback.getUserRightsForCashbackClaim(personID=arguments.personID,cashbackClaimID=arguments.entityID)>
			</cfcase>
			<cfcase value="rwTransaction">
				<cfset entityRights = application.com.relayIncentive.getUserRightsForRWTransaction(personID=arguments.personID,rwTransactionID=arguments.entityID)>
			</cfcase>
			<cfcase value="cjm">
				<cfset entityRights = application.com.cjm.getUserRightsForcjm(personID=arguments.personID,CJMID=arguments.entityID)>
			</cfcase>
			<!--- NJH 2010/04/13 P-PAN002 - added opportunity rights --->
			<cfcase value="opportunity">
				<cfset entityRights = application.com.opportunity.getUserRightsForOpportunity(personID=arguments.personID,opportunityID=arguments.entityID)>
			</cfcase>
			<cfcase value="person,location,organisation">
				<cfset arguments.entityTypeID = arguments.entityType>
				<cfset entityRights = application.com.structureFunctions.queryRowToStruct(query=getPOLRights(argumentCollection=arguments),row=1)>
			</cfcase>
				<!--- NJH 2016/06/16 JIRA PROD2016-1273 - add function to check lead rights. --->
				<cfcase value="lead">
					<cfset entityRights = application.com.relayLeads.getUserRightsForLead(personID=arguments.personID,leadID=arguments.entityID)>
				</cfcase>
		</cfswitch>

		<cfset structAppend(result,entityRights)>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getUserRightsToEntityField" access="public" returnType="struct">
		<cfargument name="entityType" required="true">
		<cfargument name="entityID" required="true">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityRights" type="struct" required="true">
		<cfargument name="field" type="string" required="true">

		<cfscript>
			var thisEntityType = arguments.entityType;
			//var rights = {edit=true,view=true};
			var fieldRights = getEntityFieldRights(entityTypeId=arguments.entityType,personId=arguments.personID,field=arguments.field);
			var rights = {view=true,edit=true};
			var isPersonInternalUser = false;
		</cfscript>

		<!--- see if the field actually exists... for example, we pass in lastUpdatedByPersonName in the screens code so that we can show the name
			of the person who last updated the screen.. but this 'field' actually doesn't exist in the person table --->
		<cfif structKeyExists(fieldRights,field)>
			<cfset rights = fieldRights[field]>
		</cfif>

		<!--- if we can't both edit and view, then skip the checks --->
		<cfif arguments.entityRights.edit or arguments.entityRights.view>
			<cfif isNumeric("arguments.entityType")>
				<cfset thisEntityType = application.entityType[arguments.entityType]>
			</cfif>

			<cfswitch expression="#thisEntityType#">
				<cfcase value="person">
					<cfswitch expression="#arguments.field#">
						<cfcase value="email">
							<cfif request.relayCurrentUser.isInternal and isNumeric(arguments.entityID)> <!--- don't test for new records --->
								<cfset isPersonInternalUser = application.com.login.isPersonAValidRelayInternalUser(arguments.entityID)>
								<cfif (arguments.personID neq arguments.entityID) and (isPersonInternalUser and not application.com.login.checkInternalPermissions(securityTask="securityAdminTask",securityLevel="level2"))>
									<cfset rights.edit=false>
								</cfif>
							</cfif>
						</cfcase>
					</cfswitch>
				</cfcase>
			</cfswitch>
		</cfif>

		<cfset rights.edit = arguments.entityRights.edit and rights.edit>
		<cfset rights.view = arguments.entityRights.view and rights.view>

		<cfreturn rights>

	</cffunction>


	<cffunction name="doesUserHaveRightsToEntityField" access="public" returnType="boolean">
		<cfargument name="entityType" required="true">
		<cfargument name="entityID" required="true">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="permission" required="true">
		<cfargument name="field" type="string" required="true">
		<cfargument name="entityRights" type="struct" required="true">

		<cfscript>
			var permissionStruct = {1="view",2="edit"};
			var rightsStruct = getUserRightsToEntityField(argumentCollection=arguments);
			var thisPermission = arguments.permission;

			if (isNumeric(arguments.permission)){
				thisPermission = permissionStruct[arguments.permission];
			}
		</cfscript>

		<cfreturn rightsStruct[thisPermission]>
	</cffunction>

	<!--- WAB Note 2012-07-04, no sign of this function being used --->
	<cffunction name="getFlagGroupRightsByCountry" access="public" returnType="query">
		<cfargument name="countryID" type="numeric" required="false">
		<cfargument name="flagGroupId" required="true">

		<cfscript>
			var qryGetFlagGroupRightsByCountry = "";
		</cfscript>

		<cfquery name="qryGetFlagGroupRightsByCountry" datasource="#application.siteDataSource#">
			select distinct rg.personid, r.countryid from flagGroup fg
				inner join flaggrouprights fgr on fg.flaggroupid = fgr.flaggroupid
				inner join rightsGroup rg on rg.usergroupid = fgr.userid
				inner join rightsGroup rg2 on rg2.personid = rg.personid
				inner join rights r on rg2.usergroupid = r.usergroupid and securityTypeID = (select securityTypeid from securityType where shortname= 'recordtask') and Convert(bit,r.permission & 2) = 1
			where
				<cfif isNumeric(arguments.flagGroupId)>
					fg.flagGroupId =  <cf_queryparam value="#arguments.flagGroupId#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					fg.flagGroupTextId =  <cf_queryparam value="#arguments.flagGroupId#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
					and (fgr.edit = 1 or fg.edit = 1)
				<cfif structKeyExists(arguments,"countryID")>
					and r.countryID = #arguments.countryID#
				</cfif>
		</cfquery>
	</cffunction>

	<!--- START: 2011/10/25 NYB LHID7963 added functions: --->
	<cffunction name="getDeleteRights" access="public" returnType="boolean">
		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">
		<cfargument name="entity" required="true" default="person"><!--- person, location or organisation --->
		<cfscript>
			var result = "false";
			var pricebookSettings = application.com.settings.getSetting("plo.mergeAndDelete.manualDelete.#entity#");
			var hasCorrectSecurity = application.com.login.checkInternalPermissions ("deleteTask", "level2");
		</cfscript>
		<cfif pricebookSettings and hasCorrectSecurity>
			<cfset result = "true">
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="getPersonDeleteRights" access="public" returnType="boolean">
		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">
		<cfscript>
			var result = getDeleteRights(personid=arguments.personid,entity="person");
		</cfscript>
		<cfreturn result>
	</cffunction>

	<cffunction name="getLocationDeleteRights" access="public" returnType="boolean">
		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">
		<cfscript>
			var result = getDeleteRights(personid=arguments.personid,entity="location");
		</cfscript>
		<cfreturn result>
	</cffunction>

	<cffunction name="getOrganisationDeleteRights" access="public" returnType="boolean">
		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">
		<cfscript>
			var result = getDeleteRights(personid=arguments.personid,entity="organisation");
		</cfscript>
		<cfreturn result>
	</cffunction>
	<!--- END: 2011/10/25 NYB LHID7963 --->


	<!--- 2012-02-01 NYB added function because equivalent - in login.cfc - is based on relaycurrentuser and a session variable
			I wanted a function I could use for any user
	 --->
	<cffunction access="public" name="checkInternalPermissionsForPerson" hint="Returns true or false for that level">
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="securityTask" type="string" required="yes">
		<cfargument name="securityLevel" type="string" required="yes">
		<cfset var returnVariable = false >
		<cfset var getSecurity = "">

		<!--- WAB 2012-07-05 Altered to make use of new vRights view --->
		<cfquery name="getSecurity" datasource="#application.siteDataSource#">
		select 	isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =1 ),'') as Level1Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =2 ),'') as Level2Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =3 ),'') as Level3Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =4 ),'') as Level4Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =5 ),'') as Level5Name
			from
				vrights
					where shortname = <cf_queryparam value="#arguments.securityTask#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and personid = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER">
					and #arguments.securityLevel# = 1

			<!---
			select st.*,isNULL(p.Level1,0) as Level1,isNULL(p.Level2,0) as Level2,isNULL(p.Level3,0) as Level3,isNULL(p.Level4,0) as Level4,isNULL(p.Level5,0) as Level5,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =1 ),'') as Level1Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =2 ),'') as Level2Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =3 ),'') as Level3Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =4 ),'') as Level4Name,
				isnull((select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =5 ),'') as Level5Name
			from
				securityType st left join (

						SELECT r.SecurityTypeID ,
						Convert(bit,sum(r.permission & 16)) AS Level5,
						Convert(bit,sum(r.permission & 8)) AS Level4,
						Convert(bit,sum(r.permission & 4)) AS Level3,
						Convert(bit,sum(r.permission & 2)) AS Level2,
						Convert(bit,sum(r.permission & 1)) AS Level1
						FROM Rights AS r inner join RightsGroup AS rg
							on   	r.UserGroupID = rg.UserGroupID
							WHERE rg.PersonID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER">
						GROUP BY r.SecurityTypeID
						) as p
				on  p.securitytypeid = st.securitytypeid
				where st.ShortName= <cf_queryparam value="#arguments.securityTask#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and #arguments.securityLevel# = 1
				--->
		</cfquery>

		<cfif getSecurity.recordcount gt 0>
			<cfset returnVariable = true>
		</cfif>

		<cfreturn returnVariable>

	</cffunction>


	<!--- WAB/NJH 2015-01-21 CASE 443542 Problem because this function was cached the same structure was being returned regardless of the setting of returnApiName and various other arguments.
		Implemented a unique cache key
	--->

	<cffunction name="getEntityFieldRights" access="public" returntype="struct">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="haveRightsForMethod" type="string" required="false">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="field" type="string" required="false">
		<cfargument name="returnColumns" type="string" default="type,flagGroupId,flagId,field,[view],edit">
		<cfargument name="returnApiName" type="boolean" default="false">

		<cfset var methodList = "">
		<cfset var getFlagFieldRightsForEntity = "">
		<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityTypeId)>
		<cfset var method = "">
		<cfset var viewAsApiUser = false>
		<cfset var nonEditableAPIFieldnames = "created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson,#entityTypeStruct.uniqueKey#">
		<cfset var result = structnew()>  <!--- 2013-10-17 NYB Case 437263 added --->

		<cfif structKeyExists(arguments,"haveRightsForMethod")>
			<cfset method = arguments.haveRightsForMethod>
		</cfif>
		<cfif method eq "select">
			<cfset method = "view">
		</cfif>

		<cfif not structKeyExists(request,"relayCurrentUser") or (structKeyExists(request,"relayCurrentUser") and structKeyExists(request.relayCurrentUser,"isApiUser") and request.relayCurrentUser.isApiUser)>
			<cfset viewAsApiUser = true>
		</cfif>

		<cfif not structKeyExists(session,"rights")>
			<cfset session.rights = {}>
		</cfif>

		<!--- WAB/NJH 2015-01-21 CASE 443542 Implemented a unique cache key so that different cache used dependent upon the arguments passed in
		--->
		<cfset var cachekey = "#entityTypeStruct.tablename#|#returnApiName#|#viewAsApiUser#|#method#|#personID#|#returnColumns#">
		<cfif structKeyExists(arguments,"field")>
			<cfset cachekey &= "|#arguments.field#">
		</cfif>

		<cfif not structKeyExists(session.rights,cachekey) >

			<!--- for some reason, we have a duplicate field and flagGroup name, so have to limit records returned to 1 if a field is passed through..
				get the field value. --->
			<!--- 2015-03-11	RPW	Altered filter on crm fields to allow them to be edited if Connector is turned off. --->
			<cfscript>
				var connectorActive = application.com.settings.getSetting("partnerPacks.connector");
				var crmFieldClause = "";
				if (connectorActive) {
					crmFieldClause = "or fieldname like 'crm%ID'";
				}
			</cfscript>
			<cfquery name="getFlagFieldRightsForEntity" datasource="#application.siteDataSource#">
				select <cfif structKeyExists(arguments,"field")>top 1</cfif> #returnColumns# from (

					select distinct 'flagGroup' as type,fgr.flagGroupId,null as flagId,case when len(fgr.flagGroupTextID) = 0 then cast(fgr.flagGroupID as varchar) else fgr.flagGroupTextID end as field,[view],edit
					from vFlagGroupRights fgr
						inner join flagType ft on ft.flagTypeID = fgr.flagTypeID
						inner join flag f on f.flagGroupID = fgr.flagGroupId
					where personID = <cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER">
						and entityTypeID = <cf_queryparam value="#entityTypeStruct.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
						and ft.datatable = 'boolean'
						<cfif method neq "">and [#method#]=1</cfif>
						<cfif viewAsApiUser>and len(fgr.flagGroupTextID)> 0 and len(f.flagTextID) > 0 and fgr.flagGroupTextID not like 'delete%'</cfif> <!--- need to filter out flagGroups with no textID for the apiUser or those that have flags without flagTextIDs --->
						<cfif structKeyExists(arguments,"field")>and fgr.flagGroupTextID=<cf_queryparam value="#arguments.field#" cfsqltype="CF_SQL_VARCHAR"></cfif>

					union

					select distinct 'flag' as type,fgr.flagGroupId,f.flagID,case when len(f.flagTextID) = 0 then cast(f.flagID as varchar) else f.flagTextID end as field,[view],edit
					from vFlagGroupRights fgr
						inner join flagType ft on ft.flagTypeID = fgr.flagTypeID
						inner join flag f on f.flagGroupID = fgr.flagGroupId
					where personID = <cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER">
						and entityTypeID = <cf_queryparam value="#entityTypeStruct.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
						and ft.datatable in ('integer','text','date')
						<cfif method neq "">and [#method#]=1</cfif>
						<cfif viewAsApiUser>and len(flagTextID)>0</cfif> <!--- need to filter out flags with no textID for the apiUser --->
						<cfif structKeyExists(arguments,"field")>and flagTextID=<cf_queryparam value="#arguments.field#" cfsqltype="CF_SQL_VARCHAR"></cfif>

					union

					select distinct 'field' as type,null as flagGroupID,null as flagID,
						<cfif viewAsApiUser>
							<cfif arguments.returnApiName>isNull(apiName,fieldname)<cfelse>fieldname</cfif> as field,
							apiActive as [view],
							<!--- 2015-03-11	RPW	Altered filter on crm fields to allow them to be edited if Connector is turned off. --->
							case when fieldname in (<cf_queryparam value="#nonEditableAPIFieldnames#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">) #PreserveSingleQuotes(crmFieldClause)# then 0 else apiActive end as edit
						<cfelse>
							fieldname as field,
							1 as [view],1 as edit
						</cfif>
					from modEntityDef
					where tablename =  <cf_queryparam value="#entityTypeStruct.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and fieldname not in ('Whole Record','AnyFlag')
						<cfif structKeyExists(arguments,"field")>and fieldname=<cf_queryparam value="#arguments.field#" cfsqltype="CF_SQL_VARCHAR"></cfif>
						<cfif viewAsApiUser>
							and apiActive=1
							and fieldname != 'deleted'
							<cfif method neq "">
								and
								<cfif method eq "view">1=1
								<cfelseif method eq "edit">case when fieldname in (<cf_queryparam value="#nonEditableAPIFieldnames#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">) or fieldname like 'crm%ID' then 0 else apiActive end = 1
								<cfelse>1=0</cfif>
							</cfif>
						</cfif>
				) as base
				order by field,type
			</cfquery>

			<cfset session.rights[cacheKey] = application.com.structureFunctions.queryToStruct(query=getFlagFieldRightsForEntity,key="field")>
		</cfif>

		<cfreturn duplicate(session.rights[cacheKey])>
	</cffunction>

	<cffunction name="getEntityFieldsForPersonAndMethod" access="public" returntype="string">
		<cfargument name="haveRightsForMethod" type="string" required="true">
		<cfargument name="entityTypeID" type="string" required="true">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="returnApiName" type="boolean" default="false">

		<cfreturn structKeyList(getEntityFieldRights(argumentCollection=arguments))>
	</cffunction>


	<!---
	2012-06-28
	A combination of two functions written concurrently for two different projects
	by WAB (SMART P-SMA0001) and NJH (API)

	Brings back query snippets to filter queries (initially) by country rights and (in future) by other rights (such as recordRights)

	Can bring back the filter as just a WHERE clause (easier to retrofit to much of our code) or as a JOIN with an optional WHERE clause (might be more efficient).

	Designed to be extended by client versions.  For example for SMART, Account Managers only see POL records for which they are Account Manager.
	When an extension is applied the function returns key customRightsApplied=true and customRightsMessage="Some Message".  Currently this message is displayed within user management to indicate that a custom filter will be applied to that user

	Note that this function must work for any PersonID, so cannot access request.relaycurrentUser

	TBD
	May need aliases to be guaranteed unique



	2016-10-25	WAB	PROD2016-2586 Added isInternal argument rather than using request.relayCurrentUser.isInternal within the code
					This allows code to be called from a housekeeping task where request.relayCurrentUser.isInternal is not defined
	 --->


	<cffunction name="getRightsFilterQuerySnippet" access="public" returnType="struct" output=false>
	    <cfargument name="entityType" type="string" required="true">
	    <cfargument name="Alias" type="string" required="false">
	    <cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personid#">
	    <cfargument name="isInternal" type="numeric" default="#request.relayCurrentUser.isInternal#">
	    <cfargument name="useJoins" type="boolean" default="true">
	    <cfargument name="regionList" type="string" default="" required="false">
        <cfargument name="countryID" type="numeric" required="false"> <!---Defaulted to request.relaycurrentuser.countryID for portal users, not used otherwise --->

		<!--- PPB
		I have added the optional parameter regionList to allow filtering based on regions

		I send in the list rather than a boolean
		(1) so you can send in request.relayCurrentUser.regionList OR request.relayCurrentUser.regionListSomeRights OR a custom list
		(2) to save working out the list in here if the person is not the current user

		TODO: THIS HAS NOT BEEN APPLIED TO THE JOIN SYNTAX - awaiting suggestion from Will because he may wish to extend vCountryRights or use some other approach instead
		--->

		<cfset var result = {join="",whereClause="",customRightsApplied=false,customRightsMessage=""}>
		<cfset var join = "">
		<cfset var whereClause = "">
		<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var parentTableName = "">
		<cfset var filterTableName = "">

		<!--- 2012-07-23 PPB SMA001 for entityType=opportunity we need to toggle whether to apply the country filter based on a setting;
		we don't want to avoid this whole function if applyCountryFilter=false because it may be used for non-country stuff later
		TODO: test if entityType="oppProducts"
		TODO: check with WAB
		NJH 2016/05/04 - BF-675 if not internal user, set applyCountryFilter to false
		NJH 2016/05/16 - BF-700 - reverted change from 675 - as pol rights expects country rights to be set, even for portal user. So, just make the change when dealing with opportunities
		NJH 2016/11/28	JIRA PROD2016-2642 - changed again. Set country rights to false for portal user unless dealing with POL data.
		 --->
		<cfset var applyCountryFilter = arguments.isInternal or listFindNoCase("person,location,organisation",entityTypeStruct.tablename)?true:false>
		<cfif (entityTypeStruct.tablename eq "opportunity" OR entityTypeStruct.tablename eq "opportunityProduct")>
			<cfset applyCountryFilter = arguments.isInternal? application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords"):false>
		</cfif>

		<cfif not structKeyExists(arguments,"Alias")>
			<cfset arguments.Alias = entityTypeStruct.tablename>
		</cfif>

		<!--- this table has a countryID column, so filter it on this --->
		<cfif entityTypeStruct.hasCountryIDColumn>
			<cfset filterTableName = Alias>
		<cfelse>
			<!--- ideally we want to go to the parent here, and see if the parent has a countryID column (oppProducst,person, etc) --->
			<cfswitch expression="#entityTypeStruct.tablename#">
				<cfcase value="opportunityProduct">
					<cfset parentTableName = "opportunity">
				</cfcase>
				<cfcase value="person">
					<cfset parentTableName = "location">
				</cfcase>
				<cfcase value="pricebookEntry">
					<cfset parentTableName = "pricebook">
				</cfcase>
			</cfswitch>

			<!--- Need to add a join to the table which contains the country --->
			<cfif parentTableName is not "">
				<cfif useJoins>
					<cfset result.join = "inner join #parentTableName# with (noLock) on #parentTableName#.#parentTableName#ID = #Alias#.#parentTableName#ID">
					<cfset filterTableName = parentTableName>
				<cfelse>
					<cfset result.whereClause = "AND #Alias#.#parentTableName#ID in (select #parentTableName#ID from #parentTableName# with (noLock) #getRightsFilterQuerySnippet(entityType =parentTableName,Alias = parentTableName,personid = personid,useJoins = true).join#)">
					<cfset filterTableName = ""> <!--- We have now created the whole WHERE filter, so set filterTableName to null --->
				</cfif>
			</cfif>
		</cfif>

		<!--- NJH 2016/04/26 - Sugar 448757 - part of performance enhancements. Build up an opportunity rights snippet for portal user to be used on queries, so that we can join to get rights rather than use a big in statement. Copied rights code from opportunity.getOpportunitiesAPartnerHasRightsTo
			This maybe needs to go into opportunity.cfc if we start getting specific about certain entities (as we don't want a long function dealing with all entities), but left in here for now.
		--->
		<cfif not arguments.isInternal>
			<cfif (arguments.entitytype eq "opportunity" or parentTableName eq "opportunity")>
			<!--- arguments.organisationID is an end customer organisation --->

				<!--- NJH 2016/07/19 JIRA PROD2016-351 and 599 - deal with multiple primary contacts and also primary contacts at location level. Checking that the flag is ok and that it's active so that we can transition and it won't cause any issues --->
				<!---<cfset var primaryContactPersonIds = "">
				<cfset var flagEntityID = "">

				<cfloop list="organisation,location" index="flagEntityType">
					<cfset var flagEntityID = application.com.relayEntity.getEntityType(entityTypeID=flagEntityType).uniqueKey>
					<cfif structKeyExists(arguments,flagEntityID)>
						<cfset primaryContactPersonIds = listAppend(primaryContactPersonIds,application.com.relayPLO.getPrimaryContactsForEntity(entityID=arguments[flagEntityID],entityType=flagEntityType))>
					</cfif>
				</cfloop>--->
				<cfset var isPrimaryContact = application.com.relayPLO.isPersonAPrimaryContact()>
				<cfset var accountTypeFilter = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"locationID":"organisationID">

				<!--- NJH 2016/06/02 - 2016-1-3 Hotfix. Check that reseller OrgID is defined --->
	            <!--- DAN 2016/09/24 - PROD2016-2388/Case 451338 --->
	            <cfsavecontent variable="local.oppJoins">
					<cfoutput>

	                    <!--- <cfif structKeyExists(arguments,"reseller OrgID") and arguments.reseller OrgID neq "">
	                        left join location l1 on l1.locationID=#alias#.partnerLocationID and l1.organisationid = <cf_queryparam value="#arguments.reseller OrgID#" CFSQLTYPE="CF_SQL_INTEGER">
	                        left join location l2 on l2.locationID=#alias#.distiLocationID and l2.organisationid = <cf_queryparam value="#arguments.reseller OrgID#" CFSQLTYPE="CF_SQL_INTEGER">
	                    <cfelse> --->
	                        left join location partnerLoc on partnerLoc.locationID=#alias#.partnerLocationID
	                        <cfif isPrimaryContact>
		                         and partnerLoc.#accountTypeFilter# = #request.relayCurrentUser[accountTypeFilter]#
							</cfif>
	                        left join location distiLoc on distiLoc.locationID=#alias#.distiLocationID
	                   <!---  </cfif> --->
						left outer join (vbooleanFlagData bfd_PortalLeadManager with (noLock) inner join person p1 with (noLock) on p1.personID = bfd_PortalLeadManager.entityID and bfd_PortalLeadManager.flagTextId = 'PortalLeadManager') on bfd_PortalLeadManager.entityID = #request.relayCurrentUser.personID#
						left outer join (integerFlagData int_PartnerOrderingManager with (noLock) inner join flag f_PartnerOrderingManager with (noLock) on f_PartnerOrderingManager.flagId = int_PartnerOrderingManager.flagId and f_PartnerOrderingManager.flagTextId = 'PartnerOrderingManager' inner join person p2 with (noLock) on p2.personID = int_PartnerOrderingManager.data) on int_PartnerOrderingManager.data = #request.relayCurrentUser.personID#
					</cfoutput>
				</cfsavecontent>
				<cfset result.join = result.join & local.oppJoins>

				<cfsavecontent variable="local.oppWhereClause">
					<cfoutput>
	                    and (
	                        (partnerLoc.locationID is not null
	                            <cfif not isPrimaryContact>and (#alias#.partnerSalesPersonID = #request.relaycurrentuser.personID# or #alias#.partnerSalesManagerPersonID = #request.relaycurrentuser.personID#)</cfif>
	                        ) OR
	                        (distiLoc.locationID is not null
	                            and (#alias#.distiSalesPersonID = #request.relaycurrentuser.personID#)
	                        )
	                        <!---<cfif primaryContactPersonIds neq "">or #request.relayCurrentUser.personID# in (<cf_queryparam value="#primaryContactPersonIds#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)</cfif>--->
							OR (bfd_PortalLeadManager.entityId is not null and p1.#accountTypeFilter# = partnerLoc.#accountTypeFilter#)
							OR (int_PartnerOrderingManager.data is not null and p2.#accountTypeFilter# = partnerLoc.#accountTypeFilter#)
							)
					</cfoutput>
				</cfsavecontent>
				<cfset result.whereClause = result.whereClause & local.oppWhereClause>

			<!--- NJH 2016/06/16 JIRA PROD2016-1273 - rights for leads. Only show partners their own leads --->
			<cfelseif arguments.entityType eq "lead">
				<!--- NJH 2016/07/28 JIRA PROD2016-599 if user is primary contact, then show all unassigned leads for their location --->
				<cfset var isPrimaryContact = application.com.relayPLO.isPersonAPrimaryContact()>
                <!--- PROD2016-2790 - fix for access rights for primary contacts and lead partner managers --->
                <cfset var isLeadPartnerManager = application.com.login.isPersonInOneOrMoreUserGroups(personID=request.relayCurrentUser.personID, UserGroups="Lead Partner Manager")>

				<cfset result.whereClause = result.whereClause & " and (partnerSalesPersonID=#request.relayCurrentUser.personID#">
				<cfif isPrimaryContact OR isLeadPartnerManager>
                    <cfset result.join = result.join & " left join location on location.locationID=#alias#.partnerLocationID ">
                    <cfset result.join = result.join & " and location." & (application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"locationID = #request.relayCurrentUser.locationID#":"organisationID = #request.relayCurrentUser.organisationID#")>
                    <cfset result.whereClause = result.whereClause & " or location.locationID is not null">
				</cfif>
				<cfset result.whereClause = result.whereClause & ")">
			</cfif>
		</cfif>

		<cfif filterTableName neq "">
			<cfif applyCountryFilter>
				<cfif useJoins>

					<cfset result.rightsTableAlias = "vcr_#filterTableName#_#arguments.personID#">
					<cfset result.join = result.join & " INNER JOIN vCountryAndCountryGroupRights #result.rightsTableAlias# with (noLock) ON #result.rightsTableAlias#.personid = #arguments.personID# AND #result.rightsTableAlias#.countryID = #filterTableName#.countryid "	>

				<cfelse>
					<cfset result.whereClause = "AND (">
                    <cfif arguments.isInternal>
					    <cfset result.whereClause = result.whereClause & "#Alias#.countryid IN (select countryid FROM vCountryRights vcr with (noLock) WHERE vcr.personid = #arguments.personID#)">
                    <cfelse>
						<cfparam name="arguments.countryID" default="#request.relaycurrentuser.countryID#">
                        <cfset result.whereClause = result.whereClause & "#Alias#.countryid = #arguments.countryID#"> <!--- for portal user check his country instead of countryRights--->
                    </cfif>
					<cfif arguments.regionList neq "">
						<cfset result.whereClause = result.whereClause & " OR #Alias#.countryid IN (#arguments.regionList#)">
					</cfif>
					<cfset result.whereClause = result.whereClause & ")">
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getRightsFilterWhereClause" access="public" returnType="struct" output=false>
		<cfset arguments.useJoins = false>

		<cfreturn getRightsFilterQuerySnippet(argumentCollection = arguments)>
	</cffunction>


	<cffunction name="getCountryRecordRightsFilterQuerySnippet" access="public" returnType="struct" output=false>
	    <cfargument name="entityType" type="string" required="true">
	    <cfargument name="Alias" type="string" required="false">
	    <cfargument name="personID" type="string" default="#request.relayCurrentUser.personid#" hint="Can accept a column as well as an actual personID">
	    <cfargument name="noRightsEqualsNoRightsApplied" type="boolean" default="true" hint="If no record rights exist, then assume that everyone has rights rather than no one has rights">
	    <cfargument name="rightsType" default="record" hint="Country or Record">
	    <cfargument name="countryID" type="string" required="false" hint="Can accept a column as well as an actual countryID">

		<cfset var result = {join="",whereClause="",rightsTableAlias=""}>
		<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>

		<cfif not structKeyExists(arguments,"Alias")>
			<cfset arguments.Alias = entityTypeStruct.tablename>
		</cfif>

		<cfif arguments.rightsType eq "record">
			<cfset var Level = 1>
			<cfset result.rightsTableAlias = "vrr_#arguments.alias#_#replace(arguments.personID,'.','')#">
			<cfset result.join = result.join & " left join  dbo.getRecordRightsTableByPerson (#application.entityTypeID[arguments.entityType]#, #arguments.personID#) as #result.rightsTableAlias# on #arguments.alias#.hasRecordRightsBitMask & power (2,#Level#-1) = 1 and #result.rightsTableAlias#.recordID = #arguments.alias#.#entityTypeStruct.uniqueKey# ">
			<cfset result.whereClause = " and (#arguments.alias#.hasRecordRightsBitMask & power (2,#Level#-1) = #arguments.noRightsEqualsNoRightsApplied?0:1# or #result.rightsTableAlias#.Level#level#  = 1)">
		<cfelse>
			<!--- if country isn't passed in, then get the country from the person --->
			<cfset result.rightsTableAlias = "vcs_#arguments.alias#_#structKeyExists(arguments,'countryID')?replace(arguments.countryID,'.',''):0#">
			<cfset var theCountryID = structKeyExists(arguments,"countryID")?arguments.countryID: "(select countryId from person p inner join location l on p.locationID = l.locationID where p.personID = #arguments.personID#)">
			<cfset result.join = result.join & " left join vCountryScope #result.rightsTableAlias# on #arguments.alias#.hasCountryScope_1 = 1 and #result.rightsTableAlias#.entity = '#arguments.entityType#' and #result.rightsTableAlias#.entityID = #arguments.alias#.#entityTypeStruct.uniqueKey# and #result.rightsTableAlias#.countryID = #theCountryID#">
			<cfset result.whereClause = " and (isNull(#arguments.Alias#.hasCountryScope_1,0) = #arguments.noRightsEqualsNoRightsApplied?0:1# or #result.rightsTableAlias#.countryID is not null)">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getCountryScopeFilterQuerySnippet" access="public" returnType="struct" output=false>
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personid#">
		<cfargument name="countryID" type="string" required="false" hint="Can accept a column as well as an actual countryID">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="Alias" type="string" required="false">
		<cfargument name="noRightsEqualsNoRightsApplied" type="boolean" default="true" hint="If no record rights exist, then assume that everyone has rights rather than no one has rights">

		<cfreturn getCountryRecordRightsFilterQuerySnippet(argumentCollection=arguments,rightsType="country")>
	</cffunction>


	<cffunction name="getRecordRightsFilterQuerySnippet" access="public" returnType="struct" output=false>
		 <cfargument name="personID" type="string" default="#request.relayCurrentUser.personid#" hint="Can accept a column as well as an actual personID">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="Alias" type="string" required="false">
		<cfargument name="noRightsEqualsNoRightsApplied" type="boolean" default="true" hint="If no record rights exist, then assume that everyone has rights rather than no one has rights">

		<cfreturn getCountryRecordRightsFilterQuerySnippet(argumentCollection=arguments,rightsType="record")>
	</cffunction>


	<cffunction name="getPermissions" access="public" output=false>
		<cfargument name="personid" type="numeric" required=true>
		<cfargument name="includeVersionNumber" type="boolean" default=false hint="Where this was originally extracted from a versionNumber was included from application.cachedSecurityVersionNumber, setting this to true includes this ">

		<cfset var getSecurity = "">
		<cfquery name="getSecurity" datasource="#application.siteDataSource#">
			select *,
				(select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =1 ) as Level1Name,
				(select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =2 ) as Level2Name,
				(select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =3 ) as Level3Name,
				(select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =4 ) as Level4Name,
				(select permissionname from permissiondefinition pd where pd.securityTypeID = st.securityTypeID and permissionlevel =5 ) as Level5Name
			from
				securityType st left join (

						SELECT r.SecurityTypeID ,
						Convert(bit,sum(r.permission & 16)) AS Level5,
						Convert(bit,sum(r.permission & 8)) AS Level4,
						Convert(bit,sum(r.permission & 4)) AS Level3,
						Convert(bit,sum(r.permission & 2)) AS Level2,
						Convert(bit,sum(r.permission & 1)) AS Level1
						FROM Rights AS r inner join RightsGroup AS rg
							on   	r.UserGroupID = rg.UserGroupID
							WHERE rg.PersonID = <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
						GROUP BY r.SecurityTypeID
						) as p

				on  p.securitytypeid = st.securitytypeid
			</cfquery>


		<cfscript>
			var security=structNew();
			var i = "";

			for(i=1; i lte getSecurity.recordCount; i = i + 1) {


					security[getSecurity.shortname[i]]	= structNew () ;


				security[getSecurity.shortname[i]].level1 = iif(getSecurity.level1[i] is 1,true,false) ;
				security[getSecurity.shortname[i]].level2 = iif(getSecurity.level2[i] is 1,true,false) ;
				security[getSecurity.shortname[i]].level3 = iif(getSecurity.level3[i] is 1,true,false) ;
				security[getSecurity.shortname[i]].level4 = iif(getSecurity.level4[i] is 1,true,false) ;
				security[getSecurity.shortname[i]].level5 = iif(getSecurity.level5[i] is 1,true,false) ;
				security[getSecurity.shortname[i]].updateokay = iif(getSecurity.level2[i] is 1,true,false) ;  // for backwards compatability with getpermissions
				security[getSecurity.shortname[i]].addOkay = iif(getSecurity.level3[i] is 1,true,false) ;   // for backwards compatability with getpermissions
				security[getSecurity.shortname[i]].read = iif(getSecurity.level1[i] is 1,true,false) ;// for backwards compatability with getpermissions

				if (getSecurity.Level1name[i] is not "") {security[getSecurity.shortname[i]][getSecurity.Level1name[i]] = iif(getSecurity.level1[i] is 1,true,false) ;}
				if (getSecurity.Level2name[i] is not "") {security[getSecurity.shortname[i]][getSecurity.Level2name[i]] = iif(getSecurity.level2[i] is 1,true,false) ;}
				if (getSecurity.Level3name[i] is not "") {security[getSecurity.shortname[i]][getSecurity.Level3name[i]] = iif(getSecurity.level3[i] is 1,true,false) ;}
				if (getSecurity.Level4name[i] is not "") {security[getSecurity.shortname[i]][getSecurity.Level4name[i]] = iif(getSecurity.level4[i] is 1,true,false) ;}
				if (getSecurity.Level5name[i] is not "") {security[getSecurity.shortname[i]][getSecurity.Level4name[i]] = iif(getSecurity.level5[i] is 1,true,false) ;}


			}


			// set up an application scoped version number which can be checked if it changes
			security.versionNumber = application.cachedSecurityVersionNumber;

			return security;
		</cfscript>

</cffunction>

</cfcomponent>