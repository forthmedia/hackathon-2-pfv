<!---
  --- entityLoader
  --- ------------
  ---
  --- author: William.Bibby
  --- date:   3/3/16



	
--->
<cfcomponent accessors="true" output="false" persistent="false">

	<cfset variables.instance = {}>
	<cfset variables.storageScope = variables.instance>
	<cfset variables.storageScope.derivedfieldMetaData = {}>
	<cfset variables.storageScope.fieldMetaData = {}>
	<cfset variables.storageScope.relatedObjectMetaData = {}>

	<cfset variables.cacheVersion = now()>  <!--- TBD this is just a temporary solution for flushing queries --->

	<cffunction name="load" output="false" access="public">
		<cfargument name="entityType" required="true">
		<cfargument name="entityID" required="true">

		<cfscript>
		if (structKeyExists (getSessionPersistenceStructForType (entityType), entityID)) {
			return getSessionPersistenceStructForType (entityType)[ entityID];
		}

		// first look for custom entity cfc
		local.classPath = "code.entities.#entityType#";
		local.exists = false;
		if (doesComponentExist (classPath) ) {
			local.exists = true;
		} else {
			local.classPath = "entities.#entityType#";
			if (doesComponentExist (classPath) ) {
				local.exists = true;
			}
		}

		if (local.exists) {
			var entityObject = new "#classPath#" ( argumentCollection = arguments);
		} else {
			var entityObject = new entities.entity ( argumentCollection = arguments );
		}

		</cfscript>

		<cfreturn entityObject>

	</cffunction>

	<cffunction name="new">
		<cfargument name="entityType" required="true">

		<cfreturn load (entityType = entityType, entityID = 0)>

	</cffunction>

	<cffunction name="componentPathToAbsolutePath">
		<cfargument name="component">

		<cfreturn expandPath ("\" & replace(component, "." , "\" , "ALL") & ".cfc")>

	</cffunction>

	<cffunction name="doesComponentExist">
		<cfargument name="component">

		<cfreturn fileExists (componentPathToAbsolutePath(component))>

	</cffunction>


	<cffunction name="getSessionPersistenceStruct" output="false" access="public">
			<cfscript>
				if (not structKeyExists (session,"entityObjects") ) {
					session.entityObjects = {};
				}
				return session.entityObjects;
		</cfscript>
	</cffunction>


	<cffunction name="getSessionPersistenceStructForType" output="false" access="public">
		<cfargument name="entityType" required="true">
			<cfscript>
				local.sessStruct = getSessionPersistenceStruct(entityType);
				if (not structKeyExists (local.sessStruct, arguments.entityType) ) {
					local.sessStruct[entityType] = {};
				}
				return sessStruct[entityType];
		</cfscript>
	</cffunction>


	<cffunction name="onMissingMethod" output="false" >
		<cfargument name="missingMethodName">


		<cfscript>
			var regExp = "(get)(.*)" ;
			var testForGet = application.com.regExp.refindAllOccurrences(regExp,missingMethodName,{1="get", 2="object"}) ;

			if (arraylen(testForGet)) {

				if (structKeyExists (application.entityTypeID,testForGet[1].object)) {

						return load (testForGet[1].object,missingMethodArguments [1]);

				}

			}
		</cfscript>

	</cffunction>

	<cffunction name="blowCache" access="public" output="false" >
		<cfargument name="entityType" type="string" required="false">
		<cfargument name="fieldNames" type="string" required="false" hint="Not actually used at the moment!">

		<cfif structKeyExists (arguments,"entityType")>

			<cfset structDelete (variables.storageScope.fieldMetaData, entityType)>
			<cfset structDelete (variables.storageScope.relatedObjectMetaData, entityType)>
			<cfset structDelete (variables.storageScope.derivedfieldMetaData, entityType)>

		<cfelse>
			<cfset structDelete (variables.storageScope,"fieldMetaData")>
			<cfset structDelete (variables.storageScope,"relatedObjectMetaData")>
			<cfset structDelete (variables.storageScope,"derivedfieldMetaData")>
		</cfif>

		<cfset variables.cacheVersion = now()>


	</cffunction>

	<cffunction name="getFieldMetadataStruct" access="public" output="false" returnType="struct">
		<cfargument name="entityType" type="string" required="true">

		<cfif not structKeyExists(variables.storageScope.fieldMetaData,arguments.entityType)>
			<cfquery name="local.describe" cachedWithin=#createTimespan(0, 1, 0, 0)#>-- #variables.cacheVersion#
				select * from vFieldMetaData where tablename = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.entityType#">
			</cfquery>

			<cfscript>
				var tempStruct = {};
				for (row in local.describe) {
					tempStruct[row.name] = new entities.fieldMetaDataBean(argumentCollection = row);
				}
				variables.storageScope.fieldMetaData[arguments.entityType] = tempStruct;

			</cfscript>

		</cfif>

		<cfreturn storageScope.fieldMetaData[arguments.entityType]>

	</cffunction>
	


	<cffunction name="getDerivedFieldMetadataStruct" access="public" output="false" returnType="struct">
		<cfargument name="entityType" type="string" required="true">


		<cfif not structKeyExists (storageScope.derivedfieldMetaData, entityType)>
			<cfquery name="local.describe">
			select
				entityTable as tablename
				,flagTextID as name
				,flagGroupTextID as parentFieldName
				,source = 'flag'
				,sourceID = flagID



			from vflagdef

			where flagTypeID in (2,3)
				and entityTable = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.entityType#">

			UNION

			/* any items referencing lookuplist */
			select
				  object_name(fk.parent_object_id)
				, replace(col_name (fk.parent_object_id,fk.parent_column_id),'ID','') + 'TextID'
				, col_name (fk.parent_object_id,fk.parent_column_id)
				, 'lookupList'
				, 0


			from
				sys.foreign_key_columns as fk
			where
				fk.parent_object_id = object_id (<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.entityType#"> )
				and fk.referenced_object_id = object_id ('lookupList')


			</cfquery>

			<cfset storageScope.derivedfieldMetaData[entityType] = application.com.structureFunctions.queryToStruct(local.describe, "name")>

		</cfif>

		<cfreturn storageScope.derivedfieldMetaData[entityType]>


	</cffunction>




	<cffunction name="getRelatedObjectsStruct" access="public" output="false" returnType="struct">
		<cfargument name="entityType" type="string" required="true">



		<cfif not structKeyExists(storageScope.relatedObjectMetaData,arguments.entityType)>

			<cfscript>
				var relationshipsQuery = new Query(sql = "-- #variables.cacheVersion#
													select
														'foreignkey' as relationshipType
														, case when right (name,2) = 'id' then left(name,len(name)-2) else name + '_' end as name  /* name of foreign object is column name with ID stripped off */
														, name as localColumn
														, tableName as localTable
														, foreignKey
														, dbo.regExMatch ('^.*(?=\.)',foreignKey,1) as foreignObject
														, dbo.regExMatch ('^.*(?=\.)',foreignKey,1) as foreignObjectAlias
														, dbo.regExMatch ('(?<=\.).*$',foreignKey,1) as foreignColumn
														, source
														, sourceid
														, '' plural

													FROM
														vFieldMetaData
													where
														tablename = '#entityType#'
															and foreignKey is not null

													UNION
														SELECT
															'childTable'
															, OBJECT_NAME(fk.parent_object_id) as childTable  -- name
															, c2.name localColumn
															, OBJECT_NAME(fk.referenced_object_id) as localTable  -- err this is not quite right, can't be same as foreign object
															, ''
															, OBJECT_NAME(fk.referenced_object_id) foreignObject
															, OBJECT_NAME(fk.referenced_object_id) foreignObjectAlias
															, c1.name foreignColumn
															, ''
															, ''
															, OBJECT_NAME(fk.parent_object_id)+'s' as childTablePlural
														FROM
															sys.foreign_keys fk
														INNER JOIN
															sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
														INNER JOIN
															sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
														INNER JOIN
															sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id

														WHERE OBJECT_NAME(fk.referenced_object_id) = '#entityType#'

													"
												, name = "relationships"
	                            				, cachedWithin=createTimespan(0, 1, 0, 0)
					);

					var relationships = relationshipsQuery.execute().getResult();

					var tempStruct = {};
					for (row in relationships) {
						tempStruct[row.name] = new form.relatedObjectMetaDataBean(argumentCollection = row);
					}
					variables.storageScope.relatedObjectMetaData[arguments.entityType] = tempStruct;
			</cfscript>

		</cfif>

		<cfreturn storageScope.relatedObjectMetaData[entityType]>

	</cffunction>


	<!---
		This is the updating code
		TBD Needs to deal with checking rights
	--->

	<cffunction name = "doUpsert" output="false" access="public">
		<cfargument name="entityType" >
		<cfargument name="entityID" >
		<cfargument name="scope" >

		<cfset var result = {isOK = true,validationErrors=arrayNew(1),entityID=arguments.entityID}>
		<cfset var entity = application.com.entities.load (entityType,entityID)>
		<cfset var relatedEntities  = entity.getRelationships()>
		<cfset var objectsTouched = {}>

		<cfloop collection=#scope# item="local.field">
			<cfset var objectFieldName = getObjectAndFieldFromFieldName(fieldname=field)>
			<cfif objectFieldName.objectName is entityType or structKeyExists (relatedEntities, objectFieldName.objectName) >
				<cfif  refindNoCase ("(_required|_checkbox|_radio|_orig|_date|_eurodate)$", field) is 0>
					<cfset objectsTouched[objectFieldName.objectName] = "">
					<cfset entity.getObject (objectFieldName.objectName).set (objectFieldname.fieldName, scope[field])>
				<cfelseif field contains "_checkbox"  >
					<!--- This is a hidden field which tells us that there was a checkbox on the page.  If the field no longer exists in form scope then the checkbox (or boxes) must have been unselected --->
					<cfif not structKeyExists (scope, replaceNoCase (field,"_checkbox",""))>
						<cfset entity.getObject (objectFieldName.objectName).set (replaceNoCase (objectFieldName.fieldName, "_checkbox", ""), scope[field])>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<cfloop collection=#objectsTouched# item="local.object">
			<cfset var validateResult = entity.getObject (local.object).validate()>
			<cfif !validateResult.isOK>
				<cfset arrayAppend(result.validationErrors,validateResult.validationErrors,true)>
				<cfset result.isOK = false>
			</cfif>
		</cfloop>

		<cfif result.isOK>
			<cfloop collection=#objectsTouched# item="local.object">
				<cfset var saveResult = entity.getObject (local.object).save()>
				<cfif entity.getEntityType() is arguments.entityType >
					<cfset result = saveResult>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name = "processFormScope" output="false" access="public">


		<cfset var result = {}>

		<cfif structKeyExists (form,"entityType") and application.com.security.confirmFieldsHaveBeenEncrypted("entityType")>
			<cfset var entityUniqueKey = application.com.relayEntity.getEntityType(form.entityType).uniqueKey>
			<cfif structKeyExists (form, entityUniqueKey) and application.com.security.confirmFieldsHaveBeenEncrypted(entityUniqueKey) >
				<!--- Update the entity --->
				<cfset result = doUpsert (scope=form, entityType = form.entityType, entityID = form[application.com.relayEntity.getEntityType(form.entityType).uniqueKey])>
				<cfif result.isOK>
					<cfset application.com.relayUI.setMessage(message = (result.fieldsUpdated)?"Record #result.entityID# saved":"Nothing to save", closeafter = 5)>

					<!--- 	Record screen visit
							perhaps not quite the right place to put this being screen'y rather than entity'ish
					--->
					<cfif structKeyExists (form,"screenid")>
						<CFQUERY NAME="local.insertScreenTracking" DATASOURCE=#application.sitedatasource#>
						insert into screenTracking
							(screenid, personid, entityType,entityid,created)
							select
								screenid, <cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#result.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,getdate()
							from screens
							where <cfif isNumeric(screenid)>screenid = <cf_queryparam value="#screenid#" CFSQLTYPE="cf_sql_integer" ><cfelse>screentextid = <cf_queryparam value="#screenid#" CFSQLTYPE="cf_sql_varchar" ></cfif>
						</CFQUERY>
					</cfif>

					<!--- TBD redirector needs lots of work!!! --->
					<CFIF isdefined("frmscreenpostprocessid")>
						<CFIF isdefined("frmprocessid")>
							<cfset var rememberFrmProcessID = frmProcessID>
						<cfelse>
							<cfset rememberFrmProcessID = "">
						</cfif>

						<cfset var thisProcess = "">
							<cfloop list="#frmscreenpostprocessid#" index="thisProcess">
							<cfset var frmProcessid = thisprocess>
								<cfinclude template="/screen/redirector.cfm">
							</cfloop>
						<cfset frmprocessid = rememberFrmProcessID>
					</cfif>

				<cfelse>
					<!--- WAB 2017002-06 Validation error is now a structure--->
					<cfloop array="#result.validationErrors#" index="local.error">
						<cfset application.com.relayUI.setMessage(message = "Field: #local.error.field#; Value: #local.error.value#; #local.error.message#",type="error")>
					</cfloop>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>



	<cffunction name="getObjectAndFieldFromFieldName" access="public" output="false" returnType="struct">
		<cfargument name="fieldname" type="string" required="true">

		<cfset var result = {objectName="",fieldname=""}>
		<cfset var regExpFind = application.com.regExp.refindAllOccurrences ("(.+?)_(.*)", arguments.fieldname, {"1" = "objectName", "2" = "fieldName"})>
		<cfif arraylen(regExpFind)>
			<cfset result = regExpFind[1]>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="getFieldMetaDataByType" access="public" output="false" returnType="query">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="dataType" type="string" required="true">

		<cfquery name="local.qGetFieldsByType">
			select * from vFieldMetaData
			where tablename= <cf_queryparam value="#arguments.tablename#" cfsqltype="cf_sql_varchar">
				and dataType=<cf_queryparam value="#arguments.dataType#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfreturn local.qGetFieldsByType>
	</cffunction>

</cfcomponent>