<cfcomponent hint="Provides logging for Relayware" output="false">

	<cffunction name="log_" output="false" access="public" hint="Write a line to the debug file. Includes tickcounts and memory levels. The location of the file is given by getDebugFileURL()">
            <cfargument name="Label" default="" hint="usually A string but if you pass in a structure it will be used instead of struct (ie you can pass in a single unnamed parameter of a structure if you want)">
            <cfargument name="struct" hint= "Any Structure">
            <cfargument name="forceLog" default = "false" hint= "To override the debug setting">
			<cfargument name="debugDetails" type="struct" required="true" hint= "Provides details on debugging ">
            
            <cfset var content = '' />
            <cfset var totalDiff = '' />
            <cfset var thisDiff = '' />
            <cfset var memorypercent = '' />

            <cfif debugDetails.debugLevel or forceLog>

				<cfset var fileDir = GetDirectoryFromPath(debugDetails.filepath)>
				
				<cfif not directoryExists(fileDir)>
					<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=fileDir)>
				</cfif>

                  <cfif not structKeyExists (debugDetails,"initialised") or not debugDetails.initialised>
                        <cfset debugDetails.initialised = true>
						<cfset debugDetails.start = now()>
						<cfset debugDetails.previous = now()>
						
						<cfif not structKeyExists(debugDetails,"NoDump")>
							<cfset debugDetails.noDump=true>
						</cfif>
						
						<cfif debugDetails.debugLevel gt 1>
							<cfset debugDetails.noDump=false>
						</cfif>
						
                        <cfdump var="" output=#debugDetails.filePath# format="html">
                        
                        <cfset content = '
                                    <style>
                                    //colgroup.log {vertical-align:top}
                                    tr ##warning {color:red;}
                                    td {vertical-align:top}
                                    </style>
                                    <TABLE>
                                          <colgroup class="log"><col ><col><col><col><col><col></colgroup>
                                          <tr><td>Elapsed Time</td><td>Time Diff</td><td>Mem %</td><td>Buffer</td><td>Template</td><td>Label</td><td>Data</td></tr>
										  <tr><td>#debugDetails.start#</td><td></td><td></td><td></td><td></td><td>Start Time</td><td>#debugDetails.start#</td></tr>
                        '>
                        <cffile action="append" file="#debugDetails.filepath#" output="#content#" addnewline="true">
                  </cfif>
      
                  <cfset debugDetails.now = getTickCount()>
                  <cfset totalDiff = debugDetails.now - debugDetails.start>
                  <cfset thisDiff = debugDetails.now - debugDetails.previous>
                  <cfset memorypercent = application.com.globalfunctions.getmemory().percentUsedAllo>
            
                  <cfif isStruct(Label)>
                        <cfset struct = label>
                        <cfset label = "">
                  </cfif>                 

                 <cfif true or not debugDetails.ToScreen>
                       <cfset var caller = application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction='log_')>
                        <cfset var outputString = "<tr><td>#totaldiff#</td><td>#thisdiff#</td><td>#memorypercent#</td><td>#len(getPageContext().getCFOutput().getBuffer().tostring())#</td><td>#caller.template#:#caller.function#:#caller.lineNumber#</td><td>#rereplace(htmlEditFormat(label),'\r','<BR>','ALL')#</td><td>">
                        <cfif structKeyExists (arguments,"struct") and isSimpleValue(arguments.struct)>
                              <cfset outputString &= "#htmleditformat(struct)#">
                        </cfif>
                        <cffile action="append" file="#debugDetails.filePath#" output="#outputString#" addnewline="true">
                        
                        <cfif (not debugDetails.NoDump OR forceLog) and structKeyExists (arguments,"struct") and isdefined("arguments.struct") and not (isSimpleValue(arguments.struct)) >
                              <cfdump var="#struct#" output=#debugDetails.filePath# format="html" expand="false">
                        </cfif>
                        <cffile action="append" file="#debugDetails.filePath#" output="</td></tr>" addnewline="true">
                  <CFELSE>
                        <CFOUTPUT>#rereplace(label,'\r','<BR>','ALL')#</CFOUTPUT>
                        <cfif structKeyExists (arguments,"struct")>
                              <cfdump var="#struct#">
                        </cfif>

                  
                  </CFIF> 
                  
      
                  <cfset debugDetails.previous = debugDetails.now>
      
            </cfif>
      
      </cffunction>


</cfcomponent>