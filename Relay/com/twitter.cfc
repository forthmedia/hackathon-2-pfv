<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="getAuthenticateRequestString" access="public" returnType="struct" output="false">
		<cfargument name="callbackURL" type="string" default="#request.currentSite.protocolAndDomain#/social/callback.cfm">

		<cfset var oToken = CreateObject("component", "oauth.oauthtoken").getRequestToken(argumentCollection=arguments)>
		<cfset var result = {method="post",url="https://api.twitter.com/oauth/authenticate?oauth_token=" & oToken.getKey()}>

		<cfreturn result>
	</cffunction>


	<cffunction name="getAuthoriseRequestString" access="public" returnType="struct" output="false">
		<cfargument name="callbackURL" type="string" required="false">

		<cfset var oToken = CreateObject("component", "oauth.oauthtoken").getRequestToken(argumentCollection=arguments)>
		<cfset var result = {method="get",url="https://api.twitter.com/oauth/authorize?oauth_token=" & oToken.getKey()}>

		<cfreturn result>
	</cffunction>


	<cffunction name="getProfile" access="public" returntype="struct" output="true">
		<cfargument name="fieldList" type="string" default="id,first-name,last-name,headline,phone-numbers,date-of-birth,picture-url,twitter-accounts,im-accounts,educations,public-profile-url">
		<cfargument name="entityID" type="numeric" required="false">
		<cfargument name="entityTypeID" type="numeric" default=0>

		<cfset var profileURL = "https://api.twitter.com/1.1/account/verify_credentials.json">
		<cfset var profileResponse = send(url=profileURL,argumentCollection=arguments)>
		<cfset var profileStruct = {id="",firstname="",lastname="",jobDesc="",pictureURL="/images/social/icon_no_photo_80x80.png",dob="",officePhone="",homePhone="",mobilePhone="",twitterAccount="",imAccount="",publicProfileUrl=""}>
		<cfset var profileResponseStruct = profileResponse.fileContent>
		<cfset var result = {isOK=true,message="",profile=profileStruct}>

		<cfif profileResponse.isOK>
			<cfif structKeyExists(profileResponseStruct,"id")>
				<cfset profileStruct.id = profileResponseStruct["id"]>
			</cfif>
			<cfif structKeyExists(profileResponseStruct,"profile_image_url")>
				<cfset profileStruct.pictureURL = profileResponseStruct["profile_image_url"]>
			</cfif>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = profileResponse.message>
			<!--- <cfset result.authoriseLink = profileResponse.authoriseLink> --->
		</cfif>

		<cfset result.profile = profileStruct>

		<cfreturn result>
	</cffunction>


	<cffunction name="send" access="private" returntype="struct" output="true">
		<cfargument name="url" type="string" required="true">
		<cfargument name="httpMethod" type="string" default="Get">
		<cfargument name="parameters" type="struct" default="#structNew()#">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">  <!--- is making a call on behalf of someone.. need to get their ID to get the access token --->
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeId.person#">

		<!--- set up the required objects including signature method--->
		<cfset var oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
		<cfset var result = {isOk = true,fileContent='',message="The text was tweeted successfully."}>
		<cfset var service = application.com.service.getService(serviceID="twitter")>
		<cfset var argStruct = {encryptToken=false,serviceID="twitter",entityID=arguments.entityID,entityTypeID=arguments.entityTypeID}>
		<cfset var tokenParams = duplicate(arguments.parameters)>
		<cfset var multipart = false>
		<cfset var twitterArg = "">
		<cfset var filePath = "">

		<!--- TODO: need to determine what to do if access token is empty --->
		<cfset var apiAccessToken = "">
		<cfset var oConsumer = CreateObject("component", "oauth.oauthconsumer").init(sKey = service.consumerKey, sSecret = service.consumerSecret)>
		<cfset var tokenResponse = {statusCode=401,text="",fileContent=""}>
		<cfset var oReq = "">
		<cfset var postParams = "">

		<cfif service.consumerKey neq "" and service.consumerSecret neq "">
			<cfset apiAccessToken = CreateObject("component", "oauth.oauthToken").getAccessToken(argumentCollection=argStruct)>

			<!--- create the request --->
			<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
				oConsumer = oConsumer,
				oToken = apiAccessToken,
				sHttpMethod = arguments.httpMethod,
				sHttpURL = arguments.url,stparameters=tokenParams)>

			<!--- sign the request using hash algorithm to get the signature --->
			<cfset oReq.signRequest(
				oSignatureMethod = oReqSigMethodSHA,
				oConsumer = oConsumer,
				oToken = apiAccessToken)>

			<!--- used to get person details --->
			<cfif arguments.httpMethod eq "get">
				<cfhttp url="#oREQ.getHttpURL()#" method="get" result="tokenResponse">
					<cfhttpparam type="header" name="Authorization" value="#oReq.toheader()#">
				</cfhttp>

			<!--- posting a tweet --->
			<cfelse>

				<!--- if posting a tweet without an image, the params get 'posted' on the url --->
				<cfif not structKeyExists(arguments,"media[]")>
					<cfset postParams = application.com.structurefunctions.convertStructureToNameValuePairs(struct = arguments.parameters,delimiter="&",encode="url")>
				<cfelse>
					<cfset multipart = true>
					<cfif arguments["media[]"] contains "/content/">
						<!--- NJH JIRA BF-264 - changed way to get filepath - use regular expression. Not sure how previous method worked at all! --->
						<cfset filePath = reReplaceNoCase(arguments["media[]"],"(.*)(\/content\/.*)","\2")>
						<cfset arguments["media[]"] = expandPath(filePath)>
					</cfif>
				</cfif>

				<!--- NOTE: It is VERY Important that the case of the URL variables match the case of the keys in arguments.parameter; otherwise you will get a 401 --->
				<cfhttp url="#oREQ.getHttpURL()#?#trim(postParams)#" method="post" result="tokenResponse" multipart="#multipart#">
					<cfhttpparam type="header" name="Authorization" value="#oReq.toheader()#">

					<!--- posting a tweet with an image requires the values to get posted as formfields --->
					<cfif structKeyExists(arguments,"media[]")>
					<cfloop list="status,media[]" index="twitterArg">
						<cfif structKeyExists(arguments,twitterArg)>
							<cfif twitterArg eq "media[]">
								<cfhttpparam type="file" name="#twitterArg#" file="#arguments[twitterArg]#">
							<cfelse>
								<cfhttpparam type="formfield" name="#lcase(twitterArg)#" value="#arguments[twitterArg]#">
							</cfif>
						</cfif>
					</cfloop>
					</cfif>
				</cfhttp>
			</cfif>

			<cfset result.isOK = true>
			<cfset result.fileContent = deserializeJSON(tokenResponse.fileContent)>
			<cfset structAppend(result,tokenResponse,false)>

			<cfif tokenResponse.statusCode neq "200 OK" and tokenResponse.statusCode neq "201">
				<cfset result.isOK = false>
				<cfif structKeyExists(result.fileContent,"error")>
					<cfset result.message = result.fileContent.error>
				</cfif>
			</cfif>

			<!--- if access token has expired or user is not logged in, then we will need to re-authorise. The callback should take them back to the current page
				so that in theory everything is now fine  --->
			<cfif structKeyExists(tokenResponse,"responseHeader") and structKeyExists(tokenResponse.responseHeader,"Explanation") and tokenResponse.responseHeader.Explanation eq "Unauthorized">
				<cfset result.isOK = false>
				<cfset result.message = tokenResponse.responseHeader.Explanation>
			</cfif>

		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "The service has not been initialised.">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="share" access="public" returntype="struct">
		<cfargument name="comment" type="string" required="true">
		<cfargument name="link" type="string" required="false">
		<cfargument name="image" required="false">
		<cfargument name="entityID" type="string" required="false"> <!--- if not present, then assumes the current user. --->
		<cfargument name="entityTypeID" type="numeric" default=0>

		<cfset var xmlContent = "">
		<cfset var shareURL = "https://api.twitter.com/1.1/statuses/update.json">
		<cfset var shareResponse = "">
		<cfset var result = {isOK=true,message="Successfully tweeted."}>
		<cfset var parameters = {status=arguments.comment}>
		<cfset var argStruct = {url=shareURL,httpMethod="post",status=arguments.comment,parameters=parameters}>

		<!--- if posting a tweet with an image, set the parameters struct to empty --->
		<cfif structKeyExists(arguments,"image") and arguments.image neq "">
			<cfset argStruct.url = "https://api.twitter.com/1.1/statuses/update_with_media.json">
			<cfset argStruct["media[]"] = arguments.image>
			<cfset argStruct.parameters = {}>
		</cfif>

		<cfif structKeyExists(arguments,"entityID")>
			<cfset argStruct.entityID = arguments.entityID>
			<cfset argStruct.entityTypeID = arguments.entityTypeID>
		</cfif>

		<!--- 2014-11-10	YMA	CORE-946 issue posting links resolved for linkedin posting --->
		<cfif structKeyexists(arguments,"link") and len(arguments.link)+1+len(argStruct.status) lte 140>
			<cfset argStruct.status = argStruct.status& " " & arguments.link>
			<cfif not (structKeyExists(arguments,"image") and arguments.image neq "")>
				<cfset argStruct["parameters"] = {status=argStruct.status}>
			</cfif>
		</cfif>

		<cfset shareResponse = send(argumentCollection=argStruct)>
		<cfif not shareResponse.isOk>
			<cfset result.isOK = false>
			<cfset result.message = "Unable to tweet on Twitter.">
		</cfif>

		<cfreturn result>
	</cffunction>

</cfcomponent>