<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="relayDashboardData" hint="I return the data for dashboard widgets to display">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    2006-04-14 SWJ Started to develop a better architecture and separate the data layer

	Having worked with this now for a while the attributes of the chart to display need to be included in this data.
	It then means that this data is responsible for things like the chartTitle, types, data and what to display e.g. line, bar, pie
	This should then be passed as part of the structure in getChartData

	We should also split the methods into separate components per subject e.g. salesOUtData, Incentive etc.
	We could then loop ove the available dashboard components and let the user see a list of components and methods
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLeadPipelineMonthData" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Nov|13337113
		Dec|73354672
		Jan|46627358
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getLeadPipelineRegionData" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		APAC|1333
		EMEA|4662
		US|7335
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getLeadPipelineStatusData" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Committed|2337113
		Prospect|46627358
		Suspect|73354672
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getLeadPipelineByCountryData" hint="I return a queryObject for this data">
		<cfargument name="countryID" default="">
		<cfset var lCountryIDs = "#request.relaycurrentuser.countryid#">

		<cfif arguments.countryID neq "">
			<cfset lCountryIDs = listAppend(lCountryIDs,arguments.countryID)>
		</cfif>
		<cfquery name="getChartData" datasource="#application.SiteDataSource#">
			SELECT DISTINCT	c.CountryID, c.CountryDescription as chart_Label, CONVERT(varchar(100), CAST(SUM(op.subtotal) / COUNT(c.CountryID) AS money)) AS value
			FROM			Country c 		INNER JOIN
							opportunity o	ON c.CountryID = o.countryID INNER JOIN
							opportunityProduct op	ON o.opportunityID = op.OpportunityID INNER JOIN
							OppStage os		ON o.stageID = os.OpportunityStageID
			WHERE			(os.status = 'Committed') and (c.countryID  IN ( <cf_queryparam value="#lCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			GROUP BY 		c.CountryID, c.CountryDescription
		</cfquery>

		<cfreturn getChartData>
	</cffunction>
<!--- ==============================================================================

=============================================================================== --->
	<cffunction access="public" name="getLeadsByMonthData" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Nov|13337113.23
		Dec|73354672
		Jan|46627358
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getLeadsByRegionData" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		APAC|1333
		EMEA|4662
		US|3354
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getLeadsByStatusData" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Committed|13337113.23
		Prospect|46627358
		Suspect|73354672
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

<!--- ==============================================================================
    Lead Source
=============================================================================== --->
	<cffunction access="public" name="getPipelineByIndustryValue" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Manu|123168
		Govt|343654
		Svcs|456737
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getPipelineBySource" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Events|4662
		eMarketing|3354
		Direct Mail|1245
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getPipelineByIndustryVolume" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartDataTarget
		chart_Label,value
		Manu|3133711
		Govt|4662758
		Svcs|3542894
		</cf_querySim>

		<cfset getChartData.target = getChartDataTarget>

		<cf_querySim>
		getChartDataActuals
		chart_Label,value
		Manu|2913371
		Govt|4366275
		Svcs|3654289
		</cf_querySim>

		<cfset getChartData.actual = getChartDataActuals>

		<cfreturn getChartData>
	</cffunction>
<!--- ==============================================================================
    Sales OUt Data
=============================================================================== --->

	<cffunction access="public" name="getHistoricSOD" hint="I return a queryObject for this data">

		<cf_querySim>
		getSeriesOneData
		chart_Label,value
		Group A|12337113
		Group B|63354672
		Group C|36627358
		</cf_querySim>

		<cfset getChartData.Q1 = getSeriesOneData>

		<cf_querySim>
		getSeriesTwoData
		chart_Label,value
		Group A|10337113
		Group B|53354672
		Group C|26627358
		</cf_querySim>

		<cfset getChartData.Q2 = getSeriesTwoData>

		<cf_querySim>
		getSeriesThreeData
		chart_Label,value
		Group A|13337113
		Group B|73354672
		Group C|46627358
		</cf_querySim>

		<cfset getChartData.Q3 = getSeriesThreeData>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getRegionalSOD" hint="I return a queryObject for this data">

		<cf_querySim>
		getSeriesOneData
		chart_Label,value
		UK|22337113
		France|23354672
		Germany|26627358
		</cf_querySim>
		<cfset getChartData.Q1 = getSeriesOneData>

		<cf_querySim>
		getSeriesTwoData
		chart_Label,value
		UK|32337113
		France|33354672
		Germany|36627358
		</cf_querySim>
		<cfset getChartData.Q2 = getSeriesTwoData>

		<cf_querySim>
		getSeriesThreeData
		chart_Label,value
		UK|12337113
		France|13354672
		Germany|16627358
		</cf_querySim>
		<cfset getChartData.Q3 = getSeriesThreeData>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getInventoryByDisti" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		TechData|30711
		C2000|23354
		Ingram|32735
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

<!--- ==============================================================================
    incentive
=============================================================================== --->

	<cffunction access="public" name="getAverageIncentiveClaimValue" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		Product A|1300
		Product B|1245
		Product C|1135
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getIncentiveLiabilityByCountry" hint="I return a queryObject for this data">

		<cf_querySim>
		getSeriesOneData
		chart_Label,value
		UK|37113
		France|54672
		Germany|27358
		</cf_querySim>
		<cfset getChartData.Q1 = getSeriesOneData>

		<cf_querySim>
		getSeriesTwoData
		chart_Label,value
		UK|32337
		France|33354
		Germany|17358
		</cf_querySim>
		<cfset getChartData.Q2 = getSeriesTwoData>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getIncentiveByDisti" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		TechData|711
		C2000|354
		Ingram|275
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

<!--- ==============================================================================
    eMarketing
=============================================================================== --->

	<cffunction access="public" name="getClickThruRates" hint="I return a queryObject for this data">

		<cf_querySim>
		getChartData
		chart_Label,value
		< 2%|25
		2% - 5%|15
		> 2%|3
		</cf_querySim>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="getCommsSent" hint="I return a queryObject for this data">

		<cf_querySim>
		getSeriesOneData
		chart_Label,value
		UK|13
		France|22
		Germany|8
		</cf_querySim>
		<cfset getChartData.Q1 = getSeriesOneData>

		<cf_querySim>
		getSeriesTwoData
		chart_Label,value
		UK|11
		France|17
		Germany|9
		</cf_querySim>
		<cfset getChartData.Q2 = getSeriesTwoData>

		<cf_querySim>
		getSeriesThreeData
		chart_Label,value
		UK|9
		France|12
		Germany|7
		</cf_querySim>
		<cfset getChartData.Q3 = getSeriesThreeData>

		<cfreturn getChartData>
	</cffunction>

	<cffunction access="public" name="geteMailAddressesByCountry" hint="I return a queryObject for this data">

		<cf_querySim>
		getSeriesTwoData
		chart_Label,value
		UK|234
		France|112
		Germany|739
		</cf_querySim>
		<cfset getChartData.Bad = getSeriesTwoData>

		<cf_querySim>
		getSeriesOneData
		chart_Label,value
		UK|7113
		France|3246
		Germany|5439
		</cf_querySim>
		<cfset getChartData.Good = getSeriesOneData>


		<cfreturn getChartData>
	</cffunction>


	<cffunction access="public" name="getSODproductSalesByMonth" hint="I return a queryObject for this data">
		<cfargument name="organisationID" required="no">
		<cfargument name="numOfMonths" required="no" default="3">
		<cfargument name="productCode" required="no" default="All">

		<cfquery name="getSODproductSalesByMonth" datasource="#application.siteDataSource#">

			DECLARE @beginningOfMonth datetime, @startYear int, @startMonth int

					set @beginningOfMonth = cast(DATEPART(yyyy,GetDate())as varchar)  +'/'+ cast(DATEPART(mm,GetDate()) as varchar )+'/01 00:00:00'
					set @startYear = datepart(yyyy,dateadd(mm, -#numOfMonths#,@beginningOfMonth))
					set @startMonth = datepart(mm,dateadd(mm, -#numOfMonths#,@beginningOfMonth))

			SELECT SUBSTRING(CalendarMonth.CalendarMonthDesc,1,3) AS chart_Label, a.value from
					  CalendarMonth LEFT OUTER JOIN
				(SELECT   DATEPART(yyyy, SODload.SalesDate) as salesYear, DATEPART(mm, SODload.SalesDate) as salesMonth, SUM(SODload.TotalPrice) AS value
				FROM      SODloadLocationLookup INNER JOIN
	                      Location ON SODloadLocationLookup.LocationID = Location.LocationID INNER JOIN
	                      organisation ON Location.OrganisationID = organisation.OrganisationID INNER JOIN
	                      SODload ON SODloadLocationLookup.ThirdPartyForeignKey = SODload.ThirdPartyForeignKey
				WHERE 	DATEADD(mm, -#numOfMonths#, @beginningOfMonth) <= SODload.SalesDate
						AND
						@beginningOfMonth > SODload.SalesDate
				<cfif isDefined("arguments.organisationID")>
				and 	  organisation.OrganisationID =  <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="cf_sql_integer" >
				</cfif>
				<cfif arguments.productCode neq "All">
				and 	  SODload.productCode =  <cf_queryparam value="#arguments.productCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				GROUP BY  DATEPART(yyyy, SODload.SalesDate), DATEPART(mm, SODload.SalesDate)
			) a
			ON a.SalesMonth = CalendarMonth.CalendarMonth
			AND a.SalesYear = CalendarMonth.CalendarYear
			WHERE (
				(CalendarYear = @startYear AND CalendarMonth.CalendarMonth >= @startMonth)
			OR (CalendarYear > @startYear)
				)
			AND (
				( CalendarYear = DATEPART(yyyy,GetDate()) AND CalendarMonth.CalendarMonth < DATEPART(mm,GetDate()))
			OR (CalendarYear < DATEPART(yyyy,GetDate()))
			)
			<!--- NJH 2009/02/20 Sony Bug Fix Issue 1839 - added order --->
			order by CalendarMonth.CalendarYear, CalendarMonth.CalendarMonth
		</cfquery>
		<cfset getChartData.Value = getSODproductSalesByMonth>


		<cfquery name="getSODproductVolumeByMonth" datasource="#application.siteDataSource#">

			DECLARE @beginningOfMonth datetime, @startYear int, @startMonth int

					set @beginningOfMonth = cast(DATEPART(yyyy,GetDate())as varchar)  +'/'+ cast(DATEPART(mm,GetDate()) as varchar )+'/01 00:00:00'
					set @startYear = datepart(yyyy,dateadd(mm, -#numOfMonths#,@beginningOfMonth))
					set @startMonth = datepart(mm,dateadd(mm, -#numOfMonths#,@beginningOfMonth))

			SELECT SUBSTRING(CalendarMonth.CalendarMonthDesc,1,3) AS chart_Label, a.value from
					  CalendarMonth LEFT OUTER JOIN
				(SELECT   DATEPART(yyyy, SODload.SalesDate) as salesYear, DATEPART(mm, SODload.SalesDate) as salesMonth, SUM(SODload.Quantity) AS value
				FROM      SODloadLocationLookup INNER JOIN
	                      Location ON SODloadLocationLookup.LocationID = Location.LocationID INNER JOIN
	                      organisation ON Location.OrganisationID = organisation.OrganisationID INNER JOIN
	                      SODload ON SODloadLocationLookup.ThirdPartyForeignKey = SODload.ThirdPartyForeignKey
				WHERE 	DATEADD(mm, -#numOfMonths#, @beginningOfMonth) <= SODload.SalesDate
						AND
						@beginningOfMonth > SODload.SalesDate
				<cfif isDefined("arguments.organisationID")>
				and 	  organisation.OrganisationID =  <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="cf_sql_integer" >
				</cfif>
				<cfif arguments.productCode neq "All">
				and 	  SODload.productCode =  <cf_queryparam value="#arguments.productCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				GROUP BY  DATEPART(yyyy, SODload.SalesDate), DATEPART(mm, SODload.SalesDate)
			) a
			ON a.SalesMonth = CalendarMonth.CalendarMonth
			AND a.SalesYear = CalendarMonth.CalendarYear
			WHERE (
				(CalendarYear = @startYear AND CalendarMonth.CalendarMonth >= @startMonth)
			OR (CalendarYear > @startYear)
				)
			AND (
				( CalendarYear = DATEPART(yyyy,GetDate()) AND CalendarMonth.CalendarMonth < DATEPART(mm,GetDate()))
			OR (CalendarYear < DATEPART(yyyy,GetDate()))
			)
		</cfquery>
		<cfset getChartData.Volume = getSODproductVolumeByMonth>

		<cfreturn getChartData>
	</cffunction>

</cfcomponent>

