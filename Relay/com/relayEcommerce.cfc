<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="RelayEcommerce" hint="Functions for Relay eCommerce">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPersonsProductFromSet" hint="Returns a query object containing data for a person's product">
		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="productID" type="numeric" required="yes">

		<cfscript>
			var getProduct = "";
		</cfscript>
		
		<CFQUERY NAME="getPersonsCountry" DATASOURCE="#application.sitedatasource#">
			SELECT countryid FROM location o INNER JOIN person p
			ON p.locationid = o.locationid
			WHERE p.personid = #arguments.personid#
		</CFQUERY>

		<CFQUERY NAME="GetProductSKUs" DATASOURCE="#application.sitedatasource#">
			SELECT SKU FROM Product 
			WHERE productID in (#arguments.productID#)
		</CFQUERY>
		<!--- Change to Enable multiple products to be returned --->
		<CFSET SKUFilterQ = replace(valuelist(GetProductSKUs.SKU,","),";",",","ALL")>	
		<!--- NOTE! If multiple products are used and are in different coutries this is a problem --->
		<!--- first try and get a single product from the list of SKUs for the users country --->
		<CFQUERY NAME="GetProduct" DATASOURCE="#application.sitedatasource#">
			SELECT ProductID, SKU, Description, ListPrice, DiscountPrice, PriceISOCode FROM Product
			WHERE 
			countryid =  <cf_queryparam value="#getPersonsCountry.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND SKU IN (<cf_queryparam value="#SKUFilterQ#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
		</CFQUERY>
		
		<!--- second try and get a single product from the list of SKUs for the users country groups --->
		<CFIF GetProduct.recordcount EQ 0>
			<CFQUERY NAME="GetProduct" DATASOURCE="#application.sitedatasource#">
				SELECT ProductID, SKU, Description, ListPrice, DiscountPrice, 
				PriceISOCode FROM Product
				WHERE 
				countryid IN (SELECT CountryGroupID FROM CountryGroup 
					WHERE CountryMemberID =  <cf_queryparam value="#getPersonsCountry.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > )
				AND SKU IN (<cf_queryparam value="#SKUFilterQ#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</CFQUERY>
		</CFIF>
		
		<!--- finally try and get a single product from the list of SKUs for countryID 0 (which is All countries) --->
		<CFIF GetProduct.recordcount EQ 0>
			<CFQUERY NAME="GetProduct" DATASOURCE="#application.sitedatasource#">
				SELECT ProductID, SKU, Description, ListPrice, DiscountPrice, 
				PriceISOCode FROM Product
				WHERE countryid=0
				AND SKU IN (<cf_queryparam value="#SKUFilterQ#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</CFQUERY>
		</CFIF>
		
		<cfreturn GetProduct>
	</cffunction>
	
<!--- AR 2005-04-25 GlobalSKU functionality. --->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns the set of available currencies that can be used.                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getCurrencies" returntype="query">
		
		<cfargument name="countryID" type="numeric" required="yes">
		
		<cfset var GetCurrenciesQry = "">
		<CFQUERY NAME="GetCurrenciesQry" dataSource="#application.siteDataSource#">
			SELECT DISTINCT countrycurrency    
			  FROM country 
			 where countrycurrency is not null
			 <cfif countryID is not "">
			 	and CountryID=#countryID#
			 </cfif>
		     ORDER BY countrycurrency
		</CFQUERY>
		<cfreturn GetCurrenciesQry>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns details of a promotion.                  
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 
	<cffunction name="GetPromotionDetails" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		
		<cfset var PromotionQry = "">
		<CFQUERY NAME="PromotionQry" dataSource="#application.siteDataSource#">
			SELECT * 
			FROM Promotion
				where campaignID = #arguments.campaignID#
		</CFQUERY>
		<cfreturn PromotionQry>
	
	
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns the set of currencies for a Global SKU products.                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="GetPromotionGlobalSKURegions" returntype="query" output="Yes">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="countryID" required="no" default="">
		<cfset var GetPromotionCurrenciesQry = "">
		<CFQUERY NAME="GetPromotionCurrenciesQry" dataSource="#application.siteDataSource#">
			SELECT p.campaignID,p.countryID, c.countryDescription,c.countryCurrency
			FROM         Country c INNER JOIN
                      promotionGlobalSKURegion p ON c.CountryID = p.countryID INNER JOIN
                      CountryGroup ON c.CountryID = CountryGroup.CountryGroupID
WHERE     (c.ISOCode IS NULL) AND (c.CountryCurrency IS NOT NULL) AND (p.campaignID = #arguments.campaignID#) 
				<cfif len(arguments.countryID)>
					<!--- Return the region. --->
					AND 
                      (CountryGroup.CountryMemberID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" > )	
				</cfif>
		    	ORDER BY countrycurrency
		</CFQUERY>
		<cfscript>
			if (not GetPromotionCurrenciesQry.recordcount and len(arguments.countryID)){
				// Country is not within the set of regions. So we need to 'add it' using the default currency for the campaign.
				campaignDetails = GetPromotionDetails(campaignID = arguments.campaignID);
				if (len(campaignDetails.GlobalSKUDefaultCurrency)){
					tmp = QueryAddRow(GetPromotionCurrenciesQry);
					tmp = QuerySetCell(GetPromotionCurrenciesQry,"campaignID",arguments.campaignID);
					tmp = QuerySetCell(GetPromotionCurrenciesQry,"countryID",arguments.countryID);
					tmp = QuerySetCell(GetPromotionCurrenciesQry,"countryDescription","NOT IN A REGION");
					tmp = QuerySetCell(GetPromotionCurrenciesQry,"countryCurrency",campaignDetails.GlobalSKUDefaultCurrency);
				}
			}
		
		</cfscript>
		<cfreturn GetPromotionCurrenciesQry>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Updates the set of regions for a promotion.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="updatePromotionGlobalSKURegions">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="GlobalSKURegions" required="yes">
		<!--- Delete the current set of 'languages'. --->
		<CFQUERY NAME="DelGlobalSKURegionsQry" dataSource="#application.siteDataSource#">
			delete from promotionGlobalSKURegion 
				where campaignID = #arguments.campaignID#
		</CFQUERY>
		<!--- Now update the list. --->
		<cfloop list="#arguments.GlobalSKURegions#" index="currIdx">
			<CFQUERY NAME="insGlobalSKURegionsQry" dataSource="#application.siteDataSource#">
				insert into promotionGlobalSKURegion (campaignID,countryID,lastupdatedBY)
					values (<cf_queryparam value="#arguments.campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#currIdx#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)					
			</CFQUERY>
		</cfloop>		
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns the set of products for a specified country within a campaign.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="GetGlobalSKUProductSet" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="countryID" required="Yes" type="numeric">
		<cfargument name="showSuppressed" default="false">
		
		<cfset var ProductDataQry = "">
		<!--- Determine the currency for the country based on the region it is in. --->
		<cfscript>
		CountryCurrencyQry = GetPromotionGlobalSKURegions(campaignID = arguments.campaignID, countryID = arguments.countryID);
		
		</cfscript><!--- 
		<cfquery name="CountryCurrancyQry" datasource="#application.SiteDataSource#">
			select countryCurrency from country where countryID in (
				select countryGroupID from countryGroup where countryMemberID = #arguments.countryID#) 
				AND countryID in (select countryID from promotionGlobalSKURegion where campaignID = #arguments.campaignID#)
		</cfquery> --->
		
		<cfif arguments.showSuppressed>
			<cfset productTable = "vproductlistInternal">
		<cfelse>
			<cfset productTable = "vproductlist">
		</cfif>

		<!--- 
			2015-11-16 WAB PROD2015-364 - products country restrictions
            we use ranking to detemine the best match based on whether has been explicity scoped with this country 
            (as opposed to the country being a member of a countryGroup (region) which is scoped to the entity)
		--->
		<CFQUERY NAME="ProductDataQry" datasource="#application.SiteDataSource#">
		select * from (
			SELECT 
			<cfif arguments.showSuppressed>
				distinct(SKU_group) as skugroup,
				product_group as productgroup,
				CASE WHEN supressed = 'Show' THEN '0' ELSE '1' END as deleted,
				list_Price as listprice,
				discount_Price as discountPrice,
			<cfelse>
				distinct(SKUgroup) as skugroup,
				productgroup,
				deleted,
				listPrice,
				discountPrice,
				NumberAvailable,
			</cfif> 
				SKU,
				sortorder, 
				description,
				-- countryID_notused,
				priceISOcode as currency,
				AskQuestions,
				ProductID,
				productgroupid,
				MAXQUANTITY,
				0 as PREVIOUSLYORDERED,
				(select count(productSKUGroup) from productparentsGlobalSKU where prod.<cfif arguments.showSuppressed>SKU_group<cfelse>SKUgroup</cfif>=productparentsGlobalSKU.parentSKUGroup and CampaignID = #arguments.campaignID#) as noOfChildren,
				dense_rank() over (
							partition by case when isNull(skugroup,'') = '' then convert(varchar,productid) else skugroup end 
							order by case when hascountryscope = 0 then 0 when vcs.explicityScopedToThisCountry = 0 then 1 else 2 end desc) 
							as rank
				
			FROM #productTable# prod
					left join
				vcountryscope vcs on prod.productid = vcs.entityid and vcs.entity = 'product' and vcs.countryid =  #arguments.countryID#
			WHERE 
				CampaignID = #arguments.campaignID#
				and (prod.hascountryscope = 0 or vcs.countryid is not null)
				and prod.priceISOcode =  <cf_queryparam value="#CountryCurrencyQry.countryCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 			
						
		) as dummy	
		where rank =1
				ORDER BY sortorder,SKUgroup,sku
		</CFQUERY>
		<cfreturn ProductDataQry>
	
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns the set of countries that can be used within a campaign.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getPromotionGlobalSKUCountries" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="restrictCountriesTo" required="No" default="">
		<!--- Get the set of countries. --->
		<cfset var countryQry = "">
		<CFQUERY NAME="countryQry" datasource="#application.SiteDataSource#">

			select distinct cg.CountryMemberID as countryID, c.countryDescription  
				from countryGroup cg inner join country c on c.countryID = cg.CountryMemberID
				where cg.CountryGroupID in (
					select CountryID 
						from promotionGlobalSKURegion 
						where campaignID = #arguments.campaignID#)	
					<cfif len(arguments.restrictCountriesTo)>					
					AND cg.countryMemberID  in ( <cf_queryparam value="#arguments.restrictCountriesTo#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</cfif>
						order by countryDescription
		</CFQUERY>
		<cfreturn countryQry>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns the set of currencies used within a .
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 	<cffunction name="getPromtionGlobalSKUCurrencies" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfset var currencyQry = "">
		<CFQUERY NAME="currencyQry" datasource="#application.SiteDataSource#">
			select distinct c.countryCurrency 
				from country c, promotionGlobalSKURegion pr
					where c.countryID = pr.countryID
					AND pr.campaignID = #arguments.campaignID#
		</CFQUERY>
		<!--- Get the promotion details. --->
		<cfset promoQry	= GetPromotionDetails(CampaignID = arguments.campaignID)>
		<cfif not listfind(valuelist(currencyQry.countryCurrency),promoQry.GlobalSKUdefaultCurrency)>
			<cfscript>
				tmp = QueryAddRow(currencyQry);
				tmp = QuerySetCell(currencyQry,"countryCurrency",promoQry.GlobalSKUdefaultCurrency);
			
			</cfscript>
		</cfif>
		<cfreturn currencyQry>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Inserts a NEW Global SKU product using the structure. It is expected that 
	the productData structure is the FORM struct.
	
	It returns the SKU Group.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="GlobalSKUInsertProduct" returntype="string">
	<cfargument name="productData" required="Yes" type="struct">
	<cfscript>
		var listPrice = 0;
		var discountPrice = 0;
		arguments.productData.skugroup = REreplace(arguments.productData.SKU,"[[:space:]|[:punct:]]","_","all");
	</cfscript>
	<!--- Check that the SKU group does not exist. --->
	<cfquery NAME="productSKUcheckQry" DATASOURCE="#application.SiteDataSource#">
		SELECT SKUGROUP FROM PRODUCT WHERE
			SKUGROUP =  <cf_queryparam value="#arguments.productData.SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
			CAMPAIGNID =  <cf_queryparam value="#arguments.productData.campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<cfif productSKUcheckQry.recordcount>
		<!--- SKU exists, so fail the insert. --->
		<cfreturn "">
	</cfif>
	
	<!--- Get the set of currencies for which we need to create products. --->
	<cfset currencySetQry = getPromtionGlobalSKUCurrencies (campaignID = productData.campaignID)>
	<CFSET FreezeDate = request.requestTimeODBC>
	<!--- Now loop across the set of currencies and set up the product in each. --->
	<cfloop query="currencySetQry">
		<cfset listPrice = val(evaluate("arguments.productData.listPrice#currencySetQry.countryCurrency#"))>
		<cfset discountPrice = val(evaluate("arguments.productData.discountPrice#currencySetQry.countryCurrency#"))>
		<CFQUERY NAME="InsertNewProduct" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO Product(
				CampaignID,
	                    CountryID,
					    SKU,
						deliveryDays,
					    Description,
					    DiscountPrice,
					    ListPrice,
					    Deleted,
					    distiDiscount,
					    resellerDiscount,
					    sortOrder,
					    screenId,
					    maxQuantity, 
						productGroupID, 
						priceISOCode, 
						SKUGroup,
						ProductType,
						RemoteProductID,
						COGS,
						inTradeUp,
						createdBy, 
						created) 
				VALUES(
					<cf_queryparam value="#arguments.productData.CampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >,
			       0, 
				   <cf_queryparam value="#trim(arguments.productData.SKU)#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
				   <cf_queryparam value="#arguments.productData.deliveryDays#" CFSQLTYPE="CF_SQL_INTEGER" >,
				   <cf_queryparam value="#trim(arguments.productData.Description)#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
				   <cf_queryparam value="#discountPrice#" CFSQLTYPE="CF_SQL_NUMERIC" >,
				   <cf_queryparam value="#listPrice#" CFSQLTYPE="CF_SQL_NUMERIC" >, 
				   0,
	 		       0, 
				   0, 
				   <cf_queryparam value="#arguments.productData.sortOrder#" CFSQLTYPE="CF_SQL_INTEGER" >, 
	     		   <CFIF NOT IsDefined("arguments.productData.screenid")>NULL,<CFELSEIF arguments.productData.screenId EQ ''>NULL,<CFELSE><cf_queryparam value="#arguments.productData.screenid#" CFSQLTYPE="CF_SQL_INTEGER" >,</CFIF>
	    		   <CFIF IsDefined("arguments.productData.maxquantity")><cf_queryparam value="#arguments.productData.maxQuantity#" CFSQLTYPE="CF_SQL_INTEGER" > ,<CFELSE>'0',</CFIF>
			       <cf_queryparam value="#arguments.productData.frmproductGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				   <cf_queryparam value="#currencySetQry.countryCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
				   <cf_queryparam value="#arguments.productData.skugroup#" CFSQLTYPE="CF_SQL_VARCHAR" >,
		   			<CFIF IsDefined("arguments.productData.ProductType")><cf_queryparam value="#arguments.productData.ProductType#" CFSQLTYPE="CF_SQL_INTEGER" >,<CFELSE>NULL,</CFIF>
					<CFIF IsDefined("arguments.productData.RemoteProductID")><cf_queryparam value="#arguments.productData.RemoteProductID#" CFSQLTYPE="CF_SQL_INTEGER" >,<CFELSE>NULL,</CFIF>
					<CFIF IsDefined("arguments.productData.COGS")><cf_queryparam value="#arguments.productData.COGS#" CFSQLTYPE="CF_SQL_NUMERIC" ><CFELSE>0</CFIF>,					
					<CFIF IsDefined("arguments.productData.inTradeUp")><cf_queryparam value="#arguments.productData.inTradeUp#" CFSQLTYPE="CF_SQL_INTEGER" >,<CFELSE>0,</CFIF>
				   <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				   <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >) 
		</CFQUERY>
			
	
	</cfloop>
	
 	<CFIF arguments.productData.InventoryID NEQ 0>
	
		<CFQUERY NAME="InsertInventory" DATASOURCE="#Application.SiteDataSource#">
			INSERT INTO Inventory(SKU, NumberInStock, InventoryID)
			VALUES(<cf_queryparam value="#SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#NumberInStock#" CFSQLTYPE="CF_SQL_Integer" >, <cf_queryparam value="#InventoryID#" CFSQLTYPE="CF_SQL_INTEGER" >)

		</CFQUERY>

	</CFIF> 
	<cfreturn trim(arguments.productData.SKU)>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Updates a Global product using the structure. It is expected that 
	the productData structure is the FORM struct.
	
	It returns the SKU Group.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="GlobalSKUUpdateProduct" output="Yes">
	<cfargument name="productData" required="Yes" type="struct">
	<!--- Get the set of related files. --->
	<CFQUERY NAME="getRelatedFile" DATASOURCE="#application.SiteDataSource#">
		SELECT NULL
		  FROM RelatedFile ref
		      ,RelatedFileCategory rfc
		 WHERE ref.EntityId =  <cf_queryparam value="#ProductId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		   AND ref.FileCategoryId = rfc.FileCategoryId
		   AND rfc.FileCategoryEntity = 'Product'
		   AND (ref.filename LIKE '%gif' OR ref.filename LIKE '%jpg')
	</CFQUERY>	
	<!--- Update all the verisions of the products with the description information using the SKUGroup. --->
	<CFQUERY NAME="ProductUpdate" DATASOURCE="#application.SiteDataSource#">
	UPDATE Product
	SET deliveryDays =  <cf_queryparam value="#arguments.productData.deliveryDays#" CFSQLTYPE="CF_SQL_Integer" > ,
		Description =  <cf_queryparam value="#arguments.productData.Description#" CFSQLTYPE="CF_SQL_VARCHAR" > , 
		sortOrder =  <cf_queryparam value="#arguments.productData.sortOrder#" CFSQLTYPE="CF_SQL_INTEGER" > , 
		<CFIF NOT IsDefined("arguments.productData.screenid") OR not len(arguments.productData.screenid)>
			screenid = NULL,
		<CFELSE>
			screenId =  <cf_queryparam value="#arguments.productData.screenid#" CFSQLTYPE="CF_SQL_INTEGER" > , 
		</CFIF>
		<CFIF IsDefined("arguments.productData.maxquantity")>
			maxQuantity =  <cf_queryparam value="#arguments.productData.maxQuantity#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		<CFELSE>
			maxQuantity = '0',	
		</CFIF>
		<CFIF getRelatedFile.RecordCount GT 0>
			IncludeImage = 1,
		<CFELSE>
			IncludeImage = 1,			
		</CFIF>		
		<CFIF IsDefined("arguments.productData.ProductType")>
			ProductType =  <cf_queryparam value="#arguments.productData.ProductType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,			
		</CFIF>
		<CFIF IsDefined("arguments.productData.RemoteProductID")>
			RemoteProductID =  <cf_queryparam value="#arguments.productData.RemoteProductID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,			
		</CFIF>
		<CFIF IsDefined("arguments.productData.COGS")>
			COGS =  <cf_queryparam value="#arguments.productData.COGS#" CFSQLTYPE="cf_sql_float" > ,			
		</CFIF>		
		<CFIF IsDefined("arguments.productData.inTradeUp")>
			inTradeUp =  <cf_queryparam value="#arguments.productData.inTradeUp#" CFSQLTYPE="CF_SQL_bit" > ,		
		<cfelse>
			inTradeUp = 0,	
		</CFIF>
		deleted =  <cf_queryparam value="#arguments.productData.deleted#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		productGroupID =  <cf_queryparam value="#arguments.productData.frmproductGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
	    lastUpdatedBy = #request.relayCurrentUser.usergroupid#,
	    lastUpdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	WHERE SKUGroup =  <cf_queryparam value="#arguments.productData.SKUgroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND campaignID =  <cf_queryparam value="#arguments.productData.campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<!--- For each 'global' product version update the specified price. Please note it assumes no knowledge of global. --->
	<cfscript>
		currencySetQry = getPromtionGlobalSKUCurrencies (campaignID = arguments.productData.campaignID);
	</cfscript>
	<cfloop query="currencySetQry">
		<cfquery name="updProductQry" datasource="#application.SiteDataSource#">
			UPDATE product set 
				listprice = <cf_queryparam value="#val(evaluate("arguments.productData.listPrice#currencySetQry.countryCurrency#"))#" CFSQLTYPE="CF_SQL_NUMERIC" >  ,
				discountprice = <cf_queryparam value="#val(evaluate("arguments.productData.discountprice#currencySetQry.countryCurrency#"))#" CFSQLTYPE="CF_SQL_NUMERIC" >   
				where campaignID =  <cf_queryparam value="#arguments.productData.campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >  
					AND skugroup =  <cf_queryparam value="#trim(arguments.productData.SKUgroup)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					AND countryID = 0
					AND priceISOCode =  <cf_queryparam value="#currencySetQry.countryCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 		
		</cfquery>
	</cfloop>
	<!--- Delete the current set of parentProducts. --->
	<cfquery name="delParProdqry" datasource="#application.SiteDataSource#">
		delete from ProductParentsGlobalSKU 
			where campaignID =  <cf_queryparam value="#arguments.productData.campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND
				ProductSKUGroup =  <cf_queryparam value="#arguments.productData.SKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	<cfif isdefined("arguments.productData.parentProducts")>
		<cfloop list="#arguments.productData.parentProducts#" index="parProdIdx">
			<cfquery name="insParProdqry" datasource="#application.SiteDataSource#">
				insert into ProductParentsGlobalSKU (campaignID,ProductSKUGroup,ParentSKUGroup)
					VALUES (
						<cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#arguments.productData.SKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#parProdIdx#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfquery>
		</cfloop>
	
	
	</cfif>
	
</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Updates a Global product file set. It is expected that 
	the productData structure is the FORM struct.
	
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="uploadGlobalProductFiles">
	<cfargument name="productData" required="Yes" type="struct">
	
	<CFSET thisPath = application.UserFilesAbsolutePath>
	
	<CFIF isDefined('form.frmUploadThumbnail') and Trim(FORM.frmUploadThumbnail) is not "">
		<cfset directoryPath = "#thisPath#Content\ProductThumbs\CountryID0\">
		<cfif not directoryexists(directoryPath)>
			<cfdirectory action="create" directory="#directoryPath#">
		</cfif>
	
		<cffile action="UPLOAD"
		        filefield="frmUploadThumbnail"
		        destination="#directoryPath##promoID#-#replace(replace(replace(replace(arguments.productData.SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg"
		        nameconflict="OVERWRITE">
	</CFIF>
	<CFIF isDefined('form.frmUploadHiRes') and Trim(FORM.frmUploadHiRes) is not "">
	
		<cfset directoryPath = "#thisPath#Content\ProductPhotoHires\CountryID0\">
		<cfif not directoryexists(directoryPath)>
			<cfdirectory action="create" directory="#directoryPath#">
		</cfif>
	
		<cffile action="UPLOAD"
		        filefield="frmUploadHiRes"
		        destination="#directoryPath##promoID#-#replace(replace(replace(replace(arguments.productData.SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg"
		        nameconflict="OVERWRITE">
	</CFIF>
	<CFIF isDefined('form.frmUploadPDF') and Trim(FORM.frmUploadPDF) is not "">
	
		<cffile action="UPLOAD"
		        filefield="frmUploadPDF"
		        destination="#thisPath#Content\ProductPDF\#promoID#-#replace(replace(replace(replace(arguments.productData.SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.pdf"
		        nameconflict="OVERWRITE">
	</CFIF>



</cffunction>

<cffunction name="updateGlobalProductCountryData">
	<cfargument name="campaignID" required="Yes" type="numeric">
	<cfargument name="SKUGroup" required="Yes">
	<cfargument name="countryID" required="Yes" type="numeric">
	<cfargument name="suppressed" required="Yes" type="numeric">
	<cfargument name="listprice" required="Yes">
	<cfargument name="discountPrice" required="Yes">
	<cfargument name="SKU" required="No" default="">
	<cfset var productID = "">
	<!--- Check if product exists for country --->
	<cfquery name="productExistsQry"  datasource="#application.SiteDataSource#">
		select productID from product where 
			SKUgroup =  <cf_queryparam value="#arguments.SKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
			countryID = #arguments.countryID# AND
			campaignID = #arguments.campaignID#
	
	</cfquery>
	
	<cfif not productExistsQry.recordcount>
		<!--- Copy from the 'raw data' and create a new instance for the country--->
		<cfscript>
			productID = application.com.relayEcommerce.createCountrySpecificGlobalProduct(campaignID = arguments.campaignID, skugroup = arguments.skugroup, countryID = arguments.countryID);			
		</cfscript>
	<cfelse>
		<!--- Product found for the country. --->
		<cfset productID = productExistsQry.productID>
	</cfif>
	
	<!--- Now update the values.  --->
	<cfquery name="productUpdQry"  datasource="#application.SiteDataSource#">
		update product set 
			deleted = #arguments.suppressed#,
			listprice  =  <cf_queryparam value="#val(arguments.listprice)#" CFSQLTYPE="cf_sql_numeric" >,
			discountPrice =  <cf_queryparam value="#val(discountPrice)#" CFSQLTYPE="cf_sql_float" > ,
			<cfif len(trim(arguments.SKU))>
			SKU =  <cf_queryparam value="#trim(arguments.SKU)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			</cfif>
			lastupdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			lastupdatedby = #request.relaycurrentuser.personid#
			where productID =  <cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	

</cffunction>



<cffunction name="createCountrySpecificGlobalProduct" returntype="numeric">
	<cfargument name="campaignID" required="Yes" type="numeric">
	<cfargument name="SKUgroup" required="Yes">
	<cfargument name="countryID" required="Yes" type="numeric">
	<cfscript>
		// Get the country currency we need to use.
		var productID = '';
		var countryCurrencyQry = GetPromotionGlobalSKURegions(campaignID = arguments.campaignID,countryID = arguments.countryID);
		
	</cfscript>
	<cfif countryCurrencyQry.recordcount>
		<!--- Get the productID for the 'base' product. --->
		<cfquery name="productInsQry"	datasource="#application.SiteDataSource#">
			insert into product (
				CampaignID,
				SKU,
				Deleted,
				Description,
				CountryID,
				SortOrder,
				ListPrice,
				PriceISOCode,
				DiscountPrice,
				HandlingFee,
				ShippingMethod,
				GroupID,
				ProductGroupID,
				SKUGroup,
				ProdWeight,
				InTradeUp,
				ProductType,
				screenID,
				includeImage,
				maxQuantity,
				DistiDiscount,
				ResellerDiscount,
				created,
				createdby,
				lastupdated,
				lastupdatedby,
				status,
				COGS,
				deliveryDays,
				remoteProductID,
				IsParent
			) select CampaignID,
				SKU,
				Deleted,
				Description,
				<cf_queryparam value="#arguments.CountryID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				SortOrder,
				ListPrice,
				PriceISOCode,
				DiscountPrice,
				HandlingFee,
				ShippingMethod,
				GroupID,
				ProductGroupID,
				SKUGroup,
				ProdWeight,
				InTradeUp,
				ProductType,
				screenID,
				includeImage,
				maxQuantity,
				DistiDiscount,
				ResellerDiscount,
				<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
				status,
				COGS,
				deliveryDays,
				remoteProductID,
				IsParent from product 
					where campaignID = #arguments.campaignID# AND
						skugroup =  <cf_queryparam value="#trim(arguments.skugroup)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
						AND countryID = 0 and priceISOcode =  <cf_queryparam value="#countryCurrencyQry.countryCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			
			SELECT scope_identity() AS productID
		
		</cfquery>
		<cfset productID = productInsQry.productID >
	
	</cfif>
	<cfreturn productID>

</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
           Get a product ID                
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="getGlobalSkuProductID" returntype="query">
	<cfargument name="campaignID" required="Yes" type="numeric">
	<cfargument name="skuGroup" required="Yes">
	
	<cfset var prodQry = "">
	<!---  --->
	<cfquery name="prodQry" datasource="#application.SiteDataSource#">
		select productID from product where
			campaignID = #arguments.campaignID# and
			skuGroup =  <cf_queryparam value="#trim(arguments.skuGroup)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			and countryID = 0	
	
	</cfquery>
	<cfreturn prodQry>

</cffunction>

<cffunction name="getLocalProductPrice" returntype="query">
	<cfargument name="campaignID" required="Yes" type="numeric">
	<cfargument name="skuGroup" required="Yes">
	<cfargument name="countryID" required="Yes">
	<cfargument name="countryCurrency" required="Yes">	
	
	<cfset var prodQry = "">
	<cfquery name="prodQry" datasource="#application.SiteDataSource#">
		select listprice,discountPrice,priceISOcode from product where
			campaignID = #arguments.campaignID# and
			skuGroup =  <cf_queryparam value="#trim(arguments.skuGroup)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			and countryID = 0	
			and priceISOcode =  <cf_queryparam value="#arguments.countryCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	
	</cfquery>
	<cfreturn prodQry>

</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             UPDATE Order Status                    
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updOrderStatus" hint="Updates the Status of an opportunity">
		<cfargument name="orderID" required="Yes" type="numeric">
		<cfargument name="OrderStatus" required="Yes" type="numeric">
	
		<cfquery name="qryupdOrderStatusQry" datasource="#application.SiteDataSource#">
			Update Orders
			set OrderStatus=#arguments.OrderStatus#,
			lastupdatedby = #request.relayCurrentUser.personID#,
			lastUpdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			WHERE orderID=#arguments.orderID#
		</cfquery>
		
	</cffunction>
	
	<cffunction name="getOrderDetailsByID" access="public" returntype="query" hint="Returns a query object containing data for a order filtered by orderID">
		
		<cfargument name="OrderID" type="numeric" required="yes">
		
		<cfscript>
			var qryOrderDetails = "";
		</cfscript>		
		
		<cfquery name="qryOrderDetails" datasource="#application.SiteDataSource#">
			select o.*, l.countryid from Orders O INNER JOIN location l on o.locationID = l.locationID
			where OrderID=#OrderID#
		</cfquery>
		
		<cfreturn qryOrderDetails>
	</cffunction>
	
	<cffunction name="getOrderItemsByID" access="public" returntype="query" hint="Returns a query object containing orderitems for a order filtered by orderID">
		
		<cfargument name="OrderID" type="numeric" required="yes">
		
		<cfscript>
			var qryOrderItesm = "";
		</cfscript>
		
		<cfquery name="qryOrderItesm" datasource="#application.SiteDataSource#">
			select * from Orderitem
			where OrderID=#OrderID#
		</cfquery>
		
		<cfreturn qryOrderItesm>
	</cffunction>
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             DELETE Order                    
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="delOrder" hint="Deletes a order">
		<cfargument name="OrderID" type="numeric" required="yes">
		
		<cfscript>
			var DeleteOrders = "";
			var DeleteOrderitems = "";
		</cfscript>
		
		<cfquery name="DeleteOrders" datasource="#application.SiteDataSource#">
			DELETE FROM Orders
			WHERE orderID=#orderID#
		</cfquery>
		<cfquery name="DeleteOrderitems" datasource="#application.SiteDataSource#">
			DELETE FROM Orderitem
			WHERE orderID=#orderID#
		</cfquery>
		
	</cffunction>
	
	
<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Return the set of valid order statuses                  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOrderStatuses" hint="Returns a query of order statuses" returntype="query">
		<cfargument name="statusIDList" default="" required="no">
		<cfscript>
			var statusQry = "";
		</cfscript>		
		
		<cfquery name="statusQry" datasource="#application.SiteDataSource#">
			select statusID, description as status FROM status
			<cfif len(arguments.statusIDList)>
				WHERE statusID  in ( <cf_queryparam value="#arguments.statusIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		</cfquery>
		<cfreturn statusQry>
		
	</cffunction>
	
	
<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			Returns the set of available parent products based on the campaign ID and the current SKU
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getAvailableParentProducts" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="skugroup" required="Yes">
		<cfscript>
			var availableProdQry = "";
		</cfscript>		
		
			<cfquery name="availableProdQry" datasource="#application.SiteDataSource#">
				select distinct(skuGroup),sku, description
					from product 
					where campaignID = #arguments.campaignID# and countryID = 0
						and skugroup not in( 
							select distinct(ProductSkuGroup) from ProductParentsGlobalSKU 
								where campaignID = #arguments.campaignID#) 
						and skugroup <>  <cf_queryparam value="#arguments.skugroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
			<cfreturn availableProdQry>	
	</cffunction>
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			Returns the set of parent products based on the campaign ID and the current SKU
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductParents" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="skugroup" required="Yes">
			<cfset var availableProdQry = "">
			<cfquery name="availableProdQry" datasource="#application.SiteDataSource#">
				select distinct(skuGroup),sku, description
					from product where  campaignID = #arguments.campaignID#   and countryID = 0
						and skugroup in (
						select distinct(ParentSKUGroup)
							from ProductParentsGlobalSKU 
								where ProductSkuGroup =  <cf_queryparam value="#arguments.skugroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  
								and campaignID = #arguments.campaignID#)
			</cfquery>
			<cfreturn availableProdQry>	
	</cffunction>
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			Returns the set of available parent products based on the campaign ID and the current SKU
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductChildren" returntype="query">
		<cfargument name="campaignID" required="Yes" type="numeric">
		<cfargument name="skugroup" required="Yes">
			<cfset var availableProdQry = "">
			<cfquery name="availableProdQry" datasource="#application.SiteDataSource#">
				select distinct(skuGroup),sku, description
					from product where  campaignID = #arguments.campaignID#   and countryID = 0
						and skugroup in (
						select distinct(productSKUGroup)
							from ProductParentsGlobalSKU 
								where ParentSkuGroup =  <cf_queryparam value="#arguments.skugroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  
								and campaignID = #arguments.campaignID#)
			</cfquery>
			<cfreturn availableProdQry>	
	</cffunction>
	
	
	
	
<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Returns the orderID of an order that has just been created from an opportunity   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	
	<cffunction name="convertOppToOrder" access="public" returntype="numeric" hint="Returns the orderID of an order that has just been created from an opportunity">
		<cfargument name="opportunityID" required="yes" type="numeric">
		<cfscript>
			var convertOppToOrder = "";
			var myopportunity = "";
			var qOppProducts = "";

			myopportunity = createObject("component","relay.com.opportunity");
			myopportunity.dataSource = application.sitedataSource;
			myopportunity.opportunityID = #arguments.opportunityID#;
			
			oppDetails = myopportunity.getOpportunityDetails(opportunityID=#arguments.opportunityID#);
			
			//qOppProducts = myopportunity.getOppProducts(countryID=#oppDetails.countryid#);
			myopportunity.getOppProducts(countryID=#oppDetails.countryid#);
			
			personDetails = application.com.commonQueries.getFullPersonDetails(personID = #oppDetails.partnerSalesPersonID#);
			countryDetails = application.com.commonQueries.getCountry(countryID=#oppDetails.countryid#);
		</cfscript>
		
		<!--- this gets the value of the constant constQuote for the orderStatus value--->
		<cfinclude template="/Orders/applicationOrdersIni.cfm">
		
		<!--- this sets the productCatalogueCampaignID variable, which we're using for our promoID --->
		<cfinclude template="/code/cftemplates/opportunityINI.cfm">
		<!--- now that productCatalogueCampaignID is in settings we probably no longer need to include opportunityINI --->
		<cfset productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID")>
		
		<cfquery name="getPromoID" datasource="#application.SiteDataSource#">
			select promoID from promotion where campaignID =  <cf_queryparam value="#productCatalogueCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfset freeze= #now()#>
		
		<cftransaction>
			<cfquery name="convertOppToOrder" datasource="#application.SiteDataSource#">
				BEGIN
					declare @newOrderID int
					
					select @newOrderID = max(orderID)+1 from orders
					
					Insert into orders
					(
						OrderID,
						Orderdate, 
						LocationID, 
						PersonID, 
						OrderStatus, 
						OrderCurrency, 
						CreatedBy, 
						Created, 
						LastUpdatedBy, 
						LastUpdated,
						delcontact,
						delsitename,
						<CFLOOP index="i" from="1" to="5">
							deladdress#i#,	
						</CFLOOP>
						delpostalcode,
						delcountryid,
					 	contactphone,
						contactfax,
						contactemail,
						invcontact,
						invsitename,
						<CFLOOP index="i" from="1" to="5">
							invaddress#i#,	
						</CFLOOP>
						invpostalcode,
						invcountryid,
						promoID,
						shippingCharge,
						taxRate
						
					) values (
					
						@newOrderID,
						#freeze#,
						#oppDetails.partnerLocationID#,
						#oppDetails.partnerSalesPersonID#,
						#constQuote#,
						'#oppDetails.currency#',
						#request.relayCurrentUser.PersonID#, 
						#freeze#, 
						#request.relayCurrentUser.PersonID#, 
						#freeze#,
						'#personDetails.fullname#',
						'#personDetails.sitename#',
						<CFLOOP index="i" from="1" to="5">
							<cfset tmpAddress = evaluate("personDetails.address#i#")>
							'#tmpAddress#',	
						</CFLOOP>
						'#personDetails.postalcode#',
						#oppDetails.countryid#,
					 	'#personDetails.directLine#',
						'#personDetails.faxphone#',
						'#personDetails.email#',
						'#personDetails.fullname#',
						'#personDetails.sitename#',
						<CFLOOP index="i" from="1" to="5">
							<cfset tmpAddress = evaluate("personDetails.address#i#")>
							'#tmpAddress#',	
						</CFLOOP>
						'#personDetails.postalcode#',
						#oppDetails.countryid#,
						'#getPromoID.promoID#',
						0,
						#countryDetails.VatRate#
					)
					
					<cfset orderListValue = 0>
					<cfset orderValue = 0>
					<cfif IsQuery(myopportunity.getOppProducts)>
						<cfloop query="myopportunity.getOppProducts">
 							<cfif isDefined("specialPrice") and specialPrice gt 0>
								<cfset unitDiscountPrice = #specialPrice#>
							<cfelse>
								<cfset unitDiscountPrice = #unitPrice#>
							</cfif>
							<cfset totalDiscountPrice = #unitDiscountPrice# * #quantity#>
							<cfset totalListPrice = #unitPrice# * #quantity#>
							<cfset orderValue = #totalDiscountPrice# + #orderValue#>
							<cfset orderListValue = #totalListPrice# + #orderListValue#>
							
							insert into orderItem
							(
								orderID,
								sku,
								quantity,
								productID,
								created,
								createdBy,
								lastUpdated,
								lastUpdatedBy,
								unitDiscountPrice,
								totaldiscountprice,
								unitListPrice,
								totalListPrice,
								active,
								priceISOCode
							) values (
								@newOrderID,
								'#sku#',
								#quantity#,
								#productID#,
								#freeze#,
								#request.relayCurrentUser.personID#,
								#freeze#,
								#request.relayCurrentUser.personID#,
								#unitDiscountPrice#,
								#totalDiscountPrice#,
								#unitPrice#,
								#totalListPrice#,
								1,
								'#oppDetails.currency#'
							)
						</cfloop>
					</cfif>
							
					update orders set 
						orderListvalue =  <cf_queryparam value="#orderListValue#" CFSQLTYPE="CF_SQL_money" > , 
						orderValue =  <cf_queryparam value="#orderValue#" CFSQLTYPE="cf_sql_numeric" >, 
						<cfset var tax = orderValue * countryDetails.VatRate /100 >
						taxOnOrder = <cf_queryparam value="#tax#" CFSQLTYPE="cf_sql_numeric" >
					where orderID = @newOrderID
					
					update opportunity set orderID = @newOrderID where opportunityID = <cf_queryparam value="#opportunityID#" CFSQLTYPE="cf_sql_integer" >
					
					select max(orderID) as orderID from orders
				
				END
		
			</cfquery>
		</cftransaction>
		
		<cfreturn convertOppToOrder.orderID>
		
	</cffunction>
	
	
	
</cfcomponent>
