<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			oppProductSets.cfc
Author:				AJC
Date started:		2005-06-08

Description:		Product Set functions for opportunities

Orginal Code:		CR_ATI010

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:
 --->
<cfcomponent displayname="oppProductSets" hint="Functions for opportunity Product sets">
	<cfif isDefined("application.siteDataSource")>  <!--- when reloaded remotely application is not necessarily available, will be populated by an initialise function instead  --->
		<cfparam name="datasource" default="#application.siteDataSource#">
	</cfif>


	<cffunction name="get" access="public" returntype="query" hint="This gets the opportunity products sets.">
		<!--- Passed arguments to this function --->
		<cfargument name="ProductSetID" type="string" default="">
		<cfargument name="whereclause" type="string" default="">
		<!--- local variables --->
		<cfset qry_getProductSet="">
		<!--- Call stored proceedure to get product sets --->
		<cfstoredproc procedure="sp_getProductSet" datasource="#datasource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_ProductSetID" value="#ProductSetID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_whereclause" value="#whereclause#">
			<cfprocresult name="qry_getProductSet">
		</cfstoredproc>
		<!--- return the record set --->
		<cfreturn qry_getProductSet>
	</cffunction>

	<cffunction name="getProductSetItems" access="public" returntype="query" hint="This gets the product set product list">
		<!--- Passed arguments to this function --->
		<cfargument name="ProductSetID" type="string" default="">
		<cfargument name="whereclause" type="string" default="">
		<!--- local variables --->
		<cfset qry_getProductSetItems="">
		<!--- Call stored proceedure to get product set Itemss --->
		<cfstoredproc procedure="sp_getProductSetItems" datasource="#datasource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_ProductSetID" value="#ProductSetID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_whereclause" value="#whereclause#">
			<cfprocresult name="qry_getProductSetItems">
		</cfstoredproc>
		<!--- return the record set --->
		<cfreturn qry_getProductSetItems>
	</cffunction>

	<cffunction name="addProductSetToOpportunity" access="public" hint="Adds a product set to an opportunity">
		<!--- Passed arguments to this function --->
		<cfargument name="opportunityID" type="string" default="">
		<cfargument name="ProductSetID" type="string" default="">
		<!--- Call stored proceedure to get product set Itemss --->
		<cfstoredproc procedure="sp_addProductSetToOpportunity" datasource="#datasource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_opportunityID" value="#opportunityID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_ProductSetID" value="#ProductSetID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="in_CreatedBy" value="#Request.RelayCurrentUser.PersonID#">
		</cfstoredproc>
		<!--- return the record set --->
	</cffunction>

	<cffunction name="getProductSetsWithRights" access="public" hint="Adds a product set to an opportunity with the shipment dates">
		<!--- Passed arguments to this function --->
		<cfargument name="ProductSetID" type="any">

		<!--- get product sets --->
		<cfquery name="qryGetProductSetsWithRights" datasource="#datasource#">
			SELECT *
			FROM	ProductSet ps, recordrights rr, rightsgroup rg
			WHERE	ps.productSetID = rr.recordid AND
					rr.entity = N'productSet' AND
					rr.usergroupid = rg.usergroupid AND
					rg.personID = #Request.RelayCurrentUser.personID#
		</cfquery>

		<cfreturn qryGetProductSetsWithRights>

	</cffunction>

	<cffunction name="addProductSetWithDatesToOpportunity" access="public" hint="Adds a product set to an opportunity with the shipment dates">
		<!--- Passed arguments to this function --->
		<cfargument name="opportunityID" type="any">
		<cfargument name="ProductSetID" type="any">
		<cfargument name="shipmentDate" type="any">
		<!--- get products in product set --->

		<cfquery name="qryPSProducts" datasource="#datasource#">
			SELECT	p.*, psi.Quantity AS quantity, psi.ProductSetID AS productsetid
			FROM	Product p INNER JOIN
					ProductSetItem psi ON psi.ProductID = p.ProductID
			WHERE	psi.ProductSetID =  <cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<!--- loop through products and add them to opportunity. --->
		<cfloop query="qryPSProducts">
			<cfif listprice is "">
				<cfset lPrice = 0>
			<cfelse>
				<cfset lPrice = listprice>
			</cfif>
			<cfquery name="qryOppProbability" datasource="#datasource#">
				SELECT	probability
				FROM	opportunity
				WHERE	opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset oppProbability = 100>
			<cfif qryOppProbability.recordcount gt 0>
				<cfset oppProbability = qryOppProbability.probability>
			</cfif>

			<cfquery name="qryAddProductToOpp" datasource="#datasource#">
				INSERT INTO OpportunityProduct
				(OpportunityID,ProductORGroupID,ProductORGroup,quantity,unitPrice,createdby,lastupdatedby,probability,specialPrice,forecastShipDate)
				VALUES
				(<cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" >,'p',
				<cf_queryparam value="#quantity#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#lPrice#" CFSQLTYPE="cf_sql_float" >,
				<cf_queryparam value="#Request.RelayCurrentUser.PersonID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#Request.RelayCurrentUser.PersonID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#oppProbability#" CFSQLTYPE="CF_SQL_Integer" >,0,
				<cf_queryparam value="#CreateODBCDateTime(shipmentDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</cfquery>
		</cfloop>

	</cffunction>

	<cffunction name="addProductToProductSet" access="public" hint="Adds product to product set">
		<!--- Passed arguments to this function --->
		<cfargument name="productSetID" type="any">
		<cfargument name="productID" type="any">
		<cfargument name="quantity" type="any">

		<!--- check for this product in the db first --->

		<cfquery name="qryGetProductSetProducts" datasource="#datasource#">
			SELECT	ProductSetItemID
			FROM	ProductSetItem
			WHERE	ProductSetID =  <cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND
					ProductID =  <cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif qryGetProductSetProducts.recordCount gt 0>
			<!--- replace value for product assigned to productset. --->
			<cfquery name="qryUpdateProductSetProduct" datasource="#datasource#">
				UPDATE	ProductSetItem
				SET		Quantity =  <cf_queryparam value="#quantity#" CFSQLTYPE="CF_SQL_INTEGER" >
				WHERE	ProductSetItemID =  <cf_queryparam value="#qryGetProductSetProducts.productSetItemID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		<cfelse>
			<!--- add product to productset --->
			<cfquery name="qryAddProductToProductSet" datasource="#datasource#">
				INSERT INTO ProductSetItem
					(ProductSetID,ProductID,Quantity)
				VALUES
					(<cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#quantity#" CFSQLTYPE="CF_SQL_INTEGER" >)
			</cfquery>
		</cfif>
	</cffunction>

	<cffunction name="deleteProductFromProductSet" access="public" hint="Deletes product from product set">
		<!--- Passed arguments to this function --->
		<cfargument name="productSetID" type="any">
		<cfargument name="productID" type="any">

		<!--- delete product from product set --->

		<cfquery name="qryGetProductSetProducts" datasource="#datasource#">
			DELETE FROM	ProductSetItem
			WHERE	ProductSetID =  <cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND
					ProductID =  <cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	</cffunction>

	<cffunction name="removeProductSetFromUser" access="public" hint="Removes rights to Product Set from specific user.">
		<!--- Passed arguments to this function --->
		<cfargument name="productSetID" type="any">

		<!--- remove product set from user --->
		<cfquery name="qryGetProductSetProducts" datasource="#datasource#">
			DELETE FROM	recordrights
			WHERE	Entity = N'productSet' AND
					RecordID =  <cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND
					UserGroupID = #Request.RelayCurrentUser.usergroupid#
		</cfquery>
	</cffunction>

	<cffunction name="createProductSet" access="public" hint="create a product set">
		<!--- Passed arguments to this function --->
		<cfargument name="productSetName" type="any">
		<cfargument name="products" type="any">

		<cfset var createdDateTime = CreateODBCDateTime(now())>
		<!--- create product set --->
		<cfquery name="qryCreateProductSet" datasource="#datasource#">
			INSERT INTO ProductSet
				(productsetname,created,createdby,lastupdated,lastupdatedby)
			VALUES
				(<cf_queryparam value="#productSetName#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#createdDateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#Request.RelayCurrentUser.PersonID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#createdDateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#Request.RelayCurrentUser.PersonID#" CFSQLTYPE="CF_SQL_VARCHAR" >)
		</cfquery>
		<!--- get productSetID --->
		<cfquery name="qryGetProductSetID" datasource="#datasource#">
			SELECT	ProductSetID
			FROM	ProductSet
			WHERE	productsetname =  <cf_queryparam value="#productSetName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND
					created =  <cf_queryparam value="#createdDateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND
					createdby = '#Request.RelayCurrentUser.PersonID#'
		</cfquery>

		<cfif qryGetProductSetID.recordCount neq 0>
			<cfloop list="#products#" index="productDetail">
				<cfif listLen(productDetail,"~") gt 1>
					<cfscript>
						addProductToProductSet(qryGetProductSetID.ProductSetID,listGetAt(productDetail,1,"~"),listGetAt(productDetail,2,"~"));
					</cfscript>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn qryGetProductSetID.productSetID>
	</cffunction>

	<cffunction name="createProductSetWithUser" access="public" hint="create a product set with user">
		<!--- Passed arguments to this function --->
		<cfargument name="productSetName" type="any">
		<cfargument name="products" type="any">
		<cfargument name="userRight" type="any">

		<cfset newProductSetID = createProductSet(productSetName,products)>

		<cfquery name="qrySetUserRights" datasource="#datasource#">
			INSERT INTO RecordRights
				(UserGroupID,Entity,RecordID,Permission)
			VALUES
				(<cf_queryparam value="#Request.RelayCurrentUser.UserGroupID#" CFSQLTYPE="CF_SQL_Integer" >,
				'productSet',
				<cf_queryparam value="#newProductSetID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#userRight#" CFSQLTYPE="CF_SQL_Integer" >)
		</cfquery>

		<cfreturn newProductSetID>

	</cffunction>

	<cffunction name="updateProductSet" access="public" hint="update a product set">
		<!--- Passed arguments to this function --->
		<cfargument name="productSetID" type="any">
		<cfargument name="productSetName" type="any">
		<cfargument name="products" type="any">

		<cfset var createdDateTime = CreateODBCDateTime(now())>
		<!--- update product set --->
		<cfquery name="qryCreateProductSet" datasource="#datasource#">
			UPDATE	ProductSet
			SET		productsetname =  <cf_queryparam value="#productSetName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					lastupdated =  <cf_queryparam value="#createdDateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					lastupdatedby = <cf_queryparam value="#Request.RelayCurrentUser.PersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			WHERE	ProductSetID =  <cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<!--- get productSetItems --->
		<cfquery name="qryGetProductSetItems" datasource="#datasource#">
			SELECT	*
			FROM	ProductSetItem
			WHERE	ProductSetID =  <cf_queryparam value="#productSetID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfset productsToDelete = valueList(qryGetProductSetItems.productID)>
		<cfloop list="#products#" index="productDetail">
			<cfset productPosition = listFind(productsToDelete,listGetAt(productDetail,1,"~"))>
			<cfif productPosition gt 0>
				<cfscript>
					productsToDelete = listDeleteAt(productsToDelete,productPosition);
				</cfscript>
			</cfif>
			<cfif listLen(productDetail,"~") gt 1>
				<cfscript>
					addProductToProductSet(ProductSetID,listGetAt(productDetail,1,"~"),listGetAt(productDetail,2,"~"));
				</cfscript>
			</cfif>
		</cfloop>
		<!--- delete products from set --->
		<cfif listLen(productsToDelete) gt 0>
			<cfloop list="#productsToDelete#" index="pID">
				<cfscript>
					deleteProductFromProductSet(ProductSetID,pID);
				</cfscript>
			</cfloop>
		</cfif>

	</cffunction>

	<cffunction name="deleteProductSet" access="public" hint="Adds a product set to an opportunity">
		<!--- Passed arguments to this function --->
		<cfargument name="ProductSetID" type="string" default="">
		<!--- Delete Product Set Items for this ProductSetID --->
		<cfquery name="qryDeleteProductSetItems" datasource="#datasource#">
			delete from ProductSetItem
			where ProductSetID =  <cf_queryparam value="#ProductSetID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfquery name="qryDeleteProductSet" datasource="#datasource#">
			delete from ProductSet
			where ProductSetID =  <cf_queryparam value="#ProductSetID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	</cffunction>

	<!---
		function which will be run automatically when cfc is loaded into application.com
		needs to be able to run without application available
	 --->
	<cffunction access="public" name="initialise" hint="">
		<cfargument name="applicationScope" default = "#application#">
		<!--- Note that this is a global variable, do not var --->
		<cfset datasource = applicationScope.sitedatasource>
	</cffunction>

</cfcomponent>