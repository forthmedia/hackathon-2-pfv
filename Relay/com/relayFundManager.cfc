<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayFundManager.cfc
Author:				SWJ
Date started:		2004-04-29

Purpose:	central storage for incentive params

Usage:

Amendment History:

Date (YYYY/MM/YY)	Initials 	What was changed
2007/10/16			SSS			changed the history function to add when approval amounts are changed and paymentdates and payment ref.
								make sure that makeFundActivityPayment function is only called when things change
								Make sure that the history function is only called when things changed
2009-09-21			NYB			create function getAccountFromBudgetHolderID
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
2009-10-23			AJC			Add approval level to request
2010-02-25 			NAS 		P-LEX039 Add Partner Name
2010-03-17 			PPB 		P-LEX039 Add Partner Country To Query
2010-07-14 			AJC 		P-LEX039 Exclude Fund manager emails
2010-12-13 			NYB 		Added a missing 'not' to AJC's P-LEX039 Exclude Fund manager emails cfif
2011-01-20			PPB			REL099 changes for fundRequestActivityV2.cfm
2011/01/26 			PPB			LID3670
2011-02-15 			NYB 		LHID5662
2011/03/04 			PPB 		LID5807 -previously we were picking up the nextAvailableStatus from the first fundRequestStatusQry row which isn't necessarily the current status
2011/03/07 			PPB 		LID5794 - allow muliple fund account managers in a TwoSelectsCombo
2011/03/09			NAS			LID5846: Fund Manager - can add new activity against a fund in my company for which I have not been authorized
2011/03/24 			PPB 		LID6027 send an email to all fund managers not just currentuser
2011/03/24 			PPB 		LID6028 added merge fields
2011/03/24 			PPB 		LID6037 fields roi and typeDescription now Nvarchar
2011/03/29 			PPB 		LID5826 activity status translation
2011/03/29			PPB			LID5935 avoid crash if cofunder approval amount is blank
2011/03/30 			PPB 		LID5963 add link direct to request
2011/04/01 			PPB 		LID6101 new method getBudgetPeriodAllocations
2011/04/01			PPB			add getApprovalStatus
2011/04/07 			PPB 		no longer use getSetting("fundManager.approvalStatusIDList") - use the new table col isApproved
2011/04/07 			PPB 		LID6164 add ALL Fund Managers to list for Fully Approved email
2011/04/08 			PPB 		LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list
2011/04/19 			PPB			add sort order to status dropdown
2011/04/19 			PPB			getFundAccountsAvailableForActivity: added alias to column name cos now there is a conflict due to new field
2011/04/19 			PPB 		changed fundActivityQry.organisationName to fundActivityQry.fundAccountOrganisationName in several places
2011/04/19 			PPB 		changes to getBudgetPeriods_Dropdown to work for BudgetGroupPeriodAllocations
2011/04/19 			PPB 		added getFundApprovalStatusByTextID
2011/04/20 			AJC 		Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget
2011/04/27			AJC			LID6189 - Fund Request type is not translated
2011/04/28  		AJC			LID6081 - Fund Manager - Requests can be made for future period without an approved allocated budget
2011/05/04			AJC			Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests
2011/05/12 			PPB 		LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send
2011/05/16			PPB			changes refs to request.excludeEmailsToFundManager to getsetting
2011/05/23			ppb			also return partner_name from query in getFundActivityV2
2011-09-05 			NYB 		Demo Project: Fund Request Bug - changed default from 01-01-2000 to NULL as this was disabling every date in the calender popup
2011/10/19			WAB			LID 7941 - problems if there is a gap between budget periods. Calendar can't handle it and then getFundRequestForActivity() falls over
								Was also a varing problem, so have var'ed the whole cfc
2012/02/17 			PPB 		Case 426208 stop Approval emails going out to ALL approvers but just send it to the appropriate level
2012/02/20 			PPB 		fixed a typo
2012/05/16 			IH 			Case 427978 check if MarketingCampaignID is numeric before inserting into database
2012-08-01			PPB			Case 429848 changed entityTypes sent to com.rights.getRightsFilterWhereClause()
2012-11-09 			PPB 		Case 431898 I commented out code that set the LEVEL to a STATUSID
2012-11-15 			PPB 		Case 431990 don't return a message to the user stating Account(s) Created when it already existed
2014-03-05 			PPB 		Case 438905 send email only to 'local' financeApprovers
2014-11-13			AXA			Case 442635 added scale parameter to queryparam for the approvedAmount to prevent CF from rounding the decimal value.
2015-01-29 			PPB 		P-KAS049 currentApprovalLevel bug
2015-05-27 			PPB 		Case 444781 FundsAvailable; I brought in code from trunk which Ali had merged from 2013_responsive (according to the commit comment)
2015-05-28			AHL			P-AVI001 We suppose to send the email to all the fund Manager
2015-08-10			AHL			SUS01 Making this function more robust when the total cost is not defined
2015-09-22			AHL			Dynamic Flag List. Need to be released with functionality to create (flag) views
2015-10-27			AHL		Kanban 1603 Customizing MDF Activity Columns by Settings.
2016-01-14			ESZ			case 446935 	MDF Portal Table Error
2015-12-23			NJH			JIRA BF-17 - changed cast(decimal(10,2)) to cast(decimal(18,2)) as the field is 18,2 in the getFundActivityV2 function.
2016-01-27			RJT			BF-17 Allowed for the total funding (which is the sum of 2 Decimal(18,2)) to be Decimal(19,2) so that the total possible value fits.
15/02/2016          DAN         Case 448047 - few fixes for fund manager mainly around getting correct fundRequest for fundRequestActivity and prevent overwritting checkbox field values if not available
2016/09/07			NJH	on behalf of YAN JIRA PROD2016-2200- brought in the filterApproverstoCountry and used it in onPending and onApprovalLevel
2016-11-28          DAN         JIRA ONB-328 - fix for creating fund accounts if budget group is specified for 'All Countries'
2016-10-11			atrunov		Case https://relayware.atlassian.net/browse/SSPS-48, added UserGroup selection box on Fund Accounts page, this change will go to Core
2016-10-21			atrunov		Case https://relayware.atlassian.net/browse/SSPS-49, integrated User Group Rule for fund account into existing functionality
2016-10-24			atrunov		Case https://relayware.atlassian.net/browse/SSPS-51, Visibility of Closed Fund Accounts

Possible enhancements:

 --->

<cfcomponent displayname="RelayFundManager" hint="Functions for Relay Fund Manager Module">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<cffunction access="public" name="approveFundRequestActivity" hint="Approves a request activity">
		<cfargument name="fundRequestActivityID" type="numeric" required="true">
		<cfargument name="budgetPeriodAllocationID" type="numeric" required="true">
		<cfargument name="approvedAmount" type="numeric" required="true">
		<cfargument name="approvedCoFundingAmount" type="numeric" required="false" default="0">
		<cfargument name="approverID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var getapproveActivity = '' />
		<cfset var approveActivity = '' />
		<cfset var updateapproveActivity = '' />

		<cfquery name="getapproveActivity" datasource="#application.siteDataSource#">
			select *
			from fundRequestActivityApproval
			where fundRequestActivityID = #fundRequestActivityID#
		</cfquery>
		<cfif getapproveActivity.recordcount EQ 0>
			<cfquery name="approveActivity" datasource="#application.siteDataSource#">
				<!--- START: 2014-11-13 AXA	Case 442635 added scale parameter to queryparam for the approvedAmount to prevent CF from rounding the decimal value.--->
				insert into fundRequestActivityApproval (budgetPeriodAllocationID,fundRequestActivityID,amount,CoFundingAmount,createdBy,updatedBy) values
				(<cf_queryparam value="#arguments.budgetPeriodAllocationID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.approvedAmount#" CFSQLTYPE="CF_SQL_decimal" scale="2" >,<cf_queryparam value="#arguments.approvedCoFundingAmount#" CFSQLTYPE="CF_SQL_decimal" >,<cf_queryparam value="#arguments.approverID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.approverID#" CFSQLTYPE="CF_SQL_INTEGER" >)
				<!--- END: 2014-11-13 AXA Case 442635 added scale parameter to queryparam for the approvedAmount to prevent CF from rounding the decimal value.--->
			</cfquery>
		<cfelse>
			<cfquery name="updateapproveActivity" datasource="#application.siteDataSource#">
				update fundRequestActivityApproval set
					budgetPeriodAllocationID = #arguments.budgetPeriodAllocationID#,
					amount = #arguments.approvedAmount#,
					CoFundingAmount = #arguments.approvedCoFundingAmount#,
					createdBy = #arguments.approverID#,
					updatedBy = #arguments.approverID#
				where fundRequestActivityID = #arguments.fundRequestActivityID#
			</cfquery>
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             creates RW company account - based on organisationID, locationID or personID
			 (always works out organisationID) checks whether an RW account already exists
			 for that organisationID, if it doesn't, creates a new account and returns the new accountID,
			 if it does exist, returns the existing accountID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="createFundAccount" returnType="struct" hint="creates a company account" output="true">	<!--- 2012-11-15 PPB Case 431990 returnType="struct" --->

		<cfargument name="organisationID" type="numeric" default="0">
		<cfargument name="locationID" type="numeric" default="0">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="startDate" default="">
		<cfargument name="budgetID" type="numeric" required="true">
		<cfargument name="setBudgetAccess" type="boolean" default="true">

		<cfset var getOrg = '' />
		<cfset var localOrganisationID = '' />
		<cfset var accountStartDate = '' />
		<cfset var check = '' />
		<cfset var insert = '' />
		<cfset var insertAccountBudgetAccess = '' />
		<cfset var result = StructNew() />						<!--- 2012-11-15 PPB Case 431990 --->


		<cfif arguments.organisationID>
			<cfset localOrganisationID = arguments.organisationID>
		<cfelseif arguments.locationID or arguments.personID>

			<cfquery name="getOrg" datasource="#application.SiteDataSource#">
				select organisationid
				from
				<cfif locationID>
					location where locationid = #arguments.locationID#
				<cfelseif personID>
					person where personid = #arguments.personID#
				</cfif>
			 </cfquery>

			<cfset localOrganisationID = getOrg.organisationid>

		</cfif>

<!--- 		<cfif structKeyExists(getOrg.recordCount eq 0>
			<SCRIPT type="text/javascript">
				alert("Could not create an Fund account - organisation does not exist! Most probably you have passes a wrong orgID or a wrong locationID or a wrong personID. Please check the order of your arguments in the call to the function.");
			</script>
			<cfexit method="EXITTAG">
		</cfif>
 --->

		<!---  check whether account already exists --->
		 <cfquery name="check" DATASOURCE="#application.SiteDataSource#">
		 	select accountID from fundCompanyAccount where organisationid =  <cf_queryparam value="#localOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  and budgetID=#arguments.budgetID#
		 </cfquery>

		 <cfif check.recordCount neq 0>
		 	<cfset result.accountAlreadyExisted = true>
		 	<cfset result.accountId = check.accountID>
		 <cfelse>
		 	<cfset result.accountAlreadyExisted = false>
<!---
		 	<cftransaction>						<!--- 2012-11-15 PPB Case 431990 now only 1 query so no need for transaction --->
 --->
				<cfif not len(arguments.startDate)>
					<cfset accountStartDate = now()>
				<cfelse>
					<cfset accountStartDate = arguments.startDate>
				 </cfif>

				 <cfquery name="insert" DATASOURCE="#application.SiteDataSource#">
					insert into fundCompanyAccount
					(OrganisationID, budgetID,DateTermsAgreed, Created, CreatedBy, Updated, UpdatedBy, AccountClosed,fundsStartDate)
					values
						(<cf_queryparam value="#localOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							<cf_queryparam value="#arguments.budgetID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							<cf_queryparam value="#accountStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
							getDate(),
							<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >,
							getDate(),
							<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
							0,
							<cf_queryparam value="#accountStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >);

					select scope_identity() as accountID;						<!--- 2012-11-15 PPB Case 431990 --->
				 </cfquery>

<!--- 2012-11-15 PPB Case 431990 while the hood was up removed this query in favour of scope_identity() --->
<!---
				  <cfquery name="check" DATASOURCE="#application.SiteDataSource#">
					 select max(accountID) as accountID from fundCompanyAccount
				  </cfquery>

			</cftransaction>
 --->

			<cfset result.accountId = insert.accountID>
		 </cfif>

		<!--- insert a new record into the budget Access table which allows this account to draw from this budget --->
 		<cfif arguments.setBudgetAccess>
			<cfquery name="insertAccountBudgetAccess" datasource="#application.siteDataSource#">
				if not exists (select * from fundAccountBudgetAccess where accountID =  <cf_queryparam value="#result.accountID#" CFSQLTYPE="CF_SQL_INTEGER" >  and budgetID = #arguments.budgetID#)			<!--- 2012-11-15 PPB Case 431990 --->
				begin
					insert into fundAccountBudgetAccess (accountID,budgetID,createdBy,updatedBy) values (<cf_queryparam value="#result.accountID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#arguments.budgetID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)		<!--- 2012-11-15 PPB Case 431990 --->
				end
			</cfquery>
		</cfif>

		 <cfreturn result> 		<!--- 2012-11-15 PPB Case 431990 --->

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateFundAccount" hint="Updates a fund account">
		<cfargument name="accountID" type="numeric" required="true">
		<cfargument name="accountClosed" type="numeric" required="false">
		<cfargument name="updatedBy" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var assignBudgets = '' />
		<cfset var updateAccount = '' />

		<cfquery name="updateAccount" datasource="#application.siteDataSource#">
			update fundCompanyAccount set
				<cfif isDefined("arguments.accountClosed")>
					accountClosed = #arguments.accountClosed#,
				</cfif>
				updatedBy = #arguments.updatedBy#,
				updated = #createODBCdatetime(request.requestTime)#
			where accountID = #arguments.accountID#
		</cfquery>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="assignAvailableBudgetsToAccount" hint="Assigns available budgets to an account">
		<cfargument name="accountID" type="numeric" required="true">

		<cfscript>
			var assignBudgets = "";
			var availableBudgets = getAvailableBudgetsForAccount(accountID=arguments.accountID);
			var availableBudgetIds = "";
			var availableBudgetID = "";
		</cfscript>

		<cfset availableBudgetIds = valueList(availableBudgets.budgetID)>

		<cfloop list="#availableBudgetIds#" index="availableBudgetID">
			<cfquery name="assignBudgets" datasource="#application.siteDataSource#">
				insert fundAccountBudgetAccess (accountID,budgetID,createdBy,updatedBy) values
					(<cf_queryparam value="#arguments.accountID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#availableBudgetID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)
			</cfquery>
		</cfloop>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getAvailableBudgetsForAccount" returnType="query" hint="Gets available budgets for an account">
		<cfargument name="accountID" type="numeric" required="true">

		<cfscript>
			var getBudgets = "";
		</cfscript>

		<cfquery name="getBudgets" datasource="#application.siteDataSource#">
			select b.budgetID, b.description as budget from budget b inner join
				fundCompanyAccount fca on fca.organisationID = b.entityID and b.entityTypeID=2
			where fca.accountID = #accountID#
				and b.budgetID not in (select budgetID from fundAccountBudgetAccess where accountID = #arguments.accountID#)

			union

			select b.budgetID, b.description as budget from budget b inner join
				organisation o on b.entityID = o.countryID and b.entityTypeID=3 inner join
				fundCompanyAccount fca on fca.organisationID = o.organisationID
			where fca.accountID = #accountID#
				and b.budgetID not in (select budgetID from fundAccountBudgetAccess where accountID = #arguments.accountID#)
		</cfquery>

		<cfreturn getBudgets>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getBudgets" returnType="query" hint="Gets budgets">
		<cfargument name="accountID" type="numeric" required="false">
		<cfargument name="budgetTypeID" type="numeric" required="false">
		<cfargument name="organisationID" type="numeric" required="false">
		<cfargument name="BudgetGroupID" type="numeric" required="false">

		<cfscript>
			var getBudgetsQry = "";
		</cfscript>

		<cfquery name="getBudgetsQry" datasource="#application.siteDataSource#">
			select b.budgetID, b.description as budget from budget b
			<cfif isDefined("arguments.accountID")>
				inner join
				fundAccountBudgetAccess fab on b.budgetID = fab.budgetID and fab.accountID = #arguments.accountID#
			</cfif>
            <cfif isDefined("arguments.budgetTypeID") and arguments.budgetTypeID eq 3 and isDefined("arguments.organisationID")>
                inner join countryGroup cg on cg.countryGroupID = entityid and cg.countryMemberid = (select countryid from organisation where organisationid = <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="CF_SQL_INTEGER">)
            </cfif>
			where 1=1
			<cfif isDefined("arguments.budgetTypeID")>
				and entityTypeID=#arguments.budgetTypeID#
				<!--- if an orgID has been passed in, get the budget specific to that orgID --->
				<cfif arguments.budgetTypeID eq 2 and isDefined("arguments.organisationID")>
					and entityID = <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="CF_SQL_INTEGER">
				</cfif>
			</cfif>
			<cfif isDefined("arguments.BudgetGroupID")>
				and BudgetGroupID=<cf_queryparam value="#arguments.BudgetGroupID#" CFSQLTYPE="CF_SQL_INTEGER">
			</cfif>
		</cfquery>

		<cfreturn getBudgetsQry>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getBudgetTypes" returnType="query" hint="Gets budget types">
		<cfargument name="budgetTypeIDList" type="string" required="false">

		<cfscript>
			var getBudgetTypesQry = "";
		</cfscript>

		<cfquery name="getBudgetTypesQry" datasource="#application.siteDataSource#">
			select distinct entityTypeID as value,
				case when entityTypeID = 2 then 'Organisation' when entityTypeID = 3 then 'Country' when entityTypeID = 4 then 'Region' END AS display
			from budget
			<cfif isDefined("arguments.budgetTypeIDList")>
				where entityTypeID  in ( <cf_queryparam value="#arguments.budgetTypeIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		</cfquery>

		<cfreturn getBudgetTypesQry>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getFundApprovalStatus" returnType="query" hint="Returns a query object containing approval statii">
		<cfargument name="currentStatusID"  required="no">
		<cfargument name="approvalStatusID"  required="no">
		<cfargument name="UpToapprovalStatusID"  required="no">

		<cfscript>
			var getFundApprovalStatus = "";
			var ApprovalStatusPhraseQuerySnippets = '';
			var availableStatiiList = '';
			var getNextAvailableStatii = '';
		</cfscript>

		<cfset ApprovalStatusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "title",baseTableAlias="fra")>

		<cfif isDefined("arguments.currentStatusID")>
			<cfquery name="getNextAvailableStatii" datasource="#application.siteDataSource#">
				select nextAvailableStatus from fundApprovalStatus where fundApprovalStatusID =  <cf_queryparam value="#arguments.currentStatusID#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>
			<cfset availableStatiiList = getNextAvailableStatii.nextAvailableStatus>
		</cfif>

		<!--- 2011/03/29 PPB LID5826 activity status translation --->
		<cfquery name="getFundApprovalStatus" datasource="#application.siteDataSource#">
			SELECT *
			,#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)# as fundApprovalStatus
			from fundApprovalStatus fra
			#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.join)#
			where fra.active=1
			<cfif isDefined("arguments.approvalStatusID") and arguments.approvalStatusID is not "">
				and fra.fundApprovalStatusID =  <cf_queryparam value="#arguments.approvalStatusID#" CFSQLTYPE="cf_sql_integer" >
			</cfif>
			<cfif isDefined("arguments.UpToapprovalStatusID") and arguments.UpToapprovalStatusID NEQ "">
				and fra.levelAvailabilty <=  <cf_queryparam value="#arguments.UpToapprovalStatusID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				or fra.levelAvailabilty is null
			<cfelse>
				<cfif isDefined("arguments.currentStatusID") and listLen(availableStatiiList) and arguments.currentStatusID is not "">
					and fra.fundApprovalStatusID  in ( <cf_queryparam value="#availableStatiiList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
			</cfif>
			order by sortOrder
		</cfquery>

		<cfreturn getFundApprovalStatus>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<!--- START: 2009-11-11	AJC	P-LEX039 - filter the status list based on person's level/budget group --->
	<cffunction access="public" name="getPersonFundApprovalStatus" returnType="query" hint="Returns a query object containing approval statii">
		<cfargument name="fundRequestStatusQry"  required="true" type="Query">
		<cfargument name="qryFundActivity" required="true" type="Query">

		<cfscript>
			var personFundRequestStatusQry = "";
			var qryBudgetGroupApprover = '';
			var getfinanceApprover = '';
			var qryBudgetGroupAccManager = '';
			var getNextAvailableStatus = '';
			var ApprovalStatusPhraseQuerySnippets  = '';
		</cfscript>
		<!--- 2012/02/03 PPB added ApprovalStatusPhraseQuerySnippets while debugging on Lenovo implementation with Russ cos it was crashing without it if FinanceApprover --->
		<cfset ApprovalStatusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "title",baseTableAlias="fra")>

		<cfset qryBudgetGroupApprover=getBudgetGroupApprover(budgetgroupID=qryFundActivity.budgetgroupID,ApproverLevel=qryFundActivity.currentapprovalLevel,personID=request.relaycurrentuser.personID)>
        <!--- 2011/04/08 PPB LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list --->

		<cfquery name="qryBudgetGroupAccManager" datasource="#application.siteDataSource#">
			select * from person p
			where personID IN
			(select data
			from IntegerFlagdata ifd INNER JOIN Flag f ON ifd.flagid = f.flagid WHERE f.flagtextID  IN ( <cf_queryparam value="#application.com.settings.getSetting("fundManager.orgAccountManagerTextID")#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) and ifd.entityID =  <cf_queryparam value="#qryFundActivity.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  and ifd.data=#request.relaycurrentuser.personID#)
		</cfquery>
		<cfset getfinanceApprover = application.com.flag.getFlagData(flagId="financeApprover", entityID=#request.relayCurrentUser.personID#)>

		<cfif getfinanceApprover.recordcount EQ 0>
			<cfquery name="personFundRequestStatusQry" dbtype="query">
				SELECT * from fundRequestStatusQry
				where (levelAvailabilty = '' or levelAvailabilty is null)
				<cfif qryFundActivity.fundStatusTextID neq 'approved'> 	<!---	PPB 2011/01/21 this condition was added so after Fully Approved we want to see "Fully Approved" (to enter a payment) and "Claim" (if relevant for that client) --->
					and lower(fundStatusTextID) != 'approved'
				</cfif>

				<cfif qryBudgetGroupApprover.recordcount neq 0>
					<cfif (qryBudgetGroupApprover.upperapprovalthreshold lt qryFundActivity.requestedAmount)>
						UNION
						SELECT * from fundRequestStatusQry
						where levelAvailabilty in ('#qryBudgetGroupApprover.approverlevel-1#','#qryBudgetGroupApprover.approverlevel#')
					<cfelseif (qryBudgetGroupApprover.upperapprovalthreshold gte qryFundActivity.requestedAmount)>
						UNION
						SELECT * from fundRequestStatusQry
						where lower(fundStatusTextID) = 'approved'
						or levelAvailabilty = '#qryBudgetGroupApprover.approverlevel-1#'
					</cfif>
				<cfelseif (qryBudgetGroupAccManager.recordcount gt 0)>
					UNION
					SELECT * from fundRequestStatusQry
					where levelAvailabilty='1'
				</cfif>
				UNION
				SELECT * from fundRequestStatusQry 
				where fundApprovalStatusID = #qryFundActivity.fundApprovalStatusID#
				order by sortorder
			</cfquery>

		<cfelse>
			<!--- 2011/03/04 PPB LID5807 START  --->
			<cfquery name="getNextAvailableStatus" DATASOURCE="#application.SiteDataSource#">
				select nextAvailableStatus from fundApprovalStatus where fundApprovalStatusId =  <cf_queryparam value="#qryFundActivity.fundApprovalStatusId#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<!--- 2011/03/29 PPB LID5826 activity status translation --->
			<cfquery name="personFundRequestStatusQry" DATASOURCE="#application.SiteDataSource#">
				SELECT *
				,#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)# as fundApprovalStatus
				from fundApprovalStatus fra
				#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.join)#
				where fra.fundApprovalStatusID  in ( <cf_queryparam value="#getNextAvailableStatus.nextAvailableStatus#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				order by sortorder
			</cfquery>
			<!--- 2011/04/20 PPB LEX052 added sortorder--->
			<!--- 2011/03/04 PPB LID5807 END --->
		</cfif>

		<cfreturn personFundRequestStatusQry>
	</cffunction>
<!--- END: 2009-11-11	AJC	P-LEX039 - filter the status list based on person's level/budget group --->


	<!--- PPB 2011-01-19 P-REL099 this method is to combine the logic of the above 2 methods into one place so it can be used as the query attribute of a field definition in fundRequestActivityDomain.xml; once V1 has been deprecated we may as well combine all into 1 method  --->
	<cffunction access="public" name="getFundApprovalStatusesForPerson" returnType="query" hint="Returns a query object containing approval statii" validValues="true">
		<cfargument name="fundRequestActivityID" required="yes">

		<cfset var qryFundActivity = '' />
		<cfset var fundRequestStatusQry = '' />

		<cfset qryFundActivity = application.com.relayFundManager.getFundActivityV2(activityID=fundRequestActivityID)>

		<cfset fundRequestStatusQry = application.com.relayFundManager.getFundApprovalStatus(currentStatusID=qryFundActivity.fundApprovalStatusID)>

		<cfset fundRequestStatusQry = application.com.relayFundManager.getPersonFundApprovalStatus(fundRequestStatusQry=fundRequestStatusQry,qryFundActivity=qryFundActivity)>

		<cfreturn fundRequestStatusQry>
	</cffunction>




<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFundRequestTypes" returnType="query" hint="all the Fund Request Types" validValues="true">

		<cfscript>
			var getFundRequestTypes = "";
		</cfscript>

		<cfquery name="getFundRequestTypes" DATASOURCE="#application.SiteDataSource#">
			SELECT fundRequestTypeID, fundRequestType
			FROM fundRequestType where active=1
			order by sortOrder
		</cfquery>

		<cfreturn getFundRequestTypes>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        This function will get all the marketing campaigns that have been created
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getMarketingCampaigns" returnType="query" hint="all marketing campaigns" validValues="true">
		<cfscript>
			var getMarketingCampaigns = "";
		</cfscript>

		<cfquery name="getMarketingCampaigns" DATASOURCE="#application.SiteDataSource#">
			SELECT CampaignID, CampaignName
			FROM fundMarketingCampaigns
			order by CampaignName
		</cfquery>

		<cfreturn getMarketingCampaigns>

	</cffunction>
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        This function will get seelcted marketing campaign
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getSelectedMarketingCampaign" returnType="string" hint="Get the marketing campaign that has been selected in the database">
		<cfargument name="MarketingCampaignID" type="numeric" required="Yes">
		<cfscript>
			var getSelectedMarketingCampaign = "";
		</cfscript>

		<cfquery name="getSelectedMarketingCampaign" DATASOURCE="#application.SiteDataSource#">
			SELECT CampaignName
			FROM fundMarketingCampaigns
			where campaignID = #arguments.MarketingCampaignID#
		</cfquery>

		<cfreturn getSelectedMarketingCampaign.CampaignName>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFundRequestTypeDocuments" returnType="query" hint="All the docuements for a fund request type">
		<cfargument name="fundRequestTypeID" type="numeric" required="Yes">

		<cfscript>
			var getRequestTypeDocuments = "";
		</cfscript>

		<cfquery name="getRequestTypeDocuments" datasource="#application.SiteDataSource#">
			select * from fundRequestTypeDocument where active=1
			and fundRequestTypeID = #arguments.fundRequestTypeID#
		</cfquery>

		<cfreturn getRequestTypeDocuments>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkIfUserIsApprover" hint="Returns true if the current users is an approver">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var getfinanceApprover = '' />
		<cfset var getFundApproverIDs = '' />

		<!--- check to see if current user is an approver --->
		<cfquery name="getFundApproverIDs" datasource="#application.siteDataSource#">
			select * from fundApprover where personID = #arguments.personID#
		</cfquery>

		<!--- p-lex039 check if the user is a finance Approver --->
		<cfset getfinanceApprover = application.com.flag.getFlagData(flagId="financeApprover", entityID=#arguments.personID#)>

		<cfif (getFundApproverIDs.recordcount GT 0) or (getfinanceApprover.recordcount GT 0)>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

 <cffunction access="public" name="GetApproverLevelForCountry" returnType="query" hint="Returns the approver level the user has for this fundactivity">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="fundRequestID" type="numeric" required="Yes">

		<cfset var getManagerAccountThruOrg = '' />

 		<!--- START: 2009-10-22 - AJC - Added additional inner joins to get to approverlevel --->
		<cfif 1 is 1>

		<!--- 2011/04/08 PPB LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list --->
		<cfquery name="getManagerAccountThruOrg" datasource="#application.siteDataSource#">
				select max(ApproverLevel) as ApproverLevel from
					(
						select max(fal.approvalLevel) as ApproverLevel
				from fundRequest fr inner join

				fundCompanyAccount fca on fr.accountID = fca.accountID inner join
						budget b on fca.budgetID = b.budgetID inner join
						budgetgroup bg on b.budgetgroupID = bg.budgetgroupID inner join
						budgetgroupApprover bga on bg.budgetgroupID = bga.budgetgroupID inner join
				organisation o on fca.organisationID = o.organisationID inner join
				countryGroup cg on o.countryID = cg.countryMemberID inner join
						fundApprover fa on cg.countrygroupID = fa.countryID and bga.fundapproverID = fa.fundapproverID
						inner join fundApprovalLevel fal on fa.fundApprovalLevelID = fal.fundApprovalLevelID
				where fa.personID = #arguments.personID#
				and fundrequestID = #fundRequestID#

						UNION
						<!--- get the approval level (1) if the person is the accManager --->
						select 1 as ApproverLevel
						from fundRequest fr inner join

						fundCompanyAccount fca on fr.accountID = fca.accountID inner join
						budget b on fca.budgetID = b.budgetID inner join
						budgetgroup bg on b.budgetgroupID = bg.budgetgroupID inner join
						budgetgroupApprover bga on bg.budgetgroupID = bga.budgetgroupID inner join
						organisation o on fca.organisationID = o.organisationID inner join
						countryGroup cg on o.countryID = cg.countryMemberID inner join
						fundApprover fa on cg.countrygroupID = fa.countryID and bga.fundapproverID = fa.fundapproverID
						inner join integerflagdata ifd on o.OrganisationID = ifd.entityID and ifd.data = #arguments.personID#
						inner join Flag f ON ifd.flagid = f.flagid and f.flagtextID  IN ( <cf_queryparam value="#application.com.settings.getSetting("fundManager.orgAccountManagerTextID")#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )

						where bg.accountManagerApproval= 1 and
						fundrequestID = #fundRequestID#


					) as baseqry
		</cfquery>
		<cfelse>
			<cfquery name="getManagerAccountThruOrg" datasource="#application.siteDataSource#">
				select 1 as ApproverLevel
			</cfquery>
		</cfif>
		<!--- END: 2009-10-22 - AJC - Added additional inner joins to get to approverlevel --->

		<cfreturn getManagerAccountThruOrg>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="checkIfUserIsManager" hint="Returns true if the current user is a manager">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="accountID" type="numeric" required="false" default="0">
		<cfset var result = false>

		<cfif arguments.accountID eq 0>
			<cfif application.com.relayFundManager.checkIfUserIsManagerByExplicitAssignment(personID = arguments.personID) OR
				  application.com.relayFundManager.checkIfUserIsManagerByUserGroup(personID = arguments.personID)>
				<cfset result = true>
			</cfif>
		<cfelse>
			<cfif application.com.relayFundManager.checkIfUserIsManagerByExplicitAssignment(personID = arguments.personID,
																							accountID = arguments.accountID) OR
				  application.com.relayFundManager.checkIfUserIsManagerByUserGroup(personID = arguments.personID,
																				   accountID = arguments.accountID)>
				<cfset result = true>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkIfUserIsManagerByExplicitAssignment" hint="Returns true if the current user is a manager by Explicit Assignment rule">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="accountID" type="numeric" required="false">

		<cfset var isManager = '' />
		<cfset var managerAccountIsOrgAccount = '' />
		<cfset var getManagerAccountThruOrg = '' />

		<!--- NJH 2009/01/30 Bug Fix All Sites Issue 1693 - check if flag exists first, as get flag data doesn't do that...--->
		<cfif application.com.flag.doesFlagExist("extFundManager")>
			<!--- check to see if current user is a manager --->
			<cfif not isDefined("arguments.accountID")>
				<cfset isManager = application.com.flag.getFlagData(entityID=arguments.personID,flagID="ExtFundManager")>
			<cfelse>
				<cfset isManager = application.com.flag.getFlagData(entityID=arguments.personID,flagID="ExtFundManager",data=arguments.accountID)>
			</cfif>

			<!--- determine if the user is a fund Manager of the account which is his organisation's account --->
			<cfif isManager.recordcount gt 0>
				<cfquery name="getManagerAccountThruOrg" datasource="#application.siteDataSource#">
					select accountID from fundCompanyAccount where organisationID =
					(select organisationID from person where personID = #arguments.personID#)
				</cfquery>

				<cfset managerAccountIsOrgAccount = false>
				<cfloop query="getManagerAccountThruOrg">
					<cfif listContains(valueList(isManager.data),accountID)>
						<cfset managerAccountIsOrgAccount = true>
					</cfif>
				</cfloop>

				<cfreturn managerAccountIsOrgAccount>
			<cfelse>
				<cfreturn false>
			</cfif>
		<!--- external flag manager flag does not exist --->
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="checkIfUserIsManagerByUserGroup" returntype="boolean" hint="Returns true if the current user is a manager by User Group rule">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="accountID" type="numeric" required="false" default="0">
		<cfset var result = false>

		<cfif arguments.accountID eq 0>
			<cfset var qryGetFundAccountsForPersonByUserGroup =
				application.com.relayFundManager.getFundAccountsForPersonByUserGroup(personID = arguments.personID)>
		<cfelse>
			<cfset var qryGetFundAccountsForPersonByUserGroup =
				application.com.relayFundManager.getFundAccountsForPersonByUserGroup(personID = arguments.personID,
																					accountID = arguments.accountID)>
		</cfif>

		<cfif qryGetFundAccountsForPersonByUserGroup.recordCount gt 0>
			<cfset result = true>
		</cfif>

		<cfreturn result>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getFundAccounts" hint="Returns company fund accounts" returntype="query">
		<cfargument name="organisationID" type="numeric" required="No">
		<cfargument name="accountID" type="numeric" required="No">
		<cfargument name="fundManagerID" type="numeric" required="No">
		<cfargument name="ShowLiveAccountsOnly" type="boolean" required="No" default="1">

		<cfscript>
			var qryFundAccounts="";
			var fundManagersQry = '';
		</cfscript>

		<cfif isDefined("arguments.fundManagerID")>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID(fundManagerID=arguments.fundManagerID)>
		</cfif>

		<cfquery name="qryFundAccounts" datasource="#application.siteDataSource#">
			select fca.*, o.organisationName from fundCompanyAccount fca
				inner join organisation o on fca.organisationID = o.organisationID
			<cfif isDefined("arguments.organisationID")>
				and fca.organisationID = #arguments.organisationID#
			</cfif>
			<cfif isDefined("arguments.accountID")>
				and accountID = #arguments.accountID#
			</cfif>
			<cfif isDefined("arguments.fundManagerID") and fundManagersQry.recordCount>
				and accountID  in ( <cf_queryparam value="#valueList(fundManagersQry.data)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			<cfif ShowLiveAccountsOnly>
				where accountClosed = 0
			</cfif>
		</cfquery>

		<cfreturn qryFundAccounts>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getFundAccountsAvailableForActivity" hint="Returns company fund accounts available for a new activity" returntype="query" validValues="true">
		<cfargument name="fundManagerID" type="numeric" required="No">
		<cfargument name="ShowLiveAccountsOnly" type="boolean" required="No" default="1">

		<!--- LID 2640 NJH 2009/09/14 - scoped fundManagersQry variable and changed the isDefined("arguments.fundManagerID") to structKeyExists --->
		<cfscript>
			var qryFundAccounts="";
			var fundManagersQry = "";
		</cfscript>

		<cfif structKeyExists(arguments,"fundManagerID")>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID(fundManagerID=arguments.fundManagerID)>
		<cfelse>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID()>
		</cfif>

		<cfquery name="qryFundAccounts" datasource="#application.siteDataSource#">
			select distinct o.organisationName, o.organisationID from fundCompanyAccount fca
				inner join organisation o on fca.organisationID = o.organisationID
				inner join fundAccountBudgetAccess faba on fca.budgetID = faba.budgetID and fca.accountID = faba.accountID
				inner join budget b on b.budgetID = faba.budgetID
				inner join BudgetGroup bh on bh.BudgetGroupID = b.BudgetGroupID

			<cfif arguments.ShowLiveAccountsOnly>
            	where accountClosed = 0
			<cfelse>
            	where 1 = 1
			</cfif>

			<cfif not structKeyExists(arguments,"fundManagerID")>
            	and fca.accountID in ( <cf_queryparam value="#valueList(fundManagersQry.data)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
				<!---2009-02-09 - SSS - P-LEX039 the accounts for only the country rights you have access to must show --->
				<!--- 2011/04/19 PPB P-LEX052 added prefix o. to country filter cos now we have country on BudgetGroup --->
				<!--- 2012-07-19 PPB P-SMA001 and o.countryID in (#request.relayCurrentUser.countryList#) --->
				#application.com.rights.getRightsFilterWhereClause(entityType="fundCompanyAccount",alias="o").whereClause#	<!--- 2012-07-19 PPB P-SMA001 --->

				<!--- LID 2640 NJH 2009/09/14 - changed the if structure slightly to make it slightly more robust and readable --->
				<cfif structKeyExists(arguments,"fundManagerID")>
					<cfif fundManagersQry.recordCount gt 0>
						and fca.accountID  in ( <cf_queryparam value="#valueList(fundManagersQry.data)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					<!--- we want to get accounts for a fund manager, but the fund manager doesn't exist and so we should return no records --->
					<cfelse>
						and 1=0
					</cfif>
				</cfif>


				<!--- <cfif request.budgetExclusion>
				and entityTypeID =
					(select min(entityTypeID) from budget b1 inner join budgetPeriodAllocation bpa1
							on b1.budgetID = bpa1.budgetID inner join budgetPeriod bp1
							on bp1.budgetPeriodID = bpa1.budgetPeriodID and datediff(day,bp1.startDate,GetDate()) >=0 and datediff(day,bp1.endDate,GetDate()) <=0 and bp1.periodTypeID=#request.fundsBudgetPeriodType# and bpa1.authorised=1
						where
						(entityTypeId = 2 and entityid = o.organisationID)
							or
						(entityTypeId = 3 and entityid = o.countryID)
							or
						(entityTypeID=4 and entityID in (
							select countryGroupID from countryGroup where countryMemberID = o.countryID))

					)
				</cfif> --->
				order by organisationName

		</cfquery>

		<cfreturn qryFundAccounts>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getAccountsByFundManagerID" hint="Returns all accounts for particular Fund Manager" returntype="query" validValues="true">
		<cfargument name="fundManagerID" type="numeric" required="false" default="0">
		<cfset var qryFundManagersAccountsByExplicitAssign = "">
		<cfset var qryFundManagersAccountsByUserGroup = "">

		<cfif arguments.fundManagerID eq 0>
			<cfset qryFundManagersAccountsByExplicitAssign = application.com.flag.getFlagData(flagID="ExtFundManager")>
			<cfset qryFundManagersAccountsByUserGroup = application.com.relayFundManager.getFundAccountsForPersonByUserGroup()>
		<cfelse>
			<cfset qryFundManagersAccountsByExplicitAssign = application.com.flag.getFlagData(entityID=arguments.fundManagerID,flagID="ExtFundManager")>
			<cfset qryFundManagersAccountsByUserGroup = application.com.relayFundManager.getFundAccountsForPersonByUserGroup(personID=arguments.fundManagerID)>
		</cfif>

		<cfset var qryResult = "">

		<cfquery name="qryResult" dbtype="query">
			SELECT data, personID
			FROM qryFundManagersAccountsByExplicitAssign
			UNION
			SELECT accountid as data, personID
			FROM qryFundManagersAccountsByUserGroup
		</cfquery>

		<cfreturn qryResult>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFundManagersByAccountID" hint="Returns all Fund Managers for particular account" returntype="query" validValues="true">
		<cfargument name="accountID" type="numeric" required="false" default="0">
		<cfargument name="userGroupInfoRequired" type="boolean" required="false" default="false">

		<cfset var qryManagersByExplicitAssign = "">
		<cfset var qryManagersByUserGroup = "">

		<cfif arguments.accountID eq 0>
			<cfset qryManagersByExplicitAssign = application.com.flag.getFlagData(flagID="ExtFundManager")>
			<cfif arguments.userGroupInfoRequired>
				<cfset qryManagersByUserGroup = application.com.relayFundManager.getFundAccountsForPersonByUserGroup()>
			</cfif>
		<cfelse>
			<cfset qryManagersByExplicitAssign = application.com.flag.getFlagData(data=arguments.accountID,flagID="ExtFundManager")>
			<cfif arguments.userGroupInfoRequired>
				<cfset qryManagersByUserGroup = application.com.relayFundManager.getFundAccountsForPersonByUserGroup(accountID=arguments.accountID)>
			</cfif>
		</cfif>

		<cfset var qryResult = "">
		<cfset var qryMerge = "">

		<cfquery name="qryMerge" dbtype="query">
			SELECT data, personID
			FROM qryManagersByExplicitAssign
			<cfif arguments.userGroupInfoRequired>
				UNION
				SELECT accountid as data, personID
				FROM qryManagersByUserGroup
			</cfif>
		</cfquery>

		<cfquery name="qryResult" dbtype="query">
			SELECT DISTINCT data, personID
			FROM qryMerge
		</cfquery>

		<cfreturn qryResult>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getFundRequests" hint="Returns a Fund Request" returntype="query">
		<cfargument name="fundRequestID" type="numeric" required="Yes">

		<cfscript>
			var qryFundRequests="";
		</cfscript>

		<cfquery name="qryFundRequests" datasource="#application.siteDataSource#">
			select * from fundRequest where fundRequestID = #arguments.fundRequestID#
		</cfquery>

		<cfreturn qryFundRequests>

	</cffunction>

<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getBudgetGroups" hint="Returns Budget Groups" returntype="query">
		<cfargument name="fundManagerID" type="numeric" required="false">

		<cfscript>
			var qryBudgetGroups="";
			var fundManagersQry = '';
		</cfscript>

		<cfif isDefined("arguments.fundManagerID")>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID(fundManagerID=arguments.fundManagerID)>
		</cfif>

		<cfquery name="qryBudgetGroups" datasource="#application.siteDataSource#">
			select distinct bh.* from BudgetGroup bh
			<cfif isDefined("arguments.fundManagerID")>
				inner join budget b on bh.BudgetGroupID = b.BudgetGroupID
				inner join fundAccountBudgetAccess faba on b.budgetID = faba.budgetID
				<cfif isDefined("arguments.fundManagerID") and fundManagersQry.recordCount gt 0>
				and faba.accountID  in ( <cf_queryparam value="#valueList(fundManagersQry.data)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<!--- we want to get accounts for a fund manager, but the fund manager doesn't exist and so we should return no records --->
				<cfelseif isDefined("arguments.fundManagerID") and fundManagersQry.recordCount eq 0>
				and 1=0
				</cfif>
			</cfif>
			<!--- 2012/02/13 PPB Case 424431 restrict to users country/countrygroup rights --->
			<!--- 2012-07-24 PPB P-SMA001 commented out
			where bh.countryId IN (0,37,#request.relayCurrentUser.countryList#,#request.relayCurrentUser.regionList#)		<!--- 37=Any Country (should be safe to hard code) --->
			 --->
			where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="BudgetGroup",alias="bh",regionList="0,37,#request.relayCurrentUser.regionList#").whereClause#	<!--- 2012-07-24 PPB P-SMA001 --->	<!--- 2012-08-01 PPB Case 429848 changed entityType --->

		</cfquery>

		<cfreturn qryBudgetGroups>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getBudgetGroupsForActivity" hint="Returns Budget Groups available for a fund manager" returntype="query">
		<cfargument name="fundManagerID" type="numeric" required="true">

		<cfscript>
			var qryBudgetGroups="";
			var fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID(fundManagerID=arguments.fundManagerID);
			var budgetPeriodType=application.com.settings.getSetting("fundManager.budgetPeriodType");
		</cfscript>

		<cfquery name="qryBudgetGroups" datasource="#application.siteDataSource#">
			select distinct bh.* from BudgetGroup bh
				inner join budget b on bh.BudgetGroupID = b.BudgetGroupID
				inner join fundAccountBudgetAccess faba on b.budgetID = faba.budgetID
				inner join fundCompanyAccount fca on fca.accountID = faba.accountID and fca.budgetID = faba.budgetID
				inner join organisation o on fca.organisationID = o.organisationID
				<cfif fundManagersQry.recordCount gt 0>
				and faba.accountID  in ( <cf_queryparam value="#valueList(fundManagersQry.data)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<!--- we want to get accounts for a fund manager, but the fund manager doesn't exist and so we should return no records --->
				<cfelse>
				and 1=0
				</cfif>
				inner join budgetPeriodAllocation bpa on bpa.budgetID = b.budgetID
				inner join budgetPeriod bp on bp.budgetPeriodID = bpa.budgetPeriodID
			where accountClosed = 0
				and datediff(day,bp.startDate,GetDate()) >=0 and datediff(day,bp.endDate,GetDate()) <=0
				and bpa.authorised=1
				and bp.periodTypeID =  <cf_queryparam value="#budgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" >
				and entityTypeID =
					(select min(entityTypeID) from budget b1 inner join budgetPeriodAllocation bpa1
							on b1.budgetID = bpa1.budgetID inner join budgetPeriod bp1
							on bp1.budgetPeriodID = bpa1.budgetPeriodID and datediff(day,bp1.startDate,GetDate()) >=0 and datediff(day,bp1.endDate,GetDate()) <=0 and bp1.periodTypeID =  <cf_queryparam value="#budgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" >  and bpa1.authorised=1
						where
						(entityTypeId = 2 and entityid = o.organisationID)
							or
						(entityTypeId = 3 and entityid = o.countryID)
							or
						(entityTypeID=4 and entityID in (
							select countryGroupID from countryGroup where countryMemberID = o.countryID))

					)
		</cfquery>

		<cfreturn qryBudgetGroups>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getBudgetPeriod" hint="Returns Budget Periods" returntype="query">
		<cfargument name="dateRequested" type="date" required="false">
		<cfargument name="budgetPeriodID" type="string" required="false">

		<cfscript>
			var qryBudgetPeriods="";
		</cfscript>

		<cfquery name="qryBudgetPeriods" datasource="#application.siteDataSource#">
			select * from budgetPeriod where 1=1
			<cfif isDefined("arguments.dateRequested")>
				and PeriodTypeID =  <cf_queryparam value="#application.com.settings.getSetting("fundManager.budgetPeriodType")#" CFSQLTYPE="CF_SQL_INTEGER" >
				and dateDiff(d,startDate,#createODBCDate(arguments.dateRequested)#) >= 0
				and dateDiff(d,endDate,#createODBCDate(arguments.dateRequested)#) <=0
			</cfif>
			<cfif structKeyExists(arguments, "budgetPeriodID")>
                and budgetPeriodID IN (<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.budgetPeriodID#" list="true">)
			</cfif>
		</cfquery>

		<cfreturn qryBudgetPeriods>

	</cffunction>


	<cffunction access="public" name="AddFundActivityV2" returnType="numeric" hint="Adds a fund activity to the fund request">
		<cfargument name="activityFormStruct" type="struct" required="true">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var AddActivity = '' />
		<cfset var theForm = arguments.activityFormStruct>
		<!--14/01/2016		ESZ		case 446935 add budgetGroupID as parametr for receiving correct budgetPeriod when we create new request-->
		<cfset var budgetGroupID = #LSParseNumber(theForm.budgetGroupID)#>
		<cfset var fundRequestID = getFundRequestForActivity(activityDate=theForm.StartDate,accountID=theForm.AccountID,budgetGroupID=budgetGroupID)>

		<!--- <cfset var totalCost = calculateTotalCost(activityFormStruct)> --->

		<cfquery name="AddActivity" datasource="#application.siteDataSource#">
			INSERT INTO fundRequestActivity (
				fundRequestID,
				partnerOrganisationID,
				FundRequestTypeID,
				typeDescription,
				fundApprovalStatusID,
				<cfif isdefined("theForm.MarketingCampaignID") and isNumeric(theForm.MarketingCampaignID)>marketingCampaignID,</cfif>
				totalCost,
				<cfif isDefined("theForm.proposedPartnerFunding") and val(theForm.proposedPartnerFunding) neq 0 >proposedPartnerFunding,</cfif>
				requestedAmount,
				<cfif isDefined("theForm.requestedCoFunding") and val(theForm.requestedCoFunding) neq 0 >requestedCoFunding,</cfif>
				<cfif isDefined("theForm.CoFunder")>CoFunder,</cfif>
				<cfif isDefined("theForm.ROI") and len(theForm.ROI)>roi,</cfif>
				startDate,
				endDate,
				useMarketingMaterials,
				agreeTermsConditions,
				authorised,
				createdBy,
				created)
			VALUES(
				<cf_queryparam value="#fundRequestID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#theForm.partnerOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#theForm.FundRequestTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#theForm.TypeDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				1,
				<cfif isdefined("theForm.MarketingCampaignID") and isNumeric(theForm.MarketingCampaignID)>
					<cf_queryparam value="#theForm.marketingCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				</cfif>
				<cf_queryparam value="#(theForm.totalCost neq "")?theForm.totalCost:0#" CFSQLTYPE="CF_SQL_DECIMAL" >,	<!--- 2015-08-10 AHL SUS01 Making this function more robust when the total cost is not defined --->
				<cfif isDefined("theForm.proposedPartnerFunding") and val(theForm.proposedPartnerFunding) neq 0 >
					 <cf_queryparam value="#theForm.proposedPartnerFunding#" CFSQLTYPE="CF_SQL_DECIMAL" >,
				</cfif>
				<cf_queryparam value="#theForm.RequestedAmount#" CFSQLTYPE="CF_SQL_DECIMAL" >,
				<cfif structKeyExists(theForm,"requestedCoFunding") and val(theForm.requestedCoFunding) neq 0 ><cf_queryparam value="#theForm.requestedCoFunding#" CFSQLTYPE="CF_SQL_DECIMAL" >,</cfif>
				<cfif structKeyExists(theForm,"CoFunder")><cf_queryparam value="#theForm.CoFunder#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<cfif structKeyExists(theForm,"ROI") and len(theForm.ROI)><cf_queryparam value="#theForm.ROI#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<cf_queryparam value="#theForm.StartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#theForm.EndDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#structKeyExists(theForm,'useMarketingMaterials')?1:0#" CFSQLTYPE="CF_SQL_bit" >,		<!--- checkbox: if the formfield exists then it is checked otherwise it isn't --->
				<cf_queryparam value="#structKeyExists(theForm,'agreeTermsConditions')?1:0#" CFSQLTYPE="cf_sql_bit" >,			<!--- checkbox: if the formfield exists then it is checked otherwise it isn't; is this agreeTermsConditions is validated so will always be 1 --->
				<cf_queryparam value="#structKeyExists(theForm,'authorised')?1:0#" CFSQLTYPE="cf_sql_bit" >,								<!--- checkbox: if the formfield exists then it is checked otherwise it isn't; is this authorised is validated so will always be  --->
				<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#createODBCDateTime(request.requestTime)#" CFSQLTYPE="cf_sql_timestamp" >)

			select scope_identity() as activityID
		</cfquery>

		<!--- PROCESS FLAGS --->
		<cfset application.com.flag.processFlagForm (entityid=AddActivity.activityID,formEntityID=0,entityType="fundRequestActivity")>		<!--- send formEntityID=0 cos for a new activity the entityID would have been 0 when the form was displayed --->

		<cfset addActivityHistory(activityID=AddActivity.activityID,createdBy=arguments.createdBy,activityStatusID=1)>
		<cfset invokeStatusMethod(activityID=AddActivity.activityID)>
		<cfreturn AddActivity.activityID>

	</cffunction>


	<cffunction access="public" name="UpdateFundActivityV2" hint="Updates a fund activity.">
		<cfargument name="activityFormStruct" type="struct" required="true">
		<cfargument name="updatedBy" type="numeric" default="#request.relayCurrentUser.personID#">

        <cfset var qFundRequestID = "" /> <!--- 15/02/2016 DAN Case 448047 --->
        <cfset var budgetGroupID = "" /> <!--- 15/02/2016 DAN Case 448047 --->
		<cfset var approvalStatusList = '' />
		<cfset var ListDocStatus = '' />
		<cfset var fundRequestID = '' />
		<cfset var includeCommentsInEmail = '' />
		<cfset var approvedCoFundingAmount = '' />
		<cfset var qryFundApprovalStatus = '' />
		<cfset var getActivityStatus = '' />
		<cfset var getActivityPayment = '' />
		<cfset var UpdateActivity = '' />
		<cfset var theForm = arguments.activityFormStruct>
		<cfset var qoqlevelAvailabiltyForNewStatus = '' />	<!--- 2015-01-29 PPB P-KAS049 currentApprovalLevel bug --->
		<!--- <cfset var totalCost = calculateTotalCost(activityFormStruct)> --->

		<!--- 2011/04/07 PPB changed this list from getSetting("fundManager.approvalStatusIDList") to use the new table col isApproved --->
		<cfquery name="qryFundApprovalStatus" datasource="#application.siteDataSource#">
			select fundApprovalStatusID, levelAvailabilty from fundApprovalStatus where isApproved = 1		<!--- 2015-01-29 PPB P-KAS049 currentApprovalLevel bug --->
		</cfquery>
		<cfset approvalStatusList = ValueList(qryFundApprovalStatus.fundApprovalStatusID)>

		<!--- START: 2015-02-06 PPB P-KAS049 currentApprovalLevel bug --->
		<cfif StructKeyExists(theForm,"fundApprovalStatusID") >
			<cfquery name="qoqlevelAvailabiltyForNewStatus" dbtype="query">
				select levelAvailabilty from qryFundApprovalStatus where fundApprovalStatusID = #theForm.fundApprovalStatusID#
			</cfquery>
		</cfif>
		<!--- END: 2015-02-06 PPB P-KAS049 --->

		<cfif isdefined("theForm.Invoice")>
			<cfset ListDocStatus = 'BankDetails=' & theForm.bankdetails & '#application.delim1#' & 'Invoice=' & theForm.Invoice & '#application.delim1#' & 'Proof=' & theForm.proof>
		</cfif>

        <!--- start: 15/02/2016 DAN Case 448047 --->
        <cfif structKeyexists(theForm, "fundRequestActivityID") and isNumeric(theForm.fundRequestActivityID)>
            <cfquery name="qFundRequestID" datasource="#application.siteDataSource#">
                select fundRequestID from fundRequestActivity where fundRequestActivityID =  <cf_queryparam value="#theForm.fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
            </cfquery>
            <cfset fundRequestID = qFundRequestID.fundRequestID>
        <cfelseif structKeyexists(theForm, "budgetGroupID") and isNumeric(theForm.budgetGroupID)>
            <cfset budgetGroupID = #LSParseNumber(theForm.budgetGroupID)#>
            <cfset fundRequestID = getFundRequestForActivity(activityDate=theForm.startDate, accountID=theForm.accountID, budgetGroupID=budgetGroupID)>
        <cfelse>
            <cfset fundRequestID = getFundRequestForActivity(activityDate=theForm.startDate,accountID=theForm.accountID)>	<!--- PPB: to have a StartDate (hidden) form field available here, I have defined a second field StartDate_displayOnly (html) --->
        </cfif>
        <!--- end: 15/02/2016 DAN Case 448047 --->

		<!--- add a history record only when a status has changed or comments have been added. Getting the current status --->
		<cfquery name="getActivityStatus" datasource="#application.siteDataSource#">
			select fundApprovalStatusID
			from fundRequestActivity
			where fundRequestActivityID =  <cf_queryparam value="#fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<!--- add a history record only when a status has changed or comments have been added. Getting the current status --->
		<cfquery name="getActivityPayment" datasource="#application.siteDataSource#">
			select amount, paymentRef, paymentDate
			from fundRequestActivityPayment
			where fundRequestActivityID =  <cf_queryparam value="#fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<!--- fields are protected with isDefined() cos when they are shown in html mode they are not passed thro as form fields --->
		<cfquery name="UpdateActivity" datasource="#application.siteDataSource#">
			update fundRequestActivity set
				<cfif isDefined("theForm.fundRequestTypeID")>fundRequestTypeID =  <cf_queryparam value="#theForm.fundRequestTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,</cfif>
				<cfif isDefined("theForm.typeDescription")>typeDescription =  <cf_queryparam value="#theForm.typeDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<cfif isDefined("theForm.fundApprovalStatusID")>fundApprovalStatusID =  <cf_queryparam value="#theForm.fundApprovalStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >,</cfif>
				<cfif isDefined("theForm.marketingCampaignsID")>marketingCampaignID =  <cf_queryparam value="#theForm.marketingCampaignsID#" CFSQLTYPE="CF_SQL_INTEGER" >,</cfif>
				<cfif isDefined("theForm.totalCost")>totalCost =  <cf_queryparam value="#val(theForm.totalCost)#" CFSQLTYPE="CF_SQL_decimal" >,</cfif>
				<cfif isDefined("theForm.proposedPartnerFunding")>proposedPartnerFunding =  <cf_queryparam value="#val(theForm.proposedPartnerFunding)#" CFSQLTYPE="CF_SQL_decimal" >,</cfif>	<!--- use val to convert '' to 0 and still allow user to edit from a saved value back to 0 --->
				<cfif isDefined("theForm.requestedAmount")>requestedAmount =  <cf_queryparam value="#val(theForm.requestedAmount)#" CFSQLTYPE="cf_sql_float" >,</cfif>
				<cfif isDefined("theForm.requestedCoFunding")>requestedCoFunding =  <cf_queryparam value="#val(theForm.requestedCoFunding)#" CFSQLTYPE="CF_SQL_decimal" >,</cfif>
				<cfif isDefined("theForm.CoFunder")>CoFunder =  <cf_queryparam value="#theForm.CoFunder#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<cfif isDefined("theForm.roi") and len(theForm.roi) gt 0>roi =  <cf_queryparam value="#theForm.roi#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<cfif isDefined("theForm.startDate")>startDate =  <cf_queryparam value="#createODBCDate(theForm.startDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,</cfif>
				<cfif isDefined("theForm.endDate")>endDate =  <cf_queryparam value="#createODBCDate(theForm.endDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,</cfif>
                <!--- start: 15/02/2016 DAN Case 448047 --->
                <cfif structKeyExists(theForm, "useMarketingMaterials")>useMarketingMaterials=<cf_queryparam value="#theForm.useMarketingMaterials#" CFSQLTYPE="CF_SQL_bit" >,</cfif>
                <cfif structKeyExists(theForm, "agreeTermsConditions")>agreeTermsConditions=<cf_queryparam value="#theForm.agreeTermsConditions#" CFSQLTYPE="CF_SQL_bit" >,</cfif>
                <cfif structKeyExists(theForm, "authorised")>authorised=<cf_queryparam value="#theForm.authorised#" CFSQLTYPE="CF_SQL_bit" >,</cfif>
                <!--- end: 15/02/2016 DAN Case 448047 --->
				updatedBy=#arguments.updatedBy#,
				updated=#createODBCDateTime(request.requestTime)#,
				<cfif isDefined("ListDocStatus") and ListDocStatus NEQ "">docstatus =  <cf_queryparam value="#ListDocStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				fundRequestID =  <cf_queryparam value="#fundRequestID#" CFSQLTYPE="CF_SQL_INTEGER" >

				<!--- START: AJC 2009-12-09 P-LEX039 --->
				<!--- START: 2015-02-06 PPB P-KAS049 currentApprovalLevel bug --->
				<!--- this was incorrectly setting currentApprovalLevel to theForm.fundApprovalStatusID, I have changed it to set the currentApprovalLevel to be the levelAvailabilty of the current Status --->
				<cfif StructKeyExists(theForm,"fundApprovalStatusID") and getActivityStatus.fundApprovalStatusID neq theForm.fundApprovalStatusID>
					<cfif qoqlevelAvailabiltyForNewStatus.levelAvailabilty eq "">
						, currentApprovalLevel = 0
					<cfelse>
						, currentApprovalLevel =  <cf_queryparam value="#qoqlevelAvailabiltyForNewStatus.levelAvailabilty#" CFSQLTYPE="CF_SQL_Integer" >
					</cfif>
				</cfif>
				<!--- END: 2015-02-06 PPB P-KAS049 --->
			where fundRequestActivityID =  <cf_queryparam value="#theForm.fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<!--- PROCESS FLAGS --->
		<cfset application.com.flag.processFlagForm (entityid=theForm.fundRequestActivityID,entityType="fundRequestActivity")>

		<!--- if a payment has been made or if the payment details have changed, add it to the history table --->
		<!--- START: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
		<cfif isdefined("theForm.approvedAmount") and theForm.approvedAmount neq 0 and theForm.approvedAmount neq getActivityPayment.amount>
		<!--- 	or (isdefined("theForm.frmPaymentDate") and theForm.frmPaymentDate neq getActivityPayment.paymentDate)
			or (isdefined("theForm.frmPaymentRef") and theForm.frmPaymentRef neq getActivityPayment.paymentRef) --->
				<cfset addActivityHistory(activityID=theForm.fundRequestActivityID,createdBy=arguments.updatedBy,comments=theForm.comments,approvedAmount=theForm.approvedAmount,activityStatusID=theForm.fundApprovalStatusID)>
		<cfelse>
			<!--- if status has changed or comments have been added, add it to the history table. --->
			<cfif (isDefined("theForm.fundApprovalStatusID") and getActivityStatus.fundApprovalStatusID neq theForm.fundApprovalStatusID) or (isDefined("theForm.comments") and theForm.comments neq "")>
				<cfset addActivityHistory(activityID=theForm.fundRequestActivityID,createdBy=arguments.updatedBy,comments=theForm.comments,activityStatusID=theForm.fundApprovalStatusID)>
			</cfif>
		</cfif>
		<!--- END: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->

		<!--- if the status has changed --->

		<cfif isDefined("theForm.fundApprovalStatusID") and getActivityStatus.fundApprovalStatusID neq theForm.fundApprovalStatusID>

			<!--- if we've approved an amount --->
			<cfif listFind(approvalStatusList,theForm.fundApprovalStatusID)>
				<!--- 2011/03/29 PPB LID5935 add val() to avoid crash if cofunder approval amount is blank --->
				<cfscript>
					application.com.relayFundManager.approveFundRequestActivity(fundRequestActivityID=theForm.fundRequestActivityID,budgetPeriodAllocationID=theForm.budgetPeriodAllocationID,approvedAmount=val(theForm.approvedAmount),approvedCoFundingAmount=val(theForm.approvedCoFundingAmount));
				</cfscript>
			</cfif>

			<cfif not isDefined("theForm.includeCommentsInEmail")>
				<cfset includeCommentsInEmail = 0>
			<cfelse>
				<cfset includeCommentsInEmail = 1>
			</cfif>

			<cfset invokeStatusMethod(activityID=theForm.fundRequestActivityID,includeCommentsInEmail=includeCommentsInEmail)>
		</cfif>

		<!--- NJH 2007/09/13 if we've made a payment - need to look at better way of doing payments--->
		<!--- SSS 2007/10/16 made change to only change this if items updated have changed --->
		<cfif isDefined("theForm.paymentAmount") and theForm.paymentAmount neq "" and theForm.paymentAmount neq 0>		<!--- PPB 2010/12/21 changed interface to show numeric 0.00 in the field cos the type="numeric" validation was failing with "" (not sure why it didn't fail previously) --->
		<!---
		 and theForm.frmPaymentAmount neq getActivityPayment.amount)
			  or (isdefined("theForm.frmPaymentDate") and theForm.frmPaymentDate neq getActivityPayment.paymentDate) or
			  (isdefined("theForm.frmPaymentRef") and theForm.frmPaymentRef neq getActivityPayment.paymentRef)>
			  --->
			<cfif theForm.paymentDate eq "">
				<cfset theForm.paymentDate = createodbcdatetime(now())>
			</cfif>
			<!--- START: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
			<cfset addActivityHistory(activityID=theForm.fundRequestActivityID,createdBy=arguments.updatedBy,comments=theForm.comments,paymentAmount=theForm.paymentAmount,paymentDate=theForm.paymentDate,PaymentRef=theForm.paymentRef,activityStatusID=theForm.fundApprovalStatusID)>
			<!--- END: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
			<cfscript>
				application.com.relayFundManager.makeFundActivityPayment(fundRequestActivityID=theForm.fundRequestActivityID,paymentAmount=theForm.paymentAmount,paymentDate=theForm.paymentDate,PaymentRef=theForm.paymentRef);
			</cfscript>
		</cfif>


	</cffunction>



	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateFundStatus" hint="This updates the status of the fund">
		<cfargument name="activityID" type="numeric" required="yes">

		<cfset var UpdateFundStatus = '' />

		<cfquery name="UpdateFundStatus" datasource="#application.siteDataSource#">
			update fundRequestActivity set
				fundApprovalStatusID=#fundApprovalStatusID#
			where fundRequestActivityID = #frmFundRequestActivityID#
		</cfquery>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="GetApprover" hint="This will get the correct approver for the approval level">
		<cfargument name="ApproverLevel" type="numeric" required="yes">
		<cfargument name="list" type="boolean" default="0">

		<cfset var personID = '' />
		<cfset var GetApprover = '' />

		<cfquery name="GetApprover" datasource="#application.siteDataSource#">
			select personID
			from fundApprover
			where ApproverLevel = #ApproverLevel#
		</cfquery>
		<cfif list>
			<cfif GetApprover.recordcount EQ 0>
				There are no approvers at approver level <cfoutput>#ApproverLevel#</cfoutput>
				<CF_ABORT>
			<cfelseif GetApprover.recordcount EQ 1>
				<cfset personID = GetApprover.personID>
			<cfelse>
				<cfloop query="GetApprover">
					<cfset personID = ValueList(GetApprover.personID)>
				</cfloop>
			</cfif>
		</cfif>
		<cfreturn personID>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="invokeStatusMethod" hint="Invokes a method related to the activity's status">
		<cfargument name="activityID" type="numeric" required="yes">
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">

		<cfscript>
			var fundActivityQry="";
			var statusQry="";
			var parameters = '' ;
			var deleteApproveBudgetFromActivity = '' ;
			var result = '';
		</cfscript>

		<cfset fundActivityQry = getFundActivityV2(activityID=arguments.activityID)>
		<cfset statusQry = getFundApprovalStatus(approvalStatusID=fundActivityQry.fundApprovalStatusID)>


		<!--- NJH 2008/01/29 MDF Phase 3 if the status is part of the rejected group, allocate the approved amount back to the group pot --->
		<cfif statusQry.StatusGroup eq "Rejected">
			<cfquery name="deleteApproveBudgetFromActivity" datasource="#application.siteDataSource#">
				delete from fundRequestActivityApproval where fundRequestActivityID = #arguments.activityID#
			</cfquery>
		</cfif>


		<cfif len(statusQry.fundStatusMethod)>

			<!--- pass in the fund activity query as a parameter --->
			<cfset parameters = structNew()>
			<cfset parameters.fundActivityQry = fundActivityQry>
			<!--- START: 2009-11-10 - AJC - P-LEX039 - pass status into parameters so that emailtextID is not passed --->
			<cfset parameters.statusQry = statusQry>
			<!--- END: 2009-11-10 - AJC - P-LEX039 - pass status into parameters so that emailtextID is not passed --->
			<cfset parameters.includeCommentsInEmail = includeCommentsInEmail>

			<cfinvoke
				component=#application.com.relayFundManager#
				method=#statusQry.fundStatusMethod#
				returnvariable="result"
 				argumentcollection=#parameters#>
			</cfinvoke>
		</cfif>

	</cffunction>

    <cffunction access="public" name="getFundRequestActivityByID" hint="Gets the existing fund request activity by its ID" returntype="query">
        <cfargument name="fundRequestActivityID" type="numeric" required="yes">

        <cfset var qFundRequestActivity = "">;
        <cfquery name="qFundRequestActivity" datasource="#application.siteDataSource#">
            select * from fundRequestActivity where fundRequestActivityID =  <cf_queryparam value="#arguments.fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
        </cfquery>

        <cfreturn qFundRequestActivity>
    </cffunction>

 	<cffunction access="public" name="getFundRequestForActivity" hint="Determines the fund request for a given date" returntype="numeric">
		<cfargument name="activityDate" type="date" required="yes">
		<cfargument name="accountID" type="numeric" required="yes">
		<cfargument name="budgetGroupID" type="numeric" required="false">    <!--- 14/01/2016		ESZ		case 446935 --->

		<cfscript>
			var budgetPeriod="";
			var getFundRequest="";
			var fundRequestID = '';
			var getBudgetPeriodID = '';
		</cfscript>

		<!---14/01/2016		ESZ		case 446935 add budgetGroupID as parametr for receiving correct budgetPeriod In this case we obtain  budgetPeriod exclusively by time interval and
        with query we receive first available one, that may not belong to the budget group that we choose.
		We should obtain budgetperiod through the BudgetPeriodAllocation, because we know budgetGroupId and BudgetPeriodAllocation connects specific periods to groups.--->
		<cfif structKeyexists(arguments, "budgetGroupID")>
			<cfquery name="getBudgetPeriodID" datasource="#application.siteDataSource#">
			 	select budgetPeriodID from BudgetPeriodAllocation where budgetID in (select budgetID from Budget where BudgetGroupID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.budgetGroupID#">)
			</cfquery>
			<cfset budgetPeriod = getBudgetPeriod(dateRequested=arguments.activityDate, budgetPeriodID=valueList(getBudgetPeriodID.BUDGETPERIODID))>
		<cfelse>
			<cfset budgetPeriod = getBudgetPeriod(dateRequested=arguments.activityDate)>
		</cfif>

		<cfif budgetPeriod.recordCount>
			<cfquery name="getFundRequest" datasource="#application.siteDataSource#">
				select fundRequestID from fundRequest where budgetPeriodID in (#valueList(budgetPeriod.budgetPeriodID)#)
					and accountID = #arguments.accountID#
			</cfquery>
			<cfif not getFundRequest.recordCount>
				<!--- if a fund request doesn't exist for this budget period, we'll have to create it --->
				<cfset fundRequestID = addFundRequest(accountID=arguments.accountID,budgetPeriodID=budgetPeriod.budgetPeriodID)>
			<cfelse>
				<cfset fundRequestID = getFundRequest.fundRequestID>
			</cfif>
		<cfelse>
			<!--- need to determine what to do if a budgetPeriod hasn't been set up --->
			<!--- LID 7941 WAB will fall over later if no budget period, so will abort here --->
			<cfset application.com.errorHandler.recordRelayError_Warning(Severity="Error",message="Fund Manager. No Budget Period configured for #arguments.activityDate#")>
			<cfoutput>No Budget Period configured for #arguments.activityDate#</cfoutput>
			<CF_ABORT>

		</cfif>

		<cfreturn fundRequestID>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addFundRequest" hint="Adds a fund request" returntype="numeric">
		<cfargument name="accountID" type="numeric" required="yes">
		<cfargument name="budgetPeriodID" type="numeric" required="yes">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfscript>
			var addRequest="";
			var getFundRequestID="";
			var fundRequestID = '';
		</cfscript>

		<cfquery name="getFundRequestID" datasource="#application.siteDataSource#">
			select fundRequestID from fundRequest where accountID=#arguments.accountID#
				and budgetPeriodID=#arguments.budgetPeriodID#
		</cfquery>

		<cfif not getFundRequestID.recordCount>
			<cfquery name="addRequest" datasource="#application.siteDataSource#">
				insert into fundRequest (accountID,budgetPeriodID,createdBy,updatedBy)
				values (#arguments.accountID#,#arguments.budgetPeriodID#,#arguments.createdBy#,#arguments.createdBy#)

				select scope_identity() as fundRequestID
			</cfquery>

			<cfset fundRequestID = addRequest.fundRequestID>
		<cfelse>
			<cfset fundRequestID = getFundRequestID.fundRequestID>
		</cfif>

		<cfreturn fundRequestID>

	</cffunction>


	<!--- NJH 2015/12/23 JIRA BF-17 - changed cast(decimal(10,2)) to cast(decimal(18,2)) as the field is 18,2 --->
	<cffunction access="public" name="getFundActivityV2" returnType="query" hint="Returns the Fund Activity">
		<cfargument name="activityID" type="numeric" required="Yes">
		<cfargument name="flagTextIdList" type="string" required="no" hint="list with flagTextID or flagGroupTextIDs" default=""> 	<!--- 2015-09-22 AHL Dynamic Flag List --->

		<cfscript>
			var qryFundActivity = "";
			var ApprovalStatusPhraseQuerySnippets = '' ;
			var ExternalStatusTextPhraseQuerySnippets = '' ;
			var FundRequestTypePhraseQuerySnippets = '';
			var flagColumn = '';
		</cfscript>

		<!--- START: 2011/03/28 PPB P-LEN023 - Add phrase snippets code for fundapprovalstatus --->
		<cfset ApprovalStatusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "title",baseTableAlias="fra")>
		<cfset ExternalStatusTextPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "ExternalStatusText",baseTableAlias="fra")>
		<!--- END: 2011/03/28 PPB P-LEN023 --->
		<!--- START: 2011/04/27 AJC LID6189 - Fund Request type is not translated --->
		<cfset FundRequestTypePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundRequestType", phraseTextID = "title",baseTableAlias="frt")>
		<!--- END: 2011/04/27 AJC LID6189 - Fund Request type is not translated --->

		<cfquery name="qryFundActivity" DATASOURCE="#application.SiteDataSource#">
			select fra.fundRequestActivityID,
			cast(fra.proposedPartnerFunding as decimal(18,2)) as proposedPartnerFunding,
			cast(fra.requestedAmount as decimal(18,2)) as requestedAmount,
			cast(fra.requestedCoFunding as decimal(18,2)) as requestedCoFunding,
			fra.coFunder,
			cast(fra.totalCost as decimal(19,2)) as totalCost,
			fra.roi, fra.fundRequestID, fra.fundRequestTypeID,
			fra.fundApprovalStatusID, fra.marketingcampaignID, fra.typeDescription, fra.startDate, fra.endDate, fra.useMarketingMaterials,
			fra.agreeTermsConditions, fra.authorised, fra.createdBy, fra.created, fra.updated, fra.DocStatus, frap.PaymentRef, frap.PaymentDate, ISNULL(frap.Amount,0) as paymentAmount,
				o.organisationID, <!--- still referred to despite use of fundAccountOrganisationId --->
			<!--- START: 2011/04/27 AJC LID6189 - Fund Request type is not translated --->
			#preservesinglequotes(FundRequestTypePhraseQuerySnippets.select)# as fundRequestType,
			<!--- END: 2011/04/27 AJC LID6189 - Fund Request type is not translated --->
			fas.fundApprovalStatus,
				fas.canEditActivity, fas.fundStatusTextID,
				isNull(cast(fraa.amount as decimal(18,2)),0.00) as approvedAmount,
				isNull(cast(fraa.coFundingAmount as decimal(18,2)),0.00) as approvedCoFundingAmount,
				0 as approvedTotalAmount,
				fraa.created as dateApproved,
				--2011/05/12 dont get comments from history here cos it will appear in the form field which should always be empty on form load (only comments entered on the form during that visit need to be shown on the email)
				--frah.comments as comments,
				--(select top 1 isNull(comments,'') from fundRequestActivityHistory where fundRequestActivityID = fra.fundRequestActivityID and fundApprovalStatusID = fra.fundApprovalStatusID order by created desc) as comments,
				--'' as comments,
				p.firstName + ' ' + p.lastName as fundManager, fm.entityID as fundManagerID,
				c.countryDescription as country, bh.BudgetGroupID, bh.description as BudgetGroup,
				<!--- START: 2009-10-23 - AJC - Add approval level to request --->
					currentApprovalLevel,
				<!--- END: 2009-10-23 - AJC - Add approval level to request --->
				o2.organisationname as partnerOrganisationName, <!--- PPB CHANGED was Partner_Name, --->
				o2.organisationname as partner_Name, 			<!--- 2011/05/23 PPB subsequently re-instated partner_Name as hot fix cos it was being accessed in email definitions --->
				<!--- SSS p-lex039 2010-03-11 added partners orgID --->
				o2.organisationID as PartnerOrgID,
				<!--- PPB p-lex039 2010-03-17 added partners country --->
				o2c.countryDescription as PartnerCountry,
				fr.accountID,
				b.entityTypeID as budgetType,
				isNull(bpa.amount,0) as budgetAllocated,
				fr.budgetPeriodID,bp.description as budgetPeriod,
				o.organisationId as fundAccountOrganisationId,
				o.organisationName as fundAccountOrganisationName,
				o.countryId as fundAccountOrganisationCountryId,
				<cfif request.relayCurrentUser.isInternal>
					#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)# as statusText,
				<cfelse>
					isNull(NullIf(#preservesinglequotes(ExternalStatusTextPhraseQuerySnippets.select)#,''), #preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)#) as statusText,
				</cfif>
				<!--- all fields defined in fundRequestActivityDisplay.xml must have an equivalent column in this query (even if only a dummy) --->
				0 as includeCommentsInEmail,
				'' as budgetAllocation,
				'' as budgetAvailable,
				'' as budgetPeriodAllocationID,
				'' as approvedAmount2,
				'' as message_noFundsAllocated,
				'' as documentStates,
				'' as activityHistory,
				'' as activityFiles,
				'' as bankDetails,
				'' as message_requiredFields,
				'' as startDate_displayOnly

				/* START 2015-09-22 AHL Dynamic Flag List */
				<cfloop list='#arguments.flagTextIdList#' index='flagColumn' >
					,fra.#flagColumn#
				</cfloop>
				/* END 2015-09-22 AHL */
				,fca.AccountClosed
			from vfundRequestActivity fra inner join organisation as o2		/* AHL using vfundRequestActivity instead of fundRequestActivity to enable flags */
			    on fra.partnerorganisationID = o2.organisationID inner join country o2c
				on o2c.countryID = o2.countryID inner join fundRequest fr
				on fra.fundRequestID = fr.fundRequestID
				inner join budgetPeriod bp on bp.budgetPeriodID = fr.budgetPeriodID
				inner join fundCompanyAccount fca
				on fca.accountID = fr.accountID inner join organisation o
				on fca.organisationID = o.organisationID inner join fundRequestType frt
				on frt.fundRequestTypeID = fra.fundRequestTypeID inner join fundApprovalStatus fas
				on fra.fundApprovalStatusID = fas.fundApprovalStatusID left outer join fundRequestActivityApproval fraa
				on fra.fundRequestActivityID = fraa.fundRequestActivityID /*left outer join fundRequestActivityHistory frah
				on frah.fundRequestActivityID = fra.fundRequestActivityID and frah.fundApprovalStatusID = fra.fundApprovalStatusID*/ inner join country c
				on c.countryID = o.countryID inner join fundAccountBudgetAccess faba
				on faba.budgetID = fca.budgetID and faba.accountID = fca.accountiD inner join budget b
				on b.budgetID = faba.budgetID inner join BudgetGroup bh
				on b.BudgetGroupID = bh.BudgetGroupID left outer join fundRequestActivityPayment frap
				on frap.fundRequestActivityID = fra.fundRequestActivityID
				<!---START : atrunov/SoftServe inner join information about what manager responsible for what account, added UserGroup Rule. https://relayware.atlassian.net/browse/SSPS-49--->
				<!--- Before changes only ExtFundManager flag was taking into account--->
				<!--- Old query provided duplicate records because each account can have several managers, new one implementation saves this behaviour --->
				inner join ( SELECT fm.data,
									fm.entityid,
									p.personID,
									p.firstName,
									p.lastName
							FROM dbo.integerMultipleFlagData fm
                			INNER JOIN dbo.flag f on f.FlagID = fm.flagid and f.FlagTextID = 'ExtFundManager'
                			INNER JOIN Person p ON p.personID = fm.entityid

                			UNION

                			SELECT	imf.entityid as data,
                					p.PersonID as entityid,
                					p.PersonId as personID,
                					p.firstName, p.lastName
                			FROM dbo.integerMultipleFlagData imf
                			INNER JOIN dbo.flag f on imf.flagid = f.FlagID and f.FlagTextID = 'ExtFundManagersUserGroups'
                			INNER JOIN dbo.FundCompanyAccount fca on fca.AccountID = imf.entityid
                			INNER JOIN dbo.Person p ON p.OrganisationID = fca.OrganisationID
                			INNER JOIN dbo.RightsGroup rg on rg.PersonID = p.PersonID
                			WHERE rg.UserGroupID = imf.data ) fm
				on fm.data = fca.accountID
				<!---END : atrunov/SoftServe inner join information about what manager responsible for what account, added UserGroup Rule. https://relayware.atlassian.net/browse/SSPS-49--->

				inner join person p on p.personID = fm.entityID
				left outer join budgetPeriodAllocation bpa on bpa.budgetPeriodID = fr.budgetPeriodID and bpa.budgetID = b.budgetID
				#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.join)#
				#preservesinglequotes(ExternalStatusTextPhraseQuerySnippets.join)#
				<!--- START: 2011/04/27 AJC LID6189 - Fund Request type is not translated --->
				#preservesinglequotes(FundRequestTypePhraseQuerySnippets.join)#
				<!--- END: 2011/04/27 AJC LID6189 - Fund Request type is not translated --->
			where fra.fundRequestActivityID = #arguments.activityID#
		</cfquery>

		<cfreturn qryFundActivity>

	</cffunction>




	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addActivityHistory" hint="Add a history record for an activity">
		<cfargument name="activityID" type="numeric" required="true">
		<cfargument name="comments" type="string" default="">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="created" type="date" default="#createODBCDateTime(request.requestTime)#">
		<cfargument name="activityStatusID" type="numeric" required="true">
		<!--- START: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
		<cfargument name="approvedAmount" default="">
		<!--- END: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
		<cfargument name="paymentAmount" default="">
		<cfargument name="paymentDate" default="">
		<cfargument name="PaymentRef" type="string" default="">
		<!--- START: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
		<cfscript>
			var qryAddActivityHistory = "";
			var qryFundActivity = "";
			var getActivityPaymentForHistory = '' ;
		</cfscript>

		<!--- add a history record only when a status has changed or comments have been added. Getting the current status --->
		<cfquery name="getActivityPaymentForHistory" datasource="#application.siteDataSource#">
			select amount, paymentRef, paymentDate
			from fundRequestActivityPayment
			where fundRequestActivityID=#arguments.activityID#
		</cfquery>

		<cfset qryFundActivity = getFundActivityV2(activityID=activityID)>		<!--- a bit excessive to re-interrogate the db; clean later maybe --->

		<!--- PPB 2010/12/21 rejiged line breaks so if no comments are entered you don't get a blank first line in history --->
		<cfif trim(arguments.comments) neq "">
			 <cfset arguments.comments = arguments.comments & '<br />'>
		</cfif>
		<cfif approvedAmount NEQ "" and qryFundActivity.approvedAmount neq approvedAmount>
			<cfset arguments.comments = arguments.comments & 'Approved amount = ' & approvedAmount & '<br />'>
		</cfif>
		<cfif paymentamount NEQ "" and getActivityPaymentForHistory.amount neq paymentAmount>
			<cfset arguments.comments = arguments.comments & 'Payment amount = ' & Paymentamount & '<br />'>
		</cfif>
		<cfif paymentdate NEQ "" and getActivityPaymentForHistory.paymentDate neq paymentDate>
			<cfset arguments.comments = arguments.comments & 'Payment date = #dateformat(paymentDate, "yyyy/mm/dd")#' & '<br />'>
		</cfif>
		<cfif paymentRef NEQ "" and getActivityPaymentForHistory.paymentRef neq paymentRef>
			<cfset arguments.comments = arguments.comments & 'Payment Reference = ' & paymentRef & '<br />'>
		</cfif>
		<!--- END: AJC 2009-12-09 P-LEX039 Changed so that it doesn't store approved amount as payment amount in the comments field --->
		<!--- 2011/04/08              NAS               LID6213: Fund Manager - When you add in approval comments into the approval emails it represents Russian and Japanese chars as ??? --->
		<cfquery name="qryAddActivityHistory" DATASOURCE="#application.SiteDataSource#">
			insert into fundRequestActivityHistory (fundRequestActivityID,comments,createdBy,created,fundApprovalStatusID)
			values (
				<cf_queryparam value="#arguments.activityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#arguments.comments#" CFSQLTYPE="CF_SQL_VARCHAR" >
				,<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#createODBCDateTime(arguments.created)#" CFSQLTYPE="cf_sql_timestamp" >
				,<cf_queryparam value="#arguments.activityStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		</cfquery>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addBudgetPeriodAllocation" hint="Allocate an amount for a budget and period">
		<cfargument name="budgetID" type="numeric" required="true">
		<cfargument name="budgetPeriodID" type="numeric" required="true">
		<cfargument name="allocatedAmount" type="numeric" required="true">
		<cfargument name="authorised" type="boolean" required="true">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="created" type="date" default="#createODBCDateTime(request.requestTime)#">


		<cfscript>
			var qryBudget = "";
			var qryAddBudgetPeriodAllocation = "";
			var autoAuthorise = 1;
		</cfscript>


		<cfquery name="qryBudget" DATASOURCE="#application.SiteDataSource#">
			SELECT entityTypeID FROM budget where budgetID=#arguments.budgetID#
		</cfquery>

		<!--- authoriseBudgetTypes indicates which entityTypes have to be authorised manually otherwise auto-authorise --->
		<cfif ListContains(application.com.settings.getSetting("fundManager.authoriseBudgetTypes"),qryBudget.entityTypeID)>
			<cfset autoAuthorise = 0>
		</cfif>

		<cfquery name="qryAddBudgetPeriodAllocation" DATASOURCE="#application.SiteDataSource#">
			insert into budgetPeriodAllocation (budgetID,budgetPeriodID,amount,authorised,createdBy,created,lastUpdatedBy,lastUpdated)
			values (
					<cf_queryparam value="#arguments.budgetID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#arguments.budgetPeriodID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#arguments.allocatedAmount#" CFSQLTYPE="CF_SQL_decimal" >,
					<cf_queryparam value="#autoAuthorise#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#createODBCDateTime(arguments.created)#" CFSQLTYPE="cf_sql_timestamp" >,
					<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#createODBCDateTime(arguments.created)#" CFSQLTYPE="cf_sql_timestamp" >
					)
		</cfquery>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="changeBudgetPeriodAllocation" hint="Change the amount for a budget and period">
		<cfargument name="budgetPeriodAllocationID" type="numeric" required="true">
		<cfargument name="amount" type="numeric" required="true">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="created" type="date" default="#createODBCDateTime(request.requestTime)#">

		<cfscript>
			var qryChangeBudgetPeriodAllocation = "";
		</cfscript>

		<cfquery name="qryChangeBudgetPeriodAllocation" DATASOURCE="#application.SiteDataSource#">
			update budgetPeriodAllocation set
				amount = #arguments.amount#,
				lastUpdated = #arguments.created#,
				lastUpdatedBy = #arguments.createdBy#
			where budgetPeriodAllocationID = #arguments.budgetPeriodAllocationID#
		</cfquery>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFundAccountBudgets" returnType="query" hint="Get the list of budgets for an account">
		<cfargument name="accountID" type="numeric" required="true">


		<cfscript>
			var qryGetFundAccountBudgets = "";
		</cfscript>

		<cfquery name="qryGetFundAccountBudgets" datasource="#application.SiteDataSource#">
			select b.budgetID,b.description as budget from fundAccountBudgetAccess faba inner join budget b
				on faba.budgetID = b.budgetID
			where faba.accountID = #arguments.accountID#
		</cfquery>

		<cfreturn qryGetFundAccountBudgets>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getBudgetAllocations" returnType="query" hint="Get the list of budget allocations">
		<cfargument name="budgetPeriodAllocationID" type="numeric" required="false">

		<cfscript>
			var qryGetBudgetAllocations = "";
		</cfscript>

		<cfquery name="qryGetBudgetAllocations" datasource="#application.SiteDataSource#">
			select *, amount - (select isNull(sum(amount),0) from fundRequestActivityApproval where budgetPeriodAllocationID = bpa.budgetPeriodAllocationID) as budgetRemaining
			from budgetPeriodAllocation bpa where 1=1
				<cfif isDefined("arguments.budgetPeriodAllocationID")>
					and bpa.budgetPeriodAllocationID = #arguments.budgetPeriodAllocationID#
				</cfif>
		</cfquery>


		<cfreturn qryGetBudgetAllocations>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getBudgetAllocationsForAccount" returnType="query" hint="Get the list of budget allocations for an account">
		<cfargument name="accountID" type="numeric" required="true">
		<cfargument name="budgetPeriodID" type="numeric" required="true">
		<cfargument name="BudgetGroupID" type="numeric" required="true">

		<cfscript>
			var qryGetFundAccountBudgetAllocations = "";
		</cfscript>

		<cfquery name="qryGetFundAccountBudgetAllocations" datasource="#application.SiteDataSource#">
			select distinct bpa.budgetPeriodAllocationID,b.description as budget
			from fundRequest fr inner join fundCompanyAccount fca
				on fca.accountID = fr.accountID inner join fundAccountBudgetAccess faba
				on faba.budgetID = fca.budgetID and faba.accountID = fca.accountID inner join budget b
				on b.budgetID = faba.budgetID and b.BudgetGroupID = #arguments.BudgetGroupID# inner join budgetPeriodAllocation bpa
				on bpa.budgetID = b.budgetID and bpa.budgetPeriodID = #arguments.budgetPeriodID#
			where fca.accountID = #arguments.accountID# and bpa.authorised=1
		</cfquery>

		<cfreturn qryGetFundAccountBudgetAllocations>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addFundAccountManager" returnType="boolean" hint="Adds a manager to an account">
		<cfargument name="accountID" type="string" required="true">
		<cfargument name="fundManagerID" type="string" required="true">

		<cfset var isManager = application.com.flag.getFlagData(entityID=arguments.fundManagerID,flagID="ExtFundManager",data=arguments.accountID)>

		<cfif not isManager.recordCount>
			<cfset application.com.flag.setFlagData(entityID=arguments.fundManagerID,flagID="ExtFundManager",data=arguments.accountID)>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addFundAccountManagerGroup" returnType="boolean" hint="Adds a manager group to an account">
		<cfargument name="accountID" type="string" required="true">
		<cfargument name="userGroupID" type="string" required="true">

		<cfset var isManagersGroup = application.com.flag.getFlagData(entityID=arguments.accountID,flagID="ExtFundManagersUserGroups",data=arguments.userGroupID)>

		<cfif not isManagersGroup.recordCount>
			<cfset application.com.flag.setFlagData(entityID=arguments.accountID,flagID="ExtFundManagersUserGroups",data=arguments.userGroupID)>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFundAccountManagers" returnType="query" hint="Returns the managers for an account">
		<cfargument name="accountID" type="numeric" required="false">
		<cfargument name="budgetID" type="numeric" required="false">
		<cfargument name="userGroupInfoRequired" type="boolean" required="false" default="true">

		<cfset var getFundManagers = queryNew("fullname,personID,email,officePhone")>
		<cfset var currentManagerIDlist = "">
		<!--- NYB 2011-02-15 START LHID5662 replaced:
		<cfset var currentManagers = application.com.flag.getFlagData(flagID="ExtFundManager")>
		with:--->
		<cfset var currentManagers = queryNew("entityid,PersonID,name,data,flagid,lastUpdated")>
		<!--- NYB 2011-02-15 END LHID5662 --->
		<cfset var getAccountsForBudget = "">
		<cfset var currentManagersQry = "">

		<!--- get fund managers for a particular account --->
		<cfif isDefined("arguments.accountID")>
			<cfset currentManagers = application.com.relayFundManager.getFundManagersByAccountID(accountID=arguments.accountID,
																								 userGroupInfoRequired = arguments.userGroupInfoRequired)>

		<!--- want to get fund managers with accounts for a particular budget --->
		<cfelseif isDefined("arguments.budgetID")>
			<cfquery name="getAccountsForBudget" datasource="#application.siteDataSource#">
				select accountID from fundCompanyAccount where budgetID=#arguments.budgetID#
			</cfquery>

			<cfif getAccountsForBudget.recordCount gt 0>
				<cfloop query="getAccountsForBudget">
					<cfset currentManagers = application.com.relayFundManager.getFundManagersByAccountID(accountID=accountID,
																										 userGroupInfoRequired = arguments.userGroupInfoRequired)>
					<cfquery name="currentManagersQry" dbtype="query">
						select * from currentManagers
						<cfif currentRow gt 1>
							union
							select * from currentManagersQry
						</cfif>
					</cfquery>
				</cfloop>
				<cfset currentManagers = currentManagersQry>
			</cfif>

		<cfelse>
		<!--- get all the fund managers --->
			<cfset currentManagers = application.com.relayFundManager.getFundManagersByAccountID(userGroupInfoRequired = arguments.userGroupInfoRequired)>

		</cfif>

		<cfif isQuery(currentManagers)>		<!--- PPB 2011/01/26 LID3670 added this to avoid a crash without replicating the error (i suspect the var currentManagers at the top has been added and hence i don't hit the problem) --->
		<cfloop query="currentManagers">
			<cfif personID neq "">
				<cfset currentManagerIDlist = listAppend(currentManagerIDlist,personID)>
			</cfif>
		</cfloop>
		</cfif>

		<cfif currentManagerIDlist neq "">
			<cfquery name="getFundManagers" datasource="#application.siteDataSource#">
				select distinct p.firstName+ ' '+p.lastName as fullname,p.firstname,p.lastname,p.personID,p.email, p.officephone from person p
					where personID in (#currentManagerIDlist#)
			</cfquery>
		</cfif>

		<cfreturn getFundManagers>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFundAccountManagersGroups" returnType="query" hint="Returns the managers groups for an account">
		<cfargument name="accountID" type="numeric" required="false">
		<cfset var getFundAccountManagersGroups = application.com.flag.getFlagData(flagID="ExtFundManagersUserGroups",entityID=arguments.accountID)>
		<cfreturn getFundAccountManagersGroups>
	</cffunction>
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="canAddActivity" returnType="boolean" hint="Returns whether an activity can be added">
		<cfargument name="lastSubmissionInterval" type="numeric" required="false">

		<cfset var budgetPeriodQry = '' />

		<cfif isDefined("arguments.lastSubmissionInterval")>
			<cfset budgetPeriodQry = getBudgetPeriod(dateRequested=now())>
			<!--- no budget periods have been set up --->
			<cfif budgetPeriodQry.recordCount>
				<cfif dateDiff("d",now(),budgetPeriodQry.endDate) gte lastSubmissionInterval>
					<cfreturn true>
				<cfelse>
					<cfreturn false>
				</cfif>
			<cfelse>
				<cfreturn false>
			</cfif>
		<cfelse>
			<cfreturn true>
		</cfif>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPotentialFundManagers" returnType="query" hint="Returns the potential fund managers for an account">
		<cfargument name="accountID" type="string" required="true">

		<cfset var getPotentialManagers = '' >

		<cfquery name="getPotentialManagers" datasource="#application.siteDataSource#">
			select p.firstName+ ' '+p.lastName as fullname,p.personID from person p
				inner join location l on l.locationID = p.locationID
				inner join fundCompanyAccount fca on fca.organisationID = l.organisationID
			where accountID =  <cf_queryparam value="#arguments.accountID#" CFSQLTYPE="CF_SQL_INTEGER" >
			order by fullname 	<!--- 2011/03/18 PPB LID5794 added order by --->
				<!--- 2011/03/07 PPB LID5794 commented out cos fundAccountBudgetAccess doesn't have a personID column
				  and p.personID not in (select personID from fundAccountBudgetAccess where accountID = #arguments.accountID#) --->
		</cfquery>

		<cfreturn getPotentialManagers>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="getPotentialFundManagersGroups" returnType="query" hint="Returns the potential fund managers groups">

		<cfset var getPotentialManagersGroups = '' >

		<CFQUERY NAME="getPotentialManagersGroups" datasource="#application.siteDataSource#">
			Select UserGroupID as dataValue, Name as displayValue
			FROM UserGroup
			WHERE PersonID IS NULL
			ORDER BY Name
		</CFQUERY>

		<cfreturn getPotentialManagersGroups>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="removeFundAccountManager" hint="Removes a manager from an account">
		<cfargument name="accountID" type="string" required="true">
		<cfargument name="fundManagerID" type="string" required="true">

		<cfset application.com.flag.deleteFlagData(entityID=arguments.fundManagerID,data=arguments.accountID,flagID="ExtFundManager")>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="removeFundAccountManagerGroup" hint="Removes a manager group from an account">
		<cfargument name="accountID" type="string" required="true">
		<cfargument name="userGroupID" type="string" required="true">

		<cfset application.com.flag.deleteFlagData(entityID=arguments.accountID,data=arguments.userGroupID,flagID="ExtFundManagersUserGroups")>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onPending" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfscript>
			var qryBudgetGroupAccApproval="";
			var qryBudgetGroupAccManager="";
			var resultAlert="false";
			var resultConf="false";
			var qry_get_approverList = '' ;
			var approverList = '';
 			var approverID = '';
		</cfscript>

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.companyName = fundActivityQry.fundAccountOrganisationName>		<!--- 2011/04/20 PPB was fundActivityQry.organisationName - crashed --->
		<cfset form.ContactName = fundActivityQry.fundManager>
		<cfset form.PartnerCountry = fundActivityQry.country>
		<cfset form.fundRequestType = fundActivityQry.FundRequestType>			<!--- 2011/03/24 PPB LID6028 added--->
		<cfset form.fundclaimdescription = fundActivityQry.TypeDescription>		<!--- 2011/03/24 PPB LID6028 added --->

		<cfset qry_get_approverList = application.com.relayFundManager.getBudgetGroupApprover(budgetgroupID=fundActivityQry.BudgetGroupID,ApproverLevel=1)>
		<!--- 2016/04/11	YMA	Fund approvers do not respect country assignments so breaking core to do so.  suggest moving this back into core. --->
		<cfset qry_get_approverList = filterApproverstoCountry(approversQry=qry_get_approverList,fundActivity=fundActivityQry)>
		<cfset approverList = valuelist(qry_get_approverList.personID)>

		<cfquery name="qryBudgetGroupAccApproval" datasource="#application.siteDataSource#">
			select * from budgetgroup
			where accountManagerApproval = 1 and budgetgroupID = #fundActivityQry.budgetgroupID#
		</cfquery>

		<cfif qryBudgetGroupAccApproval.recordcount gt 0>
			<cfquery name="qryBudgetGroupAccManager" datasource="#application.siteDataSource#">
				select * from person p
				where personID IN
				(select data
				from IntegerFlagdata ifd INNER JOIN Flag f ON ifd.flagid = f.flagid
				where f.flagtextID IN (#ListQualify(application.com.settings.getSetting("fundManager.orgAccountManagerTextID"),"'")#)
				and ifd.entityID=#fundActivityQry.OrganisationID#)
			</cfquery>
			<!--- START: 2011/05/04 - AJC - Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests --->
			<cfloop query="qryBudgetGroupAccManager">
				<cfset approverList = listappend(approverList,personID)>
			</cfloop>
			<!--- END: 2011/05/04 - AJC - Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests --->
		</cfif>
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif statusQry.emailTextID1 neq "">
			<cfloop index = "approverID" list = "#approverList#" delimiters = ",">
				<cfset resultAlert=sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
			</cfloop>
		</cfif>

		<cfif statusQry.emailTextID2 neq "">
			<!--- 2011/03/24 PPB LID6027 send an email to all fund managers not just currentuser --->
			<cfloop query="fundActivityQry">		<!--- fundActivityQry returns 1 row per fund manager (suprizingly!!) --->
				<cfset resultConf=sendMDFEmail(emailTextID=statusQry.emailTextID2,personID=fundActivityQry.fundManagerId,fundActivityQry=fundActivityQry)>
			</cfloop>
		</cfif>
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif resultAlert and resultConf>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onRequestQueried" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">.
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>

			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onPartlyApproved" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var requiredDocuments = '' />
		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>
		<cfset requiredDocuments = getFundRequestTypeDocuments(fundRequestTypeID=fundActivityQry.fundRequestTypeID)>
		<cfset form.popRequired = valueList(requiredDocuments.document,"<br>")>
		<cfset form.fundRequestType = fundActivityQry.fundRequestType>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>

			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onFullyApproved" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<cfargument name="mergeStruct" type="struct" DEFAULT="#structNew()#"> 			<!--- 2011/03/30 PPB needed to var it for LID5963 so may as well allow it to be passed in --->
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var Emailtolist ="">
		<cfset var qryBudgetGroupAccManager = "">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START: 2011/03/30 PPB LID5963 --->
			<cfset var link = "">
			<cfset var additionalParameters = "">
		<!--- END: 2011/03/30 PPB LID5963 --->
		<cfset var requiredDocuments = '' />
		<cfset var getchannelmanagerID = '' />
		<cfset var approverID = '' />

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>
		<cfset requiredDocuments = getFundRequestTypeDocuments(fundRequestTypeID=fundActivityQry.fundRequestTypeID)>
		<cfset form.popRequired = valueList(requiredDocuments.document,"<br>")>
		<cfset form.fundRequestType = fundActivityQry.fundRequestType>


		<!--- p-lex039 send the email to the fundmanager and the channel manager--->
		<!--- START: 2011/05/04 - AJC - Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests --->
		<cfquery name="qryBudgetGroupAccManager" datasource="#application.siteDataSource#">
			select * from person p
			where personID IN
			(select data
			from IntegerFlagdata ifd INNER JOIN Flag f ON ifd.flagid = f.flagid
			where f.flagtextID  IN ( <cf_queryparam value="#application.com.settings.getSetting("fundManager.orgAccountManagerTextID")#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			and ifd.entityID =  <cf_queryparam value="#fundActivityQry.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>
		<cfloop query="qryBudgetGroupAccManager">
			<cfset Emailtolist = ListAppend(Emailtolist, personID, ",")>
		</cfloop>
		<!--- END: 2011/05/04 - AJC - Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests --->
		<!---
		<cfset getchannelmanagerID = application.com.flag.getFlagData(flagId="AccManager", entityID=#fundActivityQry.PartnerOrgID#)>
		 --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
			<!--- 2011/04/07 PPB LID6164 add ALL Fund Managers to list --->
			<cfloop query="fundActivityQry">		<!--- fundActivityQry returns 1 row per fund manager (suprizingly!!) --->
				<cfset Emailtolist = ListAppend(Emailtolist, fundActivityQry.fundManagerId, ",") >
			</cfloop>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- <cfif getchannelmanagerID.recordcount GT 0>
			<cfset Emailtolist = ListAppend(Emailtolist, getchannelmanagerID.data, ",")>
		</cfif> --->

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif statusQry.emailTextID1 neq "">
			<cfloop index = "approverID" list = "#Emailtolist#" delimiters = ",">
				<!--- START 2011/03/30 PPB LID5963 add link direct to request --->
				<!--- This creates you a link to an element and makes sure that the correct person is logged in  --->
				<cfset link = application.com.relayElementTree.createSEIDQueryString(LinkType="tracked",personid=approverID,elementid = 'mdf')>		<!--- elementId refers to the uniqueTextId of the screen element (in screen attributes)  --->
				<!--- This adds on your extra parameters as 'formstate' --->
				<cfset additionalParameters = application.com.security.encryptquerystring("mode='edit'&fundRequestActivityID=#fundActivityQry.fundRequestActivityID#")>
				<!--- This outputs it all as a link --->
				<cfset arguments.mergeStruct.urllink = "#link#&#additionalParameters#"/>
				<!--- END 2011/03/30 PPB LID5963 --->

				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry,mergeStruct=mergeStruct)>
			</cfloop>
		</cfif>
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onRejected" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>

			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onPOPapproved" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfscript>
			var resultConf = "false";
			var popApproved = "false";
			var paymentApproved = "false";
			var getfinanceApproverID ="";
			var Emailtolist ="";
			var listgetfinanceApproverID ="";
			var approverID = '';
		</cfscript>
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>
		<cfset form.companyName = fundActivityQry.fundAccountOrganisationName>		<!--- 2011/04/18 PPB was fundActivityQry.organisationName - crashed --->
		<cfset form.PartnerCountry = fundActivityQry.country>
		<!---p-lex039 add any financeApprovers that are in the system to the email to allow approval--->
		<!--- START 2014-03-05 PPB Case 438905 send email only to 'local' financeApprovers  --->
		<!--- <cfset getfinanceApproverID = application.com.flag.getFlagData(flagId="financeApprover")> --->
		<cfquery name="getfinanceApproverID" datasource="#application.siteDataSource#">
  			SELECT EntityId
			FROM BooleanFlagData bfd
			INNER JOIN Flag f ON f.FlagId=bfd.FlagId
			INNER JOIN Person p ON p.PersonId=bfd.EntityId
			INNER JOIN Location l ON l.LocationId=p.LocationId
			INNER JOIN vCountryRights vcr ON vcr.PersonId=p.PersonId AND vcr.CountryID=l.CountryID
			WHERE f.FlagTextId='financeApprover'
			AND l.CountryId =  <cf_queryparam value="#fundActivityQry.fundAccountOrganisationCountryId#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<!--- END 2014-03-05 PPB Case 438905  --->

		<cfset listgetfinanceApproverID = valuelist(getfinanceApproverID.EntityID)>
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
		<cfset Emailtolist = ValueList(fundActivityQry.fundManagerID)><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<cfif getfinanceApproverID.recordcount GT 0>
			<cfset Emailtolist = ListAppend(Emailtolist, listgetfinanceApproverID, ",")>
		</cfif>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif statusQry.emailTextID1 neq "">
			<cfloop index = "approverID" list = "#Emailtolist#" delimiters = ",">
				<cfset popApproved = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
			</cfloop>
		</cfif>
		<!--- NJH 2008/01/28 MDF Phase 3 - send another email if supporting documentation has been approved --->
		<cfif statusQry.emailTextID2 neq "">
			<cfloop index = "approverID" list = "#Emailtolist#" delimiters = ",">
				<cfset paymentApproved = sendMDFEmail(emailTextID=statusQry.emailTextID2,personID=approverID,fundActivityQry=fundActivityQry)>
			</cfloop>
		</cfif>
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif popApproved and paymentApproved>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onPOPQueried" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var requiredDocuments = '' />
		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>
		<cfset form.startDate = fundActivityQry.startDate>
		<cfset requiredDocuments = getFundRequestTypeDocuments(fundRequestTypeID=fundActivityQry.fundRequestTypeID)>
		<cfset form.popRequired = valueList(requiredDocuments.document,"<br>")>
		<cfset form.fundRequestType = fundActivityQry.fundRequestType>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>

			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onPaid" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>
		<cfset form.paymentRef = fundActivityQry.PaymentRef>
		<cfset form.Paymentdate = fundActivityQry.PaymentDate>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>

			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>
			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onPaidTier1" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>

			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>
			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onExpired" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>

		<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>

		<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
		--->
		<cfreturn resultLastConf/>
		<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

	</cffunction>
	<!--- SSS p-lex039 new email function onfinanceapproval --->
	<cffunction access="public" name="onFinanceApproval" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfset var Emailtolist = "">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var getchannelmanagerID = '' />
		<cfset var approverID = '' />
		<cfset var qryBudgetGroupAccManager = '' />

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>
		<cfset form.approvedAmount = fundActivityQry.approvedAmount>

		<!--- START: 2011/05/04 - AJC - Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests --->
		<cfquery name="qryBudgetGroupAccManager" datasource="#application.siteDataSource#">
			select * from person p
			where personID IN
			(select data
			from IntegerFlagdata ifd INNER JOIN Flag f ON ifd.flagid = f.flagid
			where f.flagtextID  IN ( <cf_queryparam value="#application.com.settings.getSetting("fundManager.orgAccountManagerTextID")#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			and ifd.entityID =  <cf_queryparam value="#fundActivityQry.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>

		<cfloop query="qryBudgetGroupAccManager">
			<cfset Emailtolist = ListAppend(Emailtolist, personID, ",")>
		</cfloop>
		<!--- END: 2011/05/04 - AJC - Issue 6105: CR 28 - Fund Manager - Treat Inside Rep as Account Manager so that they can approve requests --->

		<!--- <cfset getchannelmanagerID = application.com.flag.getFlagData(flagId="AccManager", entityID=#fundActivityQry.PartnerOrgID#)>
		 ---><!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
			<cfset Emailtolist = ValueList(fundActivityQry.fundManagerID)>	<!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- <cfif getchannelmanagerID.recordcount GT 0>
			<cfset Emailtolist = ListAppend(Emailtolist, getchannelmanagerID.data, ",")>
		</cfif> --->

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif statusQry.emailTextID1 neq "">
			<cfloop index = "approverID" list = "#Emailtolist#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />	<!--- 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
			</cfloop>
		</cfif>

		<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
		--->
		<cfreturn resultLastConf/>
		<!--- END 2015-05-28 AHL P-AVI001 --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="onCancelled" returnType="boolean" hint="">
		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfset var resultConf = "false">
		<cfset var resultLastConf = "false"><!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

		<cfset form.fundRequestActivityID = fundActivityQry.fundRequestActivityID>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.comments = fundActivityQry.comments>
		</cfif>
 --->

		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<!--- START 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->
		<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>

			<!--- START 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfif statusQry.emailTextID1 neq "">
			<cfset var approverID = '' >

			<cfloop index = "approverID" list = "#ValueList(fundActivityQry.fundManagerID)#" delimiters = ",">
				<cfset resultConf = sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry)>
				<cfset resultLastConf = resultLastConf OR (resultConf eq "Email Sent" ) />
			</cfloop>
		</cfif>

			<!---
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
			--->
			<cfreturn resultLastConf/>
			<!--- END 2015-05-28 AHL P-AVI001 We suppose to send the email to all the fund Manager --->
		<cfelse>
			<cfreturn true>
		</cfif>
		<!--- END 2010-07-14 - AJC - P-LEX039 Exclude Fund manager emails --->

		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
	</cffunction>


	<cffunction access="public" name="onApprovalLevel" returnType="any" hint="">

		<cfargument name="fundActivityQry" type="query" required="yes">
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="statusQry" type="query" required="yes">
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfargument name="includeCommentsInEmail" type="boolean" default="false">

		<cfscript>
			var nextApprovalLevel = "0";						/* 2012/02/17 PPB Case 426208 */
			var qry_Update_ActivityLevel = "";
			var qry_get_approverList = "";
			var approverList = "";
			var resultConf = "";
			var approverID = '';
		</cfscript>

		<cfif statusQry.levelavailabilty neq "">	<!--- PPB - I put this condition around this block to defend against levelavailability being NULL on the db; should be set when fundStatusMethod="onApprovalLevel" --->
			<cfset nextApprovalLevel = statusQry.levelavailabilty+1>

			<cfquery name="qry_Update_ActivityLevel" datasource="#application.siteDataSource#">
				update fundRequestActivity set
				currentApprovalLevel =  <cf_queryparam value="#nextApprovalLevel#" CFSQLTYPE="CF_SQL_Integer" > ,
				updated=#createODBCDateTime(request.requestTime)#
				where fundRequestActivityID =  <cf_queryparam value="#fundActivityQry.fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		</cfif>

		<cfset qry_get_approverList = application.com.relayFundManager.getBudgetGroupApprover(budgetgroupID=fundActivityQry.BudgetGroupID,ApproverLevel=nextApprovalLevel)>		<!--- 2012/02/17 PPB Case 426208 send in nextApprovalLevel to stop emails going out to all approvers --->
		<!--- 2016/04/11	YMA	Fund approvers do not respect country assignments so breaking core to do so.  suggest moving this back into core. --->
		<cfset qry_get_approverList = filterApproverstoCountry(approversQry=qry_get_approverList,fundActivity=fundActivityQry)>
		<cfset approverList = valuelist(qry_get_approverList.personID)>

		<cfset form.companyName = fundActivityQry.fundAccountOrganisationName>		<!--- 2011/04/20 PPB was fundActivityQry.organisationName - crashed --->
		<cfset form.PartnerCountry = fundActivityQry.country>
		<cfset form.fundclaimamount = fundActivityQry.approvedAmount>
		<cfset form.fundRequestType = fundActivityQry.FundRequestType>
		<cfset form.fundclaimdescription = fundActivityQry.TypeDescription>

		<!--- 2011/05/12 PPB LID6566 - we want to write the comments entered on the form (during that visit) to the email if requested; in no circumstance do we want to get a comment from history to send --->
		<cfif NOT includeCommentsInEmail>
			<cfset form.Comments = "">
		</cfif>
<!---
		<cfset form.Comments = "">
		<cfif includeCommentsInEmail>
			<cfset form.Comments = fundActivityQry.comments>
		</cfif>
--->
		<!--- START: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->

		<cfif statusQry.emailTextID1 neq "">
			<cfloop index = "approverID" list = "#approverList#" delimiters = ",">
				<cfset resultConf=application.com.relayFundManager.sendMDFEmail(emailTextID=statusQry.emailTextID1,personID=approverID,fundActivityQry=fundActivityQry,cfmailtype="html")>
			</cfloop>
		</cfif>
		<!--- END: 2009-11-10 - AJC - P-LEX039 - status query for emailtextID --->
		<cfif resultConf eq "Email Sent">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="sendMDFEmail" returnType="boolean" hint="">
		<cfargument name="emailTextID" type="string" required="yes">
		<cfargument name="personID" type="numeric" required="yes">
		<!--- <cfargument name="BudgetGroupID" type="string" default=""> --->  <!--- NJH 2008/01/28 --->
		<cfargument name="ccAddress" type="string" default="">
		<cfargument name="cfmailtype" type="string" default="">			<!--- 2012/02/20 PPB fixed a typo was "maltype" --->
		<cfargument name="fundActivityQry" type="query" required="yes"> <!--- NJH 2008/01/28 --->
		<cfargument name="mergeStruct" type="struct" DEFAULT="#structNew()#"> 	<!--- 2011/03/30 PPB allow sending mergeStruct in (LID5963) --->

		<cfscript>
			var mdfEmailTextID = "";
			var BudgetGroupID = "";
			var result = "";
			var isApprover = '';
			arguments.mergeStruct.FundActivity = fundActivityQry;
		</cfscript>

		<cfset mdfEmailTextID = "#arguments.emailTextID#">
		<cfset BudgetGroupID = fundActivityQry.BudgetGroupID>
		<cfif application.com.email.doesEmailDefExist(emailTextID="#mdfEmailTextID##BudgetGroupID#")>
			<cfset mdfEmailTextID = "#mdfEmailTextID##BudgetGroupID#">
		</cfif>

		<!--- NJH 2008/01/28 MDF Phase 3 - don't send emails if the person that created the request activity is an approver --->

		<!--- AJC 2009/11/13 Not needed anymore as superceeded by Approval code <cfset isApprover = checkIfUserIsApprover(personID = fundActivityQry.createdBy)>

		<cfif not isApprover> ---->

			<cfset result=application.com.email.sendEmail(emailTextID=mdfEmailTextID,personID=arguments.personID,ccAddress=arguments.ccAddress,cfmailtype=arguments.cfmailtype,mergeStruct = mergeStruct)>

			<cfif result eq "Email Sent">
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>
		<!--- </cfif> --->

		<cfreturn true>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="makeFundActivityPayment" hint="Make a payment against a fund request activity">
		<cfargument name="fundRequestActivityID" type="numeric" required="yes">
		<cfargument name="paymentAmount" type="numeric" required="yes">
		<cfargument name="paymentRef" type="string" required="no">
		<cfargument name="paymentDate" type="date" default="#request.requestTime#">

		<cfset var makePayment = '' />

		<!--- NJH 2007/09/13 there is currently a 1 to 1 relationship between payments and activites. So, if it exists, update it --->
		<cfquery name="makePayment" datasource="#application.siteDataSource#">
			if exists (select * from fundRequestActivityPayment where fundRequestActivityID = #arguments.fundRequestActivityID#)
				update fundRequestActivityPayment set amount = #arguments.paymentAmount#, paymentDate = #arguments.paymentDate#, lastUpdated = GetDate(), lastUpdatedBy = #request.relayCurrentUser.personID# <cfif isDefined("arguments.paymentRef")>, paymentRef =  <cf_queryparam value="#arguments.paymentRef#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
					where fundRequestActivityID = #arguments.fundRequestActivityID#
			else
				insert into fundRequestActivityPayment (fundRequestActivityID,<cfif isDefined("arguments.paymentRef")>paymentRef,</cfif>amount,paymentDate,created,createdBy,lastUpdated,lastUpdatedBy)
				values (
					<cf_queryparam value="#arguments.fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif isDefined("arguments.paymentRef")>,<cf_queryparam value="#arguments.paymentRef#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
					,<cf_queryparam value="#arguments.paymentAmount#" CFSQLTYPE="cf_sql_numeric" >
					,<cf_queryparam value="#arguments.paymentDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					,GetDate()
					,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
					,GetDate()
					,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
				)
		</cfquery>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    	function:  getAccountFromBudgetGroupID
		NYB 2009-09-21 LHID2623

		WAB NOTE 2009/10/06 (LID 2734)
		This function doesn't appear to do what it is supposed to do
		It gets all the accounts which have are linked to a particular Budget Group (so could be hundreds of accounts)
		However when it is called from funds/fundrequestactivities.cfm in order to get an accountid it is expecting a single record back

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!--- ALEX HACK P-LEX039 to fix issue with new request not adding as it was joining to the fundrequest when no requests had previously been created  --->
	<cffunction access="public" name="getAccountFromBudgetGroupID" returnType="query" hint="Get the list of budget allocations for an account">
		<cfargument name="BudgetGroupID" type="numeric" required="true">
		<!--- START: 2009-12-07 - AJC - P-LEX039 Need to get by organisationID --->
		<cfargument name="OrganisationID" type="numeric" required="false" default="0">
		<!--- START: 2009-12-07 - AJC - P-LEX039 Need to get by organisationID --->
		<!--- START: 2011/04/28 - AJC - LID6081 - Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfargument name="AccountClosed" type="boolean" required="false" default="0">
		<!--- END: 2011/04/28 - AJC - LID6081 - Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfscript>
			var qryAccountDetails = "";
		</cfscript>
		<cfquery name="qryAccountDetails" datasource="#application.SiteDataSource#">
			select distinct fca.*
				from fundCompanyAccount fca
				<!--- fundRequest fr  --->
				<!--- on fca.accountID = fr.accountID --->
				inner join fundAccountBudgetAccess faba on faba.budgetID = fca.budgetID and faba.accountID = fca.accountID
				inner join budget b on b.budgetID = faba.budgetID and b.BudgetGroupID = #arguments.BudgetGroupID#
				<cfif OrganisationID neq 0>
					and fca.organisationID = #arguments.organisationID#
				</cfif>
				<!--- START: 2011/04/28 - AJC - LID6081 - Fund Manager - Requests can be made for future period without an approved allocated budget --->
				and fca.AccountClosed =  <cf_queryparam value="#AccountClosed#" CFSQLTYPE="CF_SQL_bit" >
				<!--- END: 2011/04/28 - AJC - LID6081 - Fund Manager - Requests can be made for future period without an approved allocated budget --->
		</cfquery>
		<cfreturn qryAccountDetails>
	</cffunction>
	<!--- ALEX HACK --->

	<!--- START: 2009-10-22 - AJC - Function to see if user is in budgetgroupapprover list --->
	<cffunction access="public" name="getBudgetGroupApprover" returnType="query" hint="gets all the budget group approvers">
		<cfargument name="BudgetGroupID" type="numeric" required="true">
		<cfargument name="ApproverLevel" type="numeric" required="false" default="0">
		<cfargument name="personID" type="numeric" required="false" default="0">

		<cfscript>
			var qry_get_budgetGroupApprovers = "";
		</cfscript>

		<cfquery name="qry_get_budgetGroupApprovers" datasource="#application.SiteDataSource#">
			select fundApproverID, Approver, ApproverLevel,approvalLevelPhrase, Countrydescription, personid, email, fundApproverID, 'removelink' as removelink, lowerApprovalThreshold, upperApprovalThreshold
			from vFundApprovers
			where fundApproverID IN (select fundApproverID from BudgetGroupApprover where budgetgroupID=#arguments.BudgetGroupID#)
			<cfif arguments.ApproverLevel neq 0>
				and ApproverLevel = #arguments.ApproverLevel#
			</cfif>
			<cfif arguments.personID neq 0>
				and personID = #arguments.personID#
			</cfif>
			order by ApproverLevel,approvalLevelPhrase
		</cfquery>

		<cfreturn qry_get_budgetGroupApprovers>
	</cffunction>

	<!--- START: 2009-10-22 - AJC - Function to see if user is in budgetgroupapprover list --->
	<cffunction access="public" name="isBudgetGroupApprover" returnType="boolean" hint="see if user is in budgetgroupApproverlist">
		<cfargument name="BudgetGroupID" type="numeric" required="true">
		<cfargument name="OrganisationID" type="numeric" required="false" default="0">
		<cfscript>
			var qry_get_budgetGroupApprover = "";
			var isApprover = false;
			var orgAccountManagerTextID = application.com.settings.getSetting("fundManager.orgAccountManagerTextID");
			var qry_get_budgetGroupApproverAccMng = '';
		</cfscript>
		<cfquery name="qry_get_budgetGroupApprover" datasource="#application.SiteDataSource#">
			select * from budgetgroupapprover bga
			inner join fundapprover fa on bga.fundapproverID = fa.fundapproverID
			where bga.BudgetGroupID = #arguments.BudgetGroupID#
			and fa.personID = #request.relaycurrentuser.personID#
		</cfquery>

		<cfif  qry_get_budgetGroupApprover.recordcount gt 0>
			<cfset isApprover = true>
		<cfelse>
			<cfif orgAccountManagerTextID neq "">

				<cfquery name="qry_get_budgetGroupApproverAccMng" datasource="#application.SiteDataSource#">
					select * from person p
					where personID IN
					(select data
					from IntegerFlagdata ifd INNER JOIN Flag f ON ifd.flagid = f.flagid WHERE f.flagtextID  IN ( <cf_queryparam value="#orgAccountManagerTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) and ifd.entityID=#arguments.OrganisationID# and ifd.data=#request.relaycurrentuser.personID#)
				</cfquery>
				<cfif qry_get_budgetGroupApproverAccMng.RecordCount gt 0>
					<cfset isApprover = true>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn isApprover>
	</cffunction>

	<cffunction name="getBudgetGroupsForOrganisation" access="public" returntype="query" validValues="true">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="isInternal" type="numeric" required="false" default="#request.relayCurrentUser.isInternal#">
		<cfargument name="personID" type="numeric" required="false" default="#request.relaycurrentuser.personid#">

		<cfset var qryGetBudgetGroups = "">
		<cfset var fundManagersQry = "">

		<cfif not arguments.isInternal>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID(fundManagerID=arguments.personID)>
		<cfelse>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID()>
		</cfif>

		<cfquery name="qryGetBudgetGroups" datasource="#application.siteDataSource#">
			select distinct bh.description, bh.BudgetGroupID from BudgetGroup bh inner join
				budget b on bh.BudgetGroupID = b.BudgetGroupID inner join
				fundAccountBudgetAccess faba on faba.budgetID = b.budgetID inner join
				fundCompanyAccount fca on faba.budgetID = fca.budgetID and faba.accountID = fca.accountID
			where accountClosed=0 and organisationID = #arguments.organisationID# and
					fca.accountID in (<cfqueryparam cfsqltype="cf_sql_integer" value="#valueList(fundManagersQry.data)#" list="true">)
		</cfquery>

		<cfreturn qryGetBudgetGroups>
	</cffunction>

	<cffunction name="getFundAccountsForPersonByUserGroup" access="public" returntype="query" validValues="true">
		<cfargument name="personID" type="numeric" required="false" default="0">
		<cfargument name="accountID" type="numeric" required="false" default="0">
		<cfset var qryGetFundAccountsForPersonByUserGroup = "">

		<cfquery name="qryGetFundAccountsForPersonByUserGroup" datasource="#application.siteDataSource#">
			SELECT DISTINCT imf.entityid as accountID,
					imf.data as accountUserGroupID,
					fca.OrganisationID,
					p.PersonID,
					rg.UserGroupID as personUserGroupID
			FROM dbo.integerMultipleFlagData imf
				INNER JOIN dbo.flag f on imf.flagid = f.FlagID and f.FlagTextID = 'ExtFundManagersUserGroups'
				INNER JOIN dbo.FundCompanyAccount fca on fca.AccountID = imf.entityid
				INNER JOIN dbo.Person p ON p.OrganisationID = fca.OrganisationID
				INNER JOIN dbo.RightsGroup rg on rg.PersonID = p.PersonID
			WHERE rg.UserGroupID = imf.data
			<cfif arguments.personID neq 0>
            	and p.PersonID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif arguments.accountID neq 0>
				and imf.entityid = <cf_queryparam value="#arguments.accountID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>

		<cfreturn qryGetFundAccountsForPersonByUserGroup>
	</cffunction>

	<cffunction name="getCoFunders" access="public" returntype="query">
		<cfargument name="organisationID" type="numeric" required="true">

		<cfset var qryGetCoFunders = "">
		<!--- first check if there are specific cofunders for the organisations country; if not get the cofunders for all countries --->
		<cfquery name="qryGetCoFunders" datasource="#application.siteDataSource#">
			select dataValue,validValue
			from validFieldValues vfv
			where fieldName = 'fundrequestactivity.cofunder'
			and vfv.countryId = (select countryId from organisation where organisationId = #arguments.organisationID#)
		</cfquery>

		<cfif qryGetCoFunders.recordcount eq 0>
			<cfquery name="qryGetCoFunders" datasource="#application.siteDataSource#">
				select dataValue,validValue
				from validFieldValues vfv
				where fieldName = 'fundrequestactivity.cofunder'
				and vfv.countryId = 0
			</cfquery>
		</cfif>

		<cfreturn qryGetCoFunders>
	</cffunction>

	<cffunction name="getBudgetGroup" access="public" returntype="query" hint="Returns a BudgetGroup">
		<cfargument name="budgetGroupID" type="numeric" required="true">

		<cfset var qryGetBudgetGroup = '' />

		<cfquery name="qryGetBudgetGroup" datasource="#application.siteDataSource#">
			SELECT * FROM budgetGroup WHERE budgetGroupID=#arguments.budgetGroupID#
		</cfquery>

		<cfreturn qryGetBudgetGroup>
	</cffunction>

	<cffunction name="getApprovalStatus" access="public" returntype="query" hint="Returns a query of a single Approval Status given its Text ID">
		<cfargument name="fundStatusTextID" type="string" required="true">

		<cfset var qryApprovalStatus = '' />

		<cfquery name="qryApprovalStatus" datasource="#application.siteDataSource#">
			select * from fundApprovalStatus where fundStatusTextID =  <cf_queryparam value="#arguments.fundStatusTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn qryApprovalStatus>
	</cffunction>

	<!--- 2011/04/01 PPB LID6101 moved this into CFC so Lenovo ver can easily override it --->
	<cffunction name="getBudgetPeriodAllocations" access="public" returntype="query" hint="Returns a query of a single Approval Status given its Text ID">
		<cfargument name="budgetPeriodAllocationID" type="numeric" required="false">

			<cfset var qryBudgetPeriodAllocations = '' />

			<cfquery name="qryBudgetPeriodAllocations" datasource="#application.siteDataSource#">
				select * from vBudgetAllocations
				<cfif StructKeyExists(arguments,"budgetPeriodAllocationID")>
					where budgetPeriodAllocationID=#budgetPeriodAllocationID#
				<cfelse>
					where 1=1
					<!--- 2012-07-24 PPB P-SMA001 commented out
					and countryid in (0,37,#request.RelayCurrentUser.countryList#,#request.RelayCurrentUser.regionList#)			<!--- 2012/02/13 PPB Case 424431 done in Lenovo custom CFC but should be done here too --->
					 --->
					#application.com.rights.getRightsFilterWhereClause(entityType="BudgetPeriodAllocation",alias="vBudgetAllocations",regionList="0,37,#request.relayCurrentUser.regionList#").whereClause#	<!--- 2012-07-24 PPB P-SMA001 --->	<!--- 2012-08-01 PPB Case 429848 changed entityType --->

					<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">	<!--- not ideal to include here but not easily avoidable cos can't do it later in a QoQ which doesn't support "N" prefix for unicode  --->
				</cfif>
			</cfquery>

		<cfreturn qryBudgetPeriodAllocations>
	</cffunction>

	<!---
		 2011/04/14 PPB this method is called to:
		 1) list all BudgetGroupPeriodAllocations for the list
		 2) get 1 row for the edit form (from the list) by sending budgetGroupPeriodAllocationID in
		 3) get 1 row for the edit budget form (to determine if there are sufficient BudgetGroup level funds available to allocate to the budget) by sending budgetGroupID and budgetPeriodID in
	 --->
	<cffunction name="getBudgetGroupPeriodAllocations" access="public" returntype="query" hint="Returns a query of Budget Group Period Allocations for a given budgetGroupPeriodAllocationID">
		<cfargument name="budgetGroupPeriodAllocationID" type="numeric" required="false">
		<cfargument name="budgetGroupID" type="numeric" required="false">
		<cfargument name="budgetPeriodID" type="numeric" required="false">

		<cfset var qryBudgetGroupPeriodAllocations = '' />

			<cfquery name="qryBudgetGroupPeriodAllocations" datasource="#application.siteDataSource#">
				select * , amount-amountAuthorised AS amountRemaining
				 from vBudgetGroupPeriodAllocations
				<cfif StructKeyExists(arguments,"budgetGroupPeriodAllocationID")>
					where budgetGroupPeriodAllocationID=#budgetGroupPeriodAllocationID#
				<cfelseif StructKeyExists(arguments,"budgetGroupID") and StructKeyExists(arguments,"budgetPeriodID")>
					where budgetGroupID=#budgetGroupID# and budgetPeriodID=#budgetPeriodID#
				<cfelse>
					where 1=1
					<!--- 2012-07-24 PPB P-SMA001 commented out
					and countryid in (0,37,#request.RelayCurrentUser.countryList#,#request.RelayCurrentUser.regionList#)		<!--- 2012/02/13 PPB Case 424431 done in Lenovo custom CFC but done here too for core ; 37=Any Country (should be safe to hard code) --->
					 --->
					#application.com.rights.getRightsFilterWhereClause(entityType="BudgetGroupPeriodAllocation",alias="vBudgetGroupPeriodAllocations",regionList="0,37,#request.relayCurrentUser.regionList#").whereClause#	<!--- 2012-07-24 PPB P-SMA001 --->	<!--- 2012-08-01 PPB Case 429848 changed entityType --->

					<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">	<!--- not ideal to include here but not easily avoidable cos can't do it later in a QoQ which doesn't support "N" prefix for unicode  --->
				</cfif>
			</cfquery>

		<cfreturn qryBudgetGroupPeriodAllocations>
	</cffunction>


	<!--- 2011/04/18 PPB LEX052 --->
	<cffunction access="public" name="addBudgetGroupPeriodAllocation" hint="Allocate an amount for a budget and period">
		<cfargument name="budgetGroupID" type="numeric" required="true">
		<cfargument name="budgetPeriodID" type="numeric" required="true">
		<cfargument name="allocatedAmount" type="numeric" required="true">
		<cfargument name="authorised" type="boolean" required="true">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="created" type="date" default="#createODBCDateTime(request.requestTime)#">


		<cfscript>
			var qryAddBudgetGroupPeriodAllocation = "";
		</cfscript>

		<cfquery name="qryAddBudgetGroupPeriodAllocation" DATASOURCE="#application.SiteDataSource#">
			insert into BudgetGroupPeriodAllocation (budgetGroupID,budgetPeriodID,amount,authorised,createdBy,created,lastUpdatedBy,lastUpdated)
			values (
				<cf_queryparam value="#arguments.budgetGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#arguments.budgetPeriodID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				,<cf_queryparam value="#arguments.allocatedAmount#" CFSQLTYPE="CF_SQL_decimal" >
				,<cf_queryparam value="#arguments.authorised#" CFSQLTYPE="CF_SQL_bit" >
				,<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#createODBCDateTime(arguments.created)#" CFSQLTYPE="cf_sql_timestamp" >
				,<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#createODBCDateTime(arguments.created)#" CFSQLTYPE="cf_sql_timestamp" >)
		</cfquery>

	</cffunction>


	<!--- 2011/04/18 PPB LEX052 --->
	<cffunction access="public" name="updateBudgetGroupPeriodAllocation" hint="Update the amount for a budget and period">
		<cfargument name="BudgetGroupPeriodAllocationID" type="numeric" required="true">
		<cfargument name="amount" type="numeric" required="true">
		<cfargument name="authorised" type="boolean" required="true">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="created" type="date" default="#createODBCDateTime(request.requestTime)#">


		<cfscript>
			var qryUpdateBudgetGroupPeriodAllocation = "";
		</cfscript>

		<cfquery name="qryUpdateBudgetGroupPeriodAllocation" DATASOURCE="#application.SiteDataSource#">
			update BudgetGroupPeriodAllocation set
				amount = #arguments.amount#,
				authorised =  <cf_queryparam value="#arguments.authorised#" CFSQLTYPE="CF_SQL_bit" > ,
				lastUpdated = #arguments.created#,
				lastUpdatedBy = #arguments.createdBy#
			where BudgetGroupPeriodAllocationID = #arguments.BudgetGroupPeriodAllocationID#
		</cfquery>

	</cffunction>


	<!--- 2011/04/18 PPB LEX052
		2016/01/19	NJH	JIRA 164 - removed the 'please select' - now set as null text in calling select--->
	<cffunction access="public" name="getBudgets_Dropdown" returnType="query" hint="Gets budgets to populate a dropdown">
		<cfargument name="budgetGroupID" type="numeric" required="false">

		<cfset var qryBudgets = "">

		<cfquery name="qryBudgets" datasource="#application.siteDataSource#">
			SELECT b.budgetID AS dataValue, b.description AS displayValue, 2 AS sortIndex
			FROM budget b
			WHERE b.budgetGroupID=<cf_queryparam value="#arguments.budgetGroupID#" cfsqltype="cf_sql_integer">
			ORDER BY sortIndex, displayValue
		</cfquery>

		<cfreturn qryBudgets>
	</cffunction>


	<!---
	2011/04/18 PPB LEX052
	returns budgetPeriod data for populating a dropdown
	if a budgetGroupID is sent in, then it returns only those periods for which there is not already an allocation for that BudgetGroup
	if a budgetID is sent in, then it returns only those periods for which there is not already an allocation for that Budget
	2016/01/19	NJH	JIRA 164 - removed the 'please select' - now set as null text in calling select
	--->
	<cffunction access="public" name="getBudgetPeriods_Dropdown" returnType="query" hint="Gets budgets to populate a dropdown">
		<cfargument name="budgetGroupID" type="numeric" required="false">
		<cfargument name="budgetID" type="numeric" required="false">

		<cfset var qryBudgetPeriods = "">

		<cfquery name="qryBudgetPeriods" datasource="#application.siteDataSource#">
			SELECT bp.budgetPeriodID AS dataValue, bp.description AS displayValue, 2 AS sortIndex
			FROM budgetPeriod bp
			WHERE 1=1
			<cfif structKeyExists(arguments,"budgetGroupID")>
				AND bp.budgetPeriodID NOT IN (select budgetPeriodID from budgetGroupPeriodAllocation where budgetGroupID = <cf_queryparam value="#arguments.budgetGroupID#" cfsqltype="cf_sql_integer">)
			</cfif>
			<cfif structKeyExists(arguments,"budgetID")>
				AND bp.budgetPeriodID NOT IN (select budgetPeriodID from budgetPeriodAllocation where budgetID = <cf_queryparam value="#arguments.budgetID#" cfsqltype="cf_sql_integer">)
			</cfif>
			ORDER BY sortIndex	--, displayValue
		</cfquery>

		<cfreturn qryBudgetPeriods>
	</cffunction>



	<!--- 2011/04/18 PPB LEX052 --->
	<!--- named getFundApprovalStatusByTextID cos getFundApprovalStatus already used  --->
	<cffunction access="public" name="getFundApprovalStatusByTextID" returnType="query" hint="Gets budgets to populate a dropdown">
		<cfargument name="fundStatusTextID" type="String" required="false">

		<cfset var qryFundApprovalStatus = "">

		<cfquery name="qryFundApprovalStatus" datasource="#application.siteDataSource#">
			select * from fundApprovalStatus
			<cfif structKeyExists(arguments,"fundStatusTextID")>
				 where fundStatusTextID =  <cf_queryparam value="#fundStatusTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		</cfquery>

		<cfreturn qryFundApprovalStatus>
	</cffunction>


	<!--- START: 2011/04/20 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
	<!--- COPIED FROM relayFundManagerWS.cfc --->
	<cffunction name="getOrganisationBudgetGroup" access="remote" hint="Returns a BudgetGroup">
		<cfargument name="budgetGroupID" type="numeric" required="true">
		<!--- START: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfargument name="organisationID" type="numeric" required="true">
		<!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

		<cfset var qryBudgetGroup = "" />
		<!--- START: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfset var qryBudgets = "" />
		<cfset var aBudgetGroup = "" />
		<cfset var budgetIDs = "" />
		<cfset var qryFundAccounts = '' />
		<cfset var qry_get_budgetperiodDates = '' />


		<cfset qryFundAccounts = application.com.relayFundManager.getAccountFromBudgetGroupID(budgetGroupID=arguments.budgetGroupID,organisation=arguments.organisationID) />

		<cfif qryFundAccounts.recordcount gt 0>

			<cfset qryBudgets = getAvailableBudgetsForAccount(accountID = qryFundAccounts.accountID )>

			<cfset budgetIDs = valuelist(qryBudgets.budgetID)>

		</cfif>

		<!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

		<cfset qryBudgetGroup=application.com.relayFundManager.getBudgetGroup(budgetGroupID=arguments.budgetGroupID)>

		<!--- START: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfif qryBudgetGroup.recordcount neq 0>
			<cfset aBudgetGroup = application.com.structureFunctions.QueryToArrayOfStructures(query=qryBudgetGroup) />
		<cfelse>
			<cfset aBudgetGroup = arrayNew(1) />
			<cfset arrayappend(aBudgetGroup,structnew()) />
		</cfif>
		<cfquery name="qry_get_budgetperiodDates" datasource="#application.sitedatasource#">
			select min(bp.startDate) as startDate, max(bp.endDate) as endDate from budgetPeriod bp
			inner join BudgetPeriodAllocation bpa on bp.budgetPeriodID = bpa.BudgetPeriodID and bpa.authorised = 1
			<cfif budgetIDs neq "">
				and bpa.budgetID  IN ( <cf_queryparam value="#budgetIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfelse>
				and bpa.budgetID = -1
			</cfif>

		</cfquery>

		<cfif qry_get_budgetperiodDates.endDate eq "">
			<!--- START: 2011-09-05 NYB Demo Project: Fund Request Bug - changed default from 01-01-2000 to NULL as this was disabling every date in the calender popup --->
			<cfset aBudgetGroup[1].budgetperiodEndDate = "NULL" />
			<cfset aBudgetGroup[1].budgetperiodStartDate = "NULL" /><!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
			<!--- END: 2011-09-05 NYB Demo Project: Fund Request Bug - the calender popup was disabled --->
		<cfelse>

			<cfset aBudgetGroup[1].budgetperiodEndDate = qry_get_budgetperiodDates.endDate />
			<cfset aBudgetGroup[1].budgetperiodStartDate = qry_get_budgetperiodDates.startDate /><!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		</cfif>


		<cfreturn aBudgetGroup>

	</cffunction>
	<!--- END: 2011/04/20 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

	<!--- 2011-11-30 NYB REL113
			Moved from Funds/fundRequests.cfm
	--->
	<cffunction access="public" name="getRequestsForManager" returnType="query" validvalues="true" hint="Gets Requests For Manager for Funds/fundRequests.cfm - external">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="locationID" type="numeric" default="#request.relayCurrentUser.locationID#">
		<cfargument name="ShowLiveAccountsOnly" type="boolean" required="No" default="1">

		<cfset var qryRequestsForManager = "">
		<cfset var isFundManagerPartner = application.com.relayFundManager.isFundManagerPartner(personID = arguments.personID)>
		<cfset var getAccountsForManager = getFundAccounts(fundManagerID=arguments.personID, ShowLiveAccountsOnly=arguments.ShowLiveAccountsOnly)>

		<cfquery name="qryRequestsForManager" datasource="#application.siteDataSource#">
				Declare @locationID int = <cf_queryparam value="#arguments.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				Declare @personID int = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				Declare @isFundManagerPartner int = <cf_queryparam value="#isFundManagerPartner#" CFSQLTYPE="CF_SQL_INTEGER" >

				select distinct fr.*,
					ba.budgetRemaining as fundsAvailable,					<!--- 2015-05-27 PPB Case 444781 --->
					case when count_requests > 0 then 'phr_Edit' else '' end as Edit,
					case when count_requests > 0 then case when fca.AccountClosed = 0 then 'phr_fund_ViewActivities' else 'phr_fund_ClosedActivities' end else '' end as Activities  <!--- did display phr_fund_NoActivities if there were no activities--->
					from (
							select
									v.fundRequestID,
									v.AccountID,
									v.budgetPeriodID,
									v.fundRequestID AS Request_ID,
									v.period,
									v.Account,
									v.countryID,
									v.country,
									v.BudgetGroup,
									v.BudgetGroupID,
									v.budgetID,
									SUM(v.child_approved_sum) as Sum_approved,
									SUM(v.child_requested_sum) as Sum_requested,
									SUM(v.child_request) as count_requests,
									v.OrganisationID,
									v.orgBudgetAllocation,
									v.budgetPeriodAllocationID
							from
								(SELECT
									vfrlb.*,

									case when ((vfrlb.createdby = @personID AND @isFundManagerPartner = 0) OR
									((vfrlb.locationID = @locationID or vfrlb.createdby = @personID)  AND @isFundManagerPartner = 1)) THEN vfrlb.Sum_approved else 0 end as child_approved_sum,

									case when ((vfrlb.createdby = @personID AND @isFundManagerPartner = 0) OR
									((vfrlb.locationID = @locationID or vfrlb.createdby = @personID)  AND @isFundManagerPartner = 1)) THEN vfrlb.Sum_requested else 0 end as child_requested_sum,

									case when ((vfrlb.createdby = @personID AND @isFundManagerPartner = 0) OR
									((vfrlb.locationID = @locationID or vfrlb.createdby = @personID)  AND @isFundManagerPartner = 1)) THEN 1 else 0 end as child_request

								FROM vFundRequestList_Base vfrlb

					<cfif getAccountsForManager.recordCount>
								WHERE vfrlb.AccountID in (<cfqueryparam cfsqltype="cf_sql_integer" value="#valueList(getAccountsForManager.accountID)#" list="true">)
					<cfelse>
								WHERE 1 = 0
					</cfif>
								 ) v

								GROUP BY
										v.fundRequestID,
										v.AccountID,
										v.budgetPeriodID,
										v.fundRequestID,
										v.period,
										v.Account,
										v.countryID,
										v.country,
										v.BudgetGroup,
										v.BudgetGroupID,
										v.budgetID,
										v.OrganisationID,
										v.orgBudgetAllocation,
										v.budgetPeriodAllocationID ) fr
					inner join vBudgetAllocations ba on fr.budgetPeriodAllocationID = ba.budgetPeriodAllocationID				<!--- 2015-05-27 PPB Case 444781 --->
					inner join fundCompanyAccount fca on fca.accountID = fr.accountID
		</cfquery>

		<cfreturn qryRequestsForManager>
	</cffunction>

	<cffunction access="public" name="isFundManagerPartner" returnType="boolean" hint="Check if person is Fund Manager Partner">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfset var result = false>

		<cfquery name="qryIsFundManagerPartner" datasource="#application.siteDataSource#">
			SELECT rg.PersonID, rg.UserGroupID
			FROM RightsGroup rg
			INNER JOIN UserGroup ug on rg.UserGroupID = ug.UserGroupID
			WHERE  ug.name = 'Fund Manager Partners' and rg.PersonID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif qryIsFundManagerPartner.recordcount gt 0>
			<cfset result = true>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction access="public" name="getFundRequestScopeForPerson" returnType="query"  hint="Get fund request scope for person by Fund Account Manager and Fund Manager Partner rules">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="locationID" type="numeric" default="#request.relayCurrentUser.locationID#">
		<cfset var qryGetFundRequestScopeForPerson = "">

		<cfset var getAccountsForManager = getFundAccounts(fundManagerID=arguments.personID, ShowLiveAccountsOnly=false)>

		<cfset var isFundManagerPartner = application.com.relayFundManager.isFundManagerPartner(personID = arguments.personID)>

		<cfquery name="qryGetFundRequestScopeForPerson" datasource="#application.siteDataSource#">
			SELECT fr.fundRequestID, fra.fundRequestActivityID, fra.createdBy, p.LocationID
			FROM fundRequest fr
			INNER JOIN fundRequestActivity fra on fr.fundRequestID = fra.fundRequestID
			INNER JOIN Person p on p.PersonID = fra.createdBy
			WHERE fr.AccountID in (<cfqueryparam cfsqltype="cf_sql_integer" value="#valueList(getAccountsForManager.accountID)#" list="true">)
			<cfif isFundManagerPartner >
            	and (fra.createdBy = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" > or
				p.LocationID = <cf_queryparam value="#arguments.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >)
			<cfelse>
            	and fra.createdBy = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>

		<cfreturn qryGetFundRequestScopeForPerson>
	</cffunction>

	<!--- 2011-11-30 NYB REL113
			for showTheseColumns cf_param in Funds/fundRequests.cfm
	--->
	<cffunction access="public" name="getRequestsForManagerColumnlist" validvalues="true" hint="Gets column names for Requests For Manager query">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfset var qryRequestsForManager = application.com.relayFundManager.getRequestsForManager(arguments.personID)>
		<cfset var result = "">

		<cfset var counter = 0>
		<cfset var i = ''>
		<cfquery name="result" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(1, 0, 0, 0)#">
			<cfloop index="i" list="#qryRequestsForManager.columnlist#"><cfoutput>
				<cfset counter = counter + 1>
				select '#i#' as display, '#i#' as value
				<cfif counter lt listlen(qryRequestsForManager.columnlist)>
					union
				</cfif>
			</cfoutput></cfloop>
		</cfquery>

		<cfreturn result>
	</cffunction>

	<!--- START 2015-10-27 AHL Kanban 1603 Customizing MDF Activity Columns by Settings. Moving Function to component --->
	<!---
		Returns the query used in fundRequestActivity to return the activities associated with one fund request
	--->
	<cffunction access="public" name="getFundRequestActivities" hint="Gets column names for Requests For Manager query">
		<cfargument name="fundRequestID" type="numeric" required="true" />
		<cfargument name="sortOrder" type="string" required="true" />
		<cfargument name="isManager" type="boolean" required="true" />
		<cfargument name="organisationID" type="numeric" default="#request.relayCurrentUser.organisationID#" />
		<cfargument name="isInternal" type="boolean" default="#request.relayCurrentUser.isInternal#" />
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="locationID" type="numeric" default="#request.relayCurrentUser.locationID#">

		<cfset var gryGetFundRequestScopeForPerson = "">

		<cfif not arguments.isInternal>
				<cfset gryGetFundRequestScopeForPerson = application.com.relayFundManager.getFundRequestScopeForPerson(personID=arguments.personID, locationID=arguments.locationID)>
		</cfif>

		<cfset var qryFundRequestActivities = ""/>

		<cfset var RequestActivityPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundRequestActivity", phraseTextID = "title")>

		<!--- START: 2011/03/16 AJC P-LEN023 - Add phrase snippets code for fundrequesttypes --->
		<cfset var RequestTypePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundRequestType", phraseTextID = "title",baseTableAlias="fra")>
		<!--- END: 2011/03/16 AJC P-LEN023 - Add phrase snippets code for fundrequesttypes --->

		<!--- START: 2011/03/28 PPB P-LEN023 - Add phrase snippets code for fundapprovalstatus --->
		<cfset var ApprovalStatusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "title",baseTableAlias="fra")>
		<cfset var ExternalStatusTextPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "ExternalStatusText",baseTableAlias="fra")>
		<!--- END: 2011/03/28 PPB P-LEN023 --->

		<!--- 2011/03/31 PPB LID6104 - add a column for number of proofs in list --->
		<cfquery name="qryFundRequestActivities" datasource="#application.SiteDataSource#">
			select distinct fra.*,o.organisationname as partnername,'phr_fund_upload_Proofs' as Upload_Proofs,
			(	select count(fileid)
		  		from
					RelatedFile as rf
					inner join RelatedFileCategory as rft on rf.FileCategoryID = rft.FileCategoryID
					left join Language as L on rf.Language = l.LanguageID
					left outer join person p on p.personid = rf.uploadedby
				where
					rf.EntityID = fra.fundRequestActivityID
					and rft.FileCategoryEntity = 'fundRequestActivity'
			) as numProofs

			<cfif arguments.isInternal>
				,#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)# as activityStatus
				<!--- fra.status as activityStatus --->
			<cfelse>
				,isNull(NullIf(#preservesinglequotes(ExternalStatusTextPhraseQuerySnippets.select)#,''), #preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)#) as activityStatus
				<!--- isNull(fra.externalStatusText,#preservesinglequotes(ExternalStatusTextPhraseQuerySnippets.select)#) as activityStatus	 --->
				<cfif arguments.isManager>
					,fca.AccountClosed
				</cfif>
			</cfif>

			,#preservesinglequotes(RequestActivityPhraseQuerySnippets.select)# as requestType
			,#preservesinglequotes(RequestTypePhraseQuerySnippets.select)# as fundRequestType
			from vFundRequestActivities fra
			inner join organisation o on fra.partnerOrganisationID = o.organisationID
			<!--- get only fund Request Activities that are in fund requests that a fund manager has access to --->
			<cfif arguments.isManager and not arguments.isInternal>
				inner join fundRequest fr on fra.fundRequestID = fr.fundRequestID
				inner join FundCompanyAccount fca on fr.accountID = fca.accountID and fca.organisationID = #arguments.organisationID#
			</cfif>

			 #preservesinglequotes(RequestActivityPhraseQuerySnippets.join)#
			 #preservesinglequotes(RequestTypePhraseQuerySnippets.join)#
			 #preservesinglequotes(ApprovalStatusPhraseQuerySnippets.join)#
			 #preservesinglequotes(ExternalStatusTextPhraseQuerySnippets.join)#
			where fra.fundRequestID =  <cf_queryparam value="#arguments.fundRequestID#" CFSQLTYPE="CF_SQL_INTEGER" >

			<cfif arguments.isManager and not arguments.isInternal>
                and fra.fundRequestActivityID in (<cfqueryparam cfsqltype="cf_sql_integer" value="#valueList(gryGetFundRequestScopeForPerson.fundRequestActivityID)#" list="true">)
			</cfif>

			order by #arguments.sortOrder#
		</cfquery>

		<cfreturn qryFundRequestActivities>
	</cffunction>
	<!--- END 2015-10-27 AHL Kanban 1603 --->

	<!--- 2016/04/11	YMA	Fund approvers do not respect country assignments so breaking core to do so.  suggest moving this back into core. --->
	<cffunction access="public" name="filterApproverstoCountry" returnType="query" hint="">
		<cfargument name="approversQry" type="query" required="yes">
		<cfargument name="fundActivity" type="query" required="yes">

		<cfset var approversQryFiltered = arguments.approversQry>
		<cfset var approversCountries = queryNew("")>

		<!--- loop through the original approvers query --->
		<cfloop query="approversQry">
			<!--- get the approvers country assignments --->
			<cfquery name="approversCountries">
				select c.CountryID from fundApprover fa
				join country cg on cg.CountryID = fa.countryID
				join CountryGroup cgm on cg.CountryID = cgm.CountryGroupID
				join country c on cgm.CountryMemberID = c.countryID and c.ISOCode is not null

				where fa.fundApproverID = #fundapproverID#
			</cfquery>

			<!--- test the approvers country rights against the country of the fund account --->
			<cfif listfind(valuelist(approversCountries.countryID),fundActivity.FUNDACCOUNTORGANISATIONCOUNTRYID) eq 0>
				 <cfquery name="approversQryFiltered" dbtype="query">
					SELECT * FROM approversQryFiltered where fundApproverID != #fundapproverID#
				</cfquery>
			</cfif>
		</cfloop>

		<cfreturn approversQryFiltered>
	</cffunction>
</cfcomponent>

