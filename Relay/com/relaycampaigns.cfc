<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			SWJ
Date created:	23-Jun-08

	This provides the set of functionality for campaigns

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/01/14 		NJH		LID 2702 - changed sourceID to vLeadListingCalculatedBudget.campaignID in "getCampaigns" function
2011-01-13		MS		LID:4314 Extended query getCampaigns to bring out only active campaigns when the argument active is passed as YEs
2016/02/10		NJH		JIRA PROD2015-566 - added new function to get campaigns for dropdowns. Currently returns all active, non-expired campaigns.
--->


<cfcomponent displayname="relayCampaigns" hint="Provides facilities for managing campaign information.">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the set of available campaigns.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getCampaignListing" returntype="query" hint="Returns a regions query object which can be filtered.">
		<cfargument name="sortOrder" required="no" default="">

		<cfset var getCampaigns = "">

		<!--- NJH 2010/01/14 LID 2702 - changed sourceID to vLeadListingCalculatedBudget.campaignID --->
		<CFQUERY NAME="getCampaigns">
			SELECT * ,
			(select count(1) from communication
				where sent = 1 and commFormLID = 2 and campaignID = c.campaignID) as Communications,
			(select count(1) from opportunity
				where campaignID = c.campaignID) as total_Opps,
			(select sum(calculated_budget) from vLeadListingCalculatedBudget
				where campaignID = c.campaignID) as total_Opp_Value,
			(select sum(calculated_budget) from vLeadListingCalculatedBudget
				where probability = 100 and campaignID = c.campaignID) as total_Business
			from campaign c
			<cfif arguments.sortOrder is not "">order by #arguments.sortOrder#</cfif>
		</CFQUERY>

		<cfreturn getCampaigns>
	</cffunction>


	<cffunction name="getCampaigns" returntype="query" hint="Returns active campaigns for various dropdowns." validValues="true">
		<cfargument name="currentValue" type="string" required="false">

		<cfset var getCampaignsQry = "">

		<cfquery name="getCampaignsQry" >
			select campaignId, campaignName
			from campaign c
			where (startDate <= getDate()
				and isNull(endDate,getDate()+1) >= getDate()
				and active = 1
				and isNull(complete,0) = 0)
			<cfif structKeyExists(arguments,"currentValue") and isNumeric(arguments.currentValue) and arguments.currentValue neq 0>
				or campaignId = <cf_queryparam value="#arguments.currentValue#" cfsqltype="cf_sql_integer">
			</cfif>
			order by campaignName
		</cfquery>

		<cfreturn getCampaignsQry>
	</cffunction>

</cfcomponent>