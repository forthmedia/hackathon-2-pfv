<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2005-06-29 added function getPossibleCountryFromBrowser (OK not really to do with language, but similar to gettingn language from browser)
WAB 2008/06/25 Made a change to the localisation of variables in when evaluate phrases
WAB 2008/07/09  Change to loadPhrasesFromFile so that can load phrases from userfiles
WAB 2008/09/30  change to checkForAndEvaluateCFVariables - sort problems with using the form structure
WAB 2008/10/14  more changes to checkForAndEvaluateCFVariables to try and bypass problems with using the form structure - don't try and copy it unless needed
WAB 2008/11/19  LID:1199 Ignore re-evaluation of bad expression if we still can't evaluate it on a retry
WAB 2009/02/11 mods to function getAllNotTranslatedLanguagesForAPhraseFilteredForCurrentUser to allow result from a non existent phrase
		added function to get default translation of a phrase (for elementEdit)
WAB 2009/02/12 added TranslateString Function
WAB 2009/02/23  added ability to load entity related translations from a phrase file
WAB 2009/03/04 changed regExp for checking for CF variables - requires FULL testing
WAB 2009/03/04 lots of new functions moved out of CF_translate - became CF_translateV3
WAB 2009/03/23  LID 1997 adding loop and conditional support to phrases
WAB 2009/04/29  Changes to allow countryGroups to appear in translation dropdowns
NYB 2009-07-21 Sophos LHID2462 - added additional criteria to cfif in getPhrasesFromDB function.
				Search for entitytype in EntityTypeID struct, and then confirm it doesn't exist as a PhraseTextID before treating like a non-standard phrase (ie, with EntityID > 0).
				NB: Initial sites receiving this should be tested for performance issues on browsing external pages - as results inconclusive on dev
WAB 2009/09/07 LID 2515 Removed #application. userfilespath#   - getPhraseFile function
WAB 2009/09/09
WAB 2009/09/16 LID 2572 mod to addNewPhraseAndTranslationV2 and ProcessPhraseUpdateForm - prevent phraseTextID of pattern string_string_number
WAB 2009/11/09    alteration to resetPhraseStructureKey so that i it can handle a list of phrasetextids
		mod to addPhrasesFromLoadFile to take advantage of above
NYB 2009-10-06 	LHID2728 - added cfif around relay_loop code as the lack of validation around it causes it to fall over when values passed are not in format expected
WAB 2009/11/09 mods to addNewPhraseAndTranslationV2 and ProcessPhraseUpdateForm so that javascript in phrases does not appear in debug
WAB 2010/03/17		P-SOP019 Bonanza  - error in checkForAndEvaluateCFVariables if the form scope does not exist (ie when called as part of a webservice)
WAB 2010/03/21		P-SOP019 Bonanza  - still problems (merge variables not getting evaluated)
WAB 2010/03/25 added extractListOfJavascriptPhrasesFromAString to deal with phrases of form phr.xxxx in .js files
WAB 2010/05/26 lots of varing
PPB 2010/08/10 P-SOP024 changed application.settings.Translation.CountryGroupList to use CountryGroupTypeTextId
PPB 2010/08/11 P-SOP024 add Translation country groups to drop-down for translation of a site page
WAB 2010/09/15	Implementation of phrasePointerCache
WAB 2010/10/05	Added support for defaultLanguageForCountry to the phraseWidget, needed some mods to the translatePhrases sp as well
NYB 2010/10/18 fixed error in addPhrasesFromLoadFile with too many variables (and the wrong type) being passed to regExp/refindAllOccurrences
WAB 2010/12/?	Added function getPhrasePointerQuerySnippetsForTextID
WAb 2011/03/09  Altered so that some of the functions will run using ApplicationScope. This was so that we could translate error messages thrown by the sitewide error handler - which does not have access to the standard #application# scope because it does not hit application.cfc
WAB 2011/05/05  Fixed Problem with processing phrasetextIDs where a substring looked like an entityPhrase  (eg phr_AAA_BBB_123_XXX)
WAB 2011/05/05 Added dropcachedMergeObject() function (used in merge.cfc) - at time of LID 6501
NYB 2011/07/04 P-LEN024
WAB 2011/09/28 WAB added function translateStructure() which translates all the keys of a structure
WAB 2011/10/13	LID 7982 Need to convert {{relay_ }} to <relay_  > before saving phrases
				Actually at same time I have support for {{relay_  }}, but for time being will keep db with  < > form
WAB 2011/11/10 made checkForAndEvaluateCFVariables actually do a check before loading the merge object.  old checkForAndEvaluateCFVariables became created EvaluateCFVariables()
				modification to function deleteCachedMergeObject()
NYB 2011-12-09 LHID8224 added for validation around listOfPhrases loop of getPhrasesFromDB function
WAB 2012/02/16 Added caching of translations in .js files (see cf_includeJavascriptOnce)
WAB 2012-02-28 CASE:426873 remove unwanted stuff added by CKEditor when adding/updateing phraseText
		Reorganised existing code into a single function
WAB 2012-04-19 CASE 427676 Don't even retrieve/evaluate phrases in doNotTranslate blocks
WAB 2012-05-2012 CASE 426912. Fixed problem with translation links
IH	2012-08-08	Add arguments.phraseTableAlias to allow a table to be used multiple times
WAB	2012-10-03	knock on from CASE 431031 changed attribute dontSendEvaluationErrorWarningEmail to more understandable logMergeErrors (and NOT'ed)
IH	2012-12-07	Case 432479 only add PhraseTextIDs2 to results query if phraseTextIDList is not empty
WAB 2013/02/05 	CASE 433656 alter doctorPhraseTextBeforeAddingToDatabase() so does not escape '' - now dealt with by CFQUERYPARAM
WAB 2013-03-18  Don't add translation links if encoding for javascript
WAB 2013-05-15 CASE 435127 added new function to doctorPhraseTextBeforeAddingToDatabase to deal with image size in style attribute
WAB 2013-12-17 Mod in ProcessPhraseUpdateForm() to ignore phr_*****_required_ fields (occurs when phrUpd_ field is changed from required to not required by javascript)
NYB 2014-02-26	Case 438849 changed how getPhrasePointerQuerySnippetsForTextID is done to use changes Will has made to translations/phrasepointercache
WAB 2014-09-04  Core-797 Deal with untranslated phrases on portal (within relayTags) which were outputting as blank rather than as phraseTextID (
WAB	2014-11-25	CASE 442080 - reinstate translation Ts and logging.
WAB 2015-02-10	Jira Fifteen-117 Modified regexp to deal with there being no HTML at all in the string (or the phr_ being at the beginning or end of the string)
WAB 2015-03-10  CASE 442080 continued. Code to prevent Translation Ts appearing inside of Anchor tag (work done some time ago but failed to commit!)
				Altered text returned when phrase is null - convert _ to blank and remove sys_
2015-03-12	RPW	FIFTEEN-282 - Message to Activity Stream - Record Title and Text missing from Messages table if created after clicking Save and Add New
2015-07-02	WAB	Fix in function isLogRequestsSet() - need to test for session. Sometimes called from sitewideErrorHandler before session is created
2015/11/30  SB Bootstrap
2016-01-14	WAB	JIRA PROD2015-513 Implement an 'overwrite if unedited' option in AddNewPhraseAndTranslation for use when loading phrase files
				Took opportunity to remove 2 different versions of adding/updating a phrase and replace with a call to existing stored procedure (which was based on one of the sets of code anyway)
2016-01-28 	WAB Added support for CFML comments in phrase files
2016-01-28	WAB	BF-255 Performance. Add a cache of phraseTextIDs which do not exist
2016-05-03	WAB	BF-673 Translation T Links not appearing correctly
				Also discovered (during unit testing) a conflict between adding T Links and DoNotTranslate Blocks.  Both were using the same placeHolderString for their temporary placeholders.
2016-11-29 WAB PROD2016-2837 Errors when doAnalysis=true, especially when no phrases to analyse
2017-02-27	WAB	RT-311 Translation system blows up if it comes across phrase of form  phr_XXX_YYY_099   
						ie entityPhrase with leading 0 on the entityID.
						Alter regexp so that will not match to entityPhrase if it is 
2017-03-02	WAB PROD2016-3518 Minor change so that SiteWideErrorHandler does not throw an error when its error message is displayed (application is not defined at this point, so need to refer to variables.applicationScope)
--->


<cfcomponent displayname="Relay Translations" hint="Manages translation related jobs">

	<!--- WAB 2009/03/04  all these regExp stored here --->
	<cfset this.regExp = structNew()>
	<cfset this.regExp.PhraseTextID_EntityName_EntityID = "([a-zA-Z0-9]+)_([a-zA-Z0-9]+)_([1-9][0-9]*)">
	<cfset this.regExp.PhraseTextID_EntityName_EntityID_WholeString =  "\A" & this.regExp.PhraseTextID_EntityName_EntityID & "\Z" >
	<cfset this.regExp.FindPhraseInString = "(phr_[a-zA-Z0-9_]+)([^a-zA-Z0-9_]|$)">
	<cfset this.regExp.FindJavascriptPhraseInString = "phr\.([a-zA-Z0-9_]+)([^a-zA-Z0-9_]|$)">
	<cfset this.regExp.FindPhrUpd_Country_Language = "phrupd_([a-zA-Z0-9_]+)_([0-9]+)_([a-zA-Z0-9]+)_([0-9\-]+)_([0-9]+)$">  <!--- note that entityid can be a string (for a new record) and entityType is numeric --->
	<cfset this.regExp.FindPhrUpd_WithoutCountryLanguage = "phrupd_([a-zA-Z0-9_]+)_([0-9]+)_([a-zA-Z0-9]+)[_]*$">
	<cfset this.regExp.RelayLoopIfOrComment = "RELAY_IF|RELAY_LOOP|<!---">
	<cfset this.regExp.doNotTranslate = "(<!-- StartDoNotTranslate -->)(.*?)(<!-- EndDoNotTranslate -->)">
	<!--- These all used in merging--->
	<!--- WAB 2011/05/17 altered the square bracket query to deal with nested brackets.
			was <cfset this.regExp.SquareBrackets = "\[\[([[:graph:] ]*?)\]\]">
		    When used in a loop with reFindSingleOccurrence it will find the inner brackets first and then the outer ones
			--->
	<cfset this.regExp.SquareBrackets = "\[\[([a-zA-Z_](?:(?!\[\[).)*?)\]\]">  <!--- matches square brackets without nested square brackets. WAB 2012--3-4 added check for first character after [[ being a-Z_, allowed lenovo to use [[[ something ]]] notation in their emails--->
	<cfset this.regExp.HashedVariable = "##([a-zA-Z_][A-Za-z0-9_.]*?)##">
	<!--- WAB 2013-01-07 CASE 432644, removed relayIF regexps, no longer used --->
	<cfset this.regExp.Comments = "\<\!---(((?!<\!---).)*?)---\>">
	<cfset this.regExp.SQLComments = "/\*(((?!\*/).)*?)\*/">
	<cfset this.regExp.relayLoop = "<relay_Loop (.*?)>(.*?)</relay_loop>">
	<cfset this.RegExp.relayTag="(?:\{\{|<)(RELAY_[0-9A-Z_]+)(.*?)(?:\}\}|>)" >  <!--- WAB 2011/10/13 added support for relay tags in form {{ }} incase any accidentally get into the db, although for time being we are converting back to < > on saving 2012-02-21 needed to escape the { and } for using java matcher --->
	<cfset this.RegExp.requiresEvaluating = "#this.regExp.SquareBrackets#|#this.regExp.HashedVariable#|#this.regExp.RelayTag#">  <!--- relaytag will pick up relay_loop and relay_if, probably don't need to worry about comments --->


	<cffunction access="public" name="addNewPhraseAndTranslationV2" hint="Inserts a phrase with a translation in a specific language (defaulted to english)" output="false">
		<!--- add new phrase

		WAB took code from addNewPhraseAndTranslation and made another function which does not require the
		THIS scope and so can be safely called in the application.com. form

		Left the old function for back ward compatability (only used in phrase loading)


		2007/09/11   WAB a few mods with the feedback

		2007/11/20    WAB discovered that this code didn't work for entityTypeID not 0!  fixed
		--->

			<cfargument name="phraseTextID" default = "">
			<cfargument name="userID" default ="#request.relayCurrentUser.UsergroupID#" type="numeric">
			<cfargument name="entitytypeid" default = "0" type="numeric">
			<cfargument name="entityid" default = "0" type="numeric">
			<cfargument name="LanguageID" default ="1" type="numeric">
			<cfargument name="countryID" default ="0" type="numeric">
			<cfargument name="phraseText" default = "">
			<cfargument name="addMode" type="boolean" default="true"> <!--- true adds, false doesn't add new --->
			<cfargument name="updateMode" type="string" default="overwrite">
			<cfargument name="datasource" type="string" default="#application.sitedatasource#">
			<cfargument name="updateCluster" type="boolean" default="true">


			<cfset var checkPhraseTextID = "">
			<cfset var result = structNew()>
			<cfset var addNewPhraseAndTranslationQry = "">

			<!--- WAB 2012-02-28 CASE:426873 removed 4 separate function calls into a new function --->
			<cfset arguments.phraseText = doctorPhraseTextBeforeAddingToDatabase(arguments.phraseText)>

			<!--- WAB 2009/09/16 LID 2572 check that phraseTextID doesn't match the pattern for an entity phrase - this leads to problems when trying to display --->
			<cfset checkPhraseTextID = isPhraseTextIDValid (PhraseTextID)>
			<cfif not checkPhraseTextID.isOK>
				<cfoutput>PhraseTextID #phraseTextID# is invalid.  #checkPhraseTextID.message#</cfoutput>
				<CF_ABORT>
			</cfif>

			<CFQUERY NAME="addNewPhraseAndTranslationqry" DATASOURCE="#dataSource#">
				exec addNewPhraseAndTranslation
					  @phraseTextID		= <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
					, @entitytypeid		= #entitytypeid#
					, @entityid			= #entityid#
					, @LanguageID		= #LanguageID#
					, @countryID		= #countryID#
					, @phraseText		= <cf_queryparam value="#phraseText#" CFSQLTYPE="CF_SQL_VARCHAR" >
					, @updateMode		= #listfindnocase("overwrite,overwriteUnedited",updateMode)?1:0#
					, @onlyUpdateUnedited  = #listfindnocase("overwriteUnedited",updateMode)?1:0#
					, @lastUpdatedBy	= #userID#

			</CFQUERY>


		<cfif entityTypeID is 0>
				<cfset resetPhraseStructureKey(phraseTextID=phraseTextID,updateCluster=arguments.updateCluster)>
				<cfset result.phraseTextID = phraseTextID>
		<cfelse>
			<!--- <cfoutput>updated: #phraseTextID#_#application.entityType[entityTypeID].tablename#_#entityID#</cfoutput> --->
				<cfset result.phraseTextID = "#phraseTextID#_#application.entityType[entityTypeID].tablename#_#entityID#">
				<cfset resetPhraseStructureKey(phraseTextID=result.phraseTextID,updateCluster=arguments.updateCluster)>
		</cfif>



		<cfif addNewPhraseAndTranslationqry.translationAddedUpdated>
			<cfset result.message = "#phraseTextID# set to #phraseText#">
		<cfelse >
			<cfset result.message = "">
		</cfif>
			<cfset result.details = addNewPhraseAndTranslationqry>

		<cfreturn result>

		</cffunction>


	<!--- WAB LID 2572 various functions to ensure that phrasetextids are valid and unique --->
	<cffunction name="isPhraseTextIDValid" hint="Checks whether a non-entity phraseTextID is valid, does not check for uniqueness">
		<cfargument name="phraseTextID" type="string" required="yes">

			<cfset var result = {isOK = true, message = ""}>
			<!---  check for non alphanumeric characters--->
			<cfset var regExpCheck = refindNoCase("[^0-9a-zA-Z_]",phraseTextID)>
			<cfset var PhraseTextIDLength = "">

			<cfif regExpCheck is not 0>
				<cfset result.isok = false>
				<cfset result.message = listappend(result.message,"Phrase Identifier can only contain characters 0-9, A-Z and _","|")>
				<cfreturn result>
			</cfif>

			<!--- check if the phraseTextID looks like an entity translation --->
			<cfset regExpCheck = refindNoCase(this.regexp.PhraseTextID_EntityName_EntityID_wholeString,phraseTextID)>
			<cfif regExpCheck is not 0>
				<cfset result.isok = false>
				<cfset result.message = listappend(result.message,"Phrase Identifier cannot be of the form XXXX_XXXX_999","|")>
				<cfreturn result>
			</cfif>

			<!--- also disallow   anything ending in _9999_9999 because this can look like _languageid_countryid to some of the code--->
			<cfset regExpCheck = refindNoCase("_([0-9]+)_([0-9]+)\Z",phraseTextID)>
			<cfif regExpCheck is not 0>
				<cfset result.isok = false>
				<cfset result.message = listappend(result.message,"Phrase Identifier cannot end in _999_999","|")>
				<cfreturn result>
			</cfif>

			<!--- WAB 2009/10/07 check not too long for db (after 2009-04-17 NYB Sophos in update phrase) --->
			<cfset PhraseTextIDLength = application.com.dbTools.getTableDetails(tableName="PhraseList",returnColumns="true",columnName="PhraseTextID").Column_Length>
			<cfif len(phraseTextID) gt PhraseTextIDLength>
				<cfset result.isok = false>
				<cfset result.message = listappend(result.message,"Phrase Identifier cannot be more than #PhraseTextIDLength# characters long","|")>
				<cfreturn result>
			</cfif>

			<!--- WAB noticed something odd here, we append the messages together with |, but then immediately do a cfreturn so never get more than one --->

		<cfreturn result>
	</cffunction>


	<!--- 2016-01-28	WAB	BF-255 Performance. Added cache of nonExistent phrases, and look in existing cache for existing phrases --->
	<cffunction name="doesPhraseTextIDExist" hint="checks for pre-existence of a non-entityPhraseTextID">
		<cfargument name="phraseTextID" type="string" required="yes">

		<cfset var result = true>
		<cfset var checkphrasetextID = "">

		<!--- Phrases which have already been loaded will be in application.phraseStruct, but it is conceivable that a non existant phrase has been requested - which will lead to a "_" key in the structure  --->
		<cfif structKeyExists (application.phraseStruct, phraseTextID) and not structKeyExists (application.phraseStruct[phraseTextID],"_")>
			<cfset result= true>
		<cfelseif structKeyExists (application.phraseNotExistsStruct, phraseTextID)>
			<cfset result= false>
		<cfelse>
			<cfquery name="checkphrasetextID" datasource = #application.sitedatasource#>
			select count(1) as thecount from phraseList where phraseTextID =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and entitytypeid = 0
			</cfquery>

			<cfif checkphrasetextID.theCount is 0>
				<cfset result = false>
				<cfset application.phraseNotExistsStruct[phraseTextID] = "">
			<cfelse>
				<cfset result = true> <!--- this phrase will now presumably be displayed and get into the phraseStruct for next call --->
			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="createValidUniquePhraseTextID" hint="checks for pre-existence of a non-entityPhraseTextID">
		<cfargument name="phraseTextID" type="string" required="yes">

		<cfset var counter = 1>
		<cfset var result = phraseTextID>
		<cfset var tmpPhraseTextID = "">

		<!--- NJH 2012/02/16 CASE 424858: added the line below to remove anything that is not valid in a textID column --->
		<cfset result = reReplaceNocase(result,"[^0-9a-zA-Z_]","","ALL")>

		<cfloop condition ="not isPhraseTextIDValid(result).isok">
			<cfset result = replace (result,"_","","ONCE")>
		</cfloop>

		<cfset tmpPhraseTextID = result>
		<cfloop condition ="doesPhraseTextIDExist(result)">
			<cfset result = tmpPhraseTextID & counter>
			<cfset counter = counter + 1>
		</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="getCrossSellingPhrases" hint="Returns a query object with associated cross-selling phrases">
		<cfargument name="opportunityID" type="numeric" required="no" default=0>

		<cfset var qCrossSellingPhrases = "">

		<cfquery NAME="qCrossSellingPhrases" DATASOURCE="#application.sitedatasource#">
			SELECT DISTINCT entityid as productID
			FROM 	phraselist pl INNER JOIN phrases p ON pl.phraseid = p.phraseid
			WHERE 	phrasetextid = 'crossSelling'
				AND entitytypeid IN (SELECT entitytypeid FROM schematable WHERE entityname='product')
				AND entityid IN (SELECT ProductOrGroupId FROM opportunityProduct WHERE opportunityid = #arguments.opportunityID# and productOrGroup = 'P')
		</cfquery>

		<cfreturn qCrossSellingPhrases>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setLanguageVarsFromISOCode" hint="">
		<cfargument name="lang" required="Yes" type="string">

		<cfset var langReturnVar = "">
		<!--- check that we have a valid languageISOCode --->
		<cfif structKeyExists(application.languageLookupFromISOCodeStr,arguments.lang)>
			<CFSET langReturnVar = application.languageIDLookupStr[arguments.lang]>
			<CFSET request.currentLanguageID = application.languageIDLookupStr[arguments.lang]>
			<cfset application.com.globalFunctions.cfcookie(NAME="defaultLanguageID", VALUE="#application.languageIDLookupStr[arguments.lang]#")>
			<cfset application.com.globalFunctions.cfcookie(NAME="language", VALUE="#application.languageLookupFromISOCodeStr[arguments.lang]#")>
		<cfelse>
			<CFSET langReturnVar = "English">
			<CFSET request.currentLanguageID = "1">
			<cfset application.com.globalFunctions.cfcookie(NAME="defaultLanguageID", VALUE="1")>
			<cfset application.com.globalFunctions.cfcookie(NAME="language", VALUE="English")>
		</cfif>

		<cfset application.com.RelayCurrentUser.setLanguage(cookie.defaultLanguageID)>
		<cfreturn langReturnVar>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setLanguageVarsFromLanguageID" hint="">
		<cfargument name="langID" type="numeric" required="yes">
		<cfset var langReturnVar = "">

		<!--- check that we have a valid languageISOCode --->

		<cfif structKeyExists(application.languageLookupFromIDStr,arguments.langID)>
			<CFSET langReturnVar = application.languageLookupFromIDStr[arguments.langID]>
			<CFSET request.currentLanguageID = arguments.langID>
			<cfset application.com.globalFunctions.cfcookie(NAME="defaultLanguageID", VALUE="#arguments.langID#")>
			<cfset application.com.globalFunctions.cfcookie(NAME="language", VALUE="#application.languageLookupFromIDStr[arguments.langID]#")>

		<cfelse>
			<CFSET langReturnVar = "English">
			<CFSET request.currentLanguageID = "1">
			<cfset application.com.globalFunctions.cfcookie(NAME="defaultLanguageID", VALUE="1")>
			<cfset application.com.globalFunctions.cfcookie(NAME="language", VALUE="English")>
		</cfif>

		<cfset application.com.RelayCurrentUser.setLanguage(cookie.defaultLanguageID)>
		<cfreturn langReturnVar>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUsersPreferredLangFromBrowser" hint="Returns the users preferred language">
				<cfargument name="liveLangs" type="string" default="#request.currentsite.liveLanguages#">
		<cfargument name="defaultLang" type="string" default="EN"> <!--- WAB added this 2005-06-29 because I wanted to be able to return blank if the language wasn't set --->

		<cfset var lang = "">

		<cfif isdefined("cgi.HTTP_ACCEPT_LANGUAGE")>

			<cfif listFindNoCase(liveLangs,left(listfirst(cgi.HTTP_ACCEPT_LANGUAGE),2))>

				<cfset lang = left(listfirst(cgi.HTTP_ACCEPT_LANGUAGE),2)>

			<cfelseif listlen(cgi.HTTP_ACCEPT_LANGUAGE) gt 1 and listFindNoCase(liveLangs,left(listgetAt(cgi.HTTP_ACCEPT_LANGUAGE,2),2))>
				<cfset lang = left(listgetAt(cgi.HTTP_ACCEPT_LANGUAGE,2),2)>

			<cfelseif listlen(cgi.HTTP_ACCEPT_LANGUAGE) gt 2 and listFindNoCase(liveLangs,left(listgetAt(cgi.HTTP_ACCEPT_LANGUAGE,3),2))>
				<cfset lang = left(listgetAt(cgi.HTTP_ACCEPT_LANGUAGE,3),2)>

			<cfelse>

				<cfset lang = "#defaultLang#">
			</cfif>
		<cfelse>
				<cfset lang = "#defaultLang#">
		</cfif>

		<cfreturn lang>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

WAB 2005-06-29
to be used in relaycurrentuser to set the language
2006-11-06 added listfirst - column has been changed to a varchar
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getDefaultLanguageForACountry" hint="returns the default language for a given country" returns="numeric">
		<cfargument name="countryid" type="numeric" required="true">

		<cfset var getDefaultLanguage = "">

		<CFQUERY NAME="getDefaultLanguage" DATASOURCE="#application.sitedatasource#">
		select defaultLanguageID from country where countryid = #countryid#
		</CFQUERY>

		<cfreturn listfirst(getDefaultLanguage.defaultLanguageID)>

	</cffunction>


<!---
GCC May 2007

 --->
	<cffunction access="public" name="getCountryLanguages"  hint="">

		<cfargument name="countryID" type="numeric" required="true">

		<cfscript>
			var getCountryLanguages = "";
			var countryLanguageIDlist = "";
			var getLanguages = "";
			request.languageIDLookupStr = structNew();
			request.languageLookupFromIDStr = structNew();
			request.languageIDLookupFRomNameStr = structNew();
		</cfscript>

		<CFQUERY NAME="getCountryLanguages" DATASOURCE="#application.siteDataSource#">
			SELECT defaultLanguageID
			FROM Country
			WHERE CountryID = #arguments.countryID#
		</CFQUERY>

		<cfif getCountryLanguages.defaultLanguageID eq "">
			<cfset countryLanguageIDlist = 1>
		<cfelse>
			<cfset countryLanguageIDlist = getCountryLanguages.defaultLanguageID>
		</cfif>

		<CFQUERY NAME="getLanguages" DATASOURCE="#application.siteDataSource#">
			Select ISOcode, languageid, language
			from Language where languageID  in ( <cf_queryparam value="#countryLanguageIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			order by languageID
		</CFQUERY>

		<cfoutput query="getLanguages">
			<cfset request.languageIDLookupStr[ISOCode] = languageid>
			<cfset request.languagelookupfromidstr[languageid] = language>
			<cfset request.languageIDLookupFRomNameStr[language] = languageid>
		</cfoutput>

		<cfreturn countryLanguageIDlist>

	</cffunction>




<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPossibleCountryFromBrowser" hint="Returns a guess at country">
		<cfset var country="">
		<cfset var countryid= 0>
		<cfset var LangAndCountry="">
		<cfset var getPossibleCountry="">
		<cfset var getPossibleCountry2="">


		<cfif isdefined("cgi.HTTP_ACCEPT_LANGUAGE")>

			<cfset LangAndCountry = listfirst(cgi.HTTP_ACCEPT_LANGUAGE)>
			<cfif listLen(LangAndCountry,"-") is 2>
				<cfset country = listGetAt(LangAndCountry,2,"-")>
				<cfif structKeyExists(application.countryIDLookUpFromIsocodeStr,country)>
					<cfset countryid = application.countryIDLookUpFromIsocodeStr[country]>
				</cfif>
			<cfelse>
				<!--- special cases where a single parameter defines both country and language  (the language and country isocodes happen to be the same)
				Note that this does not work for example with denmark where the language code will be DA and the country code in DK
				really need some data on these somewhere!
				--->
				<CFQUERY NAME="getPossibleCountry" DATASOURCE="#application.sitedatasource#">
					select countryid, countrydescription, language, c.isocode
					from country C
						inner join
						language L  on c.isocode = l.isocode and case when charindex(',',c.defaultlanguageid) = 0 then c.defaultlanguageid else substring(c.defaultlanguageid,1,charindex(',',c.defaultlanguageid)-1) end = l.languageid and c.isocode is not null
					and c.isocode =  <cf_queryparam value="#LangAndCountry#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</CFQUERY>

				<cfif getPossibleCountry.recordcount is not 0>
					<cfset countryid = getPossibleCountry.countryid >
				<cfelse>
					<!---
					WAB 2007-04-24
					special case for eg japan, denmark where cgi.http_accept_language is just the language code but the language code is not the same as the countrycode
					this query could bring back more than one record - but hopefully not
					relies on defaultLanguageid being set.  Can handle list of defaultLanguageIDs, but always takes first
					--->
					<CFQUERY NAME="getPossibleCountry2" DATASOURCE="#application.sitedatasource#">
					select countryid, countrydescription, language, c.isocode
					from country C
						inner join
						language L  on case when charindex(',',c.defaultlanguageid) = 0 then c.defaultlanguageid else substring(c.defaultlanguageid,1,charindex(',',c.defaultlanguageid)-1) end  = convert(varchar,l.languageid) and c.isocode is not null
						and l.isocode =  <cf_queryparam value="#LangAndCountry#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</CFQUERY>
					<cfif getPossibleCountry2.recordcount is not 0>
						<cfset countryid = getPossibleCountry2.countryid >
					</cfif>


				</cfif>
			</cfif>

		</cfif>
		<cfreturn countryid>
	</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="ProcessPhraseUpdateForm" hint="Processes the results of a phrase update form " output="false">

	<!---
	cut out of getPhraseV2 custom tag

	vars added WAB 2005-09-12

	WAB 2005-09-21 modified to return a boolean signifying whether any update has actually been done
	 --->
	<!--- assume that the form fields have been created with the format:
		phr_#phrasetextid#_#entityType#_#entityid#_#language#_#countryid#

	 --->

		<cfset var afield = "">
		<cfset var aphrase = "">
		<cfset var positionArray = "">
		<cfset var lenArray = "">
		<cfset var struct = "">
		<cfset var countryid = "">
		<cfset var languageid = "">
		<cfset var entityID = "">
		<cfset var entityTypeID = "">
		<cfset var phraseTextID = "">
		<cfset var phraseIdentifier = "">
		<cfset var entityname = "">
		<cfset var keyToDelete = "">
		<cfset var tempargumentCollection = "">
		<CFSET var AnyUpdatesDone = false>
		<CFSET var ThisItem= "">
		<CFSET var deletePhrase= "">
		<CFSET var ThisItemUpdateDone = false>
		<cfset var checkPhraseTextID = "">
		<cfset var defaultLanguageForThisCountry = "">


		<cfset request.phraseUpdate = structNew()>  <!--- request structure that can be used to prevent being called twice or to find out the country/language ids last updated --->

	<CFIF structKeyExists(form,"fieldnames")>

		<cfloop list="#form.fieldnames#" index="afield">

			<CFSET ThisItemUpdateDone = false>

			<!--- WAB 2013-12-17 change to use regular expression, needed to be able to ignore _required_ as well as _required  (occurs when phrUpd_ field is changed from required to not required by javascript) --->
			<cfif reFindNoCase("\Aphrupd_" ,afield) is NOT 0 and reFindNoCase("_required(_)?\Z" ,afield) is 0>

				<!--- process the field - it's one we're interested in. --->
				<cfset aPhrase = form[afield]>

				<!--- WAB 2012-02-28 CASE:426873 removed 4 separate function calls into a new function --->
				<!--- NJH 2013 Roadmap 2013/03/18 - needed to shorten URL in the message text --->
				<cfset aPhrase = doctorPhraseTextBeforeAddingToDatabase(inputString=aPhrase,shortenURL=structKeyExists(form,replaceNoCase(afield,"phrupd_","")&"shortenURL"))>

				<!--- use a regular expression to extract the stuff --->
					<CFSET struct = reFindNoCase(this.regExp.FindPhrUpd_Country_Language,aField,1,"true")>

					<CFSET positionArray = struct.pos>
					<CFSET LenArray = struct.len>

				<cfif LenArray[1] is 0>
					<!--- if we don't find the first regular expression, then it is possible that the fieldname doesnot have
						the language and countryid bits
						in this case the variables phrCountryID and phrLanguageID should be set
							WAB 2009/03/11 added support for fields of form phrupdcountryid_#phrasetextID#_#entityTypeID#_#entityID#
						--->
						<cfif structKeyExists(form,replacenocase(aField,"phrupd","phrupdcountryID"))>
							<cfset countryid = form[replacenocase(aField,"phrupd","phrupdcountryID")]>
							<cfset languageid = form[replacenocase(aField,"phrupd","phrupdlanguageID")]>
							<cfif languageid is -1>
								<cfset languageid = phrLanguageID>
							</cfif>
						<cfelse>
					<cfset countryid = phrCountryID>
					<cfset languageid = phrLanguageID>
						</cfif>


						<CFSET struct = reFindNoCase(this.regExp.FindPhrUpd_WithoutCountryLanguage,aField,1,"true")>
					<CFSET positionArray = struct.pos>
						<CFSET LenArray = struct.len>

				<cfelse>
					<cfset countryid = mid(aField,positionArray[6],LenArray[6])>
					<cfset languageid = mid(aField,positionArray[5],LenArray[5])>
				</cfif>


					<cfset request.phraseUpdate.lastupdate = structNew()>
					<cfset request.phraseUpdate.lastupdate.countryid = countryid>
					<cfset request.phraseUpdate.lastupdate.languageid = languageid>


				<cfset entityID = mid(aField,positionArray[4],LenArray[4])>
				<cfset entityTypeID = mid(aField,positionArray[3],LenArray[3])>
				<cfset phraseTextID = mid(aField,positionArray[2],LenArray[2])>



				<!---  it is possible that the entityID is not numeric -
					in the case of a record having just been added
					we need to find the recordid
					either it will have been passed to tag as a parameter (eg newRecord = 123)
					or it will be a variable in the calling page
					--->
				<CFIF not isNumeric(entityID)	>
<!--- 					<CFIF isDefined("#attributes.entityID#")>
						<CFSET entityID = evaluate("#attributes.entityID#")>
 --->
					<!--- 2015-03-12	RPW	FIFTEEN-282 - Code was looking at entityID, CF looks in URL scope before FORM scope so was picking up the wrong value --->
			 		<CFIF isDefined("FORM.#entityID#")>
						<CFSET entityID = evaluate("FORM.#entityID#")>
			 		<CFELSEIF isDefined("URL.#entityID#")>
						<CFSET entityID = evaluate("URL.#entityID#")>
			 		<CFELSEIF isDefined("#entityID#")>
						<CFSET entityID = evaluate("#entityID#")>
					<CFELSEIF isDefined("caller.#entityID#")>
						<CFSET entityID = evaluate("caller.#entityID#")>
					<CFELSE>
						<CFOUTPUT>#entityID# is not defined</cfoutput>
						<CFBREAK>
					</CFIF>

				</cfif>

				<!--- only do updates/inserts if country and language are set  --->
				<cfif isNUmeric(countryid) and isNUmeric(languageid)>

					<!--- WAB 2009/02/10 Major Change, If phraseText is blank then item is deleted
					This is required for the content editor so that we can have a headline in lots of languages and just have a single version of the page content (which might be a tag and therefore the same for all languages)
					Was a bit worried about compatibility of this, so for the time being I will limit it to elements, entityTypeID = 10
					--->
					<cfif aPhrase is "" and entitytypeid is "10">
						<CFQUERY NAME="deletePhrase" DATASOURCE="#application.SiteDataSource#">
							delete phrases
							from phraseList pl inner join phrases p on pl.phraseID = p.phraseid
							where
									phraseTextId =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
								and entitytypeid =  <cf_queryparam value="#entitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" >
								and entityid =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
								and languageid =  <cf_queryparam value="#languageid#" CFSQLTYPE="CF_SQL_INTEGER" >
								and countryid =  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" >
						</CFQUERY>
					<cfelse>

						<cfset var updateResult = addNewPhraseAndTranslationV2(
							userID = request.relaycurrentuser.personid,
							LanguageID = languageID,
							countryID = countryID,
							phraseText = aphrase,
							phraseTextID = PhraseTextID,
							entityID=entityID,
							entityTypeID=entityTypeID
						)>
						<CFSET ThisItemUpdateDone = updateResult.details.translationAddedUpdated>

						<!--- update defaultLanguage if necessary --->
						<!--- currently only done if phrase exists --->
						<cfif updateResult.details.translationExists >
							<cfif StructKeyExists(form,"phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#")>

								<cfset defaultLanguageForThisCountry = form["phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#"]>

								<cfif (defaultLanguageForThisCountry is languageid and updateResult.details.defaultForThisCountry is not 1)
									or (defaultLanguageForThisCountry is not languageid and updateResult.details.defaultForThisCountry is  1)>


								<CFQUERY NAME="local.Phrase_Update" DATASOURCE="#application.SiteDataSource#">
								UPDATE
									Phrases
								SET
									<cfif StructKeyExists(form,"phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#")>
										<cfset defaultLanguageForThisCountry = form["phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#"]>
										<cfif defaultLanguageForThisCountry is languageid>
										defaultForThisCountry = 1,
										<cfelse>
										defaultForThisCountry = 0,
										</cfif>
									</cfif>
									lastUpdated = getDate(),
									lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
								where
									phrases.ident =  <cf_queryparam value="#updateResult.details.ident#" CFSQLTYPE="CF_SQL_Integer" >
								</CFQUERY>
								<CFSET ThisItemUpdateDone = true>

								</cfif>
							</cfif>


							<cfif structKeyExists(form,"frmPhrDefaultForThisCountry")>

								<CFQUERY NAME="local.Phrase_Update" DATASOURCE="#application.SiteDataSource#">
								UPDATE
									Phrases
								SET
									defaultForThisCountry = <cfif form.frmPhrDefaultForThisCountry is 1>1<cfelse>0</cfif>,
									lastUpdated = getDate(),
									lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
								where
									phrases.ident =  <cf_queryparam value="#updateResult.details.ident#" CFSQLTYPE="CF_SQL_Integer" >
									AND defaultForThisCountry <>  <cfif form.frmPhrDefaultForThisCountry is 1>1<cfelse>0</cfif>
								</CFQUERY>
								<CFSET ThisItemUpdateDone = true>
							</cfif>
						</cfif>

					</cfif >	<!--- end of aPhrase is "" --->

				</cfif>

			</cfif>

		<cfif ThisItemUpdateDone>
			<cfset AnyUpdatesDone = true>

					<!--- this bit resets the structure holding the translations in memory
						entity names rather than ids are used, so have to convert
					--->

					<cfif EntityTypeID is not 0>
						<cfset entityname = application.entityType[EntityTypeID].tablename>
						<cfset keytodelete = "#phrasetextid#_#entityname#_#entityid#">
					<cfelse>
						<cfset keytodelete = "#phrasetextid#">
					</cfif>
					<cfset resetPhraseStructureKey(keytodelete)>

		</cfif>

		</cfloop>
	</cfif>


	<cfif isDefined("form.phrDelete")>
		<cfloop index="thisItem" list="#form.phrDelete#">
			<cfset phraseIdentifier = listFirst(thisItem,"-")>
			<cfset LanguageID = listGetAt(thisItem,2,"-")>
			<cfset CountryID = listGetAt(thisItem,3,"-")>
			<CFSET struct = reFindNoCase("([a-zA-Z0-9_]+)_([0-9]+)_([a-zA-Z0-9]+)$",phraseIdentifier,1,"true")>
				<CFSET LenArray = struct.len>
				<CFSET PositionArray = struct.pos>
				<cfset entityID = mid(phraseIdentifier,positionArray[4],LenArray[4])>
				<cfset entityTypeID = mid(phraseIdentifier,positionArray[3],LenArray[3])>
				<cfset phraseTextID = mid(phraseIdentifier,positionArray[2],LenArray[2])>


			<cfset deletePhraseTranslation (
				phraseTextID = 	phraseTextID ,
				entityID = entityID ,
					entityType = entityTypeID,
				LanguageID = LanguageID,
				CountryID = CountryID

			)>

		<cfset AnyUpdatesDone = true>
		</cfloop>

	</cfif>

	<cfreturn AnyUpdatesDone >

	</cffunction>


	<!---
	WAB 2012-02-27 during CASE:426873
	Added this function to encapsulate various functions used before an insert/update, since there was a duplicated section of code
	Ideally we would know what editor (such as CK) was being used so that we could do the appropriate doctoring
	Moved the various functions from regExp.cfc, they seemed more at home here
	 --->
	<cffunction name="doctorPhraseTextBeforeAddingToDatabase"  access="public" hint="" returntype="string">
		<cfargument name="inputString" type="string" required="true">
		<cfargument name="shortenURL" type="boolean" default="false">

			<cfset var result = inputstring>

			<!--- added WAB 2007-05-09 to try and prevent jaavscript probs --->
			<cfset result = stripTrailingCarriageReturns(inputString = result)	>
			<!---
				WAB 2013/02/05 CASE 433656
				Remove this escaping of double single quotes.
				This was put in here because Coldfusion did not automatically escape '' in queries (whereas ' is escaped to '')
				However now we have switched to CFQUERYPARAM, everything works OK, so we need to remove this line
			<cfset result = application.com.regExp.escapeDoubleSingleQuotes(inputString = result)	>
			 --->
			<!--- WAB 2011/10/13 for ckeditor need to change {{relay_tag }} back to <relay_tag > --->
			<cfset result = replaceRelayTagCurliesWithChevron(inputString = result)>
			<cfset result = removeUnwantedCKEditorPTags(inputString = result)>

			<!--- 	2013-05-15 WAB CASE 435127 Images in outlook (see function for details).  Perhaps overkill to do on every single phrase --->
			<cfset result = convertStyleHeightAndWidthToAttributes(inputString = result)>

			<!--- NJH 2013 Roadmap 2013/03/18 - needed to shorten URL in the message text --->
			<cfif arguments.shortenURL>
				<cfset result = application.com.social.shortenUrlInText(text=result)>
			</cfif>

		<cfreturn result>
	</cffunction>


		<!--- WAB 2007-05-09 trailing returns serve no purpose (I think!) and cause javascript problems --->
		<cffunction name="stripTrailingCarriageReturns"  access="public" returns = "string" output="false">
				<cfargument name="inputString" required = true>
				<cfset var result = rtrim(inputString)>

				<cfloop condition = "right(result,2) is chr(13)&chr(10)">
					<cfset result = left(result,len(result)-2)>
				</cfloop>
				<cfloop condition = "right(result,1) is chr(10)">
					<cfset result = left(result,len(result)-1)>
				</cfloop>

				<cfreturn result>

			</cffunction>


	<cffunction name="replaceRelayTagCurliesWithChevron"  access="public" hint="" returntype="string">

			<cfargument name="inputString" type="string" required="true">


			<!--- finds:
				1: curlies
				2: optional / then Relay_
				2a: Anything that isn't closing curlies
				3: Closing curlies
				  --->

				<cfset var regexp = "({{)([/]?Relay_[^}]*)(}})">

				<!---
				replace the curlies with chevrons
				 --->

				<cfset var replaceexp = "<\2>">



				<cfreturn REreplaceNoCase(inputString,regexp,replaceexp, "ALL")> >

	</cffunction>



<!--- WAB 2012-02-27
When CKEditor is configured with config.enterMode = CKEDITOR.ENTER_P
it is not possible to put a single relay tag on a page without it getting <P> </P> tags around it
We therefore look for this case and remove the P tags
	2013-05-15 WAB Might no longer be necessary, have reconfigured ckeditor to do 		config.autoParagraph = false; in content editor
 --->
	<cffunction name="removeUnwantedCKEditorPTags"  access="public" hint="" returntype="string">
			<cfargument name="inputString" type="string" required="true">

			<!--- If content is just <P>   <relay_sometag>  </p>	then remove the <P>s  --->

				<cfset var regexp = "\A<p>\s*(<relay_[^>]*>)\s*</p>">
				<cfset var replaceexp = "\1">

				<cfreturn REreplaceNoCase(inputString,regexp,replaceexp, "ALL")> >

	</cffunction>

	<!---
	2013-05-15 WAB CASE 435127
	Outlook can't handle image height and width as styles (which is how ckeditor adds them).
	This function will search content and replace style with attributes.
	looking for <IMG  ..... style="width: 100px; height: 55px;">
	can't handle the px either, so remove that
	 --->
	<cffunction name="convertStyleHeightAndWidthToAttributes"  access="public" hint="" returntype="string" output="false">
			<cfargument name="inputString" type="string" required="true">

				<cfset var result = inputString>
				<cfset var tag = "">
				<cfset var attributesUpdated = false>
				<cfset var newstyleString = "">
				<cfset var stylesStruct = "">
				<cfset var newImageAttributeString = "">

				<cfset var imageTags = application.com.regExp.findHTMLTagsInString(inputString=inputString,htmlTag="img")>

				<cfloop array = "#imageTags#" index="tag">

					<cfif structKeyExists (tag.attributes,"style")>
						<cfset var additionalAttributes = "">
						<cfset attributesUpdated = false>
						<cfset newstyleString =  tag.attributes.style>
						<cfset stylesStruct = application.com.regExp.convertNameValuePairStringToStructure(inputString = tag.attributes.style,delimiter = ";", equalsSign=":")>
							<cfloop list="height,width" index="local.attr">
								<cfif structKeyExists (stylesStruct,local.attr)>
									<cfset additionalAttributes = listAppend(additionalAttributes,'#local.attr#="#trim(replaceNoCase(stylesStruct[local.attr],"px",""))#"'," ")>
									<cfset attributesUpdated = true>
									<cfset newstyleString = application.com.regExp.removeItemFromNameValuePairString(inputString = newstyleString,itemToDelete = local.attr,delimiter = ";",equalsSign=":")>
								</cfif>
							</cfloop>

							<cfif attributesUpdated >
								<cfset newImageAttributeString = replace(tag.attributestring,tag.attributes.style,newstyleString)>
								<cfset newImageAttributeString= additionalAttributes & " " & newImageAttributeString >	 <!--- put attributes at beginning, otherwise go after the self closing / --->
								<cfset newImageAttributeString = replace(newImageAttributeString,'style=""',"")> <!--- remove blank style attribute --->
								<cfset result = replace(result,tag.attributestring,newImageAttributeString)>
							</cfif>

					</cfif>

				</cfloop>
				<cfreturn result >


	</cffunction>



	<cffunction name="replaceRelayTagChevronsWithCurlies"  access="public" hint="" returntype="string">

			<cfargument name="inputString" type="string" required="true">


			<!--- finds:
				1: chevron
				2: optional / then Relay_
				2a: Anything that isn't closing chevron
				3: Closing Chevron
				  --->

				<cfset var regexp = "(<)([/]?Relay_[^>]*)(>)">

				<!---
				replace the chevrons with curlies
				 --->

				<cfset var replaceexp = "{{\2}}">



				<cfreturn REreplaceNoCase(inputString,regexp,replaceexp, "ALL")> >

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            delete a phrase translation


Mods:
WAB   2005-10-18   fixed a bug - wasn't working for entityType = ""


 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deletePhraseTranslation" hint="deletes individual languages of a phrase">
		<cfargument name="phraseTextID" type="string" required="yes">
		<cfargument name="languageid" type="numeric" required = "yes">
		<cfargument name="countryid" type="numeric" required = "yes">
		<cfargument name="entityType" type="string" default = "">
		<cfargument name="entityID" type="numeric" default = 0>

		<cfset var Phrase_Delete = "">
		<cfset var keytodelete = "">
		<cfset var x = "">

						<CFQUERY NAME="Phrase_Delete" DATASOURCE="#application.SiteDataSource#">
						Delete
							phrases
						from
							phraselist,
							phrases
						where
							phrases.phraseid = phraseList.phraseid
							and phraseTextId =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and languageid = #languageid#
							and countryid = #countryid#
							and entitytypeid = <cfif entityType is not ""><cfif isNUmeric(entityType)>#entityType#<cfelse>#application.entitytypeid[entityType]#</cfif><cfelse>0</cfif>
							and entityid = #entityid#
						</CFQUERY>



				<cfif EntityType is not "">
					<cfset keytodelete = "#phrasetextid#_#entityType#_#entityid#">
				<cfelse>
					<cfset keytodelete = "#phrasetextid#">
				</cfif>
				<cfset x = resetPhraseStructureKey(keytodelete)>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deleteEntityTranslation" hint="deletes all phrases of a particular languages from an entity">
		<cfargument name="languageid" type="numeric" required = "yes">
		<cfargument name="countryid" type="numeric" required = "yes">
		<cfargument name="entityType" type="string" required = "yes">
		<cfargument name="entityID" type="numeric" required = "yes">


		<cfset var getPhrasetextIDs = "">
		<cfset var Phrase_Delete = "">
		<cfset var keytodelete = "">
		<cfset var entityTypeID = "">

		<cfif entityType is 0 or entityID is 0 >
			<cfreturn false>
		</cfif>

	<!--- WAB 2009/05/11 added support for numeric --->
		<cfif isNumeric(entityType)>
			<cfset entityTypeID = entityType>
		<cfelse>
			<cfset entityTypeID = application.entitytypeid[entityType]>
		</cfif>

						<CFQUERY NAME="getPhrasetextIDs" DATASOURCE="#application.SiteDataSource#">
						select phraseTextID from
							phraselist
						where
							entitytypeid =  <cf_queryparam value="#entityTypeid#" CFSQLTYPE="CF_SQL_INTEGER" >
							and entityid = #entityid#
						</cfquery>

						<CFQUERY NAME="Phrase_Delete" DATASOURCE="#application.SiteDataSource#">
						Delete
							phrases
						from
							phraselist inner join phrases on phrases.phraseid = phraseList.phraseid
						where
							entitytypeid =  <cf_queryparam value="#entityTypeid#" CFSQLTYPE="CF_SQL_INTEGER" >
							and entityid = #entityid#
							and languageid = #languageid#
							and countryid = #countryid#
						</CFQUERY>



			<cfloop query = "getPhrasetextIDS">
					<cfset keytodelete = "#phrasetextid#_#entityType#_#entityid#">
					<cfset resetPhraseStructureKey(keytodelete)>
			</cfloop>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="resetPhraseStructure" hint="">
		<cfargument name="updateCluster" type="string" required="no" default="true">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->

		<cfset applicationScope.phraseStruct = structNew()>
		<cfset applicationScope.phrasePointerStruct = structNew()>
		<cfset applicationScope.phrasesInJavascript= structNew()>
		<cfset applicationScope.phraseNotExistsStruct = structNew()>

		<cfif updateCluster >
			<cfset applicationScope.com.clusterManagement.updateCluster(updatemethod="resetPhraseStructure",applicationScope=applicationScope)>
		</cfif>

 		<cfreturn true>
	</cffunction>


	<!--- WAB 2009/11/09
		Altered so could take a list of phraseTextIDs
		This allowed function like addPhrasesFromLoadFile to do a single call at the end rather than lots of individual calls (and hence lots of calls to the cluster)
		2016-01-28	WAB	BF-255 Performance. Add clearing of phraseNotExistsStruct
	--->

	<cffunction access="public" name="resetPhraseStructureKey" hint="">
		<cfargument name="phraseTextID" type="string" required="yes">
		<cfargument name="updateCluster" type="string" required="no" default="true">
		<cfargument name="applicationScope" default="#application#">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->
		<cfset var thisphraseTextID = "">

		<cfloop index="thisphraseTextID" list = "#phraseTextID#">

			<cfset structdelete (applicationScope.phraseStruct,phrasetextid,true) >
			<cfset structdelete (applicationScope.phrasePointerStruct,phrasetextid,true)>
			<cfset structdelete (applicationScope.phraseNotExistsStruct,phrasetextid,true)>

		</cfloop>

		<cfif updateCluster >
			<cfset applicationScope.com.clusterManagement.updateCluster(updatemethod="resetPhraseStructureKey",phraseTextID = phraseTextID)>
		</cfif>


		<cfreturn true>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPersonLanguageRights" hint="" returntype = "string">
		<cfargument name="personid" type="numeric" required="yes">


		<cfset var getLanguageRights = "">


		<CFQUERY NAME="getLanguageRights" DATASOURCE="#application.SiteDataSource#">
		SELECT
			l.Languageid
		FROM
			Language as l,
			booleanflagdata as bfd,
			flag as f,
			flaggroup as fg
		WHERE
			fg.flaggrouptextid='LanguageRights'
			and fg.flaggroupid = f.flaggroupid
			and f.name=l.language
			and bfd.flagid= f.flagid
			and bfd.entityid = #arguments.personid#
		</cfquery>

		<cfreturn valuelist(getLanguageRights.languageid)>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getPhraseText" hint="Updates the phraseText for a give element">
		<cfargument name="phraseIdent" type="numeric" required="yes">

		<cfset var getPhraseText = "">


		<cfquery name="getPhraseText" datasource="#application.siteDataSource#">
			select ident,phraseid,phrasetext,languageid from phrases
			where ident=#phraseIdent#
		</cfquery>

		<cfreturn getPhraseText>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="setPhraseText" hint="Updates the phraseText for a give element">
		<cfargument name="phraseIdent" type="numeric" required="yes">
		<cfargument name="newPhraseText" type="string" required="yes">

		<cfset var setPhraseText = "">

		<cfquery name="setPhraseText" datasource="#application.siteDataSource#">
			update phrases
			set phraseText =  <cf_queryparam value="#newPhraseText#" CFSQLTYPE="CF_SQL_VARCHAR" >
			where ident=#phraseIdent#
		</cfquery>

		<cfreturn getPhraseText>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getPhrasesByString" hint="Returns phrases as a query where they contain a supplied string">
		<cfargument name="stringToFind" type="string" required="yes">
		<cfargument name="useExactMatch" type="boolean" default="false">

		<cfset var getPhrasesByString = "">

		<cfquery name="getPhrasesByString" datasource="#application.siteDataSource#">
			  select ident,phraseid,phrasetext,languageid from phrases
			  <cfif arguments.useExactMatch>
					where phrasetext  like  <cf_queryparam value="#stringToFind#" CFSQLTYPE="CF_SQL_VARCHAR" >
			  <cfelse>
					where phrasetext  like  <cf_queryparam value="%#stringToFind#%" CFSQLTYPE="CF_SQL_VARCHAR" >
			  </cfif>
		</cfquery>

		<cfreturn getPhrasesByString>

    </cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Mod: 2005-07-18   WAB:  when checking countryrights, add in the regions a person has complete control to
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getAllTranslatedLanguagesForAPhrase" hint="Returns a query with all the translations for a given phrase" returnType="query" output="false">
		<cfargument name="phraseTextID" type="string" default ="">
		<cfargument name="entityType" type="string" default = 0>
		<cfargument name="entityID" type="numeric" default = 0>
		<cfargument name="phraseID" type="numeric" default ="0">
		<cfargument name="defaultphraseTextIDList" type="string" default ="">
		<cfargument name="returnPhraseText" type="boolean" default ="false">  <!--- sometimes just want the list of country/language combinations --->

		<cfset var getTranslatedLanguages = "">
		<cfset var result = "">
		<cfset var entityTypeText = arguments.entityType>
		<cfset var hasEditRights = true>
		<cfset var phraseTextIDList = defaultphraseTextIDList>
		<cfset var counter=0>
		<cfset var thisphraseTextID="">

		<cfif phraseTextID is "" and phraseID is 0 and entityType is 0 and entityID is 0 >
			<cfoutput>Requires a PhraseTextID or a PhraseID <CF_ABORT></cfoutput>
		<cfelseif phraseID is 0  and phraseTextID is "" and entityType is not 0 and entityID is not 0 and defaultphraseTextIDList is "">
			<!--- get all phraseTextIds for this entity --->
			<cfset phraseTextIDList = getAllEntityPhraseTextIDs(entityType = entityType, entityID = entityID)>
		</cfif>

		<cfif phraseTextID is not "" and listFindNocase (phraseTextIDList,phraseTextID) is 0>
			<cfset phraseTextIDList = listAppend(phraseTextIDList,phraseTextID)>
		</cfif>

		<!--- NJH 2009/05/08 P-FNL Content Editor
			if the entity is an element, then check whether we have rights to edit it.
		--->
		<cfif isNumeric(arguments.entityType)>
			<cfset entityTypeText = application.entityType[arguments.entityType].tableName>
		</cfif>

		<cfif entityTypeText eq "element">
			<cfset hasEditRights = application.com.rights.Element(elementid=arguments.entityID,personid=request.relayCurrentUser.personID,permission=2).hasrights>
		<cfelse>
			<!--- application.com.rights.getRecordRights(entity=entityTypeText,entityID=arguments.entityID,personID=request.relayCurrentUser.personID,level=2)> --->
		</cfif>

		<!--- NJH 2009/05/08 P-FNL Content Editor added phraseID and phraseText to query --->
		<CFQUERY NAME="getTranslatedLanguages" DATASOURCE="#application.SiteDataSource#">
		select
				convert(varchar,p.LanguageID)+ '_' + convert(varchar,p.countryID) + '_' + convert(varchar,p.defaultforthiscountry) as uniqueGroup,
				isNull(language,'No Language') as language,
				isNull(countrydescription,'Any Country') as countrydescription,
				case when countrydescription is Null then 0 else 1 end as countryOrder,
				p.countryid, p.languageid, p.defaultforthiscountry,
				entityTypeID,
				entityID,
				<cfif hasEditRights>
<cfif request.relaycurrentuser.LanguageEditRights is not "">case when p.countryid in (0,#request.relayCurrentUser.countryList#<cfif request.relayCurrentUser.regionList is not "">,#request.relayCurrentUser.regionList#</cfif>) and  p.languageid in (0,#request.relaycurrentuser.LanguageEditRights#) then 1 else 0 end <cfelse>0</cfif> as userHasEditRights
				<cfelse>
					0 as userHasEditRights
				</cfif>
				,rtrim(phraseTextID) as phraseTextID     <!--- WAB 2010/02/10 occasional bug when phraseTextID for some reason gets a space on the end --->
				,phraseTextID as phraseTextIDs    -- this one I use for aggregating
				,p.phraseID
<cfif returnPhraseText>
				,phraseText
				</cfif>
		from
			phraseList as pl inner join phrases as p on pl.phraseid = p.phraseid
			left join language as l on p.languageid = l.languageid
			left join country as c on p.countryid = c.countryid
		where
			<cfif phraseTextIDList is not "">
				pl.phrasetextid  in ( <cf_queryparam value="#phraseTextIDList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				<cfif entityType is "" or entityType is 0>
				and pl.entityTypeID = 0
				and pl.entityID = 0
				<cfelse>
				and pl.entityTypeID = <cfif isNumeric(entityType)>#entityType#<cfelse>#application.entityTypeID[entityType]#</cfif> <!--- hack --->
				and pl.entityID = #entityID#
				</cfif>
			<cfelse>
				pl.phraseid = #phraseID#
			</cfif>

--
<!--- WAB 2009/09/22 changed order to be by country (having any country first)--->
order by countryOrder, countrydescription, defaultforthiscountry desc, language <cfif phraseTextID is not "">,case <cfset counter = 0><cfloop index="thisPhraseTextID" list="#phraseTextIDList#">when phrasetextid =  <cf_queryparam value="#thisPhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >  then #counter# <cfset counter = counter + 1></cfloop>else #counter# end </cfif>


		</CFQUERY>



		<!--- If we are not returning the phraseTest then we return an aggregated query
		 	giving details of which phrasesTextIDs have been translated into which language/country combination
			Was originally only used for entity Translations, but now 2012/11 used for normal translations as well
		--->
		<cfif not returnPhraseText and phraseTextIDList is not "">
			<!--- this line aggregates all the phraseTextIDs for a given entity into a single field, comma separated --->
			<cfset result = application.com.structureFunctions.QueryAggregateList(query = getTranslatedLanguages,groupby="uniqueGroup",field="phraseTextIDs")>

			<!---
			May only want to show list phraseTextIDs where is is different from standard,
			we will create another column which just shows these different ones
			--->

			<cfset queryAddColumn (result,"PhraseTextIDs2",arrayNew(1))>
			<cfloop query = "result">
				<cfset querySetCell (result,"PhraseTextIDs2",replaceNoCase(PhraseTextIDs,phraseTextIDList,""),currentRow)>
			</cfloop>

		<cfelse>

			<cfset result = getTranslatedLanguages>

		</cfif>

		<cfreturn  result>

	</cffunction>


	<cffunction name="generateTranslatedLanguagesTable">
		<cfargument name="translatedLanguagesQuery" type="query" required="true">
		<cfargument name="currentLanguageID" type="numeric" required = true>
		<cfargument name="currentCountryID" type="numeric" required = true>
		<cfargument name="excludeCurrent" type="boolean" default= true>
		<cfargument name="showDelete" type="boolean" default= true>
		<cfargument name="showEdit" type="boolean" default= true>
		<cfargument name="showLabel" type="boolean" default= true>
		<cfargument name="formSaveFunction" type="string" default= ""> <!--- function to run before editing/viewing --->

		<cfset var result_html ="">
		<cfset var firstpass = true>

			<cfsavecontent variable="result_html">
				<cfoutput>
					<div class="row" id="translationsCountry">
						<CFLOOP query="translatedLanguagesQuery">
							<cfif not excludeCurrent or not (languageid is currentLanguageID and countryid is currentCountryID)>
								<cfif firstpass and arguments.showLabel>
									<div class="col-xs-12">
										<h4>Also translated into:</h4>
									</div>
									<cfset firstpass = false></cfif>
									<div class="col-xs-12 col-sm-6">
										<div class="white-box-top">
											<div id="relayTranslations">
												<div>
													<h5><cfif defaultForThisCountry is 1>*</cfif> #language# <small>#countrydescription#</small></h5>
													<p>#phraseTextIDs2#</p>
													<cfif showEdit>
														<div id="relayTranslationsLinks"><A href="javascript:#arguments.formSaveFunction#changeLanguage(#languageid#,#countryid#,'#entityTypeid#', '#entityID#', #userHasEditRights#) "><CFIF userHasEditRights is 1>Edit<cfelse>view</cfif></A>
															<cfif showDelete>
															 | <CFIF userHasEditRights is 1 and not (countryid is 0 and defaultForThisCountry is 1)><A href="javascript:deleteLanguage(#languageid#,#countryid#,'#entityTypeid#', '#entityID#', #userHasEditRights#) ">Delete</A></cfif>
															</cfif>
														</div>
													</cfif>
												</div>
											</div>
										</div>
									</div>
							</cfif>
						</cfLOOP>
					</div>
					<cfif firstPass is false>  <!--- shows that atleast one has been updated --->
						<p>* indicates the default language for the given country</p>
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfset result_html = rereplaceNoCase(result_html ,"[\n\t\r]","","ALL")>

		<cfreturn result_html>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getAllNotTranslatedLanguagesForAPhrase" hint="Returns a query with all the languages not translated for a given phrase, suitable for a two selects related" returnType="query">
		<cfargument name="phraseTextID" type="string" default="">
		<cfargument name="entityType" type="string" default = "">
		<cfargument name="entityID" type="numeric" default = 0>
		<cfargument name="phraseID" type="numeric" default ="0">

		<cfargument name="filter" type="string" default = "">

		<cfset var getNonTranslatedLanguages = "">

<!---
		WAB 2009/02/11 removed this limitation - needed to get a  query for a phrase which did not yet exist, so pass in phraseid = 0
		<cfif phraseTextID is "" and phraseID is 0>
			Requires a PhraseTextID or a PhraseID <CF_ABORT>
		</cfif>
 --->

		<CFQUERY NAME="getNonTranslatedLanguages" DATASOURCE="#application.SiteDataSource#">
		select distinct
				language as language,
				countrydescription,
				c.countryid, l.languageid, countryorder
<!--- --				,				case when p.countryid = #frmCountryid# and p.languageid = #frmLanguage# then 1 else 0 end as currentRecord --->
		from
			(select language, languageid from language union select 'No Language',0) as l inner join
		<!--- WAB 2009/09/17 as a test added regions to list and altered ordering accordingly, code can stay in whether or not regions are used --->
			(select countrydescription, countryid, case when isocode is null then 3 else 4 end as countryorder from country union select 'Any Country',0,1 union select 'Choose Country',null,2) as c on 1 = 1
			left join (select distinct p.countryid, p.languageid from phraseList as pl inner join phrases as p on pl.phraseid = p.phraseid
						<cfif phraseTextID is not "">
							and pl.phrasetextid  in ( <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
							and pl.entityTypeID = <cfif entityType is not "">#application.entityTypeID[entityType]#<cfelse>0 </cfif> <!--- hack --->
							and pl.entityID = #entityID#
						<cfelse>
							and pl.phraseid = #phraseID#
						</cfif>
			) as x on x.languageid = l.languageid and x.countryid = c.countryid
		where
		(x.languageid is null or c.countryid is null)
		and countryDescription <> 'All Countries'
		<cfif filter is not "">and #filter#</cfif>

		order by language, countryorder, countrydescription

		</CFQUERY>

		<cfreturn  getNonTranslatedLanguages>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getAllNotTranslatedLanguagesForAPhraseFilteredForCurrentUser" hint="Returns a query with all the languages not translated for a given phrase, suitable for a two selects related for current user" returnType="query">
		<cfargument name="phraseTextID" type="string" default="">
		<cfargument name="entityType" type="string" default = 0>
		<cfargument name="entityID" type="numeric" default = 0>
		<cfargument name="phraseID" type="string" default="0">

		<cfset var filter = "">
		<cfset var qryCountryGroupType="">
		<cfset var useCountryGroups = application.com.settings.getSetting("translation.UseCountryGroups")> <!--- NJH 2010/08/09 RW8.3 - use new method --->

<!---
		WAB 2009/02/11 removed this limitation - needed to get a  query for a phrase which did not yet exist, so pass in phraseid = 0
		<cfif phraseTextID is "" and phraseID is 0>
			Requires a PhraseTextID or a PhraseID <CF_ABORT>
		</cfif>
 --->
<!---
	WAB 2006-02-20
	for some reason this filter failed on fnlsql1 sql server, it didn't like the long list of countryids, so I have replaced with a subquery on the usergroupcountrytable
	<cfset filter = "((c.countryid in (0,#request.relayCurrentUser.countryList#)  or c.countryid is null)">
--->

		<cfset filter = "((c.countryid in (select countryid from usergroupcountry where usergroupid = #request.relaycurrentuser.usergroupid#)  or c.countryid = 0 or c.countryid is null)">

		<cfif request.relaycurrentuser.LanguageEditRights is not ""><cfset filter = filter & " and l.languageid in (0,#request.relaycurrentuser.LanguageEditRights#) )"><cfelse><cfset filter = filter & " and l.languageid = -1 )"> </cfif>

		<!--- PPB 2010/08/11 add translation country groups to select list --->
		<cfif useCountryGroups and request.relayCurrentUser.regionList is not "">
			<cfquery name="qryCountryGroupType" datasource = "#application.siteDataSource#">
				SELECT CountryGroupTypeId FROM CountryGroupType WHERE CountryGroupTypeTextId='Translation'
			</cfquery>

			<cfset filter = filter & "OR (c.countryid in (#request.relaycurrentuser.RegionList#) AND c.countryid in (select CountryId from Country c where c.isocode is null and c.CountryGroupTypeId=#qryCountryGroupType.CountryGroupTypeId#) )">
		</cfif>

		<cfif phraseTextID is not "">
			<cfreturn  getAllNotTranslatedLanguagesForAPhrase(phrasetextID = phraseTextID,entityType=entityType,entityID=entityID,filter = filter)>
		<cfelse> <!--- WAB 2009/02/11  removed if phraseID is not 0 --->
			<cfreturn  getAllNotTranslatedLanguagesForAPhrase(phraseID = phraseID,filter = filter)>
		</cfif>

	</cffunction>


	<!--- Brings back separate queries of all a user's langauges and countries
			is combined with details of all language/country combinations currently translated
			to produce "new language" drop downs  (But without working out every single un-used combination)
	--->
	<cffunction name="getAllCountriesAndLanguagesForUser" hint="Queries of all possible languages and all possible countries for translations" returnType="struct" output="false">

		<cfset var result = structNew()>
		<CFSET var languages = "">
		<CFSET var countries = "">
		<cfset var useCountryGroups = application.com.settings.getSetting("translation.UseCountryGroups")> <!--- NJH 2010/08/09 RW8.3 - use new method --->

					<cfquery name="Languages" datasource = "#application.siteDataSource#">
					select language as display, languageid as value, 2 as orderIndex
					 from language
					where languageid  in ( <cf_queryparam value="#request.relaycurrentuser.LanguageEditRights#" CFSQLTYPE="cf_sql_integer"  list="true"> )
					union
					select 'No Language',0 , 1 as orderIndex
					union
					select 'Choose Language',null,0

					order by orderindex, display
					</cfquery>

				<cfquery name="Countries" datasource = "#application.siteDataSource#">
					select
						countrydescription as display,
						countryid as value,
						case when isocode is null then 3 else 4 end as countryorder ,    -- WAB 2009/04/29 changed order - put regions at top
						case when isocode is null then  'Regions' else  'Countries'  end as optgroup
					from country     -- WAB 2009/04/29 changed order - put regions at top
					where countryid in (0,#request.relayCurrentUser.countryList#)
					<cfif useCountryGroups and request.relayCurrentUser.regionList is not "">
						OR (
							countryid in (#request.relaycurrentuser.RegionList#)
							AND	countryid in (select CountryId from Country c inner join CountryGroupType cgt on c.CountryGroupTypeId = cgt.CountryGroupTypeId where c.isocode is null and cgt.CountryGroupTypeTextId='Translation')
<!---
	PPB 2010/08/10
	<cfif application.settings.Translation.CountryGroupList is not ""> AND countryid in (#application.settings.Translation.CountryGroupList#) </cfif>
 --->
							)
					</cfif>

				union

					select 'Any Country',0,2 ,''

				union

					select 'Choose Country',null,1,''

				order by countryorder,countrydescription
				</cfquery>
		<cfset result.languages = languages>
		<cfset result.countries = countries>

		<CFRETURN result>

	</cffunction>

	<!---
		THIS CODE FROM ELEMENTS EDIT
		It is used to decide which language/country to display when editing
	--->
	<cffunction name="getDefaultLanguageAndCountryForEditingAPhrase" hint="Returns a structure containing Language and CountryID to display when first editing a phrase" returnType="struct">

		<cfargument name="phraseTextID" type="string" required="yes"><!--- can be more than one --->
		<cfargument name="entityType" type="string" default="">
		<cfargument name="entityID" type="string" default="0">
		<cfargument name="defaultCountryID" type="numeric" default="0">
		<cfargument name="defaultLanguageID" type="numeric" default="1">

		<cfset var result = structNew()>
		<cfset var getDefaults= "">

		<cfif isNumeric(entityID)>

					<CFQUERY NAME="getDefaults" DATASOURCE="#application.SiteDataSource#">
						select distinct
								p.countryid, p.languageid, p.defaultForThisCountry	,
								<cfif request.relaycurrentuser.LanguageEditRights is not "">case when p.Languageid  in ( <cf_queryparam value="#request.relaycurrentuser.LanguageEditRights#" CFSQLTYPE="cf_sql_integer"  list="true"> ) then 1 else 0 end as order1,</cfif>
								case when p.countryid = 0 then 1 else 0 end as order2,
								case when p.countryid  in ( <cf_queryparam value="#request.relaycurrentuser.countrylist#" CFSQLTYPE="cf_sql_integer"  list="true"> ) then 1 else 0 end as order3

						from
							phraseList as pl inner join phrases as p on pl.phraseid = p.phraseid
						where pl.phrasetextid  in ( <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
						and pl.entityTypeID = <cfif entityType is not "">#application.entityTypeid[entityType]#<cfelse>0</cfif>
						and pl.entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
						order by
						<cfif request.relaycurrentuser.LanguageEditRights is not "">order1 desc,</cfif>
						order2 desc ,
						order3 desc,
						p.defaultForThisCountry	desc
						</cfquery>

						<cfif getDefaults.recordCount is not 0>
							<cfset result.countryID = getDefaults.countryiD>
							<cfset result.LanguageID = getDefaults.LanguageID>
							<cfset result.phraseExists = true>
						<cfelse>
							<cfset result.countryID = defaultCountryID>
							<cfset result.LanguageID = defaultLanguageID>
							<cfset result.phraseExists = false>
						</cfif>


				<cfelse>

							<cfset result.countryID = defaultCountryID>
							<cfset result.LanguageID = defaultLanguageID>
							<cfset result.phraseExists = false>

				</cfif>


			<cfreturn result>

	</cffunction>

	<cffunction name="getLanguageAndCountryName">
		<cfargument name="countryiD" type="string" default = "0">
		<cfargument name="languageID" type="string" default="0">
		<cfargument name="defaultForThisCountry" type="boolean" default="false">

		<cfset var result = structNew()>

			<CFIF LanguageID IS 0>
				<cfset result.Language = "No Language">
			<CFELSEif LanguageID gt 0>
				<cfset result.Language = application.languagelookupfromidstr[LanguageID]>
			<cfelse>
				<cfset result.Language = "">
			</CFIF>

			<cfif defaultForThisCountry>
				<cfset result.Language = "* " & result.Language>
			</cfif>


			<cfif countryID is not 0 and countryID is not "">
				<cfset result.country = application.countryName[countryID]>
			<cfelse>
				<cfset result.country = "Any Country">
			</cfif>

			<cfset result.shortString = "#result.Language#, #result.Country#">
			<cfset result.fullString = "Language: #result.Language#. Country: #result.Country#">

		<cfreturn result>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	WAB end 2005
	WAB 2009/02/12 modified so that would handle non-entity phrases (entityType = "").
					This allowed the editPhraseWidget (which uses this) to work with non entity phrases
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getEntityPhrases" hint="Returns a structure containing translations for one or more entities" returnType="struct">
		<cfargument name="phraseList" type="string" required="yes">
		<cfargument name="entityType" type="string" required="yes">
		<cfargument name="entityID" type="string" required="yes">
		<cfargument name="evaluateVariables" type="boolean" default=true>		<!--- WAB This parameter added  2007-09-07.  Needed so that can be set to false when bringing back the content for editing--->
		<cfargument name="executeRelayTags" type="boolean" default=true>  		<!--- note that evaluate variables has to be true as well --->
		<cfargument name="TranslatePhrasesWithinPhrases" type="boolean" default=#arguments.evaluateVariables#>
		<cfargument name="nullText" default="">
		<cfargument name="onNullShowPhraseTextID" default="no">
		<cfargument name="exactMatch" default="no">
		<cfargument name="languageID" type="string" default = "">
		<cfargument name="countryID" type="string" default = "">
		<cfargument name="debug" type="boolean" default = "false">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">

		<cfreturn getentityPhrasesV2(argumentCollection = arguments)>
	</cffunction>

<!---
 	<cffunction name="getEntityPhrasesV1" hint="Returns a structure containing translations for one or more entities" returnType="struct">
		<cfargument name="phraseList" type="string" required="yes">
		<cfargument name="entityType" type="string" required="yes">
		<cfargument name="entityID" type="string" required="yes">
		<cfargument name="evaluateVariables" type="boolean" default=true>		<!--- WAB This parameter added  2007-09-07.  Needed so that can be set to false when bringing back the content for editing--->
		<cfargument name="nullText" default="">
		<cfargument name="onNullShowPhraseTextID" default="no">



		<cfset var thephrase = "">
		<cfset var thelanguage = "">
		<cfset var fullPhraseList = "">
		<cfset var theEntityID = "">
		<cfset var languageSuffix = "">
		<cfset var result = structNew()>

		<cfif isNumeric(listFirst(entityID))>

			<cfif entityType is not "">
			<cfloop index="thePhrase" list="#phraseList#">
				<cfloop index = "theEntityID" list = "#entityID#">
					<cfset fullphraselist = listappend(fullphraselist,"#thePhrase#_#entityType#_#theEntityID#")>
				</cfloop>
			</cfloop>
			<cfelse>
				<cfset fullphraselist = phraseList>
			</cfif>


			<cfif isDefined("arguments.language") and isDefined("arguments.countryID")>
				<!--- special case when languageid is ""  - we need to return blank record with the correct update identifiers--->
				<cfset theLanguage = arguments.language>
				<cfif  theLanguage  is "">
					<cfset  theLanguage  = -1>
				</cfif>
				<cfif not isDefined("arguments.exactMatch")>
					<cfset arguments.exactMatch = true>
				</cfif>
				<cf_translate phrases = #fullPhraseList# countryid = #arguments.countryID# language =#theLanguage# exactmatch = #arguments.exactMatch# forcereload=yes evaluateVariables=#evaluateVariables# onNullShowPhraseTextID= false nulltext="" />
			<cfelse>
				<cf_translate phrases = #fullPhraseList# evaluateVariables=#evaluateVariables# onNullShowPhraseTextID=#onNullShowPhraseTextID# nulltext=#nulltext#  />

			</cfif>

			<cfif entityType is not "">
			<cfloop index="thePhrase" list="#phraseList#">
				<cfloop index = "theEntityID" list = "#entityID#">
					<cfset result[theEntityID][thePhrase] = evaluate("phr_#thePhrase#_#entityType#_#entityID#")>
					<cfif isDefined("arguments.exactMatch") and arguments.exactMatch><cfset result[theEntityID]["updateIdentifier"][thePhrase] = listfirst(evaluate("fld_#thePhrase#_#entityType#_#entityID#"),"-")></cfif>			 <!--- list first cuts the identifier off after the entityid if the language id was -1 --->
				</cfloop>
			</cfloop>
			<cfelse>
				<cfloop index="thePhrase" list="#phraseList#">
					<cfset result[0][thePhrase] = evaluate("phr_#thePhrase#")>
					<cfif isDefined("arguments.exactMatch") and arguments.exactMatch><cfset result[0]["updateIdentifier"][thePhrase] = listfirst(evaluate("fld_#thePhrase#"),"-")></cfif>			 <!--- list first cuts the identifier off after the entityid if the language id was -1 --->
				</cfloop>
			</cfif>

		<cfelse>
			<!--- cf_translate can't handle a new record, so we will do it manually
				actually assume here that in this case a language is

			--->
			<cfif not isDefined("arguments.language") or arguments.language is "" or arguments.language is "-1">
				<cfset languageSuffix = "">
			<cfelse>
				<cfset languageSuffix = "#arguments.language#_#arguments.countryid#">
			</cfif>

			<cfif entityType is not "">
			<cfloop index="thePhrase" list="#phraseList#">
				<cfloop index = "theEntityID" list = "#entityID#">
					<cfset result[theEntityID][thePhrase] = "">
					<cfset result[theEntityID]["updateIdentifier"][thePhrase] = "phrupd_#thephrase#_#application.entityTypeid[entityType]#_#theentityID#_#languageSuffix#">
				</cfloop>
			</cfloop>
			<cfelse>
				<cfloop index="thePhrase" list="#phraseList#">
						<cfset result[0][thePhrase] = "">
						<cfset result[0]["updateIdentifier"][thePhrase] = "phrupd_#thephrase#_0_0_#languageSuffix#">
				</cfloop>

			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>
--->

	<cffunction name="getEntityPhrasesV2" hint="Returns a structure containing translations for one or more entities" returnType="struct">
		<cfargument name="phraseList" type="string" required="yes">
		<cfargument name="entityType" type="string" required="yes">
		<cfargument name="entityID" type="string" required="yes">
		<cfargument name="evaluateVariables" type="boolean" default=true>		<!--- WAB This parameter added  2007-09-07.  Needed so that can be set to false when bringing back the content for editing--->
		<cfargument name="executeRelayTags" type="boolean" default=true>  		<!--- note that evaluate variables has to be true as well --->
		<cfargument name="TranslatePhrasesWithinPhrases" type="boolean" default=arguments.evaluateVariables>
		<cfargument name="nullText" default="">
		<cfargument name="onNullShowPhraseTextID" default="yes">
		<cfargument name="exactMatch" default="no">
		<cfargument name="languageID" type="string" default = "">
		<cfargument name="countryID" type="string" default = "">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">
		<cfargument name="debug" default="no">


		<cfset var thephrase = "">
		<cfset var fullPhraseList = "">
		<cfset var theEntityID = "">
		<cfset var languageSuffix = "">
		<cfset var translatedPhrases = "">
		<cfset var result = structNew()>

			<cfif debug>
			<cfdump var="#arguments#">
			</cfif>

		<cfif isNumeric(entityType)>
			<cfset arguments.entityType = application.entityType[entityType].tablename>
		</cfif>

		<cfif isNumeric(listFirst(entityID))>

			<cfif entityType is not "">
				<cfloop index="thePhrase" list="#phraseList#">
					<cfloop index = "theEntityID" list = "#entityID#">
						<cfset fullphraselist = listappend(fullphraselist,"#thePhrase#_#entityType#_#theEntityID#")>
					</cfloop>
				</cfloop>
			<cfelse>
				<cfset fullphraselist = phraseList>
			</cfif>

			<!--- WAB 2010/09/30 removed some code here, was not dealing properly with languagueid being blank
				there was some old code here setting some variables which were then not referenced - bad code on my part
			 --->

				<!--- special case when languageid is ""  - we need to return blank record with the correct update identifiers--->
				<!--- WAB 2011/02/14
					LID 5679
					This bit about arguments.languageid is "" is causing problems - portal pages appearing in the wrong language
					It is my code but can't explain it exactly
					I think that it should only apply when we are getting an exact match
					So I have added and exactMatch is True

					 --->
				<cfif  arguments.languageid is "" and exactMatch>
					<cfset  arguments.languageid  = -1>
				</cfif>
				<!--- <cf_translate phrases = #fullPhraseList# countryid = #arguments.countryID# language =#theLanguage# exactmatch = #arguments.exactMatch# forcereload=yes evaluateVariables=#evaluateVariables# onNullShowPhraseTextID= false nulltext="" />	 --->
				<cfset translatedPhrases = translateListOfPhrases (listOfphrases = fullPhraseList,argumentCollection = arguments )>

			<cfif debug>
			<cfdump var="#translatedPhrases#">
			</cfif>
			<cfif entityType is not "">
				<cfloop index="thePhrase" list="#phraseList#">
					<cfloop index = "theEntityID" list = "#entityID#">
						<cfset result[theEntityID][thePhrase] = translatedPhrases.phrases["#thePhrase#_#entityType#_#entityID#"].phraseText>
						<cfif isDefined("arguments.exactMatch") and arguments.exactMatch>

							<cfset result[theEntityID]["updateIdentifier"][thePhrase] = listfirst(translatedPhrases.phrases["#thePhrase#_#entityType#_#entityID#"].updateIdentifier,"-")>			 <!--- list first cuts the identifier off after the entityid if the language id was -1 --->
							<cfset result[theEntityID]["phraseIdentifier"][thePhrase] = "#thePhrase#_#application.entityTypeid[entityType]#_#entityID#">
							<cfset result[theEntityID]["defaultForThisCountry"][thePhrase] = translatedPhrases.phrases["#thePhrase#_#entityType#_#entityID#"].defaultForThisCountry>
						</cfif>
					</cfloop>
				</cfloop>
			<cfelse>
				<cfloop index="thePhrase" list="#phraseList#">
					<cfset result[0][thePhrase] = translatedPhrases.phrases[thePhrase].phraseText>
					<cfif isDefined("arguments.exactMatch") and arguments.exactMatch>
							<cfset result[0]["updateIdentifier"][thePhrase] = listfirst(translatedPhrases.phrases["#thePhrase#"].updateIdentifier,"-")>	 <!--- list first cuts the identifier off after the entityid if the language id was -1 --->
							<cfset result[0]["phraseIdentifier"][thePhrase] = "#thePhrase#_0_0">
							<cfset result[0]["defaultForThisCountry"][thePhrase] = 0>
					</cfif>
				</cfloop>
			</cfif>
		<cfelse>
			<!--- cf_translate can't handle a new record, so we will do it manually
				actually assume here that in this case a language is
			--->
			<cfif not isDefined("arguments.language") or arguments.language is "" or arguments.language is "-1">
				<cfset languageSuffix = "">
			<cfelse>
				<cfset languageSuffix = "#arguments.language#_#arguments.countryid#">
			</cfif>

			<cfif entityType is not "">
				<cfloop index="thePhrase" list="#phraseList#">
					<cfloop index = "theEntityID" list = "#entityID#">
						<cfset result[theEntityID][thePhrase] = "">
						<cfset result[theEntityID]["updateIdentifier"][thePhrase] = "phrupd_#thephrase#_#application.entityTypeid[entityType]#_#theentityID#_#languageSuffix#">
						<cfset result[theEntityID]["phraseIdentifier"][thePhrase] = "#thePhrase#_#application.entityTypeid[entityType]#_#theentityID#">
						<cfset result[theEntityID]["defaultForThisCountry"][thePhrase] = 0>
					</cfloop>
				</cfloop>
			<cfelse>
				<cfloop index="thePhrase" list="#phraseList#">
						<cfset result[0][thePhrase] = "">
						<cfset result[0]["updateIdentifier"][thePhrase] = "phrupd_#thephrase#_0_0_#languageSuffix#">
						<cfset result[0]["phraseIdentifier"][thePhrase] = "#thePhrase#_0_0">
				</cfloop>

			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>

<!---
	WAB 2006-03-08
	really a stub to call getEntityPhrases
	but with the additional feature that you don't have to know what phrases are associated with an entity, it will look for you
 --->
	<cffunction name="getAllEntityPhraseTextIDs" hint="returns list of all phraseTextIDs associated with an entity" returnType="string">
		<cfargument name="entityType" type="string" required="yes">
		<cfargument name="entityID" type="string" required="yes">
		<cfargument name="defaultPhraseList" type="string" default = "">


		<cfset var result = "">
		<cfset var getPhrasetextIDs = "">

		<!--- what phraseTextIDs are associated with these entities --->
		<CFQUERY NAME="getPhrasetextIDs" DATASOURCE="#application.SiteDataSource#">
			select distinct phrasetextID from
			phraselist
			where entityid =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and entityTypeID =
				<cfif isNUmeric(entityType)>
					#entityType#
				<cfelse>
					#application.entityTypeID[entityType]#
				</cfif>

			<cfif defaultPhraseList is not "">and phraseTextID  not in ( <cf_queryparam value="#defaultPhraseList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )</cfif>

		</CFQUERY>

		<cfset result = listappend(defaultPhraseList,valuelist(getPhrasetextIDs.phrasetextID))>

		<cfreturn result>

	</cffunction>


	<cffunction name="getAllEntityPhrases" hint="Returns a structure containing translations of all the phrases associated with one or more entities" returnType="struct">
		<cfargument name="defaultPhraseList" type="string" default="">
		<cfargument name="excludePhraseList" type="string" default=""> <!--- CASE 426986 NJH 2012/03/07 --->
		<cfargument name="entityType" type="string" required="yes">
		<cfargument name="entityID" type="string" required="yes">
		<cfargument name="evaluateVariables" type="boolean" default=true>
		<cfargument name="executeRelayTags" type="boolean" default=true>  		<!--- note that evaluate variables has to be true as well --->
		<cfargument name="nullText" default="">
		<cfargument name="onNullShowPhraseTextID" default="no">
		<cfargument name="exactMatch" type="boolean" default = "false">
		<cfargument name="languageID" type="string" default = "">
		<cfargument name="countryID" type="string" default = "">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">

		<cfset var phraseTextIDS = getAllEntityPhraseTextIDs(entityType = entityType, entityID = entityID,defaultPhraseList =defaultPhraseList)>

		<!--- CASE 426986 NJH 2012/03/07 - add ability to exclude phrases from being translated. --->
		<cfif excludePhraseList is not "">
			<cfset phraseTextIDS = application.com.globalFunctions.ListMinusList(phrasetextIDs,excludePhraseList)>
		</cfif>

		<!--- CASE 426986 NJH 2012/03/07 - add ability to exclude phrases from being translated. --->
		<cfif excludePhraseList is not "">
			<cfset phraseTextIDS = application.com.globalFunctions.ListMinusList(phrasetextIDs,excludePhraseList)>
		</cfif>
		<!--- WAB 2014-11-25 CASE 442080 (reinstate Ts and logging) Change call to use argumentCollection --->
		<cfreturn getEntityPhrases (phraseList = phraseTextIDS, argumentCollection = arguments)>

	</cffunction>

	<!--- WAB 2005-09-07
		useful wrapper function for returning the translation of a single phrase
		2011/06/09 added support for all arguments to be passed through
	 --->

	<cffunction name="translatePhrase" returntype="string" hint="Returns translation of a single phrase" output="false">
		<cfargument name="phrase" type="string" required="yes">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#"> <!--- NJH 2010/08/02 RW8.3 --->
		<cfargument name="evaluateVariables" type="boolean" default="yes">

		<cfset arguments.phrase = replaceNoCase(arguments.phrase,"phr_","","ALL")>
		<!--- WAB 2009/09/09 failed if tried to translate phr_   , so now test for blank --->
		<cfif arguments.phrase is not "">
			<!--- WAB 2009/07/15 changed to slightly neater form without the evaluate <cfreturn evaluate("translateListOfPhrases (listOfPhrases=phrase).Phrases.#phrase#.PhraseText")/> --->
			<cfreturn translateListOfPhrases (listOfPhrases=phrase,argumentCollection = arguments).Phrases[phrase].PhraseText/>
		<cfelse>
			<cfreturn "">
		</cfif>



	</cffunction>

	<!--- WAB 2009/02/12
		useful wrapper function for returning the translation of a string which may contain any number of phrases
	 --->

	<cffunction name="translateString" returntype="string" hint="Returns translation of a string " output="false">
		<cfargument name="string" type="string" required="yes">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">

		<cfreturn translateStringComplicated(string = string,mergeStruct=mergeStruct).string >
	</cffunction>

	<!---
	WAB 2012/02/16 This function is used to translate the whole content at the end of a request.  It automatically deals with encoding phrases which are within script tags
	WAB 2014-11-25 CASE 442080 (reinstate Ts and logging)
		Add showTranslationLink argument
		Only add Translation links in plain HTML sections.  ie not in scripts or attributes
		Note, requires new functions in regExp which factorise some existing code
	WAB 2015-03-10 Don't put Translation Ts between <A></A>	tags
		Changed various variable names
	--->
	<cffunction name="translateStringWithAutoEncode" returntype="string" hint="Returns translation of a string " output="false">
		<cfargument name="string" type="string" required="yes">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">
		<cfargument name="showTranslationLink" type="boolean" default = #showTranslationLinks()#>

		<!--- first strip out any do not translate blocks --->
		<cfset var protectResult = protectDoNotTranslateBlocks (string)>
		<cfset var tempResultString = protectResult.string>
		<cfset var args = {}>

		<cfset var section = "">
		<!--- find all script tags containing phrases
			script tag cannot be self closing, which makes the regexp slightly simpler
		--->
		<cfset var regexp = "<script(.(?!</script))*?phr_.*?</script>">
		<cfset var regexpArgumentCollection = {reg_expression = regExp,string = tempResultString, useJavaMatch = true}>
		<cfset var findScript = application.com.regexp.reFindAllOccurrences(argumentCollection = regexpArgumentCollection)>
		<!--- translate each script section individually --->
		<cfloop index="section" array="#findScript#">
			<cfset tempResultString = replace(tempResultString,section.String,translateStringComplicated(string = section.String,encode="javascript",showTranslationLink =  false, argumentCollection = arguments).string)>
		</cfloop>


		<!--- 	WAB 2014-11-25 CASE 442080
			If we are showing translation links then we want to only translate items which are in the innerHTML - never within attributes (such as the Value attribute of a button)
			WAB 2016-05-03 BF-673
			I coded so that phrases within Anchor tags were not given translation link (and got the regExp wrong as well).
			Actually turns out that we can have translation links within <A> tags.  Browsers all seem to interpret <a>xxxx<a>T</a></a> as <a>xxxx</a><a>T</a>.
			Doesn't work nicely if there is extra text after the phrase (so for something like  <a>phr_thankYou ... </a> end up as <a>Thank You</a><a>T</a> ... [with the ... outside the anchor]), but probably not a huge issue
			So removed code which excluded anchors
		 --->
		<cfif showTranslationLink>

			<!--- 	This regExp finds phrases between HTML tags but not inside them (ie not inside HTML attributes)
					It is probably a bit slow, but that does not matter since showing translation links is a not an end-user function
					Jira Fifteen-117 WAB 2015-02-10 Modified to deal with there being no HTML at all in the string (or the phr_ being at the beginning or end of the string)
			   --->

			<cfset var regexp = "(\A|>)[^<>]*?phr_.*?(<|\Z)">
			<cfset var regexpArgumentCollection = {reg_expression = regExp,string = tempResultString, useJavaMatch = true}>
			<cfset var findPhrasesInInnerHTML = application.com.regexp.reFindAllOccurrences(argumentCollection = regexpArgumentCollection)>

			<!--- translate each innerHTML section --->
			<cfloop index="section" array="#findPhrasesInInnerHTML#">
				<cfset tempResultString = replace(tempResultString,section.String,translateStringComplicated(string = section.String,showTranslationLink =  true, argumentCollection = arguments).string)>
			</cfloop>

		</cfif>

		<!--- Now translate everything else, do not show translation links, because could be anywhere --->
		<cfset tempResultString = translateStringComplicated(string = tempResultString, showTranslationLink = false, argumentCollection = arguments).string>

		<!--- finally replace any do not translate blocks --->
		<cfset var result = reinstateDoNotTranslateBlocks(tempResultString,protectResult)>

		<cfreturn result>
	</cffunction>





	<!--- WAB 2011/09/28
		Translates keys of Structure
	 --->

	<cffunction name="translateStructure" returntype="struct" hint="Returns structure with any phr_s translated (mainly of use when passing parameters into cfinput)" output="false">
		<cfargument name="structure" type="struct" required="yes">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">

		<cfset var key = "">

		<cfloop collection="#structure#" item="key">
			<cfif arguments.structure[key] contains "phr_">
				<cfset arguments.structure[key] = translateString(string = arguments.structure[key],mergeStruct = mergeStruct)>
			</cfif>
		</cfloop>

		<cfreturn arguments.structure >
	</cffunction>

	<!--- 	WAB 2014-11-25 CASE 442080 (reinstate Ts and logging) Added defaults to LogRequests and appIdentifier
 	--->
	<cffunction name="translateStringComplicated" returntype="struct"  output="false" >
		<cfargument name="languageID" type="string" default = "">
		<cfargument name="string" type="string" required="yes">
		<cfargument name="countryID" type="string" default = "">
		<cfargument name="defaultlanguage" type="numeric" default = 1>
		<cfargument name="debug" type="boolean" default="false">
		<cfargument name="evaluateVariables" type="boolean" default = "true">
		<cfargument name="executeRelayTags" type="boolean" default=true>  		<!--- note that evaluate variables has to be true as well --->
		<cfargument name="TranslatePhrasesWithinPhrases" type="boolean" default=#evaluateVariables#>
		<cfargument name="useMemoryResident" type="boolean" default = "true">
		<cfargument name="exactMatch" type="boolean" default = "false">
		<cfargument name="doAnalysis" type="boolean" default = "false">
		<cfargument name="logrequests" type="boolean" default = "#isLogRequestsSet()#">
		<cfargument name="appIdentifier" type="string" default = "#LogRequestsAppIdentifier()#">
		<cfargument name="pageIdentifier" type="string" default = "">
		<cfargument name="forcereload" type="boolean" default = "false">
		<cfargument name="onNullShowPhraseTextID" type="boolean" default="yes">
		<cfargument name="nullText" type="string" default = "">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">
		<cfargument name="logMergeErrors" type="boolean" default=true>
		<cfargument name="encode" type="string" default="">
		<cfargument name="trimPhrase" type="numeric" default="0">
		<cfargument name="ShowTranslationLink" type="Boolean" default="false">
		<cfargument name="TranslationLinkCharacter" type="String" default="T">

		<cfset var result = structNew()>
		<cfset var resultString = arguments.string>
		<cfset var listOfPhrases = extractListOfPhrasesFromAString(string)>
		<cfset var translations = "">
		<cfset var protectedContent = arrayNew(1)>
		<cfset var counter = 0>
		<cfset var thisPhrase = "">
		<cfset var PhraseTest = "">
		<cfset var identifier = "">
		<cfset var translationlink = "">

		<cfset structDelete (arguments,"string")>
		<cfset translations = translateListOfPhrases (argumentCollection = arguments,listOfPhrases = listOfPhrases)>


		<!--- Need to protect phrases within phrases when phrases are being edited - possiblity of cf_translate being put around an edit box
		so take out all the sections within these special comments
		WAB 2014-11-25 CASE 442080 - now uses a function
		--->
		<cfset var protectResult = protectDoNotTranslateBlocks (resultString)>
		<cfset resultString = protectResult.string>

			<cfloop collection = "#translations.phrases#" item="identifier">
					<cfset thisPhrase = translations.phrases[identifier]>
					<cfset translationlink = "">
					<cfif showTranslationLink and arguments.encode is not "javascript"> <!--- WAB 2013-03-18 do not add translation link when we know that we are within javascript - invariably causes problems --->

						<!--- NJH 2011/10/28 - use onClick rather than href to get through the XSS security check.. can then get rid of javascript
							WAB 2012-05-2012 CASE 426912. Translate link was not working, replaced ' with " as the attribute qualifier
						--->
						<cfset translationlink = replace(replace(replace(thisPhrase.translationLink,"HREF","ONCLICK"),"javascript:",""""),")","); return false;""")&" href=""##"">#htmleditformat(translationLinkCharacter)#</A>">
					</cfif>
					<CFSET resultString = rereplaceNoCase(resultString, "(phr_#identifier#)([^A-Za-z0-9_]|$)" , "#thisPhrase.phraseText##translationLink#\2","ALL")>
			</cfloop>

		<!--- Reinstate DONOTTRANSLATE blocks, WAB 2014-11-25 CASE 442080 - now uses a function	--->
		<cfset resultString = reinstateDoNotTranslateBlocks(resultString,protectResult)>

		<cfset result.debug = translations.debug>
		<cfif doAnalysis>
			<cfset result.queries = translations.queries>
		</cfif>
		<cfset result.string = resultString>

		<cfreturn result>

	</cffunction>


	<!--- 	WAB 2014-11-25 CASE 442080 (reinstate Ts and logging)
 	 	 	Two new functions to deal with DONNOTTRANSLATE blocks.  Call new factorised functions in regExp.cfc
			NJH 2015/02/11 Pass argments through as argumentCollection so that the content doesn't appear in debug
 	--->

	<cffunction name="protectDoNotTranslateBlocks" returntype="struct"  output="false" >
		<cfargument name="string" type="string" required="yes">

		<cfset var result = string>
		<cfset var args = {reg_expression = this.regExp.doNotTranslate,string = result}>
		<cfset var findDoNotTranslate = variables.applicationscope.com.regExp.reFindAllOccurrences (argumentCollection = args )>
		<cfset args = {string = result, matchedItems = findDoNotTranslate, placeholderString = "~NTPH"}>
		<cfset var placeHolderResult = variables.applicationscope.com.regExp.replaceMatchedItemsWithPlaceHolders (argumentCollection = args)>

		<cfreturn placeHolderResult>

	</cffunction>

	<!--- NJH 2015/02/11 Pass argments through as argumentCollection so that the content doesn't appear in debug --->
	<cffunction name="reinstateDoNotTranslateBlocks" returntype="string"  output="false" >
		<cfargument name="string" type="string" required="yes">
		<cfargument name="placeHolderStructure" type="struct" required="yes">

		<cfreturn variables.applicationscope.com.regExp.reinstatePlaceHolders(argumentcollection = arguments)>

	</cffunction>


	<!---
		extractListOfPhrasesFromAString

		Returns a list of all the phrases in a string

		Note.  To prevent the string reappearing in the debug with odd results (such as javascript running)
		I had to call it with an argument collection

		ie
		dummyStructure = structNew();
		dummyStructure.string = myStringToTranslate;
		listOfPhrases = extractListOfPhrasesFromAString(argumentCollection = dummyStructure);

	--->

	<cffunction name="extractListOfPhrasesFromAString" returntype="string"  output="false" >
			<cfargument name="string" type="string" required="yes">


			<cfset var result = "">
			<cfset var thisPhrase = "">
			<cfset var phraseTest = "">

			<!--- WAB 2012-04-19 CASE 427676
				Even though we weren't replacing phrases in doNotTranslate blocks, we were still retrieving them from the DB and evaluating relaytags - which caused some 'issues'
				So now start by removing any phrases in do not translate areas
			--->
			<cfset arguments.string = rereplacenocase(string,this.regExp.doNotTranslate,"","ALL")>

			<!--- extract list of phrases from the content --->
			<cfset phraseTest = REfindNoCase (this.regExp.FindPhraseInString, string,1, "TRUE" )>  <!--- WAB changed this 2004-11-02 to handle end of string  was "phr_[a-zA-Z0-9_]*([^a-zA-Z0-9_]"--->
		 	<cfloop condition = "phraseTest.pos[1] is not 0">
				<cfset thisPhrase = mid(string,phraseTest.pos[2],phraseTest.len[2])>
				<cfset thisPhrase = replacenocase(thisphrase,"phr_","")>
				<cfset result = listAppend(result,thisPhrase)>
				<cfset phraseTest = REfindNoCase (this.regExp.FindPhraseInString, string,phraseTest.pos[1]+1,"TRUE" )>
			</cfloop>

		<cfreturn result>
	</cffunction>

<!---
		WAB 2010/03/25
		For doing translations within .js files
		In connection with changes to includeJavascriptOnce
		--->
	<cffunction name="extractListOfJavascriptPhrasesFromAString" returntype="string"  output="false" >
			<cfargument name="string" type="string" required="yes">


			<cfset var result = "">
			<cfset var thisPhrase = "">

			<!--- extract list of phrases from the content --->
			<cfset var phraseTest = REfindNoCase (this.regExp.FindJavascriptPhraseInString, string,1, "TRUE" )>
		 	<cfloop condition = "phraseTest.pos[1] is not 0">
				<cfset thisPhrase = mid(string,phraseTest.pos[2],phraseTest.len[2])>
				<cfset result = listAppend(result,thisPhrase)>
				<cfset phraseTest = REfindNoCase (this.regExp.FindJavascriptPhraseInString, string,phraseTest.pos[1]+1,"TRUE" )>
			</cfloop>

			<cfset result = application.com.globalfunctions.removeDuplicates(result)>
		<cfreturn result>
	</cffunction>

	<!---
		translateListOfPhrases

		Takes a list of phrases and returns a structure containing details of each translation

		so translateListOfPhrases("phrase1,phrase2,Phrase3")

		returns

		result.phrases["phrase1"].phraseText
		result.phrases["phrase2"].phraseText
		result.phrases["phrase3"].phraseText

		among other keys

		WAB 2014-11-25 CASE 442080 (reinstate Ts and logging) Added defaults to LogRequests and appIdentifier
	--->
	<cffunction name="translateListOfPhrases" returntype="struct" output="false">
		<cfargument name="listOfPhrases" type="string" required="yes">
		<cfargument name="languageID" type="string" default = "">
		<cfargument name="countryID" type="string" default = "">
		<cfargument name="defaultlanguage" type="numeric" default = 1>
		<cfargument name="debug" type="string" default = "false">
		<cfargument name="evaluateVariables" type="boolean" default = "true">
		<cfargument name="executeRelayTags" type="boolean" default=true>  		<!--- note that evaluate variables has to be true as well --->
		<cfargument name="useMemoryResident" type="boolean" default = "true">
		<cfargument name="exactMatch" type="boolean" default = "false">
		<cfargument name="doAnalysis" type="boolean" default = "false">
		<cfargument name="logrequests" type="boolean" default = "#isLogRequestsSet()#">
		<cfargument name="appIdentifier" type="string" default = "#LogRequestsAppIdentifier()#">
		<cfargument name="pageIdentifier" type="string" default = "">
		<cfargument name="forcereload" type="boolean" default = "false">
		<cfargument name="onNullShowPhraseTextID" type="boolean" default = "true">
		<cfargument name="nullText" type="string" default = "">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">
		<cfargument name="logMergeErrors" type="boolean" default=true>
		<cfargument name="TranslatePhrasesWithinPhrases" type="boolean" default=true>
		<cfargument name="encode" type="string" default="">
		<cfargument name="trimPhrase" type="numeric" default="0">

		<cfset var result = structNew()>
		<cfset var phrasePointerStruct = applicationScope.phrasePointerStruct>
		<cfset var phrasesToGetFromDb = "">
		<cfset var languageKey = "">
		<cfset var savedListOfPhrases = arguments.listOfPhrases>
		<cfset var uniquelistOfPhrases = "">
		<cfset var dbResult = "">
		<cfset var dbResultAnalysis = "">
		<cfset var keyname = "">

		<!--- decide what language and country to use --->
		<CFIF LanguageID is "">
			<CFSET arguments.LanguageID = isDefined("request.relayCurrentUser.languageid")?request.relayCurrentUser.languageid:1>
			<cfparam name="arguments.exactMatch" default="false">
		<cfelse>
			<cfparam name="arguments.exactMatch" default="true">
		</cfif>

	 	<cfif countryid is "">
			<cfset arguments.countryID = isDefined("request.relayCurrentUser.Content.showForCountryID")?request.relayCurrentUser.Content.showForCountryID:17> <!---RJT 17 as a fall back for translated error messages before RelayCurrentUser is set up --->
		</cfif>


		<cfif not exactmatch>
			<cfset languageKey = "#LanguageID#_#CountryID#_#defaultLanguage#">
		<cfelse>
			<cfset languageKey = "#LanguageID#_#CountryID#">
		</cfif>

		<cfset result.debug = "">
		<cfset result.phrases = structNew()>

		<cfset uniquelistOfPhrases = applicationScope.com.globalfunctions.removeDuplicates(listOfPhrases)>

		<!--- work out which items can be got from memory, things like doAnalysis or logrequests or forcereload force database access  --->
		<cfif useMemoryResident and not (doAnalysis or logrequests or forcereload or exactmatch )>

			<!--- Loop through list, decide whether in memory, if not get from db --->
			<cfloop list="#uniquelistOfPhrases#" index="keyname">
				<cfset keyname = rereplacenocase(keyname,'[^a-zA-Z0-9_]','','all')><!--- LHID8224 NYB 2011-12-09 added for validation --->
				<!--- test whether this language is already in memory or not --->
				<cfif StructKeyExists(phrasePointerStruct,keyname) and ((not exactmatch and StructKeyExists (phrasePointerStruct[keyname],LanguageKey)) or (exactmatch and StructKeyExists (application.phraseStruct[keyname],LanguageKey)))>
						<!--- already in memory so don't need to get it again --->
						<cfif debug><cfset result.debug = result.debug & "#keyname# #LanguageKey# in memory<BR>"></cfif>
						<!--- This phrase is in memory, so pop it into our result structure before it disappears (occasional bug previously)--->
						<cfset result.phrases[keyname] = getPhraseFromMemory(phraseIdentifier = keyname,languageKey=languageKey,argumentCollection = arguments)>
				<cfelse>
						<cfif debug><cfset result.debug = result.debug & "#keyname# #LanguageKey# not in memory<BR>"></cfif>
						<cfset phrasesToGetFromDb = listappend(phrasesToGetFromDb,keyname)>
				</cfif>

			</cfloop>

		<cfelse>
			<cfset phrasesToGetFromDb = listOfPhrases>
		</cfif>


		<!--- Now get stuff from db --->
		<cfif phrasesToGetFromDb is not "">
			<cfset arguments.listOfPhrases = phrasesToGetFromDb>
			<cfset dbResult = getPhrasesFromDBToMemory(argumentCollection = arguments)>
			<cfset result.debug = result.debug  & dbResult.debug>
			<!--- now get phrases back out of memory into our results structure --->

			<cfloop list="#phrasesToGetFromDb#" index="keyname">
				<cfset keyname = rereplacenocase(keyname,'[^a-zA-Z0-9_]','','all')><!--- LHID8224 NYB 2011-12-09 added for validation --->
				<cfset result.phrases[keyname] = getPhraseFromMemory(phraseIdentifier = keyname,languageKey=languageKey,argumentCollection = arguments)>
			<!--- 	<cfset result.debug = result.debug  & result.phrases[keyname].debug> --->
			</cfloop>


		</cfif>

		<cfif doAnalysis and useMemoryResident>
			<cfset arguments.listOfPhrases = savedListOfPhrases>
			<cfset dbResultAnalysis = getPhrasesFromDBToMemory(argumentCollection = arguments)>
			<cfset result.queries =  dbResultAnalysis>
		<cfelseif doAnalysis>
			<cfset result.queries = dbResult>
		</cfif>

		<cfreturn result>

	</cffunction>

	<!---
		getPhraseFromMemory

		for a given phrase and language combination, returns a structure contaning the phraseText and other stuff from the memory structure

		The existence of the required phrase has already been confirmed

		keys returned:

		phraseText
		requiresEvaluation (although already done)
		translationLink
		updateidentifier  (form field name which can be used to do an update) - must only be used if doing exact matching and evaluate variables must have been set to false when editing

		Does all the evaluation of variables

		Occasionally this code can fail if between the time of testing for existence of the phrase and reading it, the phrase is updated.
		Possible solution is to catch and just return the original phr_  which is then picked up and retranslated

	--->

	<cffunction name="getPhraseFromMemory" output="false">
		<cfargument name="phraseIdentifier" type="string" required="yes">
		<cfargument name="languageKey" type="string" required="yes">
		<cfargument name="exactMatch" type="boolean" required="yes">
		<cfargument name="evaluateVariables" type="boolean" default = "true">
		<cfargument name="executeRelayTags" type="boolean" default=true>  		<!--- note that evaluate variables has to be true as well --->
		<cfargument name="onNullShowPhraseTextID" type="boolean" default = "true">
		<cfargument name="nullText" type="string" default = "">
		<cfargument name="mergeStruct" type="struct" default = "#structNew()#">
		<cfargument name="logMergeErrors" type="boolean" default=true>
		<cfargument name="TranslatePhrasesWithinPhrases" type="boolean" default=true>
		<cfargument name="encode" type="string" default="">
		<cfargument name="trimPhrase" type="numeric" default="0">
		<cfargument name="debug" type="boolean" default="false">


		<cfset var Result = structNew()>
		<cfset var pointerKey = "">
		<cfset var trimmedPhrase = "">
		<cfset var tempArgs = {}>
		<!---  <cfset var caller = '' /> This variable deliberately not var'ed, this line fools varScoper --->

		<!--- To allow country and language to be available to a merge
			although they don't appear as arguments above they are actually being passed in
		--->
		<cfset arguments.mergeStruct.content = {countryid = arguments.countryid, languageid = arguments.languageid}>

					<cfif not exactMatch>
						<cfset pointerKey = applicationScope.phrasePointerStruct[phraseIdentifier][LanguageKey]>
						<cfif pointerKey is "_"  ><!--- this is a phrase with no translation in the db --->
							<CFSET Result.requiresEvaluation = false>
							<cfif onNullShowPhraseTextID and not refindNoCase(this.regExp.PhraseTextID_EntityName_EntityID_wholeString,phraseIdentifier)>
								<!--- non entity phrase --->
								<!--- WAB 2015-03-10 replace _ with blank and remove sys_ to give a default phrase --->
								<CFSET Result.phraseText = replace(reReplaceNoCase(phraseIdentifier,"sys_",""),"_"," ","ALL")>
							<cfelseif onNullShowPhraseTextID and not refindNoCase(this.regExp.PhraseTextID_EntityName_EntityID_wholeString,phraseIdentifier)>
								<!--- entity phrase --->
								<CFSET Result.phraseText = "">
							<cfelse>
								<CFSET Result.phraseText = nulltext>
							</cfif>
						<cfelse>
							<CFSET Result = structCopy(applicationScope.phraseStruct[phraseIdentifier][pointerKey])>
						</cfif>
					<cfelse>
						<cfif applicationScope.phraseStruct[phraseIdentifier][LanguageKey].isNull >
							<CFSET Result.requiresEvaluation = false>
							<cfif onNullShowPhraseTextID>
								<CFSET Result.phraseText = phraseIdentifier>
							<cfelse>
								<CFSET Result.phraseText = nulltext>
							</cfif>
								<CFSET Result.defaultForThisCountry = 0>

						<cfelse>
							<CFSET Result = structCopy(applicationScope.phraseStruct[phraseIdentifier][LanguageKey])>
						</cfif>
						<cfset result.updateIdentifier = "phrupd_" & applicationScope.phraseStruct[phraseIdentifier]['updateidentifierstub'] & LanguageKey>



						<!--- <CFSET "caller.fld_#identifier#" = "phrupd_" & applicationScope.phraseStruct[identifier]['updateidentifierstub'] & LanguageKey>  					 --->
					</cfif>

					<cfset result.TranslationLink = applicationScope.phraseStruct[phraseIdentifier]['TranslationLink']>

<!--- 				<cfif result.PhraseText is "" and (nullText is not "" or onNullShowPhraseTextID) and not refindNoCase(this.regExp.PhraseTextID_EntityName_EntityID,phraseIdentifier)>  <!--- entity type phrases always get blank if null --->
						<cfset result.PhraseText = iif(nullText is not "","nullText","phraseIdentifier")>
					</cfif>
 --->
					<cfset result.debug = "">
						<!--- Evaluate any Cold Fusion Variables in the phrase --->
					<CFIF evaluateVariables and result.requiresEvaluation>
						<!--- put arguments into a structure so don't appear in CF debug --->
						<cfset tempArgs = {phrasetext = result.PhraseText , phraseTextID = phraseIdentifier, mergeStruct = mergeStruct, logMergeErrors = logMergeErrors,executeRelayTags = executeRelayTags }>
						<cfset result.PhraseText  = EvaluateCFVariables(argumentCollection = tempArgs)>
					</CFIF>


					<cfswitch expression="#encode#">
						<cfcase value = "javascript">
							<cfset result.PhraseText = replaceNoCase(result.PhraseText,"'","\'","ALL")>
							<cfset result.PhraseText = replaceNoCase(result.PhraseText,chr(13)&chr(10),"\n","ALL")>  <!--- I think that this is the usual carriage return marker - seems to work! --->
							<cfset result.PhraseText = replaceNoCase(result.PhraseText,chr(10),"\n","ALL")>  <!--- sometimes have chr(10) without the 13 --->
							<cfset result.PhraseText = replaceNoCase(result.PhraseText,"&nbsp;"," ","ALL")>
							<cfset result.PhraseText = replaceNoCase(result.PhraseText,"<b>","","ALL")>  <!--- actually need a general html remover	 --->
							<cfset result.PhraseText = replaceNoCase(result.PhraseText,"</b>","","ALL")>
						</cfcase>
					</cfswitch>

					<cfif trimPhrase>
						<cfset trimmedPhrase = left(ltrim(REReplaceNoCase(result.phraseText,"<[^>]*>","","ALL")),trimPhrase)>
						<cfif trim(trimmedPhrase) is "">
							<cfset trimmedPhrase = left(result.phraseText,trimPhrase)>
						</cfif>
						<cfset result.phraseText = trimmedPhrase>
					</cfif>

					<!--- 2009/02/12 WAB testing allowing for phrases within phrases (with a special catch for infinite recursion)--->
					<cfif TranslatePhrasesWithinPhrases and reReplaceNoCase(result.PhraseText,this.regExp.doNotTranslate,"","ALL")  contains "phr_" and result.PhraseText does not contain phraseIdentifier>
						<cfset arguments.string = result.PhraseText>
						<!--- WAB 2014-09-04 Added onNullShowPhraseTextID=true, nullText = "".  Do not want the value of this parameter passed through from the original call, return to default
								Problem particularly apparent on portal when processing relayTags
							WAB 2014-11-25 CASE 442080 (reinstate Ts and logging)
								changed to call the autoencode version of translateString

						 --->
						<cfset result.PhraseText = translateStringWithAutoEncode(argumentCollection=arguments, onNullShowPhraseTextID = true, nullText = "")>
					</cfif>


		<cfreturn result>

	</cffunction>

	<!---
	getPhraseFromDBToMemory
	called by translateListOfPhrases
	does the getting of phrases and putting them in memory

	WAB 2014-11-25 CASE 442080 (reinstate Ts and logging) Added defaults to LogRequests and appIdentifier

	 --->

	<cffunction name="getPhrasesFromDBToMemory" returntype="struct" hint="Returns translations" output="false" access="private">
		<cfargument name="listOfPhrases" type="string" required="yes">
		<cfargument name="languageID" type="numeric" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="defaultlanguage" type="numeric" default = 1>
		<cfargument name="debug" type="string" default = "false">
		<cfargument name="exactMatch" type="boolean" default = "false">
		<cfargument name="doAnalysis" type="boolean" default = "false">
		<cfargument name="logrequests" type="boolean" default = "#isLogRequestsSet()#">
		<cfargument name="appIdentifier" type="string" default = "#LogRequestsAppIdentifier()#">
		<cfargument name="pageIdentifier" type="string" default = "">
		<cfargument name="forcereload" type="boolean" default = "false">
		<cfargument name="useMemoryResident" type="boolean" default = "true">
		<cfargument name="onNullShowPhraseTextID" type="boolean" default = "true">

		<!---  <cfset var applicationScope = '' /> This variable deliberately not var'ed, this line fools varScoper --->
		<cfset var result = getPhrasesFromDB(argumentCollection = arguments)>
		<cfset var languageKey = "">
		<cfset var actualLanguageofPhrase = "">
		<cfset var tempArgs = {}>

		<cfif not exactmatch>
			<cfset languageKey = "#LanguageID#_#CountryID#_#defaultLanguage#">
		<cfelse>
			<cfset languageKey = "#LanguageID#_#CountryID#">
		</cfif>

		<!--- if we are using memory resident variables then save query results to memory --->
		<cfif useMemoryResident>
			<cfloop query="result.translate">
				<cftry>
					<cfset actualLanguageofPhrase = "#result.translate.languageid#_#result.translate.countryid#">
				<cfcatch>
					<cfoutput>Error relayTranslations debug</cfoutput>
					<cfdump var="#result#"><CF_ABORT>
				</cfcatch></cftry>

				<cfif not StructKeyExists(applicationScope.phrasePointerStruct,identifier)>
						<cfset applicationScope.phraseStruct[identifier] = structNew()>
						<cfset applicationScope.phrasePointerStruct[identifier] = structNew()>
						<cfif debug><cfset result.debug = result.debug & "new structure for #keyname#.<BR>"></cfif>
				</cfif>

				<cfif not exactMatch>
					<!--- if we are doing exact matches we don't use the pointer, we just populate the phrasestruct itself --->
					<cfset applicationScope.phrasePointerStruct[identifier][LanguageKey]	= actualLanguageofPhrase>
					<cfif debug><cfset result.debug  = result.debug  & "Pointer for #identifier# #LanguageKey# set to #actualLanguageofPhrase#	<BR>"></cfif>
					<cfset applicationScope.phraseStruct[identifier][actualLanguageofPhrase] = structNew()>
					<cfset applicationScope.phraseStruct[identifier][actualLanguageofPhrase].phraseText	= phrasetext>
					<!--- Call to checkForCFVariables outputs guff to debug.  Need to hide parameter in an argument  structure so that it does not get displayed--->
					<cfset tempArgs ={phrasetext = phrasetext}>
					<cfset applicationScope.phraseStruct[identifier][actualLanguageofPhrase].requiresEvaluation	= checkForCFVariables(argumentCollection = tempArgs)>
					<cfset applicationScope.phraseStruct[identifier][actualLanguageofPhrase].isNull	= iif(phraseid is "",true,false)>

				<cfelse>
					<cfset applicationScope.phraseStruct[identifier][LanguageKey] = structNew()>
					<cfset applicationScope.phraseStruct[identifier][LanguageKey].phraseText	= phrasetext>
					<!--- Call to checkForCFVariables outputs guff to debug.  Need to hide parameter in an argument  structure so that it does not get displayed--->
					<cfset tempArgs ={phrasetext = phrasetext}>
					<cfset applicationScope.phraseStruct[identifier][LanguageKey].requiresEvaluation	= checkForCFVariables(argumentCollection = tempArgs)>
					<cfset applicationScope.phraseStruct[identifier][LanguageKey].isNull	= iif(phraseid is "",true,false)>
					<cfset applicationScope.phraseStruct[identifier][LanguageKey].defaultForThisCountry	= defaultForThisCountry>
				</cfif>

					<cfset applicationScope.phraseStruct[identifier]['TranslationLink']	= TranslationLink>
					<cfset applicationScope.phraseStruct[identifier]['UpdateIdentifierStub']	= UpdateIdentifierStub>
				    <cfif debug><cfset result.debug = result.debug & "#identifier# #actualLanguageofPhrase# set 	<BR>"></cfif>

			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!---
	WAB 2014-11-25 CASE 442080 (reinstate Ts and logging) Added defaults to LogRequests and appIdentifier
	WAB 2016-11-29 PROD2016-2837 Errors when doAnalysis=true, especially when no phrases to analyse
	 --->

	<cffunction name="getPhrasesFromDB" returntype="struct" hint="Returns translations" output="false" access="private">
		<cfargument name="listOfPhrases" type="string" required="yes">
		<cfargument name="languageID" type="numeric" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="defaultlanguage" type="numeric" default = 1>
		<cfargument name="debug" type="string" default = "false">
		<cfargument name="exactMatch" type="boolean" default = "false">
		<cfargument name="doAnalysis" type="boolean" default = "false">
		<cfargument name="logrequests" type="boolean" default = "#isLogRequestsSet()#">
		<cfargument name="appIdentifier" type="string" default = "#LogRequestsAppIdentifier()#">
		<cfargument name="pageIdentifier" type="string" default = "">
		<cfargument name="forcereload" type="boolean" default = "false">
		<cfargument name="useMemoryResident" type="boolean" default = "true">
		<cfargument name="onNullShowPhraseTextID" type="boolean" default = "true">

		<CFSET var createTempTableQueryString ="">
		<CFSET var createTempTableForAnalysisQueryString ="">
		<cfset var querystring = "">
		<cfset var languageKey = "">
		<cfset var thisPhrase = "">
		<cfset var checkDB = "">
		<cfset var create = "">
		<cfset var insert= "">
		<cfset var index= "">
		<cfset var drop= "">
		<cfset var result = {translate = queryNew('dummy'), headings= queryNew('dummy'), data =queryNew('dummy')}>

		<cfif listOfPhrases is "">
			<cfreturn result>
		</cfif>

		<!--- create temporary Table with hopefully unique name, was using a date plus random number, but were signs of conflicts so now using UUID --->
		<cfset var uuid = replace(createUUID(),"-","","ALL")>
		<cfset var tableName = "##" & uuid>

		<cfif debug>
			<cfset tableName = "aaaaDeleteMe" & uuid >
		</cfif>


		<cfset result.debug = "">

		<cfloop list="#listOfPhrases#" index="thisPhrase">
			<cfset thisPhrase = rereplacenocase(thisPhrase,'[^a-zA-Z0-9_]','','all')><!--- LHID8224 NYB 2011-12-09 added for validation --->
			<cfset queryString = " insert into #tableName# (phraseTextID, entityType, entityid, EntityTypeID)	values (" >
			<!--- this bit creates the value string as either 'phrasetextid','entityType','entityTypeID'  or 'phrasetextid','0','0' dependant upon whether it is an entity phrase or not--->
			<cfif refindNoCase(this.regExp.PhraseTextID_EntityName_EntityID_wholeString,thisPhrase)>  <!--- test for entityPhrase --->
				<cfset queryString = queryString & "#reReplaceNoCase(thisPhrase,"#this.regExp.PhraseTextID_EntityName_EntityID_WholeString#","'\1','\2','\3',0")#">
			<cfelse>
				<cfset queryString = queryString & "#reReplaceNoCase(thisPhrase,"([a-zA-Z0-9_]*)$","'\1','','0',0")#">
			</cfif>
			<cfset queryString = queryString & ")#chr(10)#">
			<cfset createTempTableQueryString = createTempTableQueryString & querystring>
		</cfloop>

		<cfquery name = "create" dataSource = "#applicationScope.sitedatasource#">
		CREATE TABLE #tableName# (
		phrasetextid varchar (100) NOT NULL , <!--- NJH 2009/02/27 Bug Fix Sophos Relayware Set Up Issue 1873 - was 50  --->
		entityid int NOT NULL ,
		entityTypeId int NOT NULL ,
		entityType varchar (50) NOT NULL
		)
		</cfquery>

		<cfquery name = "insert" dataSource = "#applicationScope.sitedatasource#">
			#preserveSingleQuotes(createTempTableQueryString)#
		</cfquery>

		<cfquery name = "index" dataSource = "#applicationScope.sitedatasource#">
			CREATE INDEX IX_#tableName# ON #tableName# (entityTypeId, entityid, phrasetextid)
		</cfquery>

		<cfif debug>
			<cfset result.debug = result.debug & "<BR>exec translatePhrases 	@TableName = '#TableName#', 	@RequiredCountry = '#CountryID#',	@RequiredLanguage ='#LanguageID#', 	@FallbackLanguage = '#defaultLanguage#',	@ExactMatch = '#iif(exactMatch,DE('1'),DE('0'))#', 	@NullText ='',	@DoAnalysis = '#iif(doAnalysis,DE('Yes'),DE('No'))#' ,	@onNullShowPhraseTextID = #iif(onNullShowPhraseTextID,DE('1'),DE('0'))#,	@logRequests = #iif(LogRequests,DE('1'),DE('0'))#,	@appIdentifier ='#appIdentifier#',	@pageIdentifier ='#pageIdentifier#'<BR>">
		</cfif>

		<CFSTOREDPROC PROCEDURE="translatePhrases" DATASOURCE="#applicationScope.siteDataSource#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@TableName" VALUE="#TableName#" null="no">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@RequiredCountry" VALUE="#CountryID#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@RequiredLanguage" VALUE="#LanguageID#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@FallbackLanguage" VALUE="#defaultLanguage#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ExactMatch" VALUE="#iif(exactMatch,DE('1'),DE('0'))#" >
			<cfif useMemoryResident>
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@NullText" VALUE="" >
			<cfelse>
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@NullText" VALUE="#nullText#" >
			</cfif>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@DoAnalysis" VALUE="#iif(doAnalysis,DE('Yes'),DE('No'))#" >
			<cfif useMemoryResident>
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@onNullShowPhraseTextID" VALUE="1" >
			<cfelse>
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@onNullShowPhraseTextID" VALUE="#iif(onNullShowPhraseTextID,DE('1'),DE('0'))#" >
			</cfif>

			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@logRequests" VALUE="#iif(LogRequests,DE('1'),DE('0'))#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@appIdentifier" VALUE="#appIdentifier#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@pageIdentifier" VALUE="#pageIdentifier#" >

			<CFPROCRESULT NAME="result.Translate" RESULTSET="1">
			<CFPROCRESULT NAME="result.Headings" RESULTSET="2">
			<CFPROCRESULT NAME="result.Data" RESULTSET="3">

		</CFSTOREDPROC>

		<cfif not debug >

			<cfquery name = "drop" dataSource = "#applicationScope.sitedatasource#">
				drop TABLE #tableName#
			</cfquery>

		</cfif>

		<CFRETURN RESULT>

	</CFFUNCTION>


	<!---
	WAB 2014-11-25 CASE 442080 (reinstate Ts and logging) Added new functions to replace sections of code in cf_translate
	 --->
	<cffunction name="showTranslationLinks">
		<cfset var result = false>

		<cfif (isdefined("session.translation.showTranslationLinks") and session.translation.showTranslationLinks is true)
			or (isdefined("request.relayCurrentUser.content.showTranslationLinks") and request.relayCurrentUser.content.showTranslationLinks )	>
			<CFSET result = true>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="isLogRequestsSet">
		<cfset var result = false>

		<!--- 2015-07-02 WAB Add test for session. Sometimes called from sitewideErrorHandler before session is created --->
		<cfif isDefined("session")>
			<cfif structKeyExists(session,"LogTranslationRequests") and session.LogTranslationRequests is true
				and structKeyExists(session,"LogTranslationAppIdentifier") and session.LogTranslationAppIdentifier is not ""
			>
				<CFSET result = true>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="LogRequestsAppIdentifier">
		<cfset var result = "">

		<cfif isLogRequestsSet()>
			<cfset result = session.LogTranslationAppIdentifier>
		</cfif>

		<cfreturn result>
	</cffunction>


<!---
generateTranslationAnalysis

generates the crossTab report from the analysis data brought back by getPhrasesFromDB

--->

<cffunction name="generateTranslationAnalysis" >
	<cfargument name="queries" type="struct" required="yes">

			<cfset var result_html = "">
			<cfset var data = queries.data>
			<cfset var headings = queries.headings>

			<cfsaveContent variable = "result_html">
			<CF_Translate showTranslationLink="true" doAnalysis = "false" trimPhrase="30" LogRequests="false" processEvenIfNested=true>
				<cfif data.recordcount is not 0>
					<CF_FNLCrossTabV2
						columnHeadingQuery = #headings#
						dataQuery = #data#
						ColumnIDField = "LanguageANDcountry"
						ColumnHeadingEval = "'##Language##<BR>##country##'"
						RowHeadingEval = "'phr_##Identifier##<BR><FONT size=1>(##Identifier##)</font><BR>|##phraseid##'"
						valueEval = "'x'"
					>
				</cfif>
			</CF_Translate>
			</cfsavecontent>
	<cfreturn result_html>

</cffunction>



<!---
checkForAndEvaluateCFVariables

A function used to check whether a phrase has any embedded cf variables and if so to evaluate the whole thing
This is called in a few places in cf_translate

WAB 2005-10-19
From code in cf_translate with modifications to handle [[ ]] notation


WAB 2006-02-20
checkForCFVariables
function to return true or false based on same regular expressions used in checkForAndEvaluateCFVariables
allows me to check phrases once and remember whether they need to be evaluated



WAB 2009/03/04 pulled out these regExp into variables
changed regExpHash from ##([a-zA-Z_][[:graph:]]*)##  to ##([a-zA-Z_][A-Za-z0-9_.]*)## to that only picks up valid CF variable names, not bits of HTML with all sorts of characters

 --->

	<cffunction name="checkForCFVariables" output="false">>
			<cfargument name="phraseText" required="true">

		<cfif refindNoCase (this.RegExp.requiresEvaluating,phraseText) >
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>


		<cffunction name="checkForAndEvaluateCFVariables" output="false">
			<cfargument name="phraseText" required="true">
			<cfargument name="phraseTextID" default = "">
			<cfargument name="mergeStruct" default = #structNew()#>  <!--- WAB 2007-06-26 another idea of being able to pass in merge variables without having to use the form Scope.  Aliased as merge. --->
			<cfargument name="logMergeErrors" default = "true">
			<cfargument name="executeRelayTags" default = "true">

			<cfif checkForCFVariables(phraseText)>
				<cfreturn EvaluateCFVariables (argumentCollection = arguments)>
			<cfelse>
				<cfreturn phraseText>
			</cfif>

		</cffunction>

		<cffunction name="EvaluateCFVariables" output="false">
			<cfargument name="phraseText" required="true">
			<cfargument name="phraseTextID" default = "">
			<cfargument name="mergeStruct" default = #structNew()#>  <!--- WAB 2007-06-26 another idea of being able to pass in merge variables without having to use the form Scope.  Aliased as merge. --->
			<cfargument name="logMergeErrors" default = "true">
			<cfargument name="executeRelayTags" default = "true">


			<cfset var result = "">
			<cfset var mergeObject = getMergeObject()>

			<cfset mergeObject.initialiseMergeStruct(argumentCollection = arguments)>
			<cfset result = mergeObject.checkForAndEvaluateCFVariables(argumentCollection = arguments)>
			<cfreturn result>

		</cffunction>

		<cffunction name="createAMergeObject">
			<cfset var result = "">
			<cfif fileExists (expandPath("/code/cftemplates/mergefunctions.cfc"))>
				<cfset result = createObject("component","code.cftemplates.mergeFunctions")>
			<cfelse>
				<cfset result = createObject("component","com.mergeFunctions")>
			</cfif>
			<cfreturn result>
		</cffunction>

		<cffunction name="getMergeObject">

			<cfif not this.cachedMergeObjectLoaded>
				<cfset this.cachedMergeObject = createAMergeObject()>
				<cfset this.cachedMergeObjectLoaded = true >
			</cfif>

			<cfreturn  duplicate(this.cachedMergeObject)>  <!--- seems to be quicker than a creation--->
		</cffunction>
			<cfset this.cachedMergeObjectLoaded = false >

		<cffunction name="dropCachedMergeObject">

			<cfset this.cachedMergeObjectLoaded = false >
<!---
			WAB 2011/11/10 removed the actual deleting of the cachedMergeObject, just set this.cachedMergeObjectLoaded to false to ensure reloading
			I think that this might have caused some intermittent errors on some sites "CACHEDMERGEOBJECT is undefined in THIS."
			<cfif this.cachedMergeObjectLoaded>
				<cfset structdelete(this,"cachedMergeObject")>
			</cfif>
 --->

		</cffunction>


		<!--- use if for some reason arguments.executeRelayTags was set to false and you then want to execute the tags --->
		<cffunction name="executeRelayTags">
			<cfargument name="phraseText" required="true">
			<cfargument name="mergeStruct" default = #structNew()#>  <!--- WAB 2007-06-26 another idea of being able to pass in merge variables without having to use the form Scope.  Aliased as merge. --->

			<cfset var result = "">
			<cfset var mergeObject = getMergeObject()>

			<cfset mergeObject.initialiseMergeStruct(argumentCollection = arguments)>
			<cfset result = mergeObject.executeRelayTags(argumentCollection = arguments)>
			<cfreturn result>

		</cffunction>

<!--- COUNTRY GROUP METHODS --->
<cffunction name="addTranslationToCountryGroup" hint="Returns a structure containing translations of all the phrases associated with one or more entities">
	<cfargument name="phraseText" type="string" required="yes">
	<cfargument name="phraseTextID" type="string" required="yes">
	<cfargument name="countryGroupID" type="string" required="yes">
	<cfargument name="overWriteOrigional" required="no" default="false">

	<cfset var returnArray = arrayNew(1)>
	<cfset var addReturn = "">
	<cfset var updateMode = "nooverwrote">
	<cfset var newPhraseTextID = "">
	<cfset var detailStruct = "">
	<cfset var getCountryGroupDetail = "">

	<cfif arguments.overWriteOrigional is "true">
		<cfset updateMode = "overwrite">
	</cfif>
	<cfquery name="getCountryGroupDetail" datasource="#application.sitedatasource#">
		select		*
		from		translationCountryGroup tcg inner join
					translationCountryGroupCountries tcgc on tcgc.translationCountryGroupID = tcg.translationCountryGroupID inner join
					country c on tcgc.countryID = c.countryID
		where		tcg.translationCountryGroupID =  <cf_queryparam value="#arguments.countryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		order by	c.countryDescription
	</cfquery>

	<cfloop query="getCountryGroupDetail">
		<cfset newPhraseTextID = arguments.phraseTextID>
		<cfif listGetAt(arguments.phraseTextID,1,"_") is "phr">
			<cfset newPhraseTextID = listDeleteAt(arguments.phraseTextID,1,"_")>
		</cfif>
		<cfset addReturn = addNewPhraseAndTranslationV2(
			addmode = true,
			updatemode = '#updateMode#',
			userID = request.relaycurrentuser.personid,
			LanguageID = languageID,
			countryID = countryID,
			phraseText = '#arguments.phraseText#',
			phraseTextID = '#newPhraseTextID#'
		)>
		<cfset detailStruct = structNew()>
		<cfset detailStruct.countryDescription = countryDescription>
		<cfset detailStruct.countryID = countryID>
		<!--- NJH 2008/10/02 BUG Fix Lexmark Issue 1051 - changed addReturn.phraseExists to addReturn.details.phraseExists --->
		<cfset detailStruct.phraseExists = addReturn.details.phraseExists>
		<cfif detailStruct.phraseExists eq 1 and arguments.overWriteOrigional eq "true">
			<cfset detailStruct.message = "Overwritten">
		<cfelseif detailStruct.phraseExists eq 1 and arguments.overWriteOrigional eq "false">
			<cfset detailStruct.message = "Skipped">
		<cfelse>
			<cfset detailStruct.message = "Inserted">
		</cfif>
		<cfscript>
			arrayAppend(returnArray,detailStruct);
		</cfscript>
	</cfloop>

	<cfreturn returnArray>
</cffunction>

<!---
WAB 2008/07/09 changes so can deal with phrase files in userfiles directory
WAB 2009/11/09  Mod so that flush of phrases is done in one lot at the end
WAB 2009/02/23
 --->
<cffunction access="public" name="addPhrasesFromLoadFile" hint="adds Phrases which are defined in a phrase load file">
	<cfargument name="phraseFileName" default = "">
	<cfargument name="phraseFileContent" default = "">  <!--- can just pass the text of the phrase file to release the phrases --->
	<cfargument name="phraseFileLocation" type="string" default="">    <!--- in future perhpas we could load from elsewhere --->
	<cfargument name="addMode" required="no" default="true">   <!--- true/false --->
	<cfargument name="updateMode" required="no" default="overwriteUnEdited">  <!--- overwrite/merge/ignore/overwriteUnEdited--->

	<cfset var result = structNew()>
		<cfset var thisEntityType = "">
		<cfset var thisEntityTypeID = 0>
		<cfset var thisEntityID = 0>
		<cfset var thisPhraseTextID = "">
		<cfset var PhraseTextIDList = "">
		<cfset var sysPhrases = "">
		<cfset var regExp= "">
		<cfset var reFindResult = "">
		<cfset var addResult= "">
		<cfset var languageBit= "">


	<cfif phraseFileName is "" and phraseFileContent is "">
		<cfoutput>phraseFileName  or  phraseFileContent required </cfoutput><cfreturn result>
	</cfif>

		<cfset sysPhrases  = getPhraseFileQuery (phraseFileName = phraseFileName,phraseFileContent = phraseFileContent )>

		<cfset result = {phrases = structNew(), sysphrases = sysphrases, Added = "",Updated = "" ,NotUpdated = "", Other = "", notUpdatedBecauseEdited = ""}>


	<cfif addMode or listFindNoCase("overwrite,overwriteUnEdited",updateMode)>   <!--- may need "merge" here as well --->
		<cfloop  query="sysPhrases">

			<!--- WAB 2009/02/23 allow loading of entity related phrases via file
				This regExp will pick up phrases of the form "someThing_AnEntity_9999"
				--->
			<!--- 	<cfset regExp = "([a-zA-Z0-9_]*)_([a-zA-Z0-9]*)_([0-9]*)"> --->
			<!--- WAB 2010/04/12, altered to use refindAllOccurrences (which does all the mid stuff for you) --->
			<!--- NYB 2010/10/18, removed ",1, true " from end of parameter list sent to refindAllOccurrences  --->
			<cfset reFindResult = application.com.regexp.refindAllOccurrences (this.regExp.PhraseTextID_EntityName_EntityID, phraseTextID)>

			<CFIF arrayLen(reFindResult) is not 0 and structKeyExists (application.entityTypeid,reFindResult[1][3])>
				<cfset thisEntityType = reFindResult[1][3]>
				<cfset thisEntityTypeID = application.entityTypeid[thisEntityType]>
				<cfset thisPhraseTextID = reFindResult[1][2]>
				<cfset thisEntityID = reFindResult[1][4]>

			<cfelse>
				<cfset thisPhraseTextID =phraseTextID>
				<cfset thisEntityType = 0>
				<cfset thisEntityID = 0>
			</CFIF>

			<!--- WAB 2009/11/09 added updatecluster = false here and create a list of all the phrases, blow at end--->
			<cfset PhraseTextIDList = listappend (PhraseTextIDList,phraseTextID)>
			<cfset addResult =addNewPhraseAndTranslationV2(addMode=addMode, updateMode=updateMode, phraseTextID = thisphrasetextid, entityTypeID = thisEntityTypeID, EntityID =thisEntityID , phraseText = phrasetext, countryID = countryID, languageid = languageid,updatecluster= false)>

			<cfset result.phrases[phraseTextID]["#languageid#-#countryid#"] = addresult>

			<cfset languageBit = " (#languageid#-#countryid#)">


			<cfif addResult.details.translationExists and addResult.details.translationAddedUpdated>
				<cfset result.Updated = ListAppend(result.Updated,phraseTextID & languageBit)>
			<cfelseif addResult.details.translationExists and addResult.details.notUpdatedBecauseEdited>
				<cfset result.notUpdatedBecauseEdited = ListAppend(result.notUpdatedBecauseEdited,phraseTextID & languageBit)>
			<cfelseif addResult.details.translationExists and not addResult.details.translationAddedUpdated>
				<cfset result.NotUpdated = ListAppend(result.NotUpdated,phraseTextID & languageBit)>
			<cfelseif not addResult.details.translationExists and addResult.details.translationAddedUpdated>
				<cfset result.Added = ListAppend(result.Added,phraseTextID & languageBit)>
			<cfelse>
				<cfset result.Other = ListAppend(result.Other,phraseTextID & languageBit)>
			</cfif>
		</cfloop>

<!--- WAB 2009/11/09 just blow list rather than whole structure --->
			<cfset resetPhraseStructureKey(PhraseTextIDList) >


	</cfif>

		<cfreturn result>



	</cffunction>


	<!---
	WAB 2008/07/09
	I pass around the name of a phrase file as
	user|xyz_phrases.txt or relay|xyz_phrases.txt
	This function brings back the relative path to the file (I needed it in a few places)
	 --->
	<cffunction access="public" name="getPhraseFilePath" hint="gets path to phrase file">

		<cfargument name="phraseFileName" default = "">

		<cfset var path = "">
		<cfset var filename = "">

		<cfset path = "\translation\loadPhrases\">
		<cfset filename = listLast(phraseFileName,"|")>

		<cfif listfirst(phraseFileName,"|") is "user">
			<cfset path = "\code\loadPhrases\">
		</cfif>

		<cfreturn "#path##filename#">

	</cffunction>

	<!--- WAB 2016-01-28 Added support for CFML comments in phrase files
			Also removed an htmlEditFormat within the querysim - would have endcoded any HTML if the phraseFileContent argument had been used - luckily wasn't
	--->
	<cffunction access="public" name="getPhraseFileQuery" hint="uses querySim to get content of phrase file into a query">

		<cfargument name="phraseFileName" default = "">
		<cfargument name="phraseFileContent" default = "">

		<cfif PhraseFileName is not "">
			<cffile action="read" file="#expandPath (getPhraseFilePath(phraseFileName))#" variable="arguments.phraseFileContent">
		</cfif>

		<cfset arguments.phraseFileContent = application.com.regExp.removeComments (arguments.phraseFileContent,"CF")>

		<cfset var sysPhrases = "">

		<cf_querySim>
			<cfoutput>
			SysPhrases
			phraseTextID, phraseText, languageid, countryid
			#phraseFileContent#
		</cfoutput>
		</cf_querySim>

		<!--- WAB 2008/07/15 deals with blank lines --->
		<cfquery name= "sysPhrases" dbtype="query">
		select * from sysPhrases where phrasetextID <> ''
		</cfquery>

		<cfreturn sysPhrases>

	</cffunction>


	<cffunction access="public" name="doesUserHaveRightsToCountryLanguageCombination" hint="" returns="boolean">
		<cfargument name="CountryID" type="numeric" required="yes">
		<cfargument name="LanguageID" type="numeric" required="yes">

		<!--- if languageid = -1 , is new phrase so have to assume rights, or maybe should be checking add phrase right--->
		<cfif (CountryID is 0 or listfind(request.relayCurrentUser.countryList ,CountryID) or listfind(request.relayCurrentUser.regionList ,CountryID)) and (languageid is 0 or languageid is -1 or listfind(request.relaycurrentuser.LanguageEditRights,languageid))>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>


	</cffunction>


	<!---
	2010/09/15	WAB	Code to interact with Database Level PhrasePointer Cache
	--->
	<!--- Function to check that cache is up to date before runnign queries against it
	The database keeps a status table so knows whether the cache is up to date - so should be v quick once cache populated for first time
	 --->
	<cffunction access="public" name="checkPhrasePointerCache" hint="updates Cache">
			<cfargument name="entityType" required="true">
			<cfargument name="phraseTextID" required="true">
			<cfargument name="countryID" default = "#request.relayCurrentUser.Content.showForCountryID#" type="numeric">
			<cfargument name="languageID" default = "#request.relayCurrentUser.languageID#" type="numeric">

			<cfset var populateCache = "">

			<cfquery name="populatecache" datasource = "#application.sitedatasource#">
			exec updatePhrasePointerCacheForEntityType
					@entityType =  <cf_queryparam value="#arguments.entityType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					@phraseTextID =  <cf_queryparam value="#arguments.phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					@languageid  = #arguments.languageid# ,
					@countryid = #arguments.CountryID#
			</cfquery>

		<cfreturn>
	</cffunction>

	<!--- This function was only really used when I was testing.  It populates the cache for the first time for a given language combination --->
	<cffunction access="public" name="checkCompletePhrasePointerCache" hint="updates Cache">
			<cfargument name="countryID" default = "#request.relayCurrentUser.Content.showForCountryID#" type="numeric">
			<cfargument name="languageID" default = "#request.relayCurrentUser.languageID#" type="numeric">

			<cfset var populateCache = "">
			<cfset var getEntities = "">

			<cfquery name="getEntities" datasource = "#application.sitedatasource#">
			select distinct s.entityName, pl.phraseTextID from phraselist pl inner join schematable s on pl.entityTypeID = s.entityTypeID
			where pl.entityTypeID <> 0
			</cfquery>

			<cfloop query="getEntities">
				<cfquery name="populatecache" datasource = "#application.sitedatasource#">
				exec updatePhrasePointerCacheForEntityType
						@entityType =  <cf_queryparam value="#entityName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						@phraseTextID =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						@languageid  = #arguments.languageid# ,
						@countryid = #arguments.CountryID#
				</cfquery>
			</cfloop>

		<cfreturn>
	</cffunction>


	<!---
	use this function for linking tables to entity phrases directly in a query
	eg phrases like phr_Headline_Element_1
	 --->

	<cffunction access="public" name="getPhrasePointerQuerySnippets" hint="gets a join and a select statement for building queries which link to the phrase Table" returns="struct">
		<cfargument name="entityType" required="true">
		<cfargument name="phraseTextID" required="true">
		<cfargument name="countryID" default = "#request.relayCurrentUser.Content.showForCountryID#">
		<cfargument name="languageID" default = "#request.relayCurrentUser.languageID#">
		<cfargument name="baseTableAlias" default="">  <!--- alias of the table that the entityID is in, only needed there are column name conflicts (such as countryid, languageid) --->
		<cfargument name="entityIDColumn" default="">  <!--- only required if the entityID column name is not what we would expect from TableSchema (perhaps it has been aliased in a view )--->
		<cfargument name="checkCache" default="true">
		<cfargument name="cast" default="true">
		<cfargument name="phraseTableAlias" default="">
		<cfargument name="nullText" default="''"> <!--- text (in single quotes)to show if the phrase does not exist, or column name --->


		<cfset var result = structNew()>

		<cfif arguments.baseTableAlias is not "">
			<cfset arguments.baseTableAlias = baseTableAlias & ".">
		</cfif>

		<cfif arguments.entityIDColumn is "">
			<cfset arguments.entityIDColumn = application.entitytype[application.entitytypeid[entityType]].uniquekey>
		</cfif>

		<cfif arguments.phraseTableAlias is "">
			<cfset result.phraseTableAlias = "p_#entityType#_#phraseTextID#">
		<cfelse>
			<cfset result.phraseTableAlias = arguments.phraseTableAlias>
		</cfif>

		<cfset result.select = "isnull(#result.phraseTableAlias#.phraseText,#nullText#)">
		<cfif arguments.cast>
			<cfset result.select  = "convert(nVarchar(4000),#result.select#)">
		</cfif>

		<!--- 2010/11/16			WAB/NAS		LID4706: CR015 : Performance/Usability CR --->
		<cfset result.join =  "left join vphrasepointercache as #result.phraseTableAlias#
									on 		#result.phraseTableAlias#.phraseTextID = '#phraseTextID#'
										and #result.phraseTableAlias#.entityTypeID = #application.entitytypeid[entityType]#
										and #result.phraseTableAlias#.entityid = #baseTableAlias##entityIDColumn#
										and #result.phraseTableAlias#.languageid = #languageid#
										and  #result.phraseTableAlias#.countryid = #CountryID#" >

		<cfif arguments.checkCache>
			<cfset checkPhrasePointerCache (entityType = arguments.entityType, phraseTextID = arguments.phraseTextID, countryid = arguments.countryid, languageid = arguments.languageid)>
		</cfif>


		<cfreturn result>

	</cffunction>


	<!---
	use this function for linking textIDs to phrases directly in a query
	eg phrases like phr_ContractStatus_Active
	where ContractStatus is the name of a table and Active is the textID on that table

	Pass in the TableName   (eg contractStatus)
				Alias - Alias of the table  (defaults to #tablename#)
				textIDColumn  - this is the column that the phraseTextIs is derived from (defaults to #TableName#TextID   eg ContractStatusTextID)
				Prefix   - what is prefixed to the value of the textIDColumn to make the phrase (does not need the final _ which is assumed).  Defaults to the tableName
	NJH 2017/02/28	RT-310 - added nullColumn argument

	 --->

	<cffunction access="public" name="getPhrasePointerQuerySnippetsForTextID" hint="gets a join and a select statement for building queries which link to the phrase Table" returns="struct">
		<cfargument name="tableName" required="true">
		<cfargument name="Alias" default="#tableName#">
		<cfargument name="TextIDColumn" required="true" default = "#tablename#TextID">
		<cfargument name="Prefix" required="true" default = "#tableName#">
		<cfargument name="countryID" default = "#request.relayCurrentUser.Content.showForCountryID#">
		<cfargument name="languageID" default = "#request.relayCurrentUser.languageID#">
		<cfargument name="checkCache" default="true">
		<cfargument name="cast" default="true">
		<cfargument name="nullColumn" default="#Alias#.#arguments.TextIDColumn#" hint="The column to select when no data exists in the textID column">

		<cfset var result = structNew()>
		<cfset result.tablealias = "p_#arguments.alias#_#TextIDColumn#">
		<cfset result.select = "isnull(#result.tablealias#.phraseText,#nullColumn#)">  <!--- WAB 2014-02-26 Case 438849 if no translation then return the value of the textID column --->
		<cfif cast>
			<cfset result.select  = "convert(nVarchar(4000),#result.select#)">
		</cfif>

		<cfset result.join =  "left join vphrasepointercache as #result.tableAlias#  on #result.tableAlias#.phraseTextID = '#Prefix#_'+#Alias#.#textIDColumn# and #result.tableAlias#.entityTypeID = 0 and #result.tableAlias#.entityid = 0 and #result.tableAlias#.languageid = #languageid# and  #result.tableAlias#.countryid = #CountryID#" >

		<!--- START: WAB 2014-02-26 Case 438849 unhid, changed contents --->
		<cfif checkCache>
			<cfset checkPhrasePointerCache (entityType = "", phraseTextID = "#prefix#[_]%", countryid = arguments.countryid, languageid = arguments.languageid)>
		</cfif>
		<!--- END: WAB 2014-02-26 Case 438849 unhid, changed contents --->

		<cfreturn result>

	</cffunction>

	<!--- NJH 2011/03/07 - P-FNL075 Report Designer populate the phrase pointer cache for a user so that the translations exist when they view a report --->
	<cffunction name="populatePhrasePointerCacheForCurrentUser" access="public">

		<cfset var getPhraseTextsToPopulate = "">
		<cfset var populatePhrasePointerCache = "">

		<cfquery name="getPhraseTextsToPopulate" datasource="#application.siteDataSource#">
			select distinct s.entityName,phraseTextid from PhraseList pl with (noLock)
				inner join schemaTable s on s.entityTypeId = pl.entityTypeID
			where pl.EntityTypeID <> 0
		</cfquery>

		<cfloop query="getPhraseTextsToPopulate">
			<cfquery name="populatePhrasePointerCache" datasource="#application.siteDataSource#">
				updatePhrasePointerCacheforentitytype @entitytype =  <cf_queryparam value="#entityName#" CFSQLTYPE="CF_SQL_VARCHAR" > , @phraseTextid =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > , @countryid = #request.relayCurrentUser.countryID#, @languageid = #request.relayCurrentUser.languageID#
			</cfquery>
		</cfloop>
	</cffunction>

	<cffunction name="initialise">
		<cfargument name="applicationScope" default="#application#">

		<cfset variables.applicationScope = applicationScope>

		<!--- Initialise the memory variables --->
		<cfif not isDefined("applicationScope.phraseStruct") or not isDefined("applicationScope.phrasePointerStruct") or not isDefined("applicationScope.phrasesInJavascript")>
			<cfset resetPhraseStructure (updateCluster = false )>
		</cfif>

	</cffunction>



</cfcomponent>

