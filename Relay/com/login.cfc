<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Mods:  WAB 2004-12-21 altered login so that authentication can always be done by email/password or username/password

	AR 2005-03-18 : Added the applySecurityProfile function.
	WAB 2005-05-04   Mods to createusernameandpassword so that it returns the persons details (for some reason commented out)
		WAB 2005-05-12 function insertusage added left to app , and increased size to 100 in databases
	SSS 2007-01-15 changed get magic number to return org magic number and person magic number.
	SSS 2007-01-15 changed check magic number to check person and org magic numbers.
	GCC/AJC 2007-05-25 CR-LEX525 Portal Access Security -  checks for Dynamic Profile Checks

WAB 2008/02/05 added function applyMultipleSecurityProfiles
	AJC 2008-08-27 CR-SOP501 Sprint 2 - Added hook so that we can extend external login via userfile function/method
	AJC 2008-09-26	CR-TND561 - Req 4 - Inactive User - Switch in external login for allow inactive person
WAB 2008/10/01   altered qResetLoginLock
WAB 2008/11/26 added a function to encode sso links, taken from admin\usermanagement\externalsso.cfm which now uses it
NJH 2008/11/28 CR-TND569 - added ability to SSO into the portal from a 3rd party site
WAB 2009/02/04 recrafted query addPersonToUserGroupsByProfile to try and increase speed, but really to reduce chance of locking
WAB 2009/06/09 removed creation of session.fullpersondetails
WAB	FNL-069	Code to allow all site to move to using using application.encryptionMethod[relayPassword]
WAB  2010/06/14 FNL069 Recoded the two authenticateUser functions (internal and external) to be just one function run of switches set in  settings
NJH	2010/06/30	P-FNL069 - check if user has valid password when sso'ing them in
NYB	2010/10/15	Relayware 8.3 System Test Bug.  LHID 4355 - colours used for PasswordComplexity widget - moved styles to stylesheet
NYB 2010-10-19 	in changeUserPassword, added cfif around setting of reason.message, added message to initialisation of var result
MS  2010-12-17	LID:3943 - added passwordExpiryWarning in StatusOrder list to provide proper pasword expiry message - Line:536
WAB 2011-01-11  LID:3943 - I think that MS solution above will have unintended knock on effects (outlined in svn).
						I have modified the change checkWhyLoginFailed function does not return false in the case of a password expiry warning (but does return a message)
2011-03-07	NYB  	rewrote isPersonInOneOrMoreUserGroups
WAB 2011/10/12  LID 7990 Problems in authenticateUserV2 and checkWhyLoginFailed if username had a leading space. add some trimming
IH	2012/03/29	Case 426913 trim username and password
WAB 2013-09-16 CASE 436944 Case Sensitivity had been lost on password checking
2014-01-09 		NYB 	Case 438275 changed search around username to replace opening quote with closing quote and closing quote with opening quote
2014-07-09 		NYB 	Case 440570 added "order by INVALIDPASSWORD" to result.user query in getWhyLoginFailed function
2015-01-07 		WAB 	CASE 443328 add IPAddress to SSO link
2015/03/09		NJH		Jira Fifteen-21 - pass in passPhrase as argument so that passphrase is not always dependent on the setting, as each client site will have different pass keys and we need a standard one
2015/06/12		NJH		Remove the username/password check for external logins in verifySSOLink as RW is not always the authentication provider and so doesn't have usernames and passwords
						Also get requestTimeUTC from db rather then request variable.. seemed to be converting back to local time when using the request variable... don't know why
2015/10/06		RJT		initUserSession now has extracted the getting of permissions into a function so it can be used elsewhere
2015-10-09      DAN     Case 445999 - fix for user groups rights insert for the current person
2015-11-17		WAB 	Case 445999  - revert DAN fix.  The problem was actually that the flags did not have 'linksToEntityTypeID' set
2015-12-01  	WAB 	PROD2015-26 Visibility. addPersonToUserGroupsByProfile () altered to use new code
2016-10-04		WAB 	When user changes their password kill all other sessions that they may have open
--->

<cfcomponent displayname="login" hint="Relay login functions">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!---
	some initialisation
	 --->
	<cfscript>
		if (not isDefined("application.cachedSecurityVersionNumber")) {
				application.cachedSecurityVersionNumber = 1    ;
		}
	</cfscript>



	<!---
			PKP: 2008/02/05 : process externalSSO process
			WAB 2011/02/03
				Got the timeOffset working for FNL calls (since CR-TN569 it had always been giving 20 minutes).  Now the timeoffset is carried in the FNL Link
				Rewrote the code to give a failure reason
				Altered the flow so that as soon as a failure occurs the result is returned - reduced the number of ifs
	--->
	<cffunction access="public" name="verifySSOLink" hint="this is a wrapper to process a login ExternalSSO" >
		<cfargument name="decryptlink" type="string" required="yes">

		<!--- NJH 2008/11/28 CR-TND569 start --->
		<cfscript>
			var ssoArrayLen = 0;
			var result = {isValidLink = true,reason=""};
			var linkStructure = {};
			var linkInfo = {fromValidSource=false};
			var decryptedLink = '' ;
			var getPersonBypersonID = '' ;
			var getPersonByEmail = '' ;
			var getUsernameAndPassword = '';
		</cfscript>




		<cfif listLen (arguments.decryptlink,"-") GT 1>
			<!--- sourceID will be appended to the link --->
			<cfset linkInfo.source = listLast(arguments.decryptlink,"-")>
			<cfset linkInfo.encryptedPart = replace(arguments.decryptlink,"-#linkInfo.source#","")>
		<cfelse>
			<cfset result.isValidLink = false>
			<cfset result.Reason = "No Source Appended">
			<cfreturn result>
		</cfif>

			<!--- assume the first time we hit this that we've come from an internal domain. --->
			<cfif linkInfo.source eq "FNL">
				<cfset linkInfo.fromValidSource = true>
				<!--- FNL-069 2009-07-14 SSS  --->
				<!--- WAB 2012-03-28 need to use getSetting() now that passkeys are encrypted using master key
					<cfset linkInfo.passphrase = application.EncryptionParams.RWSSO.passkey>
				--->
				<cfset linkInfo.passphrase = application.com.settings.getSetting("SECURITY.ENCRYPTIONMETHODS.RWSSO.PASSKEY")>
				<cfset linkInfo.sourceID = "FNL">
			<cfelse>
				<!--- otherwise verify that request came from a valid IP address --->
				<cf_include template="\code\cftemplates\SSOINI.cfm" checkIfExists="true">

				<cfif isDefined("SSOStruct") and isStruct(SSOStruct) and structKeyExists(SSOStruct,"#linkInfo.source#")>
					<cfif structKeyExists(SSOStruct[linkInfo.source],"passPhrase")>
						<cfset linkInfo.passPhrase = SSOStruct[linkInfo.source].passphrase>
					</cfif>
					<!--- need to pass in timeOffset --->
					<cfif structKeyExists(SSOStruct[linkInfo.source],"timeOffset")>
						<cfset linkInfo.timeOffset = SSOStruct[linkInfo.source].timeOffset>
					</cfif>
					<!--- need to pass in sourceID --->
					<cfif structKeyExists(SSOStruct[linkInfo.source],"sourceID")>
						<cfset linkInfo.sourceID = SSOStruct[linkInfo.source].sourceID>
					</cfif>

					<cfset linkInfo.fromValidSource = true>
				</cfif>
			</cfif>

		<!--- NJH 2008/11/28 CR-TND569 end --->

		<cfif not linkInfo.fromValidSource>
			<cfset result.isValidLink = false>
			<cfset result.Reason = "Invalid Source">
			<cfreturn result>
		</cfif>


			<cftry>
				<cfset decryptedLink = application.com.encryption.DecryptStringAES(StringToDecrypt=replace(URLDecode(linkInfo.encryptedPart),"||","+","ALL"),Passphrase=linkInfo.passphrase)>
				<cfset linkStructure = application.com.regExp.convertNameValuePairStringToStructure (inputString = decryptedLink, delimiter = "&")>
				<cfset linkStructure.isValidLink = true>

				<cfif linkInfo.source is "FNL">
					<cfset linkInfo.timeOffset = linkStructure.lvm>
				</cfif>


				<cfif structKeyExists(linkStructure,"personID")>
					<cfquery name="getPersonBypersonID" datasource="#application.siteDataSource#">
						select personID from person where personID =  <cf_queryparam value="#linkStructure.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>
					<cfif getPersonBypersonID.recordcount EQ 0>
						<cfset linkStructure.isValidLink = false>
						<cfset linkStructure.reason = "PersonID Wrong">
						<cfreturn linkStructure>
					</cfif>

				<!--- get the personID if an email has been passed in rather than a personID --->
				<cfelseif structKeyExists(linkStructure,"email")>
					<cfquery name="getPersonByEmail" datasource="#application.siteDataSource#">
						select personID from person where email =  <cf_queryparam value="#linkStructure.email#" CFSQLTYPE="CF_SQL_VARCHAR" >   <!--- WAB 2011/04/27 was SELECT Person - which obviously wouldn't work.  Code can't have been exercised since has been like that for long time --->
					</cfquery>

					<cfset linkStructure.personID = 0>

					<cfif getPersonByEmail.recordCount eq 1>
						<cfset linkStructure.personID = getPersonByEmail.personID>
					<cfelse>
						<cfset linkStructure.isValidLink = false>
						<cfset linkStructure.reason = "Email Wrong">
						<cfreturn linkStructure>
					</cfif>


				</cfif>

				<!--- dt is the date that the link expires--->
				<!--- Is link valid --->

				<!--- commented out NJH 2008/11/27 CR-TND569
				 <cfif DateDiff("n", linkStructure.dt, Now()) lt 0 >
					<cfset linkStructure.isValidLink = true>
				<cfelse>
					<cfset linkStructure.isValidLink = false>
				</cfif>

				replaced by...
				--->

				<!---
					This is the secret key that notifies us that we have truly come from dev. If we successfully decrypted the string using the
					internal passphrase, we check if this key exists in the decrypted url string. If not, we know that this request did not come from dev.
				 --->
				<!--- <cfif arguments.attemptNumber eq 1>
					<cfif not structKeyExists(linkStructure,"1nt3rna1") or (structKeyExists(linkStructure,"1nt3rna1") and linkStructure["1nt3rna1"] neq "tru3")>
						<cfthrow message="Although the link was successfully descrypted using the default passphrase, the decrypted string does not contain all the necessary url variables.">
					</cfif>
				</cfif> --->

				<!--- CASE 439292
					WAB 2014-04-01
					Problems with timezones and DST
					It appears that whenever ColdFusion parses a datetime string it becomes a local time
					linkStructure.dt is a string representing a UTC time, but gets parsed as a local time
					This is then compared with dateConvert("local2Utc",request.requesttime) which is now a UTC time
					Although when cfoutputted the times appear to be 'the same' (give or take a few seconds), when we come to do a datediff they end up 1 hour apart (assuming UTC and BST)
					My hack work around it to make a string of the requestTime converted to UTC.  Then when we do the datediff, both datetimes are strings representing UTC and it doesn't matter whether or not CF converts them to local time before converting

					NJH 2015/06/10 - get the request time from the db, rather than the variable. For some reason, the requestTimeUTCAsString was not in UTC after applying the dateFormat. It had been reverted back to local time for some reason.
					I can't explain why this fix works, but it seemed to do the trick.
				--->

				<cfset var requestTimeUTC = application.com.dateFunctions.requestTimeUTC()>
				<cfset var requestTimeUTCAsString = dateFormat(requestTimeUTC,"YYYY-MM-DD") & " " & timeFormat(requestTimeUTC,"HH:MM") >

				<cfif abs(DateDiff("n", linkStructure.dt, requestTimeUTCAsString)) lt linkInfo.timeOffset>
					<!--- NJH 2008/11/28 CR-TND569 - check if sourceID matches what we're expecting --->
					<cfif linkStructure.sourceID eq linkInfo.sourceID>
						<!--- check if url is valid --->
						<cfif findNoCase(cgi.HTTP_HOST,linkStructure.surl) IS 0>
							<cfset linkStructure.isValidLink = false>
							<cfset linkStructure.reason= "surl wrong">
							<cfreturn linkStructure>
						</cfif>
					<cfelse>
						<cfset linkStructure.isValidLink = false>
						<cfset linkStructure.reason= "SourceIDs Do Not Match">
						<cfreturn linkStructure>

					</cfif>
				<cfelse>
					<cfset linkStructure.reason= "TimedOut  #abs(DateDiff("n", linkStructure.dt, requestTimeUTCAsString))# lt #linkInfo.timeOffset#">
					<cfset linkStructure.isValidLink = false>
					<cfreturn linkStructure>
				</cfif>

				<!--- WAB 2015-01-07 CASE 443328 add IPAddress check --->
				<cfif structKeyExists (linkStructure,"IP")>
					<cfif linkStructure.IP is not application.com.security.getRemoteAddress()>
						<cfset linkStructure.reason= "Invalid IP">
						<cfset linkStructure.isValidLink = false>
						<cfreturn linkStructure>
					</cfif>
				</cfif>


				<!--- 2010/06/30 NJH P-FNL069 check that the login is valid if everything else has passed up until this point
					2015/06/12	NJH  - removed the username/password check if SSO'ing onto external site as often people are authenticating on other systems and the username/passwords are not set on RW. We are still checking the security profiles though!
				--->
				<cfif linkStructure.isValidLink>
					<cfset var isInternalSite = request.currentsite.isInternal>

					<cfif isInternalSite>
						<cfquery name="getUsernameAndPassword" datasource="#application.siteDataSource#">
							select username, password from person where personID =  <cf_queryparam value="#linkStructure.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfquery>

						<cfset linkStructure.CheckLogin = authenticateUserV2(username=getUsernameAndPassword.username,password=getUsernameAndPassword.password,showComplexityWidget=false,passwordEncrypted = true)>
					</cfif>
					<cfset linkStructure.validUserOnSite = isPersonAValidUserOnTheCurrentSite (linkStructure.personid).isValidUser>

					<cfif isInternalSite and NOT (linkStructure.CheckLogin.isOK or linkStructure.CheckLogin.reason is "passwordExpired")>
						<cfset linkStructure.isValidLink = false>
						<cfset linkStructure.reason = linkStructure.CheckLogin.reason>
					<cfelseif not linkStructure.validUserOnSite>
						<cfset linkStructure.isValidLink = false>
						<cfset linkStructure.reason = "Not Valid User">
					</cfif>

				</cfif>


				<!--- set the currentUser to be the person if it doesn't already exist. --->
				<cfif not structKeyExists(linkStructure,"currentuser")>
					<cfset linkStructure.currentuser = linkStructure.personID>
				</cfif>

				<cfif application.testsite EQ 2>
			        <!--- debug stuff sach(2008-12-08)
				<cfdump var="#linkStructure#">
				<cfoutput>passPhrase #passPhrase#  timeOffset #timeOffset#  sourceID #sourceID#  fromValidSource #fromValidSource#  <br>
				 decryptedLink #decryptedLink#<br>  cgiHTTP_HOST 22#cgi.HTTP_HOST#22  linkStructure.surl 22#linkStructure.surl#22  ><br>
				 linkStructure.isValidLink 22#linkStructure.isValidLink#22  howmanyfound #findNoCase(cgi.HTTP_HOST,linkStructure.surl)#datediff ^^^#DateDiff("n", linkStructure.dt, dateConvert("local2Utc",Now()))#^^^ dateconverted $$$#dateConvert("local2Utc",Now())#$$$</cfoutput>--->
				</cfif>
				<cfcatch type="any">
					<cfset linkStructure.isValidLink = false>
					<cfset linkStructure.reason = cfcatch.message>
				</cfcatch>
			</cftry>

 			<cfreturn linkStructure>

	</cffunction>

	<cffunction access="public" name="createSSOLink" hint="generates an SSO link" >
		<cfargument name="personid" type="numeric" required="no">
		<cfargument name="email" required="no">
		<cfargument name="siteURL" type="string" required="yes">
		<cfargument name="linksValidForMinutes" type="numeric" default = 10>
		<cfargument name="sourceID" type="string" default="FNL"> <!--- NJH 2008/11/28 CR-TND569 --->
		<cfargument name="type" type="string" default="">  <!--- WAB 2011/02/04, can put anything here and use it to determine what to do at far end --->
		<cfargument name="passPhrase" type="string" default="#application.com.settings.getSetting('SECURITY.ENCRYPTIONMETHODS.RWSSO.PASSKEY')#"> <!--- NJH 2015/03/09 Jira Fifteen-21 - pass in passPhrase as argument --->

		<!--- NJH 2008/11/28 CR-TND569 added the dateConvert to convert the time to UTC --->
		<!--- WAB 2011/02/03 NJH changes of 2008 broke the functionality of linksValidForMinutes, which, for 3rd party logins, ended up as a setting in an ini file
				Therefore I have now changed it so that the linksValidForMinutes isn't added to the date but is passed as an extra parameter, dt now becomes the time the link was created
		 --->
		<cfset var personidOrEmailPair = "">
		<cfset var siteURLNoProtocol = rereplaceNoCase(siteURL,"\Ahttp[s]?://","","ONE")>

		<cfif structKeyExists (arguments,"personid")>
			<cfset personidOrEmailPair = "personid=#personid#">
		<cfelseif structKeyExists (arguments,"email")>
			<cfset personidOrEmailPair = "email=#email#">
		<cfelse>
			<cfoutput>Either PersonID or Email must be supplied to this function</cfoutput><CF_ABORT>
		</cfif>

		<!--- WAB 2015-01-07 CASE 443328 add IPAddress to link --->
		<cfreturn URLEncodedFormat(replace(application.com.encryption.EncryptStringAES(StringToEncrypt="currentuser=#request.relayCurrentUser.Personid#&dt=#dateConvert("local2Utc",request.requestTime)#&lvm=#linksValidForMinutes#&#personidOrEmailPair#&surl=#siteURLNoProtocol#&sourceID=#arguments.sourceID#&t=#arguments.type#&IP=#application.com.security.getRemoteAddress()#",Passphrase=arguments.passPhrase),"+","||","ALL")) & "-#sourceid#">


	</cffunction>

	<cffunction access="public" name="createStartingTemplateLinkParameters" hint="creates the URL Params for a starting template link" output="false" >
			<cfargument name="startingTemplate" required="yes">
			<cfargument name="personid" type="numeric" required="no">  <!--- can use either personid or email --->
			<cfargument name="email" required="no">
			<cfargument name="siteURL" default="#request.currentsite.protocolAndDomain#">
			<cfargument name="linksValidForMinutes" type="numeric" default = "4320">

			<cfset var result = "es=" & createSSOLink(personid = personid,siteurl = siteurl,linksValidForMinutes = linksValidForMinutes,type="ST") & "&st=#startingTemplate#" >

			<cfreturn result>

	</cffunction>

	<cffunction access="public" name="createStartingTemplateLink" hint="creates the URL Params for a starting template link" output="false" >
			<cfargument name="startingTemplate" required="yes">
			<cfargument name="personid" type="numeric" required="no"> <!--- can use either personid or email --->
			<cfargument name="email" required="no">
			<cfargument name="siteURL" default="#request.currentsite.protocolAndDomain#">
			<cfargument name="linksValidForMinutes" type="numeric" default = "4320">
			<cfargument name="QueryString" type="string" default = "">
			<!--- perhaps we need to do something about the protocol here! --->
			<cfset var protocol = "">
			<cfset var result = "">
			<cfif refindNoCase("\Ahttp[s]?://",siteURL)  is 0>
				<cfset protocol = "http://">
			</cfif>
			<cfset  result = protocol & siteurl & "/index.cfm?"& createStartingTemplateLinkParameters(argumentCollection = arguments) & "&#QueryString#" >

			<cfreturn result>

	</cffunction>


	<cffunction access="public" name="isPersonAValidUserOnTheCurrentSite" returntype="struct">
		<cfargument name="personid" type="numeric" required="yes">

		<cfreturn  isPersonAValidUserOnGivenSite (personid = personid, siteDefStruct = request.currentsite)>

	</cffunction>


	<!---
	WAB 2008/02/05
	General function to work out whether a person is a valid user of a site
	For external sites checks the security profile (doesn't check existence of username/password)
	For internal sites checks username/password/usergroup using existing function

	Can be passed a siteDefID, a domain or a sitedefstruct (usually would be request.currentsite)


	 --->

	<cffunction access="public" name="isPersonAValidUserOnGivenSite" returntype="struct">
		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="sitedefid" type="numeric">
		<cfargument name="domain" type="string">
		<cfargument name="siteDefStruct" type="struct">

		<cfset var profileResult = '' />


		<cfscript>
			var result = structNew() ;
			if (structKeyExists (arguments,"siteDefID")) {
				arguments.siteDefStruct = application.com.relaycurrentsite.getSiteDefStructureByID (siteDefID);
			} else if (structKeyExists (arguments,"domain")) {
				arguments.siteDefStruct = application.com.relaycurrentsite.getSiteDefStructureByDomain (domain);
			}

			result.isValidUser = false ;

				if (not siteDefStruct.isInternal) {
					// external site
					profileResult = application.com.login.applyMultipleSecurityProfiles (personID = personID,securityProfileIDs = siteDefStruct.securityProfile);

					if (profileResult.continueLogin) {
						result.isValidUser = true ;
					} else {
						result.failETID = profileResult.failETID ;
					}
				} else {
					// internal site
					if (application.com.login.isPersonAValidRelayInternalUser(personid=personid)) {
							result.isValidUser = true ;
					}
				}

		</cfscript>

		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="externalUserLoginProcess" output="true" hint="this is a wrapper to process a login (after a user has been authenticated)" >
		<!--- WAB this brings together all the things that are done to process a login
			except the authentication bit which has to be done before
			bascially copied from et.cfm
		 --->

		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="level" type="numeric" default="2">


		<cfset application.com.relaycurrentuser.setLoggedIn(personid,level)>


		<!--- if the login form had a rememberme checkbox on it then we can set a dejavu cookie accordingly --->
		<cfif isDefined("setRememberMe_Exists")>
			<cfif isDefined ("setRememberMe")>
				<cfset setRememberMeCookie(personid)>
			<cfelse>
				<cfset unSetRememberMeCookie()>
			</cfif>
		</cfif>
		<!--- START: AJC 2008-08-27 CR-SOP501 Sprint 2 - Added hook so that we can extend external login via userfile function/method --->
		<cfif structkeyexists(application.com.login,"extendexternalUserLoginProcess")>
			<cfscript>
				application.com.login.extendexternalUserLoginProcess();
			</cfscript>
		</cfif>
		<!--- END: AJC 2008-08-27 CR-SOP501 Sprint 2 - Added hook so that we can extend external login via userfile function/method --->
	</cffunction>


	<cffunction access="public" name="initialiseAnExternalUser" hint="Does all the things we need to do when we recognise an external user except doesn't log them in">
		<!--- WAB If you need to log them in use externalUserLoginProcess which does the login and calls this function as well  --->
		<!---  WAB 2006-10-11 added support for passwords being hashed in the database--->

		<cfargument name="personid" type="numeric" required="yes">
		<cfset var UserGroupsQuery = "">


			<cfset application.com.RelayCurrentUser.reinitialise(personid)>


	</cffunction>


<!----------------------------------------------------------------------------------------------------------------------------
	manageLoginLocks 	called if request .login LockAmount is defined and set gt 0 and request .login LockTimeout is defined and
						set gt 0.
						returns a query of person details also containing loginStatus, and other loginLock relevant data.
------------------------------------------------------------------------------------------------------------------------------>
<!---
All now built into the authentication function

	<cffunction access="public" name="manageLoginLocks" hint="Check username for validity and adjust locks if necessary" returntype="query">
		<cfargument name="Username" type="string" required="yes">
		<cfargument name="Password" type="string" required="yes">
		<cfargument name="qLoginDetail" type="query" required="yes">

		<cfset var checkUsername = "">

		<!--- if username and password were correct, then query recordcount will be gt 0 --->
		<cfif arguments.qLoginDetail.recordcount gt 0>
			<!--- if loginStatus is validated and not locked then reset the loginAttempt value. origional query will be returned by default. --->
			<cfif arguments.qLoginDetail.loginStatus eq 200>
				<cfscript>
					resetLoginLock(personID=qLoginDetail.personID);
				</cfscript>
			</cfif>
		<cfelse>
		<!--- see if username at least is correct and manage login attempts. if username doesn't exist, then origional empty query will be returned by default. --->
			<cfset checkUsername = getWhyLoginFailed(Username=arguments.Username, password = arguments.Password).user>
			<!--- if username exists --->
			<cfif checkUsername.recordcount gt 0>
				<!--- check attempts and increment if necessary. --->
				<cfif checkUsername.reason = ''>
					<cfset incrLoginLock(personID=checkUsername.personID,loginAttempt=checkUsername.loginAttempt)>
					<!--- if incremented, then return an updated query of person details and login lock status with details --->
					<cfreturn getWhyLoginFailed(Username=arguments.Username, password = arguments.Password)>
				<cfelseif checkUsername.loginAttempt eq request .login LockAmount and checkUsername.loginStatus eq 401>
					<cfset resetLoginLock(personID=checkUsername.personID,loginAttempt=1)>
					<cfreturn getWhyLoginFailed(Username=arguments.Username, password = arguments.Password)>
				<cfelseif checkUsername.loginAttempt eq request .login LockAmount and checkUsername.loginStatus eq 423>
					<cfreturn checkUsername>
				</cfif>
			</cfif>
		</cfif>
		<!--- original query is returned by default --->
		<cfreturn arguments.qLoginDetail>
	</cffunction>

--->

<!---
WAB 2010/06
Created this function out of two old functions which did internal and external users separately
These old functions now call this function and use parts of it.  In future old functions can be removed from code
Note:  No longer Suppports:
	1. Plain Passwords of any sort
	2.  Any encryption relayVar other than relaypassword
	3.	Logging in using email and dateOfBirth

But Does
	Automatically deal with login locking
	Returns Information on why authentication failed

Checks a username & password combination and applies all the rules (such as password expiry, different rules for internal/external).
Returns a structure
	result.isok   true/false
	result.userquery (1 row when succeeded, 0 rows when failed)
	 If authentication failed then returns
	 	result.reason
	 	result.failedUserQuery
	 	See function getWhyLoginFailed() for more info

Note that
	i) this function does not actually carry out the logging in
	ii) a portal user can still fail if they do not have access privileges to the particular portal
 --->

	<cffunction access="public" name="authenticateUserV2" hint="Check a user's username and password and authenticate" returntype="struct" output="false">

		<cfargument name="username" type="string" required="yes">
		<cfargument name="password" type="string" required="yes">
		<cfargument name="isInternal" type="boolean" default="#request.currentSite.isInternal#">
		<cfargument name="showComplexityWidget" type="boolean" default="true"> <!--- NJH 2010/06/30 - don't show if using SSO --->
		<cfargument name="passwordEncrypted" type="boolean" default="false" hint="is the password already encrypted"> <!--- NJH 2010/07/01 P-FNL069, use this when doing --->

		<cfset var checkUsername = "">
		<cfset var item = "">
		<cfset var loginExpressions = getLoginExpressions (Username=arguments.username,Password=arguments.Password,isInternal = isInternal,passwordEncrypted=arguments.passwordEncrypted)>
		<cfset var result = {message="",personid=0}>
		<cfset var temp = '' />
		<cfset var failureInfo = '' />

		<CFQUERY NAME="checkUsername" DATASOURCE="#application.SiteDataSource#">
		SELECT	Person.PersonID,
				Person.FirstName,
				Person.LastName,
				Person.Username,
				Person.Language,
				<cfloop item="item" collection = "#loginExpressions.selects#">
					<cfset temp = loginExpressions.selects[item]>
					#preserveSingleQuotes(temp)# as #item#,
				</cfloop>
				Person.FirstTimeUser

		FROM Person
			<cfif arguments.isInternal>
			<!--- Internal Users Have own usergroup --->
			inner join UserGroup ON Person.PersonID = UserGroup.PersonID
			</cfif>
		WHERE
			(
				<!--- WAB LID 7990 2011/10/12 Problem if username starts with a blank  added ltrim/rtrim
						which can occur when person does not have a firstname
						probably only causes problems when we do resend password (where we query the username and then pass it into this function), at other times I expect that people use their emailaddress
				--->
				<!--- START: 2014-01-09 NYB Case 438275 added replace of opening quote with closing quote --->
				(
				ltrim(rtrim(Person.Username)) =  <cf_queryparam value="#replace(Trim(arguments.username),chr(96),chr(39),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >  and Person.Username <> ''
				or
				ltrim(rtrim(Person.Username)) =  <cf_queryparam value="#replace(Trim(arguments.username),chr(39),chr(96),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >  and Person.Username <> ''
				)
				or
				(
				Person.Email =  <cf_queryparam value="#replace(Trim(arguments.username),chr(96),chr(39),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >  and Person.Email <> ''
				or
				Person.Email =  <cf_queryparam value="#replace(Trim(arguments.username),chr(39),chr(96),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >  and Person.Email <> ''
				)
				<!--- END: 2014-01-09 NYB Case 438275 --->
			)


			<cfloop item="item" collection = "#loginExpressions.conditions#">
				<cfset temp = loginExpressions.conditions[item]>
				AND #preserveSingleQuotes(temp)#
			</cfloop>
<!--- 		 	<cfif loginSettings.allowinactiveUser is false> <!--- was application. settings.login. AllowInactiveUser --->
			and active = 1
			</cfif>

		  AND Person.Password <> ''
		  AND password = '#encryptedPassword#'
		  AND Person.LoginExpires >= getDate()
		  AND (isnull(loginAttempt,0) < #loginSettings.loginlockamount# or dateDiff(n,loginLastLock,getdate()) >= #loginSettings.loginlocktimeout_minutes#  )
		  AND datediff (d,passworddate,getdate()) < #passwordSettings.expiresin_days#
 --->
		</CFQUERY>

		<cfset result.userQuery = checkUserName>

		<!--- first check is if we have a password, but it doesn't meet the password complexity rules.
				Should only be here  when migrating customers
				can only be done if password has been passed in unencrypted
				--->
		<cfif checkUserName.recordcount is not 0 and not passwordEncrypted and not checkPasswordComplexity(password=arguments.password,showComplexityWidget=arguments.showComplexityWidget).isoK>
			<cfset result.isOK = false>
			<cfset result.reason = "passwordExpired">
			<cfset result.message = "phr_login_passwordExpired">
			<cfset result.personID = checkUserName.personID>
		<cfelseif checkUserName.recordcount is not 0 >
			<cfset result.isOK = true>
			<cfset resetLoginLock(personID=checkUserName.personID)>
			<cfset result.personID = checkUserName.personID>
		<cfelse>
			<cfset result.isOK = false>
			<cfset failureInfo = getWhyLoginFailed(Username=arguments.Username, password = Password, isInternal = isInternal, passwordEncrypted = passwordEncrypted)>
			<cfif failureInfo.user.recordcount gt 0 >
				<cfif failureInfo.reason is 'invalidPassword'>
					<cfset incrLoginLock(personID=failureInfo.user.personID,isInternal=isInternal)>
					<!--- WAB we now need to adjust the getWhyLoginFailed query to take into account the fact that login attempt has been incremented
						slightly odd this, I could have put it in the getWhyLoginFailed function, but then it would return the wrong answer when it is used to to find the status of someone's account
					--->
					<cfset querysetCell(failureInfo.user,"loginAttempt",failureInfo.user.loginAttempt + 1,1)>
					<cfset querysetCell(failureInfo.user,"loginAttemptsLeft",failureInfo.user.loginAttemptsLeft - 1,1)>
				</cfif>
				<cfset result.personID = failureInfo.user.personID>
			</cfif>
			<cfset result.failedUserQuery = failureInfo.user>
			<cfset result.reason = failureInfo.reason>
			<cfset result.message = failureInfo.message>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="encryptRelayPassword">
		<cfargument name="username" type="string" required="yes">
		<cfargument name="password" type="string" required="yes">

			<cfreturn application.com.encryption.encryptString(encryptType="relaypassword",partnerUsername=arguments.username,partnerPassword=arguments.Password)>

	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------
	getWhyLoginFailed  (was getUsernameRecord)

	used to be:
			gets person details and login lock status based only on a username.
						this is used solely for managing login locking.
	extended by WAB to be a more general function for dealing with failed logins
	Slightly badly named because  ...
	Can be used outside the context of logging in to decide whether a person will be able to log in successfully
	ie a programmer can use it without passing in the password and it will tell him whether the user's login has expired etc.'


	TBD - meed to update the code bringing back the status field

	Assumes that the username is correct!

------------------------------------------------------------------------------------------------------------------------------>
	<cffunction access="public" name="getWhyLoginFailed" hint="gets a record for user based only on username... this assumes the password provided was incorrect and assigns a status accordingly.">

		<cfargument name="Username" type="string" required="yes">
		<cfargument name="Password" type="string" default="">
		<cfargument name="isInternal" type="boolean" default="#request.currentSite.isInternal#">
		<cfargument name="passwordEncrypted" type="boolean" default="false" hint="is the password already encrypted"> <!--- NJH 2010/07/01 P-FNL069 --->

		<cfset var result = {reason="unknown",message="",isOk=true}>
		<cfset var item = "">

		<cfset var loginExpressions = getLoginExpressions (Username=arguments.username,Password=arguments.Password,isInternal = isInternal,passwordEncrypted=arguments.passwordEncrypted)>
		<!--- 2011/01/12 WAB REMOVED(17-Dec-2010		MS		LID:3943 - added passwordExpiryWarning at the end in StatusOrder list to provide proper pasword expiry message) --->
		<cfset var StatusOrder = "LoginLock,invalidPassword,loginExpired,inactiveuser,passwordExpired"> <!--- This list determines the order in which the failure reasons are dealt with, first failure it comes across is given as the reason --->
		<cfset var temp = '' />
		<cfset var status = '' />

		<!--- Note 401 = Unauthorised, 423 means locked, 200 OK --->

		<CFQUERY NAME="result.user" datasource="#application.siteDataSource#">
			SELECT
				PersonID, username, firstName, lastName,
				<!---
				This was the original code for dealerNet
				case when (loginLastLock is null or loginAttempt is null or (DATEADD(Mi,-30,getDate()) > loginLastLock and loginAttempt <= 5)) then 401 else 423 end as loginStatus,
				we have replaced it with the code below
				--->
				case
					when
						<!--- This chains all the conditions together and will return 200 when the login is OK --->
						1=1
						<cfloop item="item" collection = "#loginExpressions.conditions#">
							<cfset temp = loginExpressions.conditions[item]>
							AND (#preserveSingleQuotes(temp)#)
						</cfloop>
					THEN
						200
					when
						<!--- if the login is bad because of a lock, returns 423 'Locked'--->
						NOT (#loginExpressions.conditions.LoginLock#) then 423
					else
						<!--- All other login failures get 401 'Unauthorized' --->
						401
					end
				as loginStatus,

				<cfloop item="item" collection = "#loginExpressions.conditions#">
					<cfset temp = loginExpressions.conditions[item]>
					case when #preserveSingleQuotes(temp)# then 0 else 1 end as #item#,
				</cfloop>
				<cfloop item="item" collection = "#loginExpressions.selects#">
					<cfset temp = loginExpressions.selects[item]>
					#preserveSingleQuotes(temp)# as #item#,
				</cfloop>
				isnull(loginAttempt,0) as loginAttempt,  <!--- note that this does not include the current attempt, but that when used during the login process I add 1 to this value if the login fails due to bad password--->
				loginLastLock


			from
				Person
			where
			(
				<!--- WAB LID 7990 2011/10/12 Problem if username starts with a blank  added ltrim/rtrim
						which can occur when person does not have a firstname
						probably only causes problems when we do resend password (where we query the username and then pass it into this function), at other times I expect that people use their emailaddress
				--->
				(ltrim(rtrim(Person.Username)) =  <cf_queryparam value="#Trim(arguments.username)#" CFSQLTYPE="CF_SQL_VARCHAR" >  and Person.Username <> '')
				or
				(Person.Email =  <cf_queryparam value="#Trim(arguments.username)#" CFSQLTYPE="CF_SQL_VARCHAR" >  and Person.Email <> '')
			)
			order by INVALIDPASSWORD <!--- 2014-07-09 NYB Case 440570 added --->
		</CFQUERY>

		<cfif result.user.recordcount is 0>
				<!--- Note that we may not want to distinguish between invalidUsername/Password to the user - don't want to give clues as to whether username is correct --->
				<cfset result.reason = "invalidUserName">
				<cfset result.isOK = false>
		<cfelse>
			<!--- See comment on Status list above --->
			<cfloop list="#statusOrder#" index="status">
				<cfif result.user[status][1]>
					<cfset result.reason = status>
					<cfset result.isOK = false>
					<cfbreak>
				</cfif>
			</cfloop>
			 <cfif result.isOK>
			 	<!--- If login is still OK then we will test for potential password expiry, however we will NOT set isOK to false ---->
			 	<cfif result.user.passwordExpiryWarning>
					<cfset result.reason = "passwordExpiryWarning">
				 </cfif>
			 </cfif>


		</cfif>

		<cfset result.message="phr_login_#result.reason#">

		<cfreturn result>

	</cffunction>


	<!---
		A single place where the loginTest SQL Expressions are generated
		Makes sure that all bits of code use exactly the same conditions

		It returns two structures:
			1. Conditions structure
				Each item in this structure is a SQL snippet
				Each snippet must be true for the login to be valid
				Therefore by chaining all the snippets together with ANDs you get the where clause to test a login
				Alternatively use the snippet in a CASE WHEN statement to determine whether the login has passed or failed that particular condition

			2.  Selects Structure
				Each item in this structure is a SQL snippet
				These are similar to the condition snippets, but don't necessarily have to be true for a login to be OK
				For example the test for a Password ABOUT to expire might 'fail', but this doesn't invalidate the login


		WAB 2013-09-16 CASE 436944 Case Sensitivity had been lost on password checking

	 --->
	<cffunction name="getLoginExpressions">

		<cfargument name="Username" type="string" required="yes">
		<cfargument name="Password" type="string" default="">
		<cfargument name="isInternal" type="boolean" default="#request.currentsite.isInternal#">
		<cfargument name="passwordEncrypted" type="boolean" default="false" hint="is the password already encrypted"> <!--- NJH 2010/07/01 P-FNL069 --->

		<cfset var internalExternal = iif (isInternal,de("internalsites"),de("externalsites"))>
		<cfset var passwordSettings = application.com.settings.getSetting(variablename = "security.#internalExternal#.passwords",personid = 0)>
		<cfset var loginSettings = application.com.settings.getSetting(variablename = "security.#internalExternal#.login",personid = 0)>
		<cfset var result	= {conditions= structNew(),selects=structNew()}>
		<cfset var encryptedPassword = "">
		<cfset var thisUserName = "">
		<cfset var userNameList = "">
		<cfset var getUserNames = "">

		<!---
			WAB 2012-02-08 (for Sony but generally applicable)
			Had to deal with issue of user logging in using their email address, but the password being salted with the username
			So, if username has a @ in it we look up the usernames which correspond to that email address
			We may end up with more that one record with the given email address, therefore we have to deal with a list of usernames
		 --->
		<cfif refind(".*@.*\..*",username)> <!--- only need a rough test for email address, doesn't matter if we occasionally run this query against an actual username --->
			<CFQUERY NAME="getUserNames" datasource="#application.siteDataSource#">
				<!--- 2014-01-09 NYB Case 438275 added replace of opening quote with closing quote --->
				select username, email from person where (email = <cf_queryparam value="#replace(trim(username),chr(96),chr(39),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" > or email = <cf_queryparam value="#replace(trim(username),chr(39),chr(96),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >)  or username =  <cf_queryparam value="#trim(username)#" CFSQLTYPE="CF_SQL_VARCHAR" >   <!--- it is conceivable that the username has an @ in it so we have to search username as well --->
			</CFQUERY>
			<cfset usernameList = valueList(getUsernames.username,application.delim1)> <!--- use a delimiter which we hope won't appear in a username! --->
		<cfelse>
			<cfset usernameList = arguments.username>
		</cfif>

		<!--- build up the password test SQL.  Done in a loop to handle duplicated email addresses, but generally will only go around loop once --->
		<cfset result.conditions.invalidPassword = "( isnull(password,'') <> '' AND (1=0 ">
		<cfloop index="thisuserName" list="#userNameList#" delimiters="#application.delim1#">

			<!--- NJH 2010/07/01 P-FNL069 - if the password is already encrypted, then don't re-encrypt it --->
			<cfif arguments.passwordEncrypted>
				<cfset encryptedPassword = arguments.Password>
			<cfelse>
				<cfset encryptedPassword = encryptRelayPassword(Username=thisUserName,Password=arguments.Password)>
			</cfif>
			<cfset result.conditions.invalidPassword = result.conditions.invalidPassword & " OR password = N'#trim(encryptedPassword)#' COLLATE SQL_Latin1_General_Cp1_CS_AS "> <!--- WAB 2013-09-16 CASE 436944 Case Sensitivity had been lost on passwords - only caused issues on systems that do not use password encryption (such as lenovo)--->

		</cfloop>
		<cfset result.conditions.invalidPassword = result.conditions.invalidPassword & ") )">

		<!--- apparently external logins never expire, could be a setting but for now hard coded--->
		<cfif isInternal>
			<cfset result.conditions.LoginExpired = "Person.LoginExpires >= getdate()">
			<cfset result.selects.notInternalUser	="case when exists (select 1 from usergroup where usergroup.personid = person.personid) then 0 else 1 end">
		<cfelse>
			<cfset result.conditions.LoginExpired = "1 = 1">
		</cfif>

		<cfset result.selects.LoginLockDuration ="(#loginSettings.loginlocktimeout_minutes# - dateDiff(n,loginLastLock,getdate()) ) ">
		<cfset result.conditions.LoginLock = "(isnull(loginAttempt,0) = 0 or isnull(loginAttempt,0) % #loginSettings.loginlockamount# <> 0 or #result.selects.LoginLockDuration# <= 0 or loginlastlock is null)">
		<cfset result.selects.LoginAttemptsLeft =  "#loginSettings.loginlockamount# - ((isnull(loginAttempt,0)-1) % #loginSettings.loginlockamount#) -1">

		<cfset result.selects.passwordExpiresIndays	="#passwordSettings.expiresin_days# - (datediff (d,passworddate,getdate())) ">
		<cfset result.conditions.passwordExpired = "#result.selects.passwordExpiresIndays# > 0">
		<cfif not isInternal><!--- There is only an allowInactiveUser setting on external sites --->
		 	<cfif loginSettings.allowinactiveUser is false> <!--- was application. settings.login. AllowInactiveUser --->
				<cfset result.conditions.InactiveUser = "active = 1">
			<cfelse>
				<cfset result.conditions.InactiveUser = "1 = 1">
			</cfif>
		<cfelse>

				<cfset result.conditions.InactiveUser = "1 = 1">
		</cfif>
		<cfset result.selects.passwordExpiryWarning	="case when #result.selects.passwordExpiresInDays# <= #passwordSettings.warnbeforeexpires_days# then 1 else 0 end">




		<cfreturn result>

	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------
	resetLoginLock 	will set loginAttempts back to 0 for a personID.
					this happens when time period between login attempts exceeds request .login LockTimeout amount,
					or if a user logs in successfully before exceeding the request .login LockAmount value.
------------------------------------------------------------------------------------------------------------------------------>
	<cffunction access="public" name="resetLoginLock" hint="reset login locks when user logs in (attempts set back to 0 )">

		<cfargument name="personID" type="any" required="yes">

		<cfset var qResetLoginLock = '' />

		<CFQUERY NAME="qResetLoginLock" datasource="#application.siteDataSource#">
			update	person
			set		loginAttempt = null, loginLastLock = null
			where	personID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- WAB 2008/10/01 this query was creating locks so added in the <> #arguments.loginAttempt# --->
			and loginAttempt <> 0
		</CFQUERY>

	</cffunction>

	<cffunction access="public" name="clearLoginLock" hint="clears the lock but does not set attempts back to 0 - gives user another go">

		<cfargument name="personID" type="any" required="yes">

		<cfset var qResetLoginLock = '' />

		<CFQUERY NAME="qResetLoginLock" datasource="#application.siteDataSource#">
			update	person
			set		loginLastLock = null
			where	personID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------
	incrLoginLock 	increments the value loginAttempt on the person table for a personID.
					will check the current value and will set the loginLastLock value if the loginAttempt is equal to
					the request .login LockAmount value.

					WAB Made some modifications.
					By using a Mod operator we don't have to reset the loginAttempts counter until the password is entered correctly, so keep track of the total number of failed attempts.
					Could therefore implement a complete lockout at some point

------------------------------------------------------------------------------------------------------------------------------>
	<cffunction access="public" name="incrLoginLock" hint="reset login locks">
		<cfargument name="personID" type="any" required="yes">
		<cfargument name="isInternal" type="any" required="yes">

			<cfset var internalExternal = iif (isInternal,de("internalsites"),de("externalsites"))>
			<cfset var loginSettings = application.com.settings.getsetting("security.#internalExternal#.login")>
			<cfset var qincrLoginLock = "">

			<CFQUERY  NAME="qincrLoginLock" datasource="#application.siteDataSource#">
				update	person
				set		loginAttempt = isnull(loginAttempt,0)+1
				where	personID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
						-- and isnull(loginAttempt,0) < #loginSettings.loginlockamount#



				if exists (select 1 from person where personid =  <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >  and  isnull(loginAttempt,0) > 0 AND isnull(loginAttempt,0) % <cf_queryparam value="#loginSettings.loginlockamount#" cfsqltype="cf_sql_integer"> = 0)
				BEGIN
					update	person
					set		loginLastLock = getDate()
					where	personID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				END
			</CFQUERY>


	</cffunction>



	<cffunction access="public" name="authenticateUser" hint="Check external user's username and password and authenticate" returntype="query">
		<!---
			WAB	2004-12-21	altered so that authentication can always be done by email/password or username/password
			NM	2007-07-27	added case for lockStatus. also added calls to new login locking methods.
			WAB 2010/06 altered to use V2, note nol onger supports emailDoB
		--->
		<cfargument name="partnerUsername" type="string" required="yes">
		<cfargument name="partnerPassword" type="string" required="yes">
		<cfargument name="authenticationMethod" type="string" default="usernamePassword">

		<cfif authenticationMethod is not "usernamePassword">
			<cfoutput>com.login.authenticateUser: #authenticationMethod# is not supported</cfoutput>
		</cfif>

		<!--- note, for backwards compatibilty we only return the userquery part of the result of authenticateUserV2
			NJH LID 5004 2011/01/05 - set isInternal to false --->
		<cfreturn authenticateUserV2 (username = partnerUsername,password = partnerpassword, isInternal = false).userquery>



	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="authenticateInternalUser" hint="Check internal user's username and password and authenticate" returntype="query">
		<!---
		WAB 2004-12-21 altered so that authentication can always be done by email/password or username/password
		WAB 2006-10-11 added support for passwords being hashed in the database
		WAB 2010/06 Altered to use V2
		--->

		<cfargument name="username" type="string" required="yes">
		<cfargument name="password" type="string" required="yes">

		<cfset var result = authenticateUserV2 (username = Username,password = password, isInternal = true)>

		<cfreturn result.userquery>

	</cffunction>

<!--- WAB 2010/06 P-FNL069
created this function from code in relaytagtemplates\changepassword.cfm
--->
	<cffunction name="changeUserPassword">
		<cfargument name="userName" type="string" required="yes">
		<cfargument name="currentPassword" type="string" required="yes">
		<cfargument name="newPassword1" type="string" required="yes">
		<cfargument name="newPassword2" type="string" required="yes">
		<cfargument name="passwordEncrypted" type="boolean" default="false" hint="is the password already encrypted">

		<cfset var result = {isoK = false,reason="",message=""}>
		<cfset var passwordComplexity = '' />
		<cfset var encryptedPassword = '' />
		<cfset var passwordNotUsedBefore = '' />
		<cfset var changePass = '' />

		<!--- check old password
			NJH 2010/08/24 - pass in the passwordEncrypted argument, as otherwise we end up double encrypting the original password and get a login failure.. this is happening on the internal resendPW
		--->
		<cfset var checkPassword = getWhyLoginFailed (password = arguments.currentPassword,username = arguments.username, passwordEncrypted=arguments.passwordEncrypted)>

		<cfif checkPassword.user.invalidPassword is 0 or userName is "">

			<cfif newPassword1 is newPassword2 >
				<cfset passwordComplexity =  checkPasswordComplexity (newPassword1)>

				<cfif passwordComplexity.isok>

					<cfset encryptedPassword  = encryptRelayPassword(username = username, password = newPassword1)>
					<cfset passwordNotUsedBefore = checkPasswordNotUsedBefore (encryptedPassword = encryptedPassword,personid = checkPassword.user.personid)>

					<cfif passwordNotUsedBefore>
						<CFQUERY NAME="changePass"  datasource="#application.sitedatasource#">
							UPDATE person
							SET Password =  <cf_queryparam value="#encryptedPassword #" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							Passworddate = getDate(),
							FirstTimeUser = 0,
							lastupdatedby = #request.relayCurrentUser.usergroupID# ,    <!--- WAB 2007-10-08 , so that appears correct on mods report added lastupdatedby, lastupdated --->
							lastupdated = getDate(),
							lastUpdatedByPerson = #request.relayCurrentUser.personID#
							WHERE personid =  <cf_queryparam value="#checkPassword.user.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
							<!---AND Password = '#FORM.oldPassword#'--->
						</CFQUERY>

						<cfif request.relayCurrentUser.personid is checkPassword.user.personid>
							<cfset application.com.relayCurrentUser.updatePersonalData()>
						</cfif>

						<!--- 2016-10-04		WAB 	When user changes their password kill all other sessions that they may have open --->
						<cfset application.com.sessionManagement.killSessionsByPersonID (personid = checkPassword.user.personid, excludeSessionIDs = session.sessionid)>

						<cfset result.isOK = true>
					<cfelse>
						<cfset result.reason = "passwordUsedBefore">

					</cfif>

				<cfelse>
					<cfset result.reason = "passwordsNotComplexEnough">
				</cfif>
			<cfelse>
				<cfset result.reason = "passwordsDoNotMatch">
			</cfif>
		<cfelse>
			<cfset result.reason = "currentPasswordInvalid">
		</cfif>

		<cfif len(result.reason) gt 0>
			<cfset result.message="phr_login_#result.reason#">
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="autoAuthenticateUser" hint="Check external user's username and password and authenticate" returntype="query">

		<cfargument name="listFirstP" type="numeric" required="yes">
		<cfargument name="listLastP" type="numeric" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="lastname" type="string" default="">

		<cfscript>
			var CheckLogin = "";
		</cfscript>

		<CFQUERY NAME="CheckLogin" datasource="#arguments.datasource#">
			Select
				 PersonID, Firstname, Lastname, email, username, password, sitename
			From
				Person
					inner join
				location on person.locationid = location.locationid

			Where
				personid = #arguments.listFirstP#
				and (personid & 1023) * (person.locationid & 1023)   = #arguments.listLastP#
				<cfif len(arguments.Lastname)>
					and person.Lastname =  <cf_queryparam value="#arguments.lastname#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

			UNION

			Select
				 PersonID, Firstname, Lastname, email, username, password, sitename
			From
				Person
					inner join
				location on person.locationid = location.locationid
					inner join
				dedupeAudit on person.locationid = newid and recordType = 'location'
			Where
				personid = #arguments.listFirstP#
				and (personid & 1023) * (archivedid & 1023) = #arguments.listLastP#
				<cfif len(arguments.Lastname)>
					and person.Lastname =  <cf_queryparam value="#arguments.lastname#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

			</CFQUERY>

		<cfreturn CheckLogin>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="autoAuthenticateInternalUser" hint="Check internal user's username and password and authenticate" returntype="query">

		<cfargument name="listFirstP" type="numeric" required="yes">
		<cfargument name="listLastP" type="numeric" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var CheckLogin = "";
		</cfscript>

		<CFQUERY NAME="CheckLogin" datasource="#arguments.datasource#">
			Select
				 person.PersonID, Person.Language,Person.FirstTimeUser, Firstname, Lastname, email, sitename, UserGroup.UserGroupID
			From
				Person
					inner join
				location on person.locationid = location.locationid
				inner join UserGroup ON Person.PersonID = UserGroup.PersonID
			Where
				person.personid = #arguments.listFirstP#
				and (person.personid & 1023) * (person.locationid & 1023)   = #arguments.listLastP#
				and Person.Active <> 0
				 AND Person.LoginExpires >= #CreateODBCDateTime(Now())#

				union

			Select
				 person.PersonID, Person.Language, Person.FirstTimeUser, Firstname, Lastname, email, sitename , UserGroup.UserGroupID
			From
				Person
					inner join
				location on person.locationid = location.locationid
					inner join
				dedupeAudit on person.locationid = newid and recordType = 'location'
				inner join UserGroup ON Person.PersonID = UserGroup.PersonID
			Where
				person.personid = #arguments.listFirstP#
				and (person.personid & 1023) * (archivedID & 1023) = #arguments.listLastP#
				and Person.Active <> 0
				 AND Person.LoginExpires >= #CreateODBCDateTime(Now())#
			</CFQUERY>

		<cfreturn CheckLogin>

	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUserGroups" hint="Get user groups" returntype="query">

		<cfargument name="userID" type="numeric" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var getUserGroups = "";
		</cfscript>

		<CFQUERY NAME="getUserGroups" datasource="#arguments.datasource#">
			SELECT UserGroupID from RightsGroup where PersonID = #arguments.userID#
		</CFQUERY>

		<cfreturn getUserGroups>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             createUserNamePassword
	  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createUserNamePassword" hint="Create user name and passowrd">

		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="resolveDuplicates" type="boolean" default="true">
		<cfargument name="OverWritePassword" type="boolean" default="false">
		<cfargument name="OverWriteUserName" type="boolean" default="false">

		<cfset VAR thisUser = "">

		<cf_createUserNameAndPassword
			personid = "#personID#"
			resolveDuplicates = "#resolveDuplicates#"
			OverWritePassword = "#OverWritePassword#"
			OverWriteUserName = "#OverWriteUserName#"
			>



		<cfscript>
			thisUser = application.com.commonQueries.getFullPersonDetails(personID);
		</cfscript>

		<cfreturn thisUser>

	</cffunction>

<!---
WAB P-FNL069
checks complexity of password against rules specified in the security settings
Uses a set of regular expressions which I don't understand, but they work!
Returns a structure
	result.isOK pass/or fail
	result.items is a structure with an key for each rule which has been tested and a true/false depending upon whether passed
	result.minLength  (from the settings)
	also returns a more detailed/formatted information string (see comments below for details)
--->
	<cffunction access="public" name="checkPasswordComplexity" hint="Checks Complexity">
		<cfargument name="password" type="string" required="yes">
		<cfargument name="isInternal" type="string" default="#request.currentsite.isInternal#">
		<cfargument name="showComplexityWidget" type="boolean" default="true"> <!--- NJH 2010/06/30 - don't show widget for SSO --->

		<cfset var internalExternal = iif (isInternal,de("internalsites"),de("externalsites"))>
		<cfset var passwordSettings = application.com.settings.getSetting("security.#internalExternal#.passwords") >
		<cfset var result = {isOK=true,Items=structNew(),string=password,Reason="",fullDescription="",minLength = passwordSettings.minLength}>
		<cfset var itemToTest = "">
		<cfset var testResult = "">
		<cfset var regExp = {alphaNumeric = "^(?=.*\d)(?=.*[a-zA-Z]).*$",mixCase = "^(?=.*[a-z])(?=.*[A-Z]).*$",punctuation = "[^0-9a-zA-Z]",length= ".{#passwordSettings.minLength#,}",none = "."}>


		<cfloop index="itemToTest" list = "#passwordSettings.Complexity#,length">
			<cfset testResult = refind(regExp[itemToTest],password)>
			<cfif not testResult>
				<cfset result.isOK = false>
				<cfset result.items[itemToTest] = false>
				<cfset result.Reason = listappend (result.Reason,itemToTest)>
			<cfelse>
				<cfset result.items[itemToTest] = true>
			</cfif>
		</cfloop>

		<!--- <cfdump var="#result#"> --->
		<cfif arguments.showComplexityWidget>
			<cfset result = DefaultPasswordComplexityWidget (result)>
		</cfif>
	<!--- 	<cfdump var="#result#">	 --->
		<cfreturn result>
	</cffunction>


	<cffunction access="public" name="DefaultPasswordComplexityWidget" hint="takes result of checkpasswordcomplexity function and creates something readable">
		<cfargument name="result" type="struct" required="yes" hint="result checkPasswordComplexity">

		<!---
			This section of the code builds up something which can be displayed to the user showing why a password has failed, what is required etc.
			This is currently very rough
			has been taken out into a separate function to allow different widgets to be plumbed in.
			Or alternatively create a default here.  A programmer can always take the undelying result of this function and use it in their own way

			suggest using phrases
		--->

		<cfset var numCharsNeeded = 0>
		<cfset var mergeStruct = structNew()>
		<cfset var message = "">
		<cfset var className = '' />
		<cfset var item = '' />
		<cfset var desc = '' />

		<cfset arguments.result.fullDescription = "">
		<!--- <cfif len(result.string) gt 0> --->

	 		<cfloop collection = "#result.items#" item="item">
		 		<cfif item neq "none">
		 			<cfset className = "ValidEntry">
					<cfset message = "">
					<cfif not result.items[item]>
						<!--- If not OK --->
		 				<cfset className = "InvalidEntry">
						<cfif item eq "length">
							<cfset numCharsNeeded = result.minLength-len(result.string)>
							<cfif numCharsNeeded gt 1>
								<cfset mergeStruct.numCharsNeeded = numCharsNeeded>
								<cfset message = "phr_login_needNumMoreCharacters">
							<cfelse>
								<cfset message = "phr_login_oneMoreCharacter">
							</cfif>
						<cfelseif item eq "alphanumeric">
							<cfif not reFind("[0-9]",result.string) and reFindNoCase("[a-z]",result.string)>
								<cfset message = "phr_login_oneMoreNumber">
							<cfelseif reFind("[0-9]",result.string) and not reFindNoCase("[a-z]",result.string)>
								<cfset message = "phr_login_oneMoreAlphaCharacter">
							<cfelse>
								<cfset message = "phr_login_oneMoreNumberAndAlphaCharacter">
							</cfif>
						<cfelseif item eq "punctuation">
							<cfset message = "phr_login_oneMorePunctuationCharacter">
						<cfelseif item eq "mixCase">
							<cfif not compare(ucase(result.string),result.string) and not compare(lcase(result.string),result.string)>
								<cfset message = "phr_login_mixedCaseCharacters">
							<cfelseif not compare(ucase(result.string),result.string)>
								<cfset message = "phr_login_OneMoreLowerCaseCharacter">
							<cfelseif not compare(lcase(result.string),result.string)>
								<cfset message = "phr_login_OneMoreUpperCaseCharacter">
							</cfif>
						</cfif>
					<cfelse>
						<cfset message = "phr_login_#item#">
					</cfif>

					<cfset arguments.result.fullDescription = result.fullDescription & "<div class='#className#'>" & message & "</div>">
					<!--- <cfset arguments.result.fullDescription = result.fullDescription & "<tr><td class='#className#' align='left' nowrap style='padding:0px;margin:0px;'><span><img src='#icon#' style='height:10px;width:10px'/>" & message & "</span></td></tr>">--->
					<!--- <cfset arguments.result.fullDescription = result.fullDescription & "<span style='#style#'>#tickCross# #item#</span><BR>"> --->
				</cfif>
			</cfloop>
		<!--- </cfif> --->


		<cfset arguments.result.fullDescription = application.com.relayTranslations.translateString(string=result.fullDescription,mergeStruct=mergeStruct)>
		<cfreturn arguments.result>

	</cffunction>


	<cffunction access="public" name="checkPasswordNotUsedBefore" hint="Checks Password Not Used Before within period specified in settings">
		<cfargument name="encryptedPassword" type="string" required="yes">
		<cfargument name="personid" type="numeric" required="yes">

		<cfset var internalExternal = iif (request.relayCurrentUser.isInternal,de("internalsites"),de("externalsites"))>
		<cfset var numDaysBeforePasswordCanBeReused = application.com.settings.getSetting("security.#internalExternal#.passwords.timebeforereuse_days")>
		<cfset var checkPreviousPasswords = '' />

		<CFQUERY NAME="checkPreviousPasswords"  datasource="#application.sitedatasource#">
			select * from modregister mr inner join modentitydef med on mr.modentityid = med.modentityid
			where med.tablename = 'Person' and med.fieldname = 'Password' and mr.recordid = #personid# and mr.modDate > dateAdd(d,-#numDaysBeforePasswordCanBeReused#,getdate()) and newval =  <cf_queryparam value="#encryptedPassword#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>

		<cfreturn not checkPreviousPasswords.recordcount>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUserOrganisationID" hint="Get organisation ID user is a part of" returntype="numeric">

		<cfargument name="userID" type="numeric" default="#listFirst(user, "-")#">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var getUserOrganisationID = "";
		</cfscript>

		<CFQUERY NAME="getUserOrganisationID" datasource="#arguments.datasource#">
			SELECT organisationID from person where PersonID = #arguments.userID#
		</CFQUERY>

		<cfreturn getUserOrganisationID.organisationID>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deletePartnerUserGroups" hint="Deletes any rights which have been set up automatically on logging into an external site, see cf_createusergroups" >

		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfscript>
			var deletePartnerUserGroups = "";
		</cfscript>

		<CFQUERY NAME="deletePartnerUserGroups" DATASOURCE="#application.SiteDataSource#">
			delete
			from rightsGroup
			where personid = #arguments.personID#
			and createdby = -1
		</CFQUERY>

	</cffunction>
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertUsage" hint="Record login for statistics">
		<!--- WAB 2006-04-25 level parameter added --->
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="level" type="numeric" default="2">
		<!--- WAB 2008/02/05 added  impersonatedbyPersonid and sitedefid --->
		<cfargument name="impersonatedbyPersonid" type="numeric" default="0">
		<cfargument name="siteDefID" type="numeric" default="#request.currentSite.siteDefID#">
		<cfargument name="source" type="string" default=""> <!--- NJH 2012/01/20 Social Media --->
		<cfargument name="siteDefDomainID" type="numeric" default="#request.currentSite.siteDefDomainID#"> <!--- NJH 2013/10/14 - Release 2 Sprint 2 --->
		<cfargument name="visitID" type="numeric" default="#request.relayCurrentUser.visitID#"> <!--- NJH 2013/10/14 - Release 2 Sprint 2 --->

		<cfscript>
			var insUsage = "";
		</cfscript>
		<!--- WAB 2005-05-12 added left to app , and increased size to 100 in databases --->
		<CFQUERY NAME="insUsage" DATASOURCE="#application.siteDataSource#" >
			INSERT INTO Usage (LoginDate, PersonID, Browser, RemoteIP,level, siteDefID,impersonatedbypersonid,source,siteDefDomainID,visitID)
			VALUES (<cf_queryparam value="#Now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#Left(CGI.HTTP_USER_AGENT,255)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#REMOTE_ADDR#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.level#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#siteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#impersonatedbypersonid#" CFSQLTYPE="CF_SQL_INTEGER">,
				<cf_queryparam value="#arguments.source#" CFSQLTYPE="CF_SQL_VARCHAR">,
				<cf_queryparam value="#arguments.siteDefDomainID#" CFSQLTYPE="CF_SQL_INTEGER">,
				<cf_queryparam value="#arguments.visitID#" CFSQLTYPE="CF_SQL_INTEGER">
			)
		</CFQUERY>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	checkInternalPermissions

	Checks the session security variables
	pass the securityTask  (eg commTask, AdminTask)
	optionally pass the securitylevel you wish to test (eg Level1,Level2, Edit, Add)
	optionally pass the minimumsecurityLevel
	If securitylevel passed then it returns true or false
	If securitylevel not passed (or Null) then it returns a structure with all the levels - pretents to be a query so you can check structure.recordcount

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkInternalPermissions" hint="Read permissions from the session.  If a level is given then returns true or false for that level.  If no level given then returns all permissions for that task">

		<cfargument name="securityTask" type="string" required="yes">
		<cfargument name="securityLevel" type="string" default="">
		<cfargument name="minimumsecurityLevel" type="string" default="">
		<cfset var returnVariable = false >
		<cfset var returnStruct = structnew()>

		<cfscript>

			//if any changes executed on security permissions, re-initialize the session structure
			if(not isDefined("session.security.versionNumber") or session.security.versionNumber neq application.cachedSecurityVersionNumber){
				initUserSession(request.relaycurrentuser.personid);
			}
		</cfscript>



		<cfif arguments.securityLevel is not "">
				<cfif structKeyExists(session.security, arguments.securityTask) and structKeyExists(session.security[arguments.securityTask], arguments.securityLevel)>
					<!--- WAB: occasionally for some reason the securitytask exists as a key without the securitylevel - this shouldn't be possible but??? --->
					<cfset returnVariable = session.security[arguments.securityTask][arguments.securityLevel]>
				</cfif>

			<cfreturn returnVariable>

		<cfelse>

			<!--- no securitylevel passed so return complete structure --->
			<cfscript>
					returnStruct.recordcount =  0 ; // for backwards compatability we pretend that it is a query!

				if(structKeyExists(session.security, arguments.securityTask)){
					if ( minimumsecurityLevel is '' or session.security[arguments.securityTask][minimumsecurityLevel]) {
						returnStruct = session.security[arguments.securityTask];
						returnStruct.recordcount =  1 ;
					}
				}

			</cfscript>

			<cfreturn returnStruct>


		</cfif>


	</cffunction>

	<!---
		WAB 2007-01-29 alerations to menu security to allow ANDs and ORs
	Added this procedure to handle a list of securityTypes such as   "adminTask:Level1 AND userTask:Level10"
	called by defaulLMenuXMLv2.cfm
	--->
	<cffunction access="public" name="checkSecurityList" hint="works out whether current user has permissions required in a given security List - handles ands and ors">
		<cfargument name="securityList" type="string" required="yes">
			<cfset var item = "">
			<cfset var re = "">
			<cfset var thisResult = "">
			<cfset var thisSecurity = "">
			<cfset var resultExpression = securityList>

			<cfloop index = "thisSecurity" list= #securityList# delimiters="&(|,) !">

				<cfif thisSecurity is not "AND" and thisSecurity is not "OR" and thisSecurity is not "NOT">
					<cfset re = "(\A|[^a-zA-Z0-9])(#thisSecurity#)(\Z|[^a-zA-Z0-9])">
					<cfset thisResult = checkInternalPermissions (listfirst(thissecurity,":"), listlast(thissecurity,":"))>
					<!---
					WAB 2009/06/29 replaced this replace with a regexp one which dealt with the case of one task being a substring of another  (adminTask and sysAdminTask) which cause things to break
					<cfset resultExpression = ReplaceNoCase(resultExpression,thisSecurity, thisResult,"ALL")>
					--->
					<cfset resultExpression = reReplaceNoCase(resultExpression,re, "\1#thisResult#\3","ALL")>
				</cfif>

			</cfloop>

			<cfset resultExpression = replaceNoCase(resultExpression,","," AND ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression,"&&"," AND ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression,"&"," AND ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression, "||" ," OR ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression, "|" ," OR ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression, "!" ," NOT  ","ALL")>

			<cfreturn evaluate(resultExpression)>



	</cffunction>



	<cffunction access="public" name="initUserSession" hint="Initialize user session security structure">
		<cfargument name="personID" type="numeric" required="yes">
		<cfset var security = application.com.rights.getPermissions(personID=arguments.personID, includeVersionNumber=true) > <!--- Delegate to a function RJT--->
		<cfset var FullPersonDetails = "">
		<cfset var i = 0 >
		<cfset var getSecurity = "">

			<!---	WAB 2009/06/09 removed this - legacy code not used
				<!--- put the person's details into a session variable --->
			<cfset FullPersonDetails = application.com.commonQueries.getFullPersonDetails(arguments.personID)>
			--->

			<cflock scope="session" timeout="1" type="exclusive">
				<!---  set up a security as a session variable --->
				<cfset session.security = security>
				<!---	WAB 2009/06/09 removed this - legacy code not used <cfset session.FullPersonDetails = FullPersonDetails> --->

			</cflock>


	</cffunction>


<!---
WAB 2005-03-14
This function is a duplicate of com.commonqueries.getP
 --->
	<cffunction access="public" name="getMagicNumber" hint="Returns a magic Number for a given personid" returntype="string">
		<!--- personID should  no longer be passed  - left in for backwards compatibility--->
		<cfargument name="personID" required="no">
		<cfargument name="entityID" type="numeric" required="no" default="0">
		<cfargument name="entityTypeID" type="numeric" required="no" default="-1">

		<cfset var getMagicNumber = "">

		<cfif (structkeyexists(arguments,"personID"))>

			<cfset arguments.entityID = arguments.personID>
			<cfset arguments.entityTypeID = 0>
		</cfif>

		<cfif ((arguments.entityID neq 0) and (arguments.entityTypeID neq -1))>

			<cfswitch expression="#arguments.entityTypeID#">
				<!--- person --->
				<cfcase value="0">

					<cfquery name="getMagicNumber" datasource="#application.siteDataSource#">
					select
						convert(varchar,(personid)) + '-' + convert(varchar,(personid & 1023) * (locationid & 1023)) as magicNumber
					from
						person
					where
						personid = #entityID#
					</cfquery>
				</cfcase>
				<!--- organisation --->
				<cfcase value="2">
					<cfquery name="getMagicNumber" datasource="#application.siteDataSource#">
					select
						convert(varchar,(organisationid)) + '-' + convert(varchar,(organisationid & 1023) * 1023) as magicNumber
					from
						organisation
					where
						organisationid = #entityID#
					</cfquery>
				</cfcase>

				<cfdefaultcase>
					<cfreturn "">
				</cfdefaultcase>
			</cfswitch>

			<cfreturn getMagicNumber.magicNumber>

		<cfelse>
			You must pass either personID or entityTypeID and entityID to the getMagicNumber function.
			<CF_ABORT>
		</cfif>

	</cffunction>


	<cffunction access="public" name="checkMagicNUmber" hint="verifies a magic number string and returns the personid (or 0 if not valid)" returntype="numeric">

		<cfargument name="MagicNumber" type="string" required="yes">
		<cfargument name="entityTypeID" type="numeric" required="no" default="-1">
		<cfscript>
			var ListFirstP = "";
			var ListLastP = "";
			var checkNumber = "";
		</cfscript>

		<cfif arguments.entityTypeID eq -1>
			<cfset arguments.entityTypeID = 0>
		</cfif>
		<!--- check that magic number is in form "nnnn-mmmm"--->
		<cfif listLen( MagicNumber , "-") is not 2>
			<cfreturn 0>
		</cfif>

		<cfset listlastP = listlast( MagicNumber , "-")>
		<cfset listfirstP = listfirst( MagicNumber , "-")>

		<cfif not isNumeric(listlastP) or not isNumeric(listfirstP)>
			<cfreturn 0>
		</cfif>

		<cfswitch expression="#arguments.entityTypeID#">
			<!--- person --->
			<cfcase value="0">
					<CFQUERY NAME="CheckNumber" datasource="#application.sitedatasource#">
					Select
						 PersonID as entityID
					From
						Person
							inner join
						location on person.locationid = location.locationid

					Where
						personid =  <cf_queryparam value="#listFirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
						and (personid & 1023) * (person.locationid & 1023)    =  <cf_queryparam value="#listLastP#" CFSQLTYPE="cf_sql_integer" >

					UNION

					Select
						 PersonID as entityID
					From
						Person
							inner join
						location on person.locationid = location.locationid
							inner join
						dedupeAudit on person.locationid = newid and recordType = 'location'
					Where
						personid =  <cf_queryparam value="#listFirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
						and (personid & 1023) * (archivedid & 1023)  =  <cf_queryparam value="#listLastP#" CFSQLTYPE="cf_sql_integer" >

					</CFQUERY>
				</cfcase>
				<!--- organisation --->
				<cfcase value="2">
					<CFQUERY NAME="CheckNumber" datasource="#application.sitedatasource#">
						Select organisationid as entityID
						From organisation
						where organisationid =  <cf_queryparam value="#listFirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
						and (organisationid & 1023) * 1023  =  <cf_queryparam value="#listlastp#" CFSQLTYPE="cf_sql_integer" >
					</CFQUERY>
				</cfcase>
			</cfswitch>
			<cfif checkNUmber.recordcount is not 0>
				<cfreturn checkNUmber.entityID>
			<cfelse>
				<cfreturn 0>
			</cfif>

	</cffunction>


	<!---
	WAB 2008/02/05
		factorisation of code which was appearing everywhere!
		loops through 1 or more securityProfileIDs

	--->

	<cffunction name="applyMultipleSecurityProfiles" returntype="struct" output="Yes">
		<cfargument name="personID" type="numeric" required="Yes">
		<cfargument name="securityProfileIDs" type="string" required="Yes">
		<!--- WAB 2008/06/16 removed - appeared not used <cfargument name="DynamicProfileChecks" type="string" required="No" default=""> --->

			<cfscript>
				var profileSuccess = structNew();
				var securityProfileListlength = "";
				var i = '' ;
				if (securityProfileIDs neq "" and securityProfileIDs neq 0){
					securityProfileListlength = listlen(securityProfileIDs);

					for(i=1; i lte securityProfileListlength; i=i+1) {
						profileSuccess = applySecurityProfile (personID = personID,securityProfileID = ListGetAt(securityProfileIDs,i)); // WAB 2008/06/16 removed - appeared not used  , DynamicProfileChecks=DynamicProfileChecks
						if (not profileSuccess.continueLogin) {
							break;
							}
					}
				} else {

					profileSuccess.continueLogin = true ;// no security profiles thereforeOK
				}

			</cfscript>

		<cfreturn profileSuccess>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		AR 2005-03-18

		This function applies a security profile to a person.

		Due to time constraints this code works on the assumption that ALL flags
		are Person flags.

		Please note that some of the flag work here should be in the flag.cfc

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="applySecurityProfile" returntype="struct" output="Yes">
		<cfargument name="personID" type="numeric" required="Yes">
		<cfargument name="securityProfileID" type="numeric" required="Yes">
		<!--- // WAB 2008/06/16 removed - appeared not used  ,<cfargument name="DynamicProfileChecks" type="string" required="No" default=""> --->

		<cfscript>
			var profileStruct = {};
			var profileQry = "";
			var flagsInGroupQry = "";
			var personQry = "";
			var flagQry = "";
			var SetFlagIndex = "";
			var needFlagIndex = "";
			var needFlagsValid = true;
			var needFlagIDs = "";
			var ixDynamicProfileChecks = '';
			profileStruct.continueLogin = true;
			profileStruct.failETID = "";
		</cfscript>
		<!--- Get the profile --->
		<cfquery NAME="profileQry" DATASOURCE="#application.SiteDataSource#">
			select securityProfileID,
				setFlagIDs,
				needFlagIDs,
				failETID,
				onFailContinue from securityProfile where securityProfileID = #arguments.securityProfileID#
		</cfquery>
		<!--- Logic has been assumed that all flags are  the setFlagIDs must be set against a person --->
		<!--- Clear down the set of flags as specified by the setflags flag groups. --->
		<!--- GCC - 2005/09/19 - Added listlen to allow 'need only' checks without setting flags--->
		<cfif listlen(profileQry.setFlagIDs) gt 0>
			<cfloop list="#profileQry.setFlagIDs#" index="setFlagIndex">
				<cfscript>
					flagsInGroupQry = application.com.flag.getFlagGroupFlags(FlagGroupId= application.com.flag.getFlagStructure(setFlagIndex).flaggroupID);
				</cfscript>
				<cfloop query="flagsInGroupQry">
					<cfscript>
						application.com.flag.unsetBooleanFlag(entityID = arguments.personid,flagid =flagsInGroupQry.flagID);
					</cfscript>

				</cfloop>
			</cfloop>
		</cfif>

		<!--- 2007/06/19 GCC - Can specify a flag which can be recalculated instead of using a hard coded flagTextID. syntax =  "profileID#application.delim1#profileCheck,profileID#application.delim1#profileCheck" for now. Clunky...--->
		<cfset needFlagIDs = profileQry.needFlagIDs>

		<!--- START 2007-06-20 AJC CR-LEX525 Portal Access Security --->
		<!--- <cf_include template="\code\cftemplates\DynamicProfilechecks.cfm" checkIfExists="true"> --->
		<!--- 2011/03/29 GCC put back because arguments are not passed into the template using cf_include --->
		<cfif FileExists("#application.paths.code#\cftemplates\DynamicProfilechecks.cfm")>
			<cfinclude template="/code/cftemplates/DynamicProfilechecks.cfm">
		</cfif>
		<!--- END 2007-06-20 AJC CR-LEX525 Portal Access Security --->

		<!--- If needFlagIDs is null and dynamicprofilechecks is set--->
		<!--- <cfif arguments.DynamicProfileChecks neq "" and needFlagIDs eq "">
			<cfloop list="#arguments.DynamicProfileChecks#" index="ixDynamicProfileChecks" delimiters=",">
				<cfif arguments.securityProfileID eq listfirst(ixDynamicProfileChecks,"#application.delim1#")>
					<cfset needFlagIDs = listLast(ixDynamicProfileChecks,"#application.delim1#")>
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif> --->


		<!--- WAB modified 2006-10-23
			Needed to 	i)  be able to handle flags on person/location/organisation
						ii) be able to handle AND and OR of these flags

			The list of flags can now include brackets, pipes (|) , ANDs and ORs, NOTs or &&

		<cfloop list="#needFlagIDs#" index="needFlagIndex">
			<cfscript>
			flagQry = application.com.flag.getFlagData(entityID=arguments.personID,flagID=trim(needFlagIndex));
				if (flagQry.recordcount eq 0){
					needFlagsValid = false;
				}
			</cfscript>
		</cfloop>
		 --->
		<cfset needFlagsValid = application.com.flag.evaluateFlagExpression(expression=needFlagIDs, personid = arguments.personID)>


		<!--- GCC - 2005/09/19 - Added listlen to allow 'need only' checks without setting flags--->
		<cfif needFlagsValid>

			<!--- Now set the flags. --->
			<cfif listlen(profileQry.setFlagIDs) gt 0>
				<cfloop list="#profileQry.setFlagIDs#" index="setFlagIndex">
					<cfscript>
						application.com.flag.setBooleanFlag(entityID = arguments.personid,FLAGTEXTID = setFlagIndex);
					</cfscript>
				</cfloop>
			</cfif>
		<cfelse>
			<cfset profileStruct.continueLogin = false>
			<cfset profileStruct.failETID = profileQry.failETID>
		</cfif>

		<cfreturn profileStruct>

	</cffunction>


<!---
WAB 2005-07-18
Tells you whether a person is a valid relay internal user. Handy if you are on an external site and ned to know
I am using it to set a value in relaycurrentuser structure



 --->
	<cffunction name="isPersonAValidRelayInternalUser" returntype="boolean" output="Yes">
			<cfargument name="personID" type="numeric" required="Yes">
			<cfset var checkValid ="">

			<cfquery NAME="checkValid" DATASOURCE="#application.SiteDataSource#">
			select 1 from
				person p
					inner join
				usergroup ug  on ug.personid = p.personid

			where p.personid = #personid#
			and isnull(password,'') <> ''
			and isnull(username,'') <> ''
			<!--- and DateDiff(d,PasswordDate,getdate()) > #application. passValidFor#   this ought to be part of the test, but it isn't being enforced so can't be used --->
			and LoginExpires > getdate()
			and p.personid <>  <cf_queryparam value="#application.unknownPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfreturn  checkValid.recordcount>

	</cffunction>

	<!--- when I initially wrote this function I got confused between relay and channel master and gave the function the wrong name
		left here as a stub while I correct problem
		WAB 2006-03-28
	--->
	<cffunction name="isPersonAValidChannelMasterInternalUser" returntype="boolean" output="Yes">
			<cfargument name="personID" type="numeric" required="Yes">
			<cfreturn  isPersonAValidRelayInternalUser(personid)>

	</cffunction>

		<!---2015/05/13 PYW Case 444667 - extend  to set httponly flag --->
	<cffunction name="setRememberMeCookie" >
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.Personid#">
		<cfset application.com.globalFunctions.cfcookie(name="dejavu", value="#personid#", expires="never", httponly="true")>

	</cffunction>

	<cffunction name="unSetRememberMeCookie" >
		<cfset application.com.globalFunctions.cfcookie(name="dejavu", value="", expires="NOW")>
	</cffunction>


	<!--- Adds person to user groups based on their profile (as defined in the  RightsFlagGroup table)
		code previously in customTags\CreateUserGroups.cfm but moved here when code updated
		WAB 2015-12-01  PROD2015-26 Visibility. Implementation of more sophisticated conditions.  Demise of the rightsFlagGroup table
	--->

	<cffunction name="addPersonToUserGroupsByProfile" >
		<cfargument name="personID" type="numeric" required="true">
		<cfset var insertIntoUserGroups = "">

		<cfif personid is not application.unknownpersonid or not structKeyExists (application,"unknownUserRightsLastUpdated") or datediff ("h",application.unknownUserRightsLastUpdated,now()) gt 1>

			<cflock timeout="5" throwontimeout="Yes" name="#application.applicationName#_updateUserGroups_#personid#" type="EXCLUSIVE">

				<cfif personid is not application.unknownpersonid or not structKeyExists (application,"unknownUserRightsLastUpdated") or datediff ("h",application.unknownUserRightsLastUpdated,now()) gt 1>

					<CFQUERY NAME="insertIntoUserGroups" DATASOURCE="#application.SiteDataSource#">
					exec flagConditionToUserGroup @personid = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">
					</CFQUERY>

					<cfif personid is application.unknownpersonid>
						<cfset application.unknownUserRightsLastUpdated = now()>
					</cfif>

					<cfif fileexists (expandpath ("\code\cftemplates\addPeopleToUserGroup.cfm"))>

						<!--- 
							Ideally addPeopleToUserGroup.cfm files should be removed and their logic moved to userGroup.conditionSQL
							However code left here so that things continue to work during migration
							WAB 2016-06-16  CASE 450022 Not as backwards compatible as I thought!
											When this code ran it deleted all the usergroups which had just been added by flagConditionToUserGroup above.
											Removed that piece of code

						--->

				 		<CFQUERY NAME="insertIntoUserGroups" DATASOURCE="#application.SiteDataSource#">
						declare @tempTable table (personid int, usergroupid int)
						declare @personid int
						select @personid = #personid#

						insert into @tempTable (personid, usergroupid)
							select null, null
							<!--- included file contains any number of UNIONS selecting personid and usergroupid --->
							<cf_include template="\code\cftemplates\addPeopleToUserGroup.cfm" checkIfExists="true">

								<!--- Have now created a temporary table of all the new items, time to do adds them to the rights group table --->
								begin tran

								-- add all of the new usergroups
								insert Into rightsGroup (
											personid,
											userGroupID,
											createdBy,
											Created,
											lastUpdatedBy,
											Lastupdated)

								select
									distinct newRightsGroups.personid, newRightsGroups.usergroupid, -1, getdate(), -1, getdate()
								from
									@temptable as newRightsGroups
										left join
									rightsGroup rg on rg.personid = newRightsGroups.personid and rg.usergroupid = newRightsGroups.usergroupid
									where
										newRightsGroups.personid is not null and
										rg.usergroupid is null

								commit tran
								</CFQUERY>
					</cfif>
				</cfif>

			</cflock>

		</cfif>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		NJH 2008/12/02 CR_TNDNAB529
		NYB 2011-03-07 - rewrote to use getUserGroupAllocation function
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="isPersonInOneOrMoreUserGroups" returntype="boolean" hint="Determines whether the person is in any of the userGroups">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="userGroupIDList" type="string" required="false">
		<cfargument name="UserGroups" type="string" required="false">
   		<cfargument name="IncludePersonUserGroups" type="boolean" default="true">

		<cfscript>
			var qryIsPersonInUserGroups="";
			var argumentCollectionVar = arguments;
			if (structkeyexists(arguments,"userGroupIDList")) {
				StructInsert(argumentCollectionVar,"UserGroupIDs",arguments.userGroupIDList);
			}
			qryIsPersonInUserGroups = application.com.relayUserGroup.getUserGroupAllocation(argumentCollection=argumentCollectionVar);
		</cfscript>

		<cfif qryIsPersonInUserGroups.recordCount gt 0>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>


	<!--- NJH 2008/12/02 CR_TNDNAB529 --->
	<cffunction name="isCurrentUserInOneOrMoreUserGroups" returntype="boolean" hint="Determines whether the current user is in any of the userGroups">
		<cfargument name="userGroupIDList" type="string" required="true">

		<cfreturn isPersonInOneOrMoreUserGroups(personID=request.relayCurrentUser.personID,userGroupIDList=arguments.userGroupIDList)>

	</cffunction>

	<!--- NJH LID 5004 - function moved here from webservices. --->
	<cffunction access="public" name="updatePassword"  hint="" returntype="struct" >
		<cfargument name="frmUsername">
		<cfargument name="frmPassword">
		<cfargument name="frmNewPassword1">
		<cfargument name="frmNewPassword2">

		<cfset var result = {isOK=false,reason=""}>
		<cfset var checkLogin = "">
		<cfset var x = structNew()>

		<!---
			first of all we check that the OLD password is correct
			this rather badly named function, will tell me if OLD password is OK
		--->
		<cfset checkLogin = getWhyLoginFailed(username=frmUserName,password=frmPassword)	>
		<cfset result.message = checkLogin.message>


		<cfif not checkLogin.user.invalidPassword>
			<!---
			The OLD password is correct we can go ahead and do a change
			--->
			<cfset x = changeUserPassword (username=frmUserName,currentpassword=frmPassword,newpassword1 = frmnewpassword1,newpassword2 = frmnewpassword2)>
			<cfset result.isOK = x.isOK>
			<cfset result.reason = x.reason>
			<cfset result.isLoggedIn = request.relayCurrentUser.isLoggedIn>
			<cfset result.message = x.message>
		<cfelse>

			<cfset result.reason = "invalidPassword">
			<cfset result.message = "phr_login_invalidPassword">
		</cfif>

		<cfset result.message = application.com.relayTranslations.translatePhrase(phrase=result.message)>

		<cfreturn result>
	</cffunction>


	<!--- NJH taken from templates/qryCheckUser.cfm --->
	<cffunction name="isPersonValidUser" access="public" returntype="query">
		<cfargument name="personId" type="numeric" required="true">

		<cfset var loginExpiresInDays = application.com.settings.getSetting("security.internalsites.passwords.expiresin_days")>
 		<cfset var checkUser = "">

		<cfquery name="checkUser" datasource="#application.siteDataSource#">
			select email,firstname,lastname,username,password
			from person, location, usergroup
	 		where
				usergroup.personid = person.personid
				and person.locationid = location.locationid
				and person.personid = #arguments.personID#
				AND	person.Password <> ''
				AND	person.Username <> ''
				--AND person.PasswordDate >= DateAdd("day",-#loginExpiresInDays#,getDate()) <!--- LID 5176 NJH 2011/01/05 - don't look at password date to determine if user is valid or not --->
				AND	person.LoginExpires > getDate()
				AND person.active <> 0
				AND location.active <> 0
		</cfquery>

		<cfreturn checkUser>
	</cffunction>


	<!--- NJH 2014/04/16 - case 439429 - function to set a person as an internal user if they are not already. Gives basic rights --->
	<cffunction name="setPersonAsInternalUser" access="public" output="false" hint="Sets the given person as an internal user with the minimum requirements">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="loginExpiresDate" type="date" default="#dateAdd('yyyy',1,now())#"> <!--- needed a default value so set it to a year in the future. Can easily change if needed --->
		<cfargument name="countryIDList" type="string" required="false"> <!--- list of countryIds that the person has rights to --->

		<cfset var isPersonAlreadyAnInternalUser = "">

		<cfset var lastUpdatedBy=isDefined("request.relayCurrentUser.userGroupID")?request.relayCurrentUser.userGroupID:404>
		<cfset var lastUpdatedByPerson=isDefined("request.relayCurrentUser.personID")?request.relayCurrentUser.personID:404>

		<cfquery name="isPersonAlreadyAnInternalUser">
			select 1 from usergroup where personID = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer"> and userGroupType = 'Personal'
		</cfquery>

		<!--- if they are not already an internal user, then set them up --->
		<cfif not isPersonAlreadyAnInternalUser.recordCount>

			<cfset var getPersonDetails = "">
			<cfset var setUserGroupCountry = "">
			<cfset var setRightsGroup = "">


			<cfquery name="getPersonDetails">
				update person
					set LoginExpires = <cf_queryparam value="#arguments.loginExpiresDate#" cfsqltype="cf_sql_date">,
						lastUpdated = getDate(),
						lastUpdatedBy = <cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,
						lastUpdatedByPerson = <cf_queryparam value="#lastUpdatedByPerson#" cfsqltype="cf_sql_integer">
				where
					personID = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">

				select firstname+' '+lastname as name,l.countryID
				from person p
					inner join location l on p.locationID = l.locationId
				where p.personID = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfset createUserNamePassword(personID=arguments.personID,resolveDuplicates=true,overWritePassword=false,overWriteUserName=false)>

			<cfset var userGroupID = application.com.relayUserGroup.createUserGroup(personID=arguments.personID,name=getPersonDetails.name[1],type="Personal")>

			<cfif not structKeyExists(arguments,"countryIDList") or arguments.countryIDList eq "">
				<cfset arguments.countryIDList = getPersonDetails.countryID[1]>
			</cfif>

			<cfquery name="setUserGroupCountry">
				insert into userGroupCountry (userGroupID,countryID,createdBy,created,lastUpdatedBy,lastUpdated)
				select <cf_queryparam value="#userGroupID#" cfsqltype="cf_sql_integer">, c.CountryID, <cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,getDate(),<cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,getDate()
				from country as c
		 		where c.CountryID  in (<cf_queryparam value="#arguments.countryIDList#" cfsqltype="cf_sql_integer" list="true">)
					and c.ISOCode is not null
			</cfquery>

			<cfquery name="setRights">
				insert into rights (userGroupID, securityTypeID, countryID, permission)
				select distinct u.userGroupID, (select SecurityTypeID from securityType where shortname = 'RecordTask'), c.countryID, 7
				from usergroup as u,
					country c
				where personID =  <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">
					and c.countryID in (<cf_queryparam value="#arguments.countryIDList#" cfsqltype="cf_sql_integer" list="true">)
			</cfquery>
		</cfif>

	</cffunction>
</cfcomponent>

