<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Relay Forecasting" hint="Methods for managing Relay Forecasting module">

	<cfparam name="this.countryFilter" type="string" default="">
	<cfparam name="this.productGroupFilter" type="string" default="">

	<cfparam name="this.rangeStartTimeStamp" type="string" default="">
	<cfparam name="this.rangeEndTimeStamp" type="string" default="">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             GetForecastLists
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="GetForecastLists" hint="Lists currently available forecasts.">
		<cfquery name="qGetForecastLists" datasource="#this.dataSource#">
			SELECT calendarMonthDesc as period, forecastID, forecastName as Title,
					(select sum(forecastUnits) from forecastDetail where forecastID=f.forecastID) as total_Units--,
					--(select sum(ForecastRev) from forecastDetail where forecastID=f.forecastID) as total_Forecast_Rev,
					--(select sum(ActualRev) from forecastDetail where forecastID=f.forecastID) as total_Actual_Rev
				FROM forecast f inner JOIN calendarMonth cm
				on f.calendarMonthID = cm.calendarMonthID
				Order by calendarMonthDesc
		</cfquery>
		<cfset this.qGetForecastLists = qGetForecastLists>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="GetForecastDetailList" hint="Lists the details of currently available forecasts.">

		<cfset var countryFilter = iif( this.countryFilter neq "", "this.countryFilter", 0 )>
		<cfset var productGroupFilter = iif( this.productGroupFilter neq "", "this.productGroupFilter", 0 )>

		<cfquery name="GetForecastDetailList" datasource="#this.dataSource#">
			select * from vForecastDetail
					where forecastID =  <cf_queryparam value="#this.forecastID#" CFSQLTYPE="CF_SQL_INTEGER" >
                    and countryid in ( <cf_queryparam value="#countryFilter#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> )
                    and productgroupid in ( <cf_queryparam value="#productGroupFilter#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> )
			<cfif isdefined( "this.FilterSelect" ) and isdefined( "this.FilterSelectValues" )>
			<cfloop index="filterIndex" from="1" to="#ListLen( this.FilterSelect )#">
			and #ListGetAt( this.FilterSelect, filterIndex )# =  <cf_queryparam value="#ListGetAt( this.FilterSelectValues, filterIndex )#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfloop>
			</cfif>
			<cfif this.rangeStartTimeStamp neq "" and this.rangeEndTimeStamp neq "">
				AND Ship_month BETWEEN MONTH( <cf_queryparam value="#this.rangeStartTimeStamp#" CFSQLTYPE="CF_SQL_timestamp" > ) AND MONTH( <cf_queryparam value="#this.rangeEndTimeStamp#" CFSQLTYPE="CF_SQL_timestamp" > )
			</cfif>
			<cfif isDefined("THIS.sortOrder")>
				order by #THIS.sortOrder#
			</cfif>
		</cfquery>
		<cfset this.qGetForecastDetailList = GetForecastDetailList>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Inserts a new forecast header record.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertNewForecast" hint="Inserts a new forecast header record.">
			<!--- 1. check to see if we have a calendarMonth record for the current month
			2. If not insert one using forecastINI variable
			3. Insert a header row with a default name --->
		<cfquery name="insertNewForecast" datasource="#this.dataSource#">
			if not exists (SELECT calendarMonthID FROM calendarMonth
				where calendarMonth = month(getDate())
				)
			BEGIN
				INSERT INTO [calendarMonth]([calendarID], [calendarYear], [calendarMonth], [startDate], [endDate], [CalendarMonthDesc])
				SELECT 1, year(getdate()), month(getDate()), <cf_queryparam value="#DateAdd('d',firstDayofMonth(now()), createdate(year(now()),month(now()),day(now())))#" CFSQLTYPE="CF_SQL_timestamp" >, <cf_queryparam value="#DateAdd('d',firstDayofMonth(now()), createdate(year(now()),month(now()),day(now())))#" CFSQLTYPE="CF_SQL_timestamp" >, 'Month '+ convert(varchar(2),month(getDate()))+' '+convert(varchar(4),year(getdate()))
			END

			if not exists (SELECT [forecastID]
				FROM [forecast] f
				INNER JOIN calendarMonth cm ON cm.calendarMonthID = f.calendarMonthID
				where cm.calendarMonth = month(getDate())
				)
			BEGIN
				INSERT INTO [forecast]([ForecastName], [calendarMonthID], [createdBy], [lastUpdatedBy], [created], [lastUpdated], [locked])
				select 'European Forecast', calendarMonthID, 100, 100, getDate(), getDate(), 0
				from calendarMonth
				where calendarMonth = month(getDate())
			END

		</cfquery>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertCoutryForecastRecords" hint="Inserts the country forecast records by selecting them from ">
		<cfquery name="insertCoutryForecastRecords" datasource="#this.dataSource#">
			<!--- 1.  Check to see what is already there
			2.  Insert missing rows with a insert select from the opportunity tables
				using a subquery to ensure we do not insert duplicates
				and a filter to just include the data that the forecaster user
				has rights to --->
			INSERT INTO [forecastDetail]([forecastid], [productID], [organisationID],
				[ForecastUnits], [countryID], [ForecastDetailType], [oppProductID],
				[forecastMonth], [createdBy], [created], [lastUpdatedBy], [lastUpdated])
			SELECT <cf_queryparam value="#this.forecastID#" CFSQLTYPE="CF_SQL_INTEGER" >, productOrGroupID, entityID, oppP.quantity, opp.countryID, 'P', oppProductID,
				month(forecastShipDate), 100,getDate(), 100, getDate()
			FROM oppProducts oppP
				inner JOIN opportunity opp on oppP.opportunityID = opp.opportunityID
		</cfquery>
		<!--- <cfset this.qGetForecastDetailList = qGetForecastDetailList> --->
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="populateRunRateData" hint="Inserts the country forecast records by selecting them from ">
		<cfquery name="populateRunRateData" datasource="#this.dataSource#">
/*
			1.  Check to see what is already there
			2.  Insert missing rows with a insert select from the opportunity tables
				using a subquery to ensure we do not insert duplicates
				and a filter to just include the data that the forecaster user
				has rights to

			SELECT calendarMonthDesc as Month, forecastName as forecast_Name, organisationName as organisation, forecastDetailID as ID,
				ForecastUnits as forecast_Units, p.SKU
					FROM forecastDetail fd inner JOIN forecast f on f.forecastID = fd.forecastID
					inner JOIN calendarMonth cm on f.calendarMonthID = cm.calendarMonthID
					inner JOIN product p on fd.productID = p.productID
					inner join organisation o on o.organisationID = fd.organisationID
					where fd.forecastID =  <cf_queryparam value="#this.forecastID#" CFSQLTYPE="CF_SQL_INTEGER" >
				Order by calendarMonthDesc
*/
			select top 1 * from person

		</cfquery>
		<cfset this.qRunRateData = populateRunRateData>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductGroupData" hint="Returns current productGroup">
		<cfquery name="getProductGroupData" datasource="#this.dataSource#">
			select distinct pg.productGroupID, pg.description
			from productGroup pg
			INNER JOIN product p ON p.productGroupID=pg.productGroupID
			INNER JOIN forecastDetail fd ON p.productID=fd.productID
			WHERE forecastID =  <cf_queryparam value="#this.forecastID#" CFSQLTYPE="CF_SQL_INTEGER" >
			order by pg.description
		</cfquery>
		<cfset this.qProductGroupData = getProductGroupData>
	</cffunction>

</cfcomponent>