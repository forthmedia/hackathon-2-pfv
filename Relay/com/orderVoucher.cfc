<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		orderVoucher.cfc	
Author:			JC
Date started:		2004-12-07
	
Description:		This CFC controls the relationship between Orders and Vouchers

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


 --->
 <!--- validateVoucherCode(orderid,vouchercode); --->
<cfcomponent displayname="Validate Voucher Code" hint="Functions for controlling relationship between Orders and Vouchers">
	<cfif isDefined("application.siteDataSource")>  <!--- when reloaded remotely application is not necessarily available, will be populated by an initialise function instead  --->
		<cfparam name="datasource" default="#application.siteDataSource#">
	</cfif>	

	<cffunction access="public" name="validateVoucherCode" hint="This validates the incoming Voucher Code.">
		<cfargument name="orderid" type="string" required="yes"><!--- int database format --->
		<cfargument name="vouchercode" type="string" required="yes">

		<!--- validation steps
		
		1) Voucher Active
		2) Today between Voucher start and end dates
		3) Voucher Quantity has not been exceeded (count instances of vouchercode in orderItem)
		4) Product associated with VoucherCode (VoucherProducts) exists on order form and that 
		only single quantity being ordered on this orderItem line
		---->
			
			<!---
			
			--->
			<!--- initialise query - recommended by GC to resolve a problem whereby the recordcount from this query being incorrectly returned --->
			<cfscript> 
				var qgetVoucher = ''; 
				var qVoucherUses = '';
				var error = '';
			</cfscript>
			
			<cfquery name="qgetVoucher" datasource="#application.siteDataSource#">
			 SELECT getdate() as today, oi.vouchercode,oi.productid, oi.quantity,v.active,v.voucherstartdate,v.voucherenddate,v.voucherQuantity,oi.unitlistprice
			 FROM voucher v, voucherProducts vp,orderitem oi
			 where oi.orderid =  <cf_queryparam value="#orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			 and oi.productid = vp.productid
			and vp.vouchercode =  <cf_queryparam value="#vouchercode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			 and vp.vouchercode = v.vouchercode
			 order by oi.unitlistprice desc <!--- this is set to ensure that discount products are attempted only after all +value products --->
			</cfquery>
			
			<!--- qGetVoucher will return multi rows if several products are
			on the order and are available for access via the entered Voucher --->
			
			<cfif qgetVoucher.recordcount EQ 0>
				<cfset error = "phr_Order_InvalidVoucherCode">
			<cfelseif len(trim(qgetVoucher.vouchercode)) GT 0>
				<cfset error = "phr_Order_VoucherCodeAlreadySet<BR><a href='' onclick='RunTimer();'>phr_ClickHere</a>">
			<cfelseif qgetVoucher.active NEQ 1>
				<cfset error = "phr_Order_InactiveVoucherCode">
			<cfelseif qgetVoucher.voucherstartdate NEQ "" AND datecompare(qgetVoucher.today,qgetVoucher.voucherstartdate) EQ -1>
				<cfset error = "phr_Order_VoucherCodeNotYetActive">
			<cfelseif qgetVoucher.voucherenddate NEQ "" AND datecompare(qgetVoucher.today,qgetVoucher.voucherenddate) EQ 1>
				<cfset error = "phr_Order_ExpiredVoucherCode">
			<cfelse>
				<!--- are any Voucher uses still available --->
				<cfquery name="qVoucherUses" datasource="#application.siteDataSource#">
				 SELECT count(vouchercode) as uses
				 FROM orderitem
				 where vouchercode =  <cf_queryparam value="#vouchercode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				</cfquery>
				<cfif qVoucherUses.uses GTE qgetVoucher.voucherQuantity>
					<cfset error = "phr_Order_VoucherUsageExceeded">
				</cfif>
				<cfif NOT len(error) gt 0>
					<!--- loop over all the items on the order and, if quantity on orderitem line
					is 1 apply vouchercode to first available item --->
					
					<cfset return = "">
					<cfloop query="qgetVoucher">
						<!--- 
						a single product returned that may be associated with this Order/Voucher combo.
						Only return multiple products if they apply a discount (ie have a negative unitlistprice --->
						<cfif  #qgetVoucher.quantity# EQ 1 AND (#return# EQ "" OR #qGetVoucher.unitlistprice# LT 0)>
							<cfset return = "#ListAppend(return,productid)#">
						</cfif>
					</cfloop>
					<cfif #return# EQ "">
						<cfset error = "phr_Order_VoucherQuantityError">
					</cfif>
				</cfif>
			</cfif>
			<cfif NOT len(error) gt 0>
				<!--- this will usually consist of a single productid  --->
				<cfif #IsDefined("return")#>
					<cfset return = '#return#'>
				<cfelse>
					<cfset return = '#productid#'>
				</cfif>
			<cfelse>
				<cfset return = '#error#'>
			</cfif>
		<cfreturn return>
	</cffunction>


	<cffunction access="public" name="updateOrderItem" hint="Add current Voucher Code to OrderItem.">
		<cfargument name="orderid" type="string" required="yes">
		<cfargument name="vouchercode" type="string" required="yes">
		<cfargument name="productid" type="string" required="yes">
		
			<cfscript> 
				var error = '';
			</cfscript>
			<cftransaction>
				<cfquery name="qupdateOrderItem" datasource="#application.siteDataSource#">
				 UPDATE orderitem
				 SET vouchercode =  <cf_queryparam value="#vouchercode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				 VoucherValue = 
				 	(
						select ((totallistprice * voucherDiscountvalue)/100) as VoucherValue
						from orderitem oi, voucherProducts vp, voucher v
						where oi.orderid =  <cf_queryparam value="#orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and oi.productid = vp.productid
						and vp.productid =  <cf_queryparam value="#productid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and vp.vouchercode =  <cf_queryparam value="#vouchercode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						and vp.vouchercode = v.vouchercode
					)
				 where orderid =  <cf_queryparam value="#orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				 and productid =  <cf_queryparam value="#productid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
	
				<cfquery name="qChkOrderItem" datasource="#application.siteDataSource#">
				 select vouchercode
				 from orderitem
				 where orderid =  <cf_queryparam value="#orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				 and productid =  <cf_queryparam value="#productid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				 and vouchercode =  <cf_queryparam value="#vouchercode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				</cfquery>
				<cfif #qChkOrderItem.recordcount# EQ 0>
					<cfset error = "phr_Order_VouchertoOrderItemError">
				</cfif>
			</cftransaction>
			
			<cfif NOT len(error) gt 0>
				<cfset updateresult = 'ok'>
			<cfelse>
				<cfset updateresult = '#qgetVoucher.productid#'>
			</cfif>
		<cfreturn updateresult>
	</cffunction>


	<!--- 
		function which will be run automatically when cfc is loaded into application.com
		needs to be able to run without application available
	 --->
	<cffunction access="public" name="initialise" hint="">
		<cfargument name="applicationScope" default = "#application#">	
		<!--- Note that this is a global variable, do not var --->
		<cfset datasource = applicationScope.sitedatasource>
	</cffunction>

</cfcomponent>