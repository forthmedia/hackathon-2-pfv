<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			schedule.cfc	
Author:				AJC
Date started:		2010-06-08
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016-07-16  DAN    Case 450822 - fix SQL type params for schedule update

Possible enhancements:


 --->

<cfcomponent displayname="schedule" hint="Provides functions for schedules">

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Add Schedule
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="add" output="No" returntype="string">
		<cfargument name="schedulename" type="String" required="true" />
		<cfargument name="description" type="String" required="true" />
		<cfargument name="ScheduleTypeID" type="Numeric" required="true" />
		<cfargument name="tablename" type="String" required="true" />
		<cfargument name="startdate" type="Date" required="true" />
		<cfargument name="enddate" type="any" required="false" default="" />
		<cfargument name="frequencyDailyHours" type="Numeric" required="true"/>
		<cfargument name="frequencyStartTime" type="String" required="true"/>
		<cfargument name="frequencyEndTime" type="String" required="true"/>
		<cfargument name="frequencyDaySun" type="boolean" required="true" />
		<cfargument name="frequencyDayMon" type="boolean" required="true" />
		<cfargument name="frequencyDayTue" type="boolean" required="true" />
		<cfargument name="frequencyDayWed" type="boolean" required="true" />
		<cfargument name="frequencyDayThur" type="boolean" required="true" />
		<cfargument name="frequencyDayFri" type="boolean" required="true" />
		<cfargument name="frequencyDaySat" type="boolean" required="true" />
		<cfargument name="scheduleQuery" type="string" required="true" />
		
		<cfquery name="local.qry_ins_schedule" datasource="#application.sitedatasource#">
			SET NOCOUNT ON
			insert into schedule (ScheduleName,Description,ScheduleTypeID,tablename,StartDate<cfif arguments.enddate neq "">,EndDate</cfif>,StartTime,EndTime,DailyHours,DailyMon,DailyTue,DailyWed,DailyThur,DailyFri,DailySat,DailySun,scheduleQuery)
			values (<cf_queryparam value="#arguments.ScheduleName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.ScheduleTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >,#arguments.startdate#<cfif arguments.enddate neq "">,#arguments.enddate#</cfif>,#frequencyStartTime#,#frequencyEndTime#,<cf_queryparam value="#arguments.frequencyDailyHours#" CFSQLTYPE="CF_SQL_Integer" >,<cf_queryparam value="#arguments.frequencyDayMon#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.frequencyDayTue#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.frequencyDayWed#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.frequencyDayThur#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.frequencyDayFri#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.frequencyDaySat#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.frequencyDaySun#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#arguments.scheduleQuery#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			SELECT scope_identity() as scheduleID
			SET NOCOUNT OFF
		</cfquery>
		
		<cfreturn qry_ins_schedule.scheduleID />
		
	</cffunction>
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Update Schedule
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="update" output="No" returntype="string">
		<cfargument name="scheduleID" type="String" required="true" />
		<cfargument name="schedulename" type="String" required="true" />
		<cfargument name="description" type="String" required="true" />
		<cfargument name="ScheduleTypeID" type="Numeric" required="true" />
		<cfargument name="tablename" type="String" required="true" />
		<cfargument name="startdate" type="Date" required="true" />
		<cfargument name="enddate" type="any" required="false" default="" />
		<cfargument name="frequencyDailyHours" type="Numeric" required="true"/>
		<cfargument name="frequencyStartTime" type="String" required="true"/>
		<cfargument name="frequencyEndTime" type="String" required="true"/>
		<cfargument name="frequencyDaySun" type="boolean" required="true" />
		<cfargument name="frequencyDayMon" type="boolean" required="true" />
		<cfargument name="frequencyDayTue" type="boolean" required="true" />
		<cfargument name="frequencyDayWed" type="boolean" required="true" />
		<cfargument name="frequencyDayThur" type="boolean" required="true" />
		<cfargument name="frequencyDayFri" type="boolean" required="true" />
		<cfargument name="frequencyDaySat" type="boolean" required="true" />
		<cfargument name="scheduleQuery" type="string" required="true" />
		
		<cfquery name="local.qry_ins_schedule" datasource="#application.sitedatasource#">
			update schedule 
			set ScheduleName =  <cf_queryparam value="#arguments.ScheduleName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			,Description =  <cf_queryparam value="#arguments.description#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			,ScheduleTypeID=#arguments.ScheduleTypeID#
			,tablename =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			,StartDate=#arguments.startdate#
			<cfif arguments.enddate neq "">,EndDate =  <cf_queryparam value="#arguments.enddate#" CFSQLTYPE="CF_SQL_TIMESTAMP" ></cfif>
			,StartTime =  <cf_queryparam value="#frequencyStartTime#" CFSQLTYPE="CF_SQL_INTEGER" >
			,EndTime =  <cf_queryparam value="#frequencyEndTime#" CFSQLTYPE="CF_SQL_INTEGER" >
			,DailyHours=#arguments.frequencyDailyHours#
			,DailyMon =  <cf_queryparam value="#arguments.frequencyDayMon#" CFSQLTYPE="CF_SQL_bit" > 
			,DailyTue =  <cf_queryparam value="#arguments.frequencyDayTue#" CFSQLTYPE="CF_SQL_bit" > 
			,DailyWed =  <cf_queryparam value="#arguments.frequencyDayWed#" CFSQLTYPE="CF_SQL_bit" > 
			,DailyThur =  <cf_queryparam value="#arguments.frequencyDayThur#" CFSQLTYPE="CF_SQL_bit" > 
			,DailyFri =  <cf_queryparam value="#arguments.frequencyDayFri#" CFSQLTYPE="CF_SQL_bit" > 
			,DailySat =  <cf_queryparam value="#arguments.frequencyDaySat#" CFSQLTYPE="CF_SQL_bit" > 
			,DailySun =  <cf_queryparam value="#arguments.frequencyDaySun#" CFSQLTYPE="CF_SQL_bit" > 
			,scheduleQuery =  <cf_queryparam value="#arguments.scheduleQuery#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			where scheduleID =  <cf_queryparam value="#arguments.scheduleID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<cfreturn arguments.scheduleID />
		
	</cffunction>

</cfcomponent>
