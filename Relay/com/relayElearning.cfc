﻿<!--- ©Relayware. All Rights Reserved 2014

File name:		relayElearning.cfc
Author:			SWJ
Date started:	03-07-2008

Description:	Functions for eLearning

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
3-Jul-08	SWJ	Added the function that returns the student progress
2008-10-24	NYB	Added support adding screens to modules.  incs creating function getModuleScreens
2008-11-25 - Bug where by it resets passed modules in insertPersonModuleProgress
2008-12-10 			AJC	 		Add ability to Activate a certification
2008-01-21			NYB			added functions around eligibility (that were originally in Sophos'' relayElearning.cfc)
2009-01-28			NYB			P-SOP006
2009-02-03 			NYB 		SOP-bug1338 - added Title to GetSpecialisationSummaryData
2009-02-26 			NYB 		SOP-bug1909 - added: cfif isDefined("request.sendNotificationEmail") then set arguments.sendNotificationEmail to request.sendNotificationEmail
								because there's no way built in to change the value of sendNotificationEmail
2009-02-06			NJH			P-SNY047 added new function that creates a rewards transaction when a module is complete if incentives is turned on for elearning
								Changed GetTrainingCoursesData and GetTrainingModulesData functions to use visibility rights if needed.
2009-03-13 			NYB			Stargate 2 - various changes, inc:
									- adding a call to extendInsertEntitySpecialisations (if exists) to InsertEntitySpecialisations
									- adding a call to extendExpireEntitySpecialisation (if exists) to ExpireEntitySpecialisation
2009-06-04 			NYB			SOP010 - adding support for certification pass date
2009-07-15			NJH			P-FNL069 created new function to check user rights on a particular elearning entity. It does very basic checking at the moment
								and should be expanded in the future. Also encrypted url variables for call to fileDisplay.
2009-07-16 			NYB			CR-SNY047 - changed GetStudentProgress query in GetStudentProgress function to return MODULE_STATUS prepended with 'phr_' - for elearning Student Progress relaytag
2009-07-16 			NYB			LHID2617 Bug - amended function insertPersonModuleProgress which was causing error in file relay\EventsPublic\RegUpdate.cfm
2009-09-15 			WAB			implemented encryptURL in place of encryptQueryString
2009-09-18			NYB  		LHID2578
2009-09-30			NYB  		LHID2686
2009-10-01			NJH			LID 2717
2009-10-15			NAS			LID 2750
2010-03-02          SSS         Sophos bonanza
2010-08-05 			PPB 		SOP024 - specialisation rule compulsory modules

2010-08-06			NJH			RW8.3 - useIncentives is now got from settings
2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses
2010-09-22 			PPB 		P-LEN022 (LID 4052) Function that returns the local language/country file that is in the same fileFamily as the file with the specified fileID
2010-10-06			NAS			LID4053: eLearning - Module Listing Additional Filters
2010-10-08			NAS			LID4300: Courses not launching correctly
2010-10-22			NAS			LID3990: eLearning - Courses listing filter on Course Title no course returned
2010-10-27			NYB			LHID 4121 - fixed a number of problems:
									compulsory module check was being done around insert-Specialisation logic - not the individual rules - changed
									compulsory module check was being taken into account for updating Specialisations
2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive)
2011-01-31			PPB			LID5510 - references to application.flag changed to use application.com.flag.getFlagStructure()
2011-02-24			NJH			LID 5753 - changed getCourseLauncherUrl to look for files in your language - nicked from elearningCoursesV2.cfm
2011-03-02			NYB 		LHID5772: added GetAssignableCertifications function
2011-03-07			NYB
2011-09-06 			PPB 		LID7679 country scope modules
2012-01-31			NYB			Case#426399 have added the ability to pass through module information as arguments to getCourseLauncherUrl
								instead of calling getModuleDetailsQry everysingle time, because this being called for every module returned
								by webservices/relayElearningWS.cfc/getCertificationItems was causing it to return the result so slowly it was raised as a bug
2012-02-02			NYB			Case#426399 rewrote entire getLocalVersionFileID function
2011/12/01			RMB			P-REL101	DeliveryMethod added to getModuleDetailsQry to change course link
2012-01-31			NYB			Case#426399 have added the ability to pass through module information as arguments to getCourseLauncherUrl
								instead of calling getModuleDetailsQry everysingle time, because this being called for every module returned
								by webservices/relayElearningWS.cfc/getCertificationItems was causing it to return the result so slowly it was raised as a bug
2012-02-02			NYB			Case#426399 rewrote entire getLocalVersionFileID function
2012-02-15			RMB			CASE: 426595 - Added Show_quiz to quary in "GetStudentProgress"
2012-03-27			RMB			added mobileCompatible to functions "GetTrainingCoursesData", "GetTrainingCoursesDataV2" and "GetTrainingModulesData"
2012/04/10			IH 			Case 427137 Rolled back "getLocalVersionFileID" to previous version and added new index to "files" table to fix timeout issues
2012-04-18			WAB			Case 427634 Problems with GetTrainingCoursesDataV2 - filtering not working. Looks as if translation snippet had been added badly.  Fixed
2012-04-03			Englex      P-REL109 - Add function to retreive training programs
2012-04-25			Englex 		P-REL109 - Change GetAssignableCertifications to filter by training program
2012-07-06 			WAB 		Changed way NullText generated for various queries bringing back column Training_Program using getPhrasePointerQuerySnippets() - so that do not end up having to translate 1000's of identical phr_s
2012-07-12			PJP			Case#429052 added new table trngModuleCourse, ammended queries to add in new link from trngModule to new table trngModuleCourse
2012-07-13			PJP 		added in sort order argument to function getTrainingProgram
2012-07-18			Englex		P-REL109 Phase 2 - Added filter by statustextID & personID to GetCertificationSummaryData
2012/07/26 			AJC			P-REL109 Phase 2 Module pre-requisites
2012-08-08			IH			Add Course Training Program Title to GetTrainingModulesData method
2012-08-10			IH			Add GetCourseSummary, improve GetCertificationSummary
2012-08-15 			PPB 		Case 427049 while working on this case I made changes to implement a fix done in Case 426399 but reverted in 427137
2012-09-03 			STCR		CASE 430266 Allow SCORMDataToRelayQuiz to map trngCoursewareData records where FileID=0
2012-09-06			STCR		Allow SCORM cmi.completion_status==completed to trigger module pass if module.isScorm eq 1 and module.passDecidedBy eq Courseware
2012-09-17			STCR		CASE 430523 SCORM able to populate QuizTaken.PassMark, QuizTaken.QuizStatus, trngCoursewareData.FileID, to help with reporting changes.
2012-12-11			YMA		Case:431788 fixed sendemail call in updatePersonCertificationData()
2012-11-14 			WAB/NJH 		CASE 431977 SCORMSetValue not calling updateQuizScore() correctly
2013-01-16 			PPB 		Case 433337 missing comma
2013-02-03			WAB			CASE 433333 altered GetAssignableCertifications() to deal with specialisation having more than one certification
2013-02-07 			PPB 		Case 433635 when grading students (from events delegate list) insert a userModuleProgress row (for the associated module) if there isn't one (or update and existing one)
2013-02-08 			PPB 		Case 433337 email issues
2013-02-12 			PPB 		Case 433751 argumentsStruct not reqd
2013-02-13			NYB 		Case 433693 changed alias in qryGetSpecialisationData qry in GetSpecialisationData function
2013-04-09			STCR 		CASE 432194 Translation of ModuleSet Title
2013-03-25 			PPB 		Case 431081 translate course title / module title in query so they sort
2013-04-23			STCR		CASE 432193 Respect Module and Certification Rule Activation Dates
2013-05-14 			NYB 		Case 434909
2013-05-22			YMA			Case 435369 limit certifications by publish date in certification registration relaytag
2013-05-28			STCR		CASE 435368 do not return Courses before their Activation Date, unless previewing content for a specific date
2013-05-28 			STCR		CASE 435369 Modify YMA's change, so that the Portal allows Content Editors to preview Certifications for future dates
2013-07-25			PPB			Case 436072 facility to reset a user's module progress and allow them to retake a quiz
2013-08-16 			PPB 		Case 436307 added active filter and sort order and isnulls to GetModuleSetsData
2013-09-11 			NYB 		Case 436307 - added incInactiveRules argument to GetCertificationSummary
2013-11-07 			NYB 		Case 437616 changed viewCountryIDm to viewCountryID and changed default to be based on setting in GetTrainingModulesData function
2013-11-12 			NYB 		Case 437478 replaced "=" with "in" in qryGetCertificationData query or GetAssignableCertifications function
2013-11-19 			NYB 		Case 436793 added various lines
2014-01-15 			PPB 		Case 438541 re-wrote getLocalVersionFileID function
2014-01-27 			PPB 		Case 438541 extended getLocalVersionFileID function to cope with 'All Countries' (CountryGroupID=37)
2014-03-18 			WAB  		CASE 439167 problem in updateEntitySpecialisationsRulesStatus
2014-02-20 			NYB 		Case 438849 translated Status column
2014-02-26 			NYB 		Case 438849 changed how this is done to use changes Will has made to translations/phrasepointercache
								NB:  THIS WILL REQUIRE RUNNING UPDATED VERSION OF:  StoredProcedures updatePhrasePointerCacheForEntityType.sql AND Trigger trigger_phrase_updatePhrasePointerCache.sql
2014-04-16 			NYB 		Case 439441 added ,#ModuleSetPhraseQuerySnippets.PHRASETABLEALIAS#.phrasetext to group by
2014-05-13			WAB/DCC		Performance improvements to getStudentProgress (use pre-translations and cache)
2014-09-23 			PPB 		Case 439473 transfer a few extra SCORM data items to quiz tables
2014/01/10			DXC			Case 441110 set variable "useWholeWordsOnly=false" to allow searching by whole words or part words. Defaults to whole words
2014-11-12			AHL			Case 442565 Training Module Error
2015-01-29 			GCC 		added some nolocks
2015/08/12			NJH			JIRA PROD2015-19 - brought in the trainingPath work as done originally by Yan for Lenovo.
2015-26-08			DCC			Case 445511 Additional checkzip function for autounzip setting on media group - more poignant error message if disabled on modules page
2015-11-04          DAN         Case 446442 - if accessing from mobile device filter training courses and modules by mobile compatibility
2015-11-26          DAN         BF-25 - fix duplicated WHERE clause (Partners are unable to view courses on the portal)
2016/01/06			NJH			Jira BF-169 - filter out courses where modules are inactive when getting courses that are active
2015-10-28          PYW         P-TAT006 BRD 31. Create deactivateExpiredModules. Task to deactivate active modules on due expiry date
2015-11-05          PYW         P-TAT006 BRD 31. Create sendPersonCertificationExpiryAlerts and remindModuleExpire
2015-11-24			IH			P-TAT006 BRD 31. Add showExpired to GetTrainingCoursesDataV2 and GetCourseSummary
2015-12-20          PYW         P-TAT006 BRD 31. Modify sendPersonCertificationExpiryAlerts to check for person's with person certfication expiry date
2015-12-22			PYW			P-TAT006 BRD 31. Add setPersonCertificationExpiryDate. Function that sets the expiry date for a person's certification
2015-12-22			PYW			P-TAT006 BRD 31. Add getTrngPersonCertificationMergeFields. Function that gets the merge fields for a person's certification.
								Add personCertificationExpiryDate field to GetPersonCertificationData
2016-01-04			PYW			P-TAT006 BRD 31. Update passValidPersonCertifications to check for module with Retest rule
17/02/2016          DAN         PROD2015-503 - make expiry notice period value available in the related email templates
2016-02-17          YMA         Extend function so that the expiry runs off the date the last module was passed instead of now().
2016-05-12			WAB 		During PROD2016-778 correct spelling of getRecordRightsFilterQuerySnippet()  (which was missing the T in get!)
2016-05-12			atrunov     PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules
2016-05-18			WAB			BF-714 Set validvalues="true" on getTrainingProgram()
2016-09-27 			WAB			PROD206-2393 GetCourseSummary(). Fix performance issues - also implement modern code for getting rights
2016-10-27 DCC case 452310 default validity to one month hence if none set otherwise it expires straight away
2016-11-09			DCC			PROD2016-2606 don't default it at all if set to 0
2016-11-15          DAN         PROD2016-2739 - fix setting pass date for person certification so it takes the actual module pass date instead of the request time
2017-01-12			WAB 		RT-138 InactivateCertification() timing out and causing db blocks.  Remove transaction.
2017-03-07			WAB			RT-309 getTrainingCoursesDataV2(). Training Videos Not Displaying - Country Rights Issue when module scoped to countryID 0 (ie all countries)

Possible enhancements:

should pass in "availableModules" param (preferably renamed throughout) into GetTrainingCoursesData as per GetTrainingModulesData so that you don't ever get a course listed which clicks though to an empty list of modules
while at it I question whether the logic should be "showAvailableModulesOnly" cos currently if you send in false it seems you would see a list of only the unavailable modules (not available and unavailable)

 --->

<cfcomponent displayname="relayELearning" hint="Provides functionality to manage the components">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="GetTrainingCoursesData" hint="Returns training course information">
		<cfargument name="sortOrder" required="no" default="sortOrder,Title_Of_Course">
		<cfargument name="visibleForPersonID" required="no"> <!--- NJH 2009-02-06 P-SNY047 --->
		<cfargument name="courseID" type="numeric" required="no"> <!--- NJH 2009-02-06 P-SNY047 --->
		<cfargument name="solutionArea" type="string" required="no" default=""> <!--- SKP 2010-09-01 LEN022 --->
		<cfargument name="userLevel" type="string" required="no" default=""> <!--- SKP 2010-09-01 LEN022 --->
		<cfargument name="series" type="string" required="no" default=""> <!--- SKP 2010-09-01 LEN022 --->
		<!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses --->
		<cfargument name="active" type="Boolean" required="no">
		<cfargument name="useModuleCountryScoping" type="boolean" required="no" default="false">				<!--- 2011-09-06 PPB LID7679 --->
		<cfargument name="showModulesScopedToAllCountries" type="boolean" required="no" default="true">		<!--- 2011-09-06 PPB LID7679 done for lenovo so Japanese don't have to see non-Japanese All Countries modules --->
		<cfargument name="fromActivationDate" type="date" required="false" /><!--- 2013-05-28 STCR CASE 435368 --->
        <cfargument name="isMobile" type="boolean" default="#request.relaycurrentuser.BROWSER.isMobile#" /> <!--- 2015-11-04 DAN Case 446442 --->

		<cfscript>
			var GetTrainingCoursesData = "";
			var titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "title",baseTableAlias="v");
			var descriptionPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "description",baseTableAlias="v"); // STCR 2012-05-31 P-REL109 CASE 428567. Seems that getTrainingCoursesData was using phraseQuerySnippets for the title but not for the description, however the relaytag nowadays uses the description field on the listing page not just on the detail page, so we better translate it in the query rather than in the loop
			var countryIdForAllCountries = application.com.commonQueries.getCountryIdFromDescription('All Countries');					//2011-09-06 PPB LID7679
			// START: 2012-05-01 - Englex - P-REL109 - Add training program
			var TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'");
			// END: 2012-05-01 - Englex - P-REL109 - Add training program
		</cfscript>

		<!--- NJH 2009-03-06 P-SNY047 added a distinct to the query and added sortOrder as the first column to sort by --->
		<cfquery name="GetTrainingCoursesData" datasource="#application.siteDataSource#">
				<!--- WAB 2010-09-14 Added support for phrasePointer cache --->
				<!--- START 2010-10-22			NAS			LID3990: eLearning - Courses listing filter on Course Title no course returned --->
				select * from
				(select distinct <!--- v.* , had to list fields individually since view also had a field Title_Of_Course and couldn't change name'--->
					courseid,publishedDate,Course_Code,Solution_Area,Level,Series,SortOrder,active, #descriptionPhraseQuerySnippets.select# as description, <!--- STCR 2012-05-31 P-REL109 CASE 428567. --->
					#titlePhraseQuerySnippets.select# as Title_Of_Course,courseid as viewCourseId, v.mobileCompatible, v.trainingprogramID			<!--- 2011-09-06 PPB LID7679 output viewCourseId so the subquery below can distinguish it from 'its own' courseid --->
					<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>,
					#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as Training_Program
					</cfif>
					<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
			from vTrngCourses v with (noLock)
					#preservesinglequotes(titlePhraseQuerySnippets.join)#
					#preservesinglequotes(descriptionPhraseQuerySnippets.join)# <!--- STCR 2012-05-31 P-REL109 CASE 428567. --->
					<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
					#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
					</cfif>
					<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
				<cfif structKeyExists(arguments,"visibleForPersonID")>
                    left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer">) as rr on rr.recordid = v.courseId
				</cfif>

                where 1=1

                <cfif structKeyExists(arguments,"visibleForPersonID")>
                    and (rr.level1 = 1 or ((v.hasRecordRightsBitMask & 1) = 0))
                </cfif>

				) as x
				<!--- STOP 2010-10-22		NAS			LID3990: eLearning - Courses listing filter on Course Title no course returned --->

                where 1=1

                <!--- start: 2015-11-04 DAN Case 446442 --->
                <cfif arguments.isMobile>
                    and mobileCompatible=1
                </cfif>
                <!--- end: 2015-11-04 DAN Case 446442 --->

				<!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses --->
				<cfif structKeyExists(arguments,"active")>
					and active =  <cf_queryparam value="#arguments.active#" CFSQLTYPE="cf_sql_float" >
				</cfif>
			<!--- BEGIN 2013-05-28 STCR CASE 435368 - do not return Courses before their Activation Date, unless previewing content for a specific date --->
			<cfif structKeyExists(arguments,"fromActivationDate")>
				and dbo.dateAtMidnight(publishedDate) <= #createODBCDateTime(arguments.fromActivationDate)#
			</cfif>
			<!--- END 2013-05-28 STCR CASE 435368 --->
			<cfif structKeyExists(arguments,"courseID")>
				and courseID = #arguments.courseID#
			<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<cfelseif application.com.settings.getSetting("elearning.useTrainingProgram")
				and isdefined("session.selectedTrainingProgramID")
					and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
				and TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
			</cfif>
			<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

				<cfif len(arguments.solutionArea)>
					and solution_Area IN (<cf_queryparam
											value="#arguments.solutionArea#"
											cfsqltype="cf_sql_varchar"
											list="true"
											/>)
				</cfif>
				<cfif len(arguments.userLevel)>
					and Level IN (<cf_queryparam
											value="#arguments.userLevel#"
											cfsqltype="cf_sql_varchar"
											list="true"
											/>)
				</cfif>
				<cfif len(arguments.series)>
					and series IN (<cf_queryparam
											value="#arguments.series#"
											cfsqltype="cf_sql_varchar"
											list="true"
											/>)
				</cfif>

				<!--- START: 2011-09-07 PPB LID7679 modules now are country scoped --->
				<cfif useModuleCountryScoping>
					and exists (
						select 1 from trngModuleCourse tm    <!--- PJP 2012-07-11 Case 429052 changed table from trngModule to trngModuleCourse ---->
						where tm.courseId = viewCourseId
						and
						(
						<cfif showModulesScopedToAllCountries>
							countryId IS NULL 					<!--- assume NULL = all countries to save release effort --->
							or
						</cfif>
						#request.relayCurrentUser.countryId# = countryId
						or
						#request.relayCurrentUser.countryId# in (select CountryMemberID from CountryGroup where CountryGroupID = countryId)    <!--- this would probably be more efficient using a join rather than a subquery, but its much easier to read this way --->
						<cfif not showModulesScopedToAllCountries>
							and countryId <>  <cf_queryparam value="#countryIdForAllCountries#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfif>
						)
				)
				</cfif>
				<!--- END: 2011-09-07 PPB LID7679 --->


			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			order by #arguments.sortOrder#
		</cfquery>
		<cfreturn GetTrainingCoursesData>
	</cffunction>


	<!-------------------------------------------------------------------------------------
	NYB 2011-09-01 Demo project
	Used in elearningCoursesV3.cfm (now called elearningCourses2Level) in place of GetTrainingCoursesData as this returns
	modules the course contains, and those completed
	2012-04-18	WAB	Case 427634 fixed problems with this query
	------------------------------------------------------------------------------------->
	<cffunction access="public" name="GetTrainingCoursesDataV2" hint="Returns training course information">
		<cfargument name="sortOrder" required="no" default="sortOrder,Title_Of_Course">
		<cfargument name="visibleForPersonID" required="no"> <!--- NJH 2009-02-06 P-SNY047 --->
		<cfargument name="courseID" type="numeric" required="no"> <!--- NJH 2009-02-06 P-SNY047 --->
		<cfargument name="solutionArea" type="string" required="no" default=""> <!--- SKP 2010-09-01 LEN022 --->
		<cfargument name="userLevel" type="string" required="no" default=""> <!--- SKP 2010-09-01 LEN022 --->
		<cfargument name="series" type="string" required="no" default=""> <!--- SKP 2010-09-01 LEN022 --->
		<!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses --->
		<cfargument name="active" type="Boolean" required="no">
		<cfargument name="personid" type="numeric" required="no">
		<cfargument name="fromActivationDate" type="date" required="false" /><!--- 2013-05-28 STCR CASE 435368 --->
		<cfargument name="joinTrainingPaths" type="boolean" default="false" /><!--- NJH 2015/08/12 JIRA PROD2015-19 --->
		<cfargument name="ignoreAssignmentRules" type="boolean" default="false" /><!--- NJH 2015/08/12 JIRA PROD2015-19 --->
        <cfargument name="isMobile" type="boolean" default="#request.relaycurrentuser.BROWSER.isMobile#" /> <!--- 2015-11-04 DAN Case 446442 --->
		<cfargument name="showExpired" type="boolean" default="true" /><!--- IH 2015-11-24 BRD31 --->


		<cfscript>
			var GetTrainingCoursesData = "";
			var titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "title",baseTableAlias="v");
			var descriptionPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "description",baseTableAlias="v"); // STCR 2012-05-31 P-REL109 CASE 428567. Seems that getTrainingCoursesData was using phraseQuerySnippets for the title but not for the description, however the relaytag nowadays uses the description field on the listing page not just on the detail page, so we better translate it in the query rather than in the loop
		</cfscript>

		<cfif arguments.joinTrainingPaths>
			<cfset var trngPathTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingPath", phraseTextID = "title",baseTableAlias="tp")>
		</cfif>

		<!--- NJH 2009-03-06 P-SNY047 added a distinct to the query and added sortOrder as the first column to sort by
			2016/01/06	NJH	Jira BF-169 - filter out courses where modules are inactive if active is passed through
		--->
		<cfquery name="GetTrainingCoursesData" datasource="#application.siteDataSource#">
			<!--- NJH 2015/08/12 JIRA PROD2015-19 --->
			<cfif arguments.joinTrainingPaths>
			select courseData.*,tp.ID as TrainingPathId,#trngPathTitlePhraseQuerySnippets.select# as TrainingPath, tpc.ID as trainingPathCourseID from
				(
			</cfif>
			select v.courseid,v.course_code,#descriptionPhraseQuerySnippets.select# as description <!--- STCR 2012-05-31 P-REL109 CASE 428567. --->
				,v.solution_area,v.level,v.sortorder,v.publishedDate,v.series, v.mobileCompatible,#titlePhraseQuerySnippets.select# as Title_Of_Course
				<cfif structKeyExists(arguments,"personid")>
					,count(vtm.moduleid) as num_modules_in_course,count(vtcmc.moduleid) as num_modules_attempted
					,count(vtcmcC.moduleid) as num_modules_completed
				</cfif>
			from vTrngCourses  v with (nolock)
				left join vTrngModules vtm with (nolock) on v.courseid=vtm.courseid<cfif structKeyExists(arguments,"active")> and vtm.active=1</cfif>
				<cfif not arguments.showExpired>
					and (vtm.expiryDate is null or vtm.expiryDate > GETDATE())
				</cfif>
				<cfif structKeyExists(arguments,"personid")>
				left join vTrngCourseModuleCompleted vtcmc with (nolock) on vtm.courseid=vtcmc.courseid and vtm.moduleid=vtcmc.moduleid
					and vtcmc.personid=<cf_queryparam value="#arguments.personid#" CFSQLType="CF_SQL_INTEGER">
				left join vTrngCourseModuleCompleted vtcmcC with (nolock) on vtm.courseid=vtcmcC.courseid and vtm.moduleid=vtcmcC.moduleid
					and vtcmcC.usermodulefulfilled is not null
					and vtcmcC.personid=<cf_queryparam value="#arguments.personid#" CFSQLType="CF_SQL_INTEGER">
				</cfif>
				<cfif structKeyExists(arguments,"visibleForPersonID")>
                    left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer">) as rr on rr.recordid = v.courseId
				</cfif>
				#preservesinglequotes(titlePhraseQuerySnippets.join)#
				#preservesinglequotes(descriptionPhraseQuerySnippets.join)#  <!--- STCR 2012-05-31 P-REL109 CASE 428567. --->

			where 1=1
				<!--- NJH 2016/12/16 - JIRA PROD2016-2968 - apply the country rights
						WAB 2017-03-07 RT-309  Needed to add 0 to list, to deal with modules which are not country specific
				 --->
				and isNull(vtm.countryID,0) in (#listAppend(0,listAppend(request.relayCurrentUser.countryList,request.relayCurrentUser.regionListSomeRights))#)
            <cfif structKeyExists(arguments,"visibleForPersonID")>
                and (rr.level1 = 1 or ((v.hasRecordRightsBitMask & 1) = 0))
            </cfif>

            <!--- start: 2015-11-04 DAN Case 446442 --->
            <cfif arguments.isMobile>
                and v.mobileCompatible=1
            </cfif>
            <!--- end: 2015-11-04 DAN Case 446442 --->

			<cfif structKeyExists(arguments,"active")>
				and v.active =  <cf_queryparam value="#arguments.active#" CFSQLTYPE="cf_sql_bit" >
				and vtm.availability = <cf_queryparam value="#arguments.active#" CFSQLTYPE="cf_sql_bit" >
			</cfif>
			<!--- BEGIN 2013-05-28 STCR CASE 435368 - hide Courses before their Activation Date, unless previewing content for a specific date --->
			<cfif structKeyExists(arguments,"fromActivationDate")>
				and dbo.dateAtMidnight(v.publishedDate) <= #createODBCDateTime(arguments.fromActivationDate)#
			</cfif>
			<!--- END 2013-05-28 STCR CASE 435368 --->
			<cfif structKeyExists(arguments,"courseID")>
				and v.courseID = #arguments.courseID#
			</cfif>
			<cfif len(arguments.solutionArea)>
				and v.solution_Area IN (<cf_queryparam
										value="#arguments.solutionArea#"
										cfsqltype="cf_sql_varchar"
										list="true"
										/>)
			</cfif>
			<cfif len(arguments.userLevel)>
				and Level IN (<cf_queryparam
										value="#arguments.userLevel#"
										cfsqltype="cf_sql_varchar"
										list="true"
										/>)
			</cfif>
			<cfif len(arguments.series)>
				and series IN (<cf_queryparam
										value="#arguments.series#"
										cfsqltype="cf_sql_varchar"
										list="true"
										/>)
			</cfif>
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			group by v.courseid,v.title_of_course,v.course_code,#descriptionPhraseQuerySnippets.select#,v.sortorder,v.solution_area,v.level,v.publishedDate,v.series,v.mobileCompatible,#titlePhraseQuerySnippets.select# <!--- STCR 2012-05-31 P-REL109 CASE 428567. descriptionPhraseQuerySnippets --->
			<!--- NJH 2015/08/12 JIRA PROD2015-19 --->
			<cfif arguments.joinTrainingPaths>
			) as courseData

				inner join trngTrainingPathCourse tpc on courseData.courseID = tpc.courseID
				inner join trngTrainingPath tp on tpc.TrainingPathID = tp.ID and tp.active=1
				#preservesinglequotes(trngPathTitlePhraseQuerySnippets.join)#

				<cfif structKeyExists(arguments,"personid")>
					<cfset var recordRights = application.com.rights.getRecordRightsFilterQuerySnippet(alias="tp",personID=arguments.personID,entityType="trngTrainingPath")>
					<cfset var countryScope = application.com.rights.getCountryScopeFilterQuerySnippet(alias="tp",personID=arguments.personID,entityType="trngTrainingPath")>
					left outer join trngTrainingPathPerson tpp on tpp.TrainingPathID = tp.ID
					#preserveSingleQuotes(recordRights.join)#
					#preserveSingleQuotes(countryScope.join)#
				where tp.active = 1
					and (tpp.personID = <cf_queryparam value="#arguments.personid#" CFSQLType="CF_SQL_INTEGER"> OR tp.ignoreAssignmentRules = 1)
					#recordRights.whereClause#
					#countryScope.whereClause#
				</cfif>
			</cfif>
			order by #arguments.sortOrder#
		</cfquery>
		<cfreturn GetTrainingCoursesData>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Returns Module data
		3-Jul-08	SWJ	Modified sortOrder
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetTrainingModulesData" hint="Returns training module information">
			<cfargument name="sortOrder" required="no" default="sortOrder,Title_of_Module">
			<cfargument name="moduleID" type="numeric" required="no">
			<cfargument name="courseID" type="numeric" required="no"> <!--- NJH 2009-02-06 P-SNY047 --->
			<cfargument name="visibleForPersonID" type="numeric" required="no"> <!--- NJH 2009-02-06 P-SNY047 --->
			<cfargument name="availableModules" type="Boolean" required="no"> <!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
			<cfargument name="active" type="Boolean" required="no"> <!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses --->
			<cfargument name="personid" type="numeric" required="no">
			<cfargument name="useModuleCountryScoping" type="boolean" required="no" default="#application.com.settings.getSetting('elearning.useModuleCountryScoping')#"><!--- 2013-11-07 NYB Case 437616 added default based on setting ---><!--- 2011-09-06 PPB LID7679 done for lenovo so Japanese don't have to see non-Japanese All Countries modules --->
			<cfargument name="showModulesScopedToAllCountries" type="boolean" required="no" default="true">		<!--- 2011-09-06 PPB LID7679 done for lenovo so Japanese don't have to see non-Japanese All Countries modules --->
			<cfargument name="fromActivationDate" type="date" required="false" /><!--- 2013-04-23 STCR CASE 432193 --->
            <cfargument name="isMobile" type="boolean" default="#request.relaycurrentuser.BROWSER.isMobile#" /> <!--- 2015-11-04 DAN Case 446442 --->
			<cfargument name="showExpired" type="boolean" default="true" />

			<cfscript>
				var GetTrainingModulesData = "";
				var moduleTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModule", phraseTextID = "title");
				var courseTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "title");
				var courseTrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", phraseTableAlias = "trngCourseTrainingProgram", entityIDColumn="courseTrainingProgramID",nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'");
				var countryIdForAllCountries = application.com.commonQueries.getCountryIdFromDescription('All Countries');			// 2011-09-06 PPB LID7679
				// START: 2012-05-01 - Englex - P-REL109 - Add training program
				var TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'");
				// END: 2012-05-01 - Englex - P-REL109 - Add training program
			</cfscript>

			<!--- NJH 2009-03-06 P-SNY047 added a distinct to the query and added sortOrder as the first column to sort by --->
			<cfquery name="GetTrainingModulesData" datasource="#application.siteDataSource#">
				<!--- START 2010-10-06			NAS			LID4053: eLearning - Module Listing Additional Filters --->
				select * from
					(select distinct
					<!--- 2013-11-07 NYB Case 437616 changed viewCountryIDm to viewCountryID: --->
					v.Module_Type,v.Filename,v.Maximum_Score,v.Maximum_Time_Allowed, v.Module_Code, v.moduleid, v.courseid,v.duration,v.Associated_Quiz,v.SortOrder, v.fileid, v.points, v.active, v.availability, v.publishedDate, v.countryId AS viewCountryId, v.mobileCompatible   <!--- 2011-09-06 PPB LID7679 aliased as viewCountryId to avoid ambiguity --->
					<!--- --,case when qd.active=0 then 0 else v.QuizDetailID end as quizDetailID, --->
					,v.quizDetailID, isNull(qd.active,0) as quizActive,
					v.courseTrainingProgramID, v.isScorm,
					#moduleTitlePhraseQuerySnippets.select# as Title_Of_Module,
					#courseTitlePhraseQuerySnippets.select# as Title_Of_Course
					<cfif structKeyExists(arguments,"personid")>
						,case when tump.startedDateTime is null then 'initial' else case when tump.usermodulefulfilled is null then 'started' else 'completed' end end as module_completion_status
					</cfif>
					,trainingprogramID,2 as cmSort
					<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>,
					#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as Training_Program,
					#preserveSingleQuotes(courseTrainingProgramPhraseQuerySnippets.select)#  as Course_Training_Program
					</cfif>
					<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
											Return new field 'coursewareTypeID' relevant to getCourseLauncherURL function --->
					,coursewareTypeID
				from vTrngModules v with (nolock)
					left join quizDetail qd on v.quizDetailID = qd.quizDetailID
					<cfif structKeyExists(arguments,"personid")>
						left join trngUserModuleProgress tump with (nolock) on v.moduleid=tump.moduleid
						and tump.personid = <cf_queryparam value="#arguments.personid#" CFSQLType="CF_SQL_INTEGER">
					</cfif>
					<cfif structKeyExists(arguments,"visibleForPersonID")>
						<!--- NJH 2009-02-06 P-SNY047 if we're checking visibilty rights, have to go through course and module--->
                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer">) as rrc on rrc.recordid = v.courseID
                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer">) as rrm on rrm.recordid = v.moduleID
					</cfif>
						#replace(preservesinglequotes(moduleTitlePhraseQuerySnippets.join),"moduleID","v.moduleID")#
						#preservesinglequotes(courseTitlePhraseQuerySnippets.join)#
						<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
						#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
						#preservesinglequotes(courseTrainingProgramPhraseQuerySnippets.join)#
						</cfif>
						<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->

                    where 1=1

                    <cfif structKeyExists(arguments,"visibleForPersonID")>
                        and (rrc.level1 = 1 or ((v.courseHasRecordRightsBitMask & 1) = 0))
                        and (rrm.level1 = 1 or ((v.moduleHasRecordRightsBitMask & 1) = 0))
                    </cfif>
					<cfif not arguments.showExpired>
						and (v.expiryDate is null or v.expiryDate > GETDATE())
					</cfif>

					 <!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<cfif request.relayCurrentUser.isLoggedIn and request.relayCurrentUser.isInternal>
						UNION

						select distinct
						<!--- 2013-11-07 NYB Case 437616 changed viewCountryIDm to viewCountryID: --->
						vnc.Module_Type,vnc.Filename,vnc.Maximum_Score,vnc.Maximum_Time_Allowed, vnc.Module_Code, vnc.moduleid, vnc.courseid,vnc.duration,vnc.Associated_Quiz,vnc.SortOrder, vnc.fileid, vnc.points, vnc.active, vnc.availability, vnc.publishedDate, vnc.countryId AS viewCountryId, vnc.mobileCompatible   <!--- 2011-09-06 PPB LID7679 aliased as viewCountryId to avoid ambiguity --->
						<!--- --,case when qd.active=0 then 0 else vnc.QuizDetailID end as quizDetailID, --->
						,vnc.quizDetailID, isNull(qd.active,0) as quizActive,
						vnc.courseTrainingProgramID,vnc.isScorm,
						#moduleTitlePhraseQuerySnippets.select# as Title_Of_Module,
						'phr_elearning_ModuleNoCourse' as Title_Of_Course
						<cfif structKeyExists(arguments,"personid")>
							,case when tump.startedDateTime is null then 'initial' else case when tump.usermodulefulfilled is null then 'started' else 'completed' end end as module_completion_status
						</cfif>
						,trainingprogramID, 1 as cmSort
						<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>,
						#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as Training_Program,
						#preserveSingleQuotes(courseTrainingProgramPhraseQuerySnippets.select)#  as Course_Training_Program
						</cfif>
						<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
												Return new field 'coursewareTypeID' relevant to getCourseLauncherURL function --->
						,coursewareTypeID
					from vTrngModulesNoCourse vnc with (nolock)
						left join quizDetail qd on qd.quizDetailID = vnc.quizDetailID
						<cfif structKeyExists(arguments,"personid")>
							left join trngUserModuleProgress tump with (nolock) on vnc.moduleid=tump.moduleid
							and tump.personid = <cf_queryparam value="#arguments.personid#" CFSQLType="CF_SQL_INTEGER">
						</cfif>
						<cfif structKeyExists(arguments,"visibleForPersonID")>
							<!--- NJH 2009-02-06 P-SNY047 if we're checking visibilty rights, have to go through course and module--->
	                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer">) as rrc on rrc.recordid = vnc.courseID
	                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer">) as rrm on rrm.recordid = vnc.moduleID
						</cfif>
							#replace(preservesinglequotes(moduleTitlePhraseQuerySnippets.join),"moduleID","vnc.moduleID")#
							<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
							<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
							#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
							#preservesinglequotes(courseTrainingProgramPhraseQuerySnippets.join)#
							</cfif>
							<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->

	                    where 1=1

	                    <cfif structKeyExists(arguments,"visibleForPersonID")>
	                        and (rrc.level1 = 1 or ((vnc.courseHasRecordRightsBitMask & 1) = 0))
	                        and (rrm.level1 = 1 or ((vnc.moduleHasRecordRightsBitMask & 1) = 0))
	                    </cfif>

					</cfif>
					)
					as x
					<!--- STOP 2010-10-06			NAS			LID4053: eLearning - Module Listing Additional Filters --->
				where 1=1

                    <!--- start: 2015-11-04 DAN Case 446442 --->
                    <cfif arguments.isMobile>
                        and mobileCompatible=1
                    </cfif>
                    <!--- end: 2015-11-04 DAN Case 446442 --->

					<!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
					<cfif structKeyExists(arguments,"availableModules")>
						and availability =  <cf_queryparam value="#arguments.availableModules#" CFSQLTYPE="CF_SQL_bit" >
					</cfif>
					<!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses --->
					<cfif structKeyExists(arguments,"active")>
						and (active =  <cf_queryparam value="#arguments.active#" CFSQLTYPE="cf_sql_float" >
						<cfif request.relayCurrentUser.isLoggedIn and request.relayCurrentUser.isInternal>or active is null</cfif>)
					</cfif>
				<cfif structKeyExists(arguments,"moduleID")>
					and moduleID = #arguments.moduleID#
				<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
				<cfelseif application.com.settings.getSetting("elearning.useTrainingProgram")
					and isdefined("session.selectedTrainingProgramID") and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
					and TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
				</cfif>
				<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
				<!--- BEGIN 2013-04-23 STCR CASE 432193 - ignore Modules before their Activation Date, unless previewing content for a specific date --->
				<cfif structKeyExists(arguments,"fromActivationDate")>
					and dbo.dateAtMidnight(publishedDate) <= #createODBCDateTime(arguments.fromActivationDate)#
				</cfif>
				<!--- END 2013-04-23 STCR CASE 432193 --->
				<cfif structKeyExists(arguments,"courseID")>
					and courseID = #arguments.courseID#
				</cfif>
				<!--- START: 2011-09-07 PPB LID7679 modules now are country scoped --->
				<cfif useModuleCountryScoping>
					and
						(
						<cfif showModulesScopedToAllCountries>
							viewCountryId IS NULL 					<!--- assume NULL = all countries to save release effort --->
							or
						</cfif>
						#request.relayCurrentUser.countryId# = viewCountryId
						or
						#request.relayCurrentUser.countryId# in (select CountryMemberID from CountryGroup where CountryGroupID = viewCountryId)    <!--- this would probably be more efficient using a join rather than a subquery, but its much easier to read this way --->
						<cfif not showModulesScopedToAllCountries>
							and viewCountryId <>  <cf_queryparam value="#countryIdForAllCountries#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfif>
						)
				</cfif>
				<!--- END: 2011-09-07 PPB LID7679 --->

				<cfset useWholeWordsOnly="false"><!--- 2014/01/10			DXC			Case 441110  --->
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by cmSort, #arguments.sortOrder#
			</cfquery>

			<cfreturn GetTrainingModulesData>
		</cffunction>

		<cffunction access="public" name="GetCertificationSummary" hint="Returns training certification information">
			<cfargument name="sortOrder" required="no" default="">
			<cfargument name="active" type="Boolean" required="no">
			<cfargument name="incInactiveRules" type="Boolean" required="no" default="true"> <!--- 2013-09-11 NYB Case 436307 - added argument --->

			<cfset var GetCertificationSummary = "">
			<cfset var trainingProgram = application.com.settings.getSetting("elearning.useTrainingProgram")>
			<cfset var TrainingProgramPhraseQuerySnippets = "">
			<!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
			<cfset var ModuleSetPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModuleSet", phraseTextID = "description", baseTableAlias="cr") />

			<cfif trainingProgram>
				<cfset TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'", baseTableAlias="c")>
			</cfif>

			<cfquery name="GetCertificationSummary" datasource="#application.siteDataSource#">
				select distinct * from <!--- 2013-05-28 STCR added distinct. Otherwise duplicates returned when an associated courseware file has multiple countryScope rows. --->
					(select c.certificationID, c.Title_defaultTranslation as CertificationTitle,c.code as certificationCode,
						co.countryDescription as country, t.description as CertificationType,
						cr.moduleSetID, /*ms.description as ModuleSet,*/
						#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)# as ModuleSet, <!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
						m.moduleID, m.title_defaultTranslation as ModuleTitle,
						qd.QuizDetailID, qd.title_defaultTranslation as QuizTitle,
						f.fileID as courseWareID, f.name as CoursewareTitle, case when level2 = 1 or f.personID=#request.relayCurrentUser.personID# then 'link' else 'text' end as showCoursewareTitleAs,
						case when level1 = 1 and cg.countrygroupid is not null or (f.fileID is not null and not exists (select 1 from countryscope where entityId=f.fileID and entity='files')) or (f.fileId is not null and not exists (select 1 from vRecordRights where entity='files' and recordId=f.fileID)) then '<img src="/images/icons/report_go.png"/>' else '' end as downloadFile
						<cfif trainingProgram>
						,#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as TrainingProgram
						</cfif>
					from trngCertification as c with (noLock)
					inner join country co with (noLock) on co.countryID = c.countryID
					inner join trngCertificationType t with (noLock) on t.certificationTypeID = c.certificationTypeID
					left join trngCertificationRule as cr with (noLock) on c.certificationID = cr.certificationID <cfif not incInactiveRules>and cr.active=1</cfif><!--- 2013-09-11 NYB Case 436307 - added IF --->
					left join trngModuleSet as ms with (noLock) on cr.moduleSetID = ms.moduleSetID
					left join trngModuleSetModule as msm with (noLock) on ms.moduleSetID = msm.moduleSetID
					left join trngModule as m with (noLock) on msm.moduleID = m.moduleID
					left join files AS f with (noLock) on f.fileid = m.fileID
					left join QuizDetail as qd with (noLock) on m.quizDetailId = qd.quizDetailId
					left join vRecordRights rr with (noLock) on rr.recordID=f.fileID and rr.entity='Files' and rr.personID=#request.relayCurrentUser.personID#
					left join countryScope cs with (noLock) on f.fileid = cs.entityid and cs.entity='Files'
					left join countryGroup cg with (noLock) on cg.countrygroupid = cs.countryid and cg.countrymemberid  in ( <cf_queryparam value="#request.relaycurrentuser.content.showForCountryID#" CFSQLTYPE="cf_sql_integer"  list="true"> )
					#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)# <!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
					<cfif trainingProgram>
						#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
					</cfif>
					where 1 = 1
					#application.com.rights.getRightsFilterWhereClause(entityType="trngCertification",alias="c",regionList="#request.relayCurrentUser.regionListSomeRights#").whereClause#
					<cfif structKeyExists(arguments,"active")>
						and c.active = <cf_queryparam value="#arguments.active#" CFSQLTYPE="CF_SQL_BIT">
					</cfif>
					<cfif trainingProgram and structKeyExists(session,"selectedTrainingProgramID") and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
						and c.TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER">
					</cfif>
					) as x
				where 1 = 1
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				<cfif arguments.sortOrder is not "">order by #arguments.sortOrder#</cfif>
			</cfquery>
			<cfreturn GetCertificationSummary>
		</cffunction>

		<!--- WAB 2016-09-27 PROD206-2393 Fix performance issues - also implement modern code for getting rights
								Note, also change in getFiles.sql (added a name column)
		--->
		<cffunction access="public" name="GetCourseSummary" hint="Returns training course summary information">
			<cfargument name="sortOrder" required="no" default="">
			<cfargument name="active" type="Boolean" required="no">
			<cfargument name="showExpired" type="boolean" default="true" /><!--- IH 2015-11-24 BRD31 --->

			<cfset var GetCourseSummary = "">
			<cfset var trainingProgram = application.com.settings.getSetting("elearning.useTrainingProgram")>
			<cfset var TrainingProgramPhraseQuerySnippets = "">

			<cfif trainingProgram>
				<cfset TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'", baseTableAlias="c")>
			</cfif>

			<cfquery name="GetCourseSummary" datasource="#application.siteDataSource#">
				select * from
					(select distinct
						c.courseID
						, c.title_defaultTranslation as CourseTitle
						, c.active
						, c.mobileCompatible
						, c.AICCCourseID as courseCode
						, sa.solutionAreaName as SolutionArea
						, m.moduleID
						, m.title_defaultTranslation as ModuleTitle
						, qd.QuizDetailID
						, qd.title_defaultTranslation as QuizTitle
						, f.fileID as courseWareID
						, f.name as CoursewareTitle
						, case when hasEditRights = 1 then 'link' else 'text' end as showCoursewareTitleAs
						, case when hasViewRights = 1 then '<img src="/images/icons/report_go.png"/>' else '' end as downloadFile
						<cfif trainingProgram>
						,#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as TrainingProgram
						</cfif>
					from trngCourse as c with (noLock)
					left join trngSolutionArea sa with (noLock) on sa.solutionAreaID = c.solutionAreaID
					left join trngModuleCourse as mc with (noLock) on c.courseId = mc.courseID
					left join trngModule as m with (noLock) on m.moduleID = mc.moduleID
					<cfif not arguments.showExpired>
						and (m.expiryDate is null or m.expiryDate > GETDATE())
					</cfif>
					left join QuizDetail as qd with (noLock) on m.quizDetailID = qd.quizDetailID
					left join dbo.getFiles (<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">,<cf_queryparam value="#request.relaycurrentuser.content.showForCountryID#" CFSQLTYPE="cf_sql_integer">,1) f on m.fileid = f.fileid
					<cfif trainingProgram>
					#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
					</cfif>
					where 1 = 1
					<cfif structKeyExists(arguments,"active")>
						and c.active = <cf_queryparam value="#arguments.active#" CFSQLTYPE="CF_SQL_BIT">
					</cfif>
					<cfif trainingProgram and structKeyExists(session,"selectedTrainingProgramID") and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
						and c.TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER">
					</cfif>
					) as x
				where 1 = 1
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				<cfif arguments.sortOrder is not "">order by #arguments.sortOrder#</cfif>
			</cfquery>
			<cfreturn GetCourseSummary>
		</cffunction>

		<!--- 2012-04-03 STCR Incorporation of existing PS Code from MS --->
		<cffunction access="public" name="recordCourseClick" hint="Stores information in CourseCounts table about Clicks made on take course">
			<cfargument name="moduleID" type="numeric" required="true">

			<cfscript>
				var browserLanguage = "";
			</cfscript>

				<CFIF request.relaycurrentuser.isloggedin eq 0>
					<CFSET personID=404>
				<CFELSE>
					<CFSET personID=request.relaycurrentuser.PERSONID>
				</cfif>

				<!---2011/09/01	MS	LID7678 Get Browser Language --->
				<cfif listLen(HTTP_ACCEPT_LANGUAGE)>
					<CFSET browserLanguage = ListGetAt(HTTP_ACCEPT_LANGUAGE, 1)>
				<cfelse>
					<CFSET browserLanguage = 'Not Defined'>
				</cfif>

				<cfquery name="chkIfClickRecorded" datasource="#application.siteDataSource#">
					Select * from CourseCounts
					Where 	personid =  <cf_queryparam value="#personID#" CFSQLTYPE="cf_sql_integer" >
					and 	moduleLaunchedID=#arguments.moduleID#
					and 	visitid=#request.relaycurrentuser.VISITID#
				</cfquery>

				<CFIF chkIfClickRecorded.recordcount eq 0>
					<cfquery name="insertCourseCounts" datasource="#application.siteDataSource#">
						INSERT INTO CourseCounts
						(
						personID,
						ipAdd,
						moduleLaunchedID,
						visitId,
						sessionID,
						dateCreated,
						browserLanguage,
						fileID,
						languageID
						)
						VALUES
						(
						<cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#request.relaycurrentuser.IPADDRESS#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#arguments.moduleID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#request.relaycurrentuser.VISITID#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#session.sessionid#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						getdate(),
						<cf_queryparam value="#browserLanguage#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						null,
						null
						)
					</CFQUERY>
				</CFIF>
		</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	3-Jul-08	SWJ	Added this function that returns the student progress
	2014-05-15	WAB/DCC Improve performance of translation of this query.  Pre translate some fixed items and use cache for other items.
				Note that the moduleStatus translation now comes from phr_elearning_#statusTextID# rather than phr_#statusTextID#.  But I have checked that these phrases all match anyway.
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetStudentProgress" hint="Returns a query containing the progress of the current student for a give courseID">
			<cfargument name="personID" type="Numeric" required="true">
			<cfargument name="courseID" type="Numeric" required="true">
			<cfargument name="sortOrder" required="no" default="Title_of_Module">

			<cfscript>
				var GetStudentProgress = "";
				var courseTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "title", baseTableAlias="tc");		/* 2013-03-25 PPB Case 431081 */
				var moduleTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModule", phraseTextID = "title", baseTableAlias="tm");		/* 2013-03-25 PPB Case 431081 */
				var Associated_QuizPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "quizDetail", phraseTextID = "title", baseTableAlias="qd");    /* 2014-05-13 DCC Translation abstraction from Query */
				var Module_StatusPhraseQuerySnippets =  application.com.relaytranslations.getPhrasePointerQuerySnippetsForTextID(tablename = "trngusermodulestatus", alias = "tums", textIDColumn = "statusTextID", prefix = "elearning"); /* 2014-05-13 DCC Translation abstraction from Query */
				var preTranslate =  application.com.relayTranslations.translateListOfPhrases (listOfPhrases="reset,webinar,online").Phrases;
			</cfscript>

                     <!--- NYB 2008-10-24 P-SOP007 - changed query to use translated title --->
                     <cfquery name="GetStudentProgress" datasource="#application.siteDataSource#">
                      select * from (
                                  SELECT
							#courseTitlePhraseQuerySnippets.select# as Title_Of_Course,
							tm.AICCFileName AS FileName,
							#moduleTitlePhraseQuerySnippets.select# as Title_Of_Module,
							case when tm.AICCType = 'Webinar' Then '#preTranslate.Webinar.phraseText#' when tm.AICCType = 'Online' then '#preTranslate.online.phraseText#' else tm.AiccType end as Module_Type,
							tm.AICCMaxScore AS Maximum_Score, tm.AICCMaxTimeAllowed AS Maximum_Time_Allowed, tc.courseID,
							tm.moduleID, tm.moduleCode as Module_Code, tm.moduleDuration as Duration,
							--tcc.completedDate,
							tcc.userModuleProgressID,
							tcc.quizPassedDate,
							#Associated_QuizPhraseQuerySnippets.select# AS Associated_Quiz,
						 qd.QuizDetailID
						,tcc.personid as student_personID, tm.fileID
						 ,(select count(*) from quizTaken where personID =  #arguments.personID# and quizDetailID = qd.QuizDetailID) as quiz_taken,
						 qd.numberOfAttemptsAllowed,
						 tums.statusTextID as Module_StatusTextID,
						 #Module_StatusPhraseQuerySnippets.select# as Module_Status,
						 tcc.userModuleFulfilled as Module_Pass_Date,
						 '<img src="/images/icons/bullet_arrow_down.png">' as Show_quiz
						 ,'#preTranslate.reset.phraseText#' as Reset_quiz                                         <!--- 2013-07-25 PPB Case 436072 --->
						 ,IsNull((select top 1 quizTakenID from quizTaken with (noLock) where userModuleProgressID = tcc.userModuleProgressID order by lastUpdated desc),0) as lastQuizTakenID              <!--- 2013-07-25 PPB Case 436072 get the last quizAttempt resetQuiz mechanism --->

						FROM dbo.trngModule AS tm
						INNER JOIN trngModuleCourse AS tmc ON tm.moduleID = tmc.moduleID <!--- PJP 2012-07-11 Case 429052 added inner join to trngModuleCourse ---->
						INNER JOIN trngCourse AS tc ON tmc.courseID = tc.courseId
						LEFT OUTER JOIN QuizDetail qd ON tm.quizDetailID = qd.quizDetailID
						LEFT OUTER JOIN trngUserModuleProgress tcc ON tm.moduleID = tcc.moduleID
						LEFT OUTER JOIN trngUserModuleStatus tums on tcc.moduleStatus = tums.StatusID
						/* START 2013-03-25 PPB Case 431081 */
						#preservesinglequotes(courseTitlePhraseQuerySnippets.join)#
						#preservesinglequotes(moduleTitlePhraseQuerySnippets.join)#
						/* END 2013-03-25 PPB Case 431081 */
						/* START 2014-05-13 DCC Translation abstraction from Query */
						#preservesinglequotes(Associated_QuizPhraseQuerySnippets.join)#
						#preservesinglequotes(Module_StatusPhraseQuerySnippets.join)#
						/* END 2014-05-13 DCC Translation abstraction from Query */

					where <cfif arguments.courseid neq 0>tc.courseID = #arguments.courseID# and</cfif> personID=#arguments.personID#
				) as d
				order by #arguments.sortOrder#
			</cfquery>

			<cfreturn GetStudentProgress>
		</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	4-Jul-08	SWJ	Added function that returns courses the student has registered for certifications for
	2009-02-26 GCC obsolete now I think - was only called by relaytagtemplates\elearningStudentProgress which now reports on modules taken
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetStudentRegisteredCourses" hint="Returns a query containing the courses personid is registered for.">
			<cfargument name="personID" type="Numeric" required="true">
			<cfargument name="sortOrder" required="no" default="Title_of_Course">

			<cfscript>
				var GetStudentRegisteredCourses = "";
			</cfscript>

			<cfquery name="GetStudentRegisteredCourses" datasource="#application.siteDataSource#">
				SELECT <!--- NJH 2009-07-22 P-SNY047 - translating of courses tc.AICCCourseTitle --->
				'phr_title_TrngCourse_' + CONVERT(VarChar, tc.courseId) AS Title_of_Course, tc.courseID
					FROM trngCourse AS tc
					inner JOIN trngCourseCertification tcc ON tc.courseID = tcc.courseID
				where tcc.personID = #arguments.personID#
				order by #arguments.sortOrder#
			</cfquery>

			<cfreturn GetStudentRegisteredCourses>
		</cffunction>

	<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	4-Jul-08	SWJ	Added function that returns the current Certification Progress
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetStudentCertificationProgress" hint="Returns a query containing the courses personid is registered for.">
			<cfargument name="personID" type="Numeric" required="true">
			<cfargument name="sortOrder" required="no" default="Title_of_Course">

			<cfscript>
				var GetStudentCertificationProgress = "";
			</cfscript>

			<cfquery name="GetStudentCertificationProgress" datasource="#application.siteDataSource#">
				SELECT <!--- NJH 2009-07-22 P-SNY047 translation of courses tc.AICCCourseTitle --->
				'phr_title_TrngCourse_' + CONVERT(VarChar, tc.courseId) AS Title_of_Course, tc.courseID
					FROM trngCourse AS tc
					inner JOIN trngCourseCertification tcc ON tc.courseID = tcc.courseID
				where tcc.personID = #arguments.personID#
				order by #arguments.sortOrder#
			</cfquery>

			<cfreturn GetStudentCertificationProgress>
		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-08-20 NJH Function that returns module sets data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetModuleSetsData" hint="Returns module sets information" validvalues="true">
			<cfargument name="sortOrder" required="no" default="Description">
			<cfargument name="certificationRuleID" required="no">
			<cfargument name="certificationID" required="no" default="0">
			<cfargument name="ModuleSetID" required="no" default="0">
			<cfargument name="forDropDown" type="boolean" required="false" default="false" />

			<cfscript>
				var qryGetModuleSetsData = "";
				// START: 2012-05-01 - Englex - P-REL109 - Add training program
				var TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'");
				// END: 2012-05-01 - Englex - P-REL109 - Add training program
				// STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title
				var ModuleSetPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModuleSet", phraseTextID = "description");
			</cfscript>

			<!--- STCR 2013-04-12 CASE 432194 Added query for translating ModuleSet Titles for dropdown in certificationRules XMLEditor --->
			<cfif arguments.forDropDown and isNumeric(arguments.certificationID) and isDefined("arguments.certificationRuleID") and isNumeric(arguments.certificationRuleID)>
				<cfquery name="qryGetModuleSetsData" datasource="#application.siteDataSource#">
					select moduleSetID as value,
						#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)# as display
						from trngModuleSet
						#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)#
						where moduleSetID not in (select moduleSetID from trngCertificationRule where certificationID =  <cf_queryparam value="#arguments.certificationID#" CFSQLTYPE="cf_sql_integer" > )
					    and active = 1 			<!--- 2013-08-16 PPB Case 436307 (note: I'm deliberately not restricting to active on the "currently set moduleset" unioned query below) --->
					union
					select moduleSetID as value,
						#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)# as display
						from trngModuleSet
						#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)#
						where moduleSetID = (select moduleSetID from trngCertificationRule
							where certificationRuleID  =  <cf_queryparam value="#arguments.certificationRuleID#" CFSQLTYPE="cf_sql_integer" > )
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
							and isNull(trainingProgramID,0) = (select isNull(trainingprogramID,0) from trngCertification where CertificationID  =  <cf_queryparam value="#arguments.certificationID#" CFSQLTYPE="cf_sql_integer" > )		<!--- 2013-08-16 PPB Case 436307 added isNulls --->
						</cfif>
					 ORDER BY display			<!--- 2013-08-16 PPB Case 436307 --->
				</cfquery>
			<cfelse><!--- STCR 2013-04-12 CASE 432194 standard behaviour used for listings: --->
				<cfquery name="qryGetModuleSetsData" datasource="#application.siteDataSource#">
					select distinct moduleSetID, /*Description,*/ active,numModulesInModuleSet
					,#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)#  as Description <!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
					<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>,
					#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as Training_Program,
					trainingprogramID
					</cfif>
					<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
					from vTrngModuleSets
					#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)# <!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
					<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
					<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
						#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
					</cfif>
					<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
					where 1=1
					<cfif arguments.ModuleSetID neq 0>
						and modulesetID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.modulesetID#">
					</cfif>

					<cfif isDefined("arguments.certificationRuleID")>
					and certificationRuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationRuleID#">
					<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
					<cfelseif application.com.settings.getSetting("elearning.useTrainingProgram")
						and isdefined("session.selectedTrainingProgramID")
							and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
						and TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
					<cfelseif application.com.settings.getSetting("elearning.useTrainingProgram") and certificationID neq 0>
						<cfset cTrainingProgramID = application.com.relayelearning.getCertificationData(certificationID = #CertificationID#).trainingprogramID>
						<cfif cTrainingProgramID neq 0 and cTrainingProgramID neq "">
							and TrainingProgramID = <cf_queryparam value="#cTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
						</cfif>
					</cfif>
					<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

					<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
					order by #arguments.sortOrder#
				</cfquery>
			</cfif>
			<cfreturn qryGetModuleSetsData>
		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that returns certification data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetCertificationData" hint="Returns certification information">
			<cfargument name="sortOrder" required="no" default="Code,Description">
			<cfargument name="certificationID" required="no" type="numeric">
			<cfargument name="showActive" type="boolean" default="false">
			<cfargument name="certificationTypeID" type="numeric" required="false">
			<cfargument name="countryID" type="numeric" required="false">
			<cfargument name="countryIDList" type="string" required="false">
			<cfargument name="fromActivationDate" type="date" required="false" /><!--- 2013-05-28 STCR CASE 435369 --->

			<cfscript>
				var qryGetCertificationData = "";
				var certTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCertification", phraseTextID = "title");
				var certDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCertification", phraseTextID = "description");
				// START: 2012-05-01 - Englex - P-REL109 - Add training program
				var TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'");
				// END: 2012-05-01 - Englex - P-REL109 - Add training program
			</cfscript>

			<!--- NYB 2008-10-24 P-SOP07 - added , tc.SortOrder to query  --->
			<cfquery name="qryGetCertificationData" datasource="#application.siteDataSource#">
				select distinct certificationID, SortOrder, /*title, description, */
						#certTitlePhraseQuerySnippets.select# as title,
						#certDescPhraseQuerySnippets.select# as description,
						numRules,numRulesToPass,duration,code,v.countryID,country,active,numUpdateRules,certificationType
						<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>,
						#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as Training_Program,
						trainingprogramID
						</cfif>
					<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
					from vTrngCertifications v
							<cfif isDefined("arguments.countryID") or isDefined("arguments.countryIDList") >
							left join
							countryGroup cg on  cg.countrygroupid = v.countryid
							</cfif>
						#preservesinglequotes(certTitlePhraseQuerySnippets.join)#
						#preservesinglequotes(certDescPhraseQuerySnippets.join)#
						<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
							#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
						</cfif>
						<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
					where 1=1
					<cfif arguments.showActive>
						and active <> 0
					</cfif>
					<!--- BEGIN 2013-05-28 STCR CASE 435369 - Only Certifications with at least one Active Rule should be available for Registration on the portal --->
					<cfif structKeyExists(arguments,"fromActivationDate")>
						and dbo.dateAtMidnight(v.ruleActivationDate) <= #createODBCDateTime(arguments.fromActivationDate)#
					</cfif>
					<!--- END 2013-05-28 STCR CASE 435369 --->
					<cfif structKeyExists(arguments,"certificationID")>
						and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
					<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
					<cfelseif application.com.settings.getSetting("elearning.useTrainingProgram")
						and structKeyExists(session,"selectedTrainingProgramID")
								and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
						and v.TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
					</cfif>
					<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

					<cfif structKeyExists(arguments,"certificationTypeID")>
						and certificationTypeID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationTypeID#">
					</cfif>
					<cfif structKeyExists(arguments,"countryID")>
							and (cg.countryMemberid = #arguments.countryID# or cg.countryGroupid = #arguments.countryID#)
						</cfif>
						<cfif structKeyExists(arguments,"countryIDList")>
							and (cg.countrymemberID  in ( <cf_queryparam value="#arguments.countryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) or cg.countrygroupID  in ( <cf_queryparam value="#arguments.countryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
						</cfif>
		<!---
						<cfif isDefined("arguments.countryID")>
						and (countryID in (select countryMemberID from countryGroup with (noLock) where countryGroupID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#">)
							or countryID = #arguments.countryID#
							<!--- START: NYB 2009-01-28 P-SOP066 - added --->
							or (countryID in (select countryGroupID from countryGroup with (noLock) where
							countryMemberID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.countryID#">))
							<!--- END: NYB 2009-01-28 P-SOP066 - added --->
							)
					</cfif>
						<!--- SKP 2010-09-03 LEN022 --->
						<cfif isDefined("arguments.countryIDList")>
							and (countryID in (#arguments.countryIDList#)
								)
						</cfif>
		--->
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				<cfif arguments.sortOrder neq "">order by #arguments.sortOrder#</cfif>
			</cfquery>
			<cfreturn qryGetCertificationData>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2011-03-02 NYB Function that returns certification data for elearning/specialisationRules.cfm
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetAssignableCertifications" hint="Returns certification data for elearning/specialisationRules.cfm" validValues="true">
			<cfargument name="countryID" type="numeric" required="false">
			<cfargument name="specialisationID" required="no" type="numeric">
			<cfargument name="showActiveOnly" type="boolean" default="true">
			<cfargument name="currentValue" type="numeric" required="no">
			<cfargument name="returnType" type="string" default="query"><!--- accepts 'recordCount' --->

			<cfscript>
				var qryGetCertificationData = "";
				var certTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCertification", phraseTextID = "title");
			</cfscript>

			<!--- NYB 2008-10-24 P-SOP07 - added , tc.SortOrder to query  --->
			<cfquery name="qryGetCertificationData" datasource="#application.siteDataSource#">
				<cfif returnType eq "RecordCount">
					select count(*) as recordCount
				<cfelse>
					select *
				</cfif>
					from (
						select certificationID as value, #certTitlePhraseQuerySnippets.select# as display
						from trngCertification tc
						#preservesinglequotes(certTitlePhraseQuerySnippets.join)#
						where
						<!--- start AND condition --->
						(1=1
						<cfif arguments.showActiveOnly>
							and active=1
						</cfif>
						<cfif structKeyExists(arguments,"countryID")>
							and tc.countryid in (
								select CountryGroupID from countrygroup where countrygroupid=tc.countryid
								and countrymemberid in (
									select countrymemberid from countrygroup where countrygroupid=#arguments.countryID#
									)
							)
						</cfif>
						<!--- START: 2011-04-25 Englex P-REL109 - Change GetAssignableCertifications to filter by training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram") and structKeyExists(arguments,"specialisationID")>
							<cfset strainingprogramID = application.com.relayElearning.GetSpecialisationData(specialisationID=#arguments.specialisationID#).trainingprogramID>
							<cfif strainingprogramID neq 0 and strainingprogramID neq "">
								and tc.trainingprogramID = <cf_queryparam cfsqltype="cf_sql_integer" value="#strainingprogramID#">
							</cfif>
						</cfif>
						<!--- END: 2011-04-25 Englex P-REL109 - Change GetAssignableCertifications to filter by training program --->
						)
						<!--- end AND condition --->
						<!--- start OR condition --->
						<cfif structKeyExists(arguments,"specialisationID")>
							<!--- 2013-11-12 NYB Case 437478 replaced "=" with "in" : --->
							or certificationID in (select distinct certificationID from specialisationRule
								where specialisationID=#arguments.specialisationID#
							)
						</cfif>
						<!--- end OR condition --->
						<cfif structKeyExists(arguments,"currentValue")>
							union
							select distinct certificationID as value, #certTitlePhraseQuerySnippets.select# as display
							from vTrngCertifications v
							#preservesinglequotes(certTitlePhraseQuerySnippets.join)#
							where certificationID = #arguments.currentValue#
						</cfif>
						) as data
				<cfif returnType neq "recordCount">
					order by display
				</cfif>
			</cfquery>
			<cfif returnType eq "recordCount">
				<cfreturn qryGetCertificationData.recordCount>
			<cfelse>
				<cfreturn qryGetCertificationData>
			</cfif>
		</cffunction>


		<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2010-08-05 PPB Function that returns specialisation rule compulsory modules
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetSpecialisationRuleCompulsoryModules" hint="Returns the compulsory modules specified by a specialisation rule">
			<cfargument name="specialisationRuleID" required="yes" type="numeric">

			<cfset var qryGetSpecialisationRuleCompulsoryModules = "">

			<cfquery name="qryGetSpecialisationRuleCompulsoryModules" datasource="#application.siteDataSource#">
				<!--- returns value/display for moduleId/phrase for use by dropdown but is used elsewhere --->
				select srcm.moduleID as value, 'phr_title_TrngModule_' + CONVERT(VarChar, srcm.moduleID) as display
				from SpecialisationRuleCompulsoryModule srcm
				inner join trngModule m on m.moduleId = srcm.moduleId
				inner join specialisationRule sr on sr.specialisationRuleId = srcm.specialisationRuleId
				where srcm.specialisationRuleID = #specialisationRuleID#
				and sr.active <> 0
				order by modulecode
			</cfquery>

			<!--- because we're populating using ajax it's too late to rely on <cf_translate>, so translate the display immediately --->
			<cf_translateQueryColumn query = #qryGetSpecialisationRuleCompulsoryModules# columnName = 'display' onNullShowPhraseTextID = true>

			<cfreturn qryGetSpecialisationRuleCompulsoryModules>
		</cffunction>


		<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2010-08-13 PPB Function that returns the certificate modules that potentially could be compulsory modules for the specialisation rule
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetCertificationModules" hint="Returns the potential compulsory modules for a certification">
			<cfargument name="specialisationRuleID" type="numeric" required="true">
			<cfargument name="certificationID" type="numeric" required="true" >
			<cfargument name="originalCertificationID" type="numeric" required="true" >

			<cfset var qryCertificationModules = "">
			<cfset var modTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModule", phraseTextID = "title",baseTableAlias="m")>

			<!--- 2013-11-26	YMA	Case 438108 Display module code: module title in potential modules box. --->
			<cfquery name="qryCertificationModules" datasource="#application.siteDataSource#">
				SELECT 	distinct
						isnull(m.moduleCode,'')+': '+ CASE WHEN len(m.title_defaultTranslation) > 100
						THEN left(m.title_defaultTranslation,30) +  ' ... ' + right(m.title_defaultTranslation,30)
						ELSE m.title_defaultTranslation END AS display,
						m.moduleID AS value
				FROM         trngModuleSet INNER JOIN
				                      trngCertificationRule ON trngModuleSet.moduleSetID = trngCertificationRule.moduleSetID INNER JOIN
				                      trngModule AS m INNER JOIN
				                      trngModuleSetModule ON m.moduleID = trngModuleSetModule.moduleID ON
				                      trngModuleSet.moduleSetID = trngModuleSetModule.moduleSetID
				WHERE	trngCertificationRule.certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
				AND 	trngModuleSet.active <> 0
				AND 	m.availability <> 0
				<cfif originalCertificationID eq arguments.certificationID>
					AND	m.moduleID NOT IN ( SELECT moduleID FROM SpecialisationRuleCompulsoryModule WHERE SpecialisationRuleID = #arguments.specialisationRuleID# )
				</cfif>
			</cfquery>
		<!--- 	<!--- because we're populating using ajax it's too late to rely on <cf_translate>, so translate the display immediately --->
			<cf_translateQueryColumn query = #qryCertificationModules# columnName = 'display' onNullShowPhraseTextID = true>
 --->
 			<cfreturn qryCertificationModules>

		</cffunction>



		<!--- START: 2008-12-10 AJC Add ability to Activate a certification --->
		<cffunction access="public" name="activateCertification" hint="Activates a certification">
			<cfargument name="certificationID" required="yes" type="numeric">

			<cfscript>
				var qryactivateCertification = "";
				var argumentsStruct = structNew();
			</cfscript>

			<cftransaction action = "begin">
				<cftry>

					<cfquery name="qryactivateCertification" datasource="#application.siteDataSource#">
						if exists (select 1 from trngCertification where certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
							and active = 0)
							begin
								update trngCertification set active = 1
								where certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">

								select @@rowCount as rowUpdated
							end
						else
							begin
								select 0 as rowUpdated
							end
					</cfquery>

					<cftransaction action="commit"/>

					<cfcatch type="Database">
						<cftransaction action="rollback"/>
					</cfcatch>
				</cftry>
			</cftransaction>

		</cffunction>
		<!--- END: 2008-12-10 AJC Add ability to Activate a certification --->

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-09 NJH Function that makes a certification inactive
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="InactivateCertification" hint="Inactivates a certification">
			<cfargument name="certificationID" required="yes" type="numeric">
			<cfargument name="updatePersonCertifications" type="boolean" default="true">
			<cfargument name="sendNotificationEmail" type="boolean" default="#application.com.settings.getSetting('elearning.sendNotificationEmail')#">		<!--- 2013-02-08 PPB Case 433337 default to setting --->

			<cfscript>
				var qryInactivateCertification = "";
				var argumentsStruct = structNew();
				var getPeopleWithValidCertifications = "";
			</cfscript>

			<!--- WAB 2017-01-12 RT-138 This process takes a very long time if there are a large number of items to expire!
					It is in a transaction which means that it can block the whole db
					So I have removed it from a transaction.
					Alter so process is fail-safe.  If there is a failure then the certification will remain active, but some people's certifications will have been deactivated.
					Assuming that this process is triggered from a housekeeping task then remaining people will be picked up on the next run
					I have added code to extend the request timeout as we go along

					(Ideally we would do the update in a single query and add the emails to be sent into an email queue - we would then be able to do the whole thing in a transaction)
			--->
					<cfquery name="qryCheckActive" datasource="#application.siteDataSource#">
						select 1 from trngCertification
								where certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
							and active = 1
					</cfquery>

					<cfif qryCheckActive.recordCount gt 0>
						<cfif arguments.updatePersonCertifications >
						<cfset argumentsStruct.frmExpireDate = request.requestTime>
						<!--- NYB 2008-11-06 P-SOP009 --->
						<cfset argumentsStruct.certificationID = certificationID>

						<cfquery name="getPeopleWithValidCertifications" datasource="#application.siteDataSource#">
							select distinct personCertificationID from trngPersonCertification where certificationID = #arguments.certificationID#
								and statusID in (select statusID from trngPersonCertificationStatus where statusTextID
								<!--- START: NYB 2009-09-18 - LHID2578 ---
								in ('Pending','Registered','Passed')
								!--- WITH (LHID2578) --->
								NOT in ('Expired','Cancelled')
								<!--- END: NYB 2009-09-18 - LHID2578 --->
								)
						</cfquery>

						<cfloop query="getPeopleWithValidCertifications">
								<cfset updatePersonCertificationData(personCertificationID=personCertificationID,argumentsStruct=argumentsStruct,sendNotificationEmail=sendNotificationEmail)>
								<cfif currentRow mod 100 is 0>
									<cfset application.com.request.extendTimeOutIfNecessary (60)>
								</cfif>
						</cfloop>

					</cfif>

						<cfquery name="qryInactivateCertification" datasource="#application.siteDataSource#">
							update trngCertification
								set active = 0,
									lastupdated = getdate(),
									lastUpdatedBy = #request.relayCurrentUser.userGroupID#
							where certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
						</cfquery>

					</cfif>


		</cffunction>



		<cffunction access="public" name="GetCertificationRuleData" hint="Returns certification rule information">
			<cfargument name="sortOrder" required="no" default="certificationID">
			<cfargument name="certificationRuleID" required="no" type="numeric">
			<cfargument name="certificationID" required="no" type="numeric">
			<cfargument name="activeRules" required="no" type="boolean">
			<cfargument name="activeModuleSets" required="no" type="boolean">

			<cfscript>
				var qryGetCertificationRuleData = "";
				var ModuleSetPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModuleSet", phraseTextID = "description", baseTableAlias="vtcr"); // STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title
			</cfscript>

			<cfquery name="qryGetCertificationRuleData" datasource="#application.siteDataSource#">
				select vtcr.certificationID, vtcr.certificationRuleID, vtcr.numCredits, vtcr.description AS description_defaultTranslation,
					vtcr.moduleSetID, vtcr.numModulesToPass, vtcr.ruleActive, vtcr.moduleSetActive, vtcr.studyPeriod,
					vtcr.certificationRuleTypeID, vtcr.certificationRuleType, vtcr.activationDate, vtcr.certificationRuleOrder,
					#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)# as description <!--- STCR 2013-04-12 CASE 432194 Translation of ModuleSet Title --->
				from vTrngCertificationRules vtcr
				#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)# <!--- STCR 2013-04-12 CASE 432194 Translation of ModuleSet Title --->
				where 1=1
				<cfif isDefined("arguments.certificationRuleID")>
				and certificationRuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationRuleID#">
				</cfif>
				<cfif isDefined("arguments.certificationID")>
				and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
				</cfif>
				<cfif isDefined("arguments.activeRules") and arguments.activeRules>
				and ruleActive <> 0
				</cfif>
				<cfif isDefined("arguments.activeModuleSets") and arguments.activeModuleSets>
				and moduleSetActive <> 0
				</cfif>
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by #arguments.sortOrder#
			</cfquery>
			<cfreturn qryGetCertificationRuleData>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that deletes certification rule data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="deleteCertificationRule" hint="Marks a certification rule as inactive">
			<cfargument name="certificationRuleID" required="yes">

			<cfscript>
				var qryDeleteCertificationRule = "";
			</cfscript>

			<cfquery name="qryDeleteCertificationRule" datasource="#application.siteDataSource#">
				update trngCertificationRule set active = 0 where certificationRuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationRuleID#">
			</cfquery>

		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that updates certification rule data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="updateCertificationRule" hint="Updates a certification rule">
			<cfargument name="certificationRuleID" required="yes">
			<cfargument name="numModulesToPass" required="no" type="numeric">
			<cfargument name="moduleSetID" required="no" type="numeric">

			<cfscript>
				var qryUpdateCertificationRule = "";
			</cfscript>

			<cfquery name="qryUpdateCertificationRule" datasource="#application.siteDataSource#">
				update trngCertificationRule set
					lastUpdated = <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
					lastUpdatedBy = <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">
					<cfif isDefined("arguments.numModulesToPass")>, numModulesToPass = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.numModulesToPass#"></cfif>
					<cfif isDefined("arguments.moduleSetID")>, moduleSetID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleSetID#"></cfif>
				where certificationRuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationRuleID#">
			</cfquery>

		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that adds certification rule data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="insertCertificationRule" hint="Add a certification rule to a certification">
			<cfargument name="certificationID" required="yes" type="numeric">
			<cfargument name="numModulesToPass" required="yes" type="numeric">
			<cfargument name="moduleSetID" required="yes" type="numeric">
			<cfargument name="studyPeriod" required="no" type="numeric">
			<cfargument name="certificationRuleTypeID" required="yes" type="numeric">
			<cfargument name="activationDate" required="no" type="date" default="#request.requestTimeODBC#">
			<cfargument name="certificationRuleOrder" required="no" type="numeric">

			<cfscript>
				var qryAddCertificationRule = "";
			</cfscript>

			<cfquery name="qryAddCertificationRule" datasource="#application.siteDataSource#">
				if not exists (select 1 from trngCertificationRule
					where certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
						and moduleSetID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleSetID#">
				)
				insert into trngCertificationRule
					(certificationID,numModulesToPass,moduleSetID,studyPeriod,certificationRuleTypeID,activationDate,created,createdBy,lastUpdated,lastUpdatedBy<cfif structKeyExists(arguments,"certificationRuleOrder")  and arguments.certificationRuleOrder neq 0>,certificationRuleOrder</cfif> )
				values
					( <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.numModulesToPass#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleSetID#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.studyPeriod#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationRuleTypeID#">,
					  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#arguments.activationDate#">,
					  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
					  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">
					  <cfif structKeyExists(arguments,"certificationRuleOrder") and arguments.certificationRuleOrder neq 0>, <cf_queryparam cfsqltype="cf_sql_integer" value="#certificationRuleOrder#"></cfif>
					)
			</cfquery>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that certifies people
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="passValidPersonCertifications" hint="Sets the certification pass data for people who are eligible for certification">
			<cfargument name="personID" required="no" type="numeric">
			<cfargument name="certificationID" required="no" type="numeric">
			<cfargument name="numberToProcess" required="no" type="numeric" default="100">
			<cfargument name="viewName" required="no" type="string" default="vTrngPeopleEligibleForCertificationByModules">  <!--- certifications can be passed by number of modules passed or number of credits passed --->
			<cfargument name="passDate" required="no" type="date" default="#request.requestTime#"> <!--- NYB 2009-06-04 SOP010 - added --->
			<cfargument name="userModuleProgressID" required="no"> <!--- PYW 2016-01-04 P-TAT006 BRD 31 --->

			<cfscript>
				var qryGetPeopleWithCertificationsToPass = "";
				var argumentsStruct = structNew();
				var queryResult = false;
			</cfscript>

			<!--- PYW 2016-01-04 P-TAT006 BRD 31. Check for module with Retest rule --->
			<cfif StructKeyExists(arguments,"personID") and StructKeyExists(arguments,"userModuleProgressID")>
				<cfquery name="qryGetPeopleWithCertificationsToPass" datasource="#application.siteDataSource#">
					select personCertificationID, max(userModuleFulfilled) as maxModuleFulfilledDate
					from vTrngPersonCertifications with (nolock)
					where personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
					and userModuleProgressID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.userModuleProgressID#">
					and certificationRuleType = 'Retest'
					and personCertificationStatus = 'Passed'
                    group by personCertificationID
				</cfquery>

				<cfif qryGetPeopleWithCertificationsToPass.RecordCount neq 0>
					<cfset queryResult = true>
				</cfif>
			</cfif>

			<cfif not queryResult>
				<!---
					If there are people that have a registered status...
						Update the certification pass date for those that are eligible.
						Update the sysAccessDate for all records to show that we've looked at this set of records.
				--->
				<!--- GCC 2015-01-29 added nolock
					NJh 2017/02/28	JIRA PROD2016-3498 - filter out records where the date is empty --->
				<cfquery name="qryGetPeopleWithCertificationsToPass" datasource="#application.siteDataSource#">
					select distinct top #arguments.numberToProcess# personCertificationID, maxModuleFulfilledDate
					from #arguments.viewName# v with(nolock)
					where maxModuleFulfilledDate is not null
						<cfif isDefined("arguments.personID")>
							and personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
						</cfif>
						<cfif isDefined("arguments.certificationID")>
							and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
						</cfif>
				</cfquery>
			</cfif>

			<!--- NYB 2009-06-04 SOP010 - changed request.requestTime to passDate --->
			<cfset argumentsStruct.frmPassDate = passDate>
			<cfset argumentsStruct.frmSysAccessDate = request.requestTime>

			<cfloop query="qryGetPeopleWithCertificationsToPass">
                <cfset argumentsStruct.frmPassDate = qryGetPeopleWithCertificationsToPass.maxModuleFulfilledDate>

				<cfscript>
					application.com.relayElearning.updatePersonCertificationData(personCertificationID=personCertificationID,argumentsStruct=argumentsStruct);
					setPersonCertificationExpiryDate(personCertificationID);
				</cfscript>
			</cfloop>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				2015-12-22 PYW Function that sets the expiry date for a person's certification
	 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="setPersonCertificationExpiryDate" hint="Sets the expiry date for a person's certification when certification has a validity set" returnType="void">
			<cfargument name="personCertificationID" required="yes" type="numeric">

			<cfset var local = {}>

			<cfquery name="local.qryGetPersonCertification" datasource="#application.siteDataSource#">
				SELECT
					validity
				FROM
					vTrngPersonCertifications with (nolock)
				WHERE
					personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
			</cfquery>

			<cfif IsNumeric(local.qryGetPersonCertification.validity) AND local.qryGetPersonCertification.validity NEQ 0><!--- DCC case 452310 (defaulted to month hence) Yan request PROD2016-2606 don't default validity if set to 0 --->
                <!--- 2016-02-17 YMA    Get the date the last module was passed.  --->
                <cfquery name="local.qryGetExpiryDateFromMaxUserModuleFullfilled" datasource="#application.siteDataSource#">
                    select
                        DATEADD(m,
                           <cf_queryparam cfsqltype="cf_sql_integer" value="#local.qryGetPersonCertification.validity#">,
                            max(userModuleFulfilled)
                        ) as expiryDate
                    from vTrngPersonCertifications
                    where personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
                        and userModuleStatus = 'Passed'
                </cfquery>

                <cfif local.qryGetExpiryDateFromMaxUserModuleFullfilled.recordCount GT 0>
                    <cfset local.expiryDate = local.qryGetExpiryDateFromMaxUserModuleFullfilled.expiryDate>
                <cfelse>
                    <cfset local.expiryDateTime = DateAdd("m",local.qryGetPersonCertification.validity,now())>
                    <cfset local.expiryDate = CreateDate(DatePart("yyyy",local.expiryDateTime),DatePart("m",local.expiryDateTime),DatePart("d",local.expiryDateTime))>
                </cfif>


				<cfquery datasource="#application.siteDataSource#">
					UPDATE
						trngPersonCertification
					SET
						expiryDate = <cf_queryparam cfsqltype="cf_sql_timestamp" value="#local.expiryDate#">
					WHERE
						personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
				</cfquery>
			</cfif>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			2015-12-22 PYW Function that gets the merge fields for a person's certification
	 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getTrngPersonCertificationMergeFields" returnType="struct">
			<cfargument name="personCertificationID" required="yes" type="numeric">

			<cfscript>
				var qPersonCertification = GetPersonCertificationData(personCertificationID=arguments.personCertificationID,returnPersonData=true);
				var personCertificationMergeFields = {};

				personCertificationMergeFields.personCertificationID = qPersonCertification.personCertificationID;
				personCertificationMergeFields.firstname = qPersonCertification.firstname;
				personCertificationMergeFields.lastname = qPersonCertification.lastname;
				personCertificationMergeFields.certificationID = qPersonCertification.certificationID;
				personCertificationMergeFields.certificationTitle = qPersonCertification.title;
				personCertificationMergeFields.certificationCode = qPersonCertification.certificationCode;
				personCertificationMergeFields.status = qPersonCertification.status;
				personCertificationMergeFields.registrationDate = qPersonCertification.registration_Date;
				personCertificationMergeFields.passDate = qPersonCertification.pass_Date;
				personCertificationMergeFields.expiredDate = qPersonCertification.expired_Date;
				personCertificationMergeFields.expiryDate = qPersonCertification.expiry_date;
				personCertificationMergeFields.personCertificationExpiryDate = qPersonCertification.personCertificationExpiryDate;
				personCertificationMergeFields.cancellationDate = qPersonCertification.cancellation_date;

				return personCertificationMergeFields;
			</cfscript>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that de-certifies people
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="expireInvalidPersonCertifications" hint="Sets the expired date for people whose certifications are set to expire">
			<cfargument name="personID" required="no" type="numeric">
			<cfargument name="certificationID" required="no" type="numeric">
			<cfargument name="numberToProcess" required="no" type="numeric" default="100">

			<cfscript>
				var qryGetPeopleWithCertificationsToExpire = "";
				var argumentsStruct = structNew();
			</cfscript>

			<!--- GCC 2015-01-29 added nolock --->
			<cfquery name="qryGetPeopleWithCertificationsToExpire" datasource="#application.siteDataSource#">
					select distinct top #arguments.numberToProcess# personCertificationID, expiryReason
					from vTrngPeopleWithCertificationsDueForExpiry v with(nolock)
					where 1=1
						<cfif isDefined("arguments.personID")>
							and personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
						</cfif>
						<cfif isDefined("arguments.certificationID")>
							and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
						</cfif>
				</cfquery>

				<cfset argumentsStruct.frmExpireDate = request.requestTime>
				<cfset argumentsStruct.frmSysAccessDate = request.requestTime>

				<cfloop query="qryGetPeopleWithCertificationsToExpire">
					<cfscript>
						application.com.relayElearning.updatePersonCertificationData(personCertificationID=personCertificationID,argumentsStruct=argumentsStruct,sendNotificationEmail=true,notificationEmailTextID="Trng#expiryReason#");
					</cfscript>
				</cfloop>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-08-20 NJH Function that returns person certification data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetPersonCertificationData" hint="Returns person certification information">
			<cfargument name="sortOrder" required="no" default="Description">
			<cfargument name="entityID" required="no" type="numeric">
			<cfargument name="entityTypeID" required="no" type="numeric">
			<cfargument name="certificationID" required="no" type="numeric">
			<cfargument name="returnOrgData" required="no" type="boolean" default="false">
			<cfargument name="returnPersonData" required="no" type="boolean" default="false">
			<cfargument name="personCertificationID" required="no" type="numeric">

			<cfscript>
				var qryGetPersonCertificationData = "";
			</cfscript>

			<!--- NYB 2008-11-07 P-SOP009 - added title to query--->
			<cfquery name="qryGetPersonCertificationData" datasource="#application.siteDataSource#">
				select * from (
					select distinct personCertificationID,v.personID,certificationID,personCertificationRegistrationDate as registration_Date,certificationCode as code,
					certificationDescription as description, certificationTitle AS title,personCertificationPassDate as pass_Date,personCertificationExpiredDate as expired_Date,
						personCertificationStatus as status,expiryDate as expiry_date, personCertificationCancellationDate as cancellation_date,
						<!--- 2015-12-23 PYW P-TAT005 BRD 31. Add personCertificationExpiryDate --->
						personCertificationExpiryDate, personCertificationStatusTextID as statusTextID
					<cfif arguments.returnOrgData>
						,o.organisationName as organisation_name
					</cfif>
					<cfif arguments.returnPersonData>
						,p.firstname, p.lastname, p.firstName + ' ' +p.lastname as fullname
					</cfif>
					from vTrngPersonCertifications v
					<cfif arguments.returnOrgData or arguments.returnPersonData>
						inner join person p with (noLock) on p.personID = v.personID
						<cfif arguments.returnOrgData>
							inner join organisation o with (noLock) on o.organisationID = p.organisationID
						</cfif>
					</cfif>
					where 1=1
					<cfif isDefined("arguments.entityID") and isDefined("arguments.entityTypeID")>
						<cfif arguments.entityTypeID eq 0>
							and v.personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
						<cfelseif arguments.entityTypeID eq 2 and (arguments.returnOrgData or arguments.returnPersonData)>
							and p.organisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
						</cfif>
					</cfif>
					<cfif isDefined("arguments.certificationID")>
						and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
					</cfif>
					<cfif isDefined("arguments.personCertificationID")>
						and personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
					</cfif>
				) as base where 1=1
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by #arguments.sortOrder#
			</cfquery>
			<cfreturn qryGetPersonCertificationData>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-08-20 NJH Function that returns certification summary data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetCertificationSummaryData" hint="Returns person certification summary information">
			<cfargument name="sortOrder" required="no" default="Description">
			<cfargument name="entityID" required="no" type="numeric">
			<cfargument name="entityTypeID" required="no" type="numeric">
			<cfargument name="certificationID" required="no" type="numeric">
			<cfargument name="certificationTypeID" required="no" type="numeric">
			<cfargument name="returnOrgData" required="no" type="boolean" default="false">
			<cfargument name="returnPersonData" required="no" type="boolean" default="false">
			<cfargument name="restrictToUserCountryList" required="no" type="boolean" default="true">
			<cfargument name="validCertificationsOnly" required="no" type="boolean" default="false">
			<cfargument name="useMyTeams" required="no" type="boolean" default="false">
			<cfargument name="personCertificationID" required="no" type="numeric"> <!--- NJH 2012/11/22 Case 431933 --->
			<cfargument name="Sort" required="no" type="boolean" default="true">
			<cfargument name="statustextID" required="no" type="string" default="">

			<cfscript>
				var qryGetCertificationSummaryData = "";
				var titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCertification", phraseTextID = "title",baseTableAlias="v",phraseTableAlias="titleTable");
				var descPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCertification", phraseTextID = "description",baseTableAlias="v",phraseTableAlias="descTable");
				var statusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippetsForTextID(tablename="trngPersonCertificationStatus",alias="v",textIDColumn="statustextid",Prefix="certification");
			</cfscript>
			<!--- 2013-05-21 NYB CASE 434909 added var descPhraseQuerySnippets (above) --->

			<cfquery name="qryGetCertificationSummaryData" datasource="#application.siteDataSource#">
				select * from (
				select
					<!--- NYB 2008-11-10  P-SOP010 - replaced -> ---
					code,description,registration_Date,pass_date,expiry_date,status,num_modules_to_pass,Modules_Passed,
						v.personID,expired_date,certificationID,personCertificationID
					--- <- P-SOP010 - with -> --->
					v.*
					,#titlePhraseQuerySnippets.select# as TitleTranslated <!--- STCR 2012-05-31 P-REL109 CASE 428567. Seems that this was never updated to use phraseQuerySnippets. Will use an additional column alias for now to avoid breaking anything else that might depend upon existing title column. --->
					,#descPhraseQuerySnippets.select# as DescTranslated <!--- 2013-05-21 NYB CASE 434909 --->
					,#statusPhraseQuerySnippets.select# as StatusTranslated <!--- 2014-02-26 NYB Case 438849 added --->
					<!--- <- P-SOP010 --->
					<cfif arguments.returnOrgData>
						,o.organisationName as organisation_name,c.countryDescription as country
					</cfif>
					<cfif arguments.returnPersonData>
						,p.firstname, p.lastname, p.firstName + ' ' +p.lastname as fullname
					</cfif>
					<cfif arguments.useMyTeams>
						,teamID
					</cfif>
					from vTrngCertificationSummary v with(nolock)
					#preservesinglequotes(titlePhraseQuerySnippets.join)# <!--- STCR 2012-05-31 P-REL109 CASE 428567 --->
					#preservesinglequotes(descPhraseQuerySnippets.join)# <!--- 2013-05-21 NYB CASE 434909 --->
					#preservesinglequotes(statusPhraseQuerySnippets.join)# <!--- 2014-02-26 NYB Case 438849 added --->
					<cfif arguments.returnOrgData or arguments.returnPersonData or arguments.useMyTeams>
						inner join person p with (noLock) on p.personID = v.personID
						<cfif arguments.useMyTeams>
						inner join MyTeamMember mtm with (noLock) ON p.personid = mtm.personid
						inner join MyTeam mt with (noLock) ON mtm.TeamMemberID = mt.Teamid
						</cfif>
						<cfif arguments.returnOrgData>
							inner join organisation o with (noLock) on o.organisationID = p.organisationID
							inner join country c with (noLock) on o.countryID = c.countryID
							<cfif arguments.restrictToUserCountryList>
								<!--- 2012-07-25 PPB P-SMA001 commented out
								and c.countryID in (#request.relayCurrentUser.countryList#,#request.relayCurrentUser.regionListSomeRights#)
								 --->
								#application.com.rights.getRightsFilterWhereClause(entityType="trngCertification",alias="v",regionList=request.relayCurrentUser.regionListSomeRights).whereClause#		<!--- 2012-07-25 PPB P-SMA001 --->
							</cfif>
						</cfif>
					</cfif>
					where 1=1
 					<cfif structKeyExists(arguments,"entityID") and structKeyExists(arguments,"entityTypeID")>
						<cfif arguments.entityTypeID eq 0>
							and v.personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
						<cfelseif arguments.entityTypeID eq 2 and (arguments.returnOrgData or arguments.returnPersonData)>
							and p.organisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
						</cfif>
					</cfif>
					<cfif structKeyExists(arguments,"certificationID")>
						and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
					</cfif>
					<cfif structKeyExists(arguments,"certificationTypeID")>
						and certificationTypeID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationTypeID#">
					</cfif>
					<cfif structKeyExists(arguments,"personCertificationID")>
						and personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
					</cfif>
					<!--- NJH 2009-08-28 LID 2579 - changed from status to statusTextID --->
					<cfif arguments.validCertificationsOnly>
						and statusTextID in ('Pending','Registered','Passed')
					</cfif>
					<!--- START:  2012/07/18 - Englex - P-REL109 Phase 2 - Added filter by statustextID to GetCertificationSummaryData --->

					<cfif arguments.statustextID neq "">
						and statusTextID = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.statustextID#">
					</cfif>
					<!--- END:  2012/07/18 - Englex - P-REL109 Phase 2 - Added filter by statustextID  to GetCertificationSummaryData --->

					<cfif arguments.useMyTeams>
						and mt.createdBy = #request.relayCurrentUser.personID#
					</cfif>
				) as base where 1=1
				<!--- NYB 2008-11-11 P-SOP010 - added if: --->
				<cfif arguments.Sort>
					<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
					order by #arguments.sortOrder#
				</cfif>
			</cfquery>
			<cfreturn qryGetCertificationSummaryData>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-08-26 NJH Function that adds a new entry into the trngPersonCertification table
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="insertPersonCertificationData" hint="Adds a person certification record">
			<cfargument name="personID" required="no" type="numeric" default="#request.relayCurrentUser.personID#">
			<cfargument name="certificationID" required="yes" type="numeric">
			<cfargument name="registerDate" required="no" type="date" default="#request.requestTime#">
			<cfargument name="sendNotificationEmail" required="no" type="boolean" default="#application.com.settings.getSetting('elearning.sendNotificationEmail')#">		<!--- 2013-02-08 PPB Case 433337 default to setting --->

			<cfscript>
				var qryInsertPersonCertificationData = "";
				var statusID = application.com.relayElearning.getPersonCertificationStatus(statusTextID='Registered').statusID;
				var getCertificationDetails = "";
			</cfscript>

			<!--- START: NYB 2009-02-26 Sophos Stargate 2 - added because there's no way built in set notificationEmailTextID to true --->
			<!--- <cfif isDefined("request.sendNotificationEmail")>
				<cfset arguments.sendNotificationEmail = request.sendNotificationEmail>
			</cfif> --->
			<!--- <cfset arguments.sendNotificationEmail = application.com.settings.getSetting("elearning.sendNotificationEmail")> --->				<!--- 2013-02-08 PPB Case 433337 don't stomp over argument sent in with setting --->
			<!--- END: NYB 2009-02-26 --->

			<cfquery name="qryInsertPersonCertificationData" datasource="#application.siteDataSource#">
				if not exists (select * from trngPersonCertification
					where personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
						and certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
						and cancellationDate is null
						and expiredDate is null
				)
					begin
						insert into trngPersonCertification (personId,certificationID,registrationDate,statusID,createdBy,created,lastUpdatedBy,lastUpdated,lastUpdatedByPerson)
						values
							( <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">,
							  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">,
							  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#arguments.registerDate#">,
							  <cf_queryparam cfsqltype="cf_sql_integer" value="#statusID#">,
							  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
							  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
							  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
							  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
							  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.personID#">
							)
						select 1 as rowInserted
					end
				else
					begin
						select 0 as rowInserted
					end
			</cfquery>

			<!--- NYB 2008-11-06 P-SOP009 -> --->
			<cfquery name="getCertificationDetails" datasource="#application.siteDataSource#" maxrows="1">
				select c.certificationID,
				'phr_title_trngCertification_' + convert(varchar, c.certificationID) as CertTitle,
				convert(varchar,pc.registrationDate,106) as RegistrationDate
				from trngPersonCertification pc
				inner join trngCertification c on pc.certificationID =c.certificationID
				where pc.personID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
				and c.CertificationID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
				order by pc.created desc
			</cfquery>

			<cftry>
				<cfset form.Certification = application.com.relayTranslations.translatePhrase(phrase=getCertificationDetails.CertTitle)/>
				<cfcatch type="any">
					<cfset form.Certification = getCertificationDetails.CertTitle>
				</cfcatch>
			</cftry>
			<cfset form.RegistrationDate = getCertificationDetails.RegistrationDate>
			<cfset form.PassDate = "">
			<cfset form.ExpiredDate = "">
			<cfset form.CancellationDate = "">
			<!--- <- NYB --->


			<cfif arguments.sendNotificationEmail>
				<cfscript>
					application.com.email.sendEmail(personID=arguments.personID,emailTextID='TrngCertificationRegistered');
				</cfscript>
			</cfif>

			<cfreturn qryInsertPersonCertificationData.rowInserted>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-09-10 NJH Function that returns certification status information
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getPersonCertificationStatus" hint="Gets the person certification statuses" returntype="query">
			<cfargument name="statusID" required="no" type="numeric">
			<cfargument name="statusTextID" required="no" type="string">

			<cfscript>
				var qryGetCertificationStatus = "";
			</cfscript>

			<cfquery name="qryGetCertificationStatus" datasource="#application.siteDataSource#">
				select * from trngPersonCertificationStatus where 1=1
				<cfif structKeyExists(arguments,"statusID")>
					and statusID = #arguments.statusID#
				</cfif>
				<cfif structKeyExists(arguments,"statusTextID")>
					and statusTextID =  <cf_queryparam value="#arguments.statusTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
			</cfquery>

			<cfreturn qryGetCertificationStatus>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-09-10 NJH Function that returns module information
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getUserModuleStatus" hint="Gets the userModule statuses" returntype="query">
			<cfargument name="statusID" required="no" type="numeric">
			<cfargument name="statusTextID" required="no" type="string">

			<cfscript>
				var qryGetModuleStatus = "";
			</cfscript>

			<cfquery name="qryGetModuleStatus" datasource="#application.siteDataSource#">
				select * from trngUserModuleStatus where 1=1
				<cfif structKeyExists(arguments,"statusID")>
					and statusID = #arguments.statusID#
				</cfif>
				<cfif structKeyExists(arguments,"statusTextID")>
					and statusTextID =  <cf_queryparam value="#arguments.statusTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
			</cfquery>

			<cfreturn qryGetModuleStatus>
		</cffunction>



		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-08-26 NJH Function that updates a person certification record.
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="updatePersonCertificationData" hint="Updates a person certification record">
			<cfargument name="personCertificationID" required="yes" type="numeric">
			<cfargument name="argumentsStruct" type="struct" required="yes">
			<cfargument name="sendNotificationEmail" type="boolean" default="#application.com.settings.getSetting('elearning.sendNotificationEmail')#">		<!--- 2013-02-08 PPB Case 433337 default to setting --->
			<cfargument name="notificationEmailTextID" type="string" default="">

			<cfscript>
				var qryUpdatePersonCertificationData = "";
				var emailTextID = "";
				var getPeopleWithRecordsUpdated = "";
				var getCertificationDetails = "";
				var mergeStruct = structNew();
				var shareMergeStruct = structNew();
				var insertPersonCertificationRuleModules = "";
				var insertPersonCertificationRules = "";
			</cfscript>

			<!--- START: NYB 2009-02-26 Sophos Stargate 2 - added because there's no way built in set notificationEmailTextID to true --->
			<!--- <cfif isDefined("request.sendNotificationEmail")>
				<cfset arguments.sendNotificationEmail = request.sendNotificationEmail>
			</cfif> --->
			<!--- <cfset arguments.sendNotificationEmail = application.com.settings.getSetting("elearning.sendNotificationEmail")> --->					<!--- 2013-02-08 PPB Case 433337 don't stomp over argument sent in with setting --->
			<!--- END: NYB 2009-02-26 --->

			<!--- START: NYB 2009-09-28 LHID2578 - added: --->
				<cfif not structKeyExists(arguments.argumentsStruct,"frmStatusID")>
					<!--- 2013-02-08 PPB Case 433337 added the neq "" conditions AND changed to use CFELSEs ; note: the order of these cfifs is IMPORTANT should tally with the cfif order of the email defs below --->
					<cfif structKeyExists(argumentsStruct,"frmCancellationDate") and argumentsStruct.frmCancellationDate neq "">
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Cancelled").statusID>
					<cfelseif structKeyExists(argumentsStruct,"frmExpireDate") and argumentsStruct.frmExpireDate neq "">		<!--- 2013-02-12 PPB Case 433337 correct a typo I introduced yesterday frmExpiryDate->frmExpireDate --->
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Expired").statusID>
					<cfelseif structKeyExists(argumentsStruct,"frmPassDate") and argumentsStruct.frmPassDate neq "">
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Passed").statusID>
					<cfelseif structKeyExists(argumentsStruct,"frmRegistrationDate") and argumentsStruct.frmRegistrationDate neq "">
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Registered").statusID>
					</cfif>
					<!--- END 2013-02-08 PPB Case 433337 --->

					<!--- START 2013-02-08 PPB Case 433337
					<cfif structKeyExists(arguments.argumentsStruct,"frmCancellationDate")>
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Cancelled").statusID>
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmExpireDate")>
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Expired").statusID>
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmPassDate")>
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Passed").statusID>
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmRegistrationDate")>
						<cfset arguments.argumentsStruct.frmStatusID = getPersonCertificationStatus(statusTextID="Registered").statusID>
					</cfif>
 					END 2013-02-08 PPB Case 433337 --->
				</cfif>
			<!--- END: NYB 2009-09-28 --->

			<cfquery name="qryUpdatePersonCertificationData" datasource="#application.siteDataSource#">
				if exists (
					select * from trngPersonCertification where personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
				)
				update trngPersonCertification set
					lastUpdatedBy =  #request.relayCurrentUser.userGroupID#,
					lastUpdated = #createODBCDateTime(request.requestTime)#,
					lastUpdatedByPerson = #request.relayCurrentUser.personID#
					<cfif structKeyExists(argumentsStruct,"frmRegistrationDate") and isDate(arguments.argumentsStruct.frmRegistrationDate)>
						,registrationDate = #createODBCDateTime(arguments.argumentsStruct.frmRegistrationDate)#
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmCancellationDate") and isDate(arguments.argumentsStruct.frmCancellationDate)>
						,cancellationDate = #createODBCDateTime(arguments.argumentsStruct.frmCancellationDate)#
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmExpireDate") and isDate(arguments.argumentsStruct.frmExpireDate)>
						,expiredDate = #createODBCDateTime(arguments.argumentsStruct.frmExpireDate)#
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmSysAccessDate") and isDate(arguments.argumentsStruct.frmSysAccessDate)>
						,accessDate = #createODBCDateTime(arguments.argumentsStruct.frmSysAccessDate)#
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmPassDate") and isDate(arguments.argumentsStruct.frmPassDate)>
						,passDate = #createODBCDateTime(arguments.argumentsStruct.frmPassDate)#
					</cfif>
					<cfif structKeyExists(arguments.argumentsStruct,"frmStatusID") and isNumeric(arguments.argumentsStruct.frmStatusID)>  <!--- 2013-05-16 NYB Case 434909 added isnumeric --->		<!--- 2013-06-28 PPB Case 435953 added arguments.argumentsStruct. --->
						,statusID =  <cf_queryparam value="#arguments.argumentsStruct.frmStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>
				where personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
			</cfquery>

			<cfif arguments.sendNotificationEmail or (structKeyExists(arguments.argumentsStruct,"frmPassDate") and isDate(arguments.argumentsStruct.frmPassDate) and application.com.settings.getSetting("socialMedia.enableSocialMedia"))>

				<cfquery name="getCertificationDetails" datasource="#application.siteDataSource#" maxrows="1">
					select pc.personid,c.certificationID,
					'phr_title_trngCertification_' + convert(varchar, c.certificationID) as CertTitle,
					convert(varchar,pc.registrationDate,106) as RegistrationDate,
					convert(varchar,pc.passDate,106) as PassDate,
					convert(varchar,pc.expiredDate,106) as ExpiredDate,
					convert(varchar,pc.cancellationDate,106) as CancellationDate,
					c.share
					from trngPersonCertification pc
					inner join trngCertification c on pc.certificationID =c.certificationID
					where personCertificationID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
					<!--- NYB 2009-02-26 1909 bug - added line: --->
					and pc.lastUpdated = #createODBCDateTime(request.requestTime)# and pc.lastUpdatedBy = #request.relayCurrentUser.userGroupID#
					order by pc.created desc
				</cfquery>

				<cfset mergeStruct = structNew()>
				<cfset mergeStruct.Certification = application.com.relayTranslations.translatePhrase(phrase=getCertificationDetails.CertTitle)/>
				<cfset mergeStruct.RegistrationDate = getCertificationDetails.RegistrationDate>
				<cfset mergeStruct.PassDate = getCertificationDetails.PassDate>
				<cfset mergeStruct.ExpiredDate = getCertificationDetails.ExpiredDate>
				<cfset mergeStruct.CancellationDate = getCertificationDetails.CancellationDate>

				<cfset emailTextID = arguments.notificationEmailTextID>

				<!--- START 2013-02-08 PPB Case 433337 changed to base email on status not on dates so the status and email sent cannot get out of sync (status may be based on dates - see above) --->
				<cfset statusTextID = getPersonCertificationStatus(statusID="#arguments.argumentsStruct.frmStatusID#").statusTextID>
				<cfif emailTextID eq "">
					<cfset emailTextID = "TrngCertification#statusTextID#">
				</cfif>
				<!--- END 2013-02-08 PPB Case 433337 --->

				<!--- START 2013-02-08 PPB Case 433337
				<cfif structKeyExists(argumentsStruct,"frmCancellationDate") and argumentsStruct.frmCancellationDate neq "">
						<cfset emailTextID = "TrngCertificationCancelled">
				<cfelseif structKeyExists(argumentsStruct,"frmExpireDate") and argumentsStruct.frmExpireDate neq "">
						<cfset emailTextID = "TrngCertificationExpired">
				<cfelseif structKeyExists(argumentsStruct,"frmPassDate") and argumentsStruct.frmPassDate neq "">
						<cfset emailTextID = "TrngCertificationPassed">
				<cfelseif structKeyExists(argumentsStruct,"frmRegistrationDate") and argumentsStruct.frmRegistrationDate neq "">
					<cfset emailTextID = "TrngCertificationRegistered" >
					</cfif>
				 END 2013-02-08 PPB Case 433337 --->

				<cfloop query="getCertificationDetails">
					<cfif emailTextID neq "" and arguments.sendNotificationEmail>
						<!--- YMA 2012-12-11 Case:431788 at some point (post 8.3.1) this call had lost its cfscript block, so wasn't running  --->
						<cfset application.com.email.sendEmail(personID=personID,emailTextID=emailTextID,mergeStruct=mergeStruct)>
					</cfif>

					<!---
						NJH 2012/01/10 - Social Media - create relayActivity when passing a certification
						NJH 2012/10/25	Social CR - check if trngCertifcation is shareable
					--->
					<cfif share and structKeyExists(arguments.argumentsStruct,"frmPassDate") and isDate(arguments.argumentsStruct.frmPassDate) and application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.trngCertification.passed")>
						<cfset shareMergeStruct.certification = structNew()>
						<cfset shareMergeStruct.certification.title = mergeStruct.Certification>
						<cfset application.com.service.addRelayActivity(action="passed",performedOnEntityID=certificationID,performedOnEntityTypeID=application.entityTypeID["trngCertification"],performedByEntityID=personID,mergeStruct=shareMergeStruct)>
					</cfif>
				</cfloop>

			</cfif>


			<!--- NJH 2012/11/16 CASE 432093 - if we've passed a certification, put the certification rules and passed modules into the 'history' data tables, so that we have a snapshot of what the rules and modules were at the time of passing the certification
				this is primarily for reporting purposes
				NJH 2013/09/30 CASE 437238 - moved code to trigger on personCertification table, as it would appear that some passes are getting missed
			 --->
			<!--- <cfif structKeyExists(arguments.argumentsStruct,"frmPassDate") and isDate(arguments.argumentsStruct.frmPassDate)>
				<cfquery name="insertPersonCertificationRules" datasource="#application.siteDataSource#">
					insert into trngPersonCertificationRule (personCertificationID,certificationRuleID,numModulesToPass,moduleSetID,active,studyPeriod,certificationRuleTypeID,activationDate,certificationRuleOrder,createdBy,lastUpdatedBy,lastUpdatedByPerson)
					select distinct tpc.personCertificationID,tcr.certificationRuleID,tcr.numModulesToPass,tcr.moduleSetID,tcr.active,tcr.studyPeriod,tcr.certificationRuleTypeID,tcr.activationDate,tcr.certificationRuleOrder,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personID#
						from trngCertificationRule tcr
							inner join trngPersonCertification tpc on tcr.certificationID = tpc.certificationID
							left join trngPersonCertificationRule tpcr on tpcr.personCertificationID = tpc.personCertificationID and tpcr.certificationRuleID = tcr.certificationRuleID
					where tpc.personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
						and tpcr.certificationRuleID is null
						and tcr.active = 1
						and tpc.passDate is not null
				</cfquery>

				<cfquery name="insertPersonCertificationRuleModules" datasource="#application.siteDataSource#">
					insert into trngPersonCertificationRuleModule (personCertificationRuleID,moduleID,createdBy,lastUpdatedBy,lastUpdatedByPerson)
					select distinct tpcr.personCertificationRuleID,tump.moduleID,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personID#
						from trngPersonCertificationRule tpcr
							inner join trngCertificationRule tcr on tpcr.certificationRuleID = tcr.certificationRuleID
							inner join trngModuleSetModule tmsm on tcr.moduleSetID = tmsm.moduleSetID
							inner join trngUserModuleProgress tump on tump.moduleId = tmsm.moduleID and tump.userModuleFulfilled is not null
							left join trngPersonCertificationRuleModule tpcrm on tpcrm.personCertificationRuleID = tpcr.personCertificationRuleID and tpcrm.moduleID = tump.moduleID
					where tpcr.personCertificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personCertificationID#">
						and tpcrm.moduleID is null
				</cfquery>
			</cfif> --->

		</cffunction>



		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-29 NJH Function that sets a users module progress as having been completed

	            LID 5267: Certification Passed and Module Passed Emails - NJH 2011-11-16
	            I've re-arranged the function slightly so that certification passed emails don't get sent before quiz passed emails.
	            To do this, I now pass in quiz passed email textID via notificationEmailTextID so that the quiz passed email is not actually
	            	sent in the relayQuiz.setQuizTakenCompleted function. If the rewards email hasn't been sent and notificationEmailTextID has been passed in,
	            	then sent the email.
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="setModuleComplete" returntype="struct" hint="Set user module progress as complete">
			<cfargument name="userModuleProgressID" required="no">
			<cfargument name="CompletedDate" required="no" type="date" default="#request.requestTime#">
			<cfargument name="personID" required="no" type="numeric">
			<cfargument name="moduleID" required="no" type="numeric">
			<cfargument name="rwNotificationEmailTextID" type="string" default=""> <!--- NJH 2009-02-05 P-SNY047 --->
			<cfargument name="rwNotificationEmailMergeStruct" type="struct" default="#structNew()#"> <!--- NJH 2009-02-05 P-SNY047 --->
			<cfargument name="status" type="string" required="no" default="passed"> <!--- NJH 2009-02-05 P-SNY047 thought we might need to set the status... --->
			<cfargument name="CertificationPassDate" required="no" type="date" default="#request.requestTime#"> <!--- NYB 2009-06-04 SOP010 - added --->
			<cfargument name="qryQuizTaken" type="query" required="no"> <!--- NYB 2009-09-24 LHID2667 - added --->
			<cfargument name="notificationEmailTextID" type="string" default="">
			<cfargument name="attachments" Required="No" default="#queryNew('path')#" type="Query">

			<cfscript>
				var qryCompleteUserModuleProgress = "";
				var progressID = 0;
				var argumentsStruct = structNew();
				var isPersonRegisteredQry = "";
				var pointsEarned = 0;
				var emailMergeStruct = StructNew();
				var resultStruct = {rwEmailSent=false};
				var emailReturnMsg = "";
				var useIncentives = application.com.settings.getSetting("elearning.useIncentives");
				var getUserModuleProgressID = "";
				var getUserModuleProgressPersonID = "";
				var getModuleStatus = "";
				var shareMergeStruct = structNew();
				var thisModuleID = 0;
				var canShareModuleQry = "";
			</cfscript>

			<!---
				If the userModuleID is not passed in, get the ID from the moduleID and the personID
				Need to think about when many entries in this table exist for a user/module combination.. ie. if a user has started a module multiple times
				without having completed the previous. Right now get the one based on the latest start date...
			 --->
			<cfif not structKeyExists(arguments,"userModuleProgressID") and structKeyExists(arguments,"personID") and structKeyExists(arguments,"moduleID")>
				<cfquery name="getUserModuleProgressID" datasource="#application.siteDataSource#">
					select top 1 userModuleProgressID from trngUserModuleProgress
					where moduleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleID#">
						and personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
						and userModuleFulfilled is null
					order by startedDateTime desc
				</cfquery>

				<cfif getUserModuleProgressIDs.recordCount gt 0>
					<cfset progressID = getUserModuleProgressIDs.userModuleProgressID>
				</cfif>
			<cfelseif structKeyExists(arguments,"userModuleProgressID")>
				<cfset progressID = arguments.userModuleProgressID>
			</cfif>

			<cfif structKeyExists(arguments,"qryQuizTaken")>
				<cfset argumentsStruct.qryQuizTaken=arguments.qryQuizTaken>
			</cfif>

			<cfif progressID neq 0>
				<cfquery name="getUserModuleProgressPersonID" datasource="#application.siteDataSource#">
					select personID from trngUserModuleProgress
					where userModuleProgressID =  <cf_queryparam value="#progressID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>


				<cfscript>
					//START: 2013-11-19 NYB Case 436793 wrapped line in an if
					if (structKeyExists(arguments,"status") and arguments.status eq "Passed") {
						argumentsStruct.frmUserModuleFulfilled=arguments.CompletedDate;
					}
					//END: 2013-11-19 NYB Case 436793
					// NJH 2009-03-18 - P-SNY047 - passing a status through
					if (structKeyExists(arguments,"status") and arguments.status neq "") {
						argumentsStruct.frmStatusID = getUserModuleStatus(statusTextID=arguments.status).statusID;
					}
					updateUserModuleProgress(userModuleProgressID=progressID,argumentsStruct=argumentsStruct);
					// NJH 2011-11-02 LID 5267 moved passValidPersonCertifications function call down below so email is sent after all other emails, such as quiz passed.
				</cfscript>
			</cfif>

			<cfquery name="getModuleStatus" datasource="#application.siteDataSource#">
				select statusTextID,moduleID from trngUserModuleStatus tums inner join trngUserModuleProgress tump
					on tums.statusID = tump.moduleStatus
					where tump.userModuleProgressID = <cf_queryparam cfsqltype="cf_sql_integer" value="#progressID#">
			</cfquery>

			<cfset resultStruct.moduleStatus = getModuleStatus.statusTextID>

			<cfif progressID neq 0>
				<!--- NJH 2009-02-12 P-SNY047
					If we're using incentives, then submit a claim when a module has been completed.
				--->
				<cfif useIncentives>
					<cfscript>
						if (getModuleStatus.recordCount eq 1 and getModuleStatus.statusTextID eq "Passed") {
							isPersonRegisteredQry = application.com.relayIncentive.isPersonRegistered(personID=getUserModuleProgressPersonID.personID);

							// is the person registered for rewards?
							if (isPersonRegisteredQry.recordCount) {
								pointsEarned = submitElearningRewardsClaim(userModuleProgressID=progressID);
								resultStruct.pointsEarned = pointsEarned;

								if (arguments.rwNotificationEmailTextID neq "" and pointsEarned neq 0) {

									//if (structKeyExists(arguments,"rwNotificationEmailMergeStruct")) {
									emailMergeStruct = duplicate(arguments.rwNotificationEmailMergeStruct);
									//}
									// send email with points earned
									emailMergeStruct.pointsEarned = pointsEarned;
									emailReturnMsg = application.com.email.sendEmail(personID=getUserModuleProgressPersonID.personID,emailTextID=arguments.rwNotificationEmailTextID,mergeStruct=emailMergeStruct,attachments=arguments.attachments);
									// NJH 2009-10-01 LID 2717 - check return on send email
									if (emailReturnMsg eq "Email Sent") {
										resultStruct.rwEmailSent = true;
									}
								}
							}
						}
					</cfscript>
				</cfif>

				<cfif not resultStruct.rwEmailSent and arguments.notificationEmailTextID neq "">
					<cfset application.com.email.sendEmail(personID=getUserModuleProgressPersonID.personID,emailTextID=arguments.notificationEmailTextID,mergeStruct=arguments.rwNotificationEmailMergeStruct,attachments=arguments.attachments)>
				</cfif>

				<!--- NJH 2011-11-02 - moved pass certification down here so that emails get sent after the quiz passed/module passed emails. --->
				<!--- PYW 2016-01-04 P-TAT006 BRD 31. Add userModuleProgressID argument to check for passed module with Retest rule --->
				<cfset passValidPersonCertifications(personID=getUserModuleProgressPersonID.personID,viewName = "vTrngPeopleEligibleForCertificationByModules",passDate=CertificationPassDate,userModuleProgressID=progressID)>

			</cfif>

			<!---
				NJH 2012/01/10 - Social Media - send automated share when passing a module
				NJH 2012/10/25	Social CR - check if trngModule is shareable
			--->
			<cfif getModuleStatus.statusTextID eq "Passed" and application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.trngModule.passed")>

				<cfquery name="canShareModuleQry" datasource="#application.siteDataSource#">
					select 1 from trngModule where moduleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#getModuleStatus.moduleID#"> and share=1
				</cfquery>

				<cfif canShareModuleQry.recordCount>
					<cfset shareMergeStruct.module.title = "phr_title_TrngModule_"&getModuleStatus.moduleID>
					<cfset application.com.service.addRelayActivity(action="passed",performedOnEntityID=getModuleStatus.moduleID,performedOnEntityTypeID=application.entityTypeID["trngModule"],mergeStruct=shareMergeStruct,sendShare=true)>
				</cfif>
			</cfif>

			<cfreturn resultStruct>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-09-02 NJH Function that inserts a user module record.
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="insertPersonModuleProgress" hint="Inserts a person module progress record if the user does not already have a module in progress" returntype="numeric">
			<cfargument name="moduleID" required="yes" type="numeric">
			<cfargument name="personID" type="numeric" required="no" default="#request.relayCurrentUser.personID#">
			<cfargument name="argumentsStruct" type="struct" required="no">		<!--- 2013-02-07 PPB Case 433635 pass thro struct (as per updateUserModuleProgress) --->	<!--- 2013-02-12 PPB Case 433751 argumentsStruct not reqd --->

			<cfscript>
				var qryInsertUserModuleProgress = "";
				// START: AJC 2008-11-25 - Bug where by it resets passed modules
				var fstatusID = getUserModuleStatus(statusTextID='Failed').statusID;
				// NYB 2009-09-08 - LHID2617 - removed var pstatusID as it's superfulous - see notes below
				//var pstatusID = getUserModuleStatus(statusTextID='Passed').statusID;
				var istatusID = getUserModuleStatus(statusTextID='InProgress').statusID;
				// END: AJC 2008-11-25 - Bug where by it resets passed modules
			</cfscript>

			<!--- START 2013-02-07 PPB Case 433635 --->
			<cfif StructKeyExists(arguments,"argumentsStruct") and  StructKeyExists(argumentsStruct,"frmStatusId")>	<!--- 2013-02-12 PPB Case 433751 argumentsStruct not reqd --->
				<cfset newModuleStatusId = argumentsStruct.frmStatusId>
			<cfelse>
				<cfset newModuleStatusId = istatusID>			<!--- this routine originally set the module to "In Progress" regardless --->
			</cfif>
			<!--- END 2013-02-07 PPB Case 433635 --->

			<cfquery name="qryInsertUserModuleProgress" datasource="#application.siteDataSource#">
				if not exists (
					select * from trngUserModuleProgress where moduleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleID#">
						and personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
						<!--- START: AJC 2008-11-25 - Bug where by it resets passed modules --->
						and (moduleStatus ! =  <cf_queryparam value="#fstatusID#" CFSQLTYPE="CF_SQL_INTEGER" > )
						<!--- END: AJC 2008-11-25 - Bug where by it resets passed modules --->
					)
				begin
					insert into trngUserModuleProgress (moduleID,personID,moduleStatus,createdBy,lastUpdatedBy,lastUpdatedByPerson) values (
						<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleID#">,
						<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">,
						<!--- START: AJC 2008-11-25 - Bug where by it resets passed modules --->
						<cf_queryparam cfsqltype="cf_sql_integer" value="#newModuleStatusId#">,			<!--- 2013-02-07 PPB Case 433635 --->
						<!--- END: AJC 2008-11-25 - Bug where by it resets passed modules --->
						<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
						<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
						<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.personID#">
					)
					select scope_identity() as userModuleProgressID
				end
				else
					 select userModuleProgressID from trngUserModuleProgress
					 	where moduleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleID#">
						and personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
						<!--- START: AJC 2008-11-25 - Bug where by it resets passed modules --->
							<!--- START: NYB 2009-09-08 - LHID2617 Bug - replaced the following: ---
							and (moduleStatus = #istatusID# or moduleStatus = #pstatusID#)
							!--- WITH: NYB 2009-09-08 - LHID2617 --->
							and moduleStatus ! =  <cf_queryparam value="#fstatusID#" CFSQLTYPE="CF_SQL_INTEGER" >
							<!--- END: NYB 2009-09-08 - LHID2617 - because: if status isn't i,p or f statusid then this returns nothing and makes reliant code crash, condition here must be consistant with the test above--->
						<!--- END: AJC 2008-11-25 - Bug where by it resets passed modules --->
			</cfquery>

			<!--- START 05-MAY-2011	MS	Course Counts --->
			<CFSCRIPT>recordCourseClick(moduleID=moduleID);</CFSCRIPT>
			<!--- END 05-MAY-2011	MS	Course Counts --->
			<cfreturn qryInsertUserModuleProgress.userModuleProgressID>

		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-08 NJH Function that returns specialisation data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetSpecialisationData" hint="Returns specialisation information">
			<cfargument name="sortOrder" required="no" default="Code">
			<cfargument name="specialisationID" required="no" type="numeric">
			<cfargument name="countryScope" required="false" default="false"> <!--- LID 2688 NJH 2009-09-25 --->
			<cfargument name="active" type="boolean" required="false">

			<cfscript>
				var qryGetSpecialisationData = "";
				var specTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "specialisation", phraseTextID = "title");
				var specDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "specialisation", phraseTextID = "description");
				// START: 2012-05-01 - Englex - P-REL109 - Add training program
				var TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title", nullText = "'#application.com.relaytranslations.translatePhrase("elearning_trainingProgram_all")#'");
				// END: 2012-05-01 - Englex - P-REL109 - Add training program
			</cfscript>

			<!--- LID 2688 NJh 2009-09-25 added country scope and extra columns
				NJH 2012/10/25 Case 431516 UAT - put query in a sub-select so that we can sort by derived columns
			--->
			<cfquery name="qryGetSpecialisationData" datasource="#application.siteDataSource#">
				select * from (
					select v.*,
						#specTitlePhraseQuerySnippets.select# as title,
						#specDescPhraseQuerySnippets.select# as description,
						NumRules as Number_Of_Rules, NumRulesToPass as Number_Of_Rules_To_Pass
						<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>,
						#preserveSingleQuotes(TrainingProgramPhraseQuerySnippets.select)#  as Training_Program
						</cfif>
						<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
					from vSpecialisations v
						#preservesinglequotes(specTitlePhraseQuerySnippets.join)#
						#preservesinglequotes(specDescPhraseQuerySnippets.join)#
						<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
							#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
						</cfif>
				) as base
				<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
				where 1=1
				<cfif isDefined("arguments.specialisationID")>
					<!--- Case 433693 NYB 2013-02-13 replace v. with base. : --->
					and base.specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">
				<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
				<cfelseif application.com.settings.getSetting("elearning.useTrainingProgram")
						and isdefined("session.selectedTrainingProgramID")
								and session.selectedTrainingProgramID neq "0" and session.selectedTrainingProgramID neq ""> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
					<!--- Case 433693 NYB 2013-02-13 replace v. with base. : --->
					and base.TrainingProgramID = <cf_queryparam value="#session.selectedTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
				</cfif>
				<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
				<cfif structKeyExists(arguments,"countryScope")>
					#application.com.rights.getRightsFilterWhereClause(entityType="Specialisation",alias="base",regionList=request.relayCurrentUser.regionListSomeRights).whereClause#		<!--- 2012-07-25 PPB P-SMA001 --->
				</cfif>
				<cfif structKeyExists(arguments,"active")>
					and active = <cf_queryparam value="#arguments.active#" CFSQLType="CF_SQL_BIT" />
				</cfif>
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by #arguments.sortOrder#
			</cfquery>
			<cfreturn qryGetSpecialisationData>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-09 NJH Function that returns specialisation rule data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetSpecialisationRuleData" hint="Returns specialisation rule information">
			<cfargument name="sortOrder" required="no" default="specialisationID">
			<cfargument name="specialisationID" required="no" type="numeric">
			<cfargument name="specialisationRuleID" required="no" type="numeric">

			<cfscript>
				var qryGetSpecialisationRuleData = "";
			</cfscript>

			<cfquery name="qryGetSpecialisationRuleData" datasource="#application.siteDataSource#">
				select * from specialisationRule
				where 1=1
				<cfif isDefined("arguments.specialisationID")>
				and specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">
				</cfif>
				<cfif isDefined("arguments.specialisationRuleID")>
				and specialisationRuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationRuleID#">
				</cfif>
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by #arguments.sortOrder#
			</cfquery>
			<cfreturn qryGetSpecialisationRuleData>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-08-20 NJH Function that adds specialisation rule data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="insertSpecialisationRule" hint="Add a specialisation rule to a specialisation">
			<cfargument name="certificationID" required="yes" type="numeric">
			<cfargument name="numOfCertifications" required="yes" type="numeric">
			<cfargument name="specialisationID" required="yes" type="numeric">

			<cfscript>
				var qryAddSpecialisationRule = "";
			</cfscript>

			<cfquery name="qryAddSpecialisationRule" datasource="#application.siteDataSource#">
				if not exists (select * from specialisationRule
					where certificationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
						and specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">
				)
				insert into specialisationRule
					(certificationID,numOfCertifications,specialisationID,created,createdBy,lastUpdated,lastUpdatedBy)
				values
					( <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.numOfCertifications#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">,
					  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
					  <cf_queryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
					  <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">
					)
			</cfquery>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-08-26 NJH Function that updates a person module record.
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="updateUserModuleProgress" hint="Updates a user module record">
			<cfargument name="userModuleProgressID" type="numeric" required="yes">
			<cfargument name="argumentsStruct" type="struct" required="yes">

			<cfscript>
				var qryUpdateUserModuleProgressData = "";
				var qryUserModuleProgressData = "";
			</cfscript>

			<!--- START: NYB 2009-09-28 LHID2578 - added: --->
			<cfif structKeyExists(arguments.argumentsStruct,"frmUserModuleFulfilled")>
				<cfif not structKeyExists(arguments.argumentsStruct,"frmStatusID")>
					<cfset arguments.argumentsStruct.frmStatusID = getUserModuleStatus(statusTextID="Passed").statusID>
				</cfif>
			<cfelse>
				<cfif structKeyExists(arguments.argumentsStruct,"frmStatusID")>
					<cfif getUserModuleStatus(statusID=arguments.argumentsStruct.frmStatusID).statusTextID eq "Passed">
						<cfset arguments.argumentsStruct.frmStatusID = getUserModuleStatus(statusTextID="Passed").statusID>
						<cfset arguments.argumentsStruct.frmUserModuleFulfilled = createODBCDateTime(request.requestTime)>
					</cfif>
				</cfif>
			</cfif>
			<!--- END: NYB 2009-09-28 --->

			<!--- START: NYB 2009-09-30 LHID2686 - lots of changes made here --->
			<cfquery name="qryUserModuleProgressData" datasource="#application.siteDataSource#">
				<!--- 2013-11-19 NYB Case 436793 added quizTimeSpent: --->
				select moduleID,isNULL(AICCScore,0) as moduleScore,isnull(quizTimeSpent,0) as quizTimeSpent from trngUserModuleProgress where userModuleProgressID = #arguments.userModuleProgressID#
			</cfquery>

			<!--- START: 2013-11-19 NYB Case 436793 added: --->
			<cfif structKeyExists(arguments.argumentsStruct,"qryQuizTaken")>
				<cfquery name="qryGetQuizTakenCount" datasource="#application.siteDataSource#">
					select count(*) as [count] from quizTaken where userModuleProgressID=#arguments.userModuleProgressID#
				</cfquery>
			</cfif>
			<!--- END: 2013-11-19 NYB Case 436793 --->

			<CFIF qryUserModuleProgressData.recordcount gt 0>
				<cfquery name="qryUpdateUserModuleProgressData" datasource="#application.siteDataSource#">
					update trngUserModuleProgress set
						lastUpdatedBy =  #request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson =  #request.relayCurrentUser.personID#,
						<cfif structKeyExists(arguments.argumentsStruct,"frmLastVisited")>
							lastVisitedDateTime = #createODBCDateTime(arguments.argumentsStruct.frmLastVisited)#,
						</cfif>
						<cfif structKeyExists(arguments.argumentsStruct,"frmUserModuleFulfilled")>
							userModuleFulfilled = #createODBCDateTime(arguments.argumentsStruct.frmUserModuleFulfilled)#,
						</cfif>
						<cfif structKeyExists(arguments.argumentsStruct,"frmStatusID")>
							moduleStatus =  <cf_queryparam value="#arguments.argumentsStruct.frmStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						</cfif>
						<cfif structKeyExists(arguments.argumentsStruct,"qryQuizTaken")>
							<cfif argumentsStruct.qryQuizTaken.score gte arguments.argumentsStruct.qryQuizTaken.passMark>
									quizPassed = 1,
							</cfif>
							<cfif arguments.argumentsStruct.qryQuizTaken.score gt qryUserModuleProgressData.moduleScore>
								quizPassedDate = #CreateODBCDateTime(arguments.argumentsStruct.qryQuizTaken.completed)#,
								<!--- WAB 2011-02-09 LID 5508 this said qryUserModuleProgressData.moduleScore which was wrong! --->
								AICCScore =  <cf_queryparam value="#arguments.argumentsStruct.qryQuizTaken.score#" CFSQLTYPE="CF_SQL_Integer" > ,
								quizTimeSpent =  <cf_queryparam value="#arguments.argumentsStruct.qryQuizTaken.TimeAllowed - arguments.argumentsStruct.qryQuizTaken.TimeLeft#" CFSQLTYPE="CF_SQL_Integer" > ,
							</cfif>
							<!--- START: 2013-11-19 NYB Case 436793 added: --->
							quizAttempts = (select count(*) from quizTaken where TimeLeft is not null and (Completed is not null or quizStatus is not null) and userModuleProgressID is not null and userModuleProgressID=#arguments.userModuleProgressID#),
							<cfif argumentsStruct.qryQuizTaken.NumberOfAttemptsAllowed gte qryGetQuizTakenCount.count and not structKeyExists(arguments.argumentsStruct,"frmStatusID")>
									<cfset moduleStatusID = getUserModuleStatus(statusTextID="#argumentsStruct.qryQuizTaken.quizStatus#ed").statusID>
									moduleStatus  =  <cf_queryparam value="#moduleStatusID#" CFSQLTYPE="cf_sql_integer" >,
							</cfif>
							<!--- END: 2013-11-19 NYB Case 436793 --->
						</cfif>
						lastUpdated = #createODBCDateTime(request.requestTime)#
					where userModuleProgressID = #arguments.userModuleProgressID#
				</cfquery>
			</CFIF>
			<!--- END: NYB 2009-09-30 LHID2686 --->

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            Function updates the entitySpecialisationRuleStatus. Previously a trigger in trngPersonCertifications
	            Created 2010-10-27 NYB
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="updateEntitySpecialisationsRulesStatus" hint="Add a specialisation for an entity">
			<!--- update the certification count for organisations where the certification count has changed --->

			<cfset var checkUpdateStatus = "">
			<cfset var checkInsertStatus = "">
			<cfset var qryCompulsoryModules = "">
			<cfset var modulePassed = "">
			<!--- <cfset var compulsoryModulesPassed = ""> WAB 2012-07-06 removed this var, it is not a variable but a column in a query and varing it causes a conflict between the scopes --->
			<cfset var updateStatus = "">
			<cfset var insertStatus = "">
			<cfset var flagID = "">
			<cfset var baseQuery = "">
			<cfset var deleteInvalidSpecialisations = "">

			<!--- Case ??? NJH 2013/12/17 - here we are deleting specialisations for organisations where the person has moved orgs and taken the specialisation with them... the one at the old org should be deleted...  --->
			<cfsavecontent variable="baseQuery">
				<cfoutput>
					inner join specialisationRule sr on esrs.specialisationRuleID = sr.specialisationRuleID
					inner join trngPersonCertification tpc on tpc.certificationID = sr.certificationID and sr.active=1 and sr.activationDate<=getdate()
					inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
					inner join person p on tpc.personID = p.personID and p.organisationID != esrs.entityID
					inner join
					(
						select distinct oldVal, newVal, mr.recordID, mr.modDate from modregister mr
							inner join modEntityDef med on mr.modEntityID = med.modEntityID and mr.entityTypeID=#application.entityTypeID.person# and med.fieldname='organisationID'
							inner join (
								select max(modDate) moddate,recordId from modRegister mr
									inner join modEntityDef med on mr.modEntityID = med.modEntityID and mr.entityTypeID=0 and med.fieldname='organisationID'
									group by recordID
							) lastestChange on lastestChange.moddate = mr.modDate and lastestChange.recordid = mr.recordid <!--- WAB 2014-03-18 CASE 439167, getting odd errors and noticed that there was no join on recordid  --->
					) b on b.recordID = p.personID and esrs.entityID = b.oldVal and p.organisationID = b.newVal and b.modDate > esrs.created
					where p.personID is not null
				</cfoutput>
			</cfsavecontent>

			<cfquery name="deleteInvalidSpecialisations" datasource="#application.siteDataSource#">
				delete entitySpecialisation
				from entitySpecialisation es
					inner join entitySpecialisationRuleStatus esrs on esrs.entityTypeID = es.entityTypeID and esrs.entityID = es.entityID
					#preserveSingleQuotes(baseQuery)#

				delete entitySpecialisationRuleStatus
				from entitySpecialisationRuleStatus esrs
					#preserveSingleQuotes(baseQuery)#
			</cfquery>

			<cfquery name="checkUpdateStatus" datasource="#application.siteDataSource#">
				select esrs.SpecialisationRuleID,esrs.entityid,1 as compulsoryModulesPassed from entitySpecialisationRuleStatus esrs
				inner join (
					select count(tpc.certificationID) as numOfCertificationsByOrg,p.organisationID,sr.specialisationRuleID from
					trngPersonCertification tpc
					inner join person p on tpc.personID = p.personID
					inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
					inner join specialisationRule sr on tpc.certificationID = sr.certificationID and sr.active=1 and sr.activationDate<=getdate()
					inner join entitySpecialisationRuleStatus esrs on esrs.specialisationRuleID = sr.specialisationRuleID and entityID=p.organisationID and entityTypeID=2
					group by organisationID,tpc.certificationID,sr.specialisationRuleID
				) base on esrs.entityID = base.organisationID
					and esrs.specialisationRuleID = base.specialisationRuleID and entityTypeID=2
					and esrs.numOfCertifications <> base.numOfCertificationsByOrg
			</cfquery>

			<cfquery name="checkInsertStatus" datasource="#application.siteDataSource#">
				select sr.specialisationRuleID,p.organisationid as entityid,1 as compulsoryModulesPassed
				from trngPersonCertification tpc
					inner join person p on tpc.personID = p.personID
					inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
					inner join specialisationRule sr on tpc.certificationID = sr.certificationID and sr.active=1 and sr.activationDate<=getdate()
					left join entitySpecialisationRuleStatus esrs on esrs.specialisationRuleID = sr.specialisationRuleID and entityID=p.organisationID and entityTypeID=2
				where esrs.entityID is null
				group by organisationID,tpc.certificationID,sr.specialisationRuleID
			</cfquery>

			<cfloop query="checkUpdateStatus">
				<cfset qryCompulsoryModules = GetSpecialisationRuleCompulsoryModules(checkUpdateStatus.specialisationRuleID)>
				<cfloop query="qryCompulsoryModules">
					<cfset modulePassed = checkModulePassed(specialisationRuleID=checkUpdateStatus.specialisationRuleID,entityID=checkUpdateStatus.entityId,moduleID=qryCompulsoryModules.value)>	<!--- value=moduleID --->
					<cfif not modulePassed>
						<cfset checkUpdateStatus.compulsoryModulesPassed = 0>
						<cfbreak>
					</cfif>
				</cfloop>
			</cfloop>

			<cfloop query="checkUpdateStatus">
				<cfif compulsoryModulesPassed>
					<cfquery name="updateStatus" datasource="#application.siteDataSource#">
						update entitySpecialisationRuleStatus set numOfCertifications = base.numOfCertificationsByOrg,lastUpdatedBy=4,lastUpdated=GetDate()
						from entitySpecialisationRuleStatus esrs
						inner join (
							select count(tpc.certificationID) as numOfCertificationsByOrg,p.organisationID,sr.specialisationRuleID from
							trngPersonCertification tpc
							inner join person p on tpc.personID = p.personID
							inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
							inner join specialisationRule sr on tpc.certificationID = sr.certificationID and sr.active=1 and sr.activationDate<=getdate()
							inner join entitySpecialisationRuleStatus esrs on esrs.specialisationRuleID = sr.specialisationRuleID and entityID=p.organisationID and entityTypeID=2
							group by organisationID,tpc.certificationID,sr.specialisationRuleID
						) base on esrs.entityID = base.organisationID
							and esrs.specialisationRuleID = base.specialisationRuleID and entityTypeID=2
							and esrs.numOfCertifications <> base.numOfCertificationsByOrg
							and esrs.specialisationRuleID =  <cf_queryparam value="#checkUpdateStatus.SpecialisationRuleID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>
				</cfif>
			</cfloop>

			<cfloop query="checkInsertStatus">
				<cfset qryCompulsoryModules = GetSpecialisationRuleCompulsoryModules(checkInsertStatus.specialisationRuleID)>
				<cfloop query="qryCompulsoryModules">
					<cfset modulePassed = checkModulePassed(specialisationRuleID=checkInsertStatus.specialisationRuleID,entityID=checkInsertStatus.entityId,moduleID=qryCompulsoryModules.value)>	<!--- value=moduleID --->
					<cfif not modulePassed>
						<cfset checkInsertStatus.compulsoryModulesPassed = 0>
						<cfbreak>
					</cfif>
				</cfloop>
			</cfloop>

			<cfloop query="checkInsertStatus">
				<cfif compulsoryModulesPassed>
					<cfquery name="insertStatus" datasource="#application.siteDataSource#">
						insert into entitySpecialisationRuleStatus (entityID,entityTypeID,numOfCertifications,specialisationRuleID,created,createdBy,lastUpdated,lastUpdatedBy)
						select p.organisationID,2,count(tpc.certificationID),sr.specialisationRuleID,GetDate(),4,GetDate(),4
						from trngPersonCertification tpc
							inner join person p on tpc.personID = p.personID
							inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
							inner join specialisationRule sr on tpc.certificationID = sr.certificationID and sr.active=1 and sr.activationDate<=getdate()
							left join entitySpecialisationRuleStatus esrs on esrs.specialisationRuleID = sr.specialisationRuleID and entityID=p.organisationID and entityTypeID=2
						where esrs.entityID is null
							and sr.specialisationRuleID =  <cf_queryparam value="#checkInsertStatus.SpecialisationRuleID#" CFSQLTYPE="CF_SQL_INTEGER" >
						group by organisationID,tpc.certificationID,sr.specialisationRuleID
					</cfquery>
				</cfif>
			</cfloop>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-16 NJH Function that adds entity specialisations
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="insertEntitySpecialisations" hint="Add a specialisation for an entity" returnType="struct">
			<cfargument name="numRowsToProcess" type="numeric" default="100">
			<cfargument name="excludeEntityWithFlags" type="string" default=""> <!--- a list of flagIDs which will exclude an entity from getting a specialisation if they are flagged with the flag. --->

			<cfscript>
				var qryAddEntitySpecialisation = "";
				var getEntitiesThatHaveJustBeenGivenSpecialisations = "";
				var trainingContact=0;
				var flagStructure = "";
				var flagID = "";
				var result = {isOK=true,message=""};
			</cfscript>

			<cftry>
				<!--- NYB 2010-10-27 Sophos issue. Added:  --->
				<cfset application.com.relayElearning.updateEntitySpecialisationsRulesStatus()>

				<!--- PPB 2010-08-10 code deliberately commented out; logic now in qrySpecialisationsToInsert --->
				<!--- 2010-10-27 NYB uncommented out.  Moved PPB logic to updateEntitySpecialisationsRulesStatus function instead --->
				<cfquery name="qryAddEntitySpecialisation" datasource="#application.siteDataSource#">
					insert into entitySpecialisation (entityID,entityTypeID,specialisationID,created,createdBy,lastUpdated,lastUpdatedBy)
					select distinct top #numRowsToProcess# v.entityID,v.entityTypeID,specialisationID,#createODBCDateTime(request.requestTime)#,#request.relayCurrentUser.userGroupID#,#createODBCDateTime(request.requestTime)#,#request.relayCurrentUser.userGroupID#
					from vEntitySpecialisations v with (noLock) inner join countryGroup cg with (noLock) on v.countryid = cg.countrygroupid
						left join organisation o with (noLock) on v.entityTypeID = 2 and v.entityid = o.organisationid
						left join person p with (noLock) on v.entityTypeID = 0 and v.entityid = p.personid
						left join location l on p.locationid = l.locationid
						<cfloop list="#arguments.excludeEntityWithFlags#" index="flagID">
						left outer join #application.com.flag.getFlagStructure(flagID).flagType.dataTableFullName# f#flagID# with (noLock)
							on v.entityID = f#flagID#.entityID and v.entityTypeID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID).entityType.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfloop>
					<!--- NYB 2009-03-13 Stargate 2 - replaced numRules with numRulesToPass: --->
					where entitySpecialisationRuleCount >= numRulesToPass
						and entitySpecialisationID is null
						and expiredDate is null
						and v.active = 1
						and cg.countryMemberID = case when v.entityTypeID = 2 then o.countryid when v.entityTypeID = 0 then l.countryid else null end
						<cfloop list="#arguments.excludeEntityWithFlags#" index="flagID">
						 and f#flagID#.entityID is null
						</cfloop>
				</cfquery>


				<!--- PPB 2010-08-10 prevent a specialisation being awarded if the Compulsory Modules have not been passed as defined in the specialisation rules
					  achieving this proved difficult in the existing queries/views so I have left the code to generate the specialisations as it was (now in qrySpecialisationsToInsert)
					  and post-process the results and set an indicator (compulsoryModulesPassed) against each row indicating whether it passed/failed the to get the Compulsory Modules criterion
				--->
				<!--- 2010-10-27 NYB commented out.  Moved PPB logic to updateEntitySpecialisationsRulesStatus function instead
				<cfset var qryCompulsoryModules = "">
				<cfset var modulePassed = "">
				<cfset var qryUpdateEntitySpecialisation = "">
				<cfset var qrySpecialisationRules = "">
				<cfset var qrySpecialisationsToInsert = "">
				<cfquery name="qrySpecialisationsToInsert" datasource="#application.siteDataSource#">
					select distinct top #numRowsToProcess# v.entityID,v.entityTypeID,specialisationID,#createODBCDateTime(request.requestTime)# as created,#request.relayCurrentUser.userGroupID# as createdBy,#createODBCDateTime(request.requestTime)# as lastUpdated,#request.relayCurrentUser.userGroupID# as lastUpdatedBy
					,1 as compulsoryModulesPassed
					from vEntitySpecialisations v with (noLock) inner join countryGroup cg with (noLock) on v.countryid = cg.countrygroupid
						left join organisation o with (noLock) on v.entityTypeID = 2 and v.entityid = o.organisationid
						left join person p with (noLock) on v.entityTypeID = 0 and v.entityid = p.personid
						left join location l on p.locationid = l.locationid
						<cfloop list="#excludeEntityWithFlags#" index="flagID">
							<cfset flagStructure = application.com.flag.getFlagStructure(flagID)>
						left outer join #flagStructure.flagType.dataTableFullName# f#flagID# with (noLock)
							on v.entityID = f#flagID#.entityID and v.entityTypeID = #flagStructure.entityType.entityTypeID#
						</cfloop>
					<!--- NYB 2009-03-13 Stargate 2 - replaced numRules with numRulesToPass: --->
					where entitySpecialisationRuleCount >= numRulesToPass
						and entitySpecialisationID is null
						and expiredDate is null
						and v.active = 1
						and cg.countryMemberID = case when v.entityTypeID = 2 then o.countryid when v.entityTypeID = 0 then l.countryid else null end
						<cfloop list="#excludeEntityWithFlags#" index="flagID">
						 and f#flagID#.entityID is null
						</cfloop>
				</cfquery>

				<!--- get the specialisation rules for each specialisation to insert and loop around them, for each get the compulsory modules for the rule and loop around them checking each that the module has been passed (by at least the number of certificates required for that rule)  --->
				<cfloop query="qrySpecialisationsToInsert">
					<cfquery name="qrySpecialisationRules" datasource="#application.siteDataSource#">
						SELECT * FROM SpecialisationRule WHERE SpecialisationID =  #qrySpecialisationsToInsert.SpecialisationID# AND active <> 0
					</cfquery>

					<cfloop query="qrySpecialisationRules">

						<cfset qryCompulsoryModules = GetSpecialisationRuleCompulsoryModules(qrySpecialisationRules.specialisationRuleID)>

						<cfloop query="qryCompulsoryModules">
							<cfset modulePassed = checkModulePassed(qrySpecialisationRules.specialisationRuleID,qrySpecialisationsToInsert.entityId,qrySpecialisationsToInsert.entityTypeId,qryCompulsoryModules.value)>	<!--- value=moduleID --->

							<cfif not modulePassed>
								<cfset qrySpecialisationsToInsert.compulsoryModulesPassed = 0>
								<cfbreak>
							</cfif>
						</cfloop>
					</cfloop>
				</cfloop>

				<cfloop query="qrySpecialisationsToInsert">
					<cfif compulsoryModulesPassed>
						<cfquery name="qryAddEntitySpecialisation" datasource="#application.siteDataSource#">
							insert into entitySpecialisation (entityID,entityTypeID,specialisationID,created,createdBy,lastUpdated,lastUpdatedBy)
							values (#entityID#,#entityTypeID#,#specialisationID#,'#created#',#createdBy#,'#lastUpdated#',#lastUpdatedBy#)
						</cfquery>
					</cfif>
				</cfloop>
	 --->

				<!--- START:  NYB 2009-03-13 Stargate 2 - add query for update Specialisations where rules had changed - add above indicates that perhaps entitySpecialisations should have multiple specialisationIDs for the same entityID - this is still to be comfirmed though --->
				<cfquery name="qryUpdateEntitySpecialisation" datasource="#application.siteDataSource#">
					update entitySpecialisation set expiredDate=NULL WHERE entitySpecialisationID in (
						select entitySpecialisationID
						from vEntitySpecialisations v with (noLock) inner join countryGroup cg with (noLock) on v.countryid = cg.countrygroupid
							left join organisation o with (noLock) on v.entityTypeID = 2 and v.entityid = o.organisationid
							left join person p with (noLock) on v.entityTypeID = 0 and v.entityid = p.personid
							left join location l on p.locationid = l.locationid
							<cfloop list="#arguments.excludeEntityWithFlags#" index="flagID">
							<cfset flagStructure = application.com.flag.getFlagStructure(flagID)>		<!--- 2011-01-31 PPB changed from application.flag --->
							left outer join #flagStructure.flagType.dataTableFullName# f#flagID# with (noLock)
								on v.entityID = f#flagID#.entityID and v.entityTypeID =  <cf_queryparam value="#flagStructure.entityType.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
							</cfloop>
						where entitySpecialisationRuleCount >= numRulesToPass
							and entitySpecialisationID is not null and expiredDate is not null
							and v.active = 1
							and cg.countryMemberID = case when v.entityTypeID = 2 then o.countryid when v.entityTypeID = 0 then l.countryid else null end
							<cfloop list="#arguments.excludeEntityWithFlags#" index="flagID">
							 and f#flagID#.entityID is null
							</cfloop>
					)
				</cfquery>
				<!--- END:  NYB 2009-03-13 Stargate 2 --->

				<cfquery name="getEntitiesThatHaveJustBeenGivenSpecialisations" datasource="#application.siteDataSource#">
					select es.specialisationID,
						'phr_title_specialisation_' + convert(varchar, s.specialisationid) as code,
						'phr_description_specialisation_' + convert(varchar, s.specialisationid) as description,
						es.entityID,es.entityTypeID, es.passDate
					from entitySpecialisation es
						inner join Specialisation s with (noLock) on es.specialisationid = s.specialisationid
					<!--- NYB 2009-03-13 Stargate 2 - changed "es.created" to "es.lastUpdated" --->
					where es.lastUpdated = #createODBCDateTime(request.requestTime)#
				</cfquery>
				<!--- <- P-SOP020 --->
				<cfloop query="getEntitiesThatHaveJustBeenGivenSpecialisations">
					<cfscript>
						if (entityTypeID eq 2) {
							trainingContact = getTrainingContact(organisationID=entityID);
							if (trainingContact neq 0) {
								//NYB 2008-11-20 P-SOP011 - added ->
								form.Specialisation = getEntitiesThatHaveJustBeenGivenSpecialisations.Code;
								form.SpecialisationDescription = getEntitiesThatHaveJustBeenGivenSpecialisations.Description;
								form.PassDate = getEntitiesThatHaveJustBeenGivenSpecialisations.passDate;
								// <- P-SOP011
								application.com.email.sendEmail(personID=trainingContact,emailTextID='TrngSpecialisationAchieved');
							}
						}
					</cfscript>
				</cfloop>

				<!--- START: NYB 2009-03-13 Sophos Stargate 2 - added: --->
				<cfif structkeyexists(application.com.relayElearning,"extendInsertEntitySpecialisations")>
					<cfscript>
						application.com.relayElearning.extendInsertEntitySpecialisations();
					</cfscript>
				</cfif>
				<!--- END: NYB 2009-03-13 Sophos Stargate 2 --->

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.message = cfcatch.message&" "&cfcatch.detail>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="relayElearning Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>

			<cfreturn result>

		</cffunction>



		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-22 NJH Function that expires entity specialisations
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="expireInvalidEntitySpecialisations" hint="Expire specialisations for entities that no longer should have one" returnType="struct">
			<cfargument name="numRowsToProcess" type="numeric" default="100">

			<cfscript>
				var qryExpireEntitySpecialisations = "";
				var result = {isOk=true,message=""};
			</cfscript>

			<cftry>
				<cfquery name="qryExpireEntitySpecialisations" datasource="#application.siteDataSource#">
					select top #arguments.numRowsToProcess# entitySpecialisationID, passDate
					from vEntitySpecialisations v with (noLock)
					where entitySpecialisationID is not null
						and expiredDate is null
						and passDate is not null
						<!--- NYB 2009-03-13 Stargate 2 - replaced numRules with numRulesToPass: --->
						and ((entitySpecialisationRuleCount < numRulesToPass) or (active <> 1))
				</cfquery>

				<cfloop query="qryExpireEntitySpecialisations">
					<cfscript>
						application.com.relayElearning.expireEntitySpecialisation(entitySpecialisationID=entitySpecialisationID);
					</cfscript>
				</cfloop>

				<cfcatch>
					<cfset result.isOK=false>
					<cfset result.message = cfcatch.message&" "&cfcatch.detail>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="relayElearning Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>

			<cfreturn result>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-22 NJH Function that expires invalid certifications
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="expireInvalidCertifications" hint="Expire certifications that have gone past their duration">

			<cfscript>
				var qryGetInvalidCertifications = "";
			</cfscript>

			<cfquery name="qryGetInvalidCertifications" datasource="#application.siteDataSource#">
				select * from trngCertification where DATEADD(mm,duration,created) < GetDate() and active <> 0
			</cfquery>

			<cfloop query="qryGetInvalidCertifications">
				<cfscript>
					InactivateCertification(certificationID=certificationID);
				</cfscript>
			</cfloop>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-17 NJH Function that returns the training contact for an organisation
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getTrainingContact" hint="Returns the training contact for an org" returntype="numeric">
			<cfargument name="organisationID" type="numeric" required="true">
			<cfargument name="PersonID" type="numeric" required="false">

			<cfset var qryTrainingContact="">
			<cfset var TrainingContactID = 0>
			<cfset var TrainingContactUsergroup = application.com.settings.getSetting("ELEARNING.TRAININGCONTACTUSERGROUPNAME")> <!--- 2013/03/19	YMA		trainingcontactusergroup setting was declared incorrectly. --->

			<cfscript>
				//var qryTrainingContact=""; //2008-11-12 NYB P-SOP010 - moved to top
				if (application.com.flag.doesFlagExist(flagID='KeyContactsTraining')) {
					qryTrainingContact = application.com.flag.getFlagData(flagID='KeyContactsTraining',entityID=arguments.organisationID);
					if (qryTrainingContact.recordCount gt 0) {
						//return qryTrainingContact.data;  //2008-11-12 NYB P-SOP010 - replaced with:
						TrainingContactID = qryTrainingContact.data;
					}
				}
				//2008-11-12 NYB P-SOP010 - added ->
				if (structkeyexists(arguments,"PersonID")) {
					if (application.com.relayUserGroup.IsUserInUsergroup(Usergroup=TrainingContactUsergroup,PersonID=arguments.PersonID)) {
						TrainingContactID = arguments.PersonID;
					}
				}
				return TrainingContactID;
			</cfscript>

		</cffunction>


		<cffunction access="public" name="getModuleDetailsQry" hint="Returns the course file launcher url" returntype="query">
				<cfargument name="moduleID" type="numeric" required="true">
				<cfargument name="fileID" type="numeric" required="false">
				<cfscript>
					var getModuleDetails = "";
				</cfscript>

				<!--- NJH 2009-07-17 P-FNL069 - if file is secure, append .cfm to the filename --->
				<!--- NJH 2012/12/14 - 2013 Roadmap - pass in fileID, as I wanted to get file details before it was actually tied to the module. --->
				<!--- DCC 2014/09/18 - Added f.url, for service mobile site case 441623 --->
				<cfquery name="getModuleDetails" datasource="#application.siteDataSource#">
					select AICCFilename as AICCFilename, f.fileID,ft.path, ft.secure, f.filename, f.url,
						case when ft.secure = 1 then '#application.paths.securecontent#' else '#application.paths.content#' end + '\'+ isNull(ft.path,'') as absolutePath,
						'/content/' + isNull(ft.path,'') as relativePath,
						f.DeliveryMethod
						<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
						, trainingprogramID
						</cfif>
						<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
												Return new field 'coursewareTypeID' relevant to getCourseLauncherURL function --->
						,coursewareTypeID
						from trngModule tm
							<cfif not structKeyExists(arguments,"fileID")>
								with (noLock)left outer join
							<cfelse>,
							</cfif>
							(files f with (noLock) inner join fileType ft with (noLock) on f.fileTypeID=ft.fileTypeID)
							<cfif not structKeyExists(arguments,"fileID")>
								on tm.fileID = f.fileID
							</cfif>
					where moduleID=#arguments.moduleID#
						<cfif structKeyExists(arguments,"fileID")>
						and f.fileID = #arguments.fileID#
						</cfif>
				</cfquery>
				<cfreturn getModuleDetails>

			</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-17 NJH Function that returns a course launcher url

	            2012-01-31 NYB Case#426399 have added the ability to pass through module information as
	            arguments instead of calling getModuleDetailsQry everysingle time, because this
	            being called for every module returned by webservices/relayElearningWS.cfc/getCertificationItems
	            was causing it to return the result so slowly it was raised as a bug

	            2012-01-31 NYB Case#426399 have added the ability to pass through module information as
	            arguments instead of calling getModuleDetailsQry everysingle time, because this
	            being called for every module returned by webservices/relayElearningWS.cfc/getCertificationItems
	            was causing it to return the result so slowly it was raised as a bug
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getCourseLauncherUrl" hint="Returns the course file launcher url" returntype="string">
			<cfargument name="moduleID" type="numeric" required="true">
			<cfargument name="userModuleProgressID" type="numeric" required="false"> <!--- NJH 2009-07-20 P-FNL069 --->
			<cfargument name="personID" type="numeric" required="false"> <!--- NJH 2009-07-20 P-FNL069 --->
			<cfargument name="fileID" type="numeric" required="false"> <!--- NYB 2012-01-30 Case#426399 --->
			<cfargument name="filename" type="string" required="false"> <!--- NYB 2012-01-30 Case#426399 --->
			<cfargument name="deliveryMethod" type="string" required="false"> <!--- NYB 2012-01-30 Case#426399 --->
			<cfargument name="absolutePath" type="string" required="false"> <!--- NYB 2012-01-30 Case#426399 --->
			<cfargument name="relativePath" type="string" required="false"> <!--- NYB 2012-01-30 Case#426399 --->
			<cfargument name="secure" type="numeric" required="false"> <!--- NYB 2012-01-30 Case#426399 --->

			<cfscript>
				var courseFilePresenterURL="";
				var getModuleDetails = "";
				var additionalParams = "";
				var courseFileID = 0;
				var dumpNewUrl ="";
				var UsesUrl="";
				var AkamaiPartOne="";
				var getToken="";
				var AkamaiCDNURL="";
				var courseFilename = "";
				var courseDeliveryMethod="";
				var courseAbsolutePath = "";
				var courseRelativePath="";
				var courseSecure=0;
			</cfscript>


			<cfif structkeyexists(arguments,"secure") and structkeyexists(arguments,"absolutePath") and structkeyexists(arguments,"relativePath") and structkeyexists(arguments,"fileID") and isnumeric(arguments.fileID) and structkeyexists(arguments,"filename") and structkeyexists(arguments,"deliveryMethod")>
				<cfset courseFileID = arguments.fileID>
				<cfset courseFilename = arguments.filename>
				<cfset courseDeliveryMethod = arguments.deliveryMethod>
				<cfset courseAbsolutePath = arguments.absolutePath>
				<cfset courseRelativePath = arguments.relativePath>
				<cfset courseSecure = arguments.secure>
			<cfelse>
				<cfset getModuleDetails=getModuleDetailsQry(argumentCollection=arguments)>
				<cfif getModuleDetails.fileID neq "">
					<cfset courseFileID = getModuleDetails.fileID>
				</cfif>
				<cfset courseFilename = getModuleDetails.AICCFilename>
				<cfif structKeyExists(arguments,"fileName")>
					<cfset courseFilename = arguments.fileName>
				</cfif>
				<cfif isdefined("getModuleDetails.DeliveryMethod")>
					<cfset courseDeliveryMethod = getModuleDetails.deliveryMethod>
				</cfif>
				<cfset courseAbsolutePath = getModuleDetails.absolutePath>
				<cfset courseRelativePath = getModuleDetails.relativePath>
				<cfset courseSecure = getModuleDetails.secure>
			</cfif>

			<!--- LID 5753 NJH 2011-02-24 - get the local language/country file - nicked from elearningCoursesV2.cfm --->
			<cfif courseFileID neq 0>
				<cfset courseFileID = getLocalVersionFileID(fileID=courseFileID)>
			</cfif>

			<!--- NJH 2009-07-20 P-FNL069 passing in a module progress ID or a personID. Used for fileDisplay when a user is taking a course and is tracking the user module progress
				It was moved into this function so that it could get encrypted with the whole query string.
			 --->
			<cfif structKeyExists(arguments,"userModuleProgressID") and arguments.userModuleProgressID neq 0>
				<cfset additionalParams = "&userModuleProgressID=#arguments.userModuleProgressID#">
			<cfelseif structKeyExists(arguments,"personID") and arguments.personID neq 0>
				<cfset additionalParams = "&moduleID=#arguments.moduleID#&personID=#arguments.personID#">
			</cfif>

			<!--- if we have just a fileID, call fileGet.cfm --->
			<cfif courseFileID neq 0 and courseFilename eq "">
				<!--- STCR 2012-07-02 P-REL109 Phase 2 - appended additionalParams - if the file is a swf SCO then we need to pass this through to the SCORM API --->
				<!--- <cfset courseFilePresenterURL= application.com.security.encryptURL(url = "/fileManagement/fileGet.cfm?fileID=#courseFileID#", remove=false)> --->
				<cfset courseFilePresenterURL= application.com.security.encryptURL(url = "/fileManagement/fileGet.cfm?fileID=#courseFileID##additionalParams#", remove=false)>

			<!--- if we have a fileID and a filename, then the filename is a launcher... --->
			<cfelseif courseFileID neq 0 and courseFilename neq "">
				<!--- NJH 2009-07-17 P-FNL069 encrypt the url variables for file display --->

				<!--- 2011/12/01 - RMB- P-REL101 - Use DeliveryMethod to Akamai CDN parth when needed In Func "getCourseLauncherUrlgetCourseLauncherUrl" --->
				<cfif courseDeliveryMethod EQ 'AkamaiCDN'>
					<cfif fileExists("#courseAbsolutePath#\#courseFileID#\#courseFilename#")>
						<cfset dumpNewUrl = "#courseRelativePath#/#courseFileID#/#courseFilename#">
					<!--- now check the root directory --->
					<cfelseif fileExists("#courseAbsolutePath#\#courseFilename#")>
						<cfset dumpNewUrl = "#courseRelativePath#/#courseFilename#">
					<cfelse>
						<cfreturn "">
					</cfif>

						<cfset UsesUrl = "#dumpNewUrl#">
						<!-- Not passing sExtract = UsesExtract & nTime = UsenTime -->

						<cfif courseSecure EQ 1>
							<!--- 2012-05-18 - RMB - P-REL101 - changed call to "GetAkamaiToken" from "urlauth_gen_url also now passing in fileID to new function" --->
							<cfset GetAkamaiToken = application.com.AkamaiCDN.GetAkamaiToken(sUrl = UsesUrl, fileID = courseFileID)>
							<cfset AkamaiCDNURL = "#GetAkamaiToken#">
						<cfelse>
							<cfset AkamaiPartOne = application.com.settings.getsetting('files.Akamai.AkamaiPublicRoot')>
							<cfset AkamaiCDNURL = "http://#AkamaiPartOne##UsesUrl#">
						</cfif>

					<cfset courseFilePresenterURL = "#AkamaiCDNURL#">

				<!--- DeliveryMethod set to Local --->
				<cfelse>

					<cfif fileExists("#courseAbsolutePath#\#courseFileID#\#courseFilename#")>
						<cfset courseFilePresenterURL = application.com.security.encryptURL(url = "/fileManagement/fileDisplay.cfm?filePath=#courseRelativePath#/#courseFileID#/#courseFilename#&fileName=#courseFilename##additionalParams#", dontRemoveRegExp="filename")>
					<!--- now check the root directory --->
					<cfelseif fileExists("#courseAbsolutePath#\#courseFilename#")>
						<cfset courseFilePresenterURL = application.com.security.encryptURL(url = "/fileManagement/fileDisplay.cfm?filePath=#courseRelativePath#/#courseFilename#&fileName=#courseFilename##additionalParams#", dontRemoveRegExp="filename")>
					</cfif>

	            </cfif>

			<!--- the filename holds the url to the course file --->
			<cfelseif courseFileID eq 0 and courseFilename neq "" and (left(trim(courseFilename),7) eq "http://" or left(trim(courseFilename),8) eq "https://")>
				<cfset courseFilePresenterURL = courseFilename>
			<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
						if the coursewareTypeID is not the default and the filename is not empty assume third party API --->
			<cfelseif courseFileID eq 0 and courseFilename neq ""
					and (application.com.relayForms.getDefaultLookupListForField(fieldname='coursewareTypeID').lookupID neq getModuleDetails.coursewareTypeID and getModuleDetails.coursewareTypeID neq "")>
				<cfset courseFilePresenterURL = getCustomCoursewareLauncherURL(AICCFileName=courseFilename)>
			</cfif>

			<cfreturn courseFilePresenterURL>
		</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             2008-09-22 NJH Function that returns specialisation summary data
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="GetSpecialisationSummaryData" hint="Returns specialisation summary information">
			<cfargument name="sortOrder" required="no" default="Description">
			<cfargument name="entityID" required="no" type="numeric">
			<cfargument name="entityTypeID" required="no" type="numeric" default="2">
			<cfargument name="specialisationID" required="no" type="numeric">
			<cfargument name="returnOrgData" required="no" type="boolean" default="false">
			<cfargument name="restrictToUserCountryList" required="no" type="boolean" default="true">
			<cfargument name="validSpecialisationsOnly" required="no" type="boolean" default="false">

			<cfscript>
				var qryGetSpecialisationSummaryData = "";
				var specTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "specialisation", phraseTextID = "title");
				var specDescPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "specialisation", phraseTextID = "description");
			</cfscript>

			<!--- NYB 2009-02-03 Sop-Bug1338 - added Title --->
			<cfquery name="qryGetSpecialisationSummaryData" datasource="#application.siteDataSource#">
				select *
				from(
				select distinct entitySpecialisationID, code, --title,description,
					#specTitlePhraseQuerySnippets.select# as title,
					#specDescPhraseQuerySnippets.select# as description,
					status,pass_Date,expired_date,num_certifications_to_pass,num_certifications_passed,num_modules_to_pass,num_modules_passed
				<cfif arguments.returnOrgData>
					,o.organisationName as organisation_name,c.countryDescription as country
				</cfif>
				from vSpecialisationSummary v
				<cfif arguments.returnOrgData>
					inner join organisation o with (noLock) on o.organisationID = v.entityID and v.entityTypeID = 2
					inner join country c with (noLock) on o.countryID = c.countryID
					<cfif arguments.restrictToUserCountryList>
						<!---
						and c.countryID in (#request.relayCurrentUser.countryList#,#request.relayCurrentUser.regionListSomeRights#)
					 	--->
						#application.com.rights.getRightsFilterWhereClause(entityType="Specialisation",alias="v",regionList=request.relayCurrentUser.regionListSomeRights).whereClause#		<!--- 2012-07-25 PPB P-SMA001 --->
					</cfif>
				</cfif>
					#preservesinglequotes(specTitlePhraseQuerySnippets.join)#
					#preservesinglequotes(specDescPhraseQuerySnippets.join)#
				where 1=1
				<cfif isDefined("arguments.entityID")>
					and v.entityID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">
					and v.entityTypeID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityTypeID#">
				</cfif>
				<cfif isDefined("arguments.specialisationID")>
					and specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">
				</cfif>
				<cfif arguments.validSpecialisationsOnly>
					and status = 'Passed' and active=1
				</cfif>) as base
				where 1=1
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
				order by #arguments.sortOrder#
			</cfquery>
			<cfreturn qryGetSpecialisationSummaryData>
		</cffunction>


		<cffunction access="public" name="InactivateSpecialisation" hint="Inactivates a specialisation">
			<cfargument name="specialisationID" required="yes" type="numeric">
			<cfargument name="updateEntitySpecialisations" type="boolean" default="true">
			<cfargument name="sendNotificationEmail" type="boolean" default="#application.com.settings.getSetting('elearning.sendNotificationEmail')#">		<!--- 2013-02-08 PPB Case 433337 default to setting --->

			<cfscript>
				var qryInactivateSpecialisation = "";
				var argumentsStruct = structNew();
				var getEntitiesWithValidSpecialisations = "";
			</cfscript>

			<!--- START: NYB 2009-02-26 Sophos Stargate 2 - added because there's no way built in set notificationEmailTextID to true --->
			<!--- <cfif isDefined("request.sendNotificationEmail")>
				<cfset arguments.sendNotificationEmail = request.sendNotificationEmail>
			</cfif> --->
			<!--- <cfset arguments.sendNotificationEmail = application.com.settings.getSetting("elearning.sendNotificationEmail")> --->					<!--- 2013-02-08 PPB Case 433337 don't stomp over argument sent in with setting --->
			<!--- END: NYB 2009-02-26 --->

			<cftransaction action = "begin">
				<cftry>

					<cfquery name="qryInactivateSpecialisation" datasource="#application.siteDataSource#">
						if exists (select 1 from specialisation where specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">
							and active = 1)
							begin
								update specialisation set active = 0
								where specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">

								select @@rowCount as rowUpdated
							end
						else
							begin
								select 0 as rowUpdated
							end
					</cfquery>

					<cfif arguments.updateEntitySpecialisations and qryInactivateSpecialisation.rowUpdated gt 0>

						<cfquery name="getEntitiesWithValidSpecialisations" datasource="#application.siteDataSource#">
							select entitySpecialisationID from entitySpecialisation where specialisationID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.specialisationID#">
								and expiredDate is null
						</cfquery>

						<cfloop query="getEntitiesWithValidSpecialisations">
							<cfscript>
								application.com.relayElearning.expireEntitySpecialisation(entitySpecialisationID=entitySpecialisationID);
							</cfscript>
						</cfloop>
					</cfif>

					<cftransaction action="commit"/>

					<cfcatch type="Database">
						<cftransaction action="rollback"/>
					</cfcatch>
				</cftry>
			</cftransaction>

		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-09-22 NJH Function that expires entity specialisations
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="expireEntitySpecialisation" hint="Expire specialisation for an entity that no longer should have one">
			<cfargument name="entitySpecialisationID" type="numeric" required="yes">
			<cfargument name="expireDate" type="date" default="#request.requestTime#">
			<cfargument name="sendNotificationEmail" type="boolean" default="#application.com.settings.getSetting('elearning.sendNotificationEmail')#">		<!--- 2013-02-08 PPB Case 433337 default to setting --->

			<cfscript>
				var qryExpireEntitySpecialisation = "";
				var getEntityWithSpecialisationExpired = "";
				var trainingContact=0;
			</cfscript>

			<!--- START: NYB 2009-02-26 Sophos Stargate 2 - added because there's no way built in set notificationEmailTextID to true --->
			<!--- <cfif isDefined("request.sendNotificationEmail")>
				<cfset arguments.sendNotificationEmail = request.sendNotificationEmail>
			</cfif> --->
			<!--- <cfset arguments.sendNotificationEmail = application.com.settings.getSetting("elearning.sendNotificationEmail")> --->						<!--- 2013-02-08 PPB Case 433337 don't stomp over argument sent in with setting --->
			<!--- END: NYB 2009-02-26 --->

			<cfquery name="qryExpireEntitySpecialisation" datasource="#application.siteDataSource#">
				update entitySpecialisation set expiredDate = #createODBCDateTime(arguments.expireDate)#,
					lastUpdated = #createODBCDateTime(request.requestTime)#,
					lastUpdatedBy = #request.relayCurrentUser.userGroupID#
				where entitySpecialisationID = #arguments.entitySpecialisationID#
			</cfquery>

			<!--- NYB 2008-11-20 P-SOP011 - replaced -> ---
			<cfquery name="getEntityWithSpecialisationExpired" datasource="#application.siteDataSource#">
				select entityID,entityTypeID from entitySpecialisation where entitySpecialisationID = #arguments.entitySpecialisationID#
			</cfquery>
			!--- <- P-SOP011 - with -> --->
			<!--- NYB 2009-03-13 Stargate 2 - added ",s.active" to query --->
			<cfquery name="getEntityWithSpecialisationExpired" datasource="#application.siteDataSource#">
				select es.specialisationID,--'phr_' + s.titlePhraseTextID as Code, 'phr_' + s.descriptionPhraseTextID as Description,
				'phr_title_specialisation_' + convert(varchar, s.specialisationid) as code,
				'phr_description_specialisation_' + convert(varchar, s.specialisationid) as description,
				es.entityID,es.entityTypeID, es.expiredDate, es.passDate,s.active
				from entitySpecialisation es
				inner join Specialisation s with (noLock) on es.specialisationid = s.specialisationid
				where es.entitySpecialisationID = #arguments.entitySpecialisationID#
			</cfquery>
			<!--- <- P-SOP020 NYB 2008-11-20 --->

			<cfif arguments.sendNotificationEmail and getEntityWithSpecialisationExpired.recordCount gt 0>
				<cfloop query="getEntityWithSpecialisationExpired">
					<cfscript>
						if (entityTypeID eq 2) {
							trainingContact = getTrainingContact(organisationID=entityID);
							if (trainingContact neq 0) {
								//NYB 2008-11-20 P-SOP011 - added ->
								form.Specialisation = getEntityWithSpecialisationExpired.Code;
								form.SpecialisationDescription = getEntityWithSpecialisationExpired.Description;
								form.PassDate = getEntityWithSpecialisationExpired.passDate;
								form.ExpiredDate = getEntityWithSpecialisationExpired.expiredDate;
								// <- P-SOP011
								if (active eq 0) { //NYB 2009-03-13 Stargate 2 added
									application.com.email.sendEmail(personID=trainingContact,emailTextID='TrngSpecialisationExpired');
								//START: NYB 2009-03-13 Stargate 2 added:
								} else {
									application.com.email.sendEmail(personID=trainingContact,emailTextID='TrngSpecialisationUpdateRequired');
								}
								//END: NYB 2009-03-13
							}
						}
					</cfscript>
				</cfloop>
			</cfif>

			<!--- START: NYB 2009-03-13 Sophos Stargate 2 - added: --->
			<cfif structkeyexists(application.com.relayElearning,"extendExpireEntitySpecialisation")>
				<cfscript>
					application.com.relayElearning.extendExpireEntitySpecialisation(entitySpecialisationID=entitySpecialisationID,expireDate=expireDate);
				</cfscript>
			</cfif>
			<!--- END: NYB 2009-03-13 Sophos Stargate 2 --->

		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-10-24 NYB Function that returns all TrngUserModuleProgress, or just that for specified user/module
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getTrngUserModuleProgress" returntype="query" hint="Returns all TrngUserModuleProgresses">
			<cfargument name="ModuleID" type="numeric" required="no">
			<cfargument name="PersonID" type="numeric" required="no">
			<cfargument name="SortOrder" type="string" required="no" default="ModuleID">

			<cfset var qryTrngUserModuleProgress = "">

			<cfquery name="qryTrngUserModuleProgress" datasource="#application.siteDataSource#">
				select * from trngusermoduleprogress
				where 1=1
				<cfif structkeyexists(arguments,"ModuleID")>
					and ModuleID=#arguments.ModuleID#
				</cfif>
				<cfif structkeyexists(arguments,"PersonID")>
					and PersonID=#arguments.PersonID#
				</cfif>
				order by #arguments.SortOrder#
			</cfquery>

			<cfreturn qryTrngUserModuleProgress>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            Function that returns all vTrngUserModuleProgress,
				includes additional args to limit result.  Used in
				relay/elearning/reportTraining_UserCourse.cfm
				2011-02-18 NYB LHID3810
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getUserCourseModuleProgress" returntype="query" hint="Returns all TrngUserModuleProgresses">
			<cfargument name="CountryIDList" type="string" required="no">
			<cfargument name="AdditionalWhereClause" type="string" default="">
			<cfargument name="SortOrder" type="string" required="no" default="ModuleID">

			<cfset var qryTrngUserModuleProgress = "">

			<cfquery name="qryTrngUserModuleProgress" datasource="#application.siteDataSource#">
				select * from vTrngUserModuleProgress
				where 1=1
				<cfif structkeyexists(arguments,"CountryIDList")>
					and countryid  in ( <cf_queryparam value="#arguments.CountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
				#preservesinglequotes(arguments.AdditionalWhereClause)#
				order by #arguments.SortOrder#
			</cfquery>

			<cfreturn qryTrngUserModuleProgress>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            2008-10-24 NYB
	            Function that returns all module screens, or just screens belonging to moduleID passed
	 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getModuleScreens" returntype="query" hint="Returns all module screens, or just screens belonging to moduleID passed">
			<cfargument name="ModuleID" type="numeric" required="no">
			<cfargument name="SortOrder" type="string" required="no" default="ScreenName">

			<cfset var qryModuleScreens = "">

			<cfquery name="qryModuleScreens" datasource="#application.siteDataSource#">
				select ScreenID, ScreenTextID, ScreenName from Screens
				where entitytype='trngusermoduleprogress'
				<cfif structkeyexists(arguments,"ModuleID")>
					and ScreenTextID in (select ScreenTextID from trngModule where ModuleID=#arguments.ModuleID#)
				</cfif>
				order by #arguments.SortOrder#
			</cfquery>

			<cfreturn qryModuleScreens>
		</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        2009-01-21 NYB
		Returns true if user has met the eligibility rules of this certification
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getUserEligibility" returntype="boolean" hint="Returns true if user has met the eligibility rules of this certification">
			<cfargument name="CertificationID" type="numeric" required="yes">
			<cfargument name="PersonID" type="numeric" default="#request.relaycurrentuser.personid#">

			<cfset var qryEligibleCertifications = application.com.relayElearning.getEligibleCertifications(CertificationID=CertificationID,PersonID=PersonID)>

			<cfif qryEligibleCertifications.recordcount eq 0>
				<cfreturn false>
			<cfelse>
				<cfreturn true>
			</cfif>
		</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	        2009-01-21 NYB
			Returns certifications user is eligible for
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="getEligibleCertifications" returntype="query" hint="Returns certifications user is eligible for">
			<cfargument name="PersonID" type="numeric" default="#request.relaycurrentuser.personid#">
			<cfargument name="CertificationID" type="numeric" required="no">

			<cfscript>
				var qryEligibleCertifications = "";
			</cfscript>

			<cfquery name="qryEligibleCertifications" datasource="#application.sitedatasource#">
				select certificationid, Code,
					'title_trngCertification_' + convert(varchar, certificationid) as title,
					'description_trngCertification_' + convert(varchar, certificationid) as description
				from trngCertification
				where
				<cfif structkeyexists(arguments,"CertificationID")>
					CertificationID=#arguments.CertificationID# and
				</cfif>
				(
					certificationid not in (
						select appliedToID from EligibilityRule where appliedToTable='trngCertification'
					)
					or
					certificationid in (
						select er.appliedToID from EligibilityRule er
						inner join trngPersonCertification pc on er.requiredID=pc.certificationID
						inner join trngPersonCertificationStatus pcs on pc.statusID=pcs.statusID
						where er.appliedToTable='trngCertification'
						<cfif structkeyexists(arguments,"CertificationID")>
							and er.appliedToID=#arguments.CertificationID#
						</cfif>
						and personid=#arguments.PersonID# and pcs.statusTextID='Passed' and pc.passDate is not NULL
					)
				)
			</cfquery>
			<cfreturn qryEligibleCertifications>
		</cffunction>


	<!--- NJH 2009-02-05 P-SNY047 - create a transaction, used when a quiz or classroom is passed.  --->
	<cffunction name="submitElearningRewardsClaim" access="public" returntype="numeric" hint="Creates a rewards transaction">
		<cfargument name="userModuleProgressID" type="numeric" required="false">
		<cfargument name="moduleID" type="numeric" required="false">
		<cfargument name="personID" type="numeric" required="false">

		<cfscript>
			var getPointsValue = "";
			var thisModuleID = 0;
			var thisPersonID = 0;
			var totalPointsClaimed = 0;
			var transactionID = "";
			var getModuleAndPersonIDs = "";
			var getAccountInformation = "";
			var getPromotionID = "";
			var getExistingItemsAgainstPromotion = "";
		</cfscript>

		<!--- either userModuleProgressID must be passed in, or moduleID and personID --->
		<cfif not structKeyExists(arguments,"userModuleProgressID") and (not structKeyExists(arguments,"moduleID") and not structKeyExists(arguments,"personID"))>
			<cfreturn 0>
		</cfif>

		<cfif structKeyExists(arguments,"userModuleProgressID")>
			<cfquery name="getModuleAndPersonIDs" datasource="#application.siteDataSource#">
				select moduleID,personID from trngUserModuleProgress where userModuleProgressID = #arguments.userModuleProgressID#
			</cfquery>

			<cfset thisModuleID = getModuleAndPersonIDs.moduleID>
			<cfset thisPersonID = getModuleAndPersonIDs.personID>
		<cfelse>
			<cfset thisModuleID = arguments.moduleID>
			<cfset thisPersonID = arguments.personID>
		</cfif>

		<cfquery name="getPointsValue" datasource="#application.siteDataSource#">
			select isNull(points,0) as points from trngModule where moduleID =  <cf_queryparam value="#thisModuleID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfquery name="getAccountInformation" datasource="#application.siteDataSource#">
			select distinct ca.accountID from rwCompanyAccount ca with (noLock)
				inner join organisation o with (noLock) on ca.organisationID = o.organisationID
				inner join rwPersonAccount pa with (noLock) on pa.accountID = ca.accountID
				inner join person p with (noLock) on p.organisationID = o.organisationID and p.personID = pa.personID
			where
				p.personID =  <cf_queryparam value="#thisPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				pa.accountDeleted = 0 and
				pa.testAccount = 0 and
				ca.accountDeleted = 0 and
				ca.testAccount = 0
		</cfquery>

		<cfif getAccountInformation.recordCount eq 1 and getPointsValue.recordCount eq 1>

			<!--- get the system generated promotion for this module --->
			<cfquery name="getPromotionID" datasource="#application.siteDataSource#">
				select rwPromotionId from rwPromotion where rwPromotionCode =  <cf_queryparam value="Module#thisModuleID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and triggerPoint='adhoc' and createdBy=4
			</cfquery>

			<cfif getPromotionID.recordCount eq 1>

				<cfquery name="getExistingItemsAgainstPromotion" datasource="#application.siteDataSource#">
					select * from rwTransactionItems ti with (noLock)
						inner join rwTransaction t with (noLock) on ti.rwTransactionID = t.rwTransactionID
					where t.accountID =  <cf_queryparam value="#getAccountInformation.accountID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and t.personID =  <cf_queryparam value="#thisPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and ti.rwPromotionID =  <cf_queryparam value="#getPromotionID.rwPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfscript>
					// submit claim one has not already been submitted against that promotion
					if (getExistingItemsAgainstPromotion.recordCount eq 0) {
						transactionID = application.com.relayIncentive.RWAccruePoints(AccountId=getAccountInformation.accountID,AccruedDate=CreateODBCDateTime(request.requestTime),PointsAmount=0,PersonID=thisPersonID);
						application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=transactionID, itemID=0, quantity=1, points=getPointsValue.points, pointsType="PP", RWpromotionID=getPromotionID.rwPromotionId);
						totalPointsClaimed = application.com.relayIncentive.submitClaim(RWTransactionID = transactionID, RWTransactionTypeID = "AC", distiID = 0);
					}
				</cfscript>
			</cfif>
		</cfif>

		<cfreturn totalPointsClaimed>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Function:  getModules
		2009-06-04 NYB SOP010
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getModules" returntype="query" hint="Returns details of all modules for a Certification, or all, or just for specifed module">
		<cfargument name="CertificationID" type="string" required="no">
		<cfargument name="ModuleID" type="string" required="no">
		<cfargument name="GetFirst" type="numeric" required="no">
		<cfargument name="AdditionalFields" type="string" required="no">
		<cfargument name="SortOrder" type="string" required="no" default="tc.SortOrder,tc.certificationid,tm.SortOrder,tm.ModuleID">
		<cfargument name="incModulesUserDoesntHaveRightsTo" type="boolean" default="false">

		<cfset var getModuleQry = "">
		<cfset var ModuleSetPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModuleSet", phraseTextID = "description", baseTableAlias="tcr") />

		<cfquery name="getModuleQry" datasource="#application.sitedatasource#">
			SELECT
			<cfif structkeyexists(arguments,"GetFirst")>
				top #arguments.GetFirst#
			</cfif>
			tc.certificationid, tc.Code,
				'title_trngCertification_'+convert(varchar,tc.certificationid) as CertTitle,
				'description_trngCertification_'+convert(varchar,tc.certificationid) as CertDescription,		<!--- 2013-01-16 PPB Case 433337 missing comma --->
			tcr.certificationRuleID,tcr.numModulesToPass,/*tms.description as ModuleSetName,*/ tm.ModuleID,
				#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)#  as ModuleSetName, <!--- STCR 2013-04-11 CASE 432194 Translation of ModuleSet Title --->
				tm.AICCTitlePhraseTextID as OldModuleTitle,
				tm.AICCDescriptionPhraseTextID as OldModuleDescription,
				'Title_trngModule_'+convert(varchar,tm.ModuleID) as ModuleTitle,
				'Description_trngModule_'+convert(varchar,tm.ModuleID) as ModuleDescription
			<cfif structkeyexists(arguments,"AdditionalFields")>
				,#arguments.AdditionalFields#
			</cfif>
			FROM trngCertification tc
				inner join countrygroup cg on tc.countryid=cg.countrygroupid
				inner join trngCertificationRule tcr on tc.CertificationID=tcr.CertificationID and tcr.active=1 and dbo.dateAtMidnight(tcr.activationDate) <= getDate() <!--- 2013-05-14 STCR CASE 432193 respect rule activation date --->
				inner join trngModuleSet tms on tms.moduleSetID=tcr.moduleSetID and tms.active=1
				inner join trngModuleSetModule tmsm on tms.moduleSetID=tmsm.moduleSetID
				inner join trngModule tm on tmsm.moduleID=tm.moduleID
				#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)# <!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
			/*certifications allocated to countrygroup this user is in*/
			WHERE cg.countrymemberid  in ( <cf_queryparam value="#request.relaycurrentuser.countrylist#" CFSQLTYPE="cf_sql_integer"  list="true"> )
				<cfif not incModulesUserDoesntHaveRightsTo>
					and tm.moduleID not in (
						select rr.recordID from vRecordRights rr where rr.recordid = tm.moduleID and rr.personID != <cf_queryparam value="#arguments.visibleForPersonID#" CFSQLTYPE="cf_sql_integer"> and rr.entity = 'trngModule'
					)
				</cfif>
				<cfif structkeyexists(arguments,"CertificationID")>
					and tc.certificationid  in ( <cf_queryparam value="#arguments.CertificationID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
				<cfif structkeyexists(arguments,"ModuleID")>
					and tm.ModuleID  in ( <cf_queryparam value="#arguments.ModuleID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
			group by tc.certificationid, tc.code,
				tcr.certificationRuleID, tms.description_defaultTranslation, tm.AICCTitlePhraseTextID, <!--- 2013-04-11 STCR tms.description was previously included in GROUP BY --->
				tm.AICCDescriptionPhraseTextID, tm.ModuleID, tc.SortOrder,tm.SortOrder,tcr.numModulesToPass
				,#ModuleSetPhraseQuerySnippets.PHRASETABLEALIAS#.phrasetext  <!--- 2014-04-16 NYB Case 439441 added --->
			<cfif structkeyexists(arguments,"AdditionalFields")>
				,#arguments.AdditionalFields#
			</cfif>
			order by #arguments.SortOrder#
		</cfquery>

		<cfreturn getModuleQry>
	</cffunction>


	<!--- NJH 2009-07-06 P-FNL069 determines whether a user has access to various elearning entities

		NJH 2010-11-23 - LID 4828 if person passed through is training contact, then give them access.
	--->
	<cffunction name="getUserRightsForElearning" returntype="struct" access="public">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityType" type="string">
		<cfargument name="entityID" type="numeric">

		<cfscript>
			var getPersonCertification = "";
			var getUserModuleProgress = "";
			var getQuizTaken = "";
			var isTrainingContact = false;
			var getPersonOrg = "";
			var result = {edit=false,view=false};
			var getEntitySpecialisation = "";
		</cfscript>

		<cfquery name="getPersonOrg" datasource="#application.siteDataSource#">
			select organisationId from person where personID = #arguments.personID#
		</cfquery>

		<cfif arguments.personId eq getTrainingContact(organisationID=getPersonOrg.organisationID)>
			<cfset isTrainingContact = true>
		</cfif>

		<!--- TO DO: Should check statuses of various elearning entities and return appropriate rights levels --->
		<cfif request.relayCurrentUser.isLoggedIn and not request.relayCurrentUser.isInternal>
			<cfswitch expression="#arguments.entityType#">
				<cfcase value="trngPersonCertification">
					<cfquery name="getPersonCertification" dataSource="#application.siteDataSource#">
						select 1 from trngPersonCertification where
						<cfif not isTrainingContact>
							personID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfelse>
							 personID in (select personId from person where organisationid =  <cf_queryparam value="#getPersonOrg.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
						</cfif>
							and personCertificationID = <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

					<cfif getPersonCertification.recordCount gt 0>
						<cfset result.edit = true>
						<cfset result.view = true>
					</cfif>
				</cfcase>

				<cfcase value="trngUserModuleProgress">
					<cfquery name="getUserModuleProgress" dataSource="#application.siteDataSource#">
						select 1 from trngUserModuleProgress where
						<cfif not isTrainingContact>
							personID = #arguments.personID#
						<cfelse>
							 personID in (select personId from person where organisationid =  <cf_queryparam value="#getPersonOrg.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
						</cfif>
							and userModuleProgressID = #arguments.entityID#
					</cfquery>

					<cfif getUserModuleProgress.recordCount gt 0>
						<cfset result.edit = true>
						<cfset result.view = true>
					</cfif>
				</cfcase>

				<cfcase value="quizTaken">
					<cfquery name="getQuizTaken" dataSource="#application.siteDataSource#">
						select * from quizTaken where
						<cfif not isTrainingContact>
							personID = #arguments.personID#
						<cfelse>
							 personID in (select personId from person where organisationid =  <cf_queryparam value="#getPersonOrg.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
						</cfif>
							and quizTakenID = #arguments.entityID#
					</cfquery>

					<cfif getQuizTaken.recordCount gt 0>
						<cfset result.edit = true>
						<cfset result.view = true>
					</cfif>
				</cfcase>

				<cfcase value="entitySpecialisation">
					<cfquery name="getEntitySpecialisation" dataSource="#application.siteDataSource#">
						select * from entitySpecialisation es with (noLock) inner join person p with (noLock)
							on es.entityID = p.organisationID and es.entityTypeID = 2
						where
							<cfif not isTrainingContact>
								personID = #arguments.personID#
							<cfelse>
								 personID in (select personId from person where organisationid =  <cf_queryparam value="#getPersonOrg.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
							</cfif>
							and entitySpecialisationID = #arguments.entityID#
					</cfquery>

					<cfif getEntitySpecialisation.recordCount gt 0>
						<cfset result.edit = false>
						<cfset result.view = true>
					</cfif>
				</cfcase>
			</cfswitch>

		<cfelseif (request.relayCurrentUser.isAPIUser or request.relayCurrentUser.isLoggedIn) and request.relayCurrentUser.isInternal>
			<cfset result.edit = true>
			<cfset result.view = true>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!---
	2014-01-15 PPB Case 438541 re-wrote function (see svn if reqd)
	Notes:	This is for Courseware files, there is a different function used for the filelist(filemanager.getMostSuitedFileFromFamily) but we're not too sure why the separation
			Priority:	The file country has an exact match for the user country, then where there is a match on CountryGroup and then All Countries
						The file language matches the user language, then English.
			[Enhancements: we should consider adding Other Languages for the Country eg Switzerland before English;
			this should be achievable by adding the user's country.defaultLanguageId(list) in another CASE WHEN statement and adding it to the language filter clause (it would probably be easiest to get the defaultLanguageId list beforehand in a separate query);
			Probably we should deal with each exceptions specifically]
	 --->
	<cffunction access="public" name="getLocalVersionFileID" returntype="numeric" hint="Returns the local language/country file that is in the same fileFamily as the file with the specified fileID">
		<cfargument name="fileID" type="numeric" required="yes">

		<cfset var qryFiles = "">
		<cfset var LocalVersionFileID = "">

		<cfquery name="qryFiles" datasource="#application.siteDataSource#">
				SELECT f.fileid,f.languageid,cs.countryid, cg.CountryMemberID,
				CASE WHEN cs.CountryID=#request.relayCurrentUser.countryID# THEN -1 ELSE 0 END AS CountryMatch,					<!--- use -1 to order them high just to save using DESC  --->
				CASE WHEN cg.CountryMemberID=#request.relayCurrentUser.countryID# AND cg.CountryGroupID<>37 THEN -1 ELSE 0 END AS CountryGroupMatch,		<!--- 2014-01-27 PPB Case 438541 extended function to cope with 'AllCountries' (CountryGroupID=37) --->
				CASE WHEN cg.CountryMemberID=#request.relayCurrentUser.countryID# AND cg.CountryGroupID=37 THEN -1 ELSE 0 END AS AllCountriesGroupMatch,	<!--- 2014-01-27 PPB Case 438541 extended function to cope with 'AllCountries' (CountryGroupID=37) --->
				CASE WHEN f.LanguageID=#request.relayCurrentUser.languageID# THEN -1 ELSE 0 END AS LanguageMatch,
				CASE WHEN f.LanguageID=1 THEN -1 ELSE 0 END AS LanguageEnglish
				FROM files f
				LEFT OUTER JOIN (countryscope cs INNER JOIN countrygroup cg ON cg.CountryGroupID=cs.countryid) ON entity='Files' AND cs.entityid = f.fileid
				WHERE fileFamilyID = (SELECT f2.fileFamilyId FROM files f2 WHERE f2.fileid=#arguments.fileID# AND f2.fileFamilyId <> 0)
				AND (CountryMemberID IS NULL OR CountryMemberID = #request.relayCurrentUser.countryID#)			<!--- prevents files for irrelevant CountryGroups with the right language being included; this also covers an exact country match cos countries also have an entry for themselves in CountryGroup --->
				AND f.LanguageID IN (<cf_queryparam value="#request.relayCurrentUser.languageID#" CFSQLTYPE="cf_sql_integer" >,1) <!--- 1=English; safe to hard code --->
				ORDER BY CountryMatch,CountryGroupMatch,AllCountriesGroupMatch,LanguageMatch,LanguageEnglish 	<!--- 2014-01-27 PPB Case 438541 extended function to cope with 'AllCountries' (CountryGroupID=37) --->
			</cfquery>

		<cfif qryFiles.recordCount gt 0>
			<cfset LocalVersionFileID = qryFiles.fileId>
		<cfelse>
			<cfset LocalVersionFileID = arguments.fileID> 			<!--- return the original file if no other file was found --->
		</cfif>

		<cfreturn LocalVersionFileID>
	</cffunction>


	<!--- 2010-08-06 PPB P-SOP024 --->
	<cffunction name="checkModulePassed" returntype="boolean">
		<cfargument name="specialisationRuleID" type="numeric" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" default="2">
		<cfargument name="moduleID" type="numeric" required="true">

		<cfset var qrySpecialisationRuleModuleData="">
		<cfset var qryModuleData="">

		<cfquery name="qrySpecialisationRuleModuleData" datasource="#application.siteDataSource#">
			SELECT sr.SpecialisationRuleID, sr.numOfCertifications, count(tm.modulecode) AS numModulesPassed
			FROM                  SpecialisationRule AS sr WITH (noLock) INNER JOIN
			                      trngCertification AS tc WITH (noLock) ON sr.certificationID = tc.certificationID INNER JOIN
			                      trngCertificationRule AS tcr WITH (noLock) ON tcr.certificationID = tc.certificationID INNER JOIN
			                      trngModuleSetModule AS tmsm WITH (noLock) ON tmsm.moduleSetID = tcr.moduleSetID INNER JOIN
			                      trngModule AS tm WITH (noLock) ON tm.moduleID = tmsm.moduleID INNER JOIN
			                      trngUserModuleProgress AS tump WITH (noLock) ON tump.moduleID = tm.moduleID INNER JOIN
								  Person AS p WITH (noLock) ON tump.personID = p.PersonID INNER JOIN
			                      trngUserModuleStatus AS tums WITH (noLock) ON tums.statusID = tump.moduleStatus
			WHERE	sr.specialisationRuleID = #arguments.specialisationRuleID#
			AND 	sr.active <> 0
			<cfif arguments.entityTypeID eq 1>
				AND 	p.LocationID = #arguments.entityID#
			<cfelseif arguments.entityTypeID eq 2>
				AND 	p.OrganisationID = #arguments.entityID#
			</cfif>
 			AND		tump.moduleID = #arguments.moduleID#
 			AND 	tums.statusTextID = 'Passed'
			GROUP BY sr.SpecialisationRuleID, sr.numOfCertifications
		</cfquery>

		<!--- eg if numOfCertifications=3 for a specialisationRule then at least 3 people must have passed the module for the entity --->
		<cfquery name="qryModuleData" dbtype="query">
			SELECT 1 FROM qrySpecialisationRuleModuleData
			WHERE numModulesPassed >= numOfCertifications
		</cfquery>

		<cfreturn (qryModuleData.recordcount gt 0)>
	</cffunction>

	<!--- START: 2012-04-03 - Englex - P-REL109 - Add function to retreive training programs --->
	<cffunction access="public" name="getTrainingProgram" hint="Returns trngTrainingProgram information" returntype="Query" validvalues="true">
		<cfargument name="sortOrder" required="no" default="trainingprogramID">
		<!--- 2012-07-13	PJP added in sort order argument --->
		<cfargument name="trainingprogramID" required="false" default="0"> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->
		<cfargument name="forDropDown" type="boolean" required="false" default="0">
		<cfargument name="nonePhrase" type="string" required="false" default="phr_elearning_trainingProgram_all">
		<cfif not IsNumeric(arguments.trainingprogramID)><cfset arguments.trainingprogramID = 0></cfif> <!--- 2014-11-12 AHL Case 442565 Training Module Error --->

		<cfscript>
			var qry_get_TrainingProgram = "";
		</cfscript>

		<cfset TrainingProgramPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngTrainingProgram", phraseTextID = "title")>

		<!---2016/05/13 GCC 449503 forcing to useCFVersion="false" to avoid error--->
		<cfquery name="qry_get_TrainingProgram" datasource="#application.siteDataSource#">
			<cfif forDropDown and arguments.trainingprogramID eq 0> select 0 as value, '#arguments.nonePhrase#' as display,0 as trainingProgramID UNION</cfif>
			select <cfif forDropDown>trainingProgramID as value, trainingProgram as display, trainingProgramID<cfelse>*</cfif> from (
			select trainingProgramID, trainingProgramTextID
			,#preservesinglequotes(TrainingProgramPhraseQuerySnippets.select)# as trainingProgram
			from trngTrainingProgram with (nolock)
			#preservesinglequotes(TrainingProgramPhraseQuerySnippets.join)#
			<cfif arguments.trainingprogramID neq 0>
				where trainingprogramID  =  <cf_queryparam value="#arguments.trainingprogramID#" CFSQLTYPE="cf_sql_integer" useCFVersion="false">
			</cfif>
			) as baseqry
			where 1=1
			order by #arguments.sortOrder#
		</cfquery>

		<cfreturn qry_get_TrainingProgram />
	</cffunction>
	<!--- END: 2012-04-03 - Englex - P-REL109 - Add function to retreive training programs --->

	<!--- START: 2012-04-16 - Englex - P-REL109 - Delete checks function for training programs --->
	<cffunction access="public" name="deleteTrainingProgramChecks" hint="Checks if any entities have a specific Training Program assigned to it" returntype="Struct">
		<cfargument name="trainingprogramID" type="numeric" required="false" default="0">

		<cfscript>
			var qry_get_trngCourse = "";
			var qry_get_trngModule = "";
			var qry_get_trngModuleSet = "";
			var qry_get_trngCertification = "";
			var qry_get_specialisation = "";
			var returnStruct = structnew();
		</cfscript>

		<cfquery name="qry_get_trngCourse" datasource="#application.siteDataSource#">
			select 1 from trngCourse with (nolock)
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_get_trngModule" datasource="#application.siteDataSource#">
			select 1 from trngModule with (nolock)
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_get_trngModuleSet" datasource="#application.siteDataSource#">
			select 1 from trngModuleSet with (nolock)
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_get_trngCertification" datasource="#application.siteDataSource#">
			select 1 from trngCertification with (nolock)
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_get_specialisation" datasource="#application.siteDataSource#">
			select 1 from specialisation with (nolock)
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfset returnStruct.trngCourse = qry_get_trngCourse.recordcount />
		<cfset returnStruct.trngModule = qry_get_trngModule.recordcount />
		<cfset returnStruct.trngModuleSet = qry_get_trngModuleSet.recordcount />
		<cfset returnStruct.trngCertification = qry_get_trngCertification.recordcount />
		<cfset returnStruct.specialisation= qry_get_specialisation.recordcount />

		<cfreturn returnStruct />

	</cffunction>
	<!--- END: 2012-04-16 - Englex - P-REL109 - Delete function for training programs --->

	<!--- START: 2012-04-16 - Englex - P-REL109 - Delete function for training programs --->
	<cffunction access="public" name="deleteTrainingProgram" hint="Deletes Training Program and ophans records it was assigned to">
		<cfargument name="trainingprogramID" type="numeric" required="false" default="0">

		<cfscript>
			var qry_del_TrainingProgram = "";
			var qry_upd_trngCourse = "";
			var qry_upd_trngModule = "";
			var qry_upd_trngModuleSet = "";
			var qry_upd_trngCertification = "";
			var qry_upd_specialisation = "";
		</cfscript>

		<cfquery name="qry_upd_trngCourse" datasource="#application.siteDataSource#">
			update trngCourse
			set trainingprogramID = 0
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_upd_trngModule" datasource="#application.siteDataSource#">
			update trngModule
			set trainingprogramID = 0
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_upd_trngModuleSet" datasource="#application.siteDataSource#">
			update trngModuleSet
			set trainingprogramID = 0
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_upd_trngCertification" datasource="#application.siteDataSource#">
			update trngCertification
			set trainingprogramID = 0
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_upd_specialisation" datasource="#application.siteDataSource#">
			update specialisation
			set trainingprogramID = 0
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

		<cfquery name="qry_del_TrainingProgram" datasource="#application.siteDataSource#">
			delete trngTrainingProgram
			where trainingprogramID = <cf_queryparam value="#arguments.trainingprogramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfquery>

	</cffunction>
	<!--- END: 2012-04-16 - Englex - P-REL109 - Delete function for training programs --->

	<!--- STCR 2012-06-20 P-REL109 Phase 2 --->
	<!--- Ref: RW implementing a subset of http://www.adlnet.gov/wp-content/uploads/2011/07/SCORM.2004.3ED.ConfReq.v1.0.pdf --->
	<cffunction access="public" name="SCORMInitialize" hint="" returntype="struct">
		<cfargument name="personID" type="numeric" required="false" default="0" />
		<cfargument name="moduleID" type="numeric" required="false" default="0" />
		<cfargument name="userModuleProgressID" type="numeric" required="false" default="0" />

		<cfset var result = structNew() />
		<cfset var tempLocalVar = "" />
		<cfset result.cmi = structNew() />
		<cfset result.isOK = false />
		<!--- The SCORMInitialize function should only have been called after validating session.elearning.scorm.SCORMSessionKey  --->
		<cfset result.SCORMSessionKey = session.elearning.scorm.SCORMSessionKey /><!--- Should not need to return this any more, however relaySCORM.cfm may still be expecting it --->
		<cfset structDelete(session.elearning.scorm, "QuizTakenID") /><!--- 2012-09-17 STCR Ensure that we do not map to the QuizTakenID of a previously taken SCO. We do not set this during fileDisplay.cfm along with the other RW IDs, therefore we need to reset this here because a previous SCO might not have called terminate() on closure, so an old value might be present.  --->

		<cfset result.cmi["_version"] = javaCast( "string", "1.0.0" ) /> ---><!--- plain old = "1.0" was causing an error - possibly because CF was assuming float type instead of string --->
		<cfset structAppend(result,SCORMParseManifest(userModuleProgressID=arguments.userModuleProgressID,ModuleID=arguments.moduleID)) />

		<!--- REQ_57 - RW not supporting cmi.comments_from_learner.* --->

		<!--- REQ_58 - RW not supporting cmi.comments_from_lms.* --->

		<!---
			REQ_59 The LMS shall implement the cmi.completion_status data model element.
				REQ_59.1 The LMS shall implement the cmi.completion_status data model element as
				read/write.
				REQ_59.2 The LMS shall implement the cmi.completion_status data model element as a state
				consisting of the following vocabulary tokens:
				? completed
				? incomplete
				? not attempted
				? unknown
				REQ_59.3 The LMS shall initialize the value of the cmi.completion_status data model
				element to the default value of unknown.

				RW not implementing Sequencing, so ignoring 59.4:
				REQ_59.4 The LMS shall implement the cmi.completion_status such that it has the following
				effect on sequencing behaviors:
					REQ_59.4.1  If the SCO or LMS sets the cmi.completion_status of the SCO to unknown, then
					the LMS? sequencing implementation shall behave as if the Attempt Progress Status
					for the learning activity associated with the SCO is false.
					REQ_59.4.2  If the SCO or LMS sets the cmi.completion_status of the SCO to completed, then
					the LMS? sequencing implementation shall behave as if the Attempt Progress Status
					for the learning activity associated with the SCO is true, and the Attempt
					Completion Status for the learning activity associated with the SCO is true.
					REQ_59.4.3  If the SCO or LMS sets the cmi.completion_status of the SCO to incomplete,
					then the LMS? sequencing implementation shall behave as if the Attempt Progress
					Status for the learning activity associated with the SCO is true, and the Attempt
					Completion Status for the learning activity associated with the SCO is false.
					REQ_59.4.4  If the SCO or LMS sets cmi.completion_status of the SCO to not attempted,
					then the LMS? sequencing implementation shall behave as if the Attempt Progress
					Status for the learning activity associated with the SCO shall be true and the
					Attempt Completion Status for the learning activity associated with the SCO shall
					be false.

				REQ_59.5 The LMS shall evaluate the value of the cmi.completion_status data model element
				and return the result in the response to a GetValue() request according to the
				following requirements:
					REQ_59.5.1 If a cmi.completion_threshold is defined for the SCO and the
					cmi.progress_measure data model element's value is set by the SCO and the value
					is less than the cmi.completion_threshold data model element's value, then the
					LMS shall evaluate and return the value of incomplete.

					REQ_59.5.2 If a cmi.completion_threshold  is defined for the SCO and the
					cmi.progress_measure data model element's value is set by the SCO and the value
					is greater than or equal to the cmi.completion_threshold data model element's
					value, then the LMS shall evaluate and return the value of completed.

					REQ_59.5.3 If a cmi.completion_threshold is defined for the SCO and the
					cmi.progress_measure data model element is never set by the SCO, the LMS shall
					evaluate and return the value of unknown.

					REQ_59.5.4 If no cmi.completion_threshold is defined for the SCO the LMS shall rely on the
					value set for the cmi.completion_status data model element by the SCO and return
					that value.  If no value was set by the SCO for the cmi.completion_status data
					model element then the LMS shall return unknown. --->

		<!--- Gawain and Nat agreed that completion_status does not map to any specific RW measure, but the success_status we map to tump --->
		<cfset result.cmi["completion_status"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName="cmi.completion_status") />
		<cfif result.cmi["completion_status"] eq "">
			<cfset result.cmi["completion_status"] = "unknown" />
		</cfif>

		<!--- Veronica and Gawain agreed at workshop that in RW this should be hard-coded as credit.
			REQ_61 The LMS shall implement the cmi.credit data model element.
			REQ_61.1 The LMS shall implement the cmi.credit data model element as read-only.
			REQ_61.2 The LMS shall implement the cmi.credit data model element as a state consisting of
			the following vocabulary tokens:
			? credit
			? no-credit
			REQ_61.3 The LMS shall initialize the value of the cmi.credit data model element to the
			default value of credit. --->
		<cfset result.cmi["credit"] = "credit" />

		<!---
			REQ_62 The LMS shall implement the cmi.entry data model element.
			REQ_62.1 The LMS shall implement the cmi.entry data model element as read-only.
			REQ_62.2 The LMS shall implement the cmi.entry data model element as a state consisting of
			the following vocabulary tokens:
			? ab-initio
			? resume
			? "" (empty characterstring)
			REQ_62.3 The LMS shall initialize the cmi.entry data model element based on the following
			rules:
				REQ_62.3.1 If this is the first learner session on a learner attempt, then the LMS shall set the
				cmi.entry to ab-initio prior to initial launch of the SCO.
				ADL Note:  Upon a subsequent call to GetValue(cmi.entry), the LMS shall
				return ab-initio.
				REQ_62.3.2 If the learner attempt on the SCO is being resumed from a suspended learner session
				(cmi.exit is set to suspend), then the LMS shall initialize the cmi.entry value to
				resume.
				ADL Note:  Upon a subsequent call to GetValue(cmi.entry), the LMS shall
				return resume.
				REQ_62.3.3 For all other conditions, the LMS shall set the cmi.entry to an empty characterstring
				("").
			--->
		<cfset tempLocalVar = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.exit') />
		<cfif tempLocalVar eq "suspend">
			<cfset result.cmi["entry"] = "resume" />
		<cfelseif tempLocalVar eq "">
			<cfset result.cmi["entry"] = "ab-initio" />
		<cfelse>
			<cfset result.cmi["entry"] = "" />
		</cfif>


		<!---
			REQ_64 The LMS shall support at least the SPM of 250 interactions.
			REQ_64.1 The LMS shall implement the cmi.interactions._children data model element.
			REQ_64.1.1 The LMS shall implement the cmi.interactions._children data model element as
			read-only.
			REQ_64.1.2 The LMS shall implement the cmi.interactions._children data model element as a
			characterstring.
			REQ_64.1.3 If a SCO invokes a GetValue() request to retrieve the cmi.interactions._children,
			then the LMS shall return a comma-separated list of all cmi.interactions child data
			model elements
			id
			type
			objectives
			timestamp
			correct_responses
			weighting
			learner_response
			result
			latency
			description
			ADL NOTE:  The order of these values is not significant. --->
		<cfset result.cmi["interactions._children"] = "id,type,objectives,timestamp,correct_responses,weighting,learner_response,result,latency,description" />

		<!---
			REQ_64.2 The LMS shall implement the cmi.interactions._count data model element.
			REQ_64.2.1 The LMS shall implement the cmi.interactions._count data model element as readonly.
			REQ_64.2.2 The LMS shall implement the cmi.interactions._count data model element as a
			non-negative integer.
			REQ_64.2.3 If a SCO invokes a GetValue() request to retrieve the cmi.interactions._count, then
			the LMS shall return the number of interactions currently stored by the LMS. --->
		<!--- <cfset result.cmi["interactions._count"] = 0 /> ---><!--- We should support a minimum of 250 interactions. The api already defaults this to zero, no need to set it here. --->

		<!---
			REQ_66 The LMS shall implement the cmi.learner_id data model element.
			REQ_66.1 The LMS shall implement the cmi.learner_id data model element as read-only.
			REQ_66.2 The LMS shall implement the cmi.learner_id data model element as a
			long_identifer_type with an SPM of 4000 characters.
			REQ_66.3 The LMS shall be responsible for initializing the cmi.learner_id.
			ADL NOTE: How this is done is currently outside the scope of SCORM (e.g., this
			is typically handled via a learner registration system within the LMS).  --->
		<cfset result.cmi["learner_id"] = request.relaycurrentuser.personid />

		<!---
			REQ_67 The LMS shall implement the cmi.learner_name data model element.
			REQ_67.1 The LMS shall implement the cmi.learner_name data model element as read-only.
			REQ_67.2 The LMS shall implement the cmi.learner_name data model element as a
			localized_string_type with an SPM of 250 characters.
			REQ_67.3 The LMS shall be responsible for initializing the cmi.learner_name.
			ADL NOTE: How this is done is currently outside the scope of SCORM (e.g., this
			is typically handled via a learner registration system within the LMS). --->
		<cfset result.cmi["learner_name"] = request.relaycurrentuser.person.firstname &' '& request.relaycurrentuser.person.lastname />

		<cfset result.cmi["learner_preference.language"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.learner_preference.language') />
		<cfif result.cmi["learner_preference.language"] eq "">
			<cfif isDefined("request.relaycurrentuser.languageISOCode") and request.relaycurrentuser.languageISOCode neq "">
				<cfset result.cmi["learner_preference.language"] = request.relaycurrentuser.languageISOCode />
			<cfelse>
				<cfset result.cmi["learner_preference.language"] = "" />
			</cfif>
		</cfif>

		<cfset result.cmi["learner_preference.delivery_speed"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.learner_preference.delivery_speed') />
		<cfif result.cmi["learner_preference.delivery_speed"] eq "" >
			<cfset result.cmi["learner_preference.delivery_speed"] = 1 />
		</cfif>

		<cfset result.cmi["learner_preference.audio_captioning"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.learner_preference.audio_captioning') />
		<cfif result.cmi["learner_preference.audio_captioning"] eq "" >
			<cfset result.cmi["learner_preference.audio_captioning"] = 0 />
		</cfif>

		<cfset result.cmi["learner_preference.audio_level"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.learner_preference.audio_level') />
		<cfif result.cmi["learner_preference.audio_level"] eq "" >
			<cfset result.cmi["learner_preference.audio_level"] = 1 />
		</cfif>

		<cfset result.cmi["location"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.location') />
		<cfif result.cmi["location"] eq "" >
			<cfset result.cmi["location"] = 0 />
		</cfif>

		<!---
			REQ_71 The LMS shall implement the cmi.mode data model element.
			REQ_71.1 The LMS shall implement the cmi.mode data model element as read-only.
			REQ_71.2 The LMS shall implement the cmi.mode data model element as a state consisting of
			the following vocabulary tokens:
			? browse
			? normal
			? review
			REQ_71.3 If a mechanism is not in place to support different modes, then the LMS shall
			initialize the cmi.mode data model element to the default value, normal.
			REQ_71.4  If the cmi.mode value is browse or review, the LMS shall treat any data sent by
			the SCO as informative (in order to make sequencing decisions).  Whether or not an
			LMS persists any of the data sent by the SCO, while in a mode of review or browse,
			is outside the scope of the SCORM.
		 --->
		<!--- <cfset result.cmi["mode"] = "normal" /> ---> <!--- RW only supporting "normal" mode, so we will already have initialized this to "normal" in the js API --->

		<!---
			REQ_72.1.3 If a SCO invokes a GetValue() request to retrieve the cmi.objectives._children,
			then the LMS shall return a comma-separated list of all objectives child data model
			elements:
			id
			score
			success_status
			completion_status
			progress_measure
			description
			ADL NOTE:  The order of these values is not significant.
		 --->
		<cfset result.cmi["objectives._children"] = "id,score,success_status,completion_status,progress_measure,description" />
		<!--- Other objectives elements to be initialized by parsing manifest --->

		<!---
			REQ_76 The LMS shall implement the cmi.session_time data model element.
			REQ_76.1 The LMS shall implement the cmi.session_time data model element as write-only.
			REQ_76.2 The LMS shall implement the cmi.session_time data model element a timeinterval
			(seconds, 10, 2).
			REQ_76.3 Since a SCO is not required to set a value for this data model element (not required
			to keep track of the session time), an LMS shall keep track of session time from the
			time the LMS launches the SCO.  If the SCO reports a different session time, then
			the LMS shall use the session time as reported by the SCO instead of the session
			time as measured by the LMS.
			REQ_76.4 When the SCO issues the Terminate("") or the user navigates away, the LMS shall
			take the last cmi.session_time that the SCO set (if there was a set) and accumulate
			this time to the cmi.total_time.
			REQ_76.5 If there are additional learner sessions within a learner attempt, the cmi.session_time
			shall be reset at the beginning of each additional learner session within the learner
			attempt.  --->
		<!--- Clear any previous cmi.session_time set by this SCO --->
		<cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "cmi.session_time", ElementValue="") />
		<cfset session.elearning.scorm.SCORMSessionStartTime = request.relaycurrentuser.requestTime />
		<!--- <cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "rw.SCORMSessionStartTime", ElementValue="") /> --->

		<!--- TODO if manifest parsing then we could implement REQ_77.* - success_status vs scaled_passing_score vs score.scaled --->

		<!---
			REQ_78 The LMS shall implement the cmi.suspend_data data model element.
			REQ_78.1 The LMS shall implement the cmi.suspend_data data model element as read/write.
			REQ_78.2 The LMS shall implement the cmi.suspend_data data model element as a
			characterstring with an SPM of 64000 characters.
		 --->
		<cfset result.cmi["suspend_data"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.suspend_data') />
		<cfif result.cmi["suspend_data"] eq "" >
			<cfset structDelete(result.cmi,"suspend_data") />
			<!--- <cfset result.cmi["suspend_data"] = "" /> --->
		</cfif>

		<cfset result.cmi["total_time"] = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.total_time') />
		<cfif result.cmi["total_time"] eq "" >
			<cfset structDelete(result.cmi,"total_time") />
			<!--- <cfset result.cmi["total_time"] = "PT0S" /> --->
		</cfif>

		<cftry>
			<cfif arguments.personID eq 0>
				<cfset arguments.personID = request.relaycurrentuser.personID />
			</cfif>

			<cfset result.isOK = true />
			<cfcatch>
				<cfset result.isOK = false />
			</cfcatch>
		</cftry>

		<cfreturn result />
	</cffunction>

	<cffunction access="public" name="SCORMTerminate" hint="" returntype="struct">
		<cfargument name="personID" type="numeric" required="false" default="#request.relaycurrentuser.personID#" />
		<cfargument name="userModuleProgressID" type="numeric" required="true" />

		<cfset var result = structNew() />
		<cfset var SCORMTotalTime = "" />
		<cfset var SCORMSessionTime = "" />
		<cfset var SessionTimeHours = 0 />
		<cfset var SessionTimeMinutes = 0 />
		<cfset var SessionTimeSeconds = 0 />
		<cfset var TotalTimeHours = 0 />
		<cfset var TotalTimeMinutes = 0 />
		<cfset var TotalTimeSeconds = 0 />
		<cfset result.isOK = true />

		<cftry>
			<!---
				REQ_76.4 When the SCO issues the Terminate("") or the user navigates away, the LMS shall
				take the last cmi.session_time that the SCO set (if there was a set) and accumulate
				this time to the cmi.total_time.
			 --->
			<cfset SCORMSessionTime = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.session_time') />
			<cfset SCORMTotalTime = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='cmi.total_time') />
			<cfset TotalTimeHours = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='rw.TotalTimeHours') />
			<cfset TotalTimeMinutes = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='rw.TotalTimeMinutes') />
			<cfset TotalTimeSeconds = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName='rw.TotalTimeSeconds') />

			<cfif SCORMSessionTime eq "">
				<!--- calculate SCORMSessionTime duration if cmi.session_time not provided by SCO --->
				<cfif isDefined("session.elearning.scorm.SCORMSessionStartTime") and IsDate(session.elearning.scorm.SCORMSessionStartTime)
					and isDefined("request.relaycurrentuser.requestTime") and IsDate(request.relaycurrentuser.requestTime)>
					<cfset SessionTimeHours = DateDiff('h', session.elearning.scorm.SCORMSessionStartTime, request.relaycurrentuser.requestTime) />
					<cfset SessionTimeMinutes = DateDiff('n', session.elearning.scorm.SCORMSessionStartTime, request.relaycurrentuser.requestTime) % 60 />
					<cfset SessionTimeSeconds = DateDiff('s', session.elearning.scorm.SCORMSessionStartTime, request.relaycurrentuser.requestTime) % 60 />
					<cfset SCORMSessionTime &= "PT" />
					<cfset SCORMSessionTime &= "#SessionTimeHours#" &"H" />
					<cfset SCORMSessionTime &= "#SessionTimeMinutes#" &"M" />
					<cfset SCORMSessionTime &= "#SessionTimeSeconds#" &"S" />
				<cfelse>
					<cfset SessionTimeHours = 0 />
					<cfset SessionTimeMinutes = 0 />
					<cfset SessionTimeSeconds = 0 />
					<cfset SCORMSessionTime &= "PT0S" />
				</cfif>
				<cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "cmi.session_time", ElementValue="#SCORMSessionTime#") />
			<cfelse>
				<!--- Use cmi.session_time provided by SCO --->
			</cfif>
			<cfif SCORMTotalTime eq ""><!--- If there is no previous value for total_time then assume this is the first session, therefore total_time = session_time --->
				<cfset SCORMTotalTime = SCORMSessionTime />
				<cfset TotalTimeHours = SessionTimeHours />
				<cfset TotalTimeMinutes = SessionTimeMinutes />
				<cfset TotalTimeSeconds = SessionTimeSeconds />
				<cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "cmi.total_time", ElementValue="#SCORMTotalTime#") />
				<cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "rw.TotalTimeHours", ElementValue="#TotalTimeHours#") />
				<cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "rw.TotalTimeMinutes", ElementValue="#TotalTimeMinutes#") />
				<cfset upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "rw.TotalTimeSeconds", ElementValue="#TotalTimeSeconds#") />
			<cfelse>
				<!--- Else Add the session_time to the previous total_time --->
			</cfif>

			<cfcatch>
				<cfset result.isOK = false />
			</cfcatch>
		</cftry>

		<cftry>
			<cfset SCORMDataToRelayQuiz(userModuleProgressID=arguments.userModuleProgressID, moduleID=session.elearning.scorm.currentModuleID, fileID=session.elearning.scorm.currentFileID, personID=arguments.personID) />
			<cfset result.isOK = true />
			<cfcatch>
				<cfset result.isOK = false />
			</cfcatch>
		</cftry>

		<cfset session.elearning.scorm.currentUserModuleProgressID = 0 />
		<cfset session.elearning.scorm.currentModuleID = 0 />
		<cfset session.elearning.scorm.currentFileID = 0 />
		<cfset structDelete(session.elearning.scorm, "QuizTakenID") />

		<cfreturn result />
	</cffunction>

	<cffunction access="private" name="getCoursewareData" hint="Retrieve persisted SCO data" returntype="string">
		<cfargument name="userModuleProgressID" required="true" type="numeric" />
		<cfargument name="ElementName" required="true" type="string" />
		<cfargument name="ModuleID" required="false" default="0" type="numeric" />
		<cfargument name="FileID" required="false" default="0" type="numeric" />
		<cfargument name="PersonID" required="false" type="numeric" default="#request.relaycurrentuser.personid#" />

		<cfset var getCoursewareData = "" />

		<cfquery name="getCoursewareData" datasource="#application.sitedatasource#">
			select tcd.ElementValue
			from trngCoursewareData tcd
			right join trngUserModuleProgress tump with(nolock) on tcd.ModuleID = tump.ModuleID
			and tcd.personID = tump.personID and tcd.personID =  <cf_queryparam value="#val(arguments.PersonID)#" CFSQLTYPE="cf_sql_integer" >
			and tcd.ElementName=<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.ElementName#">
			where tump.userModuleProgressID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.userModuleProgressID#">
		</cfquery>

		<cfreturn getCoursewareData.ElementValue />

	</cffunction>

	<cffunction access="private" name="upsertCoursewareData" hint="Persist SCO data" returntype="boolean">
		<cfargument name="userModuleProgressID" required="true" type="numeric" />
		<cfargument name="FileID" required="false" type="numeric" />
		<cfargument name="PersonID" required="false" type="numeric" default="#request.relaycurrentuser.personid#" />
		<cfargument name="ElementName" required="true" type="string" />
		<cfargument name="ElementValue" required="true" type="string" />

		<cfset var getCoursewareData = "" />
		<cfset var setCoursewareData = "" />
		<cfset var returnValue = false />

		<cfif (not structKeyExists(arguments,"FileID")) and isDefined("session.elearning.scorm.currentFileID") and isNumeric(session.elearning.scorm.currentFileID) and session.elearning.scorm.currentFileID gt 0>
			<cfset arguments.FileID = session.elearning.scorm.currentFileID />
		<cfelseif not structKeyExists(arguments,"FileID")>
			<cfset arguments.FileID = 0 />
		<cfelseif structKeyExists(arguments,"FileID") and not isNumeric(arguments.FileID)>
			<cfset arguments.FileID = 0 />
		</cfif>

		<cfif len(trim(arguments.userModuleProgressID)) gt 0 and isNumeric(arguments.userModuleProgressID)>
			<cftransaction>
				<!--- Use an outer join so that we can use a single query to both verify the tump ID and check if there is an existing coursewareData record to update --->
				<cfquery name="getCoursewareData" datasource="#application.sitedatasource#">
					select tump.userModuleProgressID,tump.ModuleID,tump.PersonID,tcd.CoursewareDataID,tcd.FileID,tcd.ElementName,tcd.ElementValue
					from trngCoursewareData tcd
					right join trngUserModuleProgress tump with(nolock) on tcd.ModuleID = tump.ModuleID
					and tcd.personID = tump.personID and tcd.personID =  <cf_queryparam value="#val(arguments.PersonID)#" CFSQLTYPE="cf_sql_integer" >
					and tcd.ElementName=<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.ElementName#">
					and tcd.FileID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.FileID#">
					where tump.userModuleProgressID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.userModuleProgressID#">
				</cfquery>
				<!--- If valid userModuleProgressID and CoursewareData row exists for this person+module+element, then update it --->
				<cfif getCoursewareData.recordCount eq 1 and getCoursewareData.CoursewareDataID neq "" and getCoursewareData.PersonID neq "" and getCoursewareData.ModuleID neq "">
					<cfquery name="setCoursewareData" datasource="#application.sitedatasource#">
						update trngCoursewareData
						set ElementValue = <cf_queryparam cfsqltype="cf_sql_longvarchar" value="#arguments.ElementValue#">
							,LastUpdated = getDate()
						where PersonID = <cf_queryparam cfsqltype="cf_sql_integer" value="#getCoursewareData.PersonID#">
						and ModuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#getCoursewareData.ModuleID#">
						and ElementName = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.ElementName#">
						and FileID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.FileID#">
					</cfquery>
					<cfset returnValue = true />
				<!--- Else if valid userModuleProgressID but no value held for this person+module+element, then insert new row --->
				<cfelseif getCoursewareData.recordCount eq 1 and getCoursewareData.CoursewareDataID eq "">
					<cfquery name="setCoursewareData" datasource="#application.sitedatasource#">
						insert into trngCoursewareData (moduleID,personID,fileID,ElementName,ElementValue,Created,LastUpdated)
						values(
							<cf_queryparam cfsqltype="cf_sql_integer" value="#getCoursewareData.ModuleID#">
							,<cf_queryparam cfsqltype="cf_sql_integer" value="#getCoursewareData.PersonID#">
							,<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.FileID#">
							,<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.ElementName#">
							,<cf_queryparam cfsqltype="cf_sql_longvarchar" value="#arguments.ElementValue#">
							,getDate()
							,getDate()
							)
						<!--- do we need FileID if we are using moduleID, maybe just in case file is changed later? --->
					</cfquery>
					<cfset returnValue = true />
				<!--- Else invalid userModuleProgressID --->
				<cfelse>
					<!--- Handle error if SCORM Session does not correspond to tump? --->
					<cfset returnValue = false />
				</cfif>
			</cftransaction>
		</cfif>
		<cfreturn returnValue />
	</cffunction>

	<cffunction name="SCORMSetValue" access="public" returntype="struct">
		<cfargument name="userModuleProgressID" required="true" type="string" />
		<cfargument name="ElementName" required="true" type="string" />
		<cfargument name="ElementValue" required="true" type="string" />

		<cfset var result = structNew() />
		<cfset var ElementValidValues = "" /><!--- Could be good to factor valid values in to one large struct keyed on ElementName? --->
		<cfset var getModuleInfo = "" />
		<cfset var tempValue = "" />
		<cfset var tempStruct = structNew() />
		<cfset var simpleWriteMappingsList = "cmi.exit," />
		<cfset simpleWriteMappingsList &= "cmi.learner_preference.language,cmi.learner_preference.delivery_speed," />
		<cfset simpleWriteMappingsList &= "cmi.learner_preference.audio_captioning,cmi.learner_preference.audio_level," />
		<cfset simpleWriteMappingsList &= "cmi.location,cmi.progress_measure,cmi.score.min,cmi.score.max," />
		<cfset simpleWriteMappingsList &= "cmi.session_time,cmi.suspend_data,adl.nav.request" />
		<!--- session_time is a simple value but note that it is reset during initialize() and is added to total_time during terminate() --->
		<cfset result.isOK = false />

		<cftry>
			<cfset result.isOK = true />
			<!--- First handle all the simple mappings --->
			<cfif listFind(simpleWriteMappingsList,arguments.ElementName)>
				<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
			<cfelse>
				<!--- Then deal with the more complex mappings on a case by case basis, with additional helper functions where appropriate --->
				<cfswitch expression="#arguments.ElementName#">
					<!--- Note that in SCORM 2004, cmi.completion_status is a measure of completeness of attempt, but pass/fail is indicated by cmi.success_status,
					therefore if success_status eq passed then we set userModuleFulfilled. As of 2012-09-06 we also allow completion_status to decide module pass if trngModule.passDecidedBy eq 'Courseware' --->
					<cfcase value="cmi.completion_status">
						<!--- See ADL REQ_59  --->
						<!--- Allowed values: completed, incomplete, not attempted, unknown --->
						<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
						<cfif arguments.ElementValue eq "completed">
							<cfset getModuleInfo = application.com.relayElearning.GetModuleFromProgress(userModuleProgressID=arguments.userModuleProgressID) />
							<cfif structKeyExists(getModuleInfo,"isSCORM") and getModuleInfo.isSCORM eq 1 and structKeyExists(getModuleInfo,"passDecidedBy") and getModuleInfo.passDecidedBy eq "Courseware">
								<cfset setModuleComplete(userModuleProgressID=arguments.userModuleProgressID) />
							<!--- Else do not update module status information --->
							</cfif>
						</cfif>
					</cfcase>
					<cfcase value="cmi.score.raw">
						<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
						<cfif isDefined("session.elearning.scorm.QuizTakenID") and isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0>
							<!--- Use existing session.elearning.scorm.QuizTakenID --->
						<cfelse>
							<cfset session.elearning.scorm.QuizTakenID = createSCORMQuiz(userModuleProgressID=arguments.userModuleProgressID) /><!--- If this is the first quiz element from the SCO then we need to create the quiz --->
						</cfif>
						<cfif isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0 and isNumeric(arguments.ElementValue)>
							<cfset application.com.relayQuiz.updateQuizScore(QuizTakenID=session.elearning.scorm.QuizTakenID, TotalScore=arguments.ElementValue) /><!--- update score on QuizTaken --->
						</cfif>
					</cfcase>
					<cfcase value="cmi.score.scaled">
						<cfset tempValue = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName="cmi.scaled_passing_score") />
						<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
						<cfif isNumeric(tempValue) and isNumeric(arguments.ElementValue)><!--- tempValue = cmi.scaled_passing_score (from manifest) --->
							<cfif isDefined("session.elearning.scorm.QuizTakenID") and isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0>
								<!--- Use existing session.elearning.scorm.QuizTakenID --->
							<cfelse>
								<cfset session.elearning.scorm.QuizTakenID = createSCORMQuiz(userModuleProgressID=arguments.userModuleProgressID) /><!--- If this is the first quiz element from the SCO then we need to create the quiz --->
							</cfif>
							<cfif arguments.ElementValue gte tempValue><!--- If a passing score --->
								<cfif isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0 and isNumeric(arguments.ElementValue)>
									<cfset application.com.relayQuiz.updateQuizScore(QuizTakenID=session.elearning.scorm.QuizTakenID, TotalScore=arguments.ElementValue, PassMark=tempValue, QuizStatus='Pass') /><!--- update score on QuizTaken. CASE 430523 update PassMark and QuizStatus. --->
								</cfif>
								<cfset SCORMSetValue(userModuleProgressID=arguments.UserModuleProgressID, ElementName="cmi.success_status", ElementValue="passed") />
							<cfelse><!--- Not a passing score --->
								<cfif isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0 and isNumeric(arguments.ElementValue)>
									<cfset application.com.relayQuiz.updateQuizScore(QuizTakenID=session.elearning.scorm.QuizTakenID, TotalScore=arguments.ElementValue, PassMark=tempValue, QuizStatus='Fail') /><!--- update score on QuizTaken. CASE 430523 update PassMark and QuizStatus. --->
								</cfif>
								<!--- If the score evaluates to a failure, and the learner's success_status is not already 'passed', then record as 'failed' --->
								<cfset tempValue = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName="cmi.success_status") />
								<cfif tempValue neq "passed">
									<cfset SCORMSetValue(userModuleProgressID=arguments.UserModuleProgressID, ElementName="cmi.success_status", ElementValue="failed") />
								</cfif>
							</cfif>
						</cfif>
					</cfcase>
					<cfcase value="cmi.success_status">
						<!--- Tie in with tump. Only update tump if module.isSCORM eq 1 (corresponds to option on module edit screen).
							We cannot rely on the trigger on the QuizTaken table to do the tump work for us, because that will try to inner join on QuizDetail,
							but a SCO's QuizTaken data uses QuizDetailID=0 because it is not a Relay Quiz.
							We would not be able to make use of relayQuiz.setQuizTakenCompleted() either, not without some drastic hacking, because that depends upon relayQuiz.getQuiz() and therefore a real QuizDetail record.
						 --->
						<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />

						<cfset getModuleInfo = application.com.relayElearning.GetModuleFromProgress(userModuleProgressID=arguments.userModuleProgressID) />
						<cfif structKeyExists(getModuleInfo,"isSCORM") and getModuleInfo.isSCORM eq 1>
							<cfif isDefined("session.elearning.scorm.QuizTakenID") and isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0>
								<!--- Use existing session.elearning.scorm.QuizTakenID --->
							<cfelse>
								<cfset session.elearning.scorm.QuizTakenID = createSCORMQuiz(userModuleProgressID=arguments.userModuleProgressID) />
							</cfif>
							<cfif arguments.ElementValue eq "passed">
								<cfset tempValue = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName="cmi.score.scaled") />
								<cfif tempValue eq "">
									<cfset tempValue = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName="cmi.score.raw") />
								</cfif>
								<cfif tempValue eq "">
									<cfset tempValue = 1 /><!--- If the SCO gave us a passed status without a score, then default to a score of 1 for the QuizTaken score --->
								</cfif>
								<!--- WAB/NJH 2012-11-14 CASE 431977 This update was in wrong place and so not updating the score when cmi.score.scaled existed--->
								<cfif isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0 and isNumeric(tempValue)>
									<cfset application.com.relayQuiz.updateQuizScore(QuizTakenID=session.elearning.scorm.QuizTakenID, TotalScore=tempValue, QuizStatus='Pass') /><!--- CASE 430523 update QuizStatus. We do not know PassMark value, we just know that the SCO considers the status as 'passed' --->
								</cfif>


								<cfset tempStruct.frmUserModuleFulfilled = createODBCDateTime(request.requestTime) />
								<cfset tempStruct.qryQuizTaken.score = tempValue />
								<cfset tempStruct.qryQuizTaken.PassMark = tempValue /> <!--- This PassMark is just a dummy value to allow existing functionality to recognise what we already know is a Pass. It is being passed to updateUserModuleProgress(), it should not affect the QuizTaken.passMark --->
								<cfset tempStruct.qryQuizTaken.completed = tempStruct.frmUserModuleFulfilled />
								<cfset tempStruct.qryQuizTaken.TimeAllowed = 0 />
								<cfset tempStruct.qryQuizTaken.TimeLeft = 0 />
								<cfset updateUserModuleProgress(userModuleProgressID=arguments.userModuleProgressID,argumentsStruct=tempStruct) />
							<cfelseif arguments.ElementValue eq "failed">
								<cfset tempValue = getCoursewareData(userModuleProgressID=arguments.userModuleProgressID, ElementName="cmi.score.raw") />
								<cfif isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0 and isNumeric(tempValue)>
									<cfset application.com.relayQuiz.updateQuizScore(QuizTakenID=session.elearning.scorm.QuizTakenID, TotalScore=tempValue, QuizStatus='Fail') /><!--- CASE 430523 update QuizStatus. We do not know PassMark value, we just know that the SCO considers the status as 'failed' --->
								</cfif>
							</cfif>
						<!--- Else the module config dictates not not to update module status information --->
						</cfif>
						<!--- </cfif> --->
					</cfcase>
					<cfcase value="rw.session_time_start"><!--- (convert(varchar, getDate(), 121)) --->
						<!--- During initialize() we are storing a CF timestamp in session.elearning.scorm.SCORMSessionStartTime,
							 if the SCO does not set cmi.session_time then we use our timestamp for calculating the amount to increment total_time by during terminate().
							 The rw.session_time_start value below is not strictly necessary, however this is being called from initialize() since it may be useful for future reporting and/or troubleshooting. --->
						<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue='#createODBCDateTime(request.requestTime)#') />
					</cfcase>
					<cfdefaultcase>
						<!--- handle elements which can have multiple children where we need to validate that child n exists --->
						<!--- Implementing: cmi.interactions, [cmi.interactions._count RO], cmi.interactions.n.id/type/timestamp/weighting/learner_response/result/latency/description, cmi.interactions.n.correct_responses, [cmi.interactions.n.correct_responses._count RO], cmi.interactions.n.correct_responses.m.pattern --->
						<!--- Not implementing: cmi.interactions.n.objectives._count, cmi.interactions.n.objectives.id --->
						<cfif arguments.ElementName.startsWith("cmi.interactions.")>
							<cfif listLen(arguments.ElementName,'.') eq 4 and isNumeric(listGetAt(arguments.ElementName,3,'.'))>
								<!--- Send data to bucket table, data then grouped and mapped to RelayQuiz tables when SCORMDataToRelayQuiz called during commit() or terminate() --->
								<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
							<cfelseif listLen(arguments.ElementName,'.') eq 6 and isNumeric(listGetAt(arguments.ElementName,3,'.'))
									and listGetAt(arguments.ElementName,4,'.') eq "correct_responses" and isNumeric(listGetAt(arguments.ElementName,5,'.'))
									and listGetAt(arguments.ElementName,6,'.') eq "pattern">
								<!--- Assume that API has already check that n is valid and m is valid --->
								<!--- Send to bucket table --->
								<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
							<cfelse>
								<cfset result.isOk = false />
							</cfif>
						<cfelseif arguments.ElementName.startsWith("cmi.objectives.")>
							<!--- Not mapping to RW but will persist in DB for future use. --->
							<!--- We are not really using objectives data so we do not care about validating the value of n sent by the SCO --->
							<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
						<cfelseif arguments.ElementName.startsWith("cmi.comments_from_learner.")>
							<!--- Veronica and Gawain agreed at workshop not to implement comments, however later discussions with Gawain suggested that we might as well accept them to appear conformant, even if we are not using them - since that is what we are doing with cmi.objectives --->
							<cfset upsertCoursewareData(userModuleProgressID=arguments.userModuleProgressID,ElementName=arguments.ElementName,ElementValue=arguments.ElementValue) />
						<cfelse>
							<cfset result.isOk = false />
						</cfif>
					</cfdefaultcase>
				</cfswitch>
			</cfif>
		<cfcatch>
			<cfset result.isOK = false />
		</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>

	<cffunction access="public" name="SCORMDataToRelayQuiz" hint="To be called during commit and terminate, but not on setValue" returntype="struct">
		<cfargument name="UserModuleProgressID" type="numeric" required="true" />
		<cfargument name="ModuleID" type="numeric" required="true" />
		<cfargument name="PersonID" type="numeric" required="false" default="#request.relaycurrentuser.personID#" />
		<cfargument name="FileID" type="numeric" required="false" default="0" />

		<cfset var result = structNew() />
		<cfset var getCoursewareData = "" />
		<cfset var questions = structNew() />
		<cfset var thisQuestion = structNew() />
		<cfset var thisQuizTakenID = 0 />
		<cfset var thisQuizQuestionID = 0 />
		<cfset var getQuizTakenQuestion = "" />
		<cfset var updateQuizTakenQuestion = "" />
		<cfset var insertQuizTakenQuestion = "" />
		<cfset var insertQuizTakenQuestionQueryResult = "" />
		<cfset var thisQuizTakenQuestionID = 0 />
		<cfset var insertQuizTakenAnswer = "" />

		<!--- Index within the cmi.interactions collection, prepended by "Q" so that we can use it as a structKey,
			eg if ElementName eq cmi.interactions.3 then currentIndex will be set to "Q3".
			It might look like an array would have been more appropriate than a struct,
			however we do not know the size in advance, and cannot be sure that the SCO will send data for all indexes, or whether it will be received in a meaningful order --->
		<cfset var currentIndex = "" />
		<!--- id,type,timestamp,weighting,result,latency,description --->
		<cfset var currentKey = "" />

		<cfset result.isOK = false />

		<cftry><!--- Could we safely improve performance by storing last commit time and ignoring records which were not changed since last commit? --->
			<cfquery name="getCoursewareData" datasource="#application.siteDataSource#">
				SELECT CoursewareDataID, FileID, PersonID, ElementName, ElementValue, ModuleID, Created, LastUpdated
				FROM trngCoursewareData
				WHERE ModuleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.ModuleID#">
					AND PersonID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.PersonID#">
					AND (FileID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.FileID#"> OR FileID = 0) <!--- 2012-09-03 STCR CASE 430266 added (OR FileID = 0) - previously was matching no records if a specific FileID was passed but the data for that module had been saved without a specific FileID. This is the immediate fix, although the preferred solution would be to ensure that the current FileID is specified on all coursewareData values. --->
					<!--- AND ElementName like 'cmi.interactions.%' --->			<!--- 2014-09-23 PPB Case 439473 allow cmi.score through--->
				ORDER BY ElementName
			</cfquery>

			<!--- START 2014-09-23 PPB Case 439473 --->
			<cfquery name="getScoreMin" dbtype="query">
				SELECT ElementValue FROM getCoursewareData WHERE ElementName = 'cmi.score.min'
			</cfquery>

			<cfquery name="getScoreMax" dbtype="query">
				SELECT ElementValue FROM getCoursewareData WHERE ElementName = 'cmi.score.max'
			</cfquery>
			<!--- END 2014-09-23 PPB Case 439473 --->

			<cfif application.testsite eq 2>
				<cfset result.getCoursewareData = getCoursewareData />
			</cfif>
			<cfif getCoursewareData.recordCount gt 0>
				<cfif isDefined("session.elearning.scorm.QuizTakenID") and isNumeric(session.elearning.scorm.QuizTakenID) and session.elearning.scorm.QuizTakenID gt 0>
					<!--- Use existing session.elearning.scorm.QuizTakenID --->
				<cfelse>
					<cfset session.elearning.scorm.QuizTakenID = createSCORMQuiz(userModuleProgressID=arguments.userModuleProgressID, personID=arguments.personID) />
				</cfif>
				<cfset thisQuizTakenID = session.elearning.scorm.QuizTakenID />
			</cfif>

			<!--- START 2014-09-23 PPB Case 439473 --->
			<!--- I update QuizTaken rather than do this in the insert because createSCORMQuiz is called from several places and we don't have the scoreMin/scoreMax data available --->
			<cfquery name="updateQuizTaken" datasource="#application.siteDataSource#">
				UPDATE QuizTaken
				SET ScoreMin = <cf_queryparam cfsqltype="cf_sql_numeric" value="#getScoreMin.ElementValue#">
					,ScoreMax = <cf_queryparam cfsqltype="cf_sql_numeric" value="#getScoreMax.ElementValue#">
					,LastUpdated = getdate()
					,LastUpdatedBy = #request.relayCurrentUser.userGroupId#
					,LastUpdatedByPerson = #request.relayCurrentUser.personId#
				WHERE QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#thisQuizTakenID#">
				AND (IsNull(ScoreMin,0)  <>  <cf_queryparam value="#getScoreMin.ElementValue#" CFSQLTYPE="cf_sql_numeric" > OR IsNull(ScoreMax,0)  <>  <cf_queryparam value="#getScoreMax.ElementValue#" CFSQLTYPE="cf_sql_numeric" > )			<!--- only update if it has changed --->
			</cfquery>
			<!--- END 2014-09-23 PPB Case 439473 --->

			<cfloop query="getCoursewareData">
				<cfif left(getCoursewareData.ElementName,16) eq "cmi.interactions">																				<!--- 2014-09-23 PPB Case 439473 restrict to cmi.interactions to be safe because I removed the restriction above --->
					<cfif listlen(getCoursewareData.ElementName,'.') eq 4 OR listFindNoCase(getCoursewareData.ElementName,'correct_responses','.')>				<!--- 2014-09-23 PPB Case 439473 added condition for correct_responses  --->
						<cfset currentIndex = "Q" & listGetAt(getCoursewareData.ElementName,3,'.') />
						<cfset currentKey = listGetAt(getCoursewareData.ElementName,4,'.') />																	<!--- 2014-09-23 PPB Case 439473 specifically get 4th element (not last) cos now we're processing cmi.interactions.5.correct_responses.0.pattern  --->
						<cfif listFind("id,type,timestamp,weighting,result,latency,description,learner_response,correct_responses",currentKey)>
							<cfif not isDefined("questions.#currentIndex#")>
								<cfset questions["#currentIndex#"] = structNew() />
							</cfif>
							<cfset questions["#currentIndex#"]["#currentKey#"] = getCoursewareData.ElementValue />
						</cfif>
					<cfelse>
						<cfset currentIndex = "" />
						<cfset currentKey = "" />
					</cfif>
				</cfif>																																			<!--- 2014-09-23 PPB Case 439473 --->
			</cfloop>

			<cfloop collection="#questions#" item="thisQuestion">
				<cfif isStruct(questions[thisQuestion]) and structKeyExists(questions[thisQuestion],"id") and len(questions[thisQuestion].id) gt 0>
					<cfquery name="getQuizTakenQuestion" datasource="#application.siteDataSource#">
						select QuizTakenQuestionID from QuizTakenQuestion
						where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#thisQuizTakenID#">
						AND ScormInteractionID = <cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].id#" maxlength="4000">
					</cfquery>
					<cfif getQuizTakenQuestion.recordCount gt 0>
						<cfloop query="getQuizTakenQuestion"><!--- There should only be zero or one record --->
							<cfset thisQuizTakenQuestionID = getQuizTakenQuestion.QuizTakenQuestionID />
							<cfquery name="updateQuizTakenQuestion" datasource="#application.siteDataSource#">
								UPDATE QuizTakenQuestion
								SET Viewed=1 <!--- , ViewedAt=getDate() --->
									<cfif structKeyExists(questions[thisQuestion],"description")>,QuestionTextGiven = <cf_queryparam cfsqltype="cf_sql_varchar" value="#left(questions[thisQuestion].description, 4000)#"></cfif>
									,InteractionType=<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].type#">							<!--- 2014-09-23 PPB Case 439473 --->
									,CorrectResponses=<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].correct_responses#">			<!--- 2014-09-23 PPB Case 439473 --->
									<cfif structKeyExists(questions[thisQuestion],"result") and len(questions[thisQuestion].result) gt 0>
										<cfif isNumeric(questions[thisQuestion].result)>
											,ScormResultScore = <cf_queryparam cfsqltype="cf_sql_float" value="#questions[thisQuestion].result#">
										<cfelse>
											,ScormResultState = <cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].result#" maxlength="32">
										</cfif>
									</cfif>
								WHERE QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#thisQuizTakenQuestionID#">
							</cfquery>
						</cfloop>
					<cfelse>
						<cfquery name="insertQuizTakenQuestion" datasource="#application.siteDataSource#" result="insertQuizTakenQuestionQueryResult">
							INSERT INTO QuizTakenQuestion (QuizTakenID,QuestionID,Viewed,ViewedAt
								<cfif structKeyExists(questions[thisQuestion],"description")>,QuestionTextGiven</cfif>
								,ScormInteractionID
								,InteractionType																												<!--- 2014-09-23 PPB Case 439473  --->
								,CorrectResponses																												<!--- 2014-09-23 PPB Case 439473  --->
								<cfif structKeyExists(questions[thisQuestion],"result") and len(questions[thisQuestion].result) gt 0>
									<cfif isNumeric(questions[thisQuestion].result)>
										,ScormResultScore
									<cfelse>
										,ScormResultState
									</cfif>
								</cfif>
								)
							VALUES (<cf_queryparam cfsqltype="cf_sql_integer" value="#thisQuizTakenID#">,<cf_queryparam value="#thisQuizQuestionID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,getDate() <!--- STCR Could try and use cmi.interactions.n.timestamp if we could find an example of a SCO using it, then use getDate() as fallback --->
								<cfif structKeyExists(questions[thisQuestion],"description")>,<cf_queryparam cfsqltype="cf_sql_varchar" value="#left(questions[thisQuestion].description, 4000)#"></cfif>
								,<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].id#" maxlength="4000">
								,<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].type#">	 											<!--- 2014-09-23 PPB Case 439473  --->
								,<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].correct_responses#">	 								<!--- 2014-09-23 PPB Case 439473  --->
								<cfif structKeyExists(questions[thisQuestion],"result") and len(questions[thisQuestion].result) gt 0>
									<cfif isNumeric(questions[thisQuestion].result)>
										,<cf_queryparam cfsqltype="cf_sql_float" value="#questions[thisQuestion].result#">
									<cfelse>
										,<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].result#" maxlength="32">
									</cfif>
								</cfif>
								)
						</cfquery>
						<cfset thisQuizTakenQuestionID = insertQuizTakenQuestionQueryResult.identityCol />
					</cfif>

					<cfif structKeyExists(questions[thisQuestion],"learner_response")>
						<cfquery name="insertQuizTakenAnswer" datasource="#application.siteDataSource#">
							<!--- Should we ever delete or update an old SCORM response for a given QuizTakenQuestion? --->
							if not exists (
								select QuizTakenAnswerID from QuizTakenAnswer
								where QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#thisQuizTakenQuestionID#">
								and AnswerTextGiven = <cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].learner_response#">
							)
							insert into QuizTakenAnswer (QuizTakenQuestionID, AnswerID, AnsweredAt, ScoreGiven, AnswerTextGiven)
							values (
								<cf_queryparam cfsqltype="cf_sql_integer" value="#thisQuizTakenQuestionID#">,
								<cf_queryparam cfsqltype="cf_sql_integer" value="0">,
								getDate(), <!--- STCR Could try and use cmi.interactions.n.timestamp + cmi.interactions.n.latency if we could find an example of a SCO using it, then use getDate() as fallback --->
								<cf_queryparam cfsqltype="cf_sql_numeric" value="#questions[thisQuestion].weighting#">, 										<!--- 2014-09-23 PPB Case 439473 --->
								<cf_queryparam cfsqltype="cf_sql_varchar" value="#questions[thisQuestion].learner_response#">
								)
						</cfquery>
					</cfif>
					<cfset thisQuizTakenQuestionID = 0 />
				</cfif>
			</cfloop>

			<cfset result.questions = questions />
			<cfset result.isOk = true />

		<cfcatch>
			<cfset result.cfcatch = cfcatch />
			<cfset application.com.errorHandler.recordRelayError_Warning(type="SCORM Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments) />
		</cfcatch>
		</cftry>

		<cfreturn result />

	</cffunction>

	<!--- Originally we were not using the imsmanifest.xml because it was understood to be mainly for sequencing and the other fields we might need (such as the launcher file)
		are obtained from RW module setup page as agreed with Gawain and Veronica during the workshop. There are some api elements we might want to implement (waiting for confirmation from Veronica)
		which would then mean pulling settings from the manifest, eg in order to implement cmi.scaled_passing_score which is necessary to support scoring on the 'Advanced Runtime' Golf example SCO. --->
	<cffunction access="public" name="SCORMParseManifest" hint="" returntype="struct">
		<cfargument name="userModuleProgressID" type="numeric" required="false" default="0" />
		<cfargument name="ModuleID" type="numeric" required="true" />
		<cfargument name="personID" type="numeric" required="false" />

		<cfscript>
			var result=structNew();
			var getModuleDetails = "";
			var courseFileID = 0;
			var courseAbsolutePath = "";
			var manifestPath="";
			var manifestFilename="imsmanifest.xml";
			var xmlFileContent="";
			var myXmlDoc="";
			var selectedElements="";
			var i=0;
			var j=0;
			var objectiveCount=0;
		</cfscript>
		<!--- <cfset result.isOK = false /> ---> <!--- If manifest parsing fails we are going to ignore it and initialize to defaults --->

		<cftry>

			<!--- Get folder path to imsmanifest.xml - code lifted from getCourseLauncherUrl() --->
			<cfset getModuleDetails=getModuleDetailsQry(moduleID=arguments.moduleID)>
			<cfif getModuleDetails.recordCount eq 0>
				<cfreturn result />
			</cfif>
			<cfif getModuleDetails.fileID neq "">
				<cfset courseFileID = getModuleDetails.fileID>
			</cfif>
			<cfset courseAbsolutePath = getModuleDetails.absolutePath>
			<!--- LID 5753 NJH 2011-02-24 - get the local language/country file - nicked from elearningCoursesV2.cfm --->
			<cfif courseFileID neq 0>
				<cfset courseFileID = getLocalVersionFileID(fileID=courseFileID)>
			</cfif>
			<cfif fileExists("#courseAbsolutePath#\#courseFileID#\#manifestFilename#")>
				<cfset manifestPath="#courseAbsolutePath#\#courseFileID#\#manifestFilename#" />
			<cfelse>
				<cfreturn result />
			</cfif>

			<!--- parse xml, --->
			<cffile action="read" file="#manifestPath#" variable="xmlFileContent" />
			<cfscript>
				result.cmi = structNew();
				result.rw = structNew();
				myXmlDoc = XmlParse(xmlFileContent);

				// Note that we are supporting a subset of SCORM 2004 3rd edition. The 4th edition specifies completionThreshold as additional attributes below this.
				// At the time of writing we have nothing in rw to map the corresponding completion_status to yet anyway, and we do not yet have any sample SCOs which use this,
				// so it is not critical. It is the success_status which we are mapping.
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='completionThreshold']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					result.cmi["completion_threshold"] = selectedElements[i].XmlText;
				}
				if(i gt 1 and not isNumeric(result.cmi["completion_threshold"])) { structDelete(result.cmi,"completion_threshold"); }

				/* <!---
					REQ_65.2 The LMS shall implement the cmi.launch_data data model element as a
					characterstring with an SPM of 4000 characters.
					REQ_65.3 The LMS shall initialize this data model element using the SCORM 2004 3rd
					Edition Content Packaging Extensions Version 1.0 namespace element
					<adlcp:dataFromLMS>.  If an <adlcp:dataFromLMS> element does not exist as
					a child element of the <imscp:item> element (associated with the SCO resource),
					then the element shall remain uninitialized.
				---> */
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='dataFromLMS']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					result.cmi["launch_data"] = left(selectedElements[i].XmlText, 4000);
				}

				/* <!---
					REQ_70.3 The LMS shall initialize this data model element using the IMS Simple Sequencing
					namespace element <imsss:attemptAbsoluteDurationLimit>.  If an
					<imsss:attemptAbsoluteDurationLimit> element does not exist as a child element
					of the <imscp:item> element (associated with the SCO resource), then the element
					shall remain uninitialized.
				---> */
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='attemptAbsoluteDurationLimit']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					result.cmi["max_time_allowed"] = selectedElements[i].XmlText;
				}
				if(i gt 1 and not isNumeric(result.cmi["max_time_allowed"])) { structDelete(result.cmi,"max_time_allowed"); }

				/* <!---
					REQ_72.3.3 For each objective defined (<imsss:primaryObjective> or <imsss:objective>) that
					includes an objectiveID attribute, the LMS is responsible for adhering to the
					following initialization requirements:
					REQ_72.3.3.1 The objectiveID attribute shall be used to initialize the cmi.objectives.n.id value.
				---> */
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='primaryObjective']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					if(structKeyExists(selectedElements[i], "XmlAttributes") and structKeyExists(selectedElements[i].XmlAttributes, "objectiveID") and len(selectedElements[i].XmlAttributes.objectiveID) gt 0) {
						result.cmi["objectives.#objectiveCount#.id"] = selectedElements[i].XmlAttributes.objectiveID;
						objectiveCount += 1; //SCORM uses zero-based arrays
						/* <!---
							REQ_74.3 The LMS is responsible for initializing the cmi.scaled_passing_score data model
							element using the IMS Simple Sequencing namespace element
							<imsss:minNormalizedMeasure> associated with an <imsss:primaryObjective>
							element for the <imscp:item> element that references a SCO resource as defined by
							the following requirements.
							REQ_74.3.1 If the IMS Simple Sequencing namespace attribute imsss:satisfiedByMeasure
							associated with the <imsss:primaryObjective> element for the <imscp:item>
							element that references the SCO is equal to true, then the value provided by the
							<imsss:minNormalizedMeasure> element associated with the
							<imsss:primaryObjective> element for the <imscp:item> element that references
							the SCO resource shall be used to initialize the cmi.scaled_passing_score data
							model element.
							REQ_74.3.2 If the IMS Simple Sequencing namespace attribute imsss:satisfiedByMeasure
							associated with the <imsss:primaryObjective> element for the <imscp:item>
							element that references the SCO is equal to true and no value is provided for the
							<imsss:minNormalizedMeasure> element associated with the
							<imsss:primaryObjective> element for the <imscp:item> element that references
							the SCO resource, then the LMS shall initialize the cmi.scaled_passing_score to
							1.0.
							REQ_74.3.3 If the IMS Simple Sequencing namespace attribute imsss:satisfiedByMeasure
							associated with the <imsss:primaryObjective> element for the <imscp:item>
							element that references the SCO is equal to false, then the LMS shall not make any
							assumptions of a scaled passing score (i.e., cmi.scaled_passing_score data model
							element).
						---> */
						if(structKeyExists(selectedElements[i].XmlAttributes, "satisfiedByMeasure") and selectedElements[i].XmlAttributes.satisfiedByMeasure eq "true") {
							result.cmi["scaled_passing_score"] = "1.0";
							if(structKeyExists(selectedElements[i],"XmlChildren") and isArray(selectedElements[i].XmlChildren)) {
								for(j = 1; j LTE ArrayLen(selectedElements[1].XmlChildren); j = j + 1) {
									if((selectedElements[i].XmlChildren[j].XmlName eq "imsss:minNormalizedMeasure" or selectedElements[i].XmlChildren[j].XmlName eq "minNormalizedMeasure")
											and isNumeric(selectedElements[i].XmlChildren[j].XmlText)) {
										result.cmi["scaled_passing_score"] = selectedElements[1].XmlChildren[j].XmlText;
										upsertCoursewareData(userModuleProgressID = arguments.userModuleProgressID, ElementName = "cmi.scaled_passing_score", ElementValue = selectedElements[1].XmlChildren[j].XmlText);
									}
								}
							}
						}
					}
				}

				/* <!---
					REQ_72.3.3 For each objective defined (<imsss:primaryObjective> or <imsss:objective>) that
					includes an objectiveID attribute, the LMS is responsible for adhering to the
					following initialization requirements:
					REQ_72.3.3.1 The objectiveID attribute shall be used to initialize the cmi.objectives.n.id value.
				---> */
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='objective']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					if(structKeyExists(selectedElements[i], "XmlAttributes") and structKeyExists(selectedElements[i].XmlAttributes, "objectiveID") and len(selectedElements[i].XmlAttributes.objectiveID) gt 0) {
						result.cmi["objectives.#objectiveCount#.id"] = selectedElements[i].XmlAttributes.objectiveID;
						objectiveCount += 1;
					}
				}

				/* <!---
					REQ_79.2 The LMS shall implement the cmi.time_limit_action data model element as a state
					consisting of the following vocabulary tokens:
					? exit,message
					? continue,message
					? exit,no message
					? continue,no message
					REQ_79.3 The LMS shall initialize this data model element using the SCORM 2004 3rd
					Edition Content Packaging Extensions Version 1.0 namespace element
					<adlcp:timeLimitAction>.  If an <adlcp:timeLimitAction> element does not exist
					as a child element of the <imscp:item> element (associated with the SCO resource),
					then the element shall be initialized to the default value of continue,no
					message.
				---> */
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='timeLimitAction']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					result.cmi["time_limit_action"] = selectedElements[i].XmlText;
				}
				//check valid values, set to default initialization value if not valid
				if(i gt 1 and not listFind('exit,message|continue,message|exit,no message|continue,no message',result.cmi["time_limit_action"],'|')) {
					result.cmi["time_limit_action"] = "continue,no message";
				}

				/* <!--- This is an example of how the launcher can be specified in the manifest, although currently we are using the launcher determined by RW module setup.
					We are able to parse this successfully but for the launcher href to be of use we would need to parse the manifest outside of the SCO, during courseware/module setup.

					<resource identifier="resource_1" type="webcontent" adlcp:scormType="sco" href="shared/launchpage.html">
					The scormType="sco" attribute is the indicator.
					A future upgrade to the module edit process could be to look inside courseware zips for a imsmanifest.xml and parse it
					and automatically set values such as the launcher filename on the module edit screen.

					REQ_22.1 The LMS shall be able to launch a SCORM 2004 3rd Edition conformant Sharable
					Content Object (SCO). SCOs are identified in an imsmanifest.xml as a
					<resource> with an attribute of adlcp:scormType="sco".
				---> */
				selectedElements = (XmlSearch(myXmlDoc,"//*[local-name()='resource'][@adlcp:scormType='sco']"));
				for(i = 1; i LTE ArrayLen(selectedElements); i = i + 1) {
					if(structKeyExists(selectedElements[i].XmlAttributes, "href")) { result.rw["launcher"] = selectedElements[i].XmlAttributes.href; }
				}
			</cfscript>

		<cfcatch><cfset result = structNew() /><cfif application.testsite eq 2 and isDefined("url.srdebug")><cfrethrow /></cfif></cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>

	<cffunction access="public" name="GetModuleFromProgress" hint="Returns training module information" returntype="query">
		<cfargument name="userModuleProgressID" type="numeric" required="true" />

		<cfscript>
			var GetModule = "";
		</cfscript>

		<cfquery name="GetModule" datasource="#application.siteDataSource#">
			select tm.ModuleID, isNull(tm.QuizDetailID,0) AS QuizDetailID, tm.fileID, tm.isSCORM, tm.passDecidedBy, tump.userModuleProgressID
			from trngModule tm
			inner join trngUserModuleProgress tump with(nolock) on tm.moduleID = tump.moduleID
			where tump.usermoduleProgressID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.userModuleProgressID#">
		</cfquery>

		<cfreturn GetModule />
	</cffunction>

	<cffunction access="public" name="CreateSCORMQuiz" hint="" returntype="Numeric">
		<cfargument name="userModuleProgressID" type="numeric" required="true" />
		<cfargument name="personID" type="numeric" required="false" default="#request.relaycurrentuser.personID#" />

		<cfscript>
			var newQuizTakenID = 0;
			var InsertQuiz = "";
			var InsertQuizResult = "";
		</cfscript>

		<cfquery name="InsertQuiz" datasource="#application.siteDataSource#" result="InsertQuizResult">
			INSERT INTO QuizTaken
			(EntityTypeID, QuizDetailID, PersonID, Questions, Started, TimeLeft, UserModuleProgressID, Created, CreatedBy)
			VALUES
			(0, 0, <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.PersonID#">, null, null, null, <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.UserModuleProgressID#">, getDate(), <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.PersonID#">)
		</cfquery>
		<cfset newQuizTakenID = InsertQuizResult.identityCol />

		<cfreturn newQuizTakenID />
	</cffunction>

	<!--- END STCR 2012-06-20 P-REL109 Phase 2 --->


	<cffunction name="isPersonTrainingContact" access="public" returnType="boolean">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="organisationID" type="numeric" default="#request.relayCurrentUser.organisationID#">

		<!--- JIRA PROD2016-166 - pass in personId so that we can check if the person is in the specified training admin usergroup --->
		<cfif arguments.personId eq getTrainingContact(organisationID=arguments.organisationID,personID=arguments.personID)>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>


	<!--- NJH 2012/10/25 Social CR - add module merge fields function --->
	<cffunction name="getModuleMergeFields" access="public" returnType="struct">
		<cfargument name="moduleID" type="numeric" required="true">

		<cfset var moduleQry = queryNew("title,code,description")>

		<cftry>
			<cfquery name="moduleQry" datasource="#application.siteDataSource#">
				select title_of_module as title, description_of_module as description,module_code as code
				from vTrngModules
				where moduleID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.moduleID#">
			</cfquery>

			<cfcatch>
				<cfset queryAddRow(moduleQry,1)>
			</cfcatch>
		</cftry>

		<cfreturn application.com.structureFunctions.queryRowToStruct(query=moduleQry,row=1)>

	</cffunction>


	<!--- NJH 2012/10/25 Social CR - add certification merge fields function --->
	<cffunction name="getCertificationMergeFields" access="public" returnType="struct">
		<cfargument name="certificationID" type="numeric" required="true">

		<cfset var certificationQry = queryNew("title,code,description,certificationtype")>

		<cftry>
			<cfquery name="certificationQry" datasource="#application.siteDataSource#">
				select title,description,code,'phr_' + REPLACE(certificationtype,' ','_') as certificationtype
				from vTrngCertifications
				where certificationID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.certificationID#">
			</cfquery>

			<cfcatch>
				<cfset queryAddRow(certificationQry,1)>
			</cfcatch>
		</cftry>

		<cfreturn application.com.structureFunctions.queryRowToStruct(query=certificationQry,row=1)>
	</cffunction>


	<!--- START 2013-12-02 PPB Case 438029 new function --->
	<cffunction name="getPersonCertificationDetails" access="public" returnType="query">
		<cfargument name="certificationID" type="numeric" required="true">
		<cfargument name="statusTextId" type="string" required="false"> 					<!--- can be csv --->

		<cfquery name="qryPersonCertificationDetails" datasource="#application.siteDataSource#">
			SELECT personCertificationID, personID, statusTextID
			,convert(varchar,tpc.registrationDate,106) as RegistrationDate
			,convert(varchar,tpc.passDate,106) as PassDate
			FROM trngPersonCertification tpc with (noLock)
			INNER JOIN trngPersonCertificationStatus tpcs WITH (noLock) ON tpc.statusID = tpcs.statusID AND tpcs.statusTextID IN (<cf_queryparam value="#statusTextId#" cfsqltype="CF_SQL_VARCHAR" list="true"  >)
			WHERE certificationID = <cf_queryparam value="#certificationID#" cfsqltype="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn qryPersonCertificationDetails>
	</cffunction>
	<!--- END 2013-12-02 PPB Case 438029 --->


	<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
							Placeholder function called by getCourseLauncherURL when a custom courseware file type is chosen.
							Function is designed to be extended in custom to perform whatever action is required probably via
							a third party API e.g. Scormcloud to launch a Scorm course (Nutanix) or Skytap to launch a Virtual Environment (CyberArk) --->
	<cffunction name="getCustomCoursewareLauncherURL" access="public" returnType="string">
		<cfargument name="AICCFileName" type="string" required="true">

		<cfscript>
			var result = "";
		</cfscript>

		<cfreturn result>
	</cffunction>

	<!--- START 2015-26-08 DCC Case 445511  --->
	<cffunction name="checkzip" access="public">
		<cfargument name="moduleId" type="numeric" required="true">
		<cfscript>
		var getdetails = "";
		</cfscript>

		<cfquery name="getdetails" datasource="#application.siteDataSource#">
			SELECT
			ft.secure as secure,
			f.fileid as fileid,
			tm.AICCFileName,
			tm.isScorm,
			tm.moduleId,
			ft.autounzip
			FROM files f
			inner join fileType ft with (noLock) on f.fileTypeID=ft.fileTypeID
			left outer join trngModule tm with (noLock) on f.fileid = tm.fileid
			WHERE tm.moduleid = <cf_queryparam value="#arguments.moduleId#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfreturn getdetails />
	</cffunction>
	<!--- END 2015-26-08 DCC Case 445511  --->

    <!--- 2015-11-05 PYW P-TAT006 BRD 31 --->
    <cffunction name="sendPersonCertificationExpiryAlerts" hint="I send email alerts for due expiry certifications" access="public" returnType="struct">
        <cfargument name="noticePeriod" required="true" type="numeric">

        <cfset var result = {isOK = true}>
        <cfset var local = {}>

        <cfset local.emailTextID = "PersonCertificationExpiryNotice">
        <cfset local.merge = {}>
        <cfset local.merge.noticePeriod = arguments.noticePeriod>

            <cfquery name="local.qryPersonCertifications" datasource="#application.siteDataSource#">
                SELECT DISTINCT
                      TOP 100 PERCENT personCertificationID, certificationID, certificationTitle, personID,
                      certificationCode, certificationDescription, personCertificationStatus,
                      personCertificationRegistrationDate, personCertificationPassDate, personCertificationExpiredDate, personCertificationExpiryDate
                FROM
                    vTrngPersonCertifications AS v
                WHERE
<!---                   (personCertificationStatus = 'Pending') AND (certificationRuleType = 'Update') AND (ruleActive = 1) AND--->
                    (personCertificationStatus = 'Certified Pending Update') AND (certificationRuleType = 'Update') AND (ruleActive = 1) AND
                    (DATEDIFF(d,GETDATE(),DATEADD(mm,studyPeriod,DATEADD(d,-#arguments.noticePeriod#,activationDate))) = 0) OR

                    (personCertificationStatus = 'Registered') AND (ruleActive = 1) AND
                    (DATEDIFF(d,GETDATE(),DATEADD(mm,studyPeriod,DATEADD(d,-#arguments.noticePeriod#,personCertificationRegistrationDate))) = 0) OR

                    (personCertificationStatus = 'Registered') AND (certificationActive = 1) AND
                    (DATEDIFF(d,GETDATE(),DATEADD(mm,certificationDuration,DATEADD(d,-#arguments.noticePeriod#,personCertificationRegistrationDate))) = 0) OR

                    <!--- 2015-12-20 PYW P-TAT006 BRD 31. Modify sendPersonCertificationExpiryAlerts to check for person's with person certfication expiry date --->
                    (ISDATE(personCertificationExpiryDate) = 1 AND DATEDIFF(d,GETDATE(),DATEADD(d,-#arguments.noticePeriod#,personCertificationExpiryDate)) = 0)
            </cfquery>

            <cfloop query="local.qryPersonCertifications">
                <cfset local.personID = local.qryPersonCertifications.PersonID>
                <cfset local.merge.certificationTitle = certificationTitle>
                <cfset local.merge.certificationCode = certificationCode>
                <cfset local.merge.certificationDescription = certificationDescription>
                <cfset local.merge.personCertificationStatus = personCertificationStatus>
                <cfset local.merge.personCertificationRegistrationDate = lsdateFormat(personCertificationRegistrationDate, "medium")>
                <cfset local.merge.personCertificationPassDate = lsdateFormat(personCertificationPassDate, "medium")>
                <cfset local.merge.personCertificationExpiredDate = lsdateFormat(personCertificationExpiredDate, "medium")>
                <cfset local.merge.personCertificationExpiryDate = lsdateFormat(personCertificationExpiryDate, "medium")>

                <cfset application.com.email.sendEmail(personID=local.personID,emailTextID=local.emailTextID,mergeStruct=local.merge)>

            </cfloop>

        <cfreturn result>
    </cffunction>

    <!--- 2015-11-18 PYW P-TAT006 BRD 31 --->
    <cffunction name="sendModuleExpiryAlerts" hint="I send an email notifications for modules which will expire after the notice period" access="public" returnType="struct">
        <cfargument name="noticePeriod" required="true" type="numeric">

        <cfset var result = {isOK = true}>
        <cfset var local = {}>

        <cfset local.emailTextID = "ModuleExpiryNotice">
        <cfset local.merge.noticePeriod = arguments.noticePeriod>

            <cfquery name="local.qModules" datasource="#application.siteDataSource#">
                SELECT
                    moduleID,publishedDate,expiryDate,title_defaultTranslation,lastUpdatedByPerson
                FROM
                    trngModule with (nolock)
                WHERE
                    availability <> 0
                AND
                    DATEDIFF(d,GETDATE(),expiryDate) = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.noticePeriod#">
            </cfquery>

            <cfloop query="local.qModules">
                <cfset local.personID = local.qModules.lastUpdatedByPerson>
                <cfset local.merge.moduleID = local.qModules.moduleID>
                <cfset local.merge.title = local.qModules.title_defaultTranslation>

                <cfset application.com.email.sendEmail(personID=local.personID,emailTextID=local.emailTextID,mergeStruct=local.merge)>
            </cfloop>

        <cfreturn result>
    </cffunction>

    <!--- P-TAT006 BRD 31. Task to deactivate active modules on due expiry date --->
    <cffunction name="deactivateExpiredModules" hint="I deactivate modules which have expired" access="public" returnType="struct">
        <cfargument name="deactivationInterval" default="0" type="numeric">

        <cfset var result = {isOK = true}>
        <cfset var local = {}>

            <cfquery name="local.getExpiredModules" datasource="#application.siteDataSource#">
                SELECT
                    moduleID
                FROM
                    trngModule with (nolock)
                WHERE
                    availability <> 0
                AND
                    DateDiff(d,expiryDate,getDate()) = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.deactivationInterval#">
            </cfquery>

            <cfloop query="local.getExpiredModules">
                <cfquery name="local.updateExpiredModule" datasource="#application.siteDataSource#">
                    UPDATE
                        trngModule
                    SET
                        availability = 0
                    WHERE
                        moduleID = <cf_queryparam cfsqltype="cf_sql_integer" value="#local.getExpiredModules.moduleID#">
                </cfquery>
            </cfloop>

        <cfreturn result>
    </cffunction>

</cfcomponent>
