<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent hint="Used For Setting Up the Asynchronous Gateway" >

	<cfset gatewayID = "relayAsynchronousGateway">
	
	<cffunction name="makeGatewayInstanceStruct">
		<cfargument name="gatewayID">
		<cfargument name="Path">
		<cfset var result = "">
		<cfset var WDDX_XML = '' />
	
		<cfsavecontent variable="WDDX_XML">
		<cfoutput>
		<wddxPacket version='1.0'><header/><data>
		<struct type='coldfusion.server.ConfigMap'>
			<var name='MODE'><string>AUTO</string></var>
			<var name='CFCPATHS'>
				<array length='1'>
					<string>#path#</string>
				</array>
			</var>
			<var name='CONFIGURATIONPATH'><string></string></var>
			<var name='GATEWAYID'><string>#gatewayID#</string></var>
			<var name='TYPE'><string>CFML</string></var>
		</struct>
		</data>
		</wddxPacket>
		</cfoutput>	
		</cfsavecontent>
	
		<cfwddx action="wddx2cfml" input="#WDDX_XML#" output="result">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getNeoEventPath">
		<cfreturn server.ColdFusion.rootdir & "/lib/neo-event.xml">
	</cffunction>

	<cffunction name="readAndParseNeoEvent">
	
		<cfset var neoEventWDDX = '' />
		<cfset var neoEventStruct = '' />
		
		<cffile action="read" file="#getNeoEventPath()#" variable="neoEventWDDX">
		<cfwddx action="wddx2cfml" input="#neoEventWDDX#" output="neoEventStruct">
	
		<cfreturn neoEventStruct>
	
	</cffunction>
	
	
	
	<cffunction name="getAsynchronousGatewayPath">
	
		<cfset var result = {isOK = true, path = ""}>
		<cfset var relayPath = application.paths.relay>
		<cfset var gatewayPathRelativeToRelay = "\webservices\asynchronousgateway\asynchronousgateway.cfc">
	
		<cfif left (relayPath ,2) is "\\">
			<!--- can't do unc path, so we will have to guess that it exists in a similar location on the d: drive --->
			<cfset relayPath  = rereplacenocase(relayPath,"\\\\.*?\\","D:\")>
			<cfif not fileExists (relayPath & gatewayPathRelativeToRelay)>
				<!---  --->
				<cfset result.isok = false>		
				<cfset result.message = "Cannot use a UNC path for a gateway.  Copy files to #relayPath##gatewayPathRelativeToRelay# ">		
			<cfelse>
				<cfset result.path= relayPath & gatewayPathRelativeToRelay>				
			</cfif>
	
		<cfelse>
			<cfset result.path= relayPath & gatewayPathRelativeToRelay>							
		</cfif>
	
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="isAsynchronousGatewaySetUpCorrectly">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result = {isOK = true}>
		<cfset var path = "">
		<cfset var testResult= "">
		<cfset var neoEventStruct = readAndParseNeoEvent()>
		
		<!--- Is there a gateway instance with id #gatewayid# --->
		<cfset var InstancesArray = neoEventStruct.instances>
	
		<cfset var searchForExistingInstance = applicationScope.com.structureFunctions.arrayOfStructuresFindKeyValue (InstancesArray,'gatewayid',gatewayid)>
	
		<cfif searchForExistingInstance is 0>
			<!--- No existing node, so add one --->
				<cfset result.isOK = false>
			<cfset result.Message = "No Gateway '#gatewayid#' Set Up">
		<cfelse>
			<!--- Existing node, so check it--->
			<!--- check that path is OK --->
			<cfset path = InstancesArray[searchForExistingInstance].CFCPATHS[1]>
			<cfif not fileExists (path)>
				<cfset result.isOK = false>
				<cfset result.message = "Path set incorrectly, #path# does not exist">
			<cfelse>
				<cfset result.message = "Path set to #path#">
				<cfset testResult = testAsynchronousGateway (applicationScope=applicationScope)>
				<cfset result.isOK = testResult.isOK>
				<cfset result.message = result.message & "<BR>" & testResult.message>
	

			</cfif>
		</cfif>
	
		<cfreturn result>
	
	</cffunction>
	
	<!--- Test function which calls the gateway and then checks application scope for the expected result --->
	<cffunction name="testAsynchronousGateway">
		<cfargument name="applicationScope" default="#application#">
				
		<cfset var aSynchArgs = "">
		<cfset var result = {isOK=false,message=""}>

		<cfset structDelete (applicationScope,"AsynchronousGatewayTest")>
		<cfset aSynchArgs = {appname=applicationScope.applicationname,componentName="asynchronousGatewayUtils",methodname="testAsynchronousGatewayReceiver",timestamp = now()}>
				<cftry>
					<cfset SendGatewayMessage("relayAsynchronousGateway",aSynchArgs)>
		
						<cfset sleep (1000)>
		
						<cfif structKeyExists (applicationScope,"AsynchronousGatewayTest") and applicationScope.AsynchronousGatewayTest is aSynchArgs.timestamp>
							<cfset result.isOK = true>
							<cfset result.Message= "Gateway Tested Successfully">
							<cfset structDelete (application,"AsynchronousGatewayTest")>
						<cfelse>
							<cfset result.isOK = false>
							<cfset result.Message= "Gateway Test Failed - Reason Unknown">
						
						</cfif>

					<cfcatch> 
						<!--- TODO - maybe need a error logged here --->
						<cfset result.Message= "Gateway Test Failed with message: <BR><I><B>#cfcatch.message#</b></I>">
					</cfcatch>
				</cftry>

		
		<cfreturn result>				

	
	</cffunction>

	<cffunction name="testAsynchronousGatewayReceiver">
		<cfargument name = "applicationScope">
		<cfargument name = "timestamp">
		
		<cfset arguments.applicationScope.AsynchronousGatewayTest = timestamp>

	</cffunction>


	<cffunction name="setUpAsynchronousGateway">

		<cfset var neoEventStruct = '' />
		<cfset var InstancesArray = '' />
		<cfset var searchForExistingInstance = '' />
		<cfset var newInstance = '' />
		<cfset var backup = '' />
		<cfset var neoEventWDDX = '' />
		<cfset var result = {isOK = true,message=""}>
			
		<cfset var getGatewayPath = getAsynchronousGatewayPath()>
	
		<cfif not getGatewayPath.isOK>
			<cfset result.isok = false>
			<cfset result.message = getGatewayPath.message>
	
		<cfelse>
	
			<cfset neoEventStruct = readAndParseNeoEvent()>
			
			<!--- Is there a gateway instance with name asynch --->
			<cfset InstancesArray = neoEventStruct.instances>
			<cfset searchForExistingInstance =application.com.structureFunctions.arrayOfStructuresFindKeyValue (InstancesArray,'gatewayid',gatewayid)>
			
			<cfif searchForExistingInstance is not 0>
				<!--- Existing node, so lets delete and add again --->
				<cfset arraydeleteAt(InstancesArray,searchForExistingInstance)>
			</cfif>
			
			
			<cfset newInstance = makeGatewayInstanceStruct(id = gatewayid,path = getGatewayPath.path)>
			<cfset arrayAppend(InstancesArray,newInstance)>
			<cfwddx action="cfml2wddx" input="#neoEventStruct#" output="neoEventWDDX" >
			<cfset backup = getNeoEventPath() & dateformat(now(),"yymmddhhmmss")>
			<cffile action="copy"  source="#getNeoEventPath()#" destination="#backup#" >
			<cffile action="write" file="#getNeoEventPath()#" output="#neoEventWDDX#"> 
			<cfset result.message = "Gateway #gatewayid# set up at #getGatewayPath.path#">
		</cfif>
	
	
		<cfreturn result>	
		
	</cffunction>
	

</cfcomponent>