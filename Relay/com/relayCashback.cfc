<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayCashback.cfc
Author:			NJH
Date started:	03-07-2009

Description:	Functions for the cashback module

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/07/03			NJH			P-FNL069. Added some security measures. Created a function to determine if the current user had rights
								to a cashback claim. Bit crude at the moment, as we essentially check if the user is the one who created the claim
								of if they are an internal user. Also only add/remove items from a claim that is pending and if you have rights to the claim.
								Claims can only be approved/rejected if the user is internal.
2009/11/06			NJH			P-SNY086 - added additionalReference field

Possible enhancements:


 --->


<cfcomponent displayname="Relay Cashback" hint="Methods for managing Relay Cashback module">

	<!--- NJH 2009/03/04 CR-SNY675 create a cashback claim --->
	<cffunction access="public" name="createCashbackClaim" returnType="numeric" hint="This function creates a cashback claim">
		<cfargument name="PersonID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.userGroupID#">

		<cfscript>
			var insertCashbackClaim = "";
			var cashbackClaimID = 0;
			var pendingStatusID = 0;

			pendingStatusID = getClaimStatus(ClaimStatusTextID='pending').ClaimStatusID;
		</cfscript>

		<cftry>

			<cfquery name="insertCashbackClaim" datasource="#application.siteDataSource#">
				insert into CashbackClaim (ClaimStatusID,PersonID,CreditAmount,Created,CreatedBy,LastUpdated,LastUpdatedBy)
				values (<cf_queryparam value="#pendingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,0,<cf_queryparam value="#createODBCDateTime(request.requestTime)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#createODBCDateTime(request.requestTime)#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#arguments.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >)

				select scope_identity() as cashbackClaimID
			</cfquery>

			<cfset cashbackClaimID = insertCashbackClaim.cashbackClaimID>

			<cfcatch></cfcatch>
		</cftry>

		<cfreturn cashbackClaimID>
	</cffunction>

	<!--- NJH 2009/03/04 CR-SNY675 add a cashback claim item to the cashback claim --->
	<cffunction access="public" name="AddCashbackClaimItem" returnType="numeric" hint="Adds a line item to a CashbackClaimItem">
		<cfargument name="CashbackClaimID" type="numeric" required="true">
		<cfargument name="ProductID" type="numeric" required="true">
		<cfargument name="quantity" type="numeric" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="distiID" type="numeric" default="0">
		<cfargument name="serialNo" type="string" default="">
		<cfargument name="invoiceNo" type="string" default="">
		<cfargument name="invoiceDate" type="string" default="">
		<cfargument name="endUserCompanyName" type="string" default="">
		<cfargument name="additionalReference" type="string" default=""> <!--- NJH 2009/11/06 P-SNY086 --->

		<cfscript>
			var AddCashBackItem = "";
			var CashBackItemID = 0;
			var pendingStatusID = getClaimStatus(statusID="pending").ClaimStatusID;
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);
		</cfscript>

		<!--- NJH 2009/07/03 P-FNL069 only add to claim if it is in a pending state --->
		<cfif rights.edit>
		<!--- <cftry> --->
			<cfquery name="AddCashBackItem" datasource="#application.SiteDataSource#">
					if exists (select 1 from cashbackClaim where cashbackClaimID = #arguments.cashbackClaimID#
						and claimStatusID =  <cf_queryparam value="#pendingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >
					)
						Begin
				insert into CashbackClaimItem
				(	CashbackClaimID
					,ProductID
					,Quantity
					,Value
					,distiOrganisationID
					<cfif len(arguments.serialNo)>
						,serialNumber
					</cfif>
					<cfif len(arguments.invoiceNo)>
						,invoiceNumber
					</cfif>
					<cfif len(arguments.invoiceDate)>
						,invoiceDate
					</cfif>
					<cfif len(arguments.endUserCompanyName)>
						,endUserCompanyName
					</cfif>
								<!--- NJH 2009/11/06 P-SNY086 --->
								<cfif len(arguments.additionalReference)>
									,additionalReference
								</cfif>
				)
				values
				(	<cf_queryparam value="#arguments.CashbackClaimID#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#arguments.ProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#arguments.quantity#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#arguments.value#" CFSQLTYPE="cf_sql_numeric" >,
					<cf_queryparam value="#arguments.distiID#" CFSQLTYPE="cf_sql_integer" >
					<cfif len(arguments.serialNo)>
						,<cf_queryparam value="#arguments.serialNo#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					<cfif len(arguments.invoiceNo)>
						,<cf_queryparam value="#arguments.invoiceNo#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					<cfif len(arguments.invoiceDate)>
						,<cf_queryparam value="#CreateODBCDate(arguments.invoiceDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					</cfif>
					<cfif len(arguments.endUserCompanyName)>
						,<cf_queryparam value="#arguments.endUserCompanyName#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
								<!--- NJH 2009/11/06 P-SNY086 --->
								<cfif len(arguments.additionalReference)>
									,<cf_queryparam value="#arguments.additionalReference#" CFSQLTYPE="CF_SQL_VARCHAR" >
								</cfif>
				)

				select scope_identity() as CashBackItemID
						end
					else
						begin
							select 0 as CashBackItemID
						end
			</cfquery>

			<cfset CashBackItemID = AddCashBackItem.CashBackItemID>

			<!--- <cfcatch></cfcatch>
		</cftry> --->
		</cfif>

		<cfreturn CashBackItemID>

	</cffunction>

	<!--- NJH 2009/03/04 CR-SNY675 remove a cashback claim item from the cashback claim --->
	<cffunction access="public" name="removeCashbackClaimItems" hint="Removes line items from the cashback claim">
		<cfargument name="CashbackClaimID" type="numeric" required="true">
		<cfargument name="CashbackClaimItemIDList" type="string" required="true">

		<cfscript>
			var removeCashbackClaimItems = "";
			var pendingStatusID = getClaimStatus(statusID="pending").ClaimStatusID;
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);
		</cfscript>

		<!--- NJH 2009/07/03 - P-FNL069 only remove items from claims you have rights to and that are in a pending state --->
		<cfif rights.edit>
		<cfquery name="removeCashbackClaimItems" datasource="#application.siteDataSource#">
				if exists (select 1 from cashbackClaim where cashbackClaimID = #arguments.cashbackClaimID#
					and claimStatusID =  <cf_queryparam value="#pendingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >
				)
			delete from CashbackClaimItem
				where CashbackClaimID = #arguments.CashbackClaimID#
				and CashbackClaimItemID  in ( <cf_queryparam value="#arguments.CashbackClaimItemIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		</cfif>

	</cffunction>

	<!--- NJH 2009/03/04 CR-SNY675 get cashback claim items for a cashback claim --->
	<cffunction access="public" name="getCashbackClaimItems" returnType="query" hint="Get cashback claim items for a cashback claim">
		<cfargument name="CashbackClaimID" type="numeric" required="true">
		<cfargument name="statementView" type="boolean" required="false" default="false">
		<cfargument name="orderBy" type="string" default="p.productID, invoiceDate desc">
		<cfargument name="productID" type="string" required="false">
		<cfargument name="serialNo" type="string" required="false">

		<cfscript>
			var getCashbackClaimItemsQry = "";
			var whereClause = "cc.CashbackClaimID = "&arguments.CashbackClaimID;
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);

			// NJH 2009/07/02 P-FNL069 - if on portal or not logged in, only get users claims
			if (not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn) {
				whereClause = whereClause & " and cc.personID = " & request.relayCurrentUser.personID;
			}

			if (structKeyExists(arguments,"productID")){
				whereClause = whereClause & " and cbci.productID = "&arguments.productID;
			}

			if (structKeyExists(arguments,"serialNo")){
				whereClause = whereClause & " and cbci.serialNumber = '#arguments.serialNo#'";
			}
		</cfscript>

		<!--- NJH 2009/07/02 P-FNL069 - inner join to cashback claim so that we can filter by the person if needed. --->
		<cfif rights.view>
		<cfif statementView>
			<cfquery name="getCashbackClaimItemsQry" DATASOURCE="#application.SiteDataSource#">
				SELECT cbci.*, p.sku,p.description,p.discountPrice, pg.description as ProductGroupDescription,
					valuePerUnit = cbci.value/cbci.quantity
					FROM CashbackClaimItem cbci with (noLock)
						inner join cashbackClaim cc with (noLock) on cbci.cashbackClaimID = cc.cashbackClaimID
						left outer join
					(product p inner join productGroup pg on pg.productGroupID = p.productGroupID) on cbci.productID = p.productID
				WHERE (cbci.productID = p.productID or cbci.productid = 0)
				AND #preserveSingleQuotes(whereClause)#
				order by #arguments.orderBy#
			</cfquery>
		<cfelse>
			<cfquery name="getCashbackClaimItemsQry" DATASOURCE="#application.SiteDataSource#">
				SELECT cbci.*, p.sku,p.description,p.discountPrice, pg.description as ProductGroupDescription,
					valuePerUnit = cbci.value/cbci.quantity
				FROM CashbackClaimItem cbci inner join product p on cbci.productID = p.productID
					inner join productGroup pg on pg.productGroupID = p.productGroupID
						inner join cashbackClaim cc with (noLock) on cbci.cashbackClaimID = cc.cashbackClaimID
				where #preserveSingleQuotes(whereClause)#
				order by #arguments.orderBy#
			</cfquery>
		</cfif>
		</cfif>

		<cfreturn getCashbackClaimItemsQry>

	</cffunction>


	<!--- NJH 2009/03/04 CR-SNY675 get cashback claim details for a cashback claim --->
	<cffunction access="public" name="getCashbackClaimDetails" returnType="query" hint="All the details of the passed cashbackClaim">
		<cfargument name="CashbackClaimID" type="numeric" required="true">

		<cfscript>
			var getCashbackClaimDetailsQry = "";
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);
		</cfscript>

		<cfif arguments.CashbackClaimID NEQ 0>
		<cfif rights.view>
		<cfquery name="getCashbackClaimDetailsQry" datasource="#application.siteDataSource#">
			select rc.*, rcs.ClaimStatusTextID as status from CashbackClaim rc inner join CashbackClaimStatus rcs
				on rc.ClaimStatusID = rcs.ClaimStatusID
			where CashbackClaimID = #arguments.CashbackClaimID#
		</cfquery>
			</cfif>
		<cfelse>
			<!--- START SSS LHID 2607 Added this so that a blank query will be avaliable for a new cliam --->
			<cfquery name="getCashbackClaimDetailsQry" datasource="#application.siteDataSource#">
				select rc.*, rcs.ClaimStatusTextID as status from CashbackClaim rc inner join CashbackClaimStatus rcs
					on rc.ClaimStatusID = rcs.ClaimStatusID
				where CashbackClaimID = 0
			</cfquery>
			<!--- END SSS LHID 2607 Added this so that a blank query will be avaliable for a new cliam --->
		</cfif>



		<cfreturn getCashbackClaimDetailsQry>

	</cffunction>

	<!--- NJH 2009/03/04 CR-SNY675 - submit cashback claim --->
	<cffunction access="public" name="submitCashbackClaim" hint="Submit the cashback claim and wait for approval">
		<cfargument name="CashbackClaimID" type="numeric" required="yes">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfscript>
			var getCashBackValue = "";
		</cfscript>

		<cfquery name="getCashBackValue" datasource="#application.SiteDataSource#">
			SELECT sum(value) as totalValue
			FROM CashbackClaimItem
			WHERE CashbackClaimID = #arguments.CashbackClaimID#
		</cfquery>

		<cfscript>
			updateCashbackClaim(CashbackClaimID=arguments.CashbackClaimID, statusID="submitted", creditAmount=getCashBackValue.totalValue);
		</cfscript>

		<cfreturn getCashBackValue.totalValue>

	</cffunction>


	<!--- NJH 2009/03/05 CR-SNY675 --->
	<cffunction name="getClaimStatus" access="public" hint="Gets the claim statii">
		<cfargument name="statusID" required="false">

		<cfscript>
			var getClaimStatusQry = "";
		</cfscript>

		<cfquery name="getClaimStatusQry" datasource="#application.siteDataSource#">
			select * from CashbackClaimStatus
			where 1=1
			<cfif structKeyExists(arguments,"statusID")>
				and
				<cfif isNumeric(arguments.statusID)>
					ClaimStatusID  =  <cf_queryparam value="#arguments.statusID#" CFSQLTYPE="cf_sql_integer" >
				<cfelse>
					ClaimStatusTextID =  <cf_queryparam value="#arguments.statusID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
			</cfif>
		</cfquery>

		<cfreturn getClaimStatusQry>
	</cffunction>


	<cffunction name="updateCashbackClaim" access="public" hint="Updates a CashbackClaim">
		<cfargument name="CashbackClaimID" type="numeric" required="true">
		<cfargument name="statusID" required="false">
		<cfargument name="creditAmount" type="numeric" required="false">

		<cfscript>
			var updateCashbackClaim="";
			var claimStatusID = 0;
			var claimStatusTextID = "";
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);
		</cfscript>

		<cfif structKeyExists(arguments,"statusID")>
			<cfset claimStatusID = getClaimStatus(statusID=arguments.statusID).ClaimStatusID>
			<cfset claimStatusTextID = getClaimStatus(statusID=arguments.statusID).ClaimStatusTextID>
		</cfif>

		<cfif rights.edit>
		<cfquery name="updateCashbackClaim" datasource="#application.SiteDataSource#">
			UPDATE CashbackClaim
			SET
				<cfif structKeyExists(arguments,"statusID")>
					ClaimStatusID =  <cf_queryparam value="#claimStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				</cfif>
				<cfif structKeyExists(arguments,"creditAmount")>
					creditAmount = #arguments.creditAmount#,
				</cfif>
				<cfif structKeyExists(arguments,"statusID") and claimStatusTextID eq "approved">
					approvedDate = #createODBCDateTime(request.requestTime)#,
				</cfif>
				<cfif structKeyExists(arguments,"statusID") and claimStatusTextID eq "rejected">
					rejectedDate = #createODBCDateTime(request.requestTime)#,
				</cfif>
				lastUpdated = #createODBCDateTime(request.requestTime)#,
				lastUpdatedBy = #request.relayCurrentUser.userGroupID#
			FROM CashbackClaim cc
			WHERE CashbackClaimID = #arguments.CashbackClaimID#
			<!--- only approve/reject a claim that is not pending. --->
			<cfif structKeyExists(arguments,"statusID") and listFindNoCase("approved,rejected",claimStatusTextID)>
				and ClaimStatusID <> (select ClaimStatusID from CashbackClaimStatus where ClaimStatusTextID='pending')
			</cfif>
		</cfquery>
		</cfif>
	</cffunction>


	<cffunction name="rejectCashbackClaim" access="public" hint="Reject a cashback claim">
		<cfargument name="CashbackClaimID" type="numeric" required="true">

		<!--- NJH 2009/07/03 P-FNL069 - only reject a claim if user is internal --->
		<cfscript>
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);

			if (request.relayCurrentUser.isInternal and rights.edit) {
			updateCashbackClaim(CashbackClaimID=arguments.CashbackClaimID,statusID="rejected");
			}
		</cfscript>
	</cffunction>


	<cffunction name="approveCashbackClaim" access="public" hint="Approve a cashback claim">
		<cfargument name="CashbackClaimID" type="numeric" required="true">

		<!--- NJH 2009/07/03 P-FNL069 - only approve a claim if user is internal --->
		<cfscript>
			var rights = getUserRightsForCashbackClaim(cashbackClaimID = arguments.cashbackClaimID);

			if (request.relayCurrentUser.isInternal and rights.edit) {
			updateCashbackClaim(CashbackClaimID=arguments.CashbackClaimID,statusID="approved");
			}
		</cfscript>
	</cffunction>


	<!--- NJH 2009/07/02 P-FNL069 - determine if a user can access a cashbackClaim --->
	<cffunction name="getUserRightsForCashbackClaim" access="public" returntype="struct">
		<cfargument name="cashbackClaimID" type="numeric">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfscript>
			var getOwnerQry = "";
			var result = {edit=false,view=false};
		</cfscript>

		<!--- assume that any internal user at the moment will have rights --->
		<cfif request.relayCurrentUser.isInternal>
			<cfset result.edit=true>
			<cfset result.view=true>

		<cfelseif (not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn)>
			<cfquery name="getOwnerQry" datasource="#application.siteDataSource#">
				select cc.claimStatusID, ccs.claimStatusTextID from cashbackClaim cc inner join CashbackClaimStatus ccs
					on cc.claimStatusID = ccs.claimStatusID
				where cashbackClaimID = #arguments.cashbackClaimID#
					and personID = #arguments.personID#
			</cfquery>

			<cfif getOwnerQry.recordCount gt 0>
				<cfset result.view=true>
				<!--- if claim status is pending, then user can still edit the cashback claim --->
				<cfif getOwnerQry.claimStatusTextID eq "Pending">
					<cfset result.edit=true>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>
</cfcomponent>
