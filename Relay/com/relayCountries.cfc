﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
Author:			Adam Reynolds
Date created:	2005-04-27

	This provides the set of functionality for countries

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-05-04			AJC			getRegions - Added ability to filter by users regions/countries
2009/04/17			NJH			Sophos Stargate project
2010/04/13			NJH			P-PAN002 - bring back currencyName for country currency query
2010/04/29 			AJC 		P-PAN002 - Removed querySim and specified fields required in query
2011/09/09			PPB			LID7366 allowed specification of dataValueColumn/displayValueColumn from getProvinceList for use by dropdown
2012-10-22 			PPB 		Case 431058 pass useLocalLanguage into getCountryLanguages
2013-12-23 			PPB 		Case 438000 remove spaces from list of currencies in getCountryCurrencies()
2015/03/11			NJH			Jira Fifteen-259 Added isCountryInCountryGroup function
2016/08/22			RJT2		Jira PROD2016-2198 Added isValidCountry() and isValidCountry:anguage() for Phrase API
--->


<cfcomponent displayname="relayCountries" hint="Provides facilities for managing country information.">

<!--- a list comntaining all countryIDs. Used for validating that a countryID exists. isValidCountryID --->
<cfset variables.countryIDs = getCountryIDs()>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the set of available regions.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="getRegions" returntype="query" hint="Returns a regions query object which can be filtered by withCurrencies (t/f) and showCurrentUsersCountriesOnly (t/f), does not include countries." validValues="true">
	<cfargument name="withCurrencies" default="false">
	<!--- 2006-05-04 AJC CR_CHW001 - added ability to filter by users regions/countries --->
	<cfargument name="showCurrentUsersCountriesOnly" default="false">

	<cfset var RegionsQry = "">

	<CFQUERY NAME="RegionsQry" DATASOURCE="#application.sitedatasource#">
		SELECT * from country
			WHERE ISOCode is null
			<cfif withCurrencies>
				AND countryCurrency is not null
			</cfif>
			<!--- 2006-05-04 AJC CR_CHW001 - added ability to filter by users regions/countries --->
			<cfif showCurrentUsersCountriesOnly>
				AND CountryID  IN ( <cf_queryparam value="#request.relaycurrentuser.countrylist#" CFSQLTYPE="cf_sql_integer"  list="true"> )
			</cfif>
			order by CountryDescription
	</CFQUERY>

	<cfreturn RegionsQry>
</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the set of available countries.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="getCountries" returntype="query" validValues="true" hint="Returns a countries query object which can be filtered by withCurrencies (t/f) and showCurrentUsersCountriesOnly (t/f), does not include regions.">
	<cfargument name="withCurrencies" default="false">
	<!--- 2006-05-04 AJC CR_CHW001 - added ability to filter by users regions/countries --->
	<cfargument name="showCurrentUsersCountriesOnly" default="false">

	<cfset var CountriesQry = "">
	<cfset var RightsFilter = application.com.rights.getRightsFilterQuerySnippet(entityType="country",alias="c",useJoins=true)>

	<CFQUERY NAME="CountriesQry" DATASOURCE="#application.sitedatasource#">
		SELECT * from country c
		<cfif arguments.showCurrentUsersCountriesOnly>#RightsFilter.join#</cfif>
			WHERE ISOCode is not null
			<cfif arguments.withCurrencies>
				AND countryCurrency is not null
			</cfif>
			<!--- 2006-05-04 AJC CR_CHW001 - added ability to filter by users regions/countries --->
			<!--- <cfif showCurrentUsersCountriesOnly>
				AND CountryID IN (#request.relaycurrentuser.countrylist#)
			</cfif> --->
			order by CountryDescription
	</CFQUERY>

	<cfreturn CountriesQry>
</cffunction>

<!--- WAB 2011/11/15 New function first used in settings --->
<cffunction name="getCountriesValidValues" returntype="query" hint="Returns a countries query for use in validvalues" validvalues="true">
	<cfargument name="liveCountriesOnly" default="true">
	<cfargument name="showCurrentUsersCountriesOnly" default="false">

	<cfset var CountriesQry = "">

	<CFQUERY NAME="CountriesQry" DATASOURCE="#application.sitedatasource#">
		SELECT countryid as dataValue, countrydescription as displayValue
		from country
			WHERE ISOCode is not null
			<!--- 2006-05-04 AJC CR_CHW001 - added ability to filter by users regions/countries --->
			<cfif showCurrentUsersCountriesOnly>
				AND CountryID  IN ( <cf_queryparam value="#request.relaycurrentuser.countrylist#" CFSQLTYPE="cf_sql_integer"  list="true"> )
			</cfif>
			<cfif liveCountriesOnly>
				AND CountryID  IN ( <cf_queryparam value="#request.currentsite.livecountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		order by CountryDescription
	</CFQUERY>

	<cfreturn CountriesQry>
</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the set of available regions.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="getCountryDetails" returntype="query">
	<cfargument name="countryID"  required="yes">
	<cfset var CountryQry = "">

	<CFQUERY NAME="CountryQry" DATASOURCE="#application.sitedatasource#">
		SELECT * from country
			WHERE countryID  =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="cf_sql_integer" >
	</CFQUERY>

	<cfreturn CountryQry>
</cffunction>

<cffunction name="getCountryIDs" returntype="any">
	<cfset var CountryIDQry = "">

	<CFQUERY NAME="CountryIDQry" DATASOURCE="#application.sitedatasource#">
		SELECT countryID from country
	</CFQUERY>

	<cfreturn valueList( CountryIDQry.countryID )>

</cffunction>


	<cffunction access="public" name="getProvinceList" hint="Retrieves a list of provinces for the countryID." returntype="query">
		<cfargument name="CountryID" required="no" type="numeric" hint="provide this to get the Provinces or States in a Country" >
		<cfargument name="provinceID" required="no" type="numeric" hint="Pre select a province" >
		<cfargument name="dataValueColumn" required="no" type="string" hint="" default="stateAbbreviation" >	<!--- 2011/09/08 PPB LID7366 optionally send in dataValueColumn/displayValueColumn to allow the column names to be set for a dropdown --->
		<cfargument name="displayValueColumn" required="no" type="string" hint="" default="state" >

		<cfscript>
			var qryProvinces = "";
		</cfscript>

		<!--- NJH 2009/04/17 - Sophos Stargate project - added stateAbbreviation --->
		<cfquery name="qryProvinces" datasource="#application.siteDataSource#">
			SELECT provinceID, countryID, Name AS <cfoutput>#displayValueColumn#</cfoutput>, abbreviation AS <cfoutput>#dataValueColumn#</cfoutput>
			FROM Province
			where 1=1
			<cfif isdefined('arguments.countryID') >
				AND	countryID = <cfqueryparam value="#val(arguments.countryID)#" cfsqltype="cf_sql_integer" >
			</cfif>
			ORDER BY Name
		</cfquery>

 		<cfreturn qryProvinces/>
	</cffunction>

	<cffunction name="getCountryCurrencies" access="public" returntype="query" validValues="true">
		<cfargument name="countryID" required="false" type="numeric">

		<cfset var currencyList = "">
		<cfset var getCountryCurrency = "">
		<cfset var getCurrencies = "">

		<!--- NJH case 436445 - rather than check record count, check listLen of currencyList, as we have a record count but without any data, so that in statement looked like
			in ('',)
		 --->

		<cfquery name="getCountryCurrency" datasource="#application.siteDataSource#">
			select countryCurrency from country
			<cfif structKeyExists(arguments,"countryID")>
			where countryID=#arguments.countryID#
			</cfif>
		</cfquery>

		<cfset currencyList = replace(valueList(getCountryCurrency.countryCurrency)," ","")>		<!--- 2013-12-23 PPB Case 438000 remove spaces in case db has a space eg 'USD, CAD'  --->

		<cfquery name="getCurrencies" datasource="#application.siteDataSource#">
			select currencyISOCode as currencyISO, CurrencyName from currency where currencyISOCode in (''<cfif listLen(currencyList)>,#listQualify(currencyList,"'")#</cfif>)
			order by currencyISOCode
		</cfquery>

		<cfreturn getCurrencies>
	</cffunction>


	<cffunction name="getCountryLanguages" access="public" returntype="query">
		<cfargument name="countryID" type="numeric" required="false">
		<cfargument name="useLocalLanguage" type="Boolean" required="false" default="false">		<!--- 2012-10-22 PPB Case 431058 useLocalLanguage --->

		<cfset var getLanguages = "">
		<cfset var countryLanguageIDList = application.com.relayTranslations.getCountryLanguages(countryID=arguments.countryID)>

		<cfquery name="getLanguages" datasource="#application.siteDataSource#">
			select languageId
			<!--- 2012-10-22 PPB Case 431058 START useLocalLanguage --->
			<cfif useLocalLanguage>
				,language = CASE WHEN locallanguagename IS NULL THEN language ELSE locallanguagename END
			<cfelse>
				,language
			</cfif>
			<!--- 2012-10-22 PPB Case 431058 END --->
			from language where languageID  in ( <cf_queryparam value="#countryLanguageIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>

		<cfreturn getLanguages>
	</cffunction>

	<!--- NJH 2013Roadmap 2012/12/05 --->
	<cffunction name="getCountryGroupCountries" access="public" returnType="query" validValues="true">
		<cfargument name="AllCountriesID" type="numeric" default="37" hint="The value to retrieve for all countries">

		<cfset var getCountryGroupCountriesQry = "">
		<cfset var rights = application.com.rights.getRightsFilterQuerySnippet(entityType="vCountryGroupCountry",alias="v",regionList=true)>

		<cfquery name="getCountryGroupCountriesQry" datasource="#application.siteDataSource#">
			select v.countryId,v.countryDescription,v.countryOrder
		  	from vCountryGroupCountry v
		  		#rights.join#
		 	where v.countryId != <cfif arguments.AllCountriesID eq 37>0<cfelse>37</cfif>
		 	<cfif arguments.AllCountriesID eq 0>
			union
			select v.countryId,v.countryDescription,v.countryOrder
		  	from vCountryGroupCountry v
		  	where v.countryID = 0
			</cfif>
  			order by v.countryOrder, v.countryDescription
		</cfquery>

		<cfreturn getCountryGroupCountriesQry>
	</cffunction>


	<cffunction name="isCountryInCountryGroup" access="public" returnType="boolean" output="false" hint="Returns whether a country is in a given countryGroup">
		<cfargument name="country" type="string" required="true">
		<cfargument name="countryGroup" type="string" required="true" hint="Can be a list of country groups">

		<cfset var isCountryInCountryGroup = "">

		<cfquery name="isCountryInCountryGroup">
			select 1 from vCountryGroupList where
				<cfif arguments.country neq "">
					<cf_queryparam value="#arguments.country#" cfsqltype="cf_sql_varchar"> <cfif isNumeric(arguments.country)> = countryMemberId<cfelse> in (country,countryISO)</cfif>
				<cfelse>
					1=0
				</cfif>
				and
				<cfif arguments.countryGroup neq "">
					<cfif isNumeric(listFirst(arguments.countryGroup))>countryId<cfelse>countryGroup</cfif> in (<cf_queryparam value="#arguments.countryGroup#" cfsqltype="cf_sql_varchar" list="true">)
				<cfelse>
					1=0
				</cfif>
		</cfquery>

		<cfreturn isCountryInCountryGroup.recordCount?true:false>
	</cffunction>

	<cffunction name="convertISOCodesToCountryIDs" access="public" returnType="string" output="false">
		<cfargument name="countryISOCodeList" type="String" required="true">

		<cfquery name="countryIDQuery">
			select countryID
			from country
			where ISOCode in (<cf_queryparam value="#countryISOCodeList#" cfsqltype="CF_SQL_VARCHAR" list="true">)
		</cfquery>

		<cfset var idArray=ArrayNew(1)>

		<cfloop query="#countryIDQuery#">
			<cfset ArrayAppend(idArray,countryIDQuery.countryID)>
		</cfloop>

		<cfreturn ArrayToList(idArray)>

	</cffunction>


	<cffunction name="updateInternalUserCountryRights" access="public" returnType="void" output="false">
		<cfargument name="internalPersonID" type="numeric" required="true">
		<cfargument name="countryIDList" type="String" required="true">

		<cfscript>
			var lastUpdatedBy=isDefined("request.relayCurrentUser.userGroupID")?request.relayCurrentUser.userGroupID:404;
		</cfscript>

		<cfquery name="setUserGroupCountry">
			declare @personalsergroupID int;
			set @personalsergroupID=(select usergroupID from usergroup where PersonID=<cf_queryparam value="#arguments.internalPersonID#" cfsqltype="cf_sql_integer">)

			insert into userGroupCountry (userGroupID,countryID,createdBy,created,lastUpdatedBy,lastUpdated)
			select @personalsergroupID, c.CountryID, <cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,getDate(),<cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,getDate()
			from country as c
	 		where
	 		c.CountryID  in (<cf_queryparam value="#arguments.countryIDList#" cfsqltype="cf_sql_integer" list="true">)
			and c.ISOCode is not null
			and c.CountryID not in (select countryID from userGroupCountry where userGroupID = @personalsergroupID)

			delete from userGroupCountry
			where
			countryID not in (<cf_queryparam value="#arguments.countryIDList#" cfsqltype="cf_sql_integer" list="true">)
			and countryID !=0
			and userGroupID = @personalsergroupID

		</cfquery>

		<cfquery name="setRights">
			declare @RecordTaskSecurityTypeID int;
			set @RecordTaskSecurityTypeID=(select SecurityTypeID from securityType where shortname = 'RecordTask');

			declare @personalsergroupID int;
			set @personalsergroupID=(select usergroupID from usergroup where PersonID=<cf_queryparam value="#arguments.internalPersonID#" cfsqltype="cf_sql_integer">)


			insert into rights (userGroupID, securityTypeID, countryID, permission)
			select distinct u.userGroupID, @RecordTaskSecurityTypeID, c.countryID, 7
			from usergroup as u,
				country c
			where
			personID =  <cf_queryparam value="#arguments.internalPersonID#" cfsqltype="cf_sql_integer">
			and c.countryID in (<cf_queryparam value="#arguments.countryIDList#" cfsqltype="cf_sql_integer" list="true">)
			and c.countryID not in (select countryID from rights where userGroupID = @personalsergroupID and securityTypeID = @RecordTaskSecurityTypeID and permission = 7)

			delete from rights
			where
			userGroupID=@personalsergroupID
			and permission = 7
			and countryID !=0
			and securityTypeID = @RecordTaskSecurityTypeID
			and countryID not in (<cf_queryparam value="#arguments.countryIDList#" cfsqltype="cf_sql_integer" list="true">)
		</cfquery>

	</cffunction>


	<cffunction name="isValidCountry" access="public" returnType="any" output="false">

		<cfargument name="countryID" required="true">

		<cfreturn !!listFind( variables.countryIDs , arguments.countryID )>

	</cffunction>

	<cffunction name="isValidCountryLanguage" access="public" returnType="any" output="false">

		<cfargument name="countryID" required="true">
		<cfargument name="languageID" required="true">

		<cfset countryLanguages = getCountryLanguages( arguments.countryID )>
		<cfset var languageIDs = valueList( countryLanguages.languageID )>
		<cfreturn !!listfind( languageIDs , arguments.languageID )>

	</cffunction>

	<cffunction name="getCountryIdListFromISOList" returntype="string" access="public"
				hint="Returns a list of Country IDs from a list of country ISO initials">
		<cfargument name="countrylist" type="string" required="true">
		<cfscript>
			var idArray=arrayNew(1);
	        if (arguments.countryList neq "") {
	            var queryService = new query();
	            queryService.setSQL("
	                SELECT
	                    CountryID
	                FROM
	                    country
	                WHERE
	                    ISOCode 
	                IN (:codeList)
	            ");
	            queryService.addParam(name="codeList", value=#arguments.countryList#, CFSQLTYPE="CF_SQL_VARCHAR", list="true");
	            var queryResult=queryService.execute();
	            for (var row=1; row LTE queryResult.getResult().recordcount; row++) {
	                arrayAppend(idArray, queryResult.getResult().CountryID[row]);
	            }
	        }
	        return arrayToList(idArray);
		</cfscript>
	</cffunction>


</cfcomponent>
