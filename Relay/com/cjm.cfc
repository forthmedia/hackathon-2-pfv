<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			  CJM Component version 1
Recent Authors:	  YMA
Original Author : David McLean (DAM)
Date created:	  3rd May 2005

Description:  Contains all database functions for CJM Module

Version history:
version		date		initials	notes
1			3rd may 05	DAM			First Write Up
1.1			2005-08-08	AJC			Created AssignCampaignApprovers
1.2.1		2005-08-09	AJC			Added in createdby and lastupdated to a number of functions
1.2.2		2005-08-09	AJC			Created CampaignApprovalAction
			2009-06-26	NYB 		P-FNL069
			2010/10/05  WAB         replaced application.userfiles absolutepath & application.files directory with application.paths.content
			2014/12/08	YMA			wiped all old functions as part of module re-hash.
			2015-11-10  ACPK        PROD2015-24 Added loadCustomizedTemplateCanvasBody function; updated saveCustomizedTemplate to save new parameters to database; updated exportTemplate to specify filename
			2015-12-01  ACPK        PROD2015-467 Disabled redundant code to allow use of quote marks in title/description
			2015-12-07  ACPK        BF-59 Decode URL-encoded filenames for uploaded images before copying
--->

<cfcomponent displayname="CJM">
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Returns confirmation details           
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction returntype="query" access="public" name="getCJMTemplate" hint="returns CJM templates">
		<cfargument name="CJMTemplateID" required="false">
		<cfargument name="templateTypeIDs" default="">

		<cfscript>
			var getCJMTemplates="";
			var cjmTitlePhraseSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(baseTableAlias = "template", entityType = "CJMTemplate", phraseTextID = "title");
			var cjmDescriptionPhraseSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(baseTableAlias = "template", entityType = "CJMTemplate", phraseTextID = "description");		
		</cfscript>

		<CFQUERY NAME="getCJMTemplates" DATASOURCE="#application.sitedataSource#">
			select *
				,#cjmTitlePhraseSnippet.select# as cjmTitle
				,#cjmDescriptionPhraseSnippet.select# as cjmDescription
			from CJMTemplate template
			join CJMTemplateType as templateType on template.cjmtemplateTypeID = templateType.cjmtemplateTypeID
			#preservesinglequotes(cjmTitlePhraseSnippet.join)# 
			#preservesinglequotes(cjmDescriptionPhraseSnippet.join)# 
			where 1=1
			<cfif arguments.templateTypeIDs neq "">
				and templateType.CJMTemplateTypeID  in ( <cf_queryparam value="#arguments.templateTypeIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
			</cfif>
			<cfif structKeyExists(arguments,"CJMTemplateID")>
				and CJMTemplateID = <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
			</cfif>
			<cfif not request.relaycurrentuser.isinternal>
				and active = 1
				and 1 = case hasRecordRights_1
							when 1 then (select top 1 1 from vRecordRights 
											where personID = <cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="CF_SQL_INTEGER">
											and entity = 'CJMTemplate' 
											and recordID = template.CJMTemplateID)
							else 1 end
				and 1 = case hasCountryScope_1
							when 1 then (select top 1 1 from vCountryScope 
											where countryID = <cf_queryparam value="#request.relaycurrentuser.countryID#" CFSQLTYPE="CF_SQL_INTEGER">
											and entity = 'CJMTemplate' 
											and entityID = template.CJMTemplateID)
							else 1 end
			</cfif>
			
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		</CFQUERY>
		
		<cfreturn getCJMTemplates>
	</cffunction>

	<cffunction name="deleteCJMTemplate" access="public">
		<cfargument name="CJMTemplateID" required="true">
		
		<cfset var result = application.com.fileManager.deleteAllLinkedImages(arguments.CJMTemplateID,application.entitytypeID["CJMTemplate"])>
		<cfset var deleteTemplate = "">
		
		<cfif result.isOK>
			<!--- delete template --->
			<cfquery name="deleteTemplate">
				delete from CJMTemplate where CJMTemplateID = <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
			</cfquery>
			<cfquery name="deleteRecordRights">
				delete from countryScope 
					where entityID = <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
					and entity = 'CJMTemplate'
			</cfquery>
			<cfquery name="deleteCountryScope">
				delete from recordrights 
					where recordID = <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
					and entity = 'CJMTemplate'
			</cfquery>
		</cfif>
		
		<cfreturn result>
		
	</cffunction>

	<cffunction returntype="query" access="public" name="getCustomizedTemplate" hint="returns Customized CJM templates">
		<cfargument name="personID" required="true">
		<cfargument name="cjmCustomizedTemplateID" required="false">
		<cfargument name="templateTypeIDs" default="">

		<cfscript>
			var  getCustomizedTemplates="";
			var cjmTitlePhraseSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(baseTableAlias = "cjmT", entityType = "CJMTemplate", phraseTextID = "title");
			var cjmDescriptionPhraseSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(baseTableAlias = "cjmT", entityType = "CJMTemplate", phraseTextID = "description");		
		</cfscript>

		<CFQUERY NAME="getCustomizedTemplates" DATASOURCE="#application.sitedataSource#">
			select 
			#cjmTitlePhraseSnippet.select# as cjmTitle,
			#cjmDescriptionPhraseSnippet.select# as cjmDescription,
			cjmCT.lastupdated as cjmCTLastUpdated,
			cjmCT.created as cjmCTCreated,
			* from cjmCustomizedTemplate as cjmCT
			inner join cjmTemplate as cjmT on cjmCT.cjmTemplateID = cjmT.cjmTemplateID
			join CJMTemplateType as templateType on cjmT.cjmtemplateTypeID = templateType.cjmtemplateTypeID
			#preservesinglequotes(cjmTitlePhraseSnippet.join)# 
			#preservesinglequotes(cjmDescriptionPhraseSnippet.join)# 
			where cjmCT.personID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER">
			and cjmT.active = 1
			<cfif arguments.templateTypeIDs neq "">
				and templateType.CJMTemplateTypeID  in ( <cf_queryparam value="#arguments.templateTypeIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
			</cfif>
			<cfif structKeyExists(arguments,"cjmCustomizedTemplateID")>
				and cjmCT.cjmCustomizedTemplateID = <cf_queryparam value="#arguments.cjmCustomizedTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
			</cfif>
			<cfif not request.relaycurrentuser.isinternal>
				and active = 1
				and 1 = case hasRecordRights_1
							when 1 then (select top 1 1 from vRecordRights 
											where personID = <cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="CF_SQL_INTEGER">
											and entity = 'CJMTemplate' 
											and recordID = cjmT.CJMTemplateID)
							else 1 end
				and 1 = case hasCountryScope_1
							when 1 then (select top 1 1 from vCountryScope 
											where countryID = <cf_queryparam value="#request.relaycurrentuser.countryID#" CFSQLTYPE="CF_SQL_INTEGER">
											and entity = 'CJMTemplate' 
											and entityID = cjmT.CJMTemplateID)
							else 1 end
			</cfif>
		</CFQUERY>
		
		<cfreturn getCustomizedTemplates>
	</cffunction>
		
	<cffunction name="loadTemplateAttributes" access="public">
		<cfargument name="CJMTemplateID" required="true">

		<cfquery name="getData">
			SELECT jsonContent FROM CJMTemplate
			WHERE CJMTemplateID = <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>

		<cfreturn getData.jsonContent>
	</cffunction>
	
	<cffunction name="loadCustomizedTemplateAttributes" access="public">
		<cfargument name="cjmCustomizedTemplateID" required="true">

		<cfquery name="getData">
			SELECT customizedJsonContent as jsonContent FROM CJMCustomizedTemplate
			WHERE cjmCustomizedTemplateID = <cf_queryparam value="#arguments.cjmCustomizedTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>

		<cfreturn getData.jsonContent>
	</cffunction>
	
    <cffunction name="loadCustomizedTemplateCanvasBody" access="public">
        <cfargument name="cjmCustomizedTemplateID" required="true">

        <cfquery name="getData">
            SELECT canvasBody FROM CJMCustomizedTemplate
            WHERE cjmCustomizedTemplateID = <cf_queryparam value="#arguments.cjmCustomizedTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
        </cfquery>

        <cfreturn getData.canvasBody>
    </cffunction>
	
	<cffunction name="saveTemplateAttributes" access="public">
		<cfargument name="CJMTemplateID" required="true">
		<cfargument name="data" required="true">

		<cfset var result = {isOK=true,message="Template saved successfully"}>
		<cfset var updateTable = "">
		
		<cftry>
			<cfquery name="updateTable">
				UPDATE CJMTemplate
				SET jsonContent = <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR">
				WHERE CJMTemplateID = <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
			</cfquery>
			<cfcatch>
		          <cfset result.isOK = false>
		          <cfset result.message = "Error saving template">
		     </cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>
	
	<cffunction name="saveCustomizedTemplate" access="public">
		<cfargument name="CJMTemplateID" required="true">
		<cfargument name="data" required="true">
		<cfargument name="CJMCustomizedTemplateID" required="false" default="0">
		<cfargument name="canvasBody" required="true"> <!--- 2015-11-10    ACPK    PROD2015-24 Added new parameters--->
		<cfargument name="customTitle" required="true">
        <cfargument name="customDescription" required="false">

		<cfset var result = {isOK=true,message="Template saved successfully",cjmCustomizedTemplateID=arguments.CJMCustomizedTemplateID}>
		<cfset var updateTable = "">
		<!--- 2015-12-01   ACPK    PROD2015-467 Disabled redundant code to allow use of quote marks in title/description --->
		<!--- 2015-11-10   ACPK    PROD2015-24 Remove unsafe characters from custom title/description --->
        <!--- <cfset arguments.customTitle = REReplace(arguments.customTitle,'["]','','all')>
		<cfset arguments.customDescription = REReplace(arguments.customDescription,'["]','','all')> --->
		
		<cftry>
			<cfif arguments.CJMCustomizedTemplateID eq 0>
				<!--- add new --->
				<!--- 2015-11-10    ACPK    PROD2015-24 Added canvasBody, customTitle, and customDescription to insert query --->
				<cfquery name="updateTable">
					insert into CJMCustomizedTemplate
					select <cf_queryparam value="#arguments.CJMTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
							,<cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR">
							,<cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="CF_SQL_INTEGER">
							,<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="CF_SQL_INTEGER">
							,getdate()
							,<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="CF_SQL_INTEGER">
							,getdate()
							,<cf_queryparam value="#arguments.canvasBody#" CFSQLTYPE="CF_SQL_VARCHAR">
							,<cf_queryparam value="#arguments.customTitle#" CFSQLTYPE="CF_SQL_VARCHAR">
							,<cf_queryparam value="#arguments.customDescription#" CFSQLTYPE="CF_SQL_VARCHAR">
							
					select scope_identity() as insertedID
				</cfquery>
				
		        <cfset result.cjmCustomizedTemplateID = updateTable.insertedID>
			<cfelse>
				<!--- save existing --->
				<!--- 2015-11-10    ACPK    PROD2015-24 Added canvasBody, customTitle, and customDescription to update query --->
				<cfquery name="updateTable">
					UPDATE CJMCustomizedTemplate
					SET customizedJsonContent = <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR">,
						lastupdatedby = <cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="CF_SQL_INTEGER">,
						lastupdated = getdate(),
						canvasBody = <cf_queryparam value="#arguments.canvasBody#" CFSQLTYPE="CF_SQL_VARCHAR">,
						customTitle = <cf_queryparam value="#customTitle#" CFSQLTYPE="CF_SQL_VARCHAR">,
						customDescription = <cf_queryparam value="#customDescription#" CFSQLTYPE="CF_SQL_VARCHAR">
					WHERE CJMCustomizedTemplateID = <cf_queryparam value="#arguments.CJMCustomizedTemplateID#" CFSQLTYPE="CF_SQL_INTEGER">
				</cfquery>
			
			</cfif>
		    
		    <cfset result.data = arguments.data>
			
			<cfcatch>
		          <cfset result.isOK = false>
		          <cfset result.message = "Error saving template">
		     </cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>
	
	<cffunction name="exportTemplate" access="remote">
		<cfargument name="canvasObj" required="true">
		<cfargument name="exportType" required="true" default="pdf">
		<cfargument name="filename" required="true"> <!--- 2015-11-10    ACPK    PROD2015-24 Added filename parameter--->
		
		<cfset var folder = application.com.fileManager.getTemporaryFolderDetails()>
		<cfset var orientation = 'portrait'>
		<cfset var template= DeserializeJSON(arguments.canvasObj)>
		<cfset var width = "">
		<cfset var height = "">
		<cfset var pdfwidth = "">
		<cfset var pdfheight = "">
		
		<cfset height = replace(template[1]['height'],"px","")>
		<cfset width = replace(template[1]['width'],"px","")>
		<!--- 2015-11-10   ACPK    PROD2015-24 Remove unsafe characters and substituted angle brackets from filename --->
		<cfset filename = REReplace(arguments.filename,'[\\/:*?"<>|]|&lt;|&gt;','','all')>
		
		<cfif width gt height>
			<cfset orientation = 'landscape'>
		</cfif>
		
		<!--- pdf stores at 300 dpi but must be at least 2 inches square --->
		<cfset pdfwidth = width/300>
		<cfset pdfheight = height/300>
		<cfswitch expression="#orientation#">
			<cfcase value="landscape">
				<cfif pdfheight lt 2>
					<cfset pdfwidth = pdfwidth*(2/pdfheight)>
					<cfset pdfheight = 2>
				</cfif>
			</cfcase>
			<cfcase value="portrait">
				<cfif pdfwidth lt 2>
					<cfset pdfheight = pdfheight*(2/pdfwidth)>
					<cfset pdfwidth = 2>
				</cfif>
			</cfcase>
		</cfswitch>
		
		<cfloop index="page" from="1" to="#structCount(template)#">
			<cfset template[page]['canvas'] = rereplace(template[page]['canvas'],"&##60;","<","all")>
			<cfset template[page]['canvas'] = rereplace(template[page]['canvas'],"&##62;",">","all")>
			<cfset template[page]['canvas'] = rereplace(template[page]['canvas'],"\n","<br/>","all")>
			<cfset template[page]['canvas'] = rereplace(template[page]['canvas'],"<pre","<div","all")>
			<cfset template[page]['canvas'] = rereplace(template[page]['canvas'],"/pre>","/div>","all")>
		</cfloop>
		
		<cfswitch expression="#arguments.exportType#">
			<cfcase value="png">
				
				<cfsavecontent variable="canvas">
					<!DOCTYPE html>
					<html>
						<head>
							<link rel="stylesheet" href="../styles/CJMExport.css" type="text/css">
						</head>
						<body class="converted">
							<table cellspacing="0" style="padding:0px; margin: 0x; border: 0px;" >
								<cfloop index="page" from="1" to="#structCount(template)#">
									<tr><td style="padding:0px; page-break-inside: avoid;<cfif page lt structCount(template)> page-break-after: always;</cfif>">
										<cfoutput>#template[page]['canvas']#</cfoutput>
									</td></tr>
								</cfloop>
							</table>
						</body>
					</html>
				</cfsavecontent>
				
				<!--- find any html images --->
				<cfset occurances = application.com.regExp.refindAllOccurrences(reg_expression="(href|src)\s*=\s*""([^\s]+\/\/[^\/]+.\/[^\s]+\.(jpg|jpeg|png|gif|bmp))",string=rereplace(canvas,"&quot;","","all"))>
				<!--- for each background image modify the content to point to the local copy --->
				<cfloop array="#occurances#" index="occurance">
					<cfif comparenocase(left(occurance['3'],4),"http")>
						<cfset canvas = replace(canvas,occurance['3'],"#request.currentsite.PROTOCOLANDDOMAIN#/#occurance['3']#")>
					</cfif>
				</cfloop>
				
				<cfdocument userAgent="html" name="exportedDoc" localURL="yes" format="PDF" overwrite="true" unit="in" orientation="#orientation#" pageType="a4" pageHeight="#pdfheight#" pageWidth="#pdfwidth#" margintop="0" marginbottom="0" marginright="0" marginleft="0">
					<cfoutput>#canvas#</cfoutput>
				</cfdocument>
				<cfloop index="page" from="1" to="#structCount(template)#">
					<cfset convertedImage = application.com.relayImage.convertPdfToImage(source=exportedDoc,destination="#folder.PATH#",filename="#filename#",page=page,width=width,height=height)> <!--- 2015-11-10    ACPK    PROD2015-24 Save current page as image using filename parameter --->
				</cfloop>
				<cfif structCount(template) gt 1>
					<cfzip
						action="zip"
					    file = "#folder.PATH##filename#.zip" <!--- 2015-11-10    ACPK    PROD2015-24 Save all images as ZIP using filename parameter --->
					    source = "#folder.PATH#"
					    recurse="true"
					    overwrite="true" />
					<cfset exportedDoc = "#folder.WEBPATH##URLEncodedFormat(filename)#.zip"> <!--- 2015-11-10    ACPK    PROD2015-24 Return encoded URL of ZIP (used by Template Editor) --->
				<cfelse>
					<cfset exportedDoc = "#folder.WEBPATH##URLEncodedFormat(convertedImage)#"> <!--- 2015-11-10    ACPK    PROD2015-24 Return encoded URL of image (used by Template Editor) --->
				</cfif>
			</cfcase>
			
			<cfcase value="zip">
				<cfloop index="page" from="1" to="#structCount(template)#">
					<cfset canvas = #template[page]['canvas']#>
					<!--- find any css background images --->
					<cfset occurances = application.com.regExp.refindAllOccurrences(reg_expression="\burl\s*\(\s*[""']?([^""'\r\n,]+)[""']?\s*\)",string=rereplace(canvas,"&quot;","","all"))>
					<!--- for each background image save a local copy then modify the content to point to the local copy --->
					<cfloop array="#occurances#" index="occurance">
						
						<cfif not directoryExists("#folder.PATH#images\")>
							<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath="#folder.PATH#images\")>
						</cfif>
						<cffile 
						    action = "copy" 
						    destination = "#folder.PATH#images\#listLast(occurance['2'],'/')#"
						    source="#occurance['2']#">
						    
						<cfset canvas = replace(canvas,occurance['2'],"images/#listLast(occurance['2'],'/')#")>
					</cfloop>
					
					<!--- find any html images --->
					<cfset occurances = application.com.regExp.refindAllOccurrences(reg_expression="(href|src)\s*=\s*""([^\s]+\/\/[^\/]+.\/[^\s]+\.(jpg|jpeg|png|gif|bmp))",string=rereplace(canvas,"&quot;","","all"))>
					<!--- for each background image save a local copy then modify the content to point to the local copy --->
					<cfloop array="#occurances#" index="occurance">
						
						<cfif not directoryExists("#folder.PATH#images\")>
							<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath="#folder.PATH#images\")>
						</cfif>
						
						<cfset canvas = replace(canvas,occurance['3'],"images/#listLast(occurance['3'],'/')#")>
						
						<cfif findNoCase("/content/",occurance['3']) gt 1>
							<cfset occurance['3'] = right(occurance['3'], len(occurance['3']) - findNoCase("/content/",occurance['3']))>
						</cfif>
						<!--- 2015-12-07  ACPK    BF-59 Decode URL-encoded filenames for uploaded images before copying --->
						<cffile 
						    action = "copy" 
						    destination = "#folder.PATH#images\#URLDecode(listLast(occurance['3'],'/'))#"
						    source="#application.paths.userfiles#\#URLDecode(occurance['3'])#">
					</cfloop>
					
					<cfsavecontent variable="canvas">
						<!DOCTYPE html>
						<html>
							<head>
								<style type="text/css">
									<cfinclude template="/styles/CJMExport.css" />
								</style>
							</head>
							<body>
								<cfoutput>#canvas#</cfoutput>
							</body>
						</html>
					</cfsavecontent>
					<cfsavecontent variable="export">
						<cfoutput>#canvas#</cfoutput>
					</cfsavecontent>
					<!--- save the html --->
					<cffile 
					    action = "write" 
					    file = "#folder.PATH##filename#_page_#page#.html" <!--- 2015-11-10    ACPK    PROD2015-24 Save current page as HTML using filename parameter --->
					    output = "#export#"
					    charset = "utf-8" >
				</cfloop>
				<!--- zip up the images and html --->
				<cfzip
					action="zip"
				    file = "#folder.PATH##filename#.zip" <!--- 2015-11-10    ACPK    PROD2015-24 Save all files as ZIP using filename parameter --->
				    source = "#folder.PATH#"
				    recurse="true"
				    overwrite="true" />
				<cfset exportedDoc = "#folder.WEBPATH##URLEncodedFormat(filename)#.zip"> <!--- 2015-11-10    ACPK    PROD2015-24 Return encoded URL of ZIP (used by Template Editor) --->
			</cfcase>
			<cfdefaultcase>
				<cfsavecontent variable="canvas">
					<!DOCTYPE html>
					<html>
						<head>
							<link rel="stylesheet" href="../styles/CJMExport.css" type="text/css">
						</head>
						<body class="converted">
							<table cellspacing="0" style="padding:0px; margin: 0x; border: 0px;" >
							
							<cfloop index="page" from="1" to="#structCount(template)#">
									<tr><td style="padding:0px; page-break-inside: avoid;<cfif page lt structCount(template)> page-break-after: always;</cfif>">
										<cfoutput>#template[page]['canvas']#</cfoutput>
									
									</td></tr>
							</cfloop>
							</table>
						</body>
					</html>
				</cfsavecontent>
				
				<!--- find any html images --->
				<cfset occurances = application.com.regExp.refindAllOccurrences(reg_expression="(href|src)\s*=\s*""([^\s]+\/\/[^\/]+.\/[^\s]+\.(jpg|jpeg|png|gif|bmp))",string=rereplace(canvas,"&quot;","","all"))>
				<!--- for each background image modify the content to point to the local copy --->
				<cfloop array="#occurances#" index="occurance">
					<cfif comparenocase(left(occurance['3'],4),"http")>
						<cfset canvas = replace(canvas,occurance['3'],"#request.currentsite.PROTOCOLANDDOMAIN#/#occurance['3']#")>
					</cfif>
				</cfloop>
				
				<!--- 2015-11-10    ACPK    PROD2015-24 Save PDF using filename parameter --->
				<cfdocument userAgent="html" filename="#folder.path#/#filename#.pdf" localURL="yes" format="PDF" overwrite="true" unit="in"  pageType="custom" pageHeight="#pdfheight#" pageWidth="#pdfwidth#" margintop="0" marginbottom="0" marginright="0" marginleft="0">
					<cfoutput>#canvas#</cfoutput>
				</cfdocument>
		
				<cfset exportedDoc = "#folder.WEBPATH##URLEncodedFormat(filename)#.pdf"> <!--- 2015-11-10    ACPK    PROD2015-24 Return encoded URL of PDF (used by Template Editor)--->
				<cfset exportedFilePath = "#folder.path#/#filename#.pdf"> <!--- 2015-11-10    ACPK    PROD2015-24 Return filepath of PDF (used by Email Template form to attach file to email)--->
			</cfdefaultcase>
		</cfswitch>
		
		<!--- 2015-11-10    ACPK    PROD2015-24 exportedFilePath not defined (or needed) when exporting to ZIP/PNG --->
		<cfif !isDefined("exportedFilePath")>
			<cfset exportedFilePath = "">
		</cfif>
		
		<cfreturn {isOK=true,message="Template exported successfully",exportType=arguments.exportType,exportedDoc=exportedDoc,exportedFilePath=exportedFilePath}> <!--- 2015-11-10    ACPK    PROD2015-24 Return URL and file path of exported template --->
	</cffunction>
		
</cfcomponent>

