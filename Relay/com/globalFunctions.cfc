<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
    2009-03-09		NYB Sophos Stargate 2 - created roundNumbers function
	2009/04/16 		WAB  Added function to clear the content of resetCFHtmlHead
	2010/05/24		NJH	LID 3286: Sony relayWebServices Error - added isoEncode function (actually, it's Will's function)
	2011/05/25	NYB		REL106 added ListRemoveUnfound function
	2011/05/19		NJH	8.3.1 Added lock functions
	2011-06-21	NYB	P-LEN024 added rwArrayAppend function
	2011-10-19	NYB	LHID7284 added some list functions
	2011/11/03	WAB mod to getCurrentRequestElapsedTime() so can be used if application not available
	2011-11-04 	NYB LHID8039 - created function ListContainsCount()
	2012-02-16	WAB moved all functions concerning requestTimeOut into a new request.cfc
	2012-08-20	PPB	Case 426679 reverted a change to checkReqdFunctionArgumentsExist
	2012-09-19 	WAB Modified the processLock functionality while creating CF_LOCK tag
	2012-10-31	WAB More improvements to processLock Functionality (during case 431448).  New function to get details of more than one lock at a time.  Improvments to way the metadata structure is updated (and now stored as JSON)
	2013-01-15 	WAB	Comms Improvements.  Minor change to setProcessLock() to bring back more info on locking items
	2013-01-17  P-LEX075 Added some CFC introspection functions.
	2013-10   	WAB During CASE 437315 allow locks to be updated by a third party request
	2013-11-08 	WAB During CASE 437315 mods to updateProcessLock to take account of threads
	2014-01-31 NYB Case 438543 Created function
	2014/05/13	NJH Salesforce/Connector work - added function to get the calling function. Moved listFix to this cfc from dataTransfers.cfc
	2015-03-10 	WAB Added areStringsEqual function - (for comparison with optional caseSensitivity)
	2015-04-01	WAB CASE 444268 fixed problem in deleteAllOldProcessLocks() which was deleting all locks rather than those more than x hours old
	2015-05-05  WAB Fix typo in updateProcessLock() - metdata should be metAdata
	2015-07-02	WAB Modifications to processLock code to support a variable time until locks are considered orphaned and then deleted (maximumExpectedLockMinutes)
					Added support for onErrorEmailTo parameter - cc this person if processLocks are orphaned
					Moved code to send those emails from housekeeping.cfc
	2015-09-14 	WAB added a spid column to processLock table - to help debugging
	2016-01-24 	WAB Small change to getFunctionCallingFunction.  Now returns absolute as well as relative path (which itself now works regardless of additional mappings)
2016-06-24 DCC/NAT case 449976 extension for cfc hotfix depth of 4
--->

<cfcomponent displayname="globalFunctions" hint="Common, global functions">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


2004-12-14 WAB added cfquery, cfdump and cfoutput functions
2008-09-03 NYB added FindValueCount function

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="abortIt" output="Yes" access="public">
		<CF_ABORT>
	</cffunction>

	<cffunction name="dumpIt" output="Yes" access="public">
		<cfargument name="it" type="any">
		<cfdump var="#arguments.it#">
	</cffunction>

	<cffunction name="CFQUERY" access="public" returntype="query">
		<cfargument name="SQLString" type="string" required="yes">
		<cfargument name="Datasource" type="string" required="no" default="#application.sitedatasource#">
		<cfargument name="dbType" type="string" required="no" default="">
		<cfset var RecordSet = ''>
		<CFQUERY NAME="RecordSet" Datasource="#arguments.Datasource#" dbtype="#arguments.dbType#">
				#preserveSingleQuotes(arguments.SQLString)#
			</CFQUERY>
			<cfreturn RecordSet>
		</cffunction>

		<cffunction name="CFOUTPUT" returntype="boolean" output="yes">
			<cfargument name="trace" type="string" default="nothing to output">
			<cfargument name="addLineBreak" type="boolean" default="false">
				<CFOUTPUT>#htmleditformat(arguments.trace)#<CFIF addLineBreak><BR></CFIF></CFOUTPUT>
			<cfreturn true>
		</cffunction>

		<cffunction name="CFDUMP" access="public" returntype="boolean" output="yes">
			<cfargument name="var" type="any" default="nothing to output">
				<CFDUMP Var="#arguments.var#">
			<cfreturn true>
		</cffunction>

		<!---2015/05/13 PYW Case 444667 - extend cfcookie to set httponly flag and secure flag is site is secure --->
		<cffunction name="CFCOOKIE" access="public" >
			<cfargument name="name" type="string" required="yes">
			<cfargument name="value" type="string" required="yes">
			<cfargument name="expires" type="string" required="false">

			<cfset var secureCookie=structKeyExists(request,"currentSite") and request.currentSite.isSecure?true:false>

			<!--- <cfif structKeyExists(request,"currentSite") and request.currentSite.isSecure>
				<cfset secureCookie="true">
			</cfif> --->
			<cfif structKeyExists(arguments,"expires") and arguments.expires eq "">
				<cfset structDelete(arguments,"expires")>
			</cfif>
			<cfset application.com.structureFunctions.removeUndefinedStructKeys(data=arguments)>

			<cfcookie attributeCollection="#arguments#" httpOnly="true" secure="#secureCookie#">

			<cfreturn >
		</cffunction>

		<cffunction name="getCookieExpiryDays" access="public">
			<cfargument name="isInternal" type="boolean" default="#request.relayCurrentUser.isInternal#" >

			<cfreturn application.com.settings.getSetting("security.#iif(isInternal,de('internalSites'),de('externalSites'))#.rememberUserName_days")>
		</cffunction>


		<cffunction name="getCookieExpiryDate" access="public" hint = "Gets date for a cookie to expire, rounded to a whole date ie 00:00 hrs">
			<cfargument name="isInternal" type="boolean" default="#request.relayCurrentUser.isInternal#" >

			<cfset var getCookieExpiryDays = getCookieExpiryDays(isInternal = isInternal)>

			<cfreturn application.com.datefunctions.removeTimeFromDate(dateadd("d",getCookieExpiryDays,now()))>
		</cffunction>



		<cffunction name="ISFloat" access="public">
			<!---
			this can be tested as follows:
			isFloat(3.2) = #application.com.globalFunctions.isFloat(3.2)#<br>
			isFloat(3) = #application.com.globalFunctions.isFloat(3)#<br>
			isFloat(3.2apple) = #application.com.globalFunctions.isFloat("3.2apple")#<br>
			 --->
			<cfargument name="eInt" type="string" required="yes">
			<cfreturn (find(".",eInt) gt 0) and isNumeric(eInt)>
		</cffunction>

		<cffunction name="CreateDirectoryRecursive" access="public" returntype="string" hint="Pass a path the function will create the fullpath if it does not exist">
			<cfargument name="FullPath" type="string" required="yes">

			<cfset var drive = "">
			<cfset var pathWithoutDrive = FullPath> <!--- as a default justin case regexp breaks down --->
			<cfset var PathToCreate = ''>
			<cfset var directory = "">
			<!--- discovered that did not work with UNC Paths!
				so need to remove the 'drive' and then start looping
			 --->
			<cfset var regExp = "(\\\\.*?\\.*?[\\\/]|.:[\\\/])(.*)">
			<cfset var groups = {1="drive",2="pathWithoutDrive"}>
			<cfset var splitPath = application.com.regexp.refindSingleOccurrence(regExp,arguments.fullpath,groups)>

			<cfif arrayLen(splitPath)>
				<cfset pathToCreate = splitPath[1].drive>
				<cfset pathWithoutDrive = splitPath[1].pathWithoutDrive>
			</cfif>

			<cfloop index="directory" list="#pathWithoutDrive#" delimiters="\/">
				<cfset PathToCreate = '#PathToCreate##directory#\'>
				<cfif not directoryexists(PathToCreate)>
					<cfdirectory action="CREATE" directory="#PathToCreate#">
				</cfif>
			</cfloop>
			<cfreturn FullPath>
		</cffunction>

		<!--- SSS 2009/07/08 P-FNL069 changed this function so that it would work better at accepting arguments
			WAB 2010/03/25 I note that this is a case sensitive remove duplicates (which is what i happened to want, but may not always be the case.  if anyone needs a non case sensitive version, make sure it is parameterised)
		--->
		<cffunction name="removeDuplicates" access="public">
			<cfargument name="data" required="yes">
			<cfargument name="type" default="list">
			<cfargument name="delimiter" default=",">

			<cfset var newData = "">
			<cfset var i = "">
			<cfset var intFound = 0>

			<cfif arguments.type eq "list">
				<cfset newData = "">
				<cfloop index="i" list="#arguments.data#">
					<cfset intFound = ListFind(newData, i,arguments.delimiter)>
					<cfif intFound eq 0>
						<cfset newData = ListAppend(newData,i,arguments.delimiter)>
					</cfif>
				</cfloop>

				<!--- WAB 2009/03/04 this might be a quicker way - depends whether some sort of order needs to be preserved
				<cfset var lastValue = "">
				<cfset var data = "">

				<cfset data = listsort(data)>
				<cfset lastValue = "">
				<cfloop index="i" list="#data#">
					<cfif lastValue is not I>
						<cfset newData = ListAppend(newData,i,delimiter)>
					</cfif>
				</cfloop>
				--->

			</cfif>

			<cfreturn newData>
		</cffunction>

		<cffunction name="FindValueCount" access="public" returntype="string" hint="Find number of occurences in a string, from predefined start point that defaults to 0">
			<cfargument name="SearchIn" type="string" required="yes">
			<cfargument name="SearchFor" type="string" required="yes">
			<cfargument name="SearchFrom" type="numeric" required="no" default="0">

			<cfset var FoundVar = 0>
			<cfset var i = "">

			<cfloop index= "i" from="#arguments.SearchFrom#" to="#len(SearchIn)#">
				<cfset arguments.SearchFrom = Find(SearchFor, SearchIn, SearchFrom + 1)>
				<cfif arguments.SearchFrom eq 0>
					<cfbreak>
				<cfelse>
					<cfset FoundVar = FoundVar + 1>
				</cfif>
			</cfloop>
			<cfreturn FoundVar>
		</cffunction>

		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		      Accepts a string (or just 1) number and outputs them in a requested format
		      2009-03-09 NYB Sophos Stargate 2
		 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction name="roundNumbers" access="public" output="no" hint="Accepts a list of numbers.  Returns a list of numbers formatting to appropriate strings">
			<cfargument name="numbers" type="string" default="">
			<cfargument name="decPlaces" type="numeric" default = "2">  <!--- number decimal places number displays as --->
			<cfargument name="roundToMin" type="boolean" required="no" default="true"> <!--- takes off all trailing 0, regardless of decPlaces.  If false shows all number to decPlaces  --->
			<cfargument name="rounding" type="string" required="no" default="bestFit">  <!--- accepts:  up, down, bestFit --->
			<cfargument name="delimiter" type="string" required="no" default=",">  <!--- for separating thousand --->

			<cfset var stringList = "">
			<cfset var x= "">
			<cfset var i= "">

			<cfloop index="i" list="#numbers#">
				<cfif isNumeric(i)>
					<!--- START: Round Up or Down --->
					<cfif Find(".",i) gt 0 and (len(i)-(find(".",i))) gt decPlaces and rounding neq "bestFit">
						<cfif rounding eq "up">
							<cfset i = "#left(i,find(".",i)+decPlaces)#9">
						<cfelseif rounding eq "down">
							<cfset i = "#left(i,find(".",i)+decPlaces)#0">
						</cfif>
					</cfif>
					<!--- END: Round Up or Down --->
					<cfset x = Replace(NumberFormat(i, "#delimiter#___.#RepeatString(9,decPlaces)#")," ","","all")>
					<!--- START: Remove trailing 0's --->
					<cfif roundToMin>
						<cfset x = rtrim(replace(x,"0"," ","all"))>
						<cfset x = replace(x," ","0","all")>
						<cfif right(x,1) eq ".">
							<cfset x = left(x,len(x)-1)>
						</cfif>
					</cfif>
					<!--- END: Remove trailing 0's --->
					<cfset stringList = ListAppend(stringList,x)>
				<cfelse>
					<cfset stringList = ListAppend(stringList,"** Invalid Entry **")>
				</cfif>
			</cfloop>
			<cfreturn stringList>
		</cffunction>



		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		      Returns an array of arguments a function accepts
		      2010-10-16 NYB SFDC
		 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction name="getFunctionArguments" access="public" output="no" hint="Accepts functionPath, eg application.com.flag.doesFlagExist, and returns an array of arguments it accepts">
			<cfargument name="functionPath" type="string" required="true">

			<cfset var argumentArray = ArrayNew(1)>

			<cfif isDefined(functionPath)>
				<cfset argumentArray = GetMetaData(evaluate(functionPath)).parameters>
			</cfif>

			<cfreturn argumentArray>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		      Returns a struct of required arguments a function accepts
		      2010-10-16 NYB SFDC
		 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction name="getReqdFunctionArguments" access="public" output="yes" hint="Accepts functionPath, eg application.com.flag.doesFlagExist, and returns a struct of required arguments it accepts">
			<cfargument name="functionPath" type="string" required="true">

			<cfset var argumentStruct = StructNew()>
			<cfset var argArray = getFunctionArguments(functionPath=functionPath)>
			<cfset var item = "">

			<cfloop index="item" array="#argArray#">
				<cfif structKeyExists(item,"REQUIRED") and StructFind(item, "REQUIRED")>
					<cfset StructInsert(argumentStruct,item.Name, item.Type)>
				</cfif>
			</cfloop>

			<cfreturn argumentStruct>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		      Returns a struct of required arguments missing from a function call
		      2010-10-16 NYB SFDC
		 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction name="checkReqdFunctionArgumentsExist" access="public" output="no" hint="Accepts functionPath, eg application.com.flag.doesFlagExist, and a struct of arguments and returns required arguments missing" >
			<cfargument name="functionPath" type="string" required="true">
			<cfargument name="argumentStruct" type="struct" required="true">

			<!--- 2012-08-20 PPB Case 426679 reintroduced varArgStruct so that we return an empty structure if all is ok; it should be a different struct from argStruct --->

			<cfset var varArgStruct = StructNew()>
			<cfset var key = "">

			<cfset var argStruct = getReqdFunctionArguments(functionPath=functionPath)>

			<cfloop collection = "#argStruct#" item="key">
				<cfif ArrayLen(StructFindKey(argumentStruct, key)) eq 0>
					<cfset StructInsert(varArgStruct,key, StructFind(argStruct, key))>
				</cfif>
			</cfloop>

			<cfreturn varArgStruct>
		</cffunction>


		<!---
			WAB 2013-01-17  P-LEX075, but generally applicable
			Functions to support CFC introspection.
			Particularly for getting all methods with names which match a regExp
			And optionally ordering them based on the value of an attribute

			I have function similar to this in all sorts of places, so thought that it was time to create a single one
		--->
		<cffunction name="getArrayOfCFCMethods" hint="Returns an array of metadata of all methods in a cfc. Can match name to a regExp and order by an attribute. Deals with functions in extended cfcs" output="false">
			<cfargument name="cfcObject">
			<cfargument name="nameRegExp" default="">
			<cfargument name="IndexAttribute" default="">
			<cfargument name="IndexDefaultValue" default="">

			<cfset var methodMetaDataStructure = getCFCMethodMetaDataStructure(cfcObject = cfcObject,nameRegExp = nameRegExp)>
			<cfset var result = arraynew(1)>
			<cfset var methodName = "">

			<cfif IndexAttribute is not "">
				<cfset result = application.com.structureFunctions.orderStructureByAttribute(structure = methodMetaDataStructure,pathToSubElement = IndexAttribute,defaultValue = indexDefaultValue)>
			<cfelse>
				<cfloop collection="#methodMetaDataStructure#" item="methodname">
					<cfset arrayAppend(result,methodMetaDataStructure[methodname])>
				</cfloop>
			</cfif>

			<cfreturn result>

		</cffunction>


		<cffunction name="getCFCMethodMetaDataStructure" output="false">
			<cfargument name="cfcObject">
			<cfargument name="nameRegExp" default = "">

			<cfset var objectMetaData  = getMetaData(cfcObject)>
			<cfset var result = {}>
			<cfset var arrayOfFunctionArrays = arrayNew(1) />
			<cfset var functionArray = '' />
			<cfset var function = '' />

			<!--- NJH 2013/07/25 - modified this a bit as there was a case in Lenovo where a relayopportunity.cfc stub was created in code files to deal with a merge problem
				Anyways, because this was a stub, there was no functions structure. And because opportunity.cfc in code files extended it, the structure looked something like this...
					extends
						extends
							functions
						functions
			--->
			<cfif structKeyExists(objectMetaData,"functions")>
				<cfset arrayOfFunctionArrays = [objectMetaData.functions]>
			</cfif>
			<cfif structKeyExists (objectMetaData,"extends")>
				<cfif structKeyExists (objectMetaData.extends,"functions")>
					<cfset arrayPrepend(arrayOfFunctionArrays,objectMetaData.extends.functions)>
				</cfif>
<!--- START DCC/NAT 2016-06-24 case 449976 hotfix depth of 4 --->
				<cfif structKeyExists(objectMetaData.extends,"extends")>
					<cfif structKeyExists (objectMetaData.extends.extends,"functions")>
					<cfset arrayPrepend(arrayOfFunctionArrays,objectMetaData.extends.extends.functions)>
				</cfif>

				<cfif structKeyExists(objectMetaData.extends.extends,"extends") and structKeyExists (objectMetaData.extends.extends.extends,"functions")>
					<cfset arrayPrepend(arrayOfFunctionArrays,objectMetaData.extends.extends.extends.functions)>
				</cfif>
			</cfif>
<!--- END DCC/NAT 2016-06-24 case 449976 hotfix depth of 4 --->
			</cfif>

			<cfloop array="#arrayOfFunctionArrays#" index="functionArray">
				<cfloop array="#functionArray#" index="function">
					<cfif nameRegExp is "" OR refindnocase(nameRegExp,function.name)>
						<cfset result[function.name] = function>
					</cfif>
				</cfloop>
			</cfloop>

			<cfreturn result>

		</cffunction>


		<cffunction name="removeURLToken" access="public" output="no" hint="Strip out a value pair assignment from a string" returntype="string">
			<!--- PPB 2010/08/02 useful to remove a URL parameter/value pair from a URL query_ string
			WAB there is also a similar function com.regExp.removeItemFromNameValuePairString()
			--->
			<cfargument name="queryString" type="string" required="true">
			<cfargument name="tokenName" type="string" required="true">							<!--- eg if you want to strip out the message parameter from...&message=hello&foo=bar... send tokenName="message" --->

			<cfset var returnString = "">
			<cfset var posOfToken= FindNoCase("#tokenName#=",queryString)>
			<cfset var posOfNextAmpersand = "">

			<cfif posOfToken eq 0>
				<cfset returnString = queryString>
			<cfelse>
				<cfset posOfNextAmpersand = FindNoCase("&",queryString & "&",posOfToken)>		<!--- tack a "&" to the end so we always find one --->
				<cfif posOfNextAmpersand gt len(queryString)>									<!--- implies we found the "&" we just added --->
					<cfset returnString = left(queryString,posOfToken-2)>
				<cfelse>
					<cfset returnString = left(queryString,posOfToken-2) & right(queryString, len(queryString)-posOfNextAmpersand+1)>
	 			</cfif>
			</cfif>

			<cfreturn returnString>
		</cffunction>

		<!--- NJH 2010/05/24 LID 3286: Sony relayWebServices Error
			WAB: As it happens there is a coldfusion function called XML format which will do this
		 --->
		<cffunction name="ISOEncode" access="public" output=false>  <!--- WAB 2009/07/08 added output = false, was putting spaces in code --->
			<cfargument name="string" required="yes">
			<cfscript>
			var ISOCode = "";
			var  ISOTable = {};
			var result = string;

			 StructInsert(ISOTable, """", "&quot;");
			 StructInsert(ISOTable, "'", "&apos;");
			 StructInsert(ISOTable, "&", "&amp;");
			 StructInsert(ISOTable, "<", "&lt;");
			 StructInsert(ISOTable, ">", "&gt;");
			</cfscript>
			<cfloop collection = "#ISOTable#" item = "ISOCode">
				<cfset result = Replace(result, ISOCode, StructFind(ISOTable,ISOCode),"all")>
			</cfloop>
			<cfreturn result>
		</cffunction>



		<!---
		WAB 2009/04/16 function to clear the content of CF_HTMLHead.
		Useful when doing things which require XML output, or XLS files via cfcontent
		Came from See http://www.bennadel.com/index.cfm?dax=blog:758.view#comments_4523

		 --->

		<!--- 2011/05/25 NYB REL106 added ListRemoveUnfound function --->
		<cffunction name="ListRemoveUnfound" access="public" returntype="string" hint="Removes all elements from list1 that don't exist in list2. Returns list1." output="false">
			<cfargument name="list1" required="yes">
			<cfargument name="list2" required="yes">

			<cfset var x = "">
			<cfset var i = "">
			<cfset var f = "">

			<cfloop index="i" to="1" from="#ListLen(list1)#" step=-1>
				<cfset x = ListGetAt(list1, i)>
				<cfset f = ListFindNoCase(list2, x)>
				<cfif f eq 0>
					<cfset arguments.list1 = ListDeleteAt(list1, i)>
				</cfif>
			</cfloop>
			<cfreturn arguments.list1>
		</cffunction>

		<!--- 2011/05/25 NYB REL106 added ListRemoveUnfound function --->
		<cffunction name="ListMinusList" access="public" returntype="string" hint="Removes all elements from list1 that exist in list2. Returns list1." output="false">
			<cfargument name="list1" required="yes">
			<cfargument name="list2" required="yes">

			<cfset var i = "">
			<cfset var f = "">

			<cfloop index="i" list="#arguments.list2#">
				<cfset f = ListFindNoCase(arguments.list1, i)>
				<cfif f neq 0>
					<cfloop condition="f neq 0">
						<cfset arguments.list1 = ListDeleteAt(arguments.list1, f)>
						<cfset f = ListFindNoCase(arguments.list1, i)>
					</cfloop>
				</cfif>
			</cfloop>
			<cfreturn arguments.list1>
		</cffunction>

		<!--- 2011-10-19 NYB LHID7284.  Created ListRemoveDuplicates function --->
		<cffunction name="RemoveListDuplicates" access="public" returntype="string" hint="Removes all duplicate elements from a list.  Returns amended list." output="false">
			<cfargument name="list" required="yes">
			<cfargument name="delimiter" default=",">

			<cfset var newList = "">
			<cfset var x = "">
			<cfset var i = "">
			<cfset var f = "">

			<cfloop index="i" from="1" to="#ListLen(arguments.list)#">
				<cfset x = ListGetAt(arguments.list, i)>
				<cfset f = ListFindNoCase(newList,x)>
				<cfif f eq 0>
					<cfset newList = ListAppend(newList, x, arguments.delimiter)>
				</cfif>
			</cfloop>

			<cfreturn newList>
		</cffunction>

		<!--- 2011-10-19 NYB LHID7284.  Created function --->
		<cffunction name="TestListContentsType" access="public" returntype="string" hint="Return whether or not all values in a list are of type specified.  Accepts values any of the ColdfusionDecision functions minus the Is" output="false">
			<cfargument name="list" required="yes">
			<cfargument name="datatype" required="yes">
			<cfargument name="delimiter" default=",">

			<cfset var result = "false">
			<cfset var x = "">
			<cfset var i = "">

			<cfif ListLen(arguments.list,arguments.delimiter) gt 0>
				<cfset result = "true">
				<cfloop index="i" list="#arguments.list#" delimiters="#delimiter#">
					<cfset x = evaluate("Is#arguments.datatype#(i)")>
					<cfif not x>
						<cfset result = "false">
						<cfbreak>
					</cfif>
				</cfloop>
			</cfif>

			<cfreturn result>
		</cffunction>


		<!--- 2011-10-19 NYB LHID7284.  Created function --->
		<cffunction name="RemoveUnwantedTypeFromList" access="public" returntype="string" hint="Removes contents from list not matching datatype requested." output="false">
			<cfargument name="list" required="yes">
			<cfargument name="datatype" required="yes">
			<cfargument name="delimiter" default=",">

			<cfset var newList = "">
			<cfset var x = "">
			<cfset var i = "">

			<cfloop index="i" list="#arguments.list#">
				<cfset x = evaluate("Is#arguments.datatype#(i)")>
				<cfif x>
					<cfset newList = ListAppend(newList, i, arguments.delimiter)>
				</cfif>
			</cfloop>

			<cfreturn newList>
		</cffunction>


	<cffunction name="removeEmptyListElements" output="false">
		<cfargument name="list" type="string">
		<cfargument name="delimiter" default="," type="string">

		<cfreturn ListMinusList(listFix(argumentCollection=arguments,nullValue="**NoListElement**"),"**NoListElement**")>
	</cffunction>


	<!--- NJH 2008/04/11 added for CR-LEN507 --->
	<cffunction name="listFix" access="public" returntype="string" output="false" hint="Replaces empty list elements with a specified value">
		<cfargument name="list" type="string">
		<cfargument name="delimiter" default="," type="string">
		<cfargument name="nullValue" default="NULL" type="string" hint="The value to place in the empty list element">

		<cfscript>
		/**
		 * Fixes a list by replacing null entries.
		 * This is a modified version of the ListFix UDF
		 * written by Raymond Camden. It is significantly
		 * faster when parsing larger strings with nulls.
		 * Version 2 was by Patrick McElhaney (&#112;&#109;&#99;&#101;&#108;&#104;&#97;&#110;&#101;&#121;&#64;&#97;&#109;&#99;&#105;&#116;&#121;&#46;&#99;&#111;&#109;)
		 *
		 * @param list 	 The list to parse. (Required)
		 * @param delimiter 	 The delimiter to use. Defaults to a comma. (Optional)
		 * @param null 	 Null string to insert. Defaults to "NULL". (Optional)
		 * @return Returns a list.
		 * @author Steven Van Gemert (&#112;&#109;&#99;&#101;&#108;&#104;&#97;&#110;&#101;&#121;&#64;&#97;&#109;&#99;&#105;&#116;&#121;&#46;&#99;&#111;&#109;&#115;&#118;&#103;&#50;&#64;&#112;&#108;&#97;&#99;&#115;&#46;&#110;&#101;&#116;)
		 * @version 3, July 31, 2004
		 */
		//function listFix(list) {
		if (arguments.list neq "") {
			var delim = arguments.delimiter;
			var null = arguments.nullValue;
			var special_char_list      = "\,+,*,?,.,[,],^,$,(,),{,},|,-";
			var esc_special_char_list  = "\\,\+,\*,\?,\.,\[,\],\^,\$,\(,\),\{,\},\|,\-";
			var i = "";

			if(arrayLen(arguments) gt 1) delim = arguments[2];
			if(arrayLen(arguments) gt 2) null = arguments[3];


			if(findnocase(left(arguments.list, 1),delim)) arguments.list = null & arguments.list;
			if(findnocase(right(arguments.list,1),delim)) arguments.list = arguments.list & null;

			i = len(delim) - 1;
			while(i GTE 1){
				delim = mid(delim,1,i) & "_Separator_" & mid(delim,i+1,len(delim) - (i));
				i = i - 1;
			}

			delim = ReplaceList(delim, special_char_list, esc_special_char_list);
			delim = Replace(delim, "_Separator_", "|", "ALL");

			arguments.list = rereplace(arguments.list, "(" & delim & ")(" & delim & ")", "\1" & null & "\2", "ALL");
			arguments.list = rereplace(arguments.list, "(" & delim & ")(" & delim & ")", "\1" & null & "\2", "ALL");
		}

		return arguments.list;
		//}
		</cfscript>

	</cffunction>

		<!--- 2011-11-04 NYB LHID8039.  Created function --->
		<cffunction name="ListContainsCount" access="public" returntype="string" output="false"
					hint="Counts instances that contain a specified value in a list. The search can be case sensitive or insensitive by setting NoCase=true/false, defaults to true, ie, case insensitive">
			<cfargument name="list" required="yes">
			<cfargument name="substring" required="yes">
			<cfargument name="delimiter" default=",">
			<cfargument name="NoCase" default="true"><!--- defaults to case insensitive --->

			<cfset var foundResult = 0>
			<cfset var result = 0>
			<cfset var i = "">

			<cfloop index="i" list="#list#" delimiters="#delimiter#">
				<cfif NoCase>
					<cfset foundResult = FindNoCase(substring, i)>
				<cfelse>
					<cfset foundResult = Find(substring, i)>
				</cfif>
				<cfif foundResult gt 0>
					<cfset result = result+1>
				</cfif>
			</cfloop>

			<cfreturn result>
		</cffunction>

		<!--- WAB 2013-07-02 Added (for use initially in relayTags.cfc)
			ListFindNoCase with an added twist - what you are looking for can be a list
		 --->
		<cffunction name="ListFindNoCaseWithList" access="public" returntype="string" hint="Same as ListFindNoCase, but what you are looking for can be a list as well, returns position of first match" output="false">
			<cfargument name="list" required="yes">
			<cfargument name="itemsToFind" required="yes">
			<cfargument name="delimiter" default=",">

			<cfset var foundResult = 0>
			<cfset var i = "">

			<cfloop index="i" list="#itemsToFind#" delimiters="#delimiter#">
				<cfset foundResult = listFindNoCase(list,i,delimiter)>
				<cfif foundResult is not 0>
					<cfbreak>
				</cfif>
			</cfloop>

			<cfreturn foundResult>
		</cffunction>


	<!--- WAB 2013-12-03 for 2013RoadMap2 Item 25 ContactHistory --->
	<cffunction name="listInsertAfter" output="false">
		<cfargument name="list" required= "true">
		<cfargument name="value" required= "true">
		<cfargument name="after" required= "true">
		<cfargument name="delimiter" default= ",">

		<cfset var find = listFindNoCase(list,after)>
		<cfset var result = list>
		<cfif find is not 0>
			<cfset result = listInsertAt(list,find+1,value,delimiter)>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="listInsertBefore" output="false">
		<cfargument name="list" required= "true">
		<cfargument name="value" required= "true">
		<cfargument name="before" required= "true">
		<cfargument name="delimiter" default= ",">

		<cfset var find = listFindNoCase(list,before)>
		<cfset var result = list>
		<cfif find is not 0 >
			<cfset result = listInsertAt(list,find,value,delimiter)>
		</cfif>

		<cfreturn result>

	</cffunction>




		<!--- WAB 2013-10-02  For CASE 437290--->
		<cffunction name="listAllButLast" output="false">
			<cfargument name="list" required= "true">
			<cfargument name="delimiter" default= ",">
			<cfreturn  reverse(listRest(reverse(list),delimiter))>
		</cffunction>


		<cfscript>
		function resetCFHtmlHead() {
			var out = getPageContext().getOut();
			var method = out.getClass().getDeclaredMethod("initHeaderBuffer",arrayNew(1));
			method.setAccessible(true);
			method.invoke(out,arrayNew(1));
		}

		</cfscript>

	<!---
	WAB 2012-09-19 Modified the processLock functionality while creating CF_LOCK tag
					Implemented support for timeout
					Function to clear up orphaned locks
					Function to update and add metadata information to a lock
					Function to clear up any stray locks at end of request and after an error
	WAB	2012-10-30	Added getMatchingProcessLocks()
					Store metadata as JSON.  Improve way metadata can be updated
	WAB 2013-11-08	Handle ProcessLocks within Threads better, having discovered the existence of the Thread variable when in a thread
	--->
		<cffunction name="inthread" access="public" output="false">
			<cfreturn isDefined("Thread")>
		</cffunction>

		<cffunction name="getThreadOrRequestScope" access="public" output="false">
			<cfif inThread()>
				<cfreturn Thread>
			<cfelse>
				<cfreturn request>
			</cfif>
		</cffunction>


		<!---
			2015-07-02	WAB Add support for onErrorEmailTo and maximumExpectedLockMinutes (note needs db change)
			2015-09-14 	WAB added a spid column - to help debugging
		--->
		<cffunction name="setProcessLock" access="public" output="false">
			<cfargument name="lockName" type="string" required="true">
			<cfargument name="exclusiveWith" type="string" default=""> <!--- list of other locks which will prevent this lock being set, supports wildcard --->
			<cfargument name="timeout" type="numeric" default="0">
			<cfargument name="metadata"  default="#structNew()#"> <!--- structure --->
			<cfargument name="onErrorEmailTo" type="string" default="">
			<cfargument name="maximumExpectedLockMinutes" type="numeric" default="120">

			<cfset var setLockResult = "">
			<cfset var result = {timedOut = false}>
			<cfset var startTime = getTickCount()>
			<cfset var continue = true>
			<cfset var nowTime = startTime>
			<cfset var item = "">
			<cfset var ThreadOrRequest = getThreadOrRequestScope()>

			<cfparam name="ThreadOrRequest.openLocks"  default= "">

			<cfif arguments.onErrorEmailTo is not "">
				<cfset arguments.metadata["onErrorEmailTo"] = arguments.onErrorEmailTo >
			</cfif>

			<cfloop condition="continue">
				<cfset nowTime = getTickCount()>
				<cfquery datasource="#application.siteDataSource#" result="setLockresult">
					if not exists (select 1 from processLock where lockName =  <cfqueryparam value="#arguments.lockName#" CFSQLTYPE="CF_SQL_VARCHAR" > <cfloop list="#exclusiveWith#" index="item">OR lockname like <cfqueryparam value="#item#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfloop> )
						insert into processLock (lockName,lockDate,metadata,instanceID,maximumExpectedLockMinutes,created,createdBy,spid)
						values (<cf_queryparam value="#arguments.lockName#" CFSQLTYPE="CF_SQL_VARCHAR" >,GetDate(),<cf_queryparam value="#encodeProcessLockMetadata(metadata)#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#application.instance.coldFusionInstanceID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#maximumExpectedLockMinutes#" CFSQLTYPE="CF_SQL_INTEGER" >, GetDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,@@spid)
				</cfquery>

				<cfset result.lockWait = ((nowTime - startTime)/1000)>
				<cfif setLockresult.recordCount is not 0>
					<cfbreak>
				<cfelseif result.lockWait LT timeout >
					<cfset sleep(100)>
				<cfelse>
					<cfbreak>
				</cfif>

			</cfloop>

			<!--- if lock not set then return details of who and when locked --->
			<cfif setLockresult.recordCount is 0>
				<cfset result.timedOut = true>
				<!--- WAB 2013-01-15 Comms Improvements, return full details of locking items (including metadata structure) --->
				<cfset result.lockDetail = getMatchingProcessLocks (listappend(arguments.lockName,exclusiveWith))>
			<cfelse>
				<cfset ThreadOrRequest.openLocks = listAppend(ThreadOrRequest.openLocks,arguments.lockName)>
			</cfif>

			<cfreturn result>

		</cffunction>


		<cffunction name="doesLockExist" access="public" type="boolean" output="false">
			<cfargument name="lockName" type="string" required="true">

			<cfreturn getProcessLock(lockName=arguments.lockName).lockExists>
		</cffunction>


		<cffunction name="getProcessLock" access="public" returntype="struct" output="false">
			<cfargument name="lockName" type="string" required="true">

			<cfset var getLock = "">
			<cfset var result = {}>

			<cfquery name="getLock" datasource="#application.siteDataSource#" debug=false>
				select ID,lockName,lockDate,instanceID,created,createdBy,lastUpdated,metadata
				from processLock where lockName =  <cfqueryparam value="#arguments.lockName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset result = application.com.structureFunctions.queryrowToStruct(query=getLock,row=1)>
			<cfset result.lockExists = getLock.recordCount>
			<cfset result.metadata = decodeProcessLockMetadata(result.metadata)>

			<cfreturn result>
		</cffunction>

		<cffunction name="getMatchingProcessLocks" access="public" returntype="array" output="false">
			<cfargument name="lockNames" type="string" required="true"> <!--- list of locknames, can use % wildcard --->

			<cfset var getLocks = "">
			<cfset var lock = "">
			<cfset var lockname = "">
			<cfset var result = "">

			<cfquery name="getLocks" datasource="#application.siteDataSource#">
				select ID,lockName,lockDate,instanceID,created,createdBy,lastUpdated,metadata
				from processLock where 1=0
				<cfloop list="#lockNames#" index="lockName">
					OR lockName like  <cfqueryparam value="#lockName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfloop>
			</cfquery>

			<cfset result = application.com.structureFunctions.QueryToArrayOfStructures(query=getLocks)>
			<cfloop array="#result#" index="lock">
				<cfset lock.metadata = decodeProcessLockMetadata(lock.metadata)>
			</cfloop>


			<cfreturn result>
		</cffunction>

		<cffunction name="encodeProcessLockMetadata" access="private" returntype="string" output="false">
			<cfargument name="metadata"  required=true type="struct">

			<cfset var result = "">

			<cfif StructCount(metadata)>
				<cfset result = serializeJSON(metadata)>
			</cfif>

			<cfreturn result>
		</cffunction>

		<cffunction name="decodeProcessLockMetadata" access="private" returntype="any" output="false">
			<cfargument name="metadataString"  required=true>

			<cfset var result = {}>

			<cfif isJSON(metadataString)>
					<cfset result = deserializeJSON(metadataString)>
			</cfif>

			<cfreturn result>
		</cffunction>

		<!--- 2013-02-12	WAB Sprint 15&42 Comms.  Updated to return information on current lock
			WAB 2013-10  During CASE 437315, allow a different request to update the lock metaData
						This allowed for requestB to ask RequestA to cancel a process it was running
						RequestB added a CANCEL key to the metadata, requestA was checking for the CANCEL key every x iterations of a loop.
			2015/05/05 WAB Fixed parameter name typo. Argument was named metdata, but the code was expecting (the correctly spelt) metAdata
			2015-09-14 	WAB added update of spid column
		--->
		<cffunction name="updateProcessLock" access="public" returntype="struct" output="false">
			<cfargument name="lockName" type="string" required="true">
			<cfargument name="metadata" type="struct"> <!--- structure.  Structure is appended to the current metadata structure --->
			<cfargument name="ThirdPartyUpdate" type="boolean" default="false">


			<cfset var result = {}>
			<cfset var newMetadata = "">
			<cfset var currentLock = "">
			<cfset var ThreadOrRequest = getThreadOrRequestScope()>

			<cfparam name="ThreadOrRequest.openLocks"  default= "">

			<cfif ThirdPartyUpdate OR listfindnocase(ThreadOrRequest.openLocks,arguments.lockName) >

				<cfset currentLock = getProcessLock(lockname)>

				<!--- If metadata  passed in then it is appended to the current metadata --->
				<cfif structKeyExists(arguments,"metadata") >
					<cfif isStruct(currentLock.metadata)>
						<cfset structAppend(currentLock.metadata,arguments.metadata)>
						<cfset newMetaData = currentLock.metadata>
					<cfelse>
						<cfset newmetadata = metadata>
					</cfif>

				</cfif>

				<cfquery  datasource="#application.siteDataSource#" debug=false>
					update processLock
					set
						<cfif structKeyExists(arguments,"metadata")>metadata = <cfqueryparam value="#encodeProcessLockMetaData(newmetadata)#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
						spid = @@spid,
						lastUpdated = <cfif not ThirdPartyUpdate>getdate()<cfelse>lastupdated</cfif>  <!--- don't update the lastupdated when done by a third party, because this date is used to tell whether the process has broken down --->
					where lockName =  <cfqueryparam value="#arguments.lockName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfset result = currentLock>
			</cfif>

			<cfreturn result>
		</cffunction>

		<cffunction name="deleteProcessLock" access="public" output="false">
			<cfargument name="lockName" type="string" required="true">

			<cfset var deleteLock = "">
			<cfset var ThreadOrRequest = getThreadOrRequestScope()>

			<cfif structKeyExists(ThreadOrRequest,"openLocks") and listfindnocase(ThreadOrRequest.openLocks,arguments.lockName)>

			<cfquery name="deleteLock" datasource="#application.siteDataSource#">
				delete from processLock where lockName =  <cfqueryparam value="#arguments.lockName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

				<cfset listdeleteAt(ThreadOrRequest.openLocks,listfindnocase(ThreadOrRequest.openLocks,arguments.lockName))>

			</cfif>
		</cffunction>



		<cffunction name="deleteThisRequestsProcessLocks" access="public" output="false">

			<cfset var deleteLock = "">
			<cfset var ThreadOrRequest = getThreadOrRequestScope()>

			<cfif structKeyExists(ThreadOrRequest,"openLocks") and listLen(ThreadOrRequest.openLocks)>

				<cfquery name="deleteLock" datasource="#application.siteDataSource#">
					delete from processLock where lockName in   (<cfqueryparam value="#ThreadOrRequest.openLocks#" CFSQLTYPE="CF_SQL_VARCHAR" list=true>)
				</cfquery>

				<cfset structDelete(ThreadOrRequest,"openLocks")>

			</cfif>
		</cffunction>


		<cffunction name="deleteAllOldProcessLocks" access="public" output="false">
			<cfargument name="applicationScope" default="#application#">

			<cfset var itemsDeleted = "">
			<cfset var result = {isOK = true}>

			<!---
				2015-04-01 WAB CASE 444268 discovered that the dateAdd was the wrong way round, needed a minue sign
				At same time reduced the amount of time before a lock is deleted (to 2 hours).
				Very long running requests should update the lock periodically
				Function now returns the locks which have been deleted (so that we can do some debugging)
				2015-07-02	WAB Added support for maximumExpectedLockMinutes being stored on each record
			--->

			<cfquery name = "itemsDeleted" datasource="#applicationScope.siteDataSource#">
			select * from processLock where dateadd(n,maximumExpectedLockMinutes,lastupdated) < getDate()
			delete from processLock where dateadd(n,maximumExpectedLockMinutes,lastupdated) < getDate()
			</cfquery>

			<cfset result.numberDeleted = itemsDeleted.recordCount>

			<cfif result.numberDeleted gt 0>
				<cfset result.itemsDeleted = itemsDeleted>
			</cfif>

			<cfreturn result>
		</cffunction>

		<!---
			2015-07-02	WAB Moved this function from housekeeping and added support for onErrorEmailTo
		--->
		<cffunction name="deleteOldProcessLocksAndSendWarnings" >
			<cfargument name="applicationScope" default="#application#">

			<cfset var result = applicationScope.com.globalFunctions.deleteAllOldProcessLocks(applicationScope=applicationScope)>

			<cfif result.numberDeleted is not 0 >
				<cfloop query="result.itemsDeleted">
					<cfset var ccAddress = "">
					<cfset var metadataStruct = decodeProcessLockMetadata(metadata)>
					<cfif structKeyExists (metadataStruct,"onErrorEmailTo")>
						<cfset ccAddress = metadataStruct.onErrorEmailTo>
					</cfif>

					<cfmail from = "serverSupport@relayware.com" to= "serverSupport@relayware.com" cc="#ccAddress#" subject="#cgi.server_name#. Warning: Process Lock #LockName# Deleted " type="HTML">
						The following lock has deleted because it is more than #maximumExpectedLockMinutes# minutes old.<br />
						Please investigate.  <br />
						If the process is really taking this long, it should use the updateProcessLock function to update the lock periodically
						<cfdump var="#result.itemsDeleted#">
					</cfmail>

				</cfloop>
			</cfif>

			<cfreturn result>
		</cffunction>


		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				SplitStringintoArray
		 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
		<cffunction access="public" name="SplitStringintoArray" returnType="array" output="false"
			hint="Takes a string and creates an array with each item shorter than the ma length specified">
			<!---  mainly used for splitting the feedback string for commdetail--->
			<cfargument name="theString" type="string" required="true">
			<cfargument name="maxLength" type="numeric" required="true">


				<CFSET var theArray = arrayNew (1)>
				<CFSET var I = 1>
				<CFSET var lastspace = "">

				<CFLOOP condition = "len(arguments.theString) IS NOT  0 OR I is 1">
					<CFIF len(theString) GT maxLength>
						<cfset lastspace =   maxLength - findNoCase(" ",reverse(left (theString, maxLength)))>
				 		<CFSET theArray[I] = left(theString,lastSpace)>
						<CFSET arguments.theString = mid(theString,lastSpace+1,len(theString) - lastSpace)>
					<CFELSE>
						<CFSET theArray[I] = theString>
						<CFSET arguments.theString = "">
					</cfif>

					<cfset I = I +1>
				</CFLOOP>

			<cfreturn theArray>

		</cffunction>


		<cffunction name="rwArrayAppend" access="public" hint="Joins 2 arrays - if value is an array - otherwise it just add value to the array" returntype="array" output="false">
			<cfargument name="array">
			<cfargument name="value">

			<cfset var i = "">

			<cfif isArray(value)>
				<cfloop index="i" array="#value#">
					<cfset ArrayAppend(arguments.array, i)>
				</cfloop>
			<cfelse>
				<cfset ArrayAppend(arguments.array, arguments.value)>
			</cfif>

			<cfreturn arguments.array>
		</cffunction>


		<cffunction access="public" name="rwReplaceNoCase" returnType="string" hint="Replaces a string with another, from a given start position" output="false">
			<cfargument name="string" type="string" required="true">
			<cfargument name="substring1" type="string" required="true">
			<cfargument name="substring2" type="string" required="true">
			<cfargument name="start" type="numeric" default="1">
			<cfargument name="end" type="numeric" default="0">
			<cfargument name="scope" type="string" default="one"><!--- one or all --->

			<cfset var result = StructNew()>
			<cfset var leftString = "">
			<cfset var rightString = "">

			<cfif start gt 1>
				<cfset leftString = left(arguments.string,start-1)>
				<cfset arguments.string = right(arguments.string,len(arguments.string)-(start-1))>
			</cfif>

			<cfif arguments.end neq 0>
				<cfset rightString = right(arguments.string,len(arguments.string)-(end-(start-1)))>
				<cfset arguments.string = left(arguments.string,end-(start-1))>
			</cfif>

			<cfset result = ReplaceNoCase(arguments.string, substring1, substring2, scope)>
			<cfset result = leftString&result&rightString>

			<cfreturn result>
		</cffunction>


		<!--- WAB 2011/10/10 Added these functions for dealing with memory --->
		<cfscript>
		function getMemory() {
		  javaRuntime = CreateObject("java","java.lang.Runtime").getRuntime();
		  memory = StructNew();
		  memory.freeAllocated = round(javaRuntime.freeMemory() / 1024^2);
		  memory.allocated = round(javaRuntime.totalMemory() / 1024^2);
		  memory.max = round(javaRuntime.maxMemory() / 1024^2);
		  memory.used = round(memory.allocated - memory.freeAllocated);
		  memory.freeTotal = round(memory.max - memory.allocated + memory.freeAllocated);
		  memory.percentFreeAllocated = round((memory.freeAllocated / memory.allocated) * 100);
		  memory.percentAllocated = round((memory.allocated / memory.max) * 100);
		  memory.percentUsedMax = round((memory.used / memory.max) * 100);
		  memory.percentUsedAllo = round((memory.used / memory.allocated) * 100);
		  memory.percentFreeTotal = round((memory.freeTotal / memory.max) * 100);

			return memory ;
		 }

		function runGarbageCollection () {
			var obj =createObject("java","java.lang.System");
			obj.gc();
		}

		</cfscript>

		<!--- WAB 2011/10/12 --->
		<cffunction access="public" name="isDebugOn" returnType="boolean" hint="checks whether debug is on" output="false">
			<!---
				Note that this function is fooled by <cfsetting showDebugOutput=true>
				Primary use of this function is to check that debug information is not being collected (and hogging memory) when running big processes
				Using <cfsetting showDebugOutput=true> does not prevent debug information being collected, it is just not shown.
				Therefore never run this function after a <cfsetting showDebugOutput=true>
			  --->

			<cfset var result = isdebugMode()>

			<cfif structKeyExists (URL,"_cf_nodebug") or structKeyExists (form,"_cf_nodebug") and _cf_nodebug>
				<cfset result = false>
			</cfif>

			<cfreturn result>
		</cffunction>

		<!---
			2012/02/10 PPB Case 426125
			the CF ListGetAt function doesn't count an empty string between 2 delimiters
			this function uses fn_ListFix to convert the string to change any empty strings to spaces
			NB: despite the parameter name "delimiters" (named for compatibility with ListGetAt) it only supports a single character delimiter (fn_ListFix would need changing to accept multiple delimiters)
		 --->
		<cffunction name="rwListGetAt" output="false">
			<cfargument name="list" required="true" >
			<cfargument name="position" required="true">
			<cfargument name="delimiters" required="false" default=",">

	 	<cfset var listWithoutEmptyStrings = ListFix(list=arguments.list,delimiter=arguments.delimiters,nullValue=" ")>

			<cfreturn ListGetAt(listWithoutEmptyStrings,arguments.position,arguments.delimiters)>
		</cffunction>

		<!--- NJH 2013/03/14- Case 433498  - created functions below dealing with getting and setting system users--->
		<cffunction name="getSystemPerson" returntype="query" output="false">
			<cfreturn getSysTaskPerson(firstname="System")>
		</cffunction>

		<cffunction name="getScheduledTaskPerson" returntype="query" output="false">
			<cfreturn getSysTaskPerson(firstname="Scheduled")>
		</cffunction>


		<cffunction name="getSysTaskPerson" returntype="query" output="false">
			<cfargument name="firstname" type="string" default="System">
			<cfset var getPersonQry = "">

			<!--- union del table in case the person gets deleted... should still return a personID --->
			<cfquery name="getPersonQry" datasource="#application.siteDataSource#">
				select personID,0 as userGroupID from person where firstname=<cf_queryParam value="#arguments.firstname#" cfsqltype="cf_sql_varchar"> and lastname='Task' and organisationID=1
				union
				select personID,0 as userGroupID from personDel where firstname=<cf_queryParam value="#arguments.firstname#" cfsqltype="cf_sql_varchar"> and lastname='Task' and organisationID=1
			</cfquery>

			<cfreturn getPersonQry>
		</cffunction>


		<cffunction name="setScheduledTaskAsCurrentUser" output="false">
			<cfset var scheduledTaskUser = application.com.globalFunctions.getScheduledTaskPerson()>

			<cfset request.relayCurrentUser.personID = scheduledTaskUser.personID>
			<cfset request.relayCurrentUser.userGroupID = scheduledTaskUser.userGroupID>
		</cffunction>


		<cffunction name="setSystemTaskAsCurrentUser" output="false">
			<cfset var systemTaskUser = application.com.globalFunctions.getSystemPerson()>

			<cfset request.relayCurrentUser.personID = systemTaskUser.personID>
			<cfset request.relayCurrentUser.userGroupID = systemTaskUser.userGroupID>
		</cffunction>

		<cffunction name="queryAppend" access="public" returntype="void" output="true" hint="This takes two queries and appends the second one to the first one. This actually updates the first query and does not return anything.">
		    <cfargument name="QueryOne" type="query" required="true">
		    <cfargument name="QueryTwo" type="query" required="true">

		    <cfset var LOCAL = StructNew()>
			<cfset var i = 1>

		    <cfset LOCAL.Columns = ListToArray(ARGUMENTS.QueryTwo.ColumnList)>

		    <cfloop query="ARGUMENTS.QueryTwo">
		    	<cfset QueryAddRow(ARGUMENTS.QueryOne)>
			    <cfloop index="i" from="1" to="#ArrayLen(LOCAL.Columns)#">
			    	<cfset LOCAL.ColumnName = LOCAL.Columns[i]>
			    	<cfset ARGUMENTS.QueryOne[LOCAL.ColumnName][ARGUMENTS.QueryOne.RecordCount] = ARGUMENTS.QueryTwo[LOCAL.ColumnName][ARGUMENTS.QueryTwo.CurrentRow]>
			    </cfloop>
		    </cfloop>

		    <cfreturn>

	    	</cffunction>


		<!--- 2014-01-31 NYB Case 438543 Created function --->
		<cffunction name="RandomSelectFromList" access="public" returntype="string" hint="Randomising a passed list" output="false">
			<cfargument name="list" required="yes">
			<cfargument name="size" required="no">

			<cfset var result = "">

			<cfif not structkeyexists(arguments,"size")>
				<cfset arguments.size = listlen(arguments.list)>
			</cfif>

			<CFLOOP CONDITION="ListLen(result) LT arguments.size">
				<CFSET var RandomQ = ListGetAt(list, RandRange(1, listlen(list)))>
				<CFSET result = ListAppend(result, RandomQ)>
				<CFSET arguments.list = ListDeleteAt(arguments.list, ListFind(arguments.list, RandomQ))>
			</CFLOOP>

			<cfreturn result>
	</cffunction>


	<cffunction name="getFunctionCallingFunction" output="false" returnType="struct">
		<cfargument name="ignoreFunction" type="string" default="">

	  <cfset var stackTrace = callStackGet()>
	  <cfset var item ="">
	  <cfset var counter = 0>

	  <cfloop array="#stackTrace#" index="item">

		  	<cfset counter ++>
		  	<!--- never want first function in array (this function), or second one (which is where this function is being called from - which presumably you already know the name of!) --->
			<cfif counter gt 2 and listFindNoCase (arguments.ignoreFunction, item.function) is 0 and listFindNoCase (arguments.ignoreFunction, listLast(item.template,"\")) is 0  >
		    	<cfset var result = {function = item.function,LineNumber = item.lineNumber, template = lcase(listLast(item.template,"\")), templateAbsolutePath = item.template, templatePath = item.template}>
				<!--- WAB 2016-01-24 modified so can work out relative path regardless of mapping --->
				<cfloop collection = #application.cf_mappings# item="mapping">
			    	<cfset result.templatePath = replaceNoCase(result.templatePath,application.cf_mappings[mapping],replace(mapping,"/","\"))>
				</cfloop>
			    <cfbreak>
			</cfif>
	  </cfloop>

	  <cfreturn result>

	 </cffunction>

	<!--- 	WAB 2016-01-25
			This function is mainly copied from a similar one in errorHandler.cfc
			But this one supports a regular expression
			And the lines are returned as an array rather than a structure
			I first used it in the <cf_readcode> tag which I used in examples
	--->
	<cffunction name="getLinesFromAFile" hint="Reads a set number of lines from a file, or until a regexp is matched">
		<cfargument name="filename" required="true">
		<cfargument name="line" required="true">
		<cfargument name="untilRegexp" required="false" hint = "Stop when line matches this regExp (optional, can use numberOfLines)">
		<cfargument name="numberOfLines" required = "false">

		<cfset var sourceFileContent = "">
		<cfset var sourceFileArray = "">
		<cfset var result = {isOK = true, filename = filename, startLine = line}>
		<cfset var text = "">
		<cfset var stop = 0>
		<cfset var counter = 0>

		<cfif not StructKeyExists(arguments,"untilRegexp") and not StructKeyExists(arguments,"numberOfLines")>
			<cfset result.isOK = false>
			<cfset result.message = "Must supply either untilRegexp or numberOfLines">
			<cfreturn result>
		</cfif>

		<cftry>
	  		<cffile action = "read" file="#fileName#" variable="sourceFileContent">

			<!--- replace all chr(13) since I am going to array to list on delimiter of chr(10) --->
			<cfset sourceFileContent = Replace(sourceFileContent, Chr(13), "", "all")>

			<!--- if a line has no content then add a space --->
			<cfset sourceFileContent = reReplace(sourceFileContent, "\A\n", " " & Chr(10), "all")>

			<!--- similar to above, should be able to do better with a zero width regexp --->
		    <cfloop condition="findNocase(Chr(10) & Chr(10),sourceFileContent)">
				<cfset sourceFileContent = Replace(sourceFileContent, Chr(10) & Chr(10), Chr(10) & " " & Chr(10), "all")>
		    </cfloop>

			<cfset sourceFileArray = ListToArray(sourceFileContent,  Chr(10))>
			<cfset result.line = line>


			<cfset result.lines = []>
			<cfset fileArrayIndex = line - 1 >
				<cfloop condition="not stop">
					<cfset counter ++ >
					<cfset text = sourceFileArray[line + counter - 1]>
					<cfset result.lines[counter] = text>

					<cfif StructKeyExists(arguments,"untilRegexp") and arguments.untilRegexp is not "">
						<cfset stop = refindnocase(untilRegexp,text)>
					<cfelse>
						<cfset stop = (counter GTE arguments.numberOfLines)>
					</cfif>

				</cfloop>
					<cfset result.endLine = fileArrayIndex>
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.Detail>
			</cfcatch>
		</cftry>

		<cfreturn result>

	</cffunction>




	<!--- WAB 2015-03-10 Needed this funcionality in a couple of places so added a function --->
	<cffunction name="areStringsEqual" hint="A string comparison function which is optionally case sensitive" output="false">
		<cfargument name="string1">
		<cfargument name="string2">
		<cfargument name="caseSensitive" default = "true">

		<cfset var compareResult = "">

		<cfif caseSensitive>
			<cfset compareResult = compare (string1,string2)>
		<cfelse>
			<cfset compareResult = compareNoCase (string1,string2)>
		</cfif>

		<cfreturn iif(compareResult is 0, true, false)>

	</cffunction>


	<cffunction name="_serializeJSON" returnType="string">
		<cfargument name = "JSON" type="any" required="true">
		<cfargument name = "forceToString" required="true" hint="A list of items which must be strings">

		<cfset var regExp = '("(?:#replace(arguments.forceToString,",","|","ALL")#)":)([0-9]+)'>
		<!--- NJH 2016/10/12 - if value is null, then don't put quotes around it --->
		<cfset var result = rereplaceNoCase(rereplaceNocase (serializeJSON(arguments.JSON), regExp,'\1"\2"',"ALL"),'"null"','null',"ALL")>
		<cfreturn result>
	</cffunction>


	<cffunction name="getSQLTextFromQuery" output="false" access="public" >
            <cfargument name = "queryObject">

            <cfset var extendedMetaData = queryObject.getMetaData().getExtendedMetaData()>
            <cfset var querySQL = "">

            <cfif isDefined("extendedMetaData")> <!--- if query has come from queryNew then appears not to have metadata --->
                  <cfif not structKeyExists (extendedMetaData,"sqlParameters")>
                        <cfset querySQL = extendedMetaData.sql>
                  <cfelse>
                        <cfset querySQL = replaceSQLParameters (sql= extendedMetaData.sql, sqlParameters = extendedMetaData.sqlParameters)>
                  </cfif>
            </cfif>

            <cfreturn querySQL>
      </cffunction>


      <cffunction name="replaceSQLParameters" output="false" access="private" hint="If a query has been build using CFSQLPARAM then this tries to reinstate the actual values">
            <cfargument name = "SQL">
            <cfargument name = "SQLParameters">

            <cfset var result = SQL>
            <cfset var value = "">

            <cfloop array="#SQLParameters#" index="value">
              <cfscript>
                        /* special case for bit type */
                    if ( value IS "YES" OR value IS "NO" ) {
                        value = iif(value,1,0);
                    }

                  if ( NOT isNumeric(value) ) {
                        value = "'#value#'";
                  }

                  result  = Replace(result , "?", value);
              </cfscript>
          </cfloop>

            <cfreturn result>
      </cffunction>

</cfcomponent>

