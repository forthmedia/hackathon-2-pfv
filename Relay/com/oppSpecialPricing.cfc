<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			oppSpecialPricing.cfc
Author:				???
Date started:		???

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Date (02-MAR-2005)	AJC	CR_ATI002_superAM	Function to assign a super manager to a lead's Viewing Rights IF a special price is added
Date (11-SEP-2008)	GCC	Extended getCountryApprovers method calls to include productCheck call and then productApprovalRequired passed in. BT ID
Date (20-JAN-2009)	NJH	Reset the approval status when deleting products.
Date (27-APR-2009)	GCC	LID:2137 use DB in case of changes in backend since page loaded in front end
Date (24-AUG-2009) 	NJH	P-ATI005 - changed way of calling 'CheckApproverLevelRequired' so that I could take advantage of over-riding the function.
					Also added new arguments and params for cc'ing level 4&5 approvers. Changed some queries to bring back approvers in levels 1-5 rather than 1-3.
	 (2009/11/04)	NAS LID - 2820 Updated code to stop the simultaneous changing then Approving of a Special Price
Date (11-NOV-2009)	AJC	P-LEX039 Competitive products
	  25-05-2010	NAS			P-PAN002 - Panda Imp
     2010/09/22		NAS	LID3743: Certain Products sending out approver emails
	 2011/02/01		PPB	LID5391 - user was getting "Special Pricing NOT progressed...." message when requesting an SP
	 2011/02/03		PPB	LID5367 - user was getting "Special Pricing NOT progressed...." message when more info had been requested
	2013-05-13 		WAB CASE 435201 updateSpecialPricing() replace cf_sql_decimal with cf_sql_float
	2013-08-28 		PPB Case 436679 if SP rejected then clear down values in oppProducts

Possible enhancements:
 --->
<cfcomponent displayname="oppSpecialPricing" hint="Functions for opportunity Special Pricing">

	<cfif isDefined("application.siteDataSource")>  <!--- when reloaded remotely application is not necessarily available, will be populated by an initialise function instead  --->
		<cfparam name="datasource" default="#application.siteDataSource#">
	</cfif>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateSpecialPricing" hint="This updates the special pricing status on the opportunity table.">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="opportunityID" type="numeric" required="yes">
		<cfargument name="specialPricingStatusMethod" type="string" required="yes">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="desiredDiscount" type="string">
		<cfargument name="desiredBudget" type="string">
		<cfargument name="SPExpiryDate" type="string" default="">
		<cfargument name="oppSpecialPricingStruct" type="struct" required="false" hint="structure where the keys are productID and the values hold the special price for the product">
		<cfargument name="SPReason" type="string" required="false">

		<cfset var reduceByThis = 0>
		<cfset var thisPercentage = 0>
		<cfset var comments ="Special pricing updated">
		<cfset var specialPrice = "null">

		<cfif isDefined("arguments.desiredDiscount") and arguments.desiredDiscount neq "">
			<cfset comments ="Special pricing changed to desired discount of #desiredDiscount#">
			<cfset thisPercentage = 1-(arguments.desiredDiscount/100)>
			<cfquery name="updateSpecialPricingStatus" datasource="#application.siteDataSource#">
				update opportunityProduct set specialPrice = unitPrice * #thisPercentage# where opportunityID = #arguments.opportunityID#
			</cfquery>

		<cfelseif (isDefined("desiredBudget") and desiredBudget neq "") or (structKeyExists(arguments,"oppSpecialPricingStruct") and structKeyExists(arguments.oppSpecialPricingStruct,"total") and arguments.oppSpecialPricingStruct.total neq "")>

			<cfinvoke method="getOppPricing" returnvariable="qOppPricing">
				<cfinvokeargument name="dataSource" value="#DataSource#">
				<cfinvokeargument name="opportunityID" value="#opportunityID#">
			</cfinvoke>

			<cfif structKeyExists(arguments,"oppSpecialPricingStruct") and structKeyExists(arguments.oppSpecialPricingStruct,"total") and arguments.oppSpecialPricingStruct.total neq "">
				<cfset arguments.desiredBudget = arguments.oppSpecialPricingStruct.total>
			</cfif>

			<cfset reduceByThis = arguments.desiredBudget / qOppPricing.fullprice>
			<cfset comments ="Special pricing changed to desired budget of #desiredBudget#">
			<cfquery name="updateSpecialPricingStatus" datasource="#application.siteDataSource#">
				update opportunityProduct set specialPrice = unitPrice * #reduceByThis# where opportunityID = #arguments.opportunityID#
			</cfquery>

		<!--- NJH 2010/04/27 P-PAN002 - passing in special pricing for individual products as a structure
		 	  WAB 2013-05-13 CASE 435201 replace cf_sql_decimal with cf_sqlfloat 'case was cf_sql_decimal rounds unless you use a scale parameter as well
		--->
		<cfelseif structKeyExists(arguments,"oppSpecialPricingStruct")>

			<cfloop collection="#arguments.oppSpecialPricingStruct#" item="productID">
				<cfif isNumeric(productID)>

					<cfset specialPrice = arguments.oppSpecialPricingStruct[productID]>
					<cfif arguments.oppSpecialPricingStruct[productID] eq "">
						<cfset specialPrice = "null">
					</cfif>

					<cfquery name="updateSpecialPricingStatus" datasource="#application.siteDataSource#">
						update opportunityProduct set specialPrice =  <cf_queryparam value="#specialPrice#" CFSQLTYPE="CF_SQL_FLOAT" >  where opportunityID = #arguments.opportunityID# and productOrGroupID =  <cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>
				</cfif>
			</cfloop>
		</cfif>

		<!--- GCC When adding comment preserve pricingstatusID. Collect from form --->
		<cfquery name="getSpecialPricingStatus" datasource="#application.siteDataSource#">
			select oppPricingStatusID from oppPricingStatus where
			<cfif arguments.specialPricingStatusMethod eq "SP_AddComment">
				oppPricingStatus =  <cf_queryparam value="#preservedOppPricingStatus#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				oppPricingStatusMethod =  <cf_queryparam value="#specialPricingStatusMethod#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		</cfquery>

		<cfquery name="insertOppPricingStatusHistory" datasource="#application.siteDataSource#">
			INSERT INTO [oppPricingStatusHistory]([opportunityID], [oppPricingStatusID], [personid], [comments])
			VALUES(<cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#comments#" CFSQLTYPE="CF_SQL_VARCHAR" >)

			<!--- NJH 2010/04/26 P-PAN002 - add a reason for the special pricing --->
			<cfif structKeyExists(arguments,"SPReason") and arguments.SPReason neq "">
			INSERT INTO [oppPricingStatusHistory]([opportunityID], [oppPricingStatusID], [personid], [comments])
			VALUES(<cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="Special Pricing Reason:#arguments.SPReason#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfif>
		</cfquery>

		<!--- <cfinvoke method="checkApproverLevelRequired" returnvariable="approverDetails">
			<cfinvokeargument name="dataSource" value="#dataSource#"/>
			<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
		</cfinvoke> --->

		<!--- NJH 2009/08/24 P-ATI005 - changed call to that of below so that I could take advantage of over-riding functions. --->
		<cfset approverDetails = application.com.oppSpecialPricing.checkApproverLevelRequired(dataSource=application.siteDataSource,opportunityID=opportunityID)>

		<cfquery name="updateOppPricingStatus" datasource="#application.siteDataSource#">
			update opportunity
				set oppPricingStatusID =  <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					SPRequiredApprovalLevel =  <cf_queryparam value="#listfirst(approverDetails,"|")#" CFSQLTYPE="CF_SQL_Integer" > ,
					SPCurrentApprovalLevel = 0,
					<cfif len(#arguments.SPExpiryDate#) neq 0>
					SPExpiryDate =  <cf_queryparam value="#arguments.SPExpiryDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					</cfif>
 					SPApproverPersonIDs = null
			where opportunityID = #arguments.opportunityID#
		</cfquery>

		<cfreturn "Special pricing updated">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="progressSpecialPricing" hint="This inserts a record into the OppPricingStatusHistory table">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="opportunityID" type="numeric" required="yes">
		<cfargument name="specialPricingStatusMethod" type="string" required="yes">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="comments" type="string" required="true">
		<cfargument name="SPApproverLevel" type="numeric" required="true">
		<cfargument name="SPApproverLevelRequired" type="numeric" required="true">
		<cfargument name="SPQCountryCCEmailList" type="string" required="true">
		<cfargument name="SPQLevel2CCEmailList" type="string" required="true">
		<cfargument name="SPQLevel3CCEmailList" type="string" required="true">
		<cfargument name="SPQLevel4CCEmailList" type="string" required="true"> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfargument name="SPQLevel5CCEmailList" type="string" required="true"> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfargument name="SPQViewCCEmailList" type="string" required="true">
		<cfargument name="Level3Approvers" type="numeric" required="no" default="0"> <!--- the number of Level 3 Approvers required to sign off this opp --->

		<cfscript>
			var ccPersonIDs = "";
			var myopportunity = "";
			var returncomments = "";
		</cfscript>

		<!--- Start - (2009/11/04)	NAS LID - 2820 Recheck the Approver Level Required --->
		<cfquery name="getOppDetails" datasource="#application.siteDataSource#">
			select SPCURRENTAPPROVALLEVEL from vLeadListingCalculatedBudget where opportunity_ID = #arguments.opportunityID#
		</cfquery>
		<!--- End --->

		<cfif specialPricingStatusMethod is "SP_Request">
			<cfscript>
				// create an instance of the opportunity component
				myopportunity = createObject("component","relay.com.opportunity");
				myopportunity.dataSource = application.siteDataSource;
				myopportunity.opportunityID = opportunityID;
				myopportunity.SuperAMAssign();
				myopportunity.get();
				// 2009/04/27 GCC LID:2137 use DB in case of changes in backend since page loaded in front end
				if (myopportunity.SPApproverLevelRequired neq "" and myopportunity.SPApproverLevelRequired neq 0) {
					SPApproverLevelRequired = myopportunity.SPApproverLevelRequired;
				}
			</cfscript>

			<!--- LID NJH 2011/01/12 - changed from vendorAccountManagerPersonID to vendorAccountManagerPersonID --->
			<cfif myopportunity.vendorAccountManagerPersonID is not "" and myopportunity.vendorAccountManagerPersonID neq 0>
				<cfset arguments.personID=myopportunity.vendorAccountManagerPersonID>
			</cfif>
		</cfif>

		<!--- if the approver is the same level as the level required then this should be fully approved --->
		<cfif SPApproverLevel eq SPApproverLevelRequired and specialPricingStatusMethod eq "SP_Approve">
			<cfset specialPricingStatusMethod = "SP_FullyApprove">
		</cfif>

		<!--- GCC When adding comment preserve pricingstatusID. Collect from form --->
		<cfquery name="getSpecialPricingStatus" datasource="#application.siteDataSource#">
			select oppPricingStatusID from oppPricingStatus where
			<cfif specialPricingStatusMethod eq "SP_AddComment">
				oppPricingStatus =  <cf_queryparam value="#preservedOppPricingStatus#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				oppPricingStatusMethod =  <cf_queryparam value="#specialPricingStatusMethod#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		</cfquery>

		<cfquery name="insertOppPricingStatusHistory" datasource="#application.siteDataSource#">
			INSERT INTO [oppPricingStatusHistory]([opportunityID], [oppPricingStatusID], [personid], [comments])
			VALUES(<cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#comments#" CFSQLTYPE="CF_SQL_VARCHAR" >)
		</cfquery>

		<cfset SPApproverPersonIDs = 0>
		<cfquery name="getExistingApprovers" datasource="#application.siteDataSource#">
			select SPApproverPersonIDs from opportunity where opportunityID = #arguments.opportunityID#
		</cfquery>

		<cfif getExistingApprovers.SPApproverPersonIDs neq "">
			<cfif listfind(getExistingApprovers.SPApproverPersonIDs,personid) eq 0>
				<cfset SPApproverPersonIDs = listappend(getExistingApprovers.SPApproverPersonIDs,PersonID)>
			<cfelse>
				<cfset SPApproverPersonIDs = getExistingApprovers.SPApproverPersonIDs>
			</cfif>
		<cfelse>
			<!--- 2010/09/22	NAS	LID3743: Certain Products sending out approver emails --->
			<!--- New Opportunity INI setting --->
			<!--- 2011/05/27 PPB REL106 moved to settings --->
			<cfif not application.com.settings.getSetting("leadManager.sendSPrequestEmail")>
				<!--- Set this as zero so the SP_Email function below is not invoked --->
				<cfset SPApproverPersonIDs = 0>
			<cfelse>
				<cfset SPApproverPersonIDs = arguments.PersonID>
			</cfif>
		</cfif>

		<cfquery name="updateOppPricingStatus" datasource="#application.siteDataSource#">
			update opportunity
				set oppPricingStatusID =  <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					SPRequiredApprovalLevel = #SPApproverLevelRequired#
					<cfif listFindnocase("SP_FullyApprove,SP_Approve",specialPricingStatusMethod) neq 0>
					,SPCurrentApprovalLevel = #SPApproverLevel#
					,SPApproverPersonIDs =  <cf_queryparam value="#SPApproverPersonIDs#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelseif listFindnocase("SP_Request,SP_Calculate",specialPricingStatusMethod) neq 0>
					,SPCurrentApprovalLevel = 0
					,SPApproverPersonIDs = NULL
					</cfif>
					<cfif listFindnocase("SP_FullyApprove",specialPricingStatusMethod) neq 0>
					,SPExpiryDate = CASE WHEN SPExpiryDate is null THEN DATEADD(day, 60, '#request.requestTime#') ELSE SPExpiryDate END
					</cfif>
			where opportunityID = #opportunityID#
			<!--- (2009/11/04)	NAS LID - 2820 Updated code to stop the simultaneous changing then Approving of a Special Price --->
			<cfif listFindnocase("SP_FullyApprove,SP_Approve",specialPricingStatusMethod) neq 0>
				AND SPCURRENTAPPROVALLEVEL = (#SPApproverLevel# -1)
			</cfif>
		</cfquery>

		<!--- START 2013-08-28 PPB Case 436679 --->
		<cfif specialPricingStatusMethod eq "SP_Reject">
			<cfset NullifyOppSpecialPrice(opportunityID=opportunityID)>
		</cfif>
		<!--- END 2013-08-28 PPB Case 436679 --->


		<!--- 2008/10/17 GCC / NJH removed listappend as ccPersonIDs is always empty until this point --->
		<!--- set the ccPersonIDs to always include the sender --->
		<cfset ccPersonIDs = arguments.personID>
		<!--- Start (2009/11/04)	NAS LID - 2820 Updated code to stop the simultaneous changing then Approving of a Special Price --->

		<!--- 2011/02/01 PPB LID5391 added the SP_Request condition below cos user was getting "Special Pricing NOT progressed...." message when requesting the SP --->
		<!--- 2011/02/03 PPB LID5367 added the SP_InfoResponse condition below cos user was getting "Special Pricing NOT progressed...." message when more info was requested --->
		<cfif (getOppDetails.SPCURRENTAPPROVALLEVEL EQ (#SPApproverLevel# -1))
			OR (getOppDetails.SPCURRENTAPPROVALLEVEL EQ (#SPApproverLevel#) AND (specialPricingStatusMethod eq "SP_InfoResponse") )
			OR (specialPricingStatusMethod eq "SP_Request") >
		<!--- Stop 	(2009/11/04)	NAS LID - 2820 --->

			<cfif specialPricingStatusMethod neq "SP_AddComment" and SPApproverPersonIDs neq 0>
				<cfinvoke method="SP_Email" returnvariable="message">
					<cfinvokeargument name="dataSource" value="#dataSource#"/>
					<cfinvokeargument name="senderPersonID" value="#arguments.personID#"/>
					<cfinvokeargument name="ccPersonIDs" value="#ccPersonIDs#"/>
					<cfinvokeargument name="specialPricingStatusMethod" value="#specialPricingStatusMethod#"/>
					<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
					<cfinvokeargument name="comments" value="#comments#"/>
					<cfinvokeargument name="SPApproverLevel" value="#SPApproverLevel#"/>
					<cfinvokeargument name="SPQCountryCCEmailList" value="#SPQCountryCCEmailList#"/>
					<cfinvokeargument name="SPQLevel2CCEmailList" value="#SPQLevel2CCEmailList#"/>
					<cfinvokeargument name="SPQLevel3CCEmailList" value="#SPQLevel3CCEmailList#"/>
					<cfinvokeargument name="SPQLevel4CCEmailList" value="#SPQLevel4CCEmailList#"/>
					<cfinvokeargument name="SPQLevel5CCEmailList" value="#SPQLevel5CCEmailList#"/>
					<cfinvokeargument name="SPQViewCCEmailList" value="#SPQViewCCEmailList#" />
				</cfinvoke>
				<cfif specialPricingStatusMethod eq "SP_InfoResponse">
					<cfset returncomments="Information added">
				<cfelse>
					<cfset returncomments="Special Pricing progressed to the next stage">
				</cfif>
			<cfelse>
				<cfset returncomments="Comment Added">
			</cfif>
		<!--- Start (2009/11/04)	NAS LID - 2820 --->
		<cfelse>
			<cfset returncomments="Special Pricing NOT progressed to the next stage because the opportunity has been updated.">
		</cfif>
		<!--- Stop 	(2009/11/04)	NAS LID - 2820 --->

		<cfreturn returncomments>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="SP_EMail" hint="This sends an email to the approver.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="senderPersonID" type="numeric" required="true">
		<cfargument name="ccPersonIDs" type="string" required="true">
		<cfargument name="specialPricingStatusMethod" type="string" required="yes">
		<cfargument name="opportunityID" type="numeric" required="yes">
		<cfargument name="comments" type="string" required="true">
		<cfargument name="SPApproverLevel" type="numeric" required="true">
		<cfargument name="SPQCountryCCEmailList" type="string" required="true">
		<cfargument name="SPQLevel2CCEmailList" type="string" required="true">
		<cfargument name="SPQLevel3CCEmailList" type="string" required="true">
		<cfargument name="SPQLevel4CCEmailList" type="string" required="true"> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfargument name="SPQLevel5CCEmailList" type="string" required="true"> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfargument name="SPQViewCCEmailList" type="string" required="true">

		<!--- 2009/02/20 SSS LID 708 Var'd more params to reduce chance of cc address list contamination --->
		<cfscript>
			var ccEmailList = "";
			var thisCCListSoFar = "";
			var productApprovalRequired = "";
			var SPapprovers = "";
			var thisCCList = "";
			var thisSender = "";
			var thisSendTo = "";
			var myopportunity = "";
		</cfscript>

		<!--- AJC 2006-06-27 CR_ATI035 --->
		<cfparam name="request.SPQExpiryLength" default="10">

		<cfquery name="getOppDetails" datasource="#dataSource#">
			select * from vLeadListingCalculatedBudget where opportunity_ID = #opportunityID#
		</cfquery>

		<cfscript>
		thisSender = createObject("component","relay.com.commonQueries");
		thisSender.dataSource = dataSource;
		thisSender.personID = senderPersonID;
		thisSender.getPersonDetails();

		thisSendTo = createObject("component","relay.com.commonQueries");
		thisSendTo.dataSource = dataSource;
		thisSendTo.personID = getOppDetails.vendorAccountManagerPersonID;
		thisSendTo.getPersonDetails();

		myopportunity = createObject("component","relay.com.opportunity");
		myopportunity.dataSource = application.siteDataSource;
		myopportunity.opportunityID = opportunityID;
		myopportunity.productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID");	// was hardcoded to 6
		myopportunity.countryID = getOppDetails.countryID;
		myopportunity.sortOrder = "SKU ASC";
		myopportunity.getOppProducts();

		thisCCList = createObject("component","relay.com.commonQueries");
		ccPersonIDs = listAppend(ccPersonIDs,getOppDetails.vendorAccountManagerPersonID);
		</cfscript>

		<cfloop index="i" list="#ccPersonIDs#">
		<cfscript>
		if (listfind(thisCCListSoFar,i) eq 0){
		thisCCList.dataSource = dataSource;
		thisCCList.personID = i;
		thisCCList.getPersonDetails();
		ccEmailList = listAppend(ccEmailList,thisCCList.email);
		thisCCListSoFar = listAppend(thisCCListSoFar,i);}
		</cfscript>
		</cfloop>

<!---   These CC abd BCC lists can be superceded in the opportunityINI file.
		Use a comma separated list of email addresses to always send to the list  --->
		<cfparam name="SPQbccList" default="relayhelpcc@foundation-network.com">
		<cfparam name="SPQLevel2CCEmailList" default="">
		<cfparam name="SPQLevel3CCEmailList" default="">
		<cfparam name="SPQLevel4CCEmailList" default=""> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfparam name="SPQLevel5CCEmailList" default=""> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfparam name="SPQCountryCCEmailList" default="">
		<cfparam name="setSPQExipryDate" default="false">

		<cfif #ListLen(SPQCountryCCEmailList)# GT 0>
			<cfset ccEmailList = listAppend(ccEmailList,SPQCountryCCEmailList)>
		</cfif>
		<cfif #ListLen(SPQViewCCEmailList)# GT 0>
			<cfset ccEmailList = listAppend(ccEmailList,SPQViewCCEmailList)>
		</cfif>

		<cfscript>
			productApprovalRequired = productCheck(opportunityID = opportunityID);
		</cfscript>

		<cfswitch expression="#specialPricingStatusMethod#">
			<!--- these need to be made into phrases --->
			<cfcase value="SP_Request">
				<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
					<cfinvokeargument name="dataSource" value="#dataSource#"/>
					<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
					<cfinvokeargument name ="productApprovalRequired" value="#productApprovalRequired#">
				</cfinvoke>
				<cfset mailToList = "#listLast(SPapprovers.ApproverPersonID.level1,"|")#">
				<cfset SP_Email_SubjecLine = "Special Price Request">
				<cfset SP_Email_Text = "Please approve this special Price">
				<!--- CR_ATINordicCCs GCC 2006/03/02 --->
				<!--- Will send to all start - country specific and global --->
				<cfif structkeyexists(request,"qSPStartCCEmail")>
					<cfscript>
						statusCCEmailAddresses = getStatusCCemailaddresses(sourceQuery=request.qSPStartCCEmail,countryID=myopportunity.countryID);
					</cfscript>
				</cfif>
				<!--- AJC CR_ATI035 --->
				<cfset setSPQExipryDate=true>
			</cfcase>

	<!--- I've added in a check so that level 3 approvers get cc'd on the fully approved email. MDC 29th June 2004 --->
			<cfcase value="SP_FullyApprove">
				<cfparam name="SPApprovalNumber" default="">

				<cfif SPApproverLevel eq 10>
					<cfset mailToList = "maxine@foundation-network.com">
				<cfelseif SPApproverLevel neq 10>
					<cfset mailToList = "#thisSendTo.email#">
				</cfif>

				<cfset SP_Email_SubjecLine = "Special Price Fully Approved">
				<cfset SP_Email_Text = "Your special price request has been fully approved.">

				<cfif SPApproverLevel eq 3>
					<cfset ccEmailList = listAppend(ccEmailList,SPQLevel3CCEmailList)>
				<cfelseif SPApproverLevel eq 10>
					<cfset nextApproverEmail = "">
					<cfset ccEmailList = "">
					<cfif len(SPQLevel3CCEmailList) gt 0>
						<cfset ccEmailList = "">
					</cfif>
				</cfif>

				<!--- CR_ATINordicCCs GCC 2006/03/02 --->
				<!--- Will send to all configured - country specific and global --->
				<cfif structkeyexists(request,"qSPApprovedCCEmail")>
					<cfscript>
						statusCCEmailAddresses = getStatusCCemailaddresses(sourceQuery=request.qSPApprovedCCEmail,countryID=myopportunity.countryID);
					</cfscript>
				</cfif>

				<cfinvoke method="getSPApprovalNumber" returnvariable="qSPApprovalNumber">
					<cfinvokeargument name="dataSource" value="#dataSource#"/>
					<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
				</cfinvoke>
				<cfset SP_Email_Text = SP_Email_Text & " The Special Pricing ID is #qSPApprovalNumber.SPApprovalNumber#.">
				<!--- AJC CR_ATI035 --->
				<cfset setSPQExipryDate=true>
			</cfcase>

			<cfcase value="SP_Approve">
				<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
					<cfinvokeargument name="dataSource" value="#dataSource#"/>
					<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
					<cfinvokeargument name ="productApprovalRequired" value="#productApprovalRequired#">
				</cfinvoke>
				<cfif SPApproverLevel eq 1>
					<cfset nextApproverEmail = "#listLast(SPapprovers.ApproverPersonID.level2,"|")#">
					<cfset ccEmailList = listAppend(ccEmailList,nextApproverEmail)>
					<cfif len(SPQLevel2CCEmailList) gt 0>
						<cfset ccEmailList = listAppend(ccEmailList,SPQLevel2CCEmailList)>
					</cfif>
				<cfelseif SPApproverLevel eq 2>
					<cfset nextApproverEmail = "#listLast(SPapprovers.ApproverPersonID.level3,"|")#">
					<cfset ccEmailList = listAppend(ccEmailList,nextApproverEmail)>
					<cfif len(SPQLevel3CCEmailList) gt 0>
						<cfset ccEmailList = listAppend(ccEmailList,SPQLevel3CCEmailList)>
					</cfif>
				<!--- NJH 2009/08/19 P-ATI005 added next two if blocks below - 3&4 --->
				<cfelseif SPApproverLevel eq 3>
					<cfset nextApproverEmail = "#listLast(SPapprovers.ApproverPersonID.level4,"|")#">
					<cfset ccEmailList = listAppend(ccEmailList,nextApproverEmail)>
					<cfif len(SPQLevel4CCEmailList) gt 0>
						<cfset ccEmailList = listAppend(ccEmailList,SPQLevel4CCEmailList)>
					</cfif>
				<cfelseif SPApproverLevel eq 4>
					<cfset nextApproverEmail = "#listLast(SPapprovers.ApproverPersonID.level5,"|")#">
					<cfset ccEmailList = listAppend(ccEmailList,nextApproverEmail)>
					<cfif len(SPQLevel5CCEmailList) gt 0>
						<cfset ccEmailList = listAppend(ccEmailList,SPQLevel5CCEmailList)>
					</cfif>
				</cfif>
				<cfset mailToList = "#thisSendTo.email#">
				<cfset SP_Email_SubjecLine = "Special Price Partly Approved">
				<cfset SP_Email_Text = "Your special price request has been partly approved and forwarded to the next approver.">
				<!--- AJC CR_ATI035 --->
				<cfset setSPQExipryDate=true>
			</cfcase>

			<cfcase value="SP_MoreInfo">
				<cfset mailToList = "#thisSendTo.email#">
				<cfset SP_Email_SubjecLine = "Special Price request more info">
				<cfset SP_Email_Text = "I need more infomation before we can agree this. Please provide your feedback through the Customer Platform.">
				<cfif SPApproverLevel eq 3>
					<cfset ccEmailList = listAppend(ccEmailList,SPQLevel3CCEmailList)>
				</cfif>
			</cfcase>

			<cfcase value="SP_InfoResponse">
				<!--- get approvers --->
				<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
					<cfinvokeargument name="dataSource" value="#dataSource#"/>
					<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
					<cfinvokeargument name ="productApprovalRequired" value="#productApprovalRequired#">
				</cfinvoke>
				<cfquery name="getOppDetails" datasource="#application.siteDataSource#">
					select * from vLeadListingCalculatedBudget where opportunity_ID = #opportunityID#
				</cfquery>
						<!--- errorrrrrrrr --->
				<cfif getOppDetails.SPCurrentApprovalLevel eq 0>
					<cfset mailToList = "#listLast(SPapprovers.ApproverPersonID.level1,"|")#">
				<cfelseif getOppDetails.SPCurrentApprovalLevel eq 1>
					<cfset mailToList = "#listLast(SPapprovers.ApproverPersonID.level2,"|")#">
				<cfelseif getOppDetails.SPCurrentApprovalLevel eq 2>
					<cfset mailToList = "#listLast(SPapprovers.ApproverPersonID.level3,"|")#">
				<!--- NJH 2009/08/19 P-ATI005 - added level 3&4 --->
				<cfelseif getOppDetails.SPCurrentApprovalLevel eq 3>
					<cfset mailToList = "#listLast(SPapprovers.ApproverPersonID.level4,"|")#">
				<cfelseif getOppDetails.SPCurrentApprovalLevel eq 4>
					<cfset mailToList = "#listLast(SPapprovers.ApproverPersonID.level5,"|")#">
				</cfif>
				<cfset SP_Email_SubjecLine = "Special Price more info response">
				<cfset SP_Email_Text = "I have responded to the more infomation request. Please can you approve the Special Price Request.">
				<!--- <cfset mailToList = "#thisSendTo.email#">
				<cfset SP_Email_SubjecLine = "Special Price more info response">
				<cfset SP_Email_Text = "I have responded to the more infomation request. Please can you approve the Special Price Request.">
				<cfif SPApproverLevel eq 3>
					<cfset ccEmailList = listAppend(ccEmailList,SPQLevel3CCEmailList)>
				</cfif> --->
			</cfcase>

			<cfcase value="SP_Reject">
				<cfset mailToList = "#thisSendTo.email#">
				<cfset SP_Email_SubjecLine = "Special Price Request Rejected">
				<cfset SP_Email_Text = "Sorry we cannot agree to special pricing at this level.">
				<cfif SPApproverLevel eq 3>
					<cfset ccEmailList = listAppend(ccEmailList,SPQLevel3CCEmailList)>
				</cfif>
			</cfcase>

			<cfcase value="SP_Complete">
				<cfset mailToList = "#thisSendTo.email#">
				<cfset SP_Email_SubjecLine = "Special Price Request Completed">
				<cfset SP_Email_Text = "This special price has been marked as complete.">
			</cfcase>

			<cfcase value="SP_Finance">
				<cfset mailToList = "#thisSendTo.email#">
				<cfset SP_Email_SubjecLine = "Special Price Request Completed">
				<cfset SP_Email_Text = "This special price has been marked as complete.">
			</cfcase>

			<cfcase value="SP_Expiry">
				<cfset mailToList = "#thisSendTo.email#">
				<!--- if status ID is 8, it hasn't expired yet..If it has expired, it will have been set to 2 --->
 				<cfif getOppDetails.oppPricingStatusID eq 8>
					<cfset SP_Email_SubjecLine = "Special Price Will Expire In 7 Days">
					<cfset SP_Email_Text = "This special price will expire in 7 days.">
				<cfelse>
					<cfset SP_Email_SubjecLine = "Special Price Expired">
					<cfset SP_Email_Text = "This special price has expired.">
				</cfif>
			</cfcase>

		</cfswitch>
		<!--- AJC CR_ATI035 --->
		<cfif setSPQExipryDate>
			<cfquery name="updSPExipry" datasource="#application.siteDataSource#">
				UPDATE opportunity
				set SPExpiryDate=#createodbcdatetime(dateadd("d",request.SPQExpiryLength,now()))#
				where opportunityID=#opportunityID#
			</cfquery>
		</cfif>
		<!--- CR_ATINordicCCs GCC 2006/03/02 --->
		<!--- If the case also has status CC emails associated with it apply them before sending --->
		<cfif isdefined('statusCCEmailAddresses') and statusCCEmailAddresses.recordcount gt 0>
			<cfloop query="statusCCEmailAddresses">
				<cfset ccEmailList = listAppend(ccEmailList,email)>
			</cfloop>
		</cfif>

		<!--- LID 4508 SKP 2010-10-28 --->

		<cfset protocol = "http:">

		<cfquery name="isRWVersionGTE8_3" datasource="#application.siteDataSource#">
			select * from INFORMATION_SCHEMA.COLUMNS
			where TABLE_NAME='sitedef'
			and COLUMN_NAME='issecure'
		</cfquery>
		<cfif isRWVersionGTE8_3.recordcount GT 0>
		<cfquery name="GetInternalhttpProtocol" datasource="#application.siteDataSource#">
	             select issecure from sitedef where isinternal = 1 and issecure = 1
        </cfquery>
		<cfif GetInternalhttpProtocol.recordcount GT 0>
			<cfset protocol = "https:">
			</cfif>
		</cfif>

	 	<!---  25-05-2010 NAS	P-PAN002 - Panda Imp Changed to CF_MAIL from CFMAIL --->
		<cf_mail to="#mailToList#"
	        from="#thisSender.Email#"
	        subject="#SP_Email_SubjecLine#"
			cc="#ccEmailList#"
	        bcc="#SPQbccList#"
	        type="HTML">
			<!--- added to enable direct access to SPQ from email --->
	<cfoutput>
	<a href="#protocol#//#request.currentsite.internaldomain#/index.cfm?opportunityid=#opportunityid#&st=010">Click here to view this Special Price</a><br>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
		<tr>
			<th colspan="6" align="left">
				#SP_Email_Text#
			</th>
		</tr>
		<TR>
			<td colspan="6" align="left">
				<br><strong>Project name:</strong> #getOppDetails.detail# (OpportunityID: #opportunityID#) <!--- AJC CR_ATI035 changed from entityID --->
				<br><strong>Account:</strong> #getOppDetails.account# (OrganisationID: #getOppDetails.entityID#)
				<br><strong>Account manager:</strong> #getOppDetails.ACCOUNT_MANAGER#
				<br><strong>Reason for request:</strong> #comments#
			</td>
		</tr>
		<TR>
			<cf_translate>
			<TH>Product Description</TH>
			<TH>Product SKU</TH>
			<TH>Quantity</TH>
			<TH>Unit Price</TH>
			<TH>Special Price</TH>
			<TH>Sub Total</TH>
			<!--- <TH>phr_oppProductCOGS</TH>
			<TH>phr_oppProductMargin</TH> --->
		</TR>
		</cf_translate>
		<CFLOOP QUERY="myopportunity.getOppProducts">
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<TD>#description#</TD>
				<TD>#sku#</TD>
				<td align="right">#quantity#</td>
				<td align="right">#LSCurrencyFormat(unitprice,"local")#</td>
				<td align="right">#LSCurrencyFormat(specialPrice,"local")#</td>
				<td align="right">#LSCurrencyFormat(subtotal,"local")#</td>
				<!--- <td align="right">#LSCurrencyFormat(COGS,"local")#</td>
				<td align="right">#LSCurrencyFormat(Margin,"local")#</td> --->
			</TR>
		</CFLOOP>
		<tr><td colspan="6"><a href="#protocol#//#request.currentsite.internaldomain#/remote.cfm?p=#request.relaycurrentuser.pnumber#&a=i&st=002&entityID=#opportunityID#">Click to login</a>
			</td>
		</tr>
		<tr>
			<th colspan="6" align="left">
				<p>
				<font color="##FF0000"><br>
				WARNING! THIS EMAIL IS GENERATED AUTOMATICALLY BY RELAYWARE.<br>
				DO NOT RESPOND TO THIS EMAIL! <br>
				IF YOU NEED TO MAKE COMMENTS DO SO VIA RELAYWARE.
				</font>
				</p>
			</th>
		</tr>
	</TABLE>
	</cfoutput>
	</cf_mail>

		<cfreturn "Special pricing email sent">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkApprover" hint="This checks what level approver this person is.">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="countryID" type="numeric" default="0">
		<cfargument name="opportunityID" type="numeric" required="no">

		<cfscript>
			var qCheckApprover = "";
			var productApprovalRequired = "false";
		</cfscript>
		<cfif isDefined('arguments.opportunityID')>
			<cfscript>
				productApprovalRequired = productCheck(opportunityID = arguments.opportunityID);
			</cfscript>
		</cfif>
		<cfquery name="qCheckApprover" datasource="#application.siteDataSource#">
			select * from oppSPApprover
				where personID = #personID# and countryID in (#countryID#,0)
				order by countryID desc, approverLevel asc
		</cfquery>

		<cfif qCheckApprover.recordCount gt 0>
			<cfif productApprovalRequired>
				<cfreturn valueList(qCheckApprover.productApproverLevel)>
			<cfelse>
				<cfreturn valueList(qCheckApprover.approverLevel)>
			</cfif>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkApproverLevelRequired" hint="This works out what level approver is required for this opp.">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfinvoke method="getOppPricing" returnvariable="qOppPricing">
			<cfinvokeargument name="dataSource" value="#dataSource#"/>
			<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
		</cfinvoke>

		<cfscript>
			productApprovalRequired = productCheck(opportunityID = arguments.opportunityID);
		</cfscript>

		<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
			<cfinvokeargument name="dataSource" value="#dataSource#"/>
			<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
			<cfinvokeargument name ="productApprovalRequired" value="#productApprovalRequired#">
		</cfinvoke>

		<cfif cgi.REMOTE_HOST eq "62.232.221.162">
			<cfoutput>checkApproverLevelRequired<cfdump var="#SPapprovers#"></cfoutput>
		</cfif>

<!--- 2011/03/17 PPB replaced request.qSPapproverLogic with SETTINGS
		<!--- create some rules logic in a query object in the ini file to overwrite this --->
		<cfif not structKeyExists(request,"qSPapproverLogic")>
			<cfscript>
			request.qSPapproverLogic = StructNew();
				request.qSPapproverLogic["countryID0"] = StructNew();
				request.qSPapproverLogic["countryID0"]["level1Threshold"] = "50000";
				request.qSPapproverLogic["countryID0"]["level2Threshold"] = "100000";
				request.qSPapproverLogic["countryID0"]["level1Tier1Perc"] = "6";
				request.qSPapproverLogic["countryID0"]["level1Tier2Perc"] = "8";
				request.qSPapproverLogic["countryID0"]["level2Tier1Perc"] = "6";
				request.qSPapproverLogic["countryID0"]["level2Tier2Perc"] = "8";
				request.qSPapproverLogic["countryID0"]["level3Tier1Perc"] = "5";
				request.qSPapproverLogic["countryID0"]["level3Tier2Perc"] = "7";
			</cfscript>
		</cfif>
 --->
<cfset approvalLevel1Threshold = application.com.settings.getSetting("leadManager.specialPricing.approvalLevel1Threshold")>
<cfset approvalLevel2Threshold = application.com.settings.getSetting("leadManager.specialPricing.approvalLevel2Threshold")>
<cfset approvalLevel1Tier1Perc = application.com.settings.getSetting("leadManager.specialPricing.approvalLevel1Tier1Perc")>
<cfset approvalLevel1Tier2Perc = application.com.settings.getSetting("leadManager.specialPricing.approvalLevel1Tier2Perc")>


		<!--- CR_ATI030 get special case 'product' approver here and set up as level 2 and 3 level deal --->

		<!--- debug ---><!--- 		<cfset discount = (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100>
		<cfset thresholdlevel2 = request.qSPapproverLogic.countryID0.level1Tier2Perc>
		<cfoutput>
		<cfif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				lt request.qSPapproverLogic.countryID0.level1Tier2Perc>
		level2: #discount# is less than #thresholdlevel2#
		<cfelse>
		level3: #discount# is greater than #thresholdlevel2#
		</cfif>
		</cfoutput>
		<CF_ABORT> --->
		<!--- here we sould work out which country this is in and some how set a countryID key  --->
		<!--- <cfset countryID = "countryID0"> --->
		<cfif qOppPricing.fullPrice eq 0>
			<cfoutput><script>alert ('Warning: The Full Price of this Order is Zero')</script> </cfoutput>
			<cfreturn "3|0">
		<cfelseif qOppPricing.fullPrice eq "" or qOppPricing.fullPrice eq 0>  <!--- WAB added the eq 0 - to deal with list price of zero which gives divide by zero errors --->
			<cfreturn "0|0">
		<!--- less than level one threshold --->
		<cfelseif qOppPricing.fullPrice lt approvalLevel1Threshold>
			<!--- less than level 1 percentage discount --->
			<cfif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100 lte approvalLevel1Tier1Perc>
				<cfreturn "1|#SPapprovers.ApproverPersonID.level1#">
			<!--- more than level 1 percentage discount, less than level 2 percentage discount --->
			<cfelseif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				gt approvalLevel1Tier1Perc and (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				lt approvalLevel1Tier2Perc>
				<!--- level 3 with product check, level 2 without --->
				<cfif productApprovalRequired eq "true">
					<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
						<cfinvokeargument name="dataSource" value="#dataSource#"/>
						<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
						<cfinvokeargument name="productApprovalRequired" value="true"/>
					</cfinvoke>
					<cfset maxApproverLevel = getMaxApproverLevel(opportunityID=#opportunityID#)>
					<cfset approverString = "SPapprovers.ApproverPersonID.level" & #maxApproverLevel#>
					<cfreturn "#maxApproverLevel#|#approverString#">
				<cfelse>
					<cfreturn "2|#SPapprovers.ApproverPersonID.level2#">
				</cfif>
			<!--- more than level 2 percentage discount --->
			<cfelseif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				gt approvalLevel1Tier2Perc>
				<cfreturn "3|#SPapprovers.ApproverPersonID.level3#">
			</cfif>
		<!--- more than level 1 threshold, less than level 2 threshold (makes it a level 2 minimum)--->
		<cfelseif qOppPricing.fullPrice gte approvalLevel1Threshold and qOppPricing.fullPrice lt approvalLevel2Threshold>
			<!--- less than level 2 percentage discount --->
			<cfif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100 lte approvalLevel1Tier1Perc>
				<!--- level 3 with product check, level 2 without --->
				<cfif productApprovalRequired eq "true">
					<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
						<cfinvokeargument name="dataSource" value="#dataSource#"/>
						<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
						<cfinvokeargument name="productApprovalRequired" value="true"/>
					</cfinvoke>
					<cfset maxApproverLevel = getMaxApproverLevel(opportunityID=#opportunityID#)>
					<cfset approverString = "SPapprovers.ApproverPersonID.level" & #maxApproverLevel#>
					<cfreturn "#maxApproverLevel#|#approverString#">
				<cfelse>
					<cfreturn "2|#SPapprovers.ApproverPersonID.level2#">
				</cfif>
			<!--- more than level 2 percentage discount, less than level 3 percentage discount --->
			<cfelseif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				gt approvalLevel1Tier1Perc and (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				lt approvalLevel1Tier2Perc>
				<!--- level 3 with product check, level 2 without --->
				<cfif productApprovalRequired eq "true">
					<cfinvoke method="getCountryApprovers" returnvariable="SPapprovers">
						<cfinvokeargument name="dataSource" value="#dataSource#"/>
						<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
						<cfinvokeargument name="productApprovalRequired" value="true"/>
					</cfinvoke>
					<cfset maxApproverLevel = getMaxApproverLevel(opportunityID=#opportunityID#)>
					<cfset approverString = "SPapprovers.ApproverPersonID.level" & #maxApproverLevel#>
					<cfreturn "#maxApproverLevel#|#approverString#">
				<cfelse>
					<cfreturn "2|#SPapprovers.ApproverPersonID.level2#">
				</cfif>
			<!--- more than level 2 percentage discount --->
			<cfelseif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				gt approvalLevel1Tier2Perc>
				<cfreturn "3|#SPapprovers.ApproverPersonID.level3#">
			</cfif>
		<!--- more than level 2 threshold --->
		<cfelseif qOppPricing.fullPrice gte approvalLevel2Threshold>
			<cfif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100 lte approvalLevel1Tier1Perc>
				<cfreturn "1|#SPapprovers.ApproverPersonID.level1#">
			<cfelseif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				gt approvalLevel1Tier1Perc and (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				lt approvalLevel1Tier2Perc>
				<cfreturn "2|#SPapprovers.ApproverPersonID.level2#">
			<cfelseif (1-(qOppPricing.discountedSpecialPrice / qOppPricing.fullPrice))*100
				gt approvalLevel1Tier2Perc>
				<cfreturn "3|#SPapprovers.ApproverPersonID.level3#">
			</cfif>
		</cfif>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOppPricingReason" hint="This returns a query object containing oppPricingReason data ordered by reason.">
		<cfargument name="dataSource" type="string" required="true">

		<cfquery name="getOppPricingReason" datasource="#application.siteDataSource#">
			select * from oppPricingReason order by reason
		</cfquery>
		<cfreturn getOppPricingReason>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOppPricing" hint="This returns a query object containing the pricing info for an opportunity.">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfscript>
			var getOppPricing="";
		</cfscript>

		<cfquery name="getOppPricing" datasource="#application.siteDataSource#">
			select sum(unitPrice * quantity) as fullPrice,
				sum (CASE WHEN specialPrice > 0 THEN (specialPrice * quantity)
					ELSE unitPrice * quantity
				END) as discountedSpecialPrice,
				sum((unitPrice - COGS) * quantity) as totalMarginStandardPrice,
				sum (CASE
					WHEN specialPrice > 0 THEN (specialPrice - COGS) * quantity
					ELSE (unitPrice - COGS) * quantity
				END) as totalSPMargin
			from opportunityProduct INNER JOIN product p ON p.productID = opportunityProduct.productOrGroupID and opportunityProduct.productOrGroup = 'P'
			WHERE opportunityID = #opportunityID#
		</cfquery>
		<cfreturn getOppPricing>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCountryApprovers" hint="This returns a query object containing country approvers.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name ="productApprovalRequired" required="no" default="false">

		<cfscript>
			var getApprovers = "";
			productApprovalRequired = arguments.productApprovalRequired;
		</cfscript>

		<cfquery name="getOppCountryID" datasource="#application.siteDataSource#">
			select countryID,sprequiredapprovallevel from opportunity where opportunityID = #arguments.opportunityID#
		</cfquery>

		<cfparam name="request.ProductORGroup" default="P">

		<cfif getOppCountryID.sprequiredapprovallevel gt 1 AND structkeyexists(request,"specialPricingProductORGroupIDsRequiringAdditionalApproval")>
		<cfscript>
			productApprovalRequired = productCheck(opportunityID = arguments.opportunityID);
		</cfscript>
		</cfif>

		<cfif productApprovalRequired eq "true">
			<cfquery name="getApprovers" datasource="#application.siteDataSource#">
			SELECT convert(varchar(20),o.personID) + '|' + p.email as approver, [countryID], [productapproverLevel] as approverlevel
				FROM [oppSPApprover] o
				INNER JOIN person p ON p.personID = o.personID
				where countryid in ( <cf_queryparam value="#getOppCountryID.countryid#" CFSQLTYPE="CF_SQL_INTEGER" >,0)
				and o.productapproverlevel in (1,2,3,4,5)  <!--- NJH 2009/08/19 P-ATI005 added 4,5 --->
				order by countryID desc
			</cfquery>
		<cfelse>
			<cfquery name="getApprovers" datasource="#application.siteDataSource#">
			SELECT convert(varchar(20),o.personID) + '|' + p.email as approver, [countryID], [approverLevel]
				FROM [oppSPApprover] o
				INNER JOIN person p ON p.personID = o.personID
				where countryid in (<cf_queryparam value="#getOppCountryID.countryid#" CFSQLTYPE="CF_SQL_INTEGER" >,0)
				and o.approverlevel in (1,2,3,4,5)  <!--- NJH 2009/08/19 P-ATI005 added 4,5 --->
				order by countryID desc
			</cfquery>
		</cfif>
		<cfset level1ApproverPersonID= "">
		<cfset level2ApproverPersonID= "">
		<cfset level3ApproverPersonID= "">
		<cfset level4ApproverPersonID= ""> <!--- NJH 2009/08/19 P-ATI005 --->
		<cfset level5ApproverPersonID= ""> <!--- NJH 2009/08/19 P-ATI005 --->

		<cfif getApprovers.recordCount gt 0>
			<cfloop query="getApprovers">
				<cfif level1ApproverPersonID eq "" and getApprovers.approverLevel eq "1">
					<cfset level1ApproverPersonID= getApprovers.approver>
				<cfelseif level2ApproverPersonID eq "" and getApprovers.approverLevel eq "2">
					<cfset level2ApproverPersonID= getApprovers.approver>
				<cfelseif level3ApproverPersonID eq "" and getApprovers.approverLevel eq "3">
					<cfset level3ApproverPersonID= getApprovers.approver>
				<!--- NJH 2009/08/19 P-ATI005 - added approver levels 4 and 5 --->
				<cfelseif level4ApproverPersonID eq "" and getApprovers.approverLevel eq "4">
					<cfset level4ApproverPersonID= getApprovers.approver>
				<cfelseif level5ApproverPersonID eq "" and getApprovers.approverLevel eq "5">
					<cfset level5ApproverPersonID= getApprovers.approver>
				</cfif>
			</cfloop>
		</cfif>
		<cfscript>
		SPapprovers = StructNew();
			SPapprovers["ApproverPersonID"] = StructNew();
			SPapprovers["ApproverPersonID"]["level1"] = level1ApproverPersonID;
			SPapprovers["ApproverPersonID"]["level2"] = level2ApproverPersonID;
			SPapprovers["ApproverPersonID"]["level3"] = level3ApproverPersonID;
			SPapprovers["ApproverPersonID"]["level4"] = level4ApproverPersonID; //NJH 2009/08/19 P-ATI005
			SPapprovers["ApproverPersonID"]["level5"] = level5ApproverPersonID; //NJH 2009/08/19 P-ATI005
		</cfscript>
		<cfreturn SPapprovers>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setProductShipping" hint="This updates product shipping data and adds a history record.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="formFields" type="string" required="true">
		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="opportunityID" type="numeric" required="yes">

		<CFLOOP INDEX="i" LIST="#formFields#">
			<cfif left(i,8) eq "shipped_" and listlast(i,"_") neq "" and evaluate(i) neq "">
			  	<cfquery name="setProductShipping" datasource="#application.siteDataSource#">
					UPDATE opportunityProduct set productsShipped =  <cf_queryparam value="#evaluate(i)#" CFSQLTYPE="CF_SQL_Integer" >
					WHERE opportunityProductID = <cf_queryparam value="#listlast(i,"_")#" CFSQLTYPE="CF_SQL_Integer" >
				</cfquery>
			</cfif>
			<cfif left(i,17) eq "shippingComplete_" and listlast(i,"_") neq "">
				<cfquery name="getShippingComplete" datasource="#application.siteDataSource#">
					select shippingComplete, productsShipped, quantity from opportunityProduct
					WHERE opportunityProductID =  <cf_queryparam value="#listlast(i,"_")#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfif listlast(i,"_") eq 1><cfset shippingCompleteValue = 0><cfelse><cfset shippingCompleteValue = 1></cfif>
			  	<cfquery name="setShippingComplete" datasource="#application.siteDataSource#">
					UPDATE opportunityProduct set shippingComplete =  <cf_queryparam value="#shippingCompleteValue#" CFSQLTYPE="CF_SQL_bit" >
					<cfif getShippingComplete.productsShipped eq 0>
					, productsShipped = quantity
					</cfif>
					WHERE opportunityProductID =  <cf_queryparam value="#listlast(i,"_")#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			</cfif>
		</CFLOOP>

		<cfquery name="insertOppPricingStatusHistory" datasource="#application.siteDataSource#">
			INSERT INTO [oppPricingStatusHistory]([opportunityID], [oppPricingStatusID], [personid], [comments])
			SELECT <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >, oppPricingStatusID, <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >, 'Shipping data updated by financial controller'
			FROM oppPricingStatus where oppPricingStatusMethod = 'SP_FullyApprove'
		</cfquery>

		<cfreturn "Shipping data updated">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setSPApprovalNumber" hint="This sets the unique special price approval number for an opportunity.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="SPApprovalNumber" type="string" required="true">

<!--- 	MDC 1st July 2004 added in query below so that if an spq approval number has already been
		generated it will not create a new one.--->

		<cfquery name="checkSPApprovalNumber" datasource="#application.siteDataSource#">
			select SPApprovalNumber
			from opportunity
			where opportunityID = #opportunityID#
			and SPApprovalNumber is not null
		</cfquery>

	<cfif checkSPApprovalNumber.recordcount eq 0>
		<cfquery name="SPApprovalNumber" datasource="#application.siteDataSource#">
			UPDATE opportunity
			SET SPApprovalNumber =  <cf_queryparam value="#SPApprovalNumber#" CFSQLTYPE="CF_SQL_VARCHAR" >
			WHERE opportunityID = #opportunityID#
		</cfquery>
	</cfif>
		<cfreturn checkSPApprovalNumber>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getSPApprovalNumber" hint="This returns the unique special price approval number for an opportunity.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfquery name="getSPApprovalNumber" datasource="#application.siteDataSource#" maxrows="1">
			select top 1 'SP-' + convert(varchar(25),[opportunityID]) + '-' + convert(varchar(25),[oppPricingStatusHistoryID]) as SPApprovalNumber
			from oppPricingStatusHistory
			WHERE oppPricingStatusID = 8
			 and opportunityID = #opportunityID#
			order by oppPricingStatusHistoryID desc
		</cfquery>

		<cfinvoke method="setSPApprovalNumber" returnvariable="">
			<cfinvokeArgument name="datasource" value="#dataSource#">
			<cfinvokeArgument name="opportunityID" value="#opportunityID#">
			<cfinvokeArgument name="SPApprovalNumber" value="#getSPApprovalNumber.SPApprovalNumber#">
		</cfinvoke>

		<cfreturn getSPApprovalNumber>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 	GCC 2004/09/23 added to enable bulk oppproduct updating
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="updateProbability" hint="This updates probability values for a list of opportunity products.">
		<cfargument name="oppProductID" type="string" required="true">
		<cfargument name="newProbability" type="numeric" required="true">
		<cfargument name="dataSource" type="string">
		<cfquery name="updateOppProductsProbability" datasource="#DataSource#">
			UPDATE opportunityProduct
			SET probability = #newProbability#
			where opportunityProductID  in ( <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		<cfreturn "Probability updated">
	</cffunction>

	<cffunction access="public" name="updateInteger" hint="This updates probability values for a list of opportunity products.">
		<cfargument name="oppProductID" type="string" required="true">
		<cfargument name="newInteger" type="numeric" required="true">
		<cfargument name="dataSource" type="string">
		<cfquery name="updateOppProductsInteger" datasource="#DataSource#">
			UPDATE opportunityProduct
			SET quantity = #newInteger#
			where opportunityProductID  in ( <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		<cfreturn "Quantity updated">
	</cffunction>

	<cffunction access="public" name="updateForecastShipDate" hint="This updates probability values for a list of opportunity products.">
		<cfargument name="oppProductID" type="string" required="true">
		<cfargument name="newDate" type="date" required="true">
		<cfargument name="dataSource" type="string">
		<cfquery name="updateOppProductsForecastShipDate" datasource="#DataSource#">
			UPDATE opportunityProduct
			SET forecastShipDate = #newDate#
			where opportunityProductID  in ( <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		<cfreturn "Forecast Shipping Date updated">
	</cffunction>

	<cffunction access="public" name="updateOppPricing" hint="This updates pricing values for a list of opportunity products based on the most recent price.">
		<cfargument name="oppProductID" type="string" required="true">
		<cfargument name="dataSource" type="string">

		<!--- BEGIN Loop throught the selected products --->
		<cfloop index="i" list="#oppProductID#">
			<!--- get the oppProduct data --->
			<cfquery name="qryGetOppProduct" datasource="#DataSource#" maxrows=1>
				SELECT * from opportunityProduct
				where opportunityProductID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" > and productOrGroup = 'P'
			</cfquery>
			<!--- get the product data --->
			<cfquery name="qryGetProduct" datasource="#DataSource#" maxrows=1>
				SELECT listprice from Product
				where ProductID =  <cf_queryparam value="#qryGetOppProduct.ProductOrGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<!--- BEGIN If the product listprice... is not the same as the opportunity
			 unit price then update including the subtotal --->
			<cfif qryGetOppProduct.unitprice neq qryGetProduct.listprice>

				<cfset variables.unitprice=qryGetProduct.listprice>
				<!--- BEGIN If the oppProduct has a special price--->
				<cfquery name="updateOppProductsForecastShipDate" datasource="#DataSource#">
					Update opportunityProduct
					set unitprice =  <cf_queryparam value="#variables.unitprice#" CFSQLTYPE="CF_SQL_VARCHAR" >
					Where opportunityProductID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			</cfif>
			<!--- END If the product listprice...--->
		</cfloop>
		<!--- END Loop throught the selected products --->
		<cfreturn "Pricing updated">
	</cffunction>

	<cffunction access="public" name="deleteOppProducts" hint="This removes products from the opportunity.">
		<cfargument name="oppProductID" type="string" required="true">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="opportunityID" type="numeric"> <!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 --->

		<!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 start --->
		<cfscript>
			var oppID=0;
			var getOpportunityID="";
			var updateOppProductsProbability="";
		</cfscript>

		<cfif not structKeyExists(arguments,"opportunityID")>
			<cfquery name="getOpportunityID" datasource="#dataSource#">
				select opportunityID from opportunityProduct where opportunityProductID  in ( <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>

			<cfif getOpportunityID.recordCount>
				<cfset oppID = getOpportunityID.opportunityID>
			</cfif>
		<cfelse>
			<cfset oppID = arguments.opportunityID>
		</cfif>
		<!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 end --->

		<cfquery name="updateOppProductsProbability" datasource="#DataSource#">
			Delete from opportunityProduct
			where opportunityProductID  in ( <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>

		<!--- START: 2009/11/11	AJC P-LEX039 Competitive products --->
		<cfquery name="deleteOppProductCompetitor" datasource="#application.siteDataSource#">
			delete from OppProductCompetitor
			where oppProductID =  <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<!--- END: 2009/11/11	AJC P-LEX039 Competitive products --->
		<!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 start --->
		<cfif oppID neq 0>
			<cfloop list="#oppID#" index="thisOppID">
				<!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 - reset approval when a product is deleted --->
				<cfinvoke component="relay.com.opportunity" method="specialPriceChange">
					<cfinvokeargument name="comments" value="Opportunity products deleted"/>
					<cfinvokeargument name="opportunityID" value="#thisOppID#"/>
				</cfinvoke>
			</cfloop>
		</cfif>
		<!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 end --->

		<cfreturn "Opportunity products deleted">
	</cffunction>

	<cffunction access="public" name="productCheck">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfscript>
			var qProductCheck = "";
			var productApprovalRequired = "false";
		</cfscript>
		<cfif structkeyexists(request,"specialPricingProductORGroupIDsRequiringAdditionalApproval")>
			<cfparam name="request.ProductORGroup" default="P">

				<cfquery name="qProductCheck" datasource="#application.siteDataSource#">
					SELECT     ProductORGroup,ProductORGroupID
					FROM         opportunityProduct INNER JOIN
                    Product ON opportunityProduct.ProductORGroupID = Product.ProductID
					WHERE     (OpportunityID = #arguments.opportunityID#)
					AND (opportunityProduct.ProductORGroup = 'P')
					<cfif request.ProductORGroup eq "P">
						AND ProductORGroupID  in ( <cf_queryparam value="#request.specialPricingProductORGroupIDsRequiringAdditionalApproval#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					<cfelse>
						AND (Product.ProductGroupID  IN ( <cf_queryparam value="#request.specialPricingProductORGroupIDsRequiringAdditionalApproval#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
					</cfif>
				</cfquery>
			<cfif qProductCheck.recordcount gt 0>
				<cfset productApprovalRequired = "true">
			</cfif>
		</cfif>
		<cfreturn productApprovalRequired>
	</cffunction>

	<cffunction name="getStatusCCemailaddresses" access="private">
		<cfargument name="sourceQuery" type="query" required="true">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfset var getPeople = "">
		<cfset var getEmails = "">
			<cfquery name="getPeople" dbtype="query">
				Select personID from arguments.sourceQuery
				where countryID in (#arguments.countryID#,0)
			</cfquery>
			<cfif getPeople.recordcount neq 0>
				<cfset personIDlist = valuelist(getpeople.personID)>
				<cfquery name="getEmails" datasource="#application.siteDataSource#">
					Select email from person
					where personID  in ( <cf_queryparam value="#personIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfquery>
				<cfif getEmails.recordcount gt 0>
					<cfreturn getEmails>
				</cfif>
			</cfif>
	</cffunction>


	<cffunction name="insertOppPricingStatusHistory" access="public" returntype="boolean">
		<cfargument name="opportunityID" required="yes" type="numeric">
		<cfargument name="oppPricingStatusID" required="yes" type="numeric">
		<cfargument name="personID" required="yes" type="numeric">
		<cfargument name="comments" required="no" default="" type="string">

		<cftry>
			<cfquery name="insertOppPricingStatusHistory" datasource="#application.siteDataSource#">
				INSERT INTO [oppPricingStatusHistory]([opportunityID], [oppPricingStatusID], [personid], [comments])
				VALUES(<cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.comments#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfquery>

			<cfcatch type="Any">
				<cfreturn false>
			</cfcatch>
		</cftry>

		<cfreturn true>
	</cffunction>

	<cffunction name="getMaxApproverLevel" access="public">
		<cfargument name="opportunityID" required="yes" type="numeric">
		<cfscript>
			var getOppCountryID = "";
			var qGetMaxApproverLevel = "";
		</cfscript>

		<cfquery name="getOppCountryID" datasource="#application.siteDataSource#">
			select countryID,sprequiredapprovallevel from opportunity where opportunityID = #arguments.opportunityID#
		</cfquery>

		<cfquery name="qGetMaxApproverLevel" datasource="#application.siteDataSource#">
			SELECT MAX([productapproverLevel]) as maxApproverLevel
				FROM [oppSPApprover] o
				INNER JOIN person p ON p.personID = o.personID
				where countryid in ( <cf_queryparam value="#getOppCountryID.countryid#" CFSQLTYPE="CF_SQL_INTEGER" >,0)
				and o.productapproverlevel in (1,2,3,4,5) <!--- NJH 2009/08/19 P-ATI005 - added 4,5 --->
		</cfquery>
		<cfreturn qGetMaxApproverLevel.maxApproverLevel>
	</cffunction>



	<!--- NJH 2010/03/26 - P-PAN002 --->
	<cffunction name="getOppPricingStatus" access="public" returntype="query">
		<cfargument name="oppPricingStatusID" type="numeric" required="false">

		<cfset var oppPricingStatus = "">

		<cfquery name="oppPricingStatus" dataSource="#application.siteDataSource#">
			select * from oppPricingStatus where 1=1
			<cfif structKeyExists(arguments,"oppPricingStatusID")>
				and oppPricingStatusID = #arguments.oppPricingStatusID#
			</cfif>
			order by oppPricingStatus
		</cfquery>

		<cfreturn oppPricingStatus>
	</cffunction>


	<cffunction name="getCountrySPQEmailRecipients" access="public" returntype="query">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="SPApproverLevel" type="numeric" default=0>

		<cfscript>
			var getCountryEmails = "";
		</cfscript>

		<cfquery name="getCountryEmails" datasource="#application.siteDataSource#">
			select p.email
				from flag f, integerMultipleFlagData iMFD, Person p
			where (f.flagtextid = 'SPQEmailCC'
				<cfif (arguments.SPApproverLevel EQ 1 OR arguments.SPApproverLevel EQ 2)>
					or f.flagtextid = 'SPQEmailCCApp1and2'
				</cfif>
				) and f.flagid = imfd.flagid
				and imfd.entityid =
					(select countryID from vLeadListingCalculatedBudget where opportunity_ID = #arguments.opportunityID#)and imfd.data = p.personid
		</cfquery>

		<cfreturn getCountryEmails>

	</cffunction>


	<cffunction name="getPeopleWithSPViewRights" access="public" returntype="query">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfscript>
			var viewRightsQry = "";
		</cfscript>

		<cfquery name="viewRightsQry" datasource="#application.siteDataSource#">
			select p.personid,email
					from usergroup ug, person p
					where ug.usergroupid IN (select usergroupid
					FROM RecordRights
					WHERE RecordID = #arguments.opportunityID# and entity='opportunity')
					and ug.personid = p.personid
		</cfquery>

		<cfreturn viewRightsQry>
	</cffunction>

	<!---
		function which will be run automatically when cfc is loaded into application.com
		needs to be able to run without application available
	 --->
	<cffunction access="public" name="initialise" hint="">
		<cfargument name="applicationScope" default = "#application#">
		<!--- Note that this is a global variable, do not var --->
		<cfset datasource = applicationScope.sitedatasource>
	</cffunction>


	<!--- START 2013-08-28 PPB Case 436679 --->
	<cffunction access="public" name="NullifyOppSpecialPrice" returntype="boolean">
		<cfargument name="opportunityID" type="numeric" required="false">

		<cfquery name="qryNullifyOppSpecialPrice" datasource="#application.siteDataSource#">
			UPDATE opportunityProduct
			SET specialPrice =  NULL
				,lastUpdated = getdate()
				,lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER">
			WHERE opportunityId = #arguments.opportunityID#
		</cfquery>

		<cfreturn true>
	</cffunction>
	<!--- END 2013-08-28 PPB Case 436679 --->


</cfcomponent>

