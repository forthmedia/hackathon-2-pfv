<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History:

2009-02-18 	NYB 	Sophos Stargate 2 - change getUserGroups function
2009/12/02	NJH		LHID 2896 - capitalise first letter of userGroupType and lower the rest for the userGroupForSelect query 
2011-03-07	NYB  	created getUserGroupAllocation
					rewrote GetUserGroups
2015-07/07	DXC		Case 445059 - PersonID not required for internal user group creation 					

--->

<cfcomponent displayname="RelayUserGroup" hint="Methods for dealing with usergroups">


	<cffunction access="public" name="GetUsersWithRightsToSpecifiedCountries" returnType="query" hint="returns query of users with rights to particular countries">
		<cfargument name="countryIDList" required="yes">
		<cfargument name="limitToValidUsers" default="yes">

		<cfset var getUsers = "">
	
		<CFQUERY NAME="getUsers" datasource="#application.siteDataSource#">
		SELECT DISTINCT ug.UserGroupID, p.FirstName + ' ' +  p.LastName as Name,p.FirstName, p.LastName
		 FROM 
			 UserGroup AS ug
				left Join 
			 Person AS p on p.PersonID = ug.PersonID 
				inner join 
			  Rights AS r on r.UserGroupID = ug.UserGroupID  and securityTypeID = 3   -- (recordTask which holds the countries)
		WHERE 
		  r.CountryID  IN ( <cf_queryparam value="#CountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )	
			<cfif limitToValidUsers>
			  AND	p.Active <> 0 <!--- true --->
			  AND p.Password <>  ''
			  AND p.PersonID <> #request.relayCurrentUser.personid#
			  AND p.LoginExpires > #CreateODBCDateTime(Now())#
			</cfif>
		ORDER BY p.FirstName, p.LastName
		</CFQUERY>
		<cfreturn getUsers>	
	</cffunction>

	


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		NYB 2011-03-07 - rewrote to use getUserGroupAllocation function
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="GetUserGroups" returnType="query" hint="returns query of user groups i.e not personal ones">
		<cfargument name="ShowAll" type="boolean" default="false">		
		<cfargument name="ID" type="numeric" required="false">		
		<cfargument name="Name" type="String" required="false">	
		<cfscript>
			var getUserGroups = "";
			var argumentCollectionVar = {IncludePersonUserGroups = arguments.ShowAll};
			if (structkeyexists(arguments,"ID")) {
				StructInsert(argumentCollectionVar,"UserGroupIDs",arguments.ID);
			}
			if (structkeyexists(arguments,"Name")) {
				StructInsert(argumentCollectionVar,"UserGroups",arguments.Name);
			}
			getUserGroups = getUserGroupAllocation(argumentCollection=argumentCollectionVar,SortOrder="Name");
		</cfscript>
		<cfreturn getUserGroups>	
	</cffunction>

	<cffunction access="public" name="convertUsergroupNamesToUsergroupIDs" returnType="String" hint="Takes a list of names of usergroups and returns a list of IDs. Any unrecognised names are ignored">
		<cfargument name="usergroupNameList" type="String" required="true">
		
		<cfquery name="getUserGroupsForSelectQry" datasource="#application.siteDataSource#">
			select usergroupID from usergroup
			where Name in (<cf_queryparam value="#arguments.usergroupNameList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			and personID is null
		</cfquery>
		
		<cfreturn valuelist(getUserGroupsForSelectQry.usergroupID)>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		NYB 2011-03-07 created
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	  <CFFUNCTION access="public" name="getUserGroupAllocation" returnType="query" hint="Details of Usergroups and People allocated to them.">
	   		<cfargument name="PersonID" type="numeric" required="false">
	   		<cfargument name="UserGroups" type="string" required="false">
	   		<cfargument name="UserGroupIDs" type="string" required="false">
	   		<cfargument name="IncludePersonUserGroups" type="boolean" default="false"> 
	   		<cfargument name="SortOrder" type="string" default="p.firstname asc,p.lastname asc,ug.Name asc"> 
			<cfscript>
			  var getUserGroups = "";
		    </cfscript>
			<CFQUERY NAME="getUserGroups" dataSource="#application.siteDataSource#">
			    SELECT p.PersonID,p.FirstName,p.LastName,p.FirstName+' '+p.LastName as FullName
				,ug.Name /*kept in for backwards compatability*/
				,ug.Name as UserGroupName,ug.UserGroupID 
			    FROM usergroup ug 
			    inner join RightsGroup rg on rg.UserGroupID=ug.UserGroupID
			    inner join Person p on rg.PersonID=p.PersonID 
			    <cfif structkeyexists(arguments,"PersonID")>
				    AND p.PersonID = <cfqueryparam value="#arguments.personid#" CFSQLType="CF_SQL_INTEGER" list="no"> 
				</cfif>
			    <cfif structkeyexists(arguments,"UserGroups")>
					AND ug.Name in (<cfqueryparam value="#arguments.UserGroups#" CFSQLType="CF_SQL_VARCHAR" list="yes">)  
				</cfif>
			    <cfif structkeyexists(arguments,"UserGroupIDs")>
					AND ug.UserGroupID in (<cfqueryparam value="#arguments.UserGroupIDs#" CFSQLType="CF_SQL_INTEGER" list="yes">)  
				</cfif>
			     <cfif not arguments.IncludePersonUserGroups>
			     	WHERE ug.personid is NULL 
				</cfif>
			     ORDER BY #arguments.SortOrder#
		   </CFQUERY>
		<cfreturn getUserGroups>
	  </CFFUNCTION>
  

	<!--- 	NJH 2009/02/09 P-SNY047 - created function to get the data to populate the first dropdown for ugRecordManager 
			NJH 2012/08/08 - added valid values = true so that it gets picked up as a valid values function
			WAB 2016-10-14 PROD2016-2405 Added additionalUserGroupIDsToInclude argument which overrides some of the other arguments.  
						Added to allow backwards compatibility when we removed personal usergroups from level1 permission dropdowns, but needed to make sure that existing data was still displayed and not lost
						Can also be used to keep the current user's usergroup available in permission dropdowns
	--->
	<cffunction access="public" name="getUserGroupsForSelect" returnType="query" validValues="true">
		<cfargument name="userGroupTypes" type="string" default="">
		<cfargument name="userGroupIDsToExclude" type="string" default="">
		<cfargument name="suppressGroups" type="boolean" default="false">
		<cfargument name="userGroupIDsToInclude" type="string" default="">
		<cfargument name="userGroupTypesToExclude" type="string" default="">
		<cfargument name="additionalUserGroupIDsToInclude" type="string" default="" hint="Used when userGroupTypesToExclude is set to ensure that any currently set values which might be in excluded Types are still displayed">

		<cfscript>
			var GetUserGroupsForSelectQry="";
		</cfscript>		

		<!--- NJH 2009/12/02 LHID 2896 - capitalise first letter of userGroupType and lower the rest --->
		<cfquery name="GetUserGroupsForSelectQry" datasource="#application.siteDataSource#">
			select 
			case 
				when userGroupType is null and personid is not null then 'Personal' 
				when userGroupType is null and personid is null  then 'Visibility' 
				else upper(left(userGroupType,1)) + lower(right(userGroupType,len(userGroupType)-1)) end
				as optGroup, 
			name, usergroupid 
				from usergroup with (noLock)
				<!--- NJH 2009/07/24 LID 2476 - only bring back people that are in the person table. (ie. haven't been deleted.) --->
				<!--- 2010/08/09 GCC LID3811 added filter so that 'ex' internal users that are now expired are no longer returned - loginexpires > getdate()--->
				where (personID is null or personID in (select personID from person with (noLock) where loginexpires > getdate()))

					<cfif arguments.userGroupIDsToExclude is not "">
						<!--- in twoSelectsComboQuery, an empty line is added to force the width of the select box. Here, we are removing
							the last character if it is a comma --->
						and usergroupid  NOT IN ( <cf_queryparam value="#reReplaceNoCase(arguments.userGroupIDsToExclude,",\Z","","ALL")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</cfif>

						and
					(

						<!--- WAB 2016-10-14 PROD2016-2405 Added additionalUserGroupIDsToInclude
								Not sure whether or not it should be allowed override all these options or not, but it does!
								Really only tested with userGroupTypesToExclude 
						 --->
						(
							1=1
	
							<cfif arguments.userGroupIDsToInclude neq "">
								and usergroupid IN ( <cf_queryparam value="#reReplaceNoCase(arguments.userGroupIDsToInclude,",\Z","","ALL")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
							</cfif>
							<cfif arguments.userGroupTypes neq "">
								<!--- NJH 2009/07/24 LID 2476 use listQualify instead of a replace.. --->
								and userGroupType  in ( <cf_queryparam value="#arguments.userGroupTypes#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
							</cfif>
							<cfif arguments.suppressGroups>
								and personid is not null
							</cfif>
							<cfif arguments.userGroupTypesToExclude neq "">
								and userGroupType not in ( <cf_queryparam value="#arguments.userGroupTypesToExclude#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
							</cfif>
						)
																
						<cfif arguments.additionalUserGroupIDsToInclude is not "">
						OR 	
							usergroupid IN ( <cf_queryparam value="#arguments.additionalUserGroupIDsToInclude#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
														
						</cfif>

					)	

				order by 
					case 
						when userGroupType is null and personid is not null then 'Personal' 
						when userGroupType is null and personid is null  then 'Visibility' 
						else upper(left(userGroupType,1)) + lower(right(userGroupType,len(userGroupType)-1)) end,
					name
		</cfquery>
	

		<cfreturn GetUserGroupsForSelectQry>
			
	</cffunction>
	
	<cffunction name="updatePersonsUserGroups" access="public"  hint="Updates (by default) all the usergroups a pesron has (excluding their personal usergroup). Adds/removes as required">
		<cfargument name="personID" type="numeric" required=true>
		<cfargument name="userGroupIDList" type="string" required="true" hint="A list of usergroups a person should have. Any usegroups not in this list are removed (see note on usergroups to manage)">
		<cfargument name="userGroupIDListToManage" type="string" required="false" hint="If passed only these usergroups are managed (i.e. added/removed based on the update list)">
	
		<cfset var createdBy=isDefined("request.relayCurrentUser.userGroupID")?request.relayCurrentUser.userGroupID:404>

		<cfset var updateUserGroupsQry="">

		<cfquery name="updateUserGroupsQry">
			delete rightsGroup 
			from rightsGroup
			join usergroup on rightsGroup.usergroupID=usergroup.usergroupID
			where 
			usergroup.usergroupType != 'Personal'
			and rightsGroup.personID=<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and rightsGroup.usergroupID not in (<cf_queryparam value="#userGroupIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
			
			<cfif structKeyExists(arguments,"userGroupIDListToManage")>
				and rightsGroup.usergroupID in (<cf_queryparam value="#userGroupIDListToManage#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
			</cfif>
			
			insert into rightsGroup (userGroupID,personID,createdBy,created,lastUpdatedBy,lastUpdated)
			select userGroup.userGroupID,<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#createdBy#" CFSQLTYPE="cf_sql_integer" >,getDate(),<cf_queryparam value="#createdBy#" CFSQLTYPE="cf_sql_integer" >,getDate()
			from userGroup
			where 
			usergroup.usergroupID in (<cf_queryparam value="#userGroupIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
			and usergroup.usergroupType != 'Personal'
			<cfif structKeyExists(arguments,"userGroupIDListToManage")>
				and usergroup.usergroupID in (<cf_queryparam value="#userGroupIDListToManage#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
			</cfif>
			and usergroup.usergroupID not in (select userGroupID from rightsGroup where personID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			
		</cfquery>
	
	</cffunction>
	
	
	<!--- NJH 2014/06/03 connector - add ability to pass in a tablename which stores the people to assign to a user group --->
	<cffunction name="addPersonToUserGroup" access="public">
		<cfargument name="userGroup" type="string" required="true">
		<cfargument name="personID" type="numeric" default=0>
		<cfargument name="tablename" type="string" default="">
		
		<cfset var addPersonToUserGroupQry = "">
		<cfset var personIDVal = arguments.personID>
		
		<cfif arguments.personID eq 0 and arguments.tablename eq "">
			<cfthrow message="Either a personID or a tablename containing a personID column must be passed in">
		</cfif>
		
		<cfset createdBy=isDefined("request.relayCurrentUser.userGroupID")?request.relayCurrentUser.userGroupID:404>
		
		<cfif arguments.personID neq 0>		
		<cfquery name="addPersonToUserGroupQry">
			insert into rightsGroup (userGroupID,personID,createdBy,created,lastUpdatedBy,lastUpdated)
			select u.userGroupID,<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#createdBy#" CFSQLTYPE="cf_sql_integer" >,getDate(),<cf_queryparam value="#createdBy#" CFSQLTYPE="cf_sql_integer" >,getDate()
				from userGroup u left join rightsGroup r on r.userGroupID = u.userGroupID and r.personID = #arguments.personID#
			where
				r.userGroupID is null and
				<cfif isNumeric(arguments.userGroup)>
					u.userGroupID=<cfqueryparam value="#trim(arguments.userGroup)#" cfsqltype="cf_sql_varchar">
				<cfelse>
					ltrim(rtrim(name))=<cfqueryparam value="#trim(arguments.userGroup)#" cfsqltype="cf_sql_varchar">
				</cfif> 
		</cfquery>
			
		<cfelse>
		
			<cfquery name="addPersonToUserGroupQry">
				insert into rightsGroup (userGroupID,personID,createdBy,created,lastUpdatedBy,lastUpdated)
				select u.userGroupID,p.personID,<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,getDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,getDate()
				from  #arguments.tablename# t 
					inner join person p on  p.personID = t.personID
					join userGroup u on
						<cfif isNumeric(arguments.userGroup)>
							userGroupID=<cfqueryparam value="#trim(arguments.userGroup)#" cfsqltype="cf_sql_varchar">
						<cfelse>
							ltrim(rtrim(name))=<cfqueryparam value="#trim(arguments.userGroup)#" cfsqltype="cf_sql_varchar">
						</cfif> 
				left join rightsGroup r on r.personID = p.personID
				where r.userGroupID is null 
			</cfquery>
		
		</cfif>
	</cffunction>
	
	
	<!--- NJH 2014/04/16 - case 439429 - function to create a usergroup --->
	<cffunction name="createUserGroup" access="public" output="false" hint="creates a usergroup if it doesn't exist" returnType="numeric">
		<cfargument name="name" type="string" required="true">
		<cfargument name="type" type="string" required="true">
		<cfargument name="personID" type="numeric" required="false">
		
		<cfset var insertUserGroup = "">
		
		<cfset var lastUpdatedBy=isDefined("request.relayCurrentUser.userGroupID")?request.relayCurrentUser.userGroupID:404>
		<cfset var lastUpdatedByPerson=isDefined("request.relayCurrentUser.personID")?request.relayCurrentUser.personID:404>
		
		<cfif structKeyExists(arguments,"personID")>
			<cfset arguments.type = "Personal">
		<cfelse>
			<cfif arguments.type eq "Personal"><!--- dxc case 445059 - changed Internal to Personal as per Will's instructions (see ticket for details) --->
				<cfthrow message="A personID is required if creating an personal usergroup">
			</cfif>
		</cfif>
		
		<cfquery name="insertUserGroup">
			if not exists (select 1 from userGroup 
				where 
				<cfif structKeyExists(arguments,"personID")>
					personID = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">
				<cfelse>
					name=<cf_queryparam value="#trim(arguments.name)#" cfsqltype="cf_sql_varchar"> and userGroupType=<cf_queryparam value="#trim(arguments.type)#" cfsqltype="cf_sql_varchar">
				</cfif>
				)
			insert into userGroup(<cfif structKeyExists(arguments,"personID")>personID,</cfif>UserGroupID,Name,CreatedBy,UserGroupType)
			values (<cfif structKeyExists(arguments,"personID")><cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">,</cfif>(select max(userGroupID)+1 from userGroup),<cf_queryparam value="#trim(arguments.name)#" cfsqltype="cf_sql_varchar" >,<cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,<cf_queryparam value="#trim(arguments.type)#" cfsqltype="cf_sql_varchar">)
			
			select userGroupID from userGroup
			where
				<cfif structKeyExists(arguments,"personID")>
					personID = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">
				<cfelse>
					name=<cf_queryparam value="#trim(arguments.name)#" cfsqltype="cf_sql_varchar"> and userGroupType=<cf_queryparam value="#trim(arguments.type)#" cfsqltype="cf_sql_varchar">
				</cfif>
		</cfquery>
		
		<cfif arguments.type eq "personal" and structKeyExists(arguments,"personID")><!--- dxc case 445059 - added: structKeyExists(arguments,"personID") to logic here--->
			<cfset var setRightsGroup = "">
			
			<cfquery name="setRightsGroup">
				insert into rightsGroup (PersonID, UserGroupID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
				values (<cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">,<cf_queryparam value="#insertUserGroup.userGroupID[1]#" cfsqltype="cf_sql_integer">,<cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,getDate(),<cf_queryparam value="#lastUpdatedBy#" cfsqltype="cf_sql_integer">,getDate())
			</cfquery>
		</cfif>
		
		<cfreturn insertUserGroup.userGroupID[1]>
	</cffunction>



	<cffunction name = "IsUserInUsergroup">
		<cfargument name="Usergroup" required="yes" type="string">
		<cfargument name="PersonID" type="numeric" default="#request.relaycurrentuser.personid#">
   		<cfargument name="IncludePersonUserGroups" type="boolean" default="true">
		<cfset var IntUsergroups = application.com.login.isPersonInOneOrMoreUserGroups(UserGroups=arguments.Usergroup,argumentCollection=arguments)>
		<cfreturn IntUsergroups>
	</cffunction>

	<cffunction name="getUsergroupsIdsFromNames" access="public" returntype="array" 
				hint="Returns an array of group IDs from the input list of group names">
		<cfargument name="groupNames" required="true" type="string">

		<cfscript>
			var idArray = arrayNew(1);
	        if (arguments.groupNames neq "") {
	            var queryService = new query();
	            queryService.setSQL("
	                SELECT
	                    UsergroupID
	                FROM
	                    usergroup
	                WHERE
	                    Name 
	                IN (:namesList)
	            ");
	            queryService.addParam(name="namesList", value=#arguments.groupNames#, CFSQLTYPE="CF_SQL_VARCHAR", list="true");
	            var queryResult=queryService.execute();
	            for (var row=1; row LTE queryResult.getResult().recordcount; row++) {
	                arrayAppend(idArray,queryResult.getResult().UsergroupID[row]);
	            }
	        }
	        return idArray;
		</cfscript>
		
	</cffunction>
</cfcomponent>
