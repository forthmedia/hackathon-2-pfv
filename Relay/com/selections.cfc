<!--- �Relayware. All Rights Reserved 2014 --->
<!--- File:
	relay\com\selections.cfm
 --->
<!---
WAB 2005-04-25  Mods to the refresh selection code
	2005-07-04	Mods to refresh selection code so that countries are always filtered by the owners countries (was doing the current users country - which doesn't work when a selection is shared and then refreshed)
	2005-07-07		WAB  added tiemout to the refresh() function.  added line to updated lastrefreshed date in the databse
	2007-02-05      WAB minor fix in selection refresh code
	2009/04/28  GCC		added a few missing with(nolock) comments for 'base' joins
	2010/04/14  GCC		LID3107 changed order by from s.title to title as SQL 2005 is fussier about correct aliasing
	2012-11-28 	PPB 	Case 432223 get selection owner for rights snippet in qryCreateSelection.cfm
	2012-12-06	WAB		Case 432390 Major Varing, especially for refreshSelection()
 --->

<cfcomponent displayname="selections" hint="Provides methods for managing selections.">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->


	<cffunction access="public" name="createSelection" returntype="numeric" hint="Creates a Selection ready for population and returns the selectionID.">
		<cfargument name="selectionTitle" type="string" required="yes">
		<cfargument name="selectionDescription" type="string" required="yes">
		<cfargument name="selectionCreatedBy" default="#request.relayCurrentUser.personid#" type="numeric">
		<cfargument name="runType" type="string" default="">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="FreezeDate" default="#Now()#">
		<cfargument name="refreshRate" default="A">    <!--- X - not refreshable, "N","Never" "A","Ad Hoc" "D","Daily" "W","Weekly" "M","Monthly") "X","Not Refreshable") --->
		<cfargument name="followEntityID" required="false" type="numeric">
		<cfargument name="followEntityTypeID" required="false" type="numeric">

		<cfset var getUserGroup = '' />
		<cfset var newSelectionInfo = '' />
		<cfset var insSelectGrps = '' />

			<!--- get Account Manager's usergroups(s) --->
			<CFQUERY NAME="getUserGroup" DATASOURCE="#DataSource#">
				select userGroupID
				from usergroup
				where personid = #selectionCreatedBy#
			</cfquery>

			<cfif getUserGroup.recordcount NEQ 0>

				<CFQUERY NAME="newSelectionInfo" DATASOURCE="#DataSource#">
					SET NOCOUNT ON
					INSERT INTO Selection
					(
						Title,
						RecordCount,
						RefreshRate,
						Description,
						TableName,
						CreatedBy,
						Created,
						LastUpdatedBy,
						LastUpdated
						<cfif structKeyExists(arguments,"followEntityID") and structKeyExists(arguments,"followEntityTypeID")>
						,followEntityID
						,followEntityTypeID
						</cfif>
					)
					VALUES
					(
						substring(N'#selectionTitle#',1,51),
						0,
						<cf_queryparam value="#arguments.refreshRate#" CFSQLTYPE="CF_SQL_CHAR" >,
						<cfif runtype EQ "test">
							substring('#selectionDescription#',1,100) + '_test',
						<cfelse>
							substring('#selectionDescription#',1,100),
						</cfif>
						'Person',
						#getUserGroup.usergroupID#,
						#FreezeDate#,
						#getUserGroup.usergroupID#,
						#FreezeDate#
						<cfif structKeyExists(arguments,"followEntityID") and structKeyExists(arguments,"followEntityTypeID")>
						,<cf_queryparam value="#arguments.followEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
						,<cf_queryparam value="#arguments.followEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfif>
					)
					SELECT scope_identity() as selectionID
					SET NOCOUNT OFF
				</CFQUERY>

			<CFQUERY NAME="insSelectGrps" DATASOURCE="#DataSource#">
				INSERT INTO SelectionGroup
				(
				SelectionID,
				UserGroupID,
				CreatedBy,
				Created,
				LastUpdatedBy,
				LastUpdated
				)
				values
				(
				<cf_queryparam value="#newSelectionInfo.selectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#getUserGroup.usergroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#getUserGroup.usergroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#getUserGroup.usergroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				)
			</CFQUERY>

			<cfreturn newSelectionInfo.selectionID>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>

	<cffunction access="public" name="deletePersonFromSelection" hint="Remove person from selection.">
		<cfargument name="selectionID" required="yes" type="numeric">
		<cfargument name="personID" required="yes" type="numeric">
		<cfargument name="dataSource" default="#application.SiteDataSource#">

		<cfquery name="local.qDelete" datasource="#dataSource#">
			DELETE
			FROM selectionTag
			WHERE selectionID = <cf_queryparam value="#arguments.selectionID#" CFSQLTYPE="CF_SQL_INTEGER">
			AND EntityID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfquery name="local.getPeopleCount" datasource="#DataSource#">
			SELECT Count(*) AS reccount
	  		FROM SelectionTag
	 		WHERE SelectionID = <cf_queryparam value="#arguments.selectionID#" CFSQLTYPE="CF_SQL_INTEGER">
	 		AND Status > 0
		</cfquery>

		<cfquery name="local.updatePeopleCount" datasource="#DataSource#">
			UPDATE Selection
			SET RecordCount =  <cf_queryparam value="#local.getPeopleCount.reccount#" CFSQLTYPE="CF_SQL_Integer"> ,
			selectedCount = (
				SELECT Count(EntityID)
		    	FROM SelectionTag AS st, Person AS p
	  	   	 	WHERE st.SelectionID = <cf_queryparam value="#arguments.selectionID#" CFSQLTYPE="CF_SQL_INTEGER">
		      	AND st.Status > 0
			  	AND st.EntityID = p.PersonID
			  	AND p.Active =1
			)
			WHERE selectionID = <cf_queryparam value="#arguments.selectionID#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn true>
	</cffunction>

	<cffunction access="public" name="isPersonInSelection" hint="Check if person is in this selection.">
		<cfargument name="selectionID" required="yes" type="numeric">
		<cfargument name="personID" required="yes" type="numeric">
		<cfargument name="dataSource" default="#application.SiteDataSource#">

		<cfset var returnVar = false>
		<cfquery name="local.qFind" datasource="#DataSource#">
			SELECT 1
	 		FROM selectionTag
			WHERE selectionID = <cf_queryparam value="#arguments.selectionID#" CFSQLTYPE="CF_SQL_INTEGER">
			AND EntityID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfif local.qFind.recordCount>
			<cfset returnVar = true>
		</cfif>
		<cfreturn returnVar>
	</cffunction>

	<cffunction access="public" name="insertSelectionPeople" hint="Populates a Selection with Person data from a query / list.">
		<cfargument name="selectionID" required="yes"  type="numeric">
		<cfargument name="personIDsForSelection" required="yes">
		<cfargument name="selectionCreatedBy" default="#request.relayCurrentUser.personid#" type="numeric">
		<cfargument name="dataSource" type="string" default="#application.siteDataSource#">
		<cfargument name="FreezeDate" default="#Now()#">

			<cfset var getUserGroup = '' />
			<cfset var insSelectionTag = '' />
			<cfset var getPeopleCount = '' />
			<cfset var updatePeopleCount = '' />

			<!---<CFQUERY NAME="getUserGroup" DATASOURCE="#DataSource#">
				select userGroupID
				from usergroup
				where personid = #selectionCreatedBy#
			</cfquery>--->

			<CFQUERY NAME="insSelectionTag" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO SelectionTag
				(
				SelectionID,
				EntityID,
				Status,
				CreatedBy,
				Created,
				LastUpdatedBy,
				LastUpdated,
				lastUpdatedByPerson
				)
				SELECT distinct
				<cf_queryparam value="#selectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				PersonID,
				1, <!--- checked --->
				<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- data for selection--->
				FROM person
				WHERE personid  in ( <cf_queryparam value="#personIDsForSelection#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				AND personid not in (
					select entityid from selectiontag where selectionid = #selectionID#
						)
			</CFQUERY>

			<!--- If the personids are given as a select statement then we can save the statement in the criteria table --->
			<cfif  personIDsForSelection contains "select">
				<cfset insertCriteria	(selectionid = #selectionid#,selectionField="frmPersonID",criteria="in(#personIDsForSelection#)")		>

			</cfif>


			<CFQUERY NAME="getPeopleCount" datasource="#DataSource#">
				SELECT Count(*) AS reccount
		  		FROM SelectionTag
		 		WHERE SelectionID = #selectionID#
		 		AND Status > 0 <!--- want to get status of 2 etc --->
			</CFQUERY>

			<CFQUERY NAME="updatePeopleCount" datasource="#DataSource#">
				Update Selection
					Set RecordCount =  <cf_queryparam value="#getPeopleCount.reccount#" CFSQLTYPE="CF_SQL_Integer" > ,
					selectedCount = (SELECT Count(EntityID)
				     FROM SelectionTag AS st, Person AS p
			  	    WHERE st.SelectionID = #selectionID#
				      AND st.Status > 0
					  AND st.EntityID = p.PersonID
					  AND p.Active =1
					)
				Where
					selectionID =#selectionID#
			</CFQUERY>
	</cffunction>

	<!---

		This function allows the 'simple' insertion of criteria into the selectionCriteria Table

	 --->
	<cffunction access="public" name="insertCriteria" hint="Defines a set of Criteria for a selection">
		<cfargument name="selectionID" required="yes"  type="numeric">
		<cfargument name="SelectionField" required="yes">
		<cfargument name="Criteria" required="yes">
		<cfargument name="createdBy" default="#request.relayCurrentUser.personid#"  type="numeric">
		<cfargument name="FreezeDate" default="#Now()#">

		<cfset var SaveCriteria = '' />
		<cfset var updateSelection = '' />

		<CFQUERY NAME="SaveCriteria" datasource="#application.siteDataSource#">
		INSERT INTO SelectionCriteria (SelectionID,SelectionField,Criteria, CreatedBy,Created,LastUpdatedBy,LastUpdated)
		 VALUES( <cf_queryparam value="#selectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,
	 			<cf_queryparam value="#SelectionField#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#Criteria#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#createdby#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#createdby#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
		</cfquery>

		<!--- if we are saving criteria then we should be able to assume that the selection is refreshable
			if it is set to x (not refreshable) the update to ad-hoc
		--->
		<CFQUERY NAME="updateSelection" datasource="#application.siteDataSource#">
		update selection set refreshRate = 'A' where selectionid = #selectionid# and refreshrate = 'X'
		</cfquery>



	</cffunction>


	<!---

		Get the set of selections available to the user.

	 --->
	 <!--- 2010/04/14 GCC - LID3107 changed order by from s.title to title as SQL 2005 is fussier about correct aliasing --->
	<cffunction access="public" name="getSelections" hint="returns the set of selections available to a person." returntype="query">
		<cfargument name="personID" default="#request.relayCurrentUser.personid#"  type="numeric">
		<cfargument name="orderby" default="Title">
		<cfargument name="searchString" default="">
		<cfargument name="exactMatch" type="boolean" default="false">
		<cfargument name="followEntityID" type="numeric" required="false">
		<cfargument name="followEntityTypeID" type="numeric" required="false">

		<cfset var getSelectionsQry = "">

		<CFQUERY NAME="getSelectionsQry" DATASOURCE="#application.SiteDataSource#">
			SELECT distinct s.SelectionID,
					s.RefreshRate,
					s.RecordCount as allPersons,
					s.RecordCount as records,
					s.RecordCount as theRecordCount,
					s.SelectedCount as tagPersons,
					s.SelectedCountLoc ,
					s.SelectedCountOrg ,
					ltrim(s.Title) as title,
					s.Description,
					s.TableName,
					s.Created AS datecreated,
					s.LastUpdated,
					s.CreatedBy AS owner,
					s.LastRefreshed,
					p.FirstName + ' ' + p.LastName as ownerName,
					convert(varchar,s.Created,106) + ' - ' + ltrim(s.Title) as dateTitle <!--- NJH  - used in FileEditV2 --->

		  	  FROM Selection AS s
			  	inner join SelectionGroup AS sg on sg.SelectionID = s.SelectionID
				inner join RightsGroup rg on sg.usergroupid = rg.usergroupid
				left join (UserGroup AS ug
						 	inner join
		  					Person AS p on ug.PersonID = p.PersonID
							) on s.createdby = ug.usergroupid
			 WHERE 1=1
			 <cfif arguments.searchString neq "">
			 	and s.title  like  <cfif arguments.exactMatch><cf_queryparam value="#arguments.searchString#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse><cf_queryparam value="%#arguments.searchString#%" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
			 </cfif>
			 <!--- NJH 2013/12/11 - if we're trying to find a 'Following' selection, then the personID is going to be a portal user who doesn't have rights. So, ignore that check in that case --->
			 <cfif structKeyExists(arguments,"followEntityID") and structKeyExists(arguments,"followEntityTypeID")>
				 and s.followEntityID = <cf_queryparam value="#arguments.followEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > and s.followEntityTypeID = <cf_queryparam value="#arguments.followEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				and rg.personID = #arguments.personID#
			 </cfif>
		   ORDER BY #arguments.orderBy#
		</CFQUERY>
		<cfreturn getSelectionsQry>
	</cffunction>

	<cffunction access="public" name="getSelectionPeople" hint="Returns the set of people within a selection">
		<cfargument name="selectionID" required="Yes"  type="numeric">
		<cfargument name="persinactive" required="No">
		<!--- WAB 2005-05-17 altered to make sure that uses rightsgroup table to work out if someone has rights to a selection (needed for global selections) --->

 		<cfset var ownerCountries = '' />
		<cfset var getHeadQry = '' />
		<cfset var getRecordsQry = '' />


		<CFQUERY NAME="getHeadQry" datasource="#application.siteDataSource#">
			SELECT  s.Title,
					s.Description,
					s.CreatedBy,
					ug.personid as OwnerPersonid
			<!--- 			s.RecordCount --->
				FROM Selection AS s
					left join usergroup ug on s.createdby = ug.usergroupid

					WHERE s.SelectionID = #arguments.selectionID#
			<!--- 	   AND s.CreatedBy = #request.relayCurrentUser.usergroupid# --->
		</CFQUERY>


		<!--- <cfinclude template="/templates/qrygetcountries.cfm">
		<!--- now Variables.CountryList ---> --->

		<!--- If the selection is a search criteria, then we need to limit
			the locations to the owner's countries as well --->
		<CFIF getHeadQry.CreatedBy IS NOT request.relayCurrentUser.usergroupid>

			<cfset ownerCountries = application.com.rights.getCountryRightsIDList (getHeadQry.OwnerPersonID)>
			<cfif ownerCountries  is ""><cfset ownerCountries = 0></cfif>

		</CFIF>

		<CFQUERY NAME="getRecordsQry" datasource="#application.siteDataSource#">
		SELECT  s.SelectedCount, p.PersonID, p.FirstName, o.OrganisationID, p.Email,
			p.LastName, l.SiteName, p.JobDesc,
		    (SELECT MAX(dateSent) FROM commdetail WHERE personID = p.personid) AS lastContactDate,
			l.Address1, l.Address2, l.Address3, l.Address4, l.Address5, st.Status, c.ISOCode
			FROM dbo.organisation o INNER JOIN
		         dbo.Location l ON o.OrganisationID = l.OrganisationID INNER JOIN
		         dbo.Selection s INNER JOIN
		         dbo.SelectionTag st ON s.SelectionID = st.SelectionID INNER JOIN
		         dbo.Person p ON st.EntityID = p.PersonID INNER JOIN
		         dbo.SelectionGroup sg ON s.SelectionID = sg.SelectionID inner join
				 rightsGroup rg on rg.usergroupid = sg.usergroupid
				 ON l.LocationID = p.LocationID INNER JOIN
		         dbo.Country c ON l.CountryID = c.CountryID
			WHERE s.SelectionID = #arguments.selectionID#
			  AND s.Tablename = 'Person'
			  AND
			  	<cfif structKeyExists(arguments,"persinactive")>
					p.Active <> -1
				<cfelse>
					p.Active <> 0
				</cfif>
				<!--- if person is active, then location & org must be ---->
			  <!--- any sharere can tag/untag --->
			  AND rg.personID =  <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			  <!--- but only show those people in the user's country --->
			  <!--- 2012-07-19 PPB P-SMA001	commented out
			  AND c.CountryID IN (#request.relayCurrentUser.countryList#)
			  --->
			  #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#	<!--- 2012-07-19 PPB P-SMA001 --->
			  <CFIF getHeadQry.CreatedBy IS NOT request.relayCurrentUser.usergroupid>
			  <!--- if the owner is not the user, limit to the owner's countries as well --->
			  	AND c.countryID  IN ( <cf_queryparam value="#Variables.OwnerCountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			  </CFIF>
			  <!--- 2001-10-28 SWJ Added alphaJump --->
			  <CFIF isDefined('form.frmAlphaJump') and form.frmAlphaJump neq "">
			  	AND left(SiteName,1) =  <cf_queryparam value="#form.frmAlphaJump#" CFSQLTYPE="CF_SQL_VARCHAR" >
			  </CFIF>
		   ORDER BY l.SiteName,p.LastName
		</CFQUERY>

		<cfreturn getRecordsQry>
	</cffunction>

	<cffunction access="public" name="refresh" hint="refreshes a selection">
		<cfargument name="selectionID" required="yes"  type="numeric">
		<cfargument name="timeout" default = "300">

			<cfset var Entities = '' />
			<cfset var Count = '' />
			<cfset var flagListPerson = '' />
			<cfset var flagListLocation = '' />
			<cfset var flagListOrganisation = '' />
			<cfset var FreezeDate = '' />
			<cfset var theSearchID = '' />
			<cfset var CountryList = '' />
			<cfset var UntaggedList = '' />
			<cfset var savedFields = '' />
			<cfset var savedCriteria = '' />
			<cfset var savedCriteria1 = '' />
			<cfset var message = '' />
			<cfset var theEntity = '' />
			<cfset var getCheckboxesperson = '' />
			<cfset var getCheckboxesLocation = '' />
			<cfset var getCheckboxesOrganisation = '' />
			<cfset var getTextPerson = '' />
			<cfset var getTextLocation = '' />
			<cfset var getTextOrganisation = '' />
			<cfset var getCountries = '' />
			<cfset var getUntagged = '' />
			<cfset var getCriteria = '' />
			<cfset var getEntities = '' />
			<cfset var delTags = '' />
			<cfset var refreshTags = '' />
			<cfset var getSelectionOwner = '' />
			<cfset var setUntagged = '' />
			<cfset var setSelectionHeader = '' />

		<!--- These are all items in the included file which I am going to var to be safe --->
		<cfset var crit = '' />
		<cfset var statusCrit = '' />
		<cfset var eventFlagTableJoins = '' />
		<cfset var delimiter = '' />
		<cfset var selectionField = '' />
		<cfset var criteria = '' />
		<cfset var flagType = '' />
		<cfset var flagGroupID = '' />
		<cfset var flagGroupStructure = '' />
		<cfset var joins = '' />
		<cfset var boolFlagList = '' />
		<cfset var textFlagList = '' />
		<cfset var dateFlagList = '' />
		<cfset var integerFlagList = '' />
		<cfset var integerMultipleFlagList = '' />
		<cfset var fother = '' />
		<cfset var entityID = '' />
		<cfset var entityFlagList = '' />
		<cfset var firstOpenParenthesis = '' />
		<cfset var firstClosedParenthesis = '' />
		<cfset var length = '' />
		<cfset var flagList = '' />
		<cfset var alias = '' />
		<cfset var crita = '' />
		<cfset var critb = '' />
		<cfset var critC = '' />
		<cfset var op = '' />
		<cfset var fbool = '' />
		<cfset var ftext = '' />
		<cfset var fdate = '' />
		<cfset var finteger = '' />
		<cfset var fintegerMD = '' />
		<cfset var i = '' />
		<cfset var entity = '' />
		<cfset var Flag = '' />
		<cfset var flagID = '' />
		<cfset var recordCounts = '' >
		<cfset var getCurrentSelection = '' >



		<CFSET var selectionOwnerPersonId = "">			<!--- 2012-11-28 PPB Case 432223 --->
		<cfset var frmSelectionID = selectionID>  <!--- bit of a hack - some of the included templates expect frmSelectionID --->

		<!--- timeout added WAB 2005-07-07 --->
		<cfsetting requesttimeout="#timeout#">


		<!---
		Any untagged entities in the original are untagged again
		if they appear in the refreshed selection.
		 --->

		<!--- Mods:
		WAB at some point I removed the hard coded query and included qryCreateSelection.cfm instead
		WAB 2000-04-28  now set a variable usergroupid before calling qryCreateSelection.cfm (used to define countries of selections owner)
		WAB 2000-05-08 added check for no criteria - refresh not done
		WAB 2004-09-21  Added counts on location and organisation to the selectionTable

		WAB 2005-02-28    moved to selection.cfc
		NJH	2008/10/22	using left joins in getEntities query rather than a 'not in'

		 --->


		<!--- set the entity types 0=person, 1=location --->
		<CFSET Entities="person,location,organisation">
		<CFSET Count=0>

		<!--- create the flaglists --->
		<CFLOOP LIST="#Entities#" INDEX="theEntity">
			<!--- this is for radio and checkboxes --->
			<CFQUERY NAME="local.getCheckboxes#theEntity#" datasource="#application.siteDataSource#">
				SELECT DISTINCT
						ft.Name +'_'+CONVERT(varchar(4),f.FlagGroupID) AS theFlag
				FROM	Flag as f, FlagGroup as fg, FlagType as ft
				WHERE	f.FlagGroupID=fg.FlagGroupID
				AND		(ft.FlagTypeID=2 OR ft.FlagTypeID=3) <!--- checkbox(2) or radio(3) --->
				AND		fg.FlagTypeID=ft.FlagTypeID
				AND		fg.EntityTypeID =  <cf_queryparam value="#Count#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<!--- this is for text type --->
			<CFQUERY NAME="local.getText#theEntity#" datasource="#application.siteDataSource#">
				SELECT	ft.Name+'_'+CONVERT(varchar(4),f.FlagID) AS theFlag
				FROM	Flag as f, FlagGroup as fg, FlagType as ft
				WHERE	f.FlagGroupID=fg.FlagGroupID
				AND		ft.FlagTypeID<>2
				AND		ft.FlagTypeID<>3
				AND		fg.FlagTypeID=ft.FlagTypeID
				AND		fg.EntityTypeID =  <cf_queryparam value="#Count#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
			<CFSET Count=Count+1>
		</CFLOOP>

		<CFSET flagListPerson=ValueList(local.getCheckboxesPerson.theFlag) & "," & ValueList(local.getTextPerson.theFlag)>
		<CFSET flagListLocation=ValueList(local.getCheckboxesLocation.theFlag) & "," & ValueList(local.getTextLocation.theFlag)>
		<CFSET flagListOrganisation=ValueList(local.getCheckboxesOrganisation.theFlag) & "," & ValueList(local.getTextOrganisation.theFlag)>

		<CFSET FreezeDate = request.requestTimeODBC>


		<!--- <CFSET theSearchID=SearchID> --->
			<!--- get countries that user has permissions for --->
				<CFQUERY NAME="getCountries" datasource="#application.siteDataSource#">
					SELECT r.CountryID,
				 		   Convert(bit,sum(r.permission & 2)) AS UpdateOkay,
						   Convert(bit,sum(r.permission & 4)) AS AddOkay
					  FROM Rights AS r,
						   RightsGroup AS rg
					 WHERE rg.PersonID = #request.relayCurrentUser.personid#
					   AND rg.UserGroupID = r.UserGroupID
					   AND r.Permission > 0
					   AND r.SecurityTypeID =
								(SELECT e.SecurityTypeID
								 FROM SecurityType AS e
								 WHERE e.ShortName = 'RecordTask')
					GROUP BY r.CountryID
				</CFQUERY>
				<CFSET CountryList = 0>
				<CFIF getCountries.RecordCount GT 0>
					<CFIF getCountries.CountryID IS NOT "">
						<!--- top record (or only record) is not null --->
						<CFSET CountryList = ValueList(getCountries.CountryID)>
					</CFIF>
				</CFIF>

			<!--- get currently untagged entities from selection --->
				<CFQUERY NAME="getUntagged" datasource="#application.siteDataSource#">
					SELECT EntityID
					FROM SelectionTag
					WHERE SelectionID = #selectionID# AND Status = 0
				</CFQUERY>
				<CFSET UntaggedList = 0>
				<CFIF getUntagged.RecordCount GT 0>
					<CFSET UntaggedList = ValueList(getUntagged.EntityID)>
				</CFIF>

			<!--- get search criteria for the current selection --->
				<CFQUERY NAME="getCriteria" datasource="#application.siteDataSource#">
					SELECT s.createdby, SelectionField, Criteria, criteria1
					FROM selection s inner join SelectionCriteria sc on s.selectionid = sc.selectionid
					WHERE s.SelectionID = #selectionID#
				</CFQUERY>


				<CFIF getCriteria.recordCount is not 0>

					<CFTRANSACTION>
					<!--- assume that criteria is never null or zero-length string --->
					<!--- change the delimiters of the lists to #application.delim1# --->
					<CFSET savedFields = ValueList(getCriteria.SelectionField,"#application.delim1#")>
					<CFSET savedCriteria = ValueList(getCriteria.Criteria,"#application.delim1#")>
					<CFSET savedCriteria1 = ValueList(getCriteria.Criteria1,"#application.delim1#")>

					<!--- WAB 2005-07-04
					This line below was wrong because we need to use the usergroupid of the person who owns the selection - not the person who might happen to refresh it
							<CFSET variables.usergroupid = request.relayCurrentUser.usergroupid> <!--- wab added, used for trying to convert the IN (countrylist) to a join --->
					 --->
						<CFSET variables.usergroupid = getCriteria.createdby> <!--- wab added, used for trying to convert the IN (countrylist) to a join --->


<!---


	<!--- WAB 2005-04-25
	refresh selection code was timing out
	Took a look at it and decided that there was no need to do this looping thing, we should just to
	a straight insert which will be much quicker
	 --->
			<!--- perform search based on the search retrieved from search criteria --->
					<CFQUERY NAME="getEntities" datasource="#application.siteDataSource#">
						SELECT DISTINCT a.PersonID

						<CFINCLUDE template="..\templates\qryCreateSelection.cfm">

					</CFQUERY>

				<!--- delete the old data --->
					<CFQUERY NAME="delTags" datasource="#application.siteDataSource#">
						DELETE FROM SelectionTag
						WHERE SelectionID = #selectionID#
					</CFQUERY>

				<!--- insert the new data --->
					<CFLOOP QUERY="getEntities">
						<CFQUERY NAME="refreshTags" datasource="#application.siteDataSource#">
							INSERT INTO SelectionTag
								(SelectionID,EntityID,Status,CreatedBy,Created,LastUpdatedBy,LastUpdated)
							 VALUES
							 	(#selectionID#,#PersonID#,1,#request.relayCurrentUser.personid#,#FreezeDate#,#request.relayCurrentUser.personid#,#FreezeDate#)
						</CFQUERY>
					</CFLOOP>
 --->
					<!--- delete the old data --->
					<CFQUERY NAME="delTags" datasource="#application.siteDataSource#">
						DELETE FROM SelectionTag
						WHERE SelectionID = #selectionID#
						and status <> 2    <!--- keep manually added people  Added WAB 2005-09-26 --->
					</CFQUERY>

					<!--- 2012-11-28 PPB Case 432223 START get selection owner for rights snippet in qryCreateSelection.cfm --->
					<CFQUERY NAME="getSelectionOwner" datasource="#application.siteDataSource#" >
					      SELECT ug.personId
					      from selection s with (nolock)
					            inner join
					            usergroup ug with (nolock) ON s.createdby = ug.usergroupid
					      where s.selectionid = #selectionID#
					</CFQUERY>

					<CFSET selectionOwnerPersonId = getSelectionOwner.personId>
					<!--- 2012-11-28 PPB Case 432223 END --->


					<!--- perform search based on the search retrieved from search criteria
							and insert into the selection tag table --->
<!--- WAB 2008/05/07 changed aliasing on main query - com/selections.cfc; templates/qryCreateSelection.cfm; selections/selectSum.cfm must be updated at same time --->

					<CFINCLUDE template="..\templates\qryCreateSelection.cfm">

					<CFQUERY NAME="getEntities" datasource="#application.siteDataSource#" timeout="600">
							INSERT INTO SelectionTag
							(SelectionID,EntityID,Status,CreatedBy,Created,LastUpdatedBy,LastUpdated)
						SELECT DISTINCT
							 <cf_queryparam value="#selectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,base.PersonID,1,<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >

							 FROM

							(SELECT p.personid

								#preserveSingleQuotes(selectionQrySnippet_sql)#

						<!--- exclude any people already manually added to the selection --->
						<!--- WAB 2007-02-05 added a. to fix a problem when qrycreateselection joined to another table with personid in it  --->
								<!--- NJH 2008/10/22 Lexmark Support Bug Fix Issue 1170 - use new selection query that uses joins --->
								<!--- GCC 2009/04/28 LID:2157 added a few missing with(nolock) comments --->
							) as base
						LEFT JOIN selectiontag st with (nolock) on st.entityID = base.personID and selectionID= #selectionID#
						WHERE st.selectionid is null

					</CFQUERY>


				<!--- untag any entities that were originally untagged --->
					<CFQUERY NAME="setUntagged" datasource="#application.siteDataSource#">
						UPDATE SelectionTag SET
						Status=0
						WHERE SelectionID = #selectionID#
						AND EntityID  IN ( <cf_queryparam value="#UntaggedList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</CFQUERY>


				<!--- update the selection header --->

					<!--- get all the record counts
					returns query recordCounts
					--->
					<cfinclude template = "..\templates\qrySelectionRecordCounts.cfm">

					<CFQUERY NAME="setSelectionHeader" datasource="#application.siteDataSource#">
						UPDATE Selection SET
							RecordCount =  <cf_queryparam value="#recordCounts.AllPeopleSelectedorUnselected#" CFSQLTYPE="CF_SQL_Integer" > ,
							SelectedCount =  <cf_queryparam value="#recordCounts.people#" CFSQLTYPE="CF_SQL_Integer" > ,
							SelectedCountLoc =  <cf_queryparam value="#recordCounts.locations#" CFSQLTYPE="CF_SQL_Integer" > ,
							SelectedCountOrg =  <cf_queryparam value="#recordCounts.organisations#" CFSQLTYPE="CF_SQL_Integer" > ,
							LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
							lastRefreshed =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >    <!--- added WAb 2005-07-07 --->
						WHERE SelectionID = #selectionID#
					</CFQUERY>
				</CFTRANSACTION>
				<CFSET message="Selection#iif(recordCounts.recordcount neq 1,'s','')# refreshed">
			<CFELSE>

				<CFSET message="Selection cannot be refreshed, there are no stored criteria">

			</CFIF>

			<cfreturn message>
			</cffunction>


<!---
getSelection
Just returns a query with a record from the selection table

 --->
	<cffunction access="public" name="getSelection" hint="returns record from selection table as a query" returnType = "Query">
		<cfargument name="selectionID" required="yes"  type="numeric">

			<cfset var getSelection = '' />

			<CFQUERY NAME="getSelection" datasource="#application.siteDataSource#">
			select * from selection where selectionid = #selectionid#
			</cfquery>

			<cfreturn getSelection>

	</cffunction>


	<cffunction name="refreshSelections" hint="Refreshes selections." returnType="struct">

		<cfset var getSelections = "">
		<cfset var result = {isOK=true,message=""}>
		<cfset var newMessage = "">
		<cfset var message = "">
		<cfset var errorMessage_html = "">

		<!--- WAB 2007/01/17 changed ordering so does ones that haven't been refreshed recently first and made it work even if not run on a sunday (when the weekly ones are supposed to be refreshed) --->
		<cfquery name="getSelections" datasource="#application.SiteDataSource#">
			declare @today SMALLDATETIME,	@firstdayofmonth SMALLDATETIME,	@lastsunday SMALLDATETIME,	@firstsundayofmonth SMALLDATETIME
			select @today = getdate()
			select @lastsunday = dateadd(d,-datepart(dw,@today)+1,@today)
			select @firstdayofmonth  = dateadd(d,-datepart(dd,@today)+1,@today)
			select @firstsundayofmonth = dateadd(d,7-datepart(dw,@firstdayofmonth)+1,@firstdayofmonth)

			SELECT
			 	s.SelectionID
			FROM
			 	Selection AS s
			WHERE
			   (
				   (s.RefreshRate = 'D' )    -- daily refresh
					    OR
				   (s.RefreshRate = 'W'
						AND    datediff(d,isnull(lastrefreshed,s.lastupdated),@lastsunday) > 0    -- weekly, last refreshed before last sunday
					)
						OR
				    (s.RefreshRate = 'M'
						 AND      datediff(d,isnull(lastrefreshed,s.lastupdated),@firstsundayofmonth) > 0   -- monthly, last refreshed before first sunday of month
					)
				)

			  and datediff(d,isnull(lastrefreshed,s.lastupdated),@today) > 0 -- not already done today

			ORDER BY
			CASE WHEN s.RefreshRate = 'M'  THEN 1 WHEN s.RefreshRate = 'w' THEN  2 ELSE 3 END,  -- monthly done first, so that in case of timeouts they will actually get done eventually
				lastrefreshed asc ,   -- leave most recent refreshed until last
				s.selectionid desc
		 </cfquery>

		<cfloop query="getSelections">
			<cfset newMessage = "">
			<cftry>
				<cfset message = application.com.selections.refresh(selectionID=getSelections.SelectionID,timeout=3600)>

				<cfcatch type="any">
					<cfsavecontent variable="errorMessage_html">
						<cfoutput>
							<h3>Error</h3>
							<cfif isdefined('getSelections.SelectionID')>
							<p>SelectionID : #htmleditformat(getSelections.SelectionID)#</p>
							</cfif>
						    <cfdump var="#cfcatch#">
						</cfoutput>
					</cfsavecontent>

					<cfmail type="HTML" to="Errors@foundation-network.com,Gawain@foundation-network.com" from="errors@#cgi.http_host#" subject="#cgi.http_host# Selection Refresh Error">
						#errorMessage_html#
					</cfmail>
					<cfif isdefined('getSelections.SelectionID')>
						<cfset newMessage = "#getSelections.SelectionID#:<BR>">
					</cfif>
				</cfcatch>
			</cftry>

			<cfif newMessage eq "">
				<cfset newMessage = "#getSelections.SelectionID#: #message# <BR>">
			</cfif>
			<cfset result.message = result.message & newMessage>
		</cfloop>

		<cfreturn result>
	</cffunction>

</cfcomponent>




