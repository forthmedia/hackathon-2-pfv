<!---
  --- tokens
  --- ------
  ---
  --- Methods to create and retrieve tokens
  ---
  --- author: Richard.Tingle
  --- date:   10/07/15
  --->
<cfcomponent hint="Base class for all token based entities" accessors="true" output="false" persistent="false">

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="token" type="string"  default="" />
		<cfargument name="tokenTypeTextID" type="string" required="true" />
		<cfargument name="personID" type="numeric" hint="The person for whom this token is generated" default=0 />
		<cfargument name="metadata" type="struct" hint="Any extra data to be kept with the token can be included here" default="#structnew()#" />
		<cfargument name="validity_seconds" type="numeric" hint="The tokens validity time" default="3600" />


		<cfscript>
			this.tokenTypeTextID=tokenTypeTextID;
			this.personID=personID;
			this.metadata=metadata;
			if (structkeyExists(arguments,"validity_seconds")){this.validity_seconds=validity_seconds;}
			if (structkeyExists(arguments,"token")){
				this.token=token;
			}
			if (NOT structkeyExists(arguments,"token") OR token EQ ""){
				autogenerateToken(); //create a token string etc
			}

			this.isValid=false; //not valid unless successfully pulled from database

		</cfscript>
		<cfreturn this>
	</cffunction>

	<cffunction name="setPersonID" access="public" output="false" returntype="void">
		<cfargument name="personID" type="string" hint="The person for whom this token is generated" required="true" />
		<cfset this.personID=Val(arguments.personID)>
	</cffunction>

	<cffunction name="getPersonID" access="public" output="false" returntype="numeric">
		<cfreturn this.personID>
	</cffunction>

	<cffunction name="set" access="public" output="false" returntype="void">
		<cfargument name="key" type="string" required="true" />
		<cfargument name="value" type="any" required="true" />
		<cfset this.metadata[key]=value>
	</cffunction>
	<cffunction name="get" access="public" output="false" returntype="any">
		<cfargument name="key" type="string" required="true" />
		<cfreturn this.metadata[key]>
	</cffunction>


	<cffunction name="persistToDatabase" access="public"  output="false" returntype="void">
		<CFQUERY NAME="pushtokenData" datasource="#application.siteDataSource#">

			 insert into token (tokenType,token,personID, metadata,created,expiryDate)
			 values(

			    <cf_queryparam value="#this.tokenTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			    <cf_queryparam value="#this.token#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			    <cf_queryparam value="#this.personID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			    <cf_queryparam value="#SerializeJSON(this.metadata)#" CFSQLTYPE="CF_SQL_LONGVARCHAR" > ,
			    <cf_queryparam value="#CreateODBCDateTime(this.created)#" CFSQLTYPE="CF_SQL_timestamp" > ,
			    <cf_queryparam value="#CreateODBCDateTime(this.expiryDate)#" CFSQLTYPE="CF_SQL_timestamp" >
			    )

		</CFQUERY>

	</cffunction>

	<cffunction name="pullFromDatabase" access="public"  output="false" returntype="boolean" hint="returns if successfully pulled from database">
		<cfset var isOk=true>
		<CFQUERY NAME="tokenData" datasource="#application.siteDataSource#">
			SELECT
			    tokenID,
				tokenType,
				token,
				personID,
				metadata,
				created,
				expiryDate
			FROM
				token
			WHERE
				tokenType =  <cf_queryparam value="#this.tokenTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				AND token=<cf_queryparam value="#this.token#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>


		<cfscript>
			local.returnStruct=structNew();


			if (tokenData.recordCount != 1){
				isOk=false;
				return false;
			}else{
				isOk=true;

			}

		</cfscript>
		<cfif isOk>
			<CFLOOP QUERY="tokenData">
				<cfscript>


					if (DateCompare(tokenData.expiryDate, now()) EQ 1){
						this.tokenID=tokenData.tokenID;
						this.tokenTypeTextID=tokenData.tokenType;
						this.token=tokenData.token;
						setPersonID(tokenData.personID);
						this.metadata=DeserializeJSON(tokenData.metadata);
						this.created=tokenData.created;
						this.expiryDate=tokenData.expiryDate;
						this.isValid=true;
					}else{
						this.isValid=false;
						return false;
					}

				</cfscript>

			</CFLOOP>
		</cfif>
		<cfreturn true>

	</cffunction>

	<cffunction name="getTokenValue" access="public" output="false" returntype="string">

		<cfscript>
			return this.token;

		</cfscript>


	</cffunction>

	<cffunction name="getExpiryDate" access="public" output="false" returntype="string">

		<cfscript>
			return this.expiryDate;

		</cfscript>


	</cffunction>

	<cffunction name="isValidToken" access="public" output="false" returntype="string">

		<cfscript>
			return this.isValid;

		</cfscript>


	</cffunction>


	<cffunction name="autogenerateToken" hint="Creates a new token of the specifed type and returns it's details" access="public" output="false" returntype="void">
		<cfargument name="permittedcharacters" type="string" default="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890" />
		<cfargument name="length" type="numeric" default=56 />



		<cfscript>
			//secure random is a crytographically safe random number generator
			var secureRand = CreateObject("java", "java.security.SecureRandom").init();
			var i=1;
			var character="";
			var unique=false;
			while(!unique){
				this.token="";
				for(i=1;i<=length;i++){
					character=Mid( permittedcharacters,secureRand.nextInt(len(permittedcharacters))+1, 1 );
					this.token=this.token & character;
				}
				unique=!currentlyExistsInDatabase();
			}

			this.created=now();
			this.expiryDate=DateAdd("s",this.validity_seconds,this.created);
		</cfscript>



	</cffunction>


	<cffunction name="deleteToken" hint="Deletes the token from the database. Returns if anything was deleted" access="public" output="false" returntype="boolean">
		<CFQUERY NAME="tokenDelete" datasource="#application.siteDataSource#" result="tokenDeleteData">
			delete from token
			where
			tokenType=<cf_queryparam value="#this.tokenTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and
			token=<cf_queryparam value="#this.token#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>
		<cfreturn tokenDeleteData.RECORDCOUNT>
	</cffunction>
	
	<cffunction name="invalidateToken" hint="Sets the token validity to be in the past, effectively invalidating it" access="public" output="false" returntype="void">
		<CFQUERY NAME="invalidateToken" datasource="#application.siteDataSource#" result="tokenDeleteData">
			update token
			set expiryDate=DateAdd("s",-1,GETDATE())
			where
			tokenType=<cf_queryparam value="#this.tokenTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and
			token=<cf_queryparam value="#this.token#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>
	</cffunction>
	

	<cffunction name="currentlyExistsInDatabase" hint="Checks if a token with this type and value currently exists, largly used to ensure uniqueness" access="public" output="false" returntype="boolean">
		<CFQUERY NAME="tokenData" datasource="#application.siteDataSource#">
			SELECT
			    1
			FROM
				token
			WHERE
				tokenType =  <cf_queryparam value="#this.tokenTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				AND token=<cf_queryparam value="#this.token#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>

		<cfreturn tokenData.recordCount GTE 1>
	</cffunction>


</cfcomponent>