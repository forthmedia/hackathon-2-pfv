<!---
  --- oneTimeUseToken
  --- ---------------
  --- 
  --- author: Richard.Tingle
  --- date:   15/07/15
  --->
<cfcomponent extends="token" accessors="true" output="false" persistent="false">

	<cffunction name="pullFromDatabase" access="public"  output="false" returntype="boolean" hint="returns if successfully pulled from database">
		
		<cfscript>
			var success=super.pullFromDatabase();
			if (success){
				invalidateToken();
			}
			return success;
		</cfscript>
	</cffunction>

</cfcomponent>