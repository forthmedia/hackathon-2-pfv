<!--- �Relayware. All Rights Reserved 2014 --->

<!--- sourceImagePath - path that the image will be created from

thumbnailPath - default userfiles/content/miscImages/

sizeInPixels - max height or width 

Mods 
WAB  Dec 2005 		Added error handling for corrupted images, fixed MX6/7 problem with variable names, added a regular expression and changed some variable names
2014-01-29 	NYB 	Case 438443 added a changed to the getThumbnail function: if errors then try using standard CF tags/functions to process, then record the cfcatch in relayerrors - don't just send out an email
2017-01-10	DAN		PROD2016-2880 - fix for thumnailing secure files on display, also support for TIFFs and BMPs

--->

<cfcomponent displayname="relayThumbnailDisplay" hint="Displays or creates and displays a standard thumbnail image in the location specified.">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 getThumbnail - must be passed a relative URL path to the source image somewhere in the content directory structure
 e.g. /content/miscimages/ipelalogo.jpg                                                                                                                           
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getThumbnail" hint="Saves a resized png of the source image">
	
		<cfargument name="sourceImageURL" type="string" required="yes" hint="relative path to the source image">
		<cfargument name="thumbnailURLPath" type="string" default="/content/thumbnails/" hint="relative path to the target image">
		<cfargument name="thumbnailName" type="string" default="" hint="optional new name for the target image">
		<cfargument name="size" type="numeric" default="80" hint="pixel size the image will be drawn into preserving original ratios">
		<cfargument name="flush" type="boolean" default="0" hint="set to true to force a thumbnail redraw after a size change">
		<cfargument name="preserveSourceDirStructure" type="boolean" default="true" hint="set to false to put all target images into the thumbnailURLPath directory">
		<cfargument name="overrideResize" type="boolean" default="false" hint="set to true to stop resizing of image and only have it save png in specified location">
		<cfargument name="secure" type="boolean" default="false" hint="is the image file secure?">

		<cfset var SourceImage = structNew()>
		<cfset var ThumbnailImage = structNew()>

		<cfset ThumbnailImage.name = arguments.thumbnailName>
		<cfset ThumbnailImage.ext = "png">
		
		<cfset sourceImage = application.com.regExp.getFileDetailsFromPath (arguments.sourceImageURL)>
		<cfset var contentPath = arguments.secure ? application.paths.secureContent : application.paths.content>
		<cfset var sourceImageAbsolutePath = contentPath & sourceImage.path>
		<cfset var sourceImageFullAbsolutePath = "">

		<!--- 
		replace with regExp above
		
		<!--- last "." delimited token --->
		<cfset sourceImageExtension = ListLast(arguments.sourceImageURL,".")>
		<!--- remove ".extension" --->
		<cfset sourceImageRestOfURL = replace(arguments.sourceImageURL,"." & sourceImageExtension,"","one")>
		<!--- image name is now the last "/" delimited token --->
		<cfset sourceImageName = ListLast(sourceImageRestOfURL,"/")>
		<!--- path is what is left - this is safer than replacing a word - image and directory with same name would cause problems otherwise --->
		<cfset sourceImageURLPath = left(sourceImageRestOfURL,len(sourceImageRestOfURL)-len(sourceImageName))>
		<!--- default the thumbnail name to the source image name --->
		 --->

		<cfif ThumbnailImage.name eq "">
			<cfset thumbnailImage.Name = sourceImage.Name>
		</cfif>
		<!--- debug --->
		<!--- <cfoutput>
		#sourceImageURL#<br>
		#sourceImageName#<br>
		#sourceImageExtension#<br>
		</cfoutput> --->
		
		<cfif arguments.preserveSourceDirStructure>
			<!--- relative directory path --->
			<cfset ThumbnailImage.FullURLPath = arguments.thumbnailURLPath & sourceImage.Path>
			<!--- conversion to absolute --->
			<cfset ThumbnailImage.FullAbsolutePath = application.userfilesabsolutepath & replace(arguments.thumbnailURLPath & sourceImage.Path,"/","\","ALL")>
		<cfelse>
			<!--- relative directory path --->
			<cfset ThumbnailImage.FullURLPath = arguments.thumbnailURLPath>
			<!--- conversion to absolute --->
			<cfset ThumbnailImage.FullAbsolutePath = application.userfilesabsolutepath & replace(arguments.thumbnailURLPath,"/","\","ALL")>
		</cfif>
		<!--- create thumbnail directory if it doesn't exist --->
		<cfif not directoryexists(ThumbnailImage.FullAbsolutePath)>
			<cfdirectory action="CREATE" directory="#ThumbnailImage.FullAbsolutePath#">
		</cfif>		

		<cfset ThumbnailImage.fullFileName = ThumbnailImage.Name & "." & ThumbnailImage.ext>
		
		<cfif (not fileexists(ThumbnailImage.FullAbsolutePath & ThumbnailImage.fullFileName)) or arguments.flush eq 1>
		
			<cfset nImage = application.com.relayImage.getImageComponent()>
			
			<cfset imageURL = request.currentSite.httpprotocol & cgi.SERVER_NAME & arguments.sourceImageURL>
			
			<cfif sourceImage.extension neq ThumbnailImage.ext>
				<!--- convert original image to thumbnail ext format and move it to the thumbnail dest --->
				<cfset convertedImage = application.com.relayImage.convertImage(
								sourcePath=sourceImageAbsolutePath,
								sourceName=sourceImage.fullFileName,
								targetPath=ThumbnailImage.FullAbsolutePath,
								targetName=ThumbnailImage.fullFileName
								)>
				<cfset sourceImageFullAbsolutePath = ThumbnailImage.FullAbsolutePath & ThumbnailImage.fullFileName> <!--- already converted into the correct place --->
			<cfelse>
				<cfset sourceImageFullAbsolutePath = sourceImageAbsolutePath & ThumbnailImage.fullFileName>
			</cfif>

			<cftry>

				<cfscript>
					nImage.readimage(sourceImageFullAbsolutePath);
					if (not arguments.overrideResize) {
						writeimage = 0;
						//nImage.blur(nImage.getWidth()/350);
						if (nImage.getWidth()GT nImage.getHeight()){
							if (nImage.getWidth() neq arguments.size) {
								nImage.scaleWidth(arguments.size);
								writeimage = 1;  
							}
						} else {
							if (nImage.getHeight() neq arguments.size) {
								nImage.scaleHeight(arguments.size);
								writeimage = 1; 
							}
						}
					} else {
						writeimage = 1; 
					}
					
					if (writeimage eq 1) { 
						nImage.writeImage(ThumbnailImage.FullAbsolutePath & ThumbnailImage.fullFileName, ThumbnailImage.ext);
					}
				</cfscript>

				<cfcatch>
					<!--- START: 2014-01-29 NYB Case 438443 added: if errors then try using standard CF tags/functions to process, then record the cfcatch in relayerrors - don't just send out an email --->
					<cftry>
		
						<cfimage
						    name = "nImage"
						    source = "#sourceImageFullAbsolutePath#"
						    action = "read">
		
		
						<cfscript>
							if (not arguments.overrideResize) {
								writeimage = 0;
								if (ImageGetWidth(nImage) GT ImageGetHeight(nImage)){
									if (ImageGetWidth(nImage) neq arguments.size) {
										ImageResize(nImage,arguments.size,"");
										writeimage = 1;  
									}
								} else {
									if (ImageGetHeight(nImage) neq arguments.size) {
										ImageResize(nImage,"",arguments.size);
										writeimage = 1; 
									}
								}
							} else {
								writeimage = 1; 
							}
							
							if (writeimage eq 1) { 
								ImageWrite(nImage,ThumbnailImage.FullAbsolutePath & ThumbnailImage.fullFileName);
							}
						</cfscript>	
		
						<cfcatch>
		
							<cfif application.testsite eq 2>
								<cfdump var="#cfcatch#">
							</cfif>
							
							<!--- if there is a problem, we send an email and continue
								currently will display a blank image
								
								NJH 2007/01/10 changed the email address from  #request.relayCurrentUser.SupportEmailAddress# to #application.cBccEmail#
								as support were getting a lot of emails in their inbox
							 --->
							 	<cfset application.com.errorHandler.recordRelayError_Warning(catch=cfcatch)>
								<CFMAIL 
									TO="#application.cBccEmail#"  
									FROM="errors@#cgi.http_host#" 
									SUBJECT="Image Error #sourceImage.name#.#sourceImage.extension# on #cgi.http_host#"
									type="html">
										<cfoutput>
											There was a problem trying to create a thumbnail for  #imageURL# <BR>
											Page #cgi.script_name#?#request.query_string# <BR>
											Please investigate! <BR> 
											Error message is: #cfcatch.message# <BR>
										</cfoutput>
								</cfmail>
							
						</cfcatch>
					</cftry>
					<!--- END: 2014-01-29 NYB Case 438443  --->
					
				</cfcatch>
			</cftry>

		</cfif>
		<cfset thumbnail = ThumbnailImage.FullURLPath & ThumbnailImage.fullFileName>
		<!--- return thumbnail URL --->
		<cfreturn thumbnail>
	</cffunction>
</cfcomponent>

