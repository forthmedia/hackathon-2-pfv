﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
WAB 2006-04-11  getWeekDetails and getTransactionItemsPerWeek altered to get pick up transactions up to the end of the day defined in arguments.weekending
AJC 2008-09-23 CR-SNY567 Created function to get all products for a promotion
SSS 2009/02/18 CR-LEX592 select RwaccountID from insert query get rid of transaction tags
NJH 2009/03/05 CR-SNY671 added new function to get invoice details
NJH 2009/06/24 Bug Fix Sony Issue 2395 - Changed variable name being looked at from nonGlobalCatalogueCountryList to singleCountryCatalogueCountryList so that the getProductAndGroupList
				and getProductAndGroupAndCatList functions are using the same. request.nonGlobalCatalogueCountryList did not exist in the code. Also added a new argument called accrualMethod
				which restricts the products to a particular method if the argument is passed in.
NJH	2009/07/03	P-FNL069 - Added a check in function that returns the invoice document details to see if the current person has rights to have access to them.
				Also created function to determine whether the current user had rights to the rwTransaction. Currently, you have rights if you submitted the claim or if you are an internal user.
NJH 2009/12/15	LHID 2320 - include expired points as part of spent points in the getWeekDetails function.
NYB 2011/01/14  replaced ProductDescription with 'if desc has a len of 0 (or is null) then make it equal a space'
				because no desc was causing it to not be added to the ValueList which was then causing a indexing error in RWPromotion_Edit.cfm
NYB 2011-12-13  LHID8108 changed text in popup to something more legible
2015-08-19	AHL	Updating the deprected table Promotion to RWPromotion
2015-09-03	AHL Making dates consistents (Query Change)
2015-09-03	AHL using thPointsToAdd rather pointsToAdd
2015-11-16  DAN PROD2015-364 - products country restrictions
2016-01-05	WAB	PROD2015-290 Replace calls to sql function fn_getCountryRights with getCountryScopeList
2016-01-13	AHL Case 447535 Filtering Incentive Statement by Organisation
 --->
<cfcomponent displayname="Relay Incentive Manager" hint="Methods for managing Relay Incentive module">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="RWAccruePoints" hint="Adds points to an RW Account">
		<cfargument name="AccountId" type="numeric" required="true">
		<cfargument name="DistiOrgID" type="numeric" default="0">
		<cfargument name="AccruedDate" type="date" required="yes">
		<cfargument name="PointsAmount" default="0" type="numeric">
		<cfargument name="CashAmount" default="">
		<cfargument name="PriceISOCode" type="string" default="">
		<cfargument name="PersonID" type="numeric" default="#request.relayCurrentUser.personid#">
		<cfargument name="RWTransactionType" type="string" default="AC">
		<cfargument name="pointsType" type="string" default="BP">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">  <!--- NJH 2007/09/07 --->

		<cfset var PointsAccrued="">
		<cfset var LatestTransactionID="">
		<cfset var pointsExpiryDate = "">

		<!--- NJH 2007/03/30 setting the points expiry date. Set the date span in incentiveINI --->
		<!--- <cfif isDefined("request.expirePointsDateSpan") and listLen(request.expirePointsDateSpan,"/")> --->
		<cfif application.com.settings.getSetting("incentive.expirePointsDateSpan") neq "">
			<cfset pointsExpiryDate = now()>
			<!--- add num of years to expiry date --->
			<cfset pointsExpiryDate = DateAdd("yyyy",listGetAt(request.expirePointsDateSpan,1,"/"),pointsExpiryDate)>
			<!--- add num of months to expiry date --->
			<cfset pointsExpiryDate = DateAdd("m",listGetAt(request.expirePointsDateSpan,2,"/"),pointsExpiryDate)>
			<!--- add num of weeks to expiry date --->
			<cfset pointsExpiryDate = DateAdd("ww",listGetAt(request.expirePointsDateSpan,3,"/"),pointsExpiryDate)>
			<!--- add num of days to expiry date --->
			<cfset pointsExpiryDate = DateAdd("d",listGetAt(request.expirePointsDateSpan,4,"/"),pointsExpiryDate)>
		</cfif>

		<cftransaction>

		<cfquery name="PointsAccrued" DATASOURCE="#application.SiteDataSource#">
			exec RWAccruePoints @AccountID = #arguments.accountID#, @RWTransactionTypeID =  <cf_queryparam value="#arguments.RWTransactionType#" CFSQLTYPE="CF_SQL_VARCHAR" > , @AccruedDate = #arguments.AccruedDate#, @PointsAmount = #arguments.PointsAmount#, @PersonID = #PersonID#, <cfif arguments.DistiOrgID>@DistiOrgID = #DistiOrgID#,</cfif> @Type =  <cf_queryparam value="#pointstype#" CFSQLTYPE="CF_SQL_VARCHAR" >  <cfif isDate(pointsExpiryDate)>,@expiryDate=#pointsExpiryDate#</cfif>, @CreatedPersonID=#createdBy#
		</cfquery>

		<cfquery name="LatestTransactionID" DATASOURCE="#application.SiteDataSource#">
			SELECT max(RWTransactionID) as maxTransID
			FROM RWTransaction
		</cfquery>

		</cftransaction>

		<cfreturn LatestTransactionID.maxTransID>
	</cffunction>

	<!---  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		Returns the status of the transaction.

	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getRWTransactionType" hint="Adds points to an RW Account" >
		<cfargument name="RWTransactionID" type="numeric" required="yes">

		<cfset var getTransTypeID = "">

		<cfquery name="getTransTypeID" DATASOURCE="#application.SiteDataSource#">
			SELECT RWTransactionTypeID
			FROM RWTransaction
			WHERE RWTransactionID = #arguments.RWTransactionID#
		</cfquery>

		<cfreturn getTransTypeID.RWTransactionTypeID>
	</cffunction>
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkDuplicateLineItem" hint="Checks whether a given productID is already inside the claim" >
		<cfargument name="RWTransactionID" type="numeric" required="yes">
		<cfargument name="productID" type="string" required="yes">

		<cfset var checkDuplicate = "">

		<cfquery name="checkDuplicate" DATASOURCE="#application.SiteDataSource#">
			SELECT *
			FROM RWTransactionItems
			WHERE RWTransactionID = #arguments.RWTransactionID#
			AND productID =  <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn checkDuplicate.recordCount>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="AddRWTransactionItems" returnType="numeric" hint="Adds line items to RWTransactionItems">

		<cfargument name="RWTransactionID" type="numeric" required="true">
		<cfargument name="ItemID" type="numeric" required="true">
		<cfargument name="quantity" type="numeric" required="true">
		<cfargument name="points" type="numeric" required="false">
		<cfargument name="distiID" type="numeric" default="0">
		<cfargument name="serialNo" type="string" default="">
		<cfargument name="invoiceNo" type="string" default="">
		<cfargument name="invoiceDate" type="string" default="">
		<cfargument name="endUserCompanyName" type="string" default="">
		<cfargument name="pointsType" type="string" default="BP">
		<cfargument name="RWPromotionID" type="numeric" default="0">
		<cfargument name="NoOfUsers" type="string" default=""> <!--- P-TND065 NJH 2008/03/05  --->
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var AddItems="">
		<cfset var getPointsValue = "">
		<cfset var pointsValue = 0>

		<!--- NJH 2008/03/05 P-TND065 return the rwTransactionItemsID of the item just added, so that it ties the rwTransactionItem
			so the transaction in SODMaster --->

		<!--- NJH 2008/03/05 P-TND065 A null value can be passed, so check if noOfUsers is numeric if a null value is not passed --->
		<cfif len(arguments.noOfUsers) gt 0 and not isNumeric(arguments.noOfUsers)>
			<cfreturn 0>
		</cfif>

		<cfif not structKeyExists(arguments,"points")>
			<cfquery name="getPointsValue" datasource="#application.siteDataSource#">
				select discountPrice from product where productID=#arguments.ItemID#
			</cfquery>
			<cfset pointsValue = getPointsValue.discountPrice*arguments.quantity>
		<cfelse>
			<cfset pointsValue = arguments.points>
		</cfif>

		<cfquery name="AddItems" datasource="#application.SiteDataSource#">
			INSERT INTO RWTransactionItems
			(	RWTransactionID
				,ProductID
				,Quantity
				<cfif isDefined("arguments.NoOfUsers") and isNumeric(arguments.noOfUsers)>
					,NoOfUsers
				</cfif>
				,Points
				,distiOrganisationID
				<cfif len(arguments.serialNo)>
					,serialNumber
				</cfif>
				<cfif len(arguments.invoiceNo)>
					,invoiceNumber
				</cfif>
				<cfif len(arguments.invoiceDate)>
					,invoiceDate
				</cfif>
				<cfif len(arguments.endUserCompanyName)>
					,endUserCompanyName
				</cfif>
				,pointsType
				,RWPromotionID
			)
			VALUES
			(	<cf_queryparam value="#arguments.RWTransactionID#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#arguments.ItemID#" CFSQLTYPE="CF_SQL_INTEGER" >
				,<cf_queryparam value="#arguments.quantity#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfif isDefined("arguments.NoOfUsers") and isNumeric(arguments.noOfUsers)>
					,<cf_queryparam value="#arguments.NoOfUsers#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
				,<cf_queryparam value="#pointsValue#" CFSQLTYPE="cf_sql_float" >
				,<cf_queryparam value="#arguments.distiID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfif len(arguments.serialNo)>
					,<cf_queryparam value="#arguments.serialNo#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif len(arguments.invoiceNo)>
					,<cf_queryparam value="#arguments.invoiceNo#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif len(arguments.invoiceDate)>
					,<cf_queryparam value="#CreateODBCDate(arguments.invoiceDate)#" CFSQLTYPE="CF_SQL_timestamp" >
				</cfif>
				<cfif len(arguments.endUserCompanyName)>
					,<cf_queryparam value="#arguments.endUserCompanyName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				,<cf_queryparam value="#arguments.pointsType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				,<cf_queryparam value="#arguments.RWPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
			)

			select scope_identity() as rwTransactionItemsID
		</cfquery>

		<cfreturn AddItems.rwTransactionItemsID>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This 'moves' transaction items from one transaction to another

 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="MoveRWTransactionItems" hint="Moves line items from one transaction to another.">
		<cfargument name="sourceRWTransactionID" type="numeric" required="true">
		<cfargument name="targetRWTransactionID" type="numeric" required="true">
		<cfargument name="productCampaignId" type="numeric" required="true">

		<cfset var UpdateItems = "">

		<cfquery name="UpdateItems" datasource="#application.SiteDataSource#">
			UPDATE RWTransactionItems
			SET RWTransactionID = #arguments.targetRWTransactionID#
			WHERE
				RWTransactionID = #arguments.sourceRWTransactionID#
				AND productID IN (SELECT productID FROM product WHERE campaignID = #arguments.productCampaignId#)
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="RemoveRWTransactionItems" hint="Removes line items from the transaction">
		<cfargument name="RWTransactionID" type="numeric" required="true">
		<cfargument name="RWTransactionItemsIDList" type="string" required="true">

		<cfset var RemoveItem = "">

		<cfquery name="RemoveItem" datasource="#application.siteDataSource#">
			DELETE FROM RWTransactionItems
			WHERE RWTransactionID = #arguments.RWTransactionID#
			AND RWTransactionItemsID  IN ( <cf_queryparam value="#RWTransactionItemsIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Submits claim, re-calculates totals
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="submitClaim" hint="Submit the claim and wait for approval" >

		<cfargument name="RWTransactionID" type="numeric" required="yes">
		<cfargument name="RWTransactionTypeID" type="string" default="CL">
		<cfargument name="distiID" type="numeric" required="yes">
		<cfargument name="createdBy" type="numeric" default="#request.relayCurrentUser.personID#">  <!--- NJH 2007/09/07 --->

		<cfset var pointsAmount = "">
		<cfset var pointsExpiryDate = "">
		<cfset var getAllPoints = "">
		<cfset var updateRWTransaction = "">
		<cfset var getAccountID = "">
		<cfset var PointsAccrued = "">

		<!--- NJH 2007/03/30 setting the points expiry date. Set the date span in incentiveINI --->
		<!--- <cfif isDefined("request.expirePointsDateSpan") and listLen(request.expirePointsDateSpan,"/")> --->
		<cfif application.com.settings.getSetting("incentive.expirePointsDateSpan") neq "">
			<cfset pointsExpiryDate = now()>
			<!--- add num of years to expiry date --->
			<cfset pointsExpiryDate = DateAdd("yyyy",listGetAt(request.expirePointsDateSpan,1,"/"),pointsExpiryDate)>
			<!--- add num of months to expiry date --->
			<cfset pointsExpiryDate = DateAdd("m",listGetAt(request.expirePointsDateSpan,2,"/"),pointsExpiryDate)>
			<!--- add num of weeks to expiry date --->
			<cfset pointsExpiryDate = DateAdd("ww",listGetAt(request.expirePointsDateSpan,3,"/"),pointsExpiryDate)>
			<!--- add num of days to expiry date --->
			<cfset pointsExpiryDate = DateAdd("d",listGetAt(request.expirePointsDateSpan,4,"/"),pointsExpiryDate)>
		</cfif>

		<cftransaction>

		<cfquery name="getAllPoints" DATASOURCE="#application.SiteDataSource#">
			SELECT sum(points) as allPoints
			FROM RWTransactionItems
			WHERE RWTransactionID = #arguments.RWTransactionID#
		</cfquery>

		<cfset pointsAmount = getAllPoints.allPoints>

		<cfquery name="updateRWTransaction" DATASOURCE="#application.SiteDataSource#">
			UPDATE RWTransaction
			SET RWTransactionTypeID =  <cf_queryparam value="#arguments.RWTransactionTypeID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			CreditAmount =  <cf_queryparam value="#getAllPoints.allPoints#" CFSQLTYPE="CF_SQL_Integer" >
			WHERE RWTransactionID = #arguments.RWTransactionID#
		</cfquery>

		<cfquery name="getAccountID" DATASOURCE="#application.SiteDataSource#">
			SELECT accountID
			FROM RWTransaction
			WHERE RWTransactionID = #arguments.RWTransactionID#
		</cfquery>

		<cfquery name="PointsAccrued" DATASOURCE="#application.SiteDataSource#">
			exec RWAccruePoints @AccountID =  <cf_queryparam value="#getAccountID.accountID#" CFSQLTYPE="CF_SQL_INTEGER" > , @RWTransactionTypeID =  <cf_queryparam value="#arguments.RWTransactionTypeID#" CFSQLTYPE="CF_SQL_VARCHAR" > , @PointsAmount =  <cf_queryparam value="#PointsAmount#" CFSQLTYPE="CF_SQL_INTEGER" > , @PersonID = #request.relayCurrentUser.personid#, @TransactionID = #arguments.RWTransactionID#, @Type='BP' <cfif isDate(pointsExpiryDate)>,@expiryDate=#pointsExpiryDate#</cfif>, @CreatedPersonID=#createdBy#
		</cfquery>

		</cftransaction>

		<cfreturn pointsAmount>

	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="PointsBalance" output="true" returnType="query" hint="Returns the points balance for a given organisation">
		<cfargument name="datePassed" type="date" required="yes">
		<cfargument name="organisationID" type="numeric" required="yes">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">

		<cfset var newRow = "">
		<cfset var temp = "">
		<cfset var PointsBalanceQry = "">

		<cfif not arguments.personID>
			<cfquery name="PointsBalanceQry" datasource="#application.SiteDataSource#">
				exec RWGetPointsBalance @organisationid = #arguments.organisationid#, @BalanceDate = #arguments.datePassed#
			</cfquery>

		<cfelse>

			<cfquery name="PointsBalanceQry" datasource="#application.SiteDataSource#">
				exec RWGetPointsBalancePersonal @organisationid = #arguments.organisationid#, @personID = #arguments.personID# ,@BalanceDate = #arguments.datePassed#
			</cfquery>

		</cfif>

		<cfif PointsBalanceQry.recordcount is 0>
			<cfscript>
				newRow  = QueryAddRow(PointsBalanceQry);
				temp = QuerySetCell(PointsBalanceQry, "OrganisationID", organisationID);
				temp = QuerySetCell(PointsBalanceQry, "Balance", "0");
			</cfscript>
		</cfif>

		<cfreturn PointsBalanceQry>
	</cffunction>

	<cffunction name="getPointsBalanceForPortalUser" returntype="query" access="public">
		<cfargument name="spendIncentivePointsType" type="string" default="personal">
		<cfargument name="portalViewOnly" type="boolean" default="false">
		<cfargument name="ShowPointsforUnregisteredUser" type="boolean" default="false">

		<cfscript>
			var balanceDate = lsParseDateTime(request.requestTime);
			var qPointsBalance = "";
		</cfscript>

		<cfif arguments.spendIncentivePointsType eq "personal" and not arguments.portalViewOnly>
			<cfset qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,request.relayCurrentUser.organisationID,request.relayCurrentUser.personID)>
		<cfelseif arguments.spendIncentivePointsType eq "company">
			<cfset qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,request.relayCurrentUser.organisationID)>
		<cfelse>
			<cfset qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,request.relayCurrentUser.organisationID,request.relayCurrentUser.personID)>
		</cfif>

		<cfif arguments.ShowPointsforUnregisteredUser and arguments.portalViewOnly>
			<cfset qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,request.relayCurrentUser.organisationID)>
		</cfif>

		<cfreturn qPointsBalance>

	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns the person details for for THIS.personID.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPersonOrgAndCountry" hint="Returns organisationID for personID.">

		<cfargument name="personID" type="numeric" required="yes">

		<cfscript>
			var getPersonDetails = "";
		</cfscript>

		<CFQUERY name="getPersonDetails" datasource="#application.siteDataSource#">
			SELECT l.countryid, o.organisationID
			FROM Person p
			INNER JOIN Organisation o ON p.Organisationid = o.OrganisationID
			INNER JOIN Location l ON p.locationid = l.locationID
			WHERE personID = #arguments.personID#
		</CFQUERY>

		<cfreturn getPersonDetails>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCompanyAccount" returnType="query" hint="gets basic information about the current organisation">

		<cfargument name="organisationID" type="numeric" required="true">

		<cfscript>
			var CompanyAccount = "";
		</cfscript>

		<cfquery name="CompanyAccount" DATASOURCE="#application.SiteDataSource#">
			select
				pm.firstname as pmfirstname,
				pm.lastname as pmlastname,
				sc.firstname as scfirstname,
				sc.lastname as sclastname,
				o.organisationname,
				CA.*
			from
				vRWCompanyAccount as CA
				inner join organisation as o
				on o.organisationid = ca.organisationid
				left join person as pm
				on pm.personid = ca.programmanager
				left join person as sc
				on sc.personid = ca.seniorcontact
			where
				ca.organisationid = #arguments.organisationid#
		</cfquery>

		<cfreturn CompanyAccount>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCompanyAccountSimple" returnType="numeric" hint="gets just the accountID">

		<cfargument name="organisationID" type="numeric" required="true">

		<cfscript>
			var CompanyAccount = "";
			var CompanyAccountID = 0;
		</cfscript>

		<cfquery name="CompanyAccount" DATASOURCE="#application.SiteDataSource#">
			select
				accountID
			from
				RWCompanyAccount
			where
				organisationid = #arguments.organisationid#
		</cfquery>

		<cfif CompanyAccount.recordCount gt 0>
			<cfset CompanyAccountID = CompanyAccount.accountID>
		</cfif>
		<cfreturn CompanyAccountID>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getAccountManagerDetails" returnType="query" hint="get the account manager for this partner">
		<cfargument name="organisationID" type="numeric" required="true">

		<cfscript>
			var AccountManagerDetails = "";
			var newRow = "";
			var temp = "";
		</cfscript>

		<cfquery name="AccountManagerDetails" DATASOURCE="#application.SiteDataSource#">
			select
				p.firstname + ' ' + p.lastname as AccMgrName,
				p.email as AccMgrEmail,
				p.officephone as AccMgrTel
			from
				organisation as o
				inner join person as p
				on o.AccountMngrID = p.personid
			where
				o.organisationid = #arguments.organisationid#
		</cfquery>

		<cfif AccountManagerDetails.recordcount is 0>
			<cfscript>
					newRow  = QueryAddRow(AccountManagerDetails);
					temp = QuerySetCell(AccountManagerDetails, "AccMgrName", "N/A");
					temp = QuerySetCell(AccountManagerDetails, "AccMgrEmail", "N/A");
					temp = QuerySetCell(AccountManagerDetails, "AccMgrTel", "N/A");
			</cfscript>
		</cfif>

		<cfreturn AccountManagerDetails>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPersonDetails" returnType="query" hint="get the person details">
		<cfargument name="personID" type="numeric" required="true">

		<cfscript>
			var PersonDetails = "";
		</cfscript>

		<cfquery name="PersonDetails" DATASOURCE="#application.SiteDataSource#">
			select
				p.firstname + ' ' + p.lastname as PersonName

			from
				person as p

			where
				p.personid = #arguments.personid#
		</cfquery>

		<cfreturn PersonDetails>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" output="true" name="getPointsAccrued" returnType="query" hint="gets the points accrued for an organisation for a month">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="dateFrom" type="date" required="true">
		<cfargument name="dateTo" type="date" required="true">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="RWTransactionTypeExclusionList" type="string" default="LS,LA">

		<cfscript>
			var PointsAccrued = "";
			var companyAccount = getCompanyAccountSimple(arguments.organisationID);
			var endOfWeekVar = "";
			var temp = "";
		</cfscript>

		<cfquery name="PointsAccrued" DATASOURCE="#application.SiteDataSource#">
			SELECT creditAmount as earned, debitAmount as spent, rwTransactionID,
					RWTransactionTypeID as type, created, '' as weekEndingDate
			FROM RWTransaction

			WHERE accountID =  <cf_queryparam value="#companyAccount#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND created <= #DateAdd('d', 1, arguments.dateTo)#
			AND created >= #arguments.dateFrom#
			<cfif arguments.personID>
				AND personID = #arguments.personID#
			</cfif>
			<cfif len(arguments.RWTransactionTypeExclusionList)>
				AND RWTransactionTypeID not in (<cf_queryparam value="#arguments.RWTransactionTypeExclusionList#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfif>

			ORDER BY RWTransactionID
		</cfquery>

		<cfoutput query="PointsAccrued">
			<cfscript>
				endOfWeekVar = dateFormat(dateadd('d', 7-dayOfWeek(created), created),"yyyy-mm-dd");
				temp = querySetCell(PointsAccrued, "weekEndingDate", endOfWeekVar, pointsAccrued.currentRow);
			</cfscript>
		</cfoutput>


		<!---
		<cfquery name="PointsAccrued" DATASOURCE="#application.SiteDataSource#">
			exec RWGetStatementSummary @OrganisationID = #arguments.OrganisationID#, @DateTo = #arguments.DateTo# <cfif arguments.UseDateFrom>, @DateFrom = #arguments.DateFrom#</cfif>
		</cfquery> --->

		<cfreturn PointsAccrued>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPointsAccruedPersonal" returnType="query" hint="gets the points accrued for a person for a month">


		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="personID" type="numeric" required="true"><strong></strong>
		<cfargument name="dateFrom" type="date" required="true">
		<cfargument name="dateTo" type="date" required="true">
		<cfargument name="useDateFrom" type="boolean" default="false">

		<cfscript>
			var PointsAccrued = "";
		</cfscript>

		<cfquery name="PointsAccrued" DATASOURCE="#application.SiteDataSource#">
			exec RWGetStatementSummaryPersonal @OrganisationID = #arguments.OrganisationID#, @PersonID = #arguments.PersonID#, @DateTo = #arguments.DateTo# <cfif arguments.UseDateFrom>, @DateFrom = #arguments.DateFrom#</cfif>
		</cfquery>

		<cfreturn PointsAccrued>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getEarlyExpirePoints" returnType="query" hint="get the next batch of points to expire">

		<cfargument name="organisationID" type="numeric" required="true">

		<cfscript>
			var EarlyExpirePoints = "";
		</cfscript>

		<cfquery name="EarlyExpirePoints" DATASOURCE="#application.SiteDataSource#">
			select
				pa.organisationid,
				pa.expirydate,
				sum(isnull(pa.points, 0)) as balance
			from
				rwpointsaccrued as PA
			where
				pa.organisationid = #organisationid#
				and pa.expirydate = (select min(expirydate) from rwPointsAccrued
					where organisationid = #arguments.organisationid#)
			group by
				pa.organisationid,
				expirydate

			union select
				ps.organisationid,
				ps.expirydate,
				sum(isnull(ps.spent, 0))* -1
			from
				vRWPointsSpent as PS
			where
				ps.organisationid = #arguments.organisationid#
				and ps.expirydate = (select min(expirydate) from vRWPointsSpent
					where organisationid = #arguments.organisationid#)
			group by
				ps.organisationid,
				ps.expirydate
		</cfquery>

		<cfreturn EarlyExpirePoints>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    NJH 2008/03/05 This function returns all the points to expire before a certain date.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getExpirePointsBeforeDate" returnType="query" hint="get the next batch of points to expire before a certain date">
		<cfargument name="expiryDate" type="date" required="true">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="returnPersonInfo" type="boolean" required="false">

		<cfscript>
			var GetExpirePoints = "";
		</cfscript>

		<cfquery name="GetExpirePoints" DATASOURCE="#application.SiteDataSource#">
			SELECT AccountID,
				balance = case when sum(creditamount) - sum(debitamount) > 0 then sum(creditamount) - sum(debitamount) else 0 END,
				baseQuery.PersonID
				<cfif isDefined("arguments.returnPersonInfo")>
				,
				person.Firstname + ' ' + person.Lastname AS Person,
				person.organisationid,
				organisation.organisationName,
				organisation.countryID,
				country.countryDescription
				</cfif>
			FROM (
				SELECT accountid,creditamount,debitamount,personid
				FROM RWTransaction
				WHERE rwtransactionTypeID in ('AC','AL') and
					(rwtransactionid in
					(SELECT DISTINCT RWTransaction.rwtransactionid from RWTransaction inner join
						RWTransactionSplit on RWTransaction.RWTransactionID = RWTransactionSplit.RWTransactionID inner join
						RWPointsAccrued on RWTransactionSplit.PtsAccID = RWPointsAccrued.PtsAccID
					WHERE RWPointsAccrued.expiryDate < #arguments.expiryDate# and RWPointsAccrued.ExpiredDate is null)
				OR debitamount > 0)
			) baseQuery
			<cfif isDefined("arguments.returnPersonInfo")>
			INNER JOIN person ON person.personID = baseQuery.personID
			INNER JOIN organisation ON person.organisationid = organisation.organisationid
			INNER JOIN country ON organisation.countryid = country.countryid
			</cfif>
			<cfif isDefined("arguments.personID")>
			WHERE baseQuery.personid = #arguments.personID#
			</cfif>
			GROUP BY AccountID,baseQuery.personID<cfif isDefined("arguments.returnPersonInfo")>,person.firstname,person.lastname,person.organisationid,organisation.organisationName,country.countryDescription,organisation.countryID</cfif>
			having sum(creditamount) - sum(debitamount) > 0
			order by baseQuery.personID
		</cfquery>

		<cfreturn GetExpirePoints>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="basePointDetails" returnType="query" hint="base points detail query">

		<cfargument name="weekendingdate" type="date" required="true">
		<cfargument name="thisPtsAccID" type="numeric" required="true">

		<cfscript>
			var BasePointsDetail = "";
		</cfscript>

		<cfquery name="BasePointsDetail" DATASOURCE="#application.SiteDataSource#">
			select
				sod.SKU,
				skup.Description,
				sod.Sold,
				sod.Returned,
				sod.Net,
				skup.Points
			from
				rwSalesOutData as sod
				inner join rwSKUPoints as skup
				on sod.sku = skup.sku
				and sod.weekendingdate = skup.weekendingdate
			where
				sod.weekendingdate =  <cf_queryparam value="#lsparsedatetime(arguments.weekendingdate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				and sod.PtsAccID = #arguments.thisPtsAccID#
		</cfquery>

		<cfreturn BasePointsDetail>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="PromotionPointsDetail" returnType="query" hint="Promotion Points detail query">

		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="weekendingdate" type="date" required="true">

		<cfscript>
			var PromotionPointsDetail = "";
		</cfscript>

		<cfquery name="PromotionPointsDetail" DATASOURCE="#application.SiteDataSource#">
			exec RWGetPromotionPointsDetail @OrganisationID = #arguments.OrganisationID#, @WeekEndingDate =  <cf_queryparam value="#lsparsedatetime(arguments.weekendingdate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<cfreturn PromotionPointsDetail>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="SpentPointsDetail" returnType="query" hint="Spent Points Detail query">

		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="weekendingdate" type="date" required="true">
		<cfargument name="personID" type="numeric" required="no" default="0">

		<cfscript>
			var qSpentPointsDetail = "";
		</cfscript>

		<cfif arguments.personID>
			<cfquery name="qSpentPointsDetail" DATASOURCE="#application.SiteDataSource#">
				exec RWGetSpentPointsDetailPersonal @PersonID = #arguments.personID#, @WeekEndingDate =  <cf_queryparam value="#lsParseDateTime(WeekEndingDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</cfquery>
		<cfelse>
			<cfquery name="qSpentPointsDetail" DATASOURCE="#application.SiteDataSource#">
				exec RWGetSpentPointsDetail @OrganisationID = #OrganisationID#, @WeekEndingDate =  <cf_queryparam value="#lsParseDateTime(WeekEndingDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</cfquery>
		</cfif>

		<cfreturn qSpentPointsDetail>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             creates RW company account - based on organisationID, locationID or personID
			 (always works out organisationID) checks whether an RW account already exists
			 for that organisationID, if it doesn't, creates a new account and returns the new accountID,
			 if it does exist, it undeletes the account if it is deleted and returns the existing accountID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createRWAccount" returnType="numeric" hint="creates a company account" output="true">
		<cfargument name="organisationID" type="numeric" default="0">
		<cfargument name="locationID" type="numeric" default="0">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="startDate" default="">
		<cfargument name="undeleteAccount" type="boolean" default="false" hint="Undelete the account if it exists but is deleted"> <!--- NJH 2010/03/30 P-LEX041 CS1 --->

		<cfscript>
			var getOrg = "";
			var localOrganisationID=0;
			var accountStartDate = "";
			var GetrwCompanyAccount = "";
			var check = "";
			var unDeleteOrgAccount = "";
		</cfscript>

		<cfif arguments.organisationID>
			<cfset localOrganisationID = arguments.organisationID>
		<cfelseif arguments.locationID or arguments.personID>

			<cfquery name="getOrg" DATASOURCE="#application.SiteDataSource#">
				select organisationid
				from
				<cfif arguments.locationID>
					location where locationid = #arguments.locationID#
				<cfelseif arguments.personID>
					person where personid = #arguments.personID#
				</cfif>
			 </cfquery>

			<cfif getOrg.recordCount gt 0>
				<cfset localOrganisationID = getOrg.organisationid>
			</cfif>

		</cfif>

		<!--- <cfif not getOrg.recordCount> --->
		<cfif localOrganisationID eq 0>
			<SCRIPT type="text/javascript">
				//2011-12-13 NYB LHID8108 changed text from barely legible:
				//alert("Could not create an RW account - organisation does not exist! Most probably you have passes a wrong orgID or a wrong locationID or a wrong personID. Please check the order of your arguments in the call to the function.");
				//to:
				alert("The system could not create a company account as the organisation could not be found.  Possible causes: An incorrect organisation ID, location ID or person ID have been passed to the function.");
			</script>

			<CF_ABORT>
		</cfif>

		<!---  check whether account already exists --->
		<!--- NJH 2010/03/30 P-LEX041 CS1 - Get the most recent account created.. should in theory only be one --->
		 <cfquery name="GetrwCompanyAccount" DATASOURCE="#application.SiteDataSource#">
		 	select top 1 accountID from rwcompanyAccount where organisationid =  <cf_queryparam value="#localOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  order by created desc
		 </cfquery>

		 <cfif GetrwCompanyAccount.recordCount is 0>

		 	<!---
SSS 2009/02/18 CR-LEX592 Removed cftransaction because was nested inside another transaction when doing a sod load.
decided that it wasn't required, especially if we did a select scope_identity()  to get the inserted record

<cftransaction> --->

				<cfif not len(arguments.startDate)>
					<cfset accountStartDate = now()>
				<cfelse>
					<cfset accountStartDate = arguments.startDate>
				 </cfif>

				 <cfquery name="GetrwCompanyAccount" DATASOURCE="#application.SiteDataSource#">
					insert into rwCompanyAccount
					(OrganisationID, DateTermsAgreed, Created, CreatedBy,Updated,UpdatedBy, AccountDeleted,PointStartDate)
					values
						(<cf_queryparam value="#localOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							<cf_queryparam value="#accountStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
							getDate(),
							<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
							getDate(),
							<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
							0,
							<cf_queryparam value="#accountStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)

							select scope_identity() as accountID
				 </cfquery>

				<!--- NYB 2009-01-27 CR-SNY62 - added "where OrganisationID=#localOrganisationID#" to query - as this should always return the correct result
				  <cfquery name="check" DATASOURCE="#application.SiteDataSource#">
					 select max(accountID) as accountID from rwcompanyAccount where OrganisationID=#localOrganisationID#
				  </cfquery>--->

			<!--- </cftransaction> --->
		 <cfelseif arguments.undeleteAccount>

			 <!--- NJH 2010/03/29 P-LEX041 CS1 - if an organisation already has an account that exists but it is deleted, then update the account --->
			 <cfquery name="unDeleteOrgAccount" datasource="#application.SiteDataSource#">
				 update rwCompanyAccount set AccountDeleted=0,
				 	Updated = #createODBCDateTime(request.requestTime)#,
				 	UpdatedBy = #request.relayCurrentUser.userGroupID#
				 where accountID =  <cf_queryparam value="#GetrwCompanyAccount.accountID#" CFSQLTYPE="CF_SQL_INTEGER" >
				 	and AccountDeleted = 1
			</cfquery>
		 </cfif>


		  <cfreturn GetrwCompanyAccount.accountID>

	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             first checks whether the company account has administrative person attached
			 to it. If not, it registers the current user who is registering as the
			 administrator. If administrator already exists, it registers the user
 			 as part of the company account
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 +++ --->
	<cffunction access="public" name="addPersonToRWAccount" hint="adds an individual to an RW incentive company account">
		<cfargument name="accountID" type="numeric" required="yes">
		<cfargument name="personID" type="numeric" required="yes">

		<cfscript>
			var getAdmin = "";
			var administratorID = "";
			var setAdmin = "";
			var getPerson = "";
			var setPerson = "";
		</cfscript>

		<!---Is there already an administrator?--->
		<cfquery name="getAdmin" DATASOURCE="#application.SiteDataSource#">
			select AdminPersonID
			from RWCompanyAccount
			where accountID = #arguments.accountID#
		</cfquery>

		<cfset administratorID = getAdmin.AdminPersonID>

		<!---If administrator still not registered, register the current
		user as administrator--->
		<cfif not len(administratorID)>
			<cfquery name="setAdmin" DATASOURCE="#application.SiteDataSource#">
				update RWCompanyAccount
				set AdminPersonID = #arguments.personID#
				where accountID = #arguments.accountID#
			</cfquery>
		</cfif>

		<!---Is there already a person record for this RWaccount?--->
		<cfquery name="getPerson" DATASOURCE="#application.SiteDataSource#">
			select *
			from RWPersonAccount
			where accountID = #arguments.accountID#
			and personID = #arguments.personID#
		</cfquery>

		<cfif getPerson.recordCount eq 0>

			<cfquery name="setPerson" DATASOURCE="#application.SiteDataSource#">
				insert into RWPersonAccount(accountID,personID,dateTermsAgreed,createdBy)
				values(<cf_queryparam value="#arguments.accountID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#CreateODBCDateTime(Now())#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >)
			</cfquery>

		</cfif>



	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getTransactionDetails" returnType="query" hint="all the details of the passed transactionID">

		<cfargument name="RWTransactionID" type="numeric" required="true">


		<cfscript>
			var getTransactionDetails = "";
		</cfscript>

		<cfquery name="getTransactionDetails" DATASOURCE="#application.SiteDataSource#">
			SELECT *
			FROM RWTransaction
			WHERE RWTransactionID = #arguments.RWTransactionID#
		</cfquery>

		<cfreturn getTransactionDetails>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getTransactionItems" returnType="query" hint="all the items of the passed transactionID">

		<cfargument name="RWTransactionID" type="numeric" required="true">
		<cfargument name="statementView" type="boolean" required="false" default="false">

		<cfscript>
			var getTransactionItems = "";
		</cfscript>
		<cfif statementView>
			<!--- statement - may have promotion items that aren't product related --->
			<!--- GCC 2008/05/06 CR-TND552 Moved calculation for pointsPerUnit into here fron incentivePointsClaimByProductFamily - repeated in getTransactionItemsPerWeek --->
			<cfquery name="getTransactionItems" DATASOURCE="#application.SiteDataSource#">
				SELECT *, pointsPerUnit = case when ti.noOfUsers is not null and ti.noOfUsers <> 0 then (ti.points/ti.noOfUsers)/ti.quantity else ti.points/ti.quantity end
				FROM RWTransactionItems ti left outer join product p on ti.productid = p.productid
				WHERE (ti.productID = p.productID or ti.productid = 0)
				AND RWTransactionID = #arguments.RWTransactionID#
				order by p.productID, invoicedate desc
			</cfquery>
		<cfelse>
			<cfquery name="getTransactionItems" DATASOURCE="#application.SiteDataSource#">
				SELECT *, pointsPerUnit = case when ti.noOfUsers is not null and ti.noOfUsers <> 0 then (ti.points/ti.noOfUsers)/ti.quantity else ti.points/ti.quantity end
				FROM RWTransactionItems ti,product p
				WHERE ti.productID = p.productID
				AND RWTransactionID = #arguments.RWTransactionID#
				order by p.productID, invoicedate desc
			</cfquery>
		</cfif>

		<cfreturn getTransactionItems>

	</cffunction>
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	This gets the set of distinct Campaign Ids  for a set of transaction items.
 	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getRWTransactionItemCampaignIds" returnType="query" hint="all the items of the passed transactionID">

		<cfargument name="RWTransactionID" type="numeric" required="true">


		<cfscript>
			var getItemCampaignIds = "";
		</cfscript>

		<cfquery name="getItemCampaignIds" DATASOURCE="#application.SiteDataSource#">
			SELECT distinct(p.campaignID)
			FROM RWTransactionItems ti, product p
			WHERE ti.productID = p.productID
			AND RWTransactionID = #arguments.RWTransactionID#
		</cfquery>

		<cfreturn getItemCampaignIds>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getWeekDetails" returnType="struct" hint="all the details of the passed transactionID">
		<cfargument name="WeekEnding" type="date" required="true">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="organisationID" type="numeric" required="false"/>	<!--- 2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->

		<cfscript>
			var getDetails = "";
			var weekStruct = structNew();
			// WAB 2006-04-11
			// the week details were all one day out so were changed,
			var dateTo = CreateODBCDate(DateAdd('d', 1, arguments.WeekEnding));	// need to add a day to get all transactions before 00:00:00 on the next day  (assumes that date doesn't have any time attached to it)
			var dateFrom = CreateODBCDate(DateAdd('d', -6, arguments.WeekEnding));
			var getPointsClaimed = "";
			var getPointsPromotion = "";
			var getPointsVerified = "";
			var getPointsAwaitingVerification = "";
			var getPointsSpent = "";
			var companyAccounts = structKeyExists(arguments,"organisationID")?getCompanyAccount(organisationID=arguments.organisationID):0;
			var companyAccountList = 0;
		</cfscript>

		<cfif structKeyExists(arguments,"organisationID") and companyAccounts.recordCount>
			<cfset companyAccountList = valueList(companyAccounts.AccountID)>
		</cfif>

		<!---Claimed base points--->
		<cfquery name="getPointsClaimed" DATASOURCE="#application.SiteDataSource#">
			SELECT sum(ti.points)  as PointsClaimed
			FROM RWTransaction t, RWTransactionItems ti
			WHERE t.RWTransactionID = ti.RWTransactionID
			AND t.RWTransactionTypeID IN ('AC','CL')
			AND pointsType = 'BP'
			<cfif arguments.personID>
				AND t.personID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
			</cfif>
			<!---2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->
			<cfif structKeyExists(arguments,"organisationID")>
				AND t.AccountID IN (<cfqueryparam cfsqltype="cf_sql_integer" value='#companyAccountList#' list="true" />)
			</cfif>
			AND t.created  between  <cf_queryparam value="#dateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#dateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<!---Calimed promotion points--->
		<cfquery name="getPointsPromotion" DATASOURCE="#application.SiteDataSource#">
			SELECT sum(ti.points)  as promotionPoints
			FROM RWTransaction t, RWTransactionItems ti
			WHERE t.RWTransactionID = ti.RWTransactionID
			AND t.RWTransactionTypeID IN ('AC','CL')
			AND pointsType = 'PP'
			<cfif arguments.personID>
				AND t.personID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
			</cfif>
			<!---2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->
			<cfif structKeyExists(arguments,"organisationID")>
				AND t.AccountID IN (<cfqueryparam cfsqltype="cf_sql_integer" value='#companyAccountList#' list="true" />)
			</cfif>
			AND t.created  between  <cf_queryparam value="#dateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#dateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<cfquery name="getPointsVerified" DATASOURCE="#application.SiteDataSource#">
			select sum(creditAmount) as PointsVerified
			FROM RWTransaction
			WHERE RWTransactionTypeID = 'AC'
			<cfif arguments.personID>
				AND personID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
			</cfif>
			<!---2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->
			<cfif structKeyExists(arguments,"organisationID")>
				AND AccountID IN (<cfqueryparam cfsqltype="cf_sql_integer" value='#companyAccountList#' list="true" />)
			</cfif>
			AND created  between  <cf_queryparam value="#dateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#dateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<cfquery name="getPointsAwaitingVerification" DATASOURCE="#application.SiteDataSource#">
			select sum(creditAmount) as PointsAwaitingVerification
			FROM RWTransaction
			WHERE RWTransactionTypeID = 'CL'
			<cfif arguments.personID>
				AND personID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
			</cfif>
			<!---2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->
			<cfif structKeyExists(arguments,"organisationID")>
				AND AccountID IN (<cfqueryparam cfsqltype="cf_sql_integer" value='#companyAccountList#' list="true" />)
			</cfif>
			AND created  between  <cf_queryparam value="#dateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#dateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<cfquery name="getPointsSpent" DATASOURCE="#application.SiteDataSource#">
			select sum(debitAmount) as PointsSpent
			FROM RWTransaction
			WHERE RWTransactionTypeID in ('AL','EX')  <!--- NJH 2009/12/15 LHID 2320 - include expired points as part of 'spent' points --->
			<cfif arguments.personID>
				AND personID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
			</cfif>
			<!---2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->
			<cfif structKeyExists(arguments,"organisationID")>
				AND AccountID IN (<cfqueryparam cfsqltype="cf_sql_integer" value='#companyAccountList#' list="true" />)
			</cfif>
			AND created  between  <cf_queryparam value="#dateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#dateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<cfscript>
			weekStruct.pointsClaimed = not len(getPointsClaimed.PointsClaimed)?0:getPointsClaimed.PointsClaimed;
			weekStruct.pointsPromotion = not len(getPointsPromotion.promotionPoints)?0:getPointsPromotion.promotionPoints;
			weekStruct.pointsVerified = not len(getPointsVerified.pointsVerified)?0:getPointsVerified.pointsVerified;
			weekStruct.PointsAwaitingVerification = not len(getPointsAwaitingVerification.PointsAwaitingVerification)?0:getPointsAwaitingVerification.PointsAwaitingVerification;
			weekStruct.PointsSpent = not len(getPointsSpent.PointsSpent)?0:getPointsSpent.PointsSpent;
		</cfscript>


		<cfreturn weekStruct>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getTransactionItemsPerWeek" hint="Get all transaction items in a week">

		<cfargument name="WeekEnding" type="date" required="true">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="organisationID" type="numeric" required="false"/>	<!--- 2016-01-13 AHL Case 447535 Filtering Incentive Statement by Organisation --->

		<cfscript>
			var getTransactionItemPerWeek = "";
			// the week details were all one day out so were changed,
			var dateTo = CreateODBCDate(DateAdd('d', 1, arguments.WeekEnding));	// need to add a day to get all transactions before 00:00:00 on the next day  (assumes that date doesn't have any time attached to it)
			var dateFrom = CreateODBCDate(DateAdd('d', -6, arguments.WeekEnding)); //
		</cfscript>

		<!--- 2008/05/06 GCC CR-TND552 - If qty is one but with multiple users then calculate unit proce using no of users instead --->
		<cfquery name="getTransactionItemPerWeek" datasource="#application.SiteDataSource#">
			SELECT ti.productID, ti.quantity, ti.noOfUsers, ti.points, pointsPerUnit = case when ti.noOfUsers is not null and ti.noOfUsers <> 0 then (ti.points/ti.noOfUsers)/ti.quantity else ti.points/ti.quantity end,
			t.RWTransactionTypeID, p.sku

			FROM RWTransaction t, RWTransactionItems ti, product p
			,RWCompanyAccount ac

			WHERE t.RWTransactionID = ti.RWTransactionID
			AND ti.productID = p.productID
			AND t.RWTransactionTypeID IN ('AC','CL')
			<cfif structKeyExists(arguments,"organisationID") and arguments.organisationID neq 0>
				AND ac.AccountID = t.AccountID
				AND ac.OrganisationID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.organisationID#" />
			</cfif>
			<cfif arguments.personID>
				AND t.personID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">
			</cfif>

			AND t.created  between  <cf_queryparam value="#dateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#dateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</cfquery>

		<cfreturn getTransactionItemPerWeek>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductAndGroupList" hint="Product groups and products query">

		<cfargument name="productCatalogueCampaignID" type="numeric" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="productCategoryID" type="numeric" default="0">
		<cfargument name="constrainByFlag" type="boolean" default="false">
		<cfargument name="statementView" type="boolean" default="false">


		<cfscript>
			var getProductAndGroupListQry = "";
			var titlePGPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productGroup", phraseTextID = "title",baseTableAlias="pg");
			var titleProdPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "product", phraseTextID = "title",baseTableAlias="p");
		</cfscript>

		<!--- NJH 2009/03/30 CR-SNY675 - added sku --->
		<!--- AWJR 30-03-2011  P-WEB005 - added listprice --->
		<cfquery name="getProductAndGroupListQry" datasource="#application.SiteDataSource#">
			SELECT p.productID, left(p.SKU+' '+replace(replace(replace(p.description,CHAR(34),' inch'),CHAR(39),''),CHAR(47),''),37) as productDescription,p.sortOrder,p.SKU,
				pg.productGroupID, pg.[description] as productGroupDescription, 'phr_description_productGroup_'+convert(varchar,pg.productGroupID) as translatedProductGroupDescription, p.discountprice,p.listprice, pg.IncentiveClaimRequiredFields,
				#preserveSingleQuotes(titlePGPhraseQuerySnippets.select)#  as translatedProductGroupTitle,
				#preserveSingleQuotes(titleProdPhraseQuerySnippets.select)#  as translatedProductTitle
			FROM product p
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
				INNER JOIN productGroup pg ON p.productGroupID = pg.productGroupID
				#preserveSingleQuotes(titlePGPhraseQuerySnippets.join)#
				#preserveSingleQuotes(titleProdPhraseQuerySnippets.join)#
				<cfif arguments.constrainByFlag>
					INNER JOIN productFlag pgf ON pgf.productID = p.productID
				</cfif>
				where 1=1 and

				p.campaignID in (#arguments.productCatalogueCampaignID#)

                <!---
				<!---
					NJH 2009/06/24 Bug Fix Sony Issue 2395 - was using nonGlobalCatalogueCountryList which doesn't exist in code. Use singleCountryCatalogueCountryList
				<cfif structkeyexists(request,"nonGlobalCatalogueCountryList") and listfindnocase(request.nonGlobalCatalogueCountryList,arguments.countryID,",") neq 0> --->
				<cfif structkeyexists(request,"singleCountryCatalogueCountryList") and listfindnocase(request.singleCountryCatalogueCountryList,arguments.countryID,",") neq 0>
					and (p.countryID = #arguments.countryID#)
				<cfelse>
					and (p.countryID in (select countrygroupid from countrygroup where countrymemberid = #arguments.countryID#)
					or p.countryID = 0)
				</cfif>
                --->
                and (p.hascountryScope = 0 or vcs.countryid is not null)
				and p.active=1
				and p.deleted = 0
				and pg.active=1
				<cfif not statementView>
					and accrualMethod='M'
				</cfif>

				<cfif arguments.constrainByFlag>
				and (
					pgf.flagID in (select distinct bfd.flagID from booleanFlagData bfd
					INNER JOIN flag f ON f.flagid = bfd.flagid
					INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid
					INNER JOIN organisation o ON o.organisationID = bfd.entityID
					INNER JOIN person p ON p.organisationID = o.organisationID
					where p.personID = #request.relayCurrentUser.personID# and fg.entitytypeid = 2)
					OR pgf.flagID in (select distinct bfd.flagID from booleanFlagData bfd
					INNER JOIN flag f ON f.flagid = bfd.flagid
					INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid
					INNER JOIN person p ON p.personID = bfd.entityID
					where p.personID = #request.relayCurrentUser.personID# and fg.entitytypeid = 0)
					)

				</cfif>
				<cfif arguments.productCategoryID>
					and pg.productGroupID IN (SELECT productGroupID from productGroup WHERE productCategoryID = #arguments.productCategoryID#)
				</cfif>

				order by pg.productGroupID, p.sortOrder, p.SKU, p.[description]
		</cfquery>

		<cfreturn getProductAndGroupListQry>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID

			2016/02/01 NJH	BF-420 - removed the sortOrder override as not used. Changed the sortOrder to reflect what was being selected.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" output="true" name="getProductAndGroupAndCatList" hint="Product groups, products and product categories in a query">

		<cfargument name="productCatalogueCampaignID" type="numeric" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="productCategoryID" type="numeric" default="0">
		<cfargument name="productGroupID" type="numeric" default="0">
		<cfargument name="showSKU" type="boolean" default="no">
		<cfargument name="constrainByFlag" type="boolean" default="false">
		<cfargument name="countryRestrictionOverrideList" type="string" default=""> <!--- NJH 2008/04/14 - bring back products only in this country override list. Used in promotion edit.--->
		<cfargument name="accrualMethod" type="string" default="" required="false" hint="M=manual;A=automatic"> <!--- NJH 2009/06/25 Bug Fix Sony Issue 2395 - restrict to certain accrual methods --->

		<cfscript>
			var getProductAndGroupAndCatList = "";
			var titlePGPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productGroup", phraseTextID = "title",baseTableAlias="pg");
			var titleProdPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "product", phraseTextID = "title",baseTableAlias="p");
		</cfscript>

		<cfquery name="getProductAndGroupAndCatList" datasource="#application.SiteDataSource#">
			SELECT distinct p.productID,
				left(<cfif showSKU>p.SKU+' '+</cfif>p.title_defaultTranslation,37)
				as productDescription,p.sortOrder,
				pg.productGroupID, isNull(pg.title_defaultTranslation,pg.description_defaultTranslation) as productGroupDescription,
				p.discountprice, pg.IncentiveClaimRequiredFields,
				pc.title_defaultTranslation as productCategoryName, pc.productCategoryID,
				'phr_description_productGroup_'+convert(varchar,pg.productGroupID) as translatedproductGroupDescription,
				'phr_description_productCategory_'+convert(varchar,pc.productCategoryID) as translatedproductCategoryName,
				<!--- NJH 2008/04/14 - return the isoCode for products to help distinguish the products in different countries. User in promotion edit- --->
                <!--- case when p.countryID = 0 then 'All Countries' else c.isoCode end as isoCode, --->
                ISNULL( dbo.getCountryScopeList(p.productid,'product',1,2), 'All Countries') as isoCode,
				#preserveSingleQuotes(titlePGPhraseQuerySnippets.select)#  as translatedProductGroupTitle,
				#preserveSingleQuotes(titleProdPhraseQuerySnippets.select)#  as translatedProductTitle
				FROM         productCategory pc
					INNER JOIN ProductGroup pg ON pc.ProductCategoryID = pg.ProductCategoryID
                     inner join Product p on p.ProductGroupID = pg.ProductGroupID
                     left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid
				<cfif arguments.constrainByFlag>
					INNER JOIN productFlag pf ON pf.productID = p.productID
				</cfif>
				#preserveSingleQuotes(titlePGPhraseQuerySnippets.join)#
				#preserveSingleQuotes(titleProdPhraseQuerySnippets.join)#
				where p.campaignID in (#arguments.productCatalogueCampaignID#)
				<!--- NJH 2008/04/14 we want to get only the products for countries specified in this list--->
				<cfif isDefined("arguments.countryRestrictionOverrideList") and arguments.countryRestrictionOverrideList neq "">
                    <!--- keeping in line with current functionality --->
					and (p.hascountryScope = 0 or vcs.countryid in ( <cf_queryparam value="#arguments.countryRestrictionOverrideList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
				<cfelse>
					<!--- <cfif structkeyexists(request,"singleCountryCatalogueCountryList") and listfindnocase(request.singleCountryCatalogueCountryList,arguments.countryID,",") neq 0>
						and (p.countryID = #arguments.countryID#)
					<cfelse>
						and (p.countryID in (select countrygroupid from countrygroup where countrymemberid = #arguments.countryID#)
						or p.countryID = 0)
					</cfif> --->
                    and (p.hascountryScope = 0 or vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">)
				</cfif>
				and p.active=1
				and p.deleted = 0
				and pg.active = 1
				<!--- <cfif 1 eq 0> ---><!--- request.allowPromotionsOnAutomatedAccruals eq false --->
				<!--- NJH 2009/06/25 Bug Fix Sony Issue 2395 - if accrual method is passed in, filter the products --->
				<cfif listFindNoCase("M,A",arguments.accrualMethod)>
					and accrualMethod =  <cf_queryparam value="#arguments.accrualMethod#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>

				<cfif arguments.constrainByFlag>
				and (
					pf.flagID in (select distinct bfd.flagID from booleanFlagData bfd
					INNER JOIN flag f ON f.flagid = bfd.flagid
					INNER JOIN flaggroup fg on fg.flaggroupid = f.flaggroupid
					INNER JOIN organisation o ON o.organisationID = bfd.entityID
					INNER JOIN person p ON p.organisationID = o.organisationID
					where p.personID = #request.relayCurrentUser.PersonID# and fg.entitytypeid = 2)
					or pf.flagID in (select distinct bfd.flagID from booleanFlagData bfd
					INNER JOIN flag f ON f.flagid = bfd.flagid
					INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid
					INNER JOIN person p ON p.personID = bfd.entityID
					where p.personID = #request.relayCurrentUser.PersonID# and fg.entitytypeid = 0)
					)

				</cfif>

				<cfif arguments.productCategoryID neq 0>
					AND pc.productCategoryID = #arguments.productCategoryID#
				</cfif>

				<cfif arguments.productGroupID neq 0>
					AND p.productGroupID = #arguments.productGroupID#
				</cfif>
				order by pc.title_defaultTranslation, isNull(pg.title_defaultTranslation,pg.description_defaultTranslation),left(p.title_defaultTranslation,37), p.sortOrder
		</cfquery>

		<cfreturn getProductAndGroupAndCatList>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLimitedProductAndGroupAndCatList" hint="Same as above, only limited by the passed list of entity IDs">

		<cfargument name="productCatalogueCampaignID" type="numeric" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="entityLimit" type="string" required="yes">
		<cfargument name="entityIDLimit" type="string" required="yes">
		<cfargument name="productCategoryID" type="numeric" default="0">
		<cfargument name="productGroupID" type="numeric" default="0">
		<cfargument name="showSKU" type="numeric" default="0">
		<cfargument name="countryRestrictionOverrideList" type="string" default="">  <!--- NJH 2008/04/14 - bring back products only in this country override list. Used in promotion edit.--->
		<cfargument name="orderOverride" type="string" default="" required="false">
		<cfargument name="accrualMethod" type="string" default="" required="false" hint="M=manual;A=automatic"> <!--- NJH 2009/06/25 Bug Fix Sony Issue 2395 - restrict to certain accrual methods --->

		<cfscript>
			var getLimitedProductAndGroupAndCatList = "";
		</cfscript>

		<cfquery name="getLimitedProductAndGroupAndCatList" datasource="#application.SiteDataSource#">
			SELECT distinct p.productID,
				left(<cfif showSKU>p.SKU+' '+</cfif>p.title_defaultTranslation,37)
				as productDescription,p.sortOrder,
				pg.productGroupID,
				isNULL(pg.[title_defaultTranslation],pg.[description_defaultTranslation]) as productGroupDescription,
				p.discountprice, pg.IncentiveClaimRequiredFields,
				pc.title_defaultTranslation as productCategoryName, pc.productCategoryID,
				<!--- NJH 2008/04/14 - return the isoCode for products to help distinguish the products in different countries. User in promotion edit- --->
                <!--- case when p.countryID = 0 then 'All Countries' else c.isoCode end as isoCode --->
                ISNULL( dbo.getCountryScopeList(p.productid,'product',1,2), 'All Countries') as isoCode
				FROM         productCategory pc INNER JOIN
                      ProductGroup pg ON pc.ProductCategoryID = pg.ProductCategoryID
                     inner join
                      Product p on p.ProductGroupID = pg.ProductGroupID
                      left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid
				where p.campaignID in (#arguments.productCatalogueCampaignID#)
				<cfif isDefined("arguments.countryRestrictionOverrideList") and arguments.countryRestrictionOverrideList neq "">
                    <!--- keeping in line with current functionality --->
                    and (p.hascountryScope = 0 or vcs.countryid in ( <cf_queryparam value="#arguments.countryRestrictionOverrideList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
				<cfelse>
                    and (p.hascountryScope = 0 or vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">)
				</cfif>
				and p.active=1
				and p.deleted = 0
				and pg.active=1
				<!--- <cfif 1 eq 0> ---><!--- request.allowPromotionsOnAutomatedAccruals eq false --->
				<!--- NJH 2009/06/25 Bug Fix Sony Issue 2395 - if accrual method is passed in, filter the products --->
				<cfif listFindNoCase("M,A",arguments.accrualMethod)>
					and accrualMethod =  <cf_queryparam value="#arguments.accrualMethod#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif arguments.productCategoryID neq 0>
					AND pc.productCategoryID = #arguments.productCategoryID#
				</cfif>

				<cfif arguments.productGroupID neq 0>
					AND p.productGroupID = #arguments.productGroupID#
				</cfif>

				<cfif not compareNoCase(arguments.entityLimit, "productCategoryID")>
					and pc.#arguments.entityLimit# NOT  IN ( <cf_queryparam value="#arguments.entityIDLimit#" CFSQLTYPE="cf_sql_integer"  list="true"> )
				<cfelseif not compareNoCase(arguments.entityLimit, "productGroupID")>
					and p.productGroupID  NOT IN ( <cf_queryparam value="#arguments.entityIDLimit#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<cfelseif not compareNoCase(arguments.entityLimit, "productID")>
					and p.productID  NOT IN ( <cf_queryparam value="#arguments.entityIDLimit#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>

				order by
				<cfif arguments.orderOverride neq "">
					#arguments.orderOverride#
				<cfelse>
					pc.title_defaultTranslation, pg.title_defaultTranslation, p.title_defaultTranslation, p.sortOrder
				</cfif>
		</cfquery>

		<cfreturn getLimitedProductAndGroupAndCatList>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductGroupCategory" hint="Get product categories that the passed product groups belong to">

		<cfargument name="productGroupList" type="string" required="true">
		<cfargument name="productFamilyID" type="numeric" default="0">

		<cfscript>
			var getProductGroupCategory = "";
		</cfscript>

		<cfquery name="getProductGroupCategory" datasource="#application.SiteDataSource#">
			SELECT productCategoryID, title_defaulttranslation as productcategoryName
			FROM productCategory
			WHERE productCategoryID IN
			(SELECT DISTINCT productCategoryID from productGroup where productGroupID  IN ( <cf_queryparam value="#arguments.productGroupList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			<cfif arguments.productFamilyID>
				AND productFamilyID = #arguments.productFamilyID#
			</cfif>
		</cfquery>

		<cfreturn getProductGroupCategory>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductFamily" hint="Get product categories families based on the passed product categories">

		<cfargument name="productCategoryList" type="string" required="true">


		<cfscript>
			var getProductFamily = "";
		</cfscript>

		<cfquery name="getProductFamily" datasource="#application.SiteDataSource#">
			SELECT DISTINCT pf.productFamilyID, pf.name
			FROM productFamily pf, productCategory pc
			WHERE pf.productFamilyID = pc.productFamilyID
			AND pc.productcategoryID  IN ( <cf_queryparam value="#arguments.productCategoryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>

		<cfreturn getProductFamily>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductCategories" hint="Get product categories based on the passed product categories">

		<cfargument name="productCategoryList" type="string" required="true">


		<cfscript>
			var getProductFamily = "";
		</cfscript>

		<cfquery name="getProductFamily" datasource="#application.SiteDataSource#">
			SELECT DISTINCT pc.productCategoryID, pc.productCategoryName
			FROM productCategory pc
			WHERE pc.productCategoryID  IN ( <cf_queryparam value="#arguments.productCategoryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			order by pc.productCategoryName
		</cfquery>

		<cfreturn getProductFamily>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductCategoriesForCampaign" hint="Get product categories based on the passed product catalogue" validvalues="true">
		<cfargument name="productCampaignID" type="numeric" required="true">

		<cfscript>
			var getProductCategory = "";
		</cfscript>

		<cfquery name="getProductCategory" datasource="#application.SiteDataSource#">
			SELECT DISTINCT pc.productCategoryID, pc.title_defaultTranslation as productCategoryName
			FROM productGroup pgp, productCategory pc, product p
			WHERE pc.productCategoryID = pgp.productCategoryID
			AND pgp.productGroupID = p.productGroupID
			<cfif arguments.productCampaignID neq 0>
			AND p.campaignID = #arguments.productCampaignID#
			</cfif>
		</cfquery>

		<cfreturn getProductCategory>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductGroups" hint="Get product groups based on the passed product group IDs">
		<cfargument name="productGroupList" type="string" required="true">
		<cfargument name="allowEmptyDescription" type="boolean" default="true"> <!--- LID 5400 NJH 2011/01/20 this is set to false in promotion edit, as blank descriptions cause the TwoSelectsComboList to fall over --->

		<cfscript>
			var getProductGroup = "";
		</cfscript>

		<cfquery name="getProductGroup" datasource="#application.SiteDataSource#">
			SELECT DISTINCT productGroupID, --productGroupName = case when Description is null then productGroupName else Description end,
				isNull(description_defaultTranslation,title_defaultTranslation) as productGroupName,
				<cfif not arguments.allowEmptyDescription>
					case when len(description) = 0 then ' ' else description end
				<cfelse>
					description
				</cfif> as productGroupDescription
			FROM productGroup
			WHERE productGroupID  IN ( <cf_queryparam value="#arguments.productGroupList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			order by productGroupDescription
		</cfquery>

		<cfreturn getProductGroup>

	</cffunction>

	<!--- START: AJC 2008-09-23 CR-SNY657 Created function to get all products for a promotion --->
	<cffunction access="public" name="getProductsForPromotion" hint="Get product groups based on the passed product categories">

		<cfargument name="RWPromotionID" type="string" default="">

		<cfscript>
			var qry_Get_RWEntities = "";
			var qry_Get_RWProducts = "";
			var qry_Get_RWProductGroups = "";
			var qry_Get_RWProductCategories = "";
			var RWProductIDList = "";
			var RWProductGroupIDList = "";
			var RWProductCategoryIDList = "";
			var qry_get_Products = "";

			qry_Get_RWEntities=getPromotionScopeEntities(RWPromotionID);
		</cfscript>

		<cfif qry_Get_RWEntities.recordcount gt 0>
			<cfoutput query="qry_Get_RWEntities">
				<cfswitch expression="#qry_Get_RWEntities.entity#">
					<cfcase value="productID">
						<cfscript>
							qry_Get_RWProducts = application.com.relayIncentive.getPromotionScope(arguments.RWPromotionID,qry_Get_RWEntities.entity);

							if(qry_Get_RWProducts.recordcount gt 0)
							{
								RWProductIDList=valuelist(qry_Get_RWProducts.entityID);
							}
						</cfscript>
					</cfcase>
					<cfcase value="productgroupID">
						<cfscript>
							qry_Get_RWProductGroups = application.com.relayIncentive.getPromotionScope(arguments.RWPromotionID,qry_Get_RWEntities.entity);
							if(qry_Get_RWProductGroups.recordcount gt 0)
							{
								RWProductGroupIDList=valuelist(qry_Get_RWProductGroups.entityID);
							}
						</cfscript>
					</cfcase>
					<cfcase value="ProductCategoryID">
						<cfscript>
							qry_Get_RWProductCategories = application.com.relayIncentive.getPromotionScope(arguments.RWPromotionID,qry_Get_RWEntities.entity);
							if(qry_Get_RWProductCategories.recordcount gt 0)
							{
								RWProductCategoryIDList=valuelist(qry_Get_RWProductCategories.entityID);
							}
						</cfscript>
					</cfcase>
				</cfswitch>
			</cfoutput>
		</cfif>

		<!--- NYB 29-09-2008 CR-SNY658 - changed last or condition from  'productGroup with (nolock) where productFamilyID IN' to 'productGroupCategory with (nolock) where productCategoryID IN' --->
		<cfquery name="qry_get_Products" datasource="#application.SiteDataSource#">
			select p.productID
			FROM product p with (nolock)
			WHERE 1=2
			<cfif len(RWProductIDList) gt 0>
				or productID  IN ( <cf_queryparam value="#RWProductIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			<cfif len(RWProductGroupIDList) gt 0>
				or productgroupID  IN ( <cf_queryparam value="#RWProductGroupIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			<cfif len(RWProductCategoryIDList) gt 0>
				or productgroupID IN (select productgroupID from productGroup with (nolock) where productCategoryID  IN ( <cf_queryparam value="#RWProductCategoryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			</cfif>
			order by p.productID
		</cfquery>


		<cfreturn qry_get_Products>

	</cffunction>
<!--- END: AJC 2008-09-23 CR-SNY567 Created function to get all products for a promotion --->

<!--- NJH 2009/03/11 CR-SNY675 - changed query slightly to use inner joins and with (noLock) --->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductGroupsForCategory" hint="Get product groups based on the passed Product Category">
		<cfargument name="productCampaignID" type="numeric" required="true">
		<cfargument name="productCategoryID" type="numeric" default="0">

		<cfscript>
			var getProductGroup = "";
		</cfscript>

		<cfquery name="getProductGroup" datasource="#application.SiteDataSource#">
			SELECT DISTINCT pg.productGroupID, isNull(pg.title_defaultTranslation,pg.description_defaultTranslation) as productGroupName
			<!--- 2009-05-05 MDC - commented out as per LHID 2180
			<cfif arguments.showNullForSelect>
			</cfif>
			--->
			FROM  product p with (noLock) inner join productGroup pg with (noLock)
				on pg.productGroupID = p.productGroupID inner join productGroup pgp with (noLock)
				on pg.productGroupID = pgp.productGroupID
			where 1=1
			<cfif structKeyExists(arguments,"productCampaignID")>
				<!--- 2009-05-05 MDC - added in an and as per LHID 2180 --->
				AND p.campaignID = #arguments.productCampaignID#
			</cfif>
			<cfif arguments.productCategoryID>
				AND pgp.productCategoryID = #arguments.productCategoryID#
			</cfif>
		</cfquery>

		<cfreturn getProductGroup>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProducts" hint="Get products based on the passed product IDs">

		<cfargument name="productList" type="string" required="true">
		<cfargument name="returnISOCode" type="boolean" default="false">  <!--- NJH 2008/04/14 returning the iso code for products in promotion screen so we can identity which products are in which countries--->
		<cfargument name="showSKU" type="boolean" default="no">
		<cfargument name="productGroupId" type="numeric" required="false">
        <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

		<cfscript>
			var getProduct = "";
		</cfscript>

		<cfquery name="getProduct" datasource="#application.SiteDataSource#">
			SELECT DISTINCT p.productID, left(<cfif showSKU>p.SKU+' '+</cfif>p.title_defaultTranslation,37) as productDescription
			<cfif arguments.returnISOCode>
			    <!--- , case when p.countryID = 0 then 'All Countries' else c.isoCode end as isoCode --->
                , ISNULL( dbo.getCountryScopeList(p.productid,'product',1,2), 'All Countries') as isoCode
			</cfif>
			FROM product p
            left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
			<cfif structKeyExists(arguments,"productGroupId")>
				inner join productGroup pg on p.productGroupID = pg.productGroupID
			</cfif>
			WHERE 1=1
            and (p.hascountryScope = 0 or vcs.countryid is not null)
			<cfif structKeyExists(arguments,"productList") and arguments.productList neq "">
				and p.productID  IN ( <cf_queryparam value="#arguments.productList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			<cfif structKeyExists(arguments,"productGroupId")>
				and p.productGroupId = #arguments.productGroupId#
			</cfif>
			order by productDescription
		</cfquery>

		<cfreturn getProduct>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="isPersonRegistered" returnType="query" hint="check if the person is already registered">

		<cfargument name="personID" type="numeric" required="true">

		<cfscript>
			var getPersonRegistration = "";
		</cfscript>

		<cfquery name="getPersonRegistration" DATASOURCE="#application.SiteDataSource#">
			SELECT p.firstname + '' + p.lastname as name
			FROM RWPersonAccount pa, person p
			WHERE p.personID = pa.personID
			AND pa.personID = #arguments.personID#
			and pa.accountdeleted=0
		</cfquery>

		<cfreturn getPersonRegistration>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="verifyClaim" hint="change the RWTransactionType from CL to AC">

		<cfargument name="RWTransactionID" type="numeric" required="true">

		<cfscript>
			var verifyClaim = "";
		</cfscript>

		<cfquery name="verifyClaim" DATASOURCE="#application.SiteDataSource#">
			UPDATE RWTransaction
			SET RWTransactionTypeID = CASE WHEN RWTransactionTypeID = 'CL' THEN 'AC'
										WHEN RWTransactionTypeID = 'LS' THEN 'LA' END
			WHERE RWTransactionID = #arguments.RWTransactionID# and RWTransactionTypeID in ('CL','LS')
		</cfquery>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             This will change prize status to approved
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="ApprovePrize" hint="change orderstatus form pending to approved">

		<cfargument name="orderID" type="numeric" required="true">

		<cfscript>
			var ApprovePrize = "";
		</cfscript>

		<cfquery name="ApprovePrize" DATASOURCE="#application.SiteDataSource#">
			UPDATE orders
			set orderstatus = 2
			WHERE orderID = #arguments.orderID#
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             This will change prize status to Cancelled
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="CancelledPrize" hint="change orderstatus form pending to Cancelled">
		<cfargument name="orderID" type="numeric" required="true">

		<cfscript>
			var CancelledClaim = "";
		</cfscript>

		<cfquery name="CancelledClaim" DATASOURCE="#application.SiteDataSource#">
			UPDATE orders
			set orderstatus = 5
			WHERE orderID = #arguments.orderID#
		</cfquery>

	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="cancelClaim" hint="change the RWTransactionType to CN from AC & LA">

		<cfargument name="RWTransactionID" type="numeric" required="true">

		<cfscript>
			var cancelClaim = "";
		</cfscript>

		<cfquery name="cancelClaim" DATASOURCE="#application.SiteDataSource#">
			UPDATE RWTransaction
			SET RWTransactionTypeID = 'CN'
			WHERE RWTransactionID = #arguments.RWTransactionID# and RWTransactionTypeID in ('AC','LA','CL','LS')
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="disablePersonalAccount" hint="Disable personal account for the supplied RWPersonAccountID.">

		<cfargument name="RWPersonAccountID" type="numeric" required="true">

		<cfscript>
			var disablePersonalAccount = "";
		</cfscript>

		<cfquery name="disablePersonalAccount" DATASOURCE="#application.SiteDataSource#">
			UPDATE RWPersonAccount
				SET accountDeleted = 1, deleted = getDate(), deletedBy = #request.relayCurrentUser.PersonID#,
				updated = getDate(), updatedBy = #request.relayCurrentUser.PersonID#
			WHERE RWPersonAccountID = #arguments.RWPersonAccountID#
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="enablePersonalAccount" hint="Enables the personal account for the supplied RWPersonAccountID.">

		<cfargument name="RWPersonAccountID" type="numeric" required="true">

		<cfscript>
			var enablePersonalAccount = "";
		</cfscript>

		<cfquery name="enablePersonalAccount" DATASOURCE="#application.SiteDataSource#">
			UPDATE RWPersonAccount
				SET accountDeleted = 0, deleted = null, deletedBy = null,
				updated = getDate(), updatedBy = #request.relayCurrentUser.PersonID#
			WHERE RWPersonAccountID = #arguments.RWPersonAccountID#
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getDistiName" hint="get name of the distributor organisation">

		<cfargument name="distiID" type="numeric" required="true">

		<cfscript>
			var getDistiName = "";
		</cfscript>

		<cfquery name="getDistiName" DATASOURCE="#application.SiteDataSource#">
			SELECT organisationName
			FROM organisation
			WHERE organisationID = #arguments.distiID#
		</cfquery>

		<cfreturn getDistiName.organisationName>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deletePointsFromCompanyAccount" hint="remove all the points from an account">
		<cfargument name="organisationID" default="1" type="numeric">

		<cfset var getAccountID = "">
		<cfset var localAccountID = "">
		<cfset var deleteFromRWTS = "">
		<cfset var deleteFromRWPA = "">
		<cfset var deleteFromRWTI = "">

		<CFTRANSACTION>

		<cfquery name="getAccountID" DATASOURCE="#application.SiteDataSource#">
			SELECT accountID
			FROM RWCompanyAccount
			WHERE organisationID = #arguments.organisationID#
		</cfquery>

		<cfset localAccountID = getAccountID.accountID>

		<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransactionSplit
			WHERE RWtransactionID IN (SELECT RWTransactionID from RWTransaction WHERE accountID =  <cf_queryparam value="#localAccountID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>

		<cfquery name="deleteFromRWPA" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWPointsAccrued
			WHERE accountID =  <cf_queryparam value="#localAccountID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransactionItems
			WHERE RWtransactionID IN (SELECT RWTransactionID from RWTransaction WHERE accountID =  <cf_queryparam value="#localAccountID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>

		<cfquery name="deleteFromRWTI" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransaction
			WHERE accountID =  <cf_queryparam value="#localAccountID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		</CFTRANSACTION>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deleteTransaction" hint="delete an open transaction">
		<cfargument name="RWTransactionID" type="numeric" required="yes">

		<cfset var chechTransactionOpen = "">
		<cfset var deleteFromRWTS = "">
		<cfset var deleteFromRWTI = "">

		<cfif getUserRightsForRWTransaction(RWTransactionID=arguments.RWTransactionID).edit>
			<CFTRANSACTION>

				<!---Check transaction open--->
				<cfquery name="chechTransactionOpen" DATASOURCE="#application.SiteDataSource#">
					SELECT RWTransactionID FROM RWTransaction
					WHERE RWTransactionID = #arguments.RWTransactionID#
					AND RWTransactionTypeID = 'CI'
				</cfquery>

				<cfif chechTransactionOpen.recordCount eq 1>

					<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM RWTransactionSplit
						WHERE RWtransactionID = #arguments.RWTransactionID#
					</cfquery>

					<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM RWTransactionItems
						WHERE RWtransactionID = #arguments.RWTransactionID#
					</cfquery>

					<cfquery name="deleteFromRWTI" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM RWTransaction
						WHERE RWtransactionID = #arguments.RWTransactionID#
					</cfquery>

					<cfreturn 1>

				<cfelse>

					<cfreturn 0>

				</cfif>

			</CFTRANSACTION>
		</cfif>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deletePointsFromAccount" hint="remove all the points from a personal account">
		<cfargument name="userID" required="yes" type="numeric">

		<cfset var deleteFromRWTS = "">
		<cfset var deleteFromRWTI = "">

		<CFTRANSACTION>

		<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransactionSplit
			WHERE RWtransactionID IN (SELECT RWTransactionID from RWTransaction WHERE personID = #arguments.userID#)
		</cfquery>

		<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransactionItems
			WHERE RWtransactionID IN (SELECT RWTransactionID from RWTransaction WHERE personID = #arguments.userID#)
		</cfquery>

		<cfquery name="deleteFromRWTI" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransaction
			WHERE personID = #arguments.userID#
		</cfquery>

		</CFTRANSACTION>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="unregisterUser" hint="remove user from an account">
		<cfargument name="userID" required="yes" type="numeric">

		<cfset var deleteFromRWTS = "">
		<cfset var deleteFromRWTI = "">
		<cfset var deleteFromRWPA = "">

		<CFTRANSACTION>

		<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransactionSplit
			WHERE RWtransactionID IN (SELECT RWTransactionID from RWTransaction WHERE personID = #arguments.userID#)
		</cfquery>

		<cfquery name="deleteFromRWTS" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransactionItems
			WHERE RWtransactionID IN (SELECT RWTransactionID from RWTransaction WHERE personID = #arguments.userID#)
		</cfquery>

		<cfquery name="deleteFromRWTI" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWTransaction
			WHERE personID = #arguments.userID#
		</cfquery>

		<cfquery name="deleteFromRWPA" DATASOURCE="#application.SiteDataSource#">
			DELETE FROM RWPersonAccount
			WHERE personID = #arguments.userID#
		</cfquery>

		</CFTRANSACTION>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkExclusiveCompanyPromotions" hint="Checks whether exclusive company promotion was already used">
		<cfargument name="RWPromotionID" type="numeric" required="yes">
		<cfargument name="accountID" type="numeric" required="yes">

		<cfset var getExclusiveCompanyPromotions = "">
		<!---Now check whether company has already claimed exclusive promotions
		for this period--->
		<cfquery name="getExclusiveCompanyPromotions" DATASOURCE="#application.SiteDataSource#">
			SELECT ti.RWTransactionItemsID
			FROM RWTransactionItems ti, RWTransaction t
			WHERE t.RWTransactionID = ti.RWTransactionID
			AND t.accountID = #arguments.accountID#
			AND ti.RWPromotionID = #arguments.RWPromotionID#
		</cfquery>

		<cfreturn getExclusiveCompanyPromotions.recordCount>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkExclusivePersonalPromotions" hint="Checks whether exclusive promotion promotion was already used">
		<cfargument name="RWPromotionID" type="numeric" required="yes">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var getExclusivePersonalPromotions = "">

		<!---Now check whether company has already claimed exclusive promotions
		for this period--->
		<cfquery name="getExclusivePersonalPromotions" DATASOURCE="#application.SiteDataSource#">
			SELECT ti.RWTransactionItemsID
			FROM RWTransactionItems ti, RWTransaction t
			WHERE t.RWTransactionID = ti.RWTransactionID
			AND t.personID = #arguments.personID#
			AND ti.RWPromotionID = #arguments.RWPromotionID#
		</cfquery>

		<cfreturn getExclusivePersonalPromotions.recordCount>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             getActivePromotions Finds all active promotions
			 Heavily edited to faciliatate promotion points [godfrey.smith]- April 02, 2008, 13:45:10 PM
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" output="true" name="getActivePromotions" hint="Finds all active promotions">
		<cfargument name="countryID" type="numeric" required="yes"> <!--- NJH 2008/04/07 Note: countryID doesn't seem to be used. Should it be? --->
		<cfargument name="triggerPoint" type="string" required="yes">
		<cfargument name="nowDate" type="date" required="yes">
		<!--- 'RWTransactionID now made required' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 15:48:19 PM --->
		<cfargument name="RWTransactionID" type="numeric" default="0" required="yes">

		<cfargument name="pointsPassed" type="numeric" default="0">
		<cfargument name="productID" type="numeric" default="0">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">  <!--- NJH 2008/04/07 was listFirst(cookie.user,'-') --->

		<cfset var qGetActivePromotions = "">
		<cfset var loopstartdate = "">
		<cfset var loopenddate = "">
		<cfset var accountID = "">
		<cfset var runCondition = "">
		<cfset var getProductQuantity = "">
		<cfset var getProductValue = "">
		<cfset var getPromotionScope = "">
		<cfset var thepointsToAdd = 0> <!--- NJH 2008/05/27 --->
		<cfset var pointsToAdd= "">
		<cfset var promotionExclusion= 0>
		<cfset var exclusionPromotionID = "">
		<cfset var exclusionFlagID = "">
		<cfset var entityLoopList = "">
		<cfset var anyErrors = "">
		<cfset var flagEntity = "">
		<cfset var promotionUsed = "">
		<cfset var wentHere = "">
		<cfset var totalPoints = "">
		<cfset var promotionExclusionIndex = "">
		<cfset var ixProductID = "">
		<cfset var getCompanyAccount = "">
		<cfset var approvalRWTransactionID = "">

		<!---  Promo Date Change - GCC - backwards compatibility --->
		<cfparam name="request.promotionDate" type="string" default="claimDate">

		<!---Get all currently active promotions for this trigger point and the country user is flagged with--->
		<!--- 2015-09-03 AHL Making dates consistents (Query Change) --->
		<cfquery name="qGetActivePromotions" DATASOURCE="#application.SiteDataSource#">
			SELECT *
			FROM RWPromotion p, RWPromotionType pt
			WHERE p.RWPromotionTypeID = pt.RWPromotionTypeID
			AND triggerPoint =  <cf_queryparam value="#arguments.triggerPoint#" CFSQLTYPE="CF_SQL_VARCHAR" >
			AND Active=1
			<cfif request.promotionDate EQ "invoiceDate">
			<!---  Promo Date Change - GCC - promotion never ends when it has invoice date restrictions
			 the invoivce date must be between the promotion start date and end date
			 which is checked in the output further down
			 but the promotion will always come back as active
			 --->
			<cfelse><!--- claimDate / registrationDate --->
			AND startDate <=  <cf_queryparam value="#CreateODBCDate(arguments.nowdate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND isNull(endDate,<cf_queryparam value="#CreateODBCDate(arguments.nowdate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >) >=  <cf_queryparam value="#CreateODBCDate(arguments.nowdate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  /* 2015-09-03 AHL Making dates consistents (Query Change) */
			</cfif>
			AND (EXISTS (SELECT 1
                	FROM countryScope csc
                  	WHERE csc.entity in ('Promotion','rwPromotion' )	/* 2015-09-02 AHL including all and new table names */
                	AND csc.entityId = p.RWPromotionID
                   	AND csc.countryId IN (SELECT cgr.countrygroupid
       				FROM person per
       				INNER JOIN location loc ON loc.locationId = per.locationId
               		INNER JOIN countrygroup cgr ON cgr.countrymemberid = loc.countryid
             		WHERE per.personId = #arguments.personID#)))
			ORDER BY p.Precedence
		</cfquery>

		<!---If no promotions, abort processing--->
		<cfif qGetActivePromotions.recordCount eq 0>
			<cfreturn 0>
		</cfif>

		<!---Which account does the user belong to--->
		<cfquery name="getCompanyAccount" DATASOURCE="#application.SiteDataSource#">
			SELECT DISTINCT accountID
			FROM RWPersonAccount
			WHERE personID = #arguments.personID#
		</cfquery>

		<cfset accountID = getCompanyAccount.accountID>
		<!--- 2005/12/06 - GCC - exclusion flag implementation --->
		<cfparam name="promotionExclusion" default="0">
		<!---Loop through active poromotions in order of prececdence---><!--- 'changed from cfoutput' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 15:50:14 PM --->
		<cfloop query="qGetActivePromotions">
			<!--- 'made the query only look at the date and not reference the time using 'LEFT' so whole days are matched and not day boundaries' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 16:29:26 PM --->
			<cfset loopstartdate = LEFT(startdate,10)>
			<cfset loopenddate = LEFT(enddate,10)>
			<cfif structkeyexists(request,"promotionExclusionList") and listlen(request.promotionExclusionList,",") gt 0>
				<cfloop list="#request.promotionExclusionList#" delimiters="," index="promotionExclusionIndex">
					<!--- 2005/12/06 - GCC - exclusion flag implementation --->
					<cfset promotionExclusion = 0>
					<cfset exclusionPromotionID = listfirst(promotionExclusionIndex,":")>
					<cfset exclusionFlagID = listlast(promotionExclusionIndex,":")>
					<!--- if the current promo has an exclusion --->
					<cfif exclusionPromotionID eq RWPromotionID>
						<!--- currently only works for boolean flags --->
						<cfscript>
							flagEntity = application.com.flag.getFlagStructure(exclusionFlagID).entitytype.uniquekey;
							promotionExclusion = application.com.flag.checkBooleanFlagByID(entityID = request.relaycurrentuser[flagEntity], flagID = exclusionFlagID);
						</cfscript>
						<cfif promotionExclusion eq 1>
							<cfbreak>
						</cfif>
					</cfif>
				</cfloop>
			</cfif>

			<cfscript>
				//If it is a promotion which can not be used in conjuction with any other
				if(exclusive eq 1){
					//Check whether it has been used up on company level (by any one user)
					if(companyPromotion eq 1){
						//Check whether the promotion already used up within the company
						promotionUsed = checkExclusiveCompanyPromotions(RWPromotionID, accountID);
					}
					//On personal level
					else{
						//Check whether the promotion already used up by that user
						promotionUsed = checkExclusivePersonalPromotions(RWPromotionID, arguments.personID);
					}
				}
				else{
					promotionUsed = 0;
				}

				//Work out the points
				if(findNoCase("Fixed Points", RWPromotionTypeDescription) neq 0){
					pointsToAdd = qGetActivePromotions.totalPoints;
					wentHere = "1";
				}

				else if(findNoCase("Points Multiplier", RWPromotionTypeDescription) neq 0){
					totalPoints = qGetActivePromotions.totalPoints;
					wentHere = "2";
				}
				else if(findNoCase("Registration", RWPromotionTypeDescription) neq 0){
					pointsToAdd = qGetActivePromotions.totalPoints;
					wentHere = "3";
				}

			</cfscript>

			<!---If promotion exclusive and used up return record count and abort processing--->
			<cfif promotionUsed NEQ 0 or promotionExclusion NEQ 0>
				<cfreturn 0>
			</cfif>

			<!---Otherwise, proces the promotion and apply points--->
			<cfscript>
				getPromotionScope = getPromotionScopeForPromotionID(RWPromotionID);
			</cfscript>

			<!---Loop through the scope--->
			<cfloop query="getPromotionScope">

				<!---If restrictions present for this promotion--->
				<cfif getPromotionScope.recordCount neq 0>

					<!---Run condition--->
 					<cfswitch expression="#entity#">

						<cfcase value="ProductID">
							<!--- 'added DISTINCT to obtain correct results' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 15:53:06 PM --->
							<cfquery name="runCondition" DATASOURCE="#application.SiteDataSource#">
								SELECT DISTINCT productID FROM RWTransactionItems WHERE RWTransactionID = #arguments.RWTransactionID#
								AND productID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
								<cfif request.promotionDate eq "invoiceDate">
								<!--- Promo Date Change - GCC - Get the valid invoice dates --->
								AND isnull(invoiceDate,'1900/01/01') >=  <cf_queryparam value="#loopstartdate#" CFSQLTYPE="CF_SQL_VARCHAR" >
								AND isnull(invoiceDate,'1900/01/01') <=  <cf_queryparam value="#loopenddate#" CFSQLTYPE="CF_SQL_VARCHAR" >
								</cfif>
							</cfquery>
						</cfcase>

						<cfcase value="ProductGroupID">
							<!--- 'added DISTINCT to obtain correct results' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 15:53:06 PM --->
							<cfquery name="runCondition" DATASOURCE="#application.SiteDataSource#">
								SELECT DISTINCT productID FROM RWTransactionItems WHERE RWTransactionID = #arguments.RWTransactionID#
								and productid in (SELECT productID FROM product where productgroupid =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
								<cfif request.promotionDate eq "invoiceDate">
								<!--- Promo Date Change - GCC - Get the valid invoice dates --->
								AND isnull(invoiceDate,'1900/01/01') >=  <cf_queryparam value="#loopstartdate#" CFSQLTYPE="CF_SQL_VARCHAR" >
								AND isnull(invoiceDate,'1900/01/01') <=  <cf_queryparam value="#loopenddate#" CFSQLTYPE="CF_SQL_VARCHAR" >
								</cfif>
							</cfquery>
						</cfcase>

						<cfcase value="ProductCategoryID"><!--- technically should never happen but left in for legacy / scaleability [godfrey] --->
							<cfquery name="runCondition" DATASOURCE="#application.SiteDataSource#">
								SELECT 1
								FROM ProductGroup
								WHERE 1=0
							</cfquery>
						</cfcase>

					</cfswitch>


					<cfif application.testSite eq 2 and isdefined('pointsToAdd')>
						<cfoutput>pointsPassed = #htmleditformat(pointsPassed)# pointsToAdd = #htmleditformat(pointsToAdd)# wentHere = #htmleditformat(wentHere)# entity=#htmleditformat(entity)#;<BR></cfoutput>
					</cfif>

					<cfif runCondition.recordCount NEQ 0>
						<cfset entityLoopList = valuelist(runCondition.productID)>
						<!--- 2005/07/07 GCC get the value of the product to override the total order value passed we dont want to apply the order value as a bonus to each product--->
						<!--- 2008/05/27 GCC & NJH extended loop to include addRwTransactionItems per item in ixProductID
						will create a pp row total in the transaction for each qualifying productID
						entityLoopList should never have more than 1 item in it's list for product promotions
						entityLoopList might contain more than one item for promotions on product group with multiple products in the group
						There will be an entry of promotion points for each qualifying product for product AND product group promotions --->
						<cfloop list="#entityLoopList#" index="ixProductID">
							<!--- 'added try catch block ' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 15:55:17 PM --->
							<cftry>
								<cfset anyErrors = 0>
								<cfif isdefined('wentHere') and wentHere eq 1>
									<!--- fixed points - need to multiply by quantity --->
									<cfquery name="getProductQuantity" DATASOURCE="#application.SiteDataSource#">
										SELECT sum(quantity) as thequantity
										FROM rwtransactionitems
										WHERE productID =  <cf_queryparam value="#ixProductID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND rwtransactionid = #arguments.RWTransactionID#
										AND pointsType='BP'
										<cfif request.promotionDate eq "invoiceDate">
										<!--- Promo Date Change - GCC - Get the valid invoice dates --->
										AND isnull(invoiceDate,'1900/01/01') >=  <cf_queryparam value="#loopstartdate#" CFSQLTYPE="CF_SQL_VARCHAR" >
										AND isnull(invoiceDate,'1900/01/01') <=  <cf_queryparam value="#loopenddate#" CFSQLTYPE="CF_SQL_VARCHAR" >
										</cfif>
									</cfquery>
									<!--- <cfoutput>p2a * gpq #ixProductID# <cfdump var="#pointsToAdd#"> * <cfdump var="#getProductQuantity.thequantity#"><br /></cfoutput> --->
									<cfset thepointsToAdd = pointsToAdd * getProductQuantity.thequantity>

								<cfelseif isdefined('wentHere') and wentHere eq 2>
									<!--- Points Multiplier - need to multiply by quantity --->
									<cfquery name="getProductValue" DATASOURCE="#application.SiteDataSource#">
										SELECT sum(points) as pointsToAdd
										FROM rwtransactionitems
										WHERE productID =  <cf_queryparam value="#ixProductID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND rwtransactionid = #arguments.RWTransactionID#
										AND pointsType='BP'
										<cfif request.promotionDate eq "invoiceDate">
										<!--- Promo Date Change - GCC - Get the valid invoice dates --->
										AND isnull(invoiceDate,'1900/01/01') >=  <cf_queryparam value="#loopstartdate#" CFSQLTYPE="CF_SQL_VARCHAR" >
										AND isnull(invoiceDate,'1900/01/01') <=  <cf_queryparam value="#loopenddate#" CFSQLTYPE="CF_SQL_VARCHAR" >
										</cfif>
									</cfquery>
									<!--- <cfoutput>p2a * gpv #ixProductID# <cfdump var="#totalPoints#"> * <cfdump var="#getProductValue.pointsToAdd#"><br /></cfoutput> --->
									<cfset thepointsToAdd = getProductValue.pointsToAdd * totalPoints>
								<cfelse> <!--- wentHere = 3 --->
									<cfquery name="getProductValue" DATASOURCE="#application.SiteDataSource#">
										SELECT sum(points) as pointsToAdd
										FROM rwtransactionitems
										WHERE productID =  <cf_queryparam value="#ixProductID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND rwtransactionid = #arguments.RWTransactionID#
										AND pointsType='BP'
										<cfif request.promotionDate eq "invoiceDate">
										<!--- Promo Date Change - GCC - Get the valid invoice dates --->
										AND isnull(invoiceDate,'1900/01/01') >=  <cf_queryparam value="#loopstartdate#" CFSQLTYPE="CF_SQL_VARCHAR" >
										AND isnull(invoiceDate,'1900/01/01') <=  <cf_queryparam value="#loopenddate#" CFSQLTYPE="CF_SQL_VARCHAR" >
										</cfif>
									</cfquery>
									<!--- p2a #ixProductID# <cfdump var="#getProductValue.pointsToAdd#"><br /> --->
									<cfset thepointsToAdd = getProductValue.pointsToAdd>
								</cfif>

								<cfif application.testSite eq 2>
									<cfoutput>updatedpointsToAdd = #thepointsToAdd#;<BR><br /></cfoutput>
						</cfif>

								<cfcatch type="any">
									<cfset anyErrors = 1>
									<font color="red"><br />*****ERROR:<br />#htmleditformat(ixProductID)#
									<cfdump var="#cfcatch.message#">
									</font>
								</cfcatch>
							</cftry>
						<!--- 2005/07/07 GCC - Insert the productID of the item in the PP point line
						to ensure a relationship is maintained between the product and the promotion points
						--->

							<!--- 'included a cfif clause to stop errors' [godfrey.smith CR TND 547 - SNY 639]- April 02, 2008, 15:56:49 PM --->
							<cfif anyErrors EQ 0>
								 <cfscript>
									//If transaction active, just add a transaction item
									if(structKeyExists(arguments, "RWTransactionID") and arguments.RWTransactionID neq 0){
										application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=arguments.RWTransactionID,ItemID=#ixProductID#,quantity=1,points=thepointsToAdd,pointsType='PP',RWPromotionID=RWPromotionID);
									}
									//If not, like for registration trigger, create a transaction
									else{
									//put in as approved points - person needs to be the claimant, not the approver
										approvalRWTransactionID = application.com.relayIncentive.RWAccruePoints(PersonID = arguments.PersonID, AccountID=accountID,PointsAmount=pointsToAdd,RWTransactionType='AC',AccruedDate=createODBCDateTime(arguments.nowDate));
										application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=approvalRWTransactionID,ItemID=#ixProductID#,quantity=1,points=thepointsToAdd,pointsType='PP',RWPromotionID=RWPromotionID);
									}
						</cfscript>
							</cfif>
						</cfloop>
					</cfif>
					<!--- /runCondition.recordCount NEQ 0 --->

				<cfelse>

					<cfscript>
						//If transaction active, just add a transaction item
						if(structKeyExists(arguments, "RWTransactionID") and arguments.RWTransactionID neq 0){
							application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=arguments.RWTransactionID,ItemID=0,quantity=1,points=pointsToAdd,pointsType='PP',RWPromotionID=RWPromotionID);
						}
						//If not, like for registration trigger, create a transaction
						else{
						//put in as approved points - person needs to be the claimant, not the approver
							approvalRWTransactionID = application.com.relayIncentive.RWAccruePoints(PersonID = arguments.PersonID, AccountID=accountID,PointsAmount=pointsToAdd,RWTransactionType='AC',AccruedDate=createODBCDateTime(arguments.nowDate));
							application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=approvalRWTransactionID,ItemID=0,quantity=1,points=pointsToAdd,pointsType='PP',RWPromotionID=RWPromotionID);
						}
					</cfscript>

				</cfif>
				<!--- /cfif getPromotionScope.recordCount neq 0 --->
			</cfloop>

			<cfif getPromotionScope.recordCount eq 0>
				 <cfscript>
					//If transaction active, just add a transaction item
					if(structKeyExists(arguments, "RWTransactionID") and arguments.RWTransactionID neq 0){
						application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=arguments.RWTransactionID,ItemID=0,quantity=1,points=thePointsToAdd,pointsType='PP',RWPromotionID=RWPromotionID); /* 2015-09-03 AHL using thPointsToAdd rather pointsToAdd */
					}

					//If not, like for registration trigger, create a transaction
					else{
						//put in as approved points - person needs to be the claimant, not the approver
						approvalRWTransactionID = application.com.relayIncentive.RWAccruePoints(PersonID = arguments.PersonID, AccountID=accountID,PointsAmount=pointsToAdd,RWTransactionType='AC',AccruedDate=createODBCDateTime(arguments.nowDate));
						application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=approvalRWTransactionID,ItemID=0,quantity=1,points=thePointsToAdd,pointsType='PP',RWPromotionID=RWPromotionID); 	 /* 2015-09-03 AHL using thPointsToAdd rather pointsToAdd */
					}
				</cfscript>

			</cfif>
		</cfloop>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updatePromotion" hint="Updates an existing promotion">
		<cfargument name="formStructure" type="struct" required="yes">

		<cfscript>
			var UpdateRecord = "";
			var formStruct = arguments.formStructure;
			var lastUpdated = now();
		</cfscript>

		<!---Now check whether company has already claimed exclusive promotions
		for this period--->
		<cfquery name="UpdateRecord" datasource="#application.SiteDataSource#">
			UPDATE RWPromotion
			   SET RWPromotionCode =  <cf_queryparam value="#formStruct.RWPromotionCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
				  ,RWPromotionDescription =  <cf_queryparam value="#formStruct.RWPromotionDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >
				  <CFIF len(formStruct.StartDate)>
					  ,startDate =  <cf_queryparam value="#CreateODBCdate(formStruct.startDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  </CFIF>
				  <CFIF len(formStruct.endDate)>
					  ,endDate =  <cf_queryparam value="#CreateODBCdate(formStruct.endDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  </CFIF>
				  ,RWPromotionTypeId =  <cf_queryparam value="#formStruct.RWPromotionTypeId#" CFSQLTYPE="CF_SQL_INTEGER" >
				  <CFIF len(formStruct.Rationale)>
					  ,Rationale =  <cf_queryparam value="#formStruct.Rationale#" CFSQLTYPE="CF_SQL_VARCHAR" >
				  </CFIF>
				  <CFIF len(formStruct.totalPoints)>
					  ,totalPoints =  <cf_queryparam value="#formStruct.totalPoints#" CFSQLTYPE="CF_SQL_Integer" >
				  </CFIF>
				  <!--- <CFIF len(formStruct.weekEndingDate)>
					  ,weekEndingDate=#formStruct.weekEndingDate#
				  </CFIF> --->
				  ,triggerPoint =  <cf_queryparam value="#formStruct.triggerPoint#" CFSQLTYPE="CF_SQL_VARCHAR" >
				  ,companyPromotion =  <cf_queryparam value="#formStruct.companyPromotion#" CFSQLTYPE="CF_SQL_bit" >
				  ,lastUpdated =  <cf_queryparam value="#lastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  ,lastUpdatedBy =  <cf_queryparam value="#listGetAt(Cookie.User,1,'-')#" CFSQLTYPE="cf_sql_integer" >
				  ,active =  <cf_queryparam value="#formStruct.active#" CFSQLTYPE="cf_sql_float" >
			 WHERE RWPromotionID =  <cf_queryparam value="#formStruct.RWPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPromotion" hint="Details of selected promotion">

		<cfargument name="RWPromotionID" type="numeric" required="yes">

		<cfscript>
			var getRecord = "";
		</cfscript>

		<CFQUERY name="getRecord" datasource="#application.SiteDataSource#" maxRows=1>
			SELECT *
			  FROM RWPromotion
			 WHERE RWPromotionId = #arguments.RWPromotionID#
		</CFQUERY>

		<cfreturn getRecord>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPromotionType" returntype="query" validValues="true" hint="Details of selected promotion">
		<cfargument name="ModuleCode" type="string"  required="no" default="RW">

		<cfscript>
			var getRWPromotionType = "";
		</cfscript>

		<CFQUERY NAME="getRWPromotionType" datasource="#application.SiteDataSource#">
			SELECT *
			FROM RWPromotionType
			WHERE ModuleCode =  <cf_queryparam value="#ModuleCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>

		<cfreturn getRWPromotionType>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPromotionScope" hint="Limitations of selected promotion">

		<cfargument name="RWPromotionID" type="numeric" required="yes">
		<cfargument name="Entity" type="string" required="yes">

		<cfscript>
			var getScope = "";
		</cfscript>

		<CFQUERY NAME="getScope" datasource="#application.SiteDataSource#">
			SELECT *
			FROM RWPromotionScope
			WHERE RWPromotionID = #arguments.RWPromotionID#
			AND Entity =  <cf_queryparam value="#arguments.entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
			ORDER BY Entity
		</CFQUERY>

		<cfreturn getScope>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPromotionScopeEntities" hint="Limitation types of selected promotion">

		<cfargument name="RWPromotionID" type="numeric" required="yes">

		<cfscript>
			var getEntities= "";
		</cfscript>

		<CFQUERY NAME="getEntities" datasource="#application.SiteDataSource#">
			SELECT DISTINCT Entity
			FROM RWPromotionScope
			WHERE RWPromotionID = #arguments.RWPromotionID#
		</CFQUERY>

		<cfreturn getEntities>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPromotionScopeForPromotionID" hint="Details of selected promotion">

		<cfargument name="RWPromotionID" type="numeric" required="yes">

		<cfscript>
			var getScopeForPromotionID = "";
		</cfscript>

		<CFQUERY NAME="getScopeForPromotionID" datasource="#application.SiteDataSource#">
			SELECT *
			FROM RWPromotionScope
			WHERE RWPromotionID = #arguments.RWPromotionID#

			ORDER BY Entity
		</CFQUERY>

		<cfreturn getScopeForPromotionID>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updatePromotionScope" hint="Details of selected promotion">
		<cfargument name="RWPromotionID" type="numeric" required="yes">
		<cfargument name="entity" type="string" required="yes">
		<cfargument name="entityID" type="string" required="yes">

		<cfscript>
			var deleteFromScope = "";
			var updateScope = "";
			var x = "";
		</cfscript>

		<cftransaction>

<!--- [godfrey.smith CR TND 547 - SNY 639]- March 31, 2008, 10:20:40 AM
'altered this query to make the application treat each item individually as opposed to being treated twice once as product then again as product group item'
----AS WAS----
DELETE FROM RWPromotionScope
WHERE Entity = '#arguments.entity#'
AND RWPromotionID = #arguments.RWPromotionID#
 --->

		<CFQUERY NAME="deleteFromScope" datasource="#application.SiteDataSource#">
			DELETE FROM RWPromotionScope
			WHERE RWPromotionID = #arguments.RWPromotionID#
		</CFQUERY>

		<cfloop from="1" to="#listLen(arguments.entityID)#" index="x">

			<CFQUERY NAME="updateScope" datasource="#application.SiteDataSource#">
				INSERT INTO RWPromotionScope(RWPromotionID,Entity,entityID)
				VALUES(<cf_queryparam value="#arguments.RWPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.entity#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#listGetAt(arguments.entityID, x)#" CFSQLTYPE="CF_SQL_INTEGER" >)
			</CFQUERY>

		</cfloop>

		</cftransaction>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" output="true" name="dumpIt" hint="remove all the points from an account">

		<cfargument name="it" type="any" required="true">

		<cfdump var="#arguments.it#">



	</cffunction>

	<!--- 2005/08/09 GCC Check for registration promotions - get reg date --->
	<cffunction access="public" name="getRegistrationDetails" hint="returns the registration date of a user">
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="flagTextID" type="string" required="yes">

		<cfset var qGetRegistrationDate = "">

		<cfquery name ="qGetRegistrationDate" datasource="#application.SiteDataSource#">
			select b.created as RegDate
			from booleanflagdata b inner join flag f on
			b.flagid = f.flagid
			where entityID = #arguments.personID#
		<cfif isNumeric(arguments.flagTextID)>
			and f.flagID =  <cf_queryparam value="#arguments.flagTextID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelse>
			and f.flagtextID =  <cf_queryparam value="#arguments.flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		</cfquery>

	<cfreturn qGetRegistrationDate>

	</cffunction>

	<!--- 2005/11/03 GCC Check they are an incentive user --->
	<cffunction access="public" name="isIncentiveUser" hint="returns the incentive user status of a user">
		<cfargument name="personID" type="numeric" required="yes">

		<cfset var qIsIncentiveUser = "">

		<cfquery name ="qIsIncentiveUser" datasource="#application.SiteDataSource#">
			select 1 from vRewardsPer where personid = #arguments.personID#
		</cfquery>

		<cfif qIsIncentiveUser.recordcount gt 0>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>

	<cffunction name="canPurchaseTeaserProducts" access="remote" returntype="boolean" hint="This function returns true if there are 3+ products available to buy">
	  	<cfargument name="balance" required="yes" type="numeric">
		<cfargument name="currentCountry" required="yes" type="numeric" hint="countryID">
	  	<cfargument name="teaserProductGroupIDs" required="no" type="string" hint="productGroupID">

	  	<cfscript>
			var qcanPurchaseProductsCheck = "";
			var promoid = "IncentivePrizesCatalogue";
		</cfscript>

		<CFQUERY NAME="qcanPurchaseProductsCheck" datasource="#application.sitedatasource#">
			SELECT 	count(*) as productCount
			FROM
				vProductList p
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = p.productidand and vcs.countryid = <cf_queryparam value="#currentcountry#" cfsqltype="CF_SQL_INTEGER">
			WHERE
						campaignid = (select campaignid from promotion where promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
                        and (p.hascountryScope = 0 or vcs.countryid is not null)
					and not exists(select
						*
					from
						Product P1
                        left join vCountryScope vcs2 on P1.hasCountryScope = 1 and vcs2.entity='product' and vcs2.entityid = P1.productid and vcs2.countryid = <cf_queryparam value="#currentcountry#" cfsqltype="CF_SQL_INTEGER">
					where
						((isNull(vProductList.SKUGroup,'') <> ''	and vProductList.SKUGroup = P1.SKUGroup)
							OR
							vProductList.SKU = P1.sku)
						and vProductList.campaignid = P1.campaignid
						and deleted = 0
						and active=1
					     and (P1.hascountryScope = 0 or vcs2.countryid is not null)
					)
				 )
			)
			and discountprice <= #arguments.balance#
			<cfif isdefined('arguments.teaserProductGroupIDs')>
				and productgroupid  in ( <cf_queryparam value="#arguments.teaserProductGroupIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		</CFQUERY>
		<cfif qcanPurchaseProductsCheck.productCount lt 3>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
  	</cffunction>

	<cffunction name="getIncentiveUsers">
		<cfargument name="CountryID" required="no" type="numeric" default="0" hint="countryID">
		<cfscript>
			var qGetIncentiveUsers = "";
		</cfscript>
		<cfquery name = "qGetIncentiveUsers" datasource="#application.sitedatasource#">
			Select personID, isnull(firstname,' ') + ' ' + isnull(lastname,' ') as fullname,organisation.organisationname, country.countryID, countryDescription
			from person
			inner join location
			on person.locationid = location.locationid
			inner join country
			on location.countryid = country.countryid
			inner join organisation on
			organisation.organisationID = person.organisationID
			where personid in (select personID from vrewardsper)
			<cfif arguments.countryID neq 0>
				and country.countryID = #arguments.countryID#
			</cfif>
			order by organisation.organisationname,isnull(firstname,' ') + ' ' + isnull(lastname,' ')
		</cfquery>
		<cfreturn qGetIncentiveUsers>
	</cffunction>

	<cffunction name="getIncentiveUserCountries">
		<cfargument name="countryRightsScope" required="no" type="boolean" default="false" hint="specifise filter on users country rights">
		<cfscript>
			var qGetIncentiveUserCountries = "";
		</cfscript>
		<cfquery name = "qGetIncentiveUserCountries" datasource="#application.sitedatasource#">
			Select distinct country.countryID, countrydescription
			from person inner join location
			on person.locationid = location.locationid inner join country on location.countryid = country.countryid
			where personid in (select personID from vrewardsper)
			<cfif arguments.countryRightsScope>
				and country.countryID in (#request.relaycurrentuser.countryList#)
			</cfif>
			order by countrydescription
		</cfquery>
		<cfreturn qGetIncentiveUserCountries>
	</cffunction>

	<cffunction name="getAdhocPromotions">
		<!--- returns live adhoc promos for the given countryID--->
		<cfargument name="CountryID" required="yes" type="numeric" hint="countryID">
		<cfscript>
			var qGetAdhocPromotions = "";
		</cfscript>
		<!--- AHL 2015-08-19 Updating the deprected table Promotion to RWPromotion --->
		<cfquery name = "qGetAdhocPromotions" datasource="#application.sitedatasource#">
				SELECT *
			FROM RWPromotion p, RWPromotionType pt,countryScope csc
			WHERE p.RWPromotionTypeID = pt.RWPromotionTypeID
			AND csc.entity in ('RWPromotion','Promotion')
            AND csc.entityId = p.RWPromotionID
			AND triggerPoint = 'adhoc'
			AND csc.countryId = #arguments.countryID#
			AND Active=1
			order by RWpromotionDescription
		</cfquery>
		<cfreturn qGetAdhocPromotions>
	</cffunction>


	<cffunction name="getActivePeopleAtAccount" returnType="query" hint="Returns active people at a company account">
		<cfargument name="accountID" required="false" type="numeric">
		<cfargument name="organisationID" required="false" type="numeric">

		<cfscript>
			var getActivePeople="";
		</cfscript>

		<cfquery name="getActivePeople" datasource="#application.siteDataSource#">
			select pa.rwPersonAccountID,p.personID from rwPersonAccount pa inner join rwCompanyAccount ca
				on pa.accountID = ca.accountID inner join person p
				on pa.personID = p.personID
			where pa.accountDeleted=0
				and pa.testAccount=0
				<cfif isDefined("arguments.accountID")>
					and ca.accountID = #arguments.accountID#
				<cfelseif isDefined("arguments.organisationID")>
					and ca.organisationID = #arguments.organisationID#
				</cfif>
		</cfquery>

		<cfreturn getActivePeople>
	</cffunction>

	<cffunction access="public" name="deleteProducts" hint="This function will set deleted flag to 1 for any products that are not required">
		<cfargument name="activeproducts" type="string" required="yes">
		<cfargument name="campaignID" type="numeric" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">

		<cfset var setstatedeleted = "">

		<cfquery name="setstatedeleted" datasource="#application.siteDataSource#">
			update product
			set deleted = 1
			where sku not in (<cf_queryparam value="#arguments.activeproducts#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			and campaignID = #arguments.campaignID#
			and countryID = #arguments.countryID#
			and deleted = 0
		</cfquery>
	</cffunction>

	<cffunction access="public" name="importimages" hint="This function will import images for a url">
		<cfargument name="standardTableImport" type="query" required="yes">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="fileTypes" type="string" default="thumbnail"> <!--- NJH 2009/07/06 P-SNY655 --->

		<!--- NJH 2009/07/06 P-SNY655 var'd some variables --->
		<cfscript>
			var localImagePath = "";
			var localthumbnailname = "";
			var remotefilename = "";
			var remoteImagePath = "";
			var UploadThumbNails = "";
			var imageUrl = "";
			var localThumbnailImagePath = "";
			var imagePath = "";
			var fileType = "";
		</cfscript>

		<!--- NJH 2009/07/06 P-SNY655 - look for both standard and thumbnail images --->
		<!---
			NJH 2012/12/11 Commented this out, as the path has changed to content/linkImages/product/#productID#/
			But the call to this function isn't found in core, and so I suspect it's not used anymore.. but if so, this function will nee changing slightly!
			I've broken it on purpose so that it will get fixed properly.

		<cfset localThumbnailImagePath = '#application.paths.content#\ProductThumbs\CountryID#arguments.countryID#'>
		<cfset localImagePath = '#application.paths.content#\ProductPhotoHires\CountryID#arguments.countryID#'>
		<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=localThumbnailImagePath)>
		<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=localImagePath)> --->

		<cfloop query="arguments.standardTableImport">
			<cfloop list="#arguments.fileTypes#" index="fileType">
		<cfif masterload EQ 1>
			<!--- <cfset localthumbnailname = 'IncentivePrizesCatalogue-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg'> --->
			<cfset localthumbnailname = 'thumb_#productID#.jpg'>

					<cfset imageUrl = evaluate("standardTableImport."&fileType&"Image")>
					<cfif len(imageURL) gt 0>
						<cfset remotefilename= #reverse(gettoken(reverse(imageUrl),1,"/"))#>
						<cfset remoteImagePath= reverse(removechars(reverse(imageUrl), 1, find("/", reverse(imageUrl))))>

						<cfif fileType eq "thumbnail">
							<cfset imagePath = localThumbnailImagePath>
						<cfelse>
							<cfset imagePath = localImagePath>
						</cfif>

						<cfif remotefilename neq "" and remoteImagePath neq "">
			<cfset UploadThumbNails = application.com.relayImage.getRemoteImage(
				remoteImagePath=remoteImagePath,
				remoteFileName=remotefilename,
								localImagePath=imagePath,
				localFileName=localthumbnailname,
				overwrite=True
			)>
		</cfif>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
	</cffunction>

	<cffunction access="public" name="DebitPoints" hint="This function will allow you to debit points from an account">

		<cfargument name="AccountID" required="yes" type="numeric" hint="accountID of the account to be debited">
		<cfargument name="AccruedDate" required="yes" type="numeric" hint="Date the points where accrued on">
		<cfargument name="PointsAmount" required="yes" type="numeric" hint="negative points to be debited">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personid#" hint="PersonID who is taking the points away">
		<cfargument name="endUserCompanyName" type="string" hint="organisation loosing the points">
		<cfargument name="ItemID" type="numeric" default="0" hint="The Id of the item">
		<cfargument name="quantity" type="numeric" default="1" hint="The amount to be brought">
		<cfargument name="pointstype" type="string" default="pp" hint="organisation loosing the points">
		<cfargument name="RWpromotionID" type="string" default="pp" hint="The promotion the refund should be set up against ">


		<cfscript>
			var qDebitPoints = "";
		</cfscript>

	</cffunction>


	<!--- NJH 2009/03/05 CR-SNY671 --->
	<!--- NJH 2009/07/02 P-FNL069 - implemented some rights checking --->
	<cffunction name="getInvoiceDocumentDetails" access="public" hint="Gets the details for an invoice document">
		<cfargument name="entityType" type="string" default="rwTransaction" hint="Currently can be rwTransaction or cashbackClaim">
		<cfargument name="entityID" type="numeric" required="true">

		<cfscript>
			var getInvoiceDocument = "";
			var invoiceDocQry = queryNew("temp");
			var hasRights = false;
			var doesUserHaveRights = "";
		</cfscript>

		<!--- NJH 2009/07/02 P-FNL069 if we're not logged in or on the portal, determine if we rights to see the invoice document. --->
		<cfif (not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn)>
			<cfquery name="doesUserHaveRights" datasource="#application.siteDataSource#">
				select * from #arguments.entityType# where #arguments.entityType#ID = #arguments.entityID#
					and personID = #request.relayCurrentUser.personID#
			</cfquery>

			<cfif doesUserHaveRights.recordCount gt 0>
				<cfset hasRights = true>
			</cfif>
		<!--- have to assume at the moment that if we're an internal user, we have rights --->
		<cfelseif request.relayCurrentUser.isInternal>
			<cfset hasRights = true>
		</cfif>

		<cfif hasRights>
			<cf_relatedFile action="variables" entity="#arguments.entityID#" entitytype="#arguments.entityType#">
			<cfset invoiceDocQry = relatedFileVariables.filelist>
		</cfif>

		<cfreturn invoiceDocQry>
	</cffunction>


	<!--- NJH 2009/07/02 P-FNL069 - determine if the currentUser can access an rwTransaction --->
	<cffunction name="getUserRightsForRWTransaction" access="public" returntype="struct">
		<cfargument name="rwTransactionID" type="numeric">

		<cfscript>
			var getOwnerQry = "";
			var result = {edit=false,view=false};
		</cfscript>

		<!--- assume that any internal user at the moment will have rights --->
		<cfif request.relayCurrentUser.isInternal>
			<cfset result.edit = true>
			<cfset result.view = true>

		<cfelseif (not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn)>
			<cfquery name="getOwnerQry" datasource="#application.siteDataSource#">
				select 1 from rwTransaction where rwTransactionID = #arguments.rwTransactionID#
					and personID = #request.relayCurrentUser.personID#
			</cfquery>

			<cfif getOwnerQry.recordCount gt 0>
				<cfset result.edit = true>
				<cfset result.view = true>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getTransactionValue" access="public" returntype="numeric">
		<cfargument name="rwTransactionID" type="numeric" required="true">

		<cfset var getPointsValue = "">

		<cfquery name="getPointsValue" datasource="#application.siteDataSource#">
			select isNull(sum(points),0) as points from rwTransactionItems where rwTransactionID = #arguments.rwTransactionID#
		</cfquery>

		<cfreturn getPointsValue.points>
	</cffunction>

	<cffunction name="getTriggerPoints" returntype="query" validValues="true">
		<cfargument name="ModuleCode" required="false" type="string" default="RW">
		<cfscript>
			var triggerPointsList = "";
		</cfscript>

		<cfif arguments.ModuleCode eq "RW">
		<cfset triggerPointsList = '[{"triggerPointsListDisplay":"At Registration","triggerPointsListValue":"registration"}
					,{"triggerPointsListDisplay":"At Claim","triggerPointsListValue":"claim"}
					,{"triggerPointsListDisplay":"At Select Prizes","triggerPointsListValue":"spend"}
					,{"triggerPointsListDisplay":"Adhoc","triggerPointsListValue":"adhoc"}]'>
		<cfelse>
			<cfset triggerPointsList = '[{"triggerPointsListDisplay":"Hidden Discount","triggerPointsListValue":"pricebook"}
					,{"triggerPointsListDisplay":"Visible Discount","triggerPointsListValue":"subtotal"}
					,{"triggerPointsListDisplay":"Promo Code Entry Discount","triggerPointsListValue":"promocode"}
					,{"triggerPointsListDisplay":"Special","triggerPointsListValue":"special"}]'>
		</cfif>

		<cfreturn #application.com.structurefunctions.arrayOfStructuresToQuery(DeserializeJSON(triggerPointsList))#>
	</cffunction>

</cfcomponent>

