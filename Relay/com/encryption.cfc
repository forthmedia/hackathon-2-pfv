<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
encryption.cfc

2005-05-24

general cfc for doing encryption related things



2005-05-24 WAB created encryptID and decryptID functions
		WAB  added varing of cryp variable

2008	some changes for handling customer specific encryption methods
2008/06/19    WAB added some cfparams to above code
2009/11/18    SSS I have added a encryption param to EncryptStringAES and  DecryptStringAES so that we can choose to send in the type of encoding
	    default will be base64 for backward compatability.
	     For Trend to sign back into our site from Rewards Xtra
2009/07/14   SSS FNL-069  application.encryptionParameters code
			WAB	FNL-069	added a hashPassword function - see also login.cfc
			
2011 ?		WAB added a function with a random IV - derived from a lenovo function
2012-02-12	WAB added support for alternative encoding of base64 - makes less susceptible to problems on the URL
 --->

<cfcomponent displayname="Encryption" hint="Some encryption functions">


	<cffunction name="encryptID" returntype="string" hint="encrypts an id for passing around on urls etc" output="no">	
			<cfargument name="id" required="yes" type="numeric">
			<cfset var cryp = structNew()>
			
				<cf_cryp type="en" string="#id#" key="thisoughttobeanapplicationvariable">
				<cfreturn cryp.value>
	
	</cffunction> 

	<cffunction name="decryptID" returntype="numeric" hint="decrypts an id for passing around on urls etc" output="no">	
			<cfargument name="string" required="yes" type="string">
			<cfset var cryp = structNew()>			
			
				<cf_cryp type="de" string="#string#" key="thisoughttobeanapplicationvariable">
				<cfreturn  cryp.value >
	
	</cffunction> 	

	<cffunction name="encryptString" returntype="string" hint="encrypts a string for passing around on urls etc" output="no">	
			<cfargument name="encryptType" required="no" type="string" default="standard">
			
			<cfset var cryp = structNew()>
			<cfset var methodcall = "">
			
			<!--- WAB added these cfparams because when this code was released everywhere there was no standard encryption method defined --->
			<cfparam name="application.encryptionMethod" default = "#structNew()#">
			<cfparam name="application.encryptionMethod.standard" default = "cf_cryp">
						
			<cfif structkeyExists(application,"encryptionMethod") and structkeyExists(application.encryptionMethod,"#encryptType#")>
				<cftry>
					<cfset methodCall = evaluate("encryptString"&application.encryptionMethod[encryptType])>
					<cfcatch>
						<cfoutput>#encryptType# function has caused an error</cfoutput>
						<CF_ABORT>
					</cfcatch>
				</cftry>
				
				<!--- FNL-069 2009/07/14 SSS Security get any encryption parameters from relayvars --->
				<cfif structKeyExists (application, "EncryptionParams") and structKeyExists (application.EncryptionParams, encryptType)>
					<cfset structAppend(arguments,application.EncryptionParams[encryptType],false)>
				</cfif>
				<cfreturn methodCall(argumentcollection=arguments)>
			<cfelse>
				<cfoutput>#encryptType# does not exist as an encryptionMethod in RelayVars</cfoutput>
				<CF_ABORT>
			</cfif>
	
	</cffunction>
	
	<cffunction name="encryptStringcf_cryp" returntype="string" hint="encrypts a string for passing around on urls etc" output="no">	
		<cfargument name="string" required="yes" type="string">
		<cfargument name="passKey" required="no" type="string" default="thisalsooughttobeanapplicationvariable">
		
		<cfset var cryp = structNew()>
					
		<cf_cryp type="en" string="#string#" key="#passKey#">
		<cfreturn cryp.value>
	</cffunction>
	<!--- p-fnl069 2009-07-16 SSS changed this function so that it works like encryptString and will call other functions depending
	on encryption method --->
	<cffunction name="decryptString" returntype="string" hint="decrypts a string for passing around on urls etc" output="no">	
			<cfargument name="encryptType" required="no" type="string" default="standard">
			
			<cfset var cryp = structNew()>
			<cfset var methodcall = "">
						
			<!--- WAB added these cfparams because when this code was released everywhere there was no standard encryption method defined --->
			<cfparam name="application.encryptionMethod" default = "#structNew()#">
			<cfparam name="application.encryptionMethod.standard" default = "cf_cryp">
			
			<cfif structkeyExists(application,"encryptionMethod") and structkeyExists(application.encryptionMethod,"#encryptType#")>
				<cftry>
					<cfset methodCall = evaluate("decryptString"&application.encryptionMethod[encryptType])>
					<cfcatch>
						<cfoutput>#encryptType# function has caused an error</cfoutput>
						<CF_ABORT>
					</cfcatch>
				</cftry>

				<!--- FNL-069 2009/07/14 SSS Security get any encryption parameters from relayvars --->
				<cfif structKeyExists (application, "EncryptionParams") and structKeyExists (application.EncryptionParams, encryptType)>
					<cfset structAppend(arguments,application.EncryptionParams[encryptType],false)>
				</cfif>
				<cfreturn methodCall(argumentcollection=arguments)>
			<cfelse>
				<cfoutput>#encryptType# does not exist as an encryptionMethod in RelayVars</cfoutput>
				<CF_ABORT>
			</cfif>
	</cffunction>

	<!--- p-fnl069 2009-07-16 SSS added this function so that it can be called for standard decryption --->
	<cffunction name="decryptStringcf_cryp" returntype="string" hint="decrypts a string for passing around on urls etc" output="no">	
		<cfargument name="string" required="yes" type="string">
		<cfargument name="passKey" required="no" type="string" default="thisalsooughttobeanapplicationvariable">
		<cfset var cryp = structNew()>
						
		<cf_cryp type="de" string="#string#" key="#passKey#">
		<cfreturn  cryp.value >
	</cffunction> 	
	
	<cffunction name="encryptMagicNumber" returntype="string" hint="encrypts MagicNumber for passing around on urls etc">	
			<cfargument name="MagicNumber" required="yes" type="string">
			<cfset var cryp = structNew()>			
			
			<cf_cryp type="en" string="#MagicNumber#" key="thisoughttobeanapplicationvariable">
			<cfreturn cryp.value>
	
	</cffunction> 

	<cffunction name="decryptMagicNumber" hint="decrypts MagicNumber for passing around on urls etc">	
			<cfargument name="MagicNumberEnc" required="yes" type="string">
			<cfset var cryp = structNew()>
						
			<cf_cryp type="de" string="#MagicNumberEnc#" key="thisoughttobeanapplicationvariable">
			
			<cfif cryp.error>
				<cfreturn "decryptFailed">
			<cfelse>
				<cfreturn cryp.value>
			</cfif>
			
	
	</cffunction>
	<!--- 2008-01-23 AJC P-SNY041 DealerNet Integration - Created for URL encryption --->
	<cffunction name="EncryptStringAES" access="public" returnType="string" hint="Gets Bytes of passphrase, base64 the bytes then AES Encrypt">
		<cfargument name="StringToEncrypt" type="string" required="true">
		<cfargument name="PassPhrase" type="string" required="true">
		<cfargument name="encoding" type="string" default="base64">

		<cfscript>
		// Get the bytes of the pass phrase
			var myPassPhraseInBinary = passphrase.getBytes("UTF-8");
			// Turn binary pass phrase to base 64
			var myKey = ToBase64(myPassPhraseInBinary);		
			// Then encrypt string		
			var encryptedString = encrypt(stringToEncrypt,myKey,"AES/CBC/PKCS5Padding",#arguments.encoding#,myPassPhraseInBinary);
		</cfscript>

		<cfreturn "#encryptedString#">	
			
	</cffunction>

	<cffunction name="DecryptStringAES" access="public" returnType="string" hint="Constructs the url for the SSO authentication process">
		<cfargument name="StringToDecrypt" type="string" required="true">
		<cfargument name="PassPhrase" type="string" required="true">
		<cfargument name="encoding" type="string" default="base64">

		<cfscript>
			var myPassPhraseInBinary = passphrase.getBytes("UTF-8");
			var myKey = ToBase64(myPassPhraseInBinary);		
			var decryptedString = decrypt(StringToDecrypt,myKey,"AES/CBC/PKCS5Padding",#arguments.encoding#,myPassPhraseInBinary);
		</cfscript>

		<cfreturn "#decryptedString#">	
			
	</cffunction>

	
	<!--- WAB 2009/07/14 added this function --->
	<!--- p-fnl069 2009-07-17 SSS changed the name of the function on order to follow the new format --->
	<cffunction name="encryptStringpasswordhash" access="public" returnType="string" hint="">
		<cfargument name="partnerPassword" type="string" required="true">  <!--- had to use variable named partnerPassword because already used in a sony function for encrypting passwords --->
		<cfargument name="algorithm" type="string" default="MD5">

		<cfreturn hash(Trim(partnerPassword),algorithm)>	
			
	</cffunction>

	<!--- WAB 2009/07/14 added this function as an example of what might be done--->
	<!--- p-fnl069 2009-07-17 SSS changed the name of the function on order to follow the new format --->
	<cffunction name="encryptStringusernameAndPasswordhash" access="public" returnType="string" hint="">
		<cfargument name="partnerPassword" type="string" required="true">  <!--- had to use variable named partnerPassword because already used in a sony function for encrypting passwords --->
		<cfargument name="partnerUsername" type="string" required="true">
		<cfargument name="algorithm" type="string" default="MD5">

		<cfset var result = hash(Trim(partnerPassword) & Trim(lcase(partnerUserName)),algorithm)>
		<cfoutput>This is an example function which has not been implemented yet</cfoutput>
		<cfreturn result>	
			
	</cffunction>

	
	<!--- 
	EXAMPLE FUNCTION DO NOT RELEASE
	
	WAB 2009/07/14 added this function as an example of how we might transition between two password hashing methods --->
	<!--- p-fnl069 2009-07-17 SSS changed the name of the function on order to follow the new format --->
	<cffunction name="encryptStringusernameAndPasswordhashTransitionFunction" access="public" returnType="string" hint="">
		<cfargument name="partnerPassword" type="string" required="true">  <!--- had to use variable named partnerPassword because already used in a sony function for encrypting passwords --->
		<cfargument name="partnerUsername" type="string" required="true">
		<cfargument name="algorithm" type="string" default="MD5">

		<!--- Hash passwords using old and new methods of password encryption --->
		<cfset var CheckOldEncryption = '' />
		<cfset var updateWithNewEncryption = '' />
		<cfset var oldEncrypt = hash(Trim(partnerPassword))>
		<cfset var newEncrypt = usernameAndPasswordhash(argumentCollection = arguments)>
		
		<!--- now check if the old password matches --->
		<CFQUERY NAME="CheckOldEncryption" datasource="#application.siteDataSource#">
					SELECT 
						personid
					from 
						Person 
					where 
						(username =  <cf_queryparam value="#Trim(arguments.partnerUsername)#" CFSQLTYPE="CF_SQL_VARCHAR" >  and username <> N'')
						and (password =  <cf_queryparam value="#oldEncrypt#" CFSQLTYPE="CF_SQL_VARCHAR" > )
						and password <> ''
		</CFQUERY>
		
		<cfif CheckOldEncryption.recordCount is 1>
			<!--- matches old encryption method , need to update to new value --->
			<CFQUERY NAME="updateWithNewEncryption" datasource="#application.siteDataSource#">
			update person set password =  <cf_queryparam value="#newEncrypt#" CFSQLTYPE="CF_SQL_VARCHAR" >  where personid =  <cf_queryparam value="#CheckOldEncryption.personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>

		</cfif>
		
		<cfreturn newEncrypt>	
			
	</cffunction>
	
	<!--- 2010/01/25 SSS Added this to allow existing customers to operate with no hashing of passwords post RW8.2 - set relayvar application.encryptionMethod.relayPassword = "none" to invoke plain text behaviour --->
	<cffunction name="encryptStringNone" access="public" returnType="string" hint="">
		<cfargument name="partnerPassword" type="string" required="true">
		<cfreturn arguments.partnerPassword>
	</cffunction>
	


	<!--- 
	WAB 2011
	These functions using a random IV came from Lenovo
	Decrypt and encrypt each have a private function which returns a structure of all the intermediate values - v useful for debugging - although now it is working it is probably not needed, but is perhaps a good model for getting this type of function to work
	 --->
	<cffunction name="EncryptStringRandomIV" access="public" output="no">
		<cfargument  NAME="string" type="string" required="true">		 
		<cfargument  NAME="passKey" type="string" required="true">		 
		<cfargument  NAME="modifiedEncoding" type="boolean" DEFAULT="false">
		
		<cfreturn EncryptStringRandomIV_ReturningStructure(string=string,passKey=PassKey,modifiedEncoding=modifiedEncoding).result>	
	
	</cffunction>

	<cffunction name="EncryptStringRandomIV_ReturningStructure" access="private" output="no">
		<cfargument  NAME="string" type="string" required="true">		 
		<cfargument  NAME="passKey" type="string" required="true">		 
		<cfargument  NAME="modifiedEncoding" type="boolean" DEFAULT="false">
		
		<cfset var randomIV= getrandomIV()>		 
		<cfset var result = {string = string}>
		<cfset var encryptionKey = hexToBase64(passKey)>

		<cfset result.stringHex = stringToHex(string)>		
		
		<!--- Calculate SHA256 hash and append --->
		<cfset result.hashedString = hash(String,"SHA-256")>
		<cfset result.combinedString = result.StringHex & result.hashedString>

		<!--- Apply encryption --->
		<cfset result.encryptedString = encrypt(result.combinedString,encryptionKey,"AES/CBC/PKCS5Padding","Hex",RandomIV.binary)>

		<!--- Prepend IV, then base64 encode --->
		<cfset result.fullString = RandomIV.Hex&result.encryptedString>
		<cfset result.result = hexToBase64(result.fullString,modifiedEncoding)>
		<cfreturn result>	
	
	</cffunction>


	<cffunction name="DecryptStringRandomIV" access="public" output="no">
		<cfargument  NAME="string" type="string" required="true">		 
		<cfargument  NAME="passKey" type="string" required="true">		 
		<cfargument  NAME="modifiedEncoding" type="boolean" DEFAULT="false">	

		<cfreturn DecryptStringRandomIV_ReturningStructure(string=string,passKey=PassKey,modifiedEncoding=modifiedEncoding).result>	
	
	</cffunction>

	<cffunction name="DecryptStringRandomIV_ReturningStructure" access="private" output="no">
		<cfargument  NAME="String" type="string" DEFAULT="">		 
		<cfargument  NAME="passKey" type="string" required="true">		 
		<cfargument  NAME="modifiedEncoding" type="boolean" DEFAULT="false">	
		
		<cfset var result = {encryptedPlusRandomIV = structNew(), originalString =structNew()}>		
		<cfset var encryptionKey = hextobase64(passkey)>
		<cfset var randomIV = {}>

		<cfset result.encryptedPlusRandomIV.Hex = base64ToHex(String,modifiedEncoding)>
		
		<cfset randomIV.hex = left (result.encryptedPlusRandomIV.Hex,32)>
		<cfset randomIV.binary = Binarydecode(randomIV.hex ,"hex")>
		
		<cfset result.encrypted = right(result.encryptedPlusRandomIV.Hex,len(result.encryptedPlusRandomIV.Hex)-32)>
		<cfset result.decrypted = decrypt(result.encrypted,encryptionKey,"AES/CBC/PKCS5Padding","Hex",randomIV.binary)>
		<cfset result.originalString.HexPlusHash = result.decrypted>
		<cfset result.originalString.hash = right (result.originalString.HexPlusHash,64)>
		<cfset result.originalString.hex = left(result.originalString.HexPlusHash,len(result.originalString.HexPlusHash) - 64)>

		<cfset result.originalString.string = HexToString(result.originalString.hex)>

		<!--- WAB 2012-04-13 check that the hash is correct (and nothing has been tampered with) --->
		<cfif hash(result.originalString.string,"SHA-256") is not result.originalString.hash>
			<cfthrow message ="Decryption Error.  Incorrect Hash">
		</cfif>

		<cfset result.result = result.originalString.string>

		<cfreturn result>

	</cffunction>
	<!--- gets a random string for use in encryption function --->
	<cffunction name="getrandomIV">
		<cfargument  NAME="debug" type="boolean" default="false">

		<cfset var randomIV =structNew()>
				
		<cfif debug and application.com.relayCurrentSite.isEnvironment("Dev")>
			<cfset randomIV.base64=tobase64("1234567890ABCDEF")> 
		<cfelse>
			<cfset randomIV.base64=GenerateSecretKey("AES",128)>
		</cfif>

		<cfset randomIV.binary = Binarydecode(randomIV.base64,"base64")>
		<cfset randomIV.hex = BinaryEncode(randomIV.binary,"hex")>
		
		<cfreturn randomIV>
	
	</cffunction>


	


	<!--- WAB 2011-11-21 Added some general utility functions 
			which can be used in encryption code and make it more readable--->

	<!--- these variables used in two functions, so made global --->
	<cfset this.modifiedBase64Replace 		= "+,/">
	<cfset this.modifiedBase64ReplaceWith 	= "-,_">
	
	<cffunction name="HexToBase64" output="false">
		<cfargument  NAME="hex" type="string" DEFAULT="">		 
		<cfargument  NAME="modifiedEncoding" type="boolean" DEFAULT="false">		 

		<cfset var binary = Binarydecode(hex ,"hex")>
		<cfset var base64 = Binaryencode(binary ,"base64")>
		
		<cfif modifiedEncoding>
			<!--- For modified encoding we use - & _ instead of + & / to prevent URL difficulties
				also remove the = sign padding
			 --->
			<cfset base64 = replacelist(base64,this.modifiedBase64Replace,this.modifiedBase64ReplaceWith)>
			<cfset base64 = replace(base64,"=","","ALL")>
		</cfif>

		<cfreturn base64>	

	</cffunction>

	<cffunction name="Base64ToHex" output="false">
		<cfargument NAME="base64" type="string" DEFAULT="">		 
		<cfargument NAME="modifiedEncoding" type="boolean" DEFAULT="false">		 

		<cfset var binary = "">
		<cfset var Hex = "">
		<cfset var modifiedBase64 = base64>

		<cfif modifiedEncoding>
			<!--- change - & _ back to + & / and add back padding.  Characters come in groups of 4.  If last group has 1 character then 2 = are added.  If 2 characters then 1 = added ...--->
			<cfset modifiedBase64 = replacelist(modifiedBase64,this.modifiedBase64ReplaceWith,this.modifiedBase64Replace)>
			<cfset modifiedBase64 = modifiedBase64 & repeatString("=",max(0,len(modifiedBase64) mod 4 -1))>
		</cfif>

		<cfset binary = toBinary(modifiedBase64)>
		<cfset Hex = Binaryencode(binary ,"hex")>

		<cfreturn Hex>

	</cffunction>

	<cffunction name="StringToHex" output="false">
		<cfargument  NAME="string" type="string" DEFAULT="">		 
		<cfargument  NAME="encoding" type="string" DEFAULT="utf-8">		 
	
		<cfset var base64= tobase64(String,encoding)>
		
		<cfreturn Base64ToHex (base64)>
	
	</cffunction>


	<cffunction name="HexToString" output="false">
		<cfargument  NAME="Hex" type="string" DEFAULT="">		 
		<cfargument  NAME="encoding" type="string" DEFAULT="utf-8">		 
			
		<cfset var binary = Binarydecode(hex ,"hex")>
		<cfset var string = toString(binary,encoding )>

		
		<cfreturn string>
	
	</cffunction>

	<cffunction name="GenerateSecretKeyAESHex" output="false">
		
		<cfreturn Base64ToHex(GenerateSecretKey("AES",128))>
	
	</cffunction>


	<cffunction name="getMasterKey" output="false">
		<cfset var result = "">
		<cfset var find = "">
		<cfset var xmlResult = "">

		<cfif not structKeyExists(variables,"masterkey") or variables.masterKey is "">
			<cfset xmlResult = application.com.xmlfunctions.readAndParseXMLFile(application.paths.userfiles & "\XML\datasource.xml")>	
			<cfset find = xmlSearch(xmlResult.xml,"//masterkey")>
			<cfif arrayLen(find)>
				<cfset variables.masterKey = find[1].xmltext>
			<cfelse>
				<cfset variables.masterKey = "">
			</cfif>
		</cfif>
		
		<cfreturn variables.masterKey>
		
	</cffunction>

	<cffunction name="encryptWithMasterKey" access="public" returnType="string" output="false">
		<cfargument name="string" type="string" required="true">
		
		<cfset var masterKey = getMasterKey()>
		<cfset var result = "">
		
		<cfif masterKey eq "">
			<cfthrow message="The master key has not been configured. Go to 'Admin Tools - Release Scripts - Scripts To Be Run on a Brand New Install - Create Master Key'' to create the master key.">
		<cfelse>
			<cfset result = encryptStringRandomIV(string=String,passKey=masterKey)>
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="decryptWithMasterKey" access="public" returnType="string" output="false">
		<cfargument name="String" type="string" required="true">
		
		<cfset var masterKey = getMasterKey()>
		<cfset var result = "">
		
		<cfif masterKey eq "">
			<cfthrow message="The master key has not been configured. Go to 'Admin Tools - Release Scripts - Scripts To Be Run on a Brand New Install - Create Master Key' to create the master key.">
		<cfelse>
			<cfset result = decryptStringRandomIV(string=String,passKey=masterKey)>
		</cfif>

		<cfreturn result>
	</cffunction>
	
</cfcomponent>
