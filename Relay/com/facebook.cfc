<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="getAuthenticateRequestString" access="public" returnType="struct" output="false">
		<cfargument name="callbackURL" type="string" default="#request.currentSite.protocolAndDomain#/social/callback.cfm">
		<cfargument name="callbackParams" type="struct" default="#structNew()#">
		
		<cfset var service = application.com.service.getService(serviceID="facebook")>
		<cfset var AuthorisationURL = "https://www.facebook.com/dialog/oauth?client_id=" & service.consumerKey>
		<cfset var result={method="get",url=""}>
		
		<!--- NJH 2013/08/16 this enables user to publish to pages that they have admin rights to --->
		<cfif request.relayCurrentUser.isInternal>
			<cfset AuthorisationURL = AuthorisationURL & "&scope=publish_stream,manage_pages">
		</cfif>
		
		<cfset result.url = AuthorisationURL & "&redirect_uri=#request.currentSite.protocolAndDomain#/social/callback.cfm?s=#application.com.encryption.encryptString(encryptType='standard',string='facebook')#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getAccessToken" access="public" returntype="any" output="false">
		<cfargument name="code" type="string" required="true">
		
		<cfset var service = application.com.service.getService(serviceID="facebook")>
		<cfset var tokenResponse = structNew()>
		<cfset var accessToken = "">
		
		<cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#service.consumerKey#&redirect_uri=#request.currentSite.protocolAndDomain#/social/callback.cfm?s=#application.com.encryption.encryptString(encryptType='standard',string='facebook')#&client_secret=#service.consumerSecret#&code=#arguments.code#" method="get" result="tokenResponse">

		<cfif findNoCase("access_token",tokenResponse.filecontent)>
			<cfset accessToken = listlast(listfirst(tokenResponse.filecontent,"&"),"=")>
		</cfif>
		
		<cfset accessToken = CreateObject("component", "oauth.oauthtoken").init(sKey=application.com.encryption.encryptString(encryptType="standard",string=accessToken),sSecret="")>
		<cfreturn accessToken>
	</cffunction>
	
	
	
	<!--- <cffunction name="authenticateUser" access="public" returnType="struct">
		
		<cfset var serviceEntityID = getProfile().profile.ID> <!--- the user's facebook ID --->
		<cfset var result = {isOK=false,validUser=false,personID=0,serviceEntityID=serviceEntityID}>
		
		<cfset thisPersonID = application.com.service.doesEntityExistWithServiceEntityID(serviceEntityID=serviceEntityID,entityTypeID=0).entityID>
		<cfif thisPersonID neq 0>
			<cfset result.validUser = true>
			<cfset result.personID = thisPersonID>
			<cfset result.isOK = true>
		</cfif>
<cfdump var="#result#">
<CF_ABORT>
		<cfreturn result>

	</cffunction> --->
	
	
	<cffunction name="getProfile" access="public" returntype="struct" output="false">
		<cfargument name="fieldList" type="string" default="">
		
		<cfset var profileURL = "https://graph.facebook.com/me?fields=id,name,picture">
		<cfset var profileResponse = send(url=profileURL,argumentCollection=arguments)>
		<cfset var profileStruct = {id="",firstname="",lastname="",jobDesc="",pictureURL="",dob="",officePhone="",homePhone="",mobilePhone="",twitterAccount="",imAccount=""}>
		<cfset var result = {isOK=true,message="",profile=profileStruct}>
		
		<cfif profileResponse.isOK>
			<cfset profileStruct.id = profileResponse["id"]>
			<cfset profileStruct.firstname = listFirst(profileResponse["name"]," ")>
			<cfset profileStruct.lastname = listLast(profileResponse["name"]," ")>
			
			<cfif structKeyExists(profileResponse,"picture") and structKeyExists(profileResponse.picture,"data") and structKeyExists(profileResponse.picture.data,"url")>
				<cfset profileStruct.pictureUrl = profileResponse.picture.data.url>
			</cfif>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = profileResponse.error.message>
		</cfif>
		
		<cfset result.profile = profileStruct>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="send" access="private" returntype="struct">
		<cfargument name="url" type="string" required="true">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="method" type="string" default="get">
		
		<cfset var result = {isOk = true,message="Successful"}>
		<cfset var response = structNew()>
		<cfset var apiAccessToken = "">
		<cfset var sendURL = arguments.url>
		
		<cfif arguments.method eq "get">
			<cfset apiAccessToken = CreateObject("component", "oauth.oauthToken").getAccessToken(encryptToken=false,serviceID="facebook",argumentCollection=arguments).getKey()>
			<cfset sendURL = sendUrl & "&access_token=#apiAccessToken#">
		</cfif>
		
		<cfhttp url="#sendURL#" method="#arguments.method#" result="response">
			<cfif arguments.method eq "post">
				<cfloop collection="#arguments#" item="arg">
					<cfif not listFindNoCase("url,personID,method",arg) and structKeyExists(arguments,arg)>
						<cfhttpparam type="formfield" name="#lcase(arg)#" value="#arguments[arg]#">
					</cfif>
				</cfloop>
			</cfif>
		</cfhttp>
		
		<cfif response.statusCode neq "200 OK" and response.statusCode neq "201">
			<cfset result.isOK = false>
			<cfset result.message = "Not successful.">
		</cfif>
		
		<cfif isJson(response.fileContent)>
			<cfset structAppend(result,deserializejson(response.fileContent))>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getAuthoriseLink" access="public" returnType="string">
		
		<cfreturn "window.open('#getAuthenticateRequestString()#')">
	</cffunction>
	
	
	<cffunction name="getConnections" access="public" returnType="struct" output="false">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="serviceEntityID" type="string" required="false">
		<cfargument name="next" type="string" default="">
		
		<cfset var getPersonServiceID = "">
		<cfset var connectionsURL = "">
		<cfset var connectionResponse = "">
		<cfset var result = {person=queryNew("Id,name,personID,fullname"),data=arrayNew(1),isOk=true}>
		<cfset var connection = "">
		<cfset var pagingResult = structNew()>
		
		<cfif arguments.next eq "">
			<cfset connectionsURL = "https://graph.facebook.com/me/friends?">
		<cfelse>
			<cfset connectionsURL = arguments.next>
		</cfif>
		
		<cfset connectionResponse = send(url=connectionsURL,argumentCollection=arguments,entityId=arguments.personID)>
		<cfset result.isOK = connectionResponse.isOK>
		<cfif result.isOK>
			<cfset structAppend(result,connectionResponse)>
			
			<cfif structKeyExists(connectionResponse,"paging") and structKeyExists(connectionResponse.paging,"next") and connectionResponse.paging.next neq "">
				<cfset pagingResult = getConnections(argumentCollection=arguments,next=connectionResponse.paging.next)>

				<cfquery name="result.person" dbtype="query">
					select Id,name,personID,fullname from pagingResult.person
					union
					select ID,name,personID,fullname from result.person
				</cfquery>
			</cfif>
		</cfif>

		<cfloop array="#result.data#" index="connection">
			<cfset connection.personID = application.com.service.getServiceEntity(serviceEntityID=connection.id,serviceID="facebook").entityID>
			<cfset connection.fullname = connection.name>
		</cfloop>
		<cfif arrayLen(result.data)>
			<cfset result.person = application.com.structureFunctions.arrayOfStructuresToQuery(theArray=result.data)>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getPageAccounts" access="public" returnType="struct">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="serviceEntityID" type="string" required="false">
		<cfargument name="next" type="string" default="">
		
		<cfset var getPersonServiceID = "">
		<cfset var connectionsURL = "">
		<cfset var connectionResponse = "">
		<cfset var result = {page=queryNew("Id,name,access_token","varchar,varchar,varchar"),data=arrayNew(1),isOk=true}>
		<cfset var connection = "">
		<cfset var pagingResult = structNew()>
		
		<cfif arguments.next eq "">
			<cfset connectionsURL = "https://graph.facebook.com/me/accounts?">
		<cfelse>
			<cfset connectionsURL = arguments.next>
		</cfif>
		
		<cfif application.com.service.hasEntityBeenLinked(serviceID="facebook").linkEstablished>
		
			<cfset connectionResponse = send(url=connectionsURL,argumentCollection=arguments,entityId=arguments.personID)>
			<cfset result.isOK = connectionResponse.isOK>
			
			<cfif result.isOK>
				<cfset structAppend(result,connectionResponse)>
				
				<cfif structKeyExists(connectionResponse,"paging") and structKeyExists(connectionResponse.paging,"next") and connectionResponse.paging.next neq "">
					<cfset pagingResult = getPageAccounts(argumentCollection=arguments,next=connectionResponse.paging.next)>
	
					<cfquery name="result.page" dbtype="query">
						select Id,name,access_token from pagingResult.page
						union
						select Id,name,access_token from result.page
					</cfquery>
				</cfif>
			</cfif>
	
			<cfif arrayLen(result.data)>
				<cfset structDelete(result.data[1],"perms")>
				<cfset result.page = application.com.structureFunctions.arrayOfStructuresToQuery(theArray=result.data)>
			</cfif>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getPageAccountsAsQuery" access="public" returnType="query" validValues="true">
		
		<cfreturn getPageAccounts().page>	
	</cffunction>
	
	
	<cffunction name="postPageData" access="public" returnType="struct" output="false">
		<cfargument name="pageID" type="string" required="true">
		<cfargument name="access_token" type="string" required="true" hint="The page access token, not the users access token">
		<cfargument name="message" type="string" required="true">
		<cfargument name="link" type="string" required="false">
		<cfargument name="name" type="string" required="false" hint="Use if link specified">
		<cfargument name="caption" type="string" required="false" hint="Use if link specified">
		<cfargument name="description" type="string" required="false" hint="Use if link specified">
		<cfargument name="picture" type="string" required="false">
		
		<cfset var methodArgs = duplicate(arguments)>
		
		<cfset structDelete(methodArgs,"pageID")>
		<cfreturn send(url="https://graph.facebook.com/#arguments.pageID#/feed",argumentCollection=methodArgs,method="post")>
		
	
		<!--- <cfhttp url="https://graph.facebook.com/#arguments.pageID#/feed" method="post" result="postPageResponse">
			<cfhttpparam type="formfield" name="access_token" value="#arguments.access_token#">
			<cfhttpparam type="formfield" name="message" value="Go team RelayRally">
		</cfhttp> 
		
		<cfreturn postPageResponse>--->
		
	</cffunction>

</cfcomponent>