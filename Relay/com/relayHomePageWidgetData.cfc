<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayHomePageWidgetData.cfc
Author:			
Date created:	

Amendment History:

DD-MMM-YYYY	Initials 	What was changed
11/-1/2012		RMB		P-LEN024 - CR066 - Added new data to widgets to display new coulms on widgets
2013-02-13		WAB		Sprint 15&42 Comms. Change reference to displayEmailContent to viewCommunication
--->

<cfcomponent displayname="Manage Widget Data" hint="Manage Widget Data">
<!--- ==============================================================================
WAB 2010/10/12 Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
2011/07/27	NYB		P-LEN024 changed commcheckDetails.cfm to commDetailsHome.cfm
WAB 2014-01-31		removed references to et cfm, can always just use / or /?
                                                         
=============================================================================== --->
	<cffunction name="GetMyRelayListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 2>
		<cfset var UseChangeTabText = "">
		<cfset var colList = "colAmount,displayText1,link1,changetabtext1,icon1,displayText2,link2,changetabtext2,icon2,link3,icon3,link4,icon4">
		<cfset var qryReturn = queryNew(colList)>
		
		
		<!--- get origional data --->
		<cfscript>
			GetMyActions = application.com.relayHomePage.GetMyActions();
			GetMyNewActions = application.com.relayHomePage.GetMyNewActions();
			GetMyAccounts = application.com.relayHomePage.GetMyAccounts();
			GetMyOpportunities = application.com.relayHomePage.GetMyOpportunities();
		</cfscript>
	
		<!--- use origional data to setup query for list --->


		
		<cfsavecontent variable="displaymeohyeah1">
		<cfif getMyActions.numberOfOdActions GTE 1><font color='FF0000'></cfif>Phr_Sys_myActions<br />&nbsp;&nbsp;&nbsp;&nbsp;<cfif GetMyNewActions.numberOfOdActions GTE 1><font color='FF0000'></cfif>Phr_Sys_myNewActions<cfif GetMyNewActions.numberOfOdActions GTE 1></font></cfif>
		</cfsavecontent>
		
		<cfsavecontent variable="displaymeohyeah2"><cfoutput>
		<cfif getMyActions.numberOfOdActions GTE 1><font color='FF0000'></cfif>#numberformat(getMyActions.numberOfActions,'___,___,___,___')#<cfif getMyActions.numberOfOdActions GTE 1></font></cfif><br /><cfif GetMyNewActions.numberOfOdActions GTE 1><font color='FF0000'></cfif>#numberformat(GetMyNewActions.newActions,'___,___,___,___')#<cfif GetMyNewActions.numberOfOdActions GTE 1></font></cfif>
		</cfoutput></cfsavecontent>

		<cfscript>
			queryAddRow(qryReturn,1);
			querySetCell(qryReturn,"colAmount","#colAmount#");
			querySetCell(qryReturn,"displayText1","#displaymeohyeah1#");
			querySetCell(qryReturn,"link1","/homepage/myActions.cfm");
			querySetCell(qryReturn,"changetabtext1","Phr_Sys_myActions");
			querySetCell(qryReturn,"icon1","");
			querySetCell(qryReturn,"displayText2","#displaymeohyeah2#");
			querySetCell(qryReturn,"link2","");
			querySetCell(qryReturn,"changetabtext2","Phr_Sys_myActions");
			querySetCell(qryReturn,"icon2","");
			
			queryAddRow(qryReturn,1);
			querySetCell(qryReturn,"colAmount","#colAmount#");
			querySetCell(qryReturn,"displayText1","Phr_Sys_MyAccounts");
			querySetCell(qryReturn,"link1","/homepage/myAccounts.cfm");
			querySetCell(qryReturn,"changetabtext1","");
			querySetCell(qryReturn,"icon1","");
			querySetCell(qryReturn,"displayText2","#numberformat(getMyAccounts.numberOfAccounts,'___,___,___,___')#");
			querySetCell(qryReturn,"link2","");
			querySetCell(qryReturn,"changetabtext2","");
			querySetCell(qryReturn,"icon2","");
			
			queryAddRow(qryReturn,1);
			querySetCell(qryReturn,"colAmount","#colAmount#");
			querySetCell(qryReturn,"displayText1","Phr_Sys_MyOpportunities");
			querySetCell(qryReturn,"link1","/leadManager/pipelineReport.cfm?module=leadManager&frm_vendoraccountmanagerpersonid=#request.relayCurrentUser.personID#");
			querySetCell(qryReturn,"changetabtext1","");
			querySetCell(qryReturn,"icon1","");
			querySetCell(qryReturn,"displayText2","#numberformat(getMyOpportunities.numberOfOpps,'___,___,___,___')#");
			querySetCell(qryReturn,"link2","");
			querySetCell(qryReturn,"changetabtext2","");
			querySetCell(qryReturn,"icon2","");
			
			if (application.testSite eq 2 or application.siteDataSource eq "Demo" or CGI.REMOTE_ADDR eq "62.232.221.162") {
			queryAddRow(qryReturn,1);
			querySetCell(qryReturn,"colAmount","#colAmount#");
			querySetCell(qryReturn,"displayText1","phr_Sys_My_Dashboards");
			querySetCell(qryReturn,"link1","/dashboard/dashboard.cfm");
			querySetCell(qryReturn,"changetabtext1","");
			querySetCell(qryReturn,"icon1","");
			querySetCell(qryReturn,"displayText2","");
			querySetCell(qryReturn,"link2","");
			querySetCell(qryReturn,"changetabtext2","");
			querySetCell(qryReturn,"icon2","");
			}
		</cfscript>
		
		<cfreturn qryReturn>
	
	</cffunction>
	
	<cffunction name="GetEmailShotsListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 2>
		<cfset var colList = "colAmount,displayText1,changetabtext2,iconClass2,link1,icon1,displayText2,displayText3,link2,icon2,link3,icon3,linktype1,linktype2,linktype3">
		<cfset var qryReturn = queryNew(colList)>
		<cfset var wParams = application.com.regexp.convertNameValuePairStringToStructure(arguments.widgetParams)>
		<cfset var displayComms = (Round(wParams.displayRows/2))>
		<cfset var displayRecentComms = (wParams.displayRows-displayComms)>
		
		<!--- get origional data --->
		<cfscript>
			//if (isDefined("countryScopeOrganisationRecords") and countryScopeOrganisationRecords eq "1") {
			if (application.com.settings.getSetting("plo.countryScopeOrganisationRecords")) {
				//recentComms = application.com.relayHomePage.recentComms(request.relayCurrentUser.countryList);
				recentComms = application.com.relayHomePage.recentAndQueuedComms(request.relayCurrentUser.countryList);
			} else {
				//recentComms = application.com.relayHomePage.recentComms();
				recentComms = application.com.relayHomePage.recentAndQueuedComms();
			}
		</cfscript>
		
		<!--- use origional data to setup query for list --->
		
		<!---<cfif recentComms.recentQueuedComms.recordCount lt displayComms>
			<cfset displayRecentComms = displayRecentComms+(displayComms-recentComms.recentQueuedComms.recordCount)>
		</cfif>--->

				<!---querySetCell(qryReturn,"link1","/communicate/commDetailsHome.cfm?ID=#CommID#");--->

		<cfoutput query="recentComms.recentComms">
			<cfscript>
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","3");
				querySetCell(qryReturn,"displayText1","#dateFormat(sendDate,'dd-mmm-yy')# #title#");
				querySetCell(qryReturn,"CHANGETABTEXT2","#dateFormat(sendDate,'dd-mmm-yy')# #title#");
				querySetCell(qryReturn,"iconClass2","communicate");
				querySetCell(qryReturn,"link1","viewCommunication('#commID#')");
				querySetCell(qryReturn,"icon1","");
				querySetCell(qryReturn,"displayText2","");
				querySetCell(qryReturn,"displayText3","Send Com to Self");
				querySetCell(qryReturn,"link2","/communicate/commStatusCounts.cfm?frmcommID=#CommID#");
				querySetCell(qryReturn,"icon2","/images/MISC/Icon_Graph.gif");
				querySetCell(qryReturn,"link3","/communicate/sendCommunication.cfm?frmCommID=#CommID#&frmResend=true&frmpersonids=#request.relaycurrentuser.personID#&frmTest=yes&outputMessage=true");
				querySetCell(qryReturn,"icon3","/images/icons/email_go.png");
				querySetCell(qryReturn,"linktype1","JSLink");
				querySetCell(qryReturn,"linktype2","");
				querySetCell(qryReturn,"linktype3","PopUp");
			</cfscript>
		</cfoutput>
		<cfif recentComms.recentQueuedComms.recordCount gt 0>
			<cfscript>
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","3");
				querySetCell(qryReturn,"displayText1","Phr_Sys_Queued");
				querySetCell(qryReturn,"link1","");
				querySetCell(qryReturn,"icon1","");
				querySetCell(qryReturn,"displayText2","");
				querySetCell(qryReturn,"displayText3","");
				querySetCell(qryReturn,"link2","");
				querySetCell(qryReturn,"icon2","");
				querySetCell(qryReturn,"link3","");
				querySetCell(qryReturn,"icon3","");
				querySetCell(qryReturn,"linktype1","");
				querySetCell(qryReturn,"linktype2","");
				querySetCell(qryReturn,"linktype3","");
			</cfscript>
			
			<cfoutput query="recentComms.recentQueuedComms">
				<cfscript>
					queryAddRow(qryReturn,1);
					querySetCell(qryReturn,"colAmount","3");
					querySetCell(qryReturn,"displayText1","#dateFormat(sendDate,'dd-mmm-yy')# #title#");
					querySetCell(qryReturn,"CHANGETABTEXT2","#dateFormat(sendDate,'dd-mmm-yy')# #title#");
					querySetCell(qryReturn,"iconClass2","communicate");
					querySetCell(qryReturn,"link1","viewCommunication('#commID#')");
					querySetCell(qryReturn,"icon1","");
					querySetCell(qryReturn,"displayText2","");
					querySetCell(qryReturn,"displayText3","Send Com to Self");
					querySetCell(qryReturn,"link2","/communicate/commStatusCounts.cfm?frmcommID=#CommID#");
					querySetCell(qryReturn,"icon2","/images/MISC/Icon_Graph.gif");
					querySetCell(qryReturn,"link3","/communicate/sendCommunication.cfm?frmCommID=#CommID#&frmResend=true&frmpersonids=#request.relaycurrentuser.personID#&frmTest=yes&outputMessage=true");
					querySetCell(qryReturn,"icon3","/images/icons/email_go.png");
					querySetCell(qryReturn,"linktype1","JSLink");
					querySetCell(qryReturn,"linktype2","");
					querySetCell(qryReturn,"linktype3","PopUp");
				</cfscript>
			</cfoutput>
		</cfif>

		<cfreturn qryReturn>
	
	</cffunction>
	
	<cffunction name="GetNewsItemsListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 1>
		<cfset var colList = "colAmount,displayText1,link1,icon1">
		<cfset var qryReturn = queryNew(colList)>
		
		<!--- get origional data --->
		<cfscript>
			GetNewsItems = application.com.relayHomePage.GetNewsItems();
			newsIDs = valueList(getNewsItems.id);
		</cfscript>
		
		<!--- use origional data to setup query for list --->
		<cfloop query="GetNewsItems">
			<cfscript>
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","phr_headline_element_#ID#");
				querySetCell(qryReturn,"link1","#application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain()#/?seid=#id#-pr");
				querySetCell(qryReturn,"icon1","");
			</cfscript>
		</cfloop>
		
		<cfreturn qryReturn>
	
	</cffunction>
	
	<cffunction name="GetFromTreeNewsItemsListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 1>
		<cfset var colList = "colAmount,displayText1,link1,icon1">
		<cfset var qryReturn = queryNew(colList)>
		
		<!--- get origional data --->
		<cfscript>
			GetNewsItems = application.com.relayHomePage.GetTreeNewsItems();
		</cfscript>
		
		<!--- use origional data to setup query for list --->
		<cfloop query="GetNewsItems">
			<cfif GetNewsItems.currentrow NEQ '1'>
			<cfscript>
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","#headline#");
				querySetCell(qryReturn,"link1","homepage/DisplayNewPageV1.cfm?eid=#node#");
				querySetCell(qryReturn,"icon1","");
			</cfscript>
			</cfif>
		</cfloop>
		
		<cfreturn qryReturn>
	
	</cffunction>
	
	<cffunction name="GetPromotionsListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 1>
		<cfset var colList = "colAmount,displayText1,link1,icon1">
		<cfset var qryReturn = queryNew(colList)>
		
		<!--- get origional data --->
		<cfscript>
			GetPromos = application.com.relayHomePage.GetPromos();
			promoIDs = valueList(GetPromos.id);
		</cfscript>
		
		<!--- use origional data to setup query for list --->
		<cfloop query="getPromos">
			<cfscript>
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","phr_headline_element_#ID#");
				// NJH 2008/05/30 Bug Fix  Lexmark Support 84 - changed the window to use the element preview rather than elementDetail which is old code
				querySetCell(qryReturn,"link1","#application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain()#/?seid=#id#-pr");
				querySetCell(qryReturn,"icon1","");
			</cfscript>
		</cfloop>
		
		<cfreturn qryReturn>
	
	</cffunction>
	
	<cffunction name="GetHintsAndShortcutsListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 1>
		<cfset var colList = "colAmount,displayText1,link1,icon1">
		<cfset var qryReturn = queryNew(colList)>

		<cfset var Uselink1 = "">
		<cfset var Uselink2 = "">
		<cfset var GGSI = "#application.com.settings.getsetting('homePageSettings.gettingStarted')#">
		<cfset var GFAQ = "#application.com.settings.getsetting('homePageSettings.frequentlyAskedQuestions')#">
		
		<cfif GGSI IS NOT ''>

			<CFSET Uselink1 = "/homepage/DisplayNewPageV1.cfm?eid=#GGSI#">
	
		<cfelse>
		
			<CFSET Uselink1 = "/homepage/relayOverview.cfm">
			
		</cfif>
		
		<cfif GFAQ IS NOT ''>

			<CFSET Uselink2 = "/homepage/DisplayNewPageV1.cfm?eid=#GFAQ#">

		<cfelse>
		
			<CFSET Uselink2 = "/homepage/FAQDisplay.cfm">
			
		</cfif>
		
		<!--- use original data to setup query for list --->
		<cfscript>
			// 2013-12-16	YMA	Case 438313 - Only show getting started if custom version is defined.  Core one will be re-worked next year.
			if (GGSI neq '') {
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","Phr_Sys_GettingStarted");
				querySetCell(qryReturn,"link1","#Uselink1#");
				querySetCell(qryReturn,"icon1","");
			}
			
			// 2013-12-16	YMA	Case 438313 - Only show FAQ if custom version is defined.  Core one will be re-worked next year.
			if (GFAQ neq '') {
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","Phr_Sys_FAQs");
				querySetCell(qryReturn,"link1","#Uselink2#");
				querySetCell(qryReturn,"icon1","");
			}
			
			if (application.com.login.checkInternalPermissions("approvalTask","Level1")) {
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","Phr_Sys_Approvals");
				querySetCell(qryReturn,"link1","/approvals/regApprovalReport.cfm");
				querySetCell(qryReturn,"icon1","");
			}
			
			//	2013-12-16	YMA	Case 438313 - remove usergruides it is no longer needed.  Will be re-worked next year.
			// SSS 2008-09-04 Bug fixed fixed userguides link not working 
			//queryAddRow(qryReturn,1);
			//querySetCell(qryReturn,"colAmount","#colAmount#");
			//querySetCell(qryReturn,"displayText1","phr_Sys_User_Guides");
			//querySetCell(qryReturn,"link1","/homepage/userGuides.cfm");
			//querySetCell(qryReturn,"icon1","");
			// SSS 2008-09-04 Bug fixed fixed userguides link not working */
		</cfscript>
		
		<cfreturn qryReturn>
	
	</cffunction>
	
	<cffunction name="GetKeyStatisticsListData" access="public">
		<cfargument name="widgetParams" default="">
		
		<cfset var colAmount = 2>
		<cfset var colList = "colAmount,displayText1,link1,icon1,displayText2,link2,icon2">
		<cfset var qryReturn = queryNew(colList)>
		
		
		<!--- get origional data --->
		<cfscript>
			GetKeyStats = application.com.relayHomePage.GetKeyStats();
		</cfscript>
		
		<!--- use origional data to setup query for list --->
		<cfoutput query="getKeyStats">
			<cfscript>
				queryAddRow(qryReturn,1);
				querySetCell(qryReturn,"colAmount","#colAmount#");
				querySetCell(qryReturn,"displayText1","#description#");
				if (len(path)) {
					querySetCell(qryReturn,"link1","/#getKeyStats.path#");
				} else {
					querySetCell(qryReturn,"link1","");
				}
				querySetCell(qryReturn,"icon1","");
				querySetCell(qryReturn,"displayText2","#numberformat(value,'___,___,___,___')#");
				querySetCell(qryReturn,"link2","");
				querySetCell(qryReturn,"icon2","");
			</cfscript>
		</cfoutput>

		<cfreturn qryReturn>
	
	</cffunction>
	
</cfcomponent>

