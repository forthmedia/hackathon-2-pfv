<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Various Methods for updating and reading entityApprovalTable" hint="">


	<cffunction access="public" name="setApprovalRequested">
			<cfargument name="entityID" type="numeric" required="true">
			<cfargument name="entityType" type="string" required="true">
			<cfargument name="PersonIDList" type="string" required="true">			
			<cfargument name="Date" type="date" default="#now()#">						

				<cfif isnumeric(entityType)>
					<cfset entityTypeID = entityType>
				<cfelse>
					<cfset entityTypeID = application.entityTypeID[entityType]>
				</cfif>
				

				<cfquery name="recordApprovalRequest" datasource="#application.siteDataSource#">
				-- update existing records
				update 
					entityApproval 
				set 
					approvalRequestedDate = #date#,
					lastupdatedby = #request.relaycurrentuser.usergroupid#,
					lastupdated = #now()#
				where 
						entityid = #entityID# 
					and entityTypeID = #entityTypeID#
					and personid in (#PersonIDList#)	
				
				
					
				-- insert new records
					
				insert into entityApproval
				(entityid,entityTypeID,personid,approvalRequestedDate , created, createdby,lastupdated, lastupdatedby)
				select
					#entityID#,
					#entityTypeID#,
					p.personid,
					#date#, 
					#now()#, 
					#request.relayCurrentUser.usergroupid# ,  
					#now()#, 
					#request.relayCurrentUser.usergroupid#
				
				from 
					person  p
						left join 
					entityApproval eA  on p.personid = ea.personid and ea.entityid = #entityID# and ea.entityTypeID = #entityTypeID#
				where 
						eA.personid is null
					and p.personid in (#PersonIDList#)	
				</cfquery>	
			
	
	</cffunction >
	
	
</cfcomponent>		