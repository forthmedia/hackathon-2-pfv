<!--- �Relayware. All Rights Reserved 2014 --->


<cfcomponent output="false">
	
	<cffunction name="addActivity" access="public" returntype="struct" output="false">
		<cfargument name="activityType" type="string" required="true">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="countryID" type="numeric" required="false">
		<cfargument name="created" type="datetime" required="false">
		<cfargument name="data" type="string" required="false">
		
		<cfset var addActivityQry = "">
		<cfset var result = {isOk=true,message=""}>
		<cfset var incOrgJoin = false>
		
		<cfif arguments.organisationID neq 0 or structKeyExists(arguments,"personID")>
			<cfset incOrgJoin = true>
		</cfif>
		
		<cfquery name="addActivityQry">
			insert into activity (personID,activityTypeID,organisationTypeId,organisationID,created,countryID,data)
			select 
				<cfif structKeyExists(arguments,"personID")>p.personId<cfelse>null</cfif>,
				activityTypeID,
				<cfif incOrgJoin>organisationTypeID,<cfelse>null</cfif>
				o.organisationID,
				<cfif structKeyExists(arguments,"created")><cf_queryParam value="#arguments.created#" cfsqltype="cf_sql_datetime"><cfelse>getDate()</cfif>,
				<cfif structKeyExists(arguments,"personID")>l.countryID<cfelse>o.countryID</cfif>,
				<cfif structKeyExists(arguments,"data")><cf_queryParam value="#arguments.data#" cfsqltype="cf_sql_varchar"><cfelse>null</cfif>
			from activityType
				<cfif incOrgJoin>,organisation o</cfif>
				<cfif structKeyExists(arguments,"personID")>
					inner join person p on o.organisationID = p.organisationID
					inner join location l on p.locationId = l.locationID
				</cfif>
				where activityType = <cf_queryparam value="#arguments.activityType#" cfsqltype="cf_sql_varchar">
					<cfif structKeyExists(arguments,"personID")>and p.personID = <cf_queryparam value="#arguments.personID#" cfsqltype="cf_sql_integer">
					<cfelseif incOrgJoin>and o.organisationID = <cf_queryparam value="#arguments.organisationID#" cfsqltype="cf_sql_integer">
					</cfif>
		</cfquery>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="refreshActivities" access="public" returnType="struct" output="true">
		<cfargument name="startDateTime" type="date" required="false"> <!--- add ability to pass in some dates, so that we in theory can re-set some old data --->
		<cfargument name="endDateTime" type="date" default="#request.requestTime#">
		<cfargument name="refreshActivityType" type="string" required="false">
		
		<cfset var result = {isOK=true,message=""}>
		<cfset var getActivities = "">
		<cfset var queryInfo = {}>
		<cfset var insertActivities = "">
		<cfset var getQueryInfoFunction = "">
		<cfset var containsPersonInfo = true>
		<cfset var countryAlias = "l">
		<cfset var organisationJoinAlias = "p">
		<cfset var insertValuesQryString = "">
		<cfset var insertValuesQryStringWithoutParentheses = "">
		<cfset var selectQryString = "">
		<cfset var createTempTable = "">
		<cfset var activityStartDate = "">
		<cfset var activityEndDate = arguments.endDateTime>
		<cfset var refreshOldData = false>  <!--- if we need to clear out and re-populate old data --->
		<cfset var updateLastRefreshed = true> <!--- tells us whether we need to update the lastRefeshed date... set to false if we're not doing a 'normal' run (ie. via housekeeping) or rather if a startTime is passed in that is before the current lastRefreshed date --->
		
		<cfsetting requesttimeout="3600">
		
		<cfquery name="getActivities">
			select activityType,activityTypeID,isNull(lastRefreshed,getDate()-1) as lastRefreshed,entityTypeID
			from activityType
			where 1=1
			<cfif structKeyExists(arguments,"refreshActivityType")>
				<cfif isNumeric(listFirst(arguments.refreshActivityType))>
					and activityTypeID in (<cf_queryparam value="#arguments.refreshActivityType#" cfsqltype="cf_sql_numeric" list="true">)
				<cfelse>
					and activityType in (<cf_queryparam value="#arguments.refreshActivityType#" cfsqltype="cf_sql_varchar" list="true">)
				</cfif>
			</cfif>
			order by lastRefreshed asc
		</cfquery>
		
		<!--- a temp table to store activities in that may need to be deleted and then re-inserted --->
		<cfquery name="createTempTable">
			if object_id('tempdb..##tempActivities') is null
				select * into ##tempActivities from activity where 1=0
		</cfquery>
		
		<cfloop query="getActivities">
			
			<cfset updateLastRefreshed = true>
			<cfset refreshOldData = false>
			<cfset containsPersonInfo = true>
			<cfset countryAlias = "l">
			<cfset organisationJoinAlias = "p">
			
			<cfset getQueryInfoFunction = variables["get#activityType#QueryInfo"]>
			<!--- set some default columns which can be overriden by the functions --->
			<cfset queryInfo = {personID="personID",organisationID="organisationID",whereClause="1=1",activityDate="created",join="",entityID="entityID",doCount=false,data="null",postJoin=""}>
			<cfset structAppend(queryInfo,getQueryInfoFunction(),true)>
			
			<cfif queryInfo.personID eq "null">
				<cfset containsPersonInfo = false>
				<cfset countryAlias = "o">
				<cfset organisationJoinAlias = "metric">
			</cfif>
			
			<cfset activityStartDate = lastRefreshed>
			<cfset activityEndDate = arguments.endDateTime>
			<cfif structKeyExists(arguments,"startDateTime")>
				<cfset activityStartDate = arguments.startDateTime>
			</cfif>
			
			<!--- if we're doing a count (aggregate of data) or if the startTime (passed in) is less then the last time the process was run for the given activity Type, then delete the old data
				so that it gets refreshed.
			 --->
			<cfif queryInfo.doCount or dateDiff("n",activityStartDate,lastRefreshed) gt 0>
				<cfset refreshOldData = true>
				<cfif dateDiff("n",activityStartDate,lastRefreshed) gt 0>
					<cfset updateLastRefreshed = false>
				</cfif>
				<!--- if we're doing a count, then we need to get the data from the beginning of the day... so, convert the startDate to a date... faster than using the dateAtMidnight function in the query --->
				<cfif queryInfo.doCount>
					<cfset activityStartDate = dateFormat(activityStartDate,"yyyy-mm-dd")>
				</cfif>
			</cfif>
			
			<cfsavecontent variable="insertValuesQryString">
				<cfoutput>
					(
						<cfif containsPersonInfo>personID,locationID,</cfif>
						organisationID,organisationTypeID,
						countryID,
						activityDate,
						activityTypeID,
						entityID,
						data,
						<cfif queryInfo.doCount>numActivities,</cfif>
						activityTime
					)
				</cfoutput>
			</cfsavecontent>
			
			<cfset insertValuesQryStringWithoutParentheses = replace(replace(insertValuesQryString,"(",""),")","")>
			
			<!--- Check if queryInfo.personID is already aliased, if it isn't use the metric table' RJT --->
			<cfset aliasedPersonIDColumn=FindNoCase(".",queryInfo.personID)?queryInfo.personID:"metric.#queryInfo.personID#">
			
			<cfsavecontent variable="selectQryString">
				<cfoutput>
					select distinct 
						<cfif containsPersonInfo>#aliasedPersonIDColumn#,l.locationID,</cfif>
						o.organisationID, o.organisationTypeID,
						#countryAlias#.countryID,
						dbo.dateAtMidnight(metric.#queryInfo.activityDate#),
						#activityTypeID#,
						<cfif queryInfo.entityID neq "null" and listLen(queryInfo.entityID,".") eq 1>metric.</cfif>#queryInfo.entityID#,
						#queryInfo.data#,
						<cfif queryInfo.doCount>count(<cfif listLen(queryInfo.entityID,".") eq 1>metric.</cfif>#queryInfo.entityID#),min(metric.#queryInfo.activityDate#)<cfelse>metric.#queryInfo.activityDate#</cfif>
					from #preserveSingleQuotes(queryInfo.metricTable)# metric
						#preserveSingleQuotes(queryInfo.join)#
						<cfif containsPersonInfo>inner join person p on #aliasedPersonIDColumn# = p.personID
						inner join location l on l.locationID = p.locationID</cfif>
						inner join organisation o on <cfif listLen(queryInfo.organisationID,".") eq 1>#organisationJoinAlias#.</cfif>#queryInfo.organisationID# = o.organisationID
						#preserveSingleQuotes(queryInfo.postJoin)#
					where #preserveSingleQuotes(queryInfo.whereClause)#
						and metric.#queryInfo.activityDate# between '#activityStartDate#' and '#activityEndDate#'
					<cfif queryInfo.doCount>
					group by <cfif containsPersonInfo>#aliasedPersonIDColumn#,l.locationID,</cfif>o.organisationID,o.organisationTypeID,#countryAlias#.countryID,#queryInfo.data#,<cfif listLen(queryInfo.entityID,".") eq 1>metric.</cfif>#queryInfo.entityID#,dbo.dateAtMidnight(metric.#queryInfo.activityDate#)
					</cfif>
					order by <cfif queryInfo.doCount>min(metric.#queryInfo.activityDate#)<cfelse>metric.#queryInfo.activityDate#</cfif>
				</cfoutput>
			</cfsavecontent>
			
			<cfif refreshOldData>
				<!---- if we're doing summary data, then we need to clear out old data. We then insert the new data --->
				<cfquery name="clearOldActivities">
					truncate table ##tempActivities
					
					insert into ##tempActivities
					#preserveSingleQuotes(insertValuesQryString)#
					#preserveSingleQuotes(selectQryString)#
					
					delete activity
					from activity i
						inner join ##tempActivities t on 
							isNull(i.personID,0) = isNull(t.personID,0) and i.activityTypeID = t.activityTypeID and i.entityID = t.entityID and i.activityDate = t.activityDate and isNull(i.activityTime,'') = isNull(t.activityTime,'') and isNull(i.data,'') = isNull(t.data,'') and isNull(i.locationID,0) = isNull(t.locationID,0) and i.organisationTypeID = t.organisationTypeID and i.organisationID = t.organisationID and i.countryID = t.countryID
				</cfquery>
			</cfif>
			
			<cfquery name="insertActivities">
				insert into activity
				#preserveSingleQuotes(insertValuesQryString)#
				<cfif not refreshOldData> <!--- if we're not refreshing old data, then get data straight from source, otherwise get it from the temp table that we have just inserted it into --->
				#preserveSingleQuotes(selectQryString)#
				<cfelse>
				select #preserveSingleQuotes(insertValuesQryStringWithoutParentheses)# from ##tempActivities
				</cfif>
				
				<!--- if we're doing a historical run (ie. refreshing old data), then we don't want to update the lastRefreshed column. THis should only be set when run as a scheduled task... we could perhaps check the current user? --->
				<cfif updateLastRefreshed>
				update activityType set lastRefreshed =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >  where activityTypeID = <cf_queryparam value="#activityTypeID#" cfsqltype="cf_sql_numeric">
				</cfif>
			</cfquery>

		</cfloop>
		
		<cfquery name="dropTempTable">
			if object_id('tempdb..##tempActivities') is not null
				drop table ##tempActivities
		</cfquery>
		
		<cfreturn result>
	</cffunction>
	
	
	<!--- Communications --->
	<!--- Number of Communication click-thru --->
	<cffunction name="getCommClickThruQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="dateSent",metricTable="commDetailHistory",join="inner join commDetailStatus cds on metric.commStatusID = cds.commStatusID",whereClause="cds.name in ('Click thru','Clicked thru to Remote URL','Clicked thru to ElementID')",entityID="commID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Number of Communications read --->
	<cffunction name="getCommReadQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="dateSent",metricTable="commDetailHistory",join="inner join commDetailStatus cds on metric.commStatusID = cds.commStatusID",whereClause="cds.name in ('Email Read','Email Read Receipt Received')",entityID="commID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- returns a record for every time a user unsubscribes from a comm--->
	<cffunction name="getCommUnsubscribedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="dateSent",metricTable="commDetailHistory",join="inner join commDetailStatus cds on metric.commStatusID = cds.commStatusID",whereClause="cds.name = 'Unsubscribe Response'",entityID="commID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Site --->
	<!--- gets a count of portal page hits for a given visit. Logging in to teh portal and viewing 3 pages returns a single record with a count of 3.  --->
	<cffunction name="getPagesVisitedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="personID",activityDate="created",metricTable="entityTracking",join="inner join visit v on v.visitID = metric.visitID and metric.entityTypeID=#application.entityTypeID.element# and metric.personID <> 404",entityID="v.siteDefDomainId",doCount="true",data="metric.visitID",whereClause="metric.clickThru != 1"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Returns a row when a file has been downloaded --->
	<cffunction name="getFileDownloadedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="downloadTime",metricTable="fileDownloads",entityID="fileID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- gets a record for every time the portal was visited.. a new visitID means a new record --->
	<cffunction name="getPortalVisitedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="visitStartedDate",metricTable="visit",join="inner join siteDef sd on sd.siteDefID = metric.siteDefID and sd.isInternal != 1",entityID="siteDefDomainID",data="visitID",whereClause="metric.personID <> 404"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get number of logins --->
	<cffunction name="getLoggedInQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="loginDate",metricTable="usage",entityID="siteDefDomainID",data="source",whereClause="metric.impersonatedByPersonID = 0"}>
		<cfreturn result>
	</cffunction>
		
	<!--- Social --->
	<!--- get the pages that have been shared on the social sites--->
	<cffunction name="getPageSharedOnSocialSiteQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="elementShare",join="inner join service s on s.serviceID = metric.serviceID",entityID="elementID",data="serviceTextID",whereClause="metric.personID <> 404"}>
		<cfreturn result>
	</cffunction>

	<!--- get the number of accounts that have been connected--->
	<cffunction name="getSocialAccountConnectedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="entityID",metricTable="serviceEntity",join="inner join service s on s.serviceID = metric.serviceID and metric.entityTypeID=#application.entityTypeID.person#",entityID="serviceID",data="serviceTextID",whereClause="p.loginExpires < getDate()"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get the event registrations that have been shared--->
	<cffunction name="getEventRegistrationSharedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="performedByEntityID",metricTable="relayActivity",join="inner join relayActivityShare ras on ras.relayActivityID = metric.ID and ras.shared = 1 and metric.performedByEntityTypeID=#application.entityTypeID.person# and metric.performedOnEntityTypeID=#application.entityTypeID.eventDetail# inner join service s on s.serviceID = ras.serviceID",entityID="performedOnEntityID",data="serviceTextID",whereClause="metric.action = 'registered'"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get the event attendance that have been shared--->
	<cffunction name="getEventAttendanceSharedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="performedByEntityID",metricTable="relayActivity",join="inner join relayActivityShare ras on ras.relayActivityID = metric.ID and ras.shared = 1 and metric.performedByEntityTypeID=#application.entityTypeID.person# and metric.performedOnEntityTypeID=#application.entityTypeID.eventDetail# inner join service s on s.serviceID = ras.serviceID",entityID="performedOnEntityID",data="serviceTextID",whereClause="metric.action = 'attended'"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get the modules passed that have been shared--->
	<cffunction name="getModulePassSharedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="performedByEntityID",metricTable="relayActivity",join="inner join relayActivityShare ras on ras.relayActivityID = metric.ID and ras.shared = 1 and metric.performedByEntityTypeID=#application.entityTypeID.person# and metric.performedOnEntityTypeID=#application.entityTypeID.trngModule# inner join service s on s.serviceID = ras.serviceID",entityID="performedOnEntityID",data="serviceTextID",whereClause="metric.action = 'passed'"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get the certifications passed that have been shared--->
	<cffunction name="getCertificationPassSharedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="performedByEntityID",metricTable="relayActivity",join="inner join relayActivityShare ras on ras.relayActivityID = metric.ID and ras.shared = 1 and metric.performedByEntityTypeID=#application.entityTypeID.person# and metric.performedOnEntityTypeID=#application.entityTypeID.trngCertification# inner join service s on s.serviceID = ras.serviceID",entityID="performedOnEntityID",data="serviceTextID",whereClause="metric.action = 'passed'"}>
		<cfreturn result>
	</cffunction>


	<!--- Approvals/Profiles --->
	<!--- get person approvals --->
	<cffunction name="getPersonApprovedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="entityID",activityDate="created",metricTable="booleanFlagData",whereClause="metric.flagID=#application.com.flag.getFlagStructure(flagID='perApproved').flagID#",entityID="entityID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get person rejections --->
	<cffunction name="getPersonRejectedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="entityID",activityDate="created",metricTable="booleanFlagData",whereClause="metric.flagID=#application.com.flag.getFlagStructure(flagID='perRejected').flagID#",entityID="entityID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get organisation approvals --->
	<cffunction name="getOrganisationApprovedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null",organisationID="entityID",activityDate="created",metricTable="booleanFlagData",whereClause="metric.flagID=#application.com.flag.getFlagStructure(flagID='orgApproved').flagID#",entityID="entityID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get organisation rejections --->
	<cffunction name="getOrganisationRejectedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null",organisationID="entityID",activityDate="created",metricTable="booleanFlagData",whereClause="metric.flagID=#application.com.flag.getFlagStructure(flagID='orgRejected').flagID#",entityID="entityID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get number of profiles updated by the user --->
	<cffunction name="getProfileUpdatedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="modRegister",activityDate="modDate",personID="actionByPerson",entityID="flagID",postJoin="inner join modEntityDef med on metric.modEntityID = med.modEntityID and ((metric.recordID = p.personID and metric.entityTypeID=#application.entityTypeID.person#) or (metric.recordID = p.locationID and metric.entityTypeID=#application.entityTypeID.location#) or (metric.recordID = p.organisationID and metric.entityTypeID=#application.entityTypeID.organisation#)) and metric.flagID is not null"}>
		<cfreturn result>
	</cffunction>
	
<!--- 	<cffunction name="getPageSharedOnFacebookQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="urlShare",join="inner join service s on s.serviceID = metric.serviceID and s.serviceTextID='LinkedIn'",entityID="url"}>
		<cfreturn result>
	</cffunction> --->
	

	
	<!--- <cffunction name="getFacebookAccountsConnectedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="entityID",metricTable="serviceEntity",join="inner join service s on s.serviceID = metric.serviceID and s.serviceTextID='facebook' and metric.entityTypeID=#application.entityTypeID.person#",entityID="entityID"}>
		<cfreturn result>
	</cffunction> --->

	<!--- Discussions --->
	<!--- get discussions that a user started --->
	<cffunction name="getDiscussionStartedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="discussionMessage",entityID="discussionMessageID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get discussions liked --->
	<cffunction name="getDiscussionLikedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="entityLike",entityID="entityID",whereClause="entityTypeID = #application.entityTypeID.discussionMessage#"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get discussions commented on --->
	<cffunction name="getDiscussionCommentedOnQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="entityComment",entityID="entityID",whereClause="entityTypeID = #application.entityTypeID.discussionMessage#"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Training --->
	<!--- get specialisations awarded --->
	<cffunction name="getSpecialisationAwardedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null",organisationID="entityID",activityDate="passDate",metricTable="entitySpecialisation",whereClause="passDate is not null and metric.entityTypeID=#application.entityTypeID.organisation#",entityID="specialisationID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get specialisations expired --->
	<cffunction name="getSpecialisationExpiredQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null",organisationID="entityID",activityDate="expiredDate",metricTable="entitySpecialisation",whereClause="expiredDate is not null and metric.entityTypeID=#application.entityTypeID.organisation#",entityID="specialisationID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get modules started --->
	<cffunction name="getModuleStartedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="personID",activityDate="startedDateTime",metricTable="trngUserModuleProgress",join="inner join trngUserModuleStatus t on metric.moduleStatus = t.statusID and t.statusTextID='InProgress'",entityID="moduleID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get modules passed --->
	<cffunction name="getModulePassedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="personID",activityDate="userModuleFulfilled",metricTable="trngUserModuleProgress",join="inner join trngUserModuleStatus t on metric.moduleStatus = t.statusID and t.statusTextID='Passed'",entityID="moduleID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get certifications registered --->
	<cffunction name="getCertificationRegisteredQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="personID",activityDate="registrationDate",metricTable="trngPersonCertification",join="inner join trngPersonCertificationStatus t on metric.statusID = t.statusID and t.statusTextID='Registered'",entityID="certificationID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get certifications passed --->
	<cffunction name="getCertificationPassedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="personID",activityDate="passDate",metricTable="trngPersonCertification",join="inner join trngPersonCertificationStatus t on metric.statusID = t.statusID and t.statusTextID='Passed'",entityID="certificationID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get certifications update rules passed - Need to look at modRegister, to see that the previous status was pending  --->
	<cffunction name="getCertificationUpdatePassedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="actionByPerson",activityDate="modDate",metricTable="modRegister",join="inner join modEntityDef med on metric.modEntityID = med.modEntityID and med.fieldname='statusID' and metric.entityTypeID=#application.entityTypeID.trngPersonCertification# inner join trngPersonCertificationStatus old on metric.oldVal = cast(old.statusID as varchar) and old.statusTextID='Pending' inner join trngPersonCertificationStatus new on metric.newVal = cast(new.statusID as varchar) and new.statusTextID='Passed' inner join trngPersonCertification tp on tp.personCertificationID = metric.recordID",entityID="tp.certificationID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get certifications expired --->
	<cffunction name="getCertificationExpiredQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="personID",activityDate="expiredDate",metricTable="trngPersonCertification",join="inner join trngPersonCertificationStatus t on metric.statusID = t.statusID and t.statusTextID='Expired'",entityID="certificationID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Events --->
	<!--- get events registered --->
	<cffunction name="getEventRegisteredQueryInfo" access="private" returnType="struct" output="false">
		<!--- <cfset var result = {personID="entityID",activityDate="modDate",metricTable="modRegister",join="inner join modEntityDef med on metric.modEntityID = med.modEntityID and med.fieldname='regStatus' inner join eventFlagData e on metric.statusID = t.statusID and t.statusTextID='Expired'",entityID="flagID"}> --->
		<cfset var result = {personID="entityID",activityDate="regDate",metricTable="eventFlagData",whereClause="regStatus='RegistrationSubmitted'",entityID="flagID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get events attended --->
	<cffunction name="getEventAttendedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="entityID",activityDate="attendedDate",metricTable="eventFlagData",whereClause="regStatus='Attended'",entityID="flagID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Fund Requests --->
	<!--- get fund requests submitted --->
	<cffunction name="getFundRequestSubmittedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null", organisationID="partnerOrganisationID",activityDate="created", metricTable="fundRequestActivity",entityID="fundRequestActivityID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get fund requests approved --->
	<cffunction name="getFundRequestApprovedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null", organisationID="fra.partnerOrganisationID", activityDate="modDate", metricTable="modRegister",join="inner join fundRequestActivity fra on metric.recordID = fra.fundRequestActivityID and metric.entityTypeID=#application.entityTypeID.fundRequestActivity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='fundApprovalStatusID' inner join fundApprovalStatus fas on cast(fas.fundApprovalStatusID as varchar) = metric.newVal and fas.isFullyApproved=1",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get fund requests rejected --->
	<cffunction name="getFundRequestRejectedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="null", organisationID="fra.partnerOrganisationID", activityDate="modDate", metricTable="modRegister",join="inner join fundRequestActivity fra on metric.recordID = fra.fundRequestActivityID and metric.entityTypeID=#application.entityTypeID.fundRequestActivity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='fundApprovalStatusID' inner join fundApprovalStatus fas on cast(fas.fundApprovalStatusID as varchar) = metric.newVal and fas.fundStatusTextID='Rejected'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Opportunities --->
	<!--- get deals registered --->
	<cffunction name="getDealRegisteredQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="partnerSalesPersonID", metricTable="opportunity",join="inner join oppType ot on metric.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Deals'",entityID="opportunityID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get deal registrations requested --->
	<cffunction name="getDealRegistrationRequestedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="actionByPerson", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='statusID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Deals' inner join oppStatus os on cast(os.statusID as varchar) = metric.newVal and os.statusTextID='DealRegApplied'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get deal registrations approved --->
	<cffunction name="getDealRegistrationApprovedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="actionByPerson", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='statusID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Deals' inner join oppStatus os on cast(os.statusID as varchar) = metric.newVal and os.statusTextID='DealRegApproved'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>

	<!--- get deals closed --->
	<cffunction name="getDealClosedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="actionByPerson", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='stageID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Deals'  inner join oppStage os on cast(os.opportunityStageID as varchar) = metric.newVal and os.stageTextID in (#listQualify(application.com.settings.getSetting('leadManager.closedOppStages'),"'")#)",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get leads received from the locator --->
	<cffunction name="getLeadReceivedViaLocatorQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="newVal", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# and metric.action='OPPA' and opp.createdBy=404 inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Leads' inner join oppSource os on os.opportunitySourceID = opp.sourceId and os.opportunitySource='Locator'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get leads assigned --->
	<cffunction name="getLeadAssignedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="newVal", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='partnerSalesPersonID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Leads'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get leads updated --->
	<cffunction name="getLeadUpdatedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="actionByPerson", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Leads'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get leads closed --->
	<cffunction name="getLeadClosedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="actionByPerson", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='stageID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Leads'  inner join oppStage os on cast(os.opportunityStageID as varchar) = metric.newVal and os.stageTextID in (#listQualify(application.com.settings.getSetting('leadManager.closedOppStages'),"'")#)",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get renewals assigned--->
	<cffunction name="getRenewalAssignedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="newVal", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='partnerSalesPersonID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Renewal'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get renewals closed--->
	<cffunction name="getRenewalClosedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="newVal", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='stageID' inner join oppType ot on opp.oppTypeID = ot.oppTypeID and ot.oppTypeTextID='Renewal' inner join oppStage os on cast(os.opportunityStageID as varchar) = metric.newVal and os.stageTextID in (#listQualify(application.com.settings.getSetting('leadManager.closedOppStages'),"'")#)",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get renewals closed--->
	<cffunction name="getOpportunityLostQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="newVal", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='stageID' inner join oppStage os on cast(os.opportunityStageID as varchar) = metric.newVal and os.stageTextID = 'OppStageLost'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get special pricing requested--->
	<cffunction name="getSpecialPricingRequestedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {personID="newVal", activityDate="modDate", metricTable="modRegister",join="inner join opportunity opp on metric.recordID = opp.opportunityID and metric.entityTypeID=#application.entityTypeID.opportunity# inner join modEntityDef med on med.modEntityID = metric.modEntityID and med.fieldName='oppPricingStatusID' inner join oppPricingStatus ops on cast(ops.oppPricingStatusID as varchar) = metric.newVal and ops.statusTextID = 'spqRequested'",entityID="recordID"}>
		<cfreturn result>
	</cffunction>
	
	<!--- Incentives --->
	<!--- get incentive registrations--->
	<cffunction name="getRegisteredForIncentivesQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {activityDate="dateTermsAgreed", metricTable="rwPersonAccount",entityID="personID",whereClause="accountDeleted != 1 and testAccount != 1"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get claim registrations--->
	<cffunction name="getClaimsRegisteredQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="rwTransaction",entityID="rwTransactionID",whereClause="rwTransactionTypeID = 'AC'"}>
		<cfreturn result>
	</cffunction>
	
	<!--- get rewards redeemed--->
	<cffunction name="getRewardsRedeemedQueryInfo" access="private" returnType="struct" output="false">
		<cfset var result = {metricTable="rwTransaction",entityID="rwTransactionID",whereClause="rwTransactionTypeID = 'CL'"}>
		<cfreturn result>
	</cffunction>
</cfcomponent>