<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

file:  Relay\com\redirectorAllowedFunctions.cfc

These are all stub functions which call other functions in the application.com scope.
This allows us to control which functions are accessible to the redirector 
	- any expression containing application.com is rejected, but we can alias this component as say fn.

2008/07/30 	NYF		
2008-11-12 NYB P-SOP010
NYB 2011-03-07 IsUserInUsergroup
 --->

<cfcomponent displayname="functions which can be used within the redirector, accessible as fn." hint="">


	<cffunction access="public" name="hasUserAccessedScreenRecently" return="boolean" output="no">
			<cfargument name="screenID" required="yes" type="string">				
			<cfargument name="days" required="yes" type="numeric">				

			
 		<cfreturn application.com.screens.HasPersonAccessedScreenInLastXDays(personid = #request.relaycurrentuser.personid#, argumentcollection=#arguments#)>
	
	</cffunction>
	


	<cffunction name = "isFlagSet" return="boolean" output=no>
		<cfargument name="flagID" required="yes" type="string">

		<cfreturn application.com.flag.isFlagSetForCurrentUser(flagid = flagid)>
	
	</cffunction>

	<!--- WAb 2007/12/04 really for bringing back a list of boolean flags,  --->
	<cffunction name = "getFlagList" return="boolean" output=no>
		<cfargument name="flagGroupID" required="yes" type="string">
		<cfargument name="column" default="flagTextID">   <!--- could be flagid or name --->
		
		<cfset var result = "">
		<cfset var tmp = application.com.flag.getFlagGroupDataForCurrentUser(flaggroupid = flaggroupid)>

		<cfswitch expression="#column#">
			<cfcase value="flagTextID">
				<cfset result = valueList (tmp.flagTextID)>
			</cfcase>
			<cfcase value="flagID">
				<cfset result = valueList (tmp.flagID)>
			</cfcase>
			<cfcase value="Name">
				<cfset result = valueList (tmp.Name)>
			</cfcase>

		</cfswitch>
		
				
		<cfreturn result >
	
	</cffunction>
	
	
	<cffunction name = "isUserIncentiveRegistered" return="boolean" output=no>
		<cfif application.com.relayIncentive.isPersonRegistered(personid = request.relaycurrentuser.personid).recordcount is 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>


	<cffunction name = "doesUserHaveRights" return="boolean" output=no>
		<cfargument name="task" required="yes" type="string">
		<cfargument name="level" required="yes" type="string">
		
		<cfif isNumeric (arguments.level)><cfset level = "Level" & arguments.level> </cfif>
		<cfreturn application.com.login.checkInternalPermissions(securityTask = task, securityLevel = level)>
		
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function description     
		NYB 2011-03-07 - rewrote to use existing function                   
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name = "IsUserInUsergroup"> 
		<cfargument name="Usergroup" required="yes" type="string">
		<cfargument name="PersonID" type="numeric" default="#request.relaycurrentuser.personid#">		
   		<cfargument name="IncludePersonUserGroups" type="boolean" default="true"> 
		<cfset var IntUsergroups = application.com.login.isPersonInOneOrMoreUserGroups(UserGroups=arguments.Usergroup,argumentCollection=arguments)>
		<cfreturn IntUsergroups>
	</cffunction>

	
</cfcomponent>

