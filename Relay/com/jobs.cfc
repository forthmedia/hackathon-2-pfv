<!--- �Relayware. All Rights Reserved 2014 --->

<cfcomponent displayname="Common Relay Job Queries" hint="Contains a number of commonly used Relay Job Queries">

	<cffunction name="canTaskRun" access="public" returntype="boolean" hint="Returns whether a task is allowed to run.">
		<cfargument name="jobTypeID" type="numeric" required="Yes">
		<cfargument name="recordCount" type="numeric" required="Yes">
		
		<cfset result = "true">
		
		<cfquery name="jobThresholdLevel" datasource="#application.sitedatasource#">
			select threshold from jobType where jobTypeID = #arguments.jobTypeID#
		</cfquery>
		
		<!--- if the list of entityID's exceeds the trigger level for this job, create a job for this task otherwise proceed as normal --->
		<cfif (jobThresholdLevel.recordCount gt 0) and (arguments.recordCount gt jobThresholdLevel.threshold) >
			<cfset result = "false">
		</cfif>
		
		<cfreturn result>
		
	</cffunction>
	
	<cffunction name="createJob" access="private" returntype="numeric" hint="Creates a new relay job and returns the new job ID if successful.">
		<cfargument name="jobTypeID" type="numeric" required="Yes">
		
		<cfscript>
			var newJobID = -1;
			var createJob="";
		</cfscript>
		
		<cfquery name="createJob" datasource="#application.siteDataSource#">
			insert into job (jobTypeID,owner,created,lastRun) values (<cf_queryparam value="#arguments.jobTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,#request.relayCurrentUser.personID#,<cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			select scope_identity() as newJobID from job
		</cfquery>
		<cfset newJobID = #createJob.newJobID#>

		<cfreturn newJobID>
	</cffunction>
	
	<cffunction name="handleTaskData" access="public" returntype="struct" hint="Creates a new job for the task and inserts its data into the db.">
		<cfargument name="jobTypeID" type="numeric" required="Yes">
		<cfargument name="jobData1">
		<cfargument name="jobData2">
		<cfargument name="jobData3">
		<cfargument name="jobData4">
		<cfargument name="jobData5">
		<cfargument name="jobData6">
		
		<cfset var jobID = createJob(#arguments.jobTypeID#)>
		<cfset var result = StructNew()>
		
		<cfset result.jobCreated = "false">
		<cfset result.message = "">
		
		<cfquery name="getJobDataTable" datasource="#application.siteDataSource#">
			select jobDataTable from jobType where jobTypeID = #arguments.jobTypeID#
		</cfquery>
		
		<cfif jobID neq -1>
			<cfswitch expression="#arguments.jobTypeID#">
				<!--- Set Flag Job --->
				<cfcase value="1">
					<cfset result.jobCreated = insertSetFlagData(jobTypeID,getJobDataTable.jobDataTable,jobID,jobData1,jobData2,jobData3,jobData4,jobData5,jobData6)>
				</cfcase>
				<!--- Mass Approval Job --->
				<cfcase value="2">
					<cfset result.jobCreated = insertMassApprovalData(jobTypeID,getJobDataTable.jobDataTable,jobID,jobData1,jobData2,jobData3,jobData4,jobData5,jobData6)>
				</cfcase>
			</cfswitch>
		</cfif>
		
		<cfif result.jobCreated>
			<cfquery name="getJobCreationMessage" datasource="#application.sitedatasource#">
				select jobCreationMessage from jobType where jobTypeID = #arguments.jobTypeID#
			</cfquery>
			<cfif getJobCreationMessage.recordCount gt 0 and getJobCreationMessage.jobCreationMessage neq "">
				<cfset result.message = "phr_" & getJobCreationMessage.jobCreationMessage>
			<cfelse>
				<cfset result.message = "phr_sysJob_aJobHasBeenCreated. phr_sysJob_youWillReceiveANotification.">
			</cfif>
		</cfif>
		
		<cfreturn result>
		
	</cffunction>
	
	<cffunction name="insertSetFlagData" access="private" returntype="boolean" hint="Inserts data for a set flag job and returns true if successful">
		<cfargument name="jobTypeID" type="numeric" required="Yes">
		<cfargument name="jobDataTable" type="string" required="Yes">
		<cfargument name="jobID" type="numeric" required="Yes">
		<cfargument name="entityIDlist" type="string" required="Yes">
		<cfargument name="flagID" type="string" required="Yes">
		<cfargument name="EntityType" type="string" required="Yes">
		<cfargument name="FlagEntityType" type="string" required="Yes">
		<cfargument name="Value" type="string" required="Yes">
		<cfargument name="giveFeedBack" type="boolean" required="Yes">
		
		<cfset feedback = IIF(giveFeedBack,1,0)>
		
		<cfloop list="#entityIDlist#" index="entityID" delimiters=",">
			<cfloop index="idx" from="1" to="#listlen(arguments.flagID)#">
				<cfset thisflag = listGetAt(arguments.FlagID,idx)>
				<cfif arguments.Value is not "">
					<cfset thisflagValue = listGetAt(arguments.Value,idx)>
				<cfelse>
					<cfset thisflagValue = "">
				</cfif>
				<cfset getFlag =application.com.flag.getFlagStructure(thisFlag)>
				
				<cfquery name="insertSetFlagJobData" datasource="#application.siteDataSource#">
					insert into #arguments.jobDataTable# (jobID,flagID,entityID,entityType,flagEntityType,value,giveFeedback) values (<cf_queryparam value="#arguments.jobID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getFlag.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getFlag.entityTypeID#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#getFlag.entityType.TableName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#thisflagValue#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#feedback#" CFSQLTYPE="CF_SQL_INTEGER" >)
				</cfquery>
			</cfloop>
		</cfloop>
				
		<cfreturn true>
	</cffunction>
	
	<cffunction name="insertMassApprovalData" access="private" returntype="boolean" hint="Inserts data for a mass approval job and returns true if successful">
		<cfargument name="jobTypeID" type="numeric" required="Yes">
		<cfargument name="jobDataTable" type="string" required="Yes">
		<cfargument name="jobID" type="numeric" required="Yes">
		<cfargument name="frmEntityID" type="string" required="Yes">
		<cfargument name="frmEntityTypeID" type="numeric" required="Yes">
		<cfargument name="frmFlagEntityTypeID" type="numeric" required="Yes">
		<cfargument name="fgTextID" type="string" required="Yes">
		<cfargument name="radio_818_Value" type="string" required="Yes">
		<cfargument name="radio_836_Value" type="string" required="Yes">
		
		<cfloop list="#frmEntityID#" index="entityID" delimiters=",">
				
			<cfquery name="insertMassApprovalJobData" datasource="#application.siteDataSource#">
				insert into #arguments.jobDataTable# (jobID,frmEntityID,frmEntityTypeID,frmFlagEntityTypeID,fgTextID,radio_818,radio_836) values (<cf_queryparam value="#arguments.jobID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.frmEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.frmFlagEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.fgTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.radio_818_Value#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.radio_836_Value#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfquery>
		</cfloop>
				
		<cfreturn true>
	</cffunction>
	
	<cffunction name="runJob" access="public" returntype="boolean" hint="Runs a specified job">
		<cfargument name="jobID" type="numeric" required="Yes">
		
		<cfquery name="getJobTypeInfo" datasource="#application.siteDataSource#">
			select jt.jobTypeID, rowsToExecute, jobDataTable, owner from jobType jt inner join job j on
			jt.jobTypeID = j.jobTypeID where 
			j.jobID = #arguments.jobID#
		</cfquery>
		
		<cfif getJobTypeInfo.recordCount gt 0 and getJobTypeInfo.rowsToExecute gt 0>
			<cfquery name="getJobData" datasource="#application.siteDataSource#">
				select top #getJobTypeInfo.rowsToExecute# * from #getJobTypeInfo.jobDataTable#
				where completed is null and jobID = #arguments.jobID#
			</cfquery>
		</cfif>

		<cfswitch expression="#getJobTypeInfo.jobTypeID#">
			<!--- running a setFlagJob --->
			<cfcase value="1">
				<cfset result = runSetFlagJob(getJobData,getJobTypeInfo.owner)>
			</cfcase>
			
			<!--- running a Mass Approval Job --->
			<cfcase value="2">
				<cfset result = runMassApprovalJob(getJobData,getJobTypeInfo.owner)>
			</cfcase>
			
		</cfswitch>
		<!--- do we have jobs that have completed? This could be the job just run or other jobs in the queue. It's possible 
			that job data may be duplicated across various jobs and therefore several jobs have the chance of getting done
			in one run. --->
		<cfquery name="getCompletedJobs" datasource="#application.siteDataSource#">
			select distinct jobID from #getJobTypeInfo.jobDataTable# where completed =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >  and 
				 jobID not in (select jobID from #getJobTypeInfo.jobDataTable# where completed is null)		  
		</cfquery>
		
		<cfif getCompletedJobs.recordCount gt 0>
			<!--- if we have jobs that have completed, update them --->
			<cfquery name="setJobToCompleted" datasource="#application.siteDataSource#">
				update job set completed =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >  where jobID  in ( <cf_queryparam value="#ValueList(getCompletedJobs.jobID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
		
		
			<cfloop query="getCompletedJobs">
				<!--- send email notifying owner that job has been completed --->
				<cfquery name="getJobTypeEmailDetails" datasource="#application.siteDataSource#">
					select j.owner, jt.jobCompletionNotificationEmailTextID from job j inner join jobType jt
					on j.jobTypeID = jt.jobTypeID where j.jobID = #jobID#
				</cfquery>
				
				<cfset email = application.com.email.sendemail(#getJobTypeEmailDetails.jobCompletionNotificationEmailTextID#,#getJobTypeEmailDetails.owner#)>
				<cfif email neq "Email Sent">
					<cfmail to="errors@foundation-network.com" from="#request.CurrentSite.Title#" subject="Error sending Job #jobID# Completed Notifcation Email to user #getJobTypeEmailDetails.owner#">
Job #jobID# has just completed, but there were problems sending the notification email to user #getJobTypeEmailDetails.owner#.
					</cfmail>
				</cfif>
			</cfloop>
		</cfif>

		<cfquery name="updateJobLastRun" datasource="#application.siteDataSource#">
			update job set lastRun =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >  where jobID = #arguments.jobID#
		</cfquery>
		
		<cfreturn true>
	</cffunction>
	
	<cffunction name="runSetFlagJob" access="private" hint="Runs a Set Flag job">
		<cfargument name="jobData" type="query" required="yes">
		<cfargument name="jobOwner" type="numeric" required="yes">
		
		<cfloop query="jobData">
			<cf_SetFlag 
				flagID="#flagid#"
				entityId = "#entityID#"
				EntityType = "#entityType#"
				FlagEntityType = "#flagEntityType#"
				Value = "#value#"
				giveFeedBack = "#giveFeedBack#"
				userID = "#jobOwner#">
				
			<cfquery name="updateCompletedJobData" datasource="#application.siteDataSource#">
				update jobData_1 set completed =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >  where
					jobID =  <cf_queryparam value="#jobID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
					flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
					entityId =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
					EntityType =  <cf_queryparam value="#entityType#" CFSQLTYPE="cf_sql_varchar" >  and
					FlagEntityType =  <cf_queryparam value="#flagEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
					Value =  <cf_queryparam value="#value#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
					giveFeedBack =  <cf_queryparam value="#giveFeedBack#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					--and completed is null
			</cfquery>
		</cfloop>
		
	</cffunction>
	<cffunction name="runMassApprovalJob" access="private" hint="Runs a Mass Approval Job">
		<cfargument name="jobData" type="query" required="yes">
		<cfargument name="jobOwner" type="numeric" required="yes">
		
		<cfset frmAction = "update">
		<cfset UserID = arguments.jobOwner>
		<cfset fieldNames = "">
		
		<!--- building the list of entity id's --->
		<cfset frmEntityID = ValueList(jobData.frmEntityID)>
		
		<!--- getting the constant values for the mass approval job --->
		<cfquery name="getJobConstants" dbtype="query">
			select distinct jobID,frmEntityTypeID,frmFlagEntityTypeID,fgTextID,radio_818,radio_836 from jobData
		</cfquery>
		
		<!--- using valueList should they change from being constants, but really is not necessary at the moment --->
		<cfset frmEntityTypeID = ValueList(getJobConstants.frmEntityTypeID)>
		<cfset frmFlagEntityTypeID = ValueList(getJobConstants.frmFlagEntityTypeID)>
		<cfset fgTextID = ValueList(getJobConstants.fgTextID)>
		<cfset Radio_818 = ValueList(getJobConstants.Radio_818)>
		<cfset Radio_836 = ValueList(getJobConstants.Radio_836)>
		
		<cfif Radio_818 neq "">
			<cfset fieldNames = listAppend(fieldNames,Radio_818)>
		</cfif>
		<cfif Radio_836 neq "">
			<cfset fieldNames = listAppend(fieldNames,Radio_836)>
		</cfif>
		
		<cfset request.runningMassApproval = "true">
		
		<cfinclude template = "/content/sony1/sony1registration-MassApproval.cfm">
		
		<!--- have to reset the frmEntityID to point to the people, not the orgs --->
		<cfset frmEntityID = ValueList(jobData.frmEntityID)>
		<cfset frmEntityTypeID = 0>

		<cfquery name="updateCompletedJobData" datasource="#application.siteDataSource#">
			update jobData_2 set completed =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_VARCHAR" >  where
				jobID =  <cf_queryparam value="#getJobConstants.jobID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				frmEntityID  in ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and 
				frmEntityTypeID =  <cf_queryparam value="#frmEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				frmFlagEntityTypeID =  <cf_queryparam value="#frmFlagEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
				fgTextID =  <cf_queryparam value="#fgTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
				Radio_818 =  <cf_queryparam value="#Radio_818#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
				Radio_836 =  <cf_queryparam value="#Radio_836#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				--and completed is null
		</cfquery>
	</cffunction>
</cfcomponent>
