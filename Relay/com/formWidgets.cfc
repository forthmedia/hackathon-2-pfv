<cfcomponent accessors="true" output="false" persistent="false">

	<cffunction name="getWidgetsOptionHTML" access="public" returnType="string">
		<cfargument name="widgetTextID" required="true" type="string" >
		<cfscript>
			buildCacheIfNeeded();
			var widgetData= this.widgetData[widgetTextID];
			var widgetCFC=createObject("component",widgetData.path);

			//we interpret its constructor to see if we can find any parameters
			var widgetMetaData=GetMetaData(widgetCFC);

			var initFunctionMetadata={PARAMETERS=[]}; //no opp default

			for(var i=1;i<=ArrayLen(widgetMetaData.functions);i++){
				if (widgetMetaData.functions[i].name EQ "init"){
					initFunctionMetadata=widgetMetaData.functions[i];
					break; //found init, early out
				}
			}

		</cfscript>

		<cfsavecontent variable="local.optionHTML">
			<cfset var parameter = "">
			<cfloop array="#initFunctionMetadata.PARAMETERS#" index="parameter">
				<cfif parameter.name NEQ "templateHandler"> <!--- Template handler is provided by the framework --->
					<cfscript>
						//fill in some sensible defaults
						param parameter.required=false;
						param parameter.parameterPhrase=parameter.name;
						param parameter.type="text";

					</cfscript>

					<cfif structKeyExists(parameter,"validValues") OR parameter.type eq "boolean">
						<cfscript>
							if (parameter.type eq "boolean"){
								parameter.type="select";
								parameter.validValues="yes,no";
							}

							var validValueArguments = {};
							validValueArguments.string=parameter.validValues;

							if (structKeyExists(parameter,"display")){
								validValueArguments.displayColumn=parameter.display;
							}
							if (structKeyExists(parameter,"value")){
								validValueArguments.valuecolumn=parameter.value;
							}



							var validValueQuery=application.com.relayForms.getValidValuesFromString(argumentCollection=validValueArguments);

						</cfscript>

						<cf_relayFormElementDisplay
							relayFormElementType="select"
							fieldname="#parameter.name#"
							query="#validValueQuery#"
						    required="#parameter.required#"
							label="#parameter.parameterPhrase#"
							currentValue=""> <!---Current value is inserted by javascript --->


					<cfelse>
						<cf_relayFormElementDisplay
							relayFormElementType="#parameter.type#"
							fieldname="#parameter.name#"
						    required="#parameter.required#"
							label="#parameter.parameterPhrase#"
							currentValue=""><!---Current value is inserted by javascript --->
					</cfif>



				</cfif>

			</cfloop>


		</cfsavecontent>

		<cfreturn optionHTML>
	</cffunction>


	<cfscript>

		private void function buildCacheIfNeeded(){
			lock name="widgetCacheLock" timeout="10" type="exclusive" {
				if (!structKeyExists(this,"widgetData")){
					var searchLocations=[];
					searchLocations[1]="relay\formWidgets\widgets";
					searchLocations[2]="code\formWidgets\widgets";

					this.widgetData={};
					this.widgetDataByEntityTypeID={};

					for(var i=1;i<arrayLen(searchLocations);i++){
						buildCacheFromSearchLocation(searchLocations[i]);
					}

					//after finding the cfcs the templates are searched for. Again core can be replaced by code

					for(key in this.widgetData){

						for(var j=1;j<arrayLen(searchLocations);j++){
							var searchLocation=searchLocations[j];
							var fileLocation=searchLocation & "\" & key & "\" & key & ".js";
							var absoluteFileLocation=fileLocation;

							//put in the real locations on relay and code
							if(absoluteFileLocation.startsWith("code")){
								absoluteFileLocation=replace(absoluteFileLocation,"code",application.paths.code);
							}else{
								absoluteFileLocation=replace(absoluteFileLocation,"relay",application.paths.relay);
								fileLocation=replace(fileLocation,"relay\","");
							}

							if (fileExists(absoluteFileLocation)){
								this.widgetData[key].jstemplate= "\" & fileLocation;
							}


						}

					}

				}
			}

		}


		/**
		 * Searches for CFCs in the search location and adds their data to the cache. Note that cfcs with the same name replace each other.
		 * So add the replacing (e.g. code) locations last
		 **/
		private void function buildCacheFromSearchLocation(String searchLocation){
			var widgetCFCs=application.com.applicationVariables.getCFCs(relativepath=searchLocation);

			for(key in widgetCFCs){
				var widgetCFCData=widgetCFCs[key];
				var widgetCFC=createObject("component",widgetCFCData.path);
				var widgetMetaData=GetMetaData(widgetCFC);
				var widgetNamePhrase=structKeyExists(widgetMetaData,"widgetNamePhrase")?widgetMetaData.widgetNamePhrase:'phr_widget_'&widgetCFCData.name;

				var entityTypes=[];

				if (structKeyExists(widgetMetaData,"entityTypes")){
					var entityTypesRaw=widgetMetaData.entityTypes;

					var entityTypesArray=ListToArray(entityTypesRaw);

					for(var i=1;i<=arrayLen(entityTypesArray);i++){
						if (isNumeric(entityTypesArray[i])){
							entityTypes.add(entityTypesArray[i]);
						}else{
							//convert name to numeric IDs
							arrayappend(entityTypes,application.entityTypeID[entityTypesArray[i]]);
						}
					}
				}

				this.widgetData[widgetCFCData.name]={
					widgetIdentifier=widgetCFCData.name,
					widgetNamePhrase=widgetNamePhrase,
					path=widgetCFCData.path,
					entityTypes=entityTypes,
					jstemplate=""
				};
			}

			//reprocess widgets as available by entityType
			for(var entityTypeID in application.entityType) {

				var widgetsForThatEntity={};
				for(var widgetName in this.widgetData){
					if (this.widgetData[widgetName].entityTypes.isEmpty() OR ArrayContains(this.widgetData[widgetName].entityTypes,entityTypeID)){
						widgetsForThatEntity[widgetName]=this.widgetData[widgetName];
					}
				}
				this.widgetDataByEntityTypeID[entityTypeID]=widgetsForThatEntity;
			}
		}


		public struct function getformWidgets(required numeric entityTypeID) hint="returns details of all the available widgets for an entity type" {
			buildCacheIfNeeded();


			return this.widgetDataByEntityTypeID[entityTypeID];
		}

		/**
		 * handlerString is expected to be a string in the form "option1=Value,option2=Value"
		 **/
		public any function getHandler(required String widgetName,required String handlerString){
			var handlerOptions=convertEvalStringOptionsToStruct(handlerString);


			buildCacheIfNeeded();
			handlerOptions.jstemplate=this.widgetData[widgetName].jstemplate;
			return createObject("component",this.widgetData[widgetName].path).init(argumentCollection=handlerOptions);
		}

		/**
		 * handlerString is expected to be a string in the form "option1=Value,option2=Value". Returns a struct of those options
		 **/
		private struct function convertEvalStringOptionsToStruct(String handlerString=""){
			var handlerOptions={};
			var optionParingArray=ListToArray(handlerString);

			for(var i=1;i<=ArrayLen(optionParingArray);i++){
				var optionName=ListGetAt(optionParingArray[i], 1,"=");
				if(listLen(optionParingArray[i],"=") EQ 2){
					optionValue=ListGetAt(optionParingArray[i], 2,"=");
				}else{
					optionValue="";
				}
				handlerOptions[optionName]=optionValue;
			}
			return handlerOptions;

		}




	public struct function getWidgetData (required any entity, required string widgetTextID, string widgetArguments="", boolean readonly=false) {

		var widgetHandler=getHandler(arguments.widgetTextID,arguments.widgetArguments);
		widgetHandler.onLoad(entity=arguments.entity,readonly=arguments.readonly);
		return {lineHTML=widgetHandler.getLineHTML(), buttonAttributes=widgetHandler.getButtonAttributes()};

	}

	</cfscript>

</cfcomponent>