<!--- �Relayware. All Rights Reserved 2014 --->
<!--- change history

When		Who		What
2015-08-05	DXC		Case 445410 - Long URLs make the function addEntityTrackingUrl query insertUrl fail because the MSSQL index is too long... --->
--->
<cfcomponent displayname="entityFunctions" hint="Various generic entity functions">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="addEntityTracking" hint="Adds an entity tracking record to the database." output="false">
		<cfargument name="entityID" type="numeric" required="yes">
		<cfargument name="entityTypeID" type="string" required="yes">
		<cfargument name="personid" type="numeric" required="yes">
		<cfargument name="goToURL" type="string">
		<cfargument name="commid" type="numeric">
		<cfargument name="fromUrl" type="string">
		<cfargument name="visitID" type="numeric" default="#request.relayCurrentUser.visitID#">
		<cfargument name="clickThru" type="boolean" default="false">  <!---  2013/11/04	NJH Case 437772 - add clickthru. --->

		<cfscript>
			var addEntityTracking = "";
		</cfscript>

		<!--- WAB 2013-04-30 Add cf_queryparam (for N varchar support) --->
		<cfquery name="addEntityTracking">
			insert into entityTracking  (entityID, entityTypeID, Personid,toLoc, fromLoc,created, commid, visitID,clickThru)
			select
				<cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">,
				<cf_queryParam value="#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityTypeID).entityTypeID#" cfsqltype="CF_SQL_INTEGER">,
				<cf_queryParam value="#arguments.personid#" cfsqltype="CF_SQL_INTEGER">,
				<cfif structKeyExists(arguments,"goToURL") and arguments.goToUrl neq "">dbo.entityTracking_removeUnwantedUrlVariables(<cf_queryParam value="#arguments.goToURL#" cfsqltype="CF_SQL_VARCHAR">)<cfelse> null </cfif>,
				<cfif structKeyExists(arguments,"fromUrl") and arguments.fromUrl neq "">dbo.entityTracking_removeUnwantedUrlVariables(<cf_queryParam value="#arguments.fromURL#" cfsqltype="CF_SQL_VARCHAR">) <cfelse> null	</cfif>,
				getDate(),
				<cfif structKeyExists(arguments,"commid")> <cf_queryParam value="#arguments.commid#" cfsqltype="CF_SQL_INTEGER"> <cfelse> null </cfif>,
				<cfif structKeyExists(arguments,"visitID")><cf_queryParam value="#arguments.visitID#" cfsqltype="CF_SQL_INTEGER"><cfelse>null</cfif>,
				<cf_queryParam value="#arguments.clickThru#" cfsqltype="CF_SQL_BIT">
		</cfquery>

		<cfreturn "Add a tracking record">
	</cffunction>


	<!--- <cffunction name="addEntityTrackingUrl" type="public" returnType="struct" output="false">
		<cfargument name="pageURL" type="string" default="#request.currentSite.protocolAndDomain##request.query_string_and_script_name#">

		<cfset var urlStruct = structNew()>
		<cfset var entityTrackingUrl = listFirst(arguments.pageURL,"?")>
		<cfset var urlParams = "">
		<cfset var formStateResult = structNew()>
		<cfset var result = {ID=0}>
		<cfset var unwantedVar = "">
		<!--- 2014-03-19 AXA Case 439134 added returnURL
			2015-10-13 WAB 	WAB 2015-10-13	CASE PROD2015-203 added debug & p
		--->
		<cfset var unwantedVarList = "rwsessionToken,formState,_cf_noDebug,spl,courseSortOrder,moduleSortOrder,jsessionid,flush,returnURL,urlrequested,debug,p">

		<cfif listLen(arguments.pageURL,"?") gt 1>
			<cfset urlParams = listLast(arguments.pageURL,"?")>
			<cfset urlStruct = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString=urlParams,delimiter="&")>
		</cfif>

		<cfset formStateUrl = "">
		<cfif structKeyExists(urlStruct,"formstate")>
			<!--- WAB 2014-11-03 CASE 442412, problem when more than one formstate on the URL, added a new function which deals --->
			<cfset formStateResult = application.com.security.decryptFormStateVariable(formStateString=urlStruct.formstate)> <!--- decryptFormStateVariableList(formStateStringList=urlStruct.formstate)--->
			<cfif formStateResult.isOK>
				<cfset structAppend(urlStruct,formStateResult.result)>
			</cfif>
		</cfif>

		<cfloop list="#unwantedVarList#" index="unwantedVar">
			<cfset structDelete(urlStruct,unwantedVar)>
		</cfloop>

		<cfset sortedStructList = listSort(structKeyList(urlstruct),"textNoCase","asc")>
		<!--- sort the url variables so that we have a better chance of matching existing records --->
		<cfset urlParams = application.com.structureFunctions.convertStructureToNameValuePairs(struct=urlstruct,delimiter="&",keyList=sortedStructList)>

		<cfif urlParams neq "">
			<cfset entityTrackingUrl = entityTrackingUrl&"?"&urlParams>
		</cfif>

		<!--- start case 445410 - Long URLs make the insert fail because the index is too long... --->
		<cfif len(entityTrackingURL) gt 400>
			<cfset entityTrackingUrl=left(entityTrackingUrl,400)>
		</cfif>
		<!--- end case 445410  --->

		<!--- START 20150714 AHL SAML Error connecting to partner portal --->
		<!--- Trunkating Large URL to 999 characters to fit within the db column --->
		<cfquery name="insertUrl" datasource="#application.siteDataSource#">
			if not exists (select 1 from entityTrackingUrl where url=<cf_queryParam  value="#Left(entityTrackingUrl,250)#" cfsqltype="cf_sql_varchar">)
			insert into entityTrackingUrl (url) values (<cf_queryParam  value="#Left(entityTrackingUrl,250)#" cfsqltype="cf_sql_varchar">)

			select entityTrackingUrlID from entityTrackingURL where url=<cf_queryParam  value="#Left(entityTrackingUrl,250)#" cfsqltype="cf_sql_varchar">
		</cfquery>
		<!--- END 20150714 AHL SAML Error connecting to partner portal --->

		<cfset result.ID = insertUrl.entityTrackingUrlID>

		<cfreturn result>
	</cffunction> --->


	<cffunction name="saveEntityComment" access="public">
		<cfargument name="entityCommentID" required="false" default="">
		<cfargument name="entityTypeID" required="true">
		<cfargument name="entityID" required="true">
		<cfargument name="comment" type="string" required="false">

		<cfset var returnValue = true>
		<cfset var qModifyComment = "">
		<cfset var qInsertComment = "">
		<cfset var isAdmin = false>

		<cftry>
			<cfif isNumeric(arguments.entityCommentID)>
				<cfquery name="qModifyComment" datasource="#application.siteDataSource#">
					UPDATE entityComment
					SET comment = <cf_queryParam value="#arguments.comment#" cfsqltype="CF_SQL_VARCHAR">
					WHERE entityCommentID = <cf_queryParam value="#arguments.entityCommentID#" cfsqltype="CF_SQL_INTEGER">
						AND entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
						AND entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
						<cfif not application.com.login.checkInternalPermissions("socialTask", "Level3")>AND personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER"></cfif>
				</cfquery>

			<cfelse>
				<cfquery name="qInsertComment" datasource="#application.siteDataSource#">
					INSERT INTO entityComment
						(entityTypeID, entityID, comment, personID, createdBy)
					VALUES
						(<cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_VARCHAR">,
						<cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_VARCHAR">,
						<cf_queryParam value="#arguments.comment#" cfsqltype="CF_SQL_VARCHAR">,
						<cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">)
				</cfquery>
			</cfif>
			<cfcatch>
				<cfset returnValue = "#cfcatch.message# #cfcatch.detail#">
			</cfcatch>
		</cftry>

		<cfreturn returnValue>
	</cffunction>


	<cffunction name="getEntityComment" access="public">
		<cfargument name="entityTypeID" required="true">
		<cfargument name="entityID" required="true">
		<cfargument name="searchString" required="false" default="">
		<cfargument name="pageNum" required="false" default="">
		<cfargument name="recordsPerPage" required="false" default="300">
		<cfargument name="cacheQuery" type="boolean" default="true">

		<cfset var getEntityCommentSQL = "">
		<cfset var tempTable = "####entityComments_#replace(replace(session.sessionID,'-','','ALL'),'.','')#">
		<cfset var result = {recordSet=queryNew("entityCommentID,comment,personID,firstName,lastName,pictureURL,organisationName,created,likes"),hasMore=false}>

		<cfif isNumeric(arguments.entityID) and isNumeric(arguments.entityTypeID)>
			<cfprocessingdirective suppresswhitespace="yes">
				<cfsavecontent variable="getEntityCommentSQL">
					<cfoutput>
						SELECT c.entityCommentID, c.comment, c.personID, p.firstName, p.lastName, p.pictureURL, o.organisationName, c.created, count(distinct l.personid) as likes
						FROM entityComment c
						INNER JOIN person p on c.personID = p.personID
						INNER JOIN organisation o on p.organisationid = o.organisationid
						LEFT JOIN entityLike l ON c.entityCommentID = l.entityID AND l.entityTypeID = <cf_queryParam value="#application.entityTypeID.entityComment#" cfsqltype="CF_SQL_INTEGER">
						<cfif trim(arguments.searchString) is not "">INNER JOIN CONTAINSTABLE(vEntityCommentSearch,*,<cf_queryparam value='#application.com.search.prepareSearchString(arguments.searchString)#' cfsqltype="CF_SQL_VARCHAR">) as s ON c.entityCommentID = s.[key]</cfif>
						WHERE c.entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
						AND c.entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
						AND c.active = 1
						GROUP BY c.entityCommentID, c.comment, c.personID, p.firstName, p.lastName, p.pictureURL, o.organisationName, c.created
						ORDER BY c.created asc
					</cfoutput>
				</cfsavecontent>
			</cfprocessingdirective>

			<cftry>
				<cfif not arguments.cacheQuery>
					<cfquery name="recordSet" datasource="#application.sitedataSource#">
						#preserveSingleQuotes(getEntityCommentSQL)#
					</cfquery>
					<cfset result.recordSet = recordSet>
				<cfelse>
					<cfif arguments.pageNum eq 0 or not isNumeric(arguments.pageNum)>
						<!--- if we're refreshing the feed, then delete the existing cached query and refresh it --->
						<cfset application.com.dbTools.deleteCachedQuery(tempTable=tempTable)>
						<cfset structAppend(result,application.com.dbTools.dbCacheQuery(tempTable=tempTable,queryString=getEntityCommentSQL,pageSize=recordsPerPage),true)>
					<cfelse>
						<cfset structAppend(result,application.com.dbTools.getRecordsFromCachedQuery(tempTable=tempTable),true)>
					</cfif>
				</cfif>

				<cfcatch>
					<cfset application.com.errorHandler.recordRelayError_ColdFusion(error=cfcatch)>
				</cfcatch>
			</cftry>
		</cfif>

		<cfset result.tablename=tempTable>

		<cfreturn result>
	</cffunction>


	<cffunction name="deleteEntityComment" access="public">
		<cfargument name="entityCommentID" type="numeric" required="true" default="">
		<cfset var returnValue = true>
		<cftry>
			<cfquery name="qDeleteComment" datasource="#application.siteDataSource#">
				UPDATE entityComment
				SET active = 0,
					lastUpdatedBy = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">
				WHERE entityCommentID = <cf_queryParam value="#arguments.entityCommentID#" cfsqltype="CF_SQL_INTEGER">
					<cfif not application.com.login.checkInternalPermissions("socialTask", "Level3")>AND createdBy = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER"></cfif>
			</cfquery>
			<cfcatch>
				<cfset var returnValue = false>
			</cfcatch>
		</cftry>
		<cfreturn returnValue>
	</cffunction>


	<cffunction name="getEntityLike" access="public">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfset var stLikes = {}>
		<cfquery name="qGetLikes" datasource="#application.siteDataSource#">
			SELECT entityID
			FROM entityLike
			WHERE entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
			AND personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfloop query="qGetLikes">
			<cfset stLikes[entityID]=1>
		</cfloop>
		<cfreturn stLikes>
	</cffunction>


</cfcomponent>
