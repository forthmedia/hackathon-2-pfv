<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			opportunity.cfc
Author:				SWJ
Date started:		2003

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Date (01-MAR-2005)	AJC	CR_ATI002_superAM	Function to assign a super manager to a lead's Viewing Rights
Date (08-MAR-2005)	AJC	CR_ATI009_paymentterms	Function to get all opp payment terms
Date (08-MAR-2005)	AJC	CR_ATI005_newcustomer	Function to get all opp customer types
2006-10-24 			 WAB CR_ATI036 when special price is Zero use COGS to calculate margins
2007/07/10  		WAB lighthouse bug 6, spapproverpersonids needs setting to null if a product is added/changed
24-Aug-07			SWJ	Added a few functions for SNY
2008-04-29 			WAB added nvarchar support to entity notes
2008-06-18				NYF 	 NABU bug 451 - updated maxlength of currentSituation param from 500 to 1000
2009/01/20			NJH	Bug Fix ATI Issue 1624 - modified specialPriceChange function to accept comments and an opportunityID. This function is now called when products are deleted.
2009/02/24 			WAB Bug 1881 - problem in getOppProducts when ordered by probabiilty
2009/04/27 GCC LID:2137 get SPApproverLevelRequired from DB
2009/08/24			NJH	P-ATI005 changed the way 'checkApproverLevelRequired' is called so that we could over-ride the function.
2009/09/28			NJH	P-CYM006 - added 4 custom fields to the opportunity table which we insert, get and update.
2009/09/29			NJH	LID 2689 - removed campaigns from lead source query.
2009/10/28			NJH	P-FNL079 - added sortIndex to a number of queries used in selects. Also get people flagged with 'isSalesOperationsAdmin' for the account manager query if the
					sales force module is turned on. Also added listQualify to the queries instead of having lists in single quotes, but if this is released, opportunityINI will need
					to be modified so that single quotes are removed from the lists.
2009/11/02			AJC P-LEX039 Competitive products
2010/01/14			NJH	LID 2702 - added campaignIDs to opportunities
2010/02/24			NAS P-LEX039 Demo products
2010/04/16			GCC	P-FNL079 - added sortIndex to one that had been missed
2010/05/28			AJC	LID 3486 Order Approval email - no customer Org or customer contact merge fields displayed
2010/06/09			NAS	LID 3174 - SPQ 100% Discount Broken
2010/06/23			NAS P-CLS002
2010-07-13 NYB Lexmark LHID 3636
2010/09/09			NJH	LID 3872 - when renewing an asset, set the renewal location to be the organisation's HQ, or the first location created at that org
2010/11/03			PPB LID 4686 - pricebookentry problem
2010/12/01 			PPB LID 5006 - apply promotion discount for triggerpoint='subtotal'
2010/12/02 			PPB LID 4971 - added checks that promotion is still in country/visibility scope so an appropriate message is displayed and the previously discounted amount shows in red
2011/01/14			WAB	8.3		Added an initialise  function to replace a bit of the psuedo constructor which was referencing application scope and therefore could not be reloaded remotely
2011/06/23 			PPB LID6918 setting changed from leadManager.leadVendorOrgIds to theClient.clientOrganisationID
2011/10/03 			PPB NET005 getOppProductGroups: product group must be active
2011/11/14 			PPB Kerio - use of leadManager.partnerRenewalTimeSpan changed
2011/11/14			NYB	LHID8224 Changed RecordID created,updated, etc to translations
						 added translations to Select text of functions: getDistiLocationID,getcontactPersonID,getsponsorPersonid,getTerritoryID,getsourceID,getCampaignID,getPartnerLocationID,getDistiSalesPersonID,getProductList,getProductAndGroupList,getProductGroupList
2011/11/30			NYB	LHID8245 added phrasetextid to getStatusID
2011/12/02			NJH Master Demo Enchancements Phase 3 - created a new function to return the opps a partner has rights to. It was taken from the oppCustomerController
					function.
2012/03/19 			PPB P-LEX070 defend against non-existence of the flag supplied in settings('leadManager.distiLocFlagList')
2012/05/09 			PPB P-LEX070 re-vamp of getOppPartners
2012/05/21 			PPB P-LEX070 facilitate translation of stageId
2012/06/19			RMB	P-LEX070 Added oppStatusText to show in My Opportunities
2012/08/22 			NAS 430196 Deal Reg Form does not ignore Product Groups with InActive products
2012-09-19 			PPB Case 430388 filter to ALL locations of the partners organisation
2014-01-07 			PPB Case 437061 moved the default setting for personID in GetOppPromotionProducts()
2014-01-09 			NYB Case 438437 removing organisations that have been flagged for deletion from Opp Disti list
2014-01-13 			PPB Case 436434 don't refreshPromotions if the opp has no products
2014-04-28 			PPB	Case 439350 var'ed vble to avoid clash with getOppProducts function in custom file
2014-05-13			NJH	Salesforce/Connector work. Accept an ID as string for a number of arguments as that a 'null' can be passed through. I'm trying to clean up the DB structure and enforce referential integrity.
2014-05-13			NJH	Also changed a number of the disti functions that worked off either an org or location ID, to work off only a locationID.
2014-09-02			RPW	Add Lead Screen on Portal
2014-09-02 			PPB Webroot snaglist 140/Case 441543  getOppDistiLocations : removed type="numeric" in case an empty string is sent (partnerLocationId is null)
2014-09-22			AHL Case 441630 Inactive Distributor should not be displayed in "Distributor" drop down
2014-10-23			RPW	CORE-901 Error seen while  i request for special Pricing(Opportunities) in internal site - Added column specialPrice
2014-10-06			WAB/AHL Case 441992 more var'ing (same problem as 439350)
2014-11-18			RPW	CORE-97 Phase 1: Extend database fields for Leads
2014-11-06			AHL Case 442515 Nulls in DistiLocationID on Creation. 'type="numeric"' was causing problems with NULLs through queries
2015-01-13			WAB	Case 442515 some mods while merging into PartnerCloud
2015-01-13 			DXC Case 443312 Added DISTINCT clause as multiple flags can cause user repetition
2015/08/05			NJH	Look for a default pricebook if we can't match on currency/country combo. Tidied up a number of functions that worked out which pricebook to get. (ie. did what was suggested below and got getOppProductBasePrice function to call getOppPricebook)
2015/09/22			NJH	Jira Prod2015-34 - get opp details from vOpportunity, rather than opportunity
2015/09/30			NJH	PROD2015-35 - added TFQO filtering for partner opportunties
2015-11-16          DAN PROD2015-364 - products country restrictions
2016/04/27			NJH Sugar 448757 - SQL performance in getOpportunitiesAPartnerHasRightsTo function. Join to flag tables rather than using in-statement. This function should not be used anymore. Changed functions that used this function to use rights.getRightsFilterQuerySnippet instead.
2016/12/12			NJH JIRA PROD2016-2937 - remove the reseller OrgID parameter. Now done in rights. Added rights sql to various opportunities queries

Possible enhancements:

certain functions (eg getOppProductBasePrice) should call getPricebook or getOppPricebook rather than working it out again in a subquery

 --->

<cfcomponent hint="Create, Update and Get methods for opportunity" displayname="opportunity Component">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             SET UP ONE PARAM PER DB COLUMN
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cfparam name="THIS.opportunityID" type="numeric" default = "0">
	<cfparam name="THIS.entityID" type="numeric" default="0">
	<!--- <cfset THIS.entityID = "0"> --->
	<cfparam name="THIS.opportunityView" type="string" default="application.com.settings.getSetting('leadManager.opportunityView')">  <!--- 2011/03/16 PPB default was "opportunityView" --->
	<cfparam name="THIS.detail" type="string" default = "">
	<cfparam name="THIS.budget" type="numeric" default = "0">
	<cfparam name="THIS.currency" type="string" default = "USD">
	<cfparam name="THIS.probability" type="numeric" default = "0">
	<cfparam name="THIS.stageID" type="numeric" default = "1">
	<cfparam name="THIS.currentSituation" type="string" default="">
	<cfparam name="THIS.expectedCloseDate" type="string" default="">
	<cfparam name="THIS.contactPersonID" type="numeric" default = "0">
	<cfparam name="THIS.sponsorPersonid" type="numeric" default = "0">
	<cfparam name="THIS.statusID" type="numeric" default = "0">
	<!--- START: 2009-01-05 AJC P-LEX039 Status Notes --->
	<cfparam name="THIS.statusNotes" type="string" default = "">
	<!--- END: 2009-01-05 AJC P-LEX039 Status Notes --->
	<cfparam name="THIS.countryID" type="numeric" default = "0"> <!--- NYB 2010-07-07 Lexmark LHID 3636 - changed default from 17 to 0 --->
	<cfparam name="THIS.sourceID" type="numeric" default = "0">
	<cfparam name="THIS.campaignID" type="numeric" default = "0"> <!--- NJH 2010/01/14 LID 2702 - added campaignID --->
	<cfparam name="THIS.distiLocationID" type="numeric" default = "0">
	<cfparam name="THIS.distiSalesPersonID" type="numeric" default = "0">
	<cfparam name="THIS.vendorSalesManagerPersonID" type="numeric" default = "0">
	<cfparam name="THIS.vendorAccountManagerPersonID" type="numeric" default = "0">
	<cfparam name="THIS.partnerLocationID" type="numeric" default = "0">
	<cfparam name="THIS.partnerSalesManagerPersonID" type="numeric" default = "0">
	<cfparam name="THIS.partnerSalesPersonID" type="numeric" default = "0">
	<cfparam name="THIS.createdby" type="numeric" default = "404">
	<cfparam name="THIS.created" type="date" default="#now()#">
	<cfparam name="THIS.lastupdatedby" type="numeric" default = "404">
	<cfparam name="THIS.lastupdated" type="date" default="#now()#">
	<cfparam name="THIS.OppPaymentTermID" type="numeric" default="0">
	<cfparam name="THIS.OppCustomerTypeID" type="numeric" default="0">
	<cfparam name="THIS.OrderID" type="numeric" default="0">
	<cfparam name="THIS.opptypeID" type="numeric" default="1">
	<!--- 2005/05/31 AJC START:P_SNY_011 VIP/11.1.6/Notes --->
	<cfparam name="THIS.Notes" type="string" default="">
	<!--- 2005/05/31 AJC START:P_SNY_011 VIP/11.1.6/Notes --->
	<cfparam name="THIS.oppReasonID" type="numeric" default="0">
	<cfparam name="THIS.actualCloseDate" type="numeric" default="0">
	<cfparam name="THIS.territoryID" type="numeric" default="0">
	<cfparam name="THIS.message" type="string" default="">
	<!--- these params can be overwritten using opportunityINI.cfm --->
	<!--- <cfparam name="vendorSalesManagerFlagTextIDs" default="isSalesManager"> - MOVED TO SETTINGS --->
	<!--- <cfparam name="vendorAcMngrFlagTextIDs" default="isAccountManager"> - MOVED TO SETTINGS --->
	<!--- <cfparam name="VendorAccountMngrCountryScoped" default="no">  - MOVED TO SETTINGS --->

	<!--- NJH 2009/09/28 P-CYM006 - param some custom fields --->
	<cfparam name="THIS.oppCustom1" type="string" default="">
	<cfparam name="THIS.oppCustom2" type="string" default="">
	<cfparam name="THIS.oppCustom3" type="string" default="">
	<cfparam name="THIS.oppCustom4" type="string" default="">

	<!--- 2011/03/15 PPB I added this because it is referred to below tho it wasn't set previously; probably the methods that use it are not called  --->
	<cfparam name="THIS.productCatalogueCampaignID" type="any" default="application.com.settings.getSetting('leadManager.products.productCatalogueCampaignID')">

	<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true">
	<!--- <cfif fileExists("#application. userFilesAbsolutePath#/code/cftemplates/opportunityINI.cfm")> <!--- WAB 2007-02-19 added so that site without opportunity.ini will load.  In reality this file probably shouldn't be loaded in application.com --->
		<cfinclude template="/code/cftemplates/opportunityINI.cfm">
	</cfif> --->

	<!--- if the user is External we assume they are a partner and allocate the lead to them --->
	<cfif isDefined("request.relayCurrentUser.personid") and isNumeric(request.relayCurrentUser.personid) and isDefined("request.relaycurrentuser") and not request.relaycurrentuser.isInternal> <!--- WAB 2006-01-25 Remove form.userType CompareNoCase(usertype,'external') eq 0 --->
		<cfquery name="getThisPersonsDetails" datasource="#application.siteDataSource#">
		select personid, firstname,lastName,organisationID,locationID
		from person where personid = #request.relayCurrentUser.personid#
	</cfquery>
	<cfset THIS.partnerLocationID = getThisPersonsDetails.locationID>
	<cfset THIS.partnerSalesPersonID = getThisPersonsDetails.personid>
</cfif>


<cffunction name="initialise">
	<cfargument name = "applicationScope" default="#application#">

	<cfif isDefined("applicationScope.opportunitySettings") is false>
		<cfscript>
			setOpportunitySettingDefaults(applicationScope = applicationScope);
		</cfscript>
	</cfif>
</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getThisEntityDetails"
	hint="Returns various details for this entity">
	<cfquery name="getThisEntityDetails" datasource="#application.siteDataSource#">
		select organisationID,countryID
		from organisation WHERE organisationID =  <cf_queryparam value="#THIS.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset THIS.countryid = getThisEntityDetails.countryID>
	<cfset THIS.entityID = getThisEntityDetails.organisationID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default Insert Query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="create" hint="Inserts a leadAndOpp ">
<cftransaction>
	<!--- set the current probability based what the stageID is --->
	<cfquery name="getCurrentProbability" datasource="#application.siteDataSource#">
		select probability from oppStage where opportunityStageID =  <cf_queryparam value="#THIS.stageID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset THIS.probability = getCurrentProbability.probability>

	<!--- NJH 2009/09/28 P-CYM006 - insert 4 custom fields into the opportunity table --->
	<cfquery name="insertopportunity" datasource="#application.siteDataSource#">
		INSERT INTO opportunity (
		[entityID],
		[detail],
		[budget],
		[currency],
		[probability],
		[stageID],
		[currentSituation],
		[expectedCloseDate],
		[contactPersonID],
		[sponsorPersonid],
		[statusID],
			<!--- START: 2009-01-05 AJC P-LEX039 Status Notes --->
			[statusNotes],
			<!--- END: 2009-01-05 AJC P-LEX039 Status Notes --->
		[countryID],
		[sourceID],
		[campaignID], <!--- NJH 2010/01/14 LID 2702 - added campaignID --->
		[vendorSalesManagerPersonID],
		[vendorAccountManagerPersonID],
		[partnerLocationID],
		[distiLocationID],
		[distiSalesPersonID],
		[partnerSalesManagerPersonID],
		[partnerSalesPersonID],
		[createdby],
		[created],
		[lastupdatedby],
		[lastupdated],
		[oppReasonID],
		[actualCloseDate],
		[territoryID],
		[OppPaymentTermID],
		[OppCustomerTypeID],
		[OrderID],
		[OppTypeID],
		[oppCustom1],
		[oppCustom2],
		[oppCustom3],
		[oppCustom4]
		)
		VALUES
			(
			<cfqueryparam value="#THIS.entityID#" cfsqltype="CF_SQL_INTEGER" >,
			<cfqueryparam value="#THIS.detail#" cfsqltype="CF_SQL_VARCHAR" maxlength="100">,
			<cfqueryparam value="#THIS.budget#" cfsqltype="CF_SQL_NUMERIC" scale="2">,
			<cfqueryparam value="#THIS.currency#" cfsqltype="CF_SQL_VARCHAR" maxlength="3">,
			<cfqueryparam value="#THIS.probability#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.stageID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.currentSituation#" cfsqltype="CF_SQL_VARCHAR" maxlength="2000">,
			<cfqueryparam value="#THIS.expectedCloseDate#" cfsqltype="CF_SQL_DATE">,
			<cfqueryparam value="#THIS.contactPersonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.sponsorPersonid#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.statusID#" cfsqltype="CF_SQL_INTEGER">,
				<!--- START: 2009-01-05 AJC P-LEX039 Status Notes --->
				<cfqueryparam value="#THIS.statusNotes#" cfsqltype="CF_SQL_VARCHAR" maxlength="1000">,
				<!--- END: 2009-01-05 AJC P-LEX039 Status Notes --->
			<cfqueryparam value="#THIS.countryID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.sourceID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.campaignID#" cfsqltype="CF_SQL_INTEGER">, <!--- NJH 2010/01/14 LID 2702 - added campaignID --->
			<cfqueryparam value="#THIS.vendorSalesManagerPersonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#val(THIS.vendorAccountManagerPersonID)#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.partnerLocationID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.distiLocationID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.distiSalesPersonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.partnerSalesManagerPersonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.partnerSalesPersonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.createdby#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.created#" cfsqltype="CF_SQL_TIMESTAMP">,
			<cfqueryparam value="#THIS.lastupdatedby#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.lastupdated#" cfsqltype="CF_SQL_TIMESTAMP">,
			<cfqueryparam value="#THIS.oppReasonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.actualCloseDate#" cfsqltype="CF_SQL_DATE">,
			<cfqueryparam value="#THIS.territoryID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.OppPaymentTermID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.OppCustomerTypeID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#THIS.OrderID#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#val(THIS.OppTypeID)#" cfsqltype="CF_SQL_INTEGER">,
			<cfqueryparam value="#this.oppCustom1#" cfsqltype="CF_SQL_VARCHAR">,
			<cfqueryparam value="#this.oppCustom2#" cfsqltype="CF_SQL_VARCHAR">,
			<cfqueryparam value="#this.oppCustom3#" cfsqltype="CF_SQL_VARCHAR">,
			<cfqueryparam value="#this.oppCustom4#" cfsqltype="CF_SQL_VARCHAR">
			)
		</cfquery>

		<cfquery name="getNewID"  datasource="#application.siteDataSource#">
			SELECT max(opportunityID) as NewID from opportunity
		</cfquery>
	</cftransaction>
	<cfset THIS.opportunityID = getNewID.NewID>
	<!--- START 2011-11-28 NYB LHID8224 - changed to translation --->
	<cfset form.opportunityID = THIS.opportunityID>
	<cfset THIS.message = application.com.relayTranslations.translatePhrase(phrase="opp_RecordIDxxCreated")>
	<!--- END 2011-11-28 NYB LHID8224 - changed to translation --->
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default Update Query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="update" hint="Updates the DB record for THIS.opportunityID">
	<!--- set the current probability based what the stageID is --->
	<cfquery name="getCurrentProbability" datasource="#application.siteDataSource#">
		select probability from oppStage where opportunityStageID =  <cf_queryparam value="#THIS.stageID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset THIS.probability = getCurrentProbability.probability>

	<!--- NYB 2008-11-18 Demo Bug - added ListLast() around a number of entries --->
	<!--- NJH 2009/09/28 P-CYM006 - added update on four custom fields --->
	<cfquery name="updateopportunity" datasource="#application.siteDataSource#">
		UPDATE opportunity SET
			[detail] = <cfqueryparam value="#THIS.detail#" cfsqltype="CF_SQL_VARCHAR" maxlength="100">,
			[budget] = <cfqueryparam value="#THIS.budget#" cfsqltype="CF_SQL_NUMERIC" scale="2">,
			[currency] = <cfqueryparam value="#THIS.currency#" cfsqltype="CF_SQL_VARCHAR" maxlength="3">,
			[probability] = <cfqueryparam value="#THIS.probability#" cfsqltype="CF_SQL_INTEGER">,
			[stageID] = <cfqueryparam value="#THIS.stageID#" cfsqltype="CF_SQL_INTEGER">,
			<!--- NYF 2008-06-18 NABU bug 451 - updated maxlength from 500 to 1000 --->
			[currentSituation] = <cfqueryparam value="#THIS.currentSituation#" cfsqltype="CF_SQL_VARCHAR" maxlength="1000">,
			[expectedCloseDate] = <cfqueryparam value="#THIS.expectedCloseDate#" cfsqltype="CF_SQL_DATE">,
			[contactPersonID] = <cfqueryparam value="#THIS.contactPersonID#" cfsqltype="CF_SQL_INTEGER">,
			[sponsorPersonid] = <cfqueryparam value="#THIS.sponsorPersonid#" cfsqltype="CF_SQL_INTEGER">,
			[statusID] = <cfqueryparam value="#THIS.statusID#" cfsqltype="CF_SQL_INTEGER">,
				<!--- START: 2009-01-05 AJC P-LEX039 Status Notes --->
				[statusNotes] = <cfqueryparam value="#THIS.statusNotes#" cfsqltype="CF_SQL_VARCHAR" maxlength="1000">,
				<!--- END: 2009-01-05 AJC P-LEX039 Status Notes --->
			[countryID] = <cfqueryparam value="#THIS.countryID#" cfsqltype="CF_SQL_INTEGER">,
			[sourceID] = <cfqueryparam value="#THIS.sourceID#" cfsqltype="CF_SQL_INTEGER">,
			[campaignID] = <cfqueryparam value="#THIS.campaignID#" cfsqltype="CF_SQL_INTEGER">, <!--- NJH 2010/01/14 LID 2702 - added campaignID --->
			[vendorSalesManagerPersonID] = <cfqueryparam value="#ListLast(THIS.vendorSalesManagerPersonID)#" cfsqltype="CF_SQL_INTEGER">,
			[vendorAccountManagerPersonID] = <cfqueryparam value="#val(THIS.vendorAccountManagerPersonID)#" cfsqltype="CF_SQL_INTEGER">,
			[partnerLocationID] = <cfqueryparam value="#ListLast(THIS.partnerLocationID)#" cfsqltype="CF_SQL_INTEGER">,
			[distiLocationID] = <cfqueryparam value="#ListLast(THIS.distiLocationID)#" cfsqltype="CF_SQL_INTEGER">,
			[distiSalesPersonID] = <cfqueryparam value="#ListLast(THIS.distiSalesPersonID)#" cfsqltype="CF_SQL_INTEGER">,
			[partnerSalesManagerPersonID] = <cfqueryparam value="#ListLast(THIS.partnerSalesManagerPersonID)#" cfsqltype="CF_SQL_INTEGER">,
			[partnerSalesPersonID] = <cfqueryparam value="#THIS.partnerSalesPersonID#" cfsqltype="CF_SQL_INTEGER">,
			[lastupdatedby] = <cfqueryparam value="#THIS.lastupdatedby#" cfsqltype="CF_SQL_INTEGER">,
			[lastupdated] = <cfqueryparam value="#THIS.lastupdated#" cfsqltype="CF_SQL_TIMESTAMP">, <!--- NJH 2007/11/09 was CF_SQL_DATE--->
			[oppReasonID] = <cfqueryparam value="#THIS.oppReasonID#" cfsqltype="CF_SQL_INTEGER">,
			[actualCloseDate] = <cfqueryparam value="#THIS.actualCloseDate#" cfsqltype="CF_SQL_DATE">,
			[territoryID] = <cfqueryparam value="#THIS.territoryID#" cfsqltype="CF_SQL_INTEGER">,
			[OppPaymentTermID] = <cfqueryparam value="#THIS.OppPaymentTermID#" cfsqltype="CF_SQL_INTEGER">,
			[OppCustomerTypeID] = <cfqueryparam value="#ListLast(THIS.OppCustomerTypeID)#" cfsqltype="CF_SQL_INTEGER">,
			[oppCustom1] = <cfqueryparam value="#THIS.oppCustom1#" cfsqltype="CF_SQL_VARCHAR">,
			[oppCustom2] = <cfqueryparam value="#THIS.oppCustom2#" cfsqltype="CF_SQL_VARCHAR">,
			[oppCustom3] = <cfqueryparam value="#THIS.oppCustom3#" cfsqltype="CF_SQL_VARCHAR">,
			[oppCustom4] = <cfqueryparam value="#THIS.oppCustom4#" cfsqltype="CF_SQL_VARCHAR">
	WHERE opportunityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<!--- START 2011-11-28 NYB LHID8224 - changed to translation --->
	<cfset form.opportunityID = THIS.opportunityID>
	<cfset THIS.message = application.com.relayTranslations.translatePhrase(phrase="opp_RecordIDxxUpdated")>
	<!--- END 2011-11-28 NYB LHID8224 - changed to translation --->

	<!--- 2008/01/22 GCC CR-ATI503 if deal is complete and has a special pricing status set the SPQ to complete --->
	<!--- NJH 2009/08/27 P-ATI005 - added level 4&5 cc emails --->
	<cfset specialPricingStatusMethod = GetEquivalentSpecialPricingStatus(stageID=THIS.stageID)>
	<cfset OppDetails = getOpportunityDetails(opportunityID=this.opportunityID)>
	<cfif isDefined('specialPricingStatusMethod') and specialPricingStatusMethod eq "SP_Complete" and OppDetails.oppPricingStatusID neq 1>
		<cfset x = application.com.oppSpecialPricing.progressSpecialPricing(
					dataSource=application.sitedatasource,
					opportunityID=this.opportunityID,
					specialPricingStatusMethod=specialPricingStatusMethod,
					personid=request.relaycurrentuser.personid,
					comments="Special pricing completed by project closure",
					SPApproverLevel = -1,
					SPApproverLevelRequired =OppDetails.SPRequiredApprovalLevel,
					SPQCountryCCEmailList="",
					SPQLevel2CCEmailList="",
					SPQLevel3CCEmailList="",
					SPQLevel4CCEmailList="",
					SPQLevel5CCEmailList="",
					SPQViewCCEmailList=""
					)>
		<!--- email the account Manager is part of progressSpecialPricing --->
		<!--- <cfset y = application.com.oppSpecialPricing.SP_Email(dataSource=application.sitedatasource,senderPersonID=request.relaycurrentuser.personid,ccPersonIDs="",specialPricingStatusMethod=specialPricingStatusMethod,opportunityID=this.opportunityID,comments="",SPApproverLevel=0,SPQCountryCCEmailList="",SPQLevel2CCEmailList="",SPQLevel3CCEmailList="",SPQViewCCEmailList="")> --->

		<!--- START 2011-11-28 NYB LHID8224 - changed to translation --->
		<cfset form.opportunityID = THIS.opportunityID>
		<cfset THIS.message = application.com.relayTranslations.translatePhrase(phrase="opp_RecordIDxxUpdatedAndMarkedComplete")>
		<!--- END 2011-11-28 NYB LHID8224 - changed to translation --->

	</cfif>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default SELECT Query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="get" hint="Retrieves an existing record from the opportunity table and sets it to the THIS scope.">
	<cfargument name="opportunityID" type="numeric" required="false" hint="used to return query for a Opportunity directly without the THIS scope">

	<cfquery name="getopportunity" datasource="#application.siteDataSource#">
		SELECT * FROM opportunity
		WHERE opportunityID =
		<cfif isdefined('arguments.opportunityID')>
			<cfqueryparam value="#val(arguments.opportunityID)#" cfsqltype="cf_sql_integer">
		<cfelse>
			#THIS.opportunityID#
		</cfif>
	</cfquery>

	<cfif isdefined('arguments.opportunityID')>
		<cfreturn getopportunity/>
	<cfelse>

	<cfif getopportunity.recordCount gt 0>

		<cfset THIS.EntityID = getopportunity.EntityID>
		<cfset THIS.opportunityID = getopportunity.opportunityID>
		<cfset THIS.detail = getopportunity.detail>
		<cfset THIS.budget = getopportunity.budget>
		<cfset THIS.currency = getopportunity.currency>
		<cfset THIS.probability = getopportunity.probability>
		<cfset THIS.stageID = getopportunity.stageID>
		<cfset THIS.currentSituation = getopportunity.currentSituation>
		<cfset THIS.expectedCloseDate = getopportunity.expectedCloseDate>
		<cfset THIS.contactPersonID = getopportunity.contactPersonID>
		<cfset THIS.sponsorPersonid = getopportunity.sponsorPersonid>
		<cfset THIS.statusID = getopportunity.statusID>
			<!--- START: 2009-01-05 AJC P-LEX039 Status Notes --->
			<cfset THIS.statusNotes = getopportunity.statusNotes>
			<!--- END: 2009-01-05 AJC P-LEX039 Status Notes --->
		<cfset THIS.countryID = getopportunity.countryID>
		<cfset THIS.sourceID = getopportunity.sourceID>
		<cfset THIS.campaignID = getopportunity.campaignID> <!--- NJH 2010/01/14 LID 2702 - added campaignID --->
		<cfset THIS.vendorSalesManagerPersonID = getopportunity.vendorSalesManagerPersonID>
		<cfset THIS.vendorAccountManagerPersonID = getopportunity.vendorAccountManagerPersonID>
		<cfset THIS.partnerLocationID = getopportunity.partnerLocationID>
		<cfset THIS.distiLocationID = getopportunity.distiLocationID>
		<cfif isdefined('getopportunity.distiSalesPersonID')>
		<cfset THIS.distiSalesPersonID = getopportunity.distiSalesPersonID>
		</cfif>
		<cfset THIS.partnerSalesManagerPersonID = getopportunity.partnerSalesManagerPersonID>
		<cfset THIS.partnerSalesPersonID = getopportunity.partnerSalesPersonID>
		<cfset THIS.createdby = getopportunity.createdby>
		<cfset THIS.created = getopportunity.created>
		<cfset THIS.lastupdatedby = getopportunity.lastupdatedby>
		<cfset THIS.lastupdated = getopportunity.lastupdated>
		<cfset THIS.oppReasonID = getopportunity.oppReasonID>
		<cfset THIS.actualCloseDate = getopportunity.actualCloseDate>
		<cfset THIS.productGroupID = getopportunity.productGroupID>
		<cfset THIS.OppPaymentTermID = getopportunity.OppPaymentTermID>
		<cfset THIS.OppCustomerTypeID = getopportunity.OppCustomerTypeID>
		<cfset THIS.OppTypeID = getopportunity.OppTypeID>
		<!--- 2009/04/27 GCC LID:2137 get SPApproverLevelRequired from DB--->
		<cfset THIS.SPApproverLevelRequired = getopportunity.sprequiredapprovallevel>
		<!--- NJH 2009/09/28 P-CYM006 set 4 new custom fields --->
		<cfset this.oppCustom1 = getopportunity.oppCustom1>
		<cfset this.oppCustom2 = getopportunity.oppCustom2>
		<cfset this.oppCustom3 = getopportunity.oppCustom3>
		<cfset this.oppCustom4 = getopportunity.oppCustom4>
	</cfif>

	</cfif>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default SELECT Query- Part 2 This time returns the opportunity.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getOpportunityDetails" hint="Retrieves an existing record from the opportunity table.">
	<cfargument name="opportunityID" required="No" default="">
	<cfargument name="orderID" required="No" default="">
	<cfset var getopportunityQry = "">

	<!--- NJH PROD2015-34 2015/09/22 - include a screen in the opp display - select from view so that we have flags available as well --->
	<cfquery name="getopportunityQry" datasource="#application.sitedatasource#">
		SELECT * FROM vOpportunity
		<cfif len(arguments.opportunityID)>
			WHERE opportunityID =  <cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelseif len(arguments.orderID)>
			WHERE orderID =  <cf_queryparam value="#arguments.orderID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</cfquery>
	<cfreturn getopportunityQry>
</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default SELECT Query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="listOpportunitiesInternal" hint="Retrieves an existing record from the opportunity table and sets it to the THIS scope.">
		<!--- This can be overwritten by a cfc in the Userfiles/code/cftemplates dir --->
	<cfparam name="THIS.FILTERSELECTVALUE" default="">
	<cfparam name="THIS.FilterSelect" default="#THIS.FILTERSELECTVALUE#">
	<cfparam name="THIS.FILTERSELECTVALUE2" default="">
	<cfparam name="THIS.FilterSelect2" default="#THIS.FILTERSELECTVALUE2#">
	<cfparam name="THIS.radioFilterValue" default="">
	<cfparam name="THIS.radioFilterName" default="#THIS.radioFilterValue#">
	<!--- <cfparam name="THIS.liveStatusList" default="Lead,opportunity"> 	- NO LONGER USED --->
	<cfparam name="THIS.showComplete" default="0">
	<cfparam name="THIS.alphabeticalIndexColumn" default="">
	<cfparam name="THIS.alphabeticalIndex" default="">
	<cfparam name="THIS.ShowTheseOppStageIDs" default="">

	<cfset oppTypeSnippet = application.com.relayTranslations.getPhrasePointerQuerySnippetsForTextID(tableName=THIS.opportunityView,textIDColumn="oppTypeTextID")>
	<cfset oppStatusSnippet = application.com.relayTranslations.getPhrasePointerQuerySnippetsForTextID(tableName=THIS.opportunityView,textIDColumn="oppStatusTextID")>

	<cfquery name="listOpportunitiesInternal" datasource="#application.siteDataSource#">
		SELECT * from
			(select opportunity_ID,
				Account,
				Stage,
				detail,
				calculated_budget,
				distributor,
				expectedCloseDate as expected_Close_Date,
				base_budget,
				overall_customer_budget,
				last_Updated,
				Account_Manager,
				status,
				probability,
				month_expected_close,
				Quarter_Expected_Close,
				country,
				Partner_Name,
				#THIS.opportunityView#.entityid,
					<!--- AWJR 2011/03/30 P-LEX051 Added necessary fields for the LexmarkLeadAndOppStatusList report. --->
					(select OrganisationName from Organisation where organisationid = #THIS.opportunityView#.entityid) as enduser_account,
					(SELECT     SUM([quantity]) AS Expr1
	                 FROM          dbo.opportunityProduct
	                            WHERE      (OpportunityID = #THIS.opportunityView#.Opportunity_ID)) as totalProductQuantity,
				vendorAccountManagerPersonID,
				ISOCode,
				region,
				SPRequiredApprovalLevel,
				SPCurrentApprovalLevel,
				pricing_status,
				oppPricingStatusID,
				SPapprovalNumber,
				SPExpiryDate,
				LEFT( UPPER( #THIS.alphabeticalIndexColumn# ), 1 ) as alphabeticalIndex,
				<!--- NJH 2006/10/06 --->
				rebate,
				oppTypeID,
				currency, <!--- NJH 2010/04/23 P-PAN002 --->
				forecast, <!--- YMA 2013/01/06 2013 Roadmap - 108 - Forecast Opportunity --->
				/* P-PSI001 - RMB - 2013-06-21 - Added 'OppStatus' AND 'OppType' AND 'partner_Sales_Person' */
				#oppStatusSnippet.select# as OppStatus,
				#oppTypeSnippet.select# as OppType,
				created,
				partner_Sales_Person
				FROM #THIS.opportunityView#
					#preserveSingleQuotes(oppStatusSnippet.join)#
					#preserveSingleQuotes(oppTypeSnippet.join)#
			WHERE 1=1
			<cfif THIS.alphabeticalIndex neq "">
				AND LEFT( UPPER( #THIS.alphabeticalIndexColumn# ), 1 ) =  <cf_queryparam value="#THIS.alphabeticalIndex#" CFSQLTYPE="CF_SQL_VARCHAR" >   <!--- LID 4618 - added N to deal with nvarchar --->
			</cfif>
			<!--- START LID 3761 NAS 2010-07-27  --->
			<!--- <cfif isDefined("THIS.countryIDList") and THIS.countryIDList neq "0">
			and countryID in (#THIS.countryIDList#)
			</cfif> --->
			<!--- END LID 3761 NAS 2010-07-27  --->
			<!--- 2012-07-18 PPB P-SMA001 note: setting checked inside getRightsFilterWhereClause()
			<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
				and countryID in (#request.relayCurrentUser.countryList#)
			</cfif>
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="#THIS.opportunityView#").whereClause#	<!--- 2012-07-18 PPB P-SMA001 --->
			<cfif THIS.entityID neq "0">
				and (#THIS.opportunityView#.entityID =  <cf_queryparam value="#THIS.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				or partnerLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#THIS.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				or distiLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#THIS.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
			</cfif>
			<cfif THIS.vendorAccountManagerPersonID neq "0">
				and (vendorAccountManagerPersonID =  <cf_queryparam value="#THIS.vendorAccountManagerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
				or opportunity_id in (SELECT recordid FROM recordRights
					WHERE UserGroupID = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" cfsqltype="cf_sql_integer">
					AND Entity = 'opportunity')
				)
			</cfif>
			<cfif THIS.ShowTheseOppStageIDs neq "">
				AND StageID  IN ( <cf_queryparam value="#THIS.ShowTheseOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>

			<cfif THIS.showComplete eq 0>
				<!--- 2006-02-19 SWJ changed from neq as it was the wrong way around
				as this.showComplete's default state is i.e. show me the live leads or liveStatus=1 --->
				AND liveStatus = 1
			<cfelse>
				AND liveStatus = 0
			</cfif>

			<!--- check that it is an SPQ query --->
			<CFIF ISDEFINED("THIS.SPQReport") AND THIS.SPQREPORT EQ "yes">
				<CFIF ISDEFINED("THIS.showSPComplete") AND THIS.SHOWSPCOMPLETE NEQ 0>
					AND oppPricingStatusID = 9
				<CFELSE>
					AND oppPricingStatusID <> 9
				</CFIF>
			</CFIF>

			<cfif isDefined("THIS.showUnalocated") and THIS.showUnalocated eq 1>
				AND (partnerLocationID is NULL or partnerLocationID = 0)
			</cfif>
		<!---
		NJH 2015/11/23 - PROD2015-442 - removed as this didn't ever appear to be working
		<cfif isDefined("THIS.FRMWHERECLAUSE1") and THIS.FRMWHERECLAUSE1 neq "">
			AND opportunity_ID in (Select distinct opportunityID from oppProducts
				where rtrim(datename(q, forecastShipDate)) =  <cf_queryparam value="#right(listfirst(THIS.FRMWHERECLAUSE1,"-"),1)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				and datepart(yy, forecastShipDate) =  <cf_queryparam value="#listLast(THIS.FRMWHERECLAUSE1,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
		</cfif>
		<cfif isDefined("THIS.FRMWHERECLAUSE2") and THIS.FRMWHERECLAUSE2 neq "">
			AND opportunity_ID in (Select distinct opportunityID from oppProducts
				where rtrim(datename(m, forecastShipDate)) =  <cf_queryparam value="#listfirst(THIS.FRMWHERECLAUSE2,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				and datepart(yy, forecastShipDate) =  <cf_queryparam value="#listLast(THIS.FRMWHERECLAUSE2,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
		</cfif> --->

			<cfif isDefined("THIS.SPQReport") and THIS.SPQReport eq "yes">
				AND OppPricingStatusID > 1
			</cfif>

			<!--- NJH 2006/10/06
				Jira Fifteen-221 2015/03/04 - check that oppType is not 0--->
			<cfif structKeyExists(this,"oppTypeID") and this.oppTypeID neq 0>
				<cfif listLen(this.oppTypeID) gt 1>
					and oppTypeID  in ( <cf_queryparam value="#this.oppTypeID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<cfelse>
					and oppTypeID =  <cf_queryparam value="#this.oppTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
			</cfif>
		) as internalOpportunities
		where 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">

		<cfif isDefined("THIS.sortOrder")>
			order by #THIS.sortOrder#
		</cfif>
	</cfquery><cfset THIS.qListOpportunitiesInternal = listOpportunitiesInternal>
<!--- 		<cfif listOpportunities.recordCount gt 0>
			<cfset THIS.qListOpportunities = listOpportunities>
		</cfif>
 --->	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Default SELECT Query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="listPartnerOpportunities" hint="Retrieves an existing record from the opportunity table and sets it to the THIS scope." output="true">
	<cfargument name="opportunityView" required="no" default="#THIS.opportunityView#">
	<cfargument name="sortOrder" required="no" default="#THIS.sortOrder#">
	<cfargument name="partnerSalesPersonID" required="no" default="#THIS.partnerSalesPersonID#" type="numeric">
	<cfargument name="showCurrentMonth" required="no" default="false">
	<cfargument name="filterType" required="no" default="">
	<cfargument name="filterToForecast" required="no" default="false">

	<cfset hideOppStagesFromOppList = application.com.settings.getSetting("leadManager.hideOppStagesFromOppList")>

	<!--- LHID8175 NYB 2011-12-09 added StagePhraseTextID to query --->
	<cfquery name="listPartnerOpportunities" datasource="#application.siteDataSource#">
		select * from (
			SELECT opportunity_ID,Account,Stage,stageID,detail,StagePhraseTextID,
				expectedCloseDate as Expected_Close_Date,
				overall_customer_budget, -- Changed this from budget MDC 190304 as not showing leads on partner site
				calculated_budget, -- SWJ 2006/10/06
				last_Updated,Account_Manager,entityid,opptypeID,currency,
				(SELECT status FROM oppStatus WHERE statusId=status_ID) AS StatusDescription,	<!--- PPB 2010-06-29 so can be shown on My Opportunities --->
				(SELECT 'phr_Opp_' + ot1.oppTypeTextID FROM OppType ot1 WHERE ot1.opptypeID = #arguments.opportunityView#.oppTypeID) AS oppTypeText,<!--- RMB 2012/06/10 so can be shown on My Opportunities --->
				vendorAccountManagerPersonID,partnerSalesPersonID,
				'phr_Opp_SP_Order' as CREATE_ORDER, <!--- NJH 2006/08/14 used for 'My Leads' relay tag--->
				'phr_Opp_SP_request' as PRICING_SUPPORT, <!--- NJH 2006/08/14 used for 'My Leads' relay tag--->
				rebate, <!--- NJH 2006/10/06 used for 'My Leads' relay tag--->
				month(expectedCloseDate) as oppCloseMonth, <!--- for sales tasks SKP --->
				year(expectedCloseDate) as oppCloseYear,
				/* P-PSI001 - RMB - 2013-06-21 - Added 'OppStatus' AND 'OppType' AND 'partner_Sales_Person' */
				OppStatus,
				OppType,
				partner_Sales_Person
	        FROM #arguments.opportunityView#
			WHERE 1=1
			<cfif this.partnerLocationID neq "0">
				<!--- 2012-09-19 PPB Case 430388 START : filter to ALL locations of the partners organisation --->
				<!--- and partnerLocationID = #this.partnerLocationID# --->
				and partnerLocationID IN (SELECT locationid FROM location WHERE organisationid IN (SELECT organisationid FROM location WHERE locationid = <cf_queryparam value="#this.partnerLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >) )
				<!--- 2012-09-19 PPB Case 430388 END --->
			</cfif>
			<cfif arguments.partnerSalesPersonID neq "0">
				and partnerSalesPersonID = #arguments.partnerSalesPersonID#
			</cfif>
			<cfif isDefined("THIS.showOppTypes") and THIS.showOppTypes neq "">
				and oppTypeID IN (SELECT oppTypeID FROM oppType WHERE oppTypeTextID  IN ( <cf_queryparam value="#THIS.showOppTypes#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
			</cfif>
			<cfif hideOppStagesFromOppList neq "">
				and stageID NOT IN (SELECT OpportunityStageID FROM oppStage WHERE StageTextID  IN ( <cf_queryparam value="#hideOppStagesFromOppList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
			</cfif>
			<cfif showCurrentMonth eq "true">
				AND Month(expectedCloseDate) =  <cf_queryparam value="#month(now())#" CFSQLTYPE="CF_SQL_INTEGER" >
				AND Year(expectedCloseDate) = <cf_queryparam value="#year(now())#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif filterType eq "PIPELINE">
				AND opptypeID =  <cf_queryparam value="#application.com.opportunity.GetOpptypes('DEALS').OpptypeId#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfif isDefined("pipelineBoundaryPercentage")>
					AND OppStageProbability <=  <cf_queryparam value="#pipelineBoundaryPercentage#" CFSQLTYPE="CF_SQL_Integer" >
				</cfif>
			<cfelseif filterType eq "FORECAST">
				<cfif isDefined("pipelineBoundaryPercentage")>
					AND OppStageProbability >  <cf_queryparam value="#pipelineBoundaryPercentage#" CFSQLTYPE="CF_SQL_Integer" >
				</cfif>
			</cfif>
			<cfif arguments.filterToForecast>
				and forecast = 1
			</cfif>
		) as base where 1=1
		<!--- NJH PROD2015-35 - added TFQO filtering--->
		<cfset var useWholeWordsOnly = false>
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">

		<cfif structKeyExists(arguments,"sortOrder")>
			order by #arguments.sortOrder#
		</cfif>
	</cfquery>
	<cfset THIS.qListPartnerOpportunities = listPartnerOpportunities>
<!--- 		<cfif listOpportunities.recordCount gt 0>
			<cfset THIS.qListOpportunities = listOpportunities>
		</cfif>
 --->
	<cfreturn listPartnerOpportunities>
</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for entityID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getEntityID" hint="Retrieves foreign key data for EntityID THIS.qEntityID to the query object.">
	<cfquery name="getEntityID" datasource="#application.siteDataSource#">
		SELECT LEFT(organisationname, 100) as entityName
		FROM organisation
		where organisationID =  <cf_queryparam value="#THIS.EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
<cfset THIS.qEntityID = getEntityID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for stageID
			2011/09/21 MS LID 7699 Added Translation on OpportunityStage
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getStageID" hint="Retrieves foreign key data for stageID THIS.qstageID to the query object." validvalues="true">
	<cfargument name="StageID" required="no" type="string" hint="provide this to get  a status">
	<cfargument name="showSystemStages" required="no" type="boolean" default="false" hint="Show stages that are system stages"> <!--- NJH 2010/04/21 P-PAN002 --->
	<cfargument name="stageTextID" required="no" type="string" hint="provide this to get  a status">
	<cfargument name="liveStatus" required="no" default="false">

	<cfquery name="qryStage" datasource="#application.siteDataSource#">
		SELECT opportunityStageID as ID, opportunityStage as ItemText, defaultValue, status, probability, stageTextID,
		'OppStage_'+stageTextID as PhraseTextID  <!--- NYB 2011-11-28 LHID8224 added --->
		,'phr_OppStage_'+stageTextID as PrefixedPhraseTextID	<!--- 2012/05/21 PPB P-LEX070 allows user to put display="PrefixedPhraseTextID" in the opportunityDisplay.xml for stageId; I didn't want to replace the PhraseTextId just in case its used --->
		FROM OppStage
		WHERE 1=1

		<cfif isDefined("THIS.stageIDfilter")>
		AND opportunityStageID =  <cf_queryparam value="#val(THIS.stageIDfilter)#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		<cfif structKeyExists(arguments,"StageID")>
			<cfset var thisStageID = arguments.stageID>

			<cfif not isNumeric(thisStageID)>
				<cfset thisStageID = 0>
			</cfif>
		AND opportunityStageID = <cfqueryparam value="#val(thisStageID)#" cfsqltype="cf_sql_integer" >
		</cfif>

		<!--- NJH 2010/04/21 P-PAN002 --->
		<cfif structKeyExists(arguments,"StageTextID")>
		AND stageTextID =  <cf_queryparam value="#arguments.stageTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		<cfif not arguments.showSystemStages>
		AND isSystem <> 1
		</cfif>
		<cfif arguments.liveStatus>
		AND liveStatus = 1
		</cfif>

		ORDER BY sortOrder
	</cfquery>
	<cfset THIS.qstageID = qryStage>
	<cfreturn qryStage/>

</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for contactPersonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getcontactPersonID" hint="Retrieves foreign key data for contactPersonID THIS.qcontactPersonID to the query object.">

	<!--- P-FNL079 2009/10/28 - added sortIndex --->
	<cfquery name="getcontactPersonID" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_SelectMainContact' as itemText, 1 as sortIndex
		UNION
		SELECT personID as ID, FirstName + ' ' + LastName  as ItemText, 2 as sortIndex
		FROM Person WHERE OrganisationID =  <cf_queryparam value="#THIS.EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY sortIndex,itemText
	</cfquery>
<cfset THIS.qcontactPersonID = getcontactPersonID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for sponsorPersonid
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getsponsorPersonid" hint="Retrieves foreign key data for sponsorPersonid THIS.qsponsorPersonid to the query object.">

	<!--- P-FNL079 2009/10/28 - added sortIndex --->
	<cfquery name="getsponsorPersonid" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_PleaseSpecifySponsor' as itemText, 1 as sortIndex
		UNION
		SELECT personID as ID, FirstName + ' ' + LastName  as ItemText, 2 as sortIndex
		FROM Person WHERE OrganisationID =  <cf_queryparam value="#THIS.EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY sortIndex, itemText
	</cfquery>
<cfset THIS.qsponsorPersonid = getsponsorPersonid>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for statusID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getStatusID" hint="Retrieves foreign key data for statusID THIS.qstatusID to the query object." returntype="query">
	<cfargument name="statusID" required="no" type="string" hint="provide this to get  a status" > <!--- NJH 2014/05/13 type changed to string to allow empty strings through --->

	<cfquery name="qryStatus" datasource="#application.siteDataSource#">
		SELECT statusID as ID, Status as ItemText, statusTextID,
		'OppStatus_'+statusTextID as PhraseTextID <!--- NYB 2011/11/30 LHID8245 added --->
		FROM OppStatus
		where 1=1

		<cfif structKeyExists(arguments,"statusID")>
			<cfset var oppStatusID = arguments.statusID>
			<cfif not isNumeric(oppStatusID)>
				<cfset oppStatusID = 0>
			</cfif>
		AND statusID = <cfqueryparam value="#oppStatusID#" cfsqltype="cf_sql_integer" >
		</cfif>

		ORDER BY statusID
	</cfquery>
	<cfset THIS.qstatusID = qryStatus>
	<cfreturn qryStatus/>

</cffunction>

<cffunction access="public" name="getOppPricingStatusID" hint="Retrieves foreign key data for oppPricingStatus to the query object." returntype="query">
	<cfargument name="oppPricingStatusID" required="no" type="numeric" hint="provide this to get  a oppPricingStatus" >

	<cfset var qryOppPricingStatus = "">

	<cfquery name="qryOppPricingStatus" datasource="#application.siteDataSource#">
		SELECT oppPricingStatusID as ID, oppPricingStatus, statusTextID
		FROM OppPricingStatus
		where 1=1
		<cfif structKeyExists(arguments,"oppPricingStatusID")>
		AND oppPricingStatusID = <cfqueryparam value="#arguments.oppPricingStatusID#" cfsqltype="cf_sql_integer" >
		</cfif>

		ORDER BY oppPricingStatusID
	</cfquery>

	<cfreturn qryOppPricingStatus/>

</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for countryID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getcountryID" hint="Retrieves foreign key data for countryID THIS.qcountryID to the query object." returntype="query">
	<cfargument name="CountryID" required="no" type="numeric" hint="provide this to get  a Country" >

	<cfquery name="qryCountry" datasource="#application.siteDataSource#">
		SELECT CountryID AS ID, CountryDescription AS ItemText
		FROM country
		where 1=1

		<!--- pkp bug 215 get showLiveCountryISOCODE list from opportunityINI.cfm for valid countires in the the drop down list--->
		<cfif isdefined('request.showLiveCountryISOCODE') and request.showLiveCountryISOCODE neq "">
		AND ISOcode IN (<cfqueryparam value="#request.showLiveCountryISOCODE#" cfsqltype="cf_sql_varchar" list="true">)
		</cfif>

		<cfif isdefined('arguments.CountryID') >
		AND CountryID = <cfqueryparam value="#arguments.CountryID#" cfsqltype="cf_sql_integer" >
		</cfif>

		ORDER BY CountryDescription
	</cfquery>
	<cfset THIS.qcountryID = qryCountry>
	<cfreturn qryCountry/>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for territoryID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getTerritoryID" hint="Retrieves foreign key data for countryID THIS.qcountryID to the query object.">
	<cfquery name="getTerritoryID" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_Sys_Unknown' as ItemText
		UNION
		SELECT [TerritoryID] as ID,[TerritoryName]  AS ItemText
			FROM TerritoryName
	</cfquery>
<cfset THIS.qTerritoryID = getTerritoryID>
</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for sourceID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getsourceID" returntype="query" hint="Retrieves foreign key data for sourceID THIS.qsourceID to the query object." validValues="true">
	<cfargument name="sourceID" type="Numeric" required="false">

	<cfquery name="qrysourceID" datasource="#application.siteDataSource#">
		<!--- NJH 2009/09/29 LID 2689 do not include campaigns as we've got campaigns with the same IDs as opportunity source.
			Commenting this out for now and campaigns may be a completely new field at some later stage.
		select campaignID as ID, campaignName as itemText from campaign
		UNION --->
		<cfif NOT isdefined('arguments.sourceID')>
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_Sys_Unknown' as ItemText
		UNION
		</cfif>
		SELECT OpportunitySourceID as ID, OpportunitySource as ItemText
		FROM OPPSource
		where 1=1
		<cfif isdefined('arguments.sourceID')>
		AND OpportunitySourceID = <cfqueryparam value="#val(arguments.sourceID)#" cfsqltype="cf_sql_integer">
		</cfif>
		ORDER BY ItemText
	</cfquery>
	<cfset THIS.qsourceID = qrysourceID>

	<cfreturn qrysourceID/>

</cffunction>


<!--- NJH 2010/01/14 LID 2702 - added new function --->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for campaignID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getCampaignID" returntype="query" hint="Retrieves foreign key data for campaignID THIS.qcampaignID to the query object." validValues="true">
	<cfargument name="campaignID" type="Numeric" required="false">

	<cfscript>
		var qryCampaignID = "";
	</cfscript>

	<cfquery name="qryCampaignID" datasource="#application.siteDataSource#">
		<cfif NOT isdefined('arguments.campaignID')>
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_Sys_Unknown' as ItemText, 0 as sortIndex
		UNION
		</cfif>
		select campaignID as ID, campaignName as itemText, 1 as sortIndex from campaign
		where 1=1
		<cfif isdefined('arguments.campaignID')>
		AND campaignID = <cfqueryparam value="#val(arguments.campaignID)#" cfsqltype="cf_sql_integer">
		</cfif>
		ORDER BY sortIndex,ItemText
	</cfquery>
	<cfset THIS.qCampaignID = qryCampaignID>

	<cfreturn qryCampaignID/>

</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for vendorSalesManagerPersonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getvendorSalesManagerPersonID" hint="Retrieves foreign key data for vendorSalesManagerPersonID THIS.qvendorSalesManagerPersonID to the query object." validValues="true">

	<!--- P-FNL079 2009/10/28 - added sortIndex and changed preserveSingleQuotes to listQualify --->
	<!--- 2011/06/23 PPB LID6918 setting changed from leadManager.leadVendorOrgIds to theClient.clientOrganisationID --->
	<cfset var qVendorSalesManagerPersonID = queryNew('') />

	<cfquery name="qVendorSalesManagerPersonID" datasource="#application.siteDataSource#">
		Select PersonID as ID, FirstName + ' ' + LastName as ItemText
		FROM Person p
		inner join booleanflagdata bfd on p.personid = bfd.entityid
		inner join flag f on bfd.flagid = f.flagid
		where flagtextid  in ( <cf_queryparam value="#application.com.settings.getSetting("leadManager.vendorSalesManagerFlagTextIDs")#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		AND OrganisationID  in ( <cf_queryparam value="#application.com.settings.getSetting("theClient.clientOrganisationID")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		order by itemText
	</cfquery>
<cfset THIS.qvendorSalesManagerPersonID = qVendorSalesManagerPersonID>

	<cfreturn qVendorSalesManagerPersonID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for vendorAccountManagerPersonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getvendorAccountManagerPersonID" hint="Retrieves foreign key data for vendorAccountManagerPersonID THIS.qvendorAccountManagerPersonID to the query object.">
	<cfargument name="leadVendorOrgIDs" type="string" required="false" hint="used to filter result to Person in charge of account">

	<!--- NJH 2009/10/29 P-FNL079 if salesForce module is on, then add the isSalesOperationsAdmin flag --->
	<!--- <cfif listfind(application. liveRelayApps,51)> --->

	<cfset vendorAcMngrFlagTextIDs = application.com.settings.getSetting("leadManager.vendorAcMngrFlagTextIDs")>
	<cfif application.com.relayMenu.isModuleActive(moduleID="SalesForce")>
		<cfset vendorAcMngrFlagTextIDs = listAppend(vendorAcMngrFlagTextIDs,"isSalesOperationsAdmin")>
	</cfif>

	<!--- NJH 2009/10/29 P-FNL079 - changed preserveSingleQuotes to listQualify --->
	<cfquery name="getvendorAccountManagerPersonID" datasource="#application.siteDataSource#">
		<!---
		18-10-2007 Gad: not required, already exists in Opportunity form  select 0 as ID, 'Choose account manager' as itemText
		UNION --->
		SELECT personid as ID, firstName + ' ' + lastname + ' (' + ISOCode + ')' as itemText
		from location l
		inner join person p on p.locationid = l.locationid
		inner join country c on l.countryid = c.countryid
		inner join booleanflagdata bfd on p.personid = bfd.entityid
		inner join flag f on bfd.flagid = f.flagid
		where flagtextid  in ( <cf_queryparam value="#vendorAcMngrFlagTextIDs#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )

		<cfif application.com.settings.getSetting("leadManager.vendorAccountMngrCountryScoped")>
		AND l.countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		<cfif isdefined('arguments.leadVendorOrgIDs')>
			and p.organisationID  in ( <cf_queryparam value="#leadVendorOrgIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- defined in opportunityINI.cfm --->
		</cfif>
		order by itemText
	</cfquery>
	<cfset THIS.qvendorAccountManagerPersonID = getvendorAccountManagerPersonID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for partnerLocationID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getPartnerLocationID" returntype="query" hint="Retrieves foreign key data for partnerLocationID THIS.qpartnerLocationID to the query object.">
	<cfargument name="locationID" type="numeric" required="false" hint="locationID value">
		<!--- START: 2009/11/11	AJC P-LEX039 Competitive products and partner price --->
		<!--- P-FNL079 2009/10/28 - added sortIndex and changed preserveSingleQuotes to listQualify--->

		<!--- START: 2010/07/15	AJC P-LEX039 When creating a new lead the partner fields are blank --->
		<cfset var accountManagerPersonID = "0" />

		<cfif request.relaycurrentuser.isInternal and THIS.vendorAccountManagerPersonID eq 0>
			<cfset accountManagerPersonID = request.relaycurrentUser.personID>
		<cfelse>
			<cfset accountManagerPersonID = THIS.vendorAccountManagerPersonID>
		</cfif>
		<!--- END: 2010/07/15	AJC P-LEX039 When creating a new lead the partner fields are blank --->
	<cfquery name="qrypartnerLocationID" datasource="#application.siteDataSource#">
		<cfif NOT isdefined('arguments.locationID')>
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_noPartnerAssigned' as itemText, 1 as sortIndex
		UNION
		</cfif>
		SELECT l.locationID as ID, siteName +', '+ address4 +' ('+  c.ISOCode +')' as ItemText, 2 as sortIndex
		FROM location l
		INNER JOIN country c ON c.countryID = l.countryID
		INNER JOIN person p on p.locationid = l.locationid
		WHERE l.OrganisationID IN (SELECT EntityID
			FROM BooleanFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid WHERE f.flagtextID  in ( <cf_queryparam value="#urldecode(application.com.settings.getSetting("leadManager.partnerOrgFlagList"))#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
			<cfif isdefined("personCountryAcMngrFlagTextID") and application.com.flag.isFlagSetForCurrentUser("#personCountryAcMngrFlagTextID#")>
				<!--- 2012-07-18 PPB P-SMA001 AND l.countryID IN (#request.relaycurrentuser.countryList#) --->
				#application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# 	<!--- 2012-06-27 PPB P-SMA001 --->
			<cfelseif isdefined("organisationAcMngrFlagTextIDs")>
		AND l.OrganisationID IN
		(SELECT EntityID
			<!--- START: 2010/07/15	AJC P-LEX039 When creating a new lead the partner fields are blank --->
				FROM IntegerFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid WHERE f.flagtextID  in ( <cf_queryparam value="#urldecode(organisationAcMngrFlagTextIDs)#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) and data =  <cf_queryparam value="#accountManagerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			<!--- END: 2010/07/15	AJC P-LEX039 When creating a new lead the partner fields are blank --->
				AND l.countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				AND l.countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>


		ORDER BY sortIndex, ItemText
		<!--- <cfif NOT isdefined('arguments.locationID')>
		select 0 as ID, 'No partner assigned' as itemText, 1 as sortIndex
		UNION
		</cfif>
		SELECT l.locationID as ID, siteName +', '+ address4 +' ('+  c.ISOCode +')' as ItemText, 2 as sortIndex
		FROM location l
		INNER JOIN country c ON c.countryID = l.countryID
		INNER JOIN person p on p.locationid = l.locationid
		WHERE l.OrganisationID IN (SELECT EntityID
			FROM BooleanFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid WHERE f.flagtextID in (#listQualify(partnerOrgFlagList,"'")#))
		AND l.countryID = #THIS.countryID#
		<cfif isdefined('arguments.locationID')>
		AND l.locationID = <cfqueryparam value="#val(arguments.locationID)#" cfsqltype="cf_sql_integer">
		</cfif>


		ORDER BY sortIndex, ItemText  --->
	</cfquery>
	<!--- START: 2009/11/11	AJC P-LEX039 Competitive products and partner price --->
	<cfset THIS.qpartnerLocationID = qrypartnerLocationID>

		<cfreturn qrypartnerLocationID/>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for distiLocationID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<!--- 2014/06/03 NJH connector work - rework to work off of locationIDs --->
<cffunction access="public" name="getDistiLocationID" hint="Retrieves foreign key data for distiLocationID THIS.qdistiLocationID to the query object.">
	<cfargument name="locationID" type="numeric" required="false" hint="locationID value">

	<cfset var distiLocFlagList = application.com.settings.getSetting("leadManager.distiLocFlagList")>
	<!--- <cfset var tablename = "location">
	<cfset var ItemText = "siteName"> --->

	<!--- NJH 2009/11/09 P-FNL069 - changed preserveSingleQuotes to listQualify to the two queries in this function --->
	<!--- <cfquery name="qryFlagGroup" datasource="#application.siteDataSource#">
		select distinct entitytypeid from flaggroup fg inner join flag f on fg.flagGroupid = f.flagGroupid  WHERE f.flagtextID  in ( <cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	</cfquery>

	<cfif qryFlagGroup.recordcount eq 0>
		<cfset tablename = "location">
		<cfset ItemText = "siteName">
	<cfelseif qryFlagGroup.entitytypeid eq 1>
		<cfset tablename = "location">
		<cfset ItemText = "siteName">
	<cfelse>
		<cfset tablename = "organisation">
		<cfset ItemText = "organisationName">
	</cfif> --->

	<cfquery name="qryDistiLocationID" datasource="#application.siteDataSource#">
<!--- 		select 0 as ID, 'No distributor assigned' as itemText
		UNION
		SELECT #tablename#ID as ID, #ItemText# as ItemText
		FROM #tablename#
		WHERE #tablename#ID IN (SELECT EntityID
			FROM BooleanFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid
				WHERE f.flagtextID  in ( <cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
		AND countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >

		<cfif isdefined('arguments.locationID')>
			AND #tablename#ID = <cfqueryparam value="#arguments.locationID#" cfsqltype="cf_sql_integer">
		</cfif>

		ORDER BY ItemText  --->


		<cfif NOT structKeyExists(arguments,"locationID")>
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_noDistributorAssigned' as itemText, 0 as SortOrder
		UNION
		</cfif>
		select ID,itemText, sortOrder
		from
			(
				SELECT locationID as ID, sitename as ItemText, 1 as SortOrder
				FROM location l
					inner join booleanFlagdata bfd on bfd.entityID = l.locationID
					inner join vFlagDef locFlags on locFlags.flagID = bfd.flagId and locFlags.entityTable='location' and flagTextID in (<cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">)
				where countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif structKeyExists(arguments,"locationID")>
						and locationID = <cfqueryparam value="#val(arguments.locationID)#" cfsqltype="cf_sql_integer">
					</cfif>
				union
				SELECT locationID as ID, sitename as ItemText, 1 as SortOrder
				FROM location
					inner join booleanFlagdata bfd on bfd.entityID = l.organisationID
					inner join vFlagDef locFlags on locFlags.flagID = bfd.flagId and locFlags.entityTable='organisation' and flagTextID in (<cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">)
				where countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif structKeyExists(arguments,"locationID")>
						and locationID = <cfqueryparam value="#val(arguments.locationID)#" cfsqltype="cf_sql_integer">
		</cfif>
			) locations
		ORDER BY SortOrder, ItemText
	</cfquery>

	<cfset THIS.qDistiLocationID = qryDistiLocationID>

	<cfreturn qryDistiLocationID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for distiSalesPersonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<!--- 2014/06/03 NJH connector work - rework to work off of locationIDs --->
<cffunction access="public" name="getDistiSalesPersonID" hint="Retrieves foreign key data for distiSalesPersonID THIS.qDistiSalesPersonID to the query object.">


	<cfset var getDistiSalesPersonID = "">
	<!--- <cfset var distiLocFlagList = application.com.settings.getSetting("leadManager.distiLocFlagList")> --->


	<!--- P-FNL079 NJH 2009/10/28 - added a sortIndex and changed preserveSingleQuotes to listQualify--->
	<!--- <cfquery name="qryFlagGroup" datasource="#application.siteDataSource#">
		select distinct entitytypeid from flaggroup fg inner join flag f on fg.flagGroupid = f.flagGroupid  WHERE f.flagtextID  in ( <cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	</cfquery> --->

	<cfquery name="getDistiSalesPersonID" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_SelectDistiSalesPerson' as itemText, 1 as sortIndex
		UNION
		Select PersonID as ID, FirstName + ' ' + LastName as ItemText, 2 as sortIndex
		FROM Person
		WHERE
		<!--- 8.1 Bug Fix NJH 2009/02/02 - It would appear that the distiLocationID can be a location or an organisation, depending on
			how the flag has been set up (see function getDistiLocationID). If the this.distiLocationID is a location, get people at that location otherwise get people at the organisation --->
		<!--- <cfif qryFlagGroup.recordcount eq 0 or qryFlagGroup.entitytypeid eq 1> --->
			locationID
		<!--- <cfelse>
			organisationID
		</cfif> --->
			= <cf_queryparam value="#THIS.distiLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY sortIndex, itemText
	</cfquery>
<cfset THIS.qDistiSalesPersonID = getDistiSalesPersonID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for partnerSalesManagerPersonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getpartnerSalesManagerPersonID" hint="Retrieves foreign key data for partnerSalesManagerPersonID THIS.qpartnerSalesManagerPersonID to the query object.">

	<!--- P-FNL079 2009/10/28 - added sortIndex --->
	<cfquery name="getPartnerSalesManagerPersonID" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_SelectPartnerSalesManagerPerson' as itemText, 1 as sortIndex
		UNION
		Select PersonID as ID, FirstName + ' ' + LastName as ItemText, 2 as sortIndex
		FROM Person
		WHERE locationID =  <cf_queryparam value="#THIS.partnerLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY sortIndex, itemText
	</cfquery>
<cfset THIS.qpartnerSalesManagerPersonID = getpartnerSalesManagerPersonID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for partnerSalesPersonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getPartnerSalesPersonID" hint="Retrieves foreign key data for partnerSalesPersonID THIS.qpartnerSalesPersonID to the query object.">
	<cfquery name="getPartnerSalesPersonID" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_SelectPartnerSalesPerson' as itemText, 1 as sortIndex
		UNION
		Select PersonID as ID, FirstName + ' ' + LastName as ItemText, 2 as sortIndex
		FROM Person
		WHERE locationID =  <cf_queryparam value="#THIS.partnerLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY sortIndex, itemText
	</cfquery>
	<cfset THIS.qpartnerSalesPersonID = getpartnerSalesPersonID>

	<cfreturn getPartnerSalesPersonID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             ForeignKey Query for oppReasonID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getOppReasonID" hint="Retrieves foreign key data for oppReasonID THIS.qoppReasonID to the query object." validValues="true">
	<cfargument name="oppReasonID" type="numeric" required="false">

	<cfquery name="qryoppReasonID" datasource="#application.siteDataSource#">
		SELECT oppReasonID as ID, oppReason as ItemText, 2 as sortIndex
		FROM oppReason
		where 1=1
		<cfif structKeyExists(arguments,"oppReasonID")>
		AND oppReasonID = <cfqueryparam value="#val(arguments.oppReasonID)#" cfsqltype="cf_sql_integer">
		</cfif>

		ORDER BY sortIndex,itemText
	</cfquery>
<cfset THIS.qoppReasonID = qryoppReasonID>
<cfreturn qryoppReasonID/>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getProductList" hint="Retrieves foreign key data for productGroupID THIS.qproductGroupID to the query object.">
    <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

    <cfif THIS.countryID NEQ 0>
        <cfset arguments.countryID = this.countryID> <!--- not sure if we need this --->
    </cfif>

	<cfquery name="getproductGroupID" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as ID, 'phr_opp_SelectProduct' as itemText, 0 as sortOrder
		UNION
		SELECT p.productID as ID, p.[description] as itemText,sortOrder
			FROM product p
            left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
		where p.campaignID  in ( <cf_queryparam value="#THIS.productCatalogueCampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<!--- and (countryID in (select countrygroupid from countrygroup where countrymemberid =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or countryID = 0) --->
        and (p.hascountryScope = 0 or vcs.countryid is not null)
		and deleted = 0
		and active = 1
		order by sortOrder
	</cfquery>
<cfset THIS.qproductGroupID = getproductGroupID>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             get Query for productGroupID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="getProductAndGroupList" hint="Retrieves foreign key data for productGroupID THIS.qproductGroupID to the query object.">
    <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

    <cfif THIS.countryID NEQ 0>
        <cfset arguments.countryID = this.countryID>
    </cfif>

	<cfquery name="getProductAndGroupList" datasource="#application.siteDataSource#">
		<!--- NYB 2011-11-28 LHID8224 changed itemText to a translation --->
		select 0 as productID, 'phr_opp_SelectProductGroup' as productDescription, 0,
			0 as productGroupID, '' as productGroupDescription
	UNION
		SELECT p.productID as productID, left(p.SKU+' '+replace(replace(replace(p.description,CHAR(34),' inch'),CHAR(39),''),CHAR(47),''),47) as productDescription,p.sortOrder,
			pg.productGroupID, pg.[description] as productGroupDescription
				FROM product p
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			INNER JOIN productGroup pg
			ON p.productGroupID = pg.productGroupID
			where p.campaignID  in ( <cf_queryparam value="#THIS.productCatalogueCampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<!--- and (countryID in (select countrygroupid from countrygroup where countrymemberid =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or countryID = 0) --->
            and (p.hascountryScope = 0 or vcs.countryid is not null)
			and deleted = 0
			and active = 1
			order by productGroupDescription, productGroupID, productDescription
	</cfquery>
<cfset THIS.qProductAndGroupList = getProductAndGroupList>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             select Query for products list
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOppProducts" hint="Retrieves the oppProducts setting the query object to getOppProducts.">
        <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

	<cfset var getOppProducts = QueryNew("")>		<!--- 2014-04-28 PPB Case 439350 var'ed vble to avoid clash with getOppProducts function in custom file --->
	<cfset var oppProductsEditor = application.com.settings.getSetting("leadManager.products.oppProductsEditor")>	<!--- 2014-04-28 PPB Case 439350 moved and var'ed while at it --->

	<cfparam name="this.countryID" type="numeric" default="0">
	<cfparam name="this.oppProductID" type="string" default="">
	<cfparam name="THIS.sortOrder" type="string" default="SKU">

        <cfif THIS.countryID NEQ 0>
            <cfset arguments.countryID = this.countryID>
        </cfif>

	<!--- NJH 2007/04/10  - if we don't want to display products --->
	<cfif oppProductsEditor neq "none">
		<cfquery name="qryOppProducts" datasource="#application.siteDataSource#">
			<cfswitch expression="#oppProductsEditor#">
				<cfcase value="oppProductList">
				<!--- this is the richer of the oppProducts editors --->
<!--- WAB 2009/02/24 this query falls over when the user sorts by probability, because more than one probability column exists
Solution is to do the ordering on a sub query
--->
	Select * from
					(SELECT
						p.SKU,
						left(p.[description],50) as description,
						opportunityProductID,
						quantity,
						unitPrice,
						subtotal =
						CASE
					 		WHEN pr.ProductPromotionID <> NULL THEN quantity * pp.promoprice
						 		<!--- 2010/06/09			NAS	LID 3174 - SPQ 100% Discount Broken --->
						 		WHEN specialPrice >= 0 THEN quantity * specialPrice
				    		ELSE quantity * unitprice
						END,
						specialPrice,
						<!--- 2006/04/20 CR_ATI032 Price Difference --->
						unitprice - specialPrice as PriceDifference,
						initialCOGS,
						COGS,
						(unitPrice - COGS) * quantity as standardMargin,
						SPQMargin = CASE
					 		WHEN specialPrice > 0 THEN (specialPrice - COGS) * quantity
					 		WHEN specialPrice = 0 THEN (specialPrice - COGS) * quantity				-- WAB CR_ATI036 2006-10-24 when special price is Zero use COGS
				    		ELSE (unitPrice - COGS) * quantity
						END,
						SPQPercentageMargin = CASE
						WHEN specialPrice > 0 AND subtotal > 0 THEN (((specialPrice - COGS) * quantity) / subtotal) * 100
						WHEN specialPrice > 0 AND subtotal <= 0 THEN ((specialPrice - COGS) * quantity) * 100
						WHEN specialPrice = 0 AND subtotal > 0 THEN (((specialPrice - COGS) * quantity) / subtotal) * 100    -- WAB CR_ATI036 2006-10-24 when special price is Zero use COGS
						WHEN (specialPrice <= 0 or specialprice is null) AND subtotal > 0 THEN (((unitPrice - COGS) * quantity) / subtotal ) * 100
						ELSE (((unitPrice - COGS) * quantity)) * 100
						END,
						op.probability,
						forecastShipDate,
						stockLocation,
						productsShipped,
						shippingComplete,
						specialPriceApproverPersonID,
						pp.promoprice as PromotionalPrice,
						pr.ProductPromotionID,
						SPManualEdit,
						p.productid,
							p.status,
							<!--- START: 2009-11-30	AJC	P-LEX039 Partner Price --->
							op.originalUnitPrice,
							op.approvedPrice,
							<!--- END: 2009-11-30	AJC	P-LEX039 Partner Price --->
							<!--- START: 2010/02/24	NAS	P-LEX039 Demo Products --->
							op.demo
							<!--- END: 2010/02/24	NAS	P-LEX039 Demo Products --->
					FROM opportunityProduct op
					LEFT JOIN product p ON op.ProductORGroupID = p.productid and op.productOrGroup='P'
                    left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
					LEFT JOIN opportunity o ON o.opportunityID = op.opportunityID
					LEFT OUTER JOIN productPromotionProduct pp ON pp.ProductID = p.productid
					LEFT OUTER JOIN productPromotion pr ON pr.ProductPromotionID = pp.productPromotionID
					AND op.forecastShipDate BETWEEN pr.Startdate AND pr.Enddate
					AND pr.countryid =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE op.opportunityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif this.oppProductID neq "">
						AND opportunityProductID =  <cf_queryparam value="#this.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>
                    AND (p.hascountryScope = 0 or vcs.countryid is not null)
				) as Dummy
					order by #THIS.sortOrder#
				</cfcase>
				<!--- 2014-10-23	RPW	CORE-901 Error seen while  i request for special Pricing(Opportunities) in internal site - Added column specialPrice --->
				<cfcase value="oppProductInclude">
	Select * from
				(
					SELECT
						p.SKU,
						left(p.[description],50) as description,
						opportunityProductID,
						quantity,
						unitPrice,
						subtotal,
						specialPrice,
						p.status
					FROM opportunityProduct op
						LEFT JOIN product p ON op.ProductORGroupID = p.productid and op.productOrGroup = 'P'
						LEFT JOIN opportunity o ON o.opportunityID = op.opportunityID
					WHERE op.opportunityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				) as Dummy
					order by #THIS.sortOrder#
				</cfcase>
			</cfswitch>
	</cfquery>

	<!--- 2014-04-28 PPB Case 439350 removed cfelse --->
</cfif>
<cfset THIS.getOppProducts = getOppProducts>
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             add Query for products list
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!--- START: 2009/11/11	AJC P-LEX039 Competitive products and partner price --->
	<cffunction access="public" name="addOppProduct" hint="Inserts a record for oppProducts." return="query">
	<cfparam name="this.probability" type="string" default="0">
	<cfparam name="this.specialPrice" type="string" default="">
		<cfparam name="this.partnerPrice" type="string" default="">
	<cfparam name="this.forecastShipDate" type="string" default="">
	<cfparam name="this.unitPrice" type="string" default="">
		<cfparam name="this.approvedPrice" type="string" default="">
	<cfquery name="addOppProduct" datasource="#application.siteDataSource#">
			set nocount on;

		INSERT INTO opportunityProduct (OpportunityID, [ProductORGroupID], ProductORGroup, [quantity],
			[unitPrice], [originalUnitPrice], [createdby], [lastupdated], [lastupdatedby], [created],
			probability, specialPrice, forecastShipDate, stockLocation, approvedPrice, demo)
		SELECT
			<cf_queryParam value="#THIS.opportunityID#" cfsqltype="CF_SQL_INTEGER">,
			productID, 'P',
			<cf_queryParam value="#THIS.quantity#" cfsqltype="CF_SQL_INTEGER">,
		<cfif this.unitPrice neq "">
				<cf_queryParam value="#this.unitPrice#" cfsqltype="CF_SQL_NUMERIC">,
		<cfelse>
		cast(listPrice as int),
		</cfif>
			cast(listPrice as int),
			<cf_queryParam value="#THIS.createdBy#" cfsqltype="CF_SQL_INTEGER">,
			getDate(),
			<cf_queryParam value="#THIS.lastUpdatedby#" cfsqltype="CF_SQL_INTEGER">,
			getDate(),
			<cf_queryParam value="#this.probability#" cfsqltype="CF_SQL_INTEGER">,
			<cf_queryParam value="#iif( this.specialPrice neq "", "this.specialPrice", de( "null" ) )#" cfsqltype="CF_SQL_NUMERIC">
		<cfif this.forecastShipDate neq "">
			, <cfqueryparam value="#this.forecastShipDate#" cfsqltype="CF_SQL_DATE">
		<cfelse>
			, null
		</cfif>
		<cfif isDefined("this.stockLocation")>
				, <cf_queryparam value="#this.stockLocation#" CFSQLTYPE="CF_SQL_VARCHAR">
		<cfelse>
			, null
		</cfif>
			, <cf_queryParam value="#iif( this.approvedPrice neq "", "this.approvedPrice", de( "null" ) )#" cfsqltype="CF_SQL_NUMERIC">
			<cfif isDefined("this.Demo")>
				, <cf_queryparam value="#this.Demo#" CFSQLTYPE="CF_SQL_bit" >
			<cfelse>
				, 0
			</cfif>
			FROM  product
			where productid =  <cf_queryparam value="#THIS.productID#" CFSQLTYPE="CF_SQL_INTEGER" >

			select scope_identity() as OppProductID
	</cfquery>

	<!--- WAb 2007/07/10
		if a product is added with a special price then we need to reset the approval status
	 --->
	<cfif this.specialPrice neq "">
		<cfset specialPriceChange()>
	</cfif>
		<cfreturn addOppProduct>
<!--- <cfset THIS.addOppProduct = addOppProduct> --->
</cffunction>
	<!--- END: 2009/11/11	AJC P-LEX039 Competitive products and partner price--->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             delete Query for products list
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction access="public" name="removeOppProduct" hint="DELETES a record from opportunityProduct.">
	<cfquery name="removeOppProduct" datasource="#application.siteDataSource#">
		DELETE from opportunityProduct
			where opportunityProductID =  <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
<!--- <cfset THIS.addOppProduct = addOppProduct> --->
</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             update for products list
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<!--- START: 2009/11/11	AJC P-LEX039 partner price--->
<cffunction access="public" name="updateOppProduct" hint="UPDATES a record from opportunityProduct.">
	<!--- sach: I added this to reset shipping if more products are ordered ofter shipping is complete --->
			<cfparam name="this.partnerPrice" type="string" default="">
			<cfparam name="this.approvedPrice" type="string" default="">

		<cfquery name="getSpecialPricingProduct" datasource="#application.siteDataSource#">
		select Quantity, productsShipped from opportunityProduct
		where opportunityProductID =  <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfif getSpecialPricingProduct.Quantity GT getSpecialPricingProduct.productsShipped>
			<cfset shipped = 0>
		</cfif>
	<cfquery name="updateOppProduct" datasource="#application.siteDataSource#">
		update opportunityProduct
		set
			<cfif this.forecastShipDate neq "">
				forecastShipDate = <cfqueryparam value="#this.forecastShipDate#" cfsqltype="CF_SQL_DATE">,
			</cfif>
			probability =  <cf_queryparam value="#iif( this.probability neq "", "this.probability", de( "null" ) )#" CFSQLTYPE="CF_SQL_Integer" > ,
			specialPrice =  <cf_queryparam value="#iif( this.specialPrice neq "", "this.specialPrice", de( "null" ) )#" CFSQLTYPE="CF_SQL_decimal" > ,
			quantity =  <cf_queryparam value="#THIS.quantity#" CFSQLTYPE="CF_SQL_INTEGER" > , lastUpdated = getDate(),
			stockLocation =  <cf_queryparam value="#THIS.stockLocation#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			SPManualEdit =  <cf_queryparam value="#THIS.SPManualEdit#" CFSQLTYPE="CF_SQL_bit" > ,
			<cfif isdefined("shipped")>shippingComplete = <cf_queryparam value="#shipped#" CFSQLTYPE="CF_SQL_bit" > ,</cfif>
				lastupdatedBy =  <cf_queryparam value="#THIS.lastUpdatedBy#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				unitPrice =  <cf_queryparam value="#iif( this.unitPrice neq "", "this.unitPrice", de( "null" ) )#" CFSQLTYPE="cf_sql_float" > ,
				approvedPrice =  <cf_queryparam value="#iif( this.approvedPrice neq "", "this.approvedPrice", de( "null" ) )#" CFSQLTYPE="CF_SQL_decimal" >
				<cfif isdefined("this.Demo")>,demo = <cf_queryparam value="#this.Demo#" CFSQLTYPE="CF_SQL_bit" ><cfelse>,demo = 0</cfif>
		where opportunityProductID =  <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfif this.specialPrice neq "">
		<cfquery name="getOppID" datasource="#application.siteDataSource#">
			select opportunityid from opportunityProduct where opportunityProductID =  <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfset this.opportunityid = getOppID.opportunityid>
		<cfset specialPriceChange()>
	</cfif>
</cffunction>
	<!--- END: 2009/11/11	AJC partner price--->

<cffunction access="public" name="specialPriceChange" hint="resets various things if a special price changes (either a product edited or changed)">
		<cfargument name="comments" type="string" default="Special Price change"> <!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 - called when a product is deleted as well.--->
		<cfargument name="opportunityID" type="numeric" default="0">
<!--- WAB 2007/07/10  - cut out of updateOppProduct so that it could be used with addOppProduct as well --->

		<cfscript>
			var oppID=0;
		</cfscript>

		<!--- NJH 2009/01/20 Bug Fix ATI Issue 1624 - check if this.opportunityID exists.. this should be the default --->
		<cfif structKeyExists(this,"opportunityID") and this.opportunityID neq 0>
			<cfset oppID=this.opportunityID>
		<cfelse>
			<cfset oppID=arguments.opportunityID>
		</cfif>

		<cfquery name="getSpecialPricingStatus" datasource="#application.siteDataSource#">
			select oppPricingStatusID from oppPricingStatus where oppPricingStatusMethod = 'SP_Calculate'
		</cfquery>

		<cfquery name="insertOppPricingStatusHistory" datasource="#application.siteDataSource#">
			INSERT INTO [oppPricingStatusHistory]([opportunityID], [oppPricingStatusID], [personid], [comments])
			VALUES(<cf_queryparam value="#oppID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#arguments.comments#" CFSQLTYPE="CF_SQL_VARCHAR" >)
		</cfquery>

		<!--- <cfinvoke component="relay.com.oppSpecialPricing" method="checkApproverLevelRequired" returnvariable="approverDetails">
			<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
			<cfinvokeargument name="opportunityID" value="#oppID#"/>
		</cfinvoke> --->

		<!--- NJH 2009/08/24 P-ATI005 - changed call to that of below so that I could take advantage of over-riding functions. --->
		<cfset approverDetails = application.com.oppSpecialPricing.checkApproverLevelRequired(dataSource=application.siteDataSource,opportunityID=oppID)>

		<cfquery name="updateSpecialPricingStatus" datasource="#application.siteDataSource#">
			update opportunity
				set oppPricingStatusID =  <cf_queryparam value="#getSpecialPricingStatus.oppPricingStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					SPRequiredApprovalLevel =  <cf_queryparam value="#listfirst(approverDetails,"|")#" CFSQLTYPE="CF_SQL_Integer" > ,
					SPCurrentApprovalLevel = 0,
					spapproverpersonids = null    <!--- WAB added 2007/07/10  when approval level set back to 0 then the list of approvers  should be cleared down as well.  (Lighthouse ATI-6) --->
			where opportunityID =  <cf_queryparam value="#oppID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>


</cffunction>

<!--- --------------------------------------------------------------------- --->
<!--- probability lookup --->
<!--- --------------------------------------------------------------------- --->
	<cffunction access="public" name="OppProductProbability" hint="List OppProduct probability">

		<cfparam name="this.OppProductProbabilitySelectName" type="string" default="frmProbability">
		<cfparam name="this.OppProductProbabilitySelectDefault" type="string" default="0">
		<cfparam name="this.OppProductProbabilitySelectOnchange" type="string" default="">

		<cfquery name="qOppProductProbability" datasource="#application.siteDataSource#">
			SELECT distinct probability FROM OppStage
		</cfquery>
		<cfset this.qOppProductProbability = qOppProductProbability>

		<cfset OppProductProbabilitySelect = '<select name="' & this.OppProductProbabilitySelectName & '"' & iif( this.OppProductProbabilitySelectOnchange neq "", de( ' OnChange="#this.OppProductProbabilitySelectOnchange#"' ), de( "" ) ) & '>'>
		<cfloop query="qOppProductProbability">
			<cfset OppProductProbabilitySelect = OppProductProbabilitySelect & '<option value="' & qOppProductProbability.probability & '"' & iif( qOppProductProbability.probability eq this.OppProductProbabilitySelectDefault, de( " selected" ), de( "" ) ) & '>' & qOppProductProbability.probability & '</option>'>
		</cfloop>
		<cfset OppProductProbabilitySelect = OppProductProbabilitySelect & '</select>'>
		<cfset this.OppProductProbabilitySelect = OppProductProbabilitySelect>

	</cffunction>
	<!--- --------------------------------------------------------------------- --->

	<!--- --------------------------------------------------------------------- --->
	<!--- product special price approval --->
	<!--- --------------------------------------------------------------------- --->
	<cffunction access="public" name="OppProductApproval" hint="Approve OppProduct">
		<cfparam name="this.oppProductID" type="string" default="0">
		<cfparam name="this.specialPriceApproverPersonID" type="string" default="0">
		<cfquery name="qOppProductApproval" datasource="#application.siteDataSource#">
			UPDATE opportunityProduct
			SET
			<cfif this.specialPriceApproverPersonID>
				specialPriceApproverPersonID = <cfqueryparam value="#this.specialPriceApproverPersonID#" cfsqltype="CF_SQL_INTEGER">,
			<cfelse>
				specialPriceApproverPersonID = null,
			</cfif>
				lastupdatedby = <cfqueryparam value="#this.specialPriceApproverPersonID#" cfsqltype="CF_SQL_INTEGER">,
				lastupdated = <cfqueryparam value="#now()#" cfsqltype="CF_SQL_DATE">
			WHERE opportunityProductID =  <cf_queryparam value="#this.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>
	<!--- --------------------------------------------------------------------- --->

	<!--- --------------------------------------------------------------------- --->
	<!--- trigger process action if special price has been requested --->
	<!--- --------------------------------------------------------------------- --->
	<cffunction access="public" name="OppProductSpecialPriceRequest" hint="Trigger special price process action">

		<cfparam name="this.OppProductSpecialPriceReason" type="string" default="unknown">

		<cftry>
			<cfset pid = ListFirst( Cookie.USER, "-" )>
			<cfset prid = "requestSpecialPrice">
			<cfinclude template="/proc.cfm">
			<cfcatch type="any">
	<!---
					<cfrethrow>
	 --->
			</cfcatch>
		</cftry>

	</cffunction>

	<!--- --------------------------------------------------------------------- --->
	<!--- --------------------------------------------------------------------- --->
	<!--- get the SKU from a productID --->
	<!--- --------------------------------------------------------------------- --->
	<cffunction access="public" name="OppProductGetSKUFromProductID" hint="Get SKU from product ID, used in adding multiple products">
		<cfargument name="productID" type="numeric" required="true">
		<cfquery name="getSKU" datasource="#application.siteDataSource#">
		select	sku,description
		from	product
		where	productID =  <cf_queryparam value="#oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfset oppProduct = getsku.sku & " - " & getsku.description>
		<cfreturn oppProduct>
	</cffunction>

	<!--- --------------------------------------------------------------------- --->

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             stub function for setOppSpecialProfileData
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setOppSpecialProfileData" hint="Executes a site-specific method which is defined in code/CFTemplates/opportunity.cfc">
		<cfparam name="opportunityID" type="numeric" default="0">
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            Assign Lead Manager Rights to Super Account Manager
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="SuperAMAssign" hint="Assigns a SuperAM to the Lead Rights">
		<!--- check that superAcctManagerIDs is defined in the opportunityINI file--->
		<cfif isdefined("superAcctManagerIDs")>
			<!--- check the the SAM is not the AM of the opp --->
			<cfquery name="qryGetVendorAccountManagerPersonID" datasource="#application.siteDataSource#">
				SELECT vendorAccountManagerPersonID
				FROM opportunity
				WHERE opportunityID =  <cf_queryparam value="#this.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and vendorAccountManagerPersonID =  <cf_queryparam value="#request.relayCurrentUser.personId#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfif qryGetVendorAccountManagerPersonID.RecordCount is 0>
				<!--- Check that the SAM is not already in the RecordRights for the Opp --->
				<cfquery name="qryGetUserGroupRecordRights" datasource="#application.siteDataSource#">
					SELECT UserGroupID
					FROM RecordRights
					WHERE UserGroupID =  <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
					Entity='OPPORTUNITY'
					and RecordID =  <cf_queryparam value="#this.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and Permission=1
				</cfquery>
				<cfif qryGetUserGroupRecordRights.RecordCount is 0>
					<!--- set the insert flag to false --->
					<cfset variables.assignsuperam=false>
					<cfoutput>
						<!--- loop through the superAcctManagerIDs stored in oppotunityINI.cfm --->
						<cfloop index="i" list="#superAcctManagerIDs#">
							<!--- see if superAcctManagerID exists in the users usergroups --->
							<cfif ListFind(COOKIE.USER,  i) neq 0>
								<!--- if it does then set insert flag to true --->
								<cfset variables.assignsuperam=true>
							</cfif>
						</cfloop>
						<!--- IF user is a SAM --->
						<cfif variables.assignsuperam is true>
							<CFQUERY NAME="AddSuperAMRights" DATASOURCE="#Application.SiteDataSource#">
								INSERT INTO RecordRights(UserGroupID,Entity,RecordID,Permission)
								VALUES(<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > ,'OPPORTUNITY',<cf_queryparam value="#this.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >,1)
							</CFQUERY>
						</cfif>
					</cfoutput>
				</cfif>
			</cfif>
		</cfif>

	<!--- <cfset THIS.addOppProduct = addOppProduct> --->
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            get Query for PaymentTerms
		+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getPaymentTerms" returntype="query" hint="Gets all the Payment terms from the OppPaymentTerms table, recordset included oppPaymentTermID, PaymentTerm">
		<cfargument name="PaymentTermID" type="numeric" required="false">
		<cfquery name="qryPaymentTerms" datasource="#application.siteDataSource#">
			select OppPaymentTermID as ID,PaymentTerm from OppPaymentTerms
			where 1=1
			<cfif structKeyExists(arguments,"PaymentTermID")>
			AND OppPaymentTermID = <cfqueryparam value="#val(arguments.PaymentTermID)#"  cfsqltype="cf_sql_integer">
			</cfif>
			order by OppPaymentTermID
		</cfquery>
		<cfset THIS.qPaymentTerms = qryPaymentTerms>
		<cfreturn qryPaymentTerms/>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            get Query for CustomerTypes
		+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCustomerTypes" returntype="query" hint="Gets all the Customer Types from the OppCustomerTypes table, recordset included oppCustomerTypeID, CustomerType">
		<cfargument  name="customerTypeID" type="numeric" required="false" hint="ID">
		<cfquery name="qryCustomerTypes" datasource="#application.siteDataSource#">
			select OppCustomerTypeID as ID,CustomerType,
			'OppCustomerType_'+CustomerTypeTextID as PhraseTextID <!--- NYB 2011-11-28 LHID8224 added --->
			from OppCustomerType
			where 1=1
			<cfif isdefined('arguments.customerTypeID')>
			AND	OppCustomerTypeID = <cfqueryparam value="#val(arguments.customerTypeID)#" cfsqltype="cf_sql_integer">
			</cfif>

			order by OppCustomerTypeID
		</cfquery>

		<cfset THIS.qCustomerTypes = qryCustomerTypes>
		<cfreturn qryCustomerTypes>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            UPDATE Opportunity Status By OrderID
		+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updOpportunityStatusByOrderID" hint="Updates the Status of an opportunity">

		<cfquery name="qry_updOpportunityStatusByOrderID" datasource="#application.SiteDataSource#">
			Update Opportunity
			set StageID =  <cf_queryparam value="#this.StageID#" CFSQLTYPE="CF_SQL_INTEGER" >
			WHERE orderID =  <cf_queryparam value="#this.orderID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            UPDATE Opportunity Status By OrderID
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updOpportunityStatusByopportunityID" hint="Updates the Status of an opportunity">

		<cfquery name="qry_updOpportunityStatusByopportunityID" datasource="#application.SiteDataSource#">
			Update Opportunity
			set StageID =  <cf_queryparam value="#this.StageID#" CFSQLTYPE="CF_SQL_INTEGER" >
			WHERE opportunityID =  <cf_queryparam value="#this.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	            Creates the settings for opportunity types
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setOpportunitySettingDefaults"
		hint="Creates the opportunity setting defaults for the opportunity module">
			<cfargument name = "applicationScope" default="#application#">

		<!--- gets all of the opptypes from the db to created the structures --->
		<cfquery name="qrygetOpportunityTypes" datasource="#applicationScope.siteDataSource#">
			select oppTypeID
			from OppType
			order by oppTypeID
		</cfquery>
		<!--- lock the application scope and set the opportunitySettings structure --->
		<cflock timeout="60" throwontimeout="No" type="EXCLUSIVE" scope="APPLICATION">

			<cfoutput query="qrygetOpportunityTypes">
			<cfscript>
					// set the variables that show/hide functions/fields in the opportunity section
					// AJC NOTE - Currently only implemented in opportunityedit.cfm
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID]=StructNew();
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].opptypeID=qrygetOpportunityTypes.opptypeID;
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowAccountDetails=true;
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowLeadDetails=true;
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowSalesSituation=true;
						//applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowPaymentSection=true; //MOVED TO SETTINGS
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowPartnerDetails=true;
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowCreatedBy=true;
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowLastUpdatedBy=true;
					// 2005/05/31 AJC START:P_SNY_011 VIP/11.1.6/Notes
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].ShowNotes=false;
					// 2005/05/31 AJC END:P_SNY_011 VIP/11.1.6/Notes
						applicationScope.opportunitySettings[qrygetOpportunityTypes.opptypeID].UseButtons=false;
			</cfscript>
			</cfoutput>


		</cflock>

	</cffunction>

	<!--- 2005/05/31 AJC START:P_SNY_011 VIP/11.1.6/Notes --->
	<cffunction access="public" name="getNotes" hint="Retrieves Notes data for opportunity THIS.Notes to the query object.">

		<cfscript>
			var qryGetNotes ="";
		</cfscript>
		<cfquery name="qryGetNotes" datasource="#application.siteDataSource#">
			SELECT noteText
			FROM entityNote
			where entityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=23
			ORDER BY created desc
		</cfquery>

		<cfscript>
			if(qryGetNotes.RecordCount is not 0)
			{
				THIS.Notes = qryGetNotes.noteText;
			}
		</cfscript>
	</cffunction>

	<cffunction access="public" name="updNotes" hint="Retrieves Notes data for opportunity THIS.Notes to the query object.">
		<!--- see if a note related to the opportunity already exists --->
		<cfquery name="qgetNotes" datasource="#application.siteDataSource#">
			SELECT noteText
			FROM entityNote
			where entityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=23
		</cfquery>
		<!--- if no note and there are notes to be added --->
		<cfif qgetNotes.RecordCount is 0 and THIS.notes is not "">
			<cfquery name="qDelNotes" datasource="#application.siteDataSource#">
				INSERT INTO entityNote(entityID,entityTypeID,noteText,noteType,createdBy,lastUpdatedBy)
				VALUES(<cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >,23,<cf_queryparam value="#THIS.Notes#" CFSQLTYPE="CF_SQL_VARCHAR" >,'General',<cf_queryparam value="#THIS.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#THIS.lastupdatedby#" CFSQLTYPE="CF_SQL_INTEGER" >)   <!--- WAB 2008-04-29 add N support  --->
			</cfquery>
		<!--- if there are notes however the notes have been cleared then delete --->
		<cfelseif qgetNotes.RecordCount is not 0 and THIS.notes is "">
			<cfquery name="qDelNotes" datasource="#application.siteDataSource#">
				DELETE FROM entityNote
				where entityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=23
			</cfquery>
		<!--- if there are notes and the notes have been changed then update --->
		<cfelseif qgetNotes.RecordCount is not 0 and THIS.notes is not "">
			<cfquery name="qDelNotes" datasource="#application.siteDataSource#">
				UPDATE entityNote
				set noteText =  <cf_queryparam value="#THIS.Notes#" CFSQLTYPE="CF_SQL_VARCHAR" >    <!--- WAB 2008-04-29 add N support  --->
				where entityID =  <cf_queryparam value="#THIS.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=23
			</cfquery>
		</cfif>

	</cffunction>

	<!--- --------------------------------------------------------------------- --->
	<!--- --------------------------------------------------------------------- --->
	<!--- get the opptunity textID from the opportunityID --->
	<!--- --------------------------------------------------------------------- --->
	<cffunction access="public" name="OppTextTypeID" hint="Get what type of lead this is partner ie(partner or default)">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfquery name="getOppTypeTextID" datasource="#application.siteDataSource#">
			select OppTypeTextID
			from opportunity op inner join
			opptype ot on op.opptypeID = ot.opptypeID
			where opportunityID = #opportunityID#
		</cfquery>

		<cfreturn getOppTypeTextID.OppTypeTextID>
	</cffunction>

	<!--- --------------------------------------------------------------------- --->
	<!--- --------------------------------------------------------------------- --->
	<!--- get the all the opptypes --->
	<!--- --------------------------------------------------------------------- --->
	<cffunction access="public" name="GetOppTypes" hint="Get all the opp types in the system" returntype="query">
		<cfargument name="oppTypeTextID" type="string" required="false">
		<cfargument name="oppTypeID" type="numeric" required="false">

		<cfquery name="qryOppTypes" datasource="#application.siteDataSource#">
			select distinct *
			from opptype
			WHERE 1=1
			<cfif StructKeyExists(arguments,"oppTypeTextID")>
				AND oppTypeTextID =  <cf_queryparam value="#arguments.oppTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			<cfif StructKeyExists(arguments,"oppTypeID")>
				AND oppTypeID =  <cf_queryparam value="#arguments.oppTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>

		<cfreturn qryOppTypes>
	</cffunction>
	<!--- 2005/05/31 AJC END:P_SNY_011 VIP/11.1.6/Notes --->

	<cffunction access="private" name="GetEquivalentSpecialPricingStatus">
		<cfargument name="StageID" type="numeric" required="yes">

		<cfset var getStageStatus = "">
		<cfset var getSpecialPriceEquivalent = "">

		<cfquery name="getStageStatus" datasource="#application.sitedatasource#">
			select status from OppStage where
				OpportunityStageID = #arguments.stageid#
		</cfquery>

		<cfif getStageStatus.recordcount eq 1>
			<cfquery name="getSpecialPriceEquivalent" datasource="#application.sitedatasource#">
				select oppPricingStatusMethod from oppPricingStatus where
					oppPricingStatus  like  <cf_queryparam value="%#getStageStatus.status#%" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfif getSpecialPriceEquivalent.recordcount eq 1>
				<cfreturn getSpecialPriceEquivalent.oppPricingStatusMethod>
			</cfif>
		</cfif>
		<cfreturn>
	</cffunction>

	<cffunction access="public" name="GetEmployeeTotals" hint="Get all the opp employee totals in the system" returntype="query">
		<cfargument name="employeeTotal" default="" required="No">
		<cfset var qryOppEmployeeTotals = "">
		<cfquery name="qryOppEmployeeTotals" datasource="#application.siteDataSource#">
			select distinct *
			from OppEmployeeTotal
			where 1=1
			<cfif arguments.employeeTotal neq "">
				and employeeTotal =  <cf_queryparam value="#arguments.employeeTotal#" CFSQLTYPE="CF_SQL_Integer" >
			</cfif>
		</cfquery>
		<cfreturn qryOppEmployeeTotals>
	</cffunction>


	<cffunction access="public" name="GetRWPromotions" hint="Get the RWPromotions that apply for this country/visibility" returntype="query">
		<cfargument name="PromotionCode" default="" required="No">
		<cfargument name="triggerPoints" default="" required="No">
		<cfargument name="excludeTriggerPoints" default="" required="No">

		<!--- Get the promotion Info --->
		<!--- LID 5312 NJH 2011/01/13 - Select  All Products as entity and changed inner join on promotion scope to left join, as if there are no restrictions
			on the promotion, then this returns no records  when the join is an inner join, when in fact it should be returning something --->
		<cfquery name="qryPromotions" datasource="#application.siteDataSource#">
			<!--- NYB 2011-11-28 LHID8224 changed to use a translation --->
			select distinct isNull(RWPS.Entity,'phr_opp_AllProducts') as entity,
			--select distinct RWPS.Entity ,
			<!--- If 0 is returned it means all Usergroups --->
			'Visibility' =		( CASE
						          			WHEN (EXISTS (SELECT 1
			                	FROM RecordRights rr
			                  	WHERE rr.entity = 'Promotion'
			                	AND rr.recordId = RWP.RWPromotionID)) THEN '1'
						          			ELSE '0'
						     	END),
			<!--- If 0 is returned it means all Countries --->
			'Country' =		( CASE
						          			WHEN (EXISTS (SELECT 1
			                	FROM countryScope csc
			                  	WHERE csc.entity = 'Promotion'
			                	AND csc.entityId = RWP.RWPromotionID)) THEN '1'
						          			ELSE '0'
						     	END),
			RWP.*,
			RWPromotionType.RWPromotionTypeTextID
			from RWPromotion RWP
			left join RWPromotionScope RWPS on RWP.RWPromotionID = RWPS.RWPromotionID
			inner join RWPromotionType ON RWP.RWPromotionTypeID = RWPromotionType.RWPromotionTypeID

			where 1=1
			<cfif arguments.PromotionCode neq "">
				and RWP.RWPromotionCode =  <cf_queryparam value="#arguments.PromotionCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			<cfif arguments.triggerPoints neq "">
				and RWP.TriggerPoint  IN ( <cf_queryparam value="#arguments.triggerPoints#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			</cfif>
			<cfif arguments.excludeTriggerPoints neq "">
				and RWP.TriggerPoint  NOT IN ( <cf_queryparam value="#arguments.excludeTriggerPoints#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			</cfif>
			and RWP.active = 1
		</cfquery>
		<cfreturn qryPromotions>
	</cffunction>

	<cffunction access="public" name="GetOppPromotionProducts" hint="Get all the Promotions that apply to this Opportunity" returntype="query">
		<cfargument name="OpportunityID" 	default="" required="Yes" type="numeric">
		<cfargument name="PromotionCode" 	default="" required="No">
		<cfargument name="OrderDate" 		default="" required="No">
		<cfargument name="personID" type="string" required="No">			<!--- 2014-01-07 PPB Case 437061 moved the default setting out of the cfargument definition --->
		<cfargument name="productID" default="" required="No">
		<cfargument name="triggerPoints" default="" required="No">
		<cfargument name="excludeTriggerPoints" default="" required="No">

		<cfset var qryOppPromotions = "">


		<!--- START 2014-01-07 PPB Case 437061 moved the default setting for personID from the cfargument definition because it uses arguments.OpportunityID --->
		<cfif not StructKeyExists(arguments,"personID")>
			<cfset personID=getOpportunityDetails(opportunityid = arguments.OpportunityID).partnersalesPersonID>
		</cfif>
		<!--- END 2014-01-07 PPB Case 437061 --->

		<!--- NJH 2014/06/03 connector - partnersalesPersonID can now be null on the db, so it won't be numeric --->
		<cfif not isNumeric(arguments.personID)>
			<cfset arguments.personID = 0>
		</cfif>

		<cfset GetPromotion = GetRWPromotions(PromotionCode=arguments.PromotionCode,triggerPoints=arguments.triggerPoints,excludeTriggerPoints=arguments.excludeTriggerPoints)>
		<cfif GetPromotion.RecordCount eq 0>
			<cfset qryOppPromotionProducts = GetPromotion>	<!--- if there are no appropriate promotions we still want to return a query so send back this empty one  --->
		<cfelse>

	<!--- If the Promotion exists look check it against Items in this order:
					Product Category
					Product Group
					Product
		--->

			<cfquery name="qryOppPromotionProducts" datasource="#application.siteDataSource#">
				<cfset firstLoop = true>
				<cfloop query="GetPromotion">
					<cfif firstLoop>
						<cfset firstLoop = false>
					<cfelse>
						 UNION
					</cfif>
					<!--- PPB 2010-05-26 added DISTINCT below cos promotion discount was being applied multiple times (sometimes) --->
					select DISTINCT o.opportunityID, op.opportunityProductID as OppProductID, op.opportunityProductID, op.UnitPrice, op.subtotal, RWP.RWPromotionID, RWP.RWPromotionCode, RWP.RWPromotionTypeID, RWP.TotalPoints, RWPT.RWPromotionTypeTextID
					from opportunityProduct op
					<cfif Entity EQ 'ProductCategoryID'>
						inner Join Product P on op.ProductORGroupID = p.productID and op.productOrGroup = 'P'
						inner join ProductGroupcategory PGC on p.productGroupID = PGC.productgroupID
						inner join RWPromotionScope RWPS on PGC.productCategoryID = RWPS.entityID
					<cfelseif Entity EQ 'ProductGroupID'>
						inner Join Product P on op.ProductORGroupID = p.productID and op.productOrGroup = 'P'
						inner join ProductGroup PG on p.productGroupID = PG.productgroupID
						inner join RWPromotionScope RWPS on PG.productgroupID = RWPS.entityID
					<cfelseif Entity EQ 'ProductID'>
						inner join RWPromotionScope RWPS on op.ProductORGroupID = RWPS.entityID
					</cfif>
					inner join opportunity o on op.opportunityID = o.opportunityID
					<!--- PPB 2010/12/01 added getPromotion.RWPromotionId cos when triggerPoints was specified other promotions were being returned
						NJH 2011/01/13 - moved the promotion ID into the join, and only join to rwps.promotionID when there are product restrictions, as when
							the promotion is open to all products, there are no entries in this table.
					 --->
					inner join RWPromotion RWP on RWP.RWPromotionId =  <cf_queryparam value="#getPromotion.RWPromotionId#" CFSQLTYPE="CF_SQL_INTEGER" >  <cfif not listFindNoCase("All Products,phr_opp_AllProducts",entity)>and RWPS.RWPromotionID = RWP.RWPromotionID</cfif>
					inner join RWPromotionType RWPT on RWP.RWPromotionTypeID = RWPT.RWPromotionTypeID
					where op.opportunityID =  <cf_queryparam value="#arguments.OpportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND RWP.Active = 1
					<cfif arguments.productID neq "">
						AND op.ProductORGroupID =  <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>
<!---
					<cfif Visibility EQ 1>
						AND (EXISTS (SELECT 1
					                	FROM RecordRights rr
					                  	WHERE rr.entity = 'PROMOTION'
					                	AND rr.recordId = RWP.RWPromotionID
					                   	AND rr.UserGroupId IN (SELECT ug.Usergroupid
					       				FROM person per
					               		INNER JOIN usergroup ug ON ug.personid = per.personid
					             		WHERE per.personId = #arguments.personID#)))
		           	</cfif>
 --->
					<!--- PPB 2010-06-28 visibility should use RightsGroup table --->
					<cfif Visibility EQ 1>			<!--- ie it is not visible to ALL UserGroups so we have to check --->
						AND (EXISTS (SELECT 1
										FROM vRecordRights rr
					                  	WHERE rr.entity = 'PROMOTION'
					                	AND rr.recordId = RWP.RWPromotionID
					                   	AND rr.PersonId  = <cf_queryparam value="#arguments.personId#" CFSQLTYPE="CF_SQL_INTEGER" >))
		           	</cfif>


		           	<!--- Check the Country --->
		           	<cfif Country EQ 1>				<!--- ie it is not visible to ALL Countries so we have to check --->
					AND (EXISTS (SELECT 1
				                	FROM CountryScope CS
				                  	WHERE cs.entity = 'Promotion'
				                	AND cs.entityId = RWP.RWPromotionID
				                   	AND cs.countryId IN (SELECT o.countryid
				       				FROM opportunity o
				             		WHERE o.opportunityId =  <cf_queryparam value="#arguments.OpportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > )))
		           	</cfif>
		           	<cfif arguments.OrderDate neq "">
						<cfif startdate NEQ "">
							AND arguments.OrderDate >= RWP.startdate
						</cfif>
						<cfif enddate NEQ "">
							AND arguments.OrderDate <= RWP.enddate
						</cfif>
					<cfelse>
						<cfif startdate NEQ "">
							AND GETDATE() >= RWP.startdate
						</cfif>
						<cfif enddate NEQ "">
							AND GETDATE()-1 <= RWP.enddate		<!--- -1 cos GETDATE() has a Time element --->
						</cfif>
					</cfif>
					<cfif arguments.PromotionCode neq "">
						AND RWP.RWPromotionCode =  <cf_queryparam value="#arguments.PromotionCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
		<!---
					<cfif entity neq "All Products">
					AND RWPS.Entity = '#Entity#'
					</cfif>
		 --->
				</cfloop>
			</cfquery>
		</cfif>

		<cfreturn qryOppPromotionProducts>

	</cffunction>


	<cffunction name="checkIfPromotionAlreadyApplied">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="promotionCode" type="string" required="false">

		<cfset var qryPromotionAlreadyApplied = "">
		<cfset var result=StructNew()>

		<cfquery name="qryPromotionAlreadyApplied" datasource="#application.siteDataSource#">
			SELECT     OppPromotion.OppPromotionID
			FROM         OppPromotion INNER JOIN
	                      RWPromotion ON OppPromotion.PromotionID = RWPromotion.RWPromotionID
			WHERE     (OppPromotion.OpportunityID = #OpportunityID#) AND (RWPromotion.RWPromotionCode =  <cf_queryparam value="#PromotionCode#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>

		<cfset result.PromotionAlreadyApplied=qryPromotionAlreadyApplied.recordCount>

		<cfreturn result>
	</cffunction>

	<!--- 2010/12/02 PPB LID4971 --->
	<cffunction name="checkIfCountryInScopeForPromotion">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="rwPromotionId" type="numeric" required="true">

		<cfset var countryInScope = 0>
		<cfset var qryPromotionCountries = "">
		<cfset var qryCountryInScopeForPromotion = "">

		<cfquery name="qryPromotionCountries" datasource="#application.siteDataSource#">
			SELECT cs.countryId
           	FROM CountryScope cs
            WHERE cs.entity = 'Promotion'
           	AND cs.entityId = #arguments.rwPromotionId#
		</cfquery>

		<cfif qryPromotionCountries.recordcount eq 0>	<!--- if no countries are scoped for the promo it implies All Countries are in scope --->
			<cfset countryInScope = 1>
		<cfelse>
			<cfquery name="qryCountryInScopeForPromotion" datasource="#application.siteDataSource#">
				SELECT 1
	           	FROM CountryScope cs
				INNER JOIN opportunity o ON o.countryid = cs.countryId AND o.opportunityId = #arguments.OpportunityID#
	            WHERE cs.entity = 'Promotion'
	           	AND cs.entityId = #arguments.rwPromotionId#
			</cfquery>
			<cfset countryInScope = qryCountryInScopeForPromotion.recordcount>
		</cfif>

		<cfreturn countryInScope>
	</cffunction>

	<!--- 2010/12/02 PPB LID4971 --->
	<cffunction name="checkIfPersonInScopeForPromotion">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="rwPromotionId" type="numeric" required="true">
		<cfargument name="personID" type="string" default="#getOpportunityDetails(opportunityid=arguments.OpportunityID).partnersalesPersonID#">

		<cfset var personInScope = 0>
		<cfset var qryPromotionVisibility = "">
		<cfset var qryPersonInScopeForPromotion = "">
		<cfset var thePersonID = arguments.personID>

		<cfif not isNumeric(thePersonID)>
			<cfset thePersonID = 0>
		</cfif>

		<cfquery name="qryPromotionVisibility" datasource="#application.siteDataSource#">
			SELECT 1
           	FROM RecordRights rr
            WHERE rr.entity = 'Promotion'
           	AND rr.recordId = #arguments.rwPromotionId#
		</cfquery>

		<cfif qryPromotionVisibility.recordcount eq 0>	<!--- if no specific Visibilities are scoped for the promo it implies All Persons are in scope --->
			<cfset personInScope = 1>
		<cfelse>
			<cfquery name="qryPersonInScopeForPromotion" datasource="#application.siteDataSource#">
				SELECT 1
				FROM RecordRights rr
				INNER JOIN RightsGroup rg ON rr.UserGroupID = rg.UserGroupID
                WHERE rr.entity = 'PROMOTION'
               	AND rr.recordId = <cf_queryparam value="#arguments.rwPromotionId#" cfsqltype="cf_sql_integer">
                AND rg.PersonId  = <cf_queryparam value="#thePersonID#" cfsqltype="cf_sql_integer">
			</cfquery>
			<cfset personInScope = qryPersonInScopeForPromotion.recordcount>
		</cfif>

		<cfreturn personInScope>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Update Query for OppProducts
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="applyPromotion" hint="Update the Promotion Price in opportunityProduct" returntype="query">
		<cfargument name="OpportunityID" 	default="" required="Yes">
		<cfargument name="PromotionCode" 	default="" required="No">
		<cfargument name="PromotionID" 		default="" required="No">
		<cfargument name="OrderDate" 		default="" required="No">
		<cfargument name="productID" 	 	default="" required="No">
		<cfargument name="triggerPoints" 	default="" required="No">
		<cfargument name="excludeTriggerPoints" default="" required="No">
		<cfargument name="Refresh" 			default="0" required="No">

		<cfset qryOppPromotions = GetOppPromotionProducts(argumentcollection = arguments)>

		<!--- this addOppPromotion is used for the promo code the user has just entered (so will be fired on 2 seperate occasions if 2 promos are entered); hence it doesn't need to be inside the following loop --->
		<cfif qryOppPromotions.recordcount GT 0 and arguments.Refresh EQ "0">
			<cfset AddPromo = addOppPromotion(opportunityID=#qryOppPromotions.opportunityID#,promotionID=#qryOppPromotions.RWPromotionID#)>

<!---
		<cfelseif qryOppPromotions.recordcount EQ 0>
			<!---delete promo - no longer relevant--->
			<cfset AddPromo = deleteOppPromotion(opportunityID=#arguments.opportunityID#,promotionID=#arguments.PromotionID#)>
 --->
		</cfif>

		<cfloop query="qryOppPromotions">
			<cfquery name="qapplyPromotion" datasource="#application.siteDataSource#">
				UPDATE op
				set op.promotionprice =
					CASE
						WHEN ('#qryOppPromotions.RWPromotionTypeTextID#' = 'Fixed' 		and promotionprice IS NULL) 		THEN #qryOppPromotions.UnitPrice# - #qryOppPromotions.TotalPoints#
						WHEN ('#qryOppPromotions.RWPromotionTypeTextID#' = 'Fixed' 		and promotionprice IS NOT NULL)		THEN promotionprice - #qryOppPromotions.TotalPoints#
						WHEN ('#qryOppPromotions.RWPromotionTypeTextID#' = 'Lump_Sum' 	and promotionprice IS NULL) 		THEN #qryOppPromotions.UnitPrice# - #qryOppPromotions.TotalPoints#
						WHEN ('#qryOppPromotions.RWPromotionTypeTextID#' = 'Lump_Sum' 	and promotionprice IS NOT NULL) 	THEN promotionprice - #qryOppPromotions.TotalPoints#
						WHEN ('#qryOppPromotions.RWPromotionTypeTextID#' = 'Percentage' 	and promotionprice IS NULL) 		THEN #qryOppPromotions.UnitPrice# - ((#qryOppPromotions.UnitPrice# / 100) * #qryOppPromotions.TotalPoints#)
						WHEN ('#qryOppPromotions.RWPromotionTypeTextID#' = 'Percentage' 	and promotionprice IS NOT NULL)		THEN promotionprice - ((#qryOppPromotions.UnitPrice# / 100) * #qryOppPromotions.TotalPoints#)
						ELSE promotionprice
					END
				from opportunityProduct op
				where op.opportunityID =  <cf_queryparam value="#qryOppPromotions.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and op.opportunityProductID =  <cf_queryparam value="#qryOppPromotions.OppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

		</cfloop>

		<cfreturn qryOppPromotions>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             add Query for Opp Promotion Codes
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	 <cffunction access="public" name="addOppPromotion" hint="Inserts a record for oppPromotions.">
		<cfargument name="opportunityID" required="Yes" default="">
		<cfargument name="PromotionID" required="Yes" default="">		<!--- the promotionid sent in may be the promotioncode --->

		<cfif not isnumeric("#arguments.PromotionID#")>
			<cfset arguments.PromotionID = getPromotionIdForCode('#arguments.PromotionID#')>
		</cfif>

		<cfquery name="qryaddOppPromotion" datasource="#application.siteDataSource#" result="addOppPromotionResult">
			if not exists (select 1 from OppPromotion 	where OpportunityID =  <cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND PromotionID =  <cf_queryparam value="#arguments.PromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND Active=1)
				INSERT INTO OppPromotion (OpportunityID, PromotionID, Active)
				VALUES (<cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#arguments.PromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >, 1)
		</cfquery>

		<cfreturn addOppPromotionResult.recordcount>
	</cffunction>

	<cffunction access="public" name="getPromotionIdForCode" hint="returns a promotion id for a given promotion code" returntype="numeric">
		<cfargument name="PromotionCode" required="Yes" default="">

		<cfquery name="qryPromotion" datasource="#application.siteDataSource#">
			Select RWPromotionID
			From RWPromotion
			Where RWPromotionCode =  <cf_queryparam value="#arguments.PromotionCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif qryPromotion.recordcount EQ 0>
			<cfset returnValue = 0>
		<cfelse>
			<cfset returnValue = qryPromotion.RWPromotionID>
		</cfif>

		<cfreturn returnValue>
	</cffunction>


 <!---
	<cffunction access="public" name="addOppPromotion" hint="Inserts a record for oppPromotions.">
		<cfargument name="opportunityID" required="Yes" default="">
		<cfargument name="PromotionID" required="Yes" default="">

		<cfquery name="qryaddOppPromotion" datasource="#application.siteDataSource#" result="addOppPromotionResult">
			if not exists (select 1 from OppPromotion 	where OpportunityID = #arguments.opportunityID#
														<cfif isnumeric("#arguments.PromotionID#")>
															AND PromotionID 	= #arguments.PromotionID#
														<cfelse>
															AND PromotionID 	= (Select  RWPromotionID
																					From RWPromotion
																					Where RWPromotionCode = '#arguments.PromotionID#')
														</cfif>
														)
			INSERT INTO OppPromotion (OpportunityID, PromotionID)
			<cfif isnumeric("#arguments.PromotionID#")>
				VALUES (#arguments.opportunityID#, #arguments.PromotionID#)
			<cfelse>
				Select #arguments.opportunityID#, RWPromotionID
				From RWPromotion
				Where RWPromotionCode = '#arguments.PromotionID#'
			</cfif>
		</cfquery>

		<cfreturn addOppPromotionResult.recordcount>
	</cffunction>
 --->


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Delete Query for Opp Promotion Codes
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deleteOppPromotion" hint="Deletes a record for oppPromotions.">
	<cfargument name="opportunityID" required="Yes" default="">
	<cfargument name="PromotionID" required="Yes" default="">

	<cfset var qDeleteOppPromotion = "">

	<cfquery name="qDeleteOppPromotion" datasource="#application.siteDataSource#">
			DELETE
			FROM 	OppPromotion
			WHERE 	OpportunityID =  <cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND 	PromotionID =  <cf_queryparam value="#arguments.PromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Refresh Opportunity Promotion Codes
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="refreshPromotions" hint="Refreshes the promotions of a particular Opportunity.">
		<cfargument name="opportunityID" type="numeric" required="Yes">

		<!--- START 2014-01-13 PPB Case 436434 don't bother if the opp has no products --->
        <cfquery name="qryOppProductsCount" datasource="#application.siteDataSource#">
	        SELECT COUNT(OpportunityID) AS OppProductsCount
	        from opportunityProduct op
	        where op.OpportunityID = #arguments.opportunityID#
        </cfquery>

      	<cfif qryOppProductsCount.OppProductsCount gt 0>
		<!--- END 2014-01-13 PPB Case 436434 --->

		  <!--- PPB 2010/12/01 get any current 'On Subtotal' promotions and apply them to this opp --->
		  <cfset qryRWPromotions = getRWPromotions(triggerPoints="subtotal")>
		  <cfloop query="qryRWPromotions">
			  <cfif not checkIfPromotionAlreadyApplied(opportunityID=arguments.opportunityID,promotionCode=qryRWPromotions.RWPromotionCode).PromotionAlreadyApplied>
				  <cfset applyPromotion(opportunityID=arguments.opportunityID,promotionCode=qryRWPromotions.RWPromotionCode)>
			  </cfif>
		  </cfloop>

		  <cftransaction>
			  <!--- Update the PromotionPrice on each product back to NULL --->
			  <cfquery name="qResetProducts" datasource="#application.siteDataSource#">
				  UPDATE op
				  set op.promotionprice = NULL
				  from opportunityProduct op
				  where op.OpportunityID = #arguments.opportunityID#
			  </cfquery>

			  <!--- Get the Promotions currently applied to this Opportunity --->
			  <cfquery name="qGetPromotions" datasource="#application.siteDataSource#">
				  Select op.*, RWP.RWPromotionID, RWP.RWPromotionCode
				  from oppPromotion op
				  inner join RWPromotion RWP on op.PromotionID = RWP.RWPromotionID
				  where op.OpportunityID = #arguments.opportunityID#
			  </cfquery>

			  <!--- Loop through the Promotions for this Opportunity --->
			  <cfloop query="qGetPromotions">
				  <!--- If the Promotion is still active re-apply the discount to the products --->
				  <cfset UpdateProducts = applyPromotion(opportunityID=OpportunityID,PromotionID=RWPromotionID,PromotionCode=RWPromotionCode,excludeTriggerPoints='pricebook',Refresh='1' )>		<!--- promotions with a PRICEBOOK triggerpoint are dealt with by relayOpportunityws.getPrice --->
			  </cfloop>
		  </cftransaction>

		</cfif>						<!--- 2014-01-13 PPB Case 436434 don't bother if the opp has no products --->

	</cffunction>

	<!--- NJH 2010/03/12 P-PAN002 --->
	<cffunction name="getDiscountedAmountForPromotion" access="public" hint="Returns the discount that a product has been given for a given opportunity and promotion" output="false">
		<cfargument name="promotionID" required="yes" hint="Can be a promotionCode or an ID" type="string">
		<cfargument name="opportunityID" required="yes" type="numeric">

		<cfset var getDiscountedProducts = "">

		<cfquery name="getDiscountedProducts" datasource="#application.siteDataSource#">

			SELECT     Product.SKU,
			CASE
				WHEN RWPromotionTypeTextID = 'Fixed' THEN TotalPoints
				WHEN RWPromotionTypeTextID = 'Lump_Sum' THEN TotalPoints
				WHEN RWPromotionTypeTextID = 'Percentage' THEN ((UnitPrice / 100) * quantity * TotalPoints)
			END AS discount
			FROM         opportunityProduct AS op INNER JOIN
			                      OppPromotion AS ops ON op.OpportunityID = ops.OpportunityID INNER JOIN
			                      RWPromotion AS RWP ON ops.PromotionID = RWP.RWPromotionID INNER JOIN
			                      RWPromotionType AS RWPT ON RWP.RWPromotionTypeID = RWPT.RWPromotionTypeID INNER JOIN
			                      Product ON op.ProductORGroupID = Product.ProductID and op.productOrGroup = 'P'
			WHERE     (op.OpportunityID = #arguments.opportunityID#)
			<cfif isNumeric(arguments.promotionID)>
				AND RWPromotionID =  <cf_queryparam value="#arguments.promotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				AND RWPromotionCode =  <cf_queryparam value="#arguments.promotionID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			AND (
			((SELECT COUNT(ProductID) FROM vRWPromotionProducts WHERE RWPromotionID = ops.PromotionID) = 0 )
				OR
			(ProductORGroupID IN (SELECT ProductID FROM vRWPromotionProducts WHERE RWPromotionID = ops.PromotionID))
			)
		</cfquery>

		<cfreturn getDiscountedProducts>
	</cffunction>


	<cffunction name="getAppliedOppPromotions" access="public" hint="returns all promotions that have been applied to an opportunity" output="false">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfset var getAppliedPromotions = "">

		<!--- this returns all the promotions for the opp including the inactive/expired ones so we can display a row marked "Expired" --->
		<cfquery name="getAppliedPromotions" datasource="#application.siteDataSource#">
			select rwPromotionCode, rwPromotionID,rwp.Active,rwp.StartDate,rwp.EndDate from rwPromotion rwp
				inner join oppPromotion op on rwp.rwPromotionID = op.promotionID
			where op.opportunityID = #arguments.opportunityID#
			and rwp.TriggerPoint <> 'pricebook'
			order by rwp.TriggerPoint desc <!--- PPB 2010/12/01 ordered so that on the oppedit form the subtotal promotions are displayed first followed by the promocode promotions (to keep them near the promocode entrybox) --->
		</cfquery>

		<cfreturn getAppliedPromotions>
	</cffunction>

	<!--- NJH 2011/12/02 - removed all the rights checks from the query and broke it out into another function, so that we could call a function to get all the
		opportunities that a partner has rights to. This query then uses those opportunities as a base. --->
	<cffunction access="public" name="getCustomerControllerOpportunities" hint="Returns the opportunities for a specified organisation for use on the Customer Controller tabs" returntype="query">
		<!---<cfargument name="reseller OrgID" required="yes" type="numeric">--->
		<cfargument name="organisationID" required="no" default="0" type="numeric" hint="The end customer organisation">
		<cfargument name="opportunityTypes" required="no" default="">	<!--- IMPORTANT NOTE: if you don't send opportunityTypes in you get all opps, if you send in opportunityTypes="ALL" you get all opps EXCEPT Leads (because as an afterthought we don't want to list customers that only had Leads in CustomerController... sorry!) --->
		<cfargument name="sortOrder" required="no" default="o.opportunityID">
		<cfargument name="showCurrentMonth" required="no" default="false">


		<!--- <cfset orgPrimaryContactPersonId = application.com.flag.getFlagData (flagid="KeyContacts Primary", entityID=organisationID).data>
		<cfif orgPrimaryContactPersonId eq "">
			<cfset orgPrimaryContactPersonId = -1>		<!--- use dummy value -1 just so it isn't a string --->
		</cfif>

		<!--- NB. there may be more than one PortalLeadManager --->
		<cfset qryPortalLeadManagers = application.com.flag.getFlagData (flagid = "PortalLeadManager")>
		<cfset portalLeadManagerPersonIds = "#ValueList(qryPortalLeadManagers.personId)#">

		<!--- NB. there may be more than one PartnerOrderingManager --->
		<cfset qryPartnerOrderingManagers = application.com.flag.getFlagData (flagid = "PartnerOrderingManager")>
		<cfset partnerOrderingManagerPersonIds = "#ValueList(qryPartnerOrderingManagers.data)#"> --->

		<!--- <cfset var partnerOppIDs = getOpportunitiesAPartnerHasRightsTo(argumentCollection=arguments)>
			NJH Sugar 448757 - changed to use query snippets rather than function above to improve performance.
			 --->
		<cfset var oppRightsQuerySnippet = application.com.rights.getRightsFilterQuerySnippet(entityType="opportunity",organisationID=arguments.organisationID,Alias="o")>
		<cfset var qryOpportunities = "">
		<cfset var hideOppStagesFromOppList = application.com.settings.getSetting("leadManager.hideOppStagesFromOppList")>
		<cfset var oppRightsJoin = oppRightsQuerySnippet.join>
		<cfset var oppRightsWhereClause = oppRightsQuerySnippet.whereClause>

		<cfquery name="qryOpportunities" datasource="#application.siteDataSource#">

				SELECT     o.opportunityID, o.expectedCloseDate, o.actualCloseDate, o.detail AS opportunityDescription, oppStage.opportunityStage, SUM(ISNULL(op.subtotal,ISNULL(o.budget,0))) AS TotalValue, o.currency,
				           'phr_Edit' AS EditButton,
					       month(o.expectedCloseDate) AS closeMonth,
					       year(o.expectedCloseDate) AS closeYear,
					       o.partnerSalesPersonId,
						   OppStatus.statusTextID,
						   OppType.OppTypeTextId
			FROM         opportunity AS o
			INNER JOIN organisation on o.entityid = organisation.organisationid
			INNER JOIN OppStage ON  OppStage.OpportunityStageID = o.stageID
			INNER JOIN OppType ON  OppType.OppTypeID = o.OppTypeID
			LEFT OUTER JOIN  opportunityProduct AS op ON o.opportunityID = op.OpportunityID
				LEFT OUTER JOIN OppStatus 	ON o.StatusID = OppStatus.statusID 							<!--- o.StatusID=0 to start so need OUTER join --->
			<!--- INNER JOIN location ON location.locationID = o.partnerLocationID and location.organisationid = #arguments.reseller OrgID# --->
			#preserveSingleQuotes(oppRightsJoin)#
			WHERE   1=1
			#preserveSingleQuotes(oppRightsWhereClause)#
			<cfif arguments.OrganisationID neq "0">
				AND	(o.entityid = #arguments.organisationID#)
	 		</cfif>
			<!---AND (#request.relaycurrentuser.personID# IN (o.partnerSalesPersonID, o.partnerSalesManagerPersonID) OR #request.relayCurrentUser.personID# = #orgPrimaryContactPersonId# <cfif partnerOrderingManagerPersonIds neq "">OR #request.relayCurrentUser.personID# IN(#partnerOrderingManagerPersonIds#)</cfif><cfif portalLeadManagerPersonIds neq ""> OR #request.relayCurrentUser.personID# IN(#portalLeadManagerPersonIds#)</cfif>)
			 --->
			<!--- <cfif partnerOppIDs.recordCount>
				and o.opportunityID  in ( <cf_queryparam value="#valueList(partnerOppIDs.opportunityID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfelse>
				and 1=0
			</cfif> --->
			 <cfif hideOppStagesFromOppList neq "">
				AND (OppStage.stageTextID  NOT IN ( <cf_queryparam value="#hideOppStagesFromOppList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
			</cfif>
			AND (OppStage.LiveStatus = 1)
				<cfif arguments.opportunityTypes eq "ALL">		<!--- ALL means Deals AND Renewals - NJH 2012/08/24 shouldn't really be used anymore.. have tried to deprecate it by passing in 'Deals,Renewals' which is what it original was for. --->
					AND (OppType.oppTypeTextID <> 'Leads')
				<cfelseif arguments.opportunityTypes neq "">
					and oppType.oppTypeTextID in (<cf_queryparam value="#arguments.opportunityTypes#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">)
				</cfif>
				<!--- <cfif arguments.opportunityTypes eq "NEW">
				AND (OppType.oppTypeTextID = 'Deals')
				<cfelseif arguments.opportunityTypes eq "RENEWAL">
				AND (OppType.oppTypeTextID = 'Renewal')
				<cfelseif arguments.opportunityTypes eq "ALL">			<!--- ALL means Deals AND Renewals --->
				AND (OppType.oppTypeTextID <> 'Leads')
			</cfif>	 --->

				<cfif showCurrentMonth>
					AND month(o.expectedCloseDate) =  <cf_queryparam value="#month(now())#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND year(o.expectedCloseDate) =  <cf_queryparam value="#year(now())#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				</cfif>

				<!--- 2013-05-29	YMA	Case 434462 We want to display Renewals that dont have assets in the renewals list too. --->
				<cfif arguments.opportunityTypes neq "ALL" and arguments.opportunityTypes neq "" and arguments.opportunityTypes neq "RENEWAL"> <!--- LID 6593 - NJH 2011/10/31 if no oppTypes passed through, we consider that as all.. so include renewals --->
				AND o.opportunityID
				<!--- <cfif arguments.opportunityTypes neq "RENEWAL"> --->
					NOT
				<!--- </cfif> --->
				IN (SELECT ISNULL(renewalOppID,0) FROM Asset)		<!--- the ISNULL is vital else no rows are returned if there is at least 1 asset with renewalOppID=NULL --->
			</cfif>
				GROUP BY o.opportunityID, o.expectedCloseDate, o.actualCloseDate, o.detail, oppStage.opportunityStage, o.currency, o.partnerSalesPersonId, OppStatus.statusTextID, OppType.OppTypeTextId
			ORDER BY <cf_queryObjectName value="#sortOrder#">
		</cfquery>

		<cfreturn qryOpportunities>
	</cffunction>

	<!--- NJH 2011/12/02 - Master Demo Enchancements Phase 3. a function to get all the opportunities that a partner has rights to. This was taken from getCustomerControllerOpportunities
		NJH 2016/04/27 Sugar 448757 - SQL performance. Join to flag tables rather than using in-statement.

		DO NOT USE THIS FUNCTION ANYMORE!!! Function to be used is rights.getRightsFilterQuerySnippet! Left here for backwards compatibility
	--->
	<cffunction name="getOpportunitiesAPartnerHasRightsTo" access="public" returnType="query">
		<!--- <cfargument name="reseller OrgID" required="yes" type="numeric"> --->
		<cfargument name="organisationID" required="no" default="0">

		<!--- <cfset var orgPrimaryContactPersonId = application.com.flag.getFlagData (flagid="KeyContactsPrimary", entityID=arguments.organisationID).data> --->
		<cfset var qryOpportunities = "">
		<!--- NB. there may be more than one PortalLeadManager
		<cfset var qryPortalLeadManagers = application.com.flag.getFlagData (flagid = "PortalLeadManager")>
		<cfset var portalLeadManagerPersonIds = ""> --->
		<!---<cfset var portalLeadManagerQuerySnippet = application.com.flag.getFlagQuerySnippets(textIDlist="PortalLeadManager")>
		 NB. there may be more than one PartnerOrderingManager
		<cfset var qryPartnerOrderingManagers = application.com.flag.getFlagData (flagid = "PartnerOrderingManager")>
		<cfset var partnerOrderingManagerPersonIds = "">
		<cfset var partnerOrderingManagerQuerySnippet = application.com.flag.getFlagQuerySnippets(textIDlist="PartnerOrderingManager")> --->

		<!--- <cfif qryPortalLeadManagers.recordCount>
			<cfset portalLeadManagerPersonIds = ValueList(qryPortalLeadManagers.personId)>
		</cfif>

		<cfif qryPartnerOrderingManagers.recordCount>
			<cfset partnerOrderingManagerPersonIds = ValueList(qryPartnerOrderingManagers.data)>
		</cfif> --->

		<!--- <cfif orgPrimaryContactPersonId eq "">
			<cfset orgPrimaryContactPersonId = 0>		<!--- use dummy value -1 just so it isn't a string --->
		</cfif> --->

		<cfset var oppRightsQuerySnippet = application.com.rights.getRightsFilterQuerySnippet(entityType="opportunity",Alias="o")>
		<cfset var oppRightsJoin = oppRightsQuerySnippet.join>
		<cfset var oppRightsWhereClause = oppRightsQuerySnippet.whereClause>

		<cfquery name="qryOpportunities" datasource="#application.siteDataSource#">
			SELECT o.opportunityID FROM opportunity AS o
				INNER JOIN organisation on o.entityid = organisation.organisationid
				#preserveSingleQuotes(oppRightsJoin)#
				<!--- INNER JOIN location ON location.locationID = o.partnerLocationID and location.organisationid = #arguments.resellerOrgID#
				left outer join (booleanFlagData bfd_PortalLeadManager with (noLock) inner join flag f_PortalLeadManager with (noLock) on f_PortalLeadManager.flagId = bfd_PortalLeadManager.flagId and f_PortalLeadManager.flagTextId = 'PortalLeadManager') on bfd_PortalLeadManager.entityID = #request.relayCurrentUser.personID#
				left outer join (integerFlagData int_PartnerOrderingManager with (noLock) inner join flag f_PartnerOrderingManager with (noLock) on f_PartnerOrderingManager.flagId = int_PartnerOrderingManager.flagId and f_PartnerOrderingManager.flagTextId = 'PartnerOrderingManager') on int_PartnerOrderingManager.data = #request.relayCurrentUser.personID#
				#preserveSingleQuotes(portalLeadManagerQuerySnippet.join)#
				#preserveSingleQuotes(partnerOrderingManagerQuerySnippet.join)# --->
			WHERE
				#preserveSingleQuotes(oppRightsWhereClause)#
				<!---(o.partnerSalesPersonID = #request.relaycurrentuser.personID#
					or o.partnerSalesManagerPersonID = #request.relaycurrentuser.personID#
					or #request.relayCurrentUser.personID# =  <cf_queryparam value="#orgPrimaryContactPersonId#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- <cfif partnerOrderingManagerPersonIds neq "">OR #request.relayCurrentUser.personID# IN(<cf_queryparam value="#partnerOrderingManagerPersonIds#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)</cfif>
				<cfif portalLeadManagerPersonIds neq ""> OR #request.relayCurrentUser.personID# IN(<cf_queryparam value="#portalLeadManagerPersonIds#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)</cfif> --->
					or bfd_PortalLeadManager.entityId is not null
					or int_PartnerOrderingManager.data is not null
				)--->
		</cfquery>

		<cfreturn qryOpportunities>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction name="onOppProductEdit" access="public" hint="Some functions to run when a product is added or deleted or changed in an opportunity">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="method" type="string" default="add">

		<cfset var comment="Special Price change: Product Added to Opportunity">
		<cfset var oppDetails = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID)>
		<cfset var oppSPQStatusTextID = application.com.opportunity.getOppPricingStatusID(oppPricingStatusID=oppDetails.oppPricingStatusID).statusTextID>

		<cfif oppSPQStatusTextID neq "" and oppSPQStatusTextID neq "StandardPricing">
			<cfif arguments.method eq "delete">
				<cfset comment="Special Price change: Product Removed from Opportunity">
			<cfelseif arguments.method eq "update">
				<cfset comment="Special Price change: Product Updated in Opportunity">
			</cfif>

			<cfset specialPriceChange(opportunityID=arguments.opportunityID,comments=comment)>
		</cfif>

		<cfset application.com.opportunity.refreshPromotions(opportunityID=arguments.opportunityID)>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
 	<cffunction name="removeProductFromOpportunity" access="public">
		<cfargument name="opportunityId" type="numeric" required="true">
		<cfargument name="productID" type="numeric" required="false">
		<cfargument name="oppProductID" type="numeric" required="false">

		<cfset var removeProduct = "">
		<cfset var whereClause = "">

		<cfsavecontent variable="whereClause">
			<cfoutput>
				opportunityID=#arguments.opportunityID#
				<cfif structKeyExists(arguments,"productID")>
					and productOrGroupID=#arguments.productID# and productOrGroup='P'
				</cfif>
				<cfif structKeyExists(arguments,"oppProductID")>
					and opportunityProductID=#arguments.oppProductID#
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfquery name="removeProduct" datasource="#application.siteDataSource#">
			update opportunityProduct set lastUpdated=GetDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID#,lastUpdatedByPerson=#request.relayCurrentUser.personID#
			where #preserveSingleQuotes(whereClause)#

			delete from opportunityProduct where #preserveSingleQuotes(whereClause)#
		</cfquery>

		<cfset onOppProductEdit(opportunityID=arguments.opportunityID,method="delete")>

	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction name="addProductToOpportunity" access="public" returntype="struct">
		<cfargument name="opportunityId" type="numeric" required="true">
		<cfargument name="productID" type="numeric" required="true">
		<cfargument name="quantity" type="numeric" default=1>
		<cfargument name="unitPrice" type="numeric" required="false">

		<cfset var insertProduct = "">
		<cfset var productStruct = getOppProductPrice(opportunityID=arguments.opportunityID,productID=arguments.productID)>  <!--- this price includes any PriceBook promotion discount  --->
		<cfset var productUnitPrice = 0>
		<cfset var illegalDuplicate = false>

		<cfif not structKeyExists(arguments,"unitPrice")>
			<cfset productUnitPrice = productStruct.unitPrice>
		<cfelse>
			<cfset productUnitPrice = arguments.unitPrice>
		</cfif>

		<cfif not application.com.settings.getSetting("leadManager.products.allowProductMultipleTimes")>
			<cfquery name="checkProduct" datasource="#application.siteDataSource#">
				select 1 from opportunityProduct where opportunityID=#arguments.opportunityID# and productOrGroupID = #arguments.productID# and productOrGroup = 'P'
			</cfquery>
			<cfif checkProduct.recordcount gt 0>
				<cfset illegalDuplicate = true>
			</cfif>
		</cfif>

		<cfif not illegalDuplicate>
			<cfquery name="insertProduct" datasource="#application.siteDataSource#">
				insert into opportunityProduct (opportunityID,productOrGroupID,productOrGroup,unitPrice,quantity,pricebookEntryID,createdBy,lastUpdatedBy,lastUpdatedByPerson) values (#arguments.opportunityID#,#arguments.productID#,'P',#productUnitPrice#,#arguments.quantity#,#productStruct.pricebookEntryID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personID#)
			</cfquery>

			<cfset onOppProductEdit(opportunityID=arguments.opportunityID,method="add")>
		</cfif>

		<cfset returnStruct = StructNew()>
		<cfset returnStruct.illegalDuplicate = illegalDuplicate>

		<cfreturn returnStruct>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction name="getOppProductBasePrice" access="public" returntype="struct">
		<cfargument name="productID" type="numeric" required="true">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="organisationID" type="numeric" required="false">
		<cfargument name="countryID" type="numeric" required="false" default="#request.relaycurrentuser.content.showForCountryID#">
		<cfargument name="currency" type="string" required="false">

		<!---
		if we are calling this routine from oppEdit we pass in an opportunityID (not country or currency or organisationID)
		if called from assetRenewal we send country and currency and a disti orgId
		 --->

		<cfscript>
			var result = {price = 0,currencySign = "",pricebookEntryID = 0,isDistiPrice = false};
			var getProductInfo = "";
			var oppTypeID=application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID).OPPTYPEID;
			var vendorOrganisationId = 0;
			var pricebookCampaignID = getpricebookCampaignID(oppTypeID=oppTypeID);
			var pricebookOrganisationId = 0;
			var getDistiOrganisationId = "";
		</cfscript>

		<!--- <cfif not structKeyExists(arguments,"opportunityID") and (not structKeyExists(arguments,"countryID") or not structKeyExists(arguments,"currency") or not structKeyExists(arguments,"organisationID"))>
			<cfreturn result>
		<cfelse> --->

			<!--- <cfset vendorOrganisationId = application.com.settings.getSetting("theClient.clientOrganisationId")>
			<!--- START: 2012/02/08	MS		NetGear - We would call the new fucntion getpricebookCampaignID thts an extention to core to accomodate for oppTypeID=4 thats DEMO --->

				<cfset oppTypeID = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID).OPPTYPEID>
				<!--- <cfset pricebookCampaignID = application.com.settings.getSetting("leadManager.pricebookCampaignID")> --->
				<cfset pricebookCampaignID = getpricebookCampaignID(oppTypeID=oppTypeID)>

			<!--- END: 2012/02/08	MS		NetGear --->
			<cfset pricebookOrganisationId = vendorOrganisationId>

			<cfif structKeyExists(arguments,"organisationID") and arguments.organisationID neq 0 >
				<cfset pricebookOrganisationId = arguments.organisationID>

			<!--- if an opportunity is passed, then we need to determine from the opportunity whether the reseller has been dealing with a distributor or not --->
			<cfelseif structKeyExists(arguments,"opportunityID")>
				<!--- NJH 2014/06/03 Connector - distiLocationId is now a locationID! --->
				<!--- BEWARE: the distiLocationID actually holds an OrganisationId --->
				<cfquery name="getDistiOrganisationId" dataSource="#application.siteDataSource#">
					SELECT     <!--- ISNULL(distiLocationID,0) AS distiLocationID, ---> l.organisationID
					FROM       opportunity o
								inner join location l on o.distiLocationID = l.locationID
					WHERE     (o.opportunityID = #arguments.opportunityID#)
				</cfquery>

				<cfif getDistiOrganisationId.recordcount gt 0>
					<cfset pricebookOrganisationId = getDistiOrganisationId.organisationID> <!--- BEWARE: the distiLocationID actually holds an OrganisationId --->
				</cfif>
			</cfif> --->
			<cfset oppPricebookID = getOppPricebook(opportunityID=arguments.opportunityID).pricebookID>

			<cfquery name="getProductInfo" dataSource="#application.siteDataSource#">
				select c.currencySign, pbe.unitPrice, pbe.pricebookEntryID
				from product p
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
				inner join pricebookEntry pbe on p.productID = pbe.productID
				inner join pricebook pb on pb.pricebookID = pbe.pricebookID
					<!--- <cfif structKeyExists(arguments,"opportunityID")>
					inner join opportunity o on pb.currency = o.currency and pb.countryID = o.countryID
					</cfif> --->
					inner join currency c on c.currencyISOCode = pb.currency
				where
					<!--- <cfif structKeyExists(arguments,"opportunityID")>
						o.opportunityID = #arguments.opportunityID#
					<cfelse>
						pb.currency = '#arguments.currency#' and pb.countryID = #arguments.countryID#
					</cfif> --->
					p.productID = #arguments.productID#
					and (p.campaignID = <cfqueryparam cfsqltype="cf_sql_numeric" value='#pricebookCampaignID#'/> or pb.campaignID= <cfqueryparam cfsqltype="cf_sql_numeric" value='#pricebookCampaignID#'/>)
					and p.deleted != 1
					and p.active = 1
					and pbe.deleted != 1						<!--- LID4648 wasn't checking if entry was deleted; i also added the active=1 to avoid future problems --->
					and pbe.active = 1
					and pb.pricebookID = #oppPricebookID#
                    and (p.hascountryScope = 0 or vcs.countryid is not null)
						<!--- (select top 1 pricebookID from pricebook pb2
						<cfif structKeyExists(arguments,"opportunityID")>
							inner join opportunity op on op.countryID = pb2.countryID and op.currency = pb2.currency
						</cfif>
							where pb2.startDate <= GetDate() and (pb2.endDate IS NULL OR DATEDIFF(day,GetDate(),pb2.endDate) >=0 )				<!--- use datediff to account for the time element when the enddate is today --->

								<cfif structKeyExists(arguments,"opportunityID")>
									and op.opportunityID = #arguments.opportunityID#
								<cfelse>
									and pb2.currency = '#arguments.currency#' and pb2.countryID = #arguments.countryID#
								</cfif>
								and pb2.deleted != 1
								and pb2.campaignID=#pricebookCampaignID#
								and pb2.organisationId=#pricebookOrganisationId#
						) --->
			</cfquery>

			<cfif getProductInfo.recordCount gt 0>
				<cfset result.price = getProductInfo.unitPrice>
				<cfset result.currencySign = getProductInfo.currencySign>
				<cfset result.pricebookEntryID = getProductInfo.pricebookEntryID>
				<cfif pricebookOrganisationId neq vendorOrganisationID>
					<cfset result.isDistiPrice = true>
				</cfif>
			<cfelse>
				<!--- if the disti's pricebook does not exist, then use the vendor's pricebook --->
				<cfif pricebookOrganisationId neq vendorOrganisationID>
					<cfset arguments.organisationID = vendorOrganisationID>
					<cfset result = getOppProductBasePrice(argumentCollection=arguments)>
					<cfset result.isDistiPrice = false>
				</cfif>
			</cfif>

		<!--- </cfif> --->

		<cfreturn result>

	</cffunction>


	<!--- PPB 2010/03/31 P-PAN002 --->
	<cffunction name="getOppProductPrice" access="public" returntype="struct">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="productID" type="numeric" required="true">

		<cfset var getProductInfo = getOppProductBasePrice(argumentCollection=arguments)>
		<cfset var unitPrice = 0>
		<cfset var basePrice = 0>
		<cfset var qryRWPromotions = "">
		<cfset var qryProduct = "">
		<cfset var oppDetails = "">
		<cfset var result = StructNew()>

		<cfset result.unitPrice = unitPrice>
		<cfset result.pricebookEntryID = getProductInfo.pricebookEntryID>
		<cfset result.isDistiPrice = getProductInfo.isDistiPrice>

		<cfif getProductInfo.pricebookEntryID neq 0> <!--- if we have a valid price.. can't use price neq 0, as price can be 0 in the pricebook. --->

			<cfset unitPrice=getProductInfo.price>
			<cfset basePrice=unitPrice>
			<cfset oppDetails = getOpportunityDetails(opportunityID=arguments.opportunityID)>

			<!--- if we're a tier2 reseller and the pricebook returned is not a disti pricebook, then just return the list price.. ie. don't apply
				any promotions; otherwise, carry on as normal --->
			<cfif not ((oppDetails.distiLocationID neq 0 and oppDetails.distiLocationID neq "") and not getProductInfo.isDistiPrice)>
				<cfset qryRWPromotions = application.com.opportunity.GetRWPromotions(triggerPoints='pricebook')>

				<cfloop query="qryRWPromotions">

					<cfquery name="qryProduct" dataSource="#application.siteDataSource#">
						SELECT     Product.ProductID
						FROM         Product INNER JOIN
						                      vRWPromotionProducts ON Product.ProductID = vRWPromotionProducts.ProductID
						WHERE 	(vRWPromotionProducts.RWPromotionID =  <cf_queryparam value="#qryRWPromotions.RWPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" > )
						AND 	(vRWPromotionProducts.ProductID = #arguments.ProductID#)

						<cfif qryRWPromotions.Visibility EQ 1>
							AND (EXISTS (SELECT 1
						                	FROM RecordRights rr
						                  	WHERE rr.entity = 'PROMOTION'
						                	AND rr.recordId =  <cf_queryparam value="#qryRWPromotions.RWPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
						                   	AND rr.UserGroupId IN (SELECT rg.Usergroupid
						       				FROM person per
						               		INNER JOIN rightsGroup rg ON rg.personid = per.personid
						             		WHERE per.personId =  <cf_queryparam value="#oppDetails.partnerSalesPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )))--must be the partners vis groups being checked not an internal users
			           	</cfif>
			           	<!--- Check the Country --->
			           	<cfif qryRWPromotions.Country EQ 1>
							AND (EXISTS (SELECT 1
						                	FROM CountryScope CS
						                  	WHERE cs.entity = 'Promotion'
						                	AND cs.entityId =  <cf_queryparam value="#qryRWPromotions.RWPromotionID#" CFSQLTYPE="CF_SQL_INTEGER" >
						                   	AND cs.countryId IN (SELECT o.countryid
						       				FROM opportunity o
						             		WHERE o.opportunityId = #arguments.OpportunityID#)))
			           	</cfif>

						<cfif qryRWPromotions.startdate neq "">
							AND GETDATE() >=  <cf_queryparam value="#CreateODBCDate(qryRWPromotions.startdate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
						</cfif>
						<cfif qryRWPromotions.enddate neq "">
							AND GETDATE()-1 <= <cf_queryparam value="#CreateODBCDate(qryRWPromotions.enddate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >	<!--- subtract 1 from date cos GETDATE() has a time element --->
						</cfif>
					</cfquery>

					<cfif qryProduct.recordCount gt 0>
						<cfswitch expression="#qryRWPromotions.RWPromotionTypeTextID#">
							<cfcase value="Percentage">
								<cfset unitPrice=unitPrice - ((unitPrice / 100) * qryRWPromotions.TotalPoints)>
							</cfcase>
							<cfcase value="Lump_Sum,Fixed">
								<cfset unitPrice=unitPrice - qryRWPromotions.TotalPoints>
							</cfcase>
						</cfswitch>
					</cfif>
				</cfloop>
			</cfif>

			<cfset result.unitPrice = unitPrice>
			<cfset result.basePrice = basePrice>
			<cfset result.unitPriceCurrency = getProductInfo.currencySign & ToString(unitPrice)>
			<cfset result.Currency = getProductInfo.currencySign>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- NYB LHID5122 --->
	<cffunction name="getOpportunityEndCustomerContacts" access="public" returntype="query">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="sortOrder" default="name">
		<!--- this method is called from oppEdit and customerContacts (the customercontroller contacts tab)... hence we return additional columns that are not used by the oppEdit dropdown --->

		<cfscript>
			var getContactsQry = "";
		</cfscript>

		 <!--- if not internal user, only get contacts that the partner has dealt with. This will mean those that are in opportunities that they have dealt with
			or if they are contacts that they have created
			NJH 2011/10/21 - LID 3874 - if the portal lead manager, then bring back all end contacts related to people at their organisation
		--->
		<cfquery name="getContactsQry" datasource="#application.siteDataSource#">
			select distinct p.OrganisationID, p.personID, p.firstname,p.lastname,
			p.firstname+' '+p.lastname as name,
			p.JobDesc, p.Email, p.OfficePhone AS PhoneNumber, 'Edit' AS EditButton
			from person p
			<cfif not request.relayCurrentUser.isInternal>
			inner join integermultipleflagdata imfd on p.personid=imfd.entityid and imfd.data
				<!--- NJH 2016/12/12 - JIRA PROD2016-2937 - get end customer contacts based on loc/org setting --->
				 <cfif application.com.flag.isFlagSetForCurrentUser(flagID='PortalLeadManager') or application.com.relayPLO.isPersonAPrimaryContact()>
					 <cfset var accountUniqueKey = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"locationID":"organisationID">
					 in (select personId from person where #accountUniqueKey#=#request.relayCurrentUser[accountUniqueKey]#)
				 <cfelse>
				 	=#request.relayCurrentUser.personID#
				 </cfif>
			inner join flag f on imfd.flagid=f.flagid and f.flagtextid='EndCustomerOppContacts'
			</cfif>
			where p.organisationID=#arguments.organisationID#
			order by <cf_queryObjectName value="#sortOrder#">
		</cfquery>

		<cfreturn getContactsQry>
	</cffunction>

	<!--- NJH 2010/04/27 P-PAN002
	Commented out as function now changed to use endCustomerContacts - function moved to Panda as they are still using it.
	<cffunction name="getOpportunityPartnerContacts" access="public" returntype="query">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="addPersonID" type="numeric" required="false" hint="PersonId that is not yet linked to opportunity or that was not created by partner, but that needs to be added to the query">
		<cfargument name="sortOrder" default="name">

		<!--- this method is called from oppEdit and customerContacts (the customercontroller contacts tab)... hence we return additional columns that are not used by the oppEdit dropdown --->

		<cfscript>
			var getContactsQry = "";
		</cfscript>

		<!--- if not internal user, only get contacts that the partner has dealt with. This will mean those that are in opportunities that they have dealt with
			or if they are contacts that they have created --->
		<!--- SSS 2010-08-18 changed the query so that main contact can be anyone that the partner has used before--->
		<cfquery name="getContactsQry" datasource="#application.siteDataSource#">
			<cfif structKeyExists(arguments,"addPersonID")>
				select  p.OrganisationID, p.personID, p.firstname, p.lastname, p.firstname+' '+p.lastname as name, p.JobDesc, p.Email, p.OfficePhone AS PhoneNumber,	'phr_Edit' AS EditButton
				from person
				inner join organisation o
					on o.organisationID = p.organisationID
				where p.organisationID=#arguments.organisationID#
					and p.personID = #arguments.addPersonID#
				union
			</cfif>
			select distinct p.OrganisationID, p.personID, p.firstname,p.lastname, p.firstname+' '+p.lastname as name, p.JobDesc, p.Email, p.OfficePhone AS PhoneNumber,	'phr_Edit' AS EditButton
			from person p left join
				opportunity opp on opp.contactPersonID = p.personID inner join
				organisation o on o.organisationID = p.organisationID
			where p.organisationID=#arguments.organisationID#
			<cfif not request.relayCurrentUser.isInternal>
			and (opp.contactPersonID in (select contactPersonID
										from opportunity opp inner join
										location l on opp.partnerlocationID = l.locationID inner join
										organisation o on l.organisationID = o.organisationID
										where o.organisationID = #request.relayCurrentUser.organisationID#)
			or (opp.partnerSalesPersonID = #request.relayCurrentUser.personID#) or (p.createdBy = #request.relayCurrentUser.userGroupId#)
			<!--- 2010/11/04	NAS		LID4530 - Matched Customer not appearing for the entering Partner --->
			<cfif isdefined("session.useThisPersonID")>
				or p.personid = #session.useThisPersonID#
			</cfif>)

			</cfif>
			<!---
			order by name
			select distinct p.OrganisationID, p.personID, p.firstname,p.lastname, p.firstname+' '+p.lastname as name, p.JobDesc, p.Email, p.OfficePhone AS PhoneNumber,	'phr_Edit' AS EditButton
			from person p left join opportunity opp
				on opp.contactPersonID = p.personID inner join organisation o
				on o.organisationID = p.organisationID
			where p.organisationID=#arguments.organisationID#
			and p.organisationID = #request.relayCurrentUser.organisationID#

				<cfif not request.relayCurrentUser.isInternal>
				and (
					(opp.partnerSalesPersonID = #request.relayCurrentUser.personID#) or (p.createdBy = #request.relayCurrentUser.userGroupId#)
					)
				</cfif>
				--->
			order by <cf_queryObjectName value="#sortOrder#">
		</cfquery>

		<cfreturn getContactsQry>
	</cffunction> --->


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction name="getUserRightsForOpportunity" access="public" returnType="struct">
		<cfargument name="opportunityID" required="true" type="numeric">

		<cfscript>
			var getOwnerQry = "";
			var qAllOpps = "";
			var result = {edit=false,view=false};
		</cfscript>

		<!--- assume that any internal user at the moment will have rights --->
		<cfif arguments.opportunityID eq 0 or request.relayCurrentUser.isInternal>
			<cfset result.edit = true>
			<cfset result.view = true>

		<cfelseif (not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn)>
			<!--- get all opps this reseller is allowed to see on the portal--->
			<!--- <cfset qAllOpps = getOpportunitiesAPartnerHasRightsTo(resellerorgID=request.relayCurrentUser.organisationid)> --->	<!--- deliberately don't send in opportunityTypes="ALL" so that we can access LEADS from My Opportunities --->
			<!--- NJH Sugar 448757 - changed to use query snippets rather than function above to improve performance. --->
			<cfset var oppRightsQuerySnippet = application.com.rights.getRightsFilterQuerySnippet(entityType="opportunity",Alias="o")>
			<cfset var oppRightsJoin = oppRightsQuerySnippet.join>
			<cfset var oppRightsWhereClause = oppRightsQuerySnippet.whereClause>

			<cfquery name="getOwnerQry">
				select 1 from opportunity o
					#preserveSingleQuotes(oppRightsJoin)#
				where o.opportunityID = #arguments.opportunityID#
					#preserveSingleQuotes(oppRightsWhereClause)#
			</cfquery>

			<cfif getOwnerQry.recordCount gt 0>
				<cfset result.edit = true>
				<cfset result.view = true>
			</cfif>

		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction name="getOpportunityProductCount" access="public" returntype="numeric" hint="Returns the number of products for an opportunity">
		<cfargument name="opportunityID" type="numeric">

		<cfscript>
			var getProductCountQry = "";
		</cfscript>

		<cfquery name="getProductCountQry" datasource="#application.siteDataSource#">
			select count(1) as oppProductCount from opportunityProduct
			where opportunityID = #arguments.opportunityID#
		</cfquery>
		<!--- 2010/12/02 PPB I was going to add tests for product.deleted<>1 and oppProducts.pricebookentryid<>0 so that the SP and ORDER buttons are greyed out but Gawain said we still want the user to be able to place the order --->

		<cfreturn getProductCountQry.oppProductCount>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction access="public" name="getOppVendorAccountManagers" returnType="query" hint="Retrieves foreign key data for vendorAccountManagerPersonID." validValues="true">
		<cfargument name="leadVendorOrgIDs" type="string" required="false" hint="used to filter result to Person in charge of account">
		<cfargument name="countryID" type="numeric" required="false">

		<cfscript>
			var getVendorAccountManagerPersonIDs = "";
		</cfscript>

		<!--- <cfif listfind(application. liveRelayApps,51)> --->
		<cfset vendorAcMngrFlagTextIDs = application.com.settings.getSetting("leadManager.vendorAcMngrFlagTextIDs")>
		<cfif application.com.relayMenu.isModuleActive(moduleID="SalesForce")>
			<cfset vendorAcMngrFlagTextIDs = listAppend(vendorAcMngrFlagTextIDs,"isSalesOperationsAdmin")>
		</cfif>

		<cfquery name="getVendorAccountManagerPersonIDs" datasource="#application.siteDataSource#">
			SELECT DISTINCT personid, firstName + ' ' + lastname + ' (' + ISOCode + ')' as personName <!--- 2015-01-13 DXC Case 443312 Added DISTINCT clause as multiple flags can cause user repetition--->
			from location l
				inner join person p with (noLock) on p.locationid = l.locationid
				inner join country c with (noLock) on l.countryid = c.countryid
				inner join booleanflagdata bfd with (noLock) on p.personid = bfd.entityid
				inner join flag f with (noLock) on bfd.flagid = f.flagid
			where flagtextid  in ( <cf_queryparam value="#vendorAcMngrFlagTextIDs#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )

			<cfif structKeyExists(arguments,"countryID") and application.com.settings.getSetting("leadManager.vendorAccountMngrCountryScoped")>
				AND l.countryID = #arguments.countryID#
			</cfif>
			<cfif structKeyExists(arguments,"leadVendorOrgIDs")>
				and p.organisationID  in ( <cf_queryparam value="#arguments.leadVendorOrgIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- defined in opportunityINI.cfm --->
			</cfif>
			order by personName
		</cfquery>

		<cfreturn getVendorAccountManagerPersonIDs>
	</cffunction>

	<!--- 2012/05/09 PPB P-LEX070  ALLOW vendorAccountManagerPersonID to be passed in --->
	<cffunction name="getOppPartners" access="public" returnType="query"  validValues="true">
		<cfargument name="countryId" type="numeric" required="true">
		<cfargument name="vendorAccountManagerPersonID" type="numeric" required="false">
		<cfargument name="excludeLocations" type="string" required="false">
		<cfargument name="term" type="string" required="false">
		<cfargument name="currentValue" type="string" required="false">

		<cfscript>
			var getPartnerLocations = "";
			var phr_opp_NoPartnerAssigned = "";
		</cfscript>

		<!--- <cf_translate phrases="phr_opp_NoPartnerAssigned"/> --->
		<cfset organisationAcMngrFlagTextIDs=application.com.settings.getSetting("leadManager.organisationAcMngrFlagTextIDs")>

		<cfquery name="getPartnerLocations" datasource="#application.siteDataSource#">
			<!--- <cfif structKeyExists(arguments,"showNullText") and arguments.showNullText>
			select NULL as locationID, '#phr_opp_NoPartnerAssigned#' as siteName, 1 as sortIndex
			UNION
			</cfif> --->
			SELECT distinct l.locationID, siteName +', '+ address4 +' ('+  c.ISOCode +')' as siteName, 2 as sortIndex
			FROM location l with (noLock)
				INNER JOIN country c  with (noLock)on c.countryID = l.countryID
				INNER JOIN person p  with (noLock) on p.locationid = l.locationid
			WHERE (l.OrganisationID IN
				(SELECT EntityID
					FROM BooleanFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid WHERE f.flagtextID  in ( <cf_queryparam value="#urldecode(application.com.settings.getSetting("leadManager.partnerOrgFlagList"))#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
					<cfif organisationAcMngrFlagTextIDs neq "" and ((StructKeyExists(arguments,"vendorAccountManagerPersonID") and arguments.vendorAccountManagerPersonID neq 0) OR (StructKeyExists(THIS,"vendorAccountManagerPersonID") and THIS.vendorAccountManagerPersonID neq 0))>
				AND l.OrganisationID IN
					(SELECT EntityID
							FROM IntegerFlagdata b
						INNER JOIN Flag f ON b.flagid = f.flagid
						WHERE f.flagtextID  in ( <cf_queryparam value="#urldecode(organisationAcMngrFlagTextIDs)#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
						<cfif StructKeyExists(arguments,"vendorAccountManagerPersonID")>
							and data = #arguments.vendorAccountManagerPersonID#
						<cfelse>
							and data =  <cf_queryparam value="#THIS.vendorAccountManagerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfif>
						)
			</cfif>
			AND l.countryID = #arguments.countryID#
			<cfif structKeyExists(arguments,"excludeLocations")>
				AND l.locationID not in (<cf_queryparam value="#arguments.excludeLocations#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">)
			</cfif>
			<cfif structKeyExists(arguments,"term")>
				<cfif not isNumeric(arguments.term)>
					and l.sitename  like  <cf_queryparam value="%#arguments.term#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					and l.locationID =  <cf_queryparam value="#arguments.term#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
			</cfif>
			)
			<cfif structKeyExists(arguments,"currentValue") and isNumeric(arguments.currentValue)>
				or l.locationID = <cf_queryparam value="#arguments.currentValue#"  CFSQLTYPE="CF_SQL_INTEGER">
			</cfif>
			ORDER BY sortIndex,sitename
		</cfquery>

		<cfreturn getPartnerLocations>
	</cffunction>



	<!--- NJH 2010/04/27 P-PAN002 --->
	<!--- 2013/11/21	YMA	Case 437627 dont pull back people flagged for deletion and tidy up function --->
	<!--- 2014-09-02			RPW	Add Lead Screen on Portal --->
	<cffunction name="getOppPartnerSalesPerson" access="public" hint="Retrieves foreign key data for partnerSalesPersonID." returnType="query" validValues="true">
		<cfargument name="locationID" type="numeric" required="true">

		<cfscript>
			var getPartnerSalesPersonIDs = "";
			var phr_opp_NoSalesPersonAssigned = "";
		</cfscript>

		<cfquery name="getPartnerSalesPersonIDs" datasource="#application.siteDataSource#">
			SELECT p.PersonID, isnull(p.FirstName,'') + ' ' + isnull(p.LastName,'') AS fullname, 2 AS sortIndex
			FROM Person p
			WHERE p.locationID = <cfqueryparam value="#arguments.locationID#" cfsqltype="CF_SQL_INTEGER" >
			AND p.PersonID NOT IN (SELECT bfd.entityID FROM BooleanFlagData bfd WHERE bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='DeletePerson').flagID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			ORDER BY sortIndex,fullname
		</cfquery>

		<cfreturn getPartnerSalesPersonIDs>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 --->
	<cffunction access="public" name="getOppDistiLocations" hint="Retrieves foreign key data for distiLocationID to the query object." validValues="true">
		<cfargument name="locationID" type="string" required="false" hint="The partner locationID - used for assigned distributors"> <!--- NJH 2014/05/13 type changed to string to allow empty strings through/ 2014-09-02 PPB Case 441543 removed type="numeric" in case an empty string is sent (partnerLocationId is null) --->
		<cfargument name="countryID" default="0">																				<!--- 2014-09-02 PPB snaglist 140/ Case 441543  removed type="numeric" for compatibility --->
		<cfargument name="filterInactive" type="boolean" default=false><!--- 2014-09-22 AHL Case 441630 Inactive Distributor should not be displayed in "Distributor" drop down --->

		<cfscript>
			var qryDistiLocationID = queryNew("name,ID,assignedDisti");
			var getOrganisationFromLocation = "";
			var getAssignedDisti = "";
			var distiLocFlagList = "";
			var sql = {whereClause="1=1",join=""};
		</cfscript>

		<cfset distiLocFlagList = application.com.settings.getSetting("leadManager.distiLocFlagList")>

		<!--- The assigned distributor method - get all locations at the assigned org of type disti --->
		<cfif distiLocFlagList eq "AssignDistributorOrgID">

			<cfif isNumeric(arguments.locationID)>
			<cfquery name="getOrganisationFromLocation" datasource="#application.siteDataSource#">
				select organisationID from location where locationID =  <cf_queryparam value="#arguments.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfif getOrganisationFromLocation.recordCount>
				<cfset getAssignedDisti = application.com.flag.getFlagData(flagId='AssignDistributorOrgID',entityID=getOrganisationFromLocation.organisationID,additionalColumns="")>
			</cfif>

				<cfsavecontent variable="sql.whereClause">
					<cfoutput>
						l.organisationID =  (<cf_queryparam value="#getAssignedDisti.data#" CFSQLTYPE="CF_SQL_INTEGER">)
					</cfoutput>
				</cfsavecontent>

				<cfset sql.whereClause = "l.organisationID = #getAssignedDisti.data#">
			</cfif>

		<!--- not assigned distributors, but pick from a list of distributors --->
		<cfelseif distiLocFlagList neq "" and application.com.flag.doesFlagExist (flagID=listFirst(distiLocFlagList))>  <!--- 2012/03/19 PPB P-LEX070 defend against non-existence of the flag --->
				<cfsavecontent variable="sql.join">
					<cfoutput>
						inner join booleanFlagdata bfd on bfd.entityID = l.locationID
						inner join vFlagDef locFlags on locFlags.flagID = bfd.flagId and locFlags.entityTable='location' and flagTextID in (<cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
					</cfoutput>
				</cfsavecontent>

				<!--- Begin 2014-09-22 AHL Case 441630 Inactive Distributor should not be displayed in "Distributor" drop down
						WAB 2015-01-13 modified while merging into PartnerCloud branch
				--->
				<cfif arguments.filterInactive >
					<cfset sql.whereClause &= "AND o.active = 1 AND l.active = 1">
				</cfif>
				<!--- End 2014-09-22 AHL Case 441630 Inactive Distributor should not be displayed in "Distributor" drop down --->

		</cfif>

		<cfquery name="qryDistiLocationID">
			SELECT locationID as ID,
				sitename+' ('+isNull(case when len(ltrim(rtrim(address4))) = 0 then null else address4+', 'end ,'')
							+isNull(case when len(rtrim(ltrim(postalCode))) = 0 then null else postalCode+', ' end ,'')+c.isocode+')' as name,
				0 as assignedDisti
			FROM
				location l
				inner join country c on l.countryID = c.countryID
				inner join organisation o on o.organisationid = l.organisationid   <!--- WAB 2015-01-13 added join to organisation so can test o.active --->
				#preserveSingleQuotes(sql.join)#
			where #preserveSingleQuotes(sql.whereClause)#
				and l.countryID =  (<cf_queryparam value="#arguments.countryID#" CFSQLTYPE="CF_SQL_INTEGER">)
				and l.accountTypeID =  <cf_queryparam value="#application.organisationType.distributor.organisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER">

			order by sitename
		</cfquery>

		<cfreturn qryDistiLocationID>
	</cffunction>


	<!--- NJH 2010/04/27 P-PAN002 -
		NJH 2014/05/19 now expect distiLocationID to be a locationId --->
	<!--- 2014/10/01	YMA	Prevent function from erroring if distiLocationID is passed in empty as this now binds on load from opportunityDomain.xml --->
	<cffunction access="public" name="getOppDistiSalesPerson" hint="Retrieves foreign key data for distiSalesPersonID THIS.qDistiSalesPersonID to the query object." validValues="true">
		<cfargument name="distiLocationID" required="true">		<!--- 2014-11-06 AHL Case 442515 Nulls in DistiLocationID on Creation. 'type="numeric"' was causing problems with NULLs through queries  --->

		<!--- <cfargument name="showNullText" type="boolean" required="false"> --->
		<!--- START 2014-11-06 AHL Case 442515 Nulls in DistiLocationID on Creation. 'type="numeric"' was causing problems with NULLs through queries  --->
		<cfif not isNumeric(arguments.distiLocationID)>
			<cfset arguments.distiLocationID=0>
		</cfif>
		<!--- END 2014-11-06 AHL Case 442515 Nulls in DistiLocationID on Creation. 'type="numeric"' was causing problems with NULLs through queries  --->

		<cfscript>
			var getDistiSalesPersonID = "";
			var qryFlagGroup = "";
			var phr_opp_NoDistiSalesPersonAssigned = "";
		</cfscript>

		<!--- <cfset distiLocFlagList = application.com.settings.getSetting("leadManager.distiLocFlagList")>

		<cfquery name="qryFlagGroup" datasource="#application.siteDataSource#">
			select distinct entitytypeid from flaggroup fg inner join flag f on fg.flagGroupid = f.flagGroupid  WHERE f.flagtextID  in ( <cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		</cfquery> --->

		<!--- <cf_translate phrases="phr_opp_NoDistiSalesPersonAssigned"/> --->

		<cfquery name="getDistiSalesPersonID" datasource="#application.siteDataSource#">
			<!--- <cfif structKeyExists(arguments,"showNullText") and arguments.showNullText>
			select 0 as PersonID, '#phr_opp_NoDistiSalesPersonAssigned#' as fullname, 1 as sortIndex
			UNION
			</cfif> --->
			Select PersonID, FirstName + ' ' + LastName as fullname
			FROM Person
			WHERE
			<cfif isNumeric(arguments.distiLocationID)>
			<!--- <cfif qryFlagGroup.recordcount eq 0 or qryFlagGroup.entitytypeid eq 1> --->
				locationID
			<!--- <cfelse>
				organisationID
			</cfif> --->
				= <cf_queryparam value="#arguments.distiLocationID#" CFSQLTYPE="cf_sql_integer" >
			<cfelse>
				1=0
			</cfif>
			ORDER BY fullname
		</cfquery>

		<cfreturn getDistiSalesPersonID>

	</cffunction>


	<cffunction name="getOppPricebook" access="public" hint="Returns the pricebook for a given opportunity" returntype="query" validValues="true">
		<cfargument name="opportunityID" type="numeric" required="true">
		<!--- <cfargument name="countryID" type="numeric" required="false">
		<cfargument name="currency" type="string" required="false"> --->

		<!---
		this function returns:
		an empty query (with the correct columns defined) if an invalid combination of arguments is sent in (you need opportunityID OR (countryID AND currency)) else
		the pricebook that is associated with the opportunity sent in (if it has any products) else
		the appropriate distributor pricebook (for the currency and country) if there is as disti asscociated with the opp and that disti has a pricebook else
		the appropriate vendor pricebook (for the currency and country)
		 --->

		<!---
		WARNING: there may be a userfiles version of this function
		 --->

		<cfscript>
			var getOpportunityPricebook = "";
			var getOpportunityData = "";
			var getDummyPricebook = "";
			var pricebookExpired = 0;
			var vendorOrganisationId = 0;
			var oppTypeID = 0;
		</cfscript>

		<cfset vendorOrganisationId = application.com.settings.getSetting("theClient.clientOrganisationId")>

		<!--- surely we could force the main query to return no rows?; but i don't dare disturb it now --->
		<!--- <cfif not structKeyExists(arguments,"opportunityID") and (not structKeyExists(arguments,"countryID") or not structKeyExists(arguments,"currency"))>
			<!--- if none of the arguments exist, then return nothing --->
			<cfquery name="getDummyPricebook" datasource="#application.siteDataSource#">
				select top 1 *,0 as pricebookExpired from pricebook where 1=0
			</cfquery>
			<cfreturn getDummyPricebook>
		</cfif> --->

		<!--- NJH 2014/05/19 changed to get organisationID from distiLocationId. distiLocationID now holds locationIDs!! --->
		<cfquery name="getOpportunityData" datasource="#application.siteDataSource#">
			select top 1 l.organisationID as distiOrganisationID, o.opportunityID, o.countryID, currency, pricebookID
			from opportunity o
				left join location l on l.locationID = o.distiLocationID
				left join opportunityProduct op on o.opportunityID = op.opportunityID
				left join pricebookEntry pbe on op.pricebookEntryID = pbe.pricebookEntryID
			where
				<cfif structKeyExists(arguments,"opportunityID")>
					o.opportunityID	= #arguments.opportunityID#
				<cfelseif structKeyExists(arguments,"countryID") and structKeyExists(arguments,"currency")>
					o.countryID =  <cf_queryparam value="#arguments.countryID#" CFSQLTYPE="cf_sql_integer" > and o.currency =  <cf_queryparam value="#arguments.currency#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfelse>
					1=0
				</cfif>
		</cfquery>

		<cfif getOpportunityData.pricebookID neq "">
			<cfquery name="getOpportunityPricebook" datasource="#application.siteDataSource#">
				select top 1 *, 0 as pricebookExpired
				from pricebook pb
				where pb.pricebookID =  <cf_queryparam value="#getOpportunityData.pricebookID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		<cfelse>

			<cfif getOpportunityData.distiOrganisationID neq "" and getOpportunityData.distiOrganisationID neq 0>
				<cfset pricebookOrganisationID = getOpportunityData.distiOrganisationID>
				<cfset usingDistiPricebook = true>
			<cfelse>
				<cfset pricebookOrganisationID = vendorOrganisationId>
				<cfset usingDistiPricebook = false>
			</cfif>

			<!---START: 2012/02/08		MS		Extending core so that oppTypeID=4 DEMO can have their own pricebooks alongside regular pricebooks being used for deal reg--->
			<cfset oppTypeID = application.com.opportunity.getOpportunityDetails(opportunityID=getOpportunityData.OPPORTUNITYID).OPPTYPEID>
			<cfset getOpportunityPricebook = getPricebook(currency=getOpportunityData.currency,countryID=getOpportunityData.countryID,organisationID=pricebookOrganisationId,oppTypeID=oppTypeID)>
			<!--- END: 2012/02/08		MS  	Extending core for oppType DEMO--->

			<cfif usingDistiPricebook and getOpportunityPricebook.recordcount eq 0>			<!--- if we're using a disti but there is no pricebook for that disti then use the vendor pricebook --->
				<!--- 2010/06/23			NAS P-CLS002 --->
				<cfset getOpportunityPricebook = getPricebook(currency=getOpportunityData.currency,countryID=getOpportunityData.countryID,organisationID=vendorOrganisationId)>
			</cfif>
		</cfif>

		<cfif getOpportunityPricebook.recordCount eq 1>
			<!--- 2010/12/03 PPB extend to return whether the pricebook has expired  --->
			<cfif (getOpportunityPricebook.EndDate eq "" OR getOpportunityPricebook.EndDate gte DateFormat(now(),"yyyy-mm-dd")) AND NOT getOpportunityPricebook.deleted >
				<cfset pricebookExpired = 0>		<!--- set explicitly to avoid reversing the logic of the existing code --->
			<cfelse>
				<cfset pricebookExpired = 1>
			</cfif>

			<cfset querySetCell(getOpportunityPricebook,"pricebookExpired",pricebookExpired)>

			<!--- When importing pricebooks from salesforce, we append the currency and country to the salesForceId. So, to get the original value, we need to get the first part of the ID --->
			<!---<cfset querySetCell(getOpportunityPricebook,"crmPricebookID_orig",listFirst(getOpportunityPricebook.crmPricebookID,"_"))>--->
		</cfif>

		<cfreturn getOpportunityPricebook>
	</cffunction>


	<cffunction name="getPricebook" access="public" hint="Returns the pricebook for a given currency,country,organisation combination" returntype="query">
		<cfargument name="currency" type="string" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="oppTypeID" type="numeric" required="false" default="0"> <!--- 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->

		<cfset var pricebookCampaignID = getpricebookCampaignID(oppTypeID=arguments.oppTypeID)>
		<cfset var defaultPricebookId = application.com.settings.getSetting("leadManager.defaultPricebook")> <!--- the default pricebook to use should a country/currency pricebook not be found. --->
		<cfif defaultPricebookId eq "" or arguments.organisationID neq application.com.settings.getSetting("theClient.clientOrganisationId")>
			<cfset defaultPricebookId = 0>
		</cfif>
		<cfset var getOpportunityPricebook = "">

		<!--- END: 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->
		<cfquery name="getOpportunityPricebook" datasource="#application.siteDataSource#">
			select top 1 *, 0 as pricebookExpired
			from pricebook pb
			where pb.startDate <= GetDate() and (pb.endDate IS NULL OR DATEDIFF(day,GetDate(),pb.endDate) >=0 )				<!--- use datediff to account for the time element when the enddate is today --->
			and pb.deleted != 1
			and pb.campaignID =  <cf_queryparam value="#pricebookCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and isNull(pb.active,1) = 1
			and
				(
					(pb.currency =  <cf_queryparam value="#arguments.currency#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and pb.countryID = #arguments.countryID#
						and pb.organisationId=#arguments.organisationID#
					)
				or
					(pricebookID = <cf_queryparam value="#defaultPricebookId#" cfsqltype="cf_sql_integer">)
				)
			order by case when pricebookID =  <cf_queryparam value="#defaultPricebookId#" CFSQLTYPE="cf_sql_integer" > then 1 else 0 end
		</cfquery>

		<cfreturn getOpportunityPricebook>
	</cffunction>


	<cffunction access="public" name="getProduct" hint="Retrieves the Asset Product Info" returntype="query">
		<cfargument name="productID" required="no" default="0" type="numeric">
		<cfargument name="SKU" required="no" default="">
		<cfargument name="countryID" required="no" default="#request.relaycurrentuser.content.showForCountryID#" type="numeric">

		<!--- get the Asset Product info --->
		<CFQUERY NAME="qryProduct" DATASOURCE="#application.siteDataSource#">
			SELECT top 1 p.productID, p.renewalSKU, p.description
			FROM product p
            left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
			WHERE 1=1
			<cfif arguments.productID neq 0>
				AND productID = #arguments.productID#
			</cfif>
			<cfif trim(arguments.SKU) neq ''>
				AND SKU =  <cf_queryparam value="#arguments.SKU#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			<!--- AND p.countryID in (0,#arguments.countryid#) --->
            and (p.hascountryScope = 0 or vcs.countryid is not null)
			order by vcs.countryID desc
		</CFQUERY>

		<cfreturn qryProduct>
	</cffunction>


	<cffunction name="buildOppMergeStruct" access="public" returnType="struct">
		<cfargument name="opportunityId" type="numeric">
		<cfargument name="getProductInfo" type="boolean" default="false">

		<!---
			PPB 2010-06-14: at time of writing this method is used for orderConfirmationEmail and OrderPaymentFailed emails which display opp product details
			there is an alternative mechanism for populating a opp email mergeStruct (by including opportunityEmailMergeBuilder.cfm) which collects different details and no products ; ideally the 2 techniques would be merged with switches dictating what is built
		--->

		<cfscript>
			var mergeStruct = structNew();
			var getOppProducts = ""; 						// 2014-10-06	WAB/AHL Case 441992 var'ing
			var oppDetails = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID);

			mergeStruct.opportunityID = arguments.opportunityID;
			mergeStruct.details = oppDetails.detail;
			mergeStruct.currency = oppDetails.currency;
		</cfscript>


		<!--- START: 2010/05/28 AJC LID 3486 Order Approval email - no customer Org or customer contact merge fields displayed --->
		<cfquery name="getMainContactInfo" datasource="#application.siteDatasource#">
			select firstname+' '+lastname as mainContact, organisationName from person
			inner join organisation on person.organisationID = organisation.organisationID
			where personId =  <cf_queryparam value="#oppDetails.contactPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>


		<cfset mergeStruct.maincontact=getMainContactInfo.mainContact>
		<cfset mergeStruct.organisationname=getMainContactInfo.organisationName>


		<cfif getProductInfo>
			<!--- END: 2010/05/28 AJC LID 3486 Order Approval email - no customer Org or customer contact merge fields displayed --->
			<cfquery name="getOppProducts" datasource="#application.siteDataSource#">
				select p.description,op.quantity,op.unitPrice,op.subtotal,
					(op.unitPrice*op.quantity)-op.subTotal as discount
				from opportunityProduct op
					inner join product p on op.productOrGroupID = p.productID and op.productOrGroup = 'P'
				where opportunityID=#arguments.opportunityID#
			</cfquery>

			<cfset mergeStruct.opportunityProducts = getOppProducts>
			<cfset mergeStruct.grandTotal = arraySum(listToArray(valueList(getOppProducts.subTotal)))>
		</cfif>

		<cfreturn mergeStruct>
	</cffunction>


	<cffunction name="approveOpportunity" access="public" returnType="boolean">
		<cfargument name="opportunityId" type="numeric">

		<cfset var updatedOk = false>

		<cftransaction>
			<cfquery datasource="#application.siteDataSource#">
				UPDATE opportunity
				SET resellerApprovalPersonID=#request.relaycurrentuser.personid#,
				resellerApprovalDate = GETDATE()
				WHERE opportunityId = #arguments.opportunityId#
			</cfquery>

			<cfset updatedOk = true>
		</cftransaction>

		<cfreturn updatedOk>
	</cffunction>


	<cffunction name="processOpportunity" access="public" returnType="boolean">
		<cfargument name="opportunityId" type="numeric">

		<cfscript>
			var updatedOk = false;
			var oppDetails = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID);
		</cfscript>

		<cftransaction>
			<!---<cfset mergeStruct = application.com.opportunity.buildOppMergeStruct(opportunityID=arguments.opportunityID, getProductInfo=true)>--->
			<cfset mergeStruct = getOpportunityMergeFields(opportunityID=arguments.opportunityID)>
			<cfset application.com.email.sendEmail(personId=oppDetails.partnerSalesPersonID,emailTextID="orderConfirmationEmail",mergeStruct=mergeStruct)>

			<cfset application.com.flag.setBooleanFlag(entityID=arguments.opportunityID,flagTextID="SalesForceTransferOK")>
			<cfset updatedOk = true>
		</cftransaction>

		<cfreturn updatedOk>
	</cffunction>
	<!--- ************************************************************************************************ --->
	<!--- START: 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->
	<cffunction name="getpricebookCampaignID">
		<cfargument name="oppTypeID" 	type="numeric" 	required="false">

		<cfreturn  application.com.settings.getSetting("leadManager.pricebookCampaignID")>

	</cffunction>
	<!--- END: 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->
	<!--- ************************************************************************************************ --->
	<cffunction name="getOppProductGroups" access="public" returntype="query">
		<cfargument name="organisationID" 	type="numeric" 	required="false">
		<cfargument name="countryID" 		type="numeric" 	required="false" default="#request.relaycurrentuser.content.showForCountryID#">
		<cfargument name="currency" 		type="string" 	required="false">
		<cfargument name="opportunityID" 	type="numeric" 	required="true">

		<!---
		if we are calling this routine from oppEdit we pass in an opportunityID (not country or currency or organisationID)
		if called from assetRenewal we send country and currency and a disti orgId

		NJH 2013/02/22 Get the translated title for product groups
		 --->

		<cfscript>
			var result = structNew();
			var vendorOrganisationId = 0;
			var pricebookOrganisationId = 0;
			var pricebookCampaignID = 0;
			var productGroupTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productGroup", phraseTextID = "title",baseTableAlias="pg");
			var getDistiOrganisationId = "";

			result.ProductGroupID= 0;
			result.Description = "";
		</cfscript>

		<cfset opportunityDetails = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID)>
		<!--- <<cfset arguments.countryID = opportunityDetails.countryID>
		<cfset arguments.currency = opportunityDetails.currency>

		cfif not structKeyExists(arguments,"opportunityID") and (not structKeyExists(arguments,"countryID") or not structKeyExists(arguments,"currency") or not structKeyExists(arguments,"organisationID"))>
			<cfreturn result>
		<cfelse> --->

			<!--- START: 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->
			<cfset pricebookCampaignID = getpricebookCampaignID(oppTypeID=opportunityDetails.oppTypeID)>
			<!--- END: 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->

			<!--- <cfset vendorOrganisationId = application.com.settings.getSetting("theClient.clientOrganisationId")>
			<cfset pricebookOrganisationId = vendorOrganisationId> --->

			<!--- <cfset pricebookCampaignID = application.com.settings.getSetting("leadManager.pricebookCampaignID")> --->


			<!--- <cfif structKeyExists(arguments,"organisationID") and arguments.organisationID neq 0 >
				<cfset pricebookOrganisationId = arguments.organisationID>

			<!--- if an opportunity is passed, then we need to determine from the opportunity whether the reseller has been dealing with a distributor or not --->
			<cfelseif structKeyExists(arguments,"opportunityID")>
				<!--- BEWARE: the distiLocationID actually holds an OrganisationId --->
				<cfquery name="getDistiOrganisationId" dataSource="#application.siteDataSource#">
					SELECT     <!--- ISNULL(distiLocationID,0) AS distiLocationID --->l.organisationID
					FROM       opportunity
							inner join location l on opportunity.distiLocationID = l.locationID
					WHERE     (opportunity.opportunityID = #arguments.opportunityID#)
				</cfquery>

				<cfif getDistiOrganisationId.recordcount gt 0>
					<!--- <cfif getDistiOrganisationId.distiLocationID neq 0> --->
						<cfset pricebookOrganisationId = getDistiOrganisationId.organisationID> <!--- BEWARE: the distiLocationID actually holds an OrganisationId --->
					<!--- NJH 2015/08/03 - why is end customer org the pricebook organisationID???? pricebook organisations are only distis or the vendor org - commented this out.
					<cfelse>
						<cfset pricebookOrganisationId = opportunityDetails.entityID> --->
					<!--- </cfif> --->
				</cfif>
			</cfif> --->

			<!--- START: 2012/02/07		MS/WAB		this function is extended to accept oppTypeID to ensure only relavent pricebooks are returned. --->

				<!--- <cfset pricebookID = getPricebook(currency='#arguments.currency#',countryID=#arguments.countryID#,organisationID=#pricebookOrganisationId#).pricebookID> --->
			<cfset pricebookID = getOppPricebook(opportunityID=arguments.opportunityID).pricebookID>

			<!--- END: 2012/02/07		MS/WAB		New function to keep getOppProductGroups function maintainable --->

			<cfif pricebookID neq "">
				<!--- probably the pricebook table isn't required in this query but i won't change it now cos pb.organisationid is being selected PPB @ 2010-05-13 --->
				<cfquery name="getProductGroupInfo" dataSource="#application.siteDataSource#">
					select distinct pg.productgroupID,
						#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)# as description,
						pb.pricebookID, pb.organisationid
					from product p with (noLock)
                    left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
					inner join pricebookEntry pbe with (noLock) on p.productID = pbe.productID
					inner join pricebook pb with (noLock) on pb.pricebookID = pbe.pricebookID
					inner join currency c with (noLock) on c.currencyISOCode = pb.currency
					inner join productgroup pg with (noLock) on pg.productgroupid = p.productgroupid
					#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.join)#
					where <!--- NJH 2015/08/05 Commented out this bit of the where clause as we already have this in the pricebook query. In this case, we want to get a default pricebook which may not actually match this criteria.
						pb.currency =  <cf_queryparam value="#arguments.currency#" CFSQLTYPE="CF_SQL_VARCHAR" >  and pb.countryID = #arguments.countryID#
						and  --->
						p.campaignID =  <cf_queryparam value="#pricebookCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and p.deleted != 1
						and p.active = 1
						and pb.pricebookID =  <cf_queryparam value="#pricebookID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and isNull(pg.active,1) = 1				<!--- 2011/10/03 PPB NET005 product group must be active  --->
						and isNull(pbe.active,1) = 1			<!--- 2012/08/22 NAS 430196 Deal Reg Form does not ignore Product Groups with InActive products --->
				   		and pbe.deleted != 1					<!--- 2012/08/31 NAS 430196 Deal Reg Form does not ignore Product Groups with InActive products --->
                        and (p.hascountryScope = 0 or vcs.countryid is not null)
				   order by #preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)#
				</cfquery>

			</cfif>

			<cfif pricebookID neq "" and getProductGroupInfo.recordCount gt 0>
				<cfset result = getProductGroupInfo>
			<cfelse>
				<!--- if the distis pricebook does not exist, then use the vendor's pricebook --->
				<cfif pricebookOrganisationId neq vendorOrganisationId>
					<cfset arguments.organisationID = vendorOrganisationId>
					<cfset result = getOppProductGroups(argumentCollection=arguments)>
				<cfelse>
					<cfset result = getProductGroupInfo>
				</cfif>
			</cfif>

		<!--- </cfif> --->

		<cfreturn result>

	</cffunction>

	<cffunction name="placeOpportunityOrder" type="public">
		<cfargument name="opportunityID" type="numeric" required="true">
	</cffunction>


	<cffunction name="getOppProductsTotals" access="public" returntype="struct" >
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfset var returnStruct = StructNew()>
		<cfset var qryGetOppProductsTotals = "">

		<cfquery name="qryGetOppProductsTotals" datasource="#application.siteDataSource#">
			select sum(op.unitPrice*op.quantity) as subTotal, sum(op.subTotal) as total, c.currencySign,
				sum((ISNULL(op.promotionPrice,op.unitPrice) - op.specialPrice)*op.quantity) as specialPriceDiscount,
				ops.oppPricingStatus, ops.statusTextID,o.currency
			from opportunityProduct op with(noLock) inner join opportunity o with (noLock)
				on op.opportunityID = o.opportunityID inner join currency c with (noLock)
				on c.currencyISOCode = o.currency left join oppPricingStatus ops with (noLock)
				on o.oppPricingStatusID = ops.oppPricingStatusID
			where o.opportunityID = #arguments.opportunityID#
			group by c.currencySign,o.currency,ops.oppPricingStatus,ops.statusTextID
		</cfquery>

		<cfset returnStruct.query = qryGetOppProductsTotals>

		<cfreturn returnStruct>
	</cffunction>

	<cffunction name="getCrossSellProducts" access="public" hint="Returns the discount that a product has been given for a given opportunity and promotion" output="false">
		<cfargument name="opportunityID" required="yes" type="numeric">

		<cfset var qryCrossSellProducts = "">

<!--- for each oppProduct of the opportunity, look at the CrossSellSKU of the related product row and get the product details of the CrossSellSKU (if it isn't already on the opp)
	  NB. the column CrossSellSKU on the Product table can contain a comma delimited list of SKUs --->
		<cfquery name="qryCrossSellProducts" datasource="#application.siteDataSource#">
			SELECT p2.ProductID, p2.Description, p2.ProductGroupId, p2.ProductModelID, p2.MinSeats, p2.MaxSeats
			FROM opportunityProduct op
			INNER JOIN Product p1 ON op.ProductORGroupID = p1.ProductID and op.productOrGroup = 'P'
			INNER JOIN product p2 ON dbo.listfind(p1.crossSellSKU,p2.sku) <> 0
			WHERE p1.crossSellSKU IS NOT NULL
			AND   op.OpportunityID = #arguments.opportunityID#
			AND p2.ProductID NOT IN(SELECT ProductORGroupID FROM opportunityProduct WHERE OpportunityID = #arguments.opportunityID# and productOrGroup = 'P')
		</cfquery>

		<cfreturn qryCrossSellProducts>
	</cffunction>


	<cffunction name="getOpportunitiesAwaitingApproval" access="public" hint="Returns the discount that a product has been given for a given opportunity and promotion" output="false">
		<cfargument name="sortOrder" required="no" type="string" default="oppExpectedCloseDate">

		<cfset var oppRightsQuerySnippet = application.com.rights.getRightsFilterQuerySnippet(entityType="opportunity",Alias="o")>
		<cfset var oppRightsJoin = oppRightsQuerySnippet.join>
		<cfset var oppRightsWhereClause = oppRightsQuerySnippet.whereClause>

		<cfquery name="qryOpportunities" datasource="#application.siteDataSource#">
			SELECT     opportunity.opportunityID, organisation.OrganisationName AS CustomerOrganisationName, Person.FirstName + ' ' + Person.LastName AS ContactName,
			           opportunity.detail AS description, opportunity.expectedCloseDate AS oppExpectedCloseDate, opportunity.currency, SUM(vOppValue.totalValue) AS TotalValue,
			           'phr_View' AS ViewButton
			FROM        opportunity
						INNER JOIN organisation on organisation.organisationID = opportunity.entityID
						INNER JOIN oppStage on oppStage.opportunityStageID = opportunity.stageID
			            LEFT OUTER JOIN Person ON opportunity.contactPersonID = Person.PersonID
			            LEFT OUTER JOIN vOppValue ON opportunity.opportunityID = vOppValue.OpportunityID
			            #preserveSingleQuotes(oppRightsJoin)#
			WHERE    oppStage.stageTextID='oppStageAwaitingApproval'
					#preserveSingleQuotes(oppRightsWhereClause)#
			GROUP BY opportunity.opportunityID, organisation.OrganisationName, Person.FirstName + ' ' + Person.LastName, opportunity.detail, opportunity.expectedCloseDate, opportunity.currency
			ORDER BY <cf_queryObjectName value="#sortOrder#">
		</cfquery>

		<cfreturn qryOpportunities>
	</cffunction>

	<!--- 2009/11/09 - AJC - P-LEX039 - Get the list of competitors and types --->
		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Get the list of competitors
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProductCompetitors" returntype="query" hint="Gets all the competitors">

		<cfset var qry_get_ProductCompetitors = "">

		<cfquery name="qry_get_ProductCompetitors" datasource="#application.siteDataSource#">
			select * from ProductCompetitor pc
			order by CompetitorName
		</cfquery>

		<cfset THIS.qProductCompetitors = qry_get_ProductCompetitors>
		<cfreturn qry_get_ProductCompetitors/>
	</cffunction>

	<cffunction access="public" name="getproductCompetitorProductType" returntype="query" hint="Gets all the competitors product types">

		<cfset var qry_get_productCompetitorProductType = "">

		<cfquery name="qry_get_productCompetitorProductType" datasource="#application.siteDataSource#">
			select * from productCompetitorProductType pct
			order by ProductType
		</cfquery>

		<cfset THIS.qproductCompetitorProductType = qry_get_productCompetitorProductType>
		<cfreturn qry_get_productCompetitorProductType/>
	</cffunction>

	<!--- 2009/11/09 - AJC - P-LEX039 - Get the list of competitors and types --->
		<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Get the list of competitors
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getOppProductCompetitors" returntype="query" hint="Gets all the competitors OppPaymentTerms table">

		<cfset var qry_get_OppProductCompetitors = "">

		<cfparam name="THIS.OppProductCompetitorID" default="">

		<cfquery name="qry_get_OppProductCompetitors" datasource="#application.siteDataSource#">
			select oppProductCompetitorID,pc.productCompetitorID,pcpt.ProductCompetitorProductTypeID, pc.userProductCompetitor, CASE pc.productCompetitorID WHEN 0 then userProductCompetitor ELSE pt.CompetitorName END as productCompetitorName,model,unitcost,notes,description,producttype, '<img src="/images/icons/application_form_edit.png" border="0" />' as editlink, '<img src="/images/icons/application_form_delete.png" border="0" />' as removelink  from oppProductCompetitor pc
			left join productCompetitor pt on pc.productCompetitorID = pt.productCompetitorID
			left join productCompetitorProductType pcpt on pc.productCompetitorProductTypeID = pcpt.productCompetitorProductTypeID
			where opportunityProductID =  <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfif THIS.OppProductCompetitorID neq "">
				and oppProductCompetitorID =  <cf_queryparam value="#THIS.OppProductCompetitorID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>

		<cfset THIS.qOppProductCompetitors = qry_get_OppProductCompetitors>
		<cfreturn qry_get_OppProductCompetitors/>
	</cffunction>

	<!--- START: 2009/11/11	AJC P-LEX039 Competitive products --->
	<cffunction access="public" name="addOppProductCompetitor" hint="Inserts a record for oppProductCompetitors">

		<cfset var addOppProductCompetitor="">

		<cfquery name="addOppProductCompetitor" datasource="#application.siteDataSource#">
			insert into OppProductCompetitor (productCompetitorID, productCompetitorProductTypeID, opportunityProductID, userProductCompetitor, Model, unitCost, Notes, Description, CreatedBy, Created, LastUpdated, LastUpdatedBy)
			values (<cf_queryparam value="#THIS.productCompetitorID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#THIS.productCompetitorProductTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cfif THIS.productCompetitorID eq 0> <cf_queryparam value="#THIS.userProductCompetitor#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse> ''</cfif>, <cf_queryparam value="#THIS.Model#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#THIS.unitCost#" CFSQLTYPE="CF_SQL_float" >, <cf_queryparam value="#THIS.Notes#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#THIS.Description#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="cf_sql_integer" >,getdate(),getdate(),<cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="cf_sql_integer" >)
		</cfquery>

	</cffunction>
	<!--- END: 2009/11/11	AJC P-LEX039 Competitive products --->

	<!--- START: 2009/11/11	AJC P-LEX039 Competitive products --->
	<cffunction access="public" name="updateOppProductCompetitor" hint="Inserts a record for oppProductCompetitors">

		<cfset var updateOppProductCompetitor="">

		<cfquery name="updateOppProductCompetitor" datasource="#application.siteDataSource#">
			update OppProductCompetitor
			set productCompetitorID =  <cf_queryparam value="#THIS.productCompetitorID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			productCompetitorProductTypeID =  <cf_queryparam value="#THIS.productCompetitorProductTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			oppProductID =  <cf_queryparam value="#THIS.oppProductID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			userProductCompetitor=<cfif this.productCompetitorID eq 0><cf_queryparam value="#THIS.userProductCompetitor#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse>''</cfif>,
			Model =  <cf_queryparam value="#THIS.Model#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			unitCost =  <cf_queryparam value="#THIS.unitCost#" CFSQLTYPE="CF_SQL_float" > ,
			Notes =  <cf_queryparam value="#THIS.Notes#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Description =  <cf_queryparam value="#THIS.Description#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			LastUpdated=getdate(),
			LastUpdatedBy=#request.relaycurrentuser.personID#
			WHERE oppProductCompetitorID =  <cf_queryparam value="#THIS.oppProductCompetitorID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>
	<!--- END: 2009/11/11	AJC P-LEX039 Competitive products --->

	<!--- START: 2009/11/11	AJC P-LEX039 Competitive products --->
	<cffunction access="public" name="deleteOppProductCompetitor" hint="Deletes a record from oppProductCompetitors">

		<cfset var deleteOppProductCompetitor="">

		<cfquery name="deleteOppProductCompetitor" datasource="#application.siteDataSource#">
			delete from OppProductCompetitor
			where oppProductCompetitorID =  <cf_queryparam value="#THIS.oppProductCompetitorID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cffunction>
	<!--- END: 2009/11/11	AJC P-LEX039 Competitive products --->
	<!--- START: 2009/11/11	AJC P-LEX039 get product groups by campaign --->

	<cffunction access="public" name="getProductGroupList" hint="Retrieves foreign key data for productGroupID THIS.qproductGroupID to the query object.">
        <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

        <cfif THIS.countryID NEQ 0>
            <cfset arguments.countryID = this.countryID>
        </cfif>

		<!--- 2010-07-13 NYB Lexmark LHID 3636 - changed the countryid in to be based on countryid of person who entered the opportunity --->
		<cfquery name="qry_get_productGroupList" datasource="#application.siteDataSource#">
			select 0 as productgroupID, 'phr_opp_SelectProductGroup' as productGroupDescription, 0 as sortOrder
			UNION
			SELECT pg.productgroupID, pg.Description as productGroupDescription, 1 as sortOrder
				FROM productgroup pg
			where pg.productgroupID IN
			(select productgroupID from product p
            left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
            where p.campaignID  in ( <cf_queryparam value="#THIS.productCatalogueCampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<!--- and (countryID in (select countrygroupid from countrygroup where countrymemberid in (select countryid from location where locationid =  <cf_queryparam value="#THIS.partnerLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > ) --<!---= #THIS.countryID#--->
			) or countryID = 0) --->
            and (p.hascountryScope = 0 or vcs.countryid is not null)
			and deleted = 0 and active=1)
			order by sortOrder  <!--- Wab 2011/11/22 removed pg. from sortorder --->
		</cfquery>
	<cfset THIS.qproductGroupList = qry_get_productGroupList>
	</cffunction>
	<!--- END: 2009/11/11	AJC P-LEX039 Competitive products --->


	<cffunction name="getOppPricebookProducts" returntype="query" access="public" hint="Returns products in a given pricebook">
		<cfargument name="pricebookID" type="numeric" required="true">
		<cfargument name="productGroupId" type="numeric" required="false">
        <cfargument name="countryID" type="numeric" default="#request.relaycurrentuser.content.showForCountryID#">

		<cfscript>
			var getProducts = "";
		</cfscript>

        <cfif THIS.countryID NEQ 0>
            <cfset arguments.countryID = THIS.countryID>
        </cfif>

		<cfquery name="getProducts" datasource="#application.siteDataSource#">
			select distinct p.*
			from product p with (noLock)
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid and vcs.countryid = <cf_queryparam value="#arguments.countryID#" cfsqltype="CF_SQL_INTEGER">
				inner join pricebookEntry pbe with (noLock) on p.productID = pbe.productID
				inner join pricebook pb with (noLock) on pb.pricebookID = pbe.pricebookID
				inner join productgroup pg with (noLock) on pg.productgroupid = p.productgroupid
			where p.deleted != 1 and pbe.deleted != 1 and p.active = 1 --don't bring back products deleted from the catalogue or pricebook
				and pb.active = 1
				and pb.pricebookID = #arguments.pricebookID#
				<cfif structKeyExists(arguments,"productGroupID")>
					and p.productGroupID = #arguments.productGroupID#
					and isNull(pbe.active,1) = 1			<!--- 2012/08/22 NAS 430196 Deal Reg Form does not ignore InActive products --->
				</cfif>
                and (p.hascountryScope = 0 or vcs.countryid is not null)
			order by p.description
		</cfquery>

		<cfreturn getProducts>

	</cffunction>


	<cffunction name="getOpportunityMergeFields" access="public" returntype="struct">
		<cfargument name="fieldName" type="string" default="">
		<cfargument name="opportunityID" type="numeric">
		<cfargument name="opportunity" required="false">

		<cfset var getOppProducts = "">  <!--- 2014-10-06	WAB/AHL Case 441992 var'ing --->
		<cfif not structKeyExists(arguments,"opportunity")>
			<cfset opportunity = application.com.structureFunctions.queryRowToStruct(query=application.com.opportunity.get(arguments.opportunityID),row=1)>
			<cfif isNumeric(opportunity.stageID)>
				<cfset opportunity.Stage = getstageID(stageID=opportunity.stageID).itemText>
			</cfif>
			<cfif isNumeric(opportunity.statusID)>
				<cfset opportunity.Status = getStatusID(statusID=opportunity.statusID).itemText>
			</cfif>
			<cfif isNumeric(opportunity.sourceID)>
				<cfset opportunity.Source = getSourceID(sourceID=opportunity.sourceID).itemText>
			</cfif>
			<cfif isNumeric(opportunity.oppTypeID)>
				<cfset opportunity.Type = GetOppTypes(oppTypeID=opportunity.oppTypeID).oppType>
			</cfif>
			<cfif isNumeric(opportunity.oppCustomerTypeID) and opportunity.oppCustomerTypeID neq 0>
				<cfset opportunity.CustomerType = getCustomerTypes(customerTypeID=opportunity.oppCustomerTypeID).customerType>
			</cfif>
		</cfif>

		<cfif not structKeyExists(opportunity,"partner") and isNumeric(opportunity.partnerSalesPersonID)>
			<cfset opportunity.partner = application.com.commonQueries.getFullPersonDetails(opportunity.partnerSalesPersonID)>
		</cfif>

		<cfif not structKeyExists(opportunity,"accountManager") and isNumeric(opportunity.vendorAccountManagerPersonID)>
			<cfset opportunity.accountManager = application.com.commonQueries.getFullPersonDetails(opportunity.vendorAccountManagerPersonID)>
		</cfif>

		<cfif not structKeyExists(opportunity,"EndCustomer")>
			<cfif isNumeric(opportunity.contactPersonID) and opportunity.contactPersonID neq 0>
				<cfset opportunity.EndCustomer = application.com.commonQueries.getFullPersonDetails(opportunity.contactPersonID)>
			<cfelseif isNumeric(opportunity.entityID)>
				<cfset opportunity.EndCustomer = application.com.commonQueries.getOrgDetailsForEmailMerge(OrganisationID=opportunity.entityID) />
			</cfif>
		</cfif>

		<cfif StructKeyExists(opportunity,"vendorSalesManagerPersonID") and isNumeric(opportunity.vendorSalesManagerPersonID)>
			<cfset opportunity.SalesManager = application.com.commonQueries.getFullPersonDetails(opportunity.vendorSalesManagerPersonID) />
		</cfif>

		<cfif not structKeyExists(opportunity,"opportunityProducts") or not structKeyExists(opportunity,"grandTotal")>
			<cfquery name="getOppProducts" datasource="#application.siteDataSource#">
				select p.description,op.quantity,op.unitPrice,op.subtotal,
					(op.unitPrice*op.quantity)-op.subTotal as discount
				from opportunityProduct op
					inner join product p on op.productOrGroupID = p.productID and op.productOrGroup = 'P'
				where opportunityID=#arguments.opportunityID#
			</cfquery>

			<cfset opportunity.opportunityProducts = getOppProducts>
			<cfset opportunity.grandTotal = arraySum(listToArray(valueList(getOppProducts.subTotal)))>
		</cfif>

		<cfif structKeyExists(opportunity,"expectedCloseDate")>
			<cfset opportunity.expectedCloseDate = DateFormat(opportunity.expectedCloseDate,"dd-mmm-yyyy")>	<!--- 2012/05/29 PPB P-MIC001 format the date to remove the timestamp   --->
		</cfif>

		<cf_include template="/code/cftemplates/opportunityEmailMergeBuilder_extension.cfm" checkifexists="true">

		<cfreturn opportunity>
	</cffunction>



	<!--- 2011/09/21 MS new valid value functions now used in opportunityDomain.xml --->
	<cffunction access="public" name="getOppStatusID" hint="Gets the status as a phrase from the oppStatus table for oppertunaties screen" validValues="true">
		<cfargument name="StatusID" required="no" type="numeric" hint="provide this to get  a status">

		<cfquery name="qryStatus" datasource="#application.siteDataSource#">
			<!--- select 		null as value,
						'phr_notAppliedForDealReg' as display,
						1 as orderIdx
			union --->
			<!--- NJH 2014/08/14 - cast the ID as a varchar, because the code to make a hidden field in relayFormElementDisplay looks at the metaData of teh query, and if it's numeric, then it sets an empty string as 0
				which is not what we want, as that breaks the FK constraint. Will probably need a better way of handling this in the future. --->
			select 		cast(statusID as varchar) as value,
						'phr_opp_'+statusTextID as display
			from 		oppStatus
			order by 	statusID
		</cfquery>

		<cfreturn qryStatus/>

	</cffunction>


	<cffunction access="public" name="getOppSpecialPricingStatus" hint="Gets the pricing status as a phrase from the oppPricingStatus table for oppertunaties screen" validValues="true">
		<cfargument name="pricingStatusID" required="yes" type="numeric" hint="provide this to get pricing status">

		<cfquery name="qryPricingStatus" datasource="#application.siteDataSource#">
			select oppPricingStatusID, 'phr_opp_'+statusTextID as oppPricingStatus
			from oppPricingStatus
			where oppPricingStatusID=#arguments.pricingStatusID#
		</cfquery>

		<cfreturn qryPricingStatus />

	</cffunction>

	<cffunction name="getOppProbability" access="public" hint="Returns the Probability value for an Opp Stage ID" returntype="numeric">
		<cfargument name="StageID" required="true">

		<cfset var result = 0>

			<cfquery name="qry_getOppProbability" datasource="#application.siteDataSource#">
				select isNull(probability,0) as probability from OppStage where OpportunityStageID =  <cf_queryparam value="#arguments.StageID#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>

			<cfif qry_getOppProbability.recordcount>
				<cfset result = qry_getOppProbability.probability>
			</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="isUserPortalLeadManager" type="public" returnType="boolean">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="organisationID" type="numeric" default="#request.relayCurrentUser.organisationID#">
		<cfargument name="locationID" type="numeric" default="#request.relayCurrentUser.locationId#">

		<cfset var portalLeadManagerFlagTextIDs = "KeyContactsPrimary,PortalLeadManager,PartnerOrderingManager,primaryContacts">
		<cfset var flagStruct = structNew()>
		<cfset var flagTextID = "">
		<cfset var leadManagerQry = "">
		<cfset var personIdList = "">

		<cfloop list="#portalLeadManagerFlagTextIDs#" index="flagTextID">
			<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagTextID)>

			<cfif flagStruct.isOK and flagStruct.flagActive>
				<cfset var entityID = arguments[flagStruct.entityType.uniqueKey]>
				<cfif flagStruct.flagType.datatable eq "integer">
					<cfset leadManagerQry = application.com.flag.getFlagData(flagid=flagTextID,entityID=entityID)>
					<cfset personIdList = valueList(leadManagerQry.data)>
				<cfelseif flagStruct.flagType.datatable eq "boolean">
					<cfset leadManagerQry = application.com.flag.getFlagData(flagid=flagTextID,entityID=arguments.personId)>
					<cfset personIdList = valueList(leadManagerQry.personID)>
				</cfif>

				<cfif listFind(personIdList,arguments.personID)>
					<cfreturn true>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn false>

	</cffunction>


	<cffunction name="getBudgetRanges" access="public" output="false" returnType="query" validValues="true">

		<cfset var qryBudgetRanges = "">

		<cfquery name="qryBudgetRanges" datasource="#application.siteDatasource#">
			select rangeValue as budget,range as budgetRange from range
		</cfquery>

		<cfreturn qryBudgetRanges>
	</cffunction>


	<cffunction name="reAssignOpportunities" access="public">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="assignFrom" type="string" default="person">

		<cfset var reAssignOpportunities = "">
		<cfset var getPerOpportunities = "">
		<cfset var currentLocations = "">
		<cfset var locationID = application.com.relayPLO.getPersonDetails(arguments.personID).locationID>
		<cfset var OppPartnerSalesPerson = application.com.opportunity.getOppPartnerSalesPerson(locationID)>
		<cfset var randomNumber=replace(rand(),".","")>
		<cfset var partnerLocationIDList = "">
		<cfset var countryID = "">
		<cfset var excludeLocations = "">

		<cfquery dbtype="query" name="OppPartnerSalesPerson">
			select * from  OppPartnerSalesPerson where personID != #arguments.personID#
		</cfquery>
		<!--- If no people to re-assign to then treat this as a location deletion to assign to another location in the sales persons organisation --->
		<cfif OppPartnerSalesPerson.recordcount eq 0><cfset arguments.assignFrom="location"></cfif>

		<!--- If no people to re-assign to in the salespersons organisation then treat this as an organisation deletion to assign to another partner in the salesperson's country --->
		<cfif arguments.assignFrom eq "location">
			<cfset partnerLocationIDList = application.com.relayplo.getOrgLocations(organisationID=application.com.relayPLO.getPersonDetails(arguments.personID).organisationID,excludeFlaggedForDeletion=true,additionalfilter="and locationID in (select distinct locationID from person)")>
			<cfquery dbtype="query" name="partnerLocationIDList">
				select * from partnerLocationIDList where locationID != #locationID#
			</cfquery>
			<cfif partnerLocationIDList.recordcount eq 0><cfset arguments.assignFrom="organisation"></cfif>
		</cfif>

		<cfquery name="getPerOpportunities" datasource="#application.siteDatasource#">
			select * from opportunity where partnerSalesPersonID = #arguments.personID#
		</cfquery>
		<cfsavecontent variable="reAssignOpportunities">

			<table width="100%">
				<tr><th>phr_reAssignOpp_OppID</th><th>phr_reAssignOpp_Detail</th><th>phr_reAssignOpp_Action</th></tr>
				<cfoutput query="getPerOpportunities">
					<tr>
						<td>#opportunityID#</td>
						<td>#detail#</td>
						<cfif currentRow eq 1>
							<td rowspan="#getPerOpportunities.recordcount#" style="vertical-align:top;">
								<cfswitch expression="#arguments.assignFrom#">
									<cfcase value="person">
										<select name="reAssignTo">
											<cfloop query="#OppPartnerSalesPerson#">
												<option value="#arguments.personID#|#OppPartnerSalesPerson.personID#">#OppPartnerSalesPerson.fullname#</option>
											</cfloop>
										</select>
									</cfcase>
									<cfcase value="organisation,location">
										<cfif arguments.assignFrom eq "organisation">
											<cfquery name="currentLocations" datasource="#application.siteDatasource#">
													select locationID from location
													where organisationID in (
														select organisationID from location where locationID  in ( <cf_queryparam value="#locationID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
													)
											</cfquery>
											<cfset excludeLocations = "#valueList(currentLocations.locationId)#">
										<cfelse>
											<cfset excludeLocations = "#locationID#">
										</cfif>
										<table><tr><td>
											<cf_relayFormElementDisplay relayFormElementtype="autoComplete" function="com.opportunity.getOppPartners(countryID = #application.com.relayPLO.getPersonDetails(personID = arguments.personID).countryID#, excludeLocations='#excludeLocations#')" display="siteName" value="locationid" required="true" fieldname="partners#randomNumber#" label="Choose a Partner Location" currentvalue="">
										</td></tr><tr><td>
											<cf_relayFormElementDisplay relayFormElementtype="select" currentValue="" nulltext="Choose a Partner Location" required="true" shownull="true" onclick="javascript:jQuery('##reAssignTo#randomNumber#').val('#arguments.personID#|' + this.value);" FieldName="salesPerson#randomNumber#" display="fullname" value="personID" bindonload="false" label="Choose a Sales Rep" bindFunction="cfc:relay.webservices.relayopportunityws.getOppPartnerSalesPerson({partners#randomNumber#})" >
											<cf_input type="hidden" name="reAssignTo" id="reAssignTo#randomNumber#">
										</td></tr></table>
									</cfcase>
								</cfswitch>
							</td>
						</cfif>
					</tr>
				</cfoutput>
			</table>
		</cfsavecontent>

		<cfreturn reAssignOpportunities>
	</cffunction>


	<cffunction name="updateReAssignedOpportunities" access="public">
		<cfargument name="reAssignList" type="string" required="true">

		<cfset var message = "">
		<cfset var assignFrom = "">
		<cfset var assignTo = "">
		<cfset var reAssign = "">
		<cfset var i = "">

		<cfloop list="#form.reassignTo#" index="i">
			<cfquery name="assignFrom" datasource="#application.siteDatasource#">
				select firstname + ' ' + lastname as name from person where personID =  <cf_queryparam value="#listfirst(i,"|")#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfquery name="assignTo" datasource="#application.siteDatasource#">
				select firstname + ' ' + lastname as name, personID, locationID from person where personID =  <cf_queryparam value="#listlast(i,"|")#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfquery name="reAssign" datasource="#application.siteDatasource#">
				update opportunity
				set partnerSalesPersonID =  <cf_queryparam value="#assignTo.personID#" CFSQLTYPE="CF_SQL_INTEGER" > , partnerLocationID =  <cf_queryparam value="#assignTo.locationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				where partnerSalesPersonID =  <cf_queryparam value="#listfirst(i,"|")#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset message = message & " phr_ext_OppAssignFrom " & assignFrom.name & " phr_ext_OppAssignTo " & assignTo.name & "; ">
		</cfloop>
		<cfset message = left(message,len(message)-2)>

		<cfreturn message>
	</cffunction>

	<cffunction name="getOppContactDetails" access="public" output="false" returntype="query">
		<cfargument name="opportunityID" type="numeric" required="true" />
		<cfset var qOppContact = queryNew('') />

		<cfquery name="qOppContact">
			SELECT P.PersonID,
				P.FirstName,
				P.LastName,
				P.Email,
				P.MobilePhone,
				P.OfficePhone,
				P.FaxPhone,
				OG1.OrganisationID,
				OG1.OrganisationName,
				L.LocationID,
				L.Address1,
				L.Address2,
				L.Address3,
				L.Address4,
				L.Address5,
				L.SpecificURL,
				c.CountryDescription
			FROM
				opportunity OPP
				INNER JOIN person P ON OPP.CONTACTPERSONID = P.personid
				INNER JOIN location L ON P.LocationID = L.LocationID
				inner join country c on l.countryid = c.countryid
				INNER JOIN organisation OG1 ON P.OrganisationID = OG1.OrganisationID
			WHERE
				OPP.opportunityID  = <cf_queryparam value="#arguments.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn qOppContact />
	</cffunction>

	<!--- 2014-11-18	RPW	CORE-97 Added function getOppTypeID --->
	<cffunction name="getOppTypeID" access="public" output="false">
		<cfargument name="oppTypeTextID" type="string" required="true" />

		<cfset var qOppType = queryNew('')>
		<cfset var retVal = 0 />
		<cfquery name="qOppType">
			SELECT oppTypeID
			FROM dbo.opptype
			WHERE oppTypeTextID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.oppTypeTextID#" />
		</cfquery>
		<cfif qOppType.recordCount EQ 1>
			<cfset retVal = val(qOppType.oppTypeID)>
		</cfif>
		<cfreturn retVal />
	</cffunction>


	<cffunction name="getPartnerAccountManager" output="false" public="true" returnType="numeric" hint="Returns the account manager for a partner. Used for the lead and opportunity forms">
		<cfargument name="partnerPersonID" type="numeric" require="true" default="#request.relayCurrentUser.personID#" hint="The partners personID">

		<!--- START: 2014-11-24 AXA CORE-117 added check for new settings to switch flag for vendoraccountmanager --->
		<cfscript>
			variables.accountManagerID = 0;
			// JIRA PROD2016-350 - NJH 2016/05/19 renamed setting
			// JIRA PROD2016-2697 - NJH 2016/11/14 - logic was inverted. Set it correct. Hopefully no nasty consequences.
			variables.useOrgAccManager = !application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount");

			if (arguments.partnerPersonID != request.relayCurrentUser.personID) {
				variables.personDetails = application.com.structureFunctions.queryRowToStruct(query=application.com.relayPLO.getPersonDetails(personID=arguments.partnerPersonID));
			} else {
				variables.personDetails = duplicate(request.relayCurrentUser);
			}

			if (variables.useOrgAccManager) {
				variables.vendorFlagTextID = 'accManager';
			} else {
				variables.vendorFlagTextID = 'locAccManager';
			}
			variables.qryflagData = application.com.flag.getFlagData(flagID=variables.vendorFlagTextID,entityID=personDetails[variables.useOrgAccManager?"organisationID":"locationID"]);

			if (variables.qryflagData.recordCount) {
				variables.accountManagerID = variables.qryflagData.data[1];
			}
		</cfscript>
		<!--- END: 2014-11-24 AXA CORE-117 added check for new settings to switch flag for vendoraccountmanager --->

		<cfreturn variables.accountManagerID>
	</cffunction>
</cfcomponent>
