<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
ALL UNDERDEVELOPMENT -  CUT AND PASTE JOB FROM SOME CFM FILES  WAB

BIT OF A MISHMASH



2007-05-23  WAB did some more work on it and is now working better
			added functions to make view directly from a screen, can read extra parameters from the screendefinition and from the flagdefinition (parameters must start with "view_") 
2008/03/25	WAB changed so that radio groups can be outputted as individual flags if displayAs=IndividualFlags is set 
2008/10/07	WAB changes during project SOPH Sprint 5 and creation of admin\screens\createScreenView.cfm
			including change to storing of parameters on screendefinitions to use view.  notation
			including storing parameters on the screens table
2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup
2009/03/15  WAB Problems with the above changes - something somewhere got mixed up and the cfc would not load.  Sorted out
			Also changed the grid array so that could be passed by reference (as a child of a structure)				
 --->



<cfcomponent displayname="Grids And Views" hint="">

 
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createViewFromScreen" hint="" >
		<cfargument name="ScreenId" type="string" required="true">
		<cfargument name="viewName" type="string" required="true">
		<cfargument name="showAllCheckboxesIndividually" default="false">
		<cfargument name="showAllRadiosIndividually" default="false">
		<cfargument name="booleanOn" default = "1"> 
		<cfargument name="booleanOff" default = "0">
		<cfargument name="getSettingsFromScreen" type="boolean" default="true">

		<cfset var result = structNew()>
		<cfset var createViewResult = "">


		<cfset result.isOK = true>


		<!--- Can get all the settings from the screen header, or alternatively pass them in --->
		<cfif getSettingsFromScreen>
			<cfquery name="GetScreen" dataSource="#application.siteDataSource#">
			select * from screens where screenid =  <cf_queryparam value="#ScreenId#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			<cfset screenParameterStructure= application.com.regExp.convertNameValuePairStringToStructure (inputString = GetScreen.parameters,delimiter = ",", DotNotationToStructure = true) >
			<cfif structKeyExists (screenParameterStructure,"view")>
				<cfloop item="field" collection = #screenParameterStructure.view#>
					<cfset arguments[field] = screenParameterStructure.view[field]>
				</cfloop>
			</cfif>
		</cfif>	

		
		
		<cfset columnArray = createColumnArrayFromScreen	(screenid)	>
		
		<cfset gridAndViewDefinition = createQueryAndReportDefinitionFromColumnArray(columnArray = columnArray, argumentcollection = arguments)	>
		<cfset result.gridAndViewDefinition = gridAndViewDefinition>

		<cfset viewComment = "Created from Screen #screenid# by #request.relayCurrentuser.fullname# #now()#">

		<cfset createViewResult = createViewFromDefinition	(viewdefinition  = gridAndViewDefinition.viewdefinition, viewname = viewname, viewComment = viewComment)>
		
		<cfif not createViewResult.isOK>
			<cfset result.isOK = false>
			<cfset result.error  = createViewResult.Error>
		</cfif>
		
		
		<cfreturn result>
		
	</cffunction>
 


	<cffunction access="public" name="createViewFromFieldList" hint="" >
		<cfargument name="FieldList" type="string" required="true">
		<cfargument name="viewName" type="string" required="true">
		<cfargument name="showAllCheckboxesIndividually" default="false">
		<cfargument name="showAllRadiosIndividually" default="false">		
		<cfargument name="booleanOn" default = "1"> 
		<cfargument name="booleanOff" default = "0">

		<cfset var result = structNew()>
		<cfset var createViewResult = "">

		<cfset result.isOK = true>


		<cfset columnArray = createColumnArrayFromFieldList	(FieldList)	>

		<cfset gridAndViewDefinition = createQueryAndReportDefinitionFromColumnArray(columnArray = columnArray, argumentcollection = arguments)	>
		<cfset result.gridAndViewDefinition = gridAndViewDefinition>

		<cfset viewComment = "Created by #request.relayCurrentuser.fullname# #now()#">

		
		<cfset createViewResult = createViewFromDefinition	(viewdefinition  = gridAndViewDefinition.viewdefinition, viewname = viewname, viewComment = viewComment)>
		
		<cfif not createViewResult.isOK>
			<cfset result.isOK = false>
			<cfset result.error  = createViewResult.Error>
		</cfif>
		
		
		<cfreturn result>
		
	</cffunction>


	<cffunction access="public" name="createViewFromFlagGroup" hint="" >
		<cfargument name="FlagGroupId" type="string" required="true">
		<cfargument name="showAllCheckboxesIndividually" default="true">
		<cfargument name="showAllRadiosIndividually" default="true">
		
		<cfreturn createViewFromFieldList (fieldList = "flagGroup_#flagGroupID#", argumentCollection = arguments)>
		
	</cffunction>
 


	<cffunction access="public" name="createColumnArrayFromScreen" hint="" >
		<cfargument name="ScreenId" type="string" required="true">

		<!--- 
		Takes a screen definition and creates the intermediate structure which can then beused to create a view
		Note that you can use the additional formatting parameters of a screendefinition to control how the view is created.  
		Any parameter starting "view_" is copied into the column structure (with the view_ bit removed)
		Eg:
		view_displayAs=individualFlags    (will cause a checkbox or radio group to be output as 1 column per flag)
		
		
		 --->
		
		<cfset var columnArray = arraynew(1)>	
		

		<cfset getScreendefinition = application.com.screens.getScreenAndDefinition(screenid = screenid, countryid = 0).definition>

			<cfloop query = getScreenDefinition>
				
				<cfset thisColumn = structNew()>
				
				<cfif specialFormatting is not "">
					<cfset thisColumn = addFormattingToColumnStructure(thisColumn, specialFormatting)>
				</cfif>
				
				<cfif listfindnocase("flagGroup,flag",fieldsource) is not 0>
					<cfloop list="#fieldTextID#" index = "thisFieldtextID">  <!--- WAb 2008/10/06 added loop - allows a list of flags or groups --->
						<cfset thisColumn.type = fieldSource>
						<cfset thisColumn.id = thisfieldtextid>
						<cfset arrayappend(columnArray, thisColumn)>
					</cfloop>	
				<cfelseif listfindnocase(structKeyList(application.entityTypeID),fieldsource) is not 0> <!---  WAB 2009/03/15 had to move this if below the flaggroup one because flagGroup does actually appear in the list of entities--->
					<cfset thisColumn.type = "table">
					<cfset thisColumn.tableName = fieldsource>
					<cfset thisColumn.field = fieldtextid	>
					<cfset arrayappend(columnArray, thisColumn)>

				<cfelseif fieldsource is "value">
					<cfset thisColumn.type = "value">
					<cfset thisColumn.value = fieldtextid>
					<cfset arrayappend(columnArray, thisColumn)>
					
				<!--- 2007/05/24 - GCC - I think this is a naughty hack and I should be shot by Will. Talk to me Will but this enables us to run a subquery as part of the select statement... --->
				<cfelseif fieldsource is "Query">
					<cfset thisColumn.type = "sql">
					<cfset thisColumn.id = fieldtextid>
					<cfset thisColumn.value = replace(evalString,"""","","ALL")>
					<cfset arrayappend(columnArray, thisColumn)>
				</cfif>
				
			</cfloop>

		<cfreturn columnArray>

	</cffunction>


	<cffunction access="public" name="createColumnArrayFromXML" hint="" >
		<cfargument name="XML" type="string" required="true">

		<CFSET XMLstruct = XmlParse(XML)>
	
		<cf_xmlq name="columnQuery" xpath="//view/column" xml="#xml#">	

		<!--- create an array of structures - --->
		<cfset columnArray = arraynew(1)>
		<cfset theColumnList = columnQuery.columnlist>
		<cfset theColumnList = listdeleteAt(theColumnList,listfindnocase(theColumnList,"column"))>
		<cfloop query="columnQuery">
			<cfset result = structNew()>
			<cfloop index="theColumn" list = "#theColumnList#">
				<cfset value = columnQuery[thecolumn][currentrow]>
				<cfif value is not "">
					<cfset keyName = listRest(theColumn,"_")> <!--- need to drop the column_ bit put in  by cf_xmlq --->
					<cfset result[keyName] = value>
				</cfif>
			</cfloop>
			<cfset arrayappend(columnArray,result)>
		</cfloop>
	
		<cfreturn columnArray>
		
	</cffunction>


	<cffunction access="public" name="createColumnArrayFromFieldList" hint="" >
		<cfargument name="fieldlist" type="string" required="true">

			<cfset var columnArray = arraynew(1)>	

				<cfloop index="item" list = "#fieldlist#">

					<cfset thisColumn = structNew()>
					<cfif listfindnocase("flagGroup,flag",listFirst(item,"_")) is not 0>
						<cfset thisColumn.type = listFirst (item,"_")>
						<cfset thisColumn.id = listGetAt (item,2,"_")>
					<cfelseif listfindnocase(structKeyList(application.entityTypeID),listFirst(item,"_")) is not 0>
						<cfset thisColumn.type = "table">
						<cfset thisColumn.tableName = listFirst (item,"_")>
						<cfset thisColumn.field = listGetAt (item,2,"_")	>
					</cfif>

					<cfset arrayappend(columnArray, thisColumn)>
				</cfloop>	
					
			<cfreturn columnArray>

	</cffunction>


	<cffunction access="public" name="createQueryAndReportDefinitionFromColumnArray" hint="" >
		<cfargument name="columnArray" type="array" required="true">	
		<cfargument name="showAllCheckboxesIndividually" default = "false">
		<cfargument name="showAllRadiosIndividually" default="false">
	
	
			<!--- START: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->				
			<cfset var entityTableList = "">
			<cfset var entityTableName = "">
			<cfset var entityTableUniqueKey = "">
			<cfset var entityKey = "">
			<!--- END: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
		 	<cfset joinArray = arraynew(1)>
	 		<cfset filterArray = arraynew(1)>


			<cfset viewdefinition = structNew()>
			<cfset viewdefinition.selectList = "">
			<cfset viewdefinition.selectListDef = "">
			<cfset viewdefinition.joins = "">

			<cfset viewDefinition.lastUpdatedFields = "">

				<cfset viewDefinition.flagidsDone = "">
			<cfset viewDefinition.flaggroupidsDone = "">
			<cfset viewDefinition.aliasesUsed = "">
			
			<cfset viewDefinition.whichTables = structNew()>
			
			<cfset gridDefinition = structNew()>
			<cfset gridDefinition.gridArray = arrayNew(1)> <!--- has to be an array within a structure (arrays can't be passed arounf by reference) --->
			
			<cfset blankGridItem = structNew()>
			<cfset blankGridItem.name = ""> 
			<cfset blankGridItem.header = ""> 
			<cfset blankGridItem.type = "STRING_NOCASE"> 
			<cfset blankGridItem.values = ""> 
			<cfset blankGridItem.width = "0"> 
			<cfset blankGridItem.select = "yes"> 
			<cfset blankGridItem.display = "yes"> 
			<cfset blankGridItem.canOrderBy = "false"> 
			<cfset blankGridItem.valuesdisplay = ""> 
		
			<cfloop index= "I" from = 1 to = #arrayLen(columnArray)# >
			
				<cfset thisColumn = columnArray[I]>
			
				<cfset addTheColumnToTheViewAndGridDefinitions (thisColumn = columnArray[I],viewDefinition =viewDefinition,  gridDefinition = gridDefinition, argumentcollection = arguments)>
				
				
			</cfloop>		
						
			<cfif structkeyExists(viewdefinition.whichtables,"organisation") or structkeyExists(viewdefinition.whichtables,"location") or structkeyExists(viewdefinition.whichtables,"person") >
			
					<!--- Which tables do we need to join on --->
					<cfset mainJoin = " organisation o WITH (NOLOCK) ">
					<cfset viewdefinition.selectList =  listappend("#chr(10)# o.organisationid ",viewdefinition.selectList)>
					<cfset viewdefinition.selectListDef = listappend(" 1 as organisationid ",viewdefinition.selectListDef)>
					<cfif structkeyExists(viewdefinition.whichtables,"organisation")>
						<cfset viewDefinition.lastUpdatedFields = listappend(viewDefinition.lastUpdatedFields,"o.lastupdated" )>
					</cfif>
					<cfset uniquekey = "convert(varchar,o.organisationid)">
					<cfset thisGridItem = structCopy(blankGridItem)>
					<cfset thisGridItem.Name = "organisationid">
					<cfset thisGridItem.Header = "OrganisationID">
					<cfset thisGridItem.Select = "no">
					<cfset thisGridItem.Width = "30">	
					<cfset arrayappend(gridDefinition.gridarray,structCopy(thisGridItem))>

				<cfif structkeyExists(viewdefinition.whichtables,"location") or structkeyExists(viewdefinition.whichtables,"person")>
					<cfset mainJoin = mainJoin & " left join location l  WITH (NOLOCK) on o.organisationid = l.organisationid  ">
					<cfset viewdefinition.selectList =  listappend("#chr(10)# l.locationid ",viewdefinition.selectList)>
					<cfset viewdefinition.selectListDef = listappend(" 1 as locationid ",viewdefinition.selectListDef)>
					<cfif structkeyExists(viewdefinition.whichtables,"location")>
						<cfset viewDefinition.lastUpdatedFields = listappend(viewDefinition.lastUpdatedFields,"l.lastupdated" )>
					</cfif>	
					<cfset uniquekey =  "convert(varchar,l.locationid)">
					<cfset thisGridItem = structCopy(blankGridItem)>
					<cfset thisGridItem.Name = "locationid">
					<cfset thisGridItem.Header = "LocationID">
					<cfset thisGridItem.Select = "no">
					<cfset thisGridItem.Width = "30">
					<cfset arrayappend(gridDefinition.gridarray,structCopy(thisGridItem))>
				</cfif>
						
				<cfif structKeyExists(viewDefinition.whichTables,"person") >
					<cfset mainJoin = mainJoin & " left join person p  WITH (NOLOCK) on l.locationid = p.locationid  ">
					<cfset viewdefinition.selectList =  listappend(" #chr(10)# p.personid ",viewdefinition.selectList)>
					<cfset viewdefinition.selectListDef = listappend(" 1 as personid ",viewdefinition.selectListDef)>
					<cfset viewDefinition.lastUpdatedFields = listappend(viewDefinition.lastUpdatedFields,"p.lastupdated" )>
					<cfset uniquekey =  "convert(varchar,p.personid)">
					<cfset thisGridItem = structCopy(blankGridItem)>
					<cfset thisGridItem.Name = "Personid">
					<cfset thisGridItem.Header = "PersonID">
					<cfset thisGridItem.Select = "no">
					<cfset thisGridItem.Width = "30">
					<cfset arrayappend(gridDefinition.gridarray,structCopy(thisGridItem))>
				</cfif>

			
			<cfelse>
			
				<!--- non POL table --->
				<cfset tableName = structKeyList (viewDefinition.whichTables)>
				<cfset tableDetail = application.entityType[application.entityTypeid[tableName]]>

				<cfset mainJoin = " #tableName# as #left(tablename,1)# ">
				<!--- add the primary key field if not already there --->
				<cfif not listfindnocase (viewDefinition.aliasesUsed, tableDetail.uniqueKey)>
						<cfset viewdefinition.selectList =  listappend(" #chr(10)# #left(tableName,1)#.#tableDetail.uniqueKey# ",viewdefinition.selectList)>
					<cfset viewdefinition.selectListDef = listappend(" 1 as #tableDetail.uniqueKey# ",viewdefinition.selectListDef)>
				</cfif>
				<!--- check for a lastupdated column on this table--->
				<cfquery name ="chkLastUpdated" datasource = "#application.sitedatasource#">
					select 1 from information_schema.columns where table_name =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and column_Name = 'lastupdated'
				</cfquery>
				
				<cfif chkLastUpdated.recordcount is not 0>
					<cfset viewDefinition.lastUpdatedFields = listappend(viewDefinition.lastUpdatedFields,"#left(tableName,1)#.lastupdated" )>
				</cfif>
				
				<cfset uniquekey =  "convert(varchar,#left(tableName,1)#.#tableDetail.uniqueKey#)">
				<cfset thisGridItem = structCopy(blankGridItem)>
				<cfset thisGridItem.Name = "#tableDetail.uniqueKey#">
				<cfset thisGridItem.Header = "#tableDetail.uniqueKey#">
				<cfset thisGridItem.Select = "no">
				<cfset thisGridItem.Width = "30"> 
				<cfset arrayappend(gridDefinition.gridarray,structCopy(thisGridItem))>
			
			</cfif>

						
		<cfset viewdefinition.joins = mainjoin & viewdefinition.joins>
		
		<cfif isDefined("additionaluniquekey")>
			<cfset uniquekey =  uniquekey & " + char(45) + " & additionaluniquekey>
		</cfif>
		
		
		<cfloop index= "I" from = 1 to = #arrayLen(joinArray)# >
			<cfset viewdefinition.joins = viewdefinition.joins & joinArray[I].sql>
		</cfloop>
		
		
		<cfset viewdefinition.selectList = listappend("#chr(10)##uniquekey# as uniquekey ",viewdefinition.selectList)> 
		<cfset viewdefinition.selectListDef = listappend(" 1 as uniquekey  ",viewdefinition.selectListDef)>
		
		<!--- add on a lastupdated field which combines all lastupdated s --->
		<cfset tempDateField = listfirst(viewDefinition.lastUpdatedFields)>
		<cfloop list = "#listrest(viewDefinition.lastUpdatedFields)#" index = "field">
			<cfset tempDateField = "dbo.maximumDateValue(#field#,#tempDateField#)">
		</cfloop>
		<cfset viewdefinition.selectList = listappend(viewdefinition.selectList," #tempDateField# as allFieldsLastUpdated")> 						
		<cfset viewdefinition.selectListDef = listappend("#chr(10)# 1 as  allFieldsLastUpdated ",viewdefinition.selectListDef)>

		<cfset result = structNew()>
		<cfset result.viewDefinition = viewDefinition>
		<cfset result.gridarray = gridDefinition.gridArray>		
		
		<cfreturn result>	
	
	
	</cffunction>



	<cffunction access="private" name="addTheColumnToTheViewAndGridDefinitions" hint="" >
		<cfargument name="thisColumn" type="struct" required="true">				
		<cfargument name="viewDefinition" type="struct" required="true">				
		<cfargument name="gridDefinition" type="struct" required="true"> <!--- actually a structure containing an array so that can be passed by reference --->				
		<cfargument name="showAllCheckboxesIndividually" default = "false">
		<cfargument name="showAllRadiosIndividually" default = "false">
		<cfargument name="textIDAsAlias" default = "true">
		<cfargument name="booleanOn" default = "1"> 
		<cfargument name="booleanOff" default = "0">

		
				<cfset var thisGridItem = blankReportColumn()>
				<cfif isDefined("thisColumn.width")>
					<cfset thisGridItem.width = thisColumn.width>
				</cfif>
				<cfif isDefined("thisColumn.readOnly") and thisColumn.readOnly>
					<cfset thisGridItem.select = "No">
				</cfif>
				<cfif isDefined("thisColumn.canOrderBY") and thisColumn.canOrderBY>
					<cfset thisGridItem.canOrderBY = true>
				</cfif>
				
		<!--- add single quotes to the booleanOn and BooleanOff if required --->
		<cfif not isNumeric(booleanOn) or not isNumeric(booleanOff)  >
			<cfif left(booleanOn,1) is not "'">
				<cfset booleanOn = listQualify(booleanOn,"'")>
			</cfif>
			<cfif left(booleanOff,1) is not "'">
				<cfset booleanOff = listQualify(booleanOff,"'")>
			</cfif>	
		</cfif>		
				
				<cfif thisColumn.type is "table">
					
						<cfif thisColumn.field is "*">
							<cfquery  name = "getFields" datasource = "#application.sitedatasource#">
							select column_name from information_schema.columns where table_name =  <cf_queryparam value="#thisColumn.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >  
							</cfquery>
							<cfset thisColumn.field = valueList(getFields.column_name)>
						</cfif>

						<cfset thisTable = thisColumn.TableName>
						
						<cfloop index ="thisField" list = "#thisColumn.field#">
							<cfset viewDefinition.whichTables[#thisTable#] = true>
							<cfif isDefined("thisColumn.alias")>
								<cfset alias = thisColumn.alias>
							<cfelseif listfindnocase("person,location,organisation", thisTable) and (thisField contains "lastupdated" or thisField Contains "created")>
								<cfset alias = "#thisTable#_#thisField#">
							<cfelse>
								<cfset alias = "#thisField#">
							</cfif>
				
							<cfif listfindnocase(viewDefinition.aliasesUsed,alias) is 0>
						
								<cfset viewdefinition.selectList = LISTAPPEND(viewdefinition.selectList, "#chr(10)# #left(thisTable,1)#.#thisField# as #alias# ")>
					
								<cfset viewdefinition.selectListDef = LISTAPPEND(viewdefinition.selectListDef, " 1 as #thisTable#_#thisField# ")>
				
								<cfset thisGridItem.name = alias>
								<cfset thisGridItem.header = iif(not isdefined("thisColumn.header"),"alias","thisColumn.header")>
								<cfset viewDefinition.aliasesUsed = listappend(viewDefinition.aliasesUsed,alias)>
				
							</cfif>
						</cfloop>			
						
					<cfelseif thisColumn.type is "flag">		
					
						<cfset flagid = thisColumn.id>
						<cfset theFlagStructure = application.com.flag.getFlagStructure(flagid)>
						<cfset flagid = theFlagSTructure.flagid>  <!--- any textids will be converted to numerics here --->
						<cfif isDefined("thisColumn.tableAlias")>
							<cfset tableAlias = thisColumn.tableAlias>
						<cfelse>
							<cfset tableAlias = "Flag_#flagid#">
						</cfif>
				
						<cfparam name="thisColumn.joinType" default = "left">
						
						<cfset flagType = theFlagSTructure.flagType.name>
						<cfset flagName = theFlagSTructure.Name>
						<cfset flagName = REReplace(replace(flagname," ","_","ALL"),"[\'\?\-`&\\\/\(\)]","","ALL") >
						<cfset entityType = theFlagSTructure.entityType.tablename>
						<!--- START: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
						<cfset entityTypeKey = theFlagGroupSTructure.entityType.uniqueKey>
						<!--- END: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
						
						<cfset dataTableName = theFlagSTructure.flagType.datatablefullname>
						<cfset viewDefinition.whichTables[#entityType#] = true>
						<cfset fieldName = "Flag_#entityType#_#flagType#_#flagid#">
						<cfset LongfieldName = left(flagname,30)>
						<cfif isDefined("thisColumn.field")>
							<cfset flagField = thisColumn.field>
							<cfset fieldName = fieldName & "_" & flagField>
							<cfset LongfieldName = LongfieldName & "_" & flagField>
						<cfelse>		
							<cfset flagField = "data">
						</cfif>
						<cfif isDefined("thisColumn.alias")>
							<cfset alias = thisColumn.alias>
						<cfelseif ((isDefined("thisColumn.textidAsAlias") and thisColumn.textidAsAlias) or textidAsAlias) and theFlagStructure.flagtextid is not "">
							<cfset alias = theFlagStructure.flagtextid>
						<cfelse>
							<cfset alias = longFieldName>	
						</cfif>
				
						<cfif isNumeric(alias)>
							<cfset alias = "[#alias#]">
						</cfif>
				

						<cfif listfindnocase(viewDefinition.aliasesUsed,alias) is 0>
				
							<cfset thisGridItem.name = alias>
							<cfset thisGridItem.header = alias>
					
							
							<cfif (flagType is "radio" or flagType is "checkbox") and flagField is "data">
								<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)# case when #tableAlias#.flagid is null then #booleanOff# else #booleanOn# end as #alias#")>
								<cfset thisGridItem.Type = "boolean">
							<cfelse>
								<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)# #tableAlias#.#flagField# AS #alias# " )>
							</cfif>
								<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef,"#chr(10)# 1 as #fieldname#")>
								
							<cfif listfind(viewDefinition.flagidsDone,flagid)	is 0 >
								<!--- START: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
								<cfset thisjoin = " #thisColumn.joinType# join #dataTableName#  as #tableAlias#  WITH (NOLOCK) on #left(entitytype,1)#.#entityTypeKey# = #tableAlias#.entityid and #tableAlias#.flagid = #flagid# ">
								<!--- END: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
								<cfset viewdefinition.joins = viewdefinition.joins & thisjoin>
								<cfset viewDefinition.flagidsDone = listappend(viewDefinition.flagidsDone,flagid)>
							</cfif>
				
							<cfif theFlagSTructure.linkstoentityTypeid IS NOT "">
								<!--- WAB 2009/06/18 LID2310 added a try catch.  Sometimes fails if validvalues require countries or entities, but nothing we can do about that--->
								<cftry>
									<cf_getValidValues validFieldName = "flag.#theFlagSTructure.flagTextid#">
										<cfset thisGridItem.values = valuelist(validvalues.datavalue)>
										<cfset thisGridItem.valuesDisplay = valuelist(validvalues.displayvalue)>						
	
									<cfcatch>
									</cfcatch>
								</cftry>			
							</cfif>
							
							<cfset viewDefinition.aliasesUsed = listappend(viewDefinition.aliasesUsed,alias)>
							<cfset viewDefinition.lastUpdatedFields = listappend(viewDefinition.lastUpdatedFields,"#tableAlias#.lastupdated" )>
				
						</cfif>
				
						
				
					<cfelseif thisColumn.type  is "flaggroup">		
				
						<cfset flaggroupid = thisColumn.id>
						<cfset theFlagGroupSTructure = application.com.flag.getFlagGroupStructure(flaggroupid)>		
						<cfset flaggroupid = theFlagGroupSTructure.flaggroupid>
						<cfset tableAlias = "Flaggroup_#flaggroupid#">
						<cfset flagGroupType = theFlagGroupSTructure.flagType.Name>
						<cfset flagGroupName = theFlagGroupSTructure.Name>
						<cfset flagGroupName = REReplace(replace(flaggroupname," ","_","ALL"),"[\'\?\-`&\\\/\(\)]","","ALL") >
						<cfset entityType = theFlagGroupSTructure.entityType.tablename>
						<cfset entityTypeKey = theFlagGroupSTructure.entityType.uniqueKey>
						<cfset dataTableName = theFlagGroupSTructure.flagType.datatablefullname>
						<cfset viewDefinition.whichTables[#entityType#] = true>

						<cfset fieldName = "Flaggroup_#entityType#_#flagGroupType#_#flaggroupid#">
						<cfset LongfieldName = "#flaggroupname#">
						<cfif isDefined("thisColumn.field")>
							<cfif flagGroupType is "event"><cfset longfieldname = "Event"><cfset additionaluniquekey  = "convert(varchar,isnull(#tableAlias#.flagid,0))+ char(45) "></cfif>  <!--- hack! --->
							<cfset flagField = thisColumn.field>
							<cfset fieldName = fieldName & "_" & flagField>
							<cfset LongfieldName = LongfieldName & "_" & flagField>
						<cfelse>		
							<cfset flagField = "data">
						</cfif>
						
						<cfif isDefined("thisColumn.alias")>
							<cfset alias = thisColumn.alias>
						<cfelseif (isDefined("thisColumn.textidAsAlias") and thisColumn.textidAsAlias) or arguments.textidAsAlias and theFlagGroupStructure.flaggrouptextid is not "">
							<cfset alias = theFlagGroupStructure.flaggrouptextid>
						<cfelse>
							<cfset alias = longFieldName>	
						</cfif>

						<cfif isNumeric(alias)>
							<cfset alias = "[#alias#]">
						</cfif>


						<cfif flagGroupType is "group">

							<!--- output all groups in the flaggroup --->
							<cfquery  name = "getflagGroups" datasource = "#application.sitedatasource#">
							select * from flagGroup where parentflaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</cfquery>
									
							<cfset tempColumn = duplicate(thisColumn)>
							<cfloop query="getFlagGroups">
								<cfset tempColumn.type="flaggroup">
								<cfset tempColumn.id = flaggroupid>

								<cfset structDelete (arguments,"thisColumn")>
								<cfset addTheColumnToTheViewAndGridDefinitions(argumentcollection = arguments, thisColumn = tempColumn)>
							</cfloop>
								<cfset thisGridItem.name = ""> 				<!--- need this to stop last item beign added to the grid twice --->


						
						<cfelseif (flagGroupType is "checkbox" and  ((isDefined("thisColumn.displayas") and thisColumn.displayas is "individualFlags") or (not isDefined("thisColumn.displayas") and showAllCheckboxesIndividually)))
								or (flagGroupType is "radio" and  (isDefined("thisColumn.displayas") and thisColumn.displayas is "individualFlags") or (not isDefined("thisColumn.displayas") and showAllRadiosIndividually))
						>
				
							<!--- output all items in the flaggroup --->
							<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
							select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</cfquery>
									
							<cfset tempColumn = duplicate(thisColumn)>
							<cfloop query="getFlags">
								<cfset tempColumn.type="flag">
								<cfset tempColumn.id = flagid>
								<!--- 2007/05/24 GCC temp hack as when in showAllCheckboxesIndividually mode applying these formatting params stops subsequent flags in the group being rendered --->
								<cfif FormattingParameters is not "" and not showAllCheckboxesIndividually>
									<cfset tempColumn = addFormattingToColumnStructure(tempColumn, FormattingParameters)>
								</cfif>

								<cfset structDelete (arguments,"thisColumn")>
								<cfset addTheColumnToTheViewAndGridDefinitions(argumentcollection = arguments, thisColumn = tempColumn)>
							</cfloop>
								<cfset thisGridItem.name = ""> 				<!--- need this to stop last item beign added to the grid twice --->
						
						<cfelseif listfindnocase(viewDefinition.aliasesUsed,alias) is 0>
				
								<cfset thisGridItem.name = "#alias#">
								<cfset thisGridItem.header = alias>
							
							<cfif flagGroupType is "radio" or flagGroupType is "checkbox">
								<cfif not isDefined("thisColumn.displayas") or thisColumn.displayas is "idlist">
									<cfif isDefined("thisColumn.displayas")>
										<cfset aliasSuffix = "">
									<cfelse>
										<cfset aliasSuffix = "_id"><!--- if no "displayas" is defined then we give both a name list and an id list and we need to distinguish between them--->
									</cfif>
									<!--- START: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
									<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)# dbo.BooleanFlagNumericList(#flaggroupid#,#left(entitytype,1)#.#ENTITYtypekey#) as #alias##aliasSuffix#")>
									<!--- END: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
									<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #fieldname#")>
					
									<cfset thisGridItem.name = "#alias#">
									<cfset thisGridItem.header = alias>
								</cfif>
						
								<cfif not isDefined("thisColumn.displayas") or thisColumn.displayas is "namelist">
									<!--- START: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
									<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)# dbo.BooleanFlagList(#flaggroupid#,#left(entitytype,1)#.#ENTITYtypekey#,default) as #alias#")>
									<!--- END: 2008-10-30	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->	
									<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef,"#chr(10)# 1 as #fieldname#_text")>
								</cfif>
								
					
								<!--- get all radio items --->
									<cfif flagGroupType is "radio">
										<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
										select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
										</cfquery>
									
										<cfset thisGridItem.values = valuelist(getflags.flagid)>
										<cfset thisGridItem.valuesDisplay = valuelist(getflags.name)>						
									</cfif>
							
				
						
							<cfelseif flaggrouptype is "event">
								<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)# #tableAlias#.#flagField# AS #alias# " )>
								<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef,"#chr(10)# 1 as #fieldname#")>
								
								<cfif listfind(viewDefinition.flaggroupidsDone,flaggroupid)	is 0 >
									<cfset thisjoin = " left join (#datatablename#  as #tableAlias# inner join flag f on #tableAlias#.flagid = f.flagid and f.flaggroupid = #flaggroupid#) on #left(entitytype,1)#.#ENTITYtype#rid = #tableAlias#.entityid ">
									<cfset thisjoin = " left join (select x	.*, f.name from #datatablename#  x inner join flag f on x.flagid = f.flagid and f.flaggroupid = #flaggroupid#) as #tableAlias# on #left(entitytype,1)#.#ENTITYtype#rid = #tableAlias#.entityid ">
									<cfset viewdefinition.joins = viewdefinition.joins & thisjoin>
									<cfset viewDefinition.flaggroupidsDone = listappend(viewDefinition.flaggroupidsDone,flaggroupid)>
								</cfif>
						
							<cfelse>
								<!--- output all items in the flaggroup --->
								<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
								select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
								</cfquery>
								
								<cfset currentColumn = thisColumn>
								<cfloop query="getFlags">
									<cfset thisColumn.type="flag">
									<cfset thisColumn.id = flagid>
									<cfset addTheColumnToTheViewAndGridDefinitions(argumentcollection = arguments, thisColumn = thisColumn)>
					
								
								</cfloop>
							
							
								
							</cfif>
				
							<cfset viewDefinition.aliasesUsed = listappend(viewDefinition.aliasesUsed,alias)>
				
						</cfif>
				
							
					<cfelseif thisColumn.type  is "Value">					
							<cfset theValue = thisColumn.value>
							<cfif structKeyExists(thisColumn,"name")>
								<cfset theFieldname = thisColumn.name>
							<cfelse>
								<cfset theFieldname = "avalue"> 
							</cfif>
							
							
							<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)#'#theValue#' as #theFieldName#")>
							<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef,"#chr(10)# 1 as #theFieldName#")>
				
					<!--- 2007/05/24 - GCC - I think this is a naughty hack and I should be shot by Will. Talk to me Will but this enables us to run a subquery as part of the select statement... --->
					<cfelseif thisColumn.type  is "sql">					
							<cfset thesql = thisColumn.value>
							<cfset theFieldname = thisColumn.id>
							<cfset thisGridItem.name = thisColumn.id>
							<cfset thisGridItem.header = thisColumn.id>
							
							<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#chr(10)# (#thesql#) as #theFieldName#")>
							<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef,"#chr(10)# 1 as #theFieldName#")>
				
				
						
					</cfif>
					
				
					<cfif thisGridItem.name is not "">			
						<cfif thisGridItem.name contains "updated" or thisGridItem.name contains "created">
							<cfset thisGridItem.select = "No">
						</cfif> 
						<cfset arrayAppend(gridDefinition.gridarray,structCopy(thisGridItem))>
					</cfif>		

	</cffunction>


	<cffunction access="private" name="blankReportColumn" hint="" >

		<cfset var result = structNew()>
		<cfset result.name = ""> 
		<cfset result.header = ""> 
		<cfset result.type = "STRING_NOCASE"> 
		<cfset result.values = ""> 
		<cfset result.width = "0"> 
		<cfset result.select = "yes"> 
		<cfset result.display = "yes"> 
		<cfset result.canOrderBy = "false"> 
		<cfset result.valuesdisplay = ""> 
		
		<cfreturn result>
	
	</cffunction>
	
	<!--- takes additional name value pairs and adds them to the column definition structure --->
	<cffunction access="private" name="addFormattingToColumnStructure" hint="" >
		<cfargument name="columnStruct" type="struct" required="true">
		<cfargument name="FormattingString" type="string" required="true">

				<!--- special hack to get individual formatting info to come from a screen definition
					just make the parameter start with "VIEW_"
					eg: view_displayAs = idlist"
				--->

			<cfset var key = "">
			<!--- WAB 2008/10/06 Add use of the dot notation so that all formatting parameters todo with views appear as a subStructure called view,
				 once this has been checked and released we can remove the bit that looks for "view_", but will need to update screen 469 on Lexmark--->
			<cfset var theStruct = application.com.regExp.convertNameValuePairStringToStructure (inputString = FormattingString,delimiter = ",", DotNotationToStructure = true)>

				<cfif structKeyExists (theStruct , "view")>
					<cfset columnStruct = theStruct.view > 
				</cfif>
			
				<cfloop collection = "#theStruct#" item = "key">
					<cfif listFirst(key,"_") is "view">
						<cfset columnStruct[listRest(key,"_")] = theStruct[key]>						
					</cfif>
				</cfloop>

			<cfreturn columnStruct>
			
	</cffunction> 



	<cffunction access="public" name="createViewFromDefinition" hint="" >
		<cfargument name="viewDefinition" type="struct" required="true">
		<cfargument name="viewName" type="string" required="true">
		<cfargument name="viewFilter" type="string" default = "">
		<cfargument name="FilterArray" type="array" default = "#arrayNew(1)#"> <!--- can't remember what this is --->
		<cfargument name="reCreateStoredProcedure" type="boolean" default="false">
		<cfargument name="updateable" type="boolean" default = "false">
		<cfargument name="outputScripts" type="boolean" default="false">
		<cfargument name="viewComment" type="string" default ="">
		
		<cfset var result = structNew()>

		<cfset result.isOK = true>
				
	<cftry>
	<cftransaction>
	
<cfset query = "	create view dbo.#arguments.viewName#  as 
/* 
AutoGenerated View (Removing this line will prevent this view being overwritten by the view creation tool)
#viewComment#
*/
							
select #viewdefinition.selectList#, 0 as updatedby 
from #viewdefinition.joins#"
>
							
		<cfif viewFilter is not "">
			<cfset query = query & " where #viewFilter#">
		<cfelseif arraylen(filterArray) is not 0>
			<cfset query = query & " where ">
			<cfloop index= "I" from = 1 to = #arrayLen(filterArray)# >
					<cfset query  = query  & filterArray[I].sql & " and ">
			</cfloop>
					<cfset query  = query  &  " 1 = 1 ">
		</cfif>					
							
	<cfif outputScripts><CFOUTPUT>#QUERY#</cfoutput> <BR><BR><BR></cfif> 
	
			<cfquery name="dropView" datasource = "#application.sitedatasource#">
			if exists (select * from information_schema.tables where table_type = 'view' and table_name =  <cf_queryparam value="#viewname#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					if exists (select * from sysobjects so inner join syscomments sc on so.id = sc.id where name =  <cf_queryparam value="#viewname#" CFSQLTYPE="CF_SQL_VARCHAR" >  and sc.text like '%AutoGenerated View%')
						begin
							drop view #viewname#
							select 1 as done
						end
					else
							select 0 as done
							
				end	
			else
				select 1 as done
			</cfquery>

			<cfif dropView.done is 0>
				<cfset result.isOK = false>
				<cfset result.error = "Existing View With Same Name Cannot Be Deleted">
				<cfreturn result>
			</cfif>
			
			<cfquery name="createView" datasource = "#application.sitedatasource#">
			#preserveSingleQuotes(query)#
			</cfquery>
			
		
	
		<cfif updateable>
	
			<cfset query = "	create view dbo.#viewName#_def as 
								select #viewdefinition.selectListDef# , 0 as updatedby 
								from dual">
							
	
			<cfif outputScripts><CFOUTPUT>#htmleditformat(QUERY)#</cfoutput> <BR><BR><BR></cfif>
	
			<cfquery name="dropView" datasource = "#application.sitedatasource#">
			if exists (select * from information_schema.tables where table_type = 'view' and table_name =  <cf_queryparam value="#viewname#_def" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					drop view #viewname#_def
				end	
			</cfquery>
		
			<cfquery name="createView" datasource = "#application.sitedatasource#">
			#query#
			</cfquery>
			
			<cfquery name="createTrigger" datasource = "#application.sitedatasource#">
			create trigger #viewName#InsteadOf on #viewName# instead of update,insert,delete
			as 
		
			select * into ##D from deleted
			select * into ##I from inserted
		
			exec updateDynamicView '#viewName#'  
			</cfquery>
		</cfif>
	
	</cftransaction>	
	
	<cfcatch>
		<cfrethrow>
	</cfcatch>	
	</cftry>

		
		<cfif updateable>
			<cfset reCreateStoredProcedure = arguments.reCreateStoredProcedure>
		
			<cfif not reCreateStoredProcedure>
					<cfquery name="checkForSP" datasource = "#application.sitedatasource#">
						select * from information_schema.routines where routine_type = 'procedure' and routine_name = 'updateDynamicView'
					</cfquery>
					<cfif checkForSP.recordcount is 0 >
						<cfset reCreateStoredProcedure = true>
					</cfif>
				
			</cfif>


			<cfif reCreateStoredProcedure>

						<cfquery name="dropSP" datasource = "#application.sitedatasource#">
						if exists (select * from information_schema.routines where routine_type = 'procedure' and routine_name = 'updateDynamicView')
							begin
								drop procedure updateDynamicView
							end	
						</cfquery>


						<cfquery name="createSP" datasource = "#application.sitedatasource#">
						
						/*   
						A stored procedure for updating views made up of a mixture of tables and flags
						
						The views have to be created in a particular way - currently using createview.cfm
						
						Basically each view has an associated view (myView_def) which defines where each field comes from
						
						
						
						*/
						
						
						
						create procedure dbo.updateDynamicView 
							@viewName varchar(50)
						
						as 
						
							declare	
							@ColName	varchar(80),
							@ColIdentifier	varchar(80),
							@Colid		int,
							@mod8 int,
							@rem8 int,
							@bit int,
							@bitvalue int,
							@byte int,
							@flagid varchar(10),
							@flaggroupid varchar(10),
							@flagtype varchar(30),
							@entitytype varchar(30),
							@flagtable varchar(30),
							@tableType varchar(30),
							@fieldname varchar(30),
							@rest varchar(80),
							@sqlcmd		nvarchar(2000),
							@pointer1 int,
							@pointer2 int,
							@theDate varchar(25),
							@Item1 varchar(30),
							@Item2 varchar(30),
							@Item3 varchar(30),
							@Item4 varchar(30),
							@Item5 varchar(30)
						
							set nocount on 
						
							select @theDate = '''' + convert(varchar,getDate()) +'''' 
						
						--	select * from ##d
						--	select * from ##i
						
						--	print getdate()
						
							declare cols insensitive cursor for
							Select c.colid, c.name, c2.name
								from (sysobjects s 
									inner join 
							              syscolumns c on s.id =c.id and s.name= @viewName and s.type ='V' )
									inner join 
								      (sysobjects s2 
									inner join 
							              syscolumns c2 on s2.id =c2.id and s2.name= @viewName + '_def'  and s2.type ='V' )			
									on c.colid = c2.colid
						
						-- and charindex('_',c.name) <> 0
							order by c.colid
								
							open cols
						
						
							print COLUMNS_UPDATED()
							fetch next from cols into 
								@colid,@ColName,@colIdentifier
						
							while @@fetch_status = 0 
							begin
						
						--		print @colid
						--		print @colname
						
						       if charindex('_',@colIdentifier) <> 0
						 	  begin
						
						
								select @bit = (@colid - 1 )% 8 + 1  -- refers to which bit in the byte
								select @bitvalue = power(2,@bit-1)  
								select @byte = ((@colid - 1) / 8) + 1  -- refers to which byte in columns_updated
								
						
						--		print @colname +': byte ' + convert(varchar,@mod8) + '.  bit ' + convert(varchar,@rem8)
								
						--		print @colname
						--		print 'bit :' + convert(varchar,((@colid - 1 )% 8 + 1) )
						--		print 'bit :' + convert(varchar,power(2, ((@colid - 1 )% 8 + 1) - 1) )
						--		print 'pmc: ' + convert(varchar,power(2,(@rem8-1)))
						--		print 'byte :' + convert(varchar,((@colid - 1) / 8) + 1)
						--		print @bit
						--		print @byte
						
						
						
								if SUBSTRING(COLUMNS_UPDATED(),@byte,1) & @bitvalue > 0
								begin
									print 'update ' + @colname + ' = ' + @colIdentifier
									
									select @rest = @colIdentifier
									Select @TableType = left(@rest,charindex('_',@rest)-1)
									select @rest = right (@rest,len(@rest) - charindex('_',@rest)) + '_'
						
									Select @Item2 = left(@rest,charindex('_',@rest)-1)
									select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'
						
									Select @Item3 = left(@rest,charindex('_',@rest)-1)
									select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'
						
									Select @Item4 = left(@rest,charindex('_',@rest)-1)
									select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'
						
									Select @Item5 = left(@rest,charindex('_',@rest)-1)
									select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'
						
						
						--			print 'table ' + @tableType
						--			print 'rest ' + @rest
						
						
								  if @tableType = 'Flag'
								  begin
												
									Select @EntityType = @Item2
									Select @flagtype = @Item3 
									Select @flagid = @Item4
									select @fieldname = @Item5
						
									if @fieldname = ''
										select @fieldname = 'data'
						
									print 'updating flag: ' + @entityType + ' ' + @flagType + ' ' +@flagid + ' ' +@fieldname
						
						
						--			select @flagid = right (@colname,len(@colname) - charindex('_',@colname))
						
						
									if @flagtype not in ('radio','checkbox')
									begin
										select @flagTable = @flagType + 'flagdata'			
									  	IF @fieldname = 'data'	
										  begin -- flags with single data columns , need to be inserted and deleted if the old/new value is null, else do an update
											select @sqlcmd = 'update ' + @flagtable
												+ ' set ' +@fieldname + ' = i.' + @colname
												+ ' ,lastupdated = ' + @theDate
												+ ' ,lastupdatedby = i.updatedby '
												+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
												+ ' inner join ' +  @flagtable +  ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
												+ ' and i.' + @colname + ' <> d.' + @colname 
						--						+ ' and d.' + @colname + ' is not null'
												+ ' and i.' + @colname + ' is not null'
							
											exec (@sqlcmd)
								
											select @sqlcmd = 'insert into ' + @flagtable + ' (entityid,flagid,createdby,created,lastupdatedby,lastupdated' +@fieldname + ')'
												+ ' select i.' + @entityType + 'id, i.updatedby,' + @theDate + 'i.updatedby,' + @theDate + ',' + @flagid + ', i.' + @colname
						 						+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
												+ ' and i.' + @colname + ' is not null'
												+ ' and d.' + @colname + ' is null'
						
											exec (@sqlcmd)
						
											-- set lastupdatedby before deleting
						
											select @sqlcmd = 'update ' + @flagtable 
												+ 'set lastupdatedby = i.updatedby , lastupdated = ' + @theDate  
												+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
												+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
												+ ' and d.' + @colname + ' is not null'
												+ ' and i.' + @colname + ' is null'
						
											exec (@sqlcmd)
											
											select @sqlcmd = 'delete ' + @flagtable 
												+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
												+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
												+ ' and d.' + @colname + ' is not null'
												+ ' and i.' + @colname + ' is null'
							
											exec (@sqlcmd)
								
										  end   -- end of flags with a single data column
										else
										  begin  -- flags with multiple data columns , don't delete if a field is null, may need to do inserts
						       	 					
											select @sqlcmd = 'insert into ' + @flagtable + '(flagid,entityid,createdby,created,lastupdatedby,lastupdated' + @fieldname + ')'
													+' select distinct ' + @flagid + ',i.' + @entityType + 'id, i.updatedby,' + @theDate + ',i.updatedby,' + @theDate + ', i.' + @colname
													+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
													+ ' left join ' +  @flagtable +  ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
													+ ' where isnull(i.' + @colname + ','''') <> isnull(d.' + @colname + ','''')'
													+ ' and fd.flagid is null  '
						
						--				print @sqlcmd
										exec (@sqlcmd)
						
						
											select @sqlcmd = 'update ' + @flagtable
													+ ' set ' +@fieldname + ' = i.' + @colname
													+ ' ,lastupdatedby = i.updatedby , lastupdated = ' + @theDate  
													+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
													+ ' inner join ' +  @flagtable +  ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
													+ ' where isnull(i.' + @colname + ','''') <> isnull(d.' + @colname + ','''')'
							
						--				print @sqlcmd
										exec (@sqlcmd)
							
										  
						
										  end
						
						
							end  -- end of other flags
									  else
									begin  -- booleanflagdata flags
						
									select @flagtable = 'booleanflagdata'
						
									select @sqlcmd = 'insert into ' + @flagtable + ' (entityid,flagid,createdby,created,lastupdatedby,lastupdated)'
										+ ' select distinct i.' + @entityType + 'id, ' + @flagid + ',i.updatedby,' + @theDate + ',i.updatedby,' + @theDate 
										+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
										+ ' and isnull(i.' + @colname + ',0) = 1 '
										+ ' and isnull(d.' + @colname + ',0) = 0 '
						
									exec (@sqlcmd)
						
									select @sqlcmd = 'update ' + @flagtable 
										+ ' set lastupdatedby = i.updatedby , lastupdated = ' + @theDate  
										+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'				
										+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
										+ ' and isnull(i.' + @colname + ',0) = 0 '
										+ ' and isnull(d.' + @colname + ',0) = 1 '
						
									exec (@sqlcmd)
									
									select @sqlcmd = 'delete ' + @flagtable 
										+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'				
										+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
										+ ' and isnull(i.' + @colname + ',0) = 0 '
										+ ' and isnull(d.' + @colname + ',0) = 1 '
						
									exec (@sqlcmd)
									end
						
						
									
						
						
								  end  -- end of flag columns
								  else IF @tableType in ('person','location','organisation')
								  begin  -- update a main table column
						
									select @fieldname = @item2
						
										print 'update table ' + @tableType + ' ' + @fieldname
											select @sqlcmd = 'update ' + @tableType
												+ ' set ' +@fieldname + ' = i.' + @colname
												+ ' ,lastupdatedby = i.updatedby ,lastupdated = ' + @theDate
												+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
												+ ' inner join ' +  @tableType +  ' x on x.' + @tableType + 'id = i.' + @tableType + 'id '
												+ ' and i.' + @colname + ' <> d.' + @colname 
							
											exec (@sqlcmd)
						
						
									
						
								  end
						
						
						
								  else IF @tableType = 'FlagGroup'
								  begin  -- update a FlagGroup
						
									Select @EntityType = @Item2
									Select @flagtype = @Item3
									Select @flaggroupid = @Item4
									select @fieldname = @Item5
						
									if @flagType = 'checkbox' or @flagType = 'radio' 
									begin
										select @flagtable = 'booleanflagdata'	
						
									-- delete items in deleted but not in inserted
									select @sqlcmd = 'delete booleanflagdata  '
										+ ' from '
										+ '  ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
										+ ' inner join flag f on (dbo.listfind(i.'+@colname+',f.flagid) = 0 and dbo.listfind(i.'+@colname+',f.name) = 0)and f.flaggroupid = ' + @flaggroupid
										+ ' and (dbo.listfind(d.'+@colname+',f.flagid) = 1 or dbo.listfind(d.'+@colname+',f.name) = 1)' 
										+ ' inner join booleanflagdata bfd on bfd.flagid = f.flagid and bfd.entityid = i.' + @entityType + 'id ' 
										
						--			print @sqlcmd
									exec (@sqlcmd)
												
									-- insert items not already in the table (i.e in the inserted but  not in deleted)
									select @sqlcmd = 'insert into ' + @flagtable + ' (entityid,flagid)'
										+ ' select distinct i.' + @entityType + 'id, f.flagid '
										+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
										+ ' inner join flag f on (dbo.listfind(i.'+@colname+',f.flagid) =1 or dbo.listfind(i.'+@colname+',f.name) =1) and f.flaggroupid = ' + @flaggroupid
										+ ' and (dbo.listfind(d.'+@colname+',f.flagid) = 0 and dbo.listfind(d.'+@colname+',f.name) = 0)' 
						
						--			print @sqlcmd
									exec (@sqlcmd)
						
						
						
						
						
									end
						
									
						
								  end
						
						
								end  -- end of updated columns
						
						
							  end	-- end of column names with _ in them
								fetch next from cols into 
									@colid,@ColName,@colIdentifier
							end  -- end of while loop
							
							close cols
							deallocate cols
						
						
						</cfquery>
			</cfif >
		</cfif>
			
		<cfreturn result>
	</cffunction>

</cfcomponent>
	

