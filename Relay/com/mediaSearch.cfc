<!--- �Relayware. All Rights Reserved 2014 --->

<cfcomponent displayname="mediaSearch" hint="Functions for Media_Search relaytag">

	<cffunction name="getMediaLibrary" access="public" output="false" returnType="string">
		<cfargument name="pageNumber" type="numeric" default="1">

		<cfargument name="filesQuery" type="query" required="false">
		<cfargument name="hasMore" type="boolean" required="false">
		<cfargument name="random" type="string" required="false" default="#replace(CreateUUID(),'-','','ALL')#">

		<cfset var files = getMediaLibraryFiles(argumentCollection=arguments)>
		<cfset arguments.totalrecordcount=files.totalrecordcount>

		<cfset arguments.filesQuery = files.recordSet>
		<cfset arguments.hasMore = files.hasMore>

		<cfreturn displayMediaLibRows(argumentCollection=arguments)>
	</cffunction>

		<cffunction name="getMediaLibraryFiles" access="public" output="false" returnType="struct">
		<cfargument name="pageNumber" type="numeric" default="1">
		<cfargument name="pageSize" type="numeric" default="10">
		<cfargument name="queryID" type="string" default="">
		<cfargument name="cacheQuery" type="boolean" default="true">
		<cfargument name="searchTextString" type="string" default="">
		<cfargument name="fileTypeGroupID" type="string" default="">
		<cfargument name="flagTextIDs" type="string" default="">
		<cfargument name="languages" type="string" default="">
		<cfargument name="fileExtensions" type="string" default="">
		<cfargument name="flagID" type="string" default="0">
		<cfargument name="fileext" type="string" default="">
		<cfargument name="keywordsProducts" type="string" default="">
		<cfargument name="sortOrder" type="string" default="">

		<cfset var qSearchResultSQL="">
		<cfset var qSources="">
		<cfset var qSearchResult="">
		<cfset var recordSet="">
		<cfset var result={recordSet="",hasMore=false}>
		<cfset var tempTable = "####mediaLibraryFiles_#arguments.random#">

		<cfif arguments.queryID neq "">
			<cfset tempTable = arguments.queryID>
		</cfif>

		<cfif not arguments.cacheQuery or arguments.pageNumber eq 1>
			
			<cfset var langFilter = "">
			<cfset var langList = "">
			<cfloop list="#arguments.languages#" index="local.langId">
				<cfif local.langId neq "">
					<cfset langList = listAppend(langList, local.langId)>
				</cfif>
			</cfloop>
			<cfif listLen(langList) gt 0>
				<cfset langFilter = " and f.languageId in (" & langList & ")">
			</cfif>

			<cfset var extFilter = "">
			<cfset var extList = "">
			<cfloop list="#arguments.fileExtensions#" index="local.extension">
				<cfif local.extension neq "">
					<cfset extList = listAppend(extList, "'" & local.extension & "'")>
				</cfif>
			</cfloop>
			<cfif listLen(extList) gt 0>
				<cfset extFilter = " and RIGHT(f.filename, (CHARINDEX('.', REVERSE(f.filename), 0) - 1)) in (" & extList & ")">
			</cfif>

			<!--- SOLR search occurs inside getFileList --->
			<cfset qSearchResultSQL = application.com.filemanager.getFilesAPersonHasCountryViewRightsTo(
				searchString=arguments.searchTextString,
				returnType="sql",
				pageNumber=pageNumber,
				fileext=fileext,
				keywordsProducts=keywordsProducts,
				fileTypeGroupID=fileTypeGroupID,
				flagTextIDs=flagTextIDs,
				flagID=flagID,
				checkExpires=1,
				orderby=sortOrder,
				filter="portalSearch = 1 and DefaultDeliveryMethod = 'Local' and fT.path is not null" & langFilter & extFilter) /> <!--- is filename not null is removed from here --->
		</cfif>

		<cfif not arguments.cacheQuery>
			<cfquery name="recordSet" datasource="#application.sitedataSource#">
				#preserveSingleQuotes(qSearchResultSQL)#
			</cfquery>
			<cfset result.recordSet = recordSet>
			<cfset result.totalrecordcount = recordSet.recordCount>
		<cfelse>
			<cfif arguments.pageNumber eq 1>
				<!--- if we're refreshing the feed, then delete the existing cached query and refresh it --->
				<cfset application.com.dbTools.deleteCachedQuery(tempTable=tempTable)>
				<cfset structAppend(result,application.com.dbTools.dbCacheQuery(tempTable=tempTable,queryString=qSearchResultSQL,pageSize=pageSize),true)>
			<cfelse>
				<cfset structAppend(result,application.com.dbTools.getRecordsFromCachedQuery(tempTable=tempTable,pageNumber=arguments.pageNumber),true)>
			</cfif>
		</cfif>

		<cfset result.tablename=tempTable>
		<cfreturn result>

	</cffunction>

	<cffunction name="getHeaderColumnView" access="private" returnType="string" output="false">
		<cfargument name="label" type="string" required="true">
		<cfargument name="field" type="string" required="true">
		<cfargument name="sortOrder" type="string" default="">
		<cfargument name="args" type="struct" required="true">

		<cfset var sortOrderOnly = "">
		<cfif findNoCase(arguments.field, arguments.sortOrder)>
			<cfset sortOrderOnly = findNoCase("#arguments.field# desc", arguments.sortOrder) ? "asc" : "desc">
		</cfif>
		<cfsavecontent variable="local.headerColumnView">
			<cfoutput>
				<cfswitch expression="#sortOrderOnly#">
					<cfcase value="desc">
						<cfset arguments.args.sortOrder = "#arguments.field# desc">
						<a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments.args)#')" title="Sort Z-A">
							#label#
							<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
						</a>
					</cfcase>
					<cfcase value="asc">
						<cfset arguments.args.sortOrder = "#arguments.field# asc">
						<a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments.args)#')" title="Sort A-Z">
							#label#
							<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
						</a>
					</cfcase>
					<cfdefaultcase>
						<cfset arguments.args.sortOrder = "#arguments.field# asc">
						<a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments.args)#')" title="Sort A-Z">#label#</a>
					</cfdefaultcase>
				</cfswitch>
			</cfoutput>
		</cfsavecontent>

		<cfreturn local.headerColumnView>
	</cffunction>

	<cffunction name="displayMediaLibRows" access="public" returnType="string" output="false">
		<cfargument name="filesQuery" type="query" required="true">
		<cfargument name="pageNumber" type="numeric" required="false">
		<cfargument name="hasMore" type="boolean" required="false">
		<cfargument name="totalrecordcount" type="numeric" default="0" required="false">
		<cfargument name="PagingType" type="string" default="Infinity" required="true">
		<cfargument name="DataShowList" type="string" required="true">
		<cfargument name="DataShowGrid" type="string" required="true">
		<cfargument name="showThumbnailAsMainImage" type="boolean" default="true" required="true">
		<cfargument name="showMediaGroupAsMainImage" type="boolean" default="true" required="true">
		<cfargument name="showResourceTypeAsMainImage" type="boolean" default="true" required="true">
		<cfargument name="GridColumns" type="numeric" default="3" required="true">
		<cfargument name="dateformatmask" type="string" default="MM-DD-YYYY" required="true">
		<cfargument name="pathForMediaGroup" type="string" default="resourcelibrary/mediagroup/" required="true">
		<cfargument name="pathForFileType" type="string" default="resourcelibrary/filetype/" required="true">
		<cfargument name="disableListView" type="boolean" default="false" required="false">
		
		<cfset var DataShow=listAppend(arguments.DataShowGrid,arguments.DataShowList)>
		<cfset var mediaLibRows = "">
		<cfset var variables = {}>
		<cfset var thisDataShowListCols = "">
		<cfset var nhref = "">
		<cfset var i = "">

		<cfif arguments.filesQuery.recordCount>
			<cfsavecontent variable="mediaLibRows">
				<cfoutput>
					<cfif pageNumber eq 1 or arguments.PagingType neq "Infinity">
						<h2 class="mla-results-title">phr_mla_resource_library_results #arguments.totalrecordcount# phr_mla_resource_library_results_found</h2>

						<div id="resultsTitle" class="resultsTitle clearfix">
							<cfif not disableListView>
								<div class="gridlist">
									<div class="btn-group btn-group-sm">
										<a href="##" id="list" class="btn btn-default btn-sm">
											<span class="fa fa-bars"></span>
											phr_mla_List
										</a>
										<a href="##" id="grid" class="btn btn-default btn-sm">
											<span class="fa fa-th"></span>
											phr_mla_Grid
										</a>
									</div>
								</div>
							</cfif>
							<div class="backToTop">
								<a href="##">
									<p>phr_MLA_backToTop</p>
									<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
								</a>
							</div>

							<div class="row resultsHeader">
								<cfset var DataShowColsList = 12/listLen(DataShowList)>
								<cfset var DataShowColsListFloor = fix(DataShowColsList)>
								<cfset var DataShowColsListRemainder = 0>
								<cfif DataShowColsListFloor lt DataShowColsList>
									<cfset DataShowColsListRemainder = (DataShowColsList-DataShowColsListFloor)*listLen(DataShowList)>
								</cfif>

								<cfset var args = duplicate(arguments)>
								<cfset args.pageNumber = 1>

								<cfset var firstCol=true>
								<cfif listfindnocase(DataShow,'FileID')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'FileID') ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'FileID') ? '' : 'hiddenGrid')#">
										#getHeaderColumnView("phr_mla_FileID", "f.FileID", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'Title')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'Title') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Title') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_title", "f.name", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'Thumbnailresourceimage') and (arguments.showThumbnailAsMainImage or arguments.showMediaGroupAsMainImage or arguments.showResourceTypeAsMainImage)>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'Thumbnailresourceimage') gt 0 ?'' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Thumbnailresourceimage') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										<span class="headerTitle">phr_mla_Image</span>
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'MediaGroup')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'MediaGroup') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'MediaGroup') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Media_Group", "heading", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'ResourceType')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'ResourceType') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'ResourceType') gt 0 ? '' : 'hiddenGrid')#"  listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Resource_Type", "type", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'FileExtension')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'FileExtension') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'FileExtension') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_File_Extension", "RIGHT(f.filename, (CHARINDEX('.', REVERSE(f.filename), 0) - 1))", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'FileSize')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'FileSize') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'FileSize') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_File_Size", "FileSize", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'Description')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<cfif listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = thisDataShowListCols +DataShowColsListRemainder /><!--- 2016-09-27 STCR Description width --->
										<cfset firstCol = false>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'Description') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Description') gt 0 ? '' : 'hiddenGrid')#"  listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Description", "f.description", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'language')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'language') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'language') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Language", "[Language]", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'source')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'source') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'source') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Source", "source", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'PublishDate')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'PublishDate') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'PublishDate') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Publish_Date", "f.publishDate", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'CreatedDate')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'CreatedDate') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'CreatedDate') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Created_Date", "f.created", arguments.sortOrder, args)#
									</div>
								</cfif>
								<cfif listfindnocase(DataShow,'Lastupdated')>
									<cfif firstCol and not listfindnocase(DataShow,'Description')>
										<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
										<cfset firstCol = false>
									<cfelse>
										<cfset thisDataShowListCols = DataShowColsListFloor>
									</cfif>
									<div class="col-xs-#thisDataShowListCols# #(listfindnocase(DataShowList,'Lastupdated') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Lastupdated') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
										#getHeaderColumnView("phr_mla_Last_Updated", "f.lastUpdated", arguments.sortOrder, args)#
									</div>
								</cfif>
							</div>

						</div>
						<script>
							jQuery(function(jQuery) {
								jQuery(document).ready(function(){
									loadBackToTop();
								});
							});
						</script>
					</cfif>
					<cfloop query="arguments.filesQuery">
						<cfset nhref = "javascript:getFile(#fileID#)">
						<cfset variables.fileextension = trim(listlast(fileurl,".")) />

						<cfset DataShowColsList = 12/listLen(DataShowList)>
						<cfset DataShowColsListFloor = fix(DataShowColsList)>
						<cfset DataShowColsListRemainder = 0>
						<cfif DataShowColsListFloor lt DataShowColsList>
							<cfset DataShowColsListRemainder = (DataShowColsList-DataShowColsListFloor)*listLen(DataShowList)>
						</cfif>

						<cfset var DataShowColsGrid = 12/listLen(DataShowGrid)>
						<cfset var DataShowColsGridFloor = fix(DataShowColsGrid)>
						<cfset var DataShowColsGridRemainder = 0>
						<cfif DataShowColsGridFloor lt DataShowColsGrid>
							<cfset DataShowColsGridRemainder = (DataShowColsGrid-DataShowColsGridFloor)*listLen(DataShowGrid)>
						</cfif>

						<Cfset var bsGridCols = 12/arguments.GridColumns>

						<div class="resultsRow col-xs-12 col-sm-6 col-md-#bsGridCols#" xClass="col-xs-12 col-sm-6 col-md-#bsGridCols#">
							<a href="#nhref#" title="phr_Download #name#">
								<div class="insideAnchor">

									<cfset var firstCol=true>
									<cfif listfindnocase(DataShow,'FileID')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-id #(listfindnocase(DataShowList,'FileID') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'FileID') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#fileID#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'Title')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-title #(listfindnocase(DataShowList,'Title') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Title') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#name#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'Thumbnailresourceimage') and (showThumbnailAsMainImage or showMediaGroupAsMainImage or showResourceTypeAsMainImage)>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-img #(listfindnocase(DataShowList,'Thumbnailresourceimage') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Thumbnailresourceimage') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											<cfset variables.imageshown = false />
											<cfif showThumbnailAsMainImage>
												<cfif fileexists("#application.paths.content#\linkImages\files\#fileID#\Thumb_#fileID#.png")>
													<div class="mla-image">
														<img src="/content/linkImages/files/#fileID#/Thumb_#fileID#.png" alt="" />
													</div>
													<cfset variables.imageshown = true />
												<cfelseif fileexists("#application.paths.content#\linkImages\files\#fileID#\Thumb_#fileID#.jpg")>
													<div class="mla-image">
														<img src="/content/linkImages/files/#fileID#/Thumb_#fileID#.jpg" alt="" />
													</div>
													<cfset variables.imageshown = true />
												<cfelseif listFindNoCase('png,jpg,jpeg',variables.fileextension) and arguments.PagingType neq "Infinity">
													<div class="mla-image" >
														<img src="#fileurl#" alt="" />
													</div>
													<cfset variables.imageshown = true />
												</cfif>
											</cfif>
											<cfif showMediaGroupAsMainImage and not variables.imageshown>
												<cfif fileexists("#application.paths.content#\#replace(pathForMediaGroup,"/","\","all")##heading#.png")>
													<div class="mla-image">
														<img src="/content/#pathForMediaGroup#/#heading#.png" alt="#heading#" />
													</div>
													<cfset variables.imageshown = true />
												</cfif>
											</cfif>
											<cfif showResourceTypeAsMainImage and not variables.imageshown>
												<cfset variables.fileextension = trim(listlast(fileurl,".")) />
												<cfif fileexists("#application.paths.content#\#replace(pathForFileType,"/","\","all")##variables.fileextension#.png")>
													<div class="mla-image">
														<img src="/content/#pathForFileType##variables.fileextension#.png" alt="" />
													</div>
													<cfset variables.imageshown = true />
												</cfif>
											</cfif>
											<cfif not variables.imageshown>
												<cfif fileexists("#application.paths.content#\resourcelibrary\images\medialibrarydefault.png")>
													<div class="mla-image">
														<img src="/content/resourcelibrary/images/medialibrarydefault.png" alt="" />
													</div>
													<cfset variables.imageshown = true />
												<cfelse>
													<div class="mla-image">
														&nbsp;
													</div>
												</cfif>
											</cfif>
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'MediaGroup')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-media #(listfindnocase(DataShowList,'MediaGroup') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'MediaGroup') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#heading#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'ResourceType')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-resource #(listfindnocase(DataShowList,'ResourceType') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'ResourceType') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#type#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'FileExtension')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-file #(listfindnocase(DataShowList,'FileExtension') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'FileExtension') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#variables.fileextension#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'FileSize')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-resource #(listfindnocase(DataShowList,'FileSize') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'FileSize') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#NumberFormat(FileSize)# phr_mla_kb
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'Description')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<cfset thisDataShowListCols = thisDataShowListCols +DataShowColsListRemainder /><!--- 2016-09-27 STCR Description width --->
										<cfset firstCol = false>
										<div class="resultContent result-description #(listfindnocase(DataShowList,'Description') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Description') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#description#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'Language')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-language #(listfindnocase(DataShowList,'Language') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Language') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#Language#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'Source')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-source #(listfindnocase(DataShowList,'Source') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Source') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#Source#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'PublishDate')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-published #(listfindnocase(DataShowList,'PublishDate') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'PublishDate') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#lsdateformat(publishDate,dateformatmask)#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'CreatedDate')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-created #(listfindnocase(DataShowList,'CreatedDate') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'CreatedDate') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
											#lsdateformat(Created,dateformatmask)#
										</div>
									</cfif>
									<cfif listfindnocase(DataShow,'Lastupdated')>
										<cfif firstCol and not listfindnocase(DataShow,'Description')>
											<cfset thisDataShowListCols = DataShowColsListFloor+DataShowColsListRemainder>
											<cfset firstCol = false>
										<cfelse>
											<cfset thisDataShowListCols = DataShowColsListFloor>
										</cfif>
										<div class="resultContent result-updated #(listfindnocase(DataShowList,'Lastupdated') gt 0 ? '' : 'hiddenList')# #(listfindnocase(DataShowGrid,'Lastupdated') gt 0 ? '' : 'hiddenGrid')#" listCols="#thisDataShowListCols#">
												#lsdateformat(lastUpdated,dateformatmask)#
										</div>
									</cfif>
								</div>
							</a>
						</div>
					</cfloop>
					<cfif arguments.PagingType eq "Infinity">
						<cfif structKeyExists(arguments,"hasMore") and arguments.hasMore>
							<cfset arguments.pageNumber = arguments.pageNumber + 1>

							<div class="next">
								<a id="showMoreMLA" class="btn btn-default" href='#getShowMoreMediaFilesUrl(argumentCollection=arguments)#'>phr_MLALoadMore</a>phr_MLAConsiderFilters
							</div>
						</cfif>

						<script>
							setCurrentDisplay();
						</script>
					<cfelse>
						<cfset var currentPage = arguments.pageNumber>
						<cfset arguments.showForURL = false>
						<cfset var totalPages = ceiling(arguments.totalrecordcount/pageSize)>
						<cfif totalPages gt 1>
							<div class="search-results-pagination" aria-label="Page navigation">
								<ul class="pagination clearfix">
									<CFOUTPUT>
										<cfif currentPage gt 1>
											<cfif totalPages gt 10>
												<cfset arguments.pageNumber = 1>
												<li><a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments)#')">phr_mla_First_Page</a></li>
											</cfif>
											<cfset arguments.pageNumber = currentPage-1>
											<li><a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments)#')">phr_mla_Previous_Page</a></li>
										</cfif>

										<cfset var StartLoop = currentPage-4>
										<cfif StartLoop lt 1>
											<cfset StartLoop = 1>
										</cfif>
										<cfset var EndLoop = currentPage+5>
										<cfif endloop lt 10>
											<cfset endloop = 10>
										</cfif>
										<Cfif EndLoop gt totalPages>
											<cfset EndLoop = totalPages>
										</cfif>

										<cfset var nPage = StartLoop>

										<cfloop from=#StartLoop# to=#EndLoop# step=1 index="i">
											<cfif currentPage neq nPage>
												<cfset arguments.pageNumber = nPage>
												<li><a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments)#')">#htmleditformat(nPage)#</a></li>
											<cfelse>
												<li><a class="active">#htmleditformat(nPage)#</a><li>
											</cfif>
											<cfset nPage = nPage+1>
										</cfloop>
										<cfif currentPage lt nPage-1>
											<cfset arguments.pageNumber = currentPage+1>
											<li><a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments)#')">phr_mla_Next_Page</a></li>
											<cfif totalPages gt 10>
												<cfset arguments.pageNumber = totalPages>
												<li><a href="javascript:loadMLAPage('#getShowMoreMediaFilesUrl(argumentCollection=arguments)#')">phr_mla_Last_Page</a></li>
											</cfif>
										</cfif>
									</CFOUTPUT>
								</ul>
							</div>
						</cfif>
					</cfif>
				</cfoutput>
			</cfsavecontent>
		<cfelseif arguments.filesQuery.recordCount eq 0>
			<cfsavecontent variable="mediaLibRows">
				<cfoutput>
					<h2 class="mla-results-title">phr_mla_resource_library_results #arguments.totalrecordcount# phr_mla_resource_library_results_found</h2>
				</cfoutput>
			</cfsavecontent>
		</cfif>

		<cfreturn mediaLibRows>
	</cffunction>

	<cffunction name="getShowMoreMediaFilesUrl" access="public" returntype="string" output="false"> <!--- this output=false is VERY important!! --->

		<cfset var urlString = "#request.currentSite.protocolAndDomain#/webservices/callWebService.cfc?method=callWebService&webServiceName=mediaLibraryWS&methodName=getMediaLibrary&returnFormat=plain&_cf_nodebug=true">
		<cfset var argument = "">
		<cfloop collection="#arguments#" item="argument">
			<cfif listFindNoCase('FILESQUERY,_rwRandom,_rwsessiontoken,showForURL',argument) eq 0>
				<cfset urlString = urlString & "&#ltrim(rtrim(argument))#=#URLEncodedFormat(replace(arguments[argument], "'", "\'","all"))#">
			</cfif>
		</cfloop>

		<cfreturn urlString>
	</cffunction>

	<cffunction name="getFileExtensionsQuery" access="public" returntype="query" output="false">
		<cfscript>
			var extensionsList = application.com.settings.getSetting("files.allowedFileExtensions");
			var extensionData = arrayNew(1);
			for (ext in extensionsList) {
				arrayAppend(extensionData, {"extension" = ext});
			}
			var extensionsQuery = queryNew("extension", "CF_SQL_VARCHAR", extensionData);
			return extensionsQuery;
		</cfscript>
	</cffunction>

	<cffunction name="getFilterViewById" access="public" returntype="string" output="false">
		<cfargument name="filtersDisplayAs" type="string" required="true">
		<cfargument name="filterId" type="string" required="true">
		<cfargument name="selectedFlags" type="string" required="true">
		<cfargument name="selectedLanguages" type="string" required="true">
		<cfargument name="selectedExtensions" type="string" required="true">

		<cfscript>
			var viewArgs = {filtersDisplayAs = arguments.filtersDisplayAs, 
							filterId = arguments.filterId,
							filterNamePrefix = ""};

			if (filterId eq "language") {
				viewArgs.filterValuesQuery = application.com.commonQueries.getLanguages();
				viewArgs.filterValueIdColumn = "languageID";
				viewArgs.filterValueDisplayColumn = "Language";
				viewArgs.filterTitle = "phr_mla_Language";
				viewArgs.selectedFilterIds = arguments.selectedLanguages;
			} else if (filterId eq "fileExtension") {
				viewArgs.filterValuesQuery = getFileExtensionsQuery();
				viewArgs.filterValueIdColumn = "extension";
				viewArgs.filterValueDisplayColumn = "extension";
				viewArgs.filterTitle = "phr_mla_Extension";
				viewArgs.selectedFilterIds = arguments.selectedExtensions;
			} else {
				if (application.com.flag.getFlagGroupStructure(flagGroupID=filterId).Active eq 1) {
					viewArgs.filterValuesQuery = application.com.flag.getFlagGroupFlags(flaggroupID=filterId,active=1);
				} else {
					viewArgs.filterValuesQuery = queryNew("");
				}
				viewArgs.filterValueIdColumn = "flagtextID";
				viewArgs.filterValueDisplayColumn = "translatedname";
				viewArgs.filterTitle = application.com.flag.getFlagGroupStructure(flagGroupID=filterId).translatedname;
				viewArgs.filterNamePrefix = "filterflaggroup_";
				viewArgs.selectedFilterIds = arguments.selectedFlags;
			}

			return getFilterView(argumentCollection = viewArgs);
		</cfscript>
	</cffunction>

	<cffunction name="getFilterView" access="private" returntype="string" output="false">
		<cfargument name="filtersDisplayAs" type="string" required="true">
		<cfargument name="filterId" type="string" required="true">
		<cfargument name="selectedFilterIds" type="string" required="true">
		<cfargument name="filterValuesQuery" type="query" required="true">
		<cfargument name="filterValueIdColumn" type="string" required="true">
		<cfargument name="filterValueDisplayColumn" type="string" required="true">
		<cfargument name="filterTitle" type="string" required="true">
		<cfargument name="filterNamePrefix" type="string" required="false" default="">

		<cfsavecontent variable="local.result">
			<cfoutput>
				<cfif filterValuesQuery.recordcount neq 0>
				  <div class="borderBottom clearfix filterflagGroup#filterId# form-group">
					<div class="col-md-2 col-sm-2 col-xs-3 filter#filterId#Title filterMediaGroupTitle">
					  <label>#filterTitle#:</label>
					  <!--- 2016-08-05 PPB P-MIC001 Case 451195 --->
					</div>
					<cfif filtersDisplayAs eq "Dropdown">				<!--- 2016-08-05 PPB P-MIC001 Case 451195 --->
					  <div class="col-md-10 col-sm-10 col-xs-9 filter#filterId#Field">
						<div class="filterOption">
						  <select name="#filterNamePrefix##filterId#" class="form-control">
							<option value="">
							  phr_mla_please_select
							</option>
							<cfloop index="local.i" from="1" to="#filterValuesQuery.recordCount#">
							  <option id="#filterValuesQuery['#filterValueIdColumn#'][local.i]#" value="#filterValuesQuery['#filterValueIdColumn#'][local.i]#" <cfif listfind(selectedFilterIds,filterValuesQuery['#filterValueIdColumn#'][local.i],",")>selected</cfif>>
								#filterValuesQuery['#filterValueDisplayColumn#'][local.i]#
							  </option>
							</cfloop>
						  </select>
						</div>
					  </div>
					<cfelseif filtersDisplayAs eq "Radio">				<!--- 2016-08-05 PPB P-MIC001 Case 451195 --->
					  <div class="col-md-10 col-sm-10 col-xs-9 filterMediaGroupField">
						<cfloop index="local.i" from="1" to="#filterValuesQuery.recordCount#">
						  <div class="filterOption" id="#filterValuesQuery['#filterValueIdColumn#'][local.i]#">
							<div class="filterfield">
							  <div class="radio">
								<label  for="#filterValuesQuery['#filterValueIdColumn#'][local.i]#">
								  <input type="radio" id="#filterValuesQuery['#filterValueIdColumn#'][local.i]#" name="#filterNamePrefix##filterId#" value="#filterValuesQuery['#filterValueIdColumn#'][local.i]#" <cfif listfind(selectedFilterIds,filterValuesQuery['#filterValueIdColumn#'][local.i],",")>checked</cfif> /> #filterValuesQuery['#filterValueDisplayColumn#'][local.i]#
								</label>
							  </div>
							</div>
						  </div>
						</cfloop>
					  </div>
					<cfelse>
					  <div class="col-md-10 col-sm-10 col-xs-9 filterMediaGroupField">
						<cfloop index="local.i" from="1" to="#filterValuesQuery.recordCount#">
						  <div class="filterOption" id="#filterValuesQuery['#filterValueIdColumn#'][local.i]#">
							<div class="filterfield">
							  <div class="checkbox">
								<label  for="#filterValuesQuery['#filterValueIdColumn#'][local.i]#">
								  <input type="checkbox" id="#filterValuesQuery['#filterValueIdColumn#'][local.i]#" name="#filterNamePrefix##filterId#" value="#filterValuesQuery['#filterValueIdColumn#'][local.i]#" <cfif listfind(selectedFilterIds,filterValuesQuery['#filterValueIdColumn#'][local.i],",")>checked</cfif> /> #filterValuesQuery['#filterValueDisplayColumn#'][local.i]#
								</label>
							  </div>
							</div>
						  </div>
						</cfloop>
					  </div>
					</cfif>
				  </div>
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn local.result>
	</cffunction>

</cfcomponent>

