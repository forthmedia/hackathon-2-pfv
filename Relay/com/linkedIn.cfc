<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

<!--- 
	<cfset consumerKey = "8nr3k07nydxd"> <!--- the consumer key you got from LinkedIn when registering you app  --->
	<cfset consumerSecret = "Z4TeS1Gpq6OdTgym"> <!--- the consumer secret you got from LinkedIn --->
 --->
	<cffunction name="getProfile" access="public" returntype="struct" output="true">
		<cfargument name="fieldList" type="string" default="id,first-name,last-name,headline,phone-numbers,date-of-birth,picture-url,twitter-accounts,im-accounts,educations,public-profile-url">
		<cfargument name="entityID" type="numeric" required="false">
		<cfargument name="entityTypeID" type="numeric" default=0>
		
		<cfset var profileURL = "https://api.linkedin.com/v1/people/%7E:(#arguments.fieldList#)">
		<cfset var profileResponse = send(url=profileURL,argumentCollection=arguments)>
		<cfset var profileStruct = {id="",firstname="",lastname="",jobDesc="",pictureURL="/images/social/icon_no_photo_80x80.png",dob="",officePhone="",homePhone="",mobilePhone="",twitterAccount="",imAccount="",publicProfileUrl=""}>
		<cfset var profileResponseStruct = application.com.xmlFunctions.ConvertXmlToStruct(xmlNode=profileResponse.fileContent,str=structNew())>
		<cfset var result = {isOK=true,message="",profile=profileStruct}>

		<cfif profileResponse.isOK>
			<cfif structKeyExists(profileResponseStruct,"id")>
				<cfset profileStruct.id = profileResponseStruct["id"]>
			</cfif>
			<cfif structKeyExists(profileResponseStruct,"first-name")>
				<cfset profileStruct.firstname = profileResponseStruct["first-name"]>
			</cfif>
			<cfif structKeyExists(profileResponseStruct,"last-name")>
				<cfset profileStruct.lastname = profileResponseStruct["last-name"]>
			</cfif>
	
			<cfif structKeyExists(profileResponseStruct,"headline")>
				<cfset profileStruct.jobDesc = profileResponseStruct["headline"]>
			</cfif>
			
			<cfif structKeyExists(profileResponseStruct,"picture-url") and profileResponseStruct["picture-url"] neq "">
				<cfset profileStruct.pictureUrl = profileResponseStruct["picture-url"]>
			</cfif>
			
			<cfif structKeyExists(profileResponseStruct,"date-of-birth")>
				<cfif structCount(profileResponseStruct["date-of-birth"]) eq 3>
					<cfset profileStruct.dob = createDate(profileResponseStruct["date-of-birth"]["year"],profileResponseStruct["date-of-birth"]["month"],profileResponseStruct["date-of-birth"]["day"])>
				</cfif>
			</cfif>

			<cfif structKeyExists(profileResponseStruct,"phone-numbers") and isStruct(profileResponseStruct["phone-numbers"]) and structKeyExists(profileResponseStruct["phone-numbers"],"phone-number")>
				<cfif profileResponseStruct["phone-numbers"]["phone-number"]["phone-type"] eq "work">
					<cfset profileStruct.officePhone = profileResponseStruct["phone-numbers"]["phone-number"]["phone-number"]>
				<cfelseif profileResponseStruct["phone-numbers"]["phone-number"]["phone-type"] eq "home">
					<cfset profileStruct.homePhone = profileResponseStruct["phone-numbers"]["phone-number"]["phone-number"]>
				<cfelseif profileResponseStruct["phone-numbers"]["phone-number"]["phone-type"] eq "mobile">
					<cfset profileStruct.mobilePhone = profileResponseStruct["phone-numbers"]["phone-number"]["phone-number"]>
				</cfif>
			</cfif>

			<cfif structKeyExists(profileResponseStruct,"twitter-accounts") and isStruct(profileResponseStruct["twitter-accounts"]) and structKeyExists(profileResponseStruct["twitter-accounts"],"twitter-account")>
				<cfset profileStruct.twitterAccount = profileResponseStruct["twitter-accounts"]["twitter-account"]["provider-account-name"]>
			</cfif>

			<cfif structKeyExists(profileResponseStruct,"im-accounts") and isStruct(profileResponseStruct["im-accounts"]) and structKeyExists(profileResponseStruct["im-accounts"],"im-account")>
				<cfset profileStruct.imAccount = profileResponseStruct["im-accounts"]["im-account"]["im-account-name"]>
			</cfif>

			<cfif structKeyExists(profileResponseStruct,"public-profile-url")>
				<cfset profileStruct.publicProfileUrl = profileResponseStruct["public-profile-url"]>
			</cfif>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = profileResponse.message>
			<cfset result.authoriseLink = profileResponse.authoriseLink>
		</cfif>
		
		<cfset result.profile = profileStruct>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="send" access="private" returntype="struct" output="true">
		<cfargument name="url" type="string" required="true">
		<cfargument name="httpMethod" type="string" default="Get">
		<cfargument name="parameters" type="struct" default="#structNew()#">
		<cfargument name="xmlContent" type="xml" required="false">
		<cfargument name="entityID" type="numeric" required="false">  <!--- is making a call on behalf of someone.. need to get their ID to get the access token --->
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeId.person#">
		
		<!--- set up the required objects including signature method--->
		<!--- <cfset var oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")> --->
		<cfset var result = {isOk = true,authoriseLink=getAuthoriseLink(),fileContent='<?xml version="1.0" encoding="UTF-8"?><message>Problem</message>'}>
		<cfset var service = application.com.service.getService(serviceID="linkedIn")>
		<cfset var argStruct = {encryptToken=false}>
		
		<!--- TODO: need to determine what to do if access token is empty --->
		<cfset var apiAccessToken = "">
		<cfset var oConsumer = CreateObject("component", "oauth.oauthconsumer").init(sKey = service.consumerKey, sSecret = service.consumerSecret)>
		<cfset var tokenResponse = {statusCode=401,text="",fileContent=""}>
		<cfset var urlArgs = "">
		<!--- <cfset var oReq = ""> --->
	
		<cfif service.consumerKey neq "" and service.consumerSecret neq "">
			<!--- if we're getting an access token for a given person --->
			<cfif structKeyExists(arguments,"entityID") and arguments.entityTypeID eq 0>
				<cfset argStruct.personID = arguments.entityID>
			</cfif>
			<cfset apiAccessToken = CreateObject("component", "oauth.oauthToken").getAccessToken(argumentCollection=argStruct)>
	
			<!--- create the request
			<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
				oConsumer = oConsumer,
				oToken = apiAccessToken,
				sHttpMethod = arguments.httpMethod,
				sHttpURL = arguments.url,stparameters=arguments.parameters)>
			 --->
			<!--- sign the request using hash algorithm to get the signature
			<cfset oReq.signRequest(
				oSignatureMethod = oReqSigMethodSHA,
				oConsumer = oConsumer,
				oToken = apiAccessToken)> --->	
				
			<cfset urlArgs = application.com.structureFunctions.convertStructureToNameValuePairs(struct=arguments.parameters,delimiter="&")>
			<cfif urlArgs neq "">
				<cfset urlArgs = "&"&urlArgs>
			</cfif>
				
			<cfif apiAccessToken.getKey() neq "">
				<cfif arguments.httpMethod eq "get">
					<!--- <cfhttp url="#oREQ.getString()#" method="get" result="tokenResponse"/> --->
					<cfhttp url="#arguments.url#?oauth2_access_token=#apiAccessToken.getKey()##urlArgs#" method="get" result="tokenResponse"/>
					
				<cfelse>
					
					<cfif not structKeyExists(arguments,"xmlContent")>
						<cfthrow message="You must pass in xmlContent when doing a post.....">
					</cfif>
					<cfset arguments.xmlContent = trim(arguments.xmlContent)>
					<cfset arguments.url = arguments.url & "?oauth2_access_token="&apiAccessToken.getKey()>
					
					<cftry>
						<cfscript>
							//get url obj
							objUrl = createobject("java","java.net.URL").init(arguments.url);
							// connection
							conn = objUrl.openConnection();
							//set some props
							conn.setRequestMethod("POST");
							conn.setDoOutput(true);
							conn.setUseCaches(false);
							conn.setRequestProperty("Content-type", "text/xml; charset=UTF-8");
							//conn.setRequestProperty("Authorization", oREQ.toHeader());
							//set ouptput
							dtOut = conn.getOutputStream();
							dtOut.write(Javacast("String",arguments.xmlContent).toString().getBytes());
							dtOut.flush();
							dtOut.Close();
							// set input
							inS = createobject("java","java.io.InputStreamReader").init(conn.getInputStream());
							inVar = createObject("java","java.io.BufferedReader").init(inS);
							tokenResponse.statusCode = conn.getResponseCode();
							tokenResponse.text = conn.getResponseMessage();
							tokenResponse.fileContent = "";
						</cfscript>
						<cfcatch>
							<cfset tokenResponse.statusCode = conn.getResponseCode()>
							<cfset tokenResponse.text = conn.getResponseMessage()>
						</cfcatch>
					</cftry>
				</cfif>		
		
				<cfset result.isOK = true>
				<cfif tokenResponse.statusCode neq "200 OK" and tokenResponse.statusCode neq "201">
					<cfset result.isOK = false>
				</cfif>
				<cfset structAppend(result,tokenResponse)>
		
				<!--- if access token has expired or user is not logged in, then we will need to re-authorise. The callback should take them back to the current page
					so that in theory everything is now fine  --->
				<cfif structKeyExists(tokenResponse,"responseHeader") and structKeyExists(tokenResponse.responseHeader,"Explanation") and tokenResponse.responseHeader.Explanation eq "Unauthorized">
					<!--- <cfoutput>
						<script>
							if (confirm('Relayware is currently unauthorised to retrieve your linkedIn details. Would you like to reauthorise?')) {
								window.open('/social/ServiceAccess.cfm?a=authorise&callback=#request.currentSite.protocolAndDomain#/#cgi.SCRIPT_NAME#?#cgi.QUERY_STRING#','LinkedInLogin','width=400px,height=280px');
							}
						</script>
					</cfoutput> --->
					<cfset result.isOK = false>
					<cfset result.message = tokenResponse.responseHeader.Explanation>
				</cfif>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = "Problem obtaining the access token">
			</cfif>
		
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "The service has not been initialised.">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="share" access="public" returntype="struct">
		<cfargument name="comment" type="string" required="true">
		<cfargument name="visibility" type="string" default="connections-only">
		<cfargument name="url" type="string" default="">
		<cfargument name="urlTitle" type="string" default="">
		<cfargument name="urlImage" type="string" default="">
		<cfargument name="entityID" type="string" required="false"> <!--- if not present, then assumes the current user. --->
		<cfargument name="entityTypeID" type="numeric" default=0>
		
		<cfset var xmlContent = "">
		<cfset var shareURL = "https://api.linkedin.com/v1/people/~/shares">
		<cfset var shareResponse = "">
		<cfset var result = {isOK=true,message="Shared Successfully on LinkedIn."}>
		<cfset var argStruct = {url=shareURL,httpMethod="post"}>
		
		<!--- NJH 2012/10/25 - Added share image --->
		<cfoutput>
			<cfsavecontent variable="xmlContent"><?xml version="1.0" encoding="UTF-8"?>
				<share>
				<comment><![CDATA[#arguments.comment#]]></comment>
				<cfif arguments.url neq ""><content>
					<cfif arguments.urlTitle neq ""><title><![CDATA[#arguments.urlTitle#]]></title></cfif>
					<submitted-url><![CDATA[#arguments.url#]]></submitted-url>
					<cfif arguments.urlImage neq ""><submitted-image-url><![CDATA[#arguments.urlImage#]]></submitted-image-url></cfif>
				</content></cfif>
				<visibility>
				<code>#arguments.visibility#</code>
				</visibility>
				</share>
			</cfsavecontent>
		</cfoutput>
		
		<cfset argStruct.xmlContent = xmlContent>
		<cfif structKeyExists(arguments,"entityID")>
			<cfset argStruct.entityID = arguments.entityID>
			<cfset argStruct.entityTypeID = arguments.entityTypeID>
		</cfif>
		
		<cfset shareResponse = send(argumentCollection=argStruct)>
		<cfif not shareResponse.isOk>
			<cfset result.isOK = false>
			<cfset result.message = "Unable to share on LinkedIn.">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getAuthoriseRequestString" access="public" returnType="struct" output="false">
		<cfargument name="callbackURL" type="string" required="false">
		
		<cfset var service = application.com.service.getService(serviceID="linkedIn")>
		<cfset var state = left(hash("linkedInConnection"),22)> <!--- TODO: need to make this much more secure.. --->
		<cfset var result={method="post",url = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&clientId=#service.consumerKey#&state=#state#&redirect_uri=#request.currentSite.protocolAndDomain#/social/callback.cfm"}>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getAuthenticateRequestString" access="public" returnType="struct" output="false">
		<cfargument name="callbackURL" type="string" required="false">
		
		<cfset var service = application.com.service.getService(serviceID="linkedIn")>
		<cfset var state = left(hash("linkedInConnection"),22)> <!--- TODO: need to make this much more secure.. --->
		<cfset var result = {method="get",url="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=#service.consumerKey#&state=#state#&redirect_uri=#request.currentSite.protocolAndDomain#/social/callback.cfm?s=#application.com.encryption.encryptString(encryptType='standard',string='linkedIn')#"}>

		<cfreturn result>
	</cffunction>


	<cffunction name="getAuthoriseLink" access="public" returnType="string" output="false">
		<cfreturn "window.open('/social/ServiceAccess.cfm?a=authorise&s=linkedIn','LinkedInAuthorise','width=400px,height=280px')">
	</cffunction>

	<cffunction name="getLinkedInCompanyDetails" access="public" returntype="struct" output="false">
		<cfargument name="linkedInCompanyId" type="string">
		
		<cfset var searchURL = "">
		<cfset var searchResponse = "">
		<cfset var searchResponseStruct = "">
		<cfset var result = structNew()>

		<cfset searchURL = "https://api.linkedin.com/v1/companies/#arguments.linkedInCompanyId#">
		<cfset searchResponse = send(url=searchURL,parameters=arguments)>

		<cfset searchResponseStruct = application.com.xmlFunctions.ConvertXmlToStruct(xmlNode=searchResponse.fileContent,str=structNew())>

		<cfset result.isOK = searchResponse.isOK>
		<cfset result.searchResponse=searchResponseStruct>

		<cfreturn result>
	</cffunction>

	<cffunction name="searchForCompany" access="public" returntype="struct" output="false">
		<cfargument name="keywords" type="string">
		<cfargument name="count" type="string" required="false" default="20">	<!--- linkedIn seems to return a max of 20 despite the documentation suggesting a max of 110 --->
		<cfargument name="start" type="string" required="false" default="0">	<!--- 0 returns 0-19 assuming count=20, send in "20" for page 2, "40" for page 3  --->
		
		<cfset var searchURL = "">
		<cfset var searchResponse = "">
		<cfset var searchResponseStruct = "">
		<cfset var result = structNew()>

		<cfset searchURL = "https://api.linkedin.com/v1/company-search:(companies:(id,name,website-url,logo-url,ticker,locations))">
		<cfset searchResponse = send(url=searchURL,parameters=arguments)>

		<cfset searchResponseStruct = application.com.xmlFunctions.ConvertXmlToStruct(xmlNode=searchResponse.fileContent,str=structNew())>

		<cfset result.isOK = searchResponse.isOK>
		<cfset result.searchResponse=searchResponseStruct>

		<cfreturn result>
	</cffunction>

	<cffunction name="getConnections" access="public" returnType="struct" output="false">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="serviceEntityID" type="string" required="false">
		
		<cfset var getPersonServiceID = "">
		<cfset var connectionsURL = "">
		<cfset var connectionResponse = "">
		<cfset var result = {person=arrayNew(1),isOk=true}>
		<cfset var connection = "">
		
		<cfset connectionsURL = "https://api.linkedin.com/v1/people/id=#arguments.serviceEntityID#/connections:(id,first-name,last-name)">
		<cfset connectionResponse = send(url=connectionsURL,argumentCollection=arguments,entityId=arguments.personID)>
		<cfset result.isOK = connectionResponse.isOK>
	
		<cfif result.isOK>
			<!--- this will append the contents of fileContent to the result. We are converting the list of people to an array, as we are getting (potentially) more than 1 person--->
			<cfset application.com.xmlFunctions.ConvertXmlToStruct(xmlNode=connectionResponse.fileContent,str=result)>
		</cfif>

		<cfloop array="#result.person#" index="connection">
			<cfset connection.personID = application.com.service.getServiceEntity(serviceEntityID=connection.id,serviceID="linkedIn").entityID>
			<cfset connection.firstname = connection["first-name"]>
			<cfset connection.lastname = connection["last-name"]>
			<cfset connection.fullname = connection.firstName&" "&connection.lastname>
			<cfset structDelete(connection,"first-name")>
			<cfset structDelete(connection,"last-name")>
		</cfloop>
		<cfset result.person = application.com.structureFunctions.arrayOfStructuresToQuery(theArray=result.person)>

		<cfreturn result>
	</cffunction>
	
	<cffunction name="getAccessToken" access="public" returntype="any" output="false">
		<cfargument name="code" type="string" required="true">
		
		<cfset var service = application.com.service.getService(serviceID="linkedIn")>
		<cfset var tokenResponse = structNew()>
		<cfset var accessTokenResult = {}>
		<cfset var accessToken = "">
		
		<cfhttp url="https://www.linkedin.com/uas/oauth2/accessToken" method="post" result="tokenResponse">
			<cfhttpparam type="url" name="grant_type" value="authorization_code">
			<cfhttpparam type="url" name="client_id" value="#service.consumerKey#">
			<cfhttpparam type="url" name="redirect_uri" value="#request.currentSite.protocolAndDomain#/social/callback.cfm?s=#application.com.encryption.encryptString(encryptType='standard',string='linkedIn')#">
			<cfhttpparam type="url" name="client_secret" value="#service.consumerSecret#">
			<cfhttpparam type="url" name="code" value="#arguments.code#">
		</cfhttp>

		<cfif isJSON(tokenResponse.fileContent)>
			<cfset accessTokenResult = deserializeJSON(tokenResponse.fileContent)>
		</cfif>
		
		<cfif structKeyExists(accessTokenResult,"access_token")>
			<cfset accessToken = accessTokenResult.access_token>
		</cfif>
		
		<cfif accessToken neq "">
			<cfset accessToken = CreateObject("component", "oauth.oauthtoken").init(sKey=application.com.encryption.encryptString(encryptType="standard",string=accessToken),sSecret="")>
		</cfif>
		
		<cfreturn accessToken>
	</cffunction>
	
</cfcomponent>