﻿<!---
File name:		relayDisplay.cfc
Author:			NJH
Date started:	03-03-2010

Description:	Functions to set form field attributes for displaying

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010-06-23			NAS			P-CLS002
2010/07/24  WAB   Due to an unfortunate change in XMLFunctions.RunFunctionAgainstNodes, the Node argument to functions mergeXML, setNodeAttributesForDisplay and validateFieldAttributes had to be changed to XMLNode.
				  Since each function was coded using arguments.node I have done a little hack by using <cfargument name="node" default="#arguments.XMLNode#">
				  Still needs testing (I have experience some problems on opportunityEdit.cfm)
2011-01-10 			NYB 		LHID5257 added newPhrase call, changed currentValue value passed
2011/03/10			NAS			LID5787: Fund Manager - 'Hyperlink to PDF with instructions' is not showing on the partner portal
2011/03/23			PPB			LID5789: send  any additional formatting attributes thro to getFlagDataAndControls in displayFormField
2011/04/04			PPB			send entity ID directly to getFlagGroupDataAndControls
2012-01-26 			NYB 		Case:426292 ensured existance of label in displayFormField
2012/05/09 			PPB 		MIC001 pass through trstyle so we can hide a flag/group
2012-06-28			GCC/PPB 	MIC001 send additionalFormatting thro to cf_relayFormElementDisplay
2013-01-30			WAB			Altered getQueryAttributeForSelect to use relayForms.getValidValuesFromString.  Changes to relayXMLEditor highlighted that old code didn't support valid values in form func:comonent.functionName().
2013/02/14			YMA			Fix issue where checkboxes weren't saving in Opp edit.
2013-11-20 			NYB 		Case 437839 added support for maxlength and size passed in through the xml
2013-12-05			NYB			Case 437286
2013-12-11			NYB			Case 437286 changed if around passing countryid to field
2014-04-30 			NYB 		Case 439373
2014-06-20 			REH 		Case 439470 default the 'value' attribute of a checkbox to 1
2014-08-12			AXA			Added responsive block for form groups.
2015-03-16			NJH			Pass through the dontremoveregexp through to encryptHiddenFields if it exists
2015-04-15 			WAB 		CASE 444394 displayFormField() Label problems when displaying checkboxes.
2015-04-23 			NJH 		displayFormField() TextArea not displaying as readonly, due to readonly attribute being stripped out from additional formats
2015-05-13			NJH			Check that flag is ok before attempting to display it. Also made changes that would allow labels to get passed through and override the default ones
2015-07-01			WAB			CASE Fifteen-395 (related to Case 444625). Give Select boxes in XML editor a default null text
2014/09/22			NJH			Jira Prod2015-34 - add screens to opp display capabilities. Added function showScreenElementsInDisplayXML
2015-10-06			WAB			Jira PROD2015-41  displayXMLEditorFields(). Define ranges for all numeric fields.  Tidy up code in this area
2015-11-30  		SB			Bootstrap
2015-12-23			NJH			JIRA BF-17 use the code Will did for Jira PROD2015-41 in the validateFieldAttributes function, used by fundRequestActivities and opportunities.
2016/01/05			NJH			Jira BF-61 - if range has been specified for number, then set maxLength to be the length of the max range number.
2016-02-26			WAB 		During PROD2015-291  Media Library Visibility. Alter displayXMLEditorFields() to deal with parameters for new style UGRecordManager
2016-03-03			WAB			And a fix for above, has wrong default for suppressUserGroups
2016-11-07			WAB 		FormRenderer - Moved all code from cf_relayFormElementDisplay into relayFormElementDisplay()
2017-02-28 			WAB			RT-291 Added support for running queries within showScreenElementsInDisplayXML()
2016-08-25 			DCC 		case 451392 moved code line outside of if statement from line 1238 to 1232 div was missing ID attribute
Possible enhancements:

 --->

<cfcomponent hint="Functions supporting the relay display layer">

	<cffunction name="mergeXML" access="public" hint="Merges the display XML with the domain XML">
		<cfargument name="XMLnode" required="true" type="xml">
		<cfargument name="domainXML" required="true" type="xml" hint="The domain xml which holds the meta-data for a field">
		<!--- <cfargument name="node" default="#arguments.XMLNode#"> ---> <!--- See main comment 2010/07/24 --->

		<cfset var fieldArray = arrayNew(1)>

		<cfif arguments.XMLnode.xmlName is "field">
			<cfset fieldArray = xmlSearch(arguments.domainXML,"//#arguments.XMLnode.xmlName#[@name='#arguments.XMLnode.xmlAttributes.name#']")>
			<cfif arrayLen(fieldArray) gt 0>
				<cfset structAppend(arguments.XMLnode.xmlattributes, fieldArray[1].xmlattributes,false)>
			</cfif>
		</cfif>

	</cffunction>


	<cffunction name="setNodeAttributesForDisplay" access="public" hint="A function to set field attributes for display">
		<cfargument name="XMLnode" required="true" type="xml" hint="The node for which to set the attribute">
		<cfargument name="attributeStruct" type="struct" default="#structNew()#" hint="A structure whose key contains fieldnames and whose values are the value of the attributes">
		<cfargument name="node" default="#arguments.XMLNode#"> <!--- See main comment 2010/07/24 --->

		<cfset var fieldname="">
		<cfset var xmlDoc = "">
		<cfset var newPhrase = "">
		<cfset var fieldAttribute = '' />
		<cfset var com = application.com>
		<cfset var functionResult = "">
		<cfset var functionStruct = {}>

		<!--- PPB 2010/12/14 REL099 extended to copy the attributes for other nodes (not just "field") so we can set a group or include file to be hidden/visible using a standard technique --->
		<cfif structKeyExists(arguments.node.xmlAttributes,"name")>
			<cfset fieldname=arguments.node.xmlAttributes.name>

			<cfif structKeyExists(arguments.attributeStruct,fieldname)>
				<cfset structAppend(arguments.node.xmlAttributes,arguments.attributeStruct[fieldname],true)>

				<cfif arguments.node.xmlName is "field">

					<!--- if we specify a field as being HTML, then don't make it required --->
					<cfif structKeyExists(arguments.node.xmlAttributes,"relayFormElementType") and arguments.node.xmlAttributes.relayFormElementType eq "HTML">
						<cfset arguments.node.xmlAttributes.required = false>
					</cfif>

					<!--- if we're disabling a field that is required, then create a hidden field. This is for select boxes, etc as CF doesn't
						allow you to make a select read-only
					 --->
					<cfif structKeyExists(attributeStruct[fieldname],"disabled") and attributeStruct[fieldname].disabled and
							structKeyExists(arguments.node.xmlAttributes,"required") and arguments.node.xmlAttributes.required and
							structKeyExists(arguments.node.xmlAttributes,"relayFormElementType") and arguments.node.xmlAttributes.relayFormElementType neq "hidden">

						<cfset xmlDoc = application.com.xmlFunctions.getXMLDocObjectFromNode(node)>
						<cfset arrayAppend(arguments.node.xmlParent.xmlChildren, XmlElemNew(xmlDoc,"field"))>
						<cfset arguments.node.xmlParent.xmlChildren[arrayLen(arguments.node.xmlParent.xmlChildren)].xmlAttributes = arguments.node.xmlAttributes>
						<cfset arguments.node.xmlParent.xmlChildren[arrayLen(arguments.node.xmlParent.xmlChildren)].xmlAttributes["relayFormElementType"] = "hidden">
					</cfif>

					<!--- 2010-06-23			NAS			P-CLS002 - set the default value if the current entityID is 0 --->
					<cfif structKeyExists(arguments.node.xmlAttributes,"defaultValue") and structKeyExists(attributeStruct,"currentEntity") and structKeyExists(attributeStruct.currentEntity,"entityID") and attributeStruct.currentEntity.entityID eq 0>
						<cfset arguments.node.xmlAttributes.currentValue = arguments.node.xmlAttributes["defaultValue"]>
					</cfif>

					<!--- check for merge fields - it looks for a field like [[merge.entityid.currentValue]] - where entityID is the fieldname --->
					<cfloop collection="#arguments.node.xmlAttributes#" item="fieldAttribute">
						<!--- set the date format for a date --->
						<cfif fieldAttribute eq "dateFormat" and isDate(arguments.node.xmlAttributes.currentValue)>
							<!--- NYB 2011-01-10 LHID5257 start - added newPhrase call, changed currentValue value passed --->
							<cfset newPhrase=application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=arguments.node.xmlAttributes[fieldAttribute], mergeStruct = attributeStruct)>
							<cfset arguments.node.xmlAttributes.currentValue = application.com.dateFunctions.dateTimeFormat(arguments.node.xmlAttributes.currentValue,newPhrase)>
							<!--- NYB 2011-01-10 LHID5257 end --->
						</cfif>

						<cfif application.com.relaytranslations.checkForCfVariables(arguments.node.xmlAttributes[fieldAttribute])>
							<cfset newPhrase=application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=arguments.node.xmlAttributes[fieldAttribute], mergeStruct = attributeStruct)>
							<cfset arguments.node.xmlAttributes[fieldAttribute] = newPhrase>
						</cfif>

						<!--- NJH 2013/11/11 2013 2 Sprint 3 Look for a function on any attribute apart from query --->
						<cfif fieldAttribute neq "query" and listFirst(arguments.node.xmlAttributes[fieldAttribute],":") eq "func">
							<cfset functionStruct = application.com.relayForms.getFunctionPointerFromString(function=listLast(arguments.node.xmlAttributes[fieldAttribute],":"))>
							<cfinvoke component="#functionStruct.componentPointer#" argumentcollection="#functionStruct.args#" method="#functionStruct.method#" returnvariable="functionResult">
							<cfif isStruct(functionResult)>
								<cfset functionResult = functionResult[listLast(arguments.node.xmlAttributes[fieldAttribute],".")]>
							</cfif>
							<cfset arguments.node.xmlAttributes[fieldAttribute] = functionResult>
						</cfif>
					</cfloop>

					<!--- NJH add ability to set a currentValue as a function. used at the moment to set a default value --->
					<!--- <cfif listFirst(arguments.node.xmlAttributes["currentValue"],":") eq "func">
						<cfset arguments.node.xmlAttributes["currentValue"] = evaluate(listLast(arguments.node.xmlAttributes["currentValue"],":"))>
					</cfif> --->

					<!--- P-PAN004 NJH 2010/10/03 if a query or bind function has been set to empty, then delete it from the structure --->
					<cfif structKeyExists(arguments.node.xmlAttributes,"query") and arguments.node.xmlAttributes["query"] eq "">
						<cfset structDelete(arguments.node.xmlAttributes,"query")>
					</cfif>
					<cfif structKeyExists(arguments.node.xmlAttributes,"bindFunction") and arguments.node.xmlAttributes["bindFunction"] eq "">
						<cfset structDelete(arguments.node.xmlAttributes,"bindFunction")>
					</cfif>

				</cfif>

				<!--- NJH 2010/10/07 - here, we append the new node attributes to the attribtue struct, so that we can reference
					other form fields and their attributes.. this will only work for fields that have been set before a given field
				 --->
				<cfset structAppend(arguments.attributeStruct[fieldname],arguments.node.xmlAttributes,true)>
	 		</cfif>
	 	</cfif>

	</cffunction>


	<cffunction name="validateFieldAttributes" access="public" hint="Validates an XML field node's attributes based on the database table details">
		<cfargument name="XMLnode" required="true" type="xml">
		<cfargument name="dbTableStruct" required="true" default="#structNew()#" hint="A structure whose keys are the db column names">
		<cfargument name="tablename" type="string" required="true" hint="A tablename">
		<!--- <cfargument name="node" default="#arguments.XMLNode#"> ---> <!--- See main comment 2010/07/24 --->

		<cfset var fieldAttributes = structNew()>
		<cfset var fieldname = "">
		<cfset var maxLength = 0>
		<cfset var required = false>
		<cfset var relayFormElementType = "text">
		<cfset var dbFieldLength = 0 />

		<!--- PPB 2010/12/18 REL099 extended so that we copy "name" to "fieldname", type to "relayFormElementType" etc for non table fields also --->
		<cfif arguments.XMLnode.xmlName is "field">
			<cfset fieldAttributes = arguments.XMLnode.xmlAttributes>
			<cfset fieldName = arguments.XMLnode.xmlAttributes.name>
			<cfset fieldAttributes.fieldName = fieldName>
			<cfif not StructKeyExists(fieldAttributes,"label")>		<!--- PPB 2010/12/18 REL099 override only if it wasn't specified in XML --->
				<cfset fieldAttributes.label="phr_#fieldName#">
			</cfif>
			<cfset fieldAttributes.currentValue = "">
			<cfset fieldAttributes.trID = "tr_#lcase(fieldName)#">

			<cfif structKeyExists(arguments.dbTableStruct,arguments.XMLnode.xmlAttributes.name)>

				<!--- <cfif structKeyExists(request,"fieldNameList") and listFindNoCase(request.fieldNameList,fieldName)>
					<cfset application.com.xmlFunctions.deleteNode(node)>
				<cfelse> --->

				<!--- set length --->
				<cfif structKeyExists(fieldAttributes,"maxLength")>
					<cfset maxLength = fieldAttributes.maxLength>
				</cfif>
				<cfset dbFieldLength = arguments.dbTableStruct[fieldName].column_length>
				<cfif (not isDefined("maxLength")) or (maxLength gt dbFieldLength) or maxLength eq 0>
					<cfset maxLength=dbFieldLength>
				</cfif>
				<cfif maxLength gt 0>
					<cfset fieldAttributes.maxLength = maxLength>
				</cfif>

				<!--- set required --->
				<cfif structKeyExists(fieldAttributes,"required")>
					<cfset required=fieldAttributes.required>
				</cfif>

				<cfif not dbTableStruct[fieldName].IsNullable>
					<cfset required=true>
				</cfif>
				<cfset fieldAttributes.required = required>

				<!--- set the form type --->
				<cfif structKeyExists(fieldAttributes,"type")>
					<cfset relayFormElementType = fieldAttributes.type>
					<!--- if field is set as text and it's a number, set it to be a numeric field --->
					<cfif listFindNoCase("bit,int,float,smallint,numeric,decimal",arguments.dbTableStruct[fieldName].data_type) and relayFormElementType eq "text">
						<cfset relayFormElementType = "numeric">
					</cfif>
				<cfelse>
					<cfif listFindNoCase("bit,int,float,smallint,numeric,decimal",arguments.dbTableStruct[fieldName].data_type)>
						<cfset relayFormElementType = "numeric">
					<cfelseif listFindNoCase("date,datetime",arguments.dbTableStruct[fieldName].data_type)>
						<cfset relayFormElementType = "date">
					</cfif>
				</cfif>

				<cfif relayFormElementType eq "select">
					<cfif not structKeyExists(fieldAttributes,"value")>
						<cfset fieldAttributes.value = "value">
					</cfif>
					<cfif not structKeyExists(fieldAttributes,"display")>
						<cfset fieldAttributes.display = "display">
					</cfif>
				</cfif>
			<cfelse>
				<cfif StructKeyExists(fieldAttributes,"type")>
					<cfset relayFormElementType = fieldAttributes.type>
				</cfif>
			</cfif>
			<cfset fieldAttributes.relayFormElementType=relayFormElementType>

			<!--- if field is displayed as HTML, then don't add it to the list of form fields that has been displayed --->
			<cfif relayFormElementType neq "HTML">
				<cfset request.fieldNameList=listAppend(request.fieldNameList,fieldName)>
			</cfif>

			<!--- 2015-12-23 NJH JIRA BF-17 - brought in from Wills work in displayXMLEditorFields - could be made a function, but hope that screen builder will do all that for us. Leaving it for now. --->
			<cfif relayFormElementType eq "numeric">
				<cfset var fieldMetaData = application.com.relayEntity.getTableFieldStructure(tablename=arguments.tablename,fieldname=fieldName)>
				<cfif fieldMetaData.isOK>
					<cfif fieldMetaData.maxLength gt 0>
						<cfset fieldAttributes.maxLength = fieldMetaData.maxLength>
					</cfif>

					<cfif fieldMetaData.simpleDataType is "numeric">
						<cfset fieldAttributes.validate="numeric">
					</cfif>

					<!--- Set a range for numeric fields if not already defined.  Cold be cleverer and check that the supplied range is valid - but perhaps that would be overkill
						JIRA BF-17 - set the max and min fields if they are open ended...
					 --->
					<cfif fieldMetaData.max is not "">
						<cfif not structKeyExists(fieldAttributes,"range")>
							<cfset fieldAttributes.range = "#fieldMetaData.min#,#fieldMetaData.max#">
						<cfelseif listLen(fieldAttributes.range) eq 1>
							<cfif listFirst(fieldAttributes.range,",",true) eq "">
								<cfset fieldAttributes.range = listSetAt(fieldAttributes.range,1,fieldMetaData.min,",",true)>
							<cfelseif listLast(fieldAttributes.range,",",true) eq "">
								<cfset fieldAttributes.range = listSetAt(fieldAttributes.range,2,fieldMetaData.max,",",true)>
							</cfif>
						</cfif>
					</cfif>

					<!--- NJH 2016/01/05 BF-61 - if range is set, then use that to set the max length. This assumes that the range supplied has been valid and is not longer than the maxlength of the db field.  --->
					<cfif structKeyExists(fieldAttributes,"range") and listlen(fieldAttributes.range) eq 2>
						<cfset fieldAttributes.maxLength = max(len(listFirst(fieldAttributes.range)),len(listLast(fieldAttributes.range)))>
					</cfif>
				</cfif>
			</cfif>

		</cfif>
	</cffunction>


	<cffunction name="getQueryAttributeForSelect" access="public" hint="Sets the query attribute for a given form field" returntype="struct">
		<cfargument name="fieldAttributes" type="struct">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">
		<cfargument name="currentValue" type="string" required="false">

		<cfset var attributeStruct = duplicate(arguments.fieldAttributes)>
		<cfset var variableName = "">
		<cfset var com = application.com>
		<cfset var getValues = '' />
		<cfset var validValueMergeStruct = duplicate(arguments.mergeStruct)>

		<!--- JIRA PROD2015-566 NJH 2016/02/10 - pass through current value to function. --->
		<cfif structKeyExists(arguments,"currentValue")>
			<cfset validValueMergeStruct.currentValue =arguments.currentValue>
		</cfif>

		<cfif structKeyExists(attributeStruct,"query")>
			<cfset attributeStruct.query = application.com.relayForms.getValidValuesFromString(string=attributeStruct.query,mergeStruct=validValueMergeStruct)>
		</cfif>

		<cfreturn attributeStruct>
	</cffunction>


	<cffunction name="setFieldAttributesForDisplay" access="public" hint="">
		<cfargument name="node" required="true" type="xml">
		<cfargument name="displayStruct" type="struct" default="#structNew()#">

		<cfset var additionalArgs = {attributeStruct=arguments.displayStruct}>

		<cfset application.com.xmlfunctions.runFunctionAgainstNodes(XMLNodeArray=arguments.node,function=setNodeAttributesForDisplay,level=0,additionalArgs=additionalArgs)>

	</cffunction>


	<cffunction name="loadDisplayXML" access="public" hint="Loads the display xml into memory">
		<cfargument name="tablename" type="string" required="true" hint="A tablename">

		<!--- read the XML file that holds the opportunity layout --->
		<cfset var domainXml = "">
		<cfset var displayXml = "">
		<cfset var domainXmlFile = "">
		<cfset var displayXmlFile = "">
		<cfset var additionalArgs = structNew()>
		<cfset var getRequiredFields = "">
		<cfset var hiddenFieldAttributes = structNew()>
		<cfset var fieldNameList = '' />

		<!--- get opportunity table details from the DB. This is used to validate fields in the xml against what is in the db --->
		<cfset var entityTableDetails = application.com.dbTools.getTableDetails(tablename=arguments.tablename,returnColumns="true")>
		<cfset var entityTableStruct = application.com.structureFunctions.queryToStruct(query=entityTableDetails,key="column_name")>
		<cfset var domainXmlFilePath = "#application.paths.relayware#\xml\#arguments.tablename#Domain.xml">
		<cfset var displayXmlFilePath = "#application.paths.relayware#\xml\#arguments.tablename#Display.xml">

		<cfif fileExists("#application.paths.aboveCode#\xml\#arguments.tablename#Display.xml")>
			<cfset displayXmlFilePath = "#application.paths.aboveCode#\xml\#arguments.tablename#Display.xml">
		</cfif>

		<cffile action="read" file="#domainXmlFilePath#" variable="domainXmlFile">
		<cfset domainXML = xmlParse(domainXmlFile)>
		<cffile action="read" file="#displayXmlFilePath#" variable="displayXmlFile">
		<cfset displayXML = xmlParse(displayXmlFile)>

		<cfset application["#arguments.tablename#XML"] = displayXML>
		<cfset request.fieldNameList=""> <!--- holds the fields that have been displayed --->

		<!--- merge the attributes from the domain file into the display file --->
		<cfset additionalArgs = {domainXML = domainXML}>
		<cfset application.com.xmlfunctions.runFunctionAgainstNodes(xmlNodeArray=application["#arguments.tablename#XML"][arguments.tablename],function=application.com.relayDisplay.mergeXML,level=0,additionalArgs=additionalArgs)>

		<!--- set any missing required attributes. Verify attributes against the db --->
		<cfset additionalArgs = {dbTableStruct = entityTableStruct,tablename=arguments.tablename}>
		<cfset application.com.xmlfunctions.runFunctionAgainstNodes(xmlNodeArray=application["#arguments.tablename#XML"][arguments.tablename],function=application.com.relayDisplay.validateFieldAttributes,level=0,additionalArgs=additionalArgs)>

		<!--- output any required fields that have not been specified as hidden fields --->
		<cfquery name="getRequiredFields" dbType="query">
			select * from entityTableDetails where isNullable <> 1 and hasDefault <> 1
			and column_name not in ('createdby','lastupdatedby','lastUpdatedByPerson')
		</cfquery>

		<cfloop query="getRequiredFields">
			<cfif not listFindNoCase(request.fieldNameList,column_name)>
				<cfset fieldNameList=listAppend(request.fieldNameList,column_name)>

				<cfset arrayPrepend(application["#arguments.tablename#XML"][arguments.tablename].XmlChildren, XmlElemNew(application["#arguments.tablename#XML"],"field"))>
				<cfset hiddenFieldAttributes = structNew()>
				<cfset hiddenFieldAttributes.label="">
				<cfset hiddenFieldAttributes.currentValue="">
				<cfif listFindNoCase("bit,int,float,smallint,numeric",data_type)>
					<cfset hiddenFieldAttributes.currentValue=0>
				</cfif>
				<cfset hiddenFieldAttributes.fieldname=column_name>
				<cfset hiddenFieldAttributes.name=column_name>
				<cfset hiddenFieldAttributes.relayFormElementType = "hidden">
				<cfset application["#arguments.tablename#XML"][arguments.tablename].XmlChildren[1].xmlAttributes=hiddenFieldAttributes>
			</cfif>
		</cfloop>

	</cffunction>

	<cffunction name="includeFileForDisplay" access="public" hint="includes a file as specified in the display xml">
		<cfargument name="node" type="xml" required="true">

		<cfset var varName = '' />

		<cfif structKeyExists(arguments.node.xmlAttributes,"fileName")>
			<!--- 2011/03/10			NAS			LID5787: Fund Manager - 'Hyperlink to PDF with instructions' is not showing on the partner portal --->
			<cfif fileExists(application.paths.code & "\cftemplates\" & arguments.node.xmlAttributes.filename)>
				<cfloop collection="#node.xmlAttributes#" item="varName">
					<cfset variables["#varName#"] = arguments.node.xmlAttributes[varName]>
				</cfloop>
				<cfinclude template="/code/cftemplates/#arguments.node.xmlAttributes.filename#">
			</cfif>
		</cfif>
	</cffunction>


	<!--- NJH PROD2015-34 2015/09/22 - include a screen in the opp display 
		2017-02-28 WAB	RT-291 Added support for running queries within showScreenElementsInDisplayXML()
	--->
	<cffunction name="showScreenElementsInDisplayXML" access="public" output="true" returnType="void" hint="Display screen elements on a form that is drawn with a display XML">
		<cfargument name="screenID" type="string" required="true">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityDetails" type="struct" default="#structNew()#"><!--- if not passed in, could have function call to go get them based on screen entityType --->

		<cfif application.com.screens.doesScreenExist(currentScreenId=arguments.screenID)>
			<cfset var screenAndDef = application.com.screens.getScreenAndDefinition(screenid=arguments.screenID,method="edit",countryID=arguments.countryID)>
			<cfset var screenDefQry = screenAndDef.definition>

			<!--- create an 'object' here, so that the current record (or object) can be used in the evaluation below... ie. opportunity.countryID eq 13 --->
			<cfset variables[screenAndDef.screen.entityType] = arguments.entityDetails>

			<cfloop query="#screenDefQry#">
				<cfset var screenArgs = {}>

				<cfset var showScreenElement = true>

				<cfif screenDefQry.thisLineCondition[currentRow] neq "">
					<cftry>
						<cfset showScreenElement = evaluate(screenDefQry.thisLineCondition[currentRow])>
						<cfcatch>
							<cfset application.com.errorHandler.recordRelayError_Warning(type="Screen",Severity="error",catch=cfcatch,warningStructure=arguments)>
						</cfcatch>
					</cftry>
				</cfif>

				<cfif showScreenElement>
					<!--- only have support for flags and flagGroups at the moment --->
					<cfif listFindNoCase("flag,flagGroup",screenDefQry.fieldSource[currentRow])>
	
						<!--- NJH 2017/01/03 PROD2016-3073 - dont show inactive flags/groups
							NJH 2017/01/23 JIRA RT-177 - check for proper column'active' when looking at flags and groups. Not named the same! --->
						<cfset var functionArgs[fieldSource&"ID"] = screenDefQry.fieldTextID[currentRow]>
						<cfset var functionResult = "">
						<cfinvoke component="#application.com.flag#" argumentcollection="#functionArgs#" method="get#screenDefQry.fieldSource[currentRow]#Structure" returnvariable="functionResult">
						<cfset var showField = functionResult.isOK>
						<cfif showField>
							<cfif screenDefQry.fieldSource[currentRow] eq "flagGroup">
								<cfset showField = functionResult.active>
							<cfelse>
								<cfset showField = functionResult.flagActive>
							</cfif>
						</cfif>
	
						<cfif showField>
							<cfset screenArgs[fieldSource&"ID"] = screenDefQry.fieldTextID[currentRow]>
							<cfset screenArgs.required = screenDefQry.required[currentRow]>
							<cfif screenDefQry.fieldLabel[currentRow] neq "">
								<cfset screenArgs.label = screenDefQry.fieldLabel[currentRow]>
							</cfif>
							<cfset screenArgs.readonly = screenDefQry.method[currentRow] eq "view"?true:false>
	
							<cfset structAppend(screenArgs,application.com.structureFunctions.convertNameValuePairStringToStructure(inputString=screenDefQry.specialFormatting[currentRow],delimiter=","))>
							<cfset displayFormField(fieldAttributes=screenArgs,entityID=arguments.entityID,countryID=arguments.countryID)>
						</cfif>
	
					<cfelseif screenDefQry.fieldSource[currentRow] is "query">
						<CFSET local.queryString = evaluate(screenDefQry.evalstring[currentRow])>
						<!--- check for disallowed bits of sql 
							regExp should allow words which contain these words
						--->
						<cfset var re = "(\A|[^A-Za-z0-9])(drop|create|truncate|insert|update|delete)(\Z|[^A-Za-z0-9])">
						<cfif reFindNoCase(re,local.queryString) is not 0>
							<cfoutput>disallowed query #htmleditformat(queryString)#</cfoutput><CF_ABORT>
						<cfelse>
							<CFQUERY name="local.#fieldTextID#">
								#preserveSingleQuotes(queryString)#
							</cfquery>
						</cfif>	
	
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
	</cffunction>


	<cffunction name="displayFormField" access="public" hint="displays a form field">
		<cfargument name="fieldAttributes" type="struct" required="true">
		<cfargument name="entityID" type="numeric" required="false" default=0>
		<cfargument name="newLine" type="boolean" default="true">
		<cfargument name="createOrig" type="boolean" default="false">
		<cfargument name="countryID" type="numeric" default=0> <!--- 2013-06-06  MS   Case. 435616. Use countryID in a valid values Query --->

		<cfset var result_html = "">
		<cfset var method = "">
		<cfset var additionalFormatting = '' />
		<cfset var flagControlArray = arrayNew(1)> <!--- NJH 2012/12/04 - changed to array as part of 2013 Roadmap --->
		<cfset var trStyle="">

		<!--- START: 2012-01-26 NYB Case:426292 added: --->
		<cfif not structkeyexists(arguments.fieldAttributes,"label")>
			<cfif structkeyexists(arguments.fieldAttributes,"name")>
				<cfset arguments.fieldAttributes.label = "phr_"&arguments.fieldAttributes.name>
			<cfelse>
				<cfset arguments.fieldAttributes.label = "">
			</cfif>
		</cfif>
		<!--- END: 2012-01-26 NYB Case:426292 --->

		<!--- START: 2013-12-05 NYB Case 437286 added, 2013-12-11 changed: --->
		 <cfif arguments.countryid gt 0 and (not structKeyExists(arguments.fieldAttributes,"countryid"))>
			 <cfset arguments.fieldAttributes.countryid = arguments.countryid>
		</cfif>
		<!--- END: 2013-12-05 and 2013-12-11 NYB Case 437286 --->

        <cfif structKeyExists(arguments.fieldAttributes,"flagID") or structKeyExists(arguments.fieldAttributes,"flagGroupID")>
 			<cfif structKeyExists(arguments.fieldAttributes,"readOnly") and arguments.fieldAttributes.readOnly eq "true">
				<cfset method="view">
			<cfelse>
				<cfset method="edit">
			</cfif>

			<!--- 2011/03/23 PPB to get any additional formatting attributes take a clone of all attributes sent thro and then remove the ones we know aren't formatting --->
			<cfset additionalFormatting = duplicate(arguments.fieldAttributes)>
			<cfset structDelete(additionalFormatting,"name")>
			<cfset structDelete(additionalFormatting,"flagID")>
			<cfset structDelete(additionalFormatting,"flagGroupID")>
			<cfset structDelete(additionalFormatting,"entityid")>
			<cfset structDelete(additionalFormatting,"relayformelementtype")>
			<cfset structDelete(additionalFormatting,"fieldname")>
			<cfset structDelete(additionalFormatting,"trid")>
			<cfset structDelete(additionalFormatting,"currentvalue")>
			<!--- NJH 2015/04/23 - pass through readonly if it exists and is true. Needed to display flag as readonly--->
			<cfif structKeyExists(additionalFormatting,"readonly") and not additionalFormatting.readonly>
				<cfset structDelete(additionalFormatting,"readonly")>
			</cfif>

			<cfif structKeyExists(arguments.fieldAttributes,"flagID")>
         		<cfset arrayAppend(flagControlArray,application.com.flag.getFlagDataAndControls(flagID=arguments.fieldAttributes.flagID,entityID=arguments.entityID,method=method,additionalFormatting=additionalFormatting,countryID=arguments.countryID))> <!--- 2013-06-06  MS   Case. 435616. Use countryID in a valid values Query --->
				<!--- RMB - 2014-02-24 - 438819 - Required Messages not getting translated - not file name to pick up in lable--->
				<cfif flagControlArray[1].isOK>
					<cfset additionalFormatting.fieldname = flagControlArray[1].fieldname>
				</cfif>
			<cfelse>
            	<cfset flagControlArray = application.com.flag.getFlagGroupDataAndControls(flagGroupID=arguments.fieldAttributes.flagGroupID,entityID=arguments.entityID,method=method,additionalFormatting=additionalFormatting,countryID=arguments.countryID)> <!--- 2013-06-06  MS   Case. 435616. Use countryID in a valid values Query --->
			</cfif>

			<!--- 2012/05/09 PPB MIC001 pass through trstyle so we can hide a flag --->
			<cfif structKeyExists(arguments.fieldAttributes,"trstyle")>
				<!--- <cfset trStyle=arguments.fieldAttributes.trstyle> --->
				<cfset additionalFormatting.trStyle=arguments.fieldAttributes.trstyle>
			</cfif>
			<cfif structKeyExists(arguments.fieldAttributes,"trid")>
				<cfset additionalFormatting.trid=arguments.fieldAttributes.trid>
			</cfif>

			<cfoutput>
				<!--- NJH 2015/03/16 - pass through fieldname so that we can create the label for the element properly --->
				<cfloop array="#flagControlArray#" index="result_html">
					<!--- WAB 2015-04-15 CASE 444394 Label problems.  Labels in left hand column getting moved by bootstrap
							This occurred because result_html.label contains a <label> tag with a for= attribute  (designed for when checkboxes are displayed in a tabular layout with the label to the right of the checkbox)
							Also relayFormElementDisplay was adding another <label> tag around the outside
							Solution is to remove the label tag from result_html.label and also set the labelFor attribute to blank (this is a new attribute)
					--->
					<cfif result_html.isOK>
						<cfset var label = rereplaceNoCase(result_html.label,"<label.*?>(.*)</label>","\1")>
						<cfif structKeyExists(arguments.fieldAttributes,"label") and arguments.fieldAttributes.label neq "">
							<cfset label = arguments.fieldAttributes.label>
						</cfif>

						<!--- NJH 2015/06/05 - set label for for anything other than checkboxes and radios --->
						<cf_relayFormElementDisplay relayFormElementType="html" fieldname="#result_html.fieldname#" attributeCollection=#additionalFormatting# label="#label#" labelFor="#listFindNoCase('checkbox,radio',result_html.typename)?'':result_html.fieldname#">#result_html.control_html#</cf_relayFormElementDisplay>   <!--- 2012-06-28 GCC/PPB MIC001 send additionalFormatting thro --->
					</cfif>
				</cfloop>
			</cfoutput>

        <cfelseif structKeyExists(arguments.fieldAttributes,"encrypt") and arguments.fieldAttributes.encrypt>
			<!--- NJH 2015/03/16 - check to see if dontRemoveRegExp has been passed through. Wanted to keep opportunityId on form ---->
			<cfset var dontRemoveRegExp = structKeyExists(arguments.fieldAttributes,"dontRemoveRegExp")?arguments.fieldAttributes.dontRemoveRegExp:"">

			<cf_encryptHiddenFields dontRemoveRegExp="#dontRemoveRegExp#">
				<cf_relayFormElementDisplay attributeCollection=#arguments.fieldAttributes#>
			</cf_encryptHiddenFields>

		<cfelse>

			<!--- 2013/02/14	YMA	Fix issue where checkboxes weren't saving in Opp edit. --->
			<cfif arguments.fieldAttributes.relayFormElementType eq "checkbox">
				<cf_input type="hidden" id="#arguments.fieldAttributes.fieldname#_checkbox" name="#arguments.fieldAttributes.fieldname#_checkbox" value="">
			</cfif>
			<cfif arguments.newLine>
				<cf_relayFormElementDisplay attributeCollection=#arguments.fieldAttributes#>
			<cfelse>
				<cfif structKeyExists(arguments.fieldAttributes,"label") and structKeyExists(arguments.fieldAttributes,"labelAlign") and arguments.fieldAttributes.labelAlign eq "left">
					<cfoutput><label for="#arguments.fieldAttributes.fieldname#" id="#arguments.fieldAttributes.fieldname#_label" class="label">#arguments.fieldAttributes.label#</label></cfoutput>
				</cfif>
				<cf_relayFormElement attributeCollection=#arguments.fieldAttributes#>

				<cfif structKeyExists(arguments.fieldAttributes,"label") and structKeyExists(arguments.fieldAttributes,"labelAlign") and arguments.fieldAttributes.labelAlign eq "right">
					<cfoutput><label for="#arguments.fieldAttributes.fieldname#" id="#arguments.fieldAttributes.fieldname#_label" class="label">#arguments.fieldAttributes.label#</label></cfoutput>
				</cfif>
			</cfif>

			<!--- NJH 2016/03/24 BF-606 - as a result of some changes in xml editor, a disabled field was getting blanked on a record update because the field didn't exist but the _orig field did. (This work was done
				to support multi-selects). So, here, we are not outputting the orig field if the field is disabled or readonly to prevent this from happening. --->
			<cfif arguments.createOrig and not listFindNoCase("html,hidden",arguments.fieldAttributes.relayFormElementType) and (not structKeyExists(arguments.fieldAttributes,"disabled") or not arguments.fieldAttributes.disabled) and (not structKeyExists(arguments.fieldAttributes,"readonly") or not arguments.fieldAttributes.readonly)>
				<cf_encryptHiddenFields>
					<cf_input type="hidden" value="#arguments.fieldAttributes.currentValue#" name="#arguments.fieldAttributes.name#_orig">
				</cf_encryptHiddenFields>
			</cfif>

		</cfif>

		<cfreturn result_html>
	</cffunction>


	<cffunction name="displayXMLEditorFields" access="public">
		<cfargument name="fieldNodes" type="array" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="recordDetails" type="query" required="false">
		<cfargument name="newLine" type="boolean" default="true">
		<cfargument name="readOnly" type="boolean" default="false"> <!--- NJH whether record can be edited --->
		<cfargument name="quickAdd" type="boolean" default="false"> <!--- used when adding child records in the parent record edit screen.. a quick way of adding a child record and sometimes elements are handled slightly differently in this mode --->
		<cfargument name="primaryKey" type="string" required="false">

		<cfset var fieldAttributes =structNew()>
		<cfset var fieldNode = "">
		<cfset var filePrefix = "">
		<cfset var imageFileCount = 0>
		<cfset var fileCount = 0>
		<cfset var parametersStruct = structNew()>
		<cfset var displayHeight  = 0>
		<cfset var displayWidth  = 0>
		<cfset var filePath = "/content/linkImages/#arguments.entityType#/#arguments.entityID#/">
		<cfset var control = "">
		<cfset var fieldMetaData = structNew()>
		<cfset var subFieldAttributes = structNew()>
		<cfset var groupID = "">
		<cfset var currentRecord = ""> <!--- used in conditions...(ie. if currentRecord.entityID neq 0) --->
		<cfset var isImageFile = false>
		<cfset var filePathAndName = "">
		<cfset var allowFileDelete = true>
		<cfset var defaultImage = false>
		<cfset var fieldFunction = "">
		<cfset var functionArgs = structNew()>
		<cfset var functionStruct = structNew()>
		<cfset var comboQueryAttributes = structNew()>
		<cfset var queryName = "">
		<cfset var theQuery = "">
		<cfset var qrySelectable = "">
		<cfset var userGroupID = 0>
		<cfset var showRecordDelete = "true"> <!--- used to get passed through to the childRecords webservice --->
		<cfset var thePrimaryKey = "">
		<cfset var helpPhraseTextID = "">
		<cfset var placeHolderTextID = "">
		<cfset var htmlContent = "">
		<cfset var setHiddenDefaultImagePath = "">
		<cfset var functionStruct = {}>
		<cfset var functionResult = "">
		<cfset var attr = "">
		<cfset var qGetUserName = "">
		<cfset var entityFiles = "">

		<cfif not structKeyExists(arguments,"primaryKey")>
			<cfif structKeyExists(application.entityTypeID,arguments.entityType)>
				<cfset thePrimaryKey = application.com.relayEntity.getEntityType(entityTypeID=application.entityTypeID[arguments.entityType]).uniqueKey>
			</cfif>
		<cfelse>
			<cfset thePrimaryKey = arguments.primaryKey>
		</cfif>

		<cfif structKeyExists(arguments,"recordDetails")>
			<cfset currentRecord = arguments.recordDetails>
		<cfelse>
			<cfquery name="currentRecord" datasource="#application.siteDataSource#">
				select * from #arguments.entityType# where #thePrimaryKey# = #arguments.entityId#
			</cfquery>
		</cfif>

		<cfset var mergeStruct = {"#entityType#" = application.com.structureFunctions.queryRowToStruct(query=currentRecord,row=1)}>
		<cfset mergeStruct[entityType][thePrimaryKey] = arguments.entityId>

		<cfloop array="#arguments.fieldNodes#" index="fieldNode">
			<cfset fieldAttributes = duplicate(fieldNode.xmlattributes)>

			<!--- NJH 2013/02/20 Item 13 - now support for conditions on fields.. so, show if condition is true --->
			<cfif not structKeyExists(fieldAttributes,"condition") or evaluate(fieldAttributes.condition)>

				<!--- provide support for functions as fields.. probably a better way of doing this, rather than an evaluate, etc... --->
				<cfloop collection="#fieldAttributes#" item="attr">
					<!--- try and evaluate cf variables --->
					<cfif find("[[",fieldAttributes[attr]) and not left(fieldAttributes[attr],5) eq "func:">
						<cfset fieldAttributes[attr] = application.com.relayTranslations.checkForAndEvaluateCFVariables (phraseText = fieldAttributes[attr],mergeStruct = mergeStruct)>
						<cftry>
							<!--- attempt to evaluate any expressions. Only evaluate if not a single name --->
							<cfif listLen(fieldAttributes[attr]," ") gt 1>
								<cfset fieldAttributes[attr] = evaluate(fieldAttributes[attr])>
							</cfif>
							<cfcatch>
							</cfcatch>
						</cftry>
					</cfif>
					<cfif not listFindNoCase("query,validValues",attr) and (left(fieldAttributes[attr],8) eq "func:com" or left(fieldAttributes[attr],15) eq "func:getObject(")>
						<cfset functionStruct = application.com.relayForms.getFunctionPointerFromString(function=listLast(fieldAttributes[attr],":"))>
						<cfinvoke component="#functionStruct.componentPointer#" argumentcollection="#functionStruct.args#" method="#functionStruct.method#" returnvariable="functionResult">
						<cfif isStruct(functionResult)>
							<cfset functionResult = functionResult[listLast(fieldAttributes[attr],".")]>
						</cfif>
						<cfset fieldAttributes[attr] = functionResult>
					</cfif>
				</cfloop>

				<!--- NJH 2013/02/20 - if the whole record is not readonly, then decide on a field by field basis --->
				<cfif not arguments.readOnly>
					<cfif not structKeyExists(fieldAttributes,"readOnly")>
						<cfset fieldAttributes.readOnly = "false">
					<cfelse>
						<cfset fieldAttributes.readOnly = evaluate(fieldAttributes.readOnly)>
					</cfif>
				<cfelse>
					<cfset fieldAttributes.readOnly = "true">
				</cfif>
				<cfset fieldAttributes.labelStyle = "width:30%;white-space:nowrap;">

				<!--- primarily used for a non-dn field... show disconnect link in twitter accounts, for example.. --->
				<cfif trim(fieldNode.xmlText) neq "">
					<cfset fieldAttributes.currentValue = fieldNode.xmlText>
				</cfif>

				<cfset isImageFile = false>

				<!--- go through if the entityID is not 0 or if it's 0, and we have an entityType variable for it, then ignore the crud columns; if it's 0 and we don't have an entityType variable, then ignore created/lastupdated --->
				<cfif fieldNode.xmlName eq "field" and not (arguments.entityID eq 0 and ((listFindNoCase("created,createdBy,lastUpdated,lastUpdatedBy",fieldAttributes.name) and structKeyExists(application.entityTypeID,entityType)) or (listFindNoCase("created,lastUpdated",fieldAttributes.name) and not structKeyExists(application.entityTypeID,entityType))))>
					<cfif structKeyExists(fieldAttributes,"query")>
						<cfset var currentValueForSelect = structKeyExists(mergeStruct[arguments.entityType],fieldAttributes.name)?mergeStruct[arguments.entityType][fieldAttributes.name]:"">
						<cfset fieldAttributes = application.com.relayDisplay.getQueryAttributeForSelect(fieldAttributes=fieldAttributes,mergeStruct=mergeStruct,currentValue=currentValueForSelect)>
					</cfif>
					<cfset fieldAttributes.relayFormElementType = thePrimaryKey eq fieldAttributes.name?"html":"text">
					<cfset fieldAttributes.useCFForm = false>

					<cfif fieldAttributes.name neq "">
						<cfparam name="request.fieldList" default="">

						<cfif not listfindNoCase(request.fieldList,fieldAttributes.name) and not structKeyExists(fieldAttributes,"disabled")>
							<cfset request.fieldList = listAppend(request.fieldList,fieldAttributes.name)>
						</cfif>

						<cfset fieldMetaData = application.com.relayEntity.getTableFieldStructure(tablename=arguments.entityType,fieldname=fieldAttributes.name)>
						<cfif fieldMetaData.isOK>
							<cfif not structKeyExists(fieldAttributes,"label")>
								<cfset fieldAttributes.label = "phr_#arguments.entityType#_#fieldAttributes.name#">
							</cfif>

							<cfif fieldMetaData.maxLength gt 0>
								<cfset fieldAttributes.maxLength = fieldMetaData.maxLength>
							</cfif>

							<!--- 2015-10-06	WAB	Jira PROD2015-41
									Automatically populate range attribute for numeric fields
									Tidied up code which add validate, maxLength and size attributes for numeric types.  Values can now all come from fieldMetaData, don't need lots of case statements
							--->
							<cfif fieldMetaData.simpleDataType is "numeric">
								<cfset fieldAttributes.validate="numeric">
							</cfif>

							<!--- Set a range for numeric fields if not already defined.  Cold be cleverer and check that the supplied range is valid - but perhaps that would be overkill
								JIRA BF-17 - set the max and min fields if they are open ended...
							--->
							<cfif fieldMetaData.max is not "" and not structKeyExists(fieldAttributes,"range")>
								<cfset fieldAttributes.range = "#fieldMetaData.min#,#fieldMetaData.max#">
							</cfif>

							<cfif fieldMetaData.max is not "">
								<cfif not structKeyExists(fieldAttributes,"range")>
									<cfset fieldAttributes.range = "#fieldMetaData.min#,#fieldMetaData.max#">
								<cfelseif listLen(fieldAttributes.range) eq 1>
									<cfif listFirst(fieldAttributes.range,",",true) eq "">
										<cfset fieldAttributes.range = listSetAt(fieldAttributes.range,1,fieldMetaData.min,",",true)>
									<cfelseif listLast(fieldAttributes.range,",",true) eq "">
										<cfset fieldAttributes.range = listSetAt(fieldAttributes.range,2,fieldMetaData.max,",",true)>
									</cfif>
								</cfif>
							</cfif>

							<!--- NJH 2016/01/05 BF-61 - if range is set, then use that to set the max length. This assumes that the range supplied has been valid and is not longer than the maxlength of the db field.  --->
							<cfif structKeyExists(fieldAttributes,"range") and listlen(fieldAttributes.range) eq 2>
								<cfset fieldAttributes.maxLength = max(len(listFirst(fieldAttributes.range)),len(listLast(fieldAttributes.range)))>
							</cfif>

							<cfswitch expression="#fieldMetaData.data_type#">
								<cfcase value="bit">
									<cfif not arguments.quickAdd>
										<cfif not structKeyExists(fieldAttributes,"control") or fieldAttributes.control neq "hidden">
											<cfset fieldAttributes.control = "YorN">
										</cfif>
									<cfelse>
										<!--- on a quick add mode, we want to display checkboxes rather then a set of radios --->
										<cfset fieldAttributes.control = "checkbox">
										<cfif fieldMetaData.defaultValue eq 1>
											<cfset fieldAttributes.checked=true>
										</cfif>
										<cfset fieldAttributes.value= 1>				<!--- 2014-06-20 REH Case 439470 default the 'value' attribute to 1 irrespective of whether the checkbox is ticked or not - if checked this will post a value of 1 to the DB, otherwise it will be ignore as checkboxes are not submitted if not selected --->
										<cfset fieldAttributes.labelAlign = "right">
										<cfset fieldAttributes.label = "phr_yes">
									</cfif>
								</cfcase>
							</cfswitch>

							<!--- START: 2013-11-20 NYB Case 437839 added: --->
							<cfif not structkeyexists(fieldAttributes,"maxLength")>
								<cfset fieldAttributes.maxLength = fieldMetaData.maxLength neq -1?fieldMetaData.maxLength:4000>
							</cfif>

							<cfif not structkeyexists(fieldAttributes,"size")>
								<cfif fieldMetaData.simpleDataType is "numeric">
									<cfset fieldAttributes.size = fieldMetaData.maxLength>
								<cfelse>
									<cfset fieldAttributes.size = 50>
								</cfif>
							</cfif>

							<!--- END: 2013-11-20 NYB Case 437839 added: --->
							<cfif fieldAttributes.name neq thePrimaryKey>
								<cfif listFindNoCase("nvarchar,varchar",fieldMetaData.data_type) and fieldAttributes.maxLength gte 250>
									<cfset fieldAttributes.relayFormElementType = "textArea">
								<cfelse>
									<cfset fieldAttributes.relayFormElementType = fieldMetaData.simpleDataType>
								</cfif>
							</cfif>

							<cfset fieldAttributes.currentValue = fieldMetaData.defaultValue>
							<cfif fieldMetaData.defaultValue eq "getDate()">
								<cfset fieldAttributes.currentValue = request.requestTime>
							</cfif>
							<cfif not fieldMetaData.isNullable  and not fieldMetaData.hasDefault and fieldAttributes.name neq thePrimaryKey and not fieldAttributes.readOnly>
								<cfset fieldAttributes.required="true">
							</cfif>
						</cfif>

						<cfset fieldAttributes.fieldname = fieldAttributes.name>

						<!--- setting the current value. There are many different ways of setting it, and we really need to tidy it up.. but am leaving it for another day  --->
						<cfif arguments.entityID neq 0 and structKeyExists(arguments.recordDetails,fieldAttributes.name)>
							<cfset fieldAttributes.currentValue = arguments.recordDetails[fieldAttributes.name]>
						<!--- we have a field that is not a DB field and we're on an existing record --->
						<cfelseif arguments.entityID neq 0>
							<!--- if we want to set the current value for a non-DB field. THis is using some string that we can evaluate... can't remember now where this is being used! --->
							<cfif structKeyExists(fieldAttributes,"currentValue") and trim(fieldNode.xmlText) eq "" and listLen(fieldAttributes.currentValue," ") gt 1>
								<!--- START: 2014-04-30 NYB Case 439373 - added a try around the cfset --->
								<cftry>
									<cfset fieldAttributes.currentValue = evaluate(fieldAttributes.currentValue)>
									<cfcatch type="any">
										<!--- ie,  fieldAttributes.currentValue =  fieldAttributes.currentValue --->
									</cfcatch>
								</cftry>
								<!--- END: 2014-04-30 NYB Case 439373 --->
							</cfif>

						<!--- for a new record --->
						<cfelse>
							 <!---this is a case when some url variables have been passed through.. for example, when adding a product, the campaignId was passed through
								so that the product was added against that particular catalogue. THe xmlText takes precedence. IF that is not set, then other variables are looked at
								2016-06-29	WAB	During PROD2016-876.  Removed reference to request .encryption .encryptedfields.  Should not be necessary ('private variable')
								--->
							<cfif trim(fieldNode.xmlText) eq "">
								<cfif structKeyExists(url,fieldAttributes.name)>
									<cfset fieldAttributes.currentValue = url[fieldAttributes.name]>
								<cfelseif structKeyExists(fieldAttributes,"default")>
									<cfset fieldAttributes.currentValue = fieldAttributes.default>
								</cfif>
							</cfif>
						</cfif>
						<cfif not structKeyExists(fieldAttributes,"currentValue")>
							<cfset fieldAttributes.currentValue = "">
						</cfif>

						<cfif fieldAttributes.fieldName eq "lastUpdatedByPerson">
							<cfset fieldAttributes.currentValue = request.relayCurrentUser.personID>
						</cfif>

						<cfif arguments.entityID neq 0>
							<!--- display the created/modified information --->
							<cfif listFindNoCase("createdBy,lastUpdatedBy",fieldAttributes.fieldName)>
								<cfif fieldAttributes.currentValue eq "">
									<cfset fieldAttributes.currentValue = 0>
								</cfif>

								<cfquery name="qGetUserName" datasource="#application.siteDataSource#">
									select name from userGroup where userGroupID =  <cf_queryparam value="#fieldAttributes.currentValue#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfquery>

								<cfset fieldAttributes.currentValue = qGetUserName.name>
								<cfset fieldAttributes.relayFormElementType = "html">
								<cfset fieldAttributes.label = "phr_#fieldAttributes.fieldName#">
							<cfelseif listFindNoCase("created,lastUpdated",fieldAttributes.fieldName)>
								<cfset fieldAttributes.relayFormElementType = "html">
								<cfset fieldAttributes.currentValue = "#DateFormat(fieldAttributes.currentValue)# #TimeFormat(fieldAttributes.currentValue)#">
								<cfset fieldAttributes.label = "phr_#fieldAttributes.fieldName#">
							<cfelseif listFindNoCase("sysCreated,sysLastUpdated",fieldAttributes.fieldName)>
								<cfset var createdUpdatedField = replaceNoCase(fieldAttributes.fieldName,"sys","")>

								<cfset userGroupId = arguments.recordDetails[createdUpdatedField&'By'][1]>
								<cfif userGroupId eq "">
									<cfset userGroupID = 0>
								</cfif>
								<cfquery name="qGetUserName" datasource="#application.siteDataSource#">
									select name from userGroup where userGroupID =  <cf_queryparam value="#userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfquery>

								<cfset fieldAttributes.currentValue = qGetUserName.name & " #DateFormat(arguments.recordDetails[createdUpdatedField][1])# #TimeFormat(arguments.recordDetails[createdUpdatedField][1],"HH:mm:ss")#">
								<cfset fieldAttributes.relayFormElementType = "html">
								<cfset fieldAttributes.label = "phr_editor_#createdUpdatedField#">
							</cfif>
						<cfelse>
							<cfif listFindNoCase("createdBy,lastUpdatedBy",fieldAttributes.fieldName)>
								<cfset fieldAttributes.relayFormElementType = "hidden">
								<cfset fieldAttributes.currentValue = request.relayCurrentUser.userGroupId>
							</cfif>
						</cfif>
					<cfelseif structKeyExists(fieldAttributes,"value")>
						<cfset fieldAttributes.currentValue = fieldAttributes.value>
					<cfelse>
						<cfset fieldAttributes.currentValue = "">
					</cfif>

					<!--- for backwards compatibility - used also in email edit to set the emailTextID --->
					<cfif right(fieldAttributes.name,6) eq "TextID" and control neq "translation">
						<cfset fieldAttributes.validateAt = "onBlur,onServer,onSubmit">
						<cfset fieldAttributes.pattern = "^[A-Za-z_][A-Za-z0-9_]*$">
						<cfset fieldAttributes.validate="regex">
						<cfset fieldAttributes.message = "The #fieldAttributes.label# value must start with a letter and be followed only by letters, numbers or underscores.">
					</cfif>

					<cfif structKeyExists(fieldAttributes,"bindFunction") or structKeyExists(fieldAttributes,"validValues") or structKeyExists(fieldAttributes,"query")>
						<cfif not structKeyExists(fieldAttributes,"control") or fieldAttributes.control neq "html">
							<cfset fieldAttributes.relayFormElementType = "select">
						<cfelseif fieldAttributes.control eq "html">
							<cfset fieldAttributes.relayFormElementType = "html">
						</cfif>

						<cfif structKeyExists(fieldMetaData,"isNullable") and fieldMetaData.isNullable and not structKeyExists(fieldAttributes,"showNull")>
							<cfset fieldAttributes.showNull = true>
							<!--- 2015-07-01 WAB	CASE Fifteen-395  (related to Case 444625). Give Select boxes in XML editor a default null text --->
							<cfparam name="fieldAttributes.nullText" default="phr_Ext_SelectAValue">
						</cfif>

						<!--- setting the required fields for the twoSelectsCombo --->
						<cfif structKeyExists(fieldAttributes,"displayGroupAs") and fieldAttributes.displayGroupAs eq "select">
							<cfif structKeyExists(fieldAttributes,"required") and fieldAttributes.required>
								<cfif not structKeyExists(fieldAttributes,"required1")>
									<cfset fieldAttributes.required1 = fieldAttributes.required>
								</cfif>
								<cfif not structKeyExists(fieldAttributes,"required2")>
									<cfset fieldAttributes.required2 = fieldAttributes.required>
								</cfif>
							</cfif>
							<!--- <cfset fieldAttributes.FormName = "editorForm"> --->
						</cfif>
						<!--- this was to support twoSelectsComboQuery like the relayTag editor.. gets passed as valid values, but we need the query --->
						<cfif structKeyExists(fieldAttributes,"validValues")>
							<cfif not structKeyExists(fieldAttributes,"display")>
								<cfset fieldAttributes.display = "display">
							</cfif>
							<cfif not structKeyExists(fieldAttributes,"value")>
								<cfset fieldAttributes.value = "value">
							</cfif>

							<cfset var validValueMergeStruct = duplicate(mergeStruct)>
							<cfset validValueMergeStruct.currentValue = structKeyExists(mergeStruct[entityType],fieldAttributes.name)?mergeStruct[entityType][fieldAttributes.name]:"">
							<cfset fieldAttributes.query = application.com.relayForms.getValidValuesFromString(string = fieldAttributes.validValues,displayColumn=fieldAttributes.display,valueColumn=fieldAttributes.value,mergeStruct=validValueMergeStruct)>

							<cfif structKeyExists(fieldAttributes,"default") and fieldAttributes.currentValue eq "" and arguments.entityID eq 0>
								<cfset fieldAttributes.currentValue = fieldAttributes.default>
							</cfif>
						</cfif>
					<cfelseif structKeyExists(fieldAttributes,"control")>
						<cfset fieldAttributes.relayFormElementType = fieldAttributes.control>
					</cfif>

					<cfif structKeyExists(fieldAttributes,"control") and fieldAttributes.control eq "twoSelects">
						<cfset fieldAttributes.displayAs = fieldAttributes.control>
					</cfif>

					<cfif fieldAttributes.relayFormElementType eq "html">
						<!--- <cfset fieldAttributes.required  = false> ---> <!--- don't make html fields required --->
						<cfif not structKeyExists(fieldAttributes,"fieldname")>
							<cfset fieldAttributes.fieldName = "htmlField">
						</cfif>
						<cfif left(fieldAttributes.currentValue,5) eq "func:">
							<cfset functionArgs = structNew()>
							<cfset functionArgs[thePrimaryKey] =arguments.entityID>
							<cfif arguments.entityID neq 0>
								<cfset functionArgs = application.com.structureFunctions.queryToStruct(query=arguments.recordDetails,key=thePrimaryKey)>
								<cfset functionArgs = functionArgs[arguments.entityId]>
							</cfif>

							<cfset functionStruct = application.com.relayForms.getFunctionPointerFromString(function=listLast(fieldAttributes.currentValue,":"))>
							<cfset fieldFunction = functionStruct.functionPointer>
							<cfset fieldAttributes.currentValue = fieldFunction(argumentCollection=functionArgs)>
						</cfif>
					</cfif>

					<!--- backwords compatibility --->
					<cfif fieldAttributes.relayFormElementType contains "textArea" and fieldAttributes.relayFormElementType neq "textArea">
						<cfset fieldAttributes.maxChars = replace(fieldAttributes.relayFormElementType,"textArea","")>
						<cfset fieldAttributes.relayFormElementType = "textArea">
					</cfif>

					<cfif fieldAttributes.relayFormElementType eq "select">
						<!--- for backwards compatibility --->
						<cfif not structKeyExists(fieldAttributes,"display")>
							<cfset fieldAttributes.display = "display">
						</cfif>
						<cfif not structKeyExists(fieldAttributes,"value")>
							<cfset fieldAttributes.value = "value">
						</cfif>
					<cfelseif fieldAttributes.relayFormElementType eq "YorN">
						<cfset fieldAttributes.list = "1|phr_yes,0|phr_no">
						<cfset fieldAttributes.displayAs = "radio">
						<cfset fieldAttributes.relayFormElementType = "select">
					</cfif>

					<!--- set some styling for the table holding the radio group as output by validValue display in relayForms.cfc --->
					<!--- <cfif fieldAttributes.relayFormElementType eq "select" and structKeyExists(fieldAttributes,"displayAs") and fieldAttributes.displayAs eq "radio">
						<cfoutput>
						<style>
							###fieldAttributes.name#_table {float:left;}
						</style>
						</cfoutput>
					</cfif> --->

					<!--- output some noteText if the given phrasetextID exists --->
					<cfset helpPhraseTextID = "help_#arguments.entityType#_#fieldAttributes.name#">
					<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID=helpPhraseTextID)>
						<cfset fieldAttributes.noteText = "phr_"&helpPhraseTextID>
						<cfset fieldAttributes.noteTextPosition = "right">
					</cfif>

					<!--- set the placeholder text for the element --->
					<cfif (not structKeyExists(fieldAttributes,"readOnly") or not fieldAttributes.readOnly) and (not structKeyExists(fieldAttributes,"disabled") or not fieldAttributes.disabled)>
						<cfset fieldAttributes.placeholder="Enter #fieldAttributes.name#">
						<cfset placeHolderTextID = "placeholder_#arguments.entityType#_#fieldAttributes.name#">
						<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID=placeHolderTextID)>
							<cfset fieldAttributes.placeholder = "phr_"&placeHolderTextID>
						</cfif>
					</cfif>

					<cfif listFindNoCase("translation,file,includeTemplate,recordRights,countryScope,childRecords",fieldAttributes.relayFormElementType) and arguments.newLine>
						<cfset control = fieldAttributes.relayFormElementType>
						<cfset fieldAttributes.relayFormElementType="HTML">

						<cfset subFieldAttributes = duplicate(fieldAttributes)>

						<cfsavecontent variable  = "HTMLContent">
							<cfswitch expression="#control#">
								<cfcase value="countryScope">
									<cfif not structKeyExists(subFieldAttributes,"countryAndRegionList")>
										<cfset subFieldAttributes.countryAndRegionList = listappend(request.relaycurrentuser.countrylist,request.relaycurrentuser.regionlist)>
									</cfif>

									<CF_CountryScope
										entity="#arguments.entityType#"
										entityid="#arguments.entityID#"
										Form="editorForm"
										method = "edit"
										countryIDFilterList = "#subFieldAttributes.CountryAndRegionList#"
										attributeCollection=#subFieldAttributes#
									>
								</cfcase>

								<cfcase value="TRANSLATION">

									<!--- if we're dealing with a translation, then we work out the label for the translation, so that the required message is picking up the correct field, rather than the id of the translation input text box --->
									<cfif control eq "translation">
										<!--- create some default parameters, we need phraseTextID --->
										<cfset var parametersStructure  = structNew()>
									  	<cfset parametersStructure.phraseTextID  = "Title">
									  	<cfset parametersStructure.DisplayAs  = "Input">
										<cfset parametersStructure.Rows  = "4">
										<cfset parametersStructure.Cols  = "40">

										<cfset var parameters  = subFieldAttributes.parameters>
										<cfset structAppend(parametersStructure,application.com.structureFunctions.convertNameValuePairStringToStructure(parameters,","),true)>

										<cfif not StructKeyExists(parametersStructure,"phraseTextID")>
											<cfoutput>XML Structure Requires Field Parameter="PhraseTextID=yourPhraseTextID"></cfoutput><CF_ABORT>
										</cfif>

										<cfset structDelete(subFieldAttributes,"parameters")>
										<cfset structAppend(parametersStructure,subFieldAttributes)>

										<cfset var thisEntityID = arguments.entityID>
										<cfif arguments.entityID eq 0>
											<cfif structKeyExists(application.entityTypeID,entityType)>
												<cfset thisEntityID = application.com.relayEntity.getEntityType(entityTypeID=application.entityTypeID[entityType]).uniqueKey>
											<cfelse>
												<cfset thisEntityID = "#entityType#ID">
											</cfif>
										</cfif>
									</cfif>
									<cfparam name="fieldAttributes.required" default="false">
									<cf_editPhraseWidget entityID = #thisEntityID# entityType = #arguments.entityType# attributeCollection = #parametersStructure#>

									<cfset fieldAttributes.fieldname = editPhraseWidget.id>
								</cfcase>

								<!--- <cfcase value="YorN">
									<CF_INPUT TYPE="radio" id="#subFieldAttributes.name#_yes" NAME="#subFieldAttributes.name#" VALUE="1" Checked="#iif(isBoolean(subFieldAttributes.currentValue) and subFieldAttributes.currentValue,true,false)#" disabled="#subFieldAttributes.readOnly#"><cfoutput><label for="#subFieldAttributes.name#_yes">Yes</cfoutput>
									<CF_INPUT TYPE="radio" id="#subFieldAttributes.name#_no" NAME="#subFieldAttributes.name#" VALUE="0" Checked="#iif(not isBoolean(subFieldAttributes.currentValue) or not subFieldAttributes.currentValue,true,false)#" disabled="#subFieldAttributes.readOnly#"><cfoutput><label for="#subFieldAttributes.name#_no">No</cfoutput>
									<cfif fieldAttributes.readOnly>
										<cf_input type="hidden" name="#subFieldAttributes.name#" value="#subFieldAttributes.currentValue#">
									</cfif>
									<cf_input type="hidden" name="#subFieldAttributes.name#_orig" value="#subFieldAttributes.currentValue#">
								</cfcase> --->

								<cfcase value="file">
									<cfset fileCount++>
									<cfset subFieldAttributes.relayFormElementType="file">
									<cfset subFieldAttributes.fieldname = "file#fileCount#">
									<cfif structKeyExists(subFieldAttributes,"name") and subFieldAttributes.name neq "">
										<cfset subFieldAttributes.fieldname = subFieldAttributes.name>
									</cfif>
									<cfset subFieldAttributes.labelAlign = "">
									<!--- at the moment, fieldAttributes get modified when calling cf_relayFormElementDisplay above. ID gets set to "no_fieldname"--->
									<cfset subFieldAttributes.ID = subFieldAttributes.fieldname>
									<cfif structKeyExists(subFieldAttributes,"parameters")>
										<cfset parametersStruct = application.com.structureFunctions.convertNameValuePairStringToStructure(subFieldAttributes.parameters,",")>
										<cfset structAppend(subFieldAttributes,parametersStruct)>
									</cfif>

									<cfif structKeyExists(fieldAttributes,"acceptType") and fieldAttributes.acceptType eq "image">
										<cfset isImageFile = true>
									</cfif>
									<cfif structKeyExists(fieldAttributes,"accept")>
										<cfset subFieldAttributes.accept = fieldAttributes.accept>
									</cfif>

									<!--- Now handled in relayForms..
									<cf_includeJavascriptOnce template="/javascript/fileDownload.js">
									<cfif StructKeyExists(parametersStruct,"accept")>
										<cfset subFieldAttributes.submitFunction = "verifyFileExtension(thisField.value,'#parametersStruct.accept#');">
									<cfelseif isImageFile>
										<cfset subFieldAttributes.submitFunction = "verifyImageFile(thisField.value);">
									</cfif> --->

									<cfset filePrefix = "">
									<cfset filePathAndName = "">
									<cfif StructKeyExists(parametersStruct,"FilePrefix")>
										<cfset filePrefix  = parametersStruct.FilePrefix>
									</cfif>

									<!--- if there is an extension specified in the XML use it otherwise pick the first file from the folder with the prefix + entityid --->
									<cfif StructKeyExists(parametersStruct,"FileExtension")>
										<cfset var fileName  = filePrefix & arguments.entityID & "." & parametersStruct.FileExtension>
									<cfelse>
										<cfdirectory name="entityFiles" action="list" directory="#ExpandPath(filePath)#" filter="#filePrefix##arguments.entityID#.*">
										<cfset fileName  = entityFiles.name>
									</cfif>

									<cfif not fieldAttributes.readOnly>
										<cfset subFieldAttributes["currentValue"] = #filePath# & #fileName#>
										<cfset displayFormField(fieldAttributes=subFieldAttributes,entityID=arguments.entityID,newline="false")>
									</cfif>

									<cfset displayHeight = 0>
									<cfif StructKeyExists(parametersStruct,"displayHeight")>
										<cfset displayHeight  = parametersStruct.displayHeight>
									</cfif>

									<cfset displayWidth  = 0>
									<cfif StructKeyExists(parametersStruct,"displayWidth")>
										<cfset displayWidth  = parametersStruct.displayWidth>
									</cfif>

									<cfset allowFileDelete = true>
									<cfif StructKeyExists(parametersStruct,"allowFileDelete")>
										<cfset allowFileDelete  = parametersStruct.allowFileDelete>
									</cfif>

									<cfset defaultImage = false>
									<cfif StructKeyExists(parametersStruct,"defaultImage")>
										<cfset defaultImage  = parametersStruct.defaultImage>
									</cfif>

									<cfset setHiddenDefaultImagePath = false>

									<cfif isImageFile and defaultImage>
										<cfif arguments.entityID eq 0>
										<cfdirectory name="entityFiles" action="list" directory="#filePath#" filter="#filePrefix##arguments.entityID#.*">
										<cfloop query="entityFiles">
											<cfset filePathAndName = entityFiles.name>
										</cfloop>
										<cfset setHiddenDefaultImagePath = true>

										<!--- 2013-06-13	YMA	Lets create the default image if the entity already existed and is now set to have a default image --->
										<cfelseif arguments.entityID neq 0 and not FileExists("#ExpandPath(filePath)##fileName#")>
											<cfdirectory name="entityFiles" action="list" directory="#ExpandPath(replaceNoCase(filePath,'/'&arguments.entityID&'/','/0/'))#" filter="#filePrefix#0.*">
											<cfloop query="entityFiles">
												<cfset filePathAndName = replaceNoCase(filePath,'/'&arguments.entityID&'/','/0/') & entityFiles.name>
											</cfloop>
											<cfset setHiddenDefaultImagePath = true>
										</cfif>
									</cfif>

									<cfif filePathAndName eq "">
										<cfset filePathAndName = "#filePath##fileName#">
									</cfif>

								</cfcase>
								<cfcase value="includeTemplate">
									<cf_include template="#fieldAttributes.template#" checkIfExists="true">
								</cfcase>
								<cfcase value="recordRights">

									<cfset var defaultAttributes = {permissionLevel = 1, usergroupTypes = "", userGroupFilter  = false, suppressUserGroups = false, singleUserGroupTypeOnly = false, useExtendedRights=false }>
									<cfset structAppend (fieldAttributes,defaultAttributes,false)>

									<CF_UGRecordManager
										entity = "#arguments.entityType#"
										entityid = "#arguments.entityId#"
										level = "#fieldAttributes.permissionLevel#"
										suppressGroups = "#fieldAttributes.suppressUserGroups#"
										userGroupTypes = "#fieldAttributes.userGroupTypes#"
										userGroupFilter = "#fieldAttributes.userGroupFilter#"
										singleUserGroupTypeOnly = "#fieldAttributes.singleUserGroupTypeOnly#"
										useExtendedRights="#fieldAttributes.useExtendedRights#"
										readonly = "#fieldAttributes.readonly#"
										>
								</cfcase>

								<cfcase value="childRecords">
									<div id="childRecordsDiv">
									</div>

									<cfset showRecordDelete = true>
									<cfif structKeyExists(subFieldAttributes,"showDelete")>
										<cfset showRecordDelete = subFieldAttributes.showDelete>
									</cfif>

									<cfoutput>
									<script>
										showChildRecords('#subFieldAttributes.childTable#','#application.com.security.encryptVariableValue(name="columnsToShow",value=subFieldAttributes.showColumns)#','#showRecordDelete#');
									</script>
									</cfoutput>
								</cfcase>
							</cfswitch>

						</cfsavecontent>

						<cf_relayFormElementDisplay attributeCollection=#fieldAttributes#>
							<cfoutput>#htmlContent#</cfoutput>
						</cf_relayFormElementDisplay>

					<cfelse>
						<!--- only output the divs if not in quickAdd mode --->
						<cfif not arguments.quickAdd and not arguments.newLine>
							<cfoutput><div id="#fieldAttributes.name#_lineItemDiv" class="margin-bottom-15"></cfoutput>
						</cfif>
						<cfset displayFormField(fieldAttributes=fieldAttributes,entityID=arguments.entityID,newline=arguments.newLine,createOrig=true)>
						<cfif not arguments.quickAdd and not arguments.newLine>
							<cfoutput></div></cfoutput>
						</cfif>
					</cfif>


				<cfelseif fieldNode.xmlName eq "group">
					<cfif not (structKeyExists(fieldNode.xmlAttributes,"name") and fieldNode.xmlAttributes.name eq "systemFields" and arguments.entityID eq 0)>
					
						<cfset groupID = replace(fieldNode.xmlAttributes.label," ","","ALL")><!--- DCC 2016-08-25 451392 moved here from line 1238 --->
						
						<cfif request.relayFormDisplayStyle neq "HTML-div"><!--- 2014-08-12 AXA	Added responsive block for form groups. --->
							<cfoutput>
								
								<cfset var toggleGroup="if ($('#groupId#_toggle').getAttribute('src') == '/styles/cachedBGImages/level1_arrow_open.gif') $('#groupId#_toggle').src='/styles/cachedBGImages/level1_arrow_closed.gif'; else $('#groupId#_toggle').src='/styles/cachedBGImages/level1_arrow_open.gif';$('#groupID#').toggle();">
								<!--- 2015/08/13	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
														added id to roggle row in order to style it hidden in modules.cfm --->
								<tr id="#groupId#_toggleRow"><th colspan="2" onClick="#toggleGroup#"><img src="/styles/cachedBGImages/level1_arrow_open.gif" id="#groupID#_toggle"/> #htmlEditFormat(fieldNode.xmlAttributes.label)#</th></tr>
								<tbody id="#groupID#">
							</cfoutput>
							<cfset displayXMLEditorFields(fieldNodes=fieldNode.xmlChildren,argumentCollection=arguments)>
						<cfoutput></tbody></cfoutput>
						<!--- START: 2014-08-12 AXA	Added responsive block for form groups. --->
						<cfelse>
							<cfoutput>
								<div id="#groupID#">
									<h3>#htmlEditFormat(fieldNode.xmlAttributes.label)#</h3>
									<cfset displayXMLEditorFields(fieldNodes=fieldNode.xmlChildren,argumentCollection=arguments)>
								</div>
							</cfoutput>
						</cfif>
						<!--- END: 2014-08-12 AXA	Added responsive block for form groups. --->
					</cfif>
				<cfelseif fieldNode.xmlName eq "lineItem">
					<cfset fieldAttributes.relayFormElementType="HTML">
					<cf_relayFormElementDisplay attributeCollection=#fieldAttributes#>
						<cfset displayXMLEditorFields(fieldNodes=fieldNode.xmlChildren,newLine=false,argumentCollection=arguments)>
					</cf_relayFormElementDisplay>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn>
	</cffunction>

	<!--- WAB 2016-11-07 FormRenderer moved this code from cf_relayFormElementDisplay --->
	<cffunction name="relayFormElementDisplay" output="false">

		<cfargument name="Type" default="">
		<cfargument name="relayFormElementType" default="#arguments.Type#">
		<cfargument name="name" default="">
		<cfargument name="fieldName" default="#arguments.name#">

		<cfargument name="NoteText" default="">
		<cfargument name="NoteTextPosition" default="bottom"> <!--- Not supported in Div Layout --->
		<cfargument name="NoteTextImage" default="yes">
		<cfargument name="NoteTextImageSrc" default="/images/misc/iconhelpsmall.gif">
		<cfargument name="spanCols" default="listFindNoCase('submit,message', arguments.relayFormElementType)?yes:no">
		<cfargument name="labelAlign" default="left">
		<cfargument name="valueAlign" default="(arguments.relayFormElementType eq 'submit')?'right':'left'">
		<cfargument name="cellClass" default="">
		<cfargument name="labelSuffix" default="#(structKeyExists(request,'labelSuffix')?request.labelSuffix:':')#"> <!--- AJC 2007/01/23 --->
		<cfargument name="required" default="no">
		<cfargument name="case" default="">
		<cfargument name="trID" default=""> <!--- 2006-06-12 NJH currently needed for cf_divOpenClose --->
		<cfargument name="trStyle" default="">
		<cfargument name="tdStyle" default=""> <!--- 2011/05/16			NAS			LID5895 #127 "Main Contact" and other fields are not aligned properly --->
		<cfargument name="noWrap" type="boolean" default="false"> <!--- NJH 2007/01/15 --->
		<cfargument name="formName" type="string" default=""> <!--- NJH 2007/01/15 --->
		<cfargument name="noTR" default="" > <!--- GAD 2007/10/17 can be 'START', 'END', 'noTR' used for extending Form outputs or displays on a single row--->
		<cfargument name="useCFFORM" default="false" >
		<cfargument name="id" default="#arguments.fieldname#" >
		<cfargument name="tdLabelID" default="td_#lcase(arguments.id)#_label">
		<cfargument name="tdValueID" default="td_#lcase(arguments.id)#_value">
		<cfargument name="labelStyle" default="">
		<cfargument name="trClass" default="">
		<cfargument name="label" default="">
		<cfargument name="class" default="">
		<cfargument name="labelFor" default="#arguments.fieldName#"> <!--- WAB CASE 444394, add this attribute so that the LabelFor can be overridden/removed - especially for checkboxes --->



		<cfparam name="request.showRequiredSymbol" default="false"> <!--- NJH 2006/11/27 --->

		<cfset arguments.Type = arguments.relayFormElementType>
		<cfset arguments.name = arguments.fieldName>

<!--- 	TBD - lost this when converted to cfc
		<cfargument name="fieldname" default="no_fieldname" >		<!--- where a fieldname isn't supplied we can get multiple elements with the same td_ name but that should be ok --->
 --->



		<cfparam name="nonFormDisplayTypesList" default="MESSAGE,SCREEN,HTML,SUBMIT,BUTTON">

		<cfif listFindNoCase(nonFormDisplayTypesList,arguments.relayFormElementType) eq 0 and not structKeyExists (arguments,"label")>
			<cfreturn "Label Required">
		</cfif>


		<!--- 2016-01-30	WAB	Added support for NAME and TYPE as synonymns for FIELDNAME and RELAYFORMELEMENTTYPE
							With this construction the old names are still king, but supplying the new names by themselves works
		 --->


		<cfif arguments.relayFormElementType eq "">
			<cfreturn "<p>CF_relayFormElementDisplay requires attributes.Type (or attributes.RelayFormElementType can be used)</p>">
		</cfif>

		<cfif isDefined("request.relayFormDisplayStyle") and request.relayFormDisplayStyle eq "HTML-Form" and listFindNoCase(nonFormDisplayTypesList,arguments.relayFormElementType) eq 0 and arguments.fieldName is "" >
			<cfreturn "<p>CF_relayFormElementDisplay requires attributes.fieldName when used in HTML-Form style</p>">
		</cfif>



		<!--- 2013-08-20	YMA	Case 436645 sometimes required is passed in empty.  We need to convert that to false to prevent errors further down this code. --->
		<cfif arguments.required eq "">
			<cfset arguments.required = false>
		</cfif>

		<!--- WAB 2010/09/28 --->
		<cfif not arguments.useCFFORM >
			<cfset request.relayFORM.useRelayValidation = true>
		</cfif>

		<!--- if you are getting fields from more than one table then use Fieldsource to distnguish
			a list of fields for a given fieldsource is kept in structure #relayForm[tablename][fieldlist]#
		 --->
		 <cfif listFindNoCase("#nonFormDisplayTypesList#",arguments.relayFormElementType) eq 0>
			<cfparam name="arguments.FieldSource" default="">
			<cfif arguments.FieldSource is not "">
				<cfparam name="request.relayForm.fieldList[arguments.FieldSource]" default="">
				<cfset request.relayForm.fieldList[arguments.FieldSource] = listappend(request.relayForm.fieldList[arguments.FieldSource],arguments.fieldname)>
			</cfif>
			<cfparam name="request.relayForm.fieldList.All" default="">
			<cfset request.relayForm.fieldList.All = listappend(request.relayForm.fieldList.All,arguments.fieldname)>
		</cfif>

		<!--- start layout around the form element --->

			<cfif not structKeyExists(request,"relayFormDisplayStyle")>
				<!--- this is in here for backward compatibility --->
				<cfset request.relayFormLayout = "no-label">
			</cfif>

		<cfsavecontent variable="local.result">
			<cfif arguments.relayFormElementType neq "hidden">
				<!--- main relayFormDisplayStyle layout start--->

				<cfswitch expression="#arguments.case#">
					<cfcase value="upper">
						<cfset arguments.label=UCase(arguments.label)>
					</cfcase>
					<cfcase value="lower">
						<cfset arguments.label=LCase(arguments.label)>
					</cfcase>
					<cfdefaultcase>
					</cfdefaultcase>
				</cfswitch>

				<cfif arguments.relayFormElementType eq "MESSAGE">
					<cfset arguments.cellClass = "messageText">
				</cfif>


				<cfsavecontent variable="tableLabelCellNoColSpan">
					<cfoutput>
						<td <cfif arguments.tdLabelID neq "">id="#arguments.tdLabelID#" name="#arguments.tdLabelID#"</cfif> <cfif arguments.labelStyle neq "">style="#arguments.labelStyle#"</cfif> valign="top" align="#arguments.labelAlign#"><label for="#arguments.LabelFor#" <cfif arguments.required>class="required"</cfif>><cfif arguments.label neq "">#application.com.security.sanitiseHTML(arguments.label)##htmleditformat(arguments.labelSuffix)#<cfelse></cfif></label></td> <!--- 2014-11-28	AXA Case 442857 removed nested span-class=required from within a label-class=required --->
					</cfoutput>
				</cfsavecontent>

				<cfsavecontent variable="tableDataCellNoColSpan">
					<cfoutput>
						<td <cfif arguments.tdValueID neq "">id="#arguments.tdValueID#" name="#arguments.tdValueID#"</cfif> valign="top" align="#arguments.valueAlign#" <cfif arguments.noWrap>nowrap</cfif>>
					</cfoutput>
				</cfsavecontent>

				<cfsavecontent variable="tableLabelCellColSpan">
					<cfoutput>
						<td <cfif arguments.tdValueID neq "">id="#arguments.tdValueID#" name="#arguments.tdValueID#"</cfif> <cfif arguments.labelStyle neq "">style="#arguments.labelStyle#"</cfif> colspan="2" align="#arguments.valueAlign#" class="#arguments.cellClass#">
					</cfoutput>
				</cfsavecontent>

				<cfsavecontent variable="tableRow">
					<cfoutput>
						<tr <cfif arguments.trID neq "">id="#arguments.trID#" name="#arguments.trID#"</cfif> <cfif arguments.trStyle neq "">style="#arguments.trStyle#"</cfif> <cfif arguments.trClass neq "">class="#arguments.trClass#"</cfif>>
					</cfoutput>
				</cfsavecontent>

				<!--- 2014/06/05	YMA		Responsive Forms
						2015-03-18	WAB	Problems when displaying single checkbox (and presumably radio but would be odd case)
										If the label is given a FOR attribute then the responsive JS moves the label to the right of the checkbox.
										This behaviour is fine for groups of radios/checkboxes but is not suitable for single checkboxes, so in this case need to remove the FOR
		 				2015-04-15   WAB CASE 444394 implement arguments.LabelFor
				--->
				<cfsavecontent variable="divLayoutlabel">
					<cfif listfindnocase ("radio,checkbox",#arguments.relayFormElementType#)><cfset arguments.LabelFor = ""></cfif>
					<cfoutput>
						<!--- <div class="col-xs-12 col-sm-3 control-label<cfif arguments.required> required</cfif>" id="#arguments.fieldname#_label"> --->
						<cfif arguments.label neq "">
							<label <cfif arguments.id neq "">id="#arguments.id#_label"</cfif> <cfif arguments.LabelFor is not "">for="#arguments.LabelFor#"</cfif> <cfif arguments.required>class="required"</cfif>><cfif arguments.label neq "">#application.com.security.sanitiseHTML(arguments.label)##htmleditformat(arguments.labelSuffix)#<cfif request.showRequiredSymbol and arguments.required><span class="requiredSymbol">phr_sys_Form_requiredSymbol</span></cfif><cfelse></cfif></label>
						</cfif>
						<!--- note as image displayed here, note as text displayed further down.  Div layout does not support arguments.NoteTextPosition --->
						<cfif arguments.NoteText is not  "" and arguments.NoteTextImage eq "yes">
								<cfoutput><span title="#arguments.NoteText#"><img src="#arguments.NoteTextImageSrc#"></span></cfoutput>
						</cfif>

						<!---</div>--->
					</cfoutput>
				</cfsavecontent>

				<cfsavecontent variable="formGroupWrapper">
					<cfoutput>
						<div class="form-group" <cfif arguments.ID neq "">id="#arguments.ID#_formgroup"</cfif>>
					</cfoutput>
				</cfsavecontent>
				<cfsavecontent variable="inputWrapper">
					<cfoutput>
						<!---<div class="<cfif structKeyExists(arguments."inputWrapperOverrideClass")>#arguments.inputWrapperOverrideClass#<cfelse>col-xs-12 col-sm-9</cfif>" id="#arguments.fieldname#_value">--->
					</cfoutput>
				</cfsavecontent>
				<cfswitch expression="#request.relayFormDisplayStyle#">
					<cfcase value="no-label"><!--- blank - no need to set up a label ---></cfcase>
					<cfcase value="HTML-div">
						<cfoutput>
							#formGroupWrapper#
							<!--- SB 11/12/15 <div class="col-xs-12">--->
							<cfif not listfind("submit,button",arguments.RELAYFORMELEMENTTYPE)>
								#divLayoutlabel#
								#inputWrapper#
							<cfelse>
								<!--- <div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3"> --->
							</cfif>
						</cfoutput>
					</cfcase>
					<cfcase value="HTML-table"><!--- start the HTML-table layout --->
						<cfif arguments.relayFormElementType eq "SCREEN">
							<cfoutput>
							<tr <cfif arguments.trID neq "">id="#arguments.trID#" name="#arguments.trID#"</cfif> <cfif arguments.trStyle neq "">style="#arguments.trStyle#"</cfif>>
								<td colspan="2">
									<table id="relayFormElementDisplay01">
							</cfoutput>
						<cfelse>
							<cfoutput>
								<cfswitch expression="#arguments.noTR#">
									<cfcase value="START">
										<!--- 2007/10/17 GAD: starts a new table to enable Column based output for form elements
										Open a <TR> and then a <TABLE> for the extended info.... can be used to extend columns on a single row--->
										#tableRow#
										<cfif arguments.spanCols eq "yes">
											<td colspan="2" class="#htmleditformat(arguments.cellClass)#">
										<cfelse>
											#tableLabelCellNoColSpan#
											#tableDataCellNoColSpan#
										</cfif>

										<table id="relayFormElementDisplay02">
										#tableRow#
										<cfif arguments.spanCols eq "yes">
											<td <cfif arguments.tdValueID neq "">id="#arguments.tdValueID#" name="#arguments.tdValueID#"</cfif> align="#arguments.valueAlign#" class="#arguments.cellClass#">
										<cfelse>
											#tableDataCellNoColSpan#
										</cfif>
									</cfcase>
									<cfcase value="noTR,END">
										<!--- 2007/10/17 GAD: added to enable Column based output for form elements : OPENS A COLUMN FOR THE CLOSE noTR attribute--->
										<cfif arguments.spanCols eq "yes">
											#tableLabelCellColSpan#
										<cfelse>
											#tableLabelCellNoColSpan#
											#tableDataCellNoColSpan#
										</cfif>

									</cfcase>

									<cfdefaultcase>
										#tableRow#
										<!--- 2007/10/17 GAD: Original script before modification for noTR does normal ROW / Cols--->
										<cfif arguments.spanCols eq "yes">
											#tableLabelCellColSpan#
										<cfelse>
											#tableLabelCellNoColSpan#
											#tableDataCellNoColSpan#
										</cfif>

									</cfdefaultcase>
								</cfswitch>
							</cfoutput>
						</cfif>
					</cfcase>
					<cfcase value="HTML-Form"><!--- using the form label tag --->
						<!--- 2014-03-06	AXA	Case 439049 --->
						<cfoutput><label <cfif arguments.required>class="required"</cfif> for="#htmleditformat(arguments.label)#"><cfif arguments.label neq "">#htmleditformat(arguments.label)##htmleditformat(arguments.labelSuffix)#<cfelse>&nbsp;</cfif></label></cfoutput><!--- AJC 2007/01/23 --->
					</cfcase>
				</cfswitch>

				<!--- note text layout start --->
				<cfif arguments.NoteText is not  "" and arguments.NoteTextPosition neq "" and request.relayFormDisplayStyle neq "no-label">
					<cfif request.relayFormDisplayStyle eq "html-table">
						<cfoutput><table class="relayFormElementDisplay-help-layout table"></cfoutput>

						<cfswitch expression="#arguments.NoteTextPosition#">
							<cfcase value="top">
								<cfoutput>
									<tr>
										<td class="noteText">#arguments.NoteText#</td>
									</tr>
									<tr>
										<td valign="top">
								</cfoutput>
							</cfcase>
							<cfcase value="right">
								<cfoutput>
									<tr align="#arguments.valueAlign#">
										<!--- 2011/05/16			NAS			LID5895 #127 "Main Contact" and other fields are not aligned properly --->
										<td valign="top" align="#arguments.valueAlign#" style="#arguments.tdStyle#">
								</cfoutput>
							</cfcase>
							<cfcase value="bottom">
								<cfoutput>
									<tr>
										<td valign="top">
								</cfoutput>
							</cfcase>
						</cfswitch>
					</cfif>
				</cfif>
			</cfif>
			<!--- called with closing tag - use generated content --->
			<cfif structKeyExists (arguments,"html")>
				<cfoutput>
					#arguments.html#
				</cfoutput>
			<cfelse>
				<!--- called with single tag mode - generate output content --->
				<cfswitch expression="#arguments.relayFormElementType#">
					<cfcase value="HTML,MESSAGE">

						<!--- NJH 2010/03/26 - P-PAN002 - if outputting form in HTML and the element is normally a select, then get the value to be displayed
							and use that as the current value --->
						<cfif structKeyExists(arguments,"query")>
							<cfif isQuery(arguments.query)>

								<cfset metaDataArray = getMetaData(arguments.query)>
								<cfloop from=1 to=#arrayLen(metaDataArray)# index="i">
									<cfif metaDataArray[i].name eq arguments.value>
										<cfif structKeyExists(metaDataArray[i],"typeName") and listFindNoCase("int,int identity",metaDataArray[i].typeName) and arguments.currentValue eq "">
											<cfset arguments.currentValue = 0>
										</cfif>
										<cfbreak>
									</cfif>
								</cfloop>

								<cfquery name="arguments.query" dbType="query">
									select * from arguments.query where <cfif isNumeric(arguments.currentValue)>CAST ( [#arguments.value#] AS DECIMAL )<cfelse>CAST ( [#arguments.value#] AS VARCHAR )</cfif> = <cfif isNumeric(arguments.currentValue)>#arguments.currentValue#<cfelse>'#arguments.currentValue#'</cfif>
								</cfquery>

								<cfif arguments.query.recordCount>
									<cfset arguments.value = arguments.currentValue>
									<cfset arguments.currentValue = arguments.query[arguments.display]>
								<!--- if the value is a null value, then use the nullText as the current value --->
								<cfelseif structKeyExists(arguments,"nullText") and arguments.currentValue eq "">
									<cfset arguments.value = "">
									<cfset arguments.currentValue = arguments.nullText>

								<!--- otherwise use the value that has come in --->
								<cfelse>
									<cfset arguments.value = arguments.currentValue>
								</cfif>
							</cfif>
						<cfelse>

						</cfif>

						<cfoutput>
							<!--- 2014-11-19 AXA changed isDate to LSIsDate because decimal numbers were being formatted as dates. --->
							<cfif LSIsDate(arguments.currentValue)>
								<!--- change to odbcDateTime first, as the value in currentValue may not always be accepted in ls..format functions due to being formatted slightly differently --->
								<cfset odbcDateField = CreateODBCDateTime(arguments.currentValue)>
								<cfset thisValue = "#lsDateFormat(odbcDateField,"medium")# #lsTimeFormat(odbcDateField,"medium")#">
		                    <!---NJH 2016/0407 - removed this as I thought this a bit odd, and a customer also complained about this (Sugar 448817). Dawid couldn't remember why this
								was here and recommended that I get rid of it. So, am doing that for now. Need a better solution
							<cfelseif LSIsnumeric(arguments.currentValue)> <!--- 2015-09-04 DAN - number formatting for html display --->
		                        <cfset mask = ",_-_">
		                        <cfif find(".", arguments.currentValue)> <!--- only include decimal dot if it exists in the currentValue --->
		                            <cfset mask = ",_-_.__">
		                        </cfif>
		                        <cfset thisValue = lsnumberformat(arguments.currentValue, "#mask#")>--->
							<cfelse>
								<cfset thisValue = arguments.currentValue eq ""?"&nbsp;":arguments.currentValue>
							</cfif>
							<p class="form-control-static" <cfif arguments.Id neq "">id="#arguments.id#_value"</cfif>>#thisValue#</p>
						</cfoutput>

					</cfcase>
					<cfdefaultcase>
						<CF_relayFormElement
							relayFormElementType="#arguments.relayFormElementType#"
							attributecollection=#arguments#
							>
					</cfdefaultcase>

				</cfswitch>

			</cfif>

				<cfif arguments.relayFormElementType neq "hidden" and request.relayFormDisplayStyle neq "no-label">

					<cfif arguments.NoteText is not  "" AND arguments.NoteTextPosition neq "">
						<cfif request.relayFormDisplayStyle eq "html-table">
							<cfswitch expression="#arguments.NoteTextPosition#">
								<cfcase value="top,noTRend">
									<cfoutput>
											</td>
										</tr>
									</cfoutput>
								</cfcase>
								<cfcase value="right">
									<cfif arguments.NoteTextImage eq "yes">
										<cfoutput>
												<span class="help-icon" title="#arguments.NoteText#"><img src="#arguments.NoteTextImageSrc#"></span></td>
											</tr>
										</cfoutput>
									<cfelse>
										<cfoutput>
												#arguments.NoteText#</td>
											</tr>
										</cfoutput>
									</cfif>
								</cfcase>
								<cfcase value="bottom">
									<cfoutput>
											</td>
										</tr>
										<tr>
											<td class="noteText">#arguments.NoteText#</td>
										</tr>
									</cfoutput>
								</cfcase>
								<cfcase value="noTR">
									<cfoutput>
											</td>
								 	</cfoutput>
								</cfcase>

							</cfswitch>

							<cfoutput></table></cfoutput> <!--- WAB 2016-01-26 this line moved from outside of the cfif statement where it was appearing on html-div layouts --->

						<cfelse>

							<!--- note as text displayed here, note as image text displayed further up, next to label. arguments.NoteTextPosition--->
							<cfif arguments.NoteTextImage eq "NO">
								<cfoutput><span class="help-block">#arguments.NoteText#</span></cfoutput>
							</cfif>

						</cfif>

					</cfif>

				<!--- end the layout around the form element --->

					<cfswitch expression="#request.relayFormDisplayStyle#">
						<cfcase value="HTML-div">
						<!--- START: 2014-08-11 AXA --->
						<cfoutput>
								<!---</div>--->
							</div>
						</cfoutput>
						<!--- END: 2014-08-11 AXA --->
						</cfcase>
						<cfcase value="HTML-table"><!--- end the HTML-table layout --->
							<cfswitch expression="#arguments.noTR#"><!--- 2007/10/17 GAD: added to enable Column based output for form elements --->
								<cfcase value="START,noTR">
									<cfoutput></td></cfoutput>
								</cfcase>
								<cfcase value="END">
									<cfoutput>
									<!--- 17-10-2007 GAD: CLOSE THE noTR TABLE --->
										</td>
									</tr>
									</table>
									<!--- 17-10-2007 GAD: CLOSE THE MAIN DISPLAY TABLE --->
									</cfoutput>
							<cfif arguments.relayFormElementType eq "SCREEN">
									<cfoutput>
										</table>
									</td>
								</tr>
								</cfoutput>
							<cfelse>
								<cfoutput>
										</td>
									</tr>
								</cfoutput>
							</cfif>

								</cfcase>

								<cfdefaultcase>
									<cfif arguments.relayFormElementType eq "SCREEN">
										<cfoutput>
												</table>
											</td>
										</tr>
										</cfoutput>
									<cfelse>
										<cfoutput>
												</td>
											</tr>
										</cfoutput>
									</cfif>

								</cfdefaultcase>
							</cfswitch>


						</cfcase>
						<cfdefaultcase><!--- no additional layout info ---></cfdefaultcase>

					</cfswitch>
				</cfif>

				<!--- NJH 2010/04/16 P-PAN002 --->
				<!--- PPB/NJH 2011/01/18 P-REL099 moved this outside of the associated <TD></TD> so we can use the TD innerHTML to update the display of a HTML element
					NJH 2015/12/22 - check that value exists in arguments... doesn't always seem to be
				 --->
				<cfif structKeyExists(arguments,"makeHidden") and structKeyExists(arguments,"relayFormElementType") and (arguments.relayFormElementType eq "HTML" or arguments.relayFormElementType eq "Message") >

					<!---RJT BF-387 Because for queries the value field (at input) contains a field name, but then half way through the real value is set to be value and currentvalue is set to be the "display" value. --->
					<cfif (structKeyExists(arguments,"query") AND structKeyExists(arguments,'value')) or not structKeyExists(arguments,'currentValue')>
						<cfset valueInHiddenField=arguments.value>
					<cfelse>
						<cfset valueInHiddenField=arguments.currentValue>
					</cfif>

					<cfoutput><CF_INPUT type="hidden" name="#arguments.fieldName#" id="#arguments.id#" value="#valueInHiddenField#"></cfoutput>
				</cfif>
		</cfsavecontent>

		<cfreturn result>

	</cffunction>


</cfcomponent>