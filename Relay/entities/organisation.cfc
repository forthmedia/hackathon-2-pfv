/**
 * organisation
 *
 * @author nathaniel.hogeboom
 * @date 15/11/16
 **/
component extends="entity" accessors=true output=false persistent=false {

	public string function getPrimaryContactPersonIds() {
		if (!application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")) {
			return get("keyContactsPrimary");
		}
		return "";
	}


	public string function getAccountManagerPersonID() {
		if (!application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")) {
			return get("accManager");
		}
		return "";
	}

}