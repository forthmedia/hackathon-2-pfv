/**
 * location
 *
 * @author nathaniel.hogeboom
 * @date 15/11/16
 * 
 * 	WAB 2017-01-26 RT-193 Expose countryName and countryISOCode for backwards compatibility of relayCurrentUser.location
 **/
component extends="entity" accessors=true output=false persistent=false {

	public string function getPrimaryContactPersonIds() {
		if (application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")) {
			return this.getPrimaryContacts();
		} else {
			return this.getOrganisation().getPrimaryContactPersonIds();
		}
	}


	public string function getAccountManagerPersonID() {
		if (application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")) {
			return this.getLocAccManager();
		} else {
			return this.getOrganisation().getAccountManagerPersonID();
		}
	}


	/* 
	 	WAB 2017-01-26
	  	for backward compatibility with relaycurrentuser.location query we need to expose .countryName and .countryID 
	 	and make sure that they change if countryID is changed (although perhaps this is an edge case)
	*/
	
	private void function event_postLoad () {
		exposeCountryFieldsInThisScope ();		
	}

	private void function event_SetCountryID () {
		exposeCountryFieldsInThisScope ();		
	}

	private void function exposeCountryFieldsInThisScope () {
		this.countryName = getCountryName();
		this.countryisoCode = getCountryIsoCode();
	}

	public string function getCountryName () {
		var result = "";
		var countryID = this.getCountryID();
		if (countryID is not 0) {	
			result = application.countryName [countryID];
		}
		return result;
	}

	public string function  getCountryIsoCode() {
		var result = "";
		var countryID = this.getCountryID();
		if (countryID is not 0) {
			result = application.countryISO [countryID];
		}
		return result;
	}

}