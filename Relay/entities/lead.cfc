/**
 * lead
 *
 * @author nathaniel.hogeboom
 * @date 14/11/16
 **/
component displayname="lead" extends="entity" accessors=true output=false persistent=false {

	public boolean function isConverted(){
		return val(get("convertedOpportunityID")) neq 0 or (val(get("convertedPersonID")) and val(get("convertedLocationID")) neq 0)?true:false;
	}


	public boolean function canBeConverted(){
		return application.com.leads.leadCanBeConverted(isNewRecord()?0:getEntityID()) AND application.com.leads.personCanConvertLead();
	}


	public void function event_postInsert() {

		/* Lead Successfully Added Trigger Add email */
		sendLeadEmails(["LeadAccountManagerNotification","LeadNewAccountManagerNotification"],"vendorAccountManagerPersonID");
		sendLeadEmails(["LeadEmail"],"partnerSalesPersonID");
	}


	public void function event_postUpdate(required struct oldData) {

		var sendEmailsToAccountManager = listAppend("emailAccMngrLeadStatusChange",val(get("acceptedByPartner"))?"LeadAcceptedAccountManager":"LeadRejectedAccountManager");
		var sendEmailsToPartner = "";

		var rejectedStatusID = application.com.relayForms.getLookupList(fieldname='leadApprovalStatusID',textID='rejected').lookupID;
		var approvedStatusID = application.com.relayForms.getLookupList(fieldname='leadApprovalStatusID',textID='approved').lookupID;

		/* if approval status has changed */
		if (structKeyExists(arguments.oldData,"ApprovalStatusID")) {
            if (get("ApprovalStatusID") eq rejectedStatusID) {
            	sendEmailsToPartner = listAppend(sendEmailsToPartner,"LeadDeclinedEmail");
            } else if (get("ApprovalStatusID") eq approvedStatusID) {
            	sendEmailsToPartner = listAppend(sendEmailsToPartner,"LeadApprovedEmail");
            	sendEmailsToAccountManager = listAppend(sendEmailsToAccountManager,"LeadAccountManagerApproval");
            }
		}

		/* if account manager has changed */
		if (structKeyExists(arguments.oldData,"vendorAccountManagerPersonID")) {
			if (arguments.oldData.vendorAccountManagerPersonID neq "") {
				sendEmailsToAccountManager = listAppend(sendEmailsToAccountManager,"LeadOldAccountManagerNotification,emailAccMngrLeadAccMngrChange");
			} else {
				sendEmailsToAccountManager = listAppend(sendEmailsToAccountManager,"LeadNewAccountManagerNotification");
			}
		}

		sendLeadEmails(listToArray(sendEmailsToAccountManager),"vendorAccountManagerPersonID");
		sendLeadEmails(listToArray(sendEmailsToPartner),"partnerSalesPersonID");
	}


	public void function sendLeadEmails(required array emailTextArray, required string person) {
		if (get(arguments.person) neq "") {
			for(local.emailTextID in arguments.emailTextArray) {
				application.com.email.sendEmail(personID=get(arguments.person),emailTextID=local.emailTextID,mergeStruct={leadID=getEntityID()});
			}
		}
	}
}