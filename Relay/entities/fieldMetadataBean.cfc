<cfcomponent accessors="true">


<cfproperty name="APINAME">
<cfproperty name="DATATYPE">
<cfproperty name="DEFAULTVALUE">
<cfproperty name="DISPLAYAS">
<cfproperty name="FOREIGNKEY">
<cfproperty name="FOREIGNKEYVALUESSQL">
<cfproperty name="FULLFIELDNAME">
<cfproperty name="ISCOMPUTED">
<cfproperty name="ISIDENTITY">
<cfproperty name="ISNULLABLE">
<cfproperty name="ISPRIMARYKEY">
<cfproperty name="LABEL">
<cfproperty name="MAX">
<cfproperty name="MAXLENGTH">
<cfproperty name="MIN">
<cfproperty name="NAME">
<cfproperty name="PARAMETERS">
<cfproperty name="PICKLISTVALUESSQL">
<cfproperty name="SOURCE">
<cfproperty name="SOURCEID">
<cfproperty name="TABLENAME">
<cfproperty name="VALIDATE">
<cfproperty name="READONLY">

	<cfscript>

	public void function init () {
		for (item in arguments) {
			
			/* 	the getters and setters are generated automatically, 
			 	but if an extra column is added to the vFieldMetaData without a property being added then everything falls over
			 	so have added a test, which helps with backwards compatibility
			 */
			if (structKeyExists (this, "set#item#")) {
				var func = this["set#item#"];
				func(arguments[item]);
			} else {
				this [item] = arguments[item];
			}	
				
		}
		setparameters (application.com.structureFunctions.convertNameValuePairStringToStructure(inputString= getParameters(), delimiter=","));

	}

	public void function onmissingMethod (string missingMethodName) {
		var regExp = "(get|set)(.*)" ;
		var testForGetSet = application.com.regExp.refindAllOccurrences(regExp,missingMethodName,{1="getSet", 2="fieldName"}) ;

		if (testForGetSet[1].getSet is "get") {
			if (structKeyExists (this,testForGetSet[1].fieldName)) {
				return this [testForGetSet[1].fieldName];
			}
		} else if (testForGetSet[1].getSet is "set") {
			this [testForGetSet[1].fieldName] = missingMethodArguments.value;		
			return;
		}

		throw(message="Method #missingMethodName# does not exist", detail="Method #missingMethodName# does not exist");

	}


	public struct function getDisplayAttributes() {

		var result =  {	  displayAs = this.getdisplayAs()
						, label		= this.getlabel()
						, min 		= this.getmin()
						, max 		= this.getmax()
						, validate	= this.getvalidate()
						};

		/* delete empty items */
		for (local.item in result) {
			if (result[local.item] is "") {
				structDelete (result, local.item);
			}
		}

		structAppend(result, this.getparameters());

		/* set validValues from picklist/foreignKey SQL and then apply filter if required */
		var validValues = (getPickListValuesSQL( ) is not "")?getPickListValuesSQL( ):(getFOREIGNKEYVALUESSQL() is not "")?getFOREIGNKEYVALUESSQL():"";
		if (validValues is not "") {
			/*if (structKeyExists (result,"filter")) {
				validValues &= " AND " & result.filter;
				structDelete (result,"filter");
			}*/
			result.validValues = validValues;

		}


		return result;
	}


	public struct function getJoin(string entityTableAlias = this.getTableName()) {
		var join = {};

			if (this.getSource() is "flagGroup") {
				join = application.com.flag.getJoinInfoForFlagGroup (flagGroupID = this.getSourceID(), entityTableAlias = entityTableAlias);
				/* A hack to get flagtextID rather than flagID */
				join.selectField = replaceNoCase (join.selectField,".name",".flagTextID");
			} else if (this.getSource() is "flag") {
				join = application.com.flag.getJoinInfoForFlag (flagID = this.getSourceID(), entityTableAlias = entityTableAlias);
			}

		return join;
	}

	</cfscript>

	<cffunction name="getDefaultValue">
		<cfscript>
		var result = variables.defaultValue;
		if (result neq "") {
			/* currently not evaluating getdate(), as that will get run when record is inserted */
			if (result neq "(getdate())") {
				local.getDefaultValue = new Query(sql = "select #preserveSingleQuotes(defaultValue)# as defaultValue").execute().getResult();
				result = local.getDefaultValue.defaultValue;
			} else {
				result = "";
			}
		}
		return result;
		</cfscript>
	</cffunction>

	<!--- TBD we could be a bit cleverer about scale and for flags have something in the metadata to get the scale (and for core fields we ought to be able to look at the schema itself) --->
	<cffunction name="getCFqueryparamAttributes">
		<cfscript>
		var result = {}	;
		switch( lcase(this.getDataType())) {
				case "bit" :
					result.cfsqlType = 'cf_sql_bit';
					break;
				case "decimal" :
					result.cfsqlType = 'cf_sql_numeric';
					result.scale = '2';
					break;
				case "numeric" :
					result.cfsqlType = 'cf_sql_numeric';
					result.scale = '2';					
					break;
				case "date" :
					result.cfsqlType = 'cf_sql_date';
					break;
				default:
					result.cfsqlType = 'cf_sql_varchar';
					break;
		}
		return result;
		</cfscript>
	</cffunction>


</cfcomponent>