<!---
WAB 2016-11
WAB 2017-02-03	Added a getMagicNumber for backwards compatibility
				Removed an Event_PreUpdate which was only there to demonstrate that it worked
--->
<cfcomponent extends="entity">

	<cffunction name="getOrganisation">
		<cfreturn this.getLocation().getOrganisation()>
	</cffunction>


	<cffunction name="getOrganisationID">
		<cfif this.organisationID is not 0>
			<cfreturn this.organisationID>
		<cfelse>
			<cfreturn this.getLocation().getOrganisation()>
		</cfif>
	</cffunction>


	<cffunction name="getCountry">
		<cfreturn this.getLocation().getCountry()>
	</cffunction>


	<cffunction name="getCountryID">
		<cfreturn this.getLocation().getCountryID()>
	</cffunction>


	<cffunction name="getMagicNumber">
		<cfreturn application.com.login.getMagicNumber(entityID=this.getEntityID (), entityTypeID=0)>
	</cffunction>


	<cffunction name="Event_PostInsert">

		<!--- 2015-01-14 RPW JIRA 2015 Roadmap FIFTEEN 55 Modify Certifications to Register an Organization for Certifications 
			WAB 2016-11 copied from personDetail.cfm  into person.cfc
		--->
		<cfscript>
			if (fileExists("#application.paths.code#\cftemplates\code_addcertifications.cfc")) {
				application.com.code_addcertifications.AddDefaultCertifications(personID=this.getEntityID());
			}
		</cfscript>

	</cffunction>


	<cffunction name="getPrimaryContactPersonIds" output="false" returnType="string">
		<cfreturn this.getLocation().getPrimaryContactPersonIds()>
	</cffunction>

	<cffunction name="isPrimaryContact" output="false" returnType="boolean">
		<cfreturn listFindNoCase(getPrimaryContactPersonIds(),getEntityID())?true:false>
	</cffunction>


	<cffunction name="getAccountManagerPersonID" output="false" returnType="string">
		<cfreturn this.getLocation().getAccountManagerPersonID()>
	</cffunction>


	<cffunction name="validate_email" output="false" access="private" returnType="struct" hint="Checks for uniqueness of email if needed as well as the input meeting email requirements">
		<cfargument name="value" type="string" required="true" hint="The email address">

		<cfset var result = {isOK=true,message=""}>
		<cfset var uniqueEmailValidation = application.com.relayPLO.isUniqueEmailValidationOn()>

		<cfif uniqueEmailValidation.isOn and not application.com.relayPLO.isEmailAddressUnique(emailAddress=arguments.value,personID=this.isNewRecord()?0:this.getEntityID(),orgType=this.getLocation().get("accountTypeID")).isOK>
			<cfreturn {isOK=false,message=replace(application.com.relayTranslations.translatePhrase(phrase=uniqueEmailValidation.uniqueEmailMessage),"\n","")}>
		<cfelseif application.com.email.isValidEmail(emailAddress=arguments.value)>
			<cfreturn {isOK=false,message="Not a valid email address"}>
		</cfif>
		<cfreturn result>
	</cffunction>
</cfcomponent>