<!---
WAB 2016

This is the base of the Relayware Entity Object Model

2017-01-06  Add a PostLoad Event
2017-02-02  Add set#fieldName# Event (needed set event for RT-193)
2017-02-02	Added createdByName, lastupdatedbyName for backwards compatibility with relayEntity
			[ ] Escaped column names in select query
			Defaulted formatted argument in get to false (which is should have been) code is still experimental anyway
			Mods to the get function so that a call to get('XXXXX') can be overridden by getXXXXX()  [previously you would have had to call getXXXXX() directly]
			Alterations so that you can do say lead.getProgressTextID() and lead.setProgressTextID() and the lookupids will be updated correctly
			Added a simple delete function
2017-02-06	Fixed the validation which wasn't quite right, (and now returns a structure)
			Had problem with system where location.locationid had been given a default.  Explicitly exclude changing the identity
			Added functions for dealing with variables.dirtyFields (as part of fixing the above)
			Added Update#field# event
--->


<cfcomponent accessors="true" output="false">

	<cfset variables.dirtyFields = {}>
	<cfset variables.newRecord = false>
	<cfset this.persistedToSession = false>
	<cfset variables.validated = true>
	<cfset variables.isValid = true>
	<cfset variables.validationErrors = arrayNew(1)>

	<cfset variables.cacheVersion = 2>  <!--- TBD this is just a temporary solution for flushing the fieldMetaData queries --->


	<cffunction name="init" output="false">
		<cfargument name="entityID">
		<cfargument name="entityType">

		<cfscript>
		/* If this cfc has been extended by a customised entity then many of the variables may have been set, otherwise we need to work them out ourselves */
		if (not structKeyExists (variables,"entityType")) {
			variables.entityType = arguments.entityType;
		}

			variables.entityTypeStruct = application.com.relayEntity.getEntityType(variables.entityType);
			variables.entityUniqueKey = entityTypeStruct.uniqueKey; /* may need to change this to remove dependency */

		if (not structKeyExists (variables,"dataAccessView")) {
			var viewPrefix = (entityTypeStruct.flagsExist)?'v':'';
			variables.dataAccessView = viewPrefix & variables.entityType;
			variables.dataUpdateView = viewPrefix & variables.entityType & "_update";
		}

		if (arguments.entityID is 0) {
				variables.newRecord	= true;
				var getEntityIDQuery = new Query(sql = "
												declare @ident int = IDENT_CURRENT('#variables.entityType#')
												declare @newSeed int = @ident + 1

												DBCC CHECKIDENT (#variables.entityType#, reseed, @newSeed  )
												select @newSeed as entityID"
												, name = "getEntityID"
				);

				arguments.entityID = getEntityIDQuery.execute().getResult().entityID;
		}

		if (structKeyExists (arguments,entityType) and isQuery(arguments[entityType])) {
			/* load from query */
			var getEntity = arguments[entityType];
			variables.entityID = getEntity[variables.entityUniqueKey][1];

		} else {

			/* query data ourselves */
			variables.entityID = arguments.entityID;
			var fieldArray =  structKeyArray (getFieldsMetaDataStruct());

			var getEntityQuery = new Query(sql = "
													select
														#arrayToSelectList (fieldArray,'v')#
														, isNull (ec.name, isNull (ugc.name, 'Unknown')) as createdByName  /* createdbyName not well populated at moment */
														, isNull (eu.name, 'Unknown') as lastupdatedByName
													FROM
														#variables.dataAccessView# v
															left outer join usergroup ugc on v.createdby = ugc.usergroupid
															left outer join ventityName  ec on v.createdbyPerson = ec.entityID and ec.entityTypeID = 0
															left outer join ventityName  eu on v.lastupdatedbyPerson = eu.entityID and eu.entityTypeID = 0

													where
														v.#variables.entityUniqueKey# = :entityID
												"
											, name = "getEntity"
			);

			getEntityQuery.addParam(name="entityID", value=variables.entityID, cfsqltype="cf_sql_integer");
			getEntity = getEntityQuery.execute().getResult();

		}


			/* however for a brand new record there is no record so the above does not work */
			if (isNewRecord()) {
				queryAddRow (getEntity);
				this.recordCount = 0;

					for (fieldname in fieldArray) {
						var theDefault = getDefault(fieldname);
						if (theDefault is not "" and fieldName is not variables.entityUniqueKey) {
							querySetCell (getEntity, fieldname, theDefault, 1); /* do we need a getDefault method */
							setDirtyField (fieldname);
					}
				}
				querySetCell (getEntity,variables.entityUniqueKey,arguments.entityID,1);
				persistToSession ();

			} else {
				this.recordCount = 1;
			}

			/* pop all the data into this scope */
			for (var row in getEntity) {
				structAppend(this,row) ;
			}


			/* CF manages to convert bits to YES/NO , we want 0/1 so have to convert back*/
			var getBitsQuery = new Query(sql = "/*#variables.cacheVersion#*/
											select
												name
											FROM
												vFieldMetaData
											where
												tablename = '#variables.entityType#'
													and dataType = 'bit'
											"
											, name = "getBits"
                            				, cachedWithin=createTimespan(0, 1, 0, 0)

		);

		var getBits = getBitsQuery.execute().getResult();

		/* we are deliberately leaving empty spaces as is because it is concievable that a bit is nullable and so ie neither 1 nor 0. At the moment, it is up to the developer to test whether
		* 	a boolean is returned from a get(booleanField) */
		for (row in getBits) {
			if (this[row.name] neq "") {
				this[row.name] = int (this[row.name]) ;
			}
		}

		processEvent ("postLoad");

		return this;

		</cfscript>

	</cffunction>


	<!--- Removed, not supporting lazy loading at moment

	<cffunction name="getFields" hint="loads a list of fields if not already loaded" access="private" output="false">
		<cfargument name="fieldlist">

		<cfscript>
			var fieldArray = listToArray (fieldList);
			var arrayLength = arrayLen(fieldArray);

			for (var i = 1; i lte arrayLength; i++) {
				// remove if already loaded
				if (structKeyExists (this,fieldArray[i])) {
					arrayDeleteAt (fieldArray,i);
				} else {
					// checkForBooleanFlag

					/* WAB TODO An idea which might need revisiting
					var flag = application.com.flag.getFlagStructure(fieldArray[i]);
					if (flag.isOK and flag.entityType.tablename is entityType and flag.flagtype.dataTable is "boolean") {
						this[fieldArray[i]] = application.com.flag.checkBooleanFlagByID(flagid = flag.flagid, entityid = variables.entityid);
						arrayDeleteAt (fieldArray,i);
					}
					*/
				}
			}


		if ((arrayLen(fieldArray) is not 0 ) ) {
			var getEntityQuery = new Query(sql = "
												select
													#arrayToSelectList(fieldArray)#
												FROM
													#variables.dataAccessView#
												where
													#variables.entityUniqueKey# = :EntityID
												"
											, name = "getEntity"
			);

			getEntityQuery.addParam(name="entityID", value=variables.entityID, cfsqltype="cf_sql_integer");
			getEntity = getEntityQuery.execute().getResult();
			for (var row in getEntity) {
				structAppend(this,row) ;
			}
		}

		</cfscript>

	</cffunction>
 --->

	<cffunction name="get" output="false">
		<cfargument name="fieldname">
		<cfargument name="formatted" type="boolean" default="false">

		<cfscript>
		if (listLen(fieldName,'.') gt 1) {
			return getByDotPath (fieldname);

		} else if (fieldName is variables.entityType) {
			return this;

		} else if (structKeyExists (this,"get#fieldName#")) {
			var func = this["get#fieldName#"];
			return func();

		} else if (isAField(fieldName)) {
			 return getField (argumentCollection = arguments);

		} else if (isAnObject(fieldName)) {
			return getObject (fieldname);

		} else if (refindNocase ("_name", fieldName) and isAnObject(rereplaceNocase(fieldname, "_name", "")) ) {
			return getObject(rereplaceNocase(fieldname, "_name", "")).getName();

		}	else {
			writeoutput("Entity.cfc Unable to find Field/Object: #fieldName#");
			writedump(callStackget());

			abort;

		}
		</cfscript>

	</cffunction>


	<cffunction name="isAField" returns="boolean" hint="Is this the name of a field or derived field">
		<cfargument name="fieldname">
		<cfreturn structKeyExists (getFieldsMetaDataStruct(),fieldName)	OR structKeyExists (getDerivedFieldsMetaDataStruct(),fieldName)>
	</cffunction>


	<cffunction name="isAnObject" returns="boolean" hint="Is this the name of an object">
		<cfargument name="name" required="true">
		<cfreturn structKeyExists (getRelationships(),name)>
	</cffunction>

	<cffunction name="getField" output="false" access="private">
		<cfargument name="fieldname" required="true">
		<cfargument name="formatted" required="false" default = "false">

		<cfscript>

		if (not structKeyExists(this,fieldname)) {

			/* removed, not supporting lazy loading at moment
			if (structKeyExists (getFieldsMetaDataStruct(),fieldName)) {
				getFields (fieldname);

			} else
			*/

			if (structKeyExists (getDerivedFieldsMetaDataStruct(),fieldName)) {
				local.derivedField = getDerivedFieldMetaDataStruct(fieldName);

				if (local.derivedField.source is "flag") {
					/* TBD this is problematic. can't set this[fieldname] because if the parent field is changed then this item needs to change */
					return getABoolean (booleanName = fieldname, fieldname = local.derivedField.parentFieldName );
				}
				if (local.derivedField.source is "lookuplist") {
					var lookupValue = getField(replaceNoCase (fieldname,"textid","ID") );
					if (lookupValue is not "") {
						return application.lookup[getField(replaceNoCase (fieldname,"textid","ID") )].lookupTextID;
					} else {
						return '';
					}
				}
			}
		}

		var result= this[fieldName];

		if (formatted and structKeyExists (getFieldsMetaDataStruct(),fieldName)) {
			var metaData = getFieldsMetaDataStruct()[fieldName];

			if (metaData.getDataType() is "date"){
				result = lsdateformat (result);

			}

		}

		return result;

		</cfscript>
	</cffunction>


	<cffunction name="getABoolean" output="false">
		<cfargument name="booleanName">
		<cfargument name="fieldname">
			<cfoutput>#listFindNoCase (get(fieldname), booleanName)#</cfoutput>

		<cfreturn (listFindNoCase (get(fieldname), booleanName)?true:false) & listFindNoCase (get(fieldname), booleanName) & get(fieldname) >

	</cffunction>


	<cffunction name="setABoolean" output="false">
		<cfargument name="booleanName" required="true">
		<cfargument name="fieldname" required="true">
		<cfargument name="value" type="boolean">

		<cfset var fieldValue = get(fieldname)>
		<cfset var fieldMetaData = getFieldMetaDataStruct(fieldname)>

		<cfif fieldMetaData.getDisplayAs() is "select">
			<!--- just a radio, so a set it simple --->
			<cfset set (fieldname, booleanName)>
		<cfelse>
			<!--- checkbox, so have to add/remove its from current list --->
			<cfset var pos = listFindNoCase (fieldValue,booleanName)>

			<cfif value is true and pos is 0>
				<cfset fieldValue = listAppend (fieldValue,booleanName)>
				<cfset set (fieldname, fieldValue)>
			<cfelseif value is false and pos is not 0>
				<cfset fieldValue = listdeleteAt (fieldValue,pos)>
				<cfset set (fieldname, fieldValue)>
			</cfif>
		</cfif>

		<cfreturn>

	</cffunction>


	<cffunction name="getByDotPath" output="false" access="public">
		<cfargument name="path">

		<cfscript>
			if (listLen(path,".") is 1) {
				return get (path);

			} else {
				var local.object = getObject (listFirst(path,'.'));
				if (structKeyExists (local,"object")) {
					return local.object.getByDotPath (listRest(path,'.'));
				} else {
					return "";
				}
			}

		</cfscript>



	</cffunction>


	<cffunction name="getObject" output="false" access="public">
		<cfargument name="objectName" required="true">

		<cfscript>
		if (objectName is variables.entityType) {
			return this;
		}

		if (not structKeyExists(this,objectName)) {
			if (structKeyExists (this,"get#objectName#")) {
				var func = this["get#objectName#"];
				this[objectName] = func();
			} else if (structKeyExists (getRelationships(),objectName)){
				local.relationship = getRelationships()[objectName];
				if (local.relationship.relationshiptype is "foreignKey") {
					var foreignID = get(relationShip.localColumn);
					this[objectName] = application.com.entities.load(relationship.foreignObject, (isNumeric(foreignID)?foreignID:0) ) ;
				} else if (relationship.type is "childTable") {
					local.getChildEntityQuery = new Query(sql = "
															select
																#application.com.relayEntity.getEntityType(childEntity.childTable).uniqueKey# as entityID,
																#application.com.relayEntity.getEntityType(childEntity.childTable).nameExpression#  as name
															from
																#relationship.foreignTable#
															where #relationship.foreignColumn# = :entityID
														"
													, name = "local.getChildEntity"
					);

					local.getChildEntityQuery.addParam(name="entityID", value= get(relationship.localColumn), cfsqltype="cf_sql_integer");
					local.getChildEntity = getChildEntityQuery.execute().getResult();
					this[objectName] = getChildEntity ;

				}


			} else {
					writeoutput("in getObject function unable to find object of this Type");
					writedump(arguments);
					writedump(callStackGet());

					abort;

			}

		}

		return this[objectName];
		</cfscript>

	</cffunction>


	<cffunction name="set" output="false">
		<cfargument name="fieldname">
		<cfargument name="value">

		<cfscript>
		/* is the fieldname correct  and it's not the primary key*/
		if (arguments.fieldname neq getEntityUniqueKey()) {
			if (structKeyExists (getFieldsMetaDataStruct(), fieldname) ) {

				/*var validationResult = validateField (fieldName = fieldName, value = value);
				if (!validationResult.isOK) {
						validationError	(fieldname,value,validationResult.message);
				}*/

				if (not structKeyExists (this, arguments.fieldName)  OR compare (this[arguments.fieldName],arguments.value) is not 0) {
					setDirtyField (arguments.fieldName);
					this[arguments.fieldName] = arguments.value;
					processEvent (eventName = "set#arguments.fieldName#", oldValue = variables.dirtyFields[fieldName], newValue = arguments.value);

				}

			} else if (structKeyExists (getDerivedFieldsMetaDataStruct(), fieldname))  {
				local.derivedField = getDerivedFieldsMetaDataStruct()[fieldname];

				if (local.derivedField.source is "flag") {
					this.setABoolean (booleanName = fieldname, fieldname = local.derivedField.parentFieldName, value = value );
				}
				else if (local.derivedField.source is "lookuplist") {
					var idFieldName = replaceNoCase (fieldname,"textid","ID");
					this.set (idFieldName, application.lookupId["#getEntityType()##idFieldName#"][value]);
				} else {
					writeoutput ("update of field #fieldName# not supported");
				}

			}
		}

		return true;
		</cfscript>

	</cffunction>


	<cffunction name="validateField" output="false">
		<cfargument name="fieldname">
		<cfargument name="value">

		<cfscript>
			local.result = {isOK=true};
			local.fieldMetaData = getFieldMetaDataStruct(fieldname);

			switch (lcase(local.fieldMetaData.getDataType())) {
				case "numeric" :
					if (value is not "" and not isNumeric (value)) {
						local.result = {isOK = false, message="Must Be Numeric"};
					}
					break;

				case "bit":

					if (value is not "" and not isBoolean (value)) {
						local.result = {isOK = false, message="Must Be Boolean"};
					}
					break;

			}

			if (local.result.isOK) {
				/* not nullable validation */
				if (not local.fieldMetaData.getisNullable() and value is "") {
					local.result = {isOK = false, message="Not Nullable"}	;
				}

			}

			if (local.result.isOK) {

				/* primitive Validation */
				if (local.fieldMetaData.getDisplayAs() is "select") {
					if (listLen(value) gt 1) {
						local.result = {isOK = false, message="Only One Value May Be Set"}	;
					}
				}

			}

			if (local.result.isOK) {

				/* get specific validation */
				if (structKeyExists(variables,"validate_"&arguments.fieldname)) {
					local.func = variables["validate_"&arguments.fieldname];
					result = local.func(value=arguments.value);
				}

			}
			return result;
		</cfscript>
	</cffunction>


	<cffunction name="validationError" output="false">
		<cfargument name="fieldname">
		<cfargument name="Value">
		<cfargument name="message">

		<cfthrow message="Field:#fieldName#.  Value:#value#. #message#">

	</cffunction>


	<cffunction name="validationErrorArrayToString" output="false">

		<cfset var result = "">
		<cfloop array = "#variables.validationErrors#" index="local.error">
			<cfset result &= "Field: #local.error.field#; Value: #local.error.value#; #local.error.message#">
		</cfloop>
		<cfreturn result>

	</cffunction>


	<cffunction name="save" output="false">
		<cfset var field = "">

		<cfset var insert = !(variables.entityID is not 0 and not isNewRecord())>
		<cfset local.dirtyFields = getDirtyFields()>
		<cfset var result = {isOK = variables.isValid, fieldsUpdated = structCount (variables.dirtyFields), entityID = variables.entityID, action = (insert?"Insert":"Update")}>


		<cfif structCount (local.dirtyFields) >

			<cfif not variables.Validated>
				<cfset validate ()>
			</cfif>

			<cfif variables.isValid>
				<cfif !insert>
					<cfset processEvent ("preUpdate")>
					<cfquery name="qrySave">
					Update #variables.dataUpdateView#
					set
					<cfloop collection = "#local.dirtyFields#" item="field">
						[#field#] = <cf_queryparam value = "#this[field]#" attributeCollection=#getFieldMetaDataStruct(field).getCFqueryparamAttributes()# 	null = #isValueNull (value = this[field], fieldname = field)# >,
						</cfloop>
			--			lastupdated = getdate(),
						lastUpdatedByPerson = <cf_queryparam value = "#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
			<!--- 			lastUpdatedByPerson = <cf_queryparam value = "#request.relayCurrentUser.personid#" cfsqltype="cf_sql_integer"> --->
					where #entityUniqueKey# = <cf_queryparam value = "#variables.entityID#" cfsqltype="cf_sql_integer">
					</cfquery>

					<cfset var tempDirtyFields = structCopy (local.dirtyFields)>
					<cfset clearDirtyFields () >
					<cfset processEvent (eventName = "postupdate", oldData = tempDirtyFields)>

					<!--- not sure whether this should be done on just update, or on insert as well --->
					<cfloop collection="#tempDirtyFields#" item="local.dirtyField">
						<cfset processEvent(eventName="update#local.dirtyField#",oldValue=tempDirtyFields[local.dirtyField])>
					</cfloop>

				<cfelse>
					<cfset processEvent ("preinsert")>
					<cfquery name="local.qrySave">
					insert into #variables.dataUpdateView#
						(
						<cfif isNewRecord() and variables.entityID is not 0>
							#entityUniqueKey#,
						</cfif>
					<cfloop collection = "#local.dirtyFields#" item="field">
							[#field#],
						</cfloop>
						lastUpdatedByPerson
						)

					values
						(
						<cfif isNewRecord() and variables.entityID is not 0>
							<cf_queryparam value = "#variables.entityID#" cfsqltype="cf_sql_integer">,
						</cfif>
					<cfloop collection = "#local.dirtyFields#" item="field">
							<cf_queryparam value = "#this[field]#" attributeCollection=#getFieldMetaDataStruct(field).getCFqueryparamAttributes()#>,
						</cfloop>
						<cf_queryparam value = "#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
						)

						select @@identity as theidentity
					</cfquery>

					<cfset this[entityUniqueKey] = qrySave.theidentity>
					<cfset result.entityid = this[entityUniqueKey]>
					<cfset variables.newRecord = false>

					<cfset processEvent ("postinsert")>

				</cfif>

				<!--- Session persistence is only for new records at the moment, so once saved then clear --->
				<cfif this.persistedToSession>
					<cfset removeFromSession()>
				</cfif>
			<cfelse>
				<cfthrow message="#validationErrorArrayToString()#">
			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="isValueNull" output="false" returntype="boolean" hint="works out whether a blank should be interpreted as a null - based on the datatype and nullablility of the field">
		<cfargument name="fieldname" required="true" type="string">
		<cfargument name="Value" required="true" type="string">

		<cfset var metaData = getFieldMetaDataStruct(fieldname)>
		<!--- Field is null if blank unless it is a non nullable text field --->
		<cfset var result = (value is "" and not (metaData.getDataType() neq "Text" and not metaData.getisNullable()) )?true:false >

		<cfreturn result>
	</cffunction>


	<cffunction name="delete" output="false">

		<!--- Basic delete.  Not sure what should happen to object at this point! --->
		<cfquery name="local.delete">
		update
			#getEntityType()#
		set
			lastupdatedbyperson = <cf_queryparam value = "#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
			,lastupdated = getdate()
		where
			#entityUniqueKey# = <cf_queryparam value = "#variables.entityID#" cfsqltype="cf_sql_integer">

		delete from
			#getEntityType()#
		where
			#entityUniqueKey# = <cf_queryparam value = "#variables.entityID#" cfsqltype="cf_sql_integer">
		</cfquery>

	</cffunction>

	<cffunction name="persistToSession" output = "false" >
		<cfscript>
		application.com.entities.getSessionPersistenceStructForType(variables.entityType)[variables.entityID] = this;
		this.persistedToSession = true;
		</cfscript>

	</cffunction>


	<cffunction name="removeFromSession" output = "false" >
		<cfscript>
		structDelete (application.com.entities.getSessionPersistenceStructForType(variables.entityType), variables.entityID);
		this.persistedToSession = false;
		</cfscript>
	</cffunction>


	<cffunction name="isNewRecord" output="false" returns="boolean">
		<cfreturn variables.newRecord>
	</cffunction>

	<cffunction name="getCountryId" output = "false" returns="numeric">
		<cfif not structKeyExists (this,"countryID") or this.countryID is "">
			<cfreturn 0>
		<cfelse>
			<cfreturn this.countryID>
		</cfif>
	</cffunction>

	<cffunction name="processEvent" output="false">
		<cfargument name="eventName" required="true">

		<cfif structKeyExists (variables,"Event_#eventName#")>
			<cfset var func = variables["Event_#eventName#"]>
			<cfset structDelete (arguments,"eventName")>
			<cfset func(argumentCollection = arguments)>
		</cfif>

	</cffunction>

<!--- Not Used
	<cffunction name="save_entityUpsert" output="false">
		<cfset var field = "">

		<cfset local.dirtyFields = getDirtyFields()>
		<cfif structCount (local.dirtyFields)>
				<cfquery name="qrySave">
				select
					<cfloop collection = "#local.dirtyFields#" item="field">
						<cfset dataType = "cf_sql_varchar">
						<cfif isNumeric (this[field])>
							<cfset dataType = "cf_sql_numeric">
						<cfelseif isDate (this[field])>
							<cfset dataType = "cf_sql_date">
						</cfif>
					[#field#] = <cfqueryparam value = "#this[field]#" cfsqltype="#dataType#">,
					</cfloop>
					#entityUniqueKey# = <cfqueryparam value = "#variables.entityID#" cfsqltype="cf_sql_integer">
				into ##wab

				exec entityUpsert
						@validate = 0,
						@upsertTableName = '##wab',
						@columns_Updated  =   -1  ,
						@entityType = '#entityType#',
						@lastUpdatedByPerson = #request.relayCurrentUser.personid#
				</cfquery>

		</cfif>

		<cfreturn true>

	</cffunction>
 --->

	<cffunction name="getRelatedFiles" output="false">
		<cfquery name="local.qryRelatedFiles">
		SELECT
			*
		FROM
			relatedFileCategory rfc
				inner join
			relatedFile	rf ON rfc.fileCategoryID = rf.fileCategoryID
		WHERE
			rfc.fileCategoryEntity = <cfqueryparam value = "#variables.entityType#" cfsqltype="cf_SQL_varchar">
			AND rf.entityID = <cfqueryparam value = "#variables.entityID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn local.qryRelatedFiles>
	</cffunction>


	<cffunction name="onMissingMethod" output="false" access="public"> <!--- Note, must be public --->
		<cfargument name="missingMethodName">


		<cfscript>
			var regExp = "(get|Set|add|remove)(.*)" ;
			var testForGetSet = application.com.regExp.refindAllOccurrences(regExp,missingMethodName,{1="getSet", 2="fieldName"}) ;

			if (arrayLen(testForGetSet)) {

				if (structKeyExists(missingMethodArguments ,1) ) {
					var missingMethodArguments.value = missingMethodArguments [1];
					structDelete (missingMethodArguments , 1);
				}

				// calling function of form object.getFieldName() or object.setFieldName(value)
				if (testForGetSet[1].getSet is "get") {
					return get(testForGetSet[1].fieldName);

				} else if (testForGetSet[1].getSet is "add") {

					return add(ObjectType = testForGetSet[1].fieldName, argumentCollection = missingMethodArguments);

				} else if (testForGetSet[1].getSet is "remove") {

					return remove(objectType = testForGetSet[1].fieldName, argumentCollection = missingMethodArguments);

				} else {

					return set(fieldname = testForGetSet[1].fieldName, argumentCollection = missingMethodArguments);

				}

			} else {
				// assume trying to get or set a field by just doing object.fieldName()
				if (structCount(missingMethodArguments) is 1) {
					if (structKeyExists(missingMethodArguments ,1) ) {
						missingMethodArguments.value = missingMethodArguments [1];
						structDelete (missingMethodArguments , 1);
					}
					return set(fieldname = missingMethodName, argumentCollection = missingMethodArguments) ;
				} else {
					return get(missingMethodName);
				}
			}

		</cfscript>


	</cffunction>


	<cffunction name="arrayToSelectList" access="private" output="false" hint="takes an array of field and converts to a list with [ ] round each item for use in a select statement">
		<cfargument name="fieldArray" required = true>
		<cfargument name="alias" default = "">

		<cfset var result = "">
		<cfset var aliasAndDot = (arguments.alias is not ""?alias & ".":"")>

		<cfif arrayLen(fieldArray) gt 0>
			<cfset result = aliasAndDot & "[" & arrayToList (fieldArray, "]," & aliasAndDot & "[" ) &  "]">
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="describe" output="false">
		<cfargument name="fieldName">

		<cfif listLen (fieldName,".") gt 1>
			<cfif listFirst (fieldName,".") is variables.entityType>
				<cfreturn describe (listRest (fieldName,"."))>
			<cfelse>
				<cfreturn getObject(listFirst (fieldName,".")).describe (listRest (fieldName,"."))>
			</cfif>
		<cfelse>
			<cfreturn getFieldMetaDataStruct(arguments.fieldName)>
		</cfif>

	</cffunction>


	<cffunction name="describeObject" output="false">
		<cfargument name="objectName">

		<cfset var result = {}>
		<cfif objectName is variables.entityType>
			<cfset result  = {join = ""}>
		<cfelseif structKeyExists (getRelationships(),objectName)>
			<cfset result  = getRelationships()[objectName]>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="getFieldsMetaDataStruct" output="false">
		<cfif not structKeyExists (variables,"allFieldsMetaData")>
			<cfset variables.allFieldsMetaData = application.com.entities.getFieldMetadataStruct (getEntityType())>
		</cfif>

		<cfreturn variables.allFieldsMetaData>
	</cffunction>


	<cffunction name="getFieldMetaDataStruct" output="false">
		<cfargument name="fieldName" type="string" required="true">

		<cfreturn getFieldsMetaDataStruct()[fieldName]>
	</cffunction>


	<cffunction name="getDerivedFieldsMetaDataStruct" output="false">
		<cfif not structKeyExists (variables,"derivedFieldsMetaData")>
			<cfset variables.derivedFieldsMetaData = application.com.entities.getDerivedFieldMetadataStruct (getEntityType())>
		</cfif>

		<cfreturn variables.derivedFieldsMetaData>
	</cffunction>


	<cffunction name="getDerivedFieldMetaDataStruct" output="false">
		<cfargument name="fieldName" type="string" required="true">

		<cfreturn getDerivedFieldsMetaDataStruct()[fieldName]>
	</cffunction>

	<cffunction name="getDefault" output="false">
		<cfargument name="fieldName" type="string" required="true">

		<cfset var defaultValue = describe(arguments.fieldname).getDefaultValue()>
		<!--- <cfset defaultValue = getMergeObject().checkForAndEvaluateCFVariables(defaultValue)>

		<cfif defaultValue is not "">
			<cfquery name="local.default">
			select #preserveSingleQuotes(defaultValue)#	as value
			</cfquery>
			<cfset defaultValue = local.default.value>
		</cfif> --->

		<cfreturn defaultValue>
	</cffunction>


	<!--- <cffunction name="getFieldName" output="false" returnType="string">
		<cfargument name="fieldName" type="string" required="true">

		<cfreturn listLen(arguments.fieldName,".") gt 1?listRest(arguments.fieldName,"."):arguments.fieldName>
	</cffunction> --->


	<cffunction name="getPrimaryKey" output="false">
		<cfreturn get (variables.entityUniqueKey)>
	</cffunction>


	<cffunction name="getEntityType" output="false">
		<cfreturn variables.entityType>
	</cffunction>


	<cffunction name="getEntityID" output="false">
		<cfreturn get (variables.entityUniqueKey)>
	</cffunction>


	<cffunction name="getEntityUniqueKey" output="false">
		<cfreturn variables.entityUniqueKey>
	</cffunction>


	<cffunction name="getRelationships" output="false">

		<cfscript>
		if (not structKeyExists (variables,"relationships")) {
			var relationshipsQuery = new Query(sql = "/*#variables.cacheVersion#*/
												select
													'foreignkey' as relationshipType
													, case when right (name,2) = 'id' then left(name,len(name)-2) else name + '_' end as name  /* name of foreign object is column name with ID stripped off */
													, name as localColumn
													, foreignKey
													, dbo.regExMatch ('^.*(?=\.)',foreignKey,1) as foreignObject
													, dbo.regExMatch ('^.*(?=\.)',foreignKey,1) as foreignObjectAlias
													, dbo.regExMatch ('(?<=\.).*$',foreignKey,1) as foreignColumn
													, source
													, sourceid
													, '' plural

												FROM
													vFieldMetaData
												where
													tablename = '#variables.entityType#'
														and foreignKey is not null

												UNION
													SELECT
														'childTable'
														, OBJECT_NAME(fk.parent_object_id) as childTable  -- name
														, c2.name localColumn
														, ''
														, OBJECT_NAME(fk.referenced_object_id) foreignObject
														, OBJECT_NAME(fk.referenced_object_id) foreignObjectAlias
														, c1.name foreignColumn
														, ''
														, ''
														, OBJECT_NAME(fk.parent_object_id)+'s' as childTablePlural
													FROM
														sys.foreign_keys fk
													INNER JOIN
														sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
													INNER JOIN
														sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
													INNER JOIN
														sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id

													WHERE OBJECT_NAME(fk.referenced_object_id) = '#variables.entityType#'

												"
											, name = "relationships"
                            				, cachedWithin=createTimespan(0, 1, 0, 0)
			);

			var relationships = relationshipsQuery.execute().getResult();
			variables.relationships = application.com.structureFunctions.queryToStruct (relationships,"name");

			for (var foreignKey in variables.relationships) {
				var keyDetails = variables.relationships[foreignKey];

				if (keyDetails.source is "flag") {
					var join = application.com.flag.getJoinInfoForFlag (flagid = keyDetails.sourceid, entityTableAlias  = variables.entityType);
					variables.relationships[foreignKey].join = "/* join for #foreignKey# */" & replace (join.join, join.tableAlias & "_ent", foreignKey, "ALL");
				} else if (keyDetails.relationshipType is "foreignKey") {
					variables.relationships[foreignKey].join = "
																/* join for #foreignKey# */
																LEFT JOIN #keyDetails.foreignObject# AS #foreignKey# ON #variables.entityType#.#keyDetails.localColumn# = #foreignKey#.#keyDetails.foreignColumn#";
				} else if (keyDetails.relationshipType is "childTable"){
					variables.relationships[foreignKey].join = "
					/* join for #foreignKey# */
													LEFT JOIN #keyDetails.foreignObject# AS #keyDetails.foreignObject# ON #variables.entityType#.#keyDetails.localColumn# = #keyDetails.foreignObject#.#keyDetails.foreignColumn#";

				}

			}


		}

		return variables.relationships;

		</cfscript>

	</cffunction>


	<cffunction name="add" output="false">
		<cfargument name="ObjectType">

		<!--- describe the object --->
		<cfset var childEntity = getChildEntityRelationships()[objectType & "s"]>

		<cfset var obj = application.com.entities.new (objectType)>
 		<cfset obj.set (childEntity.childcolumn, get(childEntity.localColumn))>
		<cfset structDelete (arguments,"objectType")>
		<cfloop collection = #arguments# item = "local.x">
	 		<cfset obj.set (x, arguments[x])>
		</cfloop>

		<!--- TBD, maybe we should not be saving at this point, need to wait for the whole object to be persisted --->
		<cfset obj.save()>

		<!--- clear cached query of this entity --->
		<cfset structDelete (this,ObjectType & "s")>

		<cfreturn obj>

	</cffunction>


	<cffunction name="remove" output="false">
		<cfargument name="ObjectType">

		<!--- describe the object --->
		<cfset var childEntity = getChildEntityRelationships()[objectType & "s"]>

		<cfquery>
			delete from #objectType#
			where
				#childEntity.childcolumn# = #get(childEntity.localColumn)#
				<cfset structDelete (arguments,"objectType")>
				<cfloop collection=#arguments# item="local.filter">
					and #filter# = '#arguments[local.filter]#'
				</cfloop>
		</cfquery>

		<!--- clear cached query of this entity --->
		<cfset structDelete (this,ObjectType & "s")>

		<cfreturn true>

	</cffunction>


	<cffunction name="getMergeObject" output="false">
		<cfscript>
		if (not structKeyExists(variables, "mergeObject")) {
			variables.mergeObject = createObject ("component","com.mergeFunctions");
			variables.mergeObject.initialiseMergeStruct (mergeStruct = {"#getEntityType()#" = this});
		}
		return variables.mergeObject;
		</cfscript>
	</cffunction>


	<cffunction name="getFieldLastModification">
		<cfargument name="fieldName">

		<cfset var fieldMetaData = getFieldMetaDataStruct(fieldname)>

		<cfquery name="local.getLast">
		select top 1 *
		from
			vmodregister
		where
			entityType = '#getEntityType()#'
			and entityID = #getEntityID()#
		and
			<cfif fieldMetaData.getSource() is "core">
				modentityiD = #fieldMetaData.getSourceID()#
			<cfelseif fieldMetaData.getSource() is "flag">
				flagID = #fieldMetaData.getSourceID()#
			</cfif>
		order by moddate desc
		</cfquery>

		<cfreturn local.getLast>

	</cffunction>


	<cffunction name="rollback">
		<cfargument name="modDate" type="date">

		<cfquery name="local.getMods">
		select *
		from
			vmodregister
		where
			entityType = '#getEntityType()#'
			and entityID = #getEntityID()#
			and moddate >= #createODBCDateTime(modDate)#
		order by moddate desc
		</cfquery>

		<cfscript>
		local.result = {};
		for (local.row in getMods) {
			if (not structKeyExists (local.result, row.fieldname)) {
				local.result[row.fieldname] = {currentValue = row.newVal};
			}
			local.result[row.fieldname].rolledbackTo = row.oldVal;
			set (row.fieldName, row.oldVal);
		}

		return local.result;
		</cfscript>


	</cffunction>

	<cffunction name="getName">
		<cfif structKeyExists (getFieldsMetaDataStruct(),"Name")>
			<cfreturn this.name>
		<cfelse>
			<cfreturn this.get(entityTypeStruct.nameExpression)>
		</cfif>
	</cffunction>


	<cffunction name="validate" output="false" hint="Validates the entity" returnType="struct">

		<cfscript>
			variables.isValid=true;
			if (!variables.validated) {
				for (local.field in getDirtyFields()) {
					var newValue = this.get(local.field);
					local.validateResult = validateField(local.field, newValue);
					if (!local.validateResult.isOK) {
						variables.isValid=false;
						arrayAppend(variables.validationErrors,{field = field, message =local.validateResult.message, value = newValue});
					}
				}
			}
			variables.validated=true;
			return {isOk=variables.isValid,validationErrors=variables.validationErrors};
		</cfscript>
	</cffunction>


	<cffunction name="isValid" output="false" returnType="boolean">
		<cfreturn variables.isValid>
	</cffunction>

	<cffunction name="getDirtyFields" output="false" returnType="struct">
		<cfreturn variables.dirtyFields>
	</cffunction>

	<cffunction name="clearDirtyFields" output="false" returnType="void">
		<cfset variables.dirtyFields = {}>
	</cffunction>

	<cffunction name="setDirtyField" output="false" returnType="void" access="private">
		<cfargument name="fieldName" type="string" required="true">

		<cfscript>
		if (not structKeyExists (variables.dirtyFields,arguments.fieldName)) {
			if (structKeyExists (this, arguments.fieldName)) {
				variables.dirtyFields[fieldName] = this[arguments.fieldName];
			} else {
				variables.dirtyFields[fieldName] ="";
			}
		}
		variables.validated = false;
		</cfscript>

	</cffunction>

</cfcomponent>