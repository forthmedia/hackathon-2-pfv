/* �Relayware. All Rights Reserved 2014 */
/*
WAB Nov 2006
Some code for adding rows (and data) to tables 
requires a js array to define the new row

can take data pasted from Excel
2008/02/20    WAB functions for deleting/hiding rows by regular expression
2008/04/15		WAB modified the function for adding rows to a table to handle properties of the row and THs
2008/09/22		WAB localised some variables which had been contaminating each other
2009/05/12		WAB
2009/06/15		WAB	added function addRowsToTable (note 's') for adding lots of rows at a time using a single array containing lots of row arrays
					In addRowToTable, if the row array contains a rowID then this is used instead of the rowid passed in to the function
					In addRowToTable, renamed the inappropriately named variable groupID - don't think that it will cause a problem don't think that people have used variable names when calling this function
2010/02/16		WAB  functions for manipulating individual cells by name
*/

function copyTextAreaToTable(textAreaID,tableID,columnToDataMapping)  {

	if (columnToDataMapping == 'undefined') {columnToDataMapping = new Object}

	textArea = document.getElementById (textAreaID)
	inputValue = textArea.value

	table = document.getElementById(tableID)		
	lines = inputValue.split('\n')
	data = new Array(2)
	startRow = 0


	// create the dataMappings object
	// check for first line having headings, if so use to populate columnToDataMapping 
		columnToDataMapping = new Object 
		firstLine = lines[0].split('\t')
	if (firstLine[0].toLowerCase() == 'name' || firstLine[0].toLowerCase() == 'type' || firstLine[0].toLowerCase() == 'description')  {
		// first row has names of columns
		startRow = 1	
		for (j=0;j<firstLine.length;j++){
				columnToDataMapping[trim(firstLine[j].toLowerCase())] = j
		}
	} else {
		// otherwise get datamappings from the new row array
		newRowArray = eval ('newRow'+tableID)	

		for (j=0;j<newRowArray.length;j++){	
			columnToDataMapping[newRowArray[j].name.toLowerCase()] = j
		}
	}
	
	// loop through each row of data adding table rows and then data
	for (j=startRow;j<lines.length;j++) {
		data[j] = lines[j].split('\t')

		if (!isLastRowBlank (table)) {
			rowIndex = addRowToTable(tableID,eval ('newRow'+tableID))
		} else {
			rowIndex = 	table.rows.length-1
		}	
		addDataToRow (table,rowIndex,data[j],columnToDataMapping)  
	}	

}


function isLastRowBlank(table) {
		lastRow = table.rows[table.rows.length-1]
		firstCellOfLastRow = lastRow.childNodes[0]
		if (firstCellOfLastRow && firstCellOfLastRow.childNodes[0] && firstCellOfLastRow.childNodes[0].value == '') {
			return true
		} else {
			return false
		}	

}


// WAB 2008/07/02 created this function and modified  so that tableID can actually be the table object
// could have used $() if could guarantee that prototype loaded
function getObject (objectID) {
	return (typeof objectID == 'string') ? document.getElementById(objectID) : objectID
}


// WAB 2009/06/14 added this function to make it easier to add multiplerows at a time
function addRowsToTable(tableID,rowsArray,position,newRowID,updateIfRowExists) {

	
	for (var j = 0;j<rowsArray.length;j++) {
		position = addRowToTable(tableID,rowsArray[j],position,newRowID,updateIfRowExists)
		position = position + 1
	}
	

}


function addRowToTable(tableID,rowArray,position,newRowID,updateIfRowExists) {
//	newRowArray = eval ('newRow'+tableID)   // WAB added updateIfRowExists 21/03/2007
	/*
	tableID  - ID of table the row is to be added to
	rowArray 
		structure holding details of row and cells
		rowStructure.row.property
		rowStructure.cells[i].property
		(NOTE that used to just be an array of the cells, but realised that needed to have properties for the row itself so a bit of jiggery pokery required for backwards compatability)
	position  - index of row to add at
	newRowID  - is the ID of the row to be added  (WAB originally for some odd reason I called it called groupID  ).
	
	*/

	table = getObject (tableID)   // Note WAB 2009/05/12 someone had changed this back to getElementById, but this isn't right.  Don't know why done, hadn't gone to live, so rolled back

	// if no rowArray passed in we look for an array called newRow#tableID#
	if (!rowArray) {rowArray = eval ('newRow'+tableID)}  
	// jiggery pokery to deal with old and new structure of rowArray
	if (rowArray.row) {rowProperties = rowArray.row; cellArray = rowArray.cells; 	} else {rowProperties = new Array(1); cellArray = rowArray }

	// if the rowProperties has an ID attribute then this is the id of the row to be inserted
	// otherwise use the Id passed into to function
	if (rowProperties.id) {
		newRowID = rowProperties.id
	} else if (typeof(newRowID) != 'string' ) {
		newRowID = "addedRow";
	}
	// is there an existing row with this ID	
	existingRow = document.getElementById(newRowID)
	// If the position (rowIndex) where the row is to be added is not passed then add at end
	// 	WAB 2009/05/11 altered so that could handle position 0 
	//	if (!position) {position = table.rows.length}
 	 if (position == null) {position = table.rows.length}


	 // this bit is used for giving form fields in the new row unique ids
	 newIndex = 'new' + (table.rows.length>=10?"":"0") + table.rows.length   // adds a leading zero


	if (updateIfRowExists && existingRow && existingRow.parentNode.parentNode.id == table.id)   // thought that I better check that the row I've found with a matching ID is in this table, WAB 2010/02/13 should have been table.id not tableID
	{

		addRow=false
		var newRow = existingRow
		position = existingRow.rowIndex  // this is the position of the existing row, which will be returned by the function
	}else {
		addRow=true
		var newRow = table.insertRow(position);
		newRow.id = newRowID.replace(/INDEX/g,newIndex) ;   // 2009/05/12 added possibility of having an index in the row ID
	
	}
	
	if (rowProperties.classname) {
		newRow.className = rowProperties.classname; 
	}
	

	//alert ('position: ' + position)
	if (addRow){
	}


	cellArrayLength = cellArray.length

	

	for (i = 0; i<cellArrayLength; i++ ) {
		if (addRow) {
			if (!cellArray[i].isth) {
				newcell=newRow.insertCell(-1)
			} else {
				newcell = document.createElement("TH");
				newRow.appendChild(newcell);
			}

		} else {
			newcell=newRow.cells[i]
		}


		if (cellArray[i].colspan)   // note the lower case - cfml2js makes it all lower case
			{newcell.colSpan = cellArray[i].colspan	}
		if (cellArray[i].width)   // note the lower case - cfml2js makes it all lower case
			{newcell.width = cellArray[i].width	}
		if (cellArray[i].align)   // note the lower case - cfml2js makes it all lower case
			{newcell.align = cellArray[i].align	}
		if (cellArray[i].valign)   // note the lower case - cfml2js makes it all lower case
			{newcell.vAlign = cellArray[i].valign	}
		if (cellArray[i].classname)   // note the lower case - cfml2js makes it all lower case
			{newcell.className = cellArray[i].classname	}

		//alert ('oldcell ' + i + '  ' + newcell.innerHTML)
		//alert ('Cell ' + i + '  ' + cellArray[i].content)			
			
		 newcell.innerHTML = cellArray[i].content.toString().replace(/INDEX/g,newIndex)
	}
	
	return (position)
}


// This function originated from bulk loading of profiles
function addDataToRow  (table,rowIndex,data,dataMapping)
{

		var row = table.rows[rowIndex]	;
		var i = 0;
		/* loop through each cell in the table, check the name of the input object against the dataMapping and get corretc value from data */
		for (i = 0; i<row.childNodes.length; i++ ) {
			cell = row.childNodes[i]
			cellElement = cell.childNodes[0]
			cellElementName = cellElement.name.split('_')[0]

			if (dataMapping){
				dataColumnIndex = dataMapping[cellElementName.toLowerCase()]
			}else{
				dataColumnIndex = i
			}	
			
			//XXXX//alert(dataColumnIndex);

			if (data && data[dataColumnIndex]) {
				cellData = data[dataColumnIndex]
				if (cellElement.type == 'select-one'){
					populateDropDown (cellElement, cellData)
				} else {
					cellElement.value = trim(cellData)
				}// select box values have to be in correct case (we are using lower)

				
			}	
		}


}

/* given the id of a row, returns the row index */
function getRowIndexOfRow(tableID,rowID) {
	/* WAB 2010/02/16 replaced with a call to getRowById function
		table = getObject(tableID)
		rows = table.rows
		for (i=0; i<rows.length; i++) {
				if (rows[i].id == rowID) { return i}
		}
	*/	

	return getRowById(tableID,rowID).rowIndex
}

/* given the id of a row, returns the row Object */
function getRowById(tableID,rowID) {
		table = getObject(tableID)
		rows = table.rows
		for (i=0; i<rows.length; i++) {
				if (rows[i].id == rowID) { return rows[i]}

		}
}


function findRowsForDeletion(tableID,rowID) {
		regExp = new RegExp (rowID)
		deleteRowsByRegularExpression (tableID, regExp)

/*		var table = document.getElementById(tableID)
		var rows = table.rows
		var beginDelete = false;
		var deleteIDs = new Array();
		for (i=0; i<rows.length; i++) {
			if (rows[i].id == rowID) {
				deleteIDs.push(i);
			}
		}
		for (var y=deleteIDs.length-1;y>=0;y--) {
			table.deleteRow(deleteIDs[y]);
		}
*/
		
}

function deleteRowsByRegularExpression(tableID,regExp) {
		var table = getObject (tableID)
		var rows = table.rows
		var beginDelete = false;
		var deleteIDs = new Array();
		for (i=0; i<rows.length; i++) {
			if (regExp.test(rows[i].id)) {
				deleteIDs.push(i);
			}
		}
		for (var y=deleteIDs.length-1;y>=0;y--) {
			table.deleteRow(deleteIDs[y]);
		}
}

// WAB 2009/07/17 added support for tagName being a string which defines a regular expression - allows search for multiple type of tag eg tagname = 'INPUT|TEXTAREA'
function getElementsByRegularExpression(regExp,tagName,n) {                         // n is a Node 
	if(!n) n= document

	var result = new Array ()
	var tagNameRegExp = new RegExp (tagName)
	
    var children = n.childNodes;                // Now get all children of n
    for(var i=0; i < children.length; i++) {    // Loop through the children
		result = result.concat (getElementsByRegularExpression(regExp,tagName,children[i])) // Recurse on each one
    }
	
    if (n.nodeType == 1 && (tagName == undefined || tagNameRegExp.test(n.tagName) ) && regExp.test(n.id)) { /*Node.ELEMENT_NODE*/  // Check if n is an Element
		result.push(n) 
	}

	return result
}


function hideShowRowsByRegularExpression(tableID,regExp,hideShow) {
		var table = document.getElementById(tableID)
		var rows = table.rows

		for (i=0; i<rows.length; i++) {
			if (regExp.test(rows[i].id)) {
				rows[i].style.display = (hideShow)?'none':''
			}
		}
}
	
function checkForBlankRow(tableID) {
	if (!isLastRowBlank (table)) {
			addRowToTable(tableID,eval ('newRow'+tableID))
		}
		
}

// populates a drop down by either matching a value OR the text
function populateDropDown (object,value) {
	var i = 0;    // 2008/09/22 added this var to localise i - was contaminating i in function addDataToRow
	value = trim(value.toLowerCase())
	for (i=0;i<object.length;i++) {
		if (object[i].value == value) {object.selectedIndex = i; return}
	}
	for (i=0;i<object.length;i++) {
		if (object[i].text.toLowerCase() == value) {	; object.selectedIndex = i; return}
	}


}
	
// Removes leading whitespaces
function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
	
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
	
}

// Removes leading and ending whitespaces
function trim( value ) {
	
	return LTrim(RTrim(value));
}


/* WAB 2010/02/16 added these 'getParent' functions - which were developed for tfqoV2 */
function getParentOfParticularType (obj,tagName) {	
		if (obj.parentNode.tagName == tagName) 	{
			return obj.parentNode
		} else {
			return getParentOfParticularType (obj.parentNode,tagName)
		}
	
	}

function getParentTable (obj) { 
	return getParentOfParticularType (obj,'TABLE')
}	

function getParentRow (obj) { 
	return getParentOfParticularType (obj,'TR')
}	

function getParentCell (obj) { 
	return getParentOfParticularType (obj,'TD')
}	



/*  
	WAB 2010/02/16
	These functions are used for updating tables using named columns
	We use a convention that the cells in the first row of a table contain a columnname attribute
	So when we want to update a cell we use the details in the first row to find out which cell it is.
	We could just use the cellIndex to work things out, however ..
	.. Sometimes colspans are used to indent things in tables (grouping in new versions of TFQO).  In this case there might be cells 'missing' in a row
	To get round this I count up the colspans
*/	


/*  finds the cellIndex of the column with the given columnname */
function getCellIndexByColumnName (tableObj,rowObj,columnname) {
	    //find cell in first row with the correct columnname
		var firstRow = tableObj.rows[0]
		columnname = columnname.toLowerCase()
		for (i=0;i<firstRow.cells.length;i++) {
			var cell = firstRow.cells[i]
			var thisColumnName = cell.getAttribute('columnname')
			if (thisColumnName && thisColumnName.toLowerCase() == columnname) {
				return cell.cellIndex 
			}
		}

	return null	
}

/*  converts the cellIndex found in the above function to what I am calling the colspanOffset */
function getColspanOffset (rowObj,cellIndex) {
	// takes a cell and finds the total colspan from the left hand side
	if (cellIndex == null) return null
	var colspanOffset = 1 // current cell counts as one whatever the colspan
	for (var i = cellIndex -1;i>=0;i--) {
		colspanOffset +=  rowObj.cells[i].colSpan
	}
	return colspanOffset
}

/* Having found the colspanOffset of the named column (by looking in the first row of the table) we now apply this to row we are dealing with and return correct cell */
function getCellByColspanOffset (rowObj,colspanOffset) {
	// gets the cell which has this colspanOffset
	
	if (colspanOffset == null) return null
	var currentOffset = 1
	for (var i = 0;i<rowObj.cells.length;i++) {
		var currentCell = rowObj.cells[i]
		if (currentOffset == colspanOffset) {
			return currentCell
		} else if  (currentOffset > colspanOffset) {
			return null // if we arrive here it implies this named column doesn't appear in this row
		} else {
			
			currentOffset += currentCell.colSpan
		}
	}
	return null
}


/* Put the above 3 functions into a single easy to use function */
function getCellByColumnName  (rowObj,columnname) {
	tableObj = getParentTable(rowObj)
	var cellObj = getCellByColspanOffset(rowObj,getColspanOffset(rowObj,getCellIndexByColumnName (tableObj,tableObj.rows[0],columnname))) 
	return  cellObj

}

/*
Updates an existing cell in a table
table - can be object or id
row - can be object or id  (if is an object the table parameter is superfulous)
cell - can be object or the columnname   (if is an object then table and row parameters are superfulous)
data - innerHTML to update

*/
function updateTableCell (table,row,cell,data) {
	

	if (typeof cell == 'string') {

		if (typeof row == 'string') {
			table = $(table)
			row = getRowById (table,row)
		}
	
		cell = getCellByColumnName (row,cell)

	}

	if (cell)  {
		cell.innerHTML = data
	} 
	
	
	
	return cell

}

function getTotalColspan (tableObj) {
	// gets the total colspan of a table (
	
	var colspan = 0
	var row0 = tableObj.rows[0];
	for (var i = 0;i<row0.cells.length;i++) {
		colspan += row0.cells[i].colSpan
	}
	return colspan
}


/*
WAB 2010/02/17
Trying something out - I'm not very good a objects and stuff but this is an attempt to make it easier to add rows (using my rowstructure method) from within javascript
*/
// Cell object

function rowObject(id,options) {
	this.row = new Array ()   
	this.row.id = id
	this.cells = new Array ()   
	for (var property in options) {
		this.row[property] = options[property]
	}


};

rowObject.prototype.addCell = function(content,options) { 
	cellIndex = this.cells.length
	this.cells[cellIndex] = new Array ()
	this.cells[cellIndex].content = content
	for (var property in options) {
		this.cells[cellIndex][property] = options[property]
	}

	
	//this.cells[cellIndex].colspan = colspan
}

rowObject.prototype.addToTable = function(tableid,position) { 
	rowIndex = addRowToTable(tableid,this,position)
	return $(tableid).rows[rowIndex]     // not sure whether best to return object or the rowIndex (which is what addRowToTable returns)
}


/* trying a function to make it very easy to add a single celled new row to a table 
pass in 
table    - id or object
position - position at which to add row (or null for end)
rowid   - id of row to be created, or null
content - cell content
celloptions   - eg {align:'left'}  valign, classname etc (if supported)
leftoffset,rightoffset   - adds blank cells of given colspan to left and right.  Automatically makes the colspan of the content cell correct
*/


function addFullWidthRow (table,position,rowid,content,celloptions,leftOffset,rightOffset) {
	theRow = new rowObject (rowid)
	tableObj = $(table)
	totalColspan = getTotalColspan(tableObj)
	cellColspan = totalColspan - ((leftOffset)?leftOffset:0 )- ((rightOffset)?rightOffset:0)
	
	
		if(leftOffset) {
			// add a blank cell of appropriate colspan if required
			theRow.addCell ('&nbsp;',{colspan : leftOffset})
		}
		//append passed in options to the colspan option which I have worked out
		tempOptions = {colspan : cellColspan}
		for (property in celloptions) {
			tempOptions[property] = celloptions[property]
		}
		theRow.addCell (content,tempOptions)

		if(rightOffset) {
			// add a blank cell of appropriate colspan if required
			theRow.addCell ('&nbsp;',{colspan : rightOffset})
		}

		theRow.addToTable(table,position)

}


function getParentOfParticularType (obj,tagName) {	
	if (obj.parentNode.tagName == tagName) 	{
		return obj.parentNode
	} else {
		return getParentOfParticularType (obj.parentNode,tagName)
	}
}