/* ©Relayware. All Rights Reserved 2014 */
/*
	WAB 2010/06
	Code to do form validation which will work better with Ajax (and other situations where CFFORM is unsuitable)
	WAB 2011/03/03 mods to get the error div to work properly
	Also decided that labels should come from the actual element.
	WAB 2011/12/05 various improvments to layout of validation errors etc.
	2012-01-26 NYB Case:426292 added translation for Required
 	2012/02/17 PPB Case 424403 caused a problem when there was a required field with helptext/noteText against it
 	2012-07-12 PPB MF001 changes to 'Required' error messages
 	2012-11-14 WAB 432042 problem with IE9 - had to add an extra $ to get the prototype methods
 	2013-02-20 WAB During CASE 433389. Reversed some of the 2012-07-12 PPB changes. Required message comes from Label + phr.Required, not from a passed in message.  Philosophy is that the Label and the Message are dealt with separately so that a) when we go to inline error messages, the Label is not required. b) We get consistency of output c) Easier translation.
 	 	 	Also reverted to : separator between label and message, although have kept space for when used on CFFORM
	2013-03	 WAB  Major changes.
					Added support for a requiredFunction, and for checking the required function after every change
					Support for automatically updating Labels with a required class
					Support for 'onchange' inline validation
					Added a new function for initialising relayFormValidation - adds an onChange listener (or for some applications just an onchange function)
					Options are now stored as a property of the form object, so don't need to be passed around as much
	2013-04-23	NJH	Added the novalidate attribute so that any elements that have a novalidate attribute are ignored in a form validation. This was because
					I had some fields that were in 'in a different form' (form within a form scenario) and I didn't want to submit them when submitting the main form.
	2013-06-11	WAB	CASE 435691 - Fix IE8 problem where getAttribute didn't return quite what was expected
	2013-12-16	WAB Fixed bug when getting label for a field which had dots in its ID
	2014-09-22	WAB added support for masks
	2015-06-28	WAB	Added setVisibilityForControlRow() function which shows/hides a row and automatically switches validation on/off.  (originally used on oppEdit.js)
	2015-10-06	WAB Jira PROD2015-41 Range validation needs to to parseFloat rather than parseInt
	2016-02-02 	WAB Add concept of validation-group div to help when required filed is hidden or is a group of things.  
					May actually be able to use the form-group concept - we'll see
					Improve focusing on first errored item.  If item is hidden then scroll to parent node
				 	Added support for select boxes which have numeric null values as defined by data-nullvalue attribute
					Added checking for message not being defined.  For validate, just make up a message based on the value of validate
					Added triggerFormSubmit function - will do a form submit firing all the correct events		
					Tried to improve efficiency by caching pointers to all the error messages and objets given classes (may have IE problems)
	2016-03-04	WAB	BF-522 Prevent multiple validation errors appearing on a single item
 	2016-06-10	WAB BF-975 / CASE 449815. Protect setRequired function from being passed a non-existent object
	2016-11-11	WAB	Form Renderer Implement conditional display using data-showOtherFields attribute
					Added some catches around required/submit functions
 
 
*  	TODO 	IE Issues with remove()
 			Deal with plugins
 			Updating required status and labels as other things change is a bit patchy.  Labels change but existing messages are not removed
 
 
  	TBD Could all be encapsulated into a nice object, but I'm not sure that I am good enough to do that


 */



/*
relayFormOnSubmit
This is the function to put as a FORM's onSubmit function  ie: onsubmit="return relayFormOnSubmit(this)"
Gets the Array of Error Messages
	If array has items, displays the message and returns false
	otherwise returns true

Also has a function for adding a specific class to the tag immediately surrounding the element on error
[this is just an idea at the moment]
*/

function initialiseRelayFormValidation (form,options) {

    if (!form) {
        // 2015-08-20 DAN - prevent further JS errors when form is undefined
        console.log("form is undefined");
        return;
    }

	// store the options as a property of the form
	form.FormValidationOptions = extendRelayValidationOptions (options)

	form.setAttribute('novalidate','true')
	// Bind the onSubmit listener.  In special cases we need to do it as the form.onSumit (when the form is going to be validated and submitted by javascript)
	if(form.FormValidationOptions.bindValidationWithListener) {
		Event.observe(form, 'submit',relayFormOnSubmitEvent.bindAsEventListener(form));
	} else {
		form.onsubmit = relayFormOnSubmit
	}

	// Add onchange listener.  IE does not seem to support bubbling of onchange to the form, so need individual listeners - which is fine until new elements are added to the form - in which case will need a function to update the listeners
	if(!Prototype.Browser.IE) {
		 Event.observe(form, 'change',formOnChangeEvent.bind(form));
	} else {
		$(form).getElements().each( function (obj) {
									Event.observe(obj, 'change',elementOnChangeEvent.bind(obj));
								})
	}

	refreshRequiredClassOnForm (form)

	checkForShowOtherFields (form)
	
	checkForMaskAttributeOnElementArray ($(form).getElements());

	loadScript('/javascript/prototypeSpinner.js');
}


function extendRelayValidationOptions (options) {

	options =  options || { }
	var result = {
			showValidationErrorsInline:true,					// or as a popup
			applyValidationErrorClass:true,					// applies a red border around invalid fields
			validationErrorClass:'validationError',
			validationErrorMessageClass:'validationErrorMessage',
			labelSeparator:': ',								// separator between label and message in popup
			autoRefreshRequiredClass:false,						// used if one field might affect the required status of another field.  Any change in the form causes all fields to be check for requiredFunction
			autoValidate:false,									// validation of field is done when they change
			bindValidationWithListener:true						// this is set to false in situation when you want to fire the submit and/on submit events in javascript - we haven't found a way of firing the onSubmit listener version correctly in all browsers (FF actually submits the form, Chrome and IE do not)
		}

	for (var property in options) {
    	result[property] = options[property]
	}

	return result

	}


/* This is the function used as an onsubmit listener */
function relayFormOnSubmitEvent(e,options) {
	submitResult = relayFormValidation(Event.findElement(e,'FORM'),options)
	if (!submitResult) e.stop()
	return submitResult
}

/* This is the function used as an inline onSubmit function */
function relayFormOnSubmit() {
	return relayFormValidation (this)
}

function relayFormValidation (form) {

	return doRelayValidationOnArrayOfElements (form,$(form).getElements(),form.FormValidationOptions)
}


function doRelayValidationOnArrayOfElements	(parentObject,elementArray,options)  {

	if (options.applyValidationErrorClass) {
		removeErrorClassFromElementArray (elementArray,options)
	}

	/* remove old validation errors */
	if (options.showValidationErrorsInline) {
		removeErrorMessagesFromElementArray (elementArray);
	}	

	// get Error Array
	var errorArray = getRelayValidationErrorArrayForArrayOfElements (elementArray)

	if (errorArray.length) {
		// If Error Array has any items any message then display it and return false so don't submit
		// Also add Error Class
		if (options.applyValidationErrorClass || options.showValidationErrorsInline) {
			addClassAndMessageToErrorItems (errorArray,options)
		}

		if (!options.showValidationErrorsInline) {
			alert (convertErrorArrayToJavaScriptAlertString(errorArray,options))
		}
		// set focus to first item
		// problem if item is hidden, in which case scroll parentNode into view
		if (errorArray[0].fieldObject.type != 'hidden') { 
 			errorArray[0].fieldObject.focus()
		} else {
			errorArray[0].fieldObject.parentNode.scrollIntoView()
		}

		return false
	} else {
		return true
	}

}


function doRelayValidationOnForm (form) {
	return getRelayValidationErrorArrayForArrayOfElements (form.getElements())
}

/*
doRelayValidationOnCFForm
This function allows us to link this form of validation into a CFFORM validation
This is done by adding a hidden field into your CFFORM as follows
<cfinput type="Text" name="dummy" value="" onvalidate="doRelayValidationOnCFForm" style="display:none">
Minor problem is that all messages from this validation will appear in a single block
*/
function doRelayValidationOnCFForm (form) {
	var options = {labelSeparator:' '}

	var errorArray = getRelayValidationErrorArrayForArrayOfElements (form.getElements())
	addErrorArrayToCFFormErrorMessages (errorArray,form,options)

	return true
}


/*
convertErrorArrayToJavaScriptAlertString
Takes the errorArray and produces a string which can be alerted
Theory is that if you wanted the alert to be displayed differently (maybe in an HTML Popup) it would be possible to plumb in other functions
*/
function convertErrorArrayToJavaScriptAlertString (errorArray,options) {

	var result = ''
	for (var i=0; i<errorArray.length; i++) {
		result += errorArray[i].label + ((errorArray[i].label !='')?options.labelSeparator:'') + errorArray[i].message + '\n'
	}

	return result

}

/*
Adds a particular class to all items in the errorArray
(Class is applied to the parent tag - which will often be a TD)
May need more thought!
*/
function addClassAndMessageToErrorItems (errorArray,options) {

	for (var i=0; i<errorArray.length; i++) {

		if (options.applyValidationErrorClass) {
			// for most fields we add some red around them, for hidden fields we go up to the surrounding element and apply the class
			// If there a parent div with class validation-group then we use that
			var validation_group = errorArray[i].fieldObject.up('div.validation-group');
			var objectToAddClassTo = (validation_group)?validation_group:(errorArray[i].fieldObject.type == 'hidden')?errorArray[i].fieldObject.parentNode:errorArray[i].fieldObject;
			$(objectToAddClassTo).addClassName (options.validationErrorClass)
			errorArray[i].fieldObject['data-errorClassAppliedToPointer'] = objectToAddClassTo			
		}

		if (options.showValidationErrorsInline) {
			errorMsgDiv = document.createElement('div');
			errorMsgDiv.setAttribute ('class',options.validationErrorMessageClass);  // add inlineError as well so that we can search on it later to clear, don't necessarily want to clear other people's warning blocks
			errorMsgDiv.innerHTML = errorArray[i].message ;

			// is there a parent div with class validation-group?  If so then we use that, otherwise we use the object and 
			validation_group = (x = errorArray[i].fieldObject.up('div.validation-group'))?x:errorArray[i].fieldObject;
			validation_group.insert ({after : errorMsgDiv});

			errorArray[i].fieldObject['data-errorMsgDivPointer'] = errorMsgDiv			
	
		}
	}

}

/*
And removes aforementioned class - has to be on whole form
*/
function removeErrorClassFromElementArray (elementArray,options) {

	elementArray.each(function(item) { 
		if (item['data-errorClassAppliedToPointer']) {
			item['data-errorClassAppliedToPointer'].removeClassName (options.validationErrorClass);
		}
	
	});

	/*
	for (var i=0; i<elementArray.length; i++) {

		errorArray[i].fieldObject['data-errorClassAppliedToPointer'] = objectToAddClassTo			
		$(elementArray[i]).removeClassName (options.validationErrorClass)

		// for some items, such as checkboxes, the errorClass is applied to a parent, so search for and remove that
		if ((parent = $(elementArray[i]).up('.'+options.validationErrorClass))) {
			parent.removeClassName (options.validationErrorClass)
		}

	}
	*/

	return

}

/*
 Remove error messages from array of elements
*/
function removeErrorMessagesFromElementArray (elementArray) { 

	elementArray.each (function(item) { 
		var	pointerToErrorMessageDiv = item['data-errorMsgDivPointer']
		if (pointerToErrorMessageDiv) {
			$(pointerToErrorMessageDiv).remove();
			item['data-errorMsgDivPointer'] = null;	
		} 
	})

}


/*
Takes the errorArray and puts all the messages into the coldfusion error array
*/
function addErrorArrayToCFFormErrorMessages (errorArray,form,options) {

	errorArray.each(function(item) {
		_CF_error_exists = true
		_CF_onError(form, item.fieldObject.name, item.fieldObject.value, item.label + ((item.label !='')?options.labelSeparator:'') + item.message);

	})

}

/*
Adds item to error array and returns the array
*/
function addItemToErrorArray (errorArray, fieldObject,message,label) {
	// message should be defined by this point, but just incase, and to prevent an error I will check
	if (message == undefined) {message = 'Invalid'}
	// strip off anny trailing /n 'cause we will add them later
	errorArray [errorArray.length] = {fieldObject:fieldObject, message: message.replace(/\n$/,''), label: label.replace(/\n$/,'') }
	return errorArray
}


/*
The core function!
Loops through the array of elements looking for
	fields with required or requiredFunction attributes
	fields with a 'SubmitFunction' attribute
	fields with validate or onvalidate attributes (for CFFORM compatibility)
	fields with Range attribute

Errors are popped into an array with a Label and a Message
This array can either be used for a popup or for inline messages


 
WAB 2016-03-04 BF-522 Altered so that only one validation message can appear per item - do a 'continue' when error found
 				Also changed order slightly so that Required test happens earlier - but it is still after the submitFunction test

*/

function getRelayValidationErrorArrayForArrayOfElements (elementArray) {

		var msg = ''
		var errorArray = new Array()
		var form = $(elementArray[0].form) // allows this variable to be accessible to verification strings
		// loop through all elements looking for verification functions
		for (var j=0; j<elementArray.length; j++){
			var thisField = elementArray[j];
			var thisMsg = '';
			var objectToVerify = thisField

			// NJH 2013/04/23 - only validate objects that do not have the novalidate attribute or where it is set to false - used when wanting to ignore certain fields in a form...
			// WAB 2015-06-29 implemented isNoValidate() function which does same as above, but also looks for parent containers with the noValidate attribute
			if (!isNoValidate (objectToVerify)) {

				// run the submitfunctions before doing the required stuff, because some submit functions will update/select things

				// Can define a submitfunction as an attribute.  Return a string if necessary.
				// Can reference thisField and form
				// WAB 2013-02-20 can now reference 'this' instead of thisField in your functions - have atlast discovered how this is done!
				// Note that if there is a submitfunction then the _required is not dealt with
				var regexpStringDefiningAFunction = /return/gi
				var regexpFunctionNoBrackets = /^[0-9A-Za-z_]*?$/gi

				if ( thisField.getAttribute('submitfunction') != null ) {
					theFunctionString =  thisField.getAttribute('submitfunction')
					// either this is some code which defines a function - it will have the word 'return' in it
					// or it is the name of a function - will just have numbers and letters and _ in it
					// or it is just some javascript
						thisMsg = ''
					if ( theFunctionString == '') {
					}
					else if (theFunctionString.match(regexpStringDefiningAFunction)) {
						// the string defines a function
						thisField.submitFn =new Function('thisField',theFunctionString);
						thisMsg = thisField.submitFn (thisField)
					} else if (theFunctionString.match(regexpFunctionNoBrackets))  {
						// this string is the name of a function, we pass in a single parameter which is the field object
						thisField.submitFn = eval(theFunctionString)
						thisMsg = thisField.submitFn(thisField)
					} else if (theFunctionString != ''){
						// the string isn't a function, just some javascript which ends up 'returning' as string (hopefully)
						thisMsg = eval(theFunctionString)
					}

					if (thisMsg != undefined && thisMsg != '') {
						errorArray = addItemToErrorArray (errorArray,thisField,thisMsg,getLabelForField(thisField))
						continue;
					}

				}


				// Check Required
				if ((checkIsRequired =  isRequired_(thisField)).required) {
					/* if the name of the field is of form _required then it is probably for a radio or checkbox group, look for that group */
					var regEx = /_(required|checkbox|radio)$/i
					if (regEx.test (thisField.name)) {
						var relatedFieldName = thisField.name.replace(regEx,'','i')
						var objectToVerify = form.getElementsBySelector('[name = "' + relatedFieldName + '"]')
					}
					
					/*
					if ((thisField.type == 'radio'||thisField.type == 'checkbox')) {
						// for radio and checkbox, need to get pointer to the whole group
						radioCheckBoxGroup = form.getElementsBySelector('[name = "'+thisField.name+'"]')
						objectToVerify = (radioCheckBoxGroup.length >1) ?radioCheckBoxGroup:thisField
					}
					*/ 
					numberItemsSelected = checkObject (objectToVerify)

					// WAB 2016-02-02 Added support for select boxes which have numeric null values as defined by data-nullvalue attribute
					// If data-nullValue exists then could actually ignore the other test, but haven't
					if (checkIsRequired.required > numberItemsSelected || (thisField.getAttribute('data-nullvalue') &&  thisField.getAttribute('data-nullvalue') == $F(thisField) )) {
						label = getLabelForField(thisField)
							if (checkIsRequired.required>1) {
								msg =  checkIsRequired.required+ ' ' + phr.Required;		//PPB MF001 if more than 1 option is Required then I have kept the "fieldname : 2 required" format
							} else if (checkIsRequired.message){
								msg = checkIsRequired.message
							} else {
								msg = phr.Required;
							}
						errorArray = addItemToErrorArray (errorArray,thisField,msg,label)
						continue;
					}
				}


				// to be compatible with CF validation we look for a onvalidate parameter.
				// this is a function which returns true or false.  If false then the message defined in the message attribute is displayed
				if ( thisField.getAttribute('onvalidate') != null ) {
					var theFunctionString =  thisField.getAttribute('onvalidate')
					if (theFunctionString != 'doRelayValidationOnCFForm')  { // ignore this function, this is only used to connect cfform to relay validation
						try {
						    theFunction = eval(theFunctionString);
							theFunctionResult = theFunction (thisField.form,thisField,thisField.value);
						}
						catch(err) {
						    console.log ('Error: Function ' + theFunctionString + ' could not be evaluated');
						    theFunctionResult = true;
						}
						
						// alert (theFunction)
						if (isNaN(theFunctionResult) || !theFunctionResult ) {
							// check whether the result is boolean or a string
							if (isNaN(theFunctionResult)) {
								thisMsg = theFunctionResult
							} else {
								thisMsg = thisField.getAttribute('message')
							}
							errorArray = addItemToErrorArray (errorArray,thisField,thisMsg ,getLabelForField(thisField))
							continue;
						}
					}

				}


				// to emulate coldfusion's own built in validations	which run off the validate Attribute
				// these functions can deal with the required attribute as well, but since we are doing the required elsewhere, we will pass in false
				if ( thisField.getAttribute('validate') != null && thisField.getAttribute('validate') != ''  ) {
					theFunctionName =  thisField.getAttribute('validate')
					theFunctionResult = runColdFusionValidation (theFunctionName,thisField.value,false,thisField )
					if (!theFunctionResult ) {
						// ColdFusion would expect a message, but really not necessary.  So if there is not a message we will make one up
						// TODO translate and maybe have phrases for each validation type.  
						var msg = thisField.getAttribute('message');
						if (msg == undefined) {msg = 'Must Be ' + theFunctionName}
						errorArray = addItemToErrorArray (errorArray,thisField,msg,getLabelForField(thisField))
						continue;
					}

				}


				if ( thisField.getAttribute('range') != null && thisField.getAttribute('range') != ''  ) {
					minMax = thisField.getAttribute('range').split(',')
					min = (minMax[0]=='')?null:minMax[0]
					max = (minMax[1]=='')?null:minMax[1]
					theFunctionResult = _CF_numberrange (thisField.value,parseFloat(min),parseFloat(max),false )
					if (!theFunctionResult ) {
						if (min != null && max != null) {
							msg = phr.valueMustBeBetweenRange;
						} else if (min == null) {
							msg = phr.valueMustBeLessThanEqualTo;
						} else {
							msg = phr.valueMustBegGreaterThanEqualTo;
						}
						/* WAB 2013-09-18 altered to search for merge fields with [[ ]] around them - simultaneous change in includeJavascriptOnce */
						msg = msg.replace(/\[\[MIN\]\]/ig,min);
						msg = msg.replace(/\[\[MAX\]\]/ig,max);
						errorArray = addItemToErrorArray (errorArray,thisField,msg,getLabelForField(thisField))
						continue;
					}
				}
			}
		}

	return errorArray
}


/* WAB 2013-03-01
	Function which will do all the validation on a single element and update styles and inline messages accordingly
*/
function doValidationOnSingleElement (obj) {

	var options =  obj.form.FormValidationOptions

	var objectArray = [obj]
	/* add in the _required hidden field as well - it might be carrying the validation information */
	if((hiddenObject = obj.form[obj.name + '_required'])) {
		objectArray.push(hiddenObject)
	}

	/* check for errors */
	var errorArray = getRelayValidationErrorArrayForArrayOfElements (objectArray)
	if (options.applyValidationErrorClass) {
		removeErrorClassFromElementArray (objectArray,options)
	}

	/* remove old validation errors */
	if (options.showValidationErrorsInline) {
		removeErrorMessagesFromElementArray (objectArray);
	}	

	if (errorArray.length) {
		// Also add Error Class
		if (options.applyValidationErrorClass  || options.showValidationErrorsInline) {
			addClassAndMessageToErrorItems (errorArray,options)
		}

	}

	return true


}



/* checks both the required attribute and the requiredFunction to decide whether an element is required
	isRequired() just returns a number (0 = false, >0 true).  > 1 signifies number of characters required
	isRequired_() returns a structure with .required and optionally a message
*/
function isRequired (object) {
	return isRequired_(object).required
}


function isRequired_ (object) {
	var required = '0'

	// first see if there is a required function.  If there is a required function it will return .required= 0/1 and optionally a message
	// note that this function will also set/unset the required Attribute if necessary to keep it in synch with the result of the function (so for example HTML5 can do its stuff)
	// NJH 2016/01/22 - assume that required attribute on it's own means that the input is required.
	var result = checkForAndRunRequiredFunction(object)

	if (!result.required) {

		// if there is a required attribute
		var requiredAttr = object.getAttribute('required')
		if (requiredAttr == 'required') {   // WAB 2013-06-11 CASE 435691 IE8 seems to return the value as "required" even if the HTML has "true"
			result.required = 1
		} else if ( requiredAttr != null  && requiredAttr != '' && requiredAttr != 'false' && requiredAttr != 'no') {
			result.required = convertTrueFalseToNumeric (requiredAttr ) //function expects a number, not the word true
		} else if (requiredAttr == '') {
			result.required = 1;
		}	else {
			result.required = 0
		}
	}

	if (result.required && !result.message) {
		attributeRequiredMessage = object.getAttribute('requiredMessage');
		if (attributeRequiredMessage != null & attributeRequiredMessage!='') {
			result.message= attributeRequiredMessage;
		}
	}

	return result
}

/* runs a required function if element has one, returns a hash containing .required and possibly including .message  */
function checkForAndRunRequiredFunction (object) {
	var result = {}

			if ( ( requiredFunction = object.getAttribute('requiredFunction'))) {
				// run the function
				object.requiredFn =new Function('thisField',requiredFunction);
				try {
					required = object.requiredFn (object)
				} 
				catch(err) { 
					console.log ('Unable to run function', requiredFunction, err)
					result.required = 0
					return result
				}


				// if what is returned is not a number then it assumed to be a message
				if (isNaN(required))  {
					result.message = required
					result.required = 1
				} else {
					result.required = (required)?1:0
				}

				// Check whether the required attribute is in synch with the value returned from the requiredFunction.
				// If not then run setRequired which will add/remove the required Attribute and add/remove the label style
				// Don't do on hidden fields (which do tricky things!)
				if ((((requiredAttr = object.getAttribute('required')) == null && result.required ) || requiredAttr &&  !result.required ) && object.type != 'hidden') {
					setRequired(object,result.required)
				}

			}
	return result
}


// Case 433915 NJH  - deal with setting a field required or not.. set the label, create/remove the hidden field
// TBD - may need to just rename the _required hidden field, because sometimes they have requiredFunction which we want to keep
// 2016-06-10	WAB BF-975 / CASE 449815. Protect setRequired function from being passed a non-existent object (even if realy job of the caller)

function setRequired(object,required) {

	object = $(object)
	
	if (!object) {
		return
	}
	
	var requiredObjName = object.name+'_required';
	var notRequiredObjName = requiredObjName+'_'; 	// this field is used when novalidation is turned on. We change the name of any required hidden fields so that they don't get picked up in server validation.
													// if novalidation is turned off, then those fields are set to required again, and we change the name of the hidden field back to _required.

	if (required) {
		object.setAttribute('required',required);

		if (! object.form[requiredObjName]) {
			if (object.form[notRequiredObjName]) {
				object.form[notRequiredObjName].name=requiredObjName;
			} else {
				// Add a hidden _required field for server side validation.
				object.insert({after:'<input type="hidden" name="'+requiredObjName+'" value="' + getLabelForField (object) +' ' + phr.Required+ '"/>'});
			}
		}
	} else {
		//remove the required Attribute
		object.removeAttribute('required')

		// if there is a _required hidden form element then remove it
		if (object.form[requiredObjName]) {
			object.form[requiredObjName].name = notRequiredObjName;
			//object.form[requiredObjName].remove();
			//object.form[requiredObjName] = null;  // Need to set to null, otherwise object appears to remain
		}
	}

	// Add/remove required class from the label
	applyRequiredClass (object,required)

}


/* sets/unsets the required class from label associated the form element */
function applyRequiredClass (object,required) {
	labelTag = getLabelTagForObject(object)
	if (labelTag) {
		if (required) {
			$(labelTag).addClassName('required');
		} else {
			$(labelTag).removeClassName('required');
		}
	}
}

/*
	runs applyRequiredClass on whole form
	could be used on page load, or as an onChangeEvent
*/
function formOnChangeEvent(event) {
	changedElem = (event.target)?event.target:event.srcElement;
	elementChange (changedElem)
	return true
}


function elementOnChangeEvent() {
	elementChange (this)
}

function elementChange(obj) {


	if (obj.type == 'checkbox'  || obj.type == 'radio'  ) {
		/* if this a checkbox or radio we look for a field named  fieldName_checkbox/radio and fire an on change event on that */
		fieldToLookFor = obj.name + '_' + obj.type
		if (obj.form[fieldToLookFor]) {
			fireEvent_($(obj.form[fieldToLookFor]),'change'	)		
		}
	}

	var form=obj.form
	if (form.FormValidationOptions.autoValidate) {
		doValidationOnSingleElement (obj)
	}

	doShowOtherFields (obj)

	if (form.FormValidationOptions.autoRefreshRequiredClass) {
		refreshRequiredClassOnForm(form);
	}

}


function refreshRequiredClassOnForm (form) {
		refreshRequiredClassOnElementArray (form.getElements())
}

/* runs applyRequiredClass on array of elements */
function refreshRequiredClassOnElementArray (elementArray) {
	elementArray.each(
		function (object) {
			var required = isRequired(object)
			applyRequiredClass (object,required)
		}
	)
}

function checkForShowOtherFields (form) {
	elementArray = form.getElements()
	elementArray.each(
		function (object) {doShowOtherFields (object, true)}
	)
}


function doShowOtherFields (object, initialise) {
	if (initialise == undefined) {initialise = false}

	/* 	in non - initialise mode we deal with checkboxes a little differently 
		 For a checkbox group we have a single hidden field which carries the validation information and it is this which will have the showother fields info
		 in initialise mode we will come across this as we traverse the DOM, processing an individual onChange we need to look for that object
		It is conceivable that this code needs to go somewhere else, so that required can be dealt with in a similar way
	 */

	if (!initialise && /(checkbox|radio)/i.test (object.type) ) {
		lookForOtherObject = $(object.name + '_' + object.type);
		if (lookForOtherObject) {object = lookForOtherObject}
	}

			var showOtherFields = object.getAttribute('data-showotherfields')
			if (showOtherFields != null) {
				if (showOtherFields.isJSON()) {

					var showOtherFieldsStructure =  showOtherFields.evalJSON()

					/* get array of values set, whether from radio,select or text box */
					var objectValues = [];
					var regEx = /_(checkbox|radio)$/i

					if (regEx.test (object.name)) {
						/* for a checkbox/radio we will be dealing with a hidden field named #fieldName#_checkbox 
							get the value from the actual radio/checkbox group
						*/
						var relatedFieldName = object.name.replace(regEx,'','i')
						objectValues = getRadioCheckBoxGroupValue (object.form[relatedFieldName], true)
					} else if (object.tagName.toLowerCase() == 'select' || object.tagName.toLowerCase() == 'select-multiple') {
						/* For a select box we are having to do a DIY loop, because we also want to look for the existence of a data-textid attribute */
						for (var i=0; i<object.length; i++)  {
							if (object.options[i].selected == true) {
								objectValues.push (object.options[i].value)
								if (object.options[i].getAttribute ('data-textid')) {
									objectValues.push (object.options[i].getAttribute ('data-textid'))
								}
							}
						}	
					} else {	
						var objectValues = $F(object);
					}
	
					/* 	loop through structure 
						If current value is equal to key in structure then show that/those items 
						If current value is not equal to key in structure then hide those items, unless it is in the list of items to show
					 */
					var itemVisibility = {};
					showOtherFieldsStructure = $H(showOtherFieldsStructure);
	
					/* 	objectValue may be any array (from multiselect) or just a string
						later code expects an array so convert string to an array containg the same value
						If multiSelect with no values when we have an empty array, but we need a single blank item to get code to work
					*/
					if (typeof objectValues !='object') {
						objectValues = [objectValues]
					} else if (objectValues == null || objectValues.length == 0) {
						objectValues = [''];
					}
					
					showOtherFieldsStructure.each (function(pair) {
						objectValues.forEach (function (value) {
							regExp = new RegExp ('^' + value + '$','i')
							show = 	(regExp.test(pair.key))
							thisItemsToChange = pair.value.toLowerCase().split (',')
							thisItemsToChange.each (function (item){
								if (show) {
									itemVisibility [item] = true;
								} else {
									/* if not already in the structure then add with value false */
									if (!itemVisibility [item]) {
										itemVisibility [item] = false;
									}
								}
								
							})
						})
					})
	
					$H(itemVisibility).each (function(pair) {
						var objToChange = $(pair.key)
						/* If objToChange is not found then it may be the name of an item on the form rather than an id */
						if (objToChange == null && object.form [pair.key]) {
							objToChange = object.form [pair.key][0];
						}
						if (objToChange == null) {
						 	console.log ('Unable to find object ' + pair.key);
						 	return;
						}

						setVisibilityForControlRow (objToChange, pair.value)
					})	

				} else {
					console.log ('showOtherFields is not JSON', showOtherFields)
				}			
			}

}


/*
WAB 2014-09-22
Function to initialise any fields which a mask attribute.  Allows backwards compatibility with CFINPUT's mask attribute without having to use cfform
*/

function checkForMaskAttributeOnElementArray (elementArray) {

	/* check for any elements with as mask attribute, and pop into an array */
	var elementsWithMaskAttribute = [];
	elementArray.each(
		function (obj) {
			if (obj.getAttribute('mask') != null && obj.getAttribute('mask') != '') {elementsWithMaskAttribute.push (obj)}
		})

	/*  if any items found then initialise the event observers
		notice that we don't have to pre-load masks.js, it is loaded as required using the callback functionality of loadScript
	*/
	if (elementsWithMaskAttribute.length != 0) {
		loadScript ('/javascript/cf/masks.js',
			function () {
				elementsWithMaskAttribute.each(function (obj) {
					mask_initialise(obj,obj.getAttribute('mask'))
				})
			}
			,true
		)
	}

}


function getLabelForField (fieldObj) {
	var result = ''
	if (fieldObj.getAttribute('label') != null) {
		result = fieldObj.getAttribute('label')
	} else {
		labelTag = getLabelTagForObject (fieldObj)

		if(labelTag) {
			result = labelTag.innerHTML
		} else {
			result = fieldObj.name
		}
	}
	return result

}

/* WAB 2012-05-22 Mods to this function, was giving problems (returning 0 as a string, which wasn't false) */
function convertTrueFalseToNumeric (thevalue){
	var result
	if (thevalue == null||thevalue == '') {
		result = 0
	} else if (thevalue.toLowerCase() == 'true' || thevalue.toLowerCase() == 'yes') {
		result = 1
	} else if (thevalue.toLowerCase() == 'false' || thevalue.toLowerCase() == 'no') {
		result = 0
	} else {
		result = parseInt(thevalue)
	}
	return result
}

function runColdFusionValidation (validate,value,required,object){

		switch (validate.toLowerCase()) {
				case 'zipcode': theFunction = _CF_checkzip;break
				case 'numeric': theFunction = _CF_checknumber;break
				case 'float': theFunction = _CF_checknumber;break
				case 'email': theFunction = _CF_checkEmail;break
				case 'boolean': theFunction = _CF_checkBoolean;break
				case 'usdate': theFunction = _CF_checkdate;break
				case 'url': theFunction = function (value) {var regExp = /^((?:http|https)\:\/\/)?((?:[\w]+\.){1,}[\w]{2,})($|[:?#\/\\])/i; if (value != ''){ return regExp.test(value)} else {return true}};break
				case 'regular_expression':
				case 'regex': regExp = new RegExp (object.getAttribute('pattern')); theFunction = function (value,required){return _CF_checkregex(value,regExp,required)}; break
				default:theFunction = eval ('_CF_check'  + validate.toLowerCase())   // deals with integer,date,time,eurodate,
		}

		if (theFunction) {
				return theFunction (value,required)
		}	else {
			return true
		}

}

function validateNumericList(object){
	var regExp1 = /^[0-9][0-9,]*[0-9]$|^[0-9]$/ // tests that end in a number and only has numbers and commas
	var regExp2 = /^[0-9]/  // needs to start and end with a number but js does not do positive look behind, so can't test in one
	result = ''
	if (object.value == '') {return result}
	if (!regExp1.test(object.value) ) {
//		result = object.getAttribute ('message')
//		if (result == '') {
			result  = 'Must be numeric, or a list of numbers'
//		}

	}

	return result
}

function getLabelTagForObject(object) {

	object = $(object)
	if (!object.id) {
		return null
	}
	selector = 'label[for="'+object.id+'"]'   /* WAB 2013-12-16 added quotes - threw error if object ID happened to have dots in it */
	labelTags = $$(selector)

	if (labelTags.length > 0) {
		return labelTags[0];
	}
	return null;
}

/*
      Serialises all the input elements which are children of an element
      Allows us to have input elements which aren't in forms (very useful if you want to avoid forms within forms)

*/
function getInputElements(obj) {
	var tagNames = ['INPUT','SELECT','TEXTAREA']
	var toReturn = [];
	var inputObj = '';

	for (j=0;j<tagNames.length;j++) {
		childElements = obj.getElementsByTagName(tagNames[j])
        for (i = 0;i<childElements.length; i++)  {
        	inputObj = childElements[i]
        	if (inputObj.name && !inputObj.disabled && (/select|textarea/i.test(inputObj.nodeName) || /text|hidden|password|checkbox|radio/i.test(inputObj.type))) {
				toReturn.push(inputObj)
			}
        }
	}
	return toReturn;
}

function serializeInputElements(inputElementArray) {
	var inputObj = '';
    var toReturn = [];

	for (i = 0;i<inputElementArray.length; i++)  {
		inputObj = inputElementArray[i];
		isCheckbox = /checkbox/i.test(inputObj.type)
	    if ((isCheckbox && inputObj.checked) || !isCheckbox) {
	    	var val = $F(inputObj);
			toReturn.push( encodeURIComponent(inputObj.name) + "=" + encodeURIComponent(val));
	      }
	}
	return toReturn.join("&");   //.replace(/%20/g, "+")
}

function setValidate(object,validate) {
	var object = $(object);

	object.setAttribute('novalidate',!validate);
	// if we want to validate an object, then we check if the object was/is meant to be required.. set the hidden field for server validation
	// console.log ('setvalidate',validate,object.name)
	if (validate && /_required_$/i.test(object.name)) {
		object.name = object.name.replace('_required_','_required');
	} else if (!validate && /_required$/i.test(object.name)) {
		object.name = object.name.replace('_required','_required_');
	}
}

function setValidateForInputElements(inputElementArray,validate) {
	var i=0;
	for (i = 0;i<inputElementArray.length; i++)  {
		setValidate(inputElementArray[i],validate);
	}
}


/*
	WAB 2015-06-29 isNoValidate ()
	Checks whether an object or any of its parents has a noValidate attribute (excluding the form where noValidate has another meaning)
	Basic idea was already being used in getRelayValidationErrorArrayForArrayOfElements() but I added parent functionality and put into its own function
	NJH 2016/01/22 - use convertTrueFalseToNumeric because when false was returned from object.getAttribute('novalidate'), it evaluated to true as it was not a boolean, but a string.
*/

function isNoValidate(object) {
	var result = false;

	if (convertTrueFalseToNumeric(object.getAttribute('novalidate')) || object.up ('[novalidate=true]:not(form)')) {
		result = true;
	}
	return result
}

/*
	WAB 2015-06-29
*/
function setVisibilityForControlRow (object,show) {
	/* 	show/hide the row(s) an object is in and set to row to be validated or not
		may be in a tr or in a div of class form-group (ie bootstrap)
	*/

	/* 	Look first for class form-group on current element or it's parents
	 	If not found then look for a TR
	 	If none of these found then hide the object itself
	*/
	// console.log ('Setting Visibility', object.name, show)
	var container = (object.match('.form-group'))? object : object.up ('.form-group');
	if (!container) {
		container = object.match ('tr')? object : object.up ('tr');
	}
	if (!container) {
		container = object
	}

	// If we find a container then show/hide and add/remove validation
	if (container) {
		if (show) {
			container.show()
			container.removeAttribute ('novalidate')
		} else {
			container.hide()
			container.setAttribute ('novalidate','true')
		}
	} else {
		console.log ('Set Visibility, container Not Found', object)
	}

	// look for an _required field
	var fieldname = object.name + '_required' + ((show)?'_':'')
	var requiredObj = $(fieldname);
	if (requiredObj) { 
		if (show) {
			requiredObj.name = requiredObj.name.replace(/_required_$/,'_required');
		} else  {
			requiredObj.name = requiredObj.name.replace(/_required$/,'_required_');
		}
	}	

	return container
}


/* 	WAB 2016-02-03 
	After many failed attempts to try an trigger form submits which fire all the correct events and work in all browsers
	I have settled on this function which just injects a submit button and clicks it!
 */
function triggerFormSubmit (form) {

	 var inputButton = jQuery (('<input type="submit"style="display:none">'));
	 jQuery (form).append(inputButton);
	 inputButton.trigger('click');
	 inputButton.remove();
}


/* 	returns an array of values selected from a group of radios or checkboxes 
	optionally looks for data-textid attribute as well and adds that to the array
	useful for logic which needs to use static textids but where field is actually a lookup value
	  	
*/

function getRadioCheckBoxGroupValue (elementArray, includeTextID) {
	if (includeTextID == undefined) {includeTextID = false}
	var length = elementArray.length
	var result = []
	for (var i = 0; i<length; i++) { 
		if (elementArray[i].checked) {
			result.push (elementArray[i].value)
			if (includeTextID && elementArray[i].getAttribute('data-textid')) {
				result.push (elementArray[i].getAttribute('data-textid'))
			}
		}
	}	
	
	if (result.length == 0) {
		return null
	} else {
		return result
	}
	

}

