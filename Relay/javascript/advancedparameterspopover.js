jQuery(document).ready(function() {
  // VARIABLES
  var $opener = jQuery('#advparamhelp');
  var $closer = jQuery('.popoverclosebutton');	
  var $popover = jQuery('.advparamtooltip');
  var tooltipbody = '<div class="advparamtooltip" id="popovercontainer"><button class="fa fa-times popoverclosebutton" type="button"></button><div class="row"><div class="col-xs-8">ADVANCED PARAMETER</div><div class="col-xs-4">OPTIONS</div></div><div class="row"><div class="col-xs-8">showinfooter</div><div class="col-xs-4">true/false</div></div><div class="row"><div class="col-xs-8">hiderightborder</div><div class="col-xs-4">true/false</div></div><div class="row"><div class="col-xs-8">hideleftborder</div><div class="col-xs-4">true/false</div></div><div class="row"><div class="col-xs-8">section</div><div class="col-xs-4">true/false</div></div><div class="row"><div class="col-xs-8">hideleftnav</div><div class="col-xs-4">true/false</div></div><div class="row"><div class="col-xs-8">hidefooterborder</div><div class="col-xs-4">true/false</div></div><div class="row"><div class="col-xs-8">hideheaderborder</div><div class="col-xs-4">true/false</div></div></div>';
  var isVisible = false;
  // POPOVER FUNCTION								
  $opener.popover({
    content : tooltipbody,
    html : true,
    trigger: 'manual'
  });
  // POPOVER CLICK
  $opener.click(function(e) {
    e.stopPropagation();
    if(isVisible){
      $opener.popover('hide');	
      isVisible = false;
    } else {
      jQuery(this).popover('show');
      isVisible = true;
    }																																																																			
  });
  // CLOSE BUTTON CLICK
  $closer.click(function(){
    e.stopPropagation();
    $opener.popover('hide');
    isVisible = false;	
  });
  // OUTSIDE CLICK
  jQuery(document).on('click', function(e) {
    
    if(jQuery(e.target).is(".popover-content, .popover-content *:not(.popoverclosebutton)")){
      return;
    } else {
      $opener.popover('hide');
      isVisible = false;
      $opener.css('display', 'inline-block');
    }									
  });
  // HELPER FUNCTIONS
  $opener.mouseout(function(){$opener.css('display', 'inline-block');});
  $opener.on('click',function(){$opener.css('display', 'inline-block');});
});