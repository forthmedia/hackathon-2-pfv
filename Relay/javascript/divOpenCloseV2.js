/* �Relayware. All Rights Reserved 2014 */
var level1iconArrowUp = new Image();
var level1iconArrowDown = new Image();
var level2iconArrowUp = new Image();
var level2iconArrowDown = new Image();

level1iconArrowUp.src = "../images/MISC/arrow_right.gif";
level1iconArrowDown.src = "../images/MISC/arrow_down.gif";
level2iconArrowUp.src = "../images/MISC/icon_mkTreePlus.gif";
level2iconArrowDown.src = "../images/MISC/icon_mkTreeMinus.gif";

toggleVisibility = function(divName,callerObject,level)
{
	var theDiv = document.getElementById(divName);
  	theDiv.style.display = ( theDiv.style.display == "none" ) ? "" : "none";
  	if ( callerObject )
  	{
		if (level == 1) {
	    	callerObject.src = ( theDiv.style.display == "none" ) ? level1iconArrowUp.src : level1iconArrowDown.src;
		} else {
	    	callerObject.src = ( theDiv.style.display == "none" ) ? level2iconArrowUp.src : level2iconArrowDown.src;
		}
  	}
  	return true;
}
