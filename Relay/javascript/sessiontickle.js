/* �Relayware. All Rights Reserved 2014 */
function doRequest() {
	// branch for native XMLHttpRequest object
	if (window.XMLHttpRequest) {
		var req = new XMLHttpRequest();
		//req.onreadystatechange = fileCalled;
		req.open("GET", "/webservices/sessiontickle.cfm", true);
		/*req.onreadystatechange = function () {
			if (req.readyState == 4) {
				if(req.status == 200) {
					alert(req.responseText);
					timeSession(18);
				}
			} else {
				alert("Error loading page\n");
			}
		}*/
		timeSession(timeoutMins);
		req.send(null);
	// branch for IE/Windows ActiveX version
	} else if (window.ActiveXObject) {
		var req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			//req.onreadystatechange = fileCalled;
			req.open("GET", "/webservices/sessiontickle.cfm", true);
			/*req.onreadystatechange = function () {
				if (req.readyState == 4) {
					if(req.status == 200) {
						alert(req.responseText);
						timeSession(18);
					}
				} else {
					alert("Error loading page\n");
				}
			}*/
			timeSession(timeoutMins);
			req.send();
		}
	}
}

// cal from onreadystatechange... still issues with this... requires further research.
function fileCalled() {
	alert("reset session");
	timeSession(timeoutMins);
}

function timeSession(mins) {
	timeoutMins = mins;
	var milliseconds = (timeoutMins*60)*1000;
	setTimeout(doRequest,milliseconds);
}