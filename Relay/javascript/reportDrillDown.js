/* �Relayware. All Rights Reserved 2014 */
// code for doing drilldowns from reports esp. crosstabs 

			function reportDrillDown (id,row,column,action) {
				if (action.indexOf("selection") != -1) {
					newWindow = window.open( '','ReportSelection_PopUp','width=400,height=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )
				} else if (action.indexOf("drilldown") != -1)  {
					//check for a div and iframe with name reportDrillDown_#id#_div (under development)
					if (document['reportDrillDown_'+id]) {
						target = 'reportDrillDown_'+id
						// this allows the target of the post to be an iframe within a hidden div - div must have id reportDrillDown_Div, iframe must be called reportDrillDown
						theDiv = document.getElementById('reportDrillDown_' + id + '_Div')
						if (theDiv ) {
							theDiv.style.display = 'block'
						}
						newWindow = window.open( '','reportDrillDown_'+id)
					
					} else {
					
						newWindow = window.open( '','reportDrillDown','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )
						
						
					}

				} else {
					alert ('Not Supported - ReportDrillDown')
					return false
				}


				newWindow.focus();

				form = document['reportDrillDownForm_'+id]
				form.target = newWindow.name
				form.frmReportID.value = id
				form.frmRowID.value = row 
				if (form.frmColumnID) {form.frmColumnID.value = column}
				form.frmDrillDownAction.value = action
				form.submit()
			
			}
			
			

