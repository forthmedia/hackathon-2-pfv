/* �Relayware. All Rights Reserved 2014 */
/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

/*
2013-05-15 	WAB CASE 435127 
			Rationalised all the config files for different content types into a single file
			Picks up the relayContext from a relayContext attribute on the textarea, and uses to switch a few settings

*/


CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

// look for relayContextAttribute on the source element, use this for any context based swithes
	config.relayContext = this.element.$.getAttribute('relayContext').toLowerCase()

	if (config.relayContext == 'communication' || config.relayContext == 'emaildef') {
		config.fullPage = true;
		config.autoParagraph = true;
		relayToolBarItems =  ['InsertRelayMerge']
	} else if (config.relayContext == 'event' ) {
		config.fullPage = false;
		config.autoParagraph = false;
		relayToolBarItems =  []
	} else {  // ie elements
		config.fullPage = false;
		config.autoParagraph = false;
		relayToolBarItems = ['InsertRelayTag','InsertRelayMerge']
	}


/*
       Not sure which we want on this, so leaving as default for time being
       config.enterMode = CKEDITOR.ENTER_BR;
       config.shiftEnterMode = CKEDITOR.ENTER_P;
*/

	/* note  - there is unknown compatibility with imageresponsive*/
	config.extraPlugins = 'ajax,autosave,bidi,colorbutton,colordialog,dialogadvtab,div,find,flash,font,iframe,image,inlinecancel,indentblock,indentlist,indent,imageresponsive,justify,notification,pagebreak,panelbutton,relaytagsandmergefields,pastefromword,pastetext,selectall,stylesheetparser,table,widget';
	config.stylesSet = [];	
	config.contentsCss = '';
	
	/* now dynamically get stylesheets */
	if ( typeof relayContentsCss !== 'undefined' && relayContentsCss.constructor === Array) {
		config.contentsCss = relayContentsCss;
	}

	config.ProtectedSource =  /(<relay_[^>]+>)/gi 	; //keep any tag starting with <relay from being ended with an end tag.
	config.toolbar = 'MyToolbar';
	config.skin = 'bootstrapck';
	
	config.toolbar_MyToolbar =
	[
		['Source'], 
		['Inlinecancel', 'Maximize'],
		['Cut','Copy','Paste','PasteText','PasteFromWord'],
		['Undo','Redo','-','RemoveFormat'],
		['Link','Unlink','Anchor'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
		['Format','Styles','Font','FontSize','TextColor','BGColor'],	    	
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe', 'CreateDiv' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		
		relayToolBarItems
	];	

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	// config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre;div';
	config.font_names = 
	    'Arial;' +
	    'Arial Narrow;' +
	    'Arial Rounded MT Bold;' +
	    'Book Antiqua;' +
	    'Bookman Old Style;' +
	    'Bradley Hand ITC TT;' +
	    'Century Gothic;' +
	    'Comic Sans MS;' +
	    'Courier New;' +
	    'Franklin Gothic Medium;' +
	    'Garamond;' +
	    'Georgia;' +
	    'Haettenschweller;' +
	    'Impact;' +
	    'LFT Etica;' +
	    'Lucida Bright;' +
	    'Lucida Calligraphy;' +
	    'Lucida Console;' +
	    'Lucida Sans;' +
	    'Lucida Sans Typewriter;' +
	    'Lucida Sans Unicode;' +
	    'Mistral;' +
	    'MS Mincho;' +
	    'Onyx;' +
	    'Papyrus;' +
	    'Perpetua;' +
	    'PERPETUA TITLING MT;' +
	    'Playbill;' +
	    'Rockwell;' +
	    'Rockwell Extra Bold;' +
	    'STENCIL;' +
	    'Tamoha;' +
	    'Times New Roman;' +
	    'Trebuchet MS;' +
	    'Tw Cen MT;' +
	    'Verdana;' +
	    'Wide Latin;';
	
	config.entities = false;  // prevents quotes and all other odd characters (symbols,latin, greek, additional) being converted into entities .  
	config.colorButton_enableMore = true;
	config.allowedContent = true
	config.extraAllowedContent = 'img[!src,width,height] style' ;  // 2013-05-15 WAB CASE	435127 allow width and height attributes on any img which has a src attribute
	config.inlinecancel = {onCancel: function(editor) {if(confirm("Cancel and reload page? (All progress since last save will be lost.)"))location.reload(true);}};
};