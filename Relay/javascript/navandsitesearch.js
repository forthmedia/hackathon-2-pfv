/* �Relayware. All Rights Reserved 2014 */
/*
2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
13/09/2014 	SB Remove search when clicked outside 
09/14 		SB Added the drop down for the left menu
04/12/2016  SB BF-137: Fixed issue with left-margin on close tab.
*/
function siteSearch() {		  			
	var thisForm = document.getElementById('siteSearchForm');				
	
	if (thisForm.searchTextString.value == "") {				
		alert("Type in name, organisation, email, website, personID or organisationID.");
	} 
	else {				
		jQuery.ajax({
			url: '/webservices/callWebService.cfc?method=callWebService&webservicename=siteSearch&methodname=search&returnformat=plain&r=' + Math.floor(Math.random()*100000) + '&searchTextString=' + encodeURIComponent(thisForm.searchTextString.value) + '&_cf_nodebug=true',
			success: function(data) {				
				if(data.indexOf("Login Required") >= 0) {					
					doExtLogin(window, function () {siteSearch()});
				}				
				showSiteSearchResults(data);						

				//2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
				if (typeof homeIframe != 'undefined') {
					homeIframe.loadRecentSearches(homeIframe.recentSearchResultsNumber);
				}
				if (typeof loadRecentSearches != 'undefined' && typeof recentSearchResultsNumber != 'undefined') {
					loadRecentSearches(recentSearchResultsNumber);
				}
			
			},			
			error: function(data) {						
				showSiteSearchResults(data);				
			}
		});
	}
	return false;		
}

function reloadRecentSearches(){			
	jQuery.ajax({			
			url: '/webservices/callWebService.cfc?_cf_nodebug=true&webservicename=entitynavigation&methodname=getRecentSearches&method=callwebservice&returnFormat=plain',
			success: function(data) {
				if(data.indexOf("Login Required") >= 0) {					
					doExtLogin(window, function () {reloadRecentSearches()});
				}	
				showSiteSearchResults(data);					
			},			
			error: function(data) {
				showSiteSearchResults(data);					
			}
	});			
	return false;
}

function showSiteSearchResults(data) {
	jQuery('#siteSearchResultsInner').html(data);	
	jQuery('#siteSearchResults').slideDown(500);	
}


/*13/09/2014 SB Remove search when clicked outside */
jQuery("html").click(function(){ 
	jQuery('#siteSearchResults').slideUp(500);
});



function loadRecentSearch(searchTextString) {			
	document.getElementById('siteSearchForm').searchTextString.value = searchTextString;			
	siteSearch();
	return false;		
}
	
function doSearch_3Pane(queryString,name){ 	
	manageClick(
		'searchArea3Pane',
		'Search Results',
		'\/data\/dataFrame.cfm',
		queryString,          // removed '?'+, now done by manageclick
		{iconClass:'search', reuseTab:false});	
	return false;				
}

function manageClick(tabName,tabTitle,urlToOpen,urlParams,tabOptions) {		 
	if (!urlParams) {
		var urlParams = "";
	}
	conjunction = (urlToOpen.indexOf('?') == -1)? '?':'&' ;	
	path = urlToOpen+conjunction+urlParams;
	
	//2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
	parent.Ext.addTab(tabName,tabTitle.substring(0,50),path,tabOptions);		
}

	/* WAB 2008/12/08 
	Mods to allow items not in the menus to be called using the openTabByName code
	I have hard coded some links to particular tabs for time being - would like to have a structure of these some where else
	 for time being have coded in the whole js function, could have a bit of a structure
	 WAB 2015-11-17 For P-RIS001  Add Accounts
	*/			
	var extraTabItems = new Array ()
	extraTabItems ['advancedSearch'] = "manageClick('advancedSearch','Advanced Search','/data/dataSearch.cfm','',{reuseTab:true,iconClass:'search'})"
	extraTabItems ['communication'] = "manageClick('communication','Communication' ,'communicate/commDetailsHome.cfm','',{iconClass:'communicate'})"
	extraTabItems ['accounts'] = "manageClick('accounts','Accounts' ,'data/dataFrame.cfm','',{iconClass:'accounts'})"


	/*
	WAB 2012-02-19 Modified this function to work with the new (non dtree) menu structure
	*/
	function openTabByName (menuItem,params) {

		re = /.cfm/   // if menuitem has .cfm in it then it must just be a URL which has been passed
		theFunction = null
		
		if (!re.test(menuItem)) {
			// get the left nav list
			navlist = jQuery('#leftNav')
			// look for the LI item we are looking for 
			listItem = navlist[0].select('[name='+menuItem.toLowerCase()+']')
			if (listItem.length) {
				// found the correct item, so grab the href which is inside it
				theFunction = listItem[0].select('a')[0].href
			}

			if (!theFunction) {
				/* WAB 2008/12/08 
				 not found in the menus, 
				 I have hard coded some links to particular tabs for time being - would like to have a structure of these some where else
				*/

				for (var thisItem in extraTabItems)	 {
					if (thisItem  == menuItem){
						theFunction = extraTabItems [thisItem ] 
						break
						}				
				}

			}
				
			if (theFunction) {				
			
					// urlParams is never going to be defined because it doesn't exist in the xml structure that the menus are built from
					// therefore we will substitute our own params which may have been passed in to the openTabByName function
					// this rather peculiar function does that for us!
					manageClickTemp = function (tabName,tabTitle,urlToOpen,urlParams,tabOptions) {
							manageClick(tabName,tabTitle,urlToOpen,params,tabOptions)
					}
					theFunction = theFunction.replace('manageClick','manageClickTemp')   // so that manageClickTemp function is run rather than manageClick
					eval (theFunction)
					return true
		
					
			} else {
				alert ('Not Found')
				return false	
			}

		
				

		} else {
			// for items which aren't on the menus maybe we can revert to using a url, bit nasty 'cause won't get nice tab name but just use the template name
			// actually we can add a tab name after a pipe

			url = menuItem.split('|')[0]
			templatePath = url.split('?')[0]
			templatePath = templatePath.replace(/\\/g,'/')  // replace back with forward slash
			if (menuItem.split('|').length > 1) { 
				tabName = menuItem.split('|')[1]
			} else {
				pathArray = templatePath.split('\/')  // assumes forward slash 
				tabName =  pathArray[pathArray.length-1].split('.')[0]  // gets last item of path
			}
			manageClick('',tabName,'\/'+ url,params)						
			return true

		}

	}

/*opens nav drop down child elements */
jQuery(document).ready(function($) {
	
	jQuery('#siteSearchResults').hide();
	
	var navMenu = jQuery('#leftNav, #leftNav > li > ul.subNav').sortable({
		helper:'clone', 
		start : function(){jQuery(this).on("mouseleave",function(){jQuery(this).trigger("mouseup")})},
		stop : function(){jQuery(this).off("mouseleave")},
		//containment: "window",
		update:function(event,ui) {
			var order = [];
			jQuery('#leftNav .subNav').children('li').each(function(idx, elm) {order.push(elm.id)})
			jQuery.ajax({
				type: "POST",
				url: "/WebServices/relayUiWS.cfc?method=saveLeftNavOrder",
				data: {leftNavOrder: order.join(",")},
				dataType: "html",
				cache: false
			}).fail(function() {
				jQuery("#navUpdated").html('<div class="error">Error saving menu changes.</div>');
			}).done(function() {jQuery("#navUpdated").html('')});
		}
	}).disableSelection();
});
/*end opens nav drop down child elements */

jQuery(document).delegate('li.x-tab-strip-closable', 'contextmenu',function(event){
	//var parentW = jQuery('#west-panel').width();
	//SB 04-12-2016 - BF-137: Fixed issue with left-margin on close tab.
	var parentPadding = 2;
	var x = jQuery(this).position().left;
	var y = x + parentPadding;
		
	var a = '<div id="rightClickMenu" style="margin-left:' + y + 'px"><ul><li><a onclick="closeTab('+ event.currentTarget.id +'); return false;">' + phr.sys_nav_close_tab + '</a></li><li><a onclick="closeOtherTabs(' + event.currentTarget.id + '); return false;">' + phr.sys_nav_close_other_tabs + '</a></li></ul></div>';
	jQuery('#rightClickMenu').remove();	
	jQuery(this).parents('div.x-tab-panel-header').after(a);
})

jQuery('body:not(li.x-tab-strip-closable)').live('click', function(){jQuery('#rightClickMenu').remove();})
jQuery('iframe').contents().find(document).live('click', function(){jQuery('#rightClickMenu', window.parent.document).remove();})

function closeOtherTabs(t) {	
	var thisTab = getTabName(t);
	Ext.centerTabs.items.each (
		function (item)  {				
			if(item.closable && item.id != thisTab){
				Ext.centerTabs.remove(item); //Ext.centerTabs.getItem(item)
			}				
		}
	)
}

function getTabName(t) {	
	var result = jQuery(t).closest('li.x-tab-strip-closable').attr('id').replace('relayTabPanel__','')
	return result
}	

function closeTab(t) {	
    Ext.centerTabs.remove(Ext.centerTabs.getItem(getTabName(t)));
}

jQuery(document).ready(function() {	
	jQuery( "#iconNav" ).sortable({
		helper : 'clone', 
		start : function(){jQuery(this).on("mouseleave",function(){jQuery(this).trigger("mouseup")})},	
		stop : function(){jQuery(this).off("mouseleave")},	
		//containment: "window",							
		update:function(event,ui) {
			var order = [];
			jQuery('#iconNav').find('li').each(function(idx, elm) {order.push(elm.id)})
			jQuery.ajax({
				type: "POST",
				url: "/WebServices/relayUiWS.cfc?method=saveLeftNavOrder&variablename=" + menuOrderVariable,
				data: {leftNavOrder: order.join(",")},
				dataType: "html",
				cache: false
			}).fail(function() {
				jQuery("#iconNavUpdated").html('<div class="error">Error saving menu changes.</div>');
			}).done(function() {jQuery("#iconNavUpdated").html('')});
		}
	}).disableSelection();	
});			

