/* �Relayware. All Rights Reserved 2014 */
// functions used by divOpenClose custom tag
// WAB June 2005
//
//  mods
// 27/09/05  WAB added function to deal with toggling multiple divs
// 15/02/2006 modified to work properly with firefox <TR>s

	var divImages = new Array() ; 

	
function divOpenClose(objName,imgElemName,imgSet,pageID,txtAnchorID,txtToOpen,txtToClose,forceOpenClose) {

		// WAB 15/03/06 replaced getElementById with getElementsByName to make it easier to turn on and off elements with the same name at the same time
		// but falls back to getElementById if no elements with the name given
		var objs = document.getElementsByName(objName);
		if (objs.length == 0) {
			objs = new Array()
			objs[0] = document.getElementById(objName);
		}
		var imgElem = document.getElementById(imgElemName);
		var txtAnch = document.getElementById(txtAnchorID);
		var isNowOpen  = false

for (var i = 0; i < objs.length; i++) {
		obj= objs[i];
		if ((forceOpenClose =='close' || obj.style.display == "block" || obj.style.display == "" || obj.style.display == "undefined") && forceOpenClose !='open') {
			if(imgElem)	imgElem.src = divImages[imgSet].imgDown.src ;
			if (txtAnch) txtAnch.innerHTML = txtToOpen;
			obj.style.display = "none";
			divSetCookie('Div_' +pageID +'_' +objName,0);
			isNowOpen = false		;	
			
		} else {
			if(imgElem)	imgElem.src = divImages[imgSet].imgUp.src ;
			if (txtAnch) txtAnch.innerHTML = txtToClose;
			obj.style.display = "";   // used to set this to block, but this causes problems in firefox with tables
			divSetCookie('Div_' +pageID +'_' +objName,1);
			isNowOpen = true			;
		}

}
		//alert(document.cookie);
		return  isNowOpen ;
		
	}
	
	function divSetCookie(itemName,value) {
		document.cookie = itemName+"="+value;
	}
	
	
function multipleDivOpenClose (objNames,imgElemName,imgSet,pageID,txtAnchorID,txtToOpen,txtToClose,forceOpenClose) {

	objArray = new Array();
	objArray = objNames.split(',');
	var isNowOpen =false

	for (i=0;i<objArray.length;i++) {

		objName = objArray[i];

		isNowOpen   = divOpenClose(objName,imgElemName,imgSet,pageID,txtAnchorID,txtToOpen,txtToClose,forceOpenClose) ;
	}

	return  isNowOpen    ;
	
}	




