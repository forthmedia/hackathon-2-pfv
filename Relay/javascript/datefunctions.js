/* �Relayware. All Rights Reserved 2014 */

// -----------------------------------------
// compareDates function
//
// DAM 06/06/2001
// -----------------------------------------

// This requires two dates in string format "dd/mm/yyyy" and a message string.
// It makes use of isDate to determine whether the date parameters are formatted
// correctly. If the dates are not corrctly formatted message nMsg is displayed.

// If the two dates are provided in chronological order the function returns true
// otherwise it returns false

// If the message string nMsg is provided as "No Message" this will suppress date 
// format error messages.

// If the message string is "" the default message "Please enter dates in the format dd/mm/yyyy"
// will be displayed


function compareDates(aDate, bDate, nMsg){

var aDateObject = isDate(aDate,"Date")
var bDateObject = isDate(bDate,"Date")

var aDateValue = aDateObject.valueOf()
var bDateValue = bDateObject.valueOf()

if (nMsg == ""){
	nMsg = "Please enter dates in the format dd/mm/yyyy";
}

if (nMsg != "No message"){
	// If the date format is invalid it will have a negative number
	if ((aDateValue != Math.abs(aDateValue))||(bDateValue != Math.abs(bDateValue))){
		alert(nMsg)
		return false
	}
}

if (aDateValue < bDateValue){
	return true
}else{
	return false
}

}

// -----------------------------------------
// isDate function
//
// DAM 06/06/2001
// -----------------------------------------

// This requires a date string and a return type

// The function tests whether the string is a date in the format "dd/mm/yyyy"

// The return type can be either "Boolean" or "Date". "Date" is the default, so 
// only Boolean need ever be specified

// Return Type = Boolean 		: if the string is a correctly formatted date the function returns
// 								true, otherwise it returns false

// Return Type = Date (default) : if the string is a correctly formatted date the function returns
//								a date object set to that date with no hours, minutes, seconds or milliseconds
//								otherwise it returns a date object set to 31Dec 1900

function isDate(nDateIn,nReturnType){

	var nDate = new String(nDateIn)	
	var verify=1
	var x=0;
	var y=1;
	var nDay="00";
	var nMonth="00";
	var nYear="0000";

	if (nDate.substring(2,3)=="/"){
		nDay=nDate.substring(0,2);
	}

	if (nDate.substring(5,6)=="/"){
		nMonth =nDate.substring(3,5);
		nYear = nDate.substring(6,nDate.length);
		if (nYear.length != 4) {
			nYear = "0000"
			nMonth="00"
			nDay = "00"
		}
	}
	
	if (!checkDay(nYear, nMonth, nDay)) { //check day
		nYear = "0000"
		nMonth="00"
		nDay = "00"
	}

	if ((nMonth<1)||(nMonth>12)){
		nYear = "0000"
		nMonth="00"
		nDay = "00"
	}

//	if (((parseInt(nDay)<1)||(parseInt(nDay)>31))||((parseInt(nMonth)<1)||(parseInt(nMonth)>12))){
//		nYear = "0000"
//		nMonth="00"
//		nDay = "00"
//	}

	var nDateObject = new Date()
	
//	nDateObject.setDate(parseInt(nDay));
//	nDateObject.setMonth(parseInt(nMonth)-1);
//	nDateObject.setFullYear(parseInt(nYear));
	nDateObject.setDate(nDay);
	nDateObject.setMonth(nMonth-1);
	nDateObject.setFullYear(nYear);
	nDateObject.setHours(0)
	nDateObject.setMinutes(0)
	nDateObject.setSeconds(0)
	nDateObject.setMilliseconds(0)
	
	if (nReturnType=="Boolean"){
		var checkDateValue = nDateObject.valueOf()
//		if (nDateObject.valueOf==Math.abs(nDateObject.valueOf)){

		if (checkDateValue!=Math.abs(checkDateValue)){
			return false
		}else{
			return true
		}
		
	}else{
		return nDateObject
	}
	
}

function checkDay(checkYear, checkMonth, checkDay) {
	maxDay = 31;
	if (checkMonth == 4 || checkMonth == 6 ||
			checkMonth == 9 || checkMonth == 11)
		maxDay = 30;
	else
	if (checkMonth == 2) {
		if (checkYear % 4 > 0)
			maxDay =28;
		else
		if (checkYear % 100 == 0 && checkYear % 400 > 0)
			maxDay = 28;
		else
			maxDay = 29;
	}
	return numberRange(checkDay, 1, maxDay);
}

function numberRange(object_value, min_value, max_value) {
    if (min_value != null) {
        if (object_value < min_value)
		return false;
	}

    if (max_value != null) {
		if (object_value > max_value)
		return false;
	}
    return true;
}