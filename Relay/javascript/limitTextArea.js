/* �Relayware. All Rights Reserved 2014 */
// this is the beginnings of a set of functions that will limit the mount of chars
// a text area can use.  It needs to be made generic.
// NJH 2012/02/16 CASE 425074: Set CharRemainingText and maxCharsReachedText to be translations
//2014-06-20 REH Case 439403 setting charsLeftObj size based on the text it holds

// Usage:
//<FORM NAME="form" onsubmit="docheck();alert('This javascript will not send.  It is just a sample.')">
//	<TEXTAREA onclick="docheck(125,this)" onkeyup="javascript:doleft(125,this);" id="text" name="text" Rows="6" Cols="24"></TEXTAREA>
//	<BR><BR>You have <INPUT Type="Text" Name="charsleft" value="125" Size="3" readonly id="left"> characters left.<BR><BR>
//	<INPUT Type="submit" Name="" Value="Send!"> &nbsp 
//	<input type="reset" value="Clear"><BR><BR>
//</FORM>

doleft = function(maxChars,textAreaObj,charsLeftObj) {	
	
	maxCharsReachedText = phr.sys_MaximumCharactersReached; //'Maximum characters reached'
	CharRemainingText = phr.sys_CharactersRemaining; //'characters remaining'
	charsLeftObjSizePadding = 0;
	maxChars = parseInt(maxChars);

	currentChars = parseInt(textAreaObj.value.length);
	theCharsLeft = maxChars - currentChars;

	charsLeftObj = $(charsLeftObj);
	
	if (theCharsLeft < 0)
	{
		var dif = currentChars - maxChars;
		var value = textAreaObj.value.substr(0,currentChars-dif);
		textAreaObj.value = value;
		charsLeftObj.style.color="red";
		charsLeftObj.style.fontWeight="bold";
		charsLeftObj.innerHTML = maxCharsReachedText;
	}
	else if (charsLeftObj)
	{
		charsLeftObj.style.color="black";
		charsLeftObj.innerHTML = theCharsLeft +' '+ CharRemainingText
	}	 
}
