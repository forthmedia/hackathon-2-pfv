/* �Relayware. All Rights Reserved 2014 */
// code for doing the context sensitive menu in the element tree
// WAB 2007-10-17 added some more copy items
// YMA 2013-01-03 2013 Roadmap Content Deletion
// 2015-01-28 AHL Case 442825 Creating Child Pages shows Parent in designer
		
		function fnFireContext(oMenu,oClickedItem) {
		
		  // Customize this function based on your context menu
   		 var  theArray = new Array()
			  theArray = oClickedItem.id.split("-");
				id = theArray[1];
				pid = theArray[2];
				headline = jQuery("#"+oClickedItem.id).text();
		  switch (oMenu.getAttribute("menuid")) {
			 case "pagePreview":
			 	url = portalDomainAndRoot+'?seid='+id+'-pr';
				newWindow=window.open(url , 'Preview' , '' );
				newWindow.focus();
				break ; 			    
			case "pageEdit":
			  parent.editFrame.location.href = '/elements/elementEdit.cfm?RecordID=' + id;
			  break;
			case "pageHTML":
			  openNewTab('element_'+ id,oClickedItem.innerHTML,'/elements/elementEdit.cfm?RecordID='+id+ '&frmWizardPage=WYSIWYGEditor',{reuseTab:true,iconClass:'content'});
			  //parent.editFrame.location.href = '/elements/elementEdit.cfm?RecordID=' + id + '&frmWizardPage=WYSIWYGEditor';
			  break;
			case "pageAttributes":
			  openNewTab('element_'+ id,oClickedItem.innerHTML,'/elements/elementEdit.cfm?RecordID='+id+ '&frmWizardPage=attributes',{reuseTab:true,iconClass:'content'});
			  //parent.editFrame.location.href = '/elements/elementEdit.cfm?RecordID=' + id + '&frmWizardPage=attributes';	  
			  break;
			case "pageVisibility":
			  openNewTab('element_'+ id,oClickedItem.innerHTML,'/elements/elementEdit.cfm?RecordID='+id+ '&frmWizardPage=visibility',{reuseTab:true,iconClass:'content'});
			  //parent.editFrame.location.href = '/elements/elementEdit.cfm?RecordID=' + id + '&frmWizardPage=visibility';		  
			  break;
			case "pageAddChild":
			  openNewTab('element_'+ id,oClickedItem.innerHTML,'/elements/elementEdit.cfm?parentID='+id,{reuseTab:false,iconClass:'content'});	// 2015-01-28 AHL Case 442825 Creating Child Pages shows Parent in designer
			  //parent.editFrame.location.href = '/elements/elementEdit.cfm?parentID=' + id ; 	
			  break;		
			case "pageAddSibling":
			  openNewTab('element_'+ id,oClickedItem.innerHTML,'/elements/elementEdit.cfm?parentID='+id,{reuseTab:false,iconClass:'content'});	// 2015-01-28 AHL Case 442825 Creating Child Pages shows Parent in designer
			  //parent.editFrame.location.href = '/elements/elementEdit.cfm?parentID=' + pid ; 	
			  break;		
			 case "copyBranch":
			 	url = '/elements/elementBranchActions.cfm?action=cloneElementBranch&elementid='+id;
				newWindow=window.open(url , 'CopyBranch', 'height=300,width=600' );
				newWindow.focus();
				break ; 			    
			 case "copyItem":
			 	url = '/elements/elementBranchActions.cfm?action=cloneElementBranch&frmNumberOfGenerations=1&elementid='+id;
				newWindow=window.open(url , 'CopyBranch', 'height=300,width=600' );
				newWindow.focus();
				break ; 			    
			 case "copyChildren":
			 	url = '/elements/elementBranchActions.cfm?action=cloneElementBranch&frmIncludeTopElement=0&elementid='+id;
				newWindow=window.open(url , 'CopyBranch', 'height=300,width=600' );
				newWindow.focus();
				break ; 			    

				
			case "refreshTree":
			  d.getChildNodes(id) ; 	
			  break;		
			case "showOnTree":
				// WAB/NJH 2008/10/07 Bug Fix Trend Issue 142 - this was broken when prototype changed. Added 'Dummy' for the resultSearchObject
				searchByID (id,'dummy',false)
			  if (divtoggle_tree){divtoggle_tree('open')}  // must make sure that the div is open if it exists
			  break;		

				
			case "reloadTreeFromHere":
			  form = document.quickSearchForm ;
			  form.goTo.value = id ;
			  form.submit();
			  break;		

			case "pageRestore":
				var page = '/webservices/ElementTreeNav.cfm?method=pageRestore';
				var parameters = {id:id};
				
				var myAjax = new Ajax.Request(
			    	page, 
			        {
			        	method: 'get', 
			            parameters: parameters,
			            debug : false                       
			    });
			 	alert(phr.sys_pageRestored);
			 	loadArchive(function(result){document.getElementById('archiveSection').innerHTML = result;});
				break;

			case "pageDelete":
			 	r = confirm(phr.sys_deleteConfirmationPt1 + headline + phr.sys_deleteConfirmationPt2);
			 	if(r==true)
			 		{
					var page = '/webservices/ElementTreeNav.cfm?method=pageDelete';
					var parameters = {id:id};
					
					var myAjax = new Ajax.Request(
			            page, 
			            {
			                method: 'get', 
			                parameters: parameters,
			                debug : false                     
			            });
			 		alert(phr.sys_pageDeleted);
			 		loadArchive(function(result){document.getElementById('archiveSection').innerHTML = result;});
			 		}
			 	else
			 		{
			 		alert(phr.sys_pageNotDeleted);
			 		}
				break ; 
						  	
			case "sendToArchive":
				r = confirm(phr.sys_archiveConfirmationPt1 + headline + phr.sys_archiveConfirmationPt2);
			 	if(r==true)
			 		{
					var page = '/webservices/ElementTreeNav.cfm?method=sendToArchive';
					var parameters = {id:id};
					
					var myAjax = new Ajax.Request(
				    	page, 
				        {
				        	method: 'get', 
				            parameters: parameters,
				            debug : false                       
				    });

				 	document.getElementById(oClickedItem.id).parentNode.innerHTML = '';
				 	alert(phr.sys_sentToArchive);
				 	document.getElementById('d_Ajax').action = ""
					document.getElementById('d_Ajax').submit() 
			 		}
			 	else
			 		{
			 		alert(phr.sys_pageNotArchived);
			 		}
				break ;
		  }
		}
		
	function loadArchive(callback){	
		var result = "";
		var page = '/webservices/ElementTreeNav.cfm?method=loadArchive';
		myAjax = new Ajax.Request(
			    	page, 
			        {
			        	method: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				onComplete: function(transport){
            				if (200 == transport.status) {
                				result = transport.responseText;
								callback(result);
		  }
		}
			    	});
	}