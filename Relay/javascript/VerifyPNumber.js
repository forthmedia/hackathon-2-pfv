/* �Relayware. All Rights Reserved 2014 */
function verifyPNumber(thisObject)  {
	thisValue = thisObject.value;
	// regular expression for testing pNumber 
	//	start of string
	//	between 1 and 10 numeric characters
	//	a dash
	//	between 1 and 6 numeric characters
	//	end of line
	PNumberFormatRe = /^[0-9]{1,10}\-[0-9]{1,7}$/
	// test match with regular expression 
	return PNumberFormatRe.test(thisValue) 
}
