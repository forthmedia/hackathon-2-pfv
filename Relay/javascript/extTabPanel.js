/* �Relayware. All Rights Reserved 2014 */
Ext.onReady(function() {

	
		Ext.ux.extTabs_TabPanel = new Ext.TabPanel({
				id: 'tabPanel',
				border: false,
				renderTo: renderToDiv,
				stateful: true,
				width:tabPanelWidth,
				height:tabPanelHeight,
				defaults: {
					style: "padding: 5px"
				},
				enableTabScroll: true

			})
		

			
				Ext.ux.addTab = function(tabName,tabTitle,href,options){

					this.options = {
						tabTip: tabTitle,
						closable:true,
						useIFrame:true
					}
					
					Object.extend(this.options, options || { });   // this is a function in prototype.js  - effectively overwrite this.options with any items passed in


					var tabID = tabName;
	
						if (this.options.useIFrame) {
							var iframeID = 'tabPanelTabIFrame_' + tabName
							var html = '<iframe name="'+iframeID+'" id="'+iframeID+ '" src = "' + href + '"  '    + '" width="100%" height="100%" frameborder="0"></iframe>';
						} else {
							var html = href
						}
							
						var newTab = Ext.ux.extTabs_TabPanel.add({
							title: tabTitle,
							id: tabID,
							border: false,
							containerScroll:false,
							autoScroll:false,
							html: html,
							closable:options.closable,
							iframeID:iframeID
						}).show();
						
					return tabID;
				}

				
				Ext.ux.showTabByName = function (tabName) {
				
					Ext.ux.extTabs_TabPanel.getItem(tabName).show()
				}
				
				Ext.ux.refreshTab = function (tabName) {
					// may need some error handling here
					iframeID =  Ext.ux.extTabs_TabPanel.getItem(tabName).iframeID
					iframe = document.getElementById(iframeID)
					iframe.contentWindow.location.reload()
				}
				
				
			// check for existence of openStartingTabs function  (set in mainlayout.cfm) 
			if (typeof openStartingTabs != 'undefined') {
				openStartingTabs ()
			}
				
			
});
