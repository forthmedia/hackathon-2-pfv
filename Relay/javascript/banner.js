jQuery(document).ready(function(){

  /* -----------------------------------------------------
    A Sarabi 07/10/2016
    
    Hide Banner button if there is no text
  ----------------------------------------------------- */
  jQuery('.item').each(function(){
    if (!jQuery(this).find('a').text().trim().length){      
      jQuery(this).find('a').hide(); 
    }
  });
  
});