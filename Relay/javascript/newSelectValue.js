/* �Relayware. All Rights Reserved 2014 */
/*
This code is used to add a new value to a select box
Don't belive it is used anywhere in anger
WAB 2011/03/30 modified to use an input box rather than a JS prompt

*/
function newSelectValue (thisObject,thePrompt) {

		// add a sibling node
			var newNode = document.createElement('Input');
			newNode.className = 'form-control';
			newNode.setAttribute('onblur','newSelectValueOnBlur(this)');
		   	thisObject.parentNode.insertBefore(newNode,thisObject); 
		   	thisObject.hide()
		   	newNode.focus()
		

}

function newSelectValueOnBlur (inputObject) { 

	selectBoxObject = inputObject.nextSibling
	if (inputObject.value != '') {
			selectBoxObject.options[selectBoxObject.options.length] = new Option(inputObject.value,inputObject.value)	
			selectBoxObject.options[selectBoxObject.options.length-1].selected = true
	}
	selectBoxObject.show()
	inputObject.hide()

}
