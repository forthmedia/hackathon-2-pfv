/*  �Relayware. All Rights Reserved 2014 */

// output a message to the screen
function message (message, type, showClose, selector, cssClassToAdd) {
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayUIWS&methodName=message&returnFormat=plain',
        	data:{	message:message,
					type:type == undefined ? 'success' : type,
					showClose: showClose == undefined
					},
        	dataType:'html',
        	success: function(data, textStatus, jqXHR) {
				var elementToPrintAfter = jQuery(selector == undefined ? 'div.header' : selector);
				elementToPrintAfter.next("[id^='message_']").remove(); // move any existing message
				elementToPrintAfter.after(data);
				cssClassToAdd != undefined  ? elementToPrintAfter.next("[id^='message_']").addClass(cssClassToAdd) : undefined;
			}
		});
}

// output a message to the screen, but use the message stored in session. Can probably get rid of the message function or amalgamate the two.
function displayMessage (messageDivSelector) {
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayUIWS&methodName=displayMessage&returnFormat=plain',
        	dataType:'html',
        	success: function(data, textStatus, jqXHR) {
				var elementToPrintAfter = jQuery(messageDivSelector == undefined ? 'div.header' : messageDivSelector);
				elementToPrintAfter.next("[id^='message_']").remove(); // move any existing message
				elementToPrintAfter.after(data);
			}
		});
}
