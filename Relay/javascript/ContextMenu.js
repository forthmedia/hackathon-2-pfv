/* �Relayware. All Rights Reserved 2014 */
/* 	code for doing the context sensitive menus
 	Modified by WAB 8/11/05 - I took out the bits which were specific to the individual page and left the generic functions
	therefore to use this you need to have one more functions in your own bit of javascript
	fnFireContext(oMenu,oClickedItem)  see contextMenu_elementTreeNav.js for examples
	you also need your context menu defined in XML in your main page.  
	in the XML remember that the in line <contextmenu id="myContextMenu"> the ID has to match the contextmenu attribute of the Anchor tags
	<A HREF="" contextmenu = "myContextMenu" id = "xxxx" >xxxx</A>
	this code seems to have come from http://www.siteexperts.com/tips/styles/ts42/page3.asp

	sep/oct 2006 WAB  worked to get this code to work on firefox.  Managed to deal with the event model fairly easily, but more problems with the XML data island
	nov 2006 WAB sorted out final problems - non-context menu right click on FF not bringing up default context menu and enormous height of FF context menu 
	2007 ?   WAB added ability to switch items in context menu on and off by having an attribute contextmenushowitems in the href
	04/07/2011	NYB		P-LEN024 changed the way functions are named - so they don't break cflayout-tabs code
	2012/03/06	IH	Changed the way IE was detected at some places from window.event to (typeof object.releaseCapture == 'function') to fix Chrome right click menu. CASE 426672 
	2013-04-30	WAB CASE 434590 Added support for IE9 and IE10 (previously relying on setting document mode to IE8)
*/		
		// Define global script variables
		var bContextKey=false;
		var oClickedItem = '' ;
		var contextMenuDebug = false;
	
		// The fnDetermine function performs most of the work
		fnGetContextID = function(el) {
			if (contextMenuDebug) {console.log ('fnGetContextID entered')}

		//get the options for display from from embedded xml: contextmenu
		  while (el!=null) {
			elementContextmenu = el.getAttribute('contextmenu')
			if (elementContextmenu !='' && elementContextmenu != null ) {
				return elementContextmenu;
			}

			el = el.parentElement
		  }
		  return ""
		}
		
		fnDetermine = function(theEvent){			
			if (contextMenuDebug) {console.log ('fnDetermine entered')}
			theContextMenu = document.getElementById('oContextMenu')

				if (window.event) {					
					  oWorkItem=window.event.srcElement;
					  theEvent = window.event
			     } else {
       				  oWorkItem= theEvent.target;
			     }
		  // Proceed if the desired keyboard key is pressed.
		  if(bContextKey==true ){
		  // save details of the object which has been clicked
			oClickedItem = oWorkItem   // this is a global variable
			//theContextMenu = document.getElementById('oContextMenu')
			// If the menu STATUS is false, continue.

			if(theContextMenu.getAttribute("status")=="false"){
			  // Give the menu mouse capture so it can interact better with the page.
			//   theContextMenu.setCapture();   doesn't work with FF

			  // Relocate the menu to an offset from the mouse position.
			  xyObj = getScrollXY();
			  theContextMenu.style.top=theEvent.clientY + xyObj.y + 1+'px';
			  theContextMenu.style.left=theEvent.clientX + xyObj.x +  1+'px';
			  
			  theContextMenu.innerHTML="";
			  // Set its STATUS to true.
			  var sContext = fnGetContextID(oWorkItem)

				if (contextMenuDebug) {console.log ('sContext = ' + sContext)}

				if (sContext!="" && sContext!= null) {
					fnPopulate(sContext)

					// attempt to prevent the context menu falling off the bottom of the window.
					// still not perfect in IE.  I haven't got hang of all the different height properties!
					if (theEvent.clientY + theContextMenu.clientHeight > getWindowHeight()) {
						theContextMenu.style.top = getWindowHeight() - theContextMenu.clientHeight + document.body.scrollTop + 1+'px'
					}

					theContextMenu.setAttribute("status","true");
				if (window.event) {
					theEvent.returnValue=false;   //IE
			     } else {
					theEvent.preventDefault(true);  //FF
			     }

			  }
			  else
				// this extra releaseCapture stmt added to fix freezing issues
				{ 
				 if (typeof theContextMenu.releaseCapture == 'function') {theContextMenu.releaseCapture();theEvent.returnValue=true;}
				}
			}
		  }
		  else{
			// If the keyboard key was not pressed and the menu status is true, continue.
			if(theContextMenu.getAttribute("status")=="true"){

			  if((oWorkItem.parentNode.id=="oContextMenu") &&
				(oWorkItem.getAttribute("component")=="menuitem")){		
				
				// WAB added this bit so that you could have custom functions for different context menus
				// sure there must be a tidier way of doing this!
				theFunction = 'window.fnFireContext_' + oClickedItem.getAttribute('contextmenu')
				if (eval (theFunction)) {
					theFunction = theFunction + '(oWorkItem,oClickedItem)'
					eval (theFunction)
				}else {
					fnFireContext(oWorkItem,oClickedItem)
 				}
				
			  }
		

//			  // Reset the context menu, release mouse capture, and hide it.  
			  hideContextMenu (theEvent)
			// need to prevent the mouseover event firing after a contextmenu item has been clicked 
			// don't know how!! 
			// these items below just affect the current onclick event
			  theEvent.returnValue=false;
			  theEvent.cancelBubble=true;
			}
		  }
		}
			
		hideContextMenu = function(theEvent) {
		
			  // Reset the context menu, release mouse capture, and hide it.  
			  theContextMenu = document.getElementById('oContextMenu')
			  theContextMenu.style.display="none";
			  theContextMenu.setAttribute("status","false");
				if (typeof theContextMenu.releaseCapture == 'function') {
					// doesn't seem to work for firefox - oddly seems to work anyway!
					theContextMenu.releaseCapture();
				}	

			  theContextMenu.innerHTML="";
			  theEvent.returnValue=false;
		}
			
		fnPopulate = function(sID) {
		  var str=""
			if (contextMenuDebug) {console.log ('fnPopulate entered - getting context menu with id = ' + sID)}
			// altered by WAB so that can handle multiple contect menus
			  theContextMenu = document.getElementById('oContextMenu')

			elMenuRoot =  null

			// WAB 2013-04-30 major changes in this area to support IE9/10
			if(/MSIE ([0-9.]*)/ig.test(navigator.appVersion) ) {
				// IE stuff
				obj = document.all.contextDef
				if (obj.length) {
					// more than one context menu defined in the XML				
					// loop through all xml docs looking for the correct context menu
					for (i=0;i<obj.length;i++) {
						if (window.DOMParser) {
			                var parser = new DOMParser ();
               				xmlDoc = parser.parseFromString (obj[i].innerHTML, "text/xml");
							contextMenus = xmlDoc.getElementsByTagName('contextmenu')
							// loop through each context menu in the doc looking for the correct menu	
							for (var j=0; j<contextMenus.length; j++) {
								if (contextMenus[j].getAttribute('id') == sID) {
									elMenuRoot =  contextMenus[j]
									break
								}
							}

						} else {
							xmlDoc = obj[i].XMLDocument
							elMenuRoot = xmlDoc.childNodes(0).selectSingleNode('contextmenu[@id="' + sID + '"]')
						}	
						
			 			if (elMenuRoot) {
							break
						}
					} 
				} else {
					// just one bit of XML
						if (window.DOMParser) {
			                var parser = new DOMParser ();
							xmlDoc =  parser.parseFromString (obj.innerHTML, "text/xml"); 
							contextMenus = xmlDoc.getElementsByTagName('contextmenu')
							// loop through each context menu in the doc looking for the correct menu							
							for (var j=0; j<contextMenus.length; j++) {
								if (contextMenus[j].getAttribute('id') == sID) {
									elMenuRoot =  contextMenus[j]
									break
								}
							}

						} else {
							xmlDoc = obj.XMLDocument
							elMenuRoot = xmlDoc.childNodes(0).selectSingleNode('contextmenu[@id="' + sID + '"]')
						}	
				}
			} else {
			//  for firefox

			xmlDoc = document.getElementById('contextDef')
			elMenuRoot = document.getElementById(sID)
}
			
			if (contextMenuDebug) {console.log ('elMenuRoot = ' + elMenuRoot + '  length = ' + elMenuRoot.childNodes.length)}

		  if (elMenuRoot) {
		  	var contentMenuWidth=1;
			for(var i=0;i<elMenuRoot.childNodes.length;i++)
			{
				if (elMenuRoot.childNodes[i].nodeType != 3) 
				{
					showitem = true
				 		// wab trying a way of showing certain items (based on ktml context menu)
						if (elMenuRoot.childNodes[i].getAttribute("showif")) {
							showitem = eval(elMenuRoot.childNodes[i].getAttribute("showif"))
						}
						
						itemsToShow = oWorkItem.getAttribute('contextmenushowitems')
						if (itemsToShow != null) {
							itemsToShowArray = itemsToShow.split(',')
							showitem=false
							for (x=0;x<itemsToShowArray.length; x++) {
								if (itemsToShowArray[x] == elMenuRoot.childNodes[i].getAttribute("id")) {
									showitem=true
									break
								}
							
							}
							
						}
					if (showitem) {
				  str+='<span component="menuitem" menuid="' + 
						elMenuRoot.childNodes[i].getAttribute("id") + 
							'" id=oMenuItem' + i + 
							'>' +  
						elMenuRoot.childNodes[i].getAttribute("value") + 
						"</SPAN><BR>";
						if (elMenuRoot.childNodes[i].getAttribute("value").length > contentMenuWidth) {
							contentMenuWidth = elMenuRoot.childNodes[i].getAttribute("value").length;
						}
					}		
				}
			}	
			theContextMenu.innerHTML=str;
			theContextMenu.style.width=(contentMenuWidth*5)+30+'px';
			theContextMenu.style.display="block";
			// WAB removed 14/11/06, doesn't work in FF and browsers seem to get it right anyway theContextMenu.style.pixelHeight = theContextMenu.scrollHeight    
		  }
		}
		
		fnChirpOn = function(theEvent){
			if (window.event) {
				  oWorkItem=window.event.srcElement;
				  theEvent = window.event
		     } else {
  				  oWorkItem= theEvent.target;
		     }

		  if((theEvent.clientX>0) &&
			 (theEvent.clientY>0) &&
			 (theEvent.clientX<document.body.offsetWidth) &&
			 (theEvent.clientY<document.body.offsetHeight)){
			if(oWorkItem.getAttribute("component")=="menuitem"){
			  oWorkItem.className = "selected"
			}
		  }
		}

		fnChirpOff = function(theEvent){
			if (window.event) {
				  oWorkItem=window.event.srcElement;
				  theEvent = window.event
		     } else {
  				  oWorkItem= theEvent.target;
		     }

		  if((theEvent.clientX>0) &&
			 (theEvent.clientY>0) &&
			 (theEvent.clientX<document.body.offsetWidth) &&
			(theEvent.clientY<document.body.offsetHeight)){

			if(oWorkItem.getAttribute("component")=="menuitem"){
			  oWorkItem.className = ""
			}
		  }
		}
		
		fnInit = function(){
			//  runs when page loads. oContextMenu is the ID of the content menu div, so if it not present, this doesn't happen
			theContextMenu = document.getElementById('oContextMenu')
		  if (theContextMenu) {
			theContextMenu.style.width=150+'px';
		//	WAB removed 14/11/06, caused problems in firefox and seems to work better if browser does it itself: theContextMenu.style.height= document.body.offsetHeight/2;
			theContextMenu.style.zIndex=2;
			// Setup the basic styles of the context menu.
			document.oncontextmenu=fnSuppress;
		  }
		}

		fnInContext = function(el) {
			// this determines whether the click was in an existing context menu
			// console.log ('fnInContext el.id' + el.id)
		  while (el!=null) {
			if (el.id=="oContextMenu") {
				return true
			}	
			el = el.offsetParent
		  }
		  return false
		}
		

		fnSuppress = function(theEvent){
		// WAB interpretation! this function replaces the oncontextmenu event (this is done in fnInit() function)
		// when the right button is clicked, this checks to see if click is in an existing context menu in which case it is handled just like a left click (bContextKey remains false )
		// if click is not in a context menu then bContextKey is set to true - which means that fnDetermine processes it as a right click
		// you can also call it as an onclick event in order to get a context menu which comes up on a left click
			//console.log ('fnSuppress ' + theEvent)

				if (window.event) {
					srcElement = window.event.srcElement
			     } else {
			        srcElement = theEvent.target;
			     }

			  if (!(fnInContext(srcElement))) 
			  { 
					// not a click in a contextmenu				
					theContextMenu = document.getElementById('oContextMenu')
					theContextMenu.style.display="none";
					theContextMenu.setAttribute("status","false");
					
					if (typeof theContextMenu.releaseCapture == 'function') 
					{
						theContextMenu.releaseCapture();
					}	

					bContextKey=true;
		  		}
	
			//	console.log ('fnSuppress calling fndetermine' )

			  fnDetermine(theEvent);
			  bContextKey=false;
			}
		
		
 
		// these two function added by WAB to try and get this code to also work for bringing up 
		// a context menu on a left click
		// fnLeftClickContextMenu () becomes the onClick Event for the anchor
		// fnDetermineForLeftClick () becomes the onclick event for the body
		// I needed these extra functions because otherwise the onClick event of the body would get instantly get rid of the context menu popped up by the left click on the anchor
		
		var fnDetermineAlreadyRun=false
		fnDetermineForLeftClick = function(theEvent) {
				if (window.event) {
			        event.cancelBubble = true;
					theEvent = window.event

			     } else {
			        theEvent.stopPropagation(true);
			     }
			fnSuppress(theEvent)

		}
 
	 	fnLeftClickContextMenu = function(theEvent) {
	 			// 2014/10/14	YMA	CORE-688 IE opens context menu in main frame.
	 			theEvent.preventDefault()
	 			if (window.event) {
			        event.cancelBubble = true;
					theEvent = window.event
			     } else {
			        theEvent.stopPropagation(true);
			     }

			// console.log ('fnLeftClickContextMenu ' + theEvent)

			fnSuppress(theEvent);
		}

		
		fnSuppressMouseOver = function(theEvent) {
				fnSuppress(theEvent)
		}

		getWindowHeight = function() {
		  if( typeof( window.innerHeight ) == 'number' ) {
			//Non-IE
			   return window.innerHeight;
		  } else if( document.documentElement && document.documentElement.clientHeight ) {
		    //IE 6+ in 'standards compliant mode'
			    return  document.documentElement.clientHeight;
		  } else if( document.body && ( document.body.clientHeight ) ) {
		    //IE 4 compatible
			    return  document.body.clientHeight;
		  }
		}		
		
		function getScrollXY() { 
			var scrOfX = 0;
		  	var scrOfY = 0;
		  
		  	if( typeof( window.pageYOffset ) == 'number' ) { 
		    	//Netscape compliant 
		    	scrOfY = window.pageYOffset; 
		    	scrOfX = window.pageXOffset; 
		  	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) { 
		    	//DOM compliant 
		    	scrOfY = document.body.scrollTop; 
		    	scrOfX = document.body.scrollLeft; 
		  	} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) { 
		    	//IE6 standards compliant mode 
		    	scrOfY = document.documentElement.scrollTop; 
		    	scrOfX = document.documentElement.scrollLeft; 
		  	}
		  	return {x:scrOfX, y:scrOfY}; 
		}
