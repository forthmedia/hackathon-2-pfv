/* �Relayware. All Rights Reserved 2014 */
/*
	File name: restrictKeyStrokes.js
	
	Using the function
	OPTION 1: add the following function to the input box you wish to protect
	onKeyPress="restrictKeyStrokes(this,keybNumeric)"
	see relay/leadManager/oppProductList.cfm
	
	 OPTION 2: create a function to add an event and then call the function in window.onload()
	 void function setEvents() { document.all.txtYN.onkeypress = new Function('restrictKeyStrokes(this,keybYN)');}
		<body onLoad="JavaScript:setEvents()">
	
	 see authors article at http://builder.com.com/5100-6371-1044655.html
	 WAB 14/03/2007 Andy noticed that z was not in the list of letters!

	2012-09-12 PPB Case 430249 don't allow entry of commas (unless the users locale expects them as a decimal seperator 
	2012-09-17 PPB Case 430709 added a minus sign to the list of decimal chars
	2012-09-21 PPB Case 430709 added a minus sign to the list of numeric chars
	2012-09-21 PPB Case 430402 translate error messages
	2012-09-25 PPB Case 430402 remove hardcoded "Error:" so translated message is a single language
	2012-09-26 PPB LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED
	2012-11-05 WAB CASE 431742 Problem with IE9 (in IE9 mode), leaving bad character in input box.  Changed some code to test for event.preventDefault not window.event
	2013-02-13 NYB Case 433619 added function restrictOnChange(objKeyb,obj)
	2013-09-18 WAB Added NumericList Type 
	2016-12-19 DAN PROD2016-2964 / Case 453402 - additional input validation for numeric to prevent inputs such as for example 10-30 
*/



/* LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED
// 2012-09-12 PPB Case 430249 START 
var validCharsDecimal = '0123456789-'  					//2012-09-17 PPB Case 430709 added the minus sign

if (localeInfo.decimalPoint!=undefined) { 
	validCharsDecimal += localeInfo.decimalPoint ;
} else {
	validCharsDecimal += '.';
}
// 2012-09-12 PPB Case 430249 END 
*/
var validCharsDecimal = '0123456789-.'  					//2012-09-17 PPB Case 430249 LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED; THIS LINE WILL BE REMOVED

var keybYN = new keybEdit('yn',phr.YouCanOnlyEnterYN);
var keybNumeric = new keybEdit('0123456789-',phr.YouCanOnlyEnterNumerics);
var keybNumericList = new keybEdit('0123456789-,',phr.YouCanOnlyEnterNumerics);
var keybAlpha = new keybEdit('abcdefghijklmnopqurstuvwxyz',phr.YouCanOnlyEnterLetters);
var keybAlphaNumeric = new keybEdit('abcdefghijklmnopqurstuvwxyz01234567890',phr.YouCanOnlyEnterAlphaNumerics);
var keybDecimal = new keybEdit(validCharsDecimal,phr.YouCanOnlyEnterDecimals);	// 2012-09-12 PPB Case 430249 
var keybDate =  new keybEdit('01234567890/',phr.YouCanOnlyEnterDates);
var keybRelaySafe =  new keybEdit('abcdefghijklmnopqurstuvwxyz01234567890- ()',phr.YouCanOnlyEnterAlphaNumericsBracketsAndMinus);
var keybCFVariable =  new keybEdit('abcdefghijklmnopqurstuvwxyz01234567890_',phr.YouCanOnlyEnterAlphaNumericsAndUnderscore);
var keybTelephone =  new keybEdit('0123456789+- ()',phr.YouCanOnlyEnterNumericsBracketsSpaceAndPlus);

var keybYNNM = new keybEdit('yn');
var keybNumericNM = new keybEdit('0123456789-');													//2012-09-21 PPB Case 430709 added the minus sign
var keybNumericListNM = new keybEdit('0123456789-,');
var keybAlphaNM = new keybEdit('abcdefghijklmnopqurstuvwxyz');
var keybAlphaNumericNM = new keybEdit('abcdefghijklmnopqurstuvwxyz01234567890');
var keybDecimalNM = new keybEdit(validCharsDecimal);	// 2012-09-12 PPB Case 430249 
var keybDateNM = new keybEdit('01234567890/');
var keybRelaySafeNM =  new keybEdit('abcdefghijklmnopqurstuvwxyz01234567890- ()');
var keybCFVariableNM =  new keybEdit('abcdefghijklmnopqurstuvwxyz01234567890_');
var keybTelephoneNM =  new keybEdit('0123456789+- ()');

function keybEdit(strValid, strMsg) {
	/*	Function:		keybEdit
		Creation Date:	October 11, 2001
		Programmer:		Edmond Woychowsky
		Purpose:		The purpose of this function is to be a constructor for
						the keybEdit object.  keybEdit objects are used by the
						function editKeyBoard to determine which keystrokes are
						valid for form objects.  In addition, if an error occurs,
						they provide the error message.
						
						Please note that the strValid is converted to both
						upper and lower case by this constructor.  Also, that
						the error message is prefixed with 'Error:'.
						
						The properties for this object are the following:
							valid	=	Valid input characters
							message	=	Error message
							
						The methods for this object are the following:
							getValid()	=	Returns a string containing valid
											characters.
							getMessage()=	Returns a string containing the
											error message.

		Update Date:	Programmer:			Description:
	*/

	//	Variables
	var reWork = new RegExp('[a-z]','gi');		//	Regular expression\

	//	Properties
	if(reWork.test(strValid))
		this.valid = strValid.toLowerCase() + strValid.toUpperCase();
	else
		this.valid = strValid;

	if((strMsg == null) || (typeof(strMsg) == 'undefined'))
		this.message = '';
	else
		this.message = strMsg;

	//	Methods
	this.getValid = keybEditGetValid;
	this.getMessage = keybEditGetMessage;
	
	function keybEditGetValid() {
	/*	Function:		keybEdit
		Creation Date:	October 11, 2001
		Programmer:		Edmond Woychowsky
		Purpose:		The purpose of this function act as the getValid method
						for the keybEdit object.  Please note that most of the
						following logic is for handling numeric keypad input.

		Update Date:		Programmer:			Description:
	*/

		return this.valid.toString();
	}
	
	function keybEditGetMessage() {
	/*	Function:		keybEdit
		Creation Date:	October 11, 2001
		Programmer:		Edmond Woychowsky
		Purpose:		The purpose of this function act as the getMessage method
						for the keybEdit object.

		Update Date:	Programmer:			Description:
	*/
	
		return this.message;
	}
}

//NYB Case 433619 2013-02-13 added function:
 function restrictOnChange(objKeyb,obj) {
	var elemValue = obj.value;
	var newValue = "";
	var strWork = objKeyb.getValid();
	var strMsg = '';
	var blnValidChar = true;

	if (keybNumericNM == objKeyb || keybNumeric == objKeyb || keybDecimalNM == objKeyb || keybDecimal == objKeyb) {
		// validate if numeric to prevent inputs such as for example 10-30
		if (!jQuery.isNumeric(elemValue.replace(/\,/g, '\.'))) { // replace commas with dots for validation
			blnValidChar = false;
		}
	}

	if (blnValidChar) {
		// Part 1: Validate input
		for(i=0;i < elemValue.length;i++) {
			blnValidChar = false;
			for(j=0;j < strWork.length;j++) {
				if(elemValue.charCodeAt(i) == strWork.charCodeAt(j)) {
					blnValidChar = true;
					break;
				}
			}
			if (!blnValidChar) {
				break;			
			}
		}
	}

	// Part 2: Build error message
	if(!blnValidChar) {
		obj.focus();
		obj.value = "";
		if(objKeyb.getMessage().toString().length != 0) {
			alert(objKeyb.getMessage());
		}
	}
 }


 function restrictKeyStrokes(event,objKeyb) {
	/*	Function:		editKeyBoard
		Creation Date:	October 11, 2001
		Programmer:		Edmond Woychowsky
		Purpose:		The purpose of this function is to edit edit keyboard input
						to determine if the keystrokes are valid.
	
		Update Date:		Programmer:			Description:
		June 2006 			WAB					modified to work with firefox, pass in event rather than this
							WAB					mods so that FF allows delete etc.
	*/
	strWork = objKeyb.getValid();
	strMsg = '';							// Error message
	blnValidChar = false;					// Valid character flag

	if (window.event) {
//		event = window.event
		keyCode = event.keyCode
		objForm = event.srcElement
	} else {
		keyCode = event.charCode	
		if(keyCode == undefined) keyCode = event.keyCode;	
		objForm = event.target
		if (keyCode == 0 || keyCode == 9) {	blnValidChar = true;}   // this seems to be the case of cursor keys etc. in FF (CASE 428543: NJH added 9 for tabs  - specifically for Opera)
	}	

	// Part 1: Validate input
	if(!blnValidChar)
		for(i=0;i < strWork.length;i++)
			if(keyCode == strWork.charCodeAt(i)) {
				blnValidChar = true;
				break;
			}

	// Part 2: Build error message
	if(!blnValidChar) {
		if(objKeyb.getMessage().toString().length != 0)
			alert(objKeyb.getMessage());
				// Clear invalid character
				// WAB 2012-11-05 431742 changed test here from window.event to !event.preventDefault.  IE9 has window.event but does not support event.returnValue, but does support event.preventDefault
				if (!event.preventDefault) { 
					event.returnValue = false;
				} else {
					event.preventDefault();
				}
		 objForm.focus();						// Set focus
	}
}


//2012-09-24 PPB Case 430249 now that we force decimals to be entered in locale-format without thousand delimiters, we should use this function instead of parseFloat during validataion
//           however its only likely to have serious consequences not to do so if the value is then multiplied by a large number (eg opp products) because parseFloat(1234,56)->1234 
function LSParseFloat(strNumber) {
	if (localeInfo.decimalPoint!=undefined) { 
		strNumber = strNumber.replace(localeInfo.decimalPoint,'.');
	}
	return parseFloat(strNumber);	
}


function LSNumberFormat(numNumber) {
	strNumber = numNumber.toString();
	if (localeInfo.decimalPoint!=undefined) { 
		strNumber = strNumber.replace('.',localeInfo.decimalPoint);
	}
	return strNumber;	
}
