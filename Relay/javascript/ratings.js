/* �Relayware. All Rights Reserved 2014 */
	var slideDownInitHeight = new Array();
	var slidedown_direction = new Array();
	
	
	var divBoxClass = ''; // Need to set this with setBoxClass to the class of the pop up div
	var slidedownActive = false;
	var contentHeight = false;
	var slidedownSpeed = 3; 	// Higher value = faster script
	var slidedownTimer = 7;	// Lower value = faster script
	var entityTypeID = 1;
		
	function setSlideDownSpeed(newSpeed)
	{
		slidedownSpeed = newSpeed;
		
	}
	
	function setBoxClass(newDivBoxClass)
	{
		divBoxClass = newDivBoxClass;
		
	}
	
	function setEntityTypeID(etID)
	{
		entityTypeID = etID;
	}
	
	//control variable that stops re-writting star rating to DB once its been submitted
	var sendToDB = true;
	
	function getScore()
			{
			    for (var i = 0; i < document.getElementsByName('entityScore').length; i++)
			    {
			        if (document.getElementsByName('entityScore')[i].checked)
			        {
			                return document.getElementsByName('entityScore')[i].value;
			        }
		    	}
			} 
			
	//Wrote this funcion to submit rating with out the button - this would be triggered by just clicking over the star
	//This function is called from line 159 in jquery.rating.js
	function submitRatings(entityTypeID,entityID,starRating) {
		page = '/webservices/relayRatingWS.cfc?method=rateEntity&_cf_nodebug=true&returnFormat=plain'
		parameters = 'entityID='+entityID+'&rating='+ starRating + '&entityTypeID='+entityTypeID
		
		//if (sendToDB){
		// NJH 2013/02/05 - handle this now in the function...having sendToDb here meant that you could only rate one entity per page.
			var myAjax = new Ajax.Request(
				page, 
				{
					method: 'post', 
					parameters: parameters, 
					onComplete: function() {updateRatingsDiv(entityID,entityTypeID)}
				});

			//sendToDB = false;
			return;
		//}
	}
		
	
	function updateRatingsDiv(entityID,entityTypeID) {
		page = '/webservices/callWebservice.cfc?wsdl&method=callWebService&webservicename=relayRatingWS&methodName=getRatingsDiv&_cf_nodebug=true&returnFormat=plain'
		
		parameters = 'entityID='+entityID + '&entityTypeID='+entityTypeID
		
		var myAjax = new Ajax.Updater(
			'starRatings'+entityID,
			page, 
			{
				method: 'get', 
				parameters: parameters
			});
		return;
	}