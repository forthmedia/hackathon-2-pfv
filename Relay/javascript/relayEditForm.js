/* �Relayware. All Rights Reserved 2014 */
//  standard javascript functions for relayEditForms 
//  example syntax
// <SCRIPT type="text/javascript" src="http://#request.currentsite.domainandroot#/javascript/relayEditForm.js"></script>

// BUG FIX 04-03-2005 - There has been a problem in the Screen's section where you cannot add a new screen
// It was brought back to this file as the form was not called "mainForm". I have added
// an Is defined mainForm condition below to handle the situation... It is unknown if this 
// will have an effect elsewhere in the application. Alex Connell - 04-03-2005

// WAB March 2006
// Added handling of form.dirty.  If submit form is passed a second parameter promptForChange = 1 then the user is asked if they want to save the changes to the page
// function looks for hidden field dirty in the form
// defaults to promptForChange = 0 (which makes it backwards compatible)
// 2016/02/15	NJH		Use rwFormValidation - removed js alerts. Call triggerFormSubmit instead of form.submit()

function submitForm(wizardPage,promptForChange,thisObject ) {
	
	promptForChange =  (promptForChange == undefined)? 0 : promptForChange ;
	var submitForm = false;
	
	
	// New 04-03-2005 - Check document.mainForm is defined
	if (typeof(document.mainForm) != 'undefined') {
	form = document.mainForm;
	// New 04-03-2005 - Check form.frmTask is defined
		if (typeof(form.frmTask) != 'undefined') {
			
			// promptForChange we ask the user if they want to save changes (only works if a dirty variable exists)
			// WAB 2011/06/28 changed to support use of isDirty() function rather than dirty hidden field.  Left in old code but don't think it is used
			if (promptForChange) { 
				formDirty = false 
				if (typeof (form.dirty) != 'undefined' && form.dirty.value == 1) { formDirty = true ; 
				} else if (typeof (isDirty) != 'undefined' && isDirty(form,{debug:false})) { formDirty = true ; 
				}

					if (formDirty)  {
						saveChanges =  confirm ('Save changes?')
						if (!saveChanges) {
							form.frmTask.value = 'doNothing'   // by doing this, won't do any of the updates
							form.frmWizardPage.value = wizardPage;
							//form.submit()
							//triggerFormSubmit(form);
							//return
							submitForm=true;
						}
					}
			}

		
			// if there is a function isFormOKtoSubmit defined then we run it
			if (typeof isFormOKtoSubmit != 'undefined')  { 
				OKtoSubmit = isFormOKtoSubmit(form)
			} else {
				OKtoSubmit = true
			}


			if (OKtoSubmit) {
				form.frmTask.value = 'save';

				// WAB 03/06/05   added check for wizard variable being past.  sometimes just want to stay on current page and don't pass in the variable
				if (wizardPage  )	{
					form.frmWizardPage.value = wizardPage;
				}
	
				//form.submit()
				//triggerFormSubmit(form);
				//return
				submitForm=true;
			} else {
				return
			}
			
				
		}
	// New 04-03-2005 - for undefined frmTask
	} else {
		//form.submit()
		//triggerFormSubmit(form);
		//return
		submitForm=true;

	}
	
	if (submitForm) {
		console.log('form submit');
		triggerFormSubmit(form);
		//jQuery('#slideHeader a').unbind('click');
		jQuery('#slideHeader a').click(function (e) {/* e == our event data*/ alert('inclick');e.preventDefault();});
		jQuery('#slideHeader a').unbind('click');
		return
	}
	return;
}