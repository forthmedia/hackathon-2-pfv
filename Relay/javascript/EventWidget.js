calAllIndex = new Array();
calAllData = new Array();

function pureCalContainer(calId, calClass, theDate, dayNames, monthNames, dayNumbers, startDay, showAllCells, fromDate, toDate, useFromDate, useToDate, dayFunctionName){
			
			/* calIndex Holds The Index Key name to the Data of Current View */
			var calIndex = new Array();
			
			/* calData Holds The Key Data For Current View */
			var calData = new Array();
			
			/* calArray Holds The Key and Index Arrays For Current View - RETURN VALUE */
			var calArray = new Array();
			
			var baseDiv = jQuery('[id=CalWidget' + calId + ']');
			var theRows = 6;
			var theMonth = theDate.getMonth();
			var theYear = theDate.getFullYear();
			var numbersArray = dayNumbers.split(',');
			var dayArray = dayNames.split(',');
			var dayStart = jQuery.inArray(startDay, dayArray);
			var calDayArray = pureReIndexArraybyName(dayArray, dayStart);
			var monthArray = monthNames.split(',');
			var monthStart = new Date(theYear, theMonth, 1);
			var monthEnd = new Date(theYear, theMonth + 1, 1);
			var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
			var monthDay = monthStart.getDay();
			var monthYear = monthStart.getFullYear();
			var monthMonth = monthStart.getMonth();
			var lastMonthStart = new Date(monthStart.getFullYear(), (monthStart.getMonth() - 1), 1);
			var lastMonthEnd = new Date(monthStart.getFullYear(), (monthStart.getMonth() - 1 + 1), 1);
			var lastMonthLength = (lastMonthEnd - lastMonthStart) / (1000 * 60 * 60 * 24);
			var lastMonthDay = lastMonthStart.getDay();
			var lastMonthYear = lastMonthStart.getFullYear();
			var lastMonthMonth = lastMonthStart.getMonth();
			var nextMonthStart = new Date(monthStart.getFullYear(), (monthStart.getMonth() + 1), 1);
			var nextMonthEnd = new Date(monthStart.getFullYear(), (monthStart.getMonth() + 1 + 1), 1);
			var nextMonthLength = (nextMonthEnd - nextMonthStart) / (1000 * 60 * 60 * 24);
			var nextMonthDay = nextMonthStart.getDay();
			var nextMonthYear = nextMonthStart.getFullYear();
			var nextMonthMonth = nextMonthStart.getMonth();
			var convertedMonthDay = jQuery.inArray(dayArray[monthDay], calDayArray);
			var convertedlastMonthDay =	((lastMonthLength - convertedMonthDay)+1);
				
				calData.push(calId);
				calData.push(baseDiv);
				calData.push(calClass);
				calData.push(theDate);
				calData.push(dayNames);
				calData.push(monthNames);
				calData.push(dayNumbers);
				calData.push(startDay);
				calData.push(showAllCells);
				calData.push(dayStart);
				calData.push(monthStart);
				calData.push(monthYear);
				calData.push(lastMonthStart);
				calData.push(nextMonthStart);
				calData.push(fromDate);
				calData.push(toDate);
				calData.push(useFromDate);
				calData.push(useToDate);
				calData.push(dayFunctionName);
				
				calIndex.push('calId');
				calIndex.push('baseDiv');
				calIndex.push('calClass');
				calIndex.push('theDate');
				calIndex.push('dayNames');
				calIndex.push('monthNames');
				calIndex.push('dayNumbers');
				calIndex.push('startDay');
				calIndex.push('showAllCells');
				calIndex.push('dayStart');
				calIndex.push('monthStart');
				calIndex.push('monthYear');
				calIndex.push('lastMonthStart');
				calIndex.push('nextMonthStart');
				calIndex.push('fromDate');
				calIndex.push('toDate');
				calIndex.push('useFromDate');
				calIndex.push('useToDate');
				calIndex.push('dayFunctionName');
				
				calArray.push(calIndex);
				calArray.push(calData);
				
				/*
				calAllIndex = new Array();
				calAllData = new Array();
				*/
				
				var CheckMainArray = jQuery.inArray(calId, calAllIndex);
				
				if(CheckMainArray >= 0){
					calAllData[CheckMainArray] = calArray;
				} else {
					calAllIndex.push(calId);
					calAllData.push(calArray);			
				}
				
				jQuery(baseDiv).hide().html('');
				
				var CalDivRow = jQuery( '<div />' );
				jQuery(CalDivRow).addClass('DivCalRow DivCalRow' + calClass)
					.attr('calId', calId);
					
				
				var CalDivHeadTool = jQuery( '<div />' );
				jQuery(CalDivHeadTool).addClass('CalDivCell CalDivCell' + calClass + ' CalDivHead CalDivHead' + calClass + '  CalDivHeadTool CalDivHeadTool' + calClass)
					.attr('calId', calId)
					.attr('onClick', 'processCalClick(this);');
					
				var CalDivHeadToolRight = jQuery( '<div />' );
				jQuery(CalDivHeadToolRight).addClass('CalDivCell CalDivCell' + calClass + ' CalDivHead CalDivHead' + calClass + '  CalDivHeadTool CalDivHeadTool' + calClass + '  CalDivHeadToolRight CalDivHeadToolRight' + calClass)
					.attr('calId', calId)
					.attr('onClick', 'processCalClick(this);');
				
				var CalDivHead = jQuery( '<div />' );
				jQuery(CalDivHead).addClass('CalDivCell CalDivCell' + calClass + ' CalDivHead CalDivHead' + calClass)
					.attr('calId', calId)
					.attr('onClick', 'processCalClick(this);');
				
				var CalDivCell = jQuery( '<div />' );
					jQuery(CalDivCell).attr('calId', calId)
					.attr('onClick', 'processCalClick(this);');
							
				var day = 0;
				
				var HeadToolRow = jQuery(CalDivRow).clone()
					.attr('id','CalToolRow')
					.html('');
				jQuery(baseDiv).append(HeadToolRow); //.fadeIn(800, "swing", "linear");
				
				/* START - LAST MONTH */
				
				disableLast = false;
				disableApply = "";
				disableClass = "";
				
				if(useFromDate){
					var checkDate = new Date(monthYear, monthMonth - 1, 1);
					
					if(checkDate <= fromDate){
						disableLast = true;	
					} else {
						disableLast = false;
					}
				}	
				
				if(disableLast){
					disableApply = "Disabled"
					disableClass = "calDisabled"
				}
				
				var LastDiv = jQuery(CalDivHeadTool).clone()
					.attr('id', 'CalLastMonthYear' + disableApply)
					.addClass(disableClass)
					//.removeClass('CalDivHead')
					//.removeClass('CalDivHead' + calClass)
					.html('&nbsp;');

				if(!disableLast){
					jQuery(LastDiv)
						.removeClass('calDisabled')
						//.addClass('CalDivHead')
						//.addClass('CalDivHead' + calClass)
						.html('&lt;');
				}
					
				jQuery(HeadToolRow).append(LastDiv); //.fadeIn(800, "swing", "linear");
				/* END - LAST MONTH */

				/* MAIN HEADING - START */
				var ToolDiv = jQuery(CalDivHead).clone()
					.attr('id', 'CalToolMonthYear')
					.attr('calId', calId)
					.attr('class', 'CalToolMonthYear CalToolMonthYear' + calClass + ' CalDivHeadTool CalDivHeadTool' + calClass + '  CalDivHeadToolMain CalDivHeadToolMain' + calClass)
					.html(monthArray[monthMonth] + ' ' + monthYear);
					
				jQuery(HeadToolRow).append(ToolDiv); //.fadeIn(800, "swing", "linear");
				/* MAIN HEADING - END */	

				/* NEXT MONTH - START */

				var disableNext = false;
				var disableApply = "";
				var disableClass = "";
				
				if(useToDate){
					var checkDate = new Date(monthYear, monthMonth + 1, 1 - 1);
					if(checkDate >= toDate){
						disableNext = true;	
					} else {
						disableNext = false;
					}
				}	
				
				if(disableNext){
					disableApply = "Disabled"
					disableClass = "calDisabled"
				}
							
				var NextDiv = jQuery(CalDivHeadToolRight).clone()
					.attr('id', 'CalNextMonthYear' + disableApply)
					.attr('calId', calId)
					.addClass(disableClass)
					//.removeClass('CalDivHead')
					//.removeClass('CalDivHead' + calClass)
					.html('&nbsp;');

				if(!disableNext){
					jQuery(NextDiv)
						.removeClass('calDisabled')
						//.addClass('CalDivHead')
						//.addClass('CalDivHead' + calClass)
						.html('&gt;');;
				}
					
				jQuery(HeadToolRow).append(NextDiv); //.fadeIn(800, "swing", "linear");
				/* NEXT MONTH - END */
				
				var HeadRow = jQuery(CalDivRow).clone().attr('id','CalHeadRow').html('');
				jQuery(baseDiv).append(HeadRow); //.fadeIn(800, "swing", "linear");
			
				
				jQuery(calDayArray).each(function(){
				
					var DayLoopName = calDayArray[day];
					
					var HeadCell = jQuery(CalDivHead).clone().attr('id','CalHeadCell' + day).html(DayLoopName)

					jQuery(HeadRow).append(HeadCell); //.fadeIn(800, "swing", "linear");
					
					day++	
					if(day >= dayArray.length){
						day = 0;
					}
				
				})
				
				var cell = 0;
				var nextCell = 0;	
				var DC = 1;				
				var StartDayCount = false;
				
				for(var RN = 0; RN < theRows; RN++){
					
					var brakeCallLoop = false;
					var theCellRow = jQuery(CalDivRow).clone()
						.attr('id','CalRow' + RN)
						.html('')
					
					jQuery(baseDiv).append(theCellRow); //.fadeIn(800, "swing", "linear");
			
					var day = 0;
					jQuery(calDayArray).each(function(){
						var useDate = monthStart;
					
						var DayLoopName = calDayArray[day];
						
						var DivHtml = "&nbsp;";
						
						if(cell >= convertedMonthDay){
							var StartDayCount = true;
							var useClass = 'CalDivCell CalDivCell' + calClass + ' CalActive CalActive' + calClass;
							nextCell++;
						}
						
						if(DC > monthLength){
							DC = 1;
						}
	
						if(nextCell > monthLength){
							var useClass = 'CalDivCell CalDivCell' + calClass + ' CalNext CalNext' + calClass;
							var useDate = monthEnd;
							if(day+1 == dayArray.length && showAllCells == false){
								brakeCallLoop = true;
							}
						}
						
						if(StartDayCount == true){
							var DivHtml = DC;
							DC++;
						} else {	
							var DivHtml = parseInt(convertedlastMonthDay);
							var useDate = lastMonthStart;
							var useClass = 'CalDivCell CalDivCell' + calClass + ' CalLast CalLast' + calClass;
							convertedlastMonthDay ++;
						}
						
						cellDate = new Date(useDate.getFullYear(), useDate.getMonth(), DivHtml);
						disableDate = false;
						disableApply = "";
						disableClass = "";
						
						if(useFromDate){
							if(cellDate <= fromDate){
								disableDate = true;	
							}
						}

						if(useToDate){
							if(cellDate > toDate){
								disableDate = true;	
							}
						}	
						
						if(disableDate){
							disableApply = "Disabled"
							disableClass = "calCellDisabled"
						}
						var NewCell = jQuery(CalDivCell).clone()
							.attr('id', 'CalCell' + disableApply)
							.attr('class', useClass)
							.attr('CalRow', RN)
							.attr('CalCell',cell)
							.attr('CalDay',day)
							.attr('CalDate',cellDate)
							.addClass(disableClass)
							.html(numbersArray[DivHtml-1]);

						if(!disableDate){
							jQuery(NewCell).removeClass('calCellDisabled');
						}

						jQuery(theCellRow).append(NewCell);

						if(typeof window[dayFunctionName] == 'function') {
							var fn = window[dayFunctionName];
							fn(cellDate, cell);
						} 

						cell++;
						day++;	
						if(day >= dayArray.length){
							day = 0;
						}
					
					})
				
					if(brakeCallLoop == true){
						jQuery(baseDiv).show();
						return calArray;
					}
				
				}

	jQuery(baseDiv).show();
	return calArray;
}

function pureReIndexArraybyName(theArray, theStart){
						
	var sortedArray = new Array();
	var sortIndex = 0;
	var DaysCount = false;
	var newIndex = 0;
	
	while(newIndex < theArray.length){
		
		if(sortIndex == theStart){
			DaysCount = true;
		}
		
		if(DaysCount == true){
			sortedArray[newIndex] = theArray[sortIndex];
			newIndex++
		}
	
		sortIndex++;
		
		if(sortIndex >= theArray.length){
			sortIndex = 0;
		}
	}
	
	return sortedArray;
}

function processCalClick(useCalThis){
	var UseCalId = jQuery(useCalThis).attr('calId');
	
	if(Math.floor(UseCalId) == UseCalId && jQuery.isNumeric(UseCalId)) {
		UseCalId = parseInt(UseCalId);
	}
	
	var UseID = jQuery(useCalThis).attr('id');
	var CheckMainArray = jQuery.inArray(UseCalId, calAllIndex);
		
	if(CheckMainArray >= 0){
		var SavedData = calAllData[CheckMainArray];

			var calClass = processCalSetting(SavedData, 'calClass');
			var dayNames = processCalSetting(SavedData, 'dayNames');
			var startDay = processCalSetting(SavedData, 'startDay');
			var monthNames = processCalSetting(SavedData, 'monthNames');
			var dayNumbers = processCalSetting(SavedData, 'dayNumbers');			
			var theDate = processCalSetting(SavedData, 'theDate');
			var fromDate = processCalSetting(SavedData, 'fromDate');
			var toDate = processCalSetting(SavedData, 'toDate');
			var useFromDate = processCalSetting(SavedData, 'useFromDate');
			var useToDate = processCalSetting(SavedData, 'useToDate');
			var showAllCells = processCalSetting(SavedData, 'showAllCells');
			var dayFunctionName = processCalSetting(SavedData, 'dayFunctionName');

			if(UseID == 'CalLastMonthYear' || UseID == 'CalNextMonthYear'){
				if(UseID == 'CalLastMonthYear'){
					var newCalStart = processCalSetting(SavedData, 'lastMonthStart');
				} else {
					var newCalStart = processCalSetting(SavedData, 'nextMonthStart');
				}
				var newCalData = pureCalContainer(UseCalId, calClass, newCalStart, dayNames, monthNames, dayNumbers, startDay, showAllCells, fromDate, toDate, useFromDate, useToDate, dayFunctionName);
			}
			
			if(UseID == 'CalToolMonthYear'){
				processYearView(SavedData);
			}
			
			if(UseID == 'CalLastYear' || UseID == 'CalNextYear' || UseID == 'CalCellMonth'){
				jQuery('[id=CalYearView' + UseCalId + ']').remove();
				var useCalDate = new Date(jQuery(useCalThis).attr('CalDate'));
				var newCalData = pureCalContainer(UseCalId, calClass, useCalDate, dayNames, monthNames, dayNumbers, startDay, showAllCells, fromDate, toDate, useFromDate, useToDate, dayFunctionName);
				
				if(UseID != 'CalCellMonth'){
					var newSavedData = calAllData[CheckMainArray];
					processYearView(newSavedData);
				}
			}
			
			if(UseID == 'CalToolYear'){
				jQuery('[id=CalYearView' + UseCalId + ']').remove();
				processSelectView(SavedData);
			}
			
			if(UseID == 'CalLastSelect' || UseID == 'CalNextSelect'){
				jQuery('[id=CalSelectView' + UseCalId + ']').remove();
				var useCalDate = new Date(jQuery(useCalThis).attr('CalDate'));
				var newCalData = pureCalContainer(UseCalId, calClass, useCalDate, dayNames, monthNames, dayNumbers, startDay, showAllCells, fromDate, toDate, useFromDate, useToDate, dayFunctionName);
				var newSavedData = calAllData[CheckMainArray];
				processSelectView(newSavedData);
			}
			
			if(UseID == 'CalCellYear'){
				jQuery('[id=CalSelectView' + UseCalId + ']').remove();
				var useCalDate = new Date(jQuery(useCalThis).attr('CalDate'));
				var newCalData = pureCalContainer(UseCalId, calClass, useCalDate, dayNames, monthNames, dayNumbers, startDay, showAllCells, fromDate, toDate, useFromDate, useToDate, dayFunctionName);
				var newSavedData = calAllData[CheckMainArray];
				processYearView(newSavedData);
			}
			
	} else {
		console.log('missing cal data');				
	}
}

function processSelectView(SavedData){

	var useCalId = processCalSetting(SavedData, 'calId');
	var useBaseDiv = processCalSetting(SavedData, 'baseDiv');
	var useCalClass = processCalSetting(SavedData, 'calClass');
	var useMonthYear = processCalSetting(SavedData, 'monthYear');
	
	var divSelectView = jQuery( '<div />' );
	jQuery(divSelectView).addClass('CalSelectView CalSelectView' + useCalClass)
		.attr('calId', useCalId)
		.attr('Id', 'CalSelectView' + useCalId).html('Loading....')
		
	jQuery(useBaseDiv).html('').append(divSelectView).hide().show();
	
		jQuery(divSelectView).html('');
		
		var CalDivRow = jQuery( '<div />' );
		jQuery(CalDivRow).addClass('DivCalRow DivCalRow' + useCalClass)
			.attr('calId', useCalId);
			
		
		var CalDivHeadTool = jQuery( '<div />' );
		jQuery(CalDivHeadTool).addClass('CalDivCell CalDivCell' + useCalClass + ' CalDivHead CalDivHead' + useCalClass + '  CalDivHeadTool CalDivHeadTool' + useCalClass)
			.attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
			
		var CalDivHeadToolRight = jQuery( '<div />' );
		jQuery(CalDivHeadToolRight).addClass('CalDivCell CalDivCell' + useCalClass + ' CalDivHead CalDivHead' + useCalClass + '  CalDivHeadTool CalDivHeadTool' + useCalClass + '  CalDivHeadToolRight CalDivHeadToolRight' + useCalClass)
			.attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
		
		var CalDivHead = jQuery( '<div />' );
		jQuery(CalDivHead).addClass('CalDivCell CalDivCell' + useCalClass + ' CalDivHead CalDivHead' + useCalClass)
			.attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
		
		var CalDivCell = jQuery( '<div />' );
			jQuery(CalDivCell).attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
					
		
		var HeadToolRow = jQuery(CalDivRow).clone()
			.attr('id','CalToolRow')
			.html('');
		jQuery(divSelectView).append(HeadToolRow); //.fadeIn(800, "swing", "linear");
		
		var newYear = useMonthYear - 5 - 6 - 1;
		var NewDate = new Date(newYear, 0, 1);
		
		var LastDiv = jQuery(CalDivHeadTool).clone()
			.attr('id', 'CalLastSelect')
			.attr('CalDate',NewDate)
			.html('&lt;');
		jQuery(HeadToolRow).append(LastDiv); //.fadeIn(800, "swing", "linear");
		
		var ToolDiv = jQuery(CalDivHead).clone()
			.attr('id', 'CalToolSelect')
			.attr('calId', useCalId)
			.attr('class', 'CalToolSelect CalToolSelect' + useCalClass + ' CalDivHeadTool CalDivHeadTool' + useCalClass + '  CalDivHeadToolMain CalDivHeadToolMain' + useCalClass)
			.html('Select A Year');
		jQuery(HeadToolRow).append(ToolDiv); //.fadeIn(800, "swing", "linear");
		
		var newYear = useMonthYear + 6 + 5 + 1;
		var NewDate = new Date(newYear, 0, 1);
		
		var NextDiv = jQuery(CalDivHeadToolRight).clone()
			.attr('id', 'CalNextSelect')
			.attr('CalDate',NewDate)
			.attr('calId', useCalId)
			.html('&gt;');
			
		jQuery(HeadToolRow).append(NextDiv); //.fadeIn(800, "swing", "linear");
		
				var cell = useMonthYear - 5;
				var nextCell = 0;	
				var DC = 1;				
				var StartDayCount = false;
				
				for(var RN = 0; RN < 4; RN++){
					
					var brakeCallLoop = false;
					var theCellRow = jQuery(CalDivRow).clone()
						.attr('id','CalSelectRow' + RN)
						.html('')
					
					jQuery(divSelectView).append(theCellRow); //.fadeIn(800, "swing", "linear");
					
					for(var MN = 0; MN < 3; MN++){
					
						var NewDate = new Date(cell, 0, 1);
						
						var NextDiv = jQuery(CalDivCell).clone()
							.attr('id', 'CalCellYear')
							.attr('class', 'CalDivMonthCell CalDivMonthCell' + useCalClass + ' CalActive CalActive' + useCalClass)
							.attr('CalRow', MN)
							.attr('CalCell',cell)
							.attr('CalDate',NewDate)
							.html(cell)
							
						jQuery(theCellRow).append(NextDiv); //.fadeIn(800, "swing", "linear");
						cell++;
						
					}
						
				}

}

function processYearView(SavedData){

	var useCalId = processCalSetting(SavedData, 'calId');
	var useBaseDiv = processCalSetting(SavedData, 'baseDiv');
	var useCalClass = processCalSetting(SavedData, 'calClass');
	var useMonthYear = processCalSetting(SavedData, 'monthYear');
	var useMonthNames = processCalSetting(SavedData, 'monthNames');
	var useMonthArray = useMonthNames.split(',');
	
	var divYearView = jQuery( '<div />' );
	jQuery(divYearView).addClass('CalYearView CalYearView' + useCalClass)
		.attr('calId', useCalId)
		.attr('Id', 'CalYearView' + useCalId).html('Loading....')
		
	jQuery(useBaseDiv).html('').append(divYearView).hide().show();
	
		jQuery(divYearView).html('');
		
		var CalDivRow = jQuery( '<div />' );
		jQuery(CalDivRow).addClass('DivCalRow DivCalRow' + useCalClass)
			.attr('calId', useCalId);
			
		
		var CalDivHeadTool = jQuery( '<div />' );
		jQuery(CalDivHeadTool).addClass('CalDivCell CalDivCell' + useCalClass + ' CalDivHead CalDivHead' + useCalClass + '  CalDivHeadTool CalDivHeadTool' + useCalClass)
			.attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
			
		var CalDivHeadToolRight = jQuery( '<div />' );
		jQuery(CalDivHeadToolRight).addClass('CalDivCell CalDivCell' + useCalClass + ' CalDivHead CalDivHead' + useCalClass + '  CalDivHeadTool CalDivHeadTool' + useCalClass + '  CalDivHeadToolRight CalDivHeadToolRight' + useCalClass)
			.attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
		
		var CalDivHead = jQuery( '<div />' );
		jQuery(CalDivHead).addClass('CalDivCell CalDivCell' + useCalClass + ' CalDivHead CalDivHead' + useCalClass)
			.attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
		
		var CalDivCell = jQuery( '<div />' );
			jQuery(CalDivCell).attr('calId', useCalId)
			.attr('onClick', 'processCalClick(this);');
					
		
		var HeadToolRow = jQuery(CalDivRow).clone()
			.attr('id','CalToolRow')
			.html('');
		jQuery(divYearView).append(HeadToolRow); //.fadeIn(800, "swing", "linear");
					
		var NewDate = new Date(useMonthYear - 1, 0, 1);
		
		var LastDiv = jQuery(CalDivHeadTool).clone()
			.attr('id', 'CalLastYear')
			.attr('CalDate',NewDate)
			.html('&lt;');
		jQuery(HeadToolRow).append(LastDiv); //.fadeIn(800, "swing", "linear");
		
		var ToolDiv = jQuery(CalDivHead).clone()
			.attr('id', 'CalToolYear')
			.attr('calId', useCalId)
			.attr('class', 'CalToolYear CalToolYear' + useCalClass + ' CalDivHeadTool CalDivHeadTool' + useCalClass + '  CalDivHeadToolMain CalDivHeadToolMain' + useCalClass)
			.html(useMonthYear);
		jQuery(HeadToolRow).append(ToolDiv); //.fadeIn(800, "swing", "linear");
					
		var NewDate = new Date(useMonthYear + 1, 0, 1);
		
		var NextDiv = jQuery(CalDivHeadToolRight).clone()
			.attr('id', 'CalNextYear')
			.attr('CalDate',NewDate)
			.attr('calId', useCalId)
			.html('&gt;');
			
		jQuery(HeadToolRow).append(NextDiv); //.fadeIn(800, "swing", "linear");
		
				var cell = 0;
				var nextCell = 0;	
				var DC = 1;				
				var StartDayCount = false;
				
				for(var RN = 0; RN < 4; RN++){
					
					var brakeCallLoop = false;
					var theCellRow = jQuery(CalDivRow).clone()
						.attr('id','CalYearRow' + RN)
						.html('')
					
					jQuery(divYearView).append(theCellRow); //.fadeIn(800, "swing", "linear");
					
					for(var MN = 0; MN < 3; MN++){
					
						var NewDate = new Date(useMonthYear, cell, 1);
						
						var NextDiv = jQuery(CalDivCell).clone()
							.attr('id', 'CalCellMonth')
							.attr('class', 'CalDivMonthCell CalDivMonthCell' + useCalClass + ' CalActive CalActive' + useCalClass)
							.attr('CalRow', MN)
							.attr('CalCell',cell)
							.attr('CalDate',NewDate)
							.html(useMonthArray[cell])
							
						jQuery(theCellRow).append(NextDiv); //.fadeIn(800, "swing", "linear");
						cell++;
						
					}
						
				}

}

function processCalSetting(settingArray, getMe){

	var theIndex = settingArray[0];
	var theData = settingArray[1];
	
	var CheckDataArray = jQuery.inArray(getMe, theIndex);
		
	if(CheckDataArray >= 0){
		var SavedData = theData[CheckDataArray];
		return SavedData;
	} else {
		console.log('missing cal data');				
	}

}	