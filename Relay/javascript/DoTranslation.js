/* �Relayware. All Rights Reserved 2014 */
/*
 use internalDomainAndRoot so that translator is always logged on as themselves on the internal site
WAB 2009/02/11 Added showAddNewPhrase parameter to allow hiding of that part of form.  Defaults to true for backwards compatability
*/ 


function doTranslation(phraseid,showAddNewPhrase) {
	//-----------------
	var now = new Date()
	now = now.getTime()
	showAddNewPhrase = (showAddNewPhrase == undefined)?true:false
	newWindow = window.open( iDomainAndRoot + '/translation/editPhrase.cfm?frmphraseid='+phraseid + "&showAddNewPhrase=" + showAddNewPhrase +  "&x=" + now, 'translator','width=800,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'  )
	newWindow.focus()
	}


function doNewTranslation(phrasetextid, entitytype, entityid) {
	//-----------------

	var now = new Date()
	now = now.getTime()

	var scriptname = iDomainAndRoot + "/translation/addNewPhrase.cfm?frmphrasetextid=" + phrasetextid

	if (entitytype != null)
		scriptname += ("&entitytype=" + entitytype)
	if (entityid != null)
		scriptname += ("&entityid=" + entityid)

	scriptname += ("&cid=" + now)
	
	newWindow = window.open(scriptname, "translator", "width=800,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1")
	newWindow.focus()
	}
