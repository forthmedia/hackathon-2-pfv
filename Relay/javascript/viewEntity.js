/* �Relayware. All Rights Reserved 2014 */
/*

	code for bringing up the entityNavigation.
	looks around for the entityNavigationFrame if not found it opens it in a new window (maybe a tab when we go EXT)

WAB 2008/07/09 problem if openInNewTab when ext not being used
WAB 2012-10-15 fixed problem searhcing into IFrames which were in a different domain
2014-08-15	RPW	Relayware Internal Search - revert to displaying Dashboard first
*/


	
//  Looks for a particular function in the current window and all its child frames 
// this is needed because I can't be sure where the entityNavigation is
//  if found returns the object, otherwise returns false
	function findFrameContainingFunction (obj, functionName, recurseParents, excludeObj) {
		/* obj = place to start search usually window
			functioName = name of function to search for
			excludeObj - used when recursing parent to exclude the part of the tree you are working up
			recurseparents - true or false (set to false when working down the tree)
		*/
		// look for function in current window
		if (eval('obj.' + functionName)) {
			// found, so return a pointer to this window
			return obj
		} else if (obj.frames){
			// not found, but has child frames so recurse through those frames
			for (var i=0;i<obj.frames.length;i++) {
				// Child frame must be in our domain, haven't worked out best way of doing this, but this try/catch works
				
				try {
					test = obj.frames[i].document
					ownDomain = true;
				}
				catch (err ){
					ownDomain = false;
				}		
				if (ownDomain) {
					// excludeObjName = (excludeObj) ? excludeObj.name : excludeObj
					if (obj.frames[i] != excludeObj) {  // don't recurse back down branch we have come up
						foundObj = findFrameContainingFunction (obj.frames[i], functionName, false)
						// if found in children then object is returned
						if (foundObj) {
							return foundObj
						}
					}	
				}
			}
		}

		// not found in this window or any of its children
		// so look in the parent window, but need to exclude this one from search
		if (recurseParents) {

			theparent = obj.parent
				// check that we aren't at the top of the frame set
				// WAB 2008/05/07 seemed to sometimes open in viewer belonging to other sites, so checking hostname (may be spurious test) - I think maybe the problem I had was with openWin using the same name between sites
			if (theparent != obj && theparent.location.hostname == obj.location.hostname ) { 
				foundObj = findFrameContainingFunction (theparent, functionName, true, obj)
				if (foundObj) {
					return foundObj
				}
			}
		}
		
		return false
	
	}
	

	//SSS 2008/04/14 moved inside function to enable IE to continue to work after pop up closed and viewEntity clicked again from selections Bug 97 lexmark support
	var  lastEntityNavigationWindow = null     // try using this so that if a popup is opened, it continues to be used
	// WAB 2008/06/17 needs to be a global variable, so moved back here and added another check to see if window still open
	
	function viewEntity (entityTypeID, entityID, screenID, returnFunction, options)  {
		  var screenID = (screenID == null) ? '' : screenID;   //  if undefined/null use ''
		 
		  if (!options) options = [];
		if (!options.openInOwnTab ) {		
			if (lastEntityNavigationWindow && !lastEntityNavigationWindow.closed && lastEntityNavigationWindow.loadNewEntity)		{
				result = lastEntityNavigationWindow.loadNewEntity (entityTypeID = entityTypeID, entityID = entityID, screenID = screenID,refresh = true,returnFunction = returnFunction)
				if (result) {
					lastEntityNavigationWindow.focus()
				}
				return result	
			}
			if (window.entityFrame) { 
				lastEntityNavigationWindow = window.entityFrame
				window.entityFrame.location.href = '/data/entityFrame.cfm?frmentityTypeID='+entityTypeID+'&frmentityID='+entityID+'&frmEntityScreen='+ screenID
				return true
			}
	
			entityNavigationFrame = findFrameContainingFunction(window,'loadNewEntity',true)
		
			if (entityNavigationFrame) { 
				// WAB 2008/05/07 if we are using tabs, then need to activate the tab that the entityFrame is in (among other things the form won't post otherwise)
				// requires that extExtension.js is loaded, although I check that it is there
				if (typeof(areWeUsingExtUI) != 'undefined' && areWeUsingExtUI()) {
					activateTabContainingThisObject(entityNavigationFrame)
				} 

				entityNavigationFrame.focus()	
				return entityNavigationFrame.loadNewEntity (entityTypeID = entityTypeID, entityID = entityID, screenID = screenID,refresh = true,returnFunction = returnFunction)
			} else { 
					if (typeof(areWeUsingExtUI) != 'undefined' && areWeUsingExtUI()) {
						// if using tabs then open in a new tab
						return openEntityFrameInNewTab (entityTypeID, entityID, screenID,options)
					} else {
				// this will reopen the entity navigation frameSet in a new window called detail
						replaceRegExp = /[\.-]/g
						entityWindowName = 'EntityFramePopup' + window.location.hostname.replace(replaceRegExp,'') // make unique to each site
						checkForEntityNavigationWindow = openWin ('',entityWindowName,'menubar=no,location=no,width=1200,height=800,resizable=yes,scrollbars=yes')  // WAb 2008/07/15 added opening parameters here
				if (checkForEntityNavigationWindow && checkForEntityNavigationWindow.loadNewEntity) {
					lastEntityNavigationWindow = checkForEntityNavigationWindow
					result = checkForEntityNavigationWindow.loadNewEntity (entityTypeID = entityTypeID, entityID = entityID, screenID = screenID,refresh = true,returnFunction = returnFunction)
					if (result) {
						checkForEntityNavigationWindow.focus()
					}
					return result	
				} else {
								lastEntityNavigationWindow =  openWin ('/data/entityFrame.cfm?frmentityTypeID='+entityTypeID+'&frmentityID='+entityID+'&frmEntityScreen='+ screenID,entityWindowName,'menubar=no,location=no,width=1200,height=800,resizable=yes,scrollbars=yes');
								lastEntityNavigationWindow.focus();
					return true
				}
					}
			}
		} else {
			return openEntityFrameInNewTab (entityTypeID, entityID, screenID,options)
		} 
			
			}

	// needed this code in two places so put in a function 
// WAB 2008/07/09 reverts to openWin if non tabbed interface, opens a brand new window for each entity - which may be a bit of overkill
	function openEntityFrameInNewTab (entityTypeID, entityID, screenID,options) {
			var urlToOpen = '/data/entityFrame.cfm';
			var urlParams = '&frmentityTypeID=' + entityTypeID+'&frmentityID='+entityID+'&frmEntityScreen='+ screenID;
			var finalHref = urlToOpen + '?' +urlParams

			if (typeof(areWeUsingExtUI) != 'undefined' && areWeUsingExtUI()) {
						if (typeof(options['tabTitle']) == "undefined") {
							options.tabTitle = 'ID ' + entityID;
						}
						if (typeof(options['iconClass']) == "undefined") {
							options.iconClass = 'accounts';
						}
						
					return	openNewTab('ViewEntity',options.tabTitle,finalHref,options);
			} else {
				replaceRegExp = /[\.-]/g
				entityWindowName = 'EntityFramePopup' + window.location.hostname.replace(replaceRegExp,'') + '_' + entityTypeID  + '_' +  entityID // make unique to each entity
				newWin =  openWin (finalHref,entityWindowName,'menubar=no,location=no,width=1200,height=800,resizable=yes,scrollbars=yes')
				newWin .focus()
			}
	}
	
	
	function viewPerson (personID, screenID, options) {
		return viewEntity (0, personID, screenID,null,options)  	  	
	}
		
	function viewLocation (locationID, screenID, options) {
		return  viewEntity (1, locationID, screenID,null,options)  	  	
	}

	function viewOrganisation (organisationID, screenID, options) {
		return viewEntity (2,organisationID,screenID,null,options)  	
	}

