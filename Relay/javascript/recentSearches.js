/* �Relayware. All Rights Reserved 2014 */
/*
2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
*/
function toggleRecentSearches(actn) {
	switch(actn){
		/*case "hide":
			jQuery("#recentSearchs").hide();
			return false;
			break;*/
		case "show":
			jQuery("#recentSearchs").show();
			return false;
			break;
	}
}

function loadRecentSearches(top) {
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=siteSearch&methodName=getRecentSearches&returnFormat=json',
        	data:{top:top},
        	dataType:'json',
        	error: function() {},
        	success: function(data,textStatus,jqXHR) {

				var searchOutput = "<ol>";
				for (var c=0;c < data.RESULTS.length;c++) {

					searchOutput += '<li><a href="" onclick="toggleRecentSearches(\'hide\');return loadRecentSearch(\'' + data.RESULTS[c].searchString['JSStringFormat'] + '\');" title="">' + data.RESULTS[c].searchString['HTMLEditFormat'] + '</a></li>';

				}
				searchOutput += "</ol>";

				jQuery("#recentSearchList").html(searchOutput);


			}
	});
}

jQuery(document).ready(function() {	
	recentSearchResultsNumber = 6;
	loadRecentSearches(recentSearchResultsNumber);
});			

