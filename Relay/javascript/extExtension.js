/* �Relayware. All Rights Reserved 2014 */
/*

Functions used with the Relay Ext UI

NM / WAB Jan 2008
 WAB 2008/09/17   altered openNewTab so that reverts to open win if no ext tabs available
2008/10/28	WAB/NH   Added function for opening tab by name (Bug Fix T-10 Issue 1)

*/

function openNewTab(tabName,tabTitle,href,options) {

	ext = getReferenceToUIExt()
	// WAB 2008/09/17   altered so that reverts to open win if no ext tabs available

	if (ext) {
	var frameName = ext.addTab(tabName,tabTitle,href,options);
	//alert(extParent.document.getElementById(frameName));
	//return extParent.document.getElementById(frameName);
	return frameName;
	} else {
		openWin( href, tabName, '' )
		return tabName
	}	
}


function getReferenceToUIExt () {

	var thisFrame = parent;
	// check for existence of centerTabs to show that this is the correct Ext instance
//	while (typeof(thisFrame.Ext) == "undefined"  || typeof(thisFrame.Ext.centerTabs) == "undefined" || incr == 15) {
	while (thisFrame && !(thisFrame.Ext && thisFrame.Ext.centerTabs) && thisFrame != thisFrame.parent) {
		thisFrame = thisFrame.parent;
	}
	
	 return (thisFrame.Ext && thisFrame.Ext.centerTabs ) ?	thisFrame.Ext : false    // WAB 2008/03/27 change here, was picking up incorrect instances of Ext 

}

var  relayUIExt = getReferenceToUIExt ()


function getReferenceToUIExtWindow () {
// useful if you want to run a function in the top window

	var thisFrame = parent;
	// check for existence of centerTabs to show that this is the correct Ext instance
//	while (typeof(thisFrame.Ext) == "undefined"  || typeof(thisFrame.Ext.centerTabs) == "undefined" || incr == 15) {
	while (thisFrame && !(thisFrame.Ext && thisFrame.Ext.centerTabs) && thisFrame != thisFrame.parent) {
		thisFrame = thisFrame.parent;
	}
	

	return (thisFrame.Ext && thisFrame.Ext.centerTabs)?	thisFrame : false
	

}

var  relayUI = getReferenceToUIExtWindow ()



// gets Reference to current tab IFrame, or if obj is passed then the tab IFrame that that object is in
function getReferenceToTabIFrame (obj) {

	/*
		WAB 2008/11/10 original function replaced with one within applayout.js
	var thisFrame = (obj) ? obj.window: window;
	var regExp = /tabIFrame_/ 


	// check for existence of centerTabs to show that this is the correct Ext instance
	while (	!regExp.test(thisFrame.name) && thisFrame != thisFrame.parent) {
		thisFrame = thisFrame.parent;
	}
	
	return thisFrame
	*/
	
	return relayUIExt.getReferenceToTabIFrame((obj) ? obj: window) 

}

// gets Reference to current tab, or if obj is passed then the tab that that object is in
// this is the ext object which has information about the tab
// to get the element (LI) which displays the tab bar you need something like tabElement = ext.centerTabs.getTabEl(getReferenceToThisTab ())
// to get the element (DIV) which holds the iframe you need ... 

function getReferenceToThisTab (obj) {
	ext = getReferenceToUIExt();
	iframename = getReferenceToTabIFrame(obj).name;	
	tabname = iframename.replace('tabIFrame_','theTab_');
	return  ext.centerTabs.getItem(tabname);
}

function activateTabContainingThisObject (obj) {
		tabID = getReferenceToThisTab(obj).id
		ext = getReferenceToUIExt()
		ext.centerTabs.activate(tabID)		
}



function updateTab (options) {
	
	
	theTab = getReferenceToThisTab ()
	if (options.tabTitle) theTab.setTitle(options.tabTitle)

	if (options.tabTip) {
		//  alert (Ext.fly(theTab).child)
		// alert (Ext.fly(theTab).child('span.x-tab-strip-text', true))
		 //  Ext.fly(theTab).child('span.x-tab-strip-text', true).qtip = options.tabTip;
//			alert (options.tabTip)
		//alert (theTab.setTooltip)
	//		theTab.setTooltip(options.tabTip)
	}		

	
	
}

function areWeUsingExtUI () {
	if (getReferenceToUIExt()){
		return true
	} else {
		return false

	}

}


// 2008/10/28 WAB/NJH created for T-10 Bug Fix Issue 1
function openTabByName (tabName, urlParams) { 

	topframe = getReferenceToUIExtWindow()
	topframe.openTabByName (tabName,urlParams)
}


function showLoadingIcon () { 
	
	/*tabID = getReferenceToThisTab()
				tabObject = relayUIExt.centerTabs.getItem(tabID)
					alert (tabObject)
			currentClass = tabObject.iconCls

					tabElement = relayUIExt.centerTabs.getTabEl(tabObject)
					relayUIExt.fly(tabElement).child('.x-tab-strip-text').replaceClass(currentClass, 'loadingIcon');
					tabObject.setIconClass(iconClass);  // Thought that this function would do the rendering, but doesn't seem to - hence the line above which actually changes the class.  This function just keeps the iconClass sored in the JS structure in line with what is happening on the ground
*/

	relayUIExt.showLoadingIcon (window)

}


