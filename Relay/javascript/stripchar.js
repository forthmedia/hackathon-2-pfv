/* �Relayware. All Rights Reserved 2014 */
//PPB 2010-04-12 

//this function removes ALL occurrences of a specified character from a specified string

function stripChar(stringToStrip, char) {
	while (stringToStrip.indexOf(char) > 0)
	{
		stringToStrip = stringToStrip.replace(char,"");	
	}

	return stringToStrip
}
