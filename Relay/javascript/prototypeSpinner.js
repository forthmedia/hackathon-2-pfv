/* �Relayware. All Rights Reserved 2014 */
/*
WAB 2013-01  (In the dying days of prototype, - I also have an analagous jquery one)
Function for easily displaying/removing a spinner from any object
Fed up with needing to craft indivual spinners for every need
Somewhat experimental, but was used on communication pages





*/

		function spinner (obj,passedoptions) {

			// Test whether object already has a loadingDiv
			if (!obj.spinnerDiv) {
					// default options
					defaultOptions = {position:'center',size:'small'}
					// for an anchor the default is left
					if(obj.tagName == 'A') {defaultOptions.position = 'left'}

					// for large object then default to large icon
					var dimensions = obj.getDimensions()
					if (dimensions.width > 100 && dimensions.height > 100) {defaultOptions.size = 'large'}

					// combine the default and passed options
					Object.extend( defaultOptions, passedoptions || {})  ;
					options = defaultOptions	

					if (options.size == 'large') {
						var img = {url:'/images/misc/ajax-loading.gif',size:60}
					} else {
						var img = {url:'/images/icons/loading.gif',size:16}
					}
					
					// where are we going to create the spinner div
					// Inside for div,a
					// After for input,textarea

				if (obj.tagName == 'BODY') {
					wrap = obj
				} else {
					wrap = Element.wrap(obj,'span')
				}

					// create the div
//					divHTML = '<div style="position:absolute;z-index:1;top:0;left:0;" class="spinnerInner"><img src="'+img.url+'"></div>'
					divHTML = '<div class="spinnerOuter" style="position:relative;display:inline;"><div style="position:absolute;z-index:1;top:0;left:0;" class="spinnerInner"><img src="'+img.url+'"></div></div>'

						Element.insert(wrap,{  top: divHTML}); 


/*					if (obj.tagName == 'INPUT' || obj.tagName == 'TEXTAREA' ) {
						Element.insert(obj,{  after: divHTML}); 
						spinnerDiv = obj.next()
					} else {
						var spinnerDiv = obj.firstChild
					}	
*/	

						spinnerDiv = wrap.firstChild
						spinnerDivInner = spinnerDiv.firstChild

	//				spinnerDiv = $(spinnerDiv)
					objPosition = obj.cumulativeOffset()
					obj.spinnerDiv = spinnerDiv										

					spinnerDivPosition = spinnerDiv.cumulativeOffset()
					relativePosition = {top:spinnerDivPosition.top- objPosition.top , left:spinnerDivPosition.left - objPosition.left }


					// work out where to position the image
					if (options.position == 'left') {
						positionRelativeToTopLeft = {left:-img.size -2 ,top:0}
					} else if (options.position == 'right') {
						positionRelativeToTopLeft = {left:dimensions.width + 2 ,top:0}
					} else {
						// ie center
						positionRelativeToTopLeft = {left:(dimensions.width /2 - img.size/2),top:(dimensions.height/2 - img.size/2)} 
					}	
						shift = {top:(-relativePosition.top + positionRelativeToTopLeft.top),left:(-relativePosition.left + positionRelativeToTopLeft.left )}

				/*	
						console.log(spinnerDivPosition)
						console.log(objPosition)
						console.log (obj.tagName)
						console.log (options)
						console.log ('dimensions' )
						console.log (dimensions)
						console.log ('relativePosition ' )
						console.log (relativePosition )
						console.log ('image positionRelativeToTopLeft ')
						console.log (positionRelativeToTopLeft)
						console.log ('shift ')
						console.log (shift.top)
				*/	

					shift = {top:(-relativePosition.top + positionRelativeToTopLeft.top),left:(-relativePosition.left + positionRelativeToTopLeft.left )}
					spinnerDivInner.style.marginLeft = shift.left + 'px' //(-positionRelativeToTopLeft.left) + 'px' //(relativePosition.left + positionRelativeToTopLeft.left) + 'px'
					spinnerDivInner.style.marginTop = shift.top + 'px' // (relativePosition.top + positionRelativeToTopLeft.top) + 'px'
			
				}	

			return obj.spinnerDiv
		}

		function removeSpinner (obj,passedoptions) {

				options = {force:true}
				Object.extend( options, passedoptions || {})  ;
				if (obj.spinnerDiv) {
						var spinnerDiv = obj.spinnerDiv
						// possibly check that this is the correct div
						spinnerDiv.remove()	
						obj.parentNode.replace(obj)	
						obj.spinnerDiv= null

				}
		}


Element.addMethods({

  spinner : function (element, options) {spinner (element,options); return element }

  ,

  removeSpinner : function (element, options) {removeSpinner (element,options); return element }

 });


