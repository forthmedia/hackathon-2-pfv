/* �Relayware. All Rights Reserved 2014 */
/* 

 	DAM 08/06/2001 - amendment to consider object type file in the same manner as object type text.
					I don't think this is correct but it does let us get the file upload mandatory
					functionality ready for In Focus
					At a later date this needs to be revisited.
  	WAB 2010/06   Added password type
 	2012-12-05 PPB Case 429559 set a textarea dirty (esp if in CKEDITOR) 
 	2013-02-20	WAB	Sprint 15&42 Comms - added support for custom isDirty functions on elements
	2013-06-17	WAB	Added a new way of dealing with isDirty on hidden fields since the .defaultValue method no longer seems to work (if it ever did)
	2013-09-03	NJH Case 436844 - check for hidden fields required as well, as the twoSelectsCombo deals with a hidden field..
	2014-10-27  WAB Core-892 altered checkObject and isDirty to deal with HTML5 input fields with various types.  rather than testing for type=text, look for tagName=input and type not submit/reset/image/button ...
	2015-03-02 	WAB Corrected problem in isDirty function from work of 2014-10-27.  This replaces a couple of other fixes to the problem with the way that was originally intended
*/

		


/*
	checkObject()
		javascript function which returns details of whether an object has been selected / filled in 
	  	takes single parameter which is the object in question (eg document.myForm.myObject)
  		returns a number which signifies:
  			either the number which have been selected (checkbox, radio, select, multiselect)
			or the length of text in a text field

*/
var nonTextInputTypesRegExp = /image|submit|button|reset|radio|checkbox/i

function checkObject(thisObject) {

		if (thisObject.type == 'select-one') {
		
			selected = 0

			if (thisObject.selectedIndex != -1 && thisObject.options[thisObject.selectedIndex].value != '') {
				
				selected = 1
			}
			
			return selected

		} else if (thisObject.type == 'select-multiple') {

			numberSelected = 0
			
			if (thisObject.selectedIndex == -1 )  {
				// nothing selected
				
			} else {
				// loop through counting number selected
				for (i=thisObject.selectedIndex;i<thisObject.length;i++)  {
					if (thisObject.options[i].selected == true) {						
						numberSelected++		
					}
				
				}
			
			}

			return numberSelected


		} else if (thisObject.type == 'checkbox' || thisObject.type == 'radio') {

			// single checkbox or radio
			numberChecked = 0
			

				if (thisObject.checked == true && thisObject.value != '') {
					numberChecked ++
				}

				return numberChecked
				
				
		} else if (thisObject.length > 1) {
			// checkboxes or radio buttons 
			numberChecked = 0				
				for (j=0; j<thisObject.length; j++){

					if (thisObject[j].checked == true && thisObject[j].value != '') {
						numberChecked ++
					}

				}
				
				return numberChecked

		} else if (thisObject.type == 'textarea'|(thisObject.tagName == 'INPUT' &&  !nonTextInputTypesRegExp.test(thisObject.type))) {
			/* textAreas and all INPUTs with a simple value (previously tested for a list of types, but adjusted to deal with HTML5 types by excluding ones we don't want) 
				note that strip is prototype's trim function
			*/		

			return thisObject.value.strip().length
	
		} 

}





function isItemInCheckboxGroupChecked (thisObject,thisValue)  {
// for checking whether a particular item in a group of checkboxes is checked
// takes the object and the value to check for
// also works with radio buttons

	if (thisObject.type == 'checkbox' || thisObject.type == 'radio') {
		// single checkbox or radio
			if (thisObject.checked == true && thisObject.value == thisValue) {
				return true
			} else {
				return false
			}
	} else {
			for (j=0; j<thisObject.length; j++){
				// WAb 2008/06/02 checks for both selected and checked so that works with radio/checkbox or selects
				if ((thisObject[j].selected == true||thisObject[j].checked == true) && thisObject[j].value == thisValue) {
					return true
				}
			}
			return false;
	}
}


/*
isdirty ()
Works out whether anything has changed on the given form
WAB 2008/01/28 copied from somewhere!
WAB 2011/02/02 added a debug parameter and a debug message
WAB 2011/06/28 added regExp and checkHidden (for phrasewidget) had to change to an option collection - checked that no live code was using the debug parameter

*/

function isDirty(form,options) {

		options =  options || { }	
		var defaultOptions = {checkHidden:true,debug:false}
		for (var property in options) {
    		defaultOptions[property] = options[property]
		}	
		options = defaultOptions
	
    var dirty = false;    
	/*these variables just for debug */
	var debugMessage = ''

	for(var i=0, len=form.elements.length; i<len; i++)
		{        
			var element = form.elements[i];        
			var type = element.type;
			if (element.name != '' && (!options.regExp || options.regExp.test(element.name))) {

			if(type == "button" || type == "submit" || type == "reset")	{ 
				           continue;        
			}  
			
				/* WAB 2013-02-20 Added support for a customised isDirty function on an element - specifically for twoSelectsComboQuery where the items in the multiselect are not selected until submit time
					Function can be defined either as an attribute isDirtyFunction (text) or as a an actual isDirty function extending the object */
				if ((isDirtyFunctionText = element.getAttribute('isDirtyFunction') )!= null  || (element.isDirty)) {
					if (isDirtyFunctionText != null) {
						// if function is defined as an attribute, then create the actual function
						element.isDirty = new Function (isDirtyFunctionText) // note that by creating the function in the element object, the 'this' scope is available in the function
					}
					
					// now call the isDirty() function
					if ((dirty = element.isDirty())) {
						break;
					}
	
				} else if ( ( element.tagName == 'INPUT' && !nonTextInputTypesRegExp.test(type) && type != "hidden"   ) || type =="textarea" ) {            
					/* all text type fields including (WAB 2014-10-27 HTML5) except hidden (which is dealt with below) */
					if(element.defaultValue != element.value) {                
						debugMessage  = element.defaultValue + ' -> ' +element.value
						dirty = true;
						break;            
					}   
				
					//2012-12-05 PPB Case 429559 START - warn the user if moving away from a dirty tab in edit communication
					if (type =="textarea" && typeof(CKEDITOR) != 'undefined'  &&  (ckInstance = CKEDITOR.instances[element.id]) ) { 
	                     if (ckInstance.checkDirty ()) {   // do this before updating the linked field.  Sometimes seems to inexplicably go dirty after the update
	                          dirty = true
	                          break;            
	                    }
	        		}
					//2012-12-05 PPB Case 429559 END
					     
				} else if(type == "checkbox" || type == "radio")        {
					if(element.checked != element.defaultChecked)     {
						debugMessage  = element.value + ((element.checked) ? ' checked' : ' unchecked')
						dirty = true;                
						break;            
					}        
				} else if(type == "select-one" )    {            
						// for these I ignore option 0, because often the first item isn't selected by default and ends up showing as a change
						// WAB - above should be OK because any change should also show up as something else being selected.  
					for(var j=1,len1=element.options.length; j<len1; j++)   {
						if(element.options[j].selected != element.options[j].defaultSelected){ 
							debugMessage  = element.options[j].text + ((element.options[j].selected ) ? ' selected' : ' unselected')
							dirty = true;
						    break;               
						}           
					}            
					if(dirty){                
						break;       
					}        
	
	
				} else if(type == "select-multiple")    {            
					for(var j=0,len1=element.options.length; j<len1; j++)   {
						if(element.options[j].selected != element.options[j].defaultSelected){ 
							debugMessage  = element.options[j].text + ((element.options[j].selected ) ? ' selected' : ' unselected')
							dirty = true;
						    break;               
						}           
					}            
					if(dirty){                
						break;       
					}        
	
				} else if(options.checkHidden && type == "hidden") {      // WAB 2009/06/02 added hidden, but only do if regular expression specified
					if(element.defaultValue != element.value) {                
						dirty = true;                
						break;            
					}        
					/* 2013-06-17	WAB	Added a new way of dealing with isDirty on hidden fields since the .defaultValue method no longer seems to work (if it ever did)
						check for an attribute data-defaultvalue (which has to be set in code, see eg twoSelectsCombo.js)
					 */
					if(element.getAttribute('data-defaultvalue')!=null && element.getAttribute('data-defaultvalue')  != element.value) {                
						dirty = true;                
						break;            
					}        
	
	
				}
			
			}    
		}    
		if (dirty  && options.debug) {console.log ('Form Dirty: ' + element.name + ': ' + debugMessage)}
	return dirty;
	
	}

// When using ajax we sometimes save forms and then want to see if they are still dirty
// This resets the defaultValue of all elements on the form.  It seems to work even though defaultValue is sometimes documentes as readOnly

function setFormNotDirty(form,options) {
	
		options =  options || { }	
		var defaultOptions = {checkHidden:true,debug:false}
		for (var property in options) {
    		defaultOptions[property] = options[property]
		}	
		options = defaultOptions
	
	
	for(var i=0, len=form.elements.length; i<len; i++)
		{        
			var element = form.elements[i];        
			var type = element.type;

			if (!options.regExp || options.regExp.test(element.name)) {

				if(type == "button" || type == "submit" || type == "reset")	{ 
					continue;        
				}        

				if(type == "text" || type =="password" ) {
					element.defaultValue = element.value
				} else if(type == "checkbox" || type == "radio")        {
					element.defaultChecked = element.checked 
				} else if(type == "select-one" )    {            
					for(var j=0,len1=element.options.length; j<len1; j++)   {
						element.options[j].defaultSelected = element.options[j].selected   
					}            
	
				} else if(type == "select-multiple")    {            
					for(var j=0,len1=element.options.length; j<len1; j++)   {
						element.options[j].defaultSelected = element.options[j].selected 
					}            

				} else if(options.checkHidden && type == "hidden") {      
					element.defaultValue = element.value
				}
			
			}    
		}
	}

