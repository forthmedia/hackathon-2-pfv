var scrollBarFadeInOutInitialised = ['discussionGroups','discussionMessages','discussionComments'];

function showDiscussionGroups(more,searchString,pageNum,recordsPerPage,eid,discussionGroupIDs) {
	
	page = '/webservices/callWebService.cfc?'
	
	jQuery.ajax({
		type: "GET",
		url: page,
		data: {method:'callWebService',webServiceName:'discussions',methodName:'getHTMLForDiscussionGroups',discussionGroupIDs:discussionGroupIDs,pageNum:pageNum,searchString:searchString,recordsPerPage:recordsPerPage,eid:eid,returnFormat:'plain',_cf_nodebug:true},
		dataType: "html",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			if(more){
				jQuery('.showMore').parent().append(data);
			}else{
				jQuery('#discussionGroupsData').parent().html(data);
			}
			
			//if container is empty shrink it!
			var childElements = jQuery("#discussionGroups").find(".message");
			var totalHeight = 0;
			jQuery(childElements).each(function(){
				totalHeight = totalHeight + jQuery(this).outerHeight(true);
			});
			var parentElementHeight = jQuery("#discussionGroups").outerHeight(true);
			
			if (jQuery(window).width() > 767) {
				if(totalHeight < parentElementHeight){
					jQuery("#discussionGroups").height(totalHeight);
				}else{
					if (!scrollBarFadeInOutInitialised['#discussionGroups']) {
						initialiseScrollBarForDiscussions('#discussionGroups');
					} else {
						jQuery('#discussionGroups').jScrollPane()
					}
				}
			}
			setExpander(); // need to call this here again for the new content that have been displayed
		}
	})
}

function showDiscussionMessages(more,searchString,pageNum,recordsPerPage,eid,discussionGroupID,discussionMessageID,showVote) {
	
	page = '/webservices/callWebService.cfc?'
	
	jQuery.ajax({
		type: "GET",
		url: page,
		data: {method:'callWebService',webServiceName:'discussions',methodName:'getHTMLForDiscussionMessages',discussionMessageID:discussionMessageID,discussionGroupID:discussionGroupID,pageNum:pageNum,searchString:searchString,recordsPerPage:recordsPerPage,eid:eid,showVote:showVote,returnFormat:'plain',_cf_nodebug:true},
		dataType: "html",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			if(more){
				jQuery('.showMore').parent().append(data);
			}else{
				jQuery('#discussionMessagesData').parent().html(data);
			}
			
			//if container is empty shrink it!
			var childElements = jQuery("#discussionMessages").find(".message");
			var totalHeight = 0;
			jQuery(childElements).each(function(){
				totalHeight = totalHeight + jQuery(this).outerHeight(true);
			});
			var parentElementHeight = jQuery("#discussionMessages").outerHeight(true);
			
			if (jQuery(window).width() > 767) {
				if(totalHeight < parentElementHeight){
					jQuery("#discussionMessages").height(totalHeight);
				}else{
					if(discussionMessageID == ''){
						if (!scrollBarFadeInOutInitialised['#discussionMessages']) {
							initialiseScrollBarForDiscussions('#discussionMessages');
						} else {
							jQuery('#discussionMessages').jScrollPane()
						}
					}
				}
			}
			setExpander(); // need to call this here again for the new content that have been displayed
		}
	})
}

function showDiscussionComments(more,searchString,pageNum,recordsPerPage,eid,discussionMessageID,discussionGroupID) {
	
	page = '/webservices/callWebService.cfc?'
	
	jQuery.ajax({
		type: "GET",
		url: page,
		data: {method:'callWebService',webServiceName:'discussions',methodName:'getHTMLForDiscussionComments',discussionMessageID:discussionMessageID,discussionGroupID:discussionGroupID,pageNum:pageNum,searchString:searchString,recordsPerPage:recordsPerPage,eid:eid,returnFormat:'plain',_cf_nodebug:true},
		dataType: "html",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			if(more){
				jQuery('.showMore').parent().append(data);
			}else{
				jQuery('#discussionCommentsData').parent().html(data);
			}
			//if container is empty shrink it!
			var childElements = jQuery("#discussionComments").find(".comment");
			var totalHeight = 0;
			jQuery(childElements).each(function(){
				totalHeight = totalHeight + jQuery(this).outerHeight(true);
			});
			var parentElementHeight = jQuery("#discussionComments").outerHeight(true);
			
			if (jQuery(window).width() > 767) {
				if(totalHeight < parentElementHeight){
					jQuery("#discussionComments").height(totalHeight);
				}
				if (!scrollBarFadeInOutInitialised['#discussionComments']) {
					initialiseScrollBarForDiscussions('#discussionComments');
				} else {
					jQuery('#discussionComments').jScrollPane()
				}
			}
			setExpander(); // need to call this here again for the new content that have been displayed
		}
	})
}


function initialiseScrollBarForDiscussions(divID) {
	if (jQuery(window).width() > 767) {
	scrollBarFadeInOutInitialistion(divID);
	scrollBarFadeInOutInitialised[divID] = true;
	}
}
function initialiseJscroll(divID,nextSelector) {
	jQuery(divID).jscroll({
		loadingHtml: '<img src="/images/icons/loading.gif" alt="Loading"/>',
		   padding: 40,
		   nextSelector:nextSelector
	});
}