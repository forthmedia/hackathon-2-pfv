/* �Relayware. All Rights Reserved 2014 */

function getLoginWebServicePath (methodName) {

	result  = '/webservices/callWebservice.cfc?wsdl&returnFormat=json&webservicename=user&method=callWebservice&_cf_nodebug=true&methodname=' + methodName
	return result
	
}
		

function doLogin() {
	
	form = $('LoginForm')
	$('message').show();
	$('loginMessage').innerHTML = phr.login_attemptingToLogin;
	
	parameters = form.serialize()
	page = getLoginWebServicePath ('doLogin')
		var myAjax = new Ajax.Request(
				page, 
				{
					method: 'post', 
					parameters:  parameters, 
					asynchronous: true,
					onComplete: function  (requestObject,JSON) { 
							json = requestObject.responseText.evalJSON(true)							
									isPasswordExpired = (json.REASON.toLowerCase() == 'passwordexpired')			
										//msg = '';
										msg=json.MESSAGE;
										if (json.ISOK || isPasswordExpired) {
											if (json.PASSWORDEXPIRYWARNING || isPasswordExpired)
												{
													// WAB 2016-07-16 PROD2016-1419 Need to update the XSRF token on this page
													updateRWSessionToken (json._RWSESSIONTOKEN);

													// show change password rows, hide password rows
													// if just a warning then can show a continue link
													$('UsernameRow').hide();$('PasswordRow').hide();
													$('forgottenPW').hide();
													$('loginMessage').innerHTML = json.MESSAGE;
													
													$('NewPasswordRow1').show();$('NewPasswordRow2').show();$('NewPasswordRow3').show();$('changePasswordLinks').show();
													$('UsernameRow').hide();$('PasswordRow').hide();$('loginLinks').hide()
							
													if (isPasswordExpired) {
														$('changePasswordContinueLink').hide()
														$('frmNewPassword1').focus();
													} else {
														$('changePasswordContinueLink').show()
													}
							
												} else {
													$('loginMessage').innerHTML = phr.login_AuthenticatedPleaseWait;
													window.location.href = removeUnwantedURLParameters(window.location.href);
												}
										}
					
										if (msg !='') {
											$('message').show();
											$('loginMessage').innerHTML = msg;
										}
							}

				}
			)
}

function validatePassword() {
	form = $('LoginForm')

	if (isBlank(stripChar(form['frmNewPassword1'].value," "))) {
		alert(phr.login_pleaseEnterNewPassword);
		form['frmNewPassword1'].focus();
		return false;
	}

	if (form['frmNewPassword2'].value == '' && form['frmNewPassword1'].value != '') {
		alert(phr.login_pleaseConfirmPassword)
		form['frmNewPassword2'].focus();
		return false;
	}

	if (form['frmNewPassword1'].value != form['frmNewPassword2'].value) {
		alert (phr.login_passwordsMustMatch)
		form['frmNewPassword2'].focus();
		return false
	}
	
	// check that passwords 1 and 2 match and are complex enough
	// checking if already used is one later
	page = getLoginWebServicePath ('validatePasswordStrength') 
	parameters ="password=" + encodeURIComponent(form['frmNewPassword1'].value)
		var myAjax = new Ajax.Request(
				page, 
				{
					method: 'post',    
					parameters:  parameters, 
					asynchronous: false
				}
			)
				
	json = myAjax.transport.responseText.evalJSON()			
	if (!json.ISOK) { 
		$('passwordStrengthIndicator').innerHTML = json.MESSAGE									
		alert (phr.login_passwordIsNotComplexEnough);
		form['frmNewPassword1'].focus();
		return false
	}
	
	return true
}


function updatePasswordWithOrig() {
	if (validatePassword()) {
		$('LoginForm').submit();
	}
}

function updatePassword () {

	if (validatePassword()) {
		form = $('LoginForm')
		
		parameters = form.serialize()
		page = getLoginWebServicePath ('updatePassword')  
		
			var myAjax = new Ajax.Request(
					page, 
					{
						method: 'post',    
						parameters:  parameters, 
						asynchronous: false
					}
				)
					
		json = myAjax.transport.responseText.evalJSON()			

			if (json.ISOK) {
					msg = phr.login_passwordChangedPleaseWait
				if (json.ISLOGGEDIN){
					// if they are already logged (must have been just a warning of password expiry) we can reload the page
						window.location.href = removeUnwantedURLParameters(window.location.href)
				} else {
					// if they aren't already logged in we can resubmit the form using the new password that has just been entered
					form['frmPassword'].value = form['frmNewPassword1'].value
					doLogin()					
				}		
			
			} else {
					msg = json.MESSAGE;
			}
		$('message').show();
		$('loginMessage').innerHTML = msg
	}
}

function validatePasswordStrength (obj) {

	/* to prevent multiple requests in a short space of time (which seem to cause probs on the dev site)
		we wait for a few milliseconds and then check that the value hasn't changed in the meantime
	*/
	var currentValue = obj.value
	setTimeout (
		function ()  {
			if (currentValue == obj.value) {
				page = getLoginWebServicePath ('validatePasswordStrength')  
				parameters ="password=" + encodeURIComponent(obj.value)
	
				var myAjax = new Ajax.Request(
						page, 
						{
							method: 'post',    
							parameters:  parameters, 
							asynchronous: true,
							onComplete: function  (requestObject,JSON) {
								json = requestObject.responseText.evalJSON(true)
								$('passwordStrengthIndicator').innerHTML = json.MESSAGE									
							}
						}
				)
			}	
		}
		,200
	) 

	
}


function removeURLParameter (string,parameter) { 
	var result = string
	// this removes parameter=....   character before parameter is & or ? and after is & or endline.  Value is optional
	var regExpString = '(&|\\?)('+ parameter+'=.*?)(&|$)'
	var regExp = new RegExp(regExpString, "gi")
	var result  = result.replace(regExp,'$1')

	// this deals with parameter without the equals
		regExpString = '(&|\\?)('+ parameter+')(&|$)'
	 	regExp = new RegExp(regExpString, "gi")
		result  = result.replace(regExp,'$1')

	return result
}


function removeUnwantedURLParameters (string) { 
	var result = string
		result = removeURLParameter (result,'flush')
		result = removeURLParameter (result,'logout')
		
	return result	
}

function isBlank (InString) {
	if (InString==null) return (!false)
	if (InString.length!=0)
		return (!true);
	else
		return (!false);
}

function stripChar (InString, StripThis)  {
	var Count;
	var TempChar;
	var OutString;

	OutString="";
	for (Count=0; Count < InString.length; Count++)  {
		TempChar=InString.substring (Count, Count+1);
		if (TempChar!=StripThis)
			OutString=OutString+TempChar;
	}
	return (OutString);
}


/*
WAB 2011/03/30
Added functionality to do login from inside the EXT interface
*/
function doExtLogin (obj,returnFunction) {

		var extLoginDiv = $('extLoginDiv')

		if (!extLoginDiv) {
			page =  getLoginWebServicePath('getExtLoginForm')
			parameters = 'username='+userName
			var myAjax = new Ajax.Request(
					page, 
					{
						method: 'get', 
						parameters:  parameters, 
						asynchronous: true,
						onComplete: function  (requestObject,JSON) { 
								json = requestObject.responseText.evalJSON(true)	;						
								if (json.RELOAD==true){
									window.top.location.href = "/";
								}else{
									theReturnFunction = returnFunction ; // slight cheat here, this is a global variable
									extLoginDiv = document.createElement('div');
									extLoginDiv.id = 'extLoginDiv';		
									height = 265;
									width = 380; //400
						
									node = window.document.body.childNodes[0];
								   	window.document.body.insertBefore(extLoginDiv,node); 
									// note extLoginWindow also a global window
									extLoginWindow = new Ext.Window  ({ 
														applyTo:'extLoginDiv',
										                layout:'fit',
						                				width:width,
						                				height:height,
														bodyCfg: {
														        cls: 'miniLoginBackground',  // Default class not applied if Custom element specified
														        html: json.HTML
														    },
														modal : true,
														draggable : false,
														title: ''
										            })
						
									 extLoginWindow.show();
									 // this is a hack way of getting setting the focus 
									 // couldn't get scripts to work inline
									 // must be some onload parameter, but I couldn't work it out
									 // needed a delay to get it to work though
										window.setTimeout (function (){$('frmPassword').focus()},100)
								}
						}			
					})			
									
		}
						
}


// Checks for enter being pressed and submits the form
function passwordKeypress (event) {

      	event = event || window.event;
		if (event.keyCode == 13) { 
			target = event.target || event.srcElement
			submitExtLoginForm (target.form)
			
		}
		
}

function submitExtLoginForm (form) {

	if (form.frmPassword.value != '') {
		updateExtLoginMessage(phr.login_attemptingToLogin);
		parameters = $(form).serialize(true)
		parameters['frmusername'] = userName
		page = getLoginWebServicePath ('doLogin')
			var myAjax = new Ajax.Request(
					page, 
					{
						method: 'post', 
						parameters:  parameters, 
						asynchronous: false
					}
				)
	
		json = myAjax.transport.responseText.evalJSON()
		
		if (json.ISOK) {
				updateRWSessionToken (json._RWSESSIONTOKEN);
				// NJH CASE 421766 test for existence of tickleJasperServerLogin function
				if (typeof(tickleJasperServerLogin) == 'function')
					tickleJasperServerLogin();
				closeExtLoginWindow();
				theReturnFunction();
		} else {
			updateExtLoginMessage (json.MESSAGE);
		}
	}	
	return false
}

function runFunctionOnEveryFrame (obj,thefunction)  {

	thefunction (obj)
	for (var i=0;i<obj.frames.length;i++) {
			// some iframes are not in our domain, haven't worked out best way to test but this seems to work
			try {
				test = obj.frames[i].document
				runFunctionOnEveryFrame (obj.frames[i],thefunction)				
				}
			catch (err ){
			}		

	}


} 

function updateRWSessionToken(newValue) {

	runFunctionOnEveryFrame (window, 
			
			function (obj) {
				// loop all forms in window
				for (form in obj.document.forms)  {
					// if this form has a _rwsessiontoken field then it needs to be updated
					if (obj.document.forms[form]._rwsessiontoken ) {
							obj.document.forms[form]._rwsessiontoken.value = newValue
					}
				}	
		
				if (obj._rw) {
					obj._rw.sessiontoken = newValue
				}
			}

	)

}


function updateExtLoginMessage (msg) {
	$('loginMessage').innerHTML = msg

}


function closeExtLoginWindow () {
		extLoginWindow.close()	

}


