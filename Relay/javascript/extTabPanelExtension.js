/* �Relayware. All Rights Reserved 2014 */

/*

Functions used with the Relay Ext UI

NM / WAB Jan 2008
 WAB 2008/09/17   altered openNewTab so that reverts to open win if no ext tabs available
2008/10/28	WAB/NH   Added function for opening tab by name (Bug Fix T-10 Issue 1)
 2009/04/21 WAB/NJH		Version 1 S-01011 - pass window object to showLoadingIcon function

*/

function openNewTab(tabName,tabTitle,href,options) {

	ext = getReferenceToExtTabsExt ()
	// WAB 2008/09/17   altered so that reverts to open win if no ext tabs available

	if (ext) {
		var tabName = ext.addTab(tabName,tabTitle,href,options);
		//alert(extParent.document.getElementById(frameName));
		//return extParent.document.getElementById(frameName);
		return tabName;
	}	
}


function getReferenceToExtTabsExt () {

	var thisFrame = parent;
	// check for existence of extTabs_Panel to show that this is the correct Ext instance
//	while (typeof(thisFrame.Ext) == "undefined"  || typeof(thisFrame.Ext.centerTabs) == "undefined" || incr == 15) {
	
	while (thisFrame && !(thisFrame.Ext && thisFrame.Ext.ux.extTabs_TabPanel) && thisFrame != thisFrame.parent) {
		thisFrame = thisFrame.parent;
	}
	 return (thisFrame.Ext && thisFrame.Ext.ux.extTabs_TabPanel ) ?	thisFrame.Ext : false    // WAB 2008/03/27 change here, was picking up incorrect instances of Ext 

}

var  extTabs_Ext = getReferenceToExtTabsExt ()


function showTab (tabName) {

	extTabs_Ext.ux.showTabByName(tabName)
}

function refreshTab (tabName) {

	extTabs_Ext.ux.refreshTab(tabName)
}



