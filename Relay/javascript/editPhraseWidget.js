/* �Relayware. All Rights Reserved 2014 */
/*
editPhraseWidget.js
May 2009
WAB
Code to go with editPhraseWidget.cfm

2011/10/13 WAB LID 7923 need to convert <relay_tag > to {{}} notation before loading into ckeditor
2016/04/04 NJH	448729 - remove stripScripts call when outputing translations.
*/

								var phraseTextidRegExp = /phrupd_(.*?)_[0-9]*_[0-9]*_$/

							function changeLanguage(languageID,countryID,entityTypeID,entityID,editable) {
								var phraseTextElements = getPhraseTextElements (entityTypeID,entityID)   // all the phrase input boxes
								var allPhraseElements = getAllPhraseElements (entityTypeID,entityID)	// includes all the hidden fields (countryid and languageid)
								var underLyingForm = getUnderlyingForm (entityTypeID,entityID)
								var	isThereAFunctionToSaveUnderlyingForm = false ;
						//		phraseElementsSerialised = Form.serializeElements (phraseElements)  // seemed to serialise something else as well, so went back to a long winded method
						
								var phraseElementsSerialised = ''
								var phraseTextIDs = new Array(1)
								var translationsDirty = false
								var underlyingFormDirty = false
								var debug = false
								var confirmSave = false

								
								for (var i=0;i<phraseTextElements.length;i++) {
									var thisElement = phraseTextElements[i]
									
									/*
									WAB 2012-10-23 use a regExp rather than a plain split - to allow for non-entity translations which might have underscores in phraseTextID
									phraseTextIDs[i] = 	thisElement.id.split('_')[1]
									*/
									phraseTextIDs[i] = 	phraseTextidRegExp.exec(thisElement.id)[1]
								
									/* NJH/WAB added the fckEditor 2009/05/05 */


									// check whether this is an ckedit field.  Am guessing that status 0 is loading.  2 seems to be loaded, but not offical!
									if (typeof(CKEDITOR) != 'undefined'  &&  (ckInstance = CKEDITOR.instances[thisElement.id]) ) { 
	
										if (debug) alert ('ckInstance.checkDirty() = ' + ckInstance.checkDirty()) 

										if (ckInstance.checkDirty ()) {   // do this before updating the linked field.  Sometimes seems to inexplicably go dirty after the update
											 translationsDirty = true
											 // alert ('CK is dirty')
										}
									
										// alert ('ckInstance.getData(): ' + ckInstance.getData()	)
										thisElement.value = ckInstance.getData()
										//ckInstance.UpdateLinkedField()
										
									} else if (phraseTextElements[i].defaultValue != phraseTextElements[i].value) {
										if (debug) alert (phraseTextElements[i].id + ' changed from:\n' + phraseTextElements[i].defaultValue + '\nto\n' + phraseTextElements[i].value)
										translationsDirty = true
									}
									
								}

		
						
								for (i=0;i<allPhraseElements.length;i++) {
									thisElement = allPhraseElements[i]
									// if the input box or fck hidden field is disabled, don't submit the value
									if (!thisElement.disabled) {
										
										phraseElementsSerialised += '&' + thisElement.name + '=' + encodeURIComponent(thisElement.value); 
									}
								}
								
									
										page = '/webservices/relayTranslationsWS.cfc?wsdl&_cf_nodebug=true&method=saveTranslationsReturnNewLanguage&returnFormat=JSON'
										parameters = '&phraseTextIDs=' + phraseTextIDs  + '&entityTypeID=' + entityTypeID + '&entityID='  +entityID + '&languageID=' + languageID + '&countryID=' + countryID
			

								if (typeof(editPhraseWidgetUnderlyingFormSave) != 'undefined' ) {
									isThereAFunctionToSaveUnderlyingForm = true
								}


								nonPhraseRegExp = /^((?!phrupd).)*$/
								 underLyingFormDirty = isDirty(underLyingForm,{checkHidden:false,regExp : nonPhraseRegExp,debug:debug})
								if (debug) alert ('underlying form dirty: ' +underLyingFormDirty)									
								if (debug) alert ('has save function ' + isThereAFunctionToSaveUnderlyingForm)
								if (debug) alert ('translations dirty ' + translationsDirty)
								
								if (editPhraseWidgetPromptForSave && (translationsDirty || (isThereAFunctionToSaveUnderlyingForm && underLyingFormDirty))) {
										confirmSave = confirm(editPhraseWidgetMsgSaveChanges)
										if (confirmSave) {
											parameters = parameters + '&' + phraseElementsSerialised  
										}
						
									}
										
								if (isThereAFunctionToSaveUnderlyingForm && confirmSave && underLyingFormDirty) {		
									editPhraseWidgetUnderlyingFormSave()
									setFormNotDirty(underLyingForm,false)
								}

											var myAjax = new Ajax.Request(
												page, 
												{
													method: 'post',    // must be post for the form update to work, but sometimes changes to GET for debug
													parameters:  parameters, 
													onComplete: 	function (requestObject,JSON) {
																			json = requestObject.responseText.evalJSON(true);  // the strip scripts allows me to have a general login required <script> in the web service as well as a JSON version (NJH 2016/04/04 - removed .stripScripts() as unsure why needed and was removing script content for kaspersky)

																			if (!json) {
																				return false
																			} else if (!json.ISOK) {
																				alert (json.ERRORMESSAGE)
																				return false
																			}

						
																			translationTableDiv = document.getElementById('translationTable')
																			translationStringDiv = document.getElementById('translationString')

																				// I am going to start by blanking out the country and language values
																				// this is a precaution in case things break down before the end
																				// don't want to be left with wrong ids for the phrases on screen
																				if ($('phrcountryid')) {
																					$('phrcountryid').value = ''
																					$('phrlanguageid').value = ''
																				}	
																	
																			translationStringDiv.innerHTML = 'Loading'
						
																				
																			// loop through phrase Elements, get
																			for (i=0;i<phraseTextElements.length;i++) {
						
																				phraseElement = phraseTextElements[i]
																				phraseTextID = 	phraseTextidRegExp.exec(phraseElement.id)[1]
																				// phraseTextID = 	phraseElement.id.split('_')[1]
																						
																						
																				// update the language fields
																				// at one point used separate fields for each phrasetextid, but now just using a single set per page, but have left test for others in incase it is used again (eg if more than one language on a page)
																				phraseCountryIDFieldName = phraseElement.id.replace('upd','updcountryid')
																				phraseLanguageIDFieldName = phraseElement.id.replace('upd','updlanguageid')
																				if ($(phraseCountryIDFieldName)) {
																					$(phraseCountryIDFieldName).value = json.COUNTRYID
																					$(phraseLanguageIDFieldName).value = json.LANGUAGEID
																				} else {
																					$('phrcountryid').value = json.COUNTRYID
																					$('phrlanguageid').value = json.LANGUAGEID
																				}	

																				newPhraseValue =  json.TRANSLATIONS[phraseTextID.toUpperCase()]
																				if (typeof(CKEDITOR) != 'undefined'  &&  (ckInstance = CKEDITOR.instances[phraseElement.id]) ) { 
																				// replace <relay_tag > notation with {{ }}	
																				relaytagregExp = 	/(<)([/]?Relay_[^>]*)(>)/ig
																				newPhraseValue = newPhraseValue.replace(relaytagregExp,'{{$2}}')
																				// alert ('Update With New Text: ' + newPhraseValue)

																					ckInstance.setData(newPhraseValue, function()
																					    {
																					        this.resetDirty();    

																					    });
						
																				} else {
																					phraseElement.value =  newPhraseValue;
																				}		
						
																						phraseElement.defaultValue = newPhraseValue  // IE and FF seem to behave differently with repsect to the defaultvalue of the hidden field associated with fck.
																		
																				// set the element to be disabled if necessary.  Code above should deal with fck editor, but don't work!
																				if (json.ISEDITABLE && !phraseElement.getAttribute('permanentlyDisabled')) {
																					phraseElement.disabled = false
																				} else {
																					phraseElement.disabled = true
																				}
						
						
																			}
						
																			if (translationTableDiv) {
																				translationTableDiv.innerHTML = json.OTHERTRANSLATIONSHTML
																			}		
																		
																			if (translationStringDiv) {
																				translationStringDiv.innerHTML = json.LANGUAGEANDCOUNTRYSTRING
																			}	
						
																			// all done so update the language fields
																			if ($('phrcountryid')) {
																				$('phrcountryid').value = json.COUNTRYID
																				$('phrlanguageid').value = json.LANGUAGEID
																			}	
						
																	},
						
													debug:false
												});   
								
							}
							
						

							function deleteLanguage (languageID,countryID,entityTypeID,entityID) {

								phraseTextElements = getPhraseTextElements (entityTypeID,entityID)  
								phraseTextIDs = new Array(1)
								for (i=0;i<phraseTextElements.length;i++) {
									phraseTextIDs[i] = phraseTextidRegExp.exec(phraseTextElements[i].id)[1]
									// phraseTextIDs[i] = 	phraseTextElements[i].id.split('_')[1]
								}
									
							

										page = '/webservices/relayTranslationsWS.cfc?wsdl&_cf_nodebug=true&method=deleteTranslationReturnLanguageBox&returnFormat=JSON'
										parameters = '&phraseTextIDs=' + phraseTextIDs   + '&entityTypeID=' + entityTypeID + '&entityID='  +entityID + '&languageID=' + languageID + '&countryID=' + countryID 	+ '&currentlanguageID=' +$('phrlanguageid').value + '&currentcountryID=' + $('phrcountryid').value
										
											var myAjax = new Ajax.Request(
												page, 
												{
													method: 'get',    // must be post for the form update to work, but sometimes changes to GET for debug
													parameters:  parameters, 
													onComplete: 	function (requestObject,JSON) {
							
																			json = requestObject.responseText.stripScripts().evalJSON(true);  // the strip scripts allows me to have a general login required <script> in the web service as well as a JSON version
						
																			if (!json) {
																				return false
																			} else if (!json.ISOK) {
																				alert (json.ERRORMESSAGE)
																				return false
																			}
						
						
																			translationTableDiv = document.getElementById('translationTable')
						
																			if (translationTableDiv) {
																				translationTableDiv.innerHTML = json.OTHERTRANSLATIONSHTML
																			}		
																		
						
																	},
						
													debug:false
												});   
								
							}
							

							function getPhraseTextElements (entityTypeID,entityID) { 
							
								regExp = new RegExp ('phrupd_.*' + entityTypeID + '_' + entityID + '_$')
								return getElementsByRegularExpression(regExp,'INPUT|TEXTAREA')
							
							}
						
							// finds form name by getting form name of one of the phrase elements
							function getUnderlyingForm (entityTypeID,entityID) { 
							
								return getPhraseTextElements(entityTypeID,entityID)[0].form
							
							}

						
							function getAllPhraseElements (entityTypeID,entityID) { 
							
								regExp = new RegExp ('phrupd.*' + entityTypeID + '_' + entityID + '_$|phrcountryid|phrlanguageid')
								return getElementsByRegularExpression(regExp,'INPUT|TEXTAREA')
							
							}
								
						
							
							/* function addNewLanguageChange (languageID,countryID,entityTypeID,entityID) {
							
								phraseElements = getPhraseElements (entityTypeID,entityID)
								for (i=0;i<phraseElements;i++) {
									phraseElements[i].value = '';
									phraseElements[i].name = phraseElements[i].id + '_' + languageID + '_' + countryID
								}
						
							}
							*/
						
						
							// note that there is also a function  with this name in tableManipulation.js (repeated here to keep weight down, but could lead to problems)
							function getElementsByRegularExpression(regExp,tagName,n) {                         // n is a Node 
								if(!n) n= document
								
								var result = new Array ()
								var tagNameRegExp = new RegExp (tagName)
							    var children = n.childNodes;                // Now get all children of n
							    for(var i=0; i < children.length; i++) {    // Loop through the children
									result = result.concat (getElementsByRegularExpression(regExp,tagName,children[i])) // Recurse on each one
							    }
								
							    if (n.nodeType == 1 && (tagName == undefined || tagNameRegExp.test(n.tagName) ) && regExp.test(n.id)) { /*Node.ELEMENT_NODE*/  // Check if n is an Element
									result.push(n) 
								}
							
								return result
							}
