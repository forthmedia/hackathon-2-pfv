/* �Relayware. All Rights Reserved 2014 */
function getClientTimeZone() {
			// function which is called when a session is initialised 
			// tells server what timezone the user is in
			// domainAndRoot is not defined until a little later in the process, so need to delay running this function for a short while
			dummyImage = new Image();
			currentTime = new Date();
			//2012-07-17 PPB ENP001 remove reference to domainAndRoot so we don't need the timer 
			//theUrl = '\/webservices\/user.cfc?method=setClientTimeOffset&ClientUTCOffsetMinutes=' + currentTime.getTimezoneOffset();
			theUrl = '\/webservices\/user.cfc?method=setClientTimeOffset&ClientUTCOffsetMinutes=' + currentTime.getTimezoneOffset();
			dummyImage.src = theUrl ;
}

//2012-07-17 PPB ENP001 removed reference to domainAndRoot so we don't need the timer and call function directly (was failing intermittently even with timer increased to 9000)
//setTimeout(getClientTimeZone,2000) ;		
getClientTimeZone();