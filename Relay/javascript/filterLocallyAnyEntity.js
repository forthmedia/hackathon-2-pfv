/**
 * 
 */



function loadOperator(operatorContainer,comparisonContainer,fieldValue,entityTypeID) {
	operatorContainer.html("");
	comparisonContainer.html("");

	if(fieldValue != ""){
		jQuery.ajax(
	    	{
	        	type: "GET",
                url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=filterSelectAnyEntityWS&methodName=loadOperator&returnFormat=plain',
	        	data:{'field':fieldValue,'entityTypeID':entityTypeID, 'comparisonContainerId':comparisonContainer.attr('id')},
	        	dataType:'text',
	        	success: function(data) {operatorContainer.html(data);}
	    	});
	}
}


function loadComparison(fieldValue,operatorValue,entityTypeID,comparisonContainer) {
	comparisonContainer.html("");

	if(fieldValue != ""){
		jQuery.ajax(
	    	{
	        	type: "GET",
                url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=filterSelectAnyEntityWS&methodName=loadComparison&returnFormat=plain',
	        	data:{'field':fieldValue,'operator':operatorValue,'entityTypeID':entityTypeID},
	        	dataType:'text',
	        	success: function(data) {comparisonContainer.html(data);}
	    	});
	}
}

function addFilter(filterName) {
	
	//kill any old error messages
	jQuery("#"+filterName+"_operatorMissingError").hide();
	
	//grab the data for the new filter
	
	var newFieldElement=jQuery("#" + filterName + "_filterFieldContainer").find(".filterField");
	var newField=newFieldElement.val();
	var newFieldDisplay=newFieldElement.find("option:selected").text()
	
	var newOperatorElement=jQuery("#" + filterName + "_operatorContainer").find(".operator");
	var newOperator=newOperatorElement.val();
	var newOperatorDisplay=newOperatorElement.find("option:selected").text()
	
	var newComparisonContainerElement=jQuery("#" + filterName + "_comparisonContainer");
	var newComparisonBElement=newComparisonContainerElement.find(".comparisonB");
	var newComparisonCElement=newComparisonContainerElement.find(".comparisonC"); //may or may not exist
	
	// note that newComparisonBElement and newComparisonCElement may simply not exist (be undefined) this is fine, but if they exist they must not be empty
	if (newField!="" && newOperator !="" && newComparisonBElement.val()!="" &&  newComparisonCElement.val()!=""){
		var newComparisonB=""
		if (newComparisonBElement.length!=0){
			newComparisonB=newComparisonBElement.val(); //may be undefined
		}
	
		var newComparisonC="";
		if (newComparisonCElement.length!=0){
			newComparisonC=newComparisonCElement.val(); //may be undefined
		}
		
		var newComparisonBDisplay="";
		if( newComparisonBElement.prop('type') == 'select-one' ){
			newComparisonBDisplay=newComparisonBElement.find("option:selected").text();
		}else{
			newComparisonBDisplay=newComparisonB;
		}
		
		var newComparisonCDisplay="";
		if( newComparisonCElement.prop('type') == 'select-one' ){
			newComparisonCDisplay=newComparisonCElement.find("option:selected").text();
		}else{
			newComparisonCDisplay=newComparisonC;
		}
		
		var combinedComparatorDisplay="";
		if (newComparisonCElement.length!=0){
			combinedComparatorDisplay=newComparisonBDisplay+' and ' + newComparisonCDisplay;
		}else if (newComparisonBElement.length!=0){
			combinedComparatorDisplay=newComparisonBDisplay;
		}	
		//if there is no options (the case for boolean values) combinedComparatorDisplay remains blank
	
		/* Update the html for the addition  */
		var deletefunctionCall="deleteFilter('"+ filterName + "',jQuery(this).closest('tr').index())";
		
		var newRowHTML=	
			'<tr>' +
				'<td>'+newFieldDisplay+'</td>'+
				'<td>'+newOperatorDisplay+'</td>' +
				'<td>'+combinedComparatorDisplay+'</td>'+	
				'<td><a href="##" class="btn btn-primary" onclick='+deletefunctionCall+'>Delete</a></td>'+
			'</tr>'
			;
		
		jQuery('#'+filterName+'_filterEditor').prev('tr').after(newRowHTML);
		
		/* Update the underlying data for the addition  */
		
		var hiddenDataField=JSON.parse(jQuery("#"+filterName).val());
		
		//append the new filter structure
		hiddenDataField.FILTER.push({
									FILTERDATA:{FIELD:newField,OPERATOR:newOperator, COMPARATORB:newComparisonB,COMPARATORC:newComparisonC },
									FILTERDISPLAY:{FIELD:newFieldDisplay,OPERATOR:newOperatorDisplay, COMPARATOR:combinedComparatorDisplay}
									});
		
		
		jQuery("#"+filterName).val(JSON.stringify(hiddenDataField)); //put the data back in the hidden field
		
		
		/* clear the old filter */
		
		jQuery("#" + filterName +'_filterField').val("");
		jQuery("#" + filterName +'_operatorContainer').html("");
		jQuery("#" + filterName +'_comparisonContainer').html("");
	
	}else{
		jQuery("#"+filterName+"_operatorMissingError").show();
	}
}


function deleteFilter(filterName, filterRow) {
	

	
	var hiddenDataField=JSON.parse(jQuery("#"+filterName).val());
	
	
	hiddenDataField.FILTER.splice(filterRow-1, 1); //-1 as rows start at 1 but arrays at 0
	jQuery("#"+filterName).val(JSON.stringify(hiddenDataField)); //put the data back in the hidden field
	
	//jQuery("#"+filterName+"_filterTable").deleteRow(filterIndex);
	
	
	jQuery("#"+filterName+"_filterTable tr:eq("+filterRow+")").remove();
	
}




