/* �Relayware. All Rights Reserved 2014 */

function CheckSQLInput(x,nForm,nURLRoot,nScriptName){

	x = x.toUpperCase()
	if ((x.indexOf("INSERT") >-1) || (x.indexOf("UPDATE") >-1) || (x.indexOf("DELETE") >-1) || (x.indexOf("CREATE") >-1)  || (x.indexOf("ALTER") >-1)|| (x.indexOf("DROP") >-1)){
		alert("Use of SQL keywords such as Insert or Update are illegal on this field.")
		nAction = nURLRoot + "/templates/RecordIllegalInput.cfm?NextTemplate=" + nScriptName
		alert(nAction)
		nForm.action=nAction
		return;
	}
}