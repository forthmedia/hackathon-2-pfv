/* �Relayware. All Rights Reserved 2014 */
//2012-06-22 PPB Case 429018 add replaceNonAlphaNumericChars

//this function replaces ALL Non-AlphaNumeric characters in a specified string with a specified replacement (the default replacement is an empty string)

function replaceNonAlphaNumericChars(stringIn, replacementChar) {

	if (replacementChar==undefined){
		var replacementChar = "";
	}

	var newString = stringIn.replace(/[^a-zA-Z0-9]/g, replacementChar);

return newString;
}
