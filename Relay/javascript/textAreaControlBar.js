/* �Relayware. All Rights Reserved 2014 */
/* NJH 2011/07/25 - had to fix the insertText function for IE 8.. if no text was selected, then the 'Insert Merge' button was updated.
So, had to focus on the textbox first if no selection found. And then had to set the document.selection.createRange.text field rather than the caretPos variable.

2014/11/28 DXC Case 438898 - document.selection deprecated in IE11

*
*
*/

var isSelected = false 

function insertTag ( txtObj, tag, enclose ) {
 var closeTag = tag;
 if ( enclose ) {
   var attribSplit = tag.indexOf ( ' ' );
   if ( tag.indexOf ( ' ' ) > -1 )
     closeTag = tag.substring ( 0, attribSplit );
 }

 if ( isSelected ) {
   if (txtObj.createTextRange && txtObj.caretPos) {
     var caretPos = txtObj.caretPos;
     caretPos.text = ( ( enclose ) ? "<"+tag+">"+caretPos.text+"</"+closeTag+">" : "<"+tag+"/>"+caretPos.text );
     markSelection ( txtObj );
     if ( txtObj.caretPos.text=='' ) {
       isSelected=false;
    txtObj.focus();
     }
   }
 } else {
   alert("insertTag: You must place your cursor in the text area first");
   // placeholder for loss of focus handler

 }
}

function insertRelayTag ( txtObj, textString) {

	textString = replaceCurlyBracketsWithChevrons(textString)
	return  insertText ( txtObj, textString) 	

}

		function replaceCurlyBracketsWithChevrons (text) {
			regexp = /(\{\{)(.*)(\}\})/ ;
			replaceexp = "\<$2\>" ; 

			return text.replace(regexp,replaceexp) ; 
		}	


function insertText ( txtObj, textString) {
 markSelection(txtObj);
 if ( isSelected ) {
   if (txtObj.createTextRange && txtObj.caretPos && document.selection) {
   		 if (document.selection && document.selection.type == 'None' ) {
    		txtObj.focus();
	     }
	     var caretPos = txtObj.caretPos;
		 document.selection.createRange().text = ( textString );
	     
    	//caretPos.text = ( textString+" "+caretPos.text );
    	
   } else if (txtObj.caretPos && txtObj.caretPos.selectionStart)  
   {
	   txtObj.value = txtObj.value.substr(0, txtObj.caretPos.selectionStart)
				    + textString
	 				   + txtObj.value.substr(txtObj.caretPos.selectionEnd, txtObj.value.length);
	   txtObj.setSelectionRange(txtObj.caretPos.selectionStart + textString.length,txtObj.caretPos.selectionStart + textString.length);
   } else if (txtObj.selectionStart)   // WAB trying FF
   {
	   var previouseSelectionStart = txtObj.selectionStart
	   txtObj.value = txtObj.value.substr(0, txtObj.selectionStart)
				    + textString
	 				   + txtObj.value.substr(txtObj.selectionEnd, txtObj.value.length);
	   txtObj.setSelectionRange(previouseSelectionStart + textString.length,previouseSelectionStart + textString.length);
	   txtObj.focus();
   }
   return true
 } else {
 
 	if (txtObj.value == '') {
 		txtObj.value = textString;
 		return true;
 	} else {
   		alert("You must place your cursor in the text area first");
			return false
   		// placeholder for loss of focus handler
   	}
 }
}
				

			function markSelection ( txtObj ) {

				 if ( txtObj.createTextRange && document.selection) { // dxc 2014/11/28 DXC Case 438898
					// 2014/10/15	YMA	IE11 no longer supports document.selection
					 if (document.selection && document.selection.createRange) {
						txtObj.caretPos = document.selection.createRange().duplicate();
				    } else if (window.getSelection && txtObj.selectionStart && txtObj.textContent) {
				        txtObj.caretPos = {'text':txtObj.textContent.substr(txtObj.selectionStart,txtObj.selectionEnd-txtObj.selectionStart),
				        					'selectionStart': txtObj.selectionStart,
				        					'selectionEnd': txtObj.selectionEnd
				        					};
				    }
				   isSelected = true; 
			 	} else if  (txtObj.selectionStart) 				// WAB trying FF
			 	{	
				   	isSelected = true; 
				}

			}
		
		
function openRelayTagPicker (txtObj,tagType,context) {
			url = iDomainAndRoot + '/relayTagEditors/relayTagPicker.cfm?'
			returnFunction = 'opener.insertRelayTag(opener.document.' + txtObj.form.name + '.' + txtObj.name + ',\'*TAGTEXT*\')'
			url += 'returnFunction=' +returnFunction + '&relayInsertType=' + tagType+'&context='+context
			editWindow = window.open(url ,'TagPicker','scrollbars=yes,menubar=no,toolbar=no,resizable=yes,status=yes,width=550,height=650');
			editWindow.focus();
}
		
function quickHTML ( txtObj, txtType) {
	 if ( isSelected ) {
		var thisHTML;
		var replaceSelection = false;
		if (txtObj.createTextRange && txtObj.caretPos) {
			var caretPos = txtObj.caretPos;
			switch(txtType) {
				case 'quickTable': if (caretPos.text != "") {thisHTML = longTable(caretPos.text); replaceSelection = true; break;} else {thisHTML = quickTable(); break;}
				case 'quickUL': if (caretPos.text != "") {thisHTML = longULOL(caretPos.text,'ul'); replaceSelection = true; break;} else {thisHTML = quickUL(); break;}
				case 'quickOL': if (caretPos.text != "") {thisHTML = longULOL(caretPos.text,'ol'); replaceSelection = true; break;} else {thisHTML = quickOL(); break;}
				case 'quickHref': thisHTML = quickHref(); break;
				case 'quickHTMLPage': thisHTML = quickHTMLPage(); break;
				default: thisHTML = "I dunno."; break;
				}

	     	if (replaceSelection) {
	     		caretPos.text = ( thisHTML );
			} else {
				caretPos.text = ( thisHTML+" "+caretPos.text );
			}
	     	markSelection ( txtObj );
	     	if ( txtObj.caretPos.text=='' ) {
	       		isSelected=false;
	    		txtObj.focus();
	     	}
	   	}
	 } else {
	   alert("You must place your cursor in the text area first");
	   // placeholder for loss of focus handler
	 }
}

function trimString(str) {
	var startPoint = 0;
	var endPoint = str.length;
	for (var i=0;i<str.length;i++) {
		if (str.charAt(i) == " " || str.charAt(i) == "\n" || str.charAt(i) == "\r") {
			startPoint = i;
		} else {
			break;
		}
	}
	for (var i=str.length-1;i>=0;i--) {
		if (str.charAt(i) == " " || str.charAt(i) == "\n" || str.charAt(i) == "\r") {
			endPoint = i
		} else {
			break;
		}
	}
	return str.slice(startPoint,endPoint);
}

function longULOL (tText,tagType) {
	if (tagType == undefined) {
		tagType = "ul";
	}
	
 	var newTagText = "<"+tagType+">\n";
	
	for (var x=0;x<tText.length;x++) {
		if (x == 0) {
			newTagText += "\t<li>";
		}
		if (tText.charAt(x) != "\n" && tText.charAt(x) != "\r") {
			newTagText += tText.charAt(x);
		} else if (tText.charAt(x) == "\n") {
			newTagText += "</li>\n\t<li>";
		}
		
		if (x == tText.length-1) {
			newTagText += "</li>";
		}
	}
	newTagText+="\n</"+tagType+">";
	return newTagText;
}

function findColCount(tText) {
	var trArr = new Array();
	var tdAmount = 1;
	var arrCount = 0;
	for (var x=0;x<tText.length;x++) {
		if (tText.charAt(x) == "~") {
			tdAmount++;
		}
		if (tText.charAt(x) == "\n") {
			trArr[arrCount] = tdAmount;
			tdAmount = 1;
			arrCount++;
		}
	}
	var highestNum = 0;
	for (var i=0;i<trArr.length;i++) {
		if (trArr[i] > highestNum) {
			highestNum = trArr[i];
		}
	}
	return highestNum;
}

function longTable(tText) {
	var colAmount = findColCount(tText);
	var newTagText = "<table>\n";
	var trCount = 0;
	var tdCount = 1;
	var finalTD = false;
	for (var x=0;x<tText.length;x++) {
		if (x == 0) {
			newTagText += "\t<tr>\n\t\t<td>";
		}
		if (tText.charAt(x) != "\n" && tText.charAt(x) != "\r" && tText.charAt(x) != "~") {
			newTagText += tText.charAt(x);
		} else if (tText.charAt(x) == "~") {
			newTagText += "</td>\n\t\t<td>";
			tdCount++;
		} else if (tText.charAt(x) == "\n") {
			if (tdCount < colAmount) {
				newTagText += "</td>";
				for (var t=1;t<=colAmount-tdCount;t++) {
					newTagText += "\n\t\t<td></td>";
				}
				newTagText += "\n\t</tr>\n\t<tr>\n\t\t<td>";
			} else {
				newTagText += "</td>\n\t</tr>\n\t<tr>\n\t\t<td>";
			}
			
			trCount = 0;
			tdCount = 1;
		}
		if (x == tText.length-1) {
			if (tdCount < colAmount) {
				newTagText += "</td>";
				for (var t=1;t<=colAmount-tdCount;t++) {
					newTagText += "\n\t\t<td></td>";
				}
				newTagText += "\n\t</tr>";
			} else {
				newTagText += "</td>\n\t</tr>";
			}
		}
	}
	newTagText+="\n</table>";
	return newTagText;
}

function quickHTMLPage () {
 	var quickHTML = "<HTML>";
	quickHTML += unescape('%0D');
	quickHTML += "<head>";
	quickHTML += unescape('%0D');
	quickHTML += "</head>";
	quickHTML += unescape('%0D');
	quickHTML += unescape('%0D');
	quickHTML += "<body>";
	quickHTML += unescape('%0D');
	quickHTML += unescape('%0D');
	quickHTML += "</body>";
	quickHTML += unescape('%0D');
	quickHTML += "</HTML>";
	return quickHTML;
}

function quickHref () {
	var URL = prompt("Enter/paste the URL here", "HTTP://www....");
	var AnchorText = prompt("Enter the text you will click on the link", "Click");
	var fullHRefString = "<a href='" + URL + "'>" + AnchorText + "</a>";
	return fullHRefString;
}

function quickTable () {
 	var quickTable = "<table>";
	quickTable += unescape('%0D');
	quickTable += "  <tr><td></td><td></td></tr>";
	quickTable += unescape('%0D');
	quickTable += "  <tr><td></td><td></td></tr>";
	quickTable += unescape('%0D');
	quickTable += "  <tr><td></td><td></td></tr>";
	quickTable += unescape('%0D');
	quickTable += "  <tr><td></td><td></td></tr>";
	quickTable += unescape('%0D');
	quickTable += "</table>";
	return quickTable;
}

function quickUL () {
 	var quickHTML = "<ul>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "</ul>";
	return quickHTML;
}

function quickOL () {
 	var quickHTML = "<ol>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "  <li></li>";
	quickHTML += unescape('%0D');
	quickHTML += "</ol>";
	return quickHTML;
}


