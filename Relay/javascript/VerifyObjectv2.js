/* �Relayware. All Rights Reserved 2014 */
 // javascript function for verifying whether object selected and returning a message

		var focusChanged=false

		
function verifyObjectv2(thisObject, objectMessage, aParameter) {
	// aParameter is normally 1, but for multiselects, checkboxes it is the number required to be selected
	// mod by wab 20/12/00 so that object is passed as a reference rather than just the name

	// WAB 2012/11/08 CASE 431804 Some code has started passing 'true' whereas this code was expecting a number (because we need to be able to say things like '3 items required')
	//    				So test for 'true'
	if (isNaN(aParameter)) {
		if (aParameter.toString().toLowerCase() == 'true') {
			aParameter = 1
		} else {
			aParameter = 0
		}
	}

		thisResult = ''
		if (checkObject (thisObject) < aParameter)  {
		
			thisResult = objectMessage + '\n'  ;
			if (focusChanged==false ) {
				if (thisObject.type=='text' | thisObject.type=='select-one' | thisObject.type=='select-multiple') {
					thisObject.focus()
					focusChanged = true
				}
			}
		
		}


	return thisResult

}


// verifies that contents of a field aren't too long (specifically for textareas where length can't be set)
function verifyMaxLength(thisObject, objectMessage, maxLength) {
	// checks that object has correct size 
		thisResult = ''


		if (checkObject (thisObject) > maxLength)  {
		
			thisResult = objectMessage + '\n'  ;
			if (focusChanged==false ) {
				if (thisObject.type=='text' | thisObject.type=='select-one' | thisObject.type=='select-multiple') {
					thisObject.focus()
					focusChanged = true
				}
			}
		
		}

		
	return thisResult

}

