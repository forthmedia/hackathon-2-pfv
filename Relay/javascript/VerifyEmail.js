/* �Relayware. All Rights Reserved 2014 */
/*
	2008/09/24 WAB CR-TND561 Req8 lots of rejigging of code and addition of Ajax check for unique email addresses
	2008/11/13 SSS Len bug fix - allowed # character in emails before the @ sign.
	2010/09/21	NJH	LID 3956 Check the org type when doing unique email validation. Skip the check for end customers
 	170100 WAB Added apostrophe
 	WAB 2008/09/17 pulled out into a separate function
	NYB 2012-01-17 Case425166 added else to validateEmail function - for when email is not required and is blank
	2014-02-24 PPB Case 438846 simply searches for an '@' and a subsequent '.' to cope with Cyrillic chars
	2014-08-26 REH Case 441394 Spaces in email addresses
	2015/09/22 DAN Case 445955 - update regexp for e-mail validation
	2016-07-15 DAN Case 450806 - prevent passing "undefined" orgType parameter on email validation
*/
//	RegExp function for validating an email address

function isEmailAddressValid(emailAddress)  {
	
		/* regular expression for testing Email */
		//NumberFormatRe = /^[\w_\.\-']+@[\w_\.\-']+\.[\w_\.\-']+$/
		//NumberFormatRe = /.*@.*\./ 										/* 2014-02-24 PPB Case 438846 simply searches for an '@' and a subsequent '.' to cope with Cyrillic chars; also in cfform.js  */
	
		/*2014-08-26 REH Case 441394 - To discuss with Gawain and roadmap team if we should adopt a relaxed email validation so we can support non-ASCII character email addresses*/
		// 2015/09/22 DAN Case 445955 - update regexp for e-mail validation
		NumberFormatRe =  /^\S+@\S+\.\S{2,}$/                              /* 2014-08-26 REH Case 441394 Spaces in email addresses - simply searches for an '@', a subsequent '.' and not allowing spaces to cope with Cyrillic chars. Also ending with 2 or 3 char */

		return NumberFormatRe.test(emailAddress) 

}


/* These 3 functions now replaced with a backwards compatible version of verifyEmail.  
VerifyEmailMessageV2 not used anywhere!


// Original function which checks email address based on object name - not very widely applicable since name of form is hard coded
// USAGE: verifyEmail('mailTo');  -- NB. pass text string of object name to this function

function verifyEmail(objectName)  {

	thisObject = eval('document.mainForm.'+objectName);
	thisValue = thisObject.value;
	msg = '';	
	
	if (thisValue != '') {
		if (!isEmailAddressValid(thisValue)) {
			msg = 'Please enter an email address of the form abc@def.ghi\n';
		} 
	} 	

	return msg
} 
	

// later function to check validity of email - takes form object as a parameter
// WAB 2008/09/17 changed to use same regexp 
function verifyEmailV2(thisObject)  {
	
	thisValue = thisObject.value

		return isEmailAddressValid(thisValue) 

}


// takes object and a message and returns message if verification fails
function verifyEmailMessageV2  (object,message)	{

	if (object.value != '' && !verifyEmailV2 (object))  {
		return message + '\n'
	} else {
		return ''
	}
	
}
*/


// function used in screens to verify email addresses
function verifyEmail  (field,validMessage)	{

	// for backwards compatability with old version of verifyEmail which passes a string and no message
	if (typeof field == 'string')
	{
		field = eval('document.mainForm.'+field);
	}
	if (!validMessage) {
		validMessage = 'Please enter an email address of the form abc@def.ghi\n';
	}
	
	msg = ''
	value = field.value

	if (value != '') {

		// WAB 2008/05/20 check for blacklisting.  If blacklisted then throw an item into the coldfusion error array (using the _CF_onError function)
		if (! isFieldValueNotBlackListed (field, value)) {
			blacklistMessage = (field.getAttribute('blacklistMessage')) ? field.getAttribute('blacklistMessage') : 'Please use a business email address'
			msg +=  blacklistMessage + '\n'

		}

		// check for uniqueness of email address if necessary
		if (field.getAttribute('uniqueEmail')) {

			personid = field.name.split('_')[2]
			uniqueResult = isEmailUniqueAjax (value,personid )

			if (!uniqueResult) {
				uniqueEmailMessage = field.getAttribute('uniqueEmailMessage')
				uniqueEmailMessage = uniqueEmailMessage.replace(/\\n/,'\n');   // slightly odd this, but because the text has come from a hidden field \n is not actually interpreted as newline

				msg +=  uniqueEmailMessage + '\n'

			}

		}		
		
		msg +=  (isEmailAddressValid(value))?'':validMessage


	}

	return msg
}

			

/* 	 email validation for use with CFForm and RelayInputField - implements unique email addresses and blacklisting
	WAB 2016-03-04 BF-522 removed  code for Case425166.  Required'ness is now handled elsewhere
*/
  
function validateEmail(form, field, value) { 
	if (value != '') {
		// WAB 2008/05/20 check for blacklisting.  If blacklisted then throw an item into the coldfusion error array (using the _CF_onError function)
		if (! isFieldValueNotBlackListed (field, value)) {
		
			blacklistMessage = (field.getAttribute('blacklistMessage')) ? field.getAttribute('blacklistMessage') : 'Please use a business email address'
			_CF_onError(form, field.name, value, blacklistMessage)		

		}

		// check for uniqueness of email address if necessary
		if (field.getAttribute('uniqueEmail')) {

			personid = (field.getAttribute('personid'))?field.getAttribute('personid'):0
			orgType = (field.getAttribute('orgType')) && field.getAttribute('orgType') != '' && field.getAttribute('orgType') != 0?field.getAttribute('orgType'):'Reseller'
			if (personid == 0 && field.name.indexOf('_') != -1) {
				// PROD2016-2424 / Case 451836 - second try to get the personID - JIRA 2436 - check that value is defined
				var fieldArr = field.name.split('_');
				if (jQuery.isNumeric(fieldArr[fieldArr.length-1])) {
					personid = fieldArr[fieldArr.length-1];
				}
			}
			uniqueResult = isEmailUniqueAjax (value,personid,orgType)
			
			if (!uniqueResult) {
				uniqueEmailMessage = field.getAttribute('uniqueEmailMessage')
				uniqueEmailMessage = uniqueEmailMessage.replace(/\\n/,'\n');   // slightly odd this, but because the text has come from a hidden field \n is not actually interpreted as newline

				_CF_onError(form, field.name, value, uniqueEmailMessage);
				return uniqueEmailMessage;	// NJH used for rwFormValidation			
			}
		}		
		
		return _CF_checkEmail(value, false);
	} else {
		return true;
	}
}



// This is the synchronous ajax call which can be used when a form is submitted
function isEmailUniqueAjax (emailAddress,personid,orgType) {
	page = '/webservices/mxAjaxRelayPLO.cfc?wsdl'
	parameters  = 'method=init&emailAddress='+emailAddress +'&personid=' + personid + '&function=isEmailAddressUnique';

	if (orgType !== undefined) {
		parameters += '&orgType=' + orgType;
	}

	var myAjax = new Ajax.Request(
							page, 
								{method: 'get', 
								asynchronous: false,
								evalJSON: 'force',
								parameters: parameters, 
								debug:false}
					)		
	responseArray = myAjax.transport.responseText.evalJSON()

	return responseArray.isok
}




// checks whether a field value is blacklisted or not.  Note that returns true when value is OK and false when not OK.  Allows to be used directly as validate function in cfinput
function isFieldValueNotBlackListed (field, value) { 

	blacklist = field.getAttribute('blacklist')
	if (blacklist) {
		blackListRegExp = new RegExp(blacklist,'i'); 	
		return !blackListRegExp.test(value) 
	} 

	return true 
}
