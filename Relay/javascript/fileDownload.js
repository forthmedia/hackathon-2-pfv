/* �Relayware. All Rights Reserved 2014 */

function getFile(fileID) {
	window.open('/fileManagement/FileGet.cfm?fileID='+fileID,'FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
}


function downloadFile(fileUrl) {
    var docIframeID = '#fileLoad';
    
    if (jQuery(docIframeID).length == 0) {
        jQuery('<iframe src="" id="fileLoad" height="0px" width="0px" style="display:none"></iframe>').appendTo('body');
        
        jQuery(docIframeID).load(function (e) {
            var iframe = jQuery(docIframeID);
        
            if (iframe.html() ) {
                // get and check the Title (and H tags if you want)
                var ifTitle = iframe.contentDocument.title;
                if (ifTitle.indexOf("404")>=0 ) {
                    // we have a winner! probably a 404 page!
                }
            } else {
                alert('File not found');
            }
        });
    }
    
    jQuery(docIframeID).attr('src',fileUrl);
}


function verifyImageFile(filename) {
	if (filename != '') {
		var dot = filename.lastIndexOf(".");
		if (dot != -1) {
			var fileExt = filename.substr(dot+1,filename.length);
			if (!(fileExt.toLowerCase() == 'bmp' || fileExt.toLowerCase() == 'gif' || fileExt.toLowerCase() == 'jpg' || fileExt.toLowerCase() == 'jpeg' || fileExt.toLowerCase() == 'png')) {
				return 'Invalid image file. Image files can only be of type bmp,gif,jpg,jpeg or png.';
			}
		}
	}
}

function verifyFileExtension(filename,extension) {
	var extValid = false;
	if (filename == '') {
		extValid = true;
	} else if (filename != '' && extension != '') {
		var dot = filename.lastIndexOf(".");
		if (dot != -1) {
			var pattern = extension.replace(/,/g,'|')    // make commas into pipes (OR)
 			var regExp = new RegExp(pattern,'gi')
 			if (regExp.test (filename.substr(dot+1,filename.length))) {
 				extValid = true;
 			}
		}
	}
	if (!extValid) {
		return 'must be of type '+extension;
	}
}