/* �Relayware. All Rights Reserved 2014 */

function addMyLink(type,location,name,group,module) {
	var timeNow = new Date();
	page = '/webservices/myRelayWS.cfc';
	parameters = 'type='+type+'&location='+encodeURIComponent(location)+'&name='+name+'&group='+group+'&module='+module+'&returnFormat=json&_cf_nodebug=true&wsdl&method=addMyLink&time='+timeNow;
	
	var myAjax = new Ajax.Request(
		page,
		{
			method: 'get', 
			parameters: parameters, 
			evalJSON: 'force',
			debug : false,
			asynchronous: false
		}
	);
}


function removeMyLink(type,location,group) {
	var timeNow = new Date();
	page = '/webservices/myRelayWS.cfc';
	parameters = 'type='+type+'&location='+encodeURIComponent(location)+'&group='+group+'&returnFormat=json&_cf_nodebug=true&wsdl&method=removeMyLink&time='+timeNow;
	
	var myAjax = new Ajax.Request(
		page,
		{
			method: 'get', 
			parameters: parameters, 
			evalJSON: 'force',
			debug : false,
			asynchronous: false
		}
	);
}

function addToFavourites(obj) {
	addMyLink(obj.getAttribute('linkType'),obj.getAttribute('linkLocation'),obj.getAttribute('linkName'),obj.getAttribute('linkAttribute1'),obj.getAttribute('module'));
	obj.className = 'isFavorite';
	obj.setAttribute('title',phr.sys_myLinks_removeFromMyLinks);
	obj.onclick = function() {removeFromFavourites(obj);};
}

function removeFromFavourites(obj) {
	removeMyLink(obj.getAttribute('linkType'),obj.getAttribute('linkLocation'),obj.getAttribute('linkAttribute1'));
	obj.className = 'notIsFavorite';
	obj.setAttribute('title',phr.sys_myLinks_AddToMyLinks);
	obj.onclick = function() {addToFavourites(obj);};
}