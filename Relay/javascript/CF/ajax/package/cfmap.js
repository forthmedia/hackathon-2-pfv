/*ADOBE SYSTEMS INCORPORATED
Copyright 2007 Adobe Systems Incorporated
All Rights Reserved.

NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance with the
terms of the Adobe license agreement accompanying it.  If you have received this file from a
source other than Adobe, then your use, modification, or distribution of it requires the prior
written permission of Adobe.*/
if(!ColdFusion.Map){
ColdFusion.Map={};
}
var coldFusion_markerObjCache=new Array();
var $MAP=ColdFusion.Map;
$MAP.statusCodeObject={code200:"A directions request could not be successfully parsed. For example, the request may have been rejected if it contained more than the maximum number of waypoints allowed.",code400:"A directions request could not be successfully parsed. For example, the request may have been rejected if it contained more than the maximum number of waypoints allowed.",code500:"A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known",code601:"The HTTP query parameter was either missing or had no value. For geocoding requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input",code602:"No corresponding geographic location could be found for the specified address. This may be due to the fact that the address is relatively new, or it may be incorrect",code603:"The geocode for the given address or the route for the given directions query cannot be returned due to legal or contractual reasons",code604:"The GDirections object could not compute directions between the points mentioned in the query. This is usually because there is no route available between the two points, or because we do not have data for routing in that region",code610:"This request was invalid.",code620:"The webpage has gone over the requests limit in too short a period of time."};
ColdFusion.Map._init=function(_521,_522,_523,type,_525,_526,_527,_528,_529,_52a,_52b,_52c,_52d,_52e,_52f,_530,_531,_532,_533,_534,_535,_536,_537,_538,_539,_53a,_53b,_53c,_53d,_53e,_53f){
var _540=null;
if(navigator.geolocation){
navigator.geolocation.getCurrentPosition(function(_541){
if(_53f<1){
_534=_541.coords.latitude;
_535=_541.coords.longitude;
_533=null;
}
if(_53f!==0){
if(_53f<0){
_53f=_53f*-1;
}
_539[_53f-1].latitude=_541.coords.latitude;
_539[_53f-1].longitude=_541.coords.longitude;
}
_540=ColdFusion.Map.init(_521,_522,_523,type,_525,_526,_527,_528,_529,_52a,_52b,_52c,_52d,_52e,_52f,_530,_531,_532,_533,_534,_535,_536,_537,_538,_539,_53a,_53b,_53c,_53d,_53e);
},function(_542){
_540=ColdFusion.Map.init(_521,_522,_523,type,_525,_526,_527,_528,_529,_52a,_52b,_52c,_52d,_52e,_52f,_530,_531,_532,_533,_534,_535,_536,_537,_538,_539,_53a,_53b,_53c,_53d,_53e);
});
}else{
_540=ColdFusion.Map.init(_521,_522,_523,type,_525,_526,_527,_528,_529,_52a,_52b,_52c,_52d,_52e,_52f,_530,_531,_532,_533,_534,_535,_536,_537,_538,_539,_53a,_53b,_53c,_53d,_53e);
}
return _540;
};
ColdFusion.Map.init=function(_543,_544,_545,type,_547,_548,_549,_54a,_54b,_54c,_54d,_54e,_54f,_550,_551,_552,_553,_554,_555,_556,_557,_558,_559,_55a,_55b,_55c,_55d,_55e,_55f,_560){
var _561={divName:_543,type:type,layout:"fit",renderTo:_543,centerAddress:_555,centerLatitude:_556,centerLongitude:_557,markerItems:_55b,onLoad:_55c,onError:_55d,showCenterMarker:_550,showAllMarker:_551,markerColor:_559,markerIcon:_55a,markerBindListener:_55f,initShow:_54a};
if(_545!=null&&typeof (_545)!="undefined"){
_561.width=_545;
}else{
_561.width=400;
}
if(_544!=null&&typeof (_544)!="undefined"){
_561.height=_544;
}else{
_561.height=400;
}
if(_547!=null&&typeof (_547)!="undefined"){
_561.zoomLevel=_547;
}else{
_561.zoomLevel=3;
}
_561.hideBorders=_549;
if(!_549){
if(_548==null||typeof _548==="undefined"||_548.length==0){
_548=" ";
}
_561.title=_548;
_561.collapsible=_54b;
}
if(_559==null&&_55a==null){
_561.markerColor="#00FF00";
}
var _562=new Ext.Panel(_561);
ColdFusion.objectCache[_543]=_561;
_561.mapPanel=_562;
var _563=["enableDragging"];
var swz=false;
if(_54c){
swz=true;
}
if(_54d){
_563.push("enableDoubleClickZoom");
}else{
_563.push("disableDoubleClickZoom");
}
if(_54e){
_563.push("enableContinuousZoom");
}else{
_563.push("disableContinuousZoom");
}
var _565=["NonExistantControl"];
if(_54f){
_565.push("scaleControl");
}
var mtc=false;
var mtco="";
if(_553&&_553.toUpperCase()=="BASIC"){
mtc=true;
mtco="google.maps.MapTypeControlStyle.HORIZONTAL_BAR";
}else{
if(_553&&_553.toUpperCase()=="ADVANCED"){
mtc="true";
mtco=google.maps.MapTypeControlStyle.DROPDOWN_MENU;
}
}
if(_552){
_565.push("overviewMapControl");
}
var zc=false;
var zco="";
if(_554!=null&&_554!="undefined"){
_554=_554.toUpperCase();
switch(_554){
case "SMALL":
zco=google.maps.ZoomControlStyle.SMALL;
zc=true;
break;
case "SMALL3D":
zco=google.maps.ZoomControlStyle.SMALL;
zc=true;
break;
case "LARGE":
zco=google.maps.ZoomControlStyle.LARGE;
zc=true;
break;
case "LARGE3D":
zco=google.maps.ZoomControlStyle.LARGE;
zc=true;
break;
}
}
var _56a=[];
for(i=0;i<_561.markerItems.length;i++){
var _56b=$MAP.parseMarker(_561.markerItems[i],_543);
_56a.push(_56b);
}
if(_558==null||typeof _558==="undefined"){
_558="";
}
var _56c={marker:{title:_558,iscenter:true}};
if(_561.markerColor!=null&&typeof _561.markerColor!="undefined"){
_56c.marker.markercolor=_561.markerColor;
}else{
if(_561.markerIcon!=null&&typeof _561.markerIcon!="undefined"){
_56c.marker.markericon=_561.markerIcon;
}
}
if(_55e===true){
_56c.listeners={click:$MAP.markerOnClickHandler};
if(_560!=null){
_56c.marker.markerwindowcontent=_560;
}else{
_56c.marker.bindcallback=_55f;
}
_56c.marker.name=_543;
}
if(_561.centerAddress!=null&&typeof _561.centerAddress==="string"){
_56c.geoCodeAddr=_561.centerAddress;
_56c.marker.address=_561.centerAddress;
}else{
_56c.lat=_561.centerLatitude;
_56c.lng=_561.centerLongitude;
_56c.marker.address=_561.centerAddress;
}
var _56d=false;
if(_553!=null&&typeof _553=="string"&&_553.toUpperCase()=="ADVANCED"){
_56d=true;
}
var _56e=new Ext.ux.GMapPanel({xtype:"gmappanel",region:"center",zoomLevel:_561.zoomLevel,gmapType:_561.type,mapConfOpts:_563,mapControls:_565,setCenter:_56c,markers:_56a,border:!_561.hideBorders,onLoadhandler:$MAP.onLoadCompleteHandler,onErrorhandler:$MAP.onErrorHandler,name:_561.divName,noCenterMarker:!_550,showAllMarker:_551,advanceMapTypeControl:_56d,initShow:_54a,zc:zc,zco:zco,mtc:mtc,mtco:mtco,swz:swz});
_562.add(_56e);
_561.mapPanelObject=_56e;
if(_54a===false){
_562.hide();
}else{
_562.doLayout();
}
ColdFusion.Log.info("map.initialized","widget",[_543]);
return _562;
};
$MAP.addMarker=function(name,_570){
var _571=$MAP.getMapPanelObject(name);
var _572=$MAP.parseMarker(_570,name);
var _573=[];
_573.push(_572);
_571.addMarkers(_573);
ColdFusion.Log.info("map.addmarker.markeradded","widget",[name,_573.length]);
};
$MAP.setCenter=function(name,_575){
var _576=$MAP.getMapPanelObject(name);
var lat;
var lng;
if(_575.latitude&&_575.longitude){
if(typeof _575.latitude!="number"||typeof _575.longitude!="number"){
ColdFusion.handleError(null,"map.setcenter.latlngnonnumeric","widget",[name,_575.latitude,_575.longitude],null,null,true);
}else{
lat=_575.latitude;
lng=_575.longitude;
}
var _579=new google.maps.LatLng(lat,lng);
_576.getMap().setCenter(_579,_576.zoomLevel);
}else{
if(_575.address){
if(typeof _575.address!="string"){
ColdFusion.handleError(null,"map.setcenter.addressnotstring","widget",[name,_575.address],null,null,true);
}else{
_576.geoCodeLookup(_575.address,null,null,true);
}
}else{
ColdFusion.handleError(null,"map.setcenter.invalidcenter","widget",[name],null,null,true);
}
}
ColdFusion.Log.info("map.setcenter.centerset","widget",[name]);
};
$MAP.getLatitudeLongitude=function(_57a,_57b){
geocoder=new google.maps.Geocoder();
if(_57b==null||!typeof _57b==="function"){
_57b=$MAP.LatitudeLongitudeHanlder;
}
geocoder.geocode({"address":_57a},_57b);
};
$MAP.addEvent=function(name,_57d,_57e,_57f){
if(_57d=="singlerightclick"){
_57d="rightclick";
}
if(_57d=="maptypechanged"){
_57d="maptypeid_changed";
}
var _580=$MAP.getMapPanelObject(name);
_580.addEventToMap(_57d,_57e,_57f);
};
$MAP.setZoomLevel=function(name,_582){
var _583=$MAP.getMapPanelObject(name);
_583.zoomLevel=_582;
_583.getMap().setZoom(_582);
};
$MAP.getMapObject=function(name){
var _585=$MAP.getMapPanelObject(name);
if(_585!=null){
return _585.getMap();
}
};
$MAP.parseMarker=function(_586,_587){
var _588={};
if(_586.latitude&&_586.longitude){
if(typeof _586.latitude!="number"||typeof _586.longitude!="number"){
ColdFusion.handleError(null,"map.marker.latlngnonnumeric","widget",[_586.latitude,_586.longitude],null,null,true);
}else{
_588.lat=_586.latitude;
_588.lng=_586.longitude;
}
}else{
if(_586.address!=null){
if(typeof _586.address!="string"){
ColdFusion.handleError(null,"map.marker.addressnotstring","widget",[_586.address],null,null,true);
}else{
_588.address=_586.address;
}
}
}
var _589={};
if(_586.tip==null){
_589.title="";
}else{
_589.title=_586.tip;
}
if(_586.markercolor!=null&&typeof _586.markercolor!="undefined"){
_589.markercolor=_586.markercolor;
}else{
if(_586.markericon!=null&&typeof _586.markericon!="undefined"){
_589.markericon=_586.markericon;
}
}
if(_586.showmarkerwindow===true||_586.markerwindowcontent!=null){
var _58a=ColdFusion.objectCache[_587];
var _58b;
if(_58a!=null||typeof (_58a)!="undefined"){
_58b=_58a.markerBindListener;
}
if(_58b!=null||_586.markerwindowcontent!=null){
_588.listeners={click:$MAP.markerOnClickHandler};
if(_586.markerwindowcontent!=null){
_589.markerwindowcontent=_586.markerwindowcontent;
}else{
_589.bindcallback=_58b;
}
_589.name=_586.name;
}
}
_588.marker=_589;
return _588;
};
$MAP.onErrorHandler=function(name,_58d){
var _58e=ColdFusion.objectCache[name];
var _58f=$MAP.statusCodeObject;
var _590=$MAP.retrieveStatueMessage(_58d);
var _591=_58e.onError;
if(_591!=null&&typeof _591==="function"){
_591.call(null,_58d,_590);
}else{
alert("Error: "+_590);
}
ColdFusion.handleError(null,"map.loadMap.error","map",[name,_58d,_590],null,null,true);
};
$MAP.onLoadCompleteHandler=function(name){
var _593=ColdFusion.objectCache[name];
var _594=_593.onLoad;
if(_594!=null&&typeof _594==="function"){
_594.call();
}
};
$MAP.retrieveStatueMessage=function(code){
var _596;
switch(code){
case "ZERO_RESULTS":
_596=$MAP.statusCodeObject.code602;
break;
case "OVER_QUERY_LIMIT":
_596=$MAP.statusCodeObject.code620;
break;
case "REQUEST_DENIED":
_596=$MAP.statusCodeObject.code610;
break;
case "INVALID_REQUEST":
_596=$MAP.statusCodeObject.code610;
break;
}
return _596;
};
var currentopenwindow="";
$MAP.markerOnClickHandler=function(_597){
coldFusion_markerObjCache[this.name]=this.scope.marker;
if(this.bindcallback!=null&&typeof this.bindcallback=="function"){
var _598=this.address;
if(_598==null||typeof _598=="undefined"){
_598="";
}
this.bindcallback.call(null,this.name,_597.latLng.lat(),_597.latLng.lng(),_598);
}else{
if(this.scope.statictext!=null&&typeof this.scope.statictext!="undefined"){
var me=this,infoWindow=new google.maps.InfoWindow({content:this.scope.statictext,position:this.scope.marker.position});
if(currentopenwindow!=""){
currentopenwindow.close();
}
infoWindow.open(this.scope.marker.map);
currentopenwindow=infoWindow;
}
}
};
ColdFusion.Map.loadMarkerWindowInfo=function(data,_59b){
var _59c=coldFusion_markerObjCache[_59b._cf_marker_name];
var me=this,infoWindow=new google.maps.InfoWindow({content:data,position:_59c.position});
infoWindow.open(_59c.map);
};
ColdFusion.Map.bindOnErrorHandler=function(data,_59f){
ColdFusion.handleError(null,"map.markerbind.binderror","widget",[data],null,null,true);
};
$MAP.getMapPanelObject=function(name){
var _5a1=ColdFusion.objectCache[name];
if(_5a1==null||typeof (_5a1)=="undefined"){
ColdFusion.handleError(null,"map.getmappanelobject.notfound","widget",[name],null,null,true);
}
return _5a1.mapPanelObject;
};
$MAP.refresh=function(name){
var _5a3=ColdFusion.objectCache[name];
if(_5a3==null||typeof (_5a3)=="undefined"){
ColdFusion.handleError(null,"map.refresh.notfound","widget",[name],null,null,true);
}
_5a3.mapPanel.doLayout();
};
$MAP.hide=function(name){
var _5a5=ColdFusion.objectCache[name];
if(_5a5==null||typeof (_5a5)=="undefined"){
ColdFusion.handleError(null,"map.hide.notfound","widget",[name],null,null,true);
}
_5a5.mapPanel.hide();
};
$MAP.show=function(name){
var _5a7=ColdFusion.objectCache[name];
if(_5a7==null||typeof (_5a7)=="undefined"){
ColdFusion.handleError(null,"map.show.notfound","widget",[name],null,null,true);
}
_5a7.mapPanel.show();
_5a7.mapPanel.doLayout();
};
