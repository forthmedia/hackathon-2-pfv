/*ADOBE SYSTEMS INCORPORATED
Copyright 2007 Adobe Systems Incorporated
All Rights Reserved.

NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance with the
terms of the Adobe license agreement accompanying it.  If you have received this file from a
source other than Adobe, then your use, modification, or distribution of it requires the prior
written permission of Adobe.*/
if(!ColdFusion.Layout){
ColdFusion.Layout={};
}
var ACCORDION_TITLE_ICON_CSS_TEMPLATE=".{0} { background-image:url({1}); }";
if(!ColdFusion.MapVsAccordion){
ColdFusion.MapVsAccordion={};
}
ColdFusion.Layout.initializeTabLayout=function(id,_5a9,_5aa,_5ab,_5ac){
Ext.QuickTips.init();
var _5ad;
if(_5aa){
_5ad={renderTo:id,height:_5aa};
}else{
_5ad={renderTo:id,autoHeight:true};
}
if(_5ab&&_5ab!="undefined"){
_5ad.width=_5ab;
}else{
_5ad.autoWidth=true;
}
if(_5a9){
_5ad.tabPosition="bottom";
}else{
_5ad.enableTabScroll=true;
}
_5ad.plain=!_5ac;
var _5ae=new Ext.TabPanel(_5ad);
ColdFusion.objectCache[id]=_5ae;
return _5ae;
};
ColdFusion.Layout.getTabLayout=function(_5af){
var _5b0=ColdFusion.objectCache[_5af];
if(!_5b0||!(_5b0 instanceof Ext.TabPanel)){
ColdFusion.handleError(null,"layout.gettablayout.notfound","widget",[_5af],null,null,true);
}
return _5b0;
};
ColdFusion.Layout.onTabActivate=function(tab){
tab._cf_visible=true;
if(tab._cf_dirtyview){
var _5b2=ColdFusion.bindHandlerCache[tab.contentEl];
if(_5b2){
_5b2();
}
tab._cf_dirtyview=false;
}
var el=Ext.get(tab.contentEl);
el.move("left",1);
el.move("right",1);
};
ColdFusion.Layout.onTabDeactivate=function(tab){
tab._cf_visible=false;
if(tab._cf_refreshOnActivate){
tab._cf_dirtyview=true;
}
};
ColdFusion.Layout.onTabClose=function(tab){
tab._cf_visible=false;
};
ColdFusion.Layout.addTab=function(_5b6,_5b7,name,_5b9,_5ba,_5bb,_5bc,_5bd,_5be){
if(_5ba!=null&&_5ba.length==0){
_5ba=null;
}
var _5bf=_5b6.initialConfig.autoHeight;
if(typeof _5bf=="undefined"){
_5bf=false;
}
var _5c0=new Ext.Panel({title:_5b9,contentEl:_5b7,_cf_body:_5b7,id:name,closable:_5bb,tabTip:_5ba,autoScroll:_5be,autoShow:true,autoHeight:_5bf});
var tab=_5b6.add(_5c0);
if(_5bd){
_5c0.setDisabled(true);
}
tab._cf_visible=false;
tab._cf_dirtyview=true;
tab._cf_refreshOnActivate=_5bc;
tab.addListener("activate",ColdFusion.Layout.onTabActivate);
tab.addListener("deactivate",ColdFusion.Layout.onTabDeactivate);
tab.addListener("close",ColdFusion.Layout.onTabClose);
ColdFusion.objectCache[name]=tab;
var _5c2=tab.height;
if(_5c2&&_5c2>1){
var _5c3=document.getElementById(_5b7);
_5c3.style.height=_5c2;
}
};
ColdFusion.Layout.enableTab=function(_5c4,_5c5){
var _5c6=ColdFusion.objectCache[_5c4];
var _5c7=ColdFusion.objectCache[_5c5];
if(_5c6&&(_5c6 instanceof Ext.TabPanel)&&_5c7){
_5c7.setDisabled(false);
ColdFusion.Log.info("layout.enabletab.enabled","widget",[_5c5,_5c4]);
}else{
ColdFusion.handleError(null,"layout.enabletab.notfound","widget",[_5c4],null,null,true);
}
};
ColdFusion.Layout.disableTab=function(_5c8,_5c9){
var _5ca=ColdFusion.objectCache[_5c8];
var _5cb=ColdFusion.objectCache[_5c9];
if(_5ca&&(_5ca instanceof Ext.TabPanel)&&_5cb){
_5cb.setDisabled(true);
ColdFusion.Log.info("layout.disabletab.disabled","widget",[_5c9,_5c8]);
}else{
ColdFusion.handleError(null,"layout.disabletab.notfound","widget",[_5c8],null,null,true);
}
};
ColdFusion.Layout.selectTab=function(_5cc,_5cd){
var _5ce=ColdFusion.objectCache[_5cc];
var tab=ColdFusion.objectCache[_5cd];
if(_5ce&&(_5ce instanceof Ext.TabPanel)&&tab){
_5ce.setActiveTab(tab);
ColdFusion.Log.info("layout.selecttab.selected","widget",[_5cd,_5cc]);
}else{
ColdFusion.handleError(null,"layout.selecttab.notfound","widget",[_5cc],null,null,true);
}
};
ColdFusion.Layout.hideTab=function(_5d0,_5d1){
var _5d2=ColdFusion.objectCache[_5d0];
if(_5d2&&(_5d2 instanceof Ext.TabPanel)){
var _5d3=ColdFusion.objectCache[_5d1];
var _5d4=false;
if(_5d3){
if(_5d2.getActiveTab()&&_5d2.getActiveTab().getId()==_5d1){
var i;
for(i=0;i<_5d2.items.length;i++){
var _5d6=_5d2.getComponent(i);
if(_5d6.hidden==false){
_5d4=true;
_5d6.show();
break;
}
}
if(_5d4==false){
document.getElementById(_5d1).style.display="none";
}
}
_5d2.hideTabStripItem(_5d3);
ColdFusion.Log.info("layout.hidetab.hide","widget",[_5d1,_5d0]);
}
}else{
ColdFusion.handleError(null,"layout.hidetab.notfound","widget",[_5d0],null,null,true);
}
};
ColdFusion.Layout.showTab=function(_5d7,_5d8){
var _5d9=ColdFusion.objectCache[_5d7];
var _5da=ColdFusion.objectCache[_5d8];
if(_5d9&&(_5d9 instanceof Ext.TabPanel)&&_5da){
_5d9.unhideTabStripItem(_5da);
ColdFusion.Log.info("layout.showtab.show","widget",[_5d8,_5d7]);
}else{
ColdFusion.handleError(null,"layout.showtab.notfound","widget",[_5d7],null,null,true);
}
};
ColdFusion.Layout.disableSourceBind=function(_5db){
var _5dc=ColdFusion.objectCache[_5db];
if(_5dc==null||_5dc=="undefined"){
ColdFusion.handleError(null,"layout.disableSourceBind.notfound","widget",[_5db],null,null,true);
}
_5dc._cf_dirtyview=false;
};
ColdFusion.Layout.enableSourceBind=function(_5dd){
var _5de=ColdFusion.objectCache[_5dd];
if(_5de==null||_5de=="undefined"){
ColdFusion.handleError(null,"layout.enableSourceBind.notfound","widget",[_5dd],null,null,true);
}
_5de._cf_dirtyview=true;
};
ColdFusion.Layout.createTab=function(_5df,_5e0,_5e1,_5e2,_5e3){
var _5e4=ColdFusion.objectCache[_5df];
var _5e5=_5e0;
if(_5df&&typeof (_5df)!="string"){
ColdFusion.handleError(null,"layout.createtab.invalidname","widget",null,null,null,true);
return;
}
if(!_5df||ColdFusion.trim(_5df)==""){
ColdFusion.handleError(null,"layout.createtab.emptyname","widget",null,null,null,true);
return;
}
if(_5e0&&typeof (_5e0)!="string"){
ColdFusion.handleError(null,"layout.createtab.invalidareaname","widget",null,null,null,true);
return;
}
if(!_5e0||ColdFusion.trim(_5e0)==""){
ColdFusion.handleError(null,"layout.createtab.emptyareaname","widget",null,null,null,true);
return;
}
if(_5e1&&typeof (_5e1)!="string"){
ColdFusion.handleError(null,"layout.createtab.invalidtitle","widget",null,null,null,true);
return;
}
if(!_5e1||ColdFusion.trim(_5e1)==""){
ColdFusion.handleError(null,"layout.createtab.emptytitle","widget",null,null,null,true);
return;
}
if(_5e2&&typeof (_5e2)!="string"){
ColdFusion.handleError(null,"layout.createtab.invalidurl","widget",null,null,null,true);
return;
}
if(!_5e2||ColdFusion.trim(_5e2)==""){
ColdFusion.handleError(null,"layout.createtab.emptyurl","widget",null,null,null,true);
return;
}
_5e0="cf_layoutarea"+_5e0;
if(_5e4&&(_5e4 instanceof Ext.TabPanel)){
var _5e6=null;
var ele=document.getElementById(_5e0);
if(ele!=null){
ColdFusion.handleError(null,"layout.createtab.duplicateel","widget",[_5e0],null,null,true);
return;
}
var _5e8=false;
var _5e9=false;
var _5ea=false;
var _5eb=false;
var _5ec=false;
var _5ed=null;
if((_5e4.items.length<=0)){
_5ea=true;
}
if(_5e3!=null){
if(typeof (_5e3)!="object"){
ColdFusion.handleError(null,"layout.createtab.invalidconfig","widget",null,null,null,true);
return;
}
if(typeof (_5e3.closable)!="undefined"&&_5e3.closable==true){
_5e8=true;
}
if(typeof (_5e3.disabled)!="undefined"&&_5e3.disabled==true){
_5e9=true;
}
if(typeof (_5e3.selected)!="undefined"&&_5e3.selected==true){
_5ea=true;
}
if(typeof (_5e3.inithide)!="undefined"&&_5e3.inithide==true){
_5eb=true;
}
if(typeof (_5e3.tabtip)!="undefined"&&_5e3.tabtip!=null){
_5ed=_5e3.tabtip;
}
}
var _5ee=document.getElementById(_5df);
if(_5ee){
var _5ef=document.getElementById(_5df);
var _5f0=document.createElement("div");
_5f0.id=_5e0;
_5f0.className="ytab";
if(_5e3!=null&&typeof (_5e3.align)!="undefined"){
_5f0.align=_5e3.align;
}
var _5f1="";
if(_5e4.tabheight){
_5f1="height:"+_5e4.tabheight+";";
}
if(_5e3!=null&&typeof (_5e3.style)!="undefined"){
var _5f2=new String(_5e3.style);
_5f2=_5f2.toLowerCase();
_5f1=_5f1+_5f2;
}
if(_5e3!=null&&typeof (_5e3.overflow)!="undefined"){
var _5f3=new String(_5e3.overflow);
_5f3=_5f3.toLowerCase();
if(_5f3!="visible"&&_5f3!="auto"&&_5f3!="scroll"&&_5f3!="hidden"){
ColdFusion.handleError(null,"layout.createtab.invalidoverflow","widget",null,null,null,true);
return;
}
if(_5f3.toLocaleLowerCase()==="hidden"){
_5ec=false;
}
_5f1=_5f1+"overflow:"+_5f3+";";
}else{
_5f1=_5f1+"; overflow:auto;";
}
_5f0.style.cssText=_5f1;
_5ef.appendChild(_5f0);
}
ColdFusion.Layout.addTab(_5e4,_5e0,_5e5,_5e1,_5ed,_5e8,false,_5e9,_5ec);
ColdFusion.Log.info("layout.createtab.success","http",[_5e0,_5df]);
if(_5ea==true){
ColdFusion.Layout.selectTab(_5df,_5e5);
}
if(_5eb==true){
ColdFusion.Layout.hideTab(_5df,_5e5);
}
if(_5e2!=null&&typeof (_5e2)!="undefined"&&_5e2!=""){
if(_5e2.indexOf("?")!=-1){
_5e2=_5e2+"&";
}else{
_5e2=_5e2+"?";
}
var _5f4;
var _5f5;
if(_5e3){
_5f4=_5e3.callbackHandler;
_5f5=_5e3.errorHandler;
}
ColdFusion.Ajax.replaceHTML(_5e0,_5e2,"GET",null,_5f4,_5f5);
}
}else{
ColdFusion.handleError(null,"layout.createtab.notfound","widget",[_5df],null,null,true);
}
};
ColdFusion.Layout.getBorderLayout=function(_5f6){
var _5f7=ColdFusion.objectCache[_5f6];
if(!_5f7){
ColdFusion.handleError(null,"layout.getborderlayout.notfound","widget",[_5f6],null,null,true);
}
return _5f7;
};
ColdFusion.Layout.showArea=function(_5f8,_5f9){
var _5fa=ColdFusion.Layout.convertPositionToDirection(_5f9);
var _5fb=ColdFusion.objectCache[_5f8];
var _5fc;
if(_5fb){
var _5fd=_5fb.items;
for(var i=0;i<_5fd.getCount();i++){
var _5ff=_5fd.itemAt(i);
if(_5ff instanceof Ext.Panel&&_5ff.region==_5fa){
_5fc=_5ff;
break;
}
}
if(_5fc){
_5fc.show();
_5fc.expand();
ColdFusion.Log.info("layout.showarea.shown","widget",[_5f9,_5f8]);
}else{
ColdFusion.handleError(null,"layout.showarea.areanotfound","widget",[_5f9],null,null,true);
}
}else{
ColdFusion.handleError(null,"layout.showarea.notfound","widget",[_5f8],null,null,true);
}
};
ColdFusion.Layout.hideArea=function(_600,_601){
var _602=ColdFusion.Layout.convertPositionToDirection(_601);
var _603=ColdFusion.objectCache[_600];
var _604;
if(_603){
var _605=_603.items;
for(var i=0;i<_605.getCount();i++){
var _607=_605.itemAt(i);
if(_607 instanceof Ext.Panel&&_607.region==_602){
_604=_607;
break;
}
}
if(_604){
_604.hide();
ColdFusion.Log.info("layout.hidearea.hidden","widget",[_601,_600]);
}else{
ColdFusion.handleError(null,"layout.hidearea.areanotfound","widget",[_601],null,null,true);
}
}else{
ColdFusion.handleError(null,"layout.hidearea.notfound","widget",[_600],null,null,true);
}
};
ColdFusion.Layout.collapseArea=function(_608,_609){
var _60a=ColdFusion.Layout.convertPositionToDirection(_609);
var _60b=ColdFusion.objectCache[_608];
var _60c;
if(_60b){
var _60d=_60b.items;
for(var i=0;i<_60d.getCount();i++){
var _60f=_60d.itemAt(i);
if(_60f instanceof Ext.Panel&&_60f.region==_60a){
_60c=_60f;
break;
}
}
if(_60c){
_60c.collapse(true);
ColdFusion.Log.info("layout.collpasearea.collapsed","widget",[_609,_608]);
}else{
ColdFusion.handleError(null,"layout.collpasearea.areanotfound","widget",[_609],null,null,true);
}
}else{
ColdFusion.handleError(null,"layout.collpasearea.notfound","widget",[_609],null,null,true);
}
};
ColdFusion.Layout.expandArea=function(_610,_611){
var _612=ColdFusion.Layout.convertPositionToDirection(_611);
var _613=ColdFusion.objectCache[_610];
var _614;
if(_613){
var _615=_613.items;
for(var i=0;i<_615.getCount();i++){
var _617=_615.itemAt(i);
if(_617 instanceof Ext.Panel&&_617.region==_612){
_614=_617;
break;
}
}
if(_614){
_614.expand();
ColdFusion.Log.info("layout.expandarea.expanded","widget",[_611,_610]);
}else{
ColdFusion.handleError(null,"layout.expandarea.areanotfound","widget",[_611],null,null,true);
}
}else{
ColdFusion.handleError(null,"layout.expandarea.notfound","widget",[_611],null,null,true);
}
};
ColdFusion.Layout.printObject=function(obj){
var str="";
for(key in obj){
str=str+"  "+key+"=";
value=obj[key];
if(typeof (value)==Object){
value=$G.printObject(value);
}
str+=value;
}
return str;
};
ColdFusion.Layout.InitAccordion=function(_61a,_61b,_61c,_61d,_61e,_61f,_620,_621){
var _622=false;
if(_61c.toUpperCase()=="LEFT"){
_622=true;
}
if(_61f==null||typeof (_61f)=="undefined"){
_61e=false;
}
var _623={activeOnTop:_61b,collapseFirst:_622,titleCollapse:_61d,fill:_61e};
var _624={renderTo:_61a,layoutConfig:_623,items:_621,layout:"accordion"};
if(_61f==null||typeof (_61f)=="undefined"){
_624.autoHeight=true;
}else{
_624.height=_61f;
}
if(_620==null||typeof (_620)=="undefined"){
_624.autoWidth=true;
}else{
_624.width=_620;
}
var _625=new Ext.Panel(_624);
ColdFusion.objectCache[_61a]=_625;
ColdFusion.Log.info("layout.accordion.initialized","widget",[_61a]);
return _625;
};
ColdFusion.Layout.InitAccordionChildPanel=function(_626,_627,_628,_629,_62a,_62b,_62c,_62d){
if(_628==null||typeof (_628)==undefined||_628.length==0){
_628="  ";
}
var _62e={contentEl:_626,id:_627,title:_628,collapsible:_629,closable:_62a,animate:true,autoScroll:_62b,_cf_body:_626};
if(_62c&&typeof _62c=="string"){
_62e.iconCls=_62c;
}
var _62f=new Ext.Panel(_62e);
_62f._cf_visible=false;
_62f._cf_dirtyview=true;
_62f._cf_refreshOnActivate=_62d;
_62f.on("expand",ColdFusion.Layout.onAccordionPanelExpand,this);
_62f.on("collapse",ColdFusion.Layout.onAccordionPanelCollapse,this);
_62f.on("hide",ColdFusion.Layout.onAccordionPanelHide,this);
_62f.on("show",ColdFusion.Layout.onAccordionPanelExpand,this);
ColdFusion.objectCache[_627]=_62f;
ColdFusion.Log.info("layout.accordion.childinitialized","widget",[_627]);
return _62f;
};
ColdFusion.Layout.getAccordionLayout=function(_630){
var _631=ColdFusion.objectCache[_630];
if(!_631||!(_631 instanceof Ext.Panel)){
ColdFusion.handleError(null,"layout.getaccordionlayout.notfound","widget",[_630],null,null,true);
}
return _631;
};
ColdFusion.Layout.onAccordionPanelExpand=function(_632){
_632._cf_visible=true;
if(_632._cf_dirtyview){
var _633=ColdFusion.bindHandlerCache[_632.contentEl];
if(_633){
_633();
}
_632._cf_dirtyview=false;
}
var el=Ext.get(_632.contentEl);
el.move("left",1);
el.move("right",1);
var _635=ColdFusion.MapVsAccordion[_632._cf_body];
if(_635!=undefined){
var _636=$MAP.getMapPanelObject(_635);
if(_636!=undefined){
if(_636.initShow===true){
$MAP.show(_635);
}
}
}
};
ColdFusion.Layout.onAccordionPanelCollapse=function(_637){
_637._cf_visible=false;
if(_637._cf_refreshOnActivate){
_637._cf_dirtyview=true;
}
};
ColdFusion.Layout.onAccordionPanelHide=function(_638){
_638._cf_visible=false;
};
ColdFusion.Layout.hideAccordion=function(_639,_63a){
var _63b=ColdFusion.objectCache[_639];
var _63c=ColdFusion.objectCache[_63a];
if(!_63b||!_63b instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.hideaccordion.layoutnotfound","widget",[_639],null,null,true);
}
if(!_63c||!_63c instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.hideaccordion.panelnotfound","widget",[_63a],null,null,true);
}
_63c.hide();
ColdFusion.Log.info("layout.hideaccordion.hidden","widget",[_63a,_639]);
};
ColdFusion.Layout.showAccordion=function(_63d,_63e){
var _63f=ColdFusion.objectCache[_63d];
var _640=ColdFusion.objectCache[_63e];
if(!_63f||!_63f instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.showaccordion.layoutnotfound","widget",[_63d],null,null,true);
}
if(!_640||!_640 instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.showaccordion.panelnotfound","widget",[_63e],null,null,true);
}
_640.show();
ColdFusion.Log.info("layout.showaccordion.shown","widget",[_63e,_63d]);
};
ColdFusion.Layout.expandAccordion=function(_641,_642){
var _643=ColdFusion.objectCache[_641];
var _644=ColdFusion.objectCache[_642];
if(!_643||!_643 instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.expandaccordion.layoutnotfound","widget",[_641],null,null,true);
}
if(!_644||!_644 instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.expandaccordion.panelnotfound","widget",[_642],null,null,true);
}
_644.expand();
ColdFusion.Log.info("layout.expandaccordion.expanded","widget",[_642,_641]);
};
ColdFusion.Layout.selectAccordion=function(_645,_646){
return ColdFusion.Layout.expandAccordion(_645,_646);
};
ColdFusion.Layout.collapseAccordion=function(_647,_648){
var _649=ColdFusion.objectCache[_647];
var _64a=ColdFusion.objectCache[_648];
if(!_649||!_649 instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.collapseaccordion.layoutnotfound","widget",[_647],null,null,true);
}
if(!_64a||!_64a instanceof Ext.Panel){
ColdFusion.handleError(null,"layout.collapseaccordion.panelnotfound","widget",[_648],null,null,true);
}
_64a.collapse();
ColdFusion.Log.info("layout.collapseaccordion.collapsed","widget",[_648,_647]);
};
ColdFusion.Layout.createAccordionPanel=function(_64b,_64c,_64d,url,_64f){
var _650=ColdFusion.objectCache[_64b];
var _651=_64c;
if(_64b&&typeof (_64b)!="string"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidname","widget",[_64b],null,null,true);
return;
}
if(!_64b||ColdFusion.trim(_64b)==""){
ColdFusion.handleError(null,"layout.createaccordionpanel.emptyname","widget",[_64b],null,null,true);
return;
}
if(_64c&&typeof (_64c)!="string"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidaccordionpanelname","widget",[_64c],null,null,true);
return;
}
if(!_64c||ColdFusion.trim(_64c)==""){
ColdFusion.handleError(null,"layout.createaccordionpanel.emptyaccordionpanelname","widget",[_64c],null,null,true);
return;
}
if(_64d&&typeof (_64d)!="string"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidtitle","widget",[_64c],null,null,true);
return;
}
if(!_64d||ColdFusion.trim(_64d)==""){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidtitle","widget",[_64c],null,null,true);
return;
}
if(url&&typeof (url)!="string"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidurl","widget",[_64c],null,null,true);
return;
}
if(!url||ColdFusion.trim(url)==""){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidurl","widget",[_64c],null,null,true);
return;
}
_64c="cf_layoutarea"+_651;
if(_650&&(_650 instanceof Ext.Panel)){
var _652=null;
var ele=document.getElementById(_64c);
if(ele!=null){
ColdFusion.handleError(null,"layout.createaccordionpanel.duplicateel","widget",[_64c],null,null,true);
return;
}
var _654=true;
var _655;
var _656=false;
var _657=null;
if(_64f!=null){
if(typeof (_64f)!="object"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidconfig","widget",[_64c],null,null,true);
return;
}
}
if(_64f&&typeof (_64f.selected)!="undefined"&&_64f.selected==true){
_656=true;
}
if(_64f&&_64f.titleicon){
if(typeof _64f.titleicon!="string"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidtitleicon","widget",[_64c],null,null,true);
return;
}
var _658=String.format(ACCORDION_TITLE_ICON_CSS_TEMPLATE,_64c,_64f.titleicon);
Ext.util.CSS.createStyleSheet(_658,_64c+"_cf_icon");
_657=_64c;
}
var _659=_650.layoutConfig;
var _65a=true;
if(_659&&typeof _659.fill!="undefined"){
_65a=_659.fill;
}
if(_64f!=null&&typeof (_64f.overflow)!="undefined"){
var _655=new String(_64f.overflow);
_655=_655.toLowerCase();
if(_655!="visible"&&_655!="auto"&&_655!="scroll"&&_655!="hidden"){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidoverflow","widget",[_64c],null,null,true);
return;
}
if(!_65a&&(_655=="auto"||_655=="scroll")){
ColdFusion.handleError(null,"layout.createaccordionpanel.invalidoverflowforfillheight","widget",[_64c],null,null,true);
return;
}
if(_655=="hidden"){
_654=false;
}
}else{
_655="auto";
_654=true;
}
var _65b=document.getElementById(_64b);
if(_65b){
var _65c=document.getElementById(_64b);
var _65d=document.createElement("div");
_65d.id=_64c;
if(_64f!=null&&typeof (_64f.align)!="undefined"){
_65d.align=_64f.align;
}
var _65e="";
if(_650.height){
_65e="height:"+_650.height+";";
}
if(_64f!=null&&typeof (_64f.style)!="undefined"){
var _65f=new String(_64f.style);
_65f=_65f.toLowerCase();
_65e=_65e+_65f;
}
_65e=_65e+"overflow:"+_655+";";
_65d.style.cssText=_65e;
_65c.appendChild(_65d);
}
var _660=true;
var _661=true;
itemobj=ColdFusion.Layout.InitAccordionChildPanel(_64c,_651,_64d,_661,_660,_654,_657,false);
_650.add(itemobj);
if(url!=null&&typeof (url)!="undefined"&&url!=""){
if(url.indexOf("?")!=-1){
url=url+"&";
}else{
url=url+"?";
}
var _662;
var _663;
if(_64f){
_662=_64f.callbackHandler;
_663=_64f.errorHandler;
}
ColdFusion.Ajax.replaceHTML(_64c,url,"GET",null,_662,_663);
}
_650.doLayout();
if(_656){
ColdFusion.Layout.expandAccordion(_64b,_651);
}
ColdFusion.Log.info("layout.createaccordionpanel.created","widget",[_64c]);
}else{
ColdFusion.handleError(null,"layout.createaccordionpanel.layoutnotfound","widget",[_64b],null,null,true);
}
};
ColdFusion.Layout.initViewport=function(_664,item){
var _666=new Array();
_666[0]=item;
var _667={items:_666,layout:"fit",name:_664};
var _668=new Ext.Viewport(_667);
return _668;
};
ColdFusion.Layout.convertPositionToDirection=function(_669){
if(_669.toUpperCase()=="LEFT"){
return "west";
}else{
if(_669.toUpperCase()=="RIGHT"){
return "east";
}else{
if(_669.toUpperCase()=="CENTER"){
return "center";
}else{
if(_669.toUpperCase()=="BOTTOM"){
return "south";
}else{
if(_669.toUpperCase()=="TOP"){
return "north";
}
}
}
}
}
};
ColdFusion.Layout.addMapInAccordionMapping=function(_66a,map){
ColdFusion.MapVsAccordion[_66a]=map;
};
