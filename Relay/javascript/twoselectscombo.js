/* �Relayware. All Rights Reserved 2014 */
/*
	This code used by \customTags\TwoSelectsComboList.cfm
	LH 2280 SSS 2009-06-12 
	WAB 2009-07-08 problems with the blank row inserted to force a width
	WAB 2011/03/30 seemed to be more problems with this blank row
	2011/08/25 PPB LID5173 sort lists after moving an item from 1 to the other
	2011/11/30	WAB added a doSort attribute - doesn't make sense when we have up/down control as well
	2013-02-20	WAB	Sprint 15&42 Comms - added support for custom isDirty functions on elements
	2013/03/25 NJH Store values in a hidden field - no longer need the selAll function
	2013-05-23 WAB no longer need the isDirty stuff I had added.  Also converted to allow use in ajax requests
	2013-06-04 NJH - removed all comments and changed the way options were created. Use the new Option method (so that IE is happy) and removed the old methods of working out position, etc. 
					Don't need to worry about the empty value anymore as that has been gotten rid of.
	2014-06-19 REH Case 439402 fixed issue with duplicate items added to selectbox and headings not being removed
	2014-12-12 DCC Case 443039 changed dodgy call jQuery(selObject).children().empty('option'); line 117
	2015-09-24	ACPK	PROD2015-92 Fixed title of option not being transferred and being overwritten on sort
	2016/02/04 NJH BF-465 - check the length of selObject as a length of 0 causes a problem. Return empty array if length is 0.
*/
	 moveFromTo = function(obj,fromName,toName,doSort) {
			startpos=0;
			endpos=0;
			lastItemInOptGroup=false;

		thisForm = obj.form
	 	fromObject =  thisForm[fromName]
		toObject = thisForm[toName]

			for (i=fromObject.length -1 ; i >=0 ; i--) { 
				if (fromObject.options[i].selected) {
					SelFrom = fromObject.options[i].selected;
					if (fromObject.options[i].value != '') {

						if (fromObject.options[i].parentNode.tagName == 'OPTGROUP') {
							// need to either find existing optgroup or create our own
							requiredOptGroupLabel = fromObject.options[i].parentNode.label;
							//2014-06-19 REH Case 439402 changed to children instead of childNodes as its a html collection of the child element. childNodes contains methods 
							lastItemInOptGroup = (fromObject.options[i].parentNode.children.length == 1)?true:false
							optGroupsArray=toObject.getElementsByTagName('optgroup');
							var optGroupFound = false
							if (optGroupsArray.length != 0) {
								for (j=0;j<optGroupsArray.length;j++) {
									if(optGroupsArray[j].label == requiredOptGroupLabel) {
										optGroupObject = optGroupsArray[j]
										optGroupFound = true
										break;
									}
								}
							} 
							
							if (!optGroupFound){
								optGroupObject = document.createElement('optgroup')
								optGroupObject.label =  requiredOptGroupLabel
								toObject.insertBefore(optGroupObject,toObject.firstChild) ;
							}
	
							/* 2013-06-04 NJH use new Option method
								newOption = document.createElement('option');
							//	newOption.text  = fromObject.options[i].text;     // this doesn't work so well in IE
								newOption.innerHTML  = fromObject.options[i].text; 
								newOption.value = fromObject.options[i].value
								//newOption = Element.clone(fromObject.options[i],true);
							*/		

								newOption = new Option(fromObject.options[i].text, fromObject.options[i].value);
								newOption.setAttribute('title', fromObject.options[i].title); 		//2015-09-24	ACPK	PROD2015-92 Fixed title of option not being transferred
								optGroupObject.appendChild(newOption);
						} else {
							toObject.options[toObject.options.length] = new Option(fromObject.options[i].text, fromObject.options[i].value);
							toObject.options[toObject.options.length - 1].setAttribute('title', fromObject.options[i].title); 		//2015-09-24	ACPK	PROD2015-92 Fixed title of option not being transferred
						}
					
						startpos=i;
						if (lastItemInOptGroup) {
							fromObject.removeChild(fromObject.options[i].parentNode);					
						}else {
							$(fromObject.options[i]).remove();
						}
					}
				}
			}
		if (doSort) {
			sortList(toObject);			// 2011/08/25 PPB LID5173
		}
		updateHiddenField(fromName);
		updateHiddenField(toName);
		// 2014/01/17	YMA	Trigger the change event to run corresponding function in relayForms that removes any duplicate values for twoSelects controlled by a bind.
		jQuery("#"+toName).trigger("change");
	}

/* 
2011/08/25 PPB LID5173 START sort lists after moving an item from 1 to the other
copy all OPTIONS to an array, sort it then copy them back (into the right optGroups where used)  
2016/02/04	NJH BF-465 - check the length of selObject as a length of 0 causes a problem. Return empty array if length is 0.
*/

 get_options = function (selObject) {
 	var tmp = new Array(0);
 	if (selObject.length > 0) {
		tmp = new Array(selObject.length-1);
		for (var i = 0; i < selObject.length; i++) {
			tmp[i] = new Array(3);
		
			tmp[i][0] = selObject[i].text.toLowerCase();			//deliberately element 0 for sort() to work; used for sorting so all words starting with lower case don't end up at bottom
			tmp[i][1] = selObject[i].text;
			tmp[i][2] = selObject[i].value;
			tmp[i][3] = selObject[i].parentNode.label;
			tmp[i][4] = selObject[i].title;							//2015-09-24	ACPK	PROD2015-92 Fixed title of option not being transferred
		}
	}
	return tmp;
}

 sortList = function (selObject) {

	var selOptions = get_options(selObject.options);
	selOptions.sort();

	//2014-06-19 REH Case 439402 changed empty instead of remove as it removes the child elements. Remove, removes the element it self out of the DOM which is not what we want to do here.
	//; previous approaches removed - see svn
	//2014-12-12 DCC Case 443039 changed dodgy call jQuery(selObject).children().empty('option');
	jQuery(selObject).find('option').remove().end();

	for (var i = 0; i < selOptions.length; i++) {
	
		requiredOptGroupLabel = selOptions[i][3];
		
		optGroupsArray=selObject.getElementsByTagName('optgroup');
		if (optGroupsArray.length != 0) {
			for (j=0;j<optGroupsArray.length;j++) {
				if(optGroupsArray[j].label == requiredOptGroupLabel) {
					optGroupObject = optGroupsArray[j]
					break;
				}
			}
			
			newOption = document.createElement('option');
			newOption.innerHTML  = selOptions[i][1];
			newOption.value = selOptions[i][2];
			newOption.title = selOptions[i][4]; 		//2015-09-24	ACPK	PROD2015-92 Fixed titles of options being overwritten on sort
			optGroupObject.appendChild(newOption); 	  
		} else {
			selObject.options[selObject.options.length] = new Option(selOptions[i][1], selOptions[i][2]);
			selObject.options[selObject.options.length - 1].setAttribute('title', selOptions[i][4]); 		//2015-09-24	ACPK	PROD2015-92 Fixed titles of options being overwritten on sort
		} 
	}
	}

/* 2011/08/25 PPB LID5173 END */

  Move = function(obj,objectName,moveIndex) {

	thisForm = obj.form
	thisObject =  thisForm[objectName]

			Sel   = thisObject.options.selectedIndex;
			SelTo = thisObject.options.selectedIndex + moveIndex;
			Last = thisObject.length
      if ((SelTo >= 0) && (SelTo <= Last-1)) {
			    tempvalue  = thisObject.options[Sel].value;
			    temptext  = thisObject.options[Sel].text;
			    thisObject.options[Sel].value    = thisObject.options[SelTo].value;
			    thisObject.options[Sel].text    = thisObject.options[SelTo].text;
			    thisObject.options[SelTo].value  = tempvalue;
			    thisObject.options[SelTo].text  = temptext;
				thisObject.options.selectedIndex = SelTo;
 			}		
		updateHiddenField(objectName)
	}


 
  SelAll_ = function (formName,objectName) {
 
	//var thisForm = eval('document.'+formName);
	var thisForm = 	document[formName]	
		
	for (thisElement=0; thisElement<thisForm.elements.length; thisElement++) {

		if  ( thisForm.elements[thisElement].name.substr(0,objectName.length) == objectName) {

			if (thisForm.elements[thisElement].type== 'select-multiple') {

				thisObject =  thisForm.elements[thisElement]
  			    thisObject.multiple = true;
				SelectAllItems(thisObject)

			}
		}
																									
	}

}

 SelectAllItems =function (obj) {
		//LH 2280 SSS 2009-06-12 the length had a -1 which meant that the last element was never saved
		//      WAB - this was -1 because there used always to be a blank row used top force the width.  Now check for value != ''
		for (i=0;i<obj.length;i++) {
			if (obj.options[i].value != '') {
				obj.options[i].selected = true;
			}
		}
		return ''   //
}


/* WAB 2012-02-20  Added these two functions to support isdirty handling - see checkObject.isDirty() for more info 
		Can't use normal methods to check for dirtiness, because the options are not selected until submit time
	WAB 2013-05-23 removed the isDirty function because we now store the data in a hidden field can work out if it is dirty
*/

 getOrderedOptionsList = function (obj) {
	/* create a list of all the options in a multiselect, ordered*/

				var myArray=new Array()
				for(var j=0,len=obj.options.length; j<len; j++)   { 
					if (obj.options[j].value != '') {
						myArray.push (obj.options[j].value)
					}	
				}
				return (myArray.join(','))
			}


/* 	
	NJH 2013/03/25 - puts values of the second select into a hidden field so that we don't need to call the submitFunction after every change 
	WAB 2013-04-09	jquery selector did not work with elements with dots in the id  (in settings).  Replaced jquery qith call to existing function getOrderedOptionsList()
*/
 updateHiddenField = function(selectBoxID) {

	var hiddenFieldID = selectBoxID.replace('_select','');
	var hiddenObj = $(hiddenFieldID)
	if (hiddenObj && selectBoxID.match(/_select$/i)) {
		hiddenObj.value = getOrderedOptionsList ($(selectBoxID))
	}


}

