/* �Relayware. All Rights Reserved 2014 */
/*
WAB 2010/04/28  some of our own functions for doing Ajaxy things
WAB 2010/05/18  Added options to deal with nullText
PPB 2010/08/16  just 'touched' file to get it into release

*/

	/*
	This function is based on the one in cfajax.js, with our own code for dealing with optgroups, selected items etc.
	*/

	function populateSelect(obj,values, options) {


		obj = $(obj)
		options =  options || { }	
		defaultOptions = {valueColumn : 'VALUE',displayColumn:'DISPLAY',optgroupColumn:'OPTGROUP',selectedColumn :'SELECTED',selected:'',nullText:'',keepFirstItem:false}
		options = Object.extend (defaultOptions,options)
		var selectedValue = options.selected.toString()  // just to make sure that it is a string for comparison later 
		var valueCol = -1
		var displayCol = -1
		var optGroupCol = -1
		var selectedCol = -1
		
				/*
				WAB 2009/02/10
				This section modified to deal with OPTGROUPs
				If the query has a column called OPTGROUP then it is used
				*/
			
					for(var i=0;i<values.COLUMNS.length;i++){
						var col=values.COLUMNS[i];
						if(col==options.valueColumn.toUpperCase()){
							valueCol=i;
						}
						if(col==options.displayColumn.toUpperCase()){
							displayCol=i;
						}
						if(col == options.optgroupColumn.toUpperCase()) {
							optGroupCol = i;
						}
						if(col == options.selectedColumn.toUpperCase()) {
							selectedCol = i;
						}
					}
					if(valueCol==-1||displayCol==-1){
						alert ('values does not contain required fields')
						return;
					}


					firstItem = obj.options[0]   // this is saved in case it is the nullText which needs to be re-used
					// WAB 2009/02/10 also need to delete all Opt Groups (backwards 'cause IE recalculates length
					// this also removes all the options, so don't need obj.options.length=0
					for (var i = obj.childNodes.length-1;i>=0;i--) {
						obj.removeChild(obj.childNodes[i])
					}

					// can either leave the first item in the list (the 'Select an Item') 
					// or provide some text for the null item
					if (options.keepFirstItem) {
						var offset = 1
						obj.options[0] = firstItem
					} else if (options.nullText != '') {
						var opt=new Option(options.nullText,'');
						obj.options[0]=opt;
						offset  = 1 ;
					} else {
						offset  = 0 ;
					}

					if (optGroupCol == -1){
						for(var i=0;i<values.DATA.length;i++){
							var opt=new Option(values.DATA[i][displayCol],values.DATA[i][valueCol]);
							obj.options[i + offset]=opt;
							if (selectedCol != -1 && values.DATA[i][selectedCol]) {
								alert ('selecting 1')
								obj.options[i + offset].selected = true
							} else if (selectedValue == values.DATA[i][valueCol].toString()) {   // added .toString 'cause had problems if query column was numeric.  ended up with '' matching 0
								obj.options[i + offset].selected = true
							}
						}
					} else {

						// New code for dealing with OptGroups
						var currentOptGroup = ''		 
						for(var i=0;i<values.DATA.length;i++){
							if(values.DATA[i][optGroupCol]  != currentOptGroup) {
								// create optGroup Object
								var optGroupObject = document.createElement('optgroup')
								optGroupObject.label =  currentOptGroup = values.DATA[i][optGroupCol];
								obj.appendChild(optGroupObject) ;
							}
							var newOption = document.createElement('option');
							newOption.innerHTML   = values.DATA[i][displayCol]; 
							newOption.value = values.DATA[i][valueCol]

							optGroupObject.appendChild(newOption) 	

							if (selectedCol != -1 && values.DATA[i][selectedCol]) {
								obj.options[i + offset].selected = true
							} else if (selectedValue!= '' && selectedValue == values.DATA[i][valueCol]) {
								obj.options[i + offset].selected = true
							}

							
						}
					}
					
		}	
		
/*	
	populateSelectBoxViaAjax(
           objectname,    - object  (or id of object) to update
           cfcpath,            - path to cfc (relative to root, including.cfc )
           methodname,    - method in the cfc to call
           parameters,        - parameters to pass to the CFC  (eg: Param1=A&Param2=B)
           options            - an array 



The cfc function returns a query with all the values to be displayed.
	It can either return
		a) Just the query  (in which case it is compatible with the binding in cfselect)
	or 	b)	A Structure containing:
        		structure.isOK = true    (just because I like to be able to fail gracefully)
        		structure.VALUES  = theQuery 
		        if structure.isOK is false then you can set structure.ERRORMESSAGE
			
     by default the query must have columns called value and display
      can also have column optGroup (to create opt groups) 
      and selected  (set to 1 for that item to be selected)


At the moment, the available options are:
    Options to change the names of the columns used in the query
    {valueColumn : 'VALUE',displayColumn:'DISPLAY',optgroupColumn:'OPTGROUP',selectedColumn :'SELECTED'}

    An option to specify which options is selected
        {selected:''valueOfItemToselect'}
    [Only a single value - if you want to do multi-selects then use the selected column in the query]

	Options to deal with nullText  (ie the 'Select An Item')
	either use  nullText - this is added as the first item of the list
				keepFirstItem   true/false (default) this leaves the existing first item in the select box
	
*/
		function populateSelectBoxViaAjax(
			objectname,
			cfcpath,
			methodname,
			parameters,
			options
		) 
			{
					conjunction = (cfcpath.indexOf('?') == -1)? '?':'&' ;
					var page = cfcpath + conjunction +  'returnformat=json&_cf_nodebug=true&method=' + methodname 
					var myAjax = new Ajax.Request(
						page, 
						{
							method: 'post', 
							parameters: parameters, 
							onComplete:  function (requestObject,JSON) {
								json = requestObject.responseText.evalJSON(true)
								if (!json) {
									alert ('json not parsed')
									return false
								} else {
									if (json.ISOK == undefined) {
										// ISOK not defined, therefore must be a CF compatible webservice call which is just bringing back the queryobject
										var values = json
									} else {
										if (!json.ISOK) {
											if (json.ERRORMESSAGE) {
												alert (json.ERRORMESSAGE)
											}
											return false
										}
										var values= json.VALUES
									}	
								}
								populateSelect(objectname,values,options)
								if (options.onCompleteFunction != undefined) {
									options.onCompleteFunction()
								}
								return true
							},
							debug:false
						});   


	}
		
	/* 
		WAB/NJH 2010/10/12
		pass in a select object the value of the selected item
		Could be extended to handle multi-selects
		WAB 2011/02/03 added a setAsDefaultSelected, this sets the .defaultSelected value so that the isDirty function can be used successfully
		ACPK 2014-01-12 extended function to handle multiple selected items 
		NJH 2015/07/02 Handle case when selected is null. Also convert selected to a string, so that functions like .toUpperCase work as expected.
	*/
	
	
	function setSelected (selectObj,selected,setAsDefaultSelected) {
		if (selected !== null) {
			selected = String(selected);
			selectedArray = selected.toUpperCase().split(',')
			for (each in selectedArray) {
				for (var i = 0;i<selectObj.options.length;i++) {
					if (selectObj.options[i].value.toUpperCase() == selectedArray[each]) {
						selectObj.options[i].selected = true
						if (setAsDefaultSelected){selectObj.options[i].defaultSelected = true}
					}
				}
			}
		}
	}
		
		