/* �Relayware. All Rights Reserved 2014 */
	function saveChecks (entityType) {
		return saveChecksNew (entityType, 'mainForm')
	}
	
	function saveChecksNew (entityType, myform) {
		// create a string of all the checked entities
		var form = eval('document.'+ myform);

		if(!form){return null}   // if form does not exist then return nothing  WAB05/12/06 to get rid of a minor JS bug

		var checkedItems = "0";
		// work out name of checkbox
		checkBoxName = 'frm'+entityType+'Check'
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
		
			if (thiselement.name == checkBoxName) {
				if (thiselement.checked  && thiselement.value != '') {
					if (checkedItems == "0") {
						checkedItems = thiselement.value;
					} else {
						checkedItems += "," + thiselement.value;
					}
				
				}
			}
		}
		return checkedItems;
	}

	//2011 RMB LID5031 For warning message to confirm that only one location can be moved
	function countLocations (entityType) {
		return countLocationsNew (entityType, 'mainForm')
	}
	
	function countLocationsNew (entityType, myform) {
		// create a string of all the checked entities
		var form = eval('document.'+ myform);

		if(!form){return null}   // if form does not exist then return nothing  WAB05/12/06 to get rid of a minor JS bug

		var checkedItems = "0";
		var countlocations = 0;
		// work out name of checkbox
		checkBoxName = 'frm'+entityType+'Check'
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
		

			if (thiselement.name == checkBoxName) {
				if (thiselement.checked  && thiselement.value != '') {
					if (checkedItems == "0") {
						checkedItems = thiselement.value;
						countlocations = countlocations + 1;
					} else {
						checkedItems += "," + thiselement.value;
						countlocations = countlocations + 1;
					}
				
				}
			}
		}
		return countlocations;
	}

	
	function noChecks(entityType){
				if (entityType == 'Person'){
					entityPlural ='People'
				}else {
					entityPlural = entityType + 's'	
				}			
				alert('You have not checked any ' + entityPlural)
			
	}
	
	function multiChecks(entityType){
				if (entityType == 'Person'){
					entityPlural ='People'
				}else {
					entityPlural = entityType + 's'	
				}			
				alert('You can only check one ' + entityType)
			
	}
	
	
	
	
	
	function doChecks (entityType, setting) {
	doChecksNew (entityType, setting, 'mainForm')
	
	}
	
	function doChecksNew (entityType, setting, myform) {
//	checks or unchecks all checkboxes on page		
			var form = eval('document.'+ myform);

		// work out name of checkbox
		checkBoxName = 'frm'+entityType+'Check'

		for (j=0; j<form.elements.length; j++){

			thiselement = form.elements[j];

			if (thiselement.name == checkBoxName) { 
				thiselement.checked = setting
			}
		}

	}
	
	
	//not updated
		function countChecks() {
		// count all the checked organisations
		var form = document.ThisForm;
		var count = 0;
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmCheckIDs') { 
				if (thiselement.checked) {
					count = count+1
					
				}
			}
		}
		return count
	}	
	
	
		// toggles the state of checkboxes 
		//  if state is passed then toggle to that state, otherwise based on the value of the first item  (ie if first item is currently checked then all items are unchecked)
		// form   - formobject
		// checkBoxName  - name of checkbox to toggle
		// valueRe - regular expression so that only checkboxes with a certain value are done eg: /./
 
		function toggleCheckboxes (form,checkBoxName,valueRe,newCheckedStatus) {
			firstItem = true
			for (j=0; j<form.elements.length; j++){
				thiselement = form.elements[j];
					if (thiselement.name == checkBoxName) {
						if (valueRe.test(thiselement.value)) {
							if (firstItem && newCheckedStatus == undefined) {
								newCheckedStatus = !thiselement.checked
								firstItem = false
							}
							thiselement.checked = newCheckedStatus
						}
					}
				
			}
		}
		
		
		function tagCheckboxes(name,check) {
			jQuery('input[name='+name+']').attr('checked',check)	
		}
