/* �Relayware. All Rights Reserved 2014 */
/*--------------------------------------------------|
| dTree 2.05 | www.destroydrop.com/javascript/tree/ |
|---------------------------------------------------|
| Copyright (c) 2002-2003 Geir Landr�               |
|                                                   |
| This script can be used freely as long as all     |
| copyright messages are intact.                    |
|                                                   |
| Updated: 17.04.2003                               |
|--------------------------------------------------*/
/*  
This version was hacked by WAB Jan 06 so that it could be used as a menuing system.
	Added various functions !
	


*/

// Node object
function Node(id, pid, name, url, title, target, icon, iconOpen, open) {
	this.id = id;
	this.pid = pid;
	this.name = name;
	this.url = url;
	this.title = title;
	this.target = target;
	this.icon = icon;
	this.iconOpen = iconOpen;
	this._io = open || false;   // is open
	this._is = false;			// is selected
	this._ls = false;			// lastSibling
	this._hc = false;    // has children
	this._ai = 0;			// item in node array
	this._p;			// parent object
};

// Tree object
function dTree(objName) {
	this.config = {
		target					: null,
		folderLinks			: true,
		useSelection		: true,
		useCookies			: true,
		useLines				: true,
		useIcons				: true,
		useStatusText		: false,
		closeSameLevel	: false,
		inOrder					: false,
		treeMenu				:false,
		useParentSelection		: true,
		forceTopLevelIcon				:false
		
	}

		var path = '/images/dtree/dtree_' ;
		
	this.icon = {
		root				: path + 'base.gif',
		folder			: path + 'folder.gif',
		folderOpen	: path + 'folderopen.gif',
		node				: path + 'page.gif',
		empty				: path + 'empty.gif',
		line				: path + 'line.gif',
		join				: path + 'join.gif',
		joinBottom	: path + 'joinbottom.gif',
		plus				: path + 'plus.gif',
		plusBottom	: path + 'plusbottom.gif',
		minus				: path + 'minus.gif',
		minusBottom	: path + 'minusbottom.gif',
		nlPlus			: path + 'nolines_plus.gif',
		nlMinus			: path + 'nolines_minus.gif'
	};

	
	this.obj = objName;
	this.aNodes = [];
	this.aIndent = [];
	this.root = new Node(-1);
	this.selectedNode = null;
	this.selectedFound = false;
	this.completed = false;
};

// Adds a new node to the node array
dTree.prototype.add = function(id, pid, name, url, title, target, icon, iconOpen, open) {
	this.aNodes[this.aNodes.length] = new Node(id, pid, name, url, title, target, icon, iconOpen, open);
};

// Open/close all nodes
dTree.prototype.openAll = function() {
	this.oAll(true);
};
dTree.prototype.closeAll = function() {
	this.oAll(false);
};

// Outputs the tree to the page
dTree.prototype.toString = function() {
	var str = '<div class="dtree">\n';
	if (document.getElementById) {
		if (this.config.useCookies) this.selectedNode = this.getSelected();
		str += this.addNode(this.root);
	} else str += 'Browser not supported.';
	str += '</div>';
	if (!this.selectedFound) this.selectedNode = null;
	this.completed = true;
	return str;
};

// Creates the tree structure
dTree.prototype.addNode = function(pNode) {
	var str = '';
	var n=0;
	if (this.config.inOrder) n = pNode._ai;
	for (n; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == pNode.id) {
			var cn = this.aNodes[n];
			cn._p = pNode;
			cn._ai = n;
			this.setCS(cn);
			if (!cn.target && this.config.target) cn.target = this.config.target;
			if (cn._hc && !cn._io && this.config.useCookies) cn._io = this.isOpen(cn.id);
			if (!this.config.folderLinks && cn._hc &&!this.config.treeMenu) cn.url = null; // if there are no folder links then click needs to open the children, but not for treemenus
			if (this.config.useSelection && cn.id == this.selectedNode && !this.selectedFound) {
					cn._is = true;
					this.selectedNode = n;
					this.selectedFound = true;
			}
			str += this.node(cn, n);
			if (cn._ls) break;
		}
	}
	return str;
};

// Creates the node icon, url and text
dTree.prototype.node = function(node, nodeId) {
	var str = '\n\n<div id="div' + this.obj + nodeId + '" class="node" onfocus="javascript: ' + this.obj + '.divFocus(' + nodeId + ')"><div  class="iconArea" >' + this.indent(node, nodeId)  + '</div>'; 
	if (this.config.useIcons) {
		if (!node.icon) node.icon = (this.root.id == node.pid) ? this.icon.root : ((node._hc) ? this.icon.folder : this.icon.node);
		if (!node.iconOpen) node.iconOpen = (node._hc) ? this.icon.folderOpen : this.icon.node;
		if (this.root.id == node.pid) {
			node.icon = this.icon.root;
			node.iconOpen = this.icon.root;
		}
		str += '<img id="i' + this.obj + nodeId + '" src="' + ((node._io) ? node.iconOpen : node.icon) + '" alt="" />';
	}

	str += '<div id = "aTextArea" class="textArea">';   // wab added textarea div

	if (node.url) {
		str += '<a id="s' + this.obj + nodeId + '" class="' + ((this.config.useSelection) ? ((node._is ? 'nodeSel' : 'node')) : 'node') + '" href="' + node.url + '"';
		if (node.title) str += ' title="' + node.title + '"';
		if (node.target) str += ' target="' + node.target + '"';
		if (this.config.useStatusText) str += ' onmouseover="window.status=\'' + node.name + '\';return true;" onmouseout="window.status=\'\';return true;" ';
		if (this.config.useSelection && ((!this.config.treeMenu  && node._hc && this.config.folderLinks) || !node._hc))
			{str += ' onclick="javascript: ' + this.obj + '.openAndSelectAndCloseOthers(' + nodeId + ');"';}  // was .s

		// for folders the click needs to open and select
		if (this.config.treeMenu && node._hc && this.config.folderLinks)
			{str += ' onclick="javascript:' + this.obj + '.openAndSelectAndCloseOthers(' + nodeId + ',1);"';}   // was.o
		str += '>';
	}
	else if ((!this.config.folderLinks || !node.url) && node._hc && node.pid != this.root.id)
		str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');" class="node">';

	str += '<div class="nodeNameSpacer"></div><div class="nodeName">'+node.name+'</div>';   //NJH 2006/09/15 added the two divs to provide more handles for styling - nodeNameSpacer is used for min-height issues...

	if (node.url || ((!this.config.folderLinks || !node.url) && node._hc)) str += '</a>';

	str += '</div>';
	
	str += '</div>';
//	if (this.config.treeMenu)str += '<div id="sep" class="xxdTreeSep"></div>';

	if (node._hc) {
		if (node.pid !=-1) {str += '<div id="d' + this.obj + nodeId + '" class="clip" style="display:' + ((this.root.id == node.pid || node._io) ? 'block' : 'none') + ';">';}
		str += this.addNode(node);
		if (node.pid !=-1) {str += '</div>';}
	}
	this.aIndent.pop();
	return str;
};

// Adds the empty and line icons
dTree.prototype.indent = function(node, nodeId) {
	var str = '';
	if (this.root.id != node.pid) {
		for (var n=0; n<this.aIndent.length; n++)
			str += '<img class="indentIcon'+n+'" src="' + ( (this.aIndent[n] == 1 && this.config.useLines) ? this.icon.line : this.icon.empty ) + '" alt="" />';
		(node._ls) ? this.aIndent.push(0) : this.aIndent.push(1);
		if (node._hc) {
			str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');"><img class="lineIcon" id="j' + this.obj + nodeId + '" src="';
			if (!this.config.useLines) str += (node._io) ? this.icon.nlMinus : this.icon.nlPlus;
			else str += ( (node._io) ? ((node._ls && this.config.useLines) ? this.icon.minusBottom : this.icon.minus) : ((node._ls && this.config.useLines) ? this.icon.plusBottom : this.icon.plus ) );
			str += '" alt="" /></a>';
		} else if (this.config.forceTopLevelIcon && node.pid == this.config.treeTop) {
		str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');"><img class="lineIcon" id="j' + this.obj + nodeId + '" src="';
		if (!this.config.useLines) str += (node._io) ? this.icon.nlMinus : this.icon.nlPlus;
		str += '" alt="" /></a>';
		} 
		else str += '<img class="lineIcon" src="' + ( (this.config.useLines) ? ((node._ls) ? this.icon.joinBottom : this.icon.join ) : this.icon.empty) + '" alt="" />';
	}
	return str;
};

// Checks if a node has any children and if it is the last sibling
dTree.prototype.setCS = function(node) {
	var lastId;
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id) node._hc = true;
		if (this.aNodes[n].pid == node.pid) lastId = this.aNodes[n].id;
	}
	if (lastId==node.id) node._ls = true;
};

// Returns the selected node
dTree.prototype.getSelected = function() {
	var sn = this.getCookie('cs' + this.obj);
	return (sn) ? sn : null;
};

// Highlights the selected node
dTree.prototype.s = function(id) {
	
	if (!this.config.useSelection) return;
	var cn = this.aNodes[id];
	if (cn._hc && !this.config.folderLinks) return;
	if (this.selectedNode != id) {
		// alert ('selecting ' + id)
		if (this.config.useParentSelection) {
			newParentArray = this.getParents(this.aNodes[id])
		}

		if (this.selectedNode || this.selectedNode==0) {
			// unselect the last selected node
			itemsToUnSelect = new Array (0)
			if (this.config.useParentSelection) {
				// make array of previously selected and its parents
					oldSelectedArray = this.getParents(this.aNodes[this.selectedNode])
					oldSelectedArray = oldSelectedArray.concat (this.selectedNode )
				// unselect previouslySelected, if not parents of newly selected node			
					for (var i=0;i<oldSelectedArray.length;i++) {
						if (this.arrayFind(newParentArray,oldSelectedArray[i]) == -1 || this.selectedNode == 0) { // special case if root node is currently selected
								itemsToUnSelect.push (oldSelectedArray[i])
						}		
					}
			} else {
				itemsToUnSelect.push (this.selectedNode)
			}

			 //alert ('itemsToUnSelect	' + itemsToUnSelect)
			for (i=0;i<itemsToUnSelect.length;i++) {
				node = itemsToUnSelect[i]
			//	if (node != 0) {
					eOld = document.getElementById("s" + this.obj + node);
					eOld.className = "node";
					dOld  = document.getElementById("div" + this.obj + node);
					dOld.className = "node";
			//	}	
			}
			
		}


		itemsToSelect = new Array (0)
		itemsToSelect.push (id)
		if (this.config.useParentSelection) {
			itemsToSelect = itemsToSelect.concat(newParentArray)
		}

			// alert ('itemsToSelect ' + itemsToSelect)


		for (i=0;i<itemsToSelect.length;i++) {
			node = itemsToSelect[i]
			if (node != 0 || id == 0) { // special case, root node is not selected as a parent only if it is the actual item   
				eNew = document.getElementById("s" + this.obj + node);
				eNew.className = "nodeSel";
				dnew = document.getElementById("div" + this.obj + node);
				dnew.className = "nodeSel";
			}
		}
			
		this.selectedNode = id;
		if (this.config.useCookies) this.setCookie('cs' + this.obj, cn.id);
	}
};


// Toggle Open or close
dTree.prototype.o = function(id) {
	// alert ('o function ' + id)
	var cn = this.aNodes[id];
	this.nodeStatus(!cn._io, id, cn._ls);
	cn._io = !cn._io;
	if (this.config.closeSameLevel) this.closeLevel(cn);
	if (this.config.useCookies) this.updateCookie();
};

// Open Node and Select it and then close any items at same level
dTree.prototype.openAndSelectAndCloseOthers = function(id) {
	if (id != 0) {
		this.openTo (id,1);
	} else {
		// this.openTo does not seem to work for root node, so had to add a special case
		this.s(id); 
	}
	this.closeAllNotParents	(this.aNodes[id])

}

// Open or close all nodes
dTree.prototype.oAll = function(status) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._hc && this.aNodes[n].pid != this.root.id) {
			this.nodeStatus(status, n, this.aNodes[n]._ls)
			this.aNodes[n]._io = status;
		}
	}
	if (this.config.useCookies) this.updateCookie();
};

// Opens the tree to a specific node
dTree.prototype.openTo = function(nId, bSelect, bFirst) {
	if (!bFirst) {
		for (var n=0; n<this.aNodes.length; n++) {
			if (this.aNodes[n].id == nId) {
				nId=n;
				break;
			}
		}
	}
	var cn=this.aNodes[nId];
	if (cn.pid==this.root.id || !cn._p) return;
	cn._io = true;
	cn._is = bSelect;
	if (this.completed && cn._hc) this.nodeStatus(true, cn._ai, cn._ls);
	if (this.completed && bSelect) this.s(cn._ai);
	else if (bSelect) this._sn=cn._ai;
	this.openTo(cn._p._ai, false, true);
};


// Closes all nodes on the same level as certain node
dTree.prototype.closeLevel = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {

		if (this.aNodes[n].pid == node.pid && this.aNodes[n].id != node.id && this.aNodes[n]._hc) {

			this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);
		}
	}
}



// Closes all children of a node
dTree.prototype.closeAllChildren = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id && this.aNodes[n]._hc) {
			if (this.aNodes[n]._io) this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);		
		}
	}
}





// Change the status of a node(open or closed)
dTree.prototype.nodeStatus = function(status, id, bottom) {
	eDiv	= document.getElementById('d' + this.obj + id);
	eJoin	= document.getElementById('j' + this.obj + id);
	if (this.config.useIcons) {
		eIcon	= document.getElementById('i' + this.obj + id);
		eIcon.src = (status) ? this.aNodes[id].iconOpen : this.aNodes[id].icon;
	}
	eJoin.src = (this.config.useLines)?
	((status)?((bottom)?this.icon.minusBottom:this.icon.minus):((bottom)?this.icon.plusBottom:this.icon.plus)):
	((status)?this.icon.nlMinus:this.icon.nlPlus);
	eDiv.style.display = (status) ? 'block': 'none';
};


// [Cookie] Clears a cookie
dTree.prototype.clearCookie = function() {
	var now = new Date();
	var yesterday = new Date(now.getTime() - 1000 * 60 * 60 * 24);
	this.setCookie('co'+this.obj, 'cookieValue', yesterday);
	this.setCookie('cs'+this.obj, 'cookieValue', yesterday);
};

// [Cookie] Sets value in a cookie
dTree.prototype.setCookie = function(cookieName, cookieValue, expires, path, domain, secure) {
	document.cookie =
		escape(cookieName) + '=' + escape(cookieValue)
		+ (expires ? '; expires=' + expires.toGMTString() : '')
		+ (path ? '; path=' + path : '')
		+ (domain ? '; domain=' + domain : '')
		+ (secure ? '; secure' : '');
};

// [Cookie] Gets a value from a cookie
dTree.prototype.getCookie = function(cookieName) {
	var cookieValue = '';
	var posName = document.cookie.indexOf(escape(cookieName) + '=');
	if (posName != -1) {
		var posValue = posName + (escape(cookieName) + '=').length;
		var endPos = document.cookie.indexOf(';', posValue);
		if (endPos != -1) cookieValue = unescape(document.cookie.substring(posValue, endPos));
		else cookieValue = unescape(document.cookie.substring(posValue));
	}
	return (cookieValue);
};

// [Cookie] Returns ids of open nodes as a string
dTree.prototype.updateCookie = function() {
	var str = '';
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._io && this.aNodes[n].pid != this.root.id) {
			if (str) str += '.';
			str += this.aNodes[n].id;
		}
	}
	this.setCookie('co' + this.obj, str);
};

// [Cookie] Checks if a node id is in a cookie
dTree.prototype.isOpen = function(id) {
	// alert ('isOpen function')
	var aOpen = this.getCookie('co' + this.obj).split('.');
	for (var n=0; n<aOpen.length; n++)
		if (aOpen[n] == id) return true;
	return false;
};

// If Push and pop is not implemented by the browser
if (!Array.prototype.push) {
	Array.prototype.push = function array_push() {
		for(var i=0;i<arguments.length;i++)
			this[this.length]=arguments[i];
		return this.length;
	}
};
if (!Array.prototype.pop) {
	Array.prototype.pop = function array_pop() {
		lastElement = this[this.length-1];
		this.length = Math.max(this.length-1,0);
		return lastElement;
	}
};

dTree.prototype.divFocus = function(id) {
/*	alert ('divFocus' + id)
	if (this.aNodes[id]._hc) {
			alert (this.aNodes[id].url)
			this.o(id)

	} else {
		this.closeLevel(this.aNodes[id])
	}		
*/	
}



// Closes all nodes which are not in the branch to the given node
dTree.prototype.closeAllNotParents = function(node) {
	// get array of all parents
	parentArray = this.getParents (node)
	
	// loop through all the nodes looking for open ones which are not in this array
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._io && n != node._ai && this.aNodes[n]._hc) {
			if (this.arrayFind(parentArray,n) == -1) {
				// if n is not in the array of parents
				this.nodeStatus(false, n, this.aNodes[n]._ls);
				this.aNodes[n]._io = false;
				this.closeAllChildren(this.aNodes[n]);
			}
		}
	}	


}


// gets array of parents (internal ids)
dTree.prototype.getParents = function(node) {

	var parentArray = new Array (0)

	while (node._p &&  node._p.id != -1) {
	parentArray.push(node._p._ai)
		node = node._p
	}

	return parentArray
}




// find a value in an array
// returns position in array if found or -1
dTree.prototype.arrayFind = function(array,value) {  
		for (var a=0; a<array.length; a++) {			
			if 	(array[a] == value)  {
				return a
			}
		}	 
 	return  -1
}



dTree.prototype.getNodeByRef=function (ref) {

	for(var i=0;i<this.aNodes.length;i++){
		if (this.aNodes[i].id == ref){
			return i
		}
	}

	return null

}

dTree.prototype.openToLowestRefInList = function  (list)  {

	refArray = list.split(',') ;
	
	for (i=refArray.length-1;i>=0;i--) {
		thisNode = this.getNodeByRef (refArray[i]) 

		if (thisNode != null)
			{
				// alert ('about to open ' + thisNode + ' and close other')
				this.openAndSelectAndCloseOthers(thisNode)
				return
			}
	}


	return
}


// open all children of a node (just one level)
dTree.prototype.openAllChildren = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id && this.aNodes[n]._hc) {
			if (! this.aNodes[n]._io) this.nodeStatus(true, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = true;
		}
	}
}

dTree.prototype.openAllChildrenById = function(id) {
	openAllChildren (this.aNodes[id])
}

dTree.prototype.openAllChildrenByRef = function(ref) {
	this.openAllChildren (this.aNodes[this.getNodeByRef(ref)])
}
