/* �Relayware. All Rights Reserved 2014 */
/* 
WAB 2012-05-14 CASE 434001  

Previous code for detecting jasperserver login in an IFrame was not working.
Created this function for use on both internal and external sites

The window.jasperServerLoggedIn variable is updated after the jasperServerLogin.cfm page makes a call to Jasperserver and receives a response.  
However because of XSS restrictions we can't actually parse the response - so it is conceivable that the login will have failed
It is of particular use when the home page contains JasperServer dashboards which we can't load until the login process has been completed.
*/
		
function jasperserverLoadDetect (iframeID) {

	
	if (typeof(window.jasperServerLoggedIn) == 'undefined') {
		window.jasperServerLoggedIn = false;
	}
	
	var listener = 
		function () {
			if (!window.jasperServerLoggedIn) {

				var thirdPartyDomain = false

				try {
					// try to access the location href
					// if the href is a thirdpartydomain (ie jasperserver has replied) then some browsers will throw and error others will return a null, but either way we can detect it
					var hrefTest = this.contentWindow.location.href
					if (hrefTest == null) {
						thirdPartyDomain = true
					}
				}

				catch (event)
				 { 
						thirdPartyDomain = true
			 	}

				
				if (thirdPartyDomain) {

					$(iframeID).stopObserving(listener)
					window.jasperServerLoggedIn = true;
					// update the coldfusion variable		
					var page = '/webservices/user.cfc?wsdl&method=setIsJasperServerLoginConfirmed&_cf_noDebug=true&returnFormat=json';
				
					var myAjax = new Ajax.Request(
						page,
						{
							method: 'post',
							debug : false,
							asynchronous: false
						}
					);

				}

			}
		}			

	$(iframeID).observe('load',listener)
}
