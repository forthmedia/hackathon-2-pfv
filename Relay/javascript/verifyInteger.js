/* �Relayware. All Rights Reserved 2014 */
// Javascript function for verifying whether an object is an integer or a number depending 
// upon the parameter 'checkType'. If the object is a number a check is made that the 
// number of decimal points 'checkDecimals' is not exceeded (default is 2). 

function verifyInteger(thisObject, checkType, checkDecimals) {
	var newLength = thisObject.length
	    decimalpoint = false
	    decimalcount = 0
	    msg = ''	
			
	if (arguments.length != 3){
		checkDecimals = 2
	}
			
	for(var i = 0; i != newLength; i++) {
		aChar = thisObject.substring(i,i+1)
		if (aChar < "0" || aChar > "9") {
			if (checkType == 'number'){
				if (aChar == '.'){
					if (decimalpoint){
						msg = 'Invalid number - more than one decimal point\n';
						return msg;
					}	
					else
					{
						decimalpoint = true
					}	
				}
				else
				{
					msg = 'Invalid number - alpha characters \n';
					return msg;
				}
			}	
			else
			{
				msg = 'Invalid integer \n';
				return msg;
			}	
		}
		else
		{
			if (checkType == 'number') {
				if (decimalpoint) {
					decimalcount++
					if (decimalcount > checkDecimals){
						msg = 'Invalid number - exceeded number of '+checkDecimals+' decimal places \n';			
						return msg;
					}
				}
			}
		}				
	}
	return msg
}