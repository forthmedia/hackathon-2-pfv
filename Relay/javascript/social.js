/* �Relayware. All Rights Reserved 2014 */
/*
Change Log
2014-11-20 AHL Case 442516 Activity stream does not refresh if no data in initial response
*/

//phr.social_stopFollowing - used so that the phrases will get translated
//phr.social_startFollowing

var activityFeedDateTime = DBServerTime;
	
function runActivityRefresh() {	
	setTimeout(getNewActivityFeeds,20000); // 20 secs
}

function follow(entityTypeID,entityID,action) {
	page = '/webservices/callWebService.cfc?'
	
	jQuery.ajax({
		type: "POST",
		url: "/webservices/callWebService.cfc?",
		data: {method:'callWebService',webServiceName:'socialWS',methodName:'saveFollow', entityTypeID:entityTypeID, entityID:entityID,returnFormat:'json'},
		dataType: "json",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			if (data) {
				id = 'a.'+action+'Follow_'+entityID;
				newAction = 'stop'
				if (action == 'stop') {
					newAction = 'start';
				}
				newClass = newAction+'Follow_'+entityID;
				jQuery(id).html(phr['social_'+newAction+'Following'])
				jQuery(id).attr('onClick','follow('+entityTypeID+',\''+entityID+'\',\''+newAction+'\');return false;');
				jQuery(id).attr('id',newAction);
				jQuery(id).attr('class',newClass);
			}
		}
	})
}

function showActivityStream(pageNumber,filterType) {
	
	if (filterType == undefined) {
		filterType = globalFilterType;
	}
	if (globalFilterType != filterType) {
		resetActivityStream();
		jQuery('#'+filterType+'FilterType').attr('class','selected');
		jQuery('#'+filterType+'FilterType').parent('li').attr('class','selected');
		jQuery('#'+globalFilterType+'FilterType').parent('li').attr('class','');
		jQuery('#'+globalFilterType+'FilterType').attr('class','');
	}
	globalFilterType = filterType;

	page = '/webservices/callWebService.cfc?'
	
	jQuery.ajax({
		type: "GET",
		url: page,
		data: {method:'callWebService',webServiceName:'socialWS',methodName:'getActivityStream',pageNumber:pageNumber,filterType:filterType,returnFormat:'json',_cf_nodebug:true},
		dataType: "json",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			// 2014-11-20 AHL Case 442516 If no data add a dummy span
			if (data == "") {
				data = "<span id='actStreamData'></span>";
			}
			jQuery('#actStreamData').parent().html(data);
			// have to test for existance here, as on the first load, resetNextHref does not yet exist
			if (jQuery('#activityStream').jscroll.resetNextHref) {
				jQuery('#activityStream').jscroll.resetNextHref(data); // set the next data.href variable
			}
			setExpander(); // need to call this here again for the new content that have been displayed
		}
	})
}

function resetActivityStream() {
	// normally, there is a table that the activity stream is in. However, if there is no activity stream, then we need to find the div with class .jscroll-inner
	parentObj = jQuery('#activityStream').find('table').parent();
	if (!parentObj.length) {
		parentObj = jQuery('#activityStream').find('.jscroll-inner');
	}
	parentObj.html('<img id="actStreamData" src="/images/icons/loading.gif" alt="Loading"/>')
}

function getNewActivityFeeds() {

	page = '/webservices/callWebService.cfc?'
	
	jQuery.ajax({
		type: "GET",
		url: page,
		data: {method:'callWebService',webServiceName:'socialWS',methodName:'getNewActivityFeeds',postedSince:activityFeedDateTime,filterType:globalFilterType,returnFormat:'json',_cf_nodebug:true,noTickle:true},
		dataType: "json",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			if (data.CONTENT != '') {
				jQuery(data.CONTENT).insertBefore('#activityStream table:first');
			}
			activityFeedDateTime=data.LASTPOST;
			runActivityRefresh();
		}
	})
}


function saveLike(typeid,entityid,object,target) {
	
	jQuery.ajax({
		type: "POST",
		url: "/WebServices/socialWS.cfc?method=saveLike&_cf_nodebug=true",
		data: {entityTypeID:typeid, entityID: entityid},
		dataType: "text",
		context: object,
		cache: false,
		error: function(){alert('Error saving Like.')},
		success: function (likes){		
			var imgSrc = '/images/social/icons/like.png';
				
			jQuery(this).parents('.links').find(target).html(jQuery.trim(likes));
			var titleAttr = jQuery(this).attr('title');
			
			if(jQuery.trim(jQuery(this).html()) == phr.social_like){
				imgSrc = '/images/social/icons/like.png';
				
				jQuery(this).html(phr.social_unlike);
				if(typeof titleAttr !== "undefined"){
					titleAttr = titleAttr.replace(phr.social_like,phr.social_unlike);
				}
			} else {
				imgSrc = '/images/social/icons/like_disabled.png';
				jQuery(this).html(phr.social_like);
				if(typeof titleAttr !== "undefined"){
					titleAttr = titleAttr.replace(phr.social_unlike,phr.social_like);
				}
			}
			jQuery(this).parents('.links').find(target).parent().find('img').attr('src',imgSrc);
			jQuery(this).attr('title',titleAttr);
		}
	});
	return false;
}


function getPersonSocialProfile(personID) {
	var page = '/webservices/callWebService.cfc?';    
	var parameters = {method:'callWebService',webServiceName:'socialWS',methodName:'getPersonSocialProfile',personID:personID,returnFormat:'plain'};
	
	jQuery('#pageGrey').css("display","block");
	jQuery('#personSocialProfile').css("display","block");
	
	var myAjax = new Ajax.Updater(
		"personSocialProfileContainer",
		page, 
		{
			method: 'post', 
			parameters: parameters
		});	
}

function hidePersonSocialProfile() {
	jQuery('#pageGrey').css("display","none");
	jQuery('#personSocialProfile').css("display","none");
	jQuery('#personSocialProfileContainer').html("<div id='personSocialProfileLoading'><p><img src='/images/icons/loading.gif'></p></div>");
}


function unLinkAccount(serviceID,entityID,entityTypeID) {
	
	var page = '/webservices/serviceWS.cfc?wsdl&method=unlinkEntityFromService';
	parameters = 'serviceID='+serviceID+'&entityID='+entityID+'&entityTypeID='+entityTypeID;

	var myAjax = new Ajax.Request(
				page, 
				{
					method: 'post',    
					parameters:  parameters, 
					asynchronous:false,
					onComplete: function(){ window.location.href=window.location.href }
				}
			);
}

function shortenURL(url,callbackfunction) {
	shortenedURL = url;
	
	if (url != '') {
		page = '/webservices/callWebService.cfc?'
		jQuery.ajax({
				type: "GET",
				url: page,
				data: {method:'callWebService',webServiceName:'socialWS',methodName:'shortenURL',url:url,returnFormat:'json',_cf_nodebug:true},
				dataType: "json",
				cache: false,
				success: function(data,textStatus,jqXHR) {
					if (parseInt(data.status_code) == 200) {
						shortenedURL = data.data.url;
						callbackfunction (shortenedURL)
					} else if (!data.ISOK) {
						alert(data.MESSAGE);
					}
				}
			})
	}
	return true
}

function saveFollow(typeid,entityid,object,personid,target) {
	
	jQuery.ajax({
		type: "POST",
		url: "/webservices/callWebService.cfc?",
		data: {method:'callWebService',webServiceName:'socialWS',methodName:'saveFollow', entityTypeID:typeid, entityID:entityid, personID:personid,returnFormat:'json'},
		dataType: "json",
		context: object,
		cache: false,
		error: function(){alert('Error saving follow.')},
		success: function (follow){
			var targetHTML = '<img src="/images/social/icons/following.png" width="18" height="18">';

			if(jQuery.trim(follow.MESSAGE) == 'Deleted')
				targetHTML = '<img src="/images/social/icons/following_disabled.png" width="18" height="18">';
			jQuery(target).html(targetHTML);
			var titleAttr = jQuery(this).attr('title');
			
			if(targetHTML == '<img src="/images/social/icons/following_disabled.png" width="18" height="18">'){
				titleAttr = titleAttr.replace(phr.social_unfollow,phr.social_follow);
				var c = 'a#' + jQuery(this).attr('id');
				jQuery(c).html(phr.social_follow); 
			} else {
				titleAttr = titleAttr.replace(phr.social_follow,phr.social_unfollow);			
				var c = 'a#' + jQuery(this).attr('id');
				jQuery(c).html(phr.social_unfollow);
			}
			jQuery(this).attr('title',titleAttr)
		}
	});
	return false;
}

function saveFlag(typeid,entityid,object,target) {
	
	jQuery.ajax({
		type: "POST",
		url: "/WebServices/socialWS.cfc?method=saveFlag&_cf_nodebug=true",
		data: {entityTypeID:typeid, entityID: entityid},
		dataType: "text",
		context: object,
		cache: false,
		error: function(){alert('Error saving flag.')},
		success: function (flag){	
			var targetHTML = '<img src="/images/social/icons/flagged.png" width="18" height="18">';
			var titleAttr = jQuery(this).attr('title');
				
			if(jQuery.trim(flag) == 0)
				targetHTML = '<img src="/images/social/icons/flagged_disabled.png" width="18" height="18">';				
			jQuery(this).parent('.links').find(target).html(targetHTML);			
			if(targetHTML == '<img src="/images/social/icons/flagged_disabled.png" width="18" height="18">'){
				jQuery(this).html(phr.social_flag);
				titleAttr = titleAttr.replace(phr.social_unflag,phr.social_flag);
			} else {
				jQuery(this).html(phr.social_unflag);
				titleAttr = titleAttr.replace(phr.social_flag,phr.social_unflag);
			}
			jQuery(this).parents('.links').find(target).parent().find('img').replaceWith(targetHTML);
			jQuery(this).attr('title',titleAttr)		
		}
	});
	return false;
}


function addUrlShare(url,service,elementID) {
	
	jQuery.ajax({
		type: "POST",
		url: "/webservices/callWebService.cfc?",
		data: {method:'callWebService',webServiceName:'socialWS',methodName:'addElementShare', url:url,service:service, elementID:elementID,returnFormat:'json'},
		dataType: "json",
		cache: false
	});
	return false;
}

