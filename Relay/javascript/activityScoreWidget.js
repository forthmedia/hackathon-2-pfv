
jQuery(document).ready(function(){

    // Rosette flipping animation helper
    var $rosetteEl = jQuery('.scheme-wrapper');    
    $rosetteEl.each(function(){
      jQuery(this).on('mouseenter mouseleave click',function(event){
        event.stopPropagation();
        jQuery(this).toggleClass('flipped');
      });
    });

});