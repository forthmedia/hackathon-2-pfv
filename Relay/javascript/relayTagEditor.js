/* �Relayware. All Rights Reserved 2014 */
/*

code included in textArea control bar to allow relaytags to be edited


 WAB: 15/03/2006 All this code cut out of templates\textareacontrolbar.cfm
 Modified code for right clicking on relay tags to try and make it easier (so you don't have to highlight the whole relay tag to start with
 WAB 03/05/2006  continued working on the above
WAB Sept 2006 more work associated with doing KTML  - use of a returnFunction passed to relaytageditors and putting tag info into a structure
WAB 2008/03/05 bug fix, see body
 
*/

var showRightClickNav = true;
var savedCursorPosition = 0

				function popupCloses(txtName) {
					if (editWindow != undefined && editWindow.closed) {
						taForm[txtName].disabled = false;
						taForm[txtName].focus();
					} else {
						setTimeout("popupCloses('"+txtName+"')", 1000);
					}
				}
				
/*				// wab I don't thinkthis function is being used, so I haven't taken out the #attributes.formname#
				function updateText ( txtName, textString) {
					alert ('update text function')
					if (keptSelection != "") {
						var txtObj = eval ( "document.#attributes.formname#." + txtName );
						if (txtObj.createTextRange && txtObj.caretPos) {
							var caretPos = txtObj.caretPos;
							caretPos.text = ( textString+" "+caretPos.text );
							markSelection ( txtObj );
							if ( txtObj.caretPos.text=='' ) {
								isSelected=false;
								txtObj.focus();
							}
						}
					} else {
					alert("You must place your cursor in the text area first");
					// placeholder for loss of focus handler
					}
				}
*/				
			
				function manageRelayTag(txtName, tagName, obj, parameters,isEdit) {
					// alert ('manageRelayTag textstring ' + textString )
					// WAB 2008/05/03 since the demise of the select box for seleting tags, obj no longer defined
					// so replaced references to it (was being used to get the name of the form that the text area was on) with references to textAreaObj which I have added below

					var tagDetails = tags[tagName.toLowerCase()]
					var	textAreaObj = document.getElementById (txtName)


					if (isEdit == undefined) {
						var isEdit = false;
					}

					if (tagDetails.editorExists !=1) {

						insertText(textAreaObj,tagDetails.insertCode);
					} else {



						var winToOpen = iDomainAndRoot + tagDetails.editorPath;
						conjunction = (winToOpen.indexOf('?') == -1)? '?':'&' ;
						queryString = 'relayTag='+tagDetails.insertCode+'&frmParameterList=' + parameters+ '&formname=' + textAreaObj.form.name + ' &textAreaName=' + txtName + '&isEdit='+isEdit;
						if (!isEdit) {
							returnFunction = 'opener.insertRelayTag(opener.document.' + textAreaObj.form.name + '.' + txtName + ',\'*TAGTEXT*\')'
						} else {
							returnFunction = 'opener.replaceRelayTag(opener.document.' + textAreaObj.form.name + '.' + txtName + ',opener.keptSelection,\'*TAGTEXT*\')'
						}

						winToOpen += conjunction + queryString + '&returnFunction='+returnFunction
						editWindow = window.open(winToOpen,'Edit','scrollbars=yes,menubar=no,toolbar=no,resizable=yes,status=yes,width=600,height=300');
						editWindow.focus();
						popupCloses(txtName);

					}		


					// no longer exists obj.options.selectedIndex = 0;
				}
				

				
			function replaceRelayTag(txtObj,tText,newTText) {
				
				return replaceTag(txtObj,tText,newTText) 
				
			 }
				
				
				
				function replaceTag(txtObj,tText,newTText) {
					var beginText = "";
					var endText = "";
					var newTagText = "";
//					newTText = replaceCurlyBracketsWithChevrons(newTText) ; 
					newTText = removeCurlyBrackets(newTText) ; 
					if (tText.length > 2) {
						var startPos = tText.indexOf('<',0);
						if (startPos != -1) {
							beginText = tText.slice(0,startPos+1);
							var endPos = tText.indexOf('>',startPos);
							if (endPos != -1) {
								endText = tText.slice(endPos,tText.length);
							}
						}
					}
					txtObj.caretPos.text = beginText+newTText+endText;
					//taForm.#attributes.textAreaName#.caretPos.update;
					return true
				}
				
				function getTagText(tText) {
					var tagText = "";
			//		alert ('tText = ' +tText)
					if (tText.length > 2) {
						var startPos = tText.indexOf('<',0);
						if (startPos != -1) {
							var endPos = tText.indexOf('>',startPos);
							if (endPos != -1) {
								tagText = tText.slice(startPos+1,endPos);
							}
						}
					}
					return tagText;
				}


				// these two functions for dealing with selecting the textarea came from
				// http://www.codecomments.com/JScript/message820220.html
				// and probably only work on IE
				function getCaretPosition(elm) {

					if (typeof elm.selectionStart != "undefined")	{
						return elm.selectionStart;
					} else if (document.selection) {
						// IE doesn't have a way of getting the cursor position - this came from the internet and seems to work						var sel = document.selection.createRange(); 
						// Get current cursor point 
						var sel = document.selection.createRange(); 
						var storedRange = sel.duplicate(); 
						storedRange.moveToElementText(elm); 
						storedRange.setEndPoint('EndToEnd', sel); 
						text1 = storedRange.text.replace(/\r/g,"") // IE gets confused by carriagereturn/linefeed and counts them as two characters when really they are only 1, so remove one of them
						text2 = sel.text.replace(/\r/g,"")
						var cursorPoint = text1.length - text2.length ; 
						// alert ('cursorPoint = ' + cursorPoint)
						return cursorPoint
						
					}
				}
				

				function setSelectedTextRange(elm, selectionStart, selectionEnd) {
					if (elm.setSelectionRange) {
						elm.focus();
						elm.setSelectionRange(selectionStart, selectionEnd);
					}	else if (elm.createTextRange) {
						var range = elm.createTextRange();
						range.collapse(true);
						range.moveEnd('character', selectionEnd);
						range.moveStart('character', selectionStart);
						range.select();
					}
				}
				
				function lookForRelayTagAtPositionInString(string,currentPosition) {
					// finds whether currenPosition is in the middle of a relay tag
					// returns array with keys tagText, startPosition, endPosition
					result = new Array()
					result['relayTag'] = ''
					result['tagStart'] = 0
					result['tagEnd'] = 0
					string = string.toLowerCase().replace(/\r/g,"")  // get /n/r which counts as two characters but is really only one
					positionOfStartOfRelayTag = string.lastIndexOf('<relay_',cursorPosition)
					positionOfEndOfAnyPreviousTag = string.lastIndexOf('>',cursorPosition)

					if (positionOfStartOfRelayTag != -1 && positionOfStartOfRelayTag > positionOfEndOfAnyPreviousTag) {
						positionOfEndOfRelayTag = string.indexOf('>',positionOfStartOfRelayTag)
						result['relayTag']  = string.substring (positionOfStartOfRelayTag+1,positionOfEndOfRelayTag)
						result['tagStart'] = positionOfStartOfRelayTag
						result['tagEnd'] = positionOfEndOfRelayTag
					}			
				
					return result
				}


				function editClick(txtObj) {

						if (document.all) {

							// look for tag in current selection
							if (txtObj.caretPos) {
								var rng = txtObj.caretPos.text;
								var tagText = getTagText(rng);
								var tagObj = getTagObject(tagText);
							}						

							// wab trying to look around the current cursor position for a relay tag
							if (!tagObj) {
								tagResult = lookForRelayTagAtPositionInString(txtObj.value,savedCursorPosition)
								tagText = tagResult.relayTag

								if (tagText != '') {
									var tagObj = getTagObject(tagText);
									setSelectedTextRange(txtObj,tagResult.tagStart,tagResult.tagEnd)
								}
							}
							
							
							if (tagObj) {
								/*
								WAB 2008/03/05 javascript falling over later becauase selectObj undefined.  
								This happened because the tag select box had been removed and replaced with a button
								Removed need for this obj
								*/
								var selectObj = taForm['RelayTagSelect_'+txtObj.name];


//								var optionData = selectObj.options[tagIndex].value;
	//							var valueSplitArr = optionData.split("~");
								
								nameValuePairs =   '&frmParameterList=' + escape(tagText)
								// alert (nameValuePairs)								
								keptSelection = txtObj.caretPos.text;
								txtObj.disabled = true;
								manageRelayTag(txtObj.name,tagObj.name,selectObj,escape(tagText),true);
							}
						}
					
				}
				
				function showHideMenu(event,doWhat,txtName) {

					// menuObj = document.all['contextMenu_'+txtName]
					menuObj = document.getElementById('contextMenu_'+txtName)
					if (doWhat.toLowerCase() == "show") {
						//alert("document.all.contextMenu.style.left:"+document.all.contextMenu.style.left+"\ndocument.all.contextMenu.style.top:"+document.all.contextMenu.style.top+"\nevent.clientX:"+event.clientX+"\nevent.clientY:"+event.clientY);
						menuObj.style.left = event.clientX + document.body.scrollLeft;
						menuObj.style.top = event.clientY + document.body.scrollTop;
						menuObj.style.visibility = 'visible';
					} else {
						menuObj.style.visibility = 'hidden';
					}
				}
				
				function showMenu(event, textareaObject) {
						if (window.event) {
						  event = window.event
						} 

					if (textareaObject.testVar == undefined) {
						textareaObject.testVar = true;
						killAllowSelection(true);
						
						textareaObject.onfocus = function() {
							killAllowSelection(true);
						}
						
						textareaObject.onblur = function() {
							killAllowSelection(false);
						}
					}



					// note not implemented for FF yet
						if (document.all) {
							// look for tag in current selection
							if (textareaObject.caretPos) {
								var rng = textareaObject.caretPos.text;
								var tagObj = getTagObject(getTagText(rng));
							}
					
							// wab trying to look around the current cursor position for a relay tag
							if (!tagObj ) {

								cursorPosition  = getCaretPosition(textareaObject)
								tagResult = lookForRelayTagAtPositionInString(textareaObject.value,cursorPosition)
								tagText = tagResult.relayTag
								if (tagText != '') {
									var tagObj = getTagObject(tagText );
									savedCursorPosition = cursorPosition
								}
							}
							if ((tagObj!=undefined ) && showRightClickNav ) {

								 
								 if (tagObj.editorExists) {
									 // alert ('about to show menu')
									//WE HAVE A VALID TAG. SHOW MENU!
									showHideMenu(event,'show',textareaObject.name);
									if (window.event) {
										event.returnValue=false;   //IE
								     } else {
										event.preventDefault(true);  //FF
								     }
				
									return false;
								} else {
									alert ('Sorry no editor for this tag')
								}
				
							} 
						
					}
					showHideMenu(event,'hide',textareaObject.name);
					return true;
				}
				
				
				function getTagObject(tText) { 
					tagName = '<' + tText.split(" ")[0].toLowerCase() + '>'
					return tags[tagName.toLowerCase()]
				
				}
								
				function isValidTag(tText,txtName) {
//					if ()
					selectObj = taForm['RelayTagSelect_'+txtName];
					for (var i=0;i<selectObj.options.length;i++) {
						var optionData = selectObj.options[i].value;
						var valueSplitArr = optionData.split("~");
	
						if (valueSplitArr.length > 1 && valueSplitArr[0].slice(1,valueSplitArr[0].length-1).toLowerCase() == tText.split(" ")[0].toLowerCase()) {
							return i;
						}
					}
					return -1;
				}
				
				function manipulateItem(doWhat) {
					if (isSelected) {
						var mainRng = document.selection.createRange();
					} else {
						// if there isn't anything selected in the textbox then document.selection.createRange() creates a range at the top of the document

					}	
					mainRng.execCommand(doWhat);
				}
				

			function killAllowSelection(beWhat) {
//				alert ('killAllowSelection showRightClickNav = ' + beWhat)
				showRightClickNav = beWhat;
	   		}				

		function replaceCurlyBracketsWithChevrons (text) {
			regexp = /(\{\{)(.*)(\}\})/ ;
			replaceexp = "\<$2\>" ; 

			return text.replace(regexp,replaceexp) ; 
		}	

		function removeCurlyBrackets (text) {
			regexp = /(\{\{)(.*)(\}\})/ ;
			replaceexp = "$2" ; 
			return text.replace(regexp,replaceexp) ; 
		}			
