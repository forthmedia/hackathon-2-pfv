/* �Relayware. All Rights Reserved 2014 */
/*--------------------------------------------------|

| dTree 2.05 | www.destroydrop.com/javascript/tree/ |

|---------------------------------------------------|

| Copyright (c) 2002-2003 Geir Landr�               |

|                                                   |

| This script can be used freely as long as all     |

| copyright messages are intact.                    |

|                                                   |

| Updated: 17.04.2003  								|

| Modified:	08-Apr-2005 by RND - Added code to 		|
			integrate right-click functionality.
			23/09/05   WAB the .s function did not seem not to work - had alter the references to the objects 
			03/10/05	WAB problem with openTo function trying to select items which were not visible  corrected
			28/11/05	WAB added parentid to the ID within the anchor, so that it could be referenced by the context menu
			25/04/06	WAB removed some of the hard coding for the elementediting tree (eg elementTypeID;rootNew;rootEdit;)
							replaced with an additionalID parameter which can be used to store a textid and an additionalHTML parameter
			07/07/07	WAB sorted out some bugs

|--------------------------------------------------*/

// Node object
function Node(id, pid, name, url, title, target, icon, iconOpen, open, additionalID,additionalHTML,hasAdditionalChildren) {
	this.id = id;       // id (from users data structure)
	this.pid = pid;      // parentid
	this.name = name;
	this.url = url;
	this.title = title;
	this.target = target;
	this.icon = icon;
	this.iconOpen = iconOpen;
	this._io = open || false;     // IsOpen
	this._is = false;				// IsSelected
	this._ls = false;				// LastSibling
	this._hc = false;				//HasChildren
	this._ai = 0;					// the internal index of the node (ie the item in the aNodes array)
	this._p;						// pointer to the parent object
	this.additionalID = additionalID ;
	this.additionalHTML = additionalHTML ;
	this.hasAdditionalChildren = (hasAdditionalChildren) ? true : false ;   // added by WAB - used to show that additional children are available by webservice
};

// Tree object
function dTree(objName) {
	this.config = {
		target					: null,
		folderLinks			: true,
		useSelection		: true,
		useCookies			: true,
		useLines				: true,
		useIcons				: true,
		useStatusText		: false,
		closeSameLevel	: false,
		inOrder					: false,
		getChildNodesPage					: '',
		debugAjax : false,
		urlroot: '..'
	}
	
	/*this.icon = {
		root				: this.config.urlroot+'/images/dtree/dtree_base.gif',
		folder			: this.config.urlroot+'/images/dtree/dtree_folder.gif',
		folderOpen	: this.config.urlroot+'/images/dtree/dtree_folderopen.gif',
		node				: this.config.urlroot+'/images/dtree/dtree_page.gif',
		empty				: this.config.urlroot+'/images/dtree/dtree_empty.gif',
		line				: this.config.urlroot+'/images/dtree/dtree_line.gif',
		join				: this.config.urlroot+'/images/dtree/dtree_join.gif',
		joinBottom	: this.config.urlroot+'/images/dtree/dtree_joinbottom.gif',
		plus				: this.config.urlroot+'/images/dtree/dtree_plus.gif',
		plusBottom	: this.config.urlroot+'/images/dtree/dtree_plusbottom.gif',
		minus				: this.config.urlroot+'/images/dtree/dtree_minus.gif',
		minusBottom	: this.config.urlroot+'/images/dtree/dtree_minusbottom.gif',
		nlPlus			: this.config.urlroot+'/images/dtree/dtree_nolines_plus.gif',
		nlMinus			: this.config.urlroot+'/images/dtree/dtree_nolines_minus.gif'
	};*/
	this.setIcons();
	this.obj = objName;
	this.aNodes = [];
	this.aIndent = [];
	this.root = new Node(-1);
	this.selectedNode = null;
	this.selectedFound = false;
	this.completed = false;
};

// Adds a new node to the node array
dTree.prototype.add = function(id, pid, name, url, title, target, icon, iconOpen, open, additionalID,additionalHTML,hasAdditionalChildren) {
	this.aNodes[this.aNodes.length] = new Node(id, pid, name, url, title, target, icon, iconOpen, open, additionalID,additionalHTML,hasAdditionalChildren);
};


dTree.prototype.setIcons = function() {
	this.icon = {
		root				: this.config.urlroot+'/images/dtree/dtree_base.png',
		folder			: this.config.urlroot+'/images/dtree/dtree_folder.png',
		folderOpen	: this.config.urlroot+'/images/dtree/dtree_folderopen.png',
		node				: this.config.urlroot+'/images/dtree/dtree_page.png',
		empty				: this.config.urlroot+'/images/dtree/dtree_empty.png',
		line				: this.config.urlroot+'/images/dtree/dtree_line.png',
		join				: this.config.urlroot+'/images/dtree/dtree_join.png',
		joinBottom	: this.config.urlroot+'/images/dtree/dtree_joinbottom.png',
		plus				: this.config.urlroot+'/images/dtree/dtree_plus.png',
		plusBottom	: this.config.urlroot+'/images/dtree/dtree_plusbottom.png',
		minus				: this.config.urlroot+'/images/dtree/dtree_minus.png',
		minusBottom	: this.config.urlroot+'/images/dtree/dtree_minusbottom.png',
		nlPlus			: this.config.urlroot+'/images/dtree/dtree_nolines_plus.png',
		nlMinus			: this.config.urlroot+'/images/dtree/dtree_nolines_minus.png',
		none			: this.config.urlroot+'/images/dtree/dtree_nolines_minus.png'
	};
}

// Open/close all nodes
dTree.prototype.openAll = function() {
	this.oAll(true);
};

dTree.prototype.closeAll = function() {
	this.oAll(false);
};

// Outputs the tree to the page
dTree.prototype.toString = function() {
	var str = '<div class="dtree">\n';
	if (document.getElementById) {
		if (this.config.useCookies) this.selectedNode = this.getSelected();
		str += this.addNode(this.root);
	} else str += 'Browser not supported.';
	str += '</div>';
	str += '<div id="dtreeDummy" style="display:none"></div>' ; // this is a dummy div that is used with ajax.updater added when upgrade to prototype v1.6 which wouldn't allow a non-existent element to be 'updated'
	if (!this.selectedFound) this.selectedNode = null;
	this.completed = true;
	return str;
};

// Creates the tree structure
dTree.prototype.addNode = function(pNode) {
	var str = '';
	var n=0;
	if (this.config.inOrder) n = pNode._ai;
	for (n; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == pNode.id) {
			var cn = this.aNodes[n];
			cn._p = pNode;
			cn._ai = n;
			this.setCS(cn);
			if (!cn.target && this.config.target) cn.target = this.config.target;
			if (cn._hc && !cn._io && this.config.useCookies) cn._io = this.isOpen(cn.id);
			if (!this.config.folderLinks && cn._hc) cn.url = null;
			if (this.config.useSelection && cn.id == this.selectedNode && !this.selectedFound) {
					cn._is = true;
					this.selectedNode = n;
					this.selectedFound = true;
			}
			str += this.node(cn, n);
			if (cn._ls) break;
		}
	}

	return str;
};

// Creates the node icon, url and text
dTree.prototype.node = function(node, nodeId) {
	var str = '<div id="dx' + this.obj + nodeId + '" class="dTreeNode" >' 
		str += this.nodeDivContent(node, nodeId);
		str += '</div>';

	if (node._hc || node.hasAdditionalChildren) {
		str += '<div id="d' + this.obj + nodeId + '" class="clip" style="display:' + ((this.root.id == node.pid || node._io) ? 'block' : 'none') + ';">';
		// if the node doesn't have children loaded yet then this next function brings back '', so we end up with just a container which can be used when we need to populate this bit of the tree
		str += this.addNode(node);
		str += '</div>';
	} 

	this.aIndent.pop();

	return str;

};


// Creates the node icon, url and text
dTree.prototype.nodeDivContent = function(node, nodeId) {
	var str = this.indent(node, nodeId);
	if (this.config.useIcons) {
		if (!node.icon) node.icon = (this.root.id == node.pid) ? this.icon.root : ((node._hc || node.hasAdditionalChildren) ? this.icon.folder : this.icon.node);
		if (!node.iconOpen) node.iconOpen = (node._hc || node.hasAdditionalChildren) ? this.icon.folderOpen : this.icon.node;
		if (!node.icon && this.root.id == node.pid) {
			node.icon = this.icon.root;
			node.iconOpen = this.icon.root;
		}
		/*if (node.icon && this.root.id == node.pid) {
			node.icon = this.icon[this.icon];
			node.iconOpen = this.icon[this.iconOpen];
		}*/
		//alert(this.icon.folder);
		
		str += '<img id="i' + this.obj + nodeId + '" src="' + ((node._io) ? node.iconOpen : node.icon) + '" contextmenu="page" alt="" border="0" />';
	}
	if (node.url) {
		//' + ((this.config.useSelection) ? ((node._is ? 'nodeSel' : 'node')) : 'node') + '
		//node
		str += '<a id="s' + this.obj + nodeId + '-' + node.id + '-' + node.pid + '" contextmenu="page"  class="smallLink" href="' + node.url + '"';  // onClick = "javascript:fnLeftClickContextMenu()"
		if (node.title) str += ' title="' + node.title + '"';
		if (node.target) str += ' target="' + node.target + '"';
		if (this.config.useStatusText) str += ' onmouseover="window.status=\'' + node.name + '\';return true;" onmouseout="window.status=\'\';return true;" ';
		if (this.config.useSelection && ((node._hc && this.config.folderLinks) || !node._hc))
			str += ' onclick="javascript: ' + this.obj + '.s(' + nodeId + ');"';
		str += '>';
	}
	else if ((!this.config.folderLinks || !node.url) && node._hc && node.pid != this.root.id)
		str += '<a id="s' + this.obj + nodeId + '-' + node.id + '-' + node.pid + '" href="javascript: ' + this.obj + '.o(' + nodeId + ');" contextmenu="page" class="smallLink">';

	str += node.name;
//		if(node.id == 0){
//			str	+= ' ' + node.rootNew + ' ' + node.rootEdit;
//		}
	if (node.url || ((!this.config.folderLinks || !node.url) && node._hc)){
		str += '</a>' 
		if(node.id != 0){
			//str	+= '  <a href="' + node.url + '&frmWizardPage=WYSIWYGEditor" target="editFrame" title="Edit HTML" class="smallLink">H</a> <a href="' + node.url + '&frmWizardPage=attributes" target="editFrame" title="Edit content attributes" class="smallLink">A</a> <a href="' + node.url + '&frmWizardPage=visibility" target="editFrame" title="Edit visibility and rights" class="smallLink">V</a> <a href="' + node.addElementLink + '&typeID=' + node.elementTypeID+'" target="editFrame" title="Add child page" class="smallLink">+</a>';
// 			str	+= '  <a id="s' + this.obj + nodeId + '-' + node.id + '-' + node.pid + '" href="' + node.url + '&frmWizardPage=WYSIWYGEditor" contextmenu="page" target="editFrame" title="Edit HTML" class="smallLink">H</a> <a id="s' + this.obj + nodeId + '-' + node.id + '-' + node.pid + '" href="' + node.url + '&frmWizardPage=attributes" contextmenu="page" target="editFrame" title="Edit content attributes" class="smallLink">A</a> <a id="s' + this.obj + nodeId + '-' + node.id + '-' + node.pid + '" href="' + node.url + '&frmWizardPage=visibility" contextmenu="page" target="editFrame" title="Edit visibility and rights" class="smallLink">V</a>  <a id="s' + this.obj + nodeId + '-' + node.id + '-' + node.pid + '" href="' + node.addElementLink + '&typeID=' + node.elementTypeID+'" contextmenu="page" target="editFrame" title="Add child page" class="smallLink">+</a>';
		}
	}

	if (node.additionalHTML != '' )  {
		str += node.additionalHTML ;	
	}

	return str;

};



// Adds the empty and line icons
dTree.prototype.indent = function(node, nodeId) {
	
	var str = '';
	if (this.root.id != node.pid) {

		for (var n=0; n<this.aIndent.length; n++)
			str += '<img src="' + ( (this.aIndent[n] == 1 && this.config.useLines) ? this.icon.line : this.icon.empty ) + '" alt="" border="0" />';
							
		(node._ls) ? this.aIndent.push(0) : this.aIndent.push(1);
		if (node._hc || node.hasAdditionalChildren) {
			(openFunction =  (node._hc) ? 'o' : 'o')  // was going to be special function for loading items via web service
			str += '<a href="javascript: ' + this.obj + '.'+ openFunction +' (' + nodeId + ');"><img border="0" id="j' + this.obj + nodeId + '" src="';
			if (!this.config.useLines) str += (node._io) ? this.icon.nlMinus : this.icon.nlPlus;
			else str += ( (node._io) ? ((node._ls && this.config.useLines) ? this.icon.minusBottom : this.icon.minus) : ((node._ls && this.config.useLines) ? this.icon.plusBottom : this.icon.plus ) );
			str += '" alt="" /></a>';
		} else str += '<img src="' + ( (this.config.useLines) ? ((node._ls) ? this.icon.joinBottom : this.icon.join ) : this.icon.empty) + '" alt="" />';
	}
	return str;
};

// Checks if a node has any children and if it is the last sibling
dTree.prototype.setCS = function(node) {

	var lastId;
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id) {node._hc = true;}
		if (this.aNodes[n].pid == node.pid) {lastId = this.aNodes[n].id;  }
	}
	if (lastId==node.id) {node._ls = true} else {node._ls = false};  // wab had to add the else to deal with tree being changed on the fly and nodes no longer being the last 
};

// Returns the selected node
dTree.prototype.getSelected = function() {
	var sn = this.getCookie('cs' + this.obj);
	return (sn) ? sn : null;
};

// Highlights the selected node
dTree.prototype.s = function(id) {
	if (!this.config.useSelection) return;
	var cn = this.aNodes[id];
	if (cn._hc && !this.config.folderLinks) return;
	if (this.selectedNode != id) {
		if (this.selectedNode || this.selectedNode==0) {
			eOld = document.getElementById("s" + this.obj + this.selectedNode + '-' + this.aNodes[this.selectedNode].id + '-' + this.aNodes[this.selectedNode].pid);  // bit at end added by WAB  and added pid 28/11/05
			eOld.className = "smallLink";
		}
		eNew = document.getElementById("s" + this.obj + id + '-' + cn.id + '-' + cn.pid);  // + '-' + cn.id WAB 22/09/05 added '-' and bit after to get it to work and added pid 28/11/05
		eNew.className = "smallLinkHighlight";
		this.selectedNode = id;
		eNew.focus() ; 
		if (this.config.useCookies) this.setCookie('cs' + this.obj, cn.id);
	}
};

// Toggle Open or close
dTree.prototype.o = function(id) {
	var cn = this.aNodes[id];
	// if this item has no children then it must be an item which we need to go and get the children for
	if (!this.aNodes[id]._hc) {
		this.setIndentArray  (this.aNodes[id]);
		str = this.indent(this.aNodes[id])
		this.resetIndentArray()
		divId = 'd' + this.obj + id
		theDiv = document.getElementById(divId)
		this.icon.empty
		theDiv.innerHTML = str + ' loading ... '
		this.getChildNodes (this.aNodes[id].id)
	}
	this.nodeStatus(!cn._io, id, cn._ls);
	cn._io = !cn._io;
	if (this.config.closeSameLevel) this.closeLevel(cn);
	if (this.config.useCookies) this.updateCookie();
};

// Open or close all nodes
dTree.prototype.oAll = function(status) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._hc && this.aNodes[n].pid != this.root.id) {
			this.nodeStatus(status, n, this.aNodes[n]._ls)
			this.aNodes[n]._io = status;
		}
	}
	if (this.config.useCookies) this.updateCookie();
};

// Opens the tree to a specific node
dTree.prototype.openTo = function(nId, bSelect, bFirst) {

	if (!bFirst) {
		for (var n=0; n<this.aNodes.length; n++) {
			if (this.aNodes[n].id == nId) {
				nId=n;
				break;
			}		}
	}
	var cn=this.aNodes[nId];

	if (cn.pid==this.root.id || !cn._p) {
		return;
	}
	
	cn._io = true;
	cn._is = bSelect;
	if (this.completed && cn._hc) {
		this.nodeStatus(true, cn._ai, cn._ls);
	}	

	this.openTo(cn._p._ai, false, true);   //  WAB: 03/10/05 this line was below the next if statement. but this meant that the code tried to select it before it was visible, so I have moved it

	if (this.completed && bSelect) {
		this.s(cn._ai);
	} else if (bSelect) {
		 this._sn=cn._ai;
	}

	// WAB : this is where 	this.openTo(cn._p._ai, false, true); used to be
	
};

	
// converts your id of the node to the internal id
// returns -1 if doesn't exist
dTree.prototype.getId = function(nId) {
		for (var n=0; n<this.aNodes.length; n++) {
			if (this.aNodes[n].id == nId || this.aNodes[n].additionalID.toLowerCase() == nId.toLowerCase()) {    // WAB 2008/10/28 added the toLower to additionalID
				return (n);
			}
		}
		return (-1) ;
		
}
// Closes all nodes on the same level as certain node
dTree.prototype.closeLevel = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.pid && this.aNodes[n].id != node.id && this.aNodes[n]._hc) {
			this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);
		}
	}
}

// Closes all children of a node
dTree.prototype.closeAllChildren = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id && this.aNodes[n]._hc) {
			if (this.aNodes[n]._io) this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);		
		}
	}
}

// Change the status of a node(open or closed)
dTree.prototype.nodeStatus = function(status, id, bottom) {
	eDiv	= document.getElementById('d' + this.obj + id);
	if (!eDiv) {alert (' no div:  d' + this.obj + id)}
	eJoin	= document.getElementById('j' + this.obj + id);
	if (this.config.useIcons) {
		eIcon	= document.getElementById('i' + this.obj + id);
		eIcon.src = (status) ? this.aNodes[id].iconOpen : this.aNodes[id].icon;
	}
	eJoin.src = (this.config.useLines)?
	((status)?((bottom)?this.icon.minusBottom:this.icon.minus):((bottom)?this.icon.plusBottom:this.icon.plus)):
	((status)?this.icon.nlMinus:this.icon.nlPlus);
	eDiv.style.display = (status) ? 'block': 'none';
};

// [Cookie] Clears a cookie
dTree.prototype.clearCookie = function() {
	var now = new Date();
	var yesterday = new Date(now.getTime() - 1000 * 60 * 60 * 24);
	this.setCookie('co'+this.obj, 'cookieValue', yesterday);
	this.setCookie('cs'+this.obj, 'cookieValue', yesterday);
};

// [Cookie] Sets value in a cookie
dTree.prototype.setCookie = function(cookieName, cookieValue, expires, path, domain, secure) {
	document.cookie =
		escape(cookieName) + '=' + escape(cookieValue)
		+ (expires ? '; expires=' + expires.toGMTString() : '')
		+ (path ? '; path=' + path : '')
		+ (domain ? '; domain=' + domain : '')
		+ (secure ? '; secure' : '');
};

// [Cookie] Gets a value from a cookie
dTree.prototype.getCookie = function(cookieName) {
	var cookieValue = '';
	var posName = document.cookie.indexOf(escape(cookieName) + '=');
	if (posName != -1) {
		var posValue = posName + (escape(cookieName) + '=').length;
		var endPos = document.cookie.indexOf(';', posValue);
		if (endPos != -1) cookieValue = unescape(document.cookie.substring(posValue, endPos));
		else cookieValue = unescape(document.cookie.substring(posValue));
	}
	return (cookieValue);
};

// [Cookie] Returns ids of open nodes as a string
dTree.prototype.updateCookie = function() {
	var str = '';
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._io && this.aNodes[n].pid != this.root.id) {
			if (str) str += '.';
			str += this.aNodes[n].id;
		}
	}
	this.setCookie('co' + this.obj, str);

};

// [Cookie] Checks if a node id is in a cookie
dTree.prototype.isOpen = function(id) {
	var aOpen = this.getCookie('co' + this.obj).split('.');
	for (var n=0; n<aOpen.length; n++)
		if (aOpen[n] == id) return true;
	return false;
};

// If Push and pop is not implemented by the browser
if (!Array.prototype.push) {
	Array.prototype.push = function array_push() {
		for(var i=0;i<arguments.length;i++)
			this[this.length]=arguments[i];
		return this.length;
	}
};

if (!Array.prototype.pop) {
	Array.prototype.pop = function array_pop() {
		lastElement = this[this.length-1]; 
		this.length = Math.max(this.length-1,0);
		return lastElement;
	}
};

// WAB function
// gets node object
dTree.prototype.getNodeByRef=function (ref) {
	for(var i=0;i<this.aNodes.length;i++){
		if (this.aNodes[i].id == ref){
			return i
		}
	}

	return null

}

dTree.prototype.doesNodeExist=function (ref) {
	if (this.getNodeByRef(ref) != null ) {return true}else{return false}

}


// WAB function
// checks existence of node before opening
dTree.prototype.openToIfExists  = function(nId, bSelect, bFirst) { 
		thisNode = this.getNodeByRef (nId) 
		if (thisNode != null) {this.openTo(nId, bSelect, bFirst)}
}


// WAB function
// 
dTree.prototype.addWithCheck = function(id, pid, name, url, title, target, icon, iconOpen, open, additionalID,additionalHTML,hasAdditionalChildren) {
		if (!this.doesNodeExist (pid)) {
			// parent node not in the tree so can't add
		} else if (!this.doesNodeExist (id)) {
			this.add (id, pid, name, url, title, target, icon, iconOpen, open, additionalID,additionalHTML,hasAdditionalChildren);
		} else {
			// if the node is already in existence then we do some updating (of name etc.)  - slightly unfinished
			node = this.aNodes[this.getNodeByRef(id)]
			node.name = name
			node.title = title
			if (node.hasAdditionalChildren != hasAdditionalChildren) {
				// this item has changed from not having children to having them (or otherway round)
				// null out the icons so that they get recreated
				node.icon = null
				node.iconOpen = null
			}
			node.hasAdditionalChildren = hasAdditionalChildren
			if (icon) node.icon = icon
			if (iconOpen) node.iconOpen = iconOpen
		}
};


/* WAB 2013-05-13 CASE 434366
Added this function to deal with updating the tree if items are removed
Function takes the parent node id (pid) and a list of children nodes
*/
dTree.prototype.removeChildrenNotInList = function (pid,childrenIdList) {

	childrenArray = childrenIdList.split(',')

	// loop through all nodes looking for those which are children of pid
	for (var n=this.aNodes.length - 1; n>=0; n--) {
		if (this.aNodes[n].pid == pid) {
			nodefound = false
			// now check whether this child node is in the list of children nodes
			// as soon as found then set nodefound=true and break
			for (var i=0; i<childrenArray.length;i++) {
				if (childrenArray[i] == this.aNodes[n].id) {
					nodefound = true;
					break;
				}			
			}

			// if this node is not found in list of children then delete from array of nodes
			if (!nodefound) {
				this.aNodes.splice(n, 1)
			}
			
		}
	}
	
}


dTree.prototype.getGeneration  = function(nodeObj) { 
		generation = 0
	
		while (nodeObj.pid != -1) {
			nodeObj = nodeObj._p
			generation++
		} 
	return generation
}

// to create all the lines when adding items to the tree, we need to update this array so that it reflects where the node is in the tree
dTree.prototype.setIndentArray  = function(nodeObj) { 
		while (nodeObj.pid != -1) {
//			(nodeObj._ls) ? this.aIndent.push(0) : this.aIndent.push(1);   this was building the array back to front, needed to add items to the beginning
			(nodeObj._ls) ? this.aIndent.splice(0,0,0) : this.aIndent.splice(0,0,1);
			nodeObj = nodeObj._p
		} 
	return 
}

dTree.prototype.resetIndentArray  = function() { 
	this.aIndent = [];   // hack way of resetting it
}

// WAB Function
dTree.prototype.getListOfNodeIds = function() {
	var tempArray = [];
	for (var n=0; n<this.aNodes.length; n++) {
		tempArray[n] = this.aNodes[n].id
	}
	return tempArray.join()

};



// WAB messing with redrawing part of tree
// the idea being that you could do an asynchronous request to get xtra bits of the tree
// first need to add the new nodes to the structure, but this would redraw and open the new bit 
dTree.prototype.redraw = function (node) {

	nodeId = this.getNodeByRef(node)
	nodeObj = this.aNodes[nodeId]

	this.setCS(nodeObj)  // make sure that is knows if it now has children 

	// I think that we are best redrawing from the parent of this node.  This is because this code only updates the children of this node (so won't spot a change of name of the current norde
	// will mean more stuff to process but think OK
	// can't redraw an item without children because there isn't a named div (I think) , so need to redraw from parent anyway
//	if (!nodeObj._hc) {
//		nodeObj = nodeObj._p	
//	}else if (nodeObj._ai != 0) {
//		nodeObj = nodeObj._p	
//	}

	// need to work out the indent (generation) of the current item so that we get the right indenting
	this.setIndentArray  (nodeObj);

	if (nodeObj._hc) {
		childrenContent = this.addNode(nodeObj);
		divId = 'd' + this.obj + nodeObj._ai 
		theDiv = document.getElementById(divId)  
		theDiv.innerHTML = childrenContent
	}
	
	this.aIndent.pop();
	
	if (nodeObj._ai != 0) {
		selfContent = this.nodeDivContent(nodeObj,nodeObj._ai);
		divId = 'dx' + this.obj + nodeObj._ai 
		theDiv = document.getElementById(divId)  
		theDiv.innerHTML = selfContent
	}
		
	this.resetIndentArray()
	
	this.openTo(nodeId)
}

// function to redraw the tree from the parent of the current node
dTree.prototype.redrawParent = function (node) {
	nodeId = this.getNodeByRef(node)
	nodeObj = this.aNodes[nodeId]
        // test for special case of being the top of the tree
	if (nodeObj.pid != -1) {this.redraw(nodeObj.pid)} else {this.redraw(nodeObj.id)}
}


// function to write 'no items' if nothing returned
dTree.prototype.redrawNoItems = function (node) {
	nodeId = this.getNodeByRef(node)
	nodeObj = this.aNodes[nodeId]	
	if (nodeObj._hc || nodeObj.hasAdditionalChildren) {  // added WAB 7/2/07 - can only do if the tree thinks that there should be children - otherwise the divs won't exist
		generation = this.getGeneration (nodeObj);
		this.setIndentArray  (nodeObj);
		str = this.indent(nodeObj)
		this.resetIndentArray()     // added WAB 7/2/07 to fix bug - things would suddenly jump to the right
		divId = 'd' + this.obj + nodeObj._ai
		theDiv = document.getElementById(divId)
		theDiv.innerHTML = str + 'No Items'
	}
		
}


dTree.prototype.getChildNodes = function (nodeId) {
		intId= this.getNodeByRef(nodeId)
		theForm = $(this.obj + '_Ajax')
		page = theForm.action 
		parameters =  Form.serialize(theForm)
		parameters += '&id=' + nodeId

		var myAjax = new Ajax.Updater(
			'dtreeDummy',
			page, 
			{
				method: 'get', 
				parameters: parameters, 
				evalScripts: true,
				debug: this.config.debugAjax

			});
		
}

// searches for elements based on criteria passed and then either 
dTree.prototype.searchAndAddNodes = function (goTo,parameters,container) {
		page = '/webservices/ElementTreeNav.cfm?method=searchAndAdd'
		theForm = $(this.obj + '_Ajax')   // tree filter form
		parameters +=  '&' + Form.serialize(theForm)  // add tree filter parameters
		parameters += '&goto=' + goTo + '&currentDisplayedNodes=' + this.getListOfNodeIds() 
		var myAjax = new Ajax.Updater(
			container,
			page, 
			{
				method: 'get', 
				parameters: parameters, 
				evalScripts: true,
				debug: this.config.debugAjax
			});

}



