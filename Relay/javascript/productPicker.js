/* �Relayware. All Rights Reserved 2014 */
// YMA 2013-01-07 2013 Roadmap Product Picker JS Functions
function IsNumeric(input) {
	return ((typeof input === 'number' && isFinite(input)) || (!isNaN(input))); // NJH 2014/03/20 - added the !isNan check as well as '2' was not being returned as numberic
}

function openCategory(productCatalogueCampaignID,categoryId,contentID,imageWidth,noImageFile,catalogueShowCols,specifyGroups,productGroupID){
		if(jQuery("#groupHeading" + categoryId).parent().find("div:first").attr('class')=="selected") {
			
			jQuery("#groupHeading" + categoryId).parent().find("div:first").removeClass("selected");
			jQuery(".groupHeading").parent().find("div").addClass("notSelected");
			jQuery("#groupHeading" + categoryId).css("display","none");
			jQuery("#groupSelector" + categoryId).css("display","none");
			jQuery("#productsViewAll" + categoryId).removeClass("glyphicon glyphicon-chevron-down");
			jQuery("#productsViewAll" + categoryId).addClass("glyphicon glyphicon-chevron-right");
		
			
			
		} else {
			var page = '/webservices/relayProductWS.cfc?method=getProductGroupsHTMLForProductPicker&_cf_nodebug=true';
			var parameters = {productCatalogueCampaignID:productCatalogueCampaignID,contentID:contentID,imageWidth:imageWidth,noImageFile:noImageFile,catalogueShowCols:catalogueShowCols,specifyGroups:specifyGroups,returnFormat:'plain'};
			var updaterID = '';
			
			if(IsNumeric(categoryId)) {
				parameters.productCategoryID=categoryId;
				updaterID = categoryId;
			}
			
			jQuery(".groupSelector").html("");
			jQuery(".groupSelector").css("display","none");
			jQuery(".groupHeading").css("display","none");
			jQuery(".groupHeading").parent().find("div").removeClass("selected");
			jQuery("#groupSelector" + updaterID).css("display","block");
			jQuery("#groupHeading" + updaterID).css("display","block");
			jQuery(".groupHeading").parent().find("div").removeClass("notSelected");
			
			jQuery("#groupHeading" + updaterID).parent().find("div:first").addClass("selected");
			jQuery("#groupSelector" + updaterID).html("<div id='loadingDiv' class='productGroup' width='100%' align='center'><p><img src='/images/icons/loading.gif'></p></div>");
			jQuery("#productsViewAll" + categoryId).removeClass("glyphicon glyphicon-chevron-right");
			jQuery("#productsViewAll" + categoryId).addClass("glyphicon glyphicon-chevron-down");
			
			/** Check this Sandra**/
			jQuery("#categorySelectorClick.notSelected div span #productsViewAll" + categoryId).removeClass("glyphicon glyphicon-chevron-down");
		
			var myAjax = new Ajax.Updater(
		        "groupSelector" + updaterID,
		        page, 
				{
		            method: 'get', 
		        	parameters: parameters,
		        	onComplete: function(transport){
		        		if(IsNumeric(productGroupID)) { 
		        			openGroup(productCatalogueCampaignID,productGroupID,contentID,imageWidth,noImageFile,catalogueShowCols);
		        		}
		        	}
		    	});
			
			
		}
	}
// START 2014-07-30	AXA Added two tier hierarchy functionality.
function openGroup(productCatalogueCampaignID,productGroupID,contentID,imageWidth,noImageFile,catalogueShowCols) {
		if(jQuery("#productGroup"+productGroupID).attr('class')=="productGroup selected") {
			
			jQuery("#productGroup"+productGroupID).removeClass("selected");
			jQuery("#productSelector" + productGroupID).css("display","none");
			jQuery("#productHeading" + productGroupID).css("display","none");
			
			jQuery("#productsViewAllsub" +productGroupID).removeClass("glyphicon glyphicon-chevron-down");
			jQuery("#productsViewAllsub" +productGroupID).addClass("glyphicon glyphicon-chevron-right");
			
		} else {
			var page = '/webservices/relayProductWS.cfc?method=getProductsHTMLForProductPicker&_cf_nodebug=true';    
			var parameters = {productCatalogueCampaignID:productCatalogueCampaignID,productGroupID:productGroupID,contentID:contentID,imageWidth:imageWidth,noImageFile:noImageFile,catalogueShowCols:catalogueShowCols,returnFormat:'plain'};
			
			jQuery(".productSelector").html("");
			jQuery(".productSelector").css("display","none");
			jQuery(".productHeading").css("display","none");
			jQuery("#productGroup"+productGroupID).parent().find("div.selected").removeClass("selected");
			jQuery("#productSelector"+productGroupID).css("display","block");
			jQuery("#productHeading"+productGroupID).css("display","block");
			jQuery("#productGroup"+productGroupID).addClass("selected");
			jQuery("#productSelector"+productGroupID).html("<div id='loadingDiv' class='product' width='100%' align='center'><p><img src='/images/icons/loading.gif'></p></div>");
			
			jQuery("#productsViewAllsub" +productGroupID).removeClass("glyphicon glyphicon-chevron-right");
			jQuery("#productsViewAllsub" +productGroupID).addClass("glyphicon glyphicon-chevron-down");
			
			var myAjax = new Ajax.Updater(
				"productSelector"+productGroupID,
				page, 
				{
					method: 'get', 
					parameters: parameters
				});
		}
	}

//END 2014-07-30	AXA Added two tier hierarchy functionality.
function openGroupTier2(productCatalogueCampaignID,productGroupID,contentID,imageWidth,noImageFile,catalogueShowCols) {
		
		var openGroup = jQuery('#openGroupTier2'+productGroupID);
		var page = '/webservices/relayProductWS.cfc?method=getProductsHTMLForProductPicker&_cf_nodebug=true';    
		var parameters = {productCatalogueCampaignID:productCatalogueCampaignID,productGroupID:productGroupID,contentID:contentID,imageWidth:imageWidth,noImageFile:noImageFile,catalogueShowCols:catalogueShowCols,returnFormat:'plain'};

		if (jQuery("#productGroup"+productGroupID).attr('class')=="productGroup selected") {			

			jQuery("#productGroup"+productGroupID).addClass("notselected");
			jQuery("#productGroup"+productGroupID).removeClass("selected");
			
			jQuery("#productGroup"+productGroupID+" button.glyphicon").removeClass("glyphicon-chevron-down");
			jQuery("#productGroup"+productGroupID+" button.glyphicon").addClass("glyphicon-chevron-right");		

			openGroup.html('');

		} else {
			
			jQuery("#productGroup"+productGroupID).removeClass("notselected");
			jQuery("#productGroup"+productGroupID).addClass("selected");			

			jQuery("#productGroup"+productGroupID+" button.glyphicon").removeClass("glyphicon-chevron-right");
			jQuery("#productGroup"+productGroupID+" button.glyphicon").addClass("glyphicon-chevron-down");

			jQuery.ajax({
				type: 'POST',
				cache: false,
				url: page,
				data: parameters,
				success: function(data){
					openGroup.show();
					openGroup.html(data);	
				}
			});

		}

	}

function openProduct(encryptedLink,productID) {
		changeURL(productID);
		window.location.href = encryptedLink;
	}

function changeURL(productID) {
		var State = window.location.href;
		x = State.parseQuery()
		x.postLoadProductID = productID
		var newState = Object.toQueryString(x);
		try{history.pushState(null, null,'?' + newState);}catch(err){}
	}