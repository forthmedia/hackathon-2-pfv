<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			emails.cfm
Author:				YMA
Date started:		17-06-2013

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-08-14			YMA			Case 436224 Restrict apps list to ones that the current user has security access to use.
2013-12-18			WAB			Added the new Name field and rearranged
2014-08-13			RPW			Create ability to choose template for Standard Emails
2015-10-13          DAN         PROD2015-160 - validation for unique emailTextID
2016-10-17			YMA			PROD2016-2539 Add Active column and filter to system emails

Possible enhancements:


 --->

<!--- 2014-08-13			RPW			Create ability to choose template for Standard Emails --->
<cf_includeJavascriptOnce template="/genericEmails/js/emails.js">

<cfparam name="sortOrder" default="App,Name">
<cfset xmlSource = "">
<cfset getEmailDef = "">

<cffunction name="postSave" returntype="struct">
	<cfset var updateImageName = "">
	<cfset var result = {isOK=true,message=""}>
	<!--- files uploaded - an image in messages can never be deleted, so we always have an image --->
	<cfif structKeyExists(arguments,"filesUploaded") and arguments.filesUploaded neq "">
		<cfquery name="updateImageName" datasource="#application.siteDataSource#">
			update emaildef set messageimage =  <cf_queryparam value="#arguments.filesUploaded#" CFSQLTYPE="CF_SQL_VARCHAR" > where emaildefID = <cf_queryParam value="#arguments.emaildefID#" cfsqltype="cf_sql_integer">
		</cfquery>
	</cfif>

	<!--- 2014-08-13			RPW			Create ability to choose template for Standard Emails --->
	<cfscript>
		var pKColumnName = "EMAILDEFID";
		var entityType = "EMAILDEF";
		var editMode = "";
		if (structKeyExists(FORM,"UPDATE") && FORM.UPDATE=="Update") {
			editMode = "EDIT";
		} else {
			editMode = "ADD";
		}

		switch (editMode) {
			case "ADD":
				if (structKeyExists(FORM,"saveAsEmailTemplate") && Len(FORM.saveAsEmailTemplate) && structKeyExists(FORM,"PHRUPD_BODY_" & application.entityTypeID[entityType] & "_" & pKColumnName & "_")) {
					variables.HTMLBody = FORM["PHRUPD_BODY_" & application.entityTypeID[entityType] & "_" & pKColumnName & "_"];
					application.com.communications.saveEmailTemplate (HTMLBody = variables.HTMLBody, TextBody = "", templateName = saveAsEmailTemplate);
				}
				break;
			case "EDIT":
				if (structKeyExists(FORM,"saveAsEmailTemplate") && Len(FORM.saveAsEmailTemplate) && structKeyExists(FORM,"PHRUPD_BODY_" & application.entityTypeID[entityType] & "_" & form[pKColumnName] & "_")) {
					variables.HTMLBody = FORM["PHRUPD_BODY_" & application.entityTypeID[entityType] & "_" & form[pKColumnName] & "_"];
					application.com.communications.saveEmailTemplate (HTMLBody = variables.HTMLBody, TextBody = "", templateName = saveAsEmailTemplate);
				}
				break;
		}

	</cfscript>

	<cfreturn result>
</cffunction>


<cfif structKeyExists(url,"editor")>
    <cf_includeJavascriptOnce template="/genericEmails/js/editorEmails.js">

	<cfsavecontent variable="sendButtonHtml">
		<cfoutput>
			<cf_input type="button" value = "phr_emailDef_Test" name="frmTest" id="frmTest" onclick="testEmail();">
		</cfoutput>
	</cfsavecontent>

	<cfset excludeModuleTextID = "About">

	<cfset activeModules = application.com.relayMenu.getApps(excludeModuleTextID="excludeModuleTextID")>
	<cfset ModuleImages = application.com.settings.getsetting(variablename='module.thumbnails')>

	<cfoutput>
	<script>
		var moduleImageStruct = new Object();

		<cfloop query="activeModules">
			<cfif structKeyExists(moduleImages,moduleTextID)>
				moduleImageStruct["#moduleTextID#"] = "#moduleImages[moduleTextID]#";
			<cfelseif fileExists("#application.paths.relay#" & "/images/moduleicons/" & "#moduleTextID#" & "_thumb.png")>
				moduleImageStruct["#moduleTextID#"] = "/images/moduleIcons/#moduleTextID#_thumb.png";
			<cfelse>
				moduleImageStruct["#moduleTextID#"] = 0;
			</cfif>
		</cfloop>
	</script>
	</cfoutput>

	<cf_head>
	<cfoutput>
	<SCRIPT type="text/javascript">
		<cfif isDefined("emailDefID") and emaildefid gt 0>
		function testEmail() {
			frmCountryID = jQuery('##testCountry').val();
			frmLanguageID = jQuery('##testLanguage').val();
			testSendMessage = jQuery('##testSendMessage').is(":checked");
			jQuery('##testResults').html("");
			jQuery.ajax(
		    	{type:'get',
		        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayGenericEmailsWS&methodName=testEmail&_cf_nodebug=true&returnFormat=plain',
		        	data:{emailDefID:#emaildefid#,frmCountryID:frmCountryID,frmLanguageID:frmLanguageID,testSendMessage:testSendMessage},
		        	dataType:'html',
		        	success: function(data,textStatus,jqXHR) {
						jQuery('##testResults').html(data);
					}
				});
    	}

    	</cfif>
    	function setDefaultPlaceHolderImage() {
    		moduleTextID = jQuery('##moduleTextID option:selected').val();

	    	if(moduleImageStruct[moduleTextID] != 0){
		    	jQuery('##Placeholder').attr('src', moduleImageStruct[moduleTextID]);
		    	jQuery('##Placeholder').attr('onclick', 'javascript:setDefaultImage(this,"'+moduleImageStruct[moduleTextID]+'");');
	    	}else{
		    	jQuery('##Placeholder').attr('src', '/images/misc/blank.gif');
		    	jQuery('##Placeholder').attr('onclick', ';');
	    	}
    	}

    	document.ready = function() {setDefaultPlaceHolderImage()}
	</script>
	</cfoutput>
	</cf_head>

	<cfset listAlternateImages = 'ScreenEmails=' & moduleImages.ScreenEmails & ',Placeholder='>
	<!--- 2014-08-13			RPW			Create ability to choose template for Standard Emails --->
	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editor id="17" name="genericEmails" entity="emailDef" title="Generic Email">
			<field name="emaildefID" label="Unique Email ID" description="System generated numeric id for this email" control="html"/>
			<field name="Name" label="Name" description="Unique title for this email" required="true"/>
			<field name="emailTextID" label="Email Text ID" description="Unique ID for this email" onChange="javascript:validateEmailTextID(this.value);"/>
			<field name="" CONTROL="TRANSLATION" label="Subject" Parameters="phraseTextID=Subject,allPhraseTextIDsOnPage='subject,body,messageTitle,messageText'" required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="Body"  Parameters="phraseTextID=Body,displayAs=autoTextArea,rows=15,cols=60,showTextToolbar=true,showMergeFields=true"></field>
			<field name="AddressType" label="Address Type" description="Select an address type" control="select" query="SELECT 'personid' AS Value, 'The email of the Person (leave To Address Blank)' as Display UNION SELECT 'literal' AS Value, 'The email in the ToAddress field below (can be a phrase or a query). ' as Display UNION SELECT 'personIDQuery' AS Value, 'Send the email to and about a personID defined in a query from the ToAddress field below.' as Display"/>
			<field name="toAddress" label="To Address" description="The address this email should go to"/>
			<field name="ccAddress" label="CC Address" description="Address this email should be cc'd to"/>
			<field name="bccAddress" label="BCC Address" description="Address this email should be bcc'd to"/>
			<field name="fromAddress" label="From Address" description="The address you want this to come from"/>
			<field name="active" label="active" control="yorn"/>
			<field name="moduleTextID" onChange="setDefaultPlaceHolderImage();" label="phr_emailDef_moduleID" query="func:com.relayMenu.getApps(excludeModuleTextID=#excludeModuleTextID#)" nullText="phr_select_module" display="moduleName" value="moduleTextID"/>
			<field name="fileID" onChange="useTemplate('fileID');" label="Load Content from a Template" query="func:com.communications.getValidTemplates()" nullText="Select Template" display="description" value="fileID"/>
			<field name="saveAsEmailTemplate" label="Save this as a new Email Template called" description="Save this as a new Email Template"/>
			<group label="phr_emailDef_ActivityStreamOptions" condition="application.com.relayTags.isRelayTagInElement(relayTagName='relay_activity_Stream') gt 0">
				<field name="sendMessage" control="checkbox" label="Send Message"/>
				<field name="" CONTROL="TRANSLATION" label="phr_emailDef_messageTitle" size="70" description="The title of the message" requiredFunction=" return isItemInCheckboxGroupChecked(this.form.sendMessage,'1')" parameters="phraseTextID=messageTitle,maxLength=100"/>
				<field name="" CONTROL="TRANSLATION" label="phr_emailDef_messageText" description="The body of the message" requiredFunction="return isItemInCheckboxGroupChecked(this.form.sendMessage,'1')" parameters="phraseTextID=messageText,displayAs=TextArea,maxChars=500,shortenURL=true,showMergeFields=true,cols=50"/>
				<field name="messageUrl" label="phr_emailDef_messageUrl" description="The url of the message" size="70"/>
				<field name="" control="file" label="phr_emailDef_messageImage" acceptType="image" listAlternateImages="#listAlternateImages#" required="false" parameters="allowFileDelete=false,defaultImage=true,filePrefix=Thumb_,displayHeight=80,height=80,width=80,fileExtension=png" description=""></field>
			</group>
			<group label="phr_emailDef_Test_Options" condition="currentRecord.emailDefID neq 0">
				<field name="testCountry" label="Country For Test Emails" control="select" currentValue="#request.relaycurrentuser.location.countryID#" query="Select countryid as dataValue, countrydescription as displayValue from country where isocode is not null order by countrydescription" display="displayValue" value="dataValue" />
				<field name="testLanguage" label="Language For Test Emails" control="select"  currentValue="#request.relaycurrentuser.languageID#" query="select distinct languageid as dataValue,language as displayValue, case when sitedef.sitedefid is not null then 'Live' else 'Not Live' end as OptGroup,case when sitedef.sitedefid is not null then 1 else 0 end from language left join sitedef on dbo.listfind(livelanguageids,languageid ) &lt;&gt; 0 order by case when sitedef.sitedefid is not null then 1 else 0 end  desc" display="displayValue" value="dataValue" />
				<field name="testSendMessage" label="Don't send, just display" control="checkbox"/>
				<field name="testSend" label="" control="html">
					#htmlEditFormat(sendButtonHtml)#
				</field>
				<field name="testResults" label="" control="html" spanCols="yes">
					#htmlEditFormat('<div id="testResults"></div>')#
				</field>
			</group>
			<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
			</group>
		</editor>
		</cfoutput>
	</cfsavecontent>

<cfelse>

	<cfset permittedApps = application.com.relayMenu.getApps()>

	<!--- 2013-07-29	YMA	Case 436224 Translate modules to match the left menu. --->
	<cfquery name="getEmailDef" datasource="#application.siteDataSource#">
		SELECT
			emailDefID,
			emailTextID as Unique_ID,
			e.Name as Name,
			toAddress as To_Address,
			fromAddress as From_Address,
			sendMessage as send_message,
			case when len(isNull(rtrim(ltrim(e.moduleTextId)),'')) = 0 then ' Misc' else e.moduleTextId end as App,
			case when len(ltrim(rtrim(e.moduleTextID))) > 0  then '<img src="/images/moduleIcons/'+e.moduleTextID+'_thumb.png" height="16" width="16">' else null end as appImg,
			case when e.active = 1 then 'Yes' else 'No' end as active
			from eMailDef e
				left join relayModule rm on rm.moduleTextId = e.moduleTextId
			where isNull(rm.active,1) = 1
			and (len(isNull(rtrim(ltrim(e.moduleTextId)),'')) = 0 or e.moduleTextID in (#listQualify(valueList(permittedApps.moduleTextID),"'")#))
			order by e.moduleTextID
	</cfquery>

	<cfset previousApp = "">
	<cfset translatedTitle = "">

	<cfloop query="getEmailDef">
		<cfset currentApp = getEmailDef.app[currentrow]>
		<cfif currentApp neq previousApp>
			<cfif find("Misc",currentApp) eq 0>
				<cfset translatedTitle = application.com.relayMenu.getApps(moduleTextID=currentApp).moduleName>
			<cfelse>
				<cfset translatedTitle = "">
			</cfif>
			<cfset previousApp = currentApp>
		</cfif>

		<cfif translatedTitle neq "">
			<cfset getEmailDef.app[currentrow] = translatedTitle>
		</cfif>
	</cfloop>
	<cfset inQoQ = true>

	<cfquery name="getEmailDef" dbtype="query">
		select * from getEmailDef
		where 1 = 1
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>

</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	keyColumnList="Name"
	queryData="#getEmailDef#"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(unique_ID)##'*comma'##jsStringFormat(Unique_ID)##'*comma'/genericEmails/emails.cfm?editor=yes&hideBackButton=true&emailDefID=##emailDefID##'*comma{reuseTab:true*commaiconClass:'screens'});return false;"
	showTheseColumns="App,AppImg,Name,Unique_ID,To_Address,From_Address,active,Send_Message"
	hideTheseColumns="App"
	sortOrder="#sortOrder#"
	useInclude="false"
	booleanFormat="send_message"
	allowColumnSorting="yes"
	FilterSelectFieldList="App"
	FilterSelectFieldList2="active"
	groupByColumns="App"
	postSaveFunction = #postSave#
	columnTranslation=true
	columnTranslationPrefix="phr_report_emailDef_"
>