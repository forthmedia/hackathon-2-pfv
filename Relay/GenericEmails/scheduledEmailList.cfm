<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			scheduledEmailList.cfm
Author:
Date started:			/xx/07

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
09-DEC-2010		MS		LID4687: Phrases missing for Conditional Emails

2013-06-17		YMA		Updated to open new email editor.
2014-09-18		RPW	No Back button on Triggered Emails Screen

 --->

<cfset application.com.request.setTopHead(pageTitle = "Triggered Emails",currentTemplate = "scheduledEmailList.cfm")>

<cf_title>Triggered Email</cf_title>

<!--- 2014-09-18	RPW	No Back button on Triggered Emails Screen --->
<cf_head>
	<script language="JavaScript" type="text/javascript">
		function confirmDelete() {
			if (confirm("Are you sure you want to permanently Delete this Email?")) {
				return true;
			} else {
				return false;
			}
		}
	</script>
</cf_head>
<!--- getting the data to be displayed --->
<cfset getScheduledEmailList = application.com.scheduledEmails.getEmailSchedule() />

<!--- 2014-09-18	RPW	No Back button on Triggered Emails Screen --->
<cfscript>
	variables.deleteArray = [];
	for (variables.d=1;variables.d <= getScheduledEmailList.recordCount;variables.d++) {
		ArrayAppend(variables.deleteArray,'<img src="/images/MISC/iconDeleteCross.gif">');
	}
	QueryAddColumn(getScheduledEmailList, "Delete" , "VarChar", variables.deleteArray);

	/*application.com.relayui.displayMessage(
		scope="session",
		deleteMessage=true,
		closeAfter=3
	);*/
</cfscript>

<!--- 2014-09-18	RPW	No Back button on Triggered Emails Screen --->
<CF_tableFromQueryObject
	queryObject="#getScheduledEmailList#"
	keyColumnList="ScheduleName,eMailTextID,Delete"
	keyColumnOnClickList="void(0);,void(0);,return confirmDelete();"
	keyColumnURLList="scheduledEmail.cfm?scheduleID=,/genericEmails/emails.cfm?editor=yes&hideBackButton=false&emailDefID=,deleteScheduledEmail.cfm?scheduleID="
	keyColumnKeyList="ScheduleID,emailDefID,ScheduleID"
	hideTheseColumns="emailDefID"
	showTheseColumns="ScheduleName,emailDefID,eMailTextID,tableName,Delete"
	columnTranslation="True" 				<!--- 09-DEC-2010		MS		LID4687: added this parameter to enable translations for Conditional email headings --->
	columnTranslationPrefix="phr_email_" 	<!--- 09-DEC-2010		MS		LID4687: added this parameter to enable translations for Conditional email headings --->
	sortOrder="emailTextID"
	allowColumnSorting="no"
	useInclude="false">