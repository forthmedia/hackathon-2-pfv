/* �Relayware. All Rights Reserved 2015 */
/*
File name:			editorEmails.js
Author:				DAN
Date started:		13/10/2015

Description:	JS included when in the standard emails editor context

Amendment History:

Date (DD/MM/YYYY)	Initials 	What was changed
13/10/2015 DAN PROD2015-160 - validation for unique emailTextID

Possible enhancements:


*/


var emailTextIDOrigin = "";
function populateInitialEmailTextID() {
    emailTextIDOrigin = document.editorForm.emailTextID.value;
    jQuery('#editorForm').on('submit', function() {
        emailTextIDOrigin = document.editorForm.emailTextID.value;
    });
}

document.ready = function() {
    populateInitialEmailTextID();
}

// validate if emailTextID is unique (validation done in similar style to the profiles flagtextid)
function validateEmailTextID(emailTextID) {
    jQuery.ajax({
        type: "GET",
        url: "/webservices/callWebService.cfc?",
        data: {method:'callWebService',webServiceName:'relayGenericEmailsWS',methodName:'getEmailDefIDByTextID', emailTextID:emailTextID, returnFormat:'json'},
        dataType: "json",
        cache: false,
        async: true,
        success: function(data,textStatus,jqXHR) {
            var $_uniqueErrorMsgDiv = jQuery('#notUniqueMessage');
            if ($_uniqueErrorMsgDiv.length == 1) {
                $_uniqueErrorMsgDiv.remove();
            }
            if (data != 0 && emailTextID != emailTextIDOrigin) {
                // emailTextID already exists and its not the current email (if editting) 
                var msg = 'Email Text ID ' + emailTextID + ' is not unique';
                jQuery('#emailTextID').after('<div id="notUniqueMessage" class="errorblock">'+msg+'</div>');
                document.editorForm.emailTextID.value = emailTextIDOrigin; // set value back to the original/last saved
            }
        }
    })
}
