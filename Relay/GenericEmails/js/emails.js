/* �Relayware. All Rights Reserved 2014 */
/*
File name:			emails.js
Author:				RPW
Date started:		13-08-2014

Description:		Create ability to choose template for Standard Emails

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


*/

function useTemplate() {
	var fileID = jQuery('#fileID option:selected').val();

	if (fileID.length > 0) {

		jQuery.ajax(
		    	{type:'get',
		        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=fileWS&methodName=readFile&returnFormat=json',
		        	data:{fileID:fileID},
		        	dataType:'json',
		        	error: function() {},
		        	success: function(data,textStatus,jqXHR) {
		        		
		        		CKEDITOR.instances[CKInstanceName].setData(data.fileData);

					}
			});
		
	} else {
		CKEDITOR.instances[CKInstanceName].setData("");
	}

}
