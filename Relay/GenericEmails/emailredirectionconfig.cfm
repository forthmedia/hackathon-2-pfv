<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			emailRedirectionConfig.cfm
Author:				WAB
Date started:			2009/02/02

Description:
Used to set up the redirection of emails on a session by session basis

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-11-22 	SB Bootstrap

Possible enhancements:



--->


<!--- Process updates from the form --->
<cfif isdefined("frmSessionReplacementEmailAddress")>
	<cfset application.com.email.updateSessionReplacementEmailAddress (frmSessionReplacementEmailAddress)>
</cfif>

<cfif isDefined("frmRestoreDefaultReplacementEmailAddress")>
	<cfset application.com.email.updateSessionReplacementEmailAddress ("")>
</cfif>

<cfif isdefined("frmAddSessionRegExp")>
	<cfset application.com.email.addSessionallowedEmailAddressRegExp (frmAddSessionRegExp)>
</cfif>

<cfif isDefined("frmRemoveSessionRegExp")>
	<cfset  application.com.email.removeSessionallowedEmailAddressRegExp (frmRemoveSessionRegExp)>
</cfif>

<cfif isDefined("frmRestoreDefaultRegExp")>
	<cfset  application.com.email.restoreDefaultAllowedEmailAddressRegExp (frmRestoreDefaultRegExp)>
</cfif>


<!--- Get Current and Default Settings --->
<cfset currentSettings = application.com.email.getCurrentEmailRedirectionParameters ()>
<cfset defaultSettings = application.com.email.getDefaultEmailRedirectionParameters ()>
<cfset areEmailsBeingRedirected = application.com.email.doEmailsNeedToBeRedirected ()>

		<form id="emailRedirectionForm" name="emailRedirectionForm">
		<CF_relayFormDisplay>

			<cfif areEmailsBeingRedirected>
				<CF_relayFormElementDisplay relayFormElementType="html" label= "" currentvalue = "<div class='infoblock'>Emails are being redirected on this site</div>" spancols=true>

				<CF_relayFormElementDisplay relayFormElementType="text" fieldName ="frmSessionReplacementEmailAddress" label= "#iif(currentSettings.allowedEmailAddressRegExp is  "",de("<font color='red'>ALL </font>"),de("Most "))#Emails are being redirected to " currentvalue = "#htmleditformat(currentSettings.replacementEmailAddress)#"  noteText ="Enter an alternative email address to where all emails created in your session will be sent" noteTextPosition="Bottom">

				<cfif currentSettings.hasSessionReplacementEmailAddress>
					<CF_relayFormElementDisplay relayFormElementType="checkbox" fieldName = "frmRestoreDefaultReplacementEmailAddress" label= "Restore To Default (#defaultSettings.replacementEmailAddress#) " value = "1" currentvalue = 0 onclick="this.form.submit()">
				</cfif>

				<cfif currentSettings.allowedEmailAddressRegExp is not "">
					<CF_relayFormElementDisplay relayFormElementType="html" label= "Unless address matches #iif(listLen(currentSettings.allowedEmailAddressRegExp,"|") gt 1,de("any of"),de(""))# " currentvalue = "" >
						<cfoutput>
							<p>Remove email addresses.</p>
							<cfloop Index = "item" list = "#currentSettings.allowedEmailAddressRegExp#" delimiters  = "|">
								<div class="checkbox">
									<label><CF_INPUT type="checkbox" name = "frmRemoveSessionRegExp" value = "#item#" onclick="this.form.submit()"> #htmleditformat(item)#</label>
								</div>
							</cfloop>
						</cfoutput>
					</CF_relayFormElementDisplay>

					<cfset addLabel = "Add another Email Address (or pattern) which will not be redirected">

				<cfelse>
					<cfset addLabel = "Add an Email Address (or pattern) which will not be redirected">
				</cfif>

				<CF_relayFormElementDisplay relayFormElementType="text" fieldName ="frmAddSessionRegExp" label= "#addLabel#" currentvalue = ""  noteText ="Must 4 or more characters" noteTextPosition="Bottom">

				<cfif currentSettings.hasSessionallowedEmailAddressRegExp>
					<div class="checkbox">
						<CF_relayFormElementDisplay relayFormElementType="checkbox" fieldName = "frmRestoreDefaultRegExp" label= "Restore To Default " value = "1" currentvalue = 0 onclick="this.form.submit()">
					</div>
				</cfif>



				<CF_relayFormElementDisplay relayFormElementType="submit" label="" fieldname="Save">

			<cfelse>

				<CF_relayFormElementDisplay relayFormElementType="html" label= "" currentvalue = "Emails are not being redirected on this site" >

			</cfif>
		</CF_relayFormDisplay>
		</form>

