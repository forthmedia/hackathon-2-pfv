<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="current" type="string" default="index.cfm">
<cfparam name="templateType" type="string" default="list">
<cfparam name="pageTitle" type="string" default="Generic Emails">
<cfparam name="pagesBack" type="string" default="1">
<cfparam name="thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="#thisDir#">
	<CFIF findNoCase("scheduledEmailList.cfm",SCRIPT_NAME) neq 0 and isDefined("currentTemplate") and currentTemplate eq "scheduledEmailList.cfm">
		<CF_RelayNavMenuItem MenuItemText="Emails" CFTemplate="emails.cfm">
		<CF_RelayNavMenuItem MenuItemText="Add Triggered Email" CFTemplate="scheduledEmail.cfm">
	</CFIF> 

	<CFIF findNoCase("emails.cfm",SCRIPT_NAME) neq 0>	
		<CF_RelayNavMenuItem MenuItemText="Triggered Emails" CFTemplate="scheduledEmailList.cfm">
	<!--- 
		WAb 2009/03/13 add email to be done with emaildedit.cfm
			<CF_RelayNavMenuItem MenuItemText="Add New Email" CFTemplate="../templates/editXMLGeneric.cfm?add=Yes&Editor=genericEmails"> current
			--->
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="/genericEmails/emails.cfm?editor=yes&add=yes">
	</CFIF>

	<!--- 2014-09-18	RPW	No Back button on Triggered Emails Screen --->
	<CFIF findNoCase("scheduledEmail.cfm",SCRIPT_NAME) neq 0>
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="##" onClick="submitScheduledEmailForm();">
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="scheduledEmailList.cfm">
	</CFIF>

</CF_RelayNavMenu>