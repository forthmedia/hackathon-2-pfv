<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			deleteScheduledEmail.cfm
Author:				RPW
Date started:		2014-09-18

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-09-18		RPW	No Back button on Triggered Emails Screen

 --->

<cfscript>

	if (StructKeyExists(URL,"scheduleID") && IsNumeric(URL.scheduleID)) {
		application.com.scheduledEmails.deleteScheduledEmail(scheduleID=URL.scheduleID);
		variables.message = "Email Deleted";
		variables.messageType = "success";
	} else {
		variables.message = "An error occurred trying to delete the Email";
		variables.messageType = "warning";
	}

	application.com.relayui.setMessage(
		message=variables.message,
		type=variables.messageType,
		scope="session"
	);

	location (url="scheduledEmailList.cfm", addToken="no");

</cfscript>