<!--- �Relayware. All Rights Reserved 2014 --->
<!--- <cf_createUserNameAndPassword
		personid = "#frmpersonID#"
		resolveDuplicates = false
		OverWritePassword = false
		OverWriteUserName = false
	> --->
	
	
<!---
		DAM 19 September 2002
		
	1.	Added ability to specify a single personID from the result of a query
		passed in the parameters as person query. 
		Required conditions for this
		
		addressype = personid
		toAddress = query (normally this would actually be the personid
		parameters = ...,{parameterN},personquery=[SQL],{parameterM}...
		
		The SQL in person query can be anything so long as it selects one column
		containing a person id and aliases it to PID.
		
		CF_EvaledString is used to evaluate manually, any variables in the sql
		such as #request.relayCurrentUser.personid#. This is required because evaluate(#sql#)
		and evaluate("#sql#") and even evaluate(DE(#sql#)) can cause problems.
		
	2.  Added ability to invoke a Relay Query by stating which one to run in the 
		parameters column. The variables setup by this query are then accessible 
		to the email subject and body.

	MDC 20th July 2004
		I've added in the cfif statement around cfmailType row  37.  So that we can pass HTML in
		instead of plain text.  This has resolved an issue on the ATI Rejected email. Hungarian font
		was not displaying correctly in plain text.

	WAB corrected bug causing text emails to screw up foreign characters. 
	
	2004-12-20 - JC: Fixed bug where nested HTML not being being rendered correctly in nBody var
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate

2011/10/05 PPB LID7939 moved code for Actions to UGRecordManagerTask using sendEmail 
--->

<!--- 301104-Added by JC --->
<cfparam name="attributes.runGetPIDQuery" default="1">

<cfif not isDefined("cfmailType")>
	<cfparam name="attributes.cfmailType" default="text">
<cfelse>
 	<cfset attributes.cfmailtype = "cfmailType">
</cfif> 

<cfif not isDefined("runType")>
	<cfparam name="attributes.runType" default="Live">
<cfelse>
	<!--- JC: changed from <cfset attributes.runType = "runType">--->
 	<cfset attributes.runType = runType>
</cfif> 

<cfif not isDefined("selectionID")>
	<cfparam name="attributes.selectionID" default="0">
<cfelse>
 	<cfset attributes.selectionID = "selectionID">
</cfif>

<cfif not isDefined("emailTextID")>
	<cfif not isDefined("attributes.emailTextID")>
		eMailSender will only work if supplied with the variable emailTextID.
		<cfexit method="EXITTEMPLATE">
	<cfelse>
		<cfset emailTextID = attributes.emailTextID>
	</cfif>
</cfif>

<cfif isDefined("attributes.entityType") and isDefined("attributes.entityID")
	and attributes.entityType neq "" and attributes.entityID neq "">
	<cfswitch expression="#attributes.entityType#">
		<cfcase value="opportunity">
			<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
				select org.organisationname as Account, u.name as Owner,
				AccountMngr = case when o.vendoraccountmanagerPersonid > 0 then
							(select p.firstname + ' ' + p.lastname
							from person p 
							join opportunity o1 on p.personid = o1.vendoraccountmanagerPersonid
							and o1.opportunityid =  <cf_queryparam value="#attributes.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
							when o.vendoraccountmanagerPersonid = 0 then 'No Account Manager Assigned'
							end, 
				o.* 
				from opportunity o
				join usergroup u on u.usergroupid = o.createdby
				join organisation org on org.organisationid = o.entityid
				where o.opportunityid =  <cf_queryparam value="#attributes.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
				<cfset form.Account = GetEntityDetails.Account>
				<cfset form.Owner = GetEntityDetails.Owner>
				<cfset form.AccountMngr = GetEntityDetails.AccountMngr>
				<cfset form.Detail = GetEntityDetails.Detail>
		</cfcase>
		<!--- 2011/10/05 PPB LID7939 now we use sendEmail directly from UGRecordManagerTask for this case  --->
		<!--- 
		<cfcase value="actions">
			
			<cfquery name="getEntityType" datasource="#application.siteDataSource#">
				select * from actions where actionid = #attributes.entityID#
			</cfquery>
			<cfif getEntityType.entitytypeid eq 0>
				<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
				select pa.firstname + ' ' + pa.lastname as Owner, ActionWith = 
				case when a.entityid > 0 then (select p.firstname + ' ' + p.lastname + ' - ' + o.organisationname
					from person p
					join organisation o on o.organisationid = p.organisationid 
					join actions a1 on p.personid = a1.entityid
					and a1.actionid = #attributes.entityID#) end,
				a.*
				from actions a
				join person pa on pa.personid = a.ownerpersonid
				where a.actionid = #attributes.entityID#
				</cfquery>
				<cfset urlAttr = "frmsrchpersonID">
			<cfelseif getEntityType.entitytypeid eq 2>
			<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
				select pa.firstname + ' ' + pa.lastname as Owner, ActionWith = 
				case when a.entityid > 0 then (select o.organisationname
					from organisation o 
					join actions a1 on o.organisationid = a1.entityid
					and a1.actionid = #attributes.entityID#) end,
				a.*
				from actions a
				join person pa on pa.personid = a.ownerpersonid
				where a.actionid = #attributes.entityID#
				</cfquery>
				<cfset urlAttr = "frmsrchOrgID">
			</cfif>
				<cfset form.Owner = GetEntityDetails.Owner>
				<cfset form.ActionWith = GetEntityDetails.ActionWith>
				<cfset form.entityID = GetEntityDetails.entityID>
				<cfset form.ActionID = GetEntityDetails.ActionID>
				<cfset form.Description = GetEntityDetails.Description>
				<cfset form.DueDate = GetEntityDetails.Deadline>
				<cfset form.entityLink = "#request.currentSite.httpProtocol##HTTP_HOST#/index.cfm?relocateto=data/dataFrame.cfm?#urlAttr#=#entityID#">
			<!--- <cfabort showerror="#entityLink#"> --->
		</cfcase>
		--->
		<cfcase value="PersonRejectedEmail">
			<cfquery name="GetEntityDetails" datasource="#application.siteDataSource#">
				select firstname as FirstName, lastname as LastName
				from person 
				where personid =  <cf_queryparam value="#attributes.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfquery>
			<cfset form.Firstname = GetEntityDetails.FirstName>
			<cfset form.Lastname = GetEntityDetails.LastName>
		</cfcase>
	</cfswitch>
	
</cfif>

<CFQUERY NAME="getEmailDetails" DATASOURCE="#application.siteDataSource#">
	select * from emailDef where emailTextID =  <cf_queryparam value="#emailTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>	

	<cfquery Name="getSelections" DATASOURCE="#application.siteDataSource#">
		select title,description,created
		from selection
		where selectionID  IN ( <cf_queryparam value="#attributes.selectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfquery>

<!--- get the parameters from the parameters column --->
<CF_EvaluateNameValuePair
	NameValuePairs = #getEmailDetails.parameters#
>

<CFIF getEmailDetails.recordCount neq 1>
	<cfoutput>No email has been sent because no email with text ID #htmleditformat(emailTextID)# has been 
	defined.  Please use the email tool to define and email of this text ID.</cfoutput>
	<cfexit method="EXITTEMPLATE">
<CFELSE>

	<CFSET toAddress = "#getEmailDetails.toAddress#">
	<CFSET ccAddress = "#getEmailDetails.ccAddress#">
	<CFSET bccAddress = "#getEmailDetails.bccAddress#">
	<CF_EvaledString stringtoeval=#getEmailDetails.fromAddress#>
	<CFSET fromAddress = EvaledString>
	<!--- <CFIF refind("##[[:graph:]]*##",getEmailDetails.fromAddress) is not 0>
		<!--- if getEmailDetails.fromAddress is a hashed variable --->
		<CFSET fromAddress = evaluate("#getEmailDetails.fromAddress#")>
	<cfelse>
		<!--- if getEmailDetails.fromAddress is a literal string --->
		<CFSET fromAddress = "#getEmailDetails.fromAddress#">
	</cfif> --->
	
	<CFIF getEmailDetails.addressType eq "personid">
		<!--- only run query below for non-Scheduled Email Reminder types --->
		<cfif attributes.runGetPIDQuery EQ 1>
			<CFIF getEmailDetails.toAddress IS "Query" and isdefined("personquery")>			
			<!--- DAM 19 Sept 2002
				This indicates that the personid will be populated from the results of a query
				(see above)
			--->
				<CF_EvaledString stringtoeval=#personquery#>
				<CFQUERY name="GetPersonIDs" datasource="#application.SiteDataSource#">
					#preservesinglequotes(EvaledString)#
				</CFQUERY>
				
				<cfif getPersonIDs.recordCount gt 0>
					<!--- remove dups --->
					<CFSET tmppids="">
					<cfloop index="i" list="#ValueList(GetPersonIDs.PID)#">
						<cfif NOT ListFind(tmppids,i)>
							<cfset tmppids = listappend(tmppids,i)>
						</cfif>
					</cfloop>
					<CFSET pids="#tmppids#">
				<cfelse>
					<CFSET pids=0>
				</cfif>
			<CFELSE>
				<CFSET pids=#getEmailDetails.toAddress#>
			</CFIF>
		<cfelseif IsDefined("attributes.accountmanagerID")>
			<cfset pids=#attributes.accountmanagerID#>
		</cfif>
			<CFQUERY name="GetPersonDetails" datasource="#application.SiteDataSource#">	
				SELECT DISTINCT(p.personid), s.recordcount as Selection_Length,s.recordcount,c.EmailFrom, p.personid, p.FirstName as First_Name, p.Title,p.Username,p.personid,
				p.Password, lg.languageid, p.Salutation,
				p.LastName AS Last_Name, p.Email,org.OrganisationName AS Organisation_Name,
convert(varchar,(p.personid)) + '-' + convert(varchar,(p.personid & 1023) * (loc.locationid & 1023)) as magicNumber
				FROM         Selection s INNER JOIN
                      SelectionGroup sg ON s.SelectionID = sg.SelectionID RIGHT OUTER JOIN
                      UserGroup ug INNER JOIN
                      Country c INNER JOIN
                      Location loc ON c.CountryID = loc.CountryID INNER JOIN
                      Person p ON loc.LocationID = p.LocationID LEFT OUTER JOIN
                      Language lg ON lg.[Language] = p.[Language] INNER JOIN
                      organisation org ON p.OrganisationID = org.OrganisationID ON ug.PersonID = p.PersonID ON sg.UserGroupID = ug.UserGroupID
				WHERE p.PersonID  in ( <cf_queryparam value="#pids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<CFIF attributes.runType eq "Test">
					and s.description LIKE '%_test%'
				</cfif>
			</CFQUERY>
			<CFSET form.firstname = "#Getpersondetails.First_Name#">
			<CFSET form.lastname = "#Getpersondetails.Last_Name#">
			<CFSET form.toAddress = "#Getpersondetails.email#">
			<cfset form.Salutation= "#Getpersondetails.Salutation#">
			<cfset form.title= "#Getpersondetails.title#">
			<cfset form.username="#getpersondetails.username#">
			<cfset form.password="#getpersondetails.password#">
			<cfset form.personid="#getpersondetails.personid#">
			<cfset form.magicNumber="#getpersondetails.magicNumber#">
				
		
			<CFIF getpersondetails.recordcount gte 1>
				<CFIF isNumeric(Getpersondetails.languageid)>
					<CFSET thisPageLanguageID=Getpersondetails.languageid>
				<CFELSE>
					<CFSET thisPageLanguageID=1>
				</CFIF>
				<CFSET thisPageButtonID="e">
			</CFIF>
		</CFIF>
		<CFSET phrases = getEmailDetails.bodyPhraseTextID & "," & getEmailDetails.subjectPhraseTextID>
	
		<CFSET nSubject=getEmailDetails.subjectPhraseTextID>
		<CFSET nBody=getEmailDetails.bodyPhraseTextID>
		<CFQUERY name="GetBody" datasource="#application.SiteDataSource#">
			select PhraseText
			from phraselist pl, phrases p
			where pl.phrasetextid =  <cf_queryparam value="#nBody#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			and pl.phraseid = p.phraseid
		</CFQUERY>
		<CFQUERY name="GetSubj" datasource="#application.SiteDataSource#">
			select PhraseText
			from phraselist pl, phrases p
			where pl.phrasetextid =  <cf_queryparam value="#nSubject#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			and pl.phraseid = p.phraseid
		</CFQUERY>
		<cfoutput>
			<cfset nBody = "#GetBody.PhraseText#">
			<cfset nSubject = "#GetSubj.PhraseText#">
		</cfoutput>
		
		<CFIF isDefined("getEmailDetails.includeTemplate") and getEmailDetails.includeTemplate neq "">
			<CFINCLUDE TEMPLATE="#getEmailDetails.includeTemplate#">
		</CFIF>	
	<CFIF toAddress neq "">
		<cfif attributes.runType EQ "live">
			 <!---
			 === Send to Screen ===
			 <cfoutput>
				to="#toAddress#"
		        from="#fromAddress#"
		        subject="#nSubject#"
		        cc="#ccAddress#"
		        bcc="#bccAddress#"
		        type="#attributes.cfmailType#"<br>
				<!--- display selection list --->
				#nBody##getSelections.description#<br>
				<cfif getSelections.recordcount eq 1>
				Related selection: #nSubject# #DateFormat(getSelections.created,"dd-mmm-yy")#
				</cfif><hr>
			</cfoutput>
			--->
			
			<!--- ccAddress is obtained from EmailDef table --->
	<!--- work around the email falls over if a phrase contains a double quote os we are switching doubles for single quoates. ---> 
			<cfset nbody = ReplaceNoCase(nBody, '"', "'", "ALL")>
				
<CF_MAIL to="#form.toAddress#"
   from="#fromAddress#"
   subject="#nSubject#"
   cc="#ccAddress#"
   bcc="#bccAddress#"
   type="#attributes.cfmailType#"
><cf_translate>#nbody#</cf_translate>
<cfif getSelections.recordcount eq 1>
Related selection: #nSubject# - #DateFormat(getSelections.created,"dd-mmm-yy")#
</cfif>
</cf_mail>

		<CFELSEIF attributes.runType eq "Test">
			
			<CFIF GetPersonDetails.Recordcount gt 0>
				<CF_tableFromQueryObject queryObject="#GetPersonDetails#" 
				keyColumnList=""
				keyColumnURLList=""
				keyColumnKeyList=""
				hideTheseColumns=""
				showTheseColumns="Organisation_Name,First_Name,Last_Name,Email,Selection_Length"
				sortOrder="Organisation_Name,First_Name,Last_Name,Email"
				allowColumnSorting="no"
				useInclude = "False">

	 			<cfoutput>
				<table align="center" border=1>
					<tr><td>Email Contents</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>#htmleditformat(nBody)#</td></tr>	
					<tr><td>&nbsp;</td></tr>				
					<cfif getSelections.recordcount eq 1>
						<tr><td>Selection created: #htmleditformat(getSelections.title)# #htmleditformat(getSelections.description)# #htmleditformat(getSelections.created)#</td></tr>	
					</cfif>
				</table>
				</cfoutput>
			<CFELSE>
				No emails will be sent
			</CFIF>
		</CFIF>
	</CFIF>
</CFIF>

