<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

	This ensures that all necessary navigation hierarchies are available.

 --->
<cfparam name="url.NavID" default="">
 <cftry> 
	<cfscript>
		treeSuccess = application.com.relayProductTree.setupNavigation(treeID = url.NavID);
	</cfscript>
	
 	<cfcatch type="lock">
		<p>Please wait while the tree is updated. This can take up to 1 minute...</p>
		<script language="javascript"> 
			var reloadTimer = null;
			window.onload = function()
			{
			    setReloadTime(5); // In this example we'll use 5 seconds.
			}
			function setReloadTime(secs) 
			{
			    if (arguments.length == 1) {
			        if (reloadTimer) clearTimeout(reloadTimer);
			        reloadTimer = setTimeout("setReloadTime()", Math.ceil(parseFloat(secs) * 1000));
			    }
			    else {
			        location.reload();
			    }
			}
		</script> 
		<CF_ABORT> 
	</cfcatch> 
</cftry>
