<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			products.cfm
Author:				NJH
Date started:		2012/12/05

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-06-25	YMA		Case 435916 Pass in treatEmtpyFieldAsNull so min / max seats can be null.
2014 - 02-21	NJH		Case 438891 Reduce filter options. Moved SKU and title to search based filters
2014-09-18	RPW		Demo enhancement- product listing screen
2014-10-03	RPW		CORE-750 In Claim catalogue, when clicked the SKU field error is thrown. Changed call to salesforce to call connector
2014-12-01	AXA 	Case 442850 Per discussion with VB and JM exposing description field as an editable name/label to the user.
2015-01-07 	WAB 	CASE 442817 fix checkSeats function
2015-11-16  DAN PROD2015-364 - products country restrictions

2015-11-16  DAN PROD2015-364 - products country restrictions
2016-01-05	WAB	PROD2015-290 Replace calls to sql function fn_getCountryRights with getCountryScopeList
2016-06-10	WAB BF-975 / CASE 449815. When incentive Module not Active, JS to set discountPrice to required fails because field is not there
Possible enhancements:


 --->

<cf_title>Products</cf_title>

<cfsetting requesttimeout="300">

<!--- 2014-09-18	RPW		Demo enhancement- product listing screen --->
<cfparam name="campaignID" type="numeric" default=0>
<cfparam name="getProducts" type="string" default="">
<cfparam name="sortOrder" default="country,productGroup,sku">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="hideTheseColumns" type="string" default="productID">
<cfparam name="showTheseColumns" type="string" default="ProductCategory,ProductGroup,ProductCatalogue,SKU,Country,Title,ProductType,LastUpdated">
<cfparam name="url.active" type="Boolean" default="1">
<cfparam name="productID" type="numeric" default=0>

<!--- 2014-09-18	RPW		Demo enhancement- product listing screen --->
<cfparam name="filterBy" type="string" default="ProductCategory,ProductGroup,ProductCatalogue,Country,ProductType">

<cfset xmlSource = "">

<cfif structKeyExists(url,"editor")>

<cfset campaignName = "">
<cfset incentiveCatalogue = false>

<cfif structKeyExists(url,"campaignID") and isNumeric(url.campaignID) and campaignID neq 0>
	<cfquery name="getCampaignName" datasource="#application.siteDataSource#">
		select promoName from promotion where campaignId=<cf_queryparam value="#url.campaignID#" cfsqltype="cf_sql_integer">
	</cfquery>

	<cfset campaignName = getCampaignName.promoName>
	<cfif left(campaignName,9) eq "Incentive">
		<cfset incentiveCatalogue = true>
	</cfif>
</cfif>


<cf_head>
<!--- WAB 2015-01-07 CASE 442817 fix checkSeats function - was not using parseInt so was doing alphabetial comparison! --->
<script>
	function checkSeats() {

		if ( $('productType').value == 'License'
					&& (
						isNaN( parseInt ($('minSeats').value )) ||
						isNaN( parseInt ($('maxSeats').value )) ||
						$('maxSeats').value == 0 ||
						(parseInt ($('minSeats').value) > parseInt ($('maxSeats').value))
					)
		){
			return 'Please add a valid seat range';
		}
	}

	function checkCampaign() {
		<cfif campaignName eq "">
		var campaignName = jQuery('#campaignID').find(":selected").text();
		if (campaignName.substring(0,9) == 'Incentive') {
			return true;
		} else {
			return false;
		}
		<cfelseif incentiveCatalogue>
		return true;
		<cfelse>
		return false
		</cfif>
	}

	jQuery(document).ready(function() {
		isIncentiveCampaign = checkCampaign();
		setRequired($('listPrice'),isIncentiveCampaign);
		if ($('discountPrice')) {
			setRequired($('discountPrice'),isIncentiveCampaign);
		}	
	});
</script>
</cf_head>

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="product" title="Product Editor">
			<field name="productID" label="phr_product_productID" description="This records unique ID." control="html"></field>
			<field name="sku" label="phr_product_sku" description="The product sku" required="true"/>
			<!--- 2014-12-01 AXA Case 442850 Per discussion with VB and JM exposing description field as an editable name/label to the user. --->
			<field name="description" label="phr_product_label" description="The product label" size="50" />
			<field name="" label="phr_product_title" description="The product title" required="true" control="translation" parameters="phraseTextID=title,allPhraseTextIDsOnPage='title,description,longDescription'"/>
			<cfif application.com.relayMenu.isModuleActive(moduleID="Connector")>
				<!--- 2014-10-03	RPW		CORE-750 In Claim catalogue, when clicked the SKU field error is thrown. Changed call to salesforce to call connector --->
				<field name="crmProductID" label="phr_product_#application.com.settings.getSetting('connector.type')#ID" description="The product CRM ID" disabled="true" NoteTextImageSrc="func:getObject('connector.com.connector').getEntitySynchingStatus(entityId=#productID#,entityType='product').statusImage" NoteTextPosition="right" NoteText="func:getObject('connector.com.connector').getEntitySynchingStatus(entityId=#productID#,entityType='product').status"/>
			</cfif>
			<field name="productGroupID" label="phr_product_productGroup" required="true" description="" control="select" query="func:com.relayProduct.getProductGroups()" name1="dummy" name2="productGroupId" display="title" group="productCategoryTitle" value="productGroupID" displayGroupAs="select" required1="false" required2="true" EmptyText1="phr_Ext_SelectAValue"></field>
			<field name="campaignID" label="phr_product_Campaign" description="" required="true" <cfif campaignName neq "">control="html" makeHidden="true" <cfelse>control="select" </cfif> query="func:com.relayProduct.getProductCampaigns()" nulltext="phr_Ext_SelectAValue" display="campaign" value="campaignID"></field>
            <field name="" label="phr_restrictToCountries" description="" control="countryScope" width="200"></field>
			<field name="active" label="phr_product_Active" description="" control="YorN"></field>
			<field name="deleted" label="phr_product_deleted" description="" control="YorN"></field>
			<group label="Display Information" collapse="yes">
				<field name="" label="phr_product_description" description="The product description" control="translation" parameters="phraseTextID=description,displayAs=TextArea"/>
				<field name="" label="phr_product_longDescription" description="The product long description" control="translation" parameters="phraseTextID=longDescription,displayAs=TextArea"/>
				<field name="" control="file" acceptType="image" label="phr_product_ThumbnailImage" parameters="filePrefix=Thumb_,fileExtension=jpg,displayHeight=80,height=80,width=80" description=""></field>
				<field name="" control="file" acceptType="image" label="phr_product_HighResImage" parameters="filePrefix=highRes_,fileExtension=jpg,displayHeight=80" description=""></field>
				<field name="" control="file" label="phr_product_pdf" description="" accept="pdf" parameters="filePrefix=PDF_"></field>
			</group>
			<group label="Pricing Information">
				<field name="productType" label="phr_product_productType" description="The product type" validValues="Unit,License"/>
				<lineItem label="Seats">
					<field name="seatValidation" control="hidden" label="Seats" submitFunction="return checkSeats();" labelAlign=""/>
					<field name="minSeats" label="phr_product_minSeats" description="" size="10" range="1,"/>
					<field name="maxSeats" label="phr_product_maxSeats" description="" size="10" range="1,"/>
				</lineItem>
				<field name="listPrice" label="phr_product_listPrice" description="The product list price" size="10" range="1," requiredFunction="return checkCampaign();"/>
				<cfif application.com.relayMenu.isModuleActive(moduleID="Incentive")>
					<field name="discountPrice" label="phr_product_discountPrice" description="The product discount price" size="10" range="1," requiredFunction="return checkCampaign();"/>
				</cfif>
				<field name="priceISOCode" label="phr_product_currency" description="The product currency" control="select" query="func:com.relayProduct.getProductCurrencies()" display="currencyName" value="currencyISO"/>
				<field name="resellerDiscount" label="phr_product_resellerDiscount" description="The reseller discount" size="10"/>
				<field name="distiDiscount" label="phr_product_distiDiscount" description="The distribution discount" size="10"/>
				<field name="renewalSKU" label="phr_product_renewalSku" size="10" description="The product renewal sku" bindfunction="cfc:relay.webservices.relayOpportunityws.getProducts(productGroupID={productGroupID},campaignID={campaignID})" display="SKU" value="SKU" nullText="phr_Ext_SelectAValue"/>
			</group>
			<cfif application.com.relayMenu.isModuleActive(moduleID="Incentive")>
				<group label="Incentive Information">
					<field name="accrualMethod" label="phr_product_accrualMethod" description="The product accrual method" validValues="A#application.delim1#Automatic,M#application.delim1#Manual"/>
				</group>
			</cfif>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfelse>
	<cfset rights = application.com.rights.getRightsFilterWhereClause(entityType="Product",alias="p")>

	<!--- 2014-09-18	RPW		Demo enhancement- product listing screen --->
	<cfquery name="getProducts" datasource="#application.siteDataSource#">
		select * from (
			select distinct p.productID,p.productGroupID,p.sku,
                ISNULL( dbo.getCountryScopeList(productid,'product',1,1), 'All Countries') as country,
				<!--- case when isNull(p.countryID,0) = 0 then 'All Countries' else c.countryDescription end as country, --->
                p.title_defaultTranslation as title,
				pg.title_defaultTranslation as productGroup,p.priceISOCode as currency, p.active,p.deleted,
				pc.title_defaultTranslation as productCategory,
				p.lastUpdated,p.productType,p.campaignID,
				pm.promoName ProductCatalogue
			from product p
				<!--- #rights.join# --->
                left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid
				inner join productGroup pg on pg.productGroupID = p.productGroupID
				left join productCategory pc on pc.productCategoryID = pg.productCategoryID
				JOIN promotion pm ON p.campaignID = pm.campaignID
			where
				1=1
				<!--- and ((isNull(p.countryID,0) != 0 #rights.whereClause#) or isNull(p.countryID,0) = 0) --->
                and (p.hascountryScope = 0
                    <cfif request.relayCurrentUser.isInternal>
                        OR vcs.countryid in (select countryid FROM vCountryRights vcr with (noLock) WHERE vcr.personid = <cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="CF_SQL_INTEGER"> )
                    <cfelse>
                        OR vcs.countryid = <cf_queryparam value="#request.relaycurrentuser.countryID#" cfsqltype="CF_SQL_INTEGER">
                    </cfif>
                )
				<cfif campaignID neq 0>
					and p.campaignID = <cf_queryParam value="#campaignID#" cfSqlType="CF_SQL_INTEGER">
				</cfif>
			) base
			where 1=1
			<cfif url.active>
				and isNull(deleted,0) = <cf_queryParam value="#not(url.active)#" cfSqlType="CF_SQL_BIT">
				and isNull(active,1) = <cf_queryParam value="#url.active#" cfSqlType="CF_SQL_BIT">
			<cfelse>
				and (isNull(deleted,0) = <cf_queryParam value="#not(url.active)#" cfSqlType="CF_SQL_BIT">
					or isNull(active,1) = <cf_queryParam value="#url.active#" cfSqlType="CF_SQL_BIT">)
			</cfif>
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>

	<cfset pageTitle = "Products">
	<cfif campaignID neq 0>
		<cfquery name="getCatalogue" datasource="#application.siteDataSource#">
			select promoName from promotion where campaignID = <cf_queryparam value="#campaignID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfset pageTitle = getCatalogue.promoName& " Products">
	</cfif>

	<cfset application.com.request.setDocumentH1(text=pageTitle)>
	<cfset application.com.request.setTopHead(campaignID=campaignID)>
</cfif>

<cfset campaignURL = "">
<cfif structKeyExists(url,"campaignID") and isNumeric(url.campaignID)>
	<cfset campaignID = url.campaignID>
</cfif>

<cfif isDefined("campaignID") and campaignID neq 0>
	<cfset campaignURL = "&campaignID=#campaignID#">
</cfif>

<!--- default the search comparitor to an equal sign --->
<cfif not structKeyExists(form,"frmSearchComparitor") or form.frmSearchComparitor eq "">
	<cfset form.frmSearchComparitor = 2>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="products"
	keyColumnList="SKU"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(SKU)##'*comma'##jsStringFormat(SKU)##'*comma'/products/products.cfm?editor=yes&hideBackButton=true&productID=##productID###campaignURL#'*comma{reuseTab:true*commaiconClass:'products'});return false;"
	showTheseColumns="#showTheseColumns#"
	FilterSelectFieldList="#filterBy#"
	FilterSelectFieldList2="#filterBy#"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getProducts#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	groupByColumns="ProductCategory"
	hideTheseColumns=""
	dateFormat="lastUpdated"
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_product_"
	treatEmtpyFieldAsNull="true"
	searchColumnList="SKU,Title"
>