<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			products.cfm
Author:				NJH
Date started:		2012/12/05

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:


 --->

<cfsetting requesttimeout="300">

<cfparam name="campaignID" type="numeric" default=0>
<cfparam name="getProductCategories" type="string" default="">
<cfparam name="sortOrder" default="Title">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="showTheseColumns" type="string" default="Title,LastUpdated">
<cfparam name="url.active" type="Boolean" default="1">

<cfparam name="filterBy" type="string" default="#showTheseColumns#">

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="productCategory" title="ProductCategory Editor">
			<field name="productCategoryID" label="phr_productCategory_productCategoryID" description="This records unique ID." control="html"></field>
			<field name="" label="phr_productCategory_title" required="true" description="The product title" control="translation" parameters="phraseTextID=title"/>
			<field name="active" label="phr_productCategory_Active" description="The display of active product categories." control="YorN"></field>
			<group label="Display Information">
				<!--- <field name="" label="phr_productGroup_description" description="The product group description" control="translation" parameters="phraseTextID=description,displayAs=TextArea"/> --->
				<field name="" control="file" acceptType="image" label="phr_productCategory_ThumbnailImage" parameters="filePrefix=Thumb_,fileExtension=jpg,displayHeight=80,height=80,width=80" description=""></field>
			</group>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(url,"editor")>

	<cfquery name="getProductCategories" datasource="#application.siteDataSource#">
		select * from (
			select pc.productCategoryID, pc.title_defaultTranslation as title,
				pc.active, pc.lastUpdated
			from productCategory pc
			) base
			where 1=1
			and active = <cf_queryParam value="#url.active#" cfSqlType="CF_SQL_BIT">
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>


<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="productGroups"
	keyColumnList="Title"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(Title)##'*comma'##jsStringFormat(Title)##'*comma'/products/productCategories.cfm?editor=yes&hideBackButton=true&productCategoryID=##jsStringFormat(productCategoryID)##'*comma{reuseTab:true*commaiconClass:'products'});return false;"
	showTheseColumns="#showTheseColumns#"
	FilterSelectFieldList="#filterBy#"
	FilterSelectFieldList2=""
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getProductCategories#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	dateFormat="lastUpdated"
	showSaveAndAddNew="true"
	hideBackButton="true"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_productCategory_"
>