<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
   1999-02-21  SWJ Modified javascript to call report/wabcommcheck.cfm
   AR 2005-05-13 Added the GlobalSKU changes.
	NJH 2009/03/16 CR-SNY675 added additionalParams when editing/adding a product. Needed to pass module through.
	2014-09-24	DXC Case #441615 Cannot access Inactive Product Groups
--->

<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="edit">
<cfparam name="pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<cfif not structKeyExists(url,"editor")>
	<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="#attributes.thisDir#">

	<!--- NJH 2009/03/16 CR-SNY675 - pass through the module that we're in if it's defined, so that we can return to the proper product listing page
		when we're done editing a product --->
	<cfset additionalParams = "">
	<cfif structKeyExists(url,"module")>
		<cfset additionalParams = additionalParams&"&module="&url.module>
	</cfif>

	<CFIF findNoCase("pricebooks.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Catalogs" CFTemplate="catalogues.cfm">
		<CF_RelayNavMenuItem MenuItemText="Add Pricebook" CFTemplate="#application.com.security.encryptURL(url='pricebooks.cfm?editor=yes&add=yes&campaignID=#campaignID#')#">
	</CFIF>
	<!---
	<CFIF findNoCase("pricebookEdit.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Pricebook List" CFTemplate="#application.com.security.encryptURL(url='pricebookList.cfm?campaignID=#campaignID#')#">
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:submitform();">
	</CFIF>  --->

	<!--- 2013 Roadmap --->
	<cfif findNoCase("products.cfm",SCRIPT_NAME,1) or SCRIPT_NAME contains "productList.cfm"> <!--- NJH 2013/04/-8 - added productList.cfm to show menu in incentive claims and rewards catalog --->
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Catalogs" CFTemplate="catalogues.cfm">
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='products.cfm?editor=yes&add=yes&campaignID=#campaignID#&hideBackButton=false')#">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Inactive Products" CFTemplate="products.cfm?active=0&campaignID=#campaignID#">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Active Products" CFTemplate="products.cfm?active=1&campaignID=#campaignID#">
		</cfif>

	<cfelseif findNoCase("productGroups.cfm",SCRIPT_NAME,1)>
		<!--- START 2014-09-24	DXC Case #441615 Cannot access Inactive Product Groups --->
		<cfif url.active eq 0>
			<CF_RelayNavMenuItem MenuItemText="Product Groups" CFTemplate="productGroups.cfm?active=1">
		<Cfelse>
			<CF_RelayNavMenuItem MenuItemText="Inactive Product Groups" CFTemplate="productGroups.cfm?active=0">
		</cfif>
		<!--- END 2014-09-24	DXC Case #441615 Cannot access Inactive Product Groups --->
		<CF_RelayNavMenuItem MenuItemText="Products" CFTemplate="products.cfm?active=1">
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='productGroups.cfm?editor=yes&add=yes&hideBackButton=false')#">

	<cfelseif findNoCase("productCategories.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Products" CFTemplate="products.cfm?active=1">
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='productCategories.cfm?editor=yes&add=yes&hideBackButton=false')#">
	</cfif>

</CF_RelayNavMenu>
</cfif>
