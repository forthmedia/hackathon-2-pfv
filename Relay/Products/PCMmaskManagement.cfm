<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			pcmMaskManagement.cfm	
Author:				NJH
Date started:		2006-10-01
	
Purpose:	manipulate masks and sets for pcm product text sections 
			
Usage: 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->




<cf_head>
	<cf_title>PCM Product Masks and Sets</cf_title>
</cf_head>



<cf_translate>

<cfparam name="sectionID" type="numeric" default="0">

<cfparam name="variables.cboSiteDefID" type="numeric" default="">
<cfparam name="variables.cboSourceID" type="numeric" default="">
<cfparam name="variables.cboRemoteCategoryID" type="numeric" default="">

<cfparam name="request.showAttributeGroupMaskForSectionTypeIDList" type="string" default=""> <!--- set in pcmINI.cfm --->
<cfparam name="request.showAttributeMaskForSectionTypeIDList" type="string" default="">
<cfparam name="frmsectionMasked" default="0">
<cfparam name="request.globalMaskSectionTypeIDList" type="string" default="">


<cfif fileexists("#application.paths.code#\cftemplates\pcmINI.cfm")>
	<!--- pcmINI is used to over-ride default global application variables and params --->
	<cfinclude template="/code/cftemplates/pcmINI.cfm">
</cfif>

<cfif variables.cboSiteDefID eq "" and variables.cboSourceID eq "" and variables.cboRemoteCategoryID eq "">
	<cfoutput>cboSiteDefID, cboSourceID and cboRemoteCategoryID (scope form,url or variables) must be passed as a parameter.</cfoutput>
	<CF_ABORT>
</cfif>

<cfif listFind(request.globalMaskSectionTypeIDList,variables.cboMaskTypes)>
	<cfset maskSiteDefID = 0>
<cfelse>
	<cfset maskSiteDefID = variables.cboSiteDefID>
</cfif>

<cfscript>
	qry_get_PCMProductMask=application.com.relayProductTree.getPCMProductMask(sourceid=#variables.cbosourceid#,SiteDefID=#maskSiteDefID#,RemoteCategoryID=#variables.cboRemoteCategoryID#);
	pcmProductMaskID = qry_get_PCMProductMask.maskID;
</cfscript>

<cfif qry_get_PCMProductMask.RecordCount is 0>
	<cfscript>
		pcmProductMaskID = application.com.relayProductTree.insPCMProductMask(MaskDescription='',sourceid=#variables.cbosourceid#,SiteDefID=#maskSiteDefID#,RemoteCategoryID=#variables.cboRemoteCategoryID#);
	</cfscript>
</cfif>

<!--- <cfscript>
	qry_get_PCMProductMaskID=application.com.relayProductTree.getPCMProductMask(sourceid=#variables.cbosourceid#,SiteDefID=#variables.cboSiteDefID#,RemoteCategoryID=#variables.cboRemoteCategoryID#);
</cfscript> --->

<!--- get details about the specific section --->
<cfquery name="qry_get_sectionDetails" datasource="#application.siteDataSource#">
	select * from PCMProductTextSection
	where remoteCategoryID =  <cf_queryparam value="#variables.cboRemoteCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and sectiontypeID =  <cf_queryparam value="#variables.cboMaskTypes#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<!--- see if there is a mask already in place --->

<cfquery name="qry_chk_pcmProductMaskTransformation" datasource="#application.siteDataSource#">
	select * from pcmProductMaskTransformation
	where sectionID =  <cf_queryparam value="#qry_get_sectionDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
	AttributeID is NULL and
	sectionAttributeGroupid is NULL and
	MaskID =  <cf_queryparam value="#pcmProductMaskID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
	transformationActionID=1
</cfquery>


<cfoutput query="qry_chk_pcmProductMaskTransformation">
	<cfset frmsectionMasked=transformationValue>
</cfoutput>

<!--- if we're updating masks and we have masks to update or we're displaying the masks and the section is in the list of sections we need to display --->
<!--- <cfif (isDefined("frmUpdateAttributes") and ((isDefined("frmAttributeIDList") and frmAttributeIDList neq "") or (isDefined("frmAttributeGroupIDList") and frmAttributeGroupIDList neq "") or (isDefined("marketingStatementAttributeIDList") and marketingStatementAttributeIDList neq ""))) or 
	(not isDefined("frmUpdateAttributes") and (listFind(request.showAttributeGroupMaskForSectionTypeIDList,variables.cbomasktypes) or listFind(request.showAttributeMaskForSectionTypeIDList,variables.cbomasktypes)))>
 	<cfset productSections = application.com.relayProductTree.getProductSectionDetails(RemoteCategoryID=cboRemoteCategoryID,SiteDefID=maskSiteDefID,SourceID=cboSourceID)>
</cfif> --->

<!--- saving changes made to section attributes masks  --->
<cfif isDefined("frmUpdateAttributes")>

<!--- 	<cfif listContains(request.globalMaskSectionTypeIDList,variables.cboMaskTypes)>
		<cfset maskSiteDefID = 0>
	<cfelse>
		<cfset maskSiteDefID = cboSiteDefID>
	</cfif> --->
	
	<!--- NJH 2006-12-14 just call the query if we have to, as it's hefty --->
	<!--- to do: move the query to the beginning of the file, so that it's only called once.... --->
 	<cfif (isDefined("frmAttributeIDList") and frmAttributeIDList neq "") or (isDefined("frmAttributeGroupIDList") and frmAttributeGroupIDList neq "") or (isDefined("marketingStatementAttributeIDList") and marketingStatementAttributeIDList neq "")>
	 	<cfset productSections = application.com.relayProductTree.getProductSectionDetails(RemoteCategoryID=cboRemoteCategoryID,SiteDefID=maskSiteDefID,SourceID=cboSourceID)>
	</cfif>
	
	<cfif isDefined("frmAttributeIDList") and frmAttributeIDList neq "">
	
		<!--- updating the Attributes --->
		<cfloop list="#frmAttributeIDList#" index="attributeID">
		
			<cfquery name="getAttributeDetails" dbtype="query">
				select remoteAttributeID,currentAttributeMask, maskID,defaultAttributeMask,sectionID,
					defaultAttributeSequence,attributeTransformationSequence,attributeTransformationMask,currentAttributeSequence, attributeTransformationSequence
				from productSections 
				where remoteAttributeID = '#attributeID#' and sectionTypeID = #variables.cboMaskTypes#
			</cfquery>
			
			<cfset actionList = "1,2"> <!--- 1=mask, 2=sequence --->
			
			<cfif variables.cboMaskTypes eq 1>  <!--- if we're in overview, just mask --->
				<cfset actionlist = "1">
			</cfif>
			
			<cfloop list=#actionlist# index="actionID">	
			
				<cfswitch expression="#actionID#">
					<cfcase value="1">
						<cfif isDefined("frmAttributeMask#attributeID#")>
							<cfset newActionValue = evaluate("frmAttributeMask#attributeID#")> 
							<cfset currentActionValue = getAttributeDetails.currentAttributeMask>
							<cfset defaultActionValue = getAttributeDetails.defaultAttributeMask>
							<cfset attributeTransformationValue = getAttributeDetails.attributeTransformationMask>
						</cfif>
					</cfcase>
					<cfcase value="2">
						<cfif isDefined("frmAttributeSequence#attributeID#")>
							<cfset newActionValue = evaluate("frmAttributeSequence#attributeID#")> 
							<cfset currentActionValue = getAttributeDetails.currentAttributeSequence>
							<cfset defaultActionValue = getAttributeDetails.defaultAttributeSequence>
							<cfset attributeTransformationValue = getAttributeDetails.attributeTransformationSequence>
						</cfif>
					</cfcase>
				</cfswitch>
				
				<!--- has a transformation occurred --->
				<cfif currentActionValue neq newActionValue>
					<!--- if the new value is the default, delete the user defined mask --->
					<cfif defaultActionValue eq newActionValue>
						<cfquery name="deleteAttributeTransformation" datasource="#application.siteDataSource#">
							delete from pcmProductMaskTransformation where 
								AttributeID =  <cf_queryparam value="#attributeID#" CFSQLTYPE="CF_SQL_INTEGER" > 	and 
								transformationActionID =  <cf_queryparam value="#actionID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
								maskID =  <cf_queryparam value="#getAttributeDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
								sectionID =  <cf_queryparam value="#getAttributeDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
					<cfelse>
						<!--- if it's a user defined, and one doesn't exist for this attribute, insert the new one --->
						<cfif attributeTransformationValue eq "">
							<cfquery name="insertAttributeTransformation" datasource="#application.siteDataSource#">
								insert into pcmProductMaskTransformation 
									(maskID,sectionID,AttributeID,transformationActionID,transformationValue)
								values
									(<cf_queryparam value="#getAttributeDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getAttributeDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#attributeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#actionID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#newActionValue#" CFSQLTYPE="CF_SQL_Integer" >)
							</cfquery>
							
						<!--- otherwise update the existing one to contain the new value --->
						<cfelse>
							<cfquery name="updateAttributeTransformation" datasource="#application.siteDataSource#">
								update pcmProductMaskTransformation set transformationValue =  <cf_queryparam value="#newActionValue#" CFSQLTYPE="CF_SQL_Integer" > 
									where transformationActionID =  <cf_queryparam value="#actionID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
										AttributeID =  <cf_queryparam value="#attributeID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
										maskID =  <cf_queryparam value="#getAttributeDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
										sectionID =  <cf_queryparam value="#getAttributeDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</cfquery>
						</cfif>
					</cfif>
				</cfif>
			
			</cfloop>
		
		</cfloop>
	</cfif>
	
	
	
	<cfif isDefined("frmAttributeGroupIDList") and frmAttributeGroupIDList neq "">
	
		<!--- updating the Attribute Groups--->
		<cfloop list="#frmAttributeGroupIDList#" index="attributeGroupID">
		
			<cfquery name="getAttributeGroupDetails" dbtype="query">
				select * from productSections where sectionAttributeGroupID = #attributeGroupID#
			</cfquery>
			<!--- <cfdump var="#getAttributeGroupDetails#"><CF_ABORT> --->
			<cfloop list="1,2" index="actionID">	<!--- 1=mask, 2=sequence --->
			
				<cfswitch expression="#actionID#">
					<cfcase value="1">
						<cfset newActionValue = evaluate("frmAttributeGroupMask#attributeGroupID#")> 
						<cfset currentActionValue = getAttributeGroupDetails.currentAttributeGroupMask>
						<cfset defaultActionValue = getAttributeGroupDetails.defaultAttributeGroupMask>
						<cfset attributeGroupTransformationValue = getAttributeGroupDetails.attributeGroupTransformationMask>
					</cfcase>
					<cfcase value="2">
						<cfset newActionValue = evaluate("frmAttributeGroupSequence#attributeGroupID#")> 
						<cfset currentActionValue = getAttributeGroupDetails.currentAttributeGroupSequence>
						<cfset defaultActionValue = getAttributeGroupDetails.defaultAttributeGroupSequence>
						<cfset attributeGroupTransformationValue = getAttributeGroupDetails.attributeGroupTransformationSequence>
					</cfcase>
				</cfswitch>
				
				<!--- has a transformation occurred --->
				<cfif currentActionValue neq newActionValue>
					<!--- if the new value is the default, delete the user defined mask --->
					
					<cfif defaultActionValue eq newActionValue>
						<cfquery name="deleteAttributeGroupTransformation" datasource="#application.siteDataSource#">
							delete from pcmProductMaskTransformation where sectionAttributeGroupID =  <cf_queryparam value="#attributeGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								and transformationActionID =  <cf_queryparam value="#actionID#" CFSQLTYPE="CF_SQL_INTEGER" >  and maskID =  <cf_queryparam value="#getAttributeGroupDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
					<cfelse>
						<!--- if it's a user defined mask, and one doesn't exist for this attribute, insert the new one --->
						<cfif attributeGroupTransformationValue eq "">
							<cfquery name="insertAttributeGroupTransformation" datasource="#application.siteDataSource#">
								insert into pcmProductMaskTransformation 
									(maskID,<!--- sectionID, --->sectionAttributeGroupID,transformationActionID,transformationValue)
								values
									(<cf_queryparam value="#getAttributeGroupDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" ><!--- ,#getAttributeGroupDetails.sectionID# --->,<cf_queryparam value="#attributeGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#actionID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#newActionValue#" CFSQLTYPE="CF_SQL_Integer" >)
							</cfquery>
							
						<!--- otherwise update the existing one to contain the new value --->
						<cfelse>
							<cfquery name="updateAttributeGroupTransformation" datasource="#application.siteDataSource#">
								update pcmProductMaskTransformation set transformationValue =  <cf_queryparam value="#newActionValue#" CFSQLTYPE="CF_SQL_Integer" > 
									where transformationActionID =  <cf_queryparam value="#actionID#" CFSQLTYPE="CF_SQL_INTEGER" >  and sectionAttributeGroupID =  <cf_queryparam value="#attributeGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
									and maskID =  <cf_queryparam value="#getAttributeGroupDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</cfquery>
						</cfif>
					</cfif>
				</cfif>
			
			</cfloop>
		
		</cfloop>
	</cfif>
	
	
	<!--- updating the marketing statements --->
	<cfif isDefined("marketingStatementAttributeIDList") and marketingStatementAttributeIDList neq "">
	<Cfoutput>
		<cfloop list="#marketingStatementAttributeIDList#" index="marketingStatementAttributeID">
		
			<cfquery name="getMarketingStatementAttributeDetails" dbtype="query">
				select * from productSections where remoteAttributeID = '#marketingStatementAttributeID#'
				and sectionTypeID = 1
			</cfquery>
			
			<cfset currentActionValue = getMarketingStatementAttributeDetails.currentAttributeMask>
			<cfset defaultActionValue = getMarketingStatementAttributeDetails.defaultAttributeMask>
			<cfset attributeGroupTransformationValue = getMarketingStatementAttributeDetails.attributeTransformationMask>
			
			<!--- default to hiding the marketing Statement --->
			<cfset newActionValue = 1>
		
			<!--- if this is the statement we need to show then show it --->
			<cfif marketingStatementAttributeID eq evaluate("frmMarketingStatementsAttributeMask")>
				<cfset newActionValue = 0> 
			</cfif>

			<!--- if the currentMask is different than what it should be... --->	
			<cfif currentActionValue neq newActionValue>
				<!--- if the new value is the default, delete the user defined mask --->
				<cfif defaultActionValue eq newActionValue>
					<cfquery name="deleteMarktetingStatementAttributeTransformation" datasource="#application.siteDataSource#">
						delete from pcmProductMaskTransformation where 
							AttributeID =  <cf_queryparam value="#marketingStatementAttributeID#" CFSQLTYPE="CF_SQL_INTEGER" > 	and 
							transformationActionID = 1 and 
							maskID =  <cf_queryparam value="#getMarketingStatementAttributeDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
							sectionID =  <cf_queryparam value="#getMarketingStatementAttributeDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>
				<cfelse>
					<!--- if it's a user defined mask, and one doesn't exist for this attribute, insert the new one --->
					<cfif attributeGroupTransformationValue eq "">
						<cfquery name="insertMarktetingStatementAttributeTransformation" datasource="#application.siteDataSource#">
							insert into pcmProductMaskTransformation 
								(maskID,sectionID,attributeID,transformationActionID,transformationValue)
							values
								(<cf_queryparam value="#getMarketingStatementAttributeDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getMarketingStatementAttributeDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#marketingStatementAttributeID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,<cf_queryparam value="#newActionValue#" CFSQLTYPE="CF_SQL_Integer" >)
						</cfquery>
						
					<!--- otherwise update the existing one to contain the new value --->
					<cfelse>
						<cfquery name="updateMarktetingStatementAttributeTransformation" datasource="#application.siteDataSource#">
							update pcmProductMaskTransformation set transformationValue =  <cf_queryparam value="#newActionValue#" CFSQLTYPE="CF_SQL_Integer" > 
								where transformationActionID = 1 and 
								attributeID =  <cf_queryparam value="#marketingStatementAttributeID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
								maskID =  <cf_queryparam value="#getMarketingStatementAttributeDetails.maskID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
								sectionID =  <cf_queryparam value="#getMarketingStatementAttributeDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		</CFOUTPUT>
	</cfif>
	
	
	
	<!--- if section details found --->
	<cfif qry_get_sectionDetails.RecordCount neq 0>
		<!--- if no mask in place then create it --->
		<cfif qry_chk_pcmProductMaskTransformation.recordCount is 0 and form.frmsectionMasked neq qry_get_sectionDetails.masked>
			<cfquery name="insertSectionTransformation" datasource="#application.siteDataSource#">
				insert into pcmProductMaskTransformation 
				(maskID,sectionID,transformationActionID,transformationValue)
				values
				(<cf_queryparam value="#pcmProductMaskID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#qry_get_sectionDetails.sectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,<cf_queryparam value="#frmsectionMasked#" CFSQLTYPE="CF_SQL_Integer" >)
			</cfquery>
		<!--- if mask exists --->
		<cfelseif (qry_chk_pcmProductMaskTransformation.transformationvalue neq form.frmsectionMasked) and qry_chk_pcmProductMaskTransformation.RecordCount gt 0>
			<cfquery name="deleteSectionTransformation" datasource="#application.siteDataSource#">
				delete from pcmProductMaskTransformation 
				where maskTransformationID =  <cf_queryparam value="#qry_chk_pcmProductMaskTransformation.maskTransformationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			<cfset frmsectionMasked = 0>
		</cfif>

	</cfif>
	
</cfif>




<!--- <script>
	function displaySectionMask(sectionID) {
		sectionsForm.sectionID.value = sectionID;
		sectionsForm.submit();
	}
</script> --->

<cf_querySim>
	enableMask
	display, value
	phr_Enable|0
	phr_Disable|1
</cf_querySim>

<cfif listFind(request.showAttributeGroupMaskForSectionTypeIDList,variables.cbomasktypes) or listFind(request.showAttributeMaskForSectionTypeIDList,variables.cbomasktypes)>

 	<cfset productSections = application.com.relayProductTree.getProductSectionDetails(RemoteCategoryID=#cboRemoteCategoryID#,SiteDefID=#maskSiteDefID#,SourceID=#cboSourceID#)>
	
	<cfquery name="getSectionNames" dbtype="query">
		select distinct sectionID, RemoteCategoryID, sectionTypeName, sectiontypeID from productSections
		where remotecategoryID='#variables.cboRemoteCategoryID#' and sectionTypeID=#variables.cboMaskTypes#
	</cfquery>
	
	<cfif not isDefined("sectionID") or (isDefined("sectionID") and sectionID eq 0)>
		<cfif getSectionNames.recordCount gt 0>
			<cfset sectionID = getSectionNames.sectionID[1]>
		</cfif>
	</cfif>
	
	<cfquery name="getSectionDetailsForCurrentSection" dbtype="query">
		select distinct sectionTypeID from productSections where sectionID=#sectionID#
	</cfquery>
	
	<cfquery name="getSectionAttributeDetails" dbtype="query">
		select remoteAttributeID, sectionAttributeName, defaultAttributeSequence, defaultAttributeMask, 
			currentAttributeSequence, currentAttributeMask, sectionAttributeGroupID, sectionAttributeGroupName, defaultAttributeGroupMask,
			defaultAttributeGroupSequence, currentAttributeGroupMask, currentAttributeGroupSequence
		from productSections where sectionID = #sectionID#
		order by sectionAttributeGroupName
	</cfquery>
	
	<cfif variables.cbomasktypes eq 1>
		<cfquery name="getCurrentVisibleMarketingStatment" dbtype="query">
			select remoteAttributeID from getSectionAttributeDetails where remoteAttributeID <> '0' and currentAttributeMask = 0
		</cfquery>
		<cfif getCurrentVisibleMarketingStatment.recordCount gt 0>
			<cfset currentVisibleMarketingStatementID = getCurrentVisibleMarketingStatment.remoteAttributeID[1]>
		<cfelse>
			<cfset currentVisibleMarketingStatementID = -1>
		</cfif>
	</cfif>

</cfif>


<cfset request.relayFormDisplayStyle="HTML-table">
<!--- <cfoutput>

	<cf_relayFormDisplay>
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="" colspan="yes">
			<cfform name="sectionsForm" method="post" >
				<cf_relayFormElement relayFormElementType="hidden" currentValue="#categoryID#" fieldName="categoryID">
				<cf_relayFormElement relayFormElementType="hidden" currentValue="#sectionID#" fieldName="sectionID">
				<cfloop query="getSectionNames">
					<td><a href="##" onClick="displaySectionMask(#sectionID#)">phr_#sectionTypeName#</a></td>
				</cfloop>
			</cfform>
		</cf_relayFormElementDisplay>
	</cf_relayFormDisplay>
</cfoutput> --->
	
	<cf_relayFormDisplay class="maskManagementTable">
		<cfform name="sectionAttributesForm" method="post" >
		
			<!--- if it's overview, tech specs or supplies show the top save button--->
			<cfif listFind(request.showAttributeGroupMaskForSectionTypeIDList,variables.cbomasktypes) or listFind(request.showAttributeMaskForSectionTypeIDList,variables.cbomasktypes)>
				<tr>
					<td colspan="5" align="right"><cf_relayFormElement relayFormElementType="submit" fieldname="frmUpdateAttributes" currentValue="phr_Save"></td>
				</tr>
			</cfif>
			<tr>
				<td colspan="3">
					phr_product_mask_section: phr_Enabled <input type="radio" name="frmsectionMasked" value="0" <cfif frmsectionMasked is "0">checked</cfif>> phr_Disabled <input type="radio" name="frmsectionMasked" value="1" <cfif frmsectionMasked is "1">checked</cfif>>
				</td>
			</tr>
			
			<cf_relayFormElement relayFormElementType="hidden" currentValue="#variables.cboremotecategoryID#" fieldName="remotecategoryID">
			<cf_relayFormElement relayFormElementType="hidden" currentValue="#sectionID#" fieldName="sectionID">
			
			<!--- <cfif listFind("1,2,5",variables.cbomasktypes)> --->
			<cfif listFind(request.showAttributeGroupMaskForSectionTypeIDList,variables.cbomasktypes) or listFind(request.showAttributeMaskForSectionTypeIDList,variables.cbomasktypes)>
				<tr>
					<th>phr_name</th>
					<cfif variables.cbomasktypes neq 1>  <!--- don't show sequence for the overview section --->
						<th>phr_defaultOrder</th>
					</cfif>
					<th>phr_defaultMask</th>
					<cfif variables.cbomasktypes neq 1>
						<th>phr_currentOrder</th>
					</cfif>
					<th>phr_currentMask</th>
				</tr>
				
				<cfset attributeGroupIDList = "">
				<cfset attributeIDList = "">
				<cfset marketingStatementAttributeIDList = "">
				
				<!--- show both the attributeGroup and Attribute Masks --->
				<cfif listFind(request.showAttributeGroupMaskForSectionTypeIDList,variables.cbomasktypes) and listFind(request.showAttributeMaskForSectionTypeIDList,variables.cbomasktypes)>

					<cfoutput query="getSectionAttributeDetails" group="sectionAttributeGroupName">
						<tr class="evenRow">
							<td>
								<b><cf_divOpenClose 
									divid="#replace(sectionAttributeGroupName,' ','','ALL')#"
									startOpen=false 
									rememberSettings = true
									openText = "#UCase(sectionAttributeGroupName)#"
									closeText = "#UCase(sectionAttributeGroupName)#"
									openImage = "/styles/cachedBGImages/level1_arrow_closed.gif"
									closeImage = "/styles/cachedBGImages/level1_arrow_open.gif"
								>
								<cf_divOpenClose_Image><cf_divOpenClose_Text></cf_divOpenClose_Text></cf_divOpenClose ></b>
							</td>
							<td>#htmleditformat(defaultAttributeGroupSequence)#</td>
							<td>#IIF(defaultAttributeGroupMask,DE("phr_Disabled"),DE("phr_Enabled"))#</td>
							<td><cf_relayFormElement relayFormElementType="text" fieldname="frmAttributeGroupSequence#sectionAttributeGroupID#" label="" currentValue="#currentAttributeGroupSequence#"></td>
							<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmAttributeGroupMask#sectionAttributeGroupID#" currentValue="#currentAttributeGroupMask#" query="#enableMask#" display="display" value="value"></td>
						</tr>
						<cfset attributeGroupIDList = listAppend(attributeGroupIDList,#sectionAttributeGroupID#)>
						<cfoutput>
							<cfif listFind(attributeIDList,remoteAttributeID) eq 0>
								<tr id="#replace(sectionAttributeGroupName,' ','','ALL')#" style="#divStyle#">
									<td class="attributeName">#htmleditformat(sectionAttributeName)#</td>
									<td >#htmleditformat(defaultAttributeSequence)#</td>
									<td >#IIF(defaultAttributeMask,DE("phr_Disabled"),DE("phr_Enabled"))#</td>
									<td ><cf_relayFormElement relayFormElementType="text" fieldname="frmAttributeSequence#remoteAttributeID#" label="" currentValue="#currentAttributeSequence#"></td>
									<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmAttributeMask#remoteAttributeID#" currentValue="#currentAttributeMask#" query="#enableMask#" display="display" value="value"></td>
								</tr>
								<cfset attributeIDList=listAppend(attributeIDList,remoteAttributeID)>
								<tr id="#replace(sectionAttributeGroupName,' ','','ALL')#" style="#divStyle#">
									<td class="line" colspan="5">&nbsp;</td>
								</tr>
							</cfif>
						</cfoutput>
					</cfoutput>
					
				<!--- show just the Attribute Group Masks --->
				<cfelseif listFind(request.showAttributeGroupMaskForSectionTypeIDList,variables.cbomasktypes)>
					
					<cfoutput query="getSectionAttributeDetails" group="sectionAttributeGroupName">
						<tr class="evenRow">
							<td>#htmleditformat(sectionAttributeGroupName)#</td>
							<td>#htmleditformat(defaultAttributeGroupSequence)#</td>
							<td>#IIF(defaultAttributeGroupMask,DE("phr_Disabled"),DE("phr_Enabled"))#</td>
							<td><cf_relayFormElement relayFormElementType="text" fieldname="frmAttributeGroupSequence#sectionAttributeGroupID#" label="" currentValue="#currentAttributeGroupSequence#"></td>
							<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmAttributeGroupMask#sectionAttributeGroupID#" currentValue="#currentAttributeGroupMask#" query="#enableMask#" display="display" value="value"></td>
						</tr>
						<cfset attributeGroupIDList = listAppend(attributeGroupIDList,#sectionAttributeGroupID#)>
					</cfoutput>
					
				<!--- show just the Attribute Masks --->
				<cfelseif listFind(request.showAttributeMaskForSectionTypeIDList,variables.cbomasktypes)>
					<cfoutput>
						<cfloop query="getSectionAttributeDetails">
							<cfif not listContains(attributeIDList,remoteAttributeID)>
								<cfif variables.cbomasktypes eq 1 and sectionAttributeName eq "Bullets">
									<cf_querySim>
										disableOverviewMasks
										display, value
										phr_Enable | -1
									</cf_querySim>
									<tr>
										<td align="center">None</td>
										<td align="center">phr_Enabled</td>
										<td align="center"><cf_relayFormElement relayFormElementType="radio" fieldname="frmMarketingStatementsAttributeMask" currentValue="#currentVisibleMarketingStatementID#" query="#disableOverviewMasks#" display="display" value="value"></td>
									</tr>
									<tr><td class="line" colspan="5">&nbsp;</td></tr>
									<tr><td class="line" colspan="5">&nbsp;</td></tr>
								</cfif>
								<tr>
									<td align="center">#htmleditformat(sectionAttributeName)#</td>
									<cfif variables.cbomasktypes neq 1>  <!--- don't show sequence for the overview section --->
										<td align="center">#htmleditformat(defaultAttributeSequence)#</td>
									</cfif>
									
									<td align="center">#IIF(defaultAttributeMask,DE("phr_Disabled"),DE("phr_Enabled"))#</td>
									
									<cfif variables.cbomasktypes neq 1>  <!--- don't show sequence for the overview section --->
										<td align="center"><cf_relayFormElement relayFormElementType="text" fieldname="frmAttributeSequence#remoteAttributeID#" label="" currentValue="#currentAttributeSequence#"></td>
									</cfif>
									
									<cfif variables.cbomasktypes neq 1>
										<td align="center"><cf_relayFormElement relayFormElementType="radio" fieldname="frmAttributeMask#remoteAttributeID#" currentValue="#currentAttributeMask#" query="#enableMask#" display="display" value="value"></td>
										<cfset attributeIDList = listAppend(attributeIDList,#remoteAttributeID#)>
									<cfelse>
									
										<cfif sectionAttributeName eq "Bullets">
											<td align="center"><cf_relayFormElement relayFormElementType="radio" fieldname="frmAttributeMask#remoteAttributeID#" currentValue="#currentAttributeMask#" query="#enableMask#" display="display" value="value"></td>
											<cfset attributeIDList = listAppend(attributeIDList,#remoteAttributeID#)>
											
										<cfelse>
										
											<cf_querySim>
												enableOverviewMask
												display, value
												phr_Enable|#remoteAttributeID#
											</cf_querySim>
											
											<td align="center"><cf_relayFormElement relayFormElementType="radio" fieldname="frmMarketingStatementsAttributeMask" currentValue="#currentVisibleMarketingStatementID#" query="#enableOverviewMask#" display="display" value="value"></td>
											<cfset marketingStatementAttributeIDList = listAppend(marketingStatementAttributeIDList,#remoteAttributeID#)>
										</cfif>
									</cfif>
								</tr>
								<tr><td class="line" colspan="5">&nbsp;</td></tr>
								
							</cfif>
	
						</cfloop>
					</cfoutput>
					
				</cfif>
	
				
				<cf_relayFormElement relayFormElementType="hidden" fieldname="frmAttributeGroupIDList" currentValue="#attributeGroupIDList#">
				<cf_relayFormElement relayFormElementType="hidden" fieldname="frmAttributeIDList" currentValue="#attributeIDList#">
				<cf_relayFormElement relayFormElementType="hidden" fieldname="marketingStatementAttributeIDList" currentValue="#marketingStatementAttributeIDList#">
				
			</cfif>
			
			<cf_relayFormElement relayFormElementType="hidden" fieldname="cboSiteDefID" currentValue="#variables.cboSiteDefID#">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="cboSourceID" currentValue="#variables.cboSourceID#">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="cboRemoteCategoryID" currentValue="#variables.cboRemoteCategoryID#">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="cbomasktypes" currentValue="#variables.cbomasktypes#">
			
			<tr>
				<td colspan="5" align="right"><cf_relayFormElement relayFormElementType="submit" fieldname="frmUpdateAttributes" currentValue="phr_Save"></td>
			</tr>
		</cfform>
	</cf_relayFormDisplay>

</cf_translate>



