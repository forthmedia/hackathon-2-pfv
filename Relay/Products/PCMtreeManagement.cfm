<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		PCMtreeManagement.cfm	
Author:			  
Date started:	
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008/12/22			NJH			CR-LEX579 - added permittedLocales to the report
WAB 2010/10/12 Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
				original code was getting second in list, but this must have been Sony Specific
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
Possible enhancements:


 --->

<cfif cgi.server_name eq "www2.channelmaster.net">
	<!--- <cfinclude template="/content/sony1/treeManagementV2.cfm"> --->
	<cflocation url="http://www2.channelmaster.net/content/sony1/treeManagementV2.cfm"addToken="false">
<cfelse>
	
<cfsetting requesttimeout="5000" enablecfoutputonly="No">

<cfoutput><script src="/javascript/openWin.js" type="text/javascript"></script></cfoutput>

<!--- Export the Rawdata. --->
<!--- <cfinclude template="..\CFTemplates\relayINI.cfm"> --->
<cfinclude template="PCMdisplayNavigationSetupHierarchies.cfm">

<!--- New Tree Creation --->
<cfif isdefined("form.title") and len(trim(form.title))>
	<cfscript>
		selectTree = application.com.relayProductTree.selectTree(
			title = "#trim(form.title)#",
			sourcegroupid ="2",
			languageCode = form.LanguageCode,
			countryCode = form.CountryCode);
	</cfscript>
	<cfif selectTree.recordcount eq 0>
			<cfscript>
				treeID = application.com.relayProductTree.insertTree(
					title="#trim(form.title)#",
					sourceid =form.sourceid,
					languageCode = form.LanguageCode,
					countryCode = form.CountryCode
					);
			</cfscript>
	<cfelse>
		Tree name / country / language combination already exists <a href="javascript:history.go(-1);">back</a>
		<CF_ABORT>
	</cfif>	
	
	<!--- 2005/11/21 - GCC - CR_SNY564 populate from source tree --->
	<cfif isdefined("form.sourceTreeID") and form.sourceTreeID gt 0>
		<cfscript>
			success = application.com.relayProductTree.copyTreeTransformationActions(sourceTreeID = #form.sourceTreeID#,targettreeID = treeID);
		</cfscript>
	</cfif>
</cfif>
<!--- Tree Delete --->  
<cfif isdefined("url.Delete") and isdefined("url.treeID") and len(url.treeID)>
	<cfscript>
		application.com.relayProductTree.deleteTree(treeID = "#url.treeID#");
	</cfscript>
</cfif>

<cfif isdefined("url.buildNavigationFromDatabase") and isdefined("url.treeID") and len(url.treeID)>
	
	<cfscript>
		application.com.relayProductTree.buildNavigationTreeFromDB(
			treeID = "#url.treeID#");
	</cfscript>

</cfif>
<cfif isdefined("url.buildNavigationFromTransformation") and isdefined("url.treeID") and len(url.treeID)>
	
	<cfscript>
		application.com.relayProductTree.generateNavigationTreeFromTransformationActions(
			treeID = "#url.treeID#");
	</cfscript>

</cfif>
<!--- Deactivate/Activate function. --->
<cfif isdefined("url.activate")>
	<cfscript>
		application.com.relayProductTree.setTreeActiveStatus(
			treeID = "#url.treeID#",
			ActiveStatus = url.activestatus);
			
	</cfscript>
</cfif>
<!--- This drops the current set of memory navigations. --->
<cfif isdefined("url.refreshAllActiveNavigation")>

	<cfset setmp = structDelete(application,"NAVSTRUCT")>
	<cfinclude template="PCMdisplayNavigationSetupHierarchies.cfm">


</cfif>
<cfif isdefined("application.NAVSTRUCT")>
	<cfset memoryNavigationList = structKeylist(application.NAVSTRUCT.NAVIGATION)>
<cfelse>
	<cfset memoryNavigationList = "">
</cfif>


<!--- Display set of available trees and display the 'manual' set. --->
<table width="80%" cellpadding="5" cellspacing="0">
	<!--- Form to create new tree --->
	
<tr valign="top">
<td colspan="2" align="center" nowrap>
		<cfscript>
			getUserTrees = application.com.relayProductTree.selectTree (sourcegroupid='2');
			getUserSources = application.com.relayProductTree.selectsource (sourcegroupid='2');
		</cfscript>
		<cfif getUserSources.recordcount neq 0>
		<form action="PCMtreeManagement.cfm" method="post">Create a new tree: <input type="Text" name="title" size="20"maxlength="20"> LC*:<input type="Text" name="LanguageCode" size="3"maxlength="3" value="EN"> CC:<input type="Text" name="CountryCode"  size="3" maxlength="3" value="N/A"> Base on: <select name="sourceTreeID"><option value="0">None - create empty<cfoutput query="getUserTrees"><option value="#getUserTrees.treeID#">#htmleditformat(getUserTrees.title)#</cfoutput></select>&nbsp;<select name="sourceID"><cfoutput query="getUserSources"><option value="#sourceID#">#htmleditformat(sourcedescription)#</cfoutput></select>&nbsp;<input type="Submit" value="Add Tree"></form>	
		<cfelse>
			Not User Sources defined
		</cfif>
	</td></tr>
	<cfparam name="activeStatusToShow" default="1">
<tr><td colspan="2">
<cfif activeStatusToShow eq 1>
<a href="PCMtreeManagement.cfm?activeStatusToShow=0">Click here</a> to show inactive DATA trees
<cfelse>
<a href="PCMtreeManagement.cfm?activeStatusToShow=1">Click here</a> to show active DATA trees
</cfif>
</td></tr>
	<tr valign="top">
	<td width="50%"><strong>Available DATA trees:</strong><br>
		<cfscript>
			treeQry = application.com.relayProductTree.getTreeSet ( );
		</cfscript>
			<table cellpadding="5" cellspacing="0"><tr><th nowrap>Name</th><th>Permitted Load Locales</th><!--- <th nowrap>Lang/Country</th> ---><th nowrap>Last Updated</th><th nowrap>Actions</th></tr>
		
		<cfoutput query="treeQry">
		<cfif treeQry.sourcegroupid neq "2" and active eq activeStatusToShow>
			<tr>
				<!--- NJH 2008/12/22 CR-LEX579 - added permittedLocales to show on the report --->
			<td nowrap>#htmleditformat(title)#</td>
				<td>#htmleditformat(permittedLoadLocales)#</td>
			<!--- <td nowrap>[LA:#languageCode#][CO:#countryCode#]</td> --->
			<td nowrap>#dateformat(lastupdated,"d-mmm-yyyy")#</td>
			<td nowrap><a href="PCMtreeManagement.cfm?Delete=true&TreeID=#TreeID#" title="Delete the tree." >Del</a>
			<a href="javascript:openWin('/products/PCMtreeProductExport.cfm?treeID=#treeID#','TreeProductExport','width=750,height=550,toolbar=0,location=0,directories=0,status=1,menuBar=1,scrollBars=1,resizable=1')">Export</a>
			<cfif active><a href="PCMtreeManagement.cfm?activate=true&TreeID=#TreeID#&activestatus=0" title="De-Activate this tree. - Disables the tree.">De-Act</a>
			<cfelse><a href="PCMtreeManagement.cfm?activate=true&TreeID=#TreeID#&activestatus=1" title="Activate this tree. - Enables the tree.">Act</a>
			</cfif>
			
			<cfset navid  = replace(title," ","_","ALL")>
			<cfif listfindnocase(memoryNavigationList,navid)>
			<a href="/?etid=ProductCategories&navid=#navid#" target="_blank" title="Tree in memory and previewable.">Prev</a>
			</cfif>
			</td></tr>
		</cfif>
		</cfoutput>
		</table>
</td>
<td width="50%"><strong>Available USER trees:</strong><br>
	<cfscript>
		treeQry = application.com.relayProductTree.getTreeSet(
			sourcegroupid="2" );
	</cfscript>
	<table cellpadding="5" cellspacing="0">
	<tr><th >Name</th><th >Language/Country</th><th >Actions</th></tr>
	<cfoutput query="treeQry">
	<tr><td nowrap>#htmleditformat(title)#</td>
	<td nowrap>[LA:#htmleditformat(languageCode)#][CO:#htmleditformat(countryCode)#]</td>
	<td nowrap>
	<a href="PCMtreeEdit.cfm?WOTreeID=#TreeID#" title="Edit the tree.">Edit</a> 
	<a href="PCMtreeManagement.cfm?buildNavigationFromDatabase=true&TreeID=#TreeID#" title="Build the navigation tree from the database, refreshing the memory resident structure.">DB2MEM</a> 
	<a href="PCMtreeManagement.cfm?buildNavigationFromTransformation=true&TreeID=#TreeID#" title="Drops all DB information and runs the set of actions used to build the tree.">ACT2DB</a> 
	<a href="PCMtreeManagement.cfm?Delete=true&TreeID=#TreeID#" onclick="return confirm('This will remove all transformation information?');">Del</a>
	<a href="javascript:openWin('/products/PCMtreeProductExport.cfm?treeID=#treeID#','TreeProductExport','width=750,height=550,toolbar=0,location=0,directories=0,status=1,menuBar=1,scrollBars=1,resizable=1')">Export</a>
	<cfif active>
	<a href="PCMtreeManagement.cfm?activate=true&TreeID=#TreeID#&activestatus=0">De-Act</a>
	<cfelse>
	<a href="PCMtreeManagement.cfm?activate=true&TreeID=#TreeID#&activestatus=1" title="Activates tree.">Act</a>
	</cfif>
	<cfset navid  = replace(title," ","_","ALL")>
	<cfif listfindnocase(memoryNavigationList,navid)>
	<a href="/?etid=ProductCategoriesV2&navid=#navid#" target="_blank" title="Tree in memory and previewable.">Prev</a>
	</cfif>
	</cfoutput></td></tr>
	</table>
</td>
</tr>
</table>
</cfif>

