<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="500" enablecfoutputonly="No">
<!--- 
File name:		treeCategoryImagePickerV2.cfm	
Author:			GCC
Date started:	2005-07-20
	
Description:		This provides a listing of all product images available at this point
or below in the user tree. One can be selected and the currently selected image displays with a different BG colour.

Usage: Created for CR_SNY535 - GCC - category image chooser	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

20-JAN-2006			NM			CR_SNY568 Ability to create an uploade dimage library and pick a category image form that per tree.

Enhancements still to do:


 --->
 <cfset productCategoryImagesDirectory = request.productCategoryImagesDirectory>
 <cfset productCategoryImagesDirectoryURL = request.productCategoryImagesDirectoryURL>
 <cfset productImagesDirectory = request.productImagesDirectory>
 <cfset productImagesDirectoryURL = request.productImagesDirectoryURL>

 <cfparam name="thumbsize" default="60">
<cfif structkeyExists(request,"productThumbsize")>
	<cfset thumbsize = request.productThumbsize>
</cfif>


 


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Category Image Selector</TD>
	</TR>
</TABLE> 

<!--- clear image for either upload or product library--->
<cfif isdefined('form.productClear') or isDefined('form.uploadClear')>

	<cfif fileExists("#productCategoryImagesDirectory#\categoryImage_#url.WOtreeID#_#nodeID#.png")>
		<cfscript>
			application.com.relayProductTree.deleteCategoryImage(
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#");
		</cfscript>
	<cfelse>
		<cfscript>
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "clearCategoryImage",
				treeId = "#url.WOtreeID#",
				CategoryID = "#nodeID#");
		
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "clearCategoryImage",
				treeId = "#url.WOtreeID#",
				CategoryID = "#nodeID#");
		</cfscript>
	</cfif>

<!--- assign uploaded image to category --->
<cfelseif isDefined("form.imageFileName") and form.imageFileName neq "">

	<cfscript>
		application.com.relayProductTree.insertTransformationAction(
			actionToPerform = "clearCategoryImage",
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#");
	
		// Now run the transformation.
		application.com.relayProductTree.runTransformationAction(
			actionToPerform = "clearCategoryImage",
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#");
	</cfscript>
	<!--- upload a file for the category --->
	<cfscript>
		application.com.relayProductTree.assignUploadCategoryImage(
		imageFile = form.imageFileName,
		treeId = "#url.WOtreeID#",
		CategoryID = "#nodeID#");
	</cfscript>
<cfelseif isDefined("form.productID")>

	<cfif fileExists("#productCategoryImagesDirectory#\categoryImage_#url.WOtreeID#_#nodeID#.png")>
		<cfscript>
			application.com.relayProductTree.deleteCategoryImage(
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#");
		</cfscript>
	</cfif>
	<!--- now update the image --->
	<cfscript>
		application.com.relayProductTree.insertTransformationAction(
			actionToPerform = "editCategoryImage",
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#",
			productID = "#form.productID#");
	
		// Now run the transformation.
		application.com.relayProductTree.runTransformationAction(
			actionToPerform = "editCategoryImage",
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#",
			productID = "#form.productID#");
	</cfscript>

<!--- upload image to tree --->
<cfelseif isdefined("form.imageFile") and form.imageFile neq "">
	<!--- clear any product association --->
	<cfscript>
		application.com.relayProductTree.insertTransformationAction(
			actionToPerform = "clearCategoryImage",
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#");
	
		// Now run the transformation.
		application.com.relayProductTree.runTransformationAction(
			actionToPerform = "clearCategoryImage",
			treeId = "#url.WOtreeID#",
			CategoryID = "#nodeID#");
	</cfscript>
	<!--- upload image to tree --->
	<cfscript>
		newFile = application.com.relayProductTree.uploadTreeImage(
		imageFile = "form.imageFile",
		treeId = "#url.WOtreeID#",
		thumbSize = #thumbSize#);
	</cfscript>
	<!--- assign uploaded file to the category --->
	<cfscript>
		application.com.relayProductTree.assignUploadCategoryImage(
		imageFile = newFile,
		treeId = "#url.WOtreeID#",
		CategoryID = "#nodeID#");
	</cfscript>
</cfif>

<!--- refresh caller ---><!--- removed for performance reasons --->
<!--- <cfif not isdefined('form.productImages') or not isdefined('form.uploadImages')or not isdefined('form.uploadClear')>
	<script>
		//Reload treeManagementWindow	
		window.opener.location.reload(true);
	</script>
</cfif> --->

<cfif not isDefined("form.isActioned")>
	<cfif fileExists("#productCategoryImagesDirectory#\categoryImage_#url.WOtreeID#_#nodeID#.png")>
		<cfset showLibrary = "upload">
	<cfelse>
		<cfset showLibrary = "product">
	</cfif>
</cfif>

<cfset noOfElementsPerRow = 4>
<!--- UPLOADED IMAGES LIBRARY --->

<!--- <cfoutput>
	(form.uploadImages eq "yes"[#isDefined("form.uploadImages") and form.uploadImages eq "yes"#]) OR (isDefined("form.imageFileName")[#isDefined("form.imageFileName")#]) OR (isDefined("form.uploadClear")[#isDefined("form.uploadClear")#]) OR (isDefined("form.imageFile")[#isDefined("form.imageFile") and form.imageFile neq ""#]) OR (isDefined("showLibrary") and showLibrary eq "upload"[#isDefined("showLibrary") and showLibrary eq "upload"#])
</cfoutput> --->

<cfif (isDefined("form.uploadImages") and form.uploadImages eq "yes") OR (isDefined("form.imageFileName")) OR (isDefined("form.uploadClear")) OR (isDefined("form.imageFile") and form.imageFile neq "") OR (isDefined("showLibrary") and showLibrary eq "upload")>

	<cfoutput>
		<cfscript>
		categoryImage = application.com.relayProductTree.getLocalCategoryImage(
			nodeID=#nodeID#,treeID=#url.WOtreeID#);
		getCategoryDetails = application.com.relayProductTree.getCategoryDetails(
			nodeID=#nodeID#,treeID=#url.WOtreeID#);
		treeDetail = application.com.relayProductTree.selectTree(
			treeID=#url.WOtreeID#);
		treeImages = application.com.relayProductTree.getTreeImages(
			treeID=#url.WOtreeID#,
			topDirectory = '#productCategoryImagesDirectory#');
		</cfscript>
	</cfoutput>
	<cfform  method="POST" name="categoryImageForm" id="categoryImageForm" enctype="multipart/form-data">
		<input type="hidden" name="isActioned" value="true">
		<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" class="withBorder">
			
			<tr>
				<td>
					<input type="radio" name="productImages" value="yes" onclick="form.submit()"> Click here to go to the product images library for category: <cfoutput>#htmleditformat(getCategoryDetails.categoryName)#</cfoutput><br>
					<br><br>
				</td>
			</tr>
			</tr>
		<cfif fileExists("#productCategoryImagesDirectory#\categoryImage_#url.WOtreeID#_#nodeID#.png")>
			<tr>
				<td>
					<table>
						<tr>
							<td>
								Current category image for category: <cfoutput>#htmleditformat(getCategoryDetails.categoryName)#</cfoutput>:
							</td>
							<td>
								<cfoutput><img src="#productCategoryImagesDirectoryURL#/categoryImage_#url.WOtreeID#_#nodeID#.png"></cfoutput>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</cfif>
		<cfif isQuery(treeImages) AND treeImages.recordcount gt 0>
			<tr>
				<td valign="top">
				<cfif categoryImage.productID neq "" OR fileExists("#productCategoryImagesDirectory#\categoryImage_#url.WOtreeID#_#nodeID#.png")>
					<input type="radio" name="uploadClear" value="yes" onclick="form.submit()"> Click here to revert to automatic category image allocation for category: <cfoutput>#htmleditformat(getCategoryDetails.categoryName)#</cfoutput>.
				<cfelse>
					Click a button below to select a fixed uploaded image for category: <cfoutput>#htmleditformat(getCategoryDetails.categoryName)#</cfoutput>.
				</cfif>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table border="0">
			<cfoutput query="treeImages">
				<cfif treeImages.currentrow eq 1>
						<tr>
				</cfif>
							<td>
								<table border="1" width="100%" height="80" cellpadding="0" cellspacing="0" class="withBorder">
									<tr>
										<td>
											<table width="100%" border="0">
												<tr>
													<td>
														<img src="#productCategoryImagesDirectoryURL#/#url.WOtreeID#/#treeImages.name#" alt="" border="0">
													</td>
													<td align="right">
														<CF_INPUT type="radio" name="imageFileName" value="#treeImages.name#" onclick="form.submit()">
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
				<cfif treeImages.currentrow mod noOfElementsPerRow eq 0>
						</tr>
						<tr>
				</cfif>
				<cfif treeImages.currentrow eq treeImages.recordcount>
					<cfset remainder = noOfElementsPerRow - (treeImages.currentrow mod noOfElementsPerRow)>
					<cfloop index="i" from="1" to="#remainder#">
							<td>&nbsp;</td>
					</cfloop>
						</tr>
				</cfif>
			</cfoutput>
					</table>
				</td>
			</tr>
		<cfelse>
			<tr>
				<td valign="top">
					No images available for tree <cfoutput>#htmleditformat(treeDetail.title)#</cfoutput>.
				</td>
			</tr>
		</cfif>
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td>
								Upload an image for tree <cfoutput>#htmleditformat(treeDetail.title)#</cfoutput>:
														<cfform  method="POST" name="categoryImageUploadForm" id="categoryImageUploadForm" enctype="multipart/form-data">
									<input type="file" name="imageFile">&nbsp;&nbsp;&nbsp;<input type="submit" value="upload file">
									<cfoutput><a href=## onClick="window.open('PCMdeleteTreeUploadImage.cfm?WOTreeID=#url.WOtreeID#&nodeID=#nodeID#','Delete','height=550,width=550,scrollbars=1,resizable=1,menubar=0,toolbar=0');">Delete Tree Image</a></cfoutput>
								</cfform>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</cfform>
<!--- PRODUCT IMAGES LIBRARY --->
<cfelseif (isDefined("form.productImages") and form.productImages eq "yes") OR (isDefined("form.productID")) OR (isDefined("form.productClear")) OR (isDefined("showLibrary") and showLibrary eq "product")>
	<cfoutput>
		<cfscript>
		childlist = application.com.relayProductTree.getAllCategoriesBelow(
			nodeID=#nodeID#,treeID=#url.WOtreeID#);
		nodeList = listappend(childlist,nodeID);
		productImages = application.com.relayProductTree.getProductImages(
			nodeList=#nodeList#,treeID=#url.WOtreeID#);
		categoryImage = application.com.relayProductTree.getLocalCategoryImage(
			nodeID=#nodeID#,treeID=#url.WOtreeID#);
		getCategoryDetails = application.com.relayProductTree.getCategoryDetails(
			nodeID=#nodeID#,treeID=#url.WOtreeID#);
		treeDetail = application.com.relayProductTree.selectTree(
			treeID=#url.WOtreeID#);
		</cfscript>
	</cfoutput>
	<cfform  method="POST" name="categoryImageForm" id="categoryImageForm">
		<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" class="withBorder">
			<tr>
				<td>
					<input type="radio" name="uploadImages" value="yes" onclick="form.submit()"> Click here to go to the uploaded images library for tree <cfoutput>#htmleditformat(treeDetail.title)#</cfoutput><br>
					<br><br>
				</td>
			</tr>
		<cfif productImages.recordcount gt 0>
			<tr>
				<td valign="top">
				<cfif categoryImage.productID neq "" OR fileExists("#productCategoryImagesDirectory#\categoryImage_#url.WOtreeID#_#nodeID#.png")>
					<input type="radio" name="productClear" value="yes" onclick="form.submit()"> Click here to revert to automatic category image allocation for category: <cfoutput>#htmleditformat(getCategoryDetails.categoryName)#</cfoutput>.
					<br><br>
				<cfelse>
					Click a button below to select an image for category: <cfoutput>#htmleditformat(getCategoryDetails.categoryName)#</cfoutput>.
				</cfif>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table border="0">
			<cfoutput query="productImages">
				<cfif productImages.currentrow eq 1>
						<tr>
				</cfif>
							<td>
								<table border="1" width="100%" height="80" cellpadding="0" cellspacing="0" class="withBorder">
									<tr>
										<td<cfif productImages.productID eq categoryImage.productID> class="Submenu"</cfif>>
											<table width="100%" border="0">
												<tr>
													<td>#htmleditformat(productImages.title)#</td>
													<td>
													<cfif not fileexists("#productImagesDirectory#\#productImages.productID#tn.png")>
														<cfif isDefined("request.remoteImagePath")>
															<cfset fullThumbnailURL = "#request.remoteImagePath#/#productImages.thumbNailUrl#">
														<cfelse>
															<cfset fullThumbnailURL = productImages.thumbNailUrl>
														</cfif>
														<cfhttp url="#fullThumbnailURL#" method="get" timeout="5"></cfhttp>
														<cfif cfhttp.statusCode eq "200 OK">						
															<cfset nImage = application.com.relayImage.getImageComponent()>
															<cfscript>
																nImage.readimageFromURL(#fullThumbnailURL#);
																//nImage.blur(nImage.getWidth()/350);
																if (nImage.getWidth() gt nImage.GetHeight()){nImage.scaleWidth(thumbsize);}
																else {nImage.scaleHeight(thumbsize);}							
																nImage.writeImage("#productImagesDirectory#\#productImages.productID#tn.png","png");
															</cfscript>
															<img src="#productImagesDirectoryURL#/#productImages.productID#tn.png" alt="#productImages.title#" border="0">
															<!--- <cfset box = "BV"> --->
														<cfelse>
															No Image
														</cfif>
													<cfelse>
														<img src="#productImagesDirectoryURL#/#productImages.productID#tn.png" alt="#productImages.title#" border="0">
													</cfif>		
													</td>
													<td align="right">
														<CF_INPUT type="radio" name="productID" value="#productImages.productID#" CHECKED="#iif( productImages.productID eq categoryImage.productID,true,false)#" onclick="form.submit()">
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
				<cfif productImages.currentrow mod noOfElementsPerRow eq 0>
						</tr>
						<tr>
				</cfif>
				<cfif productImages.currentrow eq productImages.recordcount>
					<cfset remainder = noOfElementsPerRow - (productImages.currentrow mod noOfElementsPerRow)>
					<cfloop index="i" from="1" to="#remainder#">
							<td>&nbsp;</td>
					</cfloop>
						</tr>
				</cfif>
			</cfoutput>
					</table>
				</td>
			</tr>
		<cfelse>
			<tr>
				<td valign="top">
					<cfoutput>
					No images available for #htmleditformat(getCategoryDetails.categoryName)#.
					</cfoutput>
				</td>
			</tr>
		</cfif>
		</table>
	</cfform>
</cfif>





