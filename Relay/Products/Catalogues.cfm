<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2009/05/18	NJH	P-SNY069 - add promotionGroup TextID

--->

<cfparam name="promotionGroupTextID" type="string" default=""> <!--- NJH 2009/05/18 P-SNY069 --->
<cfparam name="openAsExcel" type="boolean" default="false">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<!---Get a list of the CampaignIDs the User is entitled to use--->
<CFQUERY NAME="Promotions" datasource="#application.siteDataSource#">
	Select CampaignID, ZeroPrice
	From promotion p
	<!--- NJH 2009/05/18 P-SNY069 - add promotionGroup TextID --->
	<cfif promotionGroupTextID neq "">
		inner join promotionGroup pg with (noLock) on p.promotionGroupID = pg.promotionGroupId and pg.promotionGroupTextID =  <cf_queryparam value="#promotionGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfif>
	Where CampaignID IN (select
							recordid
						from
							recordrights as rr, rightsGroup as rg
						where
								entity = 'promotion'
							and rr.userGroupid = rg.usergroupid
							and personid = #request.relayCurrentUser.personID#)
	ORDER BY PromoID
</CFQUERY>

<CFSET CampaignIDAllowed = ValueList(Promotions.CampaignID)>

<cfscript>
// create an instance of the object
myObject = createObject("component","relay.com.RelayEcommerceAdmin");
myObject.dataSource = application.siteDataSource;
myObject.CampaignIDAllowed = CampaignIDAllowed;
myObject.getCatalogueList();
</cfscript>

<cfparam name="sortOrder" default="catalogue">
<cfparam name="numRowsPerPage" default="20">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->

<cfparam name="keyColumnList" type="string" default="catalogue">
<!--- NJH 2009/05/18 P-SNY69 - added promotionGroupTextID to URL --->
<cfparam name="keyColumnURLList" type="string" default="products.cfm?CampaignID=">
<cfparam name="keyColumnKeyList" type="string" default="CampaignID">
<cfparam name="showTheseColumns" type="string" default="Catalogue">
<cfparam name="ColumnHeadingList" type="string" default="Name">

<!--- NJH 2012/10/24 Case 431472 - check user rights --->
<cfif application.com.login.checkSecurityList("leadmanagertask:Level3")>
	<cfset showTheseColumns = listAppend(showTheseColumns,"pricebookLink")>
	<cfset keyColumnURLList = listAppend(keyColumnURLList,"/products/pricebooks.cfm?CampaignID=")>
	<cfset keyColumnKeyList= listAppend(keyColumnKeyList,"CampaignID")>
	<cfset keyColumnList= listAppend(keyColumnList,"pricebookLink")>
	<cfset ColumnHeadingList = "Name, ">
</cfif>

<CF_tableFromQueryObject
	queryObject="#myObject.qGetCatalogueList#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	ColumnHeadingList="#ColumnHeadingList#"
	showTheseColumns="#showTheseColumns#"
	allowColumnSorting="yes"
	useInclude="false"
>