<!--- �Relayware. All Rights Reserved 2014 --->
<!--- We need to analyse the set of actions that may be generated.  --->

<!--- 

***************************************************************************

Any changes to this file must be reflected in the navigation.cfc

***************************************************************************



 --->
<cfif isdefined("url.TreeAction")>
	<cfswitch expression="#url.TreeAction#">
		<cfcase value="AddNewCategoryToWO">
			<cfscript>
			// Register the action (for future rebuild purposes).
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "AddNewCategoryToWO",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				CategoryName = "#url.CategoryName#");
			// Now run the transformation. 
			application.com.relayProductTree.runTransformationAction (
				actionToPerform = "AddNewCategoryToWO",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				CategoryName = "#url.CategoryName#");
			</cfscript>
		</cfcase>
		<cfcase value="EditWOCategoryName">
			<cfscript>
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "editCategoryName",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				CategoryName = "#url.CategoryName#");
		
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "editCategoryName",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				CategoryName = "#url.CategoryName#");
				
			</cfscript>
		</cfcase>
		<cfcase value="DeleteCategory">
			<cfscript>
			// Register the action (for future rebuild purposes). 
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "deleteCategory",
				treeId = "#url.wotreeID#",
				CategoryID = "#url.woCategoryID#");	
			// Now run the transformation. 
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "deleteCategory",
				treeId = "#url.wotreeID#",
				CategoryID = "#url.woCategoryID#");
			</cfscript>	
		</cfcase>
		<cfcase value="AddWFCategoryToWO">
			<cfscript>
			// Register the action (for future rebuild purposes). 
			ExtraParameters = "CopyChildren = #url.CopyChildren#,CopyProducts = #url.CopyProducts#";
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "AddWFCategoryToWO",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				sourceTreeId = "#url.WFTreeId#",
				sourceCategoryID = "#url.WFCategoryID#",
				ExtraParameters = "#ExtraParameters#");		
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "AddWFCategoryToWO",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				sourceTreeId = "#url.WFTreeId#",
				sourceCategoryID = "#url.WFCategoryID#",
				ExtraParameters = "#ExtraParameters#");
			</cfscript>
		</cfcase>
		
		<cfcase value="changeProductStatus">
			
			<CFSCRIPT>
			// Register the action (for future rebuild purposes).
			ExtraParameters = 'status = "#url.status#"';
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "changeProductStatus",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productID = "#abs(url.productID)#",
				ExtraParameters = "#ExtraParameters#");
		
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "changeProductStatus",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productID = "#abs(url.productID)#",
				ExtraParameters = "#ExtraParameters#");	
			</CFSCRIPT>
		</cfcase>		
		<cfcase value="ChangeProductPromotionStatus">
			
			<CFSCRIPT>
			// Register the action (for future rebuild purposes).
			ExtraParameters = 'promotion = "#url.status#"';
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "changeProductPromotionStatus",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productID = "#abs(url.productID)#",
				ExtraParameters = "#ExtraParameters#");
		
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "changeProductPromotionStatus",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productID = "#abs(url.productID)#",
				ExtraParameters = "#ExtraParameters#");	
			</CFSCRIPT>
		</cfcase>
		
		<cfcase value="CopyProduct">
			<cfscript>
			/*application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "CopyProduct",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productKey = "#url.productKey#",
				sourceTreeId = "#url.WFTreeId#",
				sourceCategoryID = "#url.WFCategoryID#");
		
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "CopyProduct",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productKey = "#url.productKey#",
				sourceTreeId = "#url.WFTreeId#",
				sourceCategoryID = "#url.WFCategoryID#");	
			*/
			</cfscript>
		</cfcase>
		<cfcase value="changeUserOrder">
			<cfscript>
			// Register the action (for future rebuild purposes).
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "changeUserOrder",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productID = "#abs(url.productID)#",
				userOrder = "#url.userOrder#");
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "changeUserOrder",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				productID = "#abs(url.productID)#",
				userOrder = "#url.userOrder#");	
			</cfscript>	
		</cfcase>	
		<cfcase value="changeCategoryOrder">
			<cfscript>
			// Register the action (for future rebuild purposes).
			application.com.relayProductTree.insertTransformationAction(
				actionToPerform = "changeCategoryOrder",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				userOrder = "#url.userOrder#");
			// Now run the transformation.
			application.com.relayProductTree.runTransformationAction(
				actionToPerform = "changeCategoryOrder",
				treeId = "#url.WOtreeID#",
				CategoryID = "#url.WOCategoryID#",
				userOrder = "#url.userOrder#");	
			</cfscript>	
		</cfcase>	
		<cfdefaultcase>
		<cfoutput>#htmleditformat(url.TreeAction)# is unknown.</cfoutput>
		
		</cfdefaultcase>
	
	
	
	</cfswitch>

</cfif>