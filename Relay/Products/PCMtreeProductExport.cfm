<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="true">
<cfif openAsExcel>
	<cfset theHeader = "filename=ProductTreeExport.xls">
	<cfif structkeyexists(url,"treeID")>
		<cfscript>
			selectTree = application.com.relayProductTree.selectTree(treeid = url.treeid);
		</cfscript>
		<cfset theheader = "filename=" & replace(selectTree.title," ","","All") & "-ProductExport.xls">
	</cfif>

<cfelse>

	<cf_head>
		<cf_title>Tree Product Export</cf_title>
	</cf_head>
	
</cfif>

<cfparam name="sortOrder" default="COUNTRY_DESCRIPTION">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">

<CFQUERY NAME="GetSPQProductData" datasource="#application.siteDataSource#">
SELECT     PCMproductTree.title AS TreeName, PCMproduct.ProductKey, PCMproduct.title AS ProductTitle, PCP.processStatus AS Status, 
                      Category = CASE WHEN ISNULL(productCategoryParent.displayName, productCategoryParent.CategoryName) IS NULL 
                      THEN ISNULL(PCMproductCategory.displayName, PCMproductCategory.CategoryName) ELSE ISNULL(productCategoryParent.displayName, 
                      productCategoryParent.CategoryName)END , ISNULL(PCMproductCategory.displayName, PCMproductCategory.CategoryName) as ParentCategory 
FROM         PCMproductCategoryProduct PCP INNER JOIN
                      PCMproductCategory ON PCMproductCategory.categoryID = PCP.categoryID AND PCP.treeID = PCMproductCategory.TreeID INNER JOIN
                      PCMproduct ON PCP.productID = PCMproduct .productID INNER JOIN
                      PCMproductTree ON PCMproductCategory.TreeID = PCMproductTree.TreeID LEFT OUTER JOIN
                      PCMproductCategory productCategoryParent ON PCMproductCategory.TreeID = productCategoryParent.TreeID AND PCMproductCategory.ParentCategoryID = productCategoryParent.categoryID
WHERE   1=1
AND  (PCP.treeID =  <cf_queryparam value="#url.treeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) 
 AND (PCMproductTree.TreeID =  <cf_queryparam value="#url.treeID#" CFSQLTYPE="CF_SQL_INTEGER" > )
AND (PCMproductCategory.deleted = 0)
ORDER BY 
	ISNULL(productCategoryParent.displayName, productCategoryParent.CategoryName), 
	ISNULL(PCMproductCategory.displayName,PCMproductCategory.CategoryName), 
	CASE WHEN pcp.userOrder IS NULL THEN PCP.sequence ELSE pcp.userOrder END
</CFQUERY>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#GetSPQProductData#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	showTheseColumns="TreeName,ParentCategory,Category,ProductKey,ProductTitle,Status"
	columnTranslation="false"
	sortorder=""
	
	allowColumnSorting="no"
	startRow="#startRow#"
	useInclude="false"
>

</cf_translate>





