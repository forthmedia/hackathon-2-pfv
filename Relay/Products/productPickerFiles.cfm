<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			productPickerFiles.cfm
Author:				YMA
Date started:		2013/01/14

Purpose:	Display Product Files on portal.  Included into productPicker.cfm
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-07-28 DAN 445276 - Using RELAY_CATALOGUEASSETS and RELAY_FILE_LIST relaytags on a page results in an error
2015-08-05 DAN K-1314 - allow filtering products by multiple media groups

Possible enhancements:

 --->
		<CFIF IsDefined("FORM.fileID") and FORM.fileID neq 0>
			<CF_INCLUDEONCE TEMPLATE="/fileManagement/fileGet.cfm">
		<cfelse>
			<cf_includeJavascriptOnce template = "/javascript/openWin.js">
		</CFIF>


		<!--- Set variables for fileListDisplay --->
		<!--- <cfset FileListByGroup="true"/> ---> <!--- 2015-07-28 DAN 445276 - commenting out unused property as it breaks things by overriding same named parameter in the FileList.cfm (if used both tags are used together) --->
		<cfset plainRows="false"/>
		<cfset listFilesByType="true"/>
		<cfset defaultFileListColumns=1/>
		<cfset incr = 1>
		<cfset trWritten = false>
		<cfset source = "file">
		<cfset fileExists = true>
		<cfset icon = "">
		<cfset SUPPRESSFILENAME = "true">
		<cfset colwidth =  int(100/defaultFileListColumns)>
		<cfset oldType = "">
		<cfset newType = "">

        <cfset argStruct = {productID = productID, fuzzyProductMatch = fuzzyProductMatch}>
        <!--- 2015-08-05 DAN K-1314 - file types/groups support single and multiple values (list) --->
        <cfif isDefined("productFileTypeGroupIDs") and productFileTypeGroupIDs is not "" and productFileTypeGroupIDs is not 0>
			<cfset argStruct.fileTypeGroupID = productFileTypeGroupIDs>
		</cfif>
		<cfif isDefined("productFileTypeIDs") and productFileTypeIDs is not "" and productFileTypeIDs is not 0>
			<cfset argStruct.FileTypeID = productFileTypeIDs>
		</cfif>

		<cfset GetResults = application.com.relayProduct.getProductAssets(argumentCollection=argStruct)>
		<cfif fileType eq "assets">
			<CFQUERY NAME="GetResults" dbtype="query">
				select * from GetResults where heading not like '%ELearning_CourseWare%'
			</CFQUERY>
		<cfelseif fileType eq "training">
			<CFQUERY NAME="GetResults" dbtype="query">
				select * from GetResults where heading like '%ELearning_CourseWare%'
			</CFQUERY>
		</cfif>
		<cfif GetResults.recordcount gt 0>
		<cfoutput>
		<div id="productAssets" class="row">
			<div id="fileList">
				<div id="fileList">
					<cfloop QUERY="GetResults">
						<cfset newType = GetResults.type>
						<cfif newType neq oldType>
							<div class="col-xs-12"><h4>#GetResults.type#</h4></div>
						</cfif>
							<cfinclude template = "/RelayTagTemplates/fileListDisplay.cfm">
						<cfset oldType = newType>
					</cfloop>
				</div>
			</div>
		</div>
		</cfoutput>
		</cfif>
        
		<!--- <SCRIPT type="text/javascript">
			function getFile(fileID,entityTypeID,entityID) {
				openWin (' ','FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')
				form = document.getFileForm;
				form.fileID.value = fileID;
				form.entityTypeID.value = entityTypeID;
				form.entityID.value = entityID;
				form.submit();
			}
			getFile = getFile
			function drillToFileGroup(filegroupID) {
				form = document.getFileForm;
				form.FileTypeGroupID.value = filegroupID;
				form.submit();
			}
		</script> --->
		<cfparam name="showFileListForm" default="true">
		<cfif showFileListForm>
			<cfoutput>

			<cfset fileFormAction = "/fileManagement/FileGet.cfm">
			<FORM METHOD="get" NAME="getFileForm" action = "#fileFormAction#" target = "FileDownload">
				<input type="hidden" name="fileID" value="0">
				<input type="hidden" name="entityTypeID" value="">
				<input type="hidden" name="entityID" value="">
				<cfif isdefined('request.currentElement.id')> <!--- 2016/02/26 GCC 448050 / HF1427 / RT-299 --->
					<input type="hidden" name="eid" value="#htmlEditFormat(request.currentElement.id)#">
				</cfif>
				<CF_INPUT type="hidden" name="openWinType" value="1">
				<!--- 2015-08-05 DAN - param name updated --->
				<CFIF IsDefined("productFileTypeGroupIDs") and productFileTypeGroupIDs is not "">
					<CF_INPUT type="Hidden" NAME="productFileTypeGroupIDs" value="#productFileTypeGroupIDs#">
				<CFELSE>
					<input type="Hidden" NAME="productFileTypeGroupIDs" value="0">
				</CFIF>
				<CFIF IsDefined("ElementID")>
					<CF_INPUT type="hidden" name="ElementID" value="#ElementID#">
				</CFIF>
			</FORM>
			</cfoutput>
			<cfset showFileListForm = false>
		</cfif>