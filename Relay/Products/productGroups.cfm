<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			products.cfm
Author:				NJH
Date started:		2012/12/05

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:


 --->
<cfsetting requesttimeout="300">

<cfparam name="campaignID" type="numeric" default=0>
<cfparam name="getProductGroups" type="string" default="">
<cfparam name="sortOrder" default="ProductCategory,Title">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="showTheseColumns" type="string" default="ProductCategory,Title,ProductType,LastUpdated">
<cfparam name="url.active" type="Boolean" default="1">

<cfparam name="filterBy" type="string" default="#showTheseColumns#">

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="productGroup" title="ProductGroup Editor">
			<field name="productGroupID" label="phr_productGroup_productGroupID" description="This records unique ID." control="html"></field>
			<field name="" label="phr_productGroup_title" description="The product title" required="true" control="translation" parameters="phraseTextID=title,allPhraseTextIDsOnPage='title,description'"/>
			<field name="productTypeID" label="phr_productGroup_productType" description="" control="select" query="func:com.relayProduct.getProductTypes()" display="name" value="productTypeID"></field>
			<field name="productCategoryID" label="phr_productGroup_productCategory" description="" control="select" query="func:com.relayProduct.getProductCategories()" display="productCategory" value="productCategoryID" nullText="phr_ext_selectAvalue"></field>
			<field name="active" label="phr_productGroup_Active" description="The display of Active Product Groups." control="YorN"></field>
			<group label="Display Information">
				<field name="" label="phr_productGroup_portalTitle" description="The product title as displayed within the portal" control="translation" parameters="phraseTextID=portalTitle,allPhraseTextIDsOnPage='title,description'" />
				<field name="" label="phr_productGroup_description" description="The product group description" control="translation" parameters="phraseTextID=description,displayAs=TextArea"/>
				<field name="" control="file" acceptType="image" label="phr_productGroup_ThumbnailImage" parameters="filePrefix=Thumb_,fileExtension=jpg,displayHeight=80,height=80,width=80" description=""></field>
			</group>
			<cfif application.com.relayMenu.isModuleActive(moduleID="Incentive")>
				<group label="Incentive Information">
					<field name="incentiveSerialNumberRequired" label="phr_productGroup_SerialNumberRequired" control="YorN"></field>
				</group>
			</cfif>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(url,"editor")>

	<cfquery name="getProductGroups" datasource="#application.siteDataSource#">
		select * from (
			select pg.productGroupID, case when pg.title_defaultTranslation is not null then pg.title_defaultTranslation else pg.description_defaultTranslation end as title,
				pg.description_defaultTranslation as description, pg.active,
				pc.title_defaultTranslation as productCategory,pt.name as productType,pg.lastUpdated
			from productGroup pg
				left join productCategory pc on pc.productCategoryID = pg.productCategoryID
				left join productType pt on pg.productTypeID = pt.ID
			) base
			where 1=1
			and isNull(active,1) = <cf_queryParam value="#url.active#" cfSqlType="CF_SQL_BIT">
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>


<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="productGroups"
	keyColumnList="Title"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(Title)##'*comma'##jsStringFormat(Title)##'*comma'/products/productGroups.cfm?editor=yes&hideBackButton=true&productGroupID=##jsStringFormat(productGroupID)##'*comma{reuseTab:true*commaiconClass:'products'});return false;"
	showTheseColumns="#showTheseColumns#"
	FilterSelectFieldList="#filterBy#"
	FilterSelectFieldList2="#filterBy#"
	useInclude="false"
	sortOrder="#sortOrder#"
	hideBackButton="true"
	queryData="#getProductGroups#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	groupByColumns="ProductCategory"
	hideTheseColumns=""
	dateFormat="lastUpdated"
	showSaveAndAddNew="true"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_productGroup_"
>