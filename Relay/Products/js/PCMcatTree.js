/* �Relayware. All Rights Reserved 2014 */
/*--------------------------------------------------|
| catTree 2.05 | www.destroydrop.com/javascript/tree/ |
|---------------------------------------------------|
| Copyright (c) 2002-2003 Geir Landr�               |
|                                                   |
| This script can be used freely as long as all     |
| copyright messages are intact.                    |
|                                                   |
| Updated: 17.04.2003                               |
|--------------------------------------------------*/

// Node object
function Node(id, pid, name, url, title, target, icon, iconOpen, open,nodetype,categoryid,categoryImageStatus,promotion) {
	this.id = id;
	this.pid = pid;
	this.name = name;
	this.url = url;
	this.title = title;
	this.target = target;
	this.icon = icon;
	this.iconOpen = iconOpen;
	this._io = open || false;
	this._is = false;
	this._ls = false;
	this._hc = false;
	this._ai = 0;
	this._p;
	this.nodetype = nodetype;
	this.categoryid = categoryid;
	this.categoryImageStatus = categoryImageStatus;
	this.promotion = promotion;
};

// Tree object
function catTree(objName) {
	this.config = {
		target					: null,
		folderLinks			: true,
		useSelection		: true,
		useCookies			: true,
		useLines				: true,
		useIcons				: true,
		useStatusText		: false,
		closeSameLevel	: false,
		inOrder					: false
	}
	this.icon = {
		root				: '/images/PCMicons/base.gif',
		folder			: '/images/PCMicons/folder.gif',
		folderOpen	: '/images/PCMicons/folderopen.gif',
		node				: '/images/PCMicons/page.gif',
		empty				: '/images/PCMicons/empty.gif',
		line				: '/images/PCMicons/line.gif',
		join				: '/images/PCMicons/join.gif',
		joinBottom	: '/images/PCMicons/joinbottom.gif',
		plus				: '/images/PCMicons/plus.gif',
		plusBottom	: '/images/PCMicons/plusbottom.gif',
		minus				: '/images/PCMicons/minus.gif',
		minusBottom	: '/images/PCMicons/minusbottom.gif',
		nlPlus			: '/images/PCMicons/nolines_plus.gif',
		nlMinus			: '/images/PCMicons/nolines_minus.gif'
	};
	this.obj = objName;
	this.aNodes = [];
	this.aIndent = [];
	this.root = new Node(-1);
	this.selectedNode = null;
	this.selectedFound = false;
	this.completed = false;
};

// Adds a new node to the node array
catTree.prototype.add = function(id, pid, name, url, title, target, icon, iconOpen, open,nodetype,categoryid,categoryImageStatus,promotion) {
	this.aNodes[this.aNodes.length] = new Node(id, pid, name, url, title, target, icon, iconOpen, open,nodetype,categoryid,categoryImageStatus,promotion);
};

// Open/close all nodes
catTree.prototype.openAll = function() {
	this.oAll(true);
};
catTree.prototype.closeAll = function() {
	this.oAll(false);
};

// Outputs the tree to the page
catTree.prototype.toString = function() {
	var str = '<div class="cattree">\n';
	if (document.getElementById) {
		if (this.config.useCookies) this.selectedNode = this.getSelected();
		str += this.addNode(this.root);
	} else str += 'Browser not supported.';
	str += '</div>';
	if (!this.selectedFound) this.selectedNode = null;
	this.completed = true;
	return str;
};

// Creates the tree structure
catTree.prototype.addNode = function(pNode) {
	var str = '';
	var n=0;
	if (this.config.inOrder) n = pNode._ai;
	for (n; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == pNode.id) {
			var cn = this.aNodes[n];
			cn._p = pNode;
			cn._ai = n;
			this.setCS(cn);
			if (!cn.target && this.config.target) cn.target = this.config.target;
			if (cn._hc && !cn._io && this.config.useCookies) cn._io = this.isOpen(cn.id);
			if (!this.config.folderLinks && cn._hc) cn.url = null;
			if (this.config.useSelection && cn.id == this.selectedNode && !this.selectedFound) {
					cn._is = true;
					this.selectedNode = n;
					this.selectedFound = true;
			}
			str += this.node(cn, n);
			if (cn._ls) break;
		}
	}
	return str;
};

// Creates the node icon, url and text
catTree.prototype.node = function(node, nodeId) {
	var str = '<div class="catTreeNode">' + this.indent(node, nodeId);
	if (this.config.useIcons) {
		if (!node.icon) node.icon = (this.root.id == node.pid) ? this.icon.root : ((node._hc) ? this.icon.folder : this.icon.node);
		if (!node.iconOpen) node.iconOpen = (node._hc) ? this.icon.folderOpen : this.icon.node;
		if (this.root.id == node.pid) {
			node.icon = this.icon.root;
			node.iconOpen = this.icon.root;
		}
		str += '<img id="i' + this.obj + nodeId + '" src="' + ((node._io) ? node.iconOpen : node.icon) + '" alt="" />';
		
	}
	if (node.url || ((!this.config.folderLinks || !node.url) && node._hc)) str += '</a>';
	switch(node.nodetype){
		case 'WOCategory': {
			str += '<a href="javascript: ' + 'tree_setWOCategoryID('+ node.categoryid + ');" class="node" title="Set the working category"><img src="/images/PCMicons/setnode.gif" border="0"></a>';
			str += '<a href="javascript: ' + 'tree_AddNewCategoryToWO('+  node.categoryid + ');" class="node" title="Add new child category."><img src="/images/PCMicons/new.gif" border="0"></a>';
			str += '<a href="javascript: ' + 'tree_DeleteWOCategory('+  node.categoryid + ');" class="node" title="Delete category and children."><img src="/images/PCMicons/cut.gif" border="0"></a>';
			str += '<a href="javascript: ' + 'tree_editWOCategoryName('+  node.categoryid + ');" class="node" title="Edit category name."><img src="/images/PCMicons/categoryEdit.gif" border="0"></a>';
			//CR_SNY535 - GCC - START
			//alert(node.categoryImageStatus);
			switch(node.categoryImageStatus){
				case 'Automatic': {
				str += '<a href="javascript: ' + 'tree_editWOCategoryImage('+  node.categoryid + ');" class="node" title="Edit category image."><img src="/images/PCMicons/categoryImageEditAUTO.gif" height="18" width="18" border="0"></a>';
				break;
				}
				case 'Fixed': {
				str += '<a href="javascript: ' + 'tree_editWOCategoryImage('+  node.categoryid + ');" class="node" title="Edit category image."><img src="/images/PCMicons/categoryImageEditFIXED.gif" height="18" width="18" border="0"></a>';
				break;
				}
				case 'Uploaded': {
				str += '<a href="javascript: ' + 'tree_editWOCategoryImage('+  node.categoryid + ');" class="node" title="Edit category image."><img src="/images/PCMicons/categoryImageUploadIcon.jpg" height="18" width="18" border="0"></a>';
				break;
				}
			}
			//CR_SNY535 - GCC - END		
			str += '<a href="javascript: ' + 'tree_changeCategoryOrder('+node.categoryid +');" class="node" title="Order category">123</a>';

			break;			
		}
		case 'WFCategory': {
			str += '<a href="javascript: ' + 'tree_AddWFCategoryToWO('+ node.categoryid + ',true,true);" class="node" title="Copies category, all child categories, and all products."><img src="/images/PCMicons/copyall.gif" border="0"></a>';
			str += '<a href="javascript: ' + 'tree_AddWFCategoryToWO('+ node.categoryid + ',true,false);" class="node" title="Copies category and all child categories, no products."><img src="/images/PCMicons/copycategories.gif" border="0"></a>';
			str += '<a href="javascript: ' + 'tree_AddWFCategoryToWO('+ node.categoryid + ',false,true);" class="node" title="Copies products and products in child categories."><img src="/images/PCMicons/copyproducts.gif" border="0"></a>';
			break;
		}
		/*case 'WFProductNEW': {
			str += '<a href="javascript: ' + 'tree_CopyWFProduct('+node.pid+',\''+node.id+'\');" class="node" title="Copy product to target category."><img src="/images/PCMicons/move_right.gif" border="0"></a>';
			break;
		}
		case 'WFProductEXISTING': {
			str += '<a href="javascript: ' + 'tree_CopyWFProduct('+node.pid+',\''+node.id+'\');" class="node" title="Copy product to target category."><img src="/images/PCMicons/move_right.gif" border="0"></a>';
			break;
		}*/
		case 'WOProductNEW': {
			str += '<a href="javascript: ' + 'tree_ChangeProductStatus('+node.pid+',' +'\''+node.id+'\'' + ',\'SUPPRESSED\');" class="node" title="Suppress product"><img src="/images/PCMicons/move_down.gif" border="0"></a>';
			break;
		}
		case 'WOProductEXISTING': {
			str += '<a href="javascript: ' + 'tree_ChangeProductStatus('+node.pid+',' +'\''+node.id+'\'' + ',\'SUPPRESSED\');" class="node" title="Suppress product"><img src="/images/PCMicons/move_down.gif" border="0"></a>';
			break;
		}
		case 'WOProductSUPPRESSED': {
			str += '<a href="javascript: ' + 'tree_ChangeProductStatus('+node.pid +',' +'\''+node.id+'\'' + ',\'EXISTING\');" class="node" title="Unsuppress product"><img src="/images/PCMicons/move_up.gif" border="0"></a>';
			break;
		}
	}
	// Promotion type.
	switch(node.nodetype){
	case 'WOProductNEW':
	case 'WOProductEXISTING':
	case 'WOProductSUPPRESSED':{
		if (node.promotion == 1){
			str += '<a href="javascript: ' + 'tree_ChangeProductPromotionStatus('+node.pid +',' +node.id+',0);" class="node" title="Un-promote product"><img src="/images/PCMicons/promotion.gif" border="0"></a> ';
		}
		else {
			str += '<a href="javascript: ' + 'tree_ChangeProductPromotionStatus('+node.pid +','+node.id+',1);" class="node" title="Promote product"><img src="/images/PCMicons/nonpromotion.gif" border="0"></a> ';
		}
		//CR_SNY535 - GCC - START
		str += '<a href="javascript: ' + 'tree_changeUserOrder('+node.pid +','+node.id+');" class="node" title="Order product">123</a> ';	
		//CR_SNY535 - GCC - END
	}
	}
	if (this.root.id == node.pid && this.obj == 'WO'){
		str += '<a href="javascript: ' + 'tree_AddNewCategoryToWO();" class="node" title="Add new category."><img src="/images/PCMicons/new.gif" border="0"></a>';
		str += '<a href="javascript: ' + 'tree_setWOCategoryID();" class="node" title="Clear/Set Working On category to blank."><img src="/images/PCMicons/setnode.gif" border="0"></a>';
	}
	if (this.root.id == node.pid){
		str += ' <a href="javascript: ' + this.obj+'.openAll()" class="node" title="Open All">+</a> ';
		str += ' <a href="javascript: ' + this.obj+'.closeAll()" class="node" title="Close All">-</a> ';
	
	}
	if (node.url) {
		str += '<a id="s' + this.obj + nodeId + '" class="' + ((this.config.useSelection) ? ((node._is ? 'nodeSel' : 'node')) : 'node') + '" href="' + node.url + '"';
		if (node.title) str += ' title="' + node.title + '"';
		if (node.target) str += ' target="' + node.target + '"';
		if (this.config.useStatusText) str += ' onmouseover="window.status=\'' + node.name + '\';return true;" onmouseout="window.status=\'\';return true;" ';
		if (this.config.useSelection && ((node._hc && this.config.folderLinks) || !node._hc))
			str += ' onclick="javascript: ' + this.obj + '.s(' + nodeId + ');"';
		str += '>';
	}
	else if ((!this.config.folderLinks || !node.url) && node._hc && node.pid != this.root.id)
		str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');" class="node">';
	str += node.name;
	
	str += '</div>';
	if (node._hc) {
		str += '<div id="d' + this.obj + nodeId + '" class="clip" style="display:' + ((this.root.id == node.pid || node._io) ? 'block' : 'none') + ';">';
		str += this.addNode(node);
		str += '</div>';
	}
	this.aIndent.pop();
	
	return str;
};

// Adds the empty and line icons
catTree.prototype.indent = function(node, nodeId) {
	var str = '';
	if (this.root.id != node.pid) {
		for (var n=0; n<this.aIndent.length; n++)
			str += '<img src="' + ( (this.aIndent[n] == 1 && this.config.useLines) ? this.icon.line : this.icon.empty ) + '" alt="" />';
		(node._ls) ? this.aIndent.push(0) : this.aIndent.push(1);
		if (node._hc) {
			str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');"><img id="j' + this.obj + nodeId + '" src="';
			if (!this.config.useLines) str += (node._io) ? this.icon.nlMinus : this.icon.nlPlus;
			else str += ( (node._io) ? ((node._ls && this.config.useLines) ? this.icon.minusBottom : this.icon.minus) : ((node._ls && this.config.useLines) ? this.icon.plusBottom : this.icon.plus ) );
			str += '" alt="" /></a>';
		} else str += '<img src="' + ( (this.config.useLines) ? ((node._ls) ? this.icon.joinBottom : this.icon.join ) : this.icon.empty) + '" alt="" />';
	}
	return str;
};

// Checks if a node has any children and if it is the last sibling
catTree.prototype.setCS = function(node) {
	var lastId;
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id) node._hc = true;
		if (this.aNodes[n].pid == node.pid) lastId = this.aNodes[n].id;
	}
	if (lastId==node.id) node._ls = true;
};

// Returns the selected node
catTree.prototype.getSelected = function() {
	var sn = this.getCookie('cs' + this.obj);
	return (sn) ? sn : null;
};

// Highlights the selected node
catTree.prototype.s = function(id) {
	if (!this.config.useSelection) return;
	var cn = this.aNodes[id];
	if (cn._hc && !this.config.folderLinks) return;
	if (this.selectedNode != id) {
		if (this.selectedNode || this.selectedNode==0) {
			eOld = document.getElementById("s" + this.obj + this.selectedNode);
			eOld.className = "node";
		}
		eNew = document.getElementById("s" + this.obj + id);
		eNew.className = "nodeSel";
		this.selectedNode = id;
		if (this.config.useCookies) this.setCookie('cs' + this.obj, cn.id);
	}
};

// Toggle Open or close
catTree.prototype.o = function(id) {
	var cn = this.aNodes[id];
	this.nodeStatus(!cn._io, id, cn._ls);
	cn._io = !cn._io;
	if (this.config.closeSameLevel) this.closeLevel(cn);
	if (this.config.useCookies) this.updateCookie();
};

// Open or close all nodes
catTree.prototype.oAll = function(status) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._hc && this.aNodes[n].pid != this.root.id) {
			this.nodeStatus(status, n, this.aNodes[n]._ls)
			this.aNodes[n]._io = status;
		}
	}
	if (this.config.useCookies) this.updateCookie();
};

// Opens the tree to a specific node
catTree.prototype.openTo = function(nId, bSelect, bFirst) {
	if (!bFirst) {
		for (var n=0; n<this.aNodes.length; n++) {
			if (this.aNodes[n].id == nId) {
				nId=n;
				break;
			}
		}
	}
	var cn=this.aNodes[nId];
	if (cn.pid==this.root.id || !cn._p) return;
	cn._io = true;
	cn._is = bSelect;
	if (this.completed && cn._hc) this.nodeStatus(true, cn._ai, cn._ls);
	if (this.completed && bSelect) this.s(cn._ai);
	else if (bSelect) this._sn=cn._ai;
	this.openTo(cn._p._ai, false, true);
};

// Closes all nodes on the same level as certain node
catTree.prototype.closeLevel = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.pid && this.aNodes[n].id != node.id && this.aNodes[n]._hc) {
			this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);
		}
	}
}

// Closes all children of a node
catTree.prototype.closeAllChildren = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id && this.aNodes[n]._hc) {
			if (this.aNodes[n]._io) this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);		
		}
	}
}

// Change the status of a node(open or closed)
catTree.prototype.nodeStatus = function(status, id, bottom) {
	eDiv	= document.getElementById('d' + this.obj + id);
	eJoin	= document.getElementById('j' + this.obj + id);
	if (this.config.useIcons) {
		eIcon	= document.getElementById('i' + this.obj + id);
		eIcon.src = (status) ? this.aNodes[id].iconOpen : this.aNodes[id].icon;
	}
	eJoin.src = (this.config.useLines)?
	((status)?((bottom)?this.icon.minusBottom:this.icon.minus):((bottom)?this.icon.plusBottom:this.icon.plus)):
	((status)?this.icon.nlMinus:this.icon.nlPlus);
	eDiv.style.display = (status) ? 'block': 'none';
};


// [Cookie] Clears a cookie
catTree.prototype.clearCookie = function() {
	var now = new Date();
	var yesterday = new Date(now.getTime() - 1000 * 60 * 60 * 24);
	this.setCookie('co'+this.obj, 'cookieValue', yesterday);
	this.setCookie('cs'+this.obj, 'cookieValue', yesterday);
};

// [Cookie] Sets value in a cookie
catTree.prototype.setCookie = function(cookieName, cookieValue, expires, path, domain, secure) {
	document.cookie =
		escape(cookieName) + '=' + escape(cookieValue)
		+ (expires ? '; expires=' + expires.toGMTString() : '')
		+ (path ? '; path=' + path : '')
		+ (domain ? '; domain=' + domain : '')
		+ (secure ? '; secure' : '');
};

// [Cookie] Gets a value from a cookie
catTree.prototype.getCookie = function(cookieName) {
	var cookieValue = '';
	var posName = document.cookie.indexOf(escape(cookieName) + '=');
	if (posName != -1) {
		var posValue = posName + (escape(cookieName) + '=').length;
		var endPos = document.cookie.indexOf(';', posValue);
		if (endPos != -1) cookieValue = unescape(document.cookie.substring(posValue, endPos));
		else cookieValue = unescape(document.cookie.substring(posValue));
	}
	return (cookieValue);
};

// [Cookie] Returns ids of open nodes as a string
catTree.prototype.updateCookie = function() {
	var str = '';
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._io && this.aNodes[n].pid != this.root.id) {
			if (str) str += '.';
			str += this.aNodes[n].id;
		}
	}
	this.setCookie('co' + this.obj, str);
};

// [Cookie] Checks if a node id is in a cookie
catTree.prototype.isOpen = function(id) {
	var aOpen = this.getCookie('co' + this.obj).split('.');
	for (var n=0; n<aOpen.length; n++)
		if (aOpen[n] == id) return true;
	return false;
};

// If Push and pop is not implemented by the browser
if (!Array.prototype.push) {
	Array.prototype.push = function array_push() {
		for(var i=0;i<arguments.length;i++)
			this[this.length]=arguments[i];
		return this.length;
	}
};
if (!Array.prototype.pop) {
	Array.prototype.pop = function array_pop() {
		lastElement = this[this.length-1];
		this.length = Math.max(this.length-1,0);
		return lastElement;
	}
};