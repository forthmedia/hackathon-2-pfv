function exportToFile(){

	//NB. there is a similar export from the portal in oppPriceBookExport.cfm; any changes here may need doing there too! perhaps they should be merged !!!
	var elemOrganisation = document.getElementById('organisationID');
	var elemCountry = document.getElementById('countryID');
	var elemCurrency = document.getElementById('currency');

	if (elemOrganisation.selectedIndex == -1 || elemCountry.selectedIndex == -1 || elemCurrency.selectedIndex == -1) {
		alert('phr_YouShouldSelectAnOrganisationACountryAndACurrency');
	} else {
		var organisationName = elemOrganisation.options[elemOrganisation.selectedIndex].text;
		//remove unwanted chars from the organisation name
		organisationName = stripChar(organisationName," ");
		organisationName = stripChar(organisationName,",");
		organisationName = stripChar(organisationName,"(");
		organisationName = stripChar(organisationName,")");
		organisationName = stripChar(organisationName,"&");


		var countryName = elemCountry.options[elemCountry.selectedIndex].text;
		//remove unwanted chars from the country name
		countryName = stripChar(countryName," ");
		countryName = stripChar(countryName,",");
		countryName = stripChar(countryName,"(");
		countryName = stripChar(countryName,")");
		countryName = stripChar(countryName,"&");

		var currencyISOCode = elemCurrency.options[elemCurrency.selectedIndex].value;
		var excelFileName = 'Pricebook_' + organisationName + '_' + countryName + '_'  + currencyISOCode + '.xls';		// i'm not using countryISO cos for "groups" eg Benelux it is set to null; i'm not using countryId cos country is easier to read

		location.href = location.href + '&openAsExcel=true&excelFileName=' + excelFileName;
	}
}