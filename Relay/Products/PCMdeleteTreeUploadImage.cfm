<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="500" enablecfoutputonly="No">
<!--- 
File name:		treeCategoryImagePickerV2.cfm	
Author:			GCC
Date started:	2005-07-20
	
Description:		This provides a listing of all product images available at this point
or below in the user tree. One can be selected and the currently selected image displays with a different BG colour.

Usage: Created for CR_SNY535 - GCC - category image chooser	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


 --->
 <cfparam name="thumbsize" default="60">
<cfif structkeyExists(request,"productThumbsize")>
	<cfset thumbsize = request.productThumbsize>
</cfif>

<cfset productCategoryImagesDirectory = request.productCategoryImagesDirectory>
<cfset productCategoryImagesDirectoryURL = request.productCategoryImagesDirectoryURL>
 


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Uploaded Tree Image Manager</TD>
	</TR>
</TABLE> 

<!--- delete uploaded image --->
<cfif isDefined("form.imageFileName") and form.imageFileName neq "">

	<cfscript>
		application.com.relayProductTree.deleteTreeImage(
		imageFile = form.imageFileName,
		treeId = "#url.WOtreeID#");
	</cfscript>
	<cfoutput>
		<script>
			//Reload treeManagementWindow	
			window.opener.location.href='PCMtreeCategoryImagePicker.cfm?WOTreeID=#jsStringFormat(url.WOTreeID)#&nodeID=#jsStringFormat(nodeID)#';
		</script>
	</cfoutput>


<!--- upload image to tree --->
<cfelseif isdefined("form.imageFile") and form.imageFile neq "">
	
	<!--- upload image to tree --->
	<cfscript>
		newFile = application.com.relayProductTree.uploadTreeImage(
		imageFile = "form.imageFile",
		treeId = "#url.WOtreeID#",
		thumbsize = #thumbsize#);
	</cfscript>
	<cfoutput>
		<script>
			//Reload treeManagementWindow	
			window.opener.location.href='PCMtreeCategoryImagePicker.cfm?WOTreeID=#jsStringFormat(url.WOTreeID)#&nodeID=#jsStringFormat(nodeID)#';
		</script>
	</cfoutput>
	
</cfif>


<cfset noOfElementsPerRow = 4>
<!--- UPLOADED IMAGES LIBRARY --->

<cfoutput>
	<cfscript>
		treeDetail = application.com.relayProductTree.selectTree(
			treeID=#url.WOtreeID#);
		treeImages = application.com.relayProductTree.getTreeImages(
			treeID=#url.WOtreeID#);
	</cfscript>
</cfoutput>
<cfform  method="POST" name="categoryImageForm" id="categoryImageForm" enctype="multipart/form-data">
	<input type="hidden" name="isActioned" value="true">
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" class="withBorder">	
	<cfif isQuery(treeImages) AND treeImages.recordcount gt 0>
		<tr>
			<td valign="top">
				Click a button below to delete that image.
			</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0">
		<cfoutput query="treeImages">
			<cfif treeImages.currentrow eq 1>
					<tr>
			</cfif>
						<td>
							<table border="1" width="100%" height="80" cellpadding="0" cellspacing="0" class="withBorder">
								<tr>
									<td>
										<table width="100%" border="0">
											<tr>
												<td>
													<img src="#productCategoryImagesDirectoryURL#/#url.WOtreeID#/#treeImages.name#" alt="" border="0">
												</td>
												<td align="right">
													<CF_INPUT type="radio" name="imageFileName" value="#treeImages.name#" onclick="form.submit()">
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
			<cfif treeImages.currentrow mod noOfElementsPerRow eq 0>
					</tr>
					<tr>
			</cfif>
			<cfif treeImages.currentrow eq treeImages.recordcount>
				<cfset remainder = noOfElementsPerRow - (treeImages.currentrow mod noOfElementsPerRow)>
				<cfloop index="i" from="1" to="#remainder#">
						<td>&nbsp;</td>
				</cfloop>
					</tr>
			</cfif>
		</cfoutput>
				</table>
			</td>
		</tr>
	<cfelse>
		<tr>
			<td valign="top">
				No images available for tree <cfoutput>#htmleditformat(treeDetail.title)#</cfoutput>.
			</td>
		</tr>
	</cfif>
		<tr>
			<td>
				<table widht="100%">
					<tr>
						<td>
							Upload an image for tree <cfoutput>#htmleditformat(treeDetail.title)#</cfoutput>:
						</td>
						<td>
							<cfform  method="POST" name="categoryImageUploadForm" id="categoryImageUploadForm" enctype="multipart/form-data">
								<input type="file" name="imageFile">&nbsp;&nbsp;&nbsp;<input type="submit" value="upload file">
							</cfform>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</cfform>






