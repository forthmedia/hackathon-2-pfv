<cfparam name="PricebookID" type="numeric" default="0">
<cfparam name="sortOrder" default="Organisation,PricebookName,Country,currency,StartDate">
<cfparam name="startRow" default="1">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="pricebookFilterSelectFieldList" type="string" default="Organisation,Country,Currency">
<cfparam name="checkBoxFilterName" type="string" default="showOldPricebooks">
<cfparam name="checkBoxFilterLabel" type="string" default="Show Expired Pricebooks?">


<cfset xmlSource = "">
<cfset pricebooks = "">

<cfif structKeyExists(url,"editor")>
	<cf_includeJavascriptOnce template="/javascript/stripChar.js">
	<cf_includeJavascriptOnce template="/products/js/pricebook.js">

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="pricebook" title="Pricebook Editor">
				<field name="pricebookID" label="phr_pricebook_pricebookID" description="This records unique ID." control="html"></field>
				<field name="organisationID" label="phr_pricebook_organisationID" required="true" query="func:getObject('products.com.pricebook').getPricebookOrganisations(pricebookID=#pricebookId#)" display="OrganisationName" value="OrganisationID"/>
				<field name="name" label="phr_pricebook_name" size="50" required="true"/>
				<field name="countryID" label="phr_pricebook_countryID" required="true" query="func:com.relaycountries.getCountries(withCurrencies=true,showCurrentUsersCountriesOnly=true)" display="countrydescription" value="countryID"/>
				<field name="currency" label="phr_pricebook_currency" required="true" display="currencyName" value="currencyISO" bindFunction="cfc:WebServices.relaycountries.getCurrenciesForCountry({countryID})" bindOnLoad="#pricebookID eq 0?false:true#"/>
				<field name="startDate" label="phr_pricebook_startDate" required="true"/>
				<field name="endDate" label="phr_pricebook_endDate"/>
				<field name="filename" control="file" label="phr_pricebook_file" noteText="phr_YouMustImportAcsvFile" noteTextPosition="right" accept="csv"/>
				<field name="campaignID" label="campaignID" control="hidden" <cfif structKeyExists(url,"campaignID")>currentValue="#url.campaignID#"</cfif>/>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>
	<cfset pricebooks = application.getObject("products.com.pricebook").getPricebookListing(campaignID=url.campaignID)>

	<cfset application.com.request.setDocumentH1(text="Pricebooks")>
</cfif>

<cfset campaignURL = "">
<cfif structKeyExists(url,"campaignID") and isNumeric(url.campaignID)>
	<cfset campaignURL = "&campaignID=#url.campaignID#">
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	queryData="#pricebooks#"
	sortOrder = "#sortOrder#"
	startRow = "#startRow#"
	openAsExcel = "false"
	numRowsPerPage="#numRowsPerPage#"
	ColumnTranslation="true"
    ColumnTranslationPrefix="phr_"
	ColumnHeadingList="Organisation,Name,Country,Currency,Start,End"
	showTheseColumns="Organisation,PricebookName,Country,currency,StartDate,EndDate"
	FilterSelectFieldList="#pricebookFilterSelectFieldList#"
	FilterSelectFieldList2="#pricebookFilterSelectFieldList#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	dateFormat="StartDate,EndDate"
	showCellColumnHeadings="no"
	keyColumnList="PricebookName"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(PricebookName)##'*comma'##jsStringFormat(PricebookName)##'*comma'/products/pricebooks.cfm?editor=yes&hideBackButton=true&pricebookID=##pricebookID###campaignURL#'*comma{reuseTab:true*commaiconClass:'products'});return false;"
	keyColumnOpenInWindowList="no"
	useInclude = "false"
	allowColumnSorting = "true"
	postSaveFunction = #application.getObject('products.com.pricebook').savePricebookEntries#
>

<cfif structKeyExists(url,"editor") and ((structKeyExists(url,"pricebookID") and url.pricebookID gt 0) or (structKeyExists(form,"pricebookID") and form.pricebookID gt 0))>
	<cfset pricebookID = structKeyExists(form,"pricebookID")?form.pricebookID:url.pricebookID>
	<cfparam name="openAsExcel" default="false">
	<cfparam name="startRow" default="1">
	<cfparam name="numRowsPerPage" default="50">
	<cfparam name="pricebookEntrySortOrder" default="SKU">
	<cfparam name="excelFileName" default="Pricebook_XXXXX_XXX">		<!--- the filename is overwritten with one with country and currency at point of export --->

	<div><a href="javascript:exportToFile()">Export Pricebook</a></div>

	<cfset getPriceBookEntries = application.getObject("products.com.pricebook").getPricebookEntriesForPricebook(pricebookID=pricebookID,sortOrder=pricebookEntrySortOrder)>

	<cfset currencyFormat = openAsExcel?"":"UnitPrice">

	<CF_tableFromQueryObject
		queryObject="#getPriceBookEntries#"
		sortOrder = "#pricebookEntrySortOrder#"
		startRow = "#startRow#"
		openAsExcel = "#openAsExcel#"
		excelFileName = "#excelFileName#"
		numRowsPerPage="#numRowsPerPage#"
		ColumnTranslation="true"
	    ColumnTranslationPrefix="phr_"
		ColumnHeadingList="SKU,product_description,Price"
		showTheseColumns="SKU,ProductTitle,UnitPrice"
		numberFormatMask="decimal"
		currencyformat="#currencyformat#"
		priceFromCurrencyColumn = "currency"
		showCellColumnHeadings="no"
		useInclude = "false"
		allowColumnSorting = "true"
		checkRights="true"
	>
</cfif>