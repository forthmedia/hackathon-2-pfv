<!---
  --- pricebook
  --- ---------
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   01/02/16
  --->
<cfcomponent accessors="true" output="false" persistent="false">

	<cffunction name="getPricebookOrganisations" access="public" validValues="true" output="false" returnType="query">
		<cfargument name="pricebook" type="any" required="true">

		<cfset var pricebookOrganisationID=0>
		<cfset var qryOrganisations = "">

		<cfif arguments.pricebookId neq 0>
			<cfquery name="getPricebookOrganisation">
				select organisationID from pricebook where pricebookId=<cf_queryparam value="#pricebookID#" cfsqltype="cf_sql_integer">
			</cfquery>
			<cfset pricebookOrganisationID = getPricebookOrganisation.organisationID[1]>
		</cfif>
		<cfset var vendorOrganisationID = application.com.settings.getSetting("theClient.clientOrganisationId")>

		<cfquery name="qryOrganisations">
			<cfif (application.com.settings.getSetting("leadManager.vendorPricebookSource") eq "CSV") OR (pricebookOrganisationID eq vendorOrganisationID) >
				SELECT OrganisationId,OrganisationName
				FROM  Organisation
				WHERE OrganisationId =  <cf_queryparam value="#vendorOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				UNION
			</cfif>
			SELECT organisation.OrganisationID, organisation.OrganisationName
			FROM   organisation INNER JOIN organisationType ON organisation.organisationTypeID = organisationType.organisationTypeID
			WHERE  (organisationType.TypeTextID = 'distributor')
		</cfquery>

		<cfreturn qryOrganisations>

	</cffunction>


	<cffunction name="savePricebookEntries" access="public" output="false" returnType="struct">
		<cfargument name="pricebookID" type="numeric" required="true">

		<cfset var result = {isOK=true,message=""}>
		<cfset var invalidProductList = "">
		<cfset var caughtError = {}>

		<cftransaction action="begin">

				<cftry>

				<!--- expire existing pricebooks --->
				<cfquery name="updateCurrentPricebook" result="updateResult">
					UPDATE Pricebook SET EndDate = #DateAdd('d',-1,form.StartDate)#
					WHERE PricebookID
					IN(
						SELECT PricebookID FROM Pricebook
						WHERE organisationID =  <cf_queryparam value="#form.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND CountryID =  <cf_queryparam value="#form.CountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND Currency =  <cf_queryparam value="#form.Currency#" CFSQLTYPE="CF_SQL_VARCHAR" >
						AND CampaignID=<cf_queryparam value="#form.CampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >					<!--- 2012/06/12 PPB Case 428390 only expire the pricebook for the same campaign --->
						AND (EndDate IS NULL OR (EndDate >=  <cf_queryparam value="#form.StartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND EndDate >= GETDATE()))
						<cfif arguments.PricebookID neq 0>
							AND PricebookID <>  <cf_queryparam value="#arguments.PricebookID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfif>
					)
				</cfquery>

				<cfif PricebookID gt 0 and FORM.FileName neq "">
					<cfset var newFileName = replace(listlast(FORM.FileName,"\"),".tmp",".csv","ALL")>
					<!--- NJH 2010/09/10 LID 3987 --->
					<cfset var fullPath="#application.paths.userfiles#\datatransfers\import\">
					<cfif not directoryExists(fullPath)>
						<cfset application.com.globalFunctions.CreateDirectoryRecursive(fullPath=fullPath)>
					</cfif>
					<cffile action="move" source="#FORM.FileName#" destination ="#fullPath##newFileName#">

					<cfset var pricebookImportFile = application.com.dataTransfers.fileToDB(dir="/dataTransfers/Import",filename=newFileName,Delim=",",Tablename="xNotUsed",sourceCollectionMethod="http",ColumnNames= "true",nocheckloaddata="false",charset="UTF-8")>
					<cffile action="delete" file="#application.paths.userfiles#\datatransfers\import\#newFileName#">

					<!--- if the file contains data, truncate existing data --->
					<cfif pricebookImportFile.numRowsInserted gt 0>
						<cfset var removeOldPriceBookData = "">
						<cfquery name="removeOldPriceBookData">
							delete from PricebookEntry where pricebookid =  <cf_queryparam value="#form.PricebookID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfquery>
					</cfif>

					<cfset var qryImported = pricebookImportFile.importQuery>
					<cfset var invalidProductList = "">
					<cfset var phraseTextIDList = "">

					<cfloop query="qryImported">
						<cfset application.com.request.extendTimeOutIfNecessary(60)>

						<cfset var importedSKU = trim(qryImported.SKU)>
						<cfset var importedProductDescription = trim(qryImported.Description)>
						<cfset var importedProductGroupDescription = StructKeyExists(qryImported,"ProductGroup")?trim(ProductGroup):"">

						<cfif importedSKU neq "">							<!--- 2012/06/12 PPB Case 428390 don't bother reporting on blank rows in the spreadsheet --->

							<!--- 2012/06/14 PPB Case 428390
							lookup the SKU in the product table
							- if it exists add a pricebook entry for it
							- if it doesn't exist add the product first BUT we can only add the product if we the user have been sent a valid productGroup to put it in
							 --->

							<cfset var invalidProduct = false>
							<cfset var importedProductGroupId = 0>
							<cfif importedProductGroupDescription neq "">

								<!--- NJH 2013/06/04 - match on productGroup translations --->
								<cfset var qryProductGroup = "">
							    <cfquery name="qryProductGroup">
								   select distinct entityID as productGroupID from (
								 	 	select phraseText,entityID from phrases p inner join phraseList pl on p.phraseId = pl.phraseID
								 		where pl.entityTypeID=#application.entityTypeID.productGroup#
								 			and contains(phraseText,<cf_queryparam value="""#trim(ProductGroup)#""" CFSQLTYPE="CF_SQL_VARCHAR" >)
								 			and phraseTextID in ('title','description')
								 	)
								 	as filtered
								 	where phraseText= <cf_queryparam value="#trim(ProductGroup)#" CFSQLTYPE="CF_SQL_VARCHAR" >
							    </cfquery>

							  	<cfif qryProductGroup.recordCount gt 0>
									<cfset importedProductGroupId = qryProductGroup.ProductGroupID[1]>
								</cfif>
							</cfif>

							<cfset var qryProduct = "">
						    <cfquery name="qryProduct">
							    SELECT ProductID
							    FROM Product
							    WHERE SKU =  <cf_queryparam value="#importedSKU#" CFSQLTYPE="CF_SQL_VARCHAR" >
							    AND Deleted=0
							    AND campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >
							    <cfif importedProductGroupId gt 0>	<!--- if they have provided a ProductGroup in the csv we should use it (despite the fact that if they don't provide it we just care about the SKU) --->
							    	AND productGroupID =  <cf_queryparam value="#importedProductGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfif>
						    </cfquery>

							<cfset var productID = 0>
						    <cfif qryProduct.RecordCount gt 0>
								<cfset productID = qryProduct.productID>
							<cfelse>
								<!--- if the product doesn't exist and we have sent in a valid productgroup, add the product --->
								<cfif importedProductGroupId gt 0>
									<cfset var insertProduct = "">
								  	<!--- if we have a valid productGroup, add the product --->
								    <cfquery name="insertProduct">
									    INSERT INTO Product (CampaignID,productGroupID,SKU,createdBy,lastupdatedBy,lastUpdatedByPerson)
									    VALUES (
									    	<cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >,
									    	<cf_queryparam value="#qryProductGroup.productGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
									    	<cf_queryparam value="#importedSKU#" CFSQLTYPE="CF_SQL_VARCHAR" >,
									    	<cf_queryparam value="#request.relaycurrentuser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,
									    	<cf_queryparam value="#request.relaycurrentuser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,
									    	<cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="cf_sql_integer" >);
									    select scope_identity() as newProductID
								    </cfquery>

							    	<cfset productID = insertProduct.newProductID>
							    	<cfset var addResult = application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID="title",entityTypeID=application.entityTypeID.product,entityid=productID,phraseText=trim(importedProductDescription),updateCluster=false)>
							    	<cfset var phraseTextIDList = listAppend(phraseTextIDList,addResult.phraseTextID)>
							    	<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID="description",entityTypeID=application.entityTypeID.product,entityid=productID,phraseText=trim(importedProductDescription),updateCluster=false)>
							    	<cfset var phraseTextIDList = listAppend(phraseTextIDList,addResult.phraseTextID)>
								<cfelse>
									<!--- if we don't have a productGroup sent in the csv, mark it to be added to the list of invalidProducts --->
									<cfset invalidProduct = true>
								</cfif>
						   	</cfif>

						    <cfif invalidProduct>
								<cfset invalidProductList = listAppend(invalidProductList," " & importedSKU)>
							<cfelse>
								<cfset var insertPricebookEntries = "">
							    <cfquery name="insertPricebookEntries">
								    INSERT INTO PricebookEntry (PricebookID, productID, unitPrice, createdBy, lastupdatedBy,lastUpdatedByPerson)
								    VALUES (
								    <cf_queryparam value="#PricebookID#" CFSQLTYPE="CF_SQL_INTEGER" >,
								    <cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" >,
								    <cf_queryparam value="#application.com.regexp.removeNumberFormatting(Price)#" CFSQLTYPE="cf_sql_float" >,
								    #request.relaycurrentuser.userGroupId#,
								    #request.relaycurrentuser.userGroupId#,
								    #request.relaycurrentuser.personID#)
							    </cfquery>
							</cfif>

						</cfif>
					</cfloop>

					<cfif phraseTextIDList neq "">
						<cfset application.com.relayTranslations.resetPhraseStructureKey(phraseTextID=phraseTextIDList,updateCluster=true)>
					</cfif>
				</cfif>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.message = "Error during processing - pricebook not imported. Message: #cfcatch.message#">
					<cfset caughtError = cfcatch>
				</cfcatch>
			</cftry>

			<cfif result.isOK>
				<cftransaction action="commit">
			<cfelse>
				<cftransaction action="rollback">
			</cfif>
		</cftransaction>

		<cfif not result.isOK>
			<cfset application.com.errorHandler.recordRelayError_Warning(type="ColdFusion",Severity="error",caughtError=caughtError)>
		</cfif>

		<cfset var oldPricebooksUpdated=updateResult.recordcount>
		<cfif oldPricebooksUpdated gt 0>
			<cfoutput>#application.com.relayUI.message(message="phr_opp_PricebookAutomaticallyClosed (#oldPricebooksUpdated# phr_RowsAffected)", type="info")#</cfoutput>
		</cfif>

		<cfif request.relayCurrentUser.isInternal and invalidProductList neq "">
			<cfset var getCatalogueName = "">
			<cfquery name="getCatalogueName">
				select promoName from promotion where campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfoutput>#application.com.relayUI.message(message="The following products do not exist in the #getCatalogueName.promoName# and so could not be added to the pricebook:#invalidProductList#",type="error")#</cfoutput>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getPricebookListing" access="public" output="false" returnType="query">
		<cfargument name="campaignID" type="numeric" required="true">

		<cfset var getPriceBooks = "">

		<cfquery name="getPriceBooks">
			SELECT     Pricebook.pricebookID, Pricebook.Name AS PricebookName, Pricebook.organisationID, Pricebook.campaignID, Pricebook.countryID, Pricebook.currency,
				Pricebook.StartDate, Pricebook.EndDate, organisation.OrganisationName as organisation, Country.ISOCode, Country.CountryDescription as country,
			    CASE WHEN Pricebook.EndDate < GETDATE() Then '1' ELSE '0' END  AS showOldPricebooks
		 	FROM Pricebook
		 		INNER JOIN Country ON Pricebook.countryID = Country.CountryID
		 		INNER JOIN organisation ON Pricebook.organisationID = organisation.OrganisationID
			WHERE     (Pricebook.campaignID =  <cf_queryparam value="#arguments.campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				#application.com.rights.getRightsFilterWhereClause(entityType="Pricebook").whereClause#
			AND 	  (Pricebook.deleted != 1)
		</cfquery>

		<cfreturn getPriceBooks>
	</cffunction>


	<cffunction name="getPricebookEntriesForPricebook" access="public" output="false" returnType="query" hint="Returns the pricebook entries for a given pricebook in the pricebook editor">
		<cfargument name="pricebookID" type="numeric" required="true">
		<cfargument name="sortOrder" type="string" default="SKU">

		<cfset var getPriceBookEntries = "">

		<cfquery name="getPriceBookEntries">
			SELECT Product.SKU, Product.title_defaultTranslation AS ProductTitle,
				PricebookEntry.unitPrice, pricebook.currency
			FROM PricebookEntry
				INNER JOIN pricebook on pricebook.pricebookID = pricebookEntry.pricebookID
				inner join Product ON PricebookEntry.productID = Product.ProductID
			WHERE (Product.Deleted = 0)
				AND (PricebookEntry.deleted = 0)
				AND (PricebookEntry.pricebookID =  <cf_queryparam value="#arguments.pricebookID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			ORDER BY <cf_queryObjectName value="#arguments.sortOrder#">
		</cfquery>

		<cfreturn getPriceBookEntries>
	</cffunction>

</cfcomponent>