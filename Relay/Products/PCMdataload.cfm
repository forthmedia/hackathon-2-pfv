<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="500000" showdebugoutput="no" enablecfoutputonly="yes">
<!--- 
File name:		PCMdataload.cfm	
Author:			GCC
Date started:		2006-09-07
	
Description:		Attempt to genericise Sony XML import routines into relay XML import routines.
This file is used to manually start XML loads - based heavily on sonyuserfiles/content/sony1/PCMdataload.cfm

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


 --->

<cf_head>
	<cf_title>XML Product Import</cf_title>
</cf_head>


<!--- xmlloadCFCPath should be set in cftemplates/pcmINI.cfm --->
<cftry>
	<cfscript>
		loadCFC = createObject("component",request.xmlloadCFCPath);
	</cfscript>
	<cfcatch>
	<cfoutput><p class="errorblock">Please define an XML Import function in #request.xmlloadCFCPath#.</p></cfoutput><CF_ABORT>
	</cfcatch>
</cftry>

<cfparam name="url.action" default="">
<cfparam name="url.refreshFiles" default="false">
<cfif len(action)>
<cfswitch expression="#action#">
	<cfcase value="loadXMLfile">
		<cfparam name="url.propogateTreeChanges" default="false">
		<cfscript>
		success = loadCFC.loadTreeFromXML(languageCode=url.languageCode,
			countryCode=url.CountryCode,
			sourceid=url.Sourceid, 
			simulateProcess=false, 
			varPropogateTreeChanges = url.propogateTreeChanges);
		</cfscript>
	</cfcase>
	<cfcase value="XMLbuildNavTree">
		<cfscript>
		success = loadCFC.buildNavigationTreeFromDB(treeID=url.treeID);	
		</cfscript>
	</cfcase>

	<cfcase value="forceDelete">
		<cfscript>
			application.com.relayProductTree.forceDelete(treeID = url.treeID);
		</cfscript>	
	</cfcase>
	<cfcase value="deleteNavigationMemoryStructures">
		<cfscript>
			application.com.relayProductTree.deleteMemoryStructures();
		</cfscript>	
	</cfcase>
	
	<cfcase value="dumpNavTree">
		<cfdump var="#application.navstruct.navigation[url.treeID]#">
	</cfcase>
	
	<cfcase value="DBbuildNavTree">
		<cfscript>
		application.com.relayProductTree.buildNavigationTreeFromDB (treeID = url.treeID);
		</cfscript>
	</cfcase>
	<cfcase value="globalProcess">
		<cfscript>
			treeSetQry = application.com.relayProductTree.getTreeSet (
				ActiveOnly = 1);
				
		</cfscript>
		<!--- Loop over the set trees and get the set of trees and gather enough information to run the imports. --->
		<cfset BVSet = "">
		<cfset SPSet = "">
		<cfset USSet = "">
		<cfloop query="treeSetQry">
			<cfif treeSetQry.sourcegroupid eq "2">
				<cfset USSet = listappend(USSet,treeSetQry.treeID)>
			</cfif>
		</cfloop>
		
		<!--- Now loop over the user trees and determine which in active trees we need to process. --->
		<cfloop list="#USSet#" index="USTreeID">
			<cfscript>
				childTreeQry =  application.com.relayProductTree.getSourceTreesForTree (treeID = #USTreeID#);			
			</cfscript>
			<cfloop query="childTreeQry">
				<cfif findnocase("SCORE",childTreeQry.source)  and not listfindNocase(SPSet,childTreeQry.treeID)>
					<!--- GCC ignore SCORE for now - needs running out of hours --->
					<!--- <cfset SPSet = listappend(SPSet,childTreeQry.treeID)> --->
				<cfelseif findNocase( "BV",childTreeQry.source)  and not listfindNocase(BVSet,childTreeQry.treeID)>
					<cfset BVSet = listappend(BVSet,childTreeQry.treeID)>
					
				</cfif>
			</cfloop>
		</cfloop>
	</cfcase>
	
	<cfdefaultcase>
		<cfoutput><h1>#action# unknown.</h1></cfoutput>
	</cfdefaultcase>
</cfswitch>
</cfif>
<!--- 
<h2>GLOBAL FUNCTIONS</h2>
This set of functions summarise small steps that can be taken below AND take a <strong>LONG</strong> time to run. Anything up to <strong>1 hour</strong>. Be patient and be very careful when running these functions. <br>
 --->
 <cfparam name="url.loadMethod" default="manual">
<cfif url.loadMethod eq "manual">
	<cfoutput><h2>KNOWN XML DATA FILES</h2>
		<cfscript>
			spiceFileSet = loadCFC.getSpiceFileSet(filepathprefix= request.filepathprefix);
		</cfscript>
	</cfoutput>
	
	<cfloop collection="#spiceFileSet#" item="langCode">
		<cfloop collection="#spiceFileSet[langCode]#" item="CountryCode">
			<cfloop collection="#spiceFileSet[langCode][CountryCode]#" item="Sourceid">
				
				<!--- Using the information, get the tree information. --->
				<cfscript>
				treeQry = loadCFC.selectTree ( languageCode=langCode,countryCode=CountryCode,sourceid=Sourceid);
				</cfscript>
				<cfscript>
					SourceQry=loadCFC.selectSource(sourceid=Sourceid);
				</cfscript>
				<cfoutput>
				 #htmleditformat(langCode)# #htmleditformat(CountryCode)# #htmleditformat(SourceQry.source)# #htmleditformat(treeQry.lastupdated)# <a href="PCMdataload.cfm?action=loadXMLfile&treeID=#treeQry.treeID#&languageCode=#langCode#&countryCode=#CountryCode#&sourceid=#Sourceid#&RequestTimeOut=3600" title="">XML Build</a> <a href="PCMdataload.cfm?action=loadXMLfile&treeID=#treeQry.treeID#&languageCode=#langCode#&countryCode=#CountryCode#&sourceid=#Sourceid#&propogateTreeChanges=true" title="">XML Build and Propogate</a><cfif len(treeQry.lastupdated)> <a href="PCMdataload.cfm?action=DBbuildNavTree&treeID=#treeQry.treeID#">DB Build</a><cfelse>New</cfif><br>
				</cfoutput>
			</cfloop>
		</cfloop>
	</cfloop>
	
	<cfoutput>
	<cfif isdefined ("application.navstruct.navigation")>
	<h2>Navigation Trees</h2> <a href="?action=deleteNavigationMemoryStructures">Del Nav Struct</a><br>
		<cfloop collection="#application.navstruct.navigation#" item="navidx">
			<a href="?action=dumpNavTree&treeID=#navidx#">Dump #htmleditformat(navidx)#</a>&nbsp;
		</cfloop>
	</cfif>

	<h2>Image Refresh</h2> 
	Occurs Weekly on Mondays at 7am automatically. Can be forced manually by <a href="<cfoutput></cfoutput>/scheduled/scheduledPCMimageRefresh.cfm?ImageRefresh=yes" target="_blank">clicking here</a>.
	</cfoutput>
</cfif>

