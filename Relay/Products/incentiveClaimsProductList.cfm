<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NJH 2009/03/12	CR-SNY675	- added module param so that the file could be used for both cashback and incentive modules as both have a claims catalogue.
NJH 2009/05/20	P-SNY069	 - undid some changes from last CR as they were no longer needed. Param'd campaignID.
 --->

<!--- NJH 2009/05/20 P-SNY069 - changed to be a param --->
<cfparam name="campaignID" type="numeric" default="#application.com.settings.getSetting('incentive.defaultClaimsProductCatalogueCampaignID')#">

<cf_include template="/products/products.cfm">