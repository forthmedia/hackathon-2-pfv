<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="5000" enablecfoutputonly="No">
<!---
The treeEditor will allow you to edit the current specified tree.
WOtreeid mandatory
WFtreeID
--->
<cfoutput>
	<link rel="StyleSheet" href="/styles/PCMcattree.css" type="text/css"/>
	<script type="text/javascript" src="/javascript/openWin.js"></script>
	<script type="text/javascript" src="/products/js/PCMcatTree.js"></script>
</cfoutput>

<script language="JavaScript">
	// Form Manipulation.
	var currentWOCategory = null;
	var currentWFCategory = null;
	function tree_DeleteWOCategory (categoryID){
		document.WOtree.WOCategoryID.value =categoryID;
		document.WOtree.TreeAction.value= 'DeleteCategory';
		document.WOtree.submit();
	}
	function tree_setWOCategoryID (categoryID){
		if (categoryID) document.WOtree.WOCategoryID.value = categoryID;
		else document.WOtree.WOCategoryID.value = '';
	}
	function tree_setWFCategoryID (categoryID){
		document.WOtree.WFCategoryID.value = categoryID;
	}
	function tree_AddWFCategoryToWO(WFCategoryID,CopyChildren,CopyProducts){
		document.WOtree.WFCategoryID.value =WFCategoryID;
		document.WOtree.CopyChildren.value =CopyChildren;
		document.WOtree.CopyProducts.value =CopyProducts;
		document.WOtree.TreeAction.value= 'AddWFCategoryToWO';
		document.WOtree.submit();
	}
	function tree_AddNewCategoryToWO(WOCategoryID){
		var categoryName= prompt('Please enter the category name', '');
		if (categoryName != null){
		if (WOCategoryID)document.WOtree.WOCategoryID.value = WOCategoryID;
		else document.WOtree.WOCategoryID.value = '';
		document.WOtree.CategoryName.value = categoryName;
		document.WOtree.TreeAction.value= 'AddNewCategoryToWO';
		document.WOtree.submit();}
	}
	function tree_ChangeProductStatus(categoryId,productID,productStatus){
		document.WOtree.WOCategoryID.value =categoryId;
		document.WOtree.productID.value = productID;
		document.WOtree.status.value= productStatus;
		document.WOtree.TreeAction.value= 'changeProductStatus';
		document.WOtree.submit();
	}
	function tree_CopyWFProduct (categoryID,productKey){
		if (document.WOtree.WOCategoryID.value != ''){
			document.WOtree.WFCategoryID.value = categoryID;
			document.WOtree.productID.value = productID;
			document.WOtree.TreeAction.value= 'CopyProduct';
			document.WOtree.submit();
		}
		else alert('Working Category has not been specified.');
	}
	function tree_editWOCategoryName(WOCategoryID){
		var categoryName= prompt('Please enter the new category name.', '');
		if (categoryName != null){
		if (WOCategoryID)document.WOtree.WOCategoryID.value = WOCategoryID;
		else document.WOtree.WOCategoryID.value = '';
		document.WOtree.CategoryName.value = categoryName;
		document.WOtree.TreeAction.value= 'EditWOCategoryName';
		document.WOtree.submit();}
	}
	function tree_ChangeProductPromotionStatus(categoryId,productID,promotion){
		document.WOtree.WOCategoryID.value =categoryId;
		document.WOtree.productID.value = productID;
		document.WOtree.status.value= promotion;
		document.WOtree.TreeAction.value= 'ChangeProductPromotionStatus';
		document.WOtree.submit();}

//CR_SNY535 - GCC - launch category image chooser + product ordering START
	function tree_editWOCategoryImage(WOCategoryID){
		winLink = 'PCMtreeCategoryImagePicker.cfm?nodeID=' + WOCategoryID + '&WotreeID=' + <cfoutput>#jsStringFormat(url.WOtreeID)#</cfoutput>;
		newWindow = openWin( winLink,'PopUp','width=680,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
		// newWindow.focus() removed WAB 2005-12-19 - openWin function does focus and doesn't return the object
		}

	function tree_changeUserOrder(categoryID,productID){
		var userOrder = prompt('Enter a new position for this product (whole numbers only)', '');
		if (userOrder != null){
		var newstring = parseInt(userOrder).toString();
			if (userOrder.length == newstring.length && newstring != "NaN" && userOrder > 0) {
				document.WOtree.WOCategoryID.value = categoryID;
				document.WOtree.userOrder.value = userOrder;
				document.WOtree.productID.value = productID;
				document.WOtree.TreeAction.value= 'changeUserOrder';
				document.WOtree.submit();
			} else {
			alert("Enter a whole number greater than zero");
			}
		}
	}
	function tree_changeCategoryOrder(categoryID){
		var userOrder = prompt('Enter a new position for this category (whole numbers only)', '');
		if (userOrder != null){
		var newstring = parseInt(userOrder).toString();
			if (userOrder.length == newstring.length && newstring != "NaN" && userOrder > 0) {
				document.WOtree.WOCategoryID.value = categoryID;
				document.WOtree.userOrder.value = userOrder;
				document.WOtree.TreeAction.value= 'changeCategoryOrder';
				document.WOtree.submit();
			} else {
			alert("Enter a whole number greater than zero");
			}
		}
	}
//CR_SNY535 - GCC - launch category image chooser + product ordering END
</script>
<cfsetting enablecfoutputonly="No">
<cfparam name="url.WFtreeID" default="">
<cfparam name="url.WOtreeid" default="">
<cfset attributes.WOTreeID = url.WOtreeID>
<cfset attributes.WFTreeID = url.WFtreeID>
<!--- Process any outstanding tree actions. --->
<cfinclude template="PCMtreeActions.cfm">
<a href="PCMtreeManagement.cfm">Return to Tree Management <<</a>

<cfset attributes.TreeID = attributes.WFTreeID>
<!--- WF specific functions. Primarily the ability to switch to different trees.--->
<cfscript>
treesQry = application.com.relayProductTree.getTreeSet(sourcegroupid=1);
</cfscript>
<!--- Display the trees to allow the selection of the 'working from' tree. --->

<table width="100%" bgcolor="#F4F4F4" class="cattree">
<tr>
<td colspan="4"><strong>Select DATA Tree to work from:</strong><br>
<table>
<tr>
<cfoutput query="treesQry">
	<td class="cattree"><a  href="PCMtreeEdit.cfm?WFTreeID=#TreeID#&WOTreeID=#attributes.WOTreeID#">
<cfif treesQry.treeid eq attributes.WFTreeID>
<img src="/images/PCMicons/edit.gif" border="0" alt="Currently being worked on.">
<cfelse>
<img src="/images/PCMicons/delete.gif" border="0" alt="Select to work on.">
</cfif> #htmleditformat(title)#</a></td>
<cfif not treesQry.currentrow mod 5></tr><tr></cfif>
</cfoutput>
</tr>
</table>
</td>

</tr>
<cfoutput>
<tr><td colspan="2"><a href="javascript:location.reload(true)">Click here</a> to refresh the tree display to reflect category image changes</td></tr>
<tr><th width="50%">DATA Tree Working From</th>
<td nowrap><form action="PCMtreeEdit.cfm" method="get" name="WOtree"><CF_INPUT type="hidden" name="WOTreeID" value="#attributes.WOTreeID#"><CF_INPUT type="hidden" name="WFTreeID" value="#attributes.WFTreeID#">WO CAT ID:<input type="text" name="WOCategoryID" size="2" value="" readonly><input type="hidden" name="WFCategoryID"  value="" >
<input type="hidden" name="userOrder" value=""  ><input type="hidden" name="TreeAction" value=""  ><input type="hidden" name="WFCategoryName" value="" ><input type="hidden" name="WOCategoryName" value="" ><input type="hidden" name="CategoryName" value="" ><input type="hidden" name="CopyChildren" value="" ><input type="hidden" name="CopyProducts" value="" ><input type="hidden" name="status" value="" ><input type="hidden" name="productID" value="" ></form></td>
<th width="50%">USER Tree Working On</th>
</tr>
</cfoutput>

<tr valign="top"><td align="left" width="50%">
<img src="images/spacer.gif" width="300" height="1" border="0">
<!--- Display the working from tree. --->
<cfif len(url.WFtreeID)>
	<cfscript>
	WFtreeQry = application.com.relayProductTree.selectTree(
		treeID="#trim(url.WFtreeID)#",
		sourcegroupid=1);
	</cfscript>
	<cfset attributes.treeType = "WF">
	<cfset attributes.title = WFtreeQry.title>
	<cfset attributes.treeID = url.WFtreeID>

	<cfinclude template="PCMtreeEditDisplay.cfm">

<cfelse>
	NOT DEFINED
</cfif>
</td>
<td class="cattree" width="100"><!--- Working Panel. --->



</td>
<td align="left" width="50%">
<img src="images/spacer.gif" width="300" height="1" border="0">
<cfif len(url.WOtreeID)>
	<cfscript>
	WOtreeQry = application.com.relayProductTree.selectTree(
		treeID="#trim(url.WOtreeID)#",
		sourcegroupid="2");
	</cfscript>
	<!--- Display the working on tree. --->
	<cfset attributes.treeType = "WO">
	<cfset attributes.title = WOtreeQry.title>
	<cfset attributes.treeID = url.WOtreeID>
	<cfinclude template="PCMtreeEditDisplay.cfm">
<cfelse>
	USE ERROR<CF_ABORT>
</cfif>
</td>
</tr>
</table>
<cfif isdefined("session.treeErrorMessage") and len(session.treeErrorMessage)>
<script>
<cfoutput>
alert ('#jsStringFormat(session.treeErrorMessage)#');
</cfoutput>
</script>
<cfset session.treeErrorMessage = "">

</cfif>

<!---
<cfinclude template="dealernet_product_footerV2.cfm"> --->

