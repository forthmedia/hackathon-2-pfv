<!--- �Relayware. All Rights Reserved 2014 --->
<!--- editTreeDisplayV2.cfm
This gets the set of categories for a tree and displays them. Please note that the system works by assuming the whole of the system is wrapped inside a form.
 --->
<cfif len(attributes.TreeID)>
	<cfset productCount = 0>
	<!--- having to clear out the temporary store
	cant do ti in selectTreeCategories because it self calls
	should really be a temp table but cftran should stop 
	any potential contamination appearing... --->
	<cftransaction>
		<cfquery name="tidyup" datasource="#application.siteDataSource#">	
		DELETE FROM PCMproductTempCategoryTable
		where treeID =  <cf_queryparam value="#attributes.TreeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<cfscript>
		treeCategoriesQry = application.com.relayProductTree.selectTreeCategories(
			treeID="#attributes.TreeID#");
		</cfscript>
	</cftransaction>

	<script type="text/javascript">
	<!--
	<cfoutput>
	#attributes.treetype# = new catTree('#attributes.treetype#');

	#attributes.treetype#.add(0,-1,'#jsstringformat(attributes.title)#');
	</cfoutput>
	<cfset CategoryCount = 0>
	
	<cfoutput query="treeCategoriesQry">
		<cfif treeCategoriesQry.currentrow gt 1>
			<cfset lastrow = treeCategoriesQry.currentrow-1>
			<cfset lastParentCategoryID = treeCategoriesQry.parentCategoryID[lastrow]>
			<cfif lastParentCategoryID neq treeCategoriesQry.parentCategoryID>
				<cfset CategoryCount = 0>
			</cfif>
		</cfif>
		<cfset CategoryCount = CategoryCount + 1>
		#attributes.treetype#.add(#categoryID#,#val(ParentCategoryID)#,'(#CategoryCount#) #jsstringformat(listlast(CategoryName,"|"))#','','','','/images/PCMicons/folder.gif','','','#attributes.treetype#Category','#categoryID#','#categoryImageStatus#');
		<cfscript>
		productsQry = application.com.relayProductTree.selectCategoryProducts(
			treeID="#attributes.TreeID#",
			CategoryID="#CategoryID#");
		</cfscript>
		<cfif productsQry.recordcount>
				<cfloop query="productsQry">
				<!--- CR_SNY536 - GCC - START --->
					<cfset hasSpiceAccessories = 0>
				<!--- CR_SNY535 - GCC - START --->
					#attributes.treetype#.add(-#productsQry.productID#,#productsQry.CategoryID#,'(#productsQry.currentrow#) #productsQry.productKey#-#jsstringformat(replace(productsQry.title,"'","","ALL"))# [#productsQry.countryCode# #productsQry.languageCode# #productsQry.sourceid#] <cfif hasSpiceAccessories>[Acc=#qhasSpiceAccessories.recordcount#]</cfif>','','#productsQry.processStatus#','','/images/PCMicons/product#productsQry.processStatus#.gif','img/product#productsQry.processStatus#.gif','','#attributes.treetype#Product#productsQry.processStatus#','#productsQry.categoryID#','','#val(productsQry.promotion)#');
				<!--- CR_SNY535 - GCC - END --->
				<!--- CR_SNY536 - GCC - END --->
				</cfloop>
		</cfif>
	</cfoutput>
	<cfoutput>
	document.write(#attributes.treetype#);</cfoutput>
	//-->
	</script>
<cfelse>
	No Tree Defined.
</cfif>