<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="600" enablecfoutputonly="No">

<!---
File name:		CSVdataload.cfm
Author:			AJC
Date started:		2006-10-18

Description:		Manual use file to import CSV product data originally for Lexmark Service Claims into products.
NOT related in anyway to PCM product Data

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


 --->

<cf_head>
	<cf_title>CSV Product Import</cf_title>
</cf_head>


<!--- xmlloadCFCPath should be set in cftemplates/pcmINI.cfm --->
<!--- <cfscript>
	loadCFC = createObject("component",request.csvloadCFCPath);
</cfscript> --->
<!--- <cfdump var="#structKeyList(application.spicexml)#"> --->
<cfparam name="url.action" default="">
<cfparam name="form.action" default="">
<cfparam name="variables.action" default="">
<cfparam name="url.refreshFiles" default="false">

<cfif form.action is not "">
	<cfset variables.action=form.action>
<cfelse>
	<cfset variables.action=url.action>
</cfif>

<cfswitch expression="#variables.action#">

	<cfcase value="uploadform">
		<h1>Upload CSV Import File</h1>
		<form action="CSVdataload.cfm" method="post" enctype="multipart/form-data" name="frmCSVUpload" id="frmCSVUpload">
			<table>
				<tr>
					<td>Select File:</td>
					<td><input type="file" name="filCSVFile" id="filCSVFile" accept="text/csv">*</td>
				</tr>
				<tr>
					<td>Select Type:</td>
					<td>
						<select name="cboCSVType">
							<option value="parts">Parts</option>
							<option value="products">Products</option>
							<option value="failure_codes">Failure Codes</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input type="submit" name="submit" value="Upload">
						<input type="hidden" name="action" value="uploadfile">
					</td>
				</tr>
				<tr>
					<td colspan="2">* If the filename of the CSV upload already exists then it will overwrite.</td>
				</tr>
			</table>
		</form>
	</cfcase>

	<cfcase value="uploadfile">
		<cfparam name="form.filCSVFile" default="">
		<cfparam name="form.cboCSVType" default="">
		<cftry>
			<cffile action="UPLOAD" filefield="form.filCSVFile" destination="#request.csvfilepathprefix#\#form.cboCSVType#\" nameconflict="OVERWRITE">
			<h1>CSV File has been Uploaded</h1>
			<p>Please select an option below...</p>
			<p><a href="CSVdataload.cfm?action=importselect">Import CSV data</a></p>
			<p><a href="CSVdataload.cfm?action=uploadform">Upload another CSV file</a></p>
			<p><a href="CSVdataload.cfm">Go back to the Main Menu</a></p>
			<cfcatch>
				<h1>CSV File Upload Error</h1>
				<p>There has been are error trying to upload the file.</p>
				<p><a href="CSVdataload.cfm?action=uploadform">Try again</a></p>
				<p><a href="CSVdataload.cfm">back to menu</a></p>
			</cfcatch>
		</cftry>
	</cfcase>

	<cfcase value="importselect">
		<script type='text/javascript' src='/mxajax/core/js/prototype.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxAjax.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxSelect.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxData.js'></script>

		<SCRIPT type="text/javascript">
			<cfoutput>var url = "#request.currentSite.httpProtocol##jsStringFormat(CGI.HTTP_HOST)#/WebServices/mxAjaxClaims.cfc";</cfoutput>

			function init() {
				new mxAjax.Select({
					parser: new mxAjax.CFArrayToJSKeyValueParser(),
					executeOnLoad: true,
					target: "cbofilelist",
					paramArgs: new mxAjax.Param(url,{param:"CSVType={cboCSVType}", cffunction:"getCSVList"}),
					source: "cboCSVType"
				});
			}

			addOnLoadEvent(function()
			{
				init();
			});

			function Final_Checks()
			{
				if (document.frmCSVSelect.cbofilelist.value=="")
				{
					alert('phr_error_claims_no_csv_file');
					return false;
				}
			}
		</script>

		<h1>Select Uploaded CSV Import file</h1>
		<form action="CSVdataload.cfm" method="post" enctype="multipart/form-data" name="frmCSVUpload" id="frmCSVSelect" onSubmit="return Final_Checks();">
			<table>
				<tr>
					<td>CSV Type:</td>
					<td>
						<select name="cboCSVType">
							<option value="parts">Parts</option>
							<option value="products">Products</option>
							<option value="failure_codes">Failure Codes</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>File:</td>
					<td>
						<select name="cbofilelist">
							<option value=""></option>
						</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input type="submit" name="submit" value="Import">
						<input type="hidden" name="action" value="importfile">
					</td>
				</tr>
			</table>
		</form>
	</cfcase>
	<cfcase value="importfile">
		<cfparam name="form.cboCSVType" default="">
		<cfparam name="form.cbofilelist" default="">

		<h1>Import File - <cfoutput>#Replace(form.cboCSVType, "_", " ", "ALL")#</cfoutput></h1>

		<cfoutput>
			#htmleditformat(form.cbofilelist)#<br>
		</cfoutput>

		<cfscript>
			variables.successful=application.com.lexmarkProductCSVload.importcsv(loadtype="#form.cboCSVType#",CSVfile="d:\web\lexmarkUserFiles\content\ClaimsProductData\datafiles\#form.cboCSVType#\#form.cbofilelist#");
		</cfscript>

		<cfif variables.successful is "true">
			<p>Import Successful<br>
				<a href="CSVdataload.cfm">back to menu</a><br/>
			</p>
		<cfelse>
			<p>Not Done<br>
				<a href="CSVdataload.cfm">back to menu</a><br/>
			</p>
		</cfif>

	</cfcase>
	<cfcase value="deleteselect">
		<script type='text/javascript' src='/mxajax/core/js/prototype.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxAjax.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxSelect.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxData.js'></script>

		<SCRIPT type="text/javascript">
			<cfoutput>var url = "#request.currentSite.httpProtocol##jsStringFormat(CGI.HTTP_HOST)#/WebServices/mxAjaxClaims.cfc";</cfoutput>

			function init() {
				new mxAjax.Select({
					parser: new mxAjax.CFArrayToJSKeyValueParser(),
					executeOnLoad: true,
					target: "cbofilelist",
					paramArgs: new mxAjax.Param(url,{param:"CSVType={cboCSVType}", cffunction:"getCSVList"}),
					source: "cboCSVType"
				});
			}

			addOnLoadEvent(function()
			{
				init();
			});

			function Final_Checks()
			{
				if (document.frmCSVSelect.cbofilelist.value=="")
				{
					alert('phr_error_claims_no_csv_file');
					return false;
				}
			}
		</script>

		<h1>Select Uploaded CSV Import file to DELETE</h1>
		<form action="CSVdataload.cfm" method="post" enctype="multipart/form-data" name="frmCSVUpload" id="frmCSVSelect" onSubmit="return Final_Checks();">
			<table>
				<tr>
					<td>CSV Type:</td>
					<td>
						<select name="cboCSVType">
							<option value="parts">Parts</option>
							<option value="products">Products</option>
							<option value="failure_codes">Failure Codes</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>File:</td>
					<td>
						<select name="cbofilelist">
							<option value=""></option>
						</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input type="submit" name="submit" value="Delete">
						<input type="hidden" name="action" value="deletefile">
					</td>
				</tr>
			</table>
		</form>
	</cfcase>
	<cfcase value="deletefile">
		<cfparam name="form.cbofilelist" default="">
		<cfparam name="form.cboCSVType" default="">
		<cftry>
			<cffile action="DELETE" file="#request.csvfilepathprefix#\#form.cboCSVType#\#form.cbofilelist#">
			<h1>CSV File has been DELETED</h1>
			<p>Please select an option below...</p>
			<p><a href="CSVdataload.cfm?action=importselect">Import CSV data</a></p>
			<p><a href="CSVdataload.cfm?action=uploadform">Upload CSV file</a></p>
			<p><a href="CSVdataload.cfm?action=deleteselect">Delete another CSV file</a></p>
			<p><a href="CSVdataload.cfm">Go back to the Main Menu</a></p>
			<cfcatch>
				<h1>CSV File Delete Error</h1>
				<p>There has been are error trying to delete the file.</p>
				<p><a href="CSVdataload.cfm?action=uploadform">Try again</a></p>
				<p><a href="CSVdataload.cfm">back to menu</a></p>
			</cfcatch>
		</cftry>
	</cfcase>
	<cfdefaultcase>
		<cfoutput><h1>Services Printer Parts Import Menu</h1></cfoutput>
		<p>Please select an option from the list below</p>
		<p><a href="CSVdataload.cfm?action=uploadform">Upload CSV File</a></p>
		<p><a href="CSVdataload.cfm?action=deleteselect">Delete CSV File</a></p>
		<p><a href="CSVdataload.cfm?action=importselect">Import CSV File</a></p>
	</cfdefaultcase>

</cfswitch>

<!---
<h2>GLOBAL FUNCTIONS</h2>
This set of functions summarise small steps that can be taken below AND take a <strong>LONG</strong> time to run. Anything up to <strong>1 hour</strong>. Be patient and be very careful when running these functions. <br>
 --->


