<!--- �Relayware. All Rights Reserved 2014 --->
<cf_translate>

<cfparam name="variables.cboSiteDefID" default="">
<cfparam name="url.cboSiteDefID" default="">
<cfparam name="form.cboSiteDefID" default="">
<cfparam name="variables.cboSourceID" default="">
<cfparam name="url.cboSourceID" default="">
<cfparam name="form.cboSourceID" default="">
<cfparam name="variables.cboRemoteCategoryID" default="">
<cfparam name="url.cboRemoteCategoryID" default="">
<cfparam name="form.cboRemoteCategoryID" default="">
<cfparam name="variables.cbosectiontypeID" default="">
<cfparam name="url.cbosectiontypeID" default="">
<cfparam name="form.cbosectiontypeID" default="">
<cfparam name="variables.cbomasktypes" default="">
<cfparam name="url.cbomasktypes" default="">
<cfparam name="form.cbomasktypes" default="">

<cfif form.cboSiteDefID neq "">
	<cfset variables.cboSiteDefID=form.cboSiteDefID>
<cfelseif url.cboSiteDefID neq "">
	<cfset variables.cboSiteDefID=url.cboSiteDefID>
</cfif>

<cfif form.cboSourceID neq "">
	<cfset variables.cboSourceID=form.cboSourceID>
<cfelseif url.cboSourceID neq "">
	<cfset variables.cboSourceID=url.cboSourceID>
</cfif>

<cfif form.cboRemoteCategoryID neq "">
	<cfset variables.cboRemoteCategoryID=form.cboRemoteCategoryID>
<cfelseif url.cboRemoteCategoryID neq "">
	<cfset variables.cboRemoteCategoryID=url.cboRemoteCategoryID>
</cfif>

<cfif form.cbosectiontypeID neq "">
	<cfset variables.cbosectiontypeID=form.cbosectiontypeID>
<cfelseif url.cbosectiontypeID neq "">
	<cfset variables.cbosectiontypeID=url.cbosectiontypeID>
</cfif>

<cfif form.cbomasktypes neq "">
	<cfset variables.cbomasktypes=form.cbomasktypes>
<cfelseif url.cbomasktypes neq "">
	<cfset variables.cbomasktypes=url.cbomasktypes>
</cfif>


<cfquery name="qry_get_sitedefs" datasource="#application.sitedatasource#">
	select Name, siteDefId, parameters
	from siteDef
	where isInternal=0 and parameters like '%PCMSiteSourceids%'
</cfquery>
<cfif variables.cboSiteDefID neq "">
	<cfquery name="qoq_get_sitedef" dbtype="query">
		select parameters
		from qry_get_sitedefs
		where siteDefId=#variables.cbositeDefId#
	</cfquery>
	<CF_EvaluateNameValuePair NameValuePairs=#qoq_get_sitedef.parameters# collectionName="sitedef">
	
	<cfquery name="SourceQry" datasource="#application.siteDataSource#">
		select * from PCMproductTreeSource
		where sourceID  IN ( <cf_queryparam value="#sitedef.PCMSiteSourceids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfquery>
	
	<cfif SourceQry.RecordCount is 1 or variables.cboSourceID neq "">	
		<cfif SourceQry.RecordCount is 1>
			<cfset variables.cboSourceID=SourceQry.Sourceid>
		</cfif>
		
		<cfquery name="qry_get_RemoteCategories" datasource="#application.siteDataSource#">
			SELECT	Distinct PCMproductCategory.CategoryName, PCMproductTreeSource.SourceID, PCMproductCategory.RemoteCategoryID
			FROM	PCMproductTree INNER JOIN
                    PCMproductTreeSource ON PCMproductTree.sourceID = PCMproductTreeSource.SourceID INNER JOIN
                    PCMproductCategory ON PCMproductTree.TreeID = PCMproductCategory.TreeID
			WHERE   (PCMproductTreeSource.SourceID =  <cf_queryparam value="#variables.cboSourceID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			ORDER BY PCMproductCategory.CategoryName
		</cfquery>
		<cfif qry_get_RemoteCategories.RecordCount is 1 or variables.cboRemoteCategoryID neq "">
			<cfquery name="qry_get_sectiontypes" datasource="#application.siteDataSource#">
				select * from PCMproductTextSectionType
				where sectiontypeID IN (select sectiontypeID from PCMproductTextSection where
				remoteCategoryID =  <cf_queryparam value="#variables.cboRemoteCategoryID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				order by sectionTypeName
			</cfquery>
		</cfif>
	
	</cfif>
	
	
</cfif>




<cf_head>
	<cf_title>PCM Supplies and SMB Product Masks</cf_title>
</cf_head>
<script>
	function ChangeSelection(selection)
	{
		formobj = document.PCMMaskSelector;
		if(selection == "sitedefid")
		{
			formobj.cboSourceID.selectedIndex = 0;
			formobj.cboRemoteCategoryID.selectedIndex = 0;
			formobj.cboMaskTypes.selectedIndex = 0;
			formobj.submit();
		}
		if(selection == "sourceid")
		{
			formobj.cboRemoteCategoryID.selectedIndex = 0;
			formobj.cboMaskTypes.selectedIndex = 0;
			formobj.submit();
		}
		if(selection == "remotecategoryid")
		{
			formobj.cboMaskTypes.selectedIndex = 0;
			formobj.submit();
		}
		if(selection == "masktypes")
		{
			formobj.submit();
		}
	}
</script>




<table width="80%" cellpadding="5" cellspacing="0">
	<tr valign="top">
		<td colspan="2" align="center" nowrap>
			<!--- <cfscript>
				getUserTrees = application.com.relayProductTree.selectTree (sourcegroupid='2');
				getUserSources = application.com.relayProductTree.selectsource (sourcegroupid='2');
			</cfscript> --->
			<cfif qry_get_sitedefs.recordcount neq 0>
				<form action="PCMMasksHome.cfm" method="post" name="PCMMaskSelector" id="PCMMaskSelector">
					Select Site: 
					<select name="cboSiteDefID" onchange="javascipt: ChangeSelection('sitedefid');">
						<option value="">Please Select</option>
						<cfoutput query="qry_get_sitedefs"><option value="#siteDefId#" <cfif variables.cboSiteDefID eq siteDefId>selected</cfif>>#htmleditformat(Name)#</option></cfoutput>
					</select>
					Select Source:
					<select name="cboSourceID" onchange="javascipt: ChangeSelection('sourceid');" <cfif variables.cboSiteDefID eq ""> disabled</cfif>>
						<option value="">Please Select</option>
						<cfif variables.cboSiteDefID neq "">
							<cfoutput query="SourceQry"><option value="#sourceID#" <cfif variables.cbosourceID eq sourceID>selected</cfif>>#htmleditformat(SourceDescription)#</option></cfoutput>
						</cfif>
					</select>
					Select Category:
					<select name="cboRemoteCategoryID" onchange="javascipt: ChangeSelection('remotecategoryid');" <cfif variables.cboSourceID eq ""> disabled</cfif>>
						<option value="">Please Select</option>
						<cfif variables.cboSourceID neq "">
							<cfoutput query="qry_get_RemoteCategories"><option value="#RemoteCategoryID#" <cfif variables.cboRemoteCategoryID eq RemoteCategoryID>selected</cfif>>#htmleditformat(categoryname)#</option></cfoutput>
							qry_get_RemoteCategories
						</cfif>
					</select>
					Select Mask Type:
					<select name="cboMaskTypes" onchange="javascipt: ChangeSelection('masktypes');" <cfif variables.cboRemoteCategoryID eq ""> disabled</cfif>>
						<option value="">Please Select</option>
						<cfif variables.cboRemoteCategoryID neq "">
							<cfoutput query="qry_get_sectiontypes"><option value="#sectiontypeID#" <cfif variables.cboMaskTypes eq sectiontypeID>selected</cfif>>phr_#htmleditformat(sectiontypename)#</option></cfoutput>
						</cfif>
					</select>
				</form>	
			<cfelse>
				Not Portals have been found
			</cfif>
		</td>
	</tr>
</table>
<cfif form.cboMaskTypes neq "">
	<cfinclude template="PCMmaskManagement.cfm">
</cfif>




</cf_translate>