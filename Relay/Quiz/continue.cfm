<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History
	NYB 2008/08/09  P-SOP003 - 	5:  Amended only use questions.cfm not QuestionAgain.cfm (this is now superfluous)
								6:  list questions in the order they appeared throughout the quiz
	NYB 2009-03-31  Sophos	 - 	Renamed getQuiz to getQuestions and getQuestions to getQuiz - to be in line with the rest of the Quiz code
								and fixed sort order
	NJH	2009/06/26	Bug Fix Sophos Support Issue 2349 - added orderMethod to form
	NYB	2011-09-16	P-SNY106 - introduction of stringencyMode - if of type Strict then various behaviours will be altered
	STCR 2012-08-13	P-REL109 Phase 2 - move getNotAnswered query to previously-unused relayQuiz.getUnansweredQuestions() function and call from there.
	IH 	2012-08-24	Formatting changes
	STCR 2013-04-19	CASE 432328 Preserve OrderMethod when returning to Questions.
--->
<!--- get the quiz details --->
<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder" /><!--- question order. Alpha not implemented. --->
<cfset getQuiz = application.com.relayQuiz.getQuiz(QuizTakenID = QuizTakenID, OrderMethod = OrderMethod)>

<CFSET AllQuestions = getQuiz.Questions>

<!--- STCR P-REL109 --->
<cfquery name="getQuestions" datasource="#application.siteDataSource#">
  select qq.QuestionID AS theQuestion, qq.QuestionRef AS Name, 'phr_Title_QuizQuestion_'+cast(qq.QuestionID AS varchar) AS NamePhraseTextID, 1 as sortOrder,
    qq.QuestionTypeID, qqt.QuestionTypeTextID
  from QuizQuestion qq with (noLock) inner join QuizQuestionType qqt with(nolock) on qq.QuestionTypeID = qqt.QuestionTypeID
  where qq.QuestionID IN ( <cf_queryparam value="#AllQuestions#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfquery>

<cfloop index="i" from="1" to="#getQuestions.recordcount#">
	<CFSET rowNum = ListFind(ValueList(getQuestions.theQuestion), ListGetAt(AllQuestions,i))>
	<cfif rowNum gt 0>
		<cfset temp = QuerySetCell(getQuestions, "sortOrder", i, rowNum)>
	</cfif>
</cfloop>

<CFQUERY NAME="getQuestions" dbtype="query">
	SELECT * FROM getQuestions ORDER BY sortOrder
</CFQUERY>

<!--- 2012-08-13 STCR P-REL109 Phase 2 - move getNotAnswered query to previously-unused relayQuiz.getUnansweredQuestions() function and call from there. --->
<cfset getNotAnswered = application.com.relayQuiz.getUnansweredQuestions(QuizTakenID=QuizTakenID) />
<CFSET NotAnswered = ValueList(getNotAnswered.NotAnswered)>
	<CFOUTPUT>
	<div id="bordercontentDiv" class="QuizDivWrapper">
		<h2>phr_Quiz_QuizSummary</h2>
		<h4>phr_Quiz_ClickQuestionToRtn</h4>
		<div class="row"><div class="col-xs-12">
		<ul> <!--- &zwnj; is equal to no space --->
		</CFOUTPUT>
		<CFOUTPUT QUERY="getQuestions">
			<li>
				<div class="col-xs-12 row">
					<ul class="list-inline">
						<li><span class="questionAnswer">phr_quiz_theQuestion&zwnj;#getQuestions.currentRow#:</span></li>
						<li>
							<A href="##" onclick="javascript:jQuery('##quizContainer').load('/quiz/questions.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&QuestionID=#theQuestion#&OrderMethod=#OrderMethod#&frmReturnTo=#frmReturnTo#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#');">
								<CFIF namePhrasetextID is not "">#htmleditformat(namePhrasetextID)#<CFELSE>#htmleditformat(Name)#</cfif>
							</A></span>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 row">
					<ul class="list-inline">
						<cfset qAnswers=application.com.relayQuiz.getQuizQuestionAnswersGivenByPerson(questionID=theQuestion,quizTakenID=quizTakenID)>
						<li><span class="questionAnswer">phr_quiz_yourAnswer&zwnj;#getQuestions.currentRow#:</span></li>
						<cfif qAnswers.recordCount>
							<cfloop query="qAnswers">
								<li>
									<cfif answerTextGiven neq "">
										#answerTextGiven#
									</cfif>
									<cfif matchingAnswerTextGiven neq "">
										&nbsp;-&nbsp;#matchingAnswerTextGiven#
									</cfif>
								</li>
							</cfloop>
						</cfif>
					</ul>
				</div>
			</li>
		</CFOUTPUT>
		<CFOUTPUT>
		</ul>
		</div></div>
		<SCRIPT type="text/javascript">
			function finish() {
				jQuery("##timerContainer").remove();
				jQuery("##quizContainer").load("finish.cfm?" + jQuery('form##Quiz').serialize());
			    return false;
			}

		</SCRIPT>

	<div class="row">
		<div class="form-group">
		<FORM NAME="Quiz" ID="Quiz" METHOD="post" onSubmit="return finish();">
			<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
			<div class="form-group">
				<div class="col-xs-12 col-sm-8 col-sm-offset-4">
					<INPUT TYPE="submit" value="phr_Quiz_SubmitQuiz" class="btn btn-primary">
				</div>
			</div>
			<INPUT TYPE="Hidden" NAME="frmContinue" VALUE="1">
			<cf_encryptHiddenFields>
			<CF_INPUT TYPE="Hidden" NAME="frmReturnTo" VALUE="#frmReturnTo#">
			<CF_INPUT TYPE="Hidden" NAME="QuizTakenID" VALUE="#QuizTakenID#">
			<CF_INPUT TYPE="Hidden" NAME="OrderMethod" VALUE="#orderMethod#">
			<CF_INPUT TYPE="Hidden" NAME="reviewAnswersLink" VALUE="#reviewAnswersLink#">
			</cf_encryptHiddenFields>
		</FORM>
		</div>
	</div>
	</CFOUTPUT>
