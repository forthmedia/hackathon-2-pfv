<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Filename:  relay\quiz\reviewAnswers.cfm

About
	Allows a user to review which answers they answered correctly / incorrectly.

Amendment History
2014-11-04	DXC	Case 442232 - Not translating correct answer in reviewAnswers
2015-05-13	PYW	Case 444500	- Fix XSS vulnerabilities
--->
<cfparam name="stringencyMode" default="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="showIncorrectQuestionsOnCompletion" default="false" type="boolean">
<cfparam name="reviewAnswersLink" default="false" type="boolean">
<cfparam name="message" default="">

<CFIF NOT IsDefined("QuizTakenID")>
	<cf_translate>
		<p>phr_eLearningQuizIDNotDefinedError</p>
	</cf_translate>
	<CF_ABORT>
</CFIF>

<CFIF IsDefined("getQuiz")>
	<cfset getQuizTakenInfo = getQuiz>
<cfelse>
	<!--- get the quiz details --->
	<cfset getQuizTakenInfo = application.com.relayQuiz.getQuiz(QuizTakenID = QuizTakenID)>
</CFIF>

<cfif not (getQuizTakenInfo.personid eq request.relaycurrentuser.personid)>
	<CF_ABORT>
</cfif>

<CFOUTPUT>

	<cfset encryptedLink = application.com.security.encryptURL('/quiz/start.cfm?quizDetailID=#getquiztakenInfo.QuizDetailID#&OrderMethod=#OrderMethod#&allowRedo&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')>
	<script language="Javascript">
		function redoQuiz() {
			document.location.href = '#jsStringFormat(encryptedLink)#';
		}
	</script>

	<cfset encryptedFinishLink = application.com.security.encryptURL('/quiz/finish.cfm?QuizTakenID=#QuizTakenID#&OrderMethod=#OrderMethod#&allowRedo&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')>
	<script language="Javascript">
		function finishScreen() {
			jQuery("##quizFinish").load('#jsStringFormat(encryptedFinishLink)#')
		}
	</script>

	<cfif fileExists("#application.paths.code#\styles\quizReviewAnswers.css")>
		<cfhtmlhead text='<link type="text/css" rel="stylesheet" href="/code/styles/quizReviewAnswers.css" />#chr(10)#'>
	</cfif>


		<div id="reviewAnswers">
			<p class="quizLinks">
				<ul class="list-inline quizCompleted">
					<li><a href="javascript:finishScreen()">phr_hideAnswers</a></li>
					<cfif canRedo><li><a href="javascript:redoQuiz()">phr_Quiz_ReattemptLink</a></li></cfif>
				</ul>
			</p>

			<cfset questionCounter = 1>
				<cfloop list="#getQuizTakenInfo.questionsViewed#" index="i">
				<cfset questionAnswerDetails = application.com.relayQuiz.getQuizQuestionAnswersGivenByPerson(quizTakenID = getQuizTakenInfo.quizTakenID, questionID=i)>

				<cfquery name="getCorrectAnswers" datasource="#application.siteDataSource#">
					select * from quizAnswer
					where questionID = <cf_queryparam value="#i#" cfsqltype="cf_sql_integer">
					and score > 0
				</cfquery>
				<cfif questionAnswerDetails.scoregiven gt 0><cfset correctAnswer = "answeredCorrectly"><cfelse><cfset correctAnswer = "answeredIncorrectly"></cfif>

				<div id="questionAnswer#questionCounter#" class="questionAnswer #correctAnswer#">
					<h2>phr_Question #questionCounter#</h2>
					<p class="questionAsked">#application.com.relayQuiz.getQuizQuestionList(questionID=i).questionText#</p>
					<p class="correctAnswer">
						<span class="bold">phr_correctAnswer </span>
						<cfif getCorrectAnswers.recordcount gt 0>
							<cfloop query="getCorrectAnswers">
								phr_Title_QuizAnswer_#getCorrectAnswers.AnswerID#<!--- 2014-11-04	DXC	Case 442232 --->
								<cfif getCorrectAnswers.recordcount gt 1 and getCorrectAnswers.currentRow lt getCorrectAnswers.recordcount>, </cfif><!--- 2014-11-04	DXC	Case 442232 --->
							</cfloop>
						<cfelse>
							phr_NoCorrectAnswer
						</cfif>
					</p>
					<p class="answerGiven">
						<cfif questionAnswerDetails.answerTextGiven neq "">
							<span class="bold">phr_yourAnswer </span>#questionAnswerDetails.answerTextGiven#
						<cfelse>
							<span class="bold">phr_youDidNotAnswer</span>
						</cfif>
					</p>
				</div>

				<cfset questionCounter = questionCounter + 1>
			</cfloop>
		</div>

</CFOUTPUT>