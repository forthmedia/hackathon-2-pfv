<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Filename:  relay\quiz\finish.cfm

About
	Creates form variables [[TotalScore]], [[ScoreOutOf]] and [[PassMark]]
	which can be used in most phrases displayed (except*).  These can be
	rounded by setting variable request.quiz.maxFractionDigits (in RelayINI.cfm).

	Displays the following phrase variables:
	- eLearningQuizIDNotDefinedError *
	- Quiz_FinishQuiz
	- Quiz_FailedText
	- Quiz_PassedText
	- Quiz_YourFinalScoreIs
	- and the FinishPhraseTextID and HeaderPhraseTextID allocated to the quiz

Amendment History
	NYB 2008-08-09  P-SOP003 -
	NYB 2008-10-28  P-SOP008
	NJH 2009-02-12	P-SNY047 - if quiz is completed when coming into this file, then don't update the quiz completed date. Also added the score back into the display
					I'm not sure why it was missing as an individual would want to see their score.
					Also passed in an argument to setQuizCompleted to email the individual with their results. This can be set in elearningINI.cfm. The
					default is set in elearning/elearningParams.cfm
	NYB	2009-03-09	Sophos - using new function globalFunctions.roundNumbers to display TotalScore and ScoreOutOf without trailing 0's
	NYB	2009-03-30	Sophos Bugzilla 1777
	NYB	2009-05-19	Sophos Bugzilla 2125 - removed hard coded HTTP
	NYB 2009-06-25 	P-FNL069 - added check and abort to prevent user looking at the final scores of quizzes taken by other users
	NJH	2009-06-26	Bug Fix Sophos Support Issue 2349 - added javascript redoQuiz function
	NYB 2009-08-03 	SNY047 bug
	NJH 2010-08-06	RW8.3 - get sendQuizCompletionEmail value from settings
	NYB	2011-09-16	P-SNY106 - introduction of stringencyMode - if of type Strict then various behaviours will be altered
	NYB	2011-11-07	P-SNY106 - added classes IncorrectQuestionListing & IncorrectQuestion num#currentrow#"
	IH	2012/03/28	Case 427221 save TotalScore after maxFractionDigits applied
	RMB 2012/07/04 	P-ENP Added hook to extend to PS Code
	YMA	2012/11/13	CASE:431933 change quiz popup to auto refresh the certifications list on closing so the user automatically see the certification progress.
	YMA	2014/04/14	Case 434303 Test Feedback to users
	AHL 2014-08-28	Case 441496 Quiz return.
	NJH 2015/
	PYW 2015-05-13	Case 444500	Fix XSS vulnerabilities
--->

<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
<cf_includeCSSonce template="/code/styles/quizReviewAnswers.css" checkIfExists="true">

<div id="quizFinish">

<cfparam name="stringencyMode" default="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="showIncorrectQuestionsOnCompletion" default="false" type="boolean">
<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="reviewAnswersLink" default="false" type="boolean">
<cfparam name="message" default="">

<CFIF NOT IsDefined("QuizTakenID")>
	<cf_translate>
		<p>phr_eLearningQuizIDNotDefinedError</p>
	</cf_translate>
	<CF_ABORT>
</CFIF>

<CFIF not IsDefined("URL.frmReturnTo")>
	<cfset frmReturnTo = "quizList.cfm">
</CFIF>

<CFIF IsDefined("getQuiz")>
	<cfset getQuizTakenInfo = getQuiz>
<cfelse>
	<!--- get the quiz details --->
	<cfset getQuizTakenInfo = application.com.relayQuiz.getQuiz(QuizTakenID = QuizTakenID)>
</CFIF>


<cfif not (getQuizTakenInfo.personid eq request.relaycurrentuser.personid)>
	<CF_ABORT>
</cfif>

<!--- get questions and phr_--->
<cfset GetQuizInfo = application.com.relayQuiz.getQuizInfo(quizDetailID=getquiztakenInfo.QuizDetailID)>
<cfset sendQuizCompletionEmail = application.com.settings.getSetting("elearning.quizzes.sendQuizCompletionEmail")> <!--- 2010-08-06	NJH	RW8.3 - get value from settings --->

<CFSET Form.TotalScore = 0>
<!--- 2012-02-23 PPB Case 426433 was showing failed message when passed because there were no questions taken in data which was dataloaded --->
<cfif isDefined("quizPassed") and quizPassed>
	<cfif getQuizTakenInfo.score neq "">
		<CFSET Form.TotalScore = getQuizTakenInfo.score>
	</cfif>
<cfelse>
	<cfset getScore = application.com.relayQuiz.getScore(Questions=getQuizTakenInfo.Questions,QuizTakenID=QuizTakenID)>
	<CFIF getScore.TotalScore NEQ "">
		<CFSET Form.TotalScore = getScore.TotalScore>
	</CFIF>
</cfif>

<!--- 2012-05-07 STCR P-REL109 With the new data structure we can use the QuizTakenID directly --->
<cfset form.ScoreOutOf = application.com.relayQuiz.getHighestPossibleScore(QuizTakenID=QuizTakenID)>

<!--- NJH 2009-02-12 P-SNY047 - don't reset completion date, etc if the quiz has already been completed. --->

<cfset NFclass=CreateObject("java", "java.text.NumberFormat") >
<cfset NFinstance = NFclass.getInstance()>
<cfset NFinstance.setMaximumFractionDigits(javaCast("int", application.com.settings.getSetting("elearning.quizzes.maxFractionDigits"))) >

<cfset Form.TotalScore = NFinstance.format(javaCast("double", Form.TotalScore)) >
<cfset Form.ScoreOutOf = NFinstance.format(javaCast("double", Form.ScoreOutOf)) >
<cfset Form.PassMark = NFinstance.format(javaCast("double", getquiztakenInfo.PassMark)) >

<cfif getQuizTakenInfo.completed eq "">
	<!--- update person with score--->
	<cfset updateQuiz = application.com.relayQuiz.updateQuizScore(TotalScore=Form.TotalScore,QuizTakenID=QuizTakenID)>
	<cfset setQuizTakenCompleted = application.com.relayQuiz.setQuizTakenCompleted(QuizTakenID = QuizTakenID,sendEmail=sendQuizCompletionEmail)>
</cfif>


<CFOUTPUT>
<!--- NJH 2009-06-26 Bug Fix Sophos Support Issue 2349 Added orderMethod to the url in the location below --->



<CF_TRANSLATE>

<!--- NJH 2009-06-26 Bug Fix Sophos Support Issue 2349 - added redoQuiz function --->
<cfoutput>
	<!--- 2014-04-14	YMA	Case 434303 Test Feedback to users --->
	<cfset encryptedLink = application.com.security.encryptURL('/quiz/start.cfm?quizDetailID=#getquiztakenInfo.QuizDetailID#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')>
	<script language="Javascript">
		function redoQuiz() {
			document.location.href = '#jsStringFormat(encryptedLink)#';
		}
	</script>

	<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
	<cfset encryptedReviewAnswersLink = application.com.security.encryptURL('/quiz/reviewAnswers.cfm?QuizTakenID=#QuizTakenID#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')>
	<script language="Javascript">
		function reviewAnswers(canRedo) {
			jQuery("##quizFinish").load('#jsStringFormat(encryptedReviewAnswersLink)#&canRedo='+canRedo);
		    document.getElementById('##quizFinish').scrollIntoView(true);
		}
	</script>
</cfoutput>

<cf_include template="\code\cftemplates\quizFinishExtend.cfm" checkIfExists="true">

<cfif stringencyMode eq "Strict" and not IsDefined("frmTask") and len(message) gt 0>
	<cfoutput><cf_translate><div class="message">#application.com.security.sanitiseHTML(message)#</div></cf_translate></cfoutput>
</cfif>

<cfset quizPassed = true>

			<h1>phr_Quiz_FinishQuiz</h1>
			<cfif len(GetQuizInfo.Header_defaultTranslation) gt 0>
			<p class="quizCompleted">phr_header_quizDetail_#GetQuizInfo.quizDetailID#</p>
			</cfif>

			<cfif Form.TotalScore lt GetQuizInfo.Passmark>
				<cfset quizPassed = false>
				<p class="quizCompleted">phr_Quiz_FailedText</p>
				<cfset checkQuiz = application.com.relayQuiz.checkQuiz(QuizDetailID=getquiztakenInfo.QuizDetailID)>
				<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
				<cfif reviewAnswersLink OR (checkQuiz.recordcount lt getQuizTakenInfo.NumberOfAttemptsAllowed)>
					<ul class="list-inline quizCompleted">
					<cfif checkQuiz.recordcount lt getQuizTakenInfo.NumberOfAttemptsAllowed>
						<li><a href="javascript:redoQuiz();">phr_Quiz_ReattemptLink</a></li>
						<cfif reviewAnswersLink>
						<li><a href="javascript:reviewAnswers(true);">phr_reviewAnswers</a></li>
						</cfif>
					<cfelse>
						<cfif reviewAnswersLink>
							<li><a href="javascript:reviewAnswers(false);">phr_reviewAnswers</a></li>
						</cfif>
					</cfif>
					</ul>
				</cfif>
			<cfelse>
				<p class="quizCompleted">phr_Quiz_PassedText</p>
				<!--- NJH 2012/11/21 Case 432093 --->

				<cfif getquiztakenInfo.passDecidedBy eq "quiz">
				<script>
					window.parent.refreshModuleStatus(#getquiztakenInfo.moduleID#,'completed');
				</script>
				</cfif>
			</cfif>
			<p class="quizCompleted">phr_Quiz_YourFinalScoreIs <!--- NYB 2009-03-30 Removed #Form.TotalScore# because this should be put in the translation as [[TotalScore]] ---></p>
			<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
			<cfif reviewAnswersLink and Form.TotalScore gte GetQuizInfo.Passmark>
				<ul class="list-inline quizCompleted">
					<li><a href="javascript:reviewAnswers(false);">phr_reviewAnswers</a></li>
				</ul>
			</cfif>

	</CF_TRANSLATE>
	</CFOUTPUT>

	<CFIF stringencyMode eq "Strict" OR showIncorrectQuestionsOnCompletion>
		<cfset IncorrectQuestions = application.com.relayquiz.getIncorrectQuestions(QuizTakenID)>
		<cfif IncorrectQuestions.recordcount gt 0>
			<CFOUTPUT>
				phr_elearning_IncorrectQuestions
			</CFOUTPUT>
						<CFOUTPUT QUERY="IncorrectQuestions">
										#htmlEditFormat(application.com.relaytranslations.translatephrase("phr_Title_QuizQuestion_#QuestionID#"))#
						</CFOUTPUT>
			<CFOUTPUT>
			</CFOUTPUT>
		</cfif>
	</CFIF>

	<CFOUTPUT>
	<CF_TRANSLATE>
		<cfif len(GetQuizInfo.footer_defaultTranslation) gt 0>
		<p class="quizCompleted">phr_footer_quizDetail_#GetQuizInfo.quizDetailID#</p>
		</cfif>
		<!--- Begin 2014-08-28 AHL Case 441496 Quiz return --->
		<cfset url.QuizTakenID = QuizTakenID>	<!--- QuizTakenId needs to be here to be accessible to the phrase --->
		<p>phr_elearning_QuizFooter</p>
		<!--- End  2014-08-28 AHL Case 441496 Quiz return --->
		</TD>
	</TR>
</TABLE>


<FORM NAME = "thisForm" ACTION="#frmReturnTo#" METHOD="POST">
	<CF_INPUT TYPE="Hidden" NAME="QuizTakenID" VALUE="#QuizTakenID#">
	<CF_INPUT TYPE="Hidden" NAME="QuizScore" VALUE="#Form.TotalScore#">
	<CF_INPUT TYPE="Hidden" NAME="frmPersonID" VALUE="#getQuizTakenInfo.personid#">
</FORM>

</div>

</CF_TRANSLATE>

</CFOUTPUT>
