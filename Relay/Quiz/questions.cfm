<!---  �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History
2008-08-09	NYB   	P-SOP003 - added ability to sort attributes different options via orderType.  Accepts: SortOrder, Alpha or Random
							 - Amended Allowed Attempts - wasn't working
2008-10-13	NYB		Sophos-2. 	added OrderMethod to RelayTag
							 - 5:  Changed continue to only use questions.cfm not QuestionAgain.cfm (this is now superfluous). Required minor tweaking here
2008-10-24 NYB 		p-Sop007
2009-02-09 SSS	Added a varible countryID so questions that are scoped to the users country also appear
2009-05-19	NYB		removed hard coded HTTP
2009-10-08	NYB		LHID2739 - made a number of improvements to the code - updated for version control
2010-02-12  NAS		SNY090 - added phr_Quiz_Outof transaltion
2011-04-15 	NYB		LHID4286 added orderType = OrderMethod - was losing order method in subsequent questions
2011-08-15 	PPB 	REL109 added the toggleVisibility function and grey background to questions
2011-04-15 	NYB		LHID4286 added orderType = OrderMethod - was losing order method in subsequent questions
2011-09-16 	NYB		P-SNY106 - introduction of stringencyMode - if of type Strict then various behaviours will be altered
						Mostly, if a user moves forward in the quiz, all previous questions viewable using the Back button
						will be viewonly.  If the user closes the window then reopens all questions they have viewed in the previous
						session will display as view only.
					Various changes made.  Too many to comment separately
					NB*1 - this code specifically relates to keeping questions on the current page, eg page 2,
						editable if the person uses the back button, then comes back to page 2.  Otherwise the questions
						on page 2 would be viewonly.
2012-08-22	STCR	CASE 430124 Matching Answer saving
2012-08-24	IH		Formatting changes
2013-02-27	PPB		Case 433826 translations
2013-04-03 	NYB 	Case 434596 fixed line not written correctly
2013-04-16	STCR	CASE 432328 Question OrderMethod
2015-05-13	PYW		Case 444500	Fix XSS vulnerabilities
2016-06-10	WAB		BF-977  Ajax request to submit page done as a GET, so failed if URL too long.  
					Changed back to a POST and changed some URL scopes to FORM
--->

<cfset encryptionTest = application.com.security.confirmFieldsHaveBeenEncryptedWithFeedback("QuizDetailID,QuizTakenID,previouslyStarted")>
<cfif not encryptionTest.isOK>
	<cfoutput>
	<script>
		window.close();
	</script>
	</cfoutput>
</cfif>

<!--- <cf_head>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
</cf_head> --->

<!--- go to error page if required variables are not set --->
<CFIF  NOT IsDefined("QuizDetailID")>
	<CFINCLUDE TEMPLATE="error.cfm">
	<CF_ABORT>
</CFIF>

<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder" /><!--- question order. Alpha not implemented. --->
<cfparam name="stringencyMode" default="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="showIncorrectQuestionsOnCompletion" default="false" type="boolean">
<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="reviewAnswersLink" default="false" type="boolean">
<cfparam name="message" default="">
<cfparam name="session.quiztaken#QuizTakenID#.justCreated" default="false">
<cfparam name="session.quiztaken#QuizTakenID#.latestViewedQuestions" default="">

<CFIF (not IsDefined("frmTask")) or structkeyexists(form,"Continue")>
	<cfset "session.quiztaken#QuizTakenID#.latestViewedQuestions" = "">
</CFIF>

<cfset orderType = OrderMethod>
<cfif structKeyExists(url,"OrderMethod")>
	<cfset orderType = url.OrderMethod>
</cfif>

<!--- get the quiz details --->
<cfset getQuiz =  application.com.relayQuiz.getQuiz(QuizTakenID = QuizTakenID, OrderMethod = OrderMethod)>
<CFSET ThisEntity = QuizTakenID>
<CFSET Start = 1>
<CFSET TimeLeft = getQuiz.TimeLeft>
<CFSET Completed = getQuiz.Completed>
<!--- PJP CASE 433062: Put back to original getQuiz.Questions --->
<CFSET Questions = getQuiz.Questions>
<CFSET QuestionsPerPage = getQuiz.QuestionsPerPage>
<CFSET NumberOfQuestions = getQuiz.NumToChoose>
<CFSET QuestionsViewed = getQuiz.QuestionsViewed>

<cfif NumberOfQuestions gt ListLen(Questions)>
	<CFSET NumberOfQuestions = ListLen(Questions)>
</cfif>
<CFSET QueryQuestions = "">


<CFIF IsDefined("QuestionID")>  <!--- ie, if come from continue.cfm --->
	<CFSET QuestionsPerPage = 1>
	<CFSET NumberOfQuestions = ListFind(Questions, QuestionID)>
	<CFSET Start = NumberOfQuestions>
</CFIF>

<!--- define the start record --->
<CFIF NumberOfQuestions GT Start>
 	<CFIF IsDefined("Forward")>
		 <CFSET Start = frmOrigStart + QuestionsPerPage>
 	<CFELSEIF IsDefined("Back")>
		<cfif QuestionsPerPage lt frmOrigStart>
		 <CFSET Start = frmOrigStart - QuestionsPerPage>
		</cfif>
	<CFELSEIF IsDefined("frmOrigStart")>
	 	 <CFSET Start = frmOrigStart>
	</CFIF>
</CFIF>

<cfif stringencyMode eq "Strict" and not IsDefined("frmTask")>
	<cfif ListLen(QuestionsViewed) gte Start>
		<cfset Start = ListFind(Questions,ListLast(QuestionsViewed)) + 1>
	</cfif>
</cfif>

<!--- set the 'loop to' variable --->
<cfif IsDefined("frmOrigStart") and IsDefined("frmTask") and IsDefined("Back") and QuestionsPerPage gte frmOrigStart>
	<CFSET LoopTo = frmOrigStart - 1>
<cfelse>
	<CFIF (Start + QuestionsPerPage - 1) LT NumberOfQuestions>
		<CFSET LoopTo = Evaluate("Start + QuestionsPerPage - 1")>
	<CFELSE>
		<CFSET LoopTo = NumberOfQuestions>
	</CFIF>
</CFIF>

<cfset curQns = session["quiztaken#QuizTakenID#"].latestViewedQuestions>
<!--- if answers submitted then update --->
<!--- ie, have clicked on the Continue button which appears at the end of the question chain --->
<CFIF IsDefined("frmTask")>
	<cfif stringencyMode eq "Strict" and structKeyExists(form,"forward") and not session["quiztaken#QuizTakenID#"].justCreated><!--- 2013-04-03 NYB Case 434596 fixed line not written correctly  --->
		<cfloop index="i" list="#flaglist#">
			<cfif listfind(QuestionsViewed,ListLast(i,'_')) gt 0 and listfind(curQns,ListLast(i,'_')) eq 0>
				<cfset flaglist = ListDeleteAt(flaglist,ListFind(flaglist,i))>
				<cfset message = "phr_elearning_Error_QuestionsCanNotBeSaved">
			</cfif>
		</cfloop>
	</cfif>
	<CFSET updateTime = request.requestTimeODBC>
	<!--- P-SOP003 2008-08-09 NYB - put template call in cftry as errors when a question isn't answered.  Other option was to set all questions as required. --->
	<cftry>
		<!--- <CFINCLUDE template="..\flags\flagtask.cfm"> --->
		<cfset getAllQuizTakenQuestions = application.com.relayQuiz.getQuizQuestions(QuizTakenID=#QuizTakenID#) />
		<cfset getQuizAnswers = "" />
		<cfset frmThisAnswer = "" />
		<cfloop query="getAllQuizTakenQuestions">
			<cfif structKeyExists(form,"#getAllQuizTakenQuestions.QuestionTypeTextID#_QuizQuestion_#getAllQuizTakenQuestions.QuestionID#_#getAllQuizTakenQuestions.QuizTakenQuestionID#")>
				<cfset frmThisAnswer = form["#getAllQuizTakenQuestions.QuestionTypeTextID#_QuizQuestion_#getAllQuizTakenQuestions.QuestionID#_#getAllQuizTakenQuestions.QuizTakenQuestionID#"] />
				<!--- 2012-08-22 CASE 430124 STCR fixed updateQuizTakenAnswers so that it only applies the AnswerScore to the ScoreGiven when the correct match is chosen --->
				<cfif getAllQuizTakenQuestions.QuestionTypeTextID eq "matching">
					<cfif listLen(frmThisAnswer,application.delim1) gt 1>
						<cfset updateQuizTakenAnswers = application.com.relayQuiz.updateQuizTakenAnswers(QuizTakenID=QuizTakenID, QuestionID=getAllQuizTakenQuestions.QuestionID, QuizTakenQuestionID=getAllQuizTakenQuestions.QuizTakenQuestionID, MatchingAnswerList=frmThisAnswer, MatchingPairDelim=application.delim1) />
					</cfif>
				<cfelse>
					<cfset updateQuizTakenAnswers = application.com.relayQuiz.updateQuizTakenAnswers(QuizTakenID=QuizTakenID, QuestionID=getAllQuizTakenQuestions.QuestionID, QuizTakenQuestionID=getAllQuizTakenQuestions.QuizTakenQuestionID, AnswerList=frmThisAnswer) />
				</cfif>
			</cfif>
		</cfloop>
		<cfcatch><!--- cfoutput>A question was not answered on the previous page</cfoutput---></cfcatch>
	</cftry>
	<!--- if ready to continue then skip to continue page --->
	<CFIF structkeyexists(form,"Continue")>
		<cfif stringencyMode eq "Strict">
			<cfoutput>
				<SCRIPT type="text/javascript">
					jQuery("##timerContainer").remove();
					jQuery("##quizContainer").load("finish.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#");
				</SCRIPT>
			</cfoutput>
		<cfelse>
			<cfoutput>
				<SCRIPT type="text/javascript">
					jQuery("##quizContainer").load("continue.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#");
				</SCRIPT>
			</cfoutput>
		</cfif>
		<CF_ABORT>
	</CFIF>
	<cfif session["quiztaken#QuizTakenID#"].justCreated>
		<cfset "session.quiztaken#QuizTakenID#.justCreated"="false">
	</cfif>
</CFIF>

<CFIF TimeLeft LTE 0 OR Completed NEQ "">
	<cfoutput>
		<SCRIPT type="text/javascript">
			jQuery("##quizContainer").load("finish.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#");
		</SCRIPT>
	</cfoutput>
	<CF_ABORT>
</CFIF>

<CFIF listlen(Questions) gt 0 and Start gt listlen(Questions)>
	<cfoutput>
		<SCRIPT type="text/javascript">
			jQuery("##quizContainer").load("finish.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#");
		</SCRIPT>
	</cfoutput>
	<CF_ABORT>
</CFIF>

<CFSET ThisEntity = QuizTakenID>
<CFSET column = 1>
<CFSET entityType = 7>
<CFSET countryid = 0>
<CFSET countrygroupids = 0>
<CFSET flagformat = "">
<CFSET flaglist = "">
<CFSET showNull = false>
<cfif stringencyMode eq "Strict" and IsDefined("frmTask") and len(message) gt 0>
	<cfoutput><div class="message">#application.com.security.sanitiseHTML(message)#</div></cfoutput>
</cfif>
<cf_includeCssOnce template="/Quiz/css/quiz.css">
<div id="bordercontentDiv" class="QuizDivWrapper">

	<!--- Form Submitted by click event on submit button --->
	<FORM ACTION="questions.cfm" METHOD="POST" NAME="Quiz" id="Quiz" onSubmit="return false;">
	<cfset getQuizQuestionAnswersGivenByPerson = application.com.relayQuiz.getQuizQuestionAnswersGivenByPerson(QuizTakenID=QuizTakenID) /><!--- 2012-08-23 STCR CASE 430124 --->
	<!--- START: NB*1 --->
	<cfset firstLoop = "true">
	<cfset listNewQns = "false">
	<!--- END: NB*1 --->
	<cfset questionsExist = false>
	<!--- list the questions and possible answers --->
	<CFLOOP FROM="#Start#" TO="#LoopTo#" INDEX="i">

		<CFSET Question = ListGetAt(Questions, i)>

		<!--- get the questions on this page that are associated with this quiz --->
		<!--- <cfset getFlagGroup = application.com.relayQuiz.getQuizQuestions(QuestionIDs=#Question#)> --->
		<cfset getQuizTakenQuestions = application.com.relayQuiz.getQuizQuestions(QuestionIDs=Question,QuizTakenID=QuizTakenID) />
		<cfset getQuizAnswers = application.com.relayQuiz.getQuizAnswers(QuestionID=Question,QuizTakenID=QuizTakenID) />

		<cfif getQuizTakenQuestions.recordCount gt 0>
			<!--- 2008-10-24 NYB Sophos moved SortOrder to top --->
			<cfset questionsExist = true>
			<cfoutput>
			<div class="form-horizontal">
			<h2>phr_Question #htmleditformat(I)#</h2>
			<!--- P-SOP003 2008-08-09 NYB - if not Yes/No or True/False will Randomise attribute order-> --->
			<!--- ADD CFIF options neq 2 or neq Yes/No or T/F then : --->
			<!--- <cfset flagData = application.com.flag.getFlagGroupFlags(FlagGroupId=Question)>
			<cfset flagListX = ValueList(flagData.Name, ",")>
			<cfif (flagListX eq "Yes,No") or (flagListX eq "No,Yes") or (flagListX eq "True,False") or (flagListX eq "False,True")>
				<CFset orderType="SortOrder">
			</cfif> ---><!--- STCR P-REL109 replace flag funcs --->
			<!--- SSS 2009-02-09 persons country is not taken into account when bring back questions so set it here --->
			<cfset variables.CountryID = request.relaycurrentuser.countryID>

			<CFIF IsDefined("frmTask")>
				<CFIF frmTask eq "update" and IsDefined("Forward")>
					<cfif firstLoop>
						<cfif ListFind(QuestionsViewed,Question) eq 0 and ListFind(Questions,Question) gt ListFind(Questions,ListFirst(curQns))>
							<cfset curQns = "">
							<cfset listNewQns = "true">
						</cfif>
						<cfset firstLoop = "false">
					</cfif>
					<cfif listNewQns>
						<cfset curQns = ListAppend(curQns,Question)>
					</cfif>
				</cfif>
			<cfelse>
				<cfif session["quiztaken#QuizTakenID#"].justCreated eq "false">
					<cfset curQns = ListAppend(curQns,Question)>
				</cfif>
			</cfif>

			<CFSET flagmethod = "edit">
			<CFIF ListFind(QuestionsViewed,Question) eq 0>
				<cfset QuestionsViewed = listappend(QuestionsViewed,Question)>
			<CFELSE>
				<cfif stringencyMode eq "Strict">
					<cfif ListFind(curQns,Question) eq 0>
						<CFSET flagmethod = "view">
					</cfif>
				</cfif>
			</CFIF>
			<!--- <cfinclude template="/flags/FlagGroup.cfm">  ---><!--- STCR P-REL109 render html form fields --->
				<div class="form-group row">
					<div class="col-xs-12"><h4>phr_Title_QuizQuestion_#Question#</h4></div>
					<div class="col-xs-12">
					<cfset nestedQuizAnswers = Duplicate(getQuizAnswers) />
					<cfloop query="getQuizAnswers">
						<cfif getQuizTakenQuestions.QuestionTypeTextID eq "matching" and (getQuizAnswers.matchingAnswerID eq "" or getQuizAnswers.MatchingAnswerID eq 0)>
							<!--- For matching questions we only start a new row if this is the LHS option --->
						<cfelse>
								<cfswitch expression="#getQuizTakenQuestions.QuestionTypeTextID#">
									<cfcase value="radio">
										<div class="radio col-xs-12 col-sm-6 col-md-4"><label><input type="radio" class="radio" name="radio_QuizQuestion_#Question#_#getQuizTakenQuestions.QuizTakenQuestionID#" value="#getQuizAnswers.AnswerID#" <cfif isDefined("getQuizAnswers.QuizTakenAnswerID") and isNumeric(getQuizAnswers.QuizTakenAnswerID)>checked="checked"</cfif> <cfif flagMethod neq "edit">disabled="disabled"</cfif> />
									</cfcase>
									<cfcase value="checkbox">
										<div class="checkbox col-xs-12 col-sm-6 col-md-4"><label><input type="checkbox" class="checkbox" name="checkbox_QuizQuestion_#Question#_#getQuizTakenQuestions.QuizTakenQuestionID#" value="#getQuizAnswers.AnswerID#" <cfif isDefined("getQuizAnswers.QuizTakenAnswerID") and isNumeric(getQuizAnswers.QuizTakenAnswerID)>checked="checked"</cfif> <cfif flagMethod neq "edit">disabled="disabled"</cfif> />
									</cfcase>
									<cfcase value="matching">
										<div class="form-group row"><div class="col-xs-12 col-sm-4 col-md-4 control-label"><label for="matching_QuizQuestion_#Question#_#getQuizTakenQuestions.QuizTakenQuestionID#" class="flagAttributeLabel"><span class="matchingAnswerQuestion">phr_Title_QuizAnswer_#getQuizAnswers.AnswerID#</span></label></div>
									</cfcase>
								</cfswitch>

								<cfif getQuizTakenQuestions.QuestionTypeTextID neq "matching">
									phr_Title_QuizAnswer_#getQuizAnswers.AnswerID#</label></div>
								<cfelse>
									<div class="col-xs-12 col-sm-8 col-md-8">
									<select class="form-control" id="matching_QuizQuestion_#Question#_#getQuizTakenQuestions.QuizTakenQuestionID#" name="matching_QuizQuestion_#Question#_#getQuizTakenQuestions.QuizTakenQuestionID#" <cfif flagMethod neq "edit">disabled="disabled"</cfif>>
										<option value="#getQuizAnswers.AnswerID##application.delim1#">-</option>
										<cfloop query="nestedQuizAnswers">
											<!--- Only render RHS options inside dropdown --->
											<cfif nestedQuizAnswers.AnswerID neq getQuizAnswers.AnswerID and (nestedQuizAnswers.MatchingAnswerID eq "" or nestedQuizAnswers.MatchingAnswerID eq 0)>
												<option value="#getQuizAnswers.AnswerID##application.delim1##nestedQuizAnswers.AnswerID#" <cfloop query="getQuizQuestionAnswersGivenByPerson"><cfif getQuizQuestionAnswersGivenByPerson.AnswerID eq getQuizAnswers.AnswerID and getQuizQuestionAnswersGivenByPerson.MatchingAnswerID eq nestedQuizAnswers.AnswerID>selected</cfif></cfloop>>phr_Title_QuizAnswer_#nestedQuizAnswers.AnswerID#</option>
											</cfif>
										</cfloop>
									</select>
									</div></div>
								</cfif>
						</cfif>
					</cfloop>
					</div>
				</div>
			<!--- <cfif getQuizTakenQuestions.QuestionTypeTextID neq "matching">
			</div><!--- close greyBackground --->
			</cfif> --->
			</cfoutput>
		</cfif>
	</CFLOOP>
	<cfset application.com.relayquiz.updateQuestionsViewed(QuizTakenID=QuizTakenID,QuestionsViewed=QuestionsViewed)>

	<CFIF IsDefined("frmTask")>
		<CFIF frmTask eq "update" and IsDefined("Forward")>
			<cfif curQns neq session["quiztaken#QuizTakenID#"].latestViewedQuestions>
				<cfset "session.quiztaken#QuizTakenID#.latestViewedQuestions" = curQns>
			</cfif>
		</CFIF>
	<cfelse>
		<cfif session["quiztaken#QuizTakenID#"].justCreated eq "false">
			<cfset "session.quiztaken#QuizTakenID#.latestViewedQuestions" = curQns>
		</cfif>
	</CFIF>
	<CFOUTPUT>
		<!--- CASE 425872 NJH 2012/03/13 - this needs to not be encrypted as certain javascript expects this to be here --->
		<INPUT TYPE="Hidden" NAME="frmContinue" VALUE="0">
		<cf_encryptHiddenFields>
		<CF_INPUT TYPE="Hidden" NAME="frmReturnTo" VALUE="#frmReturnTo#">
		<CF_INPUT TYPE="Hidden" NAME="QuizTakenID" VALUE="#QuizTakenID#">
		<CF_INPUT TYPE="Hidden" NAME="QuizDetailID" VALUE="#QuizDetailID#">
		<CF_INPUT TYPE="Hidden" NAME="frmOrigStart" VALUE="#Start#">
		<CF_INPUT TYPE="Hidden" NAME="flagList" VALUE="#flagList#">
		<CF_INPUT TYPE="Hidden" NAME="frmEntityID" VALUE="#ThisEntity#">
		<INPUT TYPE="Hidden" NAME="frmTask" VALUE="update">
		<CF_INPUT TYPE="Hidden" NAME="stringencyMode" VALUE="#stringencyMode#">
		<CF_INPUT TYPE="Hidden" NAME="showIncorrectQuestionsOnCompletion" VALUE="#showIncorrectQuestionsOnCompletion#">
		<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
		<CF_INPUT TYPE="Hidden" NAME="reviewAnswersLink" VALUE="#reviewAnswersLink#">
		<CF_INPUT TYPE="Hidden" NAME="OrderMethod" VALUE="#OrderMethod#">
		</cf_encryptHiddenFields>
	</CFOUTPUT>

	<!--- next, previous and finish buttons --->
	<cfif questionsExist>
		<cfif stringencyMode eq "Strict">
			<cfset submitText = "phr_FinishQuiz">
		<cfelse>
			<cfset submitText = "phr_quiz_Continue">		<!--- 2013-02-27 PPB Case 433826 translation --->
		</cfif>
		<CFIF NumberOfQuestions GT QuestionsPerPage>
			<CFOUTPUT>
				<div class="form-group">
					<div class="col-xs-12 col-sm-4 control-label"><label>
						<CFIF ListLen(QueryQuestions) EQ 1>
							phr_Question #htmleditformat(Start)#
						<CFELSEIF (Start + QuestionsPerPage - 1) LT NumberOfQuestions>
							<!--- 2010-02-12  NAS		SNY090 - added phr_Quiz_Outof transaltion --->
							phr_Quiz_Questions #htmleditformat(Start)# - #Evaluate("Start + QuestionsPerPage - 1")# phr_Quiz_Outof #htmleditformat(NumberOfQuestions)#
						<CFELSE>
							<!--- 2010-02-12  NAS		SNY090 - added phr_Quiz_Outof transaltion --->
							phr_Quiz_Questions #htmleditformat(Start)# - #htmleditformat(NumberOfQuestions)# phr_Quiz_Outof #htmleditformat(NumberOfQuestions)#
						</CFIF>
					</label></div>
					<div class="col-xs-12 col-sm-8">
						<CFIF Start GT 1>
							<!--- P-SOP003-5 2008-08-09 NYB added "and LoopTo GT Start" --->
							<INPUT TYPE="submit" NAME="Back" value="phr_quiz_Back" class="btn btn-primary" onclick="submitHandler(this);">				<!--- 2013-02-27 PPB Case 433826 translation --->
						</CFIF>
						<CFIF (Start + QuestionsPerPage - 1) GTE NumberOfQuestions>
							<CF_INPUT TYPE="submit" NAME="Continue" value="#submitText#" onclick="javascript: document.Quiz.frmContinue.value=1;submitHandler(this);">
						<CFELSEIF (Start + QuestionsPerPage - 1) LT NumberOfQuestions>
							<INPUT TYPE="submit" NAME="Forward" value="phr_quiz_Forward" class="btn btn-primary" onclick="submitHandler(this);">		<!--- 2013-02-27 PPB Case 433826 translation --->
						</CFIF>
					</div>
				</div>
			</CFOUTPUT>
		<CFELSE>
			<CFOUTPUT>
				<div class="form-group">
					<CF_INPUT TYPE="submit" NAME="Continue" value="#submitText#"onclick="javascript: document.Quiz.frmContinue.value=1;submitHandler(this);"  class="btn btn-primary">
				</div>
			</CFOUTPUT>
		</CFIF>
	<cfelse>
		<cfset errorStruct = structnew()>
		<cfset errorStruct.QuestionIDs = ''>
		<cfif isdefined('Question')>
		<cfset errorStruct.QuestionIDs = Question>
		</cfif>

		<cfset errorStruct.message = "QuizTakenID=" & QuizTakenID & " QuestionIDs=" & errorStruct.QuestionIDs>
		<cfset errorStruct.QuizTakenID = QuizTakenID>
		<cfset application.com.errorHandler.recordRelayError_Warning(severity="WARNING",type="Quiz Without Questions",caughtError=errorStruct,sendEmail=true)>
		phr_elearning_theQuizHasNotYetBeenSetUpProperly.
	</cfif>
	<cfoutput>

		</div><!--- end form-horizontal --->
	</FORM>
	</div>

	<!--- WAB 2016-06-10 BF-977  
			When this was recoded as Ajax, it was changed from a POST to a GET.
			This was a bad idea because
			a) all updates should be done as POSTS for XSRF security reasons 
			b) GETs should be idempotent
			and c) (what caused an actual bug) it failed when URL got too long
			
			So reinstated a POST, and also the encrypted URL
	--->

    <cfset encryptedLink = application.com.security.encryptURL('questions.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')><!--- PJP CASE:433062: Added in random gen num for possible caching issue in frames --->
	<SCRIPT type="text/javascript">

		function submitHandler(btn) {
			var button = jQuery(btn);
			/* serialize form as an object and add the button name/value */
			var formData = button.parents('form##Quiz').serializeArray()
			formData.push ({name:button.attr('name'), value:button.attr('value')})
			quizSubmit(formData);
		}

		function quizSubmit(formData){
		    jQuery("##quizContainer").load(
		    								"#encryptedLink#"
		    								, formData
		    								);
		    document.getElementById('timerContainer').scrollIntoView(true);
		    return false;
		}
	</script>
	</cfoutput>
