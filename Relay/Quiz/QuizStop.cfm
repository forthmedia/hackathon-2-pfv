<!--- �Relayware. All Rights Reserved 2014 --->

<!--- remove this when not testing --->
<CFPARAM NAME="QuizDetailID" DEFAULT="1">

<!--- go to error page if required variables are not set --->
<CFIF NOT IsDefined("QuizDetailID")>
	<cf_translate>
		phr_eLearningQuizIDNotDefinedError
	</cf_translate>
	<CF_ABORT>
</CFIF>

<!--- First Get the instuctions text for this QuizDetail --->

<cfset GetQuizInfo = application.com.relayQuiz.getQuizInfo(quizDetailID=getquiztakenInfo.QuizDetailID)>
<cfset phrase = GetQuizInfo.LegalPhraseTextID>

<CFOUTPUT>
<CF_TRANSLATE>
		phr_header_quizDetail_#getquiztakenInfo.QuizDetailID#
		#phrase#
</CF_TRANSLATE>
</CFOUTPUT>


<FORM ACTION="Start.cfm" METHOD="POST">
	<CFOUTPUT><CF_INPUT TYPE="Hidden" NAME="QuizDetailID" VALUE="#QuizDetailID#"></CFOUTPUT>
	<INPUT TYPE="Submit" VALUE="I understand the instructions - Start Quiz">
</FORM>