<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History
2008-08-09	NYB   	P-SOP003 - Amended Allowed Attempts - wasn't working
2008-10-13	NYB		Sophos-2. 	added OrderMethod to RelayTag
2008-10-28	NYB  		P-SOP008
2009-03-09 	NYB		Stargate2 - was not allowing user to take the quiz more then once regardless of what is in setup
2009-03-30	NYB		Sophos Bugzilla 1777
2009-03-11	NJH		P-SNY047 - doubled the row height of the timer frame and some other checks if the quiz had not been set up properly.
2009-10-08	NYB		LHID2739 - made a number of improvements to the code - updated for version control
2011-09-16 	NYB		P-SNY106 - introduction of stringencyMode - if of type Strict then various behaviours will be altered
2012-04-10	STCR	P-REL109 Phase 1 - based on start.cfm in revn 7522.
2013-03-21	NYB 	Case 434096 added TimeLeft as a param
2013-04-16	STCR	CASE 432328 Question OrderMethod
2013-05-10	STCR	CASE 434166 Ensure PassMark is populated on QuizTaken record
2014-01-31 	NYB 	Case 438543 replaced CFLOOP with a function call
2014-02-11 	NYB 	Case 438543 Passed OrderMethod to updateQuiz and got rid of call to getQuiz as this is being done by updateQuiz anyway, renamed updateQuiz to getQuiz
2015-05-13	PYW		Case 444500	Fix XSS vulnerabilities.
2015-11-20	PYW		P-TAT006 BRD 31. Kanban 1745. Fix X-frame issue and ajax returned data format for timer.cfm and questions.cfm
2016-06-28  DAN     Case 449661 - fixes for selecting correct number of questions per each question pool specified in a quiz, take into account the actual number requested from each pool

--->

<!--- 2015-05-13 PYW P-TAT006 BRD 31. Kanban 1745 Fix X-frame issue. --->
<cfset application.com.request.allowFramingByAllSites()>

<cfsetting showdebugoutput="false">
<cf_displayBorder
	noborders=true
>
<cfset encryptionTest = application.com.security.confirmFieldsHaveBeenEncryptedWithFeedback("QuizDetailID")>
<cfif not encryptionTest.isOK>
	<cfoutput>
	<script>
		window.close();
	</script>
	</cfoutput>
</cfif>

<CFPARAM NAME="frmReturnTo" DEFAULT="quizList.cfm">
<cfparam name="userModuleProgressID" type="numeric" default="0"> <!--- NJH 2008-09-03 elearning 8.1 - a quiz taken is tied to a user module progress record --->
<cfparam name="moduleID" type="numeric" default="0">
<!--- 2008-10-16 NYB added: --->
<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder" /><!--- question order. Alpha not implemented. --->
<!--- START: NYB 2011-09-16 P-SNY106 --->
<cfparam name="stringencyMode" default="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="showIncorrectQuestionsOnCompletion" default="false" type="boolean">
<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
<!--- 2015-05-13 PYW Case 444500 Fix XSS vulnerabilities. Add type check to cfparam --->
<cfparam name="reviewAnswersLink" default="false" type="boolean">
<!--- END: NYB 2011-09-16 P-SNY106 --->
<cfparam name="TimeLeft" type="numeric" default="0"> <!--- 2013-03-21 NYB Case 434096 added --->

<CFIF IsDefined("url.OrderMethod")>
	<CFSET OrderMethod = url.OrderMethod>
</CFIF>

<!--- go to error page if required variables are not set --->
<CFIF request.RelayCurrentUser.PersonID EQ "" OR NOT IsDefined("QuizDetailID")>
	<CFINCLUDE TEMPLATE="error.cfm">
	<CF_ABORT>
</CFIF>

<!--- get the information about this quiz --->
<cfset getQuizInfo = application.com.relayQuiz.getQuizInfo(quizdetailid=#QuizDetailID#)>

<!--- NJH 2009-02-06 P-SNY047 - stop if the quiz has no questions --->
<cfif getQuizInfo.possibleQuestions eq "">
	The quiz has no questions. Please contact an administrator.
	<CF_ABORT>
</cfif>

<cfscript>
	argumentsStruct = structNew();
	argumentsStruct.QuizDetailID = QuizDetailID;
	moduleID = getQuizInfo.moduleID;
	argumentsStruct.userModuleProgressID = userModuleProgressID;

	checkQuiz = application.com.relayQuiz.checkQuiz(QuizDetailID=argumentsStruct.QuizDetailID);
	quizPassed = false;

	if (userModuleProgressID eq 0 and moduleID neq 0) {
		argumentsStruct.userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=moduleID,personID=request.relayCurrentUser.personID);
	}
</cfscript>

<!--- NJH 2012/11/21 Case 432093 --->
<cfoutput>
<script>
	window.parent.refreshModuleStatus(#moduleID#,'started');
</script>
</cfoutput>

<!--- check to see if user has attempted the quiz already --->
<!--- <cfset checkQuiz = application.com.relayQuiz.checkQuiz(argumentCollection=argumentsStruct)>
<cfset quizPassed = false> --->

<CFIF checkQuiz.recordcount GT 0>
	<cfloop query="checkQuiz">
		<cfset QuizTakenID = QuizTakenID>
		<cfset TimeLeft = TimeLeft>
		<cfset Completed = Completed>
		<cfset passMark = passMark>
		<cfset score = score>
		<cfbreak>
	</cfloop>

	<!--- NJH 2009-02-06 P-SNY047 - added neq "" as there is no validation in the front end currently--->
	<cfif passMark neq "" and score neq "" and passMark lte score>
		<cfset quizPassed = true>
	</cfif>
</CFIF>

<!--- START: NYB 2011-09-16 P-SNY106 --->
<CFIF checkQuiz.recordcount gt 0 and not IsDefined("url.allowRedo") AND checkQuiz.forceCompletion eq 1>
	<cfset QuizTakenID = checkQuiz.QuizTakenID>
	<CFINCLUDE TEMPLATE="finish.cfm"><!--- STCR --->
	<CF_ABORT>
</CFIF>
<!--- END: NYB 2011-09-16 P-SNY106 --->
<!--- P-SOP003 2008-08-09 NYB - copied from questions.cfm-> --->
<CFIF (checkQuiz.recordcount GTE getQuizInfo.NUMBEROFATTEMPTSALLOWED and Completed neq "") or quizPassed>
	<!--- START:  NYB 2009-03-09 Stargate2 quiz attempts bug ---
	<CFIF TimeLeft LTE 0 OR Completed NEQ "">
	!--- WITH:  NYB 2009-03-09 --->
	<!--- NYB 2011-09-16 P-SNY106 - changed line:--->
	<CFIF (not quizPassed) and checkQuiz.recordcount GTE getQuizInfo.NUMBEROFATTEMPTSALLOWED>
	<!--- END:  NYB 2009-03-09 --->
		<!--- NYB 2011-09-16 P-SNY106 - changed line:--->
			<CFOUTPUT><cf_translate>phr_elearning_MaximumAttemptsCompleted</cf_translate></CFOUTPUT><cf_abort>
	<!--- NYB 2009-03-09 Stargate2 quiz attempts bug - added ' and not IsDefined("url.allowRedo")' --->
	<CFELSEIF not quizPassed and not IsDefined("url.allowRedo")>
		<CFOUTPUT>
			<!--- START:  NYB 2009-03-09 Stargate2 quiz attempts bug - added --->
			<!--- 2014-04-14	YMA	Case 434303 Test Feedback to users --->
			<script language="Javascript">
				function redoQuiz() {
					url = window.location;
					window.location = url + '&allowRedo&stringencyMode=#jsStringFormat(stringencyMode)#&showIncorrectQuestionsOnCompletion=#jsStringFormat(showIncorrectQuestionsOnCompletion)#&reviewAnswersLink=#jsStringFormat(reviewAnswersLink)#';
				}
			</script>
			<!--- END:  NYB 2009-03-09 --->
			<!--- NJH 2009-03-17 P-SNY047 - removed: --->
			<!--- <cf_translate><p class="quizCompleted">phr_elearning_QuizCompleted</p></cf_translate> --->
			<!--- NYB 2009-03-31 Sophos Bugzilla 1777 - put text back as Sophos expect & use it: --->
			<cf_translate><p class="quizCompleted">phr_elearning_QuizCompleted</p></cf_translate>
		</CFOUTPUT>
	</CFIF>
	<!--- START:  NYB 2009-03-09 Stargate2 quiz attempts bug - added if tag --->
	<!--- START: NYB 2011-09-16 P-SNY106 --->
	<cfif not IsDefined("url.allowRedo")>
		<cfset QuizTakenID = checkQuiz.QuizTakenID>
		<CFINCLUDE TEMPLATE="finish.cfm">
		<CF_ABORT>
	</cfif>
	<!--- END: NYB 2011-09-16 P-SNY106 --->
	<!--- END:  NYB 2009-03-09 --->
</CFIF>
<!--- <-P-SOP003 --->

<!--- P-SOP003 2008-08-09 NYB - removed as is incorporated with above check-> ---
<!--- if the user has already completed the quiz, ie, no time left --->
<CFIF checkQuiz.RecordCount GE getQuizInfo.NumberOfAttemptsAllowed
		AND (checkQuiz.TimeLeft LT 1 OR checkQuiz.Completed NEQ "")>
	You have already taken the test the maximum number of times.
	<CF_ABORT>
</CFIF>
!--- <-P-SOP003 --->

<!--- set variables --->
<CFLoop QUERY="getQuizInfo">
	<CFSET QuizDetailID = QuizDetailID>
	<CFSET TimeAllowed = TimeAllowed>
	<CFSET NumToChoose = NumToChoose>
	<CFSET FreezeDate = CreateODBCDateTime(request.requestTime)>
</CFloop>


<!--- NYB 2009-10-08 LHID2739 - removed output if NumPossQs LT NumToChoose --->

<!--- NYB 2009-10-08 LHID2739 - moved content *1... --->

<!--- NYB 2008-08-09 P-SOP003 - added: AND checkQuiz.Completed NEQ "" --->
<CFIF checkQuiz.RecordCount GT 0 AND TimeLeft GT 0 AND Completed EQ "">
	<!--- NYB 2011-09-16 P-SNY106 - added stringencyMode: --->
	<!--- 2014-02-11 NYB Case 438543 Passed OrderMethod and got rid of call to getQuiz as this is being done by updateQuiz anyway - doesn't need to be done twice, renamed updateQuiz to getQuiz --->
	<cfset getQuiz = application.com.relayQuiz.updateQuiz(QuizTakenID=#QuizTakenID#,stringencyMode=stringencyMode,OrderMethod=OrderMethod)>

<CFELSE>

    <cfset actualQuestionIDs = "">
    <cfset quizQuestionPools = application.com.relayQuiz.getQuizQuestionPools(QuizDetailID=QuizDetailID)>
    <cfloop query="#quizQuestionPools#">
        <!--- random select requested number of questions from each question pool --->
        <cfset poolQuestions = application.com.relayQuiz.getQuizQuestionList(QuestionPoolID=QuestionPoolID)>
        <cfset poolQuestionIDs = ValueList(poolQuestions.QuestionID) />
        <cfset randomPoolQuestionIDs = application.com.globalfunctions.RandomSelectFromList(poolQuestionIDs, quizQuestionPools.numOfQuestions)>
        <cfset actualQuestionIDs = listAppend(actualQuestionIDs, randomPoolQuestionIDs)>
    </cfloop>
    
    <cfset questionCount = ListLen(actualQuestionIDs)>
    <!--- total number of questions should not be greater than amount actual question --->
    <cfif NumToChoose gt questionCount>
        <cfset NumToChoose = questionCount>
    </cfif>


	<cfif stringencyMode eq "Strict" and checkQuiz.recordcount gt 0>
		<cfset alreadyUsedQuestions = "">
		<cfloop index="i" from="1" to="#checkQuiz.recordcount#">
			<cfset alreadyUsedQuestions = ListAppend(alreadyUsedQuestions,checkQuiz.Questions[i])>
		</cfloop>
		<cfloop index="i" list="#alreadyUsedQuestions#">
            <!--- strict mode does not allow to have more questions than specified in the given quiz settings --->
			<cfif questionCount gt NumToChoose>
				<cfset usedQid = ListFind(actualQuestionIDs,i)>
				<cfif usedQid neq 0>
					<cfset actualQuestionIDs = ListDeleteAt(actualQuestionIDs,usedQid)>
				</cfif>
			</cfif>
		</cfloop>
	</cfif>

	<cfset getQuiz = application.com.relayQuiz.createQuiz(QuizDetailID=#QuizDetailID#,ActualQs=#actualQuestionIDs#,TimeAllowed=#TimeAllowed#,FreezeDate=#FreezeDate#,userModuleProgressID=#argumentsStruct.userModuleProgressID#,PassMark=getQuizInfo.PassMark)>  <!--- 2013-11-19 NYB passing in PassMark req'd for Case 437785 --->

	<cfset "session.quiztaken#getQuiz.QuizTakenID#.justCreated" = "true">
</CFIF>

<CFSET Started = getQuiz.Started>
<CFSET QuizTakenID = getQuiz.QuizTakenID>
<CFSET TimeLeft = getQuiz.TimeLeft>
<CFSET OrigTimeLeft= TimeLeft>


<!--- create the timer and question frames --->
<!--- 2008-10-13 NYB added OrderMethod to questions string --->
<!--- 2008-10-16 NYB changed string passed: --->
<!--- PJP CASE:433062: Added in random gen num for possible caching issue in frames --->
<cfset randomGenUrl = createuuid()>
<cfoutput>
<div id="timerContainer"></div>
<div id="quizContainer"></div>
<script>
	//jQuery(document).ready(function(){
		<cfset encryptedLink = application.com.security.encryptURL('timer.cfm?TimeAllowed=#TimeAllowed#&QuizTakenID=#QuizTakenID#&Started=#URLEncodedFormat(Started)#&OrigTimeLeft=#TimeLeft#&TimeLeft=#TimeLeft#&r=#randomGenUrl#')><!--- PJP CASE:433062: Added in random gen num for possible caching issue in frames --->
		<cfoutput>
			jQuery("##timerContainer").load('#encryptedLink#');
		</cfoutput>

		<cfset encryptedLink = application.com.security.encryptURL('questions.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&r=#randomGenUrl#&reviewAnswersLink=#reviewAnswersLink#')><!--- PJP CASE:433062: Added in random gen num for possible caching issue in frames --->
	    <cfoutput>
			jQuery("##quizContainer").load('#encryptedLink#');
		</cfoutput>
	    
	//});
</script>
</cfoutput>
<!--- NJH 2009-03-11 P-SNY047 - doubled the row height for the timer as it was getting cut off. Was 15px --->
<!--- <FRAMESET ROWS="*,*">
    <CFOUTPUT>
	<cfset encryptedLink = application.com.security.encryptURL('timer.cfm?TimeAllowed=#TimeAllowed#&QuizTakenID=#QuizTakenID#&Started=#URLEncodedFormat(Started)#&OrigTimeLeft=#TimeLeft#&TimeLeft=#TimeLeft#&r=#randomGenUrl#')><!--- PJP CASE:433062: Added in random gen num for possible caching issue in frames --->
	<FRAME NAME="timer" SRC="#encryptedLink#" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0">
	<!--- 2014-04-14	YMA	Case 434303 Test Feedback to users --->
	<cfset encryptedLink = application.com.security.encryptURL('questions.cfm?QuizDetailID=#QuizDetailID#&QuizTakenID=#QuizTakenID#&frmReturnTo=#urlencodedformat(frmReturnTo)#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&r=#randomGenUrl#&reviewAnswersLink=#reviewAnswersLink#')><!--- PJP CASE:433062: Added in random gen num for possible caching issue in frames --->
    <FRAME NAME="questions" SRC="#encryptedLink#" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="auto" FRAMEBORDER="0">
	</CFOUTPUT>
</FRAMESET> --->