<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author PKP

DATE : 2008-03-27

A place to put all quiz related functions

Amendments
2008-10-13	NYB		removed DescriptionPhraseTextID from throughout code as this is no longer a column in the QuizDetail table

 --->



<CFIF isDefined('quizGroupID')>
	<cfset quizGroupID= quizGroupID>
<CFELSE>
	<cfset quizGroupID= 0>
</CFIF>
<cfset GetQuizes = application.com.relayQuiz.quizlist(quizGroupID= #quizGroupID#)>

<CF_TRANSLATE>
<CFOUTPUT>

<TABLE border="0" cellpadding="5" cellspacing="0" width="500" align="center">
	<TR>
		<TD colspan="2">
			<H1>phr_quizListIntroHeading</H1>
		</TD>
		<TD ALIGN="right"><img src="/images/MISC/small_webt_o.gif" alt="" width="64" height="46" border="0">&nbsp;&nbsp;</TD>		
	</TR>
	<TR>
		<TD colspan="3">
			phr_quizListIntroText
		</TD>
	</TR>

	<cfif getQuizes.recordCount GT 0>
		<CFLOOP QUERY="GetQuizes">	
			<TR>
				<td valign="top">
					<A HREF="start.cfm?quizdetailID=#quizDetailID#&frmPersonID=#request.relayCurrentUser.personid#"><B>#htmleditformat(QuizName)#</B></A>
				</td>
				<td valign="top"><cfif completed neq ""><B>phr_DateofTest:</B> <br>#dateformat(completed,'dd-mmm-yy')#<CFELSE><FONT COLOR="Red">Test not taken</FONT></CFIF></td>
				<td valign="top"><cfif completed neq ""><B>phr_TestScore:</B> <br>#htmleditformat(Score)#/#htmleditformat(NumToChoose)# (#htmleditformat(percentage)#%)</CFIF></td>
			</TR>
			<!--- 2008-10-13 NYB removed DescriptionPhraseTextID as it no longer exists in table -> ---		
			<TR>
				<TD colspan="3">
					<CFSET descriptionText = "phr_#descriptionPhraseTextID#">
					#descriptionText#<BR><BR>
				</TD>
			</TR>
			!--- <- NYB --->		
		</CFLOOP>
	<CFELSE>
			<TR>
				<TD align="cENTER" colspan="3">
				phr_quizListNoQuizesAvailable
				</TD>
			</TR>
	</CFIF>
</TABLE>
</CFOUTPUT>
</CF_TRANSLATE>


