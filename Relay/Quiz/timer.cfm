<!---  �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History

NYB	2009-05-19	removed hard coded HTTP
NYB 2009-07-16 CR-SNY047
	- replaced written text with translatable phrases
	- replaced variable name NewTimeLeft with form.TimeRemaining (as this will now be used in Phrases - ie, seen by clients)
DAN 2016-06-20 Case 450067 - encrypt url to pass the encryption test and prevent window closing

--->

<cfset encryptionTest = application.com.security.confirmFieldsHaveBeenEncryptedWithFeedback("TimeAllowed,QuizTakenID,Started,OrigTimeLeft,TimeLeft")>
<cfif not encryptionTest.isOK>
	<cfoutput>
	<script>
		window.close();
	</script>
	</cfoutput>
</cfif>

<CFIF TimeAllowed IS "" OR QuizTakenID IS "" OR Started IS "">
	<CFINCLUDE TEMPLATE="start.cfm">
	<CF_ABORT>
</CFIF>

<!--- NJH 2009/07/06 CR-SNY047 - use request.requestTime instead of now() as we use the DB time to start the quiz --->
<CFSET theDiff = DateDiff("n", Started, request.requestTime)>
<CFSET form.TimeRemaining = OrigTimeLeft - theDiff>

<CFSET application.com.relayQuiz.setQuizTakenTimeLeft(QuizTakenID=QuizTakenID,NewTimeLeft=form.TimeRemaining)>

<CFSET TimeOutMsg = application.com.relayTranslations.translatePhrase(phrase="Quiz_TimeOutMsg")>

<CFOUTPUT>
<SCRIPT type="text/javascript">

var counter;
var quizForm;

function countdown() {
	counter=#form.TimeRemaining#;
	if(counter < 1) {
		window.alert("#TimeOutMsg#");  //NYB 2009-07-16 CR-SNY047 - replaced contents with [phrase] variable
		quizForm = document.Quiz;
		quizForm.frmContinue.value=1;
		quizSubmit('');
	} else {
		// NYB 2009-05-19  removed hard coded HTTP
		//window.location.href = "#request.currentSite.httpProtocol##request.currentSite.domainandroot##ThisDir#/timer.cfm?TimeAllowed=#TimeAllowed#&QuizTakenID=#QuizTakenID#&Started=#URLEncodedFormat(Started)#&OrigTimeLeft=#OrigTimeLeft#&TimeLeft="+counter;

		<cfset encryptedLink = application.com.security.encryptURL('timer.cfm?TimeAllowed=#TimeAllowed#&QuizTakenID=#QuizTakenID#&Started=#URLEncodedFormat(Started)#&OrigTimeLeft=#OrigTimeLeft#&TimeLeft=#form.TimeRemaining#')>
		jQuery("##timerContainer").load("#encryptedLink#");

	}
}


	setTimeout('countdown()',#IIF(form.timeRemaining gte 1,DE('60000'),DE('0'))#)


</SCRIPT>

<!--- CASE 425872 NJH 2012/03/13 If 0 minutes remaining, we don't want to have to wait a minute to run the countdown function --->
<!--- <cf_body onLoad="setTimeout('countdown();',#IIF(form.timeRemaining gte 1,DE('60000'),DE('0'))#)"> --->
</CFOUTPUT>

<!--- START:  NYB 2009-07-16 CR-SNY047 - added following for phrasetext--->
<CFIF form.TimeRemaining EQ 1>
	<CFSET form.Minutes = application.com.relayTranslations.translatePhrase(phrase="minute")>
<CFELSE>
	<CFSET form.Minutes = application.com.relayTranslations.translatePhrase(phrase="minutes")>
</CFIF>
<!--- END:  2009-07-16 CR-SNY047 --->

<CFOUTPUT>
<cf_translate>
<!--- START:  NYB 2009-07-16 CR-SNY047 - replaced ---
<p>Total time to complete this test is #TimeAllowed# minutes, <CFIF NewTimeLeft LTE 2><B></CFIF>you have #NewTimeLeft# minute<CFIF NewTimeLeft NEQ 1>s</CFIF> remaining.<CFIF NewTimeLeft LTE 2></B></CFIF>
!--- WITH:  2009-07-16 CR-SNY047 --->
phr_Quiz_TimeRemaining
<!--- END:  2009-07-16 CR-SNY047 --->
</cf_translate>
</CFOUTPUT>


