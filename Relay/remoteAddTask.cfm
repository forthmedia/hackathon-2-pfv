<!--- �Relayware. All Rights Reserved 2014 --->

<CFSILENT>
<!---
File name:			remoteAddTask.cfm
Author:				SWJ
Date started:		2001-01-01

Description:

This template handles inserts from person, location and organisation.  The following
fields are significant or mandatory:
		insSiteName - this is not found it will default to "People with no Company"
		insFirstName - mandatory
		insLastName - mandatory
		insEmail - mandatory
		insCountryID - if not defined or not numeric it will use insCountryISO
There is a testing page called testRemoteAdd.htm

Processing is as follows:

1.  Organisation table is searched using orgMatchName, TOP 1 order by lastUpdated.
	If insSiteName and orgname don't match a record is added with a default country.
	If 1 record is found this is orgID is used and no update takes place.
2.  Location record is checked. If insSiteName and sitename don't match a
	record is added	with a default country
3.  Person record is checked. If insFirstName, insLastName and insEmail don't
	match their respective columns a record is added.

If the required fields are not found a message is returned to frmNext.

Upon successful processing processing is redirected to frmNext with the outome in
the message URL variable.

Additional options:
1. To stop the message being returned simply add a hidden form variable as follows
to the capture form:
<input type="hidden" name="frmReturnMessage" value="yes">

2. If the form variable insSiteName is "" then by default an organisation record
is added called "People with no Company".  An organisation record will automatically
be added per country for the first person who adds their details to the database in
that country.  You can change the default by passing a hidden form variable called
insSiteName.

3.  You'll be pleased to know that the handler does not need to use our countryID's.  It was designed to work with two character ISOCodes as an alternative.  So if you call your country select insCountryISO option value country ISO code instead of your internal codes then it will work fine.  See example below.  You must make sure you do not pass insCountryID
  <select name="insCountryISO">
       <option value="0">Select a Country</option>
       <option value="GB">United Kingdom</option>
       <option value="DE">Germany</option>
  </select>

4.  To set the profile data you will need three hiddens passed in your form.
<input type="hidden" name="frmFlagID" value="profile attribute value that you are setting">
<input type="hidden" name="frmEntityTypeID" value="0">
<input type="hidden" name="frmvalue" value="1">
Amy and Maxine have set up a set of profile attributes against an profile called "End User Information" there are two attributes set up against these 1924 EndUserCommunication and 1925 EndUserUnsubscribe so you would use these in the frmFlagID above as required.  You may need to call me to discuss this.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-09-20 	SWJ	removed the organisation insert in the code, removed all of the archive code
				and simplified the insert and update statements.
2002-12-18 	SWJ	modified the <cftransaction> code to rollback
2005-07-11 	NM  Org match name checking before insertion and country / global scope definition
2005-07-26		WAB removed default of "English" on insLanguage.  Stays blank.  Actual language used will e determined by browser or count	ry
2005-10-05	WAB Inserts now return the inserted ID via a trigger
			WAB search for existing person on firstname/lastname/email is now constrained by locationid.
2006-05-04  	AJC P_COR001 If address1 is blank then populates with firstname surname request.requesttime
2006-06-19 	GCC P_SNY039 Implemented extended match keys for VAT required companies
2007-01-15  SSS If InsorganisationID or InsLocationID is passed do not insert a location.
2007-03-16	NJH	Change the orgMatchName to include the VAT number if it was required, and used this to pass to checkOrgMatchName
2007-04-16	AJC Add new field for Organisation (organisationTypeID) defaults to 1
2007-05-02	AJC Check for organisation Type specific profile summary screen
2008-11-13	NJH Bugs Sony Bug Fix Issue 1315 - Added the cf_translate tags and added a phr_ to the person action and org action.
2009-04-30	NYB	Sophos - moved checkLoc & checkPer to com/dbTools.cfc
2009-05-12	NYB 	 added Location dedupe code
2010-03-02      SSS     Sophos bonanza
2010-11-04	NAS	LID4530 - Existing Customers not appearing for other Partners
2011-05-19	NAS	LID5947/8 & 6329
2011-05-26	NAS	LID5984
2012-01-17	NYB Case425166 rewrote logic around insEmail
2012-02-22 	PPB Case 426657 person fax number
2012-02-23 	NJH CASE 426744 - added server side unique email validation check
2012-07-26	IH	Case 429145 Fix back link to work on IE
2012-07-31	IH	Case 429379 fix error, change to cf_transaction
2015-05-06	NJH	Pass in specificURL to insertLocation to set the location website.
2015-06-30	PYW	Case 445160 Store organisation notes.
2015-10-26  DAN Case 446091 - check if person result is OK otherwise log error and rollback
2015-10-30  DAN PROD2015-260 - updates to log the errors after tx rollback
2016/09/01	NJH	JIRA PROD2016-1190 - replace code that called entity matching and entity inserting functions in dataloads with relayEntity and matching functions.
2016-11-09  DAN PROD2016-2643 - fix for setting lead data on new lead creation to match the existing location if domain matched

Possible enhancements:
1.  Convert each table process into a stored procedure.
2.  Convert this template into a component
3.  Secure this as a component
4.  Make this universal
6.  Create an app Var application.defaultCountryID

--->

<CFPARAM NAME="frmNext" DEFAULT="return">
<cfparam name="frmReturnMessage" default="yes">

<!---  these defaults are not always passed--->
<CFPARAM NAME="insCountryISO" DEFAULT="GB">
<CFPARAM NAME="insOrgURL" DEFAULT="">
<CFPARAM NAME="insOfficePhone" DEFAULT="">
<CFPARAM NAME="insOfficePhoneExt" DEFAULT="">
<CFPARAM NAME="insEmail" DEFAULT="">
<CFPARAM NAME="insMobilePhone" DEFAULT="">
<CFPARAM NAME="insLanguage" DEFAULT="">
<CFPARAM NAME="insVATnumber" DEFAULT="">
<CFPARAM NAME="insAddress1" DEFAULT="">
<CFPARAM NAME="insAddress2" DEFAULT="">
<CFPARAM NAME="insAddress3" DEFAULT="">
<CFPARAM NAME="insAddress4" DEFAULT="">
<CFPARAM NAME="insAddress5" DEFAULT="">
<CFPARAM NAME="insAddress6" DEFAULT=""> <!--- NJH 2006-12-05 added address6-address9 to capture physical address fields which the Netherlands currently captures.--->
<CFPARAM NAME="insAddress7" DEFAULT="">
<CFPARAM NAME="insAddress8" DEFAULT="">
<CFPARAM NAME="insAddress9" DEFAULT="">
<CFPARAM NAME="insAddress10" DEFAULT="">  <!--- NJH 2008-01-16 used for storing locator region table (for Trend) --->
<CFPARAM NAME="insPostalCode" DEFAULT="">
<CFPARAM NAME="insTelephone" DEFAULT="">
<CFPARAM NAME="insFax" DEFAULT="">
<CFPARAM NAME="insLocEmail" DEFAULT="">  <!--- NJH 2008-01-16 added for Trend --->
<CFPARAM NAME="insSalutation" DEFAULT="">
<cfparam name="insJobDesc" default="">
<cfparam name="insUserName" default="">
<cfparam name="insSex" default="">
<cfparam name="insDOBYear" default="">
<cfparam name="insDOBMonth" default="">
<cfparam name="insDOBDay" default="">
<cfparam name="insDOB" default="">
<cfparam name="insaka" default="">
<CFPARAM NAME="insOrganisationTypeID" DEFAULT="1"> <!--- AJC 2007-04-16 to capture OrganisationTypeID --->
<cfparam name="insR2LocId" default="">
<cfparam name="insR2PerId" default="">
<cfparam name="insFaxPhone" default="">	<!--- 2012-02-22 PPB Case 426657 --->
<cfparam name="insSpecificURL" default="">
<cfparam name="insNotes" default=""> <!--- 2015-06-30 PYW Case 445160 --->



<!--- check for the key fields which are
		insSiteName - this is not found it will default to "People with no Company"
		insFirstName
		insLastName
		insEmail
--->

<!--- CASE 426744 NJH 2012/03/07 - changed how attributesError was handled. Make it a list and then output it, with a back link.--->
<CFSET AttributesError = "">

<!--- convert a numeric language into a name for use in the Person table - how horrible --->
<CFIF structKeyExists(form,"insLanguage") and form.insLanguage neq "" and isnumeric(form.insLanguage)>
	<cftry>
		<cfset form.insLanguage = application.languageLookupFromIDstr[form.insLanguage]>
		<cfcatch>
			<cfset form.insLanguage = "English">
		</cfcatch>
	</cftry>
</CFIF>

<CFIF structKeyExists(form,"insFirstName") and form.insFirstName neq "">
	<CFSET variables.insFirstName=form.insFirstName>							<!--- PPB 2010-02-22 explicitly set to variables scope else when called from relaywareAPI.insertPOL will update the arguments scope variable --->
<CFELSE>
	<CFSET AttributesError = listAppend(AttributesError,"The attribute insFirstName is Required.","<br>")>
</CFIF>

<!--- RMB - 2014-02-04 - P-KAS024 - 438673 -  Add insSecondName (thia field is not required) - STAR --->
<CFIF StructKeyExists(form, "insSecondName") and form.insSecondName neq "">
	<CFSET variables.insSecondName=form.insSecondName>
</CFIF>
<!--- RMB - 2014-02-04 - P-KAS024 - 438673 -  Add insSecondName (thia field is not required) - STAR --->

<CFIF structKeyExists(form,"insLastName") and form.insLastName neq "">				<!--- PPB 2010-02-22 explicitly set to variables scope else when called from relaywareAPI.insertPOL will update the arguments scope variable --->
	<CFSET variables.insLastName=form.insLastName>
<CFELSE>
	<CFSET AttributesError = listAppend(AttributesError,"The attribute insLastName is Required.","<br>")>
</CFIF>

<!--- if the form is submitted with a blank insSiteName then a company is by
	 default added per country  with the default name below --->
<CFIF structKeyExists(form,"insSiteName") and form.insSiteName neq "">
	<cfset variables.insSiteName = form.insSiteName> 							<!--- PPB 2010-02-22 explicitly set to variables scope else when called from relaywareAPI.insertPOL will update the arguments scope variable --->
<CFELSE>
	<CFSET variables.insSiteName= form.insFirstName & " " & form.insLastName>
</CFIF>
<!--- 2006-05-04 AJC P_COR001 if the form is submitted with a blank address1 then a location is by
	 default added with the default name and datestamp below --->
<CFIF structKeyExists(form,"insAddress1") and form.insAddress1 neq "">
	<cfset insAddress1 = form.insAddress1> 										<!--- PPB 2010-02-22 explicitly set to variables scope else when called from relaywareAPI.insertPOL will update the arguments scope variable --->
<CFELSE>
	<CFSET insAddress1= form.insFirstName & " " & form.insLastName & " " & request.requesttime>
</CFIF>

<!--- START: NYB 2012-01-17 Case425166 rewrote: --->
<cfif structKeyExists(form,"insEmail")>
	<cfif (structKeyExists(attributes,"MANDATORYCOLS") and ListFindNoCase(attributes.MANDATORYCOLS, form.insEmail) and form.insEmail eq "")> <!--- 2013/02/27	YMA	Fix to incorrect if statement --->
		<CFSET AttributesError = listAppend(AttributesError,"The attribute insEmail is Required.","<br>")>
	<cfelse>
		<CFSET variables.insEmail=form.insEmail> <!--- PPB 22/02/2010 explicitly set to variables scope else when called from relaywareAPI.insertPOL will update the arguments scope variable --->
	</cfif>

	<!--- CASE 426744: NJH 2012/02/23 Unique Email Validation Check --->
	<cfset uniqueEmailValidation = application.com.relayPLO.isUniqueEmailValidationOn()>
	<cfif form.insEmail neq "" and uniqueEmailValidation.isOn and not application.com.relayPLO.isEmailAddressUnique(personID=0,emailAddress=form.insEmail,orgType=insOrganisationTypeID).isOK>
		<cfset attributesError = listAppend(AttributesError,uniqueEmailValidation.uniqueEmailMessage,"<br>")>
	</cfif>
</cfif>
<!--- END: NYB 2012-01-17  --->

<CFIF AttributesError IS NOT "">
	<cfoutput><div class="errorblock">#application.com.security.sanitiseHTML(AttributesError)#<br>Please go <a href="javascript:window.history.back();">back</a> and correct the problem.</div></cfoutput>
	<CF_ABORT>
</CFIF>

<!--- if insCountryID is not supplied then we need to work out the country from the country ISOCode --->
<cfif not isDefined("insCountryID") OR not IsNumeric(insCountryID)>				<!--- PPB 2010-02-22 changed AND to OR --->
	<cfquery name="getCountry" datasource="#application.siteDataSource#">
		select countryID from country where ISOCode =  <cf_queryparam value="#insCountryISO#" CFSQLTYPE="CF_SQL_VARCHAR" > 			<!--- PPB 2010-02-22 put quotes around #insCountryISO# --->
	</cfquery>
	<cfif getCountry.recordcount eq 1>
		<cfset variables.countryID = getCountry.countryID>
	<cfelse>
		<cfset variables.countryID = 9>
	</cfif>

	<cfset insCountryID = variables.countryID>									<!--- PPB 2010-02-22 code below refers to insCountryISO and variables.countryID so set both  --->
<cfelse>
	<!--- otherwise make the countryID variable local to this template --->
	<cfset variables.countryID = insCountryID>
</cfif>

<CFSET variables.updatetime = CreateODBCDateTime(Now())>
<cfif isDefined("request.relayCurrentUser.personid")>
	<CFSET variables.updateUser = request.relaycurrentuser.usergroupID>
<cfelse>
	<CFSET variables.updateUser = 404>
</cfif>

<!--- Concatonate Date of Birth for insertion --->
<cfif insDOBYear neq "" and insDOBMonth neq "" and insDOBDay neq "">
	<cfset insDOB = CreateDate (insDOBYear,insDOBMonth,insDOBDay)>
</cfif>

<cfset commitIt = "Yes">

<!--- Case 446091 / PROD2015-260 --->
<cfset errorStruct = structnew()> <!--- holds arguments for errors to log after the transaction rollback --->
<cfset messageType = "success">

<cf_transaction action = "begin">
<cfif isdefined("InsorganisationID")>
	<cfset thisOrgID = InsorganisationID>
	<cfset thisOrgAction = "found">
<cfelse>
	<!---   **********************************************************************
			insert the organisation record
	************************************************************************* --->

		<!--- NJH 2007-03-16 find out what the orgMatchKey is before we call checkOrgMatchname
		This was moved from down below where it was used just before inserting the organisation --->
		<!--- WAB 2010-03-17  need to use variables.countryid, not insCountryID (which isn't defined if done by ISOCODE--->
		<!--- GCC 2012-05-10 reinstated spaces around the application.delim1. Vital for removal / replacement of reserved words for org match name --->

		<!---  NJH 2016/08/17 JIRA PROD2016-1190  - done in insertOrganisation
		<cfset countryDetails = application.com.commonQueries.getCountry(countryid=variables.countryid)>
		<cfif countryDetails.VATrequired>
			<cfset orgMatchKey = insSiteName & " " & application.delim1 & " " & insVATnumber>
		<cfelseif isdefined("request.orgMatchNameMethodID") and request.orgMatchNameMethodID eq 2>
			<cfset orgMatchKey = replace("#insOrgURL#","http://","")>
		 <!--- 2011-05-19	NAS	LID5984 --->
		<cfelseif isdefined("request.useVATinMatchName") and request.useVATinMatchName>
			<cfset orgMatchKey = insSiteName & " " & application.delim1 & " " & insVATnumber>
		<cfelse>
			<cfset orgMatchKey = insSiteName>
		</cfif> --->

		<!--- test orgname for duplicates --->
		<!--- NJH 2007-03-16 replaced insSiteName with orgMatchKey in the call to checkOrgMatchname --->
		<!--- <cfscript>
			updateExistingOrgs = application.com.dbTools.updateOrgMatchname(countryIDlist=variables.countryid);
			// 2011-05-19	NAS	LID5947/8 & 6329
			if (IsDefined("request.orgMatchNameMethodID") AND (request.orgMatchNameMethodID EQ "3"))
			{
				checkOrg = application.com.dbTools.checkOrgMatchname(dataSource=application.siteDataSource,orgName=orgMatchKey,countryID=variables.countryid,organisationTypeID=insOrganisationTypeID); // Case 447952
			}
			else
			{
				checkOrg = application.com.dbTools.checkOrgMatchname(dataSource=application.siteDataSource,orgName=orgMatchKey,countryID=variables.countryid); // Case 447952
			}
		</cfscript> --->

		<!--- NJH 2016/08/17 JIRA PROD2016-1190 - replaced above with new matching function --->
		<cfset orgDetails = {CountryID=variables.countryID,
						organisationName=variables.insSiteName,
						organisationtypeID=insOrganisationTypeID,
						AKA =INSAKA,
						orgURL=insOrgURL,
						Notes=insNotes,
						Active=1,
						VATnumber=insVATnumber}>

		<!--- first check whether we have this organisation in the database --->
		<!--- <cfquery name="checkOrg" datasource="#application.siteDataSource#">
			<!--- change this query to use matchname, TOP 1 and order by lastupdated --->
			SELECT TOP 1 organisationID from organisation
				where organisationName = '#insSiteName#'
				order by lastUpdated desc
		</cfquery> --->
		<!---
<cfif checkOrg.recordcount gte 1>  <!--- WAB 2005-10-04 changed from eq 1 to gte 1 to handle if there are two existing matches--->
			<!--- if we match only 1 then we should use this, otherwise
				  we should insert a new one.  This does not update any org data. --->
			<cfset thisOrgID = checkOrg.organisationID>
			<cfset thisOrgAction = "found">
		<cfelse>
			<!--- AJC 2007-04-16 Added OrganisationTypeID --->
			<cftry>
				<cfscript>
					thisOrgID = application.com.relayEntity.insertOrganisation(organisationDetails=orgDetails);
				</cfscript>
				<cfset thisOrgAction = "added">
				<cfcatch type = "DATABASE">
				    <cfset commitIt = "No">
					<cfset message = cfcatch.message & " QueryName:insNewOrganisation">
                    <cfset errorStruct.insNewOrganisation.argumentCollection = {type="Remote Add Task",Severity="Error",catch=cfcatch,warningStructure=form}> <!--- Case 446091 / PROD2015-260 --->
				</cfcatch>
			</cftry>
			<!--- add the dataSource --->
			<cftry>
				<CFQUERY NAME="insOrgDataSource" datasource="#application.siteDataSource#">
					INSERT INTO OrgDataSource(organisationID, DataSourceID, RemoteDataSourceID)
					VALUES(<cf_queryparam value="#thisOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')</CFQUERY>
				<cfcatch type = "DATABASE">
				    <cfset commitIt = "No">
					<cfset message = cfcatch.message & " QueryName:insOrgDataSource">
                    <cfset errorStruct.insOrgDataSource.argumentCollection = {type="Remote Add Task",Severity="Error",catch=cfcatch,warningStructure=form}> <!--- Case 446091 / PROD2015-260 --->
				</cfcatch>
			</cftry>

		</cfif>
 --->

		<cfset insertOrgResult = application.com.relayEntity.insertOrganisation(organisationDetails=orgDetails)>
		<cfset thisOrgID = insertOrgResult.entityID>
		<cfset thisOrgAction = insertOrgResult.errorCode eq "ENTITY_EXISTS"?"found":"added">
		<cfset message = insertOrgResult.message>
		<cfset commitIt = insertOrgResult.isOK>
</cfif>

<cfif isdefined("InsLocationID")>
	<cfset thisLocID = InsLocationID>
<cfelse>
<!---   **********************************************************************
		insert the location record
************************************************************************* --->
	<!--- first check whether we have this location in the database --->
	<!--- WAB 2005-10-04 added N's --->
	<!--- NYB 2009-04-30 Sophos - replaced actually query with function call: --->
	<!--- <cfset updateExistingLocs = application.com.dbTools.updateLocationMatchname(organisationIDlist=thisOrgID)>
	<cfset checkLoc = application.com.dbTools.checkLoc(SiteName=insSiteName,Address1=insAddress1,Address4=insAddress4,PostalCode=insPostalCode,OrgID=thisOrgID,countryid=variables.countryID)> --->

	<!--- NJH 2016/08/17 JIRA PROD2016-1190 - replaced above with new matching function. Actually, not needed here as done in insertEntity--->
	<!--- <cfset checkLoc = application.com.matching.getMatchesForEntityDetails(entityType="location",SiteName=insSiteName,Address1=insAddress1,Address4=insAddress4,PostalCode=insPostalCode,OrgID=thisOrgID,countryid=variables.countryID)>

	<cfif checkLoc.recordcount eq 1>
		<!--- if we match only 1  then we should use this, otherwise
			  we should insert a new one
			  --->
		<cfset thisLocID = checkLoc.locationID>
		<cfset thisLocAction = "found">
	<cfelse>

		<cftry>

			<!--- NJH 2016/08/17 Done in insertLocation
			<cfset locMatchKeyList=application.com.dbTools.getLocationMatchnameEntries()>
			<cfset locMatchName = "">
			<cfloop list="#locMatchKeyList#" delimiters="#application.delim1#" index="keyItem">
				<cfset keyItem = "ins" & keyItem>
				<cfif locMatchName eq "">
					<cfset locMatchName = evaluate(keyItem)>
				<cfelse>
					<cfset locMatchName = locMatchName & " " & application.delim1 & " " & evaluate(keyItem)>
				</cfif>
			</cfloop> --->


			<!--- NJH 2006-12-05 added address6-address9 --->
			<!--- PPB 2010-02-22 added R2LocId (for Sophos B-01962) --->
			<cfset locationDetails = {Address1="N'#insAddress1#'",
					Address2="N'#insAddress2#'",
					Address3="N'#insAddress3#'",
					Address4="N'#insAddress4#'",
					Address5="N'#insAddress5#'",
					Address6="N'#insAddress6#'",
					Address7="N'#insAddress7#'",
					Address8="N'#insAddress8#'",
					Address9="N'#insAddress9#'",
					Address10="N'#insAddress10#'",
					PostalCode="N'#insPostalCode#'",
					CountryID="#variables.countryID#",
					OrganisationID="#thisOrgID#",
					SiteName="N'#variables.insSiteName#'",
					Telephone="'#insTelephone#'",
					Fax="'#insFax#'",
					locEmail="'#insLocEmail#'",
					Active=1,
					R2LocId="N'#insR2LocId#'",
					createdBy="#variables.updateUser#",
					created="#variables.updatetime#",
					LastUpdatedBy="#variables.updateUser#",
					LastUpdated="#variables.updatetime#",
					accountTypeID="#insOrganisationTypeID#",
					specificURL="'#insSpecificURL#'"}>

			<cfscript>
				locInsertResult = application.com.relayEntity.insertLocation(locationDetails=locationDetails);
				thisLocID = locInsertResult.entityID;
			</cfscript>


<!---
	WAB 2005-10-04 removed and replaced with an insert trigger on the location table
			<!--- get the details for the record just added --->
			<cfquery name="getNewLocId" datasource="#application.siteDataSource#">
				SELECT LocationID
			FROM Location
			WHERE organisationID = #thisOrgID#
			  AND CreatedBy = #updateUser#
			  AND Created = #updatetime#
			  AND LastUpdatedBy = #updateUser#
			  AND LastUpdated = #updatetime#
			</cfquery>
			<cfset thisLocID = getNewLocId.LocationID>
--->
			<!--- <cfset thisLocID = insNewLocation.LocationID> --->
			<cfset thisLocAction = "added">
			<cfcatch type = "DATABASE">
			    <cfset commitIt = "No">
				<cfset message = cfcatch.message & " QueryName:insNewLocation">
                <cfset errorStruct.insNewLocation.argumentCollection = {type="Remote Add Task",Severity="Error",catch=cfcatch,warningStructure=form}> <!--- Case 446091 / PROD2015-260 --->
			</cfcatch>
		</cftry>
		<!--- add the dataSource --->
		<cftry>
                        <!--- 2008-01-22 GCC <cfqueryparam value="#thisLocID#" CFSQLType="CF_SQL_INTEGER"> removed because of intermittent errors caused by cfparam 'forgetting' its variable value--->
			<CFQUERY NAME="insLocDataSource" DATASOURCE="#application.siteDataSource#">
				INSERT INTO LocationDataSource(LocationID, DataSourceID, RemoteDataSourceID)
				VALUES(<cf_queryparam value="#thisLocID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</CFQUERY>
			<cfcatch type = "DATABASE">
			    <cfset commitIt = "No">
				<cfset message = cfcatch.message & " QueryName:insLocDataSource">
                <cfset errorStruct.insLocDataSource.argumentCollection = {type="Remote Add Task",Severity="Error",catch=cfcatch,warningStructure=form}> <!--- Case 446091 / PROD2015-260 --->
			</cfcatch>
		</cftry>

	</cfif> --->

	<!--- NJH 2016/08/17 JIRA PROD2016-1190 - replaced above with below call to insertLocation. --->
	<cfset locationDetails = {Address1=insAddress1,
					Address2=insAddress2,
					Address3=insAddress3,
					Address4=insAddress4,
					Address5=insAddress5,
					Address6=insAddress6,
					Address7=insAddress7,
					Address8=insAddress8,
					Address9=insAddress9,
					Address10=insAddress10,
					PostalCode=insPostalCode,
					CountryID=variables.countryID,
					OrganisationID=thisOrgID,
					SiteName=variables.insSiteName,
					Telephone=insTelephone,
					Fax=insFax,
					locEmail=insLocEmail,
					Active=1,
					R2LocId=insR2LocId,
					accountTypeID=insOrganisationTypeID,
					specificURL=insSpecificURL}>


	<cfset locInsertResult = application.com.relayEntity.insertLocation(locationDetails=locationDetails)>
	<cfset thisLocID = locInsertResult.entityID>
	<cfset thisLocAction = locInsertResult.errorCode eq "ENTITY_EXISTS"?"found":"added">
	<cfset message = locInsertResult.message>
	<cfset commitIt = locInsertResult.isOK>

</cfif>
<!---   **********************************************************************
		insert the person record
************************************************************************* --->
	<!--- WAB 2005-10-05 altered this query
		originally it looked for a matching person anywhere in the database and if a record was found
		then it was used that record and ignored everything that the u user had just added
		We changed so that it only looks in the current location.
		There is an argument for doing a search of the whole database,
		but it should be done at a different stage
	 --->

	<!--- first check whether we have this person in the database --->
	<!--- NYB 2009-04-30 Sophos - replaced actually query with function call: --->
	<!--- <cfset updateExistingPers = application.com.dbTools.updatePersonMatchname(countryIDlist=variables.countryid)> --->
	<!--- NJH 2010/10/25 P-FNL079 used insertPerson function in relayEntity in an effort to consolidate code --->
	<cftry>
	<cfset personDetails.Salutation = insSalutation>
	<cfset personDetails.FirstName = insFirstName>
	<!--- RMB - 2014-02-04 - P-KAS024 - 438673 -  Add insSecondName - START --->
	<CFIF StructKeyExists(variables, "insSecondName") and variables.insSecondName neq "">
		<CFSET personDetails.SecondName=variables.insSecondName>
	</CFIF>
	<!--- RMB - 2014-02-04 - P-KAS024 - 438673 -  Add insSecondName - END --->
	<cfset personDetails.LastName = insLastName>
	<cfset personDetails.Username = insUserName>
	<cfset personDetails.Password = "">
	<cfset personDetails.PasswordDate = now()>
	<cfset personDetails.LoginExpires = CreateODBCDate("01-Jan-92")>
	<cfset personDetails.FirstTimeUser = 1>
	<cfset personDetails.OfficePhone = insOfficePhone>
	<cfset personDetails.OfficePhoneExt = insOfficePhoneExt>
	<cfset personDetails.MobilePhone = insMobilePhone>
	<cfset personDetails.Email = insEmail>
	<cfset personDetails.JobDesc = insjobDesc>
	<cfset personDetails.Language = insLanguage>
	<cfset personDetails.LocationID = thisLocID>
	<cfset personDetails.OrganisationID = thisOrgID>
	<cfset personDetails.Active = 1>
	<cfset personDetails.R2LocId=insR2LocId>
	<cfset personDetails.R2PerId=insR2PerId>
	<cfset personDetails.sex=insSex>
	<cfset personDetails.DateOfBirth=insDOB>
	<cfset personDetails.FaxPhone=insFaxPhone>	<!--- 2012-02-22 PPB Case 426657 --->

	<cfset personInsResult = application.com.relayPLO.insertPerson(personDetails=personDetails,insertIfExists="false")>
	<cfif personInsResult.exists and listLen(personInsResult.entitiesMatchedList) gte 1>
		<cfset thisPerAction = "found">
	<cfelseif structKeyExists(personInsResult,"errorcode") and personInsResult.errorcode eq "NON_UNIQUE_EMAIL">
		<cfset thisPerAction = "found">
		<cfset personInsResult = application.com.relayentity.doespersonexist(personDetails=personDetails,columnsToUse="email")>
	<cfelse>
		<cfset thisPerAction = "added">
	</cfif>

    <!--- START: 2015-10-26 DAN Case 446091 --->
    <cfif personInsResult.ISOK and personInsResult.entityID GT 0>
        <cfset thisPersonID = personInsResult.entityID>
    <cfelse>
        <!--- something went wrong - rollback --->
        <cfset commitIt = "No">
        <cfset message = personInsResult.message & " QueryName:insNewPerson">
        <!--- note: using error message instead of cfcatch here --->
        <cfset errorStruct.insNewPerson.argumentCollection = {type="Remote Add Task",Severity="Error",message=message,warningStructure=personDetails}> <!--- Case 446091 / PROD2015-260 --->
    </cfif>
    <!--- END: 2015-10-26 DAN Case 446091 --->


	<!--- NJH 2010-10-25 P-FNL079 - replaced below with above
	<cfset checkPer = application.com.dbTools.checkPer(FirstName=variables.insFirstName,LastName=variables.insLastName,Email=variables.insEmail,LocID=thisLocID)>
	<cfif checkPer.recordcount eq 1>
		<!--- if we match only 1 then we should use this, otherwise
			  we should insert a new one --->
		<!--- 2010-11-04	NAS		LID4530 - Existing Customers not appearing for other Partners --->
		<cfset session.useThisPersonID = checkPer.personID>
		<cfset thisPersonID = checkPer.personID>
		<cfset thisPerAction = "found">
	<cfelse>
		<cftry>
		<!--- PPB 2010-02-22 added R2LocId (for Sophos B-01962) --->
		<cfscript>
			thisPersonID = application.com.relayDataload.insertPerson(
				Salutation="N'#insSalutation#'",
				FirstName="N'#variables.insFirstName#'",
				LastName="N'#variables.insLastName#'",
				Username="N'#insUserName#'",
				Password="''",
				PasswordDate="#Now()#",
				LoginExpires="#CreateODBCDate("01-Jan-92")#",
				FirstTimeUser=1,
				OfficePhone="'#insOfficePhone#'",
				OfficePhoneExt="'#insOfficePhoneExt#'",
				MobilePhone="'#insMobilePhone#'",
				Email="'#variables.insEmail#'",
				JobDesc="N'#insjobDesc#'",
				Language="'#insLanguage#'",
				LocationID="#thisLocID#",
				OrganisationID="#thisOrgID#",
				Active=1,
				R2LocId='#insR2LocId#',
				R2PerId='#insR2PerId#',
				createdBy="#updateUser#",
				created="#updatetime#",
				LastUpdatedBy="#variables.updateUser#",
				LastUpdated="#variables.updatetime#",
				sex="'#insSex#'",
				DateOfBirth="#insDOB#"
			);
		</cfscript> --->

<!---
	WAB 2005-10-04 removed and replaced with an insert trigger on the person table
		<cfquery name="checkPer" datasource="#application.siteDataSource#">
			select personID from person
				where firstName = '#variables.insFirstName#'
				and LastName = '#variables.insLastName#'
				and Email = '#variables.insEmail#'
		</cfquery>
		<cfset thisPersonID = checkPer.personID>
--->
		<!--- <cfset thisPersonID = insNewPerson.personid> --->

		<!--- NJH 2010-10-25 P-FNL079 - replaced below with above
		<cfset thisPerAction = "added"> --->
	 	<cfcatch type = "DATABASE">
		    <cfset commitIt = "No">
			<cfset message = cfcatch.message & " QueryName:insNewPerson">
            <cfset errorStruct.insNewPerson.argumentCollection = {type="Remote Add Task",Severity="Error",catch=cfcatch,warningStructure=personDetails}> <!--- Case 446091 / PROD2015-260 --->
		</cfcatch>
	</cftry>
<!---
	</cfif>
--->

	<cfif commitIt>
	    <cf_transaction action = "commit"/>
		<cfset caller.thisPersonid = thisPersonID>
		<cfset caller.thisLocID = thisLocID>
		<cfset caller.thisOrgID = thisOrgID>
	<cfelse>
	    <cf_transaction action = "rollback"/>

        <!--- Case 446091 / PROD2015-260 - now lets log the errors --->
        <cfif not structIsEmpty(errorStruct)>
            <cfset errorMessage = "Unexpected error: #message# Please refer to the following errorIDs: ">
            <cfloop collection=#errorStruct# item="error">
                <cfif structKeyexists(errorStruct[error], "argumentCollection")>
                    <cfset errorID = application.com.errorHandler.recordRelayError_Warning(argumentCollection=errorStruct[error].argumentCollection)>
                    <cfset errorMessage &= "#errorID# ">
                </cfif>
            </cfloop>
            <cfset errorStruct.errorMessage = "#errorMessage#">
        </cfif>

	</cfif>

</cf_transaction>

<!--- Case 446091 / PROD2015-260 --->
<cfif not structIsEmpty(errorStruct)>
    <!--- something went wrong - we'd rather throw an error now before continue to the owning/parent page as it will cause further hidden errors --->
    <cfthrow message="#errorStruct.errorMessage#">
</cfif>
<!--- Case 446091 / PROD2015-260 --->

<!--- ==============================================================================
     FLAG/PROFILE UPDATES START HERE
=============================================================================== --->
<cfif isDefined("frmEntityTypeID") and frmEntityTypeID eq "0"><!--- person --->
	<cfset entityID = thisPersonID>
<cfelseif isDefined("frmEntityTypeID") and frmEntityTypeID eq "1"><!--- location --->
	<cfset entityID = thisLocID>
<cfelseif isDefined("frmEntityTypeID") and frmEntityTypeID eq "2"><!--- org --->
	<cfset entityID = thisOrgID>
</cfif>
<cfif isDefined("frmFlagID") and isNumeric(frmFlagID)
	and isDefined("entityID") and isNumeric(entityID)
	and isDefined("frmEntityTypeID") and isNumeric(frmEntityTypeID)
	and isDefined("frmvalue") and isSimpleValue(frmvalue)>
	<!--- if all of the above match then a flag will be set for  --->
	<CF_SetFlag flagID="#frmFlagID#"
			entityId = "#entityID#"
			EntityType = "#frmEntityTypeID#"
			Value = "#frmvalue#"
			giveFeedBack = "true">
</cfif>

<!--- ==============================================================================

=============================================================================== --->
<!--- /screen/updateData.cfm --->
<!--- ==============================================================================
       SHOW DEBUG OR GO TO frmNEXT
=============================================================================== --->

<!--- set up the message  --->
<cfif commitIt>
	<CFSET message = "#insFirstName# #insLastName# phr_#thisPerAction#. #insSiteName# phr_#thisLocAction#.">
	<!--- NJH 2014/07/08 Task Core-295 - create a lead on partner registration --->
	<cfif not request.relayCurrentUser.isInternal and application.com.settings.getSetting("plo.createLeadOnPartnerRegistration") and not listFindNoCase("#application.organisationType.adminCompany.organisationTypeID#,#application.organisationType.endCustomer.organisationTypeID#",insOrganisationTypeID)>
        <cfif structKeyExists(form, "insLocationID") and isNumeric(form.insLocationID) and form.insLocationID GT 0>
            <!--- PROD2016-2643 - populate lead data from the existing location --->
            <cfset existingLocation = application.com.RelayPLO.getLocDetails(locationID=form.insLocationID)>
            <cfset variables.insSiteName = existingLocation.siteName>
            <cfset variables.insAddress1 = existingLocation.Address1>
            <cfset variables.insAddress2 = existingLocation.Address2>
            <cfset variables.insAddress3 = existingLocation.Address3>
            <cfset variables.insAddress4 = existingLocation.Address4>
            <cfset variables.insAddress5 = existingLocation.Address5>
            <cfset variables.insPostalCode = existingLocation.PostalCode>
        </cfif>
		<cfset leadDetails = {salutation=insSalutation,firstname=insFirstName,lastname=insLastName,email=insEmail,officePhone=insOfficePhone,mobilePhone=insMobilePhone,company=insSiteName,address1=insAddress1,address2=insAddress2,address3=insAddress3,address4=insAddress4,address5=insAddress5,postalCode=insPostalCode,countryID=insCountryID,convertedPersonId=thisPersonID,convertedLocationID=thisLocID,leadTypeID=application.lookupID.leadtypeID.partner,progressID=application.lookupID.leadProgressID.OpenNotContacted}>
		<cfset insertLeadResult = application.com.relayEntity.insertEntity(entityDetails=leadDetails,entityTypeID=application.entityTypeID.lead)>
	</cfif>
<cfelse>
	<cfset messageType = "error">
    <cfif structKeyExists(errorStruct, "errorMessage") AND errorStruct.errorMessage NEQ "">
        <cfset message = "Your insert for '#insFirstName#' '#insLastName#' at '#insSiteName#' failed. #errorMessage#">
    <cfelse>
        <CFSET message = "Your insert for '#insFirstName#' '#insLastName#' at '#insSiteName#' failed. #message#">
    </cfif>
</cfif>

<!--- turn off IntroText in relayTags/addCOntactForm --->
<cfset showIntroText="no">
<!--- if frmDebug is set to yes then show debug info and stop here --->

</CFSILENT>

<cfif isDefined("frmDebug") and frmDebug eq "yes">
	<cfoutput>#application.com.relayUI.message(message=message,type=messageType)#
		<br>OrgID: #htmleditformat(thisOrgID)# #htmleditformat(thisOrgAction)#
		<br>LocationID: #htmleditformat(thisLocID)# #htmleditformat(thisLocAction)#
		<br>PersonID: #htmleditformat(thisPersonID)# #htmleditformat(thisPerAction)#
	</cfoutput>
</cfif>

<!--- bit hacky but it gets us one step closer to factorising all inserts through one set of code - if internal call a screen --->
<cfif request.currentSite.isInternal>
	<cfparam name="request.currentSite.screens.quickcardadd" default = "orgProfileSummary">

	<!--- 2007-05-02 AJC Check for organisation Type specific profile summary screen & if none, checks the past screen too --->
	<cfscript>
		orgTypeScreen = application.com.screens.getScreenForOrgType(request.currentSite.screens.quickcardadd,thisOrgID,"");
	</cfscript>

	<cfif orgTypeScreen neq "">
		<!--- 2008/01 changed to deal with new entityNavigation <cflocation url="entityScreens.cfm?frmcurrentEntityID=#thisPersonID#&frmEntityType=0&message=#urlEncodedFormat(message)#&frmEntityScreen=#orgTypeScreen#" addtoken="No"> --->
		<cflocation url="/data/entityFrameScreens.cfm?frmCurrentEntityID=#thisPersonID#&frmEntityTypeID=0&message=#urlEncodedFormat(message)#&frmEntityScreen=#orgTypeScreen#&frmWABEntityAdded=true" addtoken="No">
	</cfif>
</cfif>

<!--- either relocate or return with frmNext set.
	This is expecting frmNext to be in the form redirectURLstring or
	returnURLString--->
<cfif left(frmNext,8) eq "redirect" and frmReturnMessage eq "yes">
	<!--- frmReturnMessage eq "yes" pass the message in the URL --->
	<cfset frmNext = mid(frmNext,9,len(frmNext)-8)>
	<cflocation url="#frmNext#?message=#message#" addtoken="No">
<cfelseif left(frmNext,8) eq "noRedirect" and frmReturnMessage eq "no">
	<!--- frmReturnMessage eq "no" don't pass the message in the URL --->
	<cfset frmNext = mid(frmNext,9,len(frmNext)-8)>
	<cflocation url="#frmNext#" addtoken="No">
<cfelseif left(frmNext,6) eq "return" and frmReturnMessage eq "no">
	<!--- pass the bit after the include back to the calling form --->
	<cfset message = "">
	<cfset frmNext = frmNext>
<cfelseif left(frmNext,6) eq "return" and frmReturnMessage eq "yes">
	<!--- pass the bit after the include back to the calling form --->
	<cfset frmNext = frmNext>
</cfif>