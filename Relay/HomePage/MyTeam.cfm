<!--- �Relayware. All Rights Reserved 2014

WAB 2016-01-29	Removed call to Sel All Function, convert to html-div
--->


<CFSET Current="MyTeam.cfm">
<!---Create a new group and delete a group scripts--->
<SCRIPT>
	function deleteTeam(){
		form = window.document.myForm

		list = form.frmTeamID.options[form.frmTeamID.selectedIndex].text
		Team = form.frmTeamID.options[form.frmTeamID.selectedIndex].value

			if(Team != ""){
				if(confirm("Please confirm that you want to delete the Team"+" "+list)){

					window.location.href = "MyTeam.cfm?deleteTeam="+escape(Team)+"&TeamName="+escape(list)
				}
			}

			else{
			alert("You must first select a team to delete")
			}
		}

	function TeamName(){

		var subwindow = window.open("/homePage/NewTeamName.cfm","NewTeam","height=200,width=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=0") ;
		//subwindow.location.href="NewTeamName.cfm"
		//subwindow.location.reload()
		subwindow.focus();
	}
</SCRIPT>

<CFINCLUDE TEMPLATE="MyTeamTopHead.cfm">


<!---Create New Team queries--->
<CFIF IsDefined("Name")>
	<CFQUERY NAME="getTeamname" datasource="#application.siteDataSource#">
		SELECT TeamName,CreatedBy,Created
		FROM MyTeam
		WHERE TeamName =  <cf_queryparam value="#Trim(Name)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		AND CreatedBy = '#request.relaycurrentuser.personid#'
	</CFQUERY>

	<cfif getTeamname.recordcount eq 0>
		<!---Create the new Team--->
		<CFQUERY NAME="InsertTeam" datasource="#application.siteDataSource#">
			INSERT INTO MyTeam(TeamName,CreatedBy,Created)
			VALUES(<cf_queryparam value="#Trim(Name)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
		</CFQUERY>
	<cfelse>
		<cfset teamNameError = 1>
	</cfif>
</CFIF>

<!---Delete a Team--->
<CFIF IsDefined("DeleteTeam")>
	<!---Delete the Users in that Team--->
	<CFQUERY NAME="DeleteMemberteam" datasource="#application.siteDataSource#">
		DELETE from MyTeamMember WHERE TeamMemberId =  <cf_queryparam value="#DeleteTeam#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<!---Delete the team itself--->
	<CFQUERY NAME="DeleteTeam" datasource="#application.siteDataSource#">
		DELETE from MyTeam WHERE TeamID =  <cf_queryparam value="#DeleteTeam#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>


<!---If a particular Group ID is known--->
<CFIF IsDefined("frmTeamID")>
	<!---Create lists of existing members--->
	<CFQUERY NAME="GetUsers" datasource="#application.siteDataSource#">
		Select PersonID, replace(c.ISOcode +' : '+ p.FirstName,',',' ') +' '+ replace(p.Lastname,',',' ') AS Name
		from Person as p
		INNER JOIN Location as l ON p.locationid = l.locationid
		INNER JOIN country as c on l.countryid = c.countryid
		<!--- 2012-06-27 PPB P-SMA001 		WHERE  c.countryid IN (#REQUEST.RELAYCURRENTUSER.COUNTRYLIST#) --->
		WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#		<!--- 2012-06-27 PPB P-SMA001 --->
		AND p.personID IN
						(Select DISTINCT PersonID from MyTeamMember WHERE TeamMemberID =  <cf_queryparam value="#frmTeamID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		ORDER BY Name
	</CFQUERY>

	<!---Get all available users (whoever has a UserGroupID created in the UserGroup table--->
	<CFQUERY NAME="GetAllUsers" datasource="#application.siteDataSource#">
		SELECT distinct p.PersonID, replace(c.ISOcode +' : '+ p.FirstName,',',' ') +' '+ replace(p.Lastname,',',' ') AS Name
		from Person as p
		inner join UserGroup as ug  on p.PersonID = ug.PersonID
		INNER JOIN Location as l ON p.locationid = l.locationid
		INNER JOIN country as c on l.countryid = c.countryid
		<!--- 2012-06-27 PPB P-SMA001		WHERE  c.countryid IN (#REQUEST.RELAYCURRENTUSER.COUNTRYLIST#) --->
		WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#		<!--- 2012-06-27 PPB P-SMA001 --->
		AND p.personid is not Null
			AND p.FirstName+' '+p.Lastname <> ''
			AND p.FirstName+' '+p.Lastname is not null
			AND p.loginExpires > getDate()
		ORDER BY Name
	</CFQUERY>
	<CFSET ListOfAllPersonID = ValueList(GetAllUsers.PersonID)>
	<CFSET ListOfAllNames = ValueList(GetAllUsers.Name)>
</CFIF>

<!--- Team Combo--->
<CFQUERY NAME="TeamName" datasource="#application.siteDataSource#">
	Select TeamId, TeamName
	FROM MyTeam
	where createdby = #request.relayCurrentUser.personID#
	ORDER BY TeamName
</CFQUERY>


<CFIF IsDefined("DeleteGroup")>
	<cfset application.com.relayUI.message("The team has been removed.")>
</CFIF>
<CFIF IsDefined("teamNameError") and IsDefined("Name")>
	<cfset application.com.relayUI.message("Sorry a team with the name '#htmleditformat(name)#' already exists. Please enter a new team name.","Error")>
</CFIF>



<FORM ACTION="MyTeam.cfm" METHOD="POST" name="myForm">
	<cf_relayformdisplay>
		<cf_relayFormElementDisplay label = "Team" type="html">
			<SELECT NAME="frmTeamID" onChange="window.document.myForm.submit()" class="form-control">
				<CFIF NOT IsDefined("frmTeamID")>
					<OPTION VALUE="">Please select a Team
				</CFIF>
				<CFOUTPUT QUERY="TeamName">
					<OPTION VALUE=#TeamId# <CFIF IsDefined("frmTeamID")><CFIF TeamId EQ #frmTeamID#>SELECTED</CFIF></CFIF> >#htmleditformat(TeamName)#
				</CFOUTPUT>
			</SELECT>
		</cf_relayFormElementDisplay>
	</cf_relayformdisplay>
</FORM>

<CFIF IsDefined("frmTeamID")>


	<FORM ACTION="MyTeamMemberManagement.cfm" METHOD="POST" NAME="form1" >
		<cf_relayformdisplay>
			<cf_relayFormElementDisplay label = "Team Members" type="html">
				<cf_select 	NAME="Users" query="#GetAllUsers#" selected="#valueList(getUsers.personid)#" multiple="true" value="personid" display="Name" keepOrig="true">
				<INPUT TYPE="Hidden" NAME="EditTeam" VALUE="1">
				<CF_INPUT TYPE="Hidden" NAME="frmTeamID" VALUE="#frmTeamID#">
			</cf_relayFormElementDisplay>
			<cf_relayFormElementDisplay  type="submit" name="submit">
		</cf_relayformdisplay>
	</FORM>

</CFIF>


