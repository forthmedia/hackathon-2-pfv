<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			keyStatsUpdate.cfm	
Author:				SWJ
Date started:		21 September 2002
	
Purpose:	

Simply to run the updateKeyStats procedure for application.siteDataSource

Usage:	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfstoredproc procedure="updateKeystats" datasource="#application.siteDataSource#">
</cfstoredproc>
