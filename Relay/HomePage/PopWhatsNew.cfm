<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Modification History

 --->
 
<CFPARAM NAME="RemoveNewsItem" DEFAULT="No">

<!--- If the user clicked the link below to Remove what's new items a record will be inserted into the
entity tracking table to record the fact --->
<CFIF RemoveNewsItem IS "Yes">
	<CFSET entitytype="element">
	<CFQUERY NAME="Article" DATASOURCE="#application.SiteDataSource#">
	insert into entityTrackingBase (entityID, entityTypeID, Personid, created)
	select #entityid#,
			(select entityTypeId from flagEntityType where tablename =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			,#request.relayCurrentUser.personid#,getDate()
	</CFQUERY>
</CFIF>

<!--- This query returns all what's new records that have element.date greater than today's 
date and where the user has not previously supressed them using the entityTracking table
hence the l;eft outer join to that table in the query --->
<CFQUERY NAME="GetElements" DATASOURCE="#application.SiteDataSource#">
	SELECT Element.Headline, 
		Element.ID AS RecordID,
		Element.date,
		Element.Summary,
		Element.LongSummary
	 FROM element LEFT OUTER JOIN
	(select entityid from entityTracking where entitytracking.entitytypeid=10
	and entitytracking.personid=#request.relayCurrentUser.personid#) as et
	on element.id=et.entityid where et.entityid is null and element.id IN (SELECT id
			FROM Element inner join elementType on element.elementTypeID = elementtype.elementTypeID
				WHERE Element.isLive = 1
			and elementtype.Type = 'Whats New'
	and element.date>getdate())
 
</CFQUERY>

<cf_head>
	<cf_title><CFOUTPUT>What's new on #request.CurrentSite.Title#</CFOUTPUT></cf_title>
</cf_head>

<CFPARAM NAME="LoadCode" DEFAULT="">
<CFIF GetElements.recordCount EQ 0 > 
	<CFSET LOADCODE = "window.close()">
</CFIF>

<CFOUTPUT>
	<cf_body onload="#htmleditformat(LoadCode)#">
</CFOUTPUT>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" BGCOLOR="White">
	<TR VALIGN="TOP">
	    <TH COLSPAN="2"><CFOUTPUT>What's new on #request.CurrentSite.Title#</CFOUTPUT></TH>
	</tr>

	<cfoutput query="GetElements">
		<TR VALIGN="TOP">
			<TD CLASS="submenu">#htmleditformat(headline)#</td>
		</tr>
		<TR>
			<TD ALIGN="LEFT"><FONT SIZE="-1">#htmleditformat(summary)#</FONT></TD>
		</TR>
		<CFIF LongSummary NEQ "">
		<TR>
			<TD ALIGN="LEFT"><FONT SIZE="-2">#htmleditformat(LongSummary)#</FONT></TD>
		</TR>
		</CFIF>
		
		<TR VALIGN="TOP">
			<td><A HREF="/elements/ElementDetail.cfm?RecordID=#URLEncodedFormat(RecordID)#">Click for more...</A>
			&nbsp;&nbsp;<A HREF="/homepage/PopWhatsNew.cfm?entityID=#URLEncodedFormat(RecordID)#&RemoveNewsItem=yes&">Hide this news item</A></td>
		</tr>
		<TR>
			<TD>&nbsp;</TD>
		</TR>
	</cfoutput>

	<TR>	
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<A HREF="JavaScript: window.close()" Class="Submenu">Close Window</A>
		</TD>
	</TR>

</table>






