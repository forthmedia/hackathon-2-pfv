<!--- �Relayware. All Rights Reserved 2014 --->
<!---PassNew.cfm--->
<!--- cribbed from FNL projects--->
<!--- CHANGES:
NYB: 2010-10-13	Deleted all the old (commented out) code that was taking up most of this file
--->

<CFPARAM NAME="Message" DEFAULT="">
<cfparam name="loginTableClass" default="withBorder">

<cfif structKeyExists(form,"action") and form.action eq "setPassword">
	<cfset changePassResult = application.com.login.changeUserPassword(username=request.relayCurrentUser.person.username,currentPassword=form.oldPassword,newPassword1=form.frmNewPassword1,newPassword2=form.frmNewPassword2)>
	
	<cfif not changePassResult.isOK>
		<cfset message = '<div class="errorblock">phr_login_#changePassResult.reason#</div>'>
	<cfelse>
		<cfset message = '<div class="successblock">phr_login_yourPasswordHasBeenSuccessfullyChanged</div>'>
	</cfif>
</cfif>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="#loginTableClass#">
	<TR>
		<TD ALIGN="LEFT" VALIGN="TOP" >phr_login_ChangeYourPassword</TD>
	</TR>
	
	<cfinclude template="/templates/changePWForm.cfm">
</TABLE>