<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		defaulthomepageFullScreen.cfm
Author:			RMB
Date created:	2012/01/16

Amendment History:

Date		Initials 	What was changed
2012/01/16	RMB			P-LEN024 - CR066 -
2012/02/22	PPB			Case 426568 set viewAsDashboardFrame=false so we see the Report Designer Taskbar (inc filters) in the Report iFrame
2012/04/11	IH			Case 427385 change _flowId from viewReportFlow to viewAdhocReportFlow
2016/07/11	RJT			PROD2016-1321 Made size of jasper reports on defaulthomepageFullScreen not tiny; fits screen on desktop, represents 2 screens on mobile.
--->
<h1 id="dashboardHeader">Dashboard</h1>
<div id="fullScreenContainer">
	<cfset startRow = true>

<cfset DashboardsToDisplay = application.com.relayhomepage.getReportDashboard(subkey='.HOMEPAGE.DashboardsToDisplay')>

<cfif DashboardsToDisplay eq "">
	  <cfset DashboardsToDisplay = 0>
</cfif>

<cfif DashboardsToDisplay gt 1>
	<cfset cols = 2>
	<cfset timeswidth = 1>
<cfelse>
	<cfset cols = 1>
	<cfset timeswidth = 2>
</cfif>

<cfset CountSSReports = 0>
<cfset JasperServerReports = []>
<cfloop index="i" from="1" to ="#DashboardsToDisplay#">
	<cfset temp = application.com.relayhomepage.getReportDashboard(subkey='.HOMEPAGE.FullScreenReport#i#')>
	<cfif temp is not "">
		<cfset CountSSReports ++>
		<cfset JasperServerReports[i] = replace(temp," ","_","ALL")>
	</cfif>
</cfloop>

<cfoutput>


<cfset startRow = true>
<cfset numReports = arrayLen(JasperServerReports)>

<cfloop from=1 to="#numReports#" index="index">

	<cfif startRow>
		<cfset startRow = false>
		<div class="row">
	</cfif>
		<cfset tdHeight = 100/((DashboardsToDisplay+1)\2)>
		<cfset tdWidth = 100/cols>
		<cfif index eq numReports and not startRow>
			<cfset tdWidth = 100>
		</cfif>
		<div class='col-xs-12 col-sm-12 <cfif numReports NEQ 1> col-md-6 </cfif>'>
			<div id="graph">
				<div style="height:100%;">
					<iframe class="jasperFrame" src="" name="SSHold#htmleditformat(index)#" id="SSHold#htmleditformat(index)#" scrolling="auto" frameborder="0" style="width:100%;height:100%;border:0px;"></iframe>
				</div>
			</div>
		</div>
	<cfif index mod cols eq 0>
		<cfset startRow = true>
		</div>
	</cfif>
</cfloop>
</cfoutput>
</div>
<!--- Because of the need to make it fit "the remaining space" and not being able to set the containing div as style="height:100%" as that would be global we javascript the correct size  RJT & SB --->
<cfif numReports GT 0>
	<script>
		<cfset noRows=Ceiling(numReports/2)>
		
		var extraMargin=50;
		<cfoutput>
			jQuery('.jasperFrame').outerHeight((jQuery(window).height() - jQuery('##dashboardHeader').outerHeight(true)-extraMargin)/#noRows#);
		</cfoutput>
	</script>
</cfif>