<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->







<cfquery name="getFAQTreeTop" datasource="#application.siteDataSOurce#">
	select ID,headline from element where elementTextID = 'InternalFAQTreeTop'
</cfquery>

<cfif getFAQTreeTop.recordcount gt 0>
	<cfset variables.treeTopElementID = getFAQTreeTop.id>
	<cfset getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID= variables.treeTopElementID)> 
	 
	<cfquery name="fulltree" dbtype="query" >
	select node, elementtypeid, headline, sortorder,
		'phr_detail_element_' + contentidAsstring as detail
	from getElements
	where node <> #variables.treeTopElementID#
	</cfquery>
	
	<cf_translateQueryColumn query = "#fulltree#" columnname = "detail" >
	<cf_translateQueryColumn query = "#fulltree#" columnname = "headline">			
	
	<cfform name="myform" width="500" height="640" format="Flash">
	
	   <cfformgroup type="hBox">
	  		 <cfformgroup type="panel" width="480" label="#getFAQTreeTop.headline#">	
	 	 		 <cfformgroup type="accordion" width="420" height="560" label="Containers">
				 	<cfloop query="fullTree" startrow="1" endrow="10">
						<cfoutput>
						 <cfformgroup type="page" label="#headline#">
							<cfformitem type="html" width="380">#detail#</cfformitem>
						</cfformgroup>
						</cfoutput>
					</cfloop>
				</cfformgroup>
			</cfformgroup>
		</cfformgroup>
	</cfform>
<cfelse>
	<h2>To define a site FAQ you must first create a content tree with the tree top element's text ID = 'InternalFAQTreeTop'</h2>
</cfif>


