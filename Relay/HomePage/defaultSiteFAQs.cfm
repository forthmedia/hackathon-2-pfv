<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:		FAQList
Author:			SWJ
Date created:	Long time ago

Description:	Draws a FAQList from elements of that type

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Jun-2000 		SWJ	Fixed the delete query to work with the new x-table structure

--->

<CFPARAM NAME="ElementType" DEFAULT="FAQ">
<CFPARAM NAME="Title" DEFAULT="Frequently Asked Questions">

<cfquery name="getFAQTreeTop" datasource="#application.siteDataSOurce#">
	select ID from element where elementTextID = 'InternalFAQTreeTop'
</cfquery>

<!--- <CFSTOREDPROC PROCEDURE="GetElementBranch" DATASOURCE="#application.siteDataSource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ElementID" VALUE="#getFAQTreeTop.ID#" NULL="No">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE="#request.relayCurrentUser.personid#" NULL="No">
		<CFPROCRESULT NAME="ElementList" RESULTSET="1">
	<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="ErrorText" DBVARNAME="@FNLErrorParam" NULL="Yes">
 	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@Depth" VALUE="0" NULL="No">
	<CFIF IsDefined('session.FNLElementStatus')>
	 	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Status" VALUE="#session.FNLElementStatus#" NULL="No">
	</CFIF>
</CFSTOREDPROC> --->

<cfquery name="getElements" datasource="#application.sitedatasource#" >
	-- #application.cachedContentVersionNumber# (this variable is used to flush the cache when the content is changed)
	exec GetElementBranchV2 @ElementID =  <cf_queryparam value="#getFAQTreeTop.ID#" CFSQLTYPE="CF_SQL_INTEGER" > , @personid=#request.relayCurrentUser.personid#, @statusid = 4, @Depth=99
</cfquery>


<CFQUERY NAME="GetTreeData" DBTYPE="query">
	SELECT node AS ItemID,  
		Parent AS ParentItemID,
		Name as description,
		RecordRights as thisRights,
		elementTypeID,
		status,
		statusid,
		haschildren
	FROM getElements
	Order by sortorder,ParentItemID, ItemID 
</CFQUERY>

<!--- need to get this item translated now so that relay tags can be run --->
<cf_translate phrases = "headline,Detail" entitytype="element" entityid="984"/>

<CF_Translate>

<cf_head>
<STYLE>
P {	margin-left : 15px;
	margin-right : 15px;}
</STYLE>

<script>
	function tree_toggleTarget ( target ) {
		if ( target.style.display == 'none' )	target.style.display = '' ;
		else target.style.display = 'none' ;
	}


</script>
	<cf_title>Phr_Sys_FAQs</cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Phr_Sys_FAQs</TD>
	</TR>
</TABLE>

<!--- <cfdump var="#getTreeData#"> --->
<CFOUTPUT Query="GetTreeData" group="ParentItemID">
	<cfif ParentItemID eq getFAQTreeTop.ID>
		<cfset parentID = 0>
	<cfelse>
		<cfset parentID = parentItemID>
	
		<div id="parent_#parentItemID#">
			<a href onClick="javascript:tree_toggleTarget(child_#parentItemID#,'open');return(false);"><img src="/images/MISC/iconHelpSmall.gif" alt="" border="0"> phr_headline_element_#htmleditformat(parentItemID)#</a>
		</div>	
		<div id="child_#parentItemID#" style="display: none;">
	</cfif>
	<CFOUTPUT>
		<cfif getTreeData.hasChildren eq 0>
		<div id="question_#ItemID#" style="padding-left: 20px;">
			<a href onClick="javascript:tree_toggleTarget(answer_#ItemID#,'open');return(false);"><img src="/images/MISC/iconHelpSmall.gif" alt="" border="0"> phr_headline_element_#htmleditformat(ItemID)#</a>
		</div>	
		<div id="answer_#ItemID#" style="display: none; padding-left: 20px;">
	    	phr_detail_element_#htmleditformat(ItemID)#
		</div>
		</cfif>
	</CFOUTPUT>
	<cfif ParentItemID neq getFAQTreeTop.ID>
		</div>
	</cfif>
</CFOUTPUT>





</CF_Translate>