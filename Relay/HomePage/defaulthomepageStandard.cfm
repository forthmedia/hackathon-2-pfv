<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		defaulthomepageStandard.cfm
Author:			RMB
Date created:	2012/01/16

Amendment History:

DD-MMM-YYYY	Initials 	What was changed
16/01/2012		RMB		P-LEN024 - CR066 - Orignal Home Screen was all on "defaultHomePageV2.cfm"
--->

<cfset homepageDashboard = application.com.settings.getSetting("reports.homepageDashboard")>

<cfset cols = 2>
<cfset startRow = true>

<div class="homepageWidgetTable">

	<!--- NJH 2011/03/01 P-FNL075 display the dashboard as determined by preferences if it has been set --->
	<cfif homepageDashboard neq "">
		<cfset homepageDashboard = replace(homepageDashboard," ","_","ALL")>

		<cfoutput>
			<cfset jasperServerUrl = "#application.com.jasperServer.getJasperServerPath()#/flow.html?_flowId=dashboardRuntimeFlow&dashboardResource=#homepageDashboard#&decorate=no">
			<cfinclude template="/jasperServer/jasperServerLogin.cfm">
			<cfabort>
			<!--- <tr>
				<td height="1200px;" width="1200px;">
					<iframe src="http://#application.com.jasperServer.getJasperServerPath()#/flow.html?_flowId=dashboardRuntimeFlow&dashboardResource=#homepageDashboard#&decorate=no<cfif not request.relayCurrentUser.jasperServerIsLoggedIn>&#application.com.jasperServer.getJasperServerLoginParamsForCurrentUser()#</cfif>" width="100%" height="100%" scrolling="no" frameborder="no">
				</td>
			</tr>--->
		</cfoutput>
	<cfelse>
		<cfset qryHomepageWidget = application.com.relayHomePage.GetWidgetQuery()>

		<!--- styles to manage the look of the homepage widgets --->
		<cf_include template="/code/styles/homepageWidgetStyles.cfm" checkIfExists="true" onNotExiststemplate="/styles/homepageWidgetStyles.cfm">

		<!--- styles to manage the look of the widget display handler --->
		<cf_include template="/code/styles/widgetStyles.cfm" checkIfExists="true" onNotExiststemplate="/styles/widgetStyles.cfm">

		<cfloop query="qryHomepageWidget">
			<cfif startRow>
				<cfset startRow = false>
				<div class="row">
			</cfif>
			<div class="col-xs-12 col-sm-12 col-md-6 homepageWidgetTD">
				<div id="graph">
					<cf_HomepageWidgetDisplay headerText="#headerText#" iconIMG="#iconImgPath#" moreLink="#moreLink#">
						<cf_showWidget widgetData="#widgetData#" widgetDisplay="#widgetDisplay#" displayRecords="#displayRows#" widgetParams="#widgetParams#">
					</cf_HomepageWidgetDisplay>
				</div>
			</div>
			<cfif currentRow mod cols eq 0>
				<cfset startRow = true>
				</div>
			</cfif>
		</cfloop>
	</cfif>
</div>