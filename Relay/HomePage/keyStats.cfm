<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:		keyStats.cfm
Author:			SWJ
Date created:	21 September 2002

Description:	Pulls data from the key stats table.  This table contains counts 
				of any aspect of the database that the implementation wishes to
				set up.  They are each date stamped and contain multiple rows per description

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
21 September 2002 		SWJ		Created

--->
<cfparam name="countryfilter" default="">
<cfparam name="statfilter" default="mostRecent">
<cfparam name="refreshKeyStats" default="no">
<cfparam name="pagesBack" type="numeric" default="0">
<cfparam name="sortOrder" default="statistic">
<cfparam name="startRow" default="1">
<cfset pagesBack = pagesBack+1>

<cfif isDefined("refreshKeyStats") and refreshKeyStats eq "yes">
	<cfscript>
		statsUpdated = application.com.relayKeyStats.updateStandardKeyStats();
	</cfscript>
	<cfquery name="dropKeyStatsView" datasource="#application.siteDataSource#">
		drop view vKeyStatsList
	</cfquery>

	<cfset refreshKeyStats = "No">
</cfif>

<cfquery name="checkKeyStatsViewExists" datasource="#application.siteDataSource#">
	select name from dbo.sysobjects 
	where name = 'vKeyStatsList' and xtype = 'v'
</cfquery>

<cfif checkKeyStatsViewExists.recordCount eq 0>
	<cfquery name="createKeyStatsView" datasource="#application.siteDataSource#">
		create view dbo.vKeyStatsList
	AS
	SELECT ks.description as statistic, ks.value, ks.path,ks.lastUpdated as last_updated,
		ks.countryid,ks.userGroup, 
		CASE WHEN c.countryDescription IS NULL THEN 'All countries'
		ELSE c.countryDescription
		END
		AS country
	FROM relaykeyStats ks left outer join country c on ks.countryID=c.countryID
	</cfquery>
</cfif>

<CFQUERY NAME="GetKeyStats" DATASOURCE="#application.siteDataSource#">
SELECT * from vKeyStatsList
where (countryID = 0 or countryID in (select countryID 
	from rights 
	where usergroupID = #request.relayCurrentUser.usergroupid#))
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	<cfif isDefined("countryfilter") and countryfilter is not "">
		and countryid =  <cf_queryparam value="#countryfilter#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
	<cfif isDefined("statfilter") and statfilter eq "mostRecent">
		and last_Updated = (SELECT max(last_Updated) FROM vKeyStatsList)
	</cfif>
	ORDER by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>



<!--- <cfquery name="getCountries" dbtype="query">
	select distinct country_Description, countryid from getKeyStats
</cfquery>
 --->
 

<cf_head>
	<cf_title>Key Stats</cf_title>
</cf_head>


<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu"></TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<cfif request.relaycurrentuser.usergroups contains "0"><a href="/homepage/keyStats.cfm?refreshKeyStats=yes" class="Submenu">Refresh Key stats</a>&nbsp;|&nbsp;</cfif>
			<a href="javascript:history.go(-#pagesBack#)" class="Submenu">Back</a>&nbsp;&nbsp;  
		</TD>
	</TR>

</TABLE>
</cfoutput>
<CF_tableFromQueryObject 
	queryObject="#GetKeyStats#"
	sortOrder = "#sortOrder#"
	openAsExcel = "no"
	numRowsPerPage="100"
	
	keyColumnList="description"
	keyColumnURLList="/#getKeyStats.path#"
	keyColumnKeyList="countryid"
	
	hideTheseColumns="countryid"
	showTheseColumns="Statistic,Country,Value,Last_Updated"

	allowColumnSorting="yes"
	numberFormat="value"
	dateFormat="last_Updated"

	FilterSelectFieldList="Country,Statistic"
	
<!--- 	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#" --->
	
	startRow="#startRow#"
	<!--- passThroughVariablesStructure = "" --->

	>
<!--- 
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
	<FORM action="#cgi.script_name#" NAME="FilterForm" METHOD="post">
		<TR>
			
				<td align="right">Show Country</td>
				<TD><SELECT NAME="countryfilter" ONCHANGE="javascript:document.FilterForm.submit()">
					<OPTION VALUE="0" <CFIF countryfilter EQ 0>SELECTED</CFIF>>All Countries
					<CFLOOP QUERY="getCountries">
						<OPTION VALUE="#countryid#" <CFIF countryfilter EQ countryid>SELECTED</CFIF>>#countrydescription#
					</CFLOOP>
				</SELECT></TD>
			
			<td width="180" align="right">Show most recent statistics&nbsp;<INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="statFilter" VALUE="MostRecent" <CFIF statFilter EQ "MostRecent">Checked</CFIF>></td>
			<td width="180" align="right">Show all statistics&nbsp;<INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="statFilter" VALUE="All" <CFIF statFilter EQ "All">Checked</CFIF>></td>
		</TR>
		<input type="hidden" name="refreshKeyStats" value="#refreshKeyStats#">
	</FORM>
</TABLE>
</CFOUTPUT>
<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="#FFFFFF" class="withBorder" >
	<tr>
		<th>Statistic</th>
		<th>Current value</th>
		<th>Last Updated</th>
		<th>Country</th>
	</tr>
	<cfoutput query="getKeyStats">
		<TR>
			<TD><cfif len(path) gt 0><a href="/#getKeyStats.path#" class="smallLink">#description#</a><cfelse>#description#</cfif></TD>
			<td align="right">#numberformat(value,"___,___,___,___")#</td>
			<td align="center">#dateFormat(lastUpdated,"dd-mm-yyyy")#</td>
			<td><cfif countryDescription eq "">All<cfelse>#countrydescription#</cfif></td>
		</TR>
	</CFOUTPUT>
</TABLE>
 --->



