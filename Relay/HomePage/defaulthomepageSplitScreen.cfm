<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		defaulthomepageSplitScreen.cfm
Author:			RMB
Date created:	2012/01/16

Amendment History:

Date		Initials 	What was changed
2012/01/16	RMB			P-LEN024 - CR066 -
2012/02/22	PPB			Case 426568 set viewAsDashboardFrame=false so we see the Report Designer Taskbar (inc filters) in the Report iFrame
2012/04/11	IH			Case 427385 change _flowId from viewReportFlow to viewAdhocReportFlow
2016/02/08  SB			Bootstrap
--->

<h1>Dashboard</h1>
<cf_includeJavascriptOnce template="/javascript/extExtension.js">
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 LeftHPWrap">
		<cfset qryHomepageWidget = application.com.relayHomePage.GetWidgetQuery(HomePageType = 'SplitScreen')>
		<cfset cols = 1>
		<cfset startRow = true>

		<div class="homepageWidgetTable">
			<cfloop query="qryHomepageWidget">
				<div id="graph" class="homepageWidgetTD">
					<cf_HomepageWidgetDisplay headerText="#headerText#" iconIMG="#iconImgPath#" moreLink="#moreLink#">
						<cf_showWidget widgetData="#widgetData#" widgetDisplay="#widgetDisplay#" displayRecords="#displayRows#" widgetParams="#widgetParams#">
					</cf_HomepageWidgetDisplay>
				</div>
			</cfloop>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 RightHPWrap">
		<cfset DashboardsToDisplay = application.com.relayhomepage.getReportDashboard(subkey='.HOMEPAGE.DashboardsToDisplay')>

		<cfif DashboardsToDisplay EQ ''>
			  <cfset DashboardsToDisplay = 0>
		</cfif>

		<cfset CountSSReports = 0>
		<cfset JasperServerReports = []>
		<cfloop index="i" from="1" to ="#DashboardsToDisplay#">
			<cfset temp = application.com.relayhomepage.getReportDashboard(subkey='.HOMEPAGE.SplitScreenReport#i#')>
			<cfif temp is not "">
				<cfset CountSSReports ++>
				<cfset JasperServerReports[i] = replace(temp," ","_","ALL")>
			</cfif>
		</cfloop>

		<cfoutput>


		<table cellspacing="0" cellpadding="0" border="4" align="center" height="100%" width="97%">

			<cfset startRow = true>
			<cfset numReports = arrayLen(JasperServerReports)>

			<cfloop from=1 to="#numReports#" index="index">

					<cfif startRow>
						<cfset startRow = false>
						<tr>
					</cfif>
					<td style="height:#100/DashboardsToDisplay#%;width:100%;" align="center">
					<cfset WorkSSReport = JasperServerReports[index]>

					<div style="height:100%;">
						<iframe src="" name="SSHold#htmleditformat(index)#" id="SSHold#htmleditformat(index)#" scrolling="auto" frameborder="0" style="width:100%;height:100%;border:0px;"></iframe>
					</div>

					</td>
					<cfif index mod cols eq 0>
						<cfset startRow = true>
						</tr>
					</cfif>
			</cfloop>
		</table>
		</cfoutput>
	</div>
</div>
