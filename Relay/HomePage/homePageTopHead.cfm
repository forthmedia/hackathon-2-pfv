<!--- �Relayware. All Rights Reserved 2014 --->

<cfif findNoCase("myActions.cfm",SCRIPT_NAME) neq 0>
	<cf_include template="/relay/actions/actionTophead.cfm" checkIfExists="true">
<cfelse>

	<CFPARAM NAME="current" TYPE="string" DEFAULT="homepage.cfm">
	
	<CF_RelayNavMenu pageTitle="" thisDir="/homepage">
		<CFIF current eq "myActions">
	<!--- 		<CF_RelayNavMenuItem MenuItemText="Add New Action" CFTemplate="../actions/ActionTask.cfm" target="mainSub"> --->
		</CFIF>
		<CFIF findNoCase("ActionEdit.cfm",SCRIPT_NAME) neq 0 and isDefined("currentTemplate") and currentTemplate eq "ActionEdit.cfm">
			<CF_RelayNavMenuItem MenuItemText="Save & Return" CFTemplate="javaScript:save(btnActionUpdate)" target="mainSub">
		</CFIF>
		<CFIF current eq "myAccounts">
			<cfset hasCorrectSecurity = application.com.login.checkInternalPermissions ("scorecardTask", "level1")>
			<!--- ==============================================================================
			SWJ  25-06-2007 Added in this block below to show scorecard variance report
							This will only show if the site is running scorecard
			=============================================================================== ---> 		
			<!--- <cfif hasCorrectSecurity and listFind(application. liverelayapps,48)> --->
			<cfif hasCorrectSecurity and application.com.relayMenu.isModuleActive(moduleID="Scorecard")>
				<CF_RelayNavMenuItem MenuItemText="Scorecard Variance" CFTemplate="/scorecard/scoreCardVarianceReport.cfm" target="mainSub"> 
			</cfif>
		</CFIF>
	
	</CF_RelayNavMenu>
</cfif>