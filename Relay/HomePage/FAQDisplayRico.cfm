<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->




<cf_head>
	<cf_title>FAQs</cf_title>
<cfoutput>
	<script src="/javascript/prototype.js" type="text/javascript"></script>
	<script src="/javascript/rico.js" type="text/javascript"></script>
</cfoutput>
<script>
function bodyOnLoad() {
	new Rico.Accordion( $('accordionDiv'), {panelHeight:300} );
	new Rico.Accordion( $('accordionDiv1'), {panelHeight:300} );
	}
</script>

<style>
	.accordionTabTitleBar {
	   	font-size           : 12px;
		padding             : 4px 6px 4px 6px;
	   	border-style        : solid none solid none;
		border-top-color    : #BDC7E7;
		border-bottom-color : #182052;
	   	border-width        : 1px 0px 1px 0px;
	}

	.accordionTabTitleBarHover {
	   	font-size        : 11px;
		background-color : #1f669b;
		color            : #000000;
	}

	.accordionTabContentBox {
	   	font-size        : 11px;
	   	border           : 1px solid #1f669b;
	   	border-top-width : 0px;
	   	padding          : 10px;
	}

	.accordionTabContentBox p {
	   	font-size        : 11px;
	}

	.accordionTabContentBox ol, ul, li {
		list-style-type: disc;
		font-size: 11px;
		line-height: 1.8em;
		margin-top: 0.2em;
		margin-bottom: 0.1em;
	{

</style>
</cf_head>

<cf_body onload="javascript:bodyOnLoad()">

<cfquery name="getFAQTreeTop" datasource="#application.siteDataSOurce#">
	select ID,headline from element where elementTextID = 'InternalFAQTreeTop'
</cfquery>

<cfif getFAQTreeTop.recordcount gt 0>
	<cfset variables.treeTopElementID = getFAQTreeTop.id>
	<cfset getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID= variables.treeTopElementID)>

	<cfquery name="fulltree" dbtype="query" >
	select node, elementtypeid, headline, sortorder,
		'phr_detail_element_' + contentidAsstring as detail
	from getElements
	where node <> #variables.treeTopElementID#
	</cfquery>


	<cf_translateQueryColumn query = "#fulltree#" columnname = "detail" >
	<cf_translateQueryColumn query = "#fulltree#" columnname = "headline">

	<table><tr>
	<td valign="top"><cfoutput>
		<div id="accordionDiv" style="width:380px; margin:5px; border-top-width:1px; border-top-style:solid;">
			<cfloop query="fullTree">
			   	<div id="overviewPanel">
				    <div id="overviewHeader" class="accordionTabTitleBar">#htmleditformat(headline)#</div>
				  	<div id="panel1Content" class="accordionTabContentBox">
			       		#detail#
			      	</div>
			   </div>
		   </cfloop>
		</div>
	</cfoutput>
	</td>
	<td valign="top"><cfoutput>
		<div id="accordionDiv1" style="width:380px; height:500px; margin:5px; border-top-width:1px; border-top-style:solid;">
			<cfloop query="fullTree">
			   	<div id="overviewPanel">
				    <div id="overviewHeader1" class="accordionTabTitleBar">#htmleditformat(headline)#</div>
				  	<div id="panel1Content1" class="accordionTabContentBox">
			       		#detail#
			      	</div>
			   </div>
		   </cfloop>
		</div>
	</cfoutput>
	</td>

	</tr>
	</table>
</cfif>


