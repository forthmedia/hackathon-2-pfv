<!--- �Relayware. All Rights Reserved 2014 --->


<!---

File:			UsersDiary.cfm
Author:			SWJ (based on EventDiary created by DJH)
Date created:	04 July 2000

Description:
Displays a list of selectable events for a particular month.

Version history:
1

--->

<cf_head>
<SCRIPT type="text/javascript">
	<!--

	function reSearch(searchString){
		var form = document.searchForm;
		form.frmSiteName.value = searchString;
		form.submit();
		}

	//-->
</SCRIPT>

	<cf_title>Users Diary</cf_title>
</cf_head>

<cfparam name="dateFrom" default="#createdate(year(now()),month(now()),01)#">
<CFPARAM NAME="LocationID" DEFAULT="1">

<CFQUERY NAME="getData" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
SELECT p.FirstName + ' ' + p.LastName AS FullName, o.OrganisationName,
	o.OrganisationID, cds.StatsReport, cd.LastUpdatedBy, MAX(cd.DateSent) AS MaxDateSent
FROM  dbo.Person p INNER JOIN
                      dbo.commdetail cd ON p.PersonID = cd.PersonID INNER JOIN
                      dbo.organisation o ON p.OrganisationID = o.OrganisationID INNER JOIN
                      dbo.CommDetailStatus cds ON cd.CommStatusID = cds.CommStatusID
GROUP BY p.FirstName + ' ' + p.LastName, o.OrganisationName, o.OrganisationID, cds.StatsReport, cd.LastUpdatedBy
HAVING      (cds.StatsReport = 'callback')
	<!--- SELECT DISTINCT p.FirstName + ' ' + p.LastName AS FullName, o.OrganisationName,
		 o.OrganisationID, cds.StatsReport, cd.LastUpdatedBy, cd.DateSent
	FROM dbo.Person p INNER JOIN
	     dbo.commdetail cd ON p.PersonID = cd.PersonID INNER JOIN
	     dbo.organisation o ON p.OrganisationID = o.OrganisationID INNER JOIN
	     dbo.CommDetailStatus cds ON cd.CommStatusID = cds.CommStatusID
	WHERE cds.StatsReport = 'callback'  and cd.LastUpdatedBy = #request.relayCurrentUser.usergroupid# --->
</CFQUERY>


<cfset current = "usersDiary.cfm">
<CFIF getData.RecordCount GT 0>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<tr>
	<!--- find out the first date and last date an event is booked in the date column
	--->
	<cfset firstDate=ArrayMin(ListToArray(ValueList(getData.MaxDateSent)))>
	<cfset firstDate= createDate(year(firstDate), month(firstDate), 1)>
	<cfset lastStartDate=ArrayMax(ListToArray(ValueList(getData.MaxDateSent)))>
	<cfset lastDate= createDate(year(lastStartDate), month(lastStartDate), DaysInMonth(lastStartDate))>
	<cfparam name="dateTo" default="#lastDate#">
	<SCRIPT type="text/javascript">
		// make sure the first date is greater than or equal to the last date
		function formCheck(aform)
		{
			if(aform.dateFrom.selectedIndex > aform.dateTo.selectedIndex)
			{
				alert("the 'From' date must be less than or equal to the 'To' date!")
				return false
			}
			return true
		}
	</script>

	<!--- now create a select, starting from the first month and work forward until the last month is arrived at --->
	<form name="DateChooser" action="usersDiary.cfm" method="post" onSubmit="return formCheck(this)">
<!--- 	<input type="hidden" name="lastupdatedby" value="<cfoutput>#lastupdatedby#</cfoutput>"> --->
<!--- 	<input type="hidden" name="frmChooserLeft" value="<cfoutput>#frmChooserLeft#</cfoutput>">
	<input type="hidden" name="frmChooserRight" value="<cfoutput>#frmChooserRight#</cfoutput>">
 --->
	<td colspan="6" class="OffsetBackground">
		Choose a month to view records from: &nbsp;
		<cfset aMonth = firstDate>
		<select name="dateFrom" class="smaller">
			<cfloop condition="datecompare(aMonth, lastDate) lt 1">
				<cfoutput><option value="#aMonth#" <cfif aMonth is dateFrom>selected</cfif>>#dateFormat(aMonth,"mmm-yyyy")#</cfoutput>
				<cfset aMonth=dateAdd("m",1,aMonth)>
			</cfloop>
		</select>
		to:
		<cfset aMonth=createdate(year(firstDate), month(firstDate), daysInMonth(firstDate))>
		<select name="dateTo" class="smaller">
			<cfloop condition="datecompare(aMonth, lastDate) lt 1">
				<cfoutput><option value="#aMonth#" <cfif aMonth is dateTo>selected</cfif>>#dateFormat(aMonth,"mmm-yyyy")#</cfoutput>
				<cfset aMonth=dateAdd("m",1,aMonth)>
				<cfset aMonth=createDate(year(aMonth), Month(aMonth), daysInMonth(aMonth))>
			</cfloop>
		</select>
		&nbsp;
		<input type="submit" value="Refresh" class="smaller">
	</td>
	</form>
</tr>

</TABLE>
<!--- now loop through every day in the month, to create a line per
day populating each day with all the events for that day --->
<cfset thisDate = dateFrom>
<CFSET repeatDate = 'Yes'>
	<cfset row = 0>
	<cfset monthwas = 0>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<cfloop condition="datecompare(thisDate, dateFrom) gte 0 and datecompare(thisDate, dateTo) lte 0">
		<cfif month(thisDate) is not monthwas>
			<cfset monthwas = month(thisDate)>
			<tr>
				<td colspan="8" class="label"><cfoutput><b>#htmleditformat(monthAsString(month(thisdate)))#</b></cfoutput></td>
			</tr>
			<TR><TH>&nbsp;</TH>
				<TH>&nbsp;</TH>
				<TH>Company</TH>
			    <TH>Who to call</TH>
			</TR>
		</cfif>
		<cfset row = row + 1>
		<cfoutput>
		<cfif dayofweek(thisDate) is 1 or dayofweek(thisDate) is 7>
			<cfset bgcol="ffccbb">
		<cfelse>
			<cfset bgcol="FEFFD7">
		</cfif>
		<tr>
			<CFIF isDefined('lastRowsDate')><!--- If we've been around the loop once then we'll know lastRowsDate --->
				<cfif datecompare(lastRowsDate, thisDate,'d') IS 0> <!--- compare lastRowsDate to this row and if they match set repeatDate to no --->
					<CFSET repeatDate = 'No'>
				</CFIF>
			</CFIF>
			<CFSET lastRowsDate = thisDate>	<!--- set lastRowDate for the next time around the loop --->

			<!--- Show the date we're currently on, but don't repeat it if there is more than one record for that date --->
			<td bgcolor="#bgcol#" nowrap><CFIF RepeatDate IS 'Yes'>#dateformat(thisDate,"dddd dd mmm yyyy")#<CFELSE>&nbsp;</CFIF>
		</td>
		</cfoutput>
		<cfset eventCtr=0>
		<cfloop query="getData">
		<cfoutput>
			<cfif datecompare(thisDate, getData.MaxDateSent,'d') IS 0><!--- If we've got date for this row show it --->
				<cfset eventCtr = eventCtr + 1>
				<cfif eventCtr gt 1>
					</tr>
					<tr>
					<td bgcolor="#bgcol#" nowrap>#dateformat(thisDate,"dddd dd mmm yyyy")#</td>
				</cfif>
					<TD></TD>
					<TD><A href="javaScript:reSearch('#left(OrganisationName,15)#')">#htmleditformat(OrganisationName)#</A></TD>
					<TD><CFIF FullName IS ''>No DM spoken to<CFELSE>#htmleditformat(FullName)#</CFIF></TD>
			<cfelseif getData.MaxDateSent gt thisDate and eventCtr lt 1>
				<td colspan="8" bgcolor="#bgcol#">&nbsp;</td>
				<CFSET repeatDate = 'Yes'>
				<cfbreak>
			<cfelseif thisDate gt lastStartDate>
				<td colspan="8" bgcolor="#bgcol#">&nbsp;</td>
				<CFSET repeatDate = 'Yes'>
				<cfbreak>
			</cfif>
		</cfoutput>
		</cfloop>
		<cfset thisDate = dateAdd("d",1,thisDate)>
		<cfif eventCtr lte 1></tr></cfif>
	</cfloop>
</table>
<CFELSE>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr>
		<td>
		No call backs set.
		</td>
	</tr>
	</table>
</CFIF>

<FORM ACTION="../data/dataFrame.cfm" METHOD="post" NAME="searchForm" TARGET="main">
	<INPUT type="hidden" NAME="frmSiteName" value="none">
</FORM>


