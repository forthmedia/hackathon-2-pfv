<!--- �Relayware. All Rights Reserved 2014 --->
<cfset application.cachedSecurityVersionNumber = application.cachedSecurityVersionNumber+1>		

<CFIF IsDefined("UserManagement") OR IsDefined("EditTeam")>
	<!---Form submitted--->
	<CFIF IsDefined("EditTeam")>
		<CFTRANSACTION>
			<cfif not isdefined("Users")>
				<CFQUERY NAME="DeleteUser" datasource="#application.siteDataSource#">
					DELETE FROM MyTeamMember 
						WHERE TeamMemberID =  <cf_queryparam value="#frmTeamID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			<cfelse>
			<!---Check if some users need to be deleted--->
				<CFLOOP INDEX="x" FROM="1" TO="#ListLen(PreviousUsers)#">
					<CFIF ListFind(Users, ListGetAt(PreviousUsers, x)) EQ 0>
						<!---If yes, delete them--->
						<CFQUERY NAME="DeleteUser" datasource="#application.siteDataSource#">
							DELETE FROM MyTeamMember 
								WHERE TeamMemberID =  <cf_queryparam value="#frmTeamID#" CFSQLTYPE="CF_SQL_INTEGER" > 
									AND PersonID =  <cf_queryparam value="#ListGetAt(PreviousUsers, x)#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</CFQUERY>
					</CFIF>
				</CFLOOP>			
			</cfif>	
				<CFIF IsDefined("Users")>	
				<CFLOOP INDEX="x" FROM="1" TO="#ListLen(Users)#">				
					<CFIF ListFind(PreviousUsers, ListGetAt(Users, x)) EQ 0>							
						<!---Check that the user is not already a member of the Group (reload)--->
						<CFQUERY NAME="CheckUser" datasource="#application.siteDataSource#">
								SELECT * from MyTeamMember
								WHERE TeamMemberID =  <cf_queryparam value="#frmTeamID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								AND PersonID =  <cf_queryparam value="#ListGetAt(Users, x)#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</CFQUERY>						
						
						<!---If not add him--->
						<CFIF CheckUser.RecordCount EQ 0>					
							<CFQUERY NAME="AddUser" datasource="#application.siteDataSource#">
									INSERT INTO MyTeamMember(PersonID,TeamMemberID,CreatedBy)
									VALUES(<cf_queryparam value="#ListGetAt(Users, x)#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#frmTeamID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >)
							</CFQUERY>						
						</CFIF> 					
					</CFIF>					
				</CFLOOP>		
			</CFIF>		
		</CFTRANSACTION>	
	</CFIF>
	<CFINCLUDE TEMPLATE="MyTeam.cfm">	

</CFIF>