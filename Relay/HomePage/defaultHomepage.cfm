<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2011-03-01	NJH		P-FNL075 added functionality to display report designer dashboards

2012/01/05 - RMB - P-LEN024 - CR066 - This is a strange fix for IE where the overflow-x on the scroll inside the widgets was hiding content, for some reason this fixes the issue
2012/01/16 - RMB - P-LEN024 - CR066 - Orignal Home Screen was all on "defaultHomePageV2.cfm" now in defaulthomepageStandard.cfm
2013-02-13	WAB		Sprint 15&42 Comms. Change reference to displayEmailContent to viewCommunication
2014-10-23	NJH	Change default homepage to advanced.
2016-03-01	RJT	PROD2016-581 Include hook to make version announcement

--->

<cf_includeJavascriptOnce template="/javascript/extExtension.js">
<cf_includejavascriptonce template = "/javascript/openwin.js">

<cfset dashboardtype = application.com.relayhomepage.getReportDashboard(subkey='.HOMEPAGE.DASHBOARDTYPE')>
<cfif dashboardtype eq "">
	<cfset dashboardtype = "advanced">
</cfif>

<style>
	html, body {height:100%;}
</style>

<!---  2012/01/05 - RMB - P-LEN024 - CR066 - This is a strange fix --->		
<script language="javascript"></script>


<cfsetting enablecfoutputOnly = "false">

<cf_head>
	<SCRIPT type="text/javascript">
	<!--
		function viewCommunication (commid) {
			openWin('/communicate/viewCommunication.cfm?commID='+commid,'ContentPreview','height=600,width=650,toolbar=0,location=0,directories=0,status=0,menuBar=0,,scrollBars=1,resizable=1')
		}
	//-->
	</SCRIPT>
</cf_head>



<cfscript>
	GetLastWhatsNewDate = application.com.relayHomePage.GetLastWhatsNewDate();
</cfscript>

<cfif GetLastWhatsNewDate.recordCount GT 0> 
	<cfset onLoadScript = onLoadScript & " newWindow = openWin('/homepage/PopWhatsNew.cfm', 'Whats_New', 'width=450,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1">
</cfif>

<cf_title>Homepage</cf_title>

<cf_include template="/homepage/defaultHomepage#dashboardtype#.cfm" checkIfExists="true">
	
<cfif listFindNoCase("fullscreen,splitScreen",dashboardType)>
	<script>
		<cfwddx action="cfml2js" topLevelVariable = "reports" input="#JasperServerReports#">
		
		function loadReports()  {
			<cfoutput>
			var ReportPart1 = "#application.com.jasperServer.getJasperServerReportUnitPath()#";	/* 2012/02/22 PPB Case 426568 set viewAsDashboardFrame=false */
			</cfoutput>
			for (var i=0, len=reports.length; i<len; ++i){
				var ReportPart2 = reports[i];
				var ReportPart3 = ReportPart1 + ReportPart2;
				j = i + 1;
				UTargetWin = "SSHold" + j
				GTargetWin = document.getElementById(UTargetWin);
				GTargetWin.src = ReportPart3;
			}
		}
		
		function isJasperServerReady () {
	
			var milliseconds = 1000;
			
			if(relayUI.jasperServerLoggedIn==true){
				// NJH 2012-05-14 CASE 434001, believe no longer needed : setTimeout(loadReports, milliseconds);
				loadReports();
			} else {
			  	setTimeout(isJasperServerReady, 100);
			}
		}

		setTimeout(isJasperServerReady, 100)	
	</script>
</cfif>





<cf_include template="/homepage/versionAnnoucement.cfm" checkIfExists="true">