<!--- �Relayware. All Rights Reserved 2014 --->


<cf_head>
<STYLE>
P {	margin-left : 15px;
	margin-right : 15px;}
</STYLE>

	<cf_title>Relay Overview</cf_title>
</cf_head>




<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="#FFFFFF" class="withBorder" >
	<tr>
		<td<h2>Getting Started</h2></td>
	</tr>
  <tr>
    <td class="oddRow"><p>Welcome to Relay. This system is designed to make communicating between 
        existing partners and potential partners, simple, fast, and effective. 
      </p></td>
  </tr>
  <tr>
    <td><p>Relay will enable individual territories to keep up to date with pricing, 
        products, training, and marketing information, keeping them competitive 
        and at the leading edge of the industry in their country. </p></td>
    <td></td>
  </tr>
  <tr>
    <td><p>Individual territories are in control of all communications with their 
        partners.</p></td>
  </tr>
  <tr>
    <td><p>A Relay Support Helpdesk is set up ready to take calls or emails, should 
        the user have a technical problem or database query. All calls will be 
        dealt with immediately, logged and graded due to the severity of the problem.</p>
      <p>The helpdesk can be contacted at +44 (0)870 601 1090 or <a href="mailto:relayhelp@foundation-network.com">relayhelp@foundation-network.com</a></p></td>
  </tr>
  <tr>
    <td><h2>Things to explore</h2>
      <p><strong>Accessing the database<br>
        </strong>This is achieved by two methods:<br>
        - type the company you want to search for in the box on the left nav bar 
        and click search<br>
        - click Advanced Search in the left Nav bar for more search options</p>
      <p><strong>Adding a record</strong><br>
        Review this short flash movie to get an overview of adding a record <a href="http://relay.foundation-network.com/RelayAdmin/HelpFiles/FlashDemos/HelpAddingANewContact.swf" title="View Movie" target="_blank">View 
        movie</a></p>
      <p><strong>Sending an email shot</strong><br>
        Bulk email tools and reports are found under the eMarketing link in the 
        left hand nav bar. To send an example on eclick on the Help link at the 
        top right of the screen. When the window opens type &quot;sending&quot; 
        and click search. This will bring back multiple help items relating to 
        sending emails. Click on the link titled &quot;Sending an email on its 
        own&quot;.</p>
      <p>Once you've sent your test email click on the stats to see what has happened 
        to the email.</p>

	<tr><td><p>Welcome to Relay. This system is designed to make communicating between existing partners and potential partners, simple, fast, and effective. </p></td></tr>
	<tr><td><p>Relay will enable individual territories to keep up to date with pricing, products, training, and marketing information, keeping them competitive and at the leading edge of the industry in their country. </p></td><td></td></tr>
	<tr><td><p>Individual territories are in control of all communications with their partners.</p> </td></tr>
	<tr><td><p>A Relay Support Helpdesk is set up ready to take calls or emails, should the user have a technical problem or database query. All calls will be dealt with immediately, logged and graded due to the severity of the problem.</p> </td></tr>
	<tr><td></td></tr>
</table>





