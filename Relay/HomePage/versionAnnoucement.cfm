<cf_includejavascriptonce template = "/javascript/lib/fancybox/jquery.fancybox.pack.js">
<cf_includeCssOnce template="/javascript/lib/fancybox/jquery.fancybox.css" />

<cfscript>
	versionAnnouncementData=application.com.versionAnnouncement.checkLoggedInUserForVersionAnnouncement();
</cfscript>

<cfif versionAnnouncementData.needsInclude>

	<cfscript>
		token=new com.tokens.token(tokenTypeTextID="launchpadURLToken");
		
		launchPadDestinationPage=token.set("url",versionAnnouncementData.include);
		token.persistToDatabase();
		announcementURL="/templates/help.cfm?launchpadURLToken=#token.getTokenValue()#";
	</cfscript>
	
	
	<cfoutput>
		<cf_head>
		<script type="text/javascript">
				jQuery(document).ready(function() {
					
					jQuery(".gallerypdf").fancybox({
						openEffect: 'none',
						closeEffect: 'none',
						autoSize: true,
						type: 'iframe',
						iframe: {
							preload: false // works around known issue in fancybox where in  IE and edge preloading pdfs "get stuck"
						}
					});
					
					jQuery(".gallerypdf").click();
				});
			</script>
		</cf_head>
	
	
	<a class="btn btn-info btn-sm btn-hover gallerypdf" data-fancybox-type="iframe" href=#announcementURL#>
	
	</cfoutput>

</cfif>