<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		defaulthomepageAdvanced.cfm
Author:			RPW
Date created:	2014/08/21

Amendment History:

DD-MMM-YYYY	Initials 	What was changed

--->
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
		<div id="homepageLeft">
			<div id="quickLinksContainer">
				<h1 class="Label">Phr_flag_quickLinksElementFlag</h1>

				<cfscript>
					url.module="MyFavorites";
					variables.phraseSuffix = "_withCount";
					variables.itemCountStruct = {};

					variables.itemCountStruct["My_Favourite_Reports" & variables.phraseSuffix]["numberFavouriteReports"] = application.com.relayHomePage.GetMyFavouriteReports().numberOfFavouriteReports;
					variables.itemCountStruct["Opportunities" & variables.phraseSuffix]["numberOpps"] = application.com.relayHomePage.GetMyOpportunities().numberOfOpps;
					variables.itemCountStruct["Accounts" & variables.phraseSuffix]["numberAccounts"] = application.com.relayHomePage.GetMyAccounts().numberOfAccounts;
					variables.itemCountStruct["Leads" & variables.phraseSuffix]["numberLeads"] = application.com.relayHomePage.GetMyLeads().numberOfLeads;
					variables.itemCountStruct["Fund_Manager" & variables.phraseSuffix]["numberFundManagers"] = application.com.relayHomePage.GetMyFundManager().numberOfFundManagers;
					variables.itemCountStruct["My_Selections" & variables.phraseSuffix]["numberSelections"] = application.com.relayHomePage.GetMySelections().numberOfSelections;
					variables.itemCountStruct["Reports" & variables.phraseSuffix]["numberReports"] = application.com.relayHomePage.GetMyReports().numberOfReports;
					variables.itemCountStruct["Actions" & variables.phraseSuffix]["numberActions"] =application.com.relayHomePage.GetMyActions().numberOfOdActions;
				</cfscript>

				<cfinclude template="/templates/iconnav.cfm">
				<cfsetting enablecfoutputonly="no">
			</div>
			<div id="graph">
				<cf_HomepageWidgetDisplay headerText="phr_Sys_Latest_emailshots" iconIMG="/images/misc/icon_EmailShots.jpg" moreLink="javascript:void(openNewTab('phr_Sys_Latest_emailshots','phr_Sys_Latest_emailshots','/communicate/commCheck.cfm',{reuseTab:true,iconClass:'communicate'}));">
					<cf_showWidget widgetData="EmailShots" widgetDisplay="List" displayRecords="6000" widgetParams="displayRows=5">
				</cf_HomepageWidgetDisplay>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
		<div id="homePageRight">

			<cf_param label="Filter Type" name="filterTypes" default="All,Following,Flagged,Actions,Messages"  validValues="All,Following,Flagged,Actions,Messages" displayAs="twoSelects" allowSort="true"/>

			<div id="homePageRightBoxes">
				<cfinclude template="/social/activityStream.cfm">
			</div>
			<br>

			<cfinclude template="/templates/recentSearches.cfm">
		</div>
	</div>
</div>
