<!--- �Relayware. All Rights Reserved 2014 --->
<!---PassChange.cfm--->
<!---
<CFIF NOT IsDefined("Cookie.VALID")>
	<CFLOCATION URL="invalidUser.cfm">
	<CF_ABORT>
</CFIF>
--->
<!--- check that password is valid --->
<!--- AJC P-SNY041 Relay Global Encrypted Passwords --->
<!--- <cfif isdefined("application.encryptionMethod.relayPassword") and application.encryptionMethod.relayPassword is not "">
	<cfset oldPassword1 = application.com.encryption.encryptString(encryptType="relaypassword",partnerUsername=request.relayCurrentUser.person.username,partnerPassword=FORM.oldPassword)>
	<cfset newPassword3 = application.com.encryption.encryptString(encryptType="relaypassword",partnerUsername=request.relayCurrentUser.person.username,partnerPassword=FORM.frmNewPassword1)>
<cfelse>
	<cfif isdefined('application. passwordEncryptionMethod') and application. passwordEncryptionMethod is not "">
		<cfset oldPassword1 = hash(Trim(FORM.oldPassword),"#application. passwordEncryptionMethod#")>
		<cfset newPassword3 = hash(Trim(FORM.frmNewPassword1),"#application. passwordEncryptionMethod#")>
	<cfelse>
		<cfset oldPassword1 = FORM.oldPassword>
		<cfset newPassword3 = FORM.frmNewPassword1>
	</cfif>	
</cfif>
<CFQUERY NAME="getResults" datasource="#application.sitedatasource#">
    SELECT  person.PersonID,
			person.PasswordDate
      FROM person 
     WHERE person.PersonID = #request.relayCurrentUser.personID#
	AND person.password = '#oldPassword1#'
</CFQUERY>

<!--- if password valid, then check that it hasn't expired --->
				
<CFIF Getresults.RecordCount IS 0>

	<CFSET #Message# = "You did not enter your current password correctly.  Please try again.">
	<CFINCLUDE TEMPLATE="PassNew.cfm">
	<CF_ABORT>
	
<CFELSE>
	<!--- password was correct, now check to see if it has expired --->
	
		
	<CFQUERY NAME="changePass"  datasource="#application.sitedatasource#">
		UPDATE person
		   SET 
			   Password = '#Trim(newPassword3)#',
		       Passworddate = #Now()#,
			   FirstTimeUser = 0,
			   lastupdatedby = #request.relaycurrentuser.usergroupid#,
			   lastupdated = #Now()#
			WHERE PersonID = #request.relaycurrentuser.personID#
		  AND Password = '#oldPassword1#'
	</CFQUERY>
	
	<CFSET #JSMessage# = "Your password has been successfully changed.">		
	</CFIF> --->
	
<cfset changePassResult = application.com.login.changeUserPassword(username=request.relayCurrentUser.person.username,currentPassword=form.oldPassword,newPassword1=form.frmNewPassword1,newPassword2=form.frmNewPassword2)>

<cfif not changePassResult.isOK>
	<cfset message = changePassResult.reason>
	<cfinclude template="PassNew.cfm">
	<CF_ABORT>
<cfelse>
	<cfset jsMessage = "Your password has been successfully changed.">
</cfif>

	<!--- could set next page here if getResults.FirstTimeUser IS true --->
	<CFSET next_page = "homepage.cfm">


<!--- 
NJH 2008/07/03 Bug Fix Lenovo Support Issue 502
<CF_RelayNavMenu pageTitle="User Management" thisDir="utilities">
	<CF_RelayNavMenuUpper MenuItemText="Password" CFTemplate="/homepage/PassNew.cfm" target="_self">
</CF_RelayNavMenu> --->
<!--- this next table is to create a space between the header and table  --->
<table bgcolor="#FFFFFF"><tr><td></td></tr></table>
			
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Password changed</TD>
	</TR>

	<tr>
		<td class="messageText">
			Password successfully changed
		</td>
	</tr>

</TABLE>





		




