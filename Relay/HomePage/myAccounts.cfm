<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		myAccounts.cfm
Author:			SWJ
Date created:	7 Aug 2000

Description:	This code is a wrapper which calls the orglist.cfm screen
				and passes one variable frmAccount

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-04-04		WAB				Changed so that doesn't CFABORT if no permissions.  This allows it to be included in the homepage without problems
09-Jan-2004			KAP			Correction to paging and alphabetical filter
2008    someone added clientSpecificMyAccounts.cfm but didn't comment it
NYB 2009-05-06 Sophos - added concept of use2009Format, being a different layout
SSS 2009-09-03 LHID 2606 made the orgnisation load in the 3 pane view.

NJH 2009/09/23 LHID 2671 - Re-instated some of the Sophos changes as it looks like they were accidentally removed. Basically, setting the
		KeyColumn type parameters and passing the parameters to TFQO rather than having it hardcoded in the call to TFQO.
NAS 2010/08/23 LID 3732 - My Accounts not able to filter by first letter of org name
WAb 2010/10/21 chnage use2009Format to settings.versions.myaccountslayout

2011/11/24 PPB LID8074 switched from AccManager flag to AllocationSalesTechnical flag group for "2009" style report
2012-10-04	WAB		Case 430963 Remove Excel Header

--->

<cfif  fileexists("#application.paths.code#\cftemplates\clientSpecificMyAccounts.cfm")>
	<cfinclude template="/code/cftemplates/clientSpecificMyAccounts.cfm">
<cfelse>
	<cf_includejavascriptonce template="/javascript/openwin.js">
	<cf_includejavascriptonce template="/javascript/extExtension.js">
	<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
	<cfparam name="openAsExcel" type="boolean" default="false">

	<!--- START:  NYB 2009-05-06 Sophos - added: --->
	<!--- <cfparam name="use2009Format" default="false"> --->
	<cfset pageLayout = application.com.settings.getSetting("versions.myAccountsLayout")>
	<!--- END:  2009-05-06 Sophos --->

	<cfparam name="numRowsPerPage" default="200">
	<cfparam name="startRow" default="1">

	<!--- START:  NYB 2009-05-06 Sophos - added cfif use2009Format - else entry all orig --->
	<cfif pageLayout is "2009">
		<cfparam name="sortOrder" default="Partner">
		<cfparam name="myAccountFilterSelectFieldList" type="string" default="Approval_Status,Country,New_Partner,State">
		<cfparam name="alphabeticalIndexColumn" type="string" default="">
		<cfparam name="accManagerFlagTextID" type="string" default="AllocationSalesTechnical">		<!--- 2011/11/24 PPB LID8074 switched from AccManager flag to AllocationSalesTechnical flag group  --->

		<cfset hasApprovalFlagRights = application.com.flag.doesCurrentUserHaveRightsToFlagGroup(FlagGroupId="orgApprovalStatus",Type="edit")>
		<cfif hasApprovalFlagRights and application.com.login.checkInternalPermissions(securityTask='approvalTask').edit>
			<script language="JavaScript1.2" type="text/javascript">
				function openRegProcess(OrgID) {
					openWin('/approvals/approvalProcess.cfm?frmOrganisationID='+OrgID+'&frmProcessid=RegApproval&frmstepid=0','popupName','height=600,width=600,scrollbars=1');
				}
			</script>
			<cfparam name="keyColumnList" type="string" default="Partner,Approval_Status"><!--- this can contain a list of columns that can be edited --->
			<cfparam name="keyColumnURLList" type="string" default=" ,javascript:void(openRegProcess(thisurlkey));"><!--- this can contain a list of matching columns that contain the URL of the editor template --->
			<cfparam name="keyColumnKeyList" type="string" default="organisationID,organisationID"><!--- this can contain a list of matching key columns e.g. primary key --->
			<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Search Results'*comma'Search Results'*comma'/data/dataframe.cfm?frmsrchOrgID=##OrganisationID##'*comma'');return false;, "> <!--- LID 2670 NJH 2009/09/23 --->
			<cfparam name="keyColumnOpenInWindowList" type="string" default="no,no">
		<cfelse>
			<cfparam name="keyColumnList" type="string" default="Partner"><!--- this can contain a list of columns that can be edited --->
			<cfparam name="keyColumnURLList" type="string" default=" "><!--- this can contain a list of matching columns that contain the URL of the editor template --->
			<cfparam name="keyColumnKeyList" type="string" default="organisationID"><!--- this can contain a list of matching key columns e.g. primary key --->
			<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Search Results'*comma'Search Results'*comma'/data/dataframe.cfm?frmsrchOrgID=##OrganisationID##'*comma'');return false"> <!--- LID 2670 NJH 2009/09/23 --->
			<cfparam name="keyColumnOpenInWindowList" type="string" default="no">
		</cfif>

		<cfparam name="dateFormat" type="string" default="DateApproved,Created">
		<cfparam name="showTheseColumns" type="string" default="Partner,Primary_Contact,HQ_Address,State,Country,Phone,CAM,Approval_Status,Approved,Created,New_Partner">

		<cfset accountResourceFlagGroupStructure = application.com.flag.getFlagGroupStructure(accManagerFlagTextID)>	<!--- 2011/11/24 PPB LID8074 switched from AccManager flag to AllocationSalesTechnical flag group --->

		<cfquery name="queryObject" datasource="#application.siteDataSource#">
			select distinct * from (
				select o.organisationID as OrganisationID, o.organisationName as Partner,isNULL(l.address1+', '+l.PostalCode,'-HQ Unassigned-') as HQ_Address,
				case when len(l.address5) > 0 then l.address5 else ' ' end as State,c.countryDescription as Country,isNULL(p.firstName+' '+p.lastName,'-Primary Contact Unassigned-') as Primary_Contact,
				isNULL(p.officePhone,'&nbsp;') as Phone,isNULL(p.email,'&nbsp;') as Email
				,isNULL(camD.data,0) as CAMID,
				isNULL(CAM.firstName+ ' '+CAM.lastName,'-Unassigned-') as CAM,
				isNULL(orgApprFlag.Name,'-NULL-') as Approval_Status,
				case when orgApprFlag.FlagTextID = 'OrgApproved' then convert(varchar, orgApprData.Created, 20)  else '&nbsp;' end as Approved,
				o.Created,case when pivotalDS.entityid is NULL then 'Yes' else 'No' end as New_Partner
				from organisation o
				inner join country c on o.countryID = c.countryID
				left join
					(integerflagdata HQData inner join flag HQFlag on HQData.flagid=HQFlag.flagid and HQFlag.FlagTextID='HQ' inner join location l on HQData.data=l.locationid
					) on o.organisationID = HQData.entityid
				left join
					(integerflagdata PCData inner join flag PCFlag on PCData.flagid=PCFlag.flagid and PCFlag.FlagTextID='KeyContactsPrimary' inner join person p on PCData.data=p.personid
					) on o.organisationID = PCData.entityid
				left join
					(booleanflagdata pivotalDS inner join flag pivotalDSflag on pivotalDS.flagid=pivotalDSFlag.flagid and pivotalDSFlag.FlagTextID='OrgDataSourcePivotal'
					) on o.organisationID = pivotalDS.entityid
				left join
					(booleanFlagData orgApprData
					inner join flag orgApprFlag on orgApprData.flagID=orgApprFlag.flagID
					inner join flaggroup orgApprStatus on orgApprFlag.flaggroupID=orgApprStatus.flaggroupID
						and orgApprStatus.flagGroupTextID='OrgApprovalStatus'
					) on o.organisationID = orgApprData.entityID
				inner join
					(person cam
					inner join #accountResourceFlagGroupStructure.FLAGTYPE.DATATABLEFULLNAME# camD
					on cam.personid=camD.data and camD.flagid in (select f.flagID from flagGroup fg inner join flag f on fg.flagGroupID=f.flagGroupID where fg.flagGroupID =  <cf_queryparam value="#accountResourceFlagGroupStructure.FLAGGROUPID#" CFSQLTYPE="CF_SQL_VARCHAR" >  ) 	<!--- 2011/11/24 PPB LID8074 switched from AccManager flag to AllocationSalesTechnical flag group --->
						and camD.data = #request.relaycurrentuser.personid#
					)
					on camD.entityID = o.organisationID

				where o.organisationID > 2
			) b
			where 1=1
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortorder#">
		</cfquery>

	<cfelse>
		<cfparam name="sortOrder" default="account">
		<cfparam name="myAccountFilterSelectFieldList" type="string" default="Account_Type,Country">
		<cfparam name="checkBoxFilterName" type="string" default="">
		<cfparam name="checkBoxFilterLabel" type="string" default="">

		<cfparam name="alphabeticalIndexColumn" type="string" default="account">
		<cfparam name="alphabeticalIndexColumnUnaliased" type="string" default="o.organisationName">
		<cfparam name="myAccountflagGroupTextIDToShow" type="string" default="mainOrgType">

		<!--- this switches on role based scoping of MyAccounts records only and stops users with
			leadManagerTask(1) see anything but what they own.  It is controlled by adding
			roleScopeOpportunityRecords to the opportunityINI.cfm file --->
		<!--- <cfparam name="roleScopeMyAccounts" default="yes"> --->
		<cfset roleScopeMyAccounts = application.com.settings.getSetting("accounts.roleScopeMyAccounts")>

		<cfparam name="frmAccountMngrID" default="0">
		<!--- NYB 2009-05-06 Sophos - *1 added this because it's required if frmAccountMngrID not 0 - yet was never included
				despite the name on the box this actually needs to be a Flag*GROUP*TextID
		--->
		<cfparam name="AccountMngrFlagTextID" default="AllocationSalesTechnical">

		<!--- START:  NYB 2009-05-06 Sophos - added: --->
		<cfparam name="keyColumnList" type="string" default="account">
		<!---<cfparam name="keyColumnURLList" type="string" default="/data/dataFrame.cfm?frmsitename=">--->
		<cfparam name="keyColumnURLList" type="string" default="OrganisationID">
		<cfparam name="keyColumnKeyList" type="string" default=" ">
		<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Search Results'*comma'Search Results'*comma'/data/dataframe.cfm?frmsrchOrgID=##OrganisationID##'*comma'');return false">
		<cfparam name="keyColumnOpenInWindowList" type="string" default="no">

		<cfparam name="dateFormat" type="string" default="last_contact_date,last_updated">
		<cfparam name="showTheseColumns" type="string" default="Account,Account_Type,Country,Last_Contact_Date,Last_Updated">
		<!--- END:  2009-05-06 Sophos --->

		<cfif roleScopeMyAccounts
			and not application.com.login.checkInternalPermissions("dataManagerTask","level3")>
				<cfset frmAccountMngrID = request.relaycurrentuser.personID>
		</cfif>

		<cfscript>
		// create an instance of the object
		myObject = createObject("component","relay.com.commonQueries");
		myObject.dataSource = application.siteDataSource;
		myObject.personid = request.relayCurrentUser.personid;
		myObject.userGroupID = request.relayCurrentUser.usergroupid;
		myObject.frmAccountMngrID = frmAccountMngrID;
		myObject.AccountMngrFlagTextID = AccountMngrFlagTextID; //NYB 2009-05-06 Sophos - see *1 above
		myObject.flagGroupTextID = myAccountflagGroupTextIDToShow;
		if (isdefined("FilterSelect") and FilterSelect neq ""
			and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
			myObject.FilterSelect = FilterSelect;
			myObject.FilterSelectValues = FilterSelectValues;
			}
		if (isdefined("FilterSelect2") and FilterSelect2 neq ""
			and isdefined("FilterSelectValues2") and FilterSelectValues2 neq "") {
			myObject.FilterSelect2 = FilterSelect2;
			myObject.FilterSelectValues2 = FilterSelectValues2;
			}
		myObject.alphabeticalIndexColumn = alphabeticalIndexColumnUnaliased;
		myObject.alphabeticalIndex = form["frmAlphabeticalIndex"];
		myObject.sortOrder = sortOrder;
		myObject.getUserAccounts();
		</cfscript>

		<!--- START:  NYB 2009-05-06 Sophos - added to give queryObject in CF_tableFromQueryObject the same variable,
					allowing it to be used outside these if's
					and because the filter wasn't working - tableFromQuery-QueryInclude.cfm hasn't been included in getUserAccounts
		--->
		<cfquery name="queryObject" dbtype="query">
			select *,account as organisation from myObject.qUserAccounts
			where 1=1
			<!--- NAS 2010/08/23 LID 3732 - My Accounts not able to filter by first letter of org name --->
			<!--- <cfinclude template="/templates/tableFromQuery- QueryInclude.cfm"> --->
			order by <cf_queryObjectName value="#sortorder#">
		</cfquery>
		<!--- END:  2009-05-06 Sophos --->

	</cfif>
	<!--- END:  NYB 2009-05-06 Sophos - added cfif use2009Format --->


	<cfinclude template="MyAccountFunctions.cfm">

	<!--- NYB 2009-05-06 Sophos - in cfif replaced myObject.qUserAccounts with queryObject
	--->
	<cfif not isquery(queryObject)>
		There is no Account Manager flag configured.
	<cfelse>
		<!--- NYB 2009-05-06 Sophos - in tableFromQueryObject replaced:
				myObject.qUserAccounts with queryObject
				keyColumnList="account" with keyColumnList="#keyColumnList#"
				keyColumnURLList="/data/dataFrame.cfm?frmsitename="  with keyColumnURLList="#keyColumnURLList#"
				keyColumnKeyList="account" with keyColumnKeyList="#keyColumnKeyList#"
				dateFormat="last_contact_date,last_updated"
				showTheseColumns="Account,Account_Type,Country,Last_Contact_Date,Last_Updated"
			  removed:
				hideTheseColumns="OrganisationID,alphabeticalIndex"
		--->
		<!--- NJH 2009/09/23 LHID 2671 re-instated the parameters as they were removed --->
		<CF_tableFromQueryObject
			queryObject="#queryObject#"
			sortOrder = "#sortOrder#"
			startRow = "#startRow#"
			numRowsPerPage="#numRowsPerPage#"

			keyColumnList="#keyColumnList#"
			keyColumnURLList="#keyColumnURLList#"
			keyColumnKeyList="#keyColumnKeyList#"
			keyColumnOnClickList="#keyColumnOnClickList#"
			keyColumnOpenInWindowList="#keyColumnOpenInWindowList#"


			dateFormat="#dateFormat#"
			FilterSelectFieldList="#myAccountFilterSelectFieldList#"
			alphabeticalIndexColumn="#alphabeticalIndexColumn#"
			showTheseColumns="#showTheseColumns#"
			allowColumnSorting="yes"

			useInclude="false"

			rowIdentityColumnName="OrganisationID"
			functionListQuery="#comTableFunction.qFunctionList#"
		>
	</cfif>
</cfif>

