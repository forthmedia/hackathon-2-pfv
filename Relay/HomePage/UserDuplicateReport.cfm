<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

userDuplicateReport.cfm

Author:  WAB

Date : Nov 2000

Purpose:  a summary report to give number of duplicates that the current User has to 
		work on designed to be included on a person's home page
	
Comments:  Very rough bit of work	
			Link for deduplicating people not perfected yet!
	

IMPORTANT NOTE
The link in this document needs to be changed for the live site from data to data2
	
--->

	<CFQUERY NAME="DupeLocs" DATASOURCE="#application.SiteDataSource#">
		select countryDescription, l.countryid, count( distinct l.dupegroup) as Loccount
		from country as c, location as l, flag as f , integerMultipleFlagData as ifd
		where l.dupegroup <> 0
		and f.flagTextID = 'countryDedupeContacts'
		and f.flagid = ifd.flagid
		and ifd.entityid = l.countryid
		and ifd.data = #request.relayCurrentUser.usergroupid#
		and l.countryid = c.countryid
		group by countryDescription, l.countryid
	</CFQUERY>

	
	<CFQUERY NAME="Dupepers" DATASOURCE="#application.SiteDataSource#">
		select countryDescription, l.countryid, count(distinct p.dupegroup) as percount
		from country as c, location as l, person as p, flag as f , integerMultipleFlagData as ifd
		where p.dupegroup <> 0
		and p.locationid = l.locationid
		and f.flagTextID = 'countryDedupeContacts'
		and f.flagid = ifd.flagid
		and ifd.entityid = l.countryid
		and ifd.data = #request.relayCurrentUser.usergroupid#
		and l.countryid = c.countryid
		group by countryDescription, l.countryid
	</CFQUERY>
	

	<CFOUTPUT>



	<TABLE>
	<TR><TH>DEDUPES</th></TR>
	<CFIF DupeLocs.recordCount is not 0>
	<TR>
		<TD colspan="2">There are a number of Locations in your countries to be deduped</td>
	</tr>
		<CFLOOP query="DupeLocs">
			<TR>
				<TD>#htmleditformat(CountryDescription)#</td><TD><A HREF="../data2/loclist.cfm?frmCountryid=#countryid#&frmDupeLocs=1&anyCriteria=yes">#htmleditformat(loccount)#</a></td>
			</tr>
		</cfloop>
	
	
	</cfif>
	
	<CFIF DupePers.recordCount is not 0>
	<TR>
		<TD colspan="2">There are a number of People in your countries to be deduped</td>
	</TR>
		<CFLOOP query="DupePers">
			<TR>
				<TD>#htmleditformat(CountryDescription)#</td><TD>#htmleditformat(percount)#</td>
			</tr>
		</cfloop>
	
	</cfif>
	
</table>	
	</cfoutput>
	
