<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		activityStream.cfm
Author:			NJH
Date started:	21-12-2012

Description:	A users social activity stream

Amendment History:

Date 		Initials 	What was changed
2013-10-11 	PPB 		Case 437050 defend against perApproved flag not existing
2016/01/05	NJH			Include fancy box by using the custom tag rather than initialising it in the script tag for consistency sake.
2016/01/07	RJT			BF-142 Fancy box shouldn't open activityStreamTitleLink as that may be a 3rd party site that may not allow iFraming (or may have different SSL state to us)
2016/02/16	NJH			PROD2015-612 - translate the tabs

Possible enhancements:
Allow an override for the perApproved flag cos Lexmark use SMB_perApproved, Services_perApproved and Supplies_perApproved instead

 --->



<cf_includeJavascriptOnce template="/javascript/social.js" translate="true">
<cf_includeJavascriptOnce template="/javascript/lib/jquery/jquery.expander.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.jscroll.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.mousewheel.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.jscrollpane.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/scroll-startstop.events.jquery.js">
<cf_includejavascriptOnce template="/javascript/lib/scrollbar.js">

<cf_includeCssOnce template="/javascript/lib/jquery/css/jquery.jscrollpane.css">
<cf_includeCssOnce template="/code/styles/social.css">

<cfset defaultFilterType = listFirst(filterTypes)>
<cf_head>
<cfoutput>
	<script>
		var globalFilterType = '#JSStringFormat(defaultFilterType)#';

		function setExpander() {
			jQuery('div.expandable p').expander({
			    slicePoint:       120,  // default is 100
			    expandPrefix:     ' ...', // default is '... '
			    expandText:       'phr_social_more', // default is 'read more'
			    collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
			    widow: 4,
			    userCollapseText: 'phr_social_less',  // default is 'read less'
			    preserveWords:true
			  });
		}
		// you can override default options globally, so they apply to every .expander() call
		//jQuery.expander.defaults.slicePoint = 120;

		jQuery(document).ready(function() {
		  // simple example, using all default options unless overridden globally

			if (jQuery(window).width() > 767) {
				jQuery('##activityStream').addClass('activityStream');
			}
			setTimeout(initialiseActivityStreamDiv,1000); // 20 secs

		});

		function initialiseActivityStreamDiv() {
			if (jQuery('##actStreamData').is('table')) {
				setExpander();
				if (jQuery(window).width() > 767) {
					initialiseJscroll();
					scrollBarFadeInOutInitialistion('##activityStream');
				}
				runActivityRefresh();
			} else {
				setTimeout(initialiseActivityStreamDiv,1000); // 20 secs
			}
		}

		function initialiseJscroll() {
			jQuery('##activityStream').jscroll({
				loadingHtml: '<img src="/images/icons/loading.gif" alt="Loading"/>',
				   padding: 80,
				   debug:false,
				   //contentSelector:'tr',
				   nextSelector:'##showMore'
			});
		}
	</script>
</cfoutput>
<cf_head>

<!--- BF-142 RJT activityStreamTitleLink shouldn't be included here as activityStreamTitleLinksa may be 3rd party sites which must not be opened in iFrames (as their headers may refuse to display) --->
<cf_modalDialog size="large" type="iframe" identifier=".socialProfile" hideOnContentClick="false">

<cfoutput>
<!--- 2013/01/29	YMA		Create fix to contain social profile --->
<div id="pageGrey" onclick="javascript:hidePersonSocialProfile();"></div>
<div id="personSocialProfile">
	<div id="personSocialProfileCloseBar">
		<input type="button" value="phr_sys_social_close" onClick="javascript:hidePersonSocialProfile();">
	</div>
	<div id="personSocialProfileContainer">
		<div id="personSocialProfileLoading"><p><img src='/images/icons/loading.gif'></p></div>
	</div>
</div>

<div id="activityStreamContainer">

	<div id="activityStreamHeading">
		<h2 id="headingDiv"><span class="fa fa-comments"></span>phr_social_activityStream</h2>
		<ul class="nav nav-tabs filterLinks" role="tablist">
			<Cfset x="">
			<cfset socialSettings = application.com.settings.getSetting("socialMedia")>
			<cfset validServices = "linkedIn,Facebook">
			<cfloop list="#filterTypes#" index="filterType">
				<cfif (listFindNoCase(validServices,filterType) and socialSettings.enableSocialMedia and listFindNoCase(socialSettings.services,filterType)) or not listFindNoCase(validServices,filterType)>
					<li class="<cfif filterType eq defaultFilterType>selected</cfif>"><a href="" title="phr_activityStream_#filterType#" id="#htmlEditFormat(filterType)#FilterType" class="<cfif filterType eq defaultFilterType>selected</cfif>" onClick="showActivityStream(1,'#jsStringFormat(filterType)#');return false;">phr_activityStream_#filterType#</a></li>
					<cfset x=listAppend(x,filterType)>
				</cfif>
			</cfloop>
		</ul>
	</div>

	<cfset approved = application.com.flag.getFlagData(entityID=request.relayCurrentUser.personID,flagID='perApproved')>
	<div id="userHeadingDiv" class="hidden-xs">
		#request.relayCurrentUser.picture#
		<div id="name">#htmlEditFormat(request.relayCurrentUser.person.publicProfileName)#</div>
		<div id="jobDesc">#htmlEditFormat(request.relayCurrentUser.person.publicProfileJobDesc)#</div>
		<div id="location">#htmlEditFormat(request.relayCurrentUser.person.publicProfileLocation)#</div>
		<cfif StructKeyExists(approved,"lastUpdated")>	<!--- 2013-10-11 PPB Case 437050 defend against this field not existing cos Lex don't have a perApproved flag --->
			<div id="userApproved">Joined #lsDateFormat(approved.lastUpdated)#</div>
		</cfif>
	</div>
	<div id="activityStreamContainerSub">
		<div id="activityStream">
			<img id="actStreamData" src="/images/icons/loading.gif" alt="Loading"/>
		</div>
	</div>
	<script>
		showActivityStream(1,'#jsStringFormat(defaultFilterType)#');
	</script>

</div>
</cfoutput>