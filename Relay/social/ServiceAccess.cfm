<!--- �Relayware. All Rights Reserved 2014 --->
<cfif application.com.settings.getSetting("socialMedia.enableSocialMedia")>

	<cfparam name="a" default="authenticate"> <!--- it's either authenticate or authorise --->
	<cfparam name="message" default="">
	<cfparam name="s" default="linkedIn">
	<cfparam name="callback" default="">
	<cfparam name="entityID" default="#request.relayCurrentUser.personID#">
	<cfparam name="entityTypeID" default="#application.entityTypeID.person#">
	
	<cfset callbackParams = structNew()>
	<cfif message neq "">
		<cfset callbackParams.message = message>
	</cfif>
	<!--- add ability to redirect to a given page... --->
	<cfif structKeyExists(url,"redirect")>
		<cfset callbackParams.redirect = url.redirect>
	</cfif>
	
	<!--- if it's a twitter account we're linking, then we're not necessarily connecting a person's account.. so we need to pass in the entityID and entityTypeID --->
	<cfif (entityTypeId neq application.entityTypeID.person or entityID neq request.relayCurrentUser.personID) and s eq "twitter">
		<cfif isNumeric(entityID) and isNumeric(entityTypeID)>
			<cfset callbackParams.entityID = entityID>
			<cfset callbackParams.entityTypeID = entityTypeID>
		</cfif>
	</cfif>
	
	<cfset callbackParams.s = application.com.encryption.encryptString(encryptType="standard",string=s)>
	
	<cfset argStruct = structNew()>
	<cfset argStruct.serviceID=s>
	<cfset argStruct.callbackParams=callbackParams>
	<cfif callback neq "">
		<cfset argStruct.callbackURL = callback>
	</cfif>

	<cfif a eq "authenticate">
		<cfset serviceRequest = application.com.service.getAuthenticateRequestString(argumentCollection=argStruct)>
	<cfelse>
		<cfset serviceRequest = application.com.service.getAuthoriseRequestString(argumentCollection=argStruct)>
	</cfif>

	<cfoutput>
		<form id="ServiceAccess" name="ServiceAccess" action="#serviceRequest.url#" method="post">
		</form>
	</cfoutput>

	<script>
		<cfif serviceRequest.method eq "post">
		document.ServiceAccess.submit();
		<cfelse>
		<!--- for facebook authorisation, we don't post, but just do a url call... --->
		<cfoutput>window.document.location.href = '#serviceRequest.url#';</cfoutput>
		</cfif>
	</script>
</cfif>