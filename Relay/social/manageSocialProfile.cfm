<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		manageSocialProfile.cfm
Author:			NJH
Date started:	15-03-2012

Description:	Screen to manage a users social profile. It is currently included as a screen element

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

23/06/2014 SB Change the template to reflect the responsive work done with Bootstrap.

 --->

<cfset socialMedia = application.com.settings.getSetting("socialMedia")>

<cfset validProfileServices = "linkedIn,facebook">

<cfset socialPofileServices = "">

<cfloop list="#socialMedia.services#" index="serviceID">
	<cfif listFindNoCase(validProfileServices,serviceID)>
		<cfset socialPofileServices = listAppend(socialPofileServices,serviceID)>
	</cfif>
</cfloop>

<cfif socialMedia.enableSocialMedia and socialPofileServices neq "" and not request.relayCurrentUser.isInternal>
	<cfoutput>

	<cfloop list="#socialPofileServices#" index="serviceID">
		<cfif structKeyExists(form,"frmUpdate_#serviceID#")>
			<cfset application.com.flag.processFlagForm (entityid=form.frmPersonID,entityType="person")>
			<!--- only need to run this function once, so break once one of these form fields exist --->
			<cfbreak>
		</cfif>
	</cfloop>

	<cfloop list="#socialPofileServices#" index="serviceID">
		<cfif structKeyExists(form,"frmDisconnectAccount_#serviceID#")>
			<cfset application.com.service.unlinkEntityFromService(serviceID=serviceID)>
		</cfif>
	</cfloop>

	<cf_head>
		<style type="text/css" media="screen,print">@import url("/code/styles/social.css");</style>
	</cf_head>

	<cfform name="frmSocialPreferences" action="">
		<h1>phr_SocialPreferences_Heading</h1>

		<div class="summary">phr_SocialPreferences_Description</div>


		<cfloop list="#socialPofileServices#" index="serviceID">

			<div id="SocialPreferences_#serviceID#" class="SocialPreferences">
				<div id="SocialPreferencesImageAndText_#serviceID#" class="SocialPreferencesImageAndText row">

					<cfset imageURL = "/images/social/#serviceID#_WebLogo.jpg">
					<cfif not fileExists(application.paths.relay&"\images\social\#serviceID#_WebLogo.jpg")>
						<cfset imageURL = "/images/social/#serviceID#_WebLogo.png">
					</cfif>

					<div id="SocialPreferencesImage_#serviceID#" class="SocialPreferencesImage col-xs-2 col-sm-2 col-md-2 col-lg-1">
						<img src="#imageURL#">
					</div>
					<div  class="SocialPreferencesBelowImage col-xs-10 col-sm-10 col-md-10 col-lg-11">

						<div id="SocialPreferencesImageText_#serviceID#" class="SocialPreferencesImageText">phr_SocialPreferences_Settings_#serviceID#</div>

						<div id="SocialPreferencesBelowImage_#serviceID#" class="SocialPreferencesBelowImage">
						<cfset entityLinked = application.com.service.hasEntityBeenLinked(serviceID=serviceID)>

						<cfif entityLinked.linkEstablished>

								<script>
									function optIn(flagID,service) {
										optInOptOut(flagID,'optIn',service);
									}

									function optOut(flagID,service) {
										optInOptOut(flagID,'optOut',service);
									}

									function optInOptOut(flagID,method,service) {
											page = '/webservices/callWebService.cfc?'
											parameters = 'method=callWebService&webServiceName=serviceWS&methodName='+method+'&flagID='+flagID+'&returnFormat=plain&_cf_nodebug=true&serviceID='+service;

											div = 'optInOptOutDiv'+service;
											var myAjax = new Ajax.Updater(
												div,
												page,
												{
													method: 'post',
													parameters: parameters,
													evalJSON: 'force',
													evalScripts: true,
													debug : false
												});
									}

									function displayProfileManagement(service){
										optInOptOut(0,'getProfileDisplay',service);
									}
								</script>

								<div id="optInOptOutDiv#serviceID#">
									<cfif serviceID eq "linkedIn"><script>displayProfileManagement('#serviceID#')</script></cfif>
								</div>

								<div id="SocialPreferencesButtons_#serviceID#" class="SocialPreferencesButtons">
									<input id="frmDisconnectAccount_#serviceID#" class="DisconnectAccountButton button btn btn-primary" type="button" value="phr_social_disconnectAccount" name="frmDisconnectAccount_#serviceID#" onClick="javascript:unLinkServiceAccount('#serviceID#',#request.relayCurrentUser.personID#,#application.entityTypeID.person#);">
								</div>
						</div>
						<cfelse>

								<div id="linkedInInfo">
									<div class="warningImage"></div>
									<div class="warningText">phr_social_#serviceID#Profile_linkTo#serviceID#</div>
								</div>

								<div id="SocialPreferencesButtons_#serviceID#" class="SocialPreferencesButtons">
									<cfset authLink = "window.open('/social/ServiceAccess.cfm?s=#serviceID#','#serviceID#Login','width=480px,height=320px')">
									<input type="button" value="phr_social_connectAccount" name="frmConnectAccount_#serviceID#" onClick="javascript:void(#authLink#);" class=" button btn btn-primary">	<!--- 2012/04/18 PPB P-REL112 I reverted this to INPUT from CF_INPUT because we don't have the security custom tag available --->
								</div>
						</div>
						</cfif>

					</div>
				</div>
			</div>
		</cfloop>
	</cfform>

	</cfoutput>


	<cf_includejavascriptonce template = "/javascript/social.js">

	<script>
		function unLinkServiceAccount(serviceID,entityID,entityTypeID) {

			if (confirm('phr_social_PersonDetailUnlinkButtonAreYouSure')) {
				unLinkAccount(serviceID,entityID,entityTypeID);
				<!--- var page = '/webservices/serviceWS.cfc?wsdl&method=unlinkEntityFromService';
				parameters = 'serviceID='+serviceID;

				var myAjax = new Ajax.Request(
								page,
								{
									method: 'post',
									parameters:  parameters,
									asynchronous:false,
									onComplete: function(){ window.location.href=window.location.href }
								}
							); --->
			}
		}
	</script>


</cfif>