<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ConnectOrgToLinkedIn.cfm
	Author:		PPB
Date started:	2012/02/03

Description:	Pass in a RW org name as a keyword and present the user with a list of LinkedIn Companies that match, allowing the user to select a LinkedIn company with which to link the RW org
 				Called from orgList3 to link the companies and called from orgDetails to change/unlink the link

Notes:			Phrases and ids referred to in css use the abbreviation "OrgToLI" for "ConnectOrgToLinkedIn"

Amendment History:

Date (YYYY/MM/DD)	Initials 	What was changed


Possible enhancements:

--->



<cfparam name="mode" default="add">

<cfif structKeyExists(url,"ServiceEntityID")>	<!--- if were displaying/editing an existing Link then url.ServiceEntityID is sent in --->
	<cfset mode = "edit">
</cfif>

<cfoutput>
<cfif structKeyExists(url,"s")>			<!--- this is done to strip off the s, oauth_token etc which are left over from the callback from LinkedIn --->
	<script>
		window.location.href = '/social/ConnectOrgToLinkedIn.cfm?entityID='+#url.entityID#
	</script>
</cfif>
</cfoutput>

<cf_head>
	<LINK REL='stylesheet' HREF='/Styles/social.css'>
</cf_head>
<!--[if gte IE 6]>
<link rel="stylesheet" type="text/css" href="/Styles/social_ie.css" />
<![endif]-->


<script type="text/javascript">

	window.resizeTo(500,458);				//nb  resized so that when we return to it from LinkedIn (after authentication) it isn't smaller

	//alert(opener.parent.getElementById('frmOrganisationID').name);

	var orgId = <cfoutput>#url.entityID#;</cfoutput>

	function unlinkEntity() {
		document.frmOrgToLICompanies.frmUnLinkClicked.value = 1;
		document.frmOrgToLICompanies.submit();
	}

	function closePopup(refreshOrgList) {
		if (opener) {
			if (refreshOrgList) {

				opener.location.href = opener.location.href;		// reloads the org list if we click on the button in the orglist OR reloads the org detail page if we click on the button on the org detail page

				if (opener.name.substr(0,12) == 'EntityDetail') {
					opener.parent.getOrgFrame().location.href = opener.parent.getOrgFrame().location.href;		// reloads the org list
				}

				//an abandoned attempt to refresh the orgDetail to show the edit button immediately after the link has been made
				//to do: it should refresh for a different org or for a person detail or if the current org is dirty

				if (opener.name.substr(0,7) == 'OrgList') {
					if ((opener.parent.getEntityFrame().getEntityDetailFrame()) && (orgId == opener.parent.getEntityFrame().getEntityDetailFrame().document.forms["mainForm"].frmOrganisationID.value)) {
						opener.parent.getEntityFrame().getEntityDetailFrame().location.href = opener.parent.getEntityFrame().getEntityDetailFrame().location.href;		// reloads the org detail
					}
				}

			}

			self.close(); 											/* close the popup */
		}
	}


	function loginWithLinkedIn(organisationID) {
		window.location.href='/social/ServiceAccess.cfm?callback='+ encodeURIComponent('/social/ConnectOrgToLinkedIn.cfm?entityID='+organisationID)
	}
</script>

<cfoutput>

<cfif structKeyExists(form,"frmSubmit")>		<!--- the form has been submitted --->
	<cfif structKeyExists(form,"frmServiceEntityID")>
		<cfset application.com.service.linkEntityToService(serviceID='LinkedIn',entityID=frmEntityID,entityTypeID=2,serviceEntityID=frmServiceEntityID)>
	</cfif>

	<script type="text/javascript">
		closePopup(true);
	</script>

<cfelseif structKeyExists(form,"frmUnlinkClicked") and form.frmUnlinkClicked eq 1>
	<cfset application.com.service.unlinkEntityFromService(serviceID='LinkedIn',entityID=frmEntityID,entityTypeID=2)>

	<script type="text/javascript">
		closePopup(true);
	</script>

<cfelse>

	<div id="OrgToLI_Header">
		<h2 class="logo">LinkedIn</h2>
	</div>

	<form id="frmOrgToLICompanies" name="frmOrgToLICompanies" action="" method="post">

		<cfset errorMessage = "">
		<cfset attemptToAuthorise = false>

		<cfif not application.com.service.hasEntityBeenLinked().linkEstablished>
			<cfset errorMessage ="phr_social_OrgToLI_NotLinked">
			<cfset attemptToAuthorise = true>
		<cfelse>
			<cfif structKeyExists(form,"frmSearchString")>	<!--- 2012/02/01 changed from frmSearch to frmSearchString cos frmSearch didn't exist as a form vble in IE --->
				<cfset companyName = form.frmSearchString>

			<cfelseif mode eq "edit">			<!--- if we editing an existing link then get the company name using the LinkedInCompanyId and use that for the search to guarantee its in the list if it was originally set by changing the search string (or the RW company name has been changed since the link was made) --->
				<cfset searchResult = application.com.linkedIn.getLinkedInCompanyDetails(linkedInCompanyId="#url.ServiceEntityID#")>

				<cfif StructKeyExists(searchResult.searchResponse,"name")>
					<cfset companyName = searchResult.searchResponse.name>
				<cfelse>
					<cfset companyName = "">			<!--- v unlikely but could happen --->
 				</cfif>

			<cfelse>
				<cfset companyName = application.com.relayPLO.getOrgDetails(OrganisationID=url.entityID,columnList='organisationName').organisationName>

				<!--- get the last word of the company name, if its Ltd or Plc etc then strip it off prior to the LinkedIn search --->
				<cfset lastWordOfCompanyName = listLast(companyName," ")>
				<cfif right(lastWordOfCompanyName,1) eq ".">
					<cfset lastWordOfCompanyName = left(lastWordOfCompanyName,len(lastWordOfCompanyName)-1)>	<!--- ignore a final "." on eg "plc." --->
				</cfif>

				<cfquery name="qryReplaceString" datasource="#application.siteDataSource#">
					SELECT 1 FROM MatchStringReplacement
					WHERE LTrim(StringToReplace)  =  <cf_queryparam value="#lastWordOfCompanyName#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and isNull(matchType,'name') = 'name'
				</cfquery>

				<cfif qryReplaceString.recordcount gt 0 and len(companyName)-len(lastWordOfCompanyName) gt 0>
					<cfset companyName = Trim(Left(companyName,len(companyName)-len(lastWordOfCompanyName)))>
				</cfif>

			</cfif>


			<div id="OrgToLI_Search">
				phr_social_OrgToLI_Search
				<cf_input name="frmSearchString" type="text" size="30" value="#companyName#">
				<cf_input name="frmSearch" type="image" src="/images/social/linkedin-search.png"  value="" >
			</div>

			<cfset searchResult = application.com.linkedIn.searchForCompany(keywords=#companyName#)>		<!--- nb. I specifically do not urlEncodedFormat this keyword (despite the linkedIn API documentation) because it would prevent eg russian char strings working; company names with a space seem to work without encoding --->
		</cfif>

		<div style="clear:both;"></div>
		<div class="horizontalLine"></div>


		<!--- even if there is an error above we let it drop thro to display the error neatly in the body section --->
		<div id="OrgToLI_Body">

			<cfif errorMessage eq "">
				<cfif searchResult.isOK>
					<cfif not (IsDefined("searchResult.searchResponse.companies_attributes.total") and searchResult.searchResponse.companies_attributes.total eq 0)>
						<cfset matchingCompanies = searchResult.searchResponse.companies.company>

							<div id="OrgToLI_Description">phr_social_OrgToLI_Description</div>

							<table width="100%" border="0" cellspacing="0" cellpadding="3" class="">
								<cfset currentrow = 0>

								<cfif IsStruct(matchingCompanies)>	<!--- if there is only 1 company returned it is in a structure so put it into an array for consistency --->
									<cfset companiesArray = arrayNew(1)>
									<cfset companiesArray[1] = matchingCompanies>
								<cfelseif IsArray(matchingCompanies)>
									<cfset companiesArray = matchingCompanies>
								</cfif>

								<cfloop array="#companiesArray#" index="entity">
									<cfset currentrow = currentrow + 1>
									<cfif currentrow mod 2 is not 0>
										<cfset rowclass="oddrow">
									<cfelse>
										<cfset rowclass="evenrow">
									</cfif>

									<tr class="#rowClass#" <cfif mode eq "edit" and  entity["id"] eq url.ServiceEntityID>style="background-color:##CCC;"</cfif>>			<!--- show the selected row high-lighted --->
										<td valign="top" width="12px;" style="padding:10px 10px 0 14px;">
											<cf_input type="radio" name="frmServiceEntityID" value="#entity["id"]#" checked="#IIF(mode eq 'edit' and entity.id eq url.ServiceEntityID,DE(true),DE(false))#">
										</td>
										<td valign="top">
											<cfif StructKeyExists(entity,"name")>
												<span id="OrgToLI_CompanyName">#entity["name"]#</span><br />
											</cfif>
											<cfif StructKeyExists(entity,"locations") and IsStruct(entity.locations) and StructKeyExists(entity.locations,"location")>
												<cfif IsStruct(entity.locations.location) and StructKeyExists(entity.locations.location,"address")>
													<cfset fullAddress = entity.locations.location.address>
												<cfelseif IsArray(entity.locations.location) and ArrayLen(entity.locations.location) gt 0>		<!--- oddly it seems there can be 2 addresses for a location rather than 2 locations (possibly caused by xmlFunctions.ConvertXmlToStruct) --->
													<cfset fullAddress = entity.locations.location[1].address>
												<cfelse>
													<cfset fullAddress = "">
												</cfif>
												<cfif IsStruct(fullAddress)>
													<cfif StructKeyExists(fullAddress,"street1")>
														#fullAddress["street1"]#,
													</cfif>
													<cfif StructKeyExists(fullAddress,"city")>
														#fullAddress["city"]#,
													</cfif>
													<cfif StructKeyExists(fullAddress,"postal-code")>
														#fullAddress["postal-code"]#
													</cfif>
												</cfif>
												<br /><br />
											</cfif>
											<cfif StructKeyExists(entity,"website-url")>
												<a href="#entity["website-url"]#" target="_blank">#entity["website-url"]#</a><br />
											</cfif>
											<cfif StructKeyExists(entity,"ticker")>
												[Stock Market Ticker:#entity["ticker"]#]<br />
											</cfif>
										</td>
										<td valign="top">
											<cfif StructKeyExists(entity,"logo-url")>
												<img src="#entity["logo-url"]#"><br />
											</cfif>
										</td>
										<td width="12">&nbsp;
										</td>
									</tr>

									<!--- <tr class="#rowClass#"><td>&nbsp;</td></tr> --->
								</cfloop>
							</table>

							<!--- <input name="frmServiceID" type="hidden" value="LinkedIn"> --->
							<!--- <input name="frmEntityTypeID" type="hidden" value="2"> --->
							<cf_input name="frmEntityID" type="hidden" value="#url.entityID#">
					<cfelse>
						<cfset errorMessage = "phr_social_OrgToLI_NoCompaniesFound">
					</cfif>
				<cfelse>
					<cfif StructKeyExists(searchResult.searchResponse,"message") and FindNoCase("unauthorized",searchResult.searchResponse.message)>
						<cfset errorMessage = "phr_social_OrgToLI_Unauthorized">
						<cfset attemptToAuthorise = true>
					<cfelse>
						<cfset errorMessage = "phr_social_OrgToLI_Failed">
					</cfif>
				</cfif>
			</cfif>

			<cfif errorMessage neq "">		<!--- don't show the buttons if there's an error; I have not moved them up to avoid disturbing the styling --->
				<div class="errorMessage">
					<p>#errorMessage#</p>
				</div>
			</cfif>

		</div>

		<div id="OrgToLI_Actions" class="form-group">
			<cfif errorMessage eq "">		<!--- don't show the buttons if there's an error; I have not moved them up to avoid disturbing the styling --->
				<cfif mode eq "edit">
					<cfset submitPhrase = "phr_social_OrgToLI_Change">
				<cfelse>
					<cfset submitPhrase = "phr_social_OrgToLI_Accept">
				</cfif>
				<cf_input name="frmSubmit" type="submit" value="#submitPhrase#" class="btn-primary btn">


				<cfif mode eq "edit">
					<cf_input name="frmUnLink" type="button" value="phr_social_OrgToLI_UnLink" onclick="javascript:unlinkEntity()" class="btn-secondary btn">
					<cf_input id="frmUnLinkClicked" name="frmUnLinkClicked" type="hidden" value="0">
					&nbsp;&nbsp;
				</cfif>
			<cfelse>
				<cfif attemptToAuthorise>
					<cf_input name="frmLink" type="button" value="phr_social_OrgToLI_LinkPerson" onclick="javascript:loginWithLinkedIn(organisationId=#entityID#)" class="btn-primary btn">
				</cfif>
			</cfif>

			<cfif mode eq "edit" or errorMessage neq "">
				<cfset cancelPhrase = "phr_social_OrgToLI_Cancel">
			<cfelse>
				<cfset cancelPhrase = "phr_social_OrgToLI_DontKnow">
			</cfif>

			<cf_input name="frmCancel" type="button" value="#cancelPhrase#" onclick="javascript:closePopup(false)" class="btn-secondary btn">
		</div>


	</form>

</cfif>

</cfoutput>
