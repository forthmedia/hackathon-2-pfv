<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		socialAdmin.cfm
Author:			NJH
Date started:		2012/10/16

Description:		A page to administrate social settings

Usage:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Enhancements still to do:

30/09/2014 SB Added div container for styling
2015-01-15	ACPK	FIFTEEN-80 Appended random numbers to thumbnail image URL to deal with caching issues
2015/03/04	NJH		Fifteen-198 - remove case sensitivity when comparing the file extension to what is allowed. Compare everything in lower case.
2015/11/30 	SB		Bootstrap
 --->

<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia")>
	<div class="infoblock">Social Media has not been enabled. To enable social media, go to <b>Admin-Settings-Social</b> or speak to an administrator if you do not have rights.</div>
	<cf_abort>
</cfif>

<cfset thumbnailWidth = 80>

<cf_head>
	<link rel="stylesheet" href="/styles/social.css">

	<script>
		var validImageExts = new Array('jpeg','jpg','png','gif')

		function validateForm() {
			filesArray = $('socialAdmin').getInputs('file');

			for (var i=0;i<filesArray.length;i++) 	{
				var filename = filesArray[i].value;
				if (filename != '') {
					var dot = filename.lastIndexOf(".");
					if (dot != -1) {
						var fileExt = filename.substr(dot+1,filename.length).toLowerCase();
						var isValidExt = false;
						for (var ix=0;ix<validImageExts.length;ix++) {
							if (fileExt == validImageExts[ix].toLowerCase()) {
								isValidExt = true;
								break;
							}
						}
						if (!isValidExt) {
							alert('Please ensure that the files are of type .jpg,.jpeg,.gif or .png')
							return false;
						}
					}
				}
			}

			return true
		}
	</script>
</cf_head>

<cfset shareEntityActions = duplicate(application.com.settings.getSetting("socialMedia.share"))>
<cfset socialImageDir = application.paths.content&"\social">

<cf_querySim>
	entityQuery
	display,value
	<cfloop collection="#shareEntityActions#" item="entityType"><cfoutput>phr_social_#entityType#|#entityType##chr(10)##chr(13)#</cfoutput>
	</cfloop>
</cf_querySim>

<cfquery name="entityQuery" dbtype="query">
	select * from entityQuery order by display
</cfquery>

<cfif structKeyExists(form,"frmSubmit")>
	<cfloop collection="#shareEntityActions[frmEntityType]#" item="action">
		<cfset id = "#frmEntityType##action#">
		<cfif structKeyExists(form,"shareText_#id#")>
			<cfset application.com.relayTranslations.ProcessPhraseUpdateForm()>
		</cfif>

		<cfif form["file_#id#"] neq "">
			 <cfset cffile=application.com.fileManager.uploadFile(fileField="file_#id#",destination=socialImageDir,nameConflict="Overwrite",accept="image/jpeg,image/gif,image/jpg,image/png")>
			 <cffile action="rename" source="#cffile.serverdirectory#\#cffile.serverFile#" destination = "#cffile.serverdirectory#\#id#.jpg">
			 <!--- <cfset application.com.relayImage.resizeImage(targetWidth=thumbnailWidth,targetHeight=thumbnailWidth,sourcePath=cffile.serverdirectory,sourceName="#id#.jpg",targetName="#id#.jpg")>
			  --->
			 <cfif cffile.serverFileExt neq "jpg">
				<cfset deleteFile = application.com.fileManager.deleteFile("#cffile.serverdirectory#\#cffile.serverFile#")>
			</cfif>
		<cfelseif structKeyExists(form,"delete_file_#id#")>
			<cfif fileExists(socialImageDir&"\#id#.jpg")>
				<cftry>
					<cffile action="delete" file="#socialImageDir#\#id#.jpg">
					<cfcatch>
					</cfcatch>
				</cftry>
			</cfif>
		</cfif>

		<cfif not structKeyExists(form,"share_#id#")>
			<cfset form["share_#id#"] = 0>
		</cfif>
		<cfset application.com.settings.insertUpdateDeleteSetting(variableName="socialMedia.share.#frmEntityType#.#action#",variableValue=form["share_#id#"])>
	</cfloop>
</cfif>

<cfset request.relayFormDisplay = "html-div">
<cfif not structKeyExists(form,"frmEntityType")>
	<cfset form.frmEntityType = entityQuery.value[1]>
</cfif>
<div id="socialAdminContainer">
<cfform method="post" id="socialAdmin" name="socialAdmin" onSubmit="return validateForm();" enctype="multipart/form-data">
<cf_relayFormDisplay class="">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<div class="grey-box-top">
				<p>phr_social_administerContent</p>
				<!--- 2015/11/30 SB <cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_social_administerContent" label="" spanCols="true" fieldname="frmHTML"> --->
				<!--- 2015/11/30 SB <cf_relayFormElementDisplay relayFormElementType="submit" currentValue="Save" label="" fieldname="frmSubmit"> --->
				<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmEntityType" label="Select Update Type" currentValue="#frmEntityType#" query="#entityQuery#" display="display" value="value" onChange="$('socialAdmin').submit();">
			</div>
		</div>
	</div>
	<cfloop collection="#shareEntityActions[frmEntityType]#" item="action">
		<cfset id = "#frmEntityType##action#">
		<div class="row">
			<div class="col-xs-12">
				<h3>phr_title_social</h3>
					<div class="row">
						<div class="col-xs-12 col-sm-6">
						<div class="grey-box-top">
							<div class="checkbox">
								<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="#htmlEditFormat("phr_social_"&id)#" fieldname="frmHTML" spanCols="false">
									<cf_input type="checkbox" id="share_#id#" value="1" checked="#application.com.settings.getSetting('socialMedia.share.#frmEntityType#.#action#')#">
								</cf_relayFormElementDisplay>
							</div>
							<div id="socialAdminUpdate">
								<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="Update Content" fieldname="frmHTML" spanCols="false">
									<cf_editPhraseWidget phraseTextID="share_#id#_text" required="true" displayAs="textArea" showMergeFields="true" showTextToolbar="true" rows=4 cols=100>
								</cf_relayFormElementDisplay>
							</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6"><div class="grey-box-top">
							<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="Update Content URL" fieldname="frmHTML" spanCols="false">
								<cf_editPhraseWidget phraseTextID="share_#id#_url" required="false" displayAs="input">
							</cf_relayFormElementDisplay>
							<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="Update Content URL Title" fieldname="frmHTML" spanCols="false">
								<cf_editPhraseWidget phraseTextID="share_#id#_urlTitle" required="false" displayAs="input">
							</cf_relayFormElementDisplay>
							<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="Update Content URL Image" fieldname="frmHTML" spanCols="false">
								<cfset urlImageExists = false>
								<cfif fileExists(socialImageDir&"\#id#.jpg")>
									<cfset urlImageExists = true>
								</cfif>
								<cfoutput>
									<div id="shareImageDiv">
									<cfif urlImageExists>
										<!--- 2015-01-15	ACPK	FIFTEEN-80 Appended random numbers to thumbnail image URL to deal with caching issues --->
										<cfoutput><img id="shareImage" src="/content/social/#id#.jpg?RAND=#Int(Rand()*100000)#"></cfoutput>
									<cfelse>
										<!--- <span id="noImage">No image set</span> --->
									</cfif>
									</div>
								</cfoutput>
								<div class="form-group">
									<cf_input type="file" id="file_#id#" name="file_#id#">
								</div>
								<div class="checkbox">
									<label>
										<cfif urlImageExists><span id="deleteImage"><cf_input type="checkbox" id="delete_file_#id#" value="1"> Delete Image</span></cfif>
									</label>
								</div>
							</cf_relayFormElementDisplay>
							<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="" fieldname="lineBreak" spanCols="true"/>
						</div>
						</div>
					</div>

			</div>
		</div>
	</cfloop>
	<cf_relayFormElementDisplay relayFormElementType="submit" currentValue="Save" label="" fieldname="frmSubmit">
</cf_relayFormDisplay>
</cfform>
</div>