<!--- �Relayware. All Rights Reserved 2014 --->
<cf_param name="fieldlist" label="Fields to display" type="string" default="PUBLICPROFILENAME,PUBLICPROFILEJOBDESC,PUBLICPROFILELOCATION,PUBLICPROFILEABOUT,PICTUREURL">
<cf_param name="thumbImageSize" label="Thumbnail Image Size" type="integer" default=80>
<cf_param name="maxSize_KB" label="Thumbnail Max File Size (kb)" type="numeric" default="200">
<cfparam name="editable" type="boolean" default="true">
<cfset maxSize_MB = NumberFormat(maxSize_KB/1024,'9.99')>

<cf_head>
	<script>
		jQuery(document).keyup(function(e) {
			 if (e.keyCode == 27) { hidePersonSocialProfile(); }   // esc
		});
	</script>
</cf_head>

<cfset personFields = structKeyList(request.relayCurrentUser.person.getFieldsMetaDataStruct())>
<!--- strip out any fields that are not in the person table --->
<cfloop list="#fieldlist#" index="field">
	<cfif not listFindNoCase(personFields,field)>
		<cfset fieldlist = listDeleteAt(fieldList,listFindNoCase(fieldList,field))>
	</cfif>
</cfloop>

<cfset destination=application.paths.content & "\linkImages\person\#frmPersonID#">

<cfif structKeyExists(form,"save")>
	<cfset personDetails = StructNew()>

	<cfif structKeyExists(form,"PICTUREURL") and form.PICTUREURL neq "">
		<cfset uploadedFileResult = application.com.relayImage.CreateThumbnail(targetSize=thumbImageSize,sourceFile="PICTUREURL",targetFilepath=destination,targetFileExtension="jpg",targetFileName="#frmpersonID#-square-thumb",maxSize_MB=maxSize_MB)>

		<cfif uploadedFileResult.isOK>
			<cfset uploadedFile = uploadedFileResult.imagePath>
			<cfset form.PICTUREURL = "#right(uploadedFile,len(uploadedFile)-len(application.paths.userfiles))#" >
			<cfset form.PICTUREURL = replace(form.PICTUREURL,"\","/","all")>
		<cfelse>
			<cfset form.PICTUREURL = "" >
			<cfoutput><script>alert('#uploadedFileResult.message#');</script></cfoutput>
		</cfif>
	<cfelseif structKeyExists(form,"deletelinkImage") and (form.deletelinkImage eq "on")>

		<cfdirectory action="list" name="qList" directory="#destination#" filter="#frmpersonID#-square-thumb.*">
		<cfloop query="qList">
			<cftry>
				<cflock name="deleteLinkImage-#qList.name#" timeout="5">
					<cfset application.com.fileManager.deleteFile("#directory#\#qList.name#")>
				</cflock>
				<cfset StructInsert(PersonDetails,"PICTUREURL","")>
				<cfcatch>
					<cf_mail to="errors@foundation-network.com,relayhelp@foundation-network.com"
				        from="relayhelp@foundation-network.com"
				        subject="Error deleting link image"
						type = "HTML">
						<cfdump var="#cfcatch#" label="cfcatch">
					</cf_mail>
				</cfcatch>
			</cftry>
		</cfloop>
	</cfif>

	<CFQUERY NAME="personPublicProfile" datasource="#application.siteDataSource#">
		Select * FROM vPersonPublicProfile WHERE personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<cfloop list="#personPublicProfile.columnList#" index="field">
		<cfif listfindnocase(form.fieldnames,field)>
			<cfset queryFieldValue = personPublicProfile[field][1]>
			<cfset formFieldValue = form[field]>
			<cfif ((field eq "PICTUREURL" and formFieldValue neq "") or (field neq "PICTUREURL"))>
				<cfset StructInsert(PersonDetails,field,formFieldValue)>
			</cfif>
		</cfif>
	</cfloop>

	<cfif not structIsEmpty(personDetails)>
		<cfset application.com.relayEntity.updatePersonDetails(personID=personPublicProfile.personID,PersonDetails=PersonDetails)>
	</cfif>
</cfif>

<cfset relayFormElementType="textarea">
<cfset disabled= not editable>

<CFQUERY NAME="personPublicProfile" datasource="#application.siteDataSource#">
	Select * FROM vPersonPublicProfile WHERE personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>



<form action="" method="POST" enctype="multipart/form-data" name="details" id="details" class="">
	<cf_relayFormDisplay class=""> <!--- NJH 2016/09/06 JIRA PROD2016-1200 --->
		<cfset list = isDefined("fieldlist") and fieldlist neq ""?fieldlist:personPublicProfile.columnList>
		<cfif listfindnocase(list,"PICTUREURL")>
			<cfoutput>
				<cfif request.relayFormDisplayStyle eq "html-table">

					<div id="imageContainer" style="margin-right:10px;margin-left:16.5%;">

						<div class="row control-label clearfix">
								<div class="col-xs-2">
									<cfif personPublicProfile.PICTUREURL eq "">
										<cfset imgSrc = "/images/social/icon_no_photo_80x80.png">
									<cfelse>
										<cfif fileExists(application.UserFilesAbsolutePath & personPublicProfile.PICTUREURL)>
											<cfset imgSrc = request.currentsite.HTTPPROTOCOL & request.currentsite.DOMAIN & "/" & personPublicProfile.PICTUREURL>
										<cfelse>
											<cfset imgSrc = personPublicProfile.PICTUREURL>
										</cfif>
									</cfif>
									<img src="#imgSrc#" style="width:#thumbImageSize#px" class="socialImage">
								</div>
								<div class="col-xs-10">
									<cfif not disabled>
										<CF_relayFormElement relayFormElementType="file" label="" size="15" fieldname="PICTUREURL" currentvalue="" acceptType="image">
										<p class="help-block">phr_sys_MaxFileSize #maxSize_KB# KB</p>
										<div class="checkbox">
											<cfif personPublicProfile.PICTUREURL neq "">
												<label for="deleteLinkImage"><input type="checkbox" id="deleteLinkImage" name="deleteLinkImage" class="checkbox">phr_sys_DeleteImage</label>
											</cfif>
										</div>
									</cfif>
								</div>
							</div>
					</div>

				<cfelse>

					<div class="form-inline">
						<div id="imageContainer" class=" <cfif disabled> </cfif>">
							<div class="row control-label clearfix">

								<div class="col-xs-2">
									<cfif personPublicProfile.PICTUREURL eq "">
										<cfset imgSrc = "/images/social/icon_no_photo_80x80.png">
									<cfelse>
										<cfif fileExists(application.UserFilesAbsolutePath & personPublicProfile.PICTUREURL)>
											<cfset imgSrc = request.currentsite.HTTPPROTOCOL & request.currentsite.DOMAIN & "/" & personPublicProfile.PICTUREURL>
										<cfelse>
											<cfset imgSrc = personPublicProfile.PICTUREURL>
										</cfif>
									</cfif>
									<img src="#imgSrc#" style="width:#thumbImageSize#px" class="socialImage">
								</div>
								<div class="col-xs-10">
									<cfif not disabled>
										<CF_relayFormElement relayFormElementType="file" label="" size="15" fieldname="PICTUREURL" currentvalue="" acceptType="image">
										<p class="help-block">phr_sys_MaxFileSize #maxSize_KB# KB</p>
										<div class="checkbox">
											<cfif personPublicProfile.PICTUREURL neq "">
												<label for="deleteLinkImage"><input type="checkbox" id="deleteLinkImage" name="deleteLinkImage" class="checkbox">phr_sys_DeleteImage</label>
											</cfif>
										</div>
									</cfif>
								</div>
							</div>
						</div>
					</div>
				</cfif>
			</cfoutput>
		</cfif>

		<cfloop list="#list#" index="fieldname">
			<cfif listfindnocase(personPublicProfile.columnList,fieldname)>
				<cfset currentValue=personPublicProfile[fieldname][1]>
				<cfset label="Phr_Sys_#fieldname#">
				<CFQUERY NAME="fieldLength" datasource="#application.siteDataSource#">
					select character_maximum_length as length from information_schema.columns
					where table_name = 'vPersonPublicProfile'
					and column_name  =  <cf_queryparam value="#fieldname#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</CFQUERY>
				<cfif fieldname neq "PICTUREURL">
					<cfset rows = 4>
					<cfset size = 40>
					<cfif fieldLength.length lt 500>
						<cfset rows = 1>
						<cfset size = 25>
					</cfif>
					<cfset inputWrapperOverrideClass = listFirst(list) eq fieldname?" ":" ">
					<CF_relayFormElementDisplay inputWrapperOverrideClass="#inputWrapperOverrideClass#" disabled="#disabled#" relayFormElementType="#relayFormElementType#" currentValue="#currentValue#" fieldName="#fieldName#" label="#label#" maxlength="#fieldLength.length#" size=#size# rows=#rows# cols="40" required="false">
				</cfif>
			</cfif>
		</cfloop>
		<cfif not disabled>
			<cf_encryptHiddenFields>
				<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmPersonID" currentvalue="#frmPersonID#">
			</cf_encryptHiddenFields>
			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="Save" currentValue="phr_save" label="" align="center">
		</cfif>
	</cf_relayFormDisplay>
</form>