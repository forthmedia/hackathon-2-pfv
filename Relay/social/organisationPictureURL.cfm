<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		organisationPictureURL.cfm	
Author:			YMA  
Date started:	26-02-2013
	
Description:	Manage organisation picture for use as a screen include.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-08-13			YMA			Case 436541 tweaked the way the code handles deleting an image to work without errors.
2014-01-21 			YMA 		Case 438616 allow this file to be used on the portal

Possible enhancements:


 --->

<cf_param name="thumbImageSize" label="Thumbnail Image Size" type="integer" default=80>
<cf_param name="maxSize_KB" label="Thumbnail Max File Size (kb)" type="numeric" default="200">
<cfset maxSize_MB = NumberFormat(maxSize_KB/1024,'9.99')>

<cfif not isDefined("frmOrganisationID") and request.relaycurrentuser.isInternal eq 0>
	<cfset frmOrganisationID = request.relaycurrentuser.organisationID>
</cfif>

<cfset destination = "\linkImages\organisation\#frmOrganisationID#">

<cfif structKeyExists(form,"btnSave")>
	<cfset orgDetails = StructNew()>
	
	<cfif structKeyExists(form,"PICTUREURL") and form.PICTUREURL neq "">
		<cfset uploadedFileResult = application.com.relayImage.CreateThumbnail(square=true,targetSize=thumbImageSize,sourceFile="PICTUREURL",targetFilepath=destination,targetFileName="#frmorganisationID#-square-thumb",maxSize_MB=maxSize_MB)>

		<cfif uploadedFileResult.isOK>
			<cfset uploadedFile = uploadedFileResult.imagePath>
			<cfset form.PICTUREURL = "#right(uploadedFile,len(uploadedFile)-len(application.paths.userfiles))#" >
			<cfset StructInsert(orgDetails,"pictureURL",form.PICTUREURL)>
		<cfelse>
			<cfset form.PICTUREURL = "" >
			<cfoutput><script>alert('#uploadedFileResult.message#');</script></cfoutput>
		</cfif>
	<cfelseif structKeyExists(form,"deletelinkImage") and (form.deletelinkImage eq "on")>
		<cfset destination=application.paths.content & destination>					
		<cfdirectory action="list" name="qList" directory="#destination#" filter="#frmorganisationID#-square-thumb.*">
		<cfloop query="qList">												
			<cftry>													
				<cflock name="deleteLinkImage-#qList.name#" timeout="5">
					<cfset application.com.fileManager.deleteFile("#directory#\#qList.name#")>
				</cflock>
				<cfcatch>
					<cf_mail to="errors@foundation-network.com,relayhelp@foundation-network.com" 
				        from="relayhelp@foundation-network.com"
				        subject="Error deleting link image"
						type = "HTML">
						<cfdump var="#cfcatch#" label="cfcatch">
					</cf_mail>
				</cfcatch>
			</cftry>
		</cfloop>
		<cfset StructInsert(orgDetails,"PICTUREURL","")>					
	</cfif>
	<cfif not structIsEmpty(orgDetails)>
		<cfset x = application.com.relayEntity.updateOrganisationDetails(organisationID=frmOrganisationID,organisationDetails=orgDetails,treatEmtpyFieldAsNull=false)>
	</cfif>
</cfif>

<CFQUERY NAME="orgURL" datasource="#application.siteDataSource#">
	Select pictureURL FROM organisation WHERE organisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
 
<!--- <cfset request.relayFormDisplayStyle = "HTML-table"> --->
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<cfform action="#CGI.SCRIPT_NAME#?#request.query_string#" method="POST" enctype="multipart/form-data" name="details" >
 	<cf_relayFormDisplay class="screenTable">

	<cfoutput>
		<cfif #orgURL.PICTUREURL# eq "">
			<cfset imgSrc = "/images/social/icon_no_photo_80x80.png">
		<cfelse>
			<cfif fileExists(application.UserFilesAbsolutePath & orgURL.PICTUREURL)>
				<cfset imgSrc = request.currentsite.HTTPPROTOCOL & request.currentsite.DOMAIN & "/" & orgURL.PICTUREURL>
			<cfelse>
				<cfset imgSrc = orgURL.PICTUREURL>
			</cfif>
		</cfif>
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<div id="imageContainer" style="float:left; margin-right:10px;">
				<table>
					<tr><td>
						<div style="height:#thumbImageSize#px; width:#thumbImageSize#px; display:table-cell; vertical-align:middle; border:1px solid black"><img src="#imgSrc#?refresh=#replace(rand(),".","","ALL")#" style="width:#thumbImageSize#px" class="socialImage"></div>
						<cfif orgURL.PICTUREURL neq "">
							<input type="checkbox" name="deleteLinkImage">&nbsp;phr_sys_DeleteImage
						</cfif>
					</td></tr>
					<tr><td><CF_relayFormElement relayFormElementType="file" label="" size="15" fieldname="PICTUREURL" currentvalue="" acceptType="image">phr_sys_MaxFileSize #maxSize_KB# KB
					</td></tr>
				</table>
			</div>
		<cfelse>
			<div class="form-group row">
				<div class="col-xs-12">
					<img src="#imgSrc#?refresh=#replace(rand(),".","","ALL")#" style="width:#thumbImageSize#px" class="socialImage">
					<cfif orgURL.PICTUREURL neq "">
							<div class="checkbox"><label><input type="checkbox" name="deleteLinkImage" class="checkbox">phr_sys_DeleteImage</label></div>
					</cfif>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-xs-12">
					<CF_relayFormElement relayFormElementType="file" label="" size="15" fieldname="PICTUREURL" currentvalue="" acceptType="image">phr_sys_MaxFileSize #maxSize_KB# KB
				</div>
			</div>
		</cfif>
	</cfoutput>

	<cf_encryptHiddenFields>
	<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmOrganisationID" currentvalue="#frmOrganisationID#">
	</cf_encryptHiddenFields>
	<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="btnSave" currentValue="phr_save" label="" align="center">

	</cf_relayFormDisplay>
</cfform>