<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		linkedInProfile.cfm
Author:			NJH
Date started:	26-01-2012

Description:	Update Profile from LinkedIn Details.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name="fieldlist" default="salutation,firstname,lastname,jobdesc,email,officePhone,mobilephone,faxPhone,language">
<cfparam name="requiredFields" default="">
<cfparam name="flagList" default="TwitterAccount,imAccount">
<cfparam name="showUnlinkAccount" default="true">



<cfset linkedInProfileExists = false>
<cfset linkedInDifferences = false>
<cfset screenStruct = structNew()>
<cfset errorMessage = "">
<cfset personFields = structKeyList(request.relayCurrentUser.person.getFieldsMetaDataStruct())>
<!--- strip out any fields that are not in the person table --->
<cfloop list="#fieldlist#" index="field">
	<cfif not listFindNoCase(personFields,field)>
		<cfset fieldlist = listDeleteAt(fieldList,listFindNoCase(fieldList,field))>
	</cfif>
</cfloop>

<cfset socialMedia = application.com.settings.getSetting("socialMedia")>

<cfif socialMedia.enableSocialMedia and listFindNoCase(socialMedia.services,"linkedIn") and not request.relayCurrentUser.isInternal>

	<cfset message = "phr_social_linkedInProfile_IncorrectDetails">

	<cfset entityLinked = application.com.service.hasEntityBeenLinked()>

	<cfif entityLinked.linkEstablished>
		<cfset profileResult = application.com.linkedIn.getProfile()>
		<cfset linkedInProfile = profileResult.profile>
		<cfif not profileResult.isOK>
			<cfset form.authoriseLink = profileResult.authoriseLink>
			<cfset errorMessage = "phr_social_linkedInProfile_unableToAccessLinkedInDetails">
		<cfelse>
			<cfset linkedInProfileExists = true>
		</cfif>
	</cfif>

	<script>

		document.mainForm.onreset = function() {
			var elements = $$("[id$=_use]");

			for (i=0;i<elements.length;i++) {
				if (elements[i].value == 'Relayware') {
			 		setName(elements[i]);
				}
			}
		};

		function setName(radioObj) {
			var fieldPrefix = radioObj.name.split('_use')[0];

			if (radioObj.value == 'Relayware') {
				$(fieldPrefix).setAttribute('name',fieldPrefix);
				$(fieldPrefix+'_linkedIn').removeAttribute('name');
			} else {
				$(fieldPrefix+'_linkedIn').setAttribute('name',fieldPrefix);
				$(fieldPrefix).removeAttribute('name');
			}
		}
	</script>

	<cfloop list="#fieldlist#" index="field">
	 	<cfif linkedInProfileExists and structKeyExists(linkedInProfile,field) and linkedInProfile[field] neq request.relayCurrentUser.person[field][1] and linkedInProfile[field] neq "">
			<cfset linkedInDifferences = true>
			<cfbreak>
		</cfif>
	</cfloop>

	<cfif linkedInDifferences or (entityLinked.linkEstablished and not linkedInProfileExists)>
		<cfset type="info">
		<cfif errorMessage neq "">
			<cfset type="warning">
			<cfset message = errorMessage>
		</cfif>
		<cfoutput>
		<div id="linkedInInfo">
			<div class="#type#Image"></div>
			<div class="#type#Text"><cfoutput>#message#</cfoutput></div>
		</div>
		</cfoutput>
	</cfif>
</cfif>

<!--- NJH 2013/11/28 - Case 437995 - move this outside of the above if statement as if the above condition was not true in any way, we didn't have the screen definition to output the Relayware fields on their own --->
<cfif isDefined("screenID")>
	<cfquery name="getScreenDef" datasource="#application.siteDataSource#">
		select required,fieldlabel,fieldTextID,useValidValues from screenDefinition sd
			inner join screens s on s.screenID = sd.screenID
		where s.screenTextID='MyProfile'
			and s.screenID = <cf_queryparam value="#screenID#" CFSQLTYPE="CF_SQL_INTEGER">
			and fieldSource = 'person'
	</cfquery>

	<cfset screenStruct = application.com.structureFunctions.queryToStruct(query=getScreenDef,key="fieldTextID")>
</cfif>

<cfloop list="#fieldList#" index="field">
	<cfset fieldname = "person_#field#_#request.relayCurrentUser.personID#">
	<cfset fieldValue = request.relayCurrentUser.person.get(field)>
	<cfset showLinkedInOption = false>

	<cfif linkedInDifferences>
		<cfif structKeyExists(linkedInProfile,field) and linkedInProfile[field] neq fieldvalue and linkedInProfile[field] neq "">
			<cfset showLinkedInOption = true>
		</cfif>
	</cfif>

	<cfset formParams = duplicate(application.com.relayEntity.getTableFieldStructure(tablename="person",fieldname=field).cfFormParameters)>
	<cfset formParams.required=false>
	<cfif structKeyExists(screenStruct,field) and screenStruct[field].required>
		<cfset formParams.required=screenStruct[field].required>
	<cfelseif listFindNoCase(requiredFields,field)>
		<cfset formParams.required=true>
	</cfif>

	<cfset profileFieldLabel = "phr_ext_#field#">

	<cfif structKeyExists(screenStruct,field) and screenStruct[field].fieldLabel neq "">
		<cfset profileFieldLabel = "phr_#screenStruct[field].fieldLabel#">
	</cfif>

	<!--- needed for screens --->
	<cfif formParams.required>
		<cfset javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form."&fieldname&",'phr_screen_enterAValueFor "&profileFieldLabel&"',1);" >
	</cfif>

	<cfoutput>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3 control-label<cfif formParams.required> required</cfif> linkedInLabelTop">
			<label <cfif formParams.required>class="required"</cfif>>#profileFieldLabel#</label>
		</div>
			<cfset fieldStruct = "Relayware">
			<cfif showLinkedInOption>
				<cfset fieldStruct = listAppend(fieldStruct,"LinkedIn")>
			</cfif>

			<cfloop list="#fieldStruct#" index="source">
				<div class="<cfif listFind(fieldList,field) eq 1>col-xs-10 col-sm-6<cfelse>col-xs-12 col-sm-9</cfif> linkedInDifferenceRow">
					<cfif showLinkedInOption>
						<cfset fieldclass = "linkedInTrue">
						<cfif source eq "linkedIn">
							<div class="useLinkedInLabel">
								<img src="/images/social/LinkedIn_IN_Icon_25px.png" />
								<h5>phr_social_Use#source#Value</h5>
							</div>
						<cfelse>
							<div class="keepThisLabel">
								<h5>phr_social_Use#source#Value</h5>
							</div>
						</cfif>
						<cf_input type="radio" name="#fieldname#_use" id="#fieldname#_use" class="pull-left" value="#source#" checked="#IIF(source eq 'Relayware',DE('true'),DE('false'))#" onChange="javascript:setName(this)">
					</cfif>

					<cfif structKeyExists(screenStruct,field) and screenStruct[field].useValidValues and source eq "Relayware">
						<cf_displayValidValues
							validFieldName = "person.#field#"
							formFieldName = "#fieldname#"
							displayas = "select"
							currentValue = #fieldValue#
							countryID = #request.relayCurrentUser.CountryID#
							entityID = #request.relayCurrentUser.personID#
							required = #formParams.required#
							useCFFORM = true
							>
					<cfelse>
						<cfif source eq "linkedIn">
							<cfset fieldValue = linkedInProfile[field]>
							<cfset fieldname = fieldname&"_linkedIn">
							<cfset formParams.readOnly = true>
							<cfset formParams.class = "linkedInText">
						</cfif>
						<cfif showLinkedInOption>
							<cfset fieldclass = "linkedInTrue">
						<cfelse>
							<cfset fieldclass = "linkedInFalse">
						</cfif>

						<!--- 2015/01/28	YMA Case 442683 apply phone mask throughout table fields accross portal --->
						<cfif not structKeyExists(formParams,"mask") and (fieldname contains "phone" or fieldname  is "Fax") and fieldname does not contain "ext">
								<cfset formParams.mask = application.com.commonQueries.getCountry(countryid=variables.countryid).telephoneformat>
						</cfif>
						<cfif structKeyExists(formParams,"mask")>
							<script>
								jQuery(document).ready(function(){
								<cfoutput>
									checkForMaskAttributeOnElementArray($("#fieldname#").up("form").getElements())
								</cfoutput>
								});
							</script>
						</cfif>

						<cf_relayFormElement fieldname="#fieldname#" class="#fieldclass#" relayFormElementType="text" label="" currentValue=#fieldValue# maxlength=#application.com.relayEntity.getTableFieldStructure(tablename="person",fieldname=field).maxLength# size="25" attributeCollection=#formParams#>
					</cfif>
				</div>
			</cfloop>

			<cf_input type="Hidden" name="#fieldname#_orig" value="#fieldValue#">
			<cfif showLinkedInOption><cf_input type="Hidden" id="#fieldname#_linkedIn" value="#linkedInProfile[field]#"></cfif>
		<cfif listFind(fieldList,field) eq 1>
			<div id="profilePicture" class="col-xs-2 col-sm-2 pull-right"><cfoutput>#request.relayCurrentUser.picture#</cfoutput></div>
		</cfif>
	</div>
	</cfoutput>
</cfloop>
<cfset x = "">
<cfloop list="#fieldList#" index="field">
	<cfset x = x & ',person_' & field>
</cfloop>

<cfif left(x,1) eq ','>
	<cfset x = right(x,len(x)-1)>
</cfif>

<cfoutput>
<div class="form-group">
	<div class="col-xs-12 col-sm-3 control-label">
		<label>Password</label>
	</div>
	<div class="col-xs-6 col-sm-5">
		<!--- CASE 428039 NJH 2012/08/22 set autocomplete to off to avoid FF asking user to save password. --->
		<cf_input class="form-control" type="password" value="xxxxxxx" id="frmPassword" disabled=disabled autocomplete="off">
	</div>
	<div class="col-xs-6 col-sm-4">
		<input type="button" value="phr_login_ChangePassword" onClick="window.document.location.href='/?eid=changePassword'" id="changePasswordBtn" class="btn btn-primary">
	</div>
</div>
</cfoutput>

<cfoutput>
	<cf_input type="hidden" name="frmPersonFieldList" value="#x#"> <!--- used for screens code --->
	<cf_input type="hidden" name="frmTableList" value="person">
	<cf_input type="hidden" name="frmPersonIDlist" value="#request.relayCurrentUser.personID#">
	<cf_input type="hidden" name="frmPersonFlaglist" value="">
	<cf_input type="hidden" name="person_lastUpdated_#request.relayCurrentUser.personID#" value="#request.requesttime#">
</cfoutput>