<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		callback.cfm	
Author:			NJH  
Date started:	26-10-2012
	
Description:	Callback for social services		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012/10/25			NJH			Social CR - gave user option to opt out of sharing the linking of their accounts

Possible enhancements:


 --->

<cfif application.com.settings.getSetting("socialMedia.enableSocialMedia")>

	<cfset webLogo = "/images/social/relayware_webLogo.png">
	<cfset imgExtList = "jpg,png">
	<cfloop list="#imgExtList#" index="fileExt">
		<cfif fileExists("#application.paths.code#\images\social\socialWebLogo.#fileExt#")>
			<cfset webLogo = "/code/images/social/socialWebLogo.#fileExt#">
			<cfbreak>
		</cfif>
	</cfloop>

	
	
	<cfset linkPersonAccount = false>
	<cfset accountLinked = false>
	<cfset serviceID = "linkedIn">
	<cfset errorMessage = "">
	
	<!--- set in relayCurrentUser if accounts are already linked --->
	<cfif structKeyExists(request,"errorMessage")>
		<cfset errorMessage = request.errorMessage>
	</cfif>
	
	<cfif structKeyExists(url,"s")>
		<cfset serviceID = application.com.encryption.decryptString(encryptType="standard",string=url.s)>
	<cfelseif structKeyExists(form,"frmServiceID")>
		<cfset serviceID = form.frmServiceID>
	</cfif>

	<!--- if these variables exist on the url and the user has not been logged in, then they've tried to login with linkedIn but have not yet linked their accounts --->
	<cfif not request.relayCurrentUser.isLoggedIn>
		<cfif structKeyExists(url,"oauth_token") and structKeyExists(url,"oauth_verifier")>
			<!--- if these url variable exist, then we should have a linkedIn ID --->
			<cfif structKeyExists(request,"serviceEntityID")>
				<!--- check to see that it has not yet been connected. --->
				<cfif not application.com.service.doesEntityExistWithServiceEntityID(serviceID=serviceID,ServiceEntityID=request.serviceEntityID,entityTypeID=0).entityExists>
					<cfset linkPersonAccount = true>
				<cfelse>
					<cfset errorMessage = "phr_social_accountHasAlreadyBeenConnected">
				</cfif>
			<cfelse>
				<cfset errorMessage = "phr_social_problemConnectingWithService">
			</cfif>
		<!--- NJH Case 436757 add linkedIn here as well --->
		<cfelseif structKeyExists(url,"s") and listFindNoCase("facebook,linkedIn",serviceID) and structKeyExists(url,"code") and not application.com.service.hasEntityBeenLinked(serviceID=serviceID).linkEstablished>
			<cfset linkPersonAccount = true>
		</cfif>
	</cfif>
	
	<!--- attempt to link the RW and linkedIn accounts --->
	<cfif structKeyExists(form,"frmLinkAccount")>
		<cfset result = application.com.login.getWhyLoginFailed(username=form.frmUsername,password=form.frmPassword,passwordEncrypted=false)>

		<cfif not result.isOK>
			<cfset request.serviceEntityID = frmServiceEntityID>
			<cfset frmMessage = "phr_social_invalidLoginCredentials">
		<cfelseif result.user.recordCount>
			<cfif not application.com.service.doesEntityExistWithServiceEntityID(serviceID=form.frmServiceID,ServiceEntityID=form.frmServiceEntityID,entityTypeID=0).entityExists>
				<cfset CreateObject("component", "oauth.oauthtoken").setAccessToken(serviceID=form.frmServiceID,accessToken=session.accessToken,entityID=result.user.personID[1])>

				<cfset personLinked = application.com.service.linkEntityToService(serviceID=form.frmServiceID,serviceEntityID=form.frmServiceEntityID,entityID=result.user.personID[1])>
				<cfif personLinked.isOK>
					<cfset accountLinked = true>
				</cfif>
			<cfelse>
				<cfset frmMessage = "phr_social_accountHasAlreadyBeenConnected">
			</cfif>
		</cfif>
	
	<!--- at the moment, we will only be here for logging in with facebook, as linkedIn gets posted to LinkedIn --->
	<!--- NJH Case 436757 commented out --->
	<!--- <cfelseif structKeyExists(form,"frmSignIn")>
		<cfoutput>
			<script>
				window.document.location.href='#application.com.facebook.getAuthenticateRequestString().url#';
			</script>
		</cfoutput>
		<cfabort> --->
	</cfif>
	
	<cfoutput>
	<cf_displayBorder
		noborders=true
	>
	<cf_title>phr_social_ServiceAuthorisationConnection</cf_title>
	<cf_head>
		<!--- Need to create user files css --->
		<style type="text/css" media="screen,print">@import url("/code/styles/social.css");</style>

		<script language="JavaScript">			
			<!--

			function resizeWin() {
				var doc = window.document;
				var winWidth = Math.max (doc.body["scrollWidth"], doc.documentElement["scrollWidth"], doc.body["offsetWidth"], doc.documentElement["offsetWidth"]);
				var winHeight = Math.max (doc.body["scrollHeight"], doc.documentElement["scrollHeight"], doc.body["offsetHeight"], doc.documentElement["offsetHeight"]);
			    window.resizeTo(winWidth,winHeight);
			}
			
			function refreshParent() {
				<cfif structKeyExists(url,"redirect") and serviceID eq "linkedIn">
					window.opener.location.href = '/?eid=#jsStringFormat(url.redirect)#';
				<!--- NJH 2013/02/25 Adding some url variables if they exist --->
				<cfelseif not structKeyExists(url,"oauth_problem")>
					queryParams = window.opener.location.search.toQueryParams();
					<cfif structKeyExists(url,"message")>queryParams.message='#jsStringFormat(url.message)#';</cfif>
					<cfif request.relayCurrentUser.isInternal>
						<cfif structKeyExists(url,"entityID")>queryParams.entityID=#jsStringFormat(url.entityID)#;</cfif>
						<cfif structKeyExists(url,"entityTypeID")>queryParams.entityTypeID=#jsStringFormat(url.entityTypeID)#;</cfif>
					</cfif>
					window.opener.location.href = window.opener.location.href.split('?')[0]+'?'+$H(queryParams).toQueryString();
				</cfif>
				window.close();
			}
			
			<!--- if we don't have to link an account and the account has not just been linked, then close the window and refresh parent page --->
			<cfif not linkPersonAccount and not structKeyExists(form,"frmLinkAccount") and errorMessage eq "">
			refreshParent();
			</cfif>
			//-->
		</script>
	</cf_head>
	
	<cf_body onload="javascript:resizeWin();">
		
		<cfset thisEntityID = request.relayCurrentUser.personID>
		<cfset thisEntityTypeID = application.entityTypeID.person>
		<cfif structKeyExists(url,"entityID") and structKeyExists(url,"entityTypeID")>
			<cfset thisEntityID = url.entityID>
			<cfset thisEntityTypeID = url.entityTypeID>
		</cfif>
		
		<cfset profileResult = application.com.service.getProfile(entityID=thisEntityID,entityTypeID=thisEntityTypeID,serviceID=serviceID,fieldList="first-name,last-name,picture-url")>
		<cfset profile = profileResult.profile>
		<cfset form.firstname = profile.firstname>
		
		<cfif not profileResult.isOk and errorMessage eq "">
			<cfset errorMessage = "phr_social_unableToGetDetails">
		</cfif>
		
		<cfset form.service = application.com.service.getService(serviceID=serviceID).name>
		<cfif errorMessage neq "" >
			<cfset message = "<div class='warning'>#htmlEditFormat(errorMessage)#</div>">
		<cfelseif linkPersonAccount or (structKeyExists(form,"frmLinkAccount") and not accountLinked)>
			<cfset message = "<p>phr_social_ConnectlinkedInAccount</p>">
		<cfelse>
			<cfset message = "<div id='successDiv'><span class='success' id='successText'>phr_social_accountsConnectedSuccess</span></div>">
		</cfif>
		
		<cfsavecontent variable="serviceProfile">
			<cfoutput>
			<div id="serviceProfile">
				<div class="form-group">
					<div class="col-xs-12 col-sm-offset-3 col-sm-9">
						<h3>phr_#serviceID# phr_account</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-6 text-right">
						#profile["firstname"]# #profile["lastname"]#
					</div>
					<div class="col-xs-6">
						<img id="profileImage" style="width:100%; max-width:50px;" src="#profile['pictureURL']#"/>
					</div>
				</div>
			</div>
			</cfoutput>
		</cfsavecontent>
					
		<!--- here we need to link the person's RW account to linkedIn. Display the connect form. This may be the first time, or they have tried
			and have entered the wrong RW credentials --->	
		<cfif linkPersonAccount or (structKeyExists(form,"frmLinkAccount") and not accountLinked)>

			<cfif profileResult.isOK>
				<cfform name="connectAccountForm" action="/social/callback.cfm" method="post">
					<cf_relayFormDisplay class="">
							
					<div id="connectHeader">
						<div class="form-group">
							<div class="col-xs-12 connectingApps">
								<img id="linkedInLogo" src="/images/social/#serviceID#_WebLogo.png"/>
								<img src="/images/social/blue_plus.png"/>
								<div class="weblogo"></div>
							</div>
						</div>
						<div id="headerText"><cfoutput>#message#</cfoutput></div>
						<div id="connectOuterDiv">
							#serviceProfile#
							<div id="rwProfile">
								<div id="connect">
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-9 col-xs-12"><h3>#request.currentsite.title# phr_account</h3></div>
									</div>
									<cfif isDefined("frmMessage") and frmMessage neq "">
										<div class="form-group">
											<div class="col-xs-12">#application.com.relayui.message(message=frmmessage,type="error")#</div>
										</div>
									</cfif>
									<cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmUsername" currentValue="" label="phr_LoginScreen_Username">
									<cf_relayFormElementDisplay relayFormElementType="password" fieldname="frmPassword" currentValue="" label="phr_LoginScreen_Password">
									<!--- only support shares for LinkedIn at the moment --->
									<cfif serviceID eq "linkedIn">
										<div class="form-group">
											<div class="col-xs-12 col-sm-offset-3 col-sm-9">
												<div class="checkbox col-xs-12 col-sm-6 col-md-4">
													<label>phr_social_doNoSendSocialUpdate<cf_relayFormElement relayFormElementType="checkbox" fieldname="frmDoNotShareLink#serviceID#Account" currentValue="" label="phr_social_doNoSendSocialUpdate"></label>
												</div>
											</div>
										</div>
									</cfif>
									<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmLinkAccount" currentValue="Connect Accounts" label="">
								</div>
								
								<cfif structKeyExists(request,"serviceEntityID")>
									<cfset frmServiceEntityID = request.serviceEntityID>
								</cfif>
								
								<cf_encryptHiddenFields>
									<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmServiceEntityID" currentValue="#frmServiceEntityID#" label="">
									<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmServiceID" currentValue="#serviceID#" label="">
								</cf_encryptHiddenFields>
							</div>
						</div>
					</div>
					</cf_relayFormDisplay>
				</cfform>
			</cfif>

		<!--- here we have linked the RW account to their linkedIn account and we now logging in with the service --->
		<cfelseif accountLinked>

			<!--- NJH Case 436757 changes--->
			<cfset urlparam = "?s="& application.com.encryption.encryptString(encryptType='standard',string=serviceId)>
			<cfif form.frmServiceID eq "linkedIn">
				<cfif application.com.settings.getSetting("socialMedia.redirectPostLinkedInConnect") neq "">
					<cfset urlparam = urlparam&"&redirect="&application.com.settings.getSetting("socialMedia.redirectPostLinkedInConnect")>
				</cfif>
			</cfif>
			<!--- <cfif form.frmServiceID eq "linkedIn">
				<cfset callbackParams = {s=application.com.encryption.encryptString(encryptType="standard",string=form.frmServiceID)}>
				<cfif application.com.settings.getSetting("socialMedia.redirectPostLinkedInConnect") neq "">
					<cfset callbackParams.redirect=application.com.settings.getSetting("socialMedia.redirectPostLinkedInConnect")>
				</cfif>
				<cfset serviceRequest = application.com.service.getAuthenticateRequestString(serviceID=form.frmServiceID,callbackURL="#request.currentSite.protocolAndDomain#/social/callback.cfm",callbackParams=callbackParams)>
			<cfelseif form.frmServiceID eq "facebook">
				<cfset serviceRequest = {url=""}>
			</cfif> --->
			
			<cfform name="ServiceAccess" action="#urlparam#" method="post">				
				<cf_relayFormDisplay class="">
					<div id="connectOuterDiv" class="row">
						#serviceProfile#
						<div classs="col-xs-12">
							<div class="col-xs-12">#application.com.relayui.message(message="phr_social_accountSuccessfullyLinked",type="error")#</div>
						</div>
						<cf_relayFormElementdisplay relayFormElementType="submit" fieldname="frmSignInWithService" currentValue="phr_social_ContinuetoPortal" label="" spanCols="true">
					</div>
				<cf_encryptHiddenFields>
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmServiceEntityID" currentValue="#frmServiceEntityID#" label="">
				</cf_encryptHiddenFields>
				</cf_relayFormDisplay>
			</cfform>
			
		<!--- if everything is fine, then refresh the parent and close --->
		<cfelseif errorMessage eq "">
			<script>
				refreshParent();
			</script>
		</cfif>
	
	</cfoutput>
	
</cfif>