<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			organisationPublicProfile.cfm
Author:				YMA
Date started:		2013-10-22

Purpose:	Org profile display and Web to lead form for locator.

Usage:



Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
18/11/2013			YMA			Case 437561 evault would like to allow location screens.
2014-01-17  		AXA			Case 438630 - passing params to Locator
2014-09-02			RMC			Amended the screen to utilise a tabbed layout & performed a code clean-up.
2014-11-25			RPW			CORE-97 Phase 1: Extend database fields for Leads
2015-11-27  		WAB 		PROD2015-458/BF-27 Added allowFramingByAllSites()
2016-02-18			RJT			BF-229 Where a logged in user uses the partner locator then personal details are not asked for. But the code expected these and so failed to an error. This is now resolved
2016-03-11			WAB			Set relayformDisplay classes to "" (for stacked layout)
2016-07-04 			DCC			for Anna case 449822 Locator Contact process not working: Instead of picking up the details entered into the form, they are picking up the details of the person to whom the email goes
2016/07/28			NJH			JIRA PROD2016-599/351 - moved primary contacts to location level. Deal also with multiple primary contacts. Change the default value of PrimaryContactFlag to PrimaryContacts

Possible enhancements:


--->
<cfset application.com.request.allowFramingByAllSites()>

<cfif not structKeyExists(url,"forOrganisationID") or not structKeyExists(url,"forLocationID") or not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="forLocationID,forOrganisationID")>
	<cfdump var="This code should be called from locator v4 with ContactReseller enabled.  Make this page hidden from the content tree.">
	<cfabort>
</cfif>

<cfset frmorganisationid = url.forOrganisationID>
<cfset frmlocationid = url.forLocationID>

<cf_param name="contactType" label="Contact Type" type="string" default="newLead" validvalues="select 'No Contact' as display, 'noContact' as value union select 'New Lead' as display, 'newLead' as value union select 'Email (Create End User)' as display, 'email' as value union select 'Email (Do Not Create End User)' as display, 'emailDontCreatePerson' as value"/>
<cf_param name="hqLocationFlagID" label="HQ Location Profile" type="string" default="HQ" validvalues="select f.flagtextID as value, f.name as display from flag f join flaggroup fg on f.flaggroupID = fg.flaggroupID where fg.entitytypeID = 2 and f.flagtextID like '%hq%' order by f.name"/>
<cf_param name="orgApprovalFlagID" label="Organization Approval Status Profile" type="string" default="OrgApproved" validvalues="select f.flagtextID as value, f.name as display from flag f join flaggroup fg on f.flaggroupID = fg.flaggroupID where fg.entitytypeID = 2 and f.flagtextID like '%orgapprove%' order by f.name"/>
<cf_param name="PrimaryContactFlag" label="Partner Contact Profile" type="string" default="PrimaryContacts" validvalues="select f.flagtextID as value, f.name as display from flag f join flaggroup fg on f.flaggroupID = fg.flaggroupID where fg.entitytypeID in (1,2) and fg.flagtypeID in (6,8) and f.active = 1 and linksToEntityTypeID = 0 order by f.name"/>
<!--- 2013-11-19 - YMA - 437561 - Added on includedScreen so Location Screens can be added --->
<!--- 2014-09-02 - RMC - Updated this to allow multiple sceens to be referenced for use in a tabbed layout --->
<cf_param name="includedScreen" label="Included Screen" type="string" default="#application.com.settings.getSetting('screens.orgDetailsScreenID')#" validvalues="select screentextID as value, screenname as display from screens where entitytypeID in (1,2) order by screenname" displayas="twoselects" allowsort="true" />
<cf_param name="Enquiry" label="Enquiry" type="string" default="Sales Inquiry,Service Request,Partnering Inquiry,Other"/>
<cf_param name="leadEmail" label="Email to send" default="LeadEmail" relayformelementtype="select" nulltext="phr_Ext_SelectAValue" shownull="true" bindonload="true" display="display" value="value" bindfunction="cfc:webservices.socialWS.orgPublicProfileEmails(contactType={contactType})"/>
<cf_param name="showcaptcha" label="Show Captcha" type="boolean" default="true"/>

<cfparam name="activeTab" default="1" type="numeric">

<!--- Get details of the screens for the tab headings --->
<cfquery name="getScreenData" datasource="#application.SiteDataSource#">
	SELECT 	ScreenName, ScreenTextID
	FROM 	Screens
	WHERE 	screentextid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#includedScreen#" list="true">)
</cfquery>

<!--- Function to return the name of a given screen --->
<cffunction name="fnctGetScreenName" returntype="string" access="public">
	<!--- One required argument --->
	<cfargument name="screenTextId" type="any" required="true">

  	<!--- Get screen name for this tab --->
  	<cfquery name="getScreenName" dbtype="query">
		SELECT	ScreenName
		FROM 	getScreenData
		WHERE	ScreenTextID = '#arguments.screenTextId#'
	</cfquery>

	<cfreturn getScreenName.ScreenName />
</cffunction>

<cf_head>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?language=EN"></script>
</cf_head>

<cf_includeCssOnce template="/code/styles/social.css">
<cf_includeJavascriptOnce template="/locator/js/locator.js" translate="true">

<cfset hqLocation=application.com.flag.getFlagData(flagID=hqLocationFlagID,entityID=frmOrganisationID)>
<cfset orgApprovedDate=application.com.flag.getFlagData(flagID=OrgApprovalFlagID,entityID=frmOrganisationID)>

<cfquery name="getOrgDetails" datasource="#application.siteDataSource#">
	SELECT 	*
	FROM 	Organisation
	WHERE 	OrganisationID = <cfqueryparam cfsqltype="cf_sql_integer" value="#frmOrganisationID#">
</cfquery>

<!--- 2013-11-19 - YMA - 437561 - Added getLocDetails so Location Screens can be added --->
<cfset getLocDetails = application.com.relayPLO.getLocDetails(LocationID=FRMLOCATIONID)>

<cfquery name="getUserCountryID" datasource="#application.siteDataSource#">
	SELECT 	countryID
	FROM 	country
	WHERE 	isocode = <cfqueryparam cfsqltype="cf_sql_varchar" value="#right(session.relaycurrentuserstructure.localeinfo.locale,2)#">
</cfquery>

<cfset countryID = getUserCountryID.recordcount eq 1?getUserCountryID.countryID[1]:request.relaycurrentuser.content.showforcountryid>
<cfset phoneMask = application.com.commonQueries.getCountry(countryid=countryID).telephoneformat>

<cfif structKeyExists(hqLocation,"data") and isNumeric("hqLocation.data")>
	<cfset hqFormattedAddress = application.com.relayPLO.formatAddress(locationID=hqLocation.data)>
	<cfset hqLocationDetails=application.com.relayPLO.getLocDetails(locationID=hqLocation.data)>
</cfif>

<cfif left(getOrgDetails.OrgURL,4) eq 'http'>
	<cfset OrgURLWithProtocal=getOrgDetails.OrgURL>
	<cfset getOrgDetails.OrgURL=right('getOrgDetails.OrgURL',len('getOrgDetails.OrgURL')-(findNoCase('://','getOrgDetails.OrgURL')+2))>
<cfelse>
	<cfset OrgURLWithProtocal='http://' & getOrgDetails.OrgURL>
</cfif>

<!--- Process the contact form, if necessary --->
<cfif structKeyExists(form,"save") and (not isDefined("failedCaptcha") or (isDefined("failedCaptcha") and not failedCaptcha))>
		<cfoutput>
			<cfif request.relaycurrentuser.isloggedin eq 0>
				<cfparam name="frmDebug" type="boolean" default="no">

				<cfif structKeyExists(caller,"message")>
					<cfset caller.message = message>
				</cfif>

				<cfif isDefined("attributes") and structKeyExists(attributes,"debug")>
					<cfoutput>#message#</cfoutput>
				</cfif>
			<cfelse>
				<cfset thisOrgID = request.relaycurrentuser.organisationID>
			</cfif>

			<!--- NJH 2016/07/19 JIRA PROD-599 - deal with primary contact at location level as well - work out the entityType --->
			<cfset primaryContactFlagType = application.com.flag.getFlagStructure(flagId=PrimaryContactFlag).entityType.tablename>
			<cfset primaryContacts = application.com.relayPLO.getPrimaryContactsForEntity(entityID=primaryContactFlagType eq "location"?frmLocationId:frmOrganisationId,PrimaryContactFlag=PrimaryContactFlag)>
			<cfset leadEmailRecipientList = primaryContacts>

			<cfif primaryContacts eq "">
				<cfquery name="firstPersonInOrg" datasource="#application.siteDataSource#">
					SELECT 	TOP 1 personID as partnerSalesPersonID
					FROM 	person
					WHERE 	organisationID = <cf_queryparam value="#form.frmOrganisationID#" cfsqltype="cf_sql_integer">
					ORDER BY created
				</cfquery>

				<cfset leadEmailRecipientList = firstPersonInOrg.partnerSalesPersonID>
			</cfif>

			<!--- if only one primary contact then just assign them as the partner --->
			<cfif listLen(leadEmailRecipientList) eq 1>
				<cfset form.partnerSalesPersonID = leadEmailRecipientList>
			</cfif>

			<cfset form.contactPersonID = isdefined("thisPersonID")?thisPersonID:request.relaycurrentuser.personID>

			<cfset aboutPersonID = form.contactPersonID>

			<cfif contactType eq "newLead">
				<!--- NJH 2014/07/21 Store the leads in the leads table --->
				<cfset form.progressID = application.lookupID.leadProgressID.OpenNotContacted>
				<cfset form.leadTypeID = application.lookUpID.leadTypeID.Lead>
				<cfset form.sourceID = application.lookUpID.leadSourceID.Locator>

				<!---BF-229 RJT If this is a logged in user we don't ask them their details in the form but insertEntity expects those details for the lead' --->
				<cfif request.relaycurrentuser.isloggedin>
					<cfset form.Salutation = request.relaycurrentuser.person.salutation>
					<cfset form.firstName = request.relaycurrentuser.person.firstname>
					<cfset form.lastName = request.relaycurrentuser.person.lastName>
					<cfset form.company = request.relaycurrentuser.location.sitename>
					<cfset form.address4 = request.relaycurrentuser.location.address4>
					<cfset form.postalCode = request.relaycurrentuser.location.postalCode>
					<cfset form.email = request.relaycurrentuser.person.email>
					<cfset form.officePhone = request.relaycurrentuser.person.officePhone>
				</cfif>

				<cfset result = application.com.relayEntity.insertEntity(entityDetails=form,entityTypeID=application.entityTypeID.lead)>
				<cfset mergeStruct = {leadID=result.entityID}>
				<cfset aboutPersonID = form.partnerSalesPersonID>
				<cfset resultMessage = result.message>
			</cfif>

			<cfif (contactType eq "email") or (contactType eq "newLead" and result.isok)>
				<!---2016-07-04 case 449822 Locator Contact process not working: Instead of picking up the details entered into the form, they are picking up the details of the person to whom the email goes.--->
				<cfif contactType eq "email"> <!--- no lead created. Just send form fields as part of merge fields --->
					<cfif leadEmail EQ "LeadAccountManagerNotification">
						<cfset mergeStruct.lead.description = form.description>
						<cfset mergeStruct.lead.partner.organisationName = form.company>
						<cfset mergeStruct.lead.partner.fullname = form.firstname &" "& form.lastname>
					<cfelse>
						<cfset mergeStruct.enquiry = form.Detail>
						<cfset mergeStruct.description = form.description>
					</cfif>
				</cfif>

				<!--- NJH 2016/07/27 JIRA PROD2016-351 - dealing with multiple primary contacts --->
				<cfloop list="#leadEmailRecipientList#" index="recipientPersonID">
					<cfset application.com.email.sendEmail(recipientAlternativePersonID=recipientPersonID,personID=aboutPersonID,emailTextID=leadEmail,mergeStruct=mergeStruct)>
				</cfloop>
				<cfset resultMessage="phr_orgProfile_successmessage">
			</cfif>

			<cfsavecontent variable="message">
				<!--- Output the message --->
				<h3>phr_ThankYou</h3>
				#application.com.relayUI.message(message=resultMessage)#
			</cfsavecontent>
			<!--- Reload the contact screen --->
			<cfset activeTab = listlen(includedScreen,",")+1>
		</cfoutput>

<cfelseif isdefined("frmsendmessage")>
	<cfquery name="getcountry" datasource="#application.siteDataSource#">
		SELECT 	LocalCountryDescription
		FROM 	country
		WHERE 	countryID = <cf_queryparam value="#locatorcountryID#" cfsqltype="cf_sql_integer" >
	</cfquery>

	<cfset merge = structnew()>
	<cfset merge.LocatorFirstName = LocatorFirstName>
	<cfset merge.LocatorLastName = LocatorLastName>
	<cfset merge.LocatorOrganisationName = LocatorOrganisationName>
	<cfset merge.LocatorContactEmail = LocatorContactEmail>
	<cfset merge.LocatorPhoneNumber = LocatorPhoneNumber>
	<cfset merge.locatoraddress1 = locatoraddress1>
	<cfset merge.locatoraddress2 = locatoraddress2>
	<cfset merge.locatorcity = locatoraddress4>
	<cfset merge.locatorpostalcode = locatorpostalcode>
	<cfif isdefined("locatorInterestproductName")>
		<cfset merge.locatorInterestproductName = locatorInterestproductName>
	</cfif>
	<cfset merge.Countryname = getcountry.LocalCountryDescription>
	<cfset merge.Comments = Comments>
	<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
	<cfif len(trim(aboutPersonID))>
		<cfif structKeyExists(form,"locatorEnquiry")>
			<cfset merge.Enquiry = form.locatorEnquiry>
		</cfif>
		<cfset merge.CurrentSituation = merge.Comments>
		<cfset application.com.email.sendEmail(personID=aboutPersonID,emailTextID=emailTextID,mergeStruct=merge)>
	<cfelse>
		<cfloop index="locindex" list="#locationlist#" delimiters=",">
			<!--- lead type lead --->
			<cfquery name="insertlocatorLead" datasource="#application.siteDataSource#">
				DECLARE	@leadTypeId int
				SELECT 	@leadTypeID = lookupID
				FROM 	lookupList
				WHERE 	fieldname = <cfqueryparam cfsqltype="cf_sql_varchar" value="leadTypeID">
				AND 	lookupTextID = <cfqueryparam cfsqltype="cf_sql_varchar" value="Lead">

				INSERT INTO Lead(leadTypeID,leadTypeStatusID,FirstName,LastName,Telephone,Address1,Address2,Address4,PostalCode,CountryID<cfif isdefined("locatorInterestproductName")>,custom1</cfif>,partnerLocationId,company,email)
				VALUES		(@leadTypeID,0,<cf_queryparam value="#LocatorFirstName#" cfsqltype="cf_sql_varchar" >,<cf_queryparam value="#LocatorLastName#" cfsqltype="cf_sql_varchar" >,<cf_queryparam value="#LocatorPhoneNumber#" cfsqltype="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatoraddress1#" cfsqltype="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatoraddress2#" cfsqltype="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatoraddress4#" cfsqltype="cf_sql_varchar" >,<cf_queryparam value="#locatorpostalcode#" cfsqltype="cf_sql_varchar" >,'#locatorcountryID#'<cfif isdefined("locatorInterestproductName")>,'#locatorInterestproductName#'</cfif>,<cf_queryparam value="#locindex#" cfsqltype="cf_sql_varchar" >,<cf_queryparam value="#LocatorOrganisationName#" cfsqltype="cf_sql_varchar" >,<cf_queryparam value="#LocatorContactEmail#" cfsqltype="cf_sql_varchar">)
			</cfquery>
			<cfset LocatorPartnerContact = application.com.flag.getFlagData(flagid="isLocatorContact",entityid=locindex)>

			<cfif LocatorPartnerContact.recordcount EQ 1>
				<cfset merge.PartnerName = LocatorPartnerContact.data_name>
				<cfset application.com.email.sendEmail(emailtextid = emailTextID,personid = variables.LocatorPartnerContact.data, mergeStruct = merge)>
			</cfif>
		</cfloop>
	</cfif>
	<!--- END 2014-01-17  AXA Case 438630 - wrapping old code in else and closing cfif DCC 2016-07-07 took out ui message display and just display translation--->
	<cfsavecontent variable="message">
		<h3>phr_ThankYou</h3>
		phr_locator_PartnerEmailSent
	</cfsavecontent>
	<!--- Reload the contact tab --->
	<cfset activeTab = listlen(includedScreen,",")+1>
</cfif>

<cf_includeOnce template="/templates/relayFormJavaScripts.cfm">
<cfoutput>
	<div id="orgProfileWrapper">
		<div id="orgProfileUpper" class="row">
			<div id="left" class="col-xs-4">
				<div id="organisationLogo"><img src='#request.currentsite.httpprotocol##request.currentsite.domain#/<cfif getOrgDetails.pictureURL eq "">/images/social/icon_no_photo_80x80.png<cfelse>#getOrgDetails.pictureURL#</cfif>'/></div>
				<cfif contactType neq 'noContact' and not structkeyexists(form,"newLead") and not structkeyexists(form,"frmsendmessage") and not structkeyexists(form,"save")>
					<div id="contactUsDiv">
						<form  method="POST" enctype="multipart/form-data" name="contactUs" id="contactUs">
						 	<cf_relayFormDisplay class="">
								<cf_encryptHiddenFields>
								<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmOrganisationID" currentvalue="#frmOrganisationID#">
								<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmLocationID" currentvalue="#frmLocationID#">
								<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="usecurrentuser" currentvalue="false">
								<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmShowLanguageID" currentvalue="#application.com.relayTranslations.getUsersPreferredLangFromBrowser()#">
								<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="insCountryID" currentvalue="#countryID#">
								<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="FRMNEXT" currentvalue="6">
								</cf_encryptHiddenFields>
								<CF_relayFormElement relayFormElementType="submit" fieldname="newLead" currentValue="phr_orgProfile_ContactUs" label="" align="center">
							</cf_relayFormDisplay>
						</form>
					</div><!--- End contactUs div --->
				</cfif>
			</div><!--- End left div --->


			<!--- 2013-08-05	YMA	Make sure lastupdated is not null if we are going to try and show it. --->
			<div id="right" class="col-xs-8 text-right">
				<div id="approvedDate">phr_orgProfile_PartnerSince <cfif structKeyExists(orgApprovedDate,"LASTUPDATED") and orgApprovedDate.lastupdated neq ''>#DatePart("yyyy",orgApprovedDate.lastupdated)#</cfif></div>
				<div id="organisationName">#getOrgDetails.OrganisationName#</div>
				<div id="organisationDetails">
				<cfif structKeyExists(hqLocation,"data") and isNumeric("hqLocation.data")>
					<div id="Headquarters">phr_orgProfile_Headquarters: <cfif structKeyExists(hqLocation,"data")>#hqFormattedAddress#</cfif></div>
					<div id="TelephoneContact">phr_orgProfile_TelephoneContact: <cfif structKeyExists(hqLocationDetails,"telephone")>#hqLocationDetails.telephone#</cfif></div>
				</cfif>
				<div id="organisationURL">phr_orgProfile_URL: <a href="#OrgURLWithProtocal#" class="noembedly" target="_blank">#getOrgDetails.OrgURL#</a></div>
				</div><!--- End organisationDetails div --->
			</div><!--- End right div --->
		</div><!--- End orgProfileUpper div --->

		<hr>

		<div id="orgProfileLower">
			<cfif not structkeyexists(form,"newLead") and not structkeyexists(variables,"message")>
				<!--- Output tab headings --->
				<ul class="nav nav-tabs" role="tablist" id="partnerLocatorTabs">
				  	<cfloop from="1" to="#listlen(variables.includedScreen,',')#" index="tab">
					  	<cfset tabClass = "">
					  	<cfset thisTab = listgetat(variables.includedScreen,tab,",")>
					  	<!--- Get tab heading --->
						<cfset tabTitle = fnctGetScreenName(thisTab)>
						<li id="partnerLocatorTabs#tab#"<cfif tab is activeTab> class="active"</cfif>><a href="##partnerLocatorTab#tab#" role="tab" data-toggle="tab">#tabTitle#</a></li>
					</cfloop>
				</ul>

				<cfif isDefined("includedScreen") and includedScreen neq "">
					<div class="tab-content">
						<cfparam name="tabNum" default="1">
						<cfloop list="#variables.includedScreen#" index="thisScreen" delimiters=",">
							<div class="tab-pane<cfif tabNum is activeTab> active</cfif>" id="partnerLocatorTab#tabNum#">
								<cfform action="" method="POST" enctype="multipart/form-data" name="display">
								<cf_relayFormDisplay class="">
								<cf_ascreen formName = "mainForm">
									<cf_ascreenitem screenid="#thisScreen#"
											method="view"
											organisation="#getOrgDetails#"
											location="#getLocDetails#"
											countryID="#countryID#"
											divLayout="stacked"
									>
								</cf_ascreen>
								</cf_relayFormDisplay>
								</cfform>
							</div><!--- End tab-pane div --->
							<cfset tabNum = incrementValue(tabNum)>
						</cfloop>
					</div><!--- End tab content div --->

					<cfif structkeyexists(variables,"getLocDetails") and len(getLocDetails.latitude)>
						<script type="text/javascript">
							viewMapDetail('#getLocDetails.latitude#','#getLocDetails.longitude#');
						</script>
					</cfif>
				</cfif>
			<cfelse>
				<!--- Contact form and feedback message  --->
				<cfif structkeyexists(variables,"message")>
					<!--- Contact form has been submitted, so output the message --->
					#message#
				<cfelse>
					<h2>phr_contact_us</h2>
					<!--- Display the appropriate form --->
					<cfif contactType eq "emailDontCreatePerson">

						<cf_includeJavascriptOnce template="javascript/verifyEmail.js">

						<form method="POST" enctype="multipart/form-data" name="contactUs" id="contactUs">
					 	<cf_relayFormDisplay class="screenTable">

						<cfset locationlist = frmlocationid>
						<!--- START: 2014-01-17  AXA Case 438630 - passing params to Locator --->
						<cfset primaryContactFlagType = application.com.flag.getflagstructure(flagID=PrimaryContactFlag).entitytype.tablename>
						<cfset primaryContacts =  application.com.flag.getFlagData(flagId=PrimaryContactFlag, entityID=primaryContactFlagType eq "organisation"?frmOrganisationId:frmLocationId)>

						<cfif primaryContacts.recordcount eq 0>
							<cfquery name="firstPersonInOrg" datasource="#application.siteDataSource#">
								SELECT 	TOP 1 personID as partnerSalesPersonID
								FROM 	person
								WHERE 	organisationID = <cf_queryparam value="#frmOrganisationID#" cfsqltype="cf_sql_integer">
								ORDER BY created
							</cfquery>
							<cfset form.partnerSalesPersonID = firstPersonInOrg.partnerSalesPersonID>
						<cfelseif primaryContacts.recordcount eq 1>
							<cfset form.partnerSalesPersonID = primaryContacts.data[1]>
						</cfif>
						<cfif structKeyExists(form,"partnerSalesPersonID")>
							<cfset aboutPersonID = form.partnerSalesPersonID>
						</cfif>
						<cfset emailTextID = leadEmail />
						<!--- END: 2014-01-17  AXA Case 438630 - passing params to Locator --->

						<!--- Locator --->
						<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag --->
						<cfparam name="aboutPersonID" default="" />
						<cfparam name="emailTextID" default="LocatorPartnerLead" />
						<cfparam name="Enquiry" default="">
						<!--- END 2014-01-17  AXA		Case 438630 - passing params from relaytag --->
						<cfset qGetCountries=application.com.commonQueries.getCountries()>
						<cfif structkeyexists(url,"UseProducts") and url.UseProducts eq 1 and structkeyexists(url,"countryid")>
							<cfset GetInterestProducts=application.com.relaylocator.GetProductsOfInterest(countryid=url.countryid)>
						</cfif>

						<cfset defaultCountryID = request.relaycurrentuser.isunknownuser?request.relaycurrentuser.possiblecountryidforunknownuser:request.relaycurrentuser.countryid>
						<div><b>phr_LocatorReferral_ContactFormInstruction</b></div>

						<CF_relayFormElementDisplay relayFormElementType="select" currentvalue="" id="" fieldName="Salutation" label="phr_orgProfile_Salutation" validvalues="person.salutation" required="true">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="" fieldName="LocatorFirstName" label="phr_Sys_FirstName" required="Yes">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="" fieldName="LocatorLastName" label="phr_Sys_LastName" required="Yes">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="OrganisationName" fieldName="LocatorOrganisationName" label="phr_Sys_Organisation">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="contactemail" fieldName="LocatorContactEmail" label="phr_Sys_Email" required="yes">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="phonenumber" fieldName="LocatorPhoneNumber" label="phr_Sys_Telephone" required="yes" mask="#phonemask#">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="invaddress1" fieldName="locatoraddress1" label="phr_Sys_Address" >
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="locatoraddress2" fieldName="locatoraddress2" label="">
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="locatoraddress4" fieldName="locatoraddress4" label="phr_sys_cityRegion" >
						<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="locatorpostalcode" fieldName="locatorpostalcode" label="phr_Sys_PostalCode" required="yes">
						<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#defaultCountryID#" id="locatorcountryID" fieldName="locatorcountryID" label="phr_Sys_Country" query="#qGetCountries#" display="country" value="countryid" required="yes">
						<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
						<cfif len(trim(Enquiry)) GT 0>
							<CF_relayFormElementDisplay relayFormElementType="select" id="locatorEnquiry" fieldName="locatorEnquiry" label="phr_orgProfile_Enquiry" currentvalue="" validvalues="#Enquiry#">
						</cfif>
						<!--- END 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
						<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="comments" currentValue="" id="locatorcomments" label="phr_Sys_Comments" maxLength="500">
						<cfif structkeyexists(url,"UseProducts") and url.UseProducts eq 1>
							<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="locatorInterestproductName" id="InterestproductName" label="phr_sys_nav_Products" query="#GetInterestProducts#" display="InterestProductName" value="InterestProductName" multiple="yes" size="5" required="yes">
						</cfif>
						<cf_encryptHiddenFields>
							<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#locationlist#" fieldname="locationlist">
							<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
							<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#aboutPersonID#" fieldname="aboutPersonID">
							<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#emailTextID#" fieldname="emailTextID">
							<!--- END 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
						</cf_encryptHiddenFields>
						<!--- Submit button --->
						<cf_relayFormElement relayFormElementType="submit" fieldname="frmsendmessage" label="" currentValue="phr_sys_Submit" valuealign="left" spancols="true">

						<script>
							function FormVerify(){
								var nForm=document.EnterdetailsForm;
								var msg = "";
								if (isEmailAddressValid(nForm.LocatorContactEmail.value) == false){
									msg += "Please enter a valid Email.\r\n";
								}
								if (msg != ""){
									alert(msg);
									return false;
								}else{
									nForm.submit();
								}
							}
						</script>
						</cf_relayFormDisplay>
						</form>

					<cfelse>

						<cfif showcaptcha and not request.relaycurrentuser.isloggedin>
							<cfset submitfunction = "return validateCaptcha();">
							<script language="javascript">
								function validateCaptcha() {
									if ("<cfoutput>#jsStringFormat(showCaptcha)#</cfoutput>" == "false") {
										return true;
									} else {
										userResponse=document.contact.frmcaptcha.value;
										captchaResponse=document.contact.frmcaptcha_hash.value;
										var timeNow = new Date();
										page = '/WebServices/relaycaptchaWS.cfc?wsdl';
										captchaParameters  = 'returnFormat=JSON&method=getcaptchavalidation&time='+timeNow+ '&userResponse='+userResponse+'&captchaResponse='+captchaResponse;
										var myAjax = new Ajax.Request(
											page,
											{
												method: 'get',
												asynchronous: false,
												parameters: captchaParameters,
												evalJSON: 'force',
												debug : false
											}
										)
										var captchaResponse = myAjax.transport.responseText.evalJSON();

										if (captchaResponse) {
											return true;
										} else {
											var timeNow = new Date();
											page = '/webservices/relaycaptchaWS.cfc?wsdl';
											updatercaptchaParameters  = '&method=drawCaptcha&captchaerror=true&time='+timeNow;
											var div = 'captchadiv';
											var myAjax = new Ajax.Updater(
												div,
												page,
												{
													method: 'get',
													parameters: updatercaptchaParameters,
													evalJSON: 'force',
													debug : false,
													onComplete: function () {document.contact.frmcaptcha.focus();}
												});
											return false;
										}
									}
								}
							</script>
						<cfelse>
							<cfset submitfunction = "">
							<cfset showcaptcha = false>
						</cfif>
					<cfoutput>
						<form onsubmit="#submitfunction#" method="POST" name="contact" id="contact">

							<div><b>phr_LocatorReferral_ContactFormInstruction</b></div>
						 	<cf_relayFormDisplay class="">
								<cfif not request.relaycurrentuser.isloggedin>
									<CF_relayFormElementDisplay relayFormElementType="select" currentvalue="" fieldName="Salutation" label="phr_orgProfile_Salutation" validvalues="person.salutation" required="true">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="firstName" label="phr_orgProfile_FirstName" maxlength="30" size="50" required="true">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="lastName" label="phr_orgProfile_LastName" maxlength="30" size="50" required="true">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="company" label="Phr_orgProfile_Company" maxlength="50" size="50" required="true">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="address4" label="Phr_orgProfile_City" maxlength="50" size="50" required="true">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="postalCode" label="Phr_orgProfile_PostalCode" maxlength="50" size="50" required="true">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="email" label="Phr_orgProfile_Email" maxlength="50" size="50" required="true" validate="email" onValidate="validateEmail">
									<cfif application.com.relayPLO.isUniqueEmailValidationOn().isOn>
										<CF_relayFormElementDisplay relayFormElementType="html" label="" currentvalue="phr_sys_formValidation_UniqueEmailOnScreenLabel" spancols="true">
									</cfif>
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="officePhone" label="Phr_orgProfile_Phone" maxlength="30" size="50" required="true" mask="#phonemask#" abort="true">
								</cfif>
								<CF_relayFormElementDisplay relayFormElementType="select" currentvalue="" fieldName="Detail" label="phr_orgProfile_Enquiry" validvalues="#Enquiry#">
								<CF_relayFormElementDisplay relayFormElementType="textarea" currentValue="" fieldName="description" label="Phr_orgProfile_MyEnquiry" maxlength="2000" rows="8" size="50" required="true">

								<cfif showCaptcha>
									<CF_relayFormElementDisplay relayFormElementType="html" label="Phr_Ext_Captcha_instructions" required="true">
									<!--- <div class="col-xs-12 col-sm-9 col-sm-offset-3"><p>Phr_Ext_Captcha_instructions</p></div> --->
										<div id="captchadiv">#application.com.commonQueries.drawCaptcha()#</div>
									</CF_relayFormElementDisplay>
						 		</cfif>

								<!--- 2014-11-25	RPW	CORE-97 Phase 1: Extend database fields for Leads - Set the countryID to be that of the partner if it is selected --->
								<cfscript>
									if (IsNumeric(frmLocationID)) {
										variables.qGetLocatorCountries = application.com.relayPLO.GetLocationOrg(locationID=frmLocationID);
										countryID = variables.qGetLocatorCountries.countryID[1];
									}
								</cfscript>
								<cf_encryptHiddenFields>
									<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="partnerlocationID" currentvalue="#frmLocationID#">
									<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmOrganisationID" currentvalue="#frmOrganisationID#">
									<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="countryID" currentvalue="#countryID#">
									<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmShowLanguageID" currentvalue="#application.com.relayTranslations.getUsersPreferredLangFromBrowser()#">
								</cf_encryptHiddenFields>
								<CF_relayFormElementDisplay relayFormElementType="submit" label="" fieldname="save" currentValue="phr_orgProfile_submit" align="center">
							</cf_relayFormDisplay>
						</form>
					</cfoutput>
				</cfif>
			</cfif>
		</cfif>
		</div><!--- End orgProfileLower div --->
	</div><!--- End orgProfileWrapper div --->
</cfoutput>
