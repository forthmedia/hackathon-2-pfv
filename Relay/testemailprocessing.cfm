<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			testEmailProcessing.cfm	
Author:				SWJ
Date started:		2007-03-24
	
Description:		This is a simple file designed to test whether emails are being sent on the targetted box.
					It is called from fnl-Europe/devTools and by defualt sends to relayHelp

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cf_head>
	<cf_title>Email Testing</cf_title>
</cf_head>
<cfset sendTo = "relayHelp@foundation-network.com">
<cfparam name="URL.sendToaddress" default="">
<cfset sendTo = listAppend(sendTo,URL.sendToAddress,",")>

<cfmail to="#sendTo#" 
	from="emailTester@foundation-network.com" 
	subject="Test Email from '#CGI.server_name#'">
This email has been sent by a testing procedure from #CGI.server_name#.

It was most likely invoked by someone using http://fnl-europe/devdocs/ and click on the Test email processing to run this test again.

The users IP address was #CGI.REMOTE_ADDR#
</cfmail> 

<cfoutput>
Email sent to #htmleditformat(sendTo)# from #htmleditformat(CGI.server_name)#<br><br>
Your IP address is #htmleditformat(CGI.REMOTE_ADDR)#
</cfoutput>




