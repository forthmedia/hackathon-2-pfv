<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Default Layout" extends="layout">
	
	<cffunction name="head" access="public" output="yes">
		<cfargument name="title" type="string" required="yes">
	<cfcontent reset="yes">
	
	<cf_head>
		<cf_title>#htmleditformat(arguments.title)#</cf_title>
		<!--- <link rel="stylesheet" type="text/css" href="all.css" />
		<script type="text/javascript" src="all.js"></script> --->
	</cffunction>
	
	<cffunction name="body" access="public" output="yes">
	</cf_head>
	
	
	</cffunction>
	<cffunction name="topNavBar" access="public" output="yes">
		<cfargument name="NavBarTitle" type="string" default="">
		<cfargument name="showBackLink" type="boolean" default="false">
		<cfargument name="showSaveLink" type="boolean" default="false">
		<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
			<TR class="Submenu">
				<cfif len(arguments.NavBarTitle) gt 0>
					<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">#htmleditformat(arguments.NavBarTitle)#</TD>
				</cfif>
				<TD ALIGN="right" VALIGN="TOP" class="Submenu">
					<cfif arguments.showBackLink>
						<a href="javascript:history.back()" class="Submenu">Back</a>   
					</cfif>
					<cfif arguments.showSaveLink>
						 | <a href="javascript:document.frmDocMaint.submit()" class="Submenu">Save</a>
					</cfif>
				</TD>
			</TR>
		</TABLE>
	
	</cffunction>
	<cffunction name="pageTitle" access="public" output="yes">
		<cfargument name="titleText" type="string" default="">
		<cfif len(arguments.titleText) gt 0>
			<h1>#htmleditformat(arguments.titleText)#</h1>
		</cfif>
	</cffunction>
	<cffunction name="endBody" access="public" output="yes">
		<cfargument name="hideFooter" type="boolean" default="false">
		<cfif not hideFooter>
			<cfinclude template="/templates/InternalFooter.cfm">
		</cfif>

		
		
	</cffunction>
</cfcomponent>
