<cfsetting enablecfoutputonly="yes">
<cfprocessingdirective suppresswhitespace="true">
<cfsilent>
<!---
Description:
============
	Request Token Endpoint

License:
============
Copyright 2008 CONTENS Software GmbH

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--->
</cfsilent>


<cfset application.com.request.doNotAddHTMLDocTags(true)>
<cfsetting showdebugoutput="false">
<cfinclude template="common.cfm">


<!--- create dao --->
<cfset oReqDataStore = CreateObject("component", "oauth.oauthtest").init(sDataSource, true)>

<!--- create server --->
<cfset oReqServer = CreateObject("component", "oauth.oauthserver").init(oReqDataStore)>

<!--- cfdump var="request_token END POINT oReqServer" --->
<!--- cfdump var="#oReqServer#" --->

<!--- register signature methods --->
<cfset oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
<cfset oReqSigMethodPLAIN = CreateObject("component", "oauth.oauthsignaturemethod_plaintext")>
<cfset oReqServer.addSignatureMethod(oReqSigMethodSHA)>
<cfset oReqServer.addSignatureMethod(oReqSigMethodPLAIN)>

<!--- analyze request --->
<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromRequest()>
<!--- cfdump var="#oReq.getParameters()#" label="#CGI.SCRIPT_NAME#" --->

<!--- cfdump var="request_token END POINT oReq" --->
<!--- cfdump var="#oReq#" --->

<!--- retrieve request token --->
<cfset oReqToken = oReqServer.fetchRequestToken(oReq)>
<!--- cfdump var="#oReqToken#" --->
<cfoutput>#oReqToken.getString()#</cfoutput>
<cfset rmbHeaders = GetHttpRequestData().headers>

   <cfif StructKeyExists(rmbHeaders, "Authorization") AND
  Left(StructFind(rmbHeaders, "Authorization"), 5) EQ "OAuth">


	<cfset rmbSplit = CreateObject("component", "oauthrequest").splitHeader(rmbHeaders.Authorization)>

		<cfif structKeyExists(rmbSplit , "oauth_callback")>
			<!--- cfdump var = "#rmbSplit.oauth_callback#" --->

			<cfset saveTokenID = listlast(listfirst(oReqToken.getString(),"&"),"=")>
			<!--- cfdump var = "#saveTokenID#" --->

			<cfquery datasource="#application.siteDataSource#" name="saveCallbackURL">
			UPDATE oauth_tokens
			SET
				callbackURL = <cf_queryparam value="#rmbSplit.oauth_callback#" cfsqltype="CF_SQL_VARCHAR">
			WHERE
				tkey = <cf_queryparam value="#saveTokenID#" cfsqltype="CF_SQL_VARCHAR">
			</cfquery>

		<cfelse>
			<cfabort>
		</cfif>

	</cfif>
</cfprocessingdirective>