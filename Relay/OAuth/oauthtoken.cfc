<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Description:
============
	OAuth token

License:
============
Copyright 2008 CONTENS Software GmbH

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Modifications

2013/02/20	NJH		part of spring 4 item 13 - pass through entityID and entityTypeID if they are defined as it may not always be the current user (ie. twitter account)
--->

<cfcomponent displayname="oauthtoken">
	<cfset variables.sKey = "">
	<cfset variables.sSecret = "">
	<cfset variables.oUtil = CreateObject("component","oauthutil").init()>

	<cffunction name="init" access="public" returntype="oauthtoken">
		<cfargument name="sKey" required="true" type="string">
		<cfargument name="sSecret" required="true" type="string">

		<cfset setKey(arguments.sKey)>
		<cfset setSecret(arguments.sSecret)>

		<cfreturn this>
	</cffunction>

	<cffunction name="getKey" access="public" returntype="string" output="false">
		<cfreturn variables.sKey>
	</cffunction>
	<cffunction name="setKey" access="public" returntype="void">
		<cfargument name="sKey" type="string" required="yes">
		<cfset variables.sKey = arguments.sKey>
	</cffunction>

	<cffunction name="getSecret" access="public" returntype="string" output="false">
		<cfreturn variables.sSecret>
	</cffunction>
	<cffunction name="setSecret" access="public" returntype="void">
		<cfargument name="sSecret" type="string" required="yes">
		<cfset variables.sSecret = arguments.sSecret>
	</cffunction>

	<cffunction name="createEmptyToken" access="public" returntype="oauthtoken">
		<cfset var oEmptyToken = init(sKey="", sSecret="")>
		<cfreturn oEmptyToken>
	</cffunction>

	<cffunction name="isEmpty" access="public" returntype="boolean">
		<cfset var bResult = false>
		<cfif Len(getSecret()) IS 0 AND Len(getKey()) IS 0>
			<cfset bResult = true>
		</cfif>
		<cfreturn bResult>
	</cffunction>

	<!---
		generates the basic string serialization of a token that a server
		would respond to request_token and access_token calls with
	--->
	<cffunction name="getString" access="public" returntype="string" output="false">
		<cfset var sResult = "oauth_token=" & variables.oUtil.encodePercent(variables.sKey) & "&" &
			"oauth_token_secret=" & variables.oUtil.encodePercent(variables.sSecret)>
		<cfreturn sResult>
	</cffunction>
	
	
	<!--- NJH 2012/03/12 - all functions below here were written by myself to support Social Strand 1 --->
	
	<!--- Returns the access token as it is stored in the db 
		NJH 2013/02/20 Item 13 - now accept entityId and entityTypeID as arguments
	--->
	<cffunction name="getDBAccessToken" access="public" returnType="oauthtoken" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" default="0">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">
		<cfargument name="decryptToken" type="boolean" default="false">
		
		<cfset var getAccessTokenQry = "">
		<cfset var token = init(sKey="", sSecret="")>
		<cfset var accessKey = "">
		<cfset var accessSecret = "">
		
		<cfif (arguments.entityID eq 0 and arguments.entityTypeID.person) and structKeyExists(request,"relayCurrentUser") and request.relayCurrentUser.isLoggedIn>
			<cfset arguments.entityID = request.relayCurrentUser.personID>
		</cfif>
		
		<cfquery name="getAccessTokenQry" datasource="#application.siteDataSource#">
			select accessKey,accessSecret from serviceToken where entityTypeID = #arguments.entityTypeID# and entityID=#arguments.entityID# and serviceID =  <cf_queryparam value="#application.com.service.getService(arguments.serviceID).serviceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfif getAccessTokenQry.recordCount>
			<cfif arguments.decryptToken>
				<cfset accessKey = application.com.encryption.decryptString(encryptType="standard",string=getAccessTokenQry.accessKey[1])>
				<cfif getAccessTokenQry.accessSecret[1] neq "">
					<cfset accessSecret = application.com.encryption.decryptString(encryptType="standard",string=getAccessTokenQry.accessSecret[1])>
				</cfif>
			<cfelse>
				<cfset accessKey = getAccessTokenQry.accessKey[1]>
				<cfset accessSecret = getAccessTokenQry.accessSecret[1]>
			</cfif>
			<cfset token.setKey(accessKey)>
			<cfset token.setSecret(accessSecret)>
		</cfif>

		<cfreturn token>
	</cffunction>
	
	
	<!--- Returns a user's access token, whether this is stored in session, in the db or whether it needs to be retrieved from the oauth provider 
		NJH 2013/02/20 Item 13 - now accept entityId and entityTypeID as arguments
	--->
	<cffunction name="getAccessToken" access="public" returnType="oauthToken" output="true">
		<cfargument name="requestTokenKey" type="string" default="">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" required="false">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">
		<cfargument name="encryptToken" type="boolean" default="true">
		
		<cfset var token = init(sKey="", sSecret="")>
		<cfset var service = application.com.service.getService(serviceID=arguments.serviceID)>
		<cfset var oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
		<cfset var oauthRequestToken = init(sKey="", sSecret="")>
		<cfset var oConsumer = CreateObject("component", "oauth.oauthconsumer").init(sKey = service.consumerKey, sSecret = service.consumerSecret)>
		<cfset var oReq = "">
		<cfset var parameters = structNew()>
		<cfset var getForEntityID = 0>
		<cfset var accessTokenURL = "https://api.linkedin.com/uas/oauth/accessToken">
		<cfset var accessTokenMethod = "url">
		
		<!--- if a user is not logged in, and we've had to get an access token because we need to authenticate them, then it will be stored in session scope. As soon as the user logs in and their account is linked,
			this should be deleted. It should have been encrypted before it was set in session scope --->
		<cfif structKeyExists(session,"accessToken") and structKeyExists(session,"relayCurrentUserStructure") and not session.relayCurrentUserStructure.isInternal>
			<cfif not arguments.encryptToken>
				<cfset token.setKey(application.com.encryption.decryptString(encryptType="standard",string=session.accessToken.getKey()))>
				<cfif session.accessToken.getSecret() neq "">
					<cfset token.setSecret(application.com.encryption.decryptString(encryptType="standard",string=session.accessToken.getSecret()))>
				</cfif>
			<cfelse>
				<cfset token = session.accessToken>
			</cfif>
		<cfelse>
			<cfif structKeyExists(arguments,"entityID")>
				<cfset getForEntityID = arguments.entityID>
		 	<cfelseif structKeyExists(request,"relayCurrentUser") and request.relayCurrentUser.isLoggedIn>
			 	<cfset getForEntityID = request.relayCurrentUser.personID>
			 	<cfset arguments.entityTypeID = application.entityTypeID.person>
			</cfif>
			
			<cfif getForEntityID neq 0 and arguments.requestTokenKey eq "">
				<cfset token = getDBAccessToken(serviceID=arguments.serviceID,entityID=getForEntityID,entityTypeID=arguments.entityTypeID,decryptToken=not(arguments.encryptToken))>
			</cfif>
		</cfif>
		
		<cfif token.isEmpty()>
			<cfif arguments.requestTokenKey neq "">
				<cfset oauthRequestToken = CreateObject("component", "oauth.oauthtoken").init(sKey=arguments.requestTokenKey,sSecret=IIF(structKeyExists(session,"requestTokenSecret"),evaluate(de('session.requestTokenSecret')),de('')))>
			<cfelse>
				<!--- I don't think we want to be down here, as a request token requires a redirect to linkedin --->
				<cfset oauthRequestToken = getRequestToken()>
			</cfif>
			
			<cfif not oauthRequestToken.isEmpty() and structKeyExists(url,"oauth_verifier")>
			
				<cfif serviceID eq "twitter">
					<cfset accessTokenURL = "https://api.twitter.com/oauth/access_token">
					<cfset accessTokenMethod = "post">
				</cfif>
			
				<cfset parameters.oauth_verifier = url.oauth_verifier>
				<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
					oConsumer = oConsumer,
					oToken = oauthRequestToken,
					sHttpMethod = "get",
					sHttpURL = accessTokenURL,
					stparameters= parameters )>
				<cfset oReq.signRequest(
					oSignatureMethod = oReqSigMethodSHA,
					oConsumer = oConsumer,
					oToken = oauthRequestToken)>
				
				<cfif oReq.getHttpMethod() eq "get">
					<cfhttp url="#oREQ.getString()#" method="get" result="tokenResponse"/>
				<cfelse>
					<cfhttp url="#oREQ.getHttpURL()#" method="#oREQ.getHttpMethod()#" result="tokenResponse">
						<cfhttpparam type="header" name="Authorization" value="#oREQ.toHeader()#">
					</cfhttp>
				</cfif>	
				
				<cfif tokenResponse.statusCode neq "200 OK" and application.testSite>
					<!--- <Cfdump var="#oauthRequestToken.getKey()#">
					<cfdump var="#oREQ.getString()#">
					<cfdump var="#tokenResponse#">
					<CF_ABORT> --->
				</cfif>
				
				<cfif findNoCase("oauth_token",tokenresponse.filecontent)>
					<cfset accessKey = listlast(listfirst(tokenResponse.filecontent,"&"),"=")>
					<cfset accessSecret = listlast(listgetat(tokenResponse.filecontent,2,"&"),"=")>
					<cfif arguments.encryptToken>
						<cfset accessKey = application.com.encryption.encryptString(encryptType="standard",string=accessKey)>
						<cfset accessSecret = application.com.encryption.encryptString(encryptType="standard",string=accessSecret)>
					</cfif>
					<cfset token.setKey(accessKey)>
					<cfset token.setSecret(accessSecret)>
				</cfif>
			</cfif>
		</cfif>
		
		<cfreturn token>
		
	</cffunction>
	
	
	<!--- sets the user's access token in the database 
		NJH 2013/02/20 Item 13 - now accept entityId and entityTypeID as arguments
	--->
	<cffunction name="setAccessToken" access="public" output="false">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="accessToken" type="oauthtoken" required="true">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">
		
		<cfset var getServiceID = "">
		<cfset var assignAccessToken = "">
		<cfset var thisServiceID = application.com.service.getService(arguments.serviceID).serviceID>
		
		<!--- TODO: could maybe do a try/catch to determine if the access token has indeed been encrypted --->
		
		<!--- This assumes that the access token has already been encrypted! --->
		<cfquery name="assignAccessToken" datasource="#application.siteDataSource#">
			if not exists (select 1 from serviceToken where entityTypeID=#arguments.entityTypeID# and entityID=#arguments.entityID# and serviceID =  <cf_queryparam value="#thisServiceID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			insert into serviceToken (serviceID,entityID,entityTypeID,accessKey,accessSecret,createdBy,lastUpdatedBy)
				values
			(#thisServiceID#,#arguments.entityID#,#arguments.entityTypeID#,'#arguments.accessToken.getKey()#','#arguments.accessToken.getSecret()#',#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#)
			
			else
			
			update serviceToken
				set accessKey =  <cf_queryparam value="#arguments.accessToken.getKey()#" CFSQLTYPE="CF_SQL_VARCHAR" > ,accessSecret =  <cf_queryparam value="#arguments.accessToken.getSecret()#" CFSQLTYPE="CF_SQL_VARCHAR" >,lastUpdated=getDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID#
			where entityID=#arguments.entityID# and entityTypeID=#arguments.entityTypeID# and serviceID =  <cf_queryparam value="#thisServiceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
	</cffunction>
	
	
	<!--- get's a users request token.. builds up the request with parameters such as the callback page and parameters --->
	<cffunction name="getRequestToken" access="public" returnType="oauthToken" output="true">
		<cfargument name="callbackParams" type="struct" default="#structNew()#">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="callbackURL" type="string" default="#request.currentSite.protocolAndDomain#/social/callback.cfm">
		
		<cfset var oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
		<cfset var token = CreateObject("component", "oauth.oauthtoken").createEmptyToken()>
		<cfset var service = application.com.service.getService(arguments.serviceID)>
		<cfset var consumer = CreateObject("component", "oauth.oauthconsumer").init(sKey=service.consumerKey, sSecret=service.consumerSecret)>
		<cfset var parameters = structNew()>
		
		<cfif service.consumerKey neq "" and service.consumerSecret neq "">
			<cfset arguments.callbackURL = arguments.callbackURL & IIF(find("?",arguments.callbackURL),DE("&"),DE("?")) & application.com.structureFunctions.convertStructureToNameValuePairs(struct=arguments.callbackParams,delimiter="&")>
		
			<cfset parameters.oauth_callback=arguments.callbackURL>
		
			<cfif arguments.serviceID eq "linkedIN">
				<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
					oConsumer = consumer,
					oToken = token,
					sHttpMethod = "get",
					sHttpURL = "https://api.linkedin.com/uas/oauth/requestToken",stparameters=parameters)>
					
			<cfelseif arguments.serviceID eq "twitter">
				
				<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
					oConsumer = consumer,
					oToken = token,
					sHttpMethod = "post",
					sHttpURL = "https://api.twitter.com/oauth/request_token",stparameters=parameters)>
			</cfif>
			
			<cfset oReq.signRequest(
				oSignatureMethod = oReqSigMethodSHA,
				oConsumer = consumer,
				oToken = token)>

			<cfif oReq.getHttpMethod() eq "get">
				<cfhttp url="#oREQ.getString()#" method="#oREQ.getHttpMethod()#" result="tokenResponse"/>
			<cfelse>
				<cfhttp url="#oREQ.getHttpURL()#" method="#oREQ.getHttpMethod()#" result="tokenResponse">
					<cfhttpparam type="header" name="Authorization" value="#oREQ.toHeader()#">
					<cfhttpparam type="header" name="Content-type" value="text/xml; charset=UTF-8">
				</cfhttp>
			</cfif>

			<cfif findNoCase("oauth_token",tokenresponse.filecontent)>
				<cfset setKey(listlast(listfirst(tokenResponse.filecontent,"&"),"="))>
				<cfset requestTokenSecret = listlast(listgetat(tokenResponse.filecontent,2,"&"),"=")>
				<cfset setSecret(requestTokenSecret)>
				<!--- set the request token secret in session scope so that we can access it easily when making calls to get the access token. --->
				<cfset session.requestTokenSecret = requestTokenSecret>
			</cfif>
		</cfif>
		
		<cfreturn this>
	</cffunction>


</cfcomponent>