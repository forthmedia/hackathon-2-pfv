<cfsetting enablecfoutputonly="yes">
<cfsilent>
<!---
Description:
============
	User Authorization Endpoint

License:
============
Copyright 2008 CONTENS Software GmbH

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2014-11-17	WAB	CF10 - replace calls to java encyption library with single call to hmac()
2015-01-30 DCC CASE 443311 - client using extra ? in callback urls NOTE can only be tested directly on production

TODO: Take out Russ's custom code from (pembridge production and FTF) core compare with any other client
--->
</cfsilent>

<cfset application.com.request.doNotAddHTMLDocTags(true)>
<cfsetting showdebugoutput="false">
<cfinclude template="common.cfm">

<cfif StructKeyExists(url, "oauth_token")>
	<cfset form.oauth_token = url.oauth_token>
</cfif>

<cfif request.relayCurrentUser.isLoggedIn>
	<cfset form.rwOAuthLogin = "Login">
</cfif>

<cfif StructKeyExists(form, "rwOAuthLogin")>

	<cfif not request.relayCurrentUser.isLoggedIn>

		<cfset authenticateUser = application.com.login.authenticateUserV2(
					username = form.rwOAuthUserName,
					password = form.rwOAuthPassword,
					showComplexityWidget = false
				)>

		<cfset UsePersonID = authenticateUser.USERQUERY.PERSONID>

	<cfelse>

		<cfset authenticateUser = structNew()>
		<cfset authenticateUser.ISOK = true>

		<cfset UsePersonID = request.relayCurrentUser.personid>

	</cfif>

	<cfif authenticateUser.ISOK>

	<cftry>

		<cfset encoder = CreateObject("component", "oauth.oauthutil").init()>

		<cfquery datasource="#application.siteDataSource#" name="getCallbackURL">
		SELECT *
		FROM oauth_tokens
		WHERE tkey = <cf_queryparam value="#form.oauth_token#" cfsqltype="CF_SQL_VARCHAR">
		</cfquery>

		<cfquery name="qLookUpConsumer" datasource="#application.siteDataSource#" maxrows="1">
		SELECT
			*
		FROM	oauth_consumers
		WHERE	consumer_id = <cf_queryparam value="#getCallbackURL.consumer_id#" cfsqltype="CF_SQL_VARCHAR">
		</cfquery>

		<cfquery datasource="#application.siteDataSource#" name="saveCallbackURL">
		UPDATE
			oauth_tokens
		SET
			PersonID = <cf_queryparam value="#UsePersonID#" cfsqltype="CF_SQL_INTEGER">
		WHERE
			tkey = <cf_queryparam value="#form.oauth_token#" cfsqltype="CF_SQL_VARCHAR">
		</cfquery>

		<cfscript>
		oauth_verifier = URLEncodedFormat(computeHMACSignature(form.oauth_token, qLookUpConsumer.ckey));

		function computeHMACSignature(data, key) {    
			return  hmac( data, key, "HmacSHA1" );
		}
		</cfscript>

		<cfset authorize_url_extension = "">
		<cf_include template="/code/cftemplates/oAuth_authorize_extension.cfm" checkifexists="true">

		<cfif ListLen(getCallbackURL.callbackURL, "?") EQ 0>
			<cfset useGoBack = "#getCallbackURL.callbackURL#?oauth_token=#form.oauth_token#&oauth_verifier=#oauth_verifier##authorize_url_extension#">
		<cfelse>
			<cfset AddIn = "oauth_token=#form.oauth_token#&oauth_verifier=#oauth_verifier##authorize_url_extension#">

			<cfset outerCnt = 0>
			<cfset extaParamsURLEncoded = "">
			<cfset tempKey = ArrayNew(1)>
			<cfset tempValue = ArrayNew(1)>

			<!--- DCC 2015-01-30 CASE 443311 changed to listrest <cfloop list="#ListLast(getCallbackURL.callbackURL, '?')#" index="i" delimiters="&">--->			
			<cfloop list="#ListRest(getCallbackURL.callbackURL, '?')#" index="i" delimiters="&">
				<cfset ArrayAppend(tempKey, ListFirst(i, '='))>
				<cfif ListLen(i , '=') GT 1>
					<!--- DCC 2015-01-30 CASE 443311 changed to listrest <cfset ArrayAppend(tempValue, encoder.encodePercent(ListLast(i, '=')))> --->
					<cfset ArrayAppend(tempValue, encoder.encodePercent(ListRest(i, '=')))>
				<cfelse>
					<cfset ArrayAppend(tempValue, '')>
				</cfif>
			</cfloop>



			<cfloop from="1" to="#ArrayLen(tempKey)#" index="outerCnt">
			<cfoutput><cfsavecontent variable="extaParamsURLEncoded">#extaParamsURLEncoded#&#tempKey[outerCnt]#=#tempValue[outerCnt]#</cfsavecontent></cfoutput>
			</cfloop>

			<cfset useGoBack = "#ListFirst(getCallbackURL.callbackURL, '?')#?#AddIn##extaParamsURLEncoded#">
		</cfif>

		<cf_include template="/code/cftemplates/oAuth_authorize_extension_parttwo.cfm" checkifexists="true">

		<cflocation url="#useGoBack#" addToken="false">
	
		<cfcatch type="any">
		<cfoutput>phr_OAuthTokenInvalid</cfoutput>
		</cfcatch>
	
	</cftry>

	</cfif>

</cfif>

<!--- $todo: to implement  $todo --->
<cfoutput>

	<cfif not request.relayCurrentUser.isLoggedIn>

	<cfif fileexists("#application.paths.CODE#\styles\OAuthLoginStyle.css")>
		<CFHTMLHEAD TEXT='<LINK REL="stylesheet" HREF="/code/styles/OAuthLoginStyle.css">'>
	</cfif>

	<cf_include template="/code/cftemplates/oAuth_login_header.cfm" checkifexists="true">


	<!---Header Start--->







	<!---Header End--->
<cfoutput>
				<div class="loginBox">
				<form action="" method="post" name="loginform" id="loginForm">
					<div id="LoginTable" style="width: 100%;">
<cfif StructKeyExists(form, "rwOAuthLogin")>

<cfif NOT authenticateUser.ISOK>

						<div>
							<div colspan="2" style="text-align: left;" >
								<cfoutput>#authenticateUser.MESSAGE#</cfoutput>
							</div>
						</div>

</cfif>

</cfif>

						<div>
							<div id="rwOAuthDivUsernameText">
								<p id="rwOAuthUsernameText">phr_LoginScreen_Username:</p>
							</div>
								<div>
									<input id="rwOAuthUserName" name="rwOAuthUserName" class="text" type="text" <cfif IsDefined("Cookie.User")> value="#request.relayCurrentUser.person.Email#"</cfif> />
								</div>
						</div>

						<div>
							<div id="rwOAuthDivPasswordText">
								<p id="rwOAuthPasswordText">phr_LoginScreen_Password:</p>
							</div>
								<div>
									<input id="rwOAuthPassword" name="rwOAuthPassword" class="text" type="password" />
								</div>
						</div>
						
						<div style="width:50%; float:left; padding-top:10px;">

                            <input id="setRememberMeID" class="checkbox" type="checkbox" value="1" name="setRememberMe"></input>
                            <acronym title="Remember Me">Remember Me</acronym>
                            <input type="hidden" value="true" name="setRememberMe_Exists"></input>

</div>

						<div style="width:50%; float:left;"> 
							<div>
								<input id="rwOAuthLogin" name="rwOAuthLogin" style="margin:5px;" class="submit" type="submit" value="phr_Login" />
							</div>
						</div>
					</div>
					<input type="hidden" name="oauth_token" value="#form.oauth_token#">
				</form>
				</div>
</cfoutput>

	<cf_include template="/code/cftemplates/oAuth_login_footer.cfm" checkifexists="true">

	</cfif>

</cfoutput>
