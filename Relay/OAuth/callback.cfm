<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Example to connect to LinkedIn
---------------------------------------------------------------------------------
1) Assumes there is a CF mapping of oauth pointing to the oauth folder.
2) add your consumerkey and secret
3) save and run
--->
<!--- 
<!--- set up the parameters --->
<cfset sConsumerKey = "8nr3k07nydxd"> <!--- the consumer key you got from LinkedIn when registering you app  --->
<cfset sConsumerSecret = "Z4TeS1Gpq6OdTgym"> <!--- the consumer secret you got from LinkedIn --->
<cfset sTokenEndpoint = "https://api.linkedin.com/uas/oauth/requestToken"> <!--- Request Token URL --->
<cfset sAuthorizationEndpoint = "https://www.linkedin.com/uas/oauth/authenticate"> <!--- Authorize URL --->
<cfset sCallbackURL = "http://dev2-relayware83int/oauth/callback.cfm"> <!--- where LinkedIn will redirect to after the user enters their details --->
<cfset sClientToken = ""> <!--- returned after an access token call --->
<cfset sClientTokenSecret = ""> <!--- returned after an access token call ---> --->

<!--- <cfset public_profile_url = urlencodedformat("http://uk.linkedin.com/pub/gawain-claridge/1/5b8/484")>

<cfset urlToCall = "http://api.linkedin.com/v1/people/url=" & public_profile_url> --->

<cfset urlToCall = "http://api.linkedin.com/v1/people/%7E:(id,first-name,last-name,industry)">

<cfset urlToCall = "http://api.linkedin.com/v1/people/id=Tm9Ju1g8G-:(first-name,last-name)">
<cfset urlToCall = "http://api.linkedin.com/v1/people/id=Tm9Ju1g8G-/connections">

<!--- Tm9Ju1g8G-    (gawain's ID.') --->
<cfset urlToCall = "http://api.linkedin.com/v1/groups/Tm9Ju1g8G-:(id,name,site-group-url,posts)">

<cfset urlToCall = "https://api.linkedin.com/v1/people/%7E/group-memberships:(group:(id,name),membership-state)"> <!---  --->
<cfset urlToCall = "http://api.linkedin.com/v1/people-search">  <!--- add url variables (query params) as url (or oauthrequest) parameters--->
<cfset urlToCall = "http://api.linkedin.com/v1/people-search:(people:(id,first-name,last-name,picture-url,headline),num-results)">
<cfset urlToCall = "http://api.linkedin.com/v1/companies">
<cfset urlToCall = "http://api.linkedin.com/v1/companies/758947:(id,name,locations,description,website-url)">
<cfset urlToCall = "http://api.linkedin.com/v1/people/%7E/shares">
<Cfset urlToCall = "http://api.linkedin.com/v1/people/%7E/network">
<cfset urlToCall = "http://api.linkedin.com/v1/people/%7E">
<cfset urlToCall = "http://api.linkedin.com/v1/people/%7E/connections:(headline,first-name,last-name,id)">

<!--- set up the required objects including signature method--->
<cfset oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
<cfset oToken = CreateObject("component", "oauth.oauthtoken").init(sKey=session.sAccessToken,sSecret=session.sAccessTokenSecret)>
<cfset oConsumer = CreateObject("component", "oauth.oauthconsumer").init(sKey = sConsumerKey, sSecret = sConsumerSecret)>

<cfset Parameters = structNew()>

<cfset Parameters["first-name"]="david"> <!--- people search --->
<cfset Parameters["last-name"]="Bensel"> <!--- people search --->
<cfset Parameters["email-domain"]="relayware.com"> <!--- companies --->
<cfset Parameters["type"] = "SHAR"> <!--- shares --->

<cfsavecontent variable="xmlContent">
	<cfoutput>
		<?xml version="1.0" encoding="UTF-8"?>
		<share>
		  <comment>Test</comment>
		  <content>
			 <title>Survey: Social networks top hiring tool - San Francisco Business Times</title>
			 <submitted-url>http://sanfrancisco.bizjournals.com/sanfrancisco/stories/2010/06/28/daily34.html</submitted-url>
			 <submitted-image-url>http://images.bizjournals.com/travel/cityscapes/thumbs/sm_sanfrancisco.jpg</submitted-image-url>
		  </content>
		  <visibility>
			 <code>connections-only</code>
		  </visibility>
		</share>
	</cfoutput>
</cfsavecontent>

<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
	oConsumer = oConsumer,
	oToken = oToken,
	sHttpMethod = "GET",
	sHttpURL = urlToCall,stparameters= Parameters )>
<cfset oReq.signRequest(
	oSignatureMethod = oReqSigMethodSHA,
	oConsumer = oConsumer,
	oToken = oToken)>

<cfdump var="#oReq.getString()#">

<br><br>
<cfdump var="#sAccessTokenSecret#"><br>
<cfdump var="#sAccessToken#"><br>

<cfhttp url="#oREQ.getString()#" method="get" result="tokenResponse"/>
<cfdump var="#tokenResponse#">

 
<cfdump var="#oREQ.toHeader()#"><br><br>

<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
	oConsumer = oConsumer,
	oToken = oToken,
	sHttpMethod = "POST",
	sHttpURL = urlToCall,stparameters= Parameters )>
<cfset oReq.signRequest(
	oSignatureMethod = oReqSigMethodSHA,
	oConsumer = oConsumer,
	oToken = oToken)>

<cfhttp url="#urlToCall#" method="post" result="tokenResponse">
	<cfhttpparam type="header" name="Authorization" value="#oREQ.toHeader()#">
	<cfloop from="1" to="#ArrayLen(oREQ.getParameters().paramKeys)#" index="i">
		<cfhttpparam name="#oREQ.getParameters().paramKeys[i]#" value="#oREQ.getParameters().paramValues[i]#" type="formfield">
	</cfloop>
</cfhttp>

<cfdump var="#tokenResponse#">
<cfabort>

<!--- 

<cfscript>
tc = CreateObject("java", "java.util.Date").getTime();
timestamp = Int(tc / 1000);
nano = createObject('java' ,"java.lang.System"); 
session.nonce = nano.nanoTime();    
baseURI = "http://api.linkedin.com/v1/people/%7E";
signmethod = "HMAC-SHA1";   
version = "1.0";
paramsStr = "oauth_consumer_key=" & sConsumerKey & "&"& "oauth_nonce=" & session.nonce & "&"& "oauth_signature_method=" & signmethod & "&"& "oauth_timestamp=" & timestamp & "&"& "oauth_token=" & url.oauth_token & "&"& "oauth_version=" & version;
signStr = "GET&" & replace(encodeData(baseURI), "%7E", "~" ) & "&" & encodeData(paramsStr);
signature = computeHMACSignature(signStr, encodeData(sConsumerSecret) & "&" & encodeData(session.sClientTokenSecret));
authHeader = "OAuth " &createHeaderElement("oauth_consumer_key", sConsumerKey) & ", " &createHeaderElement("oauth_nonce", session.nonce) & ", " & "oauth_signature_method=""HMAC-SHA1""" & ", " & createHeaderElement("oauth_timestamp", timestamp) & ", " &"oauth_token=""" & url.oauth_token & """, " &"oauth_version=""" & version & """, " & createHeaderElement("oauth_signature", signature);

function encodeData(data) { 
	result = ""; 
	data = JavaCast("string", data);    
	encoder = createObject('java', 'java.net.URLEncoder');    
	result = encoder.encode(data, 'UTF-8');    
	return result;
}    

function computeHMACSignature(data, key) {    
	var result = "";    
	signingKey = createObject('java', 'javax.crypto.spec.SecretKeySpec').Init(key.getBytes(), 'HmacSHA1');    
	mac = createObject('java', "javax.crypto.Mac");       
	mac = mac.getInstance('HmacSHA1');    
	mac.init(signingKey);    
	rawHmac = mac.doFinal(data.getBytes('UTF-8'));    
	result = toBase64(rawHmac);    
	return result;
}
	
function createHeaderElement(name, value) {    
	return replaceNoCase(encodeData(name),"%7E", "~", "all") & "=" & """" & replaceNoCase(encodeData(value),"%7E", "~", "all") & """";
}
</cfscript>

<cfset urlString = "#urlToCall#?#paramsStr#&oauth_signature=#signature#">
<cfhttp url="#urlString#" method="get" result="xx">
	<cfhttpparam name="Authorization" value="#authHeader#" type="header"/>
</cfhttp>

<cfoutput>#urlString#</cfoutput>
<cfdump var="#xx#">
<cfdump var="#authHeader#"> --->