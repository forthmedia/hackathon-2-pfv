<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Example to connect to LinkedIn
---------------------------------------------------------------------------------
1) Assumes there is a CF mapping of oauth pointing to the oauth folder.
2) add your consumerkey and secret
3) save and run
--->

<Cfparam name="form.commethod" default="get">
<!--- set up the parameters --->
<cfset sConsumerKey = "8nr3k07nydxd"> <!--- the consumer key you got from LinkedIn when registering you app  --->
<cfset sConsumerSecret = "Z4TeS1Gpq6OdTgym"> <!--- the consumer secret you got from LinkedIn --->
<cfset sTokenEndpoint = "https://api.linkedin.com/uas/oauth/requestToken"> <!--- Request Token URL --->
<cfset sAccessTokenEndpoint = "https://api.linkedin.com/uas/oauth/accessToken"> <!--- Access Token URL --->
<cfset sAuthorizationEndpoint = "https://api.linkedin.com/uas/oauth/authorize"> <!--- Authorize URL --->
<cfset sCallbackURL = "http://dev2-relayware83int/oauth/linkedIn.cfm"> <!--- where LinkedIn will redirect to after the user enters their details --->

<cfset sRequestToken = ""> <!--- returned after an access token call --->
<cfset sRequestTokenSecret = ""> <!--- returned after an access token call --->
<cfset sAccessToken = ""> <!--- returned after an access token call --->
<cfset sAccessTokenSecret = ""> <!--- returned after an access token call --->
<cfset sAuthURL = "">

<cfif isdefined("form.requesttoken") and form.consumerkey neq "">
	<cfset sConsumerKey = form.consumerkey> <!--- the consumer key you got from twitter when registering you app  --->
	<cfset sConsumerSecret = form.consumersecret> <!--- the consumer secret you got from twitter --->
	<cfset oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
	<cfset oToken = CreateObject("component", "oauth.oauthtoken").createEmptyToken()>
	<cfset oConsumer = CreateObject("component", "oauth.oauthconsumer").init(sKey = sConsumerKey, sSecret = sConsumerSecret)>

	<cfset Parameters = structNew()>

	<cfset Parameters.oauth_callback=sCallbackURL>

	<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
		oConsumer = oConsumer,
		oToken = oToken,
		sHttpMethod = form.commethod,
		sHttpURL = sTokenEndpoint,stparameters= Parameters)>
	<cfset oReq.signRequest(
		oSignatureMethod = oReqSigMethodSHA,
		oConsumer = oConsumer,
		oToken = oToken)>

	<cfif form.commethod eq "get">
		<cfhttp url="#oREQ.getString()#" method="get" result="tokenResponse"/>
	<cfelse>
		<cfhttp url="#sTokenEndpoint#" method="post" result="tokenResponse">
			<cfhttpparam type="header" name="Authorization" value="#oREQ.toHeader()#">
		</cfhttp>
	</cfif>

	<cfif findNoCase("oauth_token",tokenresponse.filecontent)>
		<cfset sRequestToken = listlast(listfirst(tokenResponse.filecontent,"&"),"=")>
		<cfset sRequestTokenSecret = listlast(listgetat(tokenResponse.filecontent,2,"&"),"=")>
	<cfelse>
	 <cfdump var="#tokenresponse.filecontent#">
	 <cfabort>
	</cfif>
	<cfdump var="#tokenresponse#">
	
	<!--- <Cfset x = decrypt(sRequestToken,sRequestTokenSecret,"HmacSHA1")>
	<cfdump var="#x#">
	 <cfdump var="#tokenresponse.filecontent#">
	 <cfabort> --->
</cfif>

<cfif isDefined("form.authorize") and form.requesttoken neq "">
	<cfset sConsumerKey = form.consumerkey> <!--- the consumer key you got from twitter when registering you app  --->
	<cfset sConsumerSecret = form.consumersecret> <!--- the consumer secret you got from twitter --->
	<cfset sRequestToken = form.requesttoken> <!--- returned after an access token call --->
	<cfset sRequestTokenSecret = form.requesttokensecret> <!--- returned after an access token call --->
	<cfset session.requesttokensecret = sRequestTokenSecret>
	<!--- <cfset sCallbackURL = sCallbackURL & "?" &
		"key=" & sConsumerKey &
		"&" & "secret=" & sConsumerSecret &
		"&" & "token=" & sRequestToken &
		"&" & "token_secret=" & sRequestTokenSecret &
		"&" & "endpoint=" & URLEncodedFormat(sAuthorizationEndpoint)> --->

	<cfset sAuthURL = sAuthorizationEndpoint & "?oauth_token=" & sRequestToken <!--- & "&" & "oauth_callback=" & URLEncodedFormat(sCallbackURL)  --->>
	<cflocation url="#sAuthURL#">
</cfif>
<cfif isdefined("url.oauth_token") <!--- and form.oauthpin neq "" --->>

	<!--- <cfset sConsumerKey = form.consumerkey> <!--- the consumer key you got from twitter when registering you app  --->
	<cfset sConsumerSecret = form.consumersecret> <!--- the consumer secret you got from twitter ---> --->
	<cfset sRequestToken = url.oauth_token> <!--- returned after an access token call --->
	<cfset sRequestTokenSecret = session.requesttokensecret> <!--- returned after an access token call --->
	<cfset oReqSigMethodSHA = CreateObject("component", "oauth.oauthsignaturemethod_hmac_sha1")>
	<cfset oToken = CreateObject("component", "oauth.oauthtoken").init(sKey= sRequestToken,sSecret=sRequestTokenSecret)>
	<cfset oConsumer = CreateObject("component", "oauth.oauthconsumer").init(sKey = sConsumerKey, sSecret = sConsumerSecret)>

	<cfset Parameters = structNew()>
	<cfset parameters.oauth_verifier = url.oauth_verifier>
	<cfset oReq = CreateObject("component", "oauth.oauthrequest").fromConsumerAndToken(
		oConsumer = oConsumer,
		oToken = oToken,
		sHttpMethod = "get",
		sHttpURL = sAccessTokenEndpoint,
		stparameters= Parameters )>
	<cfset oReq.signRequest(
		oSignatureMethod = oReqSigMethodSHA,
		oConsumer = oConsumer,
		oToken = oToken)>
	
	<cfhttp url="#oREQ.getString()#" method="get" result="tokenResponse"/>
		<!--- <cfhttpparam type="header" name="Authorization" value="#oREQ.toHeader()#">
	</cfhttp> --->
	
	<!--- seem to have an expiry of 0.... --->
	<cfif findNoCase("oauth_token",tokenresponse.filecontent)>
		<cfset sAccessToken = listlast(listfirst(tokenResponse.filecontent,"&"),"=")>
		<cfset sAccessTokenSecret = listlast(listgetAt(tokenResponse.filecontent,2,"&"),"=")>
	</cfif>
	<Cfdump var="#tokenresponse.filecontent#">
	
</cfif>

<!--- <cfset session.sAccessToken = "a28a6529-5ba1-4ad4-b899-9652aa054565">
<cfset session.sAccessTokenSecret = "4407fcea-c709-48ec-9b28-f91857940635"> --->

<cfif sAccessToken neq "">
	<cfinclude template="callback.cfm">
	<Cfabort>
</cfif>

<cfoutput>
	
<html>
<head>
	<title>Twitter oAuth Test Harness</title>

<script>
	var enableAccessTokenButton = function enableAccessTokenButton(t){
		if(t.value.length > 0){
			document.getElementById('accessToken').disabled=false;
		}else{
			document.getElementById('accessToken').disabled=true;			
		}
	}
	
	function openWin(url){
		window.open(url,'mywindow','width=400,height=200');
	}
	
	<cfif len(sAuthUrl)>
		openWin("#sAuthUrl#");
	</cfif>
</script>
</head>
<body>
<div>
<h3>oAuth LinkedIn Test Harness</h3>
</div>
<div>
<form name="oauthInput" method="post">
	<table>
	<tr>
		<td><strong>Token Endpoint</strong>:</td>
		<td colspan="2">#sTokenEndpoint#</td>
	</tr>
	<tr>
		<td><strong>Authorization Endpoint</strong>:</td>	
		<td colspan="2">#sAuthorizationEndpoint#</td>	
	</tr>
	<tr>
		<td><strong>Access Token Endpoint</strong>:</td>	
		<td colspan="2">#sAccessTokenEndpoint#</td>	
	</tr>
	<tr>
		<td><strong>Method</strong>:</td>	
		<td colspan="2"><select name="commethod">
		<option value="get" <cfif form.commethod eq "get">selected="selected"</cfif>>Get</option>
		<option value="post" <cfif form.commethod neq "get">selected="selected"</cfif>>Post</option>
		</select></td>	
	</tr>
	<tr>
		<td>Consumer Key:</td>	
		<td colspan="2"><input type=text" name="ConsumerKey" style="width:300px" value="#sConsumerKey#"></td>	
	</tr>
	<tr>
		<td>Consumer Secret:</td>	
		<td><input type=text" name="Consumersecret" style="width:300px" value="#sConsumerSecret#"></td>
		<td><input type="submit" name="requesttoken" value="Request Token"></td>	
	</tr>
	<tr>
		<td>Request Token:</td>	
		<td colspan="2"><input type="text" name="requestToken" style="width:300px" value="#sRequestToken#"></td>
	</tr>
	<tr>
		<td>Request Token Secret:</td>	
		<td><input type="text" name="requestTokensecret" value="#sRequestTokenSecret#"  style="width:300px" ></td>
		<td><input type="submit" name="authorize" value="Authorize" <cfif #sRequestTokenSecret# eq "">disabled=true</cfif> ></td>
	</tr>
	<!--- <tr>
		<td>oAuth Pin</td>	
		<td><input type="text" name="oauthpin" value="" style="width:300px" onKeyUp="enableAccessTokenButton(this)"></td>	
		<td><input type="submit" id="accessToken" name="accesstoken" value="Access Token" disabled=true></td>
	</tr> --->
	<tr>
		<td>Access Token:</td>	
		<td colspan="2"><input type="text" name="AccessToken" value="#sAccessToken#" style="width:300px" ></td>
	</tr>
	<tr>
		<td>Access Token Secret:</td>	
		<td colspan="2"><input type="text" name="AccessTokenSecret" value="#sAccessTokenSecret#" style="width:300px" ></td>
	</tr>
</table>
</form>
</div>
</cfoutput>
</body>
</html>
