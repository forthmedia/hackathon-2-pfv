<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			connectorMapping.cfm
Author:				NJH
Date started:		2014/09/25

Description:		List and edit connector mappings.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2015/06/18			NJH			Added ability to delete mappings

Possible enhancements:
 --->

<!--- Required parameters --->
<cfparam name="url.objectID" type="numeric"> <!--- the connectorObject ID --->
<cfparam name="url.mappingId" type="numeric" default="0"> <!--- the connectorMapping ID --->
<cfparam name="url.ID" type="numeric" default="#url.mappingID#"> <!--- the connectorMapping ID --->

<cfif structKeyExists(form,"ID")>
	<cfset url.mappingID = form.ID>
	<cfset url.ID = form.ID>
</cfif>

<cfset objectFieldsQry = queryNew("object")>
<cfset xmlSource = "">
<cfset getObjectDetails = application.getObject("connector.com.connector").getConnectorObjects(ID=url.objectID)>
<cfset connectorType = getObjectDetails.connectorType[1]>
<cfset isRemoteObject = not getObjectDetails.relayware[1]>
<cfset addMapping = true>

<cfparam name="sortOrder" type="string" default="#isRemoteObject?'remoteField':'relaywareField'#">

<cfif structKeyExists(url,"editor")>

	<cfset relaywareObjectID = not isRemoteObject?url.objectID:0>
	<cfset remoteObjectID = isRemoteObject?url.objectID:0>
	<cfset relaywareObject = not isRemoteObject?getObjectDetails.object[1]:"">
	<cfset remoteObject = isRemoteObject?getObjectDetails.object[1]:"">

	<!--- Get details of the available mappings --->
	<cfset getConnectorMappings = application.getObject("connector.com.connectorMapping").getConnectorMappings(connectorType=connectorType,objectID=url.objectId,connectorMappingID=url.mappingID)>

	<cfif getConnectorMappings.recordCount>
		<cfset isMappedObject = true>
		<cfset relaywareObjectID = getConnectorMappings.connectorObjectID_relayware[1]>
		<cfset remoteObjectID = getConnectorMappings.connectorObjectID_remote[1]>
		<cfset relaywareObject = getConnectorMappings.object_relayware[1]>
		<cfset remoteObject = getConnectorMappings.object_remote[1]>
	<cfelse>
		<cfset isMappedObject = application.getObject("connector.com.connector").isObjectMapped(object_relayware=not relaywareObjectID?getObjectDetails.object[1]:"",object_remote=not remoteObjectID?getObjectDetails.object[1]:"",connectorType=connectorType)>
	</cfif>

	<cf_includeCssOnce template="/connector/css/connector.css">

	<cf_head>
		<cfoutput>
			<script>
				jQuery(document).ready( function() {onConnectorMappingPageLoaded();});
			</script>
		</cfoutput>
	</cf_head>

	<cfset canEdit = not url.mappingID or getConnectorMappings.canEdit[1]>
	<cfset excludeMappedObjects = not isMappedObject?true:false>

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="connectorMapping" title="#application.com.regexp.camelCaseToSpaces(string=connectorType)# Connector: Edit #application.com.regexp.camelCaseToSpaces(string=getObjectDetails.object[1])# Field Mapping" readOnly="#not canEdit#">
				<field name="connectorObjectId_remote" readonly="[[#isMappedObject?true:false# or connectorMapping.id neq 0?true:false or #isRemoteObject?true:false#?true:false]]" query="func:getObject('connector.com.connector').getConnectorObjects(connectorType='#connectorType#',relayware=0,excludeMappedObjects=[[connectorMapping.id eq 0 and #excludeMappedObjects#?true:false]],ID=[[connectorMapping.connectorObjectId_remote eq ''?#remoteObjectID#:connectorMapping.connectorObjectId_remote]])" display="object" value="ID" <cfif url.mappingID eq 0>default="#remoteObjectID#"</cfif> onChange="getIDColumn();"></field>
				<field name="connectorObjectId_relayware" readonly="[[#isMappedObject?true:false# or connectorMapping.id neq 0?true:false or #not isRemoteObject?true:false#?true:false]]" query="func:getObject('connector.com.connector').getConnectorObjects(connectorType='#connectorType#',relayware=1,excludeMappedObjects=[[connectorMapping.id eq 0 and #excludeMappedObjects#?true:false]]<cfif isMappedObject>,ID=[[connectorMapping.connectorObjectId_relayware eq ''?#relaywareObjectID#:connectorMapping.connectorObjectId_relayware]]</cfif>)" display="object" value="ID" <cfif url.mappingID eq 0>default="#relaywareObjectID#"</cfif> onChange="getIDColumn();getColumnsThatMapToDataType();"></field>
				<field name="direction" query="select * from (values ('', 'Both'), ('I', 'Import'),('E', 'Export')) as q (value, display)" onChange="onChangeDirection();showDefaultValueWhenNull();" showNull="false"/>
				<field name="mappingType" displayAs="radio" query="select * from (values ('DefaultValue','DefaultValue'),('Field','Field')) as q (value, display)" onChange="onChangeMappingType();showDefaultValueWhenNull();"/>
				<field name="defaultValue"/>
				<field name="column_remote" disabled="#not isRemoteObject?true:false# or [[connectormapping.id eq 0?true:false]]?false:true" query="func:getObject('connector.com.connectorMapping').getRemoteObjectFields(connectorType='#connectorType#',object='[[connectorMapping.connectorObjectId_remote eq ''?#remoteObjectID#:connectorMapping.connectorObjectId_remote]]',column='[[connectorMapping.column_Remote]]')" display="fieldname" value="fieldNameValue" dataAttributes="dataType" onChange="hideOptionsWithIncompatibleDataTypes();showMappedValues();validateDefaultValues();" showNull="true" allowCurrentValue="true" noteText="phr_sys_connector_fieldMappingTooltip"/>
				<field name="column_relayware" disabled="#isRemoteObject?true:false# or [[connectormapping.id eq 0?true:false]]?false:true" query="func:getObject('connector.com.connectorMapping').getRelaywareObjectFields(connectorType='#connectorType#',object='[[connectorMapping.connectorObjectId_relayware eq ''?#relaywareObjectID#:connectorMapping.connectorObjectId_relayware]]',column='[[connectorMapping.column_Relayware]]')" display="fieldNameDisplay" value="fieldNameValue" dataAttributes="foreignKeyTable,dataType" onChange="hideOptionsWithIncompatibleDataTypes();showMappedValues();validateDefaultValues();" showNull="true" allowCurrentValue="true" noteText="phr_sys_connector_fieldMappingTooltip"/>
				<field name="mappedRelaywareColumn"/>
				<field name="" condition="request.relayCurrentUser.organisationId eq 1?true:false" control="includeTemplate" template="/connector/configuration/mappingIssuesReportForm.cfm" spanCols="true"/>
				<field name="defaultWhenNull" control="YorN" label="phr_connectorMapping_defaultWhenNull" onChange="showDefaultValueWhenNull();validateDefaultValues();"/>
                <field name="defaultImportValue" label="phr_connectorMapping_defaultImportValue" onChange="showDefaultValueWhenNull();validateDefaultValues();"/>
                <field name="defaultExportValue" label="phr_connectorMapping_defaultExportValue" onChange="showDefaultValueWhenNull();validateDefaultValues();"/>
				<field name="active"/>
				<field name="required"/>
				<field name="emptyStringAsNull" control="hidden" default="1"/>
				<field name="notes"/>
				<field name="connectorType" control="hidden" currentValue="#connectorType#"/>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>

	<!--- here we are testing to see whether we can add a field mapping. We can't if there are no objects to map to. Output message asking user to create another object that they can map to --->
	<cfset argStruct = {connectorType=connectorType}>
	<cfset argStruct[isRemoteObject?"object_remote":"object_relayware"] = getObjectDetails.object[1]>

	<cfif not application.getObject("connector.com.connector").isObjectMapped(argumentCollection=argStruct) and not application.getObject("connector.com.connector").getConnectorObjects(connectorType=connectorType,relayware=isRemoteObject,excludeMappedObjects=true).recordCount>
		<cfset application.com.relayUI.setMessage(message="There are currently no #not isRemoteObject?'import':'export'# objects to map to. Please configure a #not isRemoteObject?'import':'export'# object before adding field mappings.",type="info")>
		<cfset addMapping = false>
	</cfif>

	<!--- Get list of fields associted with this object --->
	<cfset objectFieldsQry = application.getObject("connector.com.connectorMapping").getConnectorMappingListing(connectorObjectID=url.objectID,sortOrder=sortOrder)>

</cfif>

<cfset application.com.request.setTopHead(topHeadCfm="/relay/connector/connectorTopHead.cfm",pageTitle="#application.com.regExp.camelCaseToSpaces(string=getObjectDetails.connectorType[1])# Connector: #application.com.regexp.camelCaseToSpaces(string=getObjectDetails.object[1])# Field Mappings",addMapping=addMapping)>

<cf_includeJavascriptOnce template="/connector/configuration/js/connectorMapping.js">
<cf_includeJavascriptOnce template="/javascript/lib/jquery/spinner.js">

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="connectorEditMapping"
	useInclude="false"
	queryData="#objectFieldsQry#"
	showTheseColumns="Object,RelaywareField,RemoteField,Direction,Active,lastUpdated,Edit,Delete"
	keyColumnList="Edit,Delete"
	keyColumnURLList="/connector/configuration/connectorMapping.cfm?objectID=#url.objectID#&editor=1&mappingID=, "
	keyColumnOnClickList=" ,deleteMapping(##ID##);return false;"
	keyColumnKeyList="ID,ID"
	dateTimeFormat="lastUpdated"
	booleanFormat="active"
	treatEmtpyFieldAsNull="true"
	columnTranslationPrefix="phr_connectorMapping_report_"
	columnTranslation="true"
	sortOrder="#sortOrder#"
	rowIdentityColumnName="ID"
	preSaveFunction=#application.getObject("connector.com.connectorMapping").preSaveConnectorMapping#
	postSaveFunction=#application.getObject("connector.com.connectorMapping").postSaveConnectorMapping#
>