<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			connectorObject.cfm
Author:				NJH
Date started:		2014/11/13

Description:		displays summary of mappings of a given connector object.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2015/11/30 SB Bootstrap
2015/11/09			NJH			testing of login credentials moved to API.cfc
2016/01/25			NJH			Jira BF-351 - removed sorting as there is not really enough data to sort.
Possible enhancements:
 --->

<cfparam name="url.ID" type="numeric" default="0"> <!--- the Id of the connectorObject; 0 if new --->
<cfparam name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">  <!---the connector type --->
<cfparam name="url.relayware" type="boolean" default="1"> <!--- whether we are dealing with a Relayware object or not --->

<cfif structKeyExists(url,"editor") and url.editor>

	<cf_includeJavascriptOnce template="/javascript/ui.js">
	<cf_includeJavascriptOnce template="/connector/configuration/js/connectorObject.js">
	<cf_includeJavascriptOnce template="/javascript/lib/jquery/spinner.js">

	<cfif url.id neq 0>
		<cfset qryConnectorObject = application.getObject("connector.com.connector").getConnectorObjects(connectorType=connectorType,Id=url.ID)>
		<cfset url.relayware = qryConnectorObject.relayware[1]>
	</cfif>

	<script>
		jQuery(document).ready(function() {onConnectorMappingPageLoad();});
	</script>

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="connectorObject" title="#connectorType# Connector Object">
				<field name="ID"/>
				<field name="object" query="func:getObject('connector.com.connectorObject').getConnectorObjects(connectorType=#connectorType#,relayware=#url.relayware#,ID=[[connectorObject.ID]], UI=true)" readonly="[[connectorObject.ID eq 0?false:true]]"/>
				<field name="whereClause"/>
            	<field name="whereClauseValidationStatus" label="phr_whereClauseValidationStatus" control="readonly"/>
				<field name="parentObjectID" query="func:getObject('connector.com.connectorObject').getParentConnectorObjects(connectorType=#connectorType#,relayware=#url.relayware#,object=[[connectorObject.object]])"/>
				<field name="parentKeyColumn"/>
				<field name="postProcessID" query="select processID as value,processDescription as display from processHeader order by processDescription"/>
				<field name="allowDeletions"/>
				<field name="notes"/>
				<field name="synch" helpText="#url.relayware?'Export':'Import'# this object?"/>
				<field name="active"/>
				<field name="relayware" control="hidden" defaultValue="#url.relayware#"/>
				<field name="connectorType" control="hidden" currentValue="#connectorType#"/>
				<group label="System Fields" name="systemFields">
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

	<cf_listAndEditor
		xmlSource="#xmlSource#"
		hideBackButton="true"
		treatEmtpyFieldAsNull="true"
		postSaveFunction="#application.getObject('connector.com.connectorObject').postSaveConnectorObject#"
	>

	<!--- refresh the listing page if a record has been added or updated --->
	<cfif structKeyExists(form,"update") or structKeyExists(form,"added")>
		<script>
			parent.objectChanged = true;
		</script>
	</cfif>

<cfelse>

	<!--- not changed at the moment, but the idea is that if an object has changed in some way, we reload the listing page --->
	<script>
		var objectChanged = false;
	</script>

	<cf_modalDialog size="large" type="iframe" identifier=".Submenu, td.edit a, td.fieldMappings a" hideOnContentClick="false" beforeClose="function() {if (objectChanged) {window.location.reload();}}">

	<!--- The connector dropdown is based on what settings are available. That way, you can start without any connectorObject records. --->
	<cfif structKeyExists(form,"connectorType")>
		<cfset connectorType = form.connectorType>
	</cfif>
	<cfset connectorApiObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorApiObject(connectorType=connectorType)>

	<cfset apiConfigured = true>

	<h2><cfoutput>#application.getObject("regExp").camelCaseToSpaces(string=ConnectorType)#</cfoutput> Connector Configuration Summary</h2>

	<!--- check that we are able to make api calls to the remote site, as the configuration screens are heavily dependent on the API. If we can't connect, then output a message and disabled the edit/list field mappings links--->
	<cfif not connectorApiObject.testConnectionToRemoteSite()>
		<cfset application.com.relayUI.setMessage(message="The API user must first be configured before mappings can be created and/or edited.",type="warning")>
		<cfset apiConfigured = false>
	</cfif>

	<cfset attributeStruct = {showTheseColumns=listAppend("object,active,synch,hasWhereClause,hasPostProcess,lastUpdated",apiConfigured?"edit,fieldMappings":""),useInclude=false,booleanFormat="active,synch,hasWhereClause,hasPostProcess",dateTimeFormat="lastUpdated",rowIdentityColumnName="ID",keyColumnKeyList="Id,id",keyColumnURLList="/connector/configuration/connectorObject.cfm?editor=true&id=,/connector/configuration/connectorMapping.cfm?objectID=",keyColumnList="edit,fieldMappings",columnTranslation="true",allowColumnSorting=false}>

	<cfloop list="0,1" index="isRelaywareObject">
		<cfset qryObjects = application.getObject("connector.com.connector").getConnectorObjects(connectorType=connectorType,relayware=isRelaywareObject)>

		<cfquery name="qryObjects" dbType="query">
			select #qryObjects.columnList#, lower(object) as objectLower, '<span class="editLink" title="Edit">Edit</span>' as edit,'<span class="listLink" title="List">List</span>' as fieldMappings,1 as hasWhereClause
			from qryObjects
			where whereClause is not null
			union
			select #qryObjects.columnList#, lower(object) as objectLower, '<span class="editLink" title="Edit">Edit</span>' as edit,'<span class="listLink" title="List">List</span>' as fieldMappings,0 as hasWhereClause
			from qryObjects
			where whereClause is null
			order by objectLower
		</cfquery>

		<cfset direction = isRelaywareObject?'Export':'Import'>

		<h3><cfoutput>#direction#</cfoutput> Objects</h3>
		<div>
			<CF_tableFromQueryObject
				queryObject=#qryObjects#
				columnTranslationPrefix="phr_connector_#direction#_report_"
				attributeCollection=#attributeStruct#
			>
		</div>
	</cfloop>

	<cfset application.com.request.setTopHead(topHeadCfm="/relay/connector/connectorTopHead.cfm",connectorType=connectorType,addRemoteObject=apiConfigured)>
</cfif>