/* �Relayware. All Rights Reserved 2014 */

function hashCode(str){
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

function onConnectorMappingPageLoad() {
    jQuery('#whereClause').data("hash", hashCode(jQuery('#whereClause').val()));
    jQuery('#whereClause').data("validating", false);
    jQuery('#whereClause').blur(function() {
        var whereClauseHash = jQuery('#whereClause').data("hash");
        if (whereClauseHash != hashCode(jQuery('#whereClause').val()) && !jQuery('#whereClause').data("validating")) {
            jQuery('#whereClause').data("save", false);
            validateWhereClause();
        }
    });

    jQuery('#SaveBtn').removeAttr('onclick');
    jQuery('#SaveBtn').click(function() {
        jQuery('#whereClause').attr('readonly','true');
        var whereClauseHash = jQuery('#whereClause').data("hash");
        if (whereClauseHash != hashCode(jQuery('#whereClause').val()) && !jQuery('#whereClause').data("save") || jQuery('#whereClause').data("isOK") === undefined) {
            jQuery('#whereClause').data("save", true);
            if (!jQuery('#whereClause').data("validating")){
                validateWhereClause();
            }
        } else if (whereClauseHash === hashCode(jQuery('#whereClause').val())) {
            FormSubmit(this);
        }
    });
}

function validateWhereClause() {
    jQuery('#whereClause').data("validating", true);
    jQuery('#whereClause').data("hash", hashCode(jQuery('#whereClause').val()));
    removeValidationMessage('#whereClauseValidationStatus_label');
    startSpinner('#whereClauseValidationStatus_label');

    jQuery.ajax({
        type: "GET",
        url: "/webservices/callWebService.cfc?",
        data: {method:'callWebService',
            webServiceName:'connectorWS',
            methodName:'validateWhereClause',
            object:jQuery('#object').val().trim(),
            relayware:jQuery('#relayware').val(),
            whereClause:jQuery('#whereClause').val(),
            connectorType:jQuery('#connectorType').val().trim(),
            returnFormat:'json'
        },
        dataType: "json",
        cache: false,
        async: true,
        success: function(data,textStatus,jqXHR) {
            jQuery('#whereClause').data("isOK", result);
            var result = data.ISOK ? true : false;
            displayValidationMessage(data, '#whereClauseValidationStatus_label');
            stopSpinner('#whereClauseValidationStatus_label');
            if (jQuery('#whereClause').data("save") && result && jQuery('#whereClause').data("hash") === hashCode(jQuery('#whereClause').val())) {
                FormSubmit(jQuery('#SaveBtn').get(0));
            }
            jQuery('#whereClause').data("validating", false);
            jQuery('#whereClause').data("save", false);
            jQuery('#whereClause').removeAttr('readonly');
        },
        failure: function () {
            jQuery('#whereClause').data("validating", false);
            jQuery('#whereClause').removeAttr('readonly');
        },
        error: function() {
            stopSpinner('#whereClauseValidationStatus_label');
            displayValidationMessage(false, '#whereClauseValidationStatus_label');
            jQuery('#whereClause').data("validating", false);
            jQuery('#whereClause').removeAttr('readonly');
        }
    });
}

function removeValidationMessage(selector) {
	var elementToPrintAfter = jQuery(selector == undefined ? 'div.header' : selector);
    elementToPrintAfter.next("[id^='message_']").remove();
}

function displayValidationMessage(data, selector) {
	if (data) {
		message(data.ISOK ? data.MESSAGE : 'Invalid where clause, please fix before saving.',
				data.ISOK ? "success" : "error",
				false,
				selector,
				'form-control');
	} else {
		message("Unexpected error appeared, please try again",
				"error",
				false,
				selector,
				'form-control');
	}
}

function startSpinner(selector) {
	jQuery(selector).spinner({position:'left'});
}

function stopSpinner(selector) {
	jQuery(selector).removeSpinner();
}