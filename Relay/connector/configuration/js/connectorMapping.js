/* Relayware. All Rights Reserved 2014 */
/**
HISTORY
When 		Who 	What
2015/06/25 	DXC 	Enhancement to attempt to auto select <option> if it matches row text
2015/06/25	DXC		Enhancement to sort potential mapping dropdown alpabetically
**/

var currentlySelectedRemoteColumnValue = '';
var remoteColumnOptions = '';
var relaywareColumnOptions = '';
var connectorType = '';

function onConnectorMappingPageLoaded () {

	connectorType = jQuery('#connectorType').val();

	/* if we don't have a selected value, set the first item as selected */
	var relaywareCol = jQuery('#column_relayware');
	if (!jQuery('#column_relayware option:selected').length) {
		relaywareCol.val(jQuery("#column_relayware option:first").val());
	}

	/* if the mapping type is field, then hide the defaultValue text box */
	if (jQuery('#mappingType_Field').prop('checked')) {
		jQuery('#defaultValue_formgroup').hide();
	} else {
		if (jQuery('#direction').val().toUpperCase() == 'E') {
			jQuery('#defaultValue').val(jQuery('#column_relayware').val())
		} else if (jQuery('#direction').val().toUpperCase() == 'I') {
			jQuery('#defaultValue').val(jQuery('#column_remote').val())
		}	
	}
	
	/* save the original options for both the relayware and remote column dropdowns */
	remoteColumnOptions = jQuery('#column_remote').html();
	setRelaywareColumnOptions();
	
	onChangeDirection();
	onChangeMappingType();
	showMappedValues();
	
	hideOptionsWithIncompatibleDataTypes();
    setDefaultWhenNull();
    showDefaultValueWhenNull();

	jQuery('#column_relayware_value').append('<div>Fields prefixed with Custom: are customised field</div><div>* denotes a required field</div>');
}

function setRelaywareColumnOptions() {
	relaywareColumnOptions = jQuery('#column_relayware').html();
}

function getDirection() {
	return jQuery('#direction :selected').text().trim().toLowerCase();
}

function onChangeDirection() {
	/* if synching in both directions, then the mapping type cannot be 'defaultValue', so hide it and set the mapping type to field */
	if (getDirection() == 'both') {
		jQuery('#mappingType_formgroup').hide();
		jQuery('#mappingType_Field').prop('checked', true);
		onChangeMappingType();
	} else {
		jQuery('#mappingType_formgroup').show();
		showHideColumnDropdowns();	
	}
}

function onChangeMappingType() {
	var mappingTypeDefaultValueChecked = jQuery('#mappingType_DefaultValue').prop('checked');
	/* if mapping type is 'default value', then show the defaultValue text box; otherwise hide it */
	jQuery('#defaultValue_formgroup').css('display',(mappingTypeDefaultValueChecked)?'':'none');
	
	var mappedValuesRow = jQuery('#mappedvalues_row');
	/* if the mapped values row exists, then hide it if default value is checked */
	if (mappedValuesRow.length > 0){
		mappedValuesRow.css('display',(mappingTypeDefaultValueChecked)?'none':'');
	}
	
	jQuery('#mappedRelaywareColumn_formgroup').css('display',(mappingTypeDefaultValueChecked)?'none':'');
	
	/* if default value is displayed, make it required */
	setValidate(document.getElementById('defaultValue'),jQuery('#mappingType').is(":visible"));
	showHideColumnDropdowns();
}

function setDefaultWhenNull() {
    if(document.getElementById('defaultImportValue').value != ''
        || document.getElementById('defaultExportValue').value != '') {
        jQuery('#defaultWhenNull_1').attr('checked', true);
    } else {
        jQuery('#defaultWhenNull_0').attr('checked', true);
    }
}

function showDefaultValueWhenNull() {

	var defaultWhenNull_True = jQuery('#defaultWhenNull_1');
	var defaultWhenNull_False = jQuery('#defaultWhenNull_0');

	var defaultImportValueGroup = jQuery('#defaultImportValue_formgroup');
    var defaultImportValue = jQuery('#defaultImportValue');

	var defaultExportValueGroup = jQuery('#defaultExportValue_formgroup');
    var defaultExportValue = jQuery('#defaultExportValue');

    /* show field according to direction */
	if (defaultWhenNull_True.prop('checked')) {
		if (getDirection() == 'import') {
            defaultImportValueGroup.show();
            defaultImportValue.removeAttr('disabled');

            defaultExportValueGroup.hide();
            defaultExportValue.val('');
            defaultExportValue.attr('disabled', 'true');
		} else if (getDirection() == 'export') {
            defaultImportValueGroup.hide();
            defaultImportValue.val('');
            defaultImportValue.attr('disabled', 'true');

            defaultExportValueGroup.show();
            defaultExportValue.removeAttr('disabled');
		} else {
            defaultImportValueGroup.show();
            defaultImportValue.removeAttr('disabled');

            defaultExportValueGroup.show();
            defaultExportValue.removeAttr('disabled');
        }
    } else if (defaultWhenNull_False.prop('checked')) {
        defaultImportValueGroup.hide();
        defaultImportValue.val('');
        defaultImportValue.attr('disabled', 'true');

        defaultExportValueGroup.hide();
        defaultExportValue.val('');
        defaultExportValue.attr('disabled', 'true');
	}

	/* hide defaultImport and defaultExport values when defaultValue is checked */
    if (jQuery('#mappingType_DefaultValue').prop('checked')) {
        defaultWhenNull_False.prop('checked',true);
        defaultImportValueGroup.hide();
        defaultExportValueGroup.hide();
        defaultWhenNull_False.attr('disabled', 'true');
        defaultWhenNull_True.attr('disabled', 'true');
    } else {
        defaultWhenNull_False.removeAttr('disabled');
        defaultWhenNull_True.removeAttr('disabled');
    }

    /* if default value fields is displayed, make it required */
    defaultImportValue.get(0).required = defaultImportValue.is(':visible') ? true : false;
    defaultExportValue.get(0).required = defaultExportValue.is(':visible') ? true : false;

    onChangeMappingType();
}

function validateDefaultValues() {

    var selectedRelaywareColumn = jQuery('#column_relayware :selected');
    var selectedRemoteColumn = jQuery('#column_remote :selected');

    var defaultImportValue = jQuery('#defaultImportValue');
    var defaultExportValue = jQuery('#defaultExportValue');

    /* validate fields by datatype */
	if (defaultImportValue.is(':visible')) {
        defaultImportValue.get(0).required = true;
        if (selectedRelaywareColumn.attr('data-datatype') != 'text' & selectedRelaywareColumn.attr('data-datatype') != 'textarea' & selectedRelaywareColumn.attr('data-datatype') != 'picklist') {
            defaultImportValue.get(0).setAttribute('validate', selectedRelaywareColumn.attr('data-datatype') ? selectedRelaywareColumn.attr('data-datatype') : '');
        } else {
            defaultImportValue.get(0).removeAttribute('validate');
        }
    } else {
        defaultImportValue.get(0).required = false;
    }
    if (defaultExportValue.is(':visible')) {
        defaultExportValue.get(0).required = true;
        if (selectedRemoteColumn.attr('data-datatype') != 'text' & selectedRemoteColumn.attr('data-datatype') != 'textarea' & selectedRelaywareColumn.attr('data-datatype') != 'picklist') {
            defaultExportValue.get(0).setAttribute('validate', selectedRemoteColumn.attr('data-datatype') ? selectedRemoteColumn.attr('data-datatype') : '');
        } else {
            defaultExportValue.get(0).removeAttribute('validate');
        }
    } else {
        defaultExportValue.get(0).required = false;
    }

    /* validate lookups */
    if (selectedRemoteColumn.attr('data-datatype') == 'reference') {
        if (defaultExportValue.is(':visible')) {
            defaultExportValue.get(0).setAttribute('validate', 'regex');
            /* restrict field to 18 character for salesforce and 28 for msDynamics */
            if (connectorType == 'salesforce') {
                defaultExportValue.get(0).setAttribute('pattern', '^.{18}$');
                defaultExportValue.get(0).setAttribute('message', 'Value should be 18 character.');
            } else if (connectorType == 'msDynamics') {
                defaultExportValue.get(0).setAttribute('pattern', '^.{28}$');
                defaultExportValue.get(0).setAttribute('message', 'Value should be 28 character.');
            }
        }
        if (defaultImportValue.is(':visible')) {
            defaultImportValue.get(0).setAttribute('validate', 'numeric');
        }
    }

    if (defaultImportValue.is(':visible')) {
        doValidationOnSingleElement (defaultImportValue.get(0));
    }
    if (defaultExportValue.is(':visible')) {
        doValidationOnSingleElement (defaultExportValue.get(0));
    }

}

function showHideColumnDropdowns() {
	/* if the default value text box is shown, hide the fieldname dropdown; and if it's hidden, show the fieldname dropdown */
	var disabledColumnObj = jQuery('select[id^="column_"]:disabled')

	if (disabledColumnObj.length == 1) {
		disabledColumnObj.closest('div[id$="_formgroup"]').css('display',(jQuery('#defaultValue').is(":visible"))?'none':'');
		
	/* both selects are enabled as we are adding a new mapping */
	} else {
		/* if the mapping type is field, show both relayware and remote object dropdowns. However, hide the appropriate dropdown if we're either importing or exporting and mapping type is default value */
		var mappingTypeIsField = jQuery('input[id^="mappingType_"][id$="Field"]').prop('checked');
		var synchDirection = jQuery('#direction').val().trim().toLowerCase();

		/* show/hide column dropdowns based on mapping type and direction. set the validation on them as appropriate. Don't validate if they are hidden; do if they are shown */
		var relaywareColumnObj = jQuery('#column_relayware');
		jQuery('#column_relayware_formgroup').css('display',(mappingTypeIsField || synchDirection == 'i')?'':'none');
		setValidate(relaywareColumnObj.get(0),relaywareColumnObj.is(":visible"));
		
		var remoteColumnObj = jQuery('#column_remote');
		jQuery('#column_remote_formgroup').css('display',(mappingTypeIsField || synchDirection == 'e')?'':'none');
		setValidate(remoteColumnObj.get(0),remoteColumnObj.is(":visible"));
	}
}

function showMappedValues() {
	// if we have mapped values or a foreign key value, then show the column/value radio buttons
	$_mappedRelaywareColumnRow = jQuery('#mappedRelaywareColumn_formgroup');
	if (jQuery('select[id="column_relayware"]').length) {
		var selectedRelaywareColumn = jQuery('#column_relayware :selected');
		if (selectedRelaywareColumn.val() == '' || selectedRelaywareColumn.data('foreignkeytable') == '') {
			$_mappedRelaywareColumnRow.hide();
		}				
		onMappedValueOrColumnToggle();
		
	} else {
		$_mappedRelaywareColumnRow.hide();
	}
}


function hideOptionsWithIncompatibleDataTypes () {
	/* filter out values that are of not the correct data type. Keep any options with the 'custom' datatype */
	var enabledColumnObj = jQuery('select[id^="column_"]:enabled');

	/* editing an existing mapping */
	if (enabledColumnObj.length == 1) {

		/* only strip out invalid types if working at a field level */
		if (jQuery('#mappingType_Field').prop('checked')) {
			/* get the current value */		
			var columnVal = enabledColumnObj.val();
			/* resetting the options to be the original. We then remove ones that we don't want based on datatype */
			if (enabledColumnObj.attr('id') == 'column_relayware') {
				enabledColumnObj.html(relaywareColumnOptions);
			} else {
				enabledColumnObj.html(remoteColumnOptions);
			}
	
			enabledColumnObj.children('option[value !=""][data-datatype !="custom"][data-datatype !="'+ jQuery('select[id^="column_"]:disabled :selected').attr('data-datatype') +'"]').remove(); /* filter out values that do not have the same datatype as the disabled value */
			/* reset the value - keep the original value in the select list as an existing mapping may not have the same datatype, and we don't want to lose the original value*/
			if (enabledColumnObj.filter(':contains("'+columnVal+'")').length == 0) {
				enabledColumnObj.append(jQuery('<option></option').attr('value',columnVal).text(columnVal));
			}
			enabledColumnObj.val(columnVal);
		}
	} else {
		/* it's a new mapping, so two selects are enabled. Remove options where selected value is currently empty */
		var remoteColumnObj = jQuery('#column_remote');
		var relaywareColumnObj = jQuery('#column_relayware');

		jQuery('select[id^="column_"]').children().show();

		if (relaywareColumnObj.val() == '' && remoteColumnObj.val() != '') {
			relaywareColumnObj.html(relaywareColumnOptions);
			if (remoteColumnObj.find(":selected").attr('data-datatype')) {
				relaywareColumnObj.children('option[value !=""][data-datatype !="custom"][data-datatype !="'+ remoteColumnObj.find(":selected").data('datatype') +'"]').remove();
			}
		} else if (remoteColumnObj.val() == ''  && relaywareColumnObj.val() != '') {
			remoteColumnObj.html(remoteColumnOptions);
			/* only filter by datatype if selected datatype is not 'custom' */
			if (relaywareColumnObj.find(":selected").attr('data-datatype') !='custom') {
				remoteColumnObj.children('option[value !=""][data-datatype !="custom"][data-datatype !="'+ relaywareColumnObj.find(":selected").data('datatype') +'"]').remove(); 
			}
		} else if (remoteColumnObj.val() == ''  && relaywareColumnObj.val() == '') {
			relaywareColumnObj.html(relaywareColumnOptions);
			remoteColumnObj.html(remoteColumnOptions);	
		}
	}
}

/* the function is run when .... */
function onMappedValueOrColumnToggle () {
	// reset the mapped values display
	var mappedValuesRow = jQuery('#mappedvalues_row');
	if (mappedValuesRow.length)
		mappedValuesRow.remove();
		
	var relaywareColumn = jQuery('#column_relayware :selected');

	// if we're mapping to a column and it's not a picklist, then show the mappedColumn field
	if (relaywareColumn.length > 0 && relaywareColumn.val() != '' && relaywareColumn.data('foreignkeytable') != '' && relaywareColumn.data('datatype').toLowerCase() != 'picklist') {
		if (!jQuery('#mappingType_DefaultValue').prop('checked')) {
			jQuery('#mappedRelaywareColumn_formgroup').show();
		}
		
		page = '/webservices/callWebService.cfc?'

		jQuery.ajax({
			type: "GET",
			url: "/webservices/callWebService.cfc?",
			data: {method:'callWebService',webServiceName:'connectorWS',methodName:'getTableColumns', tablename:relaywareColumn.data('foreignkeytable'), returnFormat:'json'},
			dataType: "json",
			cache: false,
			success: function(data,textStatus,jqXHR) {
				if (data) {
					var mappedRelaywareColumn = jQuery('#mappedRelaywareColumn');
					mappedRelaywareColumn.replaceWith('<select id="mappedRelaywareColumn"><option></select>');

					var selected = '';
					for(var key in data){
						selected = '';
						if (key == mappedRelaywareColumn.val()) {
							selected='selected="SELECTED"';	
						}
			            mappedRelaywareColumn.append('<option value="'+ key +'" ' +selected + '>'+ key +'</option>')
			        }
				}
			}
		})
	}
			
	/// if we're mapping to values, show the values
	else if (relaywareColumn.val() != '' && relaywareColumn.data('datatype').toLowerCase().indexOf('picklist') != -1) {
		jQuery('#mappedRelaywareColumn_formgroup').hide();
		
		page = '/webservices/callWebService.cfc?'

		jQuery.ajax({
			type: "GET",
			url: "/webservices/callWebService.cfc?",
			data: {method:'callWebService',webServiceName:'connectorWS',methodName:'getColumnLookupValues', tablename:jQuery('#connectorObjectId_relayware_display :selected').text().trim(), column:jQuery('#column_relayware').val().trim(), connectorType:connectorType, returnFormat:'json'},
			dataType: "json",
			cache: false,
			success: function(data,textStatus,jqXHR) {
				if (data) {
					if (jQuery('#mappedValuesDiv').length = 1)
						jQuery('#mappedValuesDiv').remove();
						
					jQuery('#defaultWhenNull_formgroup').before('<div class="form-group" id="mappedValuesDiv"><label>Mapped Values</label><div id="mappedValues_value" name="mappedValues_value"><div id="mappedValuesList"></div></div></div>');
					
					var remotePicklist = jQuery('<input type="text"></input>');
					
					if (jQuery('#column_remote :selected').data('datatype').toLowerCase().indexOf('picklist') != -1) {
						jQuery.ajax({
							type: "GET",
							url: "/webservices/callWebService.cfc?",
							data: {method:'callWebService',webServiceName:'connectorWS',methodName:'getRemoteColumnLookupValues', object:jQuery('#connectorObjectId_remote_display :selected').text().trim(), column:jQuery('#column_remote').val().trim(), connectorType:connectorType, returnFormat:'json'},
							dataType: "json",
							cache: false,
							async: false,
							success: function(data,textStatus,jqXHR) {
								if (data) {
									remotePicklist = jQuery('<select class="remoteValues form-control"><option value="">Select a value</option><select>')
									remotePicklist.change(function(){removeSelectedMappingValues(this);});
									remotePicklist.focus(function(){setCurrentValue(this);});
									
									/*
									//START DXC Enhancement to sort potential mapping dropdown alpabetically
									var keyList=[];
									for(var k in data){
										if (data.hasOwnProperty(k)){
											keyList.push(k);
										}
									}
									keyList.sort();
									for(var key in keyList){
										if (keyList.hasOwnProperty(key)){
											remotePicklist.append('<option value="'+ keyList[key]+'">'+ keyList[key] +'</option>')
										}
									}
									//END DXC Enhancement
									*/
									for (var i=0,  tot=data.length; i < tot; i++) {
										remotePicklist.append('<option value="'+ data[i].VALUE+'">'+ data[i].VALUE +'</option>')
									}
								}
							}
						})
					}

					/* create a row for each value to be mapped in Relayware */
					for (var i=0,  tot=data.length; i < tot; i++) {
						var dataValueString = data[i]['DATAVALUE'].toString();
						var divID = dataValueString.replace(/\s/g, '').replace(/\./g,'') +'_remoteValueDiv';
						var relaywareDisplayValue = data[i]['DISPLAYVALUE'].toString();
						if (relaywareDisplayValue.trim() == '')
							relaywareDisplayValue = '&nbsp;';
			            jQuery('#mappedValuesList').append('<div class="group"><div class="col1">'+ relaywareDisplayValue +'</div><div class="col2">Maps To</div><div class="col3" id="'+ divID +'"></div></div>');
			            var mappedValueID = 'mappedValue_'+ data[i]['ID'] +'_'+dataValueString.replace(/\s/g, '__'); // replace spaces with double underscores
			            jQuery(remotePicklist).clone(true)
			            	.attr('id',mappedValueID)
			            	.attr('name',mappedValueID)
			            	.val(data[i]['MAPPEDVALUE'])
			            	.appendTo('#'+divID);
			        }
			        
			        var autoMapped = false;
					//START DXC Enhancement to attempt to auto select <option> if it matches row text
					jQuery("select[class~=remoteValues]").each(function(){
						textNode=jQuery(this).parent().parent().contents().first().text();
						jQuery(this).find("option").each(
							function(){
								if (jQuery(this).val()==textNode && jQuery(this).parent().val() == ''){
									jQuery(this).parent().val(textNode);
									jQuery(this).parent().parent().append('<span id="autoMappedAsterix" class="required">*</span>');
									autoMapped=true;
								}
							}
						);
						
					}
					);
			        //END DXC Enhancement to attempt to auto select
			        
			        if (autoMapped) {
			        	jQuery('#mappedValues_value').append('<div class="warningblock" id="mappedValuesWarning">The mapped values with an * have been auto-mapped and have not yet been saved. Please validate them and click \'Save\'.</div>');
			        }
			        
			        /* if these dropdowns have just been added, then we want to run the various functions against them... */
			    	jQuery('.remoteValues').each(function() {jQuery(this).focus();jQuery(this).change();})
				}
			}
		})
	}
}

/* for remote object value mappings, if one option is selected, remove that value from other dropdowns so that you can't map it twice */
function removeSelectedMappingValues(remoteValueObj) {
	var optionToRemove = jQuery(remoteValueObj).val();
	var mappedValuesDropdowns = jQuery('.remoteValues');
	if (optionToRemove != '') {
		mappedValuesDropdowns.children('option[value="'+ optionToRemove +'"]').hide();
	}
	if (optionToRemove != currentlySelectedRemoteColumnValue) {
		mappedValuesDropdowns.children('option[value="'+currentlySelectedRemoteColumnValue+'"]').show();
	}
}

function setCurrentValue(remoteValueObj) {
	currentlySelectedRemoteColumnValue = jQuery(remoteValueObj).val();	
}

function getIDColumn() {
	jQuery.ajax({
		type: "GET",
		url: "/webservices/callWebService.cfc?",
		data: {method:'callWebService',webServiceName:'connectorWS',methodName:'getRemoteIDColumn', object:jQuery('select[name^="connectorObjectId_remote"] :selected').text().trim(), connectorType:connectorType, returnFormat:'json'},
		dataType: "json",
		cache: false,
		async: false,
		success: function(data,textStatus,jqXHR) {
			var remoteColumnObj = jQuery('#column_remote');
			remoteColumnObj.find('option').remove().end();
			if (data) {
				remoteColumnObj.append('<option data-dataType="text" value="'+data+'">'+data+'</option>');
				remoteColumnObj.change();
			}
			
		}
	})
}


function getColumnsThatMapToDataType() {	
	jQuery.ajax({
		type: "GET",
		url: "/webservices/callWebService.cfc?",
		data: {method:'callWebService',webServiceName:'connectorWS',methodName:'getTableColumns', tablename:jQuery('#connectorObjectId_relayware :selected').text().trim(), dataType:jQuery('#column_remote :selected').data('datatype'), returnFormat:'json'},
		dataType: "json",
		cache: false,
		async: false,
		success: function(data,textStatus,jqXHR) {
			var relaywareColumnObj = jQuery('#column_relayware');
			relaywareColumnObj.find('option').remove().end();
			
			if (data) {
				relaywareColumnObj.append('<option data-datatype="text" value=""></option>');
				for(var key in data){
					relaywareColumnObj.append('<option data-foreignKeyTable="'+data[key].FOREIGNKEY+'" data-datatype="'+data[key].DATATYPE+'" value="'+key+'">'+key+'</option>');
				}
			}
			setRelaywareColumnOptions();
			relaywareColumnObj.change();
		}
	})
}

/* delete a connector mapping row */
function deleteMapping(ID) {
	if (confirm('Are you sure you want to delete this mapping?')) {
		var deleteTD = jQuery('tr[id="ID='+ID+'"] td.Delete');
		deleteTD.spinner();
		jQuery.ajax({
			type: "POST",
			url: "/webservices/callWebService.cfc?",
			data: {method:'callWebService',webServiceName:'connectorWS',methodName:'deleteConnectorMapping', connectorMappingID:ID, returnFormat:'json'},
			dataType: "json",
			cache: false,
			async: true,
			success: function(data,textStatus,jqXHR) {
				if (data) {
					/* remove the row from the listing table */
					deleteTD.removeSpinner();
					jQuery('tr[id*='+ID+']').remove();
				}
			}
		})
	}
	return false;
}

function openIssueReportForm(url) {
	//Direction
	var direction = jQuery('#direction option:selected').text();
	
	//Remote field
	var columnRemote = jQuery('#column_remote option:selected').val();

	//RW field
	var columnRelayware = jQuery('#column_relayware option:selected').val();

	if (direction) {
		url = url + "&direction=" + direction;
	}

	if (columnRemote) {
		url = url + "&columnRemote=" + columnRemote;
	}

	if (columnRelayware) {
		url = url + "&columnRelayware=" + columnRelayware;
	}

	window.location.href = url;
}