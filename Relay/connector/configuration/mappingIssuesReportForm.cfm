<cfparam name="url.objectID" type="numeric">
<cfparam name="url.mappingId" type="numeric" default="0">
<cfparam name="url.direction" type="string" default="">
<cfparam name="url.columnRemote" type="string" default="">
<cfparam name="url.columnRelayware" type="string" default="">

<cfif structKeyExists(form, "frmSendMail")>
	<cfset application.com.request.setTopHead(pageTitle="#application.com.relayTranslations.translatePhrase("phr_connector_ReportMappingIssue")#")>
	<CF_RelayNavMenu thisDir="connector">
		<CF_RelayNavMenuItem MenuItemText="phr_Back" CFTemplate="JavaScript:history.go(-2);">
	</CF_RelayNavMenu>

	<cfset result = application.getObject("connector.com.connectorMapping").sendConnectorMappingIssueReport(
		emailFrom = form.emailFrom,
		relaywareEnvironment = form.relaywareEnvironment,
		remoteObject = form.remoteObject,
		relaywareObject = form.relaywareObject,
		direction = form.direction,
		remoteField = form.remoteField,
		relaywareField = form.relaywareField,
		issue = form.issue)>

	<cfset application.com.relayUI.setMessage(type = result.status, message = result.message)>

<cfelseif structKeyExists(url, "showForm")>
	<cfset application.com.request.setTopHead(pageTitle="#application.com.relayTranslations.translatePhrase("phr_connector_ReportMappingIssue")#")>
	<CF_RelayNavMenu thisDir="connector">
		<CF_RelayNavMenuItem MenuItemText="phr_Back" CFTemplate="JavaScript:history.go(-1);">
	</CF_RelayNavMenu>

	<cfset getObjectDetails = application.getObject("connector.com.connector").getConnectorObjects(ID=url.objectID)>
	<cfset connectorType = getObjectDetails.connectorType[1]>
	<cfset isRemoteObject = not getObjectDetails.relayware[1]>
	
	<cfset relaywareObjectID = not isRemoteObject?url.objectID:0>
	<cfset remoteObjectID = isRemoteObject?url.objectID:0>
	<cfset relaywareObject = not isRemoteObject?getObjectDetails.object[1]:"">
	<cfset remoteObject = isRemoteObject?getObjectDetails.object[1]:"">

	<cfset getConnectorMappings = application.getObject("connector.com.connectorMapping").getConnectorMappings(connectorType=connectorType,objectID=url.objectId,connectorMappingID=url.mappingID)>
	<cfif getConnectorMappings.recordCount>
		<cfset isMappedObject = true>
		<cfset relaywareObjectID = getConnectorMappings.connectorObjectID_relayware[1]>
		<cfset remoteObjectID = getConnectorMappings.connectorObjectID_remote[1]>
		<cfset relaywareObject = getConnectorMappings.object_relayware[1]>
		<cfset remoteObject = getConnectorMappings.object_remote[1]>
	<cfelse>
		<cfset isMappedObject = application.getObject("connector.com.connector").isObjectMapped(object_relayware=not relaywareObjectID?getObjectDetails.object[1]:"",object_remote=not remoteObjectID?getObjectDetails.object[1]:"",connectorType=connectorType)>
	</cfif>
	<cfset excludeMappedObjects = not isMappedObject?true:false>

	<cfoutput>
		<p>phr_connector_pleaseReportMappingIssues</p>
	</cfoutput>
	<form method="post" name="mailForm">
		<CF_relayFormDisplay>
			<CF_relayFormElementDisplay fieldName="relaywareEnvironment" 
										label="phr_connector_rwEnvironment"
										relayFormElementType="text" 
										currentValue="#request.currentsite.domain#" 
										readonly="true" >
			
			<CF_relayFormElementDisplay fieldName="remoteObject" 
										label="phr_connectorMapping_connectorObjectId_remote" 
										relayFormElementType="select" 
										query="#application.getObject('connector.com.connector').getConnectorObjects(connectorType=connectorType, relayware=0, excludeMappedObjects=url.mappingID eq 0 and excludeMappedObjects?true:false, ID=remoteObjectID)#" 
										currentValue=""
										display="object" 
										value="object">
			
			<CF_relayFormElementDisplay fieldName="relaywareObject" 
										label="phr_connectorMapping_connectorObjectId_relayware" 
										relayFormElementType="select" 
										query="#application.getObject('connector.com.connector').getConnectorObjects(connectorType=connectorType, relayware=1, excludeMappedObjects=url.mappingID eq 0 and excludeMappedObjects?true:false, ID=isMappedObject?relaywareObjectID:0)#" 
										currentValue=""
										display="object" 
										value="object">
			
			<CF_relayFormElementDisplay fieldName="direction"
										label="phr_connectorMapping_direction" 
										relayFormElementType="select" 
										query="select * from (values ('', 'Both'), ('I', 'Import'),('E', 'Export')) as q (value, display)" 
										currentValue="#url.direction#" 
										display="display" 
										value="display">

			<CF_relayFormElementDisplay fieldName="remoteField" 
										label="phr_connectorMapping_column_remote" 
										relayFormElementType="select" 
										query="#application.getObject('connector.com.connectorMapping').getRemoteObjectFields(connectorType=connectorType,object = remoteObjectID, column= url.columnRemote)#" 
										currentValue="#url.columnRemote#" 
										display="fieldname" 
										value="fieldNameValue"
                                        showNull="true">

			<CF_relayFormElementDisplay fieldName="relaywareField" 
										label="phr_connectorMapping_column_relayware" 
										relayFormElementType="select" 
										query="#application.getObject('connector.com.connectorMapping').getRelaywareObjectFields(connectorType=connectorType, object = relaywareObjectID,column= url.columnRelayware)#" 
										currentValue="#url.columnRelayware#" 
										display="fieldNameDisplay" 
										value="fieldNameValue"
                                        showNull="true">

			<CF_relayFormElementDisplay fieldName="issue" 
										label="phr_connector_issue" 
										relayFormElementType="textArea"
										currentValue=""
										cols="40" 
										rows="10"
										maxLength="1000">

			<CF_relayFormElementDisplay fieldName="emailFrom" 
										label="phr_connector_issueReporter"
										relayFormElementType="text" 
										currentValue="#request.relayCurrentUser.fullName# <#request.relayCurrentUser.person.email#>" 
										readonly="false"
										maxLength="255">

			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="frmSendMail" currentValue="phr_send" label="">
		</CF_relayFormDisplay>
	</form>
<cfelse>
	<cfoutput>
		<a id="btnReportMappingIssue" class="btn btn-primary" href="javascript:openIssueReportForm('/connector/configuration/mappingIssuesReportForm.cfm?showForm=true&objectId=#url.objectId#&mappingID=#url.mappingID#')">phr_connector_ReportMappingIssue</a>
	</cfoutput>
</cfif>
