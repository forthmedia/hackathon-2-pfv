﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorDashboard.cfm
Author:			NJH
Date started:	23-01-2015

Description:	Connector Dashboard

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/01/23			NJH			Add the proper dates through to the status report. Also filter out inactive/not synching objects from the dashboard
2015/02/09			NJH			Jira Fifteen-128 - deal with a non-initialised connector.
2015-06-17			WAB			use CF_chart rather than CFChart
2015/06/19			NJH			Provide link around failure div to open up the actual error message.
2015/07/06			NJH			Rewrote views to work off the new vConnectorRecords view which shows the status of the records.
2015/07/10			NJH			Filter out records that don't have data because they've been deleted, but which haven't come through as deletes (generally in Q as a dependency)
2015/11/30 			SB 			Bootstrap
2016/01/12			NJH			Output message if not able to connect to the remote system.
2016/01/29			NJH			Only test connection to remote site when manually hitting the dashboard. The refresh will not test the connection as we don't want to spam the site with requests
Possible enhancements:


 --->

<!---
Status:Description

Success: flagged with either an import/export synched flag
Pending: has a record in pending and has the 'Synching' flag set and has a dependency, regardless of whether it has errored or not. This has been modified to include any item in the queue as long as it hasn't errored. This is to handle the case
		when an opportunity brings in an account, which hasn't yet been processed as it's waiting for the next run.
Failure: has a record in the error and is flagged as synching or suspended
 --->

<cfparam name="connectorType" default="#application.com.settings.getSetting("connector.type")#">

<cf_includejavascriptOnce template="/javascript/extExtension.js">
<cf_includeCSSOnce template="/connector/css/connector.css">

<cfset showLast24HoursFrom=false>
<cfset showLast24HoursTo=false>

<cfif not StructKeyExists(form,"frmFromDate") or form.frmFromDate eq "">
	<cfset form.frmFromDate = dateAdd("h",-24,request.requestTime)>
	<cfset showLast24HoursFrom=true>
</cfif>
<cfif not StructKeyExists(form,"frmToDate") or form.frmToDate eq "">
	<cfset form.frmToDate = request.requestTime>
	<cfset showLast24HoursTo=true>
</cfif>

<!--- if the to date is equal to todays date (ie. we're looking at current data), then refresh the page every minute so that we get up to date info --->
<cfif not structKeyExists(form,"frmSubmit")>
	<script type="text/javascript">
		window.setTimeout(function(){
			document.location.href = !~document.location.href.indexOf('noTickle') ? document.location.href + '&noTickle=true' : document.location.href.replace(/(noTickle=)\d+/, '$1' + 'true');
		}, 60000);
	</script>
</cfif>

<cfset toDatePlusOneDay = dateAdd("d",1,form.frmToDate)>
<cfset showLast24Hours = showLast24HoursFrom and showLast24HoursTo>

<cfset connectorEntities=application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=connectorType)>
<cfset successDiv = "<div class='successblock'>Success</div>">
<cfset failureDiv = "<div class='errorblock'>Fail</div>">
<cfif request.relayCurrentUser.errors.showLink>
	<cfset failureDiv = "<a href='/errorHandler/errorDetails.cfm?errorID=0' target='_blank'><div class='errorblock'>Fail</div></a>">
</cfif>

<cfset queryAddColumn(connectorEntities,"exportStatusDisplay","varchar",arrayNew(1))>
<cfset queryAddColumn(connectorEntities,"importStatusDisplay","varchar",arrayNew(1))>

<cfloop query="connectorEntities">
	<cfset exportStatusDiv = "">
	<cfset importStatusDiv = "">
	<cfset failureDivWithErrorID = failureDiv>

	<cfif export>
		<cfset exportStatusDiv = exportErrorID eq "" ? successDiv:replace(failureDivWithErrorID,0,exportErrorID)>
	</cfif>
	<cfif import>
		<cfset importStatusDiv = importErrorID eq "" ? successDiv:replace(failureDivWithErrorID,0,importErrorID)>
	</cfif>
	<cfset querySetCell(connectorEntities,"exportStatusDisplay",exportStatusDiv,currentRow)>
	<cfset querySetCell(connectorEntities,"importStatusDisplay",importStatusDiv,currentRow)>
</cfloop>

<cfloop list="import,export" index="direction">
	<cfset relaywareObject = direction eq "export"?1:0>

	<cfquery name="getData">
		select
	         object,
	         object_relayware,
	         count (successEntity) as Success,
	         count (distinct case when pendingEntity is not null and failedEntity is null  then pendingEntity else null end) as Pending,
	   		 count (distinct case when failedEntity is not null then failedEntity else null end) as Failure
	   from
		(
		<!--- successfully imported/exported data --->
		select distinct object, m.object_relayware, bfd.entityid as successEntity, null as pendingEntity, null as failedEntity
			from
				connectorObject o
					inner join vConnectorMapping m on #relaywareObject?"m.object_relayware":"m.object_remote"# = o.object and o.relayware=<cf_queryparam value="#relaywareObject#" cfsqltype="cf_sql_bit"> and o.active = 1 and o.synch = 1 and m.isRemoteID=1
					inner join vBooleanFlagData bfd on bfd.name = <cf_queryparam value="#direction#ed" cfsqltype="cf_sql_varchar"> and bfd.entityTypeID = m.entityTypeID and bfd.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
				where o.connectorType = <cf_queryparam value="#connectorType#" cfsqltype="cf_sql_varchar">
		union
		select distinct object, s.entityName as object_relayware, null as successEntity, case when hasError=0 then q.objectID else null end as pendingEntity, case when hasError=1 then q.objectID else null end as failedEntity
			from vConnectorRecords q with (noLock)
				inner join schemaTable s on q.entityTypeID = s.entityTypeID
		where
			q. connectorType = <cf_queryparam value="#connectorType#" cfsqltype="cf_sql_varchar">
			and q.direction=<cf_queryparam value="#left(direction,1)#" cfsqltype="cf_sql_varchar"> and q.queueDateTime between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
			and q.queueStatus != 'DeletedData'
		union
		select o.object, object_relayware, null, null, null
		from connectorObject o
			inner join vConnectorMapping m on #relaywareObject?"m.object_relayware":"m.object_remote"# = o.object and o.relayware=<cf_queryparam value="#relaywareObject#" cfsqltype="cf_sql_bit"> and m.isRemoteID=1 and o.active=1 and o.synch = 1
		where o.connectorType = <cf_queryparam value="#connectorType#" cfsqltype="cf_sql_varchar">
		) as entities
		group by object,object_relayware
		order by object
	</cfquery>

	<cfset variables["get#Direction#Data"] = getData>

	<cfsavecontent variable="dashboardCell">
		<cfoutput>
			<div class="direction"><h1><cfoutput>phr_connector_#direction#</cfoutput></h1></div>
			<div class="statusReport">
			<cf_tableFromQueryObject
				queryObject="#getData#"
				queryName="getData"
				showTheseColumns="Object,Success,Pending,Failure"
				hideTheseColumns=""
				columnTranslation="false"
				allowColumnSorting="no"
				HidePageControls="true"
				useInclude="false"
				keyColumnList="Success,Pending,Failure"
				keyColumnKeyList=" , , "
				keyColumnURLList=" , , "
				keyColumnOnClickList="javascript:openNewTab('##Object##Success#direction#'*comma'#connectorType# Success: ##Object## #direction#'*comma'/connector/connectorStatusReport.cfm?entityType=##object_relayware##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&direction=#direction#'*comma{reuseTab:true*commaiconClass:'connector'});return false;,javascript:openNewTab('##Object##Pending#direction#'*comma'Pending: ##Object## #direction#'*comma'/connector/connectorStatusReport.cfm?entityType=##object_relayware##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&dashboardStatus=Pending&direction=#direction#'*comma{reuseTab:true*commaiconClass:'connector'});return false;,javascript:openNewTab('##Object##Failure#direction#'*comma'Failures: ##object## #direction#'*comma'/connector/connectorStatusReport.cfm?entityType=##object_relayware##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&direction=#direction#&dashboardStatus=Failed'*comma{reuseTab:true*commaiconClass:'connector'});return false;"
			>
			</div>
		</cfoutput>
	</cfsavecontent>

	<cfset variables["#Direction#DashboardCell"] = dashboardCell>
</cfloop>
<!--- 2015/11/30 SB Bootstrap --->
<div id="dashboard">
	<div id="datePickerCell">
		<form name="dateForm" id="dateForm" method="post">
			<div class="grey-box-top">
				<div class="row margin-bottom-15" id="datePickerConnector">
					<div class="form-group">
						<div class="col-xs-12 col-sm-6">
							<label id="fromTo" <cfif not showLast24Hours>class="selectedDate"</cfif>>From</label>
							<CF_relayDateField currentValue="#form.frmFromDate#" fieldName="frmFromDate" anchorName="createFrom" helpText="" disableFromDate=#request.requestTime# enableRange="true">
						</div>
						<div class="col-xs-12 col-sm-6">
							<label id="fromTo" <cfif not showLast24Hours>class="selectedDate"</cfif>>To</label>
							<CF_relayDateField currentValue="#form.frmToDate#" fieldName="frmToDate" anchorName="createTo" helpText="" disableFromDate=#request.requestTime# enableRange="true">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<cf_input type="submit" value="Go" name="frmSubmit" id="connectorSubmit">
						<span id="hours">
							<a href="?" class="btn btn-primary" id="connectorLast24" <cfif showLast24Hours>class="selectedDate"</cfif>>Last 24 hours</a>
						</span>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-5 col-md-5 dashboardCell">
			<cfoutput>#variables.exportDashboardCell#</cfoutput>
		</div>
		<div class="col-xs-12 col-sm-7 col-md-7 dashboardCell">

			<cf_tableFromQueryObject
				queryObject="#connectorEntities#"
				queryName="connectorEntities"
				showTheseColumns="Object_Remote,Import,LastImportedUTC,ImportStatusDisplay,Object_Relayware,Export,LastExportedUTC,ExportStatusDisplay"
				hideTheseColumns=""
				booleanFormat="Import,Export"
				dateTimeFormat="LastImportedUTC,LastExportedUTC"
				showAsLocalTime="false"
				columnTranslation="true"
				columnTranslationPrefix="phr_connector_#connectorType#_report_"
				allowColumnSorting="no"
				HidePageControls="true"
				useInclude="false"
			>

			<p>
			<cfset requestTimeInUtc = application.com.dateFunctions.requestTimeUTC()>
			<span id="timeNow">Time Now (UTC)</span>: <cfoutput>#DateFormat(requestTimeInUtc,"mmm d, yyyy")# #TimeFormat(requestTimeInUtc, "HH:mm")#</cfoutput>
			</p>

			<cfset scheduledTaskName = "#connectorType# Connector">
			<cfset scheduleLastRunUTC = requestTimeInUtc>

			<p>
				<!--- get last scheduled task log --->
				<cfquery name="getLastScheduledTaskLog">
					select top 1 endTime from scheduledTaskLog l
						inner join scheduledTaskDefinition d on d.scheduledTaskDefID = l.scheduledTaskDefID
					where d.name = <cf_queryparam value="#scheduledTaskName#" cfsqltype="cf_sql_varchar">
						and endTime is not null
					order by scheduledTaskLogID desc
				</cfquery>

				<cfif getLastScheduledTaskLog.recordCount>
					<cfset scheduleLastRunUTC = application.com.dateFunctions.convertServerDateToUTC(date=getLastScheduledTaskLog.endTime)>
					<span id="timeLastRun">Last Run (UTC)</span>: <cfoutput>#DateFormat(scheduleLastRunUTC,"mmm d, yyyy")# #TimeFormat(scheduleLastRunUTC, "HH:mm")#</cfoutput>
				</cfif>
			</p>
			<p>
				<span id="connectorRunning">
					<cfparam name="session.connectedToRemoteSite" default="true">

					<!--- only test connection to site when manually running the dashboard. The refresh does not check the connection, as we don't want to spam SF with query requests. --->
					<cfif not structKeyExists(url,"noTickle")>
						<cfset connectorApiObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorApiObject(connectorType=connectorType)>
						<cfset session.connectedToRemoteSite = connectorApiObject.testConnectionToRemoteSite()>
					</cfif>

					<cfif not session.connectedToRemoteSite>
						<div class="errorblock">Not able to connect to <cfoutput>#connectorType#</cfoutput>. Ensure that the credentials entered in the connector settings are valid.</div>
					<cfelse>
						<cfif application.com.globalFunctions.doesLockExist(lockname=scheduledTaskName)>
							The connector is currently running.
						<cfelseif not application.com.settings.getSetting("connector.scheduledTask.pause")>
							<!--- <cfset lastRunTimeUTC = dateAdd("n",application.com.settings.getSetting("connector.scheduledTask.waitInMinutesBeforeNextRun"),scheduleLastRunUTC)> --->
							<!--- NJH 2016/09/06 - JIRA PROD2016-2232 - get next run from scheduled task --->
							<cfset scheduledTask = application.com.relayAdmin.getScheduledTaskByName(name="#connectorType# Connector")>
							<cfif scheduledTask.status eq "running">
								<cfset nextRunTimeUTC =  application.com.dateFunctions.convertServerDateToUTC(scheduledTask.starttime)>
								The connector is next due to run at <cfoutput>#TimeFormat(nextRunTimeUTC, "HH:mm")#</cfoutput>
							<cfelse>
								<!--- connector task has somehow expired. Wait for housekeeping to kick-start it again. The housekeeping process runs every 5 minutes. --->
								The connector scheduled task is currently expired. It will re-start within the next 5 minutes.
							</cfif>
						</cfif>
					</cfif>
				</span>
			</p>

		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-5 col-md-5 dashboardCell">
			<cfoutput>#variables.importDashboardCell#</cfoutput>
		</div>
		<div class="col-xs-12 col-sm-7 col-md-7 dashboardCell">
			<table class="table table-hover table-striped">
				<tr>
					<th>Summary Report</th>
				</tr>
				<tr>
					<td>
						<cfquery name="getImportSummmaryData" dbtype="query">
							select sum(success) as success,sum(pending) as pending ,sum(failure) as failure from variables.getImportData
						</cfquery>

						<cfquery name="getExportSummaryData" dbtype="query">
							select sum(success) as success,sum(pending) as pending ,sum(failure) as failure from variables.getExportData
						</cfquery>

						<cf_translate phrases="phr_#connectorType#_Export,phr_#connectorType#_Import"/>
						<cfset translations = application.com.relaytranslations.translateListOfPhrases(listOfPhrases="connector_Export,connector_Import").phrases>

						<cfset connectorExportPhrase = translations["connector_Export"].phraseText>
						<cfset connectorImportPhrase = translations["connector_Import"].phraseText>

						<cf_chart format="png" chartwidth="600">
							<cf_chartseries type="horizontalbar" seriesLabel="Total Successful" seriesColor="##55B74E">
								<cf_chartdata item="#connectorExportPhrase#" value="#getExportSummaryData.success neq ''?getExportSummaryData.success:0#">
								<cf_chartdata item="#connectorImportPhrase#" value="#getImportSummmaryData.success neq ''?getImportSummmaryData.success:0#">
							</cf_chartseries>
							<cf_chartseries type="horizontalbar" seriesLabel="Total Pending" seriesColor="##EFDF2D">
								<cf_chartdata item="#connectorExportPhrase#" value="#getExportSummaryData.pending neq ''?getExportSummaryData.pending:0#">
								<cf_chartdata item="#connectorImportPhrase#" value="#getImportSummmaryData.pending neq ''?getImportSummmaryData.pending:0#">
							</cf_chartseries>
							<cf_chartseries type="horizontalbar" seriesLabel="Total Failures" seriesColor="##D8000C">
								<cf_chartdata item="#connectorExportPhrase#" value="#getExportSummaryData.failure neq ''?getExportSummaryData.failure:0#">
								<cf_chartdata item="#connectorImportPhrase#" value="#getImportSummmaryData.failure neq ''?getImportSummmaryData.failure:0#">
							</cf_chartseries>
						</cf_chart>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<cfset application.com.request.setTopHead(pageTitle="#connectorType# Connector Dashboard")>