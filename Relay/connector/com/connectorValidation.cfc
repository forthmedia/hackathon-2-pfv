<!---
  --- connectorValidation
  --- -------------------
  ---
  --- Contains connector validation functions.
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   02/12/15

	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	2017/02/01			RMP			PROD2016-2981 Connector: Country Name Variations in Accordance to the Country Alternatives Table
  --->
<cfcomponent hint="Contains connector validation functions." accessors="true" output="false" persistent="false">

	<cffunction name="init" access="public" output="false" hint="Initialise validation object with variables from the connector">

		<cfloop collection="#arguments#" item="arg">
			<cfif structKeyExists(arguments,arg)>
				<cfset this[arg] = arguments[arg]>
			</cfif>
		</cfloop>

	</cffunction>


	<cffunction name="validation_BusinessRequiredFields" access="public" output="false" hint="Logs any errors where required fields for business rules do not exist" index="10" returnType="struct">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to validate">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table containing the error records">

		<cfset var result = {isOK=true,message="",logResult={}}>
		<cfset var getConnectorRequiredFields = "">

		<!--- This does the business level required fields--->
		<cfquery name="getConnectorRequiredFields">
			select case when '#this.direction#' = 'Import' then column_relayware else column_remote end as columnName
			from vConnectorMapping m
			where required = 1
				and connectorType = <cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
				and '#this.object#' = case when '#this.direction#' = 'Export' then object_relayware else object_remote end
				and active=1
				and #this.direction# = 1
		</cfquery>

		<cfif getConnectorRequiredFields.recordCount>

			<cfset var getRecordsWithMissingRequiredFields = "">
			<cfset var tableMetaData = application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=arguments.tablename)>
			<cfset var getRecordsWithMissingRequiredFieldsResult = "">

			<cfquery name="getRecordsWithMissingRequiredFields" result="getRecordsWithMissingRequiredFieldsResult">
				insert into #arguments.errorTableName# (field,message,value,object,objectId,connectorResponseID)
				select distinct
					case
					<cfloop query="getConnectorRequiredFields">
						when len(isNull(<cf_queryobjectname value="#columnName#">,'')) = 0 then <cf_queryparam value="#columnName#" CFSQLTYPE="cf_sql_varchar" >
					</cfloop>
					end as field, 'Required' as message, null as value,<cf_queryparam value="#this.object#" CFSQLTYPE="CF_SQL_VARCHAR" > as object,
					<cfif this.direction eq "Import">#this.baseEntityObjectKey#<cfelse>#application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)#</cfif> as objectID,
					<cfif listFindNoCase(tableMetaData.columnList,"connectorResponseID")>connectorResponseID<cfelse>null</cfif>
				from #arguments.tablename#
				where (
					<cfloop query="getConnectorRequiredFields">
						len(isNull(#columnName#,'')) = 0 <cfif currentRow neq getConnectorRequiredFields.recordCount> or</cfif>
					</cfloop>
					)
					and isDeleted !=1
			</cfquery>

			<cfset result.logResult=getRecordsWithMissingRequiredFieldsResult>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="validation_UniqueEmails" access="public" output="false" hint="Logs any errors where emails do not meet the unique validation" index="20" returnType="struct">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to validate">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table containing the error records">

		<cfset var result = {isOK=true,message=""}>

		<!--- Unique Email validation --->
		<cfif this.direction eq "import" and this.baseEntity eq "person" and application.com.relayPLO.isUniqueEmailValidationOn().isOn>
			<cfset var logNonUniqueEmailsAsErrors = "">
			<cfset var logNonUniqueEmailsAsErrorsResult = {}>

			<cfquery name="logNonUniqueEmailsAsErrors" result="logNonUniqueEmailsAsErrorsResult">
				declare @personEmails as personEmailTableType

				insert into @personEmails (personId,email,accountTypeID)
				select isNull(t.personID,0),t.email,l.accountTypeID
				from #arguments.tablename# t
					inner join location l on l.locationID = t.locationID

				insert into #arguments.errorTableName# (object,objectId,field,message,value,connectorResponseID)
				select distinct <cf_queryparam value="#this.object#" CFSQLTYPE="CF_SQL_VARCHAR" >,t.#this.baseEntityObjectKey#,'email','NonUniqueEmail',t.email,t.connectorResponseID
				from dbo.getNonUniqueEmails(@personEmails) pe
					inner join #arguments.tablename# t on pe.personID = isNull(t.personID,0) and t.email = pe.email
					inner join location l on l.locationID = t.locationID and l.accountTypeID = pe.accountTypeID
					left join #arguments.errorTableName# e on e.object =  <cf_queryparam value="#this.object#" CFSQLTYPE="CF_SQL_VARCHAR" > and t.personID = e.objectID and e.field = 'email' and e.message = 'NonUniqueEmail'
				where
					e.objectID is null
			</cfquery>

			<cfset result.logResult=logNonUniqueEmailsAsErrorsResult>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="validation_relaywareDBValidation" access="public" output="false" hint="Logs any errors where data does not meet Relayware DB rules" index="100" returnType="struct">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to validate">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table containing the error records">

		<cfset var upsertEntityResult = {isOK=true,message=""}>

		<cfif this.direction eq "import">

			<!--- validate the data that we are about to import - data types, required fields, foreign keys, etc. Don't validate records that are about to be deleted --->
			<cfset upsertEntityResult = this.connectorDAO.upsertEntity(tablename=arguments.tablename,validate=true,whereClause="isDeleted=0")>
			<cfset var consolidateResults = "">

			<cfquery name="consolidateResults">
				insert into #arguments.errorTableName# (object,objectId,field,message,statusCode,value,connectorResponseID)
				select distinct e.object,e.objectId,e.field,
					<!--- RMP 2017-02-01 added case to show appropriate user-friendly message based on message field from entityUpsert --->
					case 
						when e.message = 'REQUIRED_DATA_MISSING' and e.field = 'countryID' then 'phr_sys_connector_validation_countryNotPassed' 
						when e.message = 'REQUIRED_DATA_MISSING' then 'phr_error_required_data_missing' 
						else e.message
					end as message,
					case when e.message = 'REQUIRED_DATA_MISSING' then e.message else '' end as statusCode,
					e.value,v.connectorResponseID
				from #upsertEntityResult.errorResult.tablename# e
					inner join #this.synchDataView# v on v.upsertUniqueID = e.objectID
					left join #arguments.errorTableName# t on t.object = e.object and t.objectId = e.objectID and t.field = e.field and t.message = e.message
				where
					t.objectID is null
			</cfquery>
		</cfif>

		<cfreturn upsertEntityResult>
	</cffunction>

</cfcomponent>