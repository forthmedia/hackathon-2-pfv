﻿<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		connector.cfc
Author:			NJH
Date started:	18-06-2015

Description:	Functions for the connector

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/06/18			NJH			Added unique index on newly created CRM ID
2015/06/24			WAB/NJH		prepareObjectForConnector(). Make sure that newly created CRM ID is added to the del table as well
2015/07/06			NJH			Re-wrote getQueueRecords to get data from new vConnectorRecords view.
2015/07/13			NJH			Add function to remove records in queue that don't have data (deleteQueueItemForNonExistentRecords)
2015/10/09			NJH			Moved some functions in here from the salesforce connector that set the get and set the connector user.
2016/08/31			VSN 		case 451541 adding parameterisation to report for success, as with getQueueRecords, see line 104 and 89
2016/09/13          DAN         Case 451735/PROD2016-2336 - helper functions to update connector queue records to process to support limiting number of items per iteration
2017/02/08          RMP         PROD2016-2769 Added syncNow method to resynch selected IDs from the queue. It checks for locks and if there are no any calls connectorSynch.cfm. 
								Functions removeQueueDependencies(), resetConnectorQueueProcess(), setConnectorQueueRecordsToProcess(), resetErrorStatusForQueuedRecords() were
								updated. Now they can update status also for queue ids in case they are provided.

Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="getQueueRecords" access="public" output="false" returnType="query">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="entityTypeList" type="string" required="false">
		<cfargument name="direction" type="string" required="false">
		<cfargument name="fromDate" type="date" required="false">
		<cfargument name="toDate" type="date" required="false">
		<cfargument name="queueID" type="string" required="false" hint="Can be a list">
		<cfargument name="dashboardStatus" type="string" required="false">
		<cfargument name="returnErrorData" type="boolean" default="false">

		<cfset var getRecordsInQueue = "">
		<cfset var entityTypeIDList = "">
		<cfset var entityTypeID = 0>

		<cfif structKeyExists(arguments,"entityTypeList")>
			<cfloop list="#arguments.entityTypeList#" index="entityTypeID">
				<cfset entityTypeIDList = listAppend(entityTypeIDList,application.com.relayEntity.getEntityType(entityTypeID=entityTypeID).entityTypeID)>
			</cfloop>
		</cfif>

		<cfquery name="getRecordsInQueue">
			select * from (
				select distinct
					r.queueID,
					r.object as record_Type,
					r.objectID as record_ID,
					r.name,
					s.entityName as entity,
					r.RelaywareID as RelaywareID,
					r.remoteID,
					case when r.direction = 'E' then 'Export' else 'Import' end as direction,
					case when r.objectID != 'API' then isDeleted else null end as isDeleted,
					r.lastModified,
					r.synchAttempt,
					hasError as errored,
					case when r.objectID != 'API' then case when r.queueStatus = 'HasDependency' then 1 else 0 end else null end as hasDependency,
					case when r.hasError != 1 then 'Pending' else 'Failed' end as dashboardStatus,
					r.queueStatus,r.connectorResponseID
					<cfif arguments.returnErrorData>,e.message,e.statusCode,e.field,e.value,e.connectorResponseID as errorResponseID</cfif>
				from
					vConnectorRecords r
						inner join schemaTable s on r.entityTypeID = s.entityTypeID
					<cfif arguments.returnErrorData>
						inner join connectorQueueError e on e.connectorQueueID = r.queueID
					</cfif>
				where
					r.connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
					<cfif structKeyExists(arguments,"queueID")>and r.queueID in (<cf_queryparam value="#arguments.queueID#" cfsqltype="cf_sql_integer" list="true">)</cfif>
					<cfif entityTypeIDList neq "">and r.entityTypeID in (<cf_queryparam value="#entityTypeIDList#" cfsqltype="cf_sql_integer" list="true">)</cfif>
					<cfif structKeyExists(arguments,"direction")>and r.direction = <cf_queryparam value="#left(arguments.direction,1)#" cfsqltype="cf_sql_varchar"></cfif>
					<cfif structKeyExists(arguments,"fromDate")>and r.queueDateTime >= <cf_queryparam value="#arguments.fromDate#" cfsqltype="cf_sql_timestamp"></cfif>
					<cfif structKeyExists(arguments,"toDate")>and r.queueDateTime <= <cf_queryparam value="#arguments.toDate#" cfsqltype="cf_sql_timestamp"></cfif>
			)
			as base
			where 1=1
				<cfif structKeyExists(arguments,"dashboardStatus")>and dashboardStatus = <cf_queryparam value="#arguments.dashboardStatus#" cfsqltype="cf_sql_varchar"></cfif>
				<cfinclude template="/relay/templates/tableFromQuery-QueryInclude.cfm">
		</cfquery>

		<cfreturn getRecordsInQueue>
	</cffunction>


	<cffunction name="getDependentRecords" access="public" output="false" returnType="query">
		<cfargument name="queueID" type="numeric" required="true">

		<cfset var getDependentRecord = "">
		<cfquery name="getDependentRecord">
			select dependentOnConnectorQueueID as queueID from connectorQueueDependency where connectorQueueID = <cf_queryparam value="#arguments.queueID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn application.getObject("connector.com.connector").getQueueRecords(queueID=valueList(getDependentRecord.queueID))>
	</cffunction>


	<cffunction name="getErrorRecords" access="public" output="false" returnType="query">
		<cfargument name="queueID" type="numeric" required="true">

		<cfreturn application.getObject("connector.com.connector").getQueueRecords(queueID=arguments.queueID,returnErrorData=true)>
	</cffunction>


	<cffunction name="getPrimaryConnectorObjects" access="public" returnType="query" output="false">
		<cfargument name="object" type="string" required="false">
		<cfargument name="direction" type="string" required="false">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="testMode" type="boolean" default="false">
		<cfargument name="sortOrder" type="string" default="isNull(rwObject.sortOrder,1000)">

		<cfset var getPrimaryObjects = "">

		<cfquery name="getPrimaryObjects">
			select
				m.object_relayware,
				m.object_remote,
				case when rwObject.synch = 1 and rwObject.active = 1 then 1 else 0 end as export,
				case when object.synch = 1 and object.active = 1 then 1 else 0 end as import,
				rwObject.lastSuccessfulSynch as lastExported,
				object.lastSuccessfulSynch as lastImported,
				case when export = 1 then dbo.convertServerDateTimeToUTCDateTime(rwObject.lastSuccessfulSynch) else null end as lastExportedUTC,
				case when import = 1 then dbo.convertServerDateTimeToUTCDateTime(object.lastSuccessfulSynch) else null end as lastImportedUTC,
				rwObject.sortOrder as sortOrder,
				rwObject.synchErrorID as exportErrorID,
				object.synchErrorID as importErrorID
			from vConnectorMapping m
				inner join connectorObject rwObject on rwObject.object = m.object_relayware and rwObject.connectorType = m.connectorType and rwObject.relayware=1
				inner join connectorObject object on object.object = m.object_remote and object.connectorType = m.connectorType and object.relayware=0
			where m.connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and m.isRemoteID = 1
				<cfif not arguments.testMode>
					and m.active=1
				</cfif>
				<cfif structKeyExists(arguments,"object") and arguments.object neq "">
					and (m.object_remote in (<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar" list="true">) or m.object_relayware in (<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar" list="true">))
				</cfif>
				<cfif structKeyExists(arguments,"direction") and listFindNoCase("import,export",arguments.direction)>
					and m.#arguments.direction# = 1
				</cfif>
			order by
				<!--- I'm not using cf_queryObjectName here as I want to pass in a case statement, which is not currently supported --->
				#arguments.sortOrder#
		</cfquery>

		<cfreturn getPrimaryObjects>
	</cffunction>


	<cffunction name="upsertColumnValueMapping" access="public" output="false" hint="Inserts a new column/value mapping for a given connector." returnType="numeric">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object_relayware" type="string" required="true">
		<cfargument name="column_relayware" type="string" required="true">
		<cfargument name="value_relayware" type="string" required="true">
		<cfargument name="value_remote" type="string" required="true">
		<cfargument name="ID" type="numeric" required="false" hint="Connector Column Value Mapping ID">

		<cfset var insertValueMapping = "">

		<cfquery name="insertValueMapping">
			if not exists (
				select 1
				from connectorColumnValueMapping cm
					inner join vConnectorMapping m on cm.connectorMappingID = m.ID
				where m.connectorType=<cf_queryparam value="#trim(arguments.connectorType)#" cfsqltype="cf_sql_varchar">
					<cfif structKeyExists(arguments,"ID") and arguments.ID neq 0>
						and cm.ID = <cf_queryparam value="#arguments.ID#" cfsqltype="cf_sql_integer">
					<cfelse>
						and <cf_queryparam value="#trim(arguments.object_relayware)#" cfsqltype="cf_sql_varchar"> in (m.object_relayware,cast(m.objectID_relayware as varchar(20)))
						and m.column_relayware = <cf_queryparam value="#trim(arguments.column_relayware)#" cfsqltype="cf_sql_varchar">
						and (cm.value_relayware = <cf_queryparam value="#trim(arguments.value_relayware)#" cfsqltype="cf_sql_varchar"> or cm.value_remote = <cf_queryparam value="#trim(arguments.value_remote)#" cfsqltype="cf_sql_varchar">)
					</cfif>
			)
			begin
				insert into connectorColumnValueMapping (connectorMappingID,value_relayware,value_remote,created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson)
				select m.ID,<cf_queryparam value="#trim(arguments.value_relayware)#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#trim(arguments.value_remote)#" cfsqltype="cf_sql_varchar">,
					getDate(),
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					getDate(),
					<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
				from vConnectorMapping m
				where connectorType=<cf_queryparam value="#trim(arguments.connectorType)#" cfsqltype="cf_sql_varchar">
					and <cf_queryparam value="#trim(arguments.object_relayware)#" cfsqltype="cf_sql_varchar"> in (m.object_relayware,cast(m.objectID_relayware as varchar(20)))
					and m.column_relayware = <cf_queryparam value="#trim(arguments.column_relayware)#" cfsqltype="cf_sql_varchar">
			end
			<cfif structKeyExists(arguments,"ID") and arguments.ID neq 0>
			else
			begin
				update connectorColumnValueMapping
				set value_relayware = <cf_queryparam value="#trim(arguments.value_relayware)#" cfsqltype="cf_sql_varchar">,
					value_remote = <cf_queryparam value="#trim(arguments.value_remote)#" cfsqltype="cf_sql_varchar">,
					lastUpdated = getDate(),
					lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">,
					lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
				where
					ID = <cf_queryparam value="#arguments.ID#" cfsqltype="cf_sql_integer">
					and (value_remote != <cf_queryparam value="#trim(arguments.value_remote)#" cfsqltype="cf_sql_varchar"> or value_relayware != <cf_queryparam value="#trim(arguments.value_relayware)#" cfsqltype="cf_sql_varchar">)
			end
			</cfif>

			select @@rowCount as recordsUpserted
		</cfquery>

		<cfreturn insertValueMapping.recordsUpserted[1]>

	</cffunction>


	<cffunction name="deleteColumnValueMapping" access="public" output="false" hint="Deletes a column/value mapping record for a given connector." returnType="numeric">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object_relayware" type="string" required="false">
		<cfargument name="column_relayware" type="string" required="false">
		<cfargument name="value_relayware" type="string" required="false">
		<cfargument name="value_remote" type="string" required="false">
		<cfargument name="connectorMappingID" type="string" required="false" hint="The Connector Mapping ID">
		<cfargument name="excludeIDList" type="string" required="false" hint="The Connector Column Value Mapping ID list to exclude in the delete statement">

		<cfset var deleteValueMapping = "">

		<cfquery name="deleteValueMapping">
			delete connectorColumnValueMapping
			from connectorColumnValueMapping cm
				inner join vConnectorMapping m on cm.connectorMappingID = m.ID
			where connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				<cfif structKeyExists(arguments,"connectorMappingID")>
					and cm.connectorMappingID in (<cf_queryparam value="#arguments.connectorMappingID#" cfsqltype="cf_sql_integer" list="true">)
				<cfelse>
					and <cf_queryparam value="#arguments.object_relayware#" cfsqltype="cf_sql_varchar"> in (m.object_relayware,m.objectID_relayware)
					and m.column_relayware = <cf_queryparam value="#arguments.column_relayware#" cfsqltype="cf_sql_varchar">
					and cm.value_relayware = <cf_queryparam value="#arguments.value_relayware#" cfsqltype="cf_sql_varchar">
					and cm.value_remote = <cf_queryparam value="#arguments.value_remote#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif structKeyExists(arguments,"excludeIDList")>
					and cm.ID not in (<cf_queryparam value="#arguments.excludeIDList#" cfsqltype="cf_sql_integer" list="true">)
				</cfif>

			select @@rowCount as recordsDeleted
		</cfquery>

		<cfreturn deleteValueMapping.recordsDeleted[1]>

	</cffunction>


	<cffunction name="insertConnectorMapping" access="public" output="false" hint="Adds a new mapping record for a given connector.">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object_relayware" type="string" required="true">
		<cfargument name="column_relayware" type="string" required="true">
		<cfargument name="object_remote" type="string" required="true">
		<cfargument name="column_remote" type="string" required="true">
		<cfargument name="direction" type="string" required="false" hint="Can either be import or export">
		<cfargument name="mappingType" type="string" default="field" hint="Can be a value of 'field' or 'defaultValue'">
		<cfargument name="active" type="boolean" default="true">
		<cfargument name="emptyStringAsNull" type="boolean" default="false">

		<cfset var insertMapping = "">

		<cfquery name="insertMapping">
			if not exists (
				select 1
				from vConnectorMapping m
				where m.connectorType=<cf_queryparam value="#trim(arguments.connectorType)#" cfsqltype="cf_sql_varchar">
					and m.object_relayware=<cf_queryparam value="#trim(arguments.object_relayware)#" cfsqltype="cf_sql_varchar">
					and m.column_relayware = <cf_queryparam value="#trim(arguments.column_relayware)#" cfsqltype="cf_sql_varchar">
					and m.object = <cf_queryparam value="#trim(arguments.object_remote)#" cfsqltype="cf_sql_varchar">
					and m.column_remote = <cf_queryparam value="#trim(arguments.column_remote)#" cfsqltype="cf_sql_varchar">
			)

			insert into connectorMapping (connectorObjectID_relayware,connectorObjectID_remote,column_relayware,column_remote,mappingType,active,emptyStringAsNull<cfif structKeyExists(arguments,"direction")>,direction</cfif>)
			select rwObject.ID,object.ID,
				<cf_queryparam value="#trim(arguments.column_relayware)#" cfsqltype="cf_sql_varchar">,
				<cf_queryparam value="#trim(arguments.column_remote)#" cfsqltype="cf_sql_varchar">,
				<cf_queryparam value="#trim(arguments.mappingType)#" cfsqltype="cf_sql_varchar">,
				<cf_queryparam value="#arguments.active#" cfsqltype="cf_sql_bit">,
				<cf_queryparam value="#arguments.emptyStringAsNull#" cfsqltype="cf_sql_bit">
				<cfif structKeyExists(arguments,"direction")>
					,<cf_queryparam value="#left(trim(arguments.direction),1)#" cfsqltype="cf_sql_varchar">
				</cfif>
			from connectorObject rwObject
				join connectorObject object
			where rwObject.connectorType=<cf_queryparam value="#trim(arguments.connectorType)#" cfsqltype="cf_sql_varchar">
				and object.connectorType=<cf_queryparam value="#trim(arguments.connectorType)#" cfsqltype="cf_sql_varchar">
				and rwObject.object=<cf_queryparam value="#trim(arguments.object_relayware)#" cfsqltype="cf_sql_varchar">
				and object.object = <cf_queryparam value="#trim(arguments.object_remote)#" cfsqltype="cf_sql_varchar">
		</cfquery>

	</cffunction>


	<cffunction name="deleteConnectorMapping" access="public" output="false" hint="Deletes a mapping record for a given connector.">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object_relayware" type="string" required="true">
		<cfargument name="column_relayware" type="string" required="true">
		<cfargument name="value_relayware" type="string" required="true">
		<cfargument name="value_remote" type="string" required="true">

		<cfset var deleteMapping = "">

		<cfquery name="deleteMapping">
			delete from vConnectorMapping
			where connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and object_relayware=<cf_queryparam value="#arguments.object_relayware#" cfsqltype="cf_sql_varchar">
				and column_relayware = <cf_queryparam value="#arguments.column_relayware#" cfsqltype="cf_sql_varchar">
				and object = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and column_remote = <cf_queryparam value="#arguments.column_remote#" cfsqltype="cf_sql_varchar">
		</cfquery>

	</cffunction>


	<cffunction name="resetSynchAttemptForModifiedRecords" access="public" output="false" hint="Resets the synchAttempt for records where the field that caused an error has been modified after the error was generated which in turn removes the suspended flag.">

		<cfset var resetSynchAttempt = "">

		<!--- NJH 2016/05/11 join to flag table as the vModRegister table holds the flag name, not the textId, which is what connector mapping stores. --->
		<cfquery name="resetSynchAttempt">
			update connectorQueue
				set synchAttempt=0
			from
				connectorQueue q
				inner join connectorQueueError qe on qe.connectorQueueID = q.ID
				inner join connectorObject o on o.ID = q.connectorObjectID and o.relayware = 1
				inner join vConnectorMapping m on m.objectID_relayware = o.ID and m.column_remote = qe.field
				left join vflagDef f on f.flagTextID = m.column_relayware and f.entityTypeID = m.entityTypeID
				inner join vModRegister mr with (noLock) on mr.recordID = q.objectID and mr.entityTypeID = m.entityTypeID and (mr.field = m.column_relayware or mr.flagID=f.flagID) and mr.modDate > qe.created
			where
				synchAttempt != 0
		</cfquery>

	</cffunction>


	<cffunction name="resetSynchAttempt" access="public" output="false" hint="Resets the synchAttempt for records which in turn removes the suspended flag.">
		<cfargument name="queueIDList" type="string" required="true">

		<cfset var resetSynchAttemptQry = "">

		<cfquery name="resetSynchAttemptQry">
			update connectorQueue
				set synchAttempt=0
			where
				synchAttempt != 0
				<cfif arguments.queueIDList neq "">and ID in (<cf_queryparam value="#arguments.queueIDList#" cfsqltype="cf_sql_integer" list="true">)</cfif>
		</cfquery>

	</cffunction>

	<!--- RMP 2017/02/08 PROD2016-2769 Added synNow method--->
	<cffunction name="syncNow" access="public" output="false" hint="Check if no sync in progress and run sync immediatelly">
		<cfargument name="queueIDList" type="string" required="true">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="entityList" type="string" required="true">

		<cfset resetSynchAttempt(arguments.queueIDList)>
		<cfset connectorType = application.com.settings.getSetting("connector.type")>
		<cfif not application.com.globalFunctions.doesLockExist(lockname="#connectorType# Connector")>
			<cfset qnrQueue = application.getObject("connector.com.connector").getQueueRecords(queueID=arguments.queueIDList)>
			<cfinclude template="/scheduled/connectorSynch.cfm">
			<cfloop query="#qnrQueue#">
				<cfset qnrErrorRecords = getErrorRecords(queueid)>
				<cfif qnrErrorRecords.RecordCount>
					<cfset application.com.relayUI.setMessage(message="phr_sys_connector_resynch_record #record_ID# phr_sys_connector_resynch_failed '#qnrErrorRecords.message#'", type="error")>
				<cfelse>
					<cfset application.com.relayUI.setMessage(message="phr_sys_connector_resynch_record #record_ID# phr_sys_connector_resynch_synched_successfully", type="success")>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset application.com.relayUI.setMessage(message="phr_sys_connector_resynch_synchInProgressNow", type="error")>
		</cfif>
	</cffunction>

	<!--- NJH thought I need this function, but turned out I didn't.. but left it in for now
	<cffunction name="setSynchingStatusForQueuedRecords" access="public" output="false" hint="Sets the synching status for records in the queue">
		<cfargument name="synchStatus" type="string" required="true">
		<cfargument name="queueIDList" type="string" required="true">

		<cfquery name="setSynchingStatus">
			update booleanFlagData
				set flagID = status.flagID
			from booleanFlagData bfd
				inner join vflagDef f on f.flagID = bfd.flagID and f.flagGroupTextID like '%ConnectorSynchStatus'
				inner join vConnectorQueue q on q.objectId = cast(bfd.entityID as varchar) and f.flagTextID != q.object+<cf_queryparam value="#arguments.synchStatus#" cfsqltype="cf_sql_varchar"> and q.direction='E'
				inner join flag status on status.flagGroupID = f.flagGroupID and status.flagTextID like <cf_queryparam value="%#arguments.synchStatus#" cfsqltype="cf_sql_varchar">
			where q.ID in (<cf_queryparam value="#arguments.queueIDList#" cfsqltype="cf_sql_integer" list="true">)

			insert into booleanFlagData(flagID,entityId,createdBy,lastUpdatedBy,lastUpdatedByPerson)
			select bfd.flagID,q.objectID,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personID#
			from
				vConnectorQueue q
					left join (booleanFlagData bfd inner join vflagDef f on f.flagID = bfd.flagID and f.flagGroupTextID like '%ConnectorSynchStatus')
						on q.objectId = cast(bfd.entityID as varchar) and q.direction='E'
			where q.ID in (<cf_queryparam value="#arguments.queueIDList#" cfsqltype="cf_sql_integer" list="true">)
				and bfd.entityID is null
		</cfquery>
	</cffunction>--->


	<cffunction name="getSynchedRecords" output="false" access="public" returnType="query" hint="Returns successfully synched records for a given connector type.">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="entityTypeList" type="string" required="true">
		<cfargument name="direction" type="string" required="false">
		<cfargument name="fromDate" type="date" required="false">
		<cfargument name="toDate" type="date" required="false">

		<cfset var synchedRecords = "">

		<cfquery name="synchedRecords">
			/*Start VSN case 451541*/
			select * from (
			/*END VSN case 451541*/

			select s.entityName as Entity,
				v.name,v.entityId as relaywareID,
				v.crmID as remoteID,
				bfd.lastUpdated as synch_Date,
				case when f.flagTextID like '%exported' then 'Export' else 'Import' end as Direction,
				case when deleted.flagID is not null then 1 else v.deleted end as deleted
			from vEntityName v
				inner join schemaTable s on s.entityTypeId = v.entityTypeID and s.entityName in (<cf_queryparam value="#arguments.entityTypeList#" cfsqltype="cf_sql_varchar" list="true">)
				inner join booleanflagdata bfd with (noLock) on v.entityID = bfd.entityID
				inner join flag f on f.flagid = bfd.flagid
				inner join flagGroup fg on fg.flagGroupID = f.flagGroupID and fg.flagGroupTextID like '%connectorSynchStatus' and fg.entityTypeID = s.entityTypeID
				left join
					(booleanFlagData deleted
						inner join flag fd on deleted.flagId = fd.flagId and fd.flagTextID like 'delete%'
						inner join flagGroup fgd on fgd.flagGroupID = fd.flagGroupID and fgd.flagGroupTextID like 'delete%' ) on deleted.entityID = v.entityID and fgd.entityTypeID = s.entityTypeID
			where
				<cfif structKeyExists(arguments,"direction")>f.flagTextID like '%#arguments.direction#ed'
				<cfelse>f.flagTextId like '%imported' or f.flagTextID like '%exported'
				</cfif>
				<cfif structKeyExists(arguments,"fromDate")>and bfd.lastUpdated >= <cf_queryparam value="#arguments.fromDate#" cfsqltype="cf_sql_timeStamp"></cfif>
				<cfif structKeyExists(arguments,"toDate")>and bfd.lastUpdated <= <cf_queryparam value="#arguments.toDate#" cfsqltype="cf_sql_timeStamp"></cfif>
			/*START VSN case 451541*/
			) as success
			where 1=1
				<cfinclude template="/relay/templates/tableFromQuery-QueryInclude.cfm">
			/*END VSN case 451541*/
		</cfquery>

		<!---VSN case 451541 adding parameterisation to report for success, as with getQueueRecords, see line 104 and 89--->


		<cfreturn synchedRecords>
	</cffunction>


	<cffunction name="getLastSuccessFulSynch" access="public" returnType="query" hint="Returns the last successful synch time for a given connector">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var getLastSynchTime = "">

		<cfquery name="getLastSynchTime">
			select isNull(max(lastSuccessfulSynch),getDate()) as lastSuccessfulSynch,connectorType
			from connectorObject
			where active=1
				and synch=1
			<cfif arguments.connectorType neq "">
				and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
			</cfif>
			group by connectorType
			union
			select distinct getDate() as lastSuccessfulSynch,connectorType
			from connectorObject
				where not exists(
					select 1 from connectorObject where active=1 and synch=1
					<cfif arguments.connectorType neq "">
						and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
					</cfif>
					)
		</cfquery>

		<cfreturn getLastSynchTime>
	</cffunction>


	<cffunction name="getConnectorObject" access="public" output="false" returnType="struct" hint="Returns the details for a given object">
		<cfargument name="object" type="string" required="true">
		<cfargument name="relayware" type="string" default="#left(this.direction,1) eq 'I'?0:1#">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="testMode" type="boolean" default="false">

		<!---
			Returns;
			objectList - the primary object and any other objects that map to the object that the primary object maps to: for example, a location will return location,organisation and a SF opportunity will return opportunity,opportuntiyPartner and opportunityContactRole
			primaryKey - the object primaryKey
			columnList - the list of active columns in the mappings
			fieldColumnList - list of just columns that are of type field
			synchedColumnList - the list of columns that synch in the direction specified. Will omit default values.
			synchedFlagList - a list of flagIds that might be exported. Only really used for the getExportData function
			allowDeletions - whether we allow this object to be deleted
			whereClause - the object whereClause
			parentObject - the name of the parentObject
			active - whether the object is live or not
			synch - whether the object is synching
		 --->

		<cfset var getObject = "">
		<cfset var result = {objectList="",primaryKey="",columnList="",fieldColumnList="",importColumnList="",exportColumnList="",synchedFlagList="",allowDeletions=0,whereClause="",parentObject="",parentKeyField="",active=1,postProcessId=0}>
		<cfset var getObjectInfo = "">
		<cfset var getMappingInfo = "">
		<cfset var getDistinctObjectFields = "">
		<cfset var getBaseObject = "">
		<cfset var getObjSynchingFields = "">
		<cfset var getEntityFlagSynchingFields = "">
		<cfset var getObjectTables = "">

		<cfquery name="getMappingInfo">
			select distinct
				<cfif not arguments.relayware>object_remote<cfelse>object_relayware</cfif> as object,
				<cfif not arguments.relayware>column_remote<cfelse>column_relayware</cfif> as objectColumn,
				<cfif arguments.relayware>object_remote<cfelse>object_relayware</cfif> as mappedbject,
				import,
				export,
				isRemoteID,
				isNull(isNull(f1.flagID,f2.flagID),fhq.flagID) as flagID,
				lower(mappingType) as mappingType
			from vConnectorMapping m
				left join
					(flagGroup fg1 inner join flag f1 on fg1.flagGroupID = f1.flagGroupID) on fg1.flagGroupTextID = m.column_relayware and fg1.entityTypeID = m.entityTypeID
				left join
					(flagGroup fg2 inner join flag f2 on fg2.flagGroupID = f2.flagGroupID) on f2.flagTextID = m.column_relayware and fg2.entityTypeID = m.entityTypeID
				left join
					(flagGroup fgHq inner join flag fhq on fghq.flagGroupID = fhq.flagGroupID) on fhq.flagTextID = m.column_relayware and fghq.entityTypeID = #application.entityTypeID.organisation# and m.entityTypeID=#application.entityTypeID.location#
			where m.connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and <cfif not arguments.relayware>m.object_remote<cfelse>m.object_relayware</cfif> = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				<cfif not testMode>
					and m.active = 1
				</cfif>
		</cfquery>

		<cfquery name="getDistinctObjectFields" dbtype="query">
			select distinct objectColumn from getMappingInfo <!--- where mappingType = 'field' --->
		</cfquery>

		<cfset result.columnList = application.com.globalFunctions.removeEmptyListElements(valueList(getDistinctObjectFields.objectColumn))> <!--- all active columns in the mappings --->

		<cfquery name="getDistinctObjectFields" dbtype="query">
			select distinct objectColumn from getMappingInfo where lower(mappingType) = 'field'
		</cfquery>
		<cfset result.fieldColumnList = application.com.globalFunctions.removeEmptyListElements(valueList(getDistinctObjectFields.objectColumn))> <!--- all active field columns in the mappings --->

		<cfquery name="getBaseObject" dbtype="query">
			select * from getMappingInfo where isRemoteID=1 <!---  column_remote = '#this.objectPrimaryKey#'  --->
		</cfquery>

		<!---
			if this entity/object is not the base object, then find out what is. For example, if getting a mappedObject for an organisation
			but an organisation is not mapped to an object.
		 --->
		<cftry>
		<cfif not getBaseObject.recordCount>
			<cfquery name="getBaseObject">
				select
				<cfif not arguments.relayware>object_remote<cfelse>object_relayware</cfif> as object,
				<cfif not arguments.relayware>column_remote<cfelse>column_relayware</cfif> as objectColumn,
				import,export,isRemoteID
				from vConnectorMapping m
				where connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
					and (
							<cfif arguments.relayware>object_remote<cfelse>object_relayware</cfif> in (#quotedValueList(getMappingInfo.mappedbject)#)
							or
							<cfif not arguments.relayware>object_remote<cfelse>object_relayware</cfif> in (#quotedValueList(getMappingInfo.object)#)
						)
					and isRemoteID=1 <!---  column_remote = '#this.objectPrimaryKey#'  --->
					<cfif not arguments.testMode>
					and active = 1
					</cfif>
			</cfquery>
		</cfif>
		<cfcatch><cfdump var="#arguments#"><cfdump var="#cfcatch#"><cf_abort></cfcatch></cftry>

		<cfset result.primaryKey = not arguments.relayware?getBaseObject.objectColumn:application.entityType[application.entityTypeID[getBaseObject.object[1]]].uniqueKey>

		<cfset var synchDirection = "">

		<!--- when doing an update, we need all the fields that are in the mapping that are going in that particular direction. We only field values, as defaultValues are for create only
			TODO - NJH - I see that we are grabbing objectColumn for both import and export... that shouldn't be the case.. we either should only be grabbing either
				import or export columns, based on the object passed in... but would need time to test, so not doing it now.
		--->
		<cfloop list="import,export" index="synchDirection">
			<cfquery name="getObjSynchingFields" dbtype="query">
				select distinct objectColumn from getMappingInfo where #synchDirection# = 1 and lower(mappingType) = 'field' and objectColumn != ''
			</cfquery>
			<cfset result[synchDirection&"ColumnList"] = valueList(getObjSynchingFields.objectColumn)>
			<cfset result[synchDirection&"UpdateColumnList"] = result[synchDirection&"ColumnList"]>
		</cfloop>

		<cfloop list="import,export" index="synchDirection">
			<!--- when doing a create, we need all the fields that are in the mapping that are going in that particular direction. We want both fields and default values --->
			<cfquery name="getObjSynchingFields" dbtype="query">
				select distinct objectColumn from getMappingInfo where #synchDirection# = 1 and objectColumn != ''
			</cfquery>
			<cfset result[synchDirection&"CreateColumnList"] = valueList(getObjSynchingFields.objectColumn)>
		</cfloop>

		<!--- JIRA BF-295 special case for headquarters field. To get the headquarters set/updated, we need to set the is_HQ field --->
		<cfset var method = "">
		<cfloop list="create,update" index="method">
			<cfif listFindNoCase(result["import#method#ColumnList"],"HQ")>
				<cfset result["import#method#ColumnList"] = listAppend(result["import#method#ColumnList"],"is_HQ")>
			</cfif>
		</cfloop>

		<cfquery name="getEntityFlagSynchingFields" dbtype="query">
			select distinct flagID from getMappingInfo where export = 1 and flagID is not null
		</cfquery>
		<cfset result.synchedFlagList = valueList(getEntityFlagSynchingFields.flagID)>

		<cfset var mappedObjectArgs = {}>
		<cfif not arguments.relayware>
			<cfset mappedObjectArgs.object_remote = arguments.object>
		<cfelse>
			<cfset mappedObjectArgs.object_relayware = arguments.object>
		</cfif>

		<cfquery name="getObjectTables">
			select distinct <cfif not arguments.relayware>object_remote<cfelse>object_relayware</cfif> as object from vConnectorMapping
			where active=1
				and <cfif not arguments.relayware>object_relayware<cfelse>object_remote</cfif> = <cf_queryparam value="#getMappedObject(argumentCollection=mappedObjectArgs)#" cfsqltype="cf_sql_varchar">
				and connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
		</cfquery>
		<cfset result.objectList = valueList(getObjectTables.object)>

		<cfquery name="getObject">
			select co.ID,co.object,co.active,co.allowDeletions,co.whereClause,co.parentObjectID,co.sortOrder,parent.object as parentObject, co.synch,co.postProcessId,
				case when parent.object is not null and co.parentKeyColumn is null then parent.object+'ID' else co.parentKeyColumn end as parentKeyField
			from connectorObject co
				left join connectorObject parent on parent.ID = co.parentObjectID
			where
				co.object = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and co.relayware  = <cf_queryparam value="#arguments.relayware#" cfsqltype="cf_sql_bit"> and co.connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfset result.synch = getObject.synch[1]>
		<cfset result.active = getObject.active[1]>
		<cfset result.allowDeletions = getObject.allowDeletions[1]>
		<cfset result.whereClause = getObject.whereClause[1]>
		<cfset result.parentObject = getObject.parentObject[1]>
		<cfset result.parentKeyField = getObject.parentKeyField[1]>
		<cfset result.postProcessId = getObject.postProcessId[1]>

		<cfreturn result>
	</cffunction>


	<cffunction name="getMappedObject" access="public" output="false" hint="Returns mapping information for a given object" returnType="string">
		<cfargument name="object_remote" type="string" default="">
		<cfargument name="object_relayware" type="string" default="">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="testMode" type="boolean" default="false">

		<cfset var objectToMap = "">
		<cfset var direction = "">

		<cfif arguments.object_remote neq "">
			<cfset objectToMap = arguments.object_remote>
			<cfset direction = "import">
		<cfelseif arguments.object_relayware neq "">
			<cfset objectToMap = arguments.object_relayware>
			<cfset direction = "export">
		</cfif>

		<cfif objectToMap eq "">
			<cfthrow message="An object_remote or object_relayware must be passed in.">
		</cfif>

		<cfset var getMappedObjectQry = "">

		<cfquery name="getMappedObjectQry">
			select
				<cfif direction eq "export">object_remote<cfelse>object_relayware</cfif> as object
			from vConnectorMapping m
			where m.connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and <cfif direction eq "import">m.object_remote<cfelse>m.object_relayware</cfif> = <cf_queryparam value="#objectToMap#" cfsqltype="cf_sql_varchar">
				and isRemoteID = 1
		</cfquery>

		<cfreturn getMappedObjectQry.object[1]>

	</cffunction>


	<cffunction name="getRemoteIDColumnName" access="public" returnType="string" output="false" hint="Returns the name of the column holding the ID of the remote object">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="entityType" type="string" required="true">

		<cfset var mappedObject = getMappedObject(object_relayware=arguments.entityType)>
		<cfreturn getConnectorObject(object=mappedObject,relayware=false).primaryKey>
	</cffunction>


	<cffunction name="getMappedField" access="public" returnType="struct" output="false" hint="Returns the mapped field info for a given entity/object">
		<cfargument name="column_relayware" type="string" default="">
		<cfargument name="column_remote" type="string" default="">
		<cfargument name="object_relayware" type="string" default="">
		<cfargument name="object_remote" type="string" default="">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var getField = "">
		<cfset var result = {field="",export="0",import="0",active=0}>

		<!--- TODO: put this in memory?? --->
		<cfquery name="getField">
			select <cfif arguments.column_relayware neq "">column_remote<cfelse>column_relayware</cfif> as mappedField, export,import,active
			from vConnectorMapping m
			where
				connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar"> and
				<cfif arguments.object_remote neq "">
					object_remote = <cf_queryparam value="#arguments.object_remote#" cfsqltype="cf_sql_varchar">
				<cfelse>
					object_relayware = <cf_queryparam value="#arguments.object_relayware#" cfsqltype="cf_sql_varchar">
				</cfif>
					and
				<cfif arguments.column_relayware neq "">
					column_relayware = <cf_queryparam value="#arguments.column_relayware#" cfsqltype="cf_sql_varchar">
				<cfelse>
					column_remote = <cf_queryparam value="#arguments.column_remote#" cfsqltype="cf_sql_varchar">
				</cfif>
		</cfquery>

		<cfif getField.recordCount>
			<cfset result = {field=getField.mappedField[1],export=getField.export[1],import=getField.import[1],active=getField.active[1]}>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getEntitySynchingStatus" access="public" returnType="struct" output="false" hint="Returns the synching status for a given object">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">

		<cfset var statusList="Imported,Exported,Suspended,Synching">
		<cfset var flagTextID = "">
		<cfset var flagStruct = {}>
		<cfset var result = {status="",statusImage=""}>
		<cfset var imgStruct = {Exported="green",Imported="green",Suspended="red",Synching="orange"}>
		<cfset var synchStatus = "">
		<cfset var getSurplusInfo = queryNew("message")>
		<cfset var statusFound = false>
		<cfset var getSynchingDirection = "">
		<cfset var direction = "">
		<cfset var getMappedInfo = {}>
		<cfset var message = "">
		<cfset var flagSet = "">

		<cfif arguments.entityID neq 0>
			<!--- for backwards compatibility until the old connector is not used anymore --->
			<!--- <cfif arguments.connectorType eq "salesforce" and application.com.settings.getSetting("versions.salesforce") neq 2>
				<cfreturn application.com.salesforce.getEntitySynchingStatus(argumentCollection=arguments)>
			</cfif> --->

			<cfloop list="#statusList#" index="synchStatus">
				<cfset flagTextID = synchStatus>
				<cfset flagStruct = application.com.flag.getFlagStructure(flagID=arguments.entityType&flagTextID)>
				<cfif flagStruct.isOK>
					<cfset flagSet = application.com.flag.getFlagData(entityID=arguments.entityID,flagID=flagStruct.flagID)>
					<cfif flagSet.recordCount>
						<cfset statusFound = true>
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>

			<cfif listFindNoCase("Synching,Suspended",synchStatus)>
				<cfset var primaryObjects = getPrimaryConnectorObjects(object=arguments.entityType,direction="export",connectorType=arguments.connectorType)>
				<cfif primaryObjects.recordCount>

					<!--- check to see if this record has an error or Dependency record associated with it.
					TODO: This probably could be using the getQueueRecords function to get the info. --->
					<cfquery name="getSurplusInfo">
						select top 1
							e.message as errorMessage,
							case when q.direction ='I' then 'Import' else 'Export' end as direction,
							case when d.ID is not null then 'phr_connector_dependentOn '+qd.object +'('+ qd.objectID +')' else null end as dependentMessage
						from vConnectorQueue q
							inner join vEntityName v with (noLock) on (q.objectID = cast(v.entityID as varchar) and v.entityTypeID=#application.entityTypeID[arguments.entityType]# and q.object=<cf_queryparam value="#arguments.entityType#" cfsqltype="cf_sql_varchar">) or q.objectID=v.crmID
							left join connectorQueueError e on e.connectorQueueID = q.ID
							left join connectorQueueDependency d on d.connectorQueueID = q.ID
							left join vConnectorQueue qd on qd.ID = d.dependentOnConnectorQueueID
						where v.entityID = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer"> and v.entityTypeID=#application.entityTypeID[arguments.entityType]#
						order by isNull(e.created,d.created) desc
					</cfquery>

					<cfif getSurplusInfo.recordCount>
						<cfif getSurplusInfo.errorMessage[1] neq "">
							<cfset synchStatus = "Suspended">
							<cfset message = getSurplusInfo.errorMessage[1]>
						<cfelse>
							<cfset message = getSurplusInfo.dependentMessage[1]>
						</cfif>
						<cfset direction=getSurplusInfo.direction[1]>
					</cfif>
				</cfif>
			<cfelseif listFindNoCase("Exported,Imported",synchStatus)>
				<cfset direction = listLast(synchStatus,"_")>
			</cfif>

			<cfif statusFound>
				<cfset result.status = flagStruct.name& " ("& direction & " " & dateFormat(flagSet.lastUpdated,"dd-mmm-yyyy") & "  " & timeFormat(flagSet.lastUpdated,"HH:mm:ss") &")">
				<cfif listFindNoCase("Synching,Suspended",synchStatus) and message neq "">
					<cfset result.status = result.status & chr(13)&chr(10)& message>
				</cfif>
				<cfset result.statusImage = "/images/misc/#imgStruct[synchStatus]#dot.gif">
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="deleteErrors" access="public" output="false" hint="Removes the error records for the given Queue IDs">
		<cfargument name="queueIDList" type="string" required="true">

		<cfset var deleteErrorQry = "">

		<cfquery name="deleteErrorQry">
			delete from connectorQueueError <cfif arguments.queueIDList neq "">where connectorQueueID in (<cf_queryparam value="#arguments.queueIDList#" cfsqltype="cf_sql_varchar" list="true">)</cfif>
		</cfquery>
	</cffunction>


	<cffunction name="logConnectorResponse" access="public" ouput="false" hint="Adds an entry into the connectorResponse table" returnType="numeric">
		<cfargument name="sendXML" type="xml" required="true" hint="The query used to retrieve the data">
		<cfargument name="responseXML" type="xml" required="true" hint="The data returned as a result of the query">
		<cfargument name="connectorObject" type="string" default="">
		<cfargument name="method" type="string" default="">
		<cfargument name="relaywareResponse" type="boolean" default="false" hint="Is the call/response to a remote system or within Relayware">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var saveResponse = "">
		<cftry>
			<cfquery name="saveResponse">
				insert into connectorResponse (responseXML,sendXML,connectorObjectId,method)
				values
				(	<cf_queryparam value="#toString(arguments.responseXML)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#toString(arguments.sendXML)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cfif arguments.connectorObject neq "">
						<cfif not isNumeric(arguments.connectorObject)>
						(select Id from connectorObject where connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar"> and object=<cf_queryparam value="#arguments.connectorObject#" cfsqltype="cf_sql_varchar"> and relayware=<cf_queryparam value="#arguments.relaywareResponse#" cfsqltype="cf_sql_bit">)
						<cfelse>
						<cf_queryparam value="#arguments.connectorObject#" cfsqltype="cf_sql_integer">
						</cfif>
					<cfelse>
						null
					</cfif>,
					<cf_queryparam value="#arguments.method#" cfsqltype="cf_sql_varchar">
				)
				select scope_identity() as responseID
			</cfquery>
			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn saveResponse.responseID>

	</cffunction>


	<cffunction name="logAPIError" access="public" output="false" hint="Logs an API error. This shows in the error report under an objectID of API. Might need to be looked at later.">
		<cfargument name="object" type="string" required="true">
		<cfargument name="message" type="string" required="true">
		<cfargument name="method" type="string" required="true">
		<cfargument name="statusCode" type="string" required="true">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfif trim(arguments.object) neq "">
			<cfset deleteAPIError(object=arguments.object,method=arguments.method)>

			<!--- we set the synchAttempt to be the maximum so that this record doesn't get picked up for a synch as it's not really a record--->
			<cfquery name="local.insertAPIQueryIntoConnectorError">
				declare @queueID int
				declare @connectorObjectID int

				select @connectorObjectID = ID from connectorObject where object =  <cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" > and relayware=0 and connectorType=<cf_queryparam value="#arguments.connectorType#" CFSQLTYPE="CF_SQL_VARCHAR" >

				insert into connectorQueue (connectorObjectID,objectID,modifiedDateUTC,synchAttempt,errored) values (@connectorObjectID,'API',getDate(),#application.com.settings.getSetting("connector.maxSynchAttempts")#,1)
				select @queueID = scope_identity()

				insert into connectorQueueError (connectorQueueID,message,field,statusCode) values (@queueID,'#arguments.message#','#arguments.method#','#arguments.statusCode#')
			</cfquery>
		</cfif>

	</cffunction>


	<cffunction name="deleteAPIError" access="public" output="false" hint="Delete an API error which is slightly different than a standard error at the moment.">
		<cfargument name="object" type="string" required="true">
		<cfargument name="method" type="string" required="true">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfquery name="local.deleteAPIErrorInQueue">
			delete connectorQueue
			from
				connectorQueue q
					inner join connectorObject o on o.ID = q.connectorObjectID
					inner join connectorQueueError e on e.connectorQueueID = q.Id
			where o.object =  <cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and q.objectID='API'
				and e.field =  <cf_queryparam value="#arguments.method#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and o.connectorType=<cf_queryparam value="#arguments.connectorType#" CFSQLTYPE="CF_SQL_VARCHAR">
		</cfquery>

	</cffunction>


	<cffunction name="insertConnectorObject" access="public" output="false" returnType="struct" hint="Creates a record in the connectorObject table">
		<cfargument name="object" type="string" required="true">
		<cfargument name="relayware" type="boolean" default="true">
		<cfargument name="active" type="boolean" default="true">
		<cfargument name="synch" type="boolean" default="true">
		<cfargument name="allowDeletions" type="boolean" default="false">
		<cfargument name="whereClause" type="string" required="false">
		<cfargument name="notes" type="string" required="false">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var createResult = application.com.relayEntity.insertEntity(entityDetails=arguments,entityTypeID=application.entityTypeID.connectorObject)>

		<cfif createResult.isOK and arguments.relayware>
			<!--- <cfset createConnectorFlag()>
			<cfset regenerateVEntityName>
			--->
		</cfif>

		<cfreturn createResult>
	</cffunction>


	<!--- <cffunction name="getRemoteObjects" access="public" output="false" returnType="query" hint="Returns a query of objects used to populate the connectorObject table">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfreturn application.com["connector"&arguments.connectorType].getRemoteObjects(argumentCollection=arguments)>
	</cffunction> --->


	<cffunction name="getRemoteObjectFields" access="public" output="false" returnType="query" hint="Returns a query of object fields used to populate the connector mapping table">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object" type="string" required="true" hint="The name of the remote object. Can also be the ID as defined in the connectorObject table.">

		<cfif isNumeric(arguments.object)>
			<cfset arguments.object = getConnectorObjects(connectorType=arguments.connectorType,relayware=0,ID=arguments.object).object[1]>
		</cfif>

		<!--- <cfreturn application.com["connector"&arguments.connectorType].getRemoteObjectFields(argumentCollection=arguments)> --->
		<cfreturn application.getObject("connector.#arguments.connectorType#.com.connector#arguments.connectorType#").getRemoteObjectFields(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getConnectorObjects" access="public" output="false" returnType="query" hint="Returns a query of connector objects" validValues="true">
		<cfargument name="connectorType" type="string" required="false">
		<cfargument name="ID" type="numeric" required="false">
		<cfargument name="relayware" type="boolean" required="false">
		<cfargument name="excludeMappedObjects" type="boolean" default="false" hint="Exclude any objects that are already mapped">
		<cfargument name="active" type="boolean" required="false" hint="Filter to active/non-active objects">

		<cfset var qryConnectorObjects = "">

		<cfquery name="qryConnectorObjects">
			select co.id, co.connectorType,co.relayware,ltrim(rtrim(co.object)) as object,co.notes,co.active,co.synch,co.allowDeletions,co.parentObjectID,co.lastUpdated,co.whereClause,co.postProcessID,
				case when postProcessID is not null then 1 else 0 end as hasPostProcess
			from connectorObject co
			<cfif arguments.excludeMappedObjects>
				left join connectorMapping cm on co.Id in (cm.connectorObjectID_relayware,cm.connectorObjectID_remote)
			</cfif>
			where 1=1
			<cfif structKeyExists(arguments,"connectorType")>
				and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
			</cfif>
			<cfif structKeyexists(arguments,"relayware")>
				and relayware = <cf_queryparam value="#arguments.relayware#" cfsqltype="cf_sql_bit">
			</cfif>
			<cfif structKeyExists(arguments,"id") and arguments.Id neq 0>
				and co.ID = <cf_queryparam value="#arguments.ID#" cfsqltype="cf_sql_integer">
			</cfif>
			<cfif structKeyExists(arguments,"active")>
				and co.active = <cf_queryparam value="#arguments.active#" cfsqltype="cf_sql_bit">
			</cfif>
			<cfif arguments.excludeMappedObjects>
				and cm.ID is null
			</cfif>
		</cfquery>

		<cfreturn qryConnectorObjects>
	</cffunction>


	<cffunction name="getPickListValuesForRemoteObjectColumn" access="public" output="false" hint="Returns a query of picklist values for a given SF object/column" returnType="query">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object" type="string" required="true">
		<cfargument name="column" type="string" required="true">

		<cfset var qryPickListValues = queryNew("name,value","varchar,varchar")>

		<cfif arguments.object neq "">
			<cfset putRemoteObjectMetaDataIntoMemory(connectorType=arguments.connectorType,object=arguments.object)>

			<cfif structKeyExists(application.connector.objectMetaData[arguments.object].fields,arguments.column)>
				<cfset qryPickListValues = application.connector.objectMetaData[arguments.object].fields[arguments.column].pickListValues>
			</cfif>
		</cfif>

		<cfreturn qryPickListValues>
	</cffunction>


	<!--- <cffunction name="getPickListValuesForRemoteObjectColumn" access="public" output="false" hint="Returns a query of picklist values for a given SF object/column" returnType="query">
		<cfargument name="object" type="string" required="true">
		<cfargument name="column" type="string" required="true" hint="The name of the column to get picklist values for.">

		<cfreturn application.com["connector"&arguments.connectorType].getPickListValuesForRemoteObjectColumn(argumentCollection=arguments)>
	</cffunction> --->


	<!--- <cffunction name="sendApiRequest" access="public" output="false" hint="Wrapper to make an api request to remote system" returnType="struct">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="method" type="string" required="true">

		<cfreturn application.com["connector"&arguments.connectorType].sendApiRequest(argumentCollection=arguments)>
	</cffunction> --->


	<cffunction name="isObjectMapped" access="public" returnType="boolean" output="false" hint="Returns whether a mapping exists for a given object">
		<cfargument name="object_remote" type="string" default="">
		<cfargument name="object_relayware" type="string" default="">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfreturn getMappedObject(argumentCollection=arguments) neq ""?true:false>
	</cffunction>


	<cffunction name="getRemoteIDColumn" access="public" output="false" returnType="string" hint="Returns the ID column for a given object">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="object" type="string" required="true">

		<cfset var remoteObjectFields = getRemoteObjectFields(argumentCollection=arguments)>
		<cfset var getIdColumn = "">

		<cfquery name="getIdColumn" dbType="query">
			select name from remoteObjectFields where isId=1
		</cfquery>

		<cfreturn getIdColumn.name>
	</cffunction>


	<cffunction name="createConnectorSynchingFlags" type="public" output="false" hint="Builds the flags necessary for an object to synch">
		<cfargument name="object" type="string" required="true" hint="A relayware object">

		<cfset var flagGroupID = application.com.flag.createFlagGroup(flagType="radio",name="Connector Synch Status",entityType=application.entityTypeID[arguments.object],flagGroupTextID=arguments.object&"ConnectorSynchStatus",NamePhraseTextID="flagGroup_#arguments.object#ConnectorSynch",createDefaultTranslationName=true)>
		<cfset var connectorStatus = "">

		<cfif flagGroupID neq 0>
			<cfloop list="Exported,Imported,Synching,Suspended" index="connectorStatus">
				<cfset application.com.flag.createFlag(flagGroup=flagGroupID,name=connectorStatus,flagType="radio",flagtextID=arguments.object&connectorStatus,namePhraseTextID="flag_#arguments.object##connectorStatus#",createDefaultTranslationName=true)>
			</cfloop>
		</cfif>

	</cffunction>


	<cffunction name="prepareObjectForConnector" access="public" output="false" hint="Creates flags, adds a CRMID column, regenerates views, etc so that the object will work with the connector system">

		<!---
			create connector ID column (ie. crmID)
			regenerate vEntityname
			create connector synching flags - set flagsExists on schemaTable
		 --->

		<cfset createConnectorSynchingFlags(object=arguments.object)>

		<cfset var getConnectorObjectReadyForSynching = "">

		<cfquery name="getConnectorObjectReadyForSynching">
			declare @entityName varchar(100) = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
			declare @crmIDVarname varchar(100) = 'crm'+ upper(left(@entityName,1)) + right(@entityName,len(@entityName)-1) +'ID';

			if not exists(select 1 from information_schema.columns where table_name=@entityName and column_name like 'crm%ID')
			begin
				declare @addColSql nvarchar(max)
				select @addColSql = 'alter table '+@entityName+' add '+ @crmIDVarname +' varchar(50)'
				exec(@addColSql)

				/* WAB 2015/06/24 now add to the del Table, use generate_MRAuditDel because it guarantees that everything is in the correct order */
				exec generate_mrauditDel @entityname

				/* create a uniqueIndex to speed up views and also to enforce uniqueness */
				declare @setIndexSql nvarchar(max)
				select @setIndexSql = 'CREATE unique INDEX '+ @crmIDVarname +'_unique ON ' + @entityName +' ('+@crmIDVarname+') WHERE '+@crmIDVarname+' IS NOT NULL;';
				exec(@setIndexSql)

				select @setIndexSql = 'CREATE INDEX '+ @crmIDVarname +'_idx ON ' + @entityName+'Del' +' ('+@crmIDVarname+')';
				exec(@setIndexSql)
			end

			update schemaTable set flagsExist = 1 where isNull(flagsExist,0) = 0 and entityName=@entityName
			update schemaTable set in_vEntityName = 1 where isNull(in_vEntityName,0) = 0 and entityName=@entityName

			declare @viewName varchar(100)
			set @viewname='v'+@entityName
			<!--- this ensures that the view exists for the entity as the connector uses the view. This also regenerates vEntityName which the connector also uses --->
			exec createFlagView @entityName=@entityName, @viewname=@viewname
		</cfquery>

	</cffunction>


	<cffunction name="removeQueueDependencies" access="public" output="false" hint="Removes dependency records that are more than an hour old">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object" type="string" required="true">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="queueidList" type="string" default="">

		<cfset var deleteDependencyRecords = "">

		<cfquery name="deleteDependencyRecords">
			delete connectorQueueDependency
				from connectorQueueDependency d inner join vConnectorQueue q on d.connectorQueueID = q.id
			where d.created < dateAdd(hh,-1,getDate())
				and q.connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and q.object = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and q.direction = <cf_queryparam value="#left(arguments.direction,1)#" cfsqltype="cf_sql_varchar">
				and q.synchAttempt < <cf_queryparam value="#application.com.settings.getSetting('connector.maxSynchAttempts')#" cfsqltype="cf_sql_integer">
                <cfif ListLen(arguments.queueidList)>
                and q.id in (<cf_queryparam value="#arguments.queueidList#" cfsqltype="cf_sql_integer" list="true">)
                </cfif>				
		</cfquery>

	</cffunction>

    <!--- Case 451735 --->
    <cffunction name="resetConnectorQueueProcess" access="public" output="false" hint="Reset the connector queue records process for the given connector object">
        <cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
        <cfargument name="object" type="string" required="true">
        <cfargument name="direction" type="string" required="true">
        <cfargument name="queueidList" type="string" default="">

        <cfset var qryUpdateConnectorQueueProcess = "">

        <cfquery name="qryUpdateConnectorQueueProcess">
            update vconnectorQueue set process=0
                where process=1
                and object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
                and direction=<cf_queryparam value="#left(arguments.direction,1)#" cfsqltype="cf_sql_varchar">
                and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
                <cfif ListLen(arguments.queueidList)>
                and id in (<cf_queryparam value="#arguments.queueidList#" cfsqltype="cf_sql_integer" list="true">)
                </cfif>
        </cfquery>

    </cffunction>

    <!--- Case 451735 --->
    <cffunction name="setConnectorQueueRecordsToProcess" access="public" output="false" hint="Set the connector queue records to process for the given connector object">
        <cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
        <cfargument name="object" type="string" required="true">
        <cfargument name="direction" type="string" required="true">
        <cfargument name="queueidList" type="string" default="">

        <cfset var maxRecordsToProcessPerBatch = application.com.settings.getSetting("connector.maxRecordsToProcessPerBatch")>
        <cfset var qryUpdateConnectorQueueProcess = "">
        <cfquery name="qryUpdateConnectorQueueProcess">
            update connectorQueue set process=1 where ID in (
            select top #maxRecordsToProcessPerBatch# cq.ID from connectorQueue cq inner join vConnectorRecords cr on cr.queueID=cq.ID
                where cr.process=0
                and cr.object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
                and cr.direction=<cf_queryparam value="#left(arguments.direction,1)#" cfsqltype="cf_sql_varchar">
                and cr.connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
                and cr.queueStatus = 'synching'
                <cfif ListLen(arguments.queueidList)>
                and cq.id in (<cf_queryparam value="#arguments.queueidList#" cfsqltype="cf_sql_integer" list="true">)
                </cfif>                
                order by cr.hasError, cr.queueDateTime
            )
        </cfquery>

    </cffunction>

	<cffunction name="resetErrorStatusForQueuedRecords" access="public" output="false" hint="Resets the error status for queue items so that they become eligible for synching again">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object" type="string" required="true">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="queueidList" type="string" default="">

		<cfset var removeErrorStatusForQueuedRecords = "">

		<cfquery name="removeErrorStatusForQueuedRecords">
			update vConnectorQueue set errored=0
			where object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and direction = <cf_queryparam value="#left(arguments.direction,1)#" cfsqltype="cf_sql_varchar">
				and errored=1
				and objectID != 'API'
				and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
                <cfif ListLen(queueidList)>
                and id in (<cf_queryparam value="#arguments.queueidList#" cfsqltype="cf_sql_integer" list="true">)
                </cfif>
		</cfquery>

	</cffunction>


	<cffunction name="insertRecordIntoConnectorQueue" access="public" output="false" hint="Inserts a record into the queue if it does not already exist">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objectID" type="string" required="true">

		<cfset var insertRecord = "">

		<cfquery name="insertRecord">
			declare @connectorObjectID int
			select @connectorObjectID = Id from connectorObject where relayware=<cf_queryparam value="#arguments.direction eq 'export'?true:false#" cfsqltype="cf_sql_bit">
				and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and object = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">

			if not exists (select 1 from connectorQueue where connectorObjectID = @connectorObjectID and objectID = <cf_queryparam value="#arguments.objectID#" cfsqltype="cf_sql_varchar">)
			insert into connectorQueue(connectorObjectID,objectId,modifiedDateUTC)
				values (@connectorObjectID,<cf_queryparam value="#arguments.objectID#" cfsqltype="cf_sql_varchar">,getDate())
		</cfquery>
	</cffunction>


	<!--- remove records from queue that are not in as deleted but don't have data (ie. are deleted) and are no longer a dependency... bit of a clean up function 
			RT-159 2017-01-18 WAB This query failed if the records to be deleted from the connectorQueue table are also referenced by connectorQueueID column of connectorQueueDependency table.
									(note that the left join is to connectorQueueDependency.dependentOnConnectorQueueID)
					NJH advised that we could go ahead and delete these items from connectorQueuedepency before deleting from connectorQueue, so have adjusted the query accordingly
			
	--->
	<cffunction name="deleteQueueItemForNonExistentRecords" access="public" output="false" hint="Delete records in the queue where they have no data (ie. they have been deleted) and where they are not a dependency">

		<cfset var deleteRecordsQry = "">

		<cfquery name="deleteRecordsQry">
			declare @connectorQueueRecordsToDelete as table (ID int)
			
			insert into @connectorQueueRecordsToDelete
			select q.ID 
			
			from
				connectorQueue q
					left join connectorQueueDependency cqd on cqd.dependentOnConnectorQueueID = q.ID
			where
				1=1				
				and q.dataExists = 0
				and q.isDeleted = 0
				and cqd.Id is null

			delete 
				connectorQueueDependency
			from 
				connectorQueueDependency cqd
				inner join
				@connectorQueueRecordsToDelete del on cqd.connectorQueueID = del.ID

			delete
				connectorQueue
			from 
				connectorQueue cq
				inner join
				@connectorQueueRecordsToDelete del on cq.ID = del.ID
		</cfquery>
	</cffunction>


	<cffunction name="getDataForIDFromConnectorResponse" output="false" access="public" hint="Gets the data from the connectorResponse table showing what the state of the data was at the time of sending" returnType="query">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="connectorResponseID" type="numeric" required="true">

		<cfreturn application.getObject("connector.#arguments.connectorType#.com.connector#arguments.connectorType#").getDataForIDFromConnectorResponse(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getConnectorResponse" output="false" access="public" hint="Returns data from connectorResponse table" returnType="query">
		<cfargument name="connectorResponseID" type="numeric" required="true">
		<cfargument name="justMetaData" type="boolean" default="false">

		<cfset var getResponse = "">

		<cfquery name="getResponse">
			select
				o.object
				, case when o.relayware=1 then 'Export' else 'Import' end as direction
				, r.method
				, r.connectorObjectId
				<cfif not justMetaData>
				, r.sendXml
				, r.responseXML
				</cfif>
			from connectorResponse r
				left join connectorObject o on o.ID = r.connectorObjectID
			where r.Id = <cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn getResponse>
	</cffunction>


	<cffunction name="getConnectorResponseMetaData" output="false" access="public" hint="Returns metadata from connectorResponse table" returnType="query">
		<cfargument name="connectorResponseID" type="numeric" required="true">

		<cfreturn getConnectorResponse(connectorResponseID = connectorResponseID, justMetaData = true)>
	</cffunction>


	<cffunction name="setConnectorUserAsCurrentUser" type="private" hint="Sets the current user as the API user" output="false">
		<cfargument name="connectorType" type="string" required="true">

		<cfset var connectorUser = getConnectorUser(connectorType=arguments.connectorType)>

		<cfif not connectorUser.exists>
			<cfthrow message = "Could not find user with firstname of '#connectorUser.firstname#' and a lastname of '#connectorUser.lastname#' in the '#connectorUser.userGroupName#' usergroup.">
		</cfif>

		<cfset request.relayCurrentUser.personID = connectorUser.personID>
		<cfset request.relayCurrentUser.userGroupID = connectorUser.userGroupID>
	</cffunction>


	<cffunction name="getConnectorUser" type="private" hint="Gets the Connector User based on naming convention" returntype="struct" output="false">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var getConnectorUserID = "">
		<cfset var apiUserGroupName = "#arguments.connectorType# API">
		<cfset var result = {firstname=arguments.connectorType,lastname="API",userGroupName=apiUserGroupName,exists=0,personId=0,userGroupId=0}>

		<cfquery name="getConnectorUserID">
			select p.personID, ug.userGroupID
			from person p with (noLock)
				inner join userGroup ug with (noLock) on p.personID = ug.personID
			where p.firstname=<cf_queryparam value="#result.firstname#" cfsqltype="cf_sql_varchar">
				and p.lastname=<cf_queryparam value="#result.lastname#" cfsqltype="cf_sql_varchar">
				and ug.name = <cf_queryparam value="#apiUserGroupName#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfset result.exists = getConnectorUserID.recordCount>
		<cfif result.exists>
			<cfset result.personID = getConnectorUserID.personID>
			<cfset result.userGroupID = getConnectorUserID.userGroupID>
		</cfif>

		<cfreturn result>
	</cffunction>



	<cffunction name="putRemoteObjectMetaDataIntoMemory" access="public" output="false" hint="Puts object meta data into memory">
		<cfargument name="object" type="string" required="true" hint="The remote object name">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfif not structKeyExists(application,"connector") or not structKeyExists(application.connector,"objectMetaData")>
			<cfset application.connector.objectMetaData = structNew()>
		</cfif>

		<cfif not structKeyExists(application.connector.objectMetaData,arguments.object)>
			<cfset var remoteObjectsResult = sendApiRequest(connectorType=arguments.connectorType,method="getRemoteObjects")>

			<cfif remoteObjectsResult.isOK>
				<cfset var getObjectMetaData = "">

				<cfquery name="getObjectMetaData">
					select * from #remoteObjectsResult.successResult.tablename# where name=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				</cfquery>

				<cfset application.connector.objectMetaData[arguments.object] = application.com.structureFunctions.queryToStruct(query=getObjectMetaData,key="name")[arguments.object]>
			</cfif>

			<cfset var metaDataResult = sendApiRequest(connectorType=arguments.connectorType,object=arguments.object,method="getObjectFieldMetaData")>

			<cfif metaDataResult.isOK>
				<!--- convertObjectFieldMetaDataToRwQuery should return the following columns:
					createable,updateable,length,name,type,nillable,pickListValues,hasPickListValues,dataType

					PicklistValues column must contain an xml like the following:
						<pickList><picklistValues><label>British Pound</label><value>GBP</value></picklistValues><picklistValues><label>Canadian Dollar</label><value>CAD</value></picklistValues></pickList>
				--->
				<cfset var fieldMetaDataQry = application.getObject("connector.#arguments.connectorType#.com.connector#arguments.connectorType#").convertObjectFieldMetaDataToRwQuery(argumentCollection=metaDataResult.successResult,object=arguments.object)>

				<cfset var getColumnsWithPicklistValues = "">
				<cfquery name="getColumnsWithPicklistValues" dbtype="query">
					select name,pickListValues from fieldMetaDataQry where pickListValues is not null
				</cfquery>

				<cfset application.connector.objectMetaData[arguments.object].fields = application.com.structureFunctions.queryToStruct(query=fieldMetaDataQry,key="name")>

				<cfset var pickListQuery = queryNew("")>
				<cfset var pickListStruct = structNew()>

				<!--- go through elements that have picklist values and add a query of picklists....store as query rather than xml. --->
				<cfloop query="getColumnsWithPicklistValues">
					<cfset pickListStruct = application.com.xmlFunctions.ConvertXmlToStruct(xmlNode=xmlParse(pickListValues),str=structNew())>
					<cfif isArray(pickListStruct.pickListValues)>
						<cfset pickListQuery = application.com.structureFunctions.arrayOfStructuresToQuery(theArray=pickListStruct.pickListValues,str=structNew())>
					<cfelse>
						<cfset pickListQuery = application.com.structureFunctions.structToQuery(struct=pickListStruct)>
					</cfif>

					<cfquery name="pickListQuery" dbtype="query">
						select [value],label from pickListQuery order by label
					</cfquery>

					<cfset application.connector.objectMetaData[arguments.object].fields[name].pickListValues = pickListQuery>
				</cfloop>
			<cfelse>
				<cfif reFindNoCase("INVALID_TYPE",metaDataResult.message)>
					<!--- <cfreturn false> --->
				</cfif>
			</cfif>
		</cfif>
	</cffunction>


	<cffunction name="putRemoteObjectsIntoMemory" access="public" output="false" hint="Puts the name of the remote objects into a query and puts it into memory">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="UI" default="false">

		<cfset var qryRemoteObjects = queryNew("label,name")>

		<!---<cfif not structKeyExists(application,"connector") or not structKeyExists(application.connector,"objects")>--->
			<cfset application.connector.objects = qryRemoteObjects>
		<!---</cfif>--->

		<cfif not application.connector.objects.recordCount>
			<cfset var remoteObjectsResult = sendApiRequest(connectorType=arguments.connectorType,method="getRemoteObjects", UI=arguments.UI)>

			<cfif remoteObjectsResult.isOK>
				<!--- The convertObjectDataToRwQuery function should return a query with two columns, name and label --->
				<cfset qryRemoteObjects = application.getObject("connector.#arguments.connectorType#.com.connector#arguments.connectorType#").convertObjectDataToRwQuery(argumentCollection=remoteObjectsResult.successResult, UI=arguments.UI)>
			</cfif>

			<cfset application.connector.objects = qryRemoteObjects>
		</cfif>
	</cffunction>


	<cffunction name="getRemoteObjects" access="public" output="false" returnType="query" hint="Returns a query of objects used to populate the connectorObject table">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="UI" default="false">

		<cfset putRemoteObjectsIntoMemory(connectorType=arguments.connectorType, UI=arguments.UI)>
		<cfreturn application.connector.objects>
	</cffunction>


	<cffunction name="sendApiRequest" access="public" output="false" hint="Wrapper to make an api request to remote system" returnType="struct">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var connectorAPIObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorAPIObject(connectorType=arguments.connectorType)>
		<cfreturn connectorAPIObject.send(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getRemoteOwnerField" access="public" output="false" hint="Returns the name of the user field for a given object based on the import transformation view" returnType="query">
		<cfargument name="connectorType" type="string" required="true" hint="The connector type">
		<cfargument name="object" type="string" required="true" hint="The name of the remote object">

		<!--- <cfset var viewMetaData = application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=arguments.synchDataImportView)>
		<cfset var containsImportUserIndex = listContainsNoCase(viewMetaData.columnList,"importUser#application.delim1#")>
		<cfset var remoteOwnerField = "">
		<cfif containsImportUserIndex>
			<cfset remoteOwnerField = listLast(listGetAt(viewMetaData.columnList,containsImportUserIndex),application.delim1)>
		</cfif> --->
		<cfset var getRemoteOwnerField = "">

		<cfquery name="getRemoteOwnerField">
			select column_remote as owner from vConnectorMapping
			where
				connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and object_remote = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and active=1
				and column_relayware like '%Acc%Manager%'
		</cfquery>

		<cfreturn getRemoteOwnerField>
	</cffunction>


	<cffunction name="getUserMatchingData" access="public" hint="Returns the column names of the user object that we will need for matching to exsiting Relayware Users. This includes both location and person fields." returnType="struct">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfreturn application.getObject("connector.#arguments.connectorType#.com.connector#arguments.connectorType#").getUserMatchingData()>
	</cffunction>


	<cffunction name="getOrganisationFieldsFromLocationFields" type="public" output="false" returnType="struct" hint="Returns anys organisation fields used to upsert based on location field mappings. Only used when organisations are not directly mapped.">
		<cfargument name="baseEntity" type="string" required="true" hint="The name of the Relayware entity that is currently being exported/imported">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var locOrgFieldsStruct = {}>

		<cfif arguments.baseEntity eq "location">
			<cfset locOrgFieldsStruct = {specificURL="organisationUrl",sitename="organisationName",accountTypeID="organisationTypeID",countryID="countryId",organisationId="organisationID"}>
			<cfset var getActiveLocMappings = "">

			<!--- filter out any columns where the location mapping doesn't exist or is not active --->
			<cfquery name="getActiveLocMappings">
				select column_relayware from vConnectorMapping where object_relayware='location'
					and active=1 and column_relayware  in ( <cf_queryparam value="#structKeyList(locOrgFieldsStruct)#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					and connectorType = <cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
			</cfquery>
			<cfset var activeLocFieldmappings = valueList(getActiveLocMappings.column_relayware)>

			<cfloop collection="#locOrgFieldsStruct#" item="locField">
				<cfif not listFindNoCase(activeLocFieldmappings,locField)>
					<cfset structDelete(locOrgFieldsStruct,locField)>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn locOrgFieldsStruct>
	</cffunction>


	<cffunction name="cleanColumnListForExport" type="public" output="false" hint="Removes any fields from list where not createable or updateable based on the given method" returnType="string">
		<cfargument name="method" type="string" required="true" hint="The name of the method to be performed on the remote system">
		<cfargument name="columnList" type="string" required="true" hint="The list of columns to be exported">
		<cfargument name="remoteObjectKey" type="string" required="true" hint="The name of the ID field for the object to be exported">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object" type="string" required="true" hint="The name of the remote object to be export">

		<cfset putRemoteObjectMetaDataIntoMemory(connectorType=arguments.connectorType,object=arguments.object)>
		<cfset var exportColumnList = arguments.columnList>
		<cfset var remoteObject = arguments.object>
		<cfset var objectKey = arguments.remoteObjectKey>

		<cfscript>
			var filteredStruct = structFilter(application.connector.objectMetaData[remoteObject].fields,function(structKey,structValue,method,exportColumnList) {
		 		 if (listFindNoCase(exportColumnList,arguments.structKey) and ((method eq "update" and (application.connector.objectMetaData[remoteObject].fields[arguments.structKey].updateable or arguments.structKey eq objectKey)) or (method eq "create" and (application.connector.objectMetaData[remoteObject].fields[arguments.structKey].createable and arguments.structKey neq objectKey)))) {
					return true;
		 		 } else {
					return false;
		 		}
			 });
		</cfscript>

		<cfif structCount(filteredStruct)>
			<cfset exportColumnList = structKeyList(filteredStruct)>
		</cfif>
		<cfreturn exportColumnList>
	</cffunction>


	<!--- NJH 2016/09/14 JIRA PROD2016-2178 - get batch size dynmically for related files as we can't exceed 52 MB going across. --->
	<cffunction name="getBatchSizeForExport" access="public" output="false" returnType="numeric" hint="Returns how many records to export in the batch">
		<cfargument name="baseEntity" type="string" required="true" hint="The Relayware object to export">
		<cfargument name="tablename" type="string" required="false" hint="The name of the table holding the data to be exported">
		<cfargument name="rowNumberLastExported" type="numeric" required="true" hint="The row that was last exported">
		<cfargument name="method" type="string" required="true" hint="The method that is being executed.">

		<cfset var batchSize = arguments.method eq "delete"?100:200> <!--- default is to send 200 records at a time --->

		<cfif arguments.baseEntity eq "relatedFile" and arguments.method neq "delete">
			<cfset var getRelatedFileBatchSize = "">

			<!--- we can't export anything more than 52 MB for SF. However, I've set the threshold to 40MB to be on the safe side.(we were still getting the error when this was set as 46. Not sure why)
				 We may have to parameterize this per connector... It must be noted that the Maximum JVM Heap Size in CF administrator needs increasing if java heap size errors are encountered
				 while exporting related files. The test site was set to 2048 MB and this seems to be ok. When set at 1024, we were getting the errors when trying to export 40 MB of related file data. --->
			<cfquery name="getRelatedFileBatchSize">
				select count(1) as batchSize from (
					select
						<!--- this works out how many bytes the file body is (ie. the file size). SF doesn't allow anything over 52 MB to be sent across over the API so we need to break it up into chunks that can be sent --->
						(select sum((3*len(body))/4) from #arguments.tablename# d where d.rowNumber <= d2.rowNumber and d.rowNumber > <cf_queryparam value="#arguments.rowNumberLastExported#" cfsqltype="cf_sql_integer">)  as cumulativeSum
					from #arguments.tablename# d2 where rowNumber > <cf_queryparam value="#arguments.rowNumberLastExported#" cfsqltype="cf_sql_integer">
				) t where cumulativeSum  <= 40000000
			</cfquery>

			<!--- assume that we have to try at least one record. If for whatever reason a file is bigger than 40 MB, then hopefully it will throw an error and we move on. --->
			<cfset batchSize = getRelatedFileBatchSize.batchSize neq 0?getRelatedFileBatchSize.batchSize:1>
		</cfif>

		<cfreturn batchSize>
	</cffunction>


	<cffunction name="getFieldsToNull" access="public" output="false" returnType="string" hint="Returns a list of fields for a remote object that have to go across as null if they are an empty string">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object" type="string" required="true" hint="The name of the remote object">

		<cfset var getFieldsToNullQry = "">

		<cfquery name="getFieldsToNullQry">
			select column_remote from vConnectorMapping
			where connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and emptyStringAsNull=1
				and active=1
				and mappingType='field'
		</cfquery>

		<cfreturn valueList(getFieldsToNullQry.column_remote)>
	</cffunction>


	<!--- NJH 2016/10/17 PROD2016-2540 - function to return whether a mapping is using mapped values --->
	<cffunction name="doesMappingHaveMappedValues" access="public" output="false" returnType="boolean" hint="Does a mapping have mapped values">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object_relayware" type="string" required="true" hint="The name of the relayware object">
		<cfargument name="column_relayware" type="string" required="true" hint="The name of the relayware column">

		<cfset var getMappedValues = "">
		<cfquery name="getMappedValues">
			select 1 from vConnectorMapping cm
				inner join connectorColumnValueMapping cvm on cm.Id = cvm.connectorMappingId
			where cm.connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="varchar">
				and cm.object_relayware=<cf_queryparam value="#arguments.object_relayware#" cfsqltype="varchar">
				and cm.column_relayware=<cf_queryparam value="#arguments.column_relayware#" cfsqltype="varchar">
		</cfquery>

		<cfreturn getMappedValues.recordCount?true:false>
	</cffunction>

</cfcomponent>