<!---
  --- connectorUtilities
  --- ------------------
  ---
  --- A set of functions that support the connector.
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   03/12/15
  --->
<cfcomponent hint="A set of functions that support the connector." accessors="true" output="false" persistent="false">

	<cffunction name="instantiateConnectorObject" access="public" output="false" hint="Returns a connector object for the connector" returnType="component">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var connectorObject = "">

		<!--- NJH 2016/09/14 JIRA PROD2016-2335 look for a customised extension in code\connector directory --->
		<cfif fileExists(application.paths.code&"\connector\#arguments.connectorType#\connector#arguments.connectorType#Object.cfc")>
			<cfset connectorObject = createObject("component","code.connector.#arguments.connectorType#.connector#arguments.connectorType#Object")>
		<!--- for backwards compatibility --->
		<cfelseif fileExists(application.paths.code&"\cftemplates\connector\connector#arguments.connectorType#Object.cfc")>
			<cfset connectorObject = createObject("component","code.cftemplates.connector.connector#arguments.connectorType#Object")>
		<cfelse>
			<cfset connectorObject = createObject("component","relay.connector.#arguments.connectorType#.connector#arguments.connectorType#Object")>
		</cfif>

		<cfreturn connectorObject>
	</cffunction>


	<cffunction name="instantiateConnectorAPIObject" access="public" output="false" hint="Returns a connector API object for the connector" returnType="component">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var connectorAPIObject = "">

		<cftry>
			<cfset connectorAPIObject = createObject("component","relay.connector.#arguments.connectorType#.connector#arguments.connectorType#API")>
			<cfcatch>
				<cfthrow message="Connector API not set up. Error creating 'relay.connector.#arguments.connectorType#.connector#arguments.connectorType#API' object.">
			</cfcatch>
		</cftry>

		<cfreturn connectorAPIObject>
	</cffunction>


	<cffunction name="initialiseResultStructure" access="public" output="false" hint="Initialises a result structure to be used by most functions." returnType="struct">
		<cfargument name="tempTable" type="boolean" default="false" hint="Whether this is a real temporary table or not.">
		<cfargument name="returnErrorTable" type="boolean" default="false" hint="Should an errorResult be returned in the structure to handle errored records.">
		<cfargument name="methodName" type="string" default="#application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction='initialiseResultStructure').function#" hint="Used to help give some idea of where the temp table is being used. More for debug purposes really.">

		<cfset var successResult = {tablename=getTempTableName(methodName=arguments.methodName,tempTable=arguments.tempTable),columnList="",recordCount=0}>
		<cfset var result = {isOk=true,message="",successResult=successResult}>
		<cfif arguments.returnErrorTable>
			<cfset result.errorResult = createErrorTable()>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="createErrorTable" access="public" output="false" hint="Creates a table to contain any errors" returnType="struct">

		<cfset var errorTableName = getTempTableName()>
		<cfset var result = {isOK=true,message="",tablename=errorTableName}>
		<cfset var createErrorsTable = "">
		<cfset var getColumnsAndTypesOfConnectorErrorTable = "">

		<!--- this is used so that we can generate a table that will be exactly like what we are expecting. We are generating a table based on the connectorQueueError table but without some unneeded columns--->
		<cfquery name="getColumnsAndTypesOfConnectorErrorTable">
			select data_type,column_name,is_nullable, character_maximum_length
			from information_schema.columns
			where table_name='connectorQueueError'
				and column_name not in ('ID','connectorQueueID','created')
		</cfquery>

		<cfquery name="createErrorsTable">
			create table #errorTableName# (
				<cfloop query="getColumnsAndTypesOfConnectorErrorTable">
				#column_name# #data_type# <cfif character_maximum_length neq "">(#character_maximum_length#)</cfif> <cfif not is_nullable>not null</cfif>,
				</cfloop>
				object varchar(50),
				objectID varchar(40)
			)
		</cfquery>

		<cfreturn result>
	</cffunction>


	<cffunction name="log_" access="public" output="false" hint="Provides logging for the connector">
		<cfargument name="label" type="string" default="#application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction='log_').function#">
		<cfargument name="logStruct" type="struct" default="#structNew()#" hint="Any type of data can be passed through, although currently we're expecting a structure">
		<!--- <cfargument name="debugLevel" type="numeric" default=0 hint="The debug level">
		<cfargument name="filePath" type="string" required="true" hint="The filepath for the log files"> --->
		<cfargument name="tempTableSuffix" type="string" required="false" hint="The suffix appended to temp tables that identifies them as being of the current run.">
		<cfargument name="debugDetails" type="struct" required="true" hint= "Provides details on debugging, such as the debugLevel and the filepath for log files">

		<cfset var logFunction = application.getObject("log").log_>
		<cfset var copyDataFromTableIntoSnapshotTable = "">
		<cfset var debugTableSuffix = "_connectorDebug">
		<cfset var debugLogStruct = duplicate(arguments.logStruct)>

		<cfparam name="request.connectorlogID" default="0">
		<cfparam name="request.logFiles" default="">

		<cfif not listFindNoCase(request.logFiles,arguments.debugDetails.filePath)>
			<cfset request.logFiles = listAppend(request.logFiles,arguments.debugDetails.filePath)>
		</cfif>
		<cfif structKeyExists(arguments.debugDetails,"tempTableSuffix") and (not structKeyExists(arguments,"tempTableSuffix") or arguments.tempTableSuffix eq "")>
			<cfset arguments.tempTableSuffix =arguments.debugDetails.tempTableSuffix>
		</cfif>

		<!--- clean up old debug tables from previous runs if they exist. We don't want to clean them up here, as that will mean the next run will remove the tables that we are wanting to potentially view.
			There is a housekeeping task that should clean up the tables as it cleans up the debug files.
		 --->
		<!--- <cfif request.connectorLogID eq 0>
			<cfset application.getObject("connector.com.connector").cleanUpTempTables(tableSuffix=debugTableSuffix)>
		</cfif> --->

		<cfset request.connectorlogID++>

		<!--- at level 3/4, create copy of table into temp table --->
		<Cftry>
		<cfif arguments.debugDetails.debugLevel gt 2>
			<cfset debugLogStruct.debug = {successTablename="",errorTablename=""}>
			<cfset var resultType = "">

			<cfif structKeyExists(debugLogStruct,"tablename") and not structKeyExists(debugLogStruct,"successResult")>
				<cfset debugLogStruct.successResult.tablename = debugLogStruct.tablename>
			</cfif>

			<cfloop list="success,error" index="resultType">

				<cfif structKeyExists(debugLogStruct,"#resultType#Result") and structKeyExists(debugLogStruct["#resultType#Result"],"tablename")>

					<cfset var dataTablename = debugLogStruct["#resultType#Result"].tablename>

					<cfif dataTablename neq "">
						<cfset debugTableSuffix = "_connectorDebug">
						<cfset var tablenameInDebug = left(dataTablename,50)>
						<!--- ensure that we have the tempTableSuffix as part of the debug table name, as this help us in clearing it out later, and also give us a unique debug table, as it could be that multiple requests
							generate the same debug connector table
						--->
						<cfif structKeyExists(arguments,"tempTableSuffix") and arguments.tempTableSuffix neq "" and not findNoCase(arguments.tempTableSuffix,tablenameInDebug)>
							<cfset debugTableSuffix = arguments.tempTableSuffix & debugTableSuffix>
						</cfif>
						<cfset var tableSnapshotTablename = "####_#left(resultType,1)##request.connectorlogID#_#tablenameInDebug##debugTableSuffix#"> <!--- length of tablename can only be 128 characters... the suffix, which we want to keep as it identifies which tables can be cleaned up, can be quite long already! ---->

						<cftry>
						<cfquery name="copyDataFromTableIntoSnapshotTable">
							if object_id('<cfif left(dataTablename,1) eq "##">tempdb..</cfif>#dataTablename#') is not null

							select * into #tableSnapshotTablename# from #dataTablename#
						</cfquery>
						<cfcatch><cfdump var="#arguments#"><Cfdump var="#cfcatch#"><cf_abort></cfcatch></cftry>

						<cfset debugLogStruct.debug["#resultType#Tablename"] = tableSnapshotTablename>

					</cfif>
				</cfif>
			</cfloop>
		</cfif>

		<cfset logFunction(label=arguments.label,struct=debugLogStruct,debugDetails=arguments.debugDetails)>
		<cfcatch><cfdump var="#this#"><cfdump var="#cfcatch#"><cf_abort></cfcatch></Cftry>
	</cffunction>


	<cffunction name="addForeignIDColumnToTable" access="public" output="false" returnType="struct" hint="Adds an ID column of the foreign system if it does not yet already exist">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records.">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#">
		<cfargument name="object" type="string" required="true" hint="The object that the table pertains to"> <!--- we are not using the this.object as default as this function can handle any sort of object --->
		<cfargument name="direction" type="string" required="true" hint="The direction that the data is going. (ie. if it has come from a remote system, we want to add a RW ID)"> <!--- we are not using the this.direction as default as this function can contain any sort of direction --->
		<cfargument name="baseEntity" type="string" required="true" hint="The name of the relayware entity that is being processed.">
		<cfargument name="objectPrimaryKey" type="string" required="true" hint="The name of the remote object primary key.">

		<cfset var result = initialiseResultStructure()>
		<cfset result.tableMetaData={}>
		<cfset result.foreignIDColumn="">
		<cfset var getDataResult = "">
		<cfset var IDColumntoAdd = arguments.objectPrimaryKey>
		<cfset var baseEntityObjectKey = application.getObject("connector.com.connector").getMappedField(object_relayware=arguments.baseEntity,column_remote=arguments.objectPrimaryKey).field>
		<cfset var selectIDColumn = baseEntityObjectKey>
		<cfset var joinOn = "entityID">
		<cfset var addColumn = true>
		<cfset var getData = "">
		<cfset var baseEntityPrimaryKey = application.com.relayEntity.getEntityType(entityTypeID=arguments.baseEntity).uniqueKey>

		<cfif arguments.direction eq "import">
			<cfset IDColumntoAdd = baseEntityPrimaryKey>
			<cfset joinOn = "remoteID">
			<cfset selectIDColumn = IDColumntoAdd>
		</cfif>

		<!--- if meta data has been passed through, we can check that we don't already have the column in our table --->
 		<cfif structKeyExists(arguments.tableMetaData,"columnList") and listFind(arguments.tableMetaData.columnList,IDColumntoAdd)>
			<cfset addColumn = false>
		</cfif>

		<cfif addColumn>
			<cfset addColumnToTable(tablename=arguments.tablename,columnName=IDColumntoAdd)>
			<cfset var setIDColumnQry = "">

			<cfquery name="setIDColumnQry">
				update #arguments.tablename#
					set #IDColumntoAdd# = isNull(e.#selectIDColumn#,ed.#selectIDColumn#)
				from #arguments.tablename# t
					left join #arguments.baseEntity# e on
					<cfif joinOn eq "entityID">e.#baseEntityPrimaryKey# = t.#baseEntityPrimaryKey#<cfelse>e.#baseEntityObjectKey# = t.#arguments.objectPrimaryKey#</cfif>
					left join #arguments.baseEntity#Del ed on
					<cfif joinOn eq "entityID">ed.#baseEntityPrimaryKey# = t.#baseEntityPrimaryKey#<cfelse>ed.#baseEntityObjectKey# = t.#arguments.objectPrimaryKey#</cfif>
				where
					t.#IDColumntoAdd# is null
			</cfquery>
		</cfif>

		<cfquery name="getData" result="getDataResult">
			select * from #arguments.tablename#
		</cfquery>

		<cfset result.successResult = getDataResult>
		<cfset result.successResult.tablename = arguments.tablename>

		<cfset result.foreignIDColumn = IDColumntoAdd>
		<cfreturn result>
	</cffunction>


	<cffunction name="getTempTableName" access="public" output="false" returnType="string" hint="Generates a temporary table name.">
		<cfargument name="methodName" type="string" default="#application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction='getTempTableName').function#" hint="Used to help give some idea of where the temp table is being used. More for debug purposes really.">
		<cfargument name="tempTable" type="boolean" default="false" hint="Whether this is a real temporary table or not."> <!--- this was set to true, but for some reason, we were losing the temp tables and we were getting errors saying that the object did not exist. So, am now using standard tables --->
		<cfargument name="tempTableSuffix" type="string" default="#structKeyExists(request,'tempTableSuffix')?request.tempTableSuffix:''#" hint="Table suffix, to identify a set of temp tables as part of a run. If called within connector, the request variable should be set">

		<cfreturn "#(arguments.tempTable?'####':'temp_')#connector_#left(replace(createUUID(),"-","","ALL"),20)#_#arguments.methodName##arguments.tempTableSuffix#">
	</cffunction>


	<cffunction name="addColumnToTable" access="public" output="false" hint="Adds a column to a table">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table.">
		<cfargument name="columnName" type="string" required="true" hint="The name of the column to add">
		<cfargument name="defaultValue" type="string" default="null" hint="Value to default the column to">
		<cfargument name="defaultValueFromColumn" type="string" default="" hint="The name of the column to populate date from">

		<cfif arguments.tablename neq "" and arguments.columnName neq "">
			<cfset var isTempTable = true>
			<cfset var addColumn = "">

			<cfif left(arguments.tablename,1) neq "##">
				<cfset isTempTable = false>
			</cfif>

			<cfquery name="addColumn">
				declare @colAdded int = 0
				if not exists (select 1 from <cfif isTempTable>tempdb.</cfif>information_schema.columns where column_name = <cf_queryparam value="#arguments.columnName#" cfsqltype="cf_sql_varchar"> and table_name= <cf_queryparam value="#arguments.tablename#" cfsqltype="cf_sql_varchar">)
				begin
					alter table #arguments.tablename# add #arguments.columnName# varchar(50) <cfif arguments.defaultValue neq "null">not null default '#arguments.defaultValue#'</cfif>
					set @colAdded = 1
				end

				select @colAdded as colAdded
			</cfquery>

			<cfif arguments.defaultValueFromColumn neq "" and addColumn.colAdded>
				<cfset var updateColumnToDefaultValue = "">

				<cfquery name="updateColumnToDefaultValue">
					update #arguments.tablename# set #arguments.columnName# = #arguments.defaultValueFromColumn# where #arguments.columnName# is null
				</cfquery>
			</cfif>
		</cfif>

	</cffunction>


	<cffunction name="cleanUpTempTables" access="public" output="false" hint="Removes temporary tables created with a given suffix">
		<cfargument name="tableSuffix" type="string" required="true">

		<cfset var removeTempTables = "">

		<!--- 2015/02/09 due to an issue where we were losing temp tables (due to possible connection resets), I've created standard temp tables. So, now look for both types of temp tables.
			Also look for tables that are half a day old and remove them as well, in case there are any hanging around
			WAB 2016-09-28 PROD 2016-2400 remove cursor, was apparently causing lock ups.  Might actually be something about the query itself, but we will find that out in due course 
			WAB 2017-01-12 Still seem to be problems with this query.  Perhaps the union between tempdb and main db is a bad idea, try separating
			 --->
		<cfquery name="removeTempTables">
			declare @sqlStatement nvarchar(max)= ''

			select 
				@sqlStatement += 'drop table ' + name + char(10) 
			from 
				tempdb.sys.objects 
			where 
				name  like  <cf_queryparam value="%connector_%#arguments.tableSuffix#" CFSQLTYPE="CF_SQL_VARCHAR" >
			
			exec(@sqlStatement)
			
			set @sqlStatement = ''
			select 
				@sqlStatement += 'drop table ' + name + char(10) 
			from 
				sys.tables 
			where 
				(name  like  <cf_queryparam value="temp_connector_%#arguments.tableSuffix#" CFSQLTYPE="CF_SQL_VARCHAR" > or (name like 'temp_connector%' and create_date < getdate()-.5))
				and type='U'
			exec(@sqlStatement)

		</cfquery>
	</cffunction>


	<cffunction name="getQueryTableMetaData" access="public" output="false" returnType="struct" hint="Returns the meta data for a given query object or table. Used to find out if a table has given columns, etc">
		<cfargument name="tablename" type="string" default="">

		<cfset var tableMetaData = {}>
		<cfset var getData = "">

		<cfif arguments.tablename neq "">
			<cfquery name="getData" result="tableMetaData">
				select top 1 * from #arguments.tablename#
			</cfquery>
		</cfif>

		<cfreturn tableMetaData>
	</cffunction>


	<cffunction name="doesTableContainRecords" access="public" output="false" returnType="boolean" hint="Selects from the table to determine whether the table has any records in it. Used to work out whether we need to continue processing in various parts of the application">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table to check.">
		<cfargument name="whereClause" type="string" default="" hint="Any where clause to determine whether records exist that meet a certain criteria... Used currently to determine if there are records to create on RW">

		<cfset var getTopRecordInTable = "">
		<cfset var hasRecords = false>

		<cfif arguments.tablename neq "">
			<cfquery name="getTopRecordInTable">
				select top 1 1 from #arguments.tablename#
				<cfif arguments.whereClause neq ""> where #arguments.whereClause#</cfif>
			</cfquery>

			<cfset hasRecords = getTopRecordInTable.recordCount>
		</cfif>

		<cfreturn hasRecords>
	</cffunction>


	<cffunction name="getMatchFieldsForExport" access="public" output="false" hint="Returns the fields used for matching on the remote system" returnType="string">
		<cfargument name="object" type="string" required="true" hint="The remote object that we're matching for export">
		<!--- <cfargument name="matchColumns" type="struct" required="true" hint="The Relayware columns to be matched on."> --->
		<cfargument name="entityType" type="string" required="true" hint="The Relayware entityType that we're going to be matching on.">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

		<cfset var remoteMatchFields = "">

		<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
			<cfset var userObjectStruct = application.getObject("connector.com.connector").getUserMatchingData(connectorType=arguments.connectorType)>
			<!--- if the object that is to be matched for export is a user, then set the match field as firstname, lastname and email --->
			<cfif arguments.object eq userObjectStruct.userObjectName>
				<cfset var userObjectFields = userObjectStruct.fieldStruct>
				<cfset remoteMatchFields = "#userObjectFields.firstname#,#userObjectFields.lastname#,#userObjectFields.email#">
			<cfelse>
				<cfset var mappedObject = application.getObject("connector.com.connector").getConnectorObject(object=arguments.entityType,relayware=true)>
				<cfset var entityTablename = "">
				<cfset var rwMatchFields = "">

				<cfloop list="#mappedObject.objectList#" index="entityTablename">
					<!--- in the case of of an organisation and a location, they both have a countryId --->
					<cfset var matchColumns = getRWMatchingFieldsForEntityType(entityType=entityTablename)>
					<cfif not listFindNoCase(rwMatchFields,matchColumns)>
						<cfset rwMatchFields = listAppend(rwMatchFields,matchColumns)>
					</cfif>
				</cfloop>

				<cfset var rwField = "">
				<cfset var remoteField = {}>

				<cfloop list="#rwMatchFields#" index="rwField">
					<cfset remoteField = application.getObject("connector.com.connector").getMappedField(object_remote=arguments.object,column_relayware=rwField)>
					<cfif remoteField.field neq "" and remoteField.active and not listFindNoCase(remoteMatchFields,remoteField.field)>
						<cfset remoteMatchFields = listAppend(remoteMatchFields,remoteField.field)>
					</cfif>
				</cfloop>

				<!--- if parentID is a mapped column, then use it DCC for VSN 2016-07-01 added mappedHQ.export case 450768 --->
				<cfset var mappedHQField = getMappedHeadquartersField()>
				<cfif mappedHQField.field neq "" and mappedHQField.active and mappedHQField.export>
					<cfif listFindNoCase(application.getObject("connector.com.connector").getConnectorObject(object=arguments.object,relayware=false).columnList,mappedHQField.field)>
						<cfset remoteMatchFields = listAppend(remoteMatchFields,mappedHQField.field)>
					</cfif>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn remoteMatchFields>
	</cffunction>


	<cffunction name="getRWMatchingFieldsForEntityType" access="private" output="false" hint="Returns a list of RW fields that is used for matching that we can use for matching on export" returnType="string">
		<cfargument name="entityType" type="string" required="true" hint="The Relayware entityType that we're going to be matching on.">

		<!--- TODO: it would be nice to get these columns from a standard RW place. These should be in settings I understand, although only part of the location matchnames is in settings. Maybe need to move to settings?
			I pass it through for the moment as then every connector gets them passed, so they don't have to worry about calling/creating their own method. But it might be better in a function.
		--->
		<!--- <cfset var matchColumns = {organisation="organisationName,organisationTypeID,countryID",location="sitename,address1,address4,postalCode,organisationID,countryID",person="firstname,lastname,email,locationID"}> --->
		<cfset var matchColumns = {}>
		<cfset matchColumns.organisation = application.com.matching.getEntityMatchFields(entityType="organisation").standard>
		<cfset matchColumns.location = application.com.matching.getEntityMatchFields(entityType="location").standard>
		<cfset matchColumns.person = application.com.matching.getEntityMatchFields(entityType="person").standard>

		<cfset var matchColumnList = "">

		<cfif structKeyExists(matchColumns,arguments.entityType)>
			<cfset matchColumnList = matchColumns[arguments.entityType]>

			<cfloop list="#matchColumns[arguments.entityType]#" index="column">
				<cfif left(column,5) eq "match">
					<cfset var mappedField = application.getObject("connector.com.connector").getMappedField(object_relayware=arguments.entityType,column_relayware=column)>
					<!--- if the matchname field is not mapped, then grab the fields that make up the matchname and add them to the list of fields that we want to use to match. Then remove the matchname field --->
					<cfif mappedField.field eq "" or not mappedField.active>
						<cfset matchColumnList = listAppend(matchColumnList,application.com.matching.getFieldsToPopulateMatchField(entityType=arguments.entityType,matchField=column))>
						<cfset matchColumnList = listDeleteAt(matchColumnList,listFindNoCase(matchColumns[arguments.entityType],column))>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn matchColumnList>
	</cffunction>


	<cffunction name="getMappedHeadquartersField" access="public" output="false" hint="Returns the remote field that is considered the parent account field" returnType="struct">
		<cfreturn application.getObject("connector.com.connector").getMappedField(object_relayware="location",column_relayware="HQ")>
	</cffunction>


	<cffunction name="isHQFieldMapped" access="public" output="false" hint="Returns whether the HQ field is being mapped" returnType="boolean">
		<cfset var mappedHQField = getMappedHeadquartersField()>

		<cfreturn mappedHQField.field neq "" and mappedHQField.active?true:false>
	</cffunction>


	<cffunction name="getQueryFieldListForDataImport" access="public" output="true" returnType="string" hint="Get the list of fields to import for an object">
		<cfargument name="object" type="string" required="true" hint="The remote object">
		<cfargument name="modifiedDataFieldsOnly" type="boolean" required="true" hint="Whether we are just getting the IDs of records that have been modified">

		<cfset var baseEntity = application.getObject("connector.com.connector").getMappedObject(object_remote=arguments.object)>
		<cfset var baseEntityUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=baseEntity).uniqueKey>
		<cfset var remoteObjectKey = application.getObject("connector.com.connector").getRemoteIDColumnName(entityType=baseEntity)>
		<cfset var mappedEntityUniqueKey = getRelaywareIDFieldInExportView(relaywareEntity=baseEntity)>

		<!--- we also need to append the modified date... at the moment, this is done in connector overrides as the field differs on each system. The isDeleted field is also wanted if it's available
			NJH 2016/03/07 JIRA PROD2016-472 - we call getRelaywareIDFieldInExportView above, because the RW ID may not be mapped (in the case of SF attachments where we can't add custom fields to the object). In this case, we want to make sure that the RW ID field is not added.
		--->
		<cfset var queryFieldList = remoteObjectKey>
		<cfif arguments.modifiedDataFieldsOnly and mappedEntityUniqueKey neq baseEntityUniqueKey>
			<cfset queryFieldList = listAppend(queryFieldList,mappedEntityUniqueKey)>
		<cfelseif not arguments.modifiedDataFieldsOnly>
			<cfset queryFieldList = application.getObject("connector.com.connector").getConnectorObject(object=arguments.object,relayware=false).importColumnList>
		</cfif>

		<cfreturn queryFieldList>
	</cffunction>


	<cffunction name="insertRecordsIntoQueue" access="public" output="false" hint="Put records to be processed into the connector queue if they do not already exist." returnType="struct">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to be inserted.">
		<cfargument name="objectColumn" type="string" default="object" hint="The column name holding the object">
		<cfargument name="objectIDColumn" type="variablename" default="objectID" hint="The column name holding the object ID">
		<cfargument name="modifiedDateColumn" type="variablename" default="lastModifiedDate" hint="The column name holding the last modified date">
		<cfargument name="object" type="string" required="true">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="dependantRecords" type="boolean" default="false" hint="Are the records to be inserted dependant records?">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting('connector.type')#">

		<cfset var insertRecordsIntoQueueQry = "">
		<cfset var insertQueueResult = "">
		<cfset var errorsTableName = getTempTableName()>
		<cfset var connectorResponseIDColumnExists = false>

		<cfif not structKeyExists(arguments,"tableMetaData") or not structKeyExists(arguments.tableMetaData,"columnList")>
			<cfset arguments.tableMetaData = getQueryTableMetaData(tablename=arguments.tablename)>
		</cfif>

		<cfif listFindNoCase(arguments.tableMetaData.columnList,"connectorResponseID")>
			<cfset connectorResponseIDColumnExists = true>
		</cfif>

		<!--- sometimes the incoming table has duplicate records, so we get the last modified record and the min connector response....
			WAB 2015-10-13 changed to join to connectorQueue rather than vConnectorQueue because later has some records filtered out
		--->

		<cfquery name="insertRecordsIntoQueueQry" result="insertQueueResult">
			insert into connectorQueue (objectId,connectorObjectID,modifiedDateUTC,synchAttempt,isDeleted<cfif connectorResponseIDColumnExists>,connectorResponseID</cfif>)
			select distinct t.#arguments.objectIDColumn#, co.ID, max(t.#arguments.modifiedDateColumn#), 1, t.isDeleted<cfif connectorResponseIDColumnExists>, min(t.connectorResponseId)</cfif>
			from #arguments.tablename# t
				inner join connectorObject co on co.object=t.#arguments.objectColumn# and relayware =  <cf_queryParam value="#left(arguments.direction,1) eq 'I'?0:1#" cfsqltype="cf_sql_bit"> and connectorType = <cf_queryParam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				left join connectorQueue q on q.objectID = cast(t.#arguments.objectIDColumn# as varchar(50)) and q.connectorObjectID = co.ID
			where q.objectID is null
				and co.connectorType = <cf_queryParam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				and len(t.#arguments.objectIDColumn#) > 0
			group by t.#arguments.objectIDColumn#,co.ID,t.isDeleted
		</cfquery>

		<cfif arguments.dependantRecords>
			<!--- here we are inserting Dependency records, and logging errors for any records that may have dependancies so that we can tell easily that they are not being synched due to a Dependency --->
			<cfset var insertDependantRecordsQry = "">
			<cfset var dependantQueueResult = {}>

			<cfquery name="insertDependantRecordsQry" result="dependantQueueResult">
				declare @currentTime datetime = getDate()

				insert into connectorQueueDependency (connectorQueueID,dependentOnConnectorQueueID,created)
				select distinct dq.ID, q.ID, @currentTime
				from #arguments.tablename# t
					inner join vConnectorQueue q on q.objectID = cast(t.#arguments.objectIDColumn# as varchar(50)) and q.object = t.#arguments.objectColumn# and q.connectorType = <cf_queryParam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
					inner join vConnectorQueue dq on dq.objectID = cast(t.blockedObjectID as varchar(50)) and dq.object = t.blockedObject and q.connectorType = <cf_queryParam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
					left join connectorQueueDependency d on d.connectorQueueID = q.ID and d.dependentOnConnectorQueueID = dq.ID
				where
					d.ID is null
			</cfquery>

			<!--- set the dataExists column for any dependent records, as it may be that a dependency is actually a deleted record and so will just sit in the queue.
				This is done for import in the getDataToImport function as we know at that point that the data doesn't exist --->
			<cfif arguments.direction eq "export">

				<cfset var setDataExistsForDeletedRecords = "">
				<cfquery name="setDataExistsForDeletedRecords">
					update connectorQueue set dataExists = 0
					from #arguments.tablename# t
						inner join vConnectorQueue q on q.direction = 'E' and q.isDeleted = 0 and q.objectID = cast(t.#arguments.objectIDColumn# as varchar(50)) and q.object = t.#arguments.objectColumn# and q.connectorType = <cf_queryParam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
						inner join schemaTable s on s.entityName = q.object
						inner join vEntityName v on cast(v.entityID as varchar(50)) = q.objectID and v.entityTypeID = s.entityTypeID and v.deleted = 1
					where q.dataExists = 1
				</cfquery>
			</cfif>
		</cfif>

		<cfreturn insertQueueResult> <!--- we're returning a struct so that we can continue logging in connectorObject.cfc --->

	</cffunction>


	<cffunction name="getRelaywareIDFieldInExportView" access="public" output="false" returnType="string" hint="Returns either the mapped RelaywareId field for a given object or the actual entity primary key, depending on the mapping.">
		<cfargument name="relaywareEntity" type="string" required="true">

		<cfset var relaywareID = application.com.relayEntity.getEntityType(entityTypeID=arguments.relaywareEntity).uniqueKey>
		<cfset var relaywareIdMapping = application.getObject("connector.com.connector").getMappedField(object_relayware=arguments.relaywareEntity,column_relayware=relaywareID)>

		<!--- if the RW ID is not a mapped/active field, then we will have the actual RW ID as a column in the export view --->
		<cfreturn relaywareIdMapping.field neq "" and relaywareIdMapping.active?relaywareIdMapping.field:relaywareID>
	</cffunction>


	<!--- NJH 2016/09/06 - JIRA PROD2016-2232 - create function to scheduled connector --->
	<cffunction name="scheduleConnectorSynch" access="public" output="false" hint="Schedules the connector scheduled task" type="boolean">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting('connector.type')#">
		<cfargument name="scheduleTime" type="date" default="#dateAdd('s',5,now())#">
		<cfargument name="scheduleTaskPaused" type="boolean" default="#application.com.settings.getSetting('connector.scheduledTask.pause')#" hint="When updating settings, this doesn't yet return the true value.">
		<cfargument name="reset" type="boolean" default="false">

		<cfset var scheduled = false>

		<cfif not arguments.scheduleTaskPaused and not application.com.globalFunctions.doesLockExist(lockname="#arguments.connectorType# Connector")>
			<cfschedule action="update" task="#arguments.connectorType# Connector" url="#request.currentSite.protocolAndDomain#/scheduled/connectorSynch.cfm?connectorType=#arguments.connectorType##arguments.reset?'&reset=true':''#" interval="once" operation="HTTPRequest" startDate="#dateFormat(arguments.scheduleTime,'mm/dd/yy')#" startTime="#arguments.scheduleTime#">
			<cfset scheduled = true>
		</cfif>
		<cfreturn scheduled>
	</cffunction>
</cfcomponent>