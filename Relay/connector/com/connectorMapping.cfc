<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorMapping.cfc
Author:			NJH
Date started:	02-12-2014

Description:	A component designed for the connector mapping edit page

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cfcomponent output="false">

	<cffunction name="getConnectorMappings" access="public" output="false" returnType="query" hint="Returns the mappings for the configuration screen">
		<cfargument name="objectID" type="string" required="true" hint="Either relayware or remote objectId">
		<cfargument name="connectorType" type="string" required="true" hint="The Connector Type">
		<cfargument name="connectorMappingID" type="numeric" default="0" hint="A connector mapping Id">

		<cfset var getConnectorMappingsQry = "">

		<cfquery name="getConnectorMappingsQry">
			select m.ID,m.connectorObjectID_remote,
				m.connectorObjectID_relayware,
				m.column_relayware,
				m.column_remote,
				m.active,
				m.required,
				m.emptyStringAsNull,
				m.mappingType,
				m.canEdit,
				m.notes,
				m.direction,
				m.mappedRelaywareColumn,
				remoteObj.object as object_remote,
				relaywareObj.object as object_relayware,
				case when fg.flagTypeID in (2,3) then 1 else 0 end as hasMappedValues,
				case when cm.ID is not null then 1 else 0 end as isCustomised
			from connectorMapping m
				inner join connectorObject relaywareObj on m.connectorObjectID_relayware = relaywareObj.ID
				inner join connectorObject remoteObj on m.connectorObjectID_remote = remoteObj.ID
				left join ConnectorCustomMapping cm on relaywareObj.object = cm.entityName and cm.columnName=m.column_relayware and cm.connectorType = relaywareObj.connectorType
				left join flagGroup fg on fg.flagGroupTextID = m.column_relayware
			where relaywareObj.connectorType = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.connectorType#">
				and <cf_queryparam value="#arguments.objectID#" cfsqltype="cf_sql_integer"> in (relaywareObj.ID,remoteObj.ID)
			order by case when m.ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.connectorMappingID#"> then 0 else 1 end,isRemoteID desc, ID
		</cfquery>

		<cfreturn getConnectorMappingsQry>
	</cffunction>

	<!--- a function run before a connectorMapping record is saved. --->
	<cffunction name="preSaveConnectorMapping" access="public" returnType="struct" hint="Run some pre-processing before saving the connector mapping record">
		<cfargument name="direction" type="string" required="true" hint="Is either I or E">
		<cfargument name="mappingType" type="string" required="true" hint="Is either DefaultValue or Field">
		<cfargument name="column_remote" type="string" required="false" hint="">
		<cfargument name="column_relayware" type="string" required="false" hint="">
		<cfargument name="defaultValue" type="string" required="true" hint="The default value for either an import or export">
		<cfargument name="fieldCollection" type="struct" required="true" hint="The form values which we can alter">

		<cfset var result = {isOK=true,message=""}>

		<!--- set the column to be the default value if the mapping type is of default value --->
		<cfif arguments.mappingType eq "defaultValue">
			<cfif arguments.direction eq "I">
				<cfset arguments.fieldCollection.column_remote = arguments.defaultValue>
			<cfelse>
				<cfset arguments.fieldCollection.column_relayware = arguments.defaultValue>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- run this function after a connectorMapping record has been saved --->
	<cffunction name="postSaveConnectorMapping" access="public" output="false" hint="Runs some processing after a connector mapping record has been saved. Deals primarily with saving value mappings." returnType="struct">
		<cfset var result = {isOK=true,message=""}>

		<cftry>

			<cfset var mappedValues = application.com.structureFunctions.getStructureKeysByRegExp(struct=arguments,regexp="\AmappedValue_")>
			<cfset var columnValueMappingStruct = {}>
			<cfset var columnValueMappingID = "">
			<cfset var relaywareValue = "">
			<cfset var remoteValue = "">
			<cfset var connectorValueMappingIDList = "">
			<cfset var updateMappedValues = "">
			<cfset var formField = "">
			<cfset var getNumOfColumnValueMappingsPreUpsert = "">

			<!--- mappedvalue fields take on the form of relaywarevalue_connectorValueMappingId_mappedValue --->
			<cfloop list="#mappedValues#" index="formField">
				<cfif listLen(formField,"_") gt 2>
					<cfset columnValueMappingID = listGetAt(formField,2,"_")>
					<cfset relaywareValue = replace(listRest(listRest(formField,"_"),"_"),"__"," ","All")> <!--- Valid Values may have spaces in them, so have replaced spaces with double underscores. Here, reversing the process --->
					<cfset remoteValue = form[formField]>
					<!--- if it's an empty string, we're going to delete the value mapping record --->
					<cfif trim(remoteValue) neq "">
						<cfset columnValueMappingStruct[columnValueMappingID&"_"&relaywareValue] = {remoteValue=remoteValue,relaywareValue=relaywareValue}>
						<cfset connectorValueMappingIDList = listAppend(connectorValueMappingIDList,columnValueMappingID)>
					</cfif>
				</cfif>
			</cfloop>

			<cfif arguments.column_remote eq application.getObject("connector.com.connector").getRemoteIDColumn(connectorType=arguments.connectorType,object=arguments.connectorObjectID_remote)>

				<cfset var setFieldAsRemoteID = "">

				<cfquery name="setFieldAsRemoteID">
					update connectorMapping set isRemoteID=1 where ID=<cf_queryparam value="#arguments.ID#" cfsqltype="cf_sql_integer">
				</cfquery>
			</cfif>

			<cfset var deleteArgs = {connectorMappingID=arguments.ID}>
			<cfif structCount(columnValueMappingStruct)>
				<cfset deleteArgs.excludeIDList = connectorValueMappingIDList>
			</cfif>

			<cfquery name="getNumOfColumnValueMappingsPreUpsert">
				select count(1) as numOfColumnValueMappings from connectorColumnValueMapping where connectorMappingID=<cf_queryparam value="#arguments.ID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfset var numRecordsDeleted = application.getObject("connector.com.connector").deleteColumnValueMapping(argumentCollection=deleteArgs)>
			<cfloop collection="#columnValueMappingStruct#" item="columnValueMappingID">
				<cfset application.getObject("connector.com.connector").upsertColumnValueMapping(connectorType=arguments.connectorType,object_relayware=arguments.connectorObjectID_relayware,column_relayware=arguments.column_relayware,value_relayware=listRest(columnValueMappingID,"_"),value_remote=columnValueMappingStruct[columnValueMappingID].remoteValue,ID=listFirst(columnValueMappingID,"_"))>
			</cfloop>

			<!--- re-generate the views if the mapping has become active or inactive, if it's a new record, or if the columns have changed or if emptyStringAsNull has changed or if we no longer have value mappings or we now do have value mappings --->
			<cfif (structKeyExists(arguments, "active_orig") and structKeyExists(arguments, "active") and arguments.active_orig neq arguments.active)
					or (structKeyExists(arguments, "active_orig") xor structKeyExists(arguments, "active"))
					or arguments.newRecord
					or (structKeyExists(arguments, "column_relayware_orig") and structKeyExists(arguments, "column_relayware") and arguments.column_relayware neq arguments.column_relayware_orig)
					or (structKeyExists(arguments, "column_relayware_orig") xor structKeyExists(arguments, "column_relayware"))
					or (structKeyExists(arguments, "column_remote_orig") and structKeyExists(arguments, "column_remote") and arguments.column_remote neq arguments.column_remote_orig)
					or (structKeyExists(arguments, "column_remote_orig") xor structKeyExists(arguments, "column_remote"))
					or (structKeyExists(arguments, "emptyStringAsNull_orig") and structKeyExists(arguments, "emptyStringAsNull") and arguments.emptyStringAsNull neq arguments.emptyStringAsNull_orig)
					or (structKeyExists(arguments, "emptyStringAsNull_orig") xor structKeyExists(arguments, "emptyStringAsNull"))
					or (numRecordsDeleted neq 0 and structCount(columnValueMappingStruct) eq 0)
					or (getNumOfColumnValueMappingsPreUpsert.numOfColumnValueMappings[1] eq 0 and structCount(columnValueMappingStruct) gt 0)
					or (structKeyExists(arguments, "defaultImportValue_orig") and structKeyExists(arguments, "defaultImportValue") and arguments.defaultImportValue neq arguments.defaultImportValue_orig)
					or (structKeyExists(arguments, "defaultImportValue_orig") xor structKeyExists(arguments, "defaultImportValue"))
					or (structKeyExists(arguments, "defaultExportValue_orig") and structKeyExists(arguments, "defaultExportValue") and arguments.defaultExportValue neq arguments.defaultExportValue_orig)
					or (structKeyExists(arguments, "defaultExportValue_orig") xor structKeyExists(arguments, "defaultExportValue"))>

				<cfset var getConnectorTypeFromId = "">

				<cfquery name="getConnectorTypeFromId">
					select connectorType,object_relayware from vConnectormapping where ID=<cf_queryparam value="#arguments.ID#" cfsqltype="cf_sql_integer">
				</cfquery>

				<cfset application.getObject("connector.com.connectorMapping").recreateConnectorViews(connectorType=getConnectorTypeFromId.connectorType[1],object_relayware=getConnectorTypeFromId.object_relayware[1])>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<!---  --->
	<cffunction name="getRelaywareObjectFields" access="public" output="false" hint="A function to populate the Relayware column dropdown in the connectorMapping edit page." validValues="true" returnType="query">
		<cfargument name="object" type="numeric" required="true">
		<cfargument name="column" type="string" required="true">
		<cfargument name="connectorType" type="string" required="true">

		<cfset var qryRelaywareObjectFields = "">
		<cfset arguments.object = application.getObject("connector.com.connector").getConnectorObjects(connectorType=arguments.connectorType,relayware=1,ID=arguments.object).object[1]>

		<!--- PROD2016-1174 NJH 2016/05/25 - don't show custom fields that have already been mapped. Added extra join to vConnectorMapping for customised fields
			NJH 2016/09/27 PROD2016-1303 - due to changes in vFieldMetaData, had to re-write this query as the displayType column had been removed. Now need to derive
				such values such as reference ID,etc
		 --->
		<cfquery name="qryRelaywareObjectFields">
			select case when ccm.ID is not null then 'Custom: ' + v.name when isNullable = 0 and defaultValue is null then v.name+' *' else v.name end as fieldnameDisplay,
				v.name as fieldnameValue,
				dbo.listGetAt(foreignKey,1,'.') as foreignKeyTable,
				case
					when ccm.ID is not null then 'custom'
					when isPrimaryKey = 1 then 'id'
					when dataType not in ('numeric','bit') and displayAs not like '%select' then lower(displayAs)
					when displayAs = 'select' and foreignKey is not null and picklistValuesSql is null then 'reference' <!--- NJH 2016/12/13 PROD2016-2958 - picklistsql takes precedence over a FK --->
					when displayAs like '%select' then replace(displayAs,'select','picklist')
					when dataType = 'bit' then 'boolean'
					else lower(dataType)
					end
				as dataType
			from
				vFieldMetaData v
					inner join connectorObject co on co.object = v.tablename and co.connectorType=<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.connectorType#"> and co.relayware=1
						and co.object = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.object#">
					left join vConnectorMapping cm on v.tablename=cm.object_relayware and v.name = cm.column_relayware and cm.connectorType = co.connectorType
					left join ConnectorCustomMapping ccm on v.tablename = ccm.entityName and v.name = ccm.columnName
			where
				v.tablename = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.object#">
				and	(cm.id is null
					or v.name = <cf_queryparam value="#arguments.column#" cfsqltype="cf_sql_varchar">)
				and v.name not like 'created%'
				and v.name not like 'lastUpdated%'
			/* grab any fields that are not standard fields but for which we have a mapping for... considered as custom functions */
			union
			select case when ccm.customised=1 then 'Custom: ' else '' end + ccm.columnName as fieldnameDisplay,
			ccm.columnName as fieldNameValue,
			null as foreignKeyTable,
			'custom' as datatype /* use custom so that we keep custom ones in the select box */
			from ConnectorCustomMapping ccm
				left join information_schema.columns c on ccm.entityName = c.table_name and ccm.columnName = c.column_name
				left join vConnectorMapping cm on ccm.entityName=cm.object_relayware and ccm.columnName = cm.column_relayware and cm.connectorType = ccm.connectorType
			where
				ccm.entityName = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and c.column_name is null
				and cm.Id is null
			order by v.name
		</cfquery>

		<!--- display fieldnameDisplay with datatype --->
		<cfloop query="qryRelaywareObjectFields">
			<cfset qryRelaywareObjectFields.fieldnameDisplay = qryRelaywareObjectFields.fieldnameDisplay & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" & qryRelaywareObjectFields.dataType & ")">
		</cfloop>

		<cfreturn qryRelaywareObjectFields>
	</cffunction>


	<cffunction name="getRemoteObjectFields" access="public" output="false" returnType="query" hint="A function to populate the Remote column dropdown in the connectorMapping edit page." validValues="true">
		<cfargument name="object" type="numeric" required="true">
		<cfargument name="column" type="string" required="true">
		<cfargument name="connectorType" type="string" required="true">

		<cfset var qryRemoteFields = application.getObject("connector.com.connector").getRemoteObjectFields(object=arguments.object,connectorType=arguments.connectorType)>
		<cfset var getMappedRemoteObjectFields = "">
		<cfset var qryRemoteObjectFields = "">

		<cfquery name="getMappedRemoteObjectFields">
			select lower(column_remote) as column_remote
			from connectorMapping m
				inner join connectorObject co on co.ID = m.connectorObjectID_remote
			where co.connectorType= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.connectorType#">
				and connectorObjectID_remote = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.object#">
		</cfquery>

		<!--- remove the currentValue from this list --->
		<cfset var mappedFields = application.com.globalFunctions.ListMinusList(list1=valueList(getMappedRemoteObjectFields.column_remote),list2=arguments.column)>

		<cfquery name="qryRemoteObjectFields" dbtype="query">
			select name as fieldname, name as fieldnameValue, <!---  type as ---> dataType,nillable
				from qryRemoteFields
			where 1=1 and
				(
				<!--- if a column has been passed through, then we want that value returned --->
				<cfif arguments.column neq "">lower(name) = '#lcase(arguments.column)#'</cfif>
				<!--- if we have mapped fields, then we want to exclude them as we don't want the user to be able to map them twice --->
				<cfif mappedFields neq ""><cfif arguments.column neq "">or</cfif> lower(name) not in (#listQualify(mappedFields,"'")#)
				<!--- otherwise, if we don't have any mappings yet, then we want to get the remote ID field as that will then determine how the objects are mapped --->
				<cfelseif getMappedRemoteObjectFields.recordCount eq 0>
					<cfif arguments.column neq "">and</cfif> type='id'
				</cfif>
				)
			order by fieldname
		</cfquery>

		<!--- display fieldname with datatype --->
		<cfloop query="qryRemoteObjectFields">
			<cfset qryRemoteObjectFields.fieldname = qryRemoteObjectFields.fieldname & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" & qryRemoteObjectFields.dataType & ")">
		</cfloop>

		<!--- set all required fields to have an asterix next to the name --->
		<cfloop query="qryRemoteObjectFields">
			<cfif not nillable>
				<cfset querySetCell(qryRemoteObjectFields,"fieldname",fieldname& " *",currentRow)>
			</cfif>
		</cfloop>

		<cfreturn qryRemoteObjectFields>
	</cffunction>


	<cffunction name="getConnectorMappingListing" access="public" output="false" returnType="query" hint="A query used to populate the connectorMapping listing screen">
		<cfargument name="connectorObjectID" type="numeric" required="true" hint="The ID of the connectorObject that we're doing a listing for">
		<cfargument name="sortOrder" type="string" default="ID" hint="The sortOrder for the listing">

		<cfset var getObjectFields = "">

		<cfquery name="getObjectFields" datasource="#application.siteDataSource#">
			select * from
				(select
					case when exists(select 1 from ConnectorCustomMapping where relaywareObj.object = entityName and m.column_relayware= columnName and relaywareObj.connectorType = connectorType and direction='E' and active=1)
						then 'Custom: ' + m.column_relayware +' *' else m.column_relayware end as relaywareField,
					case when exists(select 1 from ConnectorCustomMapping where relaywareObj.object = entityName and m.column_relayware= columnName and relaywareObj.connectorType = connectorType and direction='I' and active=1)
						then 'Custom: ' + m.column_remote +' *' else m.column_remote end as remoteField,
					m.lastUpdated, case when <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.connectorObjectID#"> = connectorObjectId_relayware then relaywareObj.object else remoteObj.object end as object, m.ID,
					case when <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.connectorObjectID#"> = connectorObjectId_relayware then 1 else 0 end as relayware,
					case when canEdit = 1 then '<span class="editLink">Edit</span>'  else null end as edit,
					case when canEdit = 1 and s.column_name is null then '<span class="deleteLink">Delete</span>' else null end as [delete],
					case when m.direction is null then 'Two-Way' when m.direction='E' then 'Export' else 'Import' end as direction,
					m.active
				from connectorMapping m
					inner join connectorObject relaywareObj on relaywareObj.id = m.connectorObjectID_relayware
					inner join connectorObject remoteObj on remoteObj.id = m.connectorObjectID_remote
					left join information_schema.columns s on s.table_name = relaywareObj.object and m.column_relayware = s.column_name and is_nullable = 'No' and column_default is null
				where <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.connectorObjectID#"> in (connectorObjectId_relayware,connectorObjectId_remote)
			) base
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			<cfif arguments.sortOrder neq "">
			order by #arguments.sortOrder#
			</cfif>
		</cfquery>

		<cfreturn getObjectFields>

	</cffunction>


	<cffunction name="recreateConnectorViews" access="public" output="false" hint="Recreate the connector views">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="object_relayware" type="string" required="true" hint="The object whose views need recreating">
		<cfargument name="direction" type="string" required="false" hint="The import or export view. Leave blank for both">

		<cfset var result = {isOK=true,message=""}>
		<cfset var importExport = structKeyExists(arguments,"direction") and arguments.direction neq ""?arguments.direction:"import,export">
		<cfset var theDirection = "">

		<cftry>
			<cfset var connector = application.getObject("connector.com.connectorUtilities").instantiateConnectorObject(connectorType=arguments.connectorType)>

			<!--- re-create the views --->
			<cfloop list="#importExport#" index="theDirection">
				<cfif listFindNoCase("import,export",theDirection)>
					<cfset var viewResult = connector.createConnectorView(object_relayware=arguments.object_relayware,direction=theDirection)>

					<cfif not viewResult.isOK>
						<cfthrow message="Unable to create #theDirection# transformation view. #viewResult.message#">
					</cfif>
				</cfif>
			</cfloop>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>
	</cffunction>


	<cffunction name="deleteConnectorMapping" access="public" output="false" returnType="boolean" hint="Deletes a connector mapping and recreates the views">
		<cfargument name="connectorMappingID" type="numeric" required="true" hint="The ID of the mapping to delete">
		<cfargument name="recreateViews" type="boolean" default="true" hint="Recreate the transformation views?">

		<cfset var result = true>
		<cfset var deleteMappingQry = "">
		<cfset var getConnectorTypeFromId = "">

		<cftry>
			<cfquery name="getConnectorTypeFromId">
				select connectorType,object_relayware from vConnectormapping where ID=<cf_queryparam value="#arguments.connectorMappingID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfquery name="deleteMappingQry">
				update connectorMapping
				set lastUpdated = getDate(),
					lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
					lastUpdatedByPerson = #request.relayCurrentUser.personID#
				where ID=<cf_queryparam value="#arguments.connectorMappingID#" cfsqltype="cf_sql_integer">

				delete from connectorMapping where ID=<cf_queryparam value="#arguments.connectorMappingID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfif arguments.recreateViews>
				<cfset application.getObject("connector.com.connectorMapping").recreateConnectorViews(connectorType=getConnectorTypeFromId.connectorType[1],object_relayware=getConnectorTypeFromId.object_relayware[1])>
			</cfif>

			<cfcatch>
				<cfset result = false>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="sendConnectorMappingIssueReport" access="public" output="false" returnType="struct">
		<cfargument name="emailFrom" type="string" required="true">
		<cfargument name="relaywareEnvironment" type="string" required="true">
		<cfargument name="remoteObject" type="string" required="true">
		<cfargument name="relaywareObject" type="string" required="true">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="remoteField" type="string" required="true">
		<cfargument name="relaywareField" type="string" required="true">
		<cfargument name="issue" type="string" required="true">

		<cfset var result = {status = "", message = ""}>
		<cftry>
			<cf_mail to="productmanagement@relayware.com"
					from="#arguments.emailFrom#"
					subject="Connector Mapping Issue Report">
				<cfoutput>
Relayware Environment: #arguments.relaywareEnvironment#

Remote Object:  #arguments.remoteObject#

Relayware Object:  #arguments.relaywareObject#

Direction:  #arguments.direction#

Remote Field:  #arguments.remoteField#

Relayware Field :  #arguments.relaywareField#

Issue: #arguments.issue#

Reporter: #arguments.emailFrom#
				</cfoutput>
			</cf_mail>
			<cfset result.status = "success">
			<cfset result.message = "Email Sent">

			<cfcatch type="Any">
				<cfset result.status = "error">
				<cfset result.message = "Email Failed">
			</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>

</cfcomponent>