<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorObject.cfc
Author:			NJH
Date started:	02-12-2014

Description:	A component designed for the connector object edit page

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="postSaveConnectorObject" access="public" output="false" hint="Performs some operations after a connector object record has been saved. Called from xmlEditor" returnType="struct">
		<cfargument name="object" type="string" required="true" hint="The name of the connector object">
		<cfargument name="active" type="boolean" required="true" hint="Whether the object is active or not">
		<cfargument name="relayware" type="boolean" required="true" hint="Whether the object is a relayware object or not">

		<cfset var result = {isOK=true,message=""}>

		<cftry>
			<cfif (form.active_orig neq arguments.active or arguments.newRecord) and arguments.active>
				<cfif arguments.relayware and not application.com.flag.getFlagStructure(flagID=arguments.object&"Synching").isOK>
					<cfset application.getObject("connector.com.connector").prepareObjectForConnector(object=arguments.object)>
				</cfif>
			</cfif>

			<!--- if allowDeletions changes, then recreate views if they exist.. ie. if a mapped object for this object exists as these views
				are generated in a given way based on this value  --->
			<cfif not arguments.newRecord and form.allowDeletions neq form.allowDeletions_orig>
				<cfset var mappedObjectArgs = {}>
				<cfif arguments.relayware>
					<cfset mappedObjectArgs.object_relayware = arguments.object>
				<cfelse>
					<cfset mappedObjectArgs.object_remote = arguments.object>
				</cfif>
				<cfset var mappedObject = application.getObject("connector.com.connector").getMappedObject(argumentCollection=mappedObjectArgs)>

				<cfif mappedObject neq "">
					<cfset var object_relayware = arguments.relayware?arguments.object:mappedObject>
					<cfset application.getObject("connector.com.connectorMapping").recreateConnectorViews(connectorType=arguments.connectorType,object_relayware=object_relayware)>
				</cfif>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="getConnectorObjects" access="public" output="false" returnType="query" validValues="true" hint="Returns a list of objects for the connector object configuration screen">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="relayware" type="boolean" required="true">
		<cfargument name="ID" type="numeric" required="true">
		<cfargument name="UI" default="false">

		<cfset var qryConnectorObjects = application.getObject("connector.com.connector").getConnectorObjects(connectorType=arguments.connectorType,relayware=arguments.relayware)>

		<!--- get a list of objects. If we're working on a new record, then exclude any objects that have been previously mapped --->

		<cfif arguments.ID eq 0>
			<cfif not arguments.relayware>
				<cfset var remoteObjects = application.getObject("connector.com.connector").getRemoteObjects(connectorType=arguments.connectorType, UI=arguments.UI)>

				<cfquery name="qryConnectorObjects" dbType="query">
					select name as display, name as [value]
					from remoteObjects
					<cfif qryConnectorObjects.recordCount>
					where name not in (#quotedValueList(qryConnectorObjects.object)#)
					</cfif>
					order by name
				</cfquery>

			<cfelse>
				<!--- need to work out which RW objects to show here... --->
				<cfset var relaywareObjects = application.com.structureFunctions.structToQuery(struct=application.entityType)>

				<cfquery name="qryConnectorObjects" dbType="query">
					select label as display, tablename as [value],lower(label) as labelLowered
					from relaywareObjects
					<cfif qryConnectorObjects.recordCount>
					where tablename not in (#quotedValueList(qryConnectorObjects.object)#)
					</cfif>
					order by labelLowered
				</cfquery>

			</cfif>
		<cfelse>

			<cfquery name="qryConnectorObjects" dbType="query">
				select object as display, object as [value]
				from qryConnectorObjects
				order by object
			</cfquery>

		</cfif>

		<cfreturn qryConnectorObjects>
	</cffunction>


	<!--- This function gets parent objects. It needs work to work out the relationships based on RW foreign keys or SF child relationships found in meta data. --->
	<cffunction name="getParentConnectorObjects" access="public" output="false" returnType="query" validValues="true" hint="Returns a list of objects that could be considered parent objects">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="relayware" type="boolean" required="true">
		<cfargument name="object" type="string" required="true">

		<cfset var qryConnectorObjects = application.getObject("connector.com.connector").getConnectorObjects(connectorType=arguments.connectorType,relayware=arguments.relayware)>

		<cfquery name="qryConnectorObjects" dbType="query">
			select ID as [value], object as display from qryConnectorObjects where lower(object) != '#arguments.object#'
			order by object
		</cfquery>

		<cfreturn qryConnectorObjects>
	</cffunction>

</cfcomponent>