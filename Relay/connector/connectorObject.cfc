﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorObject.cfc
Author:			NJH
Date started:	23-01-2015

Description:	Connector Object

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/01/23			NJH			Add function to delete incoming deleted orphaned records.
2015/01/30			NJH			Add the approval flag as a flag to be looking out for when exporting data.
2015/06/10			NJH			Change dependency column delimiter from underscore to bent pipe
2015/06/12			NJH			Change how we handle getting of deleted objects. do it right at the beginning. Before, we were only doing it if we had data to export, which meant that we weren't picking up deletions when we need to
2015/07/02			NJH			Two fundamental changes. Process nulls has been set to true so that we can delete fields. This means that we now have to pass in the columns that we want updated. Secondly, handle situation where too many errors are occuring in the batch in upsertEntity. We log the errors
								and all other records we skip to be processed next time around. Pass in an order so that we process those with errors last.
2015/07/13			NJH			Changed underlying view from vConnectorSynchingRecords to vConnectorRecords. Also set dataExists to 0 for any dependent records that we are exporting
2015/07/30			NJH			Deal with a situation (for Tata) where fields required for matching were set to export only. However, every run we would try and match which caused an issue as the fields didn't exist in the import view. So now, only match if we have records to be created.
								This only works for situations where the filter rules are filtering out any records for insert. Any record that comes in that is an insert would still cause this issue.
2015-10-12 			WAB 		During CASE 446216 Fix postProcess_RunWorkflow - was running wrong processes
2015-10-13			WAB			During testing fixed query insertRecordsIntoQueueQry because it was not filtering out all items already in the table and therefore getting a unique index violation
2015-11-25			WAB			altered getDataForObjectsInQueue() to try to improve performance
2015/11/09			NJH			Added some functions that came out of Salesforce connector... configure the API as part of the init function, rather than in connector type object. Some settings also moved from
								Salesforce to generic connector.
2016/05/18			NJH			JIRA PROD2016-953 - process records in batches to improve performance so we don't get 'stuck' behind an object.
2016/08/05          DAN         Case 451223 - limit the rows per run to avoid reaching the maximum size of request on exporting relatedFile
2016/09/13          DAN/NJH     Case 451735/PROD2016-2336 - limit number of items per iteration for export and import based on the connector queue records also limit the deletion records per run when exporting to SF
2016-12-12          DAN         PROD2016-2938 - adjust the synch time with number of seconds based on the settings to workaround the SFDC timing issue
2017/02/08			RMP			PROD2016-2769 Connector: Retry single record synch. Added queueidList argument to runExport and runImport functions so now runObjectSynch can run sync for specified ids from the queue.

Possible enhancements:


 --->

<cfcomponent output="false" extends="connectorPrePostProcessObject" hint="Holds core functions for the processing, importing and exporting of data. This cfc should be instantiated as an object.">


	<!--- initialise the connector --->
	<cffunction name="init" access="public" output="false" hint="Initialise the attributes in the 'this' scope for the connector.">
		<cfargument name="direction" type="string" required="true">
		<cfargument name="object" type="string" required="true">
		<cfargument name="debugLevel" type="numeric" default="0" hint="The level of debugging">
		<cfargument name="testMode" type="boolean" default="false" hint="Whether we are running in test mode">
		<cfargument name="queueidList" type="string" default="" hint="List of ids to import/export">

		<!--- this will be set in the connector specific connector.cfc. See connector\salesforce\connectorSalesforce.cfc as an example --->
		<cfif not structKeyExists(this,"connectorType")>
			<cfthrow message="Connector Type not set.">
		</cfif>

		<cfset this.api = application.getObject("connector.com.connectorUtilities").instantiateConnectorAPIObject(connectorType=this.connectorType)>

		<cfset this.direction = arguments.direction>  <!--- import or export --->
		<cfset this.object = arguments.object> <!--- if import, then remote object; else RW entity --->
		<cfset this.queueidList = arguments.queueidList>

		<cfset var mappedObjectArgs = {}>
		<cfif this.direction eq "import">
			<cfset mappedObjectArgs.object_remote=arguments.object>
		<cfelse>
			<cfset mappedObjectArgs.object_relayware=arguments.object>
		</cfif>

		<cfset this.mappedObject = getMappedObject(argumentCollection=mappedObjectArgs)> <!--- if import, then RW entity; else remote object --->

		<cfif this.direction eq "import">
			<cfset this.baseEntity = this.mappedObject> <!--- base RW entity that we're dealing with when either importing or exporting --->
			<cfset this.baseObject = this.object>
		<cfelse>
			<cfset this.baseEntity = this.object>
			<cfset this.baseObject = this.mappedObject>
		</cfif>
		<cfset this.baseEntityPrimaryKey = application.entityType[application.entityTypeID[this.baseEntity]].uniqueKey> <!--- the unique key of the RW entity --->
		<cfset this.testMode = arguments.testMode> <!--- whether we are running in test mode (when modifying/testing mappings) --->

		<cfset this.dbObjectSuffix = this.testMode?"_test":"">  <!--- if in 'test' mode, then add 'test' to the views and tables --->
		<cfset this.dbObjectPrefix = "connector_#this.connectorType#_">

		<cfset this.objectDataTable = getObjectDataTable()> <!--- name of the table holding the remote object data --->

		<cfset this.synchDataView = getViewForObjectSynch()> <!--- the name of the view that holds the transformed data --->

		<cfset this.tempTableSuffix = "_#left(this.connectorType,2)##left(this.direction,1)##this.object##rereplace(now(),'[^0-9]','','ALL')#"> <!--- create a random temp table suffix, so that at the end, we can clean up all temp tables related to this request--->

		<cfset this.logging = {debugLevel = arguments.debugLevel,filePath="#application.paths.content#\filesTemp\connector\#this.connectorType#\connector_#this.direction#_#this.object#_#this.tempTableSuffix#.htm",tempTableSuffix=this.tempTableSuffix}> <!--- file to hold debug info in. --->

		<cfset this.synchingFlagPrefix = this.connectorType> <!--- the synching flag prefix for a given connector --->
		<cfset var callingFunction = application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction='init').function>

		<!--- try and connect to the remote site. --->
		<cfif listFindNoCase("runExport,runImport",callingFunction)>
			<cfif not this.api.testConnectionToRemoteSite()>
				<cfthrow message="Could not connect to remote site.">
			</cfif>
		</cfif>

		<!--- <cfset var objectDetails = getConnectorObject()>

		<cfset this.whereClause = objectDetails.whereClause>
		<cfset this.parentObject = objectDetails.parentObject>
		<cfset this.allowDeletions = objectDetails.allowDeletions> --->

		<cfset runFunctionIfExists(functionName="#this.connectorType#Init")> <!--- run a connector specific init function if it exists --->

		<!--- setting up the primary key for the remote object. --->
		<cfif not structKeyExists(this,"objectPrimaryKey")>
			<cfthrow message="The objectPrimaryKey not set.">
		</cfif>
		<cfset this.baseEntityObjectKey = getMappedField(object_relayware=this.baseEntity,column_remote=this.objectPrimaryKey).field> <!--- the RW field that maps to the remote object's ID --->
		<cfif this.baseEntityObjectKey eq "">
			<cfthrow message="The baseEntityObjectKey is an empty string which is an indication that this.objectPrimaryKey (#this.objectPrimaryKey#) has not been set up correctly for object #arguments.object#.">
		</cfif>

		<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(connectorType=this.connectorType,object=this.baseObject)>

		<cfset log_(logStruct=this)>
	</cffunction>


	<cffunction name="initialiseVariablesForSynch" access="private" output="false" hint="Sets some variables in this scope needed for the synch (and not for creating views)">
		<cfset application.getObject("connector.com.connector").setConnectorUserAsCurrentUser(connectorType=this.connectorType)>

		<cfset this.connectorDAO = createObject("component","relay.connector.connectorDAO")>
		<cfset this.connectorDAO.init(argumentCollection=getArgumentStructureForEventFunctions())>

		<cfif not structKeyExists(request,"remoteServerTimeStamp")>
			<cfset request.remoteServerTimeStamp = application.com.dateFunctions.convertServerDateToUTC(date=application.com.dateFunctions.getDBDateTime())>
		</cfif>
		<cfset this.remoteServerTimeStamp = request.remoteServerTimeStamp>

		<!--- setting the remoteServerTime offset to be used in date comparisons, so that our dates are 'equal'. Only do it once per request --->
		<cfif not structKeyExists(request,"remoteServerTimeOffsetInSeconds")>
			<cfset request.remoteServerTimeOffsetInSeconds = dateDiff("s",application.com.dateFunctions.convertServerDateToUTC(date=application.com.dateFunctions.getDBDateTime()),this.remoteServerTimeStamp)>
		</cfif>
		<cfset this.remoteServerTimeOffsetInSeconds = request.remoteServerTimeOffsetInSeconds>

	</cffunction>


	<cffunction name="getObjectDataTable" access="private" output="false" returnType="string" hint="Returns the name of the table holding the data for the remote object">
		<cfargument name="object" type="string" default="#this.baseObject#">

		<cfreturn "#this.dbObjectPrefix##arguments.object##this.dbObjectSuffix#">
	</cffunction>


	<!--- initialise connector specific field joins --->
	<cffunction name="initialiseFieldJoins" access="private" output="false" hint="Initialises the field joins for the core mappings that are not exposed as a mapping in the connectorMapping table.">
		<cfset this.fieldJoin = {}>

		<cfset var getCustomisedMappings = "">

		<cfquery name="getCustomisedMappings">
			select case when direction='E' then 'Export' else 'Import' end as direction,entityName,columnName,selectStatement,joinStatement,selectDependencyStatement
			from connectorCustomMapping
			where connectorType=<cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
				and active = 1
			order by direction,entityName
		</cfquery>

		<cfloop query="getCustomisedMappings">
			<cfset this.fieldJoin[direction][entityName][columnName] = {select=selectStatement,selectDependency=selectDependencyStatement,join=joinStatement}>
		</cfloop>

		<cfif structKeyExists(this.fieldJoin[this.direction],"organisation")>
			<cfif this.baseEntity eq "location" and not listFindNoCase(getConnectorObject(object=this.baseEntity,relayware=true).objectList,"organisation") and not application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=this.connectorType,testMode=this.testMode,object="organisation").recordCount>
				<cfif not structKeyExists(this.fieldJoin[this.direction],"location")>
					<cfset this.fieldJoin[this.direction].location = {}>
				</cfif>
				<cfset structAppend(this.fieldJoin[this.direction].location,this.fieldJoin[this.direction].organisation,false)>
			</cfif>
		</cfif>

	</cffunction>


	<cffunction name="createConnectorView" access="public" output="false" hint="Wrapper function to create the transformation views">
		<cfargument name="direction" type="string" required="true" hint="Export or Import">
		<cfargument name="object_relayware" type="string" required="true" hint="The Relayware object that the view will be created for">

		<cfset init(direction=arguments.direction,object=arguments.direction eq "Export"?arguments.object_relayware:getMappedObject(object_relayware=arguments.object_relayware))>
		<cfset initialiseFieldJoins()>
		<cfset var createViewFunction = variables["create#this.direction#View"]>

		<cfreturn createViewFunction(object_relayware=arguments.object_relayware)>
	</cffunction>

	<!---
		when exporting, we are passing in the RW name, but when creating the export views, the object that is being referred to is the remote object
		However, the main object in the this scope is the RW entity.... all a bit confusing...
	 --->
	<cffunction name="createExportView" access="public" output="false" hint="Creates the transformation view needed to export a given Relayware entity">
		<cfargument name="object_relayware" type="string" required="true">

		<cfset var object = "">
		<cfset var result = {isOk=true,message=""}>

		<cfset var mappedObject = getConnectorObject(object=getMappedObject(object_relayware=arguments.object_relayware),relayware=false)>
		<cfloop list="#mappedObject.objectList#" index="object">
			<cfset var viewResult = createTransformationView(object=object)>
			<cfset structAppend(result,viewResult,viewResult.isOK?false:true)>
		</cfloop>

		<!--- if the object is not in a two way synch, we still need the base tables as we need to process any deletions that might have occurred on the remote system. If it is in a two way synch, then the 'CreateImportView'
			will handle the creation of the base tables --->
		<cfif not isObjectInTwoWaySynch()>
			<cfloop list="#mappedObject.objectList#" index="object">
				<cfset buildBaseImportTable(object=object)>
			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!---
		When importing, we are passing in the RW name, and when creating the import views, the object that is being referred to is the RW entity.
		However, the main object in the this scope is the remote object.... all a bit confusing...
	 --->
	<cffunction name="createImportView" access="public" output="false" hint="Creates the transformation view needed to import a given Relayware entity.">
		<cfargument name="object_relayware" type="string" required="true">

		<cfset var mappedObject = getMappedObject(object_relayware=arguments.object_relayware)>
		<cfset var objectList = getConnectorObject(object=mappedObject,relayware=false).objectList>
		<cfset objectList = listAppend(listDeleteAt(objectList,listFindNoCase(objectList,this.object)),this.object)>  <!--- create the view for the base object last. Needed for opportunity. Not sure if specific to opportunity yet --->

		<!--- before creating the import transformation views, we need to ensure that the base tables that the views are built on are in synch. This is where we add columns to the base tables that correspond to any new mappings that may have been
			added or remove any columns where they no longer exist in the mappings --->
		<cfset var object = "">
		<cfloop list="#objectList#" index="object">
			<cfset buildBaseImportTable(object=object)>
		</cfloop>

		<cfset var entityList = this.baseEntity>

		<!--- in the case of the salesforce connector, we are mapping locations but not organisations. However, we need to match and create organisations as well.
			So, here we are checking, if we importing a location, whether an organisation is included in the location mappings and if not, whether it has it's own separate mappings; if not, then add it as part of creating a location--->
		<cfif this.baseEntity eq "location" and not application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=this.connectorType,testMode=this.testMode,object="organisation").recordCount>
			<cfset entityList = listAppend(entityList,"organisation")>
		</cfif>

		<cfset var entity = "">
		<cfset var result = {isOk=true,message=""}>
		<cfloop list="#entityList#" index="entity">
			<cfset var viewResult = createTransformationView(object=entity)>
			<cfset structAppend(result,viewResult,viewResult.isOK?false:true)>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="runExport" access="public" output="false" hint="Handle the export of a Relayware entity to a remote system">
		<cfargument name="object_relayware" type="string" required="true">
		<cfargument name="debugLevel" type="numeric" default="0" hint="The level of debugging">
		<cfargument name="testMode" type="boolean" default="false" hint="Whether we are running in test mode">
		<cfargument name="queueidList" type="string" default="" hint="List of ids to export">

		<!--- double checking whether the object is allowed to be exported --->
		<cfif application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=this.connectorType,testMode=arguments.testMode,object=arguments.object_relayware).export[1]>
			<cfset init(direction="Export",object=arguments.object_relayware,argumentCollection=arguments)>
			<cfreturn runObjectSynch()>
		</cfif>
	</cffunction>


	<cffunction name="runImport" access="public" output="false" hint="Handle the import of a Relayware entity from a remote system">
		<cfargument name="object_relayware" type="string" required="true">
		<cfargument name="debugLevel" type="numeric" default="0" hint="The level of debugging">
		<cfargument name="testMode" type="boolean" default="false" hint="Whether we are running in test mode">
		<cfargument name="queueidList" type="string" default="" hint="List of ids to import">

		<!--- double checking whether the object is allowed to be exported --->
		<cfif application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=this.connectorType,testMode=arguments.testMode,object=arguments.object_relayware).import[1]>
			<cfset init(direction="Import",object=getMappedObject(object_relayware=arguments.object_relayware),argumentCollection=arguments)>
			<cfreturn runObjectSynch()>
		</cfif>
	</cffunction>


	<cffunction name="runObjectSynch" access="private" output="false" hint="Handle the export/import of a given Relayware entity to/from a remote system">

		<cfset var result = {isOK="true",message=""}>
		<cfset initialiseVariablesForSynch()>
		<cfset var synchTimeResult = getLastAndCurrentSynchTimeForObject()>
		<cfset var getDataFunction = variables["getDataTo#this.direction#"]>

		<cftry>
			<!--- TODO: do we need to check the last time a given object ran?? It could be that a given object is erroring and no one knows as the connector is still running ok. --->

			<!--- here we re-set all the records in the queue that have errored in the last run so that they potentially get picked up again. The errored column simply lets us know which records
				have errored in a given run, so that we don't try to further process them.
				We leave out the 'API' objectID records as these are errors that have been received when trying to make an API call.
			--->
			<cfset setErrorIDForObjectSynch()> <!--- reset the errorID column for the object we're processing --->

            <!--- Case 451735 --->
            <cfset application.getObject("connector.com.connector").resetConnectorQueueProcess(connectorType=this.connectorType,object=this.object,direction=this.direction,queueidList=this.queueidList)>

			<!--- set the error field to null so that we re-process them in case data has changed --->
			<cfset application.getObject("connector.com.connector").resetErrorStatusForQueuedRecords(connectorType=this.connectorType,object=this.object,direction=this.direction,queueidList=this.queueidList)>

			<!--- Remove any dependency records that are more than an hour old. This will allow us to process records where dependencies may have changed. --->
			<cfset application.getObject("connector.com.connector").removeQueueDependencies(connectorType=this.connectorType,object=this.object,direction=this.direction,queueidList=this.queueidList)>

			<!--- we will need to process deletions of records on remote systems if doing an export only. Have to do it here, because if there are no RW records to export, this will never get processed.
				A deletion in salesforce will come across as an update --->
			<cfif not isObjectInTwoWaySynch() and this.direction eq "Export">
				<cfset var deletedObjectsResult = getDeletedObjects(startDateTimeUTC=synchTimeResult.lastRunUTC,endDateTimeUTC=synchTimeResult.currentTimeUTC)>

				<cfif deletedObjectsResult.successResult.tablename neq "">
					<cfset log_(label="Break Tie with Remote System",result=deletedObjectsResult)>
					<!--- only break the tie with those records that we are going to export --->
					<cfset breakTieWithRemoteSystem(tablename=deletedObjectsResult.successResult.tablename,objectIDColumn=this.objectPrimaryKey,tableMetaData=application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=deletedObjectsResult.successResult.tablename))>
				</cfif>
			</cfif>

			<!--- 2017/02/08 RMP - For resync only records from queue will be reset and processed. We don't need all data from salesforce/msdynamics.  --->
			<cfif ListLen(this.queueidList)>
				<cfset var getDataResult = application.getObject("connector.com.connectorUtilities").initialiseResultStructure(tempTable=true)>
			<cfelse>
				<cfset var getDataResult = getDataFunction(startDateTimeUTC=synchTimeResult.lastRunUTC,endDateTimeUTC=synchTimeResult.currentTimeUTC,getModificationDataOnly=true)> <!--- get the data to export/import; JIRA 2016/05/18 - Only get the IDs and modified date --->
			</cfif>

			<cfset log_(label="getDataTo#this.direction#",logStruct=getDataResult)>

			<!--- continue if everything is ok --->
			<cfif getDataResult.isOK>

				<!--- we have records to insert --->
				<cfif getDataResult.successResult.recordCount>

					<!--- set the synching flag --->
					<cfset var synchResult = this.connectorDAO.setSynchStatusFlag(tablename=getDataResult.successResult.tablename,status="Synching",tableMetaData=getDataResult.successResult)>

					<!--- put the records into the connector queue --->
					<cfset var insertQueueResult = insertRecordsIntoQueue(tablename=getDataResult.successResult.tablename,tableMetaData=getDataResult.successResult)>
				</cfif>

				 <!--- Case 451735 --->
                <cfset application.getObject("connector.com.connector").setConnectorQueueRecordsToProcess(connectorType=this.connectorType,object=this.object,direction=this.direction,queueidList=this.queueidList)>

				<!---
					if we're importing, we may have some records in the queue that we could try and re-import. But we only have Ids, so we need to go and get the current data
						and append it to the records in the table generated by the 'getImportData' function.
				--->
				<cfif this.direction eq "Import">
					<cfset var truncateDataInDataTable = "">

					<!--- PROD2016-953 need to truncate the table here before we get IDs as otherwise the data table will just hold the modified data, and we won't get the full record set. This is only for records that have just changed.
						Only delete non-deleted records from table as otherwise we lose deletion records in the queue --->
					<cfquery name="truncateDataInDataTable">
						delete from #getObjectDataTable()# where isDeleted=0
					</cfquery>

					<cfset structAppend(getDataResult,getDataForObjectsInQueue())>

					<cfset log_(label="GetDataResult with objects in queue",logStruct=getDataResult)>
				</cfif>

				<cfif doesTableContainRecords(tablename=this.synchDataView)>

					<!--- TODO: split out deletes from creates and updates?? so that deleted records don't go through all the pre-processing --->

					<!--- run any pre-processing --->
					<cfset var preProcessingResult = runEventFunctions(eventType="preProcess")>

					<!--- TODO: should check whether we have records to process before continuing --->
					<!--- prepare the data. This includes matching, validating, checking that parentIds exist, etc. --->
					<cfset var prepareDataForSynchResult = prepareDataForSynch()>

					<!--- this gets the date/time just before we synch the records, so that we can get a handle on what records/fields have been changed in the synch--->
					<cfset var getDate = "">
					<cfquery name="getDate">
						select getDate() as beginSynchDateTime
					</cfquery>
					<cfset var beginSynchDateTime = getDate.beginSynchDateTime>

					<!--- run the actual import/export function. This will update records on the remote system and records on RW --->
					<cfset var synchFunction = variables["#this.direction#Data"]>
					<cfset var synchDataResult = synchFunction()>

					<cfset log_(label="#this.direction#Data",logStruct=synchDataResult)>

					<!--- handle any errors that may have occurred as a result of the import/export--->
					<cfif synchDataResult.errorResult.recordCount>
						<cfset var logErrorsResult = logErrors(tablename=synchDataResult.errorResult.tablename)>
					</cfif>

					<!--- if we have successful imports/exports, process them --->
					<cfif synchDataResult.successResult.recordCount>

						<!--- create an error table to store any errors that occur during post processing --->
						<cfset var postProcessErrorTableName = application.getObject("connector.com.connectorUtilities").createErrorTable().tablename>

						<!--- if exporting, then we need set the RemoteID (ie crmID) on the entity tables to link records in the two systems. The success table should contain the RW IDs --->
						<cfset var setRemoteIdsResult = setIDsForCreatedRecords(tablename=synchDataResult.successResult.tablename,errorTableName=postProcessErrorTableName)>

						<!--- use the error table from the setRemoteIDsFunction if it exists --->
						<!--- <cfif structKeyExists(setRemoteIdsResult,"errorResult") and structKeyExists(setRemoteIdsResult.errorResult,"tablename")>
							<cfset postProcessErrorTableName = setRemoteIdsResult.errorResult.tablename>
						</cfif> --->

						<cfset var postProcessSuccessTableName = synchDataResult.successResult.tablename>
						<!--- use the error table from the setRemoteIDsFunction if it exists --->
						<!--- <cfif structKeyExists(setRemoteIdsResult,"successResult") and structKeyExists(setRemoteIdsResult.successResult,"tablename")>
							<cfset postProcessSuccessTableName = setRemoteIdsResult.successResult.tablename>
						</cfif> --->

						<!--- run any post processing that this specific connector requires --->
						<cfset var postProcessingResult = runEventFunctions(eventType="postProcess",tablename=postProcessSuccessTableName,synchDateTime=beginSynchDateTime, errorTablename=postProcessErrorTableName)>

						<cfset logErrorsResult = logErrors(tablename=postProcessErrorTableName)>
						<!--- remove any records from the incoming table that have errors so that we don't try and process them as successes--->

						<cfset var deleteErroredRecordsFromSuccessTable = "">

						<!--- TODO: incorporate this query into the deleteRecordsFromTable function? --->
						<cfquery name="deleteErroredRecordsFromSuccessTable">
							 delete #postProcessSuccessTableName#
							 from
							 	#postProcessSuccessTableName# s
							 	inner join #postProcessErrorTableName# e on e.objectID = s.objectID and e.object = '#this.object#'
						</cfquery>

						<!--- <cfset deleteRecordsFromTable(tableToDeleteFrom=synchDataResult.successResult.tablename,ignoreModifiedDate=true,tablename=postProcessErrorTableName,column_remote="object",objectIDColumn="objectID")> --->

						<cfset var processSuccessesResult = processSuccesses(tablename=postProcessSuccessTableName,synchDateTime=beginSynchDateTime)>
					</cfif>
				</cfif>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>

				<cfset var errorID = application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,WarningStructure=arguments)>

				<cfset setErrorIDForObjectSynch(errorID=errorID)>
				<cfset log_(label="Error",logStruct=duplicate(cfcatch))> <!---  have to put to put a duplicate as cfcatch doesn't appear to be a struct. Duplicating it makes it one and gets around the problem --->
			</cfcatch>
		</cftry>

		<!--- if everything is ok, log the time, so that we know we've got data up until this point in time --->
		<cfif result.isOK and not this.testMode>
			<cfset setTimeStampForCurrentRun(synchTime=synchTimeResult.currentTime)>
		</cfif>

		<!--- clean up the temporary tables generated by this export/import --->
		<cfset application.getObject("connector.com.connectorUtilities").cleanUpTempTables(tableSuffix=this.tempTableSuffix)>

		<cfset log_(label="End",logStruct={endTime=now()})>
	</cffunction>


	<!--- <cffunction name="testConnectionToRemoteSite" access="public" output="false" returnType="boolean" hint="Runs a test query against the remote site to ensure that we can connect to it">

		<cfthrow message="Function 'testConnectionToRemoteSite' not set up for #this.connectorType# connector">
	</cffunction> --->


	<cffunction name="setErrorIDForObjectSynch" access="private" output="false" hint="Sets or resets the synchErrorID for the connector object that is currently being processed.">
		<cfargument name="errorID" type="numeric" required="false">

		<cfset var setSynchErrorID = "">
		<cfquery name="setSynchErrorID">
			update connectorObject
				set synchErrorID = <cfif structKeyExists(arguments,"errorID")><cf_queryparam value="#arguments.errorID#" cfsqltype="cf_sql_integer"><cfelse>null</cfif>
			where relayware=<cf_queryparam value="#left(this.direction,1) eq 'E'?1:0#" cfsqltype="cf_sql_bit">
				and object=<cf_queryparam value="#this.object#" cfsqltype="cf_sql_varchar">
				and connectorType=<cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
		</cfquery>
	</cffunction>


	<cffunction name="setTimeStampForCurrentRun" access="private" output="false" hint="Sets the timestamp for the current object after a successful synch">
		<Cfargument name="synchTime" type="date" required="true">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="direction" type="string" default="#this.direction#">

		<cfset var updateSuccessfulTime = "">

		<cfquery name="updateSuccessfulTime">
			update connectorObject set lastSuccessfulSynch = <cf_queryparam value="#arguments.synchTime#" cfsqltype="cf_sql_timestamp">
				<!--- NJH 2016/10/17 JIRA PROD2016-2545 - remove the update of the lastUpdated by fields
				,lastUpdated = getDate(),
				lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer"> --->
			where relayware=<cf_queryparam value="#left(arguments.direction,1) eq 'E'?1:0#" cfsqltype="cf_sql_bit">
				and object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
				and connectorType=<cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfset log_(label="setTimeStampForCurrentRun",logStruct=arguments)>

	</cffunction>


	<cffunction name="breakTieWithRemoteSystem" access="private" output="false" hint="Removes the tie between the two systems by setting the remoteID column to null on Relayware">
		<cfargument name="tablename" type="string" default="#this.synchDataView#">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#">
		<cfargument name="objectIDColumn" type="string" default="objectID">

		<cfset var deleteTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
		<cfset var getRecordsToBreakTieWith = "">
		<cfset var whereClause = "where isDeleted=1">
		<cfset var result = {}>

		<!--- if there is no isDeleted column, then assume everything is to be 'deleted' --->
		<cfif structKeyExists(arguments.tableMetaData,"columnList") and not listFindNoCase(arguments.tableMetaData.columnList,"isDeleted")>
			<cfset whereClause = "">
		</cfif>

		<!--- if we're doing an export, we may be exporting a 'delete'. In that case, we need to keep the crmID so we want to keep the crmID on the deleted records --->
		<cfif this.direction neq "export">
			<cfset removeCrmIDForDeletedRecords(argumentCollection=arguments,whereClause=whereClause)>
		</cfif>

		<!--- we don't want to remove the crmID for deleted objects just deleted as we need it in the export view so that we can process the deletions --->
		<cfquery name="getRecordsToBreakTieWith" result="result">
			select e.#this.baseEntityPrimaryKey#, cast(null as varchar(50)) as #this.baseEntityObjectKey#
				into #deleteTableName#
			from #arguments.tablename# t inner join #this.baseEntity# e
				on e.#this.baseEntityObjectKey# = t.#arguments.objectIDColumn#
			#preserveSingleQuotes(whereClause)#

			select * from #deleteTableName#
		</cfquery>

		<cfset result.tablename = deleteTableName>
		<cfset log_(label="Break Tie With Remote System",logStruct=result)>

		<cfif result.recordCount>
			<cfset this.connectorDAO.upsertEntity(tablename=deleteTableName,processNulls="true")> <!--- process nulls as the CRMID is nulled on purpose! --->
		</cfif>
	</cffunction>


	<cffunction name="removeCrmIDForDeletedRecords" access="private" output="false" hint="Remove the crmID for records in the del table">
		<cfargument name="tablename" type="string" default="#this.synchDataView#">
		<cfargument name="whereClause" type="string" default="where isDeleted=1">
		<cfargument name="objectIDColumn" type="string" default="objectID">

		<cfset var removeCRMIDForDelRecords = "">
		<cfset var removeCRMIDForDelRecordsResult = "">

		<cfquery name="removeCRMIDForDelRecords" result="removeCRMIDForDelRecordsResult">
			update #this.baseEntity#Del
				set #this.baseEntityObjectKey# = null
			from #arguments.tablename# t inner join #this.baseEntity#Del e
					on e.#this.baseEntityObjectKey# = t.#arguments.objectIDColumn#
			<cfif arguments.whereClause neq "" and listFirst(arguments.whereClause," ") neq "where">
			where
			</cfif>
			#preserveSingleQuotes(whereClause)#
		</cfquery>

		<cfset log_(label="Remove CrmID For Deleted Records",logStruct=removeCRMIDForDelRecordsResult)>

	</cffunction>


	<!--- This function returns a structure with a successResult structure containing the tablename where the data resides. The table should contain the isDeleted column, an object column and an objectID column --->
 	<cffunction name="getDeletedObjects" access="private" output="false" returnType="struct" hint="Gets the deleted objects from the remote system. This is just a stub to call the connector specific method. Only needed if the object is not in a two-way synch.">
		<cfargument name="startDateTimeUTC" type="date" required="true" hint="The start date in UTC of the date range for which to get deleted records for.">
		<cfargument name="endDateTimeUTC" type="date" required="true" hint="The end date in UTC of the date range for which to get deleted records for.">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The object for which to get deleted records for.">

		<!--- this function should be overridden so we will never end up in here. This is just a stub --->
		<cfthrow message="Function 'getDeletedObjects' not set up for #this.connectorType# connector">
	</cffunction>


	<cffunction name="getLastAndCurrentSynchTimeForObject" access="private" output="false" returnType="struct" hint="Returns the time in UTC that an object was last synched">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="direction" type="string" default="#this.direction#">

		<cfset var result = {}>
		<cfset var getLastSynchTime = "">
		<!--- <cfset var utcTimeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset()> --->
		<cfset var serverTimeOffsetInSeconds = 0>
		<cfset var seconds = application.com.settings.getSetting("connector.scheduledTask.numberOfSecondsToAdjustSynchTime")>

		<!--- when we are getting data from the remote system, we need to take into account the server time offset --->
		<cfif arguments.direction eq "import">
			<cfset serverTimeOffsetInSeconds = this.remoteServerTimeOffsetInSeconds>
		</cfif>

		<!--- get the last synch time --->
		<cfquery name="getLastSynchTime">
			select top 1
				dateadd(s,#serverTimeOffsetInSeconds#,lastRunUTC) as lastRunUTC,
				dateadd(s,#serverTimeOffsetInSeconds#,currentTimeUTC) as currentTimeUTC,
				lastRun,
				currentTime
			from (
				select dbo.convertServerDateTimeToUTCDateTime(lastSuccessfulSynch) as lastRunUTC,
					dbo.convertServerDateTimeToUTCDateTime(getDate()) as currentTimeUTC,
					lastSuccessfulSynch as lastRun,
					getDate() as currentTime
				from connectorObject
				where
					connectorType=<cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar"> and
					object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar"> and
					relayware=<cf_queryparam value="#left(arguments.direction,1) eq 'E'?1:0#" cfsqltype="cf_sql_bit">
			) as base order by lastRunUTC desc
		</cfquery>

		<cfset result.lastRunUTC = createODBCDateTime(getLastSynchTime.lastRunUTC)>
		<cfset result.currentTimeUTC = dateAdd("s",-seconds,createODBCDateTime(getLastSynchTime.currentTimeUTC))>
		<cfset result.lastRun = createODBCDateTime(getLastSynchTime.lastRun)>
		<cfset result.currentTime = dateAdd("s",-seconds,createODBCDateTime(getLastSynchTime.currentTime))>

		<cfset log_(logStruct=result)>

		<cfreturn result>

	</cffunction>


	<cffunction name="logErrors" access="private" output="false" returnType="struct" hint="Logs any errors and increments the synch attempt on the records in the queue.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the errors. Expecting columns object,objectID,message,value and field.">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#" hint="The table meta data. We ideally want the column list">

		<cfset var result = {isOK=true,message=""}>
		<cfset var updateRecordsInConnectorQueueAsErrored = "">
		<cfset var updateRecordsInConnectorQueueAsErroredResult = structNew()>

		<cfset log_(label="Begin Log Errors",logStruct=arguments)>

		<cfif arguments.tablename neq "">

			<cfif doesTableContainRecords(tablename=arguments.tablename)>

				<cfif not structKeyExists(arguments.tableMetaData,"columnList")>
					<cfset arguments.tableMetaData = application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=arguments.tablename)>
				</cfif>

				<!--- mark records in queue as being errored and put in error message. Delete existing errors and re-insert the new ones. Then update the synch attempt on the queue.
					The suspending of records where the max attempt has been reached should now be handled by a trigger on the connectorQueue table which sets the suspended flag if maxSynchAttempts is reached...

					JIRA PROD2016-953 NJH 2016/05/26 - add dataConnectorResponseID to hold ID of data response record in error table
				--->
				<cfquery name="updateRecordsInConnectorQueueAsErrored" result="updateRecordsInConnectorQueueAsErroredResult">
					delete connectorQueueError
					from connectorQueueError e
						inner join vConnectorQueue q on q.ID = e.connectorQueueID
						inner join #arguments.tablename# t on cast(t.objectID as varchar(50)) = q.objectID and t.object = q.object and q.connectorType = <cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">

					insert into connectorQueueError(connectorQueueID,message,value,field <cfif listFindNoCase(arguments.tableMetaData.columnList,"statusCode")>,statusCode</cfif><cfif listFindNoCase(arguments.tableMetaData.columnList,"connectorResponseID")>,connectorResponseID</cfif>)
					select distinct q.ID,t.message,t.value,t.field <cfif listFindNoCase(arguments.tableMetaData.columnList,"statusCode")>,statusCode</cfif><cfif listFindNoCase(arguments.tableMetaData.columnList,"connectorResponseID")>,t.connectorResponseID</cfif>
					from vConnectorQueue q
						inner join #arguments.tablename# t on cast(t.objectID as varchar(50)) = q.objectID and t.object = q.object and q.connectorType = <cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">

					<!--- increment the synch attempt on the queue	and set the records as errored so that they are not picked up to be created/updated --->
					update vConnectorQueue set synchAttempt = synchAttempt+1, errored=1 <cfif listFindNoCase(arguments.tableMetaData.columnList,"connectorResponseID")>, dataConnectorResponseID = t.connectorResponseID</cfif>
					from vConnectorQueue q
						inner join #arguments.tablename# t on cast(t.objectID as varchar(50)) = q.objectID and t.object = q.object and q.connectorType = <cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
				</cfquery>
			</cfif>
		</cfif>

		<cfset structAppend(updateRecordsInConnectorQueueAsErroredResult,result)>

		<cfset log_(label="End Log Errors",logStruct=updateRecordsInConnectorQueueAsErroredResult)>

		<cfreturn result>
	</cffunction>


	<cffunction name="doesTableContainRecords" access="private" output="false" returnType="boolean" hint="Selects from the table to determine whether the table has any records in it. Used to work out whether we need to continue processing in various parts of the application">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table to check.">
		<cfargument name="whereClause" type="string" default="" hint="Any where clause to determine whether records exist that meet a certain criteria... Used currently to determine if there are records to create on RW">

		<cfset var getRecordsInTable = "">
		<cfset var hasRecords = false>
		<cfset var getRecordsInTableResult = structNew()>


		<cfif arguments.tablename neq "">
			<!--- if we're logging, then run our own query and log the results. Otherwise, call the function that returns the boolean value --->
			<cfif this.logging.debugLevel eq 3>
				<cfquery name="getRecordsInTable" result="getRecordsInTableResult">
					select * from #arguments.tablename#
					<cfif arguments.whereClause neq ""> where #arguments.whereClause#</cfif>
			</cfquery>

				<cfif getRecordsInTable.recordCount gt 0>
					<cfset hasRecords = true>
		</cfif>

				<cfset getRecordsInTableResult.tablename=arguments.tablename>
				<cfset log_(label="doesTableContainRecords",logStruct=getRecordsInTableResult)>
			<cfelse>
				<cfset hasRecords = application.getObject("connector.com.connectorUtilities").doesTableContainRecords(argumentCollection=arguments)>
			</cfif>
		</cfif>

		<cfreturn hasRecords>
	</cffunction>


	<!--- <cffunction name="exportData" access="private" output="false" returnType="struct" hint="Exports data in the transformation view to the remote system. Should handle deletes,creates and updates.">

		<!--- this function should be overridden so we will never end up in here. This is just a stub --->
		<cfthrow message="Function 'ExportData' not set up for #this.connectorType# connector">
	</cffunction> --->


	<cffunction name="importData" access="private" output="false" returnType="struct" hint="Imports data into Relayware. It handles creates,updates and deletes">
		<cfargument name="tablename" type="string" default="#this.synchDataView#">
		<cfargument name="entityType" type="string" default="#this.baseEntity#">
		<cfargument name="columns_Updated" type="string" default="#getConnectorObject(object=arguments.entityType,relayware=true).importCreateColumnList#"> <!--- by default, import the import column list --->

		<cfset var result = {isOK=true,message=""}>
		<cfset var preImportResult = runPreImport(argumentCollection=arguments)>

		<cfset log_(label="Begin: Import data",logStruct=arguments)>

		<cfif preImportResult.isOK>

			<cfset var upsertTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
			<cfset var getRecordsToUpsert = "">
			<cfset var getRecordsToUpsertResult = "">
			<cfset var columnsToUpdate = arguments.columns_Updated>
			<cfset var entityUniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey>

			<!--- should these be put into getConnectorObject ?? --->
			<!--- add entity Id if it's not present as it's needed for upsertEntity --->
			<cfif not listFindNoCase(columnsToUpdate,entityUniqueKey)>
				<cfset columnsToUpdate = listAppend(columnsToUpdate,entityUniqueKey)>
			</cfif>
			<!--- the organisationID on the location object is not a mapped field (if accounts are location based) so it's not in the 'importColumnList''. However, it's a field that exists as a special field. It does exist in the view because it is necessary for a location import --->
			<cfif arguments.entityType eq "location" and not listFindNoCase(columnsToUpdate,"organisationID")>
				<cfset columnsToUpdate = listAppend(columnsToUpdate,"organisationID")>
			</cfif>

			<cfquery name="getRecordsToUpsert" result="getRecordsToUpsertResult">
				select distinct upsertUniqueID,hasError,#columnsToUpdate# into #upsertTableName# from #arguments.tablename# t where isDeleted=0
			</cfquery>

			<cfif this.logging.debugLevel gt 1>
				<cfset log_(label="getRecordsToUpsert",logStruct=getRecordsToUpsertResult)>
			</cfif>

			<cfset var upsertResult =  this.connectorDAO.upsertEntity(argumentCollection=arguments,columns_Updated=columnsToUpdate,tablename=upsertTableName,orderBy="hasError")> <!--- only upsert where the records are not marked as deleted. Order the processing so that any records with errors we process last so that we can process as much as possible. --->
			<cfset result = upsertResult>

			<!--- incoming deletions only apply to base entities (ie. not an organisation when an account is mapped to a location) --->
			<cfif arguments.entityType eq this.baseEntity>
				<cfset var deleteTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
				<cfset var getRecordsToDelete = "">
				<cfset var recordsToDeleteResult = "">

				<!--- Need to put the records into a temp table as we delete the CRMID. here, we still have a handle on what records to delete from the queue --->
				<cfquery name="getRecordsToDelete" result="recordsToDeleteResult">
					select *, #this.baseEntityObjectKey# as objectID, '#this.object#' as object into #deleteTableName# from #arguments.tablename# t where isDeleted=1
				</cfquery>

				<cfset recordsToDeleteResult.tablename=deleteTableName>
				<cfset log_(label="Records To Delete",logStruct=recordsToDeleteResult)>

				<cfset var deletionsResult = {isOK=true}>
				<cfif getConnectorObject(object=this.baseEntity,relayware=true).allowDeletions and doesTableContainRecords(tablename=deleteTableName)>
					<cfset deletionsResult = this.connectorDAO.processDeletions(argumentCollection=arguments,tablename=deleteTableName)> <!--- handle deletions if allowed to --->
				</cfif>

				<cfif deletionsResult.isOK>

					<cfset var collateResponse = "">
					<cfset var collateResult = {}>

					<!--- if everything is ok, then append the deletions result to the upsert result, so that they are handled as successes --->
					<cfquery name="collateResponse" result="collateResult">
						insert into #upsertResult.successResult.tablename# (action,objectID,object,#this.baseEntityPrimaryKey#)
						select 'delete',objectID,object,d.#this.baseEntityPrimaryKey# from #deleteTableName# d

						select * from #upsertResult.successResult.tablename#
					</cfquery>

					<cfset structAppend(result.successResult,collateResult)>
				</cfif>
			</cfif>
		<cfelse>
			<cfset structAppend(result,preImportResult)>
		</cfif>

		<cfset log_(label="End: Import data",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="processSuccesses" access="private" returntype="struct" output="false" hint="Process successful imports/exports.">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="tableMetaData" type="struct" default="#application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=arguments.tablename)#">

		<cfset var result = {isOK=true,message=""}>
		<cfset var synchResult = this.connectorDAO.setSynchStatusFlag(tablename=arguments.tablename,status="#this.direction#ed")>
		<cfset var deletedRecordsTablename = application.getObject("connector.com.connectorUtilities").getTempTableName()>
		<cfset var deletedRecordsResult = {}>

		<!---
			This is the last thing we do. We want to keep the handle between the two systems as long as possible, as it will enable us to do any necessary queries.
			But now need to work out which records have been deleted.
			We have to run this query BEFORE deleting the records from the queue as deleting the records from the queue will remove the records from the view
		--->

		<cfif doesTableContainRecords(tablename=arguments.tablename)>
			<cfif this.direction eq "import">
				<cfset var getDeletedRecords = "">

				<cfquery name="getDeletedRecords" result="deletedRecordsResult">
					select #this.baseEntityObjectKey# as objectID into #deletedRecordsTablename# from #this.synchDataView# where isDeleted=1

					select * from #deletedRecordsTablename#
				</cfquery>

				<cfset deletedRecordsResult.tablename=deletedRecordsTablename>
			</cfif>

			<!--- tidying up the table for the delete records function as it expects an object column, which we may not have at this point --->
			<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=arguments.tablename,columnName="object",defaultValue=this.object)>

			<!--- delete all successful synchs from the queue. Ignore the lastModifiedDate... --->
			<cfset var deleteResult = deleteRecordsFromQueue(tablename=arguments.tablename,ignoreModifiedDate=true)>

			<!--- if we're importing, break the tie with the remote system --->
			<cfif this.direction eq "import">
				<cfset breakTieWithRemoteSystem(tablename=deletedRecordsTablename,tableMetaData=deletedRecordsResult)>
			</cfif>
		</cfif>

		<cfset log_(logStruct=deletedRecordsResult)>

		<cfreturn result>
	</cffunction>


	<cffunction name="setIDsForCreatedRecords" access="private" output="false" returnType="struct" hint="A wrapper function to call the correct function for either import or export. Sets the ID for newly created records">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records that have just been imported. We currently expect the remoteID and the entityId columns to exist on this table.">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table that should be populated with any errors encountered.">

		<cfset var functionArgs = duplicate(arguments)>
		<cfset functionArgs.object = this.object>

		<cfif this.direction eq "import">
			<cfset var startTime = GetTickCount()>
			<cfset var entityIdMapping = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey)> <!--- the remote field that holds the Relayware entityId --->
			<cfset var tempTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
			<cfset var getCreatedIdsIntoTempTable = "">
			<cfset var getIDsResult = {}>
			<cfset var relaywareIdColumn = entityIdMapping.field neq "" and entityIdMapping.active?entityIdMapping.field:"">
			<cfset logRequestTime(timediff=GetTickCount()-startTime, method="setIDsForCreatedRecordsImport", endtime=Now())>

			<cfset var startTime = GetTickCount()>
			<!--- TODO: need to work out what to do with records that matched and got updated, rather than created as new!! --->
			<cfquery name="getCreatedIdsIntoTempTable" result="getIDsResult">
				select distinct ROW_NUMBER() over (order by #this.baseEntityPrimaryKey#) AS rowNumber,
					<!--- #this.baseEntityPrimaryKey#, --->
					<cfif relaywareIdColumn neq "">#this.baseEntityPrimaryKey# as #relaywareIdColumn#,</cfif> <!--- it should never happen that we don't have an entityId field mapped --->
					upsertUniqueID as #this.objectPrimaryKey#
				into #tempTableName#
				from #arguments.tablename# t
					where [action]='INSERT'

				select * from #tempTableName#
			</cfquery>
			<cfset functionArgs.tablename = tempTableName>
			<cfset functionArgs.tableMetaData = getIDsResult>
			<cfset logRequestTime(timediff=GetTickCount()-startTime, method="setIDsForCreatedRecordsIF", endtime=Now())>
		</cfif>
		<cfset var startTime = GetTickCount()>

		<!--- if exporting, then we need set the RemoteID (ie crmID) on the entity tables to link records in the two systems. The success table should contain the RW IDs --->
		<cfset var setRemoteIDsFunction = variables["setRemoteIDForNewly#this.direction#edRecords"]>
		<cfset var setRemoteIdsResult = setRemoteIDsFunction(argumentCollection=functionArgs)>
		<cfset log_(label="setRemoteIDForNewly#this.direction#edRecords",logStruct=setRemoteIdsResult)>
		<cfset logRequestTime(timediff=GetTickCount()-startTime, method="setIDsForCreatedRecords", endtime=Now())>
		<cfreturn setRemoteIdsResult>
	</cffunction>


	<cffunction name="setRemoteIDForNewlyExportedRecords" access="private" output="false" returnType="struct" hint="Sets the Relayware ID for newly exported records">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records that have just been exported. We currently expect the remoteID and the entityId columns to exist on this table.">

		<cfreturn this.connectorDAO.upsertEntity(tablename=arguments.tablename,whereClause="action = 'create'",uniqueKeyColumn=this.baseEntityPrimaryKey)>
	</cffunction>


<!--- 	<cffunction name="setRemoteIDForNewlyImportedRecords" access="private" output="false" returnType="struct" hint="Sets the Remote ID for newly imported records.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records that have just been imported. We currently expect the remoteID and the entityId columns to exist on this table.">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table that should be populated with any errors encountered.">

		<!--- this function should be overridden so we will never end up in here. This is just a stub --->
		<cfthrow message="Function 'setRemoteIDForNewlyImportedRecords' not set up for #this.connectorType# connector">
	</cffunction> --->


	<cffunction name="getDataForObjectsInQueue" access="public" output="false" returnType="struct" hint="Populates the import table with data from the remote system for IDs in the queue for the object that we're currently importing.">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="getDataForRecordsWithDependencies" type="boolean" default="false"> <!--- normally, we don't want to get data for items that can't be synched anyways. But in the case of pricebooks, we have some dependencies where we actually want to get data for the dependent records --->

		<cfset var objectDataTable = getObjectDataTable(object=arguments.object)>
		<cfset var result = {isOK=true,message="",tablename=objectDataTable}> <!--- the import table is in the connectorTypeObject naming convention - for example, SFDCOpportunity. it will already hold some records for data that has just changed --->
		<cfset var columnListForSelect = "">
		<cfset var getIdsForObjectsToImport = "">
		<cfset var populateDataTableWithDeletedRecords = "">
		<cfset var getIdsForObjectsToImportResult = structNew()>

		<!--- get data for objects in the queue that are not deleted and that are not in the base object connector table.
			JIRA PROD2016-953 2016/05/18 - Only get X amount of records so that we process smaller batches rather than everything in the queue
			NJH 2016/09/21 JIRA PROD2016-2364 get distinct IDs.. vConnectorRecords has duplicates if record has more than 1 error associated with it. Had to add hasError and queueDateTime -
			--->
		<cfquery name="getIdsForObjectsToImport" result="getIdsForObjectsToImportResult">
			select distinct top #application.com.settings.getSetting("connector.maxRecordsToProcessPerBatch")# q.objectID,q.hasError, q.queueDateTime
			from vConnectorRecords q
				left outer join #result.tablename# t on t.#this.objectPrimaryKey# = q.objectID
			where q.process=1 and
				q.entityTypeId=#application.entityTypeID[this.baseEntity]# and
				q.isDeleted = 0 and
				q.object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar"> and
				q.direction=<cf_queryparam value="#left(this.direction,1)#" cfsqltype="cf_sql_varchar"> and
				q.connectorType = <cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar"> and
				<cfif not arguments.getDataForRecordsWithDependencies> q.queueStatus = 'synching' and</cfif>
				t.#this.objectPrimaryKey# is null
			order by q.hasError, q.queueDateTime
		</cfquery>

		<cfset log_(label="Get data for Ids in the Queue",logStruct=getIdsForObjectsToImportResult)>

		<!--- if we have IDs, then go and get the data. The 'getDataForObjectIDs' function will be connector specific --->
		<cfif getIdsForObjectsToImport.recordCount>
			<cfset structAppend(result,populateDataTableWithDataForIds(object=arguments.object,objectIDs=listToArray(valueList(getIdsForObjectsToImport.objectID))))>
		</cfif>

		<!--- this is so that we have data in our view, as the SF getDataToImport only does it for non-deleted records --->
		<cfquery name="populateDataTableWithDeletedRecords">
			insert into #this.objectDataTable# (#this.objectPrimaryKey#,isDeleted,object)
			select q.objectID,q.isDeleted,q.object
				from vConnectorQueue q
					left join #objectDataTable# d on q.objectID = d.#this.objectPrimaryKey# and q.object = d.object
			where
				d.#this.objectPrimaryKey# is null
				and q.isDeleted = 1
				and q.object =  <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar" >
				and q.connectorType = <cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfset var setDataResponseID = "">
		<cfquery name="setDataResponseID">
			update connectorQueue set dataConnectorResponseID=d.connectorResponseID
			from
				vConnectorQueue q
					inner join #objectDataTable# d on q.objectID = d.#this.objectPrimaryKey# and q.object = d.object
		</cfquery>

		<cfreturn result>
	</cffunction>


	<cffunction name="logRequestTime" access="public" returntype="String">
		<cfargument name="timediff" required="true" type="String">
		<cfargument name="method" required="true" type="String">
		<cfargument name="endtime" required="false" default="#Now()#">

		<cfif structKeyExists(url,"debugLevel") and url.debugLevel eq 3>
			<cfquery name="createNonExistenTable">
                    if not exists (select  1 from dbo.sysobjects o where o.xtype in ('U') and o.name =  'connectorLogTime' )
            begin
            create table [dbo].[connectorLogTime] (
                timediff int, method varchar(50), endtime datetime
                ) ON [PRIMARY]
                end
            </cfquery>

			<cfquery name="qlogRequest" result="qlogRequestResult">
                INSERT INTO connectorLogTime VALUES (<cf_queryparam value="#arguments.timediff#" cfsqltype="cf_sql_varchar">,
				<cf_queryparam value="#arguments.method#" cfsqltype="cf_sql_varchar">,
				<cf_queryparam value="#arguments.endtime#" cfsqltype="cf_sql_timestamp">);
			</cfquery>
		</cfif>
	</cffunction>


	<cffunction name="populateDataTableWithDataForIds" access="public" output="false" returnType="struct" hint="Populates the remote data table with data from the remote system for IDs in the queue">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="objectIds" type="array" required="true">

		<cfset var startTime = GetTickCount()>
		<cfset var getDataResult = getDataForObjectIDs(objectIDs=arguments.objectIds,object=arguments.object)>
		<cfset logRequestTime(timediff=GetTickCount()-startTime, method="populateDataForObjectIDs", endtime=Now())>
		<cfset var addDataToTable = "">
		<cfset var result = {isOK=true,message=""}>
		<cfset var startTime = GetTickCount()>
		<cfset var objectDataTable = getObjectDataTable(object=arguments.object)>
		<cfset logRequestTime(timediff=GetTickCount()-startTime, method="populateObjectDataTable", endtime=Now())>

		<cfset var startTime = GetTickCount()>
		<cfset log_(logStruct=getDataResult)>

		<!--- once we have the data, then insert it into the object's import table. TODO: need to handle cases when we have objects in the queue that no longer exist!!! THis is why we have to check the tablename
			as we can't always be guaranteed of a response. Check that we haven't already put it into the object table. --->
		<cfif getDataResult.successResult.tablename neq "" and getDataResult.successResult.tablename neq objectDataTable>
			<cfquery name="addDataToTable">
				insert into #objectDataTable# (#getDataResult.successResult.columnList#)
				select #getDataResult.successResult.columnList#
				from #getDataResult.successResult.tablename#
			</cfquery>

			<cfset structAppend(result,getDataResult)>
		</cfif>


		<cfif getDataResult.errorResult.tablename neq "">
			<cfif not structKeyExists(getDataResult.errorResult,"recordCount") or getDataResult.errorResult.recordCount gt 0>
				<cfset logErrors(tablename=getDataResult.errorResult.tablename)>
			</cfif>
		</cfif>
		<cfset logRequestTime(timediff=GetTickCount()-startTime, method="populateDataTableWithDataForIds", endtime=Now())>

		<cfreturn result>
	</cffunction>


	<cffunction name="getDataForObjectIDs" access="private" output="false" returnType="struct" hint="Gets data from the remote system for the given IDs">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="objectIds" type="array" required="true">

		<cfthrow message="Function 'getDataForObjectIDs' not set up for #this.connectorType# connector">
	</cffunction>


	<cffunction name="getTempTableName" access="private" output="false" returnType="string" hint="Generates a temporary table name.">
		<cfargument name="methodName" type="string" default="#application.com.globalFunctions.getFunctionCallingFunction(ignoreFunction='getTempTableName').function#" hint="Used to help give some idea of where the temp table is being used. More for debug purposes really.">
		<cfargument name="tempTable" type="boolean" default="false" hint="Whether this is a real temporary table or not."> <!--- this was set to true, but for some reason, we were losing the temp tables and we were getting errors saying that the object did not exist. So, am now using standard tables --->

		<cfreturn application.getObject("connector.com.connectorUtilities").getTempTableName(argumentCollection=arguments,tempTableSuffix=this.tempTableSuffix)>
	</cffunction>


	<cffunction name="deleteRecordsFromTable" access="private" output="false" hint="Removes records from the a given table based on matching on earlier modification date. Used primarily to delete records from the queue." returnType="struct">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to be deleted. This may not be the table that the records are deleted from!">
		<cfargument name="column_remote" type="string" default="object" hint="The column name holding the object. Can also be a value if a column doesn't exist.">
		<cfargument name="objectIDColumn" type="string" default="objectID" hint="The column name holding the object ID">
		<cfargument name="modifiedDateColumn" type="string" default="lastModifiedDate" hint="The column name holding the last modified date">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="ignoreModifiedDate" type="boolean" default="false" hint="If we successfully synched records and want them removed from the queue, we just want to delete records based on type and Id">
		<!--- <cfargument name="tableMetaData" type="struct" default="#structNew()#"> --->
		<cfargument name="tableToDeleteFrom" type="string" default="#arguments.tablename#" hint="The table holding the records which are to be deleted.">
		<cfargument name="dependantRecords" type="boolean" default="false" hint="Are the records to be inserted dependant records?">
		<cfargument name="direction" type="string" default="#this.direction#">

		<cfset var deleteExistingRecordsFromTable = "">
		<cfset var baseTableJoinCol = "crmID">
		<cfset var connectorJoinIDColumn = "entityID">
		<cfset var connectorJoinObj = this.object>
		<cfset var result = {isOK=true,message=""}>
		<cfset var deleteResult = {}>

		<!--- work out the various columns to be using in our joins, based on the type of data in the incoming table. If we find an 'EntityID' col, then we need to join out to get SF Ids.
			If we have an ID, then it could hold either RW or SF data, so work it out based on the current direction (ie. import/export)
		 --->

		<<!--- cfif structKeyExists(arguments.tableMetaData,"columnList")> --->
			<!--- if we have a RW entityID  column or if we have incoming RW records, then we will join to the RW table on the RW entityID and then join to the connector on the crmID --->

		<!--- NJH 2016/08/12 JIRA PROD2016-2159 - don't look at table meta data anymore, as the rw entity field exists on incoming tables as well (such as opporutnityID for msDynamics opportunities table). Base it off direction. --->
			<cfif arguments.direction eq "export">
				<cfset baseTableJoinCol = "entityID">
				<cfset connectorJoinIDColumn = "crmID">
				<cfset connectorJoinObj = this.mappedObject>
				<!--- <cfif arguments.direction eq "export">
					<cfset connectorJoinObj = this.mappedObject> --->
			<!--- <cfelse>
				<cfset connectorJoinObj = this.object> --->
			</cfif>
			<!--- </cfif> --->
		<!--- </cfif> --->

		<!---
			delete any records that are already in there and that have an earlier modification date. We also need to delete any records going the opposite way that might be in the queue
			The incoming table can either be a RW table (ie. holding personIDs) or a remote table (ie holding crmIDs.) Need to be able to handle both scenarios. We join to the base
			RW object so that we can delete any related records related through the remote keys.

			NJH 2015/09/15	Aerohive issue - add the tableToDeleteFrom to the join - it could be a the connectorQueue table or a data table with results to be imported or exported
		--->
		<cfif arguments.tablename neq "">
			<!--- deleting records - delete older records of the same system and the opposite system
				NJH 2016/09/19 JIRA PROD2016-2355 - put records into table variable and then do join, rather than a join on subselect. I suspect that the union was causing subselect version to 'time out'
			--->
			<cfif not arguments.dependantRecords>
				<cfquery name="deleteExistingRecordsFromTable" result="deleteResult">
					declare @deletionsTable as table (objectId varchar(50),object varchar(50),modifiedDate datetime)

					insert into @deletionsTable (objectId,object,modifiedDate)
					select objectId,object,<cfif not arguments.ignoreModifiedDate>modifiedDate<cfelse>null</cfif> from
						(
						select cast(#arguments.objectIDColumn# as varchar(50)) as objectID, #preserveSingleQuotes(arguments.column_remote)# as object<cfif not arguments.ignoreModifiedDate>,t.#arguments.modifiedDateColumn# as modifiedDate</cfif>
							from #arguments.tablename# t
							union
						select cast(v.#connectorJoinIDColumn# as varchar(50)) as objectID,'#connectorJoinObj#' as object <cfif not arguments.ignoreModifiedDate>,t.#arguments.modifiedDateColumn# as modifiedDate</cfif>
							from vEntityName v
								inner join #arguments.tablename# t on t.#arguments.objectIDColumn# = cast(isNull(v.#baseTableJoinCol#,'') as varchar(50)) and v.entityTypeID = #application.entityTypeID[this.baseEntity]#
					) d

					delete #arguments.tableToDeleteFrom#
					from
						vConnectorQueue q
						inner join @deletionsTable as t on q.objectID = t.objectID and q.object = t.object
							and q.connectorType=<cf_queryParam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
							<!--- if we're deleting from the connector queue, we want to delete any records that have been inserted earlier than the incoming. --->
							<cfif not arguments.ignoreModifiedDate>and q.modifiedDateUTC <cfif arguments.tableToDeleteFrom eq "connectorQueue"><<cfelse>></cfif> t.modifiedDate</cfif>
						inner join #arguments.tableToDeleteFrom# on <cfif arguments.tableToDeleteFrom eq "connectorQueue">#arguments.tableToDeleteFrom#.ID = q.ID<cfelse>#arguments.tableToDeleteFrom#.objectId = q.objectID and cast(#arguments.tableToDeleteFrom#.object as varchar(50)) = cast(q.object as varchar(50))</cfif>
					<cfif arguments.tableToDeleteFrom eq "connectorQueue">
						left join connectorQueueDependency queueDependency on queueDependency.connectorQueueID = #arguments.tableToDeleteFrom#.ID
					where
						queueDependency.dependentOnConnectorQueueID is null
					</cfif>
				</cfquery>

			<!--- the incoming records are IDs of objects that have not yet synched, so all that join above a) won't work and b) is not necessary --->
			<cfelse>

				<cfquery name="deleteExistingRecordsFromTable" result="deleteResult">
					delete #arguments.tableToDeleteFrom#
					from #arguments.tablename# t
						inner join vConnectorQueue q on q.objectID = cast(t.#arguments.objectIDColumn# as varchar(50)) and q.object = t.#arguments.column_remote#
							and q.connectorType=<cf_queryParam value="#this.connectorType#" cfsqltype="cf_sql_varchar">
							and q.modifiedDateUTC < t.#arguments.modifiedDateColumn#
						inner join connectorQueue cq on cq.ID = q.ID
					<cfif arguments.tableToDeleteFrom eq "connectorQueue">
						left join connectorQueueDependency queueDependency on queueDependency.connectorQueueID = cq.ID
					where
						queueDependency.dependentOnConnectorQueueID is null
					</cfif>
				</cfquery>

			</cfif>
		</cfif>

		<cfset deleteResult.tablename=arguments.tablename>
		<cfset structAppend(result,deleteResult)>

		<cfset log_(logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="deleteRecordsFromQueue" access="private" output="false" hint="Removes records from the connector queue based on matching on earlier modification date" returnType="struct">
		<cfreturn deleteRecordsFromTable(argumentCollection=arguments,tableToDeleteFrom="connectorQueue")>
	</cffunction>


	<cffunction name="insertRecordsIntoQueue" access="private" output="false" hint="Put records to be processed into the connector queue if they do not already exist.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to be inserted.">
		<cfargument name="objectColumn" type="string" default="object" hint="The column name holding the object">
		<cfargument name="objectIDColumn" type="string" default="objectID" hint="The column name holding the object ID">
		<cfargument name="modifiedDateColumn" type="string" default="lastModifiedDate" hint="The column name holding the last modified date">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="direction" type="string" default="#this.direction#">
		<cfargument name="dependantRecords" type="boolean" default="false" hint="Are the records to be inserted dependant records?">

		<cfif this.logging.debugLevel gt 1>
			<cfset log_(label="Begin insertRecordsIntoQueue",logStruct=arguments)>
		</cfif>

		<!--- delete any records that are already in there and that have an earlier modification date --->
		<cfif not arguments.dependantRecords>
			<cfset deleteRecordsFromQueue(argumentCollection=arguments)>
		</cfif>
		<cfset var insertQueueResult = application.getObject("connector.com.connectorUtilities").insertRecordsIntoQueue(argumentCollection=arguments)>

		<cfset log_(logStruct=insertQueueResult)>
	</cffunction>


	<cffunction name="prepareDataForSynch" access="private" output="false" returntype="struct" hint="Prepares the data for import/export. Runs validation, matching, puts parent records not yet exported/imported into the queue.">

		<cfset var result = {isOK=true,message=""}>
		<cfset var mappedObjectDirection = this.direction eq "import"?"export":"import">
		<cfset var theMappedObject = this.mappedObject>
		<cfset var successResult = {tablename="",recordCount=0,columnList=""}>
		<cfset var modificationsData = {successResult=successResult}>

		<cfset putMissingDependanciesIntoQueue()> <!--- get the IDs for any parent records that we do not yet have and put them into the queue to be processed on the next run --->
		<cfset matchRecords()>
		<cfset var synchTimeResult = getLastAndCurrentSynchTimeForObject(object=theMappedObject,direction=mappedObjectDirection)>

		<!--- if it's a two way synch, then get objects from other system so that we can compare the data--->
		<cfif isObjectInTwoWaySynch()>
			<cfset log_(label="Begin: getDataTo#mappedObjectDirection#")>
			<cfset var getModificationsFunction = variables["getDataTo#mappedObjectDirection#"]>

			<!--- get records on opposite system to be checked against current records that we are processing which will allow to make sure that we don't overwrite any changes that may have happened later --->
			<cfset var modificationsData = getModificationsFunction(startDateTimeUTC=synchTimeResult.lastRunUTC,endDateTimeUTC=synchTimeResult.currentTimeUTC,object=theMappedObject,getModificationDataOnly=true)>
			<cfset log_(label="End: getDataTo#mappedObjectDirection#",logStruct=modificationsData)>

			<!--- if we have changed records on the opposite system, then process them. --->
			<cfif modificationsData.successResult.recordCount>
				<cfset log_(label="Begin processing modifications")>

				<!--- NJH 2016/05/18  - have commented this out for now, as I'm not entirely sure now why it's here
				<cfset var tableMetaData = application.getObject("connector.com.connectorUtilities").addForeignIDColumnToTable(tablename=modificationsData.successResult.tablename,object=theMappedObject,direction=mappedObjectDirection,baseEntity=this.baseEntity,objectPrimaryKey=this.objectPrimaryKey)>
				 --->

				<!--- delete any records from the incoming table where there is already a matching record in the queue that has a greater modified date--->
				<cfset deleteRecordsFromTable(tablename=modificationsData.successResult.tablename,object=theMappedObject,direction=mappedObjectDirection)>
				<!--- put any records into the queue that don't exist or that have a greater modified date --->
				<cfset insertRecordsIntoQueue(tablename=modificationsData.successResult.tablename,object=theMappedObject,tableMetaData=modificationsData.successResult,direction=mappedObjectDirection)>
				<cfset log_(label="End processing modifications")>
			</cfif>

		<!--- <cfelse>
			<!--- if we are exporting some RW records, then we want to ensure that we process any deletions first from the remote system, as we would then get errors relating to updating records
				that no longer exist, for example
				TODO: do we need to handle deletions on import as well??? probably do...
			 --->
			<cfif this.direction eq "Export">
				<cfset modificationsData = getDeletedObjects(startDateTimeUTC=synchTimeResult.lastRunUTC,endDateTimeUTC=synchTimeResult.currentTimeUTC)>
			</cfif> --->
		</cfif>

		<!--- if we're about to export some records, then we want to handle any deletions that occurred on the remote system, so that we don't try and update records that have been deleted. This will/should
			only happen if an object has been deleted on the remote system, and within seconds, the same record has been updated on Relayware
			To be honest, I'm not sure we really need this. It seems to be causing more problems than it's worth, and I'm wondering whether I put this in for a valid reason, or just to be safe
		--->
		<!--- <cfif modificationsData.successResult.tablename neq "" and this.direction eq "Export">
			<cfset log_(label="Break Tie with Remote System")>
			<!--- only break the tie with those records that we are going to export --->
			<cfset breakTieWithRemoteSystem(tablename=getViewForObjectSynch(),objectIDColumn=this.objectPrimaryKey)> <!--- modificationsData.successResult.tablename --->
		</cfif> --->

		<!--- validate the data that we are about to import - data types, required fields, missing profiles, etc --->
		<cfset var validateResult = validateData(tablename=this.synchDataView,direction=this.direction,object=this.object,baseEntityObjectKey=this.baseEntityObjectKey,baseEntity=this.baseEntity,connectorType=this.connectorType)>

		<!--- validate data for import --->
		<cfif this.direction eq "import">
			<!--- upsert entity may tell us that some of the flag values are not valid (ie. they don't exist'). In this case, we want to add them. --->
			<cfset var createFlagsResult = createMissingFlags(tablename=validateResult.errorResult.tablename)>

			<!--- remove any 'NotValidFlag' errors where we successfully have flags so that these errors are not logged --->
			<cfset removeRecordsFromErrorsTable(tablename=validateResult.errorResult.tablename,value=createFlagsResult.createdFlagTextIDs,message="NotValidFlag")>
		</cfif>

		<!--- log any validation errors that still exist --->
		<cfset logErrors(tablename=validateResult.errorResult.tablename)>

		<!--- remove any records from the incoming table that have errors so that we don't try and process them --->
		<cfset deleteRecordsFromTable(tablename=validateResult.errorResult.tablename,ignoreModifiedDate=true)>

		<cfreturn result>
	</cffunction>


	<cffunction name="validateData" access="private" output="false" returnType="struct" hint="Validates data">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records to validate. It will generally be the transformation view">

		<cfset var result = {isOK=true,message="",errorResult={}}>
		<cfset var errorTableName = application.getObject("connector.com.connectorUtilities").createErrorTable().tablename>

		<cfset log_(label="Begin: Validate Data",logStruct=arguments)>

		<!--- run each of the validation methods. It expects each method to put it's own data into the error table. Perhaps get them to return the problem records and then we handle
			putting them into the error table, but then that requires creating multiple temp tables.... but that may be better than this..
			TODO: could remove any records from validation where they are to be deleted. In reality, this is probably a very small number so won't really affect things too much
			--->
		<cfset result = runEventFunctions(eventType="validation",argumentCollection=arguments,errorTableName=errorTableName)>

		<cfset result.errorResult.tablename = errorTableName>

		<cfset log_(label="End: Validate Data",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="createMissingFlags" access="public" output="false" returnType="struct" hint="Adds missing flags and returns the ones that were successfully created.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the 'error' records. This should have the following columns: field,message,value.">

		<cfset var getMissingFlagValues = "">
		<cfset var createdFlagTextIDs = "">
		<cfset var result = {isOK=true,message="",createdFlagTextIDs=""}>
		<cfset var flagTextId = "">

		<!--- column name is going to be the flagGroupTextID of the flagGroup that the flag belongs in. Value is the name of the flag --->
		<cfquery name="getMissingFlagValues">
			select distinct t.field, t.value, fg.flagTypeID
			from #arguments.tablename# t
				inner join flagGroup fg on fg.flagGroupTextID = t.field
			where t.message = 'NotValidFlag'
		</cfquery>

		<!--- loop over and create missing flags. Keep track of the ones that were successfully created --->
		<cfloop query="getMissingFlagValues">
			<cfset flagTextId = application.com.regExp.makeSafeTextID(inputString=replace(field&"_"&value," ","_","ALL"))>
			<cfif application.com.flag.createFlag(flagGroup=field,Name=value,flagType=flagTypeID,flagtextID=flagTextId) neq 0>
				<cfset createdFlagTextIDs = listAppend(createdFlagTextIDs,flagTextId)>

				<!--- insert the flag into the connectorColumnValueMapping table --->
				<cfset application.getObject("connector.com.connector").upsertColumnValueMapping(connectorType=this.connectorType,object_relayware=this.baseEntity,column_relayware=field,value_relayware=flagTextId,value_remote=value)>
			</cfif>
		</cfloop>

		<cfset result.createdFlagTextIDs = createdFlagTextIDs>

		<cfset log_(label="Create Missing Flags",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="removeRecordsFromErrorsTable" access="private" output="false" returnType="struct" hint="Removes records from the errors table">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the 'error' records. This should have the following columns: value.">
		<cfargument name="value" type="string" required="true" hint="A list of values">
		<cfargument name="message" type="string" required="true" hint="The type of error to delete">

		<cfset var result = {isOK=true,message=""}>
		<cfset var removeErrorRecords = "">

		<cfif arguments.tablename neq "">
			<cfquery name="removeErrorRecords">
				delete from #arguments.tablename#
				where
					message = <cf_queryparam value="#arguments.message#" cfsqltype="cf_sql_varchar">
				<cfif arguments.value neq "">and value in (<cf_queryparam value="#arguments.value#" cfsqltype="cf_sql_varchar" list="true">)</cfif>
			</cfquery>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="putMissingDependanciesIntoQueue"  access="private" output="false" hint="Populate the queue with parent IDs that need importing/exporting as they do not yet exist on the remote system.">
		<cfargument name="viewName" type="string" default="#this.synchDataView#">

		<cfset var getDependantRecordsForQueue = "">
		<cfset var getDependantColumns = "">
		<cfset var tempTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
		<cfset var tempTableForViewData = application.getObject("connector.com.connectorUtilities").getTempTableName()>
		<cfset var result = {isOk=true,message=""}>

		<cfif this.logging.debugLevel gt 1>
			<cfset log_(label="Begin: Put Missing Dependancies Into Queue",logStruct=arguments)>
		</cfif>

		<!--- do parent columns exist?? There is a naming convention which goes like:
			directionparentColNameparentObject

			for example:
			importaccountIDaccount, importownerIDuser
		 --->
		<cfquery name="getDependantColumns" result="result">
			declare @delimiter char(1)

			set @delimiter = char(172)

			select dbo.listGetAt(column_name,2,@delimiter) as parentIDColumn,
				dbo.listGetAt(column_name,3,@delimiter) as object,
				'['+column_name+']' as columnWithIDToImport
			from information_schema.columns
			where table_name=<cf_queryparam value="#arguments.viewName#" cfsqltype="cf_sql_varchar">
				and column_name like <cf_queryparam value="#this.direction#" cfsqltype="cf_sql_varchar"> + @delimiter + '%'
				and dbo.listLen(column_name,@delimiter) >= 3
			order by column_name
		</cfquery>

		<!--- if we have some parent columns (ie. an accountID on a contact), then select the original ID where the transformed ID is null (ie where they do not yet exist). Don't grab stuff that is already in the queue. --->
		<cfif getDependantColumns.recordCount gt 0>

			<cfset var blockedObjectID = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)>
			<cfif this.direction eq "import">
				<cfset blockedObjectID = this.baseEntityObjectKey>
			<cfelseif blockedObjectID eq "">
				<cfset blockedObjectID = this.baseEntityPrimaryKey>
			</cfif>

			<cfquery name="getDependantRecordsForQueue" result="result">
				declare @dependencyTable TABLE
					(
					  objectID varchar(50),
					  object nvarchar(50),
					  lastModifiedDate datetime,
					  isDeleted bit,
					  blockedObjectID varchar(50),
					  blockedObject nvarchar(50),
					  connectorResponseID int
					)

				declare @delimiter char(1)
				set @delimiter = char(172)

				<!--- select data from view into temp table so that we're not selecting from the view everytime. Just do it once. Just get data that we need --->
				select #blockedObjectID#,connectorResponseID,#valueList(getDependantColumns.columnWithIDToImport)#,#valueList(getDependantColumns.parentIdColumn)#
					into #tempTableForViewData#
				from #arguments.viewName#
					where isDeleted=0

				<cfloop query="getDependantColumns">
					<!--- NJH 2016/03/10 - JIRA PROD2016-472 Synching Attachments - when synching related files, the object is dynamic as it will depend on the entityType for the relatedFileCategory. In this case, the 'columnWithIDToImport' will be a list, where the first item is the object/entityID and the second item will be the object/entityType  --->
					<!--- remove the left join to dependency table for performance reasons. We get a distinct below anyways, so it shouldn't matter --->
					<cfset var objectSql = object neq "object"?"'#object#'":"dbo.listGetAt(t.#columnWithIDToImport#,2,@delimiter)">
					insert into @dependencyTable
					select
						dbo.listGetAt(t.#columnWithIDToImport#,1,@delimiter) as objectID
						,#preserveSingleQuotes(objectSql)# as object
						,getDate() as lastModifiedDate, 0 as isDeleted
						,t.#blockedObjectID# as blockedObjectID
						,'#this.object#' as blockedObject
						,min(t.connectorResponseID)
					from
						#tempTableForViewData# t
						left join (vConnectorQueue q inner join connectorQueueDependency qd on q.ID = qd.connectorQueueID)
							 on cast(t.#blockedObjectID# as varchar(50)) = q.objectID and q.object =  <cf_queryparam value="#this.object#" CFSQLTYPE="CF_SQL_VARCHAR" >
						--left join @dependencyTable dt on dt.objectID = dbo.listGetAt(t.#columnWithIDToImport#,1,@delimiter) and dt.object = #preserveSingleQuotes(objectSql)#
					where
						/*dt.objectID is null
						and*/ t.#parentIdColumn# is null
						and len(isNull(t.#columnWithIDToImport#,'')) > 0
						<!---and t.isDeleted = 0 --->
						and q.ID is null
					group by t.#columnWithIDToImport#,t.#blockedObjectID#
				</cfloop>

				select distinct * into #tempTableName# from @dependencyTable

				select * from #tempTableName#
			</cfquery>

			<cfset result.tablename=tempTableName>

			<!--- put any dependant ID records into the connector queue to be processed when we're processing the given entity type --->
			<cfif result.recordCount>
				<cfset insertRecordsIntoQueue(tablename=tempTableName,dependantRecords=true)>
			</cfif>
		</cfif>

		<cfset log_(logStruct=result)>

		<cfreturn>
	</cffunction>


	<cffunction name="matchRecords"  access="private" output="false" hint="Perform matching on records that are about to be created due to no remote key existing on their records.">

		<cfset var matchRecordsFunction = variables["matchRecordsFor#this.direction#"]>
		<cfset var matchResult = matchRecordsFunction()>

		<cfset log_(logStruct=matchResult)>

		<cfreturn>
	</cffunction>


	<cffunction name="matchRecordsForImport" access="private" output="false" returnType="struct" hint="Match records about to be created on Relayware">
		<cfargument name="entityType" type="string" default="#this.baseEntity#">
		<cfargument name="tablename" type="string" default="#this.synchDataView#">

		<cfset var result = {isOK=true,message=""}>

		<cfset log_(label="Begin: Match Records For Import",logStruct=arguments)>

		<cfif listFindNoCase("organisation,location,person",arguments.entityType)>

			<!--- only run matching code if we have records that are going to be inserted. Tata had an issue when fields required for matching were set to export only. In this case, they didn't want to create any records in RW, but
				they did want to update certain fields
				JIRA PROD2016-1146. NJH 2016/05/24 For some reason, because the locationID is now coming from vConnectorRecords, we error when selecting from the view 'where locationID is null'.
				We have to cast it to a varchar. Same goes for the query below!
			 --->
			<cfif doesTableContainRecords(tablename=arguments.tablename,whereClause="#application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey# is null")>

				<!--- update the core table matchnames first --->
				<cfset application.com.matching.updateMatchFields(tablename=arguments.entityType,entityType=arguments.entityType)>
				<!--- update the matchnames in the incoming table/view --->
				<cfset application.com.matching.updateMatchFields(tablename=arguments.tablename,entityType=arguments.entityType)>

				<cfif arguments.entityType eq this.baseEntity>

					<!--- just get data we need for matching, and also put into temp table for performance reasons --->
					<cfset var tempTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
					<cfset var viewColList = application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=arguments.tablename).columnList>
					<cfset var existingMatchingFields = "">
					<cfset var nonExistingMatchingFields = "">
					<cfset var fieldname = "">
					<cfset var getRecordsToMatch = "">

					<!--- have to work out whether all the required fields exist in the view.... this is in case some of the mappings may have been inactivated. If so, we treat them as a null... --->
					<cfloop list="#application.com.matching.getRequiredMatchFields(entityType=this.baseEntity)#" index="fieldname">
						<cfif listFindnoCase(viewColList,fieldname)>
							<cfset existingMatchingFields = listAppend(existingMatchingFields,fieldname)>
						<cfelse>
							<cfset nonExistingMatchingFields = listAppend(nonExistingMatchingFields,fieldname)>
						</cfif>
					</cfloop>

					<cfset var getRecordsToMatchResult = {}>
					<!--- need to ensure that the CRMID exists. If not in the matching fields, then explicitly add it as a column to select --->
					<cfif not listFindNoCase(existingMatchingFields,this.baseEntityObjectKey)>
						<cfset existingMatchingFields = listAppend(existingMatchingFields,this.baseEntityObjectKey)>
					</cfif>

					<cfquery name="getRecordsToMatch" result="getRecordsToMatchResult">
						select upsertUniqueID, #existingMatchingFields#
							<cfloop list="#nonExistingMatchingFields#" index="columnName">,cast(null as varchar) as #columnName#</cfloop>
						into #tempTableName#
						from #arguments.tablename#
						where isDeleted=0 and #this.baseEntityPrimaryKey# is null
					</cfquery>
					<cfset structAppend(result,getRecordsToMatchResult)>

					<cfset var excludeRecordsWithCondition = {whereClause="len(isNull(d.#this.baseEntityObjectKey#,'')) = 0"}>
					<cfset application.com.matching.matchRecords(tablename=tempTableName,entityType=arguments.entityType,uniqueIDColumnName="upsertUniqueID",matchCondition=excludeRecordsWithCondition,updateIDColumnName=this.baseEntityObjectKey,updateTableName=this.baseEntity,updateIDFromColumnName="upsertUniqueID")>
				</cfif>
			</cfif>
		</cfif>

		<cfset log_(label="End: Match Records For Import",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<!--- <cffunction name="matchRecordsForExport" access="private" output="false" returnType="struct" hint="Match records about to be created on remote system to avoid creating duplicate records.">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The remote object that we're matching for export">
		<cfargument name="matchColumns" type="struct" required="true" hint="The Relayware columns to be matched on.">
		<cfargument name="tablename" type="string" default="#this.synchDataView#" hint="The name of the table containing the data to be exported and matched">

		<!--- this function should be overridden so we will never end up in here. This is just a stub --->
		<cfthrow message="Function 'matchRecordsForExport' not set up for #this.connectorType# connector">
	</cffunction> --->


	<cffunction name="getViewForObjectSynch" access="private" output="false" returnType="string" hint="Returns the name of the transformation view">
		<cfargument name="object" type="string" default="#this.mappedObject#">
		<cfargument name="direction" type="string" default="#this.direction#">

		<cfset var rwEntity = this.baseEntity>
		<cfset var remoteObject = this.object>

		<cfif arguments.direction eq "import">
			<cfset rwEntity = arguments.object>
		<cfelse>
			<cfset remoteObject = arguments.object>
		</cfif>

		<cfreturn "v#this.dbObjectPrefix##left(arguments.direction,3)#_#rwEntity#_#remoteObject##this.dbObjectSuffix#">
	</cffunction>


	<!--- <cffunction name="getDataToImport" access="private" returnType="struct" hint="Collect records that have been modified so that they get picked up in an import" output="false">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">
		<cfargument name="startDateUTC" type="date" required="true" hint="The date/time (in UTC) when we last did an import for the given object.">
		<cfargument name="endtDateUTC" type="date" required="true" hint="The date/time (in UTC) that we are getting data up until.">
		<cfargument name="getModificationDataOnly" type="boolean" default="false" hint="Get only fields necessary to compare with records to be exported. (ie. ID, lastModifiedDate). This could maybe be changed to a fieldlist.">

		<!--- this function should be overridden so we will never end up in here. This is just a stub --->
		<cfthrow message="Function 'getDataToImport' not set up for #this.connectorType# connector">
	</cffunction> --->


	<cffunction name="getDataToExport" access="private" returnType="struct" hint="Collect records that have been modified so that they get picked up in an export" output="false">
		<cfargument name="startDateTimeUTC" type="date" required="true">
		<cfargument name="endDateTimeUTC" type="date" required="true">

		<cfreturn this.connectorDAO.getDataToExport(argumentCollection=arguments,baseEntity=this.baseEntity,baseEntityObjectKey=this.baseEntityObjectKey)>
	</cffunction>


	<cffunction name="isObjectInTwoWaySynch" access="private" output="false" returnType="boolean" hint="Returns whether a given object is in a two way synch or not">
		<cfargument name="object" type="string" default="#this.object#">

		<cfset var twoWaySynch = true>
		<cfset var primaryObjects = application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=this.connectorType,testMode=this.testMode,object=arguments.object)>

		<cfif not primaryObjects.export or not primaryObjects.import>
			<cfset twoWaySynch = false>
		</cfif>
		<cfreturn twoWaySynch>
	</cffunction>


	<cffunction name="removeIncomingOrphanDeletionsFromDataTable" access="private" output="false" hint="Removes any incoming records that are marked for deletions that do not have a tie to a RW record">
		<cfargument name="tablename" type="string" default="#this.objectDataTable#">

		<cfset var deleteOrphanedRecords = "">
		<cfset var deleteOrphanedRecordsResult = "">

		<cfquery name="deleteOrphanedRecords" result="deleteOrphanedRecordsResult">
			delete #arguments.tablename#
			from #arguments.tablename# t
				left join vEntityName v on t.objectID = v.crmID and v.entityTypeID = #application.entityTypeID[this.baseEntity]#
			where
				v.entityID is null
				and t.isDeleted = 1
		</cfquery>

		<cfset deleteOrphanedRecordsResult.tablename = arguments.tablename>
		<cfset log_(label="Remove Orphaned Deletions",logStruct=deleteOrphanedRecordsResult)>
		<!---<cfset var deleteRecordsTable = getTempTableName()>
		<cfset var getOrphanedRecords = "">

		<cfquery name="getOrphanedRecords">
			select objectID, object into #deleteRecordsTable#
			from #arguments.tablename# t
				left join vEntityName v on t.objectID = v.crmID and v.entityTypeID = #application.entityTypeID[this.baseEntity]#
			where
				v.entityID is null
				and t.isDeleted = 1
		</cfquery>

		 TODO: need to work on this function first before I can use it for this purpose; so, doing the delete in the query above now
		<cfset deleteRecordsFromTable(ignoreModifiedDate=true,tablename=deleteRecordsTable,tableToDeleteFrom=this.objectDataTable)> --->
	</cffunction>


	<cffunction name="getUserMatchingData" access="private" hint="Returns the column names of the user object that we will need for matching to exsiting Relayware Users. This includes both location and person fields." returnType="struct">

		<!--- this function should be overridden so we will never end up in here. This is just a stub. it should return the a structure with the following keys:
			fieldList - name of fields used for matching. Should contain both location and person fields. For example, "companyName,street,city,postalCode,country,firstname,lastname,email"
			userObjectName - the name of the object containing the data. For example, "user"
		 --->
		 <cftry>
			 <cfreturn application.getObject("connector.com.connector").getUserMatchingData(connectorType=this.connectorType)>
			 <cfcatch>
		<cfthrow message="Function 'getUserMatchingData' not set up for #this.connectorType# connector">
			</cfcatch>
		</cftry>
	</cffunction>


	<cffunction name="runPreImport" access="private" output="false" returnType="struct" hint="Runs some pre-import functions">
		<cfargument name="tablename" type="string" default="#this.synchDataView#">
		<cfargument name="entityType" type="string" default="#this.baseEntity#">

		<!--- work out if the view contains a user column that we then need to look at for importing users.It's usually OwnerID, but can't always be guaranteed. Look for a column starting with 'importUser' --->
		<cfset var result = {isOK=true,message=""}>
		<cfset var remoteOwnerFields = application.getObject("connector.com.connector").getRemoteOwnerField(connectorType=this.connectorType,object=getMappedObject(object_relayware=arguments.entityType))>

		<cfloop query="remoteOwnerFields">
			<cfif remoteOwnerFields.owner neq "">
				<cfset var ownerMappedField = getMappedField(column_remote=remoteOwnerFields.owner)>

			<!--- if the object has an ownerID field mapped, then import any account managers (users) that may need importing --->
			<cfif ownerMappedField.field neq "" and ownerMappedField.import>
					<cfset result = importUsers(ownerIDField=remoteOwnerFields.owner)>
			</cfif>
		</cfif>
		</cfloop>

		<!--- handle to run any object specific pre-import functions which could be set up in customised cfc --->
		<cfset result = runFunctionIfExists(functionName="runPre#arguments.entityType#Import")>

		<!--- TODO: log any errors so that we don't process any records that may have errored in a preImport function!! --->
		<cfreturn result>
	</cffunction>


	<cffunction name="importUsers" access="private" output="false" returnType="struct" hint="Imports users which are currently not mapped. They are a special case. They will exist in the queue and will get picked up in this function.">
		<cfargument name="ownerIDField" type="string" default="ownerID" hint="The name of the remote ownerID (userId) field. For Salesforce, this is usually OwnerID, but can be others as well.">

		<cfset var startTime = GetTickCount()>
		<cfset var getUserIDsToImport = "">
		<cfset var result = {isOK=true,message=""}>
		<cfset var getUserIDsToImportResult = {}>

		<!--- get the IDs of the users in the queue that we need to import --->
		<cftry>
			<cfquery name="getUserIDsToImport" result="getUserIDsToImportResult">
				select distinct v.[importUser#application.delim1##arguments.ownerIDField#] as ownerID
				from #this.synchDataView# v
					left join person p on p.crmPerID = v.[importUser#application.delim1##arguments.ownerIDField#]
				where p.personID is null
					and v.[importUser#application.delim1##arguments.ownerIDField#] is not null
			</cfquery>

			<cfset getUserIDsToImportResult.tablename = this.synchDataView>
			<cfset log_(label="Import Users",logStruct=getUserIDsToImportResult)>

			<!--- if we have user IDs to import.... --->
			<cfif getUserIDsToImport.recordCount>
				<cfset var userMatchingStruct = getUserMatchingData()>
				<cfset var userFields = "">
				<cfset var field = "">

				<cfloop collection="#userMatchingStruct.fieldStruct#" item="field">
					<cfset userFields = listAppend(userFields,userMatchingStruct.fieldStruct[field])>
				</cfloop>

				<!--- get the user data --->
				<cfset var getDataResult = getDataForObjectIDs(fieldList=userFields,object=userMatchingStruct.userObjectName,objectIDs=listToArray(valueList(getUserIDsToImport.ownerID)))>

				<cfset log_(label="User Data",logStruct=getDataResult)>

				<cfif getDataResult.successResult.recordCount>
					<cfset result = this.connectorDAO.matchAndImportRemoteUsers(tablename=getDataResult.successResult.tablename,userFieldStruct=userMatchingStruct.fieldStruct)>
				</cfif>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>

				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch)>
				<!--- TODO: log any errors here in an error table so that any problems bringing over an account manager results in a failure message. --->
			</cfcatch>
		</cftry>
		<cfset logRequestTime(timediff=GetTickCount()-startTime, method="importUsers", endtime=Now())>
		<cfreturn result>
	</cffunction>


	<cffunction name="matchRecordsForExport" access="private" output="false" returnType="struct" hint="Build up data to match POL records on the Remote system">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The SF object that we're matching for export">
		<!--- <cfargument name="matchColumns" type="struct" required="true" hint="The columns to be matched on."> --->
		<cfargument name="tablename" type="string" default="#this.synchDataView#">

		<cfset var result = {isOK=true,message="",tablename=""}>
		<cfset var userMatchingStruct = getUserMatchingData()>
		<cfset var userObjectName = userMatchingStruct.userObjectName>
		<cfset var rwBaseEntity = arguments.object eq userObjectName?"person":this.baseEntity>

		<!--- only check for POL data or remote users --->
		<cfif listFindNoCase("organisation,location,person",rwBaseEntity)>
			<cfset var columnName = "">
			<cfset var baseEntityObjectKey = arguments.object eq userObjectName?"crmPerID":this.baseEntityObjectKey>
			<cfset var remoteMatchFields = application.getObject("connector.com.connectorUtilities").getMatchFieldsForExport(object=arguments.object,entityType=rwBaseEntity)>
			<cfset var entityUniqueKey = application.entityType[application.entityTypeID[rwBaseEntity]].uniqueKey>
			<cfset var objectIdField = arguments.object eq userObjectName?userMatchingStruct.fieldStruct.ID:this.objectPrimaryKey>
			<!--- <cfset var queryString = "select #objectIdField# from #arguments.object#"> --->

			<!---
				Despite this being here from legacy connector, we actually don't want to match on only object included in synch, as objects included in synch will have already synched and will therefore not match. We may need an extra filter clause
					for matching objects but don't think the object where clause is what we need/want
			<cfif objectWhereClause neq "">
				<cfset queryString = queryString & " " & replace(objectWhereClause,"'","''","ALL") & " and "> <!--- escape single quotes with double quotes --->
			</cfif> --->

			<cfset var count=1>
			<cfset var matchFieldListLen = listLen(remoteMatchFields)>
			<cfset var matchTableName = getTempTablename()>
			<cfset var thisTempTableSuffix = request.tempTableSuffix> <!--- storing the original suffix, as we will be modiying the request variable for each thread that gets processed, so that there is no contention on tables.--->
			<cfset var getQueryStringForMatching = "">
			<cfset var relaywareIdColumn = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=rwBaseEntity)>

			<!--- escape single quotes in the columnName value in the query below --->
			<cfquery name="getQueryStringForMatching">
				select distinct
					ROW_NUMBER() over (order by <cf_queryObjectName value="#relaywareIdColumn#">) AS rowNumber, #relaywareIdColumn#, '<cfloop list="#remoteMatchFields#" index="columnName"><cfset var columnValue=replace(columnName,"'","\'","ALL")>#columnName# = '''+replace(rtrim(ltrim(isNull(#preserveSingleQuotes(columnValue)#,''))),'''','\''') +'''<cfif count lt matchFieldListLen> and </cfif><cfset count++></cfloop>' as matchingWhereClause
				from
					#arguments.tablename#
				where #objectIdField# is null
					and isDeleted = 0
			</cfquery>

			<cfset var matchArgs = {api=this.api,objectPrimaryKey=objectIdField,entityIdMappedField=relaywareIdColumn,baseEntity=rwBaseEntity,baseEntityObjectKey = baseEntityObjectKey,matchTableName=matchTableName,object=arguments.object,entityUniqueKey=entityUniqueKey,getRowsToMatch=getQueryStringForMatching,tempTableSuffix=thisTempTableSuffix,objectIdField=objectIdField}>

			<cfif getQueryStringForMatching.recordCount>

				<cfset var createMatchedRecordsTable = "">

				<cfquery name="createMatchedRecordsTable">
					select cast('' as varchar(50)) as #baseEntityObjectKey#, 0 as #entityUniqueKey# into #matchTableName# where 1=0
				</cfquery>

				<cfset result.tablename = matchTableName>
				<cfset var numberOfThreads = 4>
				<cfset var threadsUsed = "">

				<cfset var hash = createUUID()>
				<cfset var threadNumber = "">
				<cfset var getRowsToMatchForThread = "">
				<cfset var threadNamePrefix = "runMatchThread#hash#">


				<cfloop	from="1" to="#numberOfThreads#" step="1" index="threadNumber">

					<cfset var numberOfRowsToProcess = int(getQueryStringForMatching.recordCount/numberOfThreads)>
					<cfset var getRowsTo = numberOfRowsToProcess * threadNumber>
					<cfset var getRowsFrom = getRowsTo - numberOfRowsToProcess>

					<cfquery name="getRowsToMatchForThread" dbtype="query">
						select * from getQueryStringForMatching where rowNumber > #getRowsFrom# <cfif numberOfThreads neq threadNumber>and rowNumber <= #getRowsTo#</cfif>
					</cfquery>

					<cfif getRowsToMatchForThread.recordCount>
						<cfset threadsUsed = listAppend(threadsUsed,threadNumber)>
						<cfset matchArgs.getRowsToMatch = getRowsToMatchForThread>
						<cfset matchArgs.tempTableSuffix="_#threadNumber#"&thisTempTableSuffix>
						<cfset var feed = {matchExportRecords=variables.matchExportRecords,matchArgs=matchArgs}>

						<cfthread action="run" name="#threadNamePrefix##threadNumber#" feed=#feed#>

							<cftry>
								<cfset thread.result = feed.matchExportRecords(argumentCollection=feed.matchArgs)>
								<cfcatch>
									<cfset thread.result.isOk=false>
									<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch)>
								</cfcatch>
							</cftry>
						</cfthread>
					</cfif>
				</cfloop>

				<cfset var threadList = "">
				<cfif listLen(threadsUsed)>

					<cfloop list="#threadsUsed#" index="threadNumber">
					 	<cfset threadList = listAppend(threadList,"#threadNamePrefix##threadNumber#")>
					</cfloop>

					<cfthread action="join" name="#threadList#"/>

					<cfloop list="#threadsUsed#" index="threadNumber">
						<cfset var threadResult = cfthread["#threadNamePrefix##threadNumber#"].result>
					 	<cfset log_(label="matchRecordsForExport: Thread #threadNamePrefix##threadNumber#",logStruct=threadResult)>
					</cfloop>
				</cfif>

				<!--- resetting the suffix back to the original --->
				<cfset request.tempTableSuffix = thisTempTableSuffix>

				<!--- update existing records with the proper crm ID --->
				<cfset this.connectorDAO.upsertEntity(tablename=matchTableName,uniqueKeyColumn=entityUniqueKey,entityType=rwBaseEntity)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="matchExportRecords" access="private" returnType="struct" output="false" hint="Call Dynamics to match data and record any matched results we have">
		<cfargument name="api" type="component" required="true" hint="Passed in as this function is called in a thread">
		<cfargument name="entityIdMappedField" type="string" required="true">
		<cfargument name="baseEntity" type="string" required="true">
		<cfargument name="baseEntityObjectKey" type="string" required="true">
		<cfargument name="matchTableName" type="string" required="true">
		<cfargument name="object" type="string" required="true">
		<cfargument name="entityUniqueKey" type="string" required="true">
		<cfargument name="getRowsToMatch" type="query" required="true">
		<cfargument name="tempTableSuffix" type="string" required="true">
		<cfargument name="objectPrimaryKey" type="string" required="true">

		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure()>

		<cftry>
			<cfset request.tempTableSuffix = arguments.tempTableSuffix>
			<cfset var matchingRowsQry = arguments.getRowsToMatch>

			<cfloop query="matchingRowsQry">

				<cftry>
					<cfset result = arguments.api.matchObjectRecords(object=arguments.object,whereClause=matchingRowsQry.matchingWhereClause[currentRow],objectIDColumn=arguments.objectPrimaryKey)>
					<cfif result.isOK and result.successResult.tablename neq "">

						<cfset var doRemoteIDsExistInRW = "">
						<cfset var insertMatchRecords = "">

						<!--- we need to make sure that the msDynamicsID that we are going to update is not already mapped to an existing entity...Not yet sure what to do in the case that it is.. --->
						<cfquery name="doRemoteIDsExistInRW">
							select top 1 #arguments.objectPrimaryKey# as ID
							from #result.successResult.tablename# t
								left join #arguments.baseEntity# b on b.#arguments.baseEntityObjectKey# = t.#arguments.objectPrimaryKey#
							where b.#arguments.baseEntityObjectKey# is null
						</cfquery>

						<cfif doRemoteIDsExistInRW.recordCount>
							<!--- insert into the matches table where the crmID is not already in use --->
							<cfquery name="insertMatchRecords">
								if not exists (select 1 from #arguments.matchTableName# where (#arguments.entityUniqueKey#=#matchingRowsQry[arguments.entityIdMappedField][currentRow]# or #arguments.baseEntityObjectKey# = '#doRemoteIDsExistInRW.ID[1]#'))
									and
									not exists (select 1 from #arguments.baseEntity# where #arguments.baseEntityObjectKey# = '#doRemoteIDsExistInRW.ID[1]#')
								insert into #arguments.matchTableName# (#arguments.baseEntityObjectKey#,#arguments.entityUniqueKey#) values ('#doRemoteIDsExistInRW.ID[1]#',#matchingRowsQry[arguments.entityIdMappedField][currentRow]#)
							</cfquery>
						</cfif>
					</cfif>

					<cfcatch>
						<cfset result.isOk=false>
						<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch)>
					</cfcatch>

				</cftry>
			</cfloop>

			<cfcatch>
				<cfset result.isOk=false>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch)>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="setRemoteIDForNewlyImportedRecords" access="private" output="false" returnType="struct" hint="Sets the Remote ID for newly imported records. Logs any problems in the error table as provided in the arguments.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records that have just been imported. We currently expect the remoteID and the entityId columns to exist on this table.">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table that should be populated with any errors encountered. It contains the following columns: object,objectId,message,field,value,statusCode,connectorResponseID">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#" hint="The meta data of the tablename table. Contains columnList and recordCount">

		<cfset var result = {isOK=true,message=""}>
		<cfset var entityIdMapping = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey)>
		<cfset var entityIdMappedField = entityIdMapping.field neq "" and entityIdMapping.active?entityIdMapping.field:"">

		<!--- only run this if there is a field mapped to the relayware entityID. OOTB, this is RelaywareID__c --->
		<cfif entityIdMappedField neq "" and listFindNoCase(arguments.tableMetaData.columnList,entityIdMappedField) and arguments.tableMetaData.recordCount gt 0>
			<cfset var updateRemoteIdsResult = setObjectRelaywareIDs(object=arguments.object,tablename=arguments.tablename,columnList="#entityIdMappedField#,#this.objectPrimaryKey#",tableMetaData=arguments.tableMetaData,relaywareIDField=entityIdMappedField)>

			<!--- collate the responses --->
			<cfif updateRemoteIdsResult.isOK>
				<cftry>
					<!--- put errored records into the error table --->
					<cfif updateRemoteIdsResult.errorResult.recordCount>
						<cfset var collateResponses = "">

						<cfquery name="collateResponses">
							insert into #arguments.errorTablename# (object,objectId,message,field,value,statusCode,connectorResponseID)
							select distinct '#this.object#', t.#this.objectPrimaryKey#, r.message, '#entityIdMappedField#', t.#entityIdMappedField#,r.statusCode,r.connectorResponseID
							from #updateRemoteIdsResult.errorResult.tablename# r
								inner join #arguments.tableName# t on t.#this.objectPrimaryKey# = r.objectID
								left join #arguments.errorTablename# e on e.objectID = r.objectID
							where e.objectID is null
						</cfquery>

					</cfif>

					<cfcatch>
						<cfset result.isOK = false>
						<cfrethrow>
					</cfcatch>
				</cftry>
			</cfif>

			<cfset structAppend(result,updateRemoteIdsResult)>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="setObjectRelaywareIDs" access="private" output="false" returnType="struct" hint="Sets the Relayware IDs on the remote system. This is just a stub to call the connector specific method.">
		<cfargument name="object" type="string" default="#this.object#" hint="The object for which to get RelaywareIDs for.">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="columnList" type="string" required="true">

		<!--- this function should be overridden so we will never end up in here. This is just a stub --->
		<cfthrow message="Function 'setObjectRelaywareIDs' not set up for #this.connectorType# connector">
	</cffunction>


	<cffunction name="getQueryFieldListForDataImport" access="private" output="true" returnType="string" hint="Get the list of fields to import for an object">
		<cfargument name="object" type="string" required="true" hint="The remote object">
		<cfargument name="modifiedDataFieldsOnly" type="boolean" required="true" hint="Whether we are just getting the IDs of records that have been modified">

		<cfreturn application.getObject("connector.com.connectorUtilities").getQueryFieldListForDataImport(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getObjectWhereClauseForObjectImport" access="private" output="false" hint="Returns the where clause for an object with all white space removed, such as carriage returns, tabs, etc" returnType="string">
		<cfargument name="object" type="string" require="true" hint="The name of the remote object being imported">

		<!--- NJH 2016/06/01 JIRA BF-957 remove any carriage returns, tabs, etc from the where clause that the user might have entered. --->
		<cfreturn reReplace(getConnectorObject(object=arguments.object,relayware=false).whereClause,"\s"," ","ALL")>
	</cffunction>


	<cffunction name="getWhereClauseForObjectImport" access="private" output="false" hint="Returns the various where clauses needed for an object import, including the time, the object where clause, and the parent where clause" returnType="struct">
		<cfargument name="object" type="string" required="true" hint="The name of the remote object being imported">
		<cfargument name="startDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object,direction='import').lastRunUTC#" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object,direction='import').currentTimeUTC#" hint="The end date for when object data should be collected. In UTC">

		<cfparam name="request.ignoreTime" default="false"> <!--- this is set to true if we are initialising data. Currently used for pricebook imports. --->
		<cfset var objectWhereClause = getObjectWhereClauseForObjectImport(object=arguments.object)> <!--- the where clause for the object we're importing --->
		<cfset var timeDateWhereClause = request.ignoreTime?"":getTimeDateWhereClauseForObjectImport(object=arguments.object,startDateTimeUTC=arguments.startDateTimeUTC,endDateTimeUTC=arguments.endDateTimeUTC)> <!--- if we're ignoring time and doing a 'dataload' for example, then ignore the time  --->
		<cfset var currentObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var objectParentObject = currentObject.parentObject> <!--- does this object have a parent object... used to filter current object by parent object where clause... ie. only get contacts for accounts we're synching --->
		<Cfset var parentSelectWhereClause = "">

		<cfif objectParentObject neq "">
			<cfset var parentObject = getConnectorObject(object=objectParentObject,relayware=false)>
			<cfif parentObject.synch and parentObject.whereClause neq "">
				<!--- NJH 2016/06/01 JIRA BF-957 getObjectWhereClauseForObjectImport for parent whereclause so that the where clause is 'cleaned up'. Also add parentheses around parent where clause in case it contains an OR statement --->
				<cfset var parentWhereClause = getObjectWhereClauseForObjectImport(object=objectParentObject,relayware=false)>
				<cfset parentSelectWhereClause = "#timeDateWhereClause neq '' or objectWhereClause neq ''?' and':''# #currentObject.parentKeyField# in (select #this.objectPrimaryKey# from #objectParentObject# where #this.objectPrimaryKey# != '' and (#parentWhereClause#))">
			</cfif>
		</cfif>

		<cfreturn {timeDateWhereClause=timeDateWhereClause,objectWhereClause=objectWhereClause,parentSelectWhereClause=parentSelectWhereClause}>
	</cffunction>


	<cffunction name="getColumnListForDataExport" access="private" output="false" hint="Returns the columns used for export" returnType="string">
		<cfargument name="method" type="string" required="true">

		<cfset var remoteObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var entityIDMapping = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey)>
		<cfset var selectColumnList = this.objectPrimaryKey>
		<cfif entityIDMapping.field neq "" and entityIDMapping.active>
			<cfset selectColumnList = listAppend(selectColumnList,entityIDMapping.field)> <!--- just need IDs when deleting records --->
		</cfif>

		<!--- set the select list for the various methods. Some fields are just default values, in which case they are only available in a create. The same for fields that are synching only one way. --->
		<cfif arguments.method neq "delete">
			<cfset selectColumnList = remoteObject["export#arguments.method#ColumnList"]>
		</cfif>

		<cfreturn application.getObject("connector.com.connector").cleanColumnListForExport(method=method,columnList=selectColumnList,remoteObjectKey=this.objectPrimaryKey,object=this.mappedObject)>
	</cffunction>


	<cffunction name="getExportRecords" access="private" output="false" hint="Puts the export records for given method into a table">
		<cfargument name="method" type="string" required="true">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The name of the remote object to be exported">
		<cfargument name="errorTableName" type="string" default="" hint="The name of the tables that will hold any export errors. Used to pass through for getExportQueryInfo override function.">

		<cfset var entityIDMapping = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey)>
		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure()>
		<cfset var exportRecordsResult = {}>
		<cfset var putExportRecordsIntoTempTable = "">
		<cfset var relaywareIdColumn = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)>
		<cfset var isRelaywareIdMapped = entityIDMapping.field neq "" and entityIDMapping.active?true:false>

		<!--- set the select list --->
		<cfset var exportSql = getExportQueryInfo(method=arguments.method,object=arguments.object,errorTablename=arguments.errorTableName)>
		<!--- if headquarters is a mapped field, we want to create them first as other accounts will depend on these existing --->
		<cfset var HQField = application.getObject("connector.com.connectorUtilities").getMappedHeadquartersField().field>

		<cfquery name="putExportRecordsIntoTempTable" result="exportRecordsResult">
			select distinct ROW_NUMBER() over (order by <cfif HQField neq "" and listFindNoCase(exportSql.selectColumnList,HQField)>v.#HQField#,</cfif>v.#relaywareIDColumn#) AS rowNumber, #replace(exportSql.selectColumnList,",",",v.","ALL")#
			<!--- if the RW field is not mapped or it's not been defined as updateable when we're doing an update, then add it to the select so that we have a unique handle on the records --->
			<cfif not isRelaywareIdMapped or (arguments.method eq "update" and structKeyExists(application.connector.objectMetaData[arguments.object].fields,entityIDMapping.field) and not application.connector.objectMetaData[arguments.object].fields[entityIDMapping.field].updateable)>,v.#relaywareIDColumn#</cfif>
				into #result.successResult.tablename#
			from #getViewForObjectSynch(object=arguments.object)# v
				#exportSql.join#
			where
				#exportSql.whereClause#
		</cfquery>

		<cfset structAppend(result,exportRecordsResult)>
		<cfset result.tablename = result.successResult.tablename>
		<cfset result.selectColumnList = exportSql.selectColumnList>
		<cfset log_(label="Export: #arguments.object# Records to #arguments.method#",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="getExportQueryInfo" access="private" returnType="struct" output="false" hint="Returns some sql for export">
		<cfargument name="method" type="string" required="true">
		<cfargument name="errorTableName" type="string" default="" hint="The name of the tables that will hold any export errors. Used to pass through for getExportQueryInfo override function.">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The name of the remote object to be exported">

		<cfset var result = {whereClause="",join="",selectColumnList = getColumnListForDataExport(method=arguments.method,object=arguments.object)}>

		<cfsavecontent variable="result.whereClause">
			<cfoutput>
				isDeleted =
				<cfif arguments.method neq "delete">
					0 and v.#this.objectPrimaryKey# is <cfif method eq "update">not </cfif>null
				<cfelse>
					1
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn result>
	</cffunction>


	<cffunction name="runPreExport" access="private" output="false" returnType="struct" hint="Runs some pre-export functions">

		<!--- handle to run any object specific pre-import functions which could be set up in customised cfc --->
		<cfset var result = runFunctionIfExists(functionName="runPre#this.baseEntity#Export")>

		<!--- TODO: log any errors so that we don't process any records that may have errored in a preExport function!! --->
		<cfreturn result>
	</cffunction>


	<cffunction name="exportData" access="private" returnType="struct" output="false" hint="Export Relayware data to Remote CRM system.">

		<cfset log_(label="Begin: Export data",logStruct=arguments)>

		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure(returnErrorTable=true)>
		<cfset var exportRecords = {}>
		<cfset var exportResult = {}>
		<cfset var successTableName = result.successResult.tablename>
		<cfset var errorTableName = result.errorResult.tablename>
		<cfset var exportObject = "">
		<cfset var objectsToExport = getConnectorObject(object=this.mappedObject,relayware=false).objectList>
		<cfset var relaywareIdColumn = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)> <!--- if the RelaywareId field has not been mapped, then get the uniqueKey as the RW Id field --->
		<cfset var method = "">
		<cfset var collateResponses = "">
		<cfset var createSuccessTable = "">

		<cfquery name="createSuccessTable">
			create table #successTableName# (
				#this.baseEntityPrimaryKey# varchar(20),
				#this.baseEntityObjectKey# varchar(50),
				action varchar(100)
			)
		</cfquery>

		<!--- we have to loop over the object list. For example, a RW opportunity is mapped to a SF opportunity, opportunityPartner and opportunityContactRole object. So, exporting a RW opportunity
			needs to export each one of these objects --->

		<cfset runPreExport()>

		<cfloop list="#objectsToExport#" index="exportObject">
			<cfloop list="delete,Create,Update" index="method"> <!--- the order is important in this list as we break if on the update method on a non-primary object --->

			<cftry>
				<cfset var preObjectMethodResult = runFunctionIfExists(functionName="runPre#exportObject##method#",successTableName=successTableName)>
				<cfset var theRemoteObject = getConnectorObject(object=exportObject,relayware=false)>
				<cfset log_(label="Export: #exportObject# data pre-filter",logStruct=theRemoteObject)>
				<cfset exportRecords = getExportRecords(method=method,object=exportObject,errorTableName=errorTableName)>

				<cfif exportRecords.recordCount gt 0>

					<!--- if we have a specific create/delete/update method for an object.. At the moment, used for Salesforce opportunity role tables --->
					<cfset var exportResult = runFunctionIfExists(functionName="#method##exportObject#",exportTableName=exportRecords.tablename)>

					<!--- run the generic export method --->
					<cfif structKeyExists(exportResult,"exists") and not exportResult.exists>

						<cfset var fromRow = 0>
						<cfset var toRow = 0>
						<cfset var recordsProcessed = 0>

						<cfloop condition="recordsProcessed lt exportRecords.recordCount">

							<!--- NJH 2016/09/14 JIRA PROD2016-2178 deal with varying batch sizes, as SF API calls can not be bigger than 52 MB. Some attachments eat this allowance fairly quickly and so need to go across in smaller chunks --->
							<cfset var batchSize = application.getObject("connector.com.connector").getBatchSizeForExport(baseEntity=this.baseEntity,tablename=exportRecords.tablename,rowNumberLastExported=recordsProcessed,method=method)>

							<cfset fromRow = toRow>
							<cfset toRow = fromRow+batchSize>
							<cfset recordsProcessed = recordsProcessed+batchSize>

							<!--- continue if we're a primary object or if we're dealing with contact and partner roles, as then we only do a create --->
							<cfif method eq "delete">
								<cfset var getDeletedRecords = "">

								<cfquery name="getDeletedRecords">
									select top #batchSize# #this.objectPrimaryKey# as ID from #exportRecords.tablename# where rowNumber > #fromRow# order by rowNumber
								</cfquery>
								<cfset exportResult = this.api.send(method="delete",object=exportObject,ids=listToArray(valueList(getDeletedRecords.ID)))>
							<cfelse>

								<cfset exportResult = this.api.send(method=lcase(method),object=exportObject,viewName=exportRecords.tablename,columnList=exportRecords.selectColumnList,whereClause="rowNumber > #fromRow# and rowNumber <= #toRow#")>
							</cfif>

							<cfset log_(label="Export: Send Method Response for #exportObject# #method#",logStruct=exportResult)>

							<!--- when trying to do a bulk create with more than 200 records, we get an error for the lot. Need to add this to the errors table as a general error. In this case, the call is not OK. --->
							<cfif exportResult.isOK>
								<cftry>
									<!--- put errored records into the success table --->
									<cfif exportResult.result.recordCount>

										<cfset var collateResponseResult = runFunctionIfExists(functionName="collateResponseFor#method##exportObject#",successTableName=successTableName,errorTableName=errorTableName,object=exportObject,resultTableName=exportResult.result.tablename,exportDataTableName=exportRecords.tablename,rowNumberIndex=fromRow)>

										<!--- run the generic collate query if object specific one does not exist --->
										<cfif structKeyExists(collateResponseResult,"exists") and not collateResponseResult.exists>

											<cfif not structKeyExists(exportResult.result,"columnList")>
												<cfset structAppend(exportResult.result,application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=exportResult.result.tablename),false)>
											</cfif>

											<!--- TODO: put in some comments here. Chances are that if this query errors, then the RWID field on SF is not updateable or createable and so does not exist in the incoming table --->
											<cfquery name="collateResponses">
												insert into #successTableName# (#this.baseEntityPrimaryKey#,#this.baseEntityObjectKey#,action)
												select distinct v.#relaywareIDColumn#,t.ID, '#method#' as [action]
												from #exportResult.result.tablename# t
													inner join #exportRecords.tablename# v on <cfif method neq "delete">t.rowNumber + (#fromRow#) = v.rowNumber<cfelse>v.#this.objectPrimaryKey# = t.ID</cfif>
												where success = 1

												insert into #errorTableName# (object,objectId,message,field,value,statusCode,connectorResponseID)
												select distinct '#this.baseEntity#', cast(cast(v.#relaywareIDColumn# as float) as int),
												<cfif listFindNoCase(exportResult.result.columnList,"message")>message<cfelseif listFindNoCase(exportResult.result.columnList,"errors")>errors<cfelse>null</cfif>,
												<cfif listFindNoCase(exportResult.result.columnList,"fields")>fields<cfelse>null</cfif>,
												null,
												<cfif listFindNoCase(exportResult.result.columnList,"statusCode")>statusCode<cfelse>null</cfif>,
												t.connectorResponseID
												from #exportResult.result.tablename# t
													inner join #exportRecords.tablename# v on <cfif method neq "delete">t.rowNumber + (#fromRow#) = v.rowNumber<cfelse>v.#this.objectPrimaryKey# = t.ID</cfif>
												where success = 0
											</cfquery>
										</cfif>
									</cfif>

									<cfcatch>
										<cfrethrow>
									</cfcatch>
								</cftry>
							</cfif>
						</cfloop>
					</cfif>
				</cfif>

				<!--- drop the table that we've just used, as it will get re-created again --->
				<cfset var dropTmpExportTable = "">

				<cfquery name="dropTmpExportTable">
					drop table #exportRecords.tablename#
				</cfquery>

				<!--- get the RW ID for the newly created records so that we can update the RW tables with the SF ID. This at the moment gets updates as well as creates. Need to work out whether I can get just creates other than passing in a list of IDs
					if doing an update, we get the ID's returned, which we already have on RW, so no need to getUpdated records
				--->
				<!--- <cfif method eq "create" and exportRecords.recordCount gt 0 and exportObject eq this.object>

					<cfset var getRelaywareIDResult = getRelaywareIdsForNewlyCreatedRecords(object=exportObject)>

					<!--- update the SalesForceID column on the success table for new creates. This could use the upsertEntity function, but done have a query for now --->
					<cfif getRelaywareIDResult.successResult.recordCount>
						<cfset var setCrmIDForCreatedRecords = "">

						<cfquery name="setCrmIDForCreatedRecords">
							update #successTableName# set #this.baseEntityObjectKey# = t.#this.objectPrimaryKey#
							from #getRelaywareIDResult.successResult.tablename# t
								inner join #successTableName# v on
									v.#this.baseEntityPrimaryKey# = cast(cast(t.#entityIDMappedField# as float) as int)
							where #this.baseEntityObjectKey# is null
								and [action] = 'create'
						</cfquery>
					</cfif> --->

				<cfif method eq "delete">
					<cfset removeCrmIDForDeletedRecords(tablename=successTableName,objectIDColumn=this.baseEntityObjectKey,whereClause="[action] = 'delete'")>
				</cfif>

				<cfcatch>
					<cfrethrow>
				</cfcatch>
			</cftry>
			</cfloop>

			<cfset var preObjectMethodResult = runFunctionIfExists(functionName="runPost#exportObject#Export",successTableName=successTableName,errorTableName=errorTableName)>
		</cfloop>

		<!--- collate the response so that all errors and all successes are in a single table. Add an action column to indicate whether record was added or not --->
		<cfset var successResult = {}>
		<cfset var errorsResult = {}>
		<cfset var getErrors = "">
		<cfset var getSuccesses = "">

		<!--- adding some standard columns --->
		<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=successTableName,columnName="objectID",defaultValueFromColumn=this.baseEntityPrimaryKey)>
		<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=successTableName,columnName="object",defaultValue=this.object)>

		<cfquery name="getErrors" result="errorsResult">
			select * from #errorTableName#
		</cfquery>

		<cfquery name="getSuccesses" result="successResult">
			select * from #successTableName#
		</cfquery>

		<cfset structAppend(result.errorResult,errorsResult)>
		<cfset structAppend(result.successResult,successResult)>

		<cfset log_(label="End: Export data",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="getQueryInfoForDataImport" access="private" output="false" hint="Returns information needed to run a query against remote system to bring in import data" returnType="struct">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">
		<cfargument name="startDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object,direction='import').lastRunUTC#" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object,direction='import').currentTimeUTC#" hint="The end date for when object data should be collected. In UTC">
		<cfargument name="getModificationDataOnly" type="boolean" default="false" hint="Get only fields necessary to compare with records to be exported. (ie. ID, lastModifiedDate). This could maybe be changed to a fieldlist.">

		<cfset var result = {isOK=true,message="",whereClauseArray=arrayNew(1),fieldList=getQueryFieldListForDataImport(object=arguments.object,modifiedDataFieldsOnly=arguments.getModificationDataOnly)}>
		<cfset result.whereClauseArray[1] = getFullWhereClauseForObjectImportQuery(argumentCollection=arguments)>
		<cfreturn result>
	</cffunction>


	<cffunction name="getDataToImport" access="private" returnType="struct" hint="Collect records that have been modified so that they get picked up in an export" output="false">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">
		<cfargument name="startDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object,direction='import').lastRunUTC#" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object,direction='import').currentTimeUTC#" hint="The end date for when object data should be collected. In UTC">
		<cfargument name="getModificationDataOnly" type="boolean" default="false" hint="Get only fields necessary to compare with records to be exported. (ie. ID, lastModifiedDate). This could maybe be changed to a fieldlist.">
		<cfargument name="additionalWhereClause" type="string" default="" hint="Any other where clause that we may want to append to the query."> <!--- For kaspersky, we need to get all opportunityLineItems for opportunties that have just imported. Normally this is done through the parent relationship(?) but because of query issues, the opportuityLineItem is independent of the opportunity--->

		<cfset var result = {isOk=true,message=""}>
		<cfset var thisObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var importObject = "">
		<cfset var objectList = thisObject.objectList>
		<cfset var loopIndex = 0>
		<cfset var objectDataTable = getObjectDataTable(object=arguments.object)>

		<cfset objectList = listPrepend(listDeleteAt(objectList,listFindNoCase(objectList,arguments.object)),arguments.object)> <!--- stick the main object at the beginning of the list --->

		<cfloop list="#objectList#" index="importObject">

			<cfset var queryInfo = getQueryInfoForDataImport(object=importObject,argumentCollection=arguments)>
			<cfset var whereClauseArray = queryInfo.whereClauseArray>
			<cfset var deleteExistingDataInBaseTable = "">

			<cfquery name="deleteExistingDataInBaseTable">
				truncate table #getObjectDataTable(object=importObject)#
			</cfquery>

			<!--- we will have two iterations for objects that contain non-primary objects... --->
			<cfloop from=1 to=#arrayLen(whereClauseArray)# step=1 index="loopIndex">

				<!--- query changed data and put data into the data tables --->
				<cfset var modifiedDataResult = getDataToImportFromWhereClause(object=importObject,whereClause=whereClauseArray[loopIndex],loopIndex=loopIndex,fieldList=queryInfo.fieldList)>

				<cfset log_(label="Get Data To Import",logStruct=modifiedDataResult)>

				<!--- if we have related data, then we need to put the baseIds into the queue so that it gets processed. This currently means that we need to get the base data, as otherwise
					the base record will contain a lot of nulls which means that all the other fields will get set to null on RW.
				--->
				<cfset var postDataImportResult = runFunctionIfExists(functionName="postDataImportForWhereClause#loopIndex#",object=importObject,successTableName=modifiedDataResult.successResult.tablename)>

				<!--- run the generic functionality --->
				<cfif structKeyExists(postDataImportResult,"exists") and not postDataImportResult.exists>

					<!--- if we're the primary object, then go get deleted records. For Salesforce, the queryAll gets deleted records (for most objects!) so we're duplicating effort slightly. Hopefully doesn't affect performance to much--->
					<!--- Old COMMENT -> TODO: do we really need to do this (ie. queryAll should get deleted records as well for most objects!!).
							It appears that deleted opportunityLineItems and pricebookEntries don't get picked up in a queryAll. We need to work out which records do not get picked up by queryAll --->
					<cfif importObject eq arguments.object>
						<cfset structAppend(result,modifiedDataResult)>

						<cfset var deletedObjectsResult = getDeletedObjects(argumentCollection=arguments,object=importObject)>
						<cfset var collateResults = "">
						<cfset var importResults = {}>
						<cfset log_(label="Get Deleted Data To Import",logStruct=deletedObjectsResult)>

						<!--- if we both have objects that were deleted and objects that have changed, then collate the response --->
						<cfif deletedObjectsResult.successResult.tablename neq "">

							<cfquery name="collateResults" result="importResults">
								insert into #objectDataTable# (object,#this.objectPrimaryKey#,isDeleted,lastModifiedDate)
								select '#arguments.object#',t.#this.objectPrimaryKey#,1,cast(t.deletedDate as datetime)
								from #deletedObjectsResult.successResult.tablename# t
									inner join #this.baseEntity# e on e.#this.baseEntityObjectKey# = t.#this.objectPrimaryKey#
									left join #objectDataTable# o on o.#this.objectPrimaryKey# = t.#this.objectPrimaryKey#
								where
									o.#this.objectPrimaryKey# is null

								select * from #objectDataTable#
							</cfquery>

							<cfset structAppend(modifiedDataResult.successResult,importResults,true)>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>

		<!--- Here we are removing any records from the queue that are deleted but that don't have any tie to a RW record. A queryAll will pick up all records, and so here we are just doing a bit of tidy up, so that we don't
			have items that are 'stuck' --->
		<cfset removeIncomingOrphanDeletionsFromDataTable()>

		<cfset var getData = "">

		<cfquery name="getData" result="modifiedDataResult">
			select * from #objectDataTable#
		</cfquery>

		<cfset structAppend(result.successResult,modifiedDataResult)>

		<cfset result.successResult.tablename = objectDataTable> <!--- the table holding the base object data is the table that we're working with' --->

		<cfreturn result>
	</cffunction>

</cfcomponent>