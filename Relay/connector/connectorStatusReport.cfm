<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorStatusReport.cfm
Author:			NJH
Date started:	2014/07/01

Description:	Show records in the connector queue at various stages

Amendment History:

Date 		Initials 	What was changed
2015/02/09	NJH			Jira Task Fifteen-76 Add sortOrder to the query outputting the results
2015/04/13	NJH			Showing synching status for pending records as well as a pending record could be suspended and we would want to be able to unsuspend them in the report.
Possible enhancements:


 --->

<cf_includeJavascriptOnce template="/javascript/extExtension.js">
<cf_includeJavascriptOnce template="/javascript/viewEntity.js">
<cf_includeJavascriptOnce template="/connector/js/connector.js">
<cf_includeJavascriptOnce template="/javascript/lib/popover/jquery.webui-popover.js">
<cf_includeCSSOnce template="/javascript/lib/popover/jquery.webui-popover.min.css">
<cf_includeCSSOnce template="/connector/css/connector.css">

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="lastModified desc">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="fromDate" default="">
<cfparam name="toDate" default="">
<cfparam name="entityType" default="">
<cfparam name="direction" default="">
<cfparam name="connectorType" default="#application.com.settings.getSetting("connector.type")#">
<cfparam name="dashboardStatus" default="success">
<cfparam name="connectorMappingField" default="">
<cfparam name="message" default="">

<cfif structKeyExists(form,"frmShowAll")>
	<cfset fromDate = "">
	<cfset toDate = "">
</cfif>

<cfset argStruct = {direction=direction,dashboardStatus=dashboardStatus}>

<cfset pageTitle = "Connector #dashboardStatus# Report">
<cfif entityType neq "">
	<cfset argStruct.entityTypeList = entityType>
	<cfset entityType = application.com.regExp.camelCaseToSpaces(string=entityType)>
	<cfset pageTitle = pageTitle & " for #entityType# Records">
<cfelse>
	<cfset connectorEntities=application.getObject("connector.com.connector").getPrimaryConnectorObjects(direction="Export",connectorType=connectorType)>
	<cfset entityType = valueList(connectorEntities.object_relayware)>
</cfif>

<cfif direction eq "export">
	<cfset pageTitle = pageTitle&", Out to #connectorType#">
<cfelseif direction eq "import">
	<cfset pageTitle = pageTitle&", In from #connectorType#">
</cfif>
<cfif fromDate neq "">
	<cfset pageTitle = pageTitle&", from #dateFormat(fromDate,"full")#">
	<cfset argStruct.fromDate = fromDate>
</cfif>
<cfif toDate neq "">
	<cfset pageTitle = pageTitle&", to #dateFormat(toDate,"full")#">
	<cfset toDatePlusOneDay = dateAdd("d",1,toDate)>
	<cfset argStruct.toDate = toDatePlusOneDay>
</cfif>
<cfset application.com.request.setTopHead(pageTitle = pageTitle)>
<cfif (message neq "") or (connectorMappingField neq "")>
	<cfset argStruct.returnErrorData = true >
</cfif>

<cfif structKeyExists(url,"action") and structKeyExists(form,"frmRowIdentity")>
	<cfif url.action eq "syncNow">
		<cfset application.getObject("connector.com.connector").syncNow(queueIDList=form.frmRowIdentity,direction=direction,entityList=entityType)>
	<cfelseif url.action eq "unsuspend">
		<cfset application.getObject("connector.com.connector").resetSynchAttempt(queueIDList=form.frmRowIdentity)>
	<cfelseif url.action eq "deleteError">
		<cfset application.getObject("connector.com.connector").deleteErrors(queueIDList=form.frmRowIdentity)>
	</cfif>
</cfif>

<cfset hideTheseColumns = "">
<cfset rowIdentityColumnName = "">
<cfset comTableFunction.qFunctionList = QueryNew( "functionID,functionName,securityLevel,url,windowFeatures" )>
<cfset booleanFormat = "Deleted">
<cfset showTheseColumns = "Name,RelaywareID,RemoteID,Synch_Date,Deleted">
<cfset keyColumnURLList = " ">
<cfset keyColumnOnClickList = "viewEntityDetails('##Entity##'*comma##relaywareID##);return false;">
<cfset FilterSelectFieldList = "">
<cfset keyColumnList = "Name">
<cfset dateTimeFormat="Synch_Date">

<cfif dashboardStatus eq "success">
	<cfset connectorRecords = application.getObject("connector.com.connector").getSynchedRecords(argumentCollection=argStruct)>
<cfelse>

	<cfset showTheseColumns = "Name,RelaywareID,RemoteID,lastModified,isDeleted,queueStatus">
	<cfset recordType = dashboardStatus eq "failed"?"error":"dependent">
	<cfset isDashboardStatusFailed = dashboardStatus eq "failed"?true:false>
	<cfinclude template="connectorReportFunctions.cfm">
	<cfset rowIdentityColumnName="queueID">
	<cfset showTheseColumns = listAppend(showTheseColumns,recordType)>
	<cfset keyColumnList = listAppend(keyColumnList,recordType)>
	<cfset booleanFormat="isDeleted,hasDependency">
	<cfset dateTimeFormat="LastModified">
	<cfset keyColumnOnClickList = listAppend(keyColumnOnClickList,"javascript:showRelatedRecords(this*comma'##queueID##'*comma'#recordType#'*comma'#connectorType#');return false;")>
	<cfset keyColumnURLList = listAppend(keyColumnURLList," ")>

	<cfset connectorRecords = application.getObject("connector.com.connector").getQueueRecords(argumentCollection=argStruct)>
	<cfquery name="connectorRecords" dbtype="query">
		select *, '' as #recordType#
		from connectorRecords
		where
		<cfif recordType eq "dependent">hasDependency<cfelse>errored</cfif>=0
		union
		select *, '<img src="/images/icons/bullet_arrow_down.png">' as #recordType#
		from connectorRecords
		where
		<cfif recordType eq "dependent">hasDependency<cfelse>errored</cfif>=1
		<cfif connectorMappingField neq "">and field=<cfqueryparam cfsqltype="cf_sql_varchar" value="#connectorMappingField#" ></cfif>
		<cfif message neq "">and message=<cfqueryparam cfsqltype="cf_sql_varchar" value="#message#" ></cfif>
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>

<!--- default the search comparitor to an equal sign --->
<cfif not structKeyExists(form,"frmSearchComparitor") or form.frmSearchComparitor eq "">
	<cfset form.frmSearchComparitor = 2>
</cfif>

<!--- <cfif listFindNoCase(connectorRecords.columnList,"location")>
	<cfset showTheseColumns = listInsertAt(showTheseColumns,2,"Location")>
</cfif>
<cfif listFindNoCase(connectorRecords.columnList,"organisation")>
	<cfset showTheseColumns = listInsertAt(showTheseColumns,3,"Organisation")>
</cfif> --->

<cfif direction eq "">
	<cfset showTheseColumns = listPrepend(showTheseColumns,"Direction")>
	<cfset showTheseColumns = listPrepend(showTheseColumns,"Entity")>
	<cfset FilterSelectFieldList = "Entity,Direction">
</cfif>

<cfset checkBoxFilterName = "">
<cfif fromDate neq "" or toDate neq "">
	<cfset checkBoxFilterName = "frmShowAll">
</cfif>

<cf_tableFromQueryObject
	queryObject="#connectorRecords#"
	queryName="connectorRecords"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList=#keyColumnURLList#
	keyColumnOnClickList=#keyColumnOnClickList#
	keyColumnKeyList="relaywareID"
	keyColumnOpenInWindowList="no"
 	hideTheseColumns="#hideTheseColumns#"
	showTheseColumns="#showTheseColumns#"
	columnTranslation="true"
	columnTranslationPrefix="phr_connector_#connectorType#_report_"
	searchColumnList="RelaywareID,RemoteID,Name"
	allowColumnSorting="yes"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="Remove Date Filter"
	FilterSelectFieldList="#FilterSelectFieldList#"
	FilterSelectFieldList2="#FilterSelectFieldList#"
	useInclude="false"
	dateTimeFormat="#dateTimeFormat#"
	booleanFormat="#booleanFormat#"
	functionListQuery="#comTableFunction.qFunctionList#"
	rowIdentityColumnName="#rowIdentityColumnName#"
>