<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorAPI.cfc
Author:			NJH
Date started:	02-07-2015

Description:	File to do with API functionality

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/07/02			NJH			in parseSFDCResponse, join to dataobjectTable and only insert into this table where values don't already exist. We have a problem were unique constraints were being violated.
2015/07/13			NJH			Change the join to the dataObject table in parseSFDCResponse... seemed to be causing problems which I couldn't get to the bottom of.. so re-jigged it which means it's now running
								as expected (not hanging). Am also dropping and recreating indexes on the object tables.. Not sure if needed, but thought it might help speed things up even that much more.
2015/07/15			NJH			Avoid data truncation by getting length of object fields when bringing in data from Salesforce.
2015/07/24			NJH			Took out the guts of parseSFDCResponse and put it into a function in connectorSalesforce.cfc. This was because I needed the functionality elsewhere as well. It was always on the TODO list, and have now done it.
2015/09/17			NJH			Changed the endpoint from www.salesforce.com to login.salesforce.com, as the old endpoint will be removed.
2016/12/09          NJH/DAN     Case 453446 / PROD2016-2947 - remove logging dataStruct as it gets quite large due to sendSoap and we dont need it anyway becaue we have a valid error message / diagnostics

Possible enhancements:


 --->


<cfcomponent name="connectorAPI" output="false">

	<cffunction name="createSFDCSession" access="public" hint="Starts a new SFDC session" output="false">
		<cfargument name="username" type="string" default="#application.com.settings.getSetting('connector.salesForce.apiUser.username')#">
		<cfargument name="password" type="string" default="#application.com.settings.getSetting('connector.salesForce.apiUser.password')#">

		<!--- <cfset var userStruct = application.com.settings.getSetting("connector.salesForce.apiUser")> --->
		<cfset var getLoginDetails = "">

		<cfif arguments.username eq "" or arguments.password eq "">
			<cfthrow message="FAIL: SFDC Username and Password not set.">
		</cfif>

		<cfset var loginAttempt = send(object="", method="login", username=arguments.username,password=arguments.password)>

		<cfif loginAttempt.isOK>
			<cfquery name="getLoginDetails">
				select serverUrl,sessionID,userID from #loginAttempt.successResult.tablename#
			</cfquery>

			<cfset application.connector.api.serverUrl = getLoginDetails.serverUrl[1]>
			<cfset application.connector.api.sessionID = getLoginDetails.sessionID[1]>
			<cfset application.connector.api.userId = getLoginDetails.userID[1]>
		<cfelse>
			<cfthrow message="Unable to login with SalesForce API" detail="#loginAttempt.message#">
		</cfif>
	</cffunction>


	<cffunction name="callSalesforceAPI" access="private" hint="makes the actual call to Salesforce" output="false" returnType="struct">
		<cfargument name="method" type="String" required="true" hint="The SalesForce method to call">
		<cfargument name="sendSoap" type="xml" required="true" hint="The SOAP packet to send to Salesforce">
		<cfargument name="attempt" type="numeric" default="1" hint="Which attempt are we on?">

		<cfscript>
			var urlToCall = "";
			var loginUrl = "https://login.salesforce.com/services/Soap/u/30.0";
			var sendResult={};
		</cfscript>

		<!--- NJH 2016/05/11 JIRA PROD2016-934 - if a staging site, work out which SF environment to point to --->
		<cfif application.testSite eq 1 and application.com.settings.getSetting("connector.salesforce.synchWithEnvironment") neq "production"><!---sandbox access--->
			<cfset loginURL = "https://test.salesforce.com/services/Soap/u/30.0">
		</cfif>

		<cfif arguments.method eq "login">
			<cfset urlToCall = loginUrl>
		<cfelse>
			<!--- check if session exists. Create one if not --->

			<cfif not structKeyExists(application.connector,"api") or not structKeyExists(application.connector.api,"sessionID") or application.connector.api.sessionID eq "">
				<cfset createSFDCSession()>
			</cfif>
			<cfset urlToCall = application.connector.api.serverUrl>
		</cfif>

		<cfhttp url="#urlToCall#" method="post" timeout="60" result="sendResult">
			<cfhttpparam type="xml" value="#trim(arguments.sendSoap)#" />
			<cfhttpparam type="header" name="SOAPAction" value="#arguments.method#" />
		</cfhttp>

		<!--- to try and deal with 'connection failures' when making a SF call --->
		<cfif sendResult.fileContent eq "Connection Failure" and arguments.attempt lte 3>
			<cfset sendResult = callSalesForceAPI(argumentCollection=arguments,attempt=arguments.attempt+1)>
		</cfif>

		<cfreturn sendResult>
	</cffunction>


	<!--- ********************************************************************************************
	this is the master function that calls all of the methods to build a SOAP request and handle the SOAP response
	 ******************************************************************************************** --->
	<cffunction name="Send" access="public" output="false" returntype="Struct" hint="Runs a SalesForce method">
		<cfargument name="object" type="String" required="false" hint="The SalesForce object on which the operation is run">
		<cfargument name="method" type="String" required="true" hint="The SalesForce method to call">
		<cfargument name="createTempTableForResponse" type="boolean" default="true" hint="Put response in temp table rather than core table. Only data for import should go in core table.">
		<cfargument name="throwErrorOn500Response" type="boolean" default="false"> <!--- if we get a 500 from salesforce, throw an error or continue processing. THis is only used when we do an initial query --->  <!--- 2013-03-13 NJH Case 434168 throwErrorOn500Response --->

		<cfscript>
			var sendResult={};
			var startSoap="";
			var endSoap="";
			var sendSOAP="";
			var methodCall="";
			var result={isOk=false,errorCode="",message=""};
			var errorStruct = structNew();
			var oldSessionID = "";
			var sendResultResponse = "";
			var sendResponseArray = arrayNew(1);
			var responseFault = "";
			var queryMoreArgs = structNew();
			var queryMoreResult = structNew();
			var assignmentRuleHeaderUseDefaultRule = true; // defaults to true

			if (arguments.method eq "getObjectFieldMetaData"){
				arguments.method = "describeSObjects";
			} else if (arguments.method eq "getRemoteObjects"){
				arguments.method = "describeGlobal";
			}

			arguments.direction = "E";
			if (listFindNoCase("query,queryMore,queryAll,getUpdated,getDeleted,retrieve",arguments.method)) {
				arguments.direction = "I";
			}

			// in the mappings, etc, we have a partner defined as an opportunity partner. We can run the query, describe and retrieve methods against this object, but if we want to delete and create, we need to refer to it as a partner
			if (listFindNoCase("delete,create",arguments.method) and arguments.object eq "opportunityPartner") {
				arguments.object = "partner";
			}
			structAppend(errorStruct,arguments); //pass in the arguments as part of the error struct
		</cfscript>

		<!--- guard against the session not existing, as it can be deleted --->
		<cfif (not structKeyExists(application.connector,"api") or not structKeyExists(application.connector.api,"sessionID")) and arguments.method neq "login">
			<cftry>
				<cfset createSFDCSession()>
				<cfcatch>
					<cfset result.message = cfcatch.message>
					<cfreturn result>
				</cfcatch>
			</cftry>
		</cfif>

		<cfset var salesforceApiHeaders = application.com.settings.getSetting("connector.salesforce.APIHeaders")>

        <cfset var connectorSF =  application.getObject("connector.salesforce.com.connectorSalesforce")>
        <cfif structKeyExists(connectorSF, "override_assignmentRuleHeaderUseDefaultRule")>
            <!--- Some clients want to override assignmentRuleHeaderUseDefaultRule. Create a function in custom connectorSalesforce.cfc for this. --->
            <cfset assignmentRuleHeaderUseDefaultRule = connectorSF.override_assignmentRuleHeaderUseDefaultRule(argumentcollection=arguments)>
        </cfif>

		<cfoutput>
			<cfsavecontent variable="startSoap"><?xml version="1.0" encoding="UTF-8"?>
			<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
			xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soapenv:Header>
			<cfif arguments.method eq "login"><ns1:LoginScopeHeader xmlns:ns1="urn:partner.soap.sforce.com">
			<ns1:organizationId></ns1:organizationId>
			</ns1:LoginScopeHeader>
			<ns1:CallOptions xmlns:ns1="urn:partner.soap.sforce.com">
			<ns1:client></ns1:client>
			</ns1:CallOptions>
			<cfelse><ns1:SessionHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
			<ns1:sessionId>#application.connector.api.sessionID#</ns1:sessionId>
			</ns1:SessionHeader>
			<ns1:AssignmentRuleHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
			<ns1:useDefaultRule>#assignmentRuleHeaderUseDefaultRule#</ns1:useDefaultRule>
			</ns1:AssignmentRuleHeader>
			<ns1:AllOrNoneHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
			<ns1:allOrNone>false</ns1:allOrNone>
			</ns1:AllOrNoneHeader>
			<!--- START: 13/01/2016 Case 446364 --->
            <ns1:EmailHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
                <ns2:triggerAutoResponseEmail xmlns:ns2="urn:partner.soap.sforce.com">#salesforceApiHeaders.emailHeader.triggerAutoResponseEmail#</ns2:triggerAutoResponseEmail>
                <ns2:triggerOtherEmail xmlns:ns2="urn:partner.soap.sforce.com">#salesforceApiHeaders.emailHeader.triggerOtherEmail#</ns2:triggerOtherEmail>
                <ns2:triggerUserEmail xmlns:ns2="urn:partner.soap.sforce.com">#salesforceApiHeaders.emailHeader.triggerUserEmail#</ns2:triggerUserEmail>
            </ns1:EmailHeader>
            <!--- END: 13/01/2016 Case 446364 --->
			<cfif application.com.settings.getSetting('connector.allowDataTruncation')><ns1:AllowFieldTruncationHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
		    <ns1:allowFieldTruncation>true</ns1:allowFieldTruncation>
			</ns1:AllowFieldTruncationHeader></cfif></cfif>
			</soapenv:Header>
			<soapenv:Body>
			<#arguments.method# xmlns="urn:partner.soap.sforce.com">
			</cfsavecontent>

			<cfsavecontent variable="endSoap">
			</#arguments.method#>
			</soapenv:Body>
			</soapenv:Envelope></cfsavecontent>
		</cfoutput>

		<!--- check function for method exists --->
		<cftry>
			<cfif structKeyExists(variables,arguments.method)>
				<cfset methodCall = variables[arguments.method]>
			<cfelse>
				<cfoutput>Method #arguments.method# does not exist.</cfoutput><cf_abort>
			</cfif>

	 		<cfcatch type="any">
				<cfoutput>Method #arguments.method# does not exist.</cfoutput><cf_abort>
			</cfcatch>
		</cftry>

		<!--- call function for method - function will deal with different object specific behaviours - will need extending to be passed additional params
			using arguments collection forvariables like querystring for queries to execute etc. --->
		<cftry>
			<cfset sendSoap = methodCall(argumentCollection=arguments)>
			<cfset sendSoap = startSoap & sendSoap & endSoap>

		 	<cfcatch type="any">
				<cfoutput>Method #arguments.method# against #arguments.object#  failed.</cfoutput>
				<cfdump var="#cfcatch#">

				<cfif structKeyExists(url,"debug")>
					<cfrethrow>
				</cfif>
				<cf_abort>
			</cfcatch>
		</cftry>

		<cfset sendResult = callSalesforceAPI(method=arguments.method,sendSoap=sendSoap)>

		<cfset result.errorCode=sendResult.statusCode>

		<!--- if we get an error because our session has expired, then we probably need to login and then try again
			I have removed the 'and method neq "login" from the if statement, as when a username/password has changed, the session
			needs resetting and the method is login.
		 --->
		<cfif (Find("500",sendResult.statuscode) gt 0)>		<!--- 2013-01-23 PPB Case 433103 additional trap for 500 errors --->
			<cfset sendResultResponse = xmlparse(sendResult.fileContent)>
			<cfset sendResponseArray=xmlSearch(sendResultResponse,"//soapenv:Fault")>
			<cfset responseFault = sendResponseArray[1].faultstring.XmlText>

			<cfif arguments.method neq "login">
				<!--- if we have an invalid session, recreate session and try call again --->
				<cfif reFindNoCase("INVALID_SESSION_ID|INVALID_OPERATION_WITH_EXPIRED_PASSWORD",responseFault)>
					<cfif structKeyExists(application.connector.api,"sessionID")>
						<cfset oldSessionID = application.connector.api.sessionID>
					</cfif>
					<cfset createSFDCSession()>

					<!--- replace the old Session ID with  the new Session ID --->
					<cfset sendSoap = replace(sendSoap,oldSessionID,application.connector.api.sessionID)>

					<cfset sendResult = callSalesforceAPI(method=arguments.method,sendSoap=sendSoap)>
				</cfif>
			<cfelse>
				<!--- Send an email when unable to login so that people are aware --->
				<cfmail to="#application.com.settings.getSetting('connector.Salesforce.adminEmail')#;#application.com.settings.getSetting('connector.Salesforce.apiUser.username')#;customerSupport@relayware.com" from="errors@relayware.com" subject="Unable to login with SF credentials on #request.currentSite.domainAndRoot#" type="html" priority="urgent">
					Salesforce Response Fault String when trying to login: <cfoutput>#responseFault#</cfoutput><br>
				</cfmail>
			</cfif>
		</cfif>

		<cfset result.errorCode=sendResult.statusCode>

		<!--- if dealing with errors for the partner object, then we need to set the object name as opportunityPartner as that is what it will be in the connectorObject table --->
		<cfset var sfObject = structKeyExists(arguments,"object")?arguments.object:"">
		<cfif sfObject eq "partner">
			<cfset sfObject = "opportunityPartner">
		</cfif>

		<cfset var soapResponse = sendResult.fileContent>

		<cfif not isXML(soapResponse)>
			<cfset soapResponse = "<root>"&soapResponse&"</root>">
		</cfif>

		<!--- remove namespaces --->
		<cfset soapResponse = reReplaceNoCase(soapResponse,"<soapenv:Envelope.*?Body>","")> <!--- remove opening soap:envelopetags --->
		<cfset soapResponse = reReplaceNoCase(soapResponse,"</soapenv:Body.*Envelope>","")>	<!--- remove close soap:envelopetags --->
		<cfset soapResponse = reReplaceNoCase(soapResponse,'([<\s/])([a-zA-Z0-9]*:)',"\1","ALL")> <!--- remove namespaces in nodes/attributes --->
		<cfset soapResponse = reReplaceNoCase(soapResponse,'encoding="UTF-8"',"")> <!--- remove the encoding attribute, as having this causes problem when trying to save special characters into the db (such ecoute) --->
		<cfset sendSoap = reReplaceNoCase(sendSoap,'encoding="UTF-8"',"")>

		<cftry>
			<cfset var connectorResponseID = application.getObject("connector.com.connector").logConnectorResponse(sendXML=sendSoap,responseXML=soapResponse,connectorObject=sfObject,method=arguments.method)>

			<cfcatch>
				<cfmail to="nathaniel.hogeboom@relayware.com" from="nathaniel.hogeboom@relayware.com" subject="Error logging response" type="html">
					<cfdump var="#sendResult#" label="Send Result">
					<cfdump var="#sendSoap#" label="Send XML">
					<cfdump var="#soapResponse#" label="Resposnse XML">
					<cfdump var="#cfcatch#" label="Cfcatch">
				</cfmail>
			</cfcatch>
		</cftry>

		<!--- http request succeeeded --->
		<cfif sendResult.statuscode eq "200 OK">

			<!--- remove any error messages for this object if we now have a successful call --->
			<cfset application.getObject("connector.com.connector").deleteAPIError(object=sfObject,method=arguments.method)>

			<!--- this is where we should invoke an XML parser and build a result structure in CF derived from the XML response... --->
			<cfset structAppend(result,parseSFDCResponse(argumentCollection=arguments,responseID=connectorResponseID))>

			<!--- if we've run a query and there are still more records to retrieve, run the query function again until all records have been
				retrieved. Consolidate the responses into a single query response --->
			<cfif listFindNoCase("query,queryMore,queryAll",arguments.method) and not result.isDone>
				<cfset queryMoreArgs = duplicate(arguments)>
				<cfset queryMoreArgs.method = "queryMore">
				<cfset queryMoreArgs.queryLocator = result.queryLocator>

				<!--- <cfloop	index="intGet" from="1"	to="#int(result.size/1000)#" step="1">
					<cfset threadArgs[intGet]=duplicate(queryMoreArgs)>
					<cfset threadArgs[intGet].queryLocator = listFirst(queryMoreArgs.queryLocator,"-")&"-"&intGet&listLast(queryMoreArgs.queryLocator,"-")>
				</cfloop>

				<cfloop	index="intGet" from="1"	to="#int(result.size/1000)#" step="1">
					 <cfthread action="run"	name="objGet#intGet#" args=#threadArgs[intGet]#>

						<cfset queryMoreResult = send(argumentCollection=args)>
					</cfthread>
				</cfloop>

				 <cfloop index="intGet" from="1" to="2" step="1">
					<cfthread action="join" name="objGet#intGet#"/>
				</cfloop> --->
				<cfset queryMoreResult = send(argumentCollection=queryMoreArgs)>
			</cfif>

		<!--- Service returned an application error --->
		<cfelseif Find("500",sendResult.statuscode) gt 0>

			<cfset sendResponseArray=xmlSearch(sendResult.fileContent,"//soapenv:Fault")><!--- : ensures we search in all contexts not just top level in the SOAP envelope --->
			<cfset result.message = sendResponseArray[1].faultstring.XmlText>

			<cfset errorStruct.message = result.message>
			<cfset errorStruct.statusCode = sendResult.statuscode>

			<cfset application.getObject("connector.com.connector").logAPIError(object=sfObject,method=arguments.method,statusCode=sendResult.statuscode,message=result.message)>
			<cfset result.isOk = false>

			<cftry>
				<cfthrow message="Unable to complete process due to a 500 Internal server error from Salesforce. '#errorStruct.message#'">

				<cfcatch>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,WarningStructure=errorStruct)>
				</cfcatch>
			</cftry>

			<!--- START 2013-01-23 PPB Case 433103 send debug info to devs and rethrow --->

			<!--- 2013-03-13 NJH Case 434168 throwErrorOn500Response --->
			<cfif arguments.throwErrorOn500Response>
				<cfmail to="infrastructure@relayware.com" from="errors@relayware.com" subject="SalesForce Connector 500 Error" type="html" priority="urgent">
					<cfdump var="Site=#request.currentsite.title#"></br></br>
					<Cfdump var="#arguments#" label="arguments">
					<cfdump var="#sendsoap#" label="Send Soap"><br>
					<cfdump var="#sendResult#" label="Send Result">
				</cfmail>

				<cfthrow message="Unable to complete process due to a 500 Internal server error from Salesforce. '#errorStruct.message#'">
			</cfif>

		<!--- http request failed --->
		<cfelse>
			<cfset result.message=sendResult.errorDetail>
		</cfif>
		<cfset result.arguments = arguments>

		<cfreturn result>

	</cffunction>

	<cffunction name="login" access="private" hint="Creates the SOAP body for the login method" output="false">
		<cfargument name="UserName" type="String" required="true" hint="The SalesForce username">
		<cfargument name="password" type="String" required="true" hint="The SalesForce password">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
			<username>#arguments.UserName#</username>
			<password>#arguments.password#</password>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="queryMore" access="private" hint="Creates the SOAP body for the queryMore method" output="false">
		<cfargument name="queryLocator" type="string" required="true" hint="The SalesForce queryLocator">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
			<queryLocator>#arguments.queryLocator#</queryLocator>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use - methods are
	 ******************************************************************************************** --->
	<cffunction name="query" access="private" hint="Creates the SOAP body for the query method" output="false">
		<cfargument name="queryString" type="String" required="true" hint="The SalesForce query string">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
				<queryString><![CDATA[#arguments.queryString#]]></queryString>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="queryAll" access="private" hint="Creates the SOAP body for the queryAll method" output="false">
		<cfargument name="queryString" type="String" required="true" hint="The SalesForce query string">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
				<queryString><![CDATA[#arguments.queryString#]]></queryString>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
	<cffunction name="getUpdated" access="private" hint="Creates the SOAP body for the getUpdated method" output="false">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name.">
		<cfargument name="startDate" type="String" required="true" hint="The SalesForce startDate">
		<cfargument name="endDate" type="String" required="true" hint="The SalesForce endDate">

		<cfset var soap="">
		<cfset var updatedStartDate = application.com.connectorSFDC.convertDateToSalesForceFormat(dateToConvert=arguments.startDate)>
		<cfset var updatedEndDate = application.com.connectorSFDC.convertDateToSalesForceFormat(dateToConvert=arguments.endDate)>

		<cfoutput>
				<cfsavecontent variable="soap">
					<sObjectType>#arguments.object#</sObjectType>
					<startDate>#updatedStartDate#</startDate>
					<endDate>#updatedEndDate#</endDate>
				</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
	<cffunction name="getDeleted" access="private" hint="Creates the SOAP body for the getDeleted method" output="false">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="startDate" type="String" required="true" hint="The SalesForce startDate">
		<cfargument name="endDate" type="String" required="true" hint="The SalesForce endDate">

		<cfset var soap="">
		<cfset var SFDCConnector = createObject("component","connectorSalesforceObject")>
		<cfset var deletedStartDate = SFDCConnector.convertDateToSalesForceFormat(dateToConvert=arguments.startDate)>
		<cfset var deletedEndDate = SFDCConnector.convertDateToSalesForceFormat(dateToConvert=arguments.endDate)>

		<cfoutput>
			<cfsavecontent variable="soap">
				<sObjectType>#arguments.object#</sObjectType>
				<startDate>#deletedStartDate#</startDate>
				<endDate>#deletedEndDate#</endDate>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
	<cffunction name="describeSObjects" access="private" hint="Creates the SOAP body for the describeSObjects method" output="false">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
				<sObjectType>#arguments.object#</sObjectType>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>


	<cffunction name="describeGlobal" access="private" hint="Creates the SOAP body for the describeGlobal method" output="false">
		<cfreturn "">
	</cffunction>


	<cffunction name="convertLead" access="private" hint="Creates the SOAP body for the convertLead method" output="false">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table/view that is holding the data">

		<cfset var getSoapPacket = "">
		<cfset var tableMetaData = application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=arguments.tablename)>
		<cfset var validColumns = "leadId,convertedStatus,doNotCreateOpportunity,opportunityName,overwriteLeadSource,ownerId,contactId,accountId,opportunityId,sendNotificationEmail">
		<cfset var columnCount = 1>
		<cfset var column = "">

		<cfquery name="getSoapPacket">
			declare @soapPacket xml
			set @soapPacket = (
					select
					<cfloop list="#validColumns#" index="column"><cfif listFindNoCase(tableMetaData.columnList,column)><cfif columnCount neq 1>,</cfif>#column#<cfset columnCount++></cfif></cfloop>
					from #arguments.tablename# as LeadConverts
					for XML PATH('LeadConverts')
				)

			select @soapPacket as soapPacket
		</cfquery>

		<cfreturn getSoapPacket.soapPacket>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
 	<cffunction name="retrieve" access="private" output="false" hint="Creates the SOAP body for the retrieve method">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="fieldList" type="String" default="" hint="The SalesForce list of fields to retrieve">
		<cfargument name="ids" type="array" required="true" hint="The SalesForce list of ids for which to retrieve data">

		<cfscript>
			var soap="";
			var id="";
		</cfscript>

		<!--- if no fields have been passed, then by default we get all the fields for the object that we have mapped. --->
		<cfoutput>
			<cfsavecontent variable="soap">
				<fieldList>#arguments.fieldList#</fieldList>
				<sObjectType>#arguments.object#</sObjectType>
				<cfloop array="#arguments.ids#" index="id"><ids>#id#</ids></cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="update" access="private" hint="Performs updates on objects. The objects are passed through in an array and must be of the same type" output="false">
		<cfargument name="object" type="string" required="true">

		<cfreturn buildCreateUpdateSoapPacket(method="update",argumentCollection=arguments)>

	</cffunction>

	<cffunction name="process" access="private" output="false" hint="Performs a SalesForce process request">
		<cfargument name="processRequests" type="array" required="true" hint="An array of process requests">
		<cfargument name="processType" type="string" default="ProcessSubmitRequest" hint="Can be ProcessSubmitRequest or ProcessWorkitemRequest">

		<cfscript>
			var soap="";
			var node="";
		</cfscript>

		<cfoutput>
			<cfsavecontent variable="soap">
				<cfloop array="#arguments.processRequests#" index="node">
					<actions xsi:type="urn:#arguments.processType#">
						<cfif structKeyExists(node,"objectId")><objectId>#node.objectID#</objectId></cfif>
						<cfif structKeyExists(node,"nextApproverIds")><nextApproverIds>#node.nextApproverIds#</nextApproverIds></cfif>
						<cfif structKeyExists(node,"comments")><comments>#node.comments#</comments></cfif>
						<cfif structKeyExists(node,"action")><action>#node.action#</action></cfif>
						<cfif structKeyExists(node,"workitemId")><workitemId>#node.workitemId#</workitemId></cfif>
					</actions>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<!---<cfset var getSoapPacket = "">
		<cfquery name="getSoapPacket">
			declare @soapPacket xml
			set @soapPacket = (
					select
					leadID as objectId
					from lead
					for XML PATH('actions')
				)

			select @soapPacket as soapPacket
		</cfquery>

		<cfreturn getSoapPacket.soapPacket>--->
		<cfreturn soap>

	</cffunction>

	<cffunction name="upsert" access="private" output="false" hint="Performs insert/updates on objects. The objects are passed through in an array and must be of the same type. Objects are mapped based on a custom field">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="objects" type="array" required="true" hint="An array of objects">

		<cfscript>
			var soap="";
			var node="";
			var fieldName = "";
		</cfscript>

		<!--- perhaps need to filter out any unwanted fieldnames, or perhaps fieldnames that are not updateable or createable --->
		<cfoutput>
			<cfsavecontent variable="soap">
				<!--- start per object --->
				<externalIDFieldName xmlns="urn:sobject.partner.soap.sforce.com">Relayware_ID__c</externalIDFieldName>
				<cfloop array="#arguments.objects#" index="node">
					<sObjects>
					<type xmlns="urn:sobject.partner.soap.sforce.com">#arguments.Object#</type>
					<cfloop collection="#node#" item="fieldName">
					<#fieldname#><![CDATA[#node[fieldName]#]]></#fieldname#></cfloop>
					</sObjects>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="create" access="private" output="false" hint="Creates the SOAP body for the create method">
	 	<cfargument name="object" type="String" required="true" hint="The SalesForce object name">

		<cfreturn buildCreateUpdateSoapPacket(method="create",argumentCollection=arguments)>
	</cffunction>

	<!--- NJH 2013/08/14 Case 435632 - factorised function for creating soap packets for the create/update methods. --->
	<cffunction name="buildCreateUpdateSoapPacket" access="private" hint="Builds the SOAP packet for the create/update methods">
		<cfargument name="method" type="string" default="create">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="viewName" type="String" required="true" hint="The name of the transformation view">
		<cfargument name="whereClause" type="string" default="">
		<cfargument name="columnList" type="string" default="">

		<cfset var getObjectValuesToNull = "">

		<!--- working out which columns to null if they have come through as an empty string or as a null...SF expects another field to be set called fieldsToNull to have the list of fields that need nulling for a given record --->
		<!--- <cfquery name="getObjectValuesToNull">
			select column_remote from vConnectorMapping
			where object_remote =  <cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and emptyStringAsNull = 1
		</cfquery> --->

		<cfset var fieldsToNullString = "">
		<cfset var columnName = "">
		<cfset var fieldsToNull = application.getObject("connector.com.connector").getFieldsToNull(object=arguments.object)>

		<cfloop list="#arguments.columnList#" index="columnName">
			<cfif listFindNoCase(fieldsToNull,columnName)>
				<cfset fieldsToNullString = fieldsToNullString & IIF(fieldsToNullString eq"",DE(''),DE(' union '))&"select case when isNull(cast (#columnName# as nvarchar(max)),'') = '' then '#columnName#' else null end as fieldsToNull">
			</cfif>
		</cfloop>

		<cfset var getSoapPacket = "">
		<cfquery name="getSoapPacket">
			declare @soapPacket xml
			set @soapPacket = (
					select 'sobject.partner.soap.sforce.com' as "type/@xml:urn",'#arguments.object#' as "type",#arguments.columnList#
					<cfif fieldsToNullString neq "">
					, (select f.fieldsToNull from (#preserveSingleQuotes(fieldsToNullString)#) as f for XML PATH(''),type)
					</cfif>
					from #arguments.viewName# as sObject
					where 1=1
						<cfif arguments.whereClause neq "">and #preserveSingleQuotes(arguments.whereClause)#</cfif>
					order by rowNumber
					for XML PATH('sObject')
				)

			select @soapPacket as soapPacket
		</cfquery>

		<cfreturn getSoapPacket.soapPacket>
	</cffunction>


	<cffunction name="delete" access="private" output="false" hint="Creates the SOAP body for the delete method">
	 	<cfargument name="ids" type="array" required="true" hint="The SalesForce list of ids which to delete">

		<cfscript>
			var soap="";
			var deletedObjId="";
		</cfscript>

		<cfoutput>
			<cfsavecontent variable="soap">
				<cfloop array="#arguments.ids#" index="deletedObjId">
					<ids>#deletedObjId#</ids>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>
	</cffunction>


	<cffunction name="getServerTimestamp" access="private" output="false" hint="Creates the soap packet for the getServerTimestamp method">
		<cfreturn "">
	</cffunction>


 	<cffunction name="parseSFDCResponse" access="private" hint="Parses the response of a SOAP call to SFDC" output="false">
		<cfargument name="responseID" type="numeric" required="true" hint="The response XML as received from SalesForce">
		<cfargument name="method" type="string" required="true" hint="The SalesForce method that was called">
		<cfargument name="object" type="string" required="false" hint="The SalesForce object that was called">
		<cfargument name="createTempTableForResponse" type="boolean" default="true" hint="Put response in temp table rather than core table. Only data for import should go in core table.">
		<cfargument name="truncateTable" type="boolean" default="true" hint="Truncate the table before putting new data in. ALmost everytime this is true, but there is one case for oppContactRoles and oppPartners where we need to keep existing data.">

		<cfset var result = {isOK = true}>
		<cfset var errorResult ={recordCount=0,tablename="",columnList=""}>
		<cfset var successResult ={recordCount=0,tablename="",columnList=""}>
		<cfset var result = {isDone=true,queryLocator="",size=0,errorResult=errorResult,successResult=successResult,responseID=arguments.responseID}>
		<cfset var thisResult = successResult>
		<cfset var searchItem = "*search*">  <!--- the item to look for in the incoming xml --->
		<cfset var isChild=false>


		<!--- <cfset var useTempTable = false> --->
		<cfparam name="request.tempTableSuffix" default=""> <!--- set in connector.cfc - used as a handle on all temp tables so that they can be deleted after a run --->

		<cfset var tablename= listFindNoCase("create,update,upsert",arguments.method)?"connector_SaveResult":"####Salesforce#arguments.method##structKeyExists(arguments,'object')?arguments.object:''##request.tempTableSuffix#_#arguments.responseID#">
		<cfset var getXMLNodesForTableColumns = queryNew("tagName")>
		<!--- <cfset var useObjectDataTable = "false"> ---> <!--- are we using the object data table?? --->


		<cfif listFindNoCase("query,queryAll,queryMore",arguments.method)>
			<cfif not arguments.createTempTableForResponse>
				<cfset tablename= "connector_Salesforce_#arguments.object#"> <!--- TODO: make this scope available so that we can use this variable.. this.objectDataTable> --->
				<!--- <cfset useObjectDataTable = true> --->
			</cfif>
		</cfif>


			<cfset var functionArgs = {connectorResponseID=arguments.responseID,tablename=tablename,truncateTable=arguments.truncateTable}>
			<cfif structKeyExists(arguments,"object")>
				<cfset functionArgs.object = arguments.object>
			</cfif>
			<cfset thisResult = application.getObject("connector.salesforce.com.connectorSalesforce").getDataFromConnectorResponse(argumentCollection=functionArgs)>

			<cfset result.successResult = duplicate(thisResult)>

			<!--- if we have successfully gotten records, then find out if there are more to get --->
			<cfif listFindNoCase("query,queryMore,queryAll",method)>

				<cfset var getQueryMoreID = "">

				<cfquery name="getQueryMoreID">
					select distinct
						T.Col.value('./done[1]','bit') as [done],
						T.Col.value('./queryLocator[1]','varchar(50)') as [queryLocator],
						T.Col.value('./size[1]','int') as [size]
					from
						connectorResponse
					cross APPLY
						responseXML.nodes('//#method#Response/result') T(Col)
					where ID=<cf_queryparam value="#arguments.responseID#" cfsqltype="cf_sql_integer">
				</cfquery>

				<cfif getQueryMoreID.recordCount>
					<cfset result.isDone = getQueryMoreID.done[1]>
					<cfset result.queryLocator = getQueryMoreID.queryLocator[1]>
					<cfset result.size = getQueryMoreID.size[1]>
				</cfif>
			</cfif>

		<!--- add the object and objectID columns to the response table, as these are 'standard' columns that we are expecting --->
		<cfif listFindNoCase(thisResult.columnList,"ID") and left(tablename,1) eq "##">
			<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=result.successResult.tablename,columnName="objectID",defaultValueFromColumn="ID")>
			<cfif not listFindNoCase(thisResult.columnList,"object")>
				<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=result.successResult.tablename,columnName="object",defaultValue=arguments.object)>
			</cfif>
		</cfif>

		<cfset var booleanColumnName = "">
		<cfset var updateBooleanColumn = "">

		<cfloop list="success,isActive,isDeleted" index="booleanColumnName">
			<cfif listFindNoCase(thisResult.columnList,booleanColumnName)>
				<cfquery name="updateBooleanColumn">
					update #tablename# set #booleanColumnName# = case when #booleanColumnName# in ('true','1') then 1 else 0 end
				</cfquery>
			</cfif>
		</cfloop>

		<cfif arguments.method eq "getServerTimestamp">
			<cfquery name="getTimeStamp">
				select timestamp from #tablename#
			</cfquery>
			<cfset result.serverTimeStamp = getTimeStamp.timestamp[1]>
		</cfif>

		<cfset result.result = duplicate(thisResult)>
		<cfset result.isOK = true>
		<cfreturn result>

	</cffunction>


	<!---<cffunction name="logAPIError" access="private" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="message" type="string" required="true">
		<cfargument name="method" type="string" required="true">
		<cfargument name="statusCode" type="string" required="true">

		<cfif trim(arguments.object) neq "">
			<cfset deleteAPIError(object=arguments.object,method=arguments.method)>
			<cfset var insertAPIQueryIntoConnectorError = "">

			<!--- we set the synchAttempt to be the maximum so that this record doesn't get picked up for a synch as it's not really a record--->
			<cfquery name="insertAPIQueryIntoConnectorError">
				declare @queueID int
				declare @connectorObjectID int

				select @connectorObjectID = ID from connectorObject where object =  <cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" > and relayware=0 and connectorType='Salesforce'

				insert into connectorQueue (connectorObjectID,objectID,modifiedDateUTC,synchAttempt,errored) values (@connectorObjectID,'API',getDate(),#application.com.settings.getSetting("connector.maxSynchAttempts")#,1)
				select @queueID = scope_identity()

				insert into connectorQueueError (connectorQueueID,message,field,statusCode) values (@queueID,'#arguments.message#','#arguments.method#','#arguments.statusCode#')
			</cfquery>
		</cfif>

	</cffunction>


	<cffunction name="deleteAPIError" access="private" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="method" type="string" required="true">

		<cfset var deleteAPIErrorInQueue = "">

		<cfquery name="deleteAPIErrorInQueue">
			delete connectorQueue
			from
				connectorQueue q
					inner join connectorObject o on o.ID = q.connectorObjectID
					inner join connectorQueueError e on e.connectorQueueID = q.Id
			where o.object =  <cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and q.objectID='API'
				and e.field =  <cf_queryparam value="#arguments.method#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and o.connectorType='Salesforce'
		</cfquery>

	</cffunction>--->


	<cffunction name="matchObjectRecords" access="public" returnType="struct" hint="A wrapper function to send off the query to match records about to be exported">
		<cfargument name="object" type="string" required="true">
		<cfargument name="whereClause" type="string" required="true">
		<cfargument name="objectIDColumn" type="string" required="true">

		<cfset var matchingWhereClause = "(" & arguments.whereClause & ")">

		<cfreturn send(method="query",queryString="select #arguments.objectIDColumn# from #arguments.object# where #matchingWhereClause# order by lastmodifiedDate desc",object=arguments.object)>
	</cffunction>


	<cffunction name="testConnectionToRemoteSite" access="public" output="false" returnType="boolean" hint="Runs a test query against the remote site to ensure that we can connect to it">

		<cfset var connectionOk = true>
		<cfset var serverOnlineResponse = {isOK=false}>
		<cftry>
			<cfset serverOnlineResponse = send(method="query",object="user",queryString="select ID from user where username='#application.com.settings.getSetting('connector.salesforce.apiUser.username')#'")>

			<!--- salesforce server is down - I think that this is the code that indicates this... may need some tweaking --->
			<!---<cfif serverOnlineResponse.errorCode eq "Connection Failure. Status code unavailable.">--->
			<cfif not serverOnlineResponse.isOK>
				<cfset connectionOk = false>
			</cfif>

			<cfcatch>
				<cfset connectionOk = false>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,warningStructure=serverOnlineResponse)>
			</cfcatch>
		</cftry>

		<cfif not connectionOk>
			<cfmail to="nathaniel.hogeboom@relayware.com" from="nathaniel.hogeboom@relayware.com" subject="SalesForce Test Connection Failed on #request.currentSite.domainAndRoot#" type="html">
				<cfdump var="#serverOnlineResponse#">
			</cfmail>
		</cfif>

		<cfreturn connectionOk>
	</cffunction>


	<cffunction name="areLoginCredentialsValid" access="public" output="false" hint="Function called when settings change to test whether the new credentials entered are valid" returnType="boolean">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">

		<cfset var credentialsValid = true>
		<cftry>
			<cfset createSFDCSession(username=arguments.username,password=arguments.password)>
			<cfcatch>
				<cfset credentialsValid = false>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,warningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn credentialsValid>
	</cffunction>
</cfcomponent>