<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorSalesForce.cfc
Author:			NJH
Date started:	02-12-2014

Description:	A component designed for the salesforce connector that override the core connector

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-11-25	WAB getDataFromConnectorResponse() changed to use an intermediate temporary table to improve performance
2015-11-25	WAB getDataFromConnectorResponse() modified so that an extra bit of xpath can be passed in to return an individual record
				used in getDataForIDFromConnectorResponse()
				Still remarkably slow - probably quicker to do it by using CF xmlsearch (suprisingly)
2015//12/03	NJH	BF-44 - deal with the 'type' field... should select into the object column for any method that is not describeSObjects

Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="getRemoteObjectFields" access="public" output="false" returnType="query" hint="Returns a query of Salesforce object fields used to populate the connector mapping table">
		<cfargument name="object" type="string" required="true">

		<cfset var qryFields = queryNew("name,hasPickListValues,datatype,type,nillable","varchar,bit,varchar,varchar,bit")>

		<cfif arguments.object neq "">
			<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(connectorType="salesforce",object=arguments.object)>

			<cfif structKeyExists(application.connector.objectMetaData,arguments.object)>
				<cfset qryFields = application.com.structureFunctions.structToQuery(struct=application.connector.objectMetaData[arguments.object].fields,defaultColumnsAndDataTypes="name:varchar,hasPickListValues:bit,datatype:varchar,type:varchar,nillable:bit")>
			</cfif>
		</cfif>

		<cfreturn qryFields>

	</cffunction>



	<cffunction name="convertObjectFieldMetaDataToRwQuery" access="public" output="false" hint="Returns the meta data as given by remote system into a RW format" returnType="query">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="columnList" type="string" required="true">

		<!--- just getting these fields for the moment. The dataType field is a field that we use to map SF field datatypes to RW field datatypes, so we are currently looping some datatypes together under text or numeric
			NJH 2016/03/06 - JIRA PROD2016-472 - synching files and attachments. If type is base64, set length to 4001 so that column in base table gets set as nvarchar(max)
		--->
		<cfset var getObjectDetails = "">

		<cfquery name="getObjectDetails">
			select createable,updateable,name,type,nillable,
				case when type = 'base64' then 4001 else length end as length,
				<!--- apparently not all object have picklist values, so double check that the column exists before using it. (AcceptedEventRelation object is an example)--->
				<cfif listFindNoCase(arguments.columnList,"picklistValues")>
				case when len(isNull(cast(pickListValues as nvarchar(max)),'')) != 0 then '<pickList>'+cast(pickListValues as nvarchar(max))+'</pickList>' else null end as pickListValues,
				case when len(isNull(cast(pickListValues as nvarchar(max)),'')) != 0 then 1 else 0 end as hasPickListValues,
				<cfelse>
				null as pickListValues, 0 as hasPickListValues,
				</cfif>
				case when type in ('phone','email','id','string') then 'text' when type like 'date%' then 'date' when type in ('int','double','currency') then 'numeric' else type end as dataType,
				case when type = 'id' then 1 else 0 end as isID
			from #arguments.tablename#
		</cfquery>

		<cfreturn getObjectDetails>
	</cffunction>


	<cffunction name="convertObjectDataToRwQuery" access="public" output="false" hint="Returns the object data as given by remote system into a RW format" returnType="query">
		<cfargument name="tablename" type="string" required="true">

		<cfset var qryRemoteObjects = "">
		<cfquery name="qryRemoteObjects">
			select label, name from #arguments.tablename#
			where
				queryAble='true' and deprecatedAndHidden='false'
			order by label desc
		</cfquery>

		<cfreturn qryRemoteObjects>
	</cffunction>


	<cffunction name="getDataForIDFromConnectorResponse" output="false" access="public" hint="Gets the data from the connectorResponse table showing what the state of the data was at the time of sending" returnType="query">
		<cfargument name="connectorResponseID" type="numeric" required="true">
		<cfargument name="relaywareID" type="numeric" required="false">
		<cfargument name="remoteID" type="string" required="false">

		<cfset var tablename = "####temp#replace(CreateUUID(),'-','','ALL')#">
		<cfset var getData = "">

		<!--- we have a salesforceID coming through.. therefore we know that it is going to be a create/update into RW. That means that the SF data will be in the response --->
		<cfif structKeyExists(arguments,"remoteID") and arguments.remoteID neq "">
			<!--- WAB 2015-11-25 Pass a piece of xPath into getDataFromConnectorResponse() so that just brings back a single record --->
			<cfset var connectorResponseResult = getDataFromConnectorResponse(	argumentCollection=arguments,
																				tablename=tablename,
																				xPath="[Id[text()='#arguments.remoteID#']]")>

			<!--- get all the data for the give connector response.. then filter by the ID passed through
				TODO: filter data in above function rather than here --->
			<cfif connectorResponseResult.tablename neq "">
				<cfquery name="getData">
					select * from #connectorResponseResult.tablename#
					where ID=<cf_queryparam value="#arguments.remoteID#" cfsqltype="cf_sql_varchar">

					drop table #connectorResponseResult.tablename#
				</cfquery>
			</cfif>
		<cfelse>

			<!--- we don't have a salesforce Id but rather a RW ID. That means that the data is in the send xml
					WAB 2016-01-08 fixed a typo here
			--->
			<cfset var responseMetaData = application.getObject("connector.com.connector").getConnectorResponseMetaData( connectorResponseID = arguments.connectorResponseID)>
			<cfset var relaywareObject = application.getObject("connector.com.connector").getMappedObject(object_remote=responseMetaData.object,connectorType="salesforce")>
			<cfset var nonPrimaryObject = false>

			<cfif relaywareObject eq "">
				<cfquery name="getMappedObjectForNonPrimaryObject">
					select top 1 object_relayware from vConnectorMapping where Object_remote=<cf_queryparam value="#responseMetaData.object#" cfsqltype="cf_sql_varchar">
				</cfquery>
				<cfset relaywareObject=getMappedObjectForNonPrimaryObject.object_relayware[1]>
				<cfset nonPrimaryObject=true>
			</cfif>

			<cfset var relaywareIDField = application.getObject("connector.com.connector").getMappedField(connectorType="salesforce",object_relayware=relaywareObject,column_relayware=application.com.relayEntity.getEntityType(entityTypeID=relaywareObject).uniqueKey).field>
			<cfset var getColumnsFromSendXML = "">

			<cfquery name="getColumnsFromSendXML">
				;with xmlnamespaces (default 'urn:partner.soap.sforce.com')
				select distinct
					T.rows.value('local-name(.)','nvarchar(100)') COLLATE Latin1_General_CS_AS as [TagName]
				from connectorResponse
				cross APPLY
					sendXML.nodes('//#responseMetaData.method#/sObject/*') T(rows)
				where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfquery name="getData">
				;with xmlnamespaces (DEFAULT 'urn:partner.soap.sforce.com')
				select
					<cfloop query="getColumnsFromSendXML">
						x.Col.value('./#tagName#[1]','NVARCHAR(max)') as #tagName# <cfif currentRow neq getColumnsFromSendXML.recordCount>,</cfif>
					</cfloop>
				from connectorResponse r
					CROSS APPLY sendXML.nodes('//#responseMetaData.method#/sObject') x(Col)
				where r.id = <cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
				<!--- if a non primary object, then we don't have a RW ID --->
				<cfif nonPrimaryObject>
				and x.Col.value('./#relaywareIDField#[1]','INT') = <cf_queryparam value="#arguments.relaywareID#" cfsqltype="cf_sql_integer">
				</cfif>
			</cfquery>
		</cfif>

		<cfreturn getData>
	</cffunction>


	<!---
	WAB 2015-11-25  Modifications to improve performance
					Use a temporary table for the shredded XML, even if eventually going into real table
					Get rid of clustered indexes (could replace with just a regular unique index on ID)
					Added xPath parameter so that
	--->
	<cffunction name="getDataFromConnectorResponse" output="false" access="public" hint="Gets the data from the connectorResponse table showing what the state of the data was at the time of sending" returnType="struct">
		<cfargument name="connectorResponseID" type="numeric" required="true">
		<cfargument name="tablename" type="string" default="##temp#replace(CreateUUID(),'-','','ALL')#">
		<cfargument name="object" type="string" required="false">
		<cfargument name="truncateTable" type="boolean" default="false">
		<cfargument name="xPath" type="string" default="">

		<cfset var tableColumnsQry = application.getObject("connector.salesforce.com.connectorSalesforce").getTableColumnsFromXML(connectorResponseID=arguments.connectorResponseID)>
		<cfset var selectColList = "">
		<cfset var selectArray = []>
		<cfset var responseMetaData = application.getObject("connector.com.connector").getConnectorResponseMetaData(connectorResponseID=arguments.connectorResponseID)>
		<cfset var isTemporaryTable = left(arguments.tablename,1) eq "##"?true:false>
		<cfset var putXMLResponseIntoTable = "">
		<cfset var result = {recordCount=0,tablename="",columnList=""}>
		<cfset var queryResult = {}>
		<cfset var method = responseMetaData.method>
		<cfset var connectorObject = not structKeyExists(arguments,"object")?responseMetaData.object:arguments.object>
		<cfset var isChild=method eq "getUpdated"?true:false>
		<cfset var xmlSearchString = application.getObject("connector.salesforce.com.connectorSalesforce").getXmlSearchStringForMethod(method=method)>

		<cfif xPath is not "">
			<!--- add arguments.xpath to the standard search.Escaping single quotes --->
			<cfset xmlSearchString = xmlSearchString & replace (xPath,"'","''","ALL")>
		</cfif>

		<cfif method neq "queryMore" and not isTemporaryTable and arguments.truncateTable>
			<!--- using truncate rather than delete as we want to reset the rowNumber identity on the SFDCSaveResult table, as we need this number to match results back to the table were data came from --->
			<cfset var deleteExistingData = "">
			<cfquery name="deleteExistingData" result="queryResult">
				if object_id('#tablename#') is not null
				begin
				   truncate table #tablename#
				end
			</cfquery>
		</cfif>

		<!--- These methods have standard columns returned with several layers....so treated slightly differently at the moment until I work out how to deal with the extra layer --->
		<cfset var upsertMethod = listFindNoCase("create,update,upsert",method)?true:false>

		<cfif tableColumnsQry.recordCount or upsertMethod>


			<cfif isTemporaryTable>
				<cfset var intermediateTempTableName = arguments.tablename>
			<cfelse>
				<cfset var intermediateTempTableName = "##XMLResponseTempTable">
			</cfif>

			<!--- 	WAB 2015-11-25  Modifications to improve performance (and to make it easier to read the debug)--->
			<cfquery name="putXMLResponseIntoTable" result="queryResult">
				if object_id('tempdb..#intermediateTempTableName#') is not null
					begin
				   drop table #intermediateTempTableName#
					end

				SELECT

				<cfif not upsertMethod>
					<cfsilent>
					<cfparam name="request.remoteServerTimeOffsetInSeconds" default="0">

					<cfloop query="#tableColumnsQry#">
					<cfset var selectPrefix = "">
					<cfset var selectSuffix = "">
					<!--- if we are getting the lastModified date, then strip out the T and Z's... Also add the offset so that we're comparing apples with apples --->
					<cfif listFindNoCase("deletedDate,lastModifiedDate",tagName)>
						<cfset selectPrefix = "dateAdd(s,-(#request.remoteServerTimeOffsetInSeconds#),replace(replace(">
						<cfset selectSuffix = ",'T',' '),'Z',''))">
					</cfif>
						<!--- this is a bit hard-coded for now, but at the moment, it's the only time when we need this. Essentially, pickListValue holds multiple nodes, and so here we return an xml with all the different values --->
						<cfset alias = tagName>
						<!--- BF-44 the type column is the name of the object in all methods apart from describeSObjects.. so, put the value in the object column --->
						<cfif not compare(tagName,"type") and method neq "describeSObjects">
							<cfset alias = "Object">
						</cfif>
						<cfset selectColList = listAppend(selectColList,alias)>

						<cfif method eq "describeSObjects" and tagName eq "pickListValues">
							<cfset arrayAppend(selectArray,"T.Col.query('#tagName#') as [#alias#]")>
						<cfelse>
							<cfset arrayAppend(selectArray,"#selectPrefix#T.Col.value('#ischild?".":""#./#tagNameSelect#[1]','NVARCHAR(#length#)')#preserveSingleQuotes(selectSuffix)# as [#alias#]")>
						</cfif>

					</cfloop>
					</cfsilent>

					<cfset selectStatement = arrayToList (selectArray, "," & chr(10) )>
					#preserveSingleQuotes(selectStatement)#

				<cfelse>
					<!--- the create,update, and upsert methods all return the following structure... trying to capture errors as well, which is another layer --->
					T.Col.value('./id[1]','VARCHAR(20)') as [ID],
					T.Col.value('./success[1]','BIT') as [success],
					T.Col.value('./errors[1]/message[1]','VARCHAR(250)') as [message],
					T.Col.value('./errors[1]/fields[1]','VARCHAR(50)') as [fields],
					T.Col.value('./errors[1]/statusCode[1]','VARCHAR(50)') as [statusCode]
					<cfset selectColList = "ID,success,message,fields,statuscode">
				</cfif>

				,#arguments.connectorResponseID# as connectorResponseID

				into
					#intermediateTempTableName#
				FROM
				   connectorResponse r
				CROSS APPLY
					responseXML.nodes('#xmlSearchString#') T(Col)
				where
					r.ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">

				<cfif not isTemporaryTable>
					<cfset selectColList = listAppend (selectColList,"connectorResponseID")>

					insert into #arguments.tablename# (
						#selectColList#
					)
					select SfObjectData.#replace(selectColList,",",",SfObjectData.","ALL")#

					from
						#intermediateTempTableName# 	as SfObjectData
								left join
						#arguments.tablename# odt on odt.ID = SfObjectData.ID
					where
						odt.ID is null

				   drop table #intermediateTempTableName#

				</cfif>

			</cfquery>

			<cfset queryResult.tablename = arguments.tablename>
			<cfset queryResult.columnList = selectColList>

		</cfif>

		<cfset structAppend(result,queryResult)>

		<cfreturn result>
	</cffunction>


	<!--- taken from connectorSFDCAPI.cfc --->
	<cffunction name="getTableColumnsFromXML" output="false" access="public" hint="Returns table columns from xml" returnType="query">
		<cfargument name="connectorResponseID" type="numeric" required="true">
		<cfargument name="object" type="string" required="false">

		<cfset var getXMLNodesForTableColumns = queryNew("tagName,TagNameSelect,length")>
		<cfset var connectorObject = "">
		<cfset var useMetaDataLength = false>
		<cfset var asterix = "*">
		<cfset var responseMetaData = application.getObject("connector.com.connector").getConnectorResponse(connectorResponseID=arguments.connectorResponseID)>
		<cfset var connectorObject = not structKeyExists(arguments,"object")?responseMetaData.object[1]:arguments.object>
		<cfset var objectMetaDataTablename = "##objectMetaData_#connectorObject#">
		<cfset var method = responseMetaData.method[1]>
		<cfset var isChild=method eq "getUpdated"?true:false>
		<cfset var xmlSearchString = application.getObject("connector.salesforce.com.connectorSalesforce").getXmlSearchStringForMethod(method=method)>

		<cfif connectorObject neq "" and listFindNoCase("query,queryAll,queryMore,retrieve",method)>
			<cfset var objectMetaData = application.getObject("connector.com.connector").getRemoteObjectFields(connectorType="Salesforce",object=connectorObject)>
			<cf_qoqToDb query=#objectMetaData# name="#objectMetaDataTablename#">
			<cfset useMetaDataLength = true>
		</cfif>

		<cfif listFindNoCase("query,queryMore,queryAll",method)>
			<!---
				this is used to find results for any related (child) objects. for example, when querying the opportunity object, we can get data about opportunityPartners and contactRoles.
				This will come through as a node of type 'QueryResult'
			--->

			<cfset var getChildQueryColumns = "">

			<cfquery name="getChildQueryColumns">
				-- get nodes/rows from the xml.. use this to derive columns...
				select distinct
					T.rows.value('local-name(.)','nvarchar(100)') as [TagName]
					--,T.rows.query('.') as xmlfragment
				from
					connectorResponse
				cross APPLY
					responseXML.nodes('#xmlSearchString#/#asterix#[contains(@type,"QueryResult")]') T(rows)
				where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
			</cfquery>
		</cfif>

		<!--- if xmlSearchString is an empty string, then we have a standard response of 'ID,success,errors' --->
		<cfquery name="getXMLNodesForTableColumns">
			-- get nodes/rows from the xml.. use this to derive columns...
			select tagName,tagNameSelect,
			<cfif useMetaDataLength>case when omd.length > 4000 then 'max' when omd.length > 255 then cast(omd.length as varchar) else '255' end<cfelse>'255'</cfif>
			as length from (
			select distinct
				T.rows.value('local-name(.)','nvarchar(100)') COLLATE Latin1_General_CS_AS as [TagName]
				,T.rows.value('local-name(.)','nvarchar(100)') COLLATE Latin1_General_CS_AS as [TagNameSelect]
				--,T.rows.query('.') as xmlfragment
			from connectorResponse
			cross APPLY
				<!--- responseXML.nodes('#xmlSearchString#<cfif not isChild>/#asterix#<cfif searchItem neq "queryResult">[not(@*) or @nil="true"]<cfelse>[contains(@type,"QueryResult")]</cfif></cfif>') T(rows) --->
				responseXML.nodes('#xmlSearchString#<cfif not isChild>/#asterix#[not(@*) or @nil="true"]</cfif>') T(rows)
			where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
			) as base
				<cfif useMetaDataLength>left join #objectMetaDataTablename# omd on omd.name = base.tagName</cfif>
			union
			select distinct
				E.rows.value('local-name(.)','nvarchar(100)') COLLATE Latin1_General_CS_AS as [TagName]
				,'errors[1]/'+E.rows.value('local-name(.)','nvarchar(100)') COLLATE Latin1_General_CS_AS as [TagNameSelect],'255' as length
			from connectorResponse
			cross APPLY
				<!--- responseXML.nodes('#xmlSearchString#/errors<cfif not isChild>/#asterix#<cfif searchItem neq "queryResult">[not(@*) or @nil="true"]<cfelse>[contains(@type,"QueryResult")]</cfif></cfif>') E(rows) --->
				responseXML.nodes('#xmlSearchString#/errors<cfif not isChild>/#asterix#[not(@*) or @nil="true"]</cfif>') E(rows)
			where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn getXMLNodesForTableColumns>
	</cffunction>


	<cffunction name="getXmlSearchStringForMethod" access="public" output="false" hint="Returns the xml path to search/retrieve data response for a given method" returnType="string">
		<cfargument name="method" type="string" required="true">

		<cfset var searchItem = "">
		<cfset var xmlPathSearch = "//#arguments.method#Response/result">

		<cfswitch expression="#arguments.method#">
			<cfcase value="getUpdated">
				<cfset searchItem = "ids">
			</cfcase>
			<cfcase value="getDeleted">
				<cfset searchItem = "deletedRecords">
			</cfcase>
			<cfcase value="describeSObjects">
				<cfset searchItem = "fields">
			</cfcase>
			<cfcase value="describeGlobal">
				<cfset searchItem = "sobjects">
			</cfcase>
			<cfcase value="query,queryMore,queryAll">
				<cfset searchItem = "records">
			</cfcase>
			<!--- <cfcase value="create,update,upsert">
				<cfset searchItem = "id">
			</cfcase> --->
			<cfdefaultcase>
				<cfset searchItem = "">
			</cfdefaultcase>
		</cfswitch>

		<cfif searchItem neq "">
			<cfset xmlPathSearch = xmlPathSearch & "/#searchItem#">
		</cfif>

		<cfreturn xmlPathSearch>
	</cffunction>


	<cffunction name="getUserMatchingData" access="public" hint="Returns the column names of the user object that we will need for matching to exsiting Relayware Users. This includes both location and person fields." returnType="struct">
		<cfreturn {fieldStruct={id="ID",company="companyName",address1="street",phone="phone",address4="city",postalCode="postalCode",country="country",firstname="firstname",lastname="lastname",email="email"},userObjectName="user"}>
	</cffunction>
</cfcomponent>