<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorSalesforceObject.cfc
Author:			NJH
Date started:	23-01-2015

Description:	The Salesforce Connector

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/01/23			NJH			Fixed bug where opportunityContactRoles/partners were not getting deleted; Also delete deleted orphaned from the base data table so that they're not stuck in the queue.
2015/04/13			NJH			Several small changes. Filter out orgs from the import view that don't have a country set so that we only process orgs we know we can import. Fix problem in matching code if the RW ID was not the same for every object.
								Also added organisationType='AdminCompany' to sql join when matching user organisations.
2015/06/10			NJH			Changed dependency column delimiter from underscore to bent pipe
2015/07/02			NJH			Broke up the matchRecordsForExport function so that we can specify which columns we want to use for matching. Also created function that returned the org fields to synch based on the synching location fields if using account-location mapping. Needed
								these fields in various places.
2015/07/13			NJH			Set dataExists column to 0 where data no longer exists on SF. This is used for reporting and prevents records from sitting forever in the queue.
								Also changed the underlying view from vConnectorSynchingRecords to vConnectorRecords
2015/12/14			DAN			Case 446344 - add support for IsPrimary field on the opportunityContactRole object
2015/11/09			NJH			Moved some functionality out of here into generic code, as I realised that a lot of things were not actually salesforce specific. There is definately more to come.
								Some functions, like testRemoteConnection and areLoginCredentialsValid moved to the API.cfc
2016/01/08			NJH/DAN		Fixed up issues with the isPrimary field done as part of case 446344
2016/07/29          NJH/DAN     Case 451138 - populate where clause for the non-primary objects, and fix to take into account when getting remote column will return empty string

Possible enhancements:


 --->


<cfcomponent extends="relay.connector.connectorObject" output="false">

	<cfset this.connectorType="Salesforce">

	<cffunction name="SalesforceInit" access="private" output="false" hint="Initialises things specific to the salesForce connector">

		<cfset this.objectPrimaryKey = "ID"> <!--- TODO: this may need to come from the mappings table so that it is not stuck in the code, particularly if we are mapping a new object --->
		<cfset request.tempTableSuffix = this.tempTableSuffix> <!--- setting this in request scope so that it is available in the API cfc --->

		<!--- TODO: put this in application scope?? in application.SFDCAPI --->
		<!--- <cfif not structKeyExists(request,"remoteServerTimeOffsetInSeconds")>
			<cfset request.remoteServerTimestamp = replaceNoCase(replaceNoCase(this.api.send(method="getServerTimestamp",object="user").serverTimestamp,"T"," "),"Z","")>
		 --->
		<cfif not structKeyExists(application.connector,"api") or not structKeyExists(application.connector.api,"remoteServerTimestamp")>
			<cfset application.connector.api.remoteServerTimestamp = replaceNoCase(replaceNoCase(this.api.send(method="getServerTimestamp",object="user").serverTimestamp,"T"," "),"Z","")>
		</cfif>
		<cfset request.remoteServerTimestamp = application.connector.api.remoteServerTimestamp>
	</cffunction>


	<cffunction name="getTransformationViewWhereClause" access="private" output="false" returnType="string">

		<cfset var whereClause = "">
		<!--- if we are synching (exporting) leads, then we don't want to export opportunties that have been created as a result of a lead conversion and that don't yet have a SF ID.
			Essentially, we want to convert the lead on SF, and then set the converted RW opp to have the resulting SF OppId as its crmOppID
		 --->
		<cfif this.baseEntity eq "opportunity" and this.direction eq "export" and getConnectorObject(object_relayware="lead").synch>
			<cfset whereClause = "and not (opportunity.convertedFromLeadID is not null and opportunity.crmOppId is null)">
		</cfif>

		<cfreturn whereClause>
	</cffunction>


	<!--- <cffunction name="areLoginCredentialsValid" access="public" output="false" hint="Function called when settings change to test whether the new credentials entered are valid" returnType="boolean">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">

		<cfset var credentialsValid = true>
		<cftry>
			<cfset this.api.createSFDCSession(username=arguments.username,password=arguments.password)>
			<cfcatch>
				<cfset credentialsValid = false>
			</cfcatch>
		</cftry>

		<cfreturn credentialsValid>
	</cffunction>


	<cffunction name="testConnectionToRemoteSite" access="public" output="false" returnType="boolean" hint="Runs a test query against the remote site to ensure that we can connect to it">

		<cfset var connectionOk = true>
		<cfset var serverOnlineResponse = {isOK=false}>
		<cftry>
			<cfset serverOnlineResponse = this.api.send(method="query",object="user",queryString="select ID from user where username='#application.com.settings.getSetting('connector.salesforce.apiUser.username')#'")>

			<!--- salesforce server is down - I think that this is the code that indicates this... may need some tweaking --->
			<!---<cfif serverOnlineResponse.errorCode eq "Connection Failure. Status code unavailable.">--->
			<cfif not serverOnlineResponse.isOK>
				<cfset connectionOk = false>
			</cfif>

			<cfcatch>
				<cfset connectionOk = false>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,warningStructure=serverOnlineResponse)>
			</cfcatch>
		</cftry>

		<cfif not connectionOk>
			<cfmail to="nathaniel.hogeboom@relayware.com" from="nathaniel.hogeboom@relayware.com" subject="SalesForce Test Connection Failed on #request.currentSite.domainAndRoot#" type="html">
				<cfdump var="#serverOnlineResponse#">
			</cfmail>
		</cfif>

		<cfreturn connectionOk>
	</cffunction> --->


 	<cffunction name="getJoinToRelaywareTableForPricebook" access="private" output="false" hint="Provides the join needed to import pricebooks, as it's not a straightforard mapping, due to the pricebook crmID appearing multiple times, once for each currency" returnType="struct">
		<cfargument name="importTableName" type="string" required="true">

		<cfset var importPricebookEntryTable = "#this.dbObjectPrefix#pricebookEntry#this.dbObjectSuffix#">
		<cfset var currencyEnabled = getMappedField(column_remote="CurrencyIsoCode",object_remote="pricebookEntry").active>
		<cfset var pricebookEntryTableForJoin = currencyEnabled?"(select distinct pricebook2ID,currencyIsoCode from #importPricebookEntryTable#)":"#importPricebookEntryTable#">
		<cfset var result = {join="left outer join #pricebookEntryTableForJoin# pbe on pbe.pricebook2ID = #arguments.importTableName#.ID left outer join #this.baseEntity# on #this.baseEntity#.#this.baseEntityObjectKey# = #arguments.importTableName#.#this.objectPrimaryKey#"}>

		<!--- if multi-currency is not enabled, then don't join to the currency column as it won't exist --->
		<cfif currencyEnabled>
			<cfset result.join = result.join & " and #this.baseEntity#.currency = pbe.currencyIsoCode">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="buildBaseImportTable" access="private" output="false" hint="Overridden the base function in connectorUtilitiesObject.cfc because the pricebookEntry table needs creating before the pricebook table, which is non-standard.">
		<cfargument name="object" type="string" required="true">

		<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(connectorType="salesforce",object=arguments.object)> <!--- This ensure that all the objects that we care about are in memory --->

		<cfif this.baseEntity eq "pricebook">
			<cfset super.buildBaseImportTable(object="pricebookEntry")>
		</cfif>

		<!--- don't need to call this again for pricebookentry, as it has already been called when dealing with pricebooks --->
		<cfif this.baseEntity neq "pricebookEntry">
			<cfset super.buildBaseImportTable(object=arguments.object)>
		</cfif>

	</cffunction>


 	<cffunction name="getRequiredSalesforceObjectFields" access="private" output="false" returnType="struct" hint="Returns a structure of required Salesforce base table fields needed for importing an object">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The SF object that we're getting required fields for">

		<cfset var requiredFieldStruct = {}>

		<!--- could maybe do this a bit better, but for now have hard-coded it --->
		<cfif listFindNoCase("opportunityContactRole,opportunityPartner,accountPartner",arguments.object) and not isPrimaryObject(arguments.object)>
			<cfset requiredFieldStruct.role = "varchar(50)">
            <cfset requiredFieldStruct.IsPrimary = "bit"> <!--- 2015-12-14 DAN Case 446344 --->
		</cfif>

		<cfreturn requiredFieldStruct>
	</cffunction>


	<!--- <cffunction name="generateTransformationFieldSql" access="private" output="false" returnType="struct" hint="Returns the select statement needed to create the transformation fields used to create the transformation view">
		<cfargument name="object" type="string" default="#this.mappedObject#">

		<cfset var transformationFieldSelectSql = "">
		<cfset var transformationFieldWhereSql = "">
		<cfset var result = {selectSql = "", whereSql = ""}>
		<cfset var getOrgFieldsThatAreMappedToLocationFields = "">
		<cfset var locOrgFieldsStruct = arguments.object eq "organisation"?getOrganisationFieldsFromLocationFields():{}>
		<cfset var locFieldname = "">

		<cfsavecontent variable="transformationFieldSelectSql">
			<cfoutput>
				<!--- getting the export columns --->
				<cfif this.direction eq "export">
				isNull(isNull(isNull(isNull(fkm.column_relayware,fsm.column_relayware),fkmc.column_relayware),m.mappedRelaywareColumn),m.column_relayware) as selectColumn, /*link to FK mapping, then to linkToEntityType on flag, then general mapping */
				m.column_remote as asColumn,
				isNull(isNull(fs.uniqueKey,pk.COLUMN_NAME),fksc.uniqueKey) as joinToEntityTableRemoteKey, /* if we have a flag that links to an entity, then we are getting the base entity unique key, otherwise the foreign keys */
				case when m.column_remote='ownerID' then '[exportUser'+ char(172) +'personID]' else '[export'+ char(172) + m.column_remote +char(172)+ isNull(isNull(fs.entityName,pk.TABLE_NAME),fksc.entityName)+']' end as DependencyColName, <!--- if exporting an account manager, we are expecting the column to be exportUser_personID; otherwise it is standard format --->
				m.object_relayware as objectType,
				case when isNull(isNull(fs.entityName,pk.TABLE_NAME),fksc.entityName) is not null then m.column_relayware else null end as joinBaseTableColumn,

				<!--- getting the import columns --->
				<cfelse>
				case when rtrim(ltrim(m.column_remote)) != '' and m.mappingType = 'field' then isNull(isNull(isNull(isNull(fs.uniqueKey,fks.uniqueKey),fksc.uniqueKey),pk.column_name),m.column_remote) when m.mappingType = 'defaultValue' then m.column_remote else null end as selectColumn, /*this will be the unique key of the related table if it exists; otherwise the object col*/
				<!--- this is dealing with any organisation fields that we need to get from location fields --->
					<cfif arguments.object eq "organisation" and this.baseEntity neq "organisation">
						case
							<cfloop collection="#locOrgFieldsStruct#" item="locFieldname">
							when m.column_relayware = '#locFieldname#' then '#locOrgFieldsStruct[locFieldName]#'
							</cfloop>
							else m.column_relayware
						end
					<cfelse>
						m.column_relayware
					</cfif>
				 as asColumn,
				case when m.mappingType = 'field' then isNull(isNull(isNull(fsm.column_relayware,fkm.column_relayware),fkmc.column_relayware),m.mappedRelaywareColumn) else null end as joinToEntityTableRemoteKey,
				case when m.column_remote='ownerID' then '[importUser' + char(172) +'ownerID]' else '[import' + char(172) + m.column_relayware + char(172) + isNull(isNull(fsm.object_remote,fkm.object_remote),fkmc.object_remote)+']' end as DependencyColName, <!--- eg: import¬sponsorPersonID¬contact --->
				m.object_remote as objectType,
				case when isNull(isNull(fs.entityName,pk.TABLE_NAME),fksc.entityName) is not null and m.mappingType = 'field' then m.column_remote else null end as joinBaseTableColumn, /* if we have a join table, then get the column of the base join table */
				</cfif>

				case when m.mappingType = 'field' then isNull(isNull(fs.entityName,pk.TABLE_NAME),fksc.entityName) else null end as joinToEntityTable,
				case when cm.mappingType='C' then ltrim(rtrim(cm.value_relayware))+'='''+ltrim(rtrim(cm.value_remote))+'''' else null end as objJoin <!--- very specific to the opportunityPartner and opportunityContactRole mappings. Would eventually like to get rid of this --->
			</cfoutput>
		</cfsavecontent>


		<!---
			this is a case where we are dealing with a parent-child heirarchy. In this case, we may not have any organisation fields in the mappings so we then have to insert an organisation
			based on location mappings (ie use sitename for organisationName,specificUrl for organisationUrl, location countryId for organisation countryId)
		--->
		<cfif arguments.object eq "organisation" and this.baseEntity neq "organisation">

			<cfquery name="getOrgFieldsThatAreMappedToLocationFields">
				select isNull(b.column_relayware,m.column_relayware) as locField from vConnectorMapping m
					left join
					(select column_relayware,
						case
							<cfloop collection="#locOrgFieldsStruct#" item="locFieldname">
							when column_relayware =  <cf_queryparam value="#locOrgFieldsStruct[locFieldName]#" CFSQLTYPE="CF_SQL_VARCHAR" > then '#locFieldname#'
							</cfloop>
						end as orgColMapping
						from vConnectorMapping where object_relayware='organisation' and active=1) b on m.column_relayware = b.orgColMapping
				where object_relayware = 'Location'
					and active=1
					and m.column_relayware in (#listQualify(structKeyList(locOrgFieldsStruct),"'")#)
					and b.orgColMapping is null
			</cfquery>

			<cfif getOrgFieldsThatAreMappedToLocationFields.recordCount>
				<cfset transformationFieldWhereSql = "or (m.column_relayware in (#quotedValueList(getOrgFieldsThatAreMappedToLocationFields.locField)#) and m.object_relayware='Location')">
			</cfif>
		</cfif>

		<cfset result.selectSql = transformationFieldSelectSql>
		<cfset result.whereSql = transformationFieldWhereSql>
		<cfreturn result>
	</cffunction> --->


	<cffunction name="generateTransformationViewSql" access="private" output="false" returnType="string" hint="Returns the sql statement for the transformation view">

		<cfset var view_sql = "">

		<!--- have to work out a special query of the opportunityPartner and opportunityContactRole objects, as we have roles mapped to values. This means that our export views consists of a number of unions
			for the given roles
		--->
		<cfif listFindNoCase("opportunityPartner,opportunityContactRole",arguments.object) and not isPrimaryObject(arguments.object)>

			<cfset var theSelectColumn = "">
			<cfset var selectDependency = "">
			<cfset var selectColumnJoin = "">
			<cfset var whereClause = getTransformationViewWhereClause()>
			<cfset var baseEntityTable = "">

			<!--- the same where clause as give to opportunities. we don't want to export items where the opps haven't changed or are suspended. Again, might be a better' --->
			<cfprocessingdirective suppresswhitespace="yes">
				<cfsavecontent variable="view_sql">
					<cfoutput>
						<cfloop query="arguments.transformationInfo">
							<cfset baseEntityTable = replaceNoCase(joinToEntityTable,"v","")>
							<cfset theSelectColumn = "#joinToEntityTable#.#selectColumn#">
							<cfset selectDependency = "#joinToEntityTable#.#joinToEntityTableUniqueKey# as [export#application.delim1##asColumn##application.delim1##baseEntityTable#]">
							<cfset selectColumnJoin = "left join #baseEntityTable# as #joinToEntityTable# on #joinToEntityTableUniqueKey# = #objectType#.#joinBaseTableColumn#">

							<cfset var fieldJoinInfo = getFieldMappingJoinInfo(columnName=selectColumn,object="opportunity")>
							<cfif fieldJoinInfo.overrideExists>
								<cfset theSelectColumn = fieldJoinInfo.select>
								<cfset selectDependency = fieldJoinInfo.selectDependency>
								<cfset selectColumnJoin = fieldJoinInfo.join>
							</cfif>
							select #theSelectColumn# as #asColumn#,
								#selectDependency#,
								#this.baseEntityObjectKey# as #this.object#ID,
								#objectType#.#this.baseEntityPrimaryKey# as #getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field#,
                                <!--- START: 2015-12-14 DAN Case 446344 --->
                                #findNoCase("role=",objJoin)?replaceNoCase(objJoin,"role=",""):"''"# as role,
                               	#findNoCase("IsPrimary=",objJoin)?replaceNoCase(objJoin,"IsPrimary=",""):"'FALSE'"# as IsPrimary,
                                <!--- END: 2015-12-14 DAN Case 446344 --->
								case when #joinBaseTableColumn neq ""?joinBaseTableColumn:selectColumn# is null then 1 else q.isDeleted end as isDeleted,
								null as connectorResponseID
							from vConnectorRecords q
								inner join #objectType# ON q.entityTypeID=#application.entityTypeID.opportunity# and q.direction='E' and #objectType#.opportunityID = q.objectID and q.object = '#objectType#' and q.connectorType = '#this.connectorType#'
								#selectColumnJoin#
							where
								<!---suspended.entityId is null--->
								queueStatus = 'synching'
								#preserveSingleQuotes(whereClause)#
							<cfif currentRow neq arguments.transformationInfo.recordCount>
								union
							</cfif>
						</cfloop>
					</cfoutput>
				</cfsavecontent>
			</cfprocessingdirective>

		<cfelse>
			<cfset view_sql = super.generateTransformationViewSql(argumentCollection=arguments)>

			<!--- if we're importing an organisation, then we we want to filter the accounts to only show the HQ accounts, as these make up the organisations --->
			<cfif this.direction eq "import" and  arguments.object eq "organisation" and this.baseEntity neq "organisation">

				 <!--- organisations are those accounts where the parentID is null. We don't process organisation deletions, so remove them from the view. We also remove organisations that don't have a country as these will fail import. --->
				 <!--- TODO: probably good to create a function that would get me the mapping, whether it's core or overridden --->
				<cfset view_sql = view_sql& " and isNull(#replaceNoCase(this.objectDataTable,'connector_','')#.parentID,'') = '' and q.isDeleted = 0">
				<!--- NJH 2016/10/17 - JIRA PROD2016-2540 if the countryId field has mapped values, then use the sql for mapped values. Ideally that needs to go into a function. --->
				<cfif not application.getObject("connector.com.connector").doesMappingHaveMappedValues(object_relayware="location",column_relayware="countryID")>
					<cfset var orgCountryIDJoinInfo  = getFieldMappingJoinInfo(columnName="countryID",object="organisation")>
					<cfset view_sql = view_sql& " and #orgCountryIDJoinInfo.overrideExists?orgCountryIDJoinInfo.select:'country.countryID'# is not null">
				<cfelse>
					<cfset var mappedCountryField = getMappedField(object_relayware=this.baseEntity,column_relayware="countryId").field>
					<!--- TODO: move the mappedValues sql into a function --->
					<cfset view_sql = view_sql& " and dbo.replaceConnectorMappingValue('location','#mappedCountryField#',Salesforce_#this.baseObject#.#mappedCountryField#,'I',',',';','Salesforce') is not null">
				</cfif>
			</cfif>
		</cfif>

		<cfreturn view_sql>
	</cffunction>



	<!----
		**************************************************************************************************************
		PRE-PROCESSING FUNCTIONS
		**************************************************************************************************************
	--->
	<cffunction name="preProcess_ImportOpportunityStages" access="private" output="false" hint="Import any missing opportunity stages" direction="import" object="opportunity" returnType="struct">

		<cfset var result = {isOK=true,message=""}>

		<!--- bring in any missing opportunity stages, so that the stage exists for any incoming opportunities --->
		<cfset var oppStageResult= this.api.send(object="opportunityStage",method="query",queryString="select defaultProbability,masterLabel,isActive,sortOrder from opportunityStage where isActive=TRUE")>

		<cfif oppStageResult.isOK and oppStageResult.successResult.tablename neq "">
			<cfset var insertNewOpportunityStage = "">

			<cfquery name="insertNewOpportunityStage">
				insert into oppStage (probability,opportunityStage,liveStatus,stageTextID,sortOrder,status)
				select distinct case when defaultProbability = '' then defaultProbability else 0 end, masterLabel,isActive,left(replace(masterLabel,' ',''),30),case when s.sortOrder = '' then 0 else s.sortOrder end,left(masterLabel,40)
				from #oppStageResult.successResult.tablename# s
					left join oppStage os on os.opportunityStage = masterLabel
				where os.status is null
			</cfquery>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="preProcess_ImportPricebookEntries" access="private" output="false" hint="Import pricebookEntries for the given pricebooks, as we need pricebookEntry currencies to import pricebooks" direction="import" object="pricebook" returnType="struct">

		<cfset var result = {isOK=true,message=""}>
		<cfset var pricebookEntries = getDataToImport(object=getMappedObject(object_relayware="pricebookEntry"))>
		<cfset getDataForObjectsInQueue(object="PricebookEntry",getDataForRecordsWithDependencies=true)> <!--- we may need to get data for items already in the queue... --->

		<cfreturn result>
	</cffunction>


	<cffunction name="preProcess_ImportProductGroups" access="private" output="false" hint="Import any missing product groups." direction="import" object="product" returnType="struct">

		<cfset var result = {isOK=true,message=""}>
		<cfset var mappedProductGroupField = getMappedField(object_relayware=this.baseEntity,column_relayware="productGroupID").field>
		<cfset var getNewProductGroups = "">
		<cfset var tmpProductGroup = getTempTableName()>
		<cfset var createFlagView = "">

		<!--- get any missing product groups --->
		<cfquery name="getNewProductGroups">
			select distinct ROW_NUMBER() over (order by p.#mappedProductGroupField#) AS upsertUniqueID,
				null as productGroupID,
				ltrim(rtrim(p.#mappedProductGroupField#)) as description_defaultTranslation,
				ltrim(rtrim(p.#mappedProductGroupField#)) as title_defaultTranslation,
				ltrim(rtrim(p.#mappedProductGroupField#)) as productGroupName
			into #tmpProductGroup#
			from
				(select distinct #mappedProductGroupField# from #this.objectDataTable#) p
				left join productGroup pg on ltrim(rtrim(pg.description_defaultTranslation)) = ltrim(rtrim(p.#mappedProductGroupField#))
			where pg.description_defaultTranslation is null
				and ltrim(rtrim(p.#mappedProductGroupField#)) != ''

			select * from #tmpProductGroup#
		</cfquery>

		<cfset var logStruct = {tablename=tmpProductGroup}>
		<cfset log_(label="Import Product Groups",logStruct=logStruct)>

		<cfif getNewProductGroups.recordCount>
			<cfquery name="createFlagView">
				exec createFlagView @entityName = 'ProductGroup', @viewName = 'vProductGroup'
			</cfquery>
			<cfset this.connectorDAO.upsertEntity(tablename=tmpProductGroup,entityType="productGroup")>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!----
		**************************************************************************************************************
		POST-PROCESSING FUNCTIONS
		**************************************************************************************************************
	--->

	<cffunction name="postProcess_ConvertLead" access="private" output="false" returnType="struct" hint="Convert leads on Salesforce where they have been converted on Relayware" direction="export" object="lead">

		<cfset var result = {isOK=true,message=""}>
		<cfset var leadsToConvert = getTempTableName()>
		<cfset var getLeadsToConvert = "">
		<cfset var getLeadsToConvertResult = {}>
		<cfset var convertLeadResult = {}>
		<cfset var rwLeadIDMappedField = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field>

		<cfquery name="getLeadsToConvert" result="getLeadsToConvertResult">
			select distinct l.crmLocID as accountID,
				p.crmPerID as contactID,
				case when o.crmOppID is null then 'false' else 'true' end as doNotCreateOpportunity,
				v.Id as leadID,
				'#getConvertedLeadSource()#' as convertedStatus
			into #leadsToConvert#
			from #this.synchDataView# v
				inner join lead on lead.leadID = v.#rwLeadIDMappedField#
				inner join opportunity o on lead.convertedOpportunityID = o.opportunityID
				left join person p on lead.convertedPersonID = p.personID
				left join location l on lead.convertedLocationID = l.locationID
			where
				o.crmOppId is null
				and v.ID is not null
		</cfquery>

		<cfset getLeadsToConvertResult.tablename = leadsToConvert>
		<cfset log_(label="Leads to Convert",logStruct=getLeadsToConvertResult)>

		<cfif getLeadsToConvertResult.recordCount>
			<cfset convertLeadResult = this.api.send(method="convertLead",object="lead",tablename=leadsToConvert)>

			<cfif convertLeadResult.isOK>

				<cfset var convertedEntity = "">
				<cfset var entityUniqueKey = "">
				<cfset var entityCrmID = "">
				<cfset var mappedEntityObject = "">
				<cfset var updatePOLAndOppDataOnRelayware = "">

				<!--- set the crmIDs of the pol and opportunity tables to reflect the newly created records on SF --->
				<cfquery name="updatePOLAndOppDataOnRelayware">
					<cfloop list="location,person,opportunity" index="convertedEntity">
						<cfset entityUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=convertedEntity).uniqueKey>
						<cfset entityCrmID = getMappedField(object_relayware=convertedEntity,column_remote=this.objectPrimaryKey).field>
						<cfset mappedEntityObject = application.getObject("connector.com.connector").getPrimaryConnectorObjects(object=convertedEntity,direction="export").object_remote>

						update #convertedEntity#
							set #entityCrmID# = s.#mappedEntityObject#ID,
							lastUpdated = getDate(),
							lastUpdatedBy  = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">
						from
							#this.synchDataView# v
							inner join #convertLeadResult.successResult.tablename# s on v.ID = s.leadID
							inner join lead l on l.crmLeadID = s.leadID
							inner join #convertedEntity# ce on ce.#entityUniqueKey# = l.converted#entityUniqueKey#
						where
							len(isNull(s.#mappedEntityObject#ID,'')) > 0
							and len(isNull(ltrim(rtrim(ce.#entityCrmID#)),'')) = 0
							and s.success = 1
					</cfloop>
				</cfquery>

				<!--- if a message column is returned, then it indicates that at least one record has errored --->
				<cfif listFindNoCase(convertLeadResult.successResult.columnList,"message")>
					<cfset var logErrorsFromLeadConversion = "">

					<!--- log the error against the RW lead ID as this is on a lead export. --->
					<cfquery name="logErrorsFromLeadConversion">
						insert into #arguments.errorTablename# (message,statusCode,connectorResponseID,object,objectID)
						select e.message,e.statusCode,e.connectorResponseID,'lead',v.#rwLeadIDMappedField#
						from #convertLeadResult.successResult.tablename# e
							inner join #this.synchDataView# v on v.ID = e.leadId
							left join #arguments.errorTablename# pe on pe.objectID = v.#rwLeadIDMappedField# and pe.object = 'lead'
						where
							pe.objectID is null
							and e.success = 0
					</cfquery>
				</cfif>
			</cfif>
		</cfif>

		<cfset structAppend(result,convertLeadResult)>
		<cfreturn result>
	</cffunction>


	<cffunction name="getConvertedLeadSource" access="private" output="false" returnType="string" hint="Gets the status for a converted lead">

		<cfif not structKeyExists(application.connector,"convertedLeadStatus") or application.connector.convertedLeadStatus eq "">
			<cfset var leadStatusResponse = this.api.send(queryString="select masterLabel from leadStatus where IsConverted=true",object="leadStatus",method="query")>

			<cfif leadStatusResponse.isOK>
				<cfset var getConvertedLeadStatus = "">

				<cfquery name="getConvertedLeadStatus">
					select masterLabel from #leadStatusResponse.successResult.tablename#
				</cfquery>

				<cfif getConvertedLeadStatus.recordCount>
					<cfset application.connector.convertedLeadStatus = getConvertedLeadStatus.masterLabel[1]>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn application.connector.convertedLeadStatus>
	</cffunction>

	<!--- <cffunction name="runPostProcessingForPricebook2Import" access="private" output="false" hint="Set any pricebooks not imported to deleted">
		<cfset deleteEntitiesPostProcessing()>
	</cffunction>


	<cffunction name="runPostProcessingForPricebookEntryImport" access="private" output="false" hint="Set any pricebook entries not imported to deleted">
		<cfset deleteEntitiesPostProcessing()>
	</cffunction>


	<cffunction name="runPostProcessingForProduct2Import" access="private" output="false" hint="Set any products not imported to deleted">
		<cfset deleteEntitiesPostProcessing()>
	</cffunction>


	<cffunction name="deleteEntitiesPostProcessing"  access="private" output="false" hint="Set any entites that are not included in the import to deleted.">

		<cfif request.ignoreTime>
			<cfset var deleteEntities = "">

			<cfquery name="deleteEntities">
				update #this.baseEntity#
					set deleted=1
				from
					#this.baseEntity# e
					left join #this.objectDataTable# t on t.#this.objectPrimaryKey# = e.#this.baseEntityObjectKey#
				where
					t.#this.objectPrimaryKey# is null
			</cfquery>
		</cfif>
	</cffunction> --->


	<cffunction name="collateSuccessResponsesForNonPrimaryObjectExport" access="private" output="false" hint="Collate the success responses for the opportunityContactRole and partner objects primarily. But give chance to override for other objects">
		<cfargument name="successTableName" type="string" required="true" hint="The name of the table to populate with successful records">
		<cfargument name="object" type="string" required="true" hint="The name of the object whose data we are exporting">
		<cfargument name="resultTableName" type="string" required="true" hint="The name of the table containing the export result">
		<cfargument name="exportDataTableName" type="string" required="true" hint="The name of the table containing the data to export">
		<cfargument name="rowNumberIndex" type="numeric" required="true" hint="The rows to process">


		<cfset var collateResponses = "">
		<cfset var relaywareIDField = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)>
		<cfset var primaryObjectIdColumn = getParentKeyFieldForObject(object=arguments.object)>
		<cfset var relatedColumn_remote = getRelatedColumnRemoteForObject(object=arguments.object)>

		<cfquery name="collateResponses">
			insert into #arguments.successTableName# (#this.baseEntityPrimaryKey#,#this.baseEntityObjectKey#,action)
			select distinct ev.#relaywareIDField#,v.#primaryObjectIdColumn#, 'update' as [action]
			from #arguments.resultTableName# t
				inner join #arguments.exportDataTableName# v on t.rowNumber + (#arguments.rowNumberIndex#) = v.rowNumber
				inner join #getViewForObjectSynch(object=arguments.object)# ev on ev.opportunityID = v.opportunityID and ev.role=v.role and ev.IsPrimary=v.IsPrimary and ev.#relatedColumn_remote# = v.#relatedColumn_remote# <!--- 2015-12-14 DAN Case 446344 --->
				left join #arguments.successTableName# s on s.#this.baseEntityPrimaryKey# = ev.#relaywareIDField# and s.#this.baseEntityObjectKey#=v.#primaryObjectIdColumn#
			where success = 1
				and s.action is null
		</cfquery>
	</cffunction>


	<cffunction name="collateErrorResponsesForNonPrimaryObjectExport" access="private" output="false" hint="Collate the error responses for the opportunityContactRole and partner objects primarily. But give chance to override for other objects">
		<cfargument name="errorTablename" type="string" required="true" hint="The name of the table to populate with error records">
		<cfargument name="object" type="string" required="true" hint="The name of the object whose data we are exporting">
		<cfargument name="resultTableName" type="string" required="true" hint="The name of the table containing the export result">
		<cfargument name="exportDataTableName" type="string" required="true" hint="The name of the table containing the data to export">
		<cfargument name="rowNumberIndex" type="numeric" required="true" hint="The rows to process">


		<cfset var collateResponses = "">
		<cfset var relaywareIDField = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)>
		<cfset var primaryObjectIdColumn = getParentKeyFieldForObject(object=arguments.object)>
		<cfset var relatedColumn_remote = getRelatedColumnRemoteForObject(object=arguments.object)>

		<cfquery name="collateResponses">
			insert into #arguments.errorTablename# (object,objectId,message,field,value,connectorResponseID)
			select distinct
					<cf_queryparam value="#this.baseEntity#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					cast(cast(ev.#relaywareIDField# as float) as int),
					message,
					isNull(t.fields,cm.column_relayware),
					ev.#relatedColumn_remote#,
					t.connectorResponseID
			from #arguments.resultTableName# t
				inner join #arguments.exportDataTableName# v on t.rowNumber + (#arguments.rowNumberIndex#) = v.rowNumber
				inner join #getViewForObjectSynch(object=arguments.object)# ev on ev.opportunityID = v.opportunityID and ev.role=v.role and ev.IsPrimary=v.IsPrimary and ev.#relatedColumn_remote# = v.#relatedColumn_remote# <!--- 2015-12-14 DAN Case 446344 --->
				left join connectorColumnValueMapping cvm on cvm.[value_remote] = ev.role
				left join connectorMapping cm on cvm.connectormappingID = cm.ID
			where success = 0
		</cfquery>

	</cffunction>

	<!--- START: 2015-12-14 DAN Case 446344 --->
    <cffunction name="getNonPrimaryObjectDeleteFilterForChangedPrimaryObjects" access="private" output="false" returntype="string" hint="Gets the filter used for deleting non-primary objects for changed primary objects">
        <cfargument name="tablename" type="string" required="true">
        <cfargument name="object" type="string" required="true">
        <cfargument name="primaryObjectID" type="string" required="true">
        <cfargument name="fields" type="string" required="true" hint="One or more fields used to determine the filter. Expecting role and isPrimary to be passed through">

        <cfset var filterValue = "">
        <cfset var rolesToDeleteResult = {}>
        <cfset var getRolesToDelete = "">

        <cfloop list="#arguments.fields#" index="field">

            <cfif structKeyExists(application.connector.objectMetaData[arguments.object].fields, field) AND listFindNoCase("role,IsPrimary", field)>
                <cfset rolesToDeleteResult = structNew()>
                <cfset getRolesToDelete = "">

                <!--- delete any specific roles --->
                <cfquery name="getRolesToDelete" result="rolesToDeleteResult">
                    select distinct #field#
					from #arguments.tablename#
					where opportunityID =  <cf_queryparam value="#arguments.primaryObjectID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and #field# != <cfif field eq "role">''<cfelse>'FALSE'</cfif>
                </cfquery>

                <cfif not getRolesToDelete.recordCount>

                    <!--- delete any records for roles that we have mapped. Here, get the distinct list of roles that we have mapped --->
                    <cfquery name="getRolesToDelete" result="rolesToDeleteResult">
                        select distinct cvm.[value_remote] as #field# from connectorColumnValueMapping cvm
                            inner join vConnectorMapping m on m.ID = cvm.connectorMappingID
                        where cvm.value_relayware=<cf_queryparam value="#field#" cfsqltype="cf_sql_varchar">
                            and m.object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
                            and export=1
							and ltrim(rtrim(cvm.[value_remote])) != <cfif field eq "role">''<cfelse>'FALSE'</cfif>
                    </cfquery>
                </cfif>

                <cfset log_(label="Export (delete): Roles To Delete",logStruct=rolesToDeleteResult)>

                <!--- if we have mapped roles, then filter the deletion to the concerned roles --->
                <cfif getRolesToDelete.recordCount>
                    <cfif filterValue EQ "">
                        <cfset filterValue &= "("> <!--- its the first with results, 'opening a 'global' bracket --->
                    <cfelse>
                        <cfset filterValue &= " OR "> <!--- next field with results --->
                    </cfif>

                    <cfif field eq "role">
                        <cfset filterValue &= " role in (#quotedValueList(getRolesToDelete.role)#) ">
                    <cfelseif field eq "IsPrimary">
                        <cfset filterValue &= " isPrimary=TRUE ">
                    </cfif>

                </cfif>

            </cfif>

        </cfloop>

        <cfif trim(filterValue) NEQ "">
            <cfset filterValue &= ")"> <!--- closing a 'global' bracket --->
        </cfif>

        <cfreturn filterValue>
    </cffunction>
    <!--- END: 2015-12-14 DAN Case 446344 --->


    <cffunction name="deleteNonPrimaryObjectsForChangedPrimaryObjects" access="private" output="false" hint="Delete non-primary objects based on changes to primary objects. Essentially the opportunityContactRole and partner objects">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="object" type="string" required="true">

		<cfset var getPrimaryObjectIds = "">
		<cfset var getRolesToDelete = "">
		<cfset var nonPrimaryObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var nonPrimaryObjectWhereClause = nonPrimaryObject.whereClause>
		<cfset var primaryObjectID = "">
		<cfset var getPrimaryObjectIdsResult = structNew()>
		<cfset var parentKeyField = getParentKeyFieldForObject(object=arguments.object)>

        <!--- 2015-12-14 DAN Case 446344 --->
        <cfset var filter = "">
        <cfset var filterValue = "">

		<!--- we need to delete the non-primary records on a record by record basis. We can't do a blanket delete, because each object is going to be different as to what will need to be deleted --->
		<cfquery name="getPrimaryObjectIds" result="getPrimaryObjectIdsResult">
			select distinct #parentKeyField# as primaryObjectID from #arguments.tablename# where len(isNull(#parentKeyField#,'')) > 0
		</cfquery>

		<cfset log_(label="Export (delete): delete Non-Primary Objects For Changed Primary Objects",logStruct=getPrimaryObjectIdsResult)>

		<cfloop query="getPrimaryObjectIds">
			<cfset filter = nonPrimaryObjectWhereClause> <!--- 2015-12-14 DAN Case 446344 --->

			<!--- if the object has a role field (will be opportunityContactRole or partner object), then we want to find the specific roles that have changed and therefore that we have to delete. If none exist in the incoming table,
				that means that all the roles have been deleted, and so we want to find all the mapped roles so that we can delete them
			 --->
			<!--- START: 2015-12-14 DAN Case 446344 --->
            <cfset filterValue = getNonPrimaryObjectDeleteFilterForChangedPrimaryObjects(tablename=arguments.tablename,object=arguments.object,primaryObjectID="#getPrimaryObjectIds.primaryObjectID[currentRow]#",fields="role,IsPrimary")>
            <cfif filterValue NEQ "">
                <cfset filter = filter & "#filter eq '' ? '':' and '#" & filterValue>
            </cfif>
            <!--- END: 2015-12-14 DAN Case 446344 --->

			<cfset deleteNonPrimaryRecordsForPrimaryObject(object=arguments.object,primaryObjectID=getPrimaryObjectIds.primaryObjectID[currentRow],filter=filter)>
		</cfloop>
	</cffunction>


	<cffunction name="getExportQueryInfoForNonPrimaryObjects" access="private" output="false" returnType="struct" hint="Gets the select list and mappedParentKey for non-primary objects (opportunityContactRole and opportunityParter are examples)">
		<cfargument name="object" type="string" required="true" hint="The name of the salesforce object">

		<cfset var parentKeyField = getParentKeyFieldForObject(object=arguments.object)>
		<cfset var result = {selectColumnList=parentKeyField,parentKeyField=parentKeyField}>
		<cfset var roleField = "">
        <cfset var relatedColumnRemote = getRelatedColumnRemoteForObject(object=arguments.object)>

		<!--- in the case of a non primary object, we only do creates. --->
        <cfif relatedColumnRemote NEQ "">
            <!--- Case 451138 - only if it's not empty, this will be empty if there is more than 1 column for the object
            Aerohive have custom object to hold partners that has 3 distinct columns (Partner__c, SalesContact__c and VAD__c)
            and we currently support only 1 column, we expeacting either contactId or accountId --->
    		<cfset result.selectColumnList = listAppend(result.selectColumnList, relatedColumnRemote)> <!--- TODO: remove this hard-coded list --->
        </cfif>

		<!--- dealing with the objects that have a role - again, primarily the opportunityContactRole and Partner objects --->
		<cfloop list="role,isPrimary" index="roleField">
			<cfif structKeyExists(application.connector.objectMetaData[arguments.object].fields,roleField)>
				<cfset result.selectColumnList = listAppend(result.selectColumnList,roleField)>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="getRelatedColumnRemoteForObject" access="private" output="false" hint="Returns accountID or contactID" returnType="string">
		<cfargument name="object" type="string" required="true" hint="The name of the salesforce object">

		<cfset var getRelatedColumn_remote = "">
		<cfset var columnRemote = "">
		<!--- this gets the accountToID or contactId columns, depending on the object --->
		<cfquery name="getRelatedColumn_remote">
			select distinct column_remote from vConnectorMapping where connectorType=<cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar"> and object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfif getRelatedColumn_remote.recordCount eq 1>
			<cfset columnRemote = getRelatedColumn_remote.column_remote[1]>
		</cfif>
		<cfreturn columnRemote>
	</cffunction>


	<cffunction name="getDataToImportFromWhereClause" access="private" output="false" hint="Gets data meeting criteria of the where clause from the Remote system and puts it into the data table" returnType="struct">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">
		<cfargument name="whereClause" type="string" required="true" hint="The where clause criteria">
		<cfargument name="fieldList" type="string" required="true" hint="The list of fields to get data for.">
		<cfargument name="loopIndex" type="numeric" default="1" hint="When dealing with non-primary objects, we have to make several iterations to get all the data.">

		<cfreturn this.api.send(object=arguments.object,method="queryAll",queryString="SELECT #arguments.fieldlist# from #arguments.object# #arguments.whereClause#",throwErrorOn500Response=true,createTempTableForResponse=false,truncateTable=arguments.loopIndex eq 1?true:false)>
	</cffunction>


	<cffunction name="postDataImportForWhereClause2" access="private" output="false" hint="Runs some processing after the second iteration of getting data. Run for non-primary objects" returnType="struct">
		<cfargument name="successTableName" type="string" required="true" hint="The name of the table holding the data recently imported">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">

		<cfset var result = {isOk=true,message=""}>

		<cfif arguments.successTableName neq "">
			<!--- dealing with opportunityContactRoles and opportunityPartners. Here we are putting the primary object IDs (the opportunity IDs) into the queue, so that we are importing the opportunity  --->
			<cfif arguments.object neq this.object>

				<cfset var getPrimaryIdsThatAreNotInBaseTable = "">
				<cfset var getPrimaryIdsResult = {}>
				<cfset var parentKeyField = getParentKeyFieldForObject(object=arguments.object)>
				<cfset var tempTableName = getTempTableName()>

				<!--- if we have related data, then we need to put the baseIds into the queue so that it gets processed. This currently means that we need to get the base data, as otherwise
					the base record will contain a lot of nulls which means that all the other fields will get set to null on RW.
					NJH 2016/05/12 - Kaspersky bug fix.. set 0 as isDeleted, as the base object is not deleted. We don't want to get the deleted status of the roles for the primary object here
				--->
				<cfquery name="getPrimaryIdsThatAreNotInBaseTable" result="getPrimaryIdsResult">
					select '#this.object#' as object,t.#parentKeyField# as objectID,0 as isDeleted,t.lastModifiedDate
						into #tempTableName#
					from #arguments.successTableName# t
						left join #this.objectDataTable# o on o.ID = t.#parentKeyField#
					where
						o.ID is null
				</cfquery>

				<cfset var insertQueueResult = insertRecordsIntoQueue(tablename=tempTableName,tableMetaData=getPrimaryIdsResult,direction="import")>

				<cfset var nonPrimaryDataObjectTable = getObjectDataTable(object=arguments.object)>
				<cfset var removeDuplicateNonPrimaryObjectRecords = "">

				<!--- here we are removing any duplicate records that we might have in the non-primary object data table, as currently there is no check... The second iteration may
					insert the same records that the first iteration put in. Having duplicates in here will cause duplicate rows in the transformation views --->
				<cfquery name="removeDuplicateNonPrimaryObjectRecords">
					delete #nonPrimaryDataObjectTable#
					from #nonPrimaryDataObjectTable# nonPri
						left join (
							select ID,min(connectorResponseId) as connectorResponseID
							from #nonPrimaryDataObjectTable#
							group by ID) rowsToKeep
						on nonPri.ID = rowsToKeep.ID and nonPri.connectorResponseID = rowsToKeep.connectorResponseID
					where rowsToKeep.ID is null
				</cfquery>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getQueryInfoForDataImport" access="private" output="false" hint="Returns information needed to run a query against remote system to bring in import data" returnType="struct">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">
		<cfargument name="startDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object).lastRunUTC#" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object).currentTimeUTC#" hint="The end date for when object data should be collected. In UTC">
		<cfargument name="getModificationDataOnly" type="boolean" default="false" hint="Get only fields necessary to compare with records to be exported. (ie. ID, lastModifiedDate). This could maybe be changed to a fieldlist.">

		<cfset var result = {isOK=true,message="",whereClauseArray=arrayNew(1),fieldList=""}>
		<cfif application.getObject("connector.com.connector").isObjectMapped(object_remote=arguments.object,connectorType=this.connectorType)>
			<cfset result = super.getQueryInfoForDataImport(argumentCollection=arguments)>

			<!--- special case for pricebook imports. We need the currency from the pricebookEntry table, which means that we have to get pricebookEntries for pricebooks in the queue --->
			<cfif this.baseEntity eq "pricebook" and arguments.object eq "pricebookEntry">
				<cfquery name="getPricebooksInQueue">
					select objectID from vConnectorRecords where object='pricebook2' and isDeleted=0 and queueStatus = 'Synching'
				</cfquery>

				<cfif getPricebooksInQueue.recordCount>
					<cfset var pricebookIdList = quotedValueList(getPricebooksInQueue.objectID)>
					<cfset result.whereClauseArray[2] = "where pricebook2Id in (#preserveSingleQuotes(pricebookIdList)#)">
				</cfif>
			</cfif>
		<cfelse>
			<!--- if this is not the main mapped object, but a non-primary object such as opportunityContactRole or opportunityPartner --->
			<cfset var queryInfo = getQueryInfoForNonPrimaryObjectDataImport(object=arguments.object,argumentCollection=getWhereClauseForObjectImport(object=arguments.object,startDateTimeUTC=arguments.startDateTimeUTC,endDateTimeUTC=arguments.endDateTimeUTC))>
			<cfset result.fieldList = queryInfo.fieldList>
			<cfset result.whereClauseArray = queryInfo.whereClauseArray>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="getFullWhereClauseForObjectImportQuery" access="private" output="false" hint="Returns the query string to be used in where clause for object import" returnType="string">
		<cfargument name="object" type="string" required="true" hint="The salesforce object">
		<cfargument name="startDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object).lastRunUTC#" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" default="#getLastAndCurrentSynchTimeForObject(object=arguments.object).currentTimeUTC#" hint="The end date for when object data should be collected. In UTC">

		<cfset var whereClauseStruct = getWhereClauseForObjectImport(object=arguments.object,startDateTimeUTC=arguments.startDateTimeUTC,endDateTimeUTC=arguments.endDateTimeUTC)>
		<cfset var whereClause = whereClauseStruct.timeDateWhereClause>

		<!--- include the where clause  of the object and add it to the time where clause --->
		<cfif whereClauseStruct.objectWhereClause neq "">
			<cfif whereClause neq "">
				<cfset whereClause = whereClause & " and ">
			</cfif>
			<!--- NJH 2016/03/07 if the object where clause contains an or, then wrap where clause in brackets. originally didn't check the 'or', but found that when there was an 'in' statement in the where clause, we ended up
				with a SF error because it complained of a nested in statement. So, only add brackets if necessary --->
			<cfset var objectWhereClause = findNoCase(" or ",whereClauseStruct.objectWhereClause)?" (#whereClauseStruct.objectWhereClause#)":" #whereClauseStruct.objectWhereClause#">
			<cfset whereClause = whereClause & objectWhereClause>
		</cfif>

		<!--- include the parent object where clause in our query if the object has a parent --->
		<cfif whereClauseStruct.parentSelectWhereClause neq "">
			<cfset var thisObject = getConnectorObject(object=arguments.object,relayware=false)>
			<cfset whereClause = whereClause & whereClauseStruct.parentSelectWhereClause>
		</cfif>

		<cfif whereClause neq "">
			<cfset whereClause = "where " & whereClause>
		</cfif>

		<!--- this additional where clause was needed for kaspersky to import all opportunity products associated with opportunities just imported. So, we further filter the normal query to get only opportunity products for certain opportunties in this case --->
		<cfif structKeyExists(arguments,"additionalWhereClause")>
			<cfset whereClause = whereClause & arguments.additionalWhereClause>
		</cfif>

		<cfreturn whereClause>
	</cffunction>

	<!--- START: 2015-12-14 DAN Case 446344 --->
    <cffunction name="getWhereClauseForNonPrimaryObject" access="public" output="false" returntype="struct" hint="Gets the where clause for non-primary object - generally opportunityContactRole">
        <cfargument name="object" type="string" required="true" hint="The non-primary object. Generally an opportunityContactRole and/or an partner object"><>
        <cfargument name="fields" type="string" required="true" hint="One or more fields used to determine the where clause">

        <cfset var getRoles = "">
        <cfset var result = structnew()>
        <cfset result.value = "">
        <cfset result.fieldList = "">

        <cfloop list="#arguments.fields#" index="field">

            <cfif structKeyExists(application.connector.objectMetaData[arguments.object].fields, field) AND listFindNoCase("role,IsPrimary", field)>
                <cfset result.fieldList = listAppend(result.fieldList, field)>

                <cfset getRoles = "">

                <!--- get the roles that we are interested in --->
                <cfquery name="getRoles">
                    select distinct value_remote as #field# from connectorColumnValueMapping cm
                        inner join vConnectorMapping m on m.ID = cm.connectorMappingID and m.active=1 and m.import=1
                    where
                        object_remote = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
                        and value_relayware = <cf_queryparam value="#field#" cfsqltype="cf_sql_varchar">
						and ltrim(rtrim(value_remote)) != <cfif field eq "role">''<cfelse>'FALSE'</cfif>
                </cfquery>

                <cfif getRoles.recordCount>
                    <cfif result.value EQ "">
                        <cfset result.value &= "("> <!--- its the first with results, 'opening a 'global' bracket --->
                    <cfelse>
                        <cfset result.value &= " OR "> <!--- next field with results --->
                    </cfif>

                    <cfif field eq "role">
                        <cfset result.value &= " role in (#quotedValueList(getRoles.role)#) ">
                    <cfelseif field eq "IsPrimary">
                        <cfset result.value &= " IsPrimary=TRUE ">
                    </cfif>

                </cfif>

            </cfif>

        </cfloop>

        <cfif trim(result.value) NEQ "">
            <cfset result.value &= ")"> <!--- closing a 'global' bracket --->
        </cfif>

        <cfreturn result>
    </cffunction>
    <!--- END: 2015-12-14 DAN Case 446344 --->


	<cffunction name="getTimeDateWhereClauseForObjectImport" access="private" output="false" hint="Returns the time/date where clause needed for data import" returnType="string">
		<cfargument name="startDateTimeUTC" type="date" required="true" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" required="true" hint="The end date for when object data should be collected. In UTC">

		<cfreturn "lastModifiedDate >= #convertDateToSalesForceFormat(dateToConvert=arguments.startDateTimeUTC)# and lastModifiedDate <= #convertDateToSalesForceFormat(dateToConvert=arguments.endDateTimeUTC)# and lastModifiedByID != '#application.connector.api.userId#'">
	</cffunction>


	<cffunction name="getQueryFieldListForDataImport" access="private" output="true" returnType="string" hint="Get the list of fields to import for an object">
		<cfargument name="object" type="string" required="true" hint="The Salesforce object">
		<cfargument name="modifiedDataFieldsOnly" type="boolean" required="true" hint="Whether we are just getting the IDs of records that have been modified">

		<cfset var modificationFields = this.objectPrimaryKey>
		<!--- NJH 2016/03/07 JIRA PROD2016-472 - we check whether the RW ID field is mapped, because it may not be (in the case of SF attachments where we can't add custom fields to
			the object). In this case, we want to make sure that the RW ID field is not added of fields to retrieve. --->
		<cfset var entityMappedField = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field>
		<cfif entityMappedField neq "">
			<cfset modificationFields = listAppend(modificationFields,entityMappedField)>
		</cfif>

		<cfset queryFieldList = arguments.modifiedDataFieldsOnly?modificationFields:getConnectorObject(object=arguments.object,relayware=false).importColumnList>

		<cfif not listFindNoCase(queryFieldList,"lastModifiedDate")>
			<cfset queryFieldList = listAppend(queryFieldList,"lastModifiedDate")>
		</cfif>

		<cfif not listFindNoCase(queryFieldList,"isDeleted")>
			<cfset queryFieldList = listAppend(queryFieldList,"isDeleted")>
		</cfif>

		<cfreturn queryFieldList>
	</cffunction>


	<cffunction name="getQueryInfoForNonPrimaryObjectDataImport" type="private" hint="Gets query info (such as field list and where clause) for the import of non-primary objects" returnType="struct" output="false">
		<cfargument name="object" type="string" required="true" hint="The non-primary object. Generally an opportunityContactRole and/or an partner object">
		<cfargument name="timeDateWhereClause" type="string" required="true" hint="The time filter for the current run">
		<cfargument name="objectWhereClause" type="string" default="" hint="The filter applied for the object to be imported">
		<cfargument name="parentSelectWhereClause" type="string" default="" hint="The filter for the primary object's parent. This is a whole select string. (ie. selectId from parent where Id... see getWhereClauseForObjectImport)">

		<cfset var thisObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var baseObjectWhereClause = getConnectorObject(object=this.baseObject,relayware=false).whereClause>
		<cfset var parentKeyField = getParentKeyFieldForObject(object=arguments.object)>
		<cfset var result = {whereClauseArray = arrayNew(1),fieldList=listAppend(getQueryFieldListForDataImport(object=arguments.object,modifiedDataFieldsOnly=false),"#parentKeyField#,#this.objectPrimaryKey#")}> <!--- get the objectId and the parentKey field --->
		<cfset var thisObjectWhereClause = "where">

		<cfif arguments.objectWhereClause neq "">
			<cfset thisObjectWhereClause =  thisObjectWhereClause & " (#arguments.objectWhereClause#) and">
		</cfif>
		<cfif baseObjectWhereClause neq "">
			<cfset baseObjectWhereClause = " and (" &baseObjectWhereClause &")">
		</cfif>

		<!--- The where clause for non-primary objects is made up of two elements, which are similar, but not. There is a slight difference:
			1. Get all non-primary objects that meet their own where clause and that belong to base objects that have been modified in the given time span
			2. Get all non-primary objects that have been modified in the time span given and that belong to relevent base objects
		 --->
		<cfset var parentFilter = "select ID from #this.baseObject# where #arguments.timeDateWhereClause#">
		<!--- if a parent has been assigned, then add the parent (baseObject) where clause; otherwise we're only getting objects where primary objects have been modified between date period --->
		<cfif thisObject.parentObject neq "">
			<cfset var parentObjectWhereClause = getConnectorObject(object=this.baseObject,relayware=false).whereClause>
			<cfif parentObjectWhereClause neq "">
				<cfset parentFilter = parentFilter & " and " & parentObjectWhereClause>
			</cfif>
		</cfif>

		<!--- running this here should the application variable below not exist for whatever reason --->
		<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(connectorType="salesforce",object=arguments.object)>

		<!--- dealing with the objects that have a role - again, primarily the opportunityContactRole and Partner objects --->
		<!--- START: 2015-12-14 DAN Case 446344 --->
        <cfset var fieldWhereClause = getWhereClauseForNonPrimaryObject(object=arguments.object, fields="role,IsPrimary")>
        <cfif fieldWhereClause.fieldList NEQ "">
            <cfset result.fieldList = listAppend(result.fieldList, fieldWhereClause.fieldList)>
        </cfif>

		 <cfif fieldWhereClause.value NEQ "">
			 <cfset fieldWhereClause.value = " and " & fieldWhereClause.value>
		</cfif>

		<cfset result.whereClauseArray[1] = "#thisObjectWhereClause# #parentKeyField# in (#parentFilter#) #fieldWhereClause.value#"> <!--- get all the related objects for the primary objects that we have gotten --->
		<!--- <cfset result.whereClauseArray[1] = thisObject.parentObject eq ""?thisObject.whereClause:"#thisObjectWhereClause# #thisObject.parentKeyField# in (select ID from #thisObject.parentObject# where #arguments.timeDateWhereClause# #arguments.baseObjectWhereClause#)"> ---> <!--- get all the related objects for the primary objects that we have gotten --->
		<cfset result.whereClauseArray[2] = "#thisObjectWhereClause# #timeDateWhereClause# #fieldWhereClause.value# #arguments.parentSelectWhereClause#"> <!--- get all the related objects that have changed for only those base objects that we are interested in. Keep the index as 2 as the getDataToImport function relies on this being 2. Should be changed, but it's like that for now--->
        <!--- END: 2015-12-14 DAN Case 446344 --->

		<cfreturn result>
	</cffunction>


	<cffunction name="getDataForObjectIDs" access="private" output="false" returnType="struct" hint="Gets data from the remote system for the given IDs. This will populate the Salesforce object import tables">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="objectIds" type="array" required="true">
		<cfargument name="fieldList" type="string" default="">

		<cfset var successResult = {tablename="",recordCount=0,columnList=""}>
		<cfset var errorResult = {tablename="",recordCount=0,columnList=""}>
		<cfset var result = {isOk=true,message="",successResult=successResult,errorResult=errorResult}>
		<cfset var idsToImport = arrayNew(1)>
		<cfset var importObject = "">
		<cfset var objectFieldList = "">
		<cfset var connectorResponseIDList = 0>

		<!--- if we're not dealing with a user object, then get the fields and the list of objects that we need to get data for... most of the time, the data should be for the object passed through in arguments. However, currently, in the
			case of opportunities, we need to grab data for the opportunityPartners and opportunityContactRoles for the opportunities in the queue, as the absence of data will null the mapped fields on the RW opportunity table. --->
		<cfif arguments.object neq "user">
			<cfset var mappedObject = getConnectorObject(object=arguments.object,relayware=false)>
			<cfset var objectList = getConnectorObject(object=getMappedObject(object_relayware=getMappedObject(object_remote=arguments.object)),relayware=false).objectList>
			<cfset objectList = listAppend(listDeleteAt(objectList,listFindNoCase(objectList,arguments.object)),arguments.object)> <!--- stick the main object at the end of the list so that the temp table returned contains data for the main object--->

			<cfif arguments.fieldList eq "">
				<cfset arguments.fieldList = mappedObject.fieldColumnList>
			</cfif>
		<cfelse>
			<cfset objectList = arguments.object>
		</cfif>

		<cfloop list="#objectList#" index="importObject">

			<!--- dealing with opportunityPartners and opportunityContactRoles. We want to get the data for these objects for any opportunities that are in the queue. We first get the IDs of the records that we're interested in, and
				then use those IDs for the retrieve method
			--->
			<cfif importObject neq arguments.object>
				<cfset var retreiveInfo = getRetrieveInfoForNonPrimaryObjects(object=importObject,primaryObjectIDs=arguments.objectIds)>
				<cfset var objectFieldList = retreiveInfo.objectFieldList>
				<cfset idsToImport = retreiveInfo.idsToImport>
			<cfelse>
				<cfset objectFieldList = arguments.fieldList>
				<cfset idsToImport = arguments.objectIDs>
			</cfif>

			<cfset var thisTempTableSuffix = request.tempTableSuffix> <!--- storing the original suffix, as we will be modiying the request variable for each thread that gets processed, so that there is no contention on tables.--->

			<cfif arrayLen(idsToImport)>

				<cfset var processNumRows = 2000>
				<cfset var numberOfThreads = int(arrayLen(idsToImport)/processNumRows)>
				<cfset var hash = createUUID()>
				<cfset var numOfElements = 0>
				<cfset var threadNumber = 0>
				<cfset var threadNamePrefix = "runRetrieveThread#hash#">

				<cfif (arrayLen(idsToImport) MOD processNumRows) neq 0>
					<cfset numberOfThreads++>
				</cfif>

				<!--- thread these retrieve calls --->
				<cfloop from=1 to=#numberOfThreads# index="threadNumber">
					<cfset numOfElements = processNumRows>

					<cfif threadNumber eq numberOfThreads>
						<cfset numOfElements = arrayLen(idsToImport) - ((threadNumber-1)*processNumRows)>
					</cfif>

					<cfif numOfElements neq 0>
						<cfset var retrieveIdsArray = arraySlice(idsToImport,(processNumRows*(threadNumber-1))+1,numOfElements)>

						<cfset var objectTable = getObjectDataTable(object=importObject)>
						<cfset var passThruStruct = {retrieveIdsArray=retrieveIdsArray,importObject=importObject,objectFieldList=objectFieldList,objectTable=objectTable,tempTableSuffix="_#threadNumber#"&thisTempTableSuffix}>

						<cfthread action="run" name="#threadNamePrefix##threadNumber#" passThruStruct=#passThruStruct#>
							<cftry>
								<cfset thread.result = passThruStruct>
								<cfset request.tempTableSuffix = passThruStruct.tempTableSuffix>

								<cfset var dataResult = this.api.send(object=passThruStruct.importObject,ids=passThruStruct.retrieveIdsArray,fieldList=passThruStruct.objectFieldList,method="retrieve",throwErrorOn500Response=true)>

								<!--- if it is not the user table, then input it into the base object table --->
								<cfif dataResult.successResult.tablename neq "" and passThruStruct.importObject neq "user">

									<cfset var populateTables = "">

									<!--- populate the data table with the data that we have just retrieved --->
									<cfquery name="populateTables">
										insert into #passThruStruct.objectTable# (#passThruStruct.objectFieldList#,connectorResponseID)
										select distinct t.#replace(passThruStruct.objectFieldList,",",",t.","ALL")#, t.connectorResponseID
										from #dataResult.successResult.tablename# t
											left join #passThruStruct.objectTable# s on s.ID = t.ID
										where
											s.ID is null
											and t.ID is not null
									</cfquery>
								</cfif>

								<cfset thread.result = dataResult>

								<cfcatch>
									<cfset thread.result.isOk=false>
									<cfset thread.result.message = cfcatch.message>

									<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch)>
								</cfcatch>
							</cftry>
						</cfthread>
					</cfif>
				</cfloop>

				<cfset var threadList = "">
				<cfloop from="1" to="#numberOfThreads#" step="1" index="threadNumber">
				 	<cfset threadList = listAppend(threadList,"#threadNamePrefix##threadNumber#")>
				</cfloop>

				<cfthread action="join" name="#threadList#"/>

				<cfloop from="1" to="#numberOfThreads#" step="1" index="threadNumber">
					<cfset var threadResult = cfthread["#threadNamePrefix##threadNumber#"].result>
				 	<cfset log_(label="Retrieve Data for #importObject# IDs",logStruct=threadResult,tempTableSuffix="_#threadNumber#"&thisTempTableSuffix)>
				 	<cfset connectorResponseIDList = listAppend(connectorResponseIDList,threadResult.responseID)>
				</cfloop>

				<!--- resetting the suffix back to the original --->
				<cfset request.tempTableSuffix = thisTempTableSuffix>
			</cfif>
		</cfloop>


		<!--- if it is not the user table, but one of the primary tables --->
		<cfset var primaryObjects = application.getObject("connector.com.connector").getPrimaryConnectorObjects(connectorType=this.connectorType)>
		<cfif listFindNoCase(valueList(primaryObjects.object_remote),importObject) and arrayLen(idsToImport)>
			<cfset var getData = "">
			<cfset var getDataResult = "">

			<cfquery name="getData" result="getDataResult">
				select * from #objectTable#
			</cfquery>

			<cfset result.successResult = getDataResult>
			<cfset result.successResult.tablename = objectTable>

			<cfset var setDataExistsWhereNoDataReturned = "">

			<!--- here we are setting the dataExists column to 0 any time where we've requested data for a given ID but not had a response back. This generally indicates that the record has been deleted. Early, this record
				would remain in the queue as pending, but now, they are removed from the queue unless they are a dependency--->
			<cfquery name="setDataExistsWhereNoDataReturned">
				;with xmlnamespaces (DEFAULT 'urn:partner.soap.sforce.com')

				<!---select x.Col.value('.[1]','VARCHAR(20)') as objectID, '#importObject#' as object, 'No Data Returned From Salesforce' as message, null as value, null as field,r.ID as connectorResponseID
				into #result.errorResult.tablename# --->
				update vConnectorQueue set dataExists = 0
				FROM
					(select x.Col.value('.[1]','VARCHAR(20)') as ID
						from connectorResponse r
							CROSS APPLY sendXML.nodes('//retrieve/ids') x(Col)
						where r.id in (#connectorResponseIDList#)) as objectIds
					inner join vConnectorQueue q on q.objectID = objectIds.ID and q.object = <cf_queryparam value="#importObject#" cfsqltype="cf_sql_varchar"> and q.direction='I'
					left join #objectTable# o on o.ID = objectIds.ID
				where o.ID is null
			</cfquery>

			<!---<cfset structAppend(result.errorResult,insertErrorRecordsWhereNoDataReturnedResult)>--->

		<cfelseif isDefined("threadResult")>
			<cfset structAppend(result,threadResult,true)>
		</cfif>

		<cfset log_(label="Get Data For Object IDs",logStruct=result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="getDeletedObjects" access="private" output="false" returnType="struct" hint="Gets the deleted objects from SalesForce">
		<cfargument name="startDateTimeUTC" type="date" required="true">
		<cfargument name="endDateTimeUTC" type="date" required="true">
		<cfargument name="object" type="string" default="#this.mappedObject#">


		<cfset var result = {isOk=true,message="",successResult={tablename=""}}>

		<!--- getting the deleted objects from salesForce. An error is thrown if there is not a minute between the start and end date, so I add a minute to the enddate just to ensure that we don't have this problem'
			We throw an error because we don't want to continue. This function is very critical for things like merges.
			TODO: handle a 500 response from this function call.
		--->
		<!--- Salesforce currently falls over with a 500 response if the start date of the getDeleted call is greater than 30 days ago, so here we're just getting everything up until 30 days ago. Not sure whether this

			is a good solution, but it keeps the connector running. In theory, we should never get here as the connector will continually be running

			NJH 2016/02/15 - if object is not replicateable, then we can't call the getDeleted method against it
			--->
		<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(object=arguments.object)>

		<cfif application.connector.objectMetaData[arguments.object].replicateable>
			<cfset var startDateTime = arguments.startDateTimeUTC>
			<cfif dateDiff("d",startDateTime,request.requestTime) gt 29>
				<cfset startDateTime = dateAdd("d",-29,request.requestTime)>
			</cfif>
			<cfset structAppend(result,this.api.send(method="getDeleted",object=arguments.object,startDate=startDateTime,endDate=dateAdd("n",1,arguments.endDateTimeUTC),throwErrorOn500Response=true))>

			<cfif result.successResult.tablename neq "">
				<!--- for some reason, and I'm not sure if it is because we have to have at least a minute in between, but we are picking up deleted records that we have already processed.
					Because this happens, we don't process them (as the tie has been broken), and they remain in the queue. So, we filter them out here.
					TODO: look at why we are getting these records!!!
				 --->

				<cfset removeIncomingOrphanDeletionsFromDataTable()>
				<!--- <cfset var removeProcessedDeletedRecords = "">
				<cfquery name="removeProcessedDeletedRecords">
					delete #result.successResult.tablename#
					from #result.successResult.tablename# t
						left join #this.baseEntity# e on e.#this.baseEntityObjectKey# = t.#this.objectPrimaryKey#
					where e.#this.baseEntityPrimaryKey# is null
				</cfquery> --->
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="deleteNonPrimaryRecordsForPrimaryObject"  access="private" output="false" returnType="struct" hint="Deletes partners and contacts from SalesForce for objects that have just been modified">
		<cfargument name="object" type="string" required="true">
		<cfargument name="primaryObjectID" type="string" required="true">
		<!--- <cfargument name="roleList" type="string" required="false"> --->
		<cfargument name="filter" type="string" required="true" hint="The filter used to determine what records to delete">


		<cfset var result = {isOK=true,message=""}>
		<!---<cfset var theObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var roles = "">

		<cfif structKeyExists(arguments,"roleList") and arguments.roleList neq "">
			<cfset roles = listQualify(arguments.roleList,"'")>
		<cfelse>
			<cfset var getDistinctMappedRoles = "">

			<cfquery name="getDistinctMappedRoles">
				select distinct cvm.[value_remote] as role from connectorColumnValueMapping cvm
					inner join vConnectorMapping m on m.ID = cvm.connectorMappingID
				where cvm.value_relayware='role'
					and m.object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
					and export=1
			</cfquery>
			<cfset roles = quotedValueList(getDistinctMappedRoles.role)>
		</cfif> --->

		<!--- <cfif listLen(roles)> --->
		<cfif arguments.filter neq "">
			<cfset var objectFilter = arguments.filter>

			<cfif left(objectFilter,4) neq "and ">
				<cfset objectFilter = "and "&objectFilter>
			</cfif>

			<cfset var parentKeyField = getParentKeyFieldForObject(object=arguments.object)>
			<cfset var getObjectIDsToDelete = this.api.send(method="query",object=arguments.object,queryString="select ID,#parentKeyField# from #arguments.object# where #parentKeyField# = '#arguments.primaryObjectID#' #preserveSingleQuotes(objectFilter)#")>

			<cfset log_(label="Export (delete): Get Object IDs To Delete for #arguments.object# (#arguments.primaryObjectID#)",logStruct=getObjectIDsToDelete)>

			<cfif getObjectIDsToDelete.successResult.recordCount>
				<cfset var getIdsToDelete = "">

				<cfquery name="getIdsToDelete">
					select distinct ID from #getObjectIDsToDelete.successResult.tablename#
				</cfquery>

				<cfset var deleteDataResult = this.api.send(method="delete",object=arguments.object,ids=listToArray(valueList(getIdsToDelete.ID)))>

				<cfset log_(label="Export (delete): Delete Data Result",logStruct=deleteDataResult)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="convertDateToSalesForceFormat" access="public" returnType="String" output="false">
		<cfargument name="dateToConvert" required="true" type="date">
		<cfargument name="inUTC" default="true" type="boolean">

		<cfreturn "#dateFormat(arguments.dateToConvert,'yyyy-MM-dd')#T#timeFormat(arguments.dateToConvert,'HH:mm:ss')##IIF(arguments.inUTC,DE('Z'),DE(''))#">
	</cffunction>


	<cffunction name="setObjectRelaywareIDs" access="private" output="false" returnType="struct" hint="Sets the Relayware IDs on the remote system. This is just a stub to call the connector specific method.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records.">
		<cfargument name="columnList" type="string" required="true" hint="The names of the columns that we will be updating. Will be the Salesforce ID and the Relayware ID.">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#" hint="The meta data of the tablename table. Contains columnList and recordCount">
		<cfargument name="relaywareIDField" type="string" required="true" hint="The name of the field containing the RelaywareID.">

		<cfset var rowsPerRun = 200>
		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure(returnErrorTable=true)>

		<cfset var loopCount = int(arguments.tableMetaData.recordCount/rowsPerRun)>
		<cfset var rowNum = 0>
		<cfset var updateRemoteIdsResult = {}>
		<cfset var collateResponses = "">

		<cfloop from=0 to=#loopCount# index="rowNum">
			<cfset updateRemoteIdsResult = this.api.send(method="update",object=this.object,viewName=arguments.tableName,columnList=arguments.columnList,whereClause="rowNumber > #rowNum*rowsPerRun# and rowNumber <= #((rowNum+1)*rowsPerRun)#")>

			<!--- collate the responses --->
			<cfif updateRemoteIdsResult.isOK>
				<!--- put errored records into the error table --->
				<cfif updateRemoteIdsResult.result.recordCount>

					<cfquery name="collateResponses">
						insert into <cf_queryObjectName value="#result.errorResult.tablename#"> (object,objectId,message,field,value,statusCode,connectorResponseID)
						select distinct
							<cf_queryparam value="#this.object#" cfsqltype="cf_sql_varchar">,
							t.#this.objectPrimaryKey#,
							r.message,
							<cf_queryparam value="#arguments.relaywareIDField#" cfsqltype="cf_sql_varchar">,
							t.<cf_queryObjectName value="#arguments.relaywareIDField#">,
							r.statusCode,
							r.connectorResponseID
						from <cf_queryObjectName value="#updateRemoteIdsResult.result.tablename#"> r
							inner join <cf_queryObjectName value="#arguments.tableName#"> t on r.rowNumber + (#rowsPerRun#*#loopCount#) = t.rowNumber <!--- can't join on ID, as the Id doesn't exist in the result set --->
						where success = 0
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>

		<cfset var getErroredRecords = "">
		<cfset var getErroredRecordsResult = {}>

		<cfquery name="getErroredRecords" result="getErroredRecordsResult">
			select * from #result.errorResult.tablename#
		</cfquery>

		<cfset structAppend(result.errorResult,getErroredRecordsResult)>

		<cfreturn result>
	</cffunction>


	<cffunction name="escapeSpecialCharactersInSoql" access="private" hint="Escapes special characaters in SOQL query string" output="false" returnType="string">
		<cfargument name="strToEscape" type="string" required="true" hint="String to be escaped">

		<cfreturn reReplace(strToEscape,"([""'\\])","\\\1","ALL")>
	</cffunction>


	<cffunction name="getRetrieveInfoForNonPrimaryObjects" access="private" hint="Returns some info to be passed into the retreive function" output="false" returnType="struct">
		<cfargument name="object" type="string" required="true" hint="The non-primary object. Generally an opportunityContactRole or opportunityPartner">
		<cfargument name="primaryObjectIDs" type="array" required="true" hint="The IDs of the primary object. Generally opportunity Ids">

		<cfset var result = {idsToImport = arrayNew(1),objectFieldList = ""}>
		<cfset var mappedObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var primaryObjectIDList = listQualify(arrayToList(arguments.primaryObjectIDs),"'")>
		<cfset var parentKeyField = getParentKeyFieldForObject(object=arguments.object)>
		<cfset result.objectFieldList = listAppend(mappedObject.fieldColumnList,"lastModifiedDate,#parentKeyField#,ID")>
        <!--- Case 451138 - populate where clause for the non-primary objects --->
        <cfset var objectWhereClause = getObjectWhereClauseForObjectImport(arguments.object)>
        <cfif objectWhereClause NEQ "">
            <cfset objectWhereClause &= " AND ">
        </cfif>
		<cfset var baseQueryString = "SELECT ID from #arguments.object# where #objectWhereClause# #parentKeyField# in ">

        <cfset var roleQueryString = "">
		<cfset var queryString = "">
		<cfset var resultTableName = getTempTableName()>
		<cfset var collateResults = "">

		<!--- dealing with the objects that have a role - again, primarily the opportunityContactRole and Partner objects --->
        <!--- START: 2015-12-14 DAN Case 446344 --->
        <cfset var whereClause = getWhereClauseForNonPrimaryObject(object=arguments.object, fields="role,IsPrimary")>
        <cfif whereClause.fieldList NEQ "">
            <cfset result.objectFieldList = listAppend(result.objectFieldList, whereClause.fieldList)>
        </cfif>
        <cfif whereClause.value NEQ "">
            <cfset roleQueryString = "and " & whereClause.value>
        </cfif>
        <!--- END: 2015-12-14 DAN Case 446344 --->

		<cfset var createResultTable = "">
		<cfquery name="createResultTable">
			select cast(null as varchar) as ID into #resultTableName# where 1=0
		</cfquery>

		<!--- not using query here as that will truncate the data in the base table....
			Need to check the length of the queryString... Salesforce throws an error if queryString is longer than 20000 characters. So, need to do several iterations if primaryObjectIDList is quite long
		--->
		<cfset var idx = 0>
		<cfset var primaryObjectIDListForQuery = "">
		<cfloop from=1 to=#listLen(primaryObjectIDList)# index="idx">

			<cfset primaryObjectIDListForQuery = listAppend(primaryObjectIDListForQuery,listGetAt(primaryObjectIDList,idx))>

			<!--- NJH 2015/05/08 if we've got a 900 Ids or if we've hit the end of the list, run a query --->
			<cfif listLen(primaryObjectIDListForQuery) eq 900 or idx eq listLen(primaryObjectIDList)>
				<cfif primaryObjectIDListForQuery neq "">
					<cfset queryString = baseQueryString & "(#preserveSingleQuotes(primaryObjectIDListForQuery)#) " & roleQueryString>

					<cfset var modifiedDataResult = this.api.send(object=arguments.object,method="query",queryString=preserveSingleQuotes(queryString),throwErrorOn500Response=true)>

					<cfset primaryObjectIDListForQuery = "">

					<cfif modifiedDataResult.successResult.tablename neq "">

						<cfquery name="collateResults">
							insert into #resultTableName# (ID)
							select ID from #modifiedDataResult.successResult.tablename#
						</cfquery>

					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<cfset var getIDs = "">
		<cfset var getIDsResult = {}>

		<cfquery name="getIDs" result="getIDsResult">
			select ID from #resultTableName#
		</cfquery>

		<cfset result.idsToImport = listToArray(valueList(getIDs.ID))>

		<cfset log_(label="Non Primary Object IDs",logStruct=getIDsResult)>

		<cfreturn result>
	</cffunction>


	<cffunction name="getExportQueryInfo" access="private" returnType="struct" output="false" hint="Returns some sql for export">
		<cfargument name="method" type="string" required="true">
		<cfargument name="errorTableName" type="string" default="">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The name of the remote object to be exported">

		<cfset var result = {whereClause="",join="",selectColumnList=""}>

		<!--- this will be true for primary objects --->
		<cfif application.getObject("connector.com.connector").isObjectMapped(object_remote=arguments.object,connectorType=this.connectorType)>
			<cfset result = super.getExportQueryInfo(method=arguments.method,object=arguments.object)>
		<cfelse>
			<cfset var exportQueryInfo = getExportQueryInfoForNonPrimaryObjects(object=arguments.object)>
			<cfset var result.selectColumnList = exportQueryInfo.selectColumnList>
			<cfset var objectIDColumn = exportQueryInfo.parentKeyField>
			<cfset var relaywareIDField = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)>

			<!--- opportunityContactRoles and opportunityPartners are not updated. They are deleted/created. Order of the method list is important
				It may also be that we only want to import these records. We can't tell by looking at the object whether it is an import only
				as it's not a one to one mapping. So, we have to look at the exportCreateColumnList value. If it's empty, we assume it's an import only and so we don't export.
			--->

			<cfsavecontent variable="result.whereClause">
				<cfoutput>
					<cfif arguments.method eq "update"> <!--- we don't update partners or contact roles. They are a delete and create--->
						1=0
					<cfelseif getConnectorObject(object=arguments.object,relayware=false).exportCreateColumnList eq ""> <!--- see comments above; if no columns, then don't create --->
						1=0
					<cfelseif arguments.method eq "create">
						isDeleted = 0
					<cfelse><!--- get everything on a delete, as we delete all roles initially that we're interested in, and then the create creates the ones that need recreating --->
						1=1
					</cfif>
					and v.#objectIDColumn# is not null
					and err.objectId is null
				</cfoutput>
			</cfsavecontent>

			<cfif arguments.errorTableName neq "">
				<cfset result.join = "left join #arguments.errorTableName# err on err.objectID = v.#relaywareIDField#">
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="runPreOpportunityExport" access="private" output="false" returnType="struct" hint="Runs some functions before exporting opportunities">

		<cfset var result = {isOK=true,message=""}>
		<cfset var opportunityRoleObject = "">
		<cfset var objectsToExport = getConnectorObject(object="opportunity",relayware=false).objectList>

		<!--- putting any dependencies from the non-primary object views in, so that the primary view has any records with dependencies removed --->
		<cfset var opportunityRoleObjects = listDeleteAt(objectsToExport,listFindNoCase(objectsToExport,"opportunity"))>
		<cfloop list="#opportunityRoleObjects#" index="opportunityRoleObject">
			<cfset putMissingDependanciesIntoQueue(viewName=getViewForObjectSynch(object=opportunityRoleObject))>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="runPreOpportunityUpdate" access="private" output="false" hint="Runs some pre-update function for opportunities">
		<cfargument name="successTableName" type="string" required="true" hint="The name of the table that holds the successful opportunities creates that have just occurred">

		<cfset var objectsToExport = getConnectorObject(object="opportunity",relayware=false).objectList>

		<cfif listLen(objectsToExport) gt 1>
			<!--- before processing the opportunity partners/contacts, we want to set the SF ID on the opportunity table so that we have a handle on the opportunity that the partners and contacts need to be assigned to --->
			<cfset setRemoteIDForNewlyExportedRecords(tablename=arguments.successTableName)>
		</cfif>
	</cffunction>


	<cffunction name="deleteOpportunityPartner" access="private" output="false" hint="Deletes opportunity partners">
		<cfargument name="exportTableName" type="string" required="true" hint="The name of the table that holds the records to be exported">

		<cfset deleteNonPrimaryObjectsForChangedPrimaryObjects(object="opportunityPartner",tablename=arguments.exportTableName)>
	</cffunction>


	<cffunction name="deleteOpportunityContactRole" access="private" output="false" hint="Deletes opportunity contact roles">
		<cfargument name="exportTableName" type="string" required="true" hint="The name of the table that holds the records to be exported">

		<cfset deleteNonPrimaryObjectsForChangedPrimaryObjects(object="OpportunityContactRole",tablename=arguments.exportTableName)>
	</cffunction>


	<cffunction name="collateResponseForCreateOpportunityPartner" access="private" output="false" hint="Puts the response of partner create in appropriate tables">
		<cfset collateSuccessResponsesForNonPrimaryObjectExport(argumentCollection=arguments)>
		<cfset collateErrorResponsesForNonPrimaryObjectExport(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="collateResponseForCreateOpportunityContactRole" access="private" output="false" hint="Puts the response of contact roles create in appropriate tables">
		<cfset collateSuccessResponsesForNonPrimaryObjectExport(argumentCollection=arguments)>
		<cfset collateErrorResponsesForNonPrimaryObjectExport(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getRelaywareIdsForNewlyCreatedRecords" access="private" output="false" hint="Gets the RelaywareIDs for records that have just been created on the remote CRM system" returnType="struct">

		<cfset var entityIDMappedField = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field>
		<cfset var utcTimeOffsetInSeconds = application.com.dateFunctions.getUtcSecondsOffset()>
		<cfset var getCurrentUTCTimeMinusOneMinuteInSF = "">

		<cfquery name="getCurrentUTCTimeMinusOneMinuteInSF">
			select dateadd(s,#utcTimeOffsetInSeconds+this.remoteServerTimeOffsetInSeconds-60#,getDate()) as sfStartTimeUTC
		</cfquery>

		<cfreturn this.api.send(method="query",object=this.object,queryString="select #this.objectPrimaryKey#, #entityIDMappedField# from #this.object# where lastModifiedByID = '#application.connector.api.userID#' and lastModifiedDate >= #convertDateToSalesForceFormat(dateToConvert=getCurrentUTCTimeMinusOneMinuteInSF.sfStartTimeUTC)#")>
	</cffunction>


	<cffunction name="runPostOpportunityPartnerExport" access="private" output="false" hint="Runs some post export functionality for opportunity partners">
		<cfargument name="successTableName" type="string" required="true" hint="The name of the table that holds the successful creates that have just occurred">
		<cfargument name="errorTableName" type="string" required="true" hint="The name of the table that holds the errors that have just occurred">

		<cfset runPostOpportunityObjectRolesExport(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="runPostOpportunityContactRoleExport" access="private" output="false" hint="Runs some post export functionality for opportunity contact roles">
		<cfargument name="successTableName" type="string" required="true" hint="The name of the table that holds the successful creates that have just occurred">
		<cfargument name="errorTableName" type="string" required="true" hint="The name of the table that holds the errors that have just occurred">

		<cfset runPostOpportunityObjectRolesExport(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="runPostOpportunityObjectRolesExport" access="private" output="false" hint="Runs some post export functionality for opportunity role objects">
		<cfargument name="successTableName" type="string" required="true" hint="The name of the table that holds the successful creates that have just occurred">
		<cfargument name="errorTableName" type="string" required="true" hint="The name of the table that holds the errors that have just occurred">

		<cfset var deletePrimaryObjectsFromSuccesses = "">

		<!--- delete any primary object records from the success table if the related objects have had problems in some way. --->
		<cfquery name="deletePrimaryObjectsFromSuccesses">
			delete #arguments.successTableName#
			from
				#arguments.successTableName# s inner join #arguments.errorTableName# e on s.#this.baseEntityPrimaryKey# = e.objectID
		</cfquery>
	</cffunction>


	<cffunction name="getParentKeyFieldForObject" access="private" output="false" hint="Returns the parent object's primary key" returnType="string">
		<cfargument name="object" type="string" required="true" hint="The name of a SF object">

		<cfset var parentKeyField = getConnectorObject(object=arguments.object,relayware=false).parentKeyField>
		<cfif parentKeyField eq "">
			<cfset parentKeyField = this.baseObject&"ID">
		</cfif>
		<cfreturn parentKeyField>
	</cffunction>
</cfcomponent>