<!---
  --- connectorDAO
  --- ------------
  ---
  --- Gets and sets Relayware data for the connector.
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   02/12/15
  --->
<cfcomponent hint="Gets and sets Relayware data for the connector." accessors="true" output="false" persistent="false">

	<cffunction name="init" access="public" output="false" returnType="void" hint="Inits the DAO object, which does some getting and setting for the connector">
		<cfargument name="baseEntity" type="string" required="true" hint="The name of the Relayware entity that is currently being exported.">
		<cfargument name="baseEntityObjectKey" type="string" required="true" hint="The name of the column holding the remote ID on the Relayware entity being exported.">
		<cfargument name="baseEntityPrimaryKey" type="string" required="true" hint="The primary key of the Relayware entity">
		<cfargument name="logging" type="struct" required="true" hint="A logging struct to be passed into the logging function.">
		<cfargument name="connectorType" type="string" required="true" hint="The name of the connector type">
		<cfargument name="object" type="string" required="true" hint="The name of the object that is currently being imported or exported.">
		<cfargument name="objectPrimaryKey" type="string" required="true" hint="The name of the primary key on the remote object.">
		<cfargument name="direction" type="string" required="true" hint="The synch direction.">

		<cfset this.baseEntity = arguments.baseEntity>
		<cfset this.baseEntityObjectKey = arguments.baseEntityObjectKey>
		<cfset this.baseEntityPrimaryKey = arguments.baseEntityPrimaryKey>
		<cfset this.objectPrimaryKey = arguments.objectPrimaryKey>
		<cfset this.logging = arguments.logging>
		<cfset this.connectorType = arguments.connectorType>
		<cfset this.object = arguments.object>
		<cfset this.direction = arguments.direction>
	</cffunction>


	<cffunction name="getLocationsToExportForHQChanges" access="public" returnType="struct" output="false" hint="Puts sibling locations into the queue where the HQ flag has changed on one of them.">
		<cfargument name="startDateTimeUTC" type="date" required="true">
		<cfargument name="endDateTimeUTC" type="date" required="true">
		<cfargument name="baseLocationTableName" type="string" required="true" hint="The tablename containing the base location record changes">

		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure()>
		<cfset result.successResult.tablename = "">

		<cfif this.baseEntity eq "location">

			<!--- if the HQ flag is a location based flag, then get all the locations belonging to the same organisation as the flagged HQ location, so that they (and their relationship to the new HQ) all go across. --->
			<!--- <cfset var locFlag =  getMappedField(object_relayware=this.baseEntity,column_remote="parentID").field> --->
			<cfset var locHQFlag = "HQ">

			<cfif application.getObject("connector.com.connectorUtilities").isHQFieldMapped() and application.com.flag.getFlagStructure(flagID=locHQFlag).entityType.tablename eq "organisation">
				<cfset var getLocationsResult = {}>
				<cfset var getLocationsWhereHqHasChanged = "">
				<cfset result.successResult.tablename = application.getObject("connector.com.connectorUtilities").getTempTableName()>

				<cfquery name="getLocationsWhereHqHasChanged" result="getLocationsResult">
					select distinct sibling.locationID, '#this.baseEntity#' as object,getDate() as lastModifiedDate,0 as isDeleted
						into #result.successResult.tablename#
					from location sibling
						inner join location l on sibling.organisationID = l.organisationID
						inner join <cf_queryObjectName value="#arguments.baseLocationTableName#"> t on t.objectID = l.locationID
						inner join vModRegister mr on mr.recordID = l.locationID
					where
						mr.modDate between dbo.convertUTCDateTimeToServerDateTime(<cf_queryparam value="#arguments.startDateTimeUTC#" cfsqltype="cf_sql_timeStamp" useCfVersion="false">) and dbo.convertUTCDateTimeToServerDateTime(<cf_queryparam value="#arguments.endDateTimeUTC#" cfsqltype="cf_sql_timeStamp">)
						and mr.flagID = <cf_queryparam value="#application.getObject("flag").getFlagStructure(flagID=locHQFlag).flagID#" cfsqltype="cf_sql_integer">
						and not exists (select 1 from  <cf_queryObjectName value="#arguments.baseLocationTableName#"> where objectID = sibling.locationID)

					select * from #result.successResult.tablename#
				</cfquery>

				<cfset application.getObject("connector.com.connectorUtilities").insertRecordsIntoQueue(tablename=result.successResult.tablename,tableMetaData=getLocationsResult,objectIDColumn="locationID",object="location",direction="Export")>
				<cfset structAppend(result,getLocationsResult)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


<!--- Sugar 449071 - made start/end time not required but also allowed a table containing the parent objectIds to be passed in. We should never have a situation where they are all empty Should perhaps default the start/end time or check
		to see that one or the other gets passed in. --->
	<cffunction name="getDataToExport" access="public" returnType="struct" hint="Collect records that have been modified so that they get picked up in an export" output="false">
		<cfargument name="startDateTimeUTC" type="date" required="false">
		<cfargument name="endDateTimeUTC" type="date" required="false">
		<cfargument name="parentTableName" type="string" required="false" hint="The name of the table containing the parentIDs that we want to get child objects for. Only get child objects that have not yet synched.">
		<cfargument name="entityType" type="string" default="#this.baseEntity#" hint="The Relayware entity to export">

		<cfscript>
			var getModifiedRWRecords = queryNew("entityType,entityID,organisationID,approved");
			var getRWRecordsResult = "";
			var approvalEntity = arguments.entityType;
			var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure(tempTable=true);
			var mappedObject = application.getObject("connector.com.connector").getConnectorObject(object=application.getObject("connector.com.connector").getMappedObject(object_relayware=arguments.entityType),relayware=false);
			var thisObject = application.getObject("connector.com.connector").getConnectorObject(object=arguments.entityType,relayware=true);
			var whereClause = thisObject.whereClause neq ""?"where (#thisObject.whereClause#)":"";
			var entityUniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey;
			var entityObjectKey = application.getObject("connector.com.connector").getMappedField(object_relayware=arguments.entityType,column_remote=this.objectPrimaryKey).field;
		</cfscript>

		<!--- TODO. WE need to possibly change this, but this is put in here for now to get Aerohive up and running. They are in a situation where they want to synch only approved locations/people but the approval flag is not mapped.
			Therefore, approving someone is not causing them to go across. The 'synch trigger points' used to handle this, and it may be that they get re-instated in some way. But for now, just adding the approval flag as part of the list
			of flags to watch --->
		<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
			<cfset var approvedFlagStruct = application.com.flag.getFlagStructure("#left(approvalEntity,3)#Approved")>
			<cfif approvedFlagStruct.isOk>
				<cfset thisObject.synchedFlagList = listAppend(thisObject.synchedFlagList,approvedFlagStruct.flagID)>
			</cfif>
		</cfif>

		<!--- NJH 2015/06/19 - put the dropping of the temp table into this query --->
		<cfquery name="getModifiedRWRecords" result="getRWRecordsResult">
			if object_id('tempdb..#result.successResult.tablename#') is not null
			begin
			   drop table #result.successResult.tablename#
			end

			select distinct max(modDate) as lastModifiedDate,'#arguments.entityType#' as object,cast(objectID as varchar(50)) as objectID,#this.baseEntity#Synching,objectID as #this.baseEntityPrimaryKey#,isDeleted
			into #result.successResult.tablename# from (
				select distinct
				1 as #arguments.entityType#Synching,
				#arguments.entityType#.objectID,
				dbo.convertServerDateTimeToUTCDateTime(mr.modDate) as modDate,
				<!--- dateadd(s,#timeOffsetInSeconds#,mr.modDate) as modDate, --->
				#arguments.entityType#.deleted as isDeleted
				from
					(
						select #entityUniqueKey# as objectID,
							#application.entityTypeID[arguments.entityType]# as baseEntityTypeID,
							#entityUniqueKey# as recordID,
							0 as deleted,
							#entityObjectKey# as crmID
						<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
							,<cfif arguments.entityType eq "organisation">organisationTypeID<cfelse>accountTypeID</cfif> as accountTypeID
						</cfif>
						<cfif thisObject.parentObject neq "">
							,#arguments.entityType#.#thisObject.parentObject#ID
						</cfif>
						from v#arguments.entityType# #arguments.entityType#
						<cfif arguments.entityType eq "person">
							inner join location on Person.locationID = location.locationID
						</cfif>
							#preserveSingleQuotes(whereClause)#
						<cfif mappedObject.allowDeletions>
						union
						select #entityUniqueKey# as objectID,
							#application.entityTypeID[arguments.entityType]# as baseEntityTypeID,
							#entityUniqueKey# as recordID,
							1 as deleted,
							#entityObjectKey# as crmID
							<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
								,<cfif arguments.entityType eq "organisation">organisationTypeID<cfelseif arguments.entityType eq "location">locationDel.accountTypeID<cfelse>isNull(locationDel.accountTypeID,location.accountTypeID)</cfif> as accountTypeID
							</cfif>
							<cfif thisObject.parentObject neq "">
								,#arguments.entityType#Del.#thisObject.parentObject#ID
							</cfif>
						from #arguments.entityType#Del
							<cfif arguments.entityType eq "person">
								left join location on personDel.locationID = location.locationID
								left join locationDel on personDel.locationID = locationDel.locationID
							</cfif>
							where #entityObjectKey# is not null <!--- only get deleted objects where we have a crmID. We can't really apply the where clause here as all flags are deleted when deleting a POL record. So, assume that anything that has a CRM is eligble. --->
						</cfif>

						<!--- need to get all locations for any changes to the HQ flag --->
						<cfif listFind(thisObject.synchedFlagList,application.com.flag.getFlagStructure(flagID='HQ').flagID) and arguments.entityType eq "location">
						union
						select location.locationID as objectID,
							#application.entityTypeID.organisation# as baseEntityTypeID,
							o.organisationID as recordID,
							0 AS deleted,
							location.crmLocID AS crmID,
							location.accountTypeID
							<cfif thisObject.parentObject neq "">
								,location.#thisObject.parentObject#ID
							</cfif>
						from organisation o inner join vlocation location on o.organisationID = location.organisationID
							#preserveSingleQuotes(whereClause)#
						</cfif>
					)
				 	as #arguments.entityType#
					inner join vmodRegister mr on (mr.entityTypeID = #arguments.entityType#.baseEntityTypeID and mr.recordID = #arguments.entityType#.recordID)

					<!--- if POL data, join to get the org/loc types as we want to filter out admin companies, for example --->
					<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
						inner join organisationType ot with (noLock) on ot.organisationTypeID = #arguments.entityType#.accountTypeID
					</cfif>

					<!--- get any dependent records that have not yet synched for parent records that have just synched. This solves problem where a parent record does not meet filter requirements initially, it then
						has dependent records created (but they don't synch because parent it not eligible), but then parent becomes eligible --->
					<cfif structKeyExists(arguments,"parentTableName") and arguments.parentTableName neq "">
						inner join #arguments.parentTableName# parentRecords on parentRecords.#thisObject.parentObject#ID = #arguments.entityType#.#thisObject.parentObject#ID and #arguments.entityType#.crmID is null
					</cfif>

					<cfif thisObject.parentObject neq "">
						left join v#thisObject.parentObject# as #thisObject.parentObject# on #thisObject.parentObject#.#thisObject.parentObject#ID = #arguments.entityType#.#thisObject.parentObject#ID
					</cfif>

					left join booleanFlagData synch with (noLock) on synch.entityID = mr.recordID and synch.flagID = <cf_queryparam value="#application.com.flag.getFlagStructure('#arguments.entityType#Suspended').flagID#" cfsqltype="cf_sql_integer">

					<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
					left join booleanFlagData #arguments.entityType#Deleted with (noLock) on #arguments.entityType#Deleted.entityID = mr.recordId and #arguments.entityType#Deleted.flagID = <cf_queryparam value="#application.com.flag.getFlagStructure('Delete#arguments.entityType#').flagID#" cfsqltype="cf_sql_integer">
					</cfif>
				where
					isNull(actionByCF, <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">) <>  <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">
					<cfif structKeyExists(arguments,"startDateTimeUTC") and structKeyExists(arguments,"endDateTimeUTC")>
					and mr.modDate between dbo.convertUTCDateTimeToServerDateTime(<cf_queryparam value="#arguments.startDateTimeUTC#" cfsqltype="cf_sql_timeStamp" useCfVersion="false">) and dbo.convertUTCDateTimeToServerDateTime(<cf_queryparam value="#arguments.endDateTimeUTC#" cfsqltype="cf_sql_timeStamp">)
					</cfif>
					and case when #arguments.entityType#.deleted = 1 then crmId else '1' end is not null <!---  if picking up deleted records, only process deletes where we have a crmID --->
					and (
						<!--- not sure if I want the hardcoded 'convertedOpportunityID' column here, but not sure where else to put it. Essentially, we also want to pick up leads that have been converted, although we're not exporting the actual value
							(and therefore not in the exportColumnList value)
							TODO: look at moving this into a function or something
						--->
						<cfif thisObject.exportColumnList neq "">
							mr.field in ('Whole Record',<cf_queryparam value="#thisObject.exportColumnList#" cfsqltype="cf_sql_varchar" list="true"> <cfif arguments.entityType eq "lead">,'convertedOpportunityID'</cfif>)
						<cfelse>
							1=0
						</cfif>
						<cfif thisObject.synchedFlagList neq "">
							or (mr.Action in ('FA','FD','FM') and mr.flagID in ( <cf_queryparam value="#thisObject.synchedFlagList#" cfsqltype="cf_sql_integer" list="true">))
						</cfif>
						<cfif mappedObject.allowDeletions>
							or (mr.action like '%d' and mr.flagId is null)
						</cfif>
						)
					and synch.entityID is null <!--- filter out suspended records --->
				<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
					and ot.typeTextID != 'AdminCompany'   <!--- don't synch admin companies or internal users --->
					<cfif not mappedObject.allowDeletions>
					and #arguments.entityType#Deleted.entityID is null
					</cfif>
				</cfif>
				<!--- filter to only parent records that are synching --->
				<cfif thisObject.parentObject neq "">
					<cfset var parentWhereClause = application.getObject("connector.com.connector").getConnectorObject(object=thisObject.parentObject,relayware=true).whereClause>
					<cfif parentWhereClause neq ""> and (#preserveSingleQuotes(parentWhereClause)#)</cfif>
				</cfif>
				) as base
			group by objectID,#arguments.entityType#Synching,isDeleted

			select * from #result.successResult.tablename#
		</cfquery>

		<cfset var modifiedRecordsQueryXML = "">
		<cfset var modifiedRecordsSqlXML = "">

		<!--- TODO: stick in the sql parameters --->
		<cfwddx action="cfml2wddx" input="#getModifiedRWRecords#" output="modifiedRecordsQueryXML">
		<cfwddx action="cfml2wddx" input="#application.com.regExp.removeWhiteSpace(leadingWhiteSpace=true,allLineBreaks=true,string=application.com.globalFunctions.getSQLTextFromQuery(queryObject=getModifiedRWRecords))#" output="modifiedRecordsSqlXML">

		<!--- log the response --->
		<cfset var connectorResponseID = application.getObject("connector.com.connector").logConnectorResponse(sendXML=modifiedRecordsSqlXML,responseXML=modifiedRecordsQueryXML,relaywareResponse=true,method="query",connectorObject=arguments.entityType)>

		<!--- add the connectorResponse ID to the query --->
		<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=result.successResult.tablename,columnName="connectorResponseID",defaultValue=connectorResponseID)>
		<cfset structAppend(result.successResult,getRWRecordsResult)>

		<!--- if we're dealing with locations, then we need to get all other locations where the HQ field may have changed. Ie. if one of these locations in the base query became the HQ, then we'll need to
			bring across all other locations where the HQ being exported is their HQ --->
		<cfif arguments.entityType eq "location">
			<cfset var siblingLocationResult = getLocationsToExportForHQChanges(argumentCollection=arguments,baseLocationTableName=result.successResult.tablename)>

			<cfif siblingLocationResult.successResult.tablename neq "">
				<cfset var collateResult = "">
				<cfset var collateRecordsResult = {}>
				<cfset var collateTablename = application.getObject("connector.com.connectorUtilities").getTempTableName()>

				<cfquery name="collateResult" result="collateRecordsResult">
					select * into #collateTablename# from (
						select locationID as objectID,object,lastModifiedDate,1 isDeleted, locationID,1 as #arguments.entityType#Synching
						from #siblingLocationResult.successResult.tablename#
						union
						select objectID,object,lastModifiedDate,isDeleted,locationID,#arguments.entityType#Synching
						from #result.successResult.tablename#
					) as base

					select * from #collateTablename#
				</cfquery>

				<cfset result.successResult = collateRecordsResult>
				<cfset result.successResult.tablename = collateTablename>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="matchAndImportRemoteUsers" access="public" output="false" returnType="struct" hint="Imports users which are currently not mapped. They are a special case. They will exist in the queue and will get picked up in this function.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the remote user data.">
		<cfargument name="userFieldStruct" type="struct" required="true" hint="A structure containing the names of the fields that would map to the Relayware equivalent fields.">

		<cfset var userOrgTable = application.getObject("connector.com.connectorUtilities").getTempTableName(tempTable=true)>
		<cfset var userLocTable = application.getObject("connector.com.connectorUtilities").getTempTableName(tempTable=true)>
		<cfset var userPerTable = application.getObject("connector.com.connectorUtilities").getTempTableName(tempTable=true)>
		<cfset var result = {isOK=true,message=""}>
		<cfset var matchUserOrganisations = "">
		<cfset var matchUserOrganisationsResult = {}>
		<cfset var matchUserLocations = "">
		<cfset var matchUserPersons = "">

		<!--- match/insert the users' organisations against admin organisation in Relayware --->
		<cfset var createOrganisationTable = "">

		<cfquery name="createOrganisationTable">
			if object_id('tempdb..#userOrgTable#') is not null
			begin
			   drop table #userOrgTable#
			end

			select distinct ltrim(rtrim(t.#arguments.userFieldStruct.company#)) as organisationName, isNull(c.countryID,ca.countryId) as countryID, ot.organisationTypeID, null as VatNumber, cast(null as varchar(250)) as matchName, cast(null as varchar(250)) as orgUrl, cast(null as integer) as organisationID
				into #userOrgTable#
			from #arguments.tablename# t
				inner join organisationType ot on ot.typeTextID = 'AdminCompany'
				left join countryAlternatives ca on ltrim(rtrim(t.#arguments.userFieldStruct.country#)) = ltrim(rtrim(ca.alternativeCountryDescription))
				left join country c on ltrim(rtrim(t.#arguments.userFieldStruct.country#)) in (c.r1CountryID, c.isoCode, c.countryDescription,c.localCountryDescription)
		</cfquery>

		<!--- update matchnames on incoming organisations and organisations that are in RW--->
		<!--- <cfset application.com.dbTools.updateMatchFields(tablename=userOrgTable,entityType="organisation")>
		<cfset application.com.dbTools.updateMatchFields(tablename="organisation",entityType="organisation")> --->
		<cfset application.com.matching.prepareDataForMatching(tablename=userOrgTable,entityType="organisation")>

		<cfquery name="matchUserOrganisations" result="matchUserOrganisationsResult">
			update #userOrgTable#
				set organisationID = o.organisationID
			from #userOrgTable# t
				inner join organisation o on t.matchName = o.matchName
				inner join organisationType ot on ot.organisationTypeID = o.organisationTypeID and ot.typeTextID = 'AdminCompany'
		</cfquery>
		<cfset matchUserOrganisationsResult.tablename = userOrgTable>
		<cfset  application.getObject("connector.com.connectorUtilities").log_(label="Matched Organisations User Data",logStruct=matchUserOrganisationsResult,debugDetails=this.logging)>

		<!--- match/insert users' locations against admin locations in Relayware--->
		<cfset var createLocationTable = "">

		<cfquery name="createLocationTable">
			if object_id('tempdb..#userLocTable#') is not null
			begin
			   drop table #userLocTable#
			end

			select distinct ltrim(rtrim(t.#arguments.userFieldStruct.company#)) as sitename, o.organisationID, o.countryID, ltrim(rtrim(t.#arguments.userFieldStruct.postalCode#)) as postalCode, ltrim(rtrim(t.#arguments.userFieldStruct.address1#)) as address1, ltrim(rtrim(t.#arguments.userFieldStruct.address4#)) as address4, cast(null as varchar(250)) as matchName, cast(null as integer) as locationID
				into #userLocTable#
			from
				#arguments.tablename# t
				inner join #userOrgTable# o on o.organisationName = t.#arguments.userFieldStruct.company#
		</cfquery>

		<!--- update matchnames on incoming locations and locations that are in RW--->
		<<!--- cfset application.com.dbTools.updateLocationMatchname(tablename = userLocTable)>
		<cfset application.com.dbTools.updateLocationMatchname()> --->
		<cfset application.com.matching.prepareDataForMatching(tablename=userLocTable,entityType="location")>
		<cfset var matchUserLocationsResult = {}>

		<cfquery name="matchUserLocations" result="matchUserLocationsResult">
			update #userLocTable#
				set locationID = l.locationID
			from #userLocTable# t
				inner join location l on l.matchName = t.matchName
				inner join organisation o on o.organisationID = l.organisationID
				inner join organisationType ot on o.organisationTypeID = ot.organisationTypeID and ot.typeTextID = 'AdminCompany'
		</cfquery>

		<cfset matchUserLocationsResult.tablename=userLocTable>
		<cfset  application.getObject("connector.com.connectorUtilities").log_(label="Matched Locations User Data",logStruct=matchUserLocationsResult,debugDetails=this.logging)>

		<cfset var clientOrganisationID = application.com.settings.getSetting("theClient.clientOrganisationID")>
		<cfset var defaultAdminLocation = application.com.flag.getFlagData(entityID=clientOrganisationID,flagID="HQ").data[1]>

		<!--- if an HQ has not been set for this (the client) organisation, then we grab a 'random' location. We don't want it to be too random as otherwise users go in all various sorts of locations. Ideally, they need to go in one
			So, we grab the oldest location as that won't change over time, unless it gets deleted.

			NJH 2015/09/17 Modified the query slightly to include the admin location. Had a scenario where the admin location didn't actually exist, so the person was never getting created. Now, choose it if it exists or, failing that, then get the older location.
		 --->
		<cfset var getHQorOldestLocationAtOrganisation = "">

		<cfquery name="getHQorOldestLocationAtOrganisation">
			select top 1 locationID
			from location
			where organisationID = <cf_queryparam value="#clientOrganisationID#" cfsqltype="cf_sql_integer">
				<cfif defaultAdminLocation neq "">or locationId = <cf_queryparam value="#defaultAdminLocation#" cfsqltype="cf_sql_integer"></cfif>
			order by
			<cfif defaultAdminLocation neq "">case when locationId = <cf_queryparam value="#defaultAdminLocation#" cfsqltype="cf_sql_integer"> then 0 else 1 end,</cfif> created
		</cfquery>

		<cfset defaultAdminLocation = getHQorOldestLocationAtOrganisation.locationID[1]>

		<!--- match/insert persons
			NJH 2016/09/14 - JIRA PROD2016-2340 wrap fields on joins with isNull.
		 --->
		<cfset var createPersonTable = "">
		<cfquery name="createPersonTable">
			if object_id('tempdb..#userPerTable#') is not null
			begin
			   drop table #userPerTable#
			end

			select distinct null as personID, ltrim(rtrim(t.#arguments.userFieldStruct.id#)) as crmPerID,ltrim(rtrim(t.#arguments.userFieldStruct.firstname#)) as firstname, ltrim(rtrim(t.#arguments.userFieldStruct.lastname#)) as lastname, ltrim(rtrim(t.#arguments.userFieldStruct.phone#)) as OfficePhone, ltrim(rtrim(t.#arguments.userFieldStruct.email#)) as email, isNull(l.locationID,<cf_queryparam value="#defaultAdminLocation#" cfsqltype="cf_sql_integer">) as locationID,
				case when l.locationID is null then <cf_queryparam value="#clientOrganisationID#" cfsqltype="cf_sql_integer"> else l.organisationID end as organisationID, 1 as isAccountManager
				into #userPerTable#
			from
				#arguments.tablename# t
				inner join #userLocTable# l on isNull(l.sitename,'') = isNull(t.#arguments.userFieldStruct.company#,'') and isNull(l.address1,'') = isNull(t.#arguments.userFieldStruct.address1#,'') and isNull(l.address4,'') = isNull(t.#arguments.userFieldStruct.address4#,'') and isNull(l.postalCode,'') = isNull(t.#arguments.userFieldStruct.postalCode#,'')
		</cfquery>

		<cfset var matchUserPersonsResult = "">
		<cfquery name="matchUserPersons" result="matchUserPersonsResult">
			update #userPerTable#
				set personID = p.personID
			from #userPerTable# t
				inner join person p on ltrim(rtrim(p.firstname)) =  isNull(t.firstname,'') and ltrim(rtrim(p.lastname)) = isNull(t.lastname,'') and ltrim(rtrim(p.email)) =  isNull(t.email,'') and p.locationID = t.locationID
		</cfquery>

		<cfset matchUserPersonsResult.tablename = userPerTable>
		<cfset  application.getObject("connector.com.connectorUtilities").log_(label="Matched Person User Data",logStruct=matchUserPersonsResult,debugDetails=this.logging)>

		<cfset var perUpsertResult = upsertEntity(tablename=userPerTable,entityType="person")>
		<cfset application.com.relayUserGroup.addPersonToUserGroup(userGroup="#this.connectorType#ProxyUser",tablename=perUpsertResult.successResult.tablename)>

		<cfreturn result>
	</cffunction>


	<cffunction name="upsertEntity" access="public" output="false" hint="Upserts records in relayware" returnType="struct">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" default="#this.baseEntity#">
		<cfargument name="validate" type="boolean" default="false" hint="Run query in validate mode. Does not do an actual upsert">
		<cfargument name="processNulls" type="string" default="true" hint="Do not do anything with null columns. They are ignored.">
		<cfargument name="whereClause" type="string" default="1 = 1" hint="Filter out which records to validate or upsert">
		<cfargument name="truncate" type="boolean" default="#application.com.settings.getSetting('connector.allowDataTruncation')#" hint="Truncate any data that is too long.">
		<cfargument name="uniqueKeyColumn" type="string" default="upsertUniqueID" hint="The name of the column that can uniquely identity each row to be upserted.">
		<cfargument name="skipValidation" type="boolean" default="true" hint="When running in non-validate mode, whether we want to run the insert with a pre-validation. But because we have already validated, skip this when doing actual upsert">
		<cfargument name="columns_Updated" type="string" required="false" hint="The columns to process. All columns are processed by default.">
		<cfargument name="orderBy" type="string" default="" hint="The order in which to process the records">

		<cfset var errorTableName = "">
		<cfset var entityUpsertResultTablename = "">
		<cfset var successTablename = application.getObject("connector.com.connectorUtilities").getTempTableName()>
		<cfset var argName = "">

		<!--- if we are in validate mode, then any results returned from the SP should get logged in the errors table to be dealt with as errors
			If we are not in validate mode, we create the table so that it always exists. If in validate mode, the SP creates the table for us
		 --->
		<cfif not arguments.validate>
			<cfset errorTableName = application.getObject("connector.com.connectorUtilities").createErrorTable().tablename>
			<cfset entityUpsertResultTablename = successTablename>
		<cfelse>
			<cfset errorTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
			<cfset entityUpsertResultTablename = errorTableName>
		</cfif>

		<cfset var errorResult = {tablename=errorTableName,recordCount=0,columnList="objectID,object,message,field,value"}>
		<cfset var successResult = {tablename=successTablename,recordCount=0,columnList=""}>
		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure()>
		<cfset result.errorResult = errorResult>
		<cfset result.successResult=successResult>
		<cfset var recordWhereClause = "">
		<cfset var upsertRWRecords = "">
		<cfset var getRecordIdsToBeUpserted = "">
		<cfset var insertErrorRow = "">
		<cfset var uniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey>
		<cfset var returnTableExists = false>

		<!--- prepare the processNulls and truncate arguments for the entityUpsert sp--->
		<cfloop list="processNulls,truncate,columns_Updated" index="argName">
			<cfif isBoolean(arguments[argName])>
				<cfset arguments[argName]=arguments[argName]?-1:0>
			<cfelse>
				<cfset arguments[argName] = arguments[argName] neq ""?"dbo.columnListToBitmask('#arguments.tablename#','#arguments[argName]#')":-1>
			</cfif>
		</cfloop>

		<cfset application.getObject("connector.com.connectorUtilities").log_(label="Begin upsertEntity",logStruct=arguments,debugDetails=this.logging)>

		<cftry>

			<cfset var entityUpsertResult = "">

			<cfquery name="upsertRWRecords" result="entityUpsertResult">
				declare @processNullsVar varbinary(100)
				declare @truncateVar varbinary(100)
				declare @columns_Updated varbinary(100)

				select @processNullsVar = #preserveSingleQuotes(arguments.processNulls)#
				select @truncateVar = #preserveSingleQuotes(arguments.truncate)#
				select @columns_Updated = #preserveSingleQuotes(arguments.columns_Updated)#

				exec entityUpsert<cfif not arguments.validate>_withErrorHandling
					@skipValidation = <cf_queryparam value="#arguments.skipValidation#" cfsqltype="cf_sql_bit">,
					@orderBy = <cf_queryparam value="#arguments.orderBy#" cfsqltype="cf_sql_varchar">,
					@maxIterations = 30,
					<cfelse>
					@validate = <cf_queryparam value="#arguments.validate#" cfsqltype="cf_sql_bit">,</cfif>
					@upsertTableName = <cf_queryparam value="#arguments.tablename#" cfsqltype="cf_sql_varchar">,
					@columns_Updated  = @columns_Updated, /* - 1 will update all */
					@entityName = <cf_queryparam value="#arguments.entityType#" cfsqltype="cf_sql_varchar">,
					@lastUpdatedByPerson = #request.relayCurrentUser.personID#,
					@processNulls = @processNullsVar,
					@truncate = @truncateVar,
					@whereClause = <cf_queryparam value="#arguments.whereClause#" cfsqltype="cf_sql_varchar">,
					@returnTableName = <cf_queryparam value="#entityUpsertResultTablename#" cfsqltype="cf_sql_varchar">
			</cfquery>

			<cfif this.logging.debugLevel gt 1>
				<cfset application.getObject("connector.com.connectorUtilities").log_(label="upsertEntity result",logStruct=entityUpsertResult,debugDetails=this.logging)>
			</cfif>

			<!--- if we are in validate mode, then any results returned from the SP should get logged in the errors table to be dealt with as errors --->
			<cfif not arguments.validate>
				<cfset structAppend(result.successResult,entityUpsertResult)>
			<cfelse>
				<cfset structAppend(result.errorResult,entityUpsertResult)>
			</cfif>

			<cfset returnTableExists = true>

			<cfcatch>

				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector Upsert Entity",Severity="error",catch=cfcatch,WarningStructure=arguments)>

				<!--- if there is a problem doing a bulk insert, then re-try, doing a record at a time. Use the where clause to filter the records --->
				<cfquery name="getRecordIdsToBeUpserted">
					select #arguments.uniqueKeyColumn# as ID from #arguments.tablename#
				</cfquery>

				<cfset var colList = "action,upsertUniqueID,entityID">
				<cfif entityUpsertResultTablename eq errorResult.tablename>
					<cfset colList = "upsertUniqueID,entityID,message,field,value,action">
				</cfif>
				<cfset returnTableExists = false>

				<cfloop query="getRecordIdsToBeUpserted">
					<cfset recordWhereClause = arguments.whereClause & " and " & arguments.uniqueKeyColumn & " = '"&getRecordIdsToBeUpserted.ID[currentRow]&"'">

					<cftry>
						<cfset var entityUpsertQry = "">

						<cfquery name="entityUpsertQry" result="entityUpsertResult">
							exec entityUpsert<cfif not arguments.validate>_withErrorHandling
								@skipValidation = <cf_queryparam value="#arguments.skipValidation#" cfsqltype="cf_sql_bit">,
								<cfelse>
								@validate = <cf_queryparam value="#arguments.validate#" cfsqltype="cf_sql_bit">,</cfif>
								@upsertTableName = <cf_queryparam value="#arguments.tablename#" cfsqltype="cf_sql_varchar">,
								@columns_Updated  = -1, /* - 1 will update all */
								@entityName = <cf_queryparam value="#arguments.entityType#" cfsqltype="cf_sql_varchar">,
								@lastUpdatedByPerson = #request.relayCurrentUser.personID#,
								@processNulls = #arguments.processNulls#<!--- <cf_queryparam value="#arguments.processNulls#" cfsqltype="cf_sql_varbinary"> --->,
								@truncate = #arguments.truncate#<!--- <cf_queryparam value="#arguments.truncate#" cfsqltype="cf_sql_varbinary"> --->,
								@whereClause = <cf_queryparam value="#recordWhereClause#" cfsqltype="cf_sql_varchar">
								<cfif not returnTableExists>
								,@returnTableName = <cf_queryparam value="#entityUpsertResultTablename#" cfsqltype="cf_sql_varchar">
								</cfif>
						</cfquery>

						<!--- if the table doesn't exist, then the @returnTableName variable is set, meaning we don't get a result set. So, here
							we get the recordset from the temp table --->
						<cfif not returnTableExists>
							<cfquery name="entityUpsertQry">
								select * from #entityUpsertResultTablename#
							</cfquery>
						</cfif>

						<cfset var loopCount = 0>
						<cfset var colListLen = listLen(colList)>
						<cfset returnTableExists = true>
						<cfset var colname = "">
						<cfset var addProcessedRow = "">

						<!--- successful records  --->
						<cfquery name="addProcessedRow">
							if not exists (select 1 from #entityUpsertResultTablename# where #arguments.uniqueKeyColumn#  = '#getRecordIdsToBeUpserted.ID[currentRow]#')
							insert into #entityUpsertResultTablename# (#colList#) values
								(<cfloop list="#colList#" index="colname"><cfset loopCount++><cf_queryparam value="#entityUpsertQry[colname][1]#" cfsqltype="cf_sql_varchar"><cfif loopCount lt colListLen>,</cfif></cfloop>)
						</cfquery>

						<cfcatch>
							<cfset result.isOk = false>

							<cfset var errorObject = this.object>
							<cfif arguments.uniqueKeyColumn eq uniqueKey>
								<cfset errorObject = arguments.entityType>
							</cfif>

							<!--- TODO: get the record details and store them? --->
							<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector Upsert Entity for #errorObject# with ID #getRecordIdsToBeUpserted.ID[currentRow]#",Severity="error",catch=cfcatch,WarningStructure=arguments)>

							<!--- errored records  --->
							<cfquery name="insertErrorRow">
								insert into #result.errorResult.tablename# (objectID,object,message,field,value) values ('#getRecordIdsToBeUpserted.ID[currentRow]#','#errorObject#','#cfcatch.detail#',null,null)
							</cfquery>
						</cfcatch>
					</cftry>
				</cfloop>
			</cfcatch>
		</cftry>

		<cfif returnTableExists>

			<cfset var addUniqueKeyColumn = "">

			<!--- if we have an entityID column, then change it to the uniqueKey of the entity. This is so that we can use it in later queries, as we will be expecting a column name of
					personId, locationId, etc rather than entityID --->

			<cfquery name="addUniqueKeyColumn" result="successResult">
				<!--- doing a loop here, because temp tables can now be standard tables as well, due to a bug fix where we were losing temp tables due to the connection being reset --->
				<cfloop list="0,1" index="tempdb">
				if exists (select 1 from <cfif tempdb>tempdb.</cfif>information_schema.columns where table_name='#entityUpsertResultTablename#' and column_name='entityID')
					and not exists (select 1 from <cfif tempdb>tempdb.</cfif>information_schema.columns where table_name='#entityUpsertResultTablename#' and column_name='#uniqueKey#')
				exec <cfif tempdb>tempdb..</cfif>sp_RENAME '#entityUpsertResultTablename#.[entityID]' , '#uniqueKey#', 'COLUMN'
				</cfloop>

				select * from #entityUpsertResultTablename#
			</cfquery>

			<cfset structAppend(result.successResult,successResult,true)>

			<!--- adding the object and objectID columns to the result, as these are the columns used in the joins when putting records in the (error) queue  --->
			<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=entityUpsertResultTablename,columnName="objectID",defaultValueFromColumn=arguments.uniqueKeyColumn)>
			<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=entityUpsertResultTablename,columnName="object",defaultValue=arguments.uniqueKeyColumn eq uniqueKey?arguments.entityType:this.object)>
		</cfif>

		<cfset var getErrorRecords = "">

		<cfif this.logging.debugLevel eq 3>
			<cfset getUpsertResult = "">
			<cfset upsertEntityResult = {}>

			<cfquery name="getUpsertResult" result="upsertEntityResult">
				select * from #entityUpsertResultTablename#
			</cfquery>

			<cfset upsertEntityResult.tablename=entityUpsertResultTablename>

			<cfset application.getObject("connector.com.connectorUtilities").log_(Label="UpsertEntity Table Result",logStruct=upsertEntityResult,debugDetails=this.logging)>
		</cfif>

		<!--- split out the errored records into the error result and remove them from the successes if we're running in 'upsert' mode, rather than validate mode
			We've introduced an improvement to errorhandling to say that if it encounters 30 (?) errors, we abort the transaction. This is to prevent upsertEntity from running too long if a lot of errors are encountered.
			The records that errored would get marked as errored, and would be processed last in the next iteration as the view orders the queue items by whether they have errored or not. So, we now remove any records from the success table
			that have not been processed on this round.
		--->
		<cfquery name="getErrorRecords" result="errorResult">
			<cfif not arguments.validate>
			insert into #result.errorResult.tablename# (objectID,object,message,field,value)
			select objectID,object,message,field,value
			from #entityUpsertResultTablename#
			where action like 'Failed%'

			delete from #entityUpsertResultTablename# where action like 'Failed%' or action = 'NotProcessed'
			</cfif>

			select * from #result.errorResult.tablename#
		</cfquery>

		<cfset structAppend(result.errorResult,errorResult)>

		<cfset application.getObject("connector.com.connectorUtilities").log_(logStruct=result,debugDetails=this.logging)>

		<cfreturn result>
	</cffunction>


	<cffunction name="setSynchStatusFlag" access="public" output="false" hint="Sets the synch status flag for a batch of records">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="status" type="string" required="true" hint="Suspended,Synching,Exported,Imported">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#">

		<cfset var tempTableName = application.getObject("connector.com.connectorUtilities").getTempTableName()>
		<cfset var setSynchStatusQry = "">
		<cfset var useRemoteKey = false>
		<cfset var result = {isOK=true,message=""}>

		<cfif this.logging.debugLevel gt 1>
			<cfset application.getObject("connector.com.connectorUtilities").log_(label="Begin setSynchStatusFlag",logStruct=arguments,debugDetails=this.logging)>
		</cfif>

		<!--- if we have a synching flag, then set it.. in reality, we should always have one, but if for whatever reason we don't, then don't waste time trying to set it --->
		<cfif application.getObject("flag").getFlagStructure("#this.baseEntity##arguments.status#").isOK>
			<!--- if we don't have a RW key, but we have a foreign key --->
			<cfif structKeyExists(arguments.tableMetaData,"columnList") and (not listFindNoCase(arguments.tableMetaData.columnList,this.baseEntityPrimaryKey) or this.direction eq "import")>
				<cfif listFindNoCase(arguments.tableMetaData.columnList,this.objectPrimaryKey) or this.direction eq "import">
					<cfset useRemoteKey = true>
				<cfelse>
					<cfset result.isOK = false>
					<cfset result.message = "#this.baseEntityPrimaryKey# does not exist as a column in the table nor does the remote column #this.objectPrimaryKey#">
				</cfif>
			</cfif>

			<cfif result.isOK>
				<cfset var setSynchingStatusResult = {}>

				<!--- small change that is not really needed, but am joining now to vEntityName rather than the base table... --->
				<cfquery name="setSynchStatusQry" result="setSynchingStatusResult">
					select distinct #useRemoteKey?"e.entityID":"t.#this.baseEntityPrimaryKey#"# as #this.baseEntityPrimaryKey#, 1 as '#this.baseEntity##arguments.status#'
						into #tempTableName#
					from #arguments.tablename# t
						<cfif useRemoteKey>inner join vEntityName e on e.crmId = t.#this.objectPrimaryKey# and e.entityTypeID = <cf_queryparam value="#application.entityTypeID[this.baseEntity]#" cfsqltype="cf_sql_integer"></cfif>
					where #useRemoteKey?"e.entityID":"t.#this.baseEntityPrimaryKey#"# is not null
				</cfquery>

				<cfset setSynchingStatusResult.tablename = tempTableName>
				<cfset application.getObject("connector.com.connectorUtilities").log_(logStruct=setSynchingStatusResult,debugDetails=this.logging)>

				<cfset upsertEntity(tablename=tempTableName,entityType=this.baseEntity)>
			</cfif>
		</cfif>

		<cfset application.getObject("connector.com.connectorUtilities").log_(logStruct=result,debugDetails=this.logging)>

		<cfreturn result>
	</cffunction>


		<!--- we don't break the tie betweeen the two systems in this function as we need to keep the tie as long and possible to run some of the queries that we need to run.
		Breaking the tie is currently done as part of the 'processSuccesses' function
		 --->
	<cffunction name="processDeletions" access="public" output="false" returnType="struct" hint="Handles records that have been deleted on the remote system by deleting them if so configured">
		<cfargument name="tablename" type="string" default="#this.synchDataView#">

		<cfset var deleteSynchingFlag = "">
		<cfset var deleteSynchingFlagWhereClause = "">
		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure()>

		<cfif arguments.tablename neq "">

			<!--- <cfif application.getObject('connector.connectorUtilities').doesTableContainRecords(tablename=arguments.tablename)> --->

			<cfsavecontent variable="deleteSynchingFlagWhereClause">
				<cfoutput>
					from #arguments.tablename# t
						inner join #this.baseEntity# e on e.#this.baseEntityPrimaryKey# = t.#this.baseEntityPrimaryKey#
						inner join booleanFlagData bfd on bfd.entityID = e.#this.baseEntityPrimaryKey#
						inner join flag f on bfd.flagID = f.flagID and f.flagGroupID = #application.com.flag.getFlagStructure(flagID="#this.baseEntity#Exported").flagGroupID#
				</cfoutput>
			</cfsavecontent>

			<cftry>
				<!--- deleting the synching flag for core entities so that they are not picked up in any synchs--->
				<cfquery name="deleteSynchingFlag">
					update booleanFlagData set lastUpdated=getDate(),
						lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson=#request.relayCurrentUser.personID#
					#preserveSingleQuotes(deleteSynchingFlagWhereClause)#

					delete booleanFlagData
					#preserveSingleQuotes(deleteSynchingFlagWhereClause)#
				</cfquery>

				<!--- if the object is allowed to be deleted--->
				<cfif application.getObject("connector.com.connector").getConnectorObject(object=this.baseEntity,relayware=true).allowDeletions>

					<!--- if it's POL data then we only flag for deletion; if the entity has a deleted column, then we set that to true  --->
					<cfif listFindNoCase("organisation,location,person",this.baseEntity) or application.com.relayEntity.getTableFieldStructure(tablename=this.baseEntity,fieldname="deleted").isOK>

						<cfset var deleteTableName = result.successResult.tablename>
						<cfset var getRecordsToDelete = "">

						<cfquery name="getRecordsToDelete">
							select t.#this.baseEntityPrimaryKey#, 1 as <cfif listFindNoCase("organisation,location,person",this.baseEntity)>#application.com.flag.getFlagStructure(flagID="Delete#this.baseEntity#").flagTextID#<cfelse>deleted</cfif><!--- , null as #foreignKeyColumn# --->
								into #deleteTableName#
							from #arguments.tablename# t
								inner join #this.baseEntity# e on e.#this.baseEntityPrimaryKey# = t.#this.baseEntityPrimaryKey#
						</cfquery>

						<!--- setting the records as deleted --->
						<cfset result = upsertEntity(tablename=deleteTableName)>

					<!--- we're doing a hard delete --->
					<cfelse>

						<cfset var deleteEntityQry = "">
						<cfset var deleteRecordResult = {}>

						<cfquery name="deleteEntityQry" result="deleteRecordResult">
							update #this.baseEntity#
							set lastUpdated = getDate(),
								lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
								lastUpdatedByPerson=#request.relayCurrentUser.personID#
							from
								#arguments.tablename# t inner join #this.baseEntity# e
									on e.#this.baseEntityPrimaryKey# = t.#this.baseEntityPrimaryKey#

							delete #this.baseEntity#
							from
								#arguments.tablename# t inner join #this.baseEntity# e
									on e.#this.baseEntityPrimaryKey# = t.#this.baseEntityPrimaryKey#
						</cfquery>

						<cfset structAppend(result.successResult,deleteRecordResult)>
					</cfif>
				</cfif>

				<cfset application.getObject("connector.com.connectorUtilities").log_(label="processDeletions",logStruct=result,debugDetails=this.logging)>
				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.message = cfcatch.message>
					<!--- <cf_transaction action="rollback"> --->

					<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector Delete Entity",Severity="error",catch=cfcatch,WarningStructure=arguments)>

					<!--- rethrow the error here because if we continue, we lose the ties between the two systems and then we can't recuperate... so, throwing an error here means
						that we don't successfully complete this run. --->
					<cfrethrow>
				</cfcatch>
			</cftry>
			<!--- </cfif> --->
		</cfif>

		<cfreturn result>

	</cffunction>

</cfcomponent>