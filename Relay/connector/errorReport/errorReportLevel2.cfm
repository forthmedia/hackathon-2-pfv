<!--- ©Relayware. All Rights Reserved 2016 --->
<!---
File name:		errorReportLevel2.cfm
Author:			Anton Trunov
Date started:	21-11-2016

Description:	Connector error report Level 2

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/11/21			AT	Initial commit

Possible enhancements:

--->

<cf_includeJavascriptOnce template = "/javascript/extextension.js">

<cfparam name="object" type="string" default="">
<cfparam name="direction" type="string" default="">
<cfparam name="sortOrder" type="string" default="errorCount DESC">

<cfset application.com.request.setTopHead(pageTitle = "Detailed Error Report Level 2")>

<cfquery name="getReportLevel2Details">
SELECT * FROM (
    SELECT	COUNT(vcqe.ID) as errorCount,
            MAX(vcqe.created) as mostRecentError,
            vcqe.field as connectorMappingField,
            CASE
                WHEN vcqe.direction = 'E' and vcqe.connectorType = 'salesforce'  THEN 'From Salesforce : ' + '"' + vcqe.message + '"'
                WHEN vcqe.direction = 'E' and vcqe.connectorType != 'salesforce'  THEN 'From Dynamics : ' + '"' + vcqe.message + '"'
                ELSE 'From Relayware : ' + '"' + vcqe.message + '"'
            END AS message,
            'Show>' as detail,
            vcqe.direction,
            vcqe.message as originalMessage,
            vcqe.connectorType,
            vcqe.object,
            cm.object_relayware

    FROM
        vconnectorQueueError vcqe
        INNER JOIN vConnectorMapping cm
            ON (cm.object_relayware = vcqe.object or cm.object_remote = vcqe.object)
    WHERE vcqe.object = <cf_queryparam value="#object#" CFSQLTYPE="CF_SQL_VARCHAR" >
          AND vcqe.direction = <cf_queryparam value="#direction#" CFSQLTYPE="CF_SQL_VARCHAR" >
          AND cm.isRemoteID=1
          AND cm.active=1
          AND cm.connectorType =  <cf_queryparam value="#application.com.settings.getSetting("connector.type")#" CFSQLTYPE="CF_SQL_VARCHAR" >
    GROUP BY vcqe.field,
            vcqe.direction,
            vcqe.message,
            vcqe.connectorType,
            vcqe.object,
            cm.object_relayware) result

    WHERE 1=1

    <cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">

    ORDER BY <cf_queryObjectName value="#sortOrder#">

</cfquery>

<CF_tableFromQueryObject
        queryObject="#getReportLevel2Details#"
        queryName="getReportLevel2Details"
        showTheseColumns="errorCount,mostRecentError,object,connectorMappingField,message,detail"
        columnTranslation="true"
        columnTranslationPrefix="phr_sys_connector_report_"
        useInclude=false
        numRowsPerPage="400"
        allowColumnSorting="yes"
        keyColumnList="detail"
        keyColumnOnClickList="javascript:openNewTab('Detailed Error Report Level 3'*comma'Detailed Error Report Level 3'*comma'/connector/connectorStatusReport.cfm?entityType=##object_relayware##&connectorMappingField=##connectorMappingField##&message=##originalMessage##&direction=#direction#&dashboardStatus=Failed'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;"
        keyColumnURLList=" "
        keyColumnKeyList=" "
        FilterSelectFieldList="object,connectorMappingField"
        dateFilter="mostRecentError"
        >