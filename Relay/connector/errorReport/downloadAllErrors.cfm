<!--- ©Relayware. All Rights Reserved 2016 --->
<!---
File name:		downloadAllErrors.cfm
Author:			VB
Date started:	21-11-2016

Description:	Exports all connector errors to the Excel file

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/11/21			VB			Initial commit

Possible enhancements:

--->

<cfquery name="getAllErrors" DATASOURCE="#application.SiteDataSource#">
	select ID,queueID,connectorType,object,direction,objectID,modifiedDateUTC,synchAttempt,isDeleted,message,field,value,statusCode,created,connectorResponseID
		from vconnectorQueueError
</cfquery>

<cf_tableFromQueryObject
				queryObject="#getAllErrors#"
				queryName="getAllErrors"
				showTheseColumns="ID,queueID,connectorType,object,direction,objectID,modifiedDateUTC,synchAttempt,isDeleted,message,field,value,statusCode,created,connectorResponseID"
				showAsLocalTime="false"
				booleanFormat="isDeleted"
				dateFormat="modifiedDateUTC,created"
				columnTranslation="true"
				columnTranslationPrefix="phr_sys_connector_report_"
				allowColumnSorting="no"
				hidePageControls="true"
				useInclude="false"
				openAsExcel="true">