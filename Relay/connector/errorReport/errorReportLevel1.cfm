<!--- ©Relayware. All Rights Reserved 2016 --->
<!---
File name:		errorReportLevel1.cfm
Author:			Oleg Vitko
Date started:	21-11-2016

Description:	Connector error report Level 1

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/11/21			OV	Initial commit

Possible enhancements:

--->

<cfparam name="sortOrder" type="string" default="errorCount DESC">

<!--- Download All Errors --->
<cf_includeJavascriptOnce template = "/javascript/extextension.js">

<cfquery name="objectDirectionProspective" >
	SELECT *
	FROM
		(SELECT
			[object],
			CASE WHEN direction = 'I' THEN 'Import'
				 WHEN direction = 'E' THEN 'Export'
				 ELSE 'Wrong Direction' END AS directionSign,
			direction,
			COUNT(*) errorCount,
			MAX(modifiedDateUTC) mostRecentError,
			'Show>' AS detail
		FROM
			vConnectorQueueError
		GROUP BY
			[object],
			direction) as result
	WHERE 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">
    ORDER BY <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_tableFromQueryObject
        queryObject="#objectDirectionProspective#"
        queryName="objectDirectionProspective"
        showTheseColumns="object,directionSign,errorCount,mostRecentError,detail"
		columnTranslation="true"
		columnTranslationPrefix="phr_sys_connector_report_"
        allowColumnSorting="yes"
		keyColumnList="detail"
		keyColumnOnClickList="javascript:openNewTab('Detailed Error Report Level 2'*comma'Detailed Error Report Level 2'*comma'/connector/errorReport/errorReportLevel2.cfm?object=##object##&direction=##direction##'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;"
		keyColumnURLList=" "
		keyColumnKeyList=" "
		FilterSelectFieldList="object,directionSign"
		dateFilter="mostRecentError"
		>

<div>
    <a class="btn btn-primary" href="/connector/errorreport/downloadAllErrors.cfm">phr_sys_connector_report_DownloadAllErrors</a>
</div>