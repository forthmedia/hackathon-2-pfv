<!--- ©Relayware. All Rights Reserved 2015 --->
<!---
File name:		connectorMSDynamics.cfc
Author:			NJH
Date started:	02-12-2014

Description:	A component designed for the msdynamics connector that override the core connector

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="getRemoteObjectFields" access="public" output="false" returnType="query" hint="Returns a query of MSDynamics object fields used to populate the connector mapping table">
		<cfargument name="object" type="string" required="true">

		<cfset var qryFields = queryNew("name,hasPickListValues,datatype,type,nillable,length,isId","varchar,bit,varchar,varchar,bit,integer,bit")>

		<cfif arguments.object neq "">
			<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(object=arguments.object)>

			<cfif structKeyExists(application.connector.objectMetaData,arguments.object)>
				<cfset qryFields = application.com.structureFunctions.structToQuery(struct=application.connector.objectMetaData[arguments.object].fields,defaultColumnsAndDataTypes="name:varchar,hasPickListValues:bit,datatype:varchar,type:varchar,nillable:bit,isId:bit")>
			</cfif>
		</cfif>

		<cfreturn qryFields>

	</cffunction>


	<cffunction name="getPickListValuesForRemoteObjectColumn" access="public" output="false" hint="Returns a query of picklist values for a given SF object/column" returnType="query">
		<cfargument name="object" type="string" required="true">
		<cfargument name="column" type="string" required="true">

		<cfset var qryPickListValues = queryNew("name,value","varchar,varchar")>

		<cfif arguments.object neq "">
			<cfset var connectorObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorObject(connectorType="msdynamics")>
			<cfset connectorObject.putMSDCObjectIntoMemory(object=arguments.object)>

			<cfif structKeyExists(application.connector.msdynamics.objectMetaData[arguments.object],arguments.column)>
				<cfset qryPickListValues = application.connector.msdynamics.objectMetaData[arguments.object][arguments.column].pickListValues>
			</cfif>
		</cfif>

		<cfreturn qryPickListValues>
	</cffunction>


	<cffunction name="sendApiRequest" access="public" output="false" hint="Wrapper to make an api request to remote system" returnType="struct">

		<cfset var connectorObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorObject(connectorType="msdynamics")>
		<cfreturn connectorObject.api.send(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getDataForIDFromConnectorResponse" output="false" access="public" hint="Gets the data from the connectorResponse table showing what the state of the data was at the time of sending" returnType="query">
		<cfargument name="connectorResponseID" type="numeric" required="true">
		<cfargument name="relaywareID" type="numeric" required="false">
		<cfargument name="remoteID" type="string" required="false">

		<cfreturn getDataFromConnectorResponse(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getDataFromConnectorResponse" output="false" access="public" hint="Gets the data from the connectorResponse table showing what the state of the data was at the time of sending" returnType="query">
		<cfargument name="connectorResponseID" type="numeric" required="true">
		<cfargument name="remoteID" type="string" required="false" default="">
		<cfargument name="relaywareID" type="string" required="false" default="">

		<cfset var rQuery = ''>
		<cfset var rQueryResult = ''>

		<cfif arguments.remoteID eq "" and arguments.relaywareID eq "">
			<cfquery name="rQuery" result="rQueryResult">
				select responseXML from connectorResponse where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfset var response = rQuery["RESPONSEXML"]>
			<cfreturn convertStructToQuery(response)>
		</cfif>

		<cfif arguments.relaywareID neq "">
			<cfquery name="rQuery" result="rQueryResult">
				select responseXML from connectorResponse
				where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer"> and (cast(sendXML as nvarchar(max)) LIKE <cf_queryparam value="%#arguments.relaywareID#%" cfsqltype="cf_sql_varchar"> or cast(responseXML as nvarchar(max)) LIKE <cf_queryparam value="%#arguments.relaywareID#%" cfsqltype="cf_sql_varchar">)
			</cfquery>

			<cfset var response = rQuery["RESPONSEXML"]>
			<cfreturn convertStructToQuery(response)>
		</cfif>

		<cfif arguments.remoteID neq "">
			<cfquery name="rQuery" result="rQueryResult">
				select responseXML from connectorResponse
				where ID=<cf_queryparam value="#arguments.connectorResponseID#" cfsqltype="cf_sql_integer"> and (cast(sendXML as nvarchar(max)) LIKE <cf_queryparam value="%#arguments.remoteID#%" cfsqltype="cf_sql_varchar"> or cast(responseXML as nvarchar(max)) LIKE <cf_queryparam value="%#arguments.remoteID#%" cfsqltype="cf_sql_varchar">)
			</cfquery>

			<cfset var response = rQuery["RESPONSEXML"]>
			<cfreturn convertStructToQuery(response)>
		</cfif>

		<cfreturn queryNew('empty')>
	</cffunction>


	<cffunction name="convertStructToQuery" output="false" access="private" returnType="query">
		<cfargument name="response" type="string" required="true">

		<cfset var responseStruct = DeserializeJSON(#response#)>
		<cfset var responseStruct = DeserializeJSON(#responseStruct.Filecontent#)>

		<cfset var structToConvert= responseStruct>

		<cfif structKeyExists(responseStruct,"value")>
			<cfset structToConvert = responseStruct.value[1]>
		</cfif>

		<cfloop collection="#structToConvert#" item="structKey">
			<cfif findNoCase("@odata.",structKey)>
				<cfset structDelete(structToConvert,structKey)>
			</cfif>
		</cfloop>

		<cfreturn application.com.structureFunctions.structToQuerySimple(struct=structToConvert)>
	</cffunction>


	<cffunction name="convertObjectFieldMetaDataToRwQuery" access="public" output="false" hint="Returns the meta data as given by remote system into a RW format" returnType="query">
        <cfargument name="tablename" type="string" required="true">
        <cfargument name="columnList" type="string" required="true">

        <!--- just getting these fields for the moment. The dataType field is a field that we use to map SF field datatypes to RW field datatypes, so we are currently looping some datatypes together under text or numeric --->
        <cfset var getObjectDetails = "">

        <cfquery name="getObjectDetails">
            select createable,updateable,length,name,type,nillable,
                <!--- apparently not all object have picklist values, so double check that the column exists before using it. (AcceptedEventRelation object is an example)--->
                <cfif listFindNoCase(arguments.columnList,"picklistValues")>
                case when len(isNull(cast(pickListValues as nvarchar(max)),'')) != 0 then '<pickList>'+cast(pickListValues as nvarchar(max))+'</pickList>' else null end as pickListValues,
                case when len(isNull(cast(pickListValues as nvarchar(max)),'')) != 0 then 1 else 0 end as hasPickListValues,
                <cfelse>
                null as pickListValues, 0 as hasPickListValues,
                </cfif>
                dataType,
				case when type = 'id' then 1 else 0 end as isID
            from #arguments.tablename#
        </cfquery>
        <cfreturn getObjectDetails>
    </cffunction>


    <cffunction name="convertObjectDataToRwQuery" access="public" output="false" hint="Returns the object data as given by remote system into a RW format" returnType="query">
		<cfargument name="UI" default="false">
		<cfscript>
		var objArray = getRemoteObjects(UI=arguments.UI).successResult;
		var jsonObjArray = [];
		for(objName in objArray){
			newObj = {
				"name" : "#objName#",
				"label" : "#objName#"
			};
			arrayAppend(jsonObjArray, newObj);
		}

		   // fake data generated at https://www.mockaroo.com
		   result = queryNew(
		       "name, label",
		       "varchar, varchar",
		       jsonObjArray
		   );
		   return result;
		</cfscript>
        <cfreturn remoteObj>
    </cffunction>


	<cffunction name="getRemoteObjects"  output="false" returnType="struct" hint="Returns a query of msdynamics objects used to populate the connectorObject table">
		<cfargument name="UI" default="false">
        <cfset var rest = application.getObject("connector.com.connectorUtilities").instantiateConnectorAPIObject(connectorType="msdynamics")>
        <cfreturn rest.getRemoteObjects(UI=arguments.UI)>
    </cffunction>


	<cffunction name="getUserMatchingData" access="public" hint="Returns the column names of the user object that we will need for matching to exsiting Relayware Users. This includes both location and person fields." returnType="struct">
		<cfreturn {fieldStruct={id="systemuserid",company="organizationid",address1="address1_line1",phone="address1_telephone1",address4="address1_city",postalCode="address1_postalcode",country="address1_country",firstname="firstname",lastname="lastname",email="internalemailaddress"},userObjectName="systemusers"}>
	</cffunction>

	<!--- function created to get the keys for an object that is about to be exported. This was based off another function in the API, but an an attempt to improve performance, this is used  when getting the data from
		a query, so that the query generates these keys and values  --->
	<cffunction name="getExportValuesForKeys" access="public" output="false" returnType="struct">
		<cfargument name="fieldList" type="string" required="true">
		<cfargument name="objectName" type="string" required="true">

		<cfset var keyValueStruct = {}>
		<cfset var dynamicsAPI = application.getObject("connector.com.connectorUtilities").instantiateConnectorAPIObject(connectorType="msdynamics")>
        <cfset var metadata = dynamicsAPI.getMData()>
		<cfset var fieldName = "">
		<cfset var key = "">

		<cfparam name="request.exportKeys" default="#structNew()#">
		<cfif not structKeyExists(request.exportKeys,arguments.objectName)>
			<cfset request.exportKeys[arguments.objectName] = {}>
		</cfif>

		<cfloop list="#arguments.fieldList#" index="fieldName">
			<cfif not structKeyExists(request.exportKeys[arguments.objectName],fieldname)>
				<cfset var replaceFieldNameString = "#application.delim1##fieldName##application.delim1#">
				<cfset keyValueStruct = {value = fieldname, key = fieldname}>

				<cfif Lcase(fieldName).endswith('_value')>
	                <!---Specific cases can't be hundled by metadata--->
		            <cfif Lcase(fieldName) EQ '_ownerid_value'>
						<!--- <cfset key = "ownerid@odata.bind"> --->
						<cfset keyValueStruct.key = "ownerid@odata.bind">
		                <cfset keyValueStruct.value = '/systemusers(' & replaceFieldNameString & ')'>
					<cfelse>
						<cfset fieldName = Lcase(fieldName)>

		                <cfset var regExpNavPropTag = '<NavigationProperty Name=.*?<\/NavigationProperty>'>
		                <cfset var regExpReferentialConstraint = '<ReferentialConstraint Property="#fieldName#.*?'>
						<cfset var regExpParent = 'Partner=".*?"'>
		                <cfset var startTag = '<NavigationProperty Name="'>
		                <cfset var endTag = '"'>
		                <cfset var regExpNavPropName = startTag & '.*?' & endTag>
		                <cfset var match = "">
						<cfset var match2 = "">

		                <cfset var matchArray = REMatch(regExpNavPropTag, metadata)>
		                <cfif arrayLen(matchArray)>
		                    <cfloop index="i" from="1" to="#arrayLen(matchArray)#">
		                        <cfset matchReferentialConstraint = REMatch(regExpReferentialConstraint, matchArray[i])>
		                        <cfif arrayLen(matchReferentialConstraint)>
		                            <cfset var matchNavArray = REMatch(regExpNavPropName, matchArray[i])>
									<cfset var matchParentArray = REMatch(regExpParent, matchArray[i])>
		                            <cfif arrayLen(matchNavArray)>
		                                <cfset match = Replace(matchNavArray[ArrayLen(matchNavArray)], startTag, "")>
		                                <cfset match = Replace(match, endTag, "")>
										<cfset match2 = dynamicsAPI.getEntityByIdName(match)>
		                                <cfbreak>
		                            </cfif>
		                        </cfif>
		                    </cfloop>
		                </cfif>

		                <cfset keyValueStruct.value = '/' & match2 & '(' & replaceFieldNameString & ')'>
						<cfset keyValueStruct.key = match&"@odata.bind">
		            </cfif>
				</cfif>
				<cfset request.exportKeys[arguments.objectName][fieldname] = keyValueStruct>
			</cfif>
        </cfloop>

		<cfreturn request.exportKeys[arguments.objectName]>
	</cffunction>
</cfcomponent>