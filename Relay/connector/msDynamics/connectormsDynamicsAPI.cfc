<!--- �Relayware. All Rights Reserved 2015 --->
<!---
File name:		connectormsDynamicsAPI.cfc
Author:
Date started:	10-10-2016

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
2016/10/10	NJH	JIRA PROD2016-2346 - added fetch functionality when query result exceeded 50 records

 --->


<cfcomponent name="MSDynamicsAPI" output="false">

    <cfparam name="token" type="string" default="">
    <cfparam name="refreshTokenUrl" type="string" default="https://login.microsoftonline.com/common/oauth2/token">
    <cfparam name="root" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.rootOrganization')#">
    <cfparam name="ms.url" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.rootOrganization') & '/api/data/' & application.com.settings.getSetting('connector.msdynamics.apiUser.apiVersion') & '/'#">
    <cfparam name="checkTokenUrl" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.rootOrganization') & '/api/data/' & application.com.settings.getSetting('connector.msdynamics.apiUser.apiVersion') & '/territories'#">
    <cfparam name="client_id" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.clientId')#">
    <cfparam name="client_secret" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.clientSecret')#">
    <cfparam name="azure.user.name" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.azureUserName')#">
    <cfparam name="password" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.password')#">
    <cfparam name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
	<cfparam name="matadataUrl" type="string" default="#application.com.settings.getSetting('connector.msdynamics.apiUser.rootOrganization') & '/XRMServices/2011/Organization.svc/web'#">

    <cfset daoService = createObject("component","relay.connector.msDynamics.MSDynamicsDAOService")>
<!---Main Rest API method which has parameter "method" to process further requests --->
    <cffunction name="send" access="public" output="false" returntype="Any">
        <cfargument name="object" type="String" required="false"> <!--- NJH 2016/05/06 - set to false as getRemoteObjects doesn't need an object passed through --->
        <cfargument name="method" type="String" required="true">
        <cfargument name="objectId" type="string" required="false">
        <cfargument name="objectArrayId" type="array" required="false">
        <cfargument name="whereClause" type="string" required="false" default="">
        <cfargument name="viewName" type="string" required="false" default="">
        <cfargument name="columnList" type="string" required="false" default="*">
        <cfargument name="UI" default="false">

        <cfset var startTime = GetTickCount()>

        <cfset var callMethod = LCase(arguments.method)>
        <cfswitch expression="#callMethod#">
            <!---Crates new objects on MS Dynamics side--->
            <cfcase value="create">
                <cfset var objectEntityOne	= '' />
                <cfset var objCollection = daoService.exportFromDB(objectName = object, whereClause = arguments.whereClause, viewName = arguments.viewName, columnList = arguments.columnList, singleObject = parseMsEntityName(object))>
				<cfset var i= 0>

                <cfset var batch = batchExecutor(objectList=objCollection, objectName=object, httpMethod="POST")>
                <cfset var result = logCreateUpdateResponseIntoTable(methodResult=batch, objCollection=objCollection, object = object, truncateTable=true,method="create")>

                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>
				<cfreturn result>
            </cfcase>
            <!---Updates already existing objects on MS Dynamics side--->
            <cfcase value="update">
                <cfif not structKeyExists(arguments, "viewName")>
                    <cfoutput>viewName is mandatory for method #method#.</cfoutput>
                    <cf_abort>
                </cfif>

                <cfset var objCollection = daoService.exportFromDB(objectName = object, whereClause = arguments.whereClause, viewName = arguments.viewName, columnList = arguments.columnList, singleObject = parseMsEntityName(object))>
                <cfset var batchResult = batchExecutor(objCollection, arguments.object, "PATCH")>
				<cfset var result = logCreateUpdateResponseIntoTable(methodResult=batchResult,objCollection=objCollection,method="update",object=arguments.object)>

                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>

                <cfreturn result>
            </cfcase>
            <!---Delete objects on MS Dynamics side if there were deleted on RW side--->
            <cfcase value="delete">
                <cfif not structKeyExists(arguments, "ids")>
                    <cfoutput>ids is mandatory for method #method#.</cfoutput>
                    <cf_abort>
                </cfif>
				<cfset var loopCount=0>
				<cfset var thisObjectID = 0>

				<cfloop array="#arguments.ids#" index="thisObjectID">
					<cfset loopCount++>
	                <cfset var deleteResult = delete(object, thisObjectID)>

	                <cfset var objCollection=ArrayNew(1)>
	                <cfset objCollection[1] = {id=thisObjectID}>
	                <cfset logCreateUpdateResponseIntoTable(methodResult=deleteResult,objCollection=objCollection,truncateTable=loopCount eq 1?true:false,method="delete",object=arguments.object)>
	            </cfloop>

				<cfset var result = {}>
				<cfset result.result.recordCount = loopCount> <!--- just setting this as its expected... the number doesn't actually matter at this point --->
				<cfset result.result.tablename = 'connector_saveResult'>
				<cfset result.successResult.tablename = result.result.tablename>
				<cfset result.isOK = true>

                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>

				 <cfreturn result>
            </cfcase>

            <!---Retrieve object by ID--->
            <cfcase value="retrieve">
                <cfif not structKeyExists(arguments, "objectId")>
                    <cfoutput>objectId is mandatory for method #method#.</cfoutput>
                    <cf_abort>
                </cfif>
				<cfset var tablename = arguments.object neq "systemUsers"?"connector_#connectorType#_#arguments.object#":"">
                <cfset var objectReturn = getById(object=object, objectID=objectId,fieldList=fieldlist)>
                <cfset var dao = daoService.importDataToDB(objectName=object, object=objectReturn.data,temptable=tablename,connectorResponseId=objectReturn.responseID)>

				<!--- TODO: REDO this. Put this in here for now to get Polina up and running and we don't have a nicer way of doing it yet!
					if we're getting system user data, then we need to get the organisation name from the organisation entity... So, make another call. Bit nasty to do it here, but at the moment, this is the only nice place --->
				<cfif arguments.object eq "systemUsers">
					<cfset var getOrganisationName = "">
					<cfset var updateOrganisationIDCol = "">

					<cfquery name="getOrganisationName">
						select distinct organizationID from #dao.successResult.tablename#
					</cfquery>

					<cfloop query="getOrganisationName">
						 <cfset var objectReturn = getById(object="organizations", objectID=getOrganisationName.organizationID,fieldList="name")>

						 <cfquery name="updateOrganisationIDCol">
							 update #dao.successResult.tablename# set organizationId=<cf_queryparam value="#objectReturn.data.name#" cfsqltype="cf_sql_varchar"> where organizationID=<cf_queryparam value="#getOrganisationName.organizationID#" cfsqltype="cf_sql_varchar">
						</cfquery>
					</cfloop>

				</cfif>
                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>
                <cfreturn dao>
            </cfcase>
            <!---Retrieve set of objects by array of IDs --->
            <cfcase value="retrieveBulk">
                <cfset var dao = {}>
                <cfset var chunk = 50>
                <cfif not structKeyExists(arguments, "objectArrayId")>
                    <cfoutput>objectArrayId is mandatory for method #method#.</cfoutput>
                    <cf_abort>
                </cfif>
                <cfset var tablename = arguments.object neq "systemUsers"?"connector_#connectorType#_#arguments.object#":"">
                <cfset var clauses = "">
                <cfset var fieldName = application.getObject("connector.com.connector").getRemoteIDColumn(connectorType="MsDynamics",object=object)>
                <!---
                <cfset var getIdField = "">

                <cfquery name="getIdField" dbtype="query">
				    select name from objectMetaData where isid = 1
			    </cfquery>
                <cfset var fieldName = "">
                <cfloop query = "getIdField">
                    <cfset var fieldName = name>
                </cfloop>
 --->

				<!--- if doing bulk retrieve on system users, we will need to set the field as we don't hold meta data for system users object --->
				<cfif fieldname eq "" and arguments.object eq "systemUsers">
					<cfset fieldname = application.getObject("connector.msDynamics.com.connectorMSDynamics").getUserMatchingData().fieldStruct.id>
				</cfif>

                <cfset var last = ListLast(ArrayToList(objectArrayId))>
                <cfset var recordCounter = 0>
                <cfset var objectReturn = structNew()>
                <!---Chucking request and retrieve by 50 objects --->
                <cfloop array="#objectArrayId#" index="name">
                    <cfset var clauses = clauses &  fieldName & ' eq ' & name>
                    <cfset var recordCounter = recordCounter + 1>
                    <cfif recordCounter eq chunk>
                        <cfset var objectReturnTemp = getByClause(object=object, clause=clauses,fieldList=fieldList)>

                        <cfif not structIsEmpty(objectReturn)>
                            <cfset objectReturn.data.value.addAll(objectReturnTemp.data.value)>
                        </cfif>
                        <cfif structIsEmpty(objectReturn)>
                            <cfset StructAppend(objectReturn, objectReturnTemp)>
                        </cfif>

                        <cfset var recordCounter = 0>
                        <cfset var clauses = "">
                        <cfcontinue>
                    </cfif>
                    <cfif name neq last>
                        <cfset var clauses = clauses & ' or '>
                    </cfif>
                    <cfif name eq last>
                        <cfset var objectReturnTemp = getByClause(object=object, clause=clauses,fieldList=fieldList)>

                        <cfif not structIsEmpty(objectReturn)>
                            <cfset objectReturn.data.value.addAll(objectReturnTemp.data.value)>
                        </cfif>
                        <cfif structIsEmpty(objectReturn)>
                            <cfset StructAppend(objectReturn, objectReturnTemp)>
                        </cfif>

                    </cfif>
                </cfloop>

                <cfif not isDefined("objectReturn.data.value") or ArrayIsEmpty(objectReturn.data.value)>
                    <cfreturn dao>
                </cfif>

                <cfset var dao = daoService.importDataToDBBulk(objectName=object, object=objectReturn.data.value,temptable=tablename,connectorResponseId=objectReturn.responseID,fieldList=fieldList)>

					<!--- TODO: REDO this. Put this in here for now to get Polina up and running and we don't have a nicer way of doing it yet!
					if we're getting system user data, then we need to get the organisation name from the organisation entity... So, make another call. Bit nasty to do it here, but at the moment, this is the only nice place --->
					<cfif arguments.object eq "systemUsers">
						<cfset var getOrganisationName = "">
						<cfset var updateOrganisationIDCol = "">

						<cfquery name="getOrganisationName">
                            select distinct organizationID from #dao.successResult.tablename#
						</cfquery>

						<cfloop query="getOrganisationName">
							 <cfset var objectReturn = getById(object="organizations", objectID=getOrganisationName.organizationID,fieldList="name")>

							 <cfquery name="updateOrganisationIDCol">
                                 update #dao.successResult.tablename# set organizationId=<cf_queryparam value="#objectReturn.data.name#" cfsqltype="cf_sql_varchar"> where organizationID=<cf_queryparam value="#getOrganisationName.organizationID#" cfsqltype="cf_sql_varchar">
							</cfquery>
						</cfloop>
					</cfif>
                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>
                <cfreturn dao>
            </cfcase>
            <!---Execute REST API query by clause--->
            <cfcase value="query">
                <cfif not structKeyExists(arguments, "clause")>
                    <cfoutput>clause is mandatory for method #method#.</cfoutput>
                    <cf_abort>
                </cfif>
                <cfset var objectReturn = getByClauseXML(object=arguments.object, clause=arguments.clause,fieldList=arguments.fieldList)>

				<!--- initialise the return in case there are no objects in the result --->
                <cfset var dao.isOK = true>
                <cfset var dao.successResult.tablename = "connector_#connectorType#_#arguments.object#">
				<cfset var dao.successResult.recordCount = 0>

				<cfif objectReturn.isOK>
					<cfset var dao = daoService.importDataToDBBulk(objectName=arguments.object, object=objectReturn.data.value,temptable=dao.successResult.tablename,connectorResponseId=objectReturn.responseID,fieldList=arguments.fieldList)>

					<!--- NJH 2016/10/04 JIRA PROD2016-2346 - fetch more --->
					<cfloop condition="structKeyExists(objectReturn.data,'@Microsoft.Dynamics.CRM.fetchxmlpagingcookie')">
						<cfset var nextPage = fetchNextPage(response=objectReturn.data["@Microsoft.Dynamics.CRM.fetchxmlpagingcookie"])>
						<cfset objectReturn = getByClauseXML(object=arguments.object, clause=arguments.clause,fieldList=arguments.fieldList,page=nextPage.pageNumber,pagingCookie=nextPage.pagingCookie)>
						<cfset dao = daoService.importDataToDBBulk(objectName=arguments.object, object=objectReturn.data.value,temptable=dao.successResult.tablename,connectorResponseId=objectReturn.responseID,fieldList=arguments.fieldList)>
					</cfloop>
				</cfif>

                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>

                <cfreturn dao>
            </cfcase>
            <cfcase value="describeSObjects">
                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>
                <cfreturn describeSObjects(arguments.method, arguments.object)>
            </cfcase>
            <cfcase value="getObjectFieldMetaData">
                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>
                <cfreturn describeSObjects(arguments.method, arguments.object)>
            </cfcase>
			<cfcase value="getRemoteObjects">
                <cfset var endTime = GetTickCount()>
                <cfset logRequestTime(timediff=endTime-startTime, method=arguments.method, endtime=Now())>
				<cfreturn getRemoteObjects(UI=arguments.UI)>
			</cfcase>
            <cfdefaultcase>
                <cfoutput>Method #method# does not exist in MsDynamics API.</cfoutput>
                <cf_abort>
            </cfdefaultcase>
        </cfswitch>

    </cffunction>

    <!---Measure time for API requests execution only in debugLevel=3 mode --->
    <cffunction name="logRequestTime" access="public" returntype="String">
        <cfargument name="timediff" required="true" type="String">
        <cfargument name="method" required="true" type="String">
        <cfargument name="endtime" required="false" default="#Now()#">

        <cfif structKeyExists(url,"debugLevel") and url.debugLevel eq 3>
			<cfset var createNonExistentTable = "">

            <cfquery name="createNonExistentTable">
				if not exists (select  1 from dbo.sysobjects o where o.xtype in ('U') and o.name =  'DynamicApiLogTime' )
	            begin
	            create table [dbo].[DynamicApiLogTime] (
	                timediff int, method varchar(50), endtime datetime
	                ) ON [PRIMARY]
               	end
            </cfquery>

			<cfset var qlogRequest = "">

            <cfquery name="qlogRequest">
                INSERT INTO DynamicApiLogTime VALUES (<cf_queryparam value="#arguments.timediff#" cfsqltype="cf_sql_integer">,
                                                    <cf_queryparam value="#arguments.method#" cfsqltype="cf_sql_varchar">,
                                                    <cf_queryparam value="#arguments.endtime#" cfsqltype="cf_sql_timeStamp">);
            </cfquery>
        </cfif>
    </cffunction>


	<cffunction name="logCreateUpdateResponseIntoTable" access="public" output="false" return="struct">
		<cfargument name="methodResult" required="true" type="struct">
		<cfargument name="objCollection" required="true" type="array">
		<cfargument name="truncateTable" type="boolean" default="true">
        <cfargument name="object" type="string" default="">
		<cfargument name="method" type="string" default="create">

		<cfset var result = {isOK=true,successResult={}}>
		<cfset result.successResult.tablename = "connector_saveResult">
		<cfset result.result.tablename = result.successResult.tablename>
		<cfset result.result.recordCount = arrayLen(arguments.objCollection)>

		<cfset var batchResponse=not structKeyExists(arguments.methodResult,"batch")?toString(arguments.methodResult.http.Filecontent):arguments.methodResult.batch>
		<cfset var truncateData = "">
		<cfset var i=0>
		<cfset var sqlValues = "">
		<cfset var insertResponse = "">
		<cfset var arrayIndex = 0>
		<cfset var message = "">
		<cfset var success = 1>
		<cfset var bulkFailureMessage = "">

		<!--- want to clear out any old data in this table --->
		<cfif arguments.truncateTable>
			<cfquery name="truncateData">
				truncate table #result.successResult.tablename#
			</cfquery>
		</cfif>

		<!--- bulk create - slightly different because it returns us the newly created IDs in the form of

			Location: https://jabracrmtest.crm4.dynamics.com/api/data/v8.0/opportunities(d2feaf80-ea91-e611-80f4-5065f38b8511)

			Only go in here if we get a 204 (ie. a successful create). Otherwise fall into else statement below.
		 --->
       	<cfif structKeyExists(arguments.methodResult,"batch") and findNoCase('HTTP/1.1 204 No Content', arguments.methodResult.batch) and arguments.method eq "create">
			<cfset  var regExp = "Location:(.*?)#arguments.object#(.*?)\)">
            <cfset  var matchArray = REMatch(regExp, arguments.methodResult.batch)>
			<cfset i = 1>
			<cfsavecontent variable="sqlValues">
				 <cfoutput>
	               <cfloop array="#matchArray#" index="matchR">
		               <cfset var mID = Replace(matchR, ")", "")>
                		<cfset var mID = Mid(mID, Find("(", mID)+1, Len(mID))>
		                <cfif i neq 1>,<cfelse><cfset i++></cfif>
	                  (<cf_queryparam value="#mID#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" >,null, null,1,null,<cf_queryparam value="#arguments.methodResult.id#" CFSQLTYPE="CF_SQL_VARCHAR" >)
              		 </cfloop>
              	</cfoutput>
            </cfsavecontent>

		<!--- bulk update and delete responses and errors ???  --->
      	<cfelse>
			<!--- bulk updates and deletes --->
			<cfif FindNoCase('HTTP/1.1 204 No Content', batchResponse) or (batchResponse EQ "" and arguments.methodResult.http.Statuscode EQ "204 No Content")>
				<cfset message = "">
				<cfset success = 1>
			<cfelse>
				<cfset message = parse(batchResponse, '"message":"', '"')>
				<cfset success = 0>

				<cfset var mappedObject = application.getObject("connector.com.connector").getMappedObject(object_remote=arguments.object)>
				<!--- this will be the Id of the record that failed --->
				<cfset rwID = application.getObject("connector.com.connector").getMappedField(object_relayware=mappedObject,column_relayware=application.com.relayEntity.getEntityType(entityTypeID=mappedObject).uniqueKey).field>
				<cfset arrayIndex = parse(batchResponse, "Content-ID: ", "#Chr(13)##Chr(10)##Chr(13)##Chr(10)#")>
				<cfif arrayIndex neq "">
					<cfset bulkFailureMessage = "Could not export record due to failure with exporting the batch. The failed record in the batch has entity ID #arguments.objCollection[arrayIndex].json[rwID]#. Note: Relayware cannot export records that are part of a batch that has failed."> <!--- as a batch will only return the error for the first record it encounters, we mark all other records in the batch as errored but then say that they could not be exported due to a failure with the problem record --->
				<cfelse>
					<cfset bulkFailureMessage = message> <!--- if content ID not found (a generic error rather than one for a particular record) then give all records the generic message --->
				</cfif>
			</cfif>

			<cfsavecontent variable="sqlValues">
				 <cfoutput>
	               <cfloop index="i" from="1" to="#arrayLen(arguments.objCollection)#">
		                <cfif i neq 1>,</cfif>
		                <cfset var resultMessage = success or arrayIndex eq i?message:bulkFailureMessage>
	                  (
		                  <cf_queryparam value="#arguments.objCollection[i].id#" CFSQLTYPE="CF_SQL_VARCHAR" >
		                  ,<cf_queryparam value="#arguments.object#" CFSQLTYPE="CF_SQL_VARCHAR" >
		                  ,null
		                  ,<cf_queryparam value="#resultMessage#" CFSQLTYPE="CF_SQL_VARCHAR" >
		                  ,<cf_queryparam value="#success#" CFSQLTYPE="CF_SQL_BIT" >
		                  ,null
		                  ,<cf_queryparam value="#arguments.methodResult.id#" CFSQLTYPE="CF_SQL_VARCHAR" >
	                  )
              		 </cfloop>
              	</cfoutput>
            </cfsavecontent>

		</cfif>

		<cfif trim(sqlValues) neq "">
			<cfquery name="insertResponse">
            	insert into #result.successResult.tablename# (ID, object, statusCode,message,success,fields,connectorResponseID)
            	select ID, object, statusCode,message,success,fields,connectorResponseID from (values #preserveSingleQuotes(sqlValues)#) dataToInsert(ID, object, statusCode,message,success,fields,connectorResponseID)
            </cfquery>
        </cfif>

		<cfreturn result>

	</cffunction>

    <cffunction name="describeSObjects" access="public" returnType="Any">
        <cfargument name="method" type="any" required="true">
        <cfargument name="object" type="string" required="true">

        <cfparam name="request.tempTableSuffix" default=""> <!--- set in connector.cfc - used as a handle on all temp tables so that they can be deleted after a run --->
        <cfset var requestId = logRequest(requestString="#ms.url & '/$metadata'#", object=arguments.object, method='describeSObjects')>
        <cfset var tablename= "">

        <cfset var colProperties = getMsColProperties(arguments.object)>

		<cfset var out.successresult.columnlist = "">

		<cfif arrayLen(colProperties)>
			<cfset tablename = application.getObject("connector.com.connectorUtilities").getTempTableName(TEMPTABLESUFFIX=request.TEMPTABLESUFFIX)>
	        <cfloop index = "colProp" array = "#colProperties#">
	            <cfset var item = structNew()>
	            <!---Change hardcode on metadata--->
	            <cfset item.createable = LCase(colProp.writepermission)>
	            <cfset item.updateable = LCase(colProp.writepermission)>
	            <cfset item.length = 4001 >
	            <cfset item.name = LCase(colProp.name)>
	            <cfset item.type = colProp.isId?"ID":LCase(colProp.type)>
	            <cfset item.nillable = 'true'>
				<cfset item.datatype = colProp.datatype>

					<cfset item.pickListValues = getPickListIfExists(colProp.entityName, item.name, item.type)>
		            <cfif len(trim(item.pickListValues))>
					<cfset item.datatype = 'picklist'>
				</cfif>

	            <cfset out = daoService.importDataToDB(object, item, tablename)>
	        </cfloop>
	    <Cfelse>
	    	<cf_abort>
		</cfif>

		<cfset var lists = out.successresult.columnlist>

        <cfset var result.isOK = true>
        <cfset var result.successResult.tablename = tablename>
        <cfset var result.successResult.columnList = lists>
        <cfreturn result>
    </cffunction>


	<cffunction name="serialiseObjectAsJson" access="private" returntype="string">
		<cfargument name="objectName" type="string" required="true">
		<cfargument name="object" type="any" required="true">

		<cfset var objectMetaData = application.getObject("connector.com.connector").getRemoteObjectFields(connectorType="MsDynamics",object=objectName)>
		<cfparam name="request.objectStringFields" default="#structNew()#">

		<cfif not structKeyExists(request.objectStringFields,arguments.objectName)>
			<cfset var getStringFields = "">
			<!--- get all the fields of type string.. They need to go across in the JSON as type string. serializeJSON does not put quotes around the value if it's a number, even though the field is a string --->
			<cfquery name="getStringFields" dbtype="query">
				select name from objectMetaData where datatype like 'text%'
			</cfquery>
			<cfset request.objectStringFields[arguments.objectName] = valueList(getStringFields.name)>
		</cfif>
       <!---  <cfset var getBooleanFields = "">
        <cfquery name="getBooleanFields" dbtype="query">
			select name from objectMetaData where datatype like 'boolean%'
		</cfquery>
        <cfloop query = "getBooleanFields">
            <cfif structKeyExists(arguments.object, #name#) and (arguments.object[#name#] eq 1)>
                    <cfset arguments.object[#name#] = true>
            </cfif>
            <cfif structKeyExists(arguments.object, #name#) and (arguments.object[#name#] eq 0)>
                <cfset arguments.object[#name#] = false>
            </cfif>
        </cfloop> --->

		<Cfreturn application.com.globalFunctions._serializeJSON(json=arguments.object,forceToString=request.objectStringFields[arguments.objectName])>
	</cffunction>

    <!---Method execute batch request according msdn spec https://msdn.microsoft.com/en-us/library/mt607719.aspx--->
    <cffunction name="batchExecutor" access="public" returnType="Any">
        <cfargument name="objectList" type="any" required="true">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="httpMethod" type="string" required="true">
        <cfargument name="objectArrayId" type="array" required="false">


        <cfset var timestamp = now().getTime() />
        <cfset var loopId = 1>
        <cfset var lists ="">
		<Cfset var i = 0>

        <cfif arguments.httpMethod eq "PATCH">

            <cfloop index="i" from="1" to="#arrayLen(objectList)#">
				<cftry>
				<!---  <cfset var modifiedObj = modifyIdToRef(objectList[i].json, arguments.objectName)> --->
                <cfset var json = serialiseObjectAsJson(objectName=arguments.objectName,object=objectList[i].json)>
                <cfset var oId =  objectList[i].id>

                <cfset var lists = lists & "--changeset_BBB#timestamp##Chr(13)##Chr(10)#" &
                "Content-Type: application/http#Chr(13)##Chr(10)#"
                & "Content-Transfer-Encoding:binary#Chr(13)##Chr(10)#"
                & "Content-ID: #loopId##Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
                "#httpMethod# #ms.url##objectName#(#oId#) HTTP/1.1#Chr(13)##Chr(10)#" &
                "Content-Type: application/json;type=entry#Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
                json & "#Chr(13)##Chr(10)#">

                <cfset loopId++>
				<cfcatch><Cfdump var="#arguments#"><cfdump var="#cfcatch#"><Cf_abort></cfcatch></cftry>
            </cfloop>
        </cfif>

        <cfif arguments.httpMethod eq "POST">
            <cfloop index="i" from="1" to="#arrayLen(objectList)#">
                  <!---   <cfset var modifiedObj = modifyIdToRef(objectList[i].json, arguments.objectName)> --->
                    <cfset var json = serialiseObjectAsJson(objectName=arguments.objectName, object=objectList[i].json)>

                    <cfset var lists = lists & "--changeset_BBB#timestamp##Chr(13)##Chr(10)#" &
                    "Content-Type: application/http#Chr(13)##Chr(10)#"
                    & "Content-Transfer-Encoding:binary#Chr(13)##Chr(10)#"
                    & "Content-ID: #loopId##Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
                    "#httpMethod# #ms.url##objectName# HTTP/1.1#Chr(13)##Chr(10)#" &
                    "Content-Type: application/json;type=entry#Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
                    json & "#Chr(13)##Chr(10)#">

                    <cfset loopId++>
            </cfloop>
        </cfif>

        <cfif arguments.httpMethod eq "DELETE">
            <cfloop index = "objectId" array = "#objectList#">
                <cfset var lists = lists & "--changeset_BBB#timestamp##Chr(13)##Chr(10)#" &
                "Content-Type: application/http#Chr(13)##Chr(10)#"
                & "Content-Transfer-Encoding:binary#Chr(13)##Chr(10)#"
                & "Content-ID: #loopId##Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
                "#httpMethod# #ms.url##objectName#(#objectId#) HTTP/1.1#Chr(13)##Chr(10)#" &
                "Content-Type: application/json;type=entry#Chr(13)##Chr(10)##Chr(13)##Chr(10)#">

                <cfset loopId++>
            </cfloop>
        </cfif>

        <cfset var body = "--batch_AAA#timestamp##Chr(13)##Chr(10)#" &
            "Content-Type: multipart/mixed;boundary=changeset_BBB#timestamp##Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
            lists &
        "--changeset_BBB#timestamp#--#Chr(13)##Chr(10)##Chr(13)##Chr(10)#" &
        "--batch_AAA#timestamp#--">

        <cfset var bToken = checkToken()>
        <cfset var urlRequest = "#ms.url & '/$batch'#">
<!---        TODO get object id--->
        <cfset var requestId = logRequest(requestString=urlRequest&Chr(13)&Chr(10)&body, object=arguments.objectName, method='batch')>
        <cfset var httpResult	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="POST">
            <cfhttpparam type="header" name="Content-Type" value="multipart/mixed;boundary=batch_AAA#timestamp#">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
            <cfif arguments.httpMethod eq "POST">
                <cfhttpparam type="header" name="Accept" value="application/json">
                <cfhttpparam type="header" name="OData-MaxVersion" value="4.0">
                <cfhttpparam type="header" name="OData-Version" value="4.0">
            </cfif>
            <cfhttpparam type="body" value="#body#">
        </cfhttp>


        <cfset var cfData = httpResult.Filecontent>
        <cfif arguments.httpMethod eq "POST">
            <cfset var log = logResponse(cfData.toString(), requestId)>
		<cfelse>
            <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        </cfif>
        <cfset var res = structNew()>
        <cfset var res.id = requestId>
		<cfif arguments.httpMethod eq "POST">
            <cfset var res.batch = cfData.toString()>
			<cfreturn res>
		</cfif>

        <cfset var res.http = httpResult>

        <cfreturn res>
    </cffunction>


    <!---Retrieve all objects--->
	<cffunction name="getAll" access="public" returnType="Any">
        <cfargument name="object" type="string" required="true">

        <cfset var bToken = checkToken()>
        <cfset var urlRequest = ms.url & object>

        <cfset var requestId = logRequest(requestString=urlRequest, object=arguments.object, method='retrieve')>
        <cfset var httpResult   = '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        <cfset var cfData = {data=DeserializeJSON(httpResult.Filecontent),responseID=requestID}>
        <cfreturn cfData>
    </cffunction>


    <!---get one object by id--->
    <cffunction name="getById" access="public" returnType="Any">
        <cfargument name="object" type="string" required="true">
        <cfargument name="objectId" type="string" required="true">
		<cfargument name="fieldList" type="string" required="false">

        <cfset var bToken = checkToken()>
        <cfset var urlRequest = "#ms.url & object & '(' & objectId & ')'#">
		<cfif structKeyExists(arguments,"fieldList") and arguments.fieldList neq "">
			<cfset urlRequest = urlRequest & "?$select=#arguments.fieldList#">
		</cfif>

        <cfset var requestId = logRequest(requestString=urlRequest, object=arguments.object, method='retrieve')>
        <cfset var httpResult	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        <cfset var cfData = {data=DeserializeJSON(httpResult.Filecontent),responseID=requestID}>
        <cfreturn cfData>
    </cffunction>

    <!---get set of objects by clause--->
    <cffunction name="getByClause" access="public" returnType="Any">
        <cfargument name="object" type="string" required="true">
        <cfargument name="clause" type="string" required="true">
		<cfargument name="fieldList" type="string" required="false">
		<cfargument name="orderBy" type="string" required="false">

        <cfset var bToken = checkToken()>
		<cfset var urlRequest = ms.url & object & "?">
		<cfif structKeyExists(arguments,"fieldList") and arguments.fieldList neq "">
			<cfset urlRequest = urlRequest & "&$select=#arguments.fieldList#">
		</cfif>
		<cfif structKeyExists(arguments,"orderBy") and arguments.orderBy neq "">
			<cfset urlRequest = urlRequest & "&$orderby=#arguments.orderBy#">
		</cfif>
		<cfif arguments.clause neq "">
			<cfset urlRequest = urlRequest & "&$filter=" & clause>
		</cfif>
        <cfset urlRequest = REPLACE("#urlRequest#","?&","?","ALL")>
        <cfset var requestId = logRequest(requestString=urlRequest, object=arguments.object, method='query')>

        <cfset var httpResult	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        <cfset var result = {data=DeserializeJSON(httpResult.Filecontent),responseID=requestId}>
        <cfreturn result>
    </cffunction>

    <!---Convert OData clause to fetchXML format--->
    <cffunction name="clausToXML" access="public" returnType="String">
        <cfargument name="clause" type="string" required="true">

        <cfset var result = "">
        <cfif clause eq "">
            <cfreturn result>
        </cfif>

        <cfset clauseParameters = clause.Split("and ")>
        <cfloop index = "eachClause" array = "#clauseParameters#">
            <cfset clauseConditions = eachClause.Split(" ")>
            <cfif ArrayLen(clauseConditions) eq 2 or ArrayLen(clauseConditions) eq 3>
                <cfset var result = result & '<filter type="and">'>
                <cfif ArrayLen(clauseConditions) eq 2>
                    <cfset var result = result & '<condition attribute="' & clauseConditions[1] & '" operator="' & clauseConditions[2] & '" />'>
                </cfif>
                <cfif ArrayLen(clauseConditions) eq 3>
                    <cfset var result = result & '<condition attribute="' & clauseConditions[1] & '" operator="' & clauseConditions[2] & '" value="' & clauseConditions[3] & '" />'>
                </cfif>
                <cfset var result = result & '</filter>'>
            </cfif>
        </cfloop>

        <cfreturn result>
    </cffunction>

    <!---Split clause by OData and XML clause--->
    <cffunction name="splitXMLandString" access="public" returnType="Array">
    <cfargument name="clause" type="string" required="true">

        <cfset var result = arrayNew(1)>
        <cfif clause eq "">
            <cfreturn result>
        </cfif>

        <cfset var matchArray = REMatch('<.*>', clause)>
        <cfif ArrayLen(matchArray) eq 1>
            <cfset var result[2] = matchArray[1]>
            <cfset var result[1] = Replace(clause, matchArray[1], "")>
        <cfelse>
            <cfset var result[1] = clause>
        </cfif>
        <cfreturn result>
    </cffunction>

    <!---Retrieve set of objects by clause using fetch XML approach--->
    <cffunction name="getByClauseXML" access="public" returnType="Any">
        <cfargument name="object" type="string" required="true">
        <cfargument name="clause" type="string" required="true">
        <cfargument name="fieldList" type="string" required="false">
        <cfargument name="orderBy" type="string" required="false">
		<cfargument name="page" type="numeric" required="false"> <!--- NJH 2016/10/04 JIRA PROD2016-2346 --->
		<cfargument name="pagingCookie" type="string" required="false"> <!--- NJH 2016/10/04 JIRA PROD2016-2346 --->

        <cfset var bToken = checkToken()>
        <cfset var urlRequest = ms.url & object & "?">

        <cfset var orderXml = "">
        <cfset var fieldXml = "">
        <cfset var clauseXml = "">

        <cfif arguments.clause neq "">
            <cfset var clauses = splitXMLandString(arguments.clause)>
            <cfset var clauseXml = clauseXml & clausToXML(clauses[1])>
            <cfif ArrayLen(clauses) eq 2>
                <cfset var clauseXml = clauseXml & clauses[2]>
            </cfif>
        </cfif>

        <cfif structKeyExists(arguments,"orderBy")>
            <cfset var orderParameters = arguments.orderBy.Split(" ")>
            <cfset var descendingVal = false>
            <cfif ArrayLen(orderParameters) eq 2 and lCase(orderParameters[2]) eq  "desc">
                <cfset var descendingVal = true>
            </cfif>
            <cfset var orderXml = "<order attribute='" & orderParameters[1] & "' descending='" & descendingVal & "' />">
        </cfif>

        <cfif structKeyExists(arguments,"fieldList")>
            <cfloop index = "FieldListElement" list = "#fieldList#">
                <cfset var fieldXml = fieldXml & "<attribute name='" & FieldListElement & "' />">
            </cfloop>
        </cfif>

		<!--- NJH 2016/10/04 JIRA PROD2016-2346 - deal with paging requests. --->
		<cfset var fetchPagingCookie = "">
		<cfif (structKeyExists(arguments,"page") and arguments.page neq 0) and (structKeyExists(arguments,"pagingCookie") and arguments.pagingCookie neq "")>
			<cfset fetchPagingCookie = "paging-cookie='#arguments.pagingCookie#' page='#arguments.page#'">
		</cfif>

        <cfset var xmlRequest = "fetchXml=<fetch mapping='logical' #fetchPagingCookie#>" &
        "<entity name='" & parseMsEntityName(arguments.object) &"'>" &
            fieldXml &
            orderXml &
            clauseXml &
        "</entity>" &
        "</fetch>" >

        <cfset var urlRequest = urlRequest & xmlRequest>
		<cfset var requestId = logRequest(requestString=urlRequest, object=arguments.object, method="queryXML")>
        <cfset var httpResult	= "" />
        <cfset var currentToken = getDynToken()>

        <cfhttp url="#urlRequest#" result="httpResult" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>

        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        <cfset var result = {data=DeserializeJSON(httpResult.Filecontent),responseID=requestId,isOK=true}>

		<!--- JIRA PROD2016-2855 - if we get a 500 or an error returned in the http call, then log it as an API error. --->
		<cfif structKeyExists(result.data,"error")>
			<cfset application.getObject("connector.com.connector").logAPIError(object=arguments.object,message=result.data.error.innerError.message,method="query",statusCode=result.data.error.code)>
			<cfset result.isOK=false>
		</cfif>

        <cfreturn result>
    </cffunction>


    <cffunction name="delete" access="public" returnType="struct">
        <cfargument name="object" type="string" required="true">
        <cfargument name="objectId" type="string" required="true">

        <cfset var bToken = checkToken()>
        <cfset var urlRequest = "#ms.url & object & '(' & objectId & ')'#">
        <cfset var requestId = logRequest(requestString=urlRequest, object=arguments.object, method='delete')>

        <cfset var httpResult	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="DELETE">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>

		<cfset var res = {http = httpResult,id = requestId}>

		<cfreturn res>
    </cffunction>


    <cffunction name="create" access="public" returnType="struct">
        <cfargument name="object" type="string" required="true">
        <cfargument name="objectEntity" type="any" required="true">

        <cfset var bToken = checkToken()>
        <cfset var urlRequest = "#ms.url & object#">
        <cfset var modifiedObj = modifyIdToRef(arguments.objectEntity, arguments.object)>

		<cfset var jsonObject = serialiseObjectAsJson(objectName=arguments.object,object=modifiedObj)>

        <cfset var requestId = logRequest(requestString=urlRequest & jsonObject, object=arguments.object, method='create')>

        <cfset var httpResult	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="POST">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
            <cfhttpparam type="body" value="#jsonObject#">
        </cfhttp>

        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
		<cfset var createResult = {http=httpResult,id=requestId}>
		<cfreturn createResult>
    </cffunction>

    <!---Get reference for parent object--->
    <cffunction name="modifyIdToRef" access="public" returnType="struct" hint="This method prepare specific dependencies for msDynamics REST API">
        <cfargument name="object" type="struct" required="true" hint="The actual object">
		<cfargument name="objectName" type="struct" required="true" hint="The name of the object">

        <cfset var modifiedStruct = {}>
        <cfset var metadata = getMData()>
		<cfset var fieldname = "">
		<cfset var exportKeysStruct = getExportValuesForKeys(fieldlist=structKeyList(arguments.object),objectName=arguments.objectName)>

        <cfloop collection=#arguments.object# item="fieldname">
			 <cfset modifiedStruct[exportKeysStruct[fieldname].key] = replaceNoCase(exportKeysStruct[fieldname].value,"#application.delim1##fieldname##application.delim1#",arguments.object[fieldname])>
            <!--- <cfif Lcase(it).endswith('_value')>
                <!---Specific cases can't be hundled by metadata--->
            <cfif Lcase(it) EQ '_ownerid_value'>
                <cfset var key = "ownerid@odata.bind">
                <cfset modifiedStruct[key] = '/systemusers(' & arguments.object[it] & ')'>>
                <cfcontinue>
            </cfif>
				<cfset it = Lcase(it)>
                <cfset var key = it>
                <cfset var value = Lcase(arguments.object[it])>

                <cfset var regExpNavPropTag = '<NavigationProperty Name=.*?<\/NavigationProperty>'>
                <cfset var regExpReferentialConstraint = '<ReferentialConstraint Property="#it#.*?'>
				<cfset var regExpParent = 'Partner=".*?"'>
                <cfset var startTag = '<NavigationProperty Name="'>
                <cfset var endTag = '"'>
                <cfset var regExpNavPropName = startTag & '.*?' & endTag>
                <cfset var match = "">
				<cfset var match2 = "">

                <cfset var matchArray = REMatch(regExpNavPropTag, metadata)>
                <cfif arrayLen(matchArray)>
                    <cfloop index="i" from="1" to="#arrayLen(matchArray)#">
                        <cfset matchReferentialConstraint = REMatch(regExpReferentialConstraint, matchArray[i])>
                        <cfif arrayLen(matchReferentialConstraint)>
                            <cfset var matchNavArray = REMatch(regExpNavPropName, matchArray[i])>
							<cfset var matchParentArray = REMatch(regExpParent, matchArray[i])>
                            <cfif arrayLen(matchNavArray)>
                                <cfset match = Replace(matchNavArray[ArrayLen(matchNavArray)], startTag, "")>
                                <cfset match = Replace(match, endTag, "")>
								<cfset match2 = getEntityByIdName(match)>
                                <cfbreak>
                            </cfif>
                        </cfif>
                    </cfloop>
                </cfif>

                <cfset var value = '/' & match2 & '(' & arguments.object[it] & ')'>
				<cfset var key = match&"@odata.bind">

                <cfset modifiedStruct[key] = value>

            <cfelse>
                <cfset modifiedStruct[it] = arguments.object[it]>
            </cfif> --->
        </cfloop>

        <cfreturn modifiedStruct>
    </cffunction>


    <cffunction name="update" access="public" returnType="void">
        <cfargument name="object" type="string" required="true">
        <cfargument name="objectId" type="string" required="true">
        <cfargument name="objectEntity" type="any" required="true">

        <cfset var bToken = checkToken()>
        <cfset var field	= '' />
        <cfset var httpResult	= '' />
        <cfloop collection=#objectEntity# item="field">
            <cfset var lfield = LCase(field)>
            <cfset var pair = {value = objectEntity[field]}>
            <cfset var urlRequest = "#ms.url & object & '(' & objectId & ')/' & lfield#">
            <cfset var jsonObject = LCase(serializeJSON(pair))>
            <cfset var requestId = logRequest(requestString=urlRequest & jsonObject, object=arguments.object, method='update')>
<!---            ColdFusion 10 doesn't support http method PATCH that's why need loop--->
            <cfset var currentToken = getDynToken()>
            <cfhttp url="#urlRequest#" result="httpResult" method="PUT">
                <cfhttpparam type="header" name="Content-Type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
                <cfhttpparam type="body" value="#jsonObject#">
            </cfhttp>

            <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        </cfloop>
    </cffunction>

    <cffunction name="getMsColProperties" access="public" returnType="array" hint="Gets object metadata and build map MS_cols -> Edb_type">
        <cfargument name="object" type="string" required="true">

        <cfset var metadata = getMData() />
        <cfset var objectName = parseMsEntityName(object, metadata)>
        <cfset var objectStructure = parse(metadata, '<EntityType Name="' & objectName & '"', '<\/EntityType>')>
        <cfset var setProperties = parseSet(objectStructure, '<Property Name="', '\/>')>
        <cfset var props = []>
        <cfset var i = '' />
        <cfloop from="1" to="#arrayLen(setProperties)#" index="i">
			<cfset var prop = {}>
			<cfset prop.entityName = objectName>
            <cfset prop.name = parse(setProperties[i], '<Property Name="', '"')>
            <cfset prop.type = parse(setProperties[i], 'Type="', '"')>
			<cfset prop.writePermission = parsePermission(objectName, prop.name, metadata)>
			<cfset prop.datatype = convertMSDynamicsTypeToRwType(prop.type, prop.entityName, prop.name, metadata)>
			<cfset prop.isId = isPrimaryKey(objectName,prop.name,metadata)>

			<cfset arrayAppend(props, prop)>
        </cfloop>
        <cfreturn props>
    </cffunction>

    <cffunction name="parseMsEntityName">
        <cfargument name="object" type="string" required="true">
        <cfargument name="metadata" type="Any" default="#getMData()#">

        <cfset var objectName = parse(metadata, '<EntitySet Name="' & object & '"', '">')>
        <cfset var objectName = Replace(objectName, 'EntityType="Microsoft.Dynamics.CRM.', "")>
        <cfreturn trim(objectName)>
    </cffunction>

	<cffunction name="parsePermission">
	   <cfargument name="object" type="string" required="true">
	   <cfargument name="prop" type="string" required="true">
	   <cfargument name="metadata" type="Any" default="#getMData()#">

	   <!--- <cfset var propAnnotation = "#XMLSearch( metadata, "//Annotations[@Target='mscrm.contact/aging90_base']" )#" >  very good and very save but very slow--->
	   <cfset var objectPermission = parse(metadata, '<Annotations Target="mscrm.#object#\/#prop#">', '<\/Annotations>')>
	   <cfset var objectPermission = parse(objectPermission, '<Annotation Term="Org.OData.Core.V1.Permissions">', '<\/Annotation>')>
	   <cfset var objectPermission = parse(objectPermission, '<EnumMember>', '</EnumMember>')>
	   <cfset var objectPermission = Replace(objectPermission, 'Org.OData.Core.V1.PermissionType/', "")>

	   <cfreturn "Read" NEQ objectPermission >
	</cffunction>

    <cffunction name="parse" access="private" returnType="string">
        <cfargument name="text" type="string" required="true">
        <cfargument name="startTag" type="string" required="true">
        <cfargument name="endTag" type="string" required="true">

		<cfset var match = "">

	        <cftry>
	            <cfset var regExp = startTag & '(.*?)' & endTag>
				<cfset var matchArray = REMatch(regExp, text)>

				<cfif arrayLen(matchArray)>
			        <cfset match = Replace(matchArray[1], startTag, "")>
			        <cfset match = Replace(match, endTag, "")>
			     </cfif>

		        <cfcatch>
		            <cfoutput>FAILD<br></cfoutput>
					<cfdump
		                var="#regExp#"
		                top="100"
		                 />
					<cfreturn "">
		        </cfcatch>
	        </cftry>
        <cfreturn match>
    </cffunction>

    <cffunction name="parseSet" access="private" returnType="Any">
        <cfargument name="text" type="string" required="true">
        <cfargument name="startTag" type="string" required="true">
        <cfargument name="endTag" type="string" required="true">

        <cfset var regExp = startTag & '(.*?)' & endTag>
        <cfset var match = REMatch(regExp, text)>

        <cfreturn match>
    </cffunction>

    <cffunction name="getServerTime" access="public" returnType="string">
        <cfset var bToken = checkToken()>
        <cfset var urlRequest = "#ms.url#">
        <cfset var httpResult	= '' />
        <cfset var requestId = logRequest(requestString=urlRequest, object="", method='getServerTime')>
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>

        <cfif httpResult.Statuscode eq "200 OK">
            <cfset var time = httpResult.Responseheader.Date />
        </cfif>
        <cfset var time = DateAdd( "s", 0, time )>
        <cfset var time = Replace(time, "{ts '", '')>
        <cfset var time = Replace(time, ' ', 'T')>
        <cfset var time = Replace(time, "'}", 'Z')>

        <cfreturn time>
    </cffunction>

    <!---Get metadata about object or retrieve it from cache if it is already exists--->
    <cffunction name="getMData" access="public" returnType="string">
        <cfset var bToken = checkToken()>

        <cfset var metadata = CacheGet("metadata2") />

        <cfif not isDefined("metadata")>

            <cfset var urlRequest = "#ms.url & '/$metadata'#">
            <cfset var httpResult	= '' />
			<cfset var requestId = logRequest(requestString=urlRequest, object="", method='getMData')>
            <cfset var currentToken = getDynToken()>

            <cfhttp url="#urlRequest#" result="httpResult" method="GET">
                <cfhttpparam type="header" name="Content-Type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
            </cfhttp>
			<cfset var log = logResponse(serializeJSON(httpResult), requestId)>

            <cfif httpResult.Statuscode eq "200 OK">
	            <cfset var metadata = httpResult.Filecontent />
	            <cfset CachePut("metadata2", metadata, CreateTimespan( 2, 0, 0, 0) ) >
	        </cfif>
	    </cfif>

        <cfreturn metadata>
    </cffunction>

    <cffunction name="getData" access="private" returnType="string">
        <cfset var bToken = checkToken()>

        <cfset var data = CacheGet("data2") />
        <cfif not isDefined("data")>

            <cfset var urlRequest = ms.url>
            <cfset var httpResult	= '' />
            <cfset var currentToken = getDynToken()>
            <cfhttp url="#urlRequest#" result="httpResult" method="GET">
                <cfhttpparam type="header" name="Content-Type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
            </cfhttp>

            <cfset var data = httpResult.Filecontent />
            <cfset CachePut("data2", data, CreateTimespan( 2, 0, 0, 0) ) >
        </cfif>

        <cfreturn data>
    </cffunction>

    <!---get access token from DB--->
    <cffunction name="getDynToken" access="public" returntype="String">
        <cftry>
            <cfquery name="tokenGet" result="tokenResult">
             SELECT token from TokenTable where id =1
            </cfquery>
            <cfreturn #tokenGet.token#>

            <cfcatch>
                <cfquery name="createTokenTable">
                    create table [dbo].TokenTable(
                        id int,
                        token varchar(2000)
                    )
                    INSERT INTO TokenTable VALUES (1, 'token');
		        </cfquery>
                <cfreturn "token">
            </cfcatch>
        </cftry>
    </cffunction>

    <!---Set access token to DB--->
    <cffunction name="setToken" access="public" returntype="void">
        <cfargument name="token" type="string" required="true">

        <cfquery name="setToken">
              UPDATE TokenTable SET token=<cf_queryparam value="#token#" cfsqltype="cf_sql_varchar"> where id=1;
		</cfquery>
    </cffunction>

    <!---Refresh access token if it is expired--->
    <cffunction name="refreshToken" access="public" returntype="boolean">
        <cfset var responseBody = "grant_type=password&client_id=#client_id#&client_secret=#client_secret#&username=#azure.user.name#&password=#password#&resource=#root#">

        <cfset var result	= '' />
        <cfset var requestId = logRequest(requestString=refreshTokenUrl&chr(10)&chr(13)&responseBody, object="", method='refreshToken')>
        <cfhttp url="#refreshTokenUrl#" result="result" method="POST">
            <cfhttpparam type="formField" name="grant_type" value="password">
            <cfhttpparam type="formField" name="client_id" value="#client_id#">
            <cfhttpparam type="formField" name="client_secret" value="#client_secret#">
            <cfhttpparam type="formField" name="username" value="#azure.user.name#">
            <cfhttpparam type="formField" name="password" value="#password#">
            <cfhttpparam type="formField" name="resource" value="#root#">
        </cfhttp>

        <cfset var log = logResponse(serializeJSON(result), requestId)>
        <cfset var cfData = DeserializeJSON(result.Filecontent)>
        <cfif StructKeyExists(cfData, "access_token")>
            <cfset setToken(cfData.access_token)>
            <cfset var WhoAmI = setAPIuserID()>
            <cfset application.connector.API.userID = WhoAmI>
            <cfreturn true>
        </cfif>
        <cfreturn false>
    </cffunction>

    <cffunction name="setAPIuserID" access="public" returntype="String">
        <cfset var urlRequest = "#ms.url & 'WhoAmI()'#">
        <cfset var requestId = logRequest(urlRequest, 1, 'WhoAmI')>
        <cfset var httpResult	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#urlRequest#" result="httpResult" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfset var log = logResponse(serializeJSON(httpResult), requestId)>
        <cfset var cfData = DeserializeJSON(httpResult.Filecontent)>
        <cfreturn cfData.UserId>
    </cffunction>

    <cffunction name="testConnectionToRemoteSite" access="public" returntype="boolean">
        <cfreturn checkToken()>
    </cffunction>

    <!---Check if token expired--->
    <cffunction name="checkToken" access="public" returntype="boolean">
        <cfset var result	= '' />
        <cfset var currentToken = getDynToken()>
        <cfhttp url="#checkTokenUrl#" result="result" method="GET">
            <cfhttpparam type="header" name="Content-Type" value="application/json">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
        </cfhttp>
        <cfif #result.Statuscode# eq "401 Unauthorized">
            <cfset var rToken = refreshToken()>
            <cfreturn rToken>
        </cfif>
        <cfreturn true>
    </cffunction>


	<!--- TBD: Use the function in connector.cfc - this function is a duplicate --->
    <cffunction name="logRequest" access="public" returntype="string">
        <cfargument name="requestString" type="string" required="true">
        <cfargument name="object" type="string" required="true">
        <cfargument name="method" type="string" required="true">

        <cfset var insertRequest	= '' />

        <cfquery name="insertRequest">
			declare @connectorObjectID int
			select @connectorObjectID = ID from connectorObject where connectorType='msDynamics' and object=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">

            insert into connectorResponse (sendXML,responseXML,connectorObjectID,method) values (<cf_queryparam value="#xmlFormat(arguments.requestString)#" CFSQLTYPE="CF_SQL_VARCHAR" >,'',@connectorObjectID, <cf_queryparam value="#method#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			select SCOPE_IDENTITY() as ID
        </cfquery>
        <cfreturn insertRequest.ID[1]>
    </cffunction>

    <cffunction name="logResponse" access="public" returntype="void">
        <cfargument name="response" type="string" required="true">
        <cfargument name="objectId" type="numeric" required="true">
        <cfset var insertResponse	= '' />
        <cfset var result	= '' />
        <cfquery name="insertResponse" result="result">
            UPDATE connectorResponse SET responseXML =  <cf_queryparam value="#xmlFormat(arguments.response)#" CFSQLTYPE="CF_SQL_VARCHAR" > where id=#arguments.objectId#
        </cfquery>
    </cffunction>

	<cffunction name="converteMSDynamicsType">
        <cfargument name="msDynamicsType" type="string" required="true" hint="The ms dynamics type">
        <cfscript>
            var typemappingTable = {
                                    "null" = "NULL",
                                    "Edm.Guid" = "char(16)",
                                    "Edm.Binary" = "varbinary(max)",
                                    "Edm.Boolean" = "bit",
                                    "Edm.Byte" = "tinyint",
                                    "Edm.Int16" = "smallint",
                                    "Edm.SByte" = "smallint",
                                    "Edm.Int32" = "int",
                                    "Edm.Int64" = "bigint",
                                    "Edm.Single" = "single",
                                    "Edm.Double" = "double",
                                    "Edm.Decimal" = "decimal",
                                    "Edm.String" = "nvarchar(MAX)",
                                    "Edm.Time" = "time(7)",
                                    "Edm.DateTime" = "datetime",
                                    "Edm.DateTimeOffset" = "datetimeoffset(7)"
                                  };
            return typemappingTable[msDynamicsType];
        </cfscript>
    </cffunction>

<!--- case when type in ('phone','email','id','string') then 'text' when type like 'date%' then 'date' when type in ('int','double','currency') then 'numeric' else type end as dataType --->
	<cffunction name="convertMSDynamicsTypeToRwType">
        <cfargument name="msDynamicsType" type="string" required="true" hint="The ms dynamics type">
		<cfargument name="object" type="string" required="true">
        <cfargument name="prop" type="string" required="true">
        <cfargument name="metadata" type="Any" default="#getMData()#">
        <cfscript>
            var typemappingTable = {
                                    "Edm.Guid" = "reference",
                                    "Edm.Binary" = "binary",
                                    "Edm.Boolean" = "boolean",
                                    "Edm.Byte" = "numeric",
                                    "Edm.Int16" = "numeric",
                                    "Edm.SByte" = "numeric",
                                    "Edm.Int32" = "numeric",
                                    "Edm.Int64" = "numeric",
                                    "Edm.Single" = "numeric",
                                    "Edm.Double" = "numeric",
                                    "Edm.Decimal" = "numeric",
                                    "Edm.String" = "text",
                                    "Edm.Time" = "date",
                                    "Edm.Date" = "date",
                                    "Edm.DateTime" = "date",
                                    "Edm.DateTimeOffset" = "date"
                                  };

            var mappingType = typemappingTable[msDynamicsType];
            if((msDynamicsType EQ "Edm.Guid") AND isPrimaryKey(object, prop, metadata)){
                mappingType = "text";
            }
            return mappingType;
        </cfscript>
    </cffunction>

    <cffunction name="isPrimaryKey">
       <cfargument name="object" type="string" required="true">
       <cfargument name="prop" type="string" required="true">
       <cfargument name="metadata" type="Any" default="#getMData()#">
       <cfset var objectPermission = parse(metadata, '<EntityType Name="#object#"', '<\/EntityType>')>
	   <cfset var objectPermission = parse(objectPermission, '<Key>', '</Key>')>
	   <cfset var objectPermission = parse(objectPermission, '<PropertyRef Name="', '"')>

	   <cfreturn objectPermission EQ prop>
    </cffunction>

    <cffunction name="getEntityByIdName">
        <cfargument name="idName" type="string" required="true">
        <cfargument name="metadata" type="Any" default="#getMData()#">
        <cfset var entityName = parse(metadata, '<NavigationPropertyBinding Path="#idName#" Target="', '/>')>
        <cfset var entityName = Replace(entityName, '"', "")>
        <cfset var entityName = Replace(entityName, ' ', "")>

        <cfreturn entityName>
    </cffunction>

    <cffunction name="getRemoteObjects" access="public" output="false" returnType="struct" hint="Returns a query of msdynamics objects used to populate the connectorObject table">
        <cfargument name="UI" default="false">
        <cftry>
            <cfset var data = getData() />
            <cfset var serializedStr = DeserializeJSON(data)>
            <cfset var numOfObjects = ArrayLen(serializedStr.value)>
            <cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure(tempTable=true)>

            <cfif numOfObjects>
                <cfquery name="insertObjectNamesIntoTempTable">
					select name into #result.successResult.tablename# from
					(values
		            <cfloop from="1" to="#numOfObjects#" index="i">
                    ('#serializedStr.value[i].name#') <cfif i lt numOfObjects>,</cfif>
                </cfloop>
                    ) as objectNames (name)
                </cfquery>
            </cfif>

            <cfif arguments.UI>
                <cfset var objectNames = ArrayNew(1)>
                <cfloop from="1" to="#ArrayLen(serializedStr.value)#" index="i">
                    <cfset temp = ArrayAppend(objectNames, #serializedStr.value[i].name#)>
                </cfloop>
                <cfset var result.successResult = objectNames>
            </cfif>

            <cfcatch>
                <cfset var result.isOK = false>
            </cfcatch>
        </cftry>
        <cfreturn result>
    </cffunction>

    <cffunction name="getPickListIfExists" access="public" returnType="string" >
       <cfargument name="entityName" type="string" required="true">
       <cfargument name="entityColumn" type="string" required="true">
       <cfargument name="type" type="string" required="true">
       <cfscript>
           if("edm.int32" EQ type){
              return getPickList(entityName, entityColumn);
           }
           return "";
       </cfscript>
    </cffunction>

	<cffunction name="getPickList" access="public" returnType="string">
		<cfargument name="objectName" type="string" required="true">
        <cfargument name="column" type="string" required="true">
		<cfscript>
			//CacheRemove("pickList"); //clean picklist cache (only for dev purposes)
    		var pList = getCachePickList(objectName, column);
    		if("NULL" EQ pList){
    			var optionSet = getMsServerOptionSet(objectName, column);
    			var pList = optionSetIntoPickList(optionSet);
    			setCachePickList(objectName, column, pList);
    			pList = getCachePickList(objectName, column);
    		}
    		return pList;
		</cfscript>
	</cffunction>

    <cffunction name="getCachePickList" access="private" returnType="string">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="column" type="string" required="true">

        <cfset var msPickList = CacheGet("pickList") />
        <cfif isDefined("msPickList") and StructKeyExists(msPickList, "#arguments.objectName#") and StructKeyExists(msPickList[#arguments.objectName#], "#arguments.column#")>
            <cfreturn msPickList[#arguments.objectName#][#arguments.column#]>
        </cfif>
        <cfreturn "NULL" >
    </cffunction>

    <cffunction name="setCachePickList" access="private" returnType="void">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="column" type="string" required="true">
        <cfargument name="pickListObj" type="string" required="true">

        <cfset var msPickList = CacheGet("pickList") />

        <cfif not isDefined("msPickList")>
            <cfset var msPickList = structNew()>
        </cfif>

        <cfif not StructKeyExists(msPickList, "#arguments.objectName#")>
            <cfset var temp = structInsert(#msPickList#, "#arguments.objectName#", structNew())>
        </cfif>

		<cfif not StructKeyExists(msPickList[#arguments.objectName#], "#arguments.column#")>
            <cfset var temp = structInsert(#msPickList[arguments.objectName]#, "#arguments.column#", #arguments.pickListObj#)>
		</cfif>
        <cfset CachePut("pickList", #msPickList#, CreateTimespan(10, 0, 0, 0) ) >
    </cffunction>

    <cffunction name="getMsServerOptionSet" access="private" hint="Retrive optionSet from MS Dynamics server and wrap into object">
	    <cfargument name="entityLogicalName" type="string" required="true">
        <cfargument name="columnLogicalName" type="string" required="true">
		<!-- <b:value i:nil="true" /> -->
		   <cfset var req = '
				<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
				    <soapenv:Body>
				        <Execute xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
				            <request i:type="a:RetrieveAttributeRequest" xmlns:a="http://schemas.microsoft.com/xrm/2011/Contracts">
				                <a:Parameters xmlns:b="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
				                    <a:KeyValuePairOfstringanyType>
				                        <b:key>EntityLogicalName</b:key>
				                        <b:value i:type="c:string" xmlns:c="http://www.w3.org/2001/XMLSchema">#entityLogicalName#</b:value>
				                    </a:KeyValuePairOfstringanyType>
				                    <a:KeyValuePairOfstringanyType>
				                        <b:key>MetadataId</b:key>
				                        <b:value i:type="ser:guid" xmlns:ser="http://schemas.microsoft.com/2003/10/Serialization/">00000000-0000-0000-0000-000000000000</b:value>
				                    </a:KeyValuePairOfstringanyType>
				                    <a:KeyValuePairOfstringanyType>
				                        <b:key>RetrieveAsIfPublished</b:key>
				                        <b:value i:type="c:boolean" xmlns:c="http://www.w3.org/2001/XMLSchema">true</b:value>
				                    </a:KeyValuePairOfstringanyType>
				                    <a:KeyValuePairOfstringanyType>
				                        <b:key>LogicalName</b:key>
				                        <b:value i:type="c:string" xmlns:c="http://www.w3.org/2001/XMLSchema">#columnLogicalName#</b:value>
				                    </a:KeyValuePairOfstringanyType>
				                </a:Parameters><a:RequestId i:nil="true" />
				                <a:RequestName>RetrieveAttribute</a:RequestName>
				            </request>
				        </Execute>
				    </soapenv:Body>
				</soapenv:Envelope>'/>
		<cfset checktoken() />
        <cfset var currentToken = getDynToken()>
		<cfhttp url="#matadataUrl#" result="response"  method="POST">
            <cfhttpparam type="header" name="Content-Type"  value="text/xml; charset=utf-8">
            <cfhttpparam type="header" name="Authorization" value="Bearer #currentToken#">
			<cfhttpparam type="header" name="Accept"        value="application/xml, text/xml, */*">
			<cfhttpparam type="header" name="SOAPAction"    value="http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute">
            <cfhttpparam type="body" value="#req#">
        </cfhttp>
		<cfif response.statusCode eq "200 OK">
			<cfset var optionSet = parseOptionSet(response.Filecontent) >
            <cfreturn optionSet>
		</cfif>
	    <cfreturn [] />
	</cffunction>

	<cffunction name="parseOptionSet" access="private" returntype="array">
       <cfargument name="optionSetXml" type="string" required="true">

       <cfset var options = parse(optionSetXml, '<c:Options>', '<\/c:Options>')>
       <cfset var regExp = '<c:OptionMetadata>(.*?)</c:OptionMetadata>'>
	   <cfset var optionArray = REMatch(regExp, options)>
	   <cfset var optionsArr = [] />
	   <cfloop array="#optionArray#" index="optionMeta">
		    <cfset var optObj = {} />
		    <cfset optObj.value = parse(optionMeta, '<c:Value>', '<')>
			<cfset optObjt.label = parse(optionMeta, '<a:LocalizedLabel>', '<\/a:LocalizedLabel>')>
			<cfset optObj.label = parse(optionMeta, '<a:Label>', '<')>
			<cfset arrayappend(optionsArr, optObj)>
       </cfloop>
       <cfreturn optionsArr>
    </cffunction>

    <cffunction name="optionSetIntoPickList" access="private" returntype="string">
       <cfargument name="optionsArr" type="array" required="true">
       <cfscript>
		if(!ArrayIsEmpty(optionsArr)){
		   var pickList = "";
           for(opt in optionsArr){
                pickList &= "<picklistValues>
                                <label>#opt.label#</label>
								<value>#opt.value#</value>
                             </picklistValues>";
           };
           return pickList;
		}
	    return "";
	   </cfscript>
    </cffunction>


	<cffunction name="matchObjectRecords" access="public" output="false" returnType="struct" hint="A function to perform a query against dynamics to find matches for a given object. It is run in a thread.">
		<cfargument name="whereClause" type="string" required="true">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objectIDColumn" type="string" required="true">

		<cfset var result = {isOK=true,message="",successResult={}}>
		<cfset result.successResult.tablename="">

		<cfset var matchingWhereClause = replace(arguments.whereClause,"=","eq","ALL")> <!--- replace equal sign with eq --->
		<cfset matchingWhereClause = REReplace(matchingWhereClause,"'([a-fA-F0-9]{8}-([a-fA-F0-9]{4}-){3}[a-fA-F0-9]{12})'","\1")> <!--- need to remove single quotes from around the GUID --->
		<cfset matchingWhereClause = replaceNoCase(matchingWhereClause,"_parentaccountid_value eq ''","_parentaccountid_value eq null")> <!--- special case for empty parentAccountId... should be a GUID, so an empty string is considered null --->

		<cfset var objectReturn = getByClause(object=arguments.object,clause=matchingWhereClause,fieldList=arguments.objectIDColumn,orderBy="modifiedon desc")>
		<cfset var table = application.getObject("connector.com.connectorUtilities").getTempTableName(TEMPTABLESUFFIX=request.TEMPTABLESUFFIX)>

		<cfif structKeyExists(objectReturn,"data")>
			 <cfset var result = daoService.importDataToDBBulk(objectName=arguments.object, object=objectReturn.data.value,temptable=table,connectorResponseId=objectReturn.responseID,fieldList=arguments.objectIDColumn)>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="fetchNextPage" access="private" output="false" returnType="struct" hint="Returns the page number to collect based on the last response, and other attributes needed such as the cookie">
		<cfargument name="response" type="string" required="true">

		<cfset var fetchResult = {pageNumber=0,pagingCookie=""}>
		<cfset fetchResult.pageNumber = parse(arguments.response, ' pagenumber="', '"')>

		<!--- The page number in the paging cookie seems to be 1 for every request although it comes back in the response as 2. Haven't quite worked it out. So replacing it here --->
		<cfset fetchResult.pagingCookie = replace(xmlFormat(replaceNoCase(URLDecode(URLDecode(parse(arguments.response, ' pagingcookie="', '"'))),'page="2"','page="1"')),'&','%26','ALL')>

		<cfreturn fetchResult>
	</cffunction>

</cfcomponent>