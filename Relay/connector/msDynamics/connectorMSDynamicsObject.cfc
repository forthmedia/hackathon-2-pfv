<!--- �Relayware. All Rights Reserved 2015 --->
<!---
File name:		connectorMsdynamicsObject.cfc
Author:			NJH
Date started:	23-01-2015

Description:	The Msdynamics Connector

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/01/23			NJH			Fixed bug where opportunityContactRoles/partners were not getting deleted; Also delete deleted orphaned from the base data table so that they're not stuck in the queue.
2015/04/13			NJH			Several small changes. Filter out orgs from the import view that don't have a country set so that we only process orgs we know we can import. Fix problem in matching code if the RW ID was not the same for every object.
								Also added organisationType='AdminCompany' to sql join when matching user organisations.
2015/06/10			NJH			Changed dependency column delimiter from underscore to bent pipe
2015/07/02			NJH			Broke up the matchRecordsForExport function so that we can specify which columns we want to use for matching. Also created function that returned the org fields to synch based on the synching location fields if using account-location mapping. Needed
								these fields in various places.
2015/07/13			NJH			Set dataExists column to 0 where data no longer exists on SF. This is used for reporting and prevents records from sitting forever in the queue.
								Also changed the underlying view from vConnectorSynchingRecords to vConnectorRecords

Possible enhancements:


 --->


<cfcomponent extends="relay.connector.connectorObject" output="false">

	<cfparam name="request.ignoreTime" default="false"> <!--- set to true if doing a bulk import. This effectively removes the time constraint on the where clause --->

	<cfset this.connectorType="msDynamics">
	<cfset this.api = createObject("component","relay.connector.msDynamics.connectormsDynamicsAPI")>

	<cffunction name="MsdynamicsInit" access="private" output="false" hint="Initialises things specific to the msDynamics connector">

		<cfset this.objectPrimaryKey = this.api.parseMsEntityName(object=lcase(this.baseObject))&"id"> <!--- TODO: this may need to come from the mappings table so that it is not stuck in the code, particularly if we are mapping a new object --->
		<cfset request.tempTableSuffix = this.tempTableSuffix> <!--- setting this in request scope so that it is available in the API cfc --->

		<!--- TODO: put this in application scope?? in application.MDAPI --->
		<!--- <cfif not structKeyExists(request,"remoteServerTimeOffsetInSeconds")>
			<cfset request.remoteServerTimestamp = replaceNoCase(replaceNoCase(this.api.send(method="getServerTimestamp",object="user").serverTimestamp,"T"," "),"Z","")>
		<cfif not structKeyExists(application,"MDAPI") or not structKeyExists(application.MDAPI,"remoteServerTimestamp")>
			<cfset application.MDAPI.remoteServerTimestamp = replaceNoCase(replaceNoCase(this.api.send(method="getServerTimestamp",object="user").serverTimestamp,"T"," "),"Z","")>
		</cfif>
		 --->
		<!---<cfset request.remoteServerTimestamp = now()>--->
	</cffunction>


	<cffunction name="getTransformationViewWhereClause" access="private" output="false" returnType="string">

		<cfset var whereClause = "">
		<!--- if we are synching (exporting) leads, then we don't want to export opportunties that have been created as a result of a lead conversion and that don't yet have a SF ID.
			Essentially, we want to convert the lead on SF, and then set the converted RW opp to have the resulting SF OppId as its crmOppID
		 --->
		<cfif this.baseEntity eq "opportunity" and this.direction eq "export" and getConnectorObject(object_relayware="lead").synch>
			<cfset whereClause = "and not (opportunity.convertedFromLeadID is not null and opportunity.crmOppId is null)">
		</cfif>

		<cfreturn whereClause>
	</cffunction>


	<cffunction name="testConnectionToRemoteSite" access="public" output="false" returnType="boolean" hint="Runs a test query against the remote site to ensure that we can connect to it">

		<cfset var connectionOk = this.api.refreshToken()>
		<cfset var serverOnlineResponse = {isOK=false}>

		<cfif not connectionOk>
			<cfmail to="nathaniel.hogeboom@relayware.com" from="nathaniel.hogeboom@relayware.com" subject="MSDynamics Test Connection Failed on #request.currentSite.domainAndRoot#" type="html">
				<cfdump var="#serverOnlineResponse#">
			</cfmail>
		</cfif>

		<cfreturn connectionOk>
	</cffunction>


 	<cffunction name="getJoinToRelaywareTableForPricebook" access="private" output="false" hint="Provides the join needed to import pricebooks, as it's not a straightforard mapping, due to the pricebook crmID appearing multiple times, once for each currency" returnType="struct">
		<cfargument name="importTableName" type="string" required="true">

		<cfset var importPricebookEntryTable = "#this.dbObjectPrefix##getMappedObject(object_relayware='pricebookEntry')##this.dbObjectSuffix#">
		<cfset var pricebookLevelCurrencyMappedField = getMappedField(column_relayware="Currency",object_relayware="pricebookEntry")>
		<cfset var currencyEnabled = pricebookLevelCurrencyMappedField.active>
		<cfset var pricebookIDMappedField = getMappedField(column_relayware="pricebookID",object_relayware="pricebookEntry").field>
		<cfset var pricebookEntryTableForJoin = currencyEnabled?"(select distinct #pricebookIDMappedField#,#pricebookLevelCurrencyMappedField.field# from #importPricebookEntryTable#)":"#importPricebookEntryTable#">
		<cfset var result = {join="left outer join #pricebookEntryTableForJoin# pbe on pbe.#pricebookIDMappedField# = #arguments.importTableName#.#this.objectPrimaryKey# left outer join currency on currency.r1CurrencyID = pbe.#pricebookLevelCurrencyMappedField.field# left outer join #this.baseEntity# on #this.baseEntity#.#this.baseEntityObjectKey# = #arguments.importTableName#.#this.objectPrimaryKey#"}>

		<!--- if multi-currency is not enabled, then don't join to the currency column as it won't exist --->
		<cfif currencyEnabled>
			<cfset result.join = result.join & " and #this.baseEntity#.currency = currency.currencyIsoCode">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="buildBaseImportTable" access="private" output="false" hint="Overridden the base function in connectorUtilitiesObject.cfc because the pricebookEntry table needs creating before the pricebook table, which is non-standard.">
		<cfargument name="object" type="string" required="true">

		<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(connectorType=this.connectorType,object=arguments.object)> <!--- This ensure that all the objects that we care about are in memory --->

		<cfif this.baseEntity eq "pricebook">
			<cfset super.buildBaseImportTable(object=getMappedObject(object_relayware='pricebookEntry'))>
		</cfif>

		<!--- don't need to call this again for pricebookentry, as it has already been called when dealing with pricebooks --->
		<cfif this.baseEntity neq "productpricelevels">
			<cfset super.buildBaseImportTable(object=arguments.object)>
		</cfif>

	</cffunction>


 	<cffunction name="getRequiredMsdynamicsObjectFields" access="private" output="false" returnType="struct" hint="Returns a structure of required Msdynamics base table fields needed for importing an object">
		<cfargument name="object" type="string" default="#this.mappedObject#" hint="The SF object that we're getting required fields for">

		<cfset var requiredFieldStruct = {}>

		<!--- could maybe do this a bit better, but for now have hard-coded it --->
		<cfif listFindNoCase("opportunityContactRole,opportunityPartner,accountPartner",arguments.object)>
			<cfset requiredFieldStruct.role = "varchar(50)">
		</cfif>

		<cfreturn requiredFieldStruct>
	</cffunction>


	<cffunction name="generateTransformationViewSql" access="private" output="false" returnType="string" hint="Returns the sql statement for the transformation view">

		<cfset var view_sql = "">

		<!--- have to work out a special query of the opportunityPartner and opportunityContactRole objects, as we have roles mapped to values. This means that our export views consists of a number of unions
			for the given roles
		--->
		<cfif listFindNoCase("opportunityPartner,opportunityContactRole",arguments.object)>

			<cfset var theSelectColumn = "">
			<cfset var selectDependency = "">
			<cfset var selectColumnJoin = "">
			<cfset var whereClause = getTransformationViewWhereClause()>

			<!--- the same where clause as give to opportunities. we don't want to export items where the opps haven't changed or are suspended. Again, might be a better' --->
			<cfprocessingdirective suppresswhitespace="yes">
				<cfsavecontent variable="view_sql">
					<cfoutput>
						<cfloop query="arguments.transformationInfo">
							<cfset theSelectColumn = "#joinToEntityTable#.#selectColumn#">
							<cfset selectDependency = "#joinToEntityTable#.#joinToEntityTableUniqueKey# as export_#asColumn#_#replaceNoCase(joinToEntityTable,"v","")#">
							<cfset selectColumnJoin = "left join #joinToEntityTable# on #joinToEntityTableUniqueKey# = #objectType#.#joinBaseTableColumn#">

							<cfset var fieldJoinInfo = getFieldMappingJoinInfo(columnName=selectColumn,object="opportunity")>
							<cfif fieldJoinInfo.overrideExists>
								<cfset theSelectColumn = fieldJoinInfo.select>
								<cfset selectDependency = fieldJoinInfo.selectDependency>
								<cfset selectColumnJoin = fieldJoinInfo.join>
							</cfif>
							select #theSelectColumn# as #asColumn#,
								#selectDependency#,
								#this.baseEntityObjectKey# as #this.object#ID,
								#objectType#.#this.baseEntityPrimaryKey# as #getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field#, #replace(objJoin,"role=","")# as role,
								case when #joinBaseTableColumn neq ""?joinBaseTableColumn:selectColumn# is null then 1 else q.isDeleted end as isDeleted,
								null as connectorResponseID
							from vConnectorRecords q
								inner join #objectType# ON cast(#objectType#.opportunityID AS varchar) = q.objectID and q.object = '#objectType#' and q.connectorType = '#this.connectorType#'
								<!---left join booleanFlagData suspended on suspended.entityID = opportunity.opportunityID and suspended.flagID = #application.com.flag.getFlagStructure(flagID="OpportunitySuspended").flagID#--->
								#selectColumnJoin#
							where
								<!---suspended.entityId is null--->
								queueStatus = 'synching'
								#preserveSingleQuotes(whereClause)#
							<cfif currentRow neq arguments.transformationInfo.recordCount>
								union
							</cfif>
						</cfloop>
					</cfoutput>
				</cfsavecontent>
			</cfprocessingdirective>

		<cfelse>
			<cfset view_sql = super.generateTransformationViewSql(argumentCollection=arguments)>

			<!--- if we're importing an organisation, then we we want to filter the accounts to only show the HQ accounts, as these make up the organisations --->
			<cfif this.direction eq "import" and  arguments.object eq "organisation" and this.baseEntity neq "organisation">

				 <!--- organisations are those accounts where the parentID is null. We don't process organisation deletions, so remove them from the view. We also remove organisations that don't have a country as these will fail import. --->
				 <!--- TODO: probably good to create a function that would get me the mapping, whether it's core or overridden --->
				 <cfset var orgCountryIDJoinInfo  = getFieldMappingJoinInfo(columnName="countryID",object="organisation")>
				 <cfset view_sql = view_sql& " and isNull(#replace(this.objectDataTable,'connector_','')#._parentaccountid_value,'') = '' and q.isDeleted = 0 and #orgCountryIDJoinInfo.overrideExists?orgCountryIDJoinInfo.select:'country.countryID'# is not null">
			</cfif>
		</cfif>

		<cfreturn view_sql>
	</cffunction>


	<cffunction name="preProcess_ImportProductPriceLevels" access="private" output="false" hint="Import pricebookEntries for the given pricebooks, as we need pricebookEntry currencies to import pricebooks" direction="import" object="priceLevels" returnType="struct">

		<cfset var result = {isOK=true,message=""}>
		<cfset var mappedPricebookEntryObject = getMappedObject(object_relayware="pricebookEntry")>
		<cfset var pricebookEntries = getDataToImport(object=mappedPricebookEntryObject)>
		<cfset getDataForObjectsInQueue(object=mappedPricebookEntryObject,getDataForRecordsWithDependencies=true)> <!--- we may need to get data for items already in the queue... --->

		<cfreturn result>
	</cffunction>


	<cffunction name="preProcess_ImportProductGroups" access="private" output="false" hint="Import any missing product groups." direction="import" object="product" returnType="struct">

		<cfset var result = {isOK=true,message=""}>
		<cfset var mappedProductGroupField = getMappedField(object_relayware=this.baseEntity,column_relayware="productGroupID").field>
		<cfset var getNewProductGroups = "">
		<cfset var tmpProductGroup = getTempTableName()>
		<cfset var createFlagView = "">

		<cfif mappedProductGroupField neq "">
		<!--- get any missing product groups --->
		<cfquery name="getNewProductGroups">
			select distinct ROW_NUMBER() over (order by p.#mappedProductGroupField#) AS upsertUniqueID,
				null as productGroupID,
				ltrim(rtrim(p.#mappedProductGroupField#)) as description_defaultTranslation,
				ltrim(rtrim(p.#mappedProductGroupField#)) as title_defaultTranslation,
				ltrim(rtrim(p.#mappedProductGroupField#)) as productGroupName
			into #tmpProductGroup#
			from
				(select distinct #mappedProductGroupField# from #this.objectDataTable#) p
				left join productGroup pg on ltrim(rtrim(pg.description_defaultTranslation)) = ltrim(rtrim(p.#mappedProductGroupField#))
			where pg.description_defaultTranslation is null
				and ltrim(rtrim(p.#mappedProductGroupField#)) != ''

			select * from #tmpProductGroup#
		</cfquery>

		<cfset var logStruct = {tablename=tmpProductGroup}>
		<cfset log_(label="Import Product Groups",logStruct=logStruct)>

		<cfif getNewProductGroups.recordCount>
			<cfquery name="createFlagView">
				exec createFlagView @entityName = 'ProductGroup', @viewName = 'vProductGroup'
			</cfquery>
			<cfset upsertEntity(tablename=tmpProductGroup,entityType="productGroup")>
		</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


    <cffunction name="preProcess_ImportMsDynCurrency" access="public" direction="import">

		<cfset var mappedCurrencyField = getMappedField(object_relayware=this.baseEntity,column_remote="_transactioncurrencyid_value")>
		<cfparam name="request.currenciesUpdated" default="false">

		<cfif mappedCurrencyField.field neq "" and mappedCurrencyField.active and not request.currenciesUpdated>

	        <cfscript>
			  var msDynCurrencyData = this.api.getAll("transactioncurrencies");
			  var daoService = createObject("component","relay.connector.msDynamics.MSDynamicsDAOService");

			  for(currency in msDynCurrencyData.data.value){
			  	  daoService.importCurrencyToDb(currency.isocurrencycode, currency.currencyname, currency.currencysymbol, currency.transactioncurrencyid);
			  }
			  request.currenciesUpdated = true;
			</cfscript>
		</cfif>

		<cfreturn {isOK=true,message=""}>
	</cffunction>

	<!----
		**************************************************************************************************************
		POST-PROCESSING FUNCTIONS
		**************************************************************************************************************
	--->

	<cffunction name="collateSuccessResponsesForNonPrimaryObjectExport" access="private" output="false" hint="Collate the success responses for the opportunityContactRole and partner objects primarily. But give chance to override for other objects">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table to populate with successful records">
		<cfargument name="object" type="string" required="true" hint="The name of the object whose data we are exporting">
		<cfargument name="resultTableName" type="string" required="true" hint="The name of the table containing the export result">
		<cfargument name="exportDataTableName" type="string" required="true" hint="The name of the table containing the data to export">
		<cfargument name="rowNumberIndex" type="numeric" required="true" hint="The rows to process">
		<cfargument name="relatedColumn_remote" type="string" required="true" hint="The name of the column holding the relationship to the related object (ie: either contactId or accountID)">


		<cfset var collateResponses = "">
		<cfset var entityIDMappedField = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field>
		<cfset var primaryObjectIdColumn = getConnectorObject(object=arguments.object,relayware=false).parentKeyField>

		<cfquery name="collateResponses">
			insert into #arguments.tablename# (#this.baseEntityPrimaryKey#,#this.baseEntityObjectKey#,action)
			select distinct ev.#entityIDMappedField#,v.#primaryObjectIdColumn#, 'update' as [action]
			from #arguments.resultTableName# t
				inner join #arguments.exportDataTableName# v on t.rowNumber + (#arguments.rowNumberIndex#) = v.rowNumber
				inner join #getViewForObjectSynch(object=arguments.object)# ev on ev.opportunityID = v.opportunityID and ev.role=v.role and ev.#arguments.relatedColumn_remote# = v.#arguments.relatedColumn_remote#
				left join #arguments.tablename# s on s.#this.baseEntityPrimaryKey# = ev.#entityIDMappedField# and s.#this.baseEntityObjectKey#=v.#primaryObjectIdColumn#
			where success = 1
				and s.action is null
		</cfquery>

	</cffunction>


	<cffunction name="collateErrorResponsesForNonPrimaryObjectExport" access="private" output="false" hint="Collate the error responses for the opportunityContactRole and partner objects primarily. But give chance to override for other objects">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table to populate with error records">
		<cfargument name="object" type="string" required="true" hint="The name of the object whose data we are exporting">
		<cfargument name="resultTableName" type="string" required="true" hint="The name of the table containing the export result">
		<cfargument name="exportDataTableName" type="string" required="true" hint="The name of the table containing the data to export">
		<cfargument name="rowNumberIndex" type="numeric" required="true" hint="The rows to process">
		<cfargument name="relatedColumn_remote" type="string" required="true" hint="The name of the column holding the relationship to the related object (ie: either contactId or accountID)">

		<cfset var collateResponses = "">
		<cfset var entityIDMappedField = getMappedField(object_relayware=this.baseEntity,column_relayware=this.baseEntityPrimaryKey).field>
		<cfset var primaryObjectIdColumn = getConnectorObject(object=arguments.object,relayware=false).parentKeyField>

		<cfquery name="collateResponses">
			insert into #arguments.tablename# (object,objectId,message,field,value,connectorResponseID)
			select distinct '#this.baseEntity#', cast(cast(ev.#entityIDMappedField# as float) as int), message, isNull(t.fields,cm.column_relayware), ev.#arguments.relatedColumn_remote#,t.connectorResponseID
			from #arguments.resultTableName# t
				inner join #arguments.exportDataTableName# v on t.rowNumber + (#arguments.rowNumberIndex#) = v.rowNumber
				inner join #getViewForObjectSynch(object=arguments.object)# ev on ev.opportunityID = v.opportunityID and ev.role=v.role and ev.#arguments.relatedColumn_remote# = v.#arguments.relatedColumn_remote#
				left join connectorColumnValueMapping cvm on cvm.[value_remote] = ev.role
				left join connectorMapping cm on cvm.connectormappingID = cm.ID
			where success = 0
		</cfquery>

	</cffunction>


	<cffunction name="deleteNonPrimaryObjectsForChangedPrimaryObjects" access="private" output="false" hint="Delete non-primary objects based on changes to primary objects. Essentially the opportunityContactRole and partner objects">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="object" type="string" required="true">

		<cfset var getPrimaryObjectIds = "">
		<cfset var getRolesToDelete = "">
		<cfset var nonPrimaryObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var nonPrimaryObjectWhereClause = nonPrimaryObject.whereClause>
		<cfset var primaryObjectID = "">
		<cfset var getPrimaryObjectIdsResult = structNew()>

		<!--- we need to delete the non-primary records on a record by record basis. We can't do a blanket delete, because each object is going to be different as to what will need to be deleted --->
		<cfquery name="getPrimaryObjectIds" result="getPrimaryObjectIdsResult">
			select distinct #nonPrimaryObject.parentKeyField# as primaryObjectID from #arguments.tablename# where len(isNull(#nonPrimaryObject.parentKeyField#,'')) > 0
		</cfquery>

		<cfset log_(label="Export (delete): delete Non-Primary Objects For Changed Primary Objects",logStruct=getPrimaryObjectIdsResult)>

		<cfloop query="getPrimaryObjectIds">
			<cfset var filter = nonPrimaryObjectWhereClause>

			<!--- if the object has a role field (will be opportunityContactRole or partner object), then we want to find the specific roles that have changed and therefore that we have to delete. If none exist in the incoming table,
				that means that all the roles have been deleted, and so we want to find all the mapped roles so that we can delete them
			 --->
			<cfif structKeyExists(application.connector.msdynamics.objectMetaData[arguments.object],"role")>

				<cfset var rolesToDeleteResult = structNew()>

				<!--- delete any specific roles --->
				<cfquery name="getRolesToDelete" result="rolesToDeleteResult">
					select distinct role from #arguments.tablename# where opportunityID='#getPrimaryObjectIds.primaryObjectID[currentRow]#'
				</cfquery>

				<cfif not getRolesToDelete.recordCount>

					<!--- delete any records for roles that we have mapped. Here, get the distinct list of roles that we have mapped --->
					<cfquery name="getRolesToDelete" result="rolesToDeleteResult">
						select distinct cvm.[value_remote] as role from connectorColumnValueMapping cvm
							inner join vConnectorMapping m on m.ID = cvm.connectorMappingID
						where cvm.value_relayware='role'
							and m.object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
							and export=1
					</cfquery>
				</cfif>

				<cfset log_(label="Export (delete): Roles To Delete",logStruct=rolesToDeleteResult)>

				<!--- if we have mapped roles, then filter the deletion to the concerned roles --->
				<cfif getRolesToDelete.recordCount>
					<cfset filter = filter & "#filter eq '' ? '':' and '#" & "role in (#quotedValueList(getRolesToDelete.role)#)">
				</cfif>
			</cfif>

			<cfset deleteNonPrimaryRecordsForPrimaryObject(object=arguments.object,primaryObjectID=getPrimaryObjectIds.primaryObjectID[currentRow],filter=filter)>
		</cfloop>
	</cffunction>


	<cffunction name="getExportQueryInfoForNonPrimaryObjects" access="private" output="false" returnType="struct" hint="Gets the select list and mappedParentKey for non-primary objects (opportunityContactRole and opportunityParter are examples)">
		<cfargument name="object" type="string" required="true" hint="The name of the msdynamics object">

		<cfset var parentKeyField = getConnectorObject(object=arguments.object,relayware=false).parentKeyField>
		<cfset var result = {selectColumnList=parentKeyField,parentKeyField=parentKeyField,relatedColumn_remote=""}>
		<cfset var getRelatedColumn_remote = "">

		<!--- this gets the accountToID or contactId columns, depending on the object --->
		<cfquery name="getRelatedColumn_remote">
			select distinct column_remote from vConnectorMapping where connectorType=<cf_queryparam value="#this.connectorType#" cfsqltype="cf_sql_varchar"> and object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfif getRelatedColumn_remote.recordCount eq 1>
			<cfset result.relatedColumn_remote = getRelatedColumn_remote.column_remote[1]>
		</cfif>

		<!--- in the case of a non primary object, we only do creates. --->
		<cfset result.selectColumnList = listAppend(result.selectColumnList,valueList(getRelatedColumn_remote.column_remote))> <!--- TODO: remove this hard-coded list --->

		<!--- dealing with the objects that have a role - again, primarily the opportunityContactRole and Partner objects --->
		<cfif structKeyExists(application.connector.msdynamics.objectMetaData[arguments.object],"role")>
			<cfset result.selectColumnList = listAppend(result.selectColumnList,"role")>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="getDataToImportFromWhereClause" access="private" output="false" hint="Gets data meeting criteria of the where clause from the Remote system and puts it into the data table" returnType="struct">
		<cfargument name="object" type="string" default="#this.object#" hint="The name of the remote object">
		<cfargument name="whereClause" type="string" required="true" hint="The where clause criteria">
		<cfargument name="fieldList" type="string" required="true" hint="The list of fields to get data for.">

		<cfreturn this.api.send(method = "query", object=arguments.object,clause=arguments.whereClause,fieldList=arguments.fieldList)>
	</cffunction>


	<cffunction name="getFullWhereClauseForObjectImportQuery" access="private" output="false" hint="Returns the query string to be used in where clause for object import" returnType="string">
		<cfargument name="object" type="string" required="true" hint="The Msdynamics object">

		<cfset whereClauseStruct = getWhereClauseForObjectImport(object=arguments.object)>

		<cfreturn whereClauseStruct.timeDateWhereClause & whereClauseStruct.objectWhereClause <!--- not sure how to handle parent where clauses at the moment & parentWhereClause --->>
	</cffunction>


	<cffunction name="getQueryFieldListForDataImport" access="private" output="false" returnType="string" hint="Get the list of fields to import for an object">
		<cfargument name="object" type="string" required="true" hint="The Msdynamics object">
		<cfargument name="modifiedDataFieldsOnly" type="boolean" required="true" hint="Whether we are just getting the IDs of records that have been modified">

		<cfset var queryFieldList = super.getQueryFieldListForDataImport(argumentCollection=arguments)>

		<!--- we need to get the modifiedOn date as part of the record import --->
		<cfif not listFindNoCase(queryFieldList,"modifiedon")>
			<cfset queryFieldList = listAppend(queryFieldList,"modifiedon")>
		</cfif>

		<cfreturn queryFieldList>
	</cffunction>


	<cffunction name="getQueryInfoForNonPrimaryObjectDataImport" type="private" hint="Gets query info (such as field list and where clause) for the import of non-primary objects" returnType="struct" output="false">
		<cfargument name="object" type="string" required="true" hint="The non-primary object. Generally an opportunityContactRole and/or an partner object">
		<cfargument name="timeDateWhereClause" type="string" required="true" hint="The time filter for the current run">
		<cfargument name="baseObjectWhereClause" type="string" default="" hint="The filter applied for the primary object">
		<cfargument name="parentWhereClause" type="string" default="" hint="The filter for the primary object's parent">


		<cfset var thisObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var result = {whereClauseArray = arrayNew(1),fieldList=listAppend(getQueryFieldListForDataImport(object=arguments.object),"#thisObject.parentKeyField#,#this.objectPrimaryKey#")}> <!--- get the objectId and the parentKey field --->
		<cfset var objectWhereClause = thisObject.whereClause neq ""? "(#thisObject.whereClause#) and":"">

		<cfset result.whereClauseArray[1] = "#objectWhereClause# #thisObject.parentKeyField# in (select ID from #thisObject.parentObject# where #arguments.timeDateWhereClause# #arguments.baseObjectWhereClause#)"> <!--- get all the related objects for the primary objects that we have gotten --->
		<cfset result.whereClauseArray[2] = "#objectWhereClause# #timeDateWhereClause# #arguments.parentWhereClause#"> <!--- get all the related objects that have changed for only those base objects that we are interested in. Keep the index as 2 as the getDataToImport function relies on this being 2. Should be changed, but it's like that for now--->

		<!--- running this here should the application variable below not exist for whatever reason --->
		<cfset application.getObject("connector.com.connector").putRemoteObjectMetaDataIntoMemory(connectorType=this.connectorType,object=arguments.object)> <!--- This ensure that all the objects that we care about are in memory --->

		<!--- dealing with the objects that have a role - again, primarily the opportunityContactRole and Partner objects --->
		<cfif structKeyExists(application.connector.msdynamics.objectMetaData[arguments.object],"role")>

			<cfset var getRoles = "">
			<cfset result.fieldList = listAppend(result.fieldList,"role")>

			<!--- get the roles that we are interested in --->
			<cfquery name="getRoles">
				select value_remote as role from connectorColumnValueMapping cm
					inner join vConnectorMapping m on m.ID = cm.connectorMappingID and m.active=1 and m.import=1
				where
					object_remote = <cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
					and value_relayware = 'role'
			</cfquery>

			<cfif getRoles.recordCount>
				<cfset result.whereClauseArray[1] = result.whereClauseArray[1] & " and role in (#quotedValueList(getRoles.role)#)"> <!--- get all the related objects for the primary objects that we have gotten --->
				<cfset result.whereClauseArray[2] = "#objectWhereClause# #timeDateWhereClause# and role in (#quotedValueList(getRoles.role)#) #parentWhereClause#"> <!--- get all the related objects that have changed for only those base objects that we are interested in--->
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getDataForObjectIDs" access="public" output="false" returnType="struct" hint="Gets data from the remote system for the given IDs. This will populate the Msdynamics object import tables">
		<cfargument name="object" type="string" default="#this.object#">
		<cfargument name="objectIds" type="array" required="true">
		<cfargument name="fieldList" type="string" default="#getQueryFieldListForDataImport(object=arguments.object,modifiedDataFieldsOnly=false)#">

		<cfset var successResult = {tablename="",recordCount=0,columnList=""}>
		<cfset var errorResult = {tablename="",recordCount=0,columnList=""}>
		<cfset var result = {isOk=true,message="",successResult=successResult,errorResult=errorResult}>


		<!---<cfloop index="i" from="1" to="#arrayLen(objectIds)#">--->
			<cfset var sendResult = this.api.send(method = "retrieveBulk", object = arguments.object, objectArrayId = arguments.objectIds,fieldList=arguments.fieldList)>
		<!---</cfloop>--->

		<cfset structAppend(result,sendResult)>

		<cfreturn result>
	</cffunction>


		<!--- TODO: doesn't seem to be a way of getting audits for just one object. Investiage. At the moment, we are picking up all deletions!!!! --->
	<cffunction name="getDeletedObjects" access="public" output="false" returnType="struct" hint="Gets the deleted objects from MSDynamics">
		<cfargument name="startDateTimeUTC" type="date" required="true">
		<cfargument name="endDateTimeUTC" type="date" required="true">
		<cfargument name="object" type="string" default="#this.mappedObject#">

		<cfset var daoService = createObject("component","relay.connector.msDynamics.MSDynamicsDAOService")>
		<cfset var timeDateWhereClause = "createdon ge #convertDateToMSDynamicsFormat(dateToConvert=arguments.startDateTimeUTC)# and createdon le #convertDateToMSDynamicsFormat(dateToConvert=arguments.endDateTimeUTC)# and operation eq 3">

		<cfset var objectReturn = this.api.getByClause(object='audits', clause=timeDateWhereClause,fieldList="createdon,_objectid_value")>

		<!--- <cfset var table = "####msDynamics#arguments.object#" & #TimeFormat(Now(), "hh_mm_ssms")#> --->
		<cfset var table = getTempTableName()>

		<cfset var dao = {isOK = true,message="",successResult={}}>
		<cfset dao.successResult.tablename = "">

		<cfset var colId = this.api.parseMsEntityName(#arguments.object#) & 'ID'>
		<!---TODO add logic which will be filter IDs only for object type--->

		<cfif structKeyExists(objectReturn.data,"value")>
			<cfloop index="i" from="1" to="#arrayLen(objectReturn.data.value)#">
				<cfset var oneObject = objectReturn.data.value[i]>
				<cfset oneObject.createdOn = replaceNoCase(replaceNoCase(oneObject.createdOn,"T"," "),"Z","")> <!--- replace the T and Z in the datetime field --->
				<cfset var dao = daoService.importDataToDB(objectName=object, object=oneObject,tempTable= table)>
		</cfloop>

			<!--- add some columns that we're expecting, deletedDate and the object's primary key --->
			<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=dao.successResult.tablename,columnName=this.objectPrimaryKey,defaultValueFromColumn="_objectid_value")>
			<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=dao.successResult.tablename,columnName="deletedDate",defaultValueFromColumn="createdOn")>
			</cfif>

		<cfreturn dao>
	</cffunction>


	<cffunction name="deleteNonPrimaryRecordsForPrimaryObject"  access="private" output="false" returnType="struct" hint="Deletes partners and contacts from MSDynamics for objects that have just been modified">
		<cfargument name="object" type="string" required="true">
		<cfargument name="primaryObjectID" type="string" required="true">
		<!--- <cfargument name="roleList" type="string" required="false"> --->
		<cfargument name="filter" type="string" required="true" hint="The filter used to determine what records to delete">


		<cfset var result = {isOK=true,message=""}>
		<cfset var theObject = getConnectorObject(object=arguments.object,relayware=false)>

		<cfif arguments.filter neq "">
			<cfset var objectFilter = arguments.filter>

			<cfif left(objectFilter,4) neq "and ">
				<cfset objectFilter = "and "&objectFilter>
			</cfif>

			<cfset var getObjectIDsToDelete = this.api.send(method="query",object=arguments.object,queryString="select ID,#theObject.parentKeyField# from #arguments.object# where #theObject.parentKeyField# = '#arguments.primaryObjectID#' #preserveSingleQuotes(objectFilter)#")>

			<cfset log_(label="Export (delete): Get Object IDs To Delete for #arguments.object# (#arguments.primaryObjectID#)",logStruct=getObjectIDsToDelete)>

			<cfif getObjectIDsToDelete.successResult.recordCount>
				<cfset var getIdsToDelete = "">

				<cfquery name="getIdsToDelete">
					select distinct ID from #getObjectIDsToDelete.successResult.tablename#
				</cfquery>

				<cfset var deleteDataResult = this.api.send(method="delete",object=arguments.object,ids=listToArray(valueList(getIdsToDelete.ID)))>

				<cfset log_(label="Export (delete): Delete Data Result",logStruct=deleteDataResult)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="convertDateToMSDynamicsFormat" access="public" returnType="String" output="false">
		<cfargument name="dateToConvert" required="true" type="date">
		<cfargument name="inUTC" default="true" type="boolean">

		<cfreturn "#dateFormat(arguments.dateToConvert,'yyyy-MM-dd')#T#timeFormat(arguments.dateToConvert,'HH:mm:ss')##arguments.inUTC?'Z':''#">
	</cffunction>


	<cffunction name="setObjectRelaywareIDs" access="private" output="false" returnType="struct" hint="Sets the Relayware IDs on the remote system. This is just a stub to call the connector specific method.">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table containing the records.">
		<cfargument name="columnList" type="string" required="true" hint="The names of the columns that we will be updating. Will be the Salesforce ID and the Relayware ID.">
		<cfargument name="tableMetaData" type="struct" default="#structNew()#" hint="The meta data of the tablename table. Contains columnList and recordCount">
		<cfargument name="relaywareIDField" type="string" required="true" hint="The name of the field containing the RelaywareID.">

		<cfset var rowsPerRun = 200>
		<cfset var result = application.getObject("connector.com.connectorUtilities").initialiseResultStructure(returnErrorTable=true)>

		<cfset var loopCount = int(arguments.tableMetaData.recordCount/rowsPerRun)>
		<cfset var rowNum = 0>
		<cfset var updateRemoteIdsResult = {}>
		<cfset var collateResponses = "">

		<cfloop from=0 to=#loopCount# index="rowNum">
			<cfset updateRemoteIdsResult = this.api.send(method="update",object=this.object,viewName=arguments.tableName,columnList=arguments.columnList,whereClause="rowNumber > #rowNum*rowsPerRun# and rowNumber <= #((rowNum+1)*rowsPerRun)#")>

			<!--- collate the responses --->
			<cfif updateRemoteIdsResult.isOK>
					<!--- put errored records into the error table --->
					<cfif updateRemoteIdsResult.result.recordCount>

						<cfquery name="collateResponses">
						insert into <cf_queryObjectName value="#result.errorResult.tablename#"> (object,objectId,message,field,value,statusCode,connectorResponseID)
							select distinct
							<cf_queryparam value="#this.object#" cfsqltype="cf_sql_varchar">,
							t.#this.objectPrimaryKey#,
							r.message,
							<cf_queryparam value="#arguments.relaywareIDField#" cfsqltype="cf_sql_varchar">,
							t.<cf_queryObjectName value="#arguments.relaywareIDField#">,
							r.statusCode,
							r.connectorResponseID
						from <cf_queryObjectName value="#updateRemoteIdsResult.result.tablename#"> r
							inner join <cf_queryObjectName value="#arguments.tableName#"> t on r.rowNumber + (#rowsPerRun#*#loopCount#) = t.rowNumber <!--- can't join on ID, as the Id doesn't exist in the result set --->
							where success = 0
						</cfquery>
					</cfif>
			</cfif>
		</cfloop>

		<cfset var getErroredRecords = "">
		<cfset var getErroredRecordsResult = {}>

		<cfquery name="getErroredRecords" result="getErroredRecordsResult">
			select * from #result.errorResult.tablename#
		</cfquery>

		<cfset structAppend(result.errorResult,getErroredRecordsResult)>

		<cfreturn result>
	</cffunction>


	<cffunction name="getTimeDateWhereClauseForObjectImport" access="private" output="false" hint="Returns the time/date where clause needed for data import" returnType="string">
		<cfargument name="startDateTimeUTC" type="date" required="true" hint="The start date for when object data should be collected. In UTC">
		<cfargument name="endDateTimeUTC" type="date" required="true" hint="The end date for when object data should be collected. In UTC">
		<cfif not isDefined("application.connector.API.userID")>
			<cfset application.connector.API.userID = this.api.setAPIuserID()>
		</cfif>

		<cfreturn "modifiedon ge #convertDateToMSDynamicsFormat(dateToConvert=arguments.startDateTimeUTC)# and modifiedon le #convertDateToMSDynamicsFormat(dateToConvert=arguments.endDateTimeUTC)# and modifiedby ne #application.connector.API.userID# ">
	</cffunction>

	<cffunction name="getRetrieveInfoForNonPrimaryObjects" access="private" hint="Returns some info to be passed into the retreive function" output="false" returnType="struct">
		<cfargument name="object" type="string" required="true" hint="The non-primary object. Generally an opportunityContactRole or opportunityPartner">
		<cfargument name="primaryObjectIDs" type="array" required="true" hint="The IDs of the primary object. Generally opportunity Ids">

		<cfset var result = {idsToImport = arrayNew(1),objectFieldList = ""}>
		<cfset var mappedObject = getConnectorObject(object=arguments.object,relayware=false)>
		<cfset var primaryObjectIDList = listQualify(arrayToList(arguments.primaryObjectIDs),"'")>
		<cfset result.objectFieldList = listAppend(mappedObject.fieldColumnList,"modifiedon,#mappedObject.parentKeyField#,ID")>
		<cfset var baseQueryString = "SELECT ID from #arguments.object# where #mappedObject.parentKeyField# in ">
		<cfset var roleQueryString = "">
		<cfset var queryString = "">
		<cfset var resultTableName = getTempTableName()>
		<cfset var collateResults = "">

		<!--- dealing with the objects that have a role - again, primarily the opportunityContactRole and Partner objects --->
		<cfif structKeyExists(application.connector.msdynamics.objectMetaData[arguments.object],"role")>

			<cfset var getDistinctMappedRoles = "">

			<cfquery name="getDistinctMappedRoles">
				select distinct cvm.[value_remote] as role from connectorColumnValueMapping cvm
					inner join vConnectorMapping m on m.ID = cvm.connectorMappingID
				where cvm.value_relayware='role'
					and m.object_remote=<cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar">
					and import=1
					and active=1
			</cfquery>

			<cfif getDistinctMappedRoles.recordCount>
				<cfset roleQueryString = "and role in (#quotedValueList(getDistinctMappedRoles.role)#)">
			</cfif>

			<cfset result.objectFieldList = listAppend(result.objectFieldList,"role")>
		</cfif>

		<cfset var createResultTable = "">
		<cfquery name="createResultTable">
			select cast(null as varchar) as ID into #resultTableName# where 1=0
		</cfquery>

		<!--- not using query here as that will truncate the data in the base table....
			Need to check the length of the queryString... Msdynamics throws an error if queryString is longer than 20000 characters. So, need to do several iterations if primaryObjectIDList is quite long
		--->
		<cfset var idx = 0>
		<cfset var primaryObjectIDListForQuery = "">
		<cfloop from=1 to=#listLen(primaryObjectIDList)# index="idx">

			<cfset primaryObjectIDListForQuery = listAppend(primaryObjectIDListForQuery,listGetAt(primaryObjectIDList,idx))>

			<!--- NJH 2015/05/08 if we've got a 900 Ids or if we've hit the end of the list, run a query --->
			<cfif listLen(primaryObjectIDListForQuery) eq 900 or idx eq listLen(primaryObjectIDList)>
				<cfif primaryObjectIDListForQuery neq "">
					<cfset queryString = baseQueryString & "(#preserveSingleQuotes(primaryObjectIDListForQuery)#) " & roleQueryString>

					<cfset var modifiedDataResult = this.api.send(object=arguments.object,method="query",queryString=preserveSingleQuotes(queryString),throwErrorOn500Response=true)>

					<cfset primaryObjectIDListForQuery = "">

					<cfif modifiedDataResult.successResult.tablename neq "">

						<cfquery name="collateResults">
							insert into #resultTableName# (ID)
							select ID from #modifiedDataResult.successResult.tablename#
						</cfquery>

					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<cfset var getIDs = "">
		<cfset var getIDsResult = {}>

		<cfquery name="getIDs" result="getIDsResult">
			select ID from #resultTableName#
		</cfquery>

		<cfset result.idsToImport = listToArray(valueList(getIDs.ID))>

		<cfset log_(label="Non Primary Object IDs",logStruct=getIDsResult)>

		<cfreturn result>
	</cffunction>


</cfcomponent>