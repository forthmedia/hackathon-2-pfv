<!---  �Relayware. All Rights Reserved 2015 --->

<cfcomponent name="MSDynamicsDAOService" output="false">
    <cfparam name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">

    <!---Insert data to DB--->
    <cffunction name="importDataToDB" access="public" returnType="Any">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="object" type="STRUCT" required="true">
        <cfargument name="temptable" type="string" required="false" default="#application.getObject("connector.com.connectorUtilities").getTempTableName(tempTableSuffix=request.tempTableSuffix,tempTable=true)#">
		<cfargument name="connectorResponseId" type="numeric" required="false">
		<Cfargument name="truncateTable" type="boolean" default="false">

        <cfset var errorTable = "####msDynamics#arguments.objectName#" & TimeFormat(Now(), "hh_mm_ssms")>

		<!--- if an empty string was passed in, treat it as a default --->
		<cfif arguments.tempTable eq "">
			<cfset arguments.tempTable = application.getObject("connector.com.connectorUtilities").getTempTableName(tempTableSuffix=request.tempTableSuffix,tempTable=true)>
		</cfif>

		<!---this query just to get column list--->
        <cfset var ListElement	= '' />
        <cfset var col	= '' />
        <cfset var DbImpoer	= '' />
        <cfset var colVal	= '' />
        <cfset var columns = "">
        <cfset var vals = "">
		<cfset var truncateData = "">
		<cfset var isDataTable = false>
		<cfset var table = arguments.tempTable>
		<cfset var result = {isOK = true,successResult = {}}>
		<cfset result.successResult = {tablename=table,columnList="",recordCount=0}>

        <cfif arguments.temptable eq "connector_#connectorType#_#arguments.objectName#">

			<cfif arguments.truncateTable>
				<cfquery name="truncateData">
					truncate table #table#
				</cfquery>
			</cfif>
			<cfset isDataTable = true>

            <cfquery name="col" result="colVal">
	            SELECT top 1 * FROM #table# WHERE 1=1
	        </cfquery>
        <cfelse>

            <cfset var colVal = {columnList = StructKeyList(object)}>

            <cfif structKeyExists(arguments,"connectorResponseID")>
                <cfset colVal.columnList = listAppend(colVal.columnList,"connectorResponseID")>
            </cfif>

            <cftry>
                <cfset createTable(colVal.columnList, table) >
                <cfcatch>

                </cfcatch>
            </cftry>

        </cfif>

        <cfloop index = "ListElement" list = "#colVal.columnList#">
			<!--- JIRA PROD2016-2159 --->
			<cfif listElement eq "LastModifiedDate" and structKeyExists(object,"modifiedon")>
				<cfset columns = listAppend(columns,"lastModifiedDate")>
				<cfset vals = vals & dateAdd("s",-(request.remoteServerTimeOffsetInSeconds),replaceNoCase(replaceNoCase(object.modifiedon,"T"," "),"Z","")) & ",">
            <cfelseif Lcase(ListElement) neq 'id'>
                <cfif StructKeyExists(object, ListElement)>
                    <cfset var columns = listAppend(columns, "["&LCase(ListElement &"]"))>
                    <cfset var vals = vals & "N'" & replace(StructFind(object, ListElement),"'","?","All")  & "',">
                </cfif>

            </cfif>
        </cfloop>

		<cfif len(vals) gt 0>
	        <cfset var vals = Left(vals, len(vals)-1)>

			<!--- log the response as well if passed through --->
			<cfif structKeyExists(arguments,"connectorResponseID")>
				<cfset columns = listAppend(columns,"connectorResponseID")>
				<cfset vals = listAppend(vals,arguments.connectorResponseID)>
			</cfif>

	        <cftry>
				<cfif isDataTable>
					<cfset var primaryKey = application.getObject("connector.com.connector").getRemoteIDColumn(connectorType="msDynamics",object=arguments.objectName)>
				</cfif>
	            <cfquery name="DbImport">
					<!--- can't violate primary key constraint, so check that the object record doesn't already exist --->
					<cfif isDataTable and primaryKey neq "" and structKeyExists(arguments.object, primaryKey)>
						if not exists (select 1 from #table# where #primaryKey# = '#arguments.object[primaryKey]#')
					</cfif>
	                insert into #table# (#columns#) values (#PreserveSingleQuotes(vals)#);
	            </cfquery>

	            <cfset var result.successResult.columnList = colVal.columnList>
				<cfset var result.successResult.recordCount = 1> <!--- it appears that we are inserting one record at a time...  --->

	            <cfcatch>
	                <cfset var result.isOK = false>
	                <cfset var result.message = cfcatch.tagcontext>

	                <cfset createErrorTable(objectName, errorTable)>

	                <cfquery name="errorMessage">
	                        insert into #errorTable# (objectName,message) VALUES  (<cf_queryparam value="#objectName#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#cfcatch.detail#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	                 </cfquery>

	                <cfset var result.errorResult.tablename = table>
	                <cfset var result.errorResult.recordCount = 1>
	                <cfset var result.errorResult.columnList = "objectName, message">
					<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,warningStructure=arguments)>
	            </cfcatch>
	        </cftry>
	    </cfif>

        <cfreturn result>

    </cffunction>

    <!---Insert multiple data into DB for one request--->
    <cffunction name="importDataToDBBulk" access="public" returnType="Any">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="object" type="ARRAY" required="true">
        <cfargument name="temptable" type="string" required="false" default="#application.getObject("connector.com.connectorUtilities").getTempTableName(tempTableSuffix=request.tempTableSuffix,tempTable=true)#">
        <cfargument name="connectorResponseId" type="numeric" required="false">
        <Cfargument name="truncateTable" type="boolean" default="false">

        <cfset var errorTable = "####msDynamics#arguments.objectName#" & TimeFormat(Now(), "hh_mm_ssms")>

<!--- if an empty string was passed in, treat it as a default --->
        <cfif arguments.tempTable eq "">
            <cfset arguments.tempTable = application.getObject("connector.com.connectorUtilities").getTempTableName(tempTableSuffix=request.tempTableSuffix,tempTable=true)>
        </cfif>

<!---this query just to get column list--->
        <cfset var ListElement	= '' />
        <cfset var col	= '' />
        <cfset var DbImpoer	= '' />
        <cfset var colVal	= '' />
        <cfset var columns = "">
        <cfset var vals = "">
        <cfset var truncateData = "">
        <cfset var isDataTable = false>
        <cfset var table = arguments.tempTable>
        <cfset var result = {isOK = true,successResult = {}}>
        <cfset result.successResult = {tablename=table,columnList="",recordCount=0}>

		<cfif arguments.temptable eq "connector_#connectorType#_#arguments.objectName#">
			 <cfset isDataTable = true>

			 <cfif arguments.truncateTable>
                <cfquery name="truncateData">
					truncate table #table#
                </cfquery>
            </cfif>
		</cfif>

		<cfif arrayLen(arguments.object)>
			<cfif isDataTable>
	            <cfquery name="col" result="colVal">
		            SELECT top 1 * FROM #table# WHERE 1=1
		        </cfquery>
	        <cfelse>

	            <cfset var colVal = {columnList = StructKeyList(object[1])}>

	            <cfif structKeyExists(arguments,"connectorResponseID")>
	                <cfset colVal.columnList = listAppend(colVal.columnList,"connectorResponseID")>
	            </cfif>

	            <cftry>
	                <cfset createTable(colVal.columnList, table) >
	                <cfcatch>

	                </cfcatch>
	            </cftry>

	        </cfif>

	        <cfloop index = "ColumnListElement" list = "#fieldList#">
	            <cfif ColumnListElement eq "modifiedon">
	                <cfset columns = listAppend(columns,"lastModifiedDate")>
	            <cfelse>
	                <cfset var columns = listAppend(columns, "["&LCase(#ColumnListElement#&"]"))>
	            </cfif>
	        </cfloop>

	        <cfset var allRecordsToDB=ArrayNew(1)>
	        <cfset var objectCounter = 1>

	        <cfloop array="#arguments.object#" index="obj">
	            <cfset var vals = "">

	            <cfloop index = "ListElement" list = "#fieldList#">
	                <cfif listElement eq "modifiedon">
	                    <cfset vals = vals & dateAdd("s",-(request.remoteServerTimeOffsetInSeconds),replaceNoCase(replaceNoCase(obj.modifiedon,"T"," "),"Z","")) & ",">
	                <cfelse>
	                    <cfif StructKeyExists(obj, ListElement)>
	                        <cfset var vals = vals & "N'" & replace(StructFind(obj, ListElement),"'","?","All")  & "',">
	                    </cfif>
	                    <cfif not StructKeyExists(obj, ListElement)>
	                        <cfset var vals = vals & "'',">
	                    </cfif>
	                </cfif>
	            </cfloop>

	        <cfif len(vals) gt 0>
	            <cfset var vals = Left(vals, len(vals)-1)>

	<!--- log the response as well if passed through --->
	            <cfif structKeyExists(arguments,"connectorResponseID")>
	                <cfif objectCounter eq 1>
	                    <cfset columns = listAppend(columns,"connectorResponseID")>
	                </cfif>
	                <cfset vals = listAppend(vals,arguments.connectorResponseID)>
	            </cfif>

	            <cftry>
					 <cfset allRecordsToDB[objectCounter] = PreserveSingleQuotes(vals)>

	                <cfcatch>
	                    <cfset var result.isOK = false>
	                    <cfset var result.message = cfcatch.tagcontext>

	                    <cfset createErrorTable(objectName, errorTable)>

	                    <cfquery name="errorMessage">
		                        insert into #errorTable# (objectName,message) VALUES  (<cf_queryparam value="#objectName#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#cfcatch.detail#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	                    </cfquery>

	                    <cfset var result.errorResult.tablename = table>
	                    <cfset var result.errorResult.recordCount = 1>
	                    <cfset var result.errorResult.columnList = "objectName, message">
	                    <cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,warningStructure=arguments)>
	                </cfcatch>
	            </cftry>
	        </cfif>
	            <cfset var objectCounter = objectCounter + 1>
	        </cfloop>

			<cfset tempInsertTable = application.getObject("connector.com.connectorUtilities").getTempTableName(tempTableSuffix=request.tempTableSuffix,tempTable=true)>

			<cfset var primaryKey = application.getObject("connector.com.connector").getRemoteIDColumn(connectorType="msDynamics",object=arguments.objectName)>
			<cfif arguments.objectName eq "systemUsers">
				<cfset primaryKey = application.getObject("connector.msDynamics.com.connectorMSDynamics").getUserMatchingData().fieldStruct.id>
			</cfif>
			<cfset createTable(colVal.columnList, tempInsertTable) >

	        <cfset var bulkRequest = "">
	        <cfset var arLength = ArrayLen(allRecordsToDB)>
			<cfset var batchSize = 1000> <!--- it's faster if we can insert into db with 1000 record batches rather than trying to do everything at once --->
			<cfset var processLoop = int(arLength/batchSize)>
			<cfset var arrayIndex = 0>
			<cfset var DbImport2 = "">

			<cfloop from=0 to="#processLoop#" index="loopCount">
				<cfset var startFrom = (loopCount*batchSize)+1>
				<cfset var endAt = (loopCount+1)*batchSize>
				<cfif endAt gt arLength>
					<cfset endAt = arLength>
				</cfif>
				<cfset bulkRequest = "">

		        <cfloop from="#startFrom#" to="#endAt#" index="arrayIndex">
					<cfset bulkRequest = bulkRequest & "("&allRecordsToDB[arrayIndex]&")">
		           <!---  <cfset var bulkRequest = bulkRequest & 'SELECT ' & #eachValues#> --->
		            <cfif arrayIndex mod batchSize neq 0  and arrayIndex neq arLength>
		                <cfset bulkRequest = bulkRequest & " , ">
		            </cfif>
		        </cfloop>

				<cfif bulkRequest neq "">
			         <cfquery name="DbImport2">
		              	insert into #tempInsertTable# (#columns#) select #columns# from (values #PreserveSingleQuotes(bulkRequest)#) dataToInsert(#columns#)
					</cfquery>
				</cfif>
		    </cfloop>

	        <cftry>
	            <cfif arLength neq 0>

                    <cfquery name="DbImport2">
                         insert into #table# (#columns#) select distinct tt.#replace(columns,",",",tt.","ALL")# from #tempInsertTable# tt left join #table# t on tt.#primaryKey# = t.#primaryKey#
                         where t.#primaryKey# is null
                </cfquery>
	            </cfif>

	            <cfset var result.successResult.columnList = colVal.columnList>
	            <cfset var result.successResult.recordCount = arLength>

	            <cfcatch>
	                <cfset var result.isOK = false>
	                <cfset var result.message = cfcatch.tagcontext>

	                <cfset createErrorTable(objectName, errorTable)>

	                <cfquery name="errorMessage">
		                        insert into #errorTable# (objectName,message) VALUES  (<cf_queryparam value="#objectName#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#cfcatch.detail#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	                </cfquery>

	                <cfset var result.errorResult.tablename = table>
	                <cfset var result.errorResult.recordCount = 1>
	                <cfset var result.errorResult.columnList = "objectName, message">
	                <cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,warningStructure=arguments)>
	            </cfcatch>
	        </cftry>

		<!--- if no data passed in and it's not a standard data table, then we need to create an empty dummy temp table --->
	    <cfelseif not isDataTable>
	    	<cfset createTable(arguments.fieldList, result.successResult.tablename) >
	    </cfif>

        <cfreturn result>

    </cffunction>

    <cffunction name="createErrorTable" access="public" returnType="void">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="errortable" type="string" required="true">

        <cfset var createNonExistentTable = "">

		<cfquery name="createNonExistentTable">
			if not exists (select  1 from <cfif left(arguments.errorTable,1) eq "##">tempdb.</cfif>dbo.sysobjects o where o.xtype in ('U') and o.name = N'#errortable#')
				begin
					create table [dbo].[#errortable#](
                            objectName varchar(40), message nvarchar(max)
                            ) ON [PRIMARY]
                            end
             </cfquery>

    </cffunction>

    <cffunction name="createTable" access="public" returnType="void">
        <cfargument name="columns" type="any" required="true">
        <cfargument name="table" type="string" required="true">

        <cfquery name="createNonExistenTable">
				if not exists (select  1 from <cfif left(arguments.table,1) eq "##">tempdb.</cfif>dbo.sysobjects o
				    where o.xtype in ('U')
				    and o.name =  <cf_queryparam value="#table#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					create table [dbo].[#table#] (
                    <cfloop index = "ListElement" list = "#columns#">
                        [#lcase(ListElement)#] nvarchar(max),
                    </cfloop>
                        ) ON [PRIMARY]
            end
        </cfquery>

    </cffunction>

    <!---Retrieve data from DB--->
    <cffunction name="exportFromDB" access="public" returnType="Any">
        <cfargument name="objectName" type="string" required="true">
        <cfargument name="whereClause" type="string" default="" required="false">
        <cfargument name="viewName" type="string" required="false" default="">
        <cfargument name="columnList" type="string" required="false" default="*">
        <cfargument name="singleObject" type="string" required="true">

        <cfset var objectList = ArrayNew(1)>
        <cfset var exportDB	= "" />
		<cfset var selectCol = "">
		<cfset var asCol = "">
		<cfset var table = arguments.viewName eq ""?"connector_#connectorType#_#arguments.objectName#":arguments.viewName>
		<cfset var selectClause = "">
		<cfset var fieldname="">
		<cfset var bindColumnStruct = {}>

		<!--- <cfif not listFindNoCase(arguments.columnList,singleObject & "ID")>
			<cfset arguments.columnList = listAppend(arguments.columnList,singleObject & "ID")>
		</cfif> --->

		<cfset var exportKeysStruct = application.getObject("connector.msdynamics.com.connectorMsDynamics").getExportValuesForKeys(fieldlist=arguments.columnList,objectName=arguments.objectName)>
		<cfset var booleanFields = getBooleanFields(objectName=arguments.objectName)>
		<cfset var fieldsToNull = application.getObject("connector.com.connector").getFieldsToNull(object=arguments.objectName)>

		<cfloop list="#arguments.columnList#" index="fieldname">
			<cfset selectCol = replaceNoCase(exportKeysStruct[fieldname].value,"#application.delim1##fieldName##application.delim1#",fieldname)>
			<cfif selectCol neq fieldname>
				<cfset selectCol = "'#replaceNoCase(exportKeysStruct[fieldname].value,"#application.delim1##fieldName##application.delim1#","'+#fieldname#+'")#'">
			</cfif>
			<!--- if the field is a boolean, than return a true or false. Connector should probably be doing this, but doing it here for now. --->
			<cfif listFindNoCase(booleanFields,fieldname)>
				<cfset selectCol = "case when cast(#selectCol# as varchar) in ('1','true') then 'true' else 'false' end">
			</cfif>
			<cfset asCol = exportKeysStruct[fieldname].key>
			<!--- if we have a col that has a bind or if it's a field to null, then return null as the value, rather than an empty string --->
			<cfif findNoCase("@odata.bind",asCol) or listFindNoCase(fieldsToNull,fieldname)>
				<cfset selectCol = "case when len(isNull(#fieldname#,'')) = 0 then 'null' else #selectCol# end">
				<cfif findNoCase("@odata.bind",asCol)>
					<cfset bindColumnStruct[fieldname] = exportKeysStruct[fieldname].key>
				</cfif>
			</cfif>
			<cfset selectClause = listAppend(selectClause,"#selectCol# as [#asCol#]")>
		</cfloop>

        <cfquery name="exportDB">
             SELECT #preserveSingleQuotes(selectClause)# from #table# where 1=1
						<cfif arguments.whereClause neq "">and #preserveSingleQuotes(arguments.whereClause)#</cfif>
        </cfquery>

<!---         <cfloop query="exportDB">
            <cfset var oneObj = structNew()>
            <cfset var tempId = ''>
            <cfloop index = "ListElement" list = "#exportResult.columnList#">
                <cfif Lcase(ListElement) neq 'id' and Lcase(ListElement) neq (singleObject & 'ID')>
                    <cfset var field = Lcase(ListElement)>
                    <cfset var el = exportDB[ListElement]>
                    <cfif el neq "">
                        <cfset var oneObj[field] = el>
                    </cfif>
                </cfif>
                <cfif Lcase(ListElement) eq (singleObject & 'ID')>
                    <cfset var tempId = exportDB[ListElement]>
                </cfif>
            </cfloop>
<!---insert in array Struct--->
            <cfset var objWithID = structNew()>
            <cfset objWithID['json'] = oneObj>
            <cfset objWithID['id'] = tempId>
            <cfset arrayAppend(objectList, objWithID)>
        </cfloop> --->

		<cfreturn prepareDataForExport(objectArray=application.com.structureFunctions.QueryToArrayOfStructures(query=exportDB,lowerCaseKeys=true),singleObject=arguments.singleObject,bindColumnStruct=bindColumnStruct)>

		<!---<cfloop from="1" to=#arrayLen(objectList)# index="i">
			<cfset var objStruct = duplicate(objectList[i])>
			<cfset objectList[i] = {}>
			<cfset objectList[i]["id"] = structKeyExists(objStruct,singleObject & "ID")?objStruct[singleObject & 'ID']:"">
			<cfset StructDelete(objStruct,singleObject & 'ID')>
			<cfset objectList[i]["json"] = objStruct>

			<!--- here we are setting any bind values to use the original fieldnames where we are nulling data. The odata.bind is only used when setting a value, not for unsetting it. When unsetting it, use the original fieldname --->
			<cfloop collection="#bindColumnStruct#" item="fieldname">
				<cfset var fieldnameWithBind = bindColumnStruct[fieldname]>
				<cfset var fieldNameNoValue = replace(replace(fieldname,"_value",""),"_","")> <!--- remove the leading underscore and the _value  --->
				<cfif objectList[i]["json"][fieldnameWithBind] eq "null">
					<cfset objectList[i]["json"][lcase(fieldname)] = "null">
					<cfset structDelete(objectList[i]["json"],fieldnameWithBind)>
				</cfif>
			</cfloop>
		</cfloop>--->

    </cffunction>


	<cffunction name="prepareDataForExport" access="private" output="false" returnType="array">
		<cfargument name="objectArray" type="array" required="true">
		<cfargument name="singleObject" type="string" required="true">
		<cfargument name="bindColumnStruct" type="struct" default="#structNew()#">

		<cfset var i=0>
		<cfset var exportObjectArray = arrayNew(1)>
		<cfset var fieldname = "">

		<cfloop from="1" to=#arrayLen(arguments.objectArray)# index="i">
			<cfset var objStruct = duplicate(arguments.objectArray[i])>
			<cfset exportObjectArray[i] = {}>
			<cfset exportObjectArray[i]["id"] = structKeyExists(objStruct,arguments.singleObject & "ID")?objStruct[arguments.singleObject & "ID"]:"">
			<cfset StructDelete(objStruct,arguments.singleObject & "ID")>
			<cfset exportObjectArray[i]["json"] = objStruct>

			<!--- here we are setting any bind values to use the original fieldnames where we are nulling data. The odata.bind is only used when setting a value, not for unsetting it. When unsetting it, use the original fieldname --->
			<cfloop collection="#arguments.bindColumnStruct#" item="fieldname">
				<cfset var fieldnameWithBind = arguments.bindColumnStruct[fieldname]>
				<cfset var fieldNameNoValue = replace(replace(fieldname,"_value",""),"_","")> <!--- remove the leading underscore and the _value  --->
				<cfif exportObjectArray[i]["json"][fieldnameWithBind] eq "null">
					<!---commented this out for now as I don't know how we unset a entity reference. It appears as if it's separate delete call (per field!!). Am hoping to find out soon.
					<cfset exportObjectArray[i]["json"][lcase(fieldname)] = "null">--->
					<cfset structDelete(exportObjectArray[i]["json"],fieldnameWithBind)>
				</cfif>
			</cfloop>
		</cfloop>

		<cfreturn exportObjectArray>
	</cffunction>

    <cffunction name="importCurrencyToDb">
	    <cfargument name="isocurrencycode" type="string" required="true">
        <cfargument name="currencyname" type="string" required="true">
		<cfargument name="currencysymbol" type="string" required="true">
		<cfargument name="transactioncurrencyid" type="string" required="true">
		<cfquery name="importCurrency">
            if not exists (select  1 from Currency
			   where currencyISOCode = <cf_queryparam value="#arguments.isocurrencycode#" cfsqltype="cf_sql_varchar">)
	                begin
	                    insert into Currency (currencyISOCode, currencyName, currencySign, r1CurrencyID)
						VALUES  (<cf_queryparam value="#arguments.isocurrencycode#" cfsqltype="cf_sql_varchar">,
						         <cf_queryparam value="#arguments.currencyname#" cfsqltype="cf_sql_varchar">,
						         <cf_queryparam value="#arguments.currencysymbol#" cfsqltype="cf_sql_varchar">,
						         <cf_queryparam value="#arguments.transactioncurrencyid#" cfsqltype="cf_sql_varchar">);
	                end
	           else
	                begin
	                    update Currency
                            set r1CurrencyID = <cf_queryparam value="#arguments.transactioncurrencyid#" cfsqltype="cf_sql_varchar">
                            where
                            currencyISOCode  = <cf_queryparam value="#arguments.isocurrencycode#" cfsqltype="cf_sql_varchar">
	                end

        </cfquery>
	</cffunction>


	<cffunction name="getBooleanFields" access="private" output="false" returnType="string">
		<cfargument name="objectName" type="string" required="true">

		<cfparam name="request.booleanFields" default="#structNew()#">

		<cfif not structKeyExists(request.booleanFields,arguments.objectName)>
			<cfset var objectMetaData = application.getObject("connector.com.connector").getRemoteObjectFields(connectorType="MsDynamics",object=arguments.objectName)>
			<cfset var getBooleanFields = "">

			<cfquery name="getBooleanFields" dbtype="query">
				select name from objectMetaData where datatype = 'boolean'
			</cfquery>
			<cfset request.booleanFields[arguments.objectName] = valueList(getBooleanFields.name)>
		</cfif>

		<cfreturn request.booleanFields[arguments.objectName]>
	</cffunction>
</cfcomponent>