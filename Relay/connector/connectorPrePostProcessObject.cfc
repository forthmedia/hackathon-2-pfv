﻿<!--- �Relayware. All Rights Reserved 2016 --->
<!---
  --- connectorPrePostProcessObject
  --- -----------------------------
  ---
  --- The object that contains the Pre and Post Process functions.
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   15/01/16

2016/03/10	NJH		JIRA PROD2016-472 Synching Attachments - added functions to delete and create the physical files
2017-01-03	DAN		PROD2016-3094/Case 450007 - limit sending deal reg emails based on opps approval status changes to opps of type "Deals"

  --->

<cfcomponent hint="The object that contains the Pre and Post Process functions." extends="connectorUtilitiesObject" accessors="true" output="false" persistent="false">


	<cffunction name="postProcess_PersonApprovalStatusChange" access="private" output="false" hint="Sends emails upon person approvals" index="10" returnType="struct" direction="import" object="person">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="synchDateTime" type="date" required="true" hint="The current time of the DB Server, so don't need to do UTC offsets">

		<cfreturn processLocationPersonApprovalStatusChange(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="processLocationPersonApprovalStatusChange" access="private" output="false" hint="Sends emails upon location/person approvals" returnType="struct">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="synchDateTime" type="date" required="true" hint="The current time of the DB Server, so don't need to do UTC offsets">

		<cfset var result = {isOK=true,message=""}>
		<cfset var getApprovalStatusChanges = "">
		<cfset var getApprovalStatusChangesResult = "">

		<!--- run some standard post-processing --->
		<cfif this.direction eq "import">

			<cfif listFindNoCase("location,person",this.baseEntity)>

				<!--- send approval emails for people that have been approved by the connector or for people whose locations have been approved by the connector --->
				<cfset var approvalFlagGroupID = application.com.flag.getFlagStructure(flagID='#left(this.baseEntity,3)#Approved').flagGroupID>
				<cfset var personAlias = "e">
				<cfset var approvalStatusEmailTextID = "RegistrationApprovedEmail">

				<cfif this.baseEntity eq "location">
					<cfset personAlias = "p">
				</cfif>

				<cfquery name="getApprovalStatusChanges" result="getApprovalStatusChangesResult">
					select distinct #personAlias#.personID, replace(f.flagTextID,'#left(this.baseEntity,3)#','') as approvalStatus
					from
						vModRegister mr
							inner join #arguments.tablename# t on t.#this.baseEntityPrimaryKey# = mr.recordID and mr.entityTypeID=#application.entityTypeID[this.baseEntity]#
							inner join #this.baseEntity# e	on e.#this.baseEntityPrimaryKey# = t.#this.baseEntityPrimaryKey#
							<cfif this.baseEntity eq "location">
							inner join person p on p.locationID = e.locationID
							</cfif>
							inner join organisation o on o.organisationID = e.organisationID
							inner join organisationType ot on ot.organisationTypeID = o.organisationTypeID and ot.typeTextID in ('reseller','distributor')
							inner join flag f on f.flagID = mr.flagID and f.flagGroupID  = <cf_queryparam value="#approvalFlagGroupID#" cfsqltype="cf_sql_integer">
						where
							modDate >= <cf_queryparam value="#arguments.synchDateTime#" cfsqltype="cf_sql_timestamp">
							and actionByCF = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">
							and mr.action in ('FA','FM')
							and (f.flagTextID like '%approved' or f.flagTextID like '%rejected')
				</cfquery>

				<cfset log_(label="Post Processing: Person Approvals",logStruct=getApprovalStatusChangesResult)>

				<cfloop query="getApprovalStatusChanges">
					<!--- if the user is approved, create a username/password for them --->
					<cfif approvalStatus eq "approved">
						<cfset application.com.login.createUserNamePassword(personID=personID)>
						<cfset approvalStatusEmailTextID = "RegistrationApprovedEmail">
					<cfelseif approvalStatus eq "rejected">
						<cfset approvalStatusEmailTextID = "PerRejectedEmail">
					</cfif>

					<!--- send the appropriate email given the person's change in approval status --->
					<cfset application.com.email.sendEmail(personID=personID,emailTextID=approvalStatusEmailTextID)>
				</cfloop>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="postProcess_OpportunityApprovalStatusChange" access="private" output="false" hint="Sends emails upon Deal Registration approvals" index="10" returnType="struct" direction="import" object="opportunity">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="synchDateTime" type="date" required="true" hint="The current time of the DB Server, so don't need to do UTC offsets">

		<cfset var result = {isOK=true,message=""}>
		<cfset var getApprovalStatusChanges = "">
		<cfset var getApprovalStatusChangesResult = {}>

		<!--- run some standard post-processing --->
		<cfif this.direction eq "import">

			<cfif this.baseEntity eq "opportunity">

				<!--- Task Core-176 - email the primary contact if the partner salesRep does not exist --->
				<cfquery name="getApprovalStatusChanges" result="getApprovalStatusChangesResult">
					select distinct isNull(isNull(o.partnerSalesPersonID,priContactFlag.data),0) as personID,
						case when os.statusTextid = 'DealRegRejected' then 'Declined' else 'Approved' end as approvalStatus,
						o.opportunityID
					from
						vModRegister mr
							inner join #arguments.tablename# t on t.opportunityID = mr.recordID and mr.entityTypeID=#application.entityTypeID.opportunity#
							inner join opportunity o on o.opportunityID = t.opportunityID
							inner join oppStatus os on os.statusID = o.statusID
							inner join oppType ot on ot.oppTypeID = o.oppTypeID
							<cfif application.com.relayPLO.getPrimaryContactFlag() eq "PrimaryContacts">
							left join
								integerMultipleFlagData priContactFlag on priContactFlag.entityID = o.partnerLocationID and priContactFlag.flagID = <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='PrimaryContacts').flagID#" cfsqltype="cf_sql_integer">
							<cfelse>
							left join
								(location l inner join integerFlagData priContactFlag on priContactFlag.entityID = l.organisationID and priContactFlag.flagID = <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='KeyContactsPrimary').flagID#" cfsqltype="cf_sql_integer">) on l.locationID = o.partnerLocationID
							</cfif>
						where
							modDate >= <cf_queryparam value="#arguments.synchDateTime#" cfsqltype="cf_sql_timestamp">
							and actionByCF = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">
							and mr.action in ('OppA','OppM')
							and mr.field in ('Whole Record','statusID')
							and os.statusTextID in ('DealRegRejected','DealRegApproved')
							and ot.oppTypeTextID='Deals'
						order by o.opportunityID
				</cfquery>

				<cfset log_(label="Post Processing: Deal Registration Approvals",logStruct=getApprovalStatusChangesResult)>

				<cfloop query="getApprovalStatusChanges">
					<!--- send the appropriate email given the opportunity's change in approval status --->
					<cfif personID neq 0>
						<cfset var mergeStruct = {opportunityID = opportunityID}>
						<cfset application.com.email.sendEmail(personID=personID,emailTextID="deal#approvalStatus#Email",mergeStruct=mergeStruct)>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="postProcess_RunWorkflow" access="private" output="false" hint="Run any workflows that have been set up." index="50" returnType="struct">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="synchDateTime" type="date" required="true" hint="The current time of the DB Server, so don't need to do UTC offsets">

		<cfset var result = {isOK=true,message=""}>
		<cfset var getEntityIdFromTable = "">
		<cfset var doesProcessExist = "">

		<cfset variables.tablename = arguments.tablename>
		<cfset variables.synchDateTime = arguments.synchDateTime>
		<cfset variables.direction = this.direction>
		<cfset variables.entityType = this.baseEntity>
		<cfset variables.validJumpActions = "sendEmail,setFlag,include">

		<!--- WAB 2015-10-12 During CASE 446216 Wrong processes being picked up because processStepTextID is usually null, should all be ProcessID based
				Problem was a hang over from when processes were done by naming convention
		--->

		<cfquery name="doesProcessExist">
			select top 1 processID
			from processActions
		 	where processID =  <cf_queryparam value="#getConnectorObject(object=this.object,relayware=this.direction eq 'import'?false:true).postProcessID#" CFSQLTYPE="cf_sql_varchar">
		 		and active = 1
		 	order by stepId,sequenceId
		</cfquery>

		<!--- check whether the process exists before going into here, as the redirector code aborts if it doesn't find the process --->
		<cfif doesProcessExist.recordCount>
			<cfset variables.frmProcessID = doesProcessExist.processID[1]>

			<cfset var getPostProcessEntityIdsResult = {}>

			<cfquery name="getEntityIdFromTable" result="getPostProcessEntityIdsResult">
				select distinct #this.baseEntityPrimaryKey# as entityID
				from #arguments.tablename#
				where #this.baseEntityPrimaryKey# is not null
			</cfquery>

			<cfset getPostProcessEntityIdsResult.tablename = arguments.tablename>
			<cfset log_(label="Post Processing: Run Workflow",logStruct=getPostProcessEntityIdsResult)>

			<cfloop query="getEntityIdFromTable">
				<cftry>
					<cfset variables.entityID = getEntityIdFromTable.entityID[currentRow]>
					<cfinclude template="\relay\screen\redirector.cfm">

					<cfcatch>
						<cfset var warningStruct = {arguments = arguments,entityid = getEntityIdFromTable.entityID[currentRow], processID = doesProcessExist.processID[1]}>
						<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",caughtError=cfcatch,warningStructure=warningStruct)>
					</cfcatch>
				</cftry>
			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="preProcess_ExportAccountManagers"  access="private" output="false" hint="Run some pre-processing before exporting, mainly checking that their account manager exists. This is a generic function to be run for all objects." index="5" direction="export" returnType="struct">

		<cfset var result = {isOK=true,message=""}>

		<!--- if we're exporting, then we need to find any account managers that going across that we don't yet have an ID for --->
		<cfif this.direction eq "export">
			<cfset doAccountManagersExistOnRemoteSystem()>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="doAccountManagersExistOnRemoteSystem" access="private" output="false" hint="Check that the account manager exists based on the value in the ownerID">

		<!--- if the ownerID field is being synched - we use the function 'getRemoteOwnerField' to work out what field it is that contains the ownerID.  --->
		<cfif listContainsNoCase(application.getObject("connector.com.connectorUtilities").getQueryTableMetaData(tablename=this.synchDataView).columnList,"exportUser#chr(172)#")>
			<cfset var accManagersTablename = "">
			<cfset var getAccountManagers = "">
			<cfset var matchColumns = {person="firstname,lastname,email"}>
			<cfset var accManagersErrorsTableName = "">
			<cfset var getRecordsWithoutAnAccountManager = "">
			<cfset var personIDMappedField = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity="person")>
			<cfset var relaywareIDField = application.getObject("connector.com.connectorUtilities").getRelaywareIDFieldInExportView(relaywareEntity=this.baseEntity)>
			<cfset var getAccountManagersResult = {}>
			<cfset var userObjectFields = application.getObject("connector.com.connector").getUserMatchingData(connectorType=this.connectorType).fieldStruct>
			<cfset var userOwnerFields = application.getObject("connector.com.connector").getRemoteOwnerField(connectorType=this.connectorType,object=this.baseObject)>

			<cfloop query="userOwnerFields">

				<cfset accManagersTablename = getTempTableName()>
				<cfset accManagersErrorsTableName = getTempTableName()>
			<!--- get the account managers that we don't have a crmID for. These should only apply for records where we don't have a remoteID (ie. the record is about to be created on the remote system)--->
			<cfquery name="getAccountManagers" result="getAccountManagersResult">
					select distinct p.firstname as #userObjectFields.firstname#, p.lastname as #userObjectFields.lastname#, p.email as #userObjectFields.email#, p.personID as #personIDMappedField#, v.#userOwnerFields.owner# as #getUserMatchingData().fieldStruct.ID#, 0 as isDeleted
					into #accManagersTablename#
				from #this.synchDataView# v
					inner join person p on p.personID = v.[exportUser#application.delim1##userOwnerFields.owner##application.delim1#personID]
 					where v.#userOwnerFields.owner# is null
					and v.#this.objectPrimaryKey# is null
			</cfquery>

			<cfset getAccountManagersResult.tablename = accManagersTablename>
			<cfset log_(logStruct=getAccountManagersResult)>
			<!--- attempt to match the records --->
			<cfset matchRecordsForExport(object=getUserMatchingData().userObjectName,matchColumns=matchColumns,tablename=accManagersTablename)>

			<cfquery name="getRecordsWithoutAnAccountManager">
					select distinct '#this.object#' as object, v.#relaywareIDField# as objectID, '#userOwnerFields.owner#' as field, 'Could not find user on #this.connectorType# with firstname '+p.firstname+' and lastname '+ p.lastname+' and email '+p.email as message,
					[exportUser#application.delim1##userOwnerFields.owner##application.delim1#personID] as value
 				into #accManagersErrorsTableName#
				from #this.synchDataView# v
					inner join person p on p.personID = v.[exportUser#application.delim1##userOwnerFields.owner##application.delim1#personID]
 					where v.#userOwnerFields.owner# is null
			</cfquery>
				<cfset logErrors(tablename=accManagersErrorsTableName)>
			</cfloop>
		</cfif>
	</cffunction>


	<cffunction name="preProcess_InsertOrganisations" access="private" output="false" hint="Perform the organisation import needed to import the locations. Only run if organisations are not mapped as an object." direction="import" object="location" returnType="struct">

		<cfset var result = {isOK=true,message=""}>

		<!--- if the organsiation is not mapped, but locations are mapped to accounts --->
		<cfif not application.getObject("connector.com.connector").isObjectMapped(object_relayware="organisation",connectorType=this.connectorType)>
			<cftry>
				<cfset var orgView = replaceNoCase(this.synchDataView,"location","organisation")>

				<!--- in this case, the matchnames would have been populated by the location matching. So, reset to deal with organisation matchnames --->
				<cfset var setOrganisationIDs = "">
				<cfset var locationIDMappedField = application.getObject("connector.com.connector").getMappedField(object_relayware=this.baseEntity,column_relayware="locationID").field>
				<cfset var convertedFromLeadIDMappedField = application.getObject("connector.com.connector").getMappedField(object_relayware=this.baseEntity,column_relayware="convertedFromLeadID").field>

				<cfif convertedFromLeadIDMappedField neq "">

					<cfset var updateLocationIDbaseOnConvertedLeadId = "">
					<!--- do some quick location matching when inporting accounts that have been converted as lead (partner registrations). We already have an organisationID on the location, and setting the locationID
						like this will give us the correct organisationID, as otherwise it will create a new org, which we don't want--->
					<cfquery name="updateLocationIDbaseOnConvertedLeadId">
						update #this.objectDataTable# set
							#locationIDMappedField# = l.locationID,
							organisationID = l.organisationID
						from #this.objectDataTable# sfAccount
							inner join lead on lead.crmLeadID = sfAccount.#convertedFromLeadIDMappedField#
							inner join location l on lead.leadID = l.convertedFromLeadID
						where len(#locationIDMappedField#) = 0
					</cfquery>
				</cfif>

			<!--- BF-here we are setting the organisationID where the location has an organisationID and where the org location HQ does not exist

					Update the organisationID on the salesforce_account table where the incoming HQ is not the same as the current org HQ
					and the current org HQ is no longer an HQ (we have to look in the location view to determine this)

					This is the case where the organisation HQ has gone from one location to another of it's location. In this case, we want to keep the same organisationID!
				 --->
				<cfif application.getObject("connector.com.connectorUtilities").isHQFieldMapped()>
					<cfset var HQFlagID = application.com.flag.getFlagStructure(flagID="HQ").flagID>
					<cfset var HQMappedField = getMappedField(object_relayware=this.baseEntity,column_relayware="HQ").field>

					<cfquery name="setOrganisationIDs">
						update hqAccount
							set organisationID = newHQLoc.organisationID
						from #this.objectDataTable# hqAccount
							inner join #orgView# ov on ov.upsertUniqueID = hqAccount.#this.objectPrimaryKey#
							inner join #this.synchDataView# newHQLoc on newHQLoc.upsertUniqueID = hqAccount.#this.objectPrimaryKey# and len(#HQMappedField#) = 0
							inner join #this.objectDataTable# childAccount on childAccount.#HQMappedField# = hqAccount.#this.objectPrimaryKey# and len(childAccount.#HQMappedField#) != 0
							inner join #this.synchDataView# currentHQLoc on currentHQLoc.upsertUniqueID = childAccount.#this.objectPrimaryKey#
							inner join integerFlagData newHQ on newHQ.flagID=#HQFlagID# and newHQ.entityID = newHQLoc.organisationID and newHQLoc.locationID != newHQ.data and newHQLoc.is_HQ = 1 <!--- where incoming HQ is not equal to the current HQ --->
							inner join integerFlagData currentHQ on currentHQ.flagID=#HQFlagID# and currentHQ.entityID = currentHQLoc.organisationID and currentHQLoc.locationID = currentHQ.data and currentHQLoc.is_HQ = 0 <!--- and currentHQ is no longer an HQ --->
						where hqAccount.organisationID is null
							and ov.organisationID is null
							and newHQLoc.organisationID is not null
					</cfquery>
				</cfif>

				<!--- TODO: probably need to blank down all required match fields --->
				<cfquery name="resetMatchNamesonSalesforceAccountTable">
					update #this.objectDataTable# set matchname = null
				</cfquery>

				<!--- match the organisation records. This will only update the matchnames for records to be inserted, but won't actually match as it's not a base entity, which is why we call matchRecords afterwards --->
				<cfset matchRecordsForImport(entityType="organisation",tablename=orgView)>

				<!--- if we're country scoping records, then set the countryID column on the base data table. Add it if it doesn't already exist. We have to match against the base table (hence we have to add the countryID to this table)
					as the organisationID column is a derived field on the org import view (ie. it contains an 'isNull' or a case statement - can't remember now) --->
				<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
					<cfset var updateCountryIDColumn = "">
					<cfset application.getObject("connector.com.connectorUtilities").addColumnToTable(tablename=this.objectDataTable,columnName="countryId")>

					<cfquery name="updateCountryIDColumn">
						update #this.objectDataTable#
							set countryID = v.countryID
						from #this.objectDataTable# d
							inner join #orgView# v on v.upsertUniqueID = d.#this.objectPrimaryKey#
					</cfquery>
				</cfif>

				<!--- <cfset application.com.dbTools.updateMatchFields(tablename=this.objectDataTable,entityType="organisation")> --->
				<!--- set the organisationId for any columns that match organisations in RW. Exclude any organisations that are HQs/parent accounts already. We also only want to match organisationID for just records where we are about to insert as the
					matchname has been set against just these records. If, for whatever reason we need to change this, then we need to ensure that the matchname on the base account table has been updated, as otherwise we're not matching properly --->

				<!--- put data into temp table to speed up matching process --->
				<cfset var newOrgTempTable = getTempTableName()>
				<cfset var putDataIntoTempTable = "">
				<cfquery name="putDataIntoTempTable">
					select * into #newOrgTempTable# from #orgView# where organisationID is null
				</cfquery>

				<cfset excludeRecordsWithCondition = {join="inner join #newOrgTempTable# v on v.upsertUniqueId = t.#this.objectPrimaryKey# left outer join integerFlagData ifd on ifd.entityID = d.organisationID and ifd.flagID=#application.com.flag.getFlagStructure(flagID='HQ').flagID# left outer join location l on ifd.data = l.locationID",whereClause="(ifd.entityID is null or l.crmLocID is null) and v.organisationID is null"}>
				<cfset application.com.matching.matchRecords(tablename=this.objectDataTable,entityType="organisation",uniqueIDColumnName=this.objectPrimaryKey,matchCondition=excludeRecordsWithCondition)>

				<!--- Should no longer need this now that we are matching properly above.
				<cfset var siteNameMappedField = application.getObject("connector.com.connector").getMappedField(object_relayware=this.baseEntity,column_relayware="sitename").field>

				<!--- if the location fails to insert, then we tend to insert a lot of empty organisations. here, we are trying to match to empty organisations that may
					have already been created --->
				<cfquery name="setOrganisationIDs">
					update #this.objectDataTable#
						set organisationID = o.organisationID
					from #this.objectDataTable# sfAccount
						inner join
							(select min(o.organisationID) as organisationID,organisationName
								from organisation o
									left join location l on l.organisationID = o.organisationID
								where l.locationId is null
								group by organisationName) as o on o.organisationName = sfAccount.#siteNameMappedField#
					where sfAccount.organisationID is null
				</cfquery> --->

				<!--- insert any organisations that have not matched --->
				<cfset var locOrgFieldsStruct = getOrganisationFieldsFromLocationFields()>
				<cfset var orgUpsertFields = "organisationID"> <!--- this mapping is set in the customMapping table if we have location based accounts --->
				<cfloop collection="#locOrgFieldsStruct#" item="locField">
					<cfset orgUpsertFields = listAppend(orgUpsertFields,locOrgFieldsStruct[locField])>
				</cfloop>
				<cfset var orgInsertResult = importData(tablename=orgView,entityType="organisation",columns_Updated=orgUpsertFields)>

				<cfif orgInsertResult.isOK and orgInsertResult.successResult.tablename neq "">
					<!--- set the organisationID column on the base table with the organisation IDs that we just inserted. --->
					<cfquery name="setOrganisationIDs">
						update #this.objectDataTable#
							set organisationID = t.organisationID
						from #this.objectDataTable# sfAccount
							inner join #orgInsertResult.successResult.tablename# t on sfAccount.#this.objectPrimaryKey# = t.upsertUniqueID
						where t.action = 'Insert'
					</cfquery>
				</cfif>

				<cfcatch>
					<cfdump var="#cfcatch#">
					<cf_abort>
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- Related Files --->
	<cffunction name="preProcess_DeleteRelatedFiles" access="private" output="false" hint="Deletes the physical file for related file records" index="10" returnType="struct" direction="import" object="relatedFile">

		<cfset var result = {isOK=true,message=""}>
		<cfset var errorStruct = {}>

		<cfif this.direction eq "import" and this.baseEntity eq "relatedFile" and getConnectorObject(object=this.baseEntity,relayware=true).allowDeletions>

			<cfset var getRelatedFilesToDelete = "">
			<cfset var getRelatedFilesToDeleteResult = {}>

			<cfquery name="getRelatedFilesToDelete" result="getRelatedFilesToDeleteResult">
				select v.#this.baseEntityObjectKey# as ID,body, rfc.filePath +'\'+rf.filename as filePath, rfc.secure
				from #this.synchDataView# v
					inner join relatedFile rf on rf.fileID = v.fileID
					inner join relatedFileCategory rfc on rfc.fileCategoryID = rf.fileCategoryID
				where v.isDeleted=1
			</cfquery>

			<cfset log_(label="Pre Processing: Delete Related Files",logStruct=getRelatedFilesToDeleteResult)>

			<cfloop query="getRelatedFilesToDelete">
				<cftry>
					<cfset var relatedFileBasePath = getRelatedFilesToDelete.secure[currentRow]?application.paths.secureContent:application.paths.content>
					<cfset var relatedFilePath = relatedFileBasePath &"\"&getRelatedFilesToDelete.filePath[currentRow]>

					<!--- delete the file --->
					<cfif fileExists(relatedFilePath)>
						<cfscript>
							fileDelete(relatedFilePath);
						</cfscript>
					</cfif>

					<cfcatch>
						<cfset errorStruct[getRelatedFilesToDelete.ID[currentRow]] = cfcatch.message>
					</cfcatch>
				</cftry>
			</cfloop>

			<cfset var countItems = structCount(errorStruct)>
			<cfif countItems>
				<cfset var loopCount = 0>
				<cfset var insertDeleteErrors = "">
				<cfset var relatedFilesErrorsTableName = getTempTableName()>
				<cfset var relatedFileCrmId = "">

				<cfquery name="insertDeleteErrors">
					select '#this.object#' as object,objectId,'Body' as field,message,null as value
					into #relatedFilesErrorsTableName#
					from (values
					<cfloop collection="#errorStruct#" item="relatedFileCrmId">
						<cfset loopCount++>
						(#relatedFileCrmId#,'#errorStruct[relatedFileCrmId]#')<cfif loopCount neq countItems>,</cfif>
					</cfloop>
					) as erroredFiles(objectID,message)
				</cfquery>

				<cfset logErrors(tablename=relatedFilesErrorsTableName)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="preProcess_RelatedFiles" access="private" output="false" hint="Read the files to be exported into a SQL table" index="5" direction="export" returnType="struct" object="RelatedFile">

		<cfset var result = {isOK=true,message=""}>
		<cfset var truncateBodyContent = "">

		<cfquery name="truncateBodyContent">
			truncate table connector_fileBody
		</cfquery>

		<!--- if we're exporting, then read the files into the connector_fileBody table --->
		<cfif this.direction eq "export">
			<cfset var getRelatedFileIDs = "">
			<cfset var insertFileBody = "">
			<cfset var relatedFilesErrorsTableName = getTempTableName()>
			<cfset var successStruct = {}>
			<cfset var errorStruct = {}>
			<cfset var relatedFileId = 0>

			<cfquery name="getRelatedFileIDs">
				select v.fileID, rfc.filePath +'\'+rf.filename as filePath, rfc.secure
				from #this.synchDataView# v
					inner join relatedFile rf on v.fileId = rf.fileID
					inner join relatedFileCategory rfc on rfc.fileCategoryID = rf.fileCategoryID
				where v.isDeleted=0
			</cfquery>

			<!--- currently, reading in files via CF. Want to use SQL, but don't know my time constraints --->
			<cfloop query="getRelatedFileIDs">
				<cfset var relatedFileBasePath = getRelatedFileIDs.secure[currentRow]?application.paths.secureContent:application.paths.content>
				<cfset var relatedFilePath = relatedFileBasePath &"\"&getRelatedFileIDs.filePath[currentRow]>

				<cfif fileExists(relatedFilePath)>
					<cftry>
						<cfscript>
							var fileBody = toBase64(fileReadBinary(relatedFilePath));
						</cfscript>

						<!--- <cfset successStruct[getRelatedFileIDs.fileId[currentRow]] = fileBody>
						NJH 2016/09/22 JIRA PROD2016-2178 - put data directly into table rather than storing it in a structure to be put in later. We were getting java heap space errors
							and this seems to have helped. Might be a bit slower but more stable.
						--->
						<cfset var insertFileBody = "">
						<cfquery name="insertFileBody">
							insert into connector_fileBody(fileID,body) values
								(<cf_queryparam value="#getRelatedFileIDs.fileId[currentRow]#" cfsqltype="cf_sql_integer">,<cf_queryparam value="#fileBody#" cfsqltype="cf_sql_varchar">)
						</cfquery>

						<cfcatch>
							<cfset errorStruct[getRelatedFileIDs.fileId[currentRow]] = cfcatch.message>
						</cfcatch>
					</cftry>
				<cfelse>
					<cfset errorStruct[getRelatedFileIDs.fileId[currentRow]] = "File not found: '#getRelatedFileIDs.filePath[currentRow]#'">
				</cfif>
			</cfloop>

			<!--- <cfset var countItems = structCount(successStruct)>
			<cfif countItems>
				<cfset var loopCount = 0>
				<cfset var insertFileBody = "">

				<cfquery name="insertFileBody">
					insert into connector_fileBody(fileID,body) values
					<cfloop collection="#successStruct#" item="relatedFileId">
						<cfset loopCount++>
						(#relatedFileID#,'#successStruct[relatedFileID]#') <cfif loopCount neq countItems>,</cfif>
					</cfloop>
				</cfquery>
			</cfif> --->

			<cfset countItems = structCount(errorStruct)>
			<cfif countItems>
				<cfset var loopCount = 0>
				<cfset var insertFileBodyErrors = "">

				<cfquery name="insertFileBodyErrors">
					select 'Related File' as object,objectId,'Body' as field,message,null as value
					into #relatedFilesErrorsTableName#
					from (values
					<cfloop collection="#errorStruct#" item="relatedFileId">
						<cfset loopCount++>
						(#relatedFileId#,'#errorStruct[relatedFileId]#')<cfif loopCount neq countItems>,</cfif>
					</cfloop>
					) as erroredFiles(objectID,message)
				</cfquery>

				<cfset logErrors(tablename=relatedFilesErrorsTableName)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- Create Related Files --->
	<cffunction name="postProcess_CreateRelatedFiles" access="private" output="false" hint="Creates the physical file for related files" index="10" returnType="struct" direction="import" object="relatedFile">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="synchDateTime" type="date" required="true" hint="The current time of the DB Server, so don't need to do UTC offsets">

		<cfset var result = {isOK=true,message=""}>
		<cfset var errorStruct = {}>
		<cfset var crmFilesUploaded= "">

		<cfif this.direction eq "import" and this.baseEntity eq "relatedFile">

			<cfset var getRelatedFilesToCreate = "">
			<cfset var getRelatedFilesToCreateResult = {}>

			<cfquery name="getRelatedFilesToCreate" result="getRelatedFilesToCreateResult">
				select v.#this.baseEntityObjectKey# as ID,body, rfc.filePath +'\'+rf.filename as filePath, rfc.secure
				from #this.synchDataView# v
					inner join #arguments.tablename# t on v.upsertUniqueId = t.upsertUniqueId
					inner join relatedFile rf on rf.fileID = v.fileID
					inner join relatedFileCategory rfc on rfc.fileCategoryID = rf.fileCategoryID
				where t.upsertUniqueId is not null
					and v.isDeleted = 0
			</cfquery>

			<cfset log_(label="Post Processing: Create Related Files",logStruct=getRelatedFilesToCreateResult)>

			<cfloop query="getRelatedFilesToCreate">
				<cftry>
					<cfset var relatedFileBasePath = getRelatedFilesToCreate.secure[currentRow]?application.paths.secureContent:application.paths.content>
					<cfset var relatedFilePath = relatedFileBasePath &"\"&getRelatedFilesToCreate.filePath[currentRow]>

					<!--- create the file --->
					<cfif getRelatedFilesToCreate.body[currentRow] neq "">
						<cfscript>
							fileWrite(relatedFilePath,toBinary(getRelatedFilesToCreate.body[currentRow]));
						</cfscript>
						<cfset crmFilesUploaded = listAppend(crmFilesUploaded,getRelatedFilesToCreate.ID[currentRow])>
					<!--- we have a problem. Need to create the file but we have no body --->
					<cfelse>
						<cfset errorStruct[getRelatedFilesToCreate.ID[currentRow]] = "Could not find attachment body">
					</cfif>

					<cfcatch>
						<cfset errorStruct[getRelatedFilesToCreate.ID[currentRow]] = cfcatch.message>
					</cfcatch>
				</cftry>
			</cfloop>

			<!--- update the uploaded fields on the base table --->
			<cfif crmFilesUploaded neq "">
				<cfset var setUploadedFields = "">

				<cfquery name="setUploadedFields">
					update relatedFile set
						uploaded=getDate(),
						uploadedBy=<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
					where
						crmRelatedFileID in (<cf_queryparam value="#crmFilesUploaded#" cfsqltype="cf_sql_varchar" list="true">)
				</cfquery>
			</cfif>

			<cfset var countItems = structCount(errorStruct)>
			<cfif countItems>
				<cfset var loopCount = 0>
				<cfset var insertFileBodyErrors = "">
				<cfset var relatedFilesErrorsTableName = getTempTableName()>
				<cfset var relatedFileCrmId = "">

				<cfquery name="insertFileBodyErrors">
					select '#this.object#' as object,objectId,'Body' as field,message,null as value
					into #relatedFilesErrorsTableName#
					from (values
					<cfloop collection="#errorStruct#" item="relatedFileCrmId">
						<cfset loopCount++>
						('#relatedFileCrmId#','#errorStruct[relatedFileCrmId]#')<cfif loopCount neq countItems>,</cfif>
					</cfloop>
					) as erroredFiles(objectID,message)
				</cfquery>

				<cfset logErrors(tablename=relatedFilesErrorsTableName)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>

</cfcomponent>