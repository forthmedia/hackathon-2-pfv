<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			connectorReportFunctions.cfm
Author:				NJH
Date started:		2010/09/30
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-10-12	IH	Case 428026 added showResumeSynchOnErrorReport

Possible enhancements:


 --->

<cf_includeJavascriptOnce template="/javascript/lib/jquery/spinner.js">
<cf_includeJavascriptOnce template="/connector/js/connectorReport.js">

<cfparam name="showResumeSynch" type="boolean" default="true">
<cfparam name="isDashboardStatusFailed" type="boolean" default="true">

<cfset functionQueryString = application.com.regExp.removeItemFromNameValuePairString(inputString=request.query_string,itemToDelete="action",delimiter="&")>

<cfscript>	
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Connector Functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

if (isDashboardStatusFailed) {
	comTableFunction.functionName = "Re-sync record now";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript: document.frmBogusForm.method='post';document.frmBogusForm.action='/connector/connectorStatusReport.cfm?action=syncNow&#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit(); //";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (showResumeSynch) {
	comTableFunction.functionName = "Resume sync in queue";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('Are you sure you want to resume synching the selected records?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/connector/connectorStatusReport.cfm?action=unsuspend&#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (isDashboardStatusFailed) {
	comTableFunction.functionName = "Delete errors";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript: if (confirm('Are you sure you want to delete the errors for the selected records?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/connector/connectorStatusReport.cfm?action=deleteError&#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}
</cfscript>


