/* ©Relayware. All Rights Reserved 2017 */
/**
 HISTORY
 When           Who 	What
 2017/02/07 	NK      Add spinner for running report function
 **/

jQuery(document).ready(function(){
    onFunctionListChange();
});

function startSpinnerFor(functionName) {
    if (checkboxValueList != "" && jQuery('#functionListQueryDiv option:selected').text() == functionName) {
        // add div for spinner
        jQuery('#showHideFilters').before(function() {
            return "<div id='synchSpinner' style='padding: inherit; margin:10px; width:16px; height:16px;'></div>";
        });
        jQuery('#synchSpinner').spinner({position:'center'});
    }
}

function onFunctionListChange() {
    var functionList = jQuery('#functionListQueryDiv');
    var functionDropdown = functionList.find('select');

    functionDropdown.removeAttr('onchange');
    functionDropdown.change(function () {
        checkboxValueList = checkboxValuesToList(document.forms["frmBogusForm"]["frmRowIdentity"]);
        startSpinnerFor('Re-sync record now');
        eventFunctionOnChange(this);
    });
}
