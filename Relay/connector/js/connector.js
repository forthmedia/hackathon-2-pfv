/* �Relayware. All Rights Reserved 2014 */

function showRelatedRecords(obj,queueID,recordType,connectorType) {
			
	var urlVars = window.location.href.toQueryParams();
	var data = {queueID:queueID,recordType:recordType,connectorType:connectorType}
	
	$infoTr = jQuery('#relatedRecords_'+queueID);
	if ($infoTr.length) {
		$infoTr.toggle(200);
	} else {
		for (var i = 0; i < urlVars.length; i++) {
			data[urlVars[i]] = urlVars[urlVars[i]];
		}

		jQuery.ajax(
	    	{type:'get',
	        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=connectorWS&methodName=getRelatedRecords&returnFormat=plain',
	        	data:data,
	        	dataType:'html',
	        	success: function(data,textStatus,jqXHR) {
					jQuery(obj).closest('tr').after(data);
					jQuery('#relatedRecords_'+queueID).addClass(jQuery(obj).parents('tr').attr('class'));
					jQuery('#relatedRecordDiv').show(200);
					jQuery('a.recordDataPopover').webuiPopover();
				}
			});
	}
	return false;
}


function viewEntityDetails(entityType,entityID) {
	if (entityType.toLowerCase() == 'person') {
		viewEntity(0,entityID,111,'',{openInOwnTab:true});
	}
	else if (entityType.toLowerCase() == 'location') {
		viewEntity(1,entityID,91,'',{openInOwnTab:true});	
	}
	else if (entityType.toLowerCase() == 'organisation') {
		viewEntity(2,entityID,71,'',{openInOwnTab:true});	
	}
	else if (entityType.toLowerCase() == 'opportunity') {
		openNewTab('Opportunity Edit '+entityID+'','Opportunity '+entityID+'','/leadManager/opportunityEdit.cfm?opportunityid='+entityID+'',{iconClass:'leadmanager'});
	}
	else if (entityType.toLowerCase() == 'product') {
		openNewTab('Product Edit '+entityID+'','Product '+entityID+'','/products/products.cfm?editor=yes&hideBackButton=true&productID='+entityID+'',{iconClass:'products'});
	}
	else if (entityType.toLowerCase() == 'pricebook') {
		openNewTab('Pricebook Edit '+entityID+'','Pricebook '+entityID+'','/products/pricebooks.cfm?editor=yes&hideBackButton=true&pricebookID='+entityID+'',{iconClass:'products'});
	}
	else if (entityType.toLowerCase() == 'lead') {
		openNewTab('Lead Edit '+entityID+'','Lead '+entityID+'','/lead/leads.cfm?editor=yes&hideBackButton=true&leadID='+entityID+'',{iconClass:'leadmanager'});
	}
}