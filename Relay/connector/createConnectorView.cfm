<!--- �Relayware. All Rights Reserved 2014 --->

<cfparam name="connectorType" default="#application.com.settings.getSetting("connector.type")#">

<cfsetting requesttimeout="300">

<cfset connector = application.getObject("connector.com.connectorUtilities").instantiateConnectorObject(connectorType=connectorType)>
<cfset connectorObjects = application.getObject("connector.com.connector").getPrimaryConnectorObjects()>
<cfset createView.successes = {import="",export=""}>
<cfset createView.errors = {import="",export=""}>

<cfloop query="connectorObjects">

	<cfquery name="createFlagView">
		exec createFlagView @entityName = '#object_relayware#', @viewName = 'v#object_relayware#'
	</cfquery>

	<cfif export>
		<cfset exportViewResult = connector.createConnectorView(direction="Export",object_relayware=object_relayware)>
		<cfif exportViewResult.isOK>
			<cfset createView.successes.export = listAppend(createView.successes.export,object_relayware)>
		<cfelse>
			<cfset createView.errors.export = listAppend(createView.errors.export,object_relayware)>
		</cfif>
	</cfif>

	<cfif import>
		<cfset importViewResult = connector.createConnectorView(direction="Import",object_relayware=object_relayware)>
		<cfif importViewResult.isOK>
			<cfset createView.successes.import = listAppend(createView.successes.import,object_relayware)>
		<cfelse>
			<cfset createView.errors.import = listAppend(createView.errors.import,object_relayware)>
		</cfif>
	</cfif>
</cfloop>

<cfloop list="successes,errors" index="successType">
	<cfloop list="export,import" index="direction">
		<cfoutput>
			<cfif listLen(createView[successType][direction])>
				#application.getObject("relayUI").message(message="#direction# #successType#: #createView[successType][direction]#",type=IIF(successType eq "successes",DE('success'),DE('error')))#
			</cfif>
		</cfoutput>
	</cfloop>
</cfloop>