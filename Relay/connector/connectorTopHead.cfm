<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="pageTitle" default="">
<cfparam name="addMapping" default="true">
<cfparam name="addRemoteObject" default="true">

<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="connector">

	<cfif findNoCase("connectorMapping.cfm",SCRIPT_NAME) and addMapping>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='connectorMapping.cfm?editor=yes&add=yes&hideBackButton=false&showSaveAndAddNew=true&objectId=#url.objectId#')#">
	</cfif>
	<cfif findNoCase("connectorObject.cfm",SCRIPT_NAME) and not structKeyExists(url,"editor")>
		<!--- we don't want to add a remote object if we don't have the API configured, as we are dependant on the API to add a new object --->
		<cfif addRemoteObject>
			<CF_RelayNavMenuItem MenuItemText="Add Import Object" CFTemplate="#application.com.security.encryptURL(url='/connector/configuration/connectorObject.cfm?editor=yes&add=yes&hideBackButton=true&relayware=0&connectorType=#connectorType#')#" id="addObject">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Add Export Object" CFTemplate="#application.com.security.encryptURL(url='/connector/configuration/connectorObject.cfm?editor=yes&add=yes&hideBackButton=true&relayware=1&connectorType=#connectorType#')#" id="addObject">
	</cfif>
</CF_RelayNavMenu>