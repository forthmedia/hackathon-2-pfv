/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.relayware.javacompilation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import org.eclipse.jdt.internal.compiler.tool.EclipseCompiler;

public class JavaCompilationHandler {

    JavaCompiler compiler;
    
    

    
    public JavaCompilationHandler(){
        

        this.compiler =new EclipseCompiler();
    }
    
    /**
     * Compiles the source and returns a Map (aka CF struct) that CF can use to 
     * report on success/failure
     * @param sourceDirectory
     * @param libraryPaths
     * @param compileDirectory
     * @return 
     */
    public CompilationReport compile(String sourceDirectory, List<String> libraryPaths, String compileDirectory){
        
        CompilationReport report=new CompilationReport();
        
        List<File> javaFilesToCompile=getJavaSourceFilesInDirectory(new File(sourceDirectory));
        
        
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();

        
        
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
        
        
        // This sets up the class path that the compiler will use.
        // I've added the .jar file that contains the DoStuff interface within in it...
        List<String> optionList = new ArrayList<String>();
        optionList.add("-d");
        optionList.add(compileDirectory);
        optionList.add("-source");
        optionList.add("1.7");
        optionList.add("-target");
        optionList.add("1.7");
        
        optionList.add("-classpath");
        
        String classPath=createClassPath(libraryPaths);
        optionList.add(createClassPath(libraryPaths));
        
        report.addRemark("ClassPath: " + classPath);
        
        Iterable<? extends JavaFileObject> compilationUnit
                = fileManager.getJavaFileObjectsFromFiles(javaFilesToCompile);

        
        JavaCompiler.CompilationTask task = compiler.getTask(
            null, 
            fileManager, 
            diagnostics, 
            optionList, 
            null, 
            compilationUnit);

        if (task.call()){

        }else{
            for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {
                if (diagnostic.getKind().equals(Kind.ERROR)){
                    System.out.format("Error on line %d in %s%n",
                    diagnostic.getLineNumber(),
                    diagnostic.getSource().toUri());
                    String errorResponse="Error on line " +  diagnostic.getLineNumber() + " in " +  diagnostic.getSource().toUri();
                    errorResponse+=" ";
                    errorResponse+=diagnostic.getMessage(Locale.ENGLISH);


                    
                    report.addError(errorResponse);
                    
                }
                
            }

        }
        try {
            fileManager.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return report;
    }
    
    private static List<File> getJavaSourceFilesInDirectory(File directory){
        File[] subPaths=directory.listFiles();
        List<File> containingFiles=new ArrayList<File>();
        
        for(File subPath:subPaths){
            if (subPath.isDirectory()){
                containingFiles.addAll(getJavaSourceFilesInDirectory(subPath));
            }else{
                if (subPath.getPath().endsWith(".java")){
                    containingFiles.add(subPath);
                }
            }
        }
        return containingFiles;
    }

    private static String createClassPath(List<String> libraryPaths) {
        StringBuilder classPathBuilder = new StringBuilder();
        for (String path : libraryPaths) {
            if (path.endsWith("*")) { //unwrap any "everything within this folder" dependancies
                path = path.substring(0, path.length() - 1);
                File pathFile = new File(path);
                for (File file : pathFile.listFiles()) {
                    if (file.isFile() && file.getName().endsWith(".jar")) {
                        classPathBuilder.append(path);
                        classPathBuilder.append(file.getName());
                        classPathBuilder.append(System.getProperty("path.separator"));
                    }
                }
            } else {
                classPathBuilder.append(path);
                classPathBuilder.append(System.getProperty("path.separator"));
            }
        }
        return classPathBuilder.toString();
    }
    
}