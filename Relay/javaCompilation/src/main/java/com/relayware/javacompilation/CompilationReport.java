/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.relayware.javacompilation;

import java.util.ArrayList;
import java.util.List;

/**
 * A report that is returned to ColdFusion, it should contain only CF friendly pieces
 * 
 * @author Richard.Tingle
 */
public class CompilationReport {
    
    private List<String> errors=new ArrayList<>();
    private List<String> otherRemarks=new ArrayList<>();

    public boolean isOk() {
        return errors.isEmpty();
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<String> getOtherRemarks() {
        return otherRemarks;
    }
    
    @Override
    public String toString(){
        StringBuilder builder=new StringBuilder();
        builder.append("Is Ok: ").append(isOk());
        builder.append("/n");
        
        builder.append("/nErrors:/n");
        
        for(String error:errors){
            builder.append(error).append("/n");
        }
        builder.append("/nOther Remarks:/n");
        for(String remark:otherRemarks){
            builder.append(remark).append("/n");
        }
        return builder.toString();
    }

    void addRemark(String remark) {
        otherRemarks.add(remark);
    }
    
    void addError(String error) {
        errors.add(error);
    }
    
    
    
}
