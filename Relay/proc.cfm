<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		process.cfm
Author:			WAB
Date created:	2001-03-02

Description:
Just a template to call the redirector - really just here so that the URL doesn't say redirector in it!


Amendment History:

Version 	Date (DD-MMM-YYYY)	Initials 	What was changed
1
NYB 2009/06/23 - P-FNL069 - changed to throwing a less leading error - since this file is open to all
GCC 2009/06/23 P-FNL069 Security Update 
2013-10-13 NYB Case 437143 added frmOrganisationID and frmLocationID
2014-04-09 NYB Case 439327 added error logger

--->


<cfparam name="prid" default="0">  <!--- WAB 2006-06-28 this was defaulting to 140 which is odd and meant that someone calling proc.cfm by itself could get very odd results --->
<cfparam name="stid" type="numeric" default="0">

<!--- GCC 2009/06/23 P-FNL069 Security Update - only allow current user to use process - this prohibits 'on behalf off' functionality  --->
<cfset frmPersonID = request.relayCurrentUser.personid><!--- <cfparam name="frmPersonID" default="#request.relayCurrentUser.personid#"> --->   <!--- WAB CR_SNY584 2006-05-22 - used to default to 404 --->
<cfparam name="frmProcessID" default="#prid#">
<cfparam name="frmStepID" default="#stid#"> 

<!--- START: 2013-10-13 NYB Case 437143 added: --->
<cfif not isdefined("frmLocationID") and not isdefined("frmOrganisationID")>
	<cfset PersonQry = application.com.RelayPLO.getPersonDetails(personid=frmPersonID)>
	<cfif not isdefined("frmLocationID")>
		<cfset frmLocationID = PersonQry.LocationID>
	</cfif>
	<cfif not isdefined("frmOrganisationID")>
		<cfset frmOrganisationid = PersonQry.Organisationid>
	</cfif>
</cfif>
<!--- END: 2013-10-13 NYB Case 437143 --->

<!--- 
NJH 2009/06/30 - P-FNL069 - removed call to file to mitigate security threat with users being
	able to set some global parameters.
<cfinclude template="/screen/setParameters.cfm">
 --->

<!--- NYB 2009/06/23 - P-FNL069 - changed to throwing a less leading error - since this file is open to all 
		WAB 2014-11-12 Add an errorID to the error message
--->
<cftry>
	<cfinclude template="/screen/redirector.cfm">
	<cfcatch type="any">
		<cfset errorID = application.com.errorHandler.recordRelayError_Warning(type="Workflow Error",Severity="error",caughtError=cfcatch)><!--- 2014-04-09 NYB Case 439327 added --->
		<cfoutput>Incorrect data import provided. (Error ID #errorID#) </cfoutput>
		<CF_ABORT>
	</cfcatch>
</cftry>



