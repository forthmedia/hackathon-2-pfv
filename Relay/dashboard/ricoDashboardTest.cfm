<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	
<style>
	.accordionTabTitleBar {
	   	font-size           : 12px;
		padding             : 4px 6px 4px 6px;
	   	border-style        : solid none solid none;
		border-top-color    : #EEEEE6;
		border-bottom-color : #EEEEE6;
	   	border-width        : 1px 0px 1px 0px;
	}
	.accordionTabTitleBarHover {
	   	font-size        : 11px;
		background-color : #EEEEE6;
		color            : #0b0b0b;
	}
	.accordionTabContentBox {
	   	font-size        : 11px;
	   	border           : 1px solid #1f669b;
	   	border-top-width : 0px;
	   	padding          : 10px;
	}

	.accordionTabContentBox p {
	   	font-size        : 11px;
	}
	.accordionTabContentBox ol, ul, li {
		list-style-type: disc;
		font-size: 11px;
		line-height: 1.8em;
		margin-top: 0.2em;
		margin-bottom: 0.1em; 
	{

</style>
</cf_head>


<cfoutput>
	<script src="/javascript/prototype.js" type="text/javascript"></script>
	<script src="/javascript/rico.js" type="text/javascript"></script>
</cfoutput>

<script>
function bodyOnLoad() {
	new Rico.Accordion( $('accordionDiv'), {panelHeight:340} );
	}
qChartWidgets = application.com.relayDashboard.getChartWidgets();	
</script>


<div id="2" style="width: 35%; float: right; vertical-align: top; margin-top:20px;">
	<div id="accordionDiv">
		<cfoutput query="qChartWidgets">
		<div id="overviewPanel">
			<div id="overviewHeader" class="accordionTabTitleBar">#htmleditformat(qChartWidgets.title)#</div>
			<div id="panel1Content" class="accordionTabContentBox">
				<cfinclude template="../dashboardWidgets/#qChartWidgets.chartFile#.cfm">
			</div>  
		</div>	
		</cfoutput>
	</div>
</div> 


