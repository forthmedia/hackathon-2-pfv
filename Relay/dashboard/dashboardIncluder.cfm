<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="dashboardStyles.cfm">
<cfoutput><script src="/javascript/openWin.js" type="text/javascript"></script></cfoutput>
<cfif not isDefined("templateURL")>
	The page you are trying to reach does not exist.
<cfelse>
	<cfoutput><a href="/dashboard/dashboardCustomisation.cfm?#request.query_string#">Customise</a><br></cfoutput>
	<cfif findNoCase("?",templateURL) gt 0 AND request.query_string neq "">
		<cfset templateURL = "#templateURL#?#request.query_string#">
	<cfelseif request.query_string neq "">
		<cfset templateURL = "#templateURL#?#request.query_string#">
	</cfif>
	<cfloop list="#request.query_string#" index="qVar" delimiters="&">
		<cfset "#listFirst(qVar,'=')#" = "#urlDecode(listLast(qVar,'='))#">
	</cfloop>
	<cfinclude template="/dashboardWidgets/#templateURL#">
</cfif>
