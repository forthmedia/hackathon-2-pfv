<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			dashboardCustomisation.cfm	
Author:				NM
Date started:		2005-09-11
Description:		Allows customisation of dashboards for users. they will be able to pick what charts display and customise the display from here. 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:
--->



<cfparam name="filter" default="">

<cfscript>
	qTabItems = application.com.relayDashboard.getAllUserTabItemsInModule("#filter#");
	tabID = application.com.relayDashboard.getTabID("#filter#");
</cfscript>

<cfif isDefined("form.save")>
	<cfloop query="qTabItems">
		<!--- set chart display --->
		<cfif structKeyExists(form,"display#chartName#")>
			<cfset userChartPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#.chart#id#.data_customisation.#customID#")>
			<cfset userChartPrefs.display = form["display#chartName#"]>
			<cfset application.com.preferences.saveUserPreference(hive="dashboard",key="tab#tabID#.chart#id#.data_customisation.#customID#",data=userChartPrefs)>
		</cfif>
		<!--- set chart title --->
		<cfif structKeyExists(form,"chartTitle#chartName#")>
			<cfset userChartPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#.chart#id#.data_customisation.#customID#")>
			<cfset userChartPrefs.chartTitle = form["chartTitle#chartName#"]>
			<cfset application.com.preferences.saveUserPreference(hive="dashboard",key="tab#tabID#.chart#id#.data_customisation.#customID#",data=userChartPrefs)>
		</cfif>
	</cfloop>
</cfif>

<cfset qNewTabItems = queryNew(qTabItems.columnList)>

<cfloop query="qTabItems">
	<cfscript>
		queryAddRow(qNewTabItems,1);
	</cfscript>
	<cfloop list="#columnList#" index="col">
		<cfscript>
			querySetCell(qNewTabItems,col,evaluate(col));
		</cfscript>
	</cfloop>
	<cfif isDefined("form.duplicate#chartName#") and form["duplicate#chartName#"] eq "true">
		<cfset uuID = "#dateFormat(now(),'ddmmyy')##timeFormat(now(),'HHmmss')#">
		<cfscript>
			queryAddRow(qNewTabItems,1);
		</cfscript>
		<cfloop list="#columnList#" index="col">
			<cfset value = evaluate(col)>
			<cfif lCase(col) eq "customid">
				<cfset value = "#customID#_#uuID#">
				<cfset newCustomID = value>
			</cfif>
			<cfif lCase(col) eq "chartname">
				<cfset value = "#chartName#_#uuID#">
				<cfset newchartName = value>
			</cfif>
			<cfscript>
				querySetCell(qNewTabItems,col,value);
			</cfscript>
		</cfloop>
		<cfset userChartPrefs = application.com.preferences.getUserPreference(hive="dashboard",key="tab#tabID#.chart#id#.data_customisation.#newCustomID#")>
		<cfset userChartPrefs.chartTitle = chartTitle>
		<cfset userChartPrefs.chartName = newChartName>
		<cfset userChartPrefs.display = true>
		<cfset application.com.preferences.saveUserPreference(hive="dashboard",key="tab#tabID#.chart#id#.data_customisation.#newCustomID#",data=userChartPrefs)>
	</cfif>
</cfloop>

<!--- <cfdump var="#qNewTabItems#"> --->

<cfset qTabItems = qNewTabItems>


<cf_head>
	<cf_title>Dashboard Customiser</cf_title>
	<style type="text/css">
		.show_item {
			display: block;
		}
		.hide_item {
			display: none;
		}
	</style>
	<SCRIPT type="text/javascript">
		function show_hide(objName) {
			var obj = document.getElementById(objName);
			if (obj.className == "hide_item") {
				obj.className = "show_item";
			} else {
				obj.className = "hide_item";
			}
		}
	</script>
</cf_head>


<form name="customiser" action="" method="post">
	<input type="hidden" name="save" value="true">
	<table class="withBorder">
		<tr>
			<th colspan="3">Select or Deselect charts:</th>
			<td><a href="javascript:document.customiser.submit();">Save</a></td>
		</tr>
		<tr>
			<th>Chart Title</th>
			<th>Display</th>
			<th>Hide</th>
			<th>Customise</th>
		</tr>
	<cfoutput query="qTabItems">
		<cfset userChartPrefs = application.com.preferences.getUserPreference(hive ="dashboard",key="tab#tabID#.chart#id#")>
		<cfset hasDisplay = structKeyExists(userChartPrefs.data_customisation[customID],"display")>
		<cfif hasDisplay>
			<cfset display = userChartPrefs.data_customisation[customID].display>
		<cfelse>
			<cfset display = "false">
		</cfif>
		
		<tr>
			<td>#userChartPrefs.data_customisation[customID].chartTitle#</td>
			<td><CF_INPUT type="radio" name="display#chartName#" value="true" checked="#iif( display,true,false)#"></td>
			<td><CF_INPUT type="radio" name="display#chartName#" value="false" checked="#iif( not display,true,false)#"></td>
			<td><CF_INPUT type="button" name="customise#chartName#" value="Customise" onClick="show_hide('customise_area#chartName#')"></td>
			<td><CF_INPUT type="hidden" name="duplicate#chartName#" id="duplicate#chartName#" value=""><CF_INPUT type="button" name="setDuplicate#chartName#" value="+" onClick="document.getElementById('duplicate#chartName#').value='true';document.customiser.submit();"></td>
		</tr>
		<tr>
			<td colspan="5">
				<span class="hide_item" id="customise_area#chartName#">
					<table width="100%" class="withBorder">
						<tr>
							<th colspan="2">Customiser:</th>
						</tr>
						<tr>
							<td>Rename your chart:</td>
							<td><CF_INPUT type="text" name="chartTitle#chartName#" value="#userChartPrefs.data_customisation[customID].chartTitle#"></td>
						</tr>
					</table>
				</span>
			</td>
		</tr>
	</cfoutput>
	</table>
</form>



