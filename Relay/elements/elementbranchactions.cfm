	<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			elementBranchActions.cfm	
Author:				NJH
Date started:		2006-10-25
Purpose:	to perform various actions on a tree branch. 
			
Usage: 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-10-15		WAB	chanegs to add more options (copying individual items etc)
2013-04-24		YMA	Case 434404 Update was ignoring change of frmNumberOfGenerations after form submit.

Possible enhancements:
	It currently supports cloning only.. deleting/removing a branch??

--->

<cf_translate>




<cf_head>
	<cf_title>Element Branch Action</cf_title>
</cf_head>



<cfparam name="action" type="string" default="cloneElementBranch">
<cfparam name="elementID" type="numeric" default=0>
<cfparam name="frmIncludeTopElement" type="numeric" default=1>
<cfparam name="frmNumberOfGenerations" type="numeric" default=99>

<!--- 2013-04-24	YMA	Case 434404 Update was ignoring change of frmNumberOfGenerations after form submit.--->
<cfif StructKeyExists(form,"frmNumberOfGenerations")>
	<cfset frmNumberOfGenerations=form.frmNumberOfGenerations>
</cfif>

<cfif isDefined("frmTargetElementID")>
	<cfif frmTargetElementID gt 0>
		<cfquery name="doesElementExist" datasource="#application.siteDataSource#">
			select ID from element where id =  <cf_queryparam value="#frmTargetElementID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
	</cfif>
	<cfif (frmTargetElementID gt 0 and doesElementExist.recordCount eq 0) or (frmTargetElementID lt 0)>
		<cfset errorMessage = "Please enter a valid target elementID">
	</cfif>
</cfif>

<cfif not isDefined("errorMessage") and isDefined("frmSubmit")>
	<cfif isDefined("action") and action eq "cloneElementBranch">
		<cfparam name="frmIncludeTopElement" default = 0>
		<cfset message =application.com.relayElementTree.cloneElementBranch (
			datasource=application.siteDataSource,
			ElementID =frmElementID,
			personID=request.relayCurrentUser.personID,
			targetElementID=frmTargetElementID,
			numberOfGenerations=frmnumberOfGenerations,
			IncludeTopElement = frmIncludeTopElement)>
	</cfif>
	<cfset application.com.relayElementTree.incrementCachedContentVersionNumber()>
	<cfoutput>
	<SCRIPT type="text/javascript">
		if(!opener.closed){
				// opener.location.href=opener.location.href;
			if (opener.d){
				opener.d.getChildNodes(#jsStringFormat(frmTargetElementID)# )
			}	
		}
		self.close();
	</script>
	</cfoutput>
</cfif>

<cf_querysim name = "s">
GenerationsQuery
displayValue,DataValue
Just This Element|1
Element and Children|2
Element, Children and Grand Children|3
All Generations|99
</cf_querysim>

<cfset request.relayFormDisplayStyle="HTML-table">
<cf_relayFormDisplay>
	<cfif isDefined("errorMessage")>
		<cf_relayFormElementDisplay relayFormElementType="Message" fieldname="frmErrorMsg" currentValue="#errorMessage#" label="">
	</cfif>
	<cfform name="elementActionForm" method="post" >
		<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmAction" currentValue="#action#" label="">
		<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmElementID" currentValue="#ElementID#" label="">
		
		<cf_relayFormElementDisplay relayFormElementType="numeric" fieldname="frmTargetElementID" label="phr_element_CopyBranchToElementID" currentValue="" required="yes">
		<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmNumberOfGenerations" label="phr_element_CopyBranchNumberOfGenerations"  required="yes" currentvalue = "#frmNumberOfGenerations#" query=#GenerationsQuery#>
		<cf_relayFormElementDisplay relayFormElementType="checkbox" fieldname="frmIncludeTopElement" label="phr_element_IncludeTopElement" value=1 currentValue="#frmIncludeTopElement#" >
		<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmSubmit" label="" currentValue="phr_element_cloneBranch" spanCols="yes" valueAlign="right">
	</cfform>
</cf_relayFormDisplay>




</cf_translate>
