<!--- �Relayware. All Rights Reserved 2014 --->
<!---  
WAB 2006-12-11 changed entity column to entityTypeID to fit in with all other tables on system (was causing dedupe problems with old name)
--->


<!---  update any ratings which have been passed--->
<!--- eventually would be a custom tag or something --->
<CFIF isDefined("rate_element")>
	<CFLOOP index="thisElement" list = "#evaluate("rate_element")#">

		<!--- check that this element really is there --->
		<CFIF isDefined("rate_element_#thisElement#_#frmPersonid#_orig")>

			<CFPARAM name = "rate_element_#thisElement#_#frmPersonid#" default="">
			<CFSET newRating = evaluate("rate_element_#thisElement#_#frmPersonid#")>
			<CFSET oldRating = evaluate("rate_element_#thisElement#_#frmPersonid#_orig")>			

			<!---  get EntityTypeID for element--->
			<cfset thisEntityTypeID = application.entityTypeID ['Element']>
			
			<CFIF oldRating is "" and newrating is not "">
				<CFQUERY NAME="checkForExisting" DATASOURCE="#application.SiteDataSource#">		
				select * 
				from  rating
				where entityTypeID =  <cf_queryparam value="#thisEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and entityid =  <cf_queryparam value="#thiselement#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>

				<CFIF checkforExisting.recordcount is 0 >
					<!--- no old record, new record exists --->			
					<CFQUERY NAME="insertRecord" DATASOURCE="#application.SiteDataSource#">		
					insert into  
					rating (entityTypeID, entityid, personid, rating, created, lastupdated)
					values (<cf_queryparam value="#thisEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#thiselement#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#newRating#" CFSQLTYPE="CF_SQL_Integer" >, <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
					</CFQUERY>

				<CFELSE>

					<CFQUERY NAME="updateRecord" DATASOURCE="#application.SiteDataSource#">		
					update rating
					set rating =  <cf_queryparam value="#newRating#" CFSQLTYPE="CF_SQL_Integer" > ,
					lastupdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
					where entityTypeID =  <cf_queryparam value="#thisEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and entityid =  <cf_queryparam value="#thiselement#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</CFQUERY>
				
				</CFIF>

			
			<CFELSEIF oldRating is not newrating >
				<CFQUERY NAME="updateRecord" DATASOURCE="#application.SiteDataSource#">		
				update rating
				set rating =  <cf_queryparam value="#newRating#" CFSQLTYPE="CF_SQL_Integer" > ,
				lastupdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
				where entityTypeID =  <cf_queryparam value="#thisEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and entityid =  <cf_queryparam value="#thiselement#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			</cfif>
		</cfif>
	</cfloop>
</cfif>






