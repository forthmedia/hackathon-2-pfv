<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:			ElementTreeNav.cfm
Author:				SWJ
Date started:		?

Purpose:	To output element navigation tree

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
Jan 2005   			WAB 		Altered to use new getElementBrancV2  stored procedure
Mar 2005			WAB 		Added filtering tree by logged in status and by usergroup
08-Apr-2005			RND 		Added js, style & xml code for right-click menu
2005-09-27			WAB			altered the searching for an individual item to first search the javascript array and then to bring back a section of the tree
								restructured the whole code a bit
2007-10-17 			 WAB          added some more copy items to context menu
WAB 2010/10/12 Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
2013/01/03			YMA			2013 Roadmap Content Deletion
2014-01-17			AXA			CASE 438620 - fixed the 'Show Tree View' option in context menu
29-09-2014 			SB 			Added div for styling
2015/12/09 			SB			Bootstrap and removed tables
 --->

<!--- WAB REMOVED THIS 2006-02-28 - causes problems with the context menus
	SB 2016-01-06 Removed inline stylesheet.
 --->
<div id="elementTreeNavContainer">

<cf_body onLoad="fnInit();" onclick="fnDetermine(event);" oncontextmenu="return false"/>

<!--- <cfinclude template="elementTopHead.cfm"> --->

<cfsavecontent variable="headerStyle_css">
</cfsavecontent>

<cfhtmlhead text="#headerStyle_css#">

	<cf_includeJavascriptOnce template = "/javascript/contextMenu.js">
	<cf_includeJavascriptOnce template = "/javascript/contextMenu_ElementTreeNav.js" translate="true">


	<CFHTMLHEAD TEXT='<LINK REL="stylesheet" HREF="/Styles/contextMenuStyles.css">'>

	<cfoutput>
	<!--- YMA 2012-12-24 2013 Roadmap Content Deletion.  Added archive option to page right click menus--->
	<xml id="contextDef" style="visibility:hidden;">
		<xmldata>
			<contextmenu id="page">
				<item id="pageHTML" value="Edit Content" ></item>
				<item id="pageAttributes" value="Edit Content Attributes" ></item>
				<item id="pageVisibility" value="Edit Visibility" ></item>
				<item id="pagePreview" value="Preview Page" ></item>
				<item id="pageAddChild" value="Add Child Page" ></item>
				<item id="pageAddSibling" value="Add Sibling Page" ></item>
				<item id="copyBranch" value="Copy Branch" ></item>
				<item id="copyChildren" value="Copy Children" ></item>
				<item id="copyItem" value="Copy Item" ></item>
				<item id="sendToArchive" value="Send to Archive" ></item>
				<item id="refreshTree" value="Load/Reload This Section" ></item>

			</contextmenu>
		</xmldata>
	</xml>

	<xml id="contextDef" style="visibility:hidden;">
		<xmldata>
			<contextmenu id="archiveOptions">
				<item id="pageHTML" value="Edit Content" ></item>
				<item id="pageAttributes" value="Edit Content Attributes" ></item>
				<item id="pageVisibility" value="Edit Visibility" ></item>
				<item id="pageRestore" value="Restore to Draft" ></item>
				<item id="pageDelete" value="Delete Page" ></item>

			</contextmenu>
		</xmldata>
	</xml>

	<xml id="contextDef" style="visibility:hidden;">
		<xmldata>
			<contextmenu id="element">
				<item id="pageHTML" value="Edit Content" ></item>
				<item id="pageAttributes" value="Edit Content Attributes" ></item>
				<item id="pageVisibility" value="Edit Visibility" ></item>
				<item id="pagePreview" value="Preview Page" ></item>
				<item id="pageAddChild" value="Add Child Page" ></item>
				<item id="pageAddSibling" value="Add Sibling Page" ></item>
				<item id="sendToArchive" value="Send to Archive" ></item>
				<item id="showOnTree" value="Show On Tree" ></item>

			</contextmenu>
		</xmldata>
	</xml>

	<div status="false" onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);"  id="oContextMenu"  class="menu"></div>
	</cfoutput>


<cfsetting 	enablecfoutputonly="yes">


	<cfset getTopElements = application.com.relayElementTree.getTreeTopNodes()>
	<cfset SitesAndTops = application.com.relayelementtree.getSitesAndTopElements()>

<!---
	replaced with call above
	<CFQUERY NAME="getTopElements" DATASOURCE="#application.SiteDatasource#" cachedwithin="#application.cachedContentTimeSpan#">
		-- #application.cachedContentVersionNumber# (this variable is used to flush the cache when the content is changed)
		SELECT Element.Headline, ElementType.Type, element.ID, element.elementTypeID
		FROM Element INNER JOIN
		     ElementType ON Element.ElementTypeID = ElementType.ElementTypeId
			 where Element.isTreeRoot = 1 and Element.islive = 1
			Order by sortorder, element.ID asc
			-- ElementType.Type, Element.headline
	</CFQUERY>
 --->

	<CFIF getTopElements.recordCount eq 0>
		There are no element tree roots defined.  In order to use this screen you must define at least one element tree root.  You can
		do this by going to Content Trees using the tab above.
		<CF_ABORT>
	</CFIF>

	<!--- WAB 2013-04-06 Add a spinner to Search text and make sure that the search div is open --->
	<cf_includejavascriptOnce template="/javascript/prototypeSpinner.js">
	<cfoutput>
	<script>
			function searchByID (theValue,resultsObjectName,useFilters) {
				if (typeof d == "undefined")  {
					// dtree not loaded
					document.searchForm.submit();
				} else {
					// dtree is loaded, look through array first
					id = d.getId(theValue) ;

					if (id == -1) {

						if (useFilters) {

							parameters = Form.serialize(document.searchForm)
							parameters = parameters.replace('goTo=','goToxx=')  // hack to get rid of parameter because we pass in in separately
						} else
						{
							parameters = ''
						}
						$(resultsObjectName).innerHTML = '<span style="margin-left:15px" id="searchingText">Searching ...</span>'  // margin leaves space for the spinner
						divtoggle_searchResults('open')
						$('searchingText').spinner({position:'left'});
						d.searchAndAddNodes (theValue,parameters,resultsObjectName)

					} else {
						if (divtoggle_tree){divtoggle_tree('open')}  // must make sure that the div is open if it exists
						d.openTo(id,true,false)
					};
				}

			}


	<!--- function blowElementCache() {

		if (confirm('This will slow down the next page request for every user on the live site - only do when required.  Continue?'))
		{
			page = '/webservices/misc.cfc?method=incrementCachedContentVersionNumber'
			var myAjax = new Ajax.Updater(
				'dummy',
				page,
				{
					method: 'post',
					parameters: '',
					evalScripts: true,
					evalJS: true
				});
		}
	}--->



	</script>


	<!--- do searching --->
	<cfparam name="searchIn" default="basic">


<!--- **************
Search Bit

******************--->


 	<!--- Params for the tree filters --->
	<cfparam name = "frmShowTreeForusergroups" default = "-1">
		<!--- if user has rights to just 1 or 2 countries then show tree for all of them, otherwise just show own country --->
		<cfif listlen(request.relaycurrentuser.countrylist) lte 2>
			<cfparam name = "frmShowTreeForCountryID" default = ""><!--- this will show all countries that user has rights to  --->
		<cfelse>
			<cfparam name = "frmShowTreeForCountryID" default = "#request.relaycurrentuser.countryid#"><!--- 16-May-05 SWJ: Changed to show all countries  2005-09-21 changed back by WAB after complaints over size of tree--->
		</cfif>

	<cfparam name = "frmShowTreeStatusID" default = "-1">   <!--- all statuses (except expired)  and all intermediates--->
	<cfparam name = "frmShowTreeForLoggedInPerson" default = "-1">  <!--- 1 = logged in, 0 = not logged in, -1 shows tree for both --->
	<cfparam name = "frmShowIntemediateStatuses" default = true>   <!---  --->
	<cfif listrest(frmShowTreeStatusID,"|") is 0>
		<cfset frmShowIntemediateStatuses =false>
	</cfif>
	<!--- include some queries used for displaying filters --->
	<cfinclude template="elementTreeNav_FilterQueries.cfm">


<cfif isDefined("ParentElementIDAndSiteName")>
	<cfset parentElementID = listfirst (ParentElementIDAndSiteName,"|")>
<cfelseif isDefined("session.ParentElementIDAndSiteName")>
	<cfset ParentElementIDAndSiteName = session.ParentElementIDAndSiteName>
<cfelseif isDefined("request.portaldomain")>  <!--- set in relay ini for apps with multiple boxes/sites --->
	<cfquery name="getParentID" dbtype="query">
	select topelementid, sitedomain from sitesAndTops where siteDomain = '#request.portaldomain#' and topelementid is not null
	</cfquery>
	<cfif getParentID.recordcount is not 0>
		<cfset ParentElementIDAndSiteName = "#getParentID.topelementid#|#getParentID.sitedomain#">
	</cfif>
</cfif>
	<cfparam name = "parentElementIDAndSiteName" default= "#SitesAndTops.topelementid#|#SitesAndTops.sitedomain#">
	<cfset parentElementID = listfirst (ParentElementIDAndSiteName,"|")>
	<cfset ElementTreeCacheID = application.com.relayelementTree.getElementTreeCacheID(parentElementID)>

	<!--- this allows other frames to know what site we are in - however it won't necessarily work for the recent edit links --->
 	<cfset session.ElementTreeTopEID = parentElementID>
	<!--- this is a bit of a hack so that I don't have to pass this variable on every form --->
 	<cfset session.ParentElementIDAndSiteName = ParentElementIDAndSiteName>




	<cfquery name="getTopElements" dbtype="query">
	select * from getTopElements where id not in (#valuelist(SitesAndTops.topelementid)#)
	</cfquery>


	<div class="01">

	<form name="siteForm">
		<div class="form-group">
		<label for="ParentElementIDAndSiteName">Choose a site to edit</label>
		<SELECT NAME="ParentElementIDAndSiteName" id="ParentElementIDAndSiteName" onChange="this.form.submit()" class="form-control">
		<cfloop query="SitesAndTops">
			<!--- PJP 18/12/2012 CASE: 430419: Dirty hack to remove certain IP address from drop down list for KERIO--->
			<cfif not isnumeric(replacenocase(SitesAndTops.siteDomain,'.','','all'))>
				<OPTION VALUE="#SitesAndTops.topElementID#|#SitesAndTops.siteDomain#" <cfif isDefined("ParentElementIDAndSiteName") and ParentElementIDAndSiteName is "#SitesAndTops.topElementID#|#SitesAndTops.siteDomain#">selected</CFIF>>#htmleditformat(SitesAndTops.SiteDomain)# (#htmleditformat(SitesAndTops.topelementid)#)</OPTION>
			</cfif>
			<!--- END CASE: 430419--->
		</cfloop>

		<cfloop query="getTopElements">
			<OPTION VALUE="#ID#" <cfif ParentElementID is ID>selected</CFIF>>#htmleditformat(Headline)# (#htmleditformat(id)#)</OPTION>
		</cfloop>
			</SELECT>
	</form>
	</div>
	<cfoutput>
	<script>
		<cfif listlen (ParentElementIDAndSiteName,"|") is 2>
			<cfset hostname = listgetAt(ParentElementIDAndSiteName,2,"|")>
			var  portalDomainAndRoot = '#jsStringFormat(application.com.relayCurrentSite.getSiteProtocol(hostname))##jsStringFormat(hostname)#'
		<cfelse>
			var portalDomainAndRoot	 = '#jsStringFormat(application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain())#'
		</cfif>

	</script>
	</cfoutput>



		<!--- START 2014-01-17 AXA CASE 438620 - remove the comments surrounding the <tr></tr> --->

		<!--- <div id=""><A HREF="javascript:blowElementCache()">Click to Update Cache</A></div> 2014-01-17 AXA CASE 438620 commented out this line --->
		<div id="dummy" style="display:none"></div>   <!--- When I upgraded to prototype 1.6 there was a problem with evalScripts.  ajax.updater no longer worked with '' as the div to update; ajax.request didn't seem to do evalScripts (but would have done content-type text/javascript if I had wanted ) --->


		<!--- END 2014-01-17 AXA CASE 438620 - remove the comments surrounding the <tr></tr> --->


<!--- **************
Search
******************--->
		<cf_divOpenClose
		divid="searchBlock"
		startOpen=false
		rememberSettings = false
		imagePassThrough = "align = left"
		>

		<div class="accordionElementTree">
			<div class="accordionElementTreeLink"<cf_divOpenClose_onClick>>
				Search
			</div>
			<div id="searchBlock" class="accordionElementTreeOpen" style="#divStyle#">
				<form name="searchForm" onsubmit = "javascript:searchByID(this.goTo.value,'searchResults',true);return false;">
					<div class="form-group">
						<label>Search for</label>
						<input type="text" name="goTo" value="" class="form-control">
						<CF_INPUT type="hidden" name="parentElementID" value = "#parentElementID#">
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="searchIn" value="basic" checked> Quick Search (Find words starting with search string)
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="searchIn" value="allContent" > Slow Seach (Find words containing search string)
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="searchCurrentTree" value="1" checked> Limit search to current tree
						</label>
					</div>
					<INPUT Type="submit" class="btn btn-primary" value="Search">
				</form>

				<cfset divOpen = structNew()>
				<cfset divOpen.Tree = true>
				<cfset divOpen.archiveSection = false>
				<cfset divOpen.recentElements = false>
				<cfset divOpen.pipeline = false>
				<cfset divOpen.searchResults = false>

		<!--- **************
		Search Results
		******************--->

				<cf_divOpenClose
				divid="searchResults"
				startOpen=#divOpen.searchResults#
				onOpenCloseDivIDs="recentElements,archiveSection,pipeline,tree"
				onOpenSetFocusTo = "topOfBlinds"
				rememberSettings = false
				imagePassThrough = "align = left"
				>
				<cfoutput>
					<div class="accordionElementTree2">
						<div class="accordionElementTreeLink"<cf_divOpenClose_onClick>>
							Search Results
						</div>
						<div class="accordionElementTreeOpen" id="searchResults" style="#divStyle#">
							<cfinclude template = "elementTreeNav_searchResults.cfm">
						</div>
					</div>
				</cfoutput>

				</cf_divOpenClose>

			</cf_divOpenClose>
			</div>
		</div>
	</cfoutput>

<!--- **************
Tree Bit
******************--->
	<cf_divOpenClose
		divid="tree"
		startOpen=#divOpen.Tree#
		onOpenCloseDivIDs="recentElements,pipeline,archiveSection,searchResults"
		onOpenSetFocusTo = "topOfBlinds"
		rememberSettings = false
		imagePassThrough = "align = left"
		>

		<cfoutput>
	 	<div class="accordionElementTree">
			<div class="accordionElementTreeLink"<cf_divOpenClose_onClick>>
				Tree View
			</div>
			<div class="accordionElementTreeOpen" id="tree" style="#divStyle#">
		</cfoutput>
				<cfinclude template="elementTreeNav_Tree.cfm">
		<cfoutput>
			</div>
		</div>
		</cfoutput>
	</cf_divOpenClose>

<!--- YMA 2012-12-24	2013 Roadmap Content Deletion.  Archive tree. --->
<!--- **************
Archive Tree
******************--->
	<cf_divOpenClose
		divid="archiveSection"
		startOpen=#divOpen.archiveSection#
		onOpenCloseDivIDs="tree,recentElements,pipeline,searchResults"
		onOpenSetFocusTo = "topOfBlinds"
		rememberSettings = false
		imagePassThrough = "align = left"
		>

		<cfoutput>
		<div class="accordionElementTree">
		 	<div class="accordionElementTreeLink"<cf_divOpenClose_onClick> >
				Archive
			</div>
			<SCRIPT type="text/javascript">
			function loadArchive(callback){
				var result = "";
				var page = '/webservices/ElementTreeNav.cfm?method=loadArchive';
				myAjax = new Ajax.Request(
					    	page,
					        {
					        	method: 'post',
		        				contentType: 'application/x-www-form-urlencoded',
		        				onComplete: function(transport){
		            				if (200 == transport.status) {
		                				result = transport.responseText;
										callback(result);
		            				}
		       					}
					    	});
			}

			loadArchive(function(result){document.getElementById('archiveSection').innerHTML = result;});
			</SCRIPT>
			<div class="accordionElementTreeOpen"  id="archiveSection" style="#divStyle#">
			</div>
		</div>
		</cfoutput>
	</cf_divOpenClose>

<!--- **************
ContentPipeline Bit
******************--->
	<cf_divOpenClose
	divID = "pipeline"
	startOpen = #divOpen.pipeline#
	rememberSettings = false
	onOpenSetFocusTo = "topOfBlinds"
	onOpenCloseDivIDs="recentElements,tree,archiveSection,searchResults"
	imagePassThrough = "align = left"
	>
		<cfoutput>
			<div class="accordionElementTree">
				<div class="accordionElementTreeLink"<cf_divOpenClose_onClick>>
					Content Pipeline
				</div>
				<div class="accordionElementTreeOpen" id="pipeline" style="#divStyle#">
					<cfinclude template = "elementTreeNav_PipeLine.cfm">
				</div>
			</div>
		</cfoutput>
	</cf_divOpenClose>

<!--- **************
Recent Edits
******************--->

	<!--- get recently edited elements from modregister
		We need to set monitoring on for the   lastupdated 	field (which I realise is a slightly odd thing to monitor)
		It is rather slow to search for all changes to any element field
	 --->
	<cfquery name="getModIDs" datasource="#application.siteDatasource#">
		select modentityid from modentityDef where tablename = 'element'
		and fieldname in ( 'lastupdated','Whole Record') and fieldmonitor = 1
	</cfquery>

	<cfif getModIDs.recordCount is 0>
		<cfoutput>Monitoring of updates has not been set up <BR></cfoutput>

	<cfelse>
		<cfif getModIDs.recordCount is not 2>
			<cfoutput>Full monitoring of updates has not been set up <BR></cfoutput>
		</cfif>

		<cfquery name="getRecentMods" datasource="#application.siteDatasource#">
			select top 15 recordid , e.id, parentid, headline, elementTypeID, case when ecd.elementid is null then 0 else 1 end as InTree, max(moddate) as moddate
				from
					modregister mr
						inner join
					element e on e.id = mr.recordid
						left join
					elementTreeCacheData ecd on ecd.elementid = e.id and ecd.elementTreeCacheID =  <cf_queryparam value="#ElementTreeCacheID#" CFSQLTYPE="CF_SQL_INTEGER" >
				where modentityid  in ( <cf_queryparam value="#valuelist(getModIDs.modentityID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and moddate > getdate()-30 and actionbycf = #request.relaycurrentuser.usergroupid#
				group by recordid , e.id, parentid, headline, elementTypeID, case when ecd.elementid is null then 0 else 1 end
				order by max(moddate) desc
		</cfquery>
		<cfquery name="getRecentMods" dbtype="query">
		select * from getRecentMods
		order by INtree desc, moddate asc
		</cfquery>

		<cfset getRecentElements = getRecentMods>





		<cf_divOpenClose
		divID = "recentElements"
		startOpen = #divOpen.recentElements#
		rememberSettings = false
		onOpenSetFocusTo = "topOfBlinds"
		onOpenCloseDivIDs="archiveSection,tree,pipeline,searchResults"
		imagePassThrough = "align = left"
		>


		<cfoutput>
			<div class="accordionElementTree">
				<div class="accordionElementTreeLink"<cf_divOpenClose_onClick>>
					My Recent Edits
				</div>

				<div class="accordionElementTreeOpen" id="recentElements" style="#divStyle#">

		</cfoutput>
					<cfoutput query="getRecentElements" group = "inTree">
						<CFif intree is 1>Items in Current Tree<cfelse>Items in Another Tree</CFIF>
						<ul>
							<cfoutput>
							<li>
								<a contextmenu="element" id = "re-#id#-#parentid#"  href="elementEdit.cfm?RecordID=#ID#&parentID=#parentID#" target="editFrame" title="EID #id#&##10Right Click For Options" class="smalllink">#htmleditformat(headline)#</A>
							</li>
				<!--- 					<a href="elementEdit.cfm?RecordID=#ID#&parentID=#parentID#&frmWizardPage=WYSIWYGEditor" target="editFrame" title="HTML editor" class="smalllink">H</A>
									<a href="elementEdit.cfm?RecordID=#ID#&parentID=#parentID#&frmWizardPage=Attributes" target="editFrame" title="Attributes" class="smalllink">A</A>
									<a href="elementEdit.cfm?RecordID=#ID#&parentID=#parentID#&frmWizardPage=visibility" target="editFrame" title="Visibility" class="smalllink">V</A>
									<a href="elementEdit.cfm?parentID=#ID#&typeID=#elementTypeID#" target="editFrame" title="Add Child" class="smalllink">+</A>
									<!--- WAB added this Add Sibling just to see if it worked - could be good if we had an easily understood symbol for it  --->
									<a href="elementEdit.cfm?parentID=#ParentID#" target="editFrame" title="Add Sibling" class="smalllink">.</a></A>
				 --->
							</cfoutput>
						</ul>
					</cfoutput>
				</div>
			</div>
		</cf_divOpenClose>
	</cfif>
