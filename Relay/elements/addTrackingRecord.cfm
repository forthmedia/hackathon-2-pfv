<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		ENTITY TRACKING

		David McLean 23 Nov 2001

		Revised 30 Sept 2002 DAM

		Three parts to this

		A. Manage click thru

			If a user has arrived at the site from a url in a Relay communication, and this
			can be determined and validated, it is recorded as a commdetail update.

		B. Manage comm type "Web Visit"

			If a user has visited the site for the first time today set a new comm
			to indicate the fact that he did so.

		C. Insert entity tracking record

			Always record the entity that has been visited against the current user.

--->


<!---
	DAM 30 Sept 2002
	  **********************************************************************************************
		Determine whether this is a click thru from a comm and whether it is valid to update
		the commdetail.
	  **********************************************************************************************

		1. There needs to be a CID and a PID defined and they both need to be valid

		2. If there is a session open does it match the PID?
			- If it doesn't we can't continue because we want to set the user cookie to
			  relate to the PID in the click through by using the cookie.user pid, if there
			  is a session open for a person other than that for the pid in the click thru, then
			  we can't change the cookie.user pid

		3. If there is no session open set a cookie to the click thru pid. We need to do this, because
		   although we have PID on the initial click thru because its in the URL, we need to have it when
		   the user clicks to another area on the site via the menu, so it needs to be in the cookie. In
		   addition to this, we can then record all subsequent visits for that user, even if they never logon.

		4. The user will not be logged on by this page although it does set a user cookie. If
		   the click thru points to secure content then the user will have been directed to logon
		   already and will have been posted here by "urlrequested".


	WAB 2004-09-14  changed so that adding entitytracking record query recognises cid
	WAb 2005-06-28	added a "top 1" to TestTracking query and gawain added indexes to this table to improve performance
	WAb 2005-09-13	see note above call to 	<CF_addCommDetail
	WAB 2013-11-21	2013RoadMap_ Item 25 Removed code for adding a 'Web Visit' record to commdetail.  Now done through activity based on entityTracking

--->
<CFIF isDefined('cid') and isDefined('pid') and isNumeric(pid) and isNumeric(cid) and pid neq 0 and cid neq 0>

	<!--- check to see if their is a matching commDetail record --->
	<CFQUERY NAME="checkIDPair" datasource="#application.sitedatasource#">
		SELECT Commid FROM commDetail
		WHERE Commid =  <cf_queryparam value="#cid#" CFSQLTYPE="CF_SQL_INTEGER" >  AND PersonID =  <cf_queryparam value="#pID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<!--- check that the personID is valid --->
	<CFQUERY NAME="checkPerson" datasource="#application.sitedatasource#">
		SELECT	Person.PersonID,
				Person.Language,
				UserGroup.UserGroupID
		FROM Person LEFT JOIN UserGroup ON Person.PersonID = UserGroup.PersonID
		WHERE Person.PersonID =  <cf_queryparam value="#pID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFIF checkPerson.recordcount eq 1>
		<CFSET PIDOK=false>
		<!---  Validate if there is an open session --->
		<CFIF request.relaycurrentuser.isLoggedIn>   <!--- WAB 2006-01-25 Remove form.userType  (usertype IS "External" AND isDefined("cookie.PartnerSession")) OR (usertype IS "Internal" AND isDefined("cookie.Session")) --->
			<!--- There is an open session so check that it matches PID --->
			<CFIF request.relayCurrentUser.personid EQ PID>
				<CFSET PIDOK=true>
			<CFELSE>
				<CFQUERY NAME="clickperson" datasource="#application.sitedatasource#">
					SELECT	firstname, lastname
					FROM Person
					WHERE Person.PersonID =  <cf_queryparam value="#pID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>
				<CFQUERY NAME="loggedperson" datasource="#application.sitedatasource#">
					SELECT	firstname, lastname
					FROM Person
					WHERE Person.PersonID = #request.relayCurrentUser.personid#
				</CFQUERY>
				<!---  error  the click thru is for a different person than the one who is logged on --->

			</CFIF>

		<CFELSE>
			<!--- There is no open session so set a cookie
				We're not going to log them on though
			 --->
			<CFQUERY NAME="getusergroups" DATASOURCE="#application.SiteDataSource#">
			SELECT usergroupid
			FROM rightsgroup
			WHERE PersonID =  <cf_queryparam value="#pID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
			<!--- If there is a usergroupid for this person then use it, otherwise
				use the web user usergroup --->
			<CFIF len(checkPerson.usergroupid) EQ 0>
				<CFSET ug=application.WebUserID>
			<CFELSE>
				<CFSET ug=#checkPerson.usergroupid#>
			</CFIF>
			<cfset application.com.globalFunctions.cfcookie(NAME="USER", VALUE="#checkPerson.PersonID#-#ug#-#valuelist(getUserGroups.UserGroupID)#", EXPIRES=#application.com.globalFunctions.getCookieExpiryDays()#)>
			<cfset application.com.globalFunctions.cfcookie(NAME="LANGUAGE", VALUE="#checkPerson.Language#")>
			<CFSET PIDOK=true>

		</CFIF>
	<CFELSE>
		<!--- Throw error because person id is invalid possibly a hack attempt --->

	</CFIF>


	<!--- if matching commDetail record found update it
			first get the relevant statusID
--->
	<CFIF checkIDPair.recordCount eq 1 AND PIDOK IS true>

		<CFQUERY NAME="getCommStatusID" datasource="#application.sitedatasource#">
			SELECT commStatusID
			FROM commDetailstatus
			WHERE name = 'Click thru'
		</CFQUERY>

		<CFQUERY NAME="UpdateCommDetailRecord" datasource="#application.sitedatasource#">
			UPDATE commdetail
			  SET
			  	feedback = N'Click thru',
			  	commStatusID =  <cf_queryparam value="#getCommStatusID.commStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			  	lastUpdated = getDate(),
			  	lastUpdatedBy =  <cf_queryparam value="#pid#" CFSQLTYPE="CF_SQL_INTEGER" >
			WHERE Commid =  <cf_queryparam value="#cid#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND PersonID =  <cf_queryparam value="#pid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<!--- set a cookie so that later hits in this session can be recorded against this commid --->
		<cfset application.com.globalFunctions.cfcookie(NAME="commid", VALUE="#CID#")>

	<CFELSE>
		<!--- Throw error because person id is invalid possibly a hack attempt --->

	</CFIF>
</CFIF>



<!--- ***********************************************************************

		Add an entityTrackingRecord
	  ***********************************************************************
--->
<!--- don't bother to track bots --->
<cfif request.relayCurrentUser.visitID neq 0>
	<!--- NJH 2013/10/01  - add visitID to the entityTracking table and changed to use function --->
	<cfset entityTrackingArgs = {entityID=elementid,entitytypeId="element",personID=request.relayCurrentUser.personid,fromUrl=cgi.http_referer,goToURL=request.currentSite.protocolAndDomain&request.query_string_and_script_name,visitID=request.relayCurrentUser.visitID}>
	<cfif structKeyExists(Cookie,"commid")>
		<cfset entityTrackingArgs.commID = cookie.commid>
	<cfelseif isdefined("cid") and isNumeric(cid)>
		<cfset entityTrackingArgs.commID = cid>
	</cfif>

	<cfset application.com.entityFunctions.addEntityTracking(argumentCollection=entityTrackingArgs)>
</cfif>
