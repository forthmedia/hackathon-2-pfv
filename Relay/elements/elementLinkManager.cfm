<!--- �Relayware. All Rights Reserved 2014 --->
<!---




Mods

WAB 2005-10-31    Added a bit of javascript so that on adverts it isn't possible to submit the form without selecting the host element
				Split out into two forms - one for changing type/country and one for adding links (meant didn't have to worry about submitting half completed forms when changing type
				Altered so can handle url.topeid being blank - now works out what tree we are in
SB 2015-12-11	Bootstrap and remove tables
 --->





<cf_head>
	<cf_title>Element Link Manager</cf_title>

<!---alert(document.linkListForm.elementLinkIDs.option.selected);
			 document.linkListForm.elementLinkIDs.selected = false;--->


</cf_head>


<cfparam name="form.elementLinkTypeTextID" type="string" default="Element">
<cfparam name="request.advertTreeTopID" type="numeric" default="1910">
<cfparam name="request.productCFC" type="string" default="relayProductTree">  <!--- NJH 2007-05-01 set in relayINI --->

<!--- here we're adding links --->
<cfif isDefined("FORM.elementLinkTypeTextID") and isDefined("FORM.elementID") and isDefined("FORM.LinkAction") and FORM.linkAction eq "Add Links" and isDefined("FORM.elementLinkIDs") and FORM.elementLinkIDs neq "">
	<cfloop index="i" list="#FORM.elementLinkIDs#">
		<!--- NJH 2007-05-01 added country parameter as we need the country for elementLinkType 'Products' --->
		<cfif isdefined('FORM.hostElementID')>
			<cfset o = application.com.elementTreeNav.setElementLink(elementID=FORM.elementID, entityID=i, elementLinkTypeTextID=FORM.elementLinkTypeTextID, hostElementID=FORM.hostElementID,countryID=FORM.countryID)>
		<cfelse>
			<cfset o = application.com.elementTreeNav.setElementLink(elementID=FORM.elementID, entityID=i, elementLinkTypeTextID=FORM.elementLinkTypeTextID,countryID=FORM.countryID)>
		</cfif>
	</cfloop>
</cfif>

<!--- here we're deleting links --->
<cfif isDefined("FORM.elementLinkTypeTextID") and isDefined("FORM.elementID")
	and isDefined("FORM.LinkAction") and FORM.linkAction eq "breakLink"
	and isDefined("FORM.entityID") and FORM.entityID neq "">
	<cfset d = application.com.elementTreeNav.deleteElementLink(elementID=FORM.elementID, entityID=FORM.entityID, elementLinkTypeTextID=FORM.elementLinkTypeTextID)>
</cfif>

<cfif not isdefined("FORM.countryID")>
	<cfset FORM.countryID = request.relaycurrentuser.content.showForCountryID>
</cfif>


<!--- check for a blank topeid - if so, work out which tree this element is in --->
<cfif not isDefined("topeid") or topeid is "">
	<cfset topeid = application.com.relayElementTree.whichTreeIsThisNodeIn (eid)>
</cfif>


<cfscript>
	qElementLinkTypes = application.com.elementTreeNav.getElementLinkTypes();
	qLinkedElements = application.com.relayElementTree.getLinkedElements(currentElementID = eid,topElementID = topEID);
	// NJH 2007-04-30
	if (qElementLinkTypes.recordCount eq 1 and qElementLinkTypes.elementLinkTypeTextID[1] eq "Product") {
		form.elementLinkTypeTextID = "Product";
	}
	// NJH 2007-04-30 a request variable set in relayINI tells us which cfc to use, client or relayware
	if ((form.elementLinkTypeTextID eq "Product")) {
		qElementTree = evaluate("application.com.#request.productCFC#.getLinkableProducts(countryID=#FORM.countryID#)");
	} else if (form.elementLinkTypeTextID eq "Category") {
		qElementTree = evaluate("application.com.#request.productCFC#.getLinkableNodes(countryID=#FORM.countryID#)");
	}
	//qElementTree = application.com.relayElementTree.getFullTreeFromCache(topElementID = URL.topEID);
	//get Countries for choice
	getCountries = application.com.relayCountries.getCountries(showCurrentUsersCountriesOnly="true");
</cfscript>
<!--- GCC - we only want live content to be linkable to --->
<cfif form.elementLinkTypeTextID eq "Element" or form.elementLinkTypeTextID eq "Advert">
	<cfquery name="qElementTree" datasource="#application.sitedatasource#" cachedwithin="#application.cachedContentTimeSpan#">
		exec GetElementTree @ElementID =  <cf_queryparam value="#topeid#" CFSQLTYPE="CF_SQL_INTEGER" > , @personid=-1, @Statusid=4, @depth = 0, @countryid =  <cf_queryparam value="#FORM.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
</cfif>

<cfif form.elementLinkTypeTextID eq "Advert">
<!--- GCC - Need a way to choose where adverts are sourced... --->
	<cfquery name="qElementImages" datasource="#application.sitedatasource#" cachedwithin="#application.cachedContentTimeSpan#">
		exec GetElementTree @ElementID =  <cf_queryparam value="#request.advertTreeTopID#" CFSQLTYPE="CF_SQL_INTEGER" > , @personid=-1, @Statusid=4, @depth = 0, @countryid =  <cf_queryparam value="#FORM.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset advertsUsedHere = "">
	<cfoutput query="qLinkedElements">
		<cfif elementlinktypetextID eq "Advert">
		 <cfset advertsUsedHere = listappend(advertsUsedHere, qLinkedElements.entityID)>
		</cfif>
	</cfoutput>
</cfif>


<script>
		function breakLink(entityID,elementLinkTypeTextID) {
			var testVar = confirm ('Remove this link?');
			if (testVar) {
				document.linkListForm.linkAction.value = "breakLink";
				document.linkListForm.entityID.value = entityID;
				document.linkListForm.elementLinkTypeTextID.value = elementLinkTypeTextID;
				document.linkListForm.submit();
			}
		}

<!---
		WAB: not needed - moved to using two forms instead (this code wasn't working when on an advert)
		function changeElementTypeID() {
			// reset elementLinkIDs so they don't attempt to update
			// loop until there is no selected value (This returns a -1)
			while (document.linkListForm.elementLinkIDs.selectedIndex != -1)
			{
    			// Unselect the current selected value
				document.linkListForm.elementLinkIDs.options[document.linkListForm.elementLinkIDs.selectedIndex].selected = false;
     		}
			document.linkListForm.submit();
		}
 --->


		function formVerification () {
			// checks that on both advert and host page are selected on adverts
			form = document.linkListForm
			if (form.elementLinkTypeTextID.value == 'Advert') {
				if (form.hostElementID.selectedIndex == -1) {
					alert ('Select the record you want to link the advert to')
					return false
				}
			}


			if (form.elementLinkTypeTextID.value == 'Element') {
				if (form.elementLinkIDs.selectedIndex == -1) {
					alert ('Select the record(s) you want to link to')
					return false
				}
			}



		}

	</script>


<cf_translate>


<h2>Element Link Manager</h2>
<p><strong>Current Element: <cfoutput>#htmleditformat(eid)# phr_headline_element_#htmleditformat(eid)#</cfoutput></strong></p>
<div class="table1-r1">

<cfform  method="POST" name="changeTypeForm" id="changeTypeForm">
	<cfoutput><CF_INPUT type="hidden" name="elementID" value="#eid#"></cfoutput>
		<div class="form-group">
			<label>Display the elements in the list below for:</label>
			<select name="countryID" onchange="javascript:this.form.submit()" class="form-control">
				<cfoutput query="getCountries">
				<option value="#getCountries.countryID#" <cfif form.CountryID eq countryID>SELECTED</cfif>>#htmleditformat(CountryDescription)#
				</cfoutput>
			</select>
		</div>
	<div class="form-group">
		<cfif qElementLinkTypes.recordCount gt 1>
			<label>What do you want this element to link to?</label>
			<cfselect class="form-control" name="elementLinkTypeTextID"
		          size="1"
		          query="qElementLinkTypes"
		          value="elementLinkTypeTextID"
		          display="ElementLinkType"
		          selected="#form.elementLinkTypeTextID#"
				  onchange="javascript:this.form.submit()">
			</cfselect></p>
		<cfelseif qElementLinkTypes.recordCount eq 1> <!--- NJH 2007-04-30 --->
			<cfoutput>
				<CF_INPUT type="hidden" name="elementLinkTypeTextID" value="#qElementLinkTypes.elementLinkTypeTextID#">
			</cfoutput>
			<cfset form.elementLinkTypeTextID = qElementLinkTypes.elementLinkTypeTextID>
		<cfelse>
			<input type="hidden" name="elementLinkTypeTextID" value="Element">
		</cfif>
	</div>
</cfform>


<cfform  method="POST" name="linkListForm" id="linkListForm">
	<cfoutput><CF_INPUT type="hidden" name="elementID" value="#eid#">
		<CF_INPUT type="hidden" name="elementLinkTypeTextID" value="#form.elementLinkTypeTextID#">
		<CF_INPUT type="hidden" name="CountryID" value="#form.CountryID#">

		</cfoutput>

	<cfif elementLinkTypeTextID eq "Advert">

		<div class="grey-box-top">
			<p>Step 1: Select the advert (for a Flash movie no further steps are required as the target is embedded in the movie)</p>
		</div>
		<cfset advertCount = qElementImages.recordcount - 1>
		<cfoutput query="qElementImages" startrow="2">
			<cfif listfind(advertsUsedHere,qElementImages.node) eq 0>
				<CF_translate phrases="detail" entitytype="element" entityid ="#qElementImages.node#"/>
				<CFSET nContent=evaluate("phr_detail_element_#qElementImages.node#")>
				<cfset nContent = ltrim(nContent)>


				<cfif left(nContent,4) eq "<IMG">
					<div class="radio">
						<label>
							<CF_INPUT type="radio" name="elementLinkIDs" value="#qElementImages.node#" onclick="return formVerification()">
							phr_detail_element_#qElementImages.node#
						</label>
					</div>
				<cfelseif left(nContent,7) eq "<object">
					<div>#nContent# <input type="Submit" value="Add movie" class="btn btn-primary">
				<CF_INPUT type="hidden" name="hostElementID" value="#eid#">
				<CF_INPUT type="hidden" name="elementLinkIDs" value="#qElementImages.node#">
					</div>
				<cfelse>
					<div>#nContent#</div>
				</cfif>
			<cfelse>
				<cfset advertCount = advertCount - 1>
			</cfif>
		</cfoutput>
		<cfif advertCount eq 0>
		<p><b>No available adverts. Create one under element <cfoutput>#request.advertTreeTopID#</cfoutput> to proceed.</b></p>
		</cfif>

		<div class="grey-box-top">
			<p>Step 2: Select the record you want to link the advert to.</p>
		</div>
		<select name="hostElementID" size="10" class="form-control">
			<cfoutput query="qElementTree">
			<option value="#qElementTree.node#"><cfif elementLinkTypeTextID eq "Element"><cfloop from="1" to="#generation#" index="inx">&nbsp;&nbsp;</cfloop></cfif>#htmleditformat(qElementTree.node)# #htmleditformat(headline)#</option>
			</cfoutput>
		</select>
		<input type="Submit" value="Add Links" onclick="return formVerification()" class="btn btn-primary">
	<cfelse>
	<div class="form-group">
		<label>Hold ctrl + click the records you want to link and then click Add Links</label>
		<select name="elementLinkIDs" size="10" multiple class="form-control">
			<cfoutput query="qElementTree">
			<option value="#node#"><cfif elementLinkTypeTextID eq "Element"><cfloop from="1" to="#generation#" index="inx">&nbsp;&nbsp;</cfloop></cfif>#htmleditformat(node)# #htmleditformat(headline)#</option>
			</cfoutput>
		</select>
	</div>
	<input type="Submit" value="Add Links" onclick="return formVerification()" class="btn btn-primary">
	</cfif>

	<input type="hidden" name="linkAction" value="Add Links">
	<input type="hidden" name="entityID" value="">
</cfform>
</div>
<div class="table1-r2">
	<table class="table">
	<tr>
		<td valign="top">
			<table class="table table-hover table-striped">
				<tr>
					<th>Entities this element links to:</th>
				</tr>
					<cfif qLinkedElements.recordcount gt 0>
						<cfoutput query="qLinkedElements" group="elementLinkTypeTextID">
							<tr>
								<td><strong>#htmleditformat(ELEMENTLINKTYPE)#</strong></td>
							</tr>
							<cfoutput>
								<TR>
									<TD><a href="javascript:breakLink(#entityID#,'#qLinkedElements.elementLinkTypeTextID#')">Remove this link</a><cfif qLinkedElements.elementLinkTypeTextID eq "Advert">&nbsp;#htmleditformat(headline)# linked to #htmleditformat(elementID)# phr_headline_element_#htmleditformat(elementID)#<cfelse>&nbsp;#htmleditformat(ElementID)# #htmleditformat(headline)# #htmleditformat(additionalDescription)#</cfif></TD>
								</TR>
							</cfoutput>
						</cfoutput>
					<cfelse>
				<tr>
					<td>None</td>
				</tr>
				</cfif>
			</table>
		</td>
		<td valign="top">
	<cfquery name = "otherElements" datasource="#application.sitedatasource#">
	SELECT     elementID AS node, additionalDescription, ELEMENTLINKTYPE, elementLink.elementLinkTypeTextID
		FROM         elementLink LEFT OUTER JOIN
		elementLinkSchema ON elementLink.elementLinkSchemaID = elementLinkSchema.elementLinkSchemaID
		inner join elementLinkType elt ON elementLink.elementLinkTypeTextID = elt.elementLinkTypeTextID
		WHERE     (hostelementID =  <cf_queryparam value="#eid#" CFSQLTYPE="CF_SQL_INTEGER" > )
		order by elementLink.elementLinkTypeTextID
	</cfquery>

			<table class="table table-hover table-striped">
				<tr>
					<th>Other entities that currently link to this one:</th>
				</tr>
				<cfif otherElements.recordcount gt 0>
					<cfoutput query="otherElements" group="elementLinkTypeTextID">
						<tr>
							<td><strong>#htmleditformat(ELEMENTLINKTYPE)#</strong></td>
						</tr>
						<cfoutput>
							<tr>
								<td><cfif elementLinkTypeTextID eq "element" or elementLinkTypeTextID eq "Advert" ><a href="elementLinkManager.cfm?eid=#node#&topEID=#TopEID#" target="_self">Go to this elements links</a></cfif>&nbsp;<cfif elementLinkTypeTextID eq "element" or elementLinkTypeTextID eq "advert">#htmleditformat(node)# phr_headline_element_#htmleditformat(node)#<cfelse>#htmleditformat(node)# #htmleditformat(additionalDescription)#</cfif></td>
							</tr>
						</cfoutput>
					</cfoutput>
				<cfelse>
				<tr><td>None</td></tr>
				</cfif>
			</table>
		</td></tr>
	</table>
</div>

</cf_translate>


