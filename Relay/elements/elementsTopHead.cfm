<!--- �Relayware. All Rights Reserved 2014 --->

<!---
Mods

WAB 2005-10-31 - altered Links link to handle session.ElementTreeTopEID not being defined
NJH 2009/05/07	 Bug Fix Issue All Sites 2189 - removed XML link
WAB 2010/10/12 Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
 --->


<cfparam name="wizardPagesToShow"  default= "all">
<cfparam name="wizardPagesWhichCanBeHidden" DEFAULT= "WYSIWYGEditor,attributes,visibility,links">
<cfparam name="WizardPagesWhichAlwaysShow" DEFAULT= "view,stats,mods,xml,save">
<cfparam name="WizardPagesNotOnNewRecords" DEFAULT=  "attributes,visibility,links,stats,mods">
<cfparam name="pageTitle" default="Element Edit">

<cfif wizardPagesToShow is "all">
	<cfset wizardlist = listappend(wizardPagesWhichCanBeHidden,WizardPagesWhichAlwaysShow)>
<cfelse>
	<cfset wizardlist = listappend(wizardPagesToShow,WizardPagesWhichAlwaysShow)>
</cfif>

<cfset menuItemsText = {wysiwygeditor="WYSIWYG",Attributes="Attributes",visibility="Visibility",stats="Statistics",mods="Modifications"}>

<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="/elements">

	<CFIF findNoCase("elementEdit.cfm",SCRIPT_NAME) neq 0  OR (Isdefined("ThisPageName") AND ThisPageName IS "elementEdit.cfm")>
		<cfloop index = "item" list = "#wizardlist#">
			<cfif isNumeric(RECORDID) or listFindNoCase (WizardPagesNotOnNewRecords,item) is 0>

			<cfswitch expression=#item#>
				<cfcase value="wysiwygeditor,Attributes,visibility,stats,mods">
					<CF_RelayNavMenuItem MenuItemText="#menuItemsText[item]#" CFTemplate="##" onclick="javascript:submitForm('#item#',1);return false;">
				</cfcase>
				<!--- <cfcase value="Attributes">
					<CF_RelayNavMenuItem MenuItemText="Attributes" CFTemplate="##" onclick="javascript:submitForm('attributes',1);return false;">
				</cfcase>
				<cfcase value="visibility">
					<CF_RelayNavMenuItem MenuItemText="Visibility" CFTemplate="##" onclick="javascript:submitForm('visibility',1);return false;">
				</cfcase> --->
				<cfcase value="links">
					<cfset tempTopEID = structKeyExists(session,"ElementTreeTopEID")?session.ElementTreeTopEID:"">
					<CF_RelayNavMenuItem MenuItemText="Links" CFTemplate="##" onclick="javascript:{void(openWin( 'elementLinkManager.cfm?eid=#recordID#&topEID=#tempTopEID#','PopUp','width=750,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));return false;}">
				</cfcase>
				<cfcase value="view">
					<cfif structKeyExists(session,"parentElementIDAndSiteName")>
						<cfset portaldomain = listlast(session.parentElementIDAndSiteName,"|")>
					<cfelseif structKeyExists(request,"portaldomain")> <!--- set in relay.ini --->
						<cfset portaldomain = request.portaldomain>
					<cfelse>
						<cfset portaldomain = application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain()>
					</cfif>
					<CF_RelayNavMenuItem MenuItemText="Preview" CFTemplate="#application.com.relayCurrentSite.getSiteProtocol(portaldomain)##portaldomain#/?#application.com.relayelementTree.createSEIDQueryString (linktype = "preview",elementid = recordid)#" target="ContentPreview" description="Preview this page on #portaldomain#. #chr(10)#You must be logged in #chr(10)#You can email this link to any Relay User ">
				</cfcase>
				<!--- <cfcase value="stats">
					<CF_RelayNavMenuItem MenuItemText="Statistics" CFTemplate="##" onclick="javascript:submitForm('stats',1);return false;">
				</cfcase>
				<cfcase value="mods">
					<CF_RelayNavMenuItem MenuItemText="Modifications" CFTemplate="##" onclick="javascript:submitForm('mods',1);return false;">
				</cfcase> --->
				<cfcase value="xml">
				</cfcase>
				<cfcase value="save">
					<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="##" onclick="javascript:submitForm('#frmWizardPage#',0);return false;">
				</cfcase>

				<cfdefaultcase>
					<CF_RelayNavMenuItem MenuItemText="#item#" CFTemplate="##" onclick="javascript:submitForm('#item#',1);return false;">
				</cfdefaultcase>

			</cfswitch>
			</cfif>
		</cfloop>

	</CFIF>

</CF_RelayNavMenu>