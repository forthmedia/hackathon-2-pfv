<!--- �Relayware. All Rights Reserved 2014 --->
<!--- elementNew.cfm

Author WAB

Date 2000-06-16

Purpose:  To create a new element.  

Action:
	Is passed a ParentID and (optionally) a TypeID
	If TypeID is defined then control is passed directly to elementEdit.cfm
	If TypeID is not defined, then we look to see what element types are allowed as children of this parent
	If there is only one allowed type, then TypeID is set and control passes to ElementEdit.cfm
	If there are mutiple types allowed, then user is asked to select the type

2016-02-03	WAB Mass replace of displayValidValues using lists to use queries
 --->
 
 
<CFPARAM name="parentID">

<CFIF isDefined("typeID")>
	<CFINCLUDE template="elementEdit.cfm">
	<CF_ABORT>
</cfif>


<CFQUERY NAME="GetParentType" DATASOURCE="#application.SiteDataSource#">
	select elementTypeID from element where id =  <cf_queryparam value="#parentID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<CFQUERY NAME="GetTypes" DATASOURCE="#application.SiteDataSource#">
select elementTypeID, Type
from elementType
where ','+rtrim(ParentElementTypeID)+','  like  <cf_queryparam value="%,#getParentType.elementTypeID#,%" CFSQLTYPE="CF_SQL_VARCHAR" >    
<!--- slightly cumbersome way of looking for matches in a delimited  
list (all the commas and things prevent say 2 matching with 21,22,23)--->
</CFQUERY>


<CFIF getTypes.RecordCount is 1>
	<CFSET typeID = getTypes.elementTypeID>
	<CFINCLUDE template="elementEdit.cfm">
	<CF_ABORT>

<CFELSEIF getTypes.RecordCount is 0>

	No child types defined for this parent
	
<CFELSE>

	<FORM name="myForm" action="elementEdit.cfm" method="post">
		<CFOUTPUT>

		<CF_INPUT Type="hidden" name = "parentID" value="#parentID#">
	
		<cf_displayValidValues
			formFieldName="TypeID"
			validValues = "#getTypes#"
			dataValueColumn = "elementTypeID"
			displayValueColumn = "Type"
			NullText= "Please select the type of element you wish to create"
			onChange="javascript:document.myForm.submit()"
		>	
		
		</cfoutput>	
	</form>

</CFIF>

