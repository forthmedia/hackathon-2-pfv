﻿<!--- ©Relayware. All Rights Reserved 2014 --->


<!---

Mods
		2001-03-08		WAB		Modified call to get phrase so that it doesn't bring back a translation link
		30/Jun/01	SWJ		Added statusID management
		1/Jul		SWJ		Added CFIF around date and URL wizard
		05/Sep/01	DAM		Added code for elementRecord.SHOWONMENU
		2001-09-13	SWJ		Fixed bug in elementRecord.SHOWONMENU code - missing VALUE from definition
		2001-10-11	CPS		Only perform 'Sel All_' when either Country Scope or Record Rights is set.
						    Also fixed a bug by setting the referrerForm parameter to ElementEdit.cfm
		2001-10-19	SWJ		Modified the menu so that it shows at the top and conforms with the
							new look and feel.  Also modifed order so that this can be passed as a param
		2001-10-26	CPS		Incorporate ewebeditpro functionality
		2002-03-18	SWJ		Changed length of URL field
		2002-07-14	SWJ		Some major work to control what shows and when.  I've created
							a variable called frmWizardPage.  This is modified by the formSubmit in elementTopHead.
							At about line 150 you will see I have a case statement which switches on and off the
							fields.  Each field adds itself to formFields if it showing and this works in elementActionsV2.
							Most of the fields also check elementType column values but some have been depracated.
		2002-11-21	SWJ		Added the textAreaControlBar
		2004-07-07		WAB		altered name of submit function - was conflicting with another one being loaded automatically
		2004-08-19	GCC		Added Expand and Shrink JS code to the textarea editor
		2004-08-21	SWJ		Added current parent to the possibleParents query structure

		2005-03-29
		Mar 2005			WAB     Lots of stuff!
		2005-04-25	WAB		altered the drop down for changing parents to include more items
		2005-05-18		WAB 	and then removed some of them (parent can't be an exisitng child!)
		2005-10-19	WAB		fixed bug when frmCountryID = "" and frmLanguageID was set
				WAB		Added ability to default the "goLive" date to x days in the future
		2005-12-05		WAB		fixed javascript bug with the goLive thing (when already live)

		2006-07-24   WAB added code for delaying updating of the element cache until editor presses a link on the editing page
		2007-03-12	WAB 	corrected bug with parentId getting set to 0
		2007-08-08	JvdW	added check for specific WYSIWYG stylesheet (lines 1424 - 1439)
		2008-02-04	AJC	Bugs - Sony - WYSIWYG Not Expanding
		2009/02/02	WAB for SWJ added code to change the name of the tab
		2009/02/09   WAB	put JSStringFormat in
		2009/02/09 	NJH		P-SNY047 Added cfform as usergroup selection is now using cfselect
		WAB 2009/09/07 LID 2515 Removed #application. userfilespath#
		2009/11/05	NJH		LID 2638 - only bring back domains from sitedef that are set (ie. not null or blank)

		2009/03/10  WAB   Change to getEntityPhrases - added exactMatch param
		2009/04/30	NJH		P-FNL003 set FCK as content editor.
		2010/09/15  WAB  	LID 3905 - Deal with nodes which have GetChildrenFromParentID set (also change in elementTreeNav webservice)
	2010/10/12	WAB	removed references to application.site def and application.external userdomains
		2011/06/21	NJH		Removed blowelementcache link. WYSIWYG changes. Changes showSummary to radio button.
		2011/10/13	WAB		LID 7965 Fixed javascript code around 'goLiveDate' so that it understands US dateformat (note, needs updated com.datefunctions.cfc)
		2012/02/29 	PPB 	Case 426749 What's New list not refreshing
		2012-09-21	PPB 	Case 430402 add translate="true" to restrictKeyStrokes.js call
		2012-10-10 	PPB 	Case 430878 add getContentFromID to fieldList
		2012/10/25	NJH		Social CR - add ability to turn sharing off for a particular content
		2013-04-18 	PPB 	Case 433761 restrict enable content from/to datepickers to "1st Jan this year onwards"
		2015-01-28	AHL		Case 442825 Creating Child Pages shows Parent in designer
		2015-10-23	ESZ		Case 445605 User should not be able to save | add a content page without a name
		2015-11-05  ESZ		Case 444875 Page Sorting in Content Manager raised to 300
        2015-11-30  ACPK    BF-31 Set Headline field as required
		2015/12/30	NJH		BF-134 - pass through recordID to element topHead.
		2015-11-19 	WAB 	During PROD2015-290 Visibility Project. Removed pieces of code which were never used
        2015-11-30  ACPK    BF-31 Set Headline field as required
		2016-01-04	SB		Change layout
		2016/02/15	NJH		Use rwFormValidation - removed js alerts. Call triggerFormSubmit in relayEditForm.js
		2016-10-11	WAB 		PROD2016-2503 Content Tree not getting blown when loginrequired changed.  	 Added loginrequired to list of fields which make tree dirty


Stuff to do on this template:

- elementRecord.SHOWONMENU code needs to be context specific to element type
- Work out how to Edit the extended content fields i.e. summary, long summary
- develop method for inserting common HTML strings into text
- develop method for inserting elementTags and parameters
- Controlling who can modify content status
- Controlling who can modify which country specific content
- need to add logic to check we have a URL if one is required for isExternal

Testing this template
- is translations working perfectly
- are their switches for all fields

--->


<!---
WAB 2006-11-21
section for reloading items in the navigation - new dtree menu --->
<cfif structKeyExists(url,"reloadTreeForID")>

<cfhtmlhead text="
	<script>
	if (parent.NavBar) {
		if (parent.NavBar.d && parent.NavBar.d.getChildNodes) {
			parent.NavBar.d.getChildNodes(#jsStringFormat(reloadTreeForID)#) ;
		}
	}
	</script>
">
</cfif>



<!--- This form requires the following variables recordID  --->
<CFPARAM NAME="DefaultTemplate" DEFAULT="et.cfm">
<CFPARAM name="recordID" default="NewRecord">
<cfset isNewRecord = iif (isNumeric(recordID),false,true)>

<cfparam name="frmWizardPage" type="string" default="WYSIWYGEditor"> <!--- NJH set the editor as the default view --->

<cfparam name="editMode"  default="true">

<cfparam name="application.ElementGoLiveDelayDays" default = "0">

<cf_include template = "/templates/relayFormJavaScripts.cfm">
<cf_includeJavascriptOnce template = "/javascript/openwin.js">
<cf_includeJavascriptOnce template = "/javascript/checkobject.js"> <!--- WAB 2009/05/02 for the isDirty function --->
<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
<cf_includejavascriptonce template = "/javascript/restrictkeystrokes.js" translate="true">		<!--- 2012-09-21 PPB Case 430402 add translate="true" --->
<cf_includeJavascriptOnce template="/javascript/rwFormValidation.js"> <!--- NJH 2016/02/15 - done a manual include here as when in modifications, we don't have relayFormDisplay included, so triggerFormSubmit is not defined. Needs a re-write, but left if for now --->

<!--- YMA	2013/03/06	CASE:433220 Prevent anything from happening if the element was deleted already! --->
<cfif isNumeric(recordID)>
<cfquery name="elementExists" datasource="#application.sitedatasource#">
	select 1 from element where ID =  <cf_queryparam value="#recordID#" CFSQLTYPE="cf_sql_integer" >
</cfquery>
<cfif elementExists.recordcount eq 0>Page does not exists.<cf_abort></cfif>
</cfif>

<!--- WAB 2005-10-12
	added a link to the statistics pages from the top menu - had to be rather crowbared in! --->
<cfif frmwizardPage is "stats">

	<cfset thisPageNAme = "elementEdit.cfm">
	<!--- <cfinclude template="#thisDir#/elementTopHead.cfm"> --->
	<cfset application.com.request.setTopHead(frmwizardPage=frmwizardPage,recordID=recordID)>
	<cfinclude template = "..\report\websiteactivity\elementStatsIndividual.cfm">
	<CF_ABORT>

<cfelseif frmwizardPage is "mods">

	<cfset thisPageNAme = "elementEdit.cfm">
	<cfset application.com.request.setTopHead(frmwizardPage=frmwizardPage,recordID=recordID)>
	<!--- <cfinclude template="#thisDir#/elementTopHead.cfm"> --->
	<cfinclude template = "elementModifications.cfm">
	<CF_ABORT>


</cfif>

<!--- gets element record and some parent details (slightly odd join to get this for a new record!) --->
<CFQUERY NAME="getElementRecord" DATASOURCE="#application.SiteDatasource#" MAXROWS=1>
	SELECT element.id,element.share,element.createdBy,
		element.elementTypeID, element.getChildrenFromParentID,element.headline,element.statusID,element.date,element.sortOrder,element.expiresDate,element.getContentFromID,
		element.showSummary,element.parameters,element.created,element.lastUpdated,element.lastUpdatedby,element.elementTextId,element.rate,element.isExternalFile,
		element.url,element.borderSet,element.showOnMenu,element.hideChildrenFromMenu,
		element.showOnSecondaryNav,element.loginRequired,element.keywords,
		parent.id as parentid, parent.elementtypeid as parentElementTypeID, parent.Headline as parentHeadline  , (select isNull(max(sortorder),0) from element where parentid = parent.id) as MaxSortOrder,
		element.embedly, element.campaignID
	<CFIF isNewRecord>
	FROM element as	parent
		left join
		Element on parent.id = element.parentid
			and element.id is null
		WHERE parent.id =  <cf_queryparam value="#parentid#" CFSQLTYPE="CF_SQL_INTEGER" >
	<CFELSE>
	FROM element
		left join
		Element as	parent on parent.id = element.parentid
		WHERE Element.id =  <cf_queryparam value="#recordID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFIF>
</CFQUERY>
<CFSET elementRecord = application.com.structureFunctions.queryRowToStruct(getElementRecord,getElementRecord.recordcount)>   <!--- a bit easier to work with a structure (especially setting default values for a new record --->

<CFIF isNewRecord>
	<!--- set the default value for the fields if this is a new record--->
	<cfset elementRecord.id = recordid>
	<cfset elementRecord.parentid = parentid>
	<cfif structkeyexists(request,"PageTemplateDefault")>
		<CFSET elementRecord.stylesheets = request.PageTemplateDefault>
	</cfif>
	<CFSET elementRecord.isLIVE = 1>
	<CFSET elementRecord.SHOWONMENU = 1>
	<CFSET elementRecord.statusid = 0>
	<cfset elementRecord.SortOrder = elementRecord.MaxSortOrder + 2>
	<cfset elementRecord.getContentFromID = 0>

	<!--- if typeID has been passed, then use it, otherrwise getthe default type for this parent--->
	<cfif isDefined("typeID") and typeid neq "">
		<cfset ElementRecord.elementtypeID = typeID>
	<cfelse>
			<cfset ElementRecord.elementtypeID = application.elementType[ElementRecord.parentelementtypeID].allowedChildTypesArray[1]>
	</CFIF>

</CFIF>

<!--- Does user have rights to edit this record? --->
<cfset isAdmin = application.com.login.checkInternalPermissions("admintask","level1")>
<CFIF not isNewRecord>
	<cfset Level1Rights = application.com.rights.Element(elementRecord.id,1)>
	<cfset Level2Rights = application.com.rights.Element(elementRecord.id,2)>
	<cfset hasEditRights = Level2Rights.hasrights>
	<cfset Level4Rights = application.com.rights.Element(elementRecord.id,4)>
	<cfset hasOwnerRights = Level4Rights.hasrights>
	<cfset isOwner = iif(request.relaycurrentuser.usergroupid  is getElementRecord.createdby,true,false)>

<cfelse>

	<cfset hasEditRights = application.com.rights.Element(parentid,2).hasrights>
	<cfset hasOwnerRights = true>
	<cfset isOwner =true>
</cfif>


<cfif hasEditRights >
	<!--- You have rights to edit this record --->
<cfelse>
	<cfset editMode = false>
	<!--- You do not have rights to edit this record --->
</cfif>

<!--- Control which columns should show on which wizardPage --->
		<CFSET showThis.HEADLINE = 0> <!--- NJH --->
		<CFSET showThis.DETAIL = 0>
		<CFSET showThis.summary = 0>
		<CFSET showThis.longsummary = 0>
		<CFSET showThis.TextEditor = 0>
		<CFSET showThis.WYSIWYGEditor = 0>
		<CFSET showThis.parameters =0>
		<CFSET showThis.borderset = 0>
		<CFSET showThis.keywords = 0>
		<CFSET showThis.sortOrder = 0>
		<CFSET showThis.status = 0>
		<CFSET showThis.SHOWONMENU = 0>
		<CFSET showThis.showChangeParent = 0>
		<CFSET showThis.showTextID = 0>
		<CFSET showThis.ExpiresDate = 0>
		<CFSET showThis.showDATE = 0>
		<CFSET showThis.showKeyWords = 0>
		<CFSET showThis.showRate = 0>
		<CFSET showThis.showIsExternal = 0>
		<CFSET showThis.showURL = 0>
		<cfset showThis.showTranslationControls = 0>

		<CFSET showThis.summaryInContents = 0>
		<CFSET showThis.showFlags = 0>
		<CFSET showThis.showRecordRights = 0>
		<CFSET showThis.showCountryScope = 0>
		<CFSET showThis.LoginRequired = 0>

		<CFSET showThis.linkImage = 0>
		<CFSET showThis.children = 0> <!--- NJH 2009/05/08 --->
		<CFSET showThis.embedly = 0>
		<CFSET showThis.share = 0>
		<CFSET showThis.campaignID = 0>

<cfswitch expression="#frmWizardPage#">
	<cfcase value="default">
		<cfset showThis.showTranslationControls = 1>
		<CFSET showThis.HEADLINE = 1>
		<CFSET showThis.DETAIL = 1>
		<CFSET showThis.summary = 1>
		<CFSET showThis.TextEditor = 1>
		<CFSET showThis.parameters =1>
		<CFSET showThis.sortOrder = 1>
		<CFSET showThis.status = 1>
		<CFSET showThis.borderset = 1>
	</cfcase>

	<cfcase value="WYSIWYGEditor">
		<cfset showThis.showTranslationControls = 1>
		<CFSET showThis.HEADLINE = 1>
		<CFSET showThis.DETAIL = 1>
		<CFSET showThis.summary = 1>
		<CFSET showThis.WYSIWYGEditor = 1>
		<CFSET showThis.showKeyWords = 1>
		<CFSET showThis.parameters =1>
		<CFSET showThis.sortOrder = 1>
		<CFSET showThis.status = 1>
		<CFSET showThis.share = 1><!--- 2015/01/22	YMA CASE 443543 - show portal page creaton on activity stream --->
		<CFSET showThis.children = 1>
	</cfcase>

	<cfcase value="moreContent">
		<CFSET showThis.SUMMARY = 1>
		<CFSET showThis.LongSUMMARY = 1>
	</cfcase>

	<cfcase value="attributes">
		<CFSET showThis.SHOWONMENU = 0>
		<CFSET showThis.showChangeParent = 1>
		<CFSET showThis.showTextID = 1>
		<CFSET showThis.ExpiresDate = 0>
		<CFSET showThis.showDate= 0>
		<CFSET showThis.showRate = 1>
		<CFSET showThis.showIsExternal = 1>
		<CFSET showThis.sortOrder = 1>
		<CFSET showThis.showURL = 1>
		<CFSET showThis.summaryInContents = 1>
		<CFSET showThis.showFlags = 1>
		<CFSET showThis.linkImage = 1>
		<CFSET showThis.borderset = 1>
		<CFSET showThis.embedly = 1>
		<CFSET showThis.share = 1>
		<CFSET showThis.status = 1><!--- 2015/01/22	YMA CASE 443543 - show portal page creaton on activity stream --->
		<cfif application.com.settings.getSetting("campaigns.displayCampaigns")><CFSET showThis.campaignID = 1></cfif>
	</cfcase>

	<cfcase value="visibility">
		<CFSET showThis.LoginRequired = 1>
		<CFSET showThis.SHOWONMENU = 1>
		<CFSET showThis.showRecordRights = 1>
		<CFSET showThis.showCountryScope = 1>
		<CFSET showThis.summaryInContents = 1>
		<CFSET showThis.showFlags = 0>
		<CFSET showThis.status = 1>
		<CFSET showThis.share = 1><!--- 2015/01/22	YMA CASE 443543 - show portal page creaton on activity stream --->
		<CFSET showThis.showdate = 1>

	</cfcase>

</cfswitch>

<cfset getType = application.elementType[elementRecord.elementTypeID]>


	<!--- WAb experimenting with showing all items - not for release - would need to be controlled by settings somewhere, and would need to turn off the other wizard pages --->
<cfif ElementRecord.elementTypeID is 51 or ElementRecord.elementTypeID is 52>
		<cfloop collection="#showThis#" item="item">
			<cfset showThis[item] = 1>
		</cfloop>
		<CFSET showThis.WYSIWYGEditor = 0>

</cfif>

<cfset headline_FieldLength = 80>

<!--- get details of possible Parents, if types are defined--->
<CFIF elementRecord.parentElementTypeID is not "">
	<!--- set a default ParentElementID is none is defined --->
	<!--- WAB: 2004-11-22 changed to use new stored procedure, but doesn't look as if this query brings back a useful list of possible parents --->

	<!---  get branch that this element is on --->
	<cfquery name="getParents" datasource="#application.sitedatasource#" cachedwithin="#application.cachedContentTimeSpan#">
		-- #application.cachedContentVersionNumber# (this variable is used to flush the cache when the content is changed)
		exec GetElementParents @ElementID=<cfif not isNewRecord>#ElementRecord.id#<cfelse>#ElementRecord.parentid#</cfif>
	</cfquery>


	<!--- is one of the parents the topid for an elementtree/site
		if so then we can get info from the sitedef structure
		a bit messy I'm afraid   WAb 2006-09-25

		2010/10/13 domainslive etc are no more, no sign of getRoot.domain being used anywhere so got rid of it all
		 --->
	<cfquery name="getRoot" datasource="#application.sitedatasource#" cachedwithin="#application.cachedContentTimeSpan#">
		select distinct topid as elementid
		 from
		 	sitedef sd
		 		inner join
		 	elementTreeDef etd on sd.elementtreename = etd.elementtreename
				inner join
		 	sitedefdomain 	sdd on sdd.sitedefid = sd.sitedefid and sdd.testsite =  <cf_queryparam value="#application.testSite#" CFSQLTYPE="CF_SQL_INTEGER" > and sdd.active = 1
		 where
		 	etd.topid  in ( <cf_queryparam value="#valuelist(getparents.elementid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		 	<!--- NJH 2009/11/05 LID 2638 - only bring back records where domains are set
		 		WAB 2010/10 now done by join to sitedefdomain
		 	--->
	</cfquery>

	<cfif getRoot.recordcount is 0>
		<cfquery name="getRoot" dbtype="query">
		select elementid,generation,'' as domains from getParents where isTreeRoot = 1
		union
		select elementid,generation,'' as domains from getParents where generation = 1   <!--- incase there is no item marked as isTreeRoot, we will bring back the top item--->
		order by generation desc
		</cfquery>
		<!--- WAb 2010/10/12 appears not used <CFSET thisElementSiteDef = APPLICATION.SITE DEF[LISTFIRST(application.external Userdomains)]> --->
	<cfelse>
		<!--- <CFSET thisElementSiteDef = APPLICATION.SITE DEF[LISTFIRST(GETROOT.DOMAINS)]> --->
	</cfif>


	<cfif showThis.showChangeParent is 1>
		<cfset getTree = application.com.relayElementTree.getFullTreeFromCache(getRoot.elementID)>

		<cfquery name="getPossibleParents" dbtype="query">
		select node as elementid, name, generation from getTree
		where hasChildren = 1
		and ',' + parentidlist + ',' not like '%,#getElementRecord.id#,%'   <!--- this is a rather nasty construction to do a listfind within a query --->
		</cfquery>
	</cfif>

</cfif>

<CFIF not isNewRecord and showThis.children>
<!--- get All the child Records (for Information) --->
	<CFQUERY NAME="GetChildren" DATASOURCE="#application.SiteDataSource#">
		SELECT Element.Headline, Element.id AS ElementID, elementTextID
		FROM Element
		WHERE ParentID = <cfif ElementRecord.GetChildrenFromParentID is 0>#elementRecord.id#<cfelse>#ElementRecord.GetChildrenFromParentID#</cfif>
		order by headline
	</CFQUERY>

</CFIF>


<!--- START 2015-01-28 AHL Case 442825 Creating Child Pages shows Parent in designer --->
<cfif structKeyExists(elementRecord,'headline')>
	<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
	<script>
		<cfoutput>updateTab ({tabTitle:'#jsStringFormat(elementRecord.headline)#'})</cfoutput>
	</script>
</cfif>
<!--- END 2015-01-28 AHL Case 442825 Creating Child Pages shows Parent in designer --->

<cf_head>

	<SCRIPT type="text/javascript">

		<CFINCLUDE template = "#thisdir#/jsFunctions.cfm">



		function editElement (elementID)  {
			form = document.mainForm
			form.nextRecordID.value = elementID;
			form.frmWizardPage.value = "default";
			if (elementID != 0)
				{submitForm()}
			else
				{alert("You cannot edit an element with id 0")}

		}


		function newChildElement (recordID)  {
			var now = new Date();
			now= now.getTime();
			form = document.mainForm
			form.parentID.value = recordID;
			form.nextRecordID.value = "NewRecord";
			form.submit();
		}
		function newSiblingElement (recordID)  {
			var now = new Date();
			now= now.getTime();
			form = document.mainForm
			form.nextRecordID.value = "NewRecord";
			form.submit();
		}





function setDirtyFields(form) {

	// code to set the various dirty fields before submitting; 2012/02/29 PPB/WAB Case 426749 recordid was added to the list so that treeDirty is now set if any flag on the form is changed
	var treeDirtyFieldsRegExp = 'statusID|getContentFromID|sortorder|parentId|elementtextid|showOnMenu|loginRequired|<cfoutput>_#jsStringFormat(recordid)#</cfoutput>';

	//Are translations dirty?
	// this checks the text fields named phrupd, but doesn't work for the hidden field associated with fck in FF (FF reset the value of defaultValue when the field is updated).
	translationsDirty = isDirty (form,{checkHidden:true,regExp:/phrupd/i,debug:false})
	if (!translationsDirty && typeof(CKEDITOR) != 'undefined') { // if translations not already dirty then check the ck
		// is there an fck instance for the detail, need to check whether this is dirty
		if (instance = CKEDITOR.instances['phrupd_detail_10_<cfoutput>#jsStringFormat(recordid)#</cfoutput>_']) {
			translationsDirty = instance.checkDirty ()
		};
	}
	form.translationsDirty.value = (translationsDirty)?1:0 ;

	// now check the fields which would make the tree dirty
	regExp = new RegExp (treeDirtyFieldsRegExp,'i')
	treeDirty = isDirty (form,{checkHidden:false,regExp:regExp,debug:false})
	form.treeDirty.value = (treeDirty)?1:0 ;

	// now check all the fields except the phrase ones
	regExp = new RegExp ('^(?!(phrupd))','i')
	mainDirty = isDirty (form,{checkHidden:false,regExp:regExp,debug:false})
	form.mainDirty.value = (mainDirty)?1:0 ;

	// finally if anything is dirty set dirty
	// form.dirty.value = (mainDirty || translationsDirty || treeDirty) ? 1:0

	// alert ('translationsDirty=' + translationsDirty +'. treeDirty='+treeDirty +'. mainDirty=' + mainDirty)

}

function isFormOKtoSubmit(form) {

			setDirtyFields (form)

			/*NJH 2016/02/15 - removed. Get rwFormValidation to do the work. Don't see where country and language are required... possibly tranlsations, but that looks to have change recently as well
			* if (typeof(form.phrCountryID) != 'undefined' && form.phrCountryID.value == '') {
					alert ('you must select a country') ;
					return false
			}
			else if (typeof(form.phrLanguageID) != 'undefined'  && form.phrLanguageID.value == '') {
					alert ('you must select a language') ;
					return false
			}
			<!--- 2015-10-23 ESZ Case 445605 --->
			else if (typeof(form.Headline) != 'undefined'  && form.Headline.value == '') {
				alert ('Content page without a name is not valid') ;
				return false
			}
			else {

					return true
			}*/
			return true
}




function statusChangeCheckGoLiveDate (statusObject,addWorkDays,addActualDays)	{
	// if change to live then set the live date to x days in the future if it is not already set further in advance
	if (statusObject.options[statusObject.selectedIndex].value == 4 ) {
		form = document.mainForm
		nowDate = new Date() ;
		futureDate = new Date() ;
		factor = 1000*60*60*24 ;
		dateMask = form.Date.getAttribute('datemask')
		changeLiveDate = true ;

		// if the change date field is not displayed on this page there will be a div which need to be shown
		divObj = document.getElementById('LiveDateDiv');
		if (divObj != null) {
			divObj.style.display = "block";
		}

		if (form.Date.value != '') {
			// if there is already a date in the field then check whether it is further in the future than required
			currentFormDateTimeString = form.Date.value + ' ' + form.Date_Hr.value + ':' +  form.Date_Min.value
			currentFormDate = getDateFromFormat (currentFormDateTimeString,dateMask  + " HH:mm") ;
			if ((currentFormDate - nowDate )/factor    > addActualDays ) {
				changeLiveDate  = false
			}
		}

		if (form.Date.value == '' || changeLiveDate ) {
			futureDate.setTime(nowDate.getTime()+(addActualDays * factor ));
			form.Date.value = formatDate(futureDate,dateMask )  ;
			form.Date_Hr.value = futureDate.getHours ();
			form.Date_Min.value = '00';
			form.dirty.value=1
			alert ('Go Live date has been set ' + addWorkDays + ' work days in advance');

		}

	}


	}

<!--- function toggleNav( callerObject )
	{
	if ( parent.document.body.cols == "30%,*" )
		  {
		    parent.document.body.cols = '0,*';
			callerObject.src = iconDoubleArrowRight.src;
			callerObject.alt = "Click to show content Tree";
		  }
	else
		  {
		    parent.document.body.cols = '30%,*';
			callerObject.src = iconDoubleArrowLeft.src;
			callerObject.alt = "Click to hide content Tree";
		  }
	return true;
	}


var iconDoubleArrowLeft = new Image();
var iconDoubleArrowRight = new Image(); --->

<!--- function editWindowOnLoad( callerObject )
	{
	<cfoutput>
//		updateTab ({tabTitle:'Edit Page: #jsStringformat(left(elementRecord.Headline,15))#'})
	</cfoutput>
	  iconDoubleArrowLeft.src = "/images/MISC/iconDoubleArrowLeft.gif";
	  iconDoubleArrowRight.src = "/images/MISC/iconDoubleArrowRight.gif";
	} --->



	<!--- function blowElementCache() {
		if (confirm('This will slow down the next page request for every user on the live site - only do when required.  Continue?'))
		{
			page = '/webservices/misc.cfc?method=incrementCachedContentVersionNumber'
			var myAjax = new Ajax.Updater(
				'dummy',
				page,
				{
					method: 'post',
					parameters: '',
					evalScripts: true
				});
		}
	} --->



function growTextArea(txtBox,txtBoxAnchor)   {
        txtBox.rows = 30;
}

function shrinkTextArea(txtBox,thisRows) {
	txtBox.rows = thisRows;
	location.href = "#topOfPage";
}


function saveElementAjax() {
	elementForm = $('mainForm');
	if (isFormOKtoSubmit(elementForm)) {
		elementForm.request({
			parameters : {ajaxSubmit :1,_cf_nodebug:true},
			oncomplete : function () {setFormNotDirty(elementForm)}
		})
	}

}

</script>
</cf_head>

<cfset thisPageNAme = "elementEdit.cfm">
<cfset application.com.request.setTopHead(frmwizardPage=frmwizardPage,recordID=recordID)>
<!--- <cfinclude template="elementTopHead.cfm"> --->


<!--- NJH 2009/02/09 P-SNY047 Added cfform as usergroup selection is now using cfselect --->
<form action="./ElementActions.cfm" method="post" enctype="multipart/form-data" id="mainForm" name="mainForm"  class="element">
<cf_relayFormDisplay class="">

<CFSET formFieldList="ElementTypeID">

<CFOUTPUT >





		<CF_INPUT TYPE="hidden" NAME="recordID" VALUE="#elementRecord.id#">
		<CF_INPUT TYPE="hidden" NAME="nextRecordID" VALUE="#elementRecord.id#">
		<CF_INPUT TYPE="hidden" NAME="id" VALUE="#elementRecord.id#">
<!--- 		<input type="hidden" name="dirty" value="0"><!--- set to 1 when any items are changed ---> --->
		<input type="hidden" name="mainDirty" value="0"> <!--- set to 1 when items other than translations are changed --->
		<input type="hidden" name="translationsDirty" value="0"> <!--- set to one when translations are changed --->
		<input type="hidden" name="treeDirty" value="0"><!--- changed to one when items which affect the tree ar changed (determines whether the parent navigation is reloaded) --->
		<CF_INPUT TYPE="hidden" NAME="elementTypeID" VALUE="#elementRecord.elementtypeID#">



<div>
	<CFIF isDefined("message") and message neq "">
		<cfset application.com.relayUI.setMessage(message=message)>
	</CFIF>

	<cfif application.com.relayElementTree.doesCachedContentNeedToBeBlown()>
		<cfset application.com.relayElementTree.incrementCachedContentVersionNumber(updateCluster = true)>
		<!--- <div id=""><A HREF="javascript:blowElementCache()">phr_element_ClickToUpdateCache</A></div><div id="dummy" style="display:none"></div> --->
	</cfif>
</div>

</cfoutput>
<div class="row">
	<div class="col-xs-12">
		<div class="grey-box-top">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="label" currentValue="" label="phr_element_Parent > phr_element_Name (ID)"><!--- Label: Parent > Name (ID) --->
				<cfoutput>
					<a href="javascript:void(openNewTab('element_#elementRecord.parentid#','#elementRecord.parentHeadline# (#elementRecord.parentid#)','/elements/elementEdit.cfm?RecordID=#elementRecord.parentid#',{reuseTab:true,reuseTabJSFunction: function () {return true}}))" title="Edit parent record">#htmleditformat(elementRecord.parentHeadline)#</a> > #htmleditformat(elementRecord.Headline)# (#htmleditformat(elementRecord.id)#) <cfif not editMode>(phr_element_ReadOnly)</cfif></cfoutput>
					<!--- <a href="javascript:editElement(#elementRecord.parentid#)" title="Edit parent record">#elementRecord.parentHeadline#</a> > #elementRecord.Headline# (#elementRecord.id#) <cfif not editMode>(phr_element_ReadOnly)</cfif></cfoutput> --->
			</cf_relayFormElementDisplay>
			<div class="row" id="topColCMS">
				<div class="col-xs-12 col-sm-6">
					<CFOUTPUT>
						<cfif showThis.HEADLINE>
							<cfif editMode>
								<cfset formFieldList = listAppend(formFieldList,"Headline")>
							</cfif>
							<!--- 2015-11-30  ACPK    BF-31 Set Headline field as required --->
							<!--- <cf_relayFormElementDisplay relayFormElementType="Text" required="true" disabled = #not(editmode)# fieldname="Headline" currentValue="#elementRecord.Headline#" label="phr_element_name" SIZE="40"  MAXLENGTH="#headline_FieldLength#" onChange="if((field = document.mainForm.phrupd_headline_10_#elementRecord.id#_)  && field.value == '') {field.value = this.value;}" NoteText="phr_element_UsedForReportingPurposes - phr_element_notBeTranslatedOnlyAppearOnListing." NoteTextPosition="right"> --->
							<!--- 2015-11-30  ACPK    BF-31 Set Headline field as required --->
							<cf_relayFormElementDisplay class="form-control" relayFormElementType="Text" required="true" disabled = #not(editmode)# fieldname="Headline" currentValue="#elementRecord.Headline#" label="phr_element_name" SIZE="40"  MAXLENGTH="#headline_FieldLength#" onChange="if((field = document.mainForm.phrupd_headline_10_#elementRecord.id#_)  && field.value == '') {field.value = this.value;}" NoteText="phr_element_UsedForReportingPurposes - phr_element_notBeTranslatedOnlyAppearOnListing." NoteTextPosition="right">

								<!--- Label: Name
									NoteText: Used for reporting purposes - it will not be translated and will only appear on listing and reporting screens in the content management tools.
								 --->
						</cfif>
					</CFOUTPUT>

					<cfif showThis.showTextID>
						<cf_relayFormElementDisplay class="form-control" relayFormElementType="text" disabled = #not(editmode)# fieldname="elementTextID" currentValue="#elementRecord.elementTextID#" label="phr_element_uniqueTextID" size="30" maxLength="100" onKeyPress="return restrictKeyStrokes(event,keybCFVariableNM)"> <!--- Label: Unique Text ID for this content element (optional)	 --->
						<cfif editmode>
							<CFSET formFieldList = listAppend(formFieldList,"elementTextID")>
						</cfif>
					</cfif>

					<!--- Status	--->
					<cfif showThis.status eq 1>
						<cfif application.ElementGoLiveDelayDays is not 0 and ElementRecord.statusid is not 4>  <!--- need javascript check of the go live date if required and if not already live --->
							<cfset actualdaysToAdd = datediff("d",now(),dateadd("w",application.ElementGoLiveDelayDays,now()))>
							<cfset onchange = "javascript:statusChangeCheckGoLiveDate(this,#application.ElementGoLiveDelayDays#,#actualdaysToAdd#)">
						<cfelse>
							<cfset onchange = "">
						</cfif>

						<cfif editmode>
							<CFSET formFieldList = listAppend(formFieldList,"StatusID")>
						</cfif>

						<cfquery name="getElementStatii" datasource="#application.siteDataSource#">
							select StatusID, description from ElementStatus
						</cfquery>

						<cf_relayFormElementDisplay class="form-control" relayFormElementType="select" fieldname="StatusID" currentValue="#ElementRecord.statusID#" label="phr_element_status" query="#getElementStatii#" display="description" value="statusID" disabled = #not(editmode)# onchange = #onchange#> <!--- Label: Content status --->
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="status" currentValue="" label="">
							<cfoutput>
							<cfif ElementRecord.statusid is 4>
								<cfif elementRecord.DATE is not "">
									phr_element_from #dateformat(elementRecord.DATE)#  <!--- from  --->
								</cfif>
								<cfif elementRecord.ExpiresDate is not "">
									phr_element_until  #dateformat(elementRecord.ExpiresDate)# <!--- until --->
								</cfif>
							<cfelse>
								<cfif editMode and showThis.showDATE is 0 >  <!--- if there is not already a date control on this page then need to add a hidden one --->
									<div id="LiveDateDiv" style = "display:none;">
										<cf_IncludeJavascriptOnce template = "/javascript/date.js">
										<CFSET formFieldList = listAppend(formFieldList,"Date")>
										<cfset dateTypeInfo = application.com.dateFunctions.getDateTypeFormattingInfo()>

										phr_element_WillGoLiveOn <CF_INPUT Type=text name = "Date" readonly value="#dateformat(elementRecord.DATE,"dd/mm/yyyy")#" size = "10" datemask ="#dateTypeInfo.datemask#" > <!--- Will go live on  --->
										phr_element_at <CF_INPUT Type=text name = "Date_Hr" readonly value="#timeformat(elementRecord.DATE,"HH")#" size = "2">:<CF_INPUT Type=text name = "Date_Min" readonly value="#timeformat(elementRecord.DATE,"mm")#" size = "2"> hrs <!--- at --->
										<CF_INPUT Type=hidden name = "Date_#dateTypeInfo.dateHiddenFieldSuffix#">
									</div>
								</cfif>

							</cfif>
							</cfoutput>
						</cf_relayFormElementDisplay>

						<!--- NJH 2012/01/17 - Social Media - capture the original status of the element --->
						<cf_relayFormElement relayFormElementType="hidden" fieldname="StatusID_orig" currentValue="#ElementRecord.statusID#" label="">

					</cfif>
				</div>
				<div class="col-xs-12 col-sm-6">
					<!---  HEADLINE --->
					<cfif showThis.HEADLINE>
						<cf_relayFormElementDisplay class="form-control" relayFormElementType="HTML" fieldname="translatedTitle" currentValue="" label="phr_element_translatedTitle" noteText="phr_element_ThisIsTranslatableTitle." NoteTextPosition="">
							<cf_editPhraseWidget entityID = #recordID# entityType = "element" required="false" message= "" phraseTextID="headline" disabled = #not(editmode)# showOtherTranslationsTable=false>
						</cf_relayFormElementDisplay>
					</cfif>
				</div>
			</div>

			<CFIF getType.showDetail is 1 and showThis.Detail eq 1>
			<!--- 		wab / njh removed page stylesheet, not used --->

				<cfif ElementRecord.getContentFromID is not 0>
					<cfif editmode>
						<CFSET formFieldList = listAppend(formFieldList,"getContentFromID")>
					</cfif>
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="elementIDSupplyingContent" currentValue="" label="phr_element_elementIDSupplyingContent"> <!--- Element ID that is supplying the content for this element --->
						<CF_INPUT TYPE="Text" NAME="getContentFromID" VALUE="#ElementRecord.getContentFromID#"  disabled="#iif( not editMode,true,false)#">
						<cfif editmode>
							<A HREF="elementedit.cfm?recordid=#ElementRecord.getContentFromID#">phr_element_GoToTheSourceElementID #htmleditformat(ElementRecord.getContentFromID)#</A> <!--- Go to the source element ID  --->
							<br><A HREF="javascript:document.mainForm.getContentFromID.value='0';submitForm('default',0);">phr_element_ClickToRevert</a> <!--- Click to revert to using content from this element  --->
						</cfif>
					</cf_relayFormElementDisplay>

				<cfelse>

				<CFSET formFieldList = listAppend(formFieldList,"Detail")>

					<cfif showThis.WYSIWYGEditor eq 1>
						<cf_editPhraseWidget entityID = #recordID# entityType = "element" required="false" message= "Content Required" displayAs="RichTextArea" rows=100 cols=60 phraseTextID="detail" disabled = #not(editmode)# showOtherTranslationsTable=false>
					</CFIF>

					<!--- 2012-10-10 PPB Case 430878 START add getContentFromID to fieldList --->
					<cfif editmode>
						<CFSET formFieldList = listAppend(formFieldList,"getContentFromID")>
					</cfif>
					<!--- 2012-10-10 PPB Case 430878 END --->
					<cf_relayFormElementDisplay class="form-control" relayFormElementType="text" fieldname="getContentFromID" label="phr_element_pullContentFromElement" currentValue="#elementRecord.getContentFromID#" size=5  disabled = #not(editmode)#>

				</CFIF>

			</CFIF>
		</div>
	</div>
</div>
<div class="row" id="elementEdit">
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="grey-box-top">
			<!--- if there isn't a parent then elementRecord.ParentID won't be numeric --->

			<!---SB 11/03/16 Moved to the top of the page
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="label" currentValue="" label="phr_element_Parent > phr_element_Name (ID)"><!--- Label: Parent > Name (ID) --->
				<cfoutput>
					<a href="javascript:void(openNewTab('element_#elementRecord.parentid#','#elementRecord.parentHeadline# (#elementRecord.parentid#)','/elements/elementEdit.cfm?RecordID=#elementRecord.parentid#',{reuseTab:true,reuseTabJSFunction: function () {return true}}))" title="Edit parent record">#htmleditformat(elementRecord.parentHeadline)#</a> > #htmleditformat(elementRecord.Headline)# (#htmleditformat(elementRecord.id)#) <cfif not editMode>(phr_element_ReadOnly)</cfif></cfoutput>

					<!--- <a href="javascript:editElement(#elementRecord.parentid#)" title="Edit parent record">#elementRecord.parentHeadline#</a> > #elementRecord.Headline# (#elementRecord.id#) <cfif not editMode>(phr_element_ReadOnly)</cfif></cfoutput> --->
			</cf_relayFormElementDisplay> --->

			<cfif isNewRecord and arrayLen(application.elementType[elementRecord.parentelementTypeID].allowedChildTypesArray) gt 1>



			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="changeType" currentValue="" label="phr_element_ChangeType"><!--- Label: Change Type --->
				<cfquery name="types" DATASOURCE="#application.SiteDataSource#">
					select type as displayvalue, elementTypeID as datavalue from elementType where elementTypeID  in ( <cf_queryparam value="#arrayToList(application.elementType[elementRecord.parentelementTypeID].allowedChildTypesArray)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfquery>

				<CF_DisplayValidValues
					formFieldName="Typeid"
					validValues = "#types#"
					showNull = false
					currentValue="#elementRecord.elementTypeID#"
					method = "#iif(editmode,de("edit"),de("view"))#"
					onchange="var form = document.mainForm ; form.frmTask.value = '' ;form.submit()">
			</cfif>

			<cfif not isNewRecord>
				<!--- id needs to be in the fieldList because we need it for the CFUPDATE --->
				<CFSET formFieldList = listAppend(formFieldList,"id")>
			</cfif>

		<CFOUTPUT >
			<!---SB 11/03/16 Moved to the top of the page
			<cfif showThis.HEADLINE>
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"Headline")>
				</cfif>
				<!--- 2015-11-30  ACPK    BF-31 Set Headline field as required --->
				<!--- <cf_relayFormElementDisplay relayFormElementType="Text" required="true" disabled = #not(editmode)# fieldname="Headline" currentValue="#elementRecord.Headline#" label="phr_element_name" SIZE="40"  MAXLENGTH="#headline_FieldLength#" onChange="if((field = document.mainForm.phrupd_headline_10_#elementRecord.id#_)  && field.value == '') {field.value = this.value;}" NoteText="phr_element_UsedForReportingPurposes - phr_element_notBeTranslatedOnlyAppearOnListing." NoteTextPosition="right"> --->
				<!--- 2015-11-30  ACPK    BF-31 Set Headline field as required --->
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="Text" required="true" disabled = #not(editmode)# fieldname="Headline" currentValue="#elementRecord.Headline#" label="phr_element_name" SIZE="40"  MAXLENGTH="#headline_FieldLength#" onChange="if((field = document.mainForm.phrupd_headline_10_#elementRecord.id#_)  && field.value == '') {field.value = this.value;}" NoteText="phr_element_UsedForReportingPurposes - phr_element_notBeTranslatedOnlyAppearOnListing." NoteTextPosition="right">

					<!--- Label: Name
						NoteText: Used for reporting purposes - it will not be translated and will only appear on listing and reporting screens in the content management tools.
					 --->
			</cfif> --->


			<CFSET formFieldList = listAppend(formFieldList,"parentID")>

			<CFIF not isNewRecord  and isDefined("getPossibleParents") and showThis.showChangeParent is 1>
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="changeParent" currentValue="" label="phr_element_ChangeParent">
					<SELECT NAME="parentID" class="form-control"  <cfif not editMode>disabled</cfif>>
						<OPTION VALUE=0>
						<cfset itemSelected = false>
						<CFLOOP QUERY="getPossibleParents">
							<OPTION VALUE="#elementid#" <CFIF elementid IS elementRecord.ParentId>SELECTED <cfset itemSelected = true></cfif> >#repeatString("&nbsp;",generation-1)# #HTMLEditFormat(left(Name,30))# (#elementid#)
						</CFLOOP>
						<!--- WAB 2007-03-12 discovered that if an item is added and then a child is added without the cache being blown then the parent doesn't exist in the cached tree, so no parent is available in the drop down.  When the record is saved the parentid gets set to 0 --->
						<cfif itemSelected is false>
							<!--- if for some reason the current parent isn't in the tree --->
							<OPTION VALUE="#elementRecord.ParentId#" selected>#elementRecord.parentHeadline#
						</cfif>
					</SELECT>
				</cf_relayFormElementDisplay>

			<cfelse>
				<CF_INPUT type="hidden" name="parentID" value="#elementRecord.ParentId#">
			</CFIF>


			<!---  HEADLINE --->
			<!--- SB 11/03/16 Moved to the top of the page
			<cfif showThis.HEADLINE>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="HTML" fieldname="translatedTitle" currentValue="" label="phr_element_translatedTitle" noteText="phr_element_ThisIsTranslatableTitle." NoteTextPosition="">
					<cf_editPhraseWidget entityID = #recordID# entityType = "element" required="false" message= "" phraseTextID="headline" disabled = #not(editmode)# showOtherTranslationsTable=false>
				</cf_relayFormElementDisplay>
			</cfif> --->

			<!--- ElementtTextID	--->
			<!--- <cfif showThis.showTextID>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="text" disabled = #not(editmode)# fieldname="elementTextID" currentValue="#elementRecord.elementTextID#" label="phr_element_uniqueTextID" size="30" maxLength="100" onKeyPress="return restrictKeyStrokes(event,keybCFVariableNM)"> <!--- Label: Unique Text ID for this content element (optional)	 --->
				<cfif editmode>
					<CFSET formFieldList = listAppend(formFieldList,"elementTextID")>
				</cfif>
			</cfif> --->


			<!--- Status	--->
			<!--- <cfif showThis.status eq 1>
				<cfif application.ElementGoLiveDelayDays is not 0 and ElementRecord.statusid is not 4>  <!--- need javascript check of the go live date if required and if not already live --->
					<cfset actualdaysToAdd = datediff("d",now(),dateadd("w",application.ElementGoLiveDelayDays,now()))>
					<cfset onchange = "javascript:statusChangeCheckGoLiveDate(this,#application.ElementGoLiveDelayDays#,#actualdaysToAdd#)">
				<cfelse>
					<cfset onchange = "">
				</cfif>

				<cfif editmode>
					<CFSET formFieldList = listAppend(formFieldList,"StatusID")>
				</cfif>

				<cfquery name="getElementStatii" datasource="#application.siteDataSource#">
					select StatusID, description from ElementStatus
				</cfquery>

				<cf_relayFormElementDisplay class="form-control" relayFormElementType="select" fieldname="StatusID" currentValue="#ElementRecord.statusID#" label="phr_element_status" query="#getElementStatii#" display="description" value="statusID" disabled = #not(editmode)# onchange = #onchange#> <!--- Label: Content status --->
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="status" currentValue="" label="">
					<cfif ElementRecord.statusid is 4>
						<cfif elementRecord.DATE is not "">
							phr_element_from #dateformat(elementRecord.DATE)#  <!--- from  --->
						</cfif>
						<cfif elementRecord.ExpiresDate is not "">
							phr_element_until  #dateformat(elementRecord.ExpiresDate)# <!--- until --->
						</cfif>
					<cfelse>
						<cfif editMode and showThis.showDATE is 0 >  <!--- if there is not already a date control on this page then need to add a hidden one --->
							<div id="LiveDateDiv" style = "display:none;">
								<cf_IncludeJavascriptOnce template = "/javascript/date.js">
								<CFSET formFieldList = listAppend(formFieldList,"Date")>
								<cfset dateTypeInfo = application.com.dateFunctions.getDateTypeFormattingInfo()>

								phr_element_WillGoLiveOn <CF_INPUT Type=text name = "Date" readonly value="#dateformat(elementRecord.DATE,"dd/mm/yyyy")#" size = "10" datemask ="#dateTypeInfo.datemask#" > <!--- Will go live on  --->
								phr_element_at <CF_INPUT Type=text name = "Date_Hr" readonly value="#timeformat(elementRecord.DATE,"HH")#" size = "2">:<CF_INPUT Type=text name = "Date_Min" readonly value="#timeformat(elementRecord.DATE,"mm")#" size = "2"> hrs <!--- at --->
								<CF_INPUT Type=hidden name = "Date_#dateTypeInfo.dateHiddenFieldSuffix#">
							</div>
						</cfif>

					</cfif>
				</cf_relayFormElementDisplay>

				<!--- NJH 2012/01/17 - Social Media - capture the original status of the element --->
				<cf_relayFormElement relayFormElementType="hidden" fieldname="StatusID_orig" currentValue="#ElementRecord.statusID#" label="">

			</cfif> --->


			<!--- Date--->
			<cfif showThis.showDate is 1 and getType.showDate is 1>
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"Date")>
				</cfif>
				<cfset enableFromDate = "#year(now())#-01-01">  	<!--- 2013-04-18 PPB Case 433761 restrict to this year onwards (NB the tag parameter is misleadingly called disableFromDate) --->
				<cf_relayFormElementDisplay relayFormElementType="date" fieldname="Date" currentValue="#elementRecord.Date#" label="phr_element_DateContentBecomesLive" formName="mainForm" showClearLink = true  disabled = #not(editmode)# showTime=true enableRange="true" disableToDate="#enableFromDate#"> <!--- Label What date does this content become live: --->
			</CFIF>

			<!--- Expires Date --->
			<cfif showThis.showDate eq 1 and getType.showDate is 1>
				<cfif editMode>
					<CFSET formFieldList = listAppend(formFieldList,"ExpiresDate")>
				</cfif>

				<cfset enableFromDate = "#year(now())#-01-01">  	<!--- 2013-04-18 PPB Case 433761 restrict to this year onwards (NB the tag parameter is misleadingly called disableFromDate) --->
				<cf_relayFormElementDisplay relayFormElementType="date" fieldname="ExpiresDate" currentValue="#elementRecord.ExpiresDate#" label="phr_element_DateContentExpires" formName="mainForm" showClearLink = true disabled = #not(editmode)# showTime=true enableRange="true" disableToDate="#enableFromDate#"> <!--- What date does this content expire --->  <!--- 2013-04-18 PPB Case 433761 restrict to after yesterday --->
			</CFIF>


			<!---SB 11/03/16 Moved to the top of the page Detail --->
			<!--- <CFIF getType.showDetail is 1 and showThis.Detail eq 1>
			<!--- 		wab / njh removed page stylesheet, not used --->

				<cfif ElementRecord.getContentFromID is not 0>
					<cfif editmode>
						<CFSET formFieldList = listAppend(formFieldList,"getContentFromID")>
					</cfif>
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="elementIDSupplyingContent" currentValue="" label="phr_element_elementIDSupplyingContent"> <!--- Element ID that is supplying the content for this element --->
						<CF_INPUT TYPE="Text" NAME="getContentFromID" VALUE="#ElementRecord.getContentFromID#"  disabled="#iif( not editMode,true,false)#">
						<cfif editmode>
							<A HREF="elementedit.cfm?recordid=#ElementRecord.getContentFromID#">phr_element_GoToTheSourceElementID #htmleditformat(ElementRecord.getContentFromID)#</A> <!--- Go to the source element ID  --->
							<br><A HREF="javascript:document.mainForm.getContentFromID.value='0';submitForm('default',0);">phr_element_ClickToRevert</a> <!--- Click to revert to using content from this element  --->
						</cfif>
					</cf_relayFormElementDisplay>

				<cfelse>

				<CFSET formFieldList = listAppend(formFieldList,"Detail")>

					<cfif showThis.WYSIWYGEditor eq 1>
						<cf_editPhraseWidget entityID = #recordID# entityType = "element" required="false" message= "Content Required" displayAs="RichTextArea" rows=100 cols=60 phraseTextID="detail" disabled = #not(editmode)# showOtherTranslationsTable=false>
					</CFIF>

					<!--- 2012-10-10 PPB Case 430878 START add getContentFromID to fieldList --->
					<cfif editmode>
						<CFSET formFieldList = listAppend(formFieldList,"getContentFromID")>
					</cfif>
					<!--- 2012-10-10 PPB Case 430878 END --->
					<cf_relayFormElementDisplay class="form-control" relayFormElementType="text" fieldname="getContentFromID" label="phr_element_pullContentFromElement" currentValue="#elementRecord.getContentFromID#" size=5  disabled = #not(editmode)#>

				</CFIF>

			</CFIF> --->

			<cfif (getType.showDetail is 1 and showThis.Detail eq 1 and showThis.WYSIWYGEditor) or (showThis.HEADLINE)>
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="translationTable" currentValue="" label="Also translated into">
					<cf_editPhraseWidget entityID = #recordID# entityType = "element" showOtherTranslationsTable=true showTranslationTableLabel=false saveFormFunction="saveElementAjax();" promptForSave="true" allphraseTextIDsOnPage="Headline,Detail">
				</cf_relayFormElementDisplay>
			</cfif>

			<!--- Summary --->
			<CFIF getType.showSummary is 1 and showThis.Summary eq 1>

				<cfif editMode>
					<CFSET formFieldList = listAppend(formFieldList,"showSummary")>
				</cfif>

				<!---
				NJH 2011/06/21 made into radio to be consistent with other radio fields.
				<cfsavecontent variable="summaryLabel">
					phr_element_Summary<br> <!--- Summary --->
					<INPUT TYPE="checkbox" NAME="showSummary" VALUE="1"<CFIF elementRecord.showSummary IS 1> checked</CFIF>  <cfif not editMode>disabled</cfif>> phr_element_ShowSummary <!--- Show Summary --->
				</cfsavecontent> --->

				<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="showSummary" label="phr_element_ShowSummary" currentValue="#elementRecord.showSummary#" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" disabled = #not(editmode)#> <!--- Label: Show this item on Secondary Navigation --->
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="summary" currentValue="" label="phr_element_Summary">
					<cf_editPhraseWidget entityID = #recordID# entityType = "element" required="false" phraseTextID="summary" disabled = #not(editmode)# showOtherTranslationsTable=false>
				</cf_relayFormElementDisplay>


				<!--- showSummaryInContents   --->
				<CFIF getType.showSummaryInContents is 1>
					<CFSET formFieldList = listAppend(formFieldList,"SummaryInContents")>

					<cf_relayFormElementDisplay relayFormElementType="checkbox" fieldname="summaryInContents" currentValue="" label="phr_element_ShowSummaryInContents"  disabled = #not(editmode)#> <!--- Label: Show Summary In Contents --->

				</CFIF>
			</cfif>

			<!--- showLongSummary --->
			<CFIF getType.showLongSummary is 1 and showThis.LongSummary eq 1>
				<CFSET formFieldList = listAppend(formFieldList,"LongSummary")>
				<CFSET formFieldList = listAppend(formFieldList,"showLongSummary")>

				<cfsavecontent variable="longSummaryLabel_html">Long Summary <BR>
					<INPUT TYPE="checkbox" NAME="showLongSummary" VALUE="1"<CFIF elementRecord.showLongSummary IS 1> checked</CFIF>  <cfif not editMode>disabled</cfif>>phr_element_ShowLongSummary
				</cfsavecontent>

				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="longSummary" currentValue="" label="#longSummaryLabel_html#">
					<cf_editPhraseWidget entityID = #recordID# entityType = "element" required="false" phraseTextID="longSummary" disabled = #not(editmode)#>
				</cf_relayFormElementDisplay>
			</cfif>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="grey-box-top">
			<!--- NJH 2012/10/23 - Social Sharing CR - allow individual pages to be turned off for sharing --->
			<cfif showThis.share is 1>
				<cfset formFieldList = listAppend(formFieldList,"share")>
				<cfset elementShare = true>
				<cfif not (application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.element.added"))>
					<cfset elementShare = false>
				</cfif>
				<cfif isNewRecord>
					<cfif elementShare>
						<cfset elementRecord.share = 1>
					<cfelse>
						<cfset elementRecord.share = 0>
					</cfif>
				</cfif>

				<cfset disableShare = false>
				<cfif not elementShare or not editMode>
					<cfset disableShare = true>
				</cfif>
				<cfset noteText = "">
				<cfif not elementShare>
					<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia")>
						<cfset noteText = "Social Media has not been enabled">
					<cfelse>
						<cfset noteText = "Element sharing has been turned off">
					</cfif>
				</cfif>

				<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="share" label="phr_element_share" currentValue="#elementRecord.share#" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" disabled = #disableShare# noteText="#noteText#" noteTextPosition="right">
			</cfif>

			<!--- 2013-09-10	YMA	Case 436153 turn Embedly off by default but allow it to be enabled at element level. --->
			<cfif showThis.embedly is 1>
				<cfset formFieldList = listAppend(formFieldList,"embedly")>
				<cfset elementEmbedly = true>
				<cfset embedlyDisabled = false>
				<cfif not application.com.settings.getSetting("plugins").embedly.enable>
					<cfset elementEmbedly = false>
					<cfset noteText = "Embedly has been turned off">
					<cfset embedlyDisabled = true>
				</cfif>
				<cfif isNewRecord>
					<cfset elementRecord.embedly = 0>
				</cfif>

				<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="embedly" label="phr_element_embedly" currentValue="#elementRecord.embedly#" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" disabled = #embedlyDisabled# noteText="#noteText#" noteTextPosition="right">
			</cfif>

			<!---  sortOrder  --->
			<cfif showThis.sortOrder is 1>
				<CFSET sortOrderQuery = queryNew("display,value","integer,integer")>
				<CFLOOP INDEX="i" FROM="1" TO="300" STEP="1"> <!--- 2015-11-05 ESZ Case 444875 change sort order from 100 to 300 --->
					<cfset QueryAddRow(sortOrderQuery)>
					<cfset querySetCell(sortOrderQuery,"display",i)>
					<cfset querySetCell(sortOrderQuery,"value",i)>
				</CFLOOP>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="select" fieldname="sortOrder" currentValue="#elementRecord.SortOrder#" label="phr_element_sortOrder" query="#sortOrderQuery#" display="display" value="value" disabled = #not(editmode)#> <!--- Sort order --->

				<cfif editmode>
					<cfset formFieldList = listAppend(formFieldList,"SortOrder")>
				</cfif>

			</cfif>



			<!---  showRate --->
			<cfif showThis.showRate is 1 and getType.showRate is 1>
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"rate")>
				</cfif>
				<cf_relayFormElementDisplay label="phr_element_ContentRating" relayFormElementType="Radio" list="1#application.delim1#phr_element_ShowRateContentFeedbackForm,0#application.delim1#phr_element_DoNotShowContentRatingOnPage" fieldname="rate" currentvalue=#elementRecord.rate# disabled = #not(editmode)# > <!--- Label: Content Rating --->

			</CFIF>


			<!--- showonmenu  --->
			<cfif showThis.showonmenu is 1>
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"showonmenu,hideChildrenFromMenu,showonSecondaryNav")>
				</cfif>

				<!--- check whether any parent are being hidden --->
				<cfquery name="checkShowOnMenu" dbtype="query">
					select 1 from Level1Rights.parentQuery where showOnMenu <> 1
				</cfquery>

				<cfquery name="checkShowOnMenu2" dbtype="query">
					select 1 from Level1Rights.parentQuery where hideChildrenFromMenu = 1
				</cfquery>

				<cfset noteText="">
				<cfif checkShowOnMenu2.recordcount is not 0>
					<cfset noteText="Note that this item will not appear on Main Navigation because a parent is set to hide children">
				<cfelseif checkShowOnMenu.recordcount is not 0 and #elementRecord.showonmenu# IS not 0>
					<cfset noteText="Note that a parent of this item may not be shown on the menus, therefore this item may not be shown">
				</cfif>

				<cf_relayFormElementDisplay label="phr_element_showOnMainNavigation" relayFormElementType="Radio" list="1#application.delim1#phr_element_Always,0#application.delim1#phr_element_Never,2#application.delim1#phr_element_OnlyWhenLoggedIn,-1#application.delim1#phr_element_OnlyWhenLoggedOut" fieldname = "showOnMenu" currentvalue = #elementRecord.showOnMenu# disabled = #not(editmode)# noteText="#noteText#" noteTextPosition="bottom"> <!--- Navigation<BR>Show on Main Navigation --->

				<!---
						<cfif checkShowOnMenu2.recordcount is not 0>
						<Br>Note that this item will not appear on Main Navigation because a parent is set to hide children
						<cfelseif checkShowOnMenu.recordcount is not 0 and #elementRecord.showonmenu# IS not 0>
						<BR>Note that a parent of this item may not be shown on the menus, therefore this item may not be shown
						</cfif>

				 --->

				<!--- WAB added these 2007-03-14 --->
				<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="hideChildrenFromMenu" label="phr_element_HideChildrenFromMainNavigation" currentValue="#elementRecord.hideChildrenFromMenu#" list="1#application.delim1#phr_element_yesHide,0#application.delim1#phr_no" disabled = #not(editmode)#> <!--- Label : Hide Children From Main Navigation --->
				<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="showonSecondaryNav" label="phr_element_ShowItemOnSecondaryNavigation" currentValue="#elementRecord.showonSecondaryNav#" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" disabled = #not(editmode)#> <!--- Label: Show this item on Secondary Navigation --->

			</CFIF>


			<!--- isExternalFile --->
			<CFIF getType.showIsExternal and showThis.showIsExternal >
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"isExternalFile")>
				</cfif>
				<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="isExternalFile" label="phr_element_ContentNavigation" currentValue="#elementRecord.isExternalFile#" list="0#application.delim1#phr_element_NavigateToContentFromThisElement,1#application.delim1#phr_element_NavigateToContentAtURLBelow" disabled = #not(editmode)#>

				<!--- Label Content Navigation
						Navigate to the content from this element
						Navigate to content at the URL below
				 --->
			</CFIF>

			<!--- URL --->
			<CFIF getType.showURL and showThis.showURL>
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"URL")>
				</cfif>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="text" fieldname="url" label="phr_element_URLIfExternalLink" currentValue="#elementRecord.URL#" size="70" maxlength="255"  disabled = #not(editmode)#>

			</CFIF>

			<!--- Keywords  --->
			<cfif showThis.showKeywords eq 1 and getType.showKeywords is 1>
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"KeyWords")>
				</cfif>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="textArea" fieldname="Keywords" label="phr_element_KeywordsUsedToFindContent" currentValue="#elementRecord.Keywords#" size="4096" maxlength="4096"  disabled = #not(editmode)#> <!--- Label: Which keywords can be used to find this content  --->
			</cfif>

			<!--- showParameters  --->
			<cfif showThis.parameters eq 1 and getType.showParameters eq 1>
				<cfset parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(elementRecord.parameters,",")>

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="parameters" currentValue="" label="phr_element_paramsToUseWithContent">
						
						<!--- ------------------------------------------------------------------------------------------------
									A Sarabi Advanced parameters tooltip [#485] 01/09/2016
						------------------------------------------------------------------------------------------------ --->
						<img src="/images/misc/iconhelpsmall.gif" id="advparamhelp" style="display: inline-block" data-toggle="popover">
						<script language="javascript" src="../javascript/advancedparameterspopover.js"></script>					
						<!--- ------------------------------------------------------------------------------------------------
									END Advanced parameters tooltip
						------------------------------------------------------------------------------------------------ --->
						<table id="undocumentedParamsTable">
							<!--- 2012-04-24 WAB	Case 432049 changed naming convention so that Parameter becomes a Suffix --->
							<cf_addUndocumentedParametersToTable
									parameterStructure = #parametersStruct#
									tableID = "undocumentedParamsTable"
									parameterSuffix = "parameter"
									hideParameter = "partnerPack"
								>
						</table>
					</cf_relayFormElementDisplay>

				<cfset paramNoteText = "">
				<cfif editMode>
					<cfset paramNoteText= "phr_element_enterVariableAs">
					<cfset formFieldList = listAppend(formFieldList,"Parameters")>
				</cfif>
			</CFIF>


		<!--- showBorderset                                                                  --->
			<cfif showThis.borderset eq 1 >
				<cfif editMode>
					<cfset formFieldList = listAppend(formFieldList,"borderset")>
				</cfif>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="text" fieldname="BorderSet" label="phr_element_BorderSet" currentValue="#elementRecord.borderset#"  disabled = #not(editmode)# noteText="phr_element_borderSetToUseIfDifferent" noteTextPosition="right">
			</CFIF>


		<!--- Add details of the user groups who should have access to this record --->
			<CFIF (getType.showRecordRights and showThis.showRecordRights is 1) or showThis.LoginRequired is 1>
					<!--- <TR><TD class="label" ALIGN="left" VALIGN="top"><B>VISIBILITY</B>
						</TD>
						<TD>
						</TD>
					</TR>	 --->

				<cfif showThis.LoginRequired is 1>
					<cfset loginNoteText = "">
					<cfif isDefined("Level1rights") and Level1rights.loginrequired gte 1 and elementRecord.LoginRequired is 0>
						<cfset loginNoteText="A parent of this element requires the user to be logged in, so this element will only will be available when the user is logged in">
					</cfif>

					<cfif editMode>
						<cfset formFieldList = listAppend(formFieldList,"LoginRequired")>
					</cfif>
					<cf_relayFormElementDisplay relayFormElementType="radio" fieldname="LoginRequired" label="phr_element_requiresLoginToSeeContent" currentValue="#iif(elementRecord.LoginRequired neq '',elementRecord.LoginRequired,0)#" list="1#application.delim1#phr_yes,0#application.delim1#phr_No,2#application.delim1#phr_element_requiresFullLogin" disabled = #not(editmode)# noteText="#loginNoteText#" noteTextPosition="top">
					<!--- Label Does the user need to be logged in to see this content?
							Yes
							No
							Requires Full Login (ie not remember me)
						<cfif isDefined("Level1rights") and Level1rights.loginrequired gte 1 and elementRecord.LoginRequired is 0>
							<BR>A parent of this element requires the user to be logged in, so this element will only will be available when the user is logged in
						</cfif>

					 --->


				</cfif>
				<CFIF getType.showRecordRights and showThis.showRecordRights >
					<cfif editmode>
						<input type="hidden" name="frmUpdateRecordRights" value="yes">
					</cfif>

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="userGroupsVisibility" currentValue="" label="To see this content users must belong to">
						<!--- Label: Which user groups / users should see this content ? --->
						<CF_UGRecordManager entity="element" entityid="#elementRecord.id#" Form="mainForm" level = "1" method = "#iif(editmode,de("edit"),de("view"))#" useExtendedRights=true>
						<cfif Level1Rights.usergroupsofParents is not "">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<p id="visibilityFrom">This page inherits visibility restrictions from:</p>
									<ul>
									<cfloop query ="Level1Rights.parentquery">
										<cfif hasRecordRights is 1 and elementID is not elementRecord.id>
											<li>
												<a <!---title="VISIBILITY RESTRICTIONS#chr(10)##htmlEditFormat(reReplace(recordRights,"\t",chr(160)&chr(160)&chr(160)&chr(160),"ALL"))#"---> href="javascript:void(openNewTab('element_#elementid#','#Headline# (#elementid#)','/elements/elementEdit.cfm?RecordID=#elementid#&frmWizardPage=visibility',{reuseTab:true,reuseTabJSFunction: function () {return true}}))" <!---title="View Parent Visibility"--->>#htmleditformat(headline)#</a>
											</li>
										</cfif>
									</cfloop>
									</ul>
								</div>
							</div>
						</cfif> <!--- Usergroup visibility as inherited from parents --->

					</cf_relayFormElementDisplay>

				</cfif>
			</CFIF>

			<!--- Add details of the user groups who should have access to this record --->
			<CFIF getType.showCountryScope and showThis.showCountryScope>
				<cfset countriesAndregionsList = listappend(request.relaycurrentuser.countrylist,request.relaycurrentuser.regionlist)>
				<cfif editmode>
					<input type="hidden" name="frmUpdateCountryScope" value="yes">
				</cfif>

				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="userCountriesVisibility" currentValue="" label="phr_element_usersCountriesToSeeContent"> <!--- Which users countries should see this content ? --->
					<CF_CountryScope entity="element" entityid="#elementRecord.id#" Form="mainForm" method = "#iif(editmode,de("edit"),de("view"))#" countryIDFilterList = #countriesAndregionsList# width="150">

						<cfif Level1Rights.countriesOfParents is not "">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									This page inherits country restrictions from:<BR>

									<cfloop query ="Level1Rights.parentquery">
										<cfif hasCountryScope is 1 and elementID is not elementRecord.id>
											<a title="COUNTRY RESTRICTIONS#chr(10)##htmlEditFormat(reReplace(countryScope,"\t",chr(160)&chr(160)&chr(160)&chr(160),"ALL"))#" href="javascript:void(openNewTab('element_#elementid#','#Headline# (#elementid#)','/elements/elementEdit.cfm?RecordID=#elementid#&frmWizardPage=visibility',{reuseTab:true,reuseTabJSFunction: function () {return true}}))" title="View Parent Visibility">#htmleditformat(headline)#</a><BR>
										</cfif>
									</cfloop>

								</div>
							</div>


						</cfif> <!--- Usergroup visibility as inherited from parents --->



				</cf_relayFormElementDisplay>

			</cfif>


			<CFIF getType.showRecordRights and showThis.showRecordRights >
				<tr>
					<th colspan="2">phr_element_editRights</th>
				</tr>

				<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="edit" label="phr_element_whichUsersCanEditContent"> <!--- Label: Which user groups / users can edit this content ? --->
					<CFIF hasOwnerRights>
						<CF_UGRecordManager entity="element" entityid="#elementRecord.id#" Form="mainForm" level = "2" method = "#iif(editmode,de("edit"),de("view"))#">
					</CFIF>
					<cfif Level2Rights.usergroups is not "">phr_element_OverallRightsRequiredToEditRecord <BR>#htmleditformat(Level2Rights.usergroups)#</cfif> <!--- Overall rights required to edit this record  --->
				</cf_relayFormElementDisplay>


						<!--- need check here for whether person has rights to change this --->
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="userGroupsEdit" currentValue="" label="phr_element_userGroupToEditContent"> <!--- Which user groups / users can determine who should be able to edit this content ? --->
					<cfif IsOwner>	<!--- or isAdmin   --->
						<CF_UGRecordManager entity="element" entityid="#elementRecord.id#" Form="mainForm" level = "4" method = "#iif(editmode,de("edit"),de("view"))#">
					</cfif>
					<cfif Level4Rights.usergroups is not "">phr_element_OverallRightsRequiredToChooseEditors <BR> #htmleditformat(Level4Rights.usergroups)#</cfif> <!--- Overall rights required to choose editors  --->
				</cf_relayFormElementDisplay>
			</cfif>




		<!--- edit flags --->
			<cfif showThis.showFlags is 1>
			<tr>
				<td><label>phr_element_OtherProfiles</label></td>
				<td>
					<CFSET displayas = "select">
					<CFSET flagMethod = "edit">
					<CFSET entityType = "10">
					<CFSET thisEntity = elementRecord.id>
					<!--- <CFSET GroupList=GetOpp.flagid> --->
					<input type="hidden" name="frmAddFlags" value="yes">
					<CFInclude TEMPLATE="/flags/allFlag.cfm">
				</td>
			</tr>
			</cfif>
		<!--- 2005/11/09 - GCC - CR_SNY556 Stories Rollover --->
			<cfif showThis.linkImage is 1>
				<cfparam name="fileValue" default="">
				<cfset linkImagePath=application.userfilesabsolutepath & "content\elementLinkImages\linkImage-" & elementRecord.id & "-thumb.jpg">
				<cfset linkImagePath2=application.userfilesabsolutepath & "content\elementLinkImages\linkImageSquare-" & elementRecord.id & "-thumb.jpg">
				<!--- 2012/04/16	IH	added random number to prevent image caching --->
				<cfset randNum = randRange(1,100000)>
				<tr>
					<td>
						<label>phr_element_ElementLinkImage</label>
					</td>
				<td>
				<cfif fileexists(linkImagePath)>
					<img src="/content/elementLinkImages/linkImage-#elementRecord.id#-thumb.jpg?c=#randNum#" border=0 width="120">
					<cfif fileexists(linkImagePath2)>
						<img src="/content/elementLinkImages/linkImageSquare-#elementRecord.id#-thumb.jpg?c=#randNum#" border=0 width="85">
					</cfif>
					<input type="radio" name="deleteLinkImage">phr_element_DeleteImage
				<cfelse>
					<cf_relayFormElement type="file" fieldname="linkImage" acceptType="image" currentValue="">
					<!--- <input type="file" name="linkImage"> --->
				</cfif>
				</td>
				</tr>
			</cfif>

			<cfif showThis.campaignID is 1>
				<cf_relayFormElementDisplay class="form-control" relayFormElementType="select" nulltext="Phr_Ext_SelectAValue" fieldname="campaignID" currentValue="#elementRecord.campaignID#" label="Phr_Sys_AssociateCampaign" query="#application.com.relayCampaigns.GetCampaigns()#" display="campaignName" value="campaignID" disabled = #not(editmode)#>
				<cfif editmode>
					<cfset formFieldList = listAppend(formFieldList,"campaignID")>
				</cfif>
			</cfif>

		<!--- List all the children records                                                  --->
			<CFIF isDefined("getChildren") and getChildren.recordCount gt 0>
			<label>phr_element_hasFollowingChildren</label>

			<div id="elementEdit">
				<cfif ElementRecord.GetChildrenFromParentID is not 0>
					These Children are imported from Page #htmleditformat(ElementRecord.GetChildrenFromParentID)# <BR>
				</cfif>
				<ul>
					<CFLOOP query="getChildren"	>
						<li>
							<A HREF="javascript:void(openNewTab('element_#elementID#','#Headline# (#IIF(elementTextID neq "",DE(elementTextID),DE(elementID))#)','/elements/elementEdit.cfm?RecordID=#elementID#',{reuseTab:true,reuseTabJSFunction: function () {return true}}))">#htmleditformat(headline)# (#htmleditformat(elementID)#)</a>
						</li>
					</cfloop>
					<!--- <BR><A HREF="javascript:newChildElement(#elementTypeID#,#parentID#)">Add New Child</a> --->
				</ul>
			</div>
			</cfif>

			<CFIF not isNewRecord >
				<cf_entityLastUpdatedInfo
					created="#getElementRecord.Created#"
					createdby="#getElementRecord.CreatedBy#"
					lastUpdated="#getElementRecord.LastUpdated#"
					lastUpdatedby="#getElementRecord.LastUpdatedBy#">

				<CF_INPUT TYPE="hidden" NAME="lastUpdatedBy" VALUE="#request.relayCurrentUser.userGroupID#">
				<CFSET formFieldList = listAppend(formFieldList,"lastUpdatedBy")>
			</CFIF>
		</CFOUTPUT>

		</cf_relayFormDisplay>

		<!--- form buttons --->
		<CFOUTPUT>

			<INPUT TYPE="hidden" NAME="FieldList" VALUE="<cfif editmode>#FormFieldList#</cfif>">
			<INPUT TYPE="hidden" NAME="frmTask" VALUE="update">
			<CF_INPUT TYPE="hidden" NAME="frmWizardPage" VALUE="#frmWizardPage#">
		</cfoutput>
		</FORM>
		</div>
	</div>
</div>
<CFIF not isNewRecord >
	<!--- IF the ElementType has defined related fileCategories then call  CF_RelatedFiles--->
	<CFIF getType.relatedFileCategories IS NOT "">
		<!--- Section for uploading related file types --->
		phr_element_uploadAnyFilesRelatedToElementBelow
		<cf_relatedFile action="list,put" entity="#elementRecord.id#" entitytype="element" fileCategory="#getType.relatedFileCategories#">
	</CFIF>
</cfif>