<!--- �Relayware. All Rights Reserved 2014
2015/12/02 SB Removed tables. Bootstrap --->
<!--- ElementTreeDefEdit --->
<cfparam name="reload" default = 0>
<cfparam name="treeid" default = 0>
<!--- get all trees --->


<cfif reload is 1>
	<cfset application.com.relaycurrentsite.loadElementTreeDefinitions()>
	<div class="successblock">Trees Reloaded</div>
</cfif>

<div id="elementTreeDef">
<h3>Choose Tree to view</h3>
<cfloop item="id" collection="#application.ElementTreeDefByID#">
	<cfoutput><div class="margin-bottom-15"><a href="?treeid=#id#" class="btn btn-primary">#application.ElementTreeDefByID[id].elementTreeName#</a></div></cfoutput>
</cfloop>
</div>


<cfif treeid is not 0>
	<cfset application.com.relaycurrentsite.outputTreeDetails(treeid)>
</cfif>



<cfoutput><a href="?treeid=#treeid#&reload=1">Reload Tree Defs into Memory</a></cfoutput>




