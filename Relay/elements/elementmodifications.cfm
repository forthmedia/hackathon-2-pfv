<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Modifications report for the current Element
--->

<!---
Author   WAB
Date	Jan 2006
SB 2015/12/11 Added bootstrap to table.

 --->



<cfquery name="getMods" datasource="#application.sitedatasource#">
SELECT mr.moddate  ,
	(select name from usergroup where usergroupid = mr.ActionByCF) as UserName,
		mr.action,
		med.fieldname ,
		mr.oldval,
		mr.newval ,
		CASE substring(action,2,1) WHEN 'A' THEN 'Record Added' WHEN 'M' THEN 'Record Modified' ELSE action END as translatedAction
FROM modregister mr
		left join
	 modentitydef med on mr.modentityid = med.modentityid
WHERE recordID =  <cf_queryparam value="#recordid#" CFSQLTYPE="CF_SQL_INTEGER" >
AND Action like 'E%'
and med.fieldname <> 'lastupdated'

union all

select 	modified,
		(select firstname  + ' ' + lastname from person  where personid = modifiedby ) as UserName,
		'',
		phrasetextid + '(' + Language + ')',
		phrasetext,
		'',
		''
	 from phraselist pl inner join xphrases xp on pl.phraseid = xp.phraseid inner join language l on l.languageid = xp.languageid
where  entitytypeid = 10 and entityid =  <cf_queryparam value="#recordid#" CFSQLTYPE="CF_SQL_INTEGER" >

</cfquery>

<!--- problems doing the ordering in SQL - didnt like the nText fields, so doingit in memory --->
<cfquery name = getMods dbtype="query">
	select * from getMods order by moddate desc
</cfquery>


<cfoutput>

<table class="table table-hover table-striped table-wrap">
	<tr>
		<td colspan="6">List of Modifications made</td>
	</tr>
	<tr>
		<th style="width:10%">Date2</th>
		<th style="width:10%">User</th>
		<th style="width:10%">Action</th>
		<th style="width:10%">Field</th>
		<th style="width:50%">Old Value</th>
		<th style="width:10%">New Value</th>
	</tr>
	<cfloop query="getMods">
		<tr<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<td>#dateformat(ModDate)#</td>
			<td>#htmleditformat(UserName)#</td>
			<td>#htmleditformat(translatedAction)#</td>
			<cfif oldval is "" and newval is "">
				<td colspan="3">#htmleditformat(fieldname)#</td>
			<cfelse>
				<td>#htmleditformat(fieldname)#</td>
				<td>#htmleditformat(oldval)#</td>
				<td>#htmleditformat(NewVal)#</td>
			</cfif>
		</tr>
	</cfloop>

</table>

	<cfif isDefined("frmWizardPage")>

				<form name=mainForm method = get>   <!--- WAb 2008/03/03  changed to a GET - all other pages in this area are expecting url variables --->
					<CF_INPUT type="hidden" name= "frmWizardPage" value = "#frmWizardPage#">
					<input type="hidden" name= "frmTask" value = "">
					<CF_INPUT type="hidden" name= "nextRecordID" value = "#recordid#">
					<CF_INPUT type="hidden" name= "recordid" value = "#recordid#">
				</form>
			</cfif>


</cfoutput>







