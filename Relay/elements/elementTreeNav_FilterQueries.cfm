<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

page included in ElementTreeNav.cfm
does queries for populating filters

 --->

	<!--- get list of all the usergroups which are used in this tree, so that we can populate a drop down from it 
		I want to remember all the usergroups from the whole tree even if getelements is bringing back a subset, 
		so and going to remember them in a session variable
	---> 
		<CFPARAM name = "session.elementree.usergrouplist" default = "">
	
		<!--- get the list of usergroupids from the current tree - rather a nasty loop I'm afraid and will give lots of duplicates--->
		<cfset usergroupsInTree = "0">
		<cfif isDefined("getELements")>
			<cfloop query = "getELements">
				<cfif recordrightsIDs is not "" AND usergroupsInTree does not contain recordrightsIds><cfset usergroupsInTree = listappend(usergroupsInTree,recordrightsIds)></cfif>
			</cfloop>
		</cfif>
	
			<cfquery name="getusergroups" datasource="#application.sitedatasource#" >
			select distinct convert(varchar,ug.usergroupid) as datavalue, ug.name as displayvalue ,
				case when dbo.listfind ('#frmShowTreeForusergroups#',ug.usergroupid) <> 0 then 1 when personid is null then 2 else 3 end as sortorder
			from usergroup ug where (ug.usergroupid  in ( <cf_queryparam value="#usergroupsInTree#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfif session.elementree.usergrouplist is not "">
			or (ug.usergroupid  in ( <cf_queryparam value="#session.elementree.usergrouplist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and ug.usergroupid <> 0)
			</cfif>)
			union select '-1','Someone with all rights',1
			union select '0','Someone with NO rights',1
			order by sortorder, ug.name
			</cfquery>
		<cfset session.elementree.usergrouplist = valuelist(getusergroups.datavalue)>
		
		
	<!--- get list of countries to filter by --->	
		<cfquery name="getCountries" datasource="#application.sitedatasource#" >
		select countryDescription as displayvalue, countryid as datavalue,3 as sortindex from country where countryid in (#request.relaycurrentuser.countrylist#)  
			union
		select 'All Countries', -1, 1
			union
		select 'All My Countries', -2, 1
			union
		select 'Unknown Country', 0, 2
	
		order by sortindex, countrydescription	
		</cfquery>
		
		
			
	<!--- get query of statuses --->
	
	<CFQUERY NAME="GetStatII" DATASOURCE="#application.SiteDataSource#">
		select convert(varchar(3), StatusID) + '|0' as dataValue ,  status as displayValue, 2 as order1, statusid
		from ElementStatus
	
		union
		
		select convert(varchar(3), StatusID) + '|1' , 'Between ' + status + ' and Live', 3, statusid
		from ElementStatus where status <> 'Live'
	
		union
	
		select '-1|1' , 'All Statuses', 1, -1
		union
		select '0|1' , 'All Statuses except Retired', 1, 0
				
		order by order1,statusid
		
		
	</CFQUERY>

