<!--- �Relayware. All Rights Reserved 2014 --->
<!---

elementPipeLineReport.cfm

WAB Sep 2005


2005-10-04 added filtering by users countries

 --->

 <cfparam name = "frmShowTreeForCountryID" default = "9">
 <cfparam name = "frmShowTreeStatusID" default = "4">
 <cfparam name = "frmGoingLiveInNextXDays" default = "4">
 <cfparam name = "frmHasGoneLiveInPastXDays" default = "4">
 <cfparam name = "frmUpdatedInPastXDays" default = "4">


<script>
 	function loadPipeLineReport() {

		page = '/webservices/elementTreeNav.cfm?method=pipeLineReport'
		parameters = Form.serialize(document.pipelineForm)
		// alert (parameters)
		var myAjax = new Ajax.Updater(
			'pipeline_results',
			page,
			{
				method: 'get',
				parameters: parameters,
				evalScripts: true

			});


			return false
	}

</script>




	<div id="pipeline_form">

	<cfoutput>
	<form name="pipelineForm">
		<CF_INPUT type="hidden" name="parentElementID" value="#parentElementID#" >

		<div class="form-group elementTreeNavCont">
			<label>Coming Live in the next</label>
			<div class="elementTreeNavCont">
				<div class="pipeline_form_left">
					<CF_INPUT type="number" name="frmGoingLiveInNextXDays" value="#frmGoingLiveInNextXDays#" size="2">
				</div>
				<span>days</span>
			</div>
		</div>
		<div class="form-group elementTreeNavCont">
			<label>Gone Live in the previous</label>
			<div class="elementTreeNavCont">
				<div class="pipeline_form_left">
					<CF_INPUT type="number" name="frmHasGoneLiveInPastXDays" value="#frmHasGoneLiveInPastXDays#" size="2">
				</div>
				<span>days</span>
			</div>
		</div>
		<div class="form-group elementTreeNavCont">
			<label>Live Content Updated in the last</label>
			<div class="elementTreeNavSubCont">
				<div class="pipeline_form_left">
					<CF_INPUT type="number" name="frmUpdatedInPastXDays" value="#frmUpdatedInPastXDays#" size="2">
				</div>
				<span>days</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 margin-top-10">
				<input type=button value="Display Pipeline" onClick="javascript:return loadPipeLineReport()" class="btn btn-primary">
			</div>
		</div>
	</form>

	</div>


	<div id="pipeline_results">
	</div>


</cfoutput>


