<!--- �Relayware. All Rights Reserved 2014 --->
 
<!--- copyElement.cfm

Author WAB

Date 2000-06-23

Purpose 

To take a copy of an element, or elements

Takes  ElementID

2016-02-03	WAB Mass replace of displayValidValues using lists to use queries
--->

<!--- get element and parent--->
	<CFQUERY NAME="getElement" DATASOURCE="#application.SiteDataSource#">
	select 
		id as elementid,
		*
	from 
		element
	where 
		id =  <cf_queryparam value="#ElementID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>


<!--- get element and parent--->
	<CFQUERY NAME="getSameSortOrder" DATASOURCE="#application.SiteDataSource#">
	select 
		id as elementid,
		*
	from 
		element
	where 
			parentID =  <cf_queryparam value="#getElement.parentID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<CFIF getElement.sortOrder is not "">and sortOrder =  <cf_queryparam value="#getElement.sortOrder#" CFSQLTYPE="CF_SQL_INTEGER" >  <CFELSE>and 1 = 0</cfif>
	</CFQUERY>

	<CFSET sameSortOrderIds = valueList(getSameSortOrder.elementid)>
	<CFSET sameSortOrderIds = listDeleteAt(sameSortOrderIds, listFind(sameSortOrderIds,ElementID))>



<CFIF getElement.elementTypeID is not 29>

<!--- get children of grandparent 
	ie sections  of all newsletters
	apologies for the cryptic query quickly done
--->
 	<CFQUERY NAME="getUncles" DATASOURCE="#application.SiteDataSource#">
	select distinct
		Level3_id as dataValue,
		Level2_Headline + ':' + Level3_Headline as displayValue
	from 
		wabElementView
	where 
		Level1_ID = (select distinct level1_ID from wabelementView where level3_id =  <cf_queryparam value="#getElement.parentID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</CFQUERY>




<CFELSE>
	
	<CFQUERY NAME="getUncles" DATASOURCE="#application.SiteDataSource#">
	select distinct
		Level2_id as dataValue,
		Level2_Headline as displayValue
	from 
		wabElementView
	where 
		Level1_ID = (select distinct level1_ID from wabelementView where level2_id =  <cf_queryparam value="#getElement.parentID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</CFQUERY>
	
	
</cfif>	
	
	
	

  



<cf_head>
	<cf_title>Copy Element</cf_title>
</cf_head>

<cf_body onload="window.focus()">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<CFOUTPUT>
<FORM name="mainForm" action = "/elements/elementActions.cfm" method = "post">
	
	<CF_INPUT NAME="recordID" TYPE="HIDDEN" value= "#ElementID#">
	<input NAME="frmTask" TYPE="HIDDEN" value= "copy">
		<TR>
			<TD COLSPAN="2" CLASS="Submenu">
			Create copy of Element: "#htmleditformat(getElement.Headline)#" 
			</TD>
		</tr>	
	
	<!--- select new Parent --->
		<TR>
			<TD>
			Choose the parent for the copy: 	
			</TD>
			<TD>
				<cf_displayValidValues
					formFieldName = "frmCopyParentID"
					validValues = "#getUncles#"
					currentValue = "#getElement.parentID#"	>
				
			</td>
		</tr>	

		<CFIF getSameSortOrder.recordCount is not 0>
		<TR>
			<TD>
				Do you want to copy all items at the same position (sortOrder)
			</td>
			<TD>
				<CF_INPUT TYPE="checkbox" NAME="RecordID" VALUE="#sameSortOrderIds#" CHECKED>
			</td>
		
		</tr>
		</cfif>		
		
		<TR>
			<TD>
				Do you want to copy all translations
			</td>
			<TD>
				<INPUT TYPE="checkbox" NAME="frmCopyTranslations" VALUE="1" CHECKED>
			</td>
		
		</tr>
		<TR>
			<TD>
				Do you want to copy all recordRights
			</td>
			<TD>
				<INPUT TYPE="checkbox" NAME="frmCopyRecordRight" VALUE="1" CHECKED>
			</td>
		
		</tr>
		<TR>
			<TD>
				Do you want to copy all countryScopes
			</td>
			<TD>
				<INPUT TYPE="checkbox" NAME="frmCopyCountryScope" VALUE="1" CHECKED>
			</td>
		
		</tr>

		<TR>
			<TD>
				What do you want to call it
			</td>
			<TD>
				<CF_INPUT type="text" name="frmCopyHeadline" value = "#getelement.Headline#[1]">
			</td>
		
		</tr>

		<TR ALIGN="right">
			<TD colspan=2>
				<INPUT type="Submit" name="Copy" value = "Create Copy">
			</td>
		</tr>	
	
	

</cfoutput>
</form>
</table>



