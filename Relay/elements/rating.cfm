<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			rating.cfm
Author:				WAB
Date started:		2001

Description:		output a rating box

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


WAB 2006-12-11 changed entity column to entityTypeID to fit in with all other tables on system (was causing dedupe problems with old name)
WAB 2010/10/13  removed cf_get phrase  and replaced with CF_translate
ACPK 2014/11/10 completely rewrote template to use standard functions
 --->

<cfset thisEntityTypeID = application.entityTypeID ['Element']>
<CFOUTPUT>
	<div id="rating">#application.com.relayRating.getRatingsWidget(entityTypeID=thisEntityTypeID,entityID=request.currentElement.id)#</div>
</CFOUTPUT>

