<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">

<!--- mods
WAB May 00  lots of mods
		Added a variable frmTask which can be set to edit, delete, cancel etc on the elementedit page - not fully tested on areas which I was not using
		Added a variable NextRecordID which allows you to save a record and then go elsewhere, not fully tested in all situations yet


WAB 2006-11-21 changed end of edit task to a cflocation rather than cfinclude (not too good for debugging but better for page refreshing)

 NJH 2008-06-24 - When uploading a file for an element link image, create the destination directory if it doesn't yet exist before the file is uploaded.
WAB 2010-09-30 make sure that after adding a translation , the next page displays that language
			2010-10-05  WAB         replaced application.userfiles absolutepath & application.files directory with application.paths.content
LID 4229 deal with the hasCountryScope and hasRecordRights fields in a trigger
2012/03/16	IH	Case 424464 delete all 6 images

2012-10-10 	PPB Case 430878 enhancement suggestion comments only
2014-10-16	RPW	CORE-868 Elements - Problem creating thumbnail
2014-10-31	RPW	Removed extra <cfscript> tag added in error by the last merge.
2015-12-01 	WAB PROD2015-290 Visibility Project. Change reference to hasRecordRights to new hasRecordRightsBitMask
2015-12-01 	WAB Removed a plethora of code which was never called (referenced variables never set anywhere in element directory)
2016-02-24	RJT	PROD2015-601 Prevent multiple identical Elemnts being inserted (if for example the save button is clicked twice)
--->

<!---			NAVIGATION BUTTONS (view page)			--->

<!--- WAB 2006-04-26 added this check because people using SAFARI browsers kept managing to call this template directly without any parameters --->
<cfif not isDefined ("recordID") or not isDefined ("frmTask")>
	<CFOUTPUT>
	This page must be called with RecordID and frmTask Set
	</CFOUTPUT>
	<CF_ABORT>
</cfif>

<CFPARAM name="nextRecordID" default="#recordID#">   <!--- wab: added for backwards compatability really --->
<cfparam NAME="form.summaryInCOntents" default="0">
<cfparam NAME="form.showSummary" default="0">
<cfparam NAME="form.showLongSummary" default="0">


<cfparam name="treeDirty"  default="0">
<cfparam name="mainDirty"  default="1">
<cfparam name="translationsDirty"  default="1">
<cfparam name="Dirty"  default="#bitor(maindirty,translationsdirty)#">



<cfparam name="message"  default="">

<cfparam name="request.standardSizeHeight" type="numeric" default=180>
<cfparam name="request.standardSizeWidth" type="numeric" default=240>
<cfparam name="request.thumbSizeHeight" type="numeric" default=90>
<cfparam name="request.thumbSizeWidth" type="numeric" default=120>
<cfparam name="request.squareSizeHeightWidth" type="numeric" default=180>
<cfparam name="request.thumbSquareSizeHeightWidth" type="numeric" default=85>
<cfparam name="request.thumbImageType" default="jpg">



<!--- ==============================================================================
        frmTask = update                                                 
=============================================================================== --->
<CFIF (frmTask is "update" or frmTask is "save") >  <!--- WAB added save 'cause relayFormJavascripts.js was implemented and seems to use save rather than update  --->

	<cfset argumentsForNextPage = {RecordID = NextRecordID,reloadTreeForID=recordid,frmwizardPage=frmwizardPage}>

	<cfif structKeyExists(variables, "Content")>
		<CFSET Content = replaceNoCase(Content,"##","####", "ALL")>  <!--- replace all # so that CF is OK with them --->
	</cfif>

	
	<!--- join together any parameters into a list 
		2012-04-24 WAB	Case 432049 changed naming convention so that Parameter becomes a Suffix
	--->
	<cfset nameValuePairs = application.com.regExp.CollectFormFieldsIntoNameValuePairs(fieldSuffix="Parameter",fieldPrefix="")>
	<cfparam name="form.parameters" default= "">
	<!--- 2008-05-02 - GCC - list appending an empty param adds a comma to the end of the string which we don't want... --->
	<cfif namevaluepairs neq "">
		<cfset form.parameters = listappend(form.parameters,namevaluepairs)>
	</cfif>

		
	<CFIF isNumeric(Form.RecordID)>
		<cfset recordUpdated = false>
		
		<!--- process the record rights things if they are there --->
		<CF_UGRecordManagerTask>  
		<CF_CountryScopeTask>  		
		
		<cfif form.fieldList is not "">
			<!--- add a field for last updated date & time --->
			<cfset form.lastupdated = CreateODBCDateTime(now())>
			<cfset form.fieldList = listappend(form.fieldList,"lastUpdated",",")>
			
			<!--- remove leading and trailing whitespace to stop left() issues on display--->
			<cfif structKeyExists(form,"URL")>
				<cfset form.URL = trim(form.URL)>
			</cfif>		
			
			
			<!--- If there have been countryscope or recordrights changes then need to set dirty (for blowing the cache)--->
			<CFIF structKeyExists(form,"frmUpdateRecordRights") and form.frmUpdateRecordRights eq "yes" and UGRecordManager_UpdatesDone>
				<cfset dirty =1>
			</CFIF>
			<CFIF structKeyExists(form,"frmUpdateCountryScope") and form.frmUpdateCountryScope eq "yes" and CountryScope_UpdatesDone>
				<cfset dirty =1>
			</CFIF>
			
		
			<cfif not structKeyExists(form,"frmDeleteThisTranslation")>
				<cfif translationsDirty>
					<cfset translationsUpdated = application.com.relayTranslations.ProcessPhraseUpdateForm()>
					<cfset recordUpdated = true>
					<!--- WAB 2010-09-30 after adding a translation, didn't seem to be returning to it--->
					<cfif isDefined("request.phraseUpdate.lastupdate") and (not isdefined("frmLanguage") or frmLanguage  is "")>	
						<cfset argumentsForNextPage.phrLanguageID = request.phraseUpdate.lastupdate.languageid>
						<cfset argumentsForNextPage.phrCountryID = request.phraseUpdate.lastupdate.countryid>													
					</cfif>						
				</cfif>
			<cfelse	>
				<cfset application.com.relayTranslations.deleteEntityTranslation(entityType="Element",entityID=recordid,languageid=listfirst(form.frmDeleteThisTranslation,"-"),countryid=listlast(form.frmDeleteThisTranslation,"-"))>			
				<cfset frmLanguage = "">
			</cfif>
			
			<CFIF dirty>
				<!--- we do this update if any part of the record has been updated
					  it may only be the lastupdated field which gets changed - but this throws an item into modregister which is then used to populate the recent edits
					  this may miss changes to flags, where the dirty field has bot been set.
				 --->

				<!--- <cfupdate dataSource="#application.SiteDatasource#" tableName="Element" formFields="#form.fieldList#"> --->
				<cfset elementDetails = structNew()>
				<cfloop list=#form.fieldList# index="formField">
					<cfif structKeyExists(form,formField)>
						<cfset elementDetails[formField] = form[formField]>
					</cfif>
				</cfloop>

				<cfset application.com.relayEntity.updateEntityDetails(entityDetails=elementDetails,entityTypeId=application.entityTypeID.element,entityID=form.id)>
<!--- 2012-10-10 PPB Case 430878 we could replace the cfupdate (making it compatible with the use of relayEntity.insertEntity below) 
      by doing so, I think we could probably do away with maintaining the form.fieldList completely but for the fix to 430878 I added getContentFromID to fieldList (less testing reqd prior to 2012Summer release)
						
				<cfset application.com.relayEntity.updateEntityDetails(entityId=recordID,entityTypeID=application.entityTypeID["element"],entityDetails=form)>
 --->
				<!--- ******BIG HACK!!!!***** by NM to force SAVE OF HEADLINE with unicode chars --->
				<cfif structKeyExists(form,"headline")>
					<cfquery name="updateHeadline" datasource="#application.siteDataSource#">
						update element
						set headline =  <cf_queryparam value="#form.headline#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						where id =  <cf_queryparam value="#recordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>
				</cfif>
				<!--- <cfoutput><script>alert("#form.headline#");</script></cfoutput> --->

				<cfset recordUpdated = true>
			</CFIF>
		
			<CFIF ISDEFINED("frmElementidlist")>
				<CF_UPDATEDATA>
			</CFIF>
			
			<CFIF structKeyExists(form,"frmAddFlags") and form.frmAddFlags is not  "">
				<cfset tempTask = form.frmTask>  <!--- this variable has a different meaning in flagtask, so save it just in case --->
				<cfset form.frmTask = "update">
				<cfinclude template="/flags/flagTask.cfm">
				<cfset form.frmTask = tempTask>
			</CFIF>

			<!--- 2005-11-09 - GCC - CR_SNY556 Stories Rollover --->
		
			<cfif listContainsNoCase(form.fieldnames,"linkImage") is not 0>
				<!--- 2014-10-16	RPW	CORE-868 Elements - Problem creating thumbnail --->
				<cfset recordUpdated = true>
				<cfloop index="fieldname" list="#form.fieldnames#">
					<cfif left(fieldName,9) is "linkImage" and form[fieldName] neq "">
						<cfif listLen(fieldName,"_") is 2> <cfset suffix = "_" & listGetAt(fieldName,2,"_")><cfelse><cfset suffix = ""></cfif>

						<cfset UploadedFile =trim(fieldname)>
						<!--- Upload the file now  --->
						<cfset destination=application.paths.content & "\ElementLinkImages">

						<cfset cffile = application.com.fileManager.uploadFile(fileField=fieldname,destination=destination,nameConflict="Overwrite",accept="image/jpeg,image/gif,image/jpg,image/png")>
						<cfif not cffile.isOK>
							<cfoutput>#cffile.message#<a href="javascript:back(-1);">back</a></cfoutput>
							<CF_ABORT>
						</cfif>
						
						<cfset sourceImage = "linkImage#suffix#-" & form.recordID & "." & cffile.serverFileExt>
						<cfset sourceImage2 = "linkImageSquare#suffix#-" & form.recordID & "." & cffile.serverFileExt>
						
						<cftry>
							<cffile   
							 	action = "rename"   
								source = "#cffile.serverdirectory#\#cffile.serverFile#"   
								destination = "#cffile.serverdirectory#\#sourceImage#"    
							>
							<cffile   
							 	action = "copy"   
								source = "#cffile.serverdirectory#\#sourceImage#"   
								destination = "#cffile.serverdirectory#\#sourceImage2#"    
							>
							
							<!--- YMA ROD2016-2463 Keep an original version of the file in order to show high resolution version of the image in branchLinkList --->
							<cffile   
							 	action = "copy"   
								source = "#cffile.serverdirectory#\#sourceImage#"   
								destination = "#cffile.serverdirectory#\#form.recordID#_orignalFile.png"    
							>
							
							<cfset targetImageName = "linkImage#suffix#-" & form.recordID>
							<cfset targetImageName2 = "linkImageSquare#suffix#-" & form.recordID>
							<!--- process the image --->
							<!---  CASE 427398 - PJP:14 Sept 2012 - Swapped thumbSquareSize and squareSize as source file was being overwritten --->
							<!--- RMB - P-RIL001 - Fix for Link Icons / Tumbnails --->
							<!--- 2014-10-31	RPW	Removed extra <cfscript> tag added in error by the last merge. --->
							<cfscript>
								application.com.relayImage.convertImage(
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName & "-orig.jpg"
									);

								// 2014-10-16	RPW	CORE-868 Elements - Problem creating thumbnail
								application.com.relayImage.convertImage(
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName & ".jpg"
									);
								application.com.relayImage.convertImage(
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName2 & ".jpg"
									);

								application.com.relayImage.convertImage(
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName & "-thumb.jpg"
									);
								application.com.relayImage.convertImage(
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName2 & "-thumb.jpg"
									);

								standardSize = application.com.relayImage.resizeImage(
									targetWidth=request.standardSizeWidth,
									targetHeight=request.standardSizeHeight,
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName & ".jpg"
									);
								thumbSize = application.com.relayImage.resizeImage(
									targetWidth=request.thumbSizeWidth,
									targetHeight=request.thumbSizeHeight,
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage,
									targetName=targetImageName & "-thumb.jpg"
									);
								thumbSquareSize = application.com.relayImage.resizeImage(
									targetWidth=request.thumbSquareSizeHeightWidth,
									targetHeight=request.thumbSquareSizeHeightWidth,
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage2,
									targetName=targetImageName2 & "-thumb." & request.thumbImageType
									);
								squareSize = application.com.relayImage.resizeImage(
									targetWidth=request.squareSizeHeightWidth,
									targetHeight=request.squareSizeHeightWidth,
									sourcePath=cffile.serverdirectory,
									sourceName=sourceImage2,
									targetName=targetImageName2 & "." & request.thumbImageType
									);
	
							</cfscript>
						
							<!--- remove non jpg source image --->
							<cfif cffile.serverFileExt neq "jpg">
								<!--- <cffile   
								 	action = "delete"   
									file = "#cffile.serverdirectory#\#sourceImage#">
								<cffile   
								 	action = "delete"   
									file = "#cffile.serverdirectory#\#sourceImage2#"> --->
								<cfset deleteFile = application.com.fileManager.deleteFile("#cffile.serverdirectory#\#sourceImage#")>
								<cfset deleteFile = application.com.fileManager.deleteFile("#cffile.serverdirectory#\#sourceImage2#")>
							</cfif>
							
							<cfcatch>
								<!--- 2014-10-16	RPW	CORE-868 Elements - Problem creating thumbnail --->
								<cfset recordUpdated = false>
								<cfset argumentsForNextPage.message = "Problem creating thumbnail.">	
							</cfcatch>
						</cftry>
					<cfelseif left(fieldName,15) is "deletelinkImage">
						<cfset destination=application.paths.content & "\ElementLinkImages">						
						<!--- 2012/03/16	IH	Case 424464 delete all images --->								
						<cfdirectory action="list" name="qList" directory="#destination#" filter="*-#form.recordID#-*|*-#form.recordID#.*">									
						
						<cfloop query="qList">												
							<cftry>													
								<cflock name="deleteLinkImage-#name#" timeout="5">
									<cfset application.com.fileManager.deleteFile("#directory#/#name#")>
								</cflock>
								<cfcatch>
									<!--- 2014-10-16	RPW	CORE-868 Elements - Problem creating thumbnail --->
									<cfset recordUpdated = false>
									<cf_mail to="errors@foundation-network.com,relayhelp@foundation-network.com" 
								        from="relayhelp@foundation-network.com"
								        subject="Error deleting link image"
										type = "HTML">
										<cfdump var="#cfcatch#" label="cfcatch">
									</cf_mail>
								</cfcatch>
							</cftry>
						</cfloop>							
  					</cfif>
				</cfloop>
			</cfif>

			<cfif recordUpdated>
				<cfset argumentsForNextPage.message = "Record updated.">	
			</cfif>

		</cfif>
		
		<cfset frmWizardPage = #form.frmWizardPage#>
		<!--- end of updating record --->

		<!--- should only need to blow the cache if certain items in the element table have changed, 
			not if translations have been updated--->
		<cfif treeDirty is 1 or UGRecordManager_UpdatesDone or CountryScope_UpdatesDone>
			<cfset application.com.relayElementTree.cachedContentNeedsToBeBlown()>
			<!--- 			
			<cfset application.com.relayElementTree.incrementCachedContentVersionNumber()>
			<cfset message = message & " Cache updated.">  <!--- actually just blown, but will be updated! --->
			 --->		
 		</cfif>

		
	<CFELSE>
		<!--- this is adding a new record --->		
		<CFTRANSACTION>
			<cfset Form.FieldList = listappend(Form.FieldList,"createdby,lastupdatedby,created,lastupdated")>
			<cfset form.createdby = request.relaycurrentuser.usergroupid>	
			<cfset form.lastupdatedby = form.createdby >
			<cfset form.created = CreateODBCDateTime(now())>
			<cfset form.lastupdated = form.created>

			<!--- 
			NJH 2012/03/09 use insertEntity instead.. same idea, but more control and it updates nvarchar fields..
			<CFINSERT dataSource="#application.SiteDatasource#" tableName="Element" formFields="#Form.FieldList#"> --->
			
			<cfset insertResult = application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID["element"],entityDetails=form,insertIfExists=false)>
			
			<!--- <CFQUERY name="GetNewRecord" datasource="#application.SiteDatasource#" maxRows=1>
				SELECT Element.id AS RecordID
				FROM Element where abs(datediff (ss, created ,#form.created#)) < 1
				ORDER BY Element.id DESC
			</CFQUERY> --->
			<CFSET RecordID=insertResult.entityId>
			<CFSET argumentsForNextPage.RecordID=RecordID>
		</CFTRANSACTION>

		<CFSET "form.#form.recordID#" = RecordID>  <!--- eg newRecord = 1234  - used by these tags below --->
		<cfset argumentsForNextPage.reloadTreeForID		= form.parentid>

			<cfset application.com.relayTranslations.ProcessPhraseUpdateForm()>

		<CFIF structKeyExists(form,"frmUpdateRecordRights") and form.frmUpdateRecordRights eq "yes">
			<CF_UGRecordManagerTask>  
		</CFIF>
		<CFIF structKeyExists(form,"frmUpdateCountryScope") and form.frmUpdateCountryScope eq "yes">
			<CF_CountryScopeTask>  
		</CFIF>
		<CFIF structKeyExists(form,"frmAddFlags") and form.frmAddFlags eq "yes">
			<cfinclude template="/flags/flagTask.cfm">
		</CFIF>
		<cfset frmWizardPage = form.frmWizardPage>	
		
		<!--- end of adding record --->
		<cfset application.com.relayElementTree.cachedContentNeedsToBeBlown()>
		<cfset argumentsForNextPage.message = "Record added.">
		
		
	</CFIF>

	<!--- NJH 2012/01/17 - Social Media - add an activity when adding a new content page
		NJH 2012/10/25 - Social CR - check if content sharing is on and if this particular page is to be shared
	  --->
	<cfif application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.element.added") and structKeyExists(form,"statusID") and structKeyExists(form,"statusID_orig") and form.statusID neq form.statusID_orig and structKeyExists(form,"share") and form.share>
		<!--- if it's become live, then add it to the feed --->
		<cfif form.statusID eq 4>
			<cfset shareMergeStruct = {headline="phr_headline_element_#recordID#"}>
			<cfset shareArgs = structNew()>
			<cfif structKeyExists(form,"date") and form.date neq "">
				<cfset shareArgs.created = form.date>
			</cfif>
			<cfset application.com.service.addRelayActivity(action="added",performedOnEntityID=recordID,performedOnEntityTypeID=application.entityTypeID["element"],mergeStruct=shareMergeStruct,performedByEntityID=application.com.settings.getSetting("theClient.clientOrganisationID"),performedByEntityTypeID=2,argumentCollection=shareArgs)>
		</cfif>
	</cfif>

	<!--- don't redirect if doing a ajax form submit--->
	<cfif not isdefined("ajaxSubmit") >
			<CFLOCATION url="#thisdir#/elementEdit.cfm?#application.com.structureFunctions.convertStructureToNameValuePairs(argumentsForNextPage,'&')#" addtoken=false> 
	</cfif>

	<CF_ABORT>

<!--- ==============================================================================
     frmTask = copy
=============================================================================== --->
<CFELSEIF frmTask is "copy">
	<CFPARAM name="frmCopyCountryScope" default="0">
	<CFPARAM name="frmCopyRecordRight" default="0">
	<CFPARAM name="frmCopyTranslations" default="0">

	
	<!--- get new sortOrder --->
	<CFQUERY NAME="getSortOrder" DATASOURCE="#application.SiteDataSource#">			
		select max(sortOrder)+1 as SortOrder
		from element
		where parentid =  <cf_queryparam value="#frmCopyParentID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>


	<CFSET newSortOrder = getSortOrder.sortOrder>
	<CFIF newSortOrder is "">
		<CFSET newSortOrder = 1>
	</cfif>	

	<CFLOOP index="thisRecord"  list="#recordID#">
	
		<CFSET updateTime = now()>
		
		<CFTRANSACTION>
	
		<CFQUERY NAME="InsertNewRecord" DATASOURCE="#application.SiteDataSource#">		
			insert into element
				(ParentID,
				SortOrder,
				hasCountryScope,
				hasRecordRightsBitMask,	
				elementTypeID,
				headline,
				Summary,
				showSummary,
				LongSummary,
				showLongSummary,
				Detail,
				Date,
				Image,
				IsLive,
				stylesheets,
				isExternalFile,
				URL,
				ExpiresDate,
				Keywords,
				includeScreen,
				SummaryInContents,
				rate,
				parameters,
				createdby,
				created,
				lastUpdatedby,
				lastUpdated
				)
			select 
				<cf_queryparam value="#frmCopyParentID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#newSortOrder#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<CFIF frmCopyCountryScope is 0>
				0,
				<CFELSE>
				hasCountryScope,
				</cfif>
				<CFIF frmCopyRecordRight is 0>				
				0,
				<CFELSE>
				hasRecordRightsBitMask,
				</cfif>				
				
				elementTypeID,
				<CFIF frmCopyHeadline is not "">
					<cf_queryparam value="#frmCopyHeadline#" CFSQLTYPE="CF_SQL_VARCHAR" >  ,
				<CFELSE>
					'Copy of' + headline,
				</cfif>
				Summary,
				showSummary,
				LongSummary,
				showLongSummary,
				Detail,
				Date,
				Image,
				IsLive,
				stylesheets,
				isExternalFile,
				ltrim(URL),
				ExpiresDate,
				Keywords,
				includeScreen,
				SummaryInContents,
				rate,
				parameters,
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 

			FROM
				element
			where 	
				id =  <cf_queryparam value="#thisRecord#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>


		<CFQUERY NAME="getNewRecord" DATASOURCE="#application.SiteDataSource#">		
			select 
				max(id) as id
			from
				element
			where	
				parentID =  <cf_queryparam value="#frmCopyParentID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and created =  <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFQUERY>		
				
		

		<CFIF frmCopyCountryScope is 1>
				<CFQUERY NAME="copyCountryScope" DATASOURCE="#application.SiteDataSource#">					
					insert into countryScope		
						(entity,entityID,CountryID)
					select
						entity,
						<cf_queryparam value="#getNewRecord.id#" CFSQLTYPE="CF_SQL_INTEGER" >	,
						countryID
					from
						countryScope
					where entityID =  <cf_queryparam value="#thisRecord#" CFSQLTYPE="CF_SQL_INTEGER" > 	
						and entity = 'element'
				</CFQUERY>					
		</CFIF>	
	
		<CFIF frmCopyRecordRight is 1>
				<CFQUERY NAME="copyRecordRights" DATASOURCE="#application.SiteDataSource#">					
					insert into recordRights	
						(entity,recordID,usergroupid, permission)
					select
						entity,
						<cf_queryparam value="#getNewRecord.id#" CFSQLTYPE="CF_SQL_INTEGER" >	,
						userGroupID,
						permission
					from
						recordRights
					where recordID =  <cf_queryparam value="#thisRecord#" CFSQLTYPE="CF_SQL_INTEGER" > 	
					and 	entity = 'element'
				</CFQUERY>					
		</CFIF>	
	
		<CFIF frmCopyTranslations is 1>
				<CFQUERY NAME="getTranslations" DATASOURCE="#application.SiteDataSource#">									
					select
						phraseID
					from
						phraseList
					where entityID =  <cf_queryparam value="#thisRecord#" CFSQLTYPE="CF_SQL_INTEGER" > 	
					and entityTypeID = 10  <!--- hack for element --->
				
				</CFQUERY>

			<CFLOOP query="getTranslations">
				<CFQUERY NAME="copyTranslations" DATASOURCE="#application.SiteDataSource#">					
					insert into phraseList
						(phraseID, phraseTextID, entityTypeID, entityID,createdby, created )
				
				
					select
						(select max(phraseID)+1 from phraseList),
						phraseTextID,
						entityTypeID,
						<cf_queryparam value="#getNewRecord.id#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
					from
						phraseList
					where phraseID =  <cf_queryparam value="#getTranslations.phraseID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>					

				<CFQUERY NAME="copyTranslations" DATASOURCE="#application.SiteDataSource#">					
					insert into phrases
						(phraseID, phraseText, languageid, lastUpdatedby,
				lastUpdated, createdby, created)
					select
						phraseListNew.phraseID,
						phraseText,
						languageID,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					from
						phraseList as phraseListNew, phraseList, phrases
					where phraseList.phraseID =  <cf_queryparam value="#getTranslations.phraseID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
						and phrases.phraseID = phraseList.phraseID
						and phraseListNew.entityID =  <cf_queryparam value="#getNewRecord.id#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and phraseListNew.entityTypeID = 10						
						and phraseListNew.phraseTextID = phraseList.phraseTextID 
				</CFQUERY>					
			</cfloop>

			<cfset application.com.relayElementTree.cachedContentNeedsToBeBlown()>
		<!--- 	<cfset application.com.relayElementTree.incrementCachedContentVersionNumber()> --->
			
<!--- 			<cflock timeout="5" throwontimeout="No" name="processActionsLock" type="EXCLUSIVE">
				<cfset application.cachedContentVersionNumber = IncrementValue( application.cachedContentVersionNumber ) />
			</cflock>
 --->
		</CFIF>	
		</cftransaction>	
	</cfloop>
	
</CFIF>

<cfsetting enablecfoutputonly="no">
