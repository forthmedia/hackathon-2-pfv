<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			getClientXMLdata.cfm	
Author:				NJH
Date started:		2006-10-22
	
Purpose:	Reads through a list of xml files containing element content with the content 
			being in the country/language specified by the user. It creates a new 
			translation for the content.
			
Usage: 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->




<cf_head>
	<cf_title>Client XML Data</cf_title>
</cf_head>



<cf_translate>

<cfparam name="title" type="string" default="">
<cfparam name="countryID" type="string" default="9">
<cfparam name="languageID" type="string" default="1">
<cfparam name="request.clientXMLdataDir" type="string" default="#application.paths.content#\clientXMLdata\">
<cfparam name="eid" type="numeric" default=0>

<cfif title eq "">
	<cfoutput>title must be passed as a parameter</cfoutput>
	<CF_ABORT>
<cfelse>
	<cfif not isDefined("frmTitle")>
		<cfset frmTitle = trim(title)>
	</cfif>
</cfif>

<cfif eid eq 0>
	<cfoutput>eid must be passed as a parameter</cfoutput>
	<CF_ABORT>
</cfif>

<!--- hardcoding the country ID to be the UK if Any Country is specified.--->
<cfif countryID eq 0>
	<cfset countryID = 37>
</cfif>

<cfset allLanguages = application.com.commonQueries.getLanguages()>
<cfset allCountries = application.com.commonQueries.getCountries()>

<cfif isDefined("frmSearchXML") or isDefined("frmContentMatchSubmit")>
	<cfset countryID = frmCountry>
	<cfset languageID = frmLanguage>
</cfif>

<cfset countryISO = application.com.commonQueries.getISOCodeFromID("country",countryID)>
<cfset languageISO = application.com.commonQueries.getISOCodeFromID("language",languageID)> 

<!--- if a match has been selected --->
<cfif isDefined("frmContentMatchSubmit")>
	<cfquery name="getMatch" dbtype="query">
		select techTitle,html,compositeID,launchDate,archiveDate,expireDate from session.possibleMatches where 
		compositeID = #frmContentMatch#
	</cfquery>
	
	<cfif getMatch.recordCount eq 1>
		<cfset contentHeadline = getMatch.techTitle>
		<cfset contentHTML = getMatch.html>
		<cfset launchDate = getMatch.launchDate>
		<cfset archiveDate = getMatch.archiveDate>
		<cfset expireDate = getMatch.expireDate>
		
		<cfset tagsArray = application.com.regExp.findHTMLTagsInString(contentHTML,"img")>
	
		<cfset savedImagesList = "">
		<cfloop index="idx" from="1" to="#arrayLen(tagsArray)#">
			<cfif structKeyExists(tagsArray[idx],"attributes")>
				<cfset x = structFindKey(tagsArray[idx].attributes,"src")>
				<cfset origImageSrc = x[1].value>
				<cfset imageSrc = origImageSrc>
				
				<cfset fileDetails = application.com.regExp.getFileDetailsFromPath(imageSrc)>
				<cfset localFilePath = "content\miscImages\">
				<cfset localUrlFilePath = "content/miscImages/">
		
				<cfif not listfind(savedImagesList,fileDetails.fullFileName)>
					<!--- This images grabber will overwrite images if it finds an image with the same name
						We really should get a list of files in the directory. If the name exists, change the name --->
					<cfhttp url="#imageSrc#" file="#fileDetails.fullFileName#" path="#application.userfilesabsolutepath##localFilePath#" method="get" timeout="3"></cfhttp>
					<cfset savedImagesList = listAppend(savedImagesList,fileDetails.fullFileName)>
				</cfif>
				<!--- replace the old image path with the new path --->
				<cfset contentHTML = replaceNoCase(contentHTML,origImageSrc, localUrlFilePath&fileDetails.fullFileName)>
				
			</cfif>
		</cfloop>
		
		<!--- 
			updating the current element with the new content 
			We're fooling relaytranslations.ProcessPhraseUpdateForm()  into thinking that it is processing a submitted form
		--->
		<!--- All Countries --->
		<cfif countryID eq 37>
			<cfset countryID = 0>
		</cfif>
		
		<cfparam name="form.fieldnames" default = "">   <!--- need to add the field to the fieldnames variable to fool the cfc into thinking that it came from a form --->
		<cfset headline = "phrupd_headline_10_#eid#_#languageID#_#countryID#">   <!--- the 10 is the entityTypeID of elements --->
		<cfset "form.#headline#" = contentHeadline>
		<cfset form.fieldnames = listAppend(form.fieldnames,headline)>
		<cfset detail = "phrupd_detail_10_#eid#_#languageID#_#countryID#">   <!--- the 10 is the entityTypeID of elements --->
		<cfset "form.#detail#" = contentHTML>
		<cfset form.fieldnames = listAppend(form.fieldnames,detail)>
 
		<cfif application.com.relayTranslations.ProcessPhraseUpdateForm() >

			<cfif launchDate neq "" and dateDiff("d",dateFormat(launchDate),now()) gt 1>
				<cfset isLive = 1>
			<cfelse>
				<cfset isLive = 0>
			</cfif>
			
			<cfquery name="insertClientXMLdata" datasource="#application.siteDatasource#">
				update element set <!--- headline='#contentHeadline#', --->
				<cfif expireDate neq "">expiresDate='#expireDate#',</cfif> 
				isLive =  <cf_queryparam value="#isLive#" CFSQLTYPE="CF_SQL_bit" > 
				where id =  <cf_queryparam value="#eid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			
			<cfset message= "'<b>" & contentHeadline & "</b>' has successfully been added as a translation for language " & languageISO>
			<cfif countryID eq 0>
				<cfset message = message & " for all countries.">
			<cfelse>
				<cfset message = message & ", country " & countryISO & ".">
			</cfif> 
		</cfif>
		
	</cfif>
	
	<cfscript>
		structDelete(session,"possibleMatches");
	</cfscript>
	<SCRIPT type="text/javascript">
			if(!opener.closed){
				opener.location.href=opener.location.href;
			}
			self.close();
	</script>
		
</cfif>

<cfif listContains("0,37",countryID)>
	<cfset countryLanguageDir = "">
	<cfloop query="allCountries">
		<cfset countryISO = application.com.commonQueries.getISOCodeFromID("country",countryID)>
		<cfset countryLanguageDir = listAppend(countryLanguageDir,countryISO & "_" & languageISO)>
	</cfloop>
<cfelse>
	<cfset countryLanguageDir = countryISO & "_" & languageISO>
</cfif>

<!--- <cfset clientXMLdataDir = request.clientXMLdataDir & countryLanguageDir> --->

<cfset request.relayFormDisplayStyle = "HTML-table">

<cfform  name="xmlContent" method="POST">
	<cf_relayFormDisplay>
		<cfif isDefined("message")>
			<cf_relayFormElementDisplay relayFormElementType="MESSAGE" fieldName="" currentValue="#message#" label="">
		</cfif>
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="" currentValue="" label="phr_content_title">
			<cf_relayFormElement relayFormElementType="Text" fieldName="frmTitle" currentValue="#frmTitle#" label="phr_content_title">
			<cf_relayFormElement relayFormElementType="submit" fieldName="frmSearchXML" currentValue="phr_content_searchXML" label="">
		</cf_relayFormElementDisplay>
		
		<!--- <cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="frmCountry" currentValue="#countryISO#" label="phr_content_country">
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="frmLanguage" currentValue="#languageISO#" label="phr_content_language">
 --->
 		<cf_relayFormElementDisplay relayFormElementType="select" fieldName="frmCountry" currentValue="#countryID#" label="phr_content_country" query="#allCountries#" display="Country" value="countryID">
		<cf_relayFormElementDisplay relayFormElementType="select" fieldName="frmLanguage" currentValue="#languageID#" label="phr_content_language" query="#allLanguages#" display="Language" value="languageID">

	</cf_relayFormDisplay>

	<cfif isDefined("frmSearchXML")>
		<!--- loop through the directories --->
		<cfset inFirstDirectory = true>
		
		<cfloop list="#countryLanguageDir#" index="clientDirectory">
			<cfset clientXMLdataDir = request.clientXMLdataDir & clientDirectory>
			<cfif DirectoryExists(clientXMLdataDir)>
				<cfdirectory action="list" directory="#clientXMLdataDir#" filter="*.xml" name="clientXMLdataFiles">
				
				<!--- loop through the XML files --->
				<cfif clientXMLdataFiles.recordCount gt 0>
					
					<cfloop query="clientXMLdataFiles">
						<cftry>
							<cffile action="read" file="#directory#\#name#" charset="utf-8" variable="clientXMLDataFile">
						<cfcatch type="Any">XML file read error</cfcatch>
						</cftry>
						
						<cfset myxmldoc = XmlParse(clientXMLDataFile)>
						<cfset objectsArray = XmlSearch(myxmldoc,"/objects/object[contains(@objectlanguage,'#lcase(LanguageISO)#')]/")>
						
						
						<!--- if we have objects in our array --->		
						<cfset arraySize = ArrayLen(objectsArray)>
						<cfif arraySize gt 0>
							<cfset objectQuery = QueryNew("TechTitle,HTML,CompositeID,LaunchDate,ArchiveDate,ExpireDate,Directory")>
							<cfset temp = QueryAddRow(objectQuery,arraySize)>
							<cfloop from="1" to="#arraySize#" index="objectIdx">
								
								<cfset clientTitle = "">
								<!--- NJH 2006-11-14 Check first if title exists and if it contains the phrase we're looking for --->
								<cftry>
									<cfif findNoCase(frmTitle,objectsArray[objectIdx].title.xmlText)>
										<cfset clientTitle = objectsArray[objectIdx].title.xmlText>
									</cfif>
									<cfcatch>
										<!--- if it doesn't exist, try techtitle as it always exists --->
										<cfif findNoCase(frmTitle,objectsArray[objectIdx].techTitle.xmlText)>
											<cfset clientTitle = objectsArray[objectIdx].techTitle.xmlText>
										</cfif>
									</cfcatch>
								</cftry>
								
								<!--- if the title did exist, but didn't match the phrase passed in, check the techtitle --->
								<cfif clientTitle eq "">
									<cfif findNoCase(frmTitle,objectsArray[objectIdx].techTitle.xmlText)>
										<cfset clientTitle = objectsArray[objectIdx].techTitle.xmlText>
									</cfif>
								</cfif>
								
								<!--- if we've found the title in the client XML, add it to the array --->
								<cfif clientTitle neq "">
									<cfset HTML = ReplaceNoCase(objectsArray[objectIdx].HTML.xmlText,"src=""/vgn/","src=""http://images.lexmark.com/vgn/","ALL")>
									<!--- <cfset HTML = ReplaceNoCase(objectsArray[objectIdx].HTML.xmlText,"�","","ALL")> --->
																	
									<cfset temp = QuerySetCell(objectQuery,"TechTitle",clientTitle,objectIdx)>
									<cfset temp = QuerySetCell(objectQuery,"HTML",HTML,objectIdx)>
									<cfset temp = QuerySetCell(objectQuery,"CompositeID",objectsArray[objectIdx].Composite.CompositeID.xmlText,objectIdx)>
									<cfset temp = QuerySetCell(objectQuery,"LaunchDate",objectsArray[objectIdx].Scheduling.LaunchDate.xmlText,objectIdx)>
									<cfset temp = QuerySetCell(objectQuery,"ArchiveDate",objectsArray[objectIdx].Scheduling.ArchiveDate.xmlText,objectIdx)>
									<cfset temp = QuerySetCell(objectQuery,"ExpireDate",objectsArray[objectIdx].Scheduling.ExpireDate.xmlText,objectIdx)>
									<cfset temp = QuerySetCell(objectQuery,"Directory",clientDirectory,objectIdx)>
								</cfif>
							</cfloop>
							
							<cfquery name="combinedPossibleMatches" dbtype="query">
								select cast(techTitle as varchar) as techTitle, cast(html as varchar) as html,
									cast(compositeID as integer) as compositeID, cast(launchDate as varchar) as launchDate,
									cast(archiveDate as varchar) as archiveDate, cast(expireDate as varchar) as expireDate,
									cast(Directory as varchar) as Directory
								from objectQuery where techTitle is not null
								<!--- union the current results with results from the previous files--->
								<cfif (currentRow gt 1 and inFirstDirectory) or not inFirstDirectory>
								union
								select techTitle,html,compositeID,launchDate,archiveDate,expireDate,Directory from combinedPossibleMatches where techTitle is not null
								</cfif>
								order by Directory
							</cfquery>
							
							<cfset inFirstDirectory = false>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
		</cfloop>
				
		<cf_relayFormDisplay>
			<!--- we have matches to display --->
			<cfif isDefined("combinedPossibleMatches") and combinedPossibleMatches.recordCount gt 0>
				<!--- store the query in a session variable, so we can retreive the result after a form submission --->
				<cfset session.possibleMatches = combinedPossibleMatches>
				
				<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmContentMatchSubmit" label="" currentValue="phr_element_updateElementContent">
				
				<cfoutput query="combinedPossibleMatches" group="Directory">
					<tr><td colspan="2"><h1 style="font-size:20pt;color:red;">#htmleditformat(Directory)#</h1></td></tr>
					<tr><td colspan="2"><hr style="color:red;"></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<cfoutput>		
						<cf_querySim>
							contentMatch
							display,value
							&nbsp;|#htmleditformat(compositeID)#
						</cf_querySim>
						
						<tr>
							<td><h1>#htmleditformat(techTitle)#</h1>
								#html#
							</td>
							<cfif currentRow eq 1>
								<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmContentMatch" currentValue="#compositeID#" label="" query="#contentMatch#" display="display" value="value"></td>
							<cfelse>
								<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmContentMatch" currentValue="" label="" query="#contentMatch#" display="display" value="value"></td>
							</cfif>
						</tr>
						<tr><td colspan="2"><hr></td></tr>
						<tr><td colspan="2">&nbsp;</td></tr>
					</cfoutput>
				</cfoutput>
				
				<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmContentMatchSubmit" label="" currentValue="phr_element_updateElementContent">
			<cfelse>
				<cf_relayFormElementDisplay relayFormElementType="MESSAGE" currentValue="There were no results when searching the xml content titles for '#frmTitle#'" fieldname="" label="">
			</cfif>
		
		</cf_relayFormDisplay>
				
			<!--- <cfelse>
				<cfoutput>Client XML Directory '#clientXMLdataDir#' does not exist.</cfoutput> --->	
	</cfif>
</cfform>

</cf_translate>



