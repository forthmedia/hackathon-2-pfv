<!--- �Relayware. All Rights Reserved 2014 --->
<!---

element search results for more than one item
(one item gets a tree!)

We display previous results if a search hasn't been done on this page load and also if zero items are brought back

 --->

<cfif isDefined("foundElements") and foundElements.recordcount gt 1>
		<cfset session.elementTreeNav.foundElements = foundElements>
<cfelseif isDefined("foundElements") and foundElements.recordcount is 0>
	<cfoutput>No Pages Found <BR><BR>
		<cfif isDefined("session.elementTreeNav.foundElements")>Results of Previous Search: <BR></cfif>
	</cfoutput>

</cfif>
<cfif isDefined("session.elementTreeNav.foundElements")>
	<cfoutput>
 		<ul>
		<cfloop query="session.elementTreeNav.foundElements">
			<li>
				<a id = "sr-#id#-#parentid#" contextmenu = "element" href="javascript:void(openNewTab('element_#ID#','#replace(headline,"'","\'","ALL")# (#ID#)','/elements/elementEdit.cfm?RecordID=#ID#',{reuseTab:true,iconClass:'content',reuseTabJSFunction: function () {return true}}));" title="EID #id#" class="smalllink" contextmenu="page" onmouseover="javascript:fnSuppressMouseOver()" <!--- onClick = "javascript:fnLeftClickContextMenu()" ---> >#htmleditformat(headline)#</A>
			</li>
		</cfloop>
		</ul>
	</cfoutput>
</cfif>

		<!--- save this search in the session so that it will continue to appear --->

