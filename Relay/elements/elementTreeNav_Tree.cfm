<!--- �Relayware. All Rights Reserved 2014 --->
<!---

elementTreeNav_Tree


Mods WAB 2005-10-31 moved some code to a function relayelementtree.whichTreeIsThisNodeIn()
Mods WAB 2005-11-02	modified the "trimmed" tree to bring back more children generations
2013/01/03			YMA			2013 Roadmap Content Deletion
2015/12/09 	SB	Bootstrap
 --->



<!---
*********************
Tree Queries
*********************
 --->
		<cfparam name="frmDepth" default="2">  <!--- depth of initial tree to display --->
		<!--- getting a tree based on the filters --->

		<!--- decide what usergroupid to pass through to the getElementBranch SP --->
		<cfset tempUserGroupList = "-1">
		<cfif frmShowTreeForUserGroups contains "-1"><cfset tempUserGroupList  = -1><cfelseif listfind(frmShowTreeForUserGroups,0) is not 0><cfset tempUserGroupList  = 0><cfelseif frmShowTreeForUserGroups is not ""><cfset tempUserGroupList = frmShowTreeForUserGroups></cfif>

		<cfquery name="getElements" datasource="#application.sitedatasource#" >  <!--- caching removed now that we are doing small bits of tree at a time--->
		exec GetElementTree
				@ElementID =  <cf_queryparam value="#parentElementID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				@countryid = '#iif(frmShowTreeForCountryID is -2,de(request.relaycurrentuser.countrylist),de(frmShowTreeForCountryID))#',
				@statusid =  <cf_queryparam value="#listfirst(frmShowTreeStatusID,"|")#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				@Depth=2,
				@isLoggedIn =  <cf_queryparam value="#frmShowTreeForLoggedInPerson#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				@usergrouplist =  <cf_queryparam value="#tempUserGroupList#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				@date = null,
				@returnEditRightsForPersonID = #request.relayCurrentUser.personid#,
				@returnViewRightsLists = 1
		</cfquery>
		<cfset getTreeData = getElements>


<!---
*********************
Filter Form
*********************
 --->

<cfset noteText = "Changing a filter changes how subsequent sections of the tree are loaded.#chr(13)##chr(10)#
It does not change items already displayed.
Right click and select Load/Reload Section to refresh a section. Any additional pages will be added (no pages will be removed).
Click Reload to load tree from scratch with these filters.
">


<cfoutput>
	<div class="row" id="searchElementTreeNav">
		<div class="col-xs-12">
			<label>Filters <span title="#NoteText#" class="font-color-primary fa fa-question-circle"></label>
		</div>
		<!---  This form is used by dtree to load extra nodes--->
		<form name="d_Ajax" id="d_Ajax"  action="/webservices/elementTreeNav.cfm?method=addItems" method="GET">
			<!--- <input type="hidden" name="parentElementID" value="#parentElementID#"> --->
			<div class="form-group">
				<div class="col-xs-12 col-sm-5">
					<cf_displayValidValues formFieldName = "frmShowTreeForCountryid" validValues="#getCountries#" shownull = false currentvalue = "#frmShowTreeForCountryID#"  listsize = 1 >
				</div>
				<!--- YMA 2012-12-24	2013 Roadmap Content Deletion--->
				<div class="col-xs-12 col-sm-5">
					<cf_displayValidValues formFieldName = "frmShowTreeStatusID" validValues="0#application.delim1#All Draft and Live Statuses,4#application.delim1#Just Live Status,-1#application.delim1#All Statuses(including retired)" shownull = false currentvalue = "#frmShowTreeStatusID#" >
				</div>
				<div class="col-xs-12 col-sm-2">
					<input type="button" onClick="javascript:reloadTree(this)" value="Search" class="btn btn-primary">
				</div>
			</div>
		</form>
		<script>
			function reloadTree(obj) {
				obj.form.action = ""
				obj.form.submit()
			}
		</script>
	</div>
</cfoutput>



<!--- 	<cfoutput>
	<FORM NAME="filterForm" ACTION="ElementTreeNav.cfm" METHOD="GET">

		<table>




<!--- WAb 2005-09-26 put some open/close divs around this section, bit messy because of table layout --->
	<cf_divOpenClose
		divID = "extrafilters1"
		alsoToggleDivIDs = "extrafilters2,extrafilters3"
		startOpen = false
		rememberSettings = false
		openText="More Filters"
		closeText="Hide Filters"
	>

			<tr  id="extrafilters1" style="#divStyle#">
				<td valign="top">
					As seen by:
				</td>
				<td>
					<cf_displayValidValues formFieldName = "frmShowTreeForUserGroups" validValues="#getusergroups#" shownull = false currentvalue = "#frmShowTreeForusergroups#"  listsize = 4 displayas="multiselect">
				</td>
			</tr>
			<tr name="extrafilters2" id="extrafilters2" style="#divStyle#">
				<td >
					Login Status
				</td>
				<td>
						<cf_displayValidValues formFieldName = "frmShowTreeForLoggedInPerson" validValues="0#application.delim1#Logged Out,1#application.delim1#Logged In,-1#application.delim1#Any" shownull = false currentvalue = "#frmShowTreeForLoggedInPerson#" >
				</td>
			</tr>
			<tr id="extrafilters3" style="#divStyle#">
				<td >Page Status
				</td>
				<td>
					<cf_displayValidValues formFieldName = "frmShowTreeStatusID" validValues="#getStatii#" shownull = false currentvalue = "#frmShowTreeStatusID#" >
				</td>
			</tr>

			<tr >
				<td >
					<cf_divOpenClose_Image><cf_divOpenClose_Text/>
				</td>
				<td>
				</td>
			</tr>

			<tr >
				<td >
<!--- 					<cf_divOpenClose_Image><cf_divOpenClose_Text/> --->
				</td>
				<td align="right">
					<input type="submit" value="Go">
<!--- 					<a href="javascript:document.filterForm.submit();">Reload Site Tree</A> --->
				</td>
			</tr>

			<cfif isDefined("getTreeData") and getTreeData.recordCount is Not 0>
			<tr id="refreshTreeDiv" style="display:none">
				<td >

				</td>
				<td>
					<font color="red">Items in the tree may have been updated</font><BR> <a id="refreshTreeAnchor" HREF="">Reload Now</a>
				</td>
			</tr>
			</cfif>
	 	<tr colspan="2" ><td></td></tr>


	</cf_divOpenClose >
		</table>
	</form>
	</cfoutput>

 --->

<!---
*********************
Tree Output
*********************
 --->

	<cfif isDefined("parentElementID")>
		<cfset rootElementID = ParentElementID>
	<cfelse>
		<cfset rootElementID = 0>
	</cfif>

	<cf_includeJavascriptOnce template = "/javascript/dtree.js">
<cfoutput>






<!--- 			<tr>
				<!--- <td><a href="javascript:d.openTo(document.filterForm.goTo.value, true);" class="smallLink">Go To</a></td> --->
				<td>Find Page by ID or name</td>
				<td>

 --->				<!---
				changed to a goto which searches the dtree array
				<input id="goTo" name="goTo" type="text" size="10" onchange="javascript:document.ThisForm.submit();">
				&nbsp;&nbsp;&nbsp;<a href="javascript:javascript:document.ThisForm.goTo.value='';document.ThisForm.submit();" title="Show all content" class="smallLink"   >Show All</a>
			--->

<!--- 	<FORM NAME="searchForm" ACTION="ElementTreeNav.cfm" METHOD="POST">
			<input id="goTo" name="goTo" type="text" size="6" onchange="javascript:searchByID()">
			<A Href="javascript:searchByID()">Go</A>

			</td>
			</tr>
			<tr><td colspan="2"><a href="elementTreeNav.cfm" class="smallLink">Show my recent edits</a></td></tr>

			<tr><td colspan=2><HR></td></tr>


		<input name="expandAll" type="hidden" value="0">
		<input name="collapseAll" type="hidden" value="0">
		</FORM> --->



</cfoutput>

	<cf_includeJavascriptOnce template = "/javascript/ext/adapter/ext/ext-base.js">
	<cf_includeJavascriptOnce template = "/javascript/ext/ext-all.js">
	<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
	<cf_includeJavascriptOnce template = "/javascript/ext/docs/resources/TabCloseMenu.js">

	<cfif getTreeData.recordCount is 0>
		<cfoutput>
			<div class="row">
				<div class="col-xs-12">
					<h3>No record found</h3>
				</div>
			</div>
		</cfoutput>
	<cfelse>

		<cfoutput>

		<!---SB 2015/12/09 I hid this because it doesn't work and there is no point from a UX perspective to have this
		<a href="javascript:d.openAll()" title="Expand all nodes" class="smallLink"   >+ All</a>
		<a href="javascript:d.closeAll()" title="Collapse all nodes" class="smallLink" >- All</a> --->

		<div class="elementTreeNav row" id="treeMainContainer">
			<div class="col-xs-12">
				<script type="text/javascript">

					d = new dTree('d');
					d.config.inOrder=false;
					d.config.useCookies=false;

					</cfoutput>


					<cfoutput>#application.com.relayElementTree.generateDTreeAddFunctions(elementTree =getTreeData,firstNodeIsRoot = true, dtreeObjectName = "d")#</cfoutput>

				<cfoutput>
		<!--- 			<cfif form.collapseAll eq 1>
						d.closeAll();
					</cfif>
		 --->
					document.write(d);
					<cfif isDefined("foundElements")>
		//				d.openAll();
						d.openTo(<cfoutput>#jsStringFormat(foundElements.id)#</cfoutput>,true,false)
					</cfif>

					<cfif isDefined("openTreeTo")>
						d.openToIfExists(<cfoutput>#jsStringFormat(openTreeTo)#</cfoutput>,true,false)
					</cfif>

				</script>
			</div>
		</div>
		</cfoutput>
	</cfif>

<!--- 	<cfoutput></table></cfoutput>
 --->
<!--- end of tree --->