<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

archiveTree

2013/01/03			YMA			2013 Roadmap Content Deletion

 --->

<cfset tempUserGroupList = "-1">
<cfif frmShowTreeForUserGroups contains "-1"><cfset tempUserGroupList  = -1><cfelseif listfind(frmShowTreeForUserGroups,0) is not 0><cfset tempUserGroupList  = 0><cfelseif frmShowTreeForUserGroups is not ""><cfset tempUserGroupList = frmShowTreeForUserGroups></cfif>
		
<cfquery name="GetTree" datasource="#application.sitedatasource#" >
	exec GetElementTree 
			@ElementID = <cf_queryparam value="#session.ElementTreeTopEID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
			@countryid = '-2',
			@statusid = 6 , 
			@Depth=999, 
			@isLoggedIn =  -1 , 
			@usergrouplist =  -1 , 
			@date = null,
			@returnEditRightsForPersonID = -1, 
			@returnViewRightsLists = 1
</cfquery>

<!--- NJH 2014/08/11 - partner pack work. Don't show archived elements that have a paramters like partnerPack, as we don't want users to see them and restore them --->
<cfquery dbtype="query" name="ArchiveTree">
	select * from GetTree where statusID = 6 and (parameters is null or lower(parameters) not like '%partnerpack%')
</cfquery>

<cfset getArchiveData = ArchiveTree>

<cfoutput>
	<Table>
</cfoutput>	
<cfif getArchiveData.recordCount is 0>
	<cfoutput>
		<tr><td >
			No records found
		</td></tr>
	</cfoutput>
<cfelse>
	<cfoutput query="getArchiveData">
		<tr>
			<td>
				<a id = "sr-#CONTENTID#-#ACTUALPARENTID#" title="#htmleditformat(headline)# EID: #CONTENTID# Right Click For Options" href="javascript:void(openNewTab('element_#CONTENTID#','#htmleditformat(headline)# (#htmleditformat(headline)#)','/elements/elementEdit.cfm?RecordID=#CONTENTID#',{iconClass:'content',reuseTab:true,reuseTabJSFunction: function () {return true}}))" title="EID #CONTENTID#" class="smalllink" contextmenu="archiveOptions" <!---onmouseover="javascript:fnSuppressMouseOver(event)--->">#htmleditformat(headline)#</A>
			</td>
		</tr>
	</cfoutput>
</cfif>	
<cfoutput>
	</TABLE>
</cfoutput>
