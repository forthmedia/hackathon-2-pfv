<!--- �Relayware. All Rights Reserved 2014 --->
<!--- javascript functions for navigating a newsletter --->
<CFPARAM name="window" default="window">
<!--- set window = "mainFrame" for including in the newsletter frame set--->
<!--- 2006/02/07 GCC changed to use  instead of just  which is no longer valid 
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

--->
<CFOUTPUT>
		 function showArticle(articleId) {
		//		Submit form and show a new article
				form = #htmleditformat(window)#.document.mainForm;
				form.frmArticleId.value = articleId;
				form.frmSectionId.value = '';
				form.submit();
				return			;
		} 


		function showSection(sectionId) {
		//		Submit form and show a new article
				form = #htmleditformat(window)#.document.mainForm;
				form.frmArticleId.value = '';
				form.frmSectionId.value = sectionId;
				form.submit();
				return  ;			
		}
		
	<CFIF editMode>

		function editElement (elementID)  {
			var now = new Date()
			now= now.getTime()
			newWindow = window.open ('/elements/elementEdit.cfm?recordid=' + elementID + '&amp;frmLanguage=1&x=' + now, 'ElementPopUp','width=850,height=650,location=0,resizable=1,scrollbars=1,status=1')
			newWindow.focus()
		}

		function newElement (elementTypeID,parentID)  {
			var now = new Date()
			now= now.getTime()
			newWindow = window.open ('/elements/elementNew.cfm?parentid='+ parentID + '&amp;x=' + now, 'ElementPopUp','width=850,height=650,location=0,resizable=1,scrollbars=1,status=1')
			newWindow.focus()
		}

		function viewElement (elementID) {
			testElementWindow  = window.open('/?eid='+elementID,'testElement','location=0,resizable=1,scrollbars=1,menu=0,top=50,left=50,height=500,width=600')
			testElementWindow.focus();	
			}

		function copyElement (elementID) {
			copyElementWindow  = window.open('/elements/copyElement.cfm?ElementID='+elementID,'copyElement','location=0,resizable=1,scrollbars=0,menu=0,top=50,left=50,height=300,width=500')
			copyElementWindow.focus();
			}


	</cfif>
		
		function externalLink (URL)  {
			form = #htmleditformat(window)#.document.mainForm
				elementid = 0
			if (form.frmArticleId.value != '') {
				elementid = form.frmArticleId.value
			} else if (form.frmSectionId.value != '') {
			
				elementid = form.frmSectionId.value
			}
			
				loc = '/newsletter/externalLink.cfm?entityType=element&entityid=' + elementid + '&personid=' + form.frmPersonId.value + '&URL=' + escape(URL)
			newWindow = window.open (loc, 'PopUp', 'location=0,resizable=1,scrollbars=1')
			// see comment below re focus
			// newWindow.focus()

		
		}

		function externalArticle (URL,elementid)  {

			form = #htmleditformat(window)#.document.mainForm;
		
			loc = '/newsletter/externalLink.cfm?entityType=element&entityid=' + elementid + '&amp;personid=' + form.frmPersonId.value + '&amp;URL=' + escape(URL) +'&amp;SaveURL=false'
			newWindow = window.open (loc, 'PopUp', 'resizable,scrollbars,status')
			// due to problems with setting the focus on a window in a different domain, we can't set the focus here (IE4)
			// therefore the focus is now dealt with by externalArticle.cfm ina round about sort of way
			// newWindow.focus()		
		
		}

		function internalLink (URL)  {

			form = #htmleditformat(window)#.document.mainForm
				elementid = 0
			if (form.frmArticleId.value != '') {
				elementid = form.frmArticleId.value
			} else if (form.frmSectionId.value != '') {
			
				elementid = form.frmSectionId.value
			}



			newWindow = window.open (URL+'?entityType=element&entityid=' + elementid + '&amp;personid=' + form.frmPersonId.value, 'PopUp')
			 newWindow.focus()

		
		}



		function verifyForm () {
			if (verifyInput) {
				
				msg =  verifyInput ()
				
				if (msg != '')  {
					alert (msg)
					return false
				} else {
					return true
				}
				
			} else { 
			
				return true
			}			


		}
		

</CFOUTPUT>


