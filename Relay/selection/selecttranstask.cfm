<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:
		Permission:


Mods:

WAB 2005-05-17 altered code so that transferring a selection didn't make a complete new copy of the selection
WAB 2005-09-26 altered so that transferer doesn't end up with a share on a selection
WAB 2007-10-23 change copy selections selectedCount added to query
WAB 2009/10/?   Added copying of criteria1
---->


<!--- only those locations that are active
		could also search for locations with active people --->

<!--- lists all Countries to which User has rights --->

<CFIF structKeyExists(FORM,"frmSelectionID")>
	<CFIF FORM.frmSelectionID IS NOT 0>
		<CFIF structKeyExists(FORM,"frmUserID")>
			<CFIF structKeyExists(FORM,"frmtransfertype")>
				<!--- frmtransfertype is a hidden variable and it should always exist
						but leave the conditional for security --->

					<!--- 2012-07-26 PPB P-SMA001 commented out
					<cfinclude template="/templates/qrygetcountries.cfm">
					--->

				<!---
					 copy and move are identical except move also deletes the user's
					selections
					WAB:2005-05-17
					Changed this because surely copy and transfer aren't the same!!
					Firstly you can only transfer ownership of a selection to a single person!
					Secondly you don't need to take a copy of it, infact the selectionid shouldn't change
					--->

				<CFSET freezetime = Now()>

				<!--- ============================================
						only people who get the selections are
						people who have rights in at least one of the countries
						covered by the selections.  If not, there is not point
						to adding a SelectionRecord since there will not be any
						SelectionTag records --->

				<CFQUERY NAME="checkAllUsers" datasource="#application.siteDataSource#">
					SELECT ug.UserGroupID, ug.name
					FROM UserGroup AS ug, RightsGroup AS rg, Rights AS r
					WHERE ug.PersonID = rg.PersonID
					  AND rg.UserGroupID = r.UserGroupID
					  AND rg.UserGroupID  IN ( <cf_queryparam value="#frmUserID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					  <!--- find all the selection's countries --->
					  AND EXISTS (Select 1 FROM SelectionTag AS st1,
					                            Person AS p1,
												Location AS l1
										  WHERE st1.EntityID = p1.PersonID
										    AND p1.LocationID = l1.LocationID
											AND l1.CountryID = r.CountryID
											<!--- 2012-07-26 PPB P-SMA001 commented out
											AND l1.CountryID IN (#request.RelayCurrentUser.CountryList#) <!--- there may be people who the current owner does not have access --->
											 --->
											#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p1").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
											AND st1.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >   <!--- original selection --->
					  				)

				</CFQUERY>

				<CFIF checkAllUsers.RecordCount IS NOT 0>


					<CFIF FORM.frmtransfertype is "move">
						<!--- Change the createdby field to the new owner
							put the new owner in the selectionGroupTable
						 --->
						<CFTRANSACTION>
							<CFQUERY NAME="CopySelections" datasource="#application.siteDataSource#">
							Update selection set createdby =  <cf_queryparam value="#frmUserID#" CFSQLTYPE="CF_SQL_INTEGER" >
							where selectionid =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
							</cfquery>

							<CFQUERY NAME="SelectionGroup" datasource="#application.siteDataSource#">
							INSERT INTO SelectionGroup (SelectionID, UserGroupID, CreatedBy, Created,
																LastUpdatedBy, LastUpdated)

							select <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmUserID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmUserID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
									   <cf_queryparam value="#frmUserID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
									where not exists (select 1 from selectiongroup where selectionid =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >  and usergroupid =  <cf_queryparam value="#frmUserID#" CFSQLTYPE="CF_SQL_INTEGER" > )
							</CFQUERY>

							<!---  WAB: 2005-09-26 delete current users record from the selectiongroup table
								this should (evidently) have been done when I made the changes 2005-05-17 although I don't actually agree!
							--->
							<CFQUERY NAME="deleteSelectionGroup" datasource="#application.siteDataSource#">
							delete from selectiongroup
							where selectionid =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
							and usergroupid = #request.relaycurrentuser.usergroupid#
							</cfquery>



						</CFTRANSACTION>

					<CFelseIF FORM.frmtransfertype is "copy">


						<CFSET authorised_users = ValueList(checkAllUsers.UserGroupID)>


						<!--- Original Creator will be in the last updated by field,
								obviously this will change when this is updated but at
								this point we know the person who was the original owner --->

						<CFTRANSACTION>

							<!--- WAB 2007-10-29 added selectedcounts to the insert --->
							<CFQUERY NAME="CopySelections" datasource="#application.siteDataSource#">
							INSERT INTO Selection (Title, RecordCount, Description, TableName,
													CreatedBy, Created, LastUpdatedBy, LastUpdated, selectedCount,selectedCountLoc,selectedCountOrg,refreshrate)
											SELECT s.Title, s.RecordCount, s.Description, s.TableName,
													ug.UserGroupID, s.Created, s.LastUpdatedBy, <cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, selectedCount,selectedCountLoc,selectedCountOrg,refreshrate
							 FROM Selection AS s, UserGroup AS ug
							WHERE s.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
							  AND ug.UserGroupID  IN ( <cf_queryparam value="#Variables.authorised_users#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

							</CFQUERY>

							<CFQUERY NAME="FindSelections" datasource="#application.siteDataSource#">
							SELECT s.SelectionID, s.CreatedBy
							FROM Selection AS s
							WHERE abs(dateDiff(s,s.LastUpdated,<cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP">)) <=1
							</CFQUERY>

							<CFSET count = 1>
							<CFIF FindSelections.RecordCount GT 0>
								<CFLOOP QUERY="FindSelections">
									<CFSET CopySelectags = "CopySelectags" & count>

									<CFQUERY NAME="findCountries#count#" datasource="#application.siteDataSource#">
										SELECT r.CountryID
										  FROM Rights AS r
										 WHERE r.UserGroupID = #CreatedBy#  <!--- the transferrees ID --->
										   AND r.Permission > 0
										   AND r.SecurityTypeID =
													(SELECT e.SecurityTypeID
													 FROM SecurityType AS e
													 WHERE e.ShortName = 'RecordTask')
										GROUP BY r.CountryID
									</CFQUERY>



									<!--- only transfer those SelectionTags to which the new user has rights
										 AND only transfer those SelectionTags to which the current user has rights

										 The user may have created a selection, then had some rights revoked.
										 We do not delete the selectionTags relating to the revoked rights (there
										 may have been a mistake) which means we need to be careful and
										 not transfer those SelectionTags (the revocation may have been correct)
									 --->

									<CFQUERY NAME=#CopySelectags# datasource="#application.siteDataSource#">
									INSERT INTO SelectionTag (SelectionID, EntityID, Status, CreatedBy, Created,
																	LastUpdatedBy, LastUpdated)
									SELECT <cf_queryparam value="#SelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >, st.EntityID, st.Status, ug.UserGroupID, st.Created,
										   st.LastUpdatedBy, <cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
									FROM UserGroup AS ug, SelectionTag AS st, Person AS p, Location AS l
									WHERE st.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									AND ug.UserGroupID =  <cf_queryparam value="#CreatedBy#" CFSQLTYPE="CF_SQL_INTEGER" >
									AND st.EntityID = p.PersonID
									AND p.LocationID = l.LocationID
									AND l.CountryID IN (#Evaluate("ValueList(findCountries#count#.CountryID)")#) <!--- the countries of the person getting the selections --->
									<!---
									AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  <!--- user's countries --->
									 --->
									#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
									</CFQUERY>

									<!--- insert sharers, but only those people
										  who have rights to at least one country of
										  the selection --->
									<CFQUERY NAME="OwnerSelGroup#count#" datasource="#application.siteDataSource#">
									INSERT INTO SelectionGroup (SelectionID, UserGroupID, CreatedBy, Created,
																	LastUpdatedBy, LastUpdated)

									VALUES (<cf_queryparam value="#SelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#CreatedBy#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#CreatedBy#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
										   <cf_queryparam value="#CreatedBy#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#freezetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
									</CFQUERY>

									<!--- note that at this point, based on permissions,
											there may not be any people

											No clean up is currently performed --->

								 	<CFSET CopySelectCriteria = "CopySelectCriteria" & count>

									<!--- WAB 2009-10-18 added criteria1 to the copy --->
									<CFQUERY NAME=#CopySelectCriteria# datasource="#application.siteDataSource#">
									INSERT INTO SelectionCriteria (SelectionID, SelectionField, Criteria, Criteria1,
																	CreatedBy, Created, LastUpdatedBy, LastUpdated)
									SELECT #SelectionID#, sc.SelectionField, sc.Criteria, sc.criteria1,
										   ug.UserGroupID, sc.Created, sc.LastUpdatedBy, #freezetime#
									FROM UserGroup AS ug, SelectionCriteria AS sc
									WHERE sc.SelectionID = #frmSelectionID#
									AND ug.UserGroupID = #CreatedBy#
									</CFQUERY>

									<CFSET count = count + 1>
								</CFLOOP>

								<CFQUERY NAME="CheckUsedSelections" datasource="#application.siteDataSource#">
								SELECT cs.*
								FROM CommSelection AS cs, Communication AS c
								WHERE cs.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
								  AND c.CommID = cs.CommID
								  AND c.Sent = 0 <!--- false --->
								  AND cs.CommSelectLID = 0
								</CFQUERY>

								<CFIF FORM.frmtransfertype IS "move" AND CheckUsedSelections.RecordCount IS 0>

									<CFQUERY NAME="DelSelectCriteria" datasource="#application.siteDataSource#">
									DELETE
									  FROM SelectionCriteria
									 WHERE SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									</CFQUERY>
									<CFQUERY NAME="DelSelectGroup" datasource="#application.siteDataSource#">
									DELETE
									  FROM SelectionGroup
									 WHERE SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									</CFQUERY>
									<CFQUERY NAME="DelSelectTags" datasource="#application.siteDataSource#">
									DELETE
									  FROM SelectionTag
									 WHERE SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									</CFQUERY>
									<CFQUERY NAME="DelSelection" datasource="#application.siteDataSource#">
									DELETE
									  FROM Selection
									 WHERE SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									</CFQUERY>
									<CFQUERY NAME="DelCommSelection" datasource="#application.siteDataSource#">
									DELETE
									  FROM CommSelection
									 WHERE SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									</CFQUERY>

								</CFIF>
							<CFELSE>
								<CFSET problem = "The selection chosen has been deleted or transferred since the information for it was downloaded.">
								<cfset messageType="warning">
							</CFIF>
						</CFTRANSACTION>

					</CFIF>  <!--- end of move/copy --->

				<CFELSE>
					<CFSET problem = "The copy/transfer could not take place since none of the users you selected have rights to the countries used in the selection.">
					<cfset messageType="warning">
				</CFIF>
			<CFELSE>
				<CFSET problem = "You have not selected a transfer type">
				<cfset messageType="info">
			</CFIF>
		<CFELSE>
			<CFSET problem = "You have not selected any users">
			<cfset messageType="info">
		</CFIF>
	<CFELSE>
		<CFSET problem = "You have not chosen a selection">
		<cfset messageType="info">
	</CFIF>
<CFELSE>
	<CFSET problem = "You have not chosen a selection">
	<cfset messageType="info">
</CFIF>

<!---
<CFINCLUDE TEMPLATE="selecttophead.cfm">
 --->

<CFIF structKeyExists(VARIABLES,"problem")>
	<cfset message=problem>
<CFELSE>
	<cfset messageType="success">
	<cfif FORM.frmtransfertype is "copy">
		<cfset message = "Your selection was copied successfully to #FindSelections.RecordCount# user#IIF(FindSelections.RecordCount GT 1, DE("s"), DE(""))#.">
	<cfelse>
		<cfset message = "Your selection was transferred to successfully to #checkAllUsers.Name#">
	</cfif>
</CFIF>
<cfset application.com.relayUI.setMessage(message=message,type=messageType)>