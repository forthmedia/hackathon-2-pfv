<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	SelectAlterResults.cfm  WAB 1999-02-11
		Code used to give results of additions/removals from a selection 
		
		2000-06-19  WAB bug for large additions - #counts.Peopletoupdate# is not numeric so can't be added to counts.PeopletoAdd
		2015-11-13    ACPK    Replaced tables with Bootstrap
		--->

<CFIF not isdefined("HeaderDone")>
   <div class="col-xs-12">
	   	<CFIF frmtask IS "remove">
	   		<B>REMOVE FROM SELECTION</B>
	   	</CFIF>
	   	<CFIF frmtask IS "add">
	   		<B>ADD TO SELECTION</B>
	   	</CFIF>
	   	<CFIF frmtask IS "save">
   		   <B>SAVE SELECTION</B>
      	</CFIF>
       </div>

	<div class="row">
		<div class="col-xs-1">&nbsp;</div>
		<div class="col-xs-8">People Checked</div>
		<div class="col-xs-1"><CFOUTPUT>#htmleditformat(counts.PeopleChecked)#  </CFOUTPUT></div>
	</div>
	<CFSET Headerdone="">
</cfif>

<div class="col-xs-12">
	<CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH="100%" HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT>
</div>										

<div class="col-xs-12">
	<B>Selection: 	<CFOUTPUT>#htmleditformat(getSelection.Title)#</CFOUTPUT> </b>
</div>

<CFIF frmtask IS "remove">	
	<div class="row">
		<div class="col-xs-1">&nbsp;</div>
		<div class="col-xs-8">Checked People Removed From Selection</div>
		<div class="col-xs-1"><CFOUTPUT>#htmleditformat(counts.Peopletoremove)#  </CFOUTPUT></div>
	</div>

	<div class="row">
		<div class="col-xs-1">&nbsp;</div>
		<div class="col-xs-8">Checked People not in Selection</div>
		<div class="col-xs-1"><CFOUTPUT>#evaluate(counts.Peoplenotinselection + counts.Peoplealreadyunchecked)#  </CFOUTPUT></div>
	</div>
</CFIF>

<CFIF frmtask IS "add">
	<div class="row">
		<div class="col-xs-1">&nbsp;</div>
		<div class="col-xs-8">Checked People already in Selection</div>
		<div class="col-xs-1"><CFOUTPUT>#htmleditformat(counts.Peoplenoaction)#  </CFOUTPUT></div>
	</div>

	<div class="row">
		<div class="col-xs-1">&nbsp;</div>
		<div class="col-xs-8">Checked People added to selection</div>
		<div class="col-xs-1">
			<cfif isNumeric(counts.PeopletoUpdate)>
			<CFOUTPUT>#evaluate(counts.Peopletoadd+counts.Peopletoupdate)#  </CFOUTPUT>			
			<cfelse>
				<CFOUTPUT>#htmleditformat(counts.Peopletoadd)#  <!--- for large queries, counts.Peopletoupdate will be 'Not calculated', so just output #counts.Peopletoadd#, won't include updated items, but shouldn't matter--->  </CFOUTPUT>			
			</cfif>
		</div>
	</div>

	<CFIF counts.peopleinactive IS NOT 0>
		<div class="row">
			<div class="col-xs-1">&nbsp;</div>
			<div class="col-xs-8">Inactive People</div>
			<div class="col-xs-1"><CFOUTPUT>#htmleditformat(counts.Peopleinactive)#  </CFOUTPUT></div>
		</div>
	</CFIF>
</CFIF>

<CFIF frmtask IS "save">
	<div class="row">
		<div class="col-xs-1">&nbsp;</div>
		<div class="col-xs-11"><CFOUTPUT><!--- #counts.PeopleChecked# People  --->Saved</CFOUTPUT>	</div>
	</div>
</CFIF>

