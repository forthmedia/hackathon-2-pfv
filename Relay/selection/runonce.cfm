<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
some queries to run to make sure that all the required database records/fields are present


 --->


<cfquery name="addSelectionSearchItems" datasource="#application.siteDataSource#">
if not exists (select * from selectionfield where selectionfield = 'frmExOrgSelection')
BEGIN
insert into selectionfield (selectionField, criteriamask)
values ('frmExOrgSelection', 'IN (FieldData)')
END

if not exists (select * from selectionfield where selectionfield = 'frmInOrgSelection')
BEGIN
insert into selectionfield (selectionField, criteriamask)
values ('frmInOrgSelection', 'IN (FieldData)')
END

</cfquery>


<!--- Add selectedCountLoc and  selectedCountOrg  --->
<cfquery name="alterselectionTable" datasource="#application.siteDataSource#">
if not exists (select * from information_schema.columns where TABLE_NAME = 'selection' and COLUMN_NAME = 'selectedCountLoc')
	BEGIN
		alter table selection add selectedCountLoc int 
	END

if not exists (select * from information_schema.columns where TABLE_NAME = 'selection' and COLUMN_NAME = 'selectedCountOrg')
	BEGIN
		alter table selection add selectedCountOrg int 
	END

</cfquery>	

