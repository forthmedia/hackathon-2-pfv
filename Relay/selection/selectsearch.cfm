<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes" requestTimeOut="180">
<!--- user rights to access:
		SecurityTask:
		Permission:
---->
<!--- Longest lable should have <NOBR> tags to stop it wrapping, currently: Location Last Updated By : --->

<!--- only those locations that are active
		could also search for locations with active people --->

<!--- lists all countries whether or active location records exist --->

<!--- mods:
WAB added personid and locationid fields and then increased the size of the fields to handle subqueries and long list of numbers
WAB 2000-04-20  added handling of "exclude/include locations contained in these selections"
WAB 2000-04-28  added handling of UK postal code prefixes
WAB 2000-07-06  added hack to allow search for blank postalCodes and email addresses.  Type 'null' in the search field
WAB 2000-09-20  added deletion of temporary criteria (DAM code is beginning to use existing criteria, but until complete we need to delete the old criteria)
SWJ	2001-07-25  added section to supress blank emails and organisation/location contact records
SWJ	2004-02-28  added DHTML to hide divs
WAB 2005-02-21 Add frmOrganisationID and frmExOrganisationID
WAB 2005-09-20  added (supports the use of % as a wildcard) comment next to orgname and sitename
WAB 2005-11-29  added support for nVarChar criteria, but needed to reduce max criteria length from 7950 to 3990

WAB 2005-12-05   added ability to filter flags displayed by recent use
WAB 2006-03-02  made radio on frmPersLastUpdatedWhen/frmlocationLastUpdatedwhen default to something (otherwise causes errors if date entered and radio not selected)
2006-05-19 SWJ moved general selection criteria into organisation and country where they belong so it's more logical
WAB 2007-02-24 	made sure that frmPersNameType defaults to something when a name entered  (new javascript)
AJC 2007-04-19	Added in Organisation type to the start of the process for filtering
AJC 2007-05-08	Added in blank value to force a user to select an option from the initial organisation type filter
 WAB 2007-06-25 added None Set option to language
MPR 2008-03-13 P-TND009 Partner Profile Report
NYF 2008-05-06 P-TND009 Partner Profile Report - added City
NJH 2008/09/19	Elearning 8.1 - add certifications and specialisations. Changed Solutions Areas and Course Series to a select box rather than radio buttons
NJH 2008-10-08	Bug Fix - Noticed a bug. The 'any' value for frmCountryID was *0, and the query was trying to find a location in (*0) and falling over
NJH 2009/02/13 	P-SNY047 - added quizzes to selections if the elearning module is turned on
NJH 2009/03/23	Support Request Issue 23 - added some text to explain that entityId text fields are limited to 3990 characters.
NYB 2009/03/23	Moved Elearning Addon to before Organisation Name and Person Name
WAB 2009/04/21  P-TND009 Partner Profile Report actually released into main code base
NJH 2009/06/25	Bug Fix Sony1 Support Issue 2214 - added frmOrganisationFrom and frmOrganisationTo to allow searching using the between functionality on org names
NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org
WAB 2012-03-26 Removed body onLoad event which popped up /help/selectionrefresh.htm in certain circumstances (neither file nor circumstance seems to exist)
WAB 2015-06-29 	Changes to handling of nullText in relayFormElementDisplay (Case 444625) resulted in an unwanted extra null value in frmOrganisationTypeID drop down.
				Remove null item from query and pass a specific nullText and nullValue (0)
2015-11-20  ACPK    PROD2015-294 Replaced tables with Bootstrap
2015-11-23  ACPK    PROD2015-294 Added missing form-group divs
2016-02-02	WAB	Altered Organisation Type Drop down to vbe not required (actually there is no null option)
2016-03-07	WAB	PROD2016-717 Added plugin to all Multiselects.
							Don't show Selection options if user has no selections
							Removed separate page for choosing organisationType
							Removed the #ChangeCriteriaFilterDescription# which looks a bit silly
2017/02/20	NJH	JIRA RT-288 - fixed issue where an empty selection was not getting picked up as being invalid. Had to a a few more fields to exclude from the check.

--->
<!--- delete exisiting criteria
	WAB added 2000-09-20 needed while DAM code being developed but not complete
 --->

<cfparam NAME="frmFilterCriteriaDisplay" DEFAULT="MYRECENT">   <!--- ALL,MYRECENT,RECENT --->
<cfparam NAME="frmorganisationTypeID" DEFAULT="">	<!--- AJC 2007-04-19 organisationTypeID default(0,ALL ORG TYPES) --->
<cfparam name="request.showLocationFax" Default="true">
<cfparam name="request.showLocationPostalCode" Default="true">
<cfparam name="request.showLocationLocality" Default="true">
<cfparam name="request.showServiceContractExpiringBetween" Default="true">

<cfset divLayout = "horizontal">

 	<CFQUERY NAME="delCriteria" datasource="#application.siteDataSource#">
		DELETE FROM TempSelectionCriteria
		WHERE SelectionID = #request.relayCurrentUser.personid#
	</CFQUERY>

<!--- get the user's countries --->
<cfinclude template="/templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->

<CFQUERY NAME="getCountries" datasource="#application.siteDataSource#">
	SELECT DISTINCT
		a.CountryID,
		a.CountryDescription
	FROM Country AS a
	WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND a.ISOcode IS NOT null
	ORDER BY a.CountryDescription
</CFQUERY>

<CFQUERY NAME="getCountrygroups" datasource="#application.siteDataSource#">
SELECT DISTINCT b.CountryDescription AS CountryGroup, b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
  AND (b.ISOcode IS null OR b.ISOcode = '')
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
ORDER BY b.CountryDescription
</CFQUERY>


<cfset dummyQuery = queryNew('dummy')>
<cfset queryAddRow(dummyQuery)>
<CFQUERY NAME="getCountrygroupsAndCountries" dbType="query">
Select 'Any' as CountryDescription,  '*37' as CountryID
from dummyQuery

union

select CountryGroup, '*' + cast (Countryid as varchar)
from getCountrygroups

Union

select CountryDescription, cast (Countryid as varchar)
from getCountries
</CFQUERY>

<!--- AJC 2007-04-19 add in organisation type ID --->
<!--- AJC 2007-05-08	Added in blank value to force a user to select an option from the initial organisation type filter --->
<!--- WAB 2015-06-29  removed Null item, null item now set when calling CF_relayFormElementDisplay  --->
<cfquery name="qry_get_organisationTypes" datasource="#application.siteDataSource#">
	SELECT organisationTypeID,'phr_sys_'+TypeTextID as TypeTextID, sortIndex
	FROM organisationType
	WHERE Active=1
	ORDER BY sortIndex
</cfquery>

<!--- if the is only 1 active organisation type --->
<cfif qry_get_organisationTypes.recordcount is 1>
	<cfset frmorganisationTypeID=qry_get_organisationTypes.organisationtypeID>
<!--- if there are no organisation types set to default (0) --->
<cfelseif qry_get_organisationTypes.recordcount is 0>
	<cfset frmorganisationTypeID=0>
</cfif>

<CFIF getCountries.RecordCount GT 0>
	<!--- no point to running the other queries if no countries
			this IF should be consistent with the one
			controlling the display of select boxes --->


	<CFQUERY NAME="getLanguage" datasource="#application.siteDataSource#">
	SELECT * FROM Language ORDER BY Language
	</CFQUERY>

	<CFQUERY NAME="getDataSource" datasource="#application.siteDataSource#">
	SELECT * FROM DataSource ORDER BY Description
	</CFQUERY>

</CFIF>

<CFQUERY NAME="getTesters" datasource="#application.siteDataSource#">
	SELECT p.PersonID FROM Person AS p, Location AS l
	WHERE (l.LocationID = 1 OR l.LocationID = 1127973)
	AND l.LocationID = p.LocationID
</CFQUERY>

<CFSET validTesters = ValueList(getTesters.PersonID)>


<cf_head>
<CFOUTPUT>
<SCRIPT type="text/javascript">
<!-- Hide from other browsers

function openWin( windowURL,  windowName, windowFeatures ) {
	return window.open( windowURL, windowName, windowFeatures ) ;
}

function ValidateForm() {

	var form = document.ThisForm;
	var msg = "";

	if (((form.frmPostalCodefrom.value != "") && (form.frmPostalCodeto.value == ""))
			||
		((form.frmPostalCodeto.value != "") && (form.frmPostalCodefrom.value == ""))
	) {
		<!---  --->
		msg = msg + "\n* You must enter a 'to' and 'from' Postal Code range to search, or try a Wildcard search on the text entry.";
	}

	var submitted = false;
	if (msg != "") {
		alert("Check your search criteria for the following problems:\n" + msg);
	} else {
		for (count=0; count<form.elements.length; count++) {

			var elename = form.elements[count].name;
			var eletype = form.elements[count].type; <!--- kt 1999-11-26 --->

			//alert("eletype name is " + eletype + "\n element name is " + elename);

			if (elename != "frmPersLastUpdatedwhen"
			 && elename != "frmLocationLastUpdatedwhen"
			 && elename != "frmLocationLastUpdateddate_eurodate"
			 && elename != "frmPersLastUpdateddate_eurodate"
			 && elename != "PersonFlags"
			 && elename != "frmPersNameType"
			 && eletype != "hidden"
			 && elename != "filterCriteria"
			 && elename != "selectSearchBtn") {
				if (elename == "frmDirectPartner") {
					if (form.elements[count].checked && form.elements[count].value != "") {
						var submitted = true;
						preSubmit();
						break;
					}
				} else {
					if (eletype == "select-one") {
						<!--- Single Select List --->
						var idx = form.elements[count].selectedIndex;
						var cidx = form.frmCountryID.selectedIndex;

							<!--- 	WAB removed this, don't see where countries come into it ((idx != 0) && (form.frmCountryID[cidx].value != "0")) { --->
						if ((idx != 0) ) {
							var submitted = true;
							preSubmit();
							break;
						}
					} else if (eletype == "radio") {
						if (form.elements[count].checked && form.elements[count].value != "") {
							var submitted = true;
							preSubmit();
							break;
						}
					} else if (eletype == "select-multiple" && elename != "frmExSelection" && elename != "frmExLocSelection" ) {
						for (i=0; i<form.elements[count].length; i++) {
							if (form.elements[count].options[i].selected) {
								if (form.elements[count].options[i].value != "0" && form.elements[count].options[i].value != "") {
									var submitted = true;
									preSubmit();
									break;
								}
							}
						}
					} else {

							if (eletype == "checkbox") {
								if (form.elements[count].checked && form.elements[count].value != "") {
									// alert("****************************** \n\n\n\n\n\n\n\n\n\n this is a checkbox with a value and has been checked " + elename)
									var submitted = true;
									preSubmit();
									break;
								}
							} else if ((eletype != "radio" && eletype != "select-multiple" )&& (form.elements[count].value != "" && form.elements[count].value != "0" && form.elements[count].value != " " && form.elements[count].value != "  ")) {
								var submitted = true;
								preSubmit();
								break;
							}
						}
					}
				}
			}
		if (!(submitted)) {
			alert("You must enter some criteria for your selection.\n");
		}
	}
}

//2013-02-13	YMA	Case 438885: Disable empty form fields to avoid submitting form with too much data for HTTP request.
function preSubmit() {
	jQuery('##ThisForm').find(':input[value=""]').each(function()
		{
			if (jQuery(this).val() == '') {
				jQuery(this).attr("disabled", "disabled");
			}
		});
	document.ThisForm.submit();
}

function toggleVisibility( divName, callerObject )
{
	theDiv = document.getElementById(divName)
  theDiv.style.display = ( theDiv.style.display == "" ) ? "none" : "";
  if ( callerObject )
  {
    callerObject.src = ( theDiv.style.display == "" ) ? iconDoubleArrowUp.src : iconDoubleArrowDown.src;
  }
  return true;
}
var iconDoubleArrowUp = new Image();
var iconDoubleArrowDown = new Image();
function eventWindowOnLoad( callerObject )
{
  iconDoubleArrowUp.src = "/images/MISC/iconDoubleArrowUp.gif";
  iconDoubleArrowDown.src = "/images/MISC/iconDoubleArrowDown.gif";
}



// very basic function which I use with the Location/Person LastUpdated fields - sets the before/after dropdown to a default value  (really a hack to prevent the 'after' being put into the database on every search!
function setDropDownToFirstValueIfNotSelected(objectName) {
	obj = document.getElementById(objectName)
	if (obj.value == ''){obj.selectedIndex = 1}
}

// similar to above function, but for radios
function setRadioToThisValueIfNotSelected(objectName,value) {
	obj = document.getElementsByName(objectName)
	// check whether any items are already checked
	anyChecked = false
	for (j=0; j<obj.length; j++){
		if (obj[j].checked) {anyChecked = true}
	}
	// if none checked then check the default one
	if (!anyChecked ){
		for (j=0; j<obj.length; j++){
			if (obj[j].value == value)  {
				obj[j].checked = true
			}

		}

	}

}


// -->
</SCRIPT>
</CFOUTPUT>
</cf_head>

<cf_translate>
<cf_body onload="eventWindowOnLoad(this);" >
<cfset application.com.request.setDocumentH1(text="Selections")>
<!--- <cfif frmOrganisationTypeID neq ""> --->
<CFPARAM NAME="frmruninpopup" DEFAULT="">
<CFSET pageTitle = "Create New Selection">
<!--- Form which is submitted to change which flags are shown --->
<cfsetting enablecfoutputonly="no" >

<FORM ACTION="selectsearch.cfm" METHOD="POST" NAME="FilterCriteriaForm">
	<input type="hidden" name="frmFilterCriteriaDisplay" value = "">
	<cfoutput>
		<CF_INPUT type="hidden" name="frmorganisationTypeID" value = "#frmorganisationTypeID#">
	</cfoutput>
</form>

<script>
	function changeFilterCriteria (theValue) {
		form = document.FilterCriteriaForm;
		form.frmFilterCriteriaDisplay.value = theValue;
		form.submit()
		divTag = document.getElementById ("ChangeCriteriaFilterLoading")
		divTag.style.display = "block"
		divTag.focus()
	}
</script>

<cfsavecontent variable = "ChangeCriteriaFilterHTML">
	<label for="filterCriteria">Filter criteria to show</label>
	<cf_displayValidValues formfieldname="filterCriteria" validValues = "All#application.delim1#All Fields,Recent#application.delim1#Fields Searched Recently By Any Users,MyRecent#application.delim1#Fields Searched Recently By Me" onChange="javascript:changeFilterCriteria(this.value)" currentvalue = #frmFilterCriteriaDisplay# showNull = false>
</cfsavecontent>
<!--- <cfswitch expression= #frmFilterCriteriaDisplay#>
	<cfcase value = "ALL">
		<cfset ChangeCriteriaFilterDescription = "">
	</cfcase>
	<cfcase value = "RECENT">
		<cfset ChangeCriteriaFilterDescription = "(filtered to show fields recently searched by any user)">
	</cfcase>
	<cfcase value = "MYRECENT">
		<cfset ChangeCriteriaFilterDescription = "(filtered to show fields recently searched by yourself)">
	</cfcase>
	<cfdefaultcase>
		<cfset ChangeCriteriaFilterDescription = "">
	</cfdefaultcase>
</cfswitch> --->

<FORM ACTION="selectsum.cfm" METHOD="POST" id="ThisForm" NAME="ThisForm">

	<!--- GREY BOX TOP --->
	<div class="grey-box-top">
		<div class="row">
			<div class="col-xs-12 col-md-4 form-group">
				<cfoutput>
					<!--- a country exists so slap down the other selects too but only if records were returned.
					NB although records will almost certainly exist for town some of the other selects on
					PersonLookup fields may not, hence each IF --->
					#ChangeCriteriaFilterHTML#
					<div id="ChangeCriteriaFilterLoading" style = "display:none;">
						Re-Loading ....
					</div>
				</cfoutput>
			</div>

			<div class="col-xs-12 col-sm-4 form-group">
				<label for="frmCountryID">Phr_Sys_CountryCountryGroup</label>
				<cf_select id="frmCountryID" class="form-control" name="frmCountryID" MULTIPLE = true query="#getCountrygroupsAndCountries#" value="CountryID" display = "CountryDescription" selected="" pluginOptions = #{"selectAll"=false}#>
			</div>

			<div class="col-xs-12 col-sm-4 form-group">
				<cfif qry_get_organisationTypes.RecordCount gt 1>
					<cfquery name="qry_get_organisationType" datasource="#application.siteDataSource#">
						SELECT organisationTypeID,'phr_sys_'+TypeTextID as TypeTextID
						FROM organisationType
						WHERE organisationtypeID =  <cf_queryparam value="#frmorganisationtypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>
					<label for="frmOrganisationTypeID">Phr_Sys_OrganisationType</label>
					<CF_relayFormElement relayFormElementType="select" fieldName="frmOrganisationTypeID" currentValue=" " query="#qry_get_organisationTypes#" display="typeTextID" value="organisationTypeID" showNull=true nulltext="phr_sys_All_Organisation_Types" nullValue="0">
				<cfelse>
					<cfif frmOrganisationTypeID is "0">
						<cfset frmOrganisationTypeID = "">
					</cfif>
					<cfoutput><CF_INPUT type="hidden" name="frmOrganisationTypeID" value="#frmOrganisationTypeID#"></cfoutput>
				</cfif>
			</div>
		</div>
	</div> <!--- GREY BOX TOP --->


	<CFIF getCountries.RecordCount GT 0>
		<!--- New Flags --->
		<div class="accordion-group">

      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#selectSearchAccordion" href="#div-org">
					<!--- <cfoutput>
						<img src="/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-org', this );">
					</cfoutput> --->
					Phr_Sys_ORGANISATIONSELECTIONCRITERIA
				</a>
      </div>

			<div id="div-org" class="accordion-body collapse">
				<!--- <cfoutput>#htmleditformat(ChangeCriteriaFilterDescription)#</cfoutput> --->
				<CFSET flagMethod = "search">
				<CFSET entityType = 2>
				<CFINCLUDE TEMPLATE="/flags/allFlagSearch.cfm">

				<!--- START: Elearning Addon --->
				<!--- NJH 2007/11/13 if the elearning module is turned on --->
				<!--- <cfif listfind(application. liverelayapps,35)> --->
				<cfif application.com.relayMenu.isModuleActive(moduleID="elearning")>
					<!--- NJH 2008/09/19 elearning 8.1 - add specialisations to selections --->
					<cfquery name="getSpecialisations" datasource="#application.siteDataSource#">
						select specialisationID, code from specialisation order by code
					</cfquery>
					<cfif getSpecialisations.recordCount gt 0>
						<div class="row">
							<div class="col-xs-12 form-group">
								<label>phr_selection_specialisations:</label>
								<cfoutput>
									<cf_select name="frmSpecialisationID" multiple="true" query="#getSpecialisations#" Value="specialisationID" display = "code" pluginOptions = #{"selectAll"=false}#>
								</cfoutput>
							</div>
						</div>
					</cfif>
				</cfif>
				<!--- END: Elearning Addon --->

				<!--- 2006-05-19 SWJ moved to here so it's more logical --->
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Phr_Sys_OrganisationName</label>
						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationName" Value="" SIZE="25" MAXLENGTH="50">
						<p>(supports the use of % as a wildcard)</p>
						<!--- </TR>
							<TD VALIGN="CENTER" NOWRAP>
							 <label>Phr_Sys_CompanyName</label>
							</TD>
							<TD VALIGN="CENTER" COLSPAN=6>
								<INPUT TYPE="TEXT" NAME="frmCompanyName" Value="" SIZE="25" MAXLENGTH="50"> (supports the use of % as a wildcard)
							</TD>
						</TR> --->
  					<!--- NJH 2009/06/24 Bug Fix Sony1 Support Issue 2214 - changed frmCompanyFrom and frmCompanyTo to frmOrganisationFrom and frmOrganisationTo --->
						Between
						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationFrom" Value="" SIZE="5" MAXLENGTH="10">
						<p>And</p>
						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationTo" Value="" SIZE="5" MAXLENGTH="10">
						<p>e.g. Between 'C' and 'F'.  Includes all names begining with C,D,E.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Creation Date For This Organization:</label>
						<SELECT ID="frmOrgCreateContactDateWhen" CLASS="form-control" NAME="frmOrgCreateContactDateWhen">
							<option value= ""></option>
							<option value= "before">Before</option>
							<option value= "after">On or After</option>
							<option value= "between">Between</option>
						</SELECT>
						<!--- <INPUT TYPE="TEXT" NAME="frmOrgCreateDate" Value="" SIZE="25" MAXLENGTH="50" onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrgCreateContactDateWhen')"> (dd-mmm-yyyy)
						And
						<INPUT TYPE="TEXT" NAME="frmOrgCreateDateEnd" Value="" SIZE="25" MAXLENGTH="50" onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrgCreateContactDateWhen')"> (dd-mmm-yyyy) --->
						<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmOrgCreateDate" helpText=""
							onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrgCreateContactDateWhen')">
						<p>And</p>
						<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmOrgCreateDateEnd" helpText=""
							onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrgCreateContactDateWhen')">
          </div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Organization Created By:</label>
						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationCreatedby" VALUE="" SIZE="30" MAXLENGTH="30">
					</div>
				</div>

				<!--- START	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Organization Last Updated:</label>
						<SELECT ID="frmOrganisationLastUpdatedWhen" CLASS="form-control" NAME="frmOrganisationLastUpdatedWhen">
							<option value= ""></option>
							<option value= "after">On or After</option>
							<option value= "before">Before</option>
						</SELECT>
						<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmOrganisationLastUpdateddate" helpText=""
							onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrganisationLastUpdatedWhen')">
						<!---<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationLastUpdateddate" Value="" SIZE="25" MAXLENGTH="50" onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrganisationLastUpdateddate')"> (dd-mmm-yyyy)--->
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
					  <label>Organization Last Updated By:</label>
						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationLastUpdatedby" VALUE="" SIZE="30" MAXLENGTH="30">
					</div>
				</div>

				<!--- STOP	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Last Contact With This Organization:</label>
						<SELECT ID="frmOrgLastContactDateWhen" CLASS="form-control" NAME="frmOrgLastContactDateWhen">
							<option value= ""></option>
							<option value= "before">Before </option>
							<option value= "after">On or After </option>
						</SELECT>
						<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmOrgLastContactDate" helpText=""
							onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrgLastContactDateWhen')">
						<!---<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrgLastContactDate" Value="" SIZE="25" MAXLENGTH="50" onblur="javascript:setDropDownToFirstValueIfNotSelected('frmOrgLastContactDateWhen')">
						(dd-mmm-yyyy)--->
						<!--- <INPUT TYPE="HIDDEN" NAME="frmNoOrgContactSinceDate_eurodate"> --->
					</div>
				</div>

				<div class="row">
          <div class="col-xs-12 form-group">
            <label>Organization ID</label> (comma separated list):
 						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmOrganisationID" Value="" SIZE="25" MAXLENGTH="3990">
						(List can contain only 3990 characters)
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						<label>Exclude these Organization IDs</label>
						(comma separated list):
						<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmExOrganisationID" Value="" SIZE="25" MAXLENGTH="3990">
						(List can contain only 3990 characters)
					</div>
				</div>

			</div>
		</div> <!--- / END OF FIRST ACCORDION GROUP --->

	   <!---    This is the main location flag section  --->
		<div class="accordion-group">
      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#selectSearchAccordion" href="#div-loc">
					<!--- <cfoutput>
						<img src="/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-loc', this );">
					</cfoutput> --->
					Phr_Sys_LOCATIONSELECTIONCRITERIA
		    </a>
		  </div>

			<div id="div-loc" class="accordion-body collapse">
				<div class="accordion-inner">
		      <CFSET flagMethod = "search">
	        <CFSET entityType = 1>
					<!--- <cfoutput>#htmleditformat(ChangeCriteriaFilterDescription)# </cfoutput> --->
					<CFINCLUDE TEMPLATE="/flags/allFlagSearch.cfm">

					<!--- Here starts the main location section --->
			    <!--- NJH 2009/06/24 Bug Fix Sony1 Support Issue 2214 - add location site name --->
			    <div class="row">
						<div class="col-xs-12 form-group">
					    <label>Phr_Sys_CompanyName</label>
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmCompanyName" Value="" SIZE="25" MAXLENGTH="50">
							<p>(supports the use of % as a wildcard)</p>
							Between
							<INPUT TYPE="TEXT" ID="frmCompanyFrom" CLASS="form-control" NAME="frmCompanyFrom" Value="" SIZE="5" MAXLENGTH="10">
							<p>And</p>
							<INPUT ID="frmCompanyTo" TYPE="TEXT" CLASS="form-control" NAME="frmCompanyTo" Value="" SIZE="5" MAXLENGTH="10">
   						<p>e.g. Between 'C' and 'F'.  Includes all names begining with C,D,E.</p>
						</div>
					</div>

					<div class="row">
            <div class="col-xs-12 form-group">
	            <label>Phr_Sys_accountType</label>
							<SELECT CLASS="form-control" NAME="frmAccountTypeID" size="4" multiple>
								<cfoutput>
									<cfloop collection="#application.organisationType#" item="orgTypeID">
										<cfif isNumeric(orgTypeID)><OPTION VALUE="#orgTypeID#"> #htmleditformat(application.organisationType[orgTypeID].typeTextID)#</OPTION></cfif>
									</cfloop>
								</cfoutput>
							</SELECT>
						</div>
					</div>

					<CFIF getDataSource.RecordCount GT 0>
						<!--- a single select list for Data Source --->
						<div class="row">
		          <div class="col-xs-12 form-group">
								<label>Location Data Source:</label>
								<SELECT CLASS="form-control" NAME="frmDataSource">
									<OPTION VALUE="0"> Any</OPTION>
									<CFOUTPUT QUERY="getDataSource">
										<OPTION VALUE="#DataSourceID#"> #htmleditformat(Description)#</OPTION>
									</CFOUTPUT>
								</SELECT>
							</div>
						</div>
					</CFIF>

					<!--- MPR 2008-03-13 P-TND009 Partner Profile Report --->
					<div class="row">
						<div class="col-xs-12 form-group">
							<cfif request.showLocationFax>
								<label>phr_sys_LocationFax</label>
								<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmLocationFax" Value="" SIZE="25" MAXLENGTH="50">
							<cfelse>
								<INPUT TYPE="HIDDEN" NAME="frmLocationFax" Value="" SIZE="25" MAXLENGTH="50">
							</cfif>
						</div>
					</div>

					<!--- NYF 2008-05-06 P-TND009 Partner Profile Report --->
					<div class="row">
						<div class="col-xs-12 form-group">
							<label>phr_Sys_TownCity</label>
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmAddress4" Value="" SIZE="25" MAXLENGTH="50">
						</div>
					</div>

					<!--- MPR 2008-03-13 P-TND009 Partner Profile Report --->
					<div class="row">
						<div class="col-xs-12 form-group">
							<cfif request.showLocationPostalCode>
								<label>phr_sys_LocationPostalCode</label>
								<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPostalCodetext" Value="" SIZE="10" MAXLENGTH="50">
								<p>(to find blank Postal Codes, type: null)</p>
								<p>OR</p>
								Range:
								<INPUT TYPE="TEXT" ID="frmPostalCodefrom" CLASS="form-control" NAME="frmPostalCodefrom" Value="" SIZE="10" MAXLENGTH="50">
								to
								<INPUT TYPE="TEXT" ID="frmPostalCodeto" CLASS="form-control" NAME="frmPostalCodeto" Value="" SIZE="10" MAXLENGTH="50">
							<cfelse>
								<INPUT TYPE="HIDDEN" NAME="frmPostalCodetext" Value="" SIZE="10" MAXLENGTH="50">
								<INPUT TYPE="HIDDEN" NAME="frmPostalCodefrom" Value="" SIZE="10" MAXLENGTH="50"> to
								<INPUT TYPE="HIDDEN" NAME="frmPostalCodeto" Value="" SIZE="10" MAXLENGTH="50">
							</cfif>
						</div>
					</div>


					<div class="row">
						<div class="col-xs-12 form-group">
							<CFIF listFind(request.relaycurrentuser.countryList,9) is not 0>
							<!--- wab testing a new type of search on valid postal code prefixes--->
							  <label>phr_sys_PostalCodeArea</label>
		            <cf_displayValidValues formfieldName="frmUKPostalCode" validFieldname="location.postalCodePrefix" displayas="multiselect" listsize="4" countryid="9" showNull="true" NullText="Ignore">
	            </cfif>
		        </div>
					</div>


					<div class="row">
						<div class="col-xs-12 form-group">
							<CFIF listFind(request.relaycurrentuser.countryList,9) is not 0>
							<!--- wab testing a new type of search on valid postal code prefixes--->
								<label>phr_sys_Territories</label>
								<cf_displayValidValues formfieldName="frmUKTerritory" validFieldname="location.Territory" displayas="multiselect" listsize="4" countryid="9" showNull="true" NullText="Ignore">
							</cfif>
			      </div>
					</div>


					<div class="row">
						<div class="col-xs-12 form-group">
							<CFIF listFind(countryList,32) is not 0>  <!--- South Africa --->
								<label>phr_sys_SAProvince</label>
								<cf_displayValidValues formfieldName="frmAddress5" validFieldname="location.Address5" displayas="multiselect" listsize="4" countryid="32" showNull="true" NullText="Ignore">
							</cfif>
						</div>
					</div>

					<!--- MPR 2008-03-13 P-TND009 Partner Profile Report --->
					<div class="row">
						<div class="col-xs-12 form-group">
							<cfif request.showLocationLocality>
					     	<label>phr_sys_locality</label>
					     	<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmRegionState" Value="" SIZE="25" MAXLENGTH="50">
							<cfelse>
								<INPUT TYPE="HIDDEN" NAME="frmRegionState" Value="" SIZE="25" MAXLENGTH="50">
							</cfif>
						</div>
		    	</div>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Location ID</label> (comma separated list):
							<!--- NJH 2009/03/23 Support Request Issue 23 --->
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmLocationID" Value="" SIZE="25" MAXLENGTH="3990">
    					(List can contain only 3990 characters)
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Exclude these Location IDs</label> (comma separated list):
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmExLocationID" Value="" SIZE="25" MAXLENGTH="3990">
    	   			(List can contain only 3990 characters)
			      </div>
					</div>

					<div class="row">
						<div class="col-xs-12 form-group">
          		<label>Location Last Updated:</label>
        			<SELECT ID="frmLocationLastUpdatedWhen" CLASS="form-control" NAME="frmLocationLastUpdatedWhen">
        				<option value= ""></option>
        				<option value= "after">On or After</option>
        				<option value= "before">Before</option>
        			</SELECT>
        			<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmLocationLastUpdateddate" helpText=""
								onblur="javascript:setDropDownToFirstValueIfNotSelected('frmLocationLastUpdatedWhen')">
          			<!---<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmLocationLastUpdateddate" Value="" SIZE="25" MAXLENGTH="50" onblur="javascript:setDropDownToFirstValueIfNotSelected('frmLocationLastUpdatedWhen')">
          			(dd-mmm-yyyy)--->
          			<!--- <INPUT TYPE="HIDDEN" NAME="frmLocationLastUpdateddate_eurodate"> --->
          	</div>
          </div>

					<!--- <TD VALIGN="TOP" NOWRAP COLSPAN=6>
						All records before
						<INPUT TYPE="RADIO" ID="frmLocationLastUpdatedwhen" NAME="frmLocationLastUpdatedwhen" VALUE="before"> All records before
						<INPUT TYPE="RADIO" ID="frmLocationLastUpdatedwhen" NAME="frmLocationLastUpdatedwhen" VALUE="after" checked> All records on or after <!--- WAB 2006-03-02 added checked to this one by default --->
					</TD> --->

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Location Last Updated By:</label>
    					<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmLocationLastUpdatedby" VALUE="" SIZE="30" MAXLENGTH="30">
						</div>
					</div>

			 		<!--- <TR>
						<TD VALIGN="CENTER" NOWRAP>
							<label>Location Direct Partner?:</label>
						</TD>
						<TD VALIGN="CENTER" COLSPAN=6>
							<INPUT TYPE="RADIO" NAME="frmDirectPartner" VALUE="*t"> Yes
							<INPUT TYPE="RADIO" NAME="frmDirectPartner" VALUE="*f"> No
							<INPUT TYPE="RADIO" NAME="frmDirectPartner" VALUE="" CHECKED> Don't care
						</TD>
					</TR> --->

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Locality:</label>
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmRegionState" Value="" SIZE="25" MAXLENGTH="50">
		        </div>
					</div>

				</div>
			</div>
		</div>

		<!---    This is the main person flag section                                   --->
		<div class="accordion-group">
      <div class="accordion-heading">
      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#selectSearchAccordion" href="#div-per">
					<!--- <cfoutput>
						<img src="/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-per', this );">
					</cfoutput> --->
					Phr_Sys_PERSONPROFILESELECTIONCRITERIA
				</a>
			</div>

		  <div id="div-per" class="accordion-body collapse">
		    <div class="accordion-inner">
					<CFSET flagMethod = "search">
					<CFSET entityType = 0>
					<!--- <cfoutput>#htmleditformat(ChangeCriteriaFilterDescription)#</cfoutput> --->
					<CFINCLUDE TEMPLATE="/flags/allFlagSearch.cfm">
					<!--- Here starts the main person section --->

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Person Name:</label>
							<INPUT TYPE="text" id="frmPersNametext" CLASS="form-control" NAME="frmPersNametext" VALUE="" SIZE="30" MAXLENGTH="30"onblur="javascript:setRadioToThisValueIfNotSelected('frmPersNameType','last')">
							<div class="form-inline inline-radios">
								<div class="radio">
									<label>
										<INPUT TYPE="RADIO" NAME="frmPersNameType" ID="frmPersNameType" VALUE="first"> First name
										<!--- Value of this is Specific to database fieldname --->
									</label>
								</div>
								<div class="radio">
									<label>
										<INPUT TYPE="RADIO" NAME="frmPersNameType" ID="frmPersNameType"  VALUE="last"> Last name
									</label>
								</div>
								<div class="radio">
									<label>
										<INPUT TYPE="RADIO" NAME="frmPersNameType" ID="frmPersNameType"  VALUE="Full"> Full name
									</label>
								</div>
							</div>
						</div>
					</div>


					<!--- MAXINE Hello All, I've add in this gender section below --->
					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Gender:</label>
							<div class="form-inline inline-radios">
								<div class="radio">
									<label>
										<INPUT TYPE="radio" NAME="frmPersSex" Value="M"> Male
									</label>
								</div>
								<div class="radio">
									<label>
										<INPUT TYPE="radio" NAME="frmPersSex" Value="F"> Female
									</label>
								</div>
								<div class="radio">
									<label>
										<INPUT TYPE="radio" NAME="frmPersSex" Value="null"> Unknown
									</label>
								</div>
							</div>
						</div>
					</div>


					<CFIF getLanguage.RecordCount GT 0>
						<!--- a singleselect list for Language --->
						<div class="row">
							<div class="col-xs-12 form-group">
								<label>Person Language:</label>
								<!--- 2007-06-25 WAB added None Set option - note that "Null" becomes = '' in qryCreateSelection (already coded!)--->
								<SELECT CLASS="form-control" NAME="frmPersLanguage">
									<OPTION VALUE="" SELECTED> Any</OPTION>
									<OPTION VALUE="null"  >Not Specified</OPTION>
									<CFOUTPUT QUERY="getLanguage">
										<OPTION VALUE="#Language#"> #htmleditformat(Language)#</OPTION>
									</CFOUTPUT>
								</SELECT>
							</div>
						</div>
					</CFIF>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Person Email:</label>
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPersEmail" Value="" SIZE="25" MAXLENGTH="50">
							<p>(to find people with blank Emails, type: null)</p>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Email Status:</label>
							<div class="radio">
								<label>
									<INPUT TYPE="radio" NAME="frmEmailStatus" VALUE="> 0"> Email addresses which have failed
								</label>
							</div>
							<div class="radio">
								<label>
									<INPUT TYPE="radio" NAME="frmEmailStatus" VALUE="<= 0"> Email addresses which have not failed
								</label>
							</div>
							<div class="radio">
								<label>
									<INPUT TYPE="radio" NAME="frmEmailStatus" VALUE="> 0 and emailstatus <> convert(int,emailStatus)"> Email addresses which were OK, but have started to fail
								</label>
							</div>
							<div class="radio">
								<label>
									<INPUT TYPE="radio" NAME="frmEmailStatus" VALUE=" = -0.5"> Email confirmed OK
								</label>
							</div>
							<div class="checkbox">
								<label>
									<INPUT TYPE="checkbox" NAME="frmSupressBlankEmailAddress" VALUE="1"> Check to <label>supress</label> Blank emails
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Job Description:</label> (supports the use of % as a wildcard)
    					<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPersJobDesc" Value="" SIZE="25" MAXLENGTH="50">
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Person ID</label> (comma separated list):
							<!--- NJH 2009/03/23 Support Request Issue 23 --->
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPersonID" Value="" SIZE="25" MAXLENGTH="3990">
 							(List can contain only 3990 characters)
						</div>
          </div>

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Exclude these Person IDs</label> (comma separated list):
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmExPersonID" Value="" SIZE="25" MAXLENGTH="3990">
    	   			(List can contain only 3990 characters)
			      </div>
					</div>

					<!--- START	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Creation Date For This Person:</label>
							<SELECT ID="frmPersCreateContactDateWhen" CLASS="form-control" NAME="frmPersCreateContactDateWhen">
								<option value= ""></option>
								<option value= "before">Before</option>
								<option value= "after">On or After</option>
								<option value= "between">Between</option>
							</SELECT>
							<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmPersCreateDate"
								anchorName="createFrom" helpText="">
							<p>And</p>
							<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmPersCreateDateEnd"
								anchorName="createTo" helpText="">
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 form-group">
						  <label>Person Created By:</label>
				      <INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPersCreatedby" VALUE="" SIZE="30" MAXLENGTH="30">
    		    </div>
					</div>

					<!--- STOP	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
					<!--- KT 1999-10-11  Removed from form since we never want to allow a selection of inactive people! --->
					<!--- <TR>
						<TD VALIGN="CENTER" NOWRAP>
							Person:
						</TD>
						<TD VALIGN="CENTER" COLSPAN=6>
							<INPUT TYPE="radio" NAME="frmPersActive" Value="=1" CHECKED> Active &nbsp;&nbsp;
							<INPUT TYPE="radio" NAME="frmPersActive" Value="=0"> Inactive
						</TD>
					</TR> --->

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Person Last Updated:</label>
							<SELECT ID="frmPersLastUpdatedWhen" CLASS="form-control" NAME="frmPersLastUpdatedWhen">
								<option value= ""></option>
								<option value= "after">On or After</option>
								<option value= "before">Before</option>
							</SELECT>
							<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmPersLastUpdateddate"
								helpText="" onblur="javascript:setDropDownToFirstValueIfNotSelected('frmPersLastUpdatedWhen')">
							<!---  <INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPersLastUpdateddate" Value="" SIZE="25" MAXLENGTH="50" 	onblur="javascript:setDropDownToFirstValueIfNotSelected('frmPersLastUpdatedWhen')"> (dd-mmm-yyyy)
							<INPUT TYPE="HIDDEN" NAME="frmPersLastUpdateddate_eurodate"> --->
						</div>
					</div>

					<!--- WAB 2006-12-06 removed and replaced with drop down --->

					<!--- <TR>
						<TD>&nbsp;</TD>
						<TD VALIGN="TOP" COLSPAN=6>
							<INPUT TYPE="RADIO" NAME="frmPersLastUpdatedwhen" VALUE="before"> All records before
							&nbsp; &nbsp;
							<INPUT TYPE="RADIO" NAME="frmPersLastUpdatedwhen" VALUE="after" checked> All records on or after   <!--- WAB 2006-03-02 added checked to this one by default --->
						</TD>
					</TR> --->

					<div class="row">
						<div class="col-xs-12 form-group">
							<label>Person Last Updated By:</label>
							<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPersLastUpdatedby" VALUE="" SIZE="30" MAXLENGTH="30">
						</div>
					</div>

					<!--- MPR 2008-03-13 P-TND009 Partner Profile Report --->
					<cfif request.showServiceContractExpiringBetween>
						<div class="row">
							<div class="col-xs-12 form-group">
								<label>phr_sys_ServiceContractExpiringBetween:</label>
								<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmProdContractStartDate"
									anchorName="contractFrom" helpText="">
								<p>And</p>
								<CF_relayDateField currentValue="" thisFormName="ThisForm" fieldName="frmProdContractEndDate"
									anchorName="contractTo" helpText="">
							</div>
						</div>
					</cfif>

						<div class="row">
							<div class="col-xs-12 form-group">
								<label>phr_sys_IncentivePointsBalanceBetween</label>
								<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPointsBalanceFrom" Value="" SIZE="12" MAXLENGTH="20">
								<p>(inclusive)</p>
								<p>And</p>
								<INPUT TYPE="TEXT" CLASS="form-control" NAME="frmPointsBalanceTo" Value="" SIZE="12" MAXLENGTH="20">
								<p>(exclusive)</p>
							</div>
						</div>


						<!--- START: Elearning Addon --->
						<!--- NJH 2007/11/13 if the elearning module is turned on --->
						<!--- <cfif listfind(application. liverelayapps,35)> --->
						<cfif application.com.relayMenu.isModuleActive(moduleID="elearning")>
							<cfquery name="getCourseSeries" datasource="#application.siteDataSource#">
								select courseSeriesID,courseSeriesShortName from trngCourseSeries
							</cfquery>

							<cfif getCourseSeries.recordCount gt 0>
								<div class="row">
									<div class="col-xs-12 form-group">
										<label>phr_selection_CourseSeries:</label>
										<SELECT CLASS="form-control" NAME="frmCourseSeries">
											<OPTION VALUE="" SELECTED></OPTION>
											<!--- WAB 2008/11/03 removed selected from this item, because it was ending up in every single search --->
											<OPTION VALUE="null" > Any</OPTION>
											<CFOUTPUT QUERY="getCourseSeries">
												<OPTION VALUE="#courseSeriesID#"> #htmleditformat(courseSeriesShortName)#</OPTION>
											</CFOUTPUT>
										</SELECT>
									</div>
								</div>
							</cfif>

							<cfquery name="getSolutionAreas" datasource="#application.siteDataSource#">
								select solutionAreaID,solutionAreaName from trngSolutionArea
							</cfquery>

							<cfif getSolutionAreas.recordCount gt 0>
								<div class="row">
									<div class="col-xs-12 form-group">
										<label>phr_selection_SolutionAreas:</label>
										<cf_SELECT CLASS="form-control" NAME="frmSolutionArea" query="#getSolutionAreas#" value = "solutionAreaID" display="solutionAreaName" select="" shownull="true" pluginOptions = #{"selectAll"=false}#>
									</div>
								</div>
							</cfif>


							<!--- NJH 2008/09/19 elearning 8.1 --->
							<cfquery name="getCertifications" datasource="#application.siteDataSource#">
								select certificationID,code from trngCertification order by code
							</cfquery>

							<cfif getCertifications.recordCount gt 0>
								<div class="row">
									<div class="col-xs-12 form-group">
										<label>phr_selection_Certifications:</label>
										<cfoutput>
											<cf_SELECT ID="frmCertificationID" CLASS="form-control" NAME="frmCertificationID" MULTIPLE = true query="#getCertifications#" value="certificationID" display = "code" selected="" pluginOptions = #{"selectAll"=false}#>
											<!---<cfloop query="getCertifications">
												<div class="col-xs-3">
													<div class="checkbox">
														<CF_INPUT TYPE="checkbox" NAME="frmCertificationID" VALUE="#certificationID#"> #htmleditformat(code)#
													</div>
												</div>
											</cfloop>--->
										</cfoutput>
									</div>
								</div>

								<cfquery name="getCertificationStatuses" datasource="#application.siteDataSource#">
									select statusID, description from trngPersonCertificationStatus order by description
								</cfquery>

								<div class="row">
									<div class="col-xs-12 form-group">
										<label>phr_selection_CertificationStatuses:</label>
										<SELECT CLASS="form-control" NAME="frmCertificationStatusID">
											<OPTION VALUE="" SELECTED></OPTION>
											<OPTION VALUE="null" > Any</OPTION>   <!--- WAB 2008/11/03 removed selected from this item, because it was ending up in every single search, needed to add blank value above --->
											<CFOUTPUT QUERY="getCertificationStatuses">
												<OPTION VALUE="#statusID#"> #htmleditformat(description)#</OPTION>
											</CFOUTPUT>
										</SELECT>
									</div>
								</div>
							</cfif>

							<!--- NJH 2009/02/13 P-SNY047 --->
							<cfset quizQry = application.com.relayQuiz.getQuizzes()>
							<cfif quizQry.recordCount gt 0>

								<div class="row">

									<div class="col-xs-12 form-group">
										<label>phr_selection_Quizzes:</label>
								    <SELECT CLASS="form-control" NAME="frmQuizDetailID">
											<OPTION VALUE="" SELECTED></OPTION>
											<OPTION VALUE="null" > Any</OPTION>
											<CFOUTPUT QUERY="quizQry">
												<OPTION VALUE="#quizDetailID#"> #htmleditformat(quizName)#</OPTION>
											</CFOUTPUT>
										</SELECT>

										<div class="row">
											<div class="col-xs-1">
												<div class="checkbox">
					                <label>
												   <INPUT TYPE="checkbox" NAME="frmQuizPass" VALUE="1"> phr_Pass
								          </label>
						            </div>
											</div>
				              <div class="col-xs-1">
												<div class="checkbox">
				                  <label>
														<INPUT TYPE="checkbox" NAME="frmQuizFail" VALUE="1"> phr_Fail
				               		</label>
				                </div>
											</div>
										</div>
									</div>
								</div>
							</cfif>
						</cfif> <!--- END: Elearning Addon --->

					</div>
				</div>
			</div>


			<!--- Here starts the main organisation section
				added by SWJ  25 July 2001  --->

			<!--- use the template below to get all the selections the user has rights to view
						the query is named getSelections and returns
						SelectionID,title,description,datecreated,owner,lastupdated --->
			<!--- Here starts the main selection section --->

			<cfset getSelections = application.com.selections.getSelections(orderby = "s.lastupdated desc, ltrim(s.title) asc")>
			<cfif getSelections.recordCount GT 0>
				<div class="accordion-group">

			    <div class="accordion-heading">
	          <a class="accordion-toggle" data-toggle="collapse" data-parent="#selectSearchAccordion" href="#div-sel">
							<!--- <cfoutput>
							<img src="/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-sel', this );">
							</cfoutput> --->
							Phr_Sys_CRITERIAUSINGOTHERSELECTIONS
				   	</a>
	    		</div>

			    <CFINCLUDE TEMPLATE="/templates/qrygetselections.cfm">
					<div id="div-sel" class="accordion-body collapse">
				    <div class="accordion-inner">
							<div class="row">
								<div class="form-group col-xs-6">
									<label for="frmExSelection">Exclude any people in these selections:</label>
									<cf_SELECT ID="frmExSelection" CLASS="form-control" NAME="frmExSelection" MULTIPLE = true query="#getSelections#" value="SelectionID" display = "dateTitle" selected="" pluginOptions = #{"selectAll"=false}#>
								</div>
								<div class="form-group col-xs-6">
									<label for="frmInSelection">Include only people in these selections:</label>
									<cf_SELECT ID="frmInSelection" CLASS="form-control" NAME="frmInSelection"  MULTIPLE = true query="#getSelections#" value="SelectionID" display = "dateTitle" selected="" pluginOptions = #{"selectAll"=false}#>
								</div>
							</div>

	            <div class="row">
								<div class="form-group col-xs-6">
									<label for="frmExLocSelection">Exclude any people at the locations in these selections:</label>
									<cf_SELECT ID="frmExLocSelection" CLASS="form-control" NAME="frmExLocSelection"  MULTIPLE = true query="#getSelections#" value="SelectionID" display = "dateTitle" selected="" pluginOptions = #{"selectAll"=false}#>
								</div>
								<div class="form-group col-xs-6">
									<label for="frmInLocSelection">Include only people at locations in these selections:</label>
									<cf_SELECT ID="frmInLocSelection" CLASS="form-control" NAME="frmInLocSelection"  MULTIPLE = true query="#getSelections#" value="SelectionID" display = "dateTitle" selected="" pluginOptions = #{"selectAll"=false}#>
								</div>
							</div>

							<div class="row">
	              <div class="form-group col-xs-6">
									<label for="frmExOrgSelection">Exclude any people at the organizations in these selections:</label>
									<cf_SELECT ID="frmExOrgSelection" NAME="frmExOrgSelection" CLASS="form-control"  MULTIPLE = true query="#getSelections#" value="SelectionID" display = "dateTitle" selected="" pluginOptions = #{"selectAll"=false}#>
	              </div>
	 	            <div class="form-group col-xs-6">
									<label for="frmInOrgSelection">Include only people at organizations in these selections:</label>
									<CF_SELECT ID="frmInOrgSelection" NAME="frmInOrgSelection" CLASS="form-control"  MULTIPLE = true query="#getSelections#" value="SelectionID" display = "dateTitle" selected="" pluginOptions = #{"selectAll"=false}#>
								</div>
							</div>
						</div>
          </div>
				</div>
			</cfif>
		</div>

		<div class="internalBodyPadding">
			<CFOUTPUT><cf_input type="button" id="selectSearchBtn" value="Search" onclick="JavaScript:ValidateForm();"></CFOUTPUT>
		</div>

	<CFELSE>
		<P>Sorry, there are currently no organizations located in countries to which you have access.</P>
	</CFIF>

</FORM>

<!---
<cfelse>
	<cfoutput>
		<cfparam name="request.relayFormDisplayStyle" default="HTML-table">
		<cfinclude template="/templates/relayFormJavaScripts.cfm">
		<form action="selectsearch.cfm" method="post" name="ThisForm" id="ThisForm">
			<div align="left" style="width:300;">
				<CF_relayFormDisplay>
					<!--- 2007-04-12 - AJC - Trend OrgTypeID code --->
					<!--- AJC - 2007-05-08 - Added in blank value to force a user to select an option from the initial organisation type filter
						NJH 2014/09/04 Removed this.
						WAB 2016-02-02 Problems occurred when I added proper support for nullValue in form validation.
										Since this field was marked required, the validation would no longer accept 0 as an acceptable value.
										Just removed the required attribute - it can't be unselected (unless you are very clever)
										If required really need it then need to add the "All Organisation Types" item to the query

					 --->
					<!--- If there is more than 1 Active Org Type then display Drop down --->
					<!--- If 1 Org Type set hidden value to it's ID --->
					<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="" currentValue="phr_continue" label="">
				</CF_relayFormDisplay>
			</div>
		</form>
	</cfoutput>
</cfif>


 --->

</cf_translate>

