<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	WAB 2005-05-16 added usergroups to the list of users and altered layout code  --->

<!--- Show all people, regardless of country rights --->

<CFQUERY NAME="getCurrentGrp" datasource="#application.siteDataSource#">
	SELECT sg.UserGroupID,
		    ug.name,
			s.CreatedBy  <!--- to identify owner --->
	  FROM SelectionGroup AS sg, UserGroup AS ug,
	       Selection AS s
     WHERE sg.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
	   AND sg.UserGroupID = ug.UserGroupID
	   AND sg.SelectionID = s.SelectionID
	   ORDER BY Name
</CFQUERY>

<CFQUERY NAME="getHead" datasource="#application.siteDataSource#">
	SELECT  s.Title,
			s.Description
<!--- 			s.RecordCount --->
	  FROM Selection AS s
	 WHERE s.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
<!--- 	   AND s.CreatedBy = #request.relayCurrentUser.usergroupid# --->
</CFQUERY>



<CFQUERY NAME="getRecords" datasource="#application.siteDataSource#">
	SELECT	c.PersonID,
			c.FirstName,
			c.LastName,
			d.SiteName,
			b.Status
	FROM Selection AS a, SelectionTag AS b, SelectionGroup AS sg,
		 Person AS c, Location AS d, Country AS e
	WHERE a.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
	  AND a.Tablename = 'Person'
	  AND a.SelectionID = b.SelectionID
	  AND b.EntityID = c.PersonID
	  AND c.Active <> 0 <!--- true ---> <!--- if person is active, then location & org must be ---->
	  <!--- any sharere can tag/untag --->
	  AND a.SelectionID = sg.SelectionID
	  AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#

	  AND c.LocationID = d.LocationID
	  AND e.CountryID = d.CountryID
	  <!--- but only show those people in the user's country --->
	  <!--- 2012-07-20 PPB P-SMA001		AND d.CountryID IN (#request.relayCurrentUser.CountryList#) --->
	  #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="d").whereClause#	<!--- 2012-07-20 PPB P-SMA001 --->
   ORDER BY d.SiteName,c.LastName
</cFQUERY>


<!--- <CFINCLUDE TEMPLATE="selecttophead.cfm"> --->

<P><CENTER>
	<TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="320">

	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099"><FONT COLOR="#FFFFFF"><B>SELECTION INFORMATION</B></font></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2>
			<CFOUTPUT>#htmleditformat(getHead.Title)#</CFOUTPUT>
		<BR></TD>
	</TR>
		<TR><TD COLSPAN="2" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=230 HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2>
			<CFOUTPUT>Record(s) Found: #getRecords.RecordCount#</CFOUTPUT>
		<BR></TD>
	</TR>

</TABLE>


		<cfset theQuery = getCurrentGrp>
		<!--- determine how many records per column
		this will even work if 1 record --->
		<cfset colLength = structNew()>
		<CFSET percol = theQuery.RecordCount / 3>
		<CFSET colLength[1] = Int(percol) >
		<CFSET colLength[2] = Int(percol)>
		<CFSET colLength[3] = Int(percol)>

		<CFIF theQuery.RecordCount MOD 3 IS 1>
			<CFSET colLength[1] = colLength[1] + 1>
		<CFELSEIF theQuery.RecordCount MOD 3 IS 2>
			<CFSET colLength[1] = colLength[1] + 1>
			<CFSET colLength[2] = colLength[2] + 1>
		</CFIF>


		<P>
		<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2 WIDTH="95%">
		<TR><TD>
			<P>The following users may tag/untag individuals and use the
		     selection for data maintenance. Only the owner (indicated by *) may
			  communicate to the selection.
			  <P>
			  <BR>
			</TD>
		</TR>
		</TABLE>

		<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2>
		<TR>
			<cfset startRow = 1>
			<cfloop index = "column" list = "1,2,3">
				<cfset endRow = startRow + colLength[Column] -1>
				<cfif endRow gt 0 >
					<!--- column --->
					<TD VALIGN="TOP">
						<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=6>
						<CFOUTPUT QUERY="getCurrentGrp" STARTROW=#startRow# MAXROWS=#endRow#>
						<TR>
							<TD VALIGN="TOP">#HTMLEditFormat(Name)# #IIf(UserGroupID IS CreatedBy,DE(" *"),DE(""))#</TD>
						</TR>
						</CFOUTPUT>
						</TABLE>
					</TD>
				</cfif>
				<cfset startRow = endRow + 1>
			</cfloop>

		</TR>
		</TABLE>




</CENTER>





