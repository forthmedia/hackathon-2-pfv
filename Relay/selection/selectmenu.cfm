<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="frmruninpopup" DEFAULT="">

<!--- <CFINCLUDE TEMPLATE="selecttophead.cfm"> --->

<cfif message neq "">
    <CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT>
</cfif>

<cfoutput>
	<CFIF #Trim(message)# IS "">
	    <table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
			<CFIF #checkPermission.AddOkay# GT 0>
				<TR>
					<TD WIDTH="20" ALIGN="right">&nbsp;</TD>
					<TD HEIGHT="30" ALIGN="right" VALIGN="TOP">
						<A HREF="#thisdir#/selectsearch.cfm" TARGET="main" >Create a new selection</A>
					</TD>
					<td width="200" valign="top"></td>
				</TR>
			</CFIF>
		
			<CFIF #checkPermission.UpdateOkay# GT 0>
				<TR>
					<TD WIDTH="20" ALIGN="right">&nbsp;</TD>
					<TD HEIGHT="30" ALIGN="right" VALIGN="TOP">
						<A HREF="#thisdir#/selectview.cfm" TARGET="main">Edit existing selections you have previously created</A>
					</TD>
					<TD VALIGN="Top"></TD>
				</TR>
			</CFIF>
			<CFIF #checkPermission.AddOkay# GT 0>
				<TR>
					<TD WIDTH="20" ALIGN="right">&nbsp;</TD>
					<TD HEIGHT="30" ALIGN="right" VALIGN="TOP">
						<A HREF="#thisdir#/selecttrans.cfm" TARGET="main">Copy or transfer a selection to other users</A>
					</TD>
					<TD VALIGN="Top"></TD>
				</TR>
			</cfif>
		
			<CFIF #checkPermission.AddOkay# GT 0>
				<TR>
					<TD WIDTH="20" ALIGN="right">&nbsp;</TD>
					<TD HEIGHT="30" ALIGN="right" VALIGN="TOP">
						<A HREF="#thisdir#/selectgrpframe.cfm" TARGET="main">Share a selection with a other users</A>
					</TD>
					<TD VALIGN="Top"></TD>
				</TR>
			</CFIF>
		</TABLE>
	</cfif>
</cfoutput>


