<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="frmTransferType" default = "move">
<!--- user rights to access:
		SecurityTask:
		Permission:

	WAB 2005-05-16 altered layout code
    2015-11-13  ACPK    Replaced tables with Bootstrap
---->


<!--- only those locations that are active
		could also search for locations with active people --->


<CFIF request.relayCurrentUser.countryList is not "">
<!--- ==============================
		same query as in selectgrpindiv.cfm
		and selectgrplist.cfm
	  ============================== ---->

	 <cfset getUsers = application.com.relayUserGroup.GetUsersWithRightsToSpecifiedCountries(countryIDList = request.relayCurrentUser.countryList)>

	<CFIF getUsers.RecordCount GT 0>
		<CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
		SELECT s.Title, s.SelectionID, s.Description, s.LastUpdated
		  FROM Selection AS s
		 WHERE s.CreatedBy = #request.relayCurrentUser.usergroupid#
		ORDER BY s.Title, s.Description, s.LastUpdated
		</CFQUERY>
	</CFIF>
</CFIF>

<cf_head>
<SCRIPT LANGUAGE="JavaScript">
<!--

function checkSelect(type) {
	var form = document.ThisForm;
	msg="";
	if (form.frmSelectionID.options[form.frmSelectionID.selectedIndex].value == 0) {
		msg += "\n* You must choose a selection."
	}

	var found = "no";

	for (j=0; j<form.elements.length; j++){
		thiselement = form.elements[j];
		if (thiselement.name == "frmUserID") {
			if (thiselement.checked) {
				found = "yes";
			}
		}
	}

	if (found == "no") {
		msg += "\n* You must choose at least one person."
	}

	if (msg != "") {
		alert("\nYou must provide the following information:\n\n" + msg);
	} else {
		form.frmtransfertype.value = type;
		form.submit();
	}

}


//-->
</SCRIPT>
</cf_head>

<CFPARAM NAME="frmruninpopup" DEFAULT="">

<CFSET application.com.request.setDocumentH1(text=application.com.regExp.camelCaseToSpaces(string=frmTransferType) & " Selections")>
<!--- <CFINCLUDE TEMPLATE="selecttophead.cfm"> --->

<FORM CLASS="form-horizontal" ACTION="selecttranstask.cfm" METHOD="POST" NAME="ThisForm">
	<cfoutput>
	   <CF_INPUT TYPE="HIDDEN" NAME="frmtransfertype" VALUE="#frmTransferType#">
	</cfoutput>
	<CFIF request.relayCurrentUser.countryList is not "">
		<CFIF getUsers.RecordCount GT 0>
			<CFIF getSelections.RecordCount GT 0>
	           <div class="form-group">
                    <div class="col-xs-12">
						<SELECT ID="frmSelectionID" CLASS="form-control" NAME="frmSelectionID">
							<OPTION VALUE="0"> Choose a Selection</OPTION>
							<CFOUTPUT QUERY="getSelections">
								<OPTION VALUE="#SelectionID#"> #HTMLEditFormat(Title)#</OPTION>
							</CFOUTPUT>
						</SELECT>
					</div>
				</div>

				<cfif frmTransferType is "Copy">
					<div class="form-group">
                        <div class="col-xs-12">
						    <input type="button" class="btn-primary btn" onClick="JavaScript:checkSelect('copy');" value="Copy">
						</div>
                    </div>
					<!--- <A HREF="JavaScript:checkSelect('copy');"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_copy_e.gif" BORDER=0></A> --->
					<div class="form-group">
						<div class="col-xs-12">
							<P class="selecttransTableP">The selection will be copied to any users selected below:
							(This gives them their own copy of the selection and data in the selection)</p>
						</div>
					</div>
				<cfelse>
					<div class="form-group">
						<div class="col-xs-12">
							<input type="button" class="btn-primary btn" onClick="JavaScript:checkSelect('move');" value="Move">
						</div>
					</div>
					<!--- <A HREF="JavaScript:checkSelect('move');"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_trans_e.gif" BORDER=0></A> --->
					<div class="form-group">
						<div class="col-xs-12">
							<P class="selecttransTableP">The ownership of the selection will be given to the user selected below:</p>
						</div>
					</div>

				</cfif> 
			<!--- 			<P>
											<INPUT TYPE="Radio" NAME="frmtransfertype" VALUE="copy" CHECKED>Copy &nbsp;&nbsp;&nbsp;
											<INPUT TYPE="Radio" NAME="frmtransfertype" VALUE="move">Transfer  --->
				<!--- 					<P>The selection will be copied/transferred to any users selected below: --->
				
				
				<CFIF getUsers.RecordCount IS 0>
					<P>There are no users who have access to any of
					the countries to which you have rights.</p>
				<CFELSE>
		                
		    		<cfset theQuery = getUsers>
			
					<!--- determine how many records per column
					this will even work if 1 record --->
					<cfset colLength = structNew()>
					<CFSET percol = theQuery.RecordCount / 3>
					<CFSET colLength[1] = Int(percol) >
					<CFSET colLength[2] = Int(percol)>
					<CFSET colLength[3] = Int(percol)>
			
					<CFIF theQuery.RecordCount MOD 3 IS 1>
						<CFSET colLength[1] = colLength[1] + 1>
					<CFELSEIF theQuery.RecordCount MOD 3 IS 2>
						<CFSET colLength[1] = colLength[1] + 1>
						<CFSET colLength[2] = colLength[2] + 1>
					</CFIF>
			
			         <div class="row">
			         	<cfset startRow = 1>
			         	<cfloop index = "column" list = "1,2,3">
			         		<cfset endRow = startRow + colLength[Column] -1>
			         		<div class="col-xs-12 col-sm-4">
			         			<!--- column one --->
			         			<CFOUTPUT QUERY="theQuery" STARTROW=#startRow# MAXROWS=#colLength[column]#>
			         				<div class="form-group">
			         					<div class="col-xs-12">
			         						<cfif frmtransfertype is "Copy">
			         							<div class="checkbox">
			         								<label><INPUT TYPE="Checkbox" NAME="frmUserID" VALUE="#htmleditformat(UserGroupID)#">#HTMLEditFormat(Name)#</label>
			         							</div>
			         						<cfelse>
			         							<div class="radio">
			         								<label><INPUT TYPE="radio" NAME="frmUserID" VALUE="#htmleditformat(UserGroupID)#">#HTMLEditFormat(Name)#</label>
			         							</div>
			         						</cfif>
			         					</div>
			         				</div>
			         			</CFOUTPUT>
			         			<cfset startRow = endRow +1>
			         		</div>
			         	</cfloop>
			         </div>

		          </CFIF>
			<CFELSE>
				<P>You have no selections.</p>
			</CFIF>
		<CFELSE>
			<P>Sorry, there is no valid User available to transfer any selections to.</p>
		</CFIF>
	<CFELSE>
		<P>Sorry, there are currently no countries to which you have access.</p>
	</CFIF>
</FORM>
</CENTER>

</FONT>

