<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:
		Permission:
SB - 26/09/2014 - Changed tables to divs
2015-11-12  ACPK    Added Bootstrap
2015-11-13  ACPK    Added missing col-xs-12 classes
---->

<CFPARAM NAME="variables.frmSelectionID" DEFAULT="0">

<!--- only those locations that are active
		could also search for locations with active people --->

<!--- lists all Country ID's to which User has rights --->
<!--- <cfinclude template="/templates/qrygetcountries.cfm"> --->

<!--- <CFIF findCountries.RecordCount GT 0> --->
		<!--- only those selection owned by the user --->

		<CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
		SELECT s.Title, s.SelectionID, s.Description, s.LastUpdated
		  FROM Selection AS s
		 WHERE s.CreatedBy = #request.relayCurrentUser.usergroupid#
		ORDER BY s.Title, s.Description, s.LastUpdated
		</CFQUERY>
<!--- </CFIF> --->

<cf_head>
<SCRIPT LANGUAGE="JavaScript">
<!--
function getUsers(form,select) {

	var chosen = select.options[select.selectedIndex];
	if (chosen.value != 0) {
		form.submit();
	} else {
		var loc = "/templates/message.cfm";
		if ((parent.selbottom.location.href + "").indexOf(loc) == -1) {
			parent.selbottom.location.href = loc;
		}
	}
	return;
}

function clearForm() {
	var form = document.chooseSelectionForm;
	var select = form.frmSelectionID;

	if (select.selectedIndex != 0) {
		select.selectedIndex = 0;
	}
	getUsers(form,select);
	// will trigger onChange
}
//-->
</SCRIPT>
</cf_head>

<CFPARAM NAME="frmruninpopup" DEFAULT="">

<CFSET application.com.request.setDocumentH1(text="Share Selections")>
<!--- <CFINCLUDE TEMPLATE="selecttophead.cfm"> --->

<div id="selectgrpframeTable1" class="col-xs-12">
 <p>You may share your selections with other users.  These users may tag/untag individuals and use the selection for data maintenance.</p>
</div>
<CFIF request.relayCurrentUser.countryList neq "">
	<CFIF getSelections.RecordCount GT 0>
		<FORM  METHOD="POST" CLASS="form-horizontal" NAME="chooseSelectionForm">
			<div id="selectgrpframeTable2">
				<div class="form-group">
					<div class="col-xs-12">
						<Cf_relayFormElement type="select" fieldname="frmSelectionID" currentValue="#variables.frmSelectionID#" query="#getSelections#" display="Title" value="SelectionID" nullText="Choose a Selection" nullValue="0" showNull="true" onChange="this.form.submit();">
						<!--- <SELECT CLASS="form-control" NAME="frmSelectionID" onChange="this.form.submit();">
							<OPTION VALUE="0" <cfif frmSelectionId eq 0>"selected"</cfif>> Choose a Selection</OPTION>
							<CFOUTPUT QUERY="getSelections">
								<OPTION VALUE="#SelectionID#"#IIf(frmSelectionID IS SelectionID,DE(" SELECTED"),DE(""))#> #HTMLEditFormat(Title)#</OPTION>
							</CFOUTPUT>
						</SELECT> --->
					</div>
				</div>
	            <!--- <div class="form-group">
	                <div class="col-xs-12">
						<input type="button" class="btn-primary btn" onClick="JavaScript:clearForm();" value="Reset">
						<!--- <A HREF="JavaScript:clearForm();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_reset_e.gif" BORDER=0></A> --->
	                </div>
	            </div> --->
			</div>
			<div id="noteSelectgrplist">
				<p>Note:  Depending on a user's rights, a user may only be able to see a portion of the shared selection.</p>
			</div>

		</div>
		</FORM>
	<CFELSE>
		<div id="selectgrpframeTable3">
			<p>You have no selections.</p>
		</div>
	</CFIF>
<CFELSE>
		<div id="selectgrpframeTable4">
			<p>Sorry, there are currently no countries to which you have access.</p>
		</div>
</CFIF>

