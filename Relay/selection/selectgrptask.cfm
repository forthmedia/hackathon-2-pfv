<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:
		Permission:
---->

<!--- WAB 2005-04-14   modifed so that can share selections with groups of users (sorry very much a quick hack) --->


<CFSET FreezeDate = request.requestTimeODBC>

<CFIF structKeyExists(FORM,"frmSelectionID")>
	<CFIF FORM.frmSelectionID IS NOT 0>

			<!--- check that the user is the owner --->
			<CFQUERY NAME="checkSelection" datasource="#application.siteDataSource#">
				SELECT Title
				  FROM Selection
				  WHERE SelectionID =  <cf_queryparam value="#FORM.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
				    AND CreatedBy = #request.relayCurrentUser.usergroupid#
			</CFQUERY>

			<CFIF checkSelection.RecordCount IS 0>
				<CFSET message="Only the owner may share the selection with users.  You are not the owner of this selection.">
				<cfoutput>#application.com.relayUI.message(message=message,type="info")#</cfoutput>
				<!--- <cfinclude template="/templates/message.cfm"> --->
				<CF_ABORT>
			</CFIF>


			<CFIF NOT structKeyExists(FORM,"frmUserID")>
				<!--- if no users are checked, delete
						userGroups except the current user's
					  Note that any people who have access (only) to countries
					  	to which the user DOES not have access will be deleted.

						This is correct since there should not be any selection individuals
						in that country anyway.  The owner should have access to all
						the individuals.
				--->
				<CFQUERY NAME="delSelectGrps" datasource="#application.siteDataSource#">
					DELETE FROM SelectionGroup
					WHERE UserGroupID <> #request.relayCurrentUser.usergroupid#
					AND SelectionID =  <cf_queryparam value="#FORM.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>

				<CFSET message = "This selection is not currently shared with anyone.">
				<cfset messageType="info">

			<CFELSE>

				<!--- 2012-07-26 PPB P-SMA001 commented out
				<cfinclude template="/templates/qrygetcountries.cfm">
				 --->
				<CFTRANSACTION>
					<!--- delete all users not checked --->
					<CFQUERY NAME="delSelectGrps" datasource="#application.siteDataSource#">
						DELETE FROM SelectionGroup
						WHERE UserGroupID <> #request.relayCurrentUser.usergroupid#
						AND SelectionID =  <cf_queryparam value="#FORM.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND UserGroupID  NOT IN ( <cf_queryparam value="#FORM.frmUserID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</CFQUERY>

					<!---- user's must have rights to the country of at least one
							of the individuals who make up the selection

							or WAB 2005-04-14 it is a usergroup
							--->

					<!--- assumes that the only personal usergroups are valid --->

<!--- 					<CFQUERY NAME="insSelectGrps" datasource="#application.siteDataSource#">
						INSERT INTO SelectionGroup (SelectionID, UserGroupID, Created, CreatedBy, LastUpdated, LastUpdatedBy)
						SELECT DISTINCT #FORM.frmSelectionID#,
							   ug.UserGroupID,
								#FreezeDate#,
								#request.relayCurrentUser.usergroupid#,
								#FreezeDate#,
								#request.relayCurrentUser.usergroupid#
						  FROM UserGroup AS ug, SelectionTag AS st,
						  		Person AS p,Location AS l,
								Rights AS r, RightsGroup AS rg
						WHERE ug.UserGroupID <> #request.relayCurrentUser.usergroupid#
						AND ug.UserGroupID IN (#FORM.frmUserID#)
						<!--- exclude UGs already there --->
						AND ug.UserGroupID NOT IN (SELECT tsg.UserGroupID
						                             FROM SelectionGroup AS tsg
													WHERE tsg.SelectionID = #FORM.frmSelectionID#
												   )
						AND st.SelectionID = #FORM.frmSelectionID#
						AND st.EntityID = p.PersonID
						AND p.LocationID = l.LocationID
						AND l.CountryID = r.CountryID
						AND l.countryID IN (#Variables.CountryList#)  <!--- this should be the case, but check anyway --->
						AND r.UserGroupID = rg.UserGroupID
						AND rg.PersonID = ug.PersonID
					    AND r.SecurityTypeID =
							(SELECT e.SecurityTypeID
							   FROM SecurityType AS e
							  WHERE e.ShortName = 'RecordTask')
					</CFQUERY> --->
					<CFQUERY NAME="insSelectGrps" datasource="#application.siteDataSource#">
					INSERT INTO SelectionGroup (SelectionID, UserGroupID, Created, CreatedBy, LastUpdated, LastUpdatedBy)
						SELECT DISTINCT <cf_queryparam value="#FORM.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							   ug.UserGroupID,
								<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
								<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
								<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
								<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >
						  FROM UserGroup AS ug
						WHERE ug.UserGroupID <> #request.relayCurrentUser.usergroupid#
						AND ug.UserGroupID  IN ( <cf_queryparam value="#FORM.frmUserID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						<!--- exclude UGs already there --->
						AND ug.UserGroupID NOT IN (SELECT tsg.UserGroupID
						                             FROM SelectionGroup AS tsg
													WHERE tsg.SelectionID =  <cf_queryparam value="#FORM.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
												   )
						AND (EXISTS (select 2 FROM SelectionTag AS st,
							  		Person AS p,Location AS l,
									Rights AS r, RightsGroup AS rg
									WHERE st.SelectionID =  <cf_queryparam value="#FORM.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
									AND st.EntityID = p.PersonID
									AND p.LocationID = l.LocationID
									AND l.CountryID = r.CountryID
									<!--- 2012-07-26 PPB P-SMA001 commented out
									AND l.countryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  <!--- this should be the case, but check anyway --->
									 --->
									#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->

									AND r.UserGroupID = rg.UserGroupID
			 						AND rg.PersonID = ug.PersonID
								    AND r.SecurityTypeID =
										(SELECT e.SecurityTypeID
										   FROM SecurityType AS e
										  WHERE e.ShortName = 'RecordTask'))
								or ug.personid is null  <!--- WAB 2005-04-14 if the usergroup is a group of users (rather than a person) then we don't worry about   --->
							)


				</CFQUERY>
				</CFTRANSACTION>

				<CFSET message = "The list of users who can share this selection has been updated.">
				<cfset messageType = "success">
			</CFIF>
	<CFELSE>
		<CFSET message = "You have not chosen a selection">
		<cfset messageType = "info">
	</CFIF>
<CFELSE>
	<CFSET message = "You have not chosen a selection">
	<cfset messageType = "info">
</CFIF>
<!---


<CFINCLUDE TEMPLATE="selectgrpindiv.cfm">
<CF_ABORT> --->
