<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Code to put a drop down box on the location data screen

		WAB 1999-02-11

Amendment History
		WAB 1999-04-16 removed 3comu specific item
		WAB 1999-02-17	Have added a check all and uncheck all functionality.
					Rejigged java script
2001-03-22		WAB		added items to dropdowns and at same time standardised on
					the 'checkboxFunctions' javascript
					made some mods to form name and things
2011-10-12	WAB		LID 7233 added _cf_nodebug=true to many items - improve performance
2015-11-12  ACPK    Added Bootstrap
			 --->
<!--- set variable EventManager to specifiy if the user
has been given explicit rights to this event --->

<!--- KT 1999-11-25 added (again?) to set event manager variable --->
<!--- 2011-07-19 PPB LEN024 added permissions to Flagging menu option --->

<CFQUERY NAME="GetRecordRights" datasource="#application.siteDataSource#">
	SELECT rr.RecordID
	FROM RecordRights AS rr, EventDetail AS ed
	WHERE rr.Entity = 'Flag'
	AND rr.UserGroupID =  <cf_queryparam value="#ListGetAt(Cookie.user, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND rr.RecordID = ed.FlagID
</CFQUERY>
<CFSET EventManager = 0>
<CFIF GetRecordRights.RecordCount GT 0>
	<CFSET EventManager = 1>
</CFIF>



	<cf_includejavascriptonce template = "/javascript/checkboxFunctions.js">
	<SCRIPT type="text/javascript">
<!--
// original code by Bill Trefzger 1996-12-12
function go()	{


	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none")
		{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value
		document.selecter.select1.selectedIndex=0     //resets menu to top item
		}
}

	function openWin( windowURL,  windowName, windowFeatures ) {

		return window.open( windowURL, windowName, windowFeatures ) ;
	}


//-->
</SCRIPT>
<cf_translate>
<form class="form-horizontal" name="selecter">
	<div class="form-group">
	   <div class="col-xs-12">
			<label for="select1">Select a function</label>
			<select class="form-control" id="select1" name="select1" size=1 onchange="go()">
				<CFOUTPUT>
					<option value=none>Selection Functions</option>
			       <!--- 	<option value="JavaScript: if (saveChecks()!='0') { newWindow = openWin( '../selection/selectalter.cfm?frmpersonids='+saveChecks()+'&frmtask=add&frmruninpopup=true&frmselectiontitle=3ComUP_-_Invitee_List' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks()}">Add checked people to '3ComUP - Invitee List' Selection --->
					<optGroup label="Selections">
					<option value="JavaScript: checks = saveChecks('Person');  if (checks!='0') { newWindow = openWin( '/selection/selectalter.cfm?frmpersonids='+checks +'&frmtask=add&frmruninpopup=true' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('Person')}">&nbsp;Add checked people to a selection</option>
					<option value="JavaScript: checks = saveChecks('Person');  if (checks!='0') { newWindow = openWin( '/selection/selectalter.cfm?frmpersonids='+checks +'&frmtask=remove&frmruninpopup=true' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('Person')}">&nbsp;Remove checked people from a selection</option>
					<option value="JavaScript: checks = saveChecks('Person');  if (checks!='0') { newWindow = openWin( '/selection/selectalter.cfm?frmpersonids='+checks +'&frmtask=save&frmruninpopup=true','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('Person')}">&nbsp;Save checked people as new selection</option>
					</optGroup>
					<optGroup label="Mail and Download">
					<!--- <cfif isDefined("session.securityLevels") and listFindNoCase(session.securityLevels,"UserTask(1)") neq 0> --->
					<cfif application.com.login.checkInternalPermissions("UserTask","Level1")>
		    			<option value="JavaScript: checks = saveChecks('Person');if (checks!='0') { newWindow = openWin( '/utilities/createUserNamePassword.cfm?_cf_nodebug=true&frmPersonid='+checks,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'); newWindow.focus() } else {noChecks('Person')}">&nbsp;Create Username and Password for checked people</option>
					</cfif>
					<!--- <cfif isDefined("session.securityLevels") and listFindNoCase(session.securityLevels,"CommTask(7)") neq 0> --->
					<!--- PJP 13/11/2012: CASE 427937 : 'Removed send checked people a message' ---->
					<!---<cfif application.com.login.checkInternalPermissions("CommTask","Level3")>
					<option value="JavaScript: checks = saveChecks('Person');  if (checks!='0') { newWindow = openWin( '/communicate/commheader.cfm?_cf_nodebug=true&frmpersonids='+checks +'&frmselectid=0&frmruninpopup=true&qrycommformlid=2','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('Person')}">&nbsp;Send checked people a message
					</cfif>--->

					<option value="JavaScript: checks = saveChecks('Person');  if (checks!='0')  { newWindow = openWin( '/download/downheader.cfm?_cf_nodebug=true&frmpersonids='+checks +'&frmselectid=0&frmruninpopup=true&qrycommformlid=4','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('Person')}">&nbsp;Download checked people</option>
					</optGroup>
					<cfif application.com.login.checkInternalPermissions("ProfileTask","Level2")>		<!--- 2011-07-19 PPB LEN024 --->
					     <optGroup label="Flagging">
					     <option value="JavaScript: 	checks = saveChecks('Person');  if (checks!='0') { newWindow = openWin( '/flags/SetFlagsForIDList.cfm?_cf_nodebug=true&frmFlagEntityTypeID=0&frmEntityTypeID=0&frmEntityID=' + checks  ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus();  } else {noChecks('Person')}">&nbsp;Set Flags for checked people</option>
					     </optGroup>
					</cfif>
					<CFIF EventManager><optGroup label="Events"><option value="JavaScript: checks = saveChecks('Person');  if (checks!='0') { newWindow = openWin( '/events/eventAddPopup.cfm?frmpersonids='+checks +'&frmruninpopup=true','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('Person')}">&nbsp;Add checked people to an event</option></optGroup></CFIF>
				</CFOUTPUT>
			</select>
		</div>
	</div>
</form>
</cf_translate>


