<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Code to put a drop down box on the selection list screen

		WAB 1999-04-19

Amendment History
		WAB 1999-04-19 moved the delete selections function from a button to this menu
					added a function to create a selection of blank emails

2001-03-22		WAB		added items to dropdowns and at same time standardised on the 'checkboxFunctions' javascript
					made some mods to form name and things
2010-02-12	NJH		LID 3073 - submit the delete form to checkEntityForDeletion rather than updateFlag.cfm to take advantage of checks
					that are run against people before they are deleted.
					Also added a confirm when deleting people. This meant having to create a deletePeople function.
2011-07-19 	PPB 	LEN024 added permissions to Flagging menu option
2011-10-12	WAB		LID 7233 added _cf_nodebug=true to many items - improve performance
2011-10-25  NYB		LHID7095,7963 - added Delete Organisations in Checked Selections
2011-12-13  NYB		add initial setting of userHasOrganisationDeleteRights
2015-11-13  ACPK    Added Bootstrap to selector; added missing </option> tags
2016/01/26	NJH		Changed the way that the query was passed to checkentityDeletion.cfm. Added another form attribute to say what type frmEntityID was. If a selection, then we build the query string in checkEntityDeletion rather than passing it through.
			 --->



<cf_includejavascriptonce template = "/javascript/checkboxFunctions.js">

<!--- START: 2011-10-25 NYB LHID7963 replaced query with: --->
<cfset userHasPersonDeleteRights = application.com.rights.getPersonDeleteRights()>
<!--- 2011-12-13 NYB add: --->
<cfset userHasOrganisationDeleteRights = "false">

<cfif userHasPersonDeleteRights and application.com.rights.getLocationDeleteRights()>
	<cfset userHasOrganisationDeleteRights = application.com.rights.getOrganisationDeleteRights()>
</cfif>
<!--- END: 2011-10-25 NYB LHID7963 --->

<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
// original code by Bill Trefzger 1996-12-12
function go()	{


	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none")
		{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value;
		document.selecter.select1.selectedIndex=0;     //resets menu to top item
		}
}

	function openWin( windowURL,  windowName, windowFeatures ) {

		return window.open( windowURL, windowName, windowFeatures ) ;
	}

	function noChecks(){
				alert('You have not checked any people')
				document.selecter.select1.selectedIndex=0
				return
	}

	// NJH 2010-02-12 LID 3073 - added delete function below.
	// NYB 2011-10-25 LHID7963 replaced if condition
	<CFIF userHasPersonDeleteRights>
		function deletePeople () {
	  		checks = saveChecks('Selection');
	  		if (checks!='0') {
				checksArray = checks.split(',');
				if (checksArray.length == 1) {
					confirmMsg = 'Are you sure you want to delete the people in this selection?';
				} else {
					confirmMsg = 'Are you sure you want to delete the people in these selections?';
				}
	  			if (confirm(confirmMsg)) {
				    newWindow = openWin( '','PopUp','width=850,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				    newWindow.focus();
				    <cfoutput>
				    window.document.deletePeopleForm.frmEntityId.value=checks;
				    </cfoutput>
				    window.document.deletePeopleForm.submit()
				} else {
				}
			} else {
				noChecks('Selection')
			}
		}
	</cfif>

	// NJH 2011-10-25 LHID7963 - added delete function below.
	<CFIF userHasOrganisationDeleteRights>
		function deleteOrganisations () {
	  		checks = saveChecks('Selection');
	  		if (checks!='0') {
				checksArray = checks.split(',');
				if (checksArray.length == 1) {
					confirmMsg = 'Are you sure you want to delete the organisations in this selection?';
				} else {
					confirmMsg = 'Are you sure you want to delete the organisations in these selections?';
				}
	  			if (confirm(confirmMsg)) {
				    newWindow = openWin( '','PopUp','width=850,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				    newWindow.focus();
				    <cfoutput>
				    window.document.deleteOrganisationForm.frmEntityId.value=checks;
				    </cfoutput>
				    window.document.deleteOrganisationForm.submit()
				} else {
				}
			} else {
				noChecks('Selection')
			}
		}
	</cfif>

//-->
</SCRIPT>
<!--- 2015-11-12    ACPK    Added Bootstrap to selector; added missing </option> tags --->
<form name="selecter" class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-6">
		   <label for="select1">Select a function</label>
	       <select id="select1" name="select1" size=1 onchange="go()" class="form-control">
				<CFOUTPUT>
					<option value=none>Selection Functions</option>
				    <!--- 	<option value="JavaScript: if (saveChecks()!='0') { newWindow = openWin( '/selection/selectalter.cfm?frmpersonids='+saveChecks()+'&frmtask=add&frmruninpopup=true&frmselectiontitle=3ComUP_-_Invitee_List' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks()}">Add checked people to '3ComUP - Invitee List' Selection --->
					<optGroup label="Selections">
					<option value="JavaScript: if (saveChecks('Selection') != '0') {document.mainForm.submit() } else {noChecks('Selection')} ">&nbsp;Delete Tagged Selections</option>
					<!--- bit for getting bad emails out of selections --->
					<CFSET querystring="select personid from person where email not like \'%@%.%\' and personid in (select entityid from selectiontag as st where st.selectionid in (">
					<option value="JavaScript: 	checks = saveChecks('Selection');  if (checks!='0') { newWindow = openWin( '' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus(); window.document.badEmailForm.frmPersonIds.value='#querystring#' + checks +'))';  window.document.badEmailForm.submit()} else {noChecks('Selection')}">&nbsp;Create Selection of Invalid Emails in Checked Selections</option>
					<!--- <cfif isDefined("session.securityLevels") and listFindNoCase(session.securityLevels,"UserTask(1)") neq 0> --->
					<cfif application.com.login.checkInternalPermissions("UserTask","Level1")>
					   <option value="JavaScript:  checks = saveChecks('Selection');  if (checks!='0') { newWindow = openWin( '/utilities/createUserNamePassword.cfm?_cf_nodebug=true&frmSelectionID='+checks,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'); newWindow.focus() } else {noChecks('Person')}">&nbsp;Create Username and Password for People in checked selections</option>
					</cfif>
					</optGroup>
					<cfif application.com.login.checkInternalPermissions("ProfileTask","Level2")>		<!--- 2011-07-19 PPB LEN024 --->
						<optGroup label="Flagging">
						<option value="JavaScript: 	checks = saveChecks('Selection');  if (checks!='0') { newWindow = openWin( '/flags/flagaSelection.cfm?_cf_nodebug=true&frmFlagEntityTypeID=0&frmSelectionID=' + checks  ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus();  } else {noChecks('Selection')}">&nbsp;Set Flags for People in checked selections</option>
						<option value="JavaScript: 	checks = saveChecks('Selection');  if (checks!='0') { newWindow = openWin( '/flags/flagaSelection.cfm?_cf_nodebug=true&frmFlagEntityTypeID=1&frmSelectionID=' + checks  ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus();  } else {noChecks('Selection')}">&nbsp;Set Flags for Locations in checked selections</option>
						<option value="JavaScript: 	checks = saveChecks('Selection');  if (checks!='0') { newWindow = openWin( '/flags/flagaSelection.cfm?_cf_nodebug=true&frmFlagEntityTypeID=2&frmSelectionID=' + checks  ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus();  } else {noChecks('Selection')}">&nbsp;Set Flags for Organisations in checked selections</option>
						</optGroup>
					</cfif>

					<!--- START: 2011-10-25 NYB LHID7963 changed if condition, changed text to a phrase, added DeleteOrganisationInselections option: --->
					<cfif userHasPersonDeleteRights or userHasOrganisationDeleteRights>
					<optGroup label="Utilities">
					<CFIF userHasPersonDeleteRights>
						<!--- <CFSET querystring="select entityid from selectiontag as st where st.status <>0 and st.selectionid in (">
						<option value="JavaScript: checks = saveChecks('Selection'); if (checks!='0') { newWindow = openWin( '','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'); newWindow.focus(); window.document.deletePeopleForm.frmEntityId.value='#querystring#' + checks +')'; window.document.deletePeopleForm.submit(); } else {noChecks('Selection')}">&nbsp;Delete All People in Checked Selections</option> --->
						<option value="javascript:deletePeople();">&nbsp;Phr_sys_selections_DeletePeopleInSelections</option>
					</cfif>
					<CFIF userHasOrganisationDeleteRights>
						<option value="javascript:deleteOrganisations();">&nbsp;Phr_sys_selections_DeleteOrganisationsInSelections</option>
					</cfif>
					</optGroup>
					</cfif>
					<!--- END: 2011-10-25 NYB LHID7963 --->
				</CFOUTPUT>
			</select>
		</div>
	</div>
</form>


<FORM ACTION="/selection/selectalter.cfm" METHOD="POST" NAME="badEmailForm" target="PopUp">
<INPUT TYPE="Hidden" Name="frmPersonIds" Value="" >
<INPUT TYPE="Hidden" Name="frmtask" Value="save" >
<INPUT TYPE="Hidden" Name="frmruninpopup" Value="true" >
<INPUT TYPE="Hidden" Name="badEmails" Value="true" >
</form>

<!--- NJH 2010-02-12 LID 3070 change form action from "../flags/updateflag.cfm" to "/data/checkEntityForDeletion.cfm" --->
<!--- 2011-10-25 NYB LHID7963 added if around delete form --->
<CFIF userHasPersonDeleteRights>
	<FORM ACTION="/data/checkEntityForDeletion.cfm" METHOD="POST" NAME="deletePeopleForm" target="PopUp">
		<INPUT TYPE="Hidden" Name="frmEntityId" Value="" >
		<INPUT TYPE="Hidden" Name="frmflagtextid" Value="DeletePerson" >
		<INPUT TYPE="Hidden" Name="frmshowfeedback" Value="" >
		<INPUT TYPE="Hidden" Name="frmflagvalue" Value="1" >
		<INPUT TYPE="Hidden" Name="frmruninpopup" Value="true" >
		<INPUT TYPE="Hidden" Name="deleteEntityIDType" Value="personSelection" >
	</form>
</CFIF>

<!--- START: 2011-10-25 NYB LHID7963 added: --->
<CFIF userHasOrganisationDeleteRights>
	<FORM ACTION="/data/checkEntityForDeletion.cfm" METHOD="POST" NAME="deleteOrganisationForm" target="PopUp">
		<INPUT TYPE="Hidden" Name="frmEntityId" Value="" >
		<INPUT TYPE="Hidden" Name="frmflagtextid" Value="DeleteOrganisation" >
		<INPUT TYPE="Hidden" Name="frmshowfeedback" Value="" >
		<INPUT TYPE="Hidden" Name="frmflagvalue" Value="1" >
		<INPUT TYPE="Hidden" Name="frmruninpopup" Value="true" >
		<INPUT TYPE="Hidden" Name="deleteEntityIDType" Value="organisationSelection" >
	</form>
</CFIF>
<!--- END: 2011-10-25 NYB LHID7963 --->




