<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">
<!--- using this date will prevent the code from rolling over but will not
		allow the previous page button to work correctly
		since this date will obviously not be the one in SelectionTag --->

<CFQUERY NAME="getPersonInfo" datasource="#application.siteDataSource#">
	SELECT	c.PersonID,
			c.Salutation,
			c.FirstName,
			c.LastName,
			c.OfficePhone AS Pphone,
			c.FaxPhone AS Pfax,
			c.Email,
			e.OrganisationName,
			d.Address1,
			d.Address2,
			d.Address3,
			d.Address4,
			d.Address5,
			d.PostalCode,
			d.Telephone AS Cphone,
			d.Fax AS Cfax,
			d.CountryID,
			co.CountryDescription
	FROM  Country AS co,
		 Person AS c, Location AS d, Organisation AS e
	WHERE c.PersonID =  <cf_queryparam value="#frmToView#" CFSQLTYPE="CF_SQL_INTEGER" >
  	  AND c.active <> 0 <!--- recordview.cfm --->
	  AND c.OrganisationID = e.OrganisationID
	  AND c.LocationID = d.LocationID
	  AND d.CountryID = co.CountryID
</CFQUERY>

<!--- force countryID to 0 if it doesn't exist
		so getRights will run even if getPersonInfo
		record count is 0
--->

<CFSET qryCountryID = 0>
<CFIF #getPersonInfo.RecordCount# IS 1>
	<CFSET qryCountryID = getPersonInfo.CountryID>
</CFIF>

<CFQUERY NAME="getRights" datasource="#application.siteDataSource#">
	<!--- WAB 2012-07-06 convert to use vCountryRights--->
	SELECT  level1 AS readperm,
			level2 AS writeperm,
			level3 AS addperm
	FROM  vCountryRights
	WHERE PersonID = #request.relayCurrentUser.personid#
	  AND CountryID =  <cf_queryparam value="#qryCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>



<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<!--- <META HTTP-EQUIV="Expires" CONTENT="Tue, 6 Aug 1996 12:00:00 GMT"> --->
</cf_head>
<FONT FACE="Arial, Chicago, Helvetica" SIZE="2">

<!--- <CFINCLUDE TEMPLATE="selecttophead.cfm"> --->
<CENTER>

<CFIF #getPersonInfo.RecordCount# IS 0>
	<BR>
	<P>The full record could not be found.

<CFELSEIF #getRights.readperm# IS 0 OR #getRights.readperm# IS "">
	<BR>
	<P>You do not have permission to view the requested record.

<CFELSE>

	<FORM NAME="SelectionTagForm" ACTION="selectreview.cfm?mode=debug" METHOD="POST">

	<CF_INPUT TYPE="HIDDEN" NAME="frmSearchID" VALUE="#FORM.frmSearchID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#FORM.frmStart#">

	<INPUT TYPE="Image" NAME="PreviousPage" SRC="/images/buttons/c_back_e.gif" WIDTH=63 HEIGHT=22 BORDER=0 ALT="Back to Selection">


	<CFOUTPUT QUERY="getPersonInfo" MAXROWS=1>

	<!-- start Partner Detail Table -->
	<P>
	<BR><TABLE BORDER="0" CELLPADDING="5" CELLSPACING="0">
	<TR>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2"><B>Name:</B></FONT></TD>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2"><B>#HTMLEditFormat(Salutation)# #HTMLEditFormat(FirstName)# #HTMLEditFormat(LastName)#</B></FONT></TD>
	</TR>
	<TR>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">Company:</FONT></TD>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">#HTMLEditFormat(OrganisationName)#</FONT></TD>
	</TR>
	<TR>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2">Direct Telephone:</FONT></TD>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #Trim(Pphone)# IS "">n/a<CFELSE>#htmleditformat(Pphone)#</CFIF></FONT></TD>
	</TR>
	<TR>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">Direct Fax:</FONT></TD>
		<TD><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #Trim(Pfax)# IS "">n/a<CFELSE>#htmleditformat(Pfax)#</CFIF></FONT></TD>
	</TR>
	<TR>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2">Email:</FONT></TD>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #Trim(Email)# IS "">n/a<CFELSE><A HREF="mailto:#Email#">#htmleditformat(Email)#</A></CFIF></FONT></TD>
	</TR>

	<TR>
		<TD VALIGN="TOP"><FONT FACE="Arial,Chicago" SIZE="2">Address:</FONT></TD>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">
			<CFIF #Trim(Address1)# IS "">
				n/a
			<CFELSE>
				#HTMLEditFormat(Address1)#
				<CFIF #Trim(Address2)# IS NOT ""><BR>#HTMLEditFormat(Address2)#</CFIF>
				<CFIF #Trim(Address3)# IS NOT ""><BR>#HTMLEditFormat(Address3)#</CFIF>
				<CFIF #Trim(Address4)# IS NOT ""><BR>#HTMLEditFormat(Address4)#</CFIF>
				<CFIF #Trim(Address5)# IS NOT ""><BR>#HTMLEditFormat(Address5)#</CFIF>
				<CFIF #Trim(CountryDescription)# IS NOT ""><BR>#HTMLEditFormat(CountryDescription)#</CFIF>
				<CFIF #Trim(PostalCode)# IS NOT ""><BR>#HTMLEditFormat(PostalCode)#</CFIF>
			</CFIF>
		</FONT></TD>
	</TR>
	<TR>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2">Company Telephone:</FONT></TD>
		<TD BGCOLOR="##FFFFCE"><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #Trim(Cphone)# IS "">n/a<CFELSE>#htmleditformat(Cphone)#</CFIF></FONT></TD>
	</TR>
	<TR>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">Company Fax:</FONT></TD>
		<TD><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #Trim(Cfax)# IS "">n/a<CFELSE>#htmleditformat(Cfax)#</CFIF></FONT></TD>
	</TR>
	<!--- <TR>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">Email</FONT></TD>
		<TD><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #Trim(EMail)# IS "">n/a<CFELSE><A HREF="mailto:#EMail#">#EMail#</A></CFIF></FONT></TD>
	</TR> --->

	</TABLE><BR><BR>

	</CFOUTPUT>

	</FORM>

	<CFIF #getRights.writeperm# GT 0>
		<FORM NAME="FormEditRecords" ACTION="JavaScript:alert('This functionality is not yet fully tested.');" METHOD="POST">
			<!--- pass all this info so after the edit we could
					possibly return to the selections --->
			<INPUT TYPE="HIDDEN" NAME="frmFrom" VALUE="/selection/selectreview.cfm">
			<CF_INPUT TYPE="HIDDEN" NAME="frmSearchID" VALUE="#FORM.frmSearchID#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#FORM.frmStart#">
			<!--- if we want to go back to recordView,
			<INPUT TYPE="HIDDEN" NAME="frmToView" VALUE="0"> --->
			<!--- <INPUT TYPE="Image" NAME="Edit" SRC="../IMAGES/BUTTONS/C_edit_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Edit the Record"> --->
		</FORM>
	</CFIF>


</CFIF>




</CENTER>

</FONT>
<P>



