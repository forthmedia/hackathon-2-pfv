<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	SelectAlterTask.cfm  WAB 1999-02-11
		adds or removes people from a selection  --->

<!---  variables passed to form:
		frmpersonids: list of personids (in theory can be in the form of a select statement)
		frmselectid: selection to alter
		frmtask:	'add' or 'remove'
		--->

<!--- amendment history
1999-05-19 WAB added #preservesinglequotes(frmpersonids)# to handle sql statements better

WAB 2004-09-21  Added counts on location and organisation to the selectionTable
WAB 2005-09-26	altered so that a manually added person has status 2.
2012-12-13 PPB Case 432559 add allowSelectStatement to cf_queryParam for frmpersonids
2015-11-13  ACPK    Replaced tables with Bootstrap
 --->		

<SCRIPT LANGUAGE="JavaScript">
<!--
	function closewindow(){
		window.close()
		}
//-->
</SCRIPT>

<CFIF not isdefined("frmnofeedback") and  not isDefined("frmRuninPopup")><!--- WAB 2005-11-07 changed from Or to AND --->
	<CFINCLUDE TEMPLATE="selecttophead.cfm">
</CFIF>

<CFIF NOT (#checkPermission.addOkay# GT 0 OR #checkPermission.updateOkay# GT 0)>		
	<!--- don't have permissions to modifiy --->
	<CFSET message = "Sorry, you do not currently have permission to save/update selections.">
	<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
	<!--- <cfinclude template="/templates/message.cfm"> --->
	<CF_ABORT>
</CFIF>
		
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">		
		
<CFIF frmtask IS "add">
	<CFLOOP index="selectid" list=#frmselectid#>
	
		<CFQUERY name="getSelection" datasource="#application.siteDataSource#">
		SELECT Title,RecordCount,SelectedCount
		FROM Selection
		WHERE Selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	
		<CFIF getSelection.SelectedCount IS "">
			<CFQUERY name="getSelectionTags" datasource="#application.siteDataSource#">
				SELECT Count(EntityID) AS calcSelectedCount
				FROM SelectionTag
				WHERE Selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND Status <> 0
			</CFQUERY>
			<CFSET theSelectedCount=getSelectionTags.calcSelectedCount>
		<CFELSE>
			<CFSET theSelectedCount=getSelection.SelectedCount>
		</CFIF>
		
<!--- had problems with counts taking too long if too many people, test for too many and display and appropriate message --->
		<CFQUERY NAME="PeopleCount" datasource="#application.siteDataSource#">
			SELECT count(1) as peoplechecked
			FROM Person
			WHERE Personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)		<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
		</CFQUERY>
		
<CFIF PeopleCount.peoplechecked GT 500>
		<CFQUERY NAME="Counts" datasource="#application.siteDataSource#">
		SELECT #PeopleCount.peoplechecked#	 AS peoplechecked,
				(SELECT	Count(1)	
						FROM Person as p
						WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)				<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						AND p.personid not IN(select entityid from selectiontag where selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > )) 
						AS Peopletoadd ,
				'Not Calculated'		AS PeopletoUpdate,
				'Not Calculated'		AS Peoplenoaction,
				'Not Calculated'		AS PeopleInactive 
		</CFQUERY>

		<!--- set the new RecordCount and SelectedCount for the selection --->
		<CFSET newRecordCount = getSelection.RecordCount + counts.PeopleToAdd>
		<CFSET newSelectedCount = variables.theSelectedCount + counts.PeopleToAdd >  <!--- should add in, but not calculatingit to save some time on the query + counts.PeopleToUpdate --->  


<cfelse>


		<CFQUERY NAME="Counts" datasource="#application.siteDataSource#">
		SELECT 	(SELECT Count(1) 
						FROM Person
						WHERE Personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">))			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						AS peoplechecked,
				(SELECT	Count(1)	
						FROM Person as p
						WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->			
						AND p.personid not IN(select entityid from selectiontag where selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > )) 
						AS Peopletoadd ,
				(SELECT Count(1)
						FROM Selectiontag as st
						WHERE st.selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						AND  st.entityid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						AND st.status=0)
						AS PeopletoUpdate,
				(SELECT Count(1)
						FROM Selectiontag as st
						WHERE st.selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						AND  st.entityid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						AND st.status<>0)
						AS Peoplenoaction,
				(SELECT  Count(1)
						FROM Person as p
						WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						AND p.active = 0) 
						AS PeopleInactive 
				FROM dual
		</CFQUERY>


		<!--- set the new RecordCount and SelectedCount for the selection --->
		<CFSET newRecordCount = getSelection.RecordCount + counts.PeopleToAdd>
		<CFSET newSelectedCount = variables.theSelectedCount + counts.PeopleToAdd + counts.PeopleToUpdate>


</CFIF>	
		<!---  add entities which don't exist in selection  --->	
		<CFQUERY name="addperson" datasource="#application.siteDataSource#">
			INSERT INTO SelectionTag (SelectionID,EntityID,Status,CreatedBy,Created,LastUpdatedBy,LastUpdated) 
				 SELECT <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" >,	
	 					p.personid,	
						2,	   <!--- 2 indicates a manually added person  --->
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,	
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,		
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					FROM person as p
					WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
					AND p.personid not IN(select entityid from selectiontag where selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>

		<!---  update status where entity exists but is unselected - --->
		<CFQUERY name="checkperson" datasource="#application.siteDataSource#">
			UPDATE  SelectionTag
			SET Status=1
			WHERE selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND  entityid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)				<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
			AND status=0
		</cfquery>
		


<!--- <CFOUTPUT>
newSelectedCount = #newSelectedCount#<BR>
theSelectedCount = #theSelectedCount#<BR>
counts.PeopleToAdd = #counts.PeopleToAdd#<BR>
counts.PeopleToUpdate = #counts.PeopleToUpdate#<P>
</CFOUTPUT>
<CF_ABORT>
 --->		

<!--- WAB 2004-09-21 
Added columns to count locations and organisation to the selection table
need to update them here
The query included here probably duplicates some of the stuff above,
so I am no longer using newRecordCOunt and newSelectedCount (newRecordCOunt seemed to give the wrong answer sometimes anyway!)
but didn't really want to get into reworking the whole thind (sorry!)
--->

		<cfset frmSelectionID = selectid>
		<cfinclude template = "..\templates\qrySelectionRecordCounts.cfm">

		<CFQUERY name="UpdateSelectionDate" datasource="#application.siteDataSource#">
			UPDATE  Selection
			SET lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				RecordCount =  <cf_queryparam value="#recordCounts.AllPeopleSelectedorUnselected#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCount =  <cf_queryparam value="#recordCounts.people#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCountLoc =  <cf_queryparam value="#recordCounts.locations#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCountOrg =  <cf_queryparam value="#recordCounts.organisations#" CFSQLTYPE="CF_SQL_Integer" > 
			WHERE selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>

		<CFIF not isdefined("frmnofeedback")>
			<CFINCLUDE template="selectalterresults.cfm">
		</CFIF>
	
	</cfloop>

	<CFIF not isdefined("frmnofeedback")>
        <div class="col-xs-12">
			<A HREF="JavaScript: closewindow();">Close window</A>
        </div>
	</CFIF>		

<cfelseif frmtask IS "remove">

	<CFLOOP index="selectid" list=#frmselectid#>

		<CFQUERY name="getSelection" datasource="#application.siteDataSource#">
		SELECT Title,RecordCount,SelectedCount
		FROM Selection
		WHERE Selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		<CFIF getSelection.SelectedCount IS "">
			<CFQUERY name="getSelectionTags" datasource="#application.siteDataSource#">
				SELECT Count(EntityID) AS calcSelectedCount
				FROM SelectionTag
				WHERE Selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND Status <> 0
			</CFQUERY>
			<CFSET theSelectedCount=getSelectionTags.calcSelectedCount>
		<CFELSE>
			<CFSET theSelectedCount=getSelection.SelectedCount>
		</CFIF>

		<cfquery NAME="Counts" datasource="#application.siteDataSource#">
		SELECT	(SELECT Count(1) 
						FROM Person
						WHERE Personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">))			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						as peoplechecked,
				(SELECT COUNT(1)
						FROM SelectionTag as st
						WHERE st.selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						AND st.entityid IN (<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						and st.status <>0)
						as PeopletoRemove,
				(SELECT COUNT(1)
						FROM SelectionTag as st
						WHERE st.selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						AND st.entityid IN (<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
						and st.status =0)
						as Peoplealreadyunchecked,
				(SELECT	Count(1)	
						FROM Person as p
						WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->		
						AND p.personid not IN(select entityid from selectiontag where selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > )) 
						AS Peoplenotinselection,
				(SELECT  Count(1)
						FROM Person as p
						WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->	
						AND p.active = 0) 
						AS PeopleInactive 
				FROM Dual
		</cfquery>
	
		<CFQUERY name="uncheckperson" datasource="#application.siteDataSource#">
			UPDATE  SelectionTag 
			SET Status=0
			WHERE selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND entityid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)			<!--- 2012-12-13 PPB Case 432559 add allowSelectStatement --->
		</cfquery>
	
		<!--- set the new SelectedCount for the selection --->
		<CFSET newSelectedCount = variables.theSelectedCount - counts.PeopleToRemove>

		<!--- WAB 2004-09-21 
		Added columns to count locations and organisation to the selection table
		need to update them here
		The query included here probably duplicates some of the stuff above, but didn't really want to get into reworking the whole thind (sorry!)
		--->

		<cfset frmSelectionID = selectid>
		<cfinclude template = "..\templates\qrySelectionRecordCounts.cfm">

		<CFQUERY name="UpdateSelectionDate" datasource="#application.siteDataSource#">
			UPDATE  Selection
			SET lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				RecordCount =  <cf_queryparam value="#recordCounts.AllPeopleSelectedorUnselected#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCount =  <cf_queryparam value="#recordCounts.people#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCountLoc =  <cf_queryparam value="#recordCounts.locations#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCountOrg =  <cf_queryparam value="#recordCounts.organisations#" CFSQLTYPE="CF_SQL_Integer" > 
			WHERE selectionid =  <cf_queryparam value="#selectid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>

		<CFIF not isdefined("frmnofeedback")>
			<CFINCLUDE template="selectalterresults.cfm">
		</CFIF>

	</CFLOOP>

	<CFIF not isdefined("frmnofeedback")>
		<div class="col-xs-12">
			<P><A HREF="JavaScript: closewindow();">Close window</A></P>
		</div>
	</CFIF>


<cfelseif frmtask IS "save">

	<cfinclude template="/templates/qrysaveselection.cfm">

	<cfquery name="getSelection" datasource="#application.siteDataSource#">
	SELECT Title
	FROM Selection
	WHERE Selectionid =  <cf_queryparam value="#searchid#" CFSQLTYPE="CF_SQL_INTEGER" >    <!--- this variable returned by qrysaveselection.cfm ---></cfquery>

	<CFQUERY NAME="Counts" datasource="#application.siteDataSource#">
	SELECT	(SELECT Count(1) 
					FROM Person
					WHERE Personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">))	
					as peoplechecked
			FROM Dual
	</CFQUERY>

	<CFINCLUDE template="selectalterresults.cfm">
	<div class="col-xs-12">
    	<P><A HREF="JavaScript: closewindow();">Close window</A></P>
    </div>
</CFIF>
