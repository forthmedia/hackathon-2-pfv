<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Alteration History
		1999-02-11 WAB changed to allow to run in a popupwindow without any of the menu options showing (variable frmruninpopup)
		25 July 2001 SWJ alterd layout and title function
 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="selectView.cfm">
<CFPARAM NAME="pageTitle" TYPE="string" DEFAULT="Selections">

<CF_RelayNavMenu pageTitle="#pageTitle#" moduleTitle="eMarketing" thisDir="selection">
	<CFIF application.com.login.checkinternalpermissions(securityTask="selectTask").add>
		<CF_RelayNavMenuItem MenuItemText="New Selection" CFTemplate="/selection/selectsearch.cfm" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Show Selections" CFTemplate="/selection/selectview.cfm" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Share Selections" CFTemplate="/selection/selectgrpframe.cfm" target="mainSub">
	</CFIF>

	<CF_RelayNavMenuItem MenuItemText="Copy Selections" CFTemplate="/selection/selecttrans.cfm?frmTransferType=copy" target="mainSub">
	<CF_RelayNavMenuItem MenuItemText="Transfer Selections" CFTemplate="/selection/selecttrans.cfm?frmTransferType=transfer" target="mainSub">
</CF_RelayNavMenu>