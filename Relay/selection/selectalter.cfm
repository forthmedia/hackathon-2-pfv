<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	SelectAlter.cfm  WAB 1999-02-11
		Adds or removes people from a selection--->

<!--- 	List of people passed in variable #frmpersonids#  
		frmtask:	'add' or 'remove'
		Optional Parameters #frmSelectID# or #frmSelectTitle# or frmselectiontextid to predefine the selection to act on  #frmSelectTitle# has spaces converted to '_' and presumably can't have '&, ?' in it		--->

<!---   Amendment History
		WAB 1999-02-18 Can now accept #frmSelectID# or #frmSelectTitle# 
		1999-05-19 WAB added #preservesinglequotes(frmpersonids)# to handle sql statements better
		2005-06-07 WAB added another couple of preservesinglequotesto where they must have been missed

		2005-11-02	WAB added saving of criteria and passing of a default title  (frmTitle) and description (frmDescription)

		2006-11-14   WAB to improve performance (prevent errors) removed code which turned sql statements into list of personids, just pass sql to next template
		2013-01-09	YMA	Case 432684 changed link to submit form link for frmtaskAdd to submit form params rather than params in a URL string.	
		2015-11-12    ACPK    Removed tables and added Bootstrap
 --->		


<!--- Had problem getting single quotes through on the URL, so do a replace of double quotes--->

<CFIF isdefined("frmSelectid") or isdefined("frmSelectionTitle") or isdefined("frmSelectionTextID")>
	<CFPARAM name="frmselectid" default="0">
	<CFPARAM name="frmselectiontitle" default="">
	<CFPARAM name="frmselectiontextid" default="">
	
	<!---  selection is predefined so we can go straight to select alter task, after working out ID --->
	<CFQUERY NAME="getSelectionID" datasource="#application.siteDataSource#">					
		Select s.SelectionID
		From Selection as s, SelectionGroup AS sg
		WHERE (s.selectiontextid =  <cf_queryparam value="#frmselectiontextid#" CFSQLTYPE="CF_SQL_VARCHAR" >  or s.Title =  <cf_queryparam value="#replace(frmSelectionTitle,"_"," ","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" >  or s.selectionid =  <cf_queryparam value="#frmselectid#" CFSQLTYPE="CF_SQL_INTEGER" > )
		AND sg.SelectionID = s.SelectionID
		AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
	</CFQUERY>
	
	<CFIF getSelectionID.recordcount IS NOT 0>
		<CFSET frmselectid=getSelectionID.SelectionIDctionid>
	<CFELSE>
		<CFOUTPUT>
		Unable to find that selection, or you do not have rights to it
		#frmselectiontextid#
		</CFOUTPUT>
		<CF_ABORT>
	</CFIF>
		
	<CFINCLUDE template="selectaltertask.cfm">

<CFELSE>

	<CFOUTPUT>
	<cf_head>
	<cf_title>#request.CurrentSite.Title#</cf_title>
	</cf_head>
			
	
	<cfif not isDefined("frmRuninPopup")>
		<CFINCLUDE TEMPLATE="selecttophead.cfm">
	<cfelse>
		<p>&nbsp;</p>
		<cfset frmRuninPopup = "yes">
	</cfif>
	</CFOUTPUT>
	
	<cfif isDefined("frmOrganisationIDs")>
		<cfset selectionField = "frmOrganisationID">
		<cfset selectionCriteria = frmOrganisationIDs>

		<!--- <cfquery name="GetPersonIDs" datasource="#application.sitedatasource#">
			select Distinct Personid as PersonID
			from person
			where organisationid in (#preserveSingleQuotes(frmOrganisationIDs)#)
		</cfquery> 
			<cfset frmPersonids = valuelist(GetPersonIDs.PersonID)>
		<cfif GetPersonIDs.recordcount eq 0> 
			<CFOUTPUT>There are no persons records within these organisations</CFOUTPUT>
			<CF_ABORT>
		</CFIF>

		--->
			<cfset frmPersonids = "select Distinct Personid as PersonID	from person	where organisationid in (#frmOrganisationIDs#)">
		
	<cfelseif isDefined("frmPersonIDs")>

		<cfset selectionField = "frmPersonID">
		<cfset selectioncriteria = frmPersonIDs>

		<!--- 
		<cfquery name="GetPersonIDs" datasource="#application.sitedatasource#">
			select Distinct Personid as PersonID
			from person
			where personid in (#preserveSingleQuotes(frmPersonIDs)#)
		</cfquery>
		<cfset frmPersonids = valuelist(GetPersonIDs.PersonID)> 
		<cfif GetPersonIDs.recordcount eq 0> 
			<CFOUTPUT>You have not selected any people</CFOUTPUT>
			<CF_ABORT>
		</CFIF>

		WAB 2006-11-14 
		removed this because it can create a very long IN statement which is very slow
		can just continue to pass the underlying SQL around
		--->
		
	</cfif>
	
	<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">		

	<CFIF not (application.com.login.checkInternalPermissions('selectTask','addOkay') OR application.com.login.checkInternalPermissions('selectTask','updateOkay'))>
		<CFOUTPUT>Insufficent permissions</CFOUTPUT>
		<CF_ABORT>
	</CFIF>

	<CFOUTPUT>
	<SCRIPT type="text/javascript">
	<!-- 
		function checkForm(){
			var form=document.SelectionList;
			var msg = "";
//			alert("form.frmSelectID.options[form.frmSelectID.selectedIndex].value = " + form.frmSelectID.selectedIndex);
			if (form.frmSelectID.selectedIndex != -1) {
				if (form.frmSelectID.options[form.frmSelectID.selectedIndex].value == 0) {
					msg += "\n\nYou must choose a selection.";
				}
			} else {
				msg += "\n\nYou must choose a selection.";
			}
			if (msg != "") {
				alert(msg);
			} else {
				form.submit();
			}
		}
	//-->
	</SCRIPT>
	</CFOUTPUT>
	
	<CFQUERY NAME="Count" datasource="#application.siteDataSource#">
	SELECT Count(personid)  as peoplechecked, Count(distinct locationid) as locationsChecked, Count(distinct organisationid) as organisationsChecked
	FROM Person
	WHERE Personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)
	</CFQUERY>

	<!--- LID 5112 --->
	<CFIF count.peopleChecked IS 0>
		<cfif not structKeyExists(form,"badEmails")> <!--- NJH this will be defined if doing a selection on people with bad email addresses (set in selectionviewpopup.cfm) --->
			<CFOUTPUT>You have not selected any people</CFOUTPUT>
		<cfelse>
			<CFOUTPUT>No people in the selected selections have bad email addresses.</CFOUTPUT>
		</cfif>
		<CF_ABORT>
	</CFIF>


	<CFIF frmtask IS "Add" or frmtask IS "Remove">
	
		<!--- Ask user which selection to add or remove items from --->
		<!--- then call selectaltertask.cfm--->
		
		<!--- Get selections which user has rights to --->
		<cfinclude template="/templates/qrygetselections.cfm">

		<CFOUTPUT>
		
		<CFIF count.peoplechecked IS 1>
			<CFSET entityname = "person">
			<CFSET entitythis = "this">
		<CFELSE>
			<CFSET entityname = "people">
			<CFSET entitythis = "these">
		</CFIF>

		<p>You have checked #htmleditformat(count.peoplechecked)# #htmleditformat(entityname)#</p>
		<p>from #htmleditformat(count.organisationschecked)# organisation<cfif count.organisationschecked gt 1>s</cfif></p>
		
		<FORM CLASS="form-horizontal" NAME="SelectionList" ACTION="/selection/selectaltertask.cfm" METHOD="POST">
			<div class="form-group">
                <div class="col-xs-12">
					<label for="frmSelectID">
    	   				<CFIF frmtask IS "Add">Highlight the selection(s) to add #htmleditformat(entitythis)# #htmleditformat(entityname)# to: </CFIF>
                        <CFIF frmtask iS "Remove">Highlight the selection(s) to remove #htmleditformat(entitythis)# #htmleditformat(entityname)# from: </CFIF>
					</label>
					<SELECT ID="frmSelectID" CLASS="form-control" NAME="frmSelectID" SIZE="<CFIF getSelections.RecordCount GT 10>10<CFELSE><CFOUTPUT>#getSelections.RecordCount#</CFOUTPUT></CFIF>" MULTIPLE>
						<CFLOOP QUERY="getSelections">
							<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #htmleditformat(Title)#
						</CFLOOP>
					</SELECT>
				</div>
			</div>
		 	<CF_encryptHiddenFields> <!--- 2013-01-09	YMA	Case 432684 --->
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonIDs" VALUE="#frmpersonids#">
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="#frmtask#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
			</CF_encryptHiddenFields> <!--- 2013-01-09	YMA	Case 432684 --->
			<div class="form-group">
                <div class="col-xs-12">
    			     <input type="button" id="checkFormBtn" class="btn-primary btn" value="Continue" onClick="JavaScript:checkForm();">
		        </div>
		    </div>
			<!--- <A HREF="JavaScript:checkForm();"><IMG SRC="/images/BUTTONS/c_continue_e.gif" BORDER=0 ALT="Continue"></A> --->
		</FORM>
		
		</CFOUTPUT>
	
	<CFELSEIF frmtask IS "Save">

		<cfparam name="frmTitle" default = "">
		<cfparam name="frmDescription" default = "">		
		<!--- put the ids into the tempselection table and then use existing qrysaveselection --->
		<!--- first clear out temp selection stuff --->
	
		<CFQUERY NAME="delTags" datasource="#application.siteDataSource#">
			DELETE FROM TempSelectionTag
			WHERE SelectionID = #request.relayCurrentUser.personid#
		</CFQUERY>
	
		<CFQUERY NAME="delCriteria" datasource="#application.siteDataSource#">
			DELETE FROM TempSelectionCriteria
			WHERE SelectionID = #request.relayCurrentUser.personid#
		</CFQUERY>
	
		<!--- add entity ids to tempselectiontag --->
		
  		<CFQUERY name="addperson" datasource="#application.siteDataSource#">
			INSERT INTO TempSelectionTag (SelectionID,EntityID,Status,CreatedBy,Created,LastUpdatedBy,LastUpdated) 
			 SELECT #request.relayCurrentUser.personid#,	
 					p.personid,	
					1,	
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,	
					<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,		
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				FROM Person as p
				WHERE p.personid IN(<cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="true" allowSelectStatement="true">)
		</CFQUERY>

		<!--- START:  NYB 2009-02-26 - bug 1539 - added: --->
		<cfif len("in ("&selectionCriteria&")") gt 4000>
			<cfset refreshRate = "X">
			<cfset selectionCriteria = "select entityid from SelectionTag where CreatedBy=#request.relayCurrentUser.usergroupid# and Created=#FreezeDate#">
		<cfelse>
			<cfif selectionCriteria contains "select">
				<cfset refreshRate = "A">
			<cfelse>
				<cfset refreshRate = "X">
			</cfif>
		</cfif>
		<!--- END:  NYB 2009-02-26 - bug 1539 --->

		<CFQUERY name="addCriteria" datasource="#application.siteDataSource#">
			INSERT INTO tempSelectionCriteria (SelectionID,SelectionField,Criteria, CreatedBy,Created,LastUpdatedBy,LastUpdated) 		
			select #request.relayCurrentUser.personid#,<cf_queryparam value="#selectionField#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="in (#selectionCriteria#)" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</CFQUERY>
		
		<CFQUERY NAME="Count" datasource="#application.siteDataSource#">
			SELECT Count(personid)  as peoplechecked, Count(distinct locationid) as locationsChecked, Count(distinct organisationid) as organisationsChecked
			FROM Person p inner join tempselectiontag st on p.personid = st.entityid
			<!--- START:  NYB 2009-02-26 - bug 1539 - added query as it was returning an incorrect number --->
			WHERE SelectionID = #request.relayCurrentUser.personid#
			<!--- END:  NYB 2009-02-26 - bug 1539 --->
		</CFQUERY>
		
		<!--- Ask user for name for a selection--->
		<CFOUTPUT>		
		<SCRIPT type="text/javascript">
		<!--
			function verifyForm(task) {
				var form = document.selectionname;
				form.frmTask.value = task + "";
				var msg = "";
				if (form.frmTitle.value == "" || form.frmTitle.value == " ") {
					msg += "* Title\n";
				}
				if (form.frmDesc.value == "" || form.frmDesc.value == " ") {
					msg += "* Description\n";
				}		
				if (msg != "") {
					alert("\nYou must provide the following information for your selection:\n\n" + msg);
				} else {
					form.submit();
				}
			}
		//-->
		</SCRIPT>
		</CFOUTPUT>
	
		<CFOUTPUT>
			<FORM CLASS="form-horizontal" NAME="selectionname" ACTION="/selection/selectaltertask.cfm" METHOD="POST">
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="#frmtask#"> <!--- 2013-01-09	YMA	Case 432684 --->
			 	<CF_encryptHiddenFields> <!--- 2013-01-09	YMA	Case 432684 --->
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonIDs" VALUE="#preservesinglequotes(frmpersonids)#">
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmRecordCount" VALUE="#count.peoplechecked#">
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmSelectedCount" VALUE="#count.peoplechecked#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
				<!--- NYB 2009-02-26 - bug 1539 - replaced VALUE="<cfif selectionCriteria contains 'select'>A<cfelse>X</cfif>" with VALUE="#refreshRate#" --->
				<CF_INPUT TYPE="HIDDEN" NAME="frmRefresh" VALUE="#refreshRate#">
				</CF_encryptHiddenFields> <!--- 2013-01-09	YMA	Case 432684 --->
				
				<CFIF count.peoplechecked IS 1>
					<CFSET entityname = "person">
					<CFSET entitythis = "this">
				<CFELSE>
					<CFSET entityname = "people">
					<CFSET entitythis = "these">
				</CFIF>
					
				
					<h3>SAVE AS SELECTION</h3>
	 				<CFOUTPUT>
		 				<p>You have checked #htmleditformat(count.peoplechecked)# #htmleditformat(entityname)#</p>
					    <p>from #htmleditformat(count.organisationschecked)# organisation<cfif count.organisationschecked gt 1>s</cfif></p>
					</CFOUTPUT>

					<div class="form-group">
					     <div class="col-xs-12">
						     <label for="frmTitle">Title: </label>
					         <CF_INPUT TYPE="TEXT" ID="frmTitle" CLASS="form-control" NAME="frmTitle" VALUE="#frmTitle#" SIZE=25 MAXLENGTH=50>
					     </div>
					</div>
					<div class="form-group">
                        <div class="col-xs-12">
                            <label for="frmDesc">Description: </label>	
						    <CF_INPUT TYPE="TEXT" ID="frmDesc" CLASS="form-control"  NAME="frmDesc" VALUE="#frmDescription#" SIZE=25 MAXLENGTH=80>
						</div>
                    </div>

			    <div class="form-group">
			        <div class="col-xs-12">
				      	<INPUT TYPE="BUTTON" NAME="btnSave" CLASS="btn-primary btn" VALUE="Save" onClick="verifyForm('save');">
					</div>
				</div>
				<!--- <A HREF="JavaScript:verifyForm('save');"><CFOUTPUT><IMG SRC="/images/buttons/c_save_e.gif" BORDER="0"></CFOUTPUT></A> --->
				&nbsp; 
					
				<!--- <INPUT TYPE="BUTTON" NAME="btnReview" VALUE=" Save and Review " onClick="verifyForm(this.form,'review');"> --->
				<!--- 	<A HREF="JavaScript:verifyForm('review');"><CFOUTPUT><IMG SRC="/images/buttons/c_savereview_e.gif" WIDTH=144 HEIGHT=21 BORDER="0"></CFOUTPUT></A> --->
			
			</FORM>
		</CFOUTPUT>
		
		

	</CFIF>
	<!--- 2013-01-09 YMA Case 432684 changed link to submit form parameters--->
	<!--- NJH 2012/02/24 NJH CASE 426742 --->
	<cfif FRMRUNINPOPUP and isDefined("FRMPERSONIDS") and FRMPERSONIDS neq "" and isDefined("FRMTASK") and FRMTASK eq "add">
		<cfoutput>
		<FORM CLASS="form-horizontal" NAME="selectionAdd" id="selectionAdd" ACTION="/selection/selectalter.cfm" METHOD="POST">
			<CF_encryptHiddenFields> <!--- 2013-01-09	YMA	Case 432684 --->
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonIDs" VALUE="#frmpersonids#">
			 	<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="save">
				<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="true">
			</CF_encryptHiddenFields> <!--- 2013-01-09	YMA	Case 432684 --->
			<p><a href="##" onClick="javascript:document.getElementById('selectionAdd').submit();">Click here</a> to add the selected people to a new selection.</p>
		</FORM>
		</cfoutput>
	</cfif>
</CFIF>
