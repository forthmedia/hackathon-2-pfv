<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History:

Date (DD-MMM-YYYY)  Initials    What was changed
2015-11-12          ACPK        Replaced tables with Bootstrap
2015-11-13          ACPK        Replaced arrow images with standard buttons
 --->

<!--- SelectReview.cfm --->
<CFSET showrecords = 100>  <!--- overrides setting from application .cfm --->
<cf_includejavascriptonce template="/javascript/extExtension.js" > <!--- NJH 2008/11/19 Bug Fix All Sites Issue 1363--->
<cf_includeJavascriptOnce template = "/javascript/checkBoxFunctions.js">

<!--- Amendment History
	WAB 1999-04-16		removed restriction
	wab 1999-02-15   	Check Boxes and a Drop down function box - still has limited access.
					to remove restriction, remove CFIFs in THREE places
	KT  1999-10-11	count of selected entities now saved into selection table
					and reflects tagging / untagging entities

2001-03-22		WAB		added items to dropdowns and at same time standardised on the 'checkboxFunctions' javascript
					made some mods to form name and things.
2001-09-06	SWJ		Modified the listing screen.
2001-09-26	SWJ		Added link to main data screens
2001-10-28 	SWJ 	Added alphaJump

May 2005    NM		Nikki altered so that a person without rights to this selection sees it in view mode
2005-07-20	WAB		If user is just in view mode then should only show people who are actually selected as being in the selection
					(could show them but indicate that not in the selection)
2005-09-29		WAB		By default don't show people removed from selection
2005-10-03	WAB		discovered that when in view mode, if the form happened to be submitted then all the items are untagged from selection  - Corrected
			WAb		also noticed that alphajump didn't work if already used the forwards/backwards button so that frmStart is set (especially if frmStart is larger that the number of items in that letter)
2008/03/27	WAB 	changed the viewPerson/viewOrganisation links to bring up the new entityNavigation
2008/11/19	NJH/WAB	Bug Fix All Sites Issue 1363 - Made some changes in response to an email from ATI in which large selections were timing out when being viewed. It was simply a matter of lots of data
					coming back. To solve this issue, we initially get the personID's in the selection and then run the full query filtered by a selection of personIDs.
					So, we're only bringing back the data that we are viewing on the screen at that moment rather than the full dataset every time.
2009/11/17	NAS		LID 2850 cfparam added as type numeric to stop 'or 1 = 1' sql injections.
2009/12/?     	WAB		discovered error in above
2012-07-09	WAB		P-SMA001 Implement Country (and Custom) Rights Filtering using getRightsFilterQuerySnippet()
--->

<CFSET FreezeDate = request.requestTimeODBC>
<CFSET displaystart = 1>

<cfparam name="form.frmAlphaJump" default="">
<cfparam name="form.frmShowRemovedPeople" default="">


<!--- need to save the checkmarks if they exist --->

<!--- do the following only if coming in from tag selections
		which can be determined if "frmTagdate" exists
--->

<CFIF IsDefined("frmSearchID")>
	<CFSET searchID = #frmSearchID#>
<CFELSEIF NOT IsDefined("Variables.searchID")>
	<CFSET #message# = "phr_selections_invalid_request: phr_selections_you_must_choose_a_selection">
	<cfoutput>#application.com.relayUI.message(message=message,type="info")#</cfoutput>
	<!--- <cfinclude template="/templates/message.cfm"> --->
	<CF_ABORT>
</CFIF>


<!--- 	2009/11/17		NAS	LID 2850 cfparam added as type numeric to stop 'or 1 = 1' sql injections
		2009/12/?     	WAB moved this from before the IsDefined("frmSearchID") because variables.searchid is not always set at that point
--->
<cfparam name="variables.SearchID" type="numeric">


<!--- verify that the user has rights to this selection --->
<CFQUERY NAME="checkSelGroup" datasource="#application.siteDataSource#">
	SELECT * FROM SelectionGroup sg inner join rightsgroup rg on sg.usergroupid = rg.usergroupid
	WHERE SelectionID =  <cf_queryparam value="#searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND rg.personID = #request.relayCurrentUser.personid#
</CFQUERY>


	<cfset editMode = true>
	<cfset showInSelectionColumn = true>
<cfif checkPermission.UpdateOkay is 0>
	<cfset editMode = false>
<CFelseIF checkSelGroup.RecordCount IS 0>
	<cfset editMode = false>
</cfif>

<cfif not editMode and frmShowRemovedPeople is not 1>
	<cfset showInSelectionColumn = false>
</cfif>

<!--- <CFIF checkSelGroup.RecordCount IS 0>
	<CFSET #message# = "You are not authorised to tag/untag individuals in this selection.">
	<cfinclude template="/templates/message.cfm">
	<CF_ABORT>
</CFIF> --->

<!--- WAB 2008/03/27 links to new entityNavigation --->
<cf_includejavascriptonce template="/javascript/viewentity.js">
<!--- <SCRIPT type="text/javascript">
	<CFINCLUDE template="..\screen\jsViewEntity.cfm">
</SCRIPT>
 --->
<CFIF IsDefined("frmTagdate") and editmode>
	<!--- frmTagdate means that this form is the referrer and
			there are checkboxes that need to be saved
	--->

	<CFPARAM NAME="startrec" DEFAULT=1>

	<CFQUERY NAME="checkTagLock" datasource="#application.siteDataSource#">
		SELECT  LastUpdatedBy,
				Max(LastUpdated) AS LastUpd
		  FROM SelectionTag
		 WHERE SelectionID =  <cf_queryparam value="#searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
<!--- 		   AND CreatedBy = #request.relayCurrentUser.usergroupid# --->
	  GROUP BY LastUpdatedBy
	  ORDER BY Max(LastUpdated) DESC
	</CFQUERY>

	<!--- in theory this query can return more than one record
			if selections are available to more than one group
			but all we care about is the most recent date and
			who made that update --->

	<CFIF (#DateDiff("s",checkTagLock.LastUpd, FORM.frmTagdate)# IS NOT 0) AND (#checkTagLock.LastUpdatedBy# IS NOT #request.relayCurrentUser.usergroupid#)>
		<!--- if the max last modified date is different
				and was created by a different user --->
		<CFSET message = "phr_selections_error_tags_could_not_be_updated.">
		<CFSET message = message & "phr_selections_error_please <A HREF=""selectreview.cfm?frmSearchID=#Variables.searchID#&start=#Variables.displaystart#"">phr_selections_error_click_here</A> phr_selections_error_to_view_current_record_version.">
		<cfoutput>#application.com.relayUI.message(message=message,type="error")#</cfoutput>
		<!--- <cfinclude template="/templates/message.cfm"> --->
		<CF_ABORT>
	</CFIF>

	<CFIF DateDiff("s",checkTagLock.LastUpd, FORM.frmTagdate) IS 0>

		<!--- KT 1999-10-11 set the change in selected entities on this page (this is following a save change request) --->
		<CFIF IsDefined("form.frmTagIDs")>
			<CFSET NewSelected = frmSelectedOrig - ListLen(frmTagIDs)>
		<CFELSE>
			<CFSET NewSelected = frmSelectedOrig>
		</CFIF>

		<cfif frmTotalSelected neq "">
			<CFSET NewTotSelected = frmTotalSelected - NewSelected>
		<cfelse>
			<CFSET NewTotSelected = NewSelected>
		</cfif>

		<!--- dates are the same so save the records --->

 		<CFQUERY NAME="saveTagStatusNo" datasource="#application.siteDataSource#">
		UPDATE SelectionTag
		   SET Status = 0,
			   LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			   LastUpdatedBY = #request.relayCurrentUser.usergroupid#
	   	 WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- AND CreatedBy = #request.relayCurrentUser.usergroupid# --->
		   AND EntityID  IN ( <cf_queryparam value="#frmPageList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		   <CFIF IsDefined("frmTagIDs")> <!--- could be empty --->
		   AND EntityID  NOT IN ( <cf_queryparam value="#frmTagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		   </CFIF>
		</CFQUERY>

	   <CFIF IsDefined("frmTagIDs")>
	   		 <!--- could be empty
			 		in which case all frmPageList should be unchecked
					which is accomplished by the query above --->
			<CFQUERY NAME="saveTagStatusYes" datasource="#application.siteDataSource#">
				UPDATE SelectionTag
				   SET Status = 1,
					   LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					   LastUpdatedBY = #request.relayCurrentUser.usergroupid#
			   	 WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
<!--- 				   AND CreatedBy = #request.relayCurrentUser.usergroupid# --->
				   AND EntityID  IN ( <cf_queryparam value="#frmPageList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				   AND EntityID  IN ( <cf_queryparam value="#frmTagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
	   </CFIF>


		<!--- WAB 2004-09-21
		Added columns to count locations and organisation to the selection table
		need to update them here
		The query included here probably duplicates some of the stuff above, but didn't really want to get into reworking the whole thind (sorry!)
		--->

		<cfset frmSelectionID = searchid>
		<cfinclude template = "..\templates\qrySelectionRecordCounts.cfm">


		<!--- KT 1999-10-11 update the total of selected entities --->
		<CFQUERY NAME="UpdateSelected" datasource="#application.siteDataSource#">
			UPDATE Selection
			SET
				RecordCount =  <cf_queryparam value="#recordCounts.AllPeopleSelectedorUnselected#" CFSQLTYPE="CF_SQL_Integer" > ,  <!--- shouldn't have changed --->
				SelectedCount =  <cf_queryparam value="#recordCounts.people#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCountLoc =  <cf_queryparam value="#recordCounts.locations#" CFSQLTYPE="CF_SQL_Integer" > ,
				SelectedCountOrg =  <cf_queryparam value="#recordCounts.organisations#" CFSQLTYPE="CF_SQL_Integer" > ,
				LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				LastUpdatedBY = #request.relayCurrentUser.usergroupid#
			WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>


	</CFIF>

	<CFIF FORM.frmToView IS NOT 0>
		<!--- may not have saved the checkboxes if the dates are
				different --->
		<!--- <CFINCLUDE TEMPLATE="recordview.cfm"> --->
		<CFINCLUDE TEMPLATE="/data/personDetail.cfm">
		<CF_ABORT>
	</CFIF>

</CFIF>

<CFQUERY NAME="getHead" datasource="#application.siteDataSource#">
	SELECT  s.Title,
			s.Description,
			s.CreatedBy,
			s.RecordCount as allpersons,
			s.selectedCount,
			ug.personid as CreatedByPersonID
	  FROM 	Selection AS s
	  		LEFT JOIN
	  		usergroup ug on ug.usergroupid = s.createdby
	 WHERE s.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
<!--- 	   AND s.CreatedBy = #request.relayCurrentUser.usergroupid# --->
</CFQUERY>

<!--- get the next set of records
		the max date is the max for all records of a selection ID
		not just the records selected
--->

<cfinclude template="/templates/qrygetcountries.cfm">
<!--- now Variables.CountryList --->

<!--- If the selection is a search criteria, then we need to limit
	the locations to the owner's countries as well

WAB 2012-07-09 now done with getRightsFilterQuerySnippet()
<CFIF getHead.CreatedBy IS NOT request.relayCurrentUser.usergroupid>

	<CFQUERY NAME="getOwnerCountries" DATASOURCE="#application.SiteDataSource#">
		SELECT r.CountryID
		  FROM vCountryRights AS r
		  			inner join
		  		UserGroup AS ug on r.PersonID = ug.PersonID
					inner join
			   Selection AS s on ug.UserGroupID = s.CreatedBy
		 WHERE s.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<CFSET OwnerCountries = 0>
		<CFIF getOwnerCountries.RecordCount GT 0>
			<CFIF getOwnerCountries.CountryID IS NOT "">
				<!--- top record (or only record) is not null --->
				<CFSET OwnerCountries = ValueList(getOwnerCountries.CountryID)>
			</CFIF>
		</CFIF>
</CFIF>
--->

<!--- NJH 2008/11/19 Bug Fix All Sites Issue 1363 - get all the personIDs in the selection --->
<CFQUERY NAME="getRecordsPersonID" datasource="#application.siteDataSource#">
SELECT  p.personid
	FROM
         Selection s
			INNER JOIN
         SelectionTag st ON s.SelectionID = st.SelectionID
			INNER JOIN
		 Person p ON st.EntityID = p.PersonID
			INNER JOIN
         Location l 	 ON l.LocationID = p.LocationID
			INNER JOIN
		organisation o ON o.OrganisationID = l.OrganisationID

		<!--- WAB 2012-0709 P-SMA001 implement rights using getRightsFilterQuerySnippet () --->
		#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l").join#

		 <CFIF getHead.CreatedBy IS NOT request.relayCurrentUser.usergroupid>
		  <!--- if the owner is not the user, limit to the owner's countries as well --->
			#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l",personid = getHead.CreatedByPersonID).join#
	    </CFIF>


	WHERE s.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
	  AND s.Tablename = 'Person'
	  AND
	  	<cfif isDefined("frmpersinactive")>
			p.Active <> -1
		<cfelse>
			p.Active <> 0
		</cfif>

	  <!--- 2001-10-28 SWJ Added alphaJump --->
	  <CFIF isDefined('form.frmAlphaJump') and form.frmAlphaJump neq "">
	  	AND left(SiteName,1) =  <cf_queryparam value="#form.frmAlphaJump#" CFSQLTYPE="CF_SQL_VARCHAR" >
	  </CFIF>
	  <cfif frmShowRemovedPeople is not 1>
	  	and status <> 0
	  </cfif>

   ORDER BY l.SiteName,p.LastName
</CFQUERY>



<CFIF #getRecordsPersonID.RecordCount# GT #application.showRecords#>
	<!--- showrecords in application .cfm
			defines the number of records to display --->

	<!--- define the start record --->
 	<CFIF IsDefined("FORM.Forward.x")>
		 <CFSET displaystart = #frmStart# + #application.showRecords#>
 	<CFELSEIF IsDefined("FORM.Back.x")>
		 <CFSET displaystart = #frmStart# - #application.showRecords#>
	<CFELSEIF IsDefined("frmStart")>
	 	 <CFSET displaystart = #frmStart#>
	</CFIF>

</CFIF>
<cfset endRow = displaystart + application.showRecords - 1>

<cfset personIDlist = "">
<cfloop query = "getRecordsPersonID" startrow = #displaystart# endrow = #endrow#>
	<cfset personIDlist = listappend (personIDlist,personid)>
</cfloop>

<cfif personIDlist eq "">
	<cfset personIDlist = 0>
</cfif>

<!--- NJH 2008/11/19 Bug Fix All Sites Issue 1363 - get only the records that are currently being displayed --->
<CFQUERY NAME="getRecords" datasource="#application.siteDataSource#">
SELECT  s.SelectedCount, p.PersonID, p.FirstName, p.OrganisationID, p.Email,
	p.LastName, l.SiteName, p.JobDesc,
    (SELECT MAX(dateSent) FROM commdetail WHERE personID = p.personid) AS lastContactDate,
	l.Address1, l.Address2, l.Address3, l.Address4, l.Address5, st.Status, c.ISOCode
	FROM dbo.organisation o INNER JOIN
         dbo.Location l ON o.OrganisationID = l.OrganisationID INNER JOIN
         dbo.Selection s INNER JOIN
         dbo.SelectionTag st ON s.SelectionID = st.SelectionID INNER JOIN
         dbo.Person p ON st.EntityID = p.PersonID
		 ON l.LocationID = p.LocationID INNER JOIN
         dbo.Country c ON l.CountryID = c.CountryID

	<!--- WAB 2012-0709 P-SMA001 implement rights using getRightsFilterQuerySnippet () --->
		#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l").join#

		  <CFIF getHead.CreatedBy IS NOT request.relayCurrentUser.usergroupid>
			  <!--- if the owner is not the user, limit to the owner's countries as well --->
				#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l",personid = getHead.CreatedByPersonID).join#
		  </CFIF>

	WHERE s.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
	  AND s.Tablename = 'Person'
	 and p.personid  in ( <cf_queryparam value="#personIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  AND
	  	<cfif isDefined("frmpersinactive")>
			p.Active <> -1
		<cfelse>
			p.Active <> 0
		</cfif>
		<!--- if person is active, then location & org must be ---->
	  <!--- 2001-10-28 SWJ Added alphaJump --->
	  <CFIF isDefined('form.frmAlphaJump') and form.frmAlphaJump neq "">
	  	AND left(SiteName,1) =  <cf_queryparam value="#form.frmAlphaJump#" CFSQLTYPE="CF_SQL_VARCHAR" >
	  </CFIF>
	  <cfif frmShowRemovedPeople is not 1>
	  	and status <> 0
	  </cfif>

   ORDER BY l.SiteName,p.LastName
</CFQUERY>




<!--- note that the above could return nothing since selection
		tablename is not "Person" but the getMax will
		return something.  Only use getMax if getRecords.RecordCount
		is GT 0--->
<CFQUERY NAME="getMax" datasource="#application.siteDataSource#">
	SELECT	Max(LastUpdated) as MaxLastUpdated
	FROM SelectionTag
	WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
<!--- 	  AND CreatedBy = #request.relayCurrentUser.usergroupid# --->
</CFQUERY>

<cfif getMax.recordCount eq 0 or getMax.MaxLastUpdated is "">
	<cfset maxLastUpdated = now()>
<cfelse>
	<cfset maxLastUpdated = getMax.MaxLastUpdated>
</cfif>

<CFQUERY NAME="getSelectionDetail" datasource="#application.siteDataSource#">
	SELECT	*
	FROM SelectionCriteria
	WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFSET CalcSelectedCount = getRecords.SelectedCount>

<cfset application.com.request.setDocumentH1(text="Selection Review")>

<cf_head>
<SCRIPT type="text/javascript">
	<CFOUTPUT>
	function saveChanges() {
		var form = document.mainForm;
		form.frmSaveChanges.value = "1";
		form.submit();
		return;
	}

	function viewOrg(ID) {
         var form = document.orgForm;
         form.frmsrchOrgID.value = ID;
		 form.frmNext.value = "#jsStringFormat(SCRIPT_NAME)#?#jsStringFormat(request.query_string)#&frmNextTarget=mainSub";
         form.submit();
         return;
      }
	 </CFOUTPUT>

	function alphaJump(letter){
		var form = document.mainForm;
		form.frmSaveChanges.value = "1";
		form.frmAlphaJump.value = letter;
		form.frmStart.value = 1   // added WAB 2005-10-03 have to reset this to 1
		showLoadingIcon(); // NJH 2008/11/19 Bug Fix All Sites Issue 1363
		form.submit();
		return;
		}


//-->
</SCRIPT>
</cf_head>


<CFOUTPUT>
	<p>phr_selections_records_found: #getRecordsPersonid.RecordCount#</p>
	<cfif frmShowRemovedPeople is 1><p>phr_selections_selected: #htmleditformat(variables.CalcSelectedCount)#</p></cfif>
</CFOUTPUT>


<CFIF getRecords.Recordcount IS not 0>
	<CFINCLUDE template="selectionpopup.cfm">
</CFIF>


<FORM CLASS="form-horizontal" NAME="mainForm" ACTION="selectreview.cfm?RequestTimeout=200" METHOD="POST">
	<CFOUTPUT>
		<CF_INPUT TYPE="HIDDEN" NAME="frmTotalSelected" VALUE="#variables.CalcSelectedCount#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmSearchID" VALUE="#Variables.searchID#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmTagDate" VALUE="#CreateODBCDateTime(maxLastUpdated)#">
		<INPUT TYPE="HIDDEN" NAME="frmToView" VALUE="0">
		<CF_INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="#SCRIPT_NAME#?#request.query_string#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#SCRIPT_NAME#?#request.query_string#">
		<INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="0">
		<cfif frmAlphaJump eq "">
			<INPUT TYPE="HIDDEN" NAME="frmSaveChanges" VALUE="0">
		<cfelse>
			<INPUT TYPE="HIDDEN" NAME="frmSaveChanges" VALUE="1">
		</cfif>

		<CF_INPUT TYPE="HIDDEN" NAME="frmAlphaJump" VALUE="#form.frmAlphaJump#">
		<INPUT TYPE="hidden" NAME="frmFlagIDs" VALUE="0">
	</CFOUTPUT>

	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">

<CFIF getRecords.Recordcount IS 0>
	<p>	phr_selections_error_selection_has_no_individuals.</p>

	<cfif getHead.allPersons is not getHead.selectedCount and form.frmShowRemovedPeople is not 1>
		<INPUT TYPE="HIDDEN" NAME="frmSelectedOrig" VALUE="0">
		<INPUT TYPE="HIDDEN" NAME="frmPageList" VALUE="0">
		<input onClick = "javascript:this.form.submit();" name = "frmShowRemovedPeople"type="checkbox" value="1" <cfif form.frmShowRemovedPeople is 1>checked</cfif>> Show People Removed From Selection
		<input type=hidden name = "frmShowRemovedPeople" value = "">
	</cfif>

<CFELSE>
	<CFIF isDefined("message") and len(message)>
		<CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT>
	</CFIF>

	<CFOUTPUT>
		<CFIF editMode>
			<!--- 2008-07-16 NYF replaced:
			<A HREF="JavaScript:saveChanges();"><IMG SRC="/images/buttons/c_savechtosel_e.gif" ALT="phr_selections_save_changes_to_selection" BORDER="0"></A>
			--- with: --->
			<div class="form-group">
			     <div class="col-xs-12">
			         <input name="frmSave" id="frmSave" class="btn-primary btn" type="button" value="phr_label_save_changes_to_selection" border="0"  alt="phr_selections_save_changes_to_selection" onClick="JavaScript:saveChanges();"/>
			     </div>
    	     </div>
		</CFIF>
	</CFOUTPUT>

	<CFIF #getRecordsPersonID.RecordCount# GT #application.showRecords# or frmAlphaJUmp is not "">
		<!--- 2001-10-28 SWJ Added alphaJump --->
		<p>phr_selections_click_to_jump_to_orgs_beginning_with:</p>
		<!--- <CFSET i=#asc('B')#><CFOUTPUT>#i# chr(i)=#chr(90)#</CFOUTPUT> --->
		<CFLOOP INDEX="i" FROM="65" TO="90" STEP="1">
			<CFOUTPUT><A HREF="javaScript:alphaJump('#chr(i)#')">#htmleditformat(chr(i))#</A></CFOUTPUT>
		</CFLOOP>
		<cfif frmAlphaJump is not "">
			<CFOUTPUT>&nbsp;<A HREF="javaScript:alphaJump('')">phr_selections_show_all</A></CFOUTPUT>
		</cfif>
	</CFIF>

	<TABLE CLASS="table table-hover">
	<TR>
		<TH><cf_input type="checkbox" id="tagAllSelections" name="tagAllSelections" onclick="tagCheckboxes('frmPersonCheck',this.checked)"></th>
		<CFIF showInSelectionColumn>
		<TH>phr_selections_in_selection</th>
		</CFIF>
		<TH<CFIF not showInSelectionColumn> colspan="2"</CFIF>>phr_selections_name_job_description</th>
		<TH>phr_selections_company</th>
		<TH>phr_selections_email</TH>
		<TH>phr_selections_last_contact_date</TH>
	</TR>

		<CFSET IDs_on_page = "">
		<CFSET cnt = 0>
		<CFSET PageSelected = 0>

		<CFOUTPUT QUERY="getRecords" <!--- STARTROW=#Variables.displaystart# MAXROWS=#application.showRecords# --->>
					<CFSET cnt = cnt + 1>
					<CFIF getRecords.Status GT 0>
						<!--- calculate number of selected entities on this page --->
						<CFSET PageSelected = PageSelected + 1>
					</CFIF>
				<TR<CFIF cnt MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
					<TD><CF_INPUT TYPE="checkbox" NAME="frmPersonCheck" VALUE="#PersonID#"></TD>
					<CFIF showInSelectionColumn>
					<TD>
						<CFIF editMode>
							<CF_INPUT TYPE="checkbox" NAME="frmTagIDs" VALUE="#PersonID#" CHECKED="#iif(getRecords.Status GT 0,true,false)#">
						<cfelseif getRecords.Status GT 0>
							X
						</CFIF>
					</TD>
					</CFIF>
					<TD<CFIF not showInSelectionColumn> colspan="2"</CFIF>>
						<a href="Javascript:void(viewPerson(#personid#,'',{openInOwnTab:true}));">#HTMLEditFormat(FirstName)# #HTMLEditFormat(LastName)#</A>
						<BR>#htmleditformat(jobDesc)#
					</TD>
					<TD><A HREF="javaScript:void(viewOrganisation('#organisationID#','',{openInOwnTab:true}))">#HTMLEditFormat(SiteName)#</A><BR>
						<CFIF Trim(address5) IS NOT "">
							#HTMLEditFormat(address5)#,
						<CFELSEIF Trim(address4) IS NOT "">
							#HTMLEditFormat(address4)#,
						<CFELSEIF Trim(address3) IS NOT "">
							#HTMLEditFormat(address3)#,
						<CFELSEIF Trim(address2) IS NOT "">
							#HTMLEditFormat(address2)#,
						<CFELSEIF Trim(address1) IS NOT "">
							#HTMLEditFormat(address1)#,
						<CFELSE>
							&nbsp;
						</CFIF>
						#HTMLEditFormat(ISOCode)#
					</TD>
					<TD><CFIF Trim(email) IS NOT "">#HTMLEditFormat(email)#<CFELSE>&nbsp;</CFIF></TD>
					<TD><CFIF Trim(LastContactDate) IS NOT "">#dateformat(LastContactDate, 'DD-MMM-YY')#<CFELSE>&nbsp;</CFIF></TD>
		 		</TR>
					<CFSET IDs_on_page = #ListAppend(IDs_on_page,PersonID,",")#>

		</CFOUTPUT>
	</TABLE>
	<cfoutput>
		<CFIF checkSelGroup.RecordCount NEQ 0>
			<!--- 2008-07-16 NYF replaced:
			<A HREF="JavaScript:saveChanges();"><IMG SRC="/images/buttons/c_savechtosel_e.gif" ALT="phr_selections_save_changes_to_selection" BORDER="0"></A>
			--- with: --->
			<div class="form-group">
                <div class="col-xs-12">
        			<input name="frmSave" id="frmSave" class="btn-primary btn" type="button" value="phr_label_save_changes_to_selection" border="0"  alt="phr_selections_save_changes_to_selection" onClick="JavaScript:saveChanges();"/>
		        </div>
		    </div>
		</CFIF>

		<cfif getHead.allPersons is not getHead.selectedCount>
		    <div class="form-group">
			    <div class="col-xs-12">
			      <div class="checkbox">
			        <label>
			          <input onClick = "javascript:this.form.submit();" name = "frmShowRemovedPeople"type="checkbox" value="1" <cfif form.frmShowRemovedPeople is 1>checked</cfif>> phr_selections_show_people_removed_from_selection
			        </label>
			      </div>
			      <input type=hidden name = "frmShowRemovedPeople" value = "">
			    </div>
			</div>

		</cfif>

	</cfoutput>

	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmSelectedOrig" VALUE="#variables.PageSelected#">
	<CFIF Variables.IDs_on_page IS NOT "">
		<!--- a list of all Person IDs shown on the page --->
		<CF_INPUT TYPE="HIDDEN" NAME="frmPageList" VALUE="#Variables.IDs_on_page#">
	</CFIF>
	</CFOUTPUT>

	<CFIF #getRecordsPersonID.RecordCount# GT #application.showRecords#>
	<!--- 2015-11-13   ACPK    Replaced arrow images with standard buttons --->
        <div class="form-group">
        	<div class="col-xs-12">
        		<CFIF (#Variables.displaystart# + #application.showRecords# - 1) LT #getRecordsPersonID.RecordCount#>
        			<CFOUTPUT>
        				<p>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</p>
        			</CFOUTPUT>
        		<CFELSE>
        			<CFOUTPUT>
        				<p>Record(s) #htmleditformat(Variables.displaystart)# - #getRecordsPersonID.RecordCount#</p>
        			</cFOUTPUT>
        		</CFIF>
        		<CFIF #Variables.displaystart# GT 1>
        			<INPUT TYPE="Image" NAME="Back" CLASS="btn-primary btn" VALUE="Phr_Sys_Back" onClick="javascript:showLoadingIcon();">
        			<!--- NJH 2008/11/19 Bug Fix All Sites Issue 1363 - added onClick function --->
        		</CFIF>
        		<CFIF (#Variables.displaystart# + #application.showRecords# - 1) LT #getRecordsPersonID.RecordCount#>
        			<INPUT TYPE="Image" NAME="Forward" CLASS="btn-primary btn" VALUE="Phr_Sys_Next" onClick="javascript:showLoadingIcon();">
        			<!--- NJH 2008/11/19 Bug Fix All Sites Issue 1363 - added onClick function --->
        		</CFIF>
        	</div>
        </div>
        </CFIF>
</CFIF>

</FORM>


<CFSET frmNextTarget = "main">
<cfinclude template="/data/_editMainEntities.cfm">

<FORM ACTION="/data/dataFrame.cfm" METHOD="post" NAME="orgForm">
	<INPUT TYPE="hidden" NAME="frmsrchOrgID" VALUE="0">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">
</FORM>