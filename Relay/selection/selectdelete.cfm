<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask: selectTask
		Permission: Read & Update
---->

<!--- Amendment History --->
<!--- 1999-05-19	 WAB altered the parameter being passed from frmtag to frmcheckids to bring in line with other forms 
2001-03-22		WAB		added items to dropdowns and at same time standardised on the 'checkboxFunctions' javascript					
					made some mods to form name and things
2005-05-17     WAB   altered so that users can "delete" selections which they don't own - just removes their record from the selectiongroup table
08/06		WAB 	and delete selections attached to downloads					
2008/02/18	WAB    deal with trying to delete selections which are shared with Groups (get a message to say you can't do it)
2011/07/27	NYB		P-LEN024 changed commcheckDetails.cfm to commDetailsHome.cfm

--->


<CFIF NOT checkPermission.UpdateOkay GT 0>
	<CFSET message = "You are not authorised to use this function.">
	<CFINCLUDE TEMPLATE="selectmenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- one can only delete a selection if the following is true:
		* one is the creator
		* the selection is not used anywhere else
			- Communication.SelectionID
--->

<CFSET message = "">
<CFIF IsDefined("frmSelectionCheck")>

	<CFLOOP INDEX="delID" LIST="#frmSelectionCheck#">

		<CFQUERY NAME="checkSelection" datasource="#application.sitedatasource#" DEBUG>
			SELECT s.SelectionID, s.Title, s.CreatedBy
		  	  FROM Selection AS s
			 WHERE s.SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>


		<CFIF checkSelection.RecordCount IS 0 >
			<CFSET message = message & "<LI><P>The selection '#delID#' could not be deleted because it does not exist.">
		
		
		<CFELSEIF checkSelection.CreatedBy IS request.relayCurrentUser.usergroupid>
			<!--- this selection is owned by this user so can be deleted --->
		
			<CFQUERY NAME="checkComm" datasource="#application.sitedatasource#" DEBUG>
				SELECT DISTINCT c.CommID, c.Title
			  	  FROM Communication AS c, CommSelection AS cs
			      WHERE c.CommID = cs.CommID
				   AND cs.SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
				 --   AND c.SendDate IS NOT null
			<!---	   AND c.SendDate >= #CreateODBCDate(Now())# --->
				   AND (cs.Sent in (0,2)  and commformLID <> 4)  <!--- WAB 2004-10-05 just worry if individual selection attached to email but not sent (or queued) and 2005-06-08  don't worry about downloads--->
				   AND cs.CommSelectLID <> 10  <!--- don't worry about bounceback selections --->
			</CFQUERY>

<!--- 			<CFIF checkComm.RecordCount IS NOT 0>
				<CFSET message = message & "<BR><CENTER><TABLE BORDER=0 WIDTH=""80%""><TR><TD><FONT FACE=""Arial, Chicago, Helvetica"" SIZE=2>The selection '#checkSelection.Title#' could not be deleted because it is associated with the following unsent communication(s):<BR><P></FONT></TD>">
				<CFSET message = message & "<TR><TD><FONT FACE=""Arial, Chicago, Helvetica"" SIZE=2><UL>">
				<CFLOOP QUERY="checkComm">
					<CFSET message = message & "<LI>#Title#">
				</CFLOOP>
				<CFSET message = message & "</UL></FONT></TD></TR></TABLE></CENTER>">
 --->
			
			<CFIF checkComm.RecordCount IS NOT 0>
				<CFSET message = message & "<P><LI>The selection '#checkSelection.Title#' could not be deleted because it is associated with the following unsent communication(s):">
				<CFSET message = message & "<UL>">
				<CFLOOP QUERY="checkComm">
					<CFSET message = message & "<LI><A href='../communicate/commDetailsHome.cfm?frmcommid=#commid#'>#htmleditformat(Title)#</A>">
				</CFLOOP>
				<CFSET message = message & "</UL>">
	<!--- 			<CFINCLUDE TEMPLATE="selectmenu.cfm">
				<CF_ABORT>
	 --->	
	 		<CFELSE>
			
	
				
				<!--- now delete the selection --->
				<CFTRANSACTION>
				
					<CFQUERY NAME="delSelectionCriteria" datasource="#application.sitedatasource#" DEBUG>
						DELETE FROM SelectionCriteria
					  	 WHERE SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>
					<CFQUERY NAME="delSelectionTag" datasource="#application.sitedatasource#" DEBUG>
						DELETE FROM SelectionTag
					  	 WHERE SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>
					<CFQUERY NAME="delSelectionGroup" datasource="#application.sitedatasource#" DEBUG>
						DELETE FROM SelectionGroup
					  	 WHERE SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>
					
					<CFQUERY NAME="delSelection" datasource="#application.sitedatasource#" DEBUG>
						DELETE FROM Selection
					  	 WHERE SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>
			
					<!--- 	WAB 9/04 not going to delete commselection entry unless it hasn't been used to send a communication
							save the name of the selection in the table--->					
					<CFQUERY NAME="updComm" datasource="#application.sitedatasource#" DEBUG>
						update CommSelection
						set Title_IfOriginalDeleted =  <cf_queryparam value="#checkSelection.title#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					  	 WHERE SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>

					<CFQUERY NAME="updComm" datasource="#application.sitedatasource#" DEBUG>
						DELETE FROM CommSelection
					  	 WHERE SelectionID =  <cf_queryparam value="#Val(delID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and sent = 0
					</CFQUERY>

				</CFTRANSACTION>

			</CFIF>	<!--- checkComm.RecordCount IS NOT 0 --->			

		<cfelse>
			<!--- this user isn't the owner, but so just remove the rights to this selection --->
			<CFQUERY NAME="updSelectionGroup" result = "updSelectionGroupResult" datasource="#application.sitedatasource#" DEBUG>
			delete from selectionGroup 
			where selectionid = #Val(delID)#
			and usergroupid = #request.relaycurrentuser.usergroupid#
			</CFQUERY >
			
			<cfif updSelectionGroupResult.recordCount GT 0>
			
				<CFSET message = message & "<LI><P>#updSelectionGroupResult.recordCount# You do not own #checkSelection.title#.  You have been removed from the share list	 ">

			<cfelse>
				
				<!--- 
					WAB 2008/02/18
				Need to check for selections which have been shared with groups of people, in which case the user can't 'delete' themselves 
				
				--->
				<CFQUERY NAME="checkSelectionGroup" datasource="#application.sitedatasource#" DEBUG>
				select ug.name from selectionGroup sg inner join rightsGroup rg on sg.usergroupid = rg.usergroupid inner join  usergroup ug on ug.usergroupid = sg.usergroupid
				where selectionid = #htmleditformat(Val(delID))#
				and rg.personid = #htmleditformat(request.relaycurrentuser.personid)#
				</CFQUERY >
				
				<cfif checkSelectionGroup.recordcount is not 0>
					<CFSET message = message & "<LI><P>#checkSelection.title# has been shared with Usergroup(s) #valueList(checkSelectionGroup.name)#.  You cannot be removed from the share list">			
				
				</cfif>
	
			</cfif>
			
			
			
			
			
		</CFIF> <!---checkSelection.RecordCount IS 0--->
	</CFLOOP>

</CFIF>

<CFIF message IS NOT "">
	<CFSET message = "<P>The tagged selections have been deleted except for the following:<UL>" & message & "</UL>">
</CFIF>

	<CFINCLUDE TEMPLATE="selectview.cfm">


<CF_ABORT>

