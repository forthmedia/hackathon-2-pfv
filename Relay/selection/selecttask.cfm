<!--- �Relayware. All Rights Reserved 2014 --->
<!--- generic query to check that some sort of
	  permssions to the task exist

	  later we verify that the user has add/modify permissions if needed


	WAB 2004-09-21  Added counts on location and organisation to the selectionTable
	WAB 2005-06-12	added nvarchar to title and description
 --->

<CFPARAM NAME="img" DEFAULT = "title_selects_e.gif">
<CFPARAM NAME="message" DEFAULT = "">


<CFSET FreezeDate = request.requestTimeODBC>

<CFIF CompareNoCase(Left(FORM.frmID,4),"temp") IS NOT 0 AND (FORM.frmTask IS "save" OR FORM.frmTask IS "review")>
		<!--- if this selection has already been saved --->

		<!--- consider permissions as a separate condition to provide feedback --->
		<CFIF checkPermission.addOkay GT 0 OR checkPermission.updateOkay GT 0>

			<CFQUERY NAME="checkLock" datasource="#application.siteDataSource#">
				SELECT LastUpdated
				  FROM Selection
				 WHERE SelectionID =  <cf_queryparam value="#FORM.frmID#" CFSQLTYPE="CF_SQL_INTEGER" >
				   AND LastUpdated >  <cf_queryparam value="#FORM.frmSelectionLast#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  <!--- already in ODBC format --->
			</CFQUERY>

			<CFIF checkLock.RecordCount IS NOT 0>
				<CFSET message = "The record could not be updated since it has been changed since you last viewed it.">
				<cfinclude template="/templates/message.cfm">
				<CF_ABORT>
			</CFIF>


			<!--- WAB 2004-09-21
			Added columns to count locations and organisation to the selection table
			need to update them here
			--->
			<cfset frmSelectionID = #FORM.frmID#>
			<cfinclude template = "..\templates\qrySelectionRecordCounts.cfm">

			<CFQUERY NAME="saveRealHead" datasource="#application.siteDataSource#">
			UPDATE Selection
			   SET Title =  <cf_queryparam value="#FORM.frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			   	   Description =  <cf_queryparam value="#Left(FORM.frmDesc,80)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				   RecordCount =  <cf_queryparam value="#FORM.frmRecordCount#" CFSQLTYPE="CF_SQL_Integer" > ,
				   SelectedCount =  <cf_queryparam value="#FORM.frmSelectedCount#" CFSQLTYPE="CF_SQL_Integer" > ,
					SelectedCountLoc =  <cf_queryparam value="#recordCounts.locations#" CFSQLTYPE="CF_SQL_Integer" > ,
					SelectedCountOrg =  <cf_queryparam value="#recordCounts.organisations#" CFSQLTYPE="CF_SQL_Integer" > ,
				   RefreshRate =  <cf_queryparam value="#form.frmRefresh#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				   LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				   LastUpdatedBy = #request.relayCurrentUser.usergroupid#  <!--- good practice to include, could be a transferred selection --->
			 WHERE SelectionID =  <cf_queryparam value="#FORM.frmID#" CFSQLTYPE="CF_SQL_INTEGER" >
			   AND CreatedBy = #request.relayCurrentUser.usergroupid#
			   AND LastUpdated <= <cf_queryparam value="#FORM.frmSelectionLast#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</CFQUERY>

			<CFSET searchID = #FORM.frmID#>
		<CFELSE>
			<!--- don't have permissions to modifiy --->
			<CFSET message = "Sorry, you do not currently have permission to save/update selections.">
			<cfinclude template="/templates/message.cfm">
			<CF_ABORT>
		</CFIF>


<CFELSEIF CompareNoCase(Left(FORM.frmID,4),"temp") IS 0 AND (FORM.frmTask IS "save" OR FORM.frmTask IS "review")>
	<!--- this selection is in the temp tables, need to save it --->

	<!--- but, this may have already been saved, so check ---->

	<CFQUERY NAME="checkTemp" datasource="#application.siteDataSource#">
	SELECT * FROM TempSelectionCriteria
	WHERE SelectionID = #request.relayCurrentUser.personid#
	  AND LastUpdated =  <cf_queryparam value="#FORM.frmSelectionLast#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	</CFQUERY>


	<CFIF checkTemp.RecordCount IS NOT 0>
		<cfinclude template="/templates/qrysaveselection.cfm">
	<CFELSE>
		<CFQUERY NAME="getNewlySaved" datasource="#application.siteDataSource#">
		<!--- Max(SelectionID) will be the most recent record added by the user --->
		SELECT Max(SelectionID) AS mostrecent
		FROM Selection
		WHERE Title =  <cf_queryparam value="#FORM.frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" >
 	    AND CreatedBy = #request.relayCurrentUser.usergroupid#
		</CFQUERY>

		<CFIF #getNewlySaved.mostrecent# IS NOT "">
			<CFSET searchID = #getNewlySaved.mostrecent#>
		<CFELSE>
			<CFSET message = "Your selection '#frmTitle#' has already been saved.">
			<CFINCLUDE TEMPLATE="selectmenu.cfm">
			<CF_ABORT>
		</CFIF>
	</CFIF>


</CFIF>

<CFIF Trim(message) IS NOT "">
	<!--- may have encountered an illegal condition in qrysaveselection.cfm --->
	<cfinclude template="/templates/message.cfm">
	<CF_ABORT>

<CFELSEIF FORM.frmTask IS "save">
	<CFSET message = "Your selection '#frmTitle#' has been saved.">
	<CFINCLUDE TEMPLATE="selectmenu.cfm">
	<CFSET message = "">
	<CFINCLUDE TEMPLATE="selectView.cfm">
	<CF_ABORT>

<CFELSEIF FORM.frmTask IS "review">
	<CFINCLUDE TEMPLATE="selectreview.cfm">
	<CF_ABORT>

</CFIF>


