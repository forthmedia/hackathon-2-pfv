<!--- �Relayware. All Rights Reserved 2014 --->
<!---

NJH 2008-07-15 Bug Fix T-10 Issue 747 	Frameset couldn't be got out of. Converted to use IFrame.
WAB 2008-01-05  Bug 1485   (Sony)	The Problem with Iframe Scrolling.  Wrote some JS to solve the problem (just about, see explanation in \ui\extui\applayout.js)
NJH 2012/08/02 CASE 429266 - removed the divs that wrapped the grpList.cfm and the iframe.
NJH 2015/12/23	BF-38 - removed iframe completely. Changed to cfinclude.
 --->

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

<cfif structKeyExists(form,"frmSaveUserGroups")>
	<cfinclude template="/selection/selectGrpTask.cfm">
</cfif>

<cfset variables.frmSelectionID = 0>
<cfif structKeyExists(form,"frmSelectionID") or structKeyExists(url,"frmSelectionID")>
	<cfset variables.frmSelectionID = structKeyExists(form,"frmSelectionID")?form.frmSelectionID:url.frmSelectionID>
</cfif>

<cfinclude template="/selection/selectgrplist.cfm">

<cfif structKeyExists(form,"frmSelectionID") or structKeyExists(url,"frmSelectionID")>
	<cfinclude template="/selection/selectgrpindiv.cfm">
</cfif>