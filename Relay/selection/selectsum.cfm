<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History --->
<!--- 
WAB 2004-09-21  Added counts on location and organisation
2000-02-10  WAB added searches for APR double byte characters  on Sitename,  firstname, lastname
2000-02-06 	WAB added a search between on company names.  Also added personid, locationid
1999-10-08	 KT		Added refresh function
1999-07-15  WAB 
There was a major problem when searching on two (or more) location or 
person flags at the same time.
EG. search on NP + SBS created a query line
AND a.locationID IN (SELECT EntityID FROM BooleanFlagData 
WHERE FlagID in (70) AND flagID IN (82))
[NP = 70, SBS = 82]

A close look at this query shows that this will never bring back any records

What is required is a query of the form 
AND a.locationID IN (SELECT EntityID FROM BooleanFlagData WHERE FlagID in (70))
AND a.locationID IN (SELECT EntityID FROM BooleanFlagData WHERE flagID IN (82))

[There may be a better way, I don't know]

Anyhow I have hacked a correction into the code.  I have done it in the quickest possible way so I haven't changed any
of the general layout of the code, but I think that given this change the whole of the code could be rewritten on a simpler fashion

The changes I have made only effect the boolean stuff, so there is still a problem with the other flag type

WAB 2008-05-07 changed aliasing on main query - com/selections.cfc; templates/qryCreateSelection.cfm; selections/selectSum.cfm must be updated at same time

2008-07-25 NYF replace Save button as an img with Save as input code
2008-10-08 NJH abort the file processing in the cfcatch, as it continued on, resulting in variables being undefined. Also emailed errors@foundation-network.com
			if there was an error in saving/creating the selection.

2012-11-29 PPB Case 432223 set selectionOwnerPersonId for use by qryCreateSelection
2015-11-12  ACPK    Replaced table with Bootstrap
--->




<!--- user rights to access:
		SecurityTask:
		Permission:
---->

<cfset FreezeDate = request.requestTimeODBC>

<!--- 'Added try / catch to manage timeout error' [godfrey.smith]- March 19, 2008, 13:59:18 PM --->
<cftry>
	<cftransaction>
		<!--- save the criteria in a temp table if not yet saved --->

		<!--- only do this if ... --->
		<cfif NOT IsDefined("frmID")>
			<cfset tempselect = "Temp"><!--- used as a prefix to tablenames --->
			<cfinclude template="/templates/qrysavetempsel.cfm">
			<!--- searchID was set in qrysaveselection.cfm ---->
			<cfset docriteria = "yes">
		<cfelse>
			<cfset tempselect = ""><!--- used as prefix to tablenames --->
			<cfset searchID = #frmID#>
		</cfif>
		<!--- searchID was set in qrysaveselection.cfm ---->

		<cfquery name="getCriteria" datasource="#application.sitedatasource#" DEBUG>
			SELECT SelectionField, Criteria, criteria1
			FROM #Variables.tempselect#SelectionCriteria 
			WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<!--- assume that criteria is never null or zero-length string --->
		<!--- change the delimiters of the lists to #application.delim1# --->	
		<cfset savedFields = ValueList(getCriteria.SelectionField,application.delim1)>
		<cfset savedCriteria = ValueList(getCriteria.Criteria,application.delim1)>
		<cfset savedCriteria1 = ValueList(getCriteria.Criteria1,application.delim1)>
	
		<!--- only do the following if people or criteria need to be saved --->
		<cfif IsDefined("docriteria")>

			<!--- we are concerned with two types of tasks,
				first is whether the user can see and or edit the person
				the second is whether the user can do selections
				This query must be customised for all possible SelectionFields
				Currently:
			  	frmCountryID
				frmPartnerType
				frmCompanyName
				frmRegionState
					New fields: 
						frmJobFunction  XXXX Deleted 1998-07-15
						frmPersLanguage
						frmPersName
						frmPersEmail
						frmPersLastUpdateddate
						frmPersLastUpdatedby
						frmDataSource
						frmLocationFax
						frmPostalCode
						frmLocationLastUpdateddate
						frmLocationLastUpdatedby
						frmDirectPartner
					Person Flags
						names from SelectionField table
			--->
				 
			<cfinclude template="/templates/qrygetcountries.cfm">

			<!--- **************
				NOTE NOTE NOTE
				any mods to this query must be made in selections\selectrefresh.cfm as well as
				scheduled/refresh.cfm 
				No longer - now in templates directory		
			--->

			<!--- 
				debug
				<cfoutput>#Variables.savedFields#. #flagListLocation#
				<cfset flag = "TEXT_1076">
				<cfset crit= ListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"#Flag#","#application.delim1#"),"#application.delim1#")>
				#crit#
			</cfoutput> --->
			<cfset variables.usergroupid = request.relayCurrentUser.usergroupid> <!--- wab added, used for trying to convert the IN (countrylist) to a join --->
			<cfset selectionOwnerPersonId = request.relayCurrentUser.PersonID> 			<!--- 2012-11-29 PPB Case 432223 set selection owner for rights snippet in qryCreateSelection.cfm --->
	
			<cfinclude template="..\templates\qryCreateSelection.cfm">
			
			<!--- now Variables.CountryList --->
			<cfquery name="getPeople" datasource="#application.SiteDataSource#" timeout="60">
				INSERT INTO #Variables.tempselect#SelectionTag (SelectionID, EntityID, Status, CreatedBy, Created, LastUpdatedBy,LastUpdated)
				SELECT DISTINCT <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					p.PersonID,
					1,
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
					#FreezeDate#
					#preserveSingleQuotes(selectionQrySnippet_sql)#
			</cfquery>
			<!--- finished saving criteria --->
		</cfif>
	
		<cfquery name="getPeopleCount" datasource="#application.siteDataSource#">
			SELECT Count(*) AS reccount
			FROM #Variables.tempselect#SelectionTag
			WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND Status > 0 <!--- want to get status of 2 etc --->
		</cfquery>
	
		<cfif Variables.tempselect IS "">
			<cfquery name="getSelections" datasource="#application.SiteDataSource#">
				SELECT s.SelectionID,
					  s.Title,
					  s.Description,
					  s.RefreshRate,
					  s.TableName,
					  s.LastUpdated,
					  (SELECT Count(EntityID)
					     FROM #Variables.tempselect#SelectionTag AS st, Person AS p
			  		    WHERE st.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						  AND st.EntityID = p.PersonID
						  AND p.Active =1 <!--- KT 1999-10-11 always want active people --->
						<!--- KT 1999-10-11 
						<cfif isDefined("frmpersinactive")> p.Active <> -1 <cfelse> p.Active <> 0</cfif>
						--->
						  ) AS allPersons,
					  	<!--- () AS tagLocations, --->
					  (SELECT Count(EntityID)
					     FROM #Variables.tempselect#SelectionTag AS st, Person AS p
			  		    WHERE st.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					      AND st.Status > 0
						  AND st.EntityID = p.PersonID
						  AND p.Active =1 <!--- KT 1999-10-11 always want active people --->
						<!--- KT 1999-10-11 
						<cfif isDefined("frmpersinactive")>p.Active <> -1	<cfelse>p.Active <> 0 </cfif> 
						--->
						) AS tagPersons
				FROM Selection AS s
				WHERE s.CreatedBy = #request.relayCurrentUser.usergroupid#
				AND s.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				<!---  ORDER BY s.Title --->
			</cfquery>
			
			<cfset FreezeDate = getSelections.LastUpdated>
	
		<cfelse>
	
			<cfquery name="getSelections" datasource="#application.siteDataSource#">
				SELECT #Variables.searchID#,
				  '' AS Title,
				  '' AS Description,
				  'N' AS RefreshRate,
				  'Person' AS TableName,
				   Count(EntityID) AS allPersons,
				  (SELECT Count(EntityID)
				     FROM #Variables.tempselect#SelectionTag
			  	    WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				      AND Status > 0) AS tagPersons
					  
  					FROM #Variables.tempselect#SelectionTag
	  				WHERE SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>

		</cfif>
	
		<cfquery name="getNumLocs" datasource="#application.sitedatasource#" DEBUG>
			SELECT DISTINCT p.LocationID
			FROM Person AS p, #Variables.tempselect#SelectionTag as a
			WHERE p.PersonID = a.EntityID
			AND a.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND  a.Status > 0
		</cfquery>

		<cfquery name="getNumOrgs" datasource="#application.sitedatasource#" DEBUG>
			SELECT DISTINCT p.organisationid
			FROM Person AS p, #Variables.tempselect#SelectionTag as a
			WHERE p.PersonID = a.EntityID
			AND a.SelectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND a.Status > 0
		</cfquery>
	
		<!--- 2008-12-16 - added IF statment to stop query breaking: --->
		<cfif application.com.dbtools.tableExists(tableName="selectionFolder")>
			<cfquery name="getProjectFolders" datasource="#application.siteDataSource#">
				SELECT *
				FROM selectionFolder
				WHERE personID = #request.relaycurrentuser.personid#
				ORDER BY name
			</cfquery>
	
			<cfquery name="getSelectionsProjectFolder" datasource="#application.siteDataSource#">
				SELECT	*
				FROM	selectionFolder sf inner join
						selectionFolderItem sfi on
						sf.selectionFolderID = sfi.selectionFolderID
				WHERE 	sf.personID = #request.relaycurrentuser.personid# and
						sfi.selectionID =  <cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>		
	</cftransaction>

	<cfcatch type="any">
		<!---  WAB 2012-10-29 CASE 431540. Added logging of errors (and added an errorID [sorry - rather weak but better than nothing])--->
		<cfset application.com.request.extendTimeOutIfNecessary(60)>
		<cfif findnocase("Execution timeout expired",cfcatch.message) neq 0> <!--- NJH 2009-01-21 Bug Fix All Sites Issue 1543 - changed to cfcatch.message - was cfcatch.cause.message --->
			<p>The server cannot perform the selections search you requested due to high load.<br />
			Please return to the search page and increase the number of search parameters to narrow the search or try again with your existing criteria.<br />
			<a href="javascript:history.back(-1);">Search Again</a></p>
			<cfset errorID = application.com.errorHandler.recordRelayError_Warning(severity="Error",Type="Selection Search Timeout",caughtError=cfcatch)>
		<cfelse>
			<p>Unexpected Error<br/>
			<a href="javascript:history.back(-1);">Search Again</a></p>
			<cfset errorID = application.com.errorHandler.recordRelayError_Warning(severity="Error",Type="Selection Search Error",caughtError=cfcatch)>
		</cfif>
		<cfoutput><BR>Error Reference #errorID#</cfoutput> 
			<CF_ABORT>
	</cfcatch>
</cftry>
<!--- /'Added try / catch to manage timeout error' [godfrey.smith]- March 19, 2008, 13:59:18 PM --->

<!--- get the text for the search criteria
		will be returned in 
			getSearchText.criteria
			getSearchText.description
--->

<cfif #getSelections.RecordCount# IS NOT 0>
	<cfset qryTitle = getSelections.Title>
	<cfset qryDesc = getSelections.Description>
	<cfset qryRefreshRate = getSelections.RefreshRate>
</cfif>
	
<!--- 2008-12-16 - added IF statment to stop query breaking: --->
<cfif application.com.dbtools.tableExists(tableName="selectionFolder")>
	<cfif getSelectionsProjectFolder.recordCount gt 0>
		<cfset qryFolderID = getSelectionsProjectFolder.selectionFolderID>
	</cfif>
</cfif>
	
<cfparam name="qryTitle" default="">
<cfparam name="qryDesc" default="">
<cfparam name="qryRefreshRate" default="N">
<cfparam name="qryFolderID" default="">
			


<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
	<script type="text/javascript">
	<!--

		function verifyForm(task) {
		var form = document.selections;
		form.frmTask.value = task + "";
		var msg = "";
		if (form.frmTitle.value == "" || form.frmTitle.value == " ") {
			msg += "* Title\n";
		}
		if (form.frmDesc.value == "" || form.frmDesc.value == " ") {
			msg += "* Description\n";
		}		
		/*if (form.frmNewFolder.value == "" && form.frmFolder.selectedIndex == 0) {
			msg += "* Folder\n";
		}*/
		if (msg != "") {
			alert("\nYou must provide the following information for your selection:\n\n" + msg);
		} else {
			form.submit();
		}
	}

	//-->
	</script>
</cf_head>

<cfparam name="frmruninpopup" default=""> 



<cfif fileexists("#application.userFilesAbsolutePath#content\CFTemplates\selecttophead.cfm")>
	<cfinclude template="#application.userFilesPath#/content/cftemplates/selecttophead.cfm">
<cfelse>
<!--- <CFINCLUDE TEMPLATE="selecttophead.cfm"> --->
</cfif>

	<cfinclude template="/templates/qrygetsearchtext.cfm">

	<cfif getSelections.allPersons IS 0>
		<p>No records found</p>
	<cfelse>
		<cfif IsDefined("getNumLocs.RecordCount")>
			<cfif getSelections.allPersons IS 1>
				<cfoutput>
					<p>#htmleditformat(getSelections.allPersons)# person was found in #getNumLocs.RecordCount# location.</p>
				</cfoutput>
			<cfelse>
				<cfoutput>
					<p>#htmleditformat(getSelections.allPersons)# people were found. </p>
					<p>#getNumLocs.RecordCount# location<cfif #getNumLocs.RecordCount# gt 1>s</cfif> - #getNumorgs.RecordCount# organisation<cfif #getNumorgs.RecordCount# gt 1>s</cfif></p>	
				</cfoutput>
			</cfif>
		</cfif>
		<br />
		<form class="form-horizontal" action="/selection/selecttask.cfm?mode=debug" method="POST" name="selections">
			<cfoutput>
				<CF_INPUT type="HIDDEN" name="frmID" value="#Variables.tempselect##Variables.searchID#">
				<input type="HIDDEN" name="frmTask" value="save">
				<CF_INPUT type="HIDDEN" name="frmRecordCount" value="#getSelections.allPersons#">
				<CF_INPUT type="HIDDEN" name="frmSelectedCount" value="#getSelections.tagPersons#">
				<CF_INPUT type="HIDDEN" name="frmSelectionLast" value="#request.requestTime#">
			</cfoutput>
		
			<h4>STORE SELECTION</h4>
				<div class="form-group">
				    <div class="col-xs-12">
						<label for="frmTitle">Title:</label>		
						<CF_INPUT type="text" id="frmTitle" name="frmTitle" value="#qryTitle#" size="25" maxlength="50" />
                    </div>
				</div>
				<div class="form-group">
                    <div class="col-xs-12">
						<label for="frmDesc">Description:</label>
						<CF_INPUT type="text" id="frmDesc" name="frmDesc" value="#qryDesc#" size="25" maxlength="80" />
					</div>
                </div>
				<cfif qryRefreshRate EQ "X">
					<input type="hidden" name="frmRefresh" value="X" />
				<cfelse>
					<div class="form-group">
                        <div class="col-xs-12">
							<label for="frmRefresh">Refresh: </label>
							<select id="frmRefresh" class="form-control" name="frmRefresh" size="1">
								<option value="N" <cfif qryRefreshRate eq "N"> selected </cfif> >Never</option>
								<option value="A" <cfif qryRefreshRate eq "A"> selected </cfif> >Ad Hoc</option>
								<option value="D" <cfif qryRefreshRate eq "D"> selected </cfif> >Daily</option>
								<option value="W" <cfif qryRefreshRate eq "W"> selected </cfif> >Weekly</option>
								<option value="M" <cfif qryRefreshRate eq "M"> selected </cfif> >Monthly</option>
							</select>
						</div>
	                </div>
				</cfif>
				<!--- 2008-12-16 - added IF statment to stop query breaking: --->
				<cfif application.com.dbtools.tableExists(tableName="selectionFolder")>
					<cfif getProjectFolders.recordCount gt 0>
						<div class="form-group">
                            <div class="col-xs-12">		
								<select class="form-control" name="frmFolder" size="1">
									<option value="">--- Select a Folder ---</option>
									<cfoutput query="getProjectFolders">
									<option value="#selectionFolderID#"<cfif qryFolderID eq selectionFolderID> selected</cfif>>#htmleditformat(name)#</option>
									</cfoutput>
								</select>
							</div>
                        </div>
					</cfif>
	
				</cfif>
            <div class="form-group">
            	<div class="col-xs-12">
            		<input type="BUTTON" class="btn-primary btn" name="btnSave" value=" Save " onClick="verifyForm('save');">
            		<span class="selection_padding_betw_buttons">&nbsp;</span>
            		<input type="BUTTON" class="btn-primary btn" name="btnReview" value=" Save and Review " onClick="verifyForm('review');">
            	</div>
            </div>

		</form>

		<p>Note that this is a selection of People.</p>  
		<p>Any locations which do not contain any people will not be reflected in the above count</p>
		<p>There is a new option in the Data Search Screen which allows you to search for all "empty" locations</p>
	</cfif>


