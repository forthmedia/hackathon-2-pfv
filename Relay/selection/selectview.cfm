<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History
	1999-03-29	WAB	Added 'ltrim(s.Title) as title'  to query so that leading blanks don't cause probs
	1999-05-19 WAB Added dropdown functions, moved delete functionality to it, added funcitonm to create a selection of balnk emails
	2000-05-08 WAB added #message# to top
2001-03-22		WAB		added items to dropdowns and at same time standardised on the 'checkboxFunctions' javascript
					made some mods to form name and things
2001-03-23		WAB		Added the last refreshed date into the report
2001-04-09		WAB		Altered query bring back selections to bring back correct list also added use of rightsgroup
2005-05-17		WAB 	check box appears next to all selections now (whether owner or not).  Deleting a selection you do not own, removes you from the selectionGroup table
2005-09-26	WAB		added stuff to indicate if a selection is owned by someone else or shared with other users
2005-09-29		WAB		Changed numbers to only show number of people selected in selection.  Another column shows number of people removed
2008/02/19	WAB		Mod so that selections shared with Groups are indicated
2008/11/19	NJH		Bug Fix Issue 1363 All Sites - Opened the selections into a new tab.
30/09/2014 		SB		Added div container for styling
2015-11-12      ACPK    Added Bootstrap to table, removed inline styling
	--->

<!--- user rights to access:
		SecurityTask:
		Permission:
---->
<div id="selectviewContainer">
<CFSET FreezeDate = request.requestTimeODBC>
<cf_includejavascriptonce template="/javascript/extExtension.js" > <!--- NJH 2008/11/19 Issue 1363 All Sites --->
<cf_includeJavascriptOnce template = "/javascript/checkBoxFunctions.js">

<!--- get the user's countries --->
<cfinclude template="/templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->

<!--- one can only edit one's own selections defined by CreatedBy
	 otherwise rights are only view selections & sharing and
 --->

<!--- 2001-04-09
WAB altered query to use rights group
also brings back correct list when selections are shared/copied etc (I think)
2010/04/14 GCC LID3106 removed an incorrect alias in the order by clause which broke in sql 2005 but was ok in sql 2000
 --->
<CFQUERY NAME="getSelections" DATASOURCE="#application.SiteDataSource#">
	SELECT distinct s.SelectionID,
			s.RefreshRate,
			s.RecordCount as allPersons,
			isNUll(s.SelectedCount,s.RecordCount) as tagPersons,    <!--- odd construction because s.SelectedCount is not always populated - therefore assume that there are none unselected --->
			s.SelectedCountLoc ,
			s.SelectedCountOrg ,
			s.RecordCount - isNUll(s.SelectedCount,s.RecordCount) as suppressedPersons,
			ltrim(s.Title) as title,
			s.Description,
			s.TableName,
			s.Created AS datecreated,
			s.LastUpdated,
			s.CreatedBy AS owner,
			ug.name as ownerName,
			s.LastRefreshed,
			(select count(1) from selectionGroup sg2 inner join userGroup ug2 on sg2.usergroupid = ug2.usergroupid  where sg2.selectionid = s.selectionid and sg2.usergroupid <> #request.relayCurrentUser.usergroupid# and sg2.usergroupid <> s.createdby and ug2.personid is not null ) as UserSharedWithCount,
			<!--- WAB added code to show if the currentuser is seeing this selection because they are a member of a userGroup with which it has been shared --->
			(select count(1) 		from selectionGroup sg3 inner join userGroup ug3 on sg3.usergroupid = ug3.usergroupid inner join rightsgroup rg3 on rg3.usergroupid = ug3.usergroupid where sg3.selectionid = s.selectionid and ug3.personid is null and rg3.personid =  #request.relaycurrentuser.personid#  ) as UserGroupSharedWithCount,
			(select top 1 ug4.name 	from selectionGroup sg4 inner join userGroup ug4 on sg4.usergroupid = ug4.usergroupid inner join rightsgroup rg4 on rg4.usergroupid = ug4.usergroupid where sg4.selectionid = s.selectionid and ug4.personid is null and rg4.personid =  #request.relaycurrentuser.personid# ) as UserGroupSharedWithName    <!--- just gives one --->


  	  FROM Selection AS s
	  	inner join SelectionGroup AS sg on sg.SelectionID = s.SelectionID
		inner join RightsGroup rg on sg.usergroupid = rg.usergroupid
		left join usergroup ug on s.createdby = ug.usergroupid
	 WHERE rg.personID = #request.relaycurrentuser.personid#
   ORDER BY Title
</CFQUERY>




<cf_head>

<SCRIPT type="text/javascript">
	function openWin( windowURL,  windowName, windowFeatures ) {
		return window.open( windowURL, windowName, windowFeatures );
		}
</script>

</cf_head>


<CFSET currentPage = "selectView">

<div id="selectViewRow1" class="grey-box-top">
	<CFINCLUDE template="selectionviewpopup.cfm">
</div>



<CFIF isDefined("Message") and message is not "">
    <CFOUTPUT>#application.com.relayUI.message(message=message)#</cfoutput>
</cfif>

<CFIF #getSelections.RecordCount# IS 0>
	<FONT FACE="Arial, Chicago, Helvetica" SIZE=2><P>Phr_Sys_Noselectionsfound</FONT>
<CFELSE>

	<CFSET RefreshType = StructNew()>
	<CFSET tmp = StructInsert(RefreshType,"N","Never")>
	<CFSET tmp = StructInsert(RefreshType,"A","Ad Hoc")>
	<CFSET tmp = StructInsert(RefreshType,"D","Daily")>
	<CFSET tmp = StructInsert(RefreshType,"W","Weekly")>
	<CFSET tmp = StructInsert(RefreshType,"M","Monthly")>
	<CFSET tmp = StructInsert(RefreshType,"X","Not Refreshable")>

<!--- 	<INPUT TYPE="BUTTON" NAME="btnDelete" VALUE="Delete Tagged Selections" onClick="checkDelete(this.form);"> --->

	<TABLE CLASS="table table-hover">
		<FORM NAME="mainForm" ACTION="selectdelete.cfm" METHOD="POST">
		<TR>
			<TH><cf_input type="checkbox" id="tagAllSelections" name="tagAllSelections" onclick="tagCheckboxes('frmSelectionCheck',this.checked)"></TH>
			<TH>&nbsp;&nbsp;&nbsp;&nbsp;Phr_Sys_Titleclickviewpeople&nbsp;&nbsp;&nbsp;</TH>
			<TH>Phr_Sys_PeopleSelected</TH>
			<TH>Phr_Sys_Location<BR>phr_sys_count</TH>
			<TH>Phr_Sys_Organisation<BR>phr_sys_count</TH>
			<TH>People Removed</TH>
			<TH>Phr_Sys_DateCreatedUpdated</TH>
<!--- 			<TH>Drill Down</TH>		 --->
			<TH>Phr_Sys_EditTitleRefresh</TH>
			<TH>Phr_Sys_Sharing</TH>
			<TH>Phr_Sys_Refresh</TH>
		</TR>

		<CFSET checknum = 0>

		<CFOUTPUT QUERY="getSelections">
			<cfif currentrow mod 2 is not 0>
	            <cfset rowclass="oddrow">
	        <cfelse>
	            <cfset rowclass="evenrow">
	        </cfif>
	        <TR class="#rowclass#">
<!--- WAB, 2005-05-17 remove this test, there are function in the drop down which do not require ownership
				<CFIF owner IS request.relayCurrentUser.usergroupid> --->
						<TD><CF_INPUT TYPE="checkbox" NAME="frmSelectionCheck" VALUE="#SelectionID#"></TD>
					<CFSET checknum = checknum + 1>
<!--- 				<CFELSE>
					<TD ALIGN="LEFT">&nbsp;</TD>
				</CFIF>	 --->
				<TD>
				<!--- NJH 2008/11/19 - Issue 1363 All Sites - open the selection review in a new tab --->
				<A HREF="javascript:void(openNewTab('Selection#SelectionID#Review','#jsStringFormat(Title)#','/selection/selectreview.cfm?frmSearchID=#SelectionID#',{iconClass:'accounts',showLoadingIcon:true,tabTip:'#jsStringFormat(Title)#'}))" CLASS="smallLink"><IMG SRC="/images/MISC/iconpeople.gif" align="left" WIDTH="32" HEIGHT="32" ALT="Edit/View" BORDER="0"></A>
				<A HREF="javascript:void(openNewTab('Selection#SelectionID#Review','#jsStringFormat(Title)#','/selection/selectreview.cfm?frmSearchID=#SelectionID#',{iconClass:'accounts',showLoadingIcon:true,tabTip:'#jsStringFormat(Title)#'}))" CLASS="smallLink">#HTMLEditFormat(Title)#</A>
				<!--- <A HREF="selectreview.cfm?frmSearchID=#SelectionID#" CLASS="smallLink"><IMG SRC="/images/MISC/iconpeople.gif" align="left" WIDTH="32" HEIGHT="32" ALT="Edit/View" BORDER="0"></A>
				<A HREF="selectreview.cfm?frmSearchID=#SelectionID#" CLASS="smallLink">#HTMLEditFormat(Title)#</A> --->
					<CFIF owner IS not request.relayCurrentUser.usergroupid>
						<BR>(owned by #htmleditformat(ownerName)#)
						<!--- 2008/02/19 WAB  - show if shared with group		 --->
						<cfif UserGroupSharedWithCount is not 0>
							<BR>(shared with Group #htmleditformat(UserGroupSharedWithName)# )
						</cfif>
					<cfelseif UserSharedWithCount is not 0 or UserGroupSharedWithCount is not 0>
						<BR>(shared)
					</CFIF>
				</TD>
				<TD>#htmleditformat(tagPersons)#</TD>
				<TD>#htmleditformat(selectedCountLoc)#</TD>
				<TD>#htmleditformat(selectedCountOrg)#</TD>
				<TD>#htmleditformat(suppressedPersons)#</TD>

				<TD>#DateFormat(datecreated, "dd-mmm-yy")# /<br> #DateFormat(LastUpdated, "dd-mmm-yy")#</TD>

<!--- 				<TD ALIGN="CENTER"><A HREF="selectreviewnew.cfm?frmselectionid=#SelectionID#"><IMG SRC="/images/misc/redDot.gif" WIDTH="19" HEIGHT="19" ALT="Edit/View" BORDER="0"></A></TD>	 --->
				<CFIF owner IS request.relayCurrentUser.usergroupid>
					
					<TD>
						<A HREF="selectsum.cfm?frmID=#SelectionID#">
							<!-- <IMG SRC="/images/misc/iconEditPencil.gif" WIDTH=19 HEIGHT=19 ALT="Edit Title" BORDER="0"> -->
							<i class="fa fa-pencil"></i>
						</A>
					</TD>
					
					<TD>
						<A HREF="selectgrpframe.cfm?frmSelectionID=#SelectionID#">
							<!-- <IMG SRC="/images/misc/bluedot.gif" WIDTH=19 HEIGHT=19 ALT="Sharing" BORDER="0"> -->
							<i class="fa fa-share-alt"></i>
						</A>
					</TD>

				<CFELSE>
					<!-- <TD>&nbsp;</TD> -->
					<TD >
						<A HREF="selectgrpview.cfm?frmSelectionID=#SelectionID#">
							<!-- <IMG SRC="/images/misc/bluedot.gif" WIDTH=19 HEIGHT=19 ALT="Sharing" BORDER="0"> -->
							<i class="fa fa-refresh"></i>
						</A>
					</TD>
				</CFIF>
				
				<TD>
					<CFIF RefreshRate NEQ "" AND RefreshRate NEQ "N" AND RefreshRate NEQ "X">
						<A HREF="selectrefresh.cfm?frmSelectionID=#SelectionID#">
							<!-- <IMG SRC="/images/misc/bluedot.gif" WIDTH=10 HEIGHT=10 ALT="Refresh" BORDER="0"> -->
							<i class="fa fa-refresh"></i>
						</A>
					</CFIF>
					<p>
						<CFIF RefreshRate NEQ "">#StructFind(RefreshType,RefreshRate)#<CFELSE>not set</CFIF>
					</p>
				</TD>

			</TR>
		</CFOUTPUT>
		<CF_INPUT TYPE="HIDDEN" NAME="checknum" VALUE="#checknum#">
		</FORM>
	</TABLE>

</CFIF>
</div>