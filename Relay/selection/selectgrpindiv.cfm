<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
        SecurityTask:
        Permission:
---->
<!---
WAB 2005-04-14   Added usergroups to the list of users (sorry very much a quick hack)
    2005-05-16     Made it a bit neater
WAb 2009-01-05 Bug 1485 added default for frmSelectionID
2015-11-13  ACPK    Replaced tables with Bootstrap

 --->

<CFPARAM NAME="frmSelectionID" DEFAULT="0">
<!--- <cfset application.com.request.setTophead(topHeadCfm="")>
<cfset application.com.request.setDocumentH1(show=false)> --->

<CFIF frmSelectionID IS 0>
   <CFSET message = "Please choose a selection.">
  <cfset application.com.relayUI.setMessage(message=message,type="info")>
	<cf_abort>
</CFIF>

<!--- only those locations that are active
        could also search for locations with active people --->


   <CFQUERY NAME="findCountries" datasource="#application.siteDataSource#">
       SELECT DISTINCT l.CountryID
       FROM Location AS l, SelectionTag AS st,
            Person AS p
      WHERE st.EntityID = p.PersonID
        AND p.LocationID = l.LocationID
        AND st.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
   </CFQUERY>

   <!--- NJH 2009-01-26 Bug Fix Release Issue 116 --->
   <cfif findCountries.recordCount gt 0>
       <cfset locCountryList = valueList(findCountries.countryid)>
   <cfelse>
       <cfset locCountryList = 0>
   </cfif>



<!--- ==============================
        same query as in selectgrplist.cfm selections
      ============================== ---->
<!--- GCC - 2005-03-01 - Changed to offer the same list as transfering selections (selecttrans.cfm)
Removed commenting out of p.personid <> curent user to remove self fromthe list
commented out countries in selection line
--->

   <CFQUERY NAME="getUsers" datasource="#application.siteDataSource#">
   SELECT DISTINCT ug.UserGroupID, p.FirstName + ' ' + p.LastName as Name, 2 as sortOrder,
       <!--- note these two fields below aren't used, but could be to indicate which users in the list share countries with the current user --->
       case when exists (select 1 from rights r inner join RightsGroup rg on r.usergroupid = rg.usergroupid and rg.personid = ug.personid and r.countryid in (#request.RelayCurrentUser.countryList#)) then 1 else 0 end as UserSharesCountriesWithOwner,
       case when exists (select 1 from rights r inner join RightsGroup rg on r.usergroupid = rg.usergroupid and rg.personid = ug.personid and r.countryid  in ( <cf_queryparam value="#locCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) then 1 else 0 end as UserSharesCountriesWithSelection

    FROM Person AS p inner join
         UserGroup AS ug on p.PersonID = ug.PersonID
<!---         Rights AS r,
          RightsGroup AS rg,
 --->
   WHERE p.Active <> 0 <!--- true --->
     AND p.Password <>  ''
     AND p.PersonID <> #request.relayCurrentUser.personid#
<!--- Not sure if we actually need this
        for transferring s selection --->
<!---     AND p.PasswordDate > #CreateODBCDateTime(Now())# --->
     AND p.LoginExpires > #CreateODBCDateTime(Now())#
<!--- To here --->

<!---     AND r.UserGroupID = ug.UserGroupID  <!--- to get country rights via Rights --->
      AND p.PersonID = rg.PersonID
      AND rg.UserGroupID = ug.UserGroupID
      AND r.CountryID IN (#CountryList#)
      <!--- AND r.CountryID IN (#Variables.sCountries#) ---> <!--- countries in the selection --->
 --->
   ORDER BY Sortorder, Name
   </CFQUERY>

   <CFQUERY NAME="getUserGroups" datasource="#application.siteDataSource#">
   select distinct ug.usergroupid , ug.name
   from usergroup ug
   where personid is null
   ORDER BY Name
   </CFQUERY>


   <CFQUERY NAME="getCurrentGrp" datasource="#application.siteDataSource#">
       SELECT UserGroupID
         FROM SelectionGroup
        WHERE SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
   </CFQUERY>

   <CFIF getCurrentGrp.RecordCount IS 0>
       <CFSET user_list = 0>
   <CFELSE>
       <CFSET user_list = ValueList(getCurrentGrp.UserGroupID)>
   </CFIF>



<cf_head>
<SCRIPT type="text/javascript">

<!--
    function checkForm(){
        var form = document.ThisForm;
        form.submit();
    }


    function doSelf(checkbox) {
        if (!checkbox.checked) {
            alert("\nAs the owner, you must always be selected.");
            checkbox.checked = true;
        }
    }

//-->
</SCRIPT>

</cf_head>


<CFIF findCountries.RecordCount GT 0>

   <CFIF getUsers.RecordCount GT 0>

       <CFIF IsDefined("message")>
           <cfparam name="messageType" default="success">
           <cfset application.com.relayUI.setMessage(message=message,type=messageType)>
       </CFIF>

       <FORM CLASS="form-horizontal" METHOD="POST" NAME="ThisForm">
           <CF_INPUT TYPE="HIDDEN" NAME="frmSelectionID" VALUE="#frmSelectionID#">
		      <div class="form-group">
		      	<div class="col-xs-12">
		      		<cf_input type="submit" id="frmSaveUserGroups" name="frmSaveUserGroups" class="btn-primary btn" <!--- onClick="JavaScript:checkForm();" ---> value="Save">
		      		<!--- <A HREF="JavaScript:checkForm();"><IMG TYPE="IMAGE" SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_save_e.gif"  BORDER=0></A> --->
		      	</div>
		      </div>

           <cfloop index= "type" list = "getusergroups,getusers">
                <cfif Type is "getUserGroups">
                    <h2>Usergroups</h2>
                <cfelse>
                    <h2>Users</h2>
                </cfif>

	            <cfset theQuery = variables[type]>  <!--- this sets theQuery to either the getuser or gteUserGroups query--->

	            <!--- determine how many records per column
	            this will even work if 1 record --->
	            <cfset colLength = structNew()>
	            <CFSET percol = theQuery.RecordCount / 3>
	            <CFSET colLength[1] = Int(percol) >
	            <CFSET colLength[2] = Int(percol)>
	            <CFSET colLength[3] = Int(percol)>
	            <CFIF theQuery.RecordCount MOD 3 IS 1>
	                <CFSET colLength[1] = colLength[1] + 1>
	            <CFELSEIF theQuery.RecordCount MOD 3 IS 2>
	                <CFSET colLength[1] = colLength[1] + 1>
	                <CFSET colLength[2] = colLength[2] + 1>
	            </CFIF>

	         	<div class="row">
	                <!--- CASE 437942 WAB 2013-11-12 was missing out first item in each column (subsequent to the first). Modified how startRow calculated --->
	                <cfset startRow = 1>
	                <cfloop index = "column" list = "1,2,3">
		            	<!--- column one --->
		                <div class="col-xs-12 col-sm-4">
		                    <CFOUTPUT QUERY="theQuery" STARTROW=#startRow# MAXROWS=#colLength[column]#>
		                        <CFSET owner="">
		                        <div class="form-group">
		                            <div class="col-xs-12">
		                              <div class="checkbox">
		                                <label>
		                                    <CFIF UserGroupID IS request.relayCurrentUser.usergroupid>
		                                        <!---do not provide a checkbox for the user --->
		                                        <CF_INPUT TYPE="Checkbox" NAME="frmUserID" VALUE="#UserGroupID#" CHECKED onClick="doSelf(this);">
		                                        <CFSET owner=" (Owner)">
		                                    <CFELSE>
		                                        <CF_INPUT TYPE="Checkbox" NAME="frmUserID" VALUE="#UserGroupID#" CHECKED="#iif(ListFind(Variables.user_list,UserGroupID) IS NOT 0,true,false)#">
		                                    </CFIF>
		                                    #HTMLEditFormat(Name)# #htmleditformat(Variables.owner)#
		                                <label>
		                              </div>
		                            </div>
		                        </div>
		                    </CFOUTPUT>
		        		</div>
		                <cfset startRow = startRow + colLength[Column]>
	                </cfloop>
			    </div>
           </cfloop>

       </FORM>

   <CFELSE>
        <cfset application.com.relayUI.setMessage(message="There are no other users who have access to any of the countries in which the selection individuals are located.",type="info")>
   </CFIF>

<CFELSE>
    <cfset application.com.relayUI.setMessage(message="Sorry, none of the individuals are in countries to which you have access.",type="info")>
</CFIF>