<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

2008/03/25   WAB contextmenu XML requires proper closing tags for FF xml data island
2011/11/09	RMB	LID8115  staging block chart as it was using cfide added code to move chatt to temp location
 --->
<cfcomponent displayname="Reporting" hint="<b>Written by David A McLean in June 2005 for Foundation Network Limited.</b><br><br> Performs Various Reporting Functions that are either entirely generic or relay generic. <br><br>Its probably better to stick to relay generic where possible becuase there is unlikely to be any requirement beyond that. The concept is that this component should be extended to meet new reporting requirements as they arise by adding conditions to existing methods or adding whole new methods. There will be a finite set of report presentations demanded by relay at any point in time, and these are all hosed in here. I would recommend that everytime reporting is visted this cfc is visited so that all reporting is integrated including relay core code such as tableFromQueryObject.<br><br>
<table cellpadding='5'>
	<tr><th colspan='3' height='36'>Standardised Views for Resuse Associated with this component</hd></tr>
	<tr>
		<td align='left'><strong>View Name</strong></td><td align='left' colspan='2'><strong>Usage</strong></td>
	</tr>
	<tr>
		<td valign='top' align='left'>vR_FlagsByOrgFlagGroup_RegOrgs_week_country</td>
		<td valign='top' colspan='2' align='left'>
		This view returns flaggroupid, flagname, yearpart, weekpart and week commencing for flagdata created, organisations (IDs) and country (ISO).
		The view is filtered to only include registered approved organisations as defined by view vRegisteredApprovedOrgs. It also excludes organisations defined by view vOrgsToSupressFromReports. 
		</td>
	</tr>
	<tr>
		<td valign='top' align='left'>vR_FlagsByOrgFlagGroup_RegPersonsAtRegOrgs_week_country</td>
		<td valign='top' colspan='2' align='left'>
		This view returns flaggroupid, flagname, yearpart, weekpart and week commencing for flagdata created, people at organisations for organisation flags (IDs) and country (ISO).
		The view is filtered to only include registered approved organisations and registered approved persons as defined by views vRegisteredApprovedOrgs and vRegisteredApprovedPers resepectively. It also excludes organisations defined by view vOrgsToSupressFromReports. 
		</td>
	</tr>
	<tr>
		<td valign='top' align='left'>vR_OrgsForOrgFlag_week_country</td>
		<td valign='top' colspan='2' align='left'>
		This view returns flaggroupid, yearpart, weekpart and week commencing for flagdata created, organisations for organisation flags (IDs) and country (ISO).
		The view is filtered to only include registered approved organisations as defined by view vRegisteredApprovedOrgs. It also excludes organisations defined by view vOrgsToSupressFromReports. 
		</td>
	</tr>
	<tr>
		<td valign='top' align='left'>vR_RegPeopleAtRegOrgforOrgFlag_week_country</td>
		<td valign='top' colspan='2' align='left'>
		This view returns flagid, yearpart, weekpart and week commencing for flagdata created, people at organisations for organisation flags (IDs) and country (ISO).
		The view is filtered to only include registered approved organisations and registered approved persons as defined by views vRegisteredApprovedOrgs and vRegisteredApprovedPers resepectively. It also excludes organisations defined by view vOrgsToSupressFromReports. 
		</td>
	</tr>
	<tr>
		<td valign='top' align='left'>vR_RegPerAndOrgForOrgFlagGroup</td>
		<td valign='top' colspan='2' align='left'>
		This view returns flaggroupid, flagname, yearpart, weekpart and week commencing for flagdata created, people at organisations for organisation flags (IDs) and country (ISO).
		The view is filtered to only include registered approved organisations and registered approved persons as defined by views vRegisteredApprovedOrgs and vRegisteredApprovedPers resepectively. It also excludes organisations defined by view vOrgsToSupressFromReports. 
		</td>
	</tr>
	<tr><th colspan='3' height='36'>Document History</th></tr>
	<tr>
		<td align='left'><strong>Date of Change</strong></td><td align='left'><strong>Changed By</strong></td><td align='left'><strong>Details of change</strong></td>
	</tr>
	<tr>
		<td valign='top' align='left'>June 2005</td><td valign='top' align='left'>Dave McLean</td><td valign='top' align='left'>first write up</td>
	</tr>
</table>
">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Initialise          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->


	<cfproperty name="reportColumns" type="string" hint="Some methods that output crosstabs can be displayed either way across the units axis. For example if there is a crosstab that runs units by country you can decide whether to set the country axis to the column or row side of the units axis. The effect this property has can be flexible between methods so see method hints for usage.">
	<cfproperty name="showCumulative" type="boolean" hint="Some methods that output common column reports can be returned with cumulative data along side period data.">
	<cfproperty name="userCalendar" type="boolean">
	
	<cfproperty name="vList" required="yes" type="string" hint="<strong>View List.</strong> For commonColumn reports this is a comma delimited list of views to report on. The views need to have a column that contains the same data and they also need to contain the same number of rows (unless 'useCalendar' is set to yes in which case the views will be left joined to the calendar table and hence can contain differnet numbers of rows, however not more rows than exist in the calendar table).">
	<cfproperty name="cList" required="yes" type="string" hint="<strong>Column List.</strong> For commonColumn reports this is a comma delimited list of columns to select from each view listed in 'vList'; for multiple views columnlists must be separated by a semi colon, i.e. 'v1.column1,v1.column2;v2.column1,v2.column2. The first column listed is the common column, the second column listed is the data column (upon which aggregate domain functions are performed if specified), subsequent columns listed are non specific and can be listed in any order unless domain aggregates are perfomed in which case the third and subsequent column names need to be the same for all views.">
	<cfproperty name="wList" type="string" hint="<strong>Where Clause List.</strong> For commonColumn reports this is a colon delimited list of where clauses. Supply one for each view using 1=1 where a clause is not required for a second or subsequent .">
	<cfproperty name="aList" type="string" hint="<strong>Aggregate Type List.</strong> For commonColumn reports this is a comma delimited list of types of aggregate domain functions to be performed on the second listed column.">
	<cfproperty name="nGroup" type="string" hint="<strong>Group Column List.</strong> For commonColumn reports this is a comma delimited list of columns that should be grouped on which is shared between each view. DEV NOTE - it might have been useful to develop this to default to all columns except the one having the domain aggregate applied.">
	<cfproperty name="nSort" type="string" hint="<strong>Sort Columns List.</strong> For commonColumn reports this is a comma delimited list of columns to sort on which is shared between each view.">
	<cfproperty name="nSType" type="string" hint="<strong>Sort Type.</strong> The direction of sorting either 'asc' or 'desc' defaults to 'desc'">
	<cfproperty name="nNames" type="string" hint="<strong>Column Display Names.</strong> The names to display for the data columns.">
	<cfproperty name="yName" type="string" hint="<strong>Common Column Display Name.</strong> The name to display for the common column.">
	<cfproperty name="yAxis" type="string" hint="<strong>Y Axis Column.</strong> The column to be used for the Y Axis, the common colum. DEV NOTE this needs to be deprocated as this should always be the first column listed per view.">
	<cfproperty name="yAxisF" type="string" hint="<strong>Y Axis Format.</strong> Formatting for the y axis column. Only supports null or 'date', defaults to null.">
	<cfproperty name="display" type="string" hint="<i>Don't know why I put this in here, can't remember and its not used anyway.</i>">
	<cfproperty name="passThruVars" type="struct" hint="<i>Structure of variables to pass through on forms</i>">

	
	<cfproperty name="orgExclusionView" type="string" hint="Organisation Exclusion View. The name of a view which returns a single column of organisationIDs that are to be excluded from views or other queries. This property can be used to change the effect of this view or add filters to it by defining a new view that extends the original.">
	<cfproperty name="ResellerOrgsView" type="string" hint="Reseller Organisation View. The name of a view which returns a single column of organisationIDs that defines the set of 'resellers'. This property can be used to change the effect of this view or add filters to it by defining a new view that extends the original.">
	<cfproperty name="RegisteredApprovedOrgsView" type="string" hint="Registered Approved Organisation View. The name of a view which returns a single column of organisationIDs that defines the set of 'registered approved organisations'. This property can be used to change the effect of this view or add filters to it by defining a new view that extends the original.">

		<cfset this.vList="">
		<cfset this.cList="">
		<cfset this.wList="">
		<cfset this.aList="">
		<cfset this.nGroup = "">
		<cfset this.nSort = "">
		<cfset this.nSType = "">
		<cfset this.nNames = "">
		<cfset this.yName = "">
		<cfset this.yAxis="1">
		<cfset this.yAxisF="">
		<cfset this.display="">
		
		<cfset this.reportColumns = "ISOCode">
		<cfset this.showCumulative = "no">
		<cfset this.userCalendar = "no">
		<cfset this.showStartValues = "">
		
		<cfset this.orgExclusionView = "vOrgsToSupressFromReports">
		<cfset this.ResellerOrgsView = "vResellerOrgs">
		<cfset this.RegisteredApprovedOrgsView = "vRegisteredApprovedOrgs">

		<cfset this.drilldowndelimiter = "--">
		<cfset this.passThruVars = structNew()>

		<cfset this.datasource = structNew()>
		<cfset this.nDatasource = structNew()>
		

 <!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Function - commonColumnReport          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 	<cffunction output="true" name="commonColumnReport" returntype="string" access="public" hint="Returns a report with columns derived from differnet views where each view has a single common column. These are defined in the table commonColumnReport. The data is retrieved and set to this scope and these are also properties so the function can be called by setting properties rather than calling the database. <br><br><span style='font-color:red'><b>NB</b> - only tested using 2 views although it should work for any number of views with obvious implications on performance versus scale</span>.">
		<cfargument name="commonColumnReport" required="no" type="string" hint="The name of a common Column Report.">
		<cfargument name="whereClause" type="string" required="no" hint="Additional where clause tagged on to the end of those defined in the database this must be applicable to all views.">
		<cfargument name="type" type="string" required="no" default="table" hint="The type of report required.">
		<cfargument name="force" type="boolean" required="no" default="no" hint="Whether to force a requery or not.">
		<cfargument name="UseCountryFilter" type ="boolean" required="no" default="yes" hint="turns off the current user country filter - on by default">
		<cfargument name="UseLiveCountryIDs" type ="boolean" required="no" default="yes" hint="turns off the livecountryID filter - on by default">
		<cfargument name="showContextMenu" type ="boolean" required="no" default="yes" hint="">
		<cfargument name="ContextMenuPoints" type ="String" required="no" default="data,row,column" hint="where context menu shows">
		<cfargument name="ContextMenuItems" type="string" required="no" default="selection,DrillDown_person,DrillDown_organisation" hint="list context menu items eg: selection,DrillDown_Person">				
		

		
		<cfif isdefined("arguments.commonColumnReport")>
			<cfquery name="getreport" datasource="#application.sitedataSource#">
				select * from commonColumnReport where reportTitle =  <cf_queryparam value="#commonColumnReport#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
			<cfset this.vList=getreport.reportViewList>
			<cfset this.cList=getreport.colsList>
			<cfset this.wList=getreport.whereClauseList>
			<cfset this.aList=getreport.agregatetypelist>
			<cfset this.nGroup = getreport.groupClause>
			<cfset this.nSort = getreport.sortClause>
			<cfset this.nSType = getreport.sortType>
			<cfset this.nNames = getreport.colNames>
			<cfset this.yName = getreport.yAxisName>
			<cfset this.yAxis=getreport.yAxisColumn>
			<cfset this.yAxisF=getreport.yAxisFormat>
			<cfset this.display=getreport.displayColumn>
			<cfset this.Datasource[commonColumnReport] = structNew()>
			<cfset this.nDatasource[commonColumnReport] = structNew()>			
			<cfset dataSourceStruct = this.Datasource[commonColumnReport]>
			<cfset nDataSourceStruct = this.nDatasource[commonColumnReport]>
		<cfelse>
			<cfif this.vList is "" or this.cList is "">
				<p>commonColumnReport must be called either with a valid commonColumnReport name or by setting at least the vList and cList properties.</p>
				<CF_ABORT>
			</cfif>
		</cfif>

		<CFPARAM name = "frmDrillDownAction" default="">
		

		<!--- set some nice variables for showing where context menus show--->
		<cfset contextMenu = structnew()>
		<cfset contextMenu.Row = false>
		<cfset contextMenu.Column = false>
		<cfset contextMenu.Data = false>

		<cfif showContextMenu>
			<cfloop index="position" list =#ContextMenuPoints#>
				<cfset contextMenu[position] = true>			
			</cfloop>
		</cfif>

		
		<cfset getCumStart = arrayNew(1)>
		<cfset getCumFinish = arrayNew(1)>
		<cfset Cum = arrayNew(1)>


		<cfif frmDrillDownAction is not "">
				
				<cfset reportDefinition = structNew()>
				<cfset reportDefinition.id = commonColumnReport>
				<cfset reportDefinition.name = commonColumnReport>
				<cfset reportDefinition.type = "commonColumn">
				<cfset drillDownDone = doReportDrillDown (reportDefinition =reportDefinition, UseCountryFilter = UseCountryFilter, UseLiveCountryIDs = UseLiveCountryIDs, whereClause = whereClause)>


		<cfelse>

		<cfset nCount=1>
		<!--- Loop through the views and get all the data --->
		<cfloop list="#this.vList#" index="i" delimiters=",">
			<cfset columnList = gettoken(this.cList,nCount,";")>
			<cfset whereClauseList = gettoken(this.wList,nCount,";")>
			<cfset datasourceExists = structKeyExists(datasourceStruct,nCount)>
			<cfif not datasourceExists or (datasourceExists and arguments.force is "yes")>
				
				<cfquery name="theData" datasource="#application.sitedataSource#">
					<cfif this.userCalendar is "week">
						set datefirst 7
						select 
						<cfif this.yaxisF is "date">
							<cfset itemField = "CONVERT(CHAR(11),startdate,106)">
							CONVERT(CHAR(11),startdate,106) 
						<cfelse>
							<cfset itemField = "startdate">
							startdate
						</cfif>
						as item, isnull(data,0) as data, Calendaryear as yearorder, WeekOfCalendarYear as weekorder
						
						from calendarweek c
						left join
						(
					</cfif>
						
							select 
								<cfif this.yaxisF is "date">
									CONVERT(CHAR(11),#gettoken(columnList,1,",")#,106)
								<cfelse>
									#gettoken(columnList,1,",")#
								</cfif>
								 as item, 
									<cfif this.alist is "">
										#gettoken(columnList,2,",")# as data
									<cfelse>
										#gettoken(this.alist,nCount,",")#(#gettoken(columnList,2,",")#) as data
									</cfif>
									<cfif listlen(columnList) gt 2>
										<cfloop from="3" to="#listlen(columnList)#" index="j">
											,#gettoken(columnList,j,",")#
										</cfloop>
									</cfif>
							
							from #i#
							where 1=1
								<cfif UseCountryFilter>
									and
									countryid in (#request.relaycurrentuser.countrylist#) 
								</cfif>
								<cfif UseLiveCountryIDs>
									<cfif structkeyExists(application,"LiveCountryIDs")>
										and
										countryid  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
									</cfif> 
								</cfif>
								<cfif isdefined("arguments.whereClause")>
								and 
								(#preservesinglequotes(arguments.whereClause)#)
								</cfif>
								<cfif isdefined("whereClauseList") and whereClauseList neq "">
								and (#preservesinglequotes(whereClauseList)#)
								</cfif>
					
							<cfif this.nGroup is not "">
								group by #this.nGroup#
							</cfif>
							<cfif this.nSort is not "" and this.userCalendar is "">
								order by #this.nSort# <cfif this.nSType is not "">#this.nSType#</cfif>
							</cfif>
					<cfif this.userCalendar is "week">
							) a
		
							on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
							order by CalendarYear, WeekOfCalendarYear asc
					</cfif>
				</cfquery>
				<cfquery name="calendar" datasource="#application.sitedatasource#">
					select * from calendarweek
				</cfquery>
				<!--- Preserve the data of each datasource so that queries don't need to be run again --->
				<cfset dataSourceStruct[nCount] = theData>
				<!--- Set copies of each datasource for filtering and rendering --->
				<cfset ndataSourceStruct[nCount] = theData>
			</cfif>
			<cfset nCount=nCount+1>
		</cfloop>

		</cfif>

		<!--- If there are any further filters to apply do this now
			Note these should be filters that are common to all views
		 --->
		<cfif isdefined("arguments.whereClause") and arguments.whereClause is not "">
			
			<cfloop from="1" to="#listlen(this.vlist)#" index="i">
				<cfset nDatasource = dataSourceStruct[i]>
				<!--- If we want to show cumulative values we need to get the starting cumulative balance --->
				<cfif this.showCumulative is not "no">
					<cfset nValues=valuelist(nDatasource.data)>
					<cfset startvalue = 0>
					<cfif this.showStartValues neq "">
						<cfset startvalue = listgetat(this.showStartValues,i,";")>
					</cfif>
					<cfset getCumStart[i] = startvalue>
					<cfloop list="#nValues#" index="j">
						<cfset getCumStart[i] = getCumStart[i] + j>
					</cfloop>
				</cfif>
				<cfquery name="nQuery" dbtype="query">
					select * from nDatasource
					where #arguments.whereClause#
					<cfif this.nSort is not "">
						order by #this.nSort# <cfif this.nSType is not "">#this.nSType#</cfif>
					</cfif>
				</cfquery>
				<!--- If we want to show cumulative values we need to get the final cumulative balance --->
				<cfif this.showCumulative is not "no">
					<cfset nValues=valuelist(nQuery.data)>
					<cfset getCumFinish[i] = 0>
					<cfloop list="#nValues#" index="j">
						<cfset getCumFinish[i] = getCumFinish[i] + j>
					</cfloop>
				</cfif>
				<cfset nDatasourceStruct[i] = nQuery>	
			</cfloop>
		</cfif>


	<cfif arguments.showContextMenu and arguments.type is "table">
				<!--- bit of a hack to allow different columns to have different items in the context menu,  
				If need to be different, then separate list with a ;  if all the same then just define one.
				 --->
			<cfloop from="1" to="#listlen(this.vList)#" index="i">
				<cfset contextMenuID = "comCol_#arguments.commoncolumnReport#_#i#">
				<cfif arguments.contextMenuItems contains ";">
					<cfset theseContextMenuItems = listgetat(arguments.contextMenuItems,i,";")>
				<cfelse>
					<cfset theseContextMenuItems = arguments.contextMenuItems>
				</cfif>
				 
				<cfoutput>
				<xml id="contextDef">
					<xmldata>
						<contextmenu id="#htmleditformat(contextMenuID)#">
							<cfif theseContextMenuItems contains 'Selection'>
							<item id="Selection_Save" value="Save as Selection" ></item>
							<item id="Selection_Add" value="Add to Selection" ></item>
							</cfif>
							<cfif theseContextMenuItems contains 'Drilldown_Person'>
							<item id="Drilldown_Person" value="View People" ></item>
							</cfif>
							<cfif theseContextMenuItems contains 'Drilldown_Organisation'>
							<item id="Drilldown_Organisation" value="View Organisations" ></item>
							</cfif>

						</contextmenu>
					</xmldata>
				</xml>		
			
				<form method="post" name = "reportDrillDownForm_#arguments.commoncolumnReport#__#i#" target = "" style="display: inline">
					<input type="hidden" name="frmReportID" value ="">
					<input type="hidden" name="frmRowIDCols" value ="YearOrder|WeekOrder">   <!--- these values are hacked in, eventually will need to be calculated --->
					<input type="hidden" name="frmRowID" value ="">
					<input type="hidden" name="frmDrillDownAction" value ="">
				
					
				</form>
				</cfoutput>

			</cfloop>
			
		<cfif not isDefined("request.ReportDrillDownLoaded")>
			<cfoutput>

	
			<div status="false" onclick="javascript:fnDetermine(event);" onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);" id="oContextMenu" class="menu"></div>

			<cfset request.ReportDrillDownLoaded= true>
			<CFHTMLHEAD TEXT='<LINK REL="stylesheet" HREF="/Styles/contextMenuStyles.css">'>	
			<cf_includeJavascriptOnce template = "/javascript/contextMenu.js">
			<cf_includeJavascriptOnce template = "/javascript/contextMenu_Report.js">	
			<cf_includeJavascriptOnce template = "/javascript/reportdrillDown.js">	
			
			<SCRIPT>
				fnInit()
			</SCRIPT>

			</cfoutput>	
		</cfif>

	</cfif>

		<cfif arguments.type is "table">
			<table class="withBorder">
			<cfif this.showCumulative is not "no">
				<cfloop from="1" to="#listlen(this.vList)#" index="i">
					<cfif isdefined("arguments.whereClause") and arguments.whereClause is not "">
						<cfset cum[i] = getCumStart[i] - getCumFinish[i]>
					<cfelse>
						<cfset cum[i] = 0>
					</cfif>
				</cfloop>
			</cfif>
			<cfloop from="0" to="#nDatasourceStruct[1].recordcount#" index="i">
				<cfif i eq 0>
					<!--- Put in the table headers --->
					<tr>
						<th align="center">&nbsp;#htmleditformat(this.yName)#&nbsp;</th>
						<cfloop list="#this.nNames#" index="j">
							<th align="center">&nbsp;#htmleditformat(j)#&nbsp;</th>
							<cfif this.showCumulative is not "no">
								<th align="center">&nbsp;Total&nbsp;</th>
							</cfif>
						</cfloop>
					</tr>
					<cfif this.showCumulative is not "no">
						<cfif this.showStartValues neq "">
						<tr class="oddrow">
							<th align="right">phr_PreviousTotals</td>
							<cfloop list="#this.nNames#" index="j">
								<cfset ix = listfind(this.nNames,j)>
								<td align="right">#listgetat(this.showStartValues,ix,";")#</td>
								<cfif this.showCumulative is not "no">
									<td align="right">#listgetat(this.showStartValues,ix,";")#</td>
								</cfif>
							</cfloop>
						</tr>			
						</cfif>
					</cfif>
				<cfelse>
					<cfif i mod 2 eq 0><cfset nClass="evenrow"><cfelse><cfset nClass="oddrow"></cfif>
					<tr class="#nClass#">
					<cfloop from="0" to="#listlen(this.vList)#" index="j">
						<cfset contextMenuID = "comCol_#arguments.commoncolumnReport#_#j#">

						<cfif j eq 0>
							<!--- Put in the first column that is common to all the datasources --->
							<td align="right" nowrap>#nDatasourceStruct[1].item[i]#</td>
						<cfelse>
							<!--- Put in subsequent columns --->
							<cfset theData = nDatasourceStruct[j].data[i]>
							<td align="right">
									<cfif contextMenu.data >
										<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" contextmenu = "#contextMenuID#" id = "crossTab#this.drilldowndelimiter##arguments.commoncolumnreport#__#j##this.drilldowndelimiter##nDatasourceStruct[j].yearOrder[i]#|#nDatasourceStruct[j].weekOrder[i]##this.drilldowndelimiter#data" CLASS="smallLink">
										#htmleditformat(theData)#</A>
									<cfelse>
										#htmleditformat(theData)#
									</cfif>			
	
							</td>
						</cfif>
						
						<cfif this.showCumulative is not "no" and j gt 0>
							<!--- Put in the cumulative column if its required --->
							<cfset cum[j] = cum[j] + theData>
							<td align="right">#cum[j]#</td>
						</cfif>
					</cfloop>
					</tr>
				</cfif>
			</cfloop>
			</table>
			<cfif this.showCumulative is "yes"><cfset this.showCumulative = "no"></cfif>
		</cfif>
		<cfif arguments.type is "bar" or arguments.type is "line">
			<!--- Workout the maximum value to be displayed on the chart --->
			<cfset maxyAxis = 0>
			<cfloop from="1" to="#listlen(this.vlist)#"index="i">
				<cfset nDatasource = datasourceStruct[i]>
				<cfset nValues=valuelist(nDatasource.data)>
				<cfif this.showCumulative is not "no">					
					<cfif this.showStartValues neq "">
						<cfset startvalue = listgetat(this.showStartValues,i,";")>
					<cfelse>
						<cfset startvalue = 0>
					</cfif>
					<cfset jTotal = startvalue>
				</cfif>
				<cfloop list="#nValues#" index="j">
					<cfif j gt maxyAxis><cfset maxyAxis = j></cfif>
					<cfif this.showCumulative is not "no">
						<cfset jTotal=jTotal+j>
						<cfif jTotal gt maxyAxis><cfset maxyAxis = jTotal></cfif>
					</cfif>
				</cfloop>
			</cfloop>
			<!--- No data - force a height --->
			<cfif maxyAxis eq 0>
				<cfset maxyAxis = 100>
			</cfif>
			<!--- add a bit on to the top of the chart making sure its in proportion to the size of the chart 
				and makesure that the grid lines lie perfectly on magnitude units--->
			<cfset grids = int(maxyaxis/(10^(ceiling(log10(maxyaxis))-1))) + 1>
			<cfset maxyaxis=grids*(10^(ceiling(log10(maxyaxis))-1))>
			<cf_chart chartheight="300" chartwidth="600" format="png" gridlines="#grids+1#" scalefrom="0" scaleto="#maxyAxis#" showMarkers="no">
				<cfloop from="1" to="#listlen(this.vlist)#"index="i">
					<cfset nDatasourceQuery = nDataSourceStruct[i]>
					<cf_chartseries serieslabel="#gettoken(this.nNames,i,',')#" type="#arguments.type#" query="nDatasourceQuery" itemcolumn="item" valuecolumn="data">
					<!--- If we want to show cumulative values we need to get a list 
							for these series --->
					<cfif this.showCumulative is not "no">
<!--- 						<cfset nDatasourceQuery = evaluate(nDatasource)> --->
						<cfset nItemColumn = "nDatasourceQuery." & gettoken(columnlist,1,',')> 
						<cfset nValues=valuelist(nDatasourceQuery.data)>
						<cfset nItems=valuelist(nDatasourceQuery.item)>
						<cf_chartseries serieslabel="#gettoken(this.nNames,i,',')# Total" type="#arguments.type#">
							<cfif this.showStartValues neq "">
								<cfset startvalue = listgetat(this.showStartValues,i,";")>
							<cfelse>
								<cfset startvalue = 0>
							</cfif>
							<cfset cumValue=startvalue>
							<cfloop from="1" to="#listlen(nItems)#" index="j">
								<cfset cumValue=cumValue + gettoken(nValues,j,',')>
								<cf_chartdata item="#gettoken(nItems,j,',')#" value="#cumValue#">
							</cfloop>
						</cf_chartseries>
					</cfif>
				</cfloop>
			</cf_chart>

			<cfif this.showCumulative is "yes"><cfset this.showCumulative = "no"></cfif>
		</cfif>
	</cffunction>
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Function - crossTabReport          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 
 
 	<cffunction output="true" name="crossTabReport" returntype="string" access="public" hint="Returns a cross tab report for a given view. These can be displayed with considerable flexibility, for example without column totals allowing several crosstabs to be rendered next to each other.">
		<cfargument name="reportQuery" type="query" required="yes" hint="The query containing the data for the cross tab.">
		<cfargument name="ctDisplayRowTotals" type="string"  required="no" default="yes" hint="For CrossTab queries determines whether to show totals for each row or not. When set to yes these appear on the left adjacent to the row axis.">
		<cfargument name="ctDisplayRowTitles" type="string" required="no" default="yes" hint="For CrossTab queries determines whether to show Titles for each row or not.">
		<cfargument name="ctDisplayColumnTotals" type="string" required="no" default="both" hint="For CrossTab queries determines whether to show totals for each column or not; can be set to 'both' (default), 'top' or 'bottom'.">
		<cfargument name="ctOrderRowsBy" type="string" required="no" default="" hint="<strong>Sorting for Rows.</strong> For CrossTab queries determines how">
		<cfargument name="showemptycols" type="string" required="no" default="yes" hint="">
		<cfargument name="showStartValues" type="string" required="no" default="x" hint="; delimited list of values to start columns with.Subsequent columns will defualt to zero once 1+ values are declared">
		<cfargument name="showContextMenu" type="boolean" required="no" default="false" hint="set to true if you want context menu to appear on datapoints">		
		<cfargument name="ContextMenuPoints" type ="String" required="no" default="data,row,column" hint="where context menu shows">
		<cfargument name="ContextMenuItems" type="string" required="no" default="selection,DrillDown_person" hint="list context menu items eg: selection,DrillDown_Person">				
		<cfargument name="ID" type="string" required="no" default="" hint="ID of report - needed if doing drilldowns with more than one of these on a page">
		<cfif not isQuery(arguments.reportQuery)>
			<p>The reportQuery argument of RelayCrossTabReport custom tag must contain a valid query object.  
			Example syntax: <pre>&lt;cf_relayCrossTab
			reportQuery = "#htmleditformat(arguments.reportQuery)#"
			reportColumns = "reportColumns"
			reportRows = "reportRows"
			reportUnits = "reportUnits"&gt;</pre>
			</p>
			<CF_ABORT>
		</cfif>
		
		<!--- <cfdump var="#arguments.reportQuery.ColumnList#"> --->
		
		<cfif listFindNoCase(arguments.reportQuery.ColumnList,"reportColumns") eq 0>
			<p>You must call the column alias for the report columns reportColumns.</p>
			<CF_ABORT>
		</cfif>
		
		<cfif listFindNoCase(arguments.reportQuery.ColumnList,"reportRows") eq 0>
			<p>You must call the column alias for the report rows reportRows.</p>
			<CF_ABORT>
		</cfif>
		
		<cfif listFindNoCase(arguments.reportQuery.ColumnList,"reportUnits") eq 0>
			<p>You must call the column alias for the report units reportUnits.</p>
			<CF_ABORT>
		</cfif>
		

		<!--- set some nice variables for showing where context menus show--->
		<cfset contextMenu = structnew()>
		<cfset contextMenu.Row = false>
		<cfset contextMenu.Column = false>
		<cfset contextMenu.Data = false>

		<cfif showContextMenu>
			<cfloop index="position" list =#ContextMenuPoints#>
				<cfset contextMenu[position] = true>			
			</cfloop>
		</cfif>


		<cfquery name="getReportingCols" dbtype="query">
			select distinct 
			reportColumns as reportingCols 
			from arguments.reportQuery
			<cfif isdefined("arguments.ctOrderRowsBy")>
				#arguments.ctOrderRowsBy#
			</cfif>
		</cfquery> 
		
		<cfquery name="getReportingRows" dbtype="query">
			select distinct reportRows as reportingRows from arguments.reportQuery
		</cfquery> 
		
		
		<!--- build a structure that contains the row totals --->
		<cfquery name="getReportingRowTotals" dbtype="query">
			select reportColumns as reportingRow, sum(reportUnits) as rowTotal 
			from arguments.reportQuery
			group by reportColumns
		</cfquery> 
		
		<cfset reportingRowTotalsStruct = structNew()>
		<cfloop query="getReportingRowTotals">
			<cfif not structKeyExists( reportingRowTotalsStruct, "#reportingRow#" )>
				<cfset structInsert( reportingRowTotalsStruct, "#reportingRow#", rowTotal )>
			</cfif>
		</cfloop>
		
		
		<!--- build a structure that contains the row totals --->
		<cfquery name="getReportingColTotals" dbtype="query">
			select reportRows as reportingCol, sum(reportUnits) as colTotal 
			from arguments.reportQuery
			group by reportRows
		</cfquery> 
		
		<cfset reportingColTotalsStruct = structNew()>
		<cfloop query="getReportingColTotals">
			<cfif not structKeyExists( reportingColTotalsStruct, "#reportingCol#" )>
				<cfset structInsert( reportingColTotalsStruct, "#reportingCol#", colTotal )>
			</cfif>
		</cfloop>
		
		
		<!--- build a double keyed structure using a pipe delimeter to hold the units 
			NOTE its important to use trim here as the structre finctions are a bit buggy and inconsistent
					on the point of leading and trailing white space
		---> 
		<cfset reportingDataStruct = structNew()>
		<cfloop query="arguments.reportQuery">
			<cfif not structKeyExists( reportingDataStruct, "#trim(reportColumns)#|#trim(reportRows)#" )>
				<cfset structInsert( reportingDataStruct, "#trim(reportColumns)#|#trim(reportRows)#", reportUnits )>
			</cfif>
		</cfloop>
		
		
	<cfif arguments.showContextMenu >

				<cfset contextMenuID = "crossTabs_#arguments.id#">
			<cfoutput>
				<xml id="contextDef">
					<xmldata>
						<contextmenu id="#htmleditformat(contextMenuID)#">
							<cfif arguments.contextMenuItems contains 'Selection'>
							<item id="Selection_Save" value="Save as Selection" ></item>
							<item id="Selection_Add" value="Add to Selection" ></item>
							</cfif>
							<cfif arguments.contextMenuItems contains 'Drilldown_Person'>
							<item id="Drilldown_Person" value="View People" ></item>
							</cfif>
							<cfif arguments.contextMenuItems contains 'Drilldown_Organisation'>
							<item id="Drilldown_Organisation" value="View Organisations" ></item>
							</cfif>

						</contextmenu>
					</xmldata>
				</xml>


			<form method="post" name = "reportDrillDownForm_#arguments.id#" target = "" style="display: inline">
			<input type="hidden" name="frmReportID" value ="">
			<input type="hidden" name="frmRowIDCols" value ="reportRows|reportColumns">
			<input type="hidden" name="frmRowID" value ="">
			<input type="hidden" name="frmDrillDownAction" value ="">
			
			<cfloop collection="#this.passThruVars#" item="thisitem">
				<CF_INPUT type="hidden" name="#thisitem#" value ="#this.passthruvars[thisitem]#">				
			</cfloop>

			</form>

			</cfoutput>
					
		<cfif not isDefined("request.ReportDrillDownLoaded")>
			<cfset request.ReportDrillDownLoaded = true>

			<cfoutput>
			<div status="false" onclick="javascript:fnDetermine(event);" onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);" id="oContextMenu" class="menu"></div>

			<CFHTMLHEAD TEXT='<LINK REL="stylesheet" HREF="/Styles/contextMenuStyles.css">'>	

			<cf_includeJavascriptOnce template = "/javascript/contextMenu.js">
			<cf_includeJavascriptOnce template = "/javascript/contextMenu_report.js">	
			<cf_includeJavascriptOnce template = "/javascript/reportdrillDown.js">	
			</cfoutput>	

			<SCRIPT>
				fnInit()
			</SCRIPT>

		</cfif>

	</cfif>

		<cfoutput>	

		<TABLE CLASS="withBorder">
			<tr>
				<cfif arguments.ctDisplayRowTitles is "yes">
					<th>&nbsp;</th>
				</cfif>
				<cfif arguments.ctDisplayRowTotals is "yes">
					<th>Total</th>
				</cfif>
				<cfloop query="getReportingRows">
						<cfif reportingRows is not "">
					   		<th><cfset thisHeading = getReportingRows.reportingRows>
								<cfif contextMenu.column >
									<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" contextmenu = "#contextMenuID#" id = "crossTab#this.drilldowndelimiter##arguments.id##this.drilldowndelimiter##trim(thisHeading)#| #this.drilldowndelimiter# " CLASS="smallLink">
									#htmleditformat(thisHeading)#</a>
								<cfelse>
									#htmleditformat(thisHeading)#
								</cfif>		

							</th>
					   	</cfif>
				</cfloop>
				<!--- <th>Total</th> --->
			</tr>
			<cfif arguments.ctDisplayColumnTotals is "both" or arguments.ctDisplayColumnTotals is "top">
				<tr>
					<cfif arguments.ctDisplayRowTitles is "yes">
						<th>Total</th>
					</cfif>
					<cfif arguments.ctDisplayRowTotals is "yes">
						<th>&nbsp;</th>
					</cfif>
					<cfloop query="getReportingRows">
						<cfif reportingRows is not "">
								<cfset colpos = listfindnocase(arguments.showStartValues,reportingRows,";=")>
								<cfif colpos eq 0>
									<cfset startValue = 0>
								<cfelse>
									<cfset startValue = listgetat(arguments.showStartValues,colpos+1,";=")>
								</cfif>
								<cfset total = structFind( reportingColTotalsStruct, "#reportingRows#" ) + startvalue>
							<th>#total#</th>
						</cfif>
					</cfloop>
				</tr>
			</cfif>
			<cfset i = 1>
			<cfset usedCols = 0>
			<cfloop query="getReportingCols">
				<cfif showemptycols or (reportingCols is not "" and not showemptycols)>
					<cfif arguments.showStartValues neq "x" and getReportingCols.currentrow eq 1>
						<tr class="evenrow">
						<cfloop query="getReportingRows">
							<cfif reportingRows is not "">
								<cfset colpos = listfindnocase(arguments.showStartValues,reportingRows,";=")>
								<cfif colpos eq 0>
									<cfset startValue = "-">
								<cfelse>
									<cfset startValue = listgetat(arguments.showStartValues,colpos+1,";=")>
								</cfif>
								<td style="text-align: center;">#htmleditformat(startValue)#</td>
							</cfif>						
						</cfloop>
								
							</tr>	
					</cfif>
					<cfset thisReportingPeriod = getReportingCols.reportingCols>
					<cfif i mod 2 eq 0><cfset nClass="evenrow"><cfelse><cfset nClass="oddrow"></cfif>
						<tr class="#nClass#">
					<cfif arguments.ctDisplayRowTitles is "yes">
					   		<th><cfset thisHeading = thisReportingPeriod>
								<cfif contextMenu.row>
									<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" contextmenu = "#contextMenuID#" id = "crossTab#this.drilldowndelimiter##arguments.id##this.drilldowndelimiter# |#trim(thisHeading)##this.drilldowndelimiter#data" CLASS="smallLink">
									#htmleditformat(thisHeading)#</a>
								<cfelse>
									#htmleditformat(thisHeading)#
								</cfif>		
							</th>
					</cfif>
					<cfif arguments.ctDisplayRowTotals is "yes">
						<th >
							#structFind( reportingRowTotalsStruct, "#htmleditformat(thisReportingPeriod)#" )#
						</th>
					</cfif>
					<cfloop query="getReportingRows">
						 <cfif reportingRows is not "">
							<cfif structKeyExists( reportingDataStruct, "#trim(thisReportingPeriod)#|#trim(getReportingRows.reportingRows)#" )>
								<td style="text-align: center;">
									<cfset thisDataValue = structFind( reportingDataStruct, "#trim(thisReportingPeriod)#|#trim(getReportingRows.reportingRows)#" )>
									<cfif ContextMenu.data>
										<A HREF="" onclick = "javascript:fnLeftClickContextMenu(event)" contextmenu = "#contextMenuID#" id = "crossTab#this.drilldowndelimiter##arguments.id##this.drilldowndelimiter##trim(getReportingRows.reportingRows)#|#trim(thisReportingPeriod)##this.drilldowndelimiter# " CLASS="smallLink">
										#htmleditformat(thisDataValue)#</A>

									<cfelse>
										#htmleditformat(thisDataValue)#
									</cfif>			
								</td>
								
							<cfelse>
								<td style="text-align: center;">-</td>
							</cfif>
						</cfif>
					</cfloop>
					</tr>
				</cfif>
			<cfset i = i + 1>
			</cfloop>
			<cfif arguments.ctDisplayColumnTotals is "both" or arguments.ctDisplayColumnTotals is "bottom">
				<tr>
					<cfif arguments.ctDisplayRowTitles is "yes">
						<th>Total</th>
					</cfif>
					<cfif arguments.ctDisplayRowTotals is "yes">
						<th>&nbsp;</th>
					</cfif>
					
					<cfloop query="getReportingRows">
						<cfif reportingRows is not "">
								<cfset colpos = listfindnocase(arguments.showStartValues,reportingRows,";=")>
								<cfif colpos eq 0>
									<cfset startValue = 0>
								<cfelse>
									<cfset startValue = listgetat(arguments.showStartValues,colpos+1,";=")>
								</cfif>
								<cfset total = structFind( reportingColTotalsStruct, "#reportingRows#" ) + startvalue>
							<th>#total#</th>
						</cfif>
					</cfloop>
					<!--- <th>&nbsp;</th> --->
				</tr>
			</cfif>
		</table>
		</cfoutput>
	</cffunction>
	
<!--- 
WAB 2008/02/27

Take a query and make a crosstab query
Although similar to above code, this doesn't bother do the output it just generates the data (which gives more flexibility of output)
It is used by cf_crossTabTableFromQueryObject to actually do the output
See devDocs for more info
 --->
	
<cffunction name="convertQueryToCrossTab" returntype="struct" hint="Takes a query and returns another query with crosstab">
 
 <cfargument name="reportQuery" type="query" >  <!--- the query --->
 <cfargument name="ValueColumns" type="string" > <!--- this is the column, or columns, which contain the values to be displayed in the body of the crosstab --->
 <cfargument name="ValueColumnTitles" type="string" default="#ValueColumns#" >
 <cfargument name="HeaderColumn" type="string" > <!--- this is the column whose values are going to become the headers --->
 <cfargument name="HeaderOrderColumn" type="string" default = "#HeaderColumn#"> 
 <cfargument name="HeaderTitleColumn" type="string" default = "#HeaderColumn#">   <!--- not really necessary but can be handy --->
 <cfargument name="GroupByColumns" type="string" default="">
 <cfargument name="GroupByOrderColumns" type="string" default="#GroupByColumns#">
 <cfargument name="GroupByTitleColumns" type="string" default="#GroupByColumns#">
 <cfargument name="wabTest" type="boolean" default="true">
 <cfargument name="requiredHeaderValues" type="string" default = ""> 
 <cfargument name="nullValues" type="string" default = "">  <!--- may be need to support list which matches valueCOlumns --->

<cfargument name="keyColumnList" type="string" default = "">
<cfargument name="keyColumnKeyList" type="string" default = "">
<cfargument name="keyColumnURLList" type="string" default = "">
<cfargument name="KeyColumnContextMenuList" type="string" default = "">
<cfargument name="KeyColumnContextMenuIDExpressionList" type="string" default = ""> 

 
		<cfset var result = structNew()>
 		<cfset var thisGroupByColumns = "">
 		<cfset var thisGroupByOrderColumns = "">
		<cfset var thisGroupByTitleColumns = "">		
		<cfset var headerColumnNames = "">		 
		<cfset var headerColumnTitles = "">		 
		<cfset 	var tmpHeaderColumn = "">
		<cfset 	var tmpHeaderTitleColumn =  "">
		<cfset 	var tmpHeaderOrderColumn = "">
			<cfset var allButLastHeaderColumn = "">
			<cfset var allButLastHeaderOrderColumn = "">
			<cfset var allButLastHeaderTitleColumn = "">
			<cfset 	var tmpGroupByColumns = "">
			<cfset 	var tmpGroupByOrderColumns = "">
			<cfset 	var tmpGroupByTitleColumns = "">


		
<!--- 
<cfset x = duplicate(arguments)>
<cfset structDelete(x,"reportQuery")>
<cfdump var="#x#">
 --->		
		 <!--- If there aren't any groupByColumn defined then we work then out by just removing the value and header columns from the column list--->
		<cfif arguments.GroupByColumns is "">
			<!--- work out the group by column --->
			<cfset columnList = arguments.reportQuery.columnList>
			<!--- delete the value and header columns --->
			<cfloop list="#HeaderColumn#,#ValueColumns#" index= "col">
				<cfset x = listFindNoCase(columnList,col)>
				<cfif x is not 0 ><cfset columnList = listDeleteAt(columnList,x)></cfif>
			</cfloop>
			<cfset thisGroupByColumns = columnList>
			<cfset thisGroupByOrderColumns = columnList>
			<cfset thisGroupByTitleColumns = columnList>			
		<cfelse>
			<cfset thisGroupByColumns = arguments.GroupByColumns>
			<cfset thisGroupByOrderColumns = arguments.GroupByOrderColumns>
			<cfset thisGroupByTitleColumns = arguments.GroupByTitleColumns>
		</cfif>	
		
		
		<cfif listLen (arguments.HeaderColumn) gt 1>
			<!--- This is the special case of doing a double crosstab --->
			<cfset allButLastHeaderColumn = reverse(listrest(reverse(arguments.headerColumn)))>
			<cfset allButLastHeaderOrderColumn = reverse(listrest(reverse(arguments.headerOrderColumn)))>
			<cfset allButLastHeaderTitleColumn = reverse(listrest(reverse(arguments.headerTitleColumn)))>

			<cfset 	tmpGroupByColumns = listappend(arguments.GroupByColumns,listfirst(arguments.headerColumn))>
			<cfset 	tmpGroupByOrderColumns = listappend(arguments.GroupByOrderColumns,listfirst(arguments.headerOrderColumn))>
			<cfset 	tmpGroupByTitleColumns = listappend(arguments.GroupByOrderColumns,listfirst(arguments.headerTitleColumn))>
			<cfset 	tmpHeaderColumn = listrest( arguments.headerColumn)>
			<cfset 	tmpHeaderTitleColumn = listrest( arguments.headerTitleColumn)>
			<cfset 	tmpHeaderOrderColumn = listrest( arguments.headerOrderColumn)>

<!--- 			<cfoutput> Debug: 
						valueColumns = #ValueColumns#<BR>
						valueColumnTitles = #valueColumnTitles#<BR>
						headerColumn = #tmpHeaderColumn# <BR>
						headerOrderColumn = #tmpHeaderOrderColumn# <BR>
						headerTitleColumn = #tmpHeaderTitleColumn# <BR>
						GroupByColumns = #tmpGroupByColumns#<BR>
						GroupByOrderColumns = #tmpGroupByOrderColumns#<BR>
						GroupByTitleColumns = #tmpGroupByTitleColumns#<BR>
			</cfoutput>

 --->			<cfset crossTabQueryInfo = convertQueryToCrossTab(
						reportQuery = reportQuery,
						valueColumns = ValueColumns,
						valueColumnTitles = valueColumnTitles,
						headerColumn = tmpHeaderColumn ,
						headerOrderColumn = tmpHeaderOrderColumn ,
						headerTitleColumn = tmpHeaderTitleColumn ,
						GroupByColumns = tmpGroupByColumns,
						GroupByOrderColumns = tmpGroupByOrderColumns,
						GroupByTitleColumns = tmpGroupByTitleColumns,
						requiredHeaderValues =requiredHeaderValues,
						keyColumnList = keyColumnList,
						keyColumnURLList = keyColumnURLList,
						wabtest = wabtest,
						nullValues = nullValues
			)>
		
			<cfset arguments.ValueColumns = crossTabQueryInfo.headerColumnNames>
			<cfset arguments.ValueColumnTitles = crossTabQueryInfo.headerColumnTitles>
			<cfset arguments.reportQuery = crossTabQueryInfo.query>
			<cfset arguments.headerColumn = listfirst( arguments.headerColumn)>
			<cfset arguments.headerOrderColumn = listfirst( arguments.headerOrderColumn)>
			<cfset arguments.headerTitleColumn = listfirst( arguments.headerTitleColumn) >
			<cfif structKeyExists(crossTabQueryInfo.TableFromQueryObjectAttributes,"keyColumnList")>
				<cfset arguments.keyColumnList = crossTabQueryInfo.TableFromQueryObjectAttributes.keyColumnList>
				<cfset arguments.keyColumnURLList = crossTabQueryInfo.TableFromQueryObjectAttributes.keyColumnURLList>
			</cfif>
			
			
		</cfif>


		
		<!--- If more than one value fields then easier to deal with null values if we have a little lookup structure --->
		<cfset nullValuesStruct = structNew()>
		<cfloop index="i" from=1 to = "#listlen(ValueColumns)#">
			<cfset col = listgetat(ValueColumns,i)>
			<cfif listLen(ValueColumns) is listLen(nullvalues)>
				<cfset nullValuesStruct[col] = listgetat(nullvalues,i)>
			<cfelse>
				<cfset nullValuesStruct[col] = nullvalues>
			</cfif>
		</cfloop>
		
		<cfset dummy = queryNew('dummy')>
		<cfset queryAddRow(dummy,1)>
		
		<!--- Get distinct values in the headerColumn and add on any extra which are required (eg might want to make sure that all months are covered even if no data--->
		<cfquery name = "distinctheaders" dbType="query">
			select distinct #HeaderColumn# as header, #HeaderOrderColumn# as Ordering, #HeaderTitleColumn# as headerTitle
			from reportquery
			
			<!--- Add in any required headers ---> 
			<cfloop index="extras" list = "#requiredHeaderValues#">
			union select '#listfirst(extras,"|")#' as header, '#listLast(extras,"|")#' as ordering, '#listfirst(extras,"|")#' from dummy
			</cfloop>
			
		 	order by ordering
		</cfquery>
		
<!--- 		<cfdump var="#distinctheaders#"> --->
		<!--- Initialise a structure for holding details of each new column being created --->
		<cfset HeaderColumnsStruct = structNew()>
		<cfloop list = "#ValueColumns#" index="valueCol">
				<cfset HeaderColumnStruct[valueCol] = structNew()>
		</cfloop>		

		<!--- Make a blank query with all header columns and all the rows --->
		<cfset headerColumnNames = "">
		<cfquery name = "grid" dbType="query">
			select distinct #thisGroupByColumns#,#thisGroupByOrderColumns#,
			''
			<cfloop index="col" list="#thisGroupByColumns#">
			+'&#col#='+ #col#
			</cfloop>
			as CompoundKey
<!--- Very nasty here, haven't worked out which way to stack the cross tabs, maybe need to be flexible and I find myself repeating the code with the loops the other way round --->
		<cfif wabtest>
			<cfloop query = "distinctHeaders">
				<cfloop index="valueColumnIndex" from=1 to = #listlen(ValueColumns)# >
						<cfset valueCol = listgetat(valueColumns,valueColumnIndex)>
						<cfset valueColTitle = listgetat(valueColumnTitles,valueColumnIndex)>						
<!--- 				<cfloop list = "#ValueColumns#" index="valueCol"> --->
						<cfif listLen(valueColumns) is 1>
							<cfset thisColumnName = "#header#">
							<cfset thisColumnTitle = "#headerTitle#">
						<cfelse>
							<cfset thisColumnName = "#header#_#valueCol#">
							<cfset thisColumnTitle = "#headerTitle##application.delim1##valueColTitle#">
						</cfif>
						<cfset HeaderColumnStruct[valueCol][thisColumnName] = "">
						<cfset headerColumnNames = listappend(headerColumnNames,thisColumnName)>
						<cfset headerColumnTitles = listappend(headerColumnTitles,thisColumnTitle)>
						,
						<cfif isNumeric(nullValuesStruct[valuecol])>
						#nullValuesStruct[valuecol]# 
						<cfelse>
						'#nullValuesStruct[valuecol]#'
						</cfif>
						as #thisColumnName#
				</cfloop>
			</cfloop>
<cfelse>
			<cfloop index="valueColumnIndex" from=1 to = #listlen(ValueColumns)# >
					<cfset valueCol = listgetat(valueColumns,valueColumnIndex)>
					<cfset valueColTitle = listgetat(valueColumnTitles,valueColumnIndex)>						

<!--- 			<cfloop list = "#ValueColumns#" index="valueCol"> --->
				<cfloop query = "distinctHeaders">

						<cfif listLen(valueColumns) is 1>  <!--- when only one value column, doesn't make sense to put its column name into the header --->
							<cfset thisColumnName = "#header#">
							<cfset thisColumnTitle = "#headerTitle#">
						<cfelse>
							<cfset thisColumnName = "#valueCol#_#header#">
							<cfset thisColumnTitle = "#valueColTitle##application.delim1##headerTitle#">
						</cfif>
						<cfset HeaderColumnStruct[valueCol][thisColumnName] = "">
						<cfset headerColumnNames = listappend(headerColumnNames,thisColumnName)>
						<cfset headerColumnTitles = listappend(headerColumnTitles,thisColumnTitle)>
						,						
						<cfif isNumeric(nullValuesStruct[valuecol])>
						#nullValuesStruct[valuecol]# 
						<cfelse>
						'#nullValuesStruct[valuecol]#'
						</cfif>
						 as #thisColumnName#
				</cfloop>
			</cfloop>
</cfif>
			from reportquery
			<cfif thisGroupByOrderColumns is not "">
			order by 
			<cf_queryObjectName value="#thisGroupByOrderColumns#">
			</cfif>
		</cfquery>
		
		<cfif thisGroupByOrderColumns is not "">
			<cfquery name = "data" dbType="query">
			select * from reportquery
			order by 
			<cf_queryObjectName value="#thisGroupByOrderColumns#">
			</cfquery>
		<cfelse>
			<cfset data = reportquery>
			
		</cfif>

<!--- 		<cfoutput>WAB Test #wabtest# <BR>headerColumnNames: #headerColumnNames# <BR></cfoutput>
 --->
		<!--- <cfdump var="#grid#"> --->
		
		<!--- now we loop through the data and at the same time move through the blank grid which has been created 
			We test whether we are on the correct row and if not go onto the next row (which will always be the correct one because the ordering of the data and the grid is the same
		--->
			<!--- this is the expression which we evaluate to test whether we are on the correct row --->
			<cfset rowid = 1>
			<cfset expression = "true ">
			<cfloop list="#thisGroupByColumns#" index="Col">
				<cfset expression = expression & " AND #col# IS grid['#col#'][rowid] ">
			</cfloop>
		<!--- 	<cfoutput>#expression# <BR></cfoutput> --->
			<cfloop query="data">
				<!--- if not on the correct row then go onto the next one --->
				<cfif not evaluate(expression)>
					<cfset rowid = rowid + 1>
				</cfif>
				
				<!--- set the values in the grid (if more than one value column then loop over them all --->
				
				<cfloop index="col" list = "#ValueColumns#">
					<cfif listLen(valueColumns) is 1>  <!--- when only one value column, doesn't make sense to put its column name into the header --->
						<cfset headerColumnName = "#reportquery[Headercolumn][currentrow]#">
					<cfelseif wabtest>
						<cfset headerColumnName = "#reportquery[Headercolumn][currentrow]#_#col#">
					<cfelse>
						<cfset headerColumnName = "#col#_#reportquery[Headercolumn][currentrow]#">
					</cfif>
					
					<cftry>
						<cfset querySetCell(grid,headerColumnName,evaluate(col),rowid)		>
						<cfcatch>
							<cfoutput>Cross Tab Error <BR>
							Data Row = #currentRow# ; Grid Row = #rowID# <CF_ABORT>
							</cfoutput>
						</cfcatch>
					</cftry>	
				</cfloop>	
			
			
			</cfloop>

 
		<cfset result.query = grid>
		<cfset result.headerColumnNames = headerColumnNames>
		<cfset result.headerColumnTitles = headerColumnTitles>
		<cfset result.HeaderColumnStruct= HeaderColumnStruct>


		<cfset result.TableFromQueryObjectAttributes = structNew()>

			<!--- Prepare stuff for tablefromquery object --->
			<cfset result.TableFromQueryObjectAttributes.showTheseColumns = "#groupByColumns#,#headerColumnNames#">
			<cfset result.TableFromQueryObjectAttributes.columnHeadingList  = "#groupByTitleColumns#,#headerColumnTitles#">
			<cfset result.TableFromQueryObjectAttributes.rowIdentityColumnName = "compoundkey">
				
					 <cfif keyColumnList is not "">
						<!--- make sure all the key column lists have the right length - slightly nasty with extra comma --->
						<cfif KeyColumnContextMenuList is "">
							<cfset KeyColumnContextMenuList = repeatstring('no,', listlen(keyColumnList))  >
						</cfif>
						<cfif KeyColumnKeyList is "">
							<cfset KeyColumnKeyList = repeatstring('no,', listlen(keyColumnList))  >
						</cfif>
						<cfif KeyColumnContextMenuIDExpressionList is "">
							<cfset KeyColumnContextMenuIDExpressionList = repeatstring('no,', listlen(keyColumnList))  >
						</cfif>




						<!--- loop through all the key columns --->
						<cfloop index="positionInKeyColumnList" to = 1 from = "#listlen(keyColumnList)#" step = -1> <!--- not reverse loop allows us to delete items from list as we go --->
							<cfset keycolumn = listgetat(keyColumnList,positionInKeyColumnList)>
								<!--- if this is one of the columns which is now in the body of the cross tab --->
							<cfif listfindNoCase(valueColumns,keycolumn)>
										<cfloop item = "newcol" collection="#headerColumnStruct[keycolumn]#" >
											<cfset 	 keyColumnList = listappend(keyColumnList,newcol)>
											<cfset 	 keyColumnURLList = listappend(keyColumnURLList,listgetat(keyColumnURLList,positionInKeyColumnList))>
											<cfset 	 keyColumnKeyList = listappend(keyColumnKeyList,"compoundKey")>
											<cfset 	 KeyColumnContextMenuList = listappend(keyColumnContextMenuList,listgetat(KeyColumnContextMenuList,positionInKeyColumnList))>
											<cfset   KeyColumnContextMenuIDExpressionList = listappend(KeyColumnContextMenuIDExpressionList,'compoundkey')>
										</cfloop>

									<!--- now delete this item from all the key column lists --->					
									<cfset keyColumnList = listdeleteAt(keyColumnList,positionInKeyColumnList)>
									<cfset keyColumnKeyList = listdeleteAt(keyColumnKeyList,positionInKeyColumnList)>
									<cfset keyColumnURLList = listdeleteAt(keyColumnURLList,positionInKeyColumnList)>
									<cfset KeyColumnContextMenuList = listdeleteAt(KeyColumnContextMenuList,positionInKeyColumnList)>
									<cfset KeyColumnContextMenuIDExpressionList = listdeleteAt(KeyColumnContextMenuIDExpressionList,positionInKeyColumnList)>											

							</cfif>	
						</cfloop>	
						<cfset result.TableFromQueryObjectAttributes.keyColumnList = keyColumnList>
						<cfset result.TableFromQueryObjectAttributes.keyColumnKeyList = keyColumnKeyList>
						<cfset result.TableFromQueryObjectAttributes.keyColumnURLList =keyColumnURLList>
						<cfset result.TableFromQueryObjectAttributes.KeyColumnContextMenuList = KeyColumnContextMenuList>
						<cfset result.TableFromQueryObjectAttributes.KeyColumnContextMenuIDExpressionList =KeyColumnContextMenuIDExpressionList>
						
					 </cfif>

		


		<cfreturn result>

</cffunction>

<!--- Sums items in a query and adds the sums into the query in the correct place (well reorders it) --->

<cffunction name="ApplyGroupingToQuery" returntype="query" hint="Takes a query and adds Sum rows to it">
 
	 <cfargument name="reportQuery" type="query" >  <!--- the query --->
 
	 <cfargument name="GroupByColumns" type="string" default="">
	 <cfargument name="OrderByColumns" type="string" default="">  <!--- excluding the groupbys --->
	 
		<cfargument name="TotalTheseColumns" type="string" default="">
		<cfargument name="averageTheseColumns" type="string" default="">


		<!---  to do an average we actually need to do the sum, so bolt onto end--->
		<cfset TotalTheseColumns = listappend(TotalTheseColumns,averageTheseColumns)>
		
		<cfif not listfindnocase(reportQuery.columnlist,"groupGeneration")>
			<cfset queryAddColumn (reportQuery,"groupGeneration",arrayNew(1))>
		</cfif>
		<cfif not listfindnocase(reportQuery.columnlist,"rowType")>
			<cfset queryAddColumn (reportQuery,"rowType",arrayNew(1))>
		</cfif>	
		
		<cfquery name= "reportQuery" dbtype="query">
		select *, 0 as groupOrder  from reportQuery 
		</cfquery>

		
		<cfquery name= "groupResults" dbtype="query">
		select * , 0 as groupOrder from reportQuery where 1 = 0
		</cfquery>
	
			<cfset sum = structNew()>
			<cfset Count = structNew()>
						
			<cfset footertestExpression = structNew()>
			<cfset headertestExpression = structNew()>
			<Cfset numberOfGroupingLevels = listLen(GroupByColumns)>
			<!--- Expression to test when change of group--->
			<cfset footertestExpression [0] = 'currentrow is not #reportQuery.recordcount# '> <!--- Note that record count changes as we go! --->
			<cfset headertestExpression [0] = 'currentrow is not 1'> <!--- Note that record count changes as we go! --->

			<cfloop index ="i" from ="1" to ="#numberOfGroupingLevels#" step= 1>
				 <!--- decided that I wanted to work this backwards, means that when sorting the result can sort on generation ascending and get totals in the right order --->
				<cfset col = listgetat(groupByColumns,i)>
				<cfset footertestExpression[i] = footertestExpression[i-1] & " AND (reportQuery['#col#'][currentRow] is reportQuery['#col#'][currentRow +1])">
				<cfset headertestExpression[i] = headertestExpression[i-1]  & " AND (reportQuery['#col#'][currentRow] is reportQuery['#col#'][currentRow -1])">
				<cfset sum [i] = structNew()>
				<cfset count [i] = 0>
				<cfloop list="#TotalTheseColumns#" index="col">
					<cfset sum[i][col] = 0>
				</cfloop>
			</cfloop>

					<cfset count [0] = 0>
				<cfloop list="#TotalTheseColumns#" index="col">
					<cfset sum[0][col] = 0>
				</cfloop>
			
				
			<cfloop query="reportQuery">
				
				<cfloop index ="i" from  ="0" to ="#numberOfGroupingLevels#" step="1">
					<cfif not evaluate(headertestExpression[i])>
							<cfset queryAddRow (groupResults,1)>
							<cfset notI = NumberOfGroupingLevels - I + 1>
							<cfset querySetCell (groupResults,"groupOrder",-noti)>
							<cfset querySetCell (groupResults,"groupGeneration",I)>
							<cfset querySetCell (groupResults,"rowType","GroupHeader")>
							<cfloop list="#GroupByColumns#" index="g">
								<cfset querySetCell (groupResults,g,reportQuery[g][currentRow])>
							</cfloop>
					</cfif>
				</cfloop>

				<cfloop index ="i" to  ="0" from ="#numberOfGroupingLevels#" step="-1">

						<!--- same group so if bottom level add this row values to the accumulator --->
						<cfif i is numberOfGroupingLevels>
							<cfset count[i] = count[i] +1>
							<cfloop list="#TotalTheseColumns#" index="col">
								<cfif isNUmeric(ltrim(reportQuery[col][currentRow])) >
									<cfset sum[i][col] = sum[i][col] + reportQuery[col][currentRow]>
								<cfelse>
									<!--- <cfoutput>Not Numeric: |#reportQuery[col][currentRow]#| <BR></cfoutput> --->
								</cfif>
								
							</cfloop>
						</cfif>

					<cfif evaluate(footertestExpression[i])>
						<cfbreak>	
					<cfelse>	
						<!--- different group so need to 
						a) Output current totals for this level
						b) Add these totals to level above
						c) reset this level to zero
						 --->
							<cfset queryAddRow (groupResults,1)>
							<cfset notI = NumberOfGroupingLevels - I + 1>
							<cfset querySetCell (groupResults,"groupOrder",noti)>
							<cfset querySetCell (groupResults,"groupGeneration",I)>
							
							<cfset querySetCell (groupResults,"rowType","GroupFooter")>
							<cfloop list="#GroupByColumns#" index="g">
								<cfset querySetCell (groupResults,g,reportQuery[g][currentRow])>
							</cfloop>

						<cfloop list="#TotalTheseColumns#" index="col">
							
							<cfset total = sum[i][col]>
							<cfif listFindNoCase (AverageTheseColumns,col)>
								<cfif count[i] is 0>
									<cfoutput>division by zero error #i# #currentrow#</cfoutput>
								<cfelse>	
									<cfset total = total/count[i]>
									<cfset Total =  Total>  <!--- "Average: " & this word is going to be a problem come format time --->
								</cfif>
							</cfif>
							<cfset querySetCell (groupResults,col,total)>

								<cfif i is not 0>
									<cfset sum[i-1][col] = sum[i-1][col] + sum[i][col]>
									<cfset sum[i][col] = 0>
								</cfif>	
						</cfloop>
								<!--- count has to be reset outside the loop, cause it can be used for averaging all columns --->
								<cfif i is not 0>
									<cfset count[i-1] = count[i-1] + count[i]>
									<cfset count[i] = 0>
								</cfif>	

						<BR>
					</cfif>
				
				</cfloop>
							
			</cfloop>

			
	<cfquery dbtype ="query"	name="resultQuery">
	select #reportQuery.columnList#  from reportQuery
	union
	select #reportQuery.columnList# from groupResults
	order by <cf_queryObjectName value="#GroupByColumns#">, groupOrder <cfif orderByColumns is not "">,#orderByColumns#</cfif>
	</cfquery>

	 <cfreturn resultQuery>
	 
</cffunction>	 
	
<cffunction name="createTableFromQueryObjectReportStruct" returntype="struct" hint="Processes all the lists into a structure">

			<cfargument name="numberFormat"  type="string" default=""> 
			<cfargument name="dateFormat"  type="string" default=""> 			
			<cfargument name="currencyFormat"  type="string" default=""> 			
			<cfargument name="IllustrativePriceColumns"  type="string" default=""> 			


			<cfargument name="showTheseColumns"  type="string" default=""> 			
			<cfargument name="keyColumnList"  type="string" default=""> 			
			<cfargument name="GroupByColumns"  type="string" default=""> 			
			
			<cfargument name="reportStruct"  type="struct" default="#structNew()#"> 			

		<cfparam name="attributes.IllustrativePriceColumns" type="string" default=""> <!--- columns that require illustrative pricing to be shown next to them --->
		<cfparam name="attributes.IllustrativePriceFromCurrency" type="string" default=""> <!--- conversion from currency ISOCode --->
		<cfparam name="attributes.IllustrativePriceToCurrency" type="string" default=""> <!--- conversion to currency ISOcode --->
		<cfparam name="attributes.IllustrativePriceDateColumn" type="string" default=""> <!--- query column to use for date of conversion--->
		<cfparam name="attributes.IllustrativePriceMethod" type="string" default="2"> <!--- method for getting the price --->
			
		<cfparam name="reportStruct.colStruct" default = "#structNew()#">
		

		<cfset colStruct = reportStruct.colStruct>  <!--- just a pointer for ease of use --->

		
		<cfloop list = "#showTheseColumns#,#GroupByColumns#,#numberformat#,#dateFormat#,#currencyFormat#,#IllustrativePriceColumns#,#keyColumnList#" index="col">
			<cfparam name="colStruct[col]" default = "#structNew()#">
			<cfparam name="colStruct[col].hasanchor" default="false">
		</cfloop>
		
		<cfloop list = "#numberformat#" index="col">
			<cfset colStruct[col].eval = "LSnumberFormat(colValue)">
			<!--- add excel class stuff here --->
		</cfloop>

		<cfloop list = "#dateFormat#" index="col">
			<cfset colStruct[col].eval = "lsdateFormat(colValue,'medium')">
		</cfloop>

		<cfloop list = "#currencyFormat#" index="col">
			<cfset colStruct[col].eval = "LSCurrencyFormat(colValue,'local')">
		</cfloop>
		
		<cfif keyColumnList is not "">
			<cfloop list = "#keyColumnList#" index="col">
				<!--- first extract info from each list and pop in structure --->
				<cfset i= listfindNoCAse(keyColumnList,col)>
				<cfset keyStruct= structNew()>
				<cfloop list="keyColumnKeyList,keyColumnURLList,keyColumnTargetList,keyColumnOpenInWindowList,OpenWinNameList,OpenWinSettingsList,keyColumnIconList,keyColumnContextMenuList" index="x">
					<cfset property = replace(replace (x,"keyColumn",""),"List","")> <!--- Gives  properties called --->

					<cfif listlen(arguments[x]) is listlen(keyColumnList)>
						<cfset propertyValue = listgetat(arguments[x],i)>
						<cfif propertyValue is "null" or propertyValue is "no"  >  <!--- various different ways of putting blankin a list --->
							<cfset KeyStruct[property] = "">
						<cfelse>
							<cfset KeyStruct[property] = propertyValue>
						</cfif>
						
					<cfelseif arguments[x] is "" >
						<cfset KeyStruct[property] = "">
					<cfelse>
						<cfset KeyStruct[property] = "">
						<cfoutput>list length of #htmleditformat(x)# does not match keycolumnlist</cfoutput>
					</cfif> 				
				</cfloop>
				<cfset colStruct[col].keyCol = keyStruct	>			
				<cfset colStruct[col].hasAnchor = true>			
				
			
				<!--- --------------------------------------------------------------------- --->
<!--- substitute parameter --->
<!--- --------------------------------------------------------------------- --->
					<!--- use for more than 1 param --->
					<cfset keyStruct.URL = replacenocase(keyStruct.URL,"*comma",",","ALL")>   <!--- a hack WAB put in because he needed a comma in a URL, but couldn't get it in via the list --->

					<cfif listLen(keyStruct.Key,"|") gt 1>
						<cfif listLen(keyStruct.URL,"|") eq listLen(keyStruct.Key,"|")>
							<cfset tmpURL = "">
							<cfset lIncr = 1>
							<cfloop list="#keyStruct.Key#" index="item" delimiters="|">
								<cfif lIncr neq 1>
									<cfset tmpURL = tmpURL & "&" >
								</cfif>
									<cfset tmpURL = "#tmpURL##listGetAt(keyStruct.URL,lIncr,'|')####listGetAt(keyStruct.Key,lIncr,'|')###">
								<cfset lIncr = lIncr+1>
							</cfloop>
							<cfset URLToGo = tmpURL>
						<cfelse>
							<cfset URLToGo = "#listGetAt(keyStruct.URL,1,'|')####listGetAt(keyStruct.Key,1,'|')###">
						</cfif>
					<cfelse>
						<cfif FindNoCase( "thisurlkey", keyStruct.URL ) neq 0>
							<cfset URLToGo = ReplaceNoCase( keyStruct.URL, "thisurlkey", "##JSStringFormat(#keyStruct.Key#)##", "one" )>
						<cfelseif keyStruct.Key is not "">
							<cfset URLToGo = "#keyStruct.URL###URLEncodedFormat(#keyStruct.Key#)##">
						<cfelse>
							<cfset URLToGo = "#keyStruct.URL#">
						</cfif>
					</cfif>
					
 					<cfif keyStruct.OpenInWindow is not "" and keyStruct.OpenInWindow is not "no" >
						
						<cfif keyStruct.OpenWinName eq "no" or keyStruct.OpenWinName eq "">
							<cfset winName="MyWindow">
						<cfelse>
							<cfset winName=keyStruct.OpenWinName>
						</cfif>
						<cfif keyStruct.OpenWinSettings eq "no" and  keyStruct.OpenWinSettings eq "">
							<cfset winSettings="width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1">
						<cfelse>
							<cfset winSettings=keyStruct.OpenWinSettings>
						</cfif>
						
						<cfset hrefValue = "javascript:void(openWin('#URLToGo#','#winName#','#winSettings#'));">

					<cfelse>
						<cfset hrefValue = URLToGo>
					</cfif>

					<cfset keyStruct.anchorEval = '"' & hrefValue & '"' >
<!--- 					<cfoutput>keyStruct.anchorEval:#keyStruct.anchorEval#<BR></cfoutput> --->

				

			</cfloop>
		</cfif>

<cfset GroupColStruct = structNew()>
<cfloop index="i" from = 1  to  = "#listLen(groupbycolumns)#">
	<cfset GroupColStruct [i] = listgetat(groupbycolumns,i)>
</cfloop>
<cfset reportStruct.GroupColStruct = GroupColStruct>

		
<!--- --------------------------------------------------------------------- --->
<!--- extra cell attributes for Excel --->
<!--- --------------------------------------------------------------------- --->
<cfoutput>TBD Excel <BR></cfoutput>							
<!--- 	

			<cfif cellType neq 0>
<!--- --------------------------------------------------------------------- --->
<!--- extra cell attributes for Excel --->
<!--- --------------------------------------------------------------------- --->
							<cfswitch expression="#cellType#">
							<!--- need a case here for type 1 - number format --->
								<cfcase value="1">
									<cfset excelCellAttribute = ' x:num="' & attributes.queryObject[i][attributes.queryObject.CurrentRow] & '"' />
								</cfcase>
								<cfcase value="2">
									<cfset excelCellAttribute = ' x:num="' & attributes.queryObject[i][attributes.queryObject.CurrentRow] & '" class="' & excelClass & '"' />
								</cfcase>
								<cfcase value="3">
									<cfif IsDate( attributes.queryObject[i][attributes.queryObject.CurrentRow] )>
										<!--- <cfset excelCellAttribute = ' x:num="' & DateDiff( "d", CreateDate( 1900, 1, 1 ), attributes.queryObject[i][attributes.queryObject.CurrentRow] ) & '" class=xl24' />
										 <cfset excelCellAttribute = ' x:num="' & trim(dateFormat(attributes.queryObject[i][attributes.queryObject.CurrentRow],"dd/mm/yyyy")) & '" class=xl24' />
										 ---><cfset excelCellAttribute = ' class="xl24"' />
									<cfelse>
										<cfset excelCellAttribute = '' />
									</cfif>
								</cfcase>
								<cfdefaultcase>
									<cfset excelCellAttribute = '' />
								</cfdefaultcase>
							</cfswitch>
<!--- --------------------------------------------------------------------- --->
				<cfoutput><td valign="top"#excelCellAttribute# <cfif attributes.showCellColumnHeadings eq "yes">onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()"</cfif>>#cellRendition# </cfoutput>

						<cfswitch expression="#cellType#">
							<!--- need a case here for type 1 - number format --->
								<cfcase value="1">
									<cfset excelCellAttribute = ' x:num="' & attributes.queryObject[i][attributes.queryObject.CurrentRow] & '"' />
								</cfcase>
								<cfcase value="2">
									<cfset excelCellAttribute = ' x:num="' & attributes.queryObject[i][attributes.queryObject.CurrentRow] & '" class="' & excelClass & '"' />
								</cfcase>
								<cfcase value="3">
									<cfif IsDate( attributes.queryObject[i][attributes.queryObject.CurrentRow] )>
										<!--- <cfset excelCellAttribute = ' x:num="' & DateDiff( "d", CreateDate( 1900, 1, 1 ), attributes.queryObject[i][attributes.queryObject.CurrentRow] ) & '" class=xl24' />
										 <cfset excelCellAttribute = ' x:num="' & trim(dateFormat(attributes.queryObject[i][attributes.queryObject.CurrentRow],"dd/mm/yyyy")) & '" class=xl24' />
										 ---><cfset excelCellAttribute = ' class="xl24"' />
									<cfelse>
										<cfset excelCellAttribute = '' />
									</cfif>
								</cfcase>
								<cfdefaultcase>
									<cfset excelCellAttribute = '' />
								</cfdefaultcase>
							</cfswitch>					

 --->
 <cfoutput>TBD Illustrative Pricing<BR></cfoutput>							
<!--- 				<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
				<cfif isDefined("attributes.IllustrativePriceColumns")
					and listFindNoCase(attributes.IllustrativePriceColumns,i) gt 0
					and listFindNoCase("1,2",celltype)>
					<cfswitch expression="#attributes.IllustrativePriceMethod#">
						<cfcase value="2">
							<cfscript>
								if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
									exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
								} else {
									exchangeDate = '';								
								};
								IllustrativePrice = application.com.currency.convert(
									method = #attributes.IllustrativePriceMethod#,
									fromValue = evaluate(i),
									fromCurrency = attributes.IllustrativePriceFromCurrency,
									toCurrency = attributes.IllustrativePriceToCurrency,
									date = '#exchangeDate#'
									);
							</cfscript>
							<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
						</cfcase>
					</cfswitch>
					<cfswitch expression="#cellType#">
						<cfcase value="1">
							<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '"' />
						</cfcase>
						<cfcase value="2">
							<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '" class="' & excelIllustrativeClass & '"' />
						</cfcase>
					</cfswitch>
					<cfoutput><td valign="top"#excelCellAttribute# <cfif attributes.showCellColumnHeadings eq "yes">onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()"</cfif>>#IllustrativePriceOutput#&nbsp;</td></cfoutput>
				</cfif>
			</cfif>


And remember to add columns
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							(#attributes.IllustrativePriceFromCurrency#)&nbsp;
						</cfif>
					</cfif>
					</th>
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							<th valign="top">&nbsp;#thisColumnHeading# (#attributes.IllustrativePriceToCurrency#) phr_IllUSTRATIVE&nbsp;</th>
						</cfif>
					</cfif>
					
				<cfif listFindNoCase(attributes.hideTheseColumns,i) eq 0>
					<th valign="top">
						&nbsp;#replace(i,"_"," ","all")#&nbsp;
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							(#attributes.IllustrativePriceFromCurrency#)&nbsp;
						</cfif>
					</cfif>					
					</th>
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							<th valign="top">&nbsp;#replace(i,"_"," ","all")# (#attributes.IllustrativePriceToCurrency#) phr_IllUSTRATIVE&nbsp;</th>
						</cfif>
					</cfif>
					

 --->
	<cfreturn reportStruct>
			
</cffunction>


<cffunction name="ApplyFormattingToQuery" returntype="struct" hint="Formats columns and adds special columns">
		<cfargument name="reportQuery" type="query">



			<cfargument name="numberFormat"  type="string" default=""> 
			<cfargument name="dateFormat"  type="string" default=""> 			
			<cfargument name="currencyFormat"  type="string" default=""> 			
			<cfargument name="IllustrativePriceColumns"  type="string" default=""> 			


			<cfargument name="keyColumnList"  type="string" default=""> 			
			<cfargument name="groupByColumns"  type="string" default=""> 			
			
			<cfargument name="reportStruct"  type="struct" default="#structNew()#"> 			
		

		<cfset var result = structNew()>

		<cfparam name="attributes.IllustrativePriceColumns" type="string" default=""> <!--- columns that require illustrative pricing to be shown next to them --->
		<cfparam name="attributes.IllustrativePriceFromCurrency" type="string" default=""> <!--- conversion from currency ISOCode --->
		<cfparam name="attributes.IllustrativePriceToCurrency" type="string" default=""> <!--- conversion to currency ISOcode --->
		<cfparam name="attributes.IllustrativePriceDateColumn" type="string" default=""> <!--- query column to use for date of conversion--->
		<cfparam name="attributes.IllustrativePriceMethod" type="string" default="2"> <!--- method for getting the price --->

		
		<cfset reportStruct = createTableFromQueryObjectReportStruct (argumentCollection = arguments)>
		<cfset colstruct = reportStruct.colstruct>

		<!--- <cfloop item="col" collection = #colstruct#>
			<cfif structKeyExists (colstruct[col],"keyCol")>
				<cfset queryAddColumn(reportQuery,"#col#_Anchor",arrayNew(1))>
			</cfif>
		</cfloop> --->

		<!--- build up anchor expression --->
				

		<cfloop query="reportQuery">
			
			<cfloop item="col" collection="#colStruct#">
				<cfif structKeyExists(colStruct[col],"eval") and colStruct[col].eval is not "">
					<cfset colValue = reportQuery[col][currentRow]>
					<cfif not (rowType is "GroupHeader" and not listfindnocase(groupByColumns,col))>  <!--- don't format headers (end up with 0.00 type things) ---> 
						<cfset newColValue = evaluate(colStruct[col].eval)>
						<cfset querySetCell (reportQuery,col,newColValue,currentRow)>
					</cfif>	
				</cfif>
			
				<!--- <cfif structKeyExists(colStruct[col],"keyCol") >
					<cftry>
						<cfset anchor = evaluate(colStruct[col].keyCol.anchorEval)>
						<cfset querySetCell (reportQuery,"#col#_Anchor",anchor,currentRow)>
						<cfcatch>
							<cfoutput>Couldn't evaluate #colStruct[col].keyCol.anchorEval# for </cfoutput>
						</cfcatch>
					</cftry>	

				
				</cfif> --->
			
			
			</cfloop>
		
		</cfloop>
	
		<cfset result.query = reportQuery>
		<cfset result.reportStruct = reportStruct>
		
		<cfreturn result>
		
		
</cffunction>

<!--- 
This function makes a nice array which can be used to output table headings on multiple rows with colspan grouping
use the delimiter #application.delim1# within a heading to indicate a new row.
If the next heading in the list has the same heading on that row, then they are grouped together with a colspan

 --->
<cffunction name="createTableFromQueryObjectTitleArray" returntype="array" hint="Makes an array for easy outputting of titles on multiple rows">
		<cfargument name="columnHeadingList"  type="string" default=""> 
		<cfargument name="columnNameList"  type="string" default=""> 
		<cfargument name="columnTranslation"  type="string" default=""> 
		<cfargument name="columnTranslationPrefix"  type="string" default=""> 		


	
		<cfset var headingArray = arrayNew(1)>
		<cfset var columnListIndex = 0>
		<cfset var numberOfRows = 0> 
		<cfset var thisHeading = ""> 
		<cfset var thisRow = 0> 
		<cfset var cellcounter = 0> 
		<cfset var numberOfColumns = listlen(columnHeadingList)> 
		
	<!--- work out max number of rows --->

	<cfloop index = "thisHeading" list = #columnHeadingList#>
		<cfset numberOfRows = max(numberOfRows,listLen(thisHeading,"#application.delim1#"))>
	</cfloop>
	
		<cfloop index="thisROw" from = 1 to= #numberOfRows#> <!--- loop through each potential row--->
			<cfset headingArray[thisRow] = arrayNew(1)>
			<cfset cellcounter = 0> <!--- counts number of cells across, to index the result array --->
			<cfset columnListIndex = 0> <!--- counts position in the columnHeadingList--->
			<cfloop condition ="columnListIndex LT numberOfColumns" > <!--- loop until we get through the list --->
				<cfset columnListIndex = columnListIndex + 1>
				<cfset thisheading = listgetat(columnheadingList,columnListIndex)>
				<cfif listlen(thisheading,"#application.delim1#") lt thisRow >
					<!--- this heading has less items in it than the row we are upto, therefore all its stuff has already been dealt with --->
				<cfelse >
					<!--- get title for this column and row --->
					<cfset thisRowHeading = listgetat(thisHeading,thisRow,"#application.delim1#")>

					
					<cfif thisRow is listlen(thisheading,"#application.delim1#")> <!--- this is last item in list, therefore give a rowspan to the end of the rows --->
						<cfset rowSpan = numberOfRows - listlen(thisheading,"#application.delim1#") +1>
						<cfset lastItemInColumn = true>
					<cfelse>
						<cfset rowSpan = 1>
						<cfset lastItemInColumn = false>
					</cfif>

					<!--- loop through to see if this item it is same as next, actually loop until the next item is not the same
						each time through the loop we increment the colspan and columnListIndex 
					 --->
					<cfset colspan = 1>
					<cfloop condition='columnListIndex lt numberOfColumns and listlen(listgetat(columnheadingList,columnListIndex+1),"�#application.delim1#") gte thisROw and thisRowHeading is listgetat(listgetat(columnheadingList,columnListIndex+1),thisrow,"�#application.delim1#") and thisRow is not listlen(thisheading,"�#application.delim1#")'>
					<!--- 
						columnListIndex lt listlen(headingList)  -- not last item in list
						listlen(listgetat(headingList,columnListIndex+1),"#application.delim1#") gte thisROw  -- next heading has an item for this row
						thisRowHeading is listgetat(listgetat(headingList,columnListIndex+1),thisrow,"�#application.delim1#") -- next heading item same as this one
						and thisRow is not listlen(thisheading,"#application.delim1#")  -- prevents last row items having a colspan (may or may not be desired
					 --->
					
							<cfset colspan = colspan + 1>				
							<cfset columnListIndex= columnListIndex  + 1>	
					</cfloop>
						<!--- ready to 'output' a  cell, pop details into array --->				
						<cfif columnTranslation>
							<cfset thisRowHeading = columnTranslationPrefix & thisRowHeading>
						<cfelse>
							<cfset thisRowHeading = replace(thisRowHeading,"_"," ","all")>
						</cfif>

						<cfset cellcounter = cellcounter + 1>
						<cfset headingArray[thisRow][cellcounter]= structNew()>
						<cfset headingArray[thisRow][cellcounter].colspan = colspan>
						<cfset headingArray[thisRow][cellcounter].rowspan = #rowspan#>
						<cfset headingArray[thisRow][cellcounter].content = #thisRowheading#>
						<cfset headingArray[thisRow][cellcounter].lastItemInColumn = lastItemInColumn>
						<cfset headingArray[thisRow][cellcounter].columnName = listgetat(columnNameList,columnListIndex)>
					
			
				</cfif>
		
		
			</cfloop>

		</cfloop>	

	<cfreturn headingArray>	

</cffunction>

	
	<cffunction name="getRegOrgTotals" returntype="query" hint="Returns a query of the total Sony1 approved organisations per country">
		
		<cfscript>
			var qGetRegOrgTotals = "";
		</cfscript>

		<cfquery name="qGetRegOrgTotals" datasource="#application.sitedatasource#">
			select 
				'Approved Orgs' as reportcolumns,
				country as reportrows,
				count(organisations) as reportunits
							from calendarweek c
							left join
				(select 
						yearorder, weekorder, weekcommencing,
						country,
						organisations
					from vR_FlagsByOrgFlagGroup_RegOrgs_week_country
					where flaggroupid=818 and countryid  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and countryid in (#request.relaycurrentuser.countrylist#)
			) a
			on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
			group by country
		</cfquery>
		<cfreturn qGetRegOrgTotals>
	</cffunction>
	
	<cffunction name="getRegOrgTotalsVertical" returntype="query" hint="Returns a query of the total Sony1 approved organisations per country">
		
		<cfscript>
			var qGetRegOrgTotalsVertical = "";
		</cfscript>

		<cfquery name="qGetRegOrgTotalsVertical" datasource="#application.sitedatasource#">
			select 
				country as reportcolumns,
				'Approved Orgs' as reportrows,
				count(organisations) as reportunits
							from calendarweek c
							left join
				(select 
						yearorder, weekorder, weekcommencing,
						country,
						organisations
					from vR_FlagsByOrgFlagGroup_RegOrgs_week_country
					where flaggroupid=818 and countryid  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and countryid in (#request.relaycurrentuser.countrylist#)
			) a
			on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
			group by country
		</cfquery>
		<cfreturn qGetRegOrgTotalsVertical>
	</cffunction>
	
	<cffunction name="getRegPerTotals" returntype="query" hint="Returns a query of the total Sony1 approved organisations per country">
		
		<cfscript>
			var qGetRegPerTotals = "";
		</cfscript>

		<cfquery name="qGetRegPerTotals" datasource="#application.sitedatasource#">
			select 
				'Approved Pers' as reportcolumns,
				country as reportrows,
				count(people) as reportunits
							from calendarweek c
							left join
				(select 
						yearorder, weekorder, weekcommencing ,
						country,
						people
					from vR_FlagsByOrgFlagGroup_RegPersonsAtRegOrgs_week_country
					where flaggroupid=818 and countryid  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and countryid in (#request.relaycurrentuser.countrylist#)
			) a
			on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
			group by country
		</cfquery>
		<cfreturn qGetRegPerTotals>
	</cffunction>

	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            Function - flagGroupRegistered          
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 
 
	
 	<cffunction name="flagGroupRegistered" access="public" returntype="string" hint="Returns 3 reports for a given flaggroup - all companies - registered only - not registered only. Note this requires the view ">
		<cfargument name="flaggroup" type="numeric" required="yes" hint="">
		<!--- WAB 2005-11-08 altered because the orginal query was asuming that the flag was a person one, now will deal with person/location/organisation
			won't deal with other types of flags (although easily could)
			note that the country is from the organisation table if organisation flag, otherwise from location table
		 --->
		
		<cfset var theFlaggroupStructure = application.com.flag.getFlagGroupStructure(flaggroupid)>
		<cfset var dataTableName = theFlaggroupStructure.flagType.datatablefullname>
		<cfset var entityType = theFlaggroupStructure.entityType.tablename>
		<cfset var entityAlias = left(entitytype,1)>
		<cfset var entityuniquekey = theFlaggroupStructure.entityType.uniquekey>
		<cfset var thequery_sql = "">	
		<cfsetting enablecfoutputonly="yes">
		
		
		<cfset cachedWithinSpan = CreateTimeSpan(1,0,0,0)>
		

		<cfset Reports = structNew()>
		<cfset Reports[1] = structNew()>
		<cfset Reports[1].Name = "Resellers">
		<cfset Reports[1].ID = "Resellers">
		<cfset reports[1].type = "crosstab">
		<cfset reports[1].ParameterList = "ctDisplayColumnTotals='bottom',ctDisplayRowTotals='yes',ctDisplayRowTitles='yes',showemptycols=yes,ctOrderRowsBy='',showContextMenu=true,contextMenuItems='selection,drilldown_Person,drilldown_Organisation'">
		<cfsaveContent variable="thequery_sql"> <cfoutput>
			SELECT 
				f.name as <cfif this.reportColumns is "ISOCode">reportrows<cfelse>reportcolumns</cfif>, 
				c.isocode as <cfif this.reportColumns is "ISOCode">reportcolumns<cfelse>reportrows</cfif>, 
				count(fd.entityid) as reportunits
			FROM #dataTableName# fd inner join flag f
					on fd.flagid = f.flagid 
					inner join #entityType# #entityAlias#
						on #entityAlias#.#entityuniquekey# = fd.entityid

				<cfif entityType is "person">
				inner join location l on l.locationid = p.personid
				</cfif>
				<cfif entityType is "person" or entityType is "location">
				inner join organisation o on o.organisationid = l.organisationid
				</cfif>
				inner join flaggroup fg
				on fg.flaggroupid = f.flaggroupid
				inner join country c
				on <cfif entityType is "organisation">o<cfelse>l</cfif>.countryid = c.countryid
			WHERE f.flaggroupid =  <cf_queryparam value="#arguments.flaggroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and o.organisationID not in (select organisationID from #this.orgExclusionView#)
					and c.countryid in (#request.relaycurrentuser.countrylist#)
			GROUP BY f.name, c.isocode
			</cfoutput>
			</cfsavecontent>
			<cfset reports[1].query = theQuery_sql>
		
		<cfset Reports[2] = structNew()>
		<cfset Reports[2].Name = "Reg Resellers">
		<cfset Reports[2].ID = "RegResellers">
		<cfset reports[2].type = "crosstab">
		<cfset reports[2].ParameterList = "ctDisplayColumnTotals='bottom',ctDisplayRowTotals='yes',ctDisplayRowTitles='yes',showemptycols=yes,ctOrderRowsBy='',showContextMenu=true,contextMenuItems='selection,drilldown_Person,drilldown_Organisation'">
		<cfsaveContent variable="thequery_sql"> <cfoutput>
			SELECT 
				f.name as <cfif this.reportColumns is "ISOCode">reportrows<cfelse>reportcolumns</cfif>, 
				c.isocode as <cfif this.reportColumns is "ISOCode">reportcolumns<cfelse>reportrows</cfif>, 
				count(fd.entityid) as reportunits
			FROM #dataTableName# fd inner join flag f
					on fd.flagid = f.flagid 
					inner join #entityType# #entityAlias#
					on #entityAlias#.#entityuniquekey# = fd.entityid
		
					<cfif entityType is "person">
					inner join location l on l.locationid = p.personid
					</cfif>
					<cfif entityType is "person" or entityType is "location">
					inner join organisation o on o.organisationid = l.organisationid
					</cfif>
					inner join flaggroup fg
					on fg.flaggroupid = f.flaggroupid
					inner join country c
			on <cfif entityType is "organisation">o<cfelse>l</cfif>.countryid = c.countryid
			WHERE f.flaggroupid =  <cf_queryparam value="#arguments.flaggroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and o.organisationID in    (select organisationid from #this.RegisteredApprovedOrgsView#)
				and o.organisationID not in (select organisationID from #this.orgExclusionView#)
				and c.countryid in (#request.relaycurrentuser.countrylist#)
			GROUP BY f.name, c.isocode
			</cfoutput>
			</cfsavecontent>
			<cfset reports[2].query = theQuery_sql>

		<cfset Reports[3] = structNew()>
		<cfset Reports[3].Name = "Non Reg Resellers">
		<cfset Reports[3].ID = "NonRegResellers">
		<cfset reports[3].type = "crosstab">
		<cfset reports[3].ParameterList = "ctDisplayColumnTotals='bottom',ctDisplayRowTotals='yes',ctDisplayRowTitles='yes',showemptycols=yes,ctOrderRowsBy='',showContextMenu=true,contextMenuItems='selection,drilldown_Person,drilldown_Organisation'">
		<cfsaveContent variable="thequery_sql"> <cfoutput>
			SELECT 
				f.name as <cfif this.reportColumns is "ISOCode">reportrows<cfelse>reportcolumns</cfif>, 
				c.isocode as <cfif this.reportColumns is "ISOCode">reportcolumns<cfelse>reportrows</cfif>, 
				count(fd.entityid) as reportunits
			FROM #dataTableName# fd inner join flag f
				on fd.flagid = f.flagid 
				inner join #entityType# #entityAlias#
				on #entityAlias#.#entityuniquekey# = fd.entityid
	
				<cfif entityType is "person">
				inner join location l on l.locationid = p.personid
				</cfif>
				<cfif entityType is "person" or entityType is "location">
				inner join organisation o on o.organisationid = l.organisationid
				</cfif>
				inner join flaggroup fg
				on fg.flaggroupid = f.flaggroupid
				inner join country c
					on <cfif entityType is "organisation">o<cfelse>l</cfif>.countryid = c.countryid
			WHERE f.flaggroupid =  <cf_queryparam value="#arguments.flaggroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and o.organisationid not in
					(select organisationid from #this.RegisteredApprovedOrgsView#)
					and o.organisationID not in (select organisationID from #this.orgExclusionView#)
					and c.countryid in (#request.relaycurrentuser.countrylist#)
			GROUP BY f.name, c.isocode
			</cfoutput>
			</cfsavecontent>
			<cfset reports[3].query = theQuery_sql>

			
			<cfset drillDownDone = doMultipleReportDrillDown(reports)>
			<cfif  drillDownDone is true><CF_ABORT></cfif>

			

		<cfoutput>
		<table cellpadding="5">
			<tr><td colspan="3">
			<table>
			<tr><th height="12"><cfoutput>#htmleditformat(theFlaggroupStructure.name)#</cfoutput></th></tr>
			<tr>
				<td align="center"><strong>All Companies</strong></td>
		
			</tr>
			<tr>
				<td align="center" valign="top"> <!--- <cfset x=crossTabReport(		evaluate("Resellers"),'yes','yes','bottom','','no')>  --->
						#runReport(reportDefinition = reports[1])#
						<!--- 				<cfset x=crossTabReport(reportQuery=resellers,ctDisplayRowTotals='yes',ctDisplayRowTitles='yes',ctDisplayColumnTotals='bottom',ctOrderRowsBy='',showemptycols='no',drilldown=true,saveAsSelection=true,id="resellers")> --->
				</td>
			</tr>
			<tr><td height="12"></td></tr>
			<tr>
				<td align="center"><strong>Registered Companies Only</strong></td>
		
			</tr>
			<tr>
				<td align="center" valign="top">
						#runReport(reportDefinition = reports[2])#
						<!--- <cfset x=crossTabReport(evaluate("RegResellers"),'yes','yes','bottom','','no')> --->
				</td>
			</tr>
			<tr>
				<td align="center"><strong>Non Registered Companies Only</strong></td>
		
			</tr>
			<tr>
				<td align="center" valign="top">
							#runReport(reportDefinition = reports[2])#
<!--- 				<cfset x=crossTabReport(evaluate("NonRegResellers"),'yes','yes','bottom','','no')> --->
				</td>
			</tr>
			</table>
			
			</td></tr> 
		
		</table>
		</cfoutput>
	</cffunction>
	
	
 	<cffunction output="true" name="parentflagGroupRegisteredResellers" returntype="string" access="public" hint="Returns 3 reports for a given flaggroup - all companies - registered only - not registered only.">
		<cfargument name="parentflaggroup" type="numeric" required="yes" hint="">

		<cfscript>
			var qParentflaggroup = "";
		</cfscript>
			
		<cfset cachedWithinSpan = CreateTimeSpan(1,0,0,0)>
				
		<cfquery name="qParentflaggroup" datasource="#application.sitedatasource#">
			select name from flaggroup where flaggroupid=#arguments.parentflaggroup#
		</cfquery>
		<cfquery name="flaggroups" datasource="#application.sitedatasource#">
			select flaggroupid, name from flaggroup where parentflaggroupid=#arguments.parentflaggroup#
		</cfquery>
		<cfloop query="flaggroups">
			<cfquery cachedWithin=#cachedWithinSpan# name="AllDataOrg#flaggroups.flaggroupid#" datasource="#application.sitedatasource#">
			select f.name as reportrows, c.isocode as reportcolumns, count(organisationid) as reportunits
			from booleanflagdata b inner join flag f
			on b.flagid = f.flagid 
			inner join organisation o
			on o.organisationid = b.entityid
			inner join flaggroup fg
			on fg.flaggroupid = f.flaggroupid
			inner join country c
			on o.countryid = c.countryid
			where f.flaggroupid =  <cf_queryparam value="#flaggroups.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and o.organisationID not in (select organisationID from #this.orgExclusionView#)
				and o.organisationID in (select organisationid from #this.ResellerOrgsView#)
				and c.countryid in (#request.relaycurrentuser.countrylist#)
			group by f.name, c.isocode
			</cfquery>
			<cfquery cachedWithin=#cachedWithinSpan# name="RegDataOrg#flaggroups.flaggroupid#" datasource="#application.sitedatasource#">
			select f.name as reportrows, c.isocode as reportcolumns, count(organisationid) as reportunits
			from booleanflagdata b inner join flag f
			on b.flagid = f.flagid 
			inner join organisation o
			on o.organisationid = b.entityid
			inner join flaggroup fg
			on fg.flaggroupid = f.flaggroupid
			inner join country c
			on o.countryid = c.countryid
			where f.flaggroupid =  <cf_queryparam value="#flaggroups.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and o.organisationid in
				(select organisationid from #this.RegisteredApprovedOrgsView#)
				and o.organisationID not in (select organisationID from #this.orgExclusionView#)
				and o.organisationID in (select organisationid from #this.ResellerOrgsView#)
				and c.countryid in (#request.relaycurrentuser.countrylist#)
			group by f.name, c.isocode
			</cfquery>
			<cfquery cachedWithin=#cachedWithinSpan# name="NonRegDataOrg#flaggroups.flaggroupid#" datasource="#application.sitedatasource#">
			select f.name as reportrows, c.isocode as reportcolumns, count(organisationid) as reportunits
			from booleanflagdata b inner join flag f
			on b.flagid = f.flagid 
			inner join organisation o
			on o.organisationid = b.entityid
			inner join flaggroup fg
			on fg.flaggroupid = f.flaggroupid
			inner join country c
			on o.countryid = c.countryid
			where f.flaggroupid =  <cf_queryparam value="#flaggroups.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and o.organisationid in
				(select organisationid from #this.RegisteredApprovedOrgsView#)
				and o.organisationID not in (select organisationID from #this.orgExclusionView#)
				and o.organisationID in (select organisationid from #this.ResellerOrgsView#)
				and c.countryid in (#request.relaycurrentuser.countrylist#)
			group by f.name, c.isocode
			</cfquery>
		</cfloop>
		<table cellpadding="5">
			<tr><th colspan="3">#htmleditformat(qParentflaggroup.name)#</th></tr>
			<tr><td colspan="3">
			<table>
			<cfoutput query="flaggroups">
				<tr><th height="24">#htmleditformat(name)#</th></tr>
				<tr>
					<td align="center"><strong>All Reseller Companies</strong></td>
			
				</tr>
				<tr>
					<td align="center" valign="top"><cfset x=crossTabReport(evaluate("AllDataOrg#flaggroupid#"),'yes','yes','bottom','','no')></td>
				</tr>
				<tr><td height="12"></td></tr>
				<tr>
					<td align="center"><strong>Registered Reseller Companies Only</strong></td>
			
				</tr>
				<tr>
					<td align="center" valign="top"><cfset x=crossTabReport(evaluate("RegDataOrg#flaggroupid#"),'yes','yes','bottom','','no')></td>
				</tr>
				<tr><td height="12"></td></tr>
				<tr>
					<td align="center"><strong>Non Registered Reseller Companies Only</strong></td>
			
				</tr>
				<tr>
					<td align="center" valign="top"><cfset x=crossTabReport(evaluate("RegDataOrg#flaggroupid#"),'yes','yes','bottom','','no')></td>
				</tr>
				<tr><td height="12"></td></tr>
			</cfoutput>
			</table>
			</td></tr> 
		</table>
	</cffunction>



 	<cffunction output="true" name="runReport"  access="public" hint="runs report as defined in parameters">
			<cfargument name="reportDefinition" type="struct" required="yes" hint="">			
			<cfset var theQuery = "">

			<cfif structKeyExists(reportDefinition,"parameterList") >
				<cfset argumentCollection = application.com.regexp.convertNameValuePairStringToStructure (reportDefinition.parameterList,",")>
			<cfelse>
				 <cfset argumentCollection = structNew()>
			</cfif>
			
		<cfif isDefined("frmDrillDownAction") and frmDrillDownAction is not "">
		
			<cfset x = doReportDrillDown (reportDefinition = reportDefinition,argumentCollection = argumentCollection)>
			<cfif x is true>

				<CFRETURN>
			
			</cfif>
		</cfif>
		
		
		
		<cfswitch expression="#reportDefinition.type#">
			<cfcase value="crosstab">
				<!--- build query --->
				<cfset reportQuery = application.com.regExp.parseQuery (reportDefinition.query)>
	
				<cfquery name="data" datasource="#application.sitedatasource#" cachedwithin="#createtimespan (1,0,0,0)#">
				SELECT 
					#preserveSingleQuotes(reportQuery.select)#	
				FROM
					#preserveSingleQuotes(reportQuery.from)# <!--- as theView --->
				WHERE 
				<cfif reportQuery.where is not "">#preserveSingleQuotes(reportQuery.where)#<cfelse>1 = 1</cfif>
					<cfset temp = 	buildQueryWhereClause(reportQuery)>
					#preserveSingleQuotes(temp)#
	
				<cfif structKeyExists (reportQuery,"groupBy") and trim(reportQuery.groupBy) is not "">
					GROUP BY #preserveSingleQuotes(reportQuery.groupBy)#
				</cfif>
				
				<cfif isDefined("form.sortOrder") and form.sortOrder is not "">
				ORDER BY #form.sortOrder#
				<cfelseif structKeyExists (reportQuery,"orderBy") and reportQuery.orderby is not "">
				ORDER BY #reportQuery.orderby#
				</cfif>
				</cfquery>
				

				<!--- we have a default behaviour here, but the argument collection will override any parameters --->
				<cfset x= crossTabReport(reportQuery=data,ctDisplayRowTotals='no',ctDisplayRowTitles='yes',ctDisplayColumnTotals='',ctOrderRowsBy='',showemptycols='yes',showContextMenu=true,contextMenuItems="selection,drilldown_Person,drilldown_Organisation",id="#ReportDefinition.id#", argumentCollection=#argumentCollection#)>
				
				
			
			</cfcase>
		
			<cfcase value="table">

				<cfset theQuery = applyWhereClauseToQuery(query = reportDefinition.query)>


				<cfquery name="data" datasource="#application.sitedatasource#" cachedwithin="#createtimespan (1,0,0,0)#">
				#preserveSingleQuotes(theQuery)#				
				</cfquery>
			
				<cfif structKeyExists(reportdefinition,"passThroughVariablesStructure")>
					<cfset argumentCollection.passThroughVariablesStructure = reportDefinition.passThroughVariablesStructure>
				</cfif>
			
				<cf_tablefromqueryobject queryobject ="#data#"  useinclude=false id = #reportDefinition.id#  attributeCollection=#argumentCollection#>
			
			</cfcase>
			
			<cfcase value="commoncolumn">

				<cfset x =commonColumnReport(commonColumnReport = reportdefinition.id,argumentCollection=argumentCollection)>

			</cfcase>

		
		</cfswitch>


	
		

	
	
	
	</cffunction>
	

 	<cffunction output="true" name="doReportDrillDown"  access="public" hint="processes the drill downs for a report">
		<cfargument name="reportDefinition" type="struct" required="yes" hint="">			

		<!--- these defaults are for common column, and need to be the same as in the commoncolumn function (sorry bit nasty) --->
		<cfargument name="whereClause" type="string" required="no" hint="">
		<cfargument name="UseCountryFilter" type ="boolean" required="no" default="yes" hint="">
		<cfargument name="UseLiveCountryIDs" type ="boolean" required="no" default="yes" hint="">

		<cfset var querysql_sql = "">
		<cfset var querySqlForEntityID_sql = "">
		
		<!--- check whether these is a drilldown to process, if not then goes to end of function --->
		<cfif isDefined("frmDrillDownAction") and frmDrillDownAction is not "">

		
			<cfset justIDs = false>  <!--- determines whether we are going to build a query to just return ids or not --->

			<cfif gettoken(frmReportID,1,"__")  is reportDefinition.ID>
				<!--- the drilldown is for this report --->
		
				<!--- if drilldown type is selection we just need to get personids --->
				<!--- if the drilldown is say location, and we don't have a specific report defined then need to get ids and run a default report --->
				<cfset reports = #frmReportID#>
				<cfset thisReportDefinition = ReportDefinition>
				<cfif listFirst(frmDrillDownAction,"_") is "selection">
					<cfset justIDs = true>
					<cfset entityType = "person">
				<cfelseif not structKeyExists(reportDefinition,frmDrillDownAction)>
					<cfset justIDs = true>
					<cfset entityType = listlast(frmDrillDownAction,"_")>
					<cfset reportEntity = entityType>
				<cfelse>
					<cfset thisReportDefinition = reportDefinition[frmDrillDownAction]>
				</cfif>
				

				<!--- get the query definition here - if it is a common column report need to look in the database --->
				<cfif thisReportDefinition.type is "commoncolumn">
				
						<!--- bit of a problem here because we have to duplicate some stuff in the commoncolumn function 
							would like to work out a better way of doing it 
							in particular I am having to copy the default arguments and I need to evaluate the parameter list
							
						--->
						<cfif structKeyExists(thisReportDefinition,"ParameterList")>
							<cf_evaluateNameValuePair NameValuePairs = #thisReportDefinition.parameterList#>
						</cfif>	
						
						<cfquery name="getreport" datasource="#application.sitedataSource#">
						select * from commonColumnReport where reportTitle =  <cf_queryparam value="#gettoken(frmReportID,1,"__")#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfquery>
						<cfset ncount = gettoken(frmReportID,2,"__")>
						<cfset view = gettoken(getreport.reportViewList,nCount,",")>
						<cfset whereClauseList = gettoken(getreport.whereClauseList,nCount,";")>
						
						<cfsavecontent variable = "thisReportDefinition.query_sql">
							SELECT 
								* 
							FROM 
								#view# 
								WHERE
								1 = 1
							<cfif whereClauseList neq "">
							and (#preservesinglequotes(whereClauseList)#)
							</cfif>
							<cfif isDefined("UseCountryFilter") and UseCountryFilter>
							and countryid in (#request.relaycurrentuser.countrylist#) 
							</cfif>
							<cfif isDefined("UseLiveCountryIDs") and UseLiveCountryIDs>
								<cfif structkeyExists(application,"LiveCountryIDs")>
								and countryid  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
								</cfif> 
							</cfif>
						</cfsavecontent>
					<!--- we've now built (not run) a query which would bring back the commonColumn Report --->				
				</cfif>


				<!--- parse the query into its bits --->
				<cfset queryDefinition = application.com.regExp.parseQuery (thisReportDefinition.query_sql)>

				
				<cfif justIDs>
					<!--- does query have correct id column --->
 
					<cfif listFindNoCase(queryDefinition.AllColumns,"#entityType#id") is not 0>
						<cfset idColumn = #queryDefinition.column["#entityType#id"].actualName#>
					<cfelseif entityType is "person" and	listFindNoCase(queryDefinition.AllColumns,"people") is not 0>
						<cfset idColumn = "people">
					<cfelseif entityType is "organisation" and	listFindNoCase(queryDefinition.AllColumns,"companies") is not 0>
						<cfset idColumn = "companies">
					<cfelseif entityType is "organisation" and	listFindNoCase(queryDefinition.AllColumns,"organisations") is not 0>
						<cfset idColumn = "organisations">
					<cfelseif entityType is "person" and	listFindNoCase(queryDefinition.AllColumns,"organisationid") is not 0>
						<cfset idColumn = #queryDefinition.column["organisationid"].actualName#>
						<cfset entityType = "organisation">
					<cfelseif entityType is "person" and	listFindNoCase(queryDefinition.AllColumns,"companies") is not 0>
						<cfset idColumn = #queryDefinition.column["companies"].actualName#>
						<cfset entityType = "organisation">
					<cfelseif entityType is "person" and	listFindNoCase(queryDefinition.AllColumns,"organisations") is not 0>
						<cfset idColumn = #queryDefinition.column["organisations"].actualName#>
						<cfset entityType = "organisation">
					<cfelse>
						<cfoutput>No #htmleditformat(entityType)#id column exists </cfoutput> <CF_ABORT>
					</cfif>	
					
					
					<cfsavecontent 	variable="querySqlForEntityID_sql">
					<cfoutput>SELECT distinct #idColumn# from
					#preserveSingleQuotes(queryDefinition.from)# 
					WHERE
					<cfif queryDefinition.where is not "">#queryDefinition.where#<cfelse>1 = 1</cfif>
					#buildQueryWhereClause(queryDefinition)#

					</cfoutput>
					</cfsavecontent>	

					<cfset selectionTitle = ""	>
					<cfset selectionDescription = "#ReportDefinition.name#: #frmRowID# 	">
					<cfset selectionAction = listgetat(frmDrillDownAction,2,"_")>

					<cfif listFirst(frmDrillDownAction,"_") is "selection">
						<!--- <cfoutput>#querySQL#</cfoutput> <CF_ABORT> --->
		
						<cfset frmTask= selectionAction>
						<cfset frmRunInPopUP=true>
						<cfset "frm#entityType#IDs"=querySqlForEntityID_sql>
						<cfset frmDescription=selectionDescription>
						<cfset frmTitle=selectionTitle>
						<cfinclude template = "\selection\selectalter.cfm">

						<!--- 		
						kept having problems with CF location - didn't seem to like lots of the escaped characters in the querystring	
						gave up and went for cfinclude		
						<cfset querySQL = replaceNoCase(querySQL,chr(9)," ","ALL")>  
						<cfset querySQL = replaceNoCase(querySQL,chr(13)&chr(10)," ","ALL")>
						<cfset querySQL = replaceNoCase(querySQL,"    "," ","ALL")>  <!--- sometimes cflocation seems to have problems with all the spaces??	 --->
						<cfset querySQL = replaceNoCase(querySQL,"  "," ","ALL")>

						<cfset selectionDescription = urlencodedFormat(selectionDescription)	>
						<cfset querySQl = 		urlencodedFormat(querySQl )>
						<CFLOCATION url="/selection/selectalter.cfm?frmTask=#selectionAction#&frmRunInPopUP=true&frm#entityType#IDs=#querySQL#&frmDescription=#selectionDescription#&frmTitle=#selectionTitle#"> --->
						<CF_ABORT>
					</cfif>


					<!---  load up a standard screen --->
						<!--- give a standard report for this type - ought to be defined somewhere or other and passed the entityids--->
						<cfsavecontent variable = "querysql_sql">
							<cfoutput>
							<cfif reportEntity is "person">
							SELECT firstname + ' ' + lastname as name, email,OrganisationName, personid ,organisationid FROM vpeople
							<cfelseif reportEntity is "organisation">
							SELECT organisationname, organisationid FROM organisation
							</cfif>
							WHERE 
							<cfif EntityType is "person">
							personid in (#preserveSingleQuotes(querySqlForEntityID_sql)#)
							<cfelseif EntityType is "organisation">
							organisationid in (#preserveSingleQuotes(querySqlForEntityID_sql)#)
							</cfif>
							<cfif isDefined("form.sortOrder") and form.sortOrder is not "">
							ORDER BY #form.sortOrder#
							<cfelseif reportEntity is "organisation">
							ORDER BY organisationname
							<cfelseif reportEntity is "Person">							
							ORDER BY firstname
							</cfif> 
							</cfoutput>
						</cfsavecontent>
	
						<!--- need to remember these for tablefromqueryobject to keep working.  Ideally should actually pass off this query to a completely different process which remembers it --->
						<cfset passThruVars = structNew()>	
						<CFSET passThruVars.frmReportID = frmReportID>
						<CFSET passThruVars.frmRowID = frmRowID>
						<CFSET passThruVars.frmRowIDCols = frmRowIDCols>
						<CFSET passThruVars.frmDrillDownAction = frmDrillDownAction>
						<!--- creates variable queryString of all parameters --->
						<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">	
				
						<cfset form.frmDrillDownAction = "">   <!--- so that filter isn't applied again to this query --->

						<cfparam name="openAsExcel" type="boolean" default="false">

						<cfset report = structNew()>
						<cfset report.query = querySQL_sql>
						<cfset report.id = "areport">						
						<cfset report.type = "table">							
						<cfset report.parameterList = "">
						<cfset dataframeWindowSettings = "width=700,height=400,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1">
						<cfif reportEntity is "person">
							<cfset report.parameterList = "showTheseColumns='name,email,organisationname',keyColumnList='name,organisationname',keyColumnURLList='/data/dataframe.cfm?frmSrchPersonid=,/data/dataframe.cfm?frmSrchOrgid=',keyColumnKeyList='personid,organisationid',keyColumnOpenInWindowList='yes,yes',OpenWinName='dataFrame,dataframe',keyStruct.OpenWinSettings='#dataFrameWindowSettings#,#dataFrameWindowSettings#',openasExcel=#openasExcel#">
						<cfelseif reportEntity is "organisation">
							<cfset report.parameterList = "showTheseColumns='organisationname',keyColumnList='organisationname',keyColumnURLList='/data/dataframe.cfm?frmSrchOrgid=',keyColumnKeyList=organisationid,keyColumnOpenInWindowList=yes,OpenWinName=dataFrame,keyStruct.OpenWinSettings='#dataFrameWindowSettings#',openAsExcel=#openasExcel#">
						</cfif>
						

						
						<cfoutput>
						<cfif openAsExcel>
							<cfset frmRowID = ""> <!--- hack here - need to null this out to prevent filters being added to this query again (or something) --->
							<cfcontent type="application/text">
							<cfheader name="content-Disposition" value="filename=results.xls"> 
							<cfinclude template="/templates/excelHeaderInclude.cfm">
							
						<cfelse>

						
						<cf_head>
							<cf_title>#request.CurrentSite.Title#</cf_title>
						</cf_head>
						
							<A Href="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target=@@>Open in Excel</A>		
						</cfif>
						
						<cfset runReport (report)>				
						<cfif not openAsExcel>
						
								
						</cfif>				
						</cfoutput>
					
				<cfelse>
						<!--- there is a specific drill down report defined --->
						<cfset passThruVars = structNew()>	
						<CFSET passThruVars.frmReportID = frmReportID>
						<CFSET passThruVars.frmRowID = frmRowID>
						<CFSET passThruVars.frmRowIDCols = frmRowIDCols>
						<CFSET passThruVars.frmDrillDownAction = frmDrillDownAction>

						<cfset runReport (thisReportDefinition)>				
					
				
				</cfif >


			

				

				<cfreturn true>  

			<cfelse>
				<cfreturn false>  <!--- ie no drill down done --->
			</cfif>
		</cfif>
	
	
	
	
	</cffunction>


 	<cffunction output="true" name="doMultipleReportDrillDown"  access="public" hint="processes the drill downs for a report">
		<cfargument name="reportDefinitions" type="struct" required="yes" hint="">			
		
		<cfset drillDownDone = false>

		<cfif isDefined("frmDrillDownAction") and frmDrillDownAction is not "">
			<cfset keyList = structKeyList (reportDefinitions)>
			<cfloop index = "thisItem" list = "#keyList#">
				<cfif reportDefinitions[thisItem].ID is gettoken(frmReportID,1,"__")>
				
					<cfset doReportDrillDown (reportDefinitions[thisItem])>
					<cfset drillDownDone = true>
				
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn drillDownDone>
		
	</cffunction>

	
 	<cffunction output="false" name="buildQueryWhereClause"  access="public" hint="">
		<cfargument name="queryDefinition" type="struct" required="no" hint="">						
		<cfargument name="query" type="string" required="no" hint="">						

		<cfset var result_sql = "">
		
		<cfif not isDefined ("queryDefinition") and isDefined(query)>
			<cfset queryDefinition = application.com.regExp.parseQuery (query)>
		<cfelse>
			<cfoutput>query or queryDefinition required</cfoutput>	
		</cfif>


			<cfsavecontent variable = "result_sql">
					<cfif isDefined("frmDrillDownAction") and frmDrillDownAction is not "">
					<cfif frmRowID is not "">
						<cfloop index = "x" from="1" to =#listLen(frmRowIDCols,"|")#>
							<cfset field = listGetAt(frmRowIDCols,x,"|")>
							<cfset value = listGetAt(frmRowID,x,"|")>							
							<cfif trim(value) is not "">
								<cfoutput>and	#queryDefinition.column[field].actualName# = '#value#'</cfoutput>
							</cfif>
						</cfloop>
					</cfif>				
				</cfif>
			</cfsavecontent>
		<cfreturn result_sql>
	</cffunction>

 	<cffunction output="false" name="applyWhereClauseToQuery"  access="public" hint="">
		<cfargument name="queryDefinition" type="struct" required="no" hint="">						
		<cfargument name="query" type="string" required="no" hint="">						
		
		<cfset var result_sql = "">
		
		<cfif not isDefined ("arguments.queryDefinition") and isDefined("arguments.query")>
			<cfset arguments.queryDefinition = application.com.regExp.parseQuery (query)>
		<cfelse>
			<cfoutput>query or queryDefinition required</cfoutput>	
		</cfif>
	
	
			<cfsavecontent variable = "result_sql">
				<cfoutput>
				SELECT 
					#preserveSingleQuotes(arguments.queryDefinition.select)#
				FROM
					#preserveSingleQuotes(arguments.queryDefinition.from)# <!--- as theView --->
				WHERE 
				<cfif arguments.queryDefinition.where is not "">#arguments.queryDefinition.where#<cfelse>1 = 1</cfif>
	
					#buildQueryWhereClause(arguments.queryDefinition)#
	
				<cfif structKeyExists (arguments.queryDefinition,"groupBy") and trim(arguments.queryDefinition.groupBy) is not "">
					GROUP BY #arguments.queryDefinition.groupBy#
				</cfif>
				
				<cfif isDefined("form.sortOrder") and form.sortOrder is not "">
				ORDER BY #form.sortOrder#
				<cfelseif structKeyExists (arguments.queryDefinition,"orderBy") and arguments.queryDefinition.orderby is not "">
				ORDER BY #arguments.queryDefinition.orderby#
				</cfif>
				</cfoutput>
			</cfsavecontent>
	
		<cfreturn result_sql>
	</cffunction>
	
	
 </cfcomponent>

