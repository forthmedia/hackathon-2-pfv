<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			POLCountsByCountry.cfm	
Author:				SWJ
Date started:		2007-02-22
	
Description:		Gives a good overview of the data in the database

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-02-22	SWJ extended the number of rows to show per page
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:

Average locs and people per org
How many orgs with no people
How many locs with no people
How many with 2 or more people
How densely populated is job function


 --->
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="country">

	<cf_head>
		<cf_title>Counts by Country</cf_title>
	</cf_head>
	
	
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="LEFT" CLASS="SubMenu">Counts of Records In The Database By Country</TD>
			<cfoutput>
			<td align="right" class="SubMenu"><a href="#cgi["SCRIPT_NAME"]#?openAsExcel=true" target="_blank">Open In Excel</a></td>
			</cfoutput>
		</TR>
	</TABLE>

<cfquery name="totalCounts" datasource="#application.siteDataSource#" 
	cachedwithin="#createTimeSpan(0,0,20,0)#">
	select 
		(select count(*) from organisation) as Total_organisations,
		(select count(*) from person) as Total_People,
		(select count(*) from location) as Total_Locations,
		(select count(distinct organisationID) from organisation o where o.organisationID 
			not in (select distinct organisationID from person)) as total_organisations_with_no_people,
		(select count(*) from location where locationID 
			not in (select distinct locationID from person)) as total_locations_with_no_people,
		Average_people_per_organisation = 
			CASE (select count(distinct organisationID) from person) WHEN 0 THEN 0
			ELSE (select cast(round(1.00 * count(personid)/count(distinct organisationID),1) as money) from person) END,
		Average_people_per_location =
			CASE (select count(distinct locationID) from person) WHEN 0 THEN 0
			ELSE (select cast(round(1.00 * count(personid)/count(distinct locationID),1) as money) from person) END,
		(select count(*) from person where len(ltrim(rtrim(email))) > 1) as email_address_not_blank
</cfquery>


<cfquery name="initialiseCountsByCountry" datasource="#application.siteDataSource#" 
	cachedwithin="#createTimeSpan(0,0,20,0)#">
	select distinct c.CountryDescription as country, c.countryID,
		(select count(*) from organisation where countryID = c.countryID) as organisations,
		(select count(*) from vPeople where countryID = c.countryID) as People,
		(select count(*) from location where countryID = c.countryID) as Locations,
		(select count(*) from organisation o where o.countryID = c.countryID and o.organisationID 
			not in (select distinct organisationID from person)) as organisations_with_no_people,
		(select count(*) from location where countryID = c.countryID and locationID 
			not in (select distinct locationID from person)) as locations_with_no_people,
		Average_people_per_organisation = 
			CASE (select count(distinct organisationID) from vpeople where countryID = c.countryID) WHEN 0 THEN 0
			ELSE (select cast(round(1.00 * count(personid)/count(distinct organisationID),1) as money) from vpeople where countryID = c.countryID) END ,
		Average_people_per_location	= 
			CASE (select count(distinct locationID) from vpeople where countryID = c.countryID) WHEN 0 THEN 0
			ELSE (select cast(round(1.00 * count(personid)/count(distinct locationID),1) as money) from vpeople where countryID = c.countryID) END ,
		(select count(*) from vpeople where len(email) > 1 and countryID = c.countryID) as email_address_not_blank
		from organisation o
		inner join country c ON c.countryID = o.countryID
	order by countryDescription
</cfquery>

<cfquery name="countsByCountry" dbtype="query">
	select *
		from initialiseCountsByCountry
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
<p align="center"><strong>Total Records</strong></p>
<cfoutput query="totalCounts">
	<table border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
		<tr>
			<th>Organisations</th>
			<th>Locations</th>
			<th>People</th>
			<th>Total Organisations with No People</th>
			<th>Total Locations with No People</th>
			<th>Average People per Organisation</th>
			<th>Average People per Location</th>
			<th>Email addresses not blank</th>
		</tr>
		<tr>
			<td>#htmleditformat(Total_organisations)#</td>
			<td>#htmleditformat(Total_Locations)#</td>
			<td>#htmleditformat(Total_People)#</td>
			<td>#htmleditformat(total_organisations_with_no_people)#</td>
			<td>#htmleditformat(total_locations_with_no_people)#</td>
			<td>#htmleditformat(Average_people_per_organisation)#</td>
			<td>#htmleditformat(Average_people_per_location)#</td>
			<td>#htmleditformat(email_address_not_blank)#</td>
		</tr>
	</table>

</cfoutput>
<p align="center"><strong>Records by Country</strong></p>
<cfif IsQuery(countsByCountry)>
	<CF_tableFromQueryObject queryObject="#countsByCountry#" 
	
	showTheseColumns="Country,Organisations,Locations,People,organisations_with_no_people,locations_with_no_people,Average_people_per_organisation,Average_people_per_location,email_address_not_blank"
	
	HidePageControls="yes"
	useInclude = "false"
	
	numRowsPerPage="400"
	
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#">
</cfif>




