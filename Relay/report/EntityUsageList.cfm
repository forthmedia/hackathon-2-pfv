<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		UsageList.cfm
Author:			SWJ
Date created:	

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-10-29	SWJ	Added the frmPersonID cfparam call
2002-01-31	SWJ	Modified layout
2006-05-20 SWJ Modified layout and logic around dates to show
2006-06-12 WAB wasn't working when first called (UserToView not defined).  Put in a few more ifs
2013-11-21	WAB	2013RoadMap2 Item 25 Altered so that will respond to parameter VisitID
2015-06-19 DAN Case 444904 - site visits in the last 30 days should be calculated from the current date
--->

<cf_head>
<SCRIPT type="text/javascript">
<!--
function viewOrg(orgID){
		form = document.dataFrame;
		form.frmsrchOrgID.value = orgID;
		form.submit();
	}
	
function toggleVisibility( divName, callerObject )	{
	  document.all[divName].style.display = ( document.all[divName].style.display == "" ) ? "none" : "";
	  if ( callerObject )
	  {
	    callerObject.src = ( document.all[divName].style.display == "" ) ? iconDoubleArrowUp.src : iconDoubleArrowDown.src;
	  }
	  return true;
	}
	
var iconDoubleArrowUp = new Image();
var iconDoubleArrowDown = new Image();

iconDoubleArrowUp.src = "/images/MISC/iconDoubleArrowUp.gif";
iconDoubleArrowDown.src = "/images/MISC/iconDoubleArrowDown.gif";

//-->
</SCRIPT>

</cf_head>


<CFPARAM NAME="frmPersonID" TYPE="numeric" DEFAULT="#request.relayCurrentUser.personID#">
<CFPARAM NAME="reportMode" TYPE="string" DEFAULT="standalone">
<CFPARAM NAME="datePeriod" TYPE="string" DEFAULT="30">

<!--- DAN we just need to calculate the date from now --->
<cfset fromDate = dateAdd("d",-datePeriod, now()) >
<!---<CFQUERY NAME="getDate" datasource="#application.sitedatasource#">
    select dateAdd(day,-#datePeriod#,max(created)) as thisDate from entityTracking
</CFQUERY>--->


<CFIF isDefined("UserToView") AND UserToView GT 0>

	<cfset 	userDetails = application.com.commonQueries.getFullPersonDetails (personid=UserToView)>

<CFQUERY NAME="GetEntityUsage" datasource="#application.sitedatasource#">
		SELECT ET.EntityID, E.Headline, ET.Created, ET.ToLoc, ET.FromLoc, o.organisationName, o.organisationID,
			p.firstName+' '+p.lastName as fullName, 
			(datePart(day,et.created)+'-'+datePart(month,et.created)+'-'+datePart(year,et.created)) as dateOrder,
			p.personid
		FROM EntityTracking ET INNER JOIN
		     Element E ON ET.EntityID = E.id INNER JOIN
			 person p ON ET.personID = p.personid INNER JOIN organisation o
			 on p.organisationID = o.organisationID
		WHERE (ET.EntityTypeID = 10)

		<cfif isdefined("visitID")>
			AND et.visitid =  <cf_queryparam value="#visitid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelse>
			AND ET.Created >  <cf_queryparam value="#fromDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfif>

			AND p.personid =  <cf_queryparam value="#UserToView#" CFSQLTYPE="CF_SQL_INTEGER" > 
		order by fullName,et.created DESC
	</CFQUERY>

</CFIF>

<CFIF reportMode eq "Standalone">
	<CFQUERY NAME="TheUsers" datasource="#application.sitedatasource#">
		SELECT DISTINCT p.personid,  p.firstName+' '+p.lastName AS ThisUser, organisationName, lastName, firstName  
			FROM EntityTracking ET INNER JOIN
		     Element E ON ET.EntityID = E.id INNER JOIN
			 person p ON ET.personID = p.personid INNER JOIN organisation o
			 on p.organisationID = o.organisationID
		WHERE (ET.EntityTypeID = 10)
			<!--- <CFIF not isDefined("UserToView")> --->
			AND ET.Created >  <cf_queryparam value="#fromDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			<!--- </CFIF> --->
		ORDER BY organisationName, lastName, firstName
	</CFQUERY>
</CFIF>


<TABLE CLASS="withBorder table">
	
	
	<CFIF reportMode eq "Standalone">
	<FORM NAME="FilterForm" METHOD="post">
		<CFIF isDefined("UserToView") AND UserToView GT 0>
		<tr>
			<td colspan="3" CLASS="label" STYLE="font-weight: bold;">Site Visits by <CFOUTPUT>#htmleditformat(userDetails.Fullname)# (<A HREF="javaScript:viewOrg(#userDetails.organisationID#)">#htmleditformat(userDetails.OrganisationName)#</A>) in the last #htmleditformat(datePeriod)# days</CFOUTPUT></td>
		</tr>	
		</cfif>
		<tr>
			<td colspan="3" CLASS="label">View site visits for a different person&nbsp;
				<SELECT NAME="UserToView" ONCHANGE="document.FilterForm.submit()">
				<OPTION Value = 0> Select a different person
				<CFOUTPUT QUERY="TheUsers">
					<OPTION VALUE="#personid#"<CFIF isdefined("UserToView") AND UserToView EQ personid> SELECTED</CFIF> >#htmleditformat(ThisUser)# (#htmleditformat(organisationName)#)
				</CFOUTPUT>
			
			</SELECT>
			</TD>
		</tr>
		</FORM>
	</CFIF>
	<CFIF isDefined("UserToView") AND UserToView GT 0>
		<cfloop QUERY="GetEntityUsage" GROUP="fullname">
			
			<cfloop GROUP="dateOrder">
				<TR>
					<cfoutput><TH COLSPAN="3" ALIGN="left"><img src="/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-#dateOrder#', this );">#dateformat(Created,"dd-mmm-yy")#</TH></cfoutput>
				</tr>
				<TR>
					<TD COLSPAN="3">
						<cfoutput><div id="div-#dateOrder#" <cfif currentRow neq 1>style="display: none;"</cfif>></cfoutput>
				<TABLE class="table table-striped">
					<TR>
						<TH>Time</TH>
						<TH>Page Visited</TH>
						<TH>URL To - URL from</TH>
					</TR>
					<cfloop>
						<CFOUTPUT>
							<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
							 	<td>#timeformat(Created,"HH:MM")#</td>
								<td>#htmleditformat(Headline)#</td>
								<td>To: #left(ToLoc,100)#<br>From: #left(FromLoc,100)#</TD>
							</TR>	
						</CFOUTPUT>
					</cfloop>
					</TABLE>
				</div>
					</TD>
				</TR>
				
			</cfloop>
		</cfloop>
	<CFELSE>
		<TR>
			<TD>Please select a user to view their siteVisits.</TD>
		</TR>
	</CFIF>
</TABLE>

<CFOUTPUT>
	<FORM ACTION="/data/dataFrame.cfm" METHOD="post" NAME="dataFrame" ID="dataFrame">
		<INPUT TYPE="hidden" NAME="frmsrchOrgID" VALUE="0">
		<CF_INPUT TYPE="hidden" NAME="frmNext" VALUE="/report/EntityUsageList.cfm?#request.query_string#">
	</FORM>
</CFOUTPUT>