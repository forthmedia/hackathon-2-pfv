<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

	Author: SWJ

This file produces a short list of key stats about the database.  It is used on the homepage.cfm

	   --->

<CFQUERY NAME="GetNumbPers" datasource="#application.sitedatasource#">
	select count(*) as numb from person where active = 1
</CFQUERY>

<CFQUERY NAME="GetNumbOrgs" datasource="#application.sitedatasource#">
	select count(*) as numb from organisation where active = 1
</CFQUERY>

<CFQUERY NAME="GetNumbofComms" datasource="#application.sitedatasource#">
	select * from person where active = 1
</CFQUERY>



<cf_head>
<cf_title>Key Stats</cf_title>
</cf_head>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Key Database Statistics</DIV>
		</TD>
	</TR>
	<TR>
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<A HREF="javaScript: history.back()" CLASS="Submenu">Back</A>&nbsp;&nbsp;
		</TD>
	</TR>
</table>

<CFOUTPUT>
	<TABLE CELLSPACING="2" CELLPADDING="2" BORDER="0">
	<TR>
	    <TD>Total Number of Active Organisations</TD>
	    <TD>#htmleditformat(GetNumbOrgs.numb)#</TD>
	</TR>
	<TR>
	    <TD>Total Number of Active People</TD>
	    <TD>#htmleditformat(GetNumbPers.numb)#</TD>
	</TR>
	<TR>
	    <TD></TD>
	    <TD></TD>
	</TR>
	</TABLE>
</CFOUTPUT>




