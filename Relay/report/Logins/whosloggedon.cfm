<!--- �Relayware. All Rights Reserved 2014 --->
<!-- William's Code for seeing who is logged on 
2005-10-06 - SWJ Changed to use table from query and app and filter by app
--->

<CFSET freezedate=now()>
<cfparam name="sortOrder" type="string" default="logged_in DESC">
<cfparam name="startRow" default="1">

<CFQUERY NAME="WhosLoggedOn" DATASOURCE="#application.SiteDataSource#">
SELECT 	* from vLoggedInSince
WHERE 	 logged_in > dateadd(day, -2, getDate())
<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
ORDER BY <cf_queryObjectName value="#sortOrder#">
</CFQUERY>

<cf_head>
	<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT> - Who has logged on recently?</cf_title>
</cf_head>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="SubMenu"><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT> - Who has logged on recently?</TD>
	</TR>
</TABLE>

<CF_tableFromQueryObject 
	queryObject="#WhosLoggedOn#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="200"
	
	allowColumnSorting="yes"
	
	<!--- hideTheseColumns="#leadAndOppListHideTheColumns#" --->
	showTheseColumns="USER_NAME,ORGANISATION,MINUTES_ON,LOGGED_IN,SITE,USERS_IP_ADDRESS,BROWSER" optionally overrides the query column list and display order
<!--- 	
	
	currencyFormat="budget"
	dateFormat="#dateFormat#"
 --->
	FilterSelectFieldList="SITE"

	startRow="#startRow#"
>








