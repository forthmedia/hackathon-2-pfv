<!--- �Relayware. All Rights Reserved 2014 --->
<!---Modification Hisory
2011-05-27	NYB	change from request.CurrentSite.Title to application.com.settings.getSetting('settings.theclient.clientname')

--->
<CFQUERY NAME="getLoginsByPerson" DATASOURCE="#application.siteDataSource#" DBTYPE="ODBC">
select firstname+' '+lastname as name, 
	datepart(HH,logindate) as hour,
	count(*) as logins
from usage inner join person on person.personid=usage.personid
where logindate>#CreateODBCDate(now())#
group by datepart(hh,logindate),firstname, lastname
</CFQUERY>

<CFQUERY NAME="getLoginsByHour" DATASOURCE="#application.siteDataSource#" DBTYPE="ODBC">
select datepart(HH,logindate) as hour,count(*) as logins
from usage inner join person on person.personid=usage.personid
where logindate>#CreateODBCDate(now())# and logindate<#dateAdd("d",now(),1)#
group by datepart(hh,logindate)
</CFQUERY>
<TABLE WIDTH="450" BORDER="0" CELLSPACING="1" CELLPADDING="3" BGCOLOR="White">
	<tr><td class="Submenu" COLSPAN="3"><CFOUTPUT>#application.com.settings.getSetting('settings.theclient.clientname')#</CFOUTPUT> Logins by Hour</td>
	<tr><td COLSPAN="3">&nbsp;</td></tr>
	<TR>
		<TH>Name</TH>
		<TH>Hour</TH>
		<TH>Logins</TH>
	</TR>
	<CFOUTPUT QUERY="getLoginsByPerson">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD>#htmleditformat(name)#</TD>
			<TD ALIGN="centre">#htmleditformat(hour)#</TD>
			<TD ALIGN="centre">#htmleditformat(logins)#</TD>
		</TR>
	</CFOUTPUT>
</TABLE>
<BR>
<TABLE WIDTH="450" BORDER="0" CELLSPACING="1" CELLPADDING="3" BGCOLOR="White">
	<TR>
		<TH>Hour</TH>
		<TH>Logins</TH>
	</TR>
	<CFOUTPUT QUERY="getLoginsByHour">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD ALIGN="centre">#htmleditformat(hour)#</TD>
			<TD ALIGN="centre">#htmleditformat(logins)#</TD>
		</TR>
	</CFOUTPUT>
</TABLE>
