<!--- �Relayware. All Rights Reserved 2014 --->
<cf_translate>

<cfif isDefined("URL.reportName")>
	<CFSET reportName=URL.reportName>
</cfif>
<cfif isDefined("Form.reportName")>
	<CFSET reportName=Form.reportName>
</cfif>
<cfif isDefined("Form.module")>
	<CFSET reportName=Form.module>
</cfif>
<cfif isDefined("url.module")>
	<CFSET reportName=url.module>
</cfif>


<cf_head>
	<cf_title>Generic XML Report</cf_title>
</cf_head>

<cfif not isDefined("reportName")>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR>
			<TD WIDTH="10px" HEIGHT="300px">&nbsp;</TD>
			<TD VALIGN="top"><strong>phr_Sys_Pleaseselectareportfromthelistontheleft</strong></TD>
		</TR>
	</TABLE>


<CFELSE>
	<CF_RelayXMLReport
		reportName = "#reportName#"
	>
</cfif>
</cf_translate>