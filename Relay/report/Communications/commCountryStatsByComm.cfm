<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Mods:

WAB 2006-09-18     added a datesent column
NYB 2011-07-22 LHID6853 - added frmCountryID to passThroughVariablesStructure as was losing country when page numbers where clicked on
2011-07-27	NYB		P-LEN024 changed commcheckDetails.cfm to commDetailsHome.cfm


 --->

<cfparam name="frmCountryID" default = "#request.relaycurrentuser.countryid#">


<cfquery name="countries" datasource = "#application.sitedatasource#">
select countrydescription as displayvalue, countryid as datavalue from country
where countryid in (#request.relaycurrentuser.countryList#)
and isocode <> ''
order by countrydescription
</cfquery>

<cfparam name="range" default="">
<cfif isDefined("form.thisDateRange")>
	<cf_dateRangeRetrieve dateRange = #form.thisdateRange#>	
<cfelseif isDefined("url.thisDateRange")>
	<cf_dateRangeRetrieve dateRange = #url.thisdateRange#>	
<cfelse>
	<cf_DateRange type="month" value="current" year="#year(now())#">
	<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">
</cfif>

	<cfset reports = structNew()>

	<cfset thisReport = "CommsByCountry">
	<cfset Reports[thisReport] = structNew()>
	<cfset Reports[thisReport].Name = "Communications By Country">
	<cfset Reports[thisReport].ID = thisReport>
	<cfset Reports[thisReport].type = "table">

	<!--- NJH 2012/11/14 Case 431805 changed ##readPercent## to simply readPercent --->
	<cfset Reports[thisReport].ParameterList = "ColumnHeadingList='ID,Title,DateSent,Successful Sent,Read,Distinct Clicks,Total Clicks,Failed,Read/Sent%', showTheseColumns='COMMID,title,datesent,successfulsent,received,distinctClicks,TotalClicks,Notreceived,readPercent',useinclude=false,keycolumnlist='title',keycolumnkeylist='commid',keyColumnURLList='javascript:commCheckDetaildrillDown(thisurlkey)',dateformat='datesent'">
	<!--- START: NYB 2011-07-22 LHID6853 - added frmCountryID to passThroughVariablesStructure as was losing country when page numbers where clicked on --->
	<cfset Reports[thisReport].passThroughVariablesStructure = structNew() >
	<cfset Reports[thisReport].passThroughVariablesStructure["frmCountryID"] = frmCountryID >
	<cfif isdefined("thisDateRange")>
		<!--- END: NYB 2011-07-22 LHID6853 --->
		<cfset Reports[thisReport].passThroughVariablesStructure["thisDateRange"] = thisDateRange >
	</cfif>	

<!--- note, statuses between 0 and -19 are not sent --->
	<cfset Reports[thisReport].query = "
				SELECT 	CountryDescription,
						p.CountryID as thecountryid, 
						com.CommID,
						com.title,						
						(select min(sentdate) from commselection cs where sent = 1 and commselectlid = 0 and cs.commid = com.commid) as dateSent,    -- earliest sent external selection 
						case when cd.commTypeId = 2 then 'email' when cd.commTypeId = 3 then 'fax' end  as EmailorFax,
						count (distinct cd.personid) as People,
						count( distinct case when cd.statusGroup = 'notReceived' then cd.personid else null end) as NotReceived,  
						count( distinct case when cd.statusGroup in  ('sent','received') then cd.personid else null end) as successfulsent,
					--	count( distinct case when cd.statusGroup not  in  ('notsent') then cd.personid else null end) as sent,
						count( distinct case when cd.statusGroup = 'received' then cd.personid else null end) as received,
					--	count( distinct case when cd.statusGroup = 'notSent' then cd.personid else null end) as notSent,
					--	count( distinct case when cd.statusGroup = '' then cd.personid else null end) as other,
						case when count( distinct case when cd.statusGroup not in ('notsent') then cd.personid else null end) <> 0 then (100* count( distinct case when cd.statusGroup = 'received' then cd.personid else null end) / count( distinct case when cd.statusGroup not in ('notsent') then cd.personid else null end)) else 0 end as readPercent,


 
 --						count (distinct p.organisationid) as organisations
						count (distinct et.personid) as distinctclicks,
						count ( et.personid) as 	totalclicks
				FROM 	(Communication as com
							inner join 
							vCommDetailMostSignificantStatus cd
									on com.commid = cd.commid and cd.test = 0 and cd.internal = 0
							inner join 
 						vPeople p on p.personid = cd.personid 
							inner join country c
						on p.countryid = c.countryid	
							
							left join  
						entityTracking et on et.commid = com.commid	and et.personid = cd.personid
							)
				WHERE 	
								(select min(sentdate) from commselection cs where cs.commid = com.commid and commselectlid = 0 and sent = 1) >#createODBCdate(StartDate)#
						and		(select min(sentdate) from commselection cs where cs.commid = com.commid and commselectlid = 0 and sent = 1) <=#createODBCdate(EndDate)#
						and p.OrganisationID not in (select organisationid from vOrgsToSupressFromReports)
						and com.commformlid in (2,3,5,6,7)
						and cd.commTypeID in (2,3)
						and commTypeLid <> 27
						and (select distinct 1 from commselection cs where cs.commid = com.commid and cs.commselectlid = 0 and cs.sent = 1) = 1
						and p.countryid = #frmCountryID#
				GROUP BY 		commTypeId,CountryDescription ,	p.CountryID, com.commid, com.title
				ORDER BY 		commTypeId,CountryDescription ,	com.commid, com.title
				-- 1
				">


<cfset nReport = CreateObject("component","relay.report.reporting")>

<cfset drilldowndone = nReport.doMultipleReportDrillDown(reports)>
<cfif drilldowndone is true><CF_ABORT></cfif>
				
				

<cf_head>
    <cf_title>Communications Analysis</cf_title>
</cf_head>

<CFOUTPUT>

<!--- this form is for drilling down to the list of communications (doesn't use context menus) --->
<!--- <form name = "CommCheckDetailDrilldownForm" method = "post" action = "/communicate/commDetailsHome.cfm" target="drillDownIFrame2">
	<input type="hidden" name = "frmcommid" value = "">
	<input type="hidden" name = "frmruninpopup" value = "true">
	
</form> --->

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

<script>
	function commCheckDetaildrillDown(commid) {
		/*form = document.CommCheckDetailDrilldownForm
		form.frmcommid.value = commid
		showReportChildDiv  (1)
		form.submit()*/
		openNewTab('CommunicationDrillDown','Communication Statistics By Country Detail','/communicate/commDetailsHome.cfm?frmcommid='+commid,{iconClass:'communicate'});
	}


	function showReportChildDiv  (show)
	{
		var  parentZ = document.getElementById('reportMain')  ;
		var  childZ = document.getElementById('reportChild') ;


		if (show) {
			parentZ.style.zIndex =  1 ;
			childZ.style.zIndex = 2 ;
			childZ.style.display = 'block';
		} else {
			parentZ.style.zIndex =  2 ;
			childZ.style.zIndex = 1 ;
			childZ.style.display = 'none';
		}


	
	}

</script>


<div id="reportWrapper" 
	style="absolute;
		left: 0;
		top: 0; overflow:hidden">


	<div id ="reportMain" style="z-index: 2;
		position: absolute;
		left: 0;
		top: 0;
		background:##FFFFFF;
		width:100%;height:100%;
		white-space: normal;"
		onclick = "javascript:showReportChildDiv (0)"
		
		>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<TR>
	<TD ALIGN="LEFT" VALIGN="top" CLASS="Submenu">
		Country Email Statistics by Communication <BR>
	</TD>
</TR>

<tr>
	<td>
		Between #dateformat(StartDate,'dd-mmm-yy')# and #dateformat(EndDate,'dd-mmm-yy')#
	</td>
</tr>
<tr>
	<td>
	<cfform method="post" name="dateRangeForm" id="dateRangeForm">
			<cf_DateRangeSelectBox BoxName="thisDateRange" SelectedRange="#range#">

			<BR>
			<cf_displayValidValues 
				validValues = "#countries#"
				formfieldname = "frmCountryID"
				currentValue = #frmCountryID#
			
			>
			<BR>

			<input type="submit" value="Search">

			
			
	</cfform>
	</td>
</tr>
</table>
</cfoutput>



	<cfoutput>#nReport.runReport(reportDefinition = reports["CommsByCountry"])#			</cfoutput>
<!--- <cf_tablefromqueryobject queryObject = #communicationsByCountry# useinclude = false keycolumnlist='people' keycolumnkeylist='people' keyColumnURLList='xx' keyColumnContextMenuList='Menu1'> --->


<CFINCLUDE TEMPLATE="../../templates/InternalFooter.cfm">

	</div>
	
	
	<div id ="reportChild" style="z-index: 1;
		position: relative;
		left: 30;
		background:#c3c3c3;
		top: 30;width:100%;height:100%"
		>
	
	
		<iframe name="drillDownIFrame2" width = "100%" height = "100%" 
		style="background:#c3c3c3;"
		>
	
	
		</iframe>
	
	
	</div>

		
	
</div>



