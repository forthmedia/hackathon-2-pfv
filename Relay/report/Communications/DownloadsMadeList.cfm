<!--- �Relayware. All Rights Reserved 2014 --->

<CFSET displaystart = 1>
<CFSET showrecords = 40>


<CFQUERY NAME="SelectCommsMade"
         datasource="#application.sitedatasource#">
SELECT Communication.Title as CommTitle, 
	Selection.Title as SelTitle, 
	Communication.Description, 
	Communication.SendDate, 
	Communication.createdby, 
	UserGroup.Name, 
	Communication.CommID,
	commfile.directory,
	commfile.filename
FROM 	Selection, Communication, Person, UserGroup, CommSelection, commfile
where 	Person.PersonID = UserGroup.PersonID
and 	Communication.CreatedBy = UserGroup.UserGroupID
and 	Communication.CommID = CommSelection.CommID
and 	Selection.SelectionID = CommSelection.SelectionID
and 	Communication.CommID = Commfile.commID
aND 	Communication.Sent <>0 
AND 	Communication.CommFormLID=4
ORDER BY Communication.SendDate desc

</CFQUERY>



<CFIF SelectCommsMade.RecordCount GT Variables.ShowRecords>
	
	<!--- define the start record --->
 	<CFIF IsDefined("FORM.ListForward.x")>
		 <CFSET displaystart = frmStart + Variables.ShowRecords>
 	<CFELSEIF IsDefined("FORM.ListBack.x")>		 
		 <CFSET displaystart = frmStart - Variables.ShowRecords>
	<CFELSEIF IsDefined("frmStart")>
	 	 <CFSET displaystart = frmStart>
	</CFIF>

</CFIF>


<cf_head>
	<cf_title>List of downloads</cf_title>
</cf_head>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TH>&nbsp;</TH>
		<TH>Title:</TH>
		<TH>Selection:</Th>
		<TH>Description:</TH>
		<TH>Date sent:</TH>
		<TH>Who by:</TH>
		<!--- if original download done by this user, then they can download it again --->
		<TH width=90>&nbsp;</TH>
	</TR>
	
	<CFOUTPUT QUERY="SelectCommsMade" STARTROW=#Variables.displaystart# MAXROWS=#Variables.ShowRecords#>
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD><A HREF="CommRecipients.cfm?ID=#CommID#">People</A></TD>
			<TD>#htmleditformat(CommTitle)#</TD>
			<TD>#htmleditformat(SelTitle)#</TD>
			<TD>#htmleditformat(Description)#</TD>
			<TD>#DateFormat(SendDate,'dd-mmm-yyyy')#</TD>
			<TD>#htmleditformat(Name)#</TD>
			<!--- if original download done by this user, then they can download it again --->
			<CFIF createdby is request.relayCurrentUser.usergroupid>
				<TD><A HREF="#replace(directory,"\","/","ALL")#/#filename#">Re-download</A></TD>
			<cfelse>
				<td>&nbsp;</td>
			</cfif>
		</TR>
	</CFOUTPUT>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<FORM NAME="ThisForm" METHOD="POST" ACTION="DownloadsMadeList.cfm">
	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
	</cfoutput>
	<TR>
		<TD>
			<CFIF Variables.displaystart GT 1>
				<INPUT TYPE="Image" NAME="ListBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" BORDER=0 ALT="">
			</CFIF>
		</TD>
		<td align="center">
			<CFIF (Variables.displaystart + Variables.ShowRecords - 1) LT SelectCommsMade.RecordCount>
				<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + Variables.ShowRecords - 1")#</CFOUTPUT>
			<CFELSE>
				<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #SelectCommsMade.RecordCount#</cFOUTPUT>
			</CFIF>
		</td>
		<td align="right">
			<CFIF (Variables.displaystart + Variables.ShowRecords - 1) LT SelectCommsMade.RecordCount>
				<INPUT TYPE="Image" NAME="ListForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" BORDER=0 ALT="Next">
			</cFIF>
		</td>
		</TR>
	
	</FORM>		
</TABLE>
<CFINCLUDE TEMPLATE="../../templates/InternalFooter.cfm">


