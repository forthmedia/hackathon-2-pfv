<!--- �Relayware. All Rights Reserved 2014 --->

<CFQUERY NAME="SelectCommsMade"
         datasource="#application.sitedatasource#">
SELECT 	c.CommID,
		SiteName,
		FirstName + ' ' + LastName AS FullName,
		Feedback,
		DateSent, 
		c.createdby
FROM 	Location as l, Communication as c, Person as p, CommDetail as cd
WHERE 	l.LocationID = p.LocationID
and		p.PersonID = cd.PersonID
and 	c.CommID = cd.CommID
and 	c.CommID =  <cf_queryparam value="#URL.ID#" CFSQLTYPE="CF_SQL_INTEGER" > 
ORDER BY SiteName, FirstName + ' ' + LastName;
</CFQUERY>

<cf_head>
	<cf_title>List of Recipients</cf_title>
</cf_head>



<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TH ALIGN="LEFT" VALIGN="top">
			Contacts in List
		</TH>
	</TR>
</table>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<TR>
	<th>Company</th>
	<th>Contact</th>
	<th>Action</th>
	<th>Date sent</th>
</TR>
<CFOUTPUT QUERY="SelectCommsMade">
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
	<TD>#htmleditformat(Sitename)#</TD>
	<TD>#htmleditformat(FullName)#</TD>
	<TD>#htmleditformat(Feedback)#</TD>
	<TD>#DateFormat(datesent,'dd-mmm-yyyy')#</TD>
</TR>
</CFOUTPUT>
</TABLE>



