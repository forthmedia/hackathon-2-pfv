<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB 2005-11-02


Report of clickThrus from Communications

I used this report to try out some ideas about dynamically generated reports - so it is a bit of a hodge podge which may or may not ever be factorised

2012-10-04	WAB		Case 430963 Remove Excel Header 
--->


<cfparam name="startrow" default=1>
<cfparam name="frmRowID" default = "">
<cfparam name="frmRowIDCols" default = "">
<cfparam name="frmCountryID" default = "">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="saveAsSelection" default = "false">

<cfif isDefined("frmDrillDownAction") and frmDrillDownAction is "selection">
	<cfset saveAsSelection = true>
</cfif>

<cfset passThruVars = structNew()>



<cfset selectionTitle = "ClickThrus from Comms">

<cfset columnDefinition = structNew()>
<cfset columnDefinition.countryDescription.heading = "Country">
<cfset columnDefinition.countryDescription.IDColumn = "CountryID">
<cfset columnDefinition.DistinctHits.heading = "People Clicking Through<BR>(Click to Save as Selection)">
<cfset columnDefinition.Comm.heading = "Communication">
<cfset columnDefinition.Comm.IDColumn = "CommID">
<cfset columnDefinition.Person.heading = "Person">
<cfset columnDefinition.Person.IDColumn = "PersonID">
<cfset columnDefinition.Link.heading = "Link">
<cfset columnDefinition.Link.IDColumn = "Link">
<cfset columnDefinition.Hits.heading = "Total Clicks">
<cfset columnDefinition.Week.heading = "Week">
<cfset columnDefinition.Day.heading = "Day">


<!--- Default Column List --->
<cfif frmCountryID is "">
	<cfparam name="frmGroupBy" default = "CountryDescription">
<cfelse>
	<cfparam name="frmGroupBy" default = "Comm">
</cfif>

<cfif isDefined("frmDrillDownAction")>
	<cfif frmColumnID is "countryDescription">
		<!--- actually set country filter --->
		<cfset x = listfind(frmRowIDCols,"CountryID",",")>
		<cfset frmCountryID = listgetAt(frmRowID,x,"|")>
		<cfset frmRowID = listdeleteat(frmRowID,x,"|")>
		<cfset frmRowIDCols = listdeleteat(frmRowIDCols,x,",")>
		<cfset frmGroupBy  = "Comm">
	<cfelseif frmColumnID is "Comm">
		<cfset frmGroupBy  = "Comm,Link,Person">
	<cfelseif frmColumnID is "Link">
		<cfset frmGroupBy  = "Link,Person">
	</cfif>
</cfif>


<cf_listunique list = #frmGroupBy# variable = "frmGroupBy">

<cfset columns = "#frmgroupBy#,DistinctHits,Hits">


<!--- if filtering by countryid, can remove from group by --->
<cfif frmCountryID is not "" and listfindnocase(frmGroupBy,"countrydescription") is not 0>
	<cfset frmGroupBy = listdeleteat(frmGroupBy,listfindnocase(frmGroupBy,"countrydescription"))>
</cfif>

	<cfset groupArray = listToArray (frmGroupBy)> 
<cfset passThruVars.frmCOuntryID = frmCOuntryID>
<cfset passThruVars.frmGroupBy = frmGroupBy>
<cfset passThruVars.frmRowID = frmRowID>
<cfset passThruVars.frmRowIDCols = frmRowIDCols>



<cfset groupColumn = "">
<cfif openasExcel is false>
	<!--- if grouping by person then don't need the distincthits column which will always be 1 --->
	<cfif listfindnocase(frmGroupBy,"person") is not 0>
		<cfset x = listfindNoCase(columns,"DistinctHits")>
		<cfset columns = listdeleteAt(columns,x)>
	</cfif>
	<!--- if filtering on country, don't need country column --->
	<cfif frmCountryID is not "" >
		<cfset x = listfindnocase(columns,"countrydescription")>
		<cfif x is not 0 >
			<cfset columns= listdeleteat(columns,x)>
		</cfif>
	</cfif>
	<cfif arraylen(groupArray) gt 1>
		<cfset groupColumn = groupArray[1]>
		<cfset x = listfindNoCase(columns,groupColumn)>
		<cfset columns = listdeleteAt(columns,x)>
	</cfif>
</cfif>





<cfset	keyColumnList="">
<cfset	keyColumnURLList="">
<cfset	keyColumnKeyList="">


<cfset	keyColumnList="distinctHits">
<cfset	keyColumnURLList="javascript:saveAsSelection('thisURLKey')">
<cfset	keyColumnKeyList="RowID">
	
<cfif frmGroupBy does not contain "person">
	<cfset	keyColumnList=listappend(keyColumnList,"CountryDescription")>
	<cfset	keyColumnURLList=listappend(keyColumnURLList,"javascript:drillDown('thisURLKey'*comma'countryDescription')")>
	<cfset	keyColumnKeyList=listappend(keyColumnKeyList, "RowID")>

	<cfset	keyColumnList=listappend(keyColumnList,"Comm")>
	<cfset	keyColumnURLList=listappend(keyColumnURLList,"javascript:drillDown('thisURLKey'*comma'comm')")>
	<cfset	keyColumnKeyList=listappend(keyColumnKeyList, "RowID")>
<!--- 	<cfset columns = listappend(columns,"CountryDrillDown")> --->

</cfif>



<cfset columnHeadings = "">
<cfloop index = "column" list = "#columns#">
	<cfif isDefined("columnDefinition.#column#.heading")>
		<cfset columnHeadings = listappend(columnHeadings,columnDefinition[column].heading)>
	<cfelse>
		<cfset columnHeadings = listappend(columnHeadings,column)>
	</cfif>
</cfloop>





<cfif isDefined("thisDateRange")>
	<cf_dateRangeRetrieve dateRange=#thisDateRange#><!--- returns startDate,EndDate,Range --->
	<cfset passThruVars.thisdaterange = thisdaterange>
<cfelse>
	<cf_DateRange  year="#year(now())#">  <!---  type="month" value="current" --->
	<cfparam name="range" default="">
</cfif>



 

<cfparam name="request.supressOrgsFromReports" default="true">

		<CFQUERY NAME="countries" datasource="#application.sitedatasource#">
		select countryid as datavalue, countrydescription as displayvalue
		from country
		where countryid in (#request.relaycurrentuser.countryLIst#)
		order by countrydescription
		</CFQUERY>
		


<cfparam name="frmrowIDCols" default="">
<cfset rowIDCols = frmrowIDCols>
<!--- add on group by cols --->
		<cfloop index="item" list="#frmGroupBy#">
			<cfif isDefined("columnDefinition.#item#.idColumn")>
				<cfset thisIDColumn = columnDefinition[item].idcolumn>
			<cfelse>	
				<cfset thisIDColumn = item>
			</cfif>
			<cfif listfindnocase (rowIDCols,thisIDColumn) is 0 >
				<cfset rowIDCols = listappend(rowIDCols,thisIDColumn)>
			</cfif>
		</cfloop>



	
		<CFQUERY NAME="GetHitsToThisPage" datasource="#application.sitedatasource#">
		<cfsavecontent	variable = "theQuery_sql">
		select 
		<cfif saveAsSelection>
			distinct PersonID
		<cfelse>

			<cfif frmGroupBy is not "">#frmGroupBy#,</cfif>

			<cfset tempRowSql = "">
			<cfif rowIDCols is not "">
				<cfloop index="item" list="#rowIDCols#">
					<cfset tempRowSql = listappend(tempRowSql,"convert(varchar(100),#item#)","|")>
				</cfloop>
				<cfset tempRowSql = replaceNoCase(tempRowSql,"|","+'|'+","ALL")>
				#preserveSingleQuotes(tempRowSql)#
			<cfelse>
				''
			</cfif> as rowID,
			count(distinct Personid) as distinctHits, 
			count(Personid) as hits,
			'save' as saveAsSelection,
			'v' as drillDown
		</cfif>	
		from
		(
		SELECT 
		year(et.created) as year,
		convert(varchar,month(et.created)) + '/' + convert(varchar,year(et.created)) as month,
		convert(varchar,datepart(wk,et.created)) + '/' + convert(varchar,year(et.created)) as week,
		convert(varchar,datepart(d,et.created)) +' ' + dateName(month,et.created)  as day,
		p.firstname + ' ' + p.lastname + ', ' + l.sitename  as Person,
		et.personid,
		l.countryid,
		c.countryDescription,
		et.commid,
		convert(varchar(50),et.commid) + ' - ' + com.title   as Comm,
		et.toloc as link
		from
			entityTracking et
				inner join 
			communication com on com.commid = et.commid	
				inner join 
			(person p 
				inner join 
			location l on l.locationid = p.locationid
				inner join 
			country c on c.countryid = l.countryid	) on p.personid = et.personid
		where et.commid is not null
			and et.Created  BETWEEN  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
			 		<cfif request.supressOrgsFromReports and (not isDefined("frmShowSuppressedOrgs") or frmShowSuppressedOrgs is "")>
						and p.OrganisationID not IN
						(SELECT     organisationid
							FROM          vOrgsTosupressFromReports)
					</cfif>
			<cfif frmCountryID is not "">
				and l.countryid  in ( <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			
		) as aView

		<cfif frmRowID is not "">
			where 

			<cfset tmpSql = "">
			<cfloop index = "x" from="1" to =#listLen(frmRowID,"|")#>
				#listgetat(frmRowIDCols,x,",")# =  <cf_queryparam value="#listgetat(frmRowID,x,"|")#" CFSQLTYPE="CF_SQL_VARCHAR" >  and
			</cfloop>

			1 =1
		</cfif>
		
		<cfif not saveAsSelection>
			<cfif frmGroupBy is not "">group by  #frmGroupBy#<cfif rowIDCols is not "">,#rowIDCols#</cfif></cfif>
			<cfif frmGroupBy is not "">	order by  <cf_queryObjectName value="#frmGroupBy#"></cfif>
		</cfif>
	</cfsavecontent>	
	<cfif not SaveAsSelection>
		#preserveSingleQuotes(theQuery_sql)#
	<cfelse>
		select 1	
	</cfif>
	
	</CFQUERY>



	<!--- work out a description from all the filters --->
	<cfset FilterDescription = "">
	<cfif frmCOuntryID is not "">
		<cfset FilterDescription = FilterDescription & "Country = #application.countryName[frmCOuntryiD]#. ">
	</cfif>
	<cfif range is not "">
		<cfset FilterDescription = FilterDescription & "#range#. ">
	</cfif>
	
	
	<cfset DrillDownFilterDescription = "">	
	<cfloop index = "x" from="1" to =#listLen(frmRowID,"|")#>
		<cfset DrillDownFilterDescription = DrillDownFilterDescription & listgetat(frmRowIDCols,x,",") & " = " & listgetat(frmRowID,x,"|") & "<BR>">
	</cfloop>

	<cfset selectionDescription = filterDescription & replaceNoCase(DrillDownFilterDescription,"<BR>",". ","ALL")>
	
	
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">	


<cfif SaveAsSelection>
	
	
	

	<!--- seemed to get upset with the url having too many odd characters in it  --->
	<cfset theQuery_sql = replaceNoCAse(theQuery_sql,chr(9)," ","ALL")>
	<cfset theQuery_sql = replaceNoCAse(theQuery_sql,chr(13) & chr(10)," ","ALL")>
	<cfset theQuery_sql = replaceNoCAse(theQuery_sql,"  "," ","ALL")>
	<cfset theQuery_sql = replaceNoCAse(theQuery_sql,"  "," ","ALL")>

	<CFLOCATION url="\selection\selectalter.cfm?frmTask=Save&frmRunInPopUP=true&frmPersonIDs=#urlencodedFormat(theQuery_sql)#&frmDescription=#selectionDescription#&frmTitle=#selectionTitle#"addToken="false">
	<CF_ABORT>

</cfif>
	
	
	

	<cf_head>
		<cf_title>Click Thrus</cf_title>


	</cf_head>
	
	

	
	


	<cfoutput>



	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">

	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Analysis of Click Thrus from Communications</TD>
	</tr>
	<TR>
		<TD>
		<cfform method="post" name="filterForm" id="dateRangeForm" >
			<table>
			<tr>
				<td>Filter by date</td><td><cf_DateRangeSelectBox BoxName="thisDateRange" SelectedRange="#range#"></td>
			</tr>
			<tr>
				<td>Filter by country</td><td><cf_displayValidValues 
							formfieldname="frmCountryID"
							validvalues="#countries#"
							nullText = "Select a Country"
							currentValue = #frmCountryid#
							>
				</td>

			</tr>

			<cfloop index="x" from="1" to = "3">
			<tr>
				<td><cfif x is 1>Categorise by<cfelse>and </cfif></td>
				<td>
				<cfif arrayLen(groupArray) lt x><cfset currentValue =""><cfelse><cfset currentValue =groupArray[x]> </cfif>
				<cf_displayValidValues 
				formfieldname="frmGroupBy"
				validvalues="Month,Week,Day,CountryDescription,Comm,Link,Person"
				nullText = ""
				currentValue = #currentvalue#
				></td>
			</tr>
			</cfloop>
			
			<cfif DrillDownFilterDescription is not "">
			<tr><td colspan=2>
			A "drillDown" filter has been applied <BR>
			#htmleditformat(DrillDownFilterDescription)#			
			</td></tr>
			</cfif>
			<tr><td colspan=2>
			<input type="checkbox" name="frmShowSuppressedOrgs" value = "1" <cfif isDefined("frmShowSuppressedOrgs") and  frmShowSuppressedOrgs is 1>checked</cfif>> Include Test and Internal Organisations in Statistics
			</td></tr>
			<tr><td colspan=2>
			<input type="submit" value="Reload">
			</td></tr>
			</table>
		</cfform>
		
		<form name="clickThroughForm" method = "post" target="PopUp">
			<cfif isDefined("thisDateRange")><CF_INPUT type="hidden" name = "thisDateRange" value = "#thisDateRange#"></cfif>
			<CF_INPUT type="hidden" name = "frmCountryid" value = "#frmCountryid#">
			<CF_INPUT type="hidden" name = "frmGroupBy" value = "#frmGroupBy#">
			<input type="hidden" name = "frmShowSuppressedOrgs" value = "<cfif isDefined("frmShowSuppressedOrgs") and  frmShowSuppressedOrgs is 1>1</cfif>">
			<input type="hidden" name = "frmDrillDownAction" value = "">
			<CF_INPUT type="hidden" name = "frmRowIDCols" value = "#RowIDCols#">
			<input type="hidden" name = "frmRowID" value = "">
			<input type="hidden" name = "frmColumnID" value = "">
												
		</form>


				<A Href="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target=@@>Open in Excel</A>		
		</TD>
	</tr>
	</cfoutput>	

	<TR>
		<TD>



<cfif 	GetHitsToThisPage.recordcount is not 0>



<!--- some stuff trying out drill downs and things --->
	<script>
	function saveAsSelection (x) {
		newWindow = window.open( '','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )
		newWindow.focus();
		form = document.clickThroughForm
		form.target = 'PopUp'
		form.frmDrillDownAction.value = 'selection'
		form.frmRowID.value = x;
		form.submit();
		form.frmDrillDownAction.value = ''
	}

	function drillDown (row,column) {
		form = document.clickThroughForm
		form.target = ''
		form.frmRowID.value = row
		form.frmColumnID.value = column
//		form.frmGroupBy.value = form.frmGroupBy.value + ',person'
		form.submit()
	}

	</script>


	
	 <cf_tablefromqueryObject
	 	queryobject = #GetHitsToThisPage#
		sortorder = ""
		startrow = #startrow#
		HidePageControls="no"
		groupByColumns = "#groupColumn#"
		showTheseColumns="#columns#" 
		columnHeadingList="#columnHeadings#"	
		useinclude=false
		openAsExcel = #openAsExcel#

		keyColumnList="#keyColumnList#"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="#keyColumnKeyList#"
		
	 >

<cfelse>
	<center>There have been no hits to this page</center>
	
</cfif>
	 
	
</TD>
</TR>
</TABLE>
