<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	Number of communications sent to each country 

	2005-08-12 GCC A_655 - reworked to report correct data 
	2005-10-24	WAB		- reworked to get report on country of recipient not sender.  
						- changed drilldown to go to correct commcheck.cfm and added a form to pass correct variables
	2005-12-20	WAB		- converted to use context menus
	2008-03-25	WAB		- mod so that context menus work with FF
						- removed a spurious comma in the report definition
						- added context menu on organisation	
	2013-03-07 Case 433427.  Spurious space before parameter name in parameterlist causing tfqo to fall over 				
						
--->



<cfparam name="range" default="">
<cfif isDefined("form.thisDateRange")>
	<cfset thisdateRange = form.thisdateRange>
	<cf_dateRangeRetrieve dateRange = #thisdateRange#>
<cfelseif isDefined("url.thisDateRange")>
	<cfset thisdateRange = url.thisdateRange>
	<cf_dateRangeRetrieve dateRange = #thisdateRange#>
<cfelse>
	<cf_DateRange type="month" value="current" year="#year(now())#">
	<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">
</cfif>

	<cfset reports = structNew()>

	<cfset thisReport = "CommsByCountry">
	<cfset Reports[thisReport] = structNew()>
	<cfset Reports[thisReport].Name = "Communications By Country">
	<cfset Reports[thisReport].ID = thisReport>
	<cfset Reports[thisReport].type = "table">
	<!--- NJH 2012/11/14 Case 431805 changed ##readPercent## to simply readPercent --->
	<cfset Reports[thisReport].ParameterList = "ColumnHeadingList='Country,No. Comms,Successful Sent,Read,Failed,Read %,No. People,No. Organisations',showTheseColumns='CountryDescription,NumberOfComms,SuccessfulSent,Received,Failed,readPercent,People,Organisations',useinclude=false,keycolumnlist='People,Organisations,Countrydescription,NumberofComms',keycolumnkeylist='thecountryID,thecountryID,thecountryid,thecountryid',keyColumnURLList='javascript:commCheckdrillDown(thisurlkey),javascript:commCheckdrillDown(thisurlkey),javascript:commCheckdrillDown(thisurlkey),javascript:commCheckdrillDown(thisurlkey)',keyColumnContextMenuList='PersonMenu,OrganisationMenu, , ',keyColumnContextMenuIDExpressionList='countryDescription&""|""&emailOrFax,countryDescription&""|""&emailOrFax, , ', rowIDCols='countryDescription|emailOrfax',numberformat='NumberOfComms,successfulsent,received,failed,People,Organisations'">
	<cfif isdefined("thisDateRange")>
		<cfset Reports[thisReport].passThroughVariablesStructure = structNew() >
		<cfset Reports[thisReport].passThroughVariablesStructure["thisDateRange"] = thisDateRange >
	</cfif>	

<!--- note, statuses between 0 and -19 are not sent --->
	<cfset Reports[thisReport].query = "
				SELECT 	CountryDescription,
						c.CountryID as thecountryid, 
						case when cd.commTypeId = 2 then 'email' when cd.commTypeId = 3 then 'fax' end  as EmailorFax,
						Count(distinct com.CommID) AS NumberofComms,
						count (distinct cd.personid) as People,
						count (distinct p.organisationid) as organisations,
						count( case when statusGroup = 'notReceived' then 1 else null end) as failed,  
						count( case when statusGroup in ('sent','received')  then 1 else null end) as successfulsent,
						count( case when statusGroup in ('received') then 1 else null end) as received,
					--	count (case when statusGroup not in ('notSent','') then 1 else null end) as NumberOfItemsSent,
						case when (count (case when statusGroup not in ('notSent','') then 1 else null end)) <>0 then 100 * count( case when statusGroup in ('received') then 1 else null end) / count (case when statusGroup not in ('notSent','') then 1 else null end) else 0 end as ReadPercent
						
				FROM 	(Communication as com
							inner join 
						vcommdetailMostSignificantStatus cd on com.commid = cd.commid	and cd.test = 0 and cd.internal = 0
							inner join 
						vpeople p on p.personid = cd.personid 
							inner join 
						Country as c  on c.countryid = p.countryid	)
				WHERE 	
						p.OrganisationID not in (select organisationid from vOrgsToSupressFromReports)
						and		(select min(sentdate) from commselection cs where cs.commid = com.commid and commselectlid = 0 and sent = 1) >#createODBCdate(StartDate)#
						and		(select min(sentdate) from commselection cs where cs.commid = com.commid and commselectlid = 0 and sent = 1) <=#createODBCdate(EndDate)#
						and com.commformlid in (2,3,5,6,7)
						and cd.commTypeID in (2,3)
						and commTypeLid <> 27
						and (select distinct 1 from commselection cs where cs.commid = com.commid and cs.commselectlid = 0 and cs.sent = 1) = 1
				GROUP BY 		commTypeId,CountryDescription ,	c.CountryID
				ORDER BY 		commTypeId,CountryDescription
				">


<cfset nReport = CreateObject("component","relay.report.reporting")>

<cfset drilldowndone = nReport.doMultipleReportDrillDown(reports)>
<cfif drilldowndone is true><CF_ABORT></cfif>
				
				

<cf_head>
    <cf_title>Communications Analysis</cf_title>
</cf_head>

<cf_body onclick="fnDetermineForLeftClick(event);"  >
<CFOUTPUT>

<!--- this form is for drilling down to the list of communications (doesn't use context menus) --->
<!--- <form name = "CommCheckDrilldownForm" method = "post" action = "/communicate/commCheck.cfm"> 
	<input type="hidden" name = "frmSearchBy" value="country.countryid">
	<input type="hidden" name = "selectfieldtype" value="">
	<input type="hidden" name = "frmShowOwn" value="0">
	<input type="hidden" name = "frmShowTest" value="0">
	<input type="hidden" name = "frmShowStatus" value="sent">	
	<cfif isdefined("thisDateRange")>
		<CF_INPUT type="hidden" name = "thisDateRange" value="#thisDateRange#">	
	</cfif>	
	
</form>

<script>
	function commCheckdrillDown(countryID) {
		form = document.CommCheckDrilldownForm
		form.selectfieldtype.value = countryID
		form.submit()
	}
</script>
 --->



<!--- <form name = "CommCheckDrilldownForm" method = "get" action = "/report/communications/commCountryStatsByComm.cfm" target="drillDownIFrame">
	<input type="hidden" name = "frmCountryID" value="country.countryid">
	<cfif isdefined("thisDateRange")>
		<CF_INPUT type="hidden" name = "thisDateRange" value="#thisDateRange#">	
	</cfif>	
	
</form> --->

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

<script>
	function commCheckdrillDown(countryID) {
		//form = document.CommCheckDrilldownForm
		//form.frmCountryID.value = countryID
			//showReportChildDiv (1)
		//form.submit()
		url = '/report/communications/commCountryStatsByComm.cfm?frmCountryID='+countryID;
		<cfif isDefined("thisDateRange")>
			url = url+'&thisDateRange=#urlEncodedFormat(thisDateRange)#'
		</cfif>
		openNewTab('CommunicationDrillDown','Communication Statistics By Country Drill Down',url,{iconClass:'communicate'});
	}

	function showReportChildDiv  (show)
	{
	
		var  parentZ = document.getElementById('reportMain')  ;
		var  childZ = document.getElementById('reportChild') ;

		
		if (show) {
			parentZ.style.zIndex =  1 ;
			childZ.style.zIndex = 2 ;
			childZ.style.display = 'block';
		} else {
			parentZ.style.zIndex =  2 ;
			childZ.style.zIndex = 1 ;
			childZ.style.display = 'none';
		}


	
	}
	function submitToExcel()
	{
	dateRangeForm.openAsExcel.value = true;
	dateRangeForm.submit();
	dateRangeForm.openAsExcel.value = false;
	}
</script>

<div id="reportWrapper" 
	style="absolute;
		left: 0;
		top: 0; overflow:hidden">


	<div id ="reportMain" style="z-index: 2;
		position: absolute;
		left: 0;
		top: 0;
		background:##FFFFFF;
		width:100%;height:100%;
		white-space: normal;
		"
		onclick = "javascript:showReportChildDiv (0)"
		
		>

		
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD>
			<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
			<tr><td ALIGN="LEFT" VALIGN="top" CLASS="Submenu">Global Email Statistics by Country</td><td ALIGN="right" VALIGN="top" class="Submenu"><A href="##" onClick="submitToExcel();" class="Submenu">Excel</A></td></tr>
			</table>
		</TD>
	</TR>
	
	<tr>
		<td>
			Between #dateformat(StartDate,'dd-mmm-yy')# and #dateformat(EndDate,'dd-mmm-yy')#
		</td>
	</tr>
	<tr>
		<td><cfform method="post" name="dateRangeForm" id="dateRangeForm">
		<cf_DateRangeSelectBox BoxName="thisDateRange" SelectedRange="#range#">
		<input type="submit" value="Search">
		<input type="hidden" name="openAsExcel" value="false">
	</cfform>
		</td>
	</tr>
	</table>
	</cfoutput>
	
	
 
					<cfoutput>
					<xml id="contextDef">
						<xmldata>
							<contextmenu id="PersonMenu">
								<item id="Selection_Save" value="Save as Selection" ></item>
								<item id="Selection_Add" value="Add to Selection" ></item>
								<item id="Drilldown_Person" value="View People" ></item>
								<item id="Drilldown_Organisation" value="View Organisations" ></item>
							</contextmenu>
							<contextmenu id="OrganisationMenu">
								<item id="Drilldown_Organisation" value="View Organisations" ></item>
							</contextmenu>

						</xmldata>
					</xml>		
				
					<form method="post" name = "reportDrillDownForm_#reports.CommsByCountry.id#" target = "" style="display: inline">
					<input type="hidden" name="frmReportID" value ="">
					<input type="hidden" name="frmRowIDCols" value ="CountryDescription|EmailOrFax">   <!--- these values are hacked in  - need to be same as what is passed into tableFromQueryObject--->
					<input type="hidden" name="frmRowID" value ="">
					<input type="hidden" name="frmColumnID" value ="">
					<input type="hidden" name="frmDrillDownAction" value ="">
					<cfif isdefined("form.thisDateRange")>
						<CF_INPUT type="hidden" name = "thisDateRange" value="#form.thisDateRange#">	
					</cfif>	
					</form>
	
					</cfoutput>
	

	<div status="false" onclick="javascript:fnDetermine(event);" onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);" id="oContextMenu" class="menu"></div>
				<CFHTMLHEAD TEXT='<LINK REL="stylesheet" HREF="/Styles/contextMenuStyles.css">'>	
				<cf_includeJavascriptOnce template = "/javascript/contextMenu.js">
				<cf_includeJavascriptOnce template = "/javascript/contextMenu_Report.js">	
				<cf_includeJavascriptOnce template = "/javascript/reportdrillDown.js">	
	
				<SCRIPT>
					fnInit()
				//	document.body.oncontextmenu = 'fnDetermine'
				//	document.body.onclick="fnDetermineForLeftClick();"				
				</SCRIPT>
	
	

	 <!--- 2006-08-29 GCC - Crowbaring in an excel link for the top level report--->
<cfparam name="form.openAsExcel" type="boolean" default="false">
<cfif form.openAsExcel>
	<cfcontent type="application/msexcel">
	<cfheader name="content-Disposition" value="filename=commStatByCountry.xls">
	<cfinclude template="/relay/templates/excelHeaderInclude.cfm">
</cfif>

		<cfoutput>#nReport.runReport(reportDefinition = reports["CommsByCountry"])#			</cfoutput>

	<!--- <cf_tablefromqueryobject queryObject = #communicationsByCountry# useinclude = false keycolumnlist='people' keycolumnkeylist='people' keyColumnURLList='xx' keyColumnContextMenuList='Menu1'> --->

	
	<table>
		<tr>
			<td>Notes</td> 
		</tr>
		<tr>
			<td valign=top>Successfully Sent</td>
			<td>Total of all items which have not bouncedback
				<BR> may include emails which have failed to reach destination
			</td>
		</tr>
		<tr>
			<td>Read</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>Failed</td>
			<td>Items which have bounced back
				excludes items not sent because of 'unsubscribed' or 'excluded'
					</td>
		</tr>
	
		
		
	</table>
	
	<CFINCLUDE TEMPLATE="../../templates/InternalFooter.cfm">

	</div>
	
	
	<div id ="reportChild" style="z-index: 1;
		position: relative;
		left: 30;
		background:#c3c3c3;
		top: 30;width:100%;height:100%"
		>
	
	
		<iframe name="drillDownIFrame" width = "100%" height = "100%" 
		style="background:#c3c3c3;"
		>
	
	
		</iframe>
	
	
	</div>

		
	
</div>


