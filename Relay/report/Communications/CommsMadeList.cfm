<!--- �Relayware. All Rights Reserved 2014 --->

<CFQUERY NAME="SelectCommsMade"
         datasource="#application.sitedatasource#">
SELECT Communication.Title AS Field1, 
	LookupList.ItemText AS field2, 
	Selection.Title AS field3, 
	Communication.Description AS field4, 
	Communication.SendDate AS field5, 
	UserGroup.Name AS field6, 
	Communication.CommID AS Field7
FROM Selection INNER JOIN ((LookupList RIGHT JOIN(Communication INNER
	JOIN (Person INNER JOIN UserGroup
	ON Person.PersonID = UserGroup.PersonID)
	ON Communication.CreatedBy = UserGroup.UserGroupID)
	ON LookupList.LookupID = Communication.CommFormLID)
	INNER JOIN CommSelection
	ON Communication.CommID = CommSelection.CommID)
	ON Selection.SelectionID = CommSelection.SelectionID
WHERE Communication.Sent=-1 
AND Communication.CommTypeLID <>50
AND Communication.CommFormLID<>4
ORDER BY Communication.SendDate desc;


</CFQUERY>

<cf_head>
	<cf_title>List of communications sent</cf_title>
</cf_head>


<FONT size="+2"><B>List communications sent in date order</B></FONT>

<P>

<CENTER>
<TABLE>
<TR bgcolor="cccccc">
	<TD>&nbsp;</TD>
	<TD>Title:</TD>
	<TD>Type:</TD>
	<TD>Selection:</TD>
	<TD>Description:</TD>
	<TD>Date sent:</TD>
	<TD>Who by:</TD>
</TR>
<CFOUTPUT QUERY="SelectCommsMade">
<TR bgcolor="#IIf(CurrentRow Mod 2, DE('ffffff'), DE('ffffcf'))#">
	<TD><A HREF="CommRecipients.cfm?ID=#Field7#">Recipients</A></TD>
	<TD>#htmleditformat(Field1)#</TD>
	<TD>#htmleditformat(Field2)#</TD>
	<TD>#htmleditformat(Field3)#</TD>
	<TD>#htmleditformat(Field4)#</TD>
	<TD>#DateFormat(Field5,'dd-mmm-yyyy')#</TD>
	<TD>#htmleditformat(Field6)#</TD>
</TR>
</CFOUTPUT>
</TABLE>
</CENTER>



