<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Displays a table of all failed communications for a particular commID --->
<!--- Written by William Bibby 1998-07-23 --->

<!--- Relies on all the status of all communications being updated correctly by the fax and email processing  ---->


<!--- <CFINCLUDE template=wabcomm resultsquery.cfm> --->


<!---  Combine everything together!--->
<CFQUERY name="CommunicationFailures" datasource="#application.sitedatasource#">

SELECT Location.LocationID, 
Location.SiteName, 
Location.Fax, 
Location.Telephone, 
Person.FirstName, 
Person.LastName, 
Person.PersonID, 
Person.OfficePhone, 
Person.Email, 
E.commStatusID AS emailreceived, 
F.commStatusID AS faxreceived, 
E.CommFormLID, 
E_lookup.Text as emailfeedback, 
E.Feedback, 
F_lookup.Text as faxfeedback, 
F.Feedback
FROM Location INNER JOIN (((((wabtemptable AS T INNER JOIN Person ON T.PersonID = Person.PersonID) INNER JOIN CommDetail AS E ON T.EmailID = E.CommDetailID) INNER JOIN CommDetail AS F ON T.FaxID = F.CommDetailID) LEFT JOIN wabcommstatuslookup AS E_lookup ON E.commStatusID = E_lookup.status) LEFT JOIN wabcommstatuslookup AS F_lookup ON F.commStatusID = F_lookup.status) ON Location.LocationID = Person.LocationID
WHERE (((E.commStatusID) Is Null Or (E.commStatusID)<=0) 
AND ((F.commStatusID) Is Null Or (F.commStatusID)<=0))



</CFQUERY>




<CFQUERY name="CountofPeople" datasource="#application.sitedatasource#">

SELECT CommDetail.PersonID
FROM CommDetail
GROUP BY CommDetail.CommID, CommDetail.PersonID
HAVING (((CommDetail.CommID) =  <cf_queryparam value="#URL.ID#" CFSQLTYPE="CF_SQL_INTEGER" > ))

</CFQUERY>



<cf_head>
   <cf_title>Failed Communications</cf_title>
</cf_head>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR class="SubMenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="SubMenu">Failed Communications for Comm <CFOUTPUT>#htmleditformat(URL.ID)#</CFOUTPUT></TD>
		<td align="right" class="SubMenu"><a href="javascript:history.back()" class="SubMenu">Back</a></td>
	</TR>
</TABLE>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD COLSPAN="5">Number of People in Communication   <CFOUTPUT>#CountOfPeople.recordcount#  </CFOUTPUT>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN="5">Number of Failed Communications   <CFOUTPUT>#CommunicationFailures.recordcount#  </CFOUTPUT>
		</TD>
	</TR>

	<TR>
		<Th>IDs</Th>
		<Th>Recipient</Th>
		<Th>Fax/Email/Phone</Th>
		<Th>Result</Th>
	</TR>





<CFOUTPUT query="CommunicationFailures">
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>

	
	<TD>Pers: #htmleditformat(PersonID)#  <BR>
 	          Loc: #htmleditformat(LocationID)#  </TD>
	<TD>#htmleditformat(FirstName)# #htmleditformat(LastName)#<BR>
	         #htmleditformat(SiteName)#</TD>

	<TD>Fax: #htmleditformat(Fax)#<BR>
	         Email: #htmleditformat(Email)#<BR>
			 Phone: #htmleditformat(OfficePhone)#</TD>
	<TD>#htmleditformat(FaxFeedback)# <BR>
	          #htmleditformat(EmailFeedback)#</TD>


</TR>
</CFOUTPUT>

</TABLE>






