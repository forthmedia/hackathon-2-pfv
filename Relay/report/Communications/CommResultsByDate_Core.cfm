<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		CommResultsByDate.cfm
Author:			Simon WJ
Date created:

	Objective - this lists the contact history for a given date

	Syntax	  -	It defaults to the current date but can be filtered by date and person

	Parameters -none

	Return Codes - none

Amendment History:

Date 		Initials 	What was changed

2001-09-28	SWJ			Created by SWJ
2002-01-31	SWJ			Modified layout
2002-07-08	SWJ			Modified it so that it will also filter by commStatusID
2007-09-13	SSS			added extra date filters
						The display of the report is now tablefromqueryobject
2008-07-17	NJH			Bug Fix T-10 Issue 804 added 'with (noLock)' to the queries, and opened org/person detail in new window.
2009-01-23	NYB			8.1 Release: Item 115 / Bug 1664 - Made alot of changes to file to various errors:
							- dates in the first drop down were being display alphanumerically - not by date
							- page links weren't working - they would just clear the results table
							- the number returned stated in the drop downs (against each date option) were different to the number actually being brought back
2010-10-13	NAS			LID4307: Reports - Contact History by date/User report template unavailable
31-JAN-2011 MS 			LID:5453 - adding Campaign column to the Contact History report if display campaign switch is set to true
Enhancement still to do:

1.  Get date range search working
2.  Add filtering by person

--->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="frmUserGroupID" type="numeric" default="0">
<cfparam name="frmCommStatusID" type="numeric" default="0">
<cfparam name="frmCommStatusID" type="numeric" default="0">


<cfparam name="startrow" default="1">
<cfparam name="sortOrder" default="DateSent DESC">  <!--- NYB 2009-01-23 8.1 Release Bug 115 -> changed from "title" --->
<cfparam name="numRowsPerPage" default="50">

<!--- This can also be set up in the relayini file to Customize the number of days it will run for --->
<CFPARAM NAME="request.CommResultdays" DEFAULT="90">

<cfsetting requesttimeout="600">


<!--- NYB 2009-01-23 8.1 Release Bug 115 -> added --->
<CFIF NOT isdefined("form.frmReportDate")>
	<CFSET form.frmReportDate = "none">
</CFIF>
<CFIF NOT isdefined("form.frmReportMonthDate")>
	<CFSET form.frmReportMonthDate = "none">
</CFIF>
<CFIF NOT isdefined("form.frmUserGroupID")>
	<CFSET form.frmUserGroupID = 0>
</CFIF>
<CFIF NOT isdefined("form.frmCommStatusID")>
	<CFSET form.frmCommStatusID = 0>
</CFIF>
<CFIF frmReportDate eq "none" and frmReportMonthDate eq "none" and frmUserGroupID eq 0 and frmCommStatusID eq 0>
	<CFSET showResults = false>
<CFELSE>
	<CFSET showResults = true>
</CFIF>


<cfscript>
   frmVars = StructNew();
   StructInsert(frmVars, "frmReportDate", #form.frmReportDate#);
   StructInsert(frmVars, "frmReportMonthDate", #form.frmReportMonthDate#);
   StructInsert(frmVars, "frmUserGroupID", #form.frmUserGroupID#);
   StructInsert(frmVars, "frmCommStatusID", #form.frmCommStatusID#);
</cfscript>
<!--- <- NYB 2009-01-23 --->


<!--- If this is the first time you come into this report
then the Form.DateOfMods value will not be set. --->
<!---
<cfif not isdefined("form.frmReportDate")>
	<cfset querydate = createodbcdate(now())>
<cfelse>
	<cfset querydate = form.frmreportdate>
</cfif>--->

<cfquery name="getDates" datasource="#application.siteDataSource#">
select
		<!--- NYB 2009-01-23 8.1 Release Bug 115 - replaced -> ---
		convert(varchar(4),year(DateSent))+'-'+convert(varchar(2),month(DateSent))+'-'+convert(varchar(2),day(DateSent)) as datesSent,
		!--- with: --->
		convert(datetime,convert(varchar(4),year(DateSent))+'-'+convert(varchar(2),month(DateSent))+'-'+convert(varchar(2),day(DateSent))) as datesSent,
		<!--- <- NYB 2009-01-23 * --->
		count(*) as recs
		from commdetail with (noLock)
		<!--- NYB 2009-01-23 8.1 Release Bug 115 - added -> --->
		inner join commDetailStatus cds with (noLock) on commDetail.commStatusID=cds.commStatusID
		<!--- <- NYB 2009-01-23 --->
		where dateSent > DATEADD(day, -#request.CommResultdays#, getDate())
		<!--- NYB 2009-01-23 8.1 Release Bug 115 - replaced -> ---
		group by convert(varchar(4),year(dateSent))+'-'+convert(varchar(2),month(dateSent))+'-'+convert(varchar(2),day(dateSent))
		order by convert(varchar(4),year(dateSent))+'-'+convert(varchar(2),month(dateSent))+'-'+convert(varchar(2),day(dateSent)) desc
		!--- with: --->
		group by convert(datetime,convert(varchar(4),year(DateSent))+'-'+convert(varchar(2),month(DateSent))+'-'+convert(varchar(2),day(DateSent)))
		order by convert(datetime,convert(varchar(4),year(DateSent))+'-'+convert(varchar(2),month(DateSent))+'-'+convert(varchar(2),day(DateSent))) desc
		<!--- <- NYB 2009-01-23 * --->
		<!--- * NYB 2009-01-23 - to order the list by date - not alphanumerically as it was doing --->
</cfquery>

<cfquery name="getMonthDates" datasource="#application.siteDataSource#">
select convert(varchar(4),year(DateSent))+'-'+convert(varchar(2),month(DateSent)) as datesSent,
		count(*) as recs
		from commdetail with (noLock)
		<!--- NYB 2009-01-23 8.1 Release Bug 115 - added -> --->
		inner join commDetailStatus cds with (noLock) on commDetail.commStatusID=cds.commStatusID
		<!--- <- NYB 2009-01-23 --->
		where dateSent > DATEADD(day, -#request.CommResultdays#, getDate())
		group by convert(varchar(4),year(dateSent))+'-'+convert(varchar(2),month(dateSent))
		order by convert(varchar(4),year(dateSent))+'-'+convert(varchar(2),month(dateSent)) desc
</cfquery>

<cfquery name="getMaxDate" dbtype="query">
	select max(datesSent)as oldestDate from getDates
</cfquery>

<cfif getdates.recordcount eq 0>
	<!--- default to 90 days ago --->
	<cfset oldestdate = createodbcdate(dateadd("d",-#request.CommResultdays#,now()))>
<cfelse>
	<cfset oldestdate = createodbcdate(getmaxdate.oldestdate)>
</cfif>

<!--- hacked in a varible called editpersonkey that enabled me to enter multiple url params that needed to be sent to a javascript function if there is a better way to do this please inform me. --->
<!--- Query returning search results --->
<!--- NYB 2009-01-23 8.1 Release Bug 115 -> replaced: ---
<cfif isdefined("form.go")>
!--- with: --->
<cfif showResults>

	<!--- Get the set of group radio/checkbox flags for the commdetail entity  --->
	<cfquery name="getGroupFlagsQuery" datasource="#application.siteDataSource#">
		SELECT
			flaggroupID
		FROM
			[FlagGroup]
		WHERE
			[EntityTypeID] =  <cf_queryparam value="#application.entityTypeID["commDetail"]#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY
			[OrderingIndex]
	</cfquery>
	<cfset flagGroupArray = application.com.flag.getJoinInfoForListOfFlagGroups(valueList(getGroupFlagsQuery.flaggroupID))>

<!--- <- NYB 2009-01-23 --->
	<cfquery name="GetContactHistoryRecords" datasource="#application.siteDataSource#" timeout="360">
	SELECT p.FirstName + ' ' + p.LastName AS PersonName, c.DateSent, c.CommDetailID,
		 c.Feedback, c.LocationID, c.LastUpdatedBy,
		 case when camp.campaignName is null then 'Phr_Campaign_NoCampaign' else camp.campaignName end as Campaign,
		 'Detail' as Detail, o.OrganisationName, p.OrganisationID,
	     (SELECT name FROM usergroup
	      	WHERE usergroupid = c.lastupdatedby) AS agent, c.PersonID, CommDetailStatus.Name AS callResult, cast(p.organisationid as varchar(255)) + ',' + cast(p.personID as varchar(255)) as editPersonKey
		<!--- AWJR 2011-04-01 P-LEX051 Added in profiles for the commdetail.  --->
		<cfloop from="1" to="#arrayLen(flagGroupArray)#" index="arrIdx">
			,#flagGroupArray[arrIdx].SELECTFIELD# AS #flagGroupArray[arrIdx].alias#
		</cfloop>
		FROM CommDetailStatus with (noLock) INNER JOIN
	        Person p with (noLock) INNER JOIN
	        commdetail c with (noLock) ON p.PersonID = c.PersonID ON
	        CommDetailStatus.CommStatusID = c.CommStatusID RIGHT OUTER JOIN
	        organisation o with (noLock) ON p.OrganisationID = o.OrganisationID
	        left outer join campaign camp on c.CommCampaignID = camp.campaignid
		<!--- AWJR 2011-04-01 P-LEX051 Added in profiles for the commdetail.  --->
	        <cfloop from="1" to="#arrayLen(flagGroupArray)#" index="arrIdx">
			#flagGroupArray[arrIdx].JOIN#
		</cfloop>
		where dateSent > DATEADD(day, -#request.CommResultdays#, getDate())
			<cfif isdefined("form.frmreportdate") and isdate(form.frmreportdate)>
				AND c.DateSent between #form.frmreportdate# and #dateadd("d",form.frmreportdate,1)#
			</cfif>
			<cfif isdefined("frmReportMonthDate") and frmReportMonthDate NEQ "none">
				and DATEPART(yyyy, c.DateSent) + '-' + DATEPART(m, c.DateSent) = DATEPART(yyyy,#frmReportMonthDate#) + '-' + DATEPART(m, #frmReportMonthDate#)
			</cfif>
			<cfif isdefined("frmUserGroupID") and frmusergroupid neq 0>
				AND c.lastupdatedby =  <cf_queryparam value="#frmUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif isdefined("frmCommStatusID") and frmcommstatusid neq 0>
				AND c.CommStatusID =  <cf_queryparam value="#frmCommStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		ORDER BY  <cf_queryObjectName value="#sortOrder#">   <!--- NYB 2009-01-23 8.1 Release Bug 115 -> ", DateSent DESC" --->
	</cfquery>
</cfif>
<cfquery name="getCommResults" datasource="#application.siteDataSource#">
	SELECT CommDetailStatus.Name AS callResult, count(*), CommDetailStatus.CommStatusID
	FROM CommDetailStatus with (noLock)INNER JOIN commdetail with (noLock)
		 ON CommDetailStatus.CommStatusID = commdetail.CommStatusID
	where dateSent > DATEADD(day, -#request.CommResultdays#, getDate())
	group by CommDetailStatus.Name, CommDetailStatus.CommStatusID
	order by CommDetailStatus.Name
</cfquery>

<cfquery name="getAgents" datasource="#application.siteDataSource#">
select distinct ug.userGroupID, ug.name as userGroupName
	from userGroup ug with (noLock) inner join commdetail cd with (noLock)
	<!--- NYB 2009-01-23 8.1 Release Bug 115 - added -> --->
	inner join commDetailStatus cds with (noLock) on cd.commStatusID=cds.commStatusID
	<!--- <- NYB 2009-01-23 --->
ON ug.userGroupID=cd.lastUpdatedBy
WHERE dateSent > DATEADD(day, -#request.CommResultdays#, getDate())
<!--- 2010-10-13	NAS			LID4307: Reports - Contact History by date/User report template unavailable --->
ORDER by userGroupName
</cfquery>


<cf_head>
    <cf_title>CommHistory - Search Result</cf_title>

<SCRIPT type="text/javascript">
<!--

	function doForm(var1,var2) {
		var form = document.mainForm;
		form.frmCurrentEntityID.value = var1;
		form.frmentityType.value = var2;
		form.submit();
	}


//-->
</script>
<SCRIPT type="text/javascript" SRC="../../javascript/openWin.js"></SCRIPT>
</cf_head>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<th align="LEFT" valign="TOP">Contact History Records</th>
	</tr>
	<tr>
		<td><br>
		<cfoutput><form action="/report/communications/commResultsByDate.cfm" method="post"></cfoutput>
			<select name="frmReportDate">
			<cfoutput><option value="none">Choose date from the last #htmleditformat(request.CommResultdays)# days</cfoutput>
				<cfoutput query="getDates">
					<option value="#CreateODBCDate(datesSent)#" <cfif isDefined("frmReportDate") and frmReportDate eq CreateODBCDate(datesSent)> selected</cfif>>#DateFormat(CreateODBCDate(datesSent))# (#htmleditformat(recs)# contacts added)
				</cfoutput>
			</select>

			<select name="frmReportMonthDate">
			<cfoutput><option value="none">Choose Month from the last #htmleditformat(request.CommResultdays)# days</cfoutput>
				<cfoutput query="getMonthDates">
					<option value="#CreateODBCDate(datesSent)#" <cfif isDefined("frmReportMonthDate") and frmReportMonthDate eq CreateODBCDate(datesSent)> selected</cfif>>#htmleditformat(datesSent)# (#htmleditformat(recs)# contacts added)
				</cfoutput>
			</select>

			<select name="frmUserGroupID">
			<option value="0">Filter by Contacted By
				<cfoutput query="getAgents">
					<option value="#userGroupID#" <cfif frmusergroupid eq usergroupid> selected</cfif>>#htmleditformat(userGroupName)#
				</cfoutput>
			</select>

			<select name="frmCommStatusID">
			<option value="0">Filter by Outcome
				<cfoutput query="getCommResults">
					<option value="#CommStatusID#" <cfif isDefined("frmCommStatusID") and frmCommStatusID eq CommStatusID> selected</cfif>>#htmleditformat(callResult)#
				</cfoutput>
			</select>

			<input type="submit" name="go" value="Go">
		</form>
		</td>
	</tr>

</table>


<!--- <cfif isdefined("form.go")> --->
<!---
<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="White" class="withBorder">
	<cfif #getcontacthistoryrecords.recordcount# gt 0>

		<tr><td colspan="7">&nbsp;</td></tr>
		<tr>
			<th>&nbsp;</th>
		    <th>Organisation</th>
			<th>Who with</th>
			<th>Contacted by</th>
		    <th>Feedback/Notes</th>
		    <th>Contact date</th>
		    <th>Result</th>
		</tr>
		<cfoutput query="GetContactHistoryRecords">
			<tr<cfif currentrow mod 2 is not 0> class="oddrow"<cfelse> class="evenRow"</cfif>>
				<td valign="top"><a href="../../Phoning/CommHistory_Detail.cfm?CDID=#URLEncodedFormat(CommDetailID)#" class="smallLink">detail</a></td>
				<td valign="top"><a href="javaScript:editOrg('#organisationID#')"><cfif trim(organisationname) neq "">#OrganisationName#<cfelse>&nbsp;</cfif></a></td>
				<td valign="top"><a href="Javascript:editPerson(#organisationID#,#personid#);"><cfif trim(personname) neq "">#PersonName#<cfelse>&nbsp;</cfif></a></td>
				<td valign="top"><cfif trim(agent) neq "">#agent#<cfelse>&nbsp;</cfif></td>
				<td valign="top">#left(Feedback,80)# <nobr><cfif mid(feedback, 80, 50) neq ""><a href="../../Phoning/CommHistory_Detail.cfm?CDID=#URLEncodedFormat(CommDetailID)#" class="smallLink"> ... more</a></cfif></td>
				<td valign="top"><cfif trim(datesent) neq "">#dateFormat(DateSent,"dd-mmm-yy")# #timeFormat(DateSent,"HH:MM")#<cfelse>&nbsp;</cfif></td>
				<td valign="top"><cfif trim(callresult) neq "">#CallResult#<cfelse>&nbsp;</cfif></td>
			</tr>
		</cfoutput>
			<tr><tdcolspan="5">&nbsp;</td></tr>
			<cfif #getcontacthistoryrecords.recordcount# gt 5>
			</cfif>
--->
<!--- NYB 2009-01-23 8.1 Release Bug 115 -> removed ---
<cfif isdefined("form.go")>
!--- replaced with: --->
<cfif showResults>


<!--- START 31-JAN-2011 MS LID:5453 - adding Campaign column to the Contact History report if display campaign switch is set to true --->
<cfset showTheseColumns_list = "Detail,OrganisationName,PersonName,agent,Feedback,DateSent,callresult">
<CFIF application.com.settings.getSetting("campaigns.displayCampaigns")>
	<CFSET showTheseColumns_list = ListInsertAt(showTheseColumns_list, "5", "Campaign")>
</CFIF>
<!--- END 31-JAN-2011 MS LID:5453 - adding Campaign column to the Contact History report --->

<cfset columnHeadingList = showTheseColumns_list>
<!--- AWJR 2011-04-01 P-LEX051 Added in profiles for the commdetail.  --->
<cfloop from="1" to="#arrayLen(flagGroupArray)#" index="arrIdx">
	<cfset showTheseColumns_list =  listappend(showTheseColumns_list,flagGroupArray[arrIdx].ALIAS)>
	<cfset columnHeadingList =  listappend(columnHeadingList,flagGroupArray[arrIdx].name)>
</cfloop>

<!--- <- NYB 2009-01-23 --->
<cfif #getcontacthistoryrecords.recordcount# gt 0>
	<CF_tableFromQueryObject
	queryObject="#GetContactHistoryRecords#"
	queryName="GetContactHistoryRecords"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startrow="#startrow#"
	dateFormat="DateSent"
	showTheseColumns="#showTheseColumns_list#"
	columnHeadingList="#columnHeadingList#"
	keyColumnList="Detail,OrganisationName,PersonName,Feedback"
	<!--- NJH 2008-07-17 Bug Fix T-10 Issue 804 --->
	<!--- keyColumnURLList="/Phoning/CommHistory_Detail.cfm?CDID=,javascript:editOrg(thisURLkey);,javascript:editPerson(thisURLkey);,/Phoning/CommHistory_Detail.cfm?CDID=" --->
	keyColumnURLList="/Phoning/CommHistory_Detail.cfm?CDID=,/data/dataFrame.cfm?frmsrchOrgID=,/data/dataFrame.cfm?frmsrchPersonID=,/Phoning/CommHistory_Detail.cfm?CDID="
	keyColumnKeyList="URLEncodedFormat(CommDetailID),OrganisationID,PersonID,URLEncodedFormat(CommDetailID)"
	keyColumnOpenInWindowList="no,yes,yes,no"
		<!--- NYB 2009-01-23 8.1 Release Bug 115 -> added --->
		passThroughVariablesStructure = "#frmVars#"
		<!--- <- NYB 2009-01-23 --->
>
	<cfelse>
			<cfoutput>#application.com.relayUI.message(message="No Contact History records available",type="info")#</cfoutput>
	</cfif>
<cfelse>
	<cfoutput>#application.com.relayUI.message(message="phr_useFiltersToDisplayResults",type="info")#</cfoutput>
</cfif>
<!--- NYB 2009-01-23 8.1 Release Bug 115 -> removed ---
</table>
!--- <- NYB 2009-01-23 --->

<CFSET frmNextTarget = "main">
<cfinclude template="/data/_editMainEntities.cfm">


