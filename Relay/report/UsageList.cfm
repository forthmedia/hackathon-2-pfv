<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		UsageList.cfm
Author:			SWJ
Date created:	

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

--->

<CFSET REPORTBEGINDATE=NOW()-30>
<CFSET REPORTENDDATE=NOW()>
<CFSET MINLOGINS=0>
<CFPARAM NAME="frmSortOrder" DEFAULT="UserName ASC">

<!--- 2012-07-26 PPB P-SMA001 commented out
<cfinclude template="/templates/qryGetCountries.cfm">
--->

<CFQUERY NAME="GetUsage" datasource="#application.sitedatasource#" DEBUG>
SELECT 	Country.CountryDescription, location.siteName, 
		person.Username, person.PersonID,
		usage.LoginDate, 
		usage.app, usage.RemoteIP,
		usage.browser
FROM Country INNER JOIN ((usage RIGHT JOIN person ON usage.PersonID = person.PersonID) 
INNER JOIN Location ON person.LocationID = Location.LocationID) ON Country.CountryID = Location.CountryID
WHERE usage.LoginDate >  <cf_queryparam value="#createODBCdate(reportbegindate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
AND 	person.personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
<!--- 2012-07-26 PPB P-SMA001 commented out
and Country.CountryID  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
 --->
#application.com.rights.getRightsFilterWhereClause(entityType="person").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->

ORDER By usage.LoginDate desc
</CFQUERY>



<cf_head>
	<cf_title>Usage List</cf_title>
</cf_head>



<cfset subsection = "Last 30 entries in the usage table">


<table width="100%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<TR>
		<Td COLSPAN="4" ALIGN="left" class='offsetbackground'><CFOUTPUT>#htmleditformat(listfirst(GetUsage.username))# <BR>#htmleditformat(listfirst(getUsage.sitename))#</CFOUTPUT></Td>
	</TR>
	<TR>
		<TH>Application</TH>
		<TH>Remote IP</TH>
		<TH>Login date (time)</TH>
		<TH>Browser</TH>
	</TR>
	<CFOUTPUT QUERY="GetUsage" MAXROWS=30>
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		 	<TD ALIGN="CENTER">#htmleditformat(app)#</TD>
			<TD ALIGN="CENTER">#htmleditformat(RemoteIP)#</TD>
			<TD ALIGN="CENTER">#dateformat(LoginDate,"dd-mmm-yy")# (#timeformat(LoginDate,"HH:MM")#)</TD>
			<TD>#htmleditformat(Browser)#</TD>
			
		</TR>	
	</CFOUTPUT>
</TABLE>







