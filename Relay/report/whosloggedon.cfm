<!--- �Relayware. All Rights Reserved 2014 --->
<!-- William's Code for seeing who is logged on --->

<CFSET freezedate=now()>


<CFQUERY NAME="WhosLoggedOn" DATASOURCE="#application.SiteDataSource#">
SELECT 	Usage.LoginDate, 
	datediff(minute,logindate,#freezedate#) AS TimeOn, 
	Person.FirstName, 	
	Person.LastName, 
	Usage.Browser, 
	Usage.RemoteIP
FROM Person INNER JOIN Usage ON Person.PersonID = Usage.PersonID
WHERE 	Usage.LoginDate>dateadd(day, -2, #freezedate#)
ORDER BY Usage.LoginDate DESC
</CFQUERY>

<cf_head>
	<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT> - Who has logged on recently?</cf_title>
</cf_head>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu"><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT> - Who has logged on recently?</TD>
	</TR>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<TR>
	<Th>Logged on at</Th>
	<Th>Logged on for</Th>
	<Th>Who?</Th>
	<Th>Browser</Th>
	<Th>Remote IP</Th>
</TR>

<CFOUTPUT query="WhosLoggedOn">
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD>#htmleditformat(logindate)#</TD>
		<TD><CFIF Timeon LE 120 > #htmleditformat(Timeon)# minutes  <CFELSE> Timed Out</CFIF> </TD>
		<TD>#htmleditformat(firstname)# #htmleditformat(lastname)#</TD>
		<TD>#htmleditformat(browser)#</TD>
		<TD>#htmleditformat(remoteip)#</TD>
	</TR>
</CFOUTPUT>
</TABLE>


