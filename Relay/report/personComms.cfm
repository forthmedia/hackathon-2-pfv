<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Communications sent to a particular person --->



<cf_head>
	<cf_title>Comms Sent</cf_title>
</cf_head>



<CFLOOP index="personid" list="#frmpersonid#">

	<CFQUERY name="CommsSent" dataSource="#application.SiteDataSource#" maxrows = "30" DEBUG>
	SELECT 	c.Title, 
			c.commid, 
			c.SendDate, 
			LL1.ItemText as FormText, 
			LL.ItemText as TypeText,
			s.name as statustext,
			cd.feedback,
			cd.commStatusID as status,
			cd.fax as addressused
	FROM CommDetail cd 
		INNER JOIN Communication c on  cd.CommID = c.CommID
		INNER JOIN LookupList LL on  c.CommTypeLID = LL.LookupID
		INNER JOIN LookupList LL1 on cd.CommFormLID = LL1.LookupID 
		INNER JOIN commDetailStatus s on cd.commStatusID=s.commStatusID
	WHERE cd.PersonID =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND c.Sent <>0  
	AND c.CommFormLID <>4 
	AND c.CommTypeLID <>50
	ORDER BY c.commid DESC
	</CFQUERY>
	
	<CFQUERY name="getperson" dataSource="#application.SiteDataSource#" DEBUG>
	Select 	person.FirstName as firstname,
			person.lastName as lastname
	From 	Person
	Where	Personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<CFOUTPUT>
	Communications Sent to <BR>
	 #htmleditformat(getperson.Firstname)# #htmleditformat(getperson.lastname)#
	</cfoutput>
	
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TH>Date<BR>CommID</TH>
		<TH>Title</TH>
		<TH>By</TH>
		<TH>Type</TH>
		<TH>Address or Fax #</TH>
		<TH>Status</TH>
	</TR>
	
	<CFOUTPUT query="commssent" maxrows="70">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		    <TD>#DateFormat(senddate,"DD/MM/YY")#<BR>#htmleditformat(commid)#</TD>
			<TD>#htmleditformat(title)# (#htmleditformat(commid)#)</font></TD>
			<TD>#htmleditformat(formText)#</TD>
			<TD>#htmleditformat(TypeText)#</TD>
			<TD>#htmleditformat(Addressused)#</TD>
			<TD>#htmleditformat(statustext)# #iif(status LE 0, DE(". Feedback: " & feedback), DE(""))#</TD>
		</TR>
	</cfoutput>
	</TABLE>
	

<P>
</cfloop>



