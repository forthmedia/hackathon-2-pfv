<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File:			FlagGroupbyDate.cfm
Author:			SWJ
Date created:	Oct 99

Description:
	Reports on a flagGroup giving a count of records for the whole flag group


Version history:

SWJ updated 2-Mar-2000 Amended to support country filter
	2016-02-03	WAB Mass replace of displayValidValues using lists to use queries (may have touched unused files)
	
--->
<CFPARAM NAME="frmDateCriteria" DEFAULT="thisWeek">

<!--- The next block of code sets up the correct variables for the country drop down
It may be good to turn this into a Custom Tag --->
<cfinclude template="/templates/qrygetcountries.cfm">
<CFPARAM NAME="frmcountryid" DEFAULT="#CountryList#">

<CFIF ListLen(CountryList) GT 1>
	<!--- NJH 2011-01-06 - don't need this template.
	<CFSET chkPersonID = #request.relayCurrentUser.personid#>
	<cfinclude template="/templates/qryCheckUser.cfm"> --->
	<CFSET currentCountryID = request.relayCurrentUser.countryID>
</CFIF>


<CFIF NOT isDefined("flaggroupid")>
	<H2>This report is running without a flagGroup</H2>
	<CFSET flaggroupid=71>

</CFIF> 

<!--- Get FlagGroup name for use throughout report --->
<CFQUERY NAME="GetFlagGroup" datasource="#application.sitedatasource#">
	SELECT name, tablename, flaggroupid from flagGroup, flagentitytype
	where flaggroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >  
	and flaggroup.entitytypeid = flagentitytype.entitytypeid
</CFQUERY>

<CFIF GetFlagGroup.recordCount IS 0>
	<H2>Not a valid flag group</H2>
	<CF_ABORT>
<CFELSE>
	<CFSET FlagGroupName =  #GetFlagGroup.Name#>
</CFIF>


<!--- convert the date criteria chosen into a bit of SQL --->
<CFSET thisweek = datepart("ww", now())>
<CFSET thismonth = datepart("m", now())>
<CFSET datecriteria = "">
<CFIF frmdateCriteria IS "thisWeek">
	<CFSET dateCriteria = 	'AND datepart("ww",BooleanFlagData.Created) = #thisweek# '>
<CFELSEIF frmdateCriteria IS "lastWeek">
	<CFSET lastWeek = thisWeek-1>
	<CFSET dateCriteria = 	'AND datepart("ww",BooleanFlagData.Created) = #lastweek# '>
<CFELSEIF frmdateCriteria IS "thisMonth">
	<CFSET dateCriteria = 	'AND datepart("m",BooleanFlagData.Created) = #thismonth# '>
<CFELSEIF frmdateCriteria IS "lastMonth">
	<CFSET lastMonth = thisMonth-1>
	<CFIF lastMonth IS 0><CFSET lastmonth = 12></cfif>
	<CFSET dateCriteria = 	'AND datepart("m",BooleanFlagData.Created) = #lastmonth# '>
</cfif>

<!--- within last year --->
<CFSET datecriteria = datecriteria & "AND BooleanFlagData.Created > DATEADD(yy, -1, getdate()) ">




<!--- Get GrandTotal of records for flagGroup--->
<CFQUERY NAME="Query1Total" datasource="#application.sitedatasource#">
SELECT 	Count(BooleanFlagData.FlagID) AS Total
FROM 	BooleanFlagData INNER JOIN Flag ON BooleanFlagData.FlagID = Flag.FlagID
		<CFIF getFlagGroup.tablename IS "Person">,person,location</cfif>
		<CFIF getFlagGroup.tablename IS "location">,location</cfif>
		<CFIF getFlagGroup.tablename IS "organisation">,organisation</cfif>
WHERE 	(((Flag.FlagGroupID) =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > )) 
		#preserveSingleQuotes(dateCriteria)#		
		<CFIF getFlagGroup.tablename IS "Person">
		AND person.personid = booleanflagdata.entityid
		AND person.locationid = location.locationid
		</cfif>
		<CFIF getFlagGroup.tablename IS "organisation">
		AND organisation.organisationid = booleanflagdata.entityid
		</cfif>
		<CFIF getFlagGroup.tablename IS "Location">
		AND location.locationid = booleanflagdata.entityid
		</cfif>
		<CFIF getFlagGroup.tablename IS "organisation">
		AND organisation.countryid  in ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>
		<CFIF getFlagGroup.tablename IS "Location" or getFlagGroup.tablename IS "Person">
		AND location.countryid  in ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>
</CFQUERY>



<CFSET Total1 = #Query1Total.total#>
<CFSET Title1 = "for all delegates">


<CFQUERY NAME="Query1" datasource="#application.sitedatasource#">
	SELECT Flag.Name, DATEPART(week, BooleanFlagData.Created) AS Week, Flag.FlagID,
		Count(BooleanFlagData.FlagID) AS Total 
	FROM BooleanFlagData INNER JOIN Flag ON BooleanFlagData.FlagID = Flag.FlagID
		<CFIF getFlagGroup.tablename IS "Person">,person,location</cfif>
		<CFIF getFlagGroup.tablename IS "location">,location</cfif>
		<CFIF getFlagGroup.tablename IS "organisation">,organisation</cfif>
WHERE 	(((Flag.FlagGroupID) =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > )) 
		#preserveSingleQuotes(dateCriteria)#		
		<CFIF getFlagGroup.tablename IS "Person">
		AND person.personid = booleanflagdata.entityid
		AND person.locationid = location.locationid
		</cfif>
		<CFIF getFlagGroup.tablename IS "organisation">
		AND organisation.organisationid = booleanflagdata.entityid
		</cfif>
		<CFIF getFlagGroup.tablename IS "Location">
		AND location.locationid = booleanflagdata.entityid
		</cfif>
		<CFIF getFlagGroup.tablename IS "organisation">
		AND organisation.countryid  in ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>
		<CFIF getFlagGroup.tablename IS "Location" or getFlagGroup.tablename IS "Person">
		AND location.countryid  in ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>


	GROUP BY DATEPART(week, BooleanFlagData.Created),Flag.Name, Flag.FlagID
ORDER BY Week


</CFQUERY>



<cf_head>
	<cf_title><CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></cf_title>
	<SCRIPT type="text/javascript">

<!--
        function viewRecords(ID) {
                var form = document.MyForm;
                form.FlagID.value = ID;
//				alert ("There is a problem with this function.  We are working on it at the moment.")
                form.submit();
                return;
        }
//-->
 
</SCRIPT>

</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top" CLASS="Submenu">
			Count of new <CFOUTPUT>#htmleditformat(FlagGroupName)#'s added by week for #htmleditformat(frmdatecriteria)#</CFOUTPUT>
		</TD>
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="Submenu">
			<CFOUTPUT>
			<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
			</CFOUTPUT>
		</TD>
	</TR>
</table>


<FORM ACTION="/report/profiles/FlagGroupbyDate.cfm" METHOD="post">
	<cf_displayValidValues 
		formFieldName = "frmcountryid" 
		validvalues= "#findCountries#" 
		dataValueColumn = "countryid"
		displayValueColumn = "countryDescription"
		nulltext="Please select a country" 
		method = "view"
		onChange = "submit()"
		currentValue = #currentCountryID#
		>
	<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="flaggroupid" VALUE="#flaggroupid#"></CFOUTPUT>
</FORM>

<FORM ACTION="/report/profiles/FlagGroupbyDate.cfm" METHOD="POST">
To change the number of weeks analysed, select from the list and click Re-run report: 
<SELECT NAME="frmDateCriteria">
		<OPTION VALUE="All" <CFIF frmDateCriteria is "ALL">selected</cfif>>All weeks in last year</OPTION>
	   	<OPTION VALUE="LastMonth" <CFIF frmDateCriteria is "LastMonth">selected</cfif> >Last Month</OPTION>
	   	<OPTION VALUE="ThisMonth" <CFIF frmDateCriteria is "ThisMonth">selected</cfif> >This Month</OPTION>
	   	<OPTION VALUE="LastWeek" <CFIF frmDateCriteria is "LastWeek">selected</cfif>>Last Week</OPTION>
	   	<OPTION VALUE="ThisWeek" <CFIF frmDateCriteria is "ThisWeek">selected</cfif>>This week</OPTION>
</SELECT>

<CFOUTPUT>
<CF_INPUT TYPE="Hidden" NAME="flaggroupid" VALUE="#flaggroupid#">
</CFOUTPUT>
<INPUT TYPE="Submit" NAME="Go" VALUE="Re-run report">
</FORM>

<CFLOOP INDEX="QueryNumber" FROM="1" TO="1" STEP="1">
	<CFSET counttitle = evaluate("title#QueryNumber#")>
	<CFSET Grandtotal = evaluate("total#querynumber#")>
	<FORM ACTION="/report/profiles/GetRecords.cfm" NAME="MyForm" METHOD="POST">
		<CFOUTPUT>
		<INPUT TYPE="Hidden" NAME="datecriteria" VALUE="#replace(dateCriteria,'"','&quot;','ALL')#">
		<INPUT TYPE="Hidden" NAME="FlagID" VALUE="">
		<INPUT TYPE="Hidden" NAME="ByWeek" VALUE="Yes">
		</cfoutput>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR STYLE="font-family: Arial; font-weight: bold;">
		<TD WIDTH="50%" ALIGN="LEFT" STYLE="font-family: Arial; font-weight: bold;"><B><CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></B></TD>
		<TD WIDTH="15%" ALIGN="CENTER" STYLE="font-family: Arial; font-weight: bold;"><B>Week</B></TD>
		<TD WIDTH="15%" ALIGN="CENTER" STYLE="font-family: Arial; font-weight: bold;"><B>Total for that week</B></TD>
		<TD WIDTH="15%" ALIGN="CENTER" STYLE="font-family: Arial; font-weight: bold;"><B>View the Records</B></TD>
	</TR>
	<CFOUTPUT QUERY="query#querynumber#">
	<TR>
		<TD WIDTH="50%" STYLE="font-family: Arial; font-size: 10 pt; color: Navy;"><B>#htmleditformat(name)#</B></TD>
		<TD WIDTH="15%" ALIGN="CENTER" STYLE="font-family: Arial; font-size: 10 pt; color: Navy;">#htmleditformat(week)#</TD>
		<TD WIDTH="15%" ALIGN="CENTER" STYLE="font-family: Arial; font-size: 10 pt; color: Navy;">#htmleditformat(total)#</TD>
		<TD WIDTH="15%" ALIGN="CENTER" STYLE="font-family: Arial; font-size: 10 pt; color: Navy;"><A HREF="JavaScript:viewRecords('#FlagID#');"> <IMG SRC="/images/MISC/reddot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A></TD>
	</TR></CFOUTPUT>
	</FORM>
	
	<TR>
		<TD COLSPAN="2">&nbsp;</TD>
		<TD><IMG SRC="../../images/MISC/rule.gif" WIDTH=50 HEIGHT=3 BORDER=0 ALT=""></TD>
	</TR>
	<TR>
		<TD WIDTH="25%"></TD>
		<TD WIDTH="50%"><DIV align=right>Total:</DIV></TD>
		<TD WIDTH="25%" ALIGN="CENTER"><CFOUTPUT>#htmleditformat(Grandtotal)#</CFOUTPUT></TD>
	</TR>
	
	</TABLE>
</CFLOOP>











