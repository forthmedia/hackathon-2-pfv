<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	DAM 10 July 2002
	Added additional logic to ensure that when

2005-10-17	WAB 	implemented function for testing whether user has rights to edit flag/flaggroup definition
2006-04-20 WAB		made a change so that addProfile() passes an entityType (should always be there)
2008-10-14	NYB		Amended error with adding child profiles via "Buld Add" in ProfileEdit
2009/05/27 SSS		Moved the excel link here so that it is part of the navigation

--->

<cfparam name="current" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.thisDir" type="string" default="profileManager">


<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">

	
	<CFIF findNoCase("profileReportHeader.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="View Pie Chart" CFTemplate="javascript:document.pieChartForm.submit()" target="">
		<CF_RelayNavMenuItem MenuItemText="View All Records" CFTemplate="javascript:viewAllProfiles('#flagGroupID#')" target="">
		<cfset localQueryString = replace(request.query_string,"&blowQueryCache=yes","","all")>
		<CF_RelayNavMenuItem MenuItemText="Refresh Query" CFTemplate="#CGI.SCRIPT_NAME#?#localQueryString#&blowQueryCache=yes" target="">
		<!--- <CF_RelayNavMenuItem MenuItemText="Edit Profile" CFTemplate="/profileManager/profileDetails.cfm?frmFlagGroupID=#flaggroupid#" target=""> --->
	</cfif>
</CF_RelayNavMenu>


