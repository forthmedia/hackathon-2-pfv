<!--- �Relayware. All Rights Reserved 2014 --->

<!--- For testing --->
<CFSET flaggroupid=152>

<CFIF NOT isDefined("flaggroupid")>
	<H2>You must define a Flag Group for this report to run</H2>
	<CF_ABORT>
</CFIF>
<!--- Get FalgGroup name for use throughout report --->
<CFQUERY NAME="GetFlagGroup" datasource="#application.sitedatasource#">
SELECT name from flagGroup where flaggroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFIF GetFlagGroup.recordCount IS 0>
	<H2>Not a valid flag group</H2>
	<CF_ABORT>
<CFELSE>
	<CFSET FlagGroupName =  #GetFlagGroup.Name#>
</CFIF>

<!--- Get GrandTotal of records for flagGroup--->
<CFQUERY NAME="Query1Total" datasource="#application.sitedatasource#">
	SELECT Count(BooleanFlagData.FlagID) AS Total
FROM BooleanFlagData INNER JOIN Flag ON BooleanFlagData.FlagID = Flag.FlagID
WHERE (((Flag.FlagGroupID) =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > ));
</CFQUERY>
<CFSET Total1 = #Query1Total.total#>
<CFSET Title1 = "for all delegates">
<CFQUERY NAME="Query1" datasource="#application.sitedatasource#">
	SELECT Flag.Name, 
		Count(BooleanFlagData.FlagID) AS Total, 
		Fraction=(Convert(Real, Count(BooleanFlagData.FlagID))/#Total1#)*100
	FROM BooleanFlagData INNER JOIN Flag ON BooleanFlagData.FlagID = Flag.FlagID
	WHERE (((Flag.FlagGroupID) =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > ))
	GROUP BY Flag.Name
</CFQUERY>

<!--- Get 3Com of records for flagGroup--->
<CFQUERY NAME="Query2Total" datasource="#application.sitedatasource#">
SELECT Count(BooleanFlagData.FlagID) AS Total
FROM (Location INNER JOIN Person ON Location.LocationID = Person.LocationID) 
INNER JOIN (Flag INNER JOIN BooleanFlagData ON Flag.FlagID = BooleanFlagData.FlagID) ON Person.PersonID = BooleanFlagData.EntityID
WHERE (((Flag.FlagGroupID) =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND ((Location.SiteName) Like '3Com%'))
</CFQUERY>
<CFSET Total2 = #Query2Total.total#>
<CFSET Title2 = "for internal 3Com delegates">
<CFQUERY NAME="Query2" datasource="#application.sitedatasource#">
	SELECT Flag.Name, Count(BooleanFlagData.FlagID) AS Total,
	Fraction=(Convert(Real, Count(BooleanFlagData.FlagID))/#Total2#)*100
	FROM ((Person INNER JOIN Location ON Person.LocationID = Location.LocationID) 
	INNER JOIN Country ON Location.CountryID = Country.CountryID) 
	INNER JOIN (BooleanFlagData INNER JOIN Flag ON BooleanFlagData.FlagID = Flag.FlagID) 
	ON Person.PersonID = BooleanFlagData.EntityID
	WHERE Location.SiteName Like '3Com%'
	GROUP BY Flag.Name, Flag.FlagGroupID
	HAVING Flag.FlagGroupID =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- Get 3Com of records for flagGroup--->
<CFQUERY NAME="Query3Total" datasource="#application.sitedatasource#">
SELECT Count(BooleanFlagData.FlagID) AS Total
FROM (Location INNER JOIN Person ON Location.LocationID = Person.LocationID) 
INNER JOIN (Flag INNER JOIN BooleanFlagData ON Flag.FlagID = BooleanFlagData.FlagID) ON Person.PersonID = BooleanFlagData.EntityID
WHERE (((Flag.FlagGroupID) =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND ((Location.SiteName) NOT Like '3Com%'))
</CFQUERY>
<CFSET Total3 = #Query3Total.total#>
<CFSET Title3 = "for Partner delegates">
<CFQUERY NAME="Query3" datasource="#application.sitedatasource#">
	SELECT Flag.Name, Count(BooleanFlagData.FlagID) AS Total,
	Fraction=(Convert(Real, Count(BooleanFlagData.FlagID))/#Total3#)*100
	FROM ((Person INNER JOIN Location ON Person.LocationID = Location.LocationID) 
	INNER JOIN Country ON Location.CountryID = Country.CountryID) 
	INNER JOIN (BooleanFlagData INNER JOIN Flag ON BooleanFlagData.FlagID = Flag.FlagID) 
	ON Person.PersonID = BooleanFlagData.EntityID
	WHERE Location.SiteName NOT Like '3Com%'
	GROUP BY Flag.Name, Flag.FlagGroupID
	HAVING Flag.FlagGroupID =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<cf_head>
	<cf_title><CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></cf_title>
</cf_head>
<H1>Analysis of <CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></H1>


<CFLOOP INDEX="QueryNumber" FROM="1" TO="3" STEP="1">
	<CFSET counttitle = evaluate("title#QueryNumber#")>
	<CFSET Grandtotal = evaluate("total#querynumber#")>
	<H2>Analysis of <CFOUTPUT> #FlagGroupName# #counttitle#</CFOUTPUT></H2>
	<TABLE WIDTH="597" height=136 style="HEIGHT: 136px; WIDTH: 597px">
	<TR STYLE="font-family: Arial; font-weight: bold;">
		<TD WIDTH="50%" ALIGN="LEFT" STYLE="font-family: Arial; font-weight: bold;"><B><CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></B></TD>
		<TD WIDTH="50%" ALIGN="CENTER" STYLE="font-family: Arial; font-weight: bold;"><B>Count</B></TD>
		<TD WIDTH="50%" ALIGN="CENTER" STYLE="font-family: Arial; font-weight: bold;"><B>Percentage</B></TD>
	</TR>
	<CFOUTPUT QUERY="query#querynumber#">
	<TR>
		<TD WIDTH="50%"><B>#htmleditformat(name)#</B></TD>
		<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(total)#</TD>
		<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(round(Fraction))# %</TD>
	</TR></CFOUTPUT>
	<TR>
		<TD WIDTH="50%"><DIV align=right>Total:</DIV></TD>
		<TD WIDTH="25%" ALIGN="CENTER"><CFOUTPUT>#htmleditformat(Grandtotal)#</CFOUTPUT></TD>
		<TD WIDTH="25%"></TD>
	</TR>
	
	</TABLE>
	
<DIV ALIGN="center"><IMG SRC="../../images/MISC/rule.gif" WIDTH=230 HEIGHT=3 BORDER=0 ALT=""></DIV>
<P>&nbsp;</P>
</CFLOOP>



