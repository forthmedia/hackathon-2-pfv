<!--- �Relayware. All Rights Reserved 2014 --->

<!--- the application .cfm checks for read adminTask privleges --->


<!--- Mod History
1999-07-07 WAB adjusted so that all those with admin rights can see and edit all flags
1999-08-20 SWJ add frmEntityTypeID test to constrain the number of flagGroups
PKP : 2008-04-14  - some changes to display (bug TREND 162)
WAB 2008/05/19  - had to roll back most of PKP changes because it ended up with none of the child flaggroups being shown
--->

<cf_includeJavascriptOnce template = "/javascript/myRelay.js">


<CFPARAM NAME="frmEntityTypeID" DEFAULT="0">
<cfparam name="uniqueFrameID" default="#createUUID()#">

<cfif isdefined('form.frmEntityTypeID')>
	<cfset frmEntityTypeID = form.frmEntityTypeID>
</cfif>


<CFIF isDefined("frmEntityTypeID")>
	<CFSET session.frmEntityTypeID = frmEntityTypeID>
<CFELSEIF isDefined("session.frmEntityTypeID")>
	<CFSET frmEntityTypeID = session.frmEntityTypeID>
<CFELSE>
	<CFPARAM NAME="frmEntityTypeID" DEFAULT="99">
</cfif>

<CFPARAM NAME="entityType" DEFAULT="Unknown">

<CFIF isDefined("searchPhrase")>
	<CFSET session.searchPhrase = searchPhrase>
<CFELSEIF isDefined("session.searchPhrase")>
	<CFSET searchPhrase = session.searchPhrase>
<CFELSE>
	<CFPARAM NAME="searchPhrase" DEFAULT="">
</cfif>

<!--- DF --->

<CFPARAM NAME="discardEntityTypeIDs" DEFAULT="0,2">

<!--- NJH 2006/07/13 queries moved to relay\com\profileManager.cfc --->

<!--- Get USER countries --->
<!--- <cfinclude template="/templates/qrygetcountries.cfm">

<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

<!--- Find all Country Groups for this Country --->
<!--- Note: no User Country rights implemented for site currently --->
<CFQUERY NAME="getCountryGroups" datasource="#application.sitedatasource#">
	SELECT DISTINCT b.CountryID
		 FROM Country AS a, Country AS b, CountryGroup AS c
		WHERE (b.ISOcode IS null OR b.ISOcode = '')
		  AND c.CountryGroupID = b.CountryID
		  AND c.CountryMemberID = a.CountryID
		  AND a.CountryID IN (#Variables.CountryList#) <!--- User Country rights --->
	ORDER BY b.CountryID
</CFQUERY>


<CFSET UserCountryList = "0,#Variables.CountryList#">
<CFIF getCountryGroups.CountryID IS NOT "">
	<!--- top record (or only record) is not null --->
	<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>

<CFQUERY NAME="getFlagGroups" datasource="#application.sitedatasource#">
SELECT
FlagGroup.FlagGroupID AS Grouping,
FlagGroup.FlagGroupID,
FlagGroup.EntityTypeID,
FlagGroup.FlagGroupTextID,
FlagGroup.FlagTypeID,
FlagGroup.Active,
FlagGroup.Name,
FlagGroup.description,
FlagGroup.ParentFlagGroupID,
FlagGroup.ViewingAccessRights,
FlagGroup.EditAccessRights,
FlagGroup.DownloadAccessRights,
FlagGroup.SearchAccessRights,
FlagGroup.OrderingIndex AS parent_order,
0 AS child_order,
(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID) As flag_count,

(SELECT Count(1) FROM FlagGroup AS s WHERE s.FlagGroupID = FlagGroup.FlagGroupID AND
(FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (s.EditAccessRights = 0 AND s.Edit <> 0)
	or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
	WHERE  r.SecurityTypeID = s.SecurityTypeID
	AND s.ShortName  = 'AdminTask'
	AND r.usergroupid = rg.usergroupid
	AND rg.PersonID=#request.relayCurrentUser.personid# )
	OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE s.EditAccessRights <> 0
                AND s.FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.usergroupid#
                AND t.Edit <> 0)))) AS edit_count
FROM FlagGroup

<!--- added by DF --->
INNER JOIN FlagType ft ON ft.flagTypeID = flagGroup.flagTypeID
<cfif isDefined("searchPhrase") and searchPhrase neq "">
INNER JOIN Flag ON Flag.FlagGroupID = FlagGroup.FlagGroupID
</cfif>

WHERE ParentFlagGroupID = 0


<cfif compareNoCase(frmEntityTypeID,"OTHER") eq 0>
	AND FlagGroup.EntityTypeID not in (#discardEntityTypeIDs#)
<cfelse>
	AND FlagGroup.EntityTypeID = #frmEntityTypeID#
</cfif>


<!--- Added by SWJ to constrain the flag list a bit --->
AND FlagGroup.Scope IN (#UserCountryList#)
AND flagGroup.active = 1
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID
						AND s.ShortName  = 'AdminTask'
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )
	OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
	OR (FlagGroup.ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
			AND FlagGroupRights.Viewing <> 0)))

<!---added by DF --->
<cfif isDefined("searchPhrase") and searchPhrase neq "">
	AND (FlagGroup.FlagGroupTextID like '%#replaceNoCase(searchPhrase," ","%","all")#%' or FlagGroup.Name like '%#replaceNoCase(searchPhrase," ","%","all")#%' or Flag.Name like '%#replaceNoCase(searchPhrase," ","%","all")#%' or Flag.FlagTextID like '%#replaceNoCase(searchPhrase," ","%","all")#%')
</cfif>

UNION
SELECT
FlagGroup.ParentFlagGroupID,
FlagGroup.FlagGroupID,
FlagGroup.EntityTypeID,
FlagGroup.FlagGroupTextID,
FlagGroup.FlagTypeID,
FlagGroup.Active,
FlagGroup.Name,
FlagGroup.description,
FlagGroup.ParentFlagGroupID,
FlagGroup.ViewingAccessRights,
FlagGroup.EditAccessRights,
FlagGroup.DownloadAccessRights,
FlagGroup.SearchAccessRights,
p.OrderingIndex,
FlagGroup.OrderingIndex,
(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID),

(SELECT Count(1) FROM FlagGroup AS s WHERE s.FlagGroupID = FlagGroup.FlagGroupID AND
(FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (s.EditAccessRights = 0 AND s.Edit <> 0)
	or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
				WHERE  r.SecurityTypeID = s.SecurityTypeID
				AND s.ShortName  = 'AdminTask'
				AND r.usergroupid = rg.usergroupid
				AND rg.PersonID=#request.relayCurrentUser.personid# )
OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE s.EditAccessRights <> 0
                AND s.FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.usergroupid#
                AND t.Edit <> 0))))
FROM FlagGroup, FlagGroup as p
WHERE FlagGroup.ParentFlagGroupID <> 0
AND p.ParentFlagGroupID = 0
AND flagGroup.active = 1


<cfif compareNoCase(frmEntityTypeID,"OTHER") eq 0>
	AND FlagGroup.EntityTypeID not in (#discardEntityTypeIDs#)
<cfelse>
	AND FlagGroup.EntityTypeID = #frmEntityTypeID#
</cfif>


<!--- Added by SWJ to constrain the flag list a bit --->
AND p.FlagGroupID = FlagGroup.ParentFlagGroupID
AND FlagGroup.Scope IN (#UserCountryList#)
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
	or exists (	SELECT 1 FROM RightsGroup as rg, UserGroup as ug
						WHERE rg.UserGroupID = ug.UserGroupID
						AND rg.PersonID=#request.relayCurrentUser.personid# AND ug.Name='Admin-Standard'  )
	or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID
						AND s.ShortName  = 'AdminTask'
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )
	OR (FlagGroup.ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
			AND FlagGroupRights.Viewing <> 0)))

<!--- Added By DF --->

<cfif isDefined("searchPhrase") and searchPhrase neq "">
	AND ( FlagGroup.FlagGroupTextID like '%#replaceNoCase(searchPhrase," ","%","all")#%' or FlagGroup.Name like '%#replaceNoCase(searchPhrase," ","%","all")#%' )
</cfif>

ORDER BY parent_order, grouping, child_order, flagGroup.Name   <!--- DF added flagGroup.Name was just 'Name' --->
</CFQUERY> --->


<!--- NJH 2006/07/12 moved queries to profileManagement.cfc as they are used in myRelay\myLinkList.cfm as well. --->
<cfset userCountries = application.com.profileManagement.getCountryGroups()>
<cfset profileFlags = application.com.profileManagement.getProfileFlagGroups(EntityTypeID=#frmEntityTypeID#,UserCountryList=#userCountries#, searchPhrase=#searchPhrase#, discardEntityTypeIDs=#discardEntityTypeIDs#)>

<SCRIPT type="text/javascript">

		function checkType() {

				     if(document.functions.frmEntityTypeID.value == 666) {

						    alert("Phr_Sys_SelectFilter");
								document.functions.frmEntityTypeID.focus();
								return false;

						 }

		}

</script>
<cf_includejavascriptonce template = "/javascript/openwin.js">

<CFIF IsDefined('message')>

	<P><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>

</CFIF>

<!--- add the element to the query object that allows "Select" to be the default behaviour --->
<!-- DF -->

<cfscript>
	qEntitTypes = application.com.profilemanagement.GetEntityTypes();
</cfscript>

<cfset temp = QueryAddRow(qEntitTypes, 1)>
<cfset temp = QuerySetCell(qEntitTypes, "ENTITYTYPEID", 666)>
<cfset temp = QuerySetCell(qEntitTypes, "TABLENAME", "Phr_Sys_AnyProfile")>

<cfif not isdefined('form.frmEntityTypeID')>
	<cfset frmEntityTypeID = "666">
</cfif>

<cfquery name="qEntitTypes2" dbtype="query">
	select * from qEntitTypes where entityTypeID in (0,1,2) order by ENTITYTYPEID desc
</cfquery>

<div class="header">
<table width="100%" class="withBorder" cellspacing="1" cellpadding="3">

<!--- NJH 2012/11/08 Case 431718 - removed uniqueframeId from action and added it as a hidden field --->
<cfform name="functions" action="/report/profiles/flagGroupList.cfm" method="post">
	<tr><td>
<cfif isdefined('url.frmEntityTypeID')>
	<cfset frmEntityTypeID = #url.frmEntityTypeID# >
</cfif>

	<cfquery name="getCurrentEntityName" dbtype="query">
		select tablename from qEntitTypes2 where entityTypeID = #frmEntityTypeID#
	</cfquery>

	<CF_INPUT type="hidden" name="entityType" value="#entityType#">
	Phr_Sys_chooseType
	<cfselect name="frmEntityTypeID" size="1" query="qEntitTypes2"
		value="entityTypeID" display="tableName"
		selected="#frmEntityTypeID#"
	>
	</cfselect>
	<br>
	<cf_input type="hidden" name="uniqueFrameID" value="#uniqueFrameID#">

<cfoutput>

<cfif not isdefined('frmEntityTypeID')>
	<CF_INPUT type="hidden" name="frmEntityTypeID" value="#frmEntityTypeID#">
</cfif>

phr_Sys_SearchFor <CF_INPUT type="text" name="searchPhrase" size="25" value="#searchPhrase#">&nbsp;
<input type="submit" onClick="return checkType();" value="Go">

</cfoutput>

</td></tr>
</cfform>
</table>
</div>
<div style="height:75px;display:block;"></div>

<!-- / DF -->
<CFIF profileFlags.Recordcount gt 0>

  <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<CFOUTPUT>
	</CFOUTPUT>
    <CFSET cnt = 0>
   <!---  <TR class="withBorder">
      <TH colspan="2">Name (click for analysis)</TH> --->
<!---       <TH>Number<BR>
        Flags</TH>
      <TH>Report</TH>
 ---><!--- 2004-06-22 SWJ Removed for the moment      <TH>By week</TH> --->
<!---     </TR> --->

    <CFOUTPUT QUERY="profileFlags" GROUP="grouping">
      <CFSET thisFlagGroup = FlagGroupID>
      <CFSET cnt = cnt + 1>
      <TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<CFIF flag_count gt 0 or profileFlags.ParentFlagGroupID IS NOT 0>
				<td><!--- valign="top"><cfif not listFindNoCase(reportURLs,"/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#")><a href="javascript: newWindow = openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#&linkName=#Name#&linkGroup=#getCurrentEntityName.tablename#&linkType=profile','','width=500,height=50,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no')" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif></td>
				<td><A HREF="profileReportHeader.cfm?FlagGroupID=#FlagGroupID#&uniqueFrameID=#uniqueFrameID#" target="reportControl#uniqueFrameID#" CLASS="smallLink">#HTMLEditFormat(Name)#</A> --->
				<cfset application.com.myRelay.displayLink(href="/report/profiles/profileReportHeader.cfm?FlagGroupID=#FlagGroupID#&uniqueFrameID=#uniqueFrameID#",linkName=Name,linkAttribute1="#application.entityType[frmEntityTypeID].tablename# Profiles",target="reportControl#uniqueFrameID#",linkType="Profile",linkLocation="/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#")>
				</td>
			<CFELSE>
				<!--- NJH 2008/07/16 put parent flag groups in a th and other flag groups in a td --->
				<cfif profileFlags.ParentFlagGroupID IS 0>
				<th colspan="2">#HTMLEditFormat(Name)#</th>
				<cfelse>
					<td>&nbsp;#HTMLEditFormat(Name)#</td>
				</cfif>
            </CFIF>
<!---         <TD ALIGN="center"><CFIF flag_count IS NOT 0>
            #flag_count#
            <CFELSE>
            -</CFIF></TD>
        <TD ALIGN="center"><A HREF="javaScript:viewDetail('#FlagGroupID#');"><IMG SRC="/images/MISC/Icon_Graph.gif" ALT="" WIDTH="16" HEIGHT="16" BORDER="0"></A></TD>
 ---><!---  2004-06-22 SWJ Removed for the moment       <TD ALIGN="center"><A HREF="JavaScript:viewByWeek('#FlagGroupID#');" CLASS="smallLink"><IMG SRC="/images/MISC/reddot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A></TD> --->
      </TR>
			<CFOUTPUT>
					<!--- NJH 2008/07/16 Bug Fix T-10 Issue 671 - added 'and flagGroupID neq thisFlagGroup' so that we don't display duplicates--->
			        <CFIF profileFlags.ParentFlagGroupID IS NOT 0 and flagGroupID neq thisFlagGroup>
				          <TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
								<CFIF flag_count gt 0>
									<td> <!--- valign="top"><cfif not listFindNoCase(reportURLs,"/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#")><a href="javascript: newWindow = openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#&linkName=#Name#&linkGroup=#getCurrentEntityName.tablename#&linkType=profile','','width=500,height=50,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no')" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif></td>
									<td><a href="profileReportHeader.cfm?FlagGroupID=#FlagGroupID#&uniqueFrameID=#uniqueFrameID#" target="reportControl#uniqueFrameID#" class="smallLink">#HTMLEditFormat(Name)#</a>--->
									<cfset application.com.myRelay.displayLink(href="/report/profiles/profileReportHeader.cfm?FlagGroupID=#FlagGroupID#&uniqueFrameID=#uniqueFrameID#",linkName=Name,linkAttribute1="#application.entityType[frmEntityTypeID].tablename# Profiles",target="reportControl#uniqueFrameID#",linkType="Profile",linkLocation="/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#")>
									</td>
								<CFELSE>
									<td>#HTMLEditFormat(Name)#</td>
					            </CFIF>

		<!--- 		            <TD ALIGN="center"><CFIF flag_count IS NOT 0>
				                #flag_count#
				                <CFELSE>
				                -</CFIF></TD>
				            <TD ALIGN="center"><A HREF="javaScript:viewReport('#FlagGroupID#');"><IMG SRC="/images/MISC/Icon_Graph.gif" ALT="" WIDTH="16" HEIGHT="16" BORDER="0"></A></TD>
		 --->
		 <!--- 	2004-06-22 SWJ Removed for the moment	            <TD ALIGN="center"><A HREF="JavaScript:viewByWeek('#FlagGroupID#');" CLASS="smallLink"><IMG SRC="/images/MISC/reddot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A></TD> --->
				          </TR>
			        </CFIF>
		      </CFOUTPUT>
		     <!---  <tr><td colspan="2"></td></tr> --->
	</CFOUTPUT>

	</FORM>
</TABLE>
<CFELSE>
	<P>There are no Flag Groups.
</CFIF>