<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File:			FlagGroupCounts.cfm
Author:			SWJ
Date created:	Oct 99

Description:
	Reports on a flagGroup giving a count of records for the whole flag group


Version history:

2001-09-24	SWJ		Fixed the two main queries to be generic
2001-07-17	SWJ		Ammended to go to orgListing screen
2-Mar-2000 SWJ updated Amended to support country filter
2002-03-02	SWJ		Ammended to support drill through to org,loc & person entities more tightly with orgListv3.cfm.
					Corrected a bug which always queried booleanFlagData irrespective of the flagType 
2004-10-19	SWJ		Added link to show all records
2007-01-03	SWJ		Added the ability to get country counts and blow the query cache
--->

<cfinclude template="/templates/qrygetcountries.cfm">
<CFPARAM NAME="frmcountryid" DEFAULT="#CountryList#">
<CFPARAM NAME="flagid" DEFAULT=0>
<CFPARAM NAME="nodata" DEFAULT=true>
<cfparam name="uniqueFrameID" default="">
<!--- supportedEntities defines the flagEntityTypes that will support drill through to the dataFrame (and orglistV3) --->
<cfset supportedEntities = "person,location,organisation">
 
 
<CFIF NOT isDefined("flaggroupid")>
	<H2>You must define a Flag Group for this report to run</H2>
	<CF_ABORT>
</CFIF>

<cfset getflagGroup = application.com.flag.getFlagGroupStructure(flaggroupid)>


<!---  <!--- Get FlagGroup name for use throughout report --->
<CFQUERY NAME="GetFlagGroup" datasource="#application.sitedatasource#">
	SELECT fg.name, entityName, UniqueKey, ft.dataTableFullName, fg.flagGroupID, ft.FlagTypeID
		from flagGroup fg 
		inner join schemaTable st on fg.entityTypeID = st.entityTypeID
		inner join flagType ft on fg.flagTypeid = ft.flagTypeid
	where flaggroupid=#flaggroupid#
</CFQUERY>
 --->

<CFIF GetFlagGroup.flaggroupid IS 0>
	<H2>Not a valid flag group</H2>
	<CF_ABORT>
<CFELSE>
	<CFSET FlagGroupName = GetFlagGroup.Name>
</CFIF>

	<cfif structKeyExists(URL,"blowQueryCache")>
		<cfset structRptData = application.com.relayReportDataProfiles.getFlagCounts(flagGroupID=flagGroupID,cacheQueriesForMins=0)>
	<cfelse>
		<cfset structRptData = application.com.relayReportDataProfiles.getFlagCounts(flagGroupID=flagGroupID)>
	</cfif>
	<CFSET Total1 = structRptData.total>
	<CFSET Title1 = "">
	<cfset query1 = structRptData.queryData>
<cfoutput>
	
<cf_title>#FlagGroupName#</cf_title>
<cf_body onLoad="document.pieChartForm.submit()"/>

<cf_head>
	<SCRIPT type="text/javascript">
        function viewDetail(ID) {
                var form = document.MainForm#uniqueFrameID#;
                form.frmFlagIDfromFlagCount.value = ID;
                form.submit();
                return;
        }
		
        function viewAllProfiles(profileID) {
                var form = document.MainForm#uniqueFrameID#;
				//alert (profileID);
				form.frmFlagIDfromFlagCount.value = 0;
                form.frmProfileID.value = profileID;
                form.submit();
                return;
        }

		function submitExternal(ID) {
			document.FG_Form.FlagID.value = ID
			document.FG_Form.submit()
		}
	</SCRIPT>
</cf_head>

<form action="" method="post" name="countryFilter" id="countryFilter">
	<cf_displayValidValues 
		formFieldName = "frmcountryid" 
		validvalues= "#findCountries#" 
		dataValueColumn = "countryid"
		displayValueColumn = "countryDescription"
		nulltext="Please select a country" 
		method = "view"
		onChange = "submit()"
		currentValue = #frmcountryID#
		>
	<CF_INPUT TYPE="hidden" NAME="flaggroupid" VALUE="#flaggroupid#">
</form>

<cfswitch expression="#getFlagGroup.entityType.tableName#">
	<cfcase value="person"><cfset formActionTemplate = "flagPers"></cfcase>
	<cfcase value="location"><cfset formActionTemplate = "flagLocs"></cfcase>
	<cfcase value="organisation"><cfset formActionTemplate = "flagOrgs"></cfcase>
	<cfdefaultcase><cfset formActionTemplate = "flagDefault"></cfdefaultcase>
</cfswitch>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<form action="/report/chartPie.cfm" method="post" id="pieChartForm" name="pieChartForm" target="reportDetail#uniqueFrameID#">
<!--- 	<textarea cols="50" rows="15" name="WDDXQueryPacketText">#variables.querypacket#</textarea> --->
	<!--- <input type="hidden" name="WDDXQueryPacket" value="#variables.querypacket#"> --->
	<CF_INPUT type="hidden" name="chartTitle" value="#FlagGroupName# Profile"> 
	<input type="hidden" name="itemColumn" value="name"> 
	<input type="hidden" name="valuecolumn" value="total">
</form>

<form action="/data/#formActionTemplate#.cfm" method="post" name="MainForm#uniqueFrameID#" target="reportDetail#uniqueFrameID#">
		<INPUT TYPE="hidden" NAME="frmFlagIDfromFlagCount" VALUE="0">
		<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#getFlagGroup.entityType.tableName#">
		<CF_INPUT TYPE="hidden" NAME="frmFlagTypeID" VALUE="#getFlagGroup.FlagTypeID#">
		<CF_INPUT TYPE="hidden" NAME="frmdataTableFullName" VALUE="#getFlagGroup.flagType.dataTableFullName#">
		<input type="hidden" name="frmProfileID" value="0">
		

<CFLOOP INDEX="QueryNumber" FROM="1" TO="1" STEP="1">
	<CFSET counttitle = evaluate("title#QueryNumber#")>
	<CFSET Grandtotal = evaluate("total#querynumber#")>

	<tr>
		<td colspan="2">
			Click on each attribute to go to the entities with this profile set.<BR>
			&nbsp;
		</td>
	</tr>

	<CFIF Query1.recordcount EQ 0>
		<TR><TD>There is no data for this Profile</TD></TR>
		</Table>
		<CF_ABORT>
	</CFIF>
	
	<TR>
		<TH>#htmleditformat(FlagGroupName)#</TH>
		<TH>Count </TH>
		<TH>Percentage</TH>
		<TH>By Country</TH>
	</TR>
	
	<cfquery name="qFlagIDList" dbtype="query">
		select flagID from query#queryNumber#
		where flagID is not null
	</cfquery>
	<cfset flagIDList = valueList(qFlagIDList.FlagID)>
	<CF_INPUT type="hidden" name="frmFlagIDList" value="#flagIDList#">
	<CFLOOP QUERY="query#querynumber#">
		<TR>
			<TD WIDTH="50%">
			<B>
			<CFIF name neq "No Data" and listfindNoCase(supportedEntities,getFlagGroup.entityType.tableName) gt 0>
				<CFIF not request.relayCurrentUser.isInternal >
					<A HREF="JavaScript:submitExternal('#FlagID#')" TITLE="View Organisations at #name#">#htmleditformat(name)#</A>
				<CFELSE>
					<A HREF="JavaScript:viewDetail('#FlagID#')">#htmleditformat(name)#</A>
				</CFIF>
			<cfelse>
			#htmleditformat(name)# (<a href="profileReportHeader.cfm?flagGroupID=#flagGroupID#&nodata=false&uniqueFrameID=#uniqueFrameID#">view only with data</a>)
			</CFIF>
			</B>
			</TD>
			<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(total)#</TD>
			<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(round(Fraction))# %</TD>
			<td align="center"><a href="profileCountsByCountry.cfm?flagID=#flagID#" target="reportDetail#uniqueFrameID#">View</a></td>
		</TR>
	</CFLOOP>
	<TR>
		<TD></TD>
		<TD COLSPAN="2" ALIGN="center"><hr width="100%" size="1" color="##000080"></TD>
	</TR>
	<TR>
		<TD WIDTH="50%"><DIV align=right>Total:</DIV></TD>
		<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(Grandtotal)#</TD>
		<TD WIDTH="25%"></TD>
	</TR>
</TABLE>
</CFLOOP>

</FORM>

<cfwddx action="CFML2WDDX" input="#query1#" output="querypacket">
<cflock timeout="5" throwontimeout="No" type="EXCLUSIVE" scope="SESSION">
	<cfset session.WDDXQueryPacket = querypacket>
</cflock>
</cfoutput>