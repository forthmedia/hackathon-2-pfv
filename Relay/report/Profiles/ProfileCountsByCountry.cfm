<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			profileCountsByCoutry.cfm	
Author:				SWJ
Date started:		2006-12-20
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-02-13 SWJ	Tweaked the output
2007-03-13 WAB  
2012-06-28 PPB 	Case 428165 switched HidePageControls from yes to no (else we only see the first 50 rows)
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:

 --->
<cfparam name="flagID" type="numeric">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="organisations desc">

<cfset getflag = application.com.flag.getFlagStructure(flagID)>


	<cf_title>Profile Counts by Country</cf_title>
	
	<CF_RelayNavMenu pageTitle="Counts of Profiled Records In The Database By Country" thisDir="/report/profiles">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?openAsExcel=true&flagID=#flagID#">
	</CF_RelayNavMenu>


<CFIF getflag.flagid IS 0>
	<H2>Not a valid flag group</H2>
	<CF_ABORT>
<CFELSE>
	<CFSET FlagName = GetFlag.Name>
</CFIF>


<cfset entityCountryID = "CountryID">
<cfset entityType = getflag.entityType.tableName>
<cfif entityType eq "organisation">
	<cfset entityCountryID = "orgCountryID">
</cfif>


<cfquery name="initialiseCountsByCountry" datasource="#application.siteDataSource#" 
	cachedwithin="#createTimeSpan(0,0,20,0)#">
	select distinct c.CountryDescription as country, c.countryID,
		(select distinct count(distinct organisation.organisationID) from 
			<cfif entityType eq "organisation">
				organisation
			<cfelseif entityType neq "organisation">
				vPeople inner join organisation on organisation.organisationID = vPeople.organisationID
			</cfif>
			inner join #getflag.flagType.dataTableFullname# flagType ON flagType.entityID =  #getflag.entityType.uniqueKey#
			where organisation.countryID = c.countryID and flagType.flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > ) as organisations,
		(select distinct count(distinct vPeople.locationID) from vPeople
			inner join #getflag.flagType.dataTableFullname# flagType ON flagType.entityID =  #getflag.entityType.uniqueKey#
			where vPeople.#entityCountryID# = c.countryID and flagType.flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > ) as locations,
		(select distinct count(distinct personID) from vPeople  
			inner join #getflag.flagType.dataTableFullname# flagType ON flagType.entityID =  #getflag.entityType.uniqueKey#
			where vPeople.#entityCountryID# = c.countryID and flagType.flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > ) as people
		from organisation o with (noLock)
		<!--- NJH Bug Fix T-10 Issue 751 - tied the country to a location when it's a person/location profile; else the country is the org country --->
		<cfif entityType neq "organisation">
			inner join location l on o.organisationID = l.organisationID
			inner join country c with (noLock) ON c.countryID = l.countryID
		<cfelse>
			inner join country c with (noLock) ON c.countryID = o.countryID
		</cfif>
	order by organisations desc 
</cfquery>

<cfquery name="countsByCountry" dbtype="query">
	select *
		from initialiseCountsByCountry
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<p align="center"><strong><cfoutput>#htmleditformat(FlagName)#</cfoutput> Records by Country</strong></p>
<cfif IsQuery(countsByCountry)>

<!--- <cfset keyColumnURLString = getflag["entityTypeID"] --->

	<CF_tableFromQueryObject queryObject="#countsByCountry#" 
	
	showTheseColumns="Country,Organisations,Locations,People"
	
	keyColumnList="Country"
	keyColumnURLList="/data/flagOrgs.cfm?frmFlagIDfromFlagCount=#flagID#&frmdataTableFullName=#getflag["flagType"]["dataTableFullName"]#&frmEntityType=#getFlag["entityType"]["tablename"]#&FilterSelect1=country&FilterSelectValues1="  <!--- NJH 2007-02-23 changed filterSelect1 from countryDescription to country --->
	keyColumnKeyList="country"

	
	HidePageControls="no"		<!--- 2012-06-28 PPB Case 428165 switched HidePageControls from yes to no (else we only see the first 50 rows)  --->
	useInclude = "false"
	
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#">
</cfif>
