<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File:			FlagGroupCounts.cfm
Author:			SWJ
Date created:	Oct 99

Description:
	Reports on a flagGroup giving a count of records for the whole flag group


Version history:

2001-09-24	SWJ		Fixed the two main queries to be generic
2001-07-17	SWJ		Ammended to go to orgListing screen
2-Mar-2000 SWJ updated Amended to support country filter
2002-03-02	SWJ		Ammended to support drill through to org,loc & person entities more tightly with orgListv3.cfm.
					Corrected a bug which always queried booleanFlagData irrespective of the flagType 

--->
<cfinclude template="/templates/qrygetcountries.cfm">
<CFPARAM NAME="frmcountryid" DEFAULT="#CountryList#">
<CFPARAM NAME="flagid" DEFAULT=0>
<!--- supportedEntities defines the flagEntityTypes that will support drill through to the dataFrame (and orglistV3) --->
<cfset supportedEntities = "person,location,organisation">

<CFIF NOT isDefined("flaggroupid")>
	<H2>You must define a Flag Group for this report to run</H2>
	<CF_ABORT>
</CFIF>
<!--- Get FlagGroup name for use throughout report --->
<CFQUERY NAME="GetFlagGroup" datasource="#application.sitedatasource#">
	SELECT fg.name, entityName, UniqueKey, ft.dataTableFullName
		from flagGroup fg 
		inner join schemaTable st on fg.entityTypeID = st.entityTypeID
		inner join flagType ft on fg.flagTypeid = ft.flagTypeid
	where flaggroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<CFIF GetFlagGroup.recordCount IS 0>
	<H2>Not a valid flag group</H2>
	<CF_ABORT>
<CFELSE>
	<CFSET FlagGroupName =  #GetFlagGroup.Name#>
</CFIF>

		<CFIF not request.relayCurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    <CFIF UserType is "External"> --->

<!--- Get GrandTotal of records for flagGroup--->
<CFQUERY NAME="Query1Total" datasource="#application.sitedatasource#">
	SELECT COUNT(fd.entityid) AS Total
		FROM  #getFlagGroup.dataTableFullName# fd
		INNER JOIN flag f 
		ON f.flagid = fd.flagid 
		WHERE
			F.name is not null
			AND F.name <> 'suspect'
			AND F.FlagGroupID =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND F.Active = 1
</CFQUERY>
<CFELSE>
<CFQUERY NAME="Query1Total" datasource="#application.sitedatasource#">
	SELECT COUNT(e.#getFlagGroup.UniqueKey#) AS Total
		FROM #getFlagGroup.entityName# e 
</CFQUERY>
</CFIF>
<CFSET Total1 = #Query1Total.total#>
<CFSET Title1 = "for all delegates">
<CFIF Query1Total.Total GT 0>
			<CFIF not request.relayCurrentUser.isInternal> 
		<CFQUERY NAME="Query1" datasource="#application.sitedatasource#">
			SELECT 
			Flag.Name,
				Flag.FlagID, Flag.orderingIndex,
				Count(fdt.entityid) AS Total,
				Fraction=cast(((Convert(Real, Count(fdt.entityid))/#Total1#)*100) as int)
			FROM #getFlagGroup.dataTableFullName# fdt 
			RIGHT OUTER JOIN Flag ON fdt.FlagID = Flag.FlagID 
			WHERE 1=1
			AND Flag.name is not null
			AND Flag.name <> 'suspect'
			AND Flag.FlagGroupID =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND Flag.Active = 1
			GROUP BY Flag.orderingIndex, Flag.Name, flag.FlagID
			ORDER by Flag.orderingIndex
		</CFQUERY>
	<CFELSE>
		<CFQUERY NAME="Query1" datasource="#application.sitedatasource#">
			SELECT 
			isNULL(Flag.Name,'No Data') as Name, 
				Flag.FlagID, Flag.orderingIndex,
				Count(e.#getFlagGroup.UniqueKey#) AS Total,
				Fraction=cast(((Convert(Real, Count(e.#getFlagGroup.UniqueKey#))/#Total1#)*100) as int)
			FROM #getFlagGroup.dataTableFullName# fdt 
			INNER JOIN Flag ON fdt.FlagID = Flag.FlagID and Flag.FlagGroupID =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			RIGHT OUTER JOIN #getFlagGroup.entityName# e ON fdt.EntityID = e.#getFlagGroup.UniqueKey#
			GROUP BY Flag.orderingIndex, Flag.Name, flag.FlagID
			ORDER by Flag.orderingIndex
		</CFQUERY>
	</CFIF>
</CFIF>



<cf_head>
	<cf_title><CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></cf_title>
	<SCRIPT type="text/javascript">
        function viewDetail(ID) {
                var form = document.MainForm;
                form.frmFlagIDfromFlagCount.value = ID;
                form.submit();
                return;
        }
		function submitExternal(ID) {
			document.FG_Form.FlagID.value = ID
			document.FG_Form.submit()
		}
	</SCRIPT>
</cf_head>


<FORM ACTION="FlagGroupCounts.cfm">
<cf_displayValidValues 
	formFieldName = "frmcountryid" 
	validvalues= "#findCountries#" 
	dataValueColumn = "countryid"
	displayValueColumn = "countryDescription"
	nulltext="Please select a country" 
	method = "view"
	onChange = "submit()"
	currentValue = #frmcountryID#
	>
<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="flaggroupid" VALUE="#flaggroupid#"></CFOUTPUT>
</FORM>
<CFOUTPUT>
<form action="/data/dataFrame.cfm" method="post" name="MainForm" target="main">
	
		<INPUT TYPE="hidden" NAME="frmFlagIDfromFlagCount" VALUE="0">
		<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#getFlagGroup.entityName#">
		<CF_INPUT TYPE="hidden" NAME="frmdataTableFullName" VALUE="#getFlagGroup.dataTableFullName#">
	</CFOUTPUT>
<CFLOOP INDEX="QueryNumber" FROM="1" TO="1" STEP="1">
	<CFSET counttitle = evaluate("title#QueryNumber#")>
	<CFSET Grandtotal = evaluate("total#querynumber#")>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<tr><td class="Submenu" COLSPAN="2">Analysis of '<CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT>' Profile </td>
			<TD ALIGN="right" CLASS="Submenu">
				<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
			</TD>
		</tr>

		<tr>
			<td colspan="2">
				Click on each attribute to go to the entities with this profile set.<BR>
				&nbsp;
			</td>
		</tr>

	<CFIF Query1Total.Total EQ 0>
		<TR><TD>There is no data for this Profile</TD></TR>
		</Table>
		<CF_ABORT>
	</CFIF>
	
	<TR>
		<TH><CFOUTPUT>#htmleditformat(FlagGroupName)#</CFOUTPUT></TH>
		<TH>Count </TH>
		<TH>Percentage</TH>
	</TR>
	<CFOUTPUT QUERY="query#querynumber#">
		<TR>
			<TD WIDTH="50%">
			<B>
			<CFIF name neq "No Data" and listfindNoCase(supportedEntities,getFlagGroup.entityName) gt 0>
				<CFIF not request.relayCurrentUser.isInternal> 
					<A HREF="JavaScript:submitExternal('#FlagID#')" TITLE="View Organisations at #name#">#htmleditformat(name)#</A>
				<CFELSE>
					<A HREF="JavaScript:viewDetail('#FlagID#')">#htmleditformat(name)#</A>
				</CFIF>
			<cfelse>
			#htmleditformat(name)#
			</CFIF>
			</B>
			</TD>
			<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(total)#</TD>
			<TD WIDTH="25%" ALIGN="CENTER">#htmleditformat(round(Fraction))# %</TD>

		</TR>
	</CFOUTPUT>
	<TR>
		<TD></TD>
		<TD COLSPAN="2" ALIGN="center"><hr width="100%" size="1" color="#000080"></TD>
	</TR>
	<TR>
		<TD WIDTH="50%"><DIV align=right>Total:</DIV></TD>
		<TD WIDTH="25%" ALIGN="CENTER"><CFOUTPUT>#htmleditformat(Grandtotal)#</CFOUTPUT></TD>
		<TD WIDTH="25%"></TD>
	</TR>

	<CFIF not request.relayCurrentUser.isInternal and FlagID NEQ 0>
	<TR><TD>&nbsp;</TD></TR>
	<TR>
	<TD colspan=3>
	<CFQUERY NAME="ReportFlag" DBTYPE="query">
		SELECT name from query1 where flagid = #flagid#
	</CFQUERY>
	<DIV ALIGN="center">
	<CFOUTPUT>Organisations at #htmleditformat(ReportFlag.name)# on Recruitment Program #htmleditformat(FlagGroupName)#</CFOUTPUT>
	</DIV>
	</TD>
	</TR>
	<TR>
	<TD colspan=3>
	<CF_RelayXMLReport
		Reportname = "RecruitmentOrgs"
		FilterWhereClause = "f.flagID = #flagid#"
		FormFieldNames= "#FieldNames#"
	>
	</TD>
	</TR>
	</CFIF>

	
 	<TR>
		<TD COLSPAN="3">
		<cf_chart
           format="flash"
           chartheight="350"
           chartwidth="550"
           seriesplacement="default"
           labelformat="number"
           show3d="yes"
           tipstyle="mouseOver"
           pieslicestyle="sliced">
		   	<cf_chartseries
             type="pie"
             query="query#querynumber#"
             itemcolumn="Name"
             valuecolumn="Fraction"></cf_chartseries>
		</cf_chart>
		</TD>
	</TR> 
 	<TR>
		<TD COLSPAN="3">NB: fractions of percentage figures are rounded down and hence may not always sum to 100%</TD>
	</TR> 
	</TABLE>
</CFLOOP>

</FORM>

<!--- <!---Catch the New User for the Pie Copying--->
<CFSET Usr = #request.relayCurrentUser.usergroupid#>


<!---On the First Hit Delete the previously formed gif file by the same user after checking for it's existence--->

<CFDIRECTORY ACTION="LIST" DIRECTORY="#Path#\TempGraphics" FILTER="#Usr#_*.gif" NAME="MyFiles">

<CFIF MyFiles.RecordCount NEQ 0>
	<CFLOOP QUERY="MyFiles">
		<CFIF fileExists("#Path#\TempGraphics\#Name#")>
			<CFFILE ACTION="DELETE" FILE="#Path#\TempGraphics\#Name#">
		</cfif>
	</CFLOOP>
</CFIF>
 --->
<!---  <CFIF Query1.RecordCount IS 0>
	No data present for this analysis. Please try changing Search Criteria.
<CFELSE>
	
	<!---Define the pie--->	
	<CFOUTPUT>
		<CFSET TimeStamp = TimeFormat(Now(), "hhmmss")>
		
		<CFX_GIFGD ACTION="DRAW_PIE"
			OUTPUT="#path#\TempGraphics\#Usr#_#TimeStamp#.gif"
			LEGENDS="#ValueList(Query1.Name)#"
			DATA="#ValueList(Query1.Fraction)#"
			SIZE="275"
			MAXROWS="15"
			HEADER="Analysis of #FlagGroupName#"
			FOOTER= "Date: #DateFormat(Now(),'dd-mmm-yyyy')#, #TimeFormat(Now(),'hh:mm tt')#">
		<IMG SRC="\TempGraphics\#Usr#_#TimeStamp#.gif" BORDER="0">
	</CFOUTPUT>
</CFIF>  --->

<!--- <table>
	<tr>
	<td>
		<cfoutput>
			Drill through supported: #yesNoFormat(listfindNoCase(supportedEntities,getFlagGroup.entityName))# <br>
			Entity Name: #getFlagGroup.entityName#
		</cfoutput>
	</td>
	</tr>
</table> --->







