<!--- �Relayware. All Rights Reserved 2014 --->

<CFQUERY NAME="GetEntityTypes" datasource="#application.sitedatasource#">
	SELECT dbo.flagEntityType.entitytypeid, dbo.flagEntityType.tablename, 
	COUNT(DISTINCT dbo.FlagGroup.FlagGroupID) AS CountFlagGroups, 
	dbo.flagGroup.ParentFlagGrouPID,
	    COUNT(dbo.flag.FlagID) AS CountFlags
	FROM dbo.flagEntityType INNER JOIN
	                      dbo.FlagGroup ON dbo.flagEntityType.entitytypeid = dbo.FlagGroup.EntityTypeID INNER JOIN
	                      dbo.flag ON dbo.FlagGroup.FlagGroupID = dbo.flag.FlagGroupID
	GROUP BY dbo.flagEntityType.entitytypeid, dbo.flagEntityType.tablename,dbo.flagGroup.ParentFlagGrouPID
</CFQUERY>



<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD COLSPAN="3">
		<P>The list below shows those tables that currently have flag groups 
		defined for them.  Click on any name to view various flag reports.</P>
		</TD>
	</TR>
	<TR>
		<TH>Flag groups<BR>(Click to viw)</TH>
		<TH>Number of Flag Groups</TH>
		<TH>Number of Flags</TH>
	</TR>

<CFOUTPUT QUERY="GetEntityTypes">
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD>
			<A HREF="FlagGroupList.cfm?frmEntityTypeID=#entitytypeid#" CLASS="smallLink" TITLE="Flag group ID #entitytypeid#">
			#htmleditformat(tablename)#</A>
		</TD>
		<TD ALIGN="center">#htmleditformat(CountFlagGroups)#</TD>
		<TD ALIGN="center">#htmleditformat(CountFlags)#</TD>

	</TR>
</CFOUTPUT>
</TABLE>
<CFINCLUDE TEMPLATE="../../templates/InternalFooter.cfm">


