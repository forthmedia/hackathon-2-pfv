<!--- �Relayware. All Rights Reserved 2014 --->
	<!--- 
File name:			.tablefromqueryObject.cfc
Author:				WAB
Date started:			2008/03/01
	
Description:			
	Started as a function for converting a query into a crosstab query
	expanded into a cfc from which to run cf_tableFromQueryObjectV2

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-04-30		WAB		Fixed crosstab bug which occurred when data query needed to be sorted

Possible enhancements:


 --->

<cfcomponent displayname="tableFromQueryObject" hint="">


	<cfset this.groupColStruct=structNew()>
	<cfset this.reloadStructures = true>   <!--- used to signify that structures need to be reloaded --->
	<cfset this.initialisingDone = false>   

	<cfset variables.aList="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z">

	<cfset variables.renderers = structNew()>
	
<cffunction name="initialise" returntype="boolean" hint="Processes all the lists into a structure">


			<cfargument name="queryObject" type="query" required = yes>
			<cfargument name="reportTitle" type="string" default="">
			<cfargument name="showTheseColumns"  type="string" default=""> 			

			<cfargument name="hideTheseColumns" default="">  <!--- only operates if show htese columns not passed in  --->

			<!--- headings --->
			<cfargument name="columnHeadingList"  type="string" default = "#showTheseColumns#"> 
			<cfargument name="columnTranslation"  type="boolean" default = false> 
			<cfargument name="columnTranslationPrefix"  type="string" default = "phr_Sys_Report_"> 		

			<cfargument name="columnWidthList"  type="string" default = ""> 		<!--- for ext --->

			<cfargument name="allowColumnSorting"  type="boolean" default = "yes"> 		
			<cfargument  name="sortOrder" default="">
			<cfargument  name="showCellColumnHeadings" type="boolean" default="no">  <!--- headings pop up over table cells --->

			
			<!--- Formatting Arguments --->
			<cfargument name="numberFormat"  type="string" default=""> 
			<cfargument name="numberFormatMask"  type="string" default=""> 
			<cfargument name="dateFormat"  type="string" default=""> 			
			<cfargument name="dateFormatMask"  type="string" default=""> 			
			<cfargument name="currencyFormat"  type="string" default=""> 			
			<cfargument name="IllustrativePriceColumns"  type="string" default=""> 			
			<cfargument name="calculatedColumns"  type="string" default=""> 			
			
			<!--- Grouping and summing --->
			<cfargument name="GroupByColumns"  type="string" default=""> 			
			<cfargument name="GroupByOrderColumns"  type="string" default="#GroupByColumns#">
			<cfargument  name="averageTheseColumns" default="">
			<cfargument  name="totalTheseColumns" default="">
			<cfargument name="OrderByColumns" type="string" default="">  
		

			<!---  key Columns--->
			<cfargument name="keyColumnList"  type="string" default=""> 			
			<cfargument name="keyColumnURLList"  type="string" default=""> 			
			<cfargument name="keyColumnKeyList"  type="string" default=""> 			
			<cfargument name="keyColumnTargetList"  type="string" default=""> 			
			<cfargument name="keyColumnOnClickList"  type="string" default=""> 			
			<cfargument name="keyColumnOpenInWindowList"  type="string" default=""> 			
			<cfargument name="OpenWinNameList"  type="string" default=""> 			
			<cfargument name="OpenWinSettingsList"  type="string" default=""> 			
			<cfargument name="keyColumnIconList"  type="string" default=""> 			
			<cfargument name="keyColumnContextMenuList"  type="string" default=""> 			
			
				<cfargument name="openAsExcel" type="boolean" default="false">
				<cfargument name="saveExcel" type="boolean" default="false">   <!--- wab wanting to save excel file --->
				<cfargument name="outputType" type="string" default="HTMLTable">  <!--- or ext --->

				<cfargument name="passThroughVariablesStructure" type="struct" default="#StructNew()#">

				
				<cfargument  name="tableClass" default="withBorder">
				<cfargument  name="tableAlign" default="center">
				<cfargument  name="HidePageControls" type="boolean" default="no">
				<cfargument  name="numRowsPerPage" type="numeric" >  <!--- don't default - we check for existence later --->
					
				<cfargument  name="functionListQuery" type="query" default="#QueryNew( "functionID,functionName,securityLevel,url,windowFeatures" )#">
				<cfargument  name="showFunctionList" type="boolean" default="#Evaluate( "functionListQuery.RecordCount gt 0" )#">
				<cfargument  name="FunctionListComment" default="no">
			
				<!---  Filtering--->
				<cfargument  name="filterqueryObject" default="#arguments.queryObject#">



				<cfargument name="radioFilterLabel" type="string" default="">
				<cfargument name="radioFilterDefault" type="string" default="">
				<cfargument name="radioFilterName" type="string" default="">
				<cfargument name="radioFilterValues" type="string" default="">
				
				
				<cfargument name="FilterSelectFieldList" type="string" default="">  <!--- WAb for backwards compatability --->
				<cfargument name="FilterSelectFieldList1" default= #FilterSelectFieldList#>  
				<cfargument name="FilterSelectFieldList2" type="string" default="">
				<cfargument name="FilterSelectFieldList3" type="string" default="">
				
				<cfargument name="filterSelectValue" type="string" default="">
				<cfargument name="filterSelectValue2" type="string" default="">
				<cfargument name="filterSelectValue3" type="string" default="">
				
				<cfargument name="checkBoxFilterName" type="string" default="">
				<cfargument name="checkBoxFilterLabel" type="string" default="">
				<cfargument name="useTeams"  default="false">
				

				<!--- --------------------------------------------------------------------- --->
				<!--- text search --->
				<!--- --------------------------------------------------------------------- --->
				<cfargument name="searchColumnList" type="string" default="">
				<cfargument name="searchColumnDisplayList" type="string" default="#searchColumnList#">
				<cfargument name="searchColumnDataTypeList" type="string" default="">
				
				<!--- --------------------------------------------------------------------- --->
				<!--- query where clause --->
				<!--- --------------------------------------------------------------------- --->
				<cfargument name="queryWhereClauseStructure" type="struct" default="#StructNew()#">

				<cfargument name="id" default = "aReport#timeformat(now(),'HHmmssl')#">  <!--- id required for drilldown incase more than one report on a page --->
				
				<cfargument name="alphabeticalIndexColumn" type="string" default="">
	
				<cfset searchComparitorList="Phr_Sys_search_none,Phr_Sys_search_equals,Phr_Sys_search_like,Phr_Sys_search_greater,Phr_Sys_search_less">
				<cfloop index="searchColumn" from="#Evaluate( "ListLen( searchColumnDisplayList ) + 1" )#" to="#ListLen( searchColumnList )#">
					<cfset arguments.searchColumnDisplayList = ListAppend(searchColumnDisplayList, ListGetAt( searchColumnList, searchColumn ) )>
				</cfloop>
				<cfloop index="searchColumn" from="#Evaluate( "ListLen(arguments.searchColumnDataTypeList ) + 1" )#" to="#ListLen( arguments.searchColumnList )#">
					<cfset arguments.searchColumnDataTypeList = ListAppend( arguments.searchColumnDataTypeList, 0 )>
				</cfloop>


				<cfparam name="form.frmAlphabeticalIndex" type="string" default="">
				<cfparam name="form.frmAlphabeticalIndexList" type="string" default="">
				<cfset arguments.hideTheseColumns = ListAppend( arguments.hideTheseColumns, "alphabeticalIndex" )>
				

				<cfif openAsExcel><cfset this.outputType = "Excel"></cfif>
<!--- --------------------------------------------------------------------- --->

<!--- --------------------------------------------------------------------- --->
<!--- query where clause --->
<!--- --------------------------------------------------------------------- --->

<!--- 
<cfif structKeyExists(caller,"queryWhereClause")>
	<cfset attributes.queryWhereClauseStructure = caller["queryWhereClause"]>
</cfif>
 --->
<cfparam name="form.frmWhereClauseList" type="string" default="">

<cfparam name="attributes.selectbyday" type="boolean" default="false">





<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
<cfparam name="attributes.IllustrativePriceColumns" type="string" default=""> <!--- columns that require illustrative pricing to be shown next to them --->
<cfparam name="attributes.IllustrativePriceFromCurrency" type="string" default=""> <!--- conversion from currency ISOCode --->
<cfparam name="attributes.IllustrativePriceToCurrency" type="string" default=""> <!--- conversion to currency ISOcode --->
<cfparam name="attributes.IllustrativePriceDateColumn" type="string" default=""> <!--- query column to use for date of conversion--->
<cfparam name="attributes.IllustrativePriceMethod" type="string" default="2"> <!--- method for getting the price --->



			<!--- puts all arguments into THIS scope --->
			<cfloop item = "field" collection = #arguments#>
				<cfset this[field] = arguments[field]>
			</cfloop>

				
				<!--- attributes.maxrows shows the maximum number of rows of data to display per page --->
				<!--- WAB added 2005-10-26  if numRowsPerPage is a url or form variable (ie past from a previous page) then use it as long as attributes.numRowsPerPage is not being used--->
				<cfif not isDefined("this.numRowsPerPage") >
					<cfif isDefined("numRowsPerPage")>
						<cfset this.numRowsPerPage = numRowsPerPage>
					<cfelse>
						<cfset this.numRowsPerPage ="50">		
					</cfif>	
				</cfif>

				<cfif not isnumeric(this.numRowsPerPage)>
					<cfset this.numRowsPerPage=50>
					<cfset errors=errors&"A non numeric value was entered for numRowsPerPage, 50 has been used">
				</cfif>

					<!--- define start and end rows of data to display --->
					<!--- WAB added 2005-10-26 if startrow is a url or form variable (ie past from a previous page) then use it as long as attributes.startrow is not being used--->
					<cfif not isDefined("this.startrow") and  isDefined("startrow")>
						<cfset this.startrow = startrow>
					</cfif>
					<cfparam name="this.startrow" type="numeric" default="1">
					<CFPARAM name="this.endrow" type="numeric" default="1">

				<cfif this.showTheseColumns is "">
					<cfset this.showTheseColumns = this.queryObject.columnList>
						<!--- WAB 2006-11-21 hidethese columns wasn't working --->
					<cfloop index="hideCol" list ="#this.hidethesecolumns#">
						<cfset pos = listFindNoCase(this.showTheseColumns,hidecol)>
						<cfif pos is not 0>
							<cfset this.showTheseColumns = listdeleteat(this.showTheseColumns,pos)>
						</cfif>
					</cfloop>

				</cfif>

					


		<cfif this.columnHeadingList is "">
			<cfset this.columnHeadingList = this.showTheseColumns>
		</cfif>
		
		<cfif this.sortOrder is "" and isDefined("form.sortOrder")>
			<cfset this.sortOrder = form.sortOrder> <!--- WAB added, one less parameter to pass in - although won't pick up the initial sort order---> 
		</cfif>


	

		<!--- --------------------------------------------------------------------- --->
		<!--- paging and filtering --->
		<!--- --------------------------------------------------------------------- --->
		
		<cfparam name="form.frmSearchColumnDataType" type="string" default="">
		<cfparam name="form.frmSearchText" type="string" default="">
		<cfparam name="form.frmSearchColumn" type="string" default="">
		<cfparam name="form.frmSearchComparitor" type="string" default="">
		<cfparam name="form.frmWhereClauseList" type="string" default="">
		<cfparam name="form.filterselect" type="string" default="">
		<cfparam name="form.filterselectvalues" type="string" default="">
		<cfparam name="form.filterselect1" type="string" default="">
		<cfparam name="form.filterselectvalues1" type="string" default="">
		<cfparam name="form.filterselect2" type="string" default="">
		<cfparam name="form.filterselectvalues2" type="string" default="">
		<cfparam name="form.filterselect3" type="string" default="">
		<cfparam name="form.filterselectvalues3" type="string" default="">
		<cfparam name="form.frmAlphabeticalIndex" type="string" default="">
		<cfparam name="form.frmAlphabeticalIndexList" type="string" default="">
		





		<cfparam name="attributes.IllustrativePriceColumns" type="string" default=""> <!--- columns that require illustrative pricing to be shown next to them --->
		<cfparam name="attributes.IllustrativePriceFromCurrency" type="string" default=""> <!--- conversion from currency ISOCode --->
		<cfparam name="attributes.IllustrativePriceToCurrency" type="string" default=""> <!--- conversion to currency ISOcode --->
		<cfparam name="attributes.IllustrativePriceDateColumn" type="string" default=""> <!--- query column to use for date of conversion--->
		<cfparam name="attributes.IllustrativePriceMethod" type="string" default="2"> <!--- method for getting the price --->
			
		
		



		
		<cfif listLen(this.showTheseColumns) is not listLen(this.columnHeadingList)>
			<cfoutput>phr_Ext_tableQuery_ColumnHeadingListError <CF_ABORT></cfoutput>
		</cfif>

		<cfset initialiseStructures ()>
		<cfset this.initialisingDone = true>   
		
			<cfreturn true>
</cffunction>


<cffunction name="initialiseStructuresIfRequired" returntype="boolean" hint="creates control structures if needed" >
	<cfif this.reloadStructures>
		<cfset initialiseStructures()>
		<cfreturn true>
	<cfelse>	
		<cfreturn false>
	</cfif>
</cffunction>


<cffunction name="initialiseStructures" returntype="boolean" hint="creates control structures" >

		<cfset colStruct = structNew()>  <!--- just a pointer for ease of use --->

		<cfloop list = "#this.showTheseColumns#,#this.GroupByColumns#,#this.numberformat#,#this.dateFormat#,#this.currencyFormat#,#this.IllustrativePriceColumns#,#this.keyColumnList#" index="col"><!--- make sure that that we have got all the columns! --->
			<cfset colStruct[col] =  {hasanchor = false,cellType = "",align = "left",Renderer = defaultFormatter,extRenderer = "",RenderArguments = structNew(),width = "",Total = false}>
		</cfloop>
		
		<!--- Loop through all the lists which are matched in length to  #this.showTheseColumns# --->
		<cfloop index = "columnIndex" from = 1 to = "#listlen(this.showTheseColumns)#">
			<cfset col = listGetAt(this.showTheseColumns, columnIndex)>
			<cfif listLen(this.columnWidthList) gte columnIndex>
				<cfset colStruct[col].width = listGetAt(this.columnWidthList, columnIndex)>
			</cfif>
			<cfif listLen(this.columnHeadingList) gte columnIndex>
				<cfset thisHeading = listGetAt(this.columnHeadingList, columnIndex)>
				<cfif this.columnTranslation><!--- add phrase prefixes to headings, need to deal with items delimited by #application.delim1# --->
					<cfset tempHeading = "">
					<cfloop list = "#thisHeading#" index="i" delimiters = "#application.delim1#">
						<cfset tempHeading = listappend (tempHeading,"#this.columnTranslationPrefix##I#","#application.delim1#")>
					</cfloop>
					<cfset thisHeading = tempHeading>
				</cfif>
				<cfset colStruct[col].heading = thisHeading>
			</cfif>
		
		</cfloop>		
		
		
		
		<cfset counter = 0>
		<cfloop list = "#this.numberformat#" index="col">
			<cfset counter = counter + 1>
			<cfset colStruct[col].align = "right">
			<cfset colStruct[col].cellType = "number">
			<cfset colStruct[col].Renderer = numberFormatter>
			<cfif listLen(this.numberformatMask) gte counter>
				<cfset colStruct[col].RenderArguments.mask = listgetat(this.numberformatMask,counter)>
			<cfelse>
				<cfset colStruct[col].RenderArguments.mask = "0">
			</cfif>
			
			<!--- add excel class stuff here --->
		</cfloop>

		<cfset counter = 0>
		<cfloop list = "#this.dateFormat#" index="col">
			<cfset counter = counter + 1>
			<cfset colStruct[col].cellType = "date">
			<cfset colStruct[col].Renderer = dateFormatter>
			<cfif listLen(this.dateformatMask) gte counter>
				<cfset colStruct[col].RenderArguments.datemask = listfirst(listgetat(this.dateformatMask,counter),"|")>
				<cfif listLen(listgetat(this.dateformatMask,counter),"|") gt 1>
					<cfset colStruct[col].RenderArguments.timemask = listrest(listgetat(this.dateformatMask,counter),"|")>
				</cfif>
				
			<cfelse>
				<cfset colStruct[col].RenderArguments.datemask = "medium">
				<cfset colStruct[col].RenderArguments.timemask = "">			
			</cfif>


			<cfset colStruct[col].extRenderer = "fnlExtDateRenderer('#getExtDateMask(colStruct[col].RenderArguments.datemask, colStruct[col].RenderArguments.timemask)#')">  <!---  Ext.util.Format.dateRenderer need to link to user's date format ---> 
			
<!--- 			<cfset colStruct[col].Renderer = variables.renderers.dateFormatter>
 --->			
		</cfloop>

		<cfloop list = "#this.currencyFormat#" index="col">
			<cfset colStruct[col].Renderer = currencyFormatter>
			<cfset colStruct[col].cellType = "currency">
			<cfset colStruct[col].extRenderer = "Ext.util.Format.usMoney">  <!--- --->
			
		</cfloop>
		
		<cfloop list = "#this.TotalTheseColumns#" index="col">
			<cfset colStruct[col].total = true>
		</cfloop>

		<cfloop list = "#this.calculatedColumns#" index="colAndExpr" delimiters = "|">
			<cfif listLen(colAndExpr,"=") gte 2>
				<cfset colStruct[listFirst(colAndExpr,"=")].eval = listRest(colAndExpr,"=")>
			</cfif>
			
		</cfloop>



		<cfif this.keyColumnList is not "">
			<cfloop list = "#this.keyColumnList#" index="col">
				<!--- first extract info from each list and pop in structure --->
				<cfset i= listfindNoCAse(this.keyColumnList,col)>
				<cfset keyStruct= structNew()>
				<cfloop list="keyColumnKeyList,keyColumnURLList,keyColumnTargetList,keyColumnOpenInWindowList,OpenWinNameList,OpenWinSettingsList,keyColumnIconList,keyColumnContextMenuList,keyColumnOnClickList" index="x">
					<cfset property = replace(replace (x,"keyColumn",""),"List","")> <!--- Gives  properties called --->

					<cfif listlen(this[x]) is listlen(this.keyColumnList)>
						<cfset propertyValue = listgetat(this[x],i)>
						<cfif propertyValue is "null" or propertyValue is "no"  >  <!--- various different ways of putting blankin a list --->
							<cfset KeyStruct[property] = "">
						<cfelse>
							<cfset KeyStruct[property] = propertyValue>
						</cfif>
						
					<cfelseif this[x] is "" >
						<cfset KeyStruct[property] = "">
					<cfelse>
						<cfset KeyStruct[property] = "">
						<cfoutput>list length of #htmleditformat(x)# does not match keycolumnlist</cfoutput>
					</cfif> 				
				</cfloop>
				<cfset colStruct[col].keyCol = keyStruct	>			
				<cfset colStruct[col].hasAnchor = true>			
				
			
				<!--- --------------------------------------------------------------------- --->
<!--- substitute parameter --->
<!--- --------------------------------------------------------------------- --->
					<!--- use for more than 1 param --->
					<cfset keyStruct.URL = replacenocase(keyStruct.URL,"*comma",",","ALL")>   <!--- a hack WAB put in because he needed a comma in a URL, but couldn't get it in via the list --->
							
					<cfif listLen(keyStruct.Key,"|") gt 1>
						<cfif listLen(keyStruct.URL,"|") eq listLen(keyStruct.Key,"|")>
							<cfset tmpURL = "">
							<cfset lIncr = 1>
							<cfloop list="#keyStruct.Key#" index="item" delimiters="|">
								<cfif lIncr neq 1>
									<cfset tmpURL = tmpURL & "&" >
								</cfif>
									<cfset tmpURL = "#tmpURL##listGetAt(keyStruct.URL,lIncr,'|')####listGetAt(keyStruct.Key,lIncr,'|')###">
								<cfset lIncr = lIncr+1>
							</cfloop>
							<cfset URLToGo = tmpURL>
						<cfelse>
							<cfset URLToGo = "#listGetAt(keyStruct.URL,1,'|')####listGetAt(keyStruct.Key,1,'|')###">
						</cfif>
					<cfelse>
						<cfif FindNoCase( "thisurlkey", keyStruct.URL ) neq 0>
							<cfset URLToGo = ReplaceNoCase( keyStruct.URL, "thisurlkey", "##JSStringFormat(#keyStruct.Key#)##", "one" )>
						<cfelseif trim(keyStruct.Key) is not "">
							<cfset URLToGo = "#keyStruct.URL###URLEncodedFormat(#keyStruct.Key#)##">
						<cfelse>
							<cfset URLToGo = "#keyStruct.URL#">
						</cfif>
					</cfif>
					
 					<cfif keyStruct.OpenInWindow is not "" and keyStruct.OpenInWindow is not "no" >
						
						<cfif keyStruct.OpenWinName eq "no" or keyStruct.OpenWinName eq "">
							<cfset winName="MyWindow">
						<cfelse>
							<cfset winName=keyStruct.OpenWinName>
						</cfif>
						<cfif keyStruct.OpenWinSettings eq "no" and  keyStruct.OpenWinSettings eq "">
							<cfset winSettings="width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1">
						<cfelse>
							<cfset winSettings=keyStruct.OpenWinSettings>
						</cfif>
						
						<cfset hrefValue = "javascript:void(openWin('#URLToGo#','#winName#','#winSettings#'));">

					<cfelse>
						<cfset hrefValue = URLToGo>
					</cfif>

					<cfset keyStruct.anchorEval = '"' & hrefValue & '"' >
<!--- 					<cfoutput>keyStruct.anchorEval:#keyStruct.anchorEval#<BR></cfoutput> --->
					<cfif keyStruct.onClick is not "">
						<cfset keyStruct.onClickEval = '"' & replacenocase(keyStruct.onClick,"*comma",",","ALL") & '"'>					
					<cfelse>
						<cfset keyStruct.onClickEval ="">
					</cfif>
					

				

			</cfloop>
		</cfif>  <!--- end of KeyColumn Stuff --->

		
		
		<cfloop index="i" from = 1  to  = "#listLen(this.groupbycolumns)#">
			<cfset this.GroupColStruct [i] = listgetat(this.groupbycolumns,i)>
		</cfloop>


		<!--- set ColumnNumbers/Letters for excel use --->
			<cfset counter= 0>
		<cfloop index="col" list="#this.groupByColumns#,#this.showTheseColumns#">
			<cfset counter= counter + 1>
			<cfset colStruct[col].colNumber = counter>
			<cfif counter lte 26 >
				<cfset colStruct[col].colLetter = listgetat(variables.aList,counter)>
			<cfelse>	
				<cfset colStruct[col].colLetter = listgetat(variables.aList,int((counter -1) /26)) & listgetat(variables.aList,((counter -1)  mod 26) + 1) >
			</cfif>
		</cfloop>
		
		<cfset this.numberOfGroupByColumns = listLen(this.groupByColumns)>
		<cfset this.numberOfBodyColumns = listLen(this.showTheseColumns)>
		<cfset this.numberOfColumns = this.numberOfBodyColumns + this.numberOfGroupByColumns>
		

<!--- --------------------------------------------------------------------- --->
<!--- extra cell attributes for Excel --->
<!--- --------------------------------------------------------------------- --->
			<!--- setup currency formatting for excel --->
			<cfset currencyType = getLocale()>
			<cfswitch expression="#lcase(replaceNoCase(currencyType,' ','','all'))#">
				<cfcase value="english(us)">
					<cfset excelClass = "xl26">
				</cfcase>
				<cfdefaultcase>
					<cfset excelClass = "xl25">
				</cfdefaultcase>
			</cfswitch>


		<cfloop item="col" collection = #colStruct#>

							<cfswitch expression="#colStruct[col].cellType#">
							<!--- need a case here for type 1 - number format --->
								<cfcase value="number">
									<cfset colStruct[col].excelCellAttribute = '" x:num=""##cellValue##"""' />
								</cfcase>
								<cfcase value="currency">
									<cfset colStruct[col].excelCellAttribute = '" x:num=""##cellvalue##"" class=""#excelClass#"""' />
								</cfcase>
								<cfcase value="date">
										<cfset colStruct[col].excelCellAttribute = 'iif(IsDate(cellvalue),de(''class="xl24"''),"")' />
								</cfcase>
								<cfdefaultcase>
									<cfset colStruct[col].excelCellAttribute = '' />
								</cfdefaultcase>
							</cfswitch>
		
		
		</cfloop>


				<cfif ListFindNoCase( this.queryObject.ColumnList, this.alphabeticalIndexColumn ) neq 0>
						<cfif form["frmAlphabeticalIndexList"] eq "">
							<cfquery name="getAlphabeticalIndex" dbtype="query">
							SELECT DISTINCT alphabeticalIndex
							FROM attributes.queryObject
							ORDER BY alphabeticalIndex ASC
							</cfquery>
							<cfset form["frmAlphabeticalIndexList"] = ValueList( getAlphabeticalIndex.alphabeticalIndex )>
						</cfif>
				</cfif>

		
	<cfset this.colstruct = colstruct>
	<cfset this.reloadStructures = false>

	<cfreturn true>
			
</cffunction>

<cffunction name="removeColumns" returntype="numeric" hint="removes a column(s) from report" >
	<cfargument name="removeTheseColumns" type="string" default = ""> 	
	<cfset var listposition = 0>
	
	<cfloop index = "col" list = "#removeTheseColumns#">
		<cfset listposition = listfindnocase (this.showTheseColumns,col)>
		<cfif listposition is not 0>
			<cfset this.showTheseColumns = listdeleteAt (this.showTheseColumns, listposition)>
			<cfset this.columnHeadingList = listdeleteAt (this.columnHeadingList, listposition)>
		</cfif>
	</cfloop>

	<cfset this.reloadStructures = true>

	<cfreturn listposition>
</cffunction>

<cffunction name="addColumn" hint="adds extra columns to a report" >
		<cfargument name="columnName"  type="string" default=""> 			
		<cfargument name="columnHeading"  type="string" default="#columnName#"> 			
		<cfargument name="columnWidth"  type="string" default = ""> 		<!--- for ext --->
		
<!--- 			<cfargument name="iskeyColumn"  type="boolean" default="false"> 			
			<cfargument name="keyColumnURL"  type="string" default=""> 			
			<cfargument name="keyColumnKey"  type="string" default=""> 			
			<cfargument name="keyColumnTarget"  type="string" default=""> 			
			<cfargument name="keyColumnOpenInWindow"  type="string" default=""> 			
			<cfargument name="OpenWinName"  type="string" default=""> 			
			<cfargument name="OpenWinSettings"  type="string" default=""> 			
			<cfargument name="keyColumnIcon"  type="string" default=""> 			
			<cfargument name="keyColumnContextMenu"  type="string" default=""> 			
 --->
 	<cfset this.showTheseColumns = listappend(this.showTheseColumns,columnName)>
	<cfset this.columnHeadingList = listappend(this.columnHeadingList,columnHeading)>

	<cfset initialiseStructures()>
	

</cffunction>

<!--- 
WAB 2008/02/27

Take a query and make a crosstab query
 --->
	
<cffunction name="convertQueryToCrossTab" returntype="struct" hint="Takes a query and returns another query with crosstab">
 
 <cfargument name="queryObject" type="query" default = #this.queryobject#>  <!--- the query --->
 <cfargument name="ValueColumns" type="string" > <!--- this is the column, or columns, which contain the values to be displayed in the body of the crosstab --->
 <cfargument name="ValueColumnTitles" type="string" default="#ValueColumns#" >
 <cfargument name="HeaderColumn" type="string" > <!--- this is the column whose values are going to become the headers --->
 <cfargument name="HeaderOrderColumn" type="string" default = "#HeaderColumn#"> 
 <cfargument name="HeaderTitleColumn" type="string" default = "#HeaderColumn#">   <!--- not really necessary but can be handy, needed internally when doing multiple crosstabs  --->
 <cfargument name="GroupByColumns" type="string" default="">
 <cfargument name="GroupByOrderColumns" type="string" default="#GroupByColumns#">
 <cfargument name="GroupByTitleColumns" type="string" default="#GroupByColumns#">
 <cfargument name="reverseCrossTabStacking" type="boolean" default="false">
 <cfargument name="requiredHeaderValues" type="string" default = "">   <!--- if your query is a bit sparsely populated and you need to ensure that some columns get created then add details here. 
 																			if there is an order column then put that after  a |   eg: ValueA|1,Value2|2
																			if there is multiple grouping then for each grouping you need to define the required header values and separate with a #application.delim1#
																			sorry a bit messy
																			--->
 <cfargument name="nullValues" type="string" default = "">  <!--- may be need to support list which matches valueCOlumns --->

<cfargument name="keyColumnList" type="string" default = "">
<cfargument name="keyColumnKeyList" type="string" default = "">
<cfargument name="keyColumnURLList" type="string" default = "">
<cfargument name="KeyColumnContextMenuList" type="string" default = "">
<cfargument name="KeyColumnContextMenuIDExpressionList" type="string" default = ""> 



 
		<cfset var result = structNew()>
 		<cfset var thisGroupByColumns = "">
 		<cfset var thisGroupByOrderColumns = "">
		<cfset var thisGroupByTitleColumns = "">		
		<cfset var headerColumnNames = "">		 
		<cfset var headerColumnTitles = "">		 
		<cfset 	var tmpHeaderColumn = "">
		<cfset 	var tmpHeaderTitleColumn =  "">
		<cfset 	var tmpHeaderOrderColumn = "">
			<cfset var allButLastHeaderColumn = "">
			<cfset var allButLastHeaderOrderColumn = "">
			<cfset var allButLastHeaderTitleColumn = "">
			<cfset 	var tmpGroupByColumns = "">
			<cfset 	var tmpGroupByOrderColumns = "">
			<cfset 	var tmpGroupByTitleColumns = "">
			<cfset 	var grid = "">
			<cfset 	var rowid= "">
			<cfset var headerColumnName = "">
			<cfset var col = "">
			<cfset var dataOrdered = "">			

		
<!--- 
<cfset x = duplicate(arguments)>
<cfset structDelete(x,"queryObject")>
<cfdump var="#x#">
 --->		
		 <!--- If there aren't any groupByColumn defined then we work then out by just removing the value and header columns from the column list--->
		<cfif arguments.GroupByColumns is "">
			<!--- work out the group by column --->
			<cfset columnList = arguments.queryObject.columnList>
			<!--- delete the value and header columns --->
			<cfloop list="#HeaderColumn#,#ValueColumns#" index= "col">
				<cfset x = listFindNoCase(columnList,col)>
				<cfif x is not 0 ><cfset columnList = listDeleteAt(columnList,x)></cfif>
			</cfloop>
			<cfset thisGroupByColumns = columnList>
			<cfset thisGroupByOrderColumns = columnList>
			<cfset thisGroupByTitleColumns = columnList>			
		<cfelse>
			<cfset thisGroupByColumns = arguments.GroupByColumns>
			<cfset thisGroupByOrderColumns = arguments.GroupByOrderColumns>
			<cfset thisGroupByTitleColumns = arguments.GroupByTitleColumns>
		</cfif>	
		
		
		<cfif listLen (arguments.HeaderColumn) gt 1>
			<!--- This is the special case of doing a double crosstab --->
			<cfset allButLastHeaderColumn = reverse(listrest(reverse(arguments.headerColumn)))>
			<cfset allButLastHeaderOrderColumn = reverse(listrest(reverse(arguments.headerOrderColumn)))>
			<cfset allButLastHeaderTitleColumn = reverse(listrest(reverse(arguments.headerTitleColumn)))>

			<cfset 	tmpGroupByColumns = listappend(arguments.GroupByColumns,listfirst(arguments.headerColumn))>
			<cfset 	tmpGroupByOrderColumns = listappend(arguments.GroupByOrderColumns,listfirst(arguments.headerOrderColumn))>
			<cfset 	tmpGroupByTitleColumns = listappend(arguments.GroupByOrderColumns,listfirst(arguments.headerTitleColumn))>
			<cfset 	tmpHeaderColumn = listrest( arguments.headerColumn)>
			<cfset 	tmpHeaderTitleColumn = listrest( arguments.headerTitleColumn)>
			<cfset 	tmpHeaderOrderColumn = listrest( arguments.headerOrderColumn)>
			<cfset  tmprequiredHeaderValues =listrest( arguments.requiredHeaderValues,"#application.delim1#")>
			
<!--- 			<cfoutput> Debug: 
						valueColumns = #ValueColumns#<BR>
						valueColumnTitles = #valueColumnTitles#<BR>
						headerColumn = #tmpHeaderColumn# <BR>
						headerOrderColumn = #tmpHeaderOrderColumn# <BR>
						headerTitleColumn = #tmpHeaderTitleColumn# <BR>
						GroupByColumns = #tmpGroupByColumns#<BR>
						GroupByOrderColumns = #tmpGroupByOrderColumns#<BR>
						GroupByTitleColumns = #tmpGroupByTitleColumns#<BR>
			</cfoutput>

 --->			<cfset crossTabQueryInfo = convertQueryToCrossTab(
						queryObject = queryObject,
						valueColumns = ValueColumns,
						valueColumnTitles = valueColumnTitles,
						headerColumn = tmpHeaderColumn ,
						headerOrderColumn = tmpHeaderOrderColumn ,
						headerTitleColumn = tmpHeaderTitleColumn ,
						GroupByColumns = tmpGroupByColumns,
						GroupByOrderColumns = tmpGroupByOrderColumns,
						GroupByTitleColumns = tmpGroupByTitleColumns,
						requiredHeaderValues =tmpRequiredHeaderValues,
						keyColumnList = keyColumnList,
						keyColumnURLList = keyColumnURLList,
						reverseCrossTabStacking = reverseCrossTabStacking,
						nullValues = nullValues
			)>
		
			<cfset arguments.ValueColumns = crossTabQueryInfo.headerColumnNames>
			<cfset arguments.ValueColumnTitles = crossTabQueryInfo.headerColumnTitles>
			<cfset arguments.queryObject = crossTabQueryInfo.query>
			<cfset arguments.headerColumn = listfirst( arguments.headerColumn)>
			<cfset arguments.headerOrderColumn = listfirst( arguments.headerOrderColumn)>
			<cfset arguments.headerTitleColumn = listfirst( arguments.headerTitleColumn) >
			<cfset arguments.requiredHeaderValues = listfirst( arguments.requiredHeaderValues,"#application.delim1#") >
			
			<cfif structKeyExists(crossTabQueryInfo.TableFromQueryObjectAttributes,"keyColumnList")>
				<cfset arguments.keyColumnList = crossTabQueryInfo.TableFromQueryObjectAttributes.keyColumnList>
				<cfset arguments.keyColumnURLList = crossTabQueryInfo.TableFromQueryObjectAttributes.keyColumnURLList>
			</cfif>
			
			
		</cfif>


		
		<!--- If more than one value fields then easier to deal with null values if we have a little lookup structure --->
		<cfset nullValuesStruct = structNew()>
		<cfloop index="i" from=1 to = "#listlen(ValueColumns)#">
			<cfset col = listgetat(ValueColumns,i)>
			<cfif listLen(ValueColumns) is listLen(nullvalues)>
				<cfset nullValuesStruct[col] = listgetat(nullvalues,i)>
			<cfelse>
				<cfset nullValuesStruct[col] = nullvalues>
			</cfif>
		</cfloop>
		
		<cfset dummy = queryNew('dummy')>
		<cfset queryAddRow(dummy,1)>
		
		<!--- Get distinct values in the headerColumn and add on any extra which are required (eg might want to make sure that all months are covered even if no data--->
		<cfquery name = "distinctheaders" dbType="query">
			select distinct #HeaderColumn# as header, #HeaderOrderColumn# as Ordering, #HeaderTitleColumn# as headerTitle
			from queryObject
			
			<!--- Add in any required headers ---> 
			<cfloop index="extras" list = "#requiredHeaderValues#">
			union select '#listfirst(extras,"|")#' as header, '#listLast(extras,"|")#' as ordering, '#listfirst(extras,"|")#' from dummy
			</cfloop>
			
		 	order by ordering
		</cfquery>
		<cfquery name = "distinctheaders" dbType="query">
			select distinct * 
			from distinctheaders
		 	order by ordering
		</cfquery>
		
<!--- <cfdump var="#distinctheaders#">  --->
		<!--- Initialise a structure for holding details of each new column being created --->
		<cfset HeaderColumnsStruct = structNew()>
		<cfloop list = "#ValueColumns#" index="valueCol">
				<CFIF isNumeric(valueCol)>		<!--- TODO NASTY HACK TO DEAL WITH NUMERIC VALUE  --->
					<cfset newValueCol = "x_" & valueCol >
					<cfset valueColumns = listsetAt(valuecolumns,listfind(valuecolumns,valuecol),newValueCol)>
					<cfset valueCol = newValueCol>
				</CFIF>
				<cfset HeaderColumnStruct[valueCol] = structNew()>
		</cfloop>		

		<cftry>
		<!--- Make a blank query with all header columns and all the rows --->
		<cfset headerColumnNames = "">
		<cfquery name = "grid" dbType="query">
			select distinct #thisGroupByColumns#,#thisGroupByOrderColumns#,
			''
			<cfloop index="col" list="#thisGroupByColumns#">
			+'&#col#='+ cast(#col# as varchar)
			</cfloop>
			as CompoundKey 
		
		<!--- Very nasty here, haven't worked out which way to stack the cross tabs, maybe need to be flexible and I find myself repeating the code with the loops the other way round --->
		<cfif reverseCrossTabStacking>
			<cfloop query = "distinctHeaders">
				<cfloop index="valueColumnIndex" from=1 to = #listlen(ValueColumns)# >
						<cfset valueCol = listgetat(valueColumns,valueColumnIndex)>
						<cfset valueColTitle = listgetat(valueColumnTitles,valueColumnIndex)>						
<!--- 				<cfloop list = "#ValueColumns#" index="valueCol"> --->
						<cfif listLen(valueColumns) is 1>
							<cfset thisColumnName = "#header#">
							<cfset thisColumnTitle = "#headerTitle#">
						<cfelse>
							<cfset thisColumnName = "#header#_#valueCol#">
							<cfset thisColumnTitle = "#headerTitle##application.delim1##valueColTitle#">
						</cfif>
						<cfif isNumeric(thisColumnName)>		<!--- TODO NASTY HACK TO DEAL WITH NUMERIC VALUE  --->
							<cfset thisColumnName = "x" & header>
						</cfif>


						<cfset HeaderColumnStruct[valueCol][thisColumnName] = "">
						<cfset headerColumnNames = listappend(headerColumnNames,thisColumnName)>
						<cfset headerColumnTitles = listappend(headerColumnTitles,thisColumnTitle)>
						,
						<cfif isNumeric(nullValuesStruct[valuecol])>
						#nullValuesStruct[valuecol]# 
						<cfelse>
						'#nullValuesStruct[valuecol]#'
						</cfif>
						as #thisColumnName#  <!--- problem with numeric column names, don't haev solution yet --->
				</cfloop>
			</cfloop>
<cfelse>
			<cfloop index="valueColumnIndex" from=1 to = #listlen(ValueColumns)# >
					<cfset valueCol = listgetat(valueColumns,valueColumnIndex)>
					<cfset valueColTitle = listgetat(valueColumnTitles,valueColumnIndex)>						

<!--- 			<cfloop list = "#ValueColumns#" index="valueCol"> --->
				<cfloop query = "distinctHeaders">

						<cfif listLen(valueColumns) is 1>  <!--- when only one value column, doesn't make sense to put its column name into the header --->
							<cfset thisColumnName = "#header#">
							<cfif isNumeric(thisColumnName)> 		<!--- TODO NASTY HACK TO DEAL WITH NUMERIC VALUE  --->
								<cfset thisColumnName = "x" & header>
							</cfif>
							<cfset thisColumnTitle = "#headerTitle#">
						<cfelse>
							<cfset thisColumnName = "#valueCol#_#header#">
							<cfset thisColumnTitle = "#valueColTitle##application.delim1##headerTitle#">
						</cfif>
						<cfset HeaderColumnStruct[valueCol][thisColumnName] = "">
						<cfset headerColumnNames = listappend(headerColumnNames,thisColumnName)>
						<cfset headerColumnTitles = listappend(headerColumnTitles,thisColumnTitle)>
						,						
						<cfif isNumeric(nullValuesStruct[valuecol])>
						#nullValuesStruct[valuecol]# 
						<cfelse>
						'#nullValuesStruct[valuecol]#'
						</cfif>
						 as #thisColumnName#
				</cfloop>
			</cfloop>
</cfif>
			from queryObject
			<cfif thisGroupByOrderColumns is not "">
			order by 
			<cf_queryObjectName value="#thisGroupByOrderColumns#">
			</cfif>
		</cfquery>
		<cfcatch>
			<cfrethrow>
		</cfcatch>
</cftry>		
		<cfif thisGroupByOrderColumns is not "">
			<cfquery name = "dataOrdered" dbType="query">
			select * from queryObject
			order by 
			<cf_queryObjectName value="#thisGroupByOrderColumns#">
			</cfquery>
		<cfelse>
			<cfset dataOrdered = queryObject>
			
		</cfif>

		
		<!--- now we loop through the data and at the same time move through the blank grid which has been created 
			We test whether we are on the correct row and if not go onto the next row (which will always be the correct one because the ordering of the data and the grid is the same
		--->
			<!--- this is the expression which we evaluate to test whether we are on the correct row --->
			<cfset rowid = 1>
			<cfset expression = "true ">
			<cfloop list="#thisGroupByColumns#" index="Col">
				<cfset expression = expression & " AND #col# IS grid['#col#'][rowid] ">
			</cfloop>
		<!--- 	<cfoutput>#expression# <BR></cfoutput> --->

			<cfloop query="dataOrdered">
				<!--- if not on the correct row then go onto the next one --->
				<cfif not evaluate(expression)>
					<cfset rowid = rowid + 1>
				</cfif>

				<!--- set the values in the grid (if more than one value column then loop over them all --->
				<cfloop index="col" list = "#ValueColumns#">

				<cfif listLen(valueColumns) is 1>  <!--- when only one value column, doesn't make sense to put its column name into the header --->
						<cfset headerColumnName = "#dataOrdered[Headercolumn][currentrow]#">
					<cfelseif reverseCrossTabStacking>
						<cfset headerColumnName = "#dataOrdered[Headercolumn][currentrow]#_#col#">
					<cfelse>
						<cfset headerColumnName = "#col#_#dataOrdered[Headercolumn][currentrow]#">
					</cfif>
		<!--- TODO NASTY HACK TO DEAL WITH NUMERIC VALUE  --->
		<cfif isNumeric(headerColumnname)> 
			<cfset headerColumnName = 'X' & headercolumnname>
		</cfif>
					<cftry>
						<cfset querySetCell(grid,headerColumnName,evaluate(col),rowid)		>
						<cfcatch>
							<cfoutput>Cross Tab Error <BR>
							Data Row = #currentRow# ; Grid Row = #rowID# 
							<cfdump var="#CFCATCH#">
							<CF_ABORT>
							</cfoutput>
						</cfcatch>
					</cftry>	
				</cfloop>	
			
			
			</cfloop>

 
		<cfset result.query = grid>
		<cfset result.headerColumnNames = headerColumnNames>
		<cfset result.headerColumnTitles = headerColumnTitles>
		<cfset result.HeaderColumnStruct= HeaderColumnStruct>


		<cfset result.TableFromQueryObjectAttributes = structNew()>

			<!--- Prepare stuff for tablefromquery object --->
			<cfset result.TableFromQueryObjectAttributes.showTheseColumns = "#groupByColumns#,#headerColumnNames#">
			<cfset result.TableFromQueryObjectAttributes.columnHeadingList  = "#groupByTitleColumns#,#headerColumnTitles#">
			<cfset result.TableFromQueryObjectAttributes.rowIdentityColumnName = "compoundkey">


					 <cfif keyColumnList is not "">
						<!--- make sure all the key column lists have the right length - slightly nasty with extra comma --->
						<cfif KeyColumnContextMenuList is "">
							<cfset KeyColumnContextMenuList = repeatstring('no,', listlen(keyColumnList))  >
						</cfif>
						<cfif KeyColumnKeyList is "">
							<cfset KeyColumnKeyList = repeatstring('no,', listlen(keyColumnList))  >
						</cfif>
						<cfif KeyColumnContextMenuIDExpressionList is "">
							<cfset KeyColumnContextMenuIDExpressionList = repeatstring('no,', listlen(keyColumnList))  >
						</cfif>




						<!--- loop through all the key columns --->
						<cfloop index="positionInKeyColumnList" to = 1 from = "#listlen(keyColumnList)#" step = -1> <!--- not reverse loop allows us to delete items from list as we go --->
							<cfset keycolumn = listgetat(keyColumnList,positionInKeyColumnList)>
								<!--- if this is one of the columns which is now in the body of the cross tab --->
							<cfif listfindNoCase(valueColumns,keycolumn)>

										<cfloop item = "newcol" collection="#headerColumnStruct[keycolumn]#" >
											<cfset 	 keyColumnList = listappend(keyColumnList,newcol)>
											<cfset 	 keyColumnURLList = listappend(keyColumnURLList,listgetat(keyColumnURLList,positionInKeyColumnList))>
											<cfset 	 keyColumnKeyList = listappend(keyColumnKeyList,"compoundKey")>
											<cfset 	 KeyColumnContextMenuList = listappend(keyColumnContextMenuList,listgetat(KeyColumnContextMenuList,positionInKeyColumnList))>
											<cfset   KeyColumnContextMenuIDExpressionList = listappend(KeyColumnContextMenuIDExpressionList,'compoundkey')>
										</cfloop>

									<!--- now delete this item from all the key column lists --->					
									<cfset keyColumnList = listdeleteAt(keyColumnList,positionInKeyColumnList)>
									<cfset keyColumnKeyList = listdeleteAt(keyColumnKeyList,positionInKeyColumnList)>
									<cfset keyColumnURLList = listdeleteAt(keyColumnURLList,positionInKeyColumnList)>
									<cfset KeyColumnContextMenuList = listdeleteAt(KeyColumnContextMenuList,positionInKeyColumnList)>
									<cfset KeyColumnContextMenuIDExpressionList = listdeleteAt(KeyColumnContextMenuIDExpressionList,positionInKeyColumnList)>											

							</cfif>	
						</cfloop>	
						<cfset result.TableFromQueryObjectAttributes.keyColumnList = keyColumnList>
						<cfset result.TableFromQueryObjectAttributes.keyColumnKeyList = keyColumnKeyList>
						<cfset result.TableFromQueryObjectAttributes.keyColumnURLList =keyColumnURLList>
						<cfset result.TableFromQueryObjectAttributes.KeyColumnContextMenuList = KeyColumnContextMenuList>
						<cfset result.TableFromQueryObjectAttributes.KeyColumnContextMenuIDExpressionList =KeyColumnContextMenuIDExpressionList>
						
						
					 </cfif>

		<cfset this.queryObject = result.query>		
		<cfset this.showTheseColumns = result.TableFromQueryObjectAttributes.showTheseColumns>
		<cfset this.columnHeadingList = result.TableFromQueryObjectAttributes.columnHeadingList >
		<cfset this.reloadStructures = true>		

		<cfreturn result>

</cffunction>

<!--- Sums items in a query and adds the sums into the query in the correct place (well reorders it) --->

<cffunction name="ApplyGroupingToQuery" returntype="boolean" hint="Takes a query and adds Sum rows to it">
 
 <!--- 	 <cfargument name="queryObject" type="query" >  <!--- the query --->
 
  <!--- excluding the groupbys --->
	 
		 --->
	 <cfargument name="queryObject" type="query" default = "#this.queryObject#" >
	 <cfargument name="GroupByColumns" type="string" default = "#this.GroupByColumns#">
	 <cfargument name="OrderByColumns" type="string" default = "#this.OrderByColumns#">
	<cfargument name="TotalTheseColumns" type="string" default = "#this.TotalTheseColumns#">
	<cfargument name="averageTheseColumns" type="string" default = "#this.averageTheseColumns#">
	
	 <cfif isDefined ("GroupByColumns")>
	 	<cfset this.GroupByColumns = groupByColumns>
		 <cfif isDefined ("OrderByColumns")>
		 	<cfset this.orderByColumns = orderByColumns>
		<cfelse>
		 	<cfset this.orderByColumns = groupByColumns>
		 </cfif>
	 </cfif>
	
	 <cfif isDefined ("TotalTheseColumns")>
	 	<cfset this.TotalTheseColumns = TotalTheseColumns>
	</cfif>
	 <cfif isDefined ("AverageTheseColumns")>
	 	<cfset this.AverageTheseColumns = AverageTheseColumns>
	</cfif>
		
	<cfif this.GroupByColumns is "" and this.OrderByColumns is "" and this.TotalTheseColumns is "" and this.averageTheseColumns is "">
		<!--- no need to do any grouping stuff --->
		<cfreturn false>
	</cfif>	


		<!---  to do an average we actually need to do the sum, so bolt onto end--->
		<cfset TotalTheseColumns = listappend(this.TotalTheseColumns,this.averageTheseColumns)>
		
		<cfif this.openasexcel>
			<cfoutput>Excel Sums TBD</cfoutput>				
		</cfif>

		
		<cfquery name= "groupResults" dbtype="query">
		select * , '' as rowTYpe, 0 as groupGeneration, 0 as groupOrder ,'' as GroupFormula
		from queryobject where 1 = 0
		</cfquery>
	
			<!--- store sums and counts for each grouping level --->
			<cfset sum = structNew()>  
			<cfset Count = structNew()>
			<cfset formula = structNew()>  <!--- to remember what row the current group started on and keep track of row addition for excel--->
			
			<!--- these expressions built and tested to see when we get to the end of a group--->			
			<cfset footertestExpression = structNew()>
			<cfset headertestExpression = structNew()>
			
			<Cfset numberOfGroupingLevels = listLen(this.GroupByColumns)>
			<!--- Expression to test when change of group--->
			<cfset footertestExpression [0] = 'currentrow is not #Queryobject.recordcount# '> <!--- Note that record count changes as we go! --->
			<cfset headertestExpression [0] = 'currentrow is not 1'> <!--- Note that record count changes as we go! --->

			<cfloop index ="i" from ="1" to ="#numberOfGroupingLevels#" step= 1>
				 <!--- decided that I wanted to work this backwards, means that when sorting the result can sort on generation ascending and get totals in the right order --->
				<cfset col = listgetat(this.groupByColumns,i)>
				<cfset footertestExpression[i] = footertestExpression[i-1] & " AND (Queryobject['#col#'][currentRow] is QueryObject['#col#'][currentRow +1])">
				<cfset headertestExpression[i] = headertestExpression[i-1]  & " AND (Queryobject['#col#'][currentRow] is QueryObject['#col#'][currentRow -1])">
				<cfset sum [i] = structNew()>
				<cfset count [i] = 0>
				<cfset formula [i] = "">
				<cfloop list="#TotalTheseColumns#" index="col">
					<cfset sum[i][col] = 0>
				</cfloop>
			</cfloop>

					<cfset count [0] = 0>
					<cfset formula [0] = "">
				<cfloop list="#TotalTheseColumns#" index="col">
					<cfset sum[0][col] = 0>
				</cfloop>
			
			
			<cfset rowOffset = 1> <!--- this deals with the header row, may need a function for dealing with multi line headers --->				
			<cfset totalRowCount = rowOffset >				


			<cfloop query="queryobject">
				
				<cfloop index ="i" from  ="0" to ="#numberOfGroupingLevels#" step="1">
					<cfif not evaluate(headertestExpression[i])>
							<cfset queryAddRow (groupResults,1)>
							<cfset totalRowCount = totalRowCount + 1>				
							<cfif i is numberOfGroupingLevels><!--- this effectively stores the first row of this group for a Sum statement  --->
								<cfset formula [i] = "ColLetter" & totalRowCount>
							</cfif>
							<cfset notI = NumberOfGroupingLevels - I + 1>
							<cfset querySetCell (groupResults,"groupOrder",-noti)>
							<cfset querySetCell (groupResults,"groupGeneration",I)>
							<cfset querySetCell (groupResults,"rowType","GroupHeader")>
							<cfloop list="#this.GroupByColumns#,#this.GroupByOrderColumns#" index="g">
								<cfset g = listfirst(g," ")>
								<cfset querySetCell (groupResults,g,Queryobject[g][currentRow])>
							</cfloop>
					</cfif>
				</cfloop>


				<cfloop index ="i" to  ="0" from ="#numberOfGroupingLevels#" step="-1">

						<!--- same group so if bottom level add this row values to the accumulator --->
						<cfif i is numberOfGroupingLevels>
							<cfset count[i] = count[i] +1>
							<cfloop list="#TotalTheseColumns#" index="col">
								<cfif isNUmeric(ltrim(queryObject[col][currentRow])) >
									<cfset sum[i][col] = sum[i][col] + queryObject[col][currentRow]>
								<cfelse>
									<!--- <cfoutput>Not Numeric: |#queryObject[col][currentRow]#| <BR></cfoutput> --->
								</cfif>
								
							</cfloop>
						</cfif>

					<cfif evaluate(footertestExpression[i])>
						<cfbreak>	
					<cfelse>	
						<!--- different group so need to 
						a) Output current totals for this level
						b) Add these totals to level above
						c) reset this level to zero
						 --->
							<cfset queryAddRow (groupResults,1)>
							<cfset notI = NumberOfGroupingLevels - I + 1>
							<cfset querySetCell (groupResults,"groupOrder",noti)>
							<cfset querySetCell (groupResults,"groupGeneration",I)>
							<cfset querySetCell (groupResults,"rowType","GroupFooter")>
							<cfif i is numberOfGroupingLevels>
								<cfset querySetCell (groupResults,"GroupFormula","SUM(#formula[i]#:ColLetter#totalRowCount#)")>					
							<cfelse>
								<cfset querySetCell (groupResults,"GroupFormula",formula[i])>					
							</cfif>	
							

							<cfloop list="#this.GroupByColumns#,#this.GroupByOrderColumns#" index="g">
								<cfset g = listfirst(g," ")>
								<cfset querySetCell (groupResults,g,queryobject[g][currentRow])>
							</cfloop>
							<cfset totalRowCount = totalRowCount + 1>	

						<!--- output totals of this level to the query --->
						<cfloop list="#TotalTheseColumns#" index="col">
							
							<cfset total = sum[i][col]>
							<cfif listFindNoCase (this.AverageTheseColumns,col)>
								<cfif count[i] is 0>
									<cfoutput>division by zero error #i# #currentrow#</cfoutput>
								<cfelse>	
									<cfset total = total/count[i]>
									<cfset Total =  Total>  <!--- "Average: " & this word is going to be a problem come format time --->
								</cfif>
							</cfif>
							<cfset querySetCell (groupResults,col,total)>

							

								<cfif i is not 0>
									<cfset sum[i-1][col] = sum[i-1][col] + sum[i][col]>
									<cfset sum[i][col] = 0>
								</cfif>	
						</cfloop>
								<!--- count has to be reset outside the loop, cause it can be used for averaging all columns --->
								<cfif i is not 0>
									<cfset count[i-1] = count[i-1] + count[i]>
									<cfset count[i] = 0>
									
									<cfset formula[i-1] = listappend(formula[i-1],"ColLetter#totalRowCount#","+")>
								</cfif>	

					</cfif>
				
				</cfloop>
				<cfset totalRowCount = totalRowCount + 1>				
			
			</cfloop>


	<cfquery dbtype ="query"	name="resultQuery">
	select #queryObject.columnList#, '' as rowTYpe, -1 as groupGeneration, 0 as groupOrder,    '' as GroupFormula from queryObject
	union
	select #queryObject.columnList#, rowTYpe, groupGeneration,groupOrder, GroupFormula from groupResults
	where not (rowType='GroupHeader' and groupGeneration = 0 )  <!--- gets rid of very top level group which doesn't really have a purpose (the footer is the overall total --->
	order by <cfif this.groupByOrderColumns is not "">#this.groupByOrderColumns#,</cfif> groupOrder asc <cfif this.orderByColumns is not "">,#this.orderByColumns#</cfif>
	</cfquery>

	
	<cfset this.queryObject = resultQuery>

	<!--- remove any group by fields from the showcolumnlist --->
	<cfset removeColumns (this.GroupByColumns)>
	<cfset this.reloadStructures = true>			

	 <cfreturn true>
	 
</cffunction>	 
	


<cffunction name="ApplyCalculatedColumsToQuery" returntype="boolean" hint="Processes Calculated Columns etc.">

				
		<cfset colStruct = this.colStruct >
		<cfset reportQuery = this.queryObject>
		<cfset hasRowType = iif(listfindnocase(reportQuery.columnList,"rowType"),true,false)>
		<cfloop item="col" collection="#colStruct#">
			<cfif not listFindNoCase(this.queryObject.columnList,col)>
				<cfset queryAddColumn(this.queryObject,col,arrayNew(1))>
			</cfif>
		</cfloop>

		<cfloop query="this.queryObject">
			
			<cfloop item="col" collection="#colStruct#">
				<cfif structKeyExists(colStruct[col],"eval") and colStruct[col].eval is not "">
					<cfset colValue = reportQuery[col][currentRow]>
					<cfif not hasRowType or not (rowType is "GroupHeader" and not listfindnocase(this.groupByColumns,col))>  <!--- don't format headers (end up with 0.00 type things) ---> 
						<cftry>
 								<cfset newcolValue = evaluate(colStruct[col].eval)> 

								<cfcatch>
									<cfset newColValue = colValue>
<!--- 									<cfoutput>failed #col# </cfoutput>
									<cfdump var="#cfcatch#"><CF_ABORT>
 --->								</cfcatch>
						</cftry>
						<cfset querySetCell (reportQuery,col,newColValue,currentRow)>
					</cfif>	
				</cfif>
			
				<!--- <cfif structKeyExists(colStruct[col],"keyCol") >
					<cftry>
						<cfset anchor = evaluate(colStruct[col].keyCol.anchorEval)>
						<cfset querySetCell (reportQuery,"#col#_Anchor",anchor,currentRow)>
						<cfcatch>
							<cfoutput>Couldn't evaluate #colStruct[col].keyCol.anchorEval# for </cfoutput>
						</cfcatch>
					</cftry>	

				
				</cfif> --->
			
			
			</cfloop>
		
		</cfloop>
	


<cfif this.IllustrativePriceColumns is not "">
 <cfoutput>TBD Illustrative Pricing<BR></cfoutput>							
<!--- 
<cfif structkeyExists(attributes,"IllustrativePriceToCurrency")>

	<cfswitch expression="#attributes.IllustrativePriceToCurrency#">
		<cfcase value="EUR">
			<cfset excelIllustrativeClass = "xl45">
		</cfcase> 
		<cfdefaultcase>
			<cfset excelIllustrativeClass = "x125">
		</cfdefaultcase>
	</cfswitch>
</cfif> 
		<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
		
					<cfloop index="col" list = "#this.IllustrativePriceColumns#">
						<cfset querySetCell(this.queryObject)					
					
					
					
					
					</cfloop>
				</cfif>
				

					<cfswitch expression="#attributes.IllustrativePriceMethod#">
						<cfcase value="2">
							<cfscript>
								if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
									exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
								} else {
									exchangeDate = '';								
								};
								IllustrativePrice = application.com.currency.convert(
									method = #attributes.IllustrativePriceMethod#,
									fromValue = evaluate(i),
									fromCurrency = attributes.IllustrativePriceFromCurrency,
									toCurrency = attributes.IllustrativePriceToCurrency,
									date = '#exchangeDate#'
									);
							</cfscript>
							<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
						</cfcase>
					</cfswitch>
					<cfswitch expression="#cellType#">
						<cfcase value="1">
							<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '"' />
						</cfcase>
						<cfcase value="2">
							<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '" class="' & excelIllustrativeClass & '"' />
						</cfcase>
					</cfswitch>
				</cfif>
			</cfif>


And remember to add columns
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							(#attributes.IllustrativePriceFromCurrency#)&nbsp;
						</cfif>
					</cfif>
					</th>
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							<th valign="top">&nbsp;#thisColumnHeading# (#attributes.IllustrativePriceToCurrency#) phr_IllUSTRATIVE&nbsp;</th>
						</cfif>
					</cfif>
					
				<cfif listFindNoCase(attributes.hideTheseColumns,i) eq 0>
					<th valign="top">
						&nbsp;#replace(i,"_"," ","all")#&nbsp;
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							(#attributes.IllustrativePriceFromCurrency#)&nbsp;
						</cfif>
					</cfif>					
					</th>
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							<th valign="top">&nbsp;#replace(i,"_"," ","all")# (#attributes.IllustrativePriceToCurrency#) phr_IllUSTRATIVE&nbsp;</th>
						</cfif>

 ---> 

					</cfif>

		<cfif this.sortOrder is not "">
			<cfquery name="this.queryObject" dbtype="query">
			select * from this.queryObject order by #this.sortorder#
			</cfquery>
		
		</cfif>
					
		
		<cfreturn true>
		
		
</cffunction>


<!---  --->


<cffunction name="outputFilters">

<cfif IsQuery(this.filterqueryObject) and this.openAsExcel eq false>

	<cfoutput>
	<script  type="text/javascript">
		function reSort(sortOrder) {
			var form = document.sortForm;
			form.sortOrder.value = sortOrder;
			form.submit();
		}
		
		function SetFilterSelectValues(nVariableName,nValue,nWhich) {


		// Change the contents of the filter values list
		// depending upon the value selected in the filter column list

	
		if(nValue == ""){
			// if set to null then make sure that the linked value select is null as well
			nSelect = eval('document.FilterSelectForm.FilterSelectValues' +nWhich)
			nSelect.selectedIndex = 0 ;
			document.FilterSelectForm.submit()	
		}else{

			nVariable = eval(nVariableName + '_' + nWhich);
			nSelect = eval('document.FilterSelectForm.FilterSelectValues' +nWhich)
			nSelect.options.length = 0
			
			opt = new Option()
			opt.text  = "            "
			opt.value = ""
			nSelect.options[nSelect.options.length]=opt
			
				x=nVariable.indexOf(",")
				value=nVariable.substr(0,x)
				nVariable = nVariable.slice(x+1,nVariable.length)
				x=nVariable.indexOf(",")
				text=nVariable.substr(0,x)
				nVariable = nVariable.slice(x+1,nVariable.length)
				opt = new Option()
				opt.value = value.replace( /~/g, "," );;
				opt.text  = text.replace( /~/g, "," ); // commas were escaped as ~
				nSelect.options[nSelect.options.length]=opt
			
			while (x>-1){
				x=nVariable.indexOf(",")
				value=nVariable.substr(0,x)
				nVariable = nVariable.slice(x+1,nVariable.length)
				x=nVariable.indexOf(",")
				text=nVariable.substr(0,x)
				nVariable = nVariable.slice(x+1,nVariable.length)

				opt = new Option()
				opt.value = value.replace( /~/g, "," );;
				opt.text  = text.replace( /~/g, "," ); // commas were escaped as ~
				nSelect.options[nSelect.options.length]=opt
			}
			nSelect.options[nSelect.options.length-1]=null
		


		}
	}
		<!--- --------------------------------------------------------------------- --->
		<!--- function select --->
		<!--- --------------------------------------------------------------------- --->
		<cfif this.showFunctionList>
			function checkboxValuesToList( sourceCheckbox )
			{
			  var returnValue = "";
			  var checkboxIndex = 0;
			  var listDelimiter = "";
			
			  if ( (sourceCheckbox.checked) )
			  {
			    returnValue = sourceCheckbox.value;
			  }
			  else
			  {
			    for ( checkboxIndex = 0; checkboxIndex < sourceCheckbox.length; checkboxIndex++ )
			    {
			      if ( sourceCheckbox[checkboxIndex].checked )
			      {
			        returnValue += listDelimiter;
			        returnValue += sourceCheckbox[checkboxIndex].value;
			        listDelimiter = ",";
			      }
			    }
			  }
			
			  return returnValue;
			}
			
			var functionObject = new Object();
			
			<cfloop query="this.functionListQuery">
				functionObject["<cfoutput>#JSStringFormat( this.functionListQuery.functionID )#</cfoutput>"] = new Object();
				functionObject["<cfoutput>#JSStringFormat( this.functionListQuery.functionID )#</cfoutput>"]["url"] = "<cfoutput>#JSStringFormat( this.functionListQuery.url )#</cfoutput>";
				functionObject["<cfoutput>#JSStringFormat( this.functionListQuery.functionID )#</cfoutput>"]["windowFeatures"] = "<cfoutput>#JSStringFormat( this.functionListQuery.windowFeatures )#</cfoutput>";
			</cfloop>
			
			function eventFunctionOnChange( callerObject ){
			  
			  var checkboxValueList = "";
			  
			  if ( functionObject[callerObject[callerObject.selectedIndex].value]["url"] != "" ){
			  	
				checkboxValueList = checkboxValuesToList( document.forms["frmBogusForm"]["frmRowIdentity"] );
				var thisForm = document.forms["frmBogusForm"];
			    
				if ( checkboxValueList == "" && functionObject[callerObject[callerObject.selectedIndex].value]["url"].indexOf( "javascript:" ) != 0 ){
			      alert( "Please select one or more rows first" );
			    }
			    
				else if ( functionObject[callerObject[callerObject.selectedIndex].value]["windowFeatures"] != "" ){
				  var newURL = functionObject[callerObject[callerObject.selectedIndex].value]["url"];
				  var strArr = newURL.split("?");
				  var formItemName = "";
				  if (strArr.length >= 2) {
				  	var strArr2 = strArr[1].split("&");
					var strArr3 = strArr2[strArr2.length-1].split("=");
				  	formItemName = strArr3[0];
					//alert((newURL.length - 1) - (formItemName.length));
					var tmpURL = newURL.substring(0,(newURL.length) - (formItemName.length));
					newURL = tmpURL;
				  }
				  //var tmpForm = document.createElement('form');
				  var tmpForm = thisForm;
				  var ninput = document.createElement('input');
				  ninput.type = 'hidden';
				  ninput.name = formItemName;
				  ninput.value = ninput.defaultValue = checkboxValueList;
				  tmpForm.method = "post";
				  tmpForm.action = newURL;
				  tmpForm.onsubmit = window.open( "", "functionWindow", functionObject[callerObject[callerObject.selectedIndex].value]["windowFeatures"], true );
				  tmpForm.target = "functionWindow";
				  
				  tmpForm.appendChild(ninput);
				  //document.body.appendChild(tmpForm);
				  
				  tmpForm.submit();
			    }
			   
			   	else {
			      window.location.href = functionObject[callerObject[callerObject.selectedIndex].value]["url"] + escape( checkboxValueList );
			  	}
			  }
			  
			  callerObject.selectedIndex = 0;
			  
			  return true;
			
			}
		
			function checkboxSetAll( checkCheckbox )
			{
			  var checkboxIndex = 0;
			
			  if ( !(document.forms["frmBogusForm"]["frmRowIdentity"].length) )
			  {
			    document.forms["frmBogusForm"]["frmRowIdentity"].checked = checkCheckbox;
			  }
			  else
			  {
			    for ( checkboxIndex = 0; checkboxIndex < document.forms["frmBogusForm"]["frmRowIdentity"].length; checkboxIndex++ )
			    {
			      document.forms["frmBogusForm"]["frmRowIdentity"][checkboxIndex].checked = checkCheckbox;
			    }
			  }
			
			  return true;
			}
		</cfif>

	</script>
	</cfoutput>



<!--- ==============================================================================
       SET UP THE FILTERS
	   If there are any filters defined in FilterSelectFieldList
		Set Up the filter lists. This requires building lists of distinct values for each
		filter field, in Javascript, so that the contents of the select lists can bechanged at will.
		Cannot use the ValueList function because commas will corrupt the list.
=============================================================================== --->	
	<cfloop index="filterNumber" from = "3" to = "1" step = "-1">
		
		<cfset thisFilterSelectFieldList = evaluate("this.FilterSelectFieldList#filterNumber#") >

			<CFLOOP LIST="#thisFilterSelectFieldList#" INDEX="nFieldName">
					
					<CFQUERY Name="Filter#nFieldName#_#filterNumber#" dbtype="query">
						SELECT DISTINCT #nFieldName# AS FilterValue, #nFieldName# AS FilterDisplay FROM this.filterqueryObject
						where 1=1   <!--- #nFieldName# <> '' doesn't make sense to bring back blanks since we don't use the filter if the value is ''--->
						Group By #nFieldName#
						Order By <cf_queryObjectName value="#nFieldName#">
					</CFQUERY>
	
				<cfset x = "">
				<cfloop query="Filter#nFieldName#_#filterNumber#">
						<cfset x = ListAppend( x, Replace( JSStringFormat( FilterValue ), ",", "~", "all" ) )>
						<cfset x = ListAppend( x, Replace( JSStringFormat( FilterValue ), ",", "~", "all" ) )>
				</cfloop>
				
				<CFOUTPUT>	
				<SCRIPT LANGUAGE="JavaScript1.2">
					var #nFieldName#_#filterNumber# = '#x#,'
				</SCRIPT>
				</CFOUTPUT>
			</CFLOOP>
	</cfloop>

<!--- --------------------------------------------------------------------- --->
<!--- function select --->
<!--- --------------------------------------------------------------------- --->
	<cfif this.showFunctionList>
		<cfoutput>
		<table width="100%" border="0" cellspacing="0" cellpadding="3" align="#this.tableAlign#" class="#this.tableClass#">
			<tr>
			<cfif this.FunctionListComment eq "yes">
			<!--- MDC 20-01-05 I've added the above parmam into the top of this page
			If you add in the param into the table from query object you will get a translateable phrase
			opposite the function dropdown, (so long as you have cftranslate around the table from query object.
			TO DO: we need to be able to trim the function name to create a unique translation--->
			<cfset G = this.functionListQuery.functionName>
			<cfset C = #replace(G,"_","","ALL")#>
			<cfset B = C>
			<cfset A = #replace(B," ","","ALL")#>
			<td>phr_Function_#A#</td>
			</cfif>
				<td align="right" valign="top">
					<select name="frmFunction" onChange="eventFunctionOnChange(this);">
						<cfloop query="this.functionListQuery">

							<cfif this.functionListQuery.securityLevel eq "">
								<cfset securityLevelApproved = true>
							<cfelse>
								<cfset securityLevelApproved = false>
								<cfloop index="securityLevelEntry" list="#this.functionListQuery.securityLevel#">
									<!--- 
									<cfif ListFindNoCase( session["securitylevels"], securityLevelEntry ) neq 0>
										<cfset securityLevelApproved = true>
										<cfbreak>
									</cfif>
									--->
									<cfset securityLevelApproved = application.com.login.checkSecurityList(securityLevelEntry)>
									<cfif securityLevelApproved><cfbreak></cfif>								
								</cfloop>
							</cfif>

							<cfif securityLevelApproved>
								<option value="#this.functionListQuery.functionID#">#htmleditformat(this.functionListQuery.functionName)#</option>
							</cfif>
						</cfloop>
					</select>
				</td>
			</tr>
		</table>
		</cfoutput>
	</cfif>
<!--- --------------------------------------------------------------------- --->

	<cfif this.radioFilterName gt 0 or this.FilterSelectFieldList GT 0 
		or this.checkBoxFilterName gt 0 or this.searchColumnList neq "" or this.useTeams>

		<cfoutput>
		<table width="100%" border="0" cellspacing="0" cellpadding="3" align="#this.tableAlign#" class="#this.tableClass#">
		<!--- ==============================================================================
	     FILTER SECTION                                                    
		=============================================================================== --->
		<tr>
		</cfoutput>
		<!--- --------------------------------------------------------------------- --->
		<!--- recycle url parameters except for startrow --->
		<!--- --------------------------------------------------------------------- --->
		<cfset queryString = makeURLVariablesIntoQueryString(dontinclude = "startRow")>

		<!--- --------------------------------------------------------------------- --->
		<cfoutput>
		<form action="#cgi.SCRIPT_NAME#?#queryString#" method="post" name="FilterSelectForm">			
			<CFIF IsDefined("sortOrder")>
				<INPUT Name="sortOrder" Type="Hidden" Value="#sortOrder#">
			</CFIF>
		</cfoutput>
		<!--- --------------------------------------------------------------------- --->
		<!--- propagate form fields through filtering requests --->
		<!--- --------------------------------------------------------------------- --->
			<cfif IsStruct( this.passThroughVariablesStructure )>
				<cfloop collection="#this.passThroughVariablesStructure#" item="formFieldName">
					<cfif formFieldName neq "fieldnames">
						<cfoutput><CF_INPUT type="hidden" name="#formFieldName#" value="# this.passThroughVariablesStructure[formFieldName] #"></cfoutput>
					</cfif>
				</cfloop>
			</cfif>
		<!--- ==============================================================================
	      MyTeams                                                 
		=============================================================================== --->	
		
		<cfif this.useTeams>	
		
			<CFQUERY NAME="TeamName" datasource="#application.siteDataSource#">
			Select TeamId, TeamName
			FROM MyTeam
			WHERE createdby = '#request.relaycurrentuser.personid#'
			ORDER BY TeamName
			</CFQUERY>	
			<cfoutput>
			<cfif TeamName.recordcount gt 1>
				<select name="frmTeam">			
				<CFIF NOT IsDefined("frmTeam")>
					<OPTION VALUE="">Please select a Team	
				</CFIF>
				<CFLOOP QUERY="TeamName">
					<OPTION VALUE=#TeamId# <CFIF IsDefined("frmTeam")><CFIF TeamId EQ #frmTeam#>SELECTED</CFIF></CFIF> >#htmleditformat(TeamName)#
				</CFLOOP>
				</select>
				<input type="hidden" name="frmTeamColumn" value = "#this.useTeamsPersonIDColumn#">
			<cfelseif TeamName.recordcount eq 0>
				You have no Teams <a href="/homePage/MyTeam.cfm">click here</a> to create a new team.
			<cfelseif TeamName.recordcount eq 1>
				Your team : #htmleditformat(TeamName.TeamName)#. <a href="/homePage/MyTeam.cfm">click here</a> to manage your team.
				<input type="hidden" name="frmTeam" value = "#TeamName.TeamId#">
				<input type="hidden" name="frmTeamColumn" value = "#this.useTeamsPersonIDColumn#">
			</cfif>
			</cfoutput>
		</cfif>
		
<!--- --------------------------------------------------------------------- --->	
<!--- ==============================================================================
      FilterSelectFieldList                                                   
=============================================================================== --->
	<cfloop index="filterNumber" from="1"  to = "3">

		<cfset thisFilterList = evaluate("this.FilterSelectFieldList#filterNumber#")>
		<!--- SSS 2007-03-16 --->
		<cfset thisFilterList = ListChangeDelims(thisFilterList, "#application.delim1#", ",")>
		<cfset currentFilterChosen = evaluate ("form.FilterSelect#filterNumber#")>
		<cfset currentFilterValue  = evaluate ("form.FilterSelectValues#filterNumber#")>
		<cfif listlen(thisFilterList,"#application.delim1#") is 1><cfset currentFilterChosen = thisFilterList></cfif><!--- If only one item then default to select that item --->
		
		
		<CFIF listlen(thisFilterList,"#application.delim1#") GT 0>
			<cfoutput>
			<td colspan="#listLen(this.showTheseColumns )#" align="left" valign="top">
				<DIV ID=FilterSelect#filterNumber#>
					<cfif listlen(thisFilterList,"#application.delim1#") is not 1>
						<!--- Select Box for filter name --->
						<select name="FilterSelect#filterNumber#" onChange="SetFilterSelectValues(document.FilterSelectForm.FilterSelect#filterNumber#.options[document.FilterSelectForm.FilterSelect#filterNumber#.selectedIndex].value,document.FilterSelectForm.FilterSelect#filterNumber#.options[document.FilterSelectForm.FilterSelect#filterNumber#.selectedIndex].value,#filterNumber#)">
					
							<option value="">Phr_Sys_SelectFilter</option>
							<!--- 2006-05-03 AJC P_CHW001 - Added code to replace dbfieldnames with ColumnHeadingList as the display name. Value value stays the same. --->
							<cfset variables.currentlistelement=1>
							<cfloop index="filterName" list="#thisFilterList#" delimiters="#application.delim1#">
								<option value="#filterName#" <cfif filterName is currentFilterChosen >selected</cfif> >#iif( this.columnTranslation, 'this.columnTranslationPrefix & filterName', 'replace(ListGetAt(thisFilterList, variables.currentlistelement,"#application.delim1#"),"_"," ","all")' )#</option>
								<cfset variables.currentlistelement=variables.currentlistelement+1>
							</cfloop>
							<option value="">Phr_Sys_ClearFilter</option>
						</select>
					<cfelse>
						<cfset filterName = thisFilterList>
						<input type=hidden name="FilterSelect#filterNumber#" value = "#filterName#">
						#iif( this.columnTranslation, 'this.columnTranslationPrefix & filterName', 'replace(filterName,"_"," ","all")' )#
					
					</cfif>
				
			 	Phr_Sys_by 
			 
				<!--- Select Box for filter value--->
			 <select name="FilterSelectValues#filterNumber#" 
			 	
			 >

				<CFIF IsDefined("Filter#currentFilterChosen#_#filterNumber#")>
				<CFSET nQuery=evaluate("Filter#currentFilterChosen#_#filterNumber#")>
						<option value="">Phr_Sys_SelectFilterValue </option>
						<CFLOOP Query="nQuery">
							<OPTION Value="#FilterValue#"<CFIF FilterValue IS currentFilterValue>SELECTED</CFIF>>#htmleditformat(FilterDisplay)# 
						</CFLOOP> 
				<CFELSE>
					<OPTION VALUE= "">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</CFIF>
				</select>
				
				</DIV>					

			</td>
			</cfoutput>
		</CFIF>	
	</cfloop>			


<!--- --------------------------------------------------------------------- --->
<!--- text search --->
<!--- --------------------------------------------------------------------- --->
		<cfif this.searchColumnList neq "" 
			and (listLen(this.searchColumnList) eq listLen(this.searchColumnDataTypeList))
						and (listLen(this.searchColumnList) eq listLen(this.searchColumnDisplayList))>
			<!--- <cfif this.searchColumnList neq ""> --->
			<cfoutput>
			<td align="left">phr_Sys_SearchBy
				<input type="hidden" name="frmSearchColumnDataType" value="#ListGetAt( this.searchColumnDataTypeList, iif( ListFindNoCase( this.searchColumnList, form["frmSearchColumn"]  ) neq 0, 'ListFindNoCase( this.searchColumnList, form["frmSearchColumn"]  )', 1 ) )#">
				<cfif ListLen( this.searchColumnList ) eq 1>
					<input type="hidden" name="frmSearchColumn" value="#HTMLEditFormat( this.searchColumnList )#">
					#htmleditformat(this.searchColumnDisplayList)#
				<cfelse>
					<select name="frmSearchColumn" onChange='this.form["frmSearchColumnDataType"].value = [#ListQualify( this.searchColumnDataTypeList, '"' )#][this.selectedIndex];'>
						<cfloop index="searchColumn" from="1" to="#ListLen( this.searchColumnList )#">
							<option value="#ListGetAt( this.searchColumnList, searchColumn )#"#iif( form["frmSearchColumn"] eq ListGetAt( this.searchColumnList, searchColumn ), de( " selected" ), de( "" ) )#>#ListLast( ListGetAt( this.searchColumnDisplayList, searchColumn ), "." )#</option>
						</cfloop>
					</select>
				</cfif>
				<select name="frmSearchComparitor">
					<cfloop index="searchComparitor" list="#searchComparitorList#">
						<option value="#ListFindNoCase( searchComparitorList, searchComparitor )#"#iif( form["frmSearchComparitor"] eq ListFindNoCase( searchComparitorList, searchComparitor ), de( " selected" ), de( "" ) )#>#htmleditformat(searchComparitor)#</option>
					</cfloop>
				</select>
				<CF_INPUT type="text" name="frmSearchText" value="# form["frmSearchText"] #" size="10" maxlength="15">
			</td>
			</cfoutput>
		</cfif>
<!--- --------------------------------------------------------------------- --->
		
		
<!--- ==============================================================================
       RADIO FILTER - designed to support one radio filter group                    
=============================================================================== --->		
		<cfif isDefined("this.radioFilterLabel") and isDefined("this.radioFilterDefault") 
			and (listLen(this.radioFilterLabel) eq listLen(this.radioFilterDefault))
			and (listLen(this.radioFilterName) eq listLen(this.radioFilterName))
			>
			<cfset formRadioFilterName = "FORM.#this.radioFilterName#">
			<cfif not isDefined("formRadioFilterName")><cfparam name="radioFilterName" default="this.radioFilterDefault"></cfif>
			<cfoutput>
			<td valign="top">#htmleditformat(this.radioFilterLabel)#
				<cfset checkLast = "False">
				<cfset checked = "False">
				<cfloop index="i" list="#this.radioFilterValues#" delimiters="-">
					<!--- if nothing checked by the end set the last one to be checked --->
					<cfif i eq listLast(#this.radioFilterValues#,"-") and not checked>
						<cfset checkLast = "True">
					</cfif>
					<INPUT TYPE="radio" NAME="#this.radioFilterName#" VALUE="#i#" <CFIF i eq this.radioFilterDefault or checkLast><cfset checked = "true">Checked</CFIF>>#htmleditformat(i)#
				</cfloop>
			</TD>
			</cfoutput>
		</cfif>
		
<!--- ==============================================================================
       CHECKBOX FILTER - designed to support one radio filter group                    
=============================================================================== --->		
		<cfif isDefined("this.checkBoxFilterLabel") and isDefined("this.checkBoxFilterName") 
			and (listLen(this.checkBoxFilterLabel) eq listLen(this.checkBoxFilterName))
			>
			<cfloop index="i" list="#this.checkBoxFilterName#">
				<!--- if this checkbox is defined in the FORM scope the it was previously checked --->
				<cfif isDefined("FORM.#i#")><cfset showChecked = TRUE><cfelse><cfset showChecked = FALSE></cfif>
					<cfoutput>
					<td valign="top">#getToken(this.checkBoxFilterLabel,listFindNoCase(this.checkBoxFilterName,i),",")# &nbsp;&nbsp;
						<CF_INPUT type="checkbox" name="#i#" value="1" Checked="#iif( showChecked,true,false)#"> 
					</TD>
					</cfoutput>
			</cfloop>
		</cfif>

	<cfoutput>
			<td><input type="submit" name="GO" value="Phr_Sys_GO"></td>
	</tr>
	</cfoutput>
<!--- --------------------------------------------------------------------- --->
<!--- query where clause --->
<!--- --------------------------------------------------------------------- --->
				<cfif StructCount( this.queryWhereClauseStructure ) neq 0>
					<cfoutput>
					<tr>
					<td colspan="99" align="left">
						<CF_INPUT type="hidden" name="frmWhereClauseList" value="#form["frmWhereClauseList"]#">
					</cfoutput>
						<cfloop collection="#this.queryWhereClauseStructure#" item="whereClauseStructureItem">
								<cfif this.selectbyday>
								
									<cfif structkeyexists(this.queryWhereClauseStructure[whereClauseStructureItem][1], "day")>
										<cfoutput>
											<select name="#this.queryWhereClauseStructure[whereClauseStructureItem][1].day#">
												<option value="">#this.queryWhereClauseStructure[whereClauseStructureItem][1].dayTitle#</option>
												<cfloop index="i" from="1" to="31">
													<option value="#i#"<cfif i EQ #Evaluate(this.queryWhereClauseStructure[whereClauseStructureItem][1].day)#>selected</cfif>>#htmleditformat(i)#</option>
												</cfloop>
											</select>
										</cfoutput>
									</cfif>
								</cfif>
							<cfoutput><select name="frmWhereClause#whereClauseStructureItem#" onChange="eventWhereClauseOnChange(this);"></cfoutput>
<!--- 
							<cfloop collection="#this.queryWhereClauseStructure[whereClauseStructureItem]#" item="whereClauseItem">
							<!--- would have looped collection but the keys were out of order!!! --->
 --->
							<cfset tmpKeyList = ListToArray( StructKeyList( this.queryWhereClauseStructure[whereClauseStructureItem] ) )>
							<!--- <cfoutput><script>alert("#this.queryWhereClauseStructure[whereClauseStructureItem]["1"]["sql"]#");</script></cfoutput> --->
							
							
							<!--- <cfif structCount(this.queryWhereClauseStructure[whereClauseStructureItem]) gte 2 and structKeyExists(this.queryWhereClauseStructure[whereClauseStructureItem],"1")>
								<cfif isDate(this.queryWhereClauseStructure[whereClauseStructureItem][2]["sql"]) or isNumeric(this.queryWhereClauseStructure[whereClauseStructureItem][2]["sql"])>
									<cfset ArraySort( tmpKeyList, "numeric" )>
								<cfelse>
									<cfset ArraySort( tmpKeyList, "textnocase" )>
								</cfif>
							<cfelse> --->
							<cfset ArraySort( tmpKeyList, "numeric" )>
							<cfparam name="form['frmWhereClause#whereClauseStructureItem#']" default="">
							<!--- </cfif> --->
							<cfloop index="whereClauseItem" list="#ArrayToList( tmpKeyList )#">
								
								<cfset whereClauseItemSelected = ( ListFindNoCase( form["frmWhereClause#whereClauseStructureItem#"], this.queryWhereClauseStructure[whereClauseStructureItem][whereClauseItem]["sql"], "|" ) neq 0 )>
								<cfoutput><option value="#HTMLEditFormat( this.queryWhereClauseStructure[whereClauseStructureItem][whereClauseItem]["sql"] )#"#iif( whereClauseItemSelected, de( " selected" ), de( "" ) )#>#this.queryWhereClauseStructure[whereClauseStructureItem][whereClauseItem]["title"]#</option></cfoutput>
							</cfloop>
							<cfoutput></select></cfoutput>
						</cfloop>

					<cfoutput>
							<script language="JavaScript1.2" type="text/javascript">
							<!--
							function eventWhereClauseOnChange( callerObject )
							{
							  var whereClauseSuffixArray = new Array( #ListQualify( StructKeyList( this.queryWhereClauseStructure ), '"' )# );
							  var whereClauseDelimiter = "";
							  with ( callerObject.form )
							  {
							    elements["frmWhereClauseList"].value = "";
							    for ( whereClauseIndex = 0; whereClauseIndex < whereClauseSuffixArray.length; whereClauseIndex++ )
							    {
							      if ( elements["frmWhereClause" + whereClauseSuffixArray[whereClauseIndex]].value != "" )
							      {
							        elements["frmWhereClauseList"].value += whereClauseDelimiter + elements["frmWhereClause" + whereClauseSuffixArray[whereClauseIndex]].value;
							        whereClauseDelimiter = "|";
							      }
							      else
							      {
							        elements["frmWhereClause" + whereClauseSuffixArray[whereClauseIndex]].selectedIndex = 0;
							      }
							    }
							  }
							  return true;
							}
							//-->
							</script>
						</td>
						</tr>
					</cfoutput>
				</cfif>
		<!--- --------------------------------------------------------------------- --->
	
		<cfoutput>
		</form>
		</table>
		<br>
		</cfoutput>

	</cfif>	</cfif>

</cffunction>


<!--- 
This function makes a nice array which can be used to output table headings on multiple rows with colspan grouping
use the delimiter #application.delim1# within a heading to indicate a new row.
If the next heading in the list has the same heading on that row, then they are grouped together with a colspan

 --->
<cffunction name="createHeadingArray" returntype="boolean" hint="Makes an array for easy outputting of titles on multiple rows">

		<cfargument name="columnHeadingList"  type="string" default=#this.columnHeadingList#> 
		<cfargument name="columnNameList"  type="string" default=#this.showTheseColumns#> 
		<cfargument name="columnTranslation"  type="string" default=#this.columnTranslation#> 
		<cfargument name="columnTranslationPrefix"  type="string" default=#this.columnTranslationPrefix#> 		


	
		<cfset var headingArray = arrayNew(1)>
		<cfset var columnListIndex = 0>
		<cfset var numberOfRows = 0> 
		<cfset var thisHeading = ""> 
		<cfset var thisRow = 0> 
		<cfset var cellcounter = 0> 
		<cfset var numberOfColumns = listlen(columnHeadingList)> 
		
	<!--- work out max number of rows --->

	<cfloop index = "thisHeading" list = #columnHeadingList#>
		<cfset numberOfRows = max(numberOfRows,listLen(thisHeading,"#application.delim1#"))>
	</cfloop>
	
		<cfloop index="thisROw" from = 1 to= #numberOfRows#> <!--- loop through each potential row--->
			<cfset headingArray[thisRow] = arrayNew(1)>
			<cfset cellcounter = 0> <!--- counts number of cells across, to index the result array --->
			<cfset columnListIndex = 0> <!--- counts position in the columnHeadingList--->
			<cfloop condition ="columnListIndex LT numberOfColumns" > <!--- loop until we get through the list --->
				<cfset columnListIndex = columnListIndex + 1>
				<cfset thisheading = listgetat(columnheadingList,columnListIndex)>
				<cfif listlen(thisheading,"#application.delim1#") lt thisRow >
					<!--- this heading has less items in it than the row we are upto, therefore all its stuff has already been dealt with --->
				<cfelse >
					<!--- get title for this column and row --->
					<cfset thisRowHeading = listgetat(thisHeading,thisRow,"#application.delim1#")>

					
					<cfif thisRow is listlen(thisheading,"#application.delim1#")> <!--- this is last item in list, therefore give a rowspan to the end of the rows --->
						<cfset rowSpan = numberOfRows - listlen(thisheading,"#application.delim1#") +1>
						<cfset lastItemInColumn = true>
					<cfelse>
						<cfset rowSpan = 1>
						<cfset lastItemInColumn = false>
					</cfif>

					<!--- loop through to see if this item it is same as next, actually loop until the next item is not the same
						each time through the loop we increment the colspan and columnListIndex 
					 --->
					<cfset colspan = 1>
					<cfloop condition='columnListIndex lt numberOfColumns and listlen(listgetat(columnheadingList,columnListIndex+1),"#application.delim1#") gte thisROw and thisRowHeading is listgetat(listgetat(columnheadingList,columnListIndex+1),thisrow,"#application.delim1#") and thisRow is not listlen(thisheading,"#application.delim1#")'>
					<!--- 
						columnListIndex lt listlen(headingList)  -- not last item in list
						listlen(listgetat(headingList,columnListIndex+1),"#application.delim1#") gte thisROw  -- next heading has an item for this row
						thisRowHeading is listgetat(listgetat(headingList,columnListIndex+1),thisrow,"#application.delim1#") -- next heading item same as this one
						and thisRow is not listlen(thisheading,"#application.delim1#")  -- prevents last row items having a colspan (may or may not be desired
					 --->
					
							<cfset colspan = colspan + 1>				
							<cfset columnListIndex= columnListIndex  + 1>	
					</cfloop>
						<!--- ready to 'output' a  cell, pop details into array --->				
						<cfif this.columnTranslation>
							<cfset thisRowHeading = columnTranslationPrefix & thisRowHeading>
						<cfelse>
							<cfset thisRowHeading = replace(thisRowHeading,"_"," ","all")>
						</cfif>

						<cfset cellcounter = cellcounter + 1>
						<cfset headingArray[thisRow][cellcounter]= structNew()>
						<cfset headingArray[thisRow][cellcounter].colspan = colspan>
						<cfset headingArray[thisRow][cellcounter].rowspan = #rowspan#>
						<cfset headingArray[thisRow][cellcounter].content = #thisRowheading#>
						<cfset headingArray[thisRow][cellcounter].lastItemInColumn = lastItemInColumn>
						<cfset headingArray[thisRow][cellcounter].columnName = listgetat(columnNameList,columnListIndex)>
					
			
				</cfif>
		
		
			</cfloop>

		</cfloop>	
	
	<cfset this.columnheadingArray = headingArray>
		
	<cfreturn true>	

</cffunction>

	

 	<cffunction output="true" name="getColumnHeadingsOutput"  access="public" hint="">
		<cfset Var result_html = "">
		<cfset var columnheadingArray = "">

		<cfset initialiseStructuresIfRequired()>
		<cfset createHeadingArray()> 
		<cfset columnheadingArray = this.columnheadingArray>			
		
			<cfsaveContent variable = "result_html">
			<cfoutput>
			
			<script>
				function reSort(sortOrder) {
				var form = document.sortForm;
				form.sortOrder.value = sortOrder;
				form.submit();
				}
			</script>
			
			<cfset outputAForm(formName = "sortForm",dontinclude="sortOrder")>
			
			 <cfloop index = "row" from=1 to = #arrayLen(columnheadingArray)#>
				<tr>
					<cfif this.numberOfGroupByColumns is not 0>
						<TH colspan="#this.numberOfGroupByColumns#">&nbsp;</TH>
					</cfif>
			
			 	<cfloop index = "cell" from =1 to = #arrayLen(columnheadingArray[row])#>
					<cfset thisColumnHeading = columnheadingArray[row][cell].content>
					<cfset thisColumnName = columnheadingArray[row][cell].columnName>
					<th valign=top colspan="#columnheadingArray[row][cell].colspan#" rowspan="#columnheadingArray[row][cell].rowspan#" align="<cfif not columnheadingArray[row][cell].lastItemInColumn>center</cfif>">
								<cfif this.allowColumnSorting eq "yes" and this.openAsExcel eq false and columnheadingArray[row][cell].lastItemInColumn>						
									<cfif thisColumnName eq ListFirst( this.sortOrder, " " )>
										<cfswitch expression="#ListLast( this.sortOrder, " " )#">
										<cfcase value="asc">
											<a href="javaScript:reSort( '#thisColumnName# DESC' );" class="th_link" title="Sort Z-A">
											<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A" height="11" width="12">
										</cfcase>
										<cfcase value="desc">
											<a href="javaScript:reSort( '#thisColumnName# ASC' );" class="th_link" title="Sort A-Z">
											<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z" height="11" width="12">
										</cfcase>
										<cfdefaultcase>
											<a href="javaScript:reSort( '#thisColumnName# DESC' );" class="th_link" title="Sort Z-A">
										</cfdefaultcase>
										</cfswitch>
									<cfelse>
										<a href="javaScript:reSort( '#thisColumnName# ASC' );" class="th_link" title="Sort A-Z">
									</cfif>
									#thisColumnHeading#</a>
								<cfelse>
									&nbsp;#thisColumnHeading#&nbsp;
								</cfif>
					</th>
				</cfloop>
				</tr>
			 </cfloop>
			</cfoutput>
			</cfsaveContent >

		<cfreturn result_html>
	</cffunction>

 	<cffunction output="true" name="getTableBodyOutput"  access="public" hint="">
		<cfargument name="startRow" default = 1>
		<cfargument name="excelRowCount" default = 0>
		
		
		<cfset Var result = "">
		<cfset var groupColStruct = this.groupColStruct>
	
		<cfset initialiseStructuresIfRequired()>
		
		<cfif not listFindnoCAse(this.queryobject.columnlist,"groupGeneration")>
			<cfset groupGeneration = -1>  <!--- taking advantage of loose typing.  This variable will exist if it is not in the query.  Tried doing query addColumn, but it got filled with garbage --->
		</cfif>



		<cfset newGroup = true>
		<!--- if there are no GroupByColumns defined  --->
		<cfset endRow = startRow + this.numRowsPerPage>
		<cfsaveContent variable = "result">
			<cfoutput>		<form action="" name="frmBogusForm"></cfoutput>
			
			<!--- --------------------------------------------------------------------- --->
		<!--- propagate form fields through filtering requests --->
		<!--- --------------------------------------------------------------------- --->
			<cfif IsStruct( this.passThroughVariablesStructure )>
				<cfloop collection="#this.passThroughVariablesStructure#" item="formFieldName">
					<cfif ListFindNoCase( "sortorder,fieldnames", formFieldName ) eq 0>
						<CFOUTPUT><CF_INPUT type="hidden" name="#formFieldName#" value="#this.passThroughVariablesStructure[formFieldName]#"></CFOUTPUT>
					</cfif>
				</cfloop>
			</cfif>
			
			<cfloop query="this.queryObject" startrow="#startrow#" endrow = "#endrow#">
	
				<cfoutput>
					<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				</cfoutput>
	
					<cfif groupGeneration is not -1>
					<!--- This is a heading or subtotal --->
							<cfif structKeyExists (GroupColStruct,groupGeneration)><!--- won't exist for generation 0 - grand total, not sure of best way to deal yet --->
		
								<cfset colspan = groupGeneration - 1>
								<cfif colspan is not 0>
									<cfoutput><td colspan = "#colspan#">&nbsp;</td></cfoutput>		
								</cfif>
								<cfset colspan = this.numberofGroupByColumns - groupGeneration + 1>
								<cfoutput><td colspan = "#colspan#" align="left">
												<cfset columnName = GroupColStruct [groupGeneration]>
												<cfset cellValue = 	this.queryObject[columnName][currentrow]>
												<cfset renderFunction = colStruct[columnName].renderer>
												<cfset renderResult = renderFunction(value=cellValue,outputType = this.outputType,argumentCollection = colStruct[columnName].renderArguments)> 
												<cfset cellValue = renderResult.value>

												<cfif rowType is "groupFooter">
													<cfset cellValue = "Total #cellvalue#">
												</cfif>
												<cfif not this.openAsExcel and colStruct[columnName].hasAnchor>
													<cfset hrefValue = evaluate(colStruct[columnName].keycol.anchorEval)>
													<a href="#hrefValue#" CLASS="smallLink" <cfif colStruct[columnName].keycol.onClickEval is not "">#evaluate(colStruct[columnName].keycol.onClickEval)#</cfif> <cfif colStruct[columnName].keycol.ContextMenu neq ""> onclick = "javascript:fnLeftClickContextMenu () " contextmenu="#colStruct[columnName].keycol.ContextMenu#" id="tbqo--#this.id#--#this.queryObject[this.rowIdentityColumnName][currentrow]#--#i#"</cfif> <!--- was using evaluate(thisKeyColumnContextMenuIDExpression) --->  <cfif colStruct[columnName].keycol.Target neq ""> target="#colStruct[columnName].keycol.Target#"</cfif>><cfif colStruct[columnName].keycol.icon is not ""><img src="/#colStruct[columnName].keycol.Icon#" border="0" alt="#this.queryObject[columnName][currentrow]#"><cfelse>#cellValue#</cfif></A>
												<cfelse>
													#cellValue#
												</cfif> 
								<!--- #rowtype# #groupGeneration# #this.queryObject[GroupColStruct [groupGeneration]][currentrow]# --->
								</td></cfoutput>
							<cfelse>
								<cfif this.numberofGroupByColumns is not 0 ><cfoutput><td colspan = #this.numberofGroupByColumns#> Grand Total </td></cfoutput></cfif>
							</cfif>	
	
					<cfelseif this.numberOfGroupByColumns is not 0>
							<cfoutput><td colspan = "#this.numberOfGroupByColumns#"></td></cfoutput>
					</cfif>						
	
								<cfset excelRowCount = excelRowCount + 1>
								<cfif newGroup><cfset excelBeginCount = excelRowCount><cfset newGroup = false></cfif>
								<cfloop index="columnName" list="#this.showTheseColumns#">
									<cfset thisColumnStruct = this.colstruct[columnname]>	
									<cfif this.openAsExcel and thisColumnStruct.total and groupFormula is not "">
										<cfset excelFormula = " x:fmla='=#replaceNoCase(groupFormula,'ColLetter',thisColumnStruct.Colletter,'ALL')#'">									
									<cfelse>	
										<cfset excelFormula = "">
									</cfif>
									
										<cfset cellValue = 	this.queryObject[columnName][currentrow]>
										<cfset renderFunction = colStruct[columnName].renderer>
										<cfset renderResult = renderFunction(value=cellValue,outputType = this.outputType,argumentCollection = colStruct[columnName].renderArguments)> 
										<cfset cellValue = renderResult.value>

									<cfoutput><td  align="#this.colstruct[columnname].align#" #excelFormula# <cfif not this.openAsExcel and this.showCellColumnHeadings eq "yes">onmouseover="pop('##','cccccc')" onmouseout="kill()"</cfif> <cfif this.openAsExcel and colStruct[columnName].excelcellattribute is not "">#evaluate(colStruct[columnName].excelcellattribute)#</cfif> > 
										<cfif not this.openAsExcel and colStruct[columnName].hasAnchor>
											<cfset hrefValue = evaluate(colStruct[columnName].keycol.anchorEval)>
											<a href="#hrefValue#" CLASS="smallLink" <cfif colStruct[columnName].keycol.ContextMenu neq ""> onclick = "javascript:fnLeftClickContextMenu () " contextmenu="#colStruct[columnName].keycol.ContextMenu#" id="tbqo--#this.id#--#this.queryObject[this.rowIdentityColumnName][currentrow]#--#columnName#"</cfif> <!--- was using evaluate(thisKeyColumnContextMenuIDExpression) --->  <cfif colStruct[columnName].keycol.Target neq ""> target="#colStruct[columnName].keycol.Target#"</cfif>><cfif colStruct[columnName].keycol.Icon is not ""><img src="/#colStruct[columnName].keycol.Icon#" border="0" alt="#this.queryObject[columnName][currentrow]#"><cfelse>#cellValue#</cfif></A>
										<cfelse>
											#cellValue#
										</cfif> 
	
	
									</cfoutput>
					<!--- --------------------------------------------------------------------- --->
					<!--- table inclusion --->
					<!--- --------------------------------------------------------------------- --->
					<!--- 
									<cfif ListFindNoCase( this.columnInclusionList, i ) neq 0>
										<cfinclude template="#ListGetAt( this.columnInclusionTemplateList, ListFindNoCase( this.columnInclusionList, i ) )#">
									</cfif>
					 --->
									<cfif IsDefined( "caller.udfTableCallBack" ) and IsCustomFunction( caller.udfTableCallBack )>
										<cfoutput>#caller.udfTableCallBack( i, this.queryObject[columnName][this.queryObject.CurrentRow], cellType )#</cfoutput>
									</cfif>
					<!--- --------------------------------------------------------------------- --->
									<cfoutput></td></cfoutput>
					
							</cfloop>
					<!--- --------------------------------------------------------------------- --->
					<!--- function select --->
					<!--- --------------------------------------------------------------------- --->
							<cfif this.showFunctionList and this.openAsExcel eq false>
								<cfoutput><td valign="top"><CF_INPUT type="checkbox" name="frmRowIdentity" value="# this.queryObject[this.rowIdentityColumnName][CurrentRow] #" checked="#false#" style="height: 14; width: 14;"></td></cfoutput>
							</cfif>
					<!--- --------------------------------------------------------------------- --->
							<cfoutput></tr></cfoutput>
					
	
			</cfloop>
				</form>
		</cfsavecontent>

				<cfif not newGroup><cfset excelEndCount = excelrowCount><cfset newGroup = true></cfif>	
	<cfreturn result>
	
	</cffunction>

	<cffunction output="true" name="getWholeTableOutputHTML"  access="public" hint="">
			
			<cfset var result = "">	

		<cfif this.outputType is "htmltable">

				<cfif this.showCellColumnHeadings eq "yes">
					<cf_includejavascriptonce template="javascript/popupDescription.js">
				</cfif>
				<cfif this.keyColumnOpenInWindowList is not "" and listFindNoCase(this.keyColumnOpenInWindowList,"yes") neq 0>
					<cf_includejavascriptonce template="/javascript/openWin.js" >
				</cfif>
				<cfif this.OpenAsExcel>
					<cfif not this.saveAsExcel>
						<cfcontent type="application/msexcel">
					</cfif>	
					<cfinclude template = "\relay\templates\excelheaderinclude.cfm">
				</cfif>
				
				
				<cfset result = '<table border="0" cellspacing="1" cellpadding="3" align="#this.tableAlign#" class="#this.tableClass#">'>
				<cfset result = result  & getColumnHeadingsOutput()>
				<cfset result = result  & getTableBodyOutput()>
				<cfset result = result  & "</table>">

			
			<cfelseif this.outputType is "ext">
				<cfoutput>
				<link rel='stylesheet' type='text/css' href='/javascript/ext/resources/css/ext-all.css' />
				<cf_includeJavascriptOnce template="/javascript/ext/adapter/ext/ext-base.js">
				<cf_includeJavascriptOnce template="/javascript/ext/ext-all.js">

						<!--- this Script replaces Ext.util.Format.dateRenderer 
							It handles dates coming straight from the data base in the yyyy-mm-dd hh:mm:ss format
						--->
						<script>
						fnlExtDateRenderer = function (format) {return function (v) {
							dateAndTimeArray = v.split(' ')
							dateArray = dateAndTimeArray[0].split('-')
							if (dateArray[0].length == 4) {
								if (dateAndTimeArray.length > 1) {
									timeArray = dateAndTimeArray[1].split(':')
								} else {
									timeArray = [0,0,0]
								}
								var v = new Date()
								v.setFullYear(dateArray[0]);
								v.setMonth(dateArray[1]-1);
								v.setDate(dateArray[2]);
								v.setHours(timeArray[0])
								v.setMinutes(timeArray[1])
								v.setSeconds(timeArray[2])
								v.setMilliseconds(0)
							}
						  return Ext.util.Format.date(v, format);
						}}
						
						</script>
			


				
				
				<script>
				
				Ext.onReady(function(){

    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());


					    var myData = [
							<cfloop query="this.queryObject" >
								[
								<cfloop index="columnName" list="#this.showTheseColumns#">
									<cfset thiscolumnStruct = this.colstruct[columnname]>	
									<cfset cellValue = 	this.queryObject[columnName][currentrow]>
									<cfset renderFunction = thiscolumnStruct.renderer>
									<cfset renderResult = renderFunction(value=cellValue,outputType = this.outputType,argumentCollection = thiscolumnStruct.renderArguments)> 
									<cfset cellValue = renderResult.value>
									
									'#replace(cellValue,"'","\'")#',
								</cfloop>
									'1'
								]<cfif currentRow is not this.queryObject.recordcount>,</cfif>
							</cfloop>
					    	];

					    var ds = new Ext.data.Store({
					        reader: new Ext.data.ArrayReader({}, [
					               <cfloop index="columnName" list="#this.showTheseColumns#">
								   {name: '#columnName#'},
								   </cfloop>
   								   {name: 'dummy'}
					          ])
						})
					

 					    ds.loadData(myData);

				
				
					    var colModel = new Ext.grid.ColumnModel([
							<cfset firstpass = true>
					       <cfloop index="col" list=#this.showTheseColumns#>
								<cfset thisColStruct = this.colStruct[col]>
								<cfif not firstPass>,</cfif>
		   					      {header: "#replace(thisColStruct.heading,"#application.delim1#","<BR>","ALL")#", <cfif thisColStruct.width is not "">width: #thisColStruct.width#,</cfif><cfif this.allowColumnSorting> sortable: true, </cfif><cfif thisColStruct.extRenderer is not "">renderer: #thisColStruct.extRenderer#,</cfif>   dataIndex: '#Col#'} 
								<cfset firstpass = false>
						   </cfloop>
						   // {id:'company',header: "Company", width: 160, sortable: true, locked:false, dataIndex: 'company'},
					        //{header: "Price", width: 75, sortable: true, renderer: Ext.util.Format.usMoney, dataIndex: 'price'},
					        //{header: "Change", width: 75, sortable: true, renderer: change, dataIndex: 'change'},
					        //{header: "% Change", width: 75, sortable: true, renderer: pctChange, dataIndex: 'pctChange'},
					        //{header: "Last Updated", width: 85, sortable: true, renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'lastChange'}
					    ]);
					
					
					    // create the Grid
					    var grid = new Ext.grid.GridPanel({
					        el:'#this.id#',
					        ds: ds,
					        cm: colModel,
					     
					        height:350,
					        width:1200,
					        title:'#this.ReportTitle#'
					    });
					
					    grid.render();
					
					    grid.getSelectionModel().selectFirstRow();
				
 				})
				</script>
				<div id="#this.id#"></div>
			</cfoutput>
			
			
			</cfif>

			
			<cfreturn result>
			
	</cffunction>


 	<cffunction output="true" name="outputEverything"  access="public" hint="">
	
			<cfset ApplyCalculatedColumsToQuery()>
			<cfif this.outputType is not "ext">
				<cfset ApplyGroupingToQuery()>
			</cfif>

		<cfoutput>
		#outputFilters()#
		#outputPagingControls()#
		#outputWholeTable()#
		</cfoutput>

	
	</cffunction>
	

 	<cffunction output="true" name="outputWholeTable"  access="public" hint="">
			<cfoutput>#getWholeTableOutputHTML()#</cfoutput>

		<cfreturn>
	
	</cffunction>




	<!--- --------------------------------------------------------------------- --->
	<!--- function select --->
	<!--- --------------------------------------------------------------------- --->

	 	<cffunction output="true" name="outputFunctionSelector"  access="public" hint="">

				
				<cfif this.showFunctionList>

					<script>
							var functionObject = new Object();
							
							<cfloop query="this.functionListQuery">
								functionObject["<cfoutput>#JSStringFormat( this.functionListQuery.functionID )#</cfoutput>"] = new Object();
								functionObject["<cfoutput>#JSStringFormat( this.functionListQuery.functionID )#</cfoutput>"]["url"] = "<cfoutput>#JSStringFormat( this.functionListQuery.url )#</cfoutput>";
								functionObject["<cfoutput>#JSStringFormat( this.functionListQuery.functionID )#</cfoutput>"]["windowFeatures"] = "<cfoutput>#JSStringFormat( this.functionListQuery.windowFeatures )#</cfoutput>";
							</cfloop>
							
							function eventFunctionOnChange( callerObject ){
							  
							  var checkboxValueList = "";
							  
							  if ( functionObject[callerObject[callerObject.selectedIndex].value]["url"] != "" ){
							  	
								checkboxValueList = checkboxValuesToList( document.forms["frmBogusForm"]["frmRowIdentity"] );
								var thisForm = document.forms["frmBogusForm"];
							    
								if ( checkboxValueList == "" && functionObject[callerObject[callerObject.selectedIndex].value]["url"].indexOf( "javascript:" ) != 0 ){
							      alert( "Please select one or more rows first" );
							    }
							    
								else if ( functionObject[callerObject[callerObject.selectedIndex].value]["windowFeatures"] != "" ){
								  var newURL = functionObject[callerObject[callerObject.selectedIndex].value]["url"];
								  var strArr = newURL.split("?");
								  var formItemName = "";
								  if (strArr.length >= 2) {
								  	var strArr2 = strArr[1].split("&");
									var strArr3 = strArr2[strArr2.length-1].split("=");
								  	formItemName = strArr3[0];
									//alert((newURL.length - 1) - (formItemName.length));
									var tmpURL = newURL.substring(0,(newURL.length) - (formItemName.length));
									newURL = tmpURL;
								  }
								  //var tmpForm = document.createElement('form');
								  var tmpForm = thisForm;
								  var ninput = document.createElement('input');
								  ninput.type = 'hidden';
								  ninput.name = formItemName;
								  ninput.value = ninput.defaultValue = checkboxValueList;
								  tmpForm.method = "post";
								  tmpForm.action = newURL;
								  tmpForm.onsubmit = window.open( "", "functionWindow", functionObject[callerObject[callerObject.selectedIndex].value]["windowFeatures"], true );
								  tmpForm.target = "functionWindow";
								  
								  tmpForm.appendChild(ninput);
								  //document.body.appendChild(tmpForm);
								  
								  tmpForm.submit();
							    }
							   
							   	else {
							      window.location.href = functionObject[callerObject[callerObject.selectedIndex].value]["url"] + escape( checkboxValueList );
							  	}
							  }
							  
							  callerObject.selectedIndex = 0;
							  
							  return true;
							
							}


						function checkboxValuesToList( sourceCheckbox )
						{
						  var returnValue = "";
						  var checkboxIndex = 0;
						  var listDelimiter = "";
						
						  if ( (sourceCheckbox.checked) )
						  {
						    returnValue = sourceCheckbox.value;
						  }
						  else
						  {
						    for ( checkboxIndex = 0; checkboxIndex < sourceCheckbox.length; checkboxIndex++ )
						    {
						      if ( sourceCheckbox[checkboxIndex].checked )
						      {
						        returnValue += listDelimiter;
						        returnValue += sourceCheckbox[checkboxIndex].value;
						        listDelimiter = ",";
						      }
						    }
						  }
						
						  return returnValue;
						}
						
					
						function checkboxSetAll( checkCheckbox )
						{
						  var checkboxIndex = 0;
						
						  if ( !(document.forms["frmBogusForm"]["frmRowIdentity"].length) )
						  {
						    document.forms["frmBogusForm"]["frmRowIdentity"].checked = checkCheckbox;
						  }
						  else
						  {
						    for ( checkboxIndex = 0; checkboxIndex < document.forms["frmBogusForm"]["frmRowIdentity"].length; checkboxIndex++ )
						    {
						      document.forms["frmBogusForm"]["frmRowIdentity"][checkboxIndex].checked = checkCheckbox;
						    }
						  }
						
						  return true;
						}

						</script>		

					<cfoutput>
					<table width="100%" border="0" cellspacing="0" cellpadding="3" align="#this.tableAlign#" class="#this.tableClass#">
						<tr>
						<cfif this.FunctionListComment eq "yes">
						<!--- MDC 20-01-05 I've added the above parmam into the top of this page
						If you add in the param into the table from query object you will get a translateable phrase
						opposite the function dropdown, (so long as you have cftranslate around the table from query object.
						TO DO: we need to be able to trim the function name to create a unique translation--->
						<cfset G = this.functionListQuery.functionName>
						<cfset C = #replace(G,"_","","ALL")#>
						<cfset B = C>
						<cfset A = #replace(B," ","","ALL")#>
						<td>phr_Function_#A#</td>
						</cfif>
							<td align="right" valign="top">
								<select name="frmFunction" onChange="eventFunctionOnChange(this);">
									<cfloop query="this.functionListQuery">
			
										<cfif this.functionListQuery.securityLevel eq "">
											<cfset securityLevelApproved = true>
										<cfelse>
											<cfset securityLevelApproved = false>
											<cfloop index="securityLevelEntry" list="#this.functionListQuery.securityLevel#">
												<cfif ListFindNoCase( session["securitylevels"], securityLevelEntry ) neq 0>
												<cfset securityLevelApproved = true>
												<cfbreak>
											</cfif>
											</cfloop>
										</cfif>
			
										<cfif securityLevelApproved>
											<option value="#this.functionListQuery.functionID#">#htmleditformat(this.functionListQuery.functionName)#</option>
										</cfif>
									</cfloop>
								</select>
							</td>
						</tr>
					</table>
					</cfoutput>
				</cfif>
			
	
		</cffunction>

		<cffunction name="createStructureOfAllFilterAndPassThroughVariables">
			<cfset this.filterStruct = structNew()>
			
			<cfif IsStruct( this.passThroughVariablesStructure )>
				<cfloop collection="#this.passThroughVariablesStructure#" item="formFieldName">
					<cfif ListFindNoCase( "startrow,numRowsPerPage,frmSearchColumnDataType,frmSearchText,frmSearchColumn,frmSearchComparitor,fieldnames", formFieldName ) eq 0>
						<cfset 	this.filterStruct[formFieldName] = this.passThroughVariablesStructure[formFieldName]>
					</cfif>
				</cfloop>
			</cfif>


				<cfset 	this.filterStruct["sortOrder"] = this.sortOrder>
			<cfset filterfieldlist = "frmSearchColumnDataType,frmSearchText,frmSearchColumn,frmSearchComparitor,filterselect1,filterselectvalues1,filterselect2,filterselectvalues2,frmAlphabeticalIndex,frmAlphabeticalIndexList">
			<cfif trim(this.radioFilterName) neq "">
				<cfset filterfieldlist = listappend(filterfieldlist,this.radioFilterName)>
			</cfif>

			<cfloop list = "#filterfieldlist#" index="filter">
				<cfset 	this.filterStruct[filter] = form[filter]>
			</cfloop>
		
			<cfloop index="i" list="#this.checkBoxFilterName#">
				<!--- if this checkbox is defined in the FORM scope the it was previously checked --->
				<cfif isDefined("FORM.#i#")>
					<cfset 	this.filterStruct[i] = 1>
				</cfif>
			</cfloop>


		</cffunction>

		<cffunction name="outputAllFilterAndPassThroughVariablesAsHIddenFormFieldsHTML">
			<cfset createStructureOfAllFilterAndPassThroughVariables()>
			
			<cfloop collection = #this.filterStruct# item = field>
				<cfif this.filterStruct[field] is not "">
						<cfoutput><CF_INPUT type="hidden" name="#field#" value="#this.filterStruct[field]#">
						</cfoutput>
				</cfif>
			</cfloop>
		
		</cffunction>

		<cffunction name="makeURLVariablesIntoQueryString">
				<cfargument name="dontInclude" type="string" default = "">
				
				<cfset var queryString = "">
					<cfloop collection="#url#" item="urlItem">
						<cfif listfindnocase(dontInclude,urlItem) is 0>
							<cfset queryString = queryString & "&" & urlItem & "=" & URLEncodedFormat( url[urlItem] )>
						</cfif>
					</cfloop>
		
			<cfreturn queryString>
			
		</cffunction>



	 	<cffunction output="true" name="outputPagingControls"  access="public" hint="">
			
			<cfif this.HidePageControls eq "no">
				<cfoutput>
					<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" ALIGN="#this.tableAlign#" CLASS="withBorder">
						<tr>
							<td>&nbsp;</td>
							<td><!---display number of records, if appropriate --->
			
			<!--- --------------------------------------------------------------------- --->
			<!--- recycle url parameters except for startrow and numRowsPerPage --->
			<!--- --------------------------------------------------------------------- --->
			<cfset queryString = makeURLVariablesIntoQueryString(dontinclude = "startROw,numRowsPerPage")>

			<!--- --------------------------------------------------------------------- --->
									<form action="#cgi["script_name"]#?#queryString#" method="post" name="frmRecordPagingForm_#this.id#">
										#htmleditformat(outputAllFilterAndPassThroughVariablesAsHiddenFormFieldsHTML())#
			<!--- --------------------------------------------------------------------- --->
								<cfif this.queryObject.recordCount gt 0>
									<CFIF this.queryObject.recordCount EQ 0>
										phr_Ext_tablequery_norecordsfound
									<CFELSEIF this.queryObject.recordCount EQ 1>
										1 phr_Ext_tablequery_recordwasfound
									<CFELSE>
			<!--- --------------------------------------------------------------------- --->
			<!--- build argument structure for recordPager --->
			<!--- --------------------------------------------------------------------- --->
										<cfset additionalArgsStructure = structCopy(url)>
										<cfset StructAppend( additionalArgsStructure, form, true )>
										<cfset StructDelete( additionalArgsStructure, "startrow" )>
										<cfset StructDelete( additionalArgsStructure, "fieldnames" )>
										<cfloop collection="#additionalArgsStructure#" item="additionalArg">
											<cfif additionalArgsStructure[additionalArg] eq "">
												<cfset StructDelete( additionalArgsStructure, additionalArg )>
											</cfif>
										</cfloop>
			<!--- --------------------------------------------------------------------- --->
										<cfif isDefined("this.numRowsPerPage") and this.numRowsPerPage lt this.queryObject.RecordCount>#this.numRowsPerPage# phr_Ext_tablequery_of</cfif> #this.queryObject.RecordCount# phr_Ext_tablequery_recordsfound.
									&nbsp;<cf_recordPager
												totalRecordCount="#this.queryObject.recordCount#" 
												recordsPerPage="#this.numRowsPerPage#" 
												startRow="#this.startrow#"
			 									additionalArgs="#additionalArgsStructure#"
												labelText="Go to page:"
												caller="#GetFileFromPath( GetCurrentTemplatePath() )#"
												id="#this.id#">
										<CFIF this.queryObject.recordCount gt this.numRowsPerPage>
			
											&nbsp;&nbsp;&nbsp;<a href="javascript:void( document.forms['frmRecordPagingForm_#this.id#']['startRow'].value='1');void(document.forms['frmRecordPagingForm_#this.id#']['numRowsPerPage'].value='#this.queryObject.RecordCount#');document.forms['frmRecordPagingForm_#this.id#'].submit();">phr_Ext_tablequery_ShowAllRecords #this.queryObject.RecordCount#</a>
			<!--- 
											&nbsp;&nbsp;&nbsp;<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?startRow=1&numRowsPerPage=#this.queryObject.RecordCount#">Show All #this.queryObject.RecordCount# Records</a><!--- #request.query_string# --->
			 --->
										</cfif>
									</CFIF>
								</CFIF>
			<!--- --------------------------------------------------------------------- --->
			<!--- paging and filtering --->
			<!--- --------------------------------------------------------------------- --->
								<cfif ListFindNoCase( this.queryObject.ColumnList, this.alphabeticalIndexColumn ) neq 0>
									
									<cfif ListLen( form["frmAlphabeticalIndexList"] ) gt 1>
										<br>
										phr_Ext_tablequery_Showrecordsbeginingwith #htmleditformat(this.alphabeticalIndexColumn)#
										<cfloop index="alphabeticalIndexItem" list="#form["frmAlphabeticalIndexList"]#">
											&nbsp;
											<cfif alphabeticalIndexItem neq form["frmAlphabeticalIndex"]>
												<a href="javascript:void(document.forms['frmRecordPagingForm_#this.id#']['frmAlphabeticalIndex'].value='#JSStringFormat( alphabeticalIndexItem )#');document.forms['frmRecordPagingForm_#this.id#']['startRow'].value='1';document.forms['frmRecordPagingForm_#this.id#'].submit();">
											</cfif>
											#htmleditformat(alphabeticalIndexItem)#
											<cfif alphabeticalIndexItem neq form["frmAlphabeticalIndex"]>
												</a>
											</cfif>
										</cfloop>
										<cfif form["frmAlphabeticalIndex"] neq "">
											&nbsp;
											<a href="javascript:void(document.forms['frmRecordPagingForm_#this.id#']['frmAlphabeticalIndex'].value='');document.forms['frmRecordPagingForm_#this.id#']['startRow'].value='1';document.forms['frmRecordPagingForm_#this.id#'].submit();">*</a>
										</cfif>
									</cfif>
								</cfif>
			<!--- --------------------------------------------------------------------- --->
								</form>
							</TD>

						</tr>
					</TABLE>
				<br>
				</CFOUTPUT>
			</cfif>
		</cffunction>


 	<cffunction output="true" name="outputAForm"  access="public" hint="">
		<cfargument name="formName" type="string" required = yes>
		<cfargument name="dontInclude" type="string" default = "">
	
	<!--- --------------------------------------------------------------------- --->
	<!--- recycle url parameters except for sortOrder --->
	<!--- --------------------------------------------------------------------- --->
	<cfset queryString = makeURLVariablesIntoQueryString(dontinclude = dontinclude)>
	<!--- --------------------------------------------------------------------- --->
	<cfoutput>
	<form action="#cgi["script_name"]#?#queryString#" method="post" name="#formName#">
		<CF_INPUT type="hidden" name="startRow" value="#this.startrow#">
		<CF_INPUT type="hidden" name="numRowsPerPage" value="#this.numRowsPerPage#">
		#htmleditformat(outputAllFilterAndPassThroughVariablesAsHIddenFormFieldsHTML())#
	</form>
	</cfoutput>

</cffunction>

<cffunction name="dateFormatter">
	<cfargument name="value">
	<cfargument name="outputType">
	<cfargument name="datemask" default="medium">
	<cfargument name="timemask" default="">
	<cfset var result = structNew()>
	<cfset result.value = value>
	<cfset result.class = "">


	<cfswitch expression="arguments.outputType">
		<cfcase value="htmlTable">
			<cftry>
				<cfif dateMask is not "">
					<cfset result.value = lsdateFormat(value,datemask)>
				<cfelse>	
					<cfset result.value = "">
				</cfif>
				<cfif timeMask is not "">
					<cfset result.value = result.value & " " & lsTimeFormat(value,timemask)>
				</cfif>
				<cfcatch>
				</cfcatch>
			</cftry>

		</cfcase>
	</cfswitch>


	<cfreturn result>	
</cffunction>

<cffunction name="getExtDateMask">
	<cfargument name="datemask" default="medium">
	<cfargument name="timemask" default="">
	<cfset var extMask = "">
	<!--- dynamically work out the ext date mask from the Locale Specific date.
		probably doesn't support every combination yet
	 --->

	<cfif datemask is not "">
		<cfset formattedDate = lsDateFormat(createDateTime(2001,02,03,04,05,06),datemask)>
		<cfset extmask = replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(formattedDate,"2001","Y"),"01","y"),"02","m"),"03","d"),"February","F"),"Feb","M")>
	</cfif>	
	<cfif timemask is not "">
		<cfset formattedTime = lsTimeFormat(createDateTime(2001,02,03,16,07,08),timemask)>
		<cfset extmask = extmask & ' ' & replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(replaceNoCase(formattedTime,"16","H"),"04","G"),"4","g"),"07","i"),"7","i"),"08","s"),"8","s")>
	</cfif>	

	<cfreturn extmask>	
</cffunction>

<cfset variables.renderers.dateFormatter = dateFormatter>

<cffunction name="numberFormatter">
	<cfargument name="value">
	<cfargument name="outputType">
	<cfargument name="mask" default="0">

	<cfset var actualMask= "">
	<cfset var result = structNew()>
	<cfset result.value = value>
	<cfset result.class = "">

	<cfswitch expression="#mask#">
		<cfcase value="decimal">
			<cfset actualMask = "-_.__">
		</cfcase>
		<cfdefaultcase >
			<cfset actualMask = mask>
		</cfdefaultcase>
	</cfswitch>
	<cftry>
		<cfswitch expression="#arguments.outputType#">
			<cfcase value="htmlTable,ext">
				<cfset result.value =  LSNumberFormat(value,actualmask)>		
			</cfcase>
		</cfswitch>
		<cfcatch>
		</cfcatch>
	</cftry>

	<cfreturn result>
</cffunction>
<cfset variables.renderers.numberFormatter = numberFormatter>


<cffunction name="currencyFormatter">
	<cfargument name="value">
	<cfargument name="outputType">
	<cfargument name="mask" default="local">

	<cfset var result = structNew()>
	<cfset result.value = value>
	<cfset result.class = "">

	<cftry>
		<cfswitch expression="arguments.outputType">
			<cfcase value="htmlTable">
				<cfset result.value =  LSCurrencyFormat(value,mask)>		
			</cfcase>
		</cfswitch>
		<cfcatch>
		</cfcatch>
	</cftry>
	
	<cfreturn result>
</cffunction>
<cfset variables.renderers.currencyFormatter = currencyFormatter>

<cffunction name="defaultFormatter">
	<cfargument name="value">

	<cfset var result = structNew()>
	<cfset result.value = value>
	<cfset result.class = "">
	<cfreturn result>

</cffunction>

<cfset variables.renderers.defaultFormatter = defaultFormatter>



 </cfcomponent>

