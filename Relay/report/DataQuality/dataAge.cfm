<!--- �Relayware. All Rights Reserved 2014 --->

<cf_title>Data Age</cf_title>

<CFPARAM NAME="sortOrder" TYPE="string" DEFAULT="year desc, month desc">
<CFPARAM NAME="maximumRows" TYPE="numeric" DEFAULT="30">
<CFPARAM NAME="startRow" TYPE="numeric" DEFAULT="1">

<CFQUERY name="getOrgDataYearMonth" datasource="#application.sitedatasource#">
	select * from (
		select count(1) as records, year(lastupdated) as Year, month(lastUpdated) as Month
		from organisation with (noLock)
		group by year(lastupdated), month(lastUpdated)
	) as base
order by year desc, month desc
</CFQUERY>

<CF_RelayNavMenu pageTitle="Organisation Record Age by Year and Month" thisDir="data">
</CF_RelayNavMenu>
		
<CF_tableFromQueryObject 
	queryObject="#getOrgDataYearMonth#"
	showTheseColumns="Year,Month,Records"
	numRowsPerPage="#maximumRows#"
	startRow="#startRow#"
	
	HidePageControls="no"
	useInclude = "false"
	sortOrder = "#sortOrder#"
>