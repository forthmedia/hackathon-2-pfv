<!--- �Relayware. All Rights Reserved 2014 --->

<cf_title>Organisation Record Age by Year</cf_title>

<CFPARAM NAME="sortOrder" TYPE="string" DEFAULT="year desc">
<CFPARAM NAME="maximumRows" TYPE="numeric" DEFAULT="30">
<CFPARAM NAME="startRow" TYPE="numeric" DEFAULT="1">

<CFQUERY name="getOrgDataYear" datasource="#application.sitedatasource#">
select * from (
select count(1) as Records, year(lastupdated) as Year
from organisation with (noLock)
group by year(lastupdated)
) as base 
order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>

<CF_RelayNavMenu pageTitle="Organisation Record Age by Year" thisDir="data">
</CF_RelayNavMenu>

<CF_tableFromQueryObject 
	queryObject="#getOrgDataYear#"
	showTheseColumns="Year,Records"
	numRowsPerPage="#maximumRows#"
	startRow="#startRow#"
	
	HidePageControls="no"
	useInclude = "false"
	sortOrder = "#sortOrder#"
>