<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 2012-12-17 PPB Case 429161 avoid a crash if request.currentSite.liveCountryIDs eq "" --->

<cfparam name="allCountries" default="">

<CFQUERY name="checkEmailStatus" datasource="#application.sitedatasource#">
 SELECT c.ISOCode as reportRows, c.countryID, CASE 
         WHEN emailStatus = 0 THEN 'No feedback on email status'
         WHEN emailStatus = -0.5 THEN 'Known good email address'
         WHEN emailStatus = -1.0 THEN 'Blank email address'
         WHEN emailStatus in (1,2,3) THEN 'Email address has failed at least 3 times'
	 	 WHEN emailStatus > 2.5 THEN 'Failed multiple times. Address needs checking'
         WHEN emailStatus in (0.5,1.5,2.5,3.5) THEN 'Email address once succeded but subsequently failed'
END as reportcolumns, count(*) as reportUnits 
	from person p
	INNER JOIN location l on p.locationID = l.locationID
	INNER JOIN country c on c.countryID = l.countryID
	<cfif isDefined("request.currentSite.liveCountryIDs") and allCountries eq "">
		<cfif request.currentSite.liveCountryIDs eq "">			<!--- 2012-12-17 PPB Case 429161 avoid a crash if request.currentSite.liveCountryIDs eq "" --->
			where 1=0 
		<cfelse>
			where c.countryID  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
	</cfif>
	group by c.ISOCode, c.countryID, CASE 
         WHEN emailStatus = 0 THEN 'No feedback on email status'
         WHEN emailStatus = -0.5 THEN 'Known good email address'
         WHEN emailStatus = -1.0 THEN 'Blank email address'
         WHEN emailStatus in (1,2,3) THEN 'Email address has failed at least 3 times'
	 	 WHEN emailStatus > 2.5 THEN 'Failed multiple times. Address needs checking'
         WHEN emailStatus in (0.5,1.5,2.5,3.5) THEN 'Email address once succeded but subsequently failed'
	END 
	order by reportRows, reportcolumns
	
	<!--- select * from vEmailStatus
	<cfif isDefined("request.currentSite.liveCountryIDs") and allCountries eq "">
	where countryID in (#request.currentSite.liveCountryIDs#)
	</cfif>
	order by reportRows, reportcolumns--->
</CFQUERY>


<cf_head>
<cf_title>Email Status Analysis</cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">Email Status</TD>		
	</TR>
	<tr>
		<td><p>This report shows an analysis in real time of the current state of the email addresses in the 
			database for <cfif isDefined("request.currentSite.liveCountryIDs") and allCountries eq "">core countries.<cfelse>all countries.</cfif>.  
			This status is calculated by the bouncebacks that are received by the email system.</p>
			<p><cfif isDefined("request.currentSite.liveCountryIDs") and allCountries eq "">
				<a href="checkEmailStatus.cfm?allCountries=yes">Click here</a> to see all countries
			<cfelse>
				<a href="checkEmailStatus.cfm">Click here</a> to just see core countries
			</cfif></p>
			<p>The report analyses the email Status of person records within the system. This column is updated during the processing of a bulk communications especially at bounceback.<br><br></p>
		</td>
	</tr>
</TABLE>

<cf_RelayCrossTabReport	reportQuery = "#checkEmailStatus#">




