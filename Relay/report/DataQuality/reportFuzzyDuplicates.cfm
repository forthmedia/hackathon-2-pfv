<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			profileCountsByCoutry.cfm	
Author:				SWJ
Date started:		2006-12-20
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:

 --->
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Country">
<cfparam name="numRowsPerPage" default="1000">
<cfparam name="startRow" default="1">

	<cf_head>
		<cf_title>Possible Duplicate Records by Country</cf_title>
	</cf_head>
	
	
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="LEFT" CLASS="SubMenu">Count of possible duplicate records by country</TD>
			<cfoutput>
			<td align="right" class="SubMenu"><a href="#cgi["SCRIPT_NAME"]#?openAsExcel=true" target="_blank">Open In Excel</a></td>
			</cfoutput>
		</TR>
		<TR>
			<TD colspan="2">&nbsp;</TD>
			
		</TR>
	</TABLE>

<cfquery name="GetCountsByCountry" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,0,20,0)#">
select * from (
SELECT     CountryDescription as country,
			(SELECT     COUNT(dedupematchid)
				FROM          dedupeMatches
				WHERE      countryID = c.countryID AND tablename = 'organisation' AND (mergeState = 'T')) AS organisation,
			(SELECT     COUNT(dedupematchid)
				FROM          dedupeMatches
				WHERE      countryID = c.countryID AND tablename = 'location' AND (mergeState = 'T')) AS location,
			(SELECT     COUNT(dedupematchid)
				FROM          dedupeMatches
				WHERE      countryID = c.countryID AND tablename = 'person' AND (mergeState = 'T')) AS person
				FROM         Country c
				WHERE     1=1 AND (CountryID IN (#request.relayCurrentUser.Countrylist#))
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			) base
</cfquery>

<cfquery name="countsByCountry" dbtype="query">
	select *
		from GetCountsByCountry
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cfif IsQuery(GetCountsByCountry)>

<CF_tableFromQueryObject 
	queryObject="#countsByCountry#"
	showTheseColumns="Country,Organisation,Location,Person"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	HidePageControls="yes"
	useInclude = "true"
	
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
>
	
</cfif>



