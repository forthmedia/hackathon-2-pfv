<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		PartnerUsageByCountry.cfm
Author:			SWJ
Date created:	

Description:	Lists the partner facing usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
22 aug 2000 		WAB			Added usergroup table to query to filter out logins by external users
24-Jul-2000			SWJ			I added the link thru to UsageList.cfm via ViewBreakDown
07-Mar-2001			SWJ 		Added left outer join and null test to query to bring back 'parters' only
31-Jul-2001			SWJ			Added #frmGroupCol# throughout to allow users to group by GroupCol
2002-01-31			SWJ			Modified layout
26/July/2007		SSS			Added request.REPORTBEGINDATESet Paramter for the days the report will run for can now be customized in relay.ini.
05-Jan-2009			NJH			Bug Fix Sony1 Support Issue 1511 - Added the siteDef table to get only those logins into the external system. Removed the userGroup table from the query.

--->

<CFPARAM NAME="frmSortOrder" DEFAULT="Country.CountryDescription">
<CFPARAM NAME="frmGroupCol" DEFAULT="Country.CountryDescription">
<CFPARAM NAME="frmDetailCols" DEFAULT="">
<!--- This can also be set up in the relayini file to Customize the number of days it will run for ---> 
<CFPARAM NAME="request.REPORTBEGINDATESet" DEFAULT="30">

<CFSET REPORTBEGINDATE=NOW() - request.REPORTBEGINDATESet>
<CFSET REPORTENDDATE=NOW()>
<CFSET MINLOGINS=3>
<!--- location.siteName, person.Username, person.PersonID, --->

<CFQUERY NAME="Users" datasource="#application.sitedatasource#">
SELECT 	UPPER(#frmGroupCol#) as #RemoveChars(frmGroupCol, 1, Find(".",frmGroupCol))#, 
		#frmDetailCols#
		Max(usage.LoginDate) AS LastLoginDate, 
		Min(usage.LoginDate) AS FirstLoginDate, 
		Count(usage.LoginDate) AS NumberofLogins
FROM Country INNER JOIN ((usage RIGHT JOIN person ON usage.PersonID = person.PersonID) 
			INNER JOIN Location ON person.LocationID = Location.LocationID) ON Country.CountryID = Location.CountryID
			INNER JOIN siteDef on siteDef.siteDefID = usage.siteDefID
			LEFT join usergroup on person.personid = usergroup.personid   <!--- left join added so that report only gives external users --->
WHERE usage.LoginDate >  <cf_queryparam value="#createODBCdate(reportbegindate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
AND usergroup.personid IS NULL <!--- added so that report only gives external users --->
	and siteDef.isInternal <> 1
GROUP BY #frmGroupCol#, #frmDetailCols# person.Password, usage.PersonID
<!--- HAVING 	Count(usage.LoginDate)>3 --->
<!--- AND 	person.Password<>'' --->
ORDER BY <cf_queryObjectName value="#frmGroupCol#"><cfif frmDetailCols neq "">, #frmSortOrder#</CFIF> 
</CFQUERY>



<cf_head>
	<cf_title>Usage List</cf_title>
<SCRIPT type="text/javascript">
<!--
	function ViewBreakDown(personID){
		var form = document.MainForm;
		form.frmPersonID.value = personID;
		form.submit();
	}
//-->
</SCRIPT>
</cf_head>


<CFOUTPUT>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<FORM ACTION="#SCRIPT_NAME#?#request.query_string#" METHOD="post" NAME="searchForm" ID="searchForm">
	<tr><td class="Submenu" COLSPAN="3">Extranet/Portal Usage by Country</td>
	</tr>
	<tr>
	<!--- Use below if you are going to only show people who've logged in more than 3 times.
		<TD COLSPAN="3">
			List of all users who have logged onto the extranet/portal more than #minlogins# times between #dateformat(reportbegindate,'dd-mmm-yy')# and #dateformat(reportenddate,'dd-mmm-yy')#<BR>
		</TD> --->
		
		<TD COLSPAN="3">
			List of all users who have logged onto the extranet/portal between #dateformat(reportbegindate,'dd-mmm-yy')# and #dateformat(reportenddate,'dd-mmm-yy')#<BR>
		</TD>
	</tr>
</CFOUTPUT>
<tr>
	<td>
		<SELECT onChange="submit()" NAME="frmSortOrder">
			<OPTION VALUE="Username">Choose sort order</OPTION>
		   	<OPTION VALUE="NumberofLogins DESC">Number of Logins</OPTION>
		   	<OPTION VALUE="LastLoginDate ASC">Last Login Date</OPTION>
			<OPTION VALUE="SiteName ASC">Company</OPTION>
		</SELECT>	
	</td>
		<td>
		<SELECT onChange="submit()" NAME="frmGroupCol">
			<OPTION VALUE="Country.CountryDescription">Choose group by</OPTION>
		   	<OPTION VALUE="usage.App">Application</OPTION>
			<OPTION VALUE="Country.CountryDescription">Country</OPTION>
			<OPTION VALUE="person.personid">Person</OPTION>
		</SELECT>	
	</td>
	<TD>
		Show Detail? &nbsp;<INPUT TYPE="checkbox" NAME="frmDetailCols" VALUE="location.siteName, person.Username, person.PersonID," onClick="if(frmDetailCols.value) document.searchForm.submit()">
	</TD>
</tr>

</FORM>
</table>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<cfoutput><FORM NAME="MainForm" ACTION="/report/UsageList.cfm" METHOD="POST"></cfoutput>
<INPUT TYPE="hidden" NAME="frmPersonID" VALUE="2">
<CFSET COUNTER=0>

<TR>
	<TH><CFOUTPUT>#RemoveChars(frmGroupCol, 1, Find(".",frmGroupCol))#</CFOUTPUT></TH>
	<cfif frmDetailCols neq ""><TH>Username (company)</TH></cfif>
	<TH>Number of Logins</TH>
	<TH>&nbsp;&nbsp;&nbsp;Last Login&nbsp;&nbsp;&nbsp;</TH>
	<TH>Total Users</TH>
</TR>

<CFOUTPUT QUERY="users" GROUP="#trim(ucase(RemoveChars(frmGroupCol, 1, Find(".",frmGroupCol))))#">
	<TR>
		<TD colspan='10' class='offsetbackground'>#evaluate(trim(UCASE(RemoveChars(frmGroupCol, 1, Find(".",frmGroupCol)))))#</TD>
	</TR>
	<CFSET COUNTRYCOUNTER=0>
	<CFOUTPUT>
		<CFSET COUNTER=COUNTER+1>
		<CFSET COUNTRYCOUNTER=COUNTRYCOUNTER+1>
		<cfif frmDetailCols neq "">
			<TR>
			 	<TD>&nbsp;</TD>
				<cfif frmDetailCols neq ""><TD><A HREF="JavaScript:ViewBreakDown(#PersonID#)">#htmleditformat(username)#</A> (#htmleditformat(sitename)#)</TD></cfif>
			 	<TD ALIGN="CENTER">#htmleditformat(NumberofLogins)#</TD>
				<TD ALIGN="CENTER">#dateformat(LastLoginDate,"dd-mmm-yy")#</TD>
			</TR>
		</cfif>
	 </CFOUTPUT>
	<TR>
		<TD></TD>
		<cfif frmDetailCols neq ""><TD></TD></cfif>
		<TD></TD>
		<TD>Total</TD>
		<TD ALIGN="CENTER">#htmleditformat(countrycounter)#</TD>
	</TR>
	
	
	</CFOUTPUT>
<CFOUTPUT>
<TR><TD></TD>
	<cfif frmDetailCols neq ""><TD></TD></cfif>
	<TD></TD>
	<TD>Grand Total</TD><TD ALIGN="CENTER"> #htmleditformat(Counter)#</TD>
</TR>
</CFOUTPUT>
</FORM>
</TABLE>








