<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		UsageList.cfm
Author:			SWJ
Date created:	

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-10-29	SWJ	Added the frmPersonID cfparam call

--->
<CFPARAM NAME="frmPersonID" TYPE="numeric" DEFAULT="#request.relayCurrentUser.personid#">
<CFPARAM NAME="reportMode" TYPE="string" DEFAULT="standalone">

<CFQUERY NAME="GetEntityUsage" datasource="#application.sitedatasource#" DEBUG>
	SELECT E.Headline, p.firstname, p.lastName, count(*)
	FROM dbo.EntityTracking ET INNER JOIN
	     dbo.Element E ON ET.EntityID = E.id
	inner join person p on p.personid = et.personid
	WHERE (ET.EntityTypeID = 10)
		and ET.Created > '28-oct-01'
	group by E.Headline, p.firstname, p.lastName
</CFQUERY>

<CFIF reportMode eq "Standalone">


	Add in a select box with all personids that have visited in last month
</CFIF>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td colspan="4">Data for frmPersonID: #htmleditformat(frmPersonID)#</td></tr>
	<TR>
		<TH>Element</TH>
		<TH>Date</TH>
		<TH>To</TH>
		<TH>From</TH>
	</TR>
	<CFOUTPUT QUERY="GetEntityUsage" MAXROWS=30>
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		 	<TD ALIGN="CENTER">#htmleditformat(Headline)#</TD>
			<TD ALIGN="CENTER">#dateformat(Created,"dd-mmm-yy")# (#timeformat(Created,"HH:MM")#)</TD>
			<TD>#htmleditformat(ToLoc)#</TD>
			<TD>#htmleditformat(FromLoc)#</TD>
		</TR>	
	</CFOUTPUT>
</TABLE>



