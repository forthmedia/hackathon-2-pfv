<!--- �Relayware. All Rights Reserved 2014 --->
<!---


WAB 2006-06-19 altered forms to be "gets" so that variables are in URL scope and get passed around by tablefromqueryobject

WAB 2007-04-12 made sure that countryfilter was passed around in form
 --->

<cfparam name="recordid">
<cfset url.recordid = recordid>  <!--- allows this variable to be recycled by table from queryobject--->


<cfif isDefined("url.thisDateRange")>
	<cf_dateRangeRetrieve dateRange=#thisDateRange#><!--- returns startDate,EndDate,Range --->
<cfelse>
	<cf_DateRange type="month" value="current" year="#year(now())#">
	<cfparam name="range" default="">
</cfif>

<cfparam name="frmGroupBy" default = "">
<cfparam name="frmGroupBy2" default = "">
<cfif frmGroupBy is frmGroupBy2>
	<cfset frmGroupBy2 = "">
</cfif>

<cfparam name="request.supressOrgsFromReports" default="true">
<cfparam name="numRowsPerPage" default="200">
<cfparam name="startRow" default="1">



		<CFQUERY NAME="GetHitsToThisPage" datasource="#application.sitedatasource#">
		select headline,
		<cfif frmGroupBy is not "">#frmGroupBy#,</cfif>
		<cfif frmGroupBy2 is not "">#frmGroupBy2#,</cfif>
		count(distinct Personid) as distinctHits,
		count(Personid) as hits
		from
		(
		SELECT e.headline ,
		year(et.created) as year,
		month(et.created) as month,
		datepart(wk,et.created) as week,
		convert(varchar,datepart(d,et.created)) +' ' + dateName(month,et.created)  as day,
		p.firstname + ' ' + p.lastname  + ' (' + l.sitename + ')' as Personname,

<!--- 		l.sitename, --->
		et.personid
		FROM
			Element E
				inner JOIN
			EntityTracking ET ON ET.EntityID = E.id and ET.EntityTypeID =  <cf_queryparam value="#application.entityTypeID["element"]#" CFSQLTYPE="CF_SQL_INTEGER" >
				inner JOIN
			 person p ON ET.personID = p.personid
				inner JOIN
			location l on l.locationid = p.locationid
					<cfif isDefined("countryIDfilter") and countryIDFilter neq 0>
					and l.countryid =  <cf_queryparam value="#countryIDfilter#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>

		WHERE
			e.id =  <cf_queryparam value="#recordid#" CFSQLTYPE="CF_SQL_INTEGER" >
			and	et.Created  BETWEEN  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			 		<cfif request.supressOrgsFromReports and (not isDefined("frmShowSuppressedOrgs") or frmShowSuppressedOrgs is "")>
						and p.OrganisationID not IN
						(SELECT     organisationid
							FROM          vOrgsTosupressFromReports)
					</cfif>
		) as aView
		group by headline, year <cfif frmGroupBy is not "">, #frmGroupBy#</cfif><cfif frmGroupBy2 is not "">,#frmGroupBy2#</cfif>
		order by headline, year <cfif frmGroupBy is not "">, #frmGroupBy#</cfif><cfif frmGroupBy2 is not "">,#frmGroupBy2#</cfif>
	</CFQUERY>





<CFQUERY NAME="getLinksFromPage" DATASOURCE="#application.siteDataSource#">
-- report of clicks from a page
select onPage, LinkType, case when linkType = 'advert' then othername + ' - ' + LinkToName else linkToName end as linkname ,
<cfif frmGroupBy is not "">#frmGroupBy#,</cfif>
<cfif frmGroupBy2 is not "">#frmGroupBy2#,</cfif>
count(distinct Personid) as distinctclicks, count(Personid) as clicks
from

(select page.headline as onPage, elementLinkTypeTextID LinkType, host.headline linkToName, link.headline othername,
		year(et.created) as year,
		month(et.created) as month,
		datepart(wk,et.created) as week,
		convert(varchar,datepart(d,et.created)) +' ' + dateName(month,et.created)  as day,
		p.firstname + ' ' + p.lastname  + ' (' + l.sitename + ')' as Personname,
<!--- 		l.sitename, --->
		et.personid
 from
	element page
		inner join
	elementLink el  on el.elementid = page.id
		inner join
	element as link  on link.id  = el.entityid
		inner join
	element as host  on host.id  = el.hostelementid
		inner join
	entityTracking et on el.elementLinkID = et.entityID and  et.entityTypeID =  <cf_queryparam value="#application.entityTypeID["elementLink"]#" CFSQLTYPE="CF_SQL_INTEGER" >
		inner join
			person p on p.personid = et.personid
		inner join
			location l on l.locationid = p.locationid
			<cfif isDefined("countryIDfilter") and countryIDFilter neq 0>
			and l.countryid =  <cf_queryparam value="#countryIDfilter#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>

	where
		el.elementid =  <cf_queryparam value="#recordid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and et.Created  BETWEEN  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	<cfif request.supressOrgsFromReports and (not isDefined("frmShowSuppressedOrgs") or frmShowSuppressedOrgs is "")>
		and p.OrganisationID not IN
			(SELECT     organisationid
				FROM          vOrgsTosupressFromReports)
	</cfif>
)
as aView


	group by  onPage, 	<cfif frmGroupBy is not "">year , #frmGroupBy#,</cfif><cfif frmGroupBy2 is not "">#frmGroupBy2#,</cfif> LinkType, othername, linkToName

	order by  onPage, <cfif frmGroupBy is not "">year , #frmGroupBy#,</cfif><cfif frmGroupBy2 is not "">#frmGroupBy2#,</cfif> LinkType, othername, linkToName

</cfquery>

	<cfoutput>
	<cfif isDefined("frmWizardPage")>
		<form name=mainForm method = get>
			<CF_INPUT type="hidden" name= "frmWizardPage" value = "#frmWizardPage#">
			<input type="hidden" name= "frmTask" value = "">
			<CF_INPUT type="hidden" name= "nextRecordID" value = "#recordid#">
			<CF_INPUT type="hidden" name= "recordid" value = "#recordid#">
		</form>
	</cfif>

	<h2>Analysis of Page Visits</h2>

		<cfform method="get" name="dateRangeForm" id="dateRangeForm">
			<div class="form-group">
				<label for="filterByDate">Filter by date</label>
				<cf_DateRangeSelectBox id="filterByDate" BoxName="thisDateRange" SelectedRange="#range#">
			</div>
			<cfif isDefined("frmWizardPage")>
				<CF_INPUT type="hidden" name= "frmWizardPage" value = "#frmWizardPage#">
				<input type="hidden" name= "frmTask" value = "">
			</cfif>
			<CF_INPUT type="hidden" name= "recordid" value = "#recordid#">
			<cfif isDefined("countryIDfilter") and countryIDFilter neq 0>  <!--- WAB added 2007-04-12 --->
			<CF_INPUT type="hidden" name= "countryIDfilter" value = "#countryIDfilter#">
			</cfif>

			<div class="form-group">
				<label for="frmGroupBy">Categorise by</label>
				<cf_displayValidValues
				formfieldname="frmGroupBy" id="frmGroupBy"
				validvalues="Week,Day,Month,PersonName"
				nullText = ""
				currentValue = #frmGroupBy#
				>
			</div>
			<div class="form-group">
				<label for="frmGroupBy2">and</label>
				<cf_displayValidValues
				formfieldname="frmGroupBy2"
				id="frmGroupBy2"
				validvalues="Week,Day,Month,PersonName"
				nullText = ""
				currentValue = #frmGroupBy2#
				>
			</div>

<!--- 			<input type="checkbox" name="frmGroupBy2" value = "name" <cfif isDefined("frmGroupBy2") and  frmGroupBy2 is "name">checked</cfif>> Show People
 --->
			<div class="checkbox">
				<label>
					<input type="checkbox" name="frmShowSuppressedOrgs" value = "1" <cfif isDefined("frmShowSuppressedOrgs") and  frmShowSuppressedOrgs is 1>checked</cfif>> Include Test and Internal Organisations in Statistics
				</label>
			</div>

			<input type="submit" value="Reload" class="btn btn-primary">
		</cfform>

	</cfoutput>



<cfif 	GetHitsToThisPage.recordcount is not 0>
	<cfoutput>
	<div class="row margin-top-10">
		<div class="col-xs-12">
			<div class="grey-box-top">
				<h2>Page Hits </h2>
				<p>#htmleditformat(GetHitsToThisPage.headline)#</p>
			</div>
		</div>
	</div>
	</cfoutput>

	<cfset columns = "#frmgroupBy#,#frmgroupBy2#,DistinctHits,Hits">
	<cfset columnHeadings = "#frmgroupBy#,#frmgroupBy2#,DistinctHits,Hits">
	<!--- if grouping by person then don't need the distincthits column which will always be 1 --->
	<cfif frmGroupBy is "PersonName" or frmGroupBy2 is "PersonName">
		<cfset x = listfindNoCase(columns,"DistinctHits")>
		<cfset columns = listdeleteAt(columns,x)>
		<cfset columnHeadings = listdeleteAt(columnHeadings,x)>
	</cfif>
	 <cf_tablefromqueryObject
	 	queryobject = #GetHitsToThisPage#
		sortorder = ""
		<!---HidePageControls="no"--->
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"
		showTheseColumns="#columns#"
		columnHeadingList="#columnHeadings#"
		<!---alphabeticalIndexColumn="#frmgroupBy#"	--->
		useinclude=false
	 >

<cfelse>
	<p>There have been no hits to this page</p>

</cfif>

<cfif getLinksFromPage.recordCount is not 0>

	<cfset columns = "#frmgroupBy#,#frmgroupBy2#,LinkType,LinkName,DistinctCLicks,Clicks">
	<cfset columnHeadings = "#frmgroupBy#,#frmgroupBy2#,Type of Link,Page Linked To,Distinct Clicks,Total Clicks">
	<!--- if grouping by person then don't need the distincthits column which will always be 1 --->
	<cfif frmGroupBy is "PersonName" or frmGroupBy2 is "PersonName">
		<cfset x = listfindNoCase(columns,"DistinctClicks")>
		<cfset columns = listdeleteAt(columns,x)>
		<cfset columnHeadings = listdeleteAt(columnHeadings,x)>
	</cfif>



	<div class="row">
		<div class="col-xs-12">
			Clicks on Links on this Page
		</div>
	</div>
	 <cf_tablefromqueryObject
	 	queryobject = #getLinksFromPage#
		sortorder = ""
		<!---HidePageControls="yes"--->
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"
		showTheseColumns="#columns#"
		columnHeadingList="#columnHeadings#"
		useinclude=false
	 >
</cfif>
