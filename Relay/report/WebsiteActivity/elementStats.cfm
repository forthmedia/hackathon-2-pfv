<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			elementStats.cfm
Author:				WAB/SWJ
Date started:		1-Apr-05

Description:		Taken from an original idea by William and made into a report.
					Show the page hits for an element branch

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2010-10-12 Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
2012-10-04	WAB		Case 430963 Remove Excel Header

Possible enhancements:


 --->
<!---
2005-04-06
Display Stats for an element tree

 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Page Stats</cf_title>
	</cf_head>


<cfparam name="treeTopEID" >
<cfparam name="sortOrder" default="Page">
<cfparam name="range" default="">

<CFQUERY NAME="getTree" DATASOURCE="#application.siteDataSource#" cachedwithin="#application.cachedContentTimeSpan#">
	exec getElementBranchV2 @elementid =  <cf_queryparam value="#treeTopEID#" CFSQLTYPE="CF_SQL_INTEGER" > , @personid = -1
</cfquery>
	<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	passThruVars = StructNew();
	</cfscript>

<!--- <cfdump var="#getTree#"> --->

<cfif isDefined("form.thisDateRange")>
	<cf_dateRangeRetrieve dateRange=#thisDateRange#><!--- returns startDate,EndDate,Range --->

	<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	StructInsert(passthruVars, "thisDateRange", form.thisDateRange);
	</cfscript>
<cfelse>
	<cf_DateRange type="month" value="current" year="#year(now())#">
	<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">
</cfif>



<cfif isDefined("form.countryIDfilter")>
	<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	StructInsert(passthruVars, "countryIDfilter", form.countryIDfilter);
	</cfscript>
</cfif>

<cfquery name="getElementCountryIDs" datasource="#application.siteDataSource#">
select 'All countries' as countryDescription, '0' as countryID, 1 as theorder
	UNION
	select distinct c.countryDescription, l.countryID, 2 as theorder
	from element e
		left join entityTracking et
	on e.id = et.entityid and entityTypeID =  <cf_queryparam value="#application.entityTypeID['element']#" CFSQLTYPE="CF_SQL_INTEGER" >
					and et.created  between  <cf_queryparam value="#Startdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND  <cf_queryparam value="#EndDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		inner join person p on p.personid = et.personid
		inner join location l on l.locationid = p.locationid
		inner join country c on c.countryID = l.countryID
	where id  in ( <cf_queryparam value="#valuelist(gettree.node)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	order by theorder, countryDescription
</cfquery>





<cfoutput>
<!--- dummy form and javascript for drillDown remembering date and country parameters--->
<script>
	function drillDown(elementid) {
		document.drillDown.recordid.value = elementid
		document.drillDown.submit()
	}
</script>

<form action="elementStatsIndividual.cfm" method="post" name="drillDown" id="drillDown">
			<cfif isDefined("form.thisDateRange")>
				<CF_INPUT type="hidden" name="thisDateRange" value="#thisDateRange#">
			</cfif>
			<cfif isDefined("form.countryIDfilter")>
				<CF_INPUT type="hidden" name="countryIDfilter" value="#countryIDfilter#">
			</cfif>
			<cfif isDefined("frmShowSuppressedOrgs")>
				<input type="hidden" name="frmShowSuppressedOrgs" value="1">
			</cfif>

		<input type="hidden" name="recordid" value="">
</form>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Analysis of Page Visits</TD>
		<cfif openAsExcel eq false>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
		<cfform method="post" name="excelForm" id="excelForm">
			<cfif isDefined("form.thisDateRange")>
				<CF_INPUT type="hidden" name="thisDateRange" value="#thisDateRange#">
			</cfif>
			<cfif isDefined("form.countryIDfilter")>
				<CF_INPUT type="hidden" name="countryIDfilter" value="#countryIDfilter#">
			</cfif>
			<!--- NJH 2007-03-03 included this parameter when opening as excel, as results on page and in excel where different
					due to this parameter not being passed --->
			<cfif isDefined("frmShowSuppressedOrgs")>
				<input type="hidden" name="frmShowSuppressedOrgs" value="1">
			</cfif>
			<input type="hidden" name="openAsExcel" value="true">
			<a href="##" class="Submenu" onclick="javascript:document.excelForm.submit();">Open in Excel</a>
		</cfform>
		</TD>
		</cfif>
	</TR>
	<cfif openAsExcel eq true>
		<tr>
			<td>Date range: #dateFormat(Startdate,"dd-mmm-yy")# to #dateFormat(endDate,"dd-mmm-yy")#
				<cfif isDefined("form.countryIDfilter") and form.countryIDFilter neq 0>
				Counrty: #htmleditformat(form.countryIDfilter)#
				</cfif>
			</td>
		</tr>
	</cfif>
</TABLE>
</cfoutput>

<cfif openAsExcel eq false>
	<cfform method="post" name="dateRangeForm" id="dateRangeForm">
		<cf_DateRangeSelectBox BoxName="thisDateRange" SelectedRange="#range#">
		<cfselect name="countryIDfilter" size="1" query="getElementCountryIDs"
			value="countryID" display="countryDescription"></cfselect>
		<input type="submit" value="Search">
	</cfform>
</cfif>

<!--- GCC - 2005-09-29 - A_675 Using the generic suppress view in stats reports now --->
<cfparam name="request.supressOrgsFromReports" default="true">
<CFQUERY NAME="getStats" DATASOURCE="#application.siteDataSource#">
	SELECT     e.id, COUNT(et.PersonID) AS personcount, COUNT(DISTINCT et.PersonID) AS distinctpersoncount
	FROM         Person INNER JOIN
	EntityTracking et ON Person.PersonID = et.PersonID RIGHT OUTER JOIN
	Element e ON et.EntityID = e.id AND et.EntityTypeID = 10 AND et.Created  BETWEEN  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	<cfif isDefined("form.countryIDfilter") and form.countryIDFilter neq 0>
		AND et.PersonID IN
	    (SELECT     personID
	    FROM          person p INNER JOIN
	    location l ON l.locationid = p.locationid AND l.countryid =  <cf_queryparam value="#form.countryIDfilter#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfif>
	<cfif request.supressOrgsFromReports and (not isDefined("frmShowSuppressedOrgs") or frmShowSuppressedOrgs is "")>
		AND NOT (Person.OrganisationID IN
		(SELECT     organisationid
		FROM          vOrgsTosupressFromReports))
	</cfif>
	WHERE     (e.id  IN ( <cf_queryparam value="#valuelist(gettree.node)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	GROUP BY e.id, e.Headline
</cfquery>

<cfset lookup = structNew()>
<cfloop query = "getStats">
	<cfset lookup[id] = currentRow>
</cfloop>

<cfset getTreeStats = queryNew("EID,Page,Distinct_Page_Hits,Total_Page_Hits,EIDInBrackets")>
<cfloop query = "getTree">
   <cfset temp = QueryAddRow(getTreeStats)>
   <cfset pageVar = "#repeatstring("&nbsp;&nbsp;",generation)# #name#">
   <cfset Temp = QuerySetCell(getTreeStats, "EID", node)>
   <cfset Temp = QuerySetCell(getTreeStats, "Page", pageVar)>
   <cfset Temp = QuerySetCell(getTreeStats, "Distinct_Page_Hits", getstats.distinctPersonCOunt[lookup[node]])>
   <cfset Temp = QuerySetCell(getTreeStats, "Total_Page_Hits", getstats.PersonCOunt[lookup[node]])>
</cfloop>

<cfquery name="getReportData" dbtype="query">
select EID,Page,Distinct_Page_Hits,Total_Page_Hits
from getTreeStats
order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!--- <cfdump var="#getReportData#"> --->

<cfif IsQuery(getReportData)>
	<!--- WAB 2005-10-10 added  numRowsPerPage = #getReportData.recordcount#, SWJ wanted all items on single page.  previously had only 50 rows, but no controls to move to another page! --->
	<CF_tableFromQueryObject
		queryObject="#getReportData#"

		showTheseColumns="Page,Distinct_Page_Hits,Total_Page_Hits,EID"
		ColumnHeadingList="Page<BR>(Click to Preview),Distinct Page Hits<BR>(Click for Details),Total Page Hits,EID"
		HidePageControls="yes"

		numRowsPerPage = #getReportData.recordcount#
		checkBoxFilterName = "frmShowSuppressedOrgs"
		checkBoxFilterLabel = "Include stats for Test and Internal Organisations"

		keyColumnList="Page,EID,Distinct_Page_Hits,Total_Page_Hits"
		keyColumnURLList="/?eid=,javascript:drillDown(thisurlkey),javascript:drillDown(thisurlkey),javascript:drillDown(thisurlkey)"
		keyColumnKeyList="EID,EID,EID,EID"

		passThroughVariablesStructure="#passThruVars#"

		sortorder="#sortOrder#"
		>
</cfif>




