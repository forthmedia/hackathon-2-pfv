<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File:			chartPie.cfm
Author:			?
Date created:	?

Description:

Modifications:
2011-03-17		NYB		LHID4415 changed cfchart to use style xml in relay/styles/charting 
						requires the release of relay/styles/charting/default_pie.xml

--->

<cf_head>
	<cf_title>Pie Chart</cf_title>
</cf_head>



<!--- <cfscript>
function stripChars(str) {
	x="";
	for (i=1;i lte len(str); i = i + 1) {
		ch = asc(mid(str,i,1));
		if ((ch gte 32 and ch lte 126) or (ch eq 13 or ch eq 10)) {
			x = x & mid(str,i,1);
		}
	}
	return x;
}
</cfscript> --->



<cfif isDefined("session.WDDXQueryPacket")>
	<cfwddx action="WDDX2CFML" input="#session.WDDXQueryPacket#" output="chartQuery">


<cfparam name="chartQuery" type="query">
<cfparam name="chartTitle" type="string" default="">
<cfparam name="itemcolumn" type="string">
<cfparam name="valuecolumn" type="string">
<cfparam name="chartheight" type="numeric" default="275">
<cfparam name="chartwidth" type="numeric" default="550">
<cfparam name="showLegend" type="boolean" default="true"> <!--- NJH 2007-22-02 --->
<cfparam name="colorList" type="string" default=""> <!--- NJH 2007-22-02 --->
<cfparam name="onClickURL" type="string" default="">  <!--- NJH 2007-22-02 --->

<cfoutput><div style="font-weight: bold;">#htmleditformat(chartTitle)#</div></br></cfoutput>
<!--- 2011-03-17 NYB LHID4415 - added style to cfchart --->
<cf_chart
         format="flash"
         chartheight="#chartheight#"
         chartwidth="#chartwidth#"
         seriesplacement="default"
         labelformat="number"
         show3d="yes"
         tipstyle="mouseOver"
         pieslicestyle="sliced"
		 showLegend="#showLegend#"
		 url="#onClickURL#" 
		 style="/styles/charting/default_pie.xml">
   	<cf_chartseries
         type="pie"
         query="chartQuery"
         itemcolumn="#itemcolumn#"
         valuecolumn="#valuecolumn#"
		 colorList="#colorList#">
	</cf_chartseries>
</cf_chart>

<cfelse>
	<p>No data available to chart</p>
</cfif>



