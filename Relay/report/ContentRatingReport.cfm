<!--- �Relayware. All Rights Reserved 2014 --->
<!---  
WAB 2006-12-11 changed entity column to entityTypeID to fit in with all other tables on system (was causing dedupe problems with old name)
--->

<CFPARAM NAME="message" DEFAULT="">

<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
</cf_head>

<cfparam name="entity" default="12">
<cfparam name="thisentityID" default="">
<!---  --->

<cfif TRIM(thisentityID) eq ''>
	<cfquery name="GetRating" datasource="#application.sitedatasource#">
		Select rating.entityID, element.headline, avg(rating.rating) as average, 
			count(rating.rating) as number
			from rating, element
			where rating.entityTypeID =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_INTEGER" > 	
			and rating.entityID = element.ID
		Group by element.headline, rating.entityID
	</cfquery>	
<cfelse>

	<cfquery name="Getheadline" datasource="#application.sitedatasource#">
		Select headline
		from element
		where element.ID =  <cf_queryparam value="#thisentityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>	
	
	<cfquery name="GetRating" datasource="#application.sitedatasource#">
		Select  rating.rating, firstName, lastname
			from rating, person
			where rating.entityTypeID =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_INTEGER" > 	
			and rating.entityID =  <cf_queryparam value="#thisentityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and rating.personID = person.personID
	</cfquery>	
</cfif>



<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td colspan="3"><strong>Content Rating Report</strong></td></tr>
	<TR>
		<th width=300>
			<cfif TRIM(thisentityID) eq ''>
				Content being rated
			<cfelse>
				Rated by
			</cfif>			
		</td>
		<th>
			<cfif TRIM(thisentityID) eq ''>
				Average <br> Rating
			<cfelse>
				Rating
			</cfif>					
		</th>
		<cfif TRIM(thisentityID) eq ''>
			<TH>Number of <br> ratings</TH>
		</cfif>
	</tr>
	<cfloop query='GetRating'>
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		
			<td>
				<cfif TRIM(thisentityID) eq ''>
					<cfoutput><a href='/report/ContentRatingReport.cfm?thisentityID=#entityID#'>#htmleditformat(headline)#</a></cfoutput>
				<cfelse>
					<cfoutput>#htmleditformat(firstname)# #htmleditformat(lastname)#</cfoutput>
				</cfif>			
			</td>
			<td align='center'>
				<cfif TRIM(thisentityID) eq ''>
					<cfoutput>#htmleditformat(average)#</cfoutput>
				<cfelse>
					<cfoutput>#htmleditformat(rating)#</cfoutput>
				</cfif>				
				
			</td>
			<cfif TRIM(thisentityID) eq ''>
				<td align='center'>
						<cfoutput>#htmleditformat(number)#</cfoutput>
				</td>
			</cfif>
		</tr>
	</cfloop>
</table>



