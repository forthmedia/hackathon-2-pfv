<!--- �Relayware. All Rights Reserved 2014 --->
<!-- William's Code for Listing which countries are in which regions-->


<cf_head>
    <cf_title>List of Countries in each Region</cf_title>
</cf_head>

<CFQUERY NAME="CountriesByRegion" datasource="#application.sitedatasource#">
	SELECT 	R.CountryDescription AS Region, 
			C.CountryDescription AS Country
	FROM (Country AS R RIGHT JOIN CountryGroup ON R.CountryID = CountryGroup.CountryGroupID) RIGHT JOIN Country AS C ON CountryGroup.CountryMemberID = C.CountryID
	WHERE isNUll(C.ISOCode,'') <> ''
	and countryGroupid <> countrymemberid
	ORDER BY R.CountryDescription,c.countrydescription
</CFQUERY>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top" CLASS="Submenu">
			Countries by Region
		</TD>
	</TR>
</table>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr>
		<td colspan="2">
			<b> Not assigned to a region</b>
		</td>
	</tr>
	<CFOUTPUT query="CountriesByRegion" group="region">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<td colspan="2">
				<b>#htmleditformat(Region)#</b>
			</td>
		</tr>
	   <CFoutput>
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<td width="10%">&nbsp;</td>
			<td>
				#htmleditformat(Country)#
			</td>
		</tr>
	   </CFoutput>
	</CFOUTPUT>
</table>



