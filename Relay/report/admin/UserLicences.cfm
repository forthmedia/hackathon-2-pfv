<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			UserLicences.cfm	
Author:				???
Date started:		??-???-??
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference

 --->




<cfparam name="openAsExcel" type="boolean" default="false">

	
	
	

	<cf_head>
		<cf_title>User Licences</cf_title>
	</cf_head>
	
	
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">User Licences</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
		<!--- START: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
			<cfoutput><A HREF="javascript:openAsExceljs();" CLASS="Submenu">Excel</a></cfoutput>
		<!--- END: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
		</TD>		
	</TR>
</TABLE>

<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="email">
<cfquery name="getModules" datasource="#application.siteDataSource#">
	select moduleName from [relayWareModule] order by ModuleName
</cfquery>
<cfset relayWareLicenceModules = "">
<!--- START: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<cfset relayWareLicenceModules = valuelist(getModules.moduleName)>
<!--- END: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<!--- START: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<cfquery name="getUserGroups" datasource="#application.siteDataSource#">
	select distinct ug.name from <!--- securitytype st
		inner join rights r on st.securitytypeid = r.securitytypeid
		inner join ---> usergroup ug  with (nolock)<!--- on r.usergroupid = ug.usergroupid --->
		inner join rightsgroup rg with (nolock) on ug.usergroupid = rg.usergroupid
		inner join person p with (nolock) on rg.personid = p.personid
		inner join organisation o with (nolock) on p.organisationid = o.organisationid
	where 1 = 1
		and p.loginexpires > GETDATE()
		and p.personid in (select distinct personid from userGroup)
		and p.organisationid not in (select organisationID from vOrgsToSupressFromReports)
		<!--- and permission > 1 --->
		and ug.personID is null
</cfquery>
<!--- END: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<!--- START: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<cfset UserGroups = ValueList(getUserGroups.name)>
<!--- END: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<cfset showTheseColumns = "Email,OrganisationName,User_Name," & replace(replace(relayWareLicenceModules," ","_","ALL"),"&","","ALL")& "," & replace(replace(replace(replace(UserGroups," ","_","ALL"),"-","","ALL"),".","","ALL"),"&","and","ALL") & ",PersonID">
<cfset filterByList = "OrganisationName">
<!--- START: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<cfquery name="getData" datasource="#application.siteDataSource#" timeout="120">
	select distinct p.email, o.organisationName, p.userName as User_Name,
	<cfloop list="#relayWareLicenceModules#" index="licenceModule">
	<cfoutput>isNULL((select distinct 'x'
		from securitytype st with (nolock) 
			inner join rights r with (nolock) on st.securitytypeid = r.securitytypeid
			inner join usergroup ug with (nolock) on r.usergroupid = ug.usergroupid
			inner join rightsgroup rg with (nolock) on ug.usergroupid = rg.usergroupid
			where rg.personid = p.personid
			and st.relaywareModule =  <cf_queryparam value="#licenceModule#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			and r.permission > 1),' ') as  [#replace(replace(licenceModule," ","_","ALL"),"&","","ALL")#],
	</cfoutput>
	</cfloop>
	<cfloop list="#UserGroups#" index="userGroup">
	<cfoutput>isNULL((select distinct 'x'
		from usergroup ug with (nolock)
		inner join rightsgroup rg with (nolock) on ug.usergroupid = rg.usergroupid
			where rg.personid = p.personid
			and ug.name =  <cf_queryparam value="#UserGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			),' ') as  [#replace(replace(replace(replace(UserGroup," ","_","ALL"),"-","","ALL"),".","","ALL"),"&","and","ALL")#],
	</cfoutput>
	</cfloop>
	p.personid
	from securitytype st
		inner join rights r with (nolock) on st.securitytypeid = r.securitytypeid
		inner join usergroup ug with (nolock) on r.usergroupid = ug.usergroupid
		inner join rightsgroup rg with (nolock) on ug.usergroupid = rg.usergroupid
		inner join person p with (nolock) on rg.personid = p.personid
		inner join organisation o with (nolock) on p.organisationid = o.organisationid
	where 1 = 1
		and p.loginexpires > GETDATE()
		and p.personid in (select distinct personid from userGroup)
		and p.organisationid not in (select organisationID from vOrgsToSupressFromReports)
		and permission > 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">			
		order by <cf_queryObjectName value="#sortOrder#"></cfquery>
<!--- END: 2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference --->
<CF_tableFromQueryObject 
	queryObject="#getData#" 
	numRowsPerPage="1000"
	openAsExcel = "#openAsExcel#"	
	<!--- keyColumnList="User_Group"
	keyColumnURLList="../../admin/UserManagement/UserGroupManagement.cfm?TaskManagement=1&frmUserGroupID="
	keyColumnKeyList="User_Group_ID" --->
	
	showTheseColumns="#showTheseColumns#"

	FilterSelectFieldList="#filterByList#"

	useInclude = "false"
	sortOrder="#sortorder#">

	

