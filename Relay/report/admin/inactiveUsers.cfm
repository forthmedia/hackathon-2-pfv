<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			InactiveUsers.cfm	
Author:				SWJ
Date started:		19-Mar-08
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name="openAsExcel" type="boolean" default="false">
	<cf_head>
		<cf_title>Inactive Users</cf_title>
	</cf_head>
	
	


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">Inactive Users</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="inactiveUsers.cfm?openAsExcel=true"  CLASS="Submenu">Open in Excel</a> 
		</TD>		
	</TR>
</TABLE>



<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="p.FirstName">

<cfquery name="getData" datasource="#application.siteDataSource#">
	select p.Personid, daysSinceLastLogin as [Days_Since_Last_Login], 
		username as [User_Name], organisationname as [Organisation], 
		lastLoginDate as [Last_Login_Date], loginExpires as [Login_Expires],
		DATEDIFF(day, u.created, getdate()) as [Days_Since_Created], p.Email
	from vLastLogin l
	inner join vpeople p on p.personid = l.personid
	inner join usergroup u on u.personid = p.personid
	where daysSinceLastLogin > 90
		and loginExpires > getDate()
		and p.organisationid not in (select organisationID from vOrgsToSupressFromReports)
	order by daysSinceLastLogin
</cfquery>

<CF_tableFromQueryObject 
	queryObject="#getData#" 

	numRowsPerPage="200"
		
	showTheseColumns="PersonID,Days_Since_Last_Login,User_Name,Organisation,Last_Login_Date,Login_Expires,Days_Since_Created,Email"
	allowColumnSorting="yes"
	
	useInclude="false"
	sortOrder="#sortorder#">

	





