<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-02-03		WAB		Sprint Menu Changes.  Rationalise menu XML code.  Access menus through getMenuXMLDoc().  Client extensions dealt with automatically.
 --->
	

<!--- <cfset sortedMenuItems = XmlSearch(application.com.relayMenu.getMenuXMLDoc(), "//MenuData/MenuItem")>  --->  <!--- NJH 2008-02-04 Trend Bug 98. It was trying to read Menu/MenuItem which is not the structure. --->
<cfset sortedMenuItems = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=application.com.relayMenu.getMenuXMLDoc(),xPathString="//MenuData/MenuItem",lockName="applicationMenu")>

<CFQUERY NAME="getCurrentUsers" DATASOURCE="#application.SiteDataSource#">
		select upper(st.shortName) as securityName, permission, st.securityTypeID,ug.name 
	from securityType st
	inner join rights r on r.securityTypeID=st.securityTypeID
	inner join userGroup ug on r.userGroupID=ug.userGroupID
	where ug. personid is null
	order by securityName,permission
</CFQUERY>	
	
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	
	<cfoutput>
		<cfloop index="i" from="1" to="#ArrayLen(sortedMenuItems)#" step="1">
		
		<TR class="evenrow">
			<TD COLSPAN="2">#sortedMenuItems[i].XmlAttributes.ItemName#</TD>
			<td>#sortedMenuItems[i].XmlAttributes.security#</td>
			<TD><CFSET thisShortName = #left(sortedMenuItems[i].XmlAttributes.security,len(sortedMenuItems[i].XmlAttributes.security) - 7)#> <!--- NJH 2008-02-04 was -3, instead of -7--->
				<CFSET thisPermission = #mid(sortedMenuItems[i].XmlAttributes.security,len(sortedMenuItems[i].XmlAttributes.security),1)#>  <!--- NJH 2008-02-04 was -1  --->
				<CFQUERY NAME="UserGroupTasksCheckSecurity" DBTYPE="query">
					select name from getCurrentUsers 
					where securityName = '#ucase(thisShortName)#' 
					and permission = #thisPermission#
				</CFQUERY>
				<cfif UserGroupTasksCheckSecurity.recordCount neq 0>
					#thisShortName#(#thisPermission#) is assigned to #valueList(UserGroupTasksCheckSecurity.name)#
				<cfelse>
					<DIV STYLE="font-weight: bold; color: Red;"> #htmleditformat(thisShortName)#(#htmleditformat(thisPermission)#) is not assigned to a user Group</div>
				</cfif>
			</TD>
		</tr>
			<cfloop index="i2" from="1" to="#ArrayLen(sortedMenuItems[i].XmlChildren)#" step="1">
			<tr><TD>&nbsp;&nbsp;&nbsp;</TD>
				<td>#sortedMenuItems[i].XmlChildren[i2].XmlAttributes.TabText#</td>
				<td>#sortedMenuItems[i].XmlChildren[i2].XmlAttributes.security#</td>
			
			<TD><CFSET thisShortName = #left(sortedMenuItems[i].XmlChildren[i2].XmlAttributes.security,len(sortedMenuItems[i].XmlChildren[i2].XmlAttributes.security) - 7)#> <!--- NJH 2008-02-04 was -3, instead of -7--->
				<CFSET thisPermission = #mid(sortedMenuItems[i].XmlChildren[i2].XmlAttributes.security,len(sortedMenuItems[i].XmlChildren[i2].XmlAttributes.security),1)#> <!--- NJH 2008-02-04 was -1  --->
				<CFQUERY NAME="UserGroupTasksCheckSecurity" DBTYPE="query">
					select name from getCurrentUsers 
					where securityName = '#ucase(thisShortName)#' 
					and permission = #thisPermission#
				</CFQUERY>
				<cfif UserGroupTasksCheckSecurity.recordCount neq 0>
					#thisShortName#(#thisPermission#) is assigned to #valueList(UserGroupTasksCheckSecurity.name)#
				<cfelse>
					<DIV STYLE="font-weight: bold; color: Red;">#htmleditformat(thisShortName)#(#htmleditformat(thisPermission)#) is not assigned to a user Group</div>
				</cfif>
			</TD></tr>
			</cfloop>
		</cfloop>
	</cfoutput>
	
</table>



