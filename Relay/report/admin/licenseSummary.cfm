<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			UserLicences.cfm	
Author:				???
Date started:		??-???-??
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
2008-07-07	AJC		Issue 690: Licensed users report (Added NO LOCK) and filtered Excel download from user preference

 --->

<cfparam name="sortorder" default="License">
<cfparam name="startRow" default=1>
<cfparam name="openAsExcel" type="boolean" default="false">

<cfset showTheseColumns = "License,Number_Invoked">
<cfset filterByList = "License">

	<cf_relayNavMenu>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs();">
	</cf_relayNavMenu>

	<CFQUERY NAME="LicUserGroupQry" datasource="#application.siteDataSource#">
		SELECT * FROM (
			select l.licenseName as license, l.licenseID, count(distinct p.personId) as number_invoked 
			from license l inner join
				licenseOwned lo on l.licenseID = lo.licenseID inner join
				personLicense pl on pl.licenseID = l.licenseID inner join
				person p on p.personID = pl.personID --and p.organisationID not in (select organisationID from vOrgsToSupressFromReports)
			group by l.licenseName, l.licenseID
		) AS q 
		WHERE 1=1 
		<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">			
		ORDER BY <cf_queryObjectName value="#sortorder#">
	</CFQUERY>
	
	<CF_tableFromQueryObject 
		queryObject="#LicUserGroupQry#"
		sortOrder = "#sortOrder#"
		startRow = "#startRow#"
		numRowsPerPage="100"
		numberFormat="number_invoked"
		groupByColumns=""
		totalTheseColumns="number_invoked"
		HideTheseColumns=""
		keyColumnList="license"
		keyColumnURLList=" "
		keyColumnKeyList="licenseID"
		keyColumnOnClickList="javascript:openNewTab('User Licenses'*comma'User Licenses'*comma'/report/admin/userLicences.cfm?licenseID=##licenseID##'*comma'');return false"
		keyColumnOpenInWindowList="no"
		
		FilterSelectFieldList="#filterByList#"
		showTheseColumns="#showTheseColumns#" 
		allowColumnSorting="yes"
		
		rowIdentityColumnName="licenseID"
	>