<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template = "/templates/tableFromQueryHeaderInclude.cfm">

<cf_head>
	<cf_title>Security Types By User Group</cf_title>
</cf_head>




<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="User_Group,relayWare_function">
<CFPARAM NAME="numRowsPerPage" DEFAULT="200">

<cfquery name="changeView" datasource="#application.siteDataSource#">
	ALTER VIEW [dbo].[vSecurityFunctionsByUserGroup]
	AS
	select distinct ug.name as user_Group, 'Access to '+stf.type+': '+stf.description as relayWare_function
		--,st.relayWareModule, st.ShortName, stf.Permission, stf.[Function] 
		from securitytype st
		inner join rights r on st.securitytypeid = r.securitytypeid
		inner join usergroup ug on r.usergroupid = ug.usergroupid
		inner join rightsgroup rg on ug.usergroupid = rg.usergroupid
		INNER JOIN SecurityTypeFunction AS stf ON st.ShortName = stf.SecurityType
		where 1 = 1
		and ug.personid is null

</cfquery>

<cfquery name="getData" datasource="#application.siteDataSource#">
	select * 
		from vSecurityFunctionsByUserGroup 
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">			
		order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
	
<CF_tableFromQueryObject 
	queryObject="#getData#" 
	numRowsPerPage="#numRowsPerPage#"
	
	<!--- keyColumnList="User_Group"
	keyColumnURLList="../../admin/UserManagement/UserGroupManagement.cfm?TaskManagement=1&frmUserGroupID="
	keyColumnKeyList="User_Group_ID" --->
	
	showTheseColumns="User_Group,RelayWare_Function"

	FilterSelectFieldList="User_Group"

	useInclude = "false"
	groupByColumns="User_Group"
	
	sortOrder="#sortorder#">

	

