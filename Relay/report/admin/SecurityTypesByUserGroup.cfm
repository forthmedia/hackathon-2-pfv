<!--- �Relayware. All Rights Reserved 2014 --->

<cfinclude template = "/templates/tableFromQueryHeaderInclude.cfm">

<cf_head>
	<cf_title>Security Types By User Group</cf_title>
</cf_head>




<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="User_Group">
<CFPARAM NAME="numRowsPerPage" DEFAULT="200">

<!--- <cfquery name="changeView" datasource="#application.siteDataSource#">
	ALTER VIEW [dbo].[vSecurityTypesByUserGroup]
	AS
	SELECT DISTINCT 
	     st.ShortName AS Security_Type, r.Permission, st.relayWareModule as relayWare_Module,
	     st.SecurityTypeID AS Security_Type_ID, 
	     ug.Name AS User_Group, 
	     ug.UserGroupID AS User_Group_ID
	FROM SecurityType AS st 	
		INNER JOIN Rights AS r ON r.SecurityTypeID = st.SecurityTypeID 
		INNER JOIN UserGroup AS ug ON r.UserGroupID = ug.UserGroupID
	WHERE (ug.PersonID IS NULL)
</cfquery> --->

<cfquery name="getData" datasource="#application.siteDataSource#">
	select * 
		from vSecurityTypesByUserGroup 
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">			
		order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
	
<CF_tableFromQueryObject 
	queryObject="#getData#" 
	numRowsPerPage="#numRowsPerPage#"
	
	<!--- keyColumnList="User_Group"
	keyColumnURLList="/admin/UserManagement/UserGroupManagement.cfm?TaskManagement=1&frmUserGroupID="
	keyColumnKeyList="User_Group_ID" --->
	
	showTheseColumns="User_Group,Security_Type,Permission,User_Group_ID"

	FilterSelectFieldList="User_Group,Security_Type"

	useInclude = "false"
	groupByColumns="User_Group"
	
	dateFormat="Received,Loaded"
	sortOrder="#sortorder#">

	

