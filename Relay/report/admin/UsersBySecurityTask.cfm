<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			UsersBySecurityTask.cfm	
Author:				SWJ
Date started:		1-Feb-08
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name="openAsExcel" type="boolean" default="false">
	

	<cf_head>
		<cf_title>Users By Security Task</cf_title>
	</cf_head>
	
	


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">Users By Security Task</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="usersBySecurityTask.cfm?openAsExcel=true" CLASS="Submenu">Open in Excel</a> 
		</TD>		
	</TR>
</TABLE>



<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="p.FirstName">

<cfloop list="2,6,12,15,27,29,37,51" index="i">
	<cfquery name="getTaskType" datasource="#application.siteDataSource#">
		SELECT * FROM SecurityType 
		where securityTypeID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<h2>Users with <cfoutput>#htmleditformat(getTaskType.description)#</cfoutput></h2>
	<cfquery name="getData" datasource="#application.siteDataSource#">
	select fullname as User_name, jobDesc as role,organisationName as organisation
		from vPeople
		where personid in (
			SELECT distinct p.personid 
			FROM Rights r 
			inner join RightsGroup rg ON r.UsergroupID = rg.UsergroupID 
			inner join Person p ON rg.PersonID = p.PersonID 
			WHERE r.SecurityTypeID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >  
			and p.loginexpires > getdate()
			GROUP BY p.PersonID 
			having MAX(r.Permission) > 1 
		)
		and organisationID not in (1)
		and organisationID in (select organisationID from vOrgsToSupressFromReports)
	</cfquery>
	
	<CF_tableFromQueryObject 
		queryObject="#getData#" 
		openAsExcel = "#openAsExcel#"
		numRowsPerPage="200"
			
		showTheseColumns="User_Name, Role, Organisation"
		allowColumnSorting="yes"
		
		useInclude="false"
		sortOrder="#sortorder#">
</cfloop>
	





