<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Selections containing a particular person --->

<CFLOOP index="personid" list="#frmpersonid#">

	<CFQUERY name="Selections" dataSource="#application.SiteDataSource#" DEBUG>
	SELECT 	s.Title, 
			s.selectionid, 
			s.created,
			u.name
	FROM 	Selection as s, SelectionTag as st, usergroup as u
	WHERE 	s.selectionid=st.selectionid
			AND s.createdby=u.usergroupid
			AND st.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	ORDER BY s.selectionid DESC
	</CFQUERY>
	
	<CFQUERY name="getperson" dataSource="#application.SiteDataSource#" DEBUG>
	Select 	person.FirstName as firstname,
			person.lastName as lastname
	From 	Person
	Where	Personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<CFOUTPUT>
	Selections containing<BR>
	 #htmleditformat(getperson.Firstname)# #htmleditformat(getperson.lastname)#
	</cfoutput>
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	
	<TR bgcolor="cccccc">
		<TH>Date</TH>
		<TH>Title</TH>
		<TH>Owner</TH>
	</TR>
	
	<CFOUTPUT query="Selections">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		    <TD>#DateFormat(created,"DD/MM/YY")#</TD>
			<TD>#htmleditformat(title)# (#htmleditformat(selectionid)#)</font></TD>
			<TD>#htmleditformat(name)#</TD>
		</TR>
	
	</cfoutput>
	</TABLE>
	
</CFLOOP>
