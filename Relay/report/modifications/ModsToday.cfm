<!--- �Relayware. All Rights Reserved 2014 --->

<!--- If this is the first time you come into this report
then the Form.DateOfMods value will not be set. --->
<CFIF NOT isdefined("form.startdate")>
	<CFSET form.startdate = DateAdd('d',0,dateformat(Now(),"yyyy-mm-dd"))>
	<CFSET form.enddate = DateAdd('d', +1, form.startdate)>
<CFELSE>
	<CFSET form.startdate = DateAdd('d', 0, form.startdate)>
	<CFSET form.enddate = DateAdd('d', +1, form.startdate)>
</CFIF>

 <cfscript>
   dateRange = StructNew();
   StructInsert(dateRange, "startdate", #form.startdate#);
   StructInsert(dateRange, "enddate", #form.enddate#);
</cfscript>

<cfparam name="sortOrder" default="date_Modified DESC">
<cfparam name="numRowsPerPage" default="10">

<cf_title><cfoutput>Modifications to #request.CurrentSite.Title# on #DateFormat("#form.startDate#")# #IIF(isDefined('FILTERSELECT'),DE('(filtered)'),DE(''))#</cfoutput><cf_title>

<CFQUERY NAME="getModDates" datasource="#application.sitedatasource#">
select convert(varchar(4),year(date_Modified))+'-'+convert(varchar(2),month(date_Modified))+'-'+convert(varchar(2),day(date_Modified)) as dateOfMods,
		count(*) as mods
		from vModChanges
		where date_Modified >= DATEADD(day, -30, #form.startdate#)
		group by convert(varchar(4),year(date_Modified))+'-'+convert(varchar(2),month(date_Modified))+'-'+convert(varchar(2),day(date_Modified))
		order by convert(varchar(4),year(date_Modified))+'-'+convert(varchar(2),month(date_Modified))+'-'+convert(varchar(2),day(date_Modified))
</CFQUERY>
		
<CFQUERY NAME="GetData" datasource="#application.sitedatasource#">
	select * from vModChanges 
	WHERE date_Modified  between  <cf_queryparam value="#form.startdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#form.endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		<cfif isDefined("FILTERSELECT")>
 			<cfif FILTERSELECT eq "Table_Name">
					<cfset table_Name = FILTERSELECTVALUES>
					and table_name =  <cf_queryparam value="#table_Name#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				<cfelseif FILTERSELECT eq "Change">
					<cfset change = FILTERSELECTVALUES>
					and Change =  <cf_queryparam value="#change#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				<cfelseif FILTERSELECT eq "Changed_By">
					<cfset changed_By = FILTERSELECTVALUES>
					and Changed_By =  <cf_queryparam value="#Changed_By#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfif>
		</cfif>
	<cfinclude template="../../templates/tableFromQuery-QueryInclude.cfm">
	Order By <cf_queryObjectName value="#sortOrder#">
</CFQUERY>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td class="Submenu"><cfoutput>Modifications to <cfif isDefined("table_Name")>#htmleditformat(table_Name)# data</cfif> records <cfif isDefined("changed_by")>by #htmleditformat(changed_by)#</cfif> 
	on #request.CurrentSite.Title#</cfoutput></td>
	</tr>
<tr>
	<td>
	
<!--- Fill Out report Screen Title --->
<CFOUTPUT>
	This report lists actual updates <cfif isDefined('change')>to #htmleditformat(change)#</cfif> on #request.CurrentSite.Title# <cfif isDefined("table_Name")>#htmleditformat(table_Name)# data</cfif> records <cfif isDefined("changed_by")>by #htmleditformat(changed_by)#</cfif>
	on #DateFormat("#htmleditformat(form.startDate)#")#.  It shows what the updates were and
	who made them.  If the whole record was added no data appears in the 
	Old Value/New Value fields.
	
	If you want to see the changes for another date please select 
	the date from this list: 
<FORM ACTION="#SCRIPT_NAME#?#request.query_string#" METHOD="POST">

</CFOUTPUT>
	<!--- <CFSET loopdate=Now()>
	<SELECT NAME="DateOfMods" onChange="submit()">
	<OPTION VALUE="">Select a date
	<CFLOOP INDEX="LoopCount" FROM="1" TO="60" STEP="1">
		<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#loopdate#")#</CFOUTPUT>
	<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
	</CFLOOP>
	</SELECT> --->
	
	<SELECT NAME="StartDate" onChange="submit()">
		<OPTION VALUE="<cfoutput>#CreateODBCDate(startdate)#</cfoutput>">Choose date from the last 30 days
		<CFOUTPUT QUERY="getModDates">
			<cfif DateFormat(getModDates.DateofMods) eq DateFormat(CreateODBCDate(form.startdate))>
				<OPTION VALUE="#CreateODBCDate(getModDates.dateofMods)#" selected>
			<cfelse>
				<OPTION VALUE="#CreateODBCDate(getModDates.dateofMods)#">
			</cfif>
			#DateFormat(CreateODBCDate(dateOfMods))# (#htmleditformat(mods)# modifications)
		</CFOUTPUT>
	</SELECT>

</FORM>

<CF_tableFromQueryObject 
	queryObject="#GetData#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	hideTheseColumns=""

	FilterSelectFieldList="Change,Changed_By,Table_Name"
	<!---  build a start date/enddate structure and pass in using this--->
	passThroughVariablesStructure = "#dateRange#"
	searchColumnList="recordid"
	searchColumnDataTypeList="1"	
	searchColumnDisplayList="Record ID"
>


</td>
</tr>
</table>
