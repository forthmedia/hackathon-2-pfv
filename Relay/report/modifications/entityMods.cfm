<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
AMENDMENT HISTORY

DATE		DEV'R	DETAILS
2013-07-17	NYB		Case 436116 removed cf_translate 
2013-07-30 	PPB 	Case 436116 additional tweaks to avoid timeout

--->

<cfsetting requesttimeout="600">				<!--- 2013-07-30 PPB Case 436116 --->

<cfparam name="sortOrder" default="DATE_MODIFIED desc">
<cfparam name="maxRowsShowAll" default="3000"><!--- 2013-07-17 NYB Case 436116 added ---> <!--- 2013-07-30 PPB Case 436116 reduced from 5000 to 3000 --->
<cfparam name="maxRows" default="100000"><!--- 2013-07-17 NYB Case 436116 added --->

<cfif isDefined("tablename")>
	<!--- START: 2013-07-17 NYB Case 436116 added --->
	<cfif not structkeyexists(application,"ModOldestDate")>
		<cfset application.ModOldestDate = structnew()>
	</cfif>
	<cfif not structkeyexists(application.ModOldestDate,tablename)>
		<CFQUERY NAME="GetOldestDate" datasource="#application.sitedatasource#">
			select top 1 DATE_MODIFIED from vModChanges mc 
			<cfswitch expression="#tablename#">
				<cfcase value="organisation">
				INNER JOIN organisation o ON o.organisationID = mc.recordid
				</cfcase>
				<cfcase value="person">
				INNER JOIN person p ON o.personID = mc.recordid
				</cfcase>
				<cfcase value="location">
				INNER JOIN location l ON l.locationID = mc.recordid
				</cfcase>
				<cfcase value="opportunity">
				INNER JOIN opportunity o ON o.opportunityID = mc.recordid
				</cfcase>
			</cfswitch>
			where table_name =  <cf_queryparam value="#tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			order by DATE_MODIFIED asc
		</CFQUERY>
		<cfif GetOldestDate.recordCount>
			<cfset application.ModOldestDate[tablename] = GetOldestDate.DATE_MODIFIED>
		<cfelse>
			<cfset application.ModOldestDate[tablename] = now()>
		</cfif>
	</cfif>
	
	<cfset oldestDate = CreateDate(DatePart('yyyy',application.ModOldestDate[tablename]), DatePart('m',application.ModOldestDate[tablename]), 1)>
	<cfset nowDate = CreateDate(DatePart('yyyy',now()), DatePart('m',now()), 1)>
	
	<cfset form.FRMWHERECLAUSEA = "">
	<cfset form.FRMWHERECLAUSEB = "">
	<cfparam name="form.FRMWHERECLAUSEC" default="#dateformat(dateadd('m',-1,nowDate),'mmm-yyyy')#">
	<cfparam name="form.FRMWHERECLAUSED" default="#dateformat(nowDate,'mmm-yyyy')#">
	<cfparam name="form.dayend" default="#DaysInMonth(nowDate)#">
	<cfparam name="form.daystart" default="1">
	<cfset useFullRange = false>
	<cfset usePartialRange = false>

	<cfset arrayIndex = 1>
	<cfset dateIndex = oldestDate>
	
	<cfloop condition = "dateIndex lte nowDate">
		<cfset queryWhereClause.C["#arrayIndex#"] = StructNew()>
		<cfset queryWhereClause.C[arrayIndex].day = "">
		<cfset queryWhereClause.C[arrayIndex].sql = "#dateformat(dateIndex,'mmm-yyyy')#">
		<cfset queryWhereClause.C[arrayIndex].title = queryWhereClause.C[arrayIndex].sql>
		<cfset arrayIndex = arrayIndex+1>
		<cfset dateIndex = DateAdd('m', 1, dateIndex)>
	</cfloop>

	<cfset queryWhereClause.D = Duplicate(queryWhereClause.C)>

	<cfset useDateRange = true>
	<cfset dateRangeFieldName = "DATE_MODIFIED">

	<cfset passthruVars.FRMWHERECLAUSEC = form.FRMWHERECLAUSEC>
	<cfset passthruVars.FRMWHERECLAUSED = form.FRMWHERECLAUSED>

	<!--- END: 2013-07-17 NYB Case 436116 added --->
	<CFQUERY NAME="GetData" datasource="#application.sitedatasource#">
		select top #maxRows+1# mc.* from vModChanges mc <!--- 2013-07-17 NYB Case 436116 changed --->
		<cfswitch expression="#tablename#">
			<cfcase value="organisation">
			INNER JOIN organisation o ON o.organisationID = mc.recordid
			</cfcase>
			<cfcase value="person">
			INNER JOIN person p ON o.personID = mc.recordid
			</cfcase>
			<cfcase value="location">
			INNER JOIN location l ON l.locationID = mc.recordid
			</cfcase>
			<cfcase value="opportunity">
			INNER JOIN opportunity o ON o.opportunityID = mc.recordid
			</cfcase>
		</cfswitch>
		where table_name =  <cf_queryparam value="#tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		<cfif isDefined("entityID")>
			and recordID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		<cfinclude template="../../templates/tableFromQuery-QueryInclude.cfm">
		Order By <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>
	
	<!--- START: 2013-07-17 NYB Case 436116 added --->
	<cfset theNumberResultsLimitedTo = application.com.relayTranslations.translatephrase(phrase="theNumberResultsLimitedTo")>
	<cfif theNumberResultsLimitedTo eq "theNumberResultsLimitedTo">
		<cfset theNumberResultsLimitedTo = "The number of results has been limited to ">
	</cfif>
	<cfset DisplayMessage = "">
	<cfif getData.recordcount gt maxRows>
		<cfset DisplayMessage = theNumberResultsLimitedTo&maxRows>
		<CFQUERY NAME="GetData" dbtype="query" maxrows="#maxRows#">
			select * from GetData   
		</CFQUERY>
	</cfif>	
	<cfset keyColumnURLList="">

	<cfif structkeyexists(form,"NUMROWSPERPAGE") and form.NUMROWSPERPAGE gt maxRowsShowAll>
		<cfset form.NUMROWSPERPAGE = maxRowsShowAll>
		<cfset DisplayMessage = theNumberResultsLimitedTo&maxRowsShowAll>
		<CFQUERY NAME="GetData" dbtype="query" maxrows="#maxRowsShowAll#">
			select * from GetData   
		</CFQUERY>
	</cfif>

	<!--- END: 2013-07-17 NYB Case 436116 --->

	<cfswitch expression="#tablename#">
		<cfcase value="opportunity">
			<cfset keyColumnURLList="/leadmanager/opportunityEdit.cfm?opportunityid=">
		</cfcase>
	</cfswitch>

	
	<cf_head>
	<!--- Fill out report Title --->
		<cf_title><cfoutput>Modifications to #htmleditformat(tableName)# <cfif isDefined("entityID")>recordid #htmleditformat(entityID)#</cfif></cfoutput></cf_title>
	</cf_head>
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<tr>
			<td class="Submenu"><cfoutput>Modifications to #htmleditformat(tableName)# <cfif isDefined("entityID")>recordid #htmleditformat(entityID)#</cfif></cfoutput></td>
		</tr>
	</table>
	<cfif isDefined('form.numRowsPerPage') and form.numRowsPerPage GT 0>
		<cfset variables.numRowsPerPage = form.numRowsPerPage>
	<cfelse>
		<cfset variables.numRowsPerPage = 100>
	</cfif>
	
	<!--- 2013-07-17 NYB Case 436116 removed cf_translate --->
	<CF_tableFromQueryObject 
		queryObject="#GetData#"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#variables.numRowsPerPage#"
		hideTheseColumns=""
	
		keyColumnList="recordID"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="recordID"

		FilterSelectFieldList="Change,Changed_By,Table_Name"
	
		searchColumnList="recordid"
		searchColumnDataTypeList="1"	
		searchColumnDisplayList="Record ID"
		
		DisplayMessage="#DisplayMessage#"  	<!--- 2013-07-17 NYB Case 436116 added --->
	>
	<!--- 2013-07-17 NYB Case 436116 removed cf_translate --->
<cfelse>
You must defined entityID and tablename.	
</cfif>



