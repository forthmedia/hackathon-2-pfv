<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Filename:		CountAPersonFlagInaCountry.cfm
Author:			DJH
Date created:	7th March 2000

Description:

Based on Pauls sql query, but adapted for cold fusion

--->

<!--- depending on the action, display a form to get input output the results --->

<cfparam name="frmAction" default="getData">

<cfswitch expression="#frmAction#">

<cfcase value="getData">

	<!--- query to get the countries --->
	<cfquery name="getCountry" datasource="#application.sitedatasource#">
		select
			countryID,
			countryDescription
		from
			country
		order by
			countryDescription
	</cfquery>
	
	<!--- query to get the flag groups --->
	<cfquery name="getFlagGroup" datasource="#application.sitedatasource#">
		select
			flagGroupID,
			Name
		from
			flagGroup			
		where
			EntityTypeID = (select EntityTypeID from FlagEntityType where tablename = 'person')
		order by
			Name
	</cfquery>

	<!--- query to get the languages --->
	<cfquery name="getLanguage" datasource="#application.sitedatasource#">
		select
			languageID,
			language
		from
			language
		order by
			language
	</cfquery>

	<!--- 
	Client side validation for the form submission - won't let the form be submitted if a value has not been
	selected from all input fields.
	--->
	<SCRIPT type="text/javascript">
		function validateIt(aform)
		{
			if (aform.countryID.selectedIndex > 0 && aform.flagGroupID.selectedIndex > 0 && aform.languageID.selectedIndex > 0)
				return true
			else
			{
				alert("You must choose a value from all fields!")
				return false
			}
		}
	</script>

	<!--- display a form to collect the data from the user --->
	<table border="0">
		<tr>
			<td colspan="2">Please complete all information:</td>
		</tr>
		
		<!--- form top collect user input --->
		<form name="frmToGetData" action="CountAPersonFlagInaCountry.cfm" method="post" onsubmit="return validateIt(this)">

			<!--- when the form is submitted, the action is set to displayResults --->
			<input type="hidden" name="frmAction" value="displayResults">

			<!--- show the data entry fields --->
			<tr>
				<td>Country:</td>
				<td>
					<select name="countryID">
						<option value="">Select a value
						<cfoutput query="getCountry">
							<option value="#countryID#, #countryDescription#">#htmleditformat(CountryDescription)#
						</cfoutput>
					</select>
				</td>
			</tr>

			<tr>
				<td>Flag Group:</td>
				<td>
					<select name="flagGroupID">
						<option value="">Select a value
						<cfoutput query="getFlagGroup">
							<option value="#FlagGroupID#, #Name#">#htmleditformat(Name)#
						</cfoutput>
					</select>
				</td>
			</tr>

			<tr>
				<td>Language:</td>
				<td>
					<select name="languageID">
						<option value="">Select a value
						<cfoutput query="getLanguage">
							<option value="#LanguageID#, #Language#">#htmleditformat(Language)#
						</cfoutput>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" value="Run report"></td>
			</tr>			
		</form>
	</table>
</cfcase>

<cfcase value="displayResults">
	<!--- display results based on the user data collected --->
	
	<!--- the query will take the submitted data and use it to get query results --->
	<cfquery name="results" datasource="#application.sitedatasource#">
	select 
		f.FlagID,
		f.Active, 
		f.cnt, 
		f.FlagName,
		case when p.PhraseText is null then '---' else p.PhraseText end as PhraseUsed 
	from 
		(
		select
			f.FlagID, 
			case active when 1 then 'active' else 'NOTactive' end as Active,
			isnull(p.cnt, 0) as cnt, 
			substring(f.name,1,35) as flagName, 
			f.namephrasetextid
		From 
			flag as f left outer join	
			(
				select 
					b.flagid, 
					count(b.flagid) as cnt
				from 
					person as p 
					inner join booleanflagdata as b on p.personid=b.entityid
				where 
					p.locationid IN (Select Locationid from Location where countryid =  <cf_queryparam value="#listfirst(countryID)#" CFSQLTYPE="CF_SQL_INTEGER" > )
					and b.flagid in (select flagid from flag where flaggroupid =  <cf_queryparam value="#listfirst(flagGroupID)#" CFSQLTYPE="CF_SQL_INTEGER" > )
					group by b.flagid
			) as p
		
			on p.flagid=f.flagid
				
		where 
			f.flaggroupid =  <cf_queryparam value="#listfirst(FlagGroupID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		) as f

		left outer join

		(
			select 
				PhraseTextID, 
				PhraseText
			from 
				phraselist as pl 
				inner join Phrases as p
				on pl.phraseid=p.phraseid and p.languageid =  <cf_queryparam value="#listfirst(LanguageID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		) as p	

		on f.namephrasetextid=p.phrasetextid
	</cfquery>
	
	<!--- then the resulting data will be displayed using a table to format the output --->

	<table border="0">
		<cfoutput>
			<tr><td>FlagGroup: #htmleditformat(listlast(FlagGroupID))#</td></tr>
			<tr><td>In Country: #htmleditformat(listlast(CountryID))#</td></tr>
			<tr><td>With Phrase Language: #htmleditformat(listlast(LanguageID))#</td></tr>		
		</cfoutput>

		<tr>
			<td>Active</td>
			<td>Cnt</td>
			<td>Flag Name</td>
			<td>Phrase Used</td>
		</tr>
		
		<!--- output one table row per query row --->
		<cfoutput query="results">
			<tr>
				<td>#htmleditformat(Active)#</td>
				<td>#htmleditformat(Cnt)#</td>
				<td><a href="CountAPersonFlagInaCountry.cfm?FlagID=#flagID#&frmAction=drilldown">#htmleditformat(FlagName)#</a></td>
				<td>#htmleditformat(PhraseUsed)#</td>
			</tr>
		</cfoutput>
	
	</table>
</cfcase>

<cfcase value="drilldown">
	<cfoutput>
		you chose: FlagID: #htmleditformat(FlagID)#
	</cfoutput>

</cfcase>



</cfswitch>

