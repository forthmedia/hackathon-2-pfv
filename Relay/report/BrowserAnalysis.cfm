<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		UsageList.cfm
Author:			SWJ
Date created:	24-Jul-2000

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
13-03-2014	IH	Case 439143 adjusted GetUsage query to database changes, changed name "domain" back to "app" as "domain" appears to be a reserved word in query of queries

--->

<CFSET REPORTBEGINDATE=NOW()-30>
<CFPARAM NAME="sortOrder" DEFAULT="app, browser">


<CFQUERY NAME="GetUsage" datasource="#application.sitedatasource#">
	select app,browser,hits from (
	SELECT distinct s.domain as app,browser, count(browser) as hits
	FROM usage u
		inner join siteDefDomain s on u.siteDefDomainID = s.Id
	WHERE u.LoginDate >  <cf_queryparam value="#createODBCdate(reportbegindate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	group by domain,browser
	) base
	where 1=1
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Analsysis of Browser usage since <cfoutput>#dateFormat(reportbegindate,"dd-mmm-yy")#</cfoutput></TD>
	</TR>
</TABLE>
<cfif IsQuery(GetUsage)>
	<CF_tableFromQueryObject
		queryObject="#GetUsage#"
		showTheseColumns="App,Browser,Hits"
		HidePageControls="no"
		FilterSelectFieldList="App,Browser"
		sortorder="#sortOrder#"
		>
</cfif>