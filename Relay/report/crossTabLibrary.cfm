<!--- �Relayware. All Rights Reserved 2014 --->

	<cf_includejavascriptonce template = "/javascript/openwin.js">	

<cf_translate>

<cfif not isDefined("URL.reportCrossTabID")>

	<cfset myLinks = application.com.myrelay.getMyLinks(personID=#request.relayCurrentUser.personID#)>
	<cfset linkURLs = ValueList(myLinks.linkLocation)>
	
	<cfquery name="getReportDefinition" datasource="#application.siteDataSource#">
		SELECT reportCrossTabID, reportTitle, reportDescription 
		from reportCrossTab 
		order by reportTitle
	</cfquery>
	
	<cfoutput>
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="LEFT" CLASS="Submenu">Cross Tab Reports</TD>
			<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
				<A HREF="/templates/callRelayRecordManager.cfm?table=reportCrossTab" Class="Submenu">Edit Reports</A> | 
				<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
			</TD>		
		</TR>
	</TABLE>
	</cfoutput>
	
<!---  	<dl style="margin-left: 20px;"> --->
	<table width="100%" cellpadding=0 cellspacing=0 border=0>
	<tr><td colspan="2">&nbsp;</td></tr>
	<cfoutput query="getReportDefinition">

		<tr>
			<td width="25"><cfif not listFindNoCase(linkURLs,"/report/crossTabLibrary.cfm?reportCrossTabID=#getReportDefinition.reportCrossTabID#")><a href="javascript: newWindow = openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/report/crossTabLibrary.cfm?reportCrossTabID=#getReportDefinition.reportCrossTabID#&linkName=#getReportDefinition.reportTitle#&linkGroup=#URLEncodedFormat('Cross Tab')#&linkTypeID=crossTabReport','','width=500,height=50,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no')" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif></td>
			<td class="smallLink"><a href="crossTabLibrary.cfm?reportCrossTabID=#reportCrossTabID#" title="">#htmleditformat(reportTitle)#</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td>#htmleditformat(reportDescription)#</td>
		</tr>
<!--- 			<dd>#reportDescription#</dd> --->
	</cfoutput>
	</table>
<!---  	</dl> --->
<cfelse>
	
	<!--- 2006-07-09 SWJ - Modified to use a new CFC which also caches the queries
	
	<cfquery name="getReportDefinition" datasource="#application.siteDataSource#">
		SELECT * from reportCrossTab where reportCrossTabID = #URL.reportCrossTabID#
	</cfquery>
	
	<cfquery name="getReportData" datasource="#application.siteDataSource#">
		SELECT distinct #getReportDefinition.reportRows# as reportRows, #getReportDefinition.reportCols# as reportcolumns, 
			count(#getReportDefinition.reportUnits#) as reportUnits
		FROM #getReportDefinition.tables#
		WHERE #preserveSingleQuotes(getReportDefinition.whereClause)#
		group by #getReportDefinition.reportRows#, #getReportDefinition.reportCols#
		order by #getReportDefinition.reportRows#, #getReportDefinition.reportCols#
	</cfquery> --->
	<cfset getReportDefinition = application.com.relayReportDataCrossTabs.getCrossTabDefinition(reportCrossTabID=URL.reportCrossTabID)>
	<cfset getReportData = application.com.relayReportDataCrossTabs.getCrossTabData(reportCrossTabID=URL.reportCrossTabID)>
	
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="LEFT" CLASS="Submenu"><CFOUTPUT>#htmleditformat(getReportDefinition.reportTitle)#</CFOUTPUT></TD>
			<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
				<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
			</TD>		
		</TR>
	</TABLE>
	<p><CFOUTPUT>#htmleditformat(getReportDefinition.reportDescription)#</CFOUTPUT></p>
	<cf_RelayCrossTabReport reportQuery = "#getReportData#">
	 
</cfif>
</cf_translate>



