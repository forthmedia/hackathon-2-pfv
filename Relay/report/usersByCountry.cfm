<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		WabUsersByCountry.cfm
Author:			WAB
Date created:	

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
22 aug 2000 		WAB			Added usergroup table to query to filter out logins by external users
24-Jul-2000			SWJ			I added the link thru to UsageList.cfm via ViewBreakDown
05-Jan-2009			NJH			Bug Fix Sony1 Support Issue 1511 - Added the siteDef table to get only those logins into the internal system.


--->

<CFSET REPORTBEGINDATE=NOW()-30>
<CFSET REPORTENDDATE=NOW()>
<CFSET MINLOGINS=3>
<CFPARAM NAME="frmSortOrder" DEFAULT="UserName ASC">


<CFQUERY NAME="Users" datasource="#application.sitedatasource#">
SELECT 	Country.CountryDescription, location.siteName, 
		person.Username, person.PersonID,
		Max(usage.LoginDate) AS LastLoginDate, 
		Min(usage.LoginDate) AS FirstLoginDate, 
		Count(usage.LoginDate) AS NumberofLogins
FROM Country INNER JOIN ((usage RIGHT JOIN person ON usage.PersonID = person.PersonID) 
			INNER JOIN Location ON person.LocationID = Location.LocationID) ON Country.CountryID = Location.CountryID
			inner join usergroup on person.personid = usergroup.personid   <!--- added so that report only gives internal users --->
			inner join siteDef on usage.siteDefID = siteDef.siteDefID
WHERE usage.LoginDate>dateAdd(m,-1,getDate())
	and siteDef.isInternal = 1
GROUP BY Country.CountryDescription, person.Username, person.PersonID, person.Password, usage.PersonID, location.sitename
HAVING 	Count(usage.LoginDate) >  <cf_queryparam value="#minLogins#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND 	person.Password<>''
ORDER BY Countrydescription, #frmSortOrder# 
</CFQUERY>



<cf_head>
	<cf_title>Usage List</cf_title>
<SCRIPT type="text/javascript">
<!--
	function ViewBreakDown(personID){
		var form = document.MainForm;
		form.frmPersonID.value = personID;
		form.submit();
	}
//-->
</SCRIPT>
</cf_head>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td class="Submenu"><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT> Intranet Users by Country</td>
	<tr>
		<td>
			<CFOUTPUT>
			List of all users who have logged on more than #htmleditformat(minlogins)# times <BR>
			between #dateformat(reportbegindate,'dd-mmm-yy')# and #dateformat(reportenddate,'dd-mmm-yy')#<BR>
			</CFOUTPUT>
		</td>
	</tr>
	<tr>
		<td align="right">
			<cfoutput><FORM ACTION="#SCRIPT_NAME#?#request.query_string#" METHOD="POST"></cfoutput>
			<SELECT onChange="submit()" NAME="frmSortOrder">
				<OPTION VALUE="Username">Choose sort order</OPTION>
			   	<OPTION VALUE="NumberofLogins DESC">Number of Logins</OPTION>
			   	<OPTION VALUE="LastLoginDate ASC">Last Login Date</OPTION>
				<OPTION VALUE="SiteName ASC">Company</OPTION>
			</SELECT>
			</FORM>
		</td>
	</tr>
</table>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<cfoutput><FORM NAME="MainForm" ACTION="/report/UsageList.cfm" METHOD="POST"></cfoutput>
		<INPUT TYPE="hidden" NAME="frmPersonID" VALUE="2">
		<CFSET COUNTER=0>
		
		<TR>
			<TH>Country</TH>
			<TH>Username (company)</TH>
			<TH>Number of Logins</TH>
			<TH>Last Login</TH>
			<TH>Total Users</TH>
		</TR>
		<CFOUTPUT QUERY="users" GROUP="CountryDescription">
			<TR>
				<TD colspan="4"><H2>#htmleditformat(CountryDescription)#</H2></TD>
			</TR>
			<CFSET COUNTRYCOUNTER=0>
			<CFOUTPUT>
				<CFSET COUNTER=COUNTER+1>
				<CFSET COUNTRYCOUNTER=COUNTRYCOUNTER+1>
				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				 	<TD>&nbsp;</TD>
					<TD><A HREF="JavaScript:ViewBreakDown(#PersonID#)">#htmleditformat(username)#</A> (#htmleditformat(sitename)#)</TD>
				 	<TD ALIGN="CENTER">#htmleditformat(NumberofLogins)#</TD>
					<TD ALIGN="CENTER">#dateformat(LastLoginDate,"dd-mmm-yy")#</TD>
					<TD>&nbsp;</TD>
				</TR>
			</CFOUTPUT>
			<TR>
				<TD colspan="4" align="right">Total</TD>
				<TD ALIGN="CENTER">#htmleditformat(countrycounter)#</TD>
			</TR>
		</CFOUTPUT>
		<TR>
			<TD colspan="4" align="right">Grand Total</TD>
			<TD ALIGN="CENTER"><cfoutput>#htmleditformat(Counter)#</cfoutput></TD>
		</TR>
	</FORM>
</TABLE>



