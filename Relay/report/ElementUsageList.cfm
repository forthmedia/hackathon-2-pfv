<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		UsageList.cfm
Author:			SWJ
Date created:	

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-10-29	SWJ	Added the frmPersonID cfparam call
2002-01-31	SWJ	Modified layout

2005-10-11	WAb added a date filter
2013/11/04	NJH Case 437772 - re-wrote the queries so that click thrus are brought back in the main query.
--->

<cf_head>
<SCRIPT type="text/javascript">
<!--
	function viewOrg(orgID){
		form = document.dataFrame;
		form.frmsrchOrgID.value = orgID;
		form.submit();
	}
//-->
</SCRIPT>

</cf_head>


<CFPARAM NAME="frmPersonID" TYPE="numeric" DEFAULT="#request.relayCurrentUser.personid#">
<CFPARAM NAME="reportMode" TYPE="string" DEFAULT="standalone">

<!--- NJH 2007-01-31 --->
<cfparam name="sortColumn" type="string" default="headline">

<cfif structKeyExists(form,"thisDateRange")>
	<cf_dateRangeRetrieve dateRange=#thisDateRange#><!--- returns startDate,EndDate,Range --->
<cfelse>
	<cf_DateRange type="month" value="current" year="#year(now())#">
	<cfparam name="range" default="">
</cfif>



<CFIF isDefined("ElementToView") AND ElementToView GT 0>
	<CFQUERY NAME="GetElementUsage" datasource="#application.sitedatasource#">
		SELECT distinct ET.EntityID, case when clickThru = 1 then toLoc else E.Headline end as headline, o.organisationName, o.organisationID,
			p.firstName+' '+p.lastName as fullName, 
			convert(datetime,convert(varchar(2),(datePart(mm,et.created)))+'-'+convert(varchar(2),(datePart(dd,et.created)))+'-'+convert(varchar(4),(datePart(yy,et.created)))) as dateOrder,
			p.personid,clickThru
		FROM EntityTracking ET INNER JOIN
		     Element E ON ET.EntityID = E.id INNER JOIN
			 person p ON ET.personID = p.personid INNER JOIN organisation o
			 on p.organisationID = o.organisationID
		WHERE (ET.EntityTypeID = 10)
			and et.Created  BETWEEN  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
			AND e.id =  <cf_queryparam value="#elementToView#" CFSQLTYPE="CF_SQL_INTEGER" > 
		order by dateOrder desc,fullname DESC,clickThru
	</CFQUERY>
</CFIF>

<CFQUERY NAME="TheElements" datasource="#application.sitedatasource#">
	SELECT DISTINCT e.id, e.headline as ThisElement
	FROM EntityTracking ET 
	INNER JOIN Element E ON ET.EntityID = E.id 
	WHERE (ET.EntityTypeID = 10)
	AND ET.Created > getdate() - 90
	ORDER BY e.#sortColumn#			<!--- was e.headline NJH 2007-01-31 --->
</CFQUERY>




<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	
	
	<CFIF reportMode eq "Standalone">
	<FORM NAME="FilterForm" METHOD="post">
		<tr><td><CFOUTPUT><strong>Web page tracking</strong></CFOUTPUT></td>
		<TD align="right">Select page</TD>
		<TD><SELECT NAME="ElementToView" ONCHANGE="document.FilterForm.submit()">
			<OPTION Value = 0> Select a page
			<CFOUTPUT QUERY="TheElements">
				<OPTION VALUE="#id#"<CFIF isdefined("ElementToView") AND ElementToView EQ id> SELECTED</CFIF> >#htmleditformat(id)# #htmleditformat(ThisElement)#
			</CFOUTPUT>
		
		</SELECT>
		</TD>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<cf_DateRangeSelectBox BoxName="thisDateRange" SelectedRange="#range#" passthrough="onchange=document.FilterForm.submit()">
			</td>
		</tr>
		</FORM>
	</CFIF>
	<CFIF isDefined("ElementToView") AND ElementToView GT 0>
			<TR>
				<TH>Page</TH>
				<TH>Visitor</TH>
				<TH>Organisation</TH>
			</TR>
		<CFOUTPUT QUERY="GetElementUsage" GROUP="dateOrder">
			<cfset outerLoopDate = dateformat(dateOrder,"yyyy-mm-dd")>
			<TR>
				<Td COLSPAN="3"><strong>#dateformat(dateOrder,"dd-mmm-yy")#</strong></Td>
			</tr>
				<CFOUTPUT>
					<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
						<cfif not clickThru>
					 		<TD>#htmleditformat(Headline)#</TD>
							<td>#htmleditformat(Fullname)#</td>
							<td><A HREF="javaScript:viewOrg(#organisationID#)">#htmleditformat(OrganisationName)#</A></td>
						<cfelse>
							<TD colspan="3" style="padding-left:50px;">Click Thru: <a class="smalllink" href="#Headline#" target="_blank">#htmleditformat(Headline)#</a></TD>
						</cfif>
					</TR>	
				</CFOUTPUT>
		</CFOUTPUT>
	<CFELSE>
		<TR>
			<TD>Please select an element to view visits to that element.</TD>
		</TR>
	</CFIF>
</TABLE>

<CFOUTPUT>
	<FORM ACTION="/data/dataFrame.cfm" METHOD="post" NAME="dataFrame" ID="dataFrame">
		<INPUT TYPE="hidden" NAME="frmsrchOrgID" VALUE="0">
		<CF_INPUT TYPE="hidden" NAME="frmNext" VALUE="/report/ElementUsageList.cfm?#request.query_string#">
	</FORM>
</CFOUTPUT>
