<!--- �Relayware. All Rights Reserved 2014 --->
<cfoutput>
<style>
	/*controls entire table for widget. set width and height elements here.*/
	.HPW_TopTable {
		height: 100%;
		width: 100% !important;
	}
	/*controls the header td... set bg colour, heights and widths here. is on same level as rico rounding style, so will be affected and must work in conjunction.*/
	.HPW_HeaderTD {
		/*background-color: ##13516A; */
		vertical-align: top;
		height: 15px;
	}
	/*controls the table wrapping header text and morelink.*/
	.HPW_HeaderTable {
		/*background-color: ##13516A; */
		padding: 0;
		width: 100%;
		/*
		background-image: url(/styles/cachedBGImages/extbg.gif);
		background-repeat:repeat-x;
		*/
	}

	table.HPW_HeaderTable td.firstedge {
		background-image: url(/styles/cachedBGImages/extbgLeftCnr.gif);
		text-align:left;
		background-repeat:repeat-x;
	}
	table.HPW_HeaderTable td.secondedge {
		background-image: url(/styles/cachedBGImages/extbgRightCnr.gif);
		text-align:right;
		background-repeat:repeat-x;
	}
	table.HPW_HeaderTable td.firstedge, table.HPW_HeaderTable td.secondedge {
		width:5px;
	}
	table.HPW_HeaderTable td.HPW_HeaderText,table.HPW_HeaderTable td.HPW_MoreLinkTD {
		background-image: url(/styles/cachedBGImages/extbgStretch.gif);
		background-repeat:repeat-x;
	}
	table.HPW_HeaderTable,table.HPW_HeaderTable td {
		height:20px;
	}
	/*use to specify shared atributes of header text and morelink.*/
	.HPW_HeaderText, .HPW_MoreLinkTD {
		color: ##15428b;
		padding-top: 2px;
		padding-bottom: 4px;
	}
	/*td in which header text sits. specify fonts etc here.*/
	.HPW_HeaderText {
		padding-left: 15px;
		color: ##15428b;
	}
	/*controls td containing morelink*/
	.HPW_MoreLinkTD {
		float:right;
		font-size:16px;
	}
	/*controls the A element wraping morelink text.*/
	.HPW_MoreLink {
		padding-left: 6px;
	}


	/*hover state for morelink text*/

	/*overall content area (td). set outer border style in here or in HPW_ContentTable.*/
	.HPW_Content {
		border: 1px solid ##99BBE8;
		vertical-align: top;
	}
	/*controls content area table (including the icon area). set outer border style here or in HPW_Content*/
	.HPW_ContentTable {
		height: 100%;
	}
	/*controls td wrapping icon img tag. specify bg colour here.*/

	/*associated to the img tag for icon. image is set via customtag... this could be used to standardise width and height etc.*/
	.HPW_ContentIconIMG {

	}

	.HPW_ContentIconTD {float:left; width:15%;}
	.HPW_ContentIconTD img { width:100%; margin:1em;}
	/*controls td which wraps display handler with content.*/
	.HPW_ContentTD {
		vertical-align: top;
		width: 100%;
	}


</style>

<!--- RICO ROUNDING STYLES --->
<!--- <script src="/javascript/prototype.js"></script>
<script src="/javascript/rico.js"></script>

<script>
	function bodyOnLoad() {
		new Rico.Effect.Round( null, 'HPW_Rico_RoundHeader', {corners:"tl",blend:false} );
	}
</script> --->
</cfoutput>