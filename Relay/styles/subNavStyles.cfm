<!--- �Relayware. All Rights Reserved 2014 --->
<cfoutput>
<style type="text/css">
/* use to manage whole subnav. things like width and bgcolor that'll sit behind all tabs */
.SubNav {
	width: 100%;
	height: 30px;
}
html>body .SubNav {
	height: 29px;
}
/* use to specify the space between the page's left margin and the first tab. */
.FirstSpacer {
	width: 10px;
	background-image: url("/styles/cachedBGImages/bottom_far_right.gif");
	background-repeat: repeat-x;
	background-position: 0% 98%;
}
/*use to maitain width on td in page is required. */
.FirstSpacerDivSpacer {
	width: 10px;
	height: 0px;
	font-size: 0px;
}
/* use to specify the space between the last tab and the caller page tab. especially usefull if the caller page tab is aligned left... but will also maintain a space if there are many tabs or if the page is resized much smaller */
.LastSpacer {
	width: 100%;
	background-image: url("/styles/cachedBGImages/sn_bottom_right.gif");
	background-repeat: repeat-x;
	background-position: 0% 98%;
}
/* use to specify width between tabs. this will not affect the space between the left margin and the first tab nor will it affect the space between the last tab and the caller page tab. */
.TabSpacer {
	width: 5px;
	background-image: url("/styles/cachedBGImages/sn_bottom_right.gif");
	background-repeat: repeat-x;
	background-position: 0% 98%;
}
/*use to maitain width on td in page is reqised. */
.TabSpacerDivSpacer {
	width: 5px;
	height: 0px;
	font-size: 0px;
}
/* use to position the caler page tab. */
.CallerPageTabPosition {
}
/* TAB NORMAL STATES (plus calls to hover and selected states to ensure inheritance of styles) */
/* use to specify things like height of td. */
.TabTD {
	vertical-align: bottom;
}
/* use for generic tab styles like background-color and height. */
.Tab {
	height: 20px;
	position: relative;
	top: 0px;
	left: 0px;
	background-image: url("/styles/cachedBGImages/center_dulled.gif");
}
/* set top left graphic. */
.TabTopLeft, .TabTopLeftHover, .TabTopLeftSelected {
	background-image: url("/styles/cachedBGImages/sn_top_left.gif");
	background-repeat: no-repeat;
	width: 14px;
	height: 24px;
}
/* to maintain width on td if page is resized to small.*/
.TabTopLeftDivSpacer {
	width: 14px;
	height: 24px;
	font-size: 0px;
}
.TabTopMiddle, {
	background-image: url("/styles/cachedBGImages/center_dulled.gif");
}
/* set top middle graphic. */
.TabTopMiddleHover, .TabTopMiddleSelected {
	background-image: url("/styles/cachedBGImages/center_Highlighted.gif");
}
/* set top right graphic. */
.TabTopRight, TabTopRightHover, TabTopRightSelected {
	background-image: url("/styles/cachedBGImages/sn_top_right.gif");
	background-repeat: no-repeat;
	width: 12px;
	height: 24px;
}
/* to maintain width on td if page is resized to small.*/
.TabTopRightDivSpacer {
	width: 12px;
	height: 24px;
	font-size: 0px;
}
/* set bottom left graphic and bottom borders */
.TabBottomLeft, .TabBottomLeftHover, .TabBottomLeftSelected {
	background-image: url("/styles/cachedBGImages/sn_bottom_right.gif");
	background-repeat: repeat-x;
}
/* set bottom middle graphic and bottom borders */
.TabBottomMiddle, .TabBottomMiddleHover, .TabBottomMiddleSelected {
	background-image: url("/styles/cachedBGImages/sn_bottom_right.gif");
	background-repeat: repeat-x;
}
/* set bottom right graphic and bottom borders */
.TabBottomRight, .TabBottomRightHover, .TabBottomRightSelected {
	background-image: url("/styles/cachedBGImages/sn_bottom_right.gif");
	background-repeat: repeat-x;
}
/* set tab text. font styles etc. and positioning. */
.TabTextHover, .TabTextSelected {
	white-space: nowrap;
	display: block;
	padding-left: 0px;
	position: relative;
	top: 0;
	cursor: default;
	color: ##15428b;
	font-family: tahoma, arial, verdana, sans-serif;
	font-size: 11px;
	line-height: 15px;
	font-style: normal;
	font-weight: bold;
	text-decoration: none;
	text-valign: bottom;
	text-transform: Capitalize;
}
/* set tab text. font styles etc. and positioning. */
.TabText, .TextHidden {
	white-space: nowrap;
	display: block;
	padding-left: 0px;
	position: relative;
	top: 0;
	cursor: default;
	color: ##15428b;
	font-family: tahoma, arial, verdana, sans-serif;
	font-size: 11px;
	line-height: 15px;
	font-style: normal;
	text-decoration: none;
	text-valign: bottom;
	text-transform: Capitalize;
}
a {
	text-decoration: none;
	cursor: default;
}
/* TAB ROLLOVER STATES */
/* use to specify things like alignment of entire tab within the td or height of td. */
.TabTDHover {
}
/* use for generic tab styles like background-color and height. */
.TabHover {
	height: 15px;
	position: relative;
	top: 0px;
	left: 0px;
}
/* set top left graphic. */
.TabTopLeftHover {
	background-image: url("/styles/cachedBGImages/sn_top_left_hover.gif");
	background-repeat: no-repeat;
}
/* set top middle graphic. */
.TabTopMiddleHover {
/*	background-color: ##BC6313;*/
}
/* set top right graphic. */
.TabTopRightHover {
	background-image: url("/styles/cachedBGImages/sn_top_right_hover.gif");
}
/* set bottom left graphic and bottom borders */
.TabBottomLeftHover {
	background-image: url("/styles/cachedBGImages/sn_bottom_right_hover.gif");
	background-repeat: repeat-x;
}
/* set bottom middle graphic and bottom borders */
.TabBottomMiddleHover {
	background-image: url("/styles/cachedBGImages/sn_bottom_right_hover.gif");
	background-repeat: repeat-x;
}
/* set bottom right graphic and bottom borders */
.TabBottomRightHover {
	background-image: url("/styles/cachedBGImages/sn_bottom_right_hover.gif");
	background-repeat: repeat-x;
}
/* set tab text. font styles etc. and positioning. */
.TabTextHover {
}
/* TAB SELECTED STATE */
/* TAB ROLLOVER STATES */
/* use to specify things like alignment of entire tab within the td or height of td. */
.TabTDSelected {
}
/* use for generic tab styles like background-color and height. */
.TabSelected {
	height: 15px;
	position: relative;
	top: 0px;
	left: 0px;
}
/* set top left graphic. */
/* set top left graphic. */
.TabTopLeftSelected {
	background-image: url("/styles/cachedBGImages/sn_top_left_hover.gif");
	background-repeat: no-repeat;
}
/* set top middle graphic. */
.TabTopMiddleSelected {
/*	background-color: ##BC6313; */
}
/* set top right graphic. */
.TabTopRightSelected {
	background-image: url("/styles/cachedBGImages/sn_top_right_hover.gif");
}
/* set bottom left graphic and bottom borders */
.TabBottomLeftSelected {
	background-image: url("/styles/cachedBGImages/sn_bottom_right_hover.gif");
	background-repeat: repeat-x;
}
/* set bottom middle graphic and bottom borders */
.TabBottomMiddleSelected {
	background-image: url("/styles/cachedBGImages/sn_bottom_right_hover.gif");
	background-repeat: repeat-x;
}
/* set bottom right graphic and bottom borders */
.TabBottomRightSelected {
	background-image: url("/styles/cachedBGImages/sn_bottom_right_hover.gif");
	background-repeat: repeat-x;
}
/* set tab text. font styles etc. and positioning. */
.TabTextSelected {
	font-weight: bold;
}
/* PRIVATE CLASSES - DO NOT CHANGE*/
/* to force width of tabs according to text in firefox. */
.TextHidden {
	visibility: hidden;
	height: 0px;
}
/* to ensure that borders display */
.DivSpacer {
	width: 0px;
	height: 0px;
	font-size: 0px;
}
/* to maintain proper heights and to ensure borders display in firefox. */
td {
	empty-cells: show;
	font-size: 0px;
}
</style>
</cfoutput>
