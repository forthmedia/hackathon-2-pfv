<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Release Scripts for Spring 2012 Security Upgrade

WAB 2012-11-21 these functions also called from newInstallReleaseScripts.cfc 

--->

<cfcomponent output="false" index="870" active="false">  

	<cffunction name="createMasterKey" access="public" index="1" hint="Create Master Key">
		<cfset var result = {isOK= true, message=""}>
		<cfset var path = application.paths.userfiles & "\XML\datasource.xml">
		<cfset var xmlResult = application.com.xmlfunctions.readAndParseXMLFile(path)>	
		
		<cfset var find = xmlSearch(xmlResult.xml,"//masterkey")>
		<cfif not arrayLen(find)>
			<cfset masterKeyNode = XmlElemNew(xmlResult.xml,"masterkey")>
			<cfset masterKeyNode.xmltext = application.com.encryption.GenerateSecretKeyAESHex()>
			<cfset ArrayAppend(xmlResult.xml.xmlroot.XmlChildren, masterKeyNode)>		
			<cfset application.com.xmlfunctions.saveXMLDoc(path,xmlResult.xml)>
			<cfset result.message="Master Key Created">
		<cfelse>
			<cfset result.message="Master Key Already Exists">
		</cfif>
		
		<cfreturn result>
	
	</cffunction>
	
	<cffunction name="encryptPasswordsInSettings" index="2" hint="Encrypts any passwords stored in settings that are not already encrypted.">
		<cfset var result = {isOK= true, message=""}>
		<cfset var additionalArgs = {message=""}>
		
		<cfset application.com.settings.runFunctionAgainstNodes (XMLNodeArray = application.settings.masterXML.settings.xmlchildren, function = encryptPasswordField,additionalArgs = additionalArgs)>
		<cfif additionalArgs.message neq "">
			<cfset result.message = additionalArgs.message>
		</cfif>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="encryptPasswordField" access="private">
		<cfargument name = "XMLNode">
		<cfargument name="additionalArgs">
		
		<cfset var settingName = "">
		<cfset var settingValue = "">
		<cfset var searchFor = "">

		<cfif structKeyExists(xmlNode.xmlAttributes,"relayFormElementType") and xmlNode.xmlAttributes.relayFormElementType eq "password">
		
				<cfset settingName = application.com.settings.getFullVariableNameFromNode(xmlnode)>

				<cfif structKeyExists(xmlNode.xmlAttributes,"DynamicKey")>
					<cfset searchFor = reverse(listrest(reverse(settingName),".")) & "%">
				<cfelse>
					<cfset searchFor = settingName>
				</cfif>			

				<!--- WAB 2016-01-12 add in the id.  Needed when the setting is a variant (in particular cfadminpassword) --->
				<cfquery name="getDBSettingValue" datasource="#application.siteDataSource#">
					select id, name, value from settings where name like  <cf_queryparam value="#searchFor#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				</cfquery>

			
				<cfloop query="getDBSettingValue">
					<!--- test whether setting can be decrypted.  If it can't be decrypted(and so throws an error) then the setting must be unencrypted and will need encrypting--->
					<cftry>
						<cfset application.com.encryption.decryptWithMasterKey(value)>
						<cfset arguments.additionalArgs.message = arguments.additionalArgs.message & name & ": Already Encrypted<BR>">
	
						<cfcatch>
							<cfset application.com.settings.InsertUpdateDeleteSetting(settingid =id, variableName=name,variableValue=value)>
							<cfset arguments.additionalArgs.message = arguments.additionalArgs.message & name & ": Encrypted<BR>">
						</cfcatch>
					</cftry>
			</cfloop>
		</cfif>

		<cfreturn>
	</cffunction>
</cfcomponent>