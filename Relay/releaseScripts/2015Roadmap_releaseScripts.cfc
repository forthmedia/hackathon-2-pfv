<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="true"  extends="admin.release.releasescripts" index="840" hint="2015 Roadmap Release Scripts">


	<cffunction name="resetDataLengthForSalesforceDataObjectTables" hint="Set the length of the columns for the Salesforce data object tables based on the remote meta-data" >

		<cfset var result = {isOK=true,message="No Columns Need Updating"}>
		<cfset var columnsUpdated = {}>
		<cfset var getSalesforceConnectorTables = "">
		<cfset var getColumnsWhereLengthNeedsAltering = "">
		<cfset var alterTableColumnLength = "">

		<!--- NJH 2016/09/16 - JIRA PROD2016-2351 only run scripts if SF is active and only get SF objects --->
		<cfif application.com.relayMenu.isModuleActive(moduleID="connector") and application.com.settings.getSetting("connector.type") eq "salesforce">
			<cfset var connectorApiObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorApiObject(connectorType="salesforce")>
			<cfif connectorApiObject.testConnectionToRemoteSite()>

				<cfquery name="getSalesforceConnectorTables">
					select object from connectorOBject where relayware=0 and connectorType='Salesforce'
				</cfquery>

				<cfloop query="getSalesforceConnectorTables">
					<cfset var sfTableName = "connector_salesforce_#object#">
					<cfset var objectMetaData = application.getObject("connector.com.connector").getRemoteObjectFields(connectorType="salesforce",object=object)>

					<cfif listFindNoCase(objectMetaData.columnList,"length")>
						<cf_qoqToDb query=#objectMetaData# name="##objectMetaData">

						<!--- get columns where length on SF is greater than the length on the data object table... exclude columns where data table has max already set --->
						<cfquery name="getColumnsWhereLengthNeedsAltering">
							select column_name,case when o.length > 4000 then 'max' else cast(o.length as varchar) end as length
							from information_schema.columns c
								inner join ##objectMetaData o on o.name = c.column_name and o.length > c.character_maximum_length
							where table_name =  <cf_queryparam value="#sfTableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
								and o.length > 255
								and c.character_maximum_length != -1
						</cfquery>

						<cfif getColumnsWhereLengthNeedsAltering.recordCount>
							<cfset columnsUpdated[sfTableName] = "">

							<cfloop query="getColumnsWhereLengthNeedsAltering">
								<cfquery name="alterTableColumnLength">
									alter table #sfTableName# alter column #column_name# nvarchar(#length#)
								</cfquery>

								<cfset columnsUpdated[sfTableName] = listAppend(columnsUpdated[sfTableName],"#column_name# to nvarchar(#length#)")>
							</cfloop>
						</cfif>
					</cfif>
				</cfloop>

				<cfif structCount(columnsUpdated)>
					<cfset result.message = "">
					<cfloop collection="#columnsUpdated#" item="sftablename">
						<cfset result.message = result.message & "#sftablename# Columns Updated:#columnsUpdated[sftablename]#<br>">
					</cfloop>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

    <!--- 2015-08-05 DAN - change param names to avoid possible conflict with RELAY_FILE_LIST if used together --->
    <cffunction name="updateParamNamesOfRelayCatalogueAssetsTag" hint="Change RELAY_CATALOGUEASSETS param names FileTypeGroupID to productFileTypeGroupIDs and FileTypeID to productFileTypeIDs">
        <cfset var result = {isOK = true, message="",error=""}>

        <cfquery name="getMatchingPhrases" datasource="#application.sitedatasource#">
            select p.phraseText, p.phraseid, p.ident,
                case when p.entityID <> 0 then p.phraseTextID + '_' + entityname + '_' + convert(varchar,p.entityID) else p.phrasetextID end as phraseIdentifier
                from vphrases p
                    inner join schemaTable s on s.entityTypeID = p.entityTypeID
                    inner join phrases on phrases.phraseID = p.phraseID
                where (phrases.phraseText like '%<RELAY_CATALOGUEASSETS %FileTypeGroupID=%' OR phrases.phraseText like '%<RELAY_CATALOGUEASSETS %FileTypeID=%')
        </cfquery>

        <cfset showOppTypes = "">
        <cfloop query="getMatchingPhrases">
            <cfset tempPhraseText = getMatchingPhrases.phraseText>
            <cfif tempPhraseText.indexOf("<RELAY_FILE_LIST") GTE 0>
                <!--- it's unlikely to have also RELAY_FILE_LIST on the same page for existing configs but if so just replace it within the RELAY_CATALOGUEASSETS --->
                <cfset startIdx = tempPhraseText.indexOf("<RELAY_CATALOGUEASSETS")>
                <cfset endIdx = tempPhraseText.indexOf(">", startIdx)>
                <cfset tmpSubString = tempPhraseText.substring(startIdx, endIdx+1)>
                <cfset newSubString = reReplaceNoCase(tmpSubString, " FileTypeGroupID=", " productFileTypeGroupIDs=", "ALL")>
                <cfset newSubString = reReplaceNoCase(newSubString, " FileTypeID=", " productFileTypeIDs=", "ALL")>
                <cfset tempPhraseText = reReplaceNoCase(tempPhraseText, tmpSubString, newSubString, "ALL")>
            <cfelse>
                <cfset tempPhraseText = reReplaceNoCase(tempPhraseText, " FileTypeGroupID=", " productFileTypeGroupIDs=", "ALL")>
                <cfset tempPhraseText = reReplaceNoCase(tempPhraseText, " FileTypeID=", " productFileTypeIDs=", "ALL")>
            </cfif>

            <cfquery name="updatePhrase" datasource = "#application.sitedatasource#">
                update phrases set phrasetext =  <cf_queryparam value="#tempPhraseText#" CFSQLTYPE="CF_SQL_VARCHAR" >  where ident =  <cf_queryparam value="#ident#" CFSQLTYPE="CF_SQL_Integer" >
            </cfquery>
            <cfset result.message = result.message & "#phraseIdentifier# updated to #htmleditformat(tempPhraseText)# <BR>">

            <cfset application.com.relayTranslations.resetPhraseStructureKey(phraseIdentifier)> --->

        </cfloop>

        <cfreturn result>
    </cffunction>

</cfcomponent>