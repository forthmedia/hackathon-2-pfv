<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="true"  extends="admin.release.releasescripts" index="840" hint="2014 Roadmap Partner Cloud Release Scripts">

	<!--- We have removed the 'select an option' row from the query, so anywhere where this is called needs to have a nullText value set to 'Phr_ChooseVendorSalesManager' --->
	<cffunction name="CheckInstancesOfGetvendorSalesManagerPersonID" index="1" hint="Check for instances of function getVendorSalesManagerPersonID Exist for 2014 Roadmap Release 2">
		<cfset  var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>

		<cfset tempStruct = {find="getVendorSalesManagerPersonID"}>				<!--- don't pass in a Replace value so it does a Find only --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*|*.xml")>
	</cffunction>


	<cffunction name="googleAdminAreasLocations" hint="Geocoding Update - Set Administrative Areas on existing Locations." >
		
		<!--- (reverse) Geocode a limited number of items and then set a scheduled task to do another batch 
			By doing this we can
				a) deal with google API limits
				b) get some feedback in real time
		--->

		<cfset var  geoCodeResult = application.com.relayLocator.reloadGoogleAdminAreas(numberOfItems = 200)>
			
		<cfif geoCodeResult.itemsLeft GT 0>
			<cfif geoCodeResult.overQueryLimit>
				<cfset startDate = dateadd("d",1,now())>
				<cfset geoCodeResult.message &= "<BR>Google API Daily Limit exceeded. A further task has been scheduled for tomorrow.  Please check back later">				
			<cfelse>
				<cfset startDate = dateadd("n",1,now())>			
				<cfset geoCodeResult.message &= "<BR>A further task has been scheduled.  Please check back later">
			</cfif>

			<cfschedule action="update" URL="#request.currentSite.protocolAndDomain#/#cgi.script_name#?#request.query_string#" task="geocode" startdate="#startDate#" starttime="#startDate#" interval="once">

			<cfset geoCodeResult.isOK = false>
		<cfelse>
			<cfset geoCodeResult.message &= "<BR>Geocoding completed">
		</cfif>
		
		<cfreturn geoCodeResult>

	</cffunction>
	


</cfcomponent>