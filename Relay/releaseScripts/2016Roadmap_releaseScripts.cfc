<!--- �Relayware. All Rights Reserved 2016

WAB	2016-01-13 	CFAdmin Password now stored in db, so cfpassword argument now optional
--->
<cfcomponent output="true"  extends="admin.release.releasescripts" index="860" hint="2016 Roadmap Release Scripts">

	<cffunction name="checkForDeprecatedCode" index="5" hint="Checks for various deprecated variables (to be referred to a developer)">

		<cfset  var findAndReplaceArray = [
			  {find="cf_twoselectsComboList"}
			, {find="selAll"}
			, {find="cf_displayValidValueList"}
			, {find="fnlFormValidation.js", replace = "rwFormValidation.js"}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*|*.xml")>
	</cffunction>

	<!--- Because the code structure has changed, we need to ensure that any references to connector.cfcs in com directory need to be replaced. Don't do a replace at this point, but just find instances --->
	<cffunction name="CheckForReferencesToComConnectorInCodeFiles" index="10" hint="Check for instances of com.connector in code files. Any instances found will need to be dealt with manually!">
		<cfset  var findAndReplaceArray = [
			{find="com.connector"}
			,{find=" addColumnToTable",replace=' application.getObject("connector.com.connectorUtilities").addColumnToTable'}
		]>


		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<cffunction name="UpdateReferencesToOppProductInCodeFiles" index="15" hint="Update instances of oppProduct in code files to opportunityProduct">
		<cfset  var findAndReplaceArray = [
			{find="oppProducts",replace="opportunityProduct"}
			,{find="oppProductID",replace="opportunityProductID"}
			,{find="crmOppProductID",replace="crmOpportunityProductID"}
		]>


		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<cffunction name="moveCustomJSFiles" index="20" hint="Moves various custom js files from javascript folder to core equivalent folders in code files" >

		<cfset var result = {isOK=true,message="No files moved"}>
		<cfset var coreJSPaths = {lead_js = "\lead\js", oppEdit_js="\leadManager\js"}>
		<cfset var source = "">
		<cfset var dest = "">
		<cfset var filesMoved = "">
		<cfset var filesExistsInDest = "">

		<cfif DirectoryExists(application.paths.code&"\javascript")>
			<cfloop collection="#coreJSPaths#" item="jsFile">
				<cfset jsFilename = replace(lcase(jsFile),"_js",".js")>
				<cfset source = application.paths.code&"\javascript\#jsFilename#">
				<cfset dest = application.paths.code&coreJSPaths[jsFile]&"\#jsFilename#">

				<cfif fileExists(source)>
					<cfif not directoryExists(application.paths.code&coreJSPaths[jsFile])>
						<cfset directoryCreate(application.paths.code&coreJSPaths[jsFile])>
					</cfif>
					<cfif not fileExists(dest)>
						<cffile action="move" source="#source#" destination="#dest#">
						<cfset filesMoved = listAppend(filesMoved,jsFile)>
					<cfelse>
						<cfset filesExistsInDest = listAppend(filesExistsInDest,jsFile)>
					</cfif>
				</cfif>
			</cfloop>

			<cfif filesMoved neq "">
				<cfset result.message = "The following files have been moved from javascript directory:">
				<cfloop list="#filesMoved#" index="jsFile">
					<cfset jsFilename = replace(lcase(jsFile),"_js",".js")>
					<cfset result.message = result.message & "<br>#jsFilename# (#application.paths.code##coreJSPaths[jsFile]#\#jsFilename#)">
				</cfloop>
			</cfif>

			<cfif filesExistsInDest neq "">
				<cfset result.message = result.message &"<br>The following files were not moved from javascript directory as they exist in destination directory:">
				<cfloop list="#filesExistsInDest#" index="jsFile">
					<cfset jsFilename = replace(lcase(jsFile),"_js",".js")>
					<cfset result.message = result.message & "<br>#jsFilename# (#application.paths.code##coreJSPaths[jsFile]#\#jsFilename#)">
				</cfloop>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- We have removed the 'select an option' row from the query, so anywhere where this is called needs to have a nullText value set to 'Phr_ChooseVendorSalesManager' --->
	<cffunction name="UpdateReferencesTo3rdPartyJavascriptFilesFoundInCore" index="30" hint="Update paths of core 3rd party javascript includes in code files" functionRevision="2">
		<!---  look for files.js as well, relayFormJavaScipts.cfm - moved to templates  --->
		<cfset  var findAndReplaceArray = [
			{find="/javascript/(prototype.js|prototype.min.js|scrollbar.js|jquery.*?\.js)",replace="/javascript/lib/\1"}
			,{find="/javascript/files.js",replace="/fileManagement/js/files.js"}
			,{find="/javascript/elearning.js",replace="/elearning/js/elearning.js"}
			<!---  	2016-05-16 WAB/NJH Due to an error in the previous version of this release script, there may be some places incorrectly referring to /javascript/fileManagement/.... or /javascript/elearning/...
					These two lines deal with this edge case.  In reality I would expect this problem will probably have been corrected on any site that had the problem
			--->
			,{find="/javascript/fileManagement/js/files.js",replace="/fileManagement/js/files.js"}
			,{find="/javascript/elearning/js/elearning.js",replace="/elearning/js/elearning.js"}

			,{find="/javascript/relayFormJavaScripts.cfm",replace="/templates/relayFormJavaScripts.cfm"}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<!--- move the jquery css files from /javascript/css to javascript/lib/jquery/css --->
	<cffunction name="UpdateReferencesToCoreJQueryCSSFiles" index="35" hint="Update paths of core jquery css includes">
		<cfset  var findAndReplaceArray = [
			{find="/javascript/css/(jquery.*?\.css)",replace="/javascript/lib/jquery/css/\1"}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<cffunction name="removeReferencesToScriptSrc" index="50" hint="Removes the scriptSrc attribute for cfform">

		<cfset  var findAndReplaceArray = [
			 {find='scriptSrc="/javascript"',replace=""}
			,{find='scriptSrc="/javascript/cfform.js"',replace=""}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<cffunction name="checkForFile_addPeopleToUserGroup" index="50" hint="Check For Existence of \code\cftemplates\addPeopleToUserGroup.cfm">
		<cfset var filename = "\code\cftemplates\addPeopleToUserGroup.cfm">
		<cfset var result = {isOK = true,message=""}>

		<cfif fileExists (expandPath (filename))>
			<cfset result.isOK = false>
			<cfset result.Message = "The file #filename# must be manually examined and all logic moved to userGroup.conditionSQL.  See wiki page: automatically-adding-people-to-usergroups ">
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="UpdateClientCSS" index="100" hint="Make changes to various style sheets in \code\styles" RunOnEachUpgrade="true" Revision="1">

		<cfset  var findAndReplaceArray = [
			{
				  find='
					  (\r)( label \{
						color:.*?
						)(padding:0 !important;)(
					\})
				  '
				, replace="\1/*  #dateFormat(now())# Auto Replace.  Removed \3 */ \2 \4
				"
				, escapeRegExp = false
			}
			,{
				  find=
					'
					label.required {
						background: url("../borders/images/backRequiredGray.gif") no-repeat scroll 100% 0 transparent;
						padding-right: 17px !important;
						left:15px;
						position:relative;
						display: block;
					}
					'
				, replace="/* #dateFormat(now())# Auto Replace label.required style removed, now done in Global.CSS */"
				, escapeRegExp = true
			}
		]>


		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="Code", subdirectory = "\styles", filter="*.css", escapeRegExp = true, ignoreWhiteSpace = true)>
	</cffunction>


	<!--- NJH 2016/05/19 JIRA PROD2016-1148 -
		NJH 2016/08/11 JIRA PROD2016-2180 second revision - deal with incorrect name  -some people seem to have borderGeneral.js rather than bordersGeneral.js --->
	<cffunction name="moveBorderGeneralJSFiles" index="90" hint="Moves general.js custom file from border/script folder to javascript folder and renamed to borderGeneral.js" revision="2">

		<cfset var result = {isOK=true,message="No files moved"}>

		<cfset var sourceBorderJSFile = application.paths.code&"\borders\script\general.js">
		<cfset var customJSDir = application.paths.code&"\javascript">
		<cfset var destBorderJSFile = customJSDir&"\bordersGeneral.js">

		<cfif not directoryExists(customJSDir)>
			<cfdirectory action="create" directory="#customJSDir#">
		</cfif>

		<cfif fileExists(sourceBorderJSFile)>
			<cffile action="move" source="#sourceBorderJSFile#" destination="#destBorderJSFile#">
			<cfset result.message = "#sourceBorderJSFile# moved to #destBorderJSFile#. SOURCE CONTROL MUST BE UPDATED.">
		<cfelse>
			<cfset result.message = "#sourceBorderJSFile# does not exist. No file moved.">
		</cfif>

		<cfset var wronglyNameFile = application.paths.code&"\javascript\borderGeneral.js">
		<cfif fileExists(wronglyNameFile)>
			<cffile action="rename" destination="#destBorderJSFile#" source="#wronglyNameFile#">
			<cfset result.message = result.message & "#wronglyNameFile# renamed to to #destBorderJSFile#. SOURCE CONTROL MUST BE UPDATED.">
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2016/05/20 JIRA PROD2016-350 --->
	<cffunction name="renameUseOrganisationAsParentAccountSetting" index="100" hint="Rename 'Use Organization as Parent Account Model' setting">

		<cfset  var findAndReplaceArray = [
			 {find="plo.useOrganizationAsParentAccountModel",replace="plo.useLocationAsPrimaryPartnerAccount"}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<!--- NJH 2016/06/13 JIRA PROD2016-347 - update relay tags for existing customers - add the 'addNewLocation' parameter for backwards compatibility --->
	<cffunction index="130" name="SetAddNewLocationParamToTrueForExistingAddContactForm" hint="Finds occurances of Add Contact Form relay tag and add 'addNewLocation=true' to support backwards compatibility">
		<cfset var result = {isOK = true, message="",error=""}>
		<cfset var tempStruct = structNew()>
		<cfset var replaceAndSearchArray = ArrayNew(1)>

		<cfset tempStruct = {sqlcontains="<RELAY_ADDCONTACTFORM",find="(<(?:RELAY_ADDCONTACTFORM)[^>]*? )(.*?>)",replace="\1AddNewLocation=yes \2"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.element,"p.phraseText not like '%AddNewLocation%'")>
	</cffunction>

	<!--- JIRA PROD2016-595 - remove the approval flag parameters from the resend password relay tag --->
	<cffunction name="removeApprovalFlagFromResendPasswordRelayTag" returntype="struct" index="3">

		<cfset var replaceAndSearchArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>

		<cfset tempStruct = {sqllike="%<RELAY_RESENDPASSWORD%>",find="(<RELAY_RESENDPASSWORD[^>]*?)(perApprovalFlag=[0-9a-zA-Z]+)",replace="\1"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<cfset var result = updatePhrases(replaceAndSearchArray,application.entityTypeID.element)>

		<cfset tempStruct = {sqllike="%<RELAY_RESENDPASSWORD%>",find="(<RELAY_RESENDPASSWORD[^>]*?)(orgApprovalFlag=[0-9a-zA-Z]+)",replace="\1"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>


		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.element)>
	</cffunction>


	<!--- WAB 2016-06-17  PROD2016-1276   Implement requiresXSRFToken attribute to replace allowBlindPOST --->
	<cffunction name="security_replaceAllowBlindPostAttribute" index="110" hint="Security - check for allowBlindPost attribute and replace with requiresXSRFToken">

		<cfset  var findAndReplaceArray = [
			 {find='allowBlindPost\s*=\s*"true"',replace='requiresXSRFToken="false"'}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cfc|*.xml")>
	</cffunction>


	<!--- NJH 2016/07/07 PROD2016-1346 - fix lead merge fields in emails --->
	<cffunction name="renameLeadMergeFields" index="110" hint="Fix the lead merge fields in emails">

		<cfset var replaceAndSearchArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>

		<cfset tempStruct = {sqllike="%lead.endCustomer.fullname%",find="\[\[lead.endCustomer.fullname\]\]",replace="[[lead.Fullname]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="%lead.endCustomer.organizationName%",find="\[\[lead.endCustomer.organizationName\]\]",replace="[[lead.Company]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="%lead.endCustomer.address1%",find="\[\[lead.endCustomer.address1\]\]",replace="[[lead.Address1]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="%lead.status%",find="\[\[lead.status\]\]",replace="[[lead.ApprovalStatus]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="%lead.stage%",find="\[\[lead.stage\]\]",replace="[[lead.Progress]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.emailDef)>
	</cffunction>


	<!--- NJH 2016/07/28 JIRA PROD2016-599/351 - create a release script that sets the value of the PrimaryContactFlag to KeyContactsPrimary where it's not already set so that existing behaviour is kept as the default
		has now been changed to the new location level flag --->
	 <cffunction name="setPrimaryContactsParamForRelayWebToLeadAndSocialOrgProfileTags" hint="Set PrimaryContactFlag parameter for RELAY_WEBTOLEAD and RELAY_SOCIALORGANIZATIONPROFILE tags where currently it is using the default.">
        <cfset var result = {isOK = true, message="",error=""}>
		<cfset var replaceAndSearchArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>

		<cfset tempStruct = {sqllike="%<RELAY_WEBTOLEAD%>",find="(<RELAY_WEBTOLEAD((?!PrimaryContactFlag=).)*?)>",replace="\1 PrimaryContactFlag=KeyContactsPrimary>"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<cfset var result = updatePhrases(replaceAndSearchArray,application.entityTypeID.element)>

		<cfset tempStruct = {sqllike="%<RELAY_SOCIALORGANIZATIONPROFILE%>",find="(<RELAY_SOCIALORGANIZATIONPROFILE((?!PrimaryContactFlag=).)*?)>",replace="\1 PrimaryContactFlag=KeyContactsPrimary>"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.element)>
	</cffunction>


	<!--- NJH 2016/11/21 JIRA PROD2016-1190 --->
	<cffunction name="lookForInstancesOfOldMatchingCode" hint="Look for instances of old matching code being used. Any instances found need to be dealt with manually.">
		<cfset  var findAndReplaceArray = [
			 {find="application\.com\.relayDataLoad\."}
			,{find="application\.com\.dbTools\.(.*?)matchname(.*?)\("}
			,{find="assignedEntity"}
			,{find="charReplacement"}
			,{find="matchnameReplacementOrg"}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>

	<!--- NJH 2016/22/28 JIRA PROD2016-2642 - getLeads now returns all leads that you have access to if you are the primary contact. It returns it at org or loc level --->
	<cffunction name="findAndRemoveCallToGetOrganisationLeadsFunction" hint="Replace the call to deprecated GetOrganisationLeads function with getLeads">
		<cfset  var findAndReplaceArray = [
			 {find="application.com.relayLeads.GetOrganisationLeads",replace="application.com.relayLeads.GetLeads"}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>


	<cffunction name="rePopulateMatchAddress" hint="Repopulate the matchAddress field, as address4 had not been included">

		<cfset var result = {isOK=true,message="matchAddress field updated"}>

		<cftry>
			<cfquery name="local.resetMatchAddress">
				-- update the location matchAddress field
				declare @x int = 1, @total int=0
				declare @message varchar(100)
				set rowCount 200000
				while (@x!=0)
				begin
					update location set matchAddress = 'resetMatchAddress'
					where matchAddress != 'resetMatchAddress'

					set @x = @@rowcount
					set @total = @total+@x
					set @message = cast(@total as varchar)+' rows updated in total ' + convert(varchar, getdate(),9)
					RAISERROR (@message, 10, 1) WITH NOWAIT
				end

				set rowCount 0
			</cfquery>

			<cfcatch type="database">
				<cfquery name="local.resetRowCount">
					set rowCount 0
				</cfquery>

				<cfrethrow>
			</cfcatch>
		</cftry>

		<cfset application.com.matching.updateMatchFields(tablename="location",entityType="location",whereClause="matchAddress='resetMatchAddress'",setNullMatchFieldsOnly=false)>
		<cfreturn result>
	</cffunction>


	<!--- NJH 2016/12/14 JIRA PROD2016-2957 - function has two new required fields, flagTypeId and entitytable --->
	<cffunction name="findCallToIsFlagTextIDUnique" hint="Check for function call 'isFlagTextIDUnique' as two new required arguments have been added">
		<cfset  var findAndReplaceArray = [
			 {find="isFlagTextIDUnique",replace=""}
		]>

		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.cf*")>
	</cffunction>
</cfcomponent>