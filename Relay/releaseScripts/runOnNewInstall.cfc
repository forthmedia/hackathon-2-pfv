<!--- �Relayware. All Rights Reserved 2014

WAB 2014-09-04 added updatePortalSearchCollections to this file
--->

<cfcomponent output="false" hint="Scripts To Be Run on a Brand New Install" index="400" runOnNewDatabase="true">


	<cffunction name="createMasterKey" access="public" index="1" hint="Create Master Key">
		<cfset var obj = createObject("component","8_3_1_SecurityUpgrade")>

		<cfreturn obj.createMasterKey()>

	</cffunction>

	<cffunction name="encryptPasswordsInSettings" index="2" hint="Encrypts any passwords stored in settings that are not already encrypted.">
		<cfset var obj = createObject("component","8_3_1_SecurityUpgrade")>

		<cfreturn obj.encryptPasswordsInSettings()>
	</cffunction>

	<cffunction name="setUpJasperServerEnvironment" index="10" hint="Sets up Jasperserver.  Creates the tenant, sets the homefolder name and hides the temp, adhoc and organisation folders.<br>This only needs running for a new install.">
		<cfargument name="jasperServerTenantID" type="string" required="false">

		<cfset var tenantID = "">
		<cfset var clientName = application.com.settings.getSetting("theClient.clientName")>
		<cfset var result = {isOK= true, message=""}>
		<cfset var initialiseResult = structNew()>

		<cfif structKeyExists(arguments,"jasperServerTenantID")>
			<cfset tenantID = arguments.jasperServerTenantID>
		<cfelse>
			<cfset tenantID = application.com.jasperServer.getTenantID()>
		</cfif>

			<cfset initialiseResult = application.com.jasperServer.initialiseJasperServerUser()>
			<cfif initialiseResult.isOK>

				<cfquery name="doesTenantAndTenantFolderExist" datasource="jasperServer">
					select sum(x) as checksum from (
						select 1 as x from jiResourceFolder where name =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						union
						select 2 as x from jiTenant where tenantID =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" >
					) as x
				</cfquery>

				<cfif doesTenantAndTenantFolderExist.checksum eq 3>
					<cfquery name="hideTenantFolders" datasource="jasperServer">
						update jiResourceFolder set hidden=1 where uri  like  <cf_queryparam value="%#tenantID#/%" CFSQLTYPE="CF_SQL_VARCHAR" >
							and name in ('adhoc','organizations','temp')
							and parent_folder = (select id from jiResourceFolder where name =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" > )

						declare @userRoleID int

						select @userRoleID=id from jiRole where rolename='ROLE_USER'

						if not exists (select 1 from jiObjectPermission where uri='repo:/public' and recipientObjectClass='com.jaspersoft.jasperserver.api.metadata.user.domain.impl.hibernate.RepoRole' and recipientObjectId=1)
						insert into jiObjectPermission (uri,recipientObjectClass,recipientObjectId,permissionMask)
						values ('repo:/public','com.jaspersoft.jasperserver.api.metadata.user.domain.impl.hibernate.RepoRole',@userRoleID,0)


						<cfif clientName neq "">
						update jiResourceFolder set label =  <cf_queryparam value="#clientName#" CFSQLTYPE="CF_SQL_VARCHAR" >  where name =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" >

						update jiTenant set tenantName =  <cf_queryparam value="#clientName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						where tenantName = tenantID
							and tenantID =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
					</cfquery>

					<cfset result.message = "Successfully set up the jasperServer environment for tenant '#tenantID#'.<br>Please now run the import scripts.">
				<cfelse>
					<cfset result.isOK = false>
					<cfset result.message = "There seems to be a problem initialising tenant '#tenantID#'.">
				</cfif>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = initialiseResult.message>
			</cfif>


		<cfreturn result>
	</cffunction>

	<cffunction index="20" name="updatePortalSearchCollections" hint="Create, register and populate SolR collections used by portal search (it might be slow!)." runOnEachInstance="true">
		<cfset var result = {isOK = true, message="", error=""}>
		<cftry>
			<cfhttp url="#lCase(listFirst(cgi.SERVER_PROTOCOL,'/'))#://#cgi.SERVER_NAME#/scheduled/portalSearchRefresh.cfm" throwonerror="yes" timeout="1800"></cfhttp>
			<cfif listFind(cfhttp.StatusCode, '200', ' ')>
				<cfset result.message = "SolR collection has been created, populated and registered with the ColdFusion server.">
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = "Error setting up SolR collections. Status Code: " & cfhttp.StatusCode>
			</cfif>
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.error = cfcatch.message & "<br>" & cfcatch.detail>
			</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>

</cfcomponent>