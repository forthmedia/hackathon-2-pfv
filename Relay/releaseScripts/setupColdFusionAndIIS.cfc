<!--- �Relayware. All Rights Reserved 2014

WAB 2015-03-10	modifications to reflect change in way missing Template Handling is done
				Added script to add API URL rewriting
				Removed asynchronous gateway setup - not used now
WAB	2016-01-13 	CFAdmin Password now stored in db, so cfpassword argument now optional
DAN 2016-01-15  Add script to set rewrite rule for redirect to HTTPs
NJH 2016/01/15	Add release script to set CF admin settings.
WAB 2016/01/27	Add script for Mime Types.
WAB 2016-06-29	PROD2016-1342  Added runAutomatically=false to addHttpsRedirectRule
WAB 2017-01-23 	RT-150 MissingTemplateHandler re-write altered so add a functionRevision attribute to setMissingTemplateHandler()
--->

<cfcomponent hint="Set up ColdFusion and IIS <BR>(Need to be run on every new ColdFusion Instance or if name of RelayCore directory changed)" index="105" runOnEachInstance="true">

	<cffunction index="10" name="checkInstanceMapping" hint = "Checks whether the instance mapping is set correctly">
		<cfset result ={isOK= true, message=""}>

		<!--- Check Existing --->
		<cfset result  = application.com.clusterManagement.hasInstanceMappingBeenSetUpCorrectly(application = application)>
		<cfreturn result>

	</cffunction>



	<cffunction index="20" name="setUpInstanceMapping" hint = "Sets up the Instance mapping if does not exist.  <font color='red'>NOTE: May cause a server restart</font>">

		<cfset result ={isOK= true, message=""}>

		<!--- Check Existing --->
		<cfset checkExisting = application.com.clusterManagement.hasInstanceMappingBeenSetUpCorrectly(application = application)>

		<cfif not checkExisting.isOK>
			<cfset result = application.com.clusterManagement.addInstanceMapping (testMode = false,application = application)>
		<cfelse>
			<cfset result.message = "Mapping Exists and is Correct">
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction index="40" name="setMissingTemplateHandler" hint = "Sets up Missing Template Handling in IIS. <BR>Note, needs running on every machine." functionRevision="2">

		<cfset result ={isOK= true, message=""}>

		<!--- Check Existing --->
		<cfset result = application.com.iisAdmin.setUpMissingTemplateHandler()>


		<cfreturn result>

	</cffunction>


	<cffunction index="30" name="addAPIURLRewriting" hint = "Configures URL rewriting for the API">
		<cfargument name="update" default="true">


		<cfset var result ={isOK= true, message=""}>
		<cfset var rule = "">

		<!--- Check Existing --->
		<cfsavecontent variable="rule">
		<cfoutput>
		<rule name="API" stopProcessing="true">
			<match url="^api/(.*)$" />
			<conditions/>
			<serverVariables/> <!--- Not actually used, but if not here my update routines always think that rule needs updating --->
			<action type="Rewrite" url="/taffy/index.cfm/{R:1}" />
		</rule>
		</cfoutput>
		</cfsavecontent>

		<cfset result = application.com.iisAdmin.checkAndSetupURLRewriting(rewriteNode = xmlParse(rule), position = 0, update = update)>

		<cfreturn result>

	</cffunction>

    <cffunction index="35" name="addHttpsRedirectRule" hint = "Sets rewrite rule for redirect to HTTPs. <BR>(Note: ONLY run this if all sites using this code are HTTPS)" runAutomatically = "false">
        <cfargument name="update" default="true">

        <cfset var result ={isOK= true, message=""}>
        <cfset var rule = "">

		<cfif request.CurrentSite.isSecure>
	        <!--- Check Existing --->
	        <cfsavecontent variable="rule">
	        <cfoutput>
	            <rule name="Redirect to HTTPS" enabled="true" stopProcessing="true">
	                <match url="(.*)" />
	                <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
	                    <add input="{HTTPS}" pattern="^OFF$" />
	                </conditions>
	                <action type="Redirect" url="https://{HTTP_HOST}/{R:1}" redirectType="SeeOther" />
	            </rule>
	        </cfoutput>
	        </cfsavecontent>
	
	        <cfset result = application.com.iisAdmin.checkAndSetupURLRewriting(rewriteNode = xmlParse(rule), position = 0, update = update)>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "Cannot add rewrite rule.  This site is not Https">
		</cfif>

        <cfreturn result>

    </cffunction>

	<cffunction index="70" name="setUpSiteWideErrorHandler" hint = "Sets Up Cold Fusion Site-Wide Error Handler" >
		<cfargument name="cfpassword" required="false">

		<cfset var Result ={}>
		<cfset var checkResult ="">

		<cfif application.com.cfadmin.checkCFPassword(argumentCollection = arguments)>
			<!--- Check Existing --->
			<cfset checkResult  = application.com.errorHandler.checkSitewideErrorHandlerMappingAndSetting(argumentCollection = arguments)>

			<cfif checkResult.isOK>
				<cfset result = checkresult>
			<cfelse>
				<cfset setResult  = application.com.errorHandler.setSitewideErrorHandlerMappingAndSetting(argumentCollection = arguments)>
				<cfset result.message = "Setting Problem:<BR> " & checkResult.message & "<BR>Setting Updated: <BR>" & setResult.message>
			</cfif>
		<cfelse>
			<cfset result = {isOK = false, message = "Incorrect Password"}>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction index="80" name="setColdFusionSettings" hint = "Set Coldfusion Admin Settings. ">
		<cfargument name="cfpassword" required="false">

		<cfset var Result = {isOk=true,message="", updated = {}, unchanged = {}, failed = {}}>
		<cfset var setSourceResult = {}>
		<cfset var cfAdminSettings = {CFFormScriptSrc="/javascript/cf"}>
		<cfset var cfAdminSetting = "">

		<cfset var result = application.com.cfadmin.setProperties(properties = cfAdminSettings, argumentCollection = arguments)>

		<cfif result.isOK>
			<cfset statuses = {
						updated = "The following settings have updated successfully:",
						unchanged = "The following settings are unchanged:"
						}>
			<cfloop collection = "#statuses#" item= "status">
				<cfif structCount (result[status])>
					<cfset result.message &= statuses[status] & "<BR>">
					<cfloop collection="#result[status]#" item="cfAdminSetting">
						<cfset result.message &= "#cfAdminSetting# : #result[status][cfAdminSetting]#<br>">
					</cfloop>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction index="90" name="setIISMimeTypes" hint = "Set some Mime Types which may not be on IIS by default" >

		<cfset var Result = {isOk=true,message=""}>

		<cfset var mimeTypes = {
								  woff2 = "application/font-woff2"
								, mp4  = "video/mp4"
								, svg = "image/svg+xml"
								}>


		<cfloop collection = #mimeTypes# item="extension">
			<cfset var testForType  =   application.com.iisadmin.getMimeType(extension)>
			<cfif arrayLen (testForType) is 0>
				<cfset application.com.iisAdmin.setMimeType (extension,mimeTypes[extension])>
				<cfset result.message &= "#extension# Added<br />">
			<cfelse>
				<cfset result.message &= "#extension# Exists<br />">
			</cfif>

		</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- WAB 2016-05-18 BR-713/CASE 448770 I had been setting the mimetype for mp4 to video/mpeg - this was wrong --->
	<cffunction index="91" name="UpdateIISMimeTypes" hint = "Update Incorrect Mime Type (MP4)" functionRevision="1">

		<cfset var Result = {isOk=true,message=""}>

		<cfset var mimeTypes = {
								 mp4  = "video/mp4"
								}>


		<cfloop collection = #mimeTypes# item="extension">
			<cfset var testForType  =   application.com.iisadmin.getMimeType(extension)>
			<cfif arrayLen (testForType) is 0>
				<cfset application.com.iisAdmin.setMimeType (extension,mimeTypes[extension])>
				<cfset result.message &= "#extension# Mime Type Added<br />">
			<cfelseif testForType[1].xmlAttributes.mimeType is not mimeTypes[extension]>
				<cfset application.com.iisAdmin.setMimeType (extension,mimeTypes[extension])>
				<cfset result.message &= "#extension# Mime Type Updated from #testForType[1].xmlAttributes.mimeType# to #mimeTypes[extension]#<br />">
			<cfelse>
				<cfset result.message &= "#extension# Mime Type Correct<br />">
			</cfif>

		</cfloop>

		<cfreturn result>
	</cffunction>


</cfcomponent>