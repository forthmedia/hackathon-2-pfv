<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false" hint="Set Up Jasperserver" index="402" active="false"> 

	<cffunction name="setUpJasperServerEnvironment" index="1" hint="Creates the tenant, sets the homefolder name and hides the temp, adhoc and organisation folders.<br>This only needs running for a new install.">
		<cfargument name="jasperServerTenantID" type="string" required="false">
		
		<cfset var tenantID = "">
		<cfset var clientName = application.com.settings.getSetting("theClient.clientName")>
		<cfset var result = {isOK= true, message=""}>
		<cfset var initialiseResult = structNew()>
		
		<cfif structKeyExists(arguments,"jasperServerTenantID")>
			<cfset tenantID = arguments.jasperServerTenantID>
		<cfelse>
			<cfset tenantID = application.com.jasperServer.getTenantID()>
		</cfif>

		<!--- the JasperServer settting has been set --->
		<!--- <cfif not application.com.settings.isSettingDefault("reportmanager.serverPath")> --->
		
			<cfset initialiseResult = application.com.jasperServer.initialiseJasperServerUser()>
			<cfif initialiseResult.isOK>
			
				<cfquery name="doesTenantAndTenantFolderExist" datasource="jasperServer">
					select sum(x) as checksum from (
						select 1 as x from jiResourceFolder where name =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						union
						select 2 as x from jiTenant where tenantID =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					) as x
				</cfquery>
				
				<cfif doesTenantAndTenantFolderExist.checksum eq 3>
					<cfquery name="hideTenantFolders" datasource="jasperServer">
						update jiResourceFolder set hidden=1 where uri  like  <cf_queryparam value="%#tenantID#/%" CFSQLTYPE="CF_SQL_VARCHAR" > 
							and name in ('adhoc','organizations','temp')
							and parent_folder = (select id from jiResourceFolder where name =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
							
						declare @userRoleID int

						select @userRoleID=id from jiRole where rolename='ROLE_USER'
						
						if not exists (select 1 from jiObjectPermission where uri='repo:/public' and recipientObjectClass='com.jaspersoft.jasperserver.api.metadata.user.domain.impl.hibernate.RepoRole' and recipientObjectId=1)
						insert into jiObjectPermission (uri,recipientObjectClass,recipientObjectId,permissionMask)
						values ('repo:/public','com.jaspersoft.jasperserver.api.metadata.user.domain.impl.hibernate.RepoRole',@userRoleID,0)

						
						<cfif clientName neq "">	
						update jiResourceFolder set label =  <cf_queryparam value="#clientName#" CFSQLTYPE="CF_SQL_VARCHAR" >  where name =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						
						update jiTenant set tenantName =  <cf_queryparam value="#clientName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						where tenantName = tenantID
							and tenantID =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfif>
					</cfquery>
					
					<cfset result.message = "Successfully set up the jasperServer environment for tenant '#tenantID#'.<br>Please now run the import scripts.">
				<cfelse>
					<cfset result.isOK = false>
					<cfset result.message = "There seems to be a problem initialising tenant '#tenantID#'.">
				</cfif>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = initialiseResult.message>
			</cfif>
		<!--- <cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "The JasperServer serverPath setting found in 'reportmanager.serverPath' has not been set.<br>Please set this now and re-run the release script.">
		</cfif> --->
		
		<cfreturn result>
	</cffunction>

</cfcomponent>