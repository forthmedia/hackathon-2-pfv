<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2012-11-21  Created this file to hold functions which may need to be run more than once during future upgrades
2016-01-14	WAB	JIRA PROD2015-513 when loading phrases specify update mode of overwriteUnEdited
 --->

<cfcomponent output="false" hint="Scripts which may need running on each upgrade" extends="admin.release.releasescripts" index="100" runOnEachUpgrade="true">

	<cffunction name="loadPhraseFiles" hint="Load Missing Phrases from All Phrase Files">

		<cfset var result = {isOK = true, message = ""}>
		<cfset var filelocations = {relay = "#application.paths.relay#\Translation\LoadPhrases", user = "#application.paths.code#\LoadPhrases"}>
		<cfset var item="">
		<cfset var phraseFiles = "">

		<cfsetting requesttimeout="600">

		<cfloop collection="#filelocations#" item="item">

			<cfdirectory action="list" name="PhraseFiles" directory="#filelocations[item]#" filter="*_phrases.txt">

			<cfloop query="phraseFiles">
				<cfset loadResult = application.com.relayTranslations.addPhrasesFromLoadFile(phraseFileName = item & '|' & name, updatemode = "overwriteUnEdited")>
				<cfif listLen(loadResult.added) or listLen(loadResult.notUpdatedBecauseEdited)>
					<cfset result.message &=  "#name#:<BR>">
					<cfif listLen(loadResult.added)>
						<cfset result.message &=  "#listlen(loadResult.added)# Phrases added<br />">
					</cfif>
					<cfif listLen(loadResult.notUpdatedBecauseEdited)>
						<cfset result.message &=  "#listlen(loadResult.notUpdatedBecauseEdited)# Phrases Not Updated because they have been edited.">
						<cfset result.message &=  replace(loadResult.notUpdatedBecauseEdited,",","<BR>","ALL") >
					</cfif>
				</cfif>
			</cfloop>

		</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction index="1" name="AddFlagGroupsToDomains" hint = "Re-applies profile customisations to Jasperserver Domains (required when JasperServer Upgraded)">

		<cfset var result ={isOK= true, message=""}>

		<cfsetting requestTimeOut = "600">

		<cfset result  = application.com.jasperServer.applyFlagGroupsToDomains()>
		<cfreturn result>

	</cffunction>

</cfcomponent>