<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="true"  extends="admin.release.releasescripts" index="840" hint="Summer 2013 Roadmap Scripts" active="false">
	
	


	<cffunction name="moveProductImagesToNewLocation" index="1" hint="Moves existing product images to a new directory structure">
		<cfset var result = {isOK = true, message=""}>
		<cfset var contentDir = "#application.paths.content#">				<!--- 2013-10-16 PPB Case 437435 removed "\" for compatibility with other functions --->
		<cfset var subDirs = "ProductPhotoHires,ProductPDF,ProductThumbs">
		<cfset var sku = "">
		<cfset var promoID = "">
		<cfset var found = "">
		<cfset var notFound = "">
		<cfset var productID = 0>
		<cfset var getProductID = "">
		
		<cfsetting requestTimeOut = "600">
		
		<cftry>
			
			<cfloop list="#subDirs#" index="subDir">
				<cfdirectory action="list" directory="#contentDir#\#subDir#\" name="productFiles" recurse="true">		<!--- 2013-10-16 PPB Case 437435 added "\" for compatibility with other functions --->

				<cfloop query="productFiles">
					<cfif type eq "file" and name neq "thumbs.db">
						<cfset promoID = listFirst(name,"-")>
						<cfset sku = listFirst(replace(name,"#promoID#-",""),".")>
						
						<cfquery name="getProductID" datasource="#application.siteDataSource#">
							select productID from product where sku =  <cf_queryparam value="#sku#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfquery>
						
						<cfif getProductID.recordCount>
							<cfset productID = getProductID.productID>
							
							<cfif subDir eq "ProductPDF">
								<cfset filePrefix = "PDF_">
							<cfelseif subDir eq "ProductPhotoHires">
								<cfset filePrefix = "highRes_">
							<cfelseif subDir eq "ProductThumbs">
								<cfset filePrefix = "Thumb_">
							</cfif>
							<cfset newProductDir = "#contentDir#\linkImages\product\#productID#">
							<cfif not directoryExists(newProductDir)>
								<cfdirectory action="create" directory="#newProductDir#">
							</cfif>
							<cffile action="move" source="#directory#\#name#" destination="#newProductDir#\#filePrefix##productID#.jpg">
							<cfset found = found&"<br>#directory#\#name# to #newProductDir#\#filePrefix##productID#.jpg">
						<cfelse>
							<cfset notFound = notFound&"<br><b>sku</b> (#directory#\#name#)">
						</cfif>
					</cfif>
				</cfloop>
			</cfloop>
			
			<cfset result.message = "The following product images were moved:#found#">
			<cfset result.message = result.message & "<br>The following product skus were not found and so were not moved:#notFound#">
				
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message&" "&cfcatch.detail>
			</cfcatch>
		</cftry>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="moveElearningImagesToNewLocation" index="1" hint="Moves existing elearning images to a new directory structure">
		<cfset var result = {isOK = true, message=""}>
		<cfset var contentDir = "#application.paths.content#">		<!--- 2013-10-16 PPB Case 437435 removed "\linkImages" to avoid dup below  --->
		<cfset var subDirs = "trngModule,trngCourse">
		<cfset var elearningFiles = "">
		<cfset var entityID = "">
		<cfset var newDir = "">
		<cfset var moved = "">
		
		<cfsetting requestTimeOut = "600">
		
		<cftry>
			
			<cfloop list="#subDirs#" index="subDir">
				<cfdirectory action="list" directory="#contentDir#\linkImages\#subDir#\" name="elearningFiles" recurse="true">		<!--- 2013-10-16 PPB Case 437435 added "\linkImages\"  --->

				<cfloop query="elearningFiles">
					<cfif type eq "file" and name neq "thumbs.db">
						<cfset entityID = listFirst(listLast(name,"_"),".")>
						<cfset newDir = "#contentDir#\linkImages\#subDir#\#entityID#">
						<cfif not directoryExists(newDir)>
							<cfdirectory action="create" directory="#newDir#">
						</cfif>
						<cfif directory neq newDir>																					<!--- 2013-10-16 PPB Case 437435 may as well not attempt to move it if it hasn't changed  --->
						    <cffile action="move" source="#directory#\#name#" destination="#newDir#\#name#">
						    <cfset moved = moved&"<br>#directory#\#name# to #newDir#\#name#">
					    </cfif>																										<!--- 2013-10-16 PPB Case 437435 --->							
					</cfif>
				</cfloop>
			</cfloop>
			
			<cfset result.message = "The following elearning images were moved:#moved#">
				
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message&" "&cfcatch.detail>
			</cfcatch>
		</cftry>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="moveFileImagesToNewLocation" index="1" hint="Moves existing file images to a new directory structure">
		<cfset var result = {isOK = true, message=""}>
		<cfset var contentDir = "#application.paths.content#">
		<cfset var filesfiles = "">
		<cfset var fileID = "">
		<cfset var newDir = "">
		<cfset var moved = "">
		<cfset var filename = "">
		
		<cfsetting requestTimeOut = "600">
		
		<cftry>

			<cfdirectory action="list" directory="#contentDir#\fileLinkImages\" name="filesfiles" recurse="true">

			<cfloop query="filesfiles">
				<cfif type eq "file" and name neq "thumbs.db">
					<cfset fileID = listFirst(listLast(name,"_"),".")>
					<cfset newDir = "#contentDir#\linkImages\files\#fileID#">
					<cfif not directoryExists(newDir)>
						<cfdirectory action="create" directory="#newDir#">
					</cfif>
					<cfset filename = replace(name,"linkImage","thumb")>
					<cffile action="move" source="#directory#\#name#" destination="#newDir#\#filename#">
					<cfset moved = moved&"<br>#directory#\#name# to #newDir#\#filename#">
				</cfif>
			</cfloop>

			<cfset result.message = "The following file images were moved:#moved#">
				
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message&" "&cfcatch.detail>
			</cfcatch>
		</cftry>
		
		<cfreturn result>
	</cffunction>
	
	<!--- Update some potential opp merge fields, as opportunityEmailMergeBuilder has been merged with the oppmerge function in opportunity.cfc --->
	<cffunction name="UpdateOpportunityMergeFields" returntype="struct" index="3">
	
		<cfset var replaceAndSearchArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
	
		<cfset tempStruct = {sqllike="opportunity.oppType|opportunity.opportunityType",find="\[\[opportunity\.oppType\]\]|\[\[opportunity\.opportunityType\]\]",replace="[[opportunity.Type]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunity.oppStatus|opportunity.opportunityStatus|opportunity.stage.status",find="\[\[opportunity\.oppStatus\]\]|\[\[opportunity\.opportunityStatus\]\]|\[\[opportunity\.stage.status\]\]",replace="[[opportunity.Status]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>		
		<cfset tempStruct = {sqllike="opportunity.oppStage|opportunity.opportunityStage|opportunity.Stage.itemText",find="\[\[opportunity\.oppStage\]\]|\[\[opportunity\.opportunityStage\]\]|\[\[opportunity\.stage.itemText\]\]",replace="[[opportunity.Stage]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>		
		<cfset tempStruct = {sqllike="opportunity.oppcustomerType|opportunity.opportunitycustomerType",find="\[\[opportunity\.oppcustomerType\]\]|\[\[opportunity\.opportunitycustomerType\]\]",replace="[[opportunity.customerType]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunity.oppSource|opportunity.opportunitySource",find="\[\[opportunity\.oppSource\]\]|\[\[opportunity\.opportunitySource\]\]",replace="[[opportunity.Source]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
	
		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.emailDef)>
	</cffunction>
	
	<cffunction index="25" name="SearchInCodeDirectories" hint="Search For OpportunityParams.cfm in Code Directories.<BR>Reports on items which have to be dealt with Manually" access="public">

		<cfset var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		
		<cfset tempStruct = {find="opportunityParams.cfm"}>				<!--- don't pass in a Replace value so it does a Find only --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
			
		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray)>
	</cffunction>
	
	<cffunction index="26" name="SearchForJQueryInCodeDirectories" hint="Search For JQuery in Code Directories.<BR>Reports on items which have to be dealt with Manually. These should be replaced with the core jsQuery file as defined in the release notes." access="public">

		<cfset var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		
		<cfset tempStruct = {find="(?<!javascript/)jquery[^""']*?\.js"}>				<!--- finds any jquery____.js which is not in a javascript/ directory --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		<cfset tempStruct = {find="\$\.noConflict\(\)"}>				<!--- finds any noConflicts --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		
		<cfset result = FileFindAndReplace(findAndReplaceArray = findAndReplaceArray)>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction index="27" name="SearchForGetFlagGroupDataAndControlsInCodeDirectories" hint="Search For calls to getFlagGroupDataAndControls in Code Directories.<BR>Reports on items which have to be dealt with Manually. This function is now returning an array, rather than a structure." access="public">

		<cfset var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		
		<cfset tempStruct = {find="application.com.flag.getFlagGroupDataAndControls"}>				<!--- don't pass in a Replace value so it does a Find only --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
			
		<cfreturn FileFindAndReplace(findAndReplaceArray = findAndReplaceArray)>
	</cffunction>

	<cffunction index="28" name="setLocatorDefaultToV4" hint="Set locator to V4 in settings for existing clients; New clients should be using V5">
		
		<cfset var result = {isOK = true, message="",error=""}>
		<cfset var qSetLocatorToV4 = "">
		
		<cfquery datasource="#application.siteDataSource#" result="qSetLocatorToV4">		
			/*  set any existing locators to V4 as we have now introduced v5 which is the default */
			if not exists(select 1 from settings where name ='VERSIONS.LOCATOR')
			insert into settings (name,value,lastUpdatedbyPerson,lastUpdated) values ('VERSIONS.LOCATOR',4,1032,getDate())
		</cfquery>
		
		<cfif qSetLocatorToV4.recordcount is 1>
			<cfset result.message = "VERSIONS.LOCATOR updated to 4">		
		<cfelse>
			<cfset result.message = "No Change Made">		
		</cfif>
		
		<cfreturn result>
		
	</cffunction>
	<!--- 
	WAB 2013-01-31.  Change to relayDisplay.cfc to use standard code means that all valid value calls must be to functions marked as such
	--->
	<cffunction index="25" name="LookForCallsToValidValueFunctions" hint="Check that All Functions used for ValidValues have ValidValues=true attribute" access="public">

		<cfset var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		<cfset var result = {isOK = true, message="",error=""}>
		
		<!--- regExp to find func:xxxxxxx.component.methodname(--->
		<cfset tempStruct = { find="query=""func:[a-zA-Z0-9_.]*?([a-zA-Z0-9_]*?)\.([a-zA-Z0-9_]*?)\(" }>				<!--- don't pass in a Replace value so it does a Find only --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		<cfset searchResult = FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="abovecode", filter="*.cf*|*.xml",returnFoundArray=true)>	

		<!--- loop through results to get unique list of components and methodnames  (actually a structure)--->
		<cfset ComponentsAndMethods = {}>
		<cfloop collection="#searchResult.detail#" item="filename">

			<cfloop array="#searchResult.detail[filename]#" index="findResult">
					<!--- The [1] and [2] are back references to the componentName and the MethodName--->
					<cfparam name="ComponentsAndMethods[findResult[1]]" default="#structNew()#">
					<cfset ComponentsAndMethods[findResult[1]][findResult[2]] = "">
			</cfloop>
		</cfloop>
		
		<!--- Now check each component and method for the validValues attribute--->
		<cfloop collection="#ComponentsAndMethods#" item="component">
			<cfif structKeyExists (application.com,component)>
				<cfset object = application.com[component]>
				<cfset metadata = application.com.globalFunctions.getCFCMethodMetaDataStructure(object)>
				<cfloop collection = "#ComponentsAndMethods[component]#" item="method">
					<cfif structKeyExists (metadata,method)>
							<cfset functionMetadata = metadata[method]>
						<cfif not structKeyExists (functionMetadata,"validValues")>
							<cfset result.isOK = false>
							<cfset result.error &= "#component#.#method#<BR>">
						<cfelse>
							<cfset result.message &= "#component#.#method# OK<BR>">
						</cfif>
					<cfelse>
						<cfset result.error &= "function #method# not found in #component# <BR>">
					</cfif>
				</cfloop>
			<cfelse>
				<cfset result.error &= "com object #component# Not Found<BR>">
			</cfif>
		</cfloop>
		
		<cfreturn result>
	</cffunction>

	
	<cffunction index="29" name="LookForRelayTagsThatAreUsingSortOrder" hint="Checks for content that contains relay tags where the sortOrder needs slight modifying to use defaultSortOrder">
		<cfset var result = {isOK = true, message="",error=""}>
		<cfset var tempStruct = structNew()>
		<cfset var replaceAndSearchArray = ArrayNew(1)>
		
		<cfset tempStruct = {sqlcontains="<RELAY_RELATED_PERSON_LISTING|<RELAY_ELEARNING_PERSON_CERTIFICATIONS|<RELAY_ELEARNING_STUDENT_PROGRESS|<RELAY_PEOPLELIST|<RELAY_OPPAPPROVAL|<RELAY_OPPCUSTOMERLIST",find="(<(?:RELAY_RELATED_PERSON_LISTING|RELAY_ELEARNING_PERSON_CERTIFICATIONS|RELAY_ELEARNING_STUDENT_PROGRESS|RELAY_PEOPLELIST|RELAY_OPPAPPROVAL|RELAY_OPPCUSTOMERLIST)[^>]*? )(SortOrder)(.*?>)",replace="\1 default\2\3"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
	
		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.element)>
	</cffunction>
	
	
	<cffunction index="30" name="LookForOppCustomerControllerUsingOldParams" hint="Checks for OppCustomerController relaytag using UseRenewals=False then adds the parameter HideOppTypes=Renewals">
		<cfset var result = {isOK = true, message="",error=""}>
		<cfset var tempStruct = structNew()>
		<cfset var replaceAndSearchArray = ArrayNew(1)>
		
		<cfset tempStruct = {sqlcontains="<RELAY_OPPCUSTOMERCONTROLLER",find="(<(?:RELAY_OPPCUSTOMERCONTROLLER)[^>]*? )(UseRenewals=No|UseRenewals=""No""|UseRenewals=0|UseRenewals=""0""|UseRenewals=False|UseRenewals=""False"")(.*?>)",replace="\1HideOppTypes=Renewals \3"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
	
		<cfreturn updatePhrases(replaceAndSearchArray,application.entityTypeID.element)>
	</cffunction>
	
	<cffunction index="31" name="SearchSalesForceMappings" hint="Search For salesForce mappings that need changing due to translation changes.<BR>Reports on items which have to be dealt with Manually" access="public">

		<cfset var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		<cfset var result = structNew()>
		
		<cfset tempStruct = {find='table="product" column="description"'}>				<!--- don't pass in a Replace value so it does a Find only --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		<cfset tempStruct = {find='table="productGroup" column="description"'}>
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		<cfset tempStruct = {find='table="productGroup" column="productGroupName"'}>
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		
		<cfset result = FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.xml")>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction index="32" name="ChangeHomeModuleToMyFavourites" hint="Search For a reportGroup with a module of 'Home'.<BR>Reports on items which have to be dealt with Manually. If found in internalReportListExtension.xml, then replace with module='MyFavorites'" access="public">

		<cfset var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		<cfset var result = structNew()>
		
		<cfset tempStruct = {find='module="Home"'}>			<!--- don't pass in a Replace value so it does a Find only --->
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		
		<cfset result = FileFindAndReplace(findAndReplaceArray = findAndReplaceArray,relativeTo="aboveCode",filter="*.xml")>
		
		<cfreturn result>
	</cffunction>

	<cffunction index="33" name="LookForOppCustomerControllerUsingOldParamsv2" hint="Perform updates to this relaytag swapping hideOppTypes to showOppTypes">
		<cfset var result = {isOK = true, message="",error=""}>

		<cfquery name="getMatchingPhrases" datasource="#application.sitedatasource#">	
				select p.phraseText, p.phraseid, p.ident, 
				case when p.entityID <> 0 then p.phraseTextID + '_' + entityname + '_' + convert(varchar,p.entityID) else p.phrasetextID end as phraseIdentifier 
				from vphrases p
					inner join schemaTable s on s.entityTypeID = p.entityTypeID
					inner join phrases on phrases.phraseID = p.phraseID
				where
					(1=0
					OR
					(CONTAINS(phrases.phraseText, '<RELAY_OPPCUSTOMERCONTROLLER')
					and
					CONTAINS(phrases.phraseText, 'HideOppTypes=')
					)
					)
		</cfquery>
		
		<cfquery name="oppTypesToShow"  datasource="#application.sitedatasource#">
			select 0 as sort, 'SalesHistory' as value union select oppTypeID as sort, oppTypeTextID as value from oppType union select 999 as sort, 'Contacts' as value
		</cfquery>
		<cfset showOppTypes = ValueList(oppTypesToShow.value)>
	
		<cfloop query="getMatchingPhrases">
			<cfset valueArray = "">
			<cfset findArray = "">
			<cfset tempPhraseText = getMatchingPhrases.phraseText>
			<cfset findArray = application.com.regExp.reFindAllOccurrences("<(?:RELAY_OPPCUSTOMERCONTROLLER)[^>]*?",tempPhraseText)>
				<cfif arrayLen(findArray)>
					<cfset tempPhraseText = mid(tempPhraseText,findArray[1].end+1,len(tempPhraseText)-findArray[1].end+1)>
					<cfset findArray = application.com.regExp.refindSingleOccurrence(">",tempPhraseText)>
					<cfset tempPhraseText = left(tempPhraseText,findArray[1].end-1)>
					<cfset valueArray = listToArray(tempPhraseText," ")>
					<cfloop array="#valueArray#" index="i">
						<cfset param = left(i,find("=",i)-1)>
						<cfset value = mid(i,find("=",i)+1,len(i))>
						<cfif param eq "HideOppTypes">
							<cfset stringToRemove = value>
						</cfif>
						<cfif param eq "showOppTypes">
							<cfset currentshowOppTypes = value>
						</cfif>
					</cfloop>
				</cfif>
				<cfif isDefined("currentshowOppTypes") and isDefined("stringToRemove")>
					<cfset oldShowOppTypes = currentshowOppTypes>
					<cfloop list="#stringToRemove#" index="j">
						<cfset currentshowOppTypes=listDeleteAt(currentshowOppTypes,ListFindNoCase(currentshowOppTypes,j,","),",")>
					</cfloop>
					<cfset showOppTypes=currentshowOppTypes>
				<cfelseif not isDefined("currentshowOppTypes") and isDefined("stringToRemove")>
					<cfloop list="#stringToRemove#" index="j">
						<cfset showOppTypes=listDeleteAt(showOppTypes,ListFindNoCase(showOppTypes,j,","),",")>
					</cfloop>
					<cfset showOppTypes=replacelist(showOppTypes,stringToRemove,"")>
				</cfif>
			<cfset showOppTypes = "showOppTypes=" & showOppTypes>
			<cfif isDefined("oldShowOppTypes")>
				<cfset tempPhraseText = reReplaceNoCase (phrasetext,"showOppTypes=" & oldShowOppTypes,"","ALL")>
				<cfset tempPhraseText = reReplaceNoCase (tempPhraseText,"HideOppTypes=" & stringToRemove,showopptypes,"ALL")>
			<cfelse>
				<cfset tempPhraseText = reReplaceNoCase (phrasetext,"HideOppTypes=" & stringToRemove,showopptypes,"ALL")>
			</cfif>
			
			<cfquery name="updatePhrase" datasource = "#application.sitedatasource#">
			update phrases set phrasetext =  <cf_queryparam value="#tempPhraseText#" CFSQLTYPE="CF_SQL_VARCHAR" >  where ident =  <cf_queryparam value="#ident#" CFSQLTYPE="CF_SQL_Integer" > 
			</cfquery>
		 	<cfset result.message = result.message & "#phraseIdentifier# updated to #htmleditformat(tempPhraseText)# <BR>">
	
			<cfset application.com.relayTranslations.resetPhraseStructureKey(phraseIdentifier)>
			
		</cfloop>
		
		<cfreturn result>
	</cffunction>


	<cffunction index="34" name="updateEmailDefTableWithModuleTextID" hint="Change moduleID to moduleTextID on emailDef table">
		<cfset var result = {isOK = true, message=""}>		
		<cfset var qActiveModules = application.com.relayMenu.getActiveModules()>		
		<cfset var stActiveModules = {}>
		
		<cftry>
			
			<cfquery name="qCheckModuleID" datasource="#application.sitedatasource#">
				select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'moduleID'
			</cfquery>
			
			<cfif qCheckModuleID.recordCount>	
				<cfloop query="qActiveModules">
					<cfset stActiveModules[moduleID] = moduleTextId>
				</cfloop>
			
				<cfquery name="qModules" datasource="#application.sitedatasource#">
					select emailDefID, moduleID
					from emailDef
					where moduleID is not null
					and moduleTextID is null			
				</cfquery>
				
				<cfloop query="qModules">
					<cfif structKeyExists(stActiveModules,moduleID)>	
						<cfquery name="qUpdateModules" datasource="#application.sitedatasource#">
							update emailDef
							set moduleTextID =  <cf_queryparam value="#stActiveModules[moduleID]#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								moduleID = null
							where emailDefID =  <cf_queryparam value="#emailDefID#" CFSQLTYPE="cf_sql_integer" >		
						</cfquery>
					</cfif>		
				</cfloop>	
				
				<cfquery name="qUpdateTable" datasource="#application.sitedatasource#">
					alter table emailDef
					drop column moduleID
				</cfquery>	
				
				<cfset result.message = "Column moduleTextID in table emailDef has been updated and moduleID column has been dropped.">
			<cfelse>
				<cfset result.message = "Column moduleID in table emailDef doesn't exist.">
			</cfif>
			
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message & " " & cfcatch.detail>
			</cfcatch>
			
		</cftry>
		
		<cfreturn result>
	</cffunction>

</cfcomponent>

