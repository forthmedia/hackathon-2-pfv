<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false" extends="admin.release.releasescripts" index="870" active="false">

	<cffunction name="copyImageFilesFromMiscImagesToNewImageFolders" index="1" hint="Copy the image files from the dev Edit WYSIWYG locations to the new WYSIWYG folder structure.">
		
		<cfset var result = structNew()>
		<cfset var fileDetails = "">
		<cfset var globalDir = "">
		
		<cftry>
			
			<cfset globalDir = "#application.paths.content#\event\Images\Shared\Global">
			<cfif not directoryExists(globalDir)>
				<cfset application.com.globalFunctions.CreateDirectoryRecursive(fullPath=globalDir)>
			</cfif>
			
			
			<cfloop list="communication,element" index="context">
				<cfif context eq "communication">
					<cfdirectory action="list" name="imageFiles" directory="#application.paths.content#\emailImages">
				<cfelse>
					<cfdirectory action="list" name="imageFiles" directory="#application.paths.content#\miscImages">
				</cfif>
	
				<cfloop query="imageFiles">
					<cfset newImageDir = "#application.paths.content#\#context#\Images\Shared\global">
					<cfif not directoryExists(newImageDir) and imageFiles.currentRow eq 1>
						<cfset application.com.globalFunctions.CreateDirectoryRecursive(fullPath=newImageDir)>
					</cfif>
					
					<cfif type eq "file" and listFindNoCase("bmp,gif,jpeg,jpg,png",listLast(name,"."))>
						<cfset fileDetails = application.com.regExp.getFileDetailsFromPath(inputString=name)>
						<cfset filename = fileDetails.name>
						<!--- get rid of the personId in the filename if it exists --->
						<cfif listLen(fileDetails.name,"-") gt 1 and isNumeric(listLast(filename,"-"))>
							<cfset filename = left(filename,len(filename)-len(listLast(filename,"-"))-1)>
						</cfif>
						<cfset filename = filename&"."&filedetails.extension>
						<cffile action="copy" destination="#newImageDir#" source="#directory#\#name#">
						<cffile action="rename" source="#newImageDir#\#name#" destination="#newImageDir#\#filename#">
					</cfif>
				</cfloop>
			</cfloop>
			
			<cfset result.isOK = true>
			<cfif imageFiles.recordCount gt 1>
				<cfset result.message = "Successfully copied image files to #application.paths.content#\element\Images\Shared\Global and #application.paths.content#\communication\Images\Shared\Global.">
			<cfelse>
				<cfset result.message = "No images to copy.">
			</cfif>
		
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = "There seems to be a problem copying the files. #cfcatch.detail#.">
			</cfcatch>
		</cftry>
		
		<cfreturn result>
	</cffunction>

	<cffunction name="MigrateCommContent" index="2" hint="Migrate Communication Content From Files to Database (may take a few minutes)" access="public" output="false">
		<cfset var result = {isOK = true, message=""}>
		<cfsetting requestTimeOut = "3600">
		
		<cftry>
		
			<CFQUERY NAME="getCommunications" DATASOURCE="#application.SiteDataSource#">
				SELECT * FROM Communication c
				left join phraseList pl 	on pl.entityid = c.commid and pl.entityTypeID =  <cf_queryparam value="#application.entityTypeID.communication#" CFSQLTYPE="CF_SQL_INTEGER" > 
				where commformlid in (2,5,6,7)  and commtypelid not in (98,99) 
				and pl.phraseid is null
			</CFQUERY>
			
			<cfLoop query="getCommunications">
				<cfset communicationContent = getCommFileContent(CommID)>
				<cfset args = {commid = commid}>
				<cfif structKeyExists(communicationContent,"html" ) and communicationContent.html is not "">	
					<cfset args.htmlbody = communicationContent.html>
				</cfif>
				<cfif structKeyExists(communicationContent,"text" ) and communicationContent.text is not "">
					<cfset args.textbody = communicationContent.text>
				</cfif>
	
				<cfset application.com.communications.SaveCommContent(argumentCollection = args)>	
	
			</cfLoop>
	
			<cfset result.message = "#getCommunications.recordCount# records updated">
	
			<cfreturn result>
			
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = "There seems to be a problem migrating the files to the Database. #cfcatch.detail#.">
			</cfcatch>
		</cftry>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             GetCommFileContent
             This function moved from communications.cfc where it is no longer needed
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="private" name="GetCommFileContent" returnType="struct"
		hint="Returns the content of a communication">		
		<cfargument name="commid" type="string" required="true">
	
		<cfset result = structnew()>

		<cfquery name="getCommFiles" datasource="#application.siteDataSource#">
			select * from commfile where commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" > 		
		</cfquery>	

		<CFLOOP	query="getCommfiles">
		
			<cfif commformlid is 2 and commfileid is isparent>


				<CFTRY>
				<CFFILE ACTION="READ"
					    FILE="#application.userFilesAbsolutePath##Trim(Directory)#\#Trim(FileName)#"
						VARIABLE="filetext"
						charset = "UTF-8">

				<CFCATCH>
					<!--- if for some reason the file isn't there, just return blank (OK a bit rudimentary but should suffice  WAB --->
					<cfoutput>#htmleditformat(application.userFilesAbsolutePath)##htmleditformat(Trim(Directory))#\#htmleditformat(Trim(FileName))# not found</cfoutput>
					<cfset filetext = "">
				</CFCATCH>
				</CFTRY>

				<cfset emailcontent = application.com.communications.extractHTMLandText (filetext)>
				<cfset result.HTML = emailcontent.html>
				<cfset result.text = emailcontent.text>				
				<cfset result.format = emailcontent.messageformat>
			<cfelseif commformlid is 3 and commfileid is isparent>
							WAB: 2004-05-17 Code not implemented for FAXES
							this is the fax file
			<cfelseif commformlid is 3 and commfileid is not isparent>
							WAB: 2004-05-17 Code not implemented for FAXES 
							this is the fax cover
			</cfif>
				
		</CFLOOP>
		
	
		<cfreturn result>			


	</cffunction>	

	<cffunction name="UpdateOpportunityMergeFields" returntype="struct" index="3">
	
		<cfset  var replaceAndSearchArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>

		<!--- OrderConfirmationEmail,dealDeclinedEmail --->
		<cfset tempStruct = {sqllike="merge.maincontact|contactFullname",find="\[\[merge\.maincontact\]\]|\[\[contactFullName\]\]",replace="[[opportunity.endCustomer.fullname]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="details|oppDetail|detail",find="\[\[details\]\]|\[\[oppDetail\]\]|\[\[detail\]\]",replace="[[opportunity.detail]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="merge.OrganisationName|contactOrg",find="\[\[merge\.OrganisationName\]\]|\[\[contactOrg\]\]",replace="[[opportunity.endCustomer.organisationName]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="merge.opportunityproducts",find="merge\.opportunityproducts",replace="[[opportunity.opportunityProducts]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunityproducts.description",find="\[\[opportunityproducts\.description\]\]",replace="[[opportunity.opportunityProducts.description]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunityproducts.quantity",find="\[\[opportunityproducts\.quantity\]\]",replace="[[opportunity.opportunityProducts.quantity]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunityproducts.unitPrice",find="\[\[opportunityproducts\.unitPrice\]\]",replace="[[opportunity.opportunityProducts.unitPrice]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunityproducts.discount",find="\[\[opportunityproducts\.discount\]\]",replace="[[opportunity.opportunityProducts.discount]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="opportunityproducts.subtotal",find="\[\[opportunityproducts\.subtotal\]\]",replace="[[opportunity.opportunityProducts.subtotal]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="merge.grandTotal",find="\[\[merge\.grandTotal\]\]",replace="[[opportunity.grandTotal]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="merge.currency",find="\[\[merge\.currency\]\]",replace="[[opportunity.currency]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
				
		<!--- dealDeclinedEmail --->
		<cfset tempStruct = {sqllike="oppID|merge.opportunity.opportunityID",find="\[\[oppID\]\]|\[\[merge\.opportunity\.opportunityID\]\]",replace="[[opportunity.opportunityID]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="oppStatus",find="\[\[oppStatus\]\]",replace="[[opportunity.status]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<!--- can't find where this merge field is set
		<cfset tempStruct = {sqllike="[[contactAddress\]]",find="\[\[contactAddress\]\]",replace="[[opportunity.EndCustomer.address]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>--->
		
		<!--- dealApprovedEmail --->
		<cfset tempStruct = {sqllike="PartnerRepName|merge.partner.FirstName%merge.partner.Lastname",find="\[\[PartnerRepName\]\]|\[\[merge\.partner\.FirstName\]\] \[\[merge\.partner\.Lastname\]\]",replace="[[opportunity.partner.fullname]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="oppEstimatedBudget",find="\[\[oppEstimatedBudget\]\]",replace="[[opportunity.budget]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="oppExpectedCloseDate",find="\[\[oppExpectedCloseDate\]\]",replace="[[opportunity.expectedCloseDate]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="oppCurrentSituation",find="\[\[oppCurrentSituation\]\]",replace="[[opportunity.currentSituation]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		
		<!--- emailAccMngrOppStageChange --->
		<cfset tempStruct = {sqllike="merge.partner.OrganisationName",find="\[\[merge\.partner\.OrganisationName\]\]",replace="[[opportunity.partner.OrganisationName]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="merge.Stage.itemText|oppstage",find="\[\[merge\.Stage\.itemText\]\]|\[\[oppStage\]\]",replace="[[opportunity.stage]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
				
		<cfset tempStruct = {sqllike="PARTNERORGANISATIONNAME",find="\[\[PARTNERORGANISATIONNAME\]\]",replace="[[opportunity.partner.OrganisationName]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="PARTNERREPNAME",find="\[\[PARTNERREPNAME\]\]",replace="[[opportunity.partner.fullname]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>		
		<cfset tempStruct = {sqllike="AccountManager",find="\[\[AccountManager\]\]",replace="[[Opportunity.AccountManager.fullname]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>		
		<cfset tempStruct = {sqllike="partnerType",find="\[\[partnerType\]\]",replace="[[opportunity.partner.partnerType]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>		
		<cfset tempStruct = {sqllike="Opptype",find="\[\[Opptype\]\]",replace="[[opportunity.Opptype]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>		
		<cfset tempStruct = {sqllike="clientname",find="\[\[clientname\]\]",replace="[[application.clientname]]"}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<!--- oppAccountManagerNotification, dealAccountManagerApproval --->
	
		<cfreturn updatePhrases(replaceAndSearchArray,200)>
	</cffunction>

	<cffunction name="UpdateCommsUnsubscribeMergeFunctions" returntype="struct" index="4" description="For Lenovo Only - changes unsubscribe merge functions">
	
		<cfset  var replaceAndSearchArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>

		<!--- OrderConfirmationEmail,dealDeclinedEmail --->
		<cfset tempStruct = {sqllike="unsubscribe(",find="unsubscribe\(",replace="unsubscribeLink("}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>
		<cfset tempStruct = {sqllike="unsubscribeThisType(",find="unsubscribeThisType\(",replace="unsubscribeThisTypeLink("}>
		<cfset arrayAppend (replaceAndSearchArray,tempStruct)>

		<cfreturn updatePhrases(replaceAndSearchArray)>
	</cffunction>

</cfcomponent>