<!--- �Relayware. All Rights Reserved 2014

WAB 2014-09-04 moved updatePortalSearchCollections from 2013Roadmap to 2013Roadmap_2
--->
<cfcomponent output="true"  extends="admin.release.releasescripts" index="830" hint="2013 Release 2 (2014 Release 1) Scripts" active="false">

	<cffunction name="CheckUserGuidesExist" index="1" hint="Check that User Guides Exist for 2013 Roadmap Release 2">
		<cfset var result = {isOK = true, message="All the userGuides for 2013 R2 exist."}>
		<cfset var notFound = "">
		<cfset var getUserGuides  = "">
		<cfset var filesInTable = "">

		<cftry>
			<!--- get user guides from db and check if they exist --->
			<cfset getUserGuides = application.com.fileManager.getFilesForFileManagerListing(fileTypeGroup="User Guides",filetype="User Guides",checkFileExists=true)>

			<cfif getUserGuides.recordCount eq 21>
				<cfloop query="getUserGuides">
					<cfif not getUserGuides.fileExists[currentRow]>
						<cfset notFound = notFound&"<br>#filename#">
					</cfif>
				</cfloop>

				<cfif notFound neq "">
					<cfset result.message = "The following userguides do not exist and need uploading:#notFound#">
					<cfset result.isOK = false>
				</cfif>
			<cfelseif getUserGuides.recordCount gt 0>
				<cfloop query="getUserGuides">
					<cfset filesInTable = filesInTable&"<br>#filename#">
				</cfloop>

				<cfset result.message = "Not all User Guides exist in the files table. The ones that exist are:#filesInTable#">
				<cfset result.isOK = false>
			<cfelse>
				<cfset result.message = "The script to insert the User Guides entries in the files table has not been run. No records exist.">
				<cfset result.isOK = false>
			</cfif>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message&" "&cfcatch.detail>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<!--- 2014/10/16	YMA	Update createdby in custom entities to store a usergroupID and add a new column to store personID --->
	<cffunction name="updateCustomEntitiesCreatedByColumn" index="1" hint="Update createdby in custom entities to store a usergroupID and add a new column to store personID">
		<cfset var result = {isOK = true, message=""}>
		<cfset var customEntities = structNew()>
		<cfset var includenotice=false>

		<cftry>
			<!--- get list of custom entities that will need updating --->
			<cfquery name="getCustomEntities" datasource="#application.sitedatasource#">
				select stb.* from schematablebase as stb where stb.customEntity = 1
			</cfquery>
			<cfloop query="getCustomEntities">
				<cfif structKeyExists(application.entityType,"#entitytypeID#")>
					<cfset customEntities[entitytypeID] = application.entityType[entitytypeID]>
				<cfelse>
					<cfset result.message = "Error: the entity for entityTypeID #entitytypeID# is not defined in the application.  Fix this then re-run.">
					<cfset result.isOK = false>
					<cfreturn result>
				</cfif>
			</cfloop>

			<!--- for each custom entity try and update the table so the createdby field is for usergroups and add a new field createdbyperon to store the personID --->
			<cfloop collection="#customEntities#" item="entitytypeID">
				<cfquery name="hasCreatedByPerson" datasource="#application.sitedatasource#">
					select * from information_schema.columns where table_name  like  <cf_queryparam value="#customEntities[entitytypeID].tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > and column_name='createdByPerson'
				</cfquery>
				<cfif hasCreatedByPerson.recordcount eq 0>
					<!--- add createdByPerson column --->
					<cfquery name="addCreatedByPersonColumn" datasource="#application.sitedatasource#">
						alter table #customEntities[entitytypeID].tablename# add createdByPerson int NULL
					</cfquery>
					<!--- move data from createdBy to createdByPerson --->
					<cfquery name="moveData" datasource="#application.sitedatasource#">
						update #customEntities[entitytypeID].tablename#
						set createdByPerson = createdBy
					</cfquery>
					<!--- populate createdBy where possible otherwise use 404. --->
					<cfquery name="moveData" datasource="#application.sitedatasource#">
						update t
						set createdBy = isnull(ug.usergroupID,404)
						from #customEntities[entitytypeID].tablename# t
						left outer join usergroup ug on t.createdByPerson = ug.personID
					</cfquery>
					<!--- exception if the createdby = 5 that would have bene sales force and we need to change things around --->
					<cfquery name="fixSFdata" datasource="#application.sitedatasource#">
						update t
						set createdBy = 5, createdByPerson = 4
						from #customEntities[entitytypeID].tablename# t
						where createdbyperson = 5
					</cfquery>
					<cfset result.message = result.message & ' #customEntities[entitytypeID].tablename# updated. <br/>'>
					<cfset includenotice=true>
				<cfelse>
					<cfset result.message = result.message & ' #customEntities[entitytypeID].tablename# already fixed. <br/>'>
				</cfif>
			</cfloop>

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message&" "&cfcatch.detail>
			</cfcatch>
		</cftry>
		<cfif includenotice>
			<cfset result.message = result.message & '<br/><u>RELEASE MANAGER NOTICE</u><br/>Please send this list of updated tables to a configurator to validate'>
			<cfset result.message = result.message & "<br/><br/><u>CONFIGURATOR NOTICE</u><br/>If any report designer domains have been created against these custom entity tables.  The following change will need to be made: <ul><li>Any joins on createdBy to personID will need to be updated.  Use the createdbyperson field instead.</li></ul>This is because the field 'createdBy' used to store a personID but should store a usergroupID.  It has now been updated to store a usergroupID and the personID has been moved to be stored in a new field named 'createdByPerson'.">
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction index="20" name="updatePortalSearchCollections" hint="Create, register and populate SolR collections used by portal search (it might be slow!).">
		<cfset var result = {isOK = true, message="", error=""}>
		<cftry>
			<cfhttp url="#lCase(listFirst(cgi.SERVER_PROTOCOL,'/'))#://#cgi.SERVER_NAME#/scheduled/portalSearchRefresh.cfm" throwonerror="yes" timeout="1800"></cfhttp>
			<cfif listFind(cfhttp.StatusCode, '200', ' ')>
				<cfset result.message = "SolR collection has been created, populated and registered with the ColdFusion server.">
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = "Error setting up SolR collections. Status Code: " & cfhttp.StatusCode>
			</cfif>
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.error = cfcatch.message & "<br>" & cfcatch.detail>
			</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>


</cfcomponent>