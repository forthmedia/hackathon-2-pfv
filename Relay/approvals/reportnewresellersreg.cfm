<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			reportNewResellersReg.cfm.cfm	
Author:				SSS
Date started:		2007-09-17


Amendment History: This report will display client information for new resellers.

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-09-17			SSS			This report has been made generic if a client version exists it will load that if there is not a client version 
								it load the Generic version.
2007-10-29			SSS			Made a change to the keyColumnURLList to make user it will load at any level.
2008/10/07			SSS			Moved the sort order so that it is at the top of the parent file to resolve a error.
2012-10-04	WAB		Case 430963 Remove Excel Header 
								 
Possible enhancements:


--->

<!--- NYB 2008-10-03 CR-TND563 - added: SSS moved this to the top of parent file it will now work for all children--->
<cfparam name="sortorder" type="string" default="Company">

<cfif fileexists("#application.paths.code#\cftemplates\reportNewResellersReg.cfm")>
<!--- Client version of the report loads if there is one --->
	<cfinclude template ="/code/cftemplates/reportNewResellersReg.cfm">
<cfelse>
	<!--- Generic report loads if there is no client report --->
	<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
	<style>
		.red{color:red}
	</style>
	<cfparam name="regApprovalPrefix" type="string" default="">
	<cfparam name="registerDate" type="date" default="2007-05-09">
	<cfparam name="form.showcomplete" default="0">
	<cfparam name="useFullRange" type="boolean" default="true">
	<cfparam name="openAsExcel" type="boolean" default="false">
	
	<cfparam name="keyColumnList" type="string" default="Company,DistributorName"><!--- this can contain a list of columns that can be edited --->
	<cfparam name="keyColumnURLList" type="string" default="/data/dataframe.cfm?frmsrchOrgID=,../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
	<cfparam name="keyColumnKeyList" type="string" default="organisationID,distributorID"><!--- this can contain a list of matching key columns e.g. primary key --->
	
	<cfset dateRangeFieldName="dateApproved">
	<cfset dateField="bfd.created">
	<cfset tableName="booleanFlagData bfd inner join flag f on bfd.flagID=f.flagID and f.flagTextID='#regApprovalPrefix#PerApproved' and bfd.created > '#registerDate#'">
	<cfinclude template="/templates/DateQueryWhereClause.cfm">
	<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	if (not isDefined("passThruVars")) {
		passThruVars = StructNew();
	}
	StructInsert(passthruVars, "showComplete", form.showComplete);
	</cfscript>
	
	<cfquery name="getNewResellers" datasource="#application.siteDataSource#">
		select * from (
			select o.organisationID, o.organisationName as Company,l.address1+ case when len(l.address5) > 0 then ', '+l.address5 else '' end as Address,
			c.countryDescription as country,p.firstName+' '+p.lastName as Name,p.officePhone as phone,p.email,
			pd.data as distributorID,pdo.organisationName as DistributorName,perApproved.created as dateApproved,
			pc.firstName+ ' '+pc.lastName as ProgramChampion,
			case when datediff("dd",orgApproved.created,perApproved.created) < 0 then 'No' else 'Yes' end as NewReseller
				from person p inner join location l
				on p.locationID = l.locationID inner join organisation o
				on l.organisationID = o.organisationID left join integerFlagData pd
				on pd.entityID = o.organisationID and pd.flagID=2091 left join organisation pdo
				on pdo.organisationID = pd.data inner join booleanFlagData perApproved
				on p.personID = perApproved.entityID and perApproved.created > '2007-05-09' inner join flag f
				on f.flagID = perApproved.flagID and f.flagTextID='PerApproved' inner join country c
				on l.countryID = c.countryID inner join booleanFlagData orgApproved 
				on orgApproved.entityID = o.organisationID inner join flag fo
				on fo.flagID = orgApproved.flagID and fo.flagTextID='OrgApproved' left outer join integerFlagData ifd
				on ifd.entityID = o.organisationID and ifd.flagID=1870 left outer join person pc
				on ifd.data = pc.personID
			where o.organisationID <> 1
		) b
		where 1=1
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortorder#">
	</cfquery>
	
			<cf_title>New Registered Resellers</cf_title>
		
		
			<span class="messagetext">
			NB: Please restrict your search to maximum 3 month date range to prevent system overload.
	        If you need data over a larger period use the 'Quarter' search field. This can be downloaded into excel.</span>
	
		<cfinclude template="approvalsTopHead.cfm">
		
	<CF_tableFromQueryObject queryObject="#getNewResellers#" 
		FilterSelectFieldList = "Country,NewReseller"
		showTheseColumns="Company,Name,Address,Country,Phone,Email,DateApproved,NewReseller,DistributorName,ProgramChampion"
		sortOrder="organisationID"
		queryWhereClauseStructure="#queryWhereClause#"
		passThroughVariablesStructure="#passThruVars#"
		openAsExcel="true"
		keyColumnList="#keyColumnList#"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="#keyColumnKeyList#"
		keyColumnOpenInWindowList="yes,yes"
		selectbyday="true"
	>

</cfif>

