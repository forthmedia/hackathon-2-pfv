<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
	File name:			RegistrationProcess.cfm
	Author:				NYF
	Date started:		2008/05/27

	DESCRIPTION:
	Template runs Registration processes that aren't possible to be done via a Workflow/Process

	BEHAVIOURS:
	* Set PrimaryContact to current registrant - if not already set
	* If HQ not already set, to set address of Primary Contact (if exists), else to address of registrant
	* Creates Username and Password for a user set to Approved

	OPTIONS:
	* Use RegPrefix to prefix approval flagtextids from the base standard names  (Still to be added to the code)
	* Registrants added to existing Approved orgs set to Approved based on switch in RelayINI

	AMENDMENT HISTORY:
	NYB 2009-03-30 - Sophos Bugzilla 2038 - Change Replace( to ReplaceNoCase( throughout code:
	WAB 2010/10/19 get regAdditionOptions from settings

	2013-07-11 PPB Case 433798 don't automatically set the registrant to be the primary contact if there isn't one because they are not approved yet (but still synch to SF)
	2013-08-27 YMA Case 436690 Change PPB's change for case 433798 to be a switch in settings.
	2016/09/05	NJH	JIRA PROD2016-1313 - change org approval to support both loc and/or organisation approval. Have called them 'accounts'
--->

<!--- +++++++++++++ Set/Get Variables +++++++++++++ --->
<cfparam name="PrimaryContactFlagID" default="KeyContactsPrimary">
<cfparam name="HQFlagID" default="HQ">
<cfparam name="RegPrefix" default="">
<!--- NYB 05-10-2010 P-FNL079 - added: --->
<cfparam name="PrimaryLeadContactFlagID" default="PrimaryLeadContact">

<cfset regAdditionOptions = application.com.settings.getsetting("registrationApproval.additionOptions")>
<cfset autoSetPrimaryContact = application.com.settings.getsetting("registrationApproval.autoSetPrimaryContact")>

<cfset accountApprovalEntityType = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"location":"organisation">
<cfset accountEntityType = application.com.relayEntity.getEntityType(entityTypeId=accountApprovalEntityType)>
<cfset accountPrefix = left(accountApprovalEntityType,3)>
<cfset accountApprovalFlagPrefix = RegPrefix& accountPrefix>
<cfset accountApprovalFlagGroupTextID = "#accountApprovalFlagPrefix#ApprovalStatus">
<cfset PerApprovalFlagGroupTextID = "#RegPrefix#PerApprovalStatus">

<!--- Needs to be able to work after a screen when frmPersonID was not defined --->
<cfif not isDefined("frmPersonID")>
	<cfif isDefined("frmEntityTypeID") and frmEntityTypeID is 0 and isDefined("frmCurrentEntityID")>
		<cfset frmPersonID = frmCurrentEntityID>
	<cfelse>
		<cfset frmPersonID = request.relaycurrentuser.personid>
	</cfif>
</cfif>

<cfset GetPersonDetails = application.com.commonQueries.getFullPersonDetails (personID = frmPersonID)>

<cfset variables.LocationID = Getpersondetails.LocationID>
<cfset variables.OrganisationID = Getpersondetails.OrganisationID>
<cfset frmCountryID = Getpersondetails.CountryID>

<cfset GetAccountApprovalStatus = application.com.flag.getFlagGroupData (flagGroupId = accountApprovalFlagGroupTextID, entityID = variables[accountEntityType.uniqueKey])>
<cfset accountStatusFlagTextID = GetAccountApprovalStatus.flagTextID>

<cfset GetPerApprovalStatus = application.com.flag.getFlagGroupData (flagGroupId = PerApprovalFlagGroupTextID, entityID = frmPersonID)>
<cfset PerStatusFlagTextID = GetPerApprovalStatus.flagTextID>

<!--- +++++++++++++ Processing +++++++++++++ --->
<!--- Set Primary Contact & HQ  --->
<cfif accountStatusFlagTextID neq "#accountApprovalFlagPrefix#Rejected">
	<!--- NJH 2016/07/19 - JIRA PROD2016-599 - deal with primary contact being at location level --->
	<cfset GetPrimaryContact = queryNew("data")>
	<cfset flagEntityId = 0>

	<cfset primaryContactFlagStruct = application.com.flag.getFlagStructure(flagID=PrimaryContactFlagID)>
	<cfif primaryContactFlagStruct.isOK and primaryContactFlagStruct.flagActive>
		<cfset flagEntityId = variables[accountEntityType.uniqueKey]>
		<cfset flagEntityId = primaryContactFlagStruct.entityType.tablename eq "location"?frmLocationID:frmOrganisationId>
		<cfset GetPrimaryContact = application.com.flag.getFlagData (flagid = PrimaryContactFlagID, entityID = flagEntityId)>
	</cfif>
	<cfset GetHQ = application.com.flag.getFlagData (flagid = HQFlagID, entityID = frmOrganisationID)>

	<!--- NYB 05-10-2010 P-FNL079 - added START --->
	<cfset PriLeadContactFlagID = application.com.flag.doesFlagExist(PrimaryLeadContactFlagID)><!--- gets numericID - returns 0 ie false if not exists --->
	<cfif PriLeadContactFlagID neq 0>
		<cfset GetPriLeadContact = application.com.flag.getFlagData (flagid = PriLeadContactFlagID, entityID = frmOrganisationID)>
		<cfif GetPriLeadContact.recordcount eq 0>
			<cfset application.com.flag.setFlagData (flagid = PriLeadContactFlagID, entityID = frmOrganisationID, data = frmPersonID)>
		</cfif>
	</cfif>
	<!--- NYB 05-10-2010 P-FNL079 - added END --->

	<!--- 2013-07-11 PPB Case 433798 don't automatically set the registrant to be the primary contact if there isn't one because they are not approved yet (but still synch to SF) --->
	<!--- 2013-08-27 YMA Case 436690 Change PPB's change to be a switch in settings. --->
	<cfif GetPrimaryContact.recordcount eq 0 and autoSetPrimaryContact and flagEntityId neq 0>
		<cfset application.com.flag.setFlagData (flagid = PrimaryContactFlagID, entityID = flagEntityId, data = frmPersonID)>
	</cfif>


	<cfif GetHQ.recordcount eq 0>
		<cfif GetPrimaryContact.recordcount eq 0>
			<cfset application.com.flag.setFlagData (flagid = HQFlagID, entityID = frmOrganisationID, data = frmLocationID)>
		<cfelse>
			<!--- NJH 2016/07/19 JIRA PROD2016-351 - I've left this for now as we only get in here when the org doesn't have an HQ set. However, this should no longer be the case as the first location that gets created is set as the HQ automatically
			If this is a problem, then we need to work out what location to set, as now multiple primary contacts can now exist --->
			<cfset GetPCLocID = application.com.commonQueries.getFullPersonDetails (personID = GetPrimaryContact.data).LocationID>
			<cfset application.com.flag.setFlagData (flagid = HQFlagID, entityID = frmOrganisationID, data = GetPCLocID)>
		</cfif>
	</cfif>
</cfif>

<!--- If ORG/Loc is New --->
<cfif GetAccountApprovalStatus.recordcount eq 0>
	<cfset form.accountApprovalStatus = "Pending">
	<cfset form.PerApprovalStatus = form.accountApprovalStatus>
	<cfset application.com.flag.setbooleanflag(entityid=variables[accountEntityType.uniqueKey], flagtextid="#accountApprovalFlagPrefix#Applied")>
	<cfset application.com.flag.setbooleanflag(entityid=frmpersonid, flagtextid="#RegPrefix#PerApplied")>

<!--- If ORG/Loc=APPROVED --->
<cfelseif accountStatusFlagTextID eq "#accountApprovalFlagPrefix#Approved">
	<cfset form.accountApprovalStatus = "Approved">
	<cfset form.PerApprovalStatus = form.accountApprovalStatus>
	<cfif PerStatusFlagTextID neq "#RegPrefix#PerRejected">
		<cfif RegAdditionOptions contains "4" or (RegAdditionOptions contains "2" and structKeyExists(session,"UserInvited")) or (RegAdditionOptions contains "3" and request.relaycurrentuser.isInternal) or (PerStatusFlagTextID eq "#RegPrefix#PerApproved")>
			<cfset application.com.flag.setbooleanflag(entityid=frmpersonid, flagtextid="#RegPrefix#PerApproved", deleteOtherRadiosInGroup="true")>
			<cf_createUserNameAndPassword personid = #frmPersonID# resolveDuplicates = true
				OverWritePassword = false OverWriteUserName = false>
		<cfelse>
			<cfset application.com.flag.setbooleanflag(entityid=frmpersonid, flagtextid="#RegPrefix#PerApplied", deleteOtherRadiosInGroup="true")>
			<cfset form.PerApprovalStatus = "Pending">
		</cfif>
	</cfif>

<!--- If ORG/Loc not APPROVED or New --->
<cfelse>
	<!--- NYB 2009-03-30 Sophos Bugzilla 2038 - Replaced Replace with ReplaceNoCase --->
	<cfset form.accountApprovalStatus = ReplaceNoCase(ReplaceNoCase(accountStatusFlagTextID,"#RegPrefix#Org","","one"),"Applied","Pending","one")>
	<cfset form.PerApprovalStatus = form.accountApprovalStatus>

	<cfset PerStatusTo = ReplaceNoCase(accountStatusFlagTextID,accountPrefix,"Per","one")>
	<cfif PerStatusFlagTextID neq "#RegPrefix#PerRejected">
		<cfset application.com.flag.setbooleanflag(entityid=frmpersonid, flagtextid=PerStatusTo, deleteOtherRadiosInGroup="true")>
	</cfif>
</cfif>

