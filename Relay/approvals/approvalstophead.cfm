<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="list">
<cfparam name="attributes.pageTitle" type="string" default="Approvals">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<!--- NJH 2015/02/12 Jira task Fifteen-140 - removed back button for approvalprocess.cfm  --->
	<cfif listLast(SCRIPT_NAME,"/") neq "regApprovalReport.cfm" and listLast(SCRIPT_NAME,"/") neq "approvalProcess.cfm">
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:history.go(-#attributes.pagesBack#);">
	</cfif>

	<cfif listFindNoCase("ReportNewResellersReg.cfm,regApprovalReport.cfm",listLast(SCRIPT_NAME,"/"))>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</cfif>
</CF_RelayNavMenu>


