<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	File name:			RegApprovals-Org.cfm
	Author:				NYF
	Date started:		2008/05/27

	DESCRIPTION:
	Template runs Organisation Approval processes that aren't possible to be done via a Workflow/Process.
	When person set to approved it checks if the org is approved - if not, it will set the person to applied then display a relevant error message

	BEHAVIOURS:
	* Creates Username and Password for a user set to Approved
	* Sends Approved & Rejected emails out to those users given this status

	OPTIONS:
	* Uses notifyChange to turn off all screen messages - allowing it to be used for manageColleagues.cfm

	AMENDMENT HISTORY:

	2015/01/28		NJH		Param'd session.PerApprovalMsg as it's being referenced but not always set. Any reason why this is in a session variable??
	2016/09/05		NJH		JIRA PROD2016-1313 - change org approval to support both loc and/or organisation approval. Have called them 'accounts'

--->


<cfparam name="notifyChange" type="boolean" default="true">
<cfparam name="RegPrefix" default="">
<cfparam name="ApprovedEmailTextID" default="RegistrationApprovedEmail">
<cfparam name="RejectedEmailTextID" default="RegistrationRejectedEmail">

<cfset accountApprovalEntityType = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"location":"organisation">
<cfset accountEntityTypeUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=accountApprovalEntityType).uniqueKey>
<cfset accountApprovalFlagPrefix = regPrefix&left(accountApprovalEntityType,3)>

<cfset accountApprovalFlagGroupTextID = "#accountApprovalFlagPrefix#ApprovalStatus">
<cfset PerApprovalFlagGroupTextID = "#RegPrefix#PerApprovalStatus">

<cfset GetPersonDetails = application.com.commonQueries.getFullPersonDetails (personID = frmPersonID)>

<cfset firstname = Getpersondetails.firstname>
<cfset lastname = Getpersondetails.lastname>
<cfset email = Getpersondetails.email>
<cfset frmCountryID = Getpersondetails.CountryID>
<cfset primaryCountryID = Getpersondetails.CountryID>

<cfset GetAccountApprovalStatus = application.com.flag.getFlagGroupData (flagGroupId = accountApprovalFlagGroupTextID, entityID = GetPersonDetails[accountEntityTypeUniqueKey])>
<cfset accountStatusFlagTextID = GetAccountApprovalStatus.flagTextID>


<cfset GetPerApprovalStatus = application.com.flag.getFlagGroupData (flagGroupId = PerApprovalFlagGroupTextID, entityID = frmPersonID, additionalColumns="DATEDIFF(mi, fd.lastupdated, getDate()) as LastUpdatedDiff")>
<cfset PerStatusFlagTextID = GetPerApprovalStatus.flagTextID>
<cfset PerApprovalStatusName = GetPerApprovalStatus.flagname>
<cfset PerApprovalStatusUpdated = GetPerApprovalStatus.LastUpdatedDiff>


<!--- Check if org has been set to Approved --->
<cfif PerApprovalStatusUpdated lt "2">
	<cfif PerStatusFlagTextID eq "#RegPrefix#PerApproved">
		<!--- only allows users to be set to Approved if the Org/location is Approved --->
		<cfif accountStatusFlagTextID eq "#accountApprovalFlagPrefix#Approved">
			<cf_createUserNameAndPassword
				personid = #frmPersonID#
				resolveDuplicates = true
				OverWritePassword = false
				OverWriteUserName = false>
			<cfset application.com.email.sendEmail(emailtextid = ApprovedEmailTextID,personid = frmPersonID, countryid = primaryCountryID)>

			<cfif notifyChange>
				<cfset session.PerApprovalMsg = "Phr_ApprovalEmailSentto  #firstname# #lastname# (#email#)">
			</cfif>
		<cfelseif accountStatusFlagTextID eq "#accountApprovalFlagPrefix#Applied">
			<cfset application.com.flag.setbooleanflag(entityid=frmpersonid, flagtextid="#RegPrefix#PerApplied", deleteOtherRadiosInGroup="true")>
			<cfif notifyChange>
				<cfset session.PerApprovalMsg = "phr_UserCouldNotBeApproved.">
			</cfif>
		</cfif>
	<cfelseif PerStatusFlagTextID eq "PerRejected">
		<cfset application.com.email.sendEmail(emailtextid = RejectedEmailTextID,personid = frmPersonID, countryid = primaryCountryID)>
		<cfif notifyChange>
			<cfset session.PerApprovalMsg = "Phr_RejectionEmailSentto #htmleditformat(firstname)# #htmleditformat(lastname)# (#htmleditformat(email)#)">
		</cfif>
	<cfelse>
		<cfif notifyChange>
			<cfset session.PerApprovalMsg = "Phr_PersonApprovalStatusSetTo  #htmleditformat(PerApprovalStatusName)#">
		</cfif>
	</cfif>
<cfelse>
	<cfif notifyChange>
		<cfset session.PerApprovalMsg = "Phr_PersonApprovalStatusIs  #htmleditformat(PerApprovalStatusName)#">
	</cfif>
</cfif>

<cfif notifyChange>
	<cfparam name="session.PerApprovalMsg" default=""> <!--- NJH default this as not always set --->
	<cfset session.PerApprovalMsg = "#session.PerApprovalMsg#<br/><br/>Phr_ClickHereToCloseWindow">
</cfif>