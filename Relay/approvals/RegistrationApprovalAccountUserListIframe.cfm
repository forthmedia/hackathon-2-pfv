<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	File name:			managePeopleApprovalScreenIframe.cfm
	Author:				NYF
	Date started:		2008/05/27

	DESCRIPTION:
	calls in file relay/RelayTagTemplates/managePeopleApprovalStatus.cfm inside an iFrame

	AMENDMENT HISTORY:
	2016/09/05	NJH	JIRA PROD2016-1313 - change org approval to support both loc and/or organisation approval. Have called them 'accounts'

--->

<cfset accountApprovalEntityType = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"location":"organisation">
<cfset accountEntityTypeUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=accountApprovalEntityType).uniqueKey>

<cfparam name="changableStatuses" default="PerApplied,PerApproved,PerRejected,PerInactive">
<cfparam name="sortOrder" default="firstname,lastname">
<cfparam name="variables[accountEntityTypeUniqueKey]" default="#request.relayCurrentUser[accountEntityTypeUniqueKey]#">

<cf_includejavascriptonce template = "/javascript/openwin.js">
<cfoutput>
	<p align="right" style="padding-right: 20px"><input type="Button" onclick="JavaScript:verifyForm();" value="phr_Continue" class="screenButton"></p>
	<iframe src="/relayTagTemplates/managePeopleApprovalStatus.cfm?ManagePeopleApprovalFlags=#htmleditformat(changableStatuses)#&sortOrder=#htmleditformat(sortOrder)#&#accountEntityTypeUniqueKey#=#htmleditformat(variables[accountEntityTypeUniqueKey])#"
	        name="ApprovePeopleList"
	        id="ApprovePeopleList"
	        width="590"
	        height="5000"
	        scrolling="no"
			frameborder="0"
			marginwidth="0"
			marginheight="0"
			>
	</iframe>
</cfoutput>