<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:			regApprovalReport.cfm
Author:				DAM
Date started:		June 2002

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

		Registration Approval Report

		RegApprovalReport lists the registrations that have been set up in the
		system and provides links thru to launch a screen which is used to approve
		or decline them.  This screen is modified on a site by site basis so that
		site specific stuff can be implemented.

		Two flagGroups/profiles are used to manage approvals at a person and organisation level.

		These always have the flagGroupTextID PerApprovalStatus and orgApprovalStatus respectively.
		These are initially set to applied when the partner applies to join the program.
		They are then updated to approved or rejected from this template by an approvals person.

		The process of the approvals is defined in a processAction workflow with a
		processTextID called RegApproval.

		2004-12-20 - JC/WAB <CFSET SecurityList="#SecurityTask#:edit"> modified to removed additional 'view' from list
		14/4//5    WAB modification to open in excel, but actually doesn't work properly yet.
		23/5//5    WAB got it to work properly
		23/5//5    WAB also reformatted so that when opening in excel it doesn't export all the links
		2005-06-01	WAB ACTION 624.  Corrected javascript bug when opening dataframe.  Now sets the focus.
		2005-06-01  	WAB changed so that people with view rights can view the report
		2006-4-28 	MDC Changed to show an Client specific registration approval process if it exists.
		2006-20-25	WAB altered so that statuses come directly from the flags table.  Also made the change link into a popup.
		2007-06-12	WAB  problem because what links were showing was running off the status flagNames (which were then being changed in the front end).
						Changed so that now runs off the flagTextIDs with the prefix and org/per removed
						Query brings back column PersonStatus (eg applied) and PersonStatusName (eg: Application Submitted)
		2008/06/11	NJH P-CRL501 replace radio buttons with a dropdown of statuses and counts and added 'with (noLock)' to the queries.
		2008/06/23 	NYF Bug Resolution.  Change to getCountApprovalFlags query
		2008-08-04	AJC	P-CRL501 - Relayware 8.0 - 3 Pane View Issue
		2008/10/03	NJH	Bug Fix T-10 Issue 930 open the person details into a new tab/window
		2009/01/28	NJH	Bugs All Sites Issue 1699 - exclude orgs and people flagged for deletion
		2010/11/31 WAB	LID 4916 - query to get countries was timing out.  fixed by doing a join
		2011/11/28	NYB	LHID8224 replaced "No records were Found." with sys_noRecordsFound
		2012/03/02 	PPB Case 426774 added EndCustomer filter

2012/04/04			RMB			CASE:427219 - I Have made quite a few changes to the process,
								I have removed the ColdFusion code that built the SQL script
								for the filter options on the Org and Person Registration Status,
								which also including the number of people that fell into each
								combination of status. This has now been simplified into two
								individual drop downs options without the SQL that draws the
								number records which fall into each status type. The filter
								options have changed from one variable "registrationStatusFilter"
								into two filter variables "useStatusOrgType" and "useStatusPerType",
								though it will still accept the variable "registrationStatusFilter"
								but code has put in place to split that out into the two new filter variables.
2012/06/13	IH	Case 428800 add closing form tag, move form tags outside of table
2012/06/15	IH	Case 428832 add requesttimeout=180
2012-10-01 WAB	Case 430460. Altered the country list to always show the current filter country - if there were no records of the required status then the country would not appear in the FilterCountries query
2014-09-23 RMC  altered link text from ‘Approve’ to ‘Approve Organization’ and from ‘Approve’ to ‘Approve Person’ as requested by Minu
2014-09-26 SB - Added html for styling
2015/02/10	NJH	Jira Fifteen-89	Re-worked some of the variables. The form variables were not present when exporting to excel, so the filters were not getting applied when the filters were changing. Depend on the session scope now for filtering
				This whole report needs a rework!!
2015-09-08	ACPK	PROD2015-9 Added All Countries option to Countries filter and set as default value, removed Country Groups from filter, added Country column to table
2016/09/05	NJH	JIRA PROD2016-1313 - change org approval to support both loc and/or organisation approval. Have called them 'accounts'
2016/10/04	NJH		JIRA PROD2016-2429 - removed query caching.
--->

<cfsetting requesttimeout="180">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="alphabeticalIndex" default="">
<cfparam name="startrow" default="1">
<cfparam name="numRowsPerPage" default="50">

<!--- NJH 2012/05/26 - added as export to excel not working --->
<cfif openAsExcel>
	<cfcontent type="application/msexcel">
	<cfheader name="content-Disposition" value="filename=RegApprovalReport.xls">
	<cfinclude template="/templates/excelHeaderInclude.cfm">
</cfif>

<!--- Client Specific Registration Approval --->
<cfif  fileexists("#application.paths.code#\cftemplates\ClientSpecificRegistrationApproval.cfm")>
	<cfinclude template="/code/cftemplates/ClientSpecificRegistrationApproval.cfm">

<!--- Core Registration Approval - Start --->
<cfelse>

	<CFPARAM name = "RegApprovalPrefix" default = "">

	<cfset accountApprovalEntityType = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"location":"organisation">
	<cfset accountApprovalFlagPrefix = RegApprovalPrefix&left(accountApprovalEntityType,3)>

	<!--- CASE:427219 - RMB - Old filter option catch "registrationStatusFilter" --->
	<cfif isDefined("registrationStatusFilter")>
		<cfset useStatusPerType = listFirst(registrationStatusFilter,"|")>
		<cfset useStatusAccountType = listLast(registrationStatusFilter,"|")>
	</cfif>

	<!--- CASE:427219 - RMB - new filter options --->
	<cfparam name="useStatusAccountType" type="string" default="#RegApprovalPrefix#0">
	<cfparam name="useStatusPerType" type="string" default="#RegApprovalPrefix#PerApplied">
	<cfparam name="accountStatusFilter" default="#useStatusAccountType#">
	<cfparam name="perstatusFilter" default="#useStatusPerType#">

	<cfif structKeyExists(form,"useStatusAccountType")>
		<cfset form.accountStatusFilter = form.useStatusAccountType>
	</cfif>
	<cfif structKeyExists(form,"useStatusPerType")>
		<cfset form.perStatusFilter = form.useStatusPerType>
	</cfif>

		<cfquery name="getRegProcess" datasource="#application.siteDataSource#">
		SELECT DISTINCT
			CASE WHEN charindex('_',flaggroupTextID) = 0 THEN ''
			ELSE SUBSTRING(flaggroupTextID, 1, charindex('_',flaggroupTextID)) END AS 'Prefix',
			name,
			CASE WHEN fg.editaccessrights = 0 AND fg.edit = 1 THEN 1
					WHEN fg.editaccessrights = 1 AND fgr.edit = 1 THEN 1
					ELSE 0 END AS 'hasEditRights',
			CASE WHEN fg.viewingaccessrights = 0 AND fg.viewing = 1 THEN 1
					WHEN fg.viewingaccessrights = 1 AND fgr.viewing = 1 THEN 1
					ELSE 0 END AS 'hasViewRights',
			fgr.edit,
			fg.editaccessrights,
			fg.edit
		FROM
			flagGroup fg with(noLock) left join (flaggrouprights fgr with(noLock) inner join rightsGroup rg with(noLock) on fgr.userid = rg.usergroupid and rg.personid = #request.relayCurrentUser.personid# AND (fgr.EDIT = 1 or fgr.viewing=1)) on fg.flaggroupid = fgr.flaggroupid
		WHERE
			flaggroupTextID =  <cf_queryparam value="#accountApprovalFlagPrefix#ApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<!--- CASE:427219 - RMB - used in new org status filter option - also added union to add an All option --->
		<cfquery name="getAccountFlags" datasource="#application.siteDataSource#">
		SELECT
			1 AS 'OrderMe',
			flagid,
			flagtextid,
			'phr_' + f.NamePhraseTextID AS 'Display',
			flagtextid AS 'selectValue',
			f.name AS 'name'
		FROM
			flag f with(noLock)
			INNER JOIN flagGroup fg WITH(NOLOCK) ON f.flaggroupid = fg.flaggroupid
		WHERE
			flagGroupTextID =  <cf_queryparam value="#accountApprovalFlagPrefix#ApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" >

		UNION ALL

		SELECT
			2 AS 'OrderMe',
			0 AS 'flagid',
			'0' AS 'flagtextid',
			'phr_approval_allAccountTypes' AS 'Display',
			'0' AS 'selectValue',
			'All #accountApprovalEntityType# Statuses' AS 'name'
		ORDER BY
			OrderMe,
			name
		</cfquery>

		<!--- CASE:427219 - RMB - used in new person status filter option - also added union to add an All option --->
		<cfquery name="getperFlags" datasource="#application.siteDataSource#">
		SELECT
			1 AS 'OrderMe',
			flagid,
			flagtextid,
			'phr_' + f.NamePhraseTextID AS 'Display',
			flagtextid AS 'selectValue',
			f.name AS 'name'
		FROM
			flag f with(noLock)
			INNER JOIN flagGroup fg WITH(NOLOCK) ON f.flaggroupid = fg.flaggroupid
		WHERE
			flagGroupTextID =  <cf_queryparam value="#regApprovalPrefix#perApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" >

		UNION ALL

		SELECT
			2 AS 'OrderMe',
			0 AS 'flagid',
			'0' AS 'flagtextid',
			'phr_approval_allpertypes' AS 'Display',
			'0' AS 'selectValue',
			'All Person Statuses' AS 'name'
		ORDER BY
			OrderMe,
			name
		</cfquery>

		<cfif not openAsExcel>
			<cf_includejavascriptonce template = "/javascript/openWin.js">
			<cf_includejavascriptonce template = "/javascript/dropdownFunctions.js">
			<cf_includejavascriptonce template = "/javascript/checkBoxFunctions.js">
			<cf_includejavascriptonce template = "/javascript/viewEntity.js">
			<cf_includejavascriptonce template = "/javascript/extExtension.js">
		</cfif>

		<!--- 2015-09-08	ACPK	PROD2015-9 Changed default value of Country filter to All Countries --->
		<CFPARAM NAME="countryfilter" DEFAULT="0">
		<CFIF structKeyExists(form,"perStatusFilter") and (not structKeyExists(session,"perStatusFilter") or (form.perStatusFilter NEQ session.perStatusFilter))>
			<CFSET session.perStatusFilter= form.perStatusFilter>
		</CFIF>
		<CFIF structKeyExists(form,"accountStatusFilter") and (not structKeyExists(session,"accountStatusFilter") or (form.accountStatusFilter NEQ session.accountStatusFilter))>
			<CFSET session.accountStatusFilter= form.accountStatusFilter>
		</CFIF>

		<CFIF structKeyExists(form,"countryfilter") and (not structKeyExists(session,"countryfilter") or (form.countryfilter NEQ session.countryfilter))>
			<CFSET session.countryfilter= form.countryfilter>
		</CFIF>

		<CFIF structKeyExists(session,"perStatusFilter")>
			<CFSET perStatusFilter= session.perStatusFilter>
		</CFIF>

		<CFIF structKeyExists(session,"accountStatusFilter")>
			<CFSET accountStatusFilter= session.accountStatusFilter>
		</CFIF>

		<CFIF structKeyExists(session,"countryfilter")>
			<CFSET countryfilter= session.countryfilter>
		</CFIF>
		<cfset accountEntityType = application.com.relayEntity.getEntityType(entityTypeId=accountApprovalEntityType)>

		<CFQUERY NAME="getApprovals" DATASOURCE="#application.sitedatasource#">
		SELECT
			p.personid,p.firstname  +' '+ p.lastname as fullname,
			fp.name as PersonStatusName,
			replace(fp.flagTextID, '#regApprovalPrefix#Per','') as PersonStatus,
			bp.created as PersonStatusDate ,
			p.#accountEntityType.uniqueKey# as accountID,
			a.#accountEntityType.nameExpression# as accountName,
		 	fa.name AS accountStatusName,
		 	replace(fa.flagTextID, '#accountApprovalFlagPrefix#','') as AccountStatus,
			ba.created as accountStatusDate,
			c.countryDescription, c.countryid,
			LEFT(UPPER(a.#accountEntityType.nameExpression#),1) as firstLetter
		FROM
			Person p with(noLock)
			INNER JOIN #accountApprovalEntityType# a with(noLock) ON P.#accountEntityType.uniqueKey# = a.#accountEntityType.uniqueKey#
			left JOIN organisationType ot ON ot.organisationTypeID=a.#accountApprovalEntityType eq "location"?"accountTypeID":"organisationTypeID"#
			-- NJH 2016/11/16 - change to left join so that we can pick up all people as some people get created without an approval status
			left JOIN (booleanflagdata bp with(noLock)
						INNER JOIN flag fp with(noLock) ON fp.flagid = bp.flagid
						INNER JOIN flaggroup fgp with(noLock) ON fgp.flaggroupid = fp.flaggroupid AND fgp.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#PerApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" >
						) ON bp.entityid = p.personID
			left JOIN (booleanflagdata ba with(noLock)
						 INNER JOIN flag fa with(noLock) ON fa.flagid = ba.flagid
						 INNER JOIN flaggroup fga with(noLock) ON fga.flaggroupid = fa.flaggroupid AND fga.flaggrouptextid =  <cf_queryparam value="#accountApprovalFlagPrefix#ApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" >
						) ON ba.entityid = p.#accountEntityType.uniqueKey#
			INNER JOIN country c with(noLock) ON a.countryid = c.countryid
			#application.com.rights.getRightsFilterQuerySnippet(entityType=accountApprovalEntityType,alias="a").join#

			-- NJH 2009/01/28 Bugs All Sites Issue 1699 - exclude orgs and people flagged for deletion
			LEFT JOIN booleanFlagData bfpd with (noLock) on bfpd.entityID = p.personID and bfpd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure("deletePerson").flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			LEFT JOIN booleanFlagData bfad with (noLock) on bfad.entityID = p.#accountEntityType.uniqueKey# and bfad.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure("delete#accountApprovalEntityType#").flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			  WHERE
			  	isNull(ot.TypeTextID,'Reseller') not in ('EndCustomer','AdminCompany')	<!--- 2012/03/02 PPB Case 426774 added EndCustomer filter  --->
			  -- NJH 2009/01/28 Bugs All Sites Issue 1699 - exclude orgs and people flagged for deletion
			  AND	bfpd.entityID is null
			  AND bfad.entityID is null
			  <CFIF countryfilter NEQ 0>
			  	AND a.countryid =  <cf_queryparam value="#countryfilter#" CFSQLTYPE="CF_SQL_INTEGER" >
			  </CFIF>
			  <CFIF perstatusFilter NEQ 0>
			  	AND fp.flagTextID =  <cf_queryparam value="#perstatusFilter#" CFSQLTYPE="CF_SQL_VARCHAR" >
			  </CFIF>
			  <CFIF accountStatusFilter NEQ 0>
				AND fa.flagTextID =  <cf_queryparam value="#accountStatusFilter#" CFSQLTYPE="CF_SQL_VARCHAR" >
			  </CFIF>
			 ORDER BY a.#accountEntityType.nameExpression#, p.lastname
		</CFQUERY>

		<!--- This query just brings back a list of countries for the filtering
			CRM 427219 - NJH 2012/05/25 - This query was taking about 8 secs to run on Netgear (as they have 120000+ people)
			It was getting the country for every approval status there was. I have filtered it further to show only the countries
			for the current approval statuses... (the person and organisation approval status combination)
			WAB 2012-10-02 	CASE 430460 Turned out to be a problem for users because not all countries appeared in the drop down (and drop down did not always reflect the filtered country)
							Re-worked so that all countries appear in the query and are grouped by whether there is data for them at the current status.  We (VAB&WAB) have called the Groups 'Valid' and 'Other' for want of anything else
							Seemed to run better with a temporary table rather than a sub select
							Added a bit of caching and use of new/updated views vCountryRights, vBooleanFlagData
		--->
		<CFQUERY NAME="FilterCountries" DATASOURCE="#application.sitedatasource#">
		declare @countryid table (countryid int)
					insert into @countryid
					Select distinct countryid from
					#accountApprovalEntityType# a with(noLock)
					inner join Person p with(noLock) ON p.#accountEntityType.uniqueKey# = a.#accountEntityType.uniqueKey#
					INNER JOIN vbooleanflagdata bp with(noLock) ON bp.entityid = p.personID			<CFIF perstatusFilter NEQ 0>and bp.flagTextID=<cf_queryparam value="#perstatusFilter#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse>and bp.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#PerApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
					INNER JOIN vbooleanflagdata ba with(noLock) ON ba.entityid = a.#accountApprovalEntityType#ID	<CFIF accountStatusFilter NEQ 0>and ba.flagTextID = <cf_queryparam value="#accountStatusFilter#" CFSQLTYPE="CF_SQL_VARCHAR" ><CFELSE>and ba.flaggrouptextid =  <cf_queryparam value="#accountApprovalFlagPrefix#ApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
		<!--- 2015-09-08	ACPK	PROD2015-9 Added All Countries option to Countries filter --->
		SELECT
			'All Countries' as countryDescription,
			0 as countryID,
			'Valid' as Grouping,
			0 as ordering
		UNION
			SELECT DISTINCT
					c.countrydescription  ,
					c.countryid ,
					case when temp.countryid is not null then 'Valid' else 'Others' end as Grouping,
					case when temp.countryid is not null then 0 else 1 end as ordering
				FROM
					country c inner join vcountryrights r with(noLock) ON r.countryid = c.countryid and personid = #request.relaycurrentuser.personid#
					left JOIN @countryid temp ON c.CountryID = temp.countryid
				<!--- 2015-09-08	ACPK	PROD2015-9 Removed Country Groups from Countries filter --->
				WHERE
					c.isocode is not null
				ORDER BY
					ordering, countrydescription
		</cfquery>

		<cfset popupname = "approvalPopup">
		<cfset popupparams = "height=600,width=600">

		<cfif openAsExcel eq false>
			<cfinclude template="/templates/relayFormJavaScripts.cfm">
			<cfoutput>
			<form name="FilterForm" action="" method="post" id="FilterForm">
				<cf_relayFormDisplay>
					<div id="regApprovalReportSearch" class="grey-box-top">
						<div class="row">
						<!--- CASE:427219 - RMB - two simplified individual drop downs options for status filtering
						Case 430460 WAB - changed order of filters VAB and I decided that better with country after the status filters
						SB 16/10/15 - Adding Bootstrap and making all forms consistent
						--->
							<div class="form-group">
								<div class="col-xs-12 col-sm-3 col-lg-4">
									<label for="useStatusAccountType">phr_approval_filterByAccountStatus:</label>
									<cf_relayFormElement relayFormElementType="select" id="useStatusAccountType" fieldname="useStatusAccountType" label="" currentValue="#isDefined('useStatusAccountType')?useStatusAccountType:''#" query="#getAccountFlags#" display="display" value="selectValue" onChange="">
								</div>
								<div class="col-xs-12 col-sm-3 col-lg-3">
									<label for="useStatusPerType">phr_approval_filterByPerStatus:</label>
									<cf_relayFormElement relayFormElementType="select" id="useStatusPerType" fieldname="useStatusPerType" label="" currentValue="#isDefined('useStatusPerType')?useStatusPerType:''#" query="#getperFlags#" display="display" value="selectValue" onChange="">
								</div>
								<div class="col-xs-12 col-sm-3 col-lg-3">
									<label id="countryfilter">phr_approval_ShowCountry:</label>
									<cf_SELECT id="countryfilter"  name="countryfilter" label="" selected="#isDefined('countryfilter')?countryfilter:''#" query="#FilterCountries#" display="countryDescription" value="countryID" onChange="" group="grouping" >
								</div>
								<div class="col-xs-12 col-sm-3 col-lg-2">
									<cf_relayFormElement relayFormElementType="submit" fieldname="gosubmit" class="btn btn-primary" currentValue="Search">
								</div>
							</div>
						</div>
					<cf_relayFormElement relayFormElementType="hidden" fieldname="RegApprovalPrefix" label="" currentValue="#RegApprovalPrefix#">
					<cf_relayFormElement relayFormElementType="hidden" fieldname="openAsExcel" label="" currentValue="#openAsExcel#">

					<input type="hidden" name="startRow" value="#startRow#">
					<input type="hidden" name="numRowsPerPage" value="#numRowsPerPage#">
					<input type="hidden" name="alphabeticalIndex" value="">
					</div>
				</cf_relayFormDisplay>
			</form>
			</cfoutput>
		</cfif>
	<!--- SB - 19/10/15 Moved this form element out of the tabel for bootstrap --->
	<form method="get">
		<cfinclude template="/data/persondropdown.cfm">
	</form>

<!--- Moved Cfif close down - RMB - P-RIL001 --->
<TABLE id="regApproval01" WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder table table-hover">
<cfif not openasExcel>
	<cfquery name="qAlphabetical" dbtype="query">
		SELECT DISTINCT firstLetter
		FROM getApprovals
		ORDER BY firstLetter ASC
	</cfquery>
	<cfif alphabeticalIndex is not "">
		<cfquery name="getApprovals" dbtype="query">
			SELECT *
			FROM getApprovals
			WHERE firstLetter = '#alphabeticalIndex#'
		</cfquery>
	</cfif>
	<cfif qAlphabetical.recordCount>
	<tr>
		<td colspan="5" nowrap>
			<cfoutput>
			<cfif numRowsPerPage lt getApprovals.RecordCount>#numRowsPerPage# phr_Ext_tablequery_of</cfif> #getApprovals.RecordCount# phr_Ext_tablequery_recordsfound.&nbsp;
			<cf_recordPager
				totalRecordCount="#getApprovals.recordCount#"
				recordsPerPage="#numRowsPerPage#"
				startRow="#startrow#"
				labelText="Go to page:"
				caller="#GetFileFromPath(GetCurrentTemplatePath())#"
				id="">

			<CFIF getApprovals.recordCount gt numRowsPerPage>
				&nbsp;&nbsp;&nbsp;<a href="javascript:void(document.forms['FilterForm']['startRow'].value='1');void(document.forms['FilterForm']['numRowsPerPage'].value='#getApprovals.RecordCount#');document.forms['FilterForm'].submit();">phr_Ext_tablequery_ShowAllRecords #getApprovals.RecordCount#</a>
			</cfif>

			<cfif qAlphabetical.recordCount>
				<br>
				phr_Ext_tablequery_Showrecordsbeginingwith #accountApprovalEntityType#
				<cfloop query="qAlphabetical">
					&nbsp;
					<cfif firstLetter neq alphabeticalIndex>
						<a href="javascript:document.forms['FilterForm']['alphabeticalIndex'].value='#htmleditformat(firstLetter)#';document.forms['FilterForm']['startRow'].value='1';document.forms['FilterForm']['numRowsPerPage'].value=50;document.forms['FilterForm'].submit();"> #htmleditformat(firstLetter)# </a>
					<cfelse>
						<span id="regApprovalFirstLetter">#htmleditformat(firstLetter)#</span>
					</cfif>
				</cfloop>

				<cfif alphabeticalIndex neq "">
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(document.forms['FilterForm']['alphabeticalIndex'].value='');document.forms['FilterForm']['startRow'].value='1';document.forms['FilterForm'].submit();"> * </a>
				</cfif>
			</cfif>
			</cfoutput>
		</td>
	</tr>
	</cfif>
</cfif>
<FORM  NAME="mainForm">
	<!--- 2015-09-08	ACPK	PROD2015-9 Added Country column --->
	<TR>
		<th><cfif not openasExcel><cf_input type="checkbox" id="tagAllPeople" name="tagAllPeople" onclick="tagCheckboxes('frmPersonCheck',this.checked)"></cfif></th>
		<TH><cfoutput>#accountEntityType.label#</cfoutput>/Person</TH>
		<TH>Country</TH>
		<TH>Status</TH>
		<TH>Action</TH>
	</TR>
	<CFIF getApprovals.recordCount gt 0>
		<!-- StartDoNotTranslate -->
		<CFOUTPUT QUERY="getApprovals" GROUP="accountName" startrow="#startrow#" maxrows="#numRowsPerPage#">
			<TR class="evenrow" VALIGN="bottom">
				<TD HEIGHT="36" colspan=2><cfif not openasExcel><B>
					<A TITLE="Goto #accountApprovalEntityType# Record" HREF="javascript:void(openNewTab('Account Details #accountID#','#accountName#','/data/dataframe.cfm?frmsrch#accountApprovalEntityType eq "location"?"location":"org"#ID=#accountId#',''))">
					#htmleditformat(accountName)#</A></B><cfelse>#htmleditformat(accountName)#</cfif></TD>
				<TD>#htmleditformat(CountryDescription)#</TD>
 				<TD><B>#htmleditformat(accountStatusName)#</B> #dateformat(accountStatusDate,"dd-mmm-yyy")#</TD>
				<CFIF checkPermission.edit GT 0>
				<TD>
					<cfif not openasExcel>
						<cfif getRegProcess.hasEditRights is 1>
							<cfif accountStatus eq "applied">
								<A TITLE="Approve this #accountEntityType.label#" HREF="javascript:void(openWin('/approvals/approvalProcess.cfm?frm#accountEntityType.uniqueKey#=#accountID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF accountStatusFilter NEQ 0>&accountStatusFilter=#htmleditformat(accountStatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#,scrollbars=1'));" >Approve #accountEntityType.label#</a>
							<CFELSE>
								<A  TITLE="Change the Registration Status of this #accountApprovalEntityType#" HREF="javascript:void(newWin=openWin('/approvals/approvalProcess.cfm?frm#accountApprovalEntityType#ID=#accountID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF accountStatusFilter NEQ 0>&accountStatusFilter=#htmleditformat(accountStatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#,scrollbars=1'));" >Change</a>
							</cfif>
						</cfif>
					</cfif>
				</TD>
				<CFELSE>
				<TD>N/A</TD>
				</CFIF>
			</TR>
			<CFSET nCount=1>
			<CFOUTPUT>
			<CFIF nCount MOD 2 IS NOT 0><cfset useclass="oddrow"><CFELSE><cfset useclass="evenRow"></CFIF>
			<TR class="#useclass#">
				<td><cfif not openasExcel><CF_INPUT TYPE="CHECKBOX" NAME="frmPersonCheck" VALUE="#PersonID#"><cfelse>&nbsp;</cfif></td>
				<TD><cfif not openasExcel><A TITLE="Goto Person Record" HREF="javascript:void(viewPerson(#personID#,'PersonDetail',{openInOwnTab:true}))">#htmleditformat(fullname)#</A><cfelse>#htmleditformat(fullname)#</cfif></TD>
				<TD></TD>
				<TD> #htmleditformat(PersonStatusName)# #dateformat(PersonStatusdate,"dd-mmm-yyy")#</TD>
				<CFIF checkPermission.edit GT 0>
				<TD>
					<cfif not openasExcel>
						<cfif getRegProcess.hasEditRights is 1>
							<cfif listFindNoCase("active,approved,partlyApproved,applied",accountStatus)>  <!--- WAB 30/05/07 Tricia asked for applied to be added for Trend.  Seems as if trend had some different code which disappeared during code synching --->
								<cfif PersonStatus eq "applied" or PersonStatus eq "pending">
								 	<A TITLE="Approve this Person" HREF="javascript:void(newWin=openWin('/approvals/approvalProcess.cfm?frmPersonID=#personID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF accountStatusFilter NEQ 0>&accountStatusFilter=#htmleditformat(accountStatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#,scrollbars=1'));">Approve Person</a>
								<CFELSE>
									<A TITLE="Change the Registration Status of this Person" HREF="javascript:void(newWin=openWin('/approvals/approvalProcess.cfm?frmPersonID=#personID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF accountStatusFilter NEQ 0>&accountStatusFilter=#htmleditformat(accountStatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#,scrollbars=1'));">Change</a>
								</cfif>
							</cfif>
						</cfif>
					</cfif>
				<CFELSE>
				<TD>N/A</TD>
				</CFIF>
			</TR>
			<CFSET nCount=nCount+1>
			</CFOUTPUT>
		</CFOUTPUT>
		<!-- EndDoNotTranslate -->
	<CFELSE>
		<!--- 2011/11/28 NYB LHID8224 replaced "No records Found" with phr_sys_noRecordsFound --->
		<TR><TD colspan="4">phr_sys_noRecordsFound</TD></TR>
	</CFIF>
</TABLE>
</FORM></cfif>
<!--- Moved Cfif close down - RMB - P-RIL001 ---><!--- Core Registration Approval - END --->