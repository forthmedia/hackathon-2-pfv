<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	File name:			RegApprovals-Org.cfm
	Author:				NYF
	Date started:		2008/05/27

	DESCRIPTION:
	Template runs Organisation Approval processes that aren't possible to be done via a Workflow/Process
	checks status of the organisation, if org is Applied, Rejected or Approved will set
	all employees of this org to the same status
	and if Reject or Approved, will also send a relevant email to the employee
	advising them of their new status

	BEHAVIOURS:
	* Creates Username and Password for a user set to Approved
	* Sends Approved & Rejected emails out to those users given this status

	OPTIONS:
	* Uses notifyChange to turn off all screen messages - allowing it to be used for manageColleagues.cfm

	AMENDMENT HISTORY:

	2016/09/05	NJH	JIRA PROD2016-1313 - change org approval to support both loc and/or organisation approval. Have called them 'accounts'

--->

<cfparam name="RegPrefix" default="">

<cfset accountApprovalEntityType = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"location":"organisation">
<cfset accountPrefix = left(accountApprovalEntityType,3)>
<cfset accountApprovalFlagPrefix = regPrefix&accountPrefix>
<cfset accountID = 0>

<cfif accountApprovalEntityType eq "organisation">
	<cfif structKeyExists(form,"frmOrganisationId")>
		<cfset accountID = form.frmOrganisationId>
	</cfif>
<cfelse>
	<cfif structKeyExists(form,"frmLocationID")>
		<cfset accountID = form.frmLocationID>
	</cfif>
</cfif>

<cfset accountApprovalFlagGroupTextID = "#accountApprovalFlagPrefix#ApprovalStatus">
<cfset PerApprovalFlagGroupTextID = "#RegPrefix#PerApprovalStatus">

<!--- Get Org Approval Status --->
<cfset GetAccountApprovalStatus = application.com.flag.getFlagGroupData (flagGroupId = accountApprovalFlagGroupTextID, entityID = accountID, additionalColumns="DATEDIFF(mi, fd.lastupdated, getDate()) as LastUpdatedDiff, e.countryid")>
<cfset accountStatusFlagTextID = GetAccountApprovalStatus.flagTextID>
<cfset accountApprovalStatusName = GetAccountApprovalStatus.flagname>
<cfset accountApprovalStatusUpdated = GetAccountApprovalStatus.LastUpdatedDiff>
<cfset primaryCountryID = GetAccountApprovalStatus.countryid>

<cfif accountApprovalStatusUpdated lt 1>
	<cfset session.accountApprovalMsg = "Phr_AccountApprovalStatusSetTo  #accountApprovalStatusName#">

	<!--- Get users in org without a status of Rejected (haven't found an existing function that can get person details based on OrgID) --->
	<cfif (accountStatusFlagTextID eq "#accountApprovalFlagPrefix#Applied") or (accountStatusFlagTextID eq "#accountPrefix#Rejected")>
		<CFQUERY name="GetPeopleToSet" datasource="#application.siteDataSource#">
			select p.personid, p.firstname + ' '+ p.lastname as Name, p.email as Email,
			f.FlagTextID as PerStatusFlagTextID
			from flag f
			inner join flaggroup fg on f.flaggroupid=fg.flaggroupid
			join booleanflagdata b on b.flagid = f.flagid
			join person p on p.personid = b.entityid
			where fg.flaggrouptextid =  <cf_queryparam value="#RegPrefix#PerApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" >
			and f.FlagTextId! =  <cf_queryparam value="#RegPrefix#PerRejected" CFSQLTYPE="CF_SQL_VARCHAR" >
			and f.FlagTextId  not like  <cf_queryparam value="%#Replace(accountStatusFlagTextID,"#accountApprovalFlagPrefix#","","one")#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and p.#accountApprovalEntityType#id =  <cf_queryparam value="#accountID#" CFSQLTYPE="CF_SQL_INTEGER" > ;
		</CFQUERY>

		<cfset accountStatus = replaceNoCase(accountStatusFlagTextID,accountApprovalFlagPrefix,"")>
		<cfif listFindNoCase("applied,rejected",accountStatus)>
			<cfif GetPeopleToSet.recordcount gt 0>
				<!--- Update people to be Applied or rejected, based on account (loc/org) approval status --->
				<cfset message = accountStatus eq "applied"?"Phr_appliedStatusSetFor":"Phr_RejectedEmailSentTo">
				<cfset session.accountApprovalMsg = "#session.accountApprovalMsg#<br/><br/>#message#<br>">

				<cfloop query="GetPeopleToSet">
		 			<cfset application.com.flag.setbooleanflag(entityid=personid, flagtextid="#RegPrefix#Per#accountStatus#", deleteOtherRadiosInGroup="true")>
					<cfset session.OrgApprovalMsg = "#session.accountApprovalMsg#&nbsp; &nbsp; &nbsp; #name#  (#email#) <br>">
					<cfif accountStatus eq "rejected">
						<cfset application.com.email.sendEmail(emailtextid = "PerRejectedEmail",personid = PersonID, countryid = primaryCountryID)>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>
	</cfif>

	<!---

	<!--- Check if org/location has been set to Applied --->
	<cfif accountStatusFlagTextID eq "#RegPrefix##accountPrefix#Applied">
		<cfif GetPeopleToSet.recordcount gt 0>
			<!--- Update people to be Applied --->
			<cfset session.accountApprovalMsg = "#session.accountApprovalMsg#<br/><br/>Phr_AppliedStatusSetFor<br>">

			<cfloop query="GetPeopleToSet">
	 			<cfset application.com.flag.setbooleanflag(entityid=personid, flagtextid="#RegPrefix#PerApplied", deleteOtherRadiosInGroup="true")>
				<cfset session.OrgApprovalMsg = "#session.accountApprovalMsg#&nbsp; &nbsp; &nbsp; #name#  (#email#) <br>">
			</cfloop>
		</cfif>

	<!--- Check if org/location has been set to Rejected --->
	<cfelseif accountStatusFlagTextID eq "#RegPrefix##accountPrefix#Rejected">
		<cfif GetPeopleToSet.recordcount gt 0>
			<cfset EmailTextID = "PerRejectedEmail">
			<!--- Update people to be Rejected --->
			<cfset session.accountAprovalMsg = "#session.accountApprovalMsg#<br/><br/>Phr_RejectedEmailSentTo <br>">
			<cfloop query="GetPeopleToSet">
	 			<cfset application.com.flag.setbooleanflag(entityid=personid, flagtextid="#RegPrefix#PerRejected", deleteOtherRadiosInGroup="true")>
				<cfset application.com.email.sendEmail(emailtextid = emailTextID,personid = PersonID, countryid = primaryCountryID)>
				<cfset session.accountAprovalMsg = "#session.accountApprovalMsg#&nbsp; &nbsp; &nbsp; #name#  (#email#) <br>">
			</cfloop>
		</cfif>
	</cfif>
 --->

<cfelse> <!--- OrgApprovalStatusUpdated NOT lt "1" --->
	<cfset session.accountApprovalMsg = "Phr_accountApprovalStatusIs  #accountApprovalStatusName#">
</cfif> <!--- OrgApprovalStatusUpdated lt "1" --->

<cfset form.accountApprovalStatus = accountApprovalStatusName>

<cfset session.accountApprovalMsg = "#session.accountApprovalMsg#<br/><br/>Phr_ClickHereToCloseWindow">

<cfset session.OrgApprovalMsg = session.accountApprovalMsg>