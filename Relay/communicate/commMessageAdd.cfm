<!--- �Relayware. All Rights Reserved 2014 --->

<cfset communication = application.com.communications.getCommunication(commId=frmCommID)>
<cfset filePath = "\linkImages\commMessage\#form.frmCommID#">

<cf_transaction>
	<cfif form.frmMessageImage neq "">
		<cfset sourceFile = "frmMessageImage">
	<cfelse>
		<cfset sourceFile = form.frmMessageImage_orig>
	</cfif>

	<cfif sourceFile neq form.frmMessageImage_orig>
		<cfset uploadedFileResult = application.com.relayImage.CreateThumbnail(targetSize=80,sourceFile=sourceFile,targetFilePath=filePath,targetFileName="thumb_#form.frmCommID#")>
		
		<cfif uploadedFileResult.isOK>
			<cfset form.frmMessageImage = uploadedFileResult.imageURL>
		<cfelse>
			<cfif sourceFile eq "frmMessageImage_orig">
				<cfset form.frmMessageImage = form.frmMessageImage_orig>
			</cfif>
			
			<!--- if we had a problem uploading the file, then redirect back to the message tab --->
			<cfset structDelete(form,"frmCallingTemplate")>
			<cfset frmNextPage = "Message">
			<cfset message = uploadedFileResult.message>
		</cfif>
	<cfelse>
		<cfset form.frmMessageImage = form.frmMessageImage_orig>
	</cfif>
	
	<cfif form.frmMessageText eq form.frmMessageText_default>
		<cfset form.frmMessageText = "">
	</cfif>
	
	<!--- shorten any url that is in the text --->
	<cfif form.frmMessageText neq "">
		<cfset form.frmMessageText = application.com.social.shortenUrlInText(text=form.frmMessageText)>
	</cfif>
	
	<cfset sendMessage=0>
	<cfif not structKeyExists(form,"frmDoNotSendMessage")>
		<cfset sendMessage=1>
	</cfif>

	<cfset communication.SaveCommMessage(text=form.frmMessageText,title=form.frmMessageTitle,url="/communicate/viewCommunication.cfm?commID=#frmCommID#",image = form.frmMessageImage,sendMessage = sendMessage)>
<cf_transaction>


