<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2011/06/22 LID4471 take out reference to vCommdetailMostSignificantStatus
WAB 2011/09/19 LID 7613 Implemented a temporary table to speed things up
WAB 2013-01		
2013-04-02 PPB Case 432706 ensure there is a space on the end of SQL lines

 --->
	

<CFPARAM name="frmcommformlid" default="2,3">


<CFINCLUDE template = "/templates/qryGetCountries.cfm">

<!--- WAb 2008/07/02 added these cfparams because not working from commstatus count --->
<cfparam name="URL.statusView" default = "mostSignificant">
<cfparam name="URL.frmCountryID" default = "">

<cfset variables.statusView = URL.statusView>

<!--- WAB 2007-07-02 made a change so that url.status carried two bits of information separated by a -, need to extract the correct values--->

<cfif isDefined("URL.Status")>
<!---  LHID 2458 SSS 2009/08/06 changed this so that if a comm stops the not sent report will display people with blank feedback - Updated for version control ---> 
	<CFQUERY name="GetStatusdescription" datasource="#application.sitedatasource#">
	SELECT StatsReport
	FROM commdetailstatus
	Where commstatusid  IN ( <cf_queryparam value="#URL.Status#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	<cfif URL.Status EQ 'not sent'>
		or commstatusid = ''
	</cfif>
	</CFQUERY>

<cfelseif isDefined("url.statusGroup")>

	<cfset GetStatusdescription = structNew()>
	<cfset GetStatusdescription.StatsReport = statusGroup>

</cfif>



<cfset filter = application.com.communications.getCommunicationFilter(countryid=frmcountryid, filtercode = filtercode)>


<!--- this query brings back a list of ids which can then be used to either populate a selection, make a report or resend--->
<!--- WAB 2011/06/22 LID4471 take out reference to vCommdetailMostSignificantStatus  (changed way SKP had suggested) --->	
				<CFsaveContent variable = "frmPersonIDsSQL"> <!--- WAB changed to savecontent so that we could put a cfif in the middle--->
					<cfoutput>
					SELECT 
						cd.PersonID 
						FROM 
							<cfif variables.statusView is "mostSignificant">
							commdetail cd <!--- 2013-04-02 PPB Case 432706 ensure there is a space on the end of this line --->								
							<cfelse>
							CommDetailHistory cd <!--- 2013-04-02 PPB Case 432706 ensure there is a space on the end of this line --->									
							</cfif>
							<cfif isDefined("url.StatusGroup")><!--- STCR 2011-08-24 LID4471 This should fix statusGroup for statusGroup resend, but may increase timeout risk, so only join when needed below --->
							left join CommDetailStatus cds on cd.mostsignificantCommStatusID = cds.commStatusID <!--- 2013-04-02 PPB Case 432706 ensure there is a space on the end of this line --->	
							</cfif>
							#filter.country# <!--- 2013-04-02 PPB Case 432706 ensure there is a space on the end of this line --->	
						WHERE cd.CommID =  <cf_queryparam value="#url.frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  
						<cfif isDefined("URL.Status")>
							and 
							<cfif variables.statusView is "mostSignificant">
							cd.mostsignificantcommstatusid
							<cfelse>
							cd.commstatusid
							</cfif>								
							<CFIF URL.Status IS NOT ""><cfif ListLen(URL.Status,',') gt 1>in (#URL.Status#)<cfelse>=#URL.Status#</cfif> <CFELSE> IS NULL</CFIF>  <!--- NJH 2007/04/02 added cfif since breaking if URL.Status was blank ---><!--- STCR 2011-08-25 For performance, if we know there is only one status then use '=' instead of 'IN' --->
						<cfelseif isDefined("url.StatusGroup")>
							and cds.statusGroup =  <cf_queryparam value="#url.statusGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  <!--- STCR 2011-08-24 LID4471 Need to get statusGroup from join, now that WAB is not using the view --->
						<cfelse>
							and <cfif variables.statusView is "mostSignificant">cd.mostsignificantcommstatusid<cfelse>cd.commstatusid</cfif> is null
						</cfif>						
						
	
						and cd.commTypeid  in ( <cf_queryparam value="#frmcommformlid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						and #filter.TestInternalExternal#
						</cfoutput>
					</CFSAVECONTENT>
<!--- END LID4471 --->


<CFIF isdefined("selectionTask")>

						


	<CFSET frmtask= selectionTask>

	<!--- I'm submitting a form because of problems I encounter being two directories down. The next page doesn't really know where it is!!--->
	<CFOUTPUT>
	
	<FORM name="thisForm" method="post" action = "../selection/selectalter.cfm">
	<cf_encrypthiddenfields>
	<CF_INPUT type="HIDDEN" name="frmpersonids"  value="#frmpersonidsSQL#">
	<CF_INPUT type="HIDDEN" name="frmtask"  value="#frmtask#">
	<CF_INPUT type="HIDDEN" name="frmruninpopup"  value="#frmruninpopup#">
	</cf_encrypthiddenfields>	
	</form>
	
	</cfoutput>
	<SCRIPT>
		window.document.thisForm.submit()
	</script>
	<CF_ABORT>


</cfif>


<!--- 
	WAB 2013-01  moved the resend section above the commResultsByStatus query
					which is not needed for the resend and just slows it down
--->
<CFIF isdefined("resend")>

	<CFSET frmPersonID = frmPersonIDsSQL>
	<CFSET frmCommFormLID = frmCommFormLID>
	<CFINCLUDE template="resendCommItems.cfm">
<!--- 	<CFINCLUDE template="comm CheckDetails.cfm"> --->
	<CF_ABORT>
</cfif>



<CFQUERY name="CommResultsByStatus" datasource="#application.sitedatasource#">
	declare @tempPersonIDs table  (personid int)
	
	insert into @tempPersonIDs
	#preserveSingleQuotes(frmPersonIDsSQL)#


	SELECT 	cd.CommID,
			cd.CommTypeID,
			cd.PersonID,
			p.LocationID,
			cd.commStatusID,
			cd.Feedback,
			cd.CommDetailID,
			p.FirstName,
			p.LastName,
			p.Email,
	    	p.OfficePhone,
			p.organisationID,
			l.SiteName,
			l.CountryID,
			l.telephone, 
			l.Fax
	FROM 

		Commdetail cd
			inner join
		@tempPersonIDs  temp on  temp.personid = cd.personid
			inner join
		Person p on cd.PersonID = p.PersonID
			inner join 
		Location l	ON l.LocationID = p.LocationID

		WHERE 
		cd.CommID =  <cf_queryparam value="#url.frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			and cd.commTypeid  in ( <cf_queryparam value="#frmcommformlid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and #filter.TestInternalExternal#
			
	GROUP BY cd.CommDetailID,cd.CommID, cd.CommTypeID, 
		cd.PersonID, p.LocationID, cd.commStatusID, 
		cd.Feedback, p.FirstName, p.LastName, p.Email, 
		p.OfficePhone, p.organisationID, l.SiteName, l.CountryID, l.Fax, 
		l.telephone, cd.test, cd.internal
	order by l.sitename, p.lastname
</CFQUERY>



<cf_head>
<SCRIPT>

	function editCommDetail(cid) {
		var form = document.mainForm;
		form.frmCommDetailID.value = cid;
		form.submit();
	
	}

</script>


</cf_head>
   
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<CFOUTPUT>
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">Phr_Sys_Communication #htmleditformat(URL.frmCommID)# </TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="javascript:history.go(-1);" Class="Submenu">Phr_Sys_Back</A>&nbsp;&nbsp;
		</TD>		
	</TR>
</CFOUTPUT>
</TABLE>

<cfoutput>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD COLSPAN="5">Phr_Sys_ListOf #htmleditformat(getstatusdescription.statsreport)#
						<BR>#htmleditformat(filter.Message)#
		</TD>
	</TR>

<TR>

	<Th>Phr_Sys_IDs</Th>
	<Th>Phr_Sys_Recipient</Th>
	<Th>Phr_Sys_FaxEmailPhone</Th>
	<Th>Phr_Sys_Feedback</Th>

</TR>
</cfoutput>
<!--- WAB 2006-05-03 took these phrases out to get as variables.  In a very big report cf_translate can't handle translating all these phrases on each line --->
<cf_translate phrases = "sys_fax,sys_email,sys_DirectLine,sys_Switchboard,Sys_Pers,Sys_org,Sys_ClickThruDetails"/>

<CFOUTPUT query="CommResultsByStatus">
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD>#Phr_Sys_Pers#: #htmleditformat(PersonID)#  <BR> #Phr_Sys_Org#:  #htmleditformat(organisationID)#  </TD>
		<TD>#htmleditformat(FirstName)# #htmleditformat(LastName)#<BR>  #htmleditformat(SiteName)#</TD>
		<TD>#Phr_Sys_Fax#: #htmleditformat(Fax)#<BR>
		    #Phr_Sys_Email#:#htmleditformat(Email)#<BR>
			#Phr_Sys_DirectLine#: #htmleditformat(OfficePhone)#<BR>
			#Phr_Sys_Switchboard#: #htmleditformat(telephone)#
		</TD>
		<CFIF feedback IS "Click Thru">
			<TD><A href="/report/EntityUsageList.cfm?USERTOVIEW=#personid#" TITLE="#Phr_Sys_ClickThruDetails#">#htmleditformat(Feedback)#</A></TD>
		<CFELSE>
		<TD>#htmleditformat(Feedback)#</TD>
		</CFIF>
		
	</TR>
</CFOUTPUT>

</TABLE>


<!--- 
WAb 2008/06/16 no sign of this form being used
<FORM METHOD="POST" ACTION="../../phoning/CommHistory_DetailEdit.cfm" NAME="mainForm">
	<INPUT TYPE="Hidden" NAME="frmCommDetailID">
	<CFOUTPUT><INPUT TYPE="Hidden" NAME="frmNext" value="#SCRIPT_NAME#?frCommid=#URL.frmCommID#&status=#URL.Status#"></CFOUTPUT>
</form>
 --->


