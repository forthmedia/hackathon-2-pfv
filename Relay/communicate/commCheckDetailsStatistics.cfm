<!--- �Relayware. All Rights Reserved 2014 --->
<!---

commcheckdetailsstatistics.cfm


file included in commcheckdetails.cfm

displays results statistics in a table

passed #frmcommid#   the commid


WAB 2005-11-17  cfoutputonly must be true - some items, including bits of the form were not showing
WAB 2009/07/08 added selection options to the click through drop down
2011-07-04	NYB		P-LEN024
2011-12-16 	NYB		LHID8095 added resizeIframe js to page
2012-01-24 	NYB		Case#424774 various changes
2012-01-31 	NYB		Case#424774 added tabHeightSet = false; to ajax call
2012-10-04	WAB		Case 430963 Remove Excel Header
2013-01		WAB		Converted to use communication.cfc object
2013-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-05		WAB		Convert so can be loaded by ajax
2013-10		WAB		During CASE 437315 - alteration to way "comm sending" message is displayed
2016-01-29	SB		Added classes to table for Bootstrap
 --->

<cfparam name="frmCommID" default="0">
<CFPARAM name="stepName" default="">

<cfset communication = application.com.communications.getCommunication (commid = listlast(frmcommid),personid = request.relaycurrentuser.personID)>

<cfif not isDefined("filtercode")>
	<cfif communication.hasBeensentExternally() is 1>
		<cfset filtercode = "E">
	<cfelseif communication.hasBeensentInternally()is 1>
		<cfset filtercode = "I">
	<cfelse>
		<cfset filtercode = "T">
	</cfif>
</cfif>

<cfif communication.countryidList is not "">
	<cfquery name="countries" datasource = "#application.sitedatasource#">
	select countrydescription as displayvalue, countryid as datavalue from country
	where countryid  in ( <cf_queryparam value="#communication.countryidList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and isocode <> ''
	order by countrydescription
	</cfquery>
</cfif>

<cf_includejavascriptOnce template ="/javascript/contextMenu.js" >
<cf_includeJavascriptOnce template="/javascript/openwin.js">


<cf_includeJavascriptOnce template = "/javascript/prototypeSpinner.js">
<cf_includeJavascriptOnce template = "/communicate/communication.js">




	<!--- WAB 2009/07/08 added selection option to the click throughs --->

	<xml id="contextDef" style="visibility:hidden;">
		<xmldata>
			<contextmenu id="commEmailStats">
				<item id="savePeople" value="Save People as Selection" ></item>
				<item id="addPeople" value="Add People to Selection" ></item>
				<item id="removePeople" value="Remove People from Selection"  ></item>
				<item id="report" value="Report" ></item>
				<CFIF request.isAdmin or communication.isOwner>
				<item id="resend" value="Resend" ></item>
				</cfif>
			</contextmenu>
			<contextmenu id="commResultsAll">
				<item id="report" value="Report" ></item>
				<item id="download" value="Download" ></item>
			</contextmenu>
			<contextmenu id="commClickThroughs">
				<item id="report" value="Report" ></item>
				<item id="savePeople" value="Save People as Selection" ></item>
				<item id="addPeople" value="Add People to Selection" ></item>
				<item id="removePeople" value="Remove People from Selection"  ></item>
			</contextmenu>


		</xmldata>
	</xml>

	<!--- 	moved to commcheckdetail, since also used by selection section <div status="false" onclick="javascript:fnDetermine(event);"  onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);"  id="oContextMenu"  class="menu"></div> --->
	<LINK REL="stylesheet" HREF="/Styles/contextMenuStyles.css">
	<script>
		fnInit()
		$(document.body).observe('click',fnDetermine)
	</script>

	<div status="false" onclick="fnDetermine(event);"  onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);"  id="oContextMenu"  class="menu"></div>

	<cfoutput>

	<cf_includejavascriptonce template="/javascript/popupDescription.js">

	<!---  code for doing the context sensitive menu in the statistics
		emailStats and click throughs should be able to use same code.  Note that commClickThruDetails.cfm has so far only been coded to genrate a report, but could be coded to do selections as well

	--->

	<script>

	var tabHeight = 0;

	fnFireContext_commEmailStats = function(oMenu,oClickedItem) {

		pageString = iDomainAndRoot + '<cfoutput>#jsStringFormat(thisdir)#</cfoutput>/commResultsbyStatus.cfm?frmruninpopup=true';
		fnFireContext_commEmailStatsAndClickThroughs(oMenu,oClickedItem,pageString)

	}

	fnFireContext_commClickThroughs = function(oMenu,oClickedItem) {
		pageString = iDomainAndRoot + '<cfoutput>#jsStringFormat(thisdir)#</cfoutput>/commClickThruDetails.cfm?frmruninpopup=true';
		fnFireContext_commEmailStatsAndClickThroughs(oMenu,oClickedItem,pageString)
	}

	fnFireContext_commEmailStatsAndClickThroughs = function(oMenu,oClickedItem,pageString) {
		// Customize this function based on your context menu
		urlVariableString = '&' + oClickedItem.id

	//	pageString = iDomainAndRoot + '/communicate/commResultsbyStatus.cfm?frmruninpopup=true';

		// alert (pid)
		switch (oMenu.getAttribute("menuid")) {
			case "savePeople":
				url =  pageString + '&SelectionTask=save' + urlVariableString
				openWin( url ,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;

			case "addPeople":
				url =  pageString + '&SelectionTask=add' + urlVariableString
				openWin( url ,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;

			case "removePeople":
				url =  pageString + '&SelectionTask=remove' + urlVariableString
				openWin( url ,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;

			case "report":
				url =  pageString +  urlVariableString
				openWin( url ,'PopUp','width=800,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;

			case "resend":
				url =  pageString + '&resend=1' + urlVariableString
				// ought to have commformlid in here, but since we only send emails these days, should be OK
				openWin( url ,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;
		}
	}


	fnFireContext_commResultsAll = function(oMenu,oClickedItem) {
		// Customize this function based on your context menu
		urlVariableString = '&' + oClickedItem.id
		// alert (pid)
		switch (oMenu.getAttribute("menuid")) {
			case "report":
				pageString = iDomainAndRoot + '<cfoutput>#jsStringFormat(thisdir)#</cfoutput>/commResultsAll.cfm?frmruninpopup=true';
				url =  pageString + '&' + urlVariableString
				openWin( url ,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;

			case "download":
				// NJH 2008/07/09 Bug Fix T-10 Issue 682 - Changed commResultsAllDownload.cfm to commResultsAll.cfm (which uses TFQO open in excel
				pageString = iDomainAndRoot + '<cfoutput>#jsStringFormat(thisdir)#</cfoutput>/commResultsAll.cfm?frmruninpopup=true&openAsExcel=true';
				url =  pageString + '&' + urlVariableString
				openWin( url ,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
				break ;
		}
	}

	loadEmailStats = function(refresh) {
		page = '/webservices/callWebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&methodname=loadStatisticsTable&returnFormat=plain&_cf_nodebug=true'
		parameters = Form.serialize(document.statsFilter) + '&refresh=' + refresh
		document.getElementById('emailStatsStatusDiv').innerHTML = '<img src="/images/icons/loading.gif"> Loading Statistics ';

		var myAjax = new Ajax.Updater(
			'emailStatsDiv',
			page,
			{
				method: 'post',
				parameters: parameters,
				evalScripts: true,
				debug: false,
			});
			return
	}
	</script>


		<cfset checkIfCommunicationIsBeingSent = communication.checkIfCommunicationIsBeingSent()>

		<cfif checkIfCommunicationIsBeingSent.isBeingSent>
			<div class="commResultsDiv">
				<script>
					commID_enc = '#application.com.security.encryptVariableValue("commid",frmCommID)#'
					messageDiv = createCommMessageDiv (null,true,'This communication is being sent at the moment',true)
					updateCommStatus (commID_enc ,#checkIfCommunicationIsBeingSent.metadata.commSendResultID#,messageDiv)</script>
				</script>
			</div>
		</cfif>


	<cfparam name = "refreshCommStats" default = "false">
	<CFPARAM name="filtercode" >
	<cfparam name="frmCountryID" default = "">
	<!--- 2011-12-16 NYB LHID8095 gave the table an id and changed it to wrap around the whole contents --->
	<table id="commCheckDetailsStatisticsWrapperTable" class="table table-hover">
		<tr><th>Phr_Sys_GeneralOverviewofThisCommunication</th></tr>
		<tr><td>
			<form name="statsFilter" id="statsFilter" style="margin=0px;">
				<CF_INPUT type=hidden name="frmCommID" value = "#frmCommID#">
				<table class="table table-hover">
					<tr><td>These statistics show items sent </td>
					<td><select class="form-control" name = filterCode onchange = "javascript:loadEmailStats(false)" isdirtyfunction="return false">
						<Option value="I" <cfif filtercode is "I">SELECTED</cfif>>Internally
						<Option value="E"  <cfif filtercode is "E">SELECTED</cfif>>Externally
						<Option value="T"  <cfif filtercode is "T">SELECTED</cfif>>As Tests
					</select></td></tr>
					<cfif listlen(communication.countryidList) gt 1 and (filtercode contains "E" or frmCountryID is not "" ) >
						<tr><td align = right>to </td><td>
						<cf_displayValidValues
							validvalues = "#countries#"
							nulltext = "All countries"
							formfieldname = "frmCountryID"
							currentvalue = "#frmCountryID#"
							passthrough = 'onchange = "javascript:loadEmailStats(false)"'>
						</td>
						</tr>
					</cfif>

				</table>
			</form>
			<div id="emailStatsStatusDiv"></div>
			<div id="emailStatsDiv"></div>
		</td></tr>
	</table>
	<script>
	  loadEmailStats(false)
	</script>
	</cfoutput>