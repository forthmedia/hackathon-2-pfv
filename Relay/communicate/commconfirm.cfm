<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->

<!---
		2001-11-14 SWJ	Modified the layout
		2001-10-28	CPS		Changes following new attributes on commdetail record, namely commtypeid, commreasonid and commstatusid
		1999-01-25	WAB		Added code to allow sending to a list of people defined in frmpersonid
							and to run in a popup window
		2000-07-24	WAB		Made some mods to allow selections to be added to an existing selection
		2002-11-26	SWJ		Added a method for sending immediately see line 340
		2006-01-12		WAB		modified year drop down to have the current year (had stopped at 2005!)
		2008/03/03	WAb		processing on next page of date from date drop down did not work with translated months (which MonthAsString gives you).  Therefore changed to entirely numeric date and reversed to put year first
		2011-07-04	NYB		P-LEN024
		2012-01-13	NYB 	Case#425115 fixed bug I noticed fixing this one - clicking Approve then Put On Hold was throwing the layout outside the tabbed interface - a couple of lines to checkFormcommconfirm function fixed this
		2012-02-01 	NYB 	Case 425248: added hasRightsToSend and hasRightsToSchedule to getCommunication and used these in cfif instead
		2012/02/28	RMB		CASE:426666 - Added variables to reload this page when sending a test gatekeeper email
		2012-11-15 	WAB 	CASE:431825 - Problem passing long message via URL. Resorted to using a session variable
							Also removed sanitizeHTML which was removing javascript required to open/close div, and renamed message variable to message_html
		2012-11-26	IH		Case 427937 adjust script to send out communication to selected people
		2012-11-26	IH		Case 427937 Add cfparam for frmResend
		2012-12-05 	WAB 	CASE:432460  change of 2012-11-15 had left one message variable not renamed to message_html
		2013-01		WAB		Sprint 15&42 Comms. 	Converted to use communication.cfc object
							Added asynchronous sending via Ajax
		2013-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
		2013-05		WAB		Convert so can be loaded by ajax
		2013-06-19	WAB		After comm has been sent externally, remove send 'TEST' Email
		2013/09/26	NJH		Case 437046 - don't allow the communication to be sent if the owner has been deleted
		2013-10		WAB		During CASE 437315 - alteration to way "comm sending" message is displayed
		2013-12-17	WAB	 	CASE 438411 Send test comm to gatekeeper was not working (function being called no longer existed).
		2014-12-02	DXC		Case 442498 - Added href="#" to <a> so it has cursor:pointer
		2016-01-29	SB		Removed tables
--->



<cfparam name="frmSelectID" default="">
<cfparam name="FrmRunInPopup" default="false">
<CFPARAM name="stepName" default="6">
<CFPARAM name="frmpersonids" default="">
<CFPARAM name="useWrapper" default="true">
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">
<CFPARAM name="messageStruct" default="#StructNew()#">
<CFPARAM name="frmNextPage" default="commdetailsHome.cfm">
<CFPARAM name="ParentPage" default="commDetailsHome.cfm">
<cfparam name="action" default="">
<cfparam name="frmResend" default="false">

<!--- 2012-11-15 	WAB 	CASE:431825 - Problem passing long message via URL. Resorted to using a session variable --->
<cfif isDefined("messageStructID") and structKeyExists(session,"messageStructs") and structKeyExists(session.messageStructs,messageStructID)>
	<cfset messageStruct = session.messageStructs[messageStructID]>
	<cfset structDelete (session.messageStructs,messageStructID)>
</cfif>

<cfif not isdefined("communication")>
	<cfset communication = application.com.communications.getCommunication (commid = #frmcommid#,personid = request.relayCurrentUser.personid)>
</cfif>

<cfif communication.creatorID neq "">

	<cfif not communication.isContentOK()>
		<cfset validateMergedContent = communication.prepareAndValidateContent()>

		<cfif not validateMergedContent.isOK>
			<cfoutput>
				<div class="errorblock">
				Communication can not be sent <BR>
				#communication.FormatAllContentErrorArrays_HTML(validateMergedContent)#
				</div>
			</cfoutput>
			<cf_abort>
		</cfif>

	</cfif>

	<!--- While a comm can still be edited we are in 'Test' mode (for sending to self and to a friend) --->
	<cfset testMode = iif (communication.hasBeenSentLive(),de('false'),de('true'))>


	<!--- variable for SendToSelf & SendToFriend --->


		<cfset CommSelectionDetails = communication.getSelectionDetails()>

	<cfset formAction = "#thisDir#/commsend.cfm?RequestTimeout=500&frmHideTopHead=true">
	<cfset useApprovers = application.com.settings.getSetting ("communications.approvals.UseApprovers")>
	<cfset useApprovalProcess = application.com.settings.getSetting ("communications.approvals.useApprovalProcess") >
	<cfset sendImmediatelyThreshold = application.com.settings.getSetting ("communications.limits.sendImmediatelyThreshold") >
	<cfset useGatekeeperProcess = iif(application.com.settings.getSetting ("communications.approvals.ApprovalProcessType") eq "Gatekeeper",true,false)>




		<cfset approvalDetails = communication.getApprovalDetails()>

		<cfif approvalDetails.requiresApproval and approvalDetails.ApprovalProcessType is "Gatekeeper" and approvalDetails.GATEKEEPERQUERY.recordcount eq 0>
			<cfset messageStruct.message_html = "phr_sys_Comm_CannotSendForApproval_NoGatekeepersAssigned">
			<cfset messageStruct.type = "error">
		</cfif>


	<cfif useApprovalProcess>
		<cfif useGatekeeperProcess>
			<cfset hours = application.com.settings.getSetting ("communications.approvals.Gatekeeper.ProcessDurationHours")>
			<cfif approvalDetails.requiresApproval>
				<cfif approvalDetails.UserIsGatekeeper>
					<!---
					<cfif approvalDetails.SENTFORAPPROVAL>
						<cfset formAction = "approveCommunication.cfm">
					</cfif>
					--->
					<CFQUERY NAME="getUsersGatekeeperDetails" dbtype = "query" maxrows="1">
						select * from approvalDetails.GATEKEEPERQUERY where personid=#request.relaycurrentuser.personid#
					</CFQUERY>
					<cfif getUsersGatekeeperDetails.approvedReleaseDate is "">
						<!--- if the communication is already approved then default the releasedate to the same date currently approved date --->
						<cfif approvaldetails.isApproved>
							<cfset temp_date = approvaldetails.ApprovedSenddate>
						<cfelseif communication.senddate is not "" and communication.senddate gt application.com.datefunctions.addWorkHours(date=now(),hours = application.com.settings.getSetting ("communications.approvals.Gatekeeper.ProcessDurationHours"))>
							<cfset temp_date = communication.senddate>
						<cfelse>

							<cfset temp_date = application.com.datefunctions.addWorkHours(date=now(),hours=hours)>
						</cfif>
					<cfelse>
						<cfset temp_date = getUsersGatekeeperDetails.approvedReleaseDate>
					</cfif>
				<cfelse>
					<cfset temp_date = application.com.datefunctions.addWorkHours(date=now(),hours=hours)>
				</cfif>
			<cfelse> <!---using a GateKeeper approval process but does not require approval e.g. a test comm --->
				<cfset temp_date = application.com.datefunctions.addWorkHours(date=now(),hours=hours)>
			</cfif>
		</cfif>
	</cfif>


	<CFQUERY NAME="getCommInfo" DATASOURCE="#application.SiteDataSource#">
			SELECT c.Title,
				   c.Description,
				   cs.SelectionID,
				   c.CommFormLID,
				   c.SendDate,
				   c.LastUpdated
			  FROM Communication AS c
			  	left join CommSelection AS cs on c.CommID = cs.CommID
			 WHERE c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfif frmSelectID is not "">
				AND cs.selectionid  in ( <cf_queryparam value="#frmSelectID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  <!--- WAB added, limits query to the selections chosen at this time - allows selections to be added to an existing communication --->
				</cfif>
	</CFQUERY>

	<CFSET downloadname = "address">
	<CFIF getCommInfo.RecordCount IS NOT 0>
		<CFIF Trim(getCommInfo.Title) IS NOT "">
			<!--- remove all non-alpha and non-numeric characters--->
			<CFSET downloadname = REReplace(getCommInfo.Title,"[^a-z,A-Z,0-9]","","ALL")>
		</CFIF>
	</CFIF>

	<!--- we assume that LookupID 4 is download
			later in this template
			but if this is a download, then
			there will not be any records
			in CommFile
	--->
	<CFQUERY NAME="getCommForm" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT ll.ItemText, ll.LookupID
		  FROM LookupList AS ll, Communication AS c
		 WHERE ll.LookupID = c.CommFormLID
		   AND c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY ll.ItemText
	</CFQUERY>


	<CFIF getCommInfo.RecordCount IS NOT 0>
		<!--- the below is also in commsend.cfm --->
		<CFIF #ListFind("5,6,7",getCommInfo.CommFormLID)# IS NOT 0>
			<!--- can't be reached by fax or email --->
			<CFSET TagName = " - invalid fax & email">
		<CFELSEIF getCommInfo.CommFormLID IS 2>
			<!--- can't be reached by fax --->
			<CFSET TagName = " - invalid email">
		<CFELSEIF getCommInfo.CommFormLID IS 3>
			<!--- can't be reached by fax --->
			<CFSET TagName = " - invalid fax">
		</CFIF>
	<!--- =====================
		 qryCommCounts.cfm
		 ======================
		required inputs:
			a list of selectionIDs: Variables.selectIDlist
			the CommFormLID of the communication: Variables.thisCommFormLID
		 outputs (if record count is > 0):
		 	SelectTitleList (a comma separated list of all comm titles)
			TagPeople
			FeedbackCounts
	--->
		<CFSET selectIDlist = ValueList(getCommInfo.SelectionID)>
		<CFSET thisCommFormLID = getCommInfo.CommFormLID>

	<!---	<cfinclude template="#session.qrycomm#"> --->
		<cfif selectIDlist neq "">
			<cfinclude template="#thisDir#/qryCommCounts.cfm">
		</cfif>
	</CFIF>
</cfif>

<CFOUTPUT>

<cf_head>
<cf_title>#request.CurrentSite.Title#</cf_title>

<cf_includejavascriptonce template = "/javascript/openWin.js">
<cf_includejavascriptOnce template ="/javascript/dropdownFunctions.js" >
<cf_includejavascriptOnce template ="/javascript/checkBoxFunctions.js" >
<cf_includejavascriptOnce template ="/javascript/contextMenu.js" >

<cf_includeJavascriptOnce template = "/javascript/prototypeSpinner.js">
<cf_includeJavascriptOnce template = "/communicate/communication.js">
<cf_includeJavascriptOnce template = "/javascript/popupDescription.js">

</cf_head>

<script>
	fnInit()
	$(document.body).observe('click',fnDetermine)
</script>
<div status="false" onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);"  id="oContextMenu"  class="menu"></div>

</CFOUTPUT>

<CFHTMLHEAD TEXT='<LINK REL="stylesheet" HREF="/Styles/contextMenuStyles.css">'>


<cfset Sys_Comm_Approve_Text = application.com.relayTranslations.translatePhrase(phrase="Sys_Comm_Approve")>
<cfset Sys_Comm_SetApprovedReleaseDate_Text = application.com.relayTranslations.translatePhrase(phrase="Sys_Comm_SetApprovedReleaseDate")>

<cfoutput>
<!--- <cfif useWrapper> --->
<script>
	commID_enc = '#application.com.security.encryptVariableValue("commid",frmCommID)#'

	checkFormcommconfirm = function(sendAction) {
		<!--- var form = document.commConfirmForm; --->
		var form = document.getElementById("commConfirmForm");
		<cfif useWrapper>
		var parentSendActionsList = 'now,Approve';
		var parentSendActionsArray = parentSendActionsList.split(',');
		var openInParent = false;
    	for (var i = 0; i < parentSendActionsArray.length; i++) {
    		if (parentSendActionsArray[i] == sendAction) {
				openInParent = true;
    		}
    	}
    	if (openInParent) {
	    	form.target="_parent"
	    	form.frmNextPage.value = "#jsStringFormat(ParentPage)#"
    	} else {
	    	form.target="_self"
    	}
		</cfif>
		form.frmAction.value=sendAction;
		form.frmSendAction.value=sendAction;
		if (sendAction == 'Approve') {
			approverType = document.getElementById("frmApproverType");
			if (approverType.value == "Gatekeeper") {
				approveBtn = document.getElementById("frmApprove");
				if (approveBtn.value == "#jsStringFormat(Sys_Comm_SetApprovedReleaseDate_Text)#") {
					if (form.frmapprovedReleaseDate.value == "") {
						alert('Please enter a date when the communication can be sent') ;
					} else {
						form.frmSendDate.value=form.frmapprovedReleaseDate.value;
						form.submit();
					}
				} else {
					approveBtn.value = "#jsStringFormat(Sys_Comm_SetApprovedReleaseDate_Text)#"
					approvedReleaseDateArea = document.getElementById("ApprovedReleaseDateArea");
					approvedReleaseDateArea.style.display = "inline";
				}
			} else {
				form.submit();
			}
		} else {
			form.submit();
		}
	}

	 getFriendsAddresses = function () {
		$('emailAddressRow').show()
	}


	sendToFriend = function (commid,obj,options) {
		var emailAddresses = $('emailAddresses').value
		$('emailAddressRow').hide()
		if (emailAddresses != '') {
			sendComm(commid,obj,{methodname:'sendCommToFriend',emailAddresses:emailAddresses,asynchronous:false,test:options.test})
		}

		return false
	}





</script>
</cfoutput>
<!--- </cfif> --->

<cfoutput>
<div id="commconfirmWrapperTable">
	<h2>Phr_Sys_comm_Step_#htmleditformat(stepName)#_Header</h2>
</cfoutput>

<CFIF isDefined("getCommInfo") and getCommInfo.RecordCount IS NOT 0>
	<cfoutput>

	<cfset checkIfCommunicationIsBeingSent = communication.checkIfCommunicationIsBeingSent()>

		<div class="commResultsDiv">
			<cfif checkIfCommunicationIsBeingSent.isBeingSent>
				<script>
					messageDiv = createCommMessageDiv (null,true,'This communication is being sent at the moment',true)
					updateCommStatus (commID_enc ,#checkIfCommunicationIsBeingSent.metadata.commSendResultID#,messageDiv)
				</script>
			</cfif>
			<cfif not structisempty(messageStruct)>
				<div class="<cfif messageStruct.type eq 'error'>errorblock<cfelse>infoBlock</cfif>">
					#application.com.security.sanitiseHTML(messageStruct.message_HTML)#
				</div>
			</cfif>
		</div>
		<h3><cfif testMode>Send a Test Email<cfelse>Send an Individual Email</cfif></h3>
		<div class="form-group">
			<a href="" class="btn btn-primary" onclick="sendComm(commID_enc,this,{personids:'#application.com.security.encryptVariableValue("personids",request.relaycurrentuser.personid)#',test:#testMode#,asynchronous:false,methodname:'sendCommToSelf'}); return false">Click here Phr_Sys_Comm_toSendToSelf</a>
			<!--- TBD - after a comm has been sent externally, this should no longer be sending test versions - they are real! --->
			<a href="##" id="getFriendsAnchor" onclick="getFriendsAddresses();return false" class="btn btn-primary">Click here Phr_Sys_Comm_toSendToAFriend</a>
		</div> <!--- 2014-12-02	DXC --->
		<div id="emailAddressRow" style="display:none">
			<div class="form-group">
				<label>Enter Email Addresses</label>
				<textArea class="form-control" id="emailAddresses" rows=6 cols=60></textArea>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="phr_send" onclick="sendToFriend(commID_enc,$('getFriendsAnchor'),{test:#testMode#});return false">
			</div>
		</div>
	</cfoutput>
				<cfif selectIDlist neq "">
						<CFSET DefaultSend = DateAdd("d",0,Now())>
						<cfoutput>
						<FORM NAME="commConfirmForm" ID="commConfirmForm" ACTION="#formAction#" METHOD="POST" target="_self">
							<table class="table table-hover">
								<CF_INPUT TYPE="HIDDEN" NAME="frmSendDate" VALUE="#DateFormat(DefaultSend,"dd-mmm-yy")#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmSelectID" VALUE="#frmSelectID#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#frmCommID#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
								<INPUT TYPE="HIDDEN" NAME="frmAction" VALUE="queue">
								<INPUT TYPE="HIDDEN" NAME="frmSendAction" VALUE="queue">
								<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(getCommInfo.LastUpdated)#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmCallingTemplate" VALUE="#frmCallingTemplate#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmNextPage" VALUE="#frmNextPage#">
								<CF_INPUT TYPE="HIDDEN" NAME="useWrapper" VALUE="#useWrapper#">
								<CF_INPUT TYPE="HIDDEN" NAME="stepName" VALUE="#stepName#">
								<CF_INPUT TYPE="HIDDEN" NAME="frmResend" VALUE="#frmResend#">
							</cfoutput>
							<!--- </table> --->
							<cfoutput>
							<cfset links.sendTest.url = "#thisDir#/sendTestCommunication.cfm?frmCommID=#communication.CommID#&frmcallingtemplate=#frmcallingtemplate#&stepName=#stepName#">
							<cfset links.sendNow.url = "#thisDir#/sendCommunication.cfm?frmCommID=#communication.CommID#&frmnextpage=commConfirm.cfm?frmCommID=#communication.CommID#&frmcallingtemplate=#frmcallingtemplate#&stepName=#stepName#&_cf_nodebug=true">
							<cfset links.sendToQueue.url = "#thisDir#/commSend.cfm?frmCommID=#communication.CommID#&frmLastUpdated=#urlencodedformat(createodbcdatetime(communication.lastupdated))#&frmSendDate=#DateFormat(now(),"dd-mmm-yy HH:mm")#&frmnextpage=#frmnextpage#?frmcommid=#communication.commid#&frmcallingtemplate=#frmcallingtemplate#&stepName=#stepName#">
							<cfset links.resend.url = "#thisDir#/resendCommItems.cfm?frmCommID=#communication.CommID#&frmnextpage=#frmnextpage#?frmcommid=#communication.commid#&frmcallingtemplate=#frmcallingtemplate#&stepName=#stepName#&_cf_nodebug=true">
							<xml id="contextDef" style="visibility:hidden;">
								<xmldata>
									<contextmenu id="commSelections">
										<item id="view" value="View Selection" ></item>
										<item id="sendNow" value="Send Now" url=""></item>
										<item id="sendTest" value="Send Test" ></item>
										<item id="sendToQueue" value="Send To Queue" ></item>
										<CFIF request.isAdmin or communication.isOwner>
										<item id="resend" value="Re-send" ></item>
										</cfif>
									</contextmenu>
								</xmldata>
							</xml>

							<script>
							fnInit()

							fnFireContext_commSelections = function(oMenu,oClickedItem) {
								// Customize this function based on your context menu
								urlVariableString = '&' + oClickedItem.id

								//pageString = iDomainAndRoot + '/communicate/commResultsbyStatus.cfm?frmruninpopup=true';

								//alert ('Here')
								switch (oMenu.getAttribute("menuid")) {
									case "sendNow":
										sendComm(commID_enc,oClickedItem,{selectionid:oClickedItem.getAttribute('selectionid')})
										break ;

									case "sendTest":
										url =  '#jsStringFormat(links.sendTest.url)#'  + urlVariableString
										document.location.href = url
										break ;

									case "resend":
										sendComm(commID_enc,oClickedItem,{selectionid:oClickedItem.getAttribute('selectionid'),methodname:'reSendComm'})
										break ;

									case "sendToQueue":
										url =  '#jsStringFormat(links.sendToQueue.url)#'  + urlVariableString
										document.location.href = url
										break ;

									// RMB - 2012-10-22 - 431391 - Made pop up wider to fit in menu item with pop up, changed width=450 to width=800
									case "view":
										urlVariableString = urlVariableString.replace(/frmselectid/i,'frmSearchID')
										url =  iDomainAndRoot + '/\selection/\selectreview.cfm?'  + urlVariableString
										openWin( url ,'PopUp','width=800,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
										break ;
								}
							}

							</script>

							<tr>
								<TH VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>phr_sys_comm_SendCommunication</tH>
							</tr>


							</cfoutput>
							<!--- <table> --->
							<CFOUTPUT>
							<TR>
								<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>
							</CFOUTPUT>
								<cf_divOpenClose divID="selections" title="phr_sys_comm_selectionsHeader" version="2">
									<CFIF #Variables.SelectTitleList# IS NOT "">
										<CFOUTPUT>
										<p>#htmleditformat(Variables.SelectTitleList)#</p>
										<!--- tagPeople always exists --->
										<p>A total of #htmleditformat(Variables.TagPeopleCount)# individual(s) are tagged</p>
										<!--- feedback counts depends on type of communciation being sent --->
										<p>#application.com.security.sanitiseHTML(Variables.FeedbackCounts)#</p>
										</CFOUTPUT>
										<cfif CommSelectionDetails.recordCount>
											<cfoutput>
											<div id="selectionDetail">
											<TABLE class="SelectionsAssigned table table-hover">

												<tr><th colspan="4">phr_sys_comm_selectionsAssignedHeader</th></tr></cfoutput>
												<cfoutput query="CommSelectionDetails" group="type">
													<TR><TD colspan="4" class="heading2">#htmleditformat(Type)#</TD></TR>
													<cfoutput>
													<cfset contextMenuActions = "">
													<TR>
														<td>#htmleditformat(title)# <cfif isSelection>(#htmleditformat(Description)#) (ID:#htmleditformat(selectionid)#)</cfif> </TD>
														<cfif selectionDeleted is not 1>
															<td align="center" with="100">#htmleditformat(NumberofPeople)#</td>
															<td>
																<cfif isSelection>
																	<cfif Status is 'Sending'>
																			Sending Now
																	<cfelseif Status is 'PartSent'>
																			Waiting to send next batch
																		<cfif (request.isAdmin or userIsAllowedToSendNow) and isSelection and communication.isoktosendnow()>
																			<cfset contextMenuActions = listappend(contextMenuActions,"sendNow")>
																		</cfif>
																	<cfelseif status is 'scheduled'>   <!--- selection queued - can be sent now if appropriate permission --->
																		Queued <cfif sendDate is not "">
																					#application.com.datefunctions.dateTimeFormat(date = sendDate, showAsLocalTime = true,showIcon="false")#
																					<cfif communication.senddate is not "" and communication.sent is 0 and senddate gt communication.senddate>
																					but will be sent on	#application.com.datefunctions.datetimeformat(date = communication.senddate, showAsLocalTime = true,showIcon="false")#  &nbsp;
																					</cfif>

																				</cfif>

																		<cfif (request.isAdmin or userIsAllowedToSendNow) and isSelection and communication.isoktosendnow()>
																			<!--- replaced with context menu <a HREF="#links.sendNow.url#&frmselectid=#selectionid#">Send Now</a> 		 --->
																			<cfset contextMenuActions = listappend(contextMenuActions,"sendNow")>
																		</cfif>
																	<cfelseif status is 'Sent'>  <!--- selection sent - can be resent if appropriate permission --->
																		Sent <cfif sentDate is not "">#application.com.datefunctions.dateTimeFormat(date = sentDate, showAsLocalTime = true,showIcon="false")#&nbsp;</cfif>   <!--- WAb 2005-09-13 changed from senddate to sentdate --->
																		<cfif communication.isOKToSendNow() and (request.isAdmin or type is "approver") >
																		<cfset contextMenuActions = listappend(contextMenuActions,"resend")>
																		</cfif>

																	<cfelseif (communication.hasrightsToEdit())and communication.isoktoQueueNow() and commSelectLID is not application.com.communications.constants.excludeSelectLID>
																		<cfif isdefined("useCommunicationQueue") and useCommunicationQueue>
																			<!--- replaced with context menu <a HREF="#links.sendToQueue.url#&frmselectid=#selectionid#">Send to Queue</a>  --->
																			<cfset contextMenuActions = listappend(contextMenuActions,"sendToQueue")>
																		</cfif>
																		<cfif userIsAllowedToSendNow and communication.isOKToSendNow()>
																			<!--- replaced with context menu <a HREF="#links.sendNow.url#&frmselectid=#selectionid#">Send Now</a> --->
																			<cfset contextMenuActions = listappend(contextMenuActions,"sendNow")>
																		</cfif>
																		<!--- WAB 2009/02/03 if whole comm is scheduled (but the individual selection isn't), show the queued date --->
																		<cfif communication.senddate is not "">
																			Queued #application.com.datefunctions.datetimeformat(date = communication.senddate, showAsLocalTime = true,showIcon="false")#
																		</cfif>
																	</cfif>

																	<cfif commSelectLID is application.com.communications.constants.approverSelectLID>
																		<cfset contextMenuActions = listappend(contextMenuActions,"sendTest")>
																	</cfif>
																	<cfif (isSelection and sent is 0) and (communication.hasrightsToEdit())>
																		<cfset contextMenuActions = listappend(contextMenuActions,"remove")>
																	</cfif>
																</cfif>
															</td>

															<td >
																 <cfif isSelection>
																 	<!--- replaced with context menu <A HREF="..\selection\selectreview.cfm?frmsearchid=#selectionid#">View</A> --->
																	<cfset contextMenuActions = listappend(contextMenuActions,"view")>
																</cfif>
																<cfif contextMenuActions is not "">
																	<A class="btn btn-primary" href="##" onclick="javascript:fnLeftClickContextMenu(event)" selectionid="#application.com.security.encryptVariableValue("selectionID",selectionid)#" id="frmselectid=#selectionid#" oncontextmenu="return false" contextmenu = "commSelections" contextmenushowitems="#contextMenuActions#">Action</A>
																	<!--- <A href="" onclick="javascript:void(openNewTab('event','','',{reuseTab:true,reuseTabJSFunction: function () {return true}}))" id="frmselectid=#selectionid#" contextmenu = "commSelections" contextmenushowitems="#contextMenuActions#">Action</A> --->
																	<!--- javascript:void(openNewTab('event','Home page (externalHome)','',{reuseTab:true,reuseTabJSFunction: function () {return true}}))" --->
																</cfif>
															</td>
														<cfelse>
															<TD colspan="4">&nbsp;</td>
														</cfif>

													</TR>
													</cfoutput>
												</cfoutput>
											<CFOUTPUT>
											</TABLE>
											</div>
											</cfoutput>
										</cfif>
									<CFELSE>
										<CFOUTPUT><P>A related selection could not be found.</P></CFOUTPUT>
									</CFIF>
								</cf_divOpenClose>
								</TD>
							</TR>

							<cfif useApprovalProcess>
								<CFOUTPUT>
								<TR>
									<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>
								</CFOUTPUT>
									<cfset attributeStruct = {version=2}>
									<cf_divOpenClose divID="Approval" title="Approval" level="1" attributeCollection=#attributeStruct#>
										<CFOUTPUT>
										<cfif not approvalDetails.requiresApproval>
											phr_sys_comm_ApprovalNotRequired
										<cfelseif  approvalDetails.isapproved>
											<cfif structKeyExists(approvalDetails,"APPROVEDSENDDATE") and approvalDetails.APPROVEDSENDDATE is not "">
												<cfset form.ApprovedSendDateTime = application.com.datefunctions.datetimeformat(date = approvalDetails.APPROVEDSENDDATE, showAsLocalTime = true,showIcon="false")>
												phr_sys_comm_ApprovedToBeSentOn
											<cfelse>
												phr_sys_comm_HasBeenApproved
											</cfif>
										<cfelseif not approvalDetails.isPartlyApprovedByGatekeeper>
											phr_sys_comm_HasNotBeenApproved
											<cfif approvalDetails.editedSinceApproval>
											<BR>Edited since approval
											</cfif>

										<cfelse>
											<!--- WAB 2013-04024 CASE 434859 When partially approved, add details of which countries aprpoved or not (Brought from 2013) --->
											<style>
											ul.indented li {line-height:120%; font-weight:bold}
											ul.indented li ul li {padding-left:20px;line-height:normal;font-weight:normal}
											</style>

											<UL class="indented">
												<LI>Approved for
													<UL>
													<cfloop query="approvalDetails.ApprovedCountries"><LI>#countryDescription#</LI></cfloop>
													</UL>
												</LI>
										<!--- TBD - some feedback on countries not approved. (perhaps no approver) --->
												<LI>Not approved for
													<UL>
													<cfloop query="approvalDetails.notApprovedCountries"><LI>#countryDescription#</LI></cfloop>
													</UL>
											</UL>

										</cfif>
										</CFOUTPUT>
										<cfif useGatekeeperProcess and structkeyexists(approvalDetails,"GATEKEEPERQUERY") and approvalDetails.GATEKEEPERQUERY.recordcount gt 0>
											<cfset links.sendTest.url = "/communicate/sendTestCommunication.cfm?frmCommID=#communication.CommID#">
											<cfset GatekeeperIDList = "">
											<cf_divOpenClose divID="Gatekeepers" title="Gatekeepers" version="2" level="2" startCollapsed="true">
												<cfoutput><ol>
												<!--- <cfoutput query="communication.Approval.GATEKEEPERQUERY"> --->
													<!--- <li> ---><!--- #APPROVERNAME# --->
														<div id = "GatekeeperTable">
															<table>
																<tr>
																	<th>phr_Gatekeeper</th>
																	<th>phr_Countries</th>
																	<th>phr_Role</th>
																	<th>phr_Status</th>
																<td></td><td></td><td></td>
																</tr>
												</cfoutput>

																<cfoutput query="approvalDetails.GATEKEEPERQUERY" group="ApproverName">
																	<cfif personid is request.relayCurrentUser.personid><cfset userIsGateKeeper = true><cfset userRow = currentRow></cfif>
																	<cfif personid is not ""><cfset GatekeeperIDList = LISTAPPEND(GatekeeperIDList,personid)></cfif>
																	<tr>
																		<td valign=top>#ApproverName#</td>
																		<td valign=top><cfoutput>#countryDescription#<BR></cfoutput></td>
																		<!--- 2005-08-11 SWJ: CR_SNY529  Added this below so that if you set a person text flag with the
																		textFlagID CommGateKeeperRoleDescription and set that for the gatekeepers you can describe theire role --->
																		<td valign=top>
																			#roleDescription#
																		</td>
																		<td valign=top>
																			<cfif personid is not "">
																				<cfif approved is 1 and approvedDate lt communication.contentlastupdated>Edited since approval<cfelseif approved is 1 and isDate (approvedReleaseDate)> Approved to send #application.com.datefunctions.dateTimeFormat(date = approvedReleaseDate,  showAsLocalTime = true)#<cfelseif disapproved is 1>Put on Hold #application.com.datefunctions.datetimeformat(date = approvalStatusLastUpdated, showAsLocalTime = true)# <BR>(#replaceNoCase(comment,chr(13)&chr(10),"<BR>","ALL")#)<cfelseif sentForApproval is 1>Approval Requested #application.com.datefunctions.dateTimeFormat(date =approvalRequestedDate, showAsLocalTime = true,showIcon="false")#<cfelse>Not Sent</cfif>
																				<cfif approvalStatusLastUpdated is not "" and approvalStatusLastUpdated lt approvalrequestedDate><BR>Re-requested #application.com.datefunctions.dateTimeFormat(date = approvalRequestedDate, showAsLocalTime = true,showIcon="false")#</cfif>
																			</cfif>
																		</td>
																		<td valign=top>
																			<cfif personid neq "">
																			<!--- CASE 438411 WAB fixed name of method being called --->
																				<A onclick="sendComm(commID_enc,this,{personids:'#application.com.security.encryptVariableValue("personids",personid)#',test:true,resend:true,asynchronous:false,methodname:'sendCommToPerson'}); return false" >Send Test</A>
																			</cfif>
																		</td>
																	</tr>
																</cfoutput>
														<CFOUTPUT>
															</table>
														</div>
														</CFOUTPUT>
													<!--- </li> --->
												<!--- </cfoutput> --->
												<!--- <cfoutput></ol></cfoutput> --->
											</cf_divOpenClose>
										</cfif>
										<cfif structkeyexists(approvalDetails,"APPROVERSQUERY") and approvalDetails.APPROVERSQUERY.recordcount gt 0>
											<cf_divOpenClose divID="Approvers" title="Approvers" version="2" level="2" startCollapsed="true">
												<!--- <cfoutput><ol></cfoutput>
												<cfoutput query="communication.Approval.APPROVERSQUERY">
													<li>#Name#</li>
												</cfoutput>
												<cfoutput></ol></cfoutput> --->
												<CFOUTPUT>
												<div id="ApprovalTable">
													<table>
														<tr><th>Approver</th><th>Status</th></tr>
												</CFOUTPUT>
													 	<cfoutput query = "approvalDetails.approversQuery">
															<cfif approvalDetails.requiresApproval and approvalDetails.ApprovalProcessType is "Gatekeeper" and listfind(valuelist(approvalDetails.gatekeeperQuery.personid),personid) is not 0>
																<cfset isGatekeeper = true>
															<cfelse>
																<cfset isGatekeeper = false>
															</cfif>
															<tr><td>#approvalDetails.approversQuery.name# <cfif isGateKeeper> (Gatekeeper)</cfif></td><td>#approvalDetails.approversQuery.approvalstatusMessage#</td>
															</tr>

														</cfoutput>
												<CFOUTPUT>
													</table>
												</div>
												</CFOUTPUT>
											</cf_divOpenClose>
										</cfif>
									</cf_divOpenClose>
								<CFOUTPUT>
									</TD>
								</TR>
								</CFOUTPUT>
							</cfif>
							<CFOUTPUT>
							<TR><TD COLSPAN="3"><img src="/images/misc/rule.gif" alt="" width="100%" height="3" border="0"><BR></TD></TR>
								<TR>
									<TD COLSPAN="3" Class="CommButtonsArea SendButtons">
										<!--- 2012-02-01 NYB Case425248 replaced if condition with new variable: --->
										<cfif communication.hasRightsToSchedule()>
												<cfif communication.hasExternalSelectionsWaitingToSend() or listLen(frmpersonids)>
													<cfquery name="externalUserCountQry" dbtype="query">
														select sum(numberofpeople) totalNumberofpeople from CommSelectionDetails
														where sent=0 and selectiondeleted=0 and [type]='External'
													</cfquery>
													<cfset externalUserCount = externalUserCountQry.totalNumberofpeople>
													<table border="0" Class="CommButtonLayoutArea">
													<tr>
														<!--- START: 2012-02-01 NYB Case425248 added condition here --->
														<cfif isDefined("approvalDetails") and communication.isOKToQueueNow() and !listLen(frmpersonids)>
														<td>
															<CF_INPUT type="Button" name="frmSchedule" value="Phr_Sys_Comm_ScheduleCommunication" onclick="javascript:void(openWin('#thisDir#/scheduleCommunication.cfm?frmCommID=#communication.CommID#','PopUp','width=500,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0'));">
														</td>
														</cfif>
														<!--- END: 2012-02-01 NYB Case425248 --->
														<cfif communication.ISOKTOSENDNOW() and externalUserCount lt sendImmediatelyThreshold>
															<td>
																<input type="Button" class="btn btn-primary" name="frmSend" value="Phr_Sys_Comm_SendNow" onclick="sendComm(commID_enc,this); return false">
															</td>
														</cfif>
														<cfif isDefined("approvalDetails") and approvalDetails.requiresApproval and approvalDetails.ApprovalProcessType is "Gatekeeper" and approvalDetails.GATEKEEPERQUERY.recordcount gt 0>
															<td>
																<input type="Button" class="btn btn-primary" name="frmSave" value="Phr_Sys_Comm_SendForApproval"  onclick="sendComm(commID_enc,this,{methodname:'sendCommForApproval'}); return false" >
															</td>
														</cfif>
													</tr>
													</table>

													<cfif (useApprovers and structkeyexists(approvalDetails,"USERISAPPROVER")
															and approvalDetails.USERISAPPROVER and structkeyexists(approvalDetails,"gatekeeperQuery"))
														or
														(useApprovalProcess and isDefined("approvalDetails")
															and approvalDetails.requiresApproval
															and approvalDetails.ApprovalProcessType is "Gatekeeper"
															and structkeyexists(approvalDetails,"UserIsGatekeeper")
															and approvalDetails.UserIsGatekeeper)
														>
														<div id="ApprovedReleaseDateArea" name="ApprovedReleaseDateArea" style="display:none;">
															<cfquery name="gatekeeperQuery" dbtype="query" maxRows="1">
																select approved,approvedDate,disapproved,approvedReleaseDate,personid from approvalDetails.gatekeeperQuery
																where personid=<cfqueryparam value="#request.relaycurrentuser.personid#" cfsqltype="CF_SQL_INTEGER">
															</cfquery>
															<cfif isdefined("approvaldetails") and gatekeeperQuery.approvedReleaseDate is "">
																<!--- if the communication is already approved then default the releasedate to the same date currently approved date --->
																<cfif approvaldetails.isApproved>
																	<cfset temp_date = approvaldetails.ApprovedSenddate>
																<cfelseif communication.senddate is not "" and communication.senddate gt application.com.datefunctions.addWorkHours(date=now(),hours=application.com.settings.getSetting("communications.approvals.Gatekeeper.ProcessDurationHours"))>
																	<cfset temp_date = communication.senddate>
																<cfelse>
																	<cfset temp_date = application.com.datefunctions.addWorkHours(date=now(),hours =application.com.settings.getSetting("communications.approvals.Gatekeeper.ProcessDurationHours"))>
																</cfif>
															<cfelse>
																<cfset temp_date = gatekeeperQuery.approvedReleaseDate>
															</cfif>
															<cf_relaydatefield
																currentValue="#temp_date#"
																thisFormName="commConfirmForm"
																fieldName="frmapprovedReleaseDate"
																anchorname = "xx"
																showAsLocalTime="true"
															>

															<cf_relaytimefield
																currentValue="#temp_date#"
																thisFormName="commConfirmForm"
																hourfieldName="frmapprovedReleaseDate_hr"
																minutefieldName="frmapprovedReleaseDate_min"
																anchorname = "yy"
																fieldName="frmapprovedReleaseDate"
																showAsLocalTime="true"
															>
														</div>
														<table border="0" Class="CommButtonLayoutArea">
															<tr>
																<td>
																	<input type="hidden" name="frmApproverType" id="frmApproverType"
																		value=
																		<cfif useApprovalProcess and structkeyexists(approvalDetails,"UserIsGatekeeper") and approvalDetails.UserIsGatekeeper>
																			"Gatekeeper"
																		<cfelse>
																			"Approver"
																		</cfif>
																	>
																	<CF_INPUT type="Button" name="frmApprove" id="frmApprove" value="#Sys_Comm_Approve_Text#" onclick="javascript:checkFormcommconfirm('Approve')">
																</td>
																<td>
																	<input type="Button" name="frmDisApprove" value="Phr_Sys_Comm_DisApprove" onclick="javascript:checkFormcommconfirm('DisApprove');">
																</td>
															</tr>
														</table>
													</cfif>
												<cfelse>
													<br>phr_sys_Comm_CannotSend_NoSelectionsAvailable<br>
												</cfif>
										<!--- 2012-02-01 NYB Case425248 added else --->
										<cfelse>
											<br>phr_sys_Comm_UserDoesNotHaveRightsToScheduleCommunication<br>
										</cfif>
									</TD>
								</TR>
							</table>
							<!--- 	WAB commented out 2005-05-04 no longer has to populate commdetail at this stage, so this blurb is not required
									won't create the invalid email selection either - we could add this back as a separate function
									<p>When you click &quot;Send to Queue&quot;, the list of tagged individuals will
										be frozen and the the communication will be queued to be sent.</p>

										<p>Once a communication is queued it is not possible to change the (tagged) individuals who are to receive the communication.  However, until it is sent, you
										may update/add fax and email details to ensure that a greater percentage of the tagged individuals will be reached.
										Selecting &quot;send&quot; will also create a selection containing all the individuals with an invalid email address &/or and invalid fax number so that you may easily update their details.
										This selection will have the name &quot;<CFOUTPUT>#getcommInfo.Title##HTMLEditFormat(Variables.TagName)#</CFOUTPUT>&quot;.</p>
							 --->
						</FORM>
						</CFOUTPUT>
				<cfelse>
					<cfoutput>
					<TABLE class="table table-hover">
						<tr>
							<TH VALIGN="TOP" ALIGN="LEFT">phr_sys_comm_SendCommunication</tH>
						</tr>
						<tr>
							<TD>phr_sys_Comm_CannotSend_NoSelectionsAvailable</TD>
						</tr>
					</TABLE>
					<br />
					</cfoutput>
				</cfif>
	<cfoutput>

	</cfoutput>
<CFELSEIF isDefined("getCommInfo")>
	<cfoutput>
		<div class="message">The requested communication could not be found.<div>
	</cfoutput>
<CFELSEIF communication.creatorID eq "">
	<cfoutput>
		<div class="message02">
			#application.com.relayUI.message(message="The requested communication can not be sent as the owner has been deleted.",type="info",showClose=false)#
		</div>
	</cfoutput>
</CFIF>
<cfoutput>
</div>


</cfoutput>


