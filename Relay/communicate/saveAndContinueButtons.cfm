<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2011-07-04	NYB		P-LEN024 - Created - to give all the comm pages a consistent look/feel
2013-07-02 WAB	Remove save and continue later button, does not make sense now we open each comm in a new tab
2015-10-01	ACPK	CORE-1014 Added Continue button; Added missing phrase/renamed variable for Save & Continue button; Disabled showSave variable
--->
<!--- <cfparam name="showSave" default="true"> --->
<cfparam name="showSaveAndContinue" default="true">
<cfparam name="showContinue" default="false">
<div id="saveAndContinue">
	<cfoutput>
<!--- 		<td width="50%" align="right" style="padding-right:20px;"><cfif showSave><CF_INPUT type="Button" name="frmSave" value="Phr_Sys_Save_Continue_Later" onclick="JavaScript:checkForm#ListFirst(frmCallingTemplate,'.')#('save');"></cfif></td> --->
		<cfif showSaveAndContinue>
			<CF_INPUT type="Button" name="frmSave" value="Phr_Sys_Save_Continue" onclick="JavaScript:checkForm#ListFirst(frmCallingTemplate,'.')#('continue');">
		</cfif> <!--- 2015-10-01	ACPK	CORE-1014 Added missing phrase/renamed variable for Save & Continue button --->
		<cfif showContinue>
			<CF_INPUT type="Button" name="frmSave" value="Phr_Sys_Continue" onclick="JavaScript:checkForm#ListFirst(frmCallingTemplate,'.')#('continue');">
		</cfif> <!--- 2015-10-01	ACPK	CORE-1014 Added Continue button --->
	</cfoutput>
</div>
