<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25	WAB		Added code to allow sending to a list of people defined in frmpersonid rather than a selection
		2000-08-08	DJH		Changed all the in subqueries to use a tempoarary table	
		2011-07-04	NYB		P-LEN024
		
--->



<!--- =====================
	 qryCommCounts.cfm
	 ======================
	required inputs:
		a list of selectionIDs: Variables.selectIDlist
		the CommFormLID of the communication: Variables.thisCommFormLID
	 outputs (if record count is > 0):
	 	SelectTitleList (a comma separated list of all comm titles)
		TagPeople
		FeedbackCounts
	<cfinclude template="#thisDir#/qryCommCounts.cfm">		
--->
<!--- assumes the user has permission to see the selection in selectIDlist --->

<cfparam name="frmPersonIDs" default="">

<CFSET SelectTitleList = "">
<CFSET TagPeopleCount = "">
<CFSET FeedbackCounts = "">

<!--- 
WAb 2005-04-27 replaced with code below
<CFIF NOT IsDefined("Variables.CountryList")>
	<!--- get the user's countries --->
	<cfinclude template="/templates/qrygetcountries.cfm">
	<!--- qrygetcountries creates a variable CountryList --->
</CFIF> --->
<cfset ownersCountryIDList = application.com.communications.getCommunicationOwnersCountryIDList(frmCommID)>
<cfset commLimits = application.com.settings.getSetting("communications.limits")>
<cfset commConstants = application.com.communications.constants>

<CFSET TempPersonTable = "Person" & DateFormat(Now(),"yyyymmdd") & TimeFormat(Now(),"HHmmss") & "_" & request.relayCurrentUser.usergroupid>
<CFSET TempLocationTable = "Location" & DateFormat(Now(),"yyyymmdd") & TimeFormat(Now(),"HHmmss") & "_" & request.relayCurrentUser.usergroupid>

<CFIF frmPersonIDs IS "">
	<cfif selectIDList is -1>
		<!--- special code for all selections, get their details --->
		<CFQUERY NAME="getSelections" DATASOURCE="#application.SiteDataSource#">
	 		SELECT selectionid
		        FROM CommSelection AS cs
	            WHERE  cs.commID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and cs.commselectLID in (0)
		</CFQUERY>
		<cfset selectIDList = valuelist(getSelections.selectionid)>
	</cfif>

	<CFQUERY NAME="getCommTitles" DATASOURCE="#application.SiteDataSource#">
 		SELECT s.Title
	        FROM Selection AS s
            WHERE  s.SelectionID  IN ( <cf_queryparam value="#Variables.selectIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
         ORDER BY s.Title
	</CFQUERY>

	<CFSET SelectTitleList = ValueList(getCommTitles.Title)>
	<!--- queries can't use cfqueryparam because of temporary table not being accessible in sp , so use useCFVersion=false --->
	<CFQUERY NAME="getPersonIds" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT 
			st.EntityID as PersonID
		into 
			###TempPersonTable#
		FROM 
			commselection cs
				inner join
			SelectionTag AS st on cs.selectionid = st.selectionid
				inner join 
			person p on p.personid = st.entityid
				inner join 
			location l	on l.locationid = p.locationid
		WHERE 
				cs.commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false"> 
			and st.SelectionID  IN ( <cf_queryparam value="#Variables.selectIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" usecfversion="false"> )
			and personid not in (select entityid from selectiontag st inner join commselection cs on st.selectionid = cs.selectionid where cs.commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false">  and CommselectLID =  <cf_queryparam value="#commConstants.excludeSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false">  and status <> 0)
			AND st.Status <> 0
			AND p.active <> 0
 	 	  AND (l.CountryID  IN ( <cf_queryparam value="#ownersCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" usecfversion="false"> ) or commSelectLID  in ( <cf_queryparam value="#commConstants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" usecfversion="false"> ))
	</CFQUERY>
	<!---<CFSET EntityIDs=ValueList(getSelTag.EntityID)>--->
<CFELSE>
		<CFSET SelectTitleList = "Temporary Selection">							 
		<CFQUERY NAME="getPersonIds" DATASOURCE="#application.SiteDataSource#">  
			SELECT personid
			into ###TempPersonTable#
			FROM Person p
				inner join 
			location l	on l.locationid = p.locationid
			WHERE Personid  IN ( <cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" usecfversion="false"> )			<!---  frmpersonids can be either delimted list or select statement--->
 	 	 	  AND (l.CountryID  IN ( <cf_queryparam value="#ownersCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" usecfversion="false"> ) )
				AND p.active <> 0
		</CFQUERY>
	<!---<CFSET EntityIDs=valuelist(getpersonids.personid)>--->

</cfif>							 

<cfquery name="zero" DATASOURCE="#application.SiteDataSource#">
	insert into ###TempPersonTable#
	(
		PersonID
	)
	values
	(
		0
	)
		
</cfquery>

<cfquery name="indexit" DATASOURCE="#application.SiteDataSource#">
	create unique index tempindex on ###TempPersonTable#
	(PersonID) with fillfactor = 100
</cfquery>

<cfquery name="personid" DATASOURCE="#application.SiteDataSource#">
select count(*) as total from ###TempPersonTable#
</cfquery>

<!--- if there are too many people we'll do a reduced count --->
<!---<CFIF listlen(EntityIDs) LT 1000>--->
<cfif PersonID.total lt 100000>

<!--- get list of locationids --->
	<CFQUERY NAME="getlocationids" DATASOURCE="#application.SiteDataSource#">  
		SELECT distinct 
			locationid 
		into ###TempLocationTable#
		FROM 
			Person inner join
			###TempPersonTable# as tmp on Person.PersonID = tmp.PersonID
	</CFQUERY>

	<cfquery name="zero" DATASOURCE="#application.SiteDataSource#">
		insert into ###TempLocationTable#
		(
			LocationID
		)
		values
		(
			0
		)
	</cfquery>
		
	<cfquery name="indexit" DATASOURCE="#application.SiteDataSource#">
		create unique index tempindex on ###TempLocationTable#
		(LocationID) with fillfactor = 100
	</cfquery>

	<!---<CFSET LocationIDs=valuelist(getlocationids.locationid)>--->

	<!--- only email or prefer email --->			    
	<CFQUERY NAME="getCommCounts" DATASOURCE="#application.SiteDataSource#">
	Select 
	<CFIF #ListFind("2,5,6,7",Variables.thisCommFormLID)# IS NOT 0>
		(SELECT 
			count(1)
		FROM 
			Person AS p 
			inner join Location AS l 
			on p.LocationID = l.LocationID
			inner join ###TempPersonTable# as tmp
			on p.PersonID = tmp.PersonID
		WHERE 
            
<!--- removed because now  dealt with when temptablecreated
			p.Active <> 0
AND l.CountryID IN (0,#ownersCountryIDList#) --->
            
			<!--- valid email
					<> implicitily excluded nulls --->			  

			p.Email <> '' AND (p.EmailStatus <  <cf_queryparam value="#commLimits.numberoffailedemailsuntilstopsending#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false">  or p.EmailStatus is null)   <!--- wab added emailstatus is null (after we had to change the emailstatus field) --->
			<!--- and person not in unsubscribed list, and  not in 'no emails' list --->
			<!--- if comm type prefer email or both (5,6) then don't count people or locations in 'prefer fax' list --->
			AND p.personid not IN 
			(Select 
				entityid 
			from 
				booleanflagdata as bfd 
				inner join flag as f 
				on bfd.flagid=f.flagid 
				inner join ###TempPersonTable# as tmp
				on tmp.PersonID = bfd.EntityID
			where 
				f.flagtextid in ('NoCommsTypeAll' ,'NoCommsForm2Pers'	<CFIF #ListFind("5,6",Variables.thisCommFormLID)# IS NOT 0>,'PreferCommsForm3Pers'</CFIF>)
			)

			<!--- and location not in 'no emails' list --->
			AND l.locationid not IN 
			(Select 
				entityid 
			from 
				booleanflagdata as bfd
				inner join flag as f 
				on bfd.flagid=f.flagid 
				inner join ###TempLocationTable# as tmp
				on bfd.EntityID = tmp.LocationID
			where 
				f.flagtextid in ('NoCommsForm2Loc'<CFIF #ListFind("5,6",Variables.thisCommFormLID)# IS NOT 0>,'PreferCommsForm3Loc'</CFIF>) 
			)

		    <CFIF Variables.thisCommFormLID IS 7>
			<!--- preference fax so count people with INvalid faxes or prefer email flag or no faxes flag--->
			AND 
			(

			l.Fax = '' OR l.Fax IS null OR l.FaxStatus >=  <cf_queryparam value="#Variables.NumFaxStatus#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false"> 
			<!--- person in prefer email list --->
			OR p.personid IN 
			(Select 
				entityid 
			from 
				booleanflagdata as bfd
				inner join flag as f 
				on bfd.flagid=f.flagid 
				inner join ###TempPersonTable# as tmp
				on bfd.entityid = tmp.PersonID
			where 
				f.flagtextid in ('PreferCommsForm2Pers','noCommsForm3Pers')
			)
			
			<!--- location in prefer email list --->		
			OR p.locationid 
			IN 
			(Select 
				entityid 
			from 
				booleanflagdata as bfd, 
				inner join flag as f 
				on bfd.flagid=f.flagid 
				inner join ###TempLocationTable# as tmp
				on bfd.entityid = tmp.LocationID
			where 
				f.flagtextid in ('PreferCommsForm2Loc','noCommsForm3Loc')
			)
			
			)
				 
		    </CFIF>
		) AS emailpeople, 
	</CFIF>

	<CFIF #ListFind("5",Variables.thisCommFormLID)# IS NOT 0>
		<!--- both --->			    
			(SELECT 
				count(1)
			FROM 
				Person AS p 
				inner join Location AS l
				on p.LocationID = l.LocationID
				inner join ###TempPersonTable# as tmp
				on p.PersonID = tmp.PersonID
			WHERE 
<!--- removed because now  dealt with when temptablecreated				
				p.Active <> 0
				AND l.CountryID IN (0,#ownersCountryIDList#) --->
				<!--- valid email and fax
						<> implicitily excluded nulls --->			  
				<!--- not in unsubscribed list --->

				p.personid not IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempPersonTable# as tmp
					on bfd.EntityID = tmp.PersonID
				where 
					f.flagtextid in ('NoCommsTypeAll') 
				)

				AND p.Email <> '' AND p.EmailStatus <  <cf_queryparam value="#Variables.NumEmailStatus#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false"> 
				AND l.Fax <>'' AND l.FaxStatus <  <cf_queryparam value="#Variables.NumFaxStatus#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false"> 
		) AS bothpeople,
	</CFIF>				    

	<CFIF #ListFind("3,5,6,7",Variables.thisCommFormLID)# IS NOT 0>
	<!--- only fax or prefer prefer fax or preference email --->			    
		(SELECT 
			count(1)
		FROM 
			Person AS p 
			inner join Location AS l 
			on p.LocationID = l.LocationID
			inner join ###TempPersonTable# as tmp
			on p.PersonID = tmp.PersonID
		WHERE 

<!--- removed because now  dealt with when temptablecreated
			p.Active <> 0
			AND l.CountryID IN (0,#ownersCountryIDList#) --->
	    
			<!--- valid fax
					<> implicitily excluded nulls --->			  
			l.Fax <>'' AND l.FaxStatus <  <cf_queryparam value="#Variables.NumFaxStatus#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false"> 
			<!--- and person not in unsubscribed list, and not in 'no faxes' list--->
			<!--- if comm type prefer fax or both (7) then don't count people in 'prefer email' list --->

			AND p.personid not IN 
			(Select 
				entityid 
			from 
				booleanflagdata as bfd
				inner join flag as f 
				on bfd.flagid=f.flagid 
				inner join ###TempPersonTable# as tmp
				on bfd.EntityID = tmp.PersonID
			where 
				f.flagtextid in ('NoCommsTypeAll','NoCommsForm3Pers'<CFIF #ListFind("5,7",Variables.thisCommFormLID)# IS NOT 0>,'PreferCommsForm2Pers'</CFIF>) 
			)

			<!--- and location not in 'no faxes' list --->
			AND l.locationid not IN 
			(
			Select 
				entityid 
			from 
				booleanflagdata as bfd
				inner join flag as f 
				on bfd.flagid=f.flagid 
				inner join ###TempLocationTable# as tmp
				on bfd.entityid = tmp.locationid
			where 
				f.flagtextid in ('NoCommsForm3Loc' <CFIF #ListFind("5,7",Variables.thisCommFormLID)# IS NOT 0>,'PreferCommsForm2Loc'</CFIF>) 
			)
			<CFIF Variables.thisCommFormLID IS 6>
				<!--- if prefer email, only fax those with INvalid emails --->
				AND 
				(

				p.Email = '' OR p.Email IS NULL OR p.EmailStatus >=  <cf_queryparam value="#Variables.NumEmailStatus#" CFSQLTYPE="CF_SQL_INTEGER" usecfversion="false"> 
				<!--- person in prefer fax list or no email list--->		
				OR p.personid IN 

				(Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempPersonTable# as tmp
					on bfd.entityid = tmp.PersonID
				where 
					f.flagtextid in ('PreferCommsForm3Pers','noCommsForm2Pers')
				)

				<!--- location in prefer fax list or no email list--->		
				OR p.locationid IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempLocationTable# as tmp
					on bfd.entityid = tmp.LocationID
				where 
					f.flagtextid in ('PreferCommsForm3Loc','noCommsForm2Loc')
				)

				)
			</CFIF>
	    ) AS faxpeople, 
	</CFIF>
	(
	SELECT 
		count(1)
	FROM 
		Person AS p 
		inner join Location AS l
		on p.LocationID = l.LocationID
		inner join ###TempPersonTable# as tmp
		on p.PersonID = tmp.PersonID
<!--- removed because now  dealt with when temptablecreated
	WHERE 
		p.Active <> 0
		AND l.CountryID IN (#ownersCountryIDList#) --->
	 ) AS TagPeople
	FROM 
		Dual
	</CFQUERY>

	
	<CFQUERY NAME="getCommCounts2" DATASOURCE="#application.SiteDataSource#">
	Select 
		(
		SELECT 
			count(1)
		FROM 
			Person AS p 
			inner join Location AS l
			on p.LocationID = l.LocationID
			inner join ###TempPersonTable# as tmp
			on p.PersonID = tmp.PersonID
		WHERE 
<!--- removed because now  dealt with when temptablecreated
			p.Active <> 0
			AND l.CountryID IN (0,#ownersCountryIDList#) --->
			
			(
				<!--- people in unsubscribe list --->
				p.personid IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempPersonTable# as tmp
					on bfd.entityid = tmp.PersonID
				where 
					f.flagtextid in ('NoCommsTypeAll') 
				)
			<CFIF #ListFind("2",Variables.thisCommFormLID)# IS NOT 0>
				<!--- if email only then people and locations in 'no emails' list --->
				OR p.personid IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempPersonTable# as tmp
					on bfd.entityid = tmp.PersonID
				where 
					f.flagtextid in ('NoCommsForm2Pers')
				)
				OR p.locationid IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempLocationTable# as tmp
					on bfd.entityid = tmp.LocationID
				where 
					f.flagtextid in ('NoCommsForm2Loc')
				)
			</CFIF>
			<CFIF #ListFind("3",Variables.thisCommFormLID)# IS NOT 0>
				<!--- if fax only then people and locations in 'no faxes' list --->
				OR p.personid IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempPersonTable# as tmp
					on bfd.entityid = tmp.PersonID
				where 
					f.flagtextid in ('NoCommsForm3Pers')
				)
				OR p.locationid IN 
				(
				Select 
					entityid 
				from 
					booleanflagdata as bfd
					inner join flag as f 
					on bfd.flagid=f.flagid 
					inner join ###TempLocationTable# as tmp
					on bfd.entityid = tmp.LocationID
				where 
					f.flagtextid in ('NoCommsForm3Loc')
				)
			</CFIF>
			)
		) AS unsubscribedpeople
    FROM Dual
	</CFQUERY>

	<CFSET TagPeopleCount = getCommCounts.TagPeople>

	<CFIF Variables.thisCommFormLID IS NOT 4>
	<!--- this message not valid for downloads --->
		<!--- now build the feebackcounts variable --->
	
		<CFSET FeedbackCounts = FeedbackCounts & "<P>Based on current information: ">
		
		<CFIF #Variables.thisCommFormLID# IS NOT 3>
			<!--- anything but fax only --->
			<CFSET FeedbackCounts = FeedbackCounts & "<BR>#htmleditformat(getCommCounts.emailPeople)# individual(s) will receive the message by email">
		</CFIF>
		
		<CFIF #Variables.thisCommFormLID# IS NOT 2>
			<!--- anything but email only --->
			<CFSET FeedbackCounts = FeedbackCounts & "<BR>#htmleditformat(getCommCounts.faxPeople)# individual(s) will receive the message by fax">
		</CFIF>	
		
		<CFIF #Variables.thisCommFormLID# IS 5>
			<!--- if both --->
			<CFSET FeedbackCounts = FeedbackCounts & "<P>Note that #htmleditformat(getCommCounts.bothPeople)# of the individual(s) will receive the message by <I>both</I> fax and email">
		</CFIF>	

			<CFSET FeedbackCounts = FeedbackCounts & "<P>#htmleditformat(getCommCounts2.UnsubscribedPeople)# individual(s) have unsubscribed from communications">	

	</CFIF>	

	
	
<CFELSE>
<!--- reduced counts --->

	<CFQUERY NAME="getCommCounts" DATASOURCE="#application.SiteDataSource#">
		SELECT 
			count(1) AS TagPeople
		FROM 
		Person AS p
		inner join Location AS l 
		on p.LocationID = l.LocationID
		inner join ###TempPersonTable# as tmp
		on p.PersonID = tmp.PersonID
<!--- removed because now  dealt with when temptablecreated
		WHERE 
			p.Active <> 0
			AND l.CountryID IN (#ownersCountryIDList#) --->
	</CFQUERY>

	<CFSET TagPeopleCount = getCommCounts.TagPeople>

	<CFIF Variables.thisCommFormLID IS NOT 4>
	<!--- this message not valid for downloads --->
		<!--- now build the feebackcounts variable --->
	
		<CFSET FeedbackCounts = FeedbackCounts & "<P>Due to size of communication counts have not been made ">

	</CFIF>

</CFIF>	