<!--- �Relayware. All Rights Reserved 2014 --->

<!---

WAB 2011/05/17 LID 6321
RJT 2014/11/28 Bug fix (possible) for bug CORE 766 URLDecode(URLurl.path) added to deal with the inconsistent way that internet explorer encodes strings
RJT 2014/12/04 Further Bug fix for bug CORE 766, previous fix allowed file to be opened but left it with a corrupted filename, this corrects the filename as well
RJT 2014/12/04 Further For bug CORE 766, changed from a manual encoding to using filename*=UTF-8 which should work on all modern broswers (including firefox)

replace this if statement which only deals with a few mime types with a function which interrogates IIS to get a mime type

--->


<cfset fileDetails = application.com.regExp.getFileDetailsFromPath(URLDecode(URL.path))>
<cfset getMimeType = application.com.iisadmin.getMimeType(fileDetails.Extension)>

<cfif arraylen(getMimeType)>
	<cfset type=getMimeType[1].xmlattributes.mimetype>
<cfelse>
	<cfset type= "text/html">
</cfif>


<!--- for very old browsers which do not support UTF-8 RJT--->
<cfset fallbackFilename="attachment." & fileDetails.Extension>;

<!--- filename*=UTF-8 supports non english file names RJT --->
<cfheader
	name="content-disposition"
	value="attachment; filename=""#fallbackFilename#""; filename*=UTF-8''#urlEncodedFormat( fileDetails.fullFileName )#"
/>



<cfcontent file="#application.UserFilesAbsolutePath##url.path#" deletefile="no" type="#type#">