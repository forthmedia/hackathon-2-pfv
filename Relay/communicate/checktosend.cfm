<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2000-12-22   Changed name of variable to just send a single communication from justsend these to frmCommID
SWJ	2001-08-03	Changed the layout to be in keeping with the new look and feel
SWJ	2002-11-26	Now calls communicate/sendCommunication rather than tskCommSendDirectly
WAB	2005-05-02		Needed to add ability to schedule individual selections within a communication separately
				This meant that we could no longer just pass a list of Commids to sendCommunication
				Instead we may need to pass lots of sets of commids and selectionids
				Decided that easiest way to do this was to post the form back to this template and
				then loop through the list if necessary
WAB 2009/06/10 	LID 2361 replaced call to templates\qryGetCommToSend with a function call
WAB 2009/12/08	Internationalisation altered calls to dateTimeFormat to display localTime
2011/07/27	NYB		P-LEN024 changed commcheck Details.cfm to commDetailsHome.cfm

 --->

<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add

---->

<CFPARAM NAME="FreezeDate" DEFAULT=#Now()#>
<CFSET usedate = Now()>

<cfif isDefined ("form.frmaction")>

	<cfif isDefined ("form.frmCommIDAndSelectionID")>
		<!--- form being posted back to itself, need to process and pass to sendCommunication --->
		<cfif listLen(form.frmCommIDAndSelectionID,"-") is 1>
			<!--- no dashes means no individual selections specified, so we can just pass the whole lot to send communication --->
				<cfset frmCommID = form.frmCommIDAndSelectionID>
				<cfinclude template = "sendCommunication.cfm">
	
		<cfelse>
				<cfloop index = "thisCommIDAndSelectionID" list = "#form.frmCommIDAndSelectionID#">
					<cfset frmCommID = listFirst(thisCommIDAndSelectionID,"-")>
					<cfif listLen(thisCommIDAndSelectionID,"-") is 2>
						<cfset frmSelectID = listLast(thisCommIDAndSelectionID,"-")>
					<cfelse>
						<cfset frmSelectID = "">
					</cfif>
					
					<cfinclude template = "sendCommunication.cfm">
				</cfloop>
		</cfif>
	<cfelse>
		<!--- processing all --->
		<cfinclude template = "sendCommunication.cfm">
	</cfif>

<cfelse>
	
	
	
	
	<!--- 	WAB 2009/06/10 replaced with function call
	<cfinclude template="/templates/qrygetcommtosend.cfm">
	--->
	<cfset getCommInfo = application.com.communications.getCommunicationsScheduledForNow()>
	
<CFOUTPUT>
	
	
	<cf_head>
	<cf_title>#request.CurrentSite.Title#</cf_title>
	<SCRIPT type="text/javascript">
	<!--
		function verify(form) {
			if (confirm("\nAre you sure you want to send the communciations?")) {
				form.submit();
			} else {
				return;
			}
		}
	//-->
	</SCRIPT>
	</cf_head>
	
</CFOUTPUT>

	<cfparam name="frmHideTopHead" default="false">
	<cfif not frmHideTopHead>
		<cfinclude template="#thisDir#/commtophead.cfm">
	</cfif>
	<!--- <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<cfoutput>
			<TR>
				<TD ALIGN="RIGHT" VALIGN="top" CLASS="Submenu">&nbsp;
					<A HREF="/calltemplate.cfm?template=communicate/commheader.cfm" CLASS="Submenu">New Communication</A> 
				</TD>
			</TR>
		</cfoutput>
	</table>
	 ---><CENTER>
	
	<CFIF #GetCommInfo.RecordCount# IS NOT 0><!--- AND 1 IS 2 use ths  to disable sending of communications--->
	<CFOUTPUT><!--- <FORM ACTION="#thisDir#/sendCommunication.cfm?RequestTimeout=3600" METHOD="POST"> --->
	<FORM ACTION="?RequestTimeout=3600" METHOD="POST">
	<input type="hidden" name="frmaction" value = "send">
	</CFOUTPUT>
		
		<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3">
			<TR><Th><B>&nbsp;</B></Th>
				<Th><B>Communication ID</B></Th>
				<Th><B>Title</B></Th>
				<Th><B>Fax/Email</B></Th>
				<Th><B>Date</B></Th>
				<Th><B>Just send these</B></Th>
			</TR>
			<CFOUTPUT QUERY="getCommInfo">
			<TR><TD><B>#htmleditformat(CurrentRow)#.</B></TD>
				<TD>#htmleditformat(CommID)#</TD>
				<TD><A HREF="commDetailsHome.cfm?frmcommid=#commid#">#htmleditformat(titleAndSelection)#</A></TD>
				<TD ALIGN="CENTER">#htmleditformat(CommFormLID)#</TD>
				<td>
				#application.com.datefunctions.dateTimeFormat(date=SendDate,dontShowZeroHours = true, showAsLocalTime = true)# 
				</td>
				<TD ALIGN="center"><cfif (request.isAdmin or createdBy IS request.relayCurrentUser.usergroupid 
							   OR LastUpdatedBy IS request.relayCurrentUser.usergroupid) and (lastKnownIsApproved is 1 or lastKnownIsApproved is "")> 
					
					  <Input type="checkbox" name="frmCommIDAndSelectionID" value="#CommID#<cfif selectionid is not "">-#htmleditformat(selectionid)#</cfif>">
								<cfelseif (request.isAdmin or createdBy IS request.relayCurrentUser.usergroupid 
							   OR LastUpdatedBy IS request.relayCurrentUser.usergroupid) and lastKnownIsApproved is 0>
									Not Approved
								<cfelse>
									&nbsp;
								</cfif>	
									
									</TD>
			</TR>
			</CFOUTPUT>
			<TR><TD COLSPAN="5" ALIGN="center"><P><INPUT TYPE="BUTTON" name = "x" VALUE=" Process Queued Communications " onClick="verify(this.form);"></P></TD></TR>
			<TR><TD COLSPAN="5" ALIGN="center"><P><B>NB. By default all will be sent</B></P></TD></TR>
		</TABLE>
	</FORM>
			
	<CFELSE>
		<BR>
		<TABLE CELLSPACING=5><TR><TD>No communications are queued to be sent.</TD></TR></TABLE>
	
	</CFIF>
	</CENTER>
	
	
	
	
</cfif>	

