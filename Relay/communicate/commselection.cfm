<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History

1999-01-25  	WAB		Added code to allow sending to a list of people defined in frmpersonid  (effectively bypasses this screen)
						and to run in a popup window
2000-07-24		WAB		Added code to allow addition of new selections to exisiting communications
2011-03-24	NYB		Added a Cancel link on Add Selection
2011-07-04	NYB		P-LEN024
2012/12/17	NJH 	Case 432030 - widened the select boxes.
2013-01-17	NYB 	CASE 432477
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
2012-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-06-10	WAB 	Simplified the queries for populating the select boxes (more done in the getSelections() function
			Indicate if a selection has already been sent (because it can't then be removed) - for the time being it still appears in the select box!
2013-10-09 	PPB 	Case 437297 remove a trim() because it didn't work for IE8 and doesn't seem to be necessary
2016-01-29  SB 		Removed Tables
--->

<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<CFPARAM NAME="frmruninpopup" DEFAULT="false">
<CFPARAM NAME="frmnextpage" DEFAULT="">
<CFPARAM name="readonly" default="false">
<CFPARAM name="stepName" default="">
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">
<CFPARAM NAME="useWrapper" DEFAULT="false">

<cfset formAction = "commDetailsHome.cfm">

<CFSET securitylist = "CommTask:read">
<cfinclude template="/templates/qrycheckperms.cfm">

<CFIF NOT #checkPermission.Level2# GT 0>
	<CFSET message="You are not authorised to use this function">
		<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- the application. cfm checks for communication privleges but
		we also need to check for select privledges
		we can rerun the qrycheckperms but NOTE we have now lost
		the application. cfm query variables.
--->

<CFSET securitylist = "SelectTask:read">
<cfinclude template="/templates/qrycheckperms.cfm">

<CFQUERY NAME="getCommInfo"  DATASOURCE="#application.SiteDataSource#">
	SELECT CommID, LastUpdated
	FROM Communication
	WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<cfif not isdefined("communication")>
	<cfset communication = application.com.communications.getCommunication (commid = #frmcommid#)>
</cfif>

<CFPARAM NAME="frmpersonids" DEFAULT="">   <!--- if list of people already defined, don't need to ask for a selection--->
<CFPARAM NAME="frmShowExclude" default = "true">
<CFPARAM NAME="frmCommSelectLID" default = "0">  <!--- 0 is for selection of external people, 11 is approvers, 12 internal, 13 excludes--->

<CFIF frmpersonids is not "">
	<CFSET frmLastUpdated = #CreateODBCDateTime(communication.LastUpdated)#>
	<cfinclude template="#thisDir#/commselectionadd.cfm">
	<!--- <cfinclude template="#thisDir#/commconfirm.cfm"> --->
	<CF_ABORT>
</CFIF>

<!--- allow any selection to which one has sharing rights --->
<cfset getMySelections = application.com.selections.getSelections()>
<cfset communicationSelections = communication.getSelections()>

<cfquery name="getMySelections" dbtype="query">
select selectionid,title,Title+' ('+cast(selectionid as varchar)+')' as displayText from getMySelections order by title
</cfquery>

<!--- Get the current Selections, note that some may belong to other people so not be in getMySelections --->
	<cfquery name="getAssignedSelections" dbtype="query">
	select selectionid, title, titleForCommSelection as displayText,userHasRightsToSelection
	from communicationSelections where COMMSELECTLID in (#application.com.communications.constants.EXTERNALSELECTLID#,#application.com.communications.constants.INTERNALSELECTLID#)
	</cfquery>

	<cfquery name="checkForSelectionsUserDoesNotHaveRightsTo" dbtype="query">
	select 1 from getAssignedSelections where userHasRightsToSelection = 0
	</cfquery>


	<cfquery name="getUnUsedSelections" dbtype="query">
	select selectionid,title,displayText from getMySelections
	<cfif communicationSelections.recordcount is not 0>
	where selectionid not in (#valuelist(communicationSelections.selectionid)#)
	</cfif>
	order by title
	</cfquery>



<cfquery name="getExcludedSelections" dbtype="query">
	select selectionid, title, titleForCommSelection as displayText,userHasRightsToSelection
	from communicationSelections where COMMSELECTLID in (#application.com.communications.constants.EXCLUDESELECTLID#)
</cfquery>


<script>
	checkFormcommselection = function(btn) {
		var theForm=document.getElementById("CommSelectionForm");
		/*var msg = "\n\nYou must choose a selection.";

		if ((jQuery('#selections_assigned').val() === null) || (theForm.selections_assigned.options.length == 1 && theForm.selections_assigned.options[0].text == ''))	<!--- 2013-10-09 PPB Case 437297 remove the trim() because it didn't work for IE8 and doesn't seem to be necessary --->
		{
			alert(msg);
		} else { */
		theForm.frmRequestedAction.value = btn;
		triggerFormSubmit(theForm);
	}
</script>

<CFIF communication.hasrightsToEdit()>
	<cfset readonly = "false">
</CFIF>

<cfparam name="message" default = "">
<cfif message is not "">
	<CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
</cfif>

	<cfoutput>
		<h2>Phr_Sys_comm_Step_#htmleditformat(stepName)#_Header</h2>
	</cfoutput>

	<CFIF (getMySelections.RecordCount GT 0 or communicationSelections.RecordCount GT 0) AND communication.commid neq "">
		<cfoutput>
		<FORM NAME="CommSelectionForm" ID="CommSelectionForm" ACTION="#formAction#" METHOD="POST" <cfif useWrapper>target="_parent"</cfif> >
			<cf_relayFormDisplay bindValidationWithListener=false autoRefreshRequiredClass=true class="">
					<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#getCommInfo.CommID#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmCommSelectLID" VALUE="#frmCommSelectLID#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(getCommInfo.LastUpdated)#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmnextpage" VALUE="#frmnextpage#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmCallingTemplate" VALUE="#frmCallingTemplate#">
					<INPUT TYPE="HIDDEN" NAME="frmRequestedAction" ID="frmRequestedAction" VALUE="">
					<CF_INPUT TYPE="HIDDEN" NAME="useWrapper" ID="useWrapper" VALUE="#useWrapper#">
					<cf_relayFormElementDisplay type="html" label="phr_sys_comm_selectionAssignedToReceiveComm" required="true">
					<!--- <div class="form-group">
						<label for="selections_assigned">phr_sys_comm_selectionAssignedToReceiveComm</label> --->
						<cfif readonly>
							<cfif communicationSelections.recordcount is not 0>
							<BR>
								<!---PJP CASE 432477: Changed from cfoutput query to cfloop, cftranslate causing issue --->
								<cfloop query = "getAssignedSelections">
								#htmleditformat(displayText)# <BR>
								</cfloop>
							</cfif>
							<!--- xxxx
							<SELECT NAME="frmSelectID" SIZE="10" MULTIPLE>
								<CFOUTPUT QUERY="getSelections">
									<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #Title# (#SelectionID#)
								</CFOUTPUT>
							</SELECT>
							--->
						<cfelse>
						 	<CF_TwoSelectsComboQuery
									    NAME="selections_available"
									    NAME2="selections_assigned"
										value = "selectionid"
										display = "displayText"
									    QUERY1="getAssignedSelections"
										QUERY2="getUnUsedSelections"
										UP="No"
										DOWN="No"
										required="true"
										>
							<cfif isdefined("getOthersSelections") and getOthersSelections.recordcount gt 0>
								phr_sys_comm_asterixDenotesSelectionsYouDoNotHaveRightsTo
							</cfif>
						</cfif>
					</cf_relayFormElementDisplay>
			<CFIF frmShowExclude>
				<cf_relayFormElementDisplay type="html" label="phr_sys_comm_selectionAssignedToBeExcluded">
				<!--- <div class="form-group">
					<label>phr_sys_comm_selectionAssignedToBeExcluded</label> --->
					<cfif readonly>

							<!---NYB CASE 432477: Changed from cfoutput query to cfloop, cftranslate causing issue
							<cfoutput query = "getExcludedSelections">
							#htmleditformat(displayText)# <BR>
							</cfoutput>
							--->
						<cfloop query = "getExcludedSelections">
							#htmleditformat(displayText)# <BR>
						</cfloop>
					<cfelse>
						<CF_TwoSelectsComboQuery
							    NAME="selections_availableForExclusion"
							    NAME2="selections_assignedForExclusion"
								value = "selectionid"
								display = "displayText"
							    QUERY1="getExcludedSelections"
								QUERY2="getUnUsedSelections"
								up="no"
								down="no"
								>

						<cfif isdefined("checkForSelectionsUserDoesNotHaveRightsTo") and checkForSelectionsUserDoesNotHaveRightsTo.recordcount gt 0>
							phr_sys_comm_asterixDenotesSelectionsYouDoNotHaveRightsTo
						</cfif>
					</cfif>
					<!----
					<cfif not readonly>
						Exclude people in the following selections from receiving this communication
					</cfif>
					<cfif getExcludedSelections.recordcount is not 0>
						Selections chosen already<BR>
						<cfoutput query = "getExcludedSelections">
						#Title# <BR>
						</cfoutput>
					</cfif>
					--->

						<!--- xxxx
				<cfif not readonly>
					<TR>
						<td align="center">
						<SELECT NAME="frmExcludeSelectID" SIZE="10" MULTIPLE>
							<CFOUTPUT QUERY="getSelections">
								<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #Title# (#SelectionID#)
							</CFOUTPUT>
						</SELECT>
						</TD>
					</TR>
				</cfif>
						--->
				</cf_relayFormElementDisplay>
			</cfif>
			<cfinclude template="saveAndContinueButtons.cfm">
			</cf_relayFormDisplay>
		</FORM>
		</cfoutput>

	<CFELSE>
		<p>Sorry, you have no selections.</p>
	</CFIF>