<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		updateCommDetail.cfm	
Author:			SWJ
Date started:	2002-11-22
	
Description:		This provides updates commDetail.

Usage:			<CFModule TEMPLATE="#thisDir#/updateCommDetail.cfm"
					pid="#listfirst(p,"-")#"
					cid="#c#"
					action="#a#">
					
			RETURNS: CommDetailID - set to this commdetailID
					
Used by:		Remote.cfm

Amendment History:

Date 	Initials 	What was changed
2004-06-07	WAB		Changed insertStatus  query to set identity_insert ON
2005-05-10	WAB 	Added a check to see whether this is a link from a test communication (url.t will be set)
2005-08-17 	SWJ 	Added a function so that when they click thru their email status is set to -0.5 which is known good
2008/07/03	GCC		Added with(nolock) to reduce unnecessary locking
2009/01/14   WAB	Removed various bits of code to speed up
					i) No longer test for statuses existing - we have tools to make sure that new statuses can be released if necessary
					ii) Don't check that the id pair exists before updating
					iii) Don't return commdetailid  - was only used once in remote.cfm and wasn't necessary so have removed it from remote.cfm
					iv) also made changes to triggers on commdetail
					
Enhancements still to do:


 --->
<!--- manages web bug in email --->

<cfswitch expression="#attributes.action#">
	<cfcase value="t"><cfset feedback="Email Read"><cfset CommStatusID="25"></cfcase> <!--- WAB 2009/02/06 was 42 - don't know why - every site has 25 --->
	<cfcase value="u"><cfset feedback="Unsubscribe Response"><!--- wab 2005-10-04 changed from unsubscribe (which is status -11) ---><cfset CommStatusID="41"></cfcase>
	<cfcase value="d"><cfset feedback="Downloaded file"><cfset CommStatusID="60"></cfcase>
	<cfcase value="g"><cfset feedback="Clicked thru to Remote URL"><cfset CommStatusID="28"></cfcase>
	<cfcase value="e"><cfset feedback="Clicked thru to ElementID"><cfset CommStatusID="65"></cfcase>
	<cfcase value="s"><cfset feedback="Sent to a friend"><cfset CommStatusID="66"></cfcase>
	<cfdefaultcase><cfset feedback="Unspecified"><cfset CommStatusID="67"></cfdefaultcase>
</cfswitch>

<CFIF isDefined('attributes.cid') and isDefined('attributes.pid') and isNumeric(attributes.pid) and isNumeric(attributes.cid) and attributes.pid neq 0 and attributes.cid neq 0>


	<!--- 	
	WAB 2009/01/14   
		This is the original code before changes done to speed up remote.cfm


	<!--- ***************************************************************
	 UPDATE COMMDETAIL RECORD: if cid and PID are set we can update commDetail --->
	<CFQUERY NAME="getCommStatusID" datasource="#application.sitedatasource#">
		select commStatusID from commDetailstatus where name = N'#feedback#'
	</CFQUERY>	

	<!--- if there is no commDetailstatus record defined for this feedback then we should add one --->
	<CFIF getCommStatusID.recordCount neq 1>
		<CFQUERY NAME="insertStatus" datasource="#application.sitedatasource#">
			SET IDENTITY_INSERT CommDetailStatus ON 
			INSERT INTO [CommDetailStatus](CommStatusID,[Name], [Description], [StatsReport], [StatsReportOrder], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated])
			values (#CommStatusID#,N'#feedback#', N'#feedback#', N'#feedback#', 
			20, 100, getDate(), 100, getDate())		
			SET IDENTITY_INSERT CommDetailStatus OFF
		</CFQUERY>	
		<CFQUERY NAME="getCommStatusID" datasource="#application.sitedatasource#">
			select commStatusID from commDetailstatus where name = N'#feedback#'
		</CFQUERY>	
	</CFIF>
	
	<!--- check to see if their is a matching commDetail record --->
	<!--- 2008/07/03	GCC		Added with(nolock) to reduce unnecessary locking --->
	<CFQUERY NAME="checkIDPair" datasource="#application.sitedatasource#">
		select max(CommDetailID) as thisCommDetailID from commDetail with(nolock)
		where 	
			Commid = #attributes.cid# 
			and PersonID = #attributes.pID#
			and <cfif isdefined ("url.t") and url.t is 1>test = 1<cfelse>test = 0 </cfif>	
		
	</CFQUERY>

	<!--- if matching commDetail record found update it --->
	<CFIF checkIDPair.recordCount eq 1 and isNumeric(checkIDPair.thisCommDetailID)>
		<CFQUERY NAME="UpdateCommDetailRecord" datasource="#application.sitedatasource#">
			Update commdetail 
			  set feedback = N'#feedback#',
			  commStatusID = #getCommStatusID.commStatusID#,
			  lastUpdated = getDate(),
			  lastUpdatedBy = #attributes.pid#
			WHERE CommDetailID=#checkIDPair.thisCommDetailID#
			AND PersonID = #attributes.pid#
			and <cfif isdefined ("url.t") and url.t is 1>test = 1<cfelse>test = 0 </cfif>	
		</CFQUERY>
		<cfset caller.CommDetailID=checkIDPair.thisCommDetailID>
	</CFIF>	 

	<cfscript>
		// SWJ: 2005-08-17 Added this so that when they click thru their email status is set to -0.5 which is known good
		x = application.com.relayPLO.updatePersonEmailStatus(PersonID = attributes.pid);
	</cfscript>

	 --->

	 <!--- 
	 WAB 
	  --->

		<CFQUERY NAME="UpdateCommDetailRecord" datasource="#application.sitedatasource#">
			Update commdetail 
			  set feedback =  <cf_queryparam value="#feedback#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			  commStatusID =  <cf_queryparam value="#commStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			  lastUpdated = getDate(),
			  lastUpdatedBy =  <cf_queryparam value="#attributes.pid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			WHERE 
			<cfif isdefined("url.cd")>
			commdetailid =  <cf_queryparam value="#url.cd#" CFSQLTYPE="CF_SQL_INTEGER" >  and
			</cfif>
			CommID =  <cf_queryparam value="#attributes.cid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND PersonID =  <cf_queryparam value="#attributes.pid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and <cfif isdefined ("url.t") and url.t is 1>test = 1<cfelse>test = 0 </cfif>	
		</CFQUERY>

	<cfscript>
		// SWJ: 2005-08-17 Added this so that when they click thru their email status is set to -0.5 which is known good
		x = application.com.relayPLO.updatePersonEmailStatus(PersonID = attributes.pid);
	</cfscript>

 </CFIF>

