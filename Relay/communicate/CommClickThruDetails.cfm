<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
CommClickThruDetails

Accessed from the context menus

WAB 2009/07/08 Added selection stuff (copied from commCheckDetailsStatistics
NJH 2010/01/25 	LID 3014 - htmlEdit the frmPersonIDs value as this value could contain a url that needs to be htmlEdited. This is because
				the editors HTML edit the link and when attempting to save to a selection, the un-encrypted url is getting compared to the encrypted one,
				resulting in no people being returned.
PPB 2010/03/03 LID 3014 I have added the REPLACE function to the SQL cos this is still reported as a problem; ideally we should ensure that the #38; encryption doesn't get written to our table in the first place; the REPLACE will slow the query so might want to be removed at the point we're confident no new occurences are added and we clean up the existing data  
SSS 2010/04/19 LID 3014 Took the change above out because it was converting the sql to the wrong thing.
WAB 2013/03/04 Fixed error found in testing variable theQuerySnippet rather than theQuerySnippetSQL being used in clickThroughs query
--->
 
 
 <cfparam name = "frmCommID">
 <cfparam name = "frmEntityTypeID">
 <cfparam name = "frmEntityID">
 
	 <cfinclude template = "commCheckDetailsStatisticsFilter.cfm" >

	<cfsavecontent variable="theQuerySnippetSQL">
		<cfoutput>
		vpeople p
			inner join
		commdetail cd  on cd.personid = p.personid
		#filter.country#
			inner join 
		entityTracking et on (cd.commid = et.commid  or (et.entityTypeID =  <cf_queryparam value="#application.entityTypeID['Communication']#" CFSQLTYPE="CF_SQL_INTEGER" >  and et.entityid =  <cf_queryparam value="#frmCommid#" CFSQLTYPE="CF_SQL_INTEGER" > ))    
							and cd.personid = et.personid
			left join 
		Element E  on ET.EntityID = e.id and et.entityTypeid = 10 
			left join 
		Files F  on ET.EntityID = f.fileid and et.entityTypeid = 47

	where 
		cd.CommID =  <cf_queryparam value="#frmcommid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and et.entityTypeID =  <cf_queryparam value="#frmEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and et.entityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfif frmEntityTypeID is #application.entityTypeID['Communication']#>
		and toloc =  <cf_queryparam value="#frmToLoc#" CFSQLTYPE="CF_SQL_VARCHAR" >  
		</cfif>

		and #filter.TestInternalExternal#

		</cfoutput>
	</cfsavecontent>

<CFIF isdefined("selectionTask")>

	
	<CFSET frmtask= selectionTask>

	<!--- I'm submitting a form because of problems I encounter being two directories down. The next page doesn't really know where it is!!--->
	<CFOUTPUT>
	
	<FORM name="thisForm" method="post" action = "../selection/selectalter.cfm">
	<cf_encryptHiddenFields>
		<CF_INPUT type="HIDDEN" name="frmpersonids"  value="Select distinct p.personid FROM #thequerySnippetSQL#"> <!--- NJH 2010/01/25 LID 3014 - htmledit the query as it could contain a url that needs to be html edited. --->
		<CF_INPUT type="HIDDEN" name="frmtask"  value="#frmtask#">
		<CF_INPUT type="HIDDEN" name="frmruninpopup"  value="#frmruninpopup#">
	</cf_encryptHiddenFields>	
	</form>
	
	</cfoutput>
	<SCRIPT>
		window.document.thisForm.submit()
	</script>
	<CF_ABORT>


</cfif>

	
	<cfquery name="clickThroughs" datasource = "#application.siteDataSource#">
	SELECT 	
		distinct p.firstname, p.lastname, p.sitename,p.personid
	FROM
		#preserveSingleQuotes(theQuerySnippetSQL)#
	</cfquery>
	
	<cf_tableFromQueryObject queryobject ="#clickThroughs#"  useinclude=false>

