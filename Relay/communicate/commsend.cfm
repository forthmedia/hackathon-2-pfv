<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---
	2001-11-14 SWJ	Modified the layout
	25 Oct 2001		CPS	Amended to use new attributes on commdetail
	2000-07-24 WAB 	Made some mods to allow selections to be added to an existing selection
	1999-07-16 WAB 	when populating commdetail, set contactdetails to null (prevent sapce being used in database)
	1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid
	1999-01-28  WAB	Invalid email selection not appearing on selection list - so added insert into selectiongroup table for the invalid email selection
	1999-04-07	LW		Incorporated New Field, CommStatusID
	2002-11-26 SWJ	Added a method for sending immediately see frmSendAction on line 459
	2011-07-04	NYB	P-LEN024 various changes - including incorporation of sendForApproval functionality - rendering that file superfluous
	2011/07/27	NYB		P-LEN024 changed comm checkDetails.cfm to commDetailsHome.cfm
	2011/10/31	NJH	LID 7888 - moved the frmSendDate code inside the else statement as it's not needed when approving/disapproving communications
	2012-11-26	IH	Case 427937 cfparam frmPersonIDs instead of setting it to empty string

--->
<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<SCRIPT type="text/javascript">
<!--
// used when this runs in a popup window
	function closewindow(){
		window.close()
		}
//-->
</SCRIPT>

<cfparam name="frmCommID" default="">
<CFPARAM NAME="FreezeDate" DEFAULT=#Now()#>
<CFPARAM NAME="frmDoCounts" default = "true">
<cfparam name="frmSendAction" default="queue">
<cfparam name="frmPersonIDs" default="">
<cfparam name="frmSelectID" default="">
<cfparam name="frmNextPage" default="commDetailsHome.cfm">
<cfparam name="frmSubjectPrefix" default= "">
<cfparam name="frmFooter" default= "">
<cfparam name="frmPersonids" default="">
<cfparam name="frmCommSelectLID" default= "">
<cfparam name="frmTest" default= "No">
<cfparam name="frmResend" default= "No">
<cfparam name="frmReplyTo" default= "">
<cfparam name="frmshowLinkBackToCommunication" default = "false">
<cfparam name="frmHideTopHead" default="false">
<CFPARAM name="frmCallingTemplate" default="">
<CFPARAM name="useWrapper" default="false">
<CFPARAM name="stepNumber" default="">


<cfif frmSendAction eq "Approve" or frmSendAction eq "DisApprove">
	<cfinclude template="ApproveCommunication.cfm">
<cfelse>

	<!--- LID 7888 NJH 2011/10/31 - moved to inside this else statement from above as frmSendDate is not defined and needed when approving or disapproving communications --->
	<CFIF NOT IsDate(frmSendDate) and frmHideTopHead>
		<CFSET message = "That is not a valid date please select another.">
		<cfinclude template="commconfirm.cfm">
		<CF_ABORT>
	</CFIF>

	<CFIF NOT IsDate(frmSendDate) and not frmHideTopHead>
		<cfinclude template="commMenu.cfm">
		<CF_ABORT>
	</CFIF>


	<cfif isdefined("frmapprovedReleaseDate") and IsDate(frmapprovedReleaseDate)>
		<cfset frmSendDate = '#frmapprovedReleaseDate#' & ' '>
	</cfif>
	<cfif isdefined("frmapprovedReleaseDate_hr") and frmapprovedReleaseDate_hr NEQ "">
		<cfset frmSendDate = frmSendDate & '#frmapprovedReleaseDate_hr#' & ':'>
	</cfif>
	<cfif isdefined("frmapprovedReleaseDate_min") and frmapprovedReleaseDate_min NEQ "">
		<cfset frmSendDate = frmSendDate & '#frmapprovedReleaseDate_min#' & ':00'>
	</cfif>

	<!--- the application .cfm checks for communication privleges but
			we also need to check for select privledges
			we can rerun the qrycheckperms but NOTE we have now lost
			the application .cfm query variables.
	--->
	<CFSET securitylist = "SelectTask:read">
	<cfinclude template="/templates/qrycheckperms.cfm">

	<!--- returns query with all the details of the communication--->
	<cfif not isdefined("communication")>
		<cfset communication = application.com.communications.getCommunication (commid=#frmCommid#,personid=request.relayCurrentUser.personid,returnApprovalDetails = true,returnSelectionDetails = true)>
	</cfif>

	<!--- check for updates by someone else --->
	<CFIF DateDiff("s",communication.LastUpdated,frmLastUpdated) IS NOT 0>
			<CFSET message ="The communication could not be sent since it has been changed by another user.">
			<CFSET message = message & " <A HREF='commDetailsHome.cfm?frmCommID=#frmCommID#'>Click here</A> to continue.">

			<cfoutput>#application.com.security.sanitizeHTML(message)#</cfoutput>
			<CF_ABORT>
	</cfif>

	<!---
	WAB 2005-05-04 Moved to an earlier template so that personids are saved and user can exit out of the process
	at an earlier stage without losing the details (particularly important if comm isn't approved)

	<CFIF frmPersonIDs is not "">
	<!--- 	puts all the records in commdetail
			no longer worry about copying selections to commdetail until tskcomsend is run
	--->

		<CF_populateCommDetail
			commid = #frmCommID#
			personids = #frmpersonids#
			resend = false	>
	</cfif>
	 --->

	<cfif frmSendAction eq "now">
		<cfset getSelectionDetails = application.com.communications.getSelectionDetails(commid=frmCommID,selectionTypes="external")>
		<cfset directToNextPage="false">
		<cfset includeNextPage="false">
		<cfset includeMessage="false">
		<cfset frmSelectID = valuelist(getSelectionDetails.selectionid)>
		<cfparam name="frmPersonIDs" default="">
		<cfinclude template="#thisDir#/sendCommunication.cfm">
	<cfelse>
		<cfif frmSelectID is "">
			<CFQUERY NAME="updateCommunication" DATASOURCE="#application.SiteDataSource#">
			UPDATE Communication
			   SET 	 senddate = #CreateODBCDateTime(frmSendDate)#,
					sent = 0,
			       CommStatusID = 0
			WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
			  AND LastUpdated =  <cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</CFQUERY>
		<cfelse>
			<CFQUERY NAME="updateCommunication" DATASOURCE="#application.SiteDataSource#">
			UPDATE Commselection
			   SET 	 senddate = #CreateODBCDateTime(frmSendDate)#,
					sent = 2
			WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and selectionid  in ( <cf_queryparam value="#frmSelectID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>

		</cfif>

		<CFQUERY NAME="getCommInfo" DATASOURCE="#application.SiteDataSource#">
			select * from
				communication c , LookupList AS ll
			 where
				 ll.LookupID = c.CommFormLID
				and CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>


		<cfif frmDoCounts>
			<cfset 	thisCommFormLID = getCommInfo.commFormLID>
			<cfset selectIDList = frmSelectID>
			<cfinclude template="#thisDir#/qryCommCounts.cfm">
		</cfif>


		<CFIF getCommInfo.RecordCount IS NOT 0>


			<cf_head>
			<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>
			</cf_head>



			<cfparam name="frmHideTopHead" default="false">
			<cfif not frmHideTopHead>
				<cfoutput></cfoutput>
				<cfinclude template="#thisDir#/commtophead.cfm">
			</cfif>


			<CENTER><TABLE BORDER=0 BGCOLOR="#ff0000" CELLPADDING="3" CELLSPACING="0" WIDTH="600">

				<TR><td colspan="3" align="center" class="Submenu"><strong>Step #StepNumber#: Complete. Communication is queued for sending<br></strong></Td></TR>

				<CFSET DefaultSend = #DateAdd("d",1,Now())#>
				<FORM ACTION="" METHOD="POST">
		 		<TR>
				<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>
					<B>Date to send:</B>
					</TD>
					</TR>
				<TR><TD WIDTH=15>
						&nbsp;
					</TD>
					<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2>
					<CFOUTPUT>#DateFormat(getCommInfo.SendDate,"dd-mmm-yy")#</CFOUTPUT></TD>
				</TR>
				</FORM>
				<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH="100%" HEIGHT=3 ALT="--------------" BORDER="0"><BR></CFOUTPUT></TD></TR>
				<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>
						<B>Communication:</B>
					</TD>
				</TR>

				<TR><TD>
						&nbsp;
					</TD>
					<TD VALIGN="TOP" ALIGN="LEFT">
						Subject:
					</TD>
					<TD VALIGN="TOP" ALIGN="LEFT">
						<CFOUTPUT>#htmleditformat(getCommInfo.Title)#</CFOUTPUT>
					</TD>
				</TR>
				<TR><TD>
						&nbsp;
					</TD>
					<TD VALIGN="TOP" ALIGN="LEFT">
						Objective:
					</TD>
					<TD VALIGN="TOP" ALIGN="LEFT">
						<CFOUTPUT>#htmleditformat(getCommInfo.Description)#</CFOUTPUT>
					</TD>
				</TR>
				<CFIF getCommInfo.CommFormLID IS NOT 4>
					<TR><TD>
							&nbsp;
						</TD>
						<TD VALIGN="TOP" ALIGN="LEFT">
							Via:
						</TD>
					<!--- not "Download"
						add a space after each comma in the list of
						how the communication will be sent --->

						<TD VALIGN="TOP" ALIGN="LEFT">
							<CFOUTPUT>#htmleditformat(getCommInfo.ItemText)#</CFOUTPUT>
						</TD>
					</TR>

				</CFIF>
				<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH="100%" HEIGHT="3" ALT="----------------" BORDER="0"><BR></CFOUTPUT></TD></TR>
				<CFIF Variables.SelectTitleList IS NOT "">
					<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>
						<B>Selection(s):</B>
						</TD>
					</TR>
					<TR>
						<TD>&nbsp;</TD>
						<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2>
							<CFOUTPUT>
								<p>#htmleditformat(Variables.SelectTitleList)#</p>
								<!--- tagPeople always exists --->
								<p>A total of #htmleditformat(Variables.TagPeopleCount)# individual(s) are tagged</p>
								<!--- feedback counts depends on type of communciation being sent --->
								<p>#htmleditformat(Variables.FeedbackCounts)#</p>
		<!--- 						<BR>#getSelectionInfo.emailPeople# Individual(s) with email addresses
								<BR>#getSelectionInfo.faxPeople# Individual(s) with fax numbers
								<BR>#getSelectionInfo.bothPeople# Individual(s) with both --->
							</CFOUTPUT>
						</TD>
					</TR>
				<CFELSE>
					<TR><TD COLSPAN="3" ALIGN="CENTER"><P>A related selection could not be found.<BR></TD></TR>
				</CFIF>

				<CFIF getCommInfo.CommFormLID IS NOT 4>
					<CFOUTPUT>
					<TR><TD COLSPAN="3" ALIGN="CENTER"><IMG SRC="/images/misc/rule.gif" WIDTH="100%" HEIGHT="3" ALT="----------------" BORDER="0"><BR></TD></TR>
				<!---
					WAB removed 2005-05-04 - this no longer happens
					<TR><TD COLSPAN="3" VALIGN="BASELINE">
						<P>While it is not possible to add/remove individuals from the communication
						recipient list, you may update/add their fax and email details to ensure that a greater percentage of the tagged individuals will be reached.
						A selection containing all the individuals with an invalid email address &/or and invalid fax number
						has been created called &quot;<CFOUTPUT>#feedbackselectionTitle#</CFOUTPUT>&quot; to assist you
						in updating the information.
						</TD>
					</TR> --->

					<TR><TD COLSPAN="3" ALIGN="CENTER"><A HREF="commDetailsHome.cfm?frmcommid=#frmcommid#">Click here </A>to see details of your communication </TD></TR>
					</CFOUTPUT>

				</CFIF>


			<CFELSE>
				<CENTER>
				<P>The requested communication could not be found.
				</CENTER>
			</CFIF>

		<!--- </TD></TR> --->
		</TABLE>
		</CENTER>


		<cfif frmruninpopup IS "True">
			<CENTER>
			 <P><A HREF="JavaScript: closewindow();">close window</A>
			</CENTER>
		</CFIF>




	</cfif>

	<cfif isdefined("frmnextpage") and len(frmnextpage) gt 0>
		<cfinclude template="#thisDir#/#frmnextpage#">
	</cfif>

</cfif>

