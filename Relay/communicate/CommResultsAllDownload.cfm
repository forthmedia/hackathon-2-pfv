<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Downloads a table of all communications for a particular commID --->
<!---  
	2000-08-22  WAB adjusted fields in the download (added country)
	1999-02-03  WAB adjusted main query (and in other templates), took out lookuplist table and hard coded in fax and email  
	Adapted by SWJ on 1999-01-31 from
	 an original by William Bibby 1998-07-23 
wab 2006-03-23 	
 
NJH 2008/07/09 Bug Fix T-10 Issue 682. The file has been replaced by CommResultsAll.cfm (pass in openAsExcel = true)
2012-10-04	WAB		Case 430963 Remove Excel Header  
 --->


<CFINCLUDE TEMPLATE="commresultsquery.cfm">


<CFSET delim = "#Chr(9)#">


<!--- save as file --->
<CF_TemporaryFileName FileName="CommDownload" Extension = "txt">


<CFLOOP QUERY="CommunicationResults">
	<CFIF #CurrentRow# IS 1>
		<!--- add a header row as the first row of the file --->
		<CFSET header = "Fullname#delim#Email#delim#PersonID#delim#LocationID#delim#Country#delim#sitename#delim#Commid#delim#SentVia#delim#Result#delim#Status">			
					

		<CFFILE ACTION="WRITE"
			FILE=#dosFilePath#
			OUTPUT="#header#">
	
		
	</CFIF>	

	<CFSET tempResult = "#EmailFeedback#  #FaxFeedback#">
<!--- 	<CFSET SentVia = "#IIf(Emailstatus is not -2,DE(emailtext),DE(''))# #IIf(Faxstatus is not -3,DE(faxtext),DE(''))#"> --->
	<CFSET SentVia = "#IIf(byemail is 2,DE('Email'),DE(''))# #IIf(byfax is 3,DE('Fax'),DE(''))#">
	<CFSET mm = "#Fullname##delim##Email##delim##PersonID##delim##LocationID##delim##Isocode##delim##sitename##delim##url.frmCommid##delim##SentVia##delim##tempResult##delim##Result#">

	<!--- append data to file --->
	<CFFILE ACTION="APPEND"
			FILE=#dosFilePath#
			OUTPUT="#mm#">
</CFLOOP>	
			

<CFCONTENT TYPE="application/msexcel"
           FILE=#dosFilePath#
           DELETEFILE="No">
		   




