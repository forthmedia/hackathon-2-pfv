<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Outputs a graph of email statistics

WAB 2006-09-29  modifed so that uses same query as other statistics.  Didn't add links to allow changing of filtering
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object

 --->



	
<cfset communication = application.com.communications.getCommunication (frmcommid,request.relaycurrentuser.personid)>


<cfif not isDefined("filtercode")>
	<cfif communication.hasBeensentExternally() is 1>
		<cfset filtercode = "E">
	<cfelseif communication.hasBeensentInternally()is 1>
		<cfset filtercode = "I">
	<cfelse>
		<cfset filtercode = "T">
	</cfif>
</cfif>
<cfparam name="frmCountryid" default = "">

<cfset commStatistics = application.com.communications.getCommunicationStatisticsQuerys(commid =frmCommID, filtercode = filtercode, countryid = frmCountryID)>


<cfoutput>

<cf_head>
	<cf_title>Phr_Sys_AnalyseCommStatus</cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Phr_Sys_CurrentStatiiCommunication #htmleditformat(communication.title)# (#htmleditformat(frmCommID)#)</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu"><a href="javascript:history.back()" class="SubMenu">Phr_Sys_Back</a>&nbsp;&nbsp;</td>
	</TR>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorderSidesOnly">
	<thead>
	<tr>
		<th>Phr_Sys_CurrentStatus</th>
		<th>Phr_Sys_Count</th>
	</tr>
	</thead>
	<cfloop query="commStatistics.EmailStatistics">
	<tr>
				<td><a href="/communicate/CommResultsbyStatus.cfm?frmCommid=#frmCommID#&status=#Status#&filtercode=#filtercode#&statusView=mostSignificant">#htmleditformat(StatsReport)#</a></td>		
		<td>#htmleditformat(MostSignificantCount)#</td>
	</tr>
	</cfloop>
</table>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorderSidesOnly">
	<tr>
	<td width="30px">&nbsp;</td>
	<td><cfgraph type="HORIZONTALBAR"
           query="commStatistics.EmailStatistics"
           valuecolumn="MostSignificantCount"
	           itemcolumn="StatsReport"
           title="Current Statii"
           titlefont="Arial"
           fileformat="Flash"
           graphwidth="630"
           depth="15"
           colorlist="Red,Green,Blue,yellow,orange,coral"></CFGRAPH></td>
	</tr>
</table>


	<cfif commStatistics.emailStatistics.querycachedAt is not "">
	
			<TR>
			<TD>Statistics were cached #htmleditformat(commStatistics.emailStatistics.querycachedAgo)# ago <a href="?frmcommid=#frmCommid#&filtercode=#filtercode#&frmCountryID=#frmCountryID#&refreshCommStats=true">Refresh</a></TD>
			</TR>
					
	</cfif>				





</cfoutput>

