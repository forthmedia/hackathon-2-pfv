<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2011-07-04	NYB		P-LEN024 - created
2011/09/06	WAB		LID 7702 Problems changing links (if original link had ? of other regExp control character in it)
2011/11/08	WAB		use argument collections when calling certian functions so that HTML does not appear in the debug
2011/11/17	WAB		bug when dealing with [[download]] - getting fileID wrong
					Couldn't deal with new functions or new arguments to existing functions
					Therefore modified the code to be more "Generic"
2012-03-19	WAB		Fixed Problems with parsing relayFunctions if using unnamed parameters
2012-10-16	WAB		CASE 430351 - Problems with links containing old style merge fields (such as [[gotoElementID]]).  
2012/11/13	YMA		CASE:431951 removed HTMLformat around frmcontents to fix issue where images in links do not render in html
2013-04-30	WAB		Added support for Area tags
--->

<cfset positionInt = "1">

<cfif not isdefined("communication")>
	<cfset communication = application.com.communications.getCommunication (commid = #frmcommid#)>
</cfif>


<cfset content = communication.getEmailContent()>
<cfset frmMsg_HTML = content.html>

<cfset linksArray = communication.getArrayOfLinksInContent()>

<cfloop index="i" from="1" to="#ArrayLen(linksArray)#">	
	<cfset tagname = linksArray[i].tagname>
	<cfset origLink = linksArray[i].STRING>
	<cfset newLink = origLink>

	<cfif structKeyExists (form,"frm#i#Contents")>
		<cfset frmContents = form["frm#i#Contents"]>
		<cfset frmContentsorig = form["frm#i#Contentsorig"]>
		<cfset newLink = replacenocase(newLink,'>#frmContentsorig#<','>#frmContents#<')> <!--- Was rereplace, but fell over if content contained regExp control characters (such as ?) --->
	</cfif>
	
	<cfif isDefined("frm#i#Link")>
		<cfset frmLink = trim(form["frm#i#Link"])>
		<cfset frmLinkorig = form["frm#i#Linkorig"]>
		<!--- 
			WAB LID 7702 2011/09/06 this replace was not working if frmLinkorig had special regExp characters in it (such as ?)
				Think that we can just do a plain replace and not worry about the quotes
				Also added an if statement so 
			<CFSET regExp = "href=([""'])#frmLinkorig#([""'])">
			<cfset newLink = rereplacenocase(newLink,regExp,"href=""#frmLink#""","one")>		
		--->
		<cfif compareNoCase(frmLinkorig,frmLink) is not 0>
			<cfif frmLinkOrig is "">
			    <!--- WAB 2012-02-03 Lenovo manage to have links with no existing href, deal! --->
			    <cfif newLink contains "href=""">
				<cfset newLink = replacenocase(newLink,'href=""','href="#frmLink#"',"one")>		
			    <cfelse>
				<cfset newLink = replacenocase(newLink,"<#tagname#","<#tagname# " & 'href="#frmLink#"',"one")>		
			    </cfif>
			<cfelse>
			    <cfset newLink = replacenocase(newLink,frmLinkorig,frmLink,"one")>		
			</cfif>
		</cfif>

		<cfif isDefined("frm#i#Tracked")>
			<cfset frmTracked = form["frm#i#Tracked"]>
		<cfelse>
			<cfset frmTracked = "no">
		</cfif>

		<CFIF structkeyexists(linksArray[i].ATTRIBUTES,"track")>
			<CFSET regExp = "track=([""'])([^""']*)([""'])">
			<cfset newLink = rereplacenocase(newLink,regExp,'track="#frmTracked#"',"one")>		
		<CFELSE>
			<!--- 2012/11/13	YMA	CASE:431951 	removed HTMLformat around frmcontents to fix issue where images in links do not render in html --->
			<cfset newLink = replacenocase(newLink,'<#tagname#','<#tagname# track="#frmTracked#" ')>
		</CFIF>

	<cfelse>
		<cfset frmLinkType = form["frm#i#LinkType"]>
			<!--- WAB 2011/11/18 reformulated for flexibility and extensibility --->
			<!--- look through all the form fields for ones which might contain additional parameters --->
			<!--- WAB 2012-03-08 CASE 427040 added in the [^0-9] otherwise when looking for item 1 it was picking up 10,11,12,13 as well!--->
			<cfset regExp = "frm#i#([^0-9].*?)(?=(,|\Z))"> <!--- this finds all fields connected with this link --->
			<cfset groups = {1 = "parameterName", 2=2}>
			<cfset findFields =application.com.regExp.refindAllOccurrences(regExp, form.fieldNames,groups)>
			<cfset fieldsToIgnore = "Name,nameOrig,Contents,ContentsOrig,LinkType">
			<cfset args = {}> 
			<cfset keyList ="">
			<cfloop array = "#findfields#" index="thisField">
				<cfif not listFindNoCase(fieldsToIgnore,thisField.parameterName)>
					<cfset args[thisField.parameterName] = form [thisField.string]>
					<cfset keylist = listAppend(keyList,thisField.parameterName)>
				</cfif>
			</cfloop>


			<cfif structKeyExists (args,"NotFunction")>
				<!--- WAB 2012-10-16 CASE 430351 - fix problems with old style merge fields (ie not functions), was just dealing with [[download]] but now deals with any --->		
				<!--- download is a special hardcoded case because it is not a function
					ToDo we need to create a download link merge function
				 --->
				<cfset keylist = listDeleteAt(keylist,listfindnocase(keylist,"notFunction"))> 
				<cfset argString = application.com.structureFunctions.convertStructureToNameValuePairs(struct=args,delimiter="&",qualifier="",keyList = keyList,includeNulls=false)>
				<cfset functionString = "[[#frmLinkType#]]&#argString#">
				<cfset newLink = REReplaceNoCase(newLink,"(href=[""']*)([^""']*)([""']*)", "\1#functionString#\3")>
			<cfelse>
				<cfif keyList contains "unnamed_">
					<!--- Unnamed Arguments--->
					<cfset argString = "">
					<cfloop list="#keyList#" index="key">
						<cfset argString = listAppend(argString,"'" & args[key] & "'")>
					</cfloop>
				<cfelse>
					<!--- Normal named arguments--->
					<cfset argString = application.com.structureFunctions.convertStructureToNameValuePairs(struct=args,delimiter=",",qualifier="'",keyList = keyList,includeNulls=false)>
				</cfif>

				<cfset functionString = "[[#frmLinkType#(#argString#)]]">
				<cfset newLink = REReplaceNoCase(newLink,"(href=[""']*)([^\]]*)(\]\])([""']*)", "\1#functionString#\4")>
			</cfif>

	</cfif>

	<cfset frmName = form["frm#i#Name"]>
	<cfset frmNameorig = form["frm#i#Nameorig"]>

	<!---2011/08/04 GCC only add the name attribute if it has data in it otherwise every link is displayed as an anchor in the WYSIWYG --->
	<cfif len(trim(frmName)) gt 0>
		<cfset replaceNameString = 'name="#frmName#" '>
	<cfelse>
		<cfset replaceNameString = ''>
	</cfif>

	<CFIF structkeyexists(linksArray[i].ATTRIBUTES,"name")>
		<CFSET regExp = "name=([""'])#frmNameorig#([""'])">
		<cfset newLink = rereplacenocase(newLink,regExp,replaceNameString,"one")>		
	<CFELSE>
		<cfset newLink = rereplacenocase(newLink,'<a(rea)* ','<a\1 ' & replaceNameString)>
	</CFIF>

	<cfset argumentCollection = {string = frmMsg_HTML,substring1 = origLink,substring2 = newLink,start = positionInt}>
	<cfset frmMsg_HTML = application.com.globalFunctions.rwReplacenocase(argumentCollection = argumentCollection)>
	<cfset positionInt = findnocase("#newLink#","#frmMsg_HTML#",positionInt)>
	<cfset positionInt = positionInt + len(newLink)>
</cfloop>


	<cfset argumentCollection = {commid = frmcommid,HTMLBody = frmMsg_HTML}>
	<cfset saveContentResult = communication.SaveCommContent(argumentCollection = argumentCollection)>

