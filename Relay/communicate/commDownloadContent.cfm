<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2011-07-04	NYB		P-LEN024 - created
--->




<cfoutput>
	<script language="javascript">
		createFile = function(CommID) {
			page = '/webservices/callWebService.cfc?'
			parameters = 'method=callWebService&webServiceName=communicationsWS&methodName=createCommContentFile&CommID='+CommID+'&_cf_nodebug=true&returnFormat=json';
			
			var myAjax = new Ajax.Request(
				page,
				{
						method: 'post', 
						parameters: parameters, 
						evalJSON: 'force',
						debug : false,
						asynchronous:true,
						onComplete: function (transport) {
							json = transport.responseText.evalJSON();					
							filePath = json.FILEWEBPATH;
							window.open(filePath,'Download');
							//window.close();
							}
				}
			)
		}	
		createFile(#jsStringFormat(url.CommID)#);
	</script>

		<p class="message">phr_sys_comm_FileIsDownloading</p>
	</cfoutput>
