<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2011-07-04		NYB		P-LEN024
2011-11-08		NYB		LHID8100 - bit of a hack
2012-01-24 		NYB		Case#424774 wrapped checkTabs around new checkTabsResizeIframe function with setTimeout
2012/03/02		RMB		CASE : 425952 add a cflocation to reload back to start of comms if "Save and Continue Later" is clicked

2012-12-05		PPB		Case 429559 warn the user if moving away from a dirty tab in edit communication
2013-01			WAB		Sprint 15&42 Comms. 	Converted to use communication.cfc object and some ajax
2012-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-02-26		WAB		Case 433858 removed changes from Case 427937 since were causing other problems.  They were added to support 'Send Existing Comm to a Selected Person', but I have made a change so that this function does not use this template at all
2013-05-14		WAB		Add check for merge field errors and display as part of status
2013-05			WAB		Convert all tabs to load via Ajax, resize tab block to fit page, reduce message block above tabs.
2014-10-01		RPW		CORE-679 Comms - view attachment link does nothing
--->
<cfsetting showdebugoutput="false">  <!--- This prevents scrollbars appearing on the main page - just want scroll bar within the tab --->
<cfset application.com.request.hidefooter()>

<cf_title>Communication Details</cf_title>

<CFAJAXIMPORT TAGS="cflayout-border,cflayout-tab,cfform,cfgrid">
<cf_includeJavascriptOnce template = "/javascript/divOpenCloseV2.js">
<cf_includeJavaScriptOnce template = "/javascript/checkObject.js"><!--- 2012-12-05 PPB Case 429559 has isDirty function --->


<cf_includejavascriptOnce template ="/javascript/contextMenu.js" >
<!--- 2014-10-01		RPW		CORE-679 Comms - view attachment link does nothing - added openWin --->
<cf_includejavascriptOnce template ="/javascript/openWin.js" >
<cf_includejavascriptOnce template ="/communicate/communication.js" >
<div status="false" onmouseover="fnChirpOn(event);" onmouseout="fnChirpOff(event);"  id="oContextMenu"  class="menu"></div>


<SCRIPT type="text/javascript">

checkFormcommconfirm = function(sendAction) {
	var form = document.commConfirmForm;
	if (form.frmapprovedReleaseDate) {
		form.frmSendDate.value=form.frmapprovedReleaseDate;
	}
	if (sendAction == 'Approve' && form.frmapprovedReleaseDate.value == '' ) {
		alert('Please enter a date when the communication can be sent') ;
	} else {
		form.frmAction.value=sendAction;
		form.frmSendAction.value=sendAction;
		form.submit();
	}
}


setTabCompleted = function (tabName) {
		var tabImg=document.getElementById(tabName + "TabTitleImg");
		tabImg.src = tabImg.src.replace(/step([0-9])\.png/, "step$1-completed.png");
}

</SCRIPT>

<cfparam name="frmCommID" default="0">
<cfparam name="defaultFrmNextPage" default="Setup">
<cfparam name="FrmNextPage" default="">
<cfparam name="readonly" default="false">
<cfparam name="action" default="">
<cfset frmHideTopHead = "true">



<cfif structkeyexists(form,"frmCallingTemplate")>
	<cfset variables.frmCallingTemplate = form.frmCallingTemplate>
<cfelse>
	<cfif structkeyexists(url,"frmCallingTemplate")>
		<cfset variables.frmCallingTemplate = URLDecode(listlast(url.frmCallingTemplate))>
	</cfif>
</cfif>

<cfif structkeyexists(form,"frmCallingTemplate") and fileexists("#GetDirectoryFromPath(GetCurrentTemplatePath())##replacenocase(variables.frmCallingTemplate,'.cfm','Add.cfm')#")>
	<cfinclude template="#replacenocase(variables.frmCallingTemplate,'.cfm','Add.cfm')#">
</cfif>

<cfif frmCommID neq 0>
	<cfset communication = application.com.communications.getCommunication (commid = listlast(frmcommid),personid = request.relaycurrentuser.personID)>
	<cfset checkIfCommunicationIsBeingSent = communication.checkIfCommunicationIsBeingSent()>
	<cfset pagetitle = "Communication #Communication.commid#.  #Communication.title# (#communication.Description#)">
	<cfset tabTitle = "Comm #Communication.commid#">
	<cfset useMessage = communication.useMessage()>
<cfelse>
	<cfset pagetitle = "New Communication">
	<cfset tabtitle = "New Comm">
	<cfset useMessage = application.com.communications.useMessage()>
</cfif>

<cf_includejavascriptonce template="/javascript/extExtension.js">
<cfoutput>
<script>
updateTab ({tabTitle:'#jsStringFormat(tabTitle)#'})
</script>
</cfoutput>

<cfif isdefined("communication") and len(communication.commid) eq 0>
	<cfoutput><p>Communication #frmCommID# can not be found</p></cfoutput>
	<CF_ABORT>
</cfif>

<CF_RelayNavMenu pagetitle="#pageTitle#" thisDir="communicate">
	<!--- <CF_RelayNavMenuItem MenuItemText="Communication Listing" CFTemplate="commCheck.cfm" target="mainSub"> --->
</CF_RelayNavMenu>



<!--- NJH for IE 8 reasons, I had to but a styling for the default message class here in commDetailsHome.. it wasn't applying when the page loaded --->
<cf_head>
	<style>
		<cfif useMessage>.default {color:#B3B3B3;}</cfif>
		th {text-transform:none !important;}
	</style>
</cf_head>


<!--- START Create layoutareasListStuct --->
<!--- LHID8100 - added PreferredWidth to query --->
<cf_querySim>
	layoutareas
	stepName,CFMFile,useWrapper,AreaHidden,AreaDisabled,Icon,PreferredWidth
	Setup|commheader.cfm|false|false|false| |62
	Content|commFile.cfm|false|false|true| |74
	Links|commLinks.cfm|false|false|true| |58
	<cfif useMessage>Message|commMessage.cfm|false|false|true| |80</cfif>
	Preview|previewEmailContent.cfm|false|false|true| |74
	Selections|commselection.cfm|false|false|true| |87
	Send|commconfirm.cfm|false|false|true| |57
	Statistics|commCheckDetailsStatistics.cfm|false|true|false|statistics.png|82
</cf_querySim>

<!--- filter out the empty string that is caused by the cfif statement in querySim --->
<cfquery name="layoutareas" dbtype="query">
	select * from layoutareas where stepName != ''
</cfquery>

<cfset queryAddColumn (layoutareas,"rowNumber","integer",#arrayNew(1)#)>
<cfloop query="layoutareas">
	<cfset querySetCell(layoutareas,"rowNumber",currentRow,currentRow)>
	<cfif trim(icon) is "">
		<cfset querySetCell(layoutareas,"icon","step"&currentRow&".png",currentRow)>
	</cfif>
</cfloop>

<!--- END Create layoutareasListStuct --->
<cfset layoutareasBystepName = application.com.structureFunctions.queryToStruct(query=layoutareas,key="stepName")>
<cfset layoutareasByCFMFile = application.com.structureFunctions.queryToStruct(query=layoutareas,key="CFMFile")>


<!--- <cf_includeJavascriptOnce template="/javascript/verifyEmailV2.js"> --->

<!--- NJH 2011/08/01 have to call in the ckeditor files here, in the parent page for the content tab (ie.commFile.cfm)--->
<cf_includeJavascriptOnce template="/ckeditor/ckeditor.js">
<cf_includeJavascriptOnce template="/ckfinder/ckfinder.js">

<cf_includeJavascriptOnce template="/javascript/prototypeSpinner.js">
<cf_includejavascriptOnce template ="/javascript/contextMenu.js" >
<cf_includejavascriptOnce template ="/javascript/relayTagEditor.js" >
<cf_includejavascriptOnce template ="/javascript/textAreaControlBar.js" >
<cf_includeJavascriptOnce template="/javascript/limitTextArea.js" translate="true"> <!--- NJH 2013/02/12 Sprint 3 item 105--->
<cf_includejavascriptOnce template ="/javascript/rwFormValidation.js" translate="true">
<cf_includejavascriptOnce template ="/javascript/fileDownload.js" >

<!--- some javascript for commFile.cfm --->
<cfinclude template="/communicate/commJavascript.cfm">

<cffunction name="setLayoutQueryCell">
	<cfargument name="stepName">
	<cfargument name="column">
	<cfargument name="value">

	<cfset QuerySetCell(layoutareas, column, value ,layoutareasBystepName[stepname].rownumber)>
</cffunction>

<cffunction name="setIconComplete">
	<cfargument name="stepName">

	<cfset setLayoutQueryCell(stepname,"Icon",reReplaceNoCase(layoutareasBystepName[stepname].Icon,"([0-9]*)\.","\1-completed."))>

</cffunction>

<cffunction name="setAreaDisabled">
	<cfargument name="stepName">
	<cfargument name="value" type="boolean" default="true">

	<cfset setLayoutQueryCell(stepname,"AreaDisabled",value)>

</cffunction>


<cffunction name="setAreaHidden">
	<cfargument name="stepName">
	<cfargument name="value" type="boolean" default="true">

	<cfset setLayoutQueryCell(stepname,"AreaHidden",value)>

</cffunction>

<cfif frmCommID neq 0>

	<cfset setIconComplete("setup")>

	<cfif communication.canBeEdited() and communication.hasRIGHTSTOEDIT()>
		<cfset setAreaDisabled("Content",false)>
		<cfif communication.HasEmailContent() and (structkeyexists(form,"FRMEMAILTEMPLATE") eq "false" or form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")>
			<cfset setAreaDisabled("Links",false)>
		</cfif>
	<cfelse>
		<cfif communication.hasBeenSentLive()>
			<cfif useMessage>
				<cfset setAreaDisabled("Message",true)>
			</cfif>
			<cfset setAreaDisabled("Content",true)>
			<cfset setAreaDisabled("Links",true)>
		</cfif>
	</cfif>

	<!--- if we are using message and we have message content or we're not sending a message .....or if we're not using messages and we have email content... --->
	<cfif ((useMessage and (communication.hasMessageContent() and communication.sendMessage) or not communication.sendMessage) or (not useMessage and communication.HasEmailContent())) and (structkeyexists(form,"FRMEMAILTEMPLATE") eq "false" or form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")>
		<cfif useMessage>
			<cfset setIconComplete("Message")>
		</cfif>
		<cfset setAreaDisabled("Selections",false)>
		<cfset setAreaDisabled("Preview",false)>
		<cfset setAreaDisabled("Send",false)>
	</cfif>

	<cfif communication.HasEmailContent() and (structkeyexists(form,"FRMEMAILTEMPLATE") eq "false" or form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")>
		<cfset setIconComplete("Content")>
		<cfif useMessage and not communication.hasBeenSentLive()>
			<cfset setAreaDisabled("Message",false)>
		</cfif>
	</cfif>

	<CFIF communication.hasExternalSelection() gt 0>
	 	<cfset setIconComplete("Selections")>
	</CFIF>

	<cfif communication.LinksReviewed >
		<cfset setIconComplete("Links")>
	</cfif>

	<cfif NOT communication.hasLink()>
		<cfset setAreaDisabled("Links")>
	</cfif>

	<cfif communication.PreviewReviewed>
		<cfset setIconComplete("Preview")>
	</cfif>

	<cfif not communication.isContentOK()>
		<cfset setAreaDisabled("Send",True)>
	</cfif>

	<cfif communication.HASBEENsentExternally()>
		<cfset setIconComplete("Send")>
		<cfif not structkeyexists(url,"frmNextPage")>
			<cfset frmNextPage = "Statistics">
		</cfif>
	</cfif>

	<cfif communication.HASBEENsentExternally() or communication.HASBEENsentInternally() or communication.SENT or communication.hasTestCommDetailRecords()>
			<cfset setAreaHidden("Statistics",false)>
	</cfif>

</cfif>

<!--- RMB - 2012/03/02 CASE : 425952 --->
<cfif isdefined("frmRequestedAction") AND frmRequestedAction EQ 'Save'>
		<cflocation url="/communicate/commcheck.cfm" addtoken="false">
</cfif>


<!--- START:  Layout Area --->

<cfif frmCommID neq 0>
	<cfif communication.recordcount gt 0>

		<cfif structkeyexists(form,"frmCallingTemplate")>
			<cfset frmNextPage = layoutareas.stepName[layoutareasByCFMFile[variables.frmCallingTemplate].rownumber+1]>
			<!--- If the contents does not contain any links then go straight to the Preview tab - ?? this is going to need some further thought --->
			<cfif frmNextPage is "links" and not communication.hasLink()>
				<cfset frmNextPage = layoutareas.stepName[layoutareasByCFMFile[variables.frmCallingTemplate].rownumber+2]>
			</cfif>

		</cfif>

		<cfset status = communication.getStatusMessageAndNextPage(nextPage = frmNextPage)>


		<cfif not StructKeyExists (status,"nextPage")>
			<cfif frmCommID neq 0 and (communication.isoktosendnow() or communication.hasExternalSelection()) and communication.sent eq 0 and communication.previewReviewed IS 1>
				<cfset frmNextPage = "Send">
			<cfelse>
				<cfset frmNextPage = defaultFrmNextPage>
			</cfif>
		<cfelse>
			<cfset frmNextPage = status.nextPage>
		</cfif>


<cfif isdefined("message") and len(message) gt 0>
	<CFOUTPUT>
		<table width="100%">
			<tr><td align="center">
			<table class="message">
			<tr>
				<td <cfif left(message,6) eq "<table">align="center"</cfif>>
					#application.com.relayUI.message(message = message, type="infoblock",closeafter=10,callback="resizeTabPanel()")#
				</td>
			</tr>
			</table>
			</td></tr>
		</table>
	</CFOUTPUT>
</cfif>

		<cfif status.message is not "">
			<CFOUTPUT>
				<TABLE border="0" width="100%">

				<tr><td align="center">

					<TABLE align="center" width="300" id="CommDetailsHomeInfoArea">
					<tr>
						<td align="center">
							#application.com.relayUI.message(message = status.message, type=status.type,callback="resizeTabPanel()")#

						</td>
					</tr>

				</table>
				</td></tr>
				</table>
			</CFOUTPUT>
		</cfif>

<!---  WAB 2013-05 Remove the information block to create more real estate.  Important status messages generated by new getStatusMessageAndNextPage() function
		<TABLE border="0" width="100%">

		<tr><td align="center">
			<TABLE CLASS="withBorder" align="center" width="300" id="CommDetailsHomeInfoArea">
<!--- 					<TR><TH class="label" colspan="2">#htmleditformat(communication.Title)# (#htmleditformat(communication.Description)#) (#htmleditformat(communication.commid)#)</TH></TR> --->
<!---
					<TR>
						<TD class="Label">Phr_Sys_TypeofCommunication:  </TD>
						<TD class="Info">#htmleditformat(communication.CommType)#</TD>
					</TR>
					<TR>
						<TD class="Label">Phr_Sys_Sender:  </TD>
						<TD class="Info">#htmleditformat(communication.creatorName)#</TD>
					</TR>
					<TR>
						<TD class="Label">Phr_Sys_SentBy:  </TD>
						<TD class="Info">#htmleditformat(communication.commform)#</TD>
					</TR>
					<TR>
						<TD colspan="2" class="Label">Phr_Sys_comm_status:  </TD>
					</TR>
 --->

					<TR>
						<TD colspan="2" class="Info">
							<cfif checkIfCommunicationIsBeingSent.isBeingSent and communication.hasUnsentCommDetailRecords() is 1>
								phr_Sys_Comm_CommBeingSent
							<cfelseif structKeyExists(communication.metaData,"contentValidated") and NOT communication.metaData.contentValidated>
								<div class="errorblock">Merge Field Error</div>
								<cfif frmNextPage is "">
									<cfset frmNextPage = "Content">
								</cfif>
							<cfelseif communication.sendDate is not "" and (communication.hasUnsentCommDetailRecords() is 1 OR communication.hasExternalSelectionsWaitingToSend IS 1) and communication.sent is not 1>
								<cfset form.ScheduledTime = application.com.datefunctions.datetimeformat(date = communication.sendDate, showAsLocalTime = true)>
								phr_Sys_Comm_CommScheduledToBeSentOn
								<cfif communication.canBeEdited() and not userIsAllowedToSendNow>
									phr_Sys_Comm_CanBeEditedNotSent
								<cfelseif communication.canBeEdited() and userIsAllowedToSendNow and communication.isOKToSendNow()>
									phr_Sys_Comm_CanBeEditedAndSent
								<cfelseif not communication.canBeEdited() and userIsAllowedToSendNow and communication.isOKToSendNow()>
								</cfif>
							<cfelseif communication.commformlid is 2 and (communication.hasEmailContent() eq 0 or (structkeyexists(form,"FRMEMAILTEMPLATE") eq "true" and (form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")))>
								<cfif frmNextPage is "">
									<cfset frmNextPage = "Content">
								</cfif>
								phr_sys_comm_CommAwaitingContents
							<CFELSEIF communication.hasLink() is 1 and communication.LinksReviewed is not 1 and (structkeyexists(form,"FRMEMAILTEMPLATE") eq "false" or form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")>
								<cfif frmNextPage is "">
									<cfset frmNextPage = "Links">
								</cfif>
								phr_sys_comm_CommAwaitingLinksReview
							<!--- NJH 2013/02/14 Sprint 3 - go to message tab if not filled in and we're sending a message--->
							<cfelseif (not communication.hasMessageContent() and communication.sendMessage) and useMessage>
								<cfif frmNextPage is "">
									<cfset frmNextPage = "Message">
								</cfif>
							<CFELSEIF communication.PreviewReviewed is not 1>
								<cfif frmNextPage is "" OR frmNextPage is "Send">
									<cfset frmNextPage = "Preview">
								 </cfif>
							<CFELSEIF communication.hasExternalSelection() is not 1>
								<cfif frmNextPage is "">
									<cfset frmNextPage = "Selections">
								</cfif>
								phr_sys_comm_CommAwaitingSelectionAllocation
							<CFELSEIF communication.canBeEdited()>
								<cfif not communication.getApprovalDetails().requiresApproval>
									phr_Sys_Comm_IsBeingPrepared  phr_Sys_Comm_CanBeEditedAndSent
								<cfelseif communication.getApprovalDetails().isApproved > <!--- using an approval process and approval is required --->
									phr_Sys_Comm_IsReadyToSend
								<cfelse>
									phr_Sys_Comm_CanBeEditedNotSent
								</cfif>
							<cfelseif communication.hasSelectionsWaitingToSend() is 1 and communication.PreviewReviewed >
								<cfif frmNextPage is "">
									<cfset frmNextPage = "Send"><cfoutput>#frmnextpage#</cfoutput>
								</cfif>
								phr_Sys_Comm_IsReadyToSend  phr_Sys_Comm_HasPendingSelection
								<cfif userIsAllowedToSendNow>

								<cfelseif useCommunicationQueue>

								</cfif>
							<cfelse>
								<cfset readonly="true">
								phr_Sys_Comm_CommSentNotification
							</cfif>

						<!--- 	<cfset approvalDetails = communication.getApprovalDetails()>
							<cfif structKeyExists(approvalDetails,"userIsGatekeeper") and approvalDetails.userIsGatekeeper>
								phr_Sys_Comm_UserIsAGatekeeperForComm
								<cfquery name="userApprovalDetails" dbtype="query" maxRows="1">
									select approved,approvedDate,disapproved,personid,sentForApproval from approvalDetails.gatekeeperQuery
									where personid=<cfqueryparam value="#request.relaycurrentuser.personid#" cfsqltype="CF_SQL_INTEGER">
								</cfquery>
								<cfif userApprovalDetails.approved is 1 and userApprovalDetails.approvedDate lt communication.lastupdated>
									phr_Sys_Comm_Gatekeeper_CommRequiresReapproval
								<cfelseif userApprovalDetails.approved is 1>
									phr_Sys_Comm_Gatekeeper_CommIsApproved
								<cfelseif userApprovalDetails.disapproved is 1>
									phr_Sys_Comm_Gatekeeper_CommIsDisapproved
								<cfelseif userApprovalDetails.sentForApproval is 1>
									phr_Sys_Comm_Gatekeeper_CommRequiresApproval
								<cfelse>
									phr_Sys_Comm_Gatekeeper_CommNotSendForApproval
								</cfif>
							</cfif> --->

							<cfif isdefined("fileDetails") and frmNextPage is "" and fileDetails.isOK>
								<cfset frmNextPage = "Content">
							</cfif>
						</TD>
					</TR>

			</TABLE>

		</td></tr>

		</table>
		</CFOUTPUT>

--->
			</cfif>
</cfif>


<cfset count=0>

<cflayout name="commDetailsLayout" type="tab" style="height:400px; overflow:visible">
	<cfoutput query="layoutareas">
		<cfset selected = false>
		<cfif frmNextPage eq stepName >
			<cfset selected = true>
		</cfif>
		<cfset IconFile = Icon>
		<cfset count++>

		<cfif AreaDisabled>
			<cfset IconFile = "#listfirst(listfirst(Icon,'-'),'.')#-grey.#listlast(Icon,'.')#">
		</cfif>

		<cfset parameters = "frmCommID=#frmCommID#&frmHideTopHead=#frmHideTopHead#&stepName=#stepName#&stepNumber=#layoutareasBystepName[stepName].rowNumber#&readonly=#readonly#&useWrapper=#useWrapper#&action=#action#">
		<cfif useWrapper>
			<cfset srcFile = "commWrapper.cfm?returnFormat=plain&wrappedURL=#urlencodedformat("#CFMFile#?#parameters#")#">
		<cfelse>
			<cfset srcFile = "#CFMFile#?#parameters#&returnFormat=plain">
		</cfif>
		<cflayoutarea name="#stepName#" title="<span areaName='#stepName#' class='commDetailsHomeTab#stepName#' title='#stepName#'><img id='#stepName#TabTitleImg' src='../images/icons/#IconFile#' style='vertical-align:middle;'>#stepName#&nbsp;</span>" source="#srcFile#" initHide="#AreaHidden#" disabled="#AreaDisabled#" selected="#selected#"
			refreshOnActivate="true" bindOnLoad=#selected# style="height:100%"><!--- NJH 2016/01/27 - made layout area 100% to fill available space.?make refreshOnActivate conditional? based on if content or links have changed? --->
		</cflayoutarea>
	</cfoutput>
</cflayout>

</CFAJAXIMPORT>

<!--- START: LHID8100 - forcing the size of the tabs when they fail to load properly --->

<script language="javascript">
	var commid = <cfoutput>#frmCommID#</cfoutput> // used when opening a new tab to check that we are on the communication that we expect

	setTabWidth  = function (tabName,preferredWidth) {

			extTabObj = ColdFusion.Layout.getTabLayout("commDetailsLayout").getTab(tabName)
				tabVar = extTabObj.textEl.dom
				tabWidth = tabVar.style.width;
				if (tabWidth.substring(0,tabWidth.length-2) != <cfoutput>#layoutareas.PreferredWidth[1]#</cfoutput>) {
					tabVar.style.width = preferredWidth + 'px';

					tabVar = extTabObj.pnode.dom;
					if (tabVar) {
						tabVar.style.width = preferredWidth +20 + 'px';
					}
				}
	}


	resizeTabPanel = function() {
		// WAB 2013-05 set the height of the tabs to fill the remains of the page
		tabPanel = $$('.x-tab-panel')[0]
		height = document.viewport.getDimensions().height -  tabPanel.getLayout().get('top')    // odd pixels are the border of the containing div
		ColdFusion.Layout.getTabLayout("commDetailsLayout").setHeight (height)

		}


	setUpTabChangeListener	= function () {
		extTabObj = ColdFusion.Layout.getTabLayout("commDetailsLayout")

		extTabObj.on('beforetabchange',function(tablayout,toTab,fromTab) {

				var result = true
				currentTabDiv = fromTab.body.dom
				currentTabForm = currentTabDiv.down('form')
				if (!currentTabForm) {
					// if there isn't a form with the current tab, then look for an iframe inside the tab  (probably not necessary since 2013 when we got rid of the Iframes
					iframe = currentTabDiv.down('iframe')
					if (iframe)	 {
						forms = iframe.contentWindow.document.forms
						if (forms.length) {
							currentTabForm = forms[0]
						}
					}
				}

				if (currentTabForm) {
							if (isDirty(currentTabForm)) {
								if (!confirm('phr_comm_YouHaveAttemptedToChangeTabsAndWillLoseYourChanges')) {
									result = false;
								}
							}
				return result
			}

		});




	}

	<!---
	WAB 2013-02-19 	modified isFormDirty() so that uses some built in functions to access the layout.
					Will find dirty forms even if they are in Iframes
	 --->
	function isFormDirty(event) {
		activeTab = ColdFusion.Layout.getTabLayout("commDetailsLayout").getActiveTab()

		activeTabContentDiv = $(activeTab.bodyEl.dom)
		activeTabForm = activeTabContentDiv.down('form')
		if (!activeTabForm) {
			// look for an iframe inside the tab
			iframe = activeTabContentDiv.down('iframe')
			if (iframe)	 {
				forms = iframe.contentWindow.document.forms
				if (forms.length) {
					activeTabForm = forms[0]
				}
			}
		}


		if (activeTabForm) {
			var destinationTabName = this.down('span[areaName]').getAttribute('areaName');				// title of destination tab eg 'Setup','Content'
					if (isDirty(activeTabForm)) {
						if (confirm('phr_comm_YouHaveAttemptedToChangeTabsAndWillLoseYourChanges')) {
							ColdFusion.Layout.selectTab('commDetailsLayout',destinationTabName);
						}
					}
		}

	}


	window.onload = function() {
		resizeTabPanel();
		setTimeout(setUpTabChangeListener,1000); // IE timing issue
		window.onresize = resizeTabPanel


	};
	<!--- END: 2012-01-24 NYB Case#424774 --->


</script>

<!--- END: LHID8100 --->