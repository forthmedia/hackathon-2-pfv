<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Removes all records for a specified communication 
		but only if the communication has not yet been sent
		and has not been changed
		
		this is very similar to commhold

Amendment History   

06	Nov 2001		CPS		Delete commdetailhistory records.
		2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object		
--->

<CFPARAM NAME="message" DEFAULT=""> 

<CFPARAM NAME="delmessage" DEFAULT=""> <!--- comms successfully deleted --->
<CFPARAM NAME="sentmessage" DEFAULT=""> <!--- comms not deleted since sent --->
<CFPARAM NAME="chgmessage" DEFAULT=""> <!--- comms not deleted since sent --->
<CFPARAM NAME="baddatemessage" DEFAULT=""> <!--- comms send date bad sent --->
<CFPARAM NAME="scheduledmessage" DEFAULT=""> <!--- comms that have been scheduled --->
<CFPARAM NAME="missmessage" DEFAULT=""> <!--- comms that could not be found --->
<CFPARAM NAME="invalidmessage" DEFAULT=""> <!--- not authorised to edit --->




<CFIF IsDefined("frmCommID")>

	<CFLOOP INDEX="thisCommID" LIST="#frmCommID#">
	
		<CFSET thisLastUpdated = Evaluate("frmLU_#thisCommID#")>

		<cfset communication = application.com.communications.getCommunication (#thiscommid#)>

		<CFIF communication.recordcount is 0 >
			<!--- comm not found --->
			<CFSET missmessage = missmessage & "<LI>Communication ID: #thisCommID#">
		

		<CFELSEIF communication.hasbeensentExternally() IS 1>
			<!--- comm already sent --->
			<CFSET sentmessage = sentmessage & "<LI>#HTMLEditFormat(communication.Title)#">

		<CFELSEIF communication.hasbeensentExternally() IS NOT 1 AND DateDiff("s",communication.LastUpdated,thisLastUpdated) IS NOT 0>
			<!--- comm changed by someone else --->
			<CFSET chgmessage = chgmessage & "<LI>#HTMLEditFormat(communication.Title)#">

		<CFELSEIF NOT (communication.createdBy IS request.relayCurrentUser.usergroupid 
					   OR communication.LastUpdatedBy IS request.relayCurrentUser.usergroupid 
					   OR request.isadmin)>
 	
			<CFSET invalidmessage = invalidmessage & "<LI>#HTMLEditFormat(communicationTitle)#">
			
			
		<CFELSEIF communication.hasbeensentExternally() IS NOT 1>

			<CFSET delmessage = delmessage & "<LI>#HTMLEditFormat(communication.Title)#">
						
			<CFQUERY NAME="getuCommFiles" DATASOURCE="#application.SiteDataSource#">
			SELECT cf.FileName, cf.Directory, cf.CommFileID
			  FROM CommFile AS cf
			 WHERE cf.CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
			
			<CFIF getuCommFiles.RecordCount IS NOT 0>
				<!--- Delete all other files and remove the records from commFile --->
				<CFLOOP QUERY="getuCommFiles">
					<CFIF FileExists("#application.paths.userfiles##Directory#\#FileName#")>
						<CFFILE ACTION="DELETE" 
						        FILE="#application.paths.userfiles##Directory#\#FileName#">
					</CFIF>
				</CFLOOP>
			</CFIF>			
			<CFTRANSACTION>
				<CFQUERY NAME="delCommFiles" DATASOURCE="#application.SiteDataSource#">
				DELETE FROM CommFile 
				 WHERE CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
 				<CFQUERY NAME="delCommDetailHistory" DATASOURCE="#application.SiteDataSource#">
				DELETE 
				  FROM CommDetailHistory 
				 WHERE commDetailId IN (SELECT commDetailId
				                          FROM CommDetail
						 				 WHERE CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</CFQUERY>
				<CFQUERY NAME="delCommDetails" DATASOURCE="#application.SiteDataSource#">
				DELETE FROM CommDetail
				 WHERE CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
				<CFQUERY NAME="delCommSelection" DATASOURCE="#application.SiteDataSource#">
				DELETE FROM CommSelection
				 WHERE CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
				<CFQUERY NAME="delCommunication" DATASOURCE="#application.SiteDataSource#">
				DELETE FROM Communication 
				 WHERE CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			</CFTRANSACTION>
		</CFIF>		

</CFLOOP>	


</CFIF>	


<CFIF IsDefined("frmReschedule")>
	<!--- also need to reschedule communications --->
	<cfinclude template="#thisDir#/commresched.cfm">
	<CF_ABORT>
</CFIF>


<CFIF sentmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) have already been sent so they could not be deleted:<UL>" & sentmessage & "</UL>">		
</CFIF>

<CFIF chgmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be deleted because they have been changed by another user since you viewed them:<UL>" & chgmessage & "</UL>">		
</CFIF>


<CFIF delmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) were successfully deleted:<UL>" & delmessage & "</UL>">		
</CFIF>

<CFIF invalidmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be deleted because you are not authorised to change them:<UL>" & invalidmessage & "</UL>">		
</CFIF>

<CFIF missmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be found:<UL>" & missmessage & "</UL>">		
</CFIF>



	<cfif isDefined("frmNextPage") and frmNextPage is not "">
		<cfif findnocase("?",frmNextPage) is 0 and findnocase("http:",frmNextPage) is 0 >
			<cfinclude template="#frmnextpage#">
		<cfelse>
			<cflocation url="#frmnextpage#&message=#message#"addToken="false">
		</cfif>

	<cfelse>
		<cfinclude template="#thisDir#/commMenu.cfm">	
	</cfif>



<CF_ABORT>
 