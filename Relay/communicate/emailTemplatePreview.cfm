<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		emailTemplatePreview.cfm	
Author:			GCC
Date started:		2005-10-13
	
Description:		This provides a simple preview mechanism to help users choose the coorect email template.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011-06-01  	WAB LID 6769  Security Scan - prevent the use of this file to read any file on the system
2012-11-20		IH	Case 432079 remove htmlEditFormat() around filetext

Enhancements still to do:

template select element should be shared betwwen this and commheader.cfm to ensure consistency of option.index
 --->
<cf_head>
	<cf_title>phr_Email_Template_Previewer</cf_title>
</cf_head>

<SCRIPT type="text/javascript">
function submitChoice() {
	if (window.opener && !window.opener.closed) {
	window.opener.document.CommHeaderForm.frmEmailTemplate.value = document.mainForm.frmEmailTemplate.value;
	window.opener.focus();
	}
}
</script>



	<cfif structkeyexists(form,"frmEmailTemplate")>
		<cfset url.frmEmailTemplate = form.frmEmailTemplate>
	</cfif>

	<cfquery name="getTemplateDescription" datasource="#application.sitedatasource#">
		Select top 1 f.name as description 
		from files f
		where f.filename =  <cf_queryparam value="#url.frmEmailTemplate#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		order by revision desc
	</cfquery>
	
	<cfscript>
		getEmailTemplates = application.com.communications.getValidTemplates();
	</cfscript>

	<table border=0 cellpadding=5 cellspacing=0 class="withBorder" width="100%">
	<tr valign="centre">
	<td align="left" width="1">
		<form name="mainForm" action="emailTemplatePreview.cfm">
			<select name="frmEmailTemplate" size=1 onchange="javascript:mainForm.submit();">
				<option value="0">Phr_Sys_SelectAnEmailTemplate
				<CFOUTPUT query="getemailtemplates" group="priority">
				<option value="0">--------------------
				<cfoutput>
				<OPTION VALUE="#filename#" <CFIF url.frmEmailTemplate is filename>SELECTED</CFIF>>#htmleditformat(description)#
				</cfoutput>
				</CFOUTPUT>
			</select>	
		</form>
	</td>
	<td align="left"><A HREF="JavaScript:submitChoice();window.close();">phr_Comm_Usethistemplate</a>
	</td></tr>
	</table>
	<cfif not url.frmEmailTemplate eq "0">
		<!--- WAB 2011-06-01 LID 6769 security problem
			 Possible to tamper with frmEmailTemplate and get to any file in the system
				remove any instances of ..
				would also like to encrypt the whole select box
		 --->
		 <cfset url.frmEmailTemplate = replace(url.frmEmailTemplate,"..","","ALL")>
		<cfset path = #application.UserFilesAbsolutePath# & "\content\emailTemplates\" & #url.frmEmailTemplate#>
		<cfif fileexists(path)>
			<!--- <cfinclude template="/content/emailTemplates/#url.frmEmailTemplate#"> --->
			<CFFILE ACTION="Read"    
				FILE="#application.paths.content#\emailTemplates\#url.frmemailtemplate#"    
						VARIABLE="filetext"
							Charset="utf-8"
			>
			<cfoutput>
				#filetext#
			</cfoutput>
		<cfelse>
			phr_Comm_TemplateNotFound
		</cfif>
	</cfif>
