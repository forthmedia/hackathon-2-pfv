<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		broadcastFunctions.cfm	
Author:			YMA  
Date started:	21-08-2013
	
Description:	Functions for the broadcast listing page.		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfscript>	
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Message Functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Cancel Messages";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:if (confirm('Are you sure you want to cancel the selected messages?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/communicate/broadcasts.cfm';document.frmBogusForm.submit();} ;//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

</cfscript>