<!--- �Relayware. All Rights Reserved 2014 --->
<CFIF NOT #checkPermission.Level2# GT 0>
	<CFSET message="You are not authorised to use this function">
		<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>


<!--- changed the meaning of update/add okay --->
<CFSET securitylist = "SelectTask:read">
<CFQUERY NAME="checkSelectPermissions" DATASOURCE="#application.SiteDataSource#">
	SELECT <!--- a.UserGroupID, --->
		   a.SecurityTypeID,
<!--- 		   Sum((a.Permission\2) MOD 2) AS UpdateOkay,
		   Sum((a.Permission\4) MOD 2) AS AddOkay --->
   		   Sum({fn MOD ({fn CONVERT(a.Permission/2, SQL_INTEGER)},2)}) AS UpdateOkay,
		   Sum({fn MOD ({fn CONVERT(a.Permission/4, SQL_INTEGER)},2)}) AS AddOkay
	FROM Rights AS a, RightsGroup AS b
	WHERE a.SecurityTypeID = (SELECT c.SecurityTypeID FROM SecurityType AS c
         WHERE c.ShortName =  <cf_queryparam value="#ListFirst(securitylist,":")#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	  AND a.UserGroupID = b.UserGroupID
	  AND b.PersonID = #request.relayCurrentUser.personid#
	GROUP BY a.SecurityTypeID
	HAVING Sum({fn MOD ({fn CONVERT(a.Permission/1, SQL_INTEGER)},2)}) > 0 <!--- at least read --->
</CFQUERY>



<!--- communications with CommFormLID > 0 
will have files etc already uploaded.  If the CommFormLID = 0
it may be a "both" so check the CommFile table --->

<!---NB the = 0 can eventually be removed when the new prefer fax/email
	is live used for a while --->
	
<CFQUERY NAME="getComm" DATASOURCE="#application.SiteDataSource#">
	SELECT c.CommID AS CommID,
		   c.Title AS Title,
		  ll.ItemText AS ItemText
      FROM Communication AS c, LookupList AS ll
     WHERE c.Sent = 0
	  AND c.CommFormLID > 0
	  AND c.CommFormLID = ll.LookupID
 UNION 
	SELECT c.CommID,
		   c.Title,
		  ll.ItemText
      FROM Communication AS c, CommFile AS cf, LookupList AS ll
     WHERE c.Sent = 0
	  AND c.CommID = cf.CommID
	  AND c.CommFormLID = 0
	  AND cf.CommFormLID = ll.LookupID
	  ORDER BY Title, CommID, ItemText <!--- note 2 comms may have the same titles so order by CommID so loop below will work --->
</CFQUERY>



<cf_head>
	
	<SCRIPT type="text/javascript">
<!--
	function checkForm(next){
		var msg="";
		var form = document.ThisForm;
		form.btnTask.value = next;
		if (form.frmCommID.options[form.frmCommID.selectedIndex].value == 0) {
			msg = "\n\nYou must select a communication.";
			alert(msg);		
		} else {
			form.submit();
		}
	}
//-->
</SCRIPT>
</cf_head>



<cfparam name="frmHideTopHead" default="false">
<cfif not frmHideTopHead>
	<cfinclude template="#thisDir#/commtophead.cfm">
</cfif>

<BR><BR>
<cfoutput><FORM NAME="ThisForm" ACTION="#thisDir#/commtask.cfm" METHOD=POST></cfoutput>
<!--- choices are send|edit in commtask.cfm --->

<INPUT TYPE="HIDDEN" NAME="btnTask" VALUE="send">

<CENTER>

<P><SELECT NAME="frmCommID">
			<OPTION VALUE="0"> Choose a Communication
			<!--- need to make the commFormLIDs a list --->
			<CFSET #oldCommID# = 0>
			<CFSET #oldTitle# = 0>
			<CFSET #via# = "">
			<CFOUTPUT QUERY="getComm">
				<CFIF #CommID# IS #OldCommID#>
					<CFSET via = #ListAppend(via,ItemText,",")#>
				<CFELSE>
					<CFIF #oldCommID# IS NOT 0>
						<OPTION VALUE="#oldCommID#"> #htmleditformat(oldTitle)# (#htmleditformat(via)#)
					</CFIF>
					<CFSET via = #ItemText#>
					<CFSET oldCommID = #CommID#>
					<CFSET oldTitle = #Title#>
				</CFIF>
			</CFOUTPUT>
			<CFIF #oldCommID# IS NOT 0>
				<CFOUTPUT><OPTION VALUE="#oldCommID#"> #htmleditformat(oldTitle)# (#htmleditformat(via)#)</CFOUTPUT>
			</CFIF>
		</SELECT>
	<CFOUTPUT>	
		<P><A HREF="JavaScript:checkForm('edit');"><IMG SRC="/images/buttons/c_edit_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Edit communication"></A>
		<CFIF checkSelectPermissions.RecordCount GT 0>
		&nbsp; &nbsp; <A HREF="JavaScript:checkForm('send');"><IMG SRC="/images/buttons/c_continue_e.gif" WIDTH=105 HEIGHT=21 BORDER=0 ALT="Continue"></A>
		</CFIF>
	</CFOUTPUT>
</CENTER>

</FORM>



