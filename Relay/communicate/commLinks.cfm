<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2011-04-07	NYB		P-LEN024 - created
2011-09-06	WAB		LID 7702 Problems with mailTo:  - should not be trackable
2011-11-11	NYB		LHID7817 - added another table for layout
2011/11/17	WAB		Plumbed in some standard functions for parsing relayfunctions
2012-03-19	WAB		Fixed Problems with parsing relayFunctions if using unnamed parameters
2012-10-16	WAB		CASE 430351 - Problems with links containing old style merge fields (such as [[gotoElementID]]).
2012-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-04-30	WAB		Added support for Area tags
2013-05-29	WAB		Added message warning not to edit if comm is approved
2016-01-28  SB		Removed Tables
--->


<cfparam name="frmCommID" >
<CFPARAM name="stepName" default="">
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">

<cfif not isdefined("communication")>
	<cfset communication = application.com.communications.getCommunication (commid = #frmcommid#,personid = request.relayCurrentUser.personid)>
</cfif>


<CFQUERY NAME="setLinksPreviewed" DATASOURCE="#application.SiteDataSource#">
	declare @updated bit;
	set @updated=0;
	if exists (SELECT * FROM information_schema.columns where column_name='LinksReviewed' and table_name='communication')
		begin
			if exists (select * from communication with(nolock) where CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  and LinksReviewed=0)
				begin
					update communication set LinksReviewed=1 where CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > ;
					set @updated=1;
				end
		end
	select @updated as updated
</CFQUERY>

<SCRIPT type="text/javascript">
	<cfif setLinksPreviewed.updated>
		setTabCompleted ('Links')
	</cfif>

	checkFormcommLinks = function(btn) {
		var theForm=document.getElementById("linksGridForm");
		theForm.frmRequestedAction.value = btn;
		triggerFormSubmit(theForm);
	}
</SCRIPT>

<FORM NAME="linksGridForm" ID="linksGridForm" ACTION="commDetailsHome.cfm" METHOD="POST">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCallingTemplate" VALUE="#frmCallingTemplate#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmRequestedAction" ID="frmRequestedAction" VALUE="">
	<CF_INPUT type="hidden" name="frmCommID" value="#frmCommID#">

	<cfset linksArray = communication.getArrayOfLinksInContent()>

	<cfoutput>
		<h2>Phr_Sys_comm_Step_#htmleditformat(stepName)#_Header</h2>
	</cfoutput>


		<cfif ArrayLen(linksArray) gt 0>
			<!--- If communication is approved then this function will return a block of warning text --->
			<cfoutput>#communication.getEditMessageBlock()#</cfoutput>
		</cfif>


		<cfif ArrayLen(linksArray) gt 0>

				<table id="tfqo_aReport" class="table table-hover">
					<tr>
						<th id="ID">ID</th>
						<th id="Track">Track</th>
						<th id="Name">Name</th>
						<th id="Contents">Contents</th>
						<th id="Link">Link</th>
					</tr>
					<cfloop index="i" from="1" to="#ArrayLen(linksArray)#">
						<cfset linksArray[i].Tracked = "no">
						<!--- HREF is not always defined - an anchor for example only has a name --->
						<CFIF not structkeyexists(linksArray[i].ATTRIBUTES,"href")>
							<cfset linksArray[i].ATTRIBUTES.href = "">
						</CFIF>
						<CFIF structkeyexists(linksArray[i].ATTRIBUTES,"track")>
							<cfset linksArray[i].Tracked = linksArray[i].ATTRIBUTES.track>
						<CFELSE>
							<cfset linksArray[i].Tracked = "false">
						</CFIF>
						<cfif not structkeyexists(linksArray[i].ATTRIBUTES,"name")>
							<cfset linksArray[i].ATTRIBUTES.name = "">
						</cfif>
						<cfif structkeyexists(linksArray[i],"innerHTML")>
							<cfset linksArray[i].innerHTML = trim(linksArray[i].innerHTML)>
						</cfif>
						<cfoutput>
						<TR class=<CFIF i MOD 2 IS NOT 0>"oddrow"<CFELSE>"evenRow"</CFIF>>
							<td id="ID" align="center">#htmleditformat(i)#</td>
							<td id="Tracked" align="center">
							<cfif structkeyexists(linksArray[i].ATTRIBUTES,"href") and left(linksArray[i].ATTRIBUTES.href,2) eq "[[">
								<cf_input name="frm#i#Tracked" type="checkbox" checked="true" disabled="true">
							<cfelseif structkeyexists(linksArray[i].ATTRIBUTES,"href") and left(linksArray[i].ATTRIBUTES.href,7) eq "mailTo:">
								<cf_input name="frm#i#Tracked" type="checkbox" checked="false" disabled="true">
							<cfelse>
								<cf_input name="frm#i#Tracked" type="checkbox" value="yes" checked="#linksArray[i].Tracked#">
							</cfif>
							</td>
							<td id="Name">
								<cf_input name="frm#i#Name" class="form-control" type="text" value="#linksArray[i].ATTRIBUTES.name#" size="15">
								<cf_input name="frm#i#Nameorig" type="hidden" value="#linksArray[i].ATTRIBUTES.name#" size="15">
							</td>
							<td id="Contents">
								<cfif structkeyexists(linksArray[i],"innerhtml")>
									<cf_input name="frm#i#Contents" class="form-control" type="text" value="#linksArray[i].innerHTML#" size="30">
									<cf_input name="frm#i#Contentsorig" type="hidden" value="#linksArray[i].innerHTML#" size="30">
								</cfif>
							</td>
							<td id="Link">
																				<!---
								WAB 2011/11/18 replaced with some existing functions for parsing relayFunctions

								<cfif structkeyexists(linksArray[i].ATTRIBUTES,"href") and left(linksArray[i].ATTRIBUTES.href,2) eq "[[">
									<!---
									<cfset xxxx = REReplaceNoCase(linksArray[i].ATTRIBUTES.href, "\[\[([^\(\]]*)([\(\]]*)([^=]*)(=*)([^,]*)(,*)([^""']*)([""']*)([^""']*)([""']*)(.*)", "aaaaa\1bbbb\2cccccc\3dddddd\4eeeee\5ffffff\6ggggggg\7hhhhhhh\8iiiiiiii\9jjjjjj\10kkkkkkkk\11llllllllllll\12mmmmmmmmm")>
									--->
									<cfset frmLinkType = REReplaceNoCase(linksArray[i].ATTRIBUTES.href, "\[\[([^\(\]]*)([\(\]]*)([^=]*)(=*)([^,]*)(,*)([^""']*)([""']*)([^""']*)([""']*)(.*)", "\1")>
										phr_sys_comm_LinkType_#frmLinkType#<cfif listfindnocase("relayelement,RelayPageLinkV2,SecurePageLink,Download",frmLinkType) gt 0>:
											<CFSET regExp = "\[{2}[^\(]*\({1}([^\)]*)\){1}\]{2}">
											<CFSET replaceStrg = "\1\2\3\4\5\6\7\8">
											<cfset LinkAttributesStrg = rereplacenocase(linksArray[i].Attributes.href,regExp,replaceStrg,"all")>
											<cfset LinkAttributesStruct = application.com.regExp.convertNameValuePairStringToStructure(inputString=LinkAttributesStrg,delimiter=",",addSortOrder="true")>
											<cfloop from="1" to="#StructCount(LinkAttributesStruct)#" index="j">
												<cfset x = StructKeyList(LinkAttributesStruct[j])>
												<cfoutput>
												phr_#x# <cf_input name="frm#i##x#" type="text" value="#LinkAttributesStruct[j][x]#" size="#Len(LinkAttributesStruct[j][x])#">
												</cfoutput>
											</cfloop>
										</cfif>

									<cf_input name="frm#i#LinkType" type="hidden" value="#frmLinkType#" size="30">

								</cfif>
								--->
								<cfset testForRelayFunction = application.com.regExp.reFindAllOccurrences(application.com.relayTranslations.regExp.SquareBrackets,linksArray[i].ATTRIBUTES.href)> <!--- actually would also find a merge field, so need to check for ( as well --->
								<cfif arrayLen(testForRelayFunction) and left(linksArray[i].ATTRIBUTES.href,2) eq "[[" >
									<cfif testForRelayFunction[1].string contains "(">
										<cfset parseRelayFunction = application.com.regexp.parseRelayFunction(testForRelayFunction[1].string)>
										<cfset notFunction = false>
									<cfelse>
										<!--- doesn't contain ( - this must be a plain merge field - probably download, have to search for its arguments manually, ought to be replaced with a downloadLink() function --->
										<cfset parseRelayFunction =  {name = testForRelayFunction[1][2]}>
										<cfset paramList = replace(linksArray[i].ATTRIBUTES.href,testForRelayFunction[1].string,"")>
										<cfset paramResult = application.com.regExp.convertNameValuePairStringToStructure_(inputString=paramList,delimiter="&")>
										<cfset parseRelayFunction.params = paramResult.result>
										<cfset parseRelayFunction.paramListOrdered = paramResult.orderedNameList>
										<cfset notFunction = true>
									</cfif>

										<cfset frmLinkType = parseRelayFunction.name>
											<label>phr_sys_comm_LinkType_#frmLinkType#:</label>
											<cfloop index="paramName" list="#parseRelayFunction.paramListOrdered#">
												<cfif ListFirst(paramName,"_") is not "unnamed">
												phr_#htmleditformat(paramName)#</div>
												</cfif>
												<cf_input class="form-control" name="frm#i##paramName#" type="text" value="#parseRelayFunction.params[paramName]#" size="#Len(parseRelayFunction.params[paramName])#">
											</cfloop>
											<!--- WAB 2012-10-16 CASE 430351 - fix problems with old style merge fields (ie not functions)--->
											<cfif notFunction>
												<cf_input name="frm#i#NotFunction" value="">
											</cfif>

											<!---
											this is an illustration of how editing could be done with the function editor
											<cfset editorPath = "/relaytageditors/relayFunctionEditor.cfm?relayInsertType=mergeFields&context=communications&relayFunction=#urlencodedformat(testForRelayFunction[1].string)#&returnFunction=alert('update function to be written!')
											<cfoutput><a href = "#editorPath#" target="_new">edit</a>	</cfoutput>
											--->
									<cf_input name="frm#i#LinkType" type="hidden" value="#frmLinkType#" size="30">
								<cfelseif structkeyexists(linksArray[i].ATTRIBUTES,"href")>
									<cf_input name="frm#i#Link" class="form-control" type="text" value="#linksArray[i].ATTRIBUTES.href#" size="68">
									<cf_input name="frm#i#Linkorig" type="hidden" value="#linksArray[i].ATTRIBUTES.href#" size="68">
								<cfelse>
									<cf_input name="frm#i#Link" class="form-control" type="text" value="" size="68">
									<cf_input name="frm#i#Linkorig" type="hidden" value="" size="68">
								</cfif>
							</td>
						</tr>
						</cfoutput>
					</cfloop>
				</table>
		<cfelse>
			phr_sys_comm_NoLinksPresentInComm
			<cfset showSave = false>
		</cfif>
	<CFINCLUDE TEMPLATE="saveAndContinueButtons.cfm">
</FORM>

