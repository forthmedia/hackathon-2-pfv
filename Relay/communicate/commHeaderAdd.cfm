<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

CommHeaderAdd.cfm

formerly called commcreate.cfm

KT 1999-11-01 Added frmFlagID for events 
	WAB 2004-05-18 	Added proper Nvarchar support
2011-11-20 NYB	LHID 4918 - added support for CommEmailFormat being db driven
2011-07-04	NYB		P-LEN024 
2012-03-05	WAB	WYSIWYG cannot edit <A> tags without hrefs in them.. Lenovo have habit of providing templates like this so add blank href where necessary
2012-03-06 	RMB 	P-LEN044 - CR-069 - Added CharacterSet to update and insert for table "Communication"
2012-11-13 	WAB		CASE 431969	Add UTF-8 support for Loading Content from Zip File
2012-11-15 	WAB		CASE 432000 Problems with image paths in zip files if zip file contained subdirectories.  Also added some comments,tidied code (regexp replace instead of two replaces) and put image files into a comm specific subdirectory
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
2013-08-15	YMA	Case 436313 fromDiaplyName now stored in commFromDisplayName.
--->

<CFIF NOT #checkPermission.Level2# GT 0>
	<CFSET message="Phr_Sys_YouAreNotAuthorised">
		<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>


<CFPARAM NAME="frmProjectRef" DEFAULT="">
<CFPARAM NAME="frmtrackemail" DEFAULT="0">				 <!--- a checkbox --->
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">
<CFPARAM name="frmFailTo" default= "#thisDir#/commMenu.cfm">
<cfparam name="frmStyleSheet" default="">

<CFSET CommIsNew = "false">

<!--- 2013-08-15	YMA	Case 436313 From Display Name is now stored in a seperate table and can be added so needs to save seperately if its new --->
<cfif not isNumeric("#frmcommFromDisplayNameID#")>
	<CFQUERY NAME="insertComm" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO commFromDisplayName (
					displayName, 
					active, 
					CreatedBy, 
					Created, 
					LastUpdatedBy, 
					LastUpdated, 
					Lastupdatedbypersonid) 
		VALUES (
					<cf_queryparam value="#frmcommFromDisplayNameID#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
					1, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
					<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
					<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)
	</CFQUERY>
	<CFQUERY NAME="newCommFromDisplayNameID" DATASOURCE="#application.SiteDataSource#">
		select max(commFromDisplayNameID) as newCommFromDisplayNameID from commFromDisplayName
	</CFQUERY>
</cfif>

<cfif frmCommID is 0 >
	<!--- New communication being created --->
		<CFTransaction>
		<!--- NYB 2011-01-20 LHID 4918 - added CommEmailFormatID and related value --->
		<!--- 2012/03/06 - RMB - P-LEN044 - CR-069 - Added 'CharacterSet' to update and insert for table "Communication" --->
		<CFQUERY NAME="insertComm" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO Communication (
						ProjectRef, 
						<cfif isDefined("frmCampaignID")>campaignID, </cfif>
						Title, 
						Description, 
						ReplyTo, 
						Linktositeurl,
						fromAddress,
						commFromDisplayNameID,
						CommTypeLID, 
						CommFormLID, 
						TrackEmail, 
					<cfif structkeyexists(form,'frmTrackEmailWithOmniture')>
						TrackEmailWithOmniture,
					</cfif>
						Notes, 
						SendDate, 
						Sent, 
						TestFlag, 
						CommEmailFormatID,
						StyleSheet,
						CharacterSet,
		
						CreatedBy, Created, LastUpdatedBy, LastUpdated,contentLastUpdatedBy,contentLastUpdated) 
			VALUES (
						<cf_queryparam value="#frmProjectRef#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cfif isDefined("frmCampaignID")>
							<cf_queryparam value="#frmCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" null="#iif(isNumeric(frmCampaignID),de('no'),de('yes'))#">,
						</cfif>
						<cf_queryparam value="#frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
						<cf_queryparam value="#frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
						<cf_queryparam value="#frmReplyTo#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
						<cf_queryparam value="#frmLinktositeurl#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<!--- I do not update these variables if they are still the same as the default application variables
							this means that if the application variables are changed then we aren't left with old (or wrong) details in communications which haven't been sent out yet --->
						<cfif frmfromAddress is not "#application.emailFrom#@#application.mailfromdomain#"><cf_queryparam value="#frmfromAddress#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse>null</cfif>,
						<cfif isNumeric("#frmcommFromDisplayNameID#")><cf_queryparam value="#frmcommFromDisplayNameID#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse><cf_queryparam value="#newCommFromDisplayNameID.newCommFromDisplayNameID#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
						<cf_queryparam value="#frmCommTypeLID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#frmCommFormLID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#frmtrackemail#" CFSQLTYPE="CF_SQL_bit" >,
					<cfif structkeyexists(form,'frmTrackEmailWithOmniture')>
						<cf_queryparam value="#frmTrackEmailWithOmniture#" CFSQLTYPE="cf_sql_bit" >,
					</cfif>
						<cf_queryparam value="#frmNotes#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
						null, 
						0, 
						0, 
						<cf_queryparam value="#frmCommEmailFormat#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#frmStyleSheet#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#frmCharacterSet#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)

			select scope_identity() as commid
		</CFQUERY>
		</CFTRANSACTION>
		<CFSET frmcommID = insertComm.commid>
		<cfset CommIsNew = "true">

		<cfset communication = application.com.communications.getCommunication (#frmCommid#,request.relayCurrentUser.personid)>
		<!--- WAB 2005-04-14
		
		Needed to quickly add some functionality which added in some default settings to communications
		probably ought to be somewhere else (like in a createCommunication cfc) but anyhow here goes.
	
		 --->
		<cfset communication.addDefaultSelections()>

		<CFSET Message="Phr_Sys_YourComm '#frmTitle#' Phr_Sys_HasBeenAdded.">		
<cfelse>
	<!--- existing communication being updated --->

	<!--- read exising record --->
	<cfset communication = application.com.communications.getCommunication (#frmCommid#,request.relayCurrentUser.personid)>
		
		<CFIF communication.RecordCount IS 0>
			<!--- comm not found --->
			<CFSET message ="The communication (ID: #frmCommId#) could not be found.">
			<CFINCLUDE TEMPLATE="#frmFailTo#">
			<CF_ABORT>
		
		<CFELSEIF not communication.canBeEdited()>
			<!--- comm already sent --->
				<CFSET message = "The communication could not be updated since it has already been sent.">
				<CFINCLUDE TEMPLATE="#frmFailTo#">
				<CF_ABORT>
		
		<CFELSEIF  DateDiff("s",communication.LastUpdated,frmLastUpdated) IS NOT 0>
			<!--- comm changed by someone else --->
				<CFSET message ="The communication could not be updated since it has been changed by another user since you downloaded it.">
				<CFSET message = message & " <A HREF=""#thisDir#/commDetailsHome.cfm?frmCommID=#frmCommID#&btnTask=edit&frmNextPage=Setup"">Click here</A> to view the current record.">
				<CFINCLUDE TEMPLATE="#frmFailTo#">
				<CF_ABORT>
		
		<cfelseif not communication.hasrightsToEdit()>
				<CFSET message ="Sorry, you do not have rights to edit this communication.">
				<CFSET message = message & " <A HREF=""#thisDir#/commDetailsHome.cfm?frmCommID=#frmCommID#&btnTask=edit&frmNextPage=Setup"">Click here</A> to view the current record.">
				<CFINCLUDE TEMPLATE="#frmFailTo#">
				<CF_ABORT>
		
		</CFIF>			
		
		
		<!--- IT HAS NOT CHANGED SO UPDATE --->
		<!--- NYB 2011-01-20 LHID 4918 - added CommEmailFormatID and related value --->
		<!--- 2012/03/06 - RMB - P-LEN044 - CR-069 - Added 'CharacterSet' to update and insert for table "Communication" --->
		<CFQUERY NAME="UpdateComm" DATASOURCE="#application.SiteDataSource#">
			UPDATE Communication
			SET Title =  <cf_queryparam value="#frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ProjectRef =  <cf_queryparam value="#frmProjectRef#" CFSQLTYPE="CF_SQL_VARCHAR"> , 			
				<cfif isDefined("frmCampaignID")>
					campaignID =  <cf_queryparam value="#frmCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" null="#iif(isNumeric(frmCampaignID),de('no'),de('yes'))#" > ,
				</cfif>
				Description =  <cf_queryparam value="#frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ReplyTo =  <cf_queryparam value="#frmReplyTo#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				CommTypeLID =  <cf_queryparam value="#frmCommTypeLID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				Notes =  <cf_queryparam value="#frmNotes#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				Linktositeurl =  <cf_queryparam value="#frmLinktositeurl#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<!--- if the from address and name are the defaults then just update to blank - this means that if the default is changed then comms will still come from the default place --->
				fromAddress = <cfif frmfromAddress is not "#application.emailFrom#@#application.mailfromdomain#">N'#frmfromAddress#'<cfelse>null</cfif>, 
				commFromDisplayNameID = <cfif isNumeric("#frmcommFromDisplayNameID#")><cf_queryparam value="#frmcommFromDisplayNameID#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse><cf_queryparam value="#newCommFromDisplayNameID.newCommFromDisplayNameID#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>, 
				TrackEmail =  <cf_queryparam value="#frmTrackEmail#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<cfif structkeyexists(form,'frmTrackEmailWithOmniture')>
					TrackEmailWithOmniture =  <cf_queryparam value="#frmTrackEmailWithOmniture#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<cfelseif application.com.settings.getSetting(variablename="communications.tracking.allowomnituretracking",reloadonnull=false) and not structkeyexists(form,'frmTrackEmailWithOmniture')>
					TrackEmailWithOmniture = 0,
				</cfif>
				CommEmailFormatID =  <cf_queryparam value="#frmCommEmailFormat#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				StyleSheet = <cf_queryparam value="#frmStyleSheet#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				CharacterSet = <cf_queryparam value="#frmCharacterSet#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				LastUpdatedBy=#request.relayCurrentUser.usergroupid#
				<!---  WAB 2013-05  contentLastUpdated is only updated when actual content is updated (probably could be argued that the subject line is part of the content)
				ContentLastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				ContentLastUpdatedBy=#request.relayCurrentUser.usergroupid#
				--->
				<!--- 2013-07-16	YMA	Case 436112 No comm is ever created with miliseconds in the lastupdated date so ignore miliseconds in the WHERE statement. --->
			WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND DATEADD(ms, -DATEPART(ms, LastUpdated), LastUpdated) =  <cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFQUERY>

	<CFSET message = "The communication '#frmTitle#' has been updated">

</cfif>

<CFIF frmCommID IS "">	
	<!--- aggregate functions sometimes return one empty record --->
	<CFSET Message="Phr_Sys_CommNotAdded.">
	<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- START 2011-07-04 NYB P-LEN024 - added: --->
<CFIF (isDefined('form.frmUploadEmailZip') and Trim(FORM.frmUploadEmailZip) is not "")>
	<cfset tempDirectory = "#application.paths.temp#\#session.sessionid#">
	<cfset fileDetails=application.com.fileManager.uploadFile(fileField="frmUploadEmailZip", destination="#tempDirectory#", nameConflict="Overwrite")>
	<cfset fileFound = false>
	<cfset frmMsg_HTML = "">
	<cfset frmMsg_Txt = "">
	<cfif fileDetails.IsOK>
		<cfif fileDetails.CLIENTFILEEXT eq "zip">
			<cfset x = application.com.fileManager.unzipFile(fileAndPath = "#tempDirectory#\#fileDetails.ClientFile#", destination = "#tempDirectory#\unzipped", overwrite=true)>	
			<cfif ListFind("1,3",frmCommEmailFormat) gt 0>
				<!--- Look for a .htm file among the zipped files --->
				<cfdirectory directory = "#tempDirectory#\unzipped" action = "list" filter = "*.htm*" listinfo= "all" name = "ContentFiles" recurse = "yes" type = "file">

				<cfif ContentFiles.recordcount gt 0><!--- found a .htm file --->
					<cfset fileFound = true>
					
					<!--- read the .htm file --->
					<cffile action="read" file="#ContentFiles.directory#\#ContentFiles.name#" variable="FileContent" charset="utf-8">
					<cfset frmMsg_HTML = FileContent>


					<!--- Get all other files from the zip  --->
					<cfdirectory directory = "#tempDirectory#\unzipped" action = "list" listinfo= "all" name = "allFiles" recurse = "yes" type = "all">
				
					<!--- WAB 2012-11-15 CASE 432000 added commid to the image path - otherwise would surely be conflicts --->
					<cfset imageDirectory = "#application.paths.content#\emailImages\#frmCommID#">

					<cfloop query="allFiles">

						<cfif IsImageFile("#Directory#\#Name#") or ListFindNoCase("css,swf", listlast(Name,".")) gt 0>
							<!--- Image, css and swf files are copied to the server.  We need to update the html file with the new relative path --->
							<cfif not directoryExists(imageDirectory)>
								<cfdirectory action="create" directory="#imageDirectory#">
							</cfif>
							<cffile action="copy" destination="#imageDirectory#" source="#Directory#\#Name#">


							<!--- 
								work out the relative path to this file from the index (html) page 
								WAB 2012-11-15 CASE 432000 was getting relative path wrong if the htm file was in a subdirectory
												so made some mods
							--->
							<cfset relativePath = replace(replace(replace(Directory,ContentFiles.directory[1],""),"\","","one"),"\","/","all")>			
							<cfif len(relativePath) gt 0>
								<cfset relativePath = relativePath&"/">			
							</cfif>
							<cfset srcString = relativePath&Name>
	
							<cfset frmMsg_HTML = rereplacenocase(frmMsg_HTML,"([""'])#srcString#([""'])","\1/content/emailImages/#frmCommID#/#Name#\2","all")>
	
						</cfif>
					</cfloop>

					<!--- WAB 2012-03-03 Lenovo, CKEditor does not like Anchors without hrefs, so add them in if necessary --->				
					<cfset frmMsg_HTML = application.com.communications.dealWithAnchorsWithNoHREF(Content=frmMsg_HTML)>
					<cfset frmMsg_HTML = application.com.communications.replaceNonTrackedLinksInEmail(Content=frmMsg_HTML,LinkToSiteURL=form.FRMLINKTOSITEURL)>
					<cfset scriptArray = application.com.regExp.findHTMLTagsInString(inputString=frmMsg_HTML,htmlTag="script")>
					<cfset startVar=1>
					<cfloop index="i" from="1" to="#ArrayLen(scriptArray)#">
						<CFSET frmMsg_HTML = Replace(frmMsg_HTML, scriptArray[i].STRING,"")>
					</cfloop>
				</cfif>
			</cfif>
			<cfif ListFind("2,3",frmCommEmailFormat) gt 0>
				<cfdirectory directory = "#application.paths.temp#\#session.sessionid#\unzipped" action = "list"
				    filter = "*.txt" listinfo= "all" name = "ContentFiles" recurse = "yes" type = "file">
				<cfif ContentFiles.recordcount gt 0>
					<cfset fileFound = true>
					<cffile action="read" file="#ContentFiles.directory[1]#\#ContentFiles.name[1]#" variable="FileContent" charset="utf-8">
					<cfset frmMsg_Txt = FileContent>
				</cfif>
			</cfif>
			<cfif fileFound>
				<cfinclude template="commfileadd.cfm">
			<cfelse>
				<cfset message = "<b>File Upload Failure:</b><br />A suitable contents file could not be found within the upload zip file.  HTML content should be stored in a .htm or .html file, text content should be stored in a .txt file.">
			</cfif>
		<cfelse>
			<cfset message = "<b>File Upload Failure:</b><br />Uploaded file was not of type zip">
		</cfif>
		<cfdirectory action = "delete" directory = "#application.paths.temp#\#session.sessionid#" recurse = "yes" type = "all">
	<cfelse>
		<cfset message = "<b>File Upload Failure:</b><br />"&replace(fileDetails.message,",",", ","all")>
		<cfset frmNextPage = "Setup">
		<cfif structkeyexists(form,"frmCallingTemplate")>
			<cfset StructDelete(form,"frmCallingTemplate")>
		</cfif>
	</cfif>
<cfelse>
	<cfif structkeyexists(form,"frmEmailTemplate") and form.frmEmailTemplate neq "" and form.frmEmailTemplate neq "0">
	<!--- check file exists --->
		<CFSET sDirGetEmailTemplates = application.paths.content &  "\emailtemplates\">

		<CFDIRECTORY ACTION="list" DIRECTORY="#sDirGetEmailTemplates#" FILTER="#form.frmEmailTemplate#" NAME="checkfileexists">

		<CFIF checkfileexists.recordcount IS NOT 0>
			<CFFILE ACTION="Read"    
				FILE="#application.paths.content#\emailtemplates\#frmemailtemplate#"    
						VARIABLE="filetext"
							Charset="utf-8"
			>
			<!--- WAB 2004-09-29 WAB: problem with character sets here.  If we read as UTF-8 when it is not then there is a problem
				turns out to be better to load it without any character set set.  
			 --->
			<!--- 	GCC 2008-02-14 Found if we write it as UTF-8 then we are in business. Issue may be historic templates that have not been saves as UTF-8 through the front end --->
				<!--- separate out the HTML and Text versions if they are both in the file --->
				<cfset HTMLandText = application.com.communications.extractHTMLandText (filetext)>
				<cfset frmMsg_HTML = HTMLandText.html>
				<!--- WAB 2012-03-03 Lenovo, CKEditor does not like Anchors without hrefs, so add them in if necessary --->				
				<cfset frmMsg_HTML = application.com.communications.dealWithAnchorsWithNoHREF(Content=frmMsg_HTML)>

				<cfset frmMsg_TXT = HTMLandText.text>
		<CFELSE> 
			<cfset frmMsg_HTML="Template not found">
		</CFIF>
		<cfinclude template="commfileadd.cfm">
	</cfif>
</CFIF>
<!--- END 2011-07-04 NYB P-LEN024--->