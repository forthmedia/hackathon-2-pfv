<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Displays a table of all communications for a particular commID --->

<!--- Written by William Bibby 1998-07-23 

2008/07/09 	NJH	Changed this report to use TFQO. Bug Fix T-10 Issue 682
2012-10-04	WAB		Case 430963 Remove Excel Header 
--->


<CFINCLUDE template=commresultsquery.cfm>

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="showTheseCols" type="string" default="PersonID,FirstName,LastName,SiteName,SentBy,DeliverResult,DeliverStatus">

<!--- A bit lazy.... getting 'result' as 'deliverStatus' so that we don't have to create a whole bunch of new translations
	as the old version used deliverStatus as the column heading --->
<cfquery name="getCommData" dbType="query">
	select *, result as DeliverStatus from CommunicationResults
</cfquery>

	<cf_head>
	   <cf_title>Phr_Sys_AllCommunications</cf_title>
	</cf_head>
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR>
			<td align="left" valign="top" class="Submenu">Phr_Sys_AllRecordsForCommunication <CFOUTPUT>#htmleditformat(URL.frmCommID)#</CFOUTPUT></td>
			<!--- NJH 2008/07/09 added if clause --->
			<cfif not frmruninpopup><td align="right" class="Submenu"><a class="Submenu" href="javascript:window.history.go(-1)">Phr_Sys_Back</a></td></cfif>
		</TR>
	</TABLE>
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR>
			<TD COLSPAN="5"><CFOUTPUT>Number of People: #CommunicationResults.recordcount#
				<BR>#htmleditformat(filter.Message)#</CFOUTPUT>
			</TD>
		</TR>


	<cf_tableFromQueryObject 
		queryObject="#getCommData#"
		queryName="getCommData"
		showTheseColumns="#showTheseCols#"
		useInclude="false"
		columnTranslation="true"
		ColumnTranslationPrefix="phr_sys_"
	>


<!--- <cfoutput>
<TR bgcolor="cccccc">
	<th>Phr_Sys_PersonID</th>
	<th>Phr_Sys_Recipient</th>
	<th>Phr_Sys_SentBy</th>
	<th>Phr_Sys_DeliverResult</th>
	<th>Phr_Sys_DeliverStatus</th>
</TR>
</cfoutput>

<CFOUTPUT query="CommunicationResults">
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
	<TD>#PersonID#</TD>
	<TD>#FirstName# #LastName#<BR>#SiteName#</TD>
<!--- 	<TD>#IIf(Emailstatus is not -2,DE(emailtext),DE(''))# #IIf(Faxstatus is not -3,DE(faxtext),DE(''))#</TD> --->
	<TD>#IIf(byemail is 2,DE('Email'),DE(''))# #IIf(byfax is 3,DE('Fax'),DE(''))#</TD> 
	<TD>#EmailFeedback#<BR>#FaxFeedback#</TD>
	<TD>#Result#</A></TD>
</TR>
</CFOUTPUT> --->
<cfif not openAsExcel>
	</TABLE>
	<CFINCLUDE TEMPLATE="../templates/InternalFooter.cfm">
	
	
</cfif>

