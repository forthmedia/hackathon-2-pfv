<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   ---> 
<!---	1999-04-22  WAB	added code to load up text to go on the fax cover sheet
		1999-01-25  WAB	Added code to save email text as a template file
		2000-12-13 WAB   Added code to check for non Epson fax file being uploaded
		?/5/04	  WAB   Added code to deal with HTML and Text versions of an email
		2004-06-15	WAB changed default rights to templates
		2004-11-04   WAB corrected bug if saved template name has slashes in it
		SWJ  07-11-2008  Modified message variable setting because it was breaking sqlInjectionBlocker took the <script> tag out of this
2011/06		WAB changes related to storing content in phrases	
		2011-07-04	NYB		P-LEN024 
		2011/11/07 WAB 	Only updated communication.contentLastUpdated if the content has actually changed (prevents approved comm being accidentally unapproved)
				Also removed all the Fax stuff 
				moved code to create email templates into communications.cfc
		2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
--->

<cfset commLimits = application.com.settings.getSetting("communications.limits")>
<CFPARAM name="frmFailTo" default= "commDetailsHome.cfm">
<CFPARAM name="frmEmailTemplateName" default= "">

<CFIF NOT checkPermission.Level2 GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="#frmFailTo#">
	<CF_ABORT>
</CFIF>

<CFPARAM name="frmselectid" default="0">
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">
<CFPARAM NAME="frmMsg_Txt" DEFAULT="">
<CFPARAM NAME="frmMsg_HTML" DEFAULT="">

<!--- returns query with all the details of the communication--->
<cfif not isdefined("communication")>
<cfset communication = application.com.communications.getCommunication (#frmCommid#,request.relayCurrentUser.personid)>
</cfif>

<CFIF not communication.canBeEdited()>
	<CFSET message = "Sorry but this communication has already been sent so cannot be edited.">
		<CFINCLUDE TEMPLATE="#frmFailTo#">
	<CF_ABORT>
<cfelseif not communication.hasrightsToEdit()>
				<CFSET message ="Sorry, you do not have rights to edit this communication.">
				<CFSET message = message & " <A HREF=""#thisDir#/commheader.cfm?frmCommID=#frmCommID#&btnTask=edit"">Click here</A> to view the current record.">
						<CFINCLUDE TEMPLATE="#frmFailTo#">
				<CF_ABORT>
</CFIF>

<!--- <CFIF #ListFind(frmCommFormLID,"2")# IS NOT 0 AND #Trim(frmMsg_HTML)# IS ""> --->
<CFIF ListFind("2,5,6,7",frmCommFormLID) IS NOT 0 AND Trim(frmMsg_HTML & frmMsg_TXT ) IS "">
	<CFSET message = "You must enter text for the email message.">
		<cfinclude template="commDetailsHome.cfm">
	<CF_ABORT>
<!--- <CFELSEIF #ListFind(frmCommFormLID,"3")# IS NOT 0 AND #Trim(frmFaxFile)# IS "">	 --->
<CFELSEIF ListFind("3,5,6,7",frmCommFormLID) IS NOT 0 AND Trim(frmFaxFile) IS "">	
	<CFSET message = "You must provide a file for the fax message.">
		<cfinclude template="commDetailsHome.cfm">
	<CF_ABORT>
</CFIF>	

<CFSET filesAccepted = true>
<CFSET Message = "">

<!--- create a list of the unique comm formats --->

<CFIF ListFind("5,6,7",frmCommFormLID) IS NOT 0>
	<CFSET comtypelst = "2,3">
<CFELSE>
	<CFSET comtypelst = frmCommFormLID>
</CFIF>		

<CFTRANSACTION>
	<!--- WAB 2011/11/07 
	only updated communication.contentLastUpdated if the content has actually changed 
	moved the updated query to end of transaction and check result of SaveCommContent()
	actually then moved the update into SaveCommContent(), so done when this function is called from elsewhere
	--->

	<CFLOOP INDEX="LoopLID" LIST=#Variables.comtypelst#> <!--- WAB 2011/11/07 loop a bit superfulous these days since we no loger support Fax --->
		<CFIF LoopLID IS "2">
			<!--- email --->
			<CFSET CommFileName = "#frmCommID#-#LoopLID#.txt">
			<CFSET FullFilePath = "\CommFiles\emails\">
			
			<!--- 
			WAB 2011/11/07 this section no longer needed.  
			section which passed frmCorrectUnTrackedLinks has been commented out and there is now a links tab
			<cfif isdefined("frmCorrectUnTrackedLinks")>
				<cfset frmMsg_HTML = application.com.communications.replaceNonTrackedLinksInEmail(frmMsg_HTML,communication.linkToSiteURL)>
				<cfset message = message & "Links have been modified">
			<cfelse>
				<cfset untrackedLinks = application.com.communications.checkForNonTrackedLinksInEmail (frmMsg_HTML)>
				<!--- WAB 2011/06/21 mods because changed structure of untrackedLinks --->
				<cfif arrayLen(untrackedLinks) gt 0 >
					<cfset message = message & "The following links are not being tracked:<BR>">
					<cfloop index = "link" array= #untrackedLinks#>
						<cfset message = message & "#link.innerHTML#  : #link.attributes.href# <BR>">
					</cfloop>
				</cfif>
			</cfif>	
			--->
			
			
			<cfset argumentCollection = {HTMLBody = frmMsg_HTML, TextBody = frmMsg_txt}> <!--- use collection so that HTML does not appear in the debug--->
			<cfset saveContentResult = communication.SaveCommContent(argumentCollection = argumentCollection)>

			<!--- 	
				This bit deals with saving text as a template 
				WAB 2011/11/07 moved code to communications.cfc
			--->	
			<CFIF frmEmailTemplateName IS NOT "">
				<cfset application.com.communications.saveEmailTemplate (HTMLBody = frmMsg_HTML, TextBody = frmMsg_txt, templateName = frmEmailTemplateName)>
			</CFIF>   

			<CFSET CommFileSize = "0"> <!--- initialize --->
			<CFDIRECTORY ACTION="List" DIRECTORY="#application.paths.userFiles##FullFilePath#"
					   NAME="dirlist" FILTER="#CommFileName#">
				   
			<CFOUTPUT QUERY=dirlist>
 				<CFIF name IS CommFileName AND type IS "File">
					<CFSET CommFileSize = #Round(size/1024)#>
				</CFIF>
			</CFOUTPUT>		

 			<CFIF CommFileSize GT commLimits.eattmaxsize_kb>
				<CFSET filesAccepted = false>
				<CFSET message = message & "<P>The email message you uploaded was not saved since it exceeded the maximum allowed size of #commLimits.eattmaxsize_kb# kilobytes.">

			</CFIF>
								

		<CFELSE>
			<!--- neither fax nor email so error out --->
			<CFSET message ="You did not provide enough information to set up a communication.">
			<cfinclude template="commDetailsHome.cfm">
			<CF_ABORT>
	
		</CFIF>

		<CFIF filesAccepted and communication.hasEmailFiles() is not 1>
			<!--- file sizes are okay so continue --->
			<!--- if files already exist then names will all be the same so don't worry about updating the database --->
				
			<!--- loop through list of files inserting details into the database --->
			<CFLOOP index="I" FROM="1" to="#listlen(commfilename)#">
			
			<CFQUERY NAME="insertCommFile" DATASOURCE="#application.SiteDataSource#" DEBUG>
			INSERT INTO Commfile (
				CommID,
				CommFormLID,
				Directory,
				FileName,
				IsParent,
				FileSize,
				CreatedBy,
				Created,
				LastUpdatedBy,
				LastUpdated)
			VALUES (
				<cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#LoopLID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#listgetat(FullFilePath,I)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#listgetat(CommFileName,I)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				0, <!--- really should be null --->
				<cf_queryparam value="#listgetat(CommFileSize,I)#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</CFQUERY>
			
			<CFIF I is 1>    <!--- this is the parent --->
				<CFQUERY NAME="updCommFileParent" DATASOURCE="#application.SiteDataSource#" DEBUG>
				UPDATE Commfile
				   SET IsParent = CommFileID
					 WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND	CommFormLID =  <cf_queryparam value="#looplid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				   AND LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
				</CFQUERY>
				<CFQUERY NAME="getCommFileParent" DATASOURCE="#application.SiteDataSource#" DEBUG>
					select CommFileID
					from commfile
					where CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND	CommFormLID =  <cf_queryparam value="#looplid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				   AND LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
				</CFQUERY>
			<CFELSE>

				<CFQUERY NAME="updCommFileParent" DATASOURCE="#application.SiteDataSource#" DEBUG>
				UPDATE Commfile
				   SET IsParent =  <cf_queryparam value="#getcommfileparent.commfileid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					 WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND	CommFormLID =  <cf_queryparam value="#looplid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND filename =  <cf_queryparam value="#listgetat(CommFileName,I)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				   AND LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
				</CFQUERY>
			

			</cfif>
			</cfloop>
			
		</CFIF>
		</CFLOOP>
		


<!--- end CF transaction after this if, before the CFINCLUDES --->

 <CFIF not filesAccepted >
	<!--- if file is too big (or something), then will need to redo the communication
			so delete any CommFile records and delete the underlying files
	  --->
		<CFQUERY NAME="FindFiles" DATASOURCE="#application.SiteDataSource#" DEBUG>
			Select CommFileID, Directory, FileName
			FROM CommFile
			WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	
		<CFLOOP QUERY="FindFiles">
					
			<CFSET fullname = application.userFilesAbsolutePath  & #Directory#  & "\" & #FileName#>
		
			<CFIF #FileExists(fullname)#>
				<!--- if file already exists with that name, delete it --->
				<CFFILE ACTION="DELETE"
					FILE="#fullname#">
			</CFIF>
		</CFLOOP>
		
		<CFQUERY NAME="FindFiles" DATASOURCE="#application.SiteDataSource#" DEBUG>
			DELETE 
			FROM CommFile
			WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

			<!--- now all files have been removed and the CommFile records
			deleted from the database--->

		<CFSET message = message & "  Please complete this form again.">	
</CFIF>



</CFTRANSACTION>			

<!---
<cfoutput>
<SCRIPT type="text/javascript">
	var parLoc = parent.location + "&frmCallingTemplate=#URLEncodedFormat('commFile.cfm')#&frmRequestedAction=#form.frmRequestedAction#";
	parent.location = parLoc;
</script>
</cfoutput>
--->
