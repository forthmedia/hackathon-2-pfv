<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Updates a specified communication 
		but only if the communication has not yet been sent
		and has not been changed
		
		this is very similar to commdelete.cfm
--->
<CFPARAM NAME="freeze_date" DEFAULT="#Now()#">

<CFSET message = "">
<CFSET delmessage = ""> <!--- comms successfully deleted --->
<CFSET sentmessage = ""> <!--- comms not deleted since sent --->
<CFSET chgmessage = ""> <!--- comms not deleted since sent --->
<CFSET missmessage = ""> <!--- comms not found --->
<CFSET invalidmessage = ""> <!--- not authorised to edit --->

<!--- only admin users, creators or updators can edit a communication 
	
		use qrycheckperms.cfm to determine if admin user
		but rerun qrycheckperms.cfm for communication security 
		to reset query values as expected for entire template 
		in communication subdirectory	  
 --->


<CFIF IsDefined("Form.frmCommID")>
	<CFLOOP INDEX="thisItem" LIST="#frmCommID#">
	
		<CFSET thisCommID = ListFirst(thisItem,"-")>
		<CFSET thisLastUpdated = ListRest(thisItem,"-")>

		<CFQUERY NAME="chkIfSent" DATASOURCE="#application.SiteDataSource#">
		SELECT c.CommID,
		       c.Title,
			   c.Sent,
			   c.LastUpdated,
			   c.LastUpdatedBy,
			   c.CreatedBy
		  FROM Communication AS c
		 WHERE c.CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
			
		<CFIF chkIfSent.RecordCount IS 0>
			<!--- comm not found --->
			<CFSET missmessage = missmessage & "<LI>Communication ID: #thisCommID#">
		
		<CFELSEIF YesNoFormat(chkIfSent.Sent) IS "Yes">
			<!--- comm already sent --->
			<CFSET sentmessage = sentmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">

		<CFELSEIF YesNoFormat(chkIfSent.Sent) IS "No" AND DateDiff("s",chkIfSent.LastUpdated,thisLastUpdated) IS NOT 0>
			<!--- comm changed by someone else --->
			<CFSET chgmessage = chgmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">
			
		<CFELSEIF NOT (chkIfSent.createdBy IS request.relayCurrentUser.usergroupid 
					   OR chkIfSent.LastUpdatedBy IS request.relayCurrentUser.usergroupid 
					   OR request.isAdmin)>
 	
			<CFSET invalidmessage = invalidmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">
			
		<CFELSEIF YesNoFormat(chkIfSent.Sent) IS "No">
			<!--- test explicitly for no since, in theory, 
					3rd option is no records returned  --->

			<CFSET delmessage = delmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">
<!--- 			<CFTRANSACTION> --->
				<CFQUERY NAME="updComm" DATASOURCE="#application.SiteDataSource#">
					UPDATE Communication
					   SET CommStatusID = 1,
					       SendDate = null,
<!--- 						   LastUpdatedBy = #request.relayCurrentUser.usergroupid#,	
							don't change lastupdatedBy since will affect where the confirmations
							go in tskcommsend.cfm		    --->
						   LastUpdated =  <cf_queryparam value="#freeze_date#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
					 WHERE Sent = 0
					   AND CommStatusID <> -1
					   AND CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
					   AND DateDiff(ss,#thisLastUpdated#,LastUpdated) = 0
				</CFQUERY>
<!--- 			</CFTRANSACTION> --->
		</CFIF>		
</CFLOOP>	

<CFELSE>
	<!--- frmCommID not defined --->
	
	<CFSET message = message & "<P>The specified communication could not be found.">	

</CFIF>	

<cfinclude template="#thisDir#/commviewpend.cfm">	
<CF_ABORT>



<CFIF sentmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) have already been sent so they could not be paused:<UL>" & sentmessage & "</UL>">		
</CFIF>

<CFIF chgmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be paused because they have been changed by another user since you viewed them:<UL>" & chgmessage & "</UL>">		
</CFIF>


<CFIF delmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) were successfully paused:<UL>" & delmessage & "</UL>">		
</CFIF>

<CFIF invalidmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be paused because you are not authorised to change them:<UL>" & invalidmessage & "</UL>">		
</CFIF>

<CFIF missmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be found:<UL>" & missmessage & "</UL>">		
</CFIF>



<cfinclude template="#thisDir#/commMenu.cfm">	
<CF_ABORT>


