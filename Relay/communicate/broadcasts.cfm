<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		twitter.cfm
Author:			NJH
Date started:	18-02-2013

Description:	Manage communication tweets.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-05-26			DXC			Case 444779 - Removed restriction of listed accounts to just those created by the current login user as it was resulting in users trying to add the same @twitter account multiple times...

Possible enhancements:


 --->
<cfparam name="getBroadcasts" default="">
<cfparam name="sortOrder" default="sendDate desc,account,text">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="messageID" type="numeric" default="0">



<cfset request.messageSent = false>

<cfset xmlSource = "">
<cfset services = application.com.settings.getSetting('socialMedia.services')>
<cfset twitterEnabled = listFindNoCase(services,'twitter')>
<cfset facebookEnabled = listFindNoCase(services,'facebook')>

<cfif not structKeyExists(form,"messageId")>
	<cfset form.messageID = messageId>
</cfif>

<cfquery name="getBroadcastAccounts" datasource="#application.siteDataSource#">
	select * from
	(
	<cfif twitterEnabled>
	select distinct ba.account, ba.broadcastAccountID,s.name,case when b.messageId is not null then 1 else 0 end as selected
	from broadcastAccount ba
		inner join service s on ba.serviceID = s.serviceID
		inner join serviceEntity se on ba.broadcastAccountID = se.entityID and se.serviceID = s.serviceID
		left join broadcast b on b.broadcastAccountId = ba.broadcastAccountId and b.messageID=#form.messageID#
	where
			<!--- START DXC - 444779--->
			<!---ba.createdBy=#request.relayCurrentUser.userGroupID#
			and--->
			<!--- END DXC - 444779--->
		s.serviceTextID = 'twitter'
		<cfif facebookEnabled>
		union
		</cfif>
	</cfif>
	<cfif facebookEnabled>
	select distinct ba.account, ba.broadcastAccountID,s.name,case when b.messageId is not null then 1 else 0 end as selected from broadcastAccount ba
		inner join service s on ba.serviceID = s.serviceID
		left join broadcast b on b.broadcastAccountId = ba.broadcastAccountId and b.messageID=#form.messageID#
	where
			<!--- START DXC - 444779--->
			<!---ba.createdBy=#request.relayCurrentUser.userGroupID#
			and--->
			<!--- END DXC - 444779--->

	ba.accesstoken is not null and ba.serviceAccountID is not null
	and s.serviceTextID = 'facebook'
	</cfif>
	) as getBroadcastAccounts
	order by name,account
</cfquery>

<cfif getBroadcastAccounts.recordCount eq 0>
	<cfset application.com.relayUI.message(message="Please connect some social accounts to send messages to.",type="info")>
	<cfif structKeyExists(url,"editor")>
		<cfset application.com.request.setTopHead(topHeadCfm="commTopHead.cfm",pageTitle="Messages")>
		<cf_abort>
	</cfif>
</cfif>

<!--- function used once a record has been saved in xmlEditor. Send a tweet --->
<cffunction name="processSave" returnType="struct">
	<cfset var result = {isOk=true,message=""}>
	<cfset var broadcastMessage = "">
	<cfset var picture="">
	<cfset var phraseTextPrefix = "PHRUPD_TEXT_#application.entitytypeID.message#_">
	<cfset var successAccounts = "">
	<cfset var errorAccounts = "">

	<cfif form.isOK>

		<cfif structKeyExists(arguments,"imageUrl")>
			<cfset picture=application.com.relayCurrentSite.getExternalSiteURL() & arguments.imageUrl>

			<cfquery name="updateImageURL" datasource="#application.siteDataSource#">
				update message
				set image = <cf_queryParam value="#picture#" cfsqltype="cf_sql_varchar">
				where messageID = <cf_queryParam value="#arguments.messageID#" cfsqltype="cf_sql_integer">
			</cfquery>
		<cfelse>
			<cfquery name="getImageURL" datasource="#application.siteDataSource#">
				select image from message
				where messageID = <cf_queryParam value="#arguments.messageID#" cfsqltype="cf_sql_integer">
			</cfquery>
			<cfset picture = getImageURL.image>
		</cfif>

		<cfquery name="updateBroadcast" datasource="#application.siteDatasource#">
		<cfloop list="#form.broadcastAccountID#" index="broadcastAccount">
			if not exists (select 1 from broadcast where broadcastAccountID =  <cf_queryparam value="#broadcastAccount#" CFSQLTYPE="cf_sql_integer" > and messageID  =  <cf_queryparam value="#arguments.messageID#" CFSQLTYPE="CF_SQL_Integer" > )
				insert into broadcast (broadcastAccountID,messageID,createdBy,lastUpdatedBy,lastUpdatedByPerson) values (#broadcastAccount#,#arguments.messageID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personId#)
		</cfloop>

			delete from broadcast where messageID =  <cf_queryparam value="#arguments.messageID#" CFSQLTYPE="cf_sql_integer" > <cfif form.broadcastAccountId neq "">and broadcastAccountID  not in ( <cf_queryparam value="#form.broadcastAccountId#" CFSQLTYPE="cf_sql_integer"  list="true"> )</cfif>
		</cfquery>

		<cfif structKeyExists(form,"schedule") and form.schedule eq "now" and form.sendDate eq "">

			<cfif form.sendDate neq arguments.sendDate>

				<cfquery name="updateSendDate" datasource="#application.siteDataSource#">
					Update message
					set sendDate = null
					where messageID = <cf_queryParam value="#arguments.messageID#" cfsqltype="cf_sql_integer">
				</cfquery>
			</cfif>

			<cfquery name="getBroadcastAccounts" datasource="#application.siteDataSource#">
				select accessToken,serviceTextId,serviceAccountID,broadcastAccountID,account
				from broadcastAccount ba
					inner join service s on ba.serviceID = s.serviceID
				where broadcastAccountID  in ( <cf_queryparam value="#form.broadcastAccountID#" CFSQLTYPE="cf_sql_integer"  list="true"> )
			</cfquery>

			<cfif structKeyExists(form,"#phraseTextPrefix##arguments.messageID#_")>
				<cfset broadcastMessage = form["#phraseTextPrefix##arguments.messageID#_"]>
			<cfelseif structKeyExists(form,"#phraseTextPrefix#MESSAGEID_")>
				<cfset broadcastMessage = form["#phraseTextPrefix#MESSAGEID_"]>
			</cfif>

			<cfif broadcastMessage neq "">

				<cfloop query="getBroadcastAccounts">
					<cfif serviceTextID eq "twitter">
						<cfset shareResult = application.com.twitter.share(comment=broadcastMessage,entityID=broadcastAccountID,entityTypeID=application.entityTypeId.broadcastAccount,image=picture,link=arguments.url)>
					<cfelseif serviceTextID eq "facebook">
						<cfset shareResult = application.com.facebook.postPageData(pageID=serviceAccountID,access_token=accessToken,message=broadcastMessage,picture=picture,link=arguments.url)>
					</cfif>

					<cfif shareResult.isOK>
						<cfquery name="setSentDate" datasource="#application.siteDataSource#">
							update broadcast set sentDate = getDate(),
								lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
								lastUpdated = getDate(),
								lastUpdatedByPerson = #request.relayCurrentUser.personID#
							where broadcastAccountID = <cf_queryParam value="#broadcastAccountID#" cfsqltype="cf_sql_integer">
								and messageID = <cf_queryParam value="#arguments.messageID#" cfsqltype="cf_sql_integer">
						</cfquery>

						<cfset successAccounts = listAppend(successAccounts,account)>
						<cfset request.messageSent=1>
					<cfelse>
						<cfset errorAccounts = listAppend(errorAccounts,account)>
					</cfif>
				</cfloop>

			</cfif>
		</cfif>

	</cfif>

	<cfif successAccounts neq "">
		<cfset result.message = result.message & "<br>Message sent to the following accounts: "&successAccounts>
	</cfif>
	<cfif errorAccounts neq "">
		<cfset result.message = result.message & "<br>Messages were not able to be sent to the following accounts: "&errorAccounts>
	</cfif>

	<cfreturn result>
</cffunction>

<cfif structKeyExists(url,"editor")>

	<cfif not structKeyExists(form,"broadcastAccountID")>
		<cfquery name="getSelectedAccounts" dbtype="query">
			select broadcastAccountID from getBroadcastAccounts where selected=1
		</cfquery>
		<cfset form.broadcastAccountID = valueList(getSelectedAccounts.broadcastAccountID)>
	</cfif>

	<cf_includeJavascriptOnce template="/javascript/social.js">
	<cf_includejavascriptOnce template="/javascript/date.js">
	<cf_includejavascriptOnce template="/communicate/js/broadcast.js">

	<cf_head>
		<cfoutput>
			<script>
				var selectedBroadcastAccounts = '###replace(form.broadcastAccountID,",",",##","ALL")#';
			</script>
		</cfoutput>
	</cf_head>

	<cfset scheduledSet = false>

	<cfif isDefined("broadCastID")>
		<cfquery name="getSendDate" datasource="#application.siteDataSource#">
			select m.sendDate, b.sentDate from broadcast b
			inner join message m on b.messageID = m.messageID
			where b.broadcastID=<cf_queryParam value="#broadcastID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfif getSendDate.sentDate eq "">
			<cfset scheduledSet = true>
		<cfelse>
			<cfset request.messageSent = true>
		</cfif>

	<cfelse>
		<cfset scheduledSet = true>
	</cfif>

	<cfset shortenURL="">

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="message" title="Message Editor" readonly="request.messageSent" onSubmit="checkTweetLength();">
				<field name="messageID" label="phr_message_messageID" description="This records unique ID." control="html"></field>
				<field name="" label="phr_message_text" description="The body of the message" required="true" control="translation" parameters="phraseTextID=text,displayAs=TextArea,maxChars=140,shortenURL=true,showOtherTranslationsTable=false,showCurrentLanguageString=false,showTranslationLink=false" submitFunction="checkTweetLength();"/>
				<lineItem label="phr_message_url">
					<field name="url" label="phr_message_url" description="The url of the message" size="70" validate="url" message = "must be a valid url" <cfif not request.messageSent>onfocus="if (this.value == '') {this.value='http://';}" onBlur="if (this.value == 'http://') {this.value=''}"</cfif>/>
					<field name="shortenURL" label="phr_broadcast_shortenUrl" description="Utility to shorten any url contained in the text" control="html"></cfoutput>
						<cfif not request.messageSent>
							<cfsavecontent variable="shortenURL">
								<cfoutput>
									<a href="" onclick="javascript:shortenThisUrl();return false;">Shorten</a> <span title="Enter the url you wish to shorten. Click 'Shorten' and copy and paste the shortened url into your text."><img src="/images/misc/iconhelpsmall.gif"></span>
								</cfoutput>
							</cfsavecontent>
						</cfif>
						<cfoutput>#htmlEditFormat(shortenURL)#
					</field>
				</lineItem>
				<field name="" control="file" label="phr_message_image" acceptType="image" parameters="allowFileDelete=false,defaultImage=true,filePrefix=Thumb_,displayHeight=80,height=80,width=80" description=""></field>
				<cfif application.com.settings.getSetting("campaigns.displayCampaigns")><field name="campaignID" label="phr_Sys_AssociateCampaign" control="select" value="campaignID" display="campaignName" query="func:com.relayCampaigns.GetCampaigns()" nulltext="Phr_Ext_SelectAValue"/></cfif>
				<field label="phr_broadcast_sendTo" name="broadcastAccountID" control="html" required="true"></cfoutput>
					<cfsavecontent variable="accounts">
						<cfoutput>
							<cf_input type="hidden" name="broadcastAccountID_required" label="phr_broadcast_sendTo" submitFunction="verifyObjectv2(form.broadcastAccountID,'phr_sys_formvalidation_req','true')">
							<table></cfoutput>
						<cfoutput query="getBroadcastAccounts" group="name">
							<tr>
								<td><b>#name#:</b></td>
								<td>
									<cfoutput>
										<cf_input disabled="#request.messageSent#" type="checkbox" id="#lcase(name)#_#broadcastAccountID#" account="twitter" name="broadcastAccountID" value="#broadcastAccountID#" checked=#listFind(form.broadcastAccountID,broadcastAccountId)#><label for="#lcase(name)#_#broadcastAccountID#">#account#</label>
									</cfoutput>
								</td>
							</tr>
						</cfoutput>
						<cfoutput></table></cfoutput>
					</cfsavecontent>
					<cfoutput>#htmlEditFormat(accounts)#
				</field>

				<field name="schedule" label="phr_broadcast_schedule" description="Date/Time to send tweet" control="html">
					<cfsavecontent variable="scheduleInput">
						<cfoutput>
							

							<label for="scheduleLater" class="radio-inline">
								<input <cfif request.messageSent>disabled="#request.messageSent#"</cfif> type="radio" id="scheduleLater" name="schedule" value="schedule">Schedule
							</label>

							<label for="scheduleNow" class="radio-inline">
								<input <cfif request.messageSent>disabled="#request.messageSent#"</cfif> type="radio" id="scheduleNow" name="schedule" value="now" onClick="javascript:document.getElementById('sendDate').removeAttribute('value');javascript:document.getElementById('sendDate_min').removeAttribute('value');javascript:document.getElementById('sendDate_hr').removeAttribute('value');">Send Now
							</label>

						</cfoutput>
					</cfsavecontent>
					#htmlEditFormat(scheduleInput)#
				</field>
				<field label="phr_broadcast_senddate" name="sendDate" thisFormName="editorForm" submitFunction="isDateInFuture" showAsLocalTime = "true" showTime="true" required="#scheduledSet#" requiredFunction="if ($('scheduleLater')) {return isItemInCheckboxGroupChecked($('scheduleLater'),'schedule')} else {return false}" disableToDate="#request.requestTime#" enableRange="true"/>
				<cfif isDefined("broadCastId") or (isDefined("broadCastID") and broadcastID neq 0)>
					<field name="sendDate" label="phr_broadcast_sentdate" control="html"/>
				</cfif>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>

	<cfif structKeyExists(form,"frmRowIdentity")>
		<cfquery name="getMessagesToCancel" datasource="#application.siteDataSource#">
			select * from  message
			where messageID in (select messageID from broadcast where broadcastID in (<cf_queryparam value="#form.frmRowIdentity#" cfsqltype="cf_sql_integer" list="true">)
				and sentDate is not null)
		</cfquery>

		<cfif getMessagesToCancel.recordcount neq 0>
			<cfset messagesNotCancelled = "">
			<cfloop query="getMessagesToCancel">
				<cfset messagesNotCancelled = messagesNotCancelled & '"' & text_defaultTranslation & '",'>
			</cfloop>
			<cfset messagesNotCancelled = left(#messagesNotCancelled#,len(#messagesNotCancelled#)-1)>
			<cfoutput><script>alert('phr_messageNotCancelled #messagesNotCancelled# phr_messageAlreadySent');</script></cfoutput>
		</cfif>

		<cfquery name="cancelSelectedMessages" datasource="#application.siteDataSource#">
			update message
			set senddate = null,lastUpdated=getDate(),lastUpdatedBy=<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">
			where messageID in (select messageID from broadcast where broadcastID in (<cf_queryparam value="#form.frmRowIdentity#" cfsqltype="cf_sql_integer" list="true">)
				and sentDate is null)
		</cfquery>
	</cfif>

	<cfquery name="getBroadcasts" datasource="#application.siteDataSource#">
		select * from (
			select m.messageID,broadcastID,b.broadcastAccountID,ba.account,text_defaultTranslation as text,isNull(b.sentDate,m.sendDate) as sendDate, case when b.sentDate is not null then 1 else 0 end as sent, 'Tweet: '+left(text_defaulttranslation,10)+'...' as tabText
			from broadcast b
				inner join message m on m.messageID = b.messageID
				left join broadcastAccount ba on b.broadcastAccountID = ba.broadcastAccountID

						<!--- START DXC - 444779--->
			<!--- where b.createdBy=#request.relayCurrentUser.userGroupID# --->
			<!--- END DXC - 444779--->

		) as base
		where 1=1
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>

	<cfset application.com.request.setTopHead(topHeadCfm="commTopHead.cfm")>
</cfif>

<cfinclude template="broadcastFunctions.cfm">

<cf_listAndEditor
	xmlSource="#xmlSource#"
	keyColumnList="text"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(broadcastId)##'*comma'##jsStringFormat(tabText)##'*comma'/communicate/broadcasts.cfm?editor=yes&hideBackButton=true&messageID=##jsStringFormat(messageID)##&broadcastId=##jsStringFormat(broadcastId)##'*comma{reuseTab:true*commaiconClass:'communicate'});return false;"
	showTheseColumns="text,account,sendDate,sent"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getBroadcasts#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	booleanFormat="sent"
	hideTheseColumns=""
	FilterSelectFieldList="account"
	dateTimeFormat="sendDate"
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	postSaveFunction = #processSave#
	columnTranslationPrefix="phr_report_twitter_"
	rowIdentityColumnName="broadcastId"
	functionListQuery="#comTableFunction.qFunctionList#"
>