<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			commFile.cfm
Author:				BOCC
Date started:		1997

Description:		Used to create the main body of the message

Mediatypes are defined in CommFormLID as follows:
	2 Email
	3 Fax
	4 Download (not relevant here)
	5 Always by both email and Fax
	6 Either Fax or email with a preference for email
	7 Either fax or email with a prefernece for fax

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid
				and to run in a popup window
				Added code to load up an email template

		2000-06-09	DJH		added code to allow email text to be included (mainly for events)
25 Oct 2001		CPS		Incorporate call to eWebEditPro if this is the default editor.
2002-08-03	SWJ	Modified to follow the more simple work flow.
2004-05-01		AH mofified to use devedit for HTML editing
2004-05-10 ->	WAB allows both text and HTML editing
			WAB modified to allow editing of an existing communication

2008-02-04	AJC	Bugs - Sony - WYSIWYG Not Expanding
WAB LID 2515 2009/09/07 #application. UserFilesPath# removed from setimagepath
2011-11-20 NYB	LHID 4918 - added support for CommEmailFormat being db driven
2011/06		WAB changes related to storing content in phrases
2011-07-04	NYB		P-LEN024
2011/10/19	WAB	added ability to upload attachments on this page (and removed from ckeditor)
			WAB	removed all references to related files as attachments.  Was used for sending event emails but this functionality no longer works/used.
2011/10/20	WAB added <!-- Start/EndDoNotTranslate --> around editing areas
2011/11//11	WAB add HTMLEditFormat
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
2012-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-04-03 	NYB Case 433419 rearranged code, moved Attachments section to it's own form
2013-05-14	WAB	Created separate function for creating default content
				Deal with update of external stylesheet
2014-08-15	RPW	Improve Comms Template description
2016-01-28  SB  Removed tables and added cf_relayFormDisplay
2016/02/09	NJH	Changed check file to simply trigger form submit. Validation now down through RW form validation.
Enhancements still to do:

TBD Need to warn if editing an already approved communication



--->

<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="frmPersonids" DEFAULT="">
<CFPARAM NAME="frmRunInPopUP" DEFAULT="false">
<CFPARAM Name="frmEmailTemplateName" Default="">
<CFPARAM Name="frmEmailTemplate" Default="">
<CFPARAM Name="frmFaxFileName" Default="">
<CFPARAM Name="frmCommEmailAtt" Default="">
<CFPARAM Name="frmNextPage" Default="">
<CFPARAM name="frmmsg_HTML" default="">
<CFPARAM name="frmmsg_TXT" default="">
<CFPARAM name="stepName" default="">
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">
<CFPARAM name="readonly" default="false">


<CFSET securitylist = "CommTask:read">
<cfinclude template="/templates/qrycheckperms.cfm">

<CFIF NOT checkPermission.Level2 GT 0>
	<CFSET message="You are not authorised to use this function">
	<CF_ABORT>
</CFIF>

<!--- returns query with all the content --->
<cfset communication = application.com.communications.getCommunication (commid = frmcommid,personid = request.relaycurrentuser.personid)>
<cfset frmCommEmailFormat = application.com.communications.getCommEmailFormats(CommEmailFormatID=communication.CommEmailFormatID).CommEmailFormat>

<!--- <CFQUERY NAME="communicationTitle" DATASOURCE="#application.SiteDataSource#">
	SELECT c.CommID,c.Title, c.Description, c.CommFormLID, c.sent, c.replyto, ll.ItemText as commForm
	FROM Communication AS c inner join lookupList ll
	ON c.CommFormLID = ll.lookupID
	WHERE c.CommID = #frmCommID#
	AND ll.lookupID in (2,3,5,6,7)
</CFQUERY> --->



<cfif not communication.hasEmailContent()>
	<cfset editingExistingComm = false>
<cfelseif communication.canBeEdited()>
	<cfset editingExistingComm = true>
<cfelse >
	<CFSET message="This communication has already been sent and can not be edited">
	<CF_ABORT>
</cfif>

<cfif not editingExistingComm >
<!--- this is a new communication  --->

	<cfset defaultContent = communication.getDefaultContent()>

	<cfset frmMsg_HTML = defaultContent.html>
	<cfset frmMsg_TXT = defaultContent.text>

	<CFPARAM name="frmfaxcover" default="">

<CFELSE>
		<!--- this is editing an existing communication
			need to load up the existing files into variables
		--->
	<cfset emailContent = communication.getContent().email>

	<!--- WAB 2013-05-13 If external stylesheet has been updated then update the style block
		But only if the communication is not approved or scheduled
	--->
	<cfif emailContent.html is not "" and not (communication.isApprovedOrPartlyApproved() OR communication.isScheduled()) AND communication.hasStyleSheetBeenUpdated()>
		<cfset emailContent.html = communication.updateStyleBlock(emailcontent.html)>
		<cfset communication.SaveCommContent(htmlbody = emailContent.html)>
		<cfset styleSheetMessage = "Content has been updated with changes in Stylesheet #communication.stylesheet#.">
	</cfif>

	<cfset frmMsg_HTML = emailContent.html>
	<cfset frmMsg_TXT = emailContent.text>
</cfif>
<!---	not needed WAB 2012-03-08
<cfset nonTrackedLinks = application.com.communications.checkForNonTrackedLinksInEmail (frmMsg_HTML)>
--->
<!--- if supplied as a form parameter, use it --->

<!--- ==========================================================
		The radio preference buttons and the email text may exist
		if this page is returned to because the files were too big --->



<form action="commDetailsHome.cfm" method="post" enctype="multipart/form-data" name="commFileForm" id="commFileForm">
	<cf_relayFormDisplay bindValidationWithListener=false autoRefreshRequiredClass=true class="">
		<div class="communicationsContainer">
			<cfoutput>
				<h2>Phr_Sys_comm_Step_#htmleditformat(stepName)#_Header</h2>
			</cfoutput>

			<!--- If communication is approved then this function will return a block of warning text --->
			<cfoutput>#communication.getEditMessageBlock()#</cfoutput>

			<cfif isDefined("styleSheetMessage")>
				<cfoutput>#application.com.relayUI.message(message = styleSheetMessage, type="warning", showclose=false)#</cfoutput>
			</cfif>

			<CFIF IsDefined("message") and Trim(message) IS NOT "">
				<CFOUTPUT><p class="MessageText">#application.com.security.sanitiseHTML(message)#</p></CFOUTPUT>
			</CFIF>



			<cfif communication.hasEmailContent()>
				<cfset validateMergedContent = communication.prepareAndValidateContent (doEMail = true,domessage=false)>
				<cfif not validateMergedContent.isOK>
					<cfoutput><div class="errorblock">#validateMergedContent.errorMessage#</div></cfoutput>
				</cfif>
			</cfif>


		<!--- <INPUT TYPE="HIDDEN" NAME="frmOldFaxFileID" VALUE="<CFOUTPUT>#Variables.qryOldFaxFileID#</CFOUTPUT>"> --->

		<!--- <INPUT TYPE="HIDDEN" NAME="frmCommFormLID" VALUE="<CFOUTPUT>#communication.CommFormLID#</CFOUTPUT>"> --->
		<!--- what about last_updated??? --->


				<CFOUTPUT>
				<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" ID="frmCommID" VALUE="#communication.CommID#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmCommFormLID" VALUE="#communication.CommFormLID#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmCallingTemplate" VALUE="#frmCallingTemplate#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmnextpage" VALUE="#frmNextPage#">
				<INPUT TYPE="HIDDEN" NAME="frmRequestedAction" ID="frmRequestedAction" VALUE="">
				<cfif isdefined("frmFailTo")><CF_INPUT TYPE="HIDDEN" NAME="frmFailTo" VALUE="#frmFailTo#">		</cfif>
				</CFOUTPUT>


				<!--- ==============================================================================
				      EMAIL SECTION
				=============================================================================== --->
				<CFIF listFind("2,5,6,7",communication.CommFormLID) neq 0>
					<cfif frmCommEmailFormat contains "HTML">
					<!--- Start 2008-02-04	AJC	Bugs - Sony - WYSIWYG Not Expanding --->
					<cfset frmMsg_HTML = application.com.relayTranslations.replaceRelayTagChevronsWithCurlies(frmMsg_HTML)>

					<!---
					NJH 2011/08/01 - removed all the javascript for the CKEditor from here to commDetailsHome.cfm and to a new js file called cfEditor_relay.js in javascript dir.
						This is due to javascript not running properly in cflayout... at the bottom of this page, we call 'loadEditorForCommunications' which then draws the editor
					<cf_includeJavascriptOnce template="/ckeditor/ckeditor.js">
					<cf_includeJavascriptOnce template="/ckfinder/ckfinder.js">--->

					<!-- StartDoNotTranslate -->
					<cfoutput><div class="validation-group"><TextArea name="frmMsg_HTML" class="form-control" id = "frmMsg_HTML" rows = 10 cols=100 submitFunction="if (jQuery(this).val().replace(/<(\/)?(html|head|title|body).*?>/gi,'').trim() == '') {return 'Required'} else {return '';}" relayContext="communication">#htmleditformat(frmMsg_HTML)#</TEXTAREA></div></cfoutput>
					<!-- EndDoNotTranslate -->
					<cfset AjaxOnLoad("loadEditorForCommunications")>
				</CFIF>

				<CFIF frmCommEmailFormat contains "Text">
					<p><b>Message text</b></p>

					<cfmodule template="/templates/textAreaControlBar.cfm"
						textAreaName="frmMsg_TXT"
						formname="commFileForm"
						showHTMLTags="false"
						showRelayTags="false"
						showClickThrus="false"
						showMergeFields="true"
						showFilesForDownload="true"
						valueToDisplay="#frmMsg_TXT#"
						context="communication,TextCommunication"
								>
						<!-- StartDoNotTranslate -->
						<div class="validation-group"><TEXTAREA class="form-control" id="frmMsg_TXT" NAME="frmMsg_TXT" ROWS="20" submitFunction="if (jQuery(this).val() == '') {return 'Required';} else {return '';}"
							COLS="100" WRAP="VIRTUAL"
							ONSELECT="markSelection(this);"
							ONCLICK="markSelection(this);"
							ONKEYUP="markSelection(this);"><CFOUTPUT>#htmleditformat(frmMsg_TXT)#</CFOUTPUT></div></TEXTAREA>
						<!-- StartDoNotTranslate -->
				</CFIF>
					<!--- 2014-08-15	RPW	Improve Comms Template description --->
				<Cf_relayFormElementDisplay relayFormElementType="text" currentValue="#frmemailtemplatename#"  SIZE="30" MAXLENGTH="50" fieldname="frmemailtemplatename" label="Save this as a new Email Template called">

			<!---
			WAB 2011/10/19
			This is my first attempt to do asynchronous file uploading
			Perhaps I should have found a jQuery plugin to do it for me
			Creates an IFrame and posts whole form to IFrame, then looks for response before reloading attachments list
			Could be genericized
			Probably can't handle multiple uploads at a time neatly (although would probably work)
			Could do with a 'working icon'
			Would be somewhat neater if on own form
			 --->
		<!--- START: 2013-04-03 NYB Case 433419 rearranged code, moved Attachments section to it's own form --->

				</CFIF>

		<!--- 	 	<cfif arrayLen(nonTrackedLinks) is not 0>
					<TR>
						 	<td colspan="3"  valign="top" style="padding-top:20px;">
							<B>Note:</B>  Your content contains the following links which will not be tracked:
							<table>
							<!--- WAB 2011/06/21 mods because changed structure of nontrackedLinks --->
							<cfset counter = 0>
							<cfloop index = "link" array= #nontrackedLinks#>
								<cfset counter = counter+1>
								<cfoutput><tr><td style="padding-left:10px">#counter#: </td><td>#link.innerHTML#</td><td><P>#link.attributes.href#</P></td></tr>	</cfoutput>
							</cfloop>
							</table>
							<input type ="checkbox" name="frmCorrectUnTrackedLinks" value = "1"> If you would like them to be automatically corrected, check the box before saving
						</td>
					</tr>
				</cfif>
				 --->

		</div>
	</cf_relayFormDisplay>
</FORM>

<cfoutput>
	<form action="/webservices/callwebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&methodname=uploadAttachment&returnFormat=json&_cf_nodebug=true" method="post" enctype="multipart/form-data" name="commFileFormAttachments" id="commFileFormAttachments">
		<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" ID="frmCommID" VALUE="#communication.CommID#">

		<cfset table_html   = application.com.communications.createAttachmentsTable(attachmentsQuery=communication.getContent().attachments,showdeleteLink = communication.canBeEdited())>
			<div id="attachmentsDivInner">
			#table_html#
			</div>
			<div class="form-group">
				<label>Upload New Attachment</label>
				<input type="file" name="uploadedFile" onchange=doUpload(this)>
			</div>

		<!--- 2013-04-03 NYB Case 433419 due to rearrangement of code a number of lines are no longer needed: --->
		<script>
			 doUpload = function (fileObj) {
				fileObj = $(fileObj);
				fileObj.spinner({position:'right'});
				theForm = $(fileObj.form)
				// create an IFrame to post into
				iframeName = fileObj.name + '_iframe_' + Math.random()
				iframeElement  = new Element ('IFRAME',{'id':iframeName,'name':iframeName,'style':'display:none'})
				fileObjDiv =fileObj.up()
				fileObjDiv.insert({after: iframeElement})
				// we have to post form to somewhere else so save the existing action and target
				// then post
				// then copy the action and target back
				//oldAction = theForm.action
				//oldTarget = theForm.target
				//theForm.action = '/webservices/callwebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&methodname=uploadAttachment&returnFormat=json&_cf_nodebug=true'
				theForm.target = iframeName;
				theForm.submit();

				//theForm.action = oldAction
				//theForm.target = oldTarget
				// set a timeout to look for the response
				setTimeout (function () {checkResponse(fileObj,iframeName,theForm.frmCommID.value) },100)


			}

			checkResponse = function  (fileObj,iframeName,commid) {
				var iframeObj = $(iframeName)
				var iFrameDoc = iframeObj.contentWindow.document

				// in IE iFrameDoc.body doesn't exist until response arrives, in FF it is there anyway, so have to do a series of tests
				if (iFrameDoc.body && iFrameDoc.body.innerHTML != '' && iFrameDoc.body.innerHTML.isJSON()) {
					var result = 	iFrameDoc.body.innerHTML.evalJSON()
					fileObj.removeSpinner()	;
					if (!result.ISOK) {
						alert (result.MESSAGE)
					}
					updateAttachmentDiv(commid)
					//remove iframe
					iframeObj.remove()
					fileObj.up().update(fileObj.up().innerHTML) // this resets the file upload box by rewriting to HTML
				} else {
					setTimeout (function () {checkResponse(fileObj,iframeName,commid) },200)
				}


			}

			deleteAttachment = function  (commid,commfileid,obj) {
				 var page = '/webservices/callwebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&methodname=deleteAttachment&_cf_nodebug=true&returnformat=json'
				obj.spinner({position:'right'})
				 var myAjax = new Ajax.Request(
				 	page,
				 	{
				 		parameters: {frmcommid:commid,frmcommfileid:commfileid} ,
				 		onComplete:  function (requestObject,JSON) {
				 						var JSON = requestObject.responseText.evalJSON(true)
				 						if (!JSON.ISOK) {
				 							alert (JSON.MESSAGE)
				 						}
				 						updateAttachmentDiv (commid)

				 					}
				 	}
				 )

			}


			updateAttachmentDiv  = function  (commid) {

				var page = '/webservices/callwebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&methodname=getAttachmentsTable&_cf_nodebug=true&returnformat=plain'
				var myAjax = new Ajax.Updater(
						'attachmentsDivInner',
						page, {parameters:{frmcommid:commid} })

			}
		</script>
	</FORM>
</cfoutput>

<CFINCLUDE TEMPLATE="saveAndContinueButtons.cfm">
<!--- END: 2013-04-03 NYB Case 433419 --->


