<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			commFileEdit.cfm	
Author:				SWJ
Date started:		2003-04-27
	
Purpose:	To provide a method for editing communications that have not been sent.

Usage:	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
1.  Add a method for editing the subject line.
2.  Add a method for changing the sent status.
3.  Add a method for creating a copy of a particular communication.

 --->




<cf_head>
	<cf_title>Communication Log</cf_title>
</cf_head>


<cfif isDefined("form.saveChanges") and form.saveChanges eq "save changes">
	<cffile action="WRITE"
        file="#application.userFilesAbsolutePath#\commfiles\emails\#fileName#"
        output="#form.emailText#"
        addnewline="No">
</cfif>

<cfparam name="fileName" type="string" default="test.txt">
<cfparam name="pagesBack" type="numeric" default="0">
<cfparam name="subDir" type="string" default="emails">
<cfset pagesBack = pagesBack+1>

<cfset communication = application.com.communications.getCommunication (Commid)>
<cfset editComm = communication.sent>


<cfswitch expression="#subDir#">
	<cfcase value="emails">
		<cfset userFilesSection = "commfiles"></cfcase>
</cfswitch>

<cftry>
	<cffile action="READ" file="#application.userFilesAbsolutePath#\#userFilesSection#\#subdir#\#fileName#" variable="fileText">
<cfcatch type="Any">
	<cfset fileText = "Could not find the file on this server.  This is most likely because this communication was sent on a different server.">
	<cfset editComm = "0">
</cfcatch>
</cftry>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Subject &nbsp;#htmleditformat(Communications.title)#</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="javascript:history.go(-#pagesBack#)" class="Submenu">Back</a> &nbsp;&nbsp;
		</TD>
	</TR>
</TABLE>
<br><br>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td>#htmleditformat(FileText)#</td></tr>
</TABLE>	
<br><br>
<cfif editComm eq "0">
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<tr><td class="Label"><strong>Edit This Email</strong></td></tr>
		<tr><td><p>As this email has not been sent yet you can edit it in the area below.</p></td></tr>
		<tr>
			<td><form method="post" name="emailTextForm">
					<cfmodule template="/templates/textAreaControlBar.cfm" 
						textAreaName="emailText"
						formname="emailTextForm"
						showRelayTags="false"
						showClickThrus="false"
						showSpecialFields="true"
						showFilesForDownload="true">
				
					<textarea cols="100" rows="30" 
						name="emailText"
						ONSELECT="markSelection(this);" 
						ONCLICK="markSelection(this);" 
						ONKEYUP="markSelection(this);">#FileText#</textarea>
					<br><input type="submit" name="saveChanges" value="Save Changes">
					<CF_INPUT type="hidden" name="fileName" value="#fileName#">
					<CF_INPUT type="hidden" name="commid" value="#commid#">
					<CF_INPUT type="hidden" name="pagesBack" value="#pagesBack#">
				</form>
			</td>
		</tr>
		
	</TABLE>
</cfif>
</cfoutput>



