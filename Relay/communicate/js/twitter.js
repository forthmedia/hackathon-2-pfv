/* �Relayware. All Rights Reserved 2014 */
var accountMessageSpan = 'validateAccountMessage';

function checkAccountUniqueOnSubmit() {
	if (checkAccountUnique('submit') == 'false') {
		return phr.twitter_accountalreadyexists;
	};
}

function checkAccountUniqueOnBlur() {
	var $_accountMessageSpan = jQuery('#'+accountMessageSpan);
	var $_account = jQuery('#account');
	
	if ($_accountMessageSpan.length == 0) {
		$_account.after('<span id="'+accountMessageSpan+'"></span>');
	} else {
		if ($_account.attr('isValid') == '') {
			$_accountMessageSpan.html('');
			$_accountMessageSpan.attr('class','');
		}
	}
	
	checkAccountUnique('blur');
}

function checkAccountUnique(validateAt) {
	var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=socialWS&methodName=checkAccountUnique&returnformat=json'
	var result = true;
	var asynchronous = false;
	
	if (validateAt == 'blur') {
		asynchronous = true;
	}
	
	var $_account = jQuery('#account');
	if ($_account.attr('isValid') == '') {
		$_account.attr('isValid','pending');
	}
	
	if ($_account.val() != '' && $_account.attr('isValid') == 'pending') {
		var parameters = {account:$_account.val(),twitterAccountID:jQuery('#twitterAccountID').val()};
		var myAjax = new Ajax.Request(
			page, 
			{
				method: 'get', 
				asynchronous: asynchronous,
				parameters: parameters, 
				onComplete: function (requestObject,JSON) {
					json = requestObject.responseText.evalJSON(true)
					if (!json) {
					} else {
						var $_accountMessageSpan = jQuery('#'+accountMessageSpan);
						if(json.ACCOUNTEXISTS == true) {
							result = false;
							if (validateAt == 'blur') {
								$_accountMessageSpan.html(phr.twitter_accountalreadyexists);
								$_accountMessageSpan.attr('class','errorblock');
								jQuery('#connectAccount').hide();
							}
						} else {
							result = true;
							if (validateAt == 'blur') {
								$_accountMessageSpan.html('<img src="/images/icons/accept.png">');
								jQuery('#connectAccount').show();
							}
						}
						$_account.attr('isValid',result);
                   }
               }
           });
	}
	return $_account.attr('isValid');
}