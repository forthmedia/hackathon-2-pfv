var selectedBroadcastAccounts = '';

function checkTweetLength() {
	var maxLength = 140;
	var textLength = jQuery("textarea[name^='phrupd']").val().length;
	var urllength = jQuery("#url").val().length;
	var sendToTwitterAccount = jQuery("input[id^='twitter_']:checked").length

	if ((textLength+urllength > maxLength) && sendToTwitterAccount > 0) {
		return 'Text and related url cannot exceed '+maxLength +' characters in length';
	}
}

function shortenThisUrl() {
	shortenURL(jQuery('#url').val(), function (newURL) {jQuery('#url').val(newURL)});
}

function isDateInFuture(dateObj) {
	if (jQuery('#scheduleLater').length && isItemInCheckboxGroupChecked(jQuery('#scheduleLater').get(0),'schedule')) {
		var inFuture = isDateFieldInFuture(dateObj);
		if (!inFuture) {
			return 'must be in the future';
		}
	}
	return '';
}

jQuery(document).ready(function() {
	if (selectedBroadcastAccounts != '') {
		jQuery(selectedBroadcastAccounts).prop('checked',true);
	}

	if(jQuery('#sendDate').val() != ''){
		jQuery('#scheduleLater').prop('checked',true);
	}else{
		jQuery('#scheduleNow').prop('checked',true);
	}

	if(jQuery('textarea').prop('readonly')){
		jQuery('#editorForm input').each(function(){
	    	jQuery( this ).prop('disabled',true);
	    });
	}
});