function isDateInFuture(dateObj) {
	if (jQuery('#scheduleLater').length && isItemInCheckboxGroupChecked(jQuery('#scheduleLater').get(0),'schedule')) {
		var inFuture = isDateFieldInFuture(dateObj);
		if (!inFuture) {
			return 'must be in the future';
		}
	}
	return '';
}

jQuery(function() {
	jQuery('#SendToEntityName').autocomplete({
		source: '/webservices/callWebService.cfc?method=callWebService&webServiceName=communicationsWS&methodName=getMessageEntitiesSearch&returnFormat=plain&sendToEntityType='+jQuery('#sendToEntityType').val(),
			minLength: 1, // set to 1 so that we can search for personID as well
			select: function( event, ui ) {
				setSendToEntityID(ui.item.value,ui.item.id);
				jQuery('#SendToEntityName').val('');
				return false;
			}
		});
});

function toggleEntityNameSearch() {
	if (jQuery('#sendToEntityType').val() == 'Person') {
		jQuery('#SendToEntityName').show();
		jQuery('#searchImage_lineItemDiv').show();
	} else {
		jQuery('#SendToEntityName').hide();
		jQuery('#searchImage_lineItemDiv').hide();
	}
}

function setSendToEntityID (value,id) {
	jQuery('#SendToEntityID').empty();
	var newOption = jQuery('<option value="'+id+'">'+value+'</option>');
	jQuery('#SendToEntityID').append(newOption);
}