/* �Relayware. All Rights Reserved 2014 */
/*
2013-08-15	YMA	Case 436313 From Display Name is now stored in a separate table and can be added so needs to save seperately if its new

*/

function removeSelectValue(thisObject) {
		optionToHide = $(thisObject).options[$(thisObject).selectedIndex];
		if (confirm("Are you sure you want to remove '" + optionToHide.text + "'?")) {
			var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=communicationsWS&methodName=disableCommFromNameID&returnformat=json&_cf_nodebug=true'
			
			var myAjax = new Ajax.Request(
				page, 
				{
					method: 'post', 
					asynchronous: false,
					parameters: {optionToRemove:optionToHide.value},
					onComplete: function (requestObject,JSON) {
						json = requestObject.responseText.evalJSON(true)
	
						if(!json.ISOK) {
							alert(json.MESSAGE);
						} else {
							optionToHide.parentElement.removeChild(optionToHide);
						}
		           }
		       });
		}
}