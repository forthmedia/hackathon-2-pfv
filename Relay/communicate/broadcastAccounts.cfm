<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		twitterAccounts.cfm
Author:			NJH
Date started:	18-02-2013

Description:	Manage twitter accounts.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-05-21			DXC			Case 444779 - Removed restriction of listed accounts to just those created by the current login user as it was resulting in users trying to add the same @twitter account multiple times...

Possible enhancements:


 --->
<cfparam name="getAccounts" default="">
<cfparam name="sortOrder" default="sendDate,account">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="service" type="string" default="twitter">
<cfparam name="broadcastAccountID" type="numeric" default="0">

<cfset xmlSource = "">
<cfset showTheseColumns = "account">

<cfif service eq "twitter">
	<cfset thisEntityTypeID = application.entityTypeID.broadcastAccount>
	<cfset thisEntityID = broadcastAccountID>
	<cfset connectAction = "authorise">
	<cfset pageTitle = "Twitter Accounts">
	<cfset showTheseColumns = listAppend(showTheseColumns,"connected")>
<cfelse>
	<cfset thisEntityTypeID = application.entityTypeID.person>
	<cfset thisEntityID = request.relayCurrentUser.personID>
	<cfset connectAction = "authenticate">
	<cfset pageTitle = "Facebook Pages">
</cfif>

<cfset serviceStruct = application.com.service.getService(serviceID=service)>

<!--- This is the case when we have a callback from connecting the twitter account with twitter... we can get the record that we were dealing with --->
<cfif structKeyExists(url,"entityID") and structKeyExists(url,"entityTypeID") and url.entityTypeID eq thisEntityTypeID and service eq "twitter">
	<cfset form.broadcastAccountID = url.entityId>
	<cfset thisEntityId = url.entityID>
</cfif>

<cffunction name="updateBroadcastAccount" returntype="struct">

	<cfset var result = {isOK=true,message=""}>
	<cfset var getAccessTokenForPage = "">
	<cfset var accountsQry = "">

	<cfif arguments.serviceID eq application.com.service.getService(serviceID="facebook").serviceID>
		<cfset accountsQry = application.com.facebook.getPageAccountsAsQuery()>

		<cfquery name="getAccessTokenForPage" dbtype="query">
			select access_token,name,cast(ID as BIGINT) as ID from accountsQry where name='#form.account#'
		</cfquery>

		<cfif getAccessTokenForPage.recordCount>
			<cfquery name="updateAccountRecord" datasource="#application.siteDataSource#">
				update broadcastAccount set accessToken  =  <cf_queryparam value="#getAccessTokenForPage.access_token[1]#" CFSQLTYPE="CF_SQL_VARCHAR" >, serviceAccountID = <cf_queryparam value="#getAccessTokenForPage.ID[1]#" CFSQLTYPE="CF_SQL_VARCHAR" > where broadcastAccountID = <cf_queryparam value="#arguments.broadcastAccountID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		</cfif>
	</cfif>

	<cfreturn result>
</cffunction>

<cfif structKeyExists(url,"editor")>
	<cf_includeJavascriptOnce template="/javascript/social.js">
	<cf_includeJavascriptOnce template="/javascript/openWin.js">
	<cf_includeJavascriptOnce template="/communicate/js/twitter.js" translate="true">

	<cf_head>
	<cfoutput>
		<script>
			function connectAccount(){
				<!--- NJH 2016/08/05 - JIRAPROD201-1521 fix knock on effect of some security work - not sure if this if statement is needed anymore...But it hasn't been working since the broadcastID has been encrypted because we're passing broadcastId as the entityID on the url
					string, which means that we get a checksum error when decrypting it in serviceAccess.cfm
				 <cfif service eq "twitter">
					thisEntityID = document.getElementById('broadcastAccountID').value;
				<cfelse>
				thisEntityID = #thisEntityID#;
				</cfif>
				serviceParams = 'a=#connectAction#&s=#service#&entityTypeID=#thisEntityTypeID#&entityID=' + thisEntityID; --->
				<cfset encryptedParams = application.com.security.encryptQueryString(queryString="a=#connectAction#&s=#service#&entityTypeID=#thisEntityTypeID#&entityID=#thisEntityID#")>
				openWin('/social/ServiceAccess.cfm?#encryptedParams#','AccountLogin','width=480px,height=320px');
			}

			function disconnectAccount() {
				unLinkAccount('#service#',#thisEntityID#,#thisEntityTypeID#)
			}
		</script>
	</cfoutput>
	<cf_head>

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="broadcastAccount" title="#pageTitle# Editor">
				<field name="broadcastAccountID" label="phr_#service#Account_ID" description="This records unique ID." control="html"></field>
				<field name="serviceID" label="" control="hidden" default="#serviceStruct.serviceID#" ></field>
				<lineItem label="phr_#service#Account_account" required="true">
					<field name="account" label="phr_#service#Account_account" labelAlign="" description="The account account name." required="true" <cfif service eq "twitter">validate="regex" pattern="^@" message="Twitter Account must start with an @" submitFunction="checkAccountUniqueOnSubmit" onblur="checkAccountUniqueOnBlur();"<cfelseif service eq "facebook">control="select" query="func:com.facebook.getPageAccountsAsQuery()" value="name" display="name" onChange="$('account').setAttribute('isValid','');" </cfif>/>
					<field name="connect" label=" " description="Connect the #service# account" control="html" condition="not application.com.service.hasEntityBeenLinked(entityID=#thisEntityID#,entityTypeID=#thisEntityTypeId#,serviceID='#service#').linkEstablished and <cfif service eq "twitter">entityID<cfelse>#thisEntityID#</cfif> neq 0">
						<cfsavecontent variable="connectAccount">
							<cfoutput>
								<a href="" class="disconnectConnectAccountLink" id="connectAccount" name="connectAccount" onclick="javascript:connectAccount();return false;">Connect Account</a>
							</cfoutput>
						</cfsavecontent>
						#htmlEditFormat(connectAccount)#
					</field>
					<field name="disconnect" label=" " description="Disconnect the #service# account" control="html" condition="application.com.service.hasEntityBeenLinked(entityID=#thisEntityID#,entityTypeID=#thisEntityTypeId#,serviceID='#service#').linkEstablished">
						<cfsavecontent variable="disconnectAccount">
							<cfoutput>
								<a href="" class="disconnectConnectAccountLink" onclick="javascript:disconnectAccount();return false;">Disconnect Account</a>
							</cfoutput>
						</cfsavecontent>
						#htmlEditFormat(disconnectAccount)#
					</field>
				</lineItem>
				<!--- <field name="text" label="phr_twitterAccount_country" description="The country of the twitter account"/>
						<field name="text" label="phr_twitterAccount_country" description="The country of the twitter account"/>
					--->
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>
	<cfquery name="getAccounts" datasource="#application.siteDataSource#">
		select broadcastAccountID,account,countryID,languageID,s.name,
		case when serviceTextId = 'Facebook' then 1 when se.entityID is not null then 1 else 0 end as connected
		from broadcastAccount ba
			inner join service s on ba.serviceID = s.serviceID
			left join serviceEntity se on ba.broadcastAccountID = se.entityID and se.serviceID = s.serviceID
		where

			<!--- START DXC - 444779--->
			<!---ba.createdBy=#request.relayCurrentUser.userGroupID#
			and--->
			<!--- END DXC - 444779--->
			s.serviceTextID =  <cf_queryparam value="#service#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfquery>

	<cfset application.com.request.setTopHead(topHeadCfm="commTopHead.cfm",pageTitle=pageTitle)>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="broadcastAccounts"
	keyColumnList="account"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(broadcastAccountID)##'*comma'##jsStringFormat(account)##'*comma'/communicate/broadcastAccounts.cfm?editor=yes&hideBackButton=true&service=##jsStringFormat(name)##&broadcastAccountID=##jsStringFormat(broadcastAccountID)##'*comma{reuseTab:true*commaiconClass:'communicate'});return false;"
	showTheseColumns=#showTheseColumns#
	useInclude="false"
	booleanFormat="connected"
	sortOrder="#sortOrder#"
	queryData="#getAccounts#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	groupByColumns=""
	hideTheseColumns=""
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_#service#Account_"
	backForm="/communicate/broadcastAccounts.cfm?service=#service#"
	postSaveFunction = #updateBroadcastAccount#
>