<!--- �Relayware. All Rights Reserved 2014 --->
<!--- <cfset application.com.communications.setCommsParameters()>  <!--- default request parameters set in com.communications.ini, site specific in userfiles/code/cftemplates/communicate.ini --->
 --->
<!--- set up the request.isAdmin variable  if either a communications administrator or an overall administrator --->
<!--- TBD - put this code into communication.cfc --->


<cfset request.isAdmin = application.com.communications.isUserACommunicationsAdministrator()>

<!--- Case 437080 NJH 2013/09/26 check that we have security requirements before checking permission levels, as if we don't, then the checkPermission variable is not set --->
<cfif attributes.securityResultStructure.security is not "" and attributes.securityResultStructure.security is not "none">
	<cfif checkPermission.Level3 IS 1 or checkPermission.Level4 IS 1>
		<cfset userIsAllowedToSendNow = true>
	<cfelse>
		<cfset userIsAllowedToSendNow = false>
	</cfif>
<cfelse>
	<cfset userIsAllowedToSendNow = false>
</cfif>