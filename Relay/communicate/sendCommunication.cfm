<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			sendCommunication.cfm	
Author:				SWJ
Date started:		2002-11-26
	
Description:		This was introduced a) to give us the option to send immediately,
					b) to provide greater security around tskCommSend
					and c) to provide a wrapper for tskCommSend so that we can add a topHead

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/01/05		WAB   Nat had left a CFABORT in the code!
2009/02/24      WAB  problems if message returned via cflocation is very long
2009/06/16      WAB  LID 2361  alterations to the structure returned from tskcommsend.cfm
2012-11-15 		WAB CASE:431825 - Problem passing long message via URL. Resorted to using a session variable
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object

Enhancements still to do:


 --->
<CFPARAM name="frmRunInPopUp" default = false>
<cfparam name="frmHideTopHead" default="false">

<CFSET securitylist = "CommTask:edit">
<!--- NJH 2010/08/12 RW8.3 - removed this include as no longer needed
If including this file from another directory, use the following
<cf_include doDirectoryInitialisation=true applySecurity=true template="/Templates/ApplicationDirectorySecurity.cfm">
<cfinclude template="/Templates/ApplicationDirectorySecurity.cfm"> --->

<!---
<cfif not frmHideTopHead>
	<cfinclude template="#thisDir#/commtophead.cfm">
</cfif>
--->

<cfparam name = "frmCommID" default= "">
<cfparam name = "frmSelectID" default= "">
<cfparam name = "frmCommSelectLID" default= "">
<cfparam name = "frmTest" default= "No">
<cfparam name = "frmResend" default= "No">
<cfparam name = "frmPersonids" default= "">
<cfparam name = "frmSubjectPrefix" default= "">
<cfparam name = "frmFooter" default= "">
<cfparam name = "frmReplyTo" default= "">
<cfparam name = "frmshowLinkBackToCommunication" default = "false">
<cfparam name = "sendForApproval" type="boolean" default = "false"> <!--- NJH 2008/11/11 Bug Fix Sony Issue 1158 --->
<!--- START: 2011-11-14 NYB added: --->
<cfparam name = "directToNextPage" default= "true">
<cfparam name = "includeNextPage" default= "false">
<cfparam name = "includeMessage" default= "true">
<cfparam name = "outputMessage" default= "false">
<!--- END --->


<cfset communicationObject = application.com.communications.getCommunicationObject(frmCommID)>

<cfset sendResult = communicationObject.send (
	CommID = frmCommID,
	selectionid = frmSelectID,
	CommSelectLID = frmCommSelectLID,
	personids = frmpersonids,
	resend = frmResend,
	test = frmTest,
	replyTo = frmReplyTo,
	showprocessingcode = "false",
	SubjectPrefix = frmSubjectPrefix,
	FooterText = frmFooter,
	showLinkBackToCommunication = frmshowLinkBackToCommunication)>
	

<cfif not sendResult.isOK>
	<cfset messageStruct.message_html = sendResult.ErrorMessage>
	<cfset messageStruct.type = "error">
</cfif>


<cfset message="">
<cfif sendResult.isOK and includeMessage>
<!--- TBD format using function in the object --->
	<cfset formattedResultStructure = sendResult.format(funcVersion = 2)>
	<cfset message = formattedResultStructure.LongTableHTML> <!--- WAB 2007-06-11 added encoding--->
	<!---
	NJH 2011/08/02 - do a cflocation rather than a cfinclude.. for some reason, it's messing up some Javascript and I cant' figure it out at the moment... I don't see 
		any reason why a cflocation shouldn't work and it does indeed appear to be working in the new tab layout.
	<cfif findnocase("?",frmNextPage) is 0 >
		<cfinclude template="#frmnextpage#">
	<cfelse>--->
	<cfif structkeyexists(formattedResultStructure,"totalResult") and formattedResultStructure.totalResult gt 20>
		<cfset messageStruct.message_html = formattedResultStructure.ShortTableHTML>
	<cfelse>
		<cfset messageStruct.message_html = formattedResultStructure.LongTableHTML>
	</cfif>
	<cfset messageStruct.type = "sendresults">

<!--- 
	WAB 2012-11-08 message no longer picked up at other end, and 2000 characters was still too big (when urlencoded)
	<cfset message = formattedResultStructure.LongTableHTML> <!--- WAB 2007-06-11 added encoding--->
	<cfif len(message) gt 2000>
		<cfset Message = formattedResultStructure.ShortTableHTML>
	</cfif>
	<cfset message = urlencodedformat(message)>
	<!--- WAB 2009/02/24 if the message is very long then the cflocation breaks down.  Don't know exact length.  Have guessed at 2000 and then just send a short message --->
--->
	<cfif outputMessage>
		<cfoutput>#formattedResultStructure.LongTableHTML#</cfoutput>
	</cfif>

</cfif>

<cfif isdefined("frmNextPage") and frmNextPage is not "" and not includeNextPage and directToNextPage>
	<!--- WAb 2011/09/14 problems with this cflocation, 
			sometimes frmnextpage has a querystring, sometimes it does not.  
			Sometimes it has frmcommid on it, sometimes not 
			had to add some tests
			--->
	<cfif not reFindNoCase('[&\?]frmcommid=',frmnextpage)>
		<cfif findnocase("?",frmNextPage) is 0>		
			<cfset frmNextPage = frmNextPage & "?frmCommID=#frmCommID#">
		</cfif>	
		<cfif not reFindNoCase('[&\?]frmcommid=',frmnextpage)>
			<cfset frmnextpage = listappend(frmnextpage,"frmCommID=#frmCommID#","&")>
		</cfif>
	</cfif>
	
	<!--- 2012-11-15 	WAB 	CASE:431825 - Problem passing long message via URL. Resorted to using a session variable to pass message struct --->
	<cfparam name="session.messageStructs" default="#structNew()#">
	<cfset messageStructID = createUUID()>
	<cfset session.messageStructs[messageStructID] = messageStruct>
	
	<cflocation url="#thisDir#/#frmnextpage#&messageStructID=#messageStructID#&frmCallingTemplate=#frmCallingTemplate#"addToken="false">
<cfelse>	
	<cfif includeNextPage>
		<cfinclude template="#thisDir#/#frmnextpage#">
	</cfif>
</cfif>

 
 

