<!--- �Relayware. All Rights Reserved 2014 --->
<!---
report of all communications sent, original version written by WAB a long time ago
(the first bit of cold fusion I ever wrote!)
All the searching stuff added by emma

2000-10-19 WAB: Went in to correct problem which was causing all the downloads to show by default.
		Did a certain amount of other tidying up, but it is a bit of a lost cause.

14-5-01 Andy Taylor: made a few changes to fix a bug...ticket number 3711
added 2 hidden tags to the form field at the bottom to pass the value of frmsearchby & selectfieldtype
when viewing next page of results.
Also changed default sort order of query to display by date from newest to oldest all the way through
2002-01-31 SWJ Modified layout
2004-04-27  WAB removed references to calltemplate.cfm - for some templates that can be called directly
June, July 04  WAB added filter by status and just my communications
Altered so that a search by commid ignores other filters

2005-10-24 	WAB modified to allow a daterange to be passed in (coming from commsbycountry report in this case)
2005-10-31	WAB	added a param for selectOptions variable

2008/01/21  WAB Allow to be included without tophead and passed in a default  daterange
2008/07/07 	WAB added itemsBeingSent to indicate selection being processed at moment
2008/11/03	WAB mod to prevent the "Some items sent" message appearing when an email was sent to an individual person
2009/06		WAB LID 2361 change so that report will indicate if a comm is in the process of being sent
2011/06		WAB changes related to storing content in phrases
2011/06/29	WAB use relayheader footer to get correct doc type
2011-07-04	NYB	P-LEN024
2011-08-22	NYB LHID7534 added title to all items with alt text
2013-05-14	WAB	Added check for merge errors (contentValidated:false) and display in status
2013-06-20	WAB Make Send to Self ajax'y
2013-10-02	WAB Added list=true to some of the CF_QUERYPARAMs. Report was giving wrong statuses
30/09/2014	SB  Added ids to all tables
--->

<CFSET REPORTBEGINDATE=NOW()-180>

<!--- NJH 2009/02/03 Bug Fix All Site Issue 1711 - set this directory to be the communcate directory, as this file is called from
	report\communication which sets the directory to be report --->
<CFSET thisdir = "/communicate">

<cfparam name="frmSearchBy" default="c.commid">
<cfparam name="selectFieldType" default = "">
<CFPARAM name="frmCommFormLID" default = "2,3,5,6,7">  <!--- show both email and fax --->
<cfparam name="message" default = "">
<cfparam name="campaignID" default = "">

<cfparam name="commEditor" default = "commDetailsHome"><!--- formerly "commCheckDetails" --->

<cfif isdefined("url.campaignID") and url.campaignID NEQ "">
	<cfset campaignID = #url.campaignID#>
	<cfset frmSearchBy ="">
</cfif>

<!--- this section should allow the last settings to be remembered unless frm variables are past --->
<cfparam name = "session.CommCheckDefaults.frmShowTest" default="0">
<cfparam name = "session.CommCheckDefaults.frmShowOwn" default="1">
<cfparam name = "session.CommCheckDefaults.frmShowStatus" default="all">

<CFPARAM name="frmShowTest" default = "#session.CommCheckDefaults.frmShowTest#">
<CFPARAM name="frmShowOwn" default = "#session.CommCheckDefaults.frmShowOwn#">
<CFPARAM name="frmShowStatus" default = "#session.CommCheckDefaults.frmShowStatus#">

<cfset session.CommCheckDefaults.frmShowTest = frmShowTest>
<cfset session.CommCheckDefaults.frmShowOwn = frmShowOwn>
<cfset session.CommCheckDefaults.frmShowStatus = frmShowStatus>

<CFSET displaystart = 1>
<cfparam name="showrecords" default="20">
<cfparam name="selectOptions" default="">  <!--- WAB added 2005-10-31 - We were getting error when the "next page" form was submitted after a filter was set to createdby, could have added as a hidden field on the form - but this seems OK--->

<cfif frmSearchBy is "country.countryid">
	<cfquery name="GetCountry" datasource="#application.sitedatasource#">
	select CountryID as FieldType, CountryDescription as FieldName
	from Country
	</cfquery>
	<cfset selectoptions = "nottext">
	<cfset choosefieldtype="GetCountry">
</cfif>

<cfif frmSearchBy is "c.createdby">
	<cfquery name="getsender" datasource="#application.sitedatasource#">
	select distinct Communication.Createdby as FieldType, UserGroup.Name as FieldName
	from Communication,UserGroup
	where Communication.Createdby=UserGroup.Usergroupid
	order by UserGroup.Name
	</cfquery>
	<cfset choosefieldtype="getsender">
	<cfset frmShowOwn = 0><!--- doesn't make sense to just show own when searching on createdby! --->
</cfif>

<cfif isDefined("form.thisDateRange")>
	<cf_dateRangeRetrieve dateRange = #thisdateRange#>
<cfelseif isDefined("form.lastUpdatedDateRange")>
	<cf_dateRangeRetrieve dateRange = #form.lastUpdatedDateRange# returnStruct="lastUpdatedDateRangeStruct">
<cfelseif not isDefined("lastUpdatedDateRangeStruct")>
	<cf_dateRange type="previousDays" value="60" returnStruct="lastUpdatedDateRangeStruct">
</cfif>


<cfif frmSearchBy is "c.commid" or frmSearchBy is "c.title">
	<cfset selectoptions = "text">
</cfif>

<cfif frmSearchBy is "c.Title">
	<cfset selectfieldtype = "#selectfieldtype#%">
</cfif>
<cfif frmSearchBy is "">
	<cfset selectfieldtype = "">
</cfif>


<cfquery name="Communications" datasource="#application.sitedatasource#">
	SELECT
	c.CommID, c.Title, c.Description,
	c.CommTypeLID, c.CommFormLID,LookupList.Itemtext as commform,
	c.Replyto,
	c.SendDate, c.Sent as sent,
	c.locked, <!--- Added LID 2361--->
	c.lastknownisapproved,
	CASE when parameters like '%"contentValidated":false%' then 1 else 0 end as mergeError,
	c.created,
	c.CreatedBy,c.lastupdated,
	c.lastupdatedBy,UserGroup.Name, Location.SiteName,
	Location.CountryID,Country.CountryDescription,
	<!--- CommFile.FileName, CommFile.Directory,--->
	isNull((select distinct 1 from vphrases where entityTypeID =  <cf_queryparam value="#application.entityTypeID.communication#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityid = c.commid and phraseTextID in ('HTMLBody','TextBody')),0) as hasContent,
	isNULL((select distinct 1 from commselection where commselection.commid = c.commid and commselectlid IN  (<cf_queryparam value="#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> )  and sent in (1,3)),0) as hasBeenSent,  <!--- WAB 2007-03-07 added 3 for sending at moment --->
	(select distinct 1 from commselection where commselection.commid = c.commid and commselectlid IN (  <cf_queryparam value="#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> ) and sent in  (3) ) as itemsBeingSent,  				<!--- WAb 2008/07/07 added itemsBeingSent to indicate being processed at moment- not ideal yet in dealing with crashed comms, could have another query to show items which have been at status 3 for a long time--->
	(select distinct 1 from commselection where commselection.commid = c.commid and commselectlid IN (  <cf_queryparam value="#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> ) and sent in  (0,2) and selectionid <> 0 ) as itemsStillToSend,    <!--- WAB 2008/11/03 added selectionid <> 0 to prevent the dummy selection which is used for individually selected people making it always look as if a comm was partially sent --->
	(select min(sendDate)  from commselection where commselection.commid = c.commid and commselectlid IN (  <cf_queryparam value="#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> ) and sent = 2) as NextSelectionQueuedDate,
	isNULL((select distinct 1 from commdetail with(nolock) where commdetail.commid = c.commid),0) as hasCommDetailRecords
	FROM Communication as c INNER JOIN ((UserGroup left JOIN Person ON UserGroup.PersonID = Person.PersonID)   <!--- WAB made joins left  - problems with missing people--->
		left JOIN Location ON Person.LocationID = Location.LocationID) ON c.CreatedBy = UserGroup.UserGroupID
		left JOIN Country ON Location.CountryID = Country.CountryID
		INNER JOIN  lookuplist on c.CommFormLID = LookupList.LookupID
		<!--- 	left JOIN commFile ON CommFile.CommID = c.CommID and commfile.commformlid = c.commformlid and commfile.isparent = commfileid--->
	where 1=1

	<cfif selectfieldtype is not "">
		<cfif frmSearchBy is "country.countryid">
			<!--- this finds comms which have been/are to be sent to the country selected
				if no people have been selected yet then it uses the senders country
			 --->
			and dbo.listfind(isNull(NULLIF(countryidlist,''),convert(varchar,country.countryid)),'#selectfieldtype#') <> 0

		<cfelse>
			and #frmSearchBy#  like  <cf_queryparam value="#selectfieldtype#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>

	</cfif>

	<cfif campaignID NEQ "">
		and c.campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfif>


	<cfif not (selectfieldtype is not "" and frmSearchby is "c.commid") and campaignID EQ "">
		<!--- if there is a search by commid then we override all the other filters --->

		<cfif frmCommFormLID is not "">
			and c.commformlid  in ( <cf_queryparam value="#frmCommFormLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>

		<cfif frmShowTest is 0>
			and commTypeLid <> 27
		</cfif>

		<cfif frmShowOwn is 1>
			and c.createdby = #request.relayCurrentUser.usergroupid#
		</cfif>

		<cfif frmShowStatus is not "all">
		and (select distinct 1 from commselection cs where cs.commid = c.commid and cs.commselectlid =  <cf_queryparam value="#application.com.communications.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  and cs.sent = 1)
			<cfif frmShowStatus is "sent">
				= 1
			<cfelseif frmShowStatus is "unsent">
				is null  <!--- unsent or queued --->
			</cfif>
		</cfif>

		<cfif isDefined("thisdateRange")>   <!--- this variable is passed from some of the other reports when drilling down --->
			<cf_dateRangeRetrieve dateRange = #thisdateRange#>
		and (select min(sentdate) from commselection cs where cs.commid = c.commid and commselectlid = 0 and sent = 1)> =  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		and (select min(sentdate) from commselection cs where cs.commid = c.commid and commselectlid = 0 and sent = 1) <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >

		<cfelseif isDefined("lastUpdatedDateRangeStruct")>
			and c.lastupdated  BETWEEN  <cf_queryparam value="#lastUpdatedDateRangeStruct.startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and  <cf_queryparam value="#lastUpdatedDateRangeStruct.endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			<cfset lastUpdatedDateRangeStruct.used = true>
		</cfif>


	</cfif>



	order by c.commid desc

</cfquery>



<cf_title>Phr_Sys_CommsSent</cf_title>  <!--- couldn't use the tophead parameter because commtophead.cfm references checkPermission --->

<cfparam name="frmHideTopHead" default="false">
<cfif not frmHideTopHead>
	<cfinclude template="#thisDir#/commtophead.cfm">
</cfif>
<cf_includejavascriptonce template = "/javascript/openWin.js">
<cf_includejavascriptonce template = "/javascript/extExtension.js">
<cf_includeJavascriptOnce template = "/communicate/communication.js">
<cf_includeJavascriptOnce template = "/javascript/prototypeSpinner.js">


<style>
.clickable {cursor:pointer;}
</style>

<cfoutput>
<script language="javascript">
	downloadEmailContent = function(CommID)	{
		window.open("#request.currentSite.PROTOCOLANDDOMAIN##jsStringFormat(thisDir)#/commDownloadContent.cfm?CommID="+CommID,"Download",'width=100,height=10,location=no,scrollbars=no,left=10,top=10,toolbar=no,directories=no,status=no,menubar=no,resizable=no,dependent=yes');
	}
	deleteCommunication = function(CommID,lastUpdated) {
		var url;
		var confirmed;
		confirmed=confirm('phr_sys_comm_confirmCommDeletionRequest');
		if(confirmed){
			url = "#request.currentSite.PROTOCOLANDDOMAIN##jsStringFormat(thisDir)#/commdelete.cfm?frmCommID="+CommID+"&frmLU_"+CommID+"="+lastUpdated+"&frmNextPage=commcheck.cfm?";
			window.location = url;
		}
	}
</script>
</cfoutput>


<cfif message is not "">
<cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput>
</cfif>

<div id="commCheckTable1" class="actionList">
	<FORM NAME="doList" METHOD="post" action="commcheck.cfm">
		<input type="hidden" name="selectoptions">
		<div class="grey-box-top">
			<div class="row" id="commCheck1">
				<div class="col-xs-12 col-sm-6">
					<label>Phr_Sys_SearchOn</label>
					<SELECT NAME="frmSearchBy" onChange="javascript: document.doList.submit();" class="form-control">
						<OPTION VALUE="">Phr_Sys_Nothing
						<OPTION VALUE="country.countryid" <CFIF frmSearchBy IS "country.Countryid"> SELECTED </CFIF> >Phr_Sys_Country
						<OPTION VALUE="c.commid" <CFIF frmSearchBy IS "c.commid"> SELECTED </CFIF> >Phr_Sys_CommID
						<OPTION VALUE="c.createdby" <CFIF frmSearchBy IS "c.createdby"> SELECTED </CFIF> >Phr_Sys_Sender
						<OPTION VALUE="C.Title" <CFIF frmSearchBy IS "C.Title"> SELECTED </CFIF> >Phr_Sys_Title
					</SELECT>
				</div>
				<div class="col-xs-12 col-sm-6" id="PleaseTypeinSearchCriteria">
					<cfif frmSearchBy is not "">
						<cfif selectoptions is "text">
							<label>Phr_Sys_PleaseTypeinSearchCriteria</label>
							<div class="row">
								<div class="col-xs-10">
									<input type="text" name="selectfieldtype" class="form-control">
								</div>
								<div class="col-xs-2">
									<input type="submit" value="Phr_Sys_Go" name="Go" class="btn btn-primary">
								</div>
							</div>
						<cfelse>
							<label>Phr_Sys_PleaseSelectSearchCriteria</label>
							<div class="row">
								<div class="col-xs-10">
									<select name="selectFieldtype" class="form-control">
										<cfoutput query="#choosefieldtype#">
											<option value="#fieldtype#" <CFIF selectFieldType is fieldType>Selected</cfif>>#htmleditformat(FieldName)#
										</cfoutput>
									</select>
								</div>
								<div class="col-xs-2">
									<input type="submit" value="Phr_Sys_Go" name="Go" class="btn btn-primary">
								</div>
							</div>
						</cfif>
					</cfif>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<label>Phr_Sys_FilterBy Phr_Sys_Status</label>
					<div class="form-group">
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowStatus" VALUE="sent" onClick="this.form.submit()" <CFIF frmShowStatus is "sent">Checked</CFIF>> Phr_Sys_Sent
						</label>
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowStatus" VALUE="unsent" onClick="this.form.submit()" <CFIF frmShowStatus is "unsent">Checked</CFIF>> Phr_Sys_Unsent
						</label>
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowStatus" VALUE="all" onClick="this.form.submit()" <CFIF frmShowStatus is "all">Checked</CFIF>> Phr_Sys_All
						</label>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<label>Phr_Sys_JustShowMyCommunications</label>
					<div class="form-group">
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowOwn" VALUE="0" onClick="this.form.submit()" <CFIF frmShowOwn is 0>Checked</CFIF>> Phr_Sys_No
						</label>
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowOwn" VALUE="1" onClick="this.form.submit()" <CFIF frmShowOwn is 1>Checked</CFIF>> Phr_Sys_Yes
						</label>
					</div>
				</div>

				<div class="col-xs-12 col-sm-3">
					<label>Phr_Sys_ShowTestCampaigns</label>
					<div class="form-group">
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowTest" VALUE="0" onClick="this.form.submit()" <CFIF frmShowTest is 0>Checked</CFIF>> Phr_Sys_No
						</label>
						<label class="radio-inline">
							<INPUT TYPE="radio" NAME="frmShowTest" VALUE="1" onClick="this.form.submit()" <CFIF frmShowTest is 1>Checked</CFIF>> Phr_Sys_Yes
						</label>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<cfif #communications.recordcount# IS NOT 0>
						<CFIF communications.RecordCount GT Variables.ShowRecords>
							<!--- define the start record --->
						 	<CFIF IsDefined("FORM.ListForward.x")>
								 <CFSET displaystart = frmStart + Variables.ShowRecords>
						 	<CFELSEIF IsDefined("FORM.ListBack.x")>
								 <CFSET displaystart = frmStart - Variables.ShowRecords>
							<CFELSEIF IsDefined("frmStart")>
							 	 <CFSET displaystart = frmStart>
							</CFIF>
						</CFIF>
					</cfif>
					<cfif isdefined("lastUpdatedDateRangeStruct") and structKeyExists(lastUpdatedDateRangeStruct,"used")>
						<div class="form-group">
							<label>Updated in</label>
							<cf_DateRangeSelectBox BoxName="lastUpdatedDateRange" SelectedRange="#lastUpdatedDateRangeStruct.range#"  ShowPreviousDays = "30,60,90" passthrough="onChange='this.form.submit()'"  StopYearInPast="#Year(dateAdd("yyyy","-4",Now()))#">
						</div>
					</cfif>
					<div class="form-group">
						<CFIF (Variables.displaystart + Variables.ShowRecords - 1) LT communications.RecordCount>
							<CFOUTPUT>Phr_Sys_Record #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + Variables.ShowRecords - 1")# of #communications.recordcount#</CFOUTPUT>
						<CFELSE>
							<CFOUTPUT>Phr_Sys_Record #htmleditformat(Variables.displaystart)# - #communications.RecordCount# of #communications.recordcount#</cFOUTPUT>
						</CFIF>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div id="resultsDiv">

</div>

<cfif #communications.recordcount# IS NOT 0>
	<TABLE id="commCheckTable2" CLASS="table table-hover table-striped actionList">
		<TR>
			<TH>Phr_Sys_Date</TH>
			<TH>Phr_Sys_CommID</TH>
			<TH>Phr_Sys_TitleDescription</TH>
			<TH>Phr_Sys_SenderCountry</TH>
			<TH>Phr_Sys_Type</TH>
			<TH>Phr_Sys_Status</TH>
			<TH colspan="2" style="text-align:center;">Send</TH>
			<TH colspan="4" style="text-align:center;" width="72">Phr_Sys_Actions</TH>
		</TR>

		<cfoutput query="communications" STARTROW=#Variables.displaystart# MAXROWS=#Variables.ShowRecords#>
			<TR>
				<TD>#DateFormat(created,"DD/MM/YY")# <!--- #DateFormat(senddate,"DD/MM/YY")#  --->&nbsp;</td>
			 	<TD>#htmleditformat(CommID)# &nbsp;</td>
			 	<TD align="left"><a href="" onclick="openCommTab(#commid#,''); return false"  title="#title#">#left(Title,40)#</A>
			 	 <BR>#left(description,40)#</td>
				<TD>#htmleditformat(Name)#<BR>#htmleditformat(Countrydescription)#</td>
			 	<TD>#htmleditformat(Commform)#</td>
			 	<TD>
				<!--- WAB 2008/07/07 added itemsBeingSent to indicate being processed at moment- not ideal yet in dealing with crashed comms, but gives some idea of a problem
					WAb 2009/06 LID 2361 added check for locked as well
				--->
				<cfif not hasContent>
				Awaiting Content
				<cfelse>
					<cfif hasbeensent is 1>
						<cfif itemsBeingSent is 1 and locked is 1>Sending<cfelseif itemsBeingSent is 1 and locked is 0>Batches Partially Sent<cfelseif itemsStillToSend is 1>Some Items Sent<cfelse>Phr_Sys_Sent</cfif>
						<BR>
					</cfif>
					<cfif (senddate is not "" and sent is 0) or NextSelectionQueuedDate is not "">
						<cfif NextSelectionQueuedDate is "">
								<cfset thedate = senddate>
						<cfelseif not (senddate is not "" and sent is 0)>
							<cfset thedate = NextSelectionQueuedDate>
						<cfelse>
							<cfset thedate = min(senddate,NextSelectionQueuedDate)>
						</cfif>
						Queued #DateFormat(theDate,"dd-mmm-yy")# <BR></cfif>
						<cfif application.com.settings.getSetting("communications.approvals.useApprovalProcess")>
							<cfif lastKnownIsApproved is 0>Requires Approval<BR></cfif>
						</cfif>
					<cfif mergeError>
						<font color="red">Merge Field Error</font>
					</cfif>
				</cfif>
				</TD>
				<TD ALIGN="center">
					<cfif hasContent>
						<A href="##" onclick="sendComm('#application.com.security.encryptVariableValue("commid",CommID)#',$(this),{<cfif not hasBeensent>test:true,</cfif>asynchronous:false,methodname:'sendCommToSelf'});return false">
							<span class="fa fa-envelope" id="orgListEnvelope" title="Phr_sys_SendToSelf"></span>
						</a>
					</cfif>
				</TD>
				<TD ALIGN="center">
					<cfif hasContent>
						<a HREF="#thisdir#/commDetailsHome.cfm?frmCommID=#CommID#&frmNextPage=Send&action=SendToFriend">
							<span class="fa fa-users" id="orgUsers" title="Phr_Sys_ClicktoSendEmailtoSomeoneElse"></span>
						</a>
					</cfif>
				</TD>
			 	<TD WIDTH="18">
				 	<cfif #commformlid# is 2 and hasContent >
					 	<A href="##" onclick="openCommTab(#commid#,'Preview'); return false">
						 	<span class="fa fa-file-text-o" id="viewContents" alt="Phr_sys_ViewContents" title="Phr_sys_ViewContents"></span>
						</a>
				</TD>
					</cfif>
			 	<td align="center" WIDTH="18">
				 	<cfif #commformlid# is 2 and hasContent>
					 	<span class="fa fa-arrow-circle-down" id="sysDownload" ALT="Phr_Sys_Download" title="Phr_Sys_Download" onclick="javascript:downloadEmailContent(#CommID#)"></span>
					</cfif>
				</td>
			 	<td align="center" WIDTH="18">
				 	<cfif hasBeensent or hasCommDetailRecords>
					 	<A href="##" onclick="openCommTab(#commid#,'Statistics'); return false">
						 	<span class="fa fa-bar-chart" id="barChart" title="Phr_Sys_ViewStats"></span>
						</A>
					</cfif>
				</td>
			 	<td align="center" WIDTH="18">
				 	<cfif request.relaycurrentuser.usergroupID eq createdby and hasbeensent is not 1>
				 		<span class="fa fa-times-circle" id="sysCloseCircle" ALT="Phr_Sys_Delete" title="Phr_Sys_Delete" onclick="javascript:deleteCommunication(#CommID#,'#lastupdated#')"></span>
					</cfif>
				</td>
			</tr>
		</cfoutput>
	</table>


	<div id="commCheckTable3">
		<FORM NAME="ThisForm" METHOD="POST" ACTION="commCheck.cfm">
		<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
			<CF_INPUT type="hidden" name="frmSearchBy" value="#frmSearchBy#">
			<CF_INPUT type="hidden" name="selectfieldtype" value="#selectfieldtype#">
			<CF_INPUT TYPE="hidden" NAME="frmShowStatus" VALUE="#frmShowStatus#">
			<CF_INPUT TYPE="hidden" NAME="frmShowOwn" VALUE="#frmShowOwn#">
			<CF_INPUT TYPE="hidden" NAME="frmShowTest" VALUE="#frmShowTest#">
			<cfif campaignID NEQ "">
				<CF_INPUT type="hidden" name="campaignID" value="#campaignID#">
			</cfif>
			<cfif isDefined("thisdateRange")>
				<CF_INPUT TYPE="hidden" NAME="thisdateRange" VALUE="#thisdateRange#">
			</cfif>
			<cfif isDefined("lastUpdatedDateRange")>
				<CF_INPUT TYPE="hidden" NAME="lastUpdatedDateRange" VALUE="#lastUpdatedDateRange#">
			</cfif>
		</cfoutput>
		<div class="commCheckTable3Col1">
			<CFIF Variables.displaystart GT 1>
				<INPUT TYPE="Image" NAME="ListBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="">
			</CFIF>
		</div>
		<div class="commCheckTable3Col2">
			<CFIF (Variables.displaystart + Variables.ShowRecords - 1) LT communications.RecordCount>
				<CFOUTPUT>Phr_Sys_Record #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + Variables.ShowRecords - 1")#</CFOUTPUT>
			<CFELSE>
				<CFOUTPUT>Phr_Sys_Record #htmleditformat(Variables.displaystart)# - #communications.RecordCount#</cFOUTPUT>
			</CFIF>
		</div>

		<div class="commCheckTable3Col3">
			<CFIF (Variables.displaystart + Variables.ShowRecords - 1) LT communications.RecordCount>
			<INPUT TYPE="Image" NAME="ListForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Phr_Sys_Next" title="Phr_Sys_Next">
			</cFIF>
		</div>
		</FORM>
	</div>
	<div id="commCheckTable4">
		<form name="commmethod" method="post" class="form-horizontal">
			<cfoutput><CF_INPUT type="hidden" name="frmSearchBy" value="#frmSearchBy#"></cfoutput>
			<cfif isDefined("selectfieldtype")><cfoutput><CF_INPUT type="hidden" name="selectoptions" value="#selectoptions#">
			<CF_INPUT type="hidden" name="selectfieldtype" value="#selectfieldtype#"></cfoutput></cfif>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6">
					<select name="frmCommFormLID" class="form-control">
						<option value=""><cfoutput>Phr_Sys_ShowAllCommunications</cfoutput>
						<option value="2,5,6,7" <CFIF frmCommFormLID is "2,5,6,7">Selected</cfif>>Phr_Sys_ShowOnlyEmails
						<option value="3,5,6,7" <CFIF frmCommFormLID is "3,5,6,7">Selected</cfif>>Phr_Sys_ShowOnlyFaxes
						<option value="4" <CFIF frmCommFormLID is 4>Selected</cfif>>Phr_Sys_ShowOnlyDownloads
						<option value="2,3" <CFIF frmCommFormLID is "2,3,5,6,7">Selected</cfif>>Phr_Sys_BothFaxandEmail
					</select>
				</div>
				<div class="col-xs-12 col-sm-2"><input type="submit" value="Phr_Sys_Go" name="Go" class="btn btn-primary"></div>
			</div>

		 </form>
	</div>

<cfelse>
	 <TABLE id="commCheckTable5" WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	 	<TR><TD>Phr_Sys_ThereareNoRecordsMatchingThisCriteria.</TD></TR>
	 </TABLE>
 </cfif>