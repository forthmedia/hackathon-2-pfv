<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="3600" enablecfoutputonly="Yes" > 
<!---
1998-11-02  WAB Amendments to enhance Fax Cover Sheets  
1999-01-08  WAB Trim(getSelectionInfo.Email) added to handle emails with leading spaces  
1999-01-13	WAB Only one confirmation email reaching help@3contact.com per seesion - added 'cBccEmail' to other place where confirmations are done - line 209 
1999-01-11  WAB Found bug at line 835 still pointing to old style fax cover sheet - probably only caused occasional problems (last fax, single recipient)
				need to add routingcode to countrytable 
1999-01-30  WAB Removed duplicate emails to duplicate email addresses   
1999-11-01 	KT added ability to send comm from events and add a reg number to fax/e-mail 
1999-02-02  WAB put email address in the 'fax' field for emails   
1999-02-15	WAB Added code to allow user unsubscribe information to be taken into account
1999-02-15	WAB Added code to allow user email/fax preferences to be takeninto account
1999-02-15	WAB Stipped out the generation of confirmation email and the fax sub files into separate templates since they are each called in two places and the code is never identical
1999-02-17	WAB Email Subject can't have % in it - (DOS batch file gets confused) so all changed to %% 
1999-07-16	WAB added email merge support 
1999-07-16	WAB don't update contactdetails unless the value is not null - this prevents the textfield grabbing 2K per record
2000-07-06  WAB added a merge field called <<<externalAutoLogin>>> which does http://#externalsitedomain#/index.cfm?p=#persponid-#magicnumber##
2000-07-13	WAB trying out mail merge with derived fields which are defined in the screendefinition table 
2000-12-22		Added code to allow sending of a communication to a single person (or group of people) within a communciation
2000-12-22	WAB did some work with HTML
2001-02-26  WAB Added code to delete the selection of Invalid Emails and Faxes once the communication has been sent
2001-02-28 	WAB altered code so that batch files and sub files are processed for each communication in turn rather than all at the end
2001-03-07 	WAB altered code so that email from name is an application variable
2001-03-23 	WAB Added override send date so that test communications can be sent out
2001-06-15  WAB added code to handle the fax server having an international access code which isn't 00 and to handle any 00 which are in the database 
2001-07-11	WAB made the variables which control day time communication size into application varibales
SWJ	2001-08-03	Changed the layout to be in keeping with the new look and feel
SWJ 2001-08-05	added CommID as additional merge field so we can track feedback by commid (search for ChangeID:25) 
SWJ 2001-09-21	added telephone as additional merge field so we can track feedback by commid (search for ChangeID:26) 
CPS 2001-10-25	ensure e-mails are sent as HTML if the eWebEditPro editor is being used and use new attributes on commdetail 
CPS 2001-11-05	when sending previous emails through the 'send a previous communication' functionality set the respondee 
				to the person who did the re-send. In such context the confirmation email is surpressed. 
DAM 2002-08-29 	Reviewing code and tidying comments
DAM 2002-08-30 	Added the ServerID into the from address so that server specific bouncebacks can be identified
DAM 2002-09-05	Added log feed back to each line in the bat file
WAB 2004-04-28	Completely rewritten to use CFMAIL.  Also broken down into smaller parts
WAB 2004-09-08 	added cftry around the cfmail to catch any invalid email addresses which slip through the net
WAB 2004-12-28	added a mod incase application.emailfrom is incorrectly set 
WAB mid 05		- did all sort of things, including putting the feedback into a structure
WAB 2005-09-06  fixed bug .  When more than one comm being sent, the from address (and some other things) was always the value from the first comm.  
WAB 2005-10-05 	added apostophe to list of characters allowed in an email address
WAB 2005-11-01  more problems similar to 2005-09-06 bug - no implemented a complete fix
WAB 2005-11-07	Problem when queue had selection specific jobs followed by a complete communication - the complete communication was not sent
WAB 2005-12-14	Added a replyTo attribute that can be set only when sending tests
WAB 2006-06-19  added some error handling for badly formed mail merge fields.  Crowbared a message to the end user and updated the 
WAB 2007-04-25  change to unsubscribeAll link
WAB 2007-05-09  added application.mailFailToDomain to replace application.mailFromDomain which has been hijacked for other uses
NJH/WAB	2008/10/21 Bug Fix Trend Nabu Issue 1076 Extended the timeout for sending of comms based on the number of people that the comm gets sent out to.
WAB	2008/11/05  more on timeout (previous stuff rather didn't give quite enough time)
WAB 2009/01/14	add cf_Mail
WAB 2009/01/23	Change the email tracking link to go remote.cfm in the webservices\noSession Directory
WAB 2009/02/23  Bug 1869 Added a new status for Duplicate Emails not sent.  (also a db script to add the status to commdetailstatus)
WAB 2009/03/02 	LID 1916 altered code so that commselection is updated sent=3, here rather than in populate commdetail
				at same time remember current status so that if there is an error <cfcatch> then the old status is reinstated
WAB 2009/06/20 	Major Works
				Added some extra trys and catches to make more resilient.
				Will no longer run more than one scheduled process, or large manual send concurrently.
				File createScheduledCommunicationLock.cfm can be used to simulate locking conditions
				Will recover if there is an error while sending a scheduled email.
				Ability to send a large email in batches.
				Make full use of the feedback structure - new function to display it.  Gor rid of previous feedback string
				Moved many bits into separate functions in communications.cfc
				Left fax code in, but not guaranteed to work
NYB	2009-08-06 	NABU LHID 2456 - added 'with (nolock)' to various queries throughout code to resolve occasional error of:  Error Executing Database Query. [Macromedia][SQLServer JDBC Driver]Connection reset by peer
WAB 2009/11/02  LID 2816 problems when template caching turned on - doesn't recognise changes to the merge cfm file, implemented unique filenames, need to delete after use
WAB 2010/04/17 	LID 3371 - to deal properly with sending to lists of people (selectionid 0) in particular when created from 3 pane view 
WAb 2010/10/10  removed application.sitemail server, replaced with an optional setting communications.MailServerAddress
WAB 2010/10/12 	Removed references to application.internalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalInternalDomain(), ditto external.  See concerns in comments of same date in CommHeader.cfm
NJH	2010/12/21	LID 4923 - added linkToSiteURLWithProtocol to communication structure, which is the external domain plus it's protocol
WAB 2011/01/12  Change serverID to databaseId - now comes from heartbeat server (via the coldfusioninstanceautopoulated table)
WAB 2011/03/07  LID 5815 More locking in case two scheduled tasks run at same time
WAB 2011/06/  	Moved communication content to the db and do merging with the merge object
NYB	2011-08-25	LHID7571 - added acceptHashes so the system can choice to allow hashes in email content
WAB 2011/10/06  LID 7887 add ltrim/rtrim to email
WAB 2011/10/10 	moved omniture tracking code from merge.cfc to here
				managed to get rid of need for acceptHashes - now dealt with automatically in merge.cfc
				some changes to try and improve memory usage - actually main thing is to have debug off
WAB 2011/11/16 	some quite major changes so that the merge structure contains merge.person, merge.location etc. should improve performance
				info on which fields belong in which come from the mergeField XML
WAB 2012-01-30  CRM426370 'LinkToSiteURL' not being used to convert images to absolute paths  - moved code to default LinkToSiteURL so that before calls to communications.prepareEmailTextAndFilesForSending() (which was also changed)
WAB 2012-03-14  Implement Merge by .cfm file to speed it up
WAB 2012-03-22  CASE 426999 Fixed bug which was causing apparently random emails to be not sent.  
				Turned out that the bug meant emails could only be sent to a given selection once per run, so on a scheduled run with more than one communication using the same selection, things went haywire

WAB 2013-01		Sprint 15&42 Comms.  Converted to use communication.cfc object.  Almost all code from this template removed to communication.cfc
--->
	

	

<cfparam name = "attributes.test" default = "no">
<cfparam name = "attributes.showdebugoutput" default = "false"> <!--- WAB 2011/10 added this parameter while trying to find memory problems, probably not necessary --->


<cfif attributes.test>
	<cfparam name = "attributes.subjectPrefix" default = "Test: ">
	<cfparam name = "attributes.showLinkBackToCommunication" default = "false">
	<cfparam name = "attributes.footerText" default = "">
	<cfparam name = "attributes.replyto" default = "">
<cfelse>
	<!--- if not a test then can't do these special things to a communication --->
	<cfset attributes.subjectPrefix = "">
	<cfset attributes.showLinkBackToCommunication = false>
	<cfset attributes.footerText = "">
	<cfset attributes.replyTo = "">
</cfif>
	
<cfparam name = "attributes.resend" default = "false">  <!--- for populatecommdetail --->

<cfif not isDefined("attributes.userID")> <!--- odd construction here because when called from scheduled process, cookie does not get set and it errors (even though userid is passed in so doesn't need to use the default) --->
	<cfset attributes.userID =request.relayCurrentUser.usergroupid>  <!--- needed to pass to populatecommdetail --->
</cfif>

<cfparam name = "attributes.commid" default = "">  <!--- if defined then just send that communication, otherwise all scheduled ones are sent --->

<!--- various parameters can be used to limit who is sent to (even if records have been added to commdetail) --->
<cfparam name = "attributes.personids" default = "">  		<!--- list of personids --->
<cfparam name = "attributes.commSelectID" default = "">		<!--- particular selection to send to  --->
<!--- <cfset variables.commSelectID = attributes.commSelectID>  	 WAb 2005-11-07 removed and put at top of the comm loop--->
<cfparam name = "attributes.commSelectLID" default = "">		<!--- particular selection type of selection to send to  --->
<!---  if all of the above parameters are null,
		then we are sending to everyone in commdetail
--->







<!--- 	*********************************************************************************************
			INITIALISE
--->

<CFSET ResultStruct = structNew()>
<CFSET ResultStruct.comms = structNew()>
<CFSET ResultStruct.isOK = true>
<CFPARAM NAME="FreezeDate" DEFAULT=#Now()#>
<CFSET lb= Chr(13) & Chr(10)>
<CFSET prevcommID = 0>
<CFSET totalEmailsSent  = 0 >   <!--- used to limit number sent in one go --->


<cfset scheduledCommLockName = "tskCommSend">  <!--- this is the name of the lock used to lock scheduled and large comms --->

<cfif attributes.CommID is "" > <!--- running as a scheduled process on the queue --->
	<cfset scheduledCommunication = true>
<cfelse>
	<cfset scheduledCommunication = false>
</cfif>



<!--- We are going to get a lock to prevent this template running in scheduled mode more than once at a time 
	so for a scheduled process the lock has name tskcommsend, but for all other times there is a UUID appended
		--->

<cfif scheduledCommunication>
	<cfset lockName_Outer = scheduledCommLockName>
<cfelse>
	<cfset lockName_Outer = "">	<!--- don't need to lock for non scheduled running --->	
</cfif>

<cf_lock name='#lockName_Outer#' throwOnTimeout=false message="Getting Communcations To Send">

<!--- get list of communications to send--->
<cfif attributes.CommID is "" > <!--- running as a scheduled process on the queue --->
	<cfset commsToSend = application.com.communications.getCommunicationsScheduledForNow()>
<cfelse>
	<!--- Originally this query was created from the same query as is in the function getCommunicationsScheduledForNow, 
	but I had problems getting it to work during LID 2361 and created this one, ought to try again	--->
	<CFQUERY NAME="commsToSend" DATASOURCE=#application.siteDataSource#>
		SELECT distinct c.CommID,
		  c.Title,
		  c.Title    as titleAndSelection,
		<cfif attributes.commSelectID is not "" and attributes.commSelectID is not "-1">  	
			cs.selectionid,
			<cfelse>
			''  as selectionid,
		</cfif>
		  c.CommFormLID AS MainCommFormLID,
		  c.commtypelid,		<!--- added WAB 1999-02-16 --->
			c.sent,
		  cf.CommFormLID,
		  cf.CommFileID		
		<!--- START: NYB 2009-08-06 NABU LHID 2456 - added 'with (nolock)' to query --->	
		FROM Communication AS c with (nolock) 
		inner join CommFile AS cf with (nolock) on c.CommID = cf.CommID and cf.CommFileID = cf.IsParent
		inner join CommFile AS cfb	with (nolock) on cfb.CommID = c.CommID	
		left join (commselection cs with (nolock) 
		inner join selection s with (nolock) on cs.selectionid = s.selectionid
			) on cs.commid = c.commid 
		<!--- END: NYB 2009-08-06 NABU LHID 2456 - added 'with (nolock)' to query --->	
		 WHERE 
		 1 = 1
	
		 AND c.CommFormLID IN (2,3,5,6,7) <!--- exclude downloads (4) and 0 --->			
	
		 <CFIF attributes.CommID is not "">
			 AND c.CommID  in ( <cf_queryparam value="#attributes.CommID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		 </CFIF>
		
		 <cfif attributes.commSelectID is 0><!--- WAB 2010/04/17 LID 3371 selectionid = 0 is a special case which means that being sent to manually added people --->
			and cs.selectionid is null
		<cfelseif attributes.commSelectID is not "" and attributes.commSelectID is not "-1">  	
			and cs.selectionid  in ( <cf_queryparam value="#attributes.commSelectID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
	</cfquery>
</cfif>



		
		<cfset variables.previousCommID = "">
		<cfset variables.commSelectID = "">
		<cfset variables.usedCommSelectID = "">

		<CFLOOP QUERY="commsToSend">
			<cf_lockupdate message="Processing Communication #commid#">
			
			<cfif variables.previousCommID IS commid>
				<cfif variables.commSelectID neq "" and variables.commSelectID neq "0" and variables.commSelectID neq "-1">
					<cfset variables.usedCommSelectID = ListPrepend(variables.usedCommSelectID,variables.commSelectID)>
				</cfif>
			<cfelse>
				<cfset variables.usedCommSelectID = "">
			</cfif>
			<cfif attributes.commSelectID is not "">
				<cfif commsToSend.selectionid[currentrow] is not "">
					<cfset variables.commSelectID = commsToSend.selectionid[currentrow]>
				<cfelse>
					<cfset variables.commSelectID = "">  	
				</cfif>
			<cfelseif commsToSend.selectionid is not ""><!--- when running scheduled communications, we may just be sending to an individual selection --->
				<cfset variables.commSelectID = commsToSend.selectionid>
			<cfelse>
				<cfset variables.commSelectID = "">  	
			</cfif>

			<cfset communicationObject = application.com.communications.getCommunication (Commid)>

			<cfset thisCommResult = communicationObject.send(
								personids = attributes.PersonIDs,
								CommSelectLID = attributes.commSelectLID,
								selectionID = variables.commSelectID,
								test = attributes.test,
								scheduledCommunication = scheduledCommunication,
								subjectPrefix  = attributes.subjectPrefix ,
								showLinkBackToCommunication = attributes.showLinkBackToCommunication,
								footerText = attributes.footerText,
								replyTo = attributes.replyTo
									
					)>



			<!--- Sometime we can do the same comm twice in the loop (different selections) so need to make the key unique --->		
			<cfset structKey = commsToSend.commid & iif(commsToSend.selectionid is not "",de("-#commsToSend.selectionid#"),de(""))>
			<cfset resultStruct.comms[structKey] = thisCommResult>
			<!--- 		<cfset ResultStruct.feedbackString = ResultStruct.feedbackString  & thisCommResult.feedbackString> --->
		
			<!--- If it is a partial send then we can't process anymore items, so break out of the commsToSend loop 
			<cfif thisCommResult.partialSend or totalEmailsSent gte commLimits.maxNumberEmailsPerSend >
				<cfbreak>
			</cfif>
			--->
		
			<cfset variables.previousCommID = commid>

		</CFLOOP>
	
	
	</cf_lock> <!--- end of outer lock --->

	<cfif cf_lock.timedout>
		<cfset resultStruct.isOK = false>
		<cfset resultStruct.errorMessage = "Another Large Email Process is Already Running. Please Try Again Later">
	</cfif>


<!--- <cfset caller.resultstring = resultStruct.feedbackString> --->
<!--- <cfset caller.resultstring = resultstring> --->
<cfset caller.tskCommSend = {resultStruct = resultStruct}> 
