<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		previewCommunication.cfm	
Author:			AJC/WAB
Date started:	10-Nov-2010
	
Description:	This provides an online version of the communication for people to view.

Usage:			
					
Used by:		

Amendment History:

Date 	Initials 	What was changed
2011/07/12	WAB		Updated to get communication content from the db rather than files
2013/01/28 	 NJH 	Rename 
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
2013/02/28	NJH  	Case 433941 - add ability to show/view messages as well as email content. Used in contact history
					
Enhancements still to do:


 --->
<cfparam name="commid" default=0>
<cfparam name="showdebug" default="false">
<cfparam name="messageID" default=0> <!--- NJH 2013/03/01 if messageId is the same as commId, then we get the comm message. Otherwise, if messageId is not 0 and commId is, then we are getting a proper message --->

<cfif request.relayCurrentUser.isInternal>
	<cfset request.document.showFooter = false>
</cfif>

<cfset personID = request.relayCurrentUser.personID>
<cfset messageText = "">
<cfset messageTitle = "">
<cfset messageImage = "">
					
	<!--- check the commid passed is a valid commID --->
	<cfif isnumeric(commID)>
	
			<!--- get the communication structure --->
			<cfif commID neq 0>
				<cfset communication = application.com.communications.getCommunication (commid = Commid)>
				<cfset content = communication.getMergedContentForSinglePerson(personid = personID,test = 0)>
				
				<!--- if we want to show the communication message --->
				<cfif messageID eq commID>
					<cfset messageText = content.message.text.phraseText>
					<cfset messageTitle = content.message.title.phraseText>
					<cfset messageImage = content.message.image>
				</cfif>
			<cfelseif messageID neq 0>
				
				<cfquery name="getMessageDetails" datasource="#application.siteDataSource#">
					select title_defaultTranslation,text_defaultTranslation,image from message where messageID=<cf_queryparam value="#messageID#" cfsqltype="cf_sql_integer">
				</cfquery>
				
				<cfset messageText = getMessageDetails.text_defaultTranslation>
				<cfset messageTitle = getMessageDetails.title_defaultTranslation>
				<cfset messageImage = getMessageDetails.image>
			</cfif>
			
			<!--- if the communication structure can be found and built --->
			<cfif commID eq 0 or communication.recordcount gt 0>
				<cfif showdebug and commID neq 0>
					<cfdump var="#communication#">
				</cfif>
				
				<!--- NJH 2013/01/28 - put a div around the content so that I could calculate the size of the window. Putting float:left means that I can grab the width of the div
					rather than the width of the body (or window) --->
				<div id="commViewContent" style="<cfif request.relaycurrentuser.isinternal>float:left;</cfif><cfif messageID neq 0>border:1px solid grey; width:500px;height:80px;</cfif>">
					<cfif messageID eq 0>
						<cfif content.email.html.phraseText is not "">
							<cfoutput>#content.email.html.phraseText#</cfoutput>
						<cfelse>
							<!--- 2013-07-16	YMA	Case 436081 Text emails need to display in a <pre></pre> tag to format correctly. --->
							<cfoutput><pre>#htmlEditFormat(content.email.text.phraseText)#</pre></cfoutput>
						</cfif>
					<cfelse>
						<cfoutput>
						<cfif messageImage neq ""><img src="#messageImage#" style="width:80px;height:80px;float:left;padding-right:5px;"></cfif>
						<span style="padding-bottom:5px;font-weight:bold;">#messageTitle#</span><br>
						#messageText#<br>
						</cfoutput>
					</cfif>
				</div>
				
				
				<cfoutput>
				<script>
					if (window.opener) {
						height = 600;
						width = 650;
						if ($('commViewContent').getDimensions().width > width) {
							width = $('commViewContent').getDimensions().width;
						}
						if ($('commViewContent').getDimensions().height > height) {
							width = $('commViewContent').getDimensions().height;
						}
						window.resizeTo(width,height)
					}
				</script>
				</cfoutput>

			<!--- if the communication structure cannot be found and built --->
			<cfelse>
		
				<cfif showdebug is true>
					CommID cannot be found and built
				</cfif>
		
			</cfif>
	<cfelse>

		<cfif showdebug is true>
			CommID is not numeric
		</cfif>
	
	</cfif>