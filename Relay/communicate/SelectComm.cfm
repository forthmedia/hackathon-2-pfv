<!--- �Relayware. All Rights Reserved 2014 --->


<!---
File name: SelectComm.cfm		
Author:	Chris Snape		
Date created: 2001-10-01	

	Objective - allows the user to select a communication that will be sent to the personids passed in as a  
				parameter
		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

25-Oct-2001			CPS			Amended to use new attributes on commdetail
02-Nov-2001			CPS			Insert another select box to allow re-sending of communications
2007-06-12			WAB			javascript behind the view button was erroring.  Have fixed the error, but view doesn't actually work!
2009/09/08			WAB	LID 2574	Output of result string had not been updated for new comms code
2012-11-26			IH			Case 427937 rewrote large part of this script
2012-11-28			IH			Case 427937 only show communications that has been sent
2012-11-28			IH			Case 427937 fix search box border on IE
2012-11-28			IH			Case 427937 improve qComm query, formatting changes
2013-02-26			WAB			Case 433858 modified Case 427937 changes.  Post to sendCommunication.cfm not commDetailsHome.cfm
2013--05-08			WAB			Case 433974 filter on emailStatus was wrong.  Also added in feedback if people filtered out. 

Enhancement still to do:

--->


<!--- WAB 2013/01
	TBD - this code is submitting to the wrong place

--->



<cfparam name="frmCommFormLID" default = "2,5,6,7">  <!--- show both email and fax --->
<cfparam name="frmShowTest" default = "0">
<cfparam name="URL.frmPersonIDs" >
<cfparam name="URL.searchString" default="">
<cfset URL.searchString=trim(URL.searchString)>

<CFSET showrecords = 20>
<!--- WAB 2012-01 TBD lockeddate is not used any more --->
<cfquery name="qComm" DATASOURCE="#application.SiteDataSource#">
	SELECT  TOP #showrecords# c.CommID
	      ,c.Title
		  ,c.Description
		  ,max (CASE WHEN c.sendDate is not null THEN cs.sentDate END) as sentDate
	FROM Communication c
	INNER JOIN ((UserGroup INNER JOIN Person ON UserGroup.PersonID = Person.PersonID) 
	INNER JOIN Location ON Person.LocationID = Location.LocationID) ON c.CreatedBy = UserGroup.UserGroupID
	INNER JOIN Country ON Location.CountryID = Country.CountryID
	LEFT JOIN commselection cs ON (c.CommID = cs.CommID)
	WHERE (c.sent = 1 or cs.sent = 1)
	<!--- and (select distinct 1 from commselection where commselection.commid = com.commid and commselectlid = <cf_queryparam value="#application.com.communications.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  and sent=1) = 1 --->
	<cfif frmCommFormLID is not "">and c.commformlid  in (<cf_queryparam value="#trim(frmCommFormLID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)</cfif>
	<cfif frmShowTest is 0>and c.commTypeLid <> 27</cfif>	
	<cfif URL.searchString is not "">
		and (
			c.title like <cf_queryparam value="%#URL.searchString#%" CFSQLTYPE="CF_SQL_VARCHAR">
			<cfif isNumeric(URL.searchString)>or c.CommID = <cf_queryparam value="#URL.searchString#" CFSQLTYPE="CF_SQL_INTEGER"></cfif>
			<cfset preparedSearchString = replaceList(URL.searchString,'-, ,.','/,/,/')>	
			<cftry>
				<cfif LSIsDate(preparedSearchString) or isDate(preparedSearchString)>or CONVERT(DATE,(CASE WHEN c.sendDate is not null THEN cs.sentDate END)) = <cf_queryparam value="#DateFormat(preparedSearchString,'yyyy-mm-dd')#" CFSQLTYPE="CF_SQL_DATE"></cfif>
				<cfcatch> <!--- just supress the error ---> </cfcatch>
			</cftry>
		)	
	</cfif>	
	Group By c.CommID  ,c.Title ,c.Description
	order by max (CASE WHEN c.sendDate is not null THEN cs.sentDate END) desc, c.Title desc
</cfquery>

<cf_head>
	<cf_title>Send Previous Communication</cf_title>
	<style>
		#sendCommToPeople, #selectCommToSend {margin:0 auto; padding:0}
		#sendCommToPeople td, #selectCommToSend td {padding:.1em .5em}
	</style>
</cf_head>


	<cfquery name="qPersonList" datasource="#application.SiteDataSource#">
		SELECT p.personid,p.firstname + ' ' + p.lastname as name, p.email, organisationName,
		CASE WHEN p.active = 0 OR o.active = 0 OR p.emailstatus > #application.com.settings.getSetting("communications.limits.numberOfFailedEmailsUntilStopSending")# then 0 else 1 end as OKToSend,
		CASE WHEN p.active = 0 OR o.active = 0 THEN 'Inactive' WHEN p.emailstatus > #application.com.settings.getSetting("communications.limits.numberOfFailedEmailsUntilStopSending")# then #application.com.communications.emailStatusInWordsSQLSnippet("p")# else #application.com.communications.emailStatusInWordsSQLSnippet("p")# end as StatusMessage
		FROM person p inner join organisation o on p.organisationID = o.organisationID 
		WHERE p.personID in (<cfqueryparam value="#URL.frmpersonids#" cfsqltype="cf_sql_integer" list="true">)
	</cfquery>	

	<cfquery name="qPersonListGood" dbtype="query">
	select * from qPersonList
	where OKToSend = 1
	</cfquery>

	<cfquery name="qPersonListBad" dbtype="query">
	select * from qPersonList
	where OKToSend = 0
	</cfquery>

	<cfset aEvenOdd=listToArray("evenrow,oddrow")>
	<cfoutput>
		<br/><br/>		
		<table class="withBorder" id="sendCommToPeople">
			<tr><th class="label" colspan="4">Send Communication to the Following People</th></tr>
			<cfif qPersonListGood.recordCount>
				<tr><th >Name</th><th >Organisation</th><th >Email</th><th >Email Status</th></tr>
				<cfloop query="qPersonListGood">
				<tr class="#aEvenOdd[(currentRow mod 2)+1]#">
					<td>#Name#</td>
					<td>#organisationName#</td>
					<td>#Email#</td>
					<td>#StatusMessage#</td>
				</tr>
				</cfloop>				
			</cfif>
			<cfif qPersonListBad.recordCount>
					<tr class="warningblock" valign><td colspan="4" style="padding:10px;padding-left:30px">These people are not eligible to receive communications.</td></tr>
					<th >Name</th><th >Organisation</th><th >Email</th><th >Reason</th>
				<cfloop query="qPersonListBad">
				<tr class="#aEvenOdd[(currentRow mod 2)+1]#">
					<td>#Name#</td>
					<td>#organisationName#</td>
					<td>#Email#</td>
					<td>#StatusMessage#</td>
				</tr>
				</cfloop>
			</cfif>			
		</table>
		<br/><br/>
	</cfoutput>		


<cfif qPersonListGood.recordCount>
	<div style="margin:auto; width:80%;">
	<!--- javascript:manageClick('tid_tabItem_tabItem9_2','Communications','/communicate/commcheck.cfm','',{iconClass:'communicate'}) --->	
		<table style="margin:0 auto;">
			<tr>
				<td>Select Communication to Send</td>
				<td id="searchHolder" style="text-align:right">
					<form method="get">
						<CF_INPUT TYPE="hidden" NAME="frmPersonIds" VALUE="#frmPersonIds#">						
						<span style="width:auto;float:right;padding-right:5px"><input type="text" onblur="this.value=(this.value=='') ? 'Search' : this.value;" onfocus="this.value=(this.value=='Search') ? '' : this.value;" value="Search" maxlength="32" name="searchString"></span>
					</form>
				</td>
			</tr>
			<cfif qComm.recordcount>
			<tr>
				<td colspan="2">			
					<cfoutput>
					<form action="/communicate/sendCommunication.cfm" method="POST" style="margin:0;padding:0">
						<CF_INPUT TYPE="hidden" NAME="frmPersonIds" VALUE="#valueList(qPersonListGood.personid)#">
						<CF_INPUT TYPE="hidden" NAME="frmNextPage" VALUE="">
						<CF_INPUT TYPE="hidden" NAME="includemessage" VALUE="true">				
						<CF_INPUT TYPE="hidden" NAME="outputmessage" VALUE="true">				
						<CF_INPUT TYPE="hidden" NAME="frmResend" VALUE="true">				
						
					</cfoutput>				
					<table class="withBorder" id="selectCommToSend">
						<tr>
							<th>Date Sent</th>
							<th>Communication No.</th>
							<th>Title</th>
							<th>Select</th>
						</tr>			
					<cfloop query="qComm">
						<cfif currentrow mod 2 is not 0><cfset rowclass="oddrow"><cfelse><cfset rowclass="evenrow"></cfif>			
						<cfoutput>
						<tr class="#rowClass#">
							<td>#lsDateFormat(SentDATE)#</td>
							<td>#commid#</td>
							<td>#htmlEditformat(title)#</td>
							<td><CF_INPUT type="RADIO" name="frmCommID" value="#commid#" checked="#iif(currentRow eq 1,true,false)#"></td>
						</tr>
						</cfoutput>
					</cfloop>				
					</table>				
				</td>
			</tr>
			<cfelse>
			<tr>
				 <td colspan="2" align="center">There are no communications that you can send to the selected <cfif qComm.recordCount is 1>person<cfelse>people</cfif>. <cfif URL.searchString neq "">Please try a different search.</cfif></td>
			</tr>
			</cfif>
			<cfif qComm.recordcount>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="  Continue  " id="commContinue" /></form></td>
			</tr>
			</cfif>
		</table>
	</div>
</cfif>



