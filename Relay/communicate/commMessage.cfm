<!--- �Relayware. All Rights Reserved 2014
Edit History
2016-01-29	SB 	Removed tables and realigned layout
2016/02/09	NJH	Changed check file to simply trigger form submit. Validation now down through RW form validation.
--->
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">
<CFPARAM name="stepNumber" default="">


<cfset communication = application.com.communications.getCommunication(commId=frmCommID)>
<cfset messageContent = communication.getMessageContent()>
<cfset orgLogos = messageContent.image>
<cfset urlImageExists = false>

<cfloop list="1,#application.com.settings.getSetting('theClient.clientOrganisationID')#" index="organisationID">
	<cfset vendorLogoQry = application.com.relatedFile.getFileDetailsByEntityID(entityID=organisationID,entityType="organisation",FileCategory="logos")>
	<cfif vendorLogoQry.recordCount and fileExists(vendorLogoQry.fileHandle[1])>
		<cfset orgLogos = listAppend(orgLogos,replaceNoCase(vendorLogoQry.webHandle[1],left(vendorLogoQry.webHandle[1],findNoCase("/content",vendorLogoQry.webHandle[1])),"/"))>
	</cfif>
</cfloop>

<cfset orgLogos = listAppend(orgLogos,application.com.settings.getSetting("communications.message.defaultImage"))>

<cfif messageContent.image eq "">
	<cfset messageContent.image = listLast(orgLogos)>
</cfif>

<cfif messageContent.title eq "">
	<cfset messageContent.title = "You have received an email communication from [[communication.fromDisplayName]]">
</cfif>

<!--- crude way of doing placeholder text - should be replaced with placeholder, but am leaving it for now, as IE 9 doesn't yet recognise it --->
<cfset text = messageContent.text>
<cfset defaultText = "Input your message here">
<cfset defaultClass="">
<cfif text eq "">
	<cfset text = defaultText>
	<cfset defaultClass="default">
</cfif>

<cf_head>
	<cfoutput>
	<script>

		<cfset count=0>
		var messageImages = [<cfloop list="#orgLogos#" index="orgLogo"><cfset count++>'#JsStringFormat(orgLogo)#'<cfif count lt listLen(orgLogos)>,</cfif></cfloop>];
		var messageIndex = 0;

		for (var i = 0; i < messageImages.length; i++) {
			if (messageImages[i] == '#JsStringFormat(messageContent.image)#'){
				messageIndex = i;
				break;
			}
		}

		scrollImages = function(indexInc) {
			if (messageIndex == 0 && indexInc == -1) {
				messageIndex = messageImages.length;
			}
			messageIndex = (messageIndex+indexInc)%messageImages.length;
			$('msgImage').src = messageImages[messageIndex];
			$('frmMessageImage_orig').value = messageImages[messageIndex];
		}

		checkFormcommMessage = function(action) {
			document.commMessage.frmRequestedAction.value = action;
			/*if (document.commMessage.onsubmit()) {
				document.commMessage.submit();
			}*/
			triggerFormSubmit(document.commMessage);
		};

		removeDefault = function() {
			if ($('frmMessageText').value == '#JsStringFormat(defaultText)#') {
				$('frmMessageText').value='';
				$('frmMessageText').removeClassName('default');
			}
		}

		setDefault = function() {
			if ($('frmMessageText').value=='') {
				$('frmMessageText').value ='#jsStringFormat(defaultText)#';
				$('frmMessageText').addClassName('default');
			}
		}
	</script>

	<!--- Removed from here and put in commDetailsHome because of IE 8
	<style>
		.default {color:##B3B3B3;}
	</style> --->
	</cfoutput>
</cf_head>


<h2>phr_commMessageHeader</h2>
<form name="commMessage" id="commMessage" method="post" novalidate enctype="multipart/form-data" <!--- onsubmit="relayFormValidation(this)" --->>
	<cf_relayFormDisplay bindValidationWithListener=false autoRefreshRequiredClass=true class="">
		<cf_input type="hidden" name="frmCommID" value="#frmCommID#">
		<cf_input type="hidden" name="frmCallingTemplate" value="#frmCallingTemplate#">
		<cf_input type="hidden" name="frmMessageImage_orig" value="#messageContent.image#">
		<cf_input type="hidden" name="frmMessageText_default" value="#defaultText#">
		<cf_input type="hidden" name="frmRequestedAction" id="frmRequestedAction" value="">
	<p>Recipients will receive the following message within their activity stream including a link to the content.</p>
		<div id="commMessageContainer01">
			<!--- If communication is approved then this function will return a block of warning text --->
			<cfoutput>#communication.getEditMessageBlock()#</cfoutput>
		</div>

		<cfset validateMergedContent = communication.prepareAndValidateContent (doEMail = false,domessage=true)>
	<cfif not validateMergedContent.isOK>
		<div id="commMessageContainer02">
			<cfoutput>
				<div class="errorblock">#validateMergedContent.errorMessage#</div>
			</cfoutput>
		</div>
	</cfif>

	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="Message" labelAlign="right" fieldname="frmMessageTitle" spanCols="false">

		<cfif messageContent.image neq "">
			<cfset urlImageExists = true>
		</cfif>

		<cfoutput>
			<div id="msgImageDiv" style="<cfif not urlImageExists>border:1px solid ##666666;</cfif>">
				<cfif urlImageExists>

						<cfif listLen(orgLogos) gt 1>
							<div id="commMessageNavLeft">
								<a href="" onclick="javascript:scrollImages(-1);return false;"><span class="fa fa-arrow-circle-left"></span></a>
							</div>
						</cfif>

						<div id="commMessageImage">
							<cfoutput>
								<img id="msgImage" src="#messageContent.image#">
							</cfoutput>
						</div>

						<cfif listLen(orgLogos) gt 1>
							<div id="commMessageNavRight">
								<a href="" onclick="javascript:scrollImages(1);return false;">
									<span class="fa fa-arrow-circle-right"></span>
								</a>
							</div>
						</cfif>


				<cfelse>
					<span id="noImage" style="">No image set</span>
				</cfif>
			</div>
		</cfoutput>

		<div class="form-group">
			<cf_relayFormElement relayFormElementType="text" label="Title" labelAlign="right" fieldname="frmMessageTitle" currentValue="#messageContent.title#" size="67" maxLength="150" required="#communication.sendMessage#" requiredFunction="return !$('frmDoNotSendMessage').checked">
		</div>
		<div class="form-group">
			<cf_relayFormElement relayFormElementType="textArea" label="Message" labelAlign="right" fieldname="frmMessageText" currentValue="#text#" maxChars="255" submitFunction="if (jQuery(this).hasClass('default') && !jQuery('##frmDoNotSendMessage').prop('checked')) {return 'Required';} else {return '';}" maxLength="255" class="#defaultClass#" onFocus="javascript:removeDefault();" onblur="javascript:setDefault();" placeholder="Input your message here">
		</div>

	</cf_relayFormElementDisplay>

	<cfset required=true>
	<cfset requiredFunction="return !$('frmDoNotSendMessage').checked">
	<cfif not communication.sendMessage>
		<cfset required=false>
	</cfif>

	<cfif urlImageExists and fileExists(expandPath(messageContent.image))>
		<cfset required=false>
		<cfset requiredFunction = "">
	</cfif>
	<cf_relayFormElementDisplay relayFormElementType="file" label="Image" fieldname="frmMessageImage" currentValue="" required="#required#" requiredFunction="#requiredFunction#" acceptType="image" >

	<div class="checkbox">
		<label for="frmDoNotSendMessage"><cf_relayFormElement relayFormElementType="checkbox" fieldname="frmDoNotSendMessage" currentValue="#not(communication.sendMessage)#">
			phr_comm_doNotSendMessage
		</label>
	</div>


	<CFINCLUDE TEMPLATE="saveAndContinueButtons.cfm">
	</cf_relayFormDisplay>
</form>

