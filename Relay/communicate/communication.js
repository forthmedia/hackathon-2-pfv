/* �Relayware. All Rights Reserved 2014 */
/*
communication.js
Various functions used within the communication tabs
WAB 2013-10-05  During CASE 437315 Changed the way messages were updated.  
		When switching between tabs the setTimeout kept going trying to update hidden messages, and the visible message wasn't updated
*/

deleteOldMessages = function ()  {
	
	var commResultsDivs = $$('.commResultsDiv')
	commResultsDivs.each (function(commResultsDiv) {
		var itemsToDelete = commResultsDiv.select ('.infoblock:not(.loading),.errorblock:not(.loading)')
		itemsToDelete.each (function (obj) {$(obj).removeSpinner({force:true}).remove()})
	})
}


updateCommMessageDiv = function (messageDiv,isOK,message,continuing) {

	if (!messageDiv) {
		messageDiv = createCommMessageDiv()
	}

	var className = (isOK) ? 'infoblock' : 'errorblock'
	// the message box is two divs - we put the loading icon on the outer div and the message in the inner div - this prevents the update of the message div deleting the loading icon
	messageDiv.addClassName (className).select('.messageArea')[0].update(message)
	
	if (continuing) {
		// display a loading icon, the loading class prevents the message being deleted by the deleteOldMessages function
		messageDiv.addClassName('loading').spinner({position:'left'})	
	} else {
		messageDiv.removeClassName ('loading').removeSpinner({force:true})	
	}
	
	return messageDiv
}


createCommMessageDiv = function () {
	
	/* 	Message is to be put inside a div with class commResultsDiv, 
		but confusingly there can be more than one of these, one on the active tab and one on a hidden tab
		Therefore need to find them all and work out which is visible
	*/	
	var result = null
	
	var commResultsDivs = $$('.commResultsDiv')
	
	commResultsDivs.each (function(commResultsDiv) {
			if (isVisible(commResultsDiv)) {
				$(commResultsDiv).insert({bottom:'<div ><div class="messageArea"></div></div>'})
				result = $(commResultsDiv).childElements()[$(commResultsDiv).childElements().length-1]
			}	
	})

	return result
}

/* A specific isVisible() function for use to within the CFLayout tabs */
isVisible = function (elm) {
    while(elm.tagName != 'BODY') {
        if(!$(elm).visible() || elm.style.visibility == 'hidden') return false;
        elm = elm.parentNode;
    }
    return true;
}


sendComm = function (commid,obj,options) {
			var page = '/webservices/callWebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&returnFormat=json&_cf_nodebug=true'
			var parameters = {test:false,asynchronous:true,resend:false,methodname:'sendComm'}

			Object.extend(parameters, options || { });

			deleteOldMessages ()
			parameters.commid = commid
			$(obj).spinner()			
			var myAjax = new Ajax.Request(
				page, 
				{
					method: 'get', 
					 parameters: parameters, 
					debug : false,
					evalJSON: 'force',
					onComplete: function (transport) {
						$(obj).removeSpinner()	
						var json = transport.responseText.evalJSON();	
						var messageDiv = updateCommMessageDiv(null,json.ISOK,json.MESSAGE,json.ASYNCHRONOUS)
						// if we are in the tabbed interface, make sure that stats tab is showing
						if(window.ColdFusion) {
							window.ColdFusion.Layout.showTab("commDetailsLayout","Statistics")
						}

						if (json.CONTINUE) {
							updateCommStatus (commid,json.COMMSENDRESULTID,messageDiv)
						}

					}
				});
}


updateCommStatus = function (commid, commSendResultID, messageDiv )  {
			var page = '/webservices/callWebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&methodname=getCommunicationSendStatus&returnFormat=json&_cf_nodebug=true'
			var parameters  = {commid :  commid,commSendResultID:commSendResultID,asynchronous:false}
			/* TBD - we could stop this update if the tab is no longer the active one */
			if (messageDiv && isVisible(messageDiv)) {
				var myAjax = new Ajax.Request(
					page, 
					{
						method: 'get', 
						parameters: parameters, 
						debug : false,
						evalJSON: 'force',
						onComplete: function (transport) {
							var json = transport.responseText.evalJSON();	
							messageDiv = updateCommMessageDiv(messageDiv,json.ISOK,json.MESSAGE,json.CONTINUE)
							if (json.CONTINUE) {
								window.setTimeout (function () {updateCommStatus (commid = commid,commSendResultID = commSendResultID,messageDiv)} ,15 * 1000)
							} else {
								commResultDiv.removeClassName ('loading')	
							}
					
						}
					});
			}	

}


cancelCommSend = function (commid,obj,options) {
			var page = '/webservices/callWebservice.cfc?wsdl&method=callwebservice&webservicename=communicationsWS&returnFormat=json&_cf_nodebug=true'
			var parameters = {methodname:'cancelCommSend'}
			parameters.commid = commid

			Object.extend(parameters, options || { });

			var myAjax = new Ajax.Request(
				page, 
				{
					method: 'get', 
					 parameters: parameters, 
					debug : false,
					evalJSON: 'force',
					onComplete: function (transport) {
						var json = transport.responseText.evalJSON();	
						if (!json.ISOK) {
							alert (json.MESSAGE)						
						} else (
							alert ('Cancellation Requested')
						)
					}
				});
}




/* Opens a new tab for a comm, reuses an existing tab and switches to a subtab if required */
openCommTab = function (commid,subTabName)  {

		var url = '/communicate/commDetailsHome.cfm?frmcommid=' + commid
	
		if (subTabName != '') {
			url += '&FrmNextPage=' + subTabName
		}
	
		openNewTab(	'comm_' + commid,
					'Comm ' + commid,
					url,
						{iconClass:'communicate',
						reuseTab:'true',
						reuseTabJSFunction:function(iframeObj){
																// test whether this page has the communication layout on it
																if (iframeObj.ColdFusion && iframeObj.ColdFusion.Layout) {
																	if (subTabName != '') {
																		iframeObj.ColdFusion.Layout.selectTab('commDetailsLayout',subTabName);
																	} 
																	return true
																} else {
																	return false 
																}	
															}
						}) ;

		return 						
}