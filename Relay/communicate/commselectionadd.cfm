<!--- �Relayware. All Rights Reserved 2014 --->
<!---
commselectionadd.cfm


Author WAB 2004-07-13


Saves the selections chosen in commselection

This code used to be at the beginning of commconfirm.cfm
but put into a separate file so that could commselection.cfm could be used by itself

**************************
NB:  Any selection made up of users in View: vOrgsToSupressFromReports will automatically
be set to Internal, ie frmCommSelectLID=12, regardless of parameters passed.
vOrgsToSupressFromReports is all orgs starting with 'foundation network', 'test', 'ttest%', 'ttt%' or 'FNL%'

selections are added by com.communications.addSelectionToCommunication function
**************************

Amendment History

WAB 2007-03-22
2011-07-04	NYB		P-LEN024 - various changes
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
--->

<CFPARAM name="frmFailTo" default= "#thisDir#/commMenu.cfm">
<CFPARAM NAME="frmCommSelectLID" default = "0">
<CFPARAM NAME="frmSelectID" default = "">
<CFPARAM NAME="useWrapper" DEFAULT="false">

<cfsilent>
<!--- returns query with all the details of the communication--->
<cfif not isdefined("communication")>
	<cfset communication = application.com.communications.getCommunication (frmCommid,request.relayCurrentUser.personid)>
</cfif>

<CFQUERY NAME="checkSelection" DATASOURCE="#application.SiteDataSource#">
	SELECT CommID FROM Communication
	 WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 AND dbo.dateWithoutMilliseconds(LastUpdated) =  <cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
</CFQUERY>

<CFIF checkSelection.Recordcount IS 0>
	<CFSET message = "The communication record has been changed by another user since you last downloaded it.  Please begin again.">
	<CF_ABORT>

<cfelseif not communication.hasrightsToEdit()>
		<CFSET message ="Sorry, you do not have rights to edit this communication.">
		<CFSET message = message & " <A HREF=""#thisDir#/commheader.cfm?frmCommID=#frmCommID#&btnTask=edit"">Click here</A> to view the current record.">
			<CFINCLUDE TEMPLATE="#frmFailTo#">
		<CF_ABORT>
</CFIF>

<!--- check to see if there are already selections for this communciation --->

<CFIF frmpersonids is "">

	<CFIF structkeyexists(form,"selections_assigned")>
		<cfset communication.updateSelections(selectionid=selections_assigned,CommSelectLID=application.com.communications.constants.EXTERNALSELECTLID)>
	<CFELSE>
		<cfset communication.updateSelections(selectionid="",CommSelectLID=application.com.communications.constants.EXTERNALSELECTLID)>
	</CFIF>

	<CFIF structkeyexists(form,"SELECTIONS_ASSIGNEDFOREXCLUSION")>
		<cfset communication.updateSelections(selectionid=SELECTIONS_ASSIGNEDFOREXCLUSION,CommSelectLID=application.com.communications.constants.EXCLUDESELECTLID)>
	<CFELSE>
		<cfset communication.updateSelections(selectionid="",CommSelectLID=application.com.communications.constants.EXCLUDESELECTLID)>
	</CFIF>

	<cfset communication.updateCountryIDListIfRequired(delay=false)>
<CFELSE>
	<!--- This communication is to a list of people, not a selection - add a dummy record (needed in get comminfo query - not sure whether this is best way to do this) --->
	<CFQUERY NAME="saveSelection" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
		Values( <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0, <cf_queryparam value="#application.com.communications.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >)  <!--- WAB 2005-05-03 changed from 0,11   --->
	</CFQUERY>
	<CFPARAM name="frmselectid" default="">   <!--- needed later in template by regular communication --->

	<!--- add personids to comm detail
	used to be done much later in the process but moved here WAB 2005-05-04
	 --->

	 <!--- TBD check that this still works --->
	<CF_populateCommDetail
		commid = #frmCommID#
		personids = #frmpersonids#
		resend = false	>
</cfif>

</cfsilent>

