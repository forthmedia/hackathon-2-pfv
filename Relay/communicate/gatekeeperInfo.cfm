<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
gatekeeperinfo.cfm

just a very quick report 

2010/12/07			MS		LID4922: Gatekeeper Info - reference to communicate.ini




--->
<cfset approvalSettings = application.com.settings.getSetting ("communications.approvals")>

<cfif application.com.flag.doesFlagExist("CommunicationGateKeeperCountry")>

	<cfset gatekeepers = application.com.flag.getFlagData(flagid = "CommunicationGateKeeperCountry")>

	<cfquery name = "sortgatekeepers" dbtype="query">
	select data_name as country, name as gatekeeper from gatekeepers
	order by data_name, name 
	</cfquery>


	<table>
		<tr><td colspan="2"><B>GateKeepers by Country</B></td></tr>
	<cfoutput query = "sortgatekeepers" group="country">
		<tr><td colspan="2">#htmleditformat(country)#</td></tr>
		<cfoutput>
		<tr><td >&nbsp;&nbsp;	</td><td >#htmleditformat(gatekeeper)#</td></tr>
		
		
		</cfoutput>
	
	</cfoutput>
		<tr><td colspan="2">People can be set/unset as gatekeepers by going to the person's record and selecting the screen "Admin Screens" <BR>
		This function is only available to system administrators
		</td></tr>
	</table>

<cfelse>
		No Gatekeepers set up	

</cfif>




<cfset SelectLIDListNames = "External,Approver,Internal">
<cfset SelectLIDList = "#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.ApproverSelectLID#,#application.com.communications.constants.InternalSelectLID#">
<P>&nbsp;</P>
<cfoutput>
	<cfif isDefined("request.CommunicationSettings.DefaultSelections")>

		<table>
		<tr><td colspan=3><B>Default Selections</B></td></tr>
			<cfloop index ="i" from = "1" to = "#listlen(SelectLIDList)#">
				
				<tr><td>&nbsp;</td><td valign = top>  #listgetat(SelectLIDListNames,i)#</td><td><cfif structKeyExists(request.CommunicationSettings.DefaultSelections, listgetat(SelectLIDList,i)) ><cfloop index="selectionid" list = "#request.CommunicationSettings.DefaultSelections[listgetat(SelectLIDList,i)]#">#htmleditformat(application.com.selections.getSelection(selectionid).title)# (#htmleditformat(selectionid)#) <A href="/selection/selectreview.cfm?frmsearchid=#selectionid#">view</A><BR></cfloop> </cfif></td></tr>
			</cfloop>

		<!--- 2010/12/07	MS		LID4922: Removing reference to communicate.ini --->
		<!--- <tr><td colspan=3>These settings are made in the communicate.ini file</td></tr> --->

		</TABLE>

	</cfif>
</cfoutput>
<P>&nbsp;</P>
<cfoutput>
	<cfif isDefined("request.CommunicationSettings.DefaultCountrySelections")>
	<table>
	<tr><td colspan=4><B>Default Country Selections</B></td></tr>
	<cfset countrylist = structKeyList(request.CommunicationSettings.DefaultCountrySelections)>
	<cfloop index= countryid list = "#countrylist#">
		<tr><td>&nbsp;</td><td colspan= 3>#application.CountryName[countryid]#</td>
		<cfloop index ="i" from = "1" to = "#listlen(SelectLIDList)#">
		<tr><td>&nbsp;</td><td>&nbsp;</td><td valign = top>  #listgetat(SelectLIDListNames,i)#</td><td><cfif structKeyExists(request.CommunicationSettings.DefaultCountrySelections[countryid], listgetat(SelectLIDList,i)) ><cfloop index="selectionid" list = "#request.CommunicationSettings.DefaultCountrySelections[countryid][listgetat(SelectLIDList,i)]#">#htmleditformat(application.com.selections.getSelection(selectionid).title)# (#htmleditformat(selectionid)#) <A href="/selection/selectreview.cfm?frmsearchid=#selectionid#">view</A> <BR></cfloop> </cfif></td></tr>
		</cfloop>

	</cfloop>
	<!--- 2010/12/07	MS		LID4922: Removing reference to communicate.ini --->
	<!--- <tr><td colspan=4>These settings are made in the communicate.ini file</td></tr> --->

	</table>		
</cfif>
</cfoutput>

<P></P>
<cfoutput>
	
	<table>
	<tr><td colspan=4><B>Scheduling</B></td></tr>
	<tr><td colspan=2>Previews of Communications will be scheduled #htmleditformat(approvalSettings.Gatekeeper.Preview.TimeInHours)# hours before the main communication </td></tr>
	<tr><td colspan=2>These will be sent to </td></tr>
		<cfif listfind(approvalSettings.Gatekeeper.Preview.selectLIDS,application.com.communications.constants.ApproverSelectLID) is not 0 ><tr><td>&nbsp;</td><td>All Approver Selections </td></tr> </cfif>
		<cfif listfind(approvalSettings.Gatekeeper.Preview.selectLIDS,application.com.communications.constants.InternalSelectLID) is not 0 ><tr><td>&nbsp;</td><td>All Internal Selections</td></tr></cfif>
	<cfloop index="selectionid" list = "#approvalSettings.Gatekeeper.Preview.SelectIDs#"><tr><td>&nbsp;</td><td> selection #htmleditformat(application.com.selections.getSelection(selectionid).title)# (#htmleditformat(selectionid)#) <A href="/selection/selectreview.cfm?frmsearchid=#selectionid#">view</A> </td></tr></cfloop>
	</P>
	<!--- 2010/12/07	MS		LID4922: Removing reference to communicate.ini --->
	<!--- <tr><td colspan=4>These settings are made in the communicate.ini file</td></tr> --->
	<tr><td colspan=3>These settings can be set  <a href="/admin/settings/listsettingsections.cfm?section=communications" >here</a></td></tr>
	</table>
</cfoutput>






