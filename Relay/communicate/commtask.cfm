<!--- �Relayware. All Rights Reserved 2014 --->
<CFIF IsDefined("btnTask")>
	<CFIF #btnTask# IS "send">
		<cfinclude template="#thisDir#/commselection.cfm">
		<CF_ABORT>
	<CFELSEIF #btnTask# IS "edit">
		<cfinclude template="#thisDir#/commheader.cfm">
		<CF_ABORT>		
	</CFIF>	
<CFELSE>
	<!--- someone tried to get in without a parameter so 
		just cleanly send them somewhere harmless --->
	<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>		
</CFIF>