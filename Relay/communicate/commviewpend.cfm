<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
	SecurityTask:commTask
	Permission: Read
	---->
<!--- amendment history
	WAB: 2001-02-07   problem with frmCommID having two meanings sorted out
	Also added commid to the lists
	WAB 	1999-05-17  Added _eurodate to the rescheduled date
	WAB 2009/06   LID 2361 replaced call to templates/qrygetcommtosend.cfm
	2011/07/27	NYB		P-LEN024 changed comm checkDetails.cfm to commDetailsHome.cfm
	2012-06-13 	PPB 	Case 428802 adminuser variable has been deprecated
	--->
<CFSET usedate = Now()>
<CFSET enumerateall = "yes">
<!--- ignores date in qrygetcommtosend.cfm --->
<!--- admin users can change anyone's communication,
	other users can only change their own.
	use qrycheckperms.cfm to determine if admin user
	but rerun qrycheckperms.cfm for communication security
	to reset query values as expected for entire template
	in communication subdirectory
	--->
<!---  WAB: 2001-02-07
	frmCommID is being used in two places to mean two different things!
	on this template creates it as a form variable and it is used to hold the id of the comm to be altered
	in qrygetcommtosend.cfm it is now (since 12/00) used to specify getting a single communication
	So set it to "" here to make sure that it doesn't screw up the qrygetcommtosend.cfm query.  It has already done its job as a form variable
	--->
<CFSET frmCommID = "">
<!---
	WAB 2009/06   LID 2361
	<cfinclude template="/templates/qrygetcommtosend.cfm">
	--->
<cfset getCommInfo = application.com.communications.getCommunicationsScheduledForNowAndFuture()>
<!--- ran a query called "getCommInfo" --->
<!--- the above query finds the pending communications --->
<!--- now find the relevant text --->
<!--- get CommInfo considers each type (fax/email) as different
	Communication --->
<!--- #ListAppend("0",ValueList(getCommInfo.CommID))#
	produced (0,) if getCommInfo was empty
	Access did not mind this syntax, SQL Server did
	--->
<CFQUERY NAME="getCommCount" DATASOURCE="#application.SiteDataSource#">
SELECT Count(*) AS CommCnt
  FROM Communication
 WHERE CommID  IN ( <cf_queryparam value="#IIF(getCommInfo.RecordCount IS 0,DE("0"),DE(ValueList(getCommInfo.CommID)))#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>
<cfinclude template="/templates/qrygetcommOnHold.cfm">
<cf_head>
	<cf_title>
		<CFOUTPUT>
			#request.CurrentSite.Title#
		</CFOUTPUT>
	</cf_title>
	<SCRIPT type="text/javascript">
<!-- hide from other browsers
	function doCancel(form) {
		var found = "";

		if (form.frmCommID.length) {
			for (cnt=0; cnt < form.frmCommID.length; cnt++) {
				if (form.frmCommID[cnt].checked) {
					found = "yes";
					break;
				}
			}
		} else if (form.frmCommID) {
			if (form.frmCommID.checked)
				found = "yes";

		}

		if (found == "") {

			msg = "\nYou did not select any communications to pause.";
			alert(msg);

		} else {
			var msg = "\nTagged communications currently scheduled will be put on hold.  Are you sure you wish to do this?";
			if (confirm(msg)){
				form.submit();
			}

		}

		return;

	}


	function doUpdate(form) {

		var msg = "";
		var todel = "";
		var toschedule = "";
		var toprocess = form.frmNumOfHold.value;

		<!--- variables are  name:value
				frmCommID:commid for checkbox
				frmLU_commid:lastupdated
				frmSendDate_commid: datetosend
				frmTitle_commid: Title
		--->
		// only one checkbox to process
		if (toprocess == 1) {
			 var deletethis = "no";
			 <!--- find the commid --->
			 var commid = form.frmCommID.value;

	 		 if (form.frmCommID.checked) {
				<!--- determine comms to be deleted --->
					todel += "\n    * " + eval("form.frmTitle_" + commid + ".value");
					deletethis = "yes";
			 }
			<!--- if not to be deleted, check date  --->
			if (deletethis == "no" && eval("form.frmSendDate_" + commid + ".value") != "") {
				toschedule += "\n    * " + eval("form.frmTitle_" + commid + ".value");
			}


		} else if (toprocess > 1) {
			//checkbox array to process

			for (cnt=0; cnt< toprocess; cnt++) {
				 var deletethis = "no";
				 <!--- find the commid --->
				 var commid = form.frmCommID[cnt].value;
				<!--- determine comms to be deleted --->
				if (form.frmCommID[cnt].checked) {
					todel += "\n    * " + eval("form.frmTitle_" + commid + ".value");
					deletethis = "yes";
				}
				<!--- if not to be deleted, check date  --->
				if (deletethis == "no" && eval("form.frmSendDate_" + commid + ".value") != "") {
					toschedule += "\n    * " + eval("form.frmTitle_" + commid + ".value");
				}

			}
		}

		if (todel != "")
			todel = "\n\n- to be PERMANENTLY deleted:" + todel;


		if (toschedule != "")
			toschedule = "\n\n- to be scheduled:" + toschedule;

		if (todel != ""  || toschedule != "") {
			msg = "\nCommunications will be updated as follows:" + todel + toschedule;
			if (confirm(msg)) {
				form.submit();
			}

		}
		return;
	}

//-->
</SCRIPT>
</cf_head>
<cfset subsection = "Communication Summary">
<cfparam name="frmHideTopHead" default="false">
<cfif not frmHideTopHead>
	<cfinclude template="#thisDir#/commtophead.cfm">
</cfif>
<CFIF isDefined("frmMessage")>
	<cfoutput>
		#htmleditformat(frmMessage)#
	</cfoutput>
</CFIF>
<div id="commViewMessage">
	<CFIF #GetCommInfo.RecordCount# IS NOT 0>
		<div class="row">
			<div class="col-xs-12">
				<h2>Phr_Sys_ScheduledCommunications:</h2>
				<CFOUTPUT>
					#htmleditformat(GetCommcount.CommCnt)#
				</CFOUTPUT>
			</div>
		</div>
	<CFELSE>
		<div class="row">
			<div class="col-xs-12">
				<div class="bg-success">
					Phr_Sys_Therearenocommunicationsscheduledtobesent
				</div>
			</div>
		</div>
	</CFIF>
</div>
</CENTER>
<cfoutput>
	<FORM NAME="frmQueued" ACTION="#thisDir#/commHold.cfm" METHOD="POST">
</cfoutput>
<table class="table table-striped">
<TR>
	<TD><B>Phr_Sys_Communicationscheduledtobesent </B> </TD>
</TR>
<TR>
	<TD>
		<CFIF GetCommInfo.RecordCount IS  0>
			<p>Phr_Sys_Nocommunicationsfound</p><CFELSE>
			<BR>Phr_Sys_CommsSentCanPaused <CENTER>
				<BR><INPUT class="btn btn-primary" TYPE="button" NAME="btnCancel" VALUE="Pause Communications" onClick="doCancel(this.form);"> </CENTER>
				<br>
				<table class="table table-striped">
					<TR>
						<Th ALIGN="CENTER">Phr_Sys_Pause</Th>
						<Th>Phr_Sys_Communication</Th>
						<Th>Phr_Sys_Sender</Th>
						<Th>Phr_Sys_Sendtoself</Th>
						<Th>Phr_Sys_DateScheduled</Th>
						<Th>Phr_Sys_Format</Th>
					</TR>
					<CFSET cnt = 1>
					<CFOUTPUT QUERY="getCommInfo" GROUP="CommID"> <CFOUTPUT group = "selectionid">
					<TR<CFIF #cnt# MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
					<TD ALIGN="CENTER"> <CFIF request.isAdmin or createdBy IS request.relayCurrentUser.usergroupid
						OR LastUpdatedBy IS request.relayCurrentUser.usergroupid
						> <CF_INPUT TYPE="checkbox" NAME="frmCommID" VALUE="#CommID#-#CreateODBCDateTime(LastUpdated)#"> <CFELSE> &nbsp; </CFIF>
					</TD>
	<TD> <A HREF="#thisdir#/commDetailsHome.cfm?frmCommid=#commid#"> (#htmleditformat(commid)#) #htmleditformat(title)#</A> <cfif selection is not ""><BR>Selection #htmleditformat(selection)# (#htmleditformat(selectionid)#)</cfif> </TD>
	<TD> #Trim(FirstName & " " & LastName)# </TD>
	<td> <cfif createdBy eq request.relayCurrentUser.userGroupId  OR LastUpdatedBy eq request.relayCurrentUser.userGroupId OR request.isAdmin> <A HREF="#thisDir#/sendTestCommunication.cfm?frmCommID=#CommID#">Phr_Sys_SendToself</a> </cfif> </td>
	<TD> <CFIF SendDate IS ""> &nbsp; <CFELSE> #application.com.datefunctions.dateTimeFormat(date=SendDate,dontShowZeroHours = true,separator="<BR>", showAsLocalTime = true)# </CFIF> </TD>
	<TD> <CFSET formList = ""> <!--- list of the forms this communication will take ---> <CFOUTPUT> <CFIF CommFormLID IS 2><CFSET formList = ListAppend(formList,"Email")> <CFELSEIF CommFormLID IS 3><CFSET formList = ListAppend(formList,"Fax")> <CFELSE><CFSET formList = ListAppend(formList,"Other")> </CFIF> </CFOUTPUT> #Replace(formList,",",", ")#</FONT> </TD>
</TR> <CFSET cnt = cnt + 1> </CFOUTPUT> </CFOUTPUT>
				</TABLE> </CFIF>
	</TD>
</TR>
</TABLE> </FORM>
<BR>
<cfoutput>
	<FORM NAME="frmHold" ACTION="#thisDir#/commdelete.cfm" METHOD="POST">
</cfoutput>
<table class="table table-striped">
<TR>
	<TD> <B>Phr_Sys_Communicationscompletebut <B>Phr_Sys_not</B> Phr_sys_scheduled:</B></FONT> </TD>
</TR>
<TR>
<TD> <CFIF getCommOnHold.RecordCount IS 0> Phr_Sys_Nocommunicationsfound. <CFELSE> <p>Phr_Sys_completenotscheduled</p>
<p><INPUT class="btn btn-primary" TYPE="button" NAME="btnUpdate" VALUE="Update Communications" onClick="doUpdate(this.form);"></p>
<table class="table table-hover table-striped"> <TR> <TH ALIGN="CENTER">Phr_Sys_Delete</TH> <TH>Phr_Sys_Communication</TH> <TH>Phr_Sys_Sender</TH> <TH>Phr_Sys_DateScheduled</TH> <TH>Phr_Sys_Media</TH> </TR> <CFSET cnt = 1> <CFSET heldcomms = ""> <CFSET numofhold = 0> <CFOUTPUT QUERY="getCommOnHold" GROUP="CommID"> <TR<CFIF #cnt# MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>> <!--- only the creator or admin user can delete ---> <cfif createdBy eq request.relayCurrentUser.userGroupId  OR LastUpdatedBy eq request.relayCurrentUser.userGroupId OR request.isAdmin> <TD ALIGN="CENTER"> <CF_INPUT TYPE="checkbox" NAME="frmCommID" VALUE="#CommID#"> <CF_INPUT TYPE="HIDDEN" NAME="frmLU_#commid#" VALUE="#CreateODBCDateTime(LastUpdated)#"> <CF_INPUT TYPE="HIDDEN" NAME="frmTitle_#commid#" VALUE="#Title#"> </TD> <CFSET heldcomms = ListAppend(heldcomms,#commid#)> <CFSET numofhold = numofhold + 1> <CFELSE> <TD ALIGN="CENTER">&nbsp;</TD> </CFIF> <TD>(#htmleditformat(commid)#) #htmleditformat(title)#</TD> <TD>#Trim(FirstName & " " & LastName)#</TD> <!--- only the creator or admin user can change the date ---> <CFIF createdBy IS request.relayCurrentUser.usergroupid
	OR LastUpdatedBy IS request.relayCurrentUser.usergroupid
	OR checkpermission.admin> <!--- 2012-06-13 PPB Case 428802 adminuser variable has been deprecated ---> <TD> <!---<INPUT TYPE="text" NAME="frmSendDate_#commid#" SIZE=15 MAXLENGTH=15> ---> <!--- <INPUT TYPE="hidden" NAME="frmSendDate_#commid#_eurodate" SIZE=15 MAXLENGTH=15> ---> <cf_relaydatefield
	currentValue=""
	thisFormName="frmHold"
	fieldName="frmSendDate_#commid#"
	anchorname = "D"
	showClearLink = true
	showAsLocalTime = true
	> <cf_relaytimefield
	currentValue=""
	thisFormName="frmHold"
	fieldName="frmSendDate_#commid#"
	anchorname = "T"
	showAsLocalTime = true
	> </TD> <CFELSE> <TD>&nbsp;</TD> </CFIF> <TD> <CFSET formList = ""> <!--- list of the forms this communication will take ---> <CFOUTPUT> <CFIF CommFormLID IS 2><CFSET formList = ListAppend(formList,"Email")> <CFELSEIF CommFormLID IS 3><CFSET formList = ListAppend(formList,"Fax")> <CFELSE><CFSET formList = ListAppend(formList,"Other")> </CFIF> </CFOUTPUT> #Replace(formList,",",", ")# </TD> </TR> <CFSET cnt = cnt + 1> </CFOUTPUT> </TABLE> <CFOUTPUT> <!--- variables to facilitate processing ---> <CF_INPUT TYPE="hidden" NAME="frmHeldComms" VALUE="#heldcomms#"> <CF_INPUT TYPE="hidden" NAME="frmNumOfHold" VALUE="#numofhold#"> <INPUT TYPE="hidden" NAME="frmReschedule" VALUE="yes"> <!--- at the end of commdelete.cfm, continue to commresched.cfm ---> </CFOUTPUT> </CFIF> </TD> </TR> </TABLE> </FORM>
