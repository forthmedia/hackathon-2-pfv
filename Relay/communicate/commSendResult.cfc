<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
commSendResult.cfc
WAB 2013-01 Sprint 15&42 Comms

Created when communications code was converted to use communication object
This object is used to store, and then output, the results of a sent communication.
At the end of a send I serialise the data stored in it, and store in the commSendResult table.  
The object can then be reinitialised with that data.  These seemed preferable to keeping the object in session scope where it might be forgotten about, and where it cannot be accessed by other sessions.

2013-05-29		WAB		Reduce size of confirmation email (send short table)
2013-10			WAB		CASE 437315 added lots of output=false
 --->

<cfcomponent output=false>
	<cfset variables.defaultIndividualItemLimit = 10>  <!--- max Number of individual items to list in feedback before switching to just displaying totals --->

	<cffunction name="init" output=false>
		<cfargument name="commid">	
		<cfargument name="title">	
		<cfargument name="commFormLID">	
	
			<cfset var thisCommResult = '' />
			<cfset var item = '' />
			
			<!--- Set up the result Structure for this comm--->
			<cfloop collection="#arguments#" item="item">
				<cfset this[item] = arguments[item]>
			</cfloop>
			
				<cfset this.selectionid = "">
			<!--- TBD WAB 
			<cfif variables.commSelectID is not "" and variables.commSelectID is not "-1" >
				<cfset thisCommResult.selectionid = variables.commSelectID>
			</cfif>
			--->
			<cfset this.media = IIf(CommFormLID IS 2, DE('Email'),DE('Fax'))>
			<cfset this.isOK = true>
			<cfset this.ItemArray = arrayNew(1)>
			<cfset this.startTime = now()>
			<cfset this.partialSend = false>

			<cfset this.commSendResultID = createCommSendResultRecord()>
			<cfset this.numberSent = 0>
			<cfset this.numberNotSent = 0>			

	</cffunction>
	

	<cffunction name="initialiseFromDB" output=false>
		<cfargument name="commSendResultID">	

		<cfset var result = false>
		<cfset var getRecord = read (commSendResultID)>
		
		<cfif getRecord.recordCount and isJSON(getRecord.result)>
			<cfset result = true>
			<cfset initialiseWithSerialisedData(getRecord.result)>
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="setMetaData" output=false>
		<cfset var item = "">
		<cfloop collection="#arguments#" item="item">
			<cfset this[item] = arguments[item]>
		</cfloop>
	</cffunction>
	

	<cffunction access="public" name="setError" hint="Sets an error for the whole communication" output=false>
		<cfargument name="Message" >
		<cfargument name="ErrorID" default="0">
		
		<cfset this.isOK = false>
		<cfset this.errorMessage = message>
		<cfset this.errorID = errorID>
	
	</cffunction>


	<cffunction access="public" name="addItem" hint="adds the result of sending a single item to the item array" output=false>
		<cfargument name="item" >

		<cfset arrayAppend(this.itemArray,item)>

		<cfif item.OK>
			<cfset this.numberSent ++>
		<cfelse>
			<cfset this.numberNotSent ++ >			
		</cfif>
		
	</cffunction>


	<cffunction access="public" name="setPeopleCounts" output=false>

		<cfset this.counts = arguments>
	
	</cffunction>
	

	<cffunction name="sendConfirmation" hint = "Send a confirmation Email" output=false>
		<cfargument name="individualItemLimit" default="#variables.defaultIndividualItemLimit#" required="false">
		
		<cfset var confirmemail = '' />
		<cfset var c_adminmsg = '' />
		<cfset var internalSite = '' />
		<cfset var confirmTextHTML = '' />
		<!--- email the site administrator if the owner of
		 the communication (the sender) does not have a valid
		 email address --->
	
		<CFIF Trim(this.creatorEmail) IS NOT "" AND ListLen(this.creatorEmail, "@") IS 2>
			<CFSET confirmemail = 	this.creatorEmail>
			<CFSET c_adminmsg = "">				
		<CFELSE>
			<CFSET confirmemail = application.Supportemail>
			<CFSET c_adminmsg = "The owner of the following sent communication does not have a valid email address.#lb##lb#">
		</CFIF>
	
			<cfset internalSite = application.com.relayCurrentSite.getInternalSite()>
			<cfsavecontent variable="confirmTextHTML">
			<cfoutput>
			<p>#htmleditformat(this.creatorName)# </p>
			
			<p>Your communication: #htmleditformat(this.Title)# (#htmleditformat(this.Description)#) </p>
			
			<p>Started processing at #TimeFormat(Now(),"HH:mm")# GMT on #DateFormat(Now(),"dd-mmm-yy")#</p>
				
			<p>Details of recipients are below</p>
			
			<p>To check the status of your communication please <A href="#application.com.login.createStartingTemplateLink(url =internalSite.ProtocolAndDomain,startingTemplate='commcheckdetails',personid=this.creatorID,queryString='frmCommID=#this.CommID#')#">Click Here</A></p>
			
			<cfif arrayLen(this.itemArray) GT individualItemLimit>
				#format().shortTableHTML#
			<cfelse>
				#format().LongTableHTML#
			</cfif>

			</cfoutput>
			</cfsavecontent>
	
			<CF_MAIL 
				to="#Trim(confirmemail)#"
				bcc="#application.Supportemail#"
				from="#request.currentSite.systemEmail#"
				subject="#request.CurrentSite.Title#: Communication #this.CommID# #Replace(this.title,'%','%%','ALL')# Processed"
				type="HTML"
				><cfoutput>
				<!--- <LINK REL="stylesheet" HREF="#request.currentSite.protocolAndDomain##request.currentsite. stylesheet#"> --->
				#c_adminmsg##confirmTextHTML#</cfoutput>
			</CF_MAIL>
		
	</cffunction>			


	<cffunction name = "serialize" output=false>
		<cfreturn serializeJSON(this)>
	</cffunction>


	<cffunction name = "createCommSendResultRecord" access="private" output=false>

		<cfset var create = '' />
		
		<cfquery name="create" datasource="#application.sitedatasource#">
		insert into CommSendResult (commid, created, createdby) 
		values (<cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >,getdate(),<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >)
		select scope_identity() as commSendResultID
		</cfquery>

		<cfreturn create.commSendResultID>
	</cffunction>


	<cffunction name = "save" output=false>

		<cfset var saveqry = '' />
		<cfset var data = serialize()>

		<cfquery name="saveqry" datasource="#application.sitedatasource#">
		update 
			commSendResult
		set	
			result  =  <cf_queryparam value="#data#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			numberSent =  <cf_queryparam value="#this.numberSent#" CFSQLTYPE="CF_SQL_Integer" >,
			numberNotSent =  <cf_queryparam value="#this.numberNotSent#" CFSQLTYPE="CF_SQL_Integer" >,
			test =  <cf_queryparam value="#iif(this.test,1,0)#" CFSQLTYPE="CF_SQL_bit" >,
			OK =  <cf_queryparam value="#iif(this.isOK,1,0)#" CFSQLTYPE="CF_SQL_bit" >,
			completed = getdate()
		where commSendResultID =  <cf_queryparam value="#this.commSendResultID#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>

	</cffunction>


	<cffunction name = "read" output=false>
		<cfargument name="commSendResultID" >

	 	<cfset var readQry = '' />
	 	
		<cfquery name="readQry" datasource="#application.sitedatasource#">
		Select * 
		from
		
			commSendResult
		where commSendResultID =  <cf_queryparam value="#commSendResultID#" CFSQLTYPE="cf_sql_numeric" >
		</cfquery>

		<cfreturn readQry>

	</cffunction>


	<cffunction name = "delete" output=false>

		<cfquery datasource="#application.sitedatasource#">
			delete from commSendResult	where commSendResultID =  <cf_queryparam value="#this.commSendResultID#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>

		<cfreturn >
	
	</cffunction>
	

	<cffunction name = "initialiseWithSerialisedData" output=false>
		<cfargument name="data" >

		<cfset var temp = deserializeJSON (data)>
		<cfset structAppend(this,temp)>

		<cfreturn this>
	</cffunction>


	<!--- WAB 2009/12/14 for use in approveCommunication.cfm
		returns the list of personids successfully sent an email from the resultStructure
	 --->
	<cffunction name="getSuccessfulPersonIDList" output=false>
		
		<cfset var result = "">
		<cfset var i = 0>
	
			<cfloop index = "i" from = "1" to = "#arrayLen(this.itemArray)#" >
				<cfif this.itemArray[i].OK>
					<cfset result = listappend (result, this.itemArray[i].personid)>
				</cfif>	
			</cfloop>
	
		<cfreturn result>	
		
	</cffunction>

	
	<!--- 
		WAB 2009/06 LID 2361
		A structure is built up by task commsend for each comm being sent
		This function formats it for feeback
		It is called within tskcommsend to send the confirmation email and to provide on screen feedback
		Use formatTskCommSendResultStructure to format the whole result returned by tskcommsend (which is an array of resultstructures for all comms sent)
	 --->
	<cffunction name="getAjaxMessage" output=false>
		<cfargument name="individualItemLimit" default="#variables.defaultIndividualItemLimit#">
		
		<cfset var result = "">
		<cfset var getTables = format(withHeading=false)>
		
		<cfif arrayLen(this.itemArray) GT individualItemLimit>
			<cfset result = getTables.shortTableHTML >
		<cfelse>
			<cfset result = getTables.LongTableHTML >	
		</cfif>
		
		<cfif structKeyExists (this,"debugFile")>
			<cfset result &= '<A href="#this.debugFile#" target="_new">Debug</a>'>
		</cfif>
		
		
		
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="format" output=false>
		<cfargument name="funcVersion" default="1">
	
		<cfset var result = "">
		<cfif funcVersion eq "1">
			<cfset result = formatV1(argumentCollection = arguments)>
		<cfelse>
			<cfset result = formatV2(argumentCollection = arguments)>
		</cfif>
	
		<cfreturn result>
	</cffunction>
	
	
	<!--- Case 426955 reformat table in function formatTskCommSendIndividualResultStructureV1 not to use "colspan" --->
	<cffunction name="formatV1" output=false >
		<cfargument name="withHeading" default="true">
	
		<cfset var result = {PersonIDList_OK = "",okcount = 0,notokcount = 0}>
		<cfset var thisItem = "">
		<cfset var detail = "">
		<cfset var heading = "">
		<cfset var message = "">
		<cfset var footer = "">
		<cfset var table = "">
		
		<cfset Heading = "<table>">
		<cfset Heading &= "<tr><th align='center'>Communication #htmlEditFormat(this.commid)# - #htmlEditFormat(this.title)#</th></tr>"> 
		<cfif this.selectionid is not "">
			<cfset Heading = heading & "<tr><td align='center'>Selection #htmlEditFormat(this.selectionid)# </td></tr>"> 
		</cfif>
		<cfset Heading &= "</table>">
					
		<cfif not this.isOK>
			<cfset Message = "<table><TR><td><font color='red'>#application.com.security.sanitiseHTML(this.ErrorMessage)#</font></td></TR></table>">
		<cfelseif this.partialSend>
			<cfset message = "<table><TR><td>Communication partially sent</td></TR></table>">	
		<cfelse>
			<cfset message = "">	
		</cfif>
					
		<cfset detail = "<table>">
		<cfloop array = #this.ItemArray# index="thisItem">
			<cfif thisItem.OK>
				<cfset result.PersonIDList_OK = listappend(result.PersonIDList_OK,thisItem.personid)>
				<cfset detail = detail & "<tr><td>#htmlEditFormat(thisItem.Name)#</td><td>#htmlEditFormat(thisItem.address)#</td><td>Sent #htmlEditFormat(thisItem.Feedback)#</td></tr>" >
				<cfset result.okcount = result.okcount + 1>
			<cfelse>
				<cfset detail = detail  & "<tr><td>#htmlEditFormat(thisItem.Name)#</td><td>#htmlEditFormat(thisItem.address)#</td><td>Not Sent:#htmlEditFormat(thisItem.Feedback)# </td>" >				
				<cfset result.notokcount = result.notokcount + 1>
			</cfif>
		</cfloop>
		<cfset detail &= "</table>">
	
		<cfset footer = "<table>">
		<cfif result.okcount is 0 and result.notokcount is 0>
			<cfset footer = "<tr><td>No items to be sent</td></tr>">			
		<cfelse>
			<cfif result.okcount is 1>
				<cfset thisItem = "Item">
			<cfelse>
				<cfset thisItem = "Items">
			</cfif>		
			<!--- 2014/10/03	YMA	CORE-686 shouldnt have a table header here. --->
			<cfset footer = "<tr><td>#htmlEditFormat(result.okcount)# #htmlEditFormat(thisItem)# Sent</td></tr>">				
			<cfif result.notokcount is not 0>
				<cfset footer = footer & "<tr><td>#htmlEditFormat(result.notokcount)# Items Not Sent</td></tr>">
			</cfif>	
		</cfif>
		<cfif this.partialSend>
			<cfset footer = footer & "<TR><td>#htmlEditFormat(this.itemsLeft)# Items left to be sent</td></TR>">	
		</cfif>
		<cfset footer &= "</table>">
		
		<cfset table = "<table>">
		<cfif withHeading>
			<cfset table &= "<tr><td>" & heading & "</td></tr>">
		</cfif>
		<cfif message is not "">		
			<cfset table &= "<tr><td>" & message & "</td></tr>">
		</cfif>
		<cfset result.shortTableHTML = table & "<tr><td>" & replaceNocase(footer,"th>","td>","ALL") & "</td></tr></table>">
		<cfset result.LongTableHTML = table & "<tr><td>" & detail & "</td></tr>" & "<tr><td>" & footer & "</td></tr></table>">
						
		<cfset result.totalResult = result.notokcount + result.okcount>
		
		<cfreturn result>
	
	</cffunction>
	

	<cffunction name="formatV2" output=false>
		<cfargument name="tablewidth" default="600">
		<cfargument name="tableClass" default="withBorder">
	
		<cfset var result = structnew()>
		<cfset var PersonList = "">
		<cfset var okcount = 0>
		<cfset var notokcount = 0>
		<cfset var okdetail_html = "">
		<cfset var notOkDetail_html = "">
		<cfset var extenderLimit = 3>
		<cfset var outcome = "">
		<cfset var thisItem = "">
		<cfset var i = "">
	
		<cfsavecontent variable="result.LongTableHTML">
			<cfoutput>
			<TABLE WIDTH="#tablewidth#" CLASS="#tableClass#">
						<cfif this.isOK>
							<cfloop array = #this.ItemArray# index="thisItem">
								<cfset PersonList = listappend(PersonList,thisItem.address)>
								<cfif thisItem.OK>
									<cfset okdetail_HTML = okDetail_html & '<div class="SendResultItem">#htmleditformat(thisItem.Name)# (#htmleditformat(thisItem.address)#) </div>'>
									<cfset okcount = okcount + 1>
								<cfelse>
									<cfset notOkDetail_html = notOkDetail_html & '<div class="SendResultItem">#htmleditformat(thisItem.Name)# (#htmleditformat(thisItem.address)#): #htmleditformat(thisItem.Feedback)#</div>' >
									<cfset notokcount = notokcount + 1>
								</cfif>
							</cfloop>
	
							<cfif (okcount eq 0 and notokcount gt 0) or (notokcount eq 0 and okcount gt 0) or (notokcount + okcount eq 0)>
								<cfif okcount gt 0>
									<cfset outcome = "Sent">
								<cfelse>		
									<cfset outcome = "Failed">
								</cfif>
								<cfif (okcount+notokcount) gt extenderLimit>
									<tr><td>
									<cf_divOpenClose divID="#outcome#" title="phr_sys_comm_#outcome# (#okcount+notokcount#)" version="2" level="1" startCollapsed="false">
										<cfif okcount gt 0>#okDetail_html#<cfelse>#notOkDetail_html#</cfif>
									</cf_divOpenClose>
									</td></tr>
								<cfelseif notokcount + okcount eq 0>
									<tr><th align="center">phr_sys_comm_Failed</th></tr>
									<tr><td>phr_sys_comm_GeneralSendFailureMsg</td></tr>
								<cfelse>
									<tr><th align="center">phr_sys_comm_#outcome#</th></tr>
									<tr><td><cfif okcount gt 0>#okDetail_html#<cfelse>#notOkDetail_html#</cfif></td></tr>
								</cfif>
							<cfelse>
								<tr><th align="center">phr_sys_comm_SendResults</th></tr>
								<tr><td>
									<cf_divOpenClose divID="Sent" title="phr_sys_comm_Sent (#okcount#)" version="2" level="1" startCollapsed="false">
										#okDetail_html#
									</cf_divOpenClose>
								</td></tr>
								<tr><td>
									<cf_divOpenClose divID="Failed" title="phr_sys_comm_Failed (#notokcount#)" version="2" level="1" startCollapsed="false">
										#notOkDetail_html#
									</cf_divOpenClose>
								</td></tr>
							</cfif>
							<cfif this.partialSend>
								<tr>
									<td class="warningblock">phr_sys_comm_CommunicationPartiallySent</td>
								</tr>
							</cfif>
						<cfelse>
								<tr>
									<td class="errorblock">
										<cfif structkeyexists(this,"ERRORMESSAGE")>
										#application.com.security.sanitiseHTML(this.errorMessage)#
										<cfelse>
										phr_sys_comm_GeneralSendFailureMsg
										</cfif>
									</td>
								</tr>
						</cfif>
			</TABLE>
			</cfoutput>
		</cfsavecontent>
	
		<cfsavecontent variable="result.ShortTableHTML">
			<cfoutput>
			<TABLE WIDTH="#tablewidth#" CLASS="#tableClass#">
				<tr>
					<th align="center">phr_sys_comm_SendResults</th>
				</tr>
				<cfif this.isOK and this.partialSend>
					<tr><td class="errorblock">phr_sys_comm_CommunicationPartiallySent</td></tr>
				</cfif>
				<cfif okcount>
					<tr><td>phr_sys_comm_Sent:  #htmleditformat(okcount)# </td></tr>
				</cfif>
				<cfif notokcount>
					<tr><td>phr_sys_comm_Failed:  #htmleditformat(notokcount)# </td></tr>
				</cfif>
			</TABLE>
			</cfoutput>
		</cfsavecontent>
		
		<cfset result.successful = okcount>
		<cfset result.failures = notokcount>
		<cfset result.totalResult = okcount+notokcount>
	
		<cfreturn result>
	
	</cffunction>


</cfcomponent>