<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		messages.cfm
Author:			NJH
Date started:	22-02-2013

Description:	Tool to send out messages

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
05-03-2015			ACPK		FIFTEEN-273 Fixed datepicker disabling days after current date instead of before
2016/01/12			NJH			JIRA BF-204 - filter out broadcasts from the messages to activity streams. Also moved the js into message.js
Possible enhancements:


 --->

<cfparam name="getMessages" default="">
<cfparam name="sortOrder" default="sendDate desc,title_defaultTranslation">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<cfset xmlSource = "">

<cffunction name="postSave" returntype="struct">

	<cfset var updateImageName = "">
	<cfset var result = {isOK=true,message=""}>

	<!--- files uploaded - an image in messages can never be deleted, so we always have an image --->
	<cfif structKeyExists(arguments,"filesUploaded") and arguments.filesUploaded neq "">
		<cfquery name="updateImageName" datasource="#application.siteDataSource#">
			update message set image =  <cf_queryparam value="#arguments.filesUploaded#" CFSQLTYPE="CF_SQL_VARCHAR" > where messageID = <cf_queryParam value="#arguments.messageID#" cfsqltype="cf_sql_integer">
		</cfquery>
	</cfif>

	<cfif structKeyExists(form,"schedule") and form.schedule eq "now" and arguments.sendDate eq "">
		<cfquery name="setSendDate" datasource="#application.siteDataSource#">
			update message set sendDate = getDate(),
				lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
				lastUpdated = getDate(),
				lastUpdatedByPerson = #request.relayCurrentUser.personID#
			where messageID = <cf_queryParam value="#arguments.messageID#" cfsqltype="cf_sql_integer">
		</cfquery>
	</cfif>

	<cfreturn result>
</cffunction>

<cfif structKeyExists(url,"editor")>

	<cf_includeJavascriptOnce template="/javascript/date.js">
	<cf_includeJavascriptOnce template="/communicate/js/message.js">
	<cf_includeCssOnce template="/javascript/lib/jquery/css/themes/smoothness/jquery-ui.css">

	<cf_head>
		<style>
			.ui-autocomplete-loading {
			    background: white url('/images/icons/loading.gif') right center no-repeat;
			}
			#searchImage_lineItemDiv {padding-left:10px;}
		</style>
	</cf_head>

	<cfsavecontent variable="searchImage">
		<cfoutput><img src="/images/icons/search.gif"></cfoutput>
	</cfsavecontent>

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="message" title="Message Editor" readonly="currentRecord.sendDate neq '' and datediff('n',currentRecord.sendDate,request.requestTime) gt 0">
				<field name="messageID" label="phr_message_messageID" description="This records unique ID." control="html"></field>
				<field name="" label="phr_message_title" description="The title of the message" required="true" control="translation" parameters="phraseTextID=title,allPhraseTextIDsOnPage='title,text',maxLength=100"/>
				<field name="" label="phr_message_text" description="The body of the message" required="true" control="translation" parameters="phraseTextID=text,displayAs=TextArea,maxChars=250,shortenURL=true"/>
				<field name="url" label="phr_message_url" description="The url of the message" size="70" validate="url" message = "must be a valid url" onfocus="if (this.value == '') {this.value='http://';}" onBlur="if (this.value == 'http://') {this.value=''} "/>
				<field name="" control="file" label="phr_message_image" acceptType="image" parameters="allowFileDelete=false,defaultImage=true,filePrefix=Thumb_,displayHeight=80,height=80,width=80" description=""></field>
				<cfif application.com.settings.getSetting("campaigns.displayCampaigns")><field name="campaignID" label="phr_Sys_AssociateCampaign" control="select" value="campaignID" display="campaignName" query="func:com.relayCampaigns.GetCampaigns()" nulltext="Phr_Ext_SelectAValue"/></cfif>
				<field name="schedule" label="phr_message_schedule" description="Date/Time to send message" control="html" condition="not(currentRecord.sendDate neq '' and datediff('n',currentRecord.sendDate,request.requestTime) gt 0)">
					<cfsavecontent variable="scheduleInput">
						<cfoutput>
							<label for="scheduleLater" class="radio-inline">
								<input type="radio" id="scheduleLater" name="schedule" value="schedule" checked="currentRecord.sentDate neq ''">Schedule 
							</label> 
							<label for="scheduleNow" class="radio-inline">
              	<input type="radio" id="scheduleNow" name="schedule" value="now"> Send Now
              </label>
						</cfoutput>
					</cfsavecontent>
					#htmlEditFormat(scheduleInput)#
				</field>
				<!--- 2015-03-05	ACPK	FIFTEEN-273 Fixed datepicker disabling days after current date instead of before --->
				<field label="phr_message_senddate" name="sendDate" submitFunction="isDateInFuture" requiredFunction="if ($('scheduleLater')) {return isItemInCheckboxGroupChecked($('scheduleLater'),'schedule')} else {return false}" thisFormName="editorForm" showAsLocalTime = "true" showTime="true" disableToDate="#request.requestTime#" enableRange="true"/>
				<lineItem label="phr_message_sendToEntityType">
					<field class="rightMargin" name="sendToEntityType" label="phr_message_sendToEntityType" validValues="Person,Selection,Usergroup" onchange="toggleEntityNameSearch();"/>
					<field class="searchBox" name="SendToEntityName" condition="not(currentRecord.sendDate neq '' and datediff('n',currentRecord.sendDate,request.requestTime) gt 0)"></field>
				</lineItem>
				<!--- NJH 2016/07/19 - JIRA PROD2016-1444 - use call webservice for tidyness sake  --->
				<field name="SendToEntityID" label="phr_message_sendToEntityID" bindfunction="cfc:relay.webservices.callWebService.callWebService(webServiceName='communicationsWS',methodName='getMessageEntities',sendToEntityType={sendToEntityType},messageID={messageID})" display="entityName" value="entityID" required="true"/>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>
	<cfquery name="getMessages" datasource="#application.siteDataSource#">
		select m.messageID,m.title_defaultTranslation as title,m.text_defaultTranslation as text,m.url,m.sendDate, case when isNull(m.sendDate,getDate()) < getDate() then 1 else 0 end as sent,
			'Message: '+left(m.title_defaultTranslation,10)+'...' as tabTitle,
			m.sendToEntityType,
			case when p.personID is not null then p.firstname +' '+ p.lastname
				when s.selectionID is not null then s.title
				when u.userGroupID is not null then u.name end as sendToEntity
		from message m
			left join person p on m.sendToEntityID = p.personID and m.sendToEntityType = 'Person'
			left join selection s on m.sendToEntityID = s.selectionID and m.sendToEntityType = 'Selection'
			left join usergroup u on m.sendToEntityID = u.userGroupID and m.sendToEntityType = 'UserGroup'
			left join broadcast b on b.messageID = m.messageId
		where m.createdBy=<cf_queryparam value="#request.relayCurrentUser.userGroupID#" cfsqltype="cf_sql_integer">
			and b.broadcastId is null
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>

	<cfset application.com.request.setTopHead(topHeadCfm="commTopHead.cfm")>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="messages"
	keyColumnList="title"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(messageID)##'*comma'##jsStringFormat(tabTitle)##'*comma'/communicate/messages.cfm?editor=yes&hideBackButton=true&messageID=##jsStringFormat(messageID)##'*comma{reuseTab:true*commaiconClass:'communicate'});return false;"
	showTheseColumns="title,text,sendToEntityType,sendToEntity,sendDate,sent"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getMessages#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	booleanFormat="sent"
	hideTheseColumns=""
	FilterSelectFieldList=""
	dateTimeFormat="sendDate"
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	postSaveFunction = #postSave#
	columnTranslationPrefix="phr_report_message_"
>