<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 

WAB 2006-01-02

This was a very early query I built in the days when we sent faxes and emails
I'm sure I could do it better now, but we don't send faxes anymore, so it is rather redundant I have therefore cut it out

wab 2007-03-23 actually turns out that it is needed! in commresultsall and commresulktsAlldownload
I have recreated the query and brought further queries from commresultsall and commresulksAlldownload into one place


<!--- This section does the equivalent of a crosstab query and results in a table which has--->
<!--- a single row for each recipient of a communication with the following fields--->
<!--- PersonID--->
<!--- EmailID    - This is the CommDetailID of the record containing the email details (null if no emails sent)--->
<!--- FaxlID    - This is the CommDetailID of the record containing the fax  details (null if no faxes sent)--->

<cfif not isdefined ("filter_TestInternalExternal")>
	<cfinclude template = "commCheckDetailsStatisticsFilter.cfm" >
</cfif>


<CFQUERY name="truncateTable" datasource="#application.sitedatasource#">
	Truncate table commtemptable
</CFQUERY>

<!---  Create new table--->
<!---  The use of the Max is a bitof a hack!!--->
<!---  since upgrading to SQL I have had to change this a bit
if there is no fax or email then I set the faxid or emailid to 1
Therefore there has to be an item in commdetail with id 1 and wab status 0--->
<CFQUERY name="TmpCrossTab" datasource="#application.sitedatasource#">
	INSERT INTO commtemptable( PersonID, Emailid, Faxid )
	SELECT cd.PersonID AS PERSONID, 
		max(CASE WHEN commTypeid=2 THEN commdetailid   <!--- used to be commformlid --->
				ELSE 1 END) AS Emailid,
		max(CASE WHEN commTypeid=3 THEN commdetailid   <!--- used to be commformlid --->
				ELSE 1 END) AS Faxid
	FROM CommDetail cd
	WHERE cd.CommID=#frmcommid#
	and #Filter_TestInternalExternal#
	GROUP BY cd.PersonID, cd.CommID

</CFQUERY>



 --->
 
 


 <CFINCLUDE template="commCheckDetailsStatisticsFilter.cfm">

 
<cfset tempTableName = "####tempCommResults#request.relayCurrentUser.personid##int(rand()*1000)#" >
<CFQUERY name="CommunicationResults" datasource="#application.sitedatasource#"> 

	SELECT cd.PersonID AS PERSONID, 
		max(CASE WHEN commTypeid=2 THEN commdetailid   <!--- used to be commformlid --->
				ELSE 1 END) AS Emailid,
		max(CASE WHEN commTypeid=3 THEN commdetailid   <!--- used to be commformlid --->
				ELSE 1 END) AS Faxid
	INTO #tempTableName#
	FROM CommDetail cd
	WHERE cd.CommID =  <cf_queryparam value="#frmcommid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and #filter.TestInternalExternal#
	GROUP BY cd.PersonID, cd.CommID

	SELECT P.FirstName, 
	P.LastName, 
P.FirstName + ' ' + P.LastName as fullname, 
	P.PersonID, 
	p.email,
	p.locationid,
	p.sitename,p.isocode,
	E.CommID AS Commid, 
	E.commStatusId AS emailstatus, 
	F.commStatusId AS faxstatus, 
	E.CommTypeId as byemail, 
	cds1.name AS emailfeedback, 
	E.Feedback, 
	F.CommTypeId as byfax, 
	cds2.name AS faxfeedback, 
	F.Feedback,
	case when E.CommTypeId = 2 then 'Email' when F.CommTypeId = 3 then 'Fax' else '' end as sentBy, -- NJH 2008/07/09 Bug Fix T-10 Issue 682
	isNull(cds1.name,'') + '<BR>' + isNull(cds2.name,'') as DeliverResult, -- NJH 2008/07/09 Bug Fix T-10 Issue 682
	CASE WHEN (E.commStatusId > 10 OR F.commStatusId > 10) THEN 'Received OK' WHEN (E.commStatusId > 0 OR F.commStatusId > 0) THEN 'Sent OK' 
			ELSE 'Not Received' END  AS result		
	FROM 	#tempTableName# AS T 
	INNER JOIN vpeople p ON T.personid = P.PersonID 
		#(replaceNoCase(filter.country,"cd.","p.","ALL"))#
	left JOIN CommDetail AS E ON T.emailid = E.CommDetailID 
	left JOIN CommDetail AS F ON T.faxid = F.CommDetailID
	LEFT JOIN commDetailStatus AS cds1 ON E.commStatusId = cds1.commStatusId 	and 	#(replaceNoCase(filter.TestInternalExternal,"cd.","e.","ALL"))# 
	LEFT JOIN commDetailStatus AS cds2 ON F.commStatusId = cds2.commStatusId  and  #(replaceNoCase(filter.TestInternalExternal,"cd.","f.","ALL"))# 


	drop taBLE #tempTableName#
</CFQUERY>


