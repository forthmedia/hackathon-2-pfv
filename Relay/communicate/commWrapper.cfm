<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2011-07-04	NYB		P-LEN024 - generally wrapper - for incorporating pages that contain extensive javascript that wouldn't play well with it's parents code
2013-02-07	WAB		Changed way URL passed (as a variable with ? encoded)
--->
<cfoutput>
<iframe src="#wrappedURL#" width="100%" style="height:900px;" name="ContentsFrame" id="ContentsFrame" frameborder="0" vspace="0" hspace="0" marginwidth="0" marginheight="0" scrolling="no">
	<p>Your browser does not support iframes.</p>
</iframe>
</cfoutput>
