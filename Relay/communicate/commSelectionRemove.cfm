<!--- �Relayware. All Rights Reserved 2014 --->
<!--- CommSelectionRemove.cfm

Author:  WAB

Quick template to remove selection from a communication

 --->


<cfset  application.com.communications.removeSelectionFromCommunication(commid = frmCommid,selectionid = frmSelectID)>


<!--- now go to next page 
	if frmNextPage is set then goes there - either as an include or as a URL
--->
<cfif isDefined("frmNextPage") and frmnextpage is not "">
	<cfif findnocase("?",frmNextPage) is 0 and findnocase("http:",frmNextPage) is 0 >
		<cfinclude template="#frmnextpage#">
	<cfelse>
		<cflocation url="#frmnextpage#"addToken="false">
	</cfif>
	
<cfelse>	
	<cfinclude template="commconfirm.cfm">
</cfif>

