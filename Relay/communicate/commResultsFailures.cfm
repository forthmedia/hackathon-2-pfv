<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Displays a table of all failed communications for a particular commID --->
<!--- Written by William Bibby 1998-07-23 --->

<!--- Relies on all the status of all communications being updated correctly by the fax and email processing  ---->


<!--- <CFINCLUDE template=wabcommresultsquery.cfm> --->

<cfif not isdefined ("filter")>
	<cfinclude template = "commCheckDetailsStatisticsFilter.cfm" >
</cfif>


<!---  Combine everything together!--->
<CFQUERY name="CommunicationFailures" datasource="#application.sitedatasource#">

SELECT Location.LocationID, 
Location.SiteName, 
Location.Fax, 
Location.Telephone, 
Person.FirstName, 
Person.LastName, 
Person.PersonID, 
Person.OfficePhone, 
Person.Email, 
E.commstatusid AS emailreceived, 
F.commstatusid AS faxreceived, 
E.CommFormLID, 
E_lookup.name as emailfeedback, 
E.Feedback, 
F_lookup.name as faxfeedback, 
F.Feedback
FROM Location INNER JOIN (((((commtemptable AS T INNER JOIN Person ON T.PersonID = Person.PersonID) INNER JOIN CommDetail AS E ON T.EmailID = E.CommDetailID) INNER JOIN CommDetail AS F ON T.FaxID = F.CommDetailID) LEFT JOIN commdetailstatus AS E_lookup ON E.commstatusid = E_lookup.commstatusid) LEFT JOIN commdetailstatus AS F_lookup ON F.commstatusid = F_lookup.commstatusid) ON Location.LocationID = Person.LocationID
WHERE (((E.commstatusid) Is Null Or (E.commstatusid)<=0) 
AND ((F.commstatusid) Is Null Or (F.commstatusid)<=0))
and #(replaceNoCase(Filter.TestInternalExternal,"cd.","e.","ALL"))#) 
and #(replaceNoCase(Filter.TestInternalExternal,"cd.","f.","ALL"))#) 

</CFQUERY>




<CFQUERY name="CountofPeople" datasource="#application.sitedatasource#">

SELECT CommDetail.PersonID
FROM CommDetail
<cfif not includeTestCommDetails>WHERE Test = 0</cfif>
GROUP BY CommDetail.CommID, CommDetail.PersonID
HAVING CommDetail.CommID =  <cf_queryparam value="#frmcommID#" CFSQLTYPE="CF_SQL_INTEGER" > 

</CFQUERY>

 

<cf_head>
   <cf_title>Failed Communications</cf_title>
</cf_head>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR class="SubMenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="SubMenu">Failed Communications for Comm <CFOUTPUT>#htmleditformat(URL.frmCommID)#</CFOUTPUT></TD>
		<td align="right" class="SubMenu"><a href="javascript:history.back()" class="SubMenu">Back</a></td>
	</TR>
</TABLE>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD COLSPAN="5">Number of People in Communication   <CFOUTPUT>#CountOfPeople.recordcount#  </CFOUTPUT>
					<cfif not includeTestCommDetails><BR>(Only includes items sent externally)</cfif>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN="5">Number of Failed Communications   <CFOUTPUT>#CommunicationFailures.recordcount#  </CFOUTPUT>
		</TD>
	</TR>

	<TR>
		<Th>IDs</Th>
		<Th>Recipient</Th>
		<Th>Fax/Email/Phone</Th>
		<Th>Result</Th>
	</TR>





<CFOUTPUT query="CommunicationFailures">
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>

	
	<TD>Pers: #htmleditformat(PersonID)#  <BR>
 	          Loc: #htmleditformat(LocationID)#  </TD>
	<TD>#htmleditformat(FirstName)# #htmleditformat(LastName)#<BR>
	         #htmleditformat(SiteName)#</TD>

	<TD>Fax: #htmleditformat(Fax)#<BR>
	         Email: #htmleditformat(Email)#<BR>
			 Phone: #htmleditformat(OfficePhone)#</TD>
	<TD>#htmleditformat(FaxFeedback)# <BR>
	          #htmleditformat(EmailFeedback)#</TD>


</TR>
</CFOUTPUT>

</TABLE>






