<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
approvecommunication.cfm


Author WAB

2004-07-28

Used to set approval and disapproval for a communication

If gatekeepers are being used then sends emails to approvers when all the gatekeepers have approved
Also sends an email to the owner at that time



requires 
frmCommID
frmAction = Approve / Disapprove



WAB 2005-06-20   mods so that when a communication is disapproved it is removed from the schedule 
			and cannot be scheduled for a time in the past

WAB  2005-07-18	the email telling the gatekeepers that the communication had been dispproved was giving the wrong name of the disapprover!			
WAB  2005-07-18	while in the code I have added a requirement for a disapprover to give a reason  (nasty hack I'm afraid)
2009/?		Added use of CF_MAIL
WAB 2009/12/14   Discovered while investigating LID 2845 Fixed problem introduced by change in format of structure being returned by tskcommsend
NYB 2012-01-13	Case#425115 added changes around frmAction "disapprove", writes foreign chars now and populates form with current value - if exists
WAB 2013-01		Converted to use communication.cfc object	
NJH/WAB 2013/02/19 Combining of hours and minutes into approvalDate and conversion to server time now handled in applicationMain.cfc
--->

<CFPARAM name="frmapproverid" default="#request.relayCurrentUser.personid#">
<CFPARAM name="frmaction" >
<CFPARAM name="message" default="">
<CFPARAM name="useWrapper" default="false">
<CFPARAM name="frmnextpage" default="commconfirm">

<cfset approvalSettings = application.com.settings.getSetting("communications.approvals")>
<cfset communication = application.com.communications.getCommunication(commid =frmcommid)>

<cfif frmAction is "approve">
	<!--- Need to know whether the status changes from not approved to approved during this process, so get current status 
		Approval rules dealt with in the cfc - may need to be approved by more than one gatekeeper eg
	--->
	<cfset approvalDetails = communication.getApprovalDetails()>
	<cfset initialIsApproved = approvalDetails.isApproved>

</cfif>


<cfif frmAction is "disapprove" and not isDefined("frmReason")>
	<!--- WAB 2005-07-18 hacked in a field to get the reason for the disapproval
		Sorry - I know that it is very nasty practice to put things like this in the middle of updaet code!
	 --->
	<cfoutput>
		
	<cfquery name="getCurrentDisapprovalDetails" datasource="#application.siteDataSource#">
		select comment from 
		entityApproval e 
		inner join schematable s with(nolock) on e.entitytypeid=s.entitytypeid and s.entityName='Communication'
		where e.entityid=<cfqueryparam cfsqltype="cf_sql_integer" value="#frmCommID#">
		and e.approvalStatus=<cfqueryparam cfsqltype="cf_sql_integer" value="-1">
	</cfquery>
	<cfset CurrentReason = "">
	<cfif getCurrentDisapprovalDetails.recordcount gt 0>
		<cfset CurrentReason = getCurrentDisapprovalDetails.comment>
	</cfif>
		
	<form method="post" name="disapproveReasonForm" id="disapproveReasonForm">

		<cf_relayFormDisplay align="center">
			<CF_INPUT Type = "hidden" name = "frmAction" value = "#frmAction#">
			<CF_INPUT Type = "hidden" name = "frmCommID" value = "#frmCommID#">
			<CF_INPUT Type = "hidden" name = "frmCallingTemplate" value = "#frmCallingTemplate#"> <!--- LID 7888 NJH - a bit of a hack, but added two form variables... either frmSendAction or frmAction should exist, but not both --->
			<CF_INPUT Type = "hidden" name = "frmSendAction" value = "#frmAction#">
			<cfif isdefined("frmNextPage")>	<CF_INPUT Type = "hidden" name = "frmNextPage" value = "#frmNextPage#"></cfif>
			<tr>
				<td align="center" colspan=2>
					phr_sys_comm_StateHoldReason
				</td>
			</tr>
			<tr>
				<td align="center" colspan=2>
					<TextArea  name = "frmReason" cols = "40" rows = "4">#CurrentReason#</TEXTAREA>
				</td>
			</tr>
			<tr>
				<td align="center" colspan=2>
					<input type ="submit" value="continue">
				</td>
			</tr>
			</cf_relayFormDisplay>
	</form>

	
	<script type="text/javascript">
     	var reasonForm = document.getElementById("disapproveReasonForm");
	    <cfif useWrapper>window.parent.scrollTo(0,0);</cfif>
     	reasonForm.frmReason.focus();		
	</script>
	
	
	</cfoutput>

</cfif>

<cfif not (frmAction is "disapprove" and not isDefined("frmReason"))>
		<!--- set approved or disapproved in the entityApproval Table --->				
				<cfquery name="recordApproval" datasource="#application.siteDataSource#">
				-- update existing
				update entityApproval 
				set 
					approvalStatus = <cfif frmAction is "approve">1<cfelseif frmAction is "disapprove">-1<cfelse>0</cfif>, 
					approvalStatusLastUpdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					comment = <cfif isdefined("frmREASON")>N'#frmREASON#'<cfelse>''</cfif>
					<cfif isDefined("frmapprovedReleaseDate")>
					, approvedReleaseDate =  <cf_queryparam value="#frmapprovedReleaseDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
					</cfif>
				where 
					entityid =  <cf_queryparam value="#frmcommid#" CFSQLTYPE="CF_SQL_INTEGER" >  
					and entityTypeID =  <cf_queryparam value="#application.entityTypeID['communication']#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and personid  in ( <cf_queryparam value="#frmapproverid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )	
				
				
					
				-- insert new 
				insert into entityApproval
				(entityid,entityTypeID,personid,approvalStatus ,comment,approvalStatusLastUpdated  , approvedReleaseDate,created, createdby,lastupdated, lastupdatedby)
				select
					<cf_queryparam value="#frmcommid#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#application.entityTypeID['communication']#" CFSQLTYPE="CF_SQL_INTEGER" >,
					p.personid,
					<cfif frmAction is "approve">1<cfelseif frmAction is "disapprove">-1<cfelse>0</cfif>,
					<cfif isdefined("frmREASON")><cf_queryparam value="#frmReason#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse>''</cfif>,
					<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cfif isdefined("frmapprovedReleaseDate_temp")>#frmapprovedReleaseDate_temp#<cfelse>null</cfif>,
					<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,  
					<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >
				
				from 
					person  p
						left join 
					entityApproval eA  on p.personid = ea.personid and ea.entityid =  <cf_queryparam value="#frmcommid#" CFSQLTYPE="CF_SQL_INTEGER" >  and ea.entityTypeID =  <cf_queryparam value="#application.entityTypeID['communication']#" CFSQLTYPE="CF_SQL_INTEGER" > 
					
					where eA.personid is null
					and p.personid  in ( <cf_queryparam value="#frmapproverid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )	
				
				
				</cfquery>	
</cfif>

<cfif frmAction is "approve">
	<!--- Send a copy of the communication to the approver --->

				<cfset communication.send (
					personIDs = #frmApproverID#,
					test = "true",
					resend = "true",	
					showprocessingcode = false,
					SubjectPrefix = "As Approved: ",
					showLinkBackToCommunication = false,
					footerText = ""
					)>

	
	

	<cfset approvaldetails = communication.getApprovalDetails()>
	<cfset finalIsApproved = approvaldetails.isApproved>


	<cfif not initialIsApproved and finalIsApproved>  <!--- ie if approval status has changed --->

		<cfif approvalDetails.approvalProcessType is "gatekeeper">

			<!--- Need to schedule the communication to be sent out  
				internal selections get it x hours before external selections
			--->
			
			<!--- don't schedule for anytime in the past --->
			<cfset earliestDateToSend = max(approvaldetails.ApprovedSenddate,now())>			

			
			<cfset externalSendDate = earliestDateToSend + approvalSettings.Gatekeeper.Preview.TimeInHours/24>
			<cfset communication.schedule	(date= externalSendDate, selectLID = application.com.communications.constants.ExternalSelectLID)>

			<!--- WAB/ NH  2009/01/30 Bug Fix All Sites Issue 1710 - added checks for existence of all the variables --->
			<cfif structKeyExists(approvalSettings.Gatekeeper,"Preview")>
				<cfif structKeyExists(approvalSettings.Gatekeeper.preview,"SelectLIDs") and approvalSettings.Gatekeeper.Preview.SelectLIDs is not "">
					<!--- schedule particular commSelectLIDs  (eg approvers and/or internals) as defined in INI file --->
					<cfset communication.schedule	(date=earliestDateToSend, selectLID = "#approvalSettings.Gatekeeper.Preview.SelectLIDs#")>
				</cfif>
				
				<cfif structKeyExists(approvalSettings.Gatekeeper.preview,"SelectIDs") and approvalSettings.Gatekeeper.Preview.SelectIDs is not "">
			<!--- schedule particular SelectionIDs as defined in INI file 
				needs to be done after scheduling the external part of the comm, incase the selectionid defined is an external selection
			--->
					<cfset communication.schedule	(date=earliestDateToSend, selectionID = approvalSettings.Gatekeeper.Preview.SelectIDs)>
				</cfif>		

			</cfif>

			
		<!--- Need to send copy of the email to anyone in the approval selections --->
		<cfif communication.hasApprovalSelections() is 1>
		

			<cfset sendResult = communication.send (
				CommSelectLID = application.com.communications.constants.approverSelectLID,
				resend = true,
				test = true,
				showprocessingcode = false,
				SubjectPrefix = "For Final Approval: ",
				showLinkBackToCommunication = true,
				footerText = "This email has been approved by the Gatekeeper(s).<BR>It is due to be sent after #approvaldetails.ApprovedSenddate#  <BR>Please respond immediately if you have any concerns<BR>You can prevent this communication being sent by following the link above and clicking on 'I Disapprove'")
				>

				
				<!--- NEED TO  record that item has been sent for approval --->
				<!--- WAB 2009/12/14 
					problem here subsequent to changes to tskcommsend in June 2009 where structure of the results file had changed
					needed to add .comms to the reference to the result structure 
					and also re-create the list of successful personid, which had been taken out of the result structure by these changes
				--->
				<cfset sentToPersonIDs = sendResult.getSuccessfulPersonIDList()>


				<cfif sentToPersonIDs is not "">
					<cfset sentToArray = sendResult.itemArray>
					<cfset application.com.entityApproval.setApprovalRequested(entityid =#frmcommid#, entityType = 'Communication',PersonIDList = sentToPersonIDs ) >

					<!---  If the current user is not the owner, then send the owner an email --->
					<!--- TBD what is that TRUE --->
					<cfif true or communication.createdby is not request.relayCurrentUser.usergroupid>
						<cfquery name="getEmail" datasource="#application.siteDataSource#">
						select email, firstname + ' ' + lastname from person p inner join usergroup ug on p.personid = ug.personid where ug.usergroupid =  <cf_queryparam value="#communication.createdby#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>					
	
						<CF_MAIL TO=#getEmail.EMail#
						 FROM=#request.currentSite.systemEmail#
						   SUBJECT="Communication #frmCommid# (#communication.title#) Sent to Approvers"
						>
							<cfoutput>
								This communication has been sent to the following people for final approval
								<cfloop index = I from = 1 to = #arraylen(sentToArray)#>
									<cfset thisItem = sentToArray[i]>#thisItem.address# 
									<cfif not thisItem.OK >(#thisItem.feedback#)</cfif>
								</cfloop>
							</cfoutput>				   
						</CF_MAIL>
					
					</cfif>
			
					<cfset 	message  = "Approved by Gatekeepers. Sent to Approvers for Final Approval">
				<cfelse>
						<cfset 	message  = "Approved by Gatekeepers. No Approvers to send to">
				</cfif>	
		<cfelse>
				<cfset 	message  = "Approved by Gatekeepers. No Approvers to send to">
		</cfif>
				
		</cfif>
	
	<cfelse>

	</cfif>
</cfif>

<cfif not (frmAction is "disapprove" and not isDefined("frmReason"))>
	<cfif frmAction is "disapprove">
		<cfset approvaldetails = communication.getApprovalDetails()>
	
		<!--- unschedule the whole communication --->
		<cfset communication.unschedule	()>			
		
		<!--- Send an email to gatekeeper(s) to tell them that communication has been disapproved --->
		<cfset emailList = "">
		<cfif structkeyexists(approvaldetails,"gatekeeperquery")>
			<cfloop query=approvaldetails.gatekeeperquery>
				<cfif listFindNocase (emailList,email) is 0 >
					<cfset emailList = listappend(emailList, email)>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset emailList = communication.CREATOREMAIL>
		</cfif>
	
		<CF_MAIL TO=#EMailLIst#
			 FROM=#request.currentSite.systemEmail#
			  CC="#application.copyAlleMailsToThisAddress#"
			   SUBJECT="Communication #frmCommid# (#communication.title#) Put on hold"
			   type = "html">
			<cfoutput>This communication has been put on hold by #application.com.commonqueries.getFullpersondetails(request.relaycurrentuser.personid).fullname#  <BR>
			Reason <BR>
			#replaceNoCase(frmReason,chr(13)&chr(10),"<BR>","ALL")#
			</cfoutput>
		</CF_MAIL>
	
	</cfif>	
	
	<cfif isdefined("frmnextpage") and len(frmnextpage) gt 0>
		<cfinclude template="#frmnextpage#">		
	</cfif>
</cfif>

	


