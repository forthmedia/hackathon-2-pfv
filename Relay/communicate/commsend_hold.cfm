<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<CFIF NOT #checkPermission.Level2# GT 0>
	<CFSET message="You are not authorised to use this function">
		<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>


<CFPARAM NAME="FreezeDate" DEFAULT=#Now()#>

<!--- the application .cfm checks for communication privleges but
		we also need to check for select privledges
		we can rerun the qrycheckperms but NOTE we have now lost
		the application .cfm query variables.
--->
<CFSET securitylist = "SelectTask:read">
<cfinclude template="/templates/qrycheckperms.cfm">

<CFIF NOT IsDate(FORM.frmSendDate)>
	<CFSET message = "That is not a valid date please select another.">
	<cfinclude template="#thisDir#/commMenu.cfm">	
	<CF_ABORT>
</CFIF>

<!--- This compares Date submitted from browser with Now() --->
<!--- <CFIF #DateCompare(CreateODBCDate(FORM.frmSendDate), CreateODBCDate(Now()))# IS -1>
	<CFSET message = "The date selected cannot be older than today please select another.">
	<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF> --->

 <!--- 2012-07-25 PPB P-SMA001 commented out
<CFIF NOT IsDefined("Variables.CountryList")>
	<!--- get the user's countries --->
	<cfinclude template="/templates/qrygetcountries.cfm">
	<!--- qrygetcountries creates a variable CountryList --->
</CFIF>
--->
<CFQUERY NAME="checkSelection" DATASOURCE="#application.SiteDataSource#">
	SELECT CommID, CommFormLID
	  FROM Communication 
	 WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	 AND LastUpdated =  <cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
</CFQUERY>

<CFIF #checkSelection.Recordcount# IS 0>
	<CFSET message = "The communication record has been changed by another user since you last downloaded it.  Please begin again.">
	<cfinclude template="#thisDir#/commMenu.cfm">	
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="updateCommunication" DATASOURCE="#application.SiteDataSource#">
	UPDATE Communication SET SendDate = #CreateODBCDateTime(frmSendDate)#
	WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	  AND LastUpdated =  <cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
</CFQUERY>


<!--- ensure that all selections are owned by the user --->

<CFQUERY NAME="getCommSels" DATASOURCE="#application.SiteDataSource#">
		SELECT cs.SelectionID,
			   c.CommFormLID, 
			   c.SendDate,
			   c.LastUpdated
		  FROM Communication AS c, CommSelection AS cs,
		       Selection AS s
		 WHERE c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		   AND cs.CommID = c.CommID
	       AND c.sent = 0
		  AND cs.CommSelectLID = 0 <!--- the selection to send rather than the feedback selections --->
<!--- added selection to this for sharing --->		  
		  AND cs.SelectionID = s.SelectionID
		  AND s.CreatedBy <> #request.relayCurrentUser.usergroupid#
</CFQUERY>



<CFIF getCommSels.RecordCount IS NOT 0>
	<CFSET message="The communication could not be sent since there are selections associated to this communication which you are not authorised to access.">
	<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>


<!--- get communication info, for safetly only
		allow selections which are owned by the user though, if there 
		are any un-owned selections, they should have been caught above --->
<CFQUERY NAME="getCommInfo" DATASOURCE="#application.SiteDataSource#">
		SELECT c.Title,
			   c.Description,
			   cs.SelectionID,
			   c.CommFormLID, 
			   c.SendDate,
			   c.LastUpdated
		  FROM Communication AS c, CommSelection AS cs,
		       Selection AS s
		 WHERE c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		   AND cs.CommID = c.CommID
	       AND c.sent = 0
		  AND cs.CommSelectLID = 0 <!--- the selection to send rather than the feedback selections --->
<!--- added selection to this for sharing --->		  
		  AND cs.SelectionID = s.SelectionID
		  AND s.CreatedBy = #request.relayCurrentUser.usergroupid#
</CFQUERY>


<CFIF getCommInfo.RecordCount IS 0>
	<CFSET message="The communication could not be sent since you are not authorised to send a communication to the chosen selection.">
	<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>
</CFIF>


<!--- we assume that LookupID 4 is download
		later in this template
		but if this is a download, then
		there will not be any records
		in CommFile	   
--->

<CFIF getCommInfo.CommFormLID IS 5>
	<!--- both is 5 --->
	<CFSET comtypelst = "2,3">
<CFELSE>
	<!--- will be 2 email, 3 fax or 4 download
		  or 6,7 for either but prefer

		    --->
	<CFSET comtypelst = getCommInfo.CommFormLID>
</CFIF>		


<CFQUERY NAME="getCommForm" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT ll.LookupID, ll.ItemText
	FROM LookupList AS ll, Communication AS c
	WHERE ll.LookupID = c.CommFormLID
	AND c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	ORDER BY ll.ItemText
</CFQUERY>


<CFIF #getCommInfo.RecordCount# IS NOT 0>
<!--- =====================
	 qryCommCounts.cfm
	 ======================
	required inputs:
		a list of selectionIDs: Variables.selectIDlist
	 outputs (if record count is > 0):
	 	SelectTitleList (a comma separated list of all comm titles)
		TagPeople
		FeedbackCounts
--->
	<!--- we know that the user is the owner of these selections
	     from getCommInfo--->
	<CFSET selectIDlist = ValueList(getCommInfo.SelectionID)>
	<CFSET thisCommFormLID = getCommInfo.CommFormLID>

	<cfinclude template="#thisDir#/qryCommCounts.cfm">

<CFTRANSACTION>	
		<!--- can't Insert into with a select distinct if
				the table being inserted into has a TEXT or IMAGE datatype
				so get the list of people, then insert per formID

				1998-11-04 LW removed CFLOOP here to get rid of the Insert with Distinct
				--->

	 	<CFQUERY NAME="INSgetEntities" DATASOURCE="#application.SiteDataSource#">
			SELECT DISTINCT
			st.EntityID
			FROM SelectionTag AS st, Selection AS s,
				Person AS p, Location AS l
<!--- 			,	Rights AS r,
				RightsGroup AS rg --->
			WHERE s.SelectionID  IN ( <cf_queryparam value="#ValueList(getCommInfo.SelectionID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			  AND s.TableName = 'Person'
			  AND s.SelectionID = st.SelectionID
			  AND st.Status > 0
			  AND st.EntityID = p.PersonID
			  AND p.LocationID = l.LocationID
			  <!--- 2012-07-25 PPB P-SMA001 commented out
			  AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			   --->
			  #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
			  
<!---  check the countries via qryCheckCountries.cfm since need it in qryCommcounts.cfm

			  AND l.CountryID = r.CountryID
			  AND r.Permission > 0 <!--- must have at least read Permission --->
			  AND r.SecurityTypeID = (SELECT SecurityTypeID
			  					   FROM SecurityType 
								  WHERE ShortName = 'RecordTask')
			  AND r.UserGroupID = rg.UserGroupID
			  AND rg.PersonID = #request.relayCurrentUser.personid# --->
			  AND p.Active <> 0
		</CFQUERY>

		<CFIF INSgetEntities.RecordCount IS NOT 0 AND ListLen(Variables.comtypelst) IS NOT 0>
				<!--- if " 5 - always both" then each person 
					 should be added twice, once with 3, once with 2 (which is 
					 already taken into account in comtyplst
					 otherwise user 6 or 7 for "both but prefer"
					 tskCommSend will need to decide who gets email and
					 who gets fax					 --->
		
				<!--- each commFormLID will have its own set of Individuals
						which will be achieved by the cartesian product since
						there is no join between Person and LookupList --->
					
				 	<CFQUERY NAME="INSgetSelectionInfo" DATASOURCE="#application.SiteDataSource#">
						INSERT INTO CommDetail (CommID, PersonID, CommFormLID, Feedback, ContactDetails)
						SELECT 
						<cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						p.PersonID,
						l.LookupID,
						'',
						''
						FROM Person AS p, LookupList AS l
						WHERE p.PersonID  IN ( <cf_queryparam value="#INSgetEntities.EntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						AND l.LookupID  IN ( <cf_queryparam value="#Variables.comtypelst#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</CFQUERY>
		</CFIF>					
	

	<CFIF getCommInfo.CommFormLID IS NOT 4>
		<!--- make appropriate selection of people that can't be reached
			 but only if not a downlad --->		

		<!--- the below is also in commconfirm.cfm --->
		<CFIF #ListFind("5,6,7",getCommInfo.CommFormLID)# IS NOT 0>
			<!--- can't be reached by fax or email --->
			<CFSET TagName = " - invalid fax & email">
		<CFELSEIF getCommInfo.CommFormLID IS 2>
			<!--- can't be reached by fax --->
			<CFSET TagName = " - invalid email">
		<CFELSEIF getCommInfo.CommFormLID IS 3>
			<!--- can't be reached by fax --->
			<CFSET TagName = " - invalid fax">	
		</CFIF>
		
	 	<CFQUERY NAME="newSelectionInfo" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO Selection (Title, RecordCount, Description, TableName, CreatedBy, Created, LastUpdatedBy, LastUpdated )
			SELECT c.Title + <cf_queryparam value="#Variables.TagName#" CFSQLTYPE="CF_SQL_VARCHAR" >, <!--- couldn't get {fn Concat to work} + will add in Access --->
				   0,
				   'People who would not be receiving the communication',
				   'Person',
				   <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				   <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				   <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				   <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			FROM Communication AS c
			WHERE c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	
	 	<CFQUERY NAME="getNewSelectionInfo" DATASOURCE="#application.SiteDataSource#">
			SELECT Max(SelectionID) AS newSelectionID
			FROM Selection
			WHERE CreatedBy = #request.relayCurrentUser.usergroupid#
			AND Created =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			AND LastUpdatedBy = #request.relayCurrentUser.usergroupid#
			AND LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFQUERY>

	 	<CFQUERY NAME="newCommSelection" DATASOURCE="#application.SiteDataSource#">		
			INSERT INTO CommSelection(CommID, SelectionID, CommSelectLID)
			VALUES (<cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#getNewSelectionInfo.newSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,10)
		</CFQUERY>
		
		<CFQUERY NAME="insSelectionTag" DATASOURCE="#application.SiteDataSource#">				
			INSERT INTO SelectionTag (SelectionID, EntityID, Status, CreatedBy, Created, LastUpdatedBy, LastUpdated)
			SELECT <cf_queryparam value="#getNewSelectionInfo.newSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				  cd.PersonID,
				  1, <!--- checked --->
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				   <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				   <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				   <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			FROM CommDetail AS cd,
			<CFIF #ListFind("3,5,6,7",getCommInfo.CommFormLID)# IS NOT 0>
				<!--- we don't need to check countries since
					 we just did to populate the CommDetail above --->
				Location AS l,
			</CFIF>
			Person AS p
			WHERE cd.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFIF getCommInfo.CommFormLID IS 5>
				<!--- if both always than only want the person once --->
				AND cd.CommFormLID = 2  <!--- could be 3, wouldn't matter as long as one --->
			</CFIF>
			AND cd.PersonID = p.PersonID
			<CFIF #ListFind("2,5,6,7",getCommInfo.CommFormLID)# IS NOT 0>
				<!--- if email only or both then 
					select only bad emails --->
				AND (p.email = '' OR p.email IS null OR p.EmailStatus >=  <cf_queryparam value="#application.com.settings.getSetting("communication.limits.numberOfFailedeEmailsUntilStopSending")#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</CFIF>
			<CFIF #ListFind("3,5,6,7",getCommInfo.CommFormLID)# IS NOT 0>
				<!--- if fax only or both then 
					select only bad faxes --->
				AND l.LocationID = p.LocationID
		        AND (l.Fax = '' OR l.Fax IS null OR l.FaxStatus >=  <cf_queryparam value="#Variables.NumFaxStatus#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</CFIF>
		</CFQUERY>

	</CFIF>


</CFTRANSACTION>			


</CFIF>




<cf_head>
<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>
</cf_head>



<cfparam name="frmHideTopHead" default="false">
<cfif not frmHideTopHead>
	<cfinclude template="#thisDir#/commtophead.cfm">
</cfif>

<CENTER><TABLE BORDER="0"><TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">

<CENTER>
<P><BR><BR>


<CFIF #getCommInfo.RecordCount# IS NOT 0>

<P>The following communication has been queued for processing.

<P><CENTER><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="3" CELLSPACING="0" WIDTH="320">
		
		<TR><TD COLSPAN="3" ALIGN="CENTER" BGCOLOR="#000099"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B>COMMUNICATION SUMMARY</B></FONT></TD></TR>
		
		<CFSET DefaultSend = #DateAdd("d",1,Now())#>
		<FORM ACTION="" METHOD="POST">
 		<TR>
		<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			<B>Date to send:</B>
			</FONT></TD>
			</TR>
		<TR><TD WIDTH=15><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			<CFOUTPUT>#DateFormat(getCommInfo.SendDate,"dd-mmm-yy")#</CFOUTPUT>				</FONT></TD>
		</TR>
		</FORM>
		<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=280 HEIGHT=3 ALT="" BORDER="0"><BR></CFOUTPUT></TD></TR>							
		<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<B>Communication:</B>		
			</FONT></TD>
		</TR>
	
		<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				Subject:
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<CFOUTPUT>#htmleditformat(getCommInfo.Title)#</CFOUTPUT>
			</FONT></TD>
		</TR>
		<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				Objective:
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<CFOUTPUT>#htmleditformat(getCommInfo.Description)#</CFOUTPUT>
			</FONT></TD>
		</TR>					
		<CFIF #getCommInfo.CommFormLID# IS NOT 4>
			<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					&nbsp;
				</FONT></TD>
				<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					Via:
				</FONT></TD>
			<!--- not "Download"
				add a space after each comma in the list of 
				how the communication will be sent --->

				<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					<CFOUTPUT>#htmleditformat(getCommForm.ItemText)#</CFOUTPUT>				
<!--- 					<CFOUTPUT>#Replace(ValueList(getCommForm.ItemText),",",", ","ALL")#</CFOUTPUT>				 --->
				</FONT></TD>
			</TR>					
			
		</CFIF>
		<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=280 HEIGHT=3 ALT="" BORDER="0"><BR></CFOUTPUT></TD></TR>									
		<CFIF #Variables.SelectTitleList# IS NOT "">
			<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>		
				<B>Selection:</B>
				</FONT></TD>
			</TR>
			<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
				<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>		
					<CFOUTPUT>#htmleditformat(Variables.SelectTitleList)#
					<!--- tagPeople always exists --->
					
					<P>A total of #htmleditformat(Variables.TagPeopleCount)# individual(s) are tagged
					<BR><!--- feedback counts depends on type of communciation being sent --->
					#htmleditformat(Variables.FeedbackCounts)#
<!--- 						<BR>#getSelectionInfo.emailPeople# Individual(s) with email addresses
						<BR>#getSelectionInfo.faxPeople# Individual(s) with fax numbers
						<BR>#getSelectionInfo.bothPeople# Individual(s) with both --->
					</UL>
					</CFOUTPUT>
		
				<BR></FONT></TD>
			</TR>
		<CFELSE>
			<TR><TD COLSPAN="3" ALIGN="CENTER"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2><P>A related selection could not be found.<BR></FONT></TD></TR>		
		</CFIF>		
	
	</TABLE></CENTER>

	<CFIF getCommInfo.CommFormLID IS NOT 4>
	<P><CENTER>
	<TABLE BORDER=0 WIDTH="90%">
	<TR><TD VALIGN="BASELINE"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
		<P>While it is not possible to add/remove individuals from the communication
		recipient list, you
		may update/add their fax and email details to ensure that a greater percentage of the tagged individuals will be reached.
		A selection containing all the individuals with an invalid email address &/or and invalid fax number
		has been created called &quot;<CFOUTPUT>#htmleditformat(getcommInfo.Title)##HTMLEditFormat(Variables.TagName)#</CFOUTPUT>&quot; to assist you
		in updating the information.
		</FONT></TD></TR>

	</TABLE>
	</CENTER>
	</CFIF>
	
<CFELSE>
	<CENTER>
	<P>The requested communication could not be found.
	</CENTER>
</CFIF>	

</FONT></TD></TR>
</TABLE>
</CENTER>

</FONT>


