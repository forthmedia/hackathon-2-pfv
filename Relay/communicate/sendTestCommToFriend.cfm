<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

sendTestCommToFriend.cfm

Purpose 
	To send a test queued communication to an email address

Amendments

WAB 2004-09-27


Just sets the frmtest parameter and passes control to sendCommToFriend.cfm

--->

<cfset frmTest = true>
<CFPARAM name = "frmSubjectPrefix" default = "Test: ">

<cfinclude template = "sendCommToFriend.cfm">