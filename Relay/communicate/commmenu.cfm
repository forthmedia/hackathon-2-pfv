<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

amendment history 

	2000-08-08	DJH		Added test to allow to use old qrycommcounts, and old commsend...
	2001-10-28	CPS		Changes following new attributes on commdetail record, namely commtypeid, commreasonid and commstatusid
--->


<!--- <CFIF #CGI.REMOTE_ADDR# IS NOT "194.202.210.4" AND #CGI.REMOTE_ADDR# IS NOT "62.188.134.107" AND #CGI.REMOTE_ADDR# IS NOT "193.133.201.83">
	<cfinclude template="/templates/offline0.cfm">
	<CF_ABORT>
</CFIF>  --->


<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="debug" DEFAULT="No">

<cf_head>
<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>
</cf_head>

<cfparam name="frmHideTopHead" default="false">
<cfif not frmHideTopHead>
	<cfinclude template="#thisDir#/commtophead.cfm">
</cfif>

<cfif debug IS "Yes">
	<cfif isDefined("form.qrycomm")>
		<cfset session.qrycomm = "qrycommcountsold.cfm">
		<cfset session.sendcomm = "">
	<cfelse>
		<cfset session.qrycomm = "qryCommCounts.cfm">
		<cfset session.sendcomm = "">
	</cfif>
	<cfoutput><form action="#thisDir#/commmenu.cfm"></cfoutput>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			Use Old qryCommcounts? <input type="checkbox" name="qryComm" value="qryCommCountsOld.cfm" onclick="this.form.submit()"> 
		</td>
	</tr>
	</table>		
	</form>
</cfif>


<cfif Len(message)>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			<CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
		</td>
	</tr>
	</table>	
</cfif>



