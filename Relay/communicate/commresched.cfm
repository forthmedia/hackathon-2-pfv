<!--- �Relayware. All Rights Reserved 2014 --->

<CFIF NOT #checkPermission.Level2# GT 0>

	<CFSET message="You are not authorised to use this function">
	<cfinclude template="#thisDir#/commMenu.cfm">
	<CF_ABORT>

</CFIF>


<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">

<CFPARAM NAME="message" DEFAULT=""> 

<CFPARAM NAME="delmessage" DEFAULT=""> <!--- comms successfully deleted --->
<CFPARAM NAME="sentmessage" DEFAULT=""> <!--- comms not deleted since sent --->
<CFPARAM NAME="chgmessage" DEFAULT=""> <!--- comms not deleted since sent --->
<CFPARAM NAME="baddatemessage" DEFAULT=""> <!--- comms send date bad sent --->
<CFPARAM NAME="scheduledmessage" DEFAULT=""> <!--- comms that have been scheduled --->
<CFPARAM NAME="missmessage" DEFAULT=""> <!--- comms that could not be found --->
<CFPARAM NAME="invalidmessage" DEFAULT=""> <!--- not authorised to edit --->




<!--- for thoseToDelete to exist to facilitate checking --->

<CFIF IsDefined("Form.frmCommID")>			
	<CFSET thoseToDelete = Form.frmCommID>
<CFELSE>
	<CFSET thoseToDelete = "0">
</CFIF>

<!--- loop through all possible communications,
	  check that the date is valid
	  they were not to be deleted
	  not already sent
	  lastupdated had not changed
--->	  

<CFLOOP INDEX="thisCommID" LIST="#frmHeldComms#">

	<CFSET thisSendDate = Evaluate("frmSendDate_#thisCommID#")>
	
	
	<CFIF Trim(thisSendDate) IS NOT "" AND ListFind(thoseToDelete,thisCommID) IS 0>
		<!--- this comm was identified in JavaScript as one
			   to schedule --->
		<CFIF IsDate(thisSendDate)>
			<!--- parameter was a valid date so process 
					but only if not one identified to be deleted
			--->		
			<!--- <CFSET thisSendDate = CreateODBCDateTime(thisSendDate)> --->
			<!---- NJH 2013/02/19 - now handled in applicationMain.cfc
			<cfset thisSendDate = application.com.datefunctions.combineDateAndTimeFormFields("frmSendDate_#thisCommID#")>--->
			<CFSET thisLastUpdated = Evaluate("frmLU_#thisCommID#")>
	
			<CFQUERY NAME="chkIfSent" DATASOURCE="#application.SiteDataSource#">
				SELECT c.CommID,
				       c.Title,
					   c.Sent,
					   c.LastUpdated,
					   c.CreatedBy,
					   c.LastUpdatedBy
				  FROM Communication AS c
				 WHERE c.CommID =  <cf_queryparam value="#Val(thisCommID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>

			<CFIF chkIfSent.RecordCount IS 0>
				<!--- comm not found --->
				<CFSET missmessage = missmessage & "<LI>Communication ID: #thisCommID#">
		
			
			<CFELSEIF YesNoFormat(chkIfSent.Sent) IS "Yes">
			<!--- comm already sent --->
				<CFSET sentmessage = sentmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">

			<CFELSEIF YesNoFormat(chkIfSent.Sent) IS "No" AND DateDiff("s",chkIfSent.LastUpdated,thisLastUpdated) IS NOT 0>
				<!--- comm changed by someone else --->
				<CFSET chgmessage = chgmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">

			<CFELSEIF NOT (chkIfSent.createdBy IS request.relayCurrentUser.usergroupid 
					   OR chkIfSent.LastUpdatedBy IS request.relayCurrentUser.usergroupid 
					   OR request.isAdmin)>
 	
				<CFSET invalidmessage = invalidmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">
			
			<CFELSEIF YesNoFormat(chkIfSent.Sent) IS "No">
			<!--- test explicitly for no since, in theory, 
					3rd option is no records returned  --->

				<CFSET scheduledmessage = scheduledmessage & "<LI>#HTMLEditFormat(chkIfSent.Title)#">
						
<!--- 				<CFTRANSACTION> --->
					<CFQUERY NAME="UpdateComm" DATASOURCE="#application.SiteDataSource#">
						UPDATE Communication
						SET SendDate =  <cf_queryparam value="#thisSendDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
							CommStatusID=0,
<!---						LastUpdatedBy=#request.relayCurrentUser.usergroupid#,						
							don't change lastupdatedBy since will affect where the confirmations
							go in tskcommsend.cfm		    --->
							LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
						WHERE CommID =  <cf_queryparam value="#thisCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  
						  AND LastUpdated =  <cf_queryparam value="#thisLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
						  AND CommStatusID > 0 <!--- automatically excludes nulls --->
					</CFQUERY>
<!--- 				</CFTRANSACTION> --->
			</CFIF>		
	

		<CFELSE>
			<!--- date parameter was not valid --->
			<CFSET baddatemessage = baddatemessage & "<LI>#HTMLEditFormat(Evaluate("frmTitle_#thisCommID#"))#">
	
		</CFIF>
	
	
	</CFIF>
</CFLOOP>	


<cfinclude template="#thisDir#/commviewpend.cfm">	
<CF_ABORT>

	

<CFIF sentmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) have already been sent so they could not be updated:<UL>" & sentmessage & "</UL>">		
</CFIF>

<CFIF chgmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be rescheduled/deleted because they have been changed by another user since you viewed them:<UL>" & chgmessage & "</UL>">		
</CFIF>


<CFIF delmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) were successfully deleted:<UL>" & delmessage & "</UL>">		
</CFIF>

<CFIF scheduledmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) were successfully rescheduled:<UL>" & scheduledmessage & "</UL>">		
</CFIF>

<CFIF baddatemessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be rescheduled due to an invalid proposed send date:<UL>" & baddatemessage & "</UL>">		
</CFIF>

<CFIF invalidmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be rescheduled/deleted because you are not authorised to change them:<UL>" & invalidmessage & "</UL>">		
</CFIF>

<CFIF missmessage IS NOT "">
	<CFSET message = message & "<P>The following communication(s) could not be found:<UL>" & missmessage & "</UL>">		
</CFIF>




<cfinclude template="#thisDir#/commMenu.cfm">	
<CF_ABORT>

