<!--- �Relayware. All Rights Reserved 2014 --->
<!---
PreviewEmailContent.cfm

Displays both html and text and email content

included from checkCommDetail.cfm
and can also be called from a link eg in commcheck.cfm

2009/06/03 	WAB altered the preview IFrame so that we squirt to content into it, rather than using the src.  Allows us to get rid of mapping to that directory
2009/10/28  WAB	problem when called directly (ie not as a cfinclude)
2011-11-20 	NYB	LHID 4918 - added support for CommEmailFormat being db driven
2012/02/15 	NJH	CASE 426479: Use regexp functions to remove unwanted strings and tags
2013-01		WAB	Sprint 15&42 Comms.  Converted to use communication.cfc and added merge field validation with feedback
2013-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-05		WAB		Reduced size of preview frame
2015-09-23	ACPK	CORE-1014 Readded Save & Continue button
2015-09-28	ACPK	CORE-1014 Replaced Save & Continue button with Continue button
--->

<CFPARAM name="frmRows" default = "25">
<CFPARAM name="stepName" default="">
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">

<cfset height = frmRows * 20>

<cfset communication = application.com.communications.getCommunicationObject (commid = frmCommID)>

<script>
	checkFormpreviewEmailContent = function(btn) {
		document.getElementById("previewform").submit();
		return;
	}
</script>

<!--- just a small form for the continue button.. it doesn't do anything other than move on to the next page --->
<form name="preview" id="previewform" action="commDetailsHome.cfm" method="post">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#frmCommID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCallingTemplate" VALUE="#frmCallingTemplate#">
</form>

<cfif communication.hasEmailContent() >
	<cfparam name="personid" default = "#request.relayCurrentUser.personid#">

	<!--- We are going to do two merges, one evaluates all possible options when dealing with <RELAY_IF> tags - we use this validate the content, but the preview we display is a normal merge --->
	<cfset validateMergedContent = communication.prepareAndValidateContent ()>

	<cfset communication.resetPreparedContent()>

	<cfset MergedContent = communication.getMergedContentForSinglePerson (personid = personid,test=true,mergeByFile = validateMergedContent.isOK)>	<!--- if there is a merge error then we don't do a mergebyfile, so we can atleast get some preview back --->

	<cfif stepName is not ""> <!--- stepName will be set if this template is called from within the tabbed wizard, not if called from the contact history --->
			  <!--- If there are merge errors then set as not previewed. --->
			<CFQUERY NAME="setPreviewReviewed" DATASOURCE="#application.SiteDataSource#">
			update communication
			set PreviewReviewed= <cfif validateMergedContent.isOK>1<cfelse>0</cfif>
			where CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  and PreviewReviewed=0
			</CFQUERY>
	</cfif>

	<cfset frmCommEmailFormat = application.com.communications.getCommEmailFormats(CommEmailFormatID=communication.CommEmailFormatID).CommEmailFormat>

	<cfoutput>
	<cfif stepName is not "">
		<h2>Phr_Sys_comm_Step_#htmleditformat(stepName)#_Header</h2>
	</cfif>

	<div class="form-group">
		<label>Subject:</label>
		#mergedContent.email.subject.phraseText#
	</div>

	<cfif communication.sendMessage and (mergedContent.message.title.phraseText is not "" OR arrayLen(validateMergedContent.message.text.errorArray) or arrayLen(validateMergedContent.message.title.errorArray)) and communication.sendMessage>
		<div id="previewMessageContent">
			<b>Message Content</b>
		</div>

		<cfif arrayLen(validateMergedContent.message.text.errorArray) or arrayLen(validateMergedContent.message.title.errorArray)>
			<div CLASS="errorblock">
				<p>The following Merge Field errors have been found:</p>
				<cfif arrayLen(validateMergedContent.message.title.errorArray)>Title: <cfoutput>#communication.FormatContentErrorArray_HTML(validateMergedContent.message.title.errorArray)#</cfoutput></cfif>
				<cfif arrayLen(validateMergedContent.message.text.errorArray)>Body: <cfoutput>#communication.FormatContentErrorArray_HTML(validateMergedContent.message.text.errorArray)#</cfoutput></cfif>
			</div>
		</cfif>

		<div style="width:500px;height:80px;">
			<cfif mergedContent.message.image neq ""><img src="#mergedContent.message.image#" style="float:left;padding-right:5px;"></cfif>
			<span style="padding-bottom:5px;font-weight:bold;">#mergedContent.message.title.phraseText#</span><br>
			#mergedContent.message.text.phraseText#<br>
		</div>
	</cfif>

			<cfif mergedContent.email.html.phraseText is not "" OR arrayLen(validateMergedContent.email.html.errorArray)>
				<div class="form-group">
					<label>Email HTML Content</label>



				<cfif arrayLen(validateMergedContent.email.html.errorArray)>
					<div class="errorblock">
						The following Merge Field errors have been found:
						<cfoutput>#communication.FormatContentErrorArray_HTML(validateMergedContent.email.html.errorArray)#</cfoutput>
					</div>
				</cfif>

				<!---
				WAB 2009/06/03
				Populate Iframe directly with content rather than using the src
				This means that we can kill the IIS mapping to to these directories

				<iframe name="htmlcontent" width="800" height="#height#" src="#EmailFiles.url#?x=#now()#" > <!--- put the now onthe url to prevent caching of the content --->
				</iframe>
				--->
				<iframe id="htmlcontent" height="#htmleditformat(height)#"  >
				</iframe>
				<!--- having script tags in there seems to cause problems (javascript string unterminated, so just removed for the preview)
					NYB 2011-08-22 P-LEN024 bug replaced:
					<cfset doctoredcontent = rereplacenocase(emailcontent.html,"<script>.*?</script>","","ALL")> <!--- having script tags in there seems to cause problems (javascript string unterminated, so just removed for the preview) --->
					: with:
				--->
<!---						<cfset doctoredcontent = rereplacenocase(emailcontent.html,"<script{1}[^>]*[^<]*</script>{1}","","ALL")> <!--- having script tags in there seems to cause problems (javascript string unterminated, so just removed for the preview) --->
						<!--- NYB 2011-08-22 P-LEN024 bug - also removed html comments - this was causing a js error --->
 						<cfset doctoredcontent = rereplacenocase(doctoredcontent,"<!--{1}[^->]*-->{1}","","ALL")> <!--- having script tags in there seems to cause problems (javascript string unterminated, so just removed for the preview) ---> --->

				<!--- NJH 2012/02/15 CASE 426479: Use regexp functions to remove unwanted strings and tags --->
				<cfset doctoredcontent = mergedContent.email.html.phraseText>
				<cfset doctoredcontent = application.com.regExp.replaceHTMLTagsInString(inputString=doctoredcontent,htmlTag="SCRIPT",hasEndTag = true)>
				<cfset doctoredcontent = application.com.regExp.replaceHTMLTagsInString(inputString=doctoredcontent,htmlTag="SCRIPT",hasEndTag = false)>
				<cfset doctoredcontent = application.com.regExp.removeComments(doctoredcontent,"HTML")>

				<!--- WAB 2013-07-03 problem with ajax and javascript containing <head> tags (possibly in combination with style tags), so lets just remove them.
					Not quite valid HTML, but styles still work OK --->
				<cfset doctoredcontent = rereplacenocase(doctoredcontent,"<head.*?>(.*?)</head>","\1")>

				<script>
					populateIframe = function(frameID,content) {
						iFrame = document.getElementById (frameID)
						doc = iFrame.contentDocument;
				        if (doc == undefined || doc == null)
				            doc = iFrame.contentWindow.document;

						doc.open()
						doc.write (content)
						doc.close()
					}

					populateHTMLcontent = function() {
						populateIframe ('htmlcontent','#jsstringformat(doctoredcontent)#')
					}

					populateHTMLcontent()  <!--- did have a setTimeout, but no longer necessary because the ColdFusion.Event.registerOnLoad function is being called automatically --->

					<cfif stepName is not "" AND validateMergedContent.isOK>
						setTabCompleted ('Preview')
					</cfif>

				</script>
				</div>
			<cfelse>
				<!--- note, the tabbed interface will only run one piece of JS in a file, so this function has to be here and in the script block above --->
				<cfif stepName is not "">
					<SCRIPT type="text/javascript">
						var tabImg=document.getElementById("PreviewTabTitleImg");
						tabImg.src = tabImg.src.replace(/step([0-9])\.png/, "step$1-completed.png");
					</SCRIPT>
				</cfif>
			</cfif>

			<cfif trim(mergedContent.email.Text.phraseText) is not "" OR arrayLen(validateMergedContent.email.text.errorArray)>

				<div class="form-group">
					<label>Email Text Content</label>
					<cfif arrayLen(validateMergedContent.email.text.errorArray)>
						<div class="errorblock">
							The following merge errors have been found
							<cfoutput>#communication.FormatContentErrorArray_HTML(validateMergedContent.email.text.errorArray)#</cfoutput>
						</div>
					</cfif>
					<textarea class="form-control" cols="80" rows="#frmRows#" readonly="true">#mergedContent.email.Text.phraseText#</textarea>
				</div>
			</cfif>

				<cfset showSave = false>
				<cfset showSaveAndContinue = false> <!--- 2015-10-01	ACPK	CORE-1014 Replaced Save & Continue button with Continue button --->
				<cfset showContinue = true>
				<CFINCLUDE TEMPLATE="saveAndContinueButtons.cfm">



	</cfoutput>
</cfif>

