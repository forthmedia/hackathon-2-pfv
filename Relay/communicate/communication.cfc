<!--- �Relayware. All Rights Reserved 2014 --->
<!---
communication.cfc

WAB January 2013

Sprint 15&42 Comms

A large reorganisation of communications code into a single object.
Driven by two requirements
	i) a need to improve the sending code and hopefully reduce locks
	ii) the need to consolidate the code which merged a communication, so that the same could be used to
		preview content, verify content, mass merge content, display content to a single individual
It then rather mushroomed

The strucure of the data within the object is very similar to that which was previously returned
by getCommunicationDetails(), except that many of the properties have been replaced by methods.
This has improved efficiency, since complicated things do not need to be calculated until they are needed.
All the templates in the communicate directories were updated to use this object instead of getCommunicationDetails()

Most of the functions came from com.communications, plus some from tskcommsend
There are still a few functions in com.communications and other places which could be brought into this cfc

Some things of note.
DEBUG
There is a debug mode (this.debug = true) which will dump timings and data to a log file [mainly used for performance testing sending]

ASYNCHRONOUS PROCESSING
The concept of asynchronous sending using threads was added.
Much work was done to the sending page to do sending via Ajax and to give status updates on the asynchronous processing

COMMRESULTOBJECT
There is a separate commResultObject which is used to store feedback during sending.
At the end of a send, the data from this object is serialised and stored in a new commSendResult table.
It is not intended to be a permanent record, but is does allow SessionA to see details of comms sent by SessionB (which was very handy during development, may be of less use in real life)
I may add a housekeeping task to delete old items in this table


2013-06-19	WAB	Use vAdminCompany instead of vOrgstoSupressFromReports
2013-07-04	WAB	Sort out the omniture tracking
2013-08-15	YMA	Case 436313 fromDisplayName now stored in commFromDisplayName.
2013-19-02  WAB	CASE 437278 Needed to default result.message in function prepareEmailForMerge()
2013-10-11 	WAB CASE 437278 Fix did not work, have moved test into a different function
2013-10-17	WAB	CASE 		Fix error in applyFilter() when email address was longer than 100 characters
2013-10-08	WAB	CASE 437315 Work to prevent memory errors when sending large numbers of comms
							Added ability to cancel a communication during sending
2014-06-18 WAB CASE 440177/440685 Add support for an OptInCommsTypeALL flag
2014-09-23	WAB Fix Syntax error in commit of 2014-06-18
2016-01-04 RJT BF-139 Rights has been updated to take a countryID (and default to a relayCurrentUser version if its missing, we need to give it the correct one (because createCommDetailTemporaryTable is called from housekeeping not from a persons action)
2016-10-26	WAB	PROD2016-2586 still problems in same area as BF-139, this time rights needs a value for relayCurrentUser.isInternal - which does not exist for a scheduled process.  So pass in isInternal=true - this will get the correct country rights
--->

<cfcomponent output="false" cache="false">

	<!--- A structure for storing internal data --->
	<cfset variables.data = {}>

	<cfset variables.data.debug = false>

	<cfset variables.communications = application.com.communications>
	<cfset this.constants = variables.communications.constants>


	<!---
		Init()
		Object is always initialised with an existing commid
		Loads up the basic communication record and make available in the this scope
	--->
	<cffunction name="init" output="false">
		<cfargument name="commid" type="numeric" required="true">
		<cfargument name="personid" type="numeric" default="#request.relaycurrentuser.personid#">

			<cfset var details = "">
			<cfset var item = '' />
			<cfset var tempFolder = '' />
			<cfset var debugID = '' />
			<cfset var filename = '' />

			<cfset details = getCommunication(commid = arguments.commid, personid = arguments.personid)>

			<cfloop collection="#details#" item="item">
				<cfset this[item] = details[item]>
			</cfloop>

			<cfset variables.data.personid = arguments.personid>


		<cfreturn this>

	</cffunction>

	<cffunction name="setdebug" output="false">
		<cfargument name="debug" type="boolean" default="true">

		<cfset var tempFolder = '' />
		<cfset var debugID = '' />
		<cfset var filename = '' />

		<cfset variables.data.debug = arguments.debug>

		<cfif arguments.debug >
			<!--- Initialise a debugging file --->
			<cfset tempFolder = application.com.filemanager.getTemporaryFolderDetails()>
			<cfset debugID = createuuid()>
			<cfset filename = "comm_#this.commid#_#debugid#.htm">
			<cfset variables.debug = {
						debugID = debugID,
						start = getTickCount(),
						previous = getTickCount(),
						webPath = tempFolder.webpath & "/" & filename,
						filePath = 	tempFolder.path & "/" & filename}>


			<cfset variables.sendResultObject.debugFile = variables.debug.webPath>

		</cfif>

		<cfreturn >
	</cffunction>

	<cffunction name="isDebug" output="false">
		<cfreturn variables.data.debug>
	</cffunction>


	<cffunction name="hasEmailFiles" output="false">
		<cfreturn getCommFileInfo().hasEmailFiles>
	</cffunction>


	<cffunction name="hasEmailContent" output="false">
		<cfreturn getContent().email.contentExists>
	</cffunction>

	<cffunction name="hasMessageContent" output="false">
		<cfreturn getContent().message.contentExists>
	</cffunction>

	<cffunction name="useMessage" output="false">

		<cfif not structKeyExists (variables.data,"useMessage")>
			<!--- variable used to determine whether to display messages or not.. currently based on whether the activity stream has been place in content. --->
			<cfset variables.data.useMessage = application.com.communications.useMessage()>
		</cfif>

		<cfreturn variables.data.useMessage >
	</cffunction>


	<!---
		isContentOK()
		Returns whether the content will merge correctly
		Content is tested every time it is changed and the result is stored in the db
		This function returns the cached value, unless refresh=true is passed in
	 --->

	<cffunction name="isContentOK" output="false">
		<cfargument name="refresh" default = "false">

		<cfset var result = false>

		<cfif hasEmailContent()>
			<cfif refresh or not StructKeyExists (this.metadata,"contentValidated")>
				<cfset prepareAndValidateContent()>
			</cfif>
			<cfset result = this.metadata.contentValidated>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="hasEmailAttachmentFiles" output="false">
		<cfreturn getCommFileInfo().hasEmailAttachmentFiles>
	</cffunction>


	<cffunction name="hasCommDetailRecords" output="false">
			<cfreturn getCommDetailInfo().hasCommDetailRecords>
	</cffunction>


	<cffunction name="hasTestCommDetailRecords" output="false">
		<cfreturn getCommDetailInfo().hasTestCommDetailRecords>
	</cffunction>


	<cffunction name="hasUnsentCommDetailRecords"  output="false">
		<cfreturn getCommDetailInfo().hasUnsentCommDetailRecords>
	</cffunction>


	<cffunction name="canBeEdited"  output="false">
		<cfreturn not(hasBeenSentLive())>
	</cffunction>


	<cffunction name="hasRightsToEdit"  output="false">
		<cfreturn getRights().hasRightsToEdit>
	</cffunction>


	<cffunction name="hasRightsToSchedule"  output="false">
		<cfreturn getRights().hasRightsToSchedule>
	</cffunction>


	<cffunction name="hasBeenSentExternally"  output="false">
		<cfreturn getSelectionInfo().hasBeenSentExternally>
	</cffunction>


	<cffunction name="hasBeenSentInternally"  output="false">
		<cfreturn getSelectionInfo().hasBeenSentInternally>
	</cffunction>

	<cffunction name="hasBeenSentLive"  output="false">
		<cfreturn getSelectionInfo().hasBeenSentExternally OR getSelectionInfo().hasBeenSentInternally>
	</cffunction>

	<cffunction name="hasApprovalSelections"  output="false">
		<cfreturn getSelectionInfo().hasApprovalSelections>
	</cffunction>


	<cffunction name="hasExternalSelection" output="false">
		<cfreturn getSelectionInfo().hasExternalSelection>
	</cffunction>


	<cffunction name="hasFailureSelections" output="false">
		<cfreturn getSelectionInfo().hasFailureSelections>
	</cffunction>


	<cffunction name="hasSelectionsWaitingToSend" output="false">
		<cfreturn getSelectionInfo().hasSelectionsWaitingToSend>
	</cffunction>


	<cffunction name="hasExternalSelectionsWaitingToSend" output="false">
		<cfreturn getSelectionInfo().hasExternalSelectionsWaitingToSend>
	</cffunction>


	<cffunction name="getSelections" output="false">
		<!--- TBD deal with selectionTypes Parameter --->

		<cfif not structKeyExists(variables.data,"Selections")>
			<cfset variables.data.Selections = getSelectionDetails()>
		</cfif>

		<cfreturn variables.data.Selections >
	</cffunction>


	<!---
		isOKToSendNow()
		A combination of isApprovedToSendNow() and isContentOK
	 --->
	<cffunction name="isOKToSendNow" output="false">

			<cfset var result = false>
			<cfif not getApprovalDetails().requiresApproval or getApprovalDetails().isapprovedToSendNow >
				<cfset result = true>
			</cfif>

			<cfset result = BITAND (result,isContentOK())>

		<cfreturn result>

	</cffunction>


	<!---
		isOKToQueueNow()
		Must be approved and the content must be OK
	 --->
	<cffunction name="isOKToQueueNow" output="false">

			<cfset var result = false>

			<cfif not getApprovaldetails().requiresApproval or getApprovaldetails().isapproved >
				<cfset result =true>
			</cfif>
			<cfset result = BITAND (result,isContentOK())>

		<cfreturn result>

	</cffunction>


	<!---
		getCountryIDList()
		List of countries that a communication is going to be sent to
		Excludes "internal" people
		Value is cached in the db.  It is updated when selecctions are added or if refresh=true is passed
	--->
	<cffunction name="getCountryIDList" output="false">
		<cfargument name="refresh" default="false">
			<cfset var result = '' />

			<cfif not refresh>
				<cfset result = this.countryIDList>
			<cfelse>
				<cfset result = updateCountryIDList ()>
			</cfif>

		<cfreturn result >
	</cffunction>


	<cffunction access="public" name="getOwnerCountryIDList" hint=""  output="false">

		<cfif not structKeyExists (variables.data,"ownersCountryIDList")>
			<cfset variables.data.ownersCountryIDList = communications.getCommunicationOwnersCountryIDList (this.CommID)>
		</cfif>

		<cfreturn variables.data.ownersCountryIDList>

	</cffunction>

	<!---
        Gets the main details for a communication.
		Called by init function
	--->
	<!--- 2013-08-15	YMA	Case 436313 fromDiaplyName now stored in commFromDisplayName. --->
	<cffunction name="getCommunication" returnType="struct" hint="Gets the main details for a communication." access="private" output="false">
		<cfargument name="commid" type="string" required="true">
		<cfargument name="personid" type="string" required="false">

		<cfset var approvalDetails = "">
		<cfset var getCommunicationQuery = "">
		<cfset var getCommunication = structNew()>
		<cfset var getCommDetailLastSent = "">
		<cfset var field = "">
		<cfset var key = "">
		<cfset var getAttachments = "">
		<cfset var getAttachmentSize = "">
		<cfset var keyList = '' />
		<cfset var getCommDetailUnsent = '' />
		<cfset var getSiteDefStruct = {isOK = false} />

		<cfquery name="getCommunicationQuery" datasource="#application.siteDataSource#">
			SELECT
				isNull(c.FromAddress,'#application.emailFrom#@#application.mailfromdomain#') as fromaddress,
				isNull(fdn.displayName,'#application.EmailFromDisplayName#') as fromdisplayName,
				c.*,
				cp.campaignname,
				c.Created AS datecreated,
				l.countryid as creatorCountryid,
				isNull(c.SendDate,0) AS senddate,
				(select ll.ItemText from LookupLIst ll where c.CommTypeLID = ll.LookupID) as commType,
				ll.ItemText AS commform,
				ug.Name AS creatorName,
				ug.personID as creatorID,
				case when ug.personid = #personid# then 1 else 0 end as isOwner,
				p.email as creatorEmail,
				case when isnull(senddate,'') <> '' and sent = 0 Then 'true' else 'false' end as scheduled,
				c.CommEmailFormatID

			FROM (Communication c left JOIN (location l inner join Person p on p.locationid = l.locationid
			inner JOIN UserGroup ug ON p.PersonID = ug.PersonID) ON c.CreatedBy = ug.UserGroupID)
			INNER JOIN LookupList ll ON c.CommFormLID = ll.LookupID
			left join commFromDisplayName as fdn on c.commFromDisplayNameID = fdn.commFromDisplayNameID
			left join campaign cp on  cp.campaignid = c.campaignid
			WHERE c.CommID =  <cf_queryparam value="#arguments.commid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<!--- convert QueryToStruct --->
		<cfloop index = "field" list="#getCommunicationQuery.columnList#">
			<cfset getCommunication[field]	= getCommunicationQuery[field][1]>
		</cfloop>
			<cfif getCommunicationQuery.parameters is not  "">
				<cfset getCommunication.metadata = deserializeJSON(getCommunicationQuery.parameters)>
			<cfelse>
				<cfset getCommunication.metadata = {}>
			</cfif>

			<cfif getCommunication.linktoSiteURL is not "">
				<cfset getSiteDefStruct = application.com.relayCurrentSite.getSiteDefStructureByDomain(getCommunication.linktoSiteURL)>
			</cfif>

			<cfif not getSiteDefStruct.isOK><!--- TBD do something about domains which are not in the current environment (which happens when databases get moved from live to staging) --->
				<cfset getCommunication.linktoSiteURLWithProtocol = application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain()>
			<cfelse>
				<cfset getCommunication.linktoSiteURLWithProtocol = getSiteDefStruct.protocolAndDomain>
			</cfif>

			<cfset getCommunication.recordcount = getCommunicationQuery.recordcount>

		<cfreturn getCommunication>
	</cffunction>


	<cffunction access="public" name="getCountryIDListFromDB" hint="" returnType = "string" output="false">

			<cfset var getCommCountries1 = "">
			<cfset var getCommCountries2 = "">
			<cfset var getCommCountries = "">

					<!--- gets countries that a communication has been/is being sent to--->

					<!---
					TBD I wonder whether we need to exclude the people in the excluded list
					 --->

					<!--- WAB 2005-05-23
					Had some odd performance problems with this query
					the two parts of the union worked fine by themselves, but when unioned they took ages (30 seconds sometimes)
					don't know where the problem might be but decided to run them separately and union in memory
					--->
					<cfquery name="getCommCountries1" datasource="#application.siteDataSource#">
					<!--- countries from items in commdetail (ie sent) --->
				<!---
				WAB 2009/01/14 Performance probs with the left join, try not in
				select distinct c.countryid
					from
						country c
							inner join
						location l on l.countryid = c.countryid
							inner join
						person p on l.locationid = p.locationid
							inner join
						commdetail cd on cd.personid = p.personid
							left join
						vOrgsToSupressFromReports as supp    <!--- added WAB 2006-04-26 to prevent 'internal' people in mailing selection moving country and screwing up the approval process --->
							on supp.organisationid = l.organisationid
					where cd.commid = #this.commid# and Internal = 0 and test = 0
						and supp.organisationid is null
				--->
				select distinct l.countryid
					from
						location l
							inner join
						commdetail cd on l.locationid = cd.locationid
							left join
						vAdminCompany v on v.organisationid = l.organisationid
					where cd.commid = #this.commID# and Internal = 0 and test = 0
						--	and l.organisationid not in (select distinct organisationid from vOrgsToSupressFromReports)
						and v.organisationid is null

					</cfquery>
<!--- 					union --->

					<!--- countries from items in selections which haven't been sent yet
						filtered by owner country
					 --->
					<cfquery name="getCommCountries2" datasource="#application.siteDataSource#">
					select distinct l.countryid
					from
						location l
							inner join
						person p on l.locationid = p.locationid
							inner join
						selectiontag st on st.entityid = p.personid
							inner join
						commselection cs	on cs.selectionid = st.selectionid
												and cs.commselectlid = 0
												and cs.sent <> 1
												and st.status <> 0
							left join
						vadminCompany as supp    <!--- added WAB 2006-04-26 to prevent 'internal' people in mailing selection moving country and screwing up the approval process --->
							on supp.organisationid = l.organisationid

					where cs.commid = #this.commID#
						and l.countryid  in ( <cf_queryparam value="#getOwnerCountryIDList()#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						and supp.organisationid is null

					order by l.countryid
					</cfquery>

					<cfquery name="getCommCountries" dbtype=query>
					select countryid from getCommCountries1
					union
					select countryid from getCommCountries2
					</cfquery>


		<cfreturn valuelist(getCommCountries.countryid)>

	</cffunction>


	<!---
		setMetaData()

		Used to store data into the parameters db field
		Accessible through this.metadata[]
	 --->
	<cffunction name="setMetaData" output="false">
		<cfargument name="name" required="true">
		<cfargument name="value" required="true">

		<cfset var updateMetadata = '' />

		<cfset this.metadata[name] = value>

		<cfquery name="updateMetadata" datasource="#application.siteDataSource#">
		update communication
		set parameters =  <cf_queryparam value="#serializeJSON(this.metadata)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		where
		commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>

		<cfreturn value>

	</cffunction>


	<cffunction name="deleteMetaData" output="false">
		<cfargument name="name" required="true">

		<cfset var updateMetadata = '' />

		<cfset structDelete(this.metadata,name)>

		<cfquery name="updateMetadata" datasource="#application.siteDataSource#">
		update communication
		set parameters =  <cf_queryparam value="#serializeJSON(this.metadata)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		where
		commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>

	</cffunction>


	<cffunction name="getRights" output="false">
		<cfset var result = "">

		<cfif not structKeyExists(variables.data,"rights")>
			<cfset result = {hasRightsToEdit = false, hasRightsToSend = false, hasRightsToSchedule = false }>

				<!--- if personid has been passed, then check whether person has edit rights
				at the moment this is just a question of checking whether they are the creator
				but put here so can be expanded to check recordrights if required.
				  --->
				<cfset result.hasRightsToEdit =this.isOwner>

				<cfif application.com.communications.isUserACommunicationsAdministrator()>
						<cfset result.hasRightsToEdit =true>
				</cfif>


				<cfif (application.com.login.checkInternalPermissions ("CommTask", "Level3") or application.com.login.checkInternalPermissions ("CommTask", "Level4"))
						or (application.com.login.checkInternalPermissions ("CommTask", "Level2") and this.ISOWNER)>

					<cfset result.hasRightsToSchedule = true>
					<cfif isOKToSendNow()>
						<cfset result.hasRightsToSend = true>
					</cfif>
				</cfif>

				<cfset variables.data.rights = result>

		</cfif>

		<cfreturn variables.data.rights>

	</cffunction>



	<cffunction name="getSelectionInfo" output="false">

		<cfif not structKeyExists(variables.data,"SelectionInfo")>

			<cfquery name="variables.data.selectionInfo" datasource="#application.siteDataSource#">
			select
				convert(bit, isnull ( sum (case when cs.commselectlid =  <cf_queryparam value="#this.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  and cs.sent in( 1,3) then 1 else 0 end),0)) as hasBeenSentExternally,
				convert(bit, isnull ( sum (case when cs.commselectlid in (<cf_queryparam value="#this.constants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#this.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >) and sent in (1,3) then 1 else 0 end),0)) as hasBeenSentInternally,
				convert(bit, isnull ( sum (case when commselectlid = 11 then 1 else 0 end),0)) as hasApprovalSelections,
				convert(bit, isnull ( sum (case when commselectlid = 0 then 1 else 0 end),0)) as hasExternalSelection,
				convert(bit, isnull ( sum (case when commselectlid = 10  then 1 else 0 end),0)) as hasFailureSelections,
				convert(bit, isnull ( sum (case when commselectlid in (<cf_queryparam value="#this.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#this.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#this.constants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >)  and selectionid > 0 and sent <> 1  then 1 else 0 end),0)) as hasSelectionsWaitingToSend,
				convert(bit, isnull ( sum (case when  commselectlid  in ( <cf_queryparam value="#this.constants.externalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  and selectionid > 0 and sent <> 1 then 1 else 0 end),0)) as hasExternalSelectionsWaitingToSend
			from commselection cs
			where commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>
		</cfif>

		<cfreturn variables.data.SelectionInfo>

	</cffunction>

	<cffunction name="getCommDetailInfo" output="false">
		<cfif not structKeyExists(variables.data,"CommDetailInfo")>

			<cfquery name="variables.data.CommDetailInfo" datasource="#application.siteDataSource#">
			select
				convert(bit, isnull ( sum (case when test = 0 then 1 else 0 end),0))  as hasCommDetailRecords,
				convert(bit, isnull ( sum (case when test = 1then 1 else 0 end),0))  as hasTestCommDetailRecords,
				convert(bit, isnull ( sum (case when feedback='' and test = 0 then 1 else 0 end),0))   as hasUnsentCommDetailRecords

			from commdetail cs
			where commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>
		</cfif>

		<cfreturn variables.data.CommDetailInfo>

	</cffunction>

	<cffunction name="getCommFileInfo" output="false">
		<cfif not structKeyExists(variables.data,"getCommFileInfo")>

			<cfquery name="variables.data.getCommFileInfo" datasource="#application.siteDataSource#">
			select
				convert(bit, isnull ( sum (case when commformlid = 2 and commfileid = isparent then 1 else 0 end),0)) as hasEmailFiles,
				convert(bit, isnull ( sum (case when commformlid = 2 and commfileid <> isparent then 1 else 0 end),0)) as hasEmailAttachmentFiles

			from commfile
			where commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>
		</cfif>

		<cfreturn variables.data.getCommFileInfo>

	</cffunction>


	<cffunction access="public" name="getSelectionDetails" hint="" returnType = "query" output="false">
		<cfargument name="selectionTypes" type="string" required="false"><!--- supports values: external,approver,internal,exclude,failure,test --->

			<cfset var getSelectionDetails = "">
			<cfset var selectionTypeSelectLIDs = "">
			<cfset var incTest = "true">
			<cfset var i = "">
			<cfset var selectionsBeingSent = "-1">

			<cfset var checkIfCommunicationIsBeingSent = checkIfCommunicationIsBeingSent()>
			<cfif checkIfCommunicationIsBeingSent.isBeingSent and structKeyExists(checkIfCommunicationIsBeingSent.metadata,"selectionids") and checkIfCommunicationIsBeingSent.metadata.selectionids is not "">
				<cfset selectionsBeingSent = checkIfCommunicationIsBeingSent.metadata.selectionids>
			</cfif>


			<cfif structkeyexists(arguments,"selectionTypes")>
				<cfset incTest = "false">
				<cfloop index="i" list="#arguments.selectionTypes#">
					<cfif i eq "test">
						<cfset incTest = "true">
					<cfelse>
						<cfset selectionTypeSelectLIDs = ListAppend(selectionTypeSelectLIDs,this.Constants["#i#SelectLID"])>
					</cfif>
				</cfloop>
			</cfif>

			<!--- get list of selections associated with this communication  --->
			<!--- 2013-06-10	WAB Added some extra fields to indicate whether current user has rights to selection and to generate a title for the select box on the commSelection page	--->
			<CFQUERY name="getSelectionDetails" datasource="#application.sitedatasource#">
				<cfif not structkeyexists(arguments,"selectionType") or (listlen(selectionTypeSelectLIDs) gt 0)>
				SELECT 	cs.CommID,
						cs.selectionid,
						cs.CommSelectLID,
						cs.sent,
						cs.sendDate,
						cs.sentDate,
						case when cs.sendDate is not null and css.sentTextID in ('Scheduled','partSent') then 1 else 0 end as scheduled,
						css.sentTextID,
						s.lastupdated,
						case when cs.selectionid <= 0 then 0 else 1 end as isSelection,
						CASE when cs.selectionid in (#selectionsBeingSent#) THEN 'Sending' ELSE sentTextID END as Status,
						case when cs.selectionid > 0 and s.Title is null then 1 else 0 end as selectionDeleted,
						case when cs.selectionid <= 0 then 'Individually Selected People' when s.Title is null then cs.Title_IfOriginalDeleted  else s.title end as title,
						case when rg.personid is null then '* ' else '' end + title + ' (' + convert(varchar,cs.selectionid) +')' + case when css.sentTextID in ('sent','partSent') then ' (Already Sent)'	else '' end as titleForCommSelection,
						case when cs.selectionid <= 0 then '' when s.Title is null then 'Deleted' else s.Description  end as Description,
						cs.selectionid,
						case when cs.CommSelectLID = 0 then 'External' when cs.CommSelectLID =  <cf_queryparam value="#this.constants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  then 'Approver' when cs.CommSelectLID =  <cf_queryparam value="#this.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  then 'Internal' when cs.CommSelectLID =  <cf_queryparam value="#this.constants.excludeSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  then 'Excluded' end as Type,
						case when cs.CommSelectLID = 0 then 1 when cs.CommSelectLID =  <cf_queryparam value="#this.constants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  then 3 when cs.CommSelectLID =  <cf_queryparam value="#this.constants.internalSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  then 2 when cs.CommSelectLID =  <cf_queryparam value="#this.constants.excludeSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >  then 4 end as sortorder1,
						1 as sortorder2,
						case when s.createdby <> <cf_queryparam value="#this.createdby#" CFSQLType="cf_sql_integer"> then 1 else 0 end as ownedBySomeOneElse,
						case when rg.personid is not null then 1 else 0 end as userHasRightsToSelection,
						case when cs.selectionid > 0 then
						s.selectedCount
						<!--- 	WAB 2005-10-25 removed this subquery because very slow
								replaced with just using the selectedCount field in the selection table
								only makes a difference if using a shared selection which covers countries that the owner does not have rights to
							(select count(1)
								from SelectionTag as st
									inner join
								person as p on p.personid = st.entityid and p.active = 1
									inner join
								location as l on p.locationid = l.locationid and l.active = 1
									inner join
								organisation as o on l.organisationid = o.organisationid and o.active = 1
								where
									s.SelectionID = st.SelectionID AND st.Status>0
									and (l.countryid in (#this.ownersCountryIDList()#) or cs.CommSelectLID = #this.constants.approverSelectLID#)
							)
							 --->
						else (
									select count(1) from commdetail where commid = cs.commid and isnull(selectionid,0) = 0 and test = 0 and internal = (case when cs.commselectlid = 0 then 0 else 1 end)
								<!--- WAB 2005-05-03 not a very good subquery yet --->
							)
							end
							AS NumberofPeople


				FROM CommSelection as cs
						inner join
					CommSelectionSentStatus css on cs.sent = css.sent
						left join
					Selection as s on cs.SelectionID = s.SelectionID
						left join
 					(selectionGroup sg
					 	inner join
					 rightsgroup rg on sg.usergroupid = rg.usergroupid) on cs.selectionid = sg.selectionid  and rg.personid = #request.relaycurrentuser.personid#
				where
						cs.CommID=<cf_queryparam value="#this.commid#" CFSQLType="cf_sql_integer">
					<cfif structkeyexists(arguments,"selectionType")>
						cs.CommSelectLID in (<cf_queryparam value="#selectionTypeSelectLIDs#" CFSQLType="cf_sql_integer" list="yes" separator=",">)
					<cfelse>
						and cs.CommSelectLID in (0,#this.constants.approverSelectLID#,#this.constants.internalSelectLID#,#this.constants.excludeSelectLID#)
					</cfif>
				</cfif>

				<cfif not structkeyexists(arguments,"selectionTypes") or (listlen(selectionTypeSelectLIDs) gt 0 and incTest)>
					union
				</cfif>

				<cfif incTest>
					SELECT 	0,
							isNull(selectionid,0),
							case when  test = 0 and internal = 0 then 0 when internal = 1 then #this.constants.internalSelectLID# else -1 end as commSelectLID,
							1 as sent,
							'' as sendDate,
							'' as sentDate,
							0 as scheduled,
							'' as sentTextID,
							'' as lastupdated,
							0 as isSelection,
--							0,
							'' as status,
							0,
							'Individually Selected People' as title,
							'',
							'',
							0,
							case when  test = 0 and internal = 0 then 'External' when internal = 1 then 'Internal' else 'Test' end as type,
							case when  test = 0 and internal = 0 then 1 when internal = 1 then  2 else 5 end as sortorder1,
							2 as sortorder2,
								0,
								0 as userHasRightsToSelection,
							count(1)
					from commDetail
					where
						CommID=<cf_queryparam value="#this.commid#" CFSQLType="cf_sql_integer">
						and isNull(selectionid,0) = 0
						and (test = 1 )   <!--- WAb bitof a hack here --->
					group by test, internal,selectionid

				</cfif>
				order by sortorder1, sortorder2 ,title
			</CFQUERY>



		<cfreturn getSelectionDetails>

	</cffunction>


	<cffunction access="public" name="getContent" hint=""  output="false">

		<cfset var getAttachmentSize = '' />

			<cfif not structKeyExists (variables.data,"content")>

				<cfset variables.data.content = {}>
				<cfset variables.data.content.attachments = getAttachmentsQuery()>

				<CFQUERY NAME="getAttachmentSize" dbtype="query">
				select sum(fileSize) as filesize from variables.data.content.attachments
				</CFQUERY>

				<cfset variables.data.content.attachmentsTotalSize = iif(getAttachmentSize.fileSize is "",0,getAttachmentSize.fileSize)>
				<cfset variables.data.content.email = getEmailContent()>
				<cfset variables.data.content.message = getMessageContent()>

			</cfif>

			<cfreturn variables.data.content>

	</cffunction>



	<cffunction access="public" name="getDefaultContent" hint=""  output="false">

		<cfset var result = {html = "",text=""}>
		<CFSET var sDirGetEmailTemplates = application.paths.content & "\" & "\emailtemplates\">
		<CFSET var GetAutoEmailTemplate= "">
		<CFSET var checkfileexists= "">

			<CFQUERY NAME="GetAutoEmailTemplate" DATASOURCE="#application.SiteDataSource#">
				<!--- Gets a template to autoload - only gets files created by user, not ones which user has been given rights to --->
				SELECT 	Filename,
						name as Description
				FROM 	files inner join filetype on files.filetypeid=filetype.filetypeid
				WHERE 	filetype.path = 'emailtemplates'
				and     (name LIKE '%Autoload%' or name LIKE '%default%')
			 	AND 	personid=#request.relayCurrentUser.personid#
				ORDER BY Revision desc
			</CFQUERY>

			<CFIF getautoemailtemplate.recordcount GT 0>
				<!--- check file exists --->

				<CFDIRECTORY ACTION="list"
					DIRECTORY="#sDirGetEmailTemplates#"
					FILTER="#listgetat(getautoemailtemplate.filename,1)#"
					NAME="checkfileexists">
				<CFIF checkfileexists.recordcount IS NOT 0>
					<CFFILE ACTION="Read"
						FILE="#sDirGetEmailTemplates#/#checkfileexists.name#"
						VARIABLE="result.html">  <!--- WAB CASE 425951 file path being read was using wrong variable and failing --->
				</CFIF>


			</CFIF>


			<cfif result.html is "" and this.styleSheet is not "">
				<cfset result.html = "<html><head></head><body></body></html>">

				<cfset result.html = updateStyleBlock (result.html)>

			</cfif>

			<cfreturn result>

	</cffunction>


	<cffunction access="public" name="getMessageContent" returnType="struct" output="false">
		<cfset var result = {text = "",image="",url="",title="",contentExists=false}>
		<cfset var qGetCommMessage = "">
		<cfset var messageAttribute = "">

		<cfquery name="qGetCommMessage" datasource="#application.siteDataSource#">
			select phraseText,phraseTextID from vphrases
			where
					entityTypeID =  <cf_queryparam value="#application.entityTypeID.communication#" CFSQLTYPE="CF_SQL_INTEGER" >
				and entityid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="CF_SQL_INTEGER" >
				and phraseTextID in ('messageText','messageImage','messageURL','messageTitle')
				and len(cast(phrasetext as nvarchar(4000))) > 0
		</cfquery>

		<cfloop query="qGetCommMessage">
			<cfset messageAttribute = replace(phraseTextID,"message","")>
			<cfset result[messageAttribute] = phraseText>
		</cfloop>

		<!--- set content to be existing if we have a title and an image --->
		<cfif result.title neq "" and result.image neq "" and fileExists(expandPath(result.image))>
			<cfset result.contentExists = true>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction access="public" name="getEmailContent" returnType="struct" output="false"
		hint="Returns the content of a communication">

		<cfset var result = {messageFormat="",MultiPartMessage=false,SinglePartMessageFormat="",text="",html="",contentExists = false}>
		<cfset var qgetEmailContent = "">

		<cfquery name="qgetEmailContent" datasource="#application.siteDataSource#">
				select *, replace(phraseTextID,'Body','') as Type from vphrases
				where
						entityTypeID =  <cf_queryparam value="#application.entityTypeID.communication#" CFSQLTYPE="CF_SQL_INTEGER" >
					and entityid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and phraseTextID in ('HTMLBody','TextBody')    <!--- ie HTMLBody or TextBody--->
					and len(cast(phrasetext as nvarchar(4000))) > 0
			</cfquery>

		<!--- currently assuming only a single translation --->
		<cfloop query="qgetEmailContent">

				<cfset result[type] = qgetEmailContent.phraseText>
				<cfset result.contentExists = true>
				<cfset result.messageFormat = listappend (result.messageFormat,type)>

		</cfloop>

		<cfif qgetEmailContent.recordcount is 2>
			<cfset result.multipartMessage = true>
		<cfelse>
			<cfset result.SinglePartMessageFormat = qgetEmailContent.type>
		</cfif>

		<cfreturn result>


	</cffunction>



	<cffunction access="public" name="clearContent" hint="" output="false">
			<cfset structDelete (variables.data,"content")>
	</cffunction>

	<cffunction name="getAttachmentsQuery"  output="false">

		<cfset var getAttachments = "">

			<CFQUERY NAME="getAttachments" DATASOURCE="#application.SiteDataSource#">
				SELECT '#application.userFilesAbsolutePath#' + cf.Directory + '\' + cf.FileName AS attachment,
						'#application.userFilesAbsolutePath#' + cf.Directory + '\' + cf.FileName +'|' + cf.OriginalFileName AS htmlattachment,
						cf.OriginalFileName ,
						filesize,
						cf.Directory + '\' + cf.FileName  as relativePath,
						commfileid

				FROM CommFile AS cf
				 WHERE cf.CommFileID <> cf.IsParent  -- this excludes the file which is the actual email text
				   AND cf.CommFormLID = 2
				   AND cf.CommID = #this.commID#
			</CFQUERY>

		<cfreturn getAttachments>

	</cffunction>


	<cffunction name="hasLink" output="false">

		<cfset var hasLink = '' />

		<cfif not structKeyExists(variables.data,"hasLink")>

			<cfquery name="hasLink" datasource="#application.siteDataSource#">
				select 1 from vphrases where entityTypeID =  <cf_queryparam value="#application.entityTypeID.communication#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" > and phraseTextID in ('HTMLBody','TextBody') and (phraseText like '%<a %' OR phraseText like '%<area %')
			</cfquery>

			<cfset variables.data.hasLink = hasLink.recordCount>
		</cfif>

		<cfreturn variables.data.hasLink>
	</cffunction>

	<cffunction name="getArrayOfLinksInContent" output="false">
		<cfargument name="content" type="string" default="#getEmailContent().html#">

			<cfset var AnchorArray = application.com.regExp.findHTMLTagsInString(inputString=content,htmlTag="a")>
			<cfset var AreaArray = application.com.regExp.findHTMLTagsInString(inputString=content,htmlTag="area",hasendtag = false)>
			<cfset var linksArray = application.com.structureFunctions.arrayMerge(AnchorArray,AreaArray)>
			<cfset linksArray = application.com.structureFunctions.arrayOfStructsSort(arrayOfStructs = linksArray,key="position",sortType="numeric")>
		<cfreturn linksArray>
	</cffunction>

	<cffunction name="checkCommunicationSizeOkToSend" output="false">
		<cfargument name="numberOfrecipients" type="numeric" required="true">
		<cfargument name="test" type="boolean" default = "false">

		<cfset var result = structNew()>
		<cfset var timeNow = createTime(datepart("h",now()),datepart("n",now()),0)>
		<cfset var commLimits = getCommLimits()>
		<cfset var content = getContent()>

		<cfset result.isOK = true>

			 <CFIF commLimits.CommunicationMaxDayTimeSizeEnforced
				and timeNow GT createTime (listfirst(commLimits.CommunicationDayTimeStart,":"),listlast(commLimits.CommunicationDayTimeStart,":"),0)
				and	timeNow LT createTime (listfirst(commLimits.CommunicationDayTimeEnd,":"),listlast(commLimits.CommunicationDayTimeEnd,":"),0)
				and numberOfrecipients * (content.email.Size + content.attachmentsTotalSize)  GT commLimits.CommunicationMaxDayTimeSizeKB
				and not Test>
					<cfset result.numberRecipients = numberOfrecipients>
					<cfset result.size = content.email.Size + content.attachmentsTotalSize>
					<cfset result.totalSize = numberOfrecipients * (content.email.Size + content.attachmentsTotalSize)>
					<cfset result.maxAllowedSize = commLimits.CommunicationMaxDayTimeSizeKB>
					<cfset result.isOK = false>
			</cfif>

			<cfreturn result>

	</cffunction>


	<!---
		CreateCommDetailTemporaryTable
		Creates temporary table of all people a communication is to be sent to (or part of a communication)
		We run process against this temporary table to calulate options,unsubscribes etc to try and prevent locking on the commdetail table
		We then update commdetail from this temporary table
		It is then used as a filter when we build the final recipient list
	 --->

	<cffunction access="public" name="createCommDetailTemporaryTable" hint=""  output="false">
		<cfargument NAME="PersonIDs" default = "">
		<cfargument NAME="CommSelectLID" default = "#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.internalSelectLID#">
		<cfargument NAME="CommFormLID" default = "">
		<cfargument NAME="selectionID" default = "">
		<cfargument NAME="test" default = "false">

			<cfset var comtypelst = '' />
			<cfset var countryRightsFilter = '' />
			<cfset var createTempTable = '' />
			<cfset var getSelections = '' />
			<cfset var createTempTableResult = '' />
			<cfset var result = {tempTableName = '####tempCommDetail#replace(createUUID(),"-","","ALL")#'}>

			<cfset variables.tempTableName = result.tempTableName>

			<CFIF this.CommFormLID IS 5>
				<!--- both is 5 --->
				<cfset comtypelst = "2,3">
			<CFELSEIF arguments.test and (this.CommFormLID IS 6 or this.CommFormLID IS 7)>
				<!--- for tests send both types --->
				<cfset comtypelst = "2,3">
			<CFELSE>
				<!--- will be 2 email, 3 fax or 4 download
					  or 6,7 for either but prefer
					    --->
				<cfset comtypelst = this.CommFormLID>
			</CFIF>

			<cfset var communicationOwnerPersonID=communications.getCommunicationOwners(commid=this.commid)>

			<!--- 2016-10-26	WAB	PROD2016-2586 added isInternal to this calls --->
			<cfset countryRightsFilter = application.com.rights.getRightsFilterWhereClause(entityType="Location",alias="l",personid=communicationOwnerPersonID, isInternal = 1)>

			<cfif arguments.PersonIds is "">
				<!--- get List of SelectionIDs to be sent to
						this is used in the next query and also passed back in the result
				 --->
				<CFQUERY NAME="getSelections" DATASOURCE="#application.SiteDataSource#">
				select selectionid
				from commSelection cs
				where
					cs.commid = #this.commid#
					  <cfif arguments.selectionID is -1>	<!--- send to all outstanding selections regardless of type --->
						and cs.sent <> 1     -- only send to selections which haven't been sent yet
			 		  <CFELSEIF arguments.selectionID is not "">
					  	AND cs.selectionid  in ( <cf_queryparam value="#arguments.selectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  <!--- WAB added, limits query to the selections chosen at this time - allows selections to be added to an existing communications --->
					  <CFELSEIF arguments.CommSelectLID is not "">
					  	AND cs.CommSelectLID  in ( <cf_queryparam value="#arguments.CommSelectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )   <!--- or limits to a type of selection eg: approvers or internal --->
					  <cfelse>
					  	<!--- WAB 2005-05-04 removed this
							as part of changing to populate commdetail within tskcommsend.cfm
							and added the sent <> 1
						AND cs.CommSelectLID in (0)   <!--- else limit to external mailing list---> --->
						and cs.sent <>1
					  </CFIF>
					  and cs.commselectLID not in (<cf_queryparam value="#communications.constants.excludeSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" list="true" >)
					  and cs.selectionid not in (0)
				</CFQUERY>
				<cfset result.SelectionIDs = valueList (getSelections.selectionid)>

			</cfif>





			<CFQUERY NAME="createTempTable" DATASOURCE="#application.SiteDataSource#" RESULT="createTempTableResult">
			set nocount ON
			<!--- Create temporary table with all personIDs --->
			create table #tempTableName# (commid int, personid int, locationid int, commTypeId int, SelectionID int ,test int ,internal int, commdetailid int,commstatusid int, feedback varchar(50))
			INSERT INTO #tempTableName# (CommID, PersonID, locationid, commTypeId, SelectionID,test,internal,commdetailid,commstatusid, feedback )
			SELECT
				<cf_queryparam value="#this.CommID#" CFSQLTYPE="CF_SQL_INTEGER">,
				p.PersonID,
				p.locationid,
				ll.LookupID,
				<!--- selectionid field --->
				<cfif arguments.PersonIDs is "">
					min(ST.selectionid)
				<cfelse>
					null
				</cfif>,
				<!--- test and internal fields	 --->
				<cfif arguments.test>
					1, 0
				<cfelse >
					0,
					case when admin.organisationid is not null then 1 else 0 end
				</cfif>
				,null,null,null
			<cfif arguments.PersonIds is not "">
				FROM Person AS p
						INNER JOIN
					LookupList AS ll ON ll.LookupID  IN ( <cf_queryparam value="#comtypelst#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						LEFT JOIN
					vAdminCompany admin on admin.organisationid = p.organisationid
				WHERE p.PersonID  IN ( <cf_queryparam value="#arguments.PersonIds#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )

			<cfelse>
				FROM
					commselection cs
						inner join
					SelectionTag AS st on cs.selectionid = st.selectionid
						INNER JOIN
					Person AS p
						ON st.EntityID = p.PersonID
						INNER join
					 Location AS l
						on p.LocationID = l.LocationID
					INNER JOIN
						LookupList AS ll ON ll.LookupID  IN ( <cf_queryparam value="#comtypelst#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						LEFT JOIN
					vAdminCompany admin on admin.organisationid = p.organisationid


				WHERE
					cs.commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="CF_SQL_INTEGER" >

		          AND cs.selectionid in (<cfif result.selectionIDs is not ""><cf_queryparam value="#result.SelectionIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"><cfelse>0</cfif>)   <!--- WAB 2013 Slightly nasty construction here. Situation when, perhaps, both the selections and the whole communication have been scheduled, the selections earlier than communication.  When time comes for the whole communication to be sent there is nothing to go and result.selectionIDs is blank. Use 0 and everything will be fine! --->


				  AND st.Status > 0
				  AND st.EntityID = p.PersonID
				  AND p.LocationID = l.LocationID
				  AND ((1=1 #countryRightsFilter.whereclause#) OR cs.commSelectLID  in ( <cf_queryparam value="#communications.constants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
				  <!--- AND (l.CountryID  IN ( <cf_queryparam value="#getOwnerCountryIDList()#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) or cs.commSelectLID  in ( <cf_queryparam value="#communications.constants.approverSelectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) --->
				  AND p.Active <> 0
  				  AND p.personid not in (select entityid from selectiontag ste inner join commselection cse on cse.selectionid = ste.selectionid where cse.commid = <cf_queryparam value="#this.commid#" CFSQLTYPE="CF_SQL_INTEGER" > and commselectlid =  <cf_queryparam value="#communications.constants.excludeSelectLID#" CFSQLTYPE="CF_SQL_INTEGER"  >)

			</cfif>
			<cfif arguments.CommFormLID is not "">
				<!--- this can be set to just send email or fax --->
				AND ll.LookupID  IN ( <cf_queryparam value="#arguments.CommFormLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			group by p.personid, p.locationid, p.organisationid, <cfif arguments.PersonIds is ""> cs.commSelectLID,</cfif> ll.lookupid, admin.organisationid

			select @@rowCount as totalPeople
			set nocount OFF
		</CFQUERY>

<cfset debugLog("CreateTempTable",createTempTableResult)>

		<cfset result.TotalPeople = createTempTable.TotalPeople>

		<cfreturn result>
	</cffunction>



	<cffunction access="public" name="populateCommDetailFromTemporaryTable" hint=""  output="false">
		<cfargument NAME="resend" default = "false">

			<cfset var insertCommDetailRecords = '' />

	 	 	<CFQUERY NAME="insertCommDetailRecords" DATASOURCE="#application.SiteDataSource#">
			set nocount ON
			declare @peopleAdded int, @peopleToResend int, @peopleAlreadySent int, @peopleToSend int
			select @peopleToResend = 0
			<!---
				WAB 2011/11/03 LID 8105
				problem with this insert when the comm is very big and/or the commdetail table is very big
				especially happens when processing a large comm for the second time (perhaps if timed out first time around)
				Could probably be fixed with better indexing on commdetail,
				But have managed to improve performance by putting the existing commdetail records into a temp table
				and then running the left join on between the two temporary tables
				does have slight possibility of duplicate records if this query is run twice concurrently

			select
			commid,personid,commtypeid,test
			into #tempTableName#_cd
			from commdetail
			where CommID =  <cf_queryparam value="#attributes.CommID#" CFSQLTYPE="CF_SQL_INTEGER" >
			--->

			INSERT INTO commdetail (CommID, PersonID, locationid,Feedback, ContactDetails, commTypeId, commReasonId,SelectionID,test,internal)
			select
				t.CommID, t.PersonID, t.locationid,'', null, t.commTypeId, 0,t.SelectionID,t.test,t.internal
			 from
			#tempTableName#  t /* records which need to be present */
				 left join
			commdetail cd
							on cd.commid =t.commid and cd.personid = t.personid and t.commtypeid = cd.commtypeid and cd.test = T.TEST
			  WHERE
			  	t.commid = #this.commid# <!--- not strictly necessary, but makes query 200 times faster (presumably because index can be used) --->
			  	AND CD.commid is null <!--- only where there isn't an exisitng entry	 --->

			select @peopleAdded = @@rowcount

		<cfif resend>
			Update commdetail
				set feedback = '', commStatusID = 0
			from
				#tempTableName#  t /* records which need to be present */
					 left join
				commdetail cd 	on cd.commid =t.commid and cd.personid = t.personid and t.commtypeid = cd.commtypeid and cd.test = T.TEST
			where
				isnull(cd.feedback,'') <> ''

			select @peopleToResend = @@rowcount
		</cfif>

		-- we are going to return a temporary table containing all the people who are to be sent to (with correct commdetailid)
		-- so update commdetailid (where feedback is blank)and remove all the rest

		update #tempTableName#
		  set commdetailID = cd.commdetailid
			from
			#tempTableName#  t
				 inner  join
			commdetail cd /* copy of existing commdetail */
							on cd.commid =t.commid and cd.personid = t.personid and t.commtypeid = cd.commtypeid and cd.test = T.TEST
			where
			  	t.commid = #this.commid# <!--- not strictly necessary, but makes query 200 times faster (presumably because index can be used) --->
				AND isnull(cd.feedback,'') = ''
		select @peopleToSend = @@rowcount


		delete from #tempTableName# where commdetailid is null
		select @peopleAlreadySent = @@rowcount

		select @peopleAlreadySent as peopleAlreadySent, @peopleToSend as peopleToSend, @peopleToResend as peopleToResend, @peopleAdded as peopleAdded
		set nocount OFF
		</CFQUERY>

		<cfreturn insertCommDetailRecords>
	</cffunction>


	<cffunction name="setPreferencesEtc" output="false">

	 	<cfset var NoCommsForm2Loc_exists = '' />
		<cfset var NoCommsForm2Pers_exists = '' />
		<cfset var NoCommsForm2Loc_Join = '' />
		<cfset var NoCommsForm2Pers_Join = '' />
		<cfset var UnsubscribeFlag = '' />
		<cfset var optInFlag = '' />
		<cfset var setCommDetail = '' />
		<cfset var updateCommdetail = '' />
		<cfset var updateCommDetailResult = '' />

		<cfset NoCommsForm2Loc_exists = false>
		<cfset NoCommsForm2Pers_exists = false>
		<cfif application.com.flag.doesFlagExist('NoCommsForm2Loc')>
			<cfset NoCommsForm2Loc_Join = application.com.flag.getJoinInfoForFlag (flagid = 'NoCommsForm2Loc',entityTableAlias = "cd")>
			<cfset NoCommsForm2Loc_exists = true>
			</cfif>
		<cfif application.com.flag.doesFlagExist('NoCommsForm2Pers')>
			<cfset NoCommsForm2Pers_Join = application.com.flag.getJoinInfoForFlag (flagid = 'NoCommsForm2Pers',entityTableAlias = "cd")>
			<cfset NoCommsForm2Pers_exists = true>
		</cfif>

				<cfif NoCommsForm2Loc_exists OR NoCommsForm2Pers_exists>
					<!--- WAB: check for any no emails flags--->
					<CFQUERY NAME="setCommDetail" DATASOURCE="#application.SiteDataSource#">
						UPDATE #TempTableName#
						   SET Feedback = 'Not Sent: No Emails',
							   commStatusId = -12
						FROM #TempTableName# cd
							<cfif NoCommsForm2Loc_exists>
								#NoCommsForm2Loc_Join.join#
							</cfif>
							<cfif NoCommsForm2Pers_exists>
								#NoCommsForm2Pers_Join.join#
							</cfif>
						 WHERE
						  	CD.CommTypeId = 2 <!--- email --->
							AND
								(1=0
									<cfif NoCommsForm2Loc_exists>
										OR #NoCommsForm2Loc_Join.tableAlias#.flagid is not null
									</cfif>
									<cfif NoCommsForm2Pers_exists>
										OR #NoCommsForm2Pers_Join.tableAlias#.flagid is not null
									</cfif>
								)
						</CFQUERY>
				</cfif>




			<!--- ===========================================

				Check for anyone in this communication who is unsubscribed or excluded
				WAB 2005-10-05 changed order so that excluded is done before unsubscribed
		      ===========================================
			  WAB 2009/12/01  LID 2893 status column not being taken into account in the excluded selection

			  --->

						<cfset applyFilter(	queryStringJoin = " inner join commselection as cs ON cs.commid = #this.commid# and cs.commselectLID = #communications.constants.excludeSelectLID#
																inner join selectiontag as st ON cs.selectionid = st.selectionid AND cd.personid = st.entityid AND st.status <> 0",
							statusid =	-10,
							feedback = "Excluded"
									)>



				<!--- Is there an unsubscribe flag for this commtype type --->
					<cfset UnsubscribeFlag="NoCommsType"&this.commtypelid>
					<CFIF not application.com.flag.doesFlagExist(UnsubscribeFlag)>
						<cfset UnsubscribeFlag="NoCommsTypeOther">
					</CFIF>

					<cfset applyFilter(
							queryStringJoin = " inner join vbooleanflagdata bfd ON bfd.entityid = CD.PersonID AND bfd.flagtextid IN ('NoCommsTypeAll','#UnsubscribeFlag#')" ,
							statusid =	-11,
							feedback = "Unsubscribed")>


					<!---
					WAB 2010/04/29 P-TND099
					Is there an opt-in flag for this commtype type
					If so we have to remove everyone from the communication who has not got the opt-in flag set

					An Opt In flag has a flagTextID of the form OptInCommsType#commtypelid#
					People who are not opted in are given status -16
					WAB 2014-01-27 CASE 438584 Status ID was being set to -11 not -16 - must have been a cut and paste job

					WAB 2014-06-18 CASE 440177/440485 Add support for an OptInCommsTypeALL flag
					--->
					<cfset optInFlag = application.com.flag.getFlagStructure("OptInCommsType#this.commtypelid#")>

					<CFIF optInFlag.isOK>

						<cfset applyFilterArgs = {	queryStringJoin = " left join vbooleanflagdata bfd ON bfd.entityid = CD.PersonID AND bfd.flagid = #OptInFlag.flagid#" ,
							queryStringwhere = " and bfd.flagid is null" ,
							statusid =	-16,
							feedback = "NotOptedIn",
							feedbackCF = "Not Opted In"
									}>

						<!--- WAB 2014-06-18 CASE 440177 Add support for an OptInCommsTypeALL flag
							  WAB 2014-10-29 Fixed bug - was using += where should have been &=
						--->
						<cfset optInAllFlag = application.com.flag.getFlagStructure("OptInCommsTypeALL")>

						<CFIF optInAllFlag.isOK>
							<cfset applyFilterArgs.queryStringJoin &= " left join vbooleanflagdata bfdAll ON bfdAll.entityid = CD.PersonID AND bfdAll.flagid = #OptInAllFlag.flagid#">
							<cfset applyFilterArgs.queryStringwhere &= " and bfdAll.flagid is null" >
						</CFIF>

						<cfset applyFilter (argumentCollection = applyFilterArgs)>

					</cfif>




<!--- exclude any people where we have got more than x bouncebacks --->

						<cfset applyFilter(	queryStringJoin = "" ,
							queryStringwhere = " and person.emailstatus > #getCommLimits().numberOfFailedEmailsUntilStopSending#" ,
							statusid =	-14,
							feedback = "Too many previous bouncebacks"
									)>



<!--- 2009/01/14 WAB deal with blank in obviously invalid emails, quicker to do here than in main loop --->
						<cfset applyFilter(	queryStringJoin = "" ,
							queryStringwhere = " and commTypeID = 2 and (person.email = '' or charindex('@',person.email) = 0)",
							statusid =	-2,
							feedback = "Not Sent: Invalid Email"
									)>


						<cfset applyFilter(	queryStringJoin = "" ,
							queryStringJoin = " inner join organisation on organisation.organisationid = person.organisationid ",
							queryStringwhere = " and (person.active = 0 OR organisation.active = 0)",
							statusid =	-17,
							feedback = "Not Sent: Inactive"
									)>

					<CFQUERY NAME="updateCommdetail" DATASOURCE="#application.SiteDataSource#" result="updateCommDetailResult" >
						UPDATE 	commdetail
						   SET 	Feedback= tcd.feedback,
							   	commStatusId = tcd.commstatusid,
  							   	commTypeId = tcd.commTypeId
						FROM 	commdetail cd inner join #TempTableName# tCD on cd.commdetailid = tcd.commdetailid
						where isnull(cd.feedback,'') = ''
					</CFQUERY>

					<cfset debugLog ("updateCommDetailResult",updateCommDetailResult)>

	</cffunction>


	<cffunction name="applyFilter" output="false">
		<cfargument name="queryStringJoin" default="">
		<cfargument name="queryStringWHERE" default="">
		<cfargument name="Feedback" required=true>
		<cfargument name="CFfeedback" default="#arguments.feedback#">
		<cfargument name="statusID">

		<cfset var thisItemResult = '' />
		<cfset var updateAndGetItems = '' />
		<cfset var filterResult = '' />

		<cfif queryStringJoin is "" and queryStringwhere is "" >
			<cfoutput>ApplyFilter - you must provide a filter</cfoutput>
			<CF_ABORT>
		</cfif>
						<!--- WAB 2013-10-17 added nvarchar(max) - got an error here, might as well future proof!  --->
						<CFQUERY NAME="updateAndGetItems" DATASOURCE="#application.SiteDataSource#" result="filterResult">
							declare @temp AS table (commdetailid int, personid int, name nvarchar(max), email nvarchar(max))

							insert into @temp
							SELECT
								distinct cd.commdetailid,CD.personid, isnull(person.firstname,'') + ' ' + isNull(person.lastname,''), person.email
							FROM
								#TempTableName#	cd
								inner join person on person.personid = CD.personid
								#preserveSingleQuotes(queryStringJOIN)#
							WHERE
								isnull(feedback,'') = ''
								#preserveSingleQuotes(queryStringWHERE)#

							UPDATE 	#TempTableName#
							   SET 	Feedback =  <cf_queryparam value="#feedback#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								   	commStatusId =  <cf_queryparam value="#statusID#" CFSQLTYPE="cf_sql_integer" >
							FROM 	#TempTableName# CD inner join @temp temp on cd.commdetailid = temp.commdetailid

							select * from @temp
						</CFQUERY>

						<cfloop query = "updateAndGetItems">
							<cfset thisItemResult = {name = name,  personid = personid,OK = false,address = email,feedback = cffeedback}>
							<cfset variables.sendResultObject.addItem (thisItemResult)>
						</cfloop>

			<cfset debugLog ("Filter",filterResult)>


	</cffunction>





	<cffunction access="public" name="getApprovalDetails" hint=""  output="false">
		<cfargument NAME="PersonID" default="#request.relayCurrentUser.personid#">

			<cfif not structKeyExists (variables.data,"approvalDetails")>
				<!--- TBD WAB 2013 what is this PersonID used for,  !! --->
				<CFIF structKeyExists(arguments,"personid")>
					<!--- if a personid (the user) has been passed in then we pass it on --->
					<cfset variables.data.approvalDetails = getApprovalDetails_(personid = arguments.personid)>
				<cfelse>
					<cfset variables.data.approvalDetails = getApprovalDetails_()>
				</CFIF>

			</cfif>

			<cfreturn variables.data.approvalDetails>

	</cffunction>


	<cffunction access="public" name="getApprovalDetails_" hint="" returnType = "struct" output="false">
			<cfargument name="personid" type="numeric" required="false">

			<cfset var ownerCountryIDList = "">
			<cfset var getApprovers = "">
			<cfset var checkForApproverDisapproval = "">
			<cfset var CommunicationCountryIDList = "">
			<cfset var getCommCountries = "">
			<cfset var LiveCountries = "">
			<cfset var getCountriesAndGatekeepers = "">
			<cfset var isCommDisApproved = "">
			<cfset var isCommApproved = "">
			<cfset var hasApprovalBeenSought = "">
			<cfset var updateCommunication = "">
			<cfset var CommunicationGateKeeperCountryFlagID = "">
			<cfset var CommGateKeeperRoleDescriptionFlagID = "">
			<cfset var result = structNew()>
			<cfset var NotApprovedCountries = "">
			<cfset var testEditedSinceApproval = "">

			<cfset var approvalSettings = getApprovalSettings()>
			<cfset var approverSelection = 0>
			<cfset var tempApproverSelection = 0>
			<cfset var firstRun = true>
			<cfset var field = "">
			<cfset var i = "">


			<cfset result.requiresApproval = true>
			<cfset result.approvalProcessType = "">
			<cfset result.isApproved = false>
			<cfset result.isDisapproved = false>
			<cfset result.isApprovedToSendNow = false>
			<cfset result.ApprovedSendDate = "">
			<cfset result.sentForApproval = false>
			<cfset result.isDisapprovedbyApprovers = false>
			<cfset result.ApprovedCountryList = "">
			<cfset result.editedSinceApproval = false>

			<cfif approvalSettings.useApprovers>
				<!--- NYB P-LEN024 2011-07-28 removed as it doesn't seem to be working with the new Settings setup
				<CFQUERY name="getApprovers" datasource="#application.sitedatasource#">
				select distinct p.firstname + ' ' + p.lastname as name, p.personid,eA.ApprovalStatus,
						case when eA.ApprovalStatus =	-1 then 'Disapproved' when eA.ApprovalStatus =	1 then 'Approved' when eA.ApprovalRequestedDate is not null  then 'Sent' else 'Not Sent'  end as ApprovalStatusMessage
				from
				person as p
					inner join
				selectiontag st on st.entityid = p.personid
					inner join
				commselection cs on cs.selectionid = st.selectionid and cs.commid = #this.commid# and cs.commSelectLID = 11 and st.status <> 0
					left join
				entityApproval eA on eA.entityid = #this.commid# and eA.entityTypeID = #application.entityTypeID['communication']# and eA.personID = p.PersonID
				</cfquery>
				--->

				<cfif len(approvalSettings.DefaultApproverSelectionID) gt 0>
					<cfset approverSelection = approvalSettings.DefaultApproverSelectionID>
				</cfif>
				<cfset tempApproverSelection = application.com.settings.getSetting(variablename = "communications.defaultselections.11",reloadOnNull = false)>
				<cfif len(tempApproverSelection) gt 0>
					<cfset approverSelection = tempApproverSelection>
				</cfif>
				<cfset firstRun = "true">
				<cfloop index="i" list="#getCountryIDList()#">
					<cfset tempApproverSelection = application.com.settings.getSetting(variablename = "communications.defaultcountryselections.#i#.11",reloadOnNull = false)>
					<cfif len(tempApproverSelection) gt 0>
						<cfif firstRun>
							<cfset approverSelection = tempApproverSelection>
						<cfelse>
							<cfset approverSelection = approverSelection&","&tempApproverSelection>
						</cfif>
					</cfif>
				</cfloop>

				<CFQUERY name="getApprovers" datasource="#application.sitedatasource#">
				select distinct p.firstname + ' ' + p.lastname as name, p.personid,eA.ApprovalStatus,
						case when eA.ApprovalStatus =	-1 then 'Disapproved' when eA.ApprovalStatus =	1 then 'Approved' when eA.ApprovalRequestedDate is not null  then 'Sent' else 'Not Sent'  end as ApprovalStatusMessage
				from
					person as p with(nolock)
						inner join
					selectiontag st with(nolock) on st.entityid = p.personid AND st.selectionid  IN ( <cf_queryparam value="#approverSelection#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						left join
					entityApproval eA on eA.entityid = #this.commid# and eA.entityTypeID =  <cf_queryparam value="#application.entityTypeID['communication']#" CFSQLTYPE="CF_SQL_INTEGER" >  and eA.personID = p.PersonID
				</cfquery>

				<cfset result.ApproversQuery = getApprovers>

				<cfquery name="checkForApproverDisapproval" dbtype="query">
				select 1 from getApprovers where approvalstatus = -1
				</cfquery>

				<cfif checkForApproverDisapproval.recordcount is not 0>
					<cfset result.isDisapprovedbyApprovers = true>
				</cfif>

				<cfif isDefined("personid")>
					<cfif listfindnocase(valuelist(getApprovers.personid), personid) is not 0 >
						<cfset result.userIsApprover = true>
					<cfelse>
						<cfset result.userIsApprover = false>
					</cfif>
				</cfif>
			</cfif>

<!--- 					<!--- first get list of countries in communication --->
					<!--- needs to come from commdetail and selections which haven't been added to commdetail
					don't count tests or internal
					selections need to be filtered by the owners countries
					--->
					<cfset CommunicationCountryIDList = getCommunicationCountryIDList(commid, communicationDetails.ownersCountryIDList)>
 --->
					<cfquery name="getCommCountries" datasource="#application.siteDataSource#">
					select distinct c.countryid, c.countrydescription
					from
						country c
						where
						<cfif getCountryIDList() is not "">
						c.countryid  in ( <cf_queryparam value="#getCountryIDList()#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						<cfelse>
						1 = 0
						</cfif>

					</cfquery>

					<cfset result.countryquery = getCommCountries>

					<cfif approvalSettings.useApprovalProcess>

						<cfif approvalSettings.approvalProcessType is "Gatekeeper">
							<cfset result.approvalProcessType = "gatekeeper">
							<cfset livecountries = 	getCommCountries>  <!--- this aliases the query, it will be replaced with another query below if LiveCountryIDList is defined--->
							<cfif approvalSettings.gatekeeper.LiveCountryIDList is not "" >
								<!--- is this communication going to countries where this approval process is used
								--->
								<cfif getCommCountries.recordcount is not 0 >
									<cfquery name="LiveCountries"  dbtype="query">
									select * from getCommCountries where countryid in (#approvalSettings.gatekeeper.LiveCountryIDList#)
									</cfquery>
									<cfif LiveCountries.recordcount is 0>
										<cfset result.requiresApproval = false>
									</cfif>
								<cfelse>
									<!--- aren't any selections set up yet, so we'll assume for the time being that it is going to the creators country--->
									<cfif listfindnocase(approvalSettings.gatekeeper.LiveCountryIDList,this.creatorCountryID) is 0>
										<cfset result.requiresApproval = false>
									</cfif>
								</cfif>
							</cfif>

							<!--- check whether there are any commTypes which don't need approval --->
							<cfif listFindNoCase(approvalSettings.gatekeeper.commtypesNotRequiringApproval,this.commTypeLID) is not 0>
									<cfset result.requiresApproval = false>
							</cfif>

							<cfif result.requiresApproval>

								<cfset result.isApprovedByGatekeeper = false>
								<cfset result.isPartlyApprovedByGatekeeper = false>
								<cfset result.isDisApprovedByGatekeeper = false>
								<cfset result.userIsGatekeeper = false>

								<cfset CommunicationGateKeeperCountryFlagID = application.com.flag.getFlagStructure('CommunicationGateKeeperCountry').flagid>
								<cfset CommGateKeeperRoleDescriptionFlagID = application.com.flag.getFlagStructure('CommGateKeeperRoleDescription').flagid>

									<!--- brings back query with list of countries, approvers and whether or not they have approved the communication or not --->
									<cfquery name="getCountriesAndGatekeepers" datasource="#application.siteDataSource#">
									select
										c.countrydescription,
										c.countryid ,
										e.PersonID,
										CASE when e.PersonID is not null then e.FirstName + ' ' + e.lastname else 'No Gatekeeper Set ' end as Approvername ,
										e.email,
										tfd.data as RoleDescription,
										case when ApprovalStatus =	1 then 1 else 0 end as approved	,
										case when ApprovalStatus =	1 then eA.ApprovalStatusLastUpdated else null end as ApprovedDate	,
										case when ApprovalStatus =	-1 then 1 else 0 end as disapproved	,
										case when approvalRequestedDate is not null then 1 else 0 end as sentForApproval	,
										eA.approvalRequestedDate,
										eA.approvedReleaseDate,
										eA.approvalStatusLastUpdated,
										eA.comment,
										case when ApprovalStatus =  1 and ApprovalStatusLastUpdated < #createODBCDateTime(this.contentlastupdated)# then 1 else 0 end as EditedSinceApproval

									from
										country c
											<!--- WAB 2012/02/04 changed to left join so could pick up countries where there was no gatekeeper set--->
											left join (
												person e
													inner join
												IntegerMultipleflagdata fd
													on fd.entityid = e.PersonID and fd.flagId =  <cf_queryparam value="#CommunicationGateKeeperCountryFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
													left join
												textflagdata tfd on tfd.entityid = e.PersonID and tfd.flagId =  <cf_queryparam value="#CommGateKeeperRoleDescriptionFlagid#" CFSQLTYPE="CF_SQL_INTEGER" >
												) on c.countryid = fd.data
											left join
											entityApproval eA on eA.entityid = #this.commid# and eA.entityTypeID =  <cf_queryparam value="#application.entityTypeID['communication']#" CFSQLTYPE="CF_SQL_INTEGER" >  and eA.personID = e.PersonID
									where
										c.countryid in (<cfif getCommCountries.recordcount is not 0 >#valuelist(getCommCountries.countryid)#<cfelse>#this.creatorCountryID#</cfif>)
										<cfif approvalSettings.gatekeeper.LiveCountryIDList is not "" >and c.countryid  in ( <cf_queryparam value="#approvalSettings.gatekeeper.LiveCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) </cfif>
									order by  lastname asc, countryDescription asc

									</cfquery>

									<cfset result.Gatekeeperquery  = getCountriesAndGatekeepers>

									<cfquery name="result.GatekeepersDistinct"  dbtype="query">
									select distinct approverName, email, personid
									from 	getCountriesAndGatekeepers
									where personid is not null
									</cfquery>


								<cfif isDefined("personid") and listfindnocase(valuelist(getCountriesAndGatekeepers.personid), personid) is not 0 >
										<cfset result.userIsGatekeeper = true>
								</cfif>


									<!--- If the comm has been disapproved by any gatekeeper we must disapprove the whole thing --->
									<cfquery name="isCommDisApproved"  dbtype="query">
									select * from getCountriesAndGatekeepers where disapproved = 1
									</cfquery>

									<cfif isCommDisapproved.recordcount is not 0>
										<!--- definitelydisapproved --->
										<cfset result.isDisApprovedByGatekeeper = true>
									<cfelse>
										<!--- now works out whether comm has been approved by at least one gatekeeper from each country since the last edit--->
										<cfquery name="result.ApprovedCountries"  dbtype="query">
										select countryid, countryDescription, max(approvedReleaseDate) as maxapprovedDate
										from getCountriesAndGatekeepers
										where
												approved = 1
											and approvedReleaseDate is not null
											and  EditedSinceApproval = 0
										group by countryid, countryDescription
										order by approvedDate Desc
										</cfquery>

										<cfset result.approvedCountryList = valueList(result.ApprovedCountries.countryID)>

										<cfif result.ApprovedCountries.recordcount is livecountries.recordcount and livecountries.recordcount is not 0>
												<cfset result.isApprovedByGatekeeper = true>
												<cfset result.ApprovedSenddate = result.ApprovedCountries.maxapprovedDate>
										<cfelse>
											<!--- not approved, get list of not approved countries --->
											<cfquery name="NotApprovedCountries"  dbtype="query">
												select distinct countryid, countryDescription from getCountriesAndGatekeepers
												<cfif result.ApprovedCountries.recordCount is not 0>
												where countryid not in (#valueList(result.ApprovedCountries.countryID)#)
												</cfif>
											</cfquery>

											<cfset result.NotApprovedCountries = NotApprovedCountries>
											<cfif result.ApprovedCountries.recordCount is not 0  and result.NotApprovedCountries.recordCount is not 0>
												<cfset result.isPartlyApprovedByGatekeeper	= true	>
											</cfif>


											<cfif NotApprovedCountries.recordCount>
												<!--- Test for editedSinceApproval
													work out all the countries where there is a gatekeeper with 'editedsinceapproval'
													If the number of countries is the same as the not approved counties then
												--->
											<cfquery name="testEditedSinceApproval"  dbtype="query">
												select distinct countryid
											from getCountriesAndGatekeepers
											where
													approved = 1
												and approvedReleaseDate is not null
												and  EditedSinceApproval = 1
												and countryID in (#valueList (NotApprovedCountries.countryid)#)
											group by countryid
											</cfquery>

											<cfif testEditedSinceApproval.recordCount is result.NotApprovedCountries.recordCount>
												<cfset result.editedSinceApproval = true>
											</cfif>
											</cfif>


										</cfif>

									</cfif>

									<!--- if gatekeepers approved and no approvers have disapproved then whole thing is approved--->
									<cfif result.isApprovedByGatekeeper and not result.isDisapprovedbyApprovers>
											<cfset result.isApproved = true>
									<cfelseif result.isDisApprovedByGatekeeper or result.isDisapprovedbyApprovers>
											<cfset result.isDisApproved = true>
									</cfif>


									<cfquery name="hasApprovalBeenSought"  dbtype="query">
									select 1 from getCountriesAndGatekeepers where sentForApproval = 1
									</cfquery>

									<cfif hasApprovalBeenSought.recordCount is not 0>
											<cfset result.sentForApproval = true>
									</cfif>

							<cfelse>
							 	<!--- not requiresApproval --->
							</cfif>

						<cfelse>
							<!--- Unknown approval process --->
							<cfset result.isApproved = false>
						</cfif>

						<cfif result.isApproved and now() gt result.ApprovedSenddate >
							<cfset result.isApprovedToSendNow = true>
						</cfif>

					<cfelse>
						<!--- not using approval Process so return requiresApproval  = 1 --->
						<cfset result.requiresApproval = false>

					</cfif>


					<!--- update lastKnown fields if necessary
					null - approval not required
					0 - not approved
					1 - approved

					--->

				<!--- START 2011-03-28 NYB - LHID5664 change: only change LastKnownisApproved from 1 to 0 if it hasn't already been sent, or due to be sent in the next 5 minutes
					WAB 2013-05-13, don't understand above,
								Removed condition:
										and not (result.isApprovedToSendNow gt DateAdd("n", 5, now())
									because result.isApprovedToSendNow is a boolean not a date
								Removed condition
										and not result.sentForApproval and not result.isApprovedToSendNow
				--->

				<cfif result.requiresApproval>
					<cfif (result.isapproved and this.lastKnownIsApproved is not 1)
						or (not result.isapproved and this.lastKnownIsApproved is not 0 )>

							<cfquery name="updateCommunication" datasource="#application.siteDataSource#">
							update communication set LastKnownisApproved =  <cf_queryparam value="#iif(result.isApproved,1,0)#" CFSQLTYPE="CF_SQL_bit" >  where commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
							</cfquery>
					</cfif>
				<cfelse>
						<cfif this.lastKnownIsApproved is not "">
							<cfquery name="updateCommunication" datasource="#application.siteDataSource#">
							update communication set LastKnownisApproved = null where commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
							</cfquery>
						</cfif>
				</cfif>

		<!--- END 2011-03-28 NYB --->
		<cfreturn result>

	</cffunction>



	<cffunction access="public" name="isApprovedToSendNow" hint=""  output="false">

		<cfset var approvaldetails = getApprovalDetails()>
		<cfset var result = {isOK = false}>

			<cfif not approvaldetails.requiresApproval or approvaldetails.isapprovedToSendNow >
				<cfset result.isOK = true>

			<cfelse>

					<cfif approvaldetails.ApprovedSendDate GT now()>
						<cfset result.Message = "Not approved for sending until #application.com.datefunctions.datetimeformat(approvaldetails.ApprovedSendDate)#">
						<cfset result.ErrorCode = "NotApprovedYet">
					<cfelseif approvaldetails.requiresApproval and not approvaldetails.isApproved>
						<cfset result.Message = "Not Approved">
						<cfset result.ErrorCode = "NotApproved">
					<cfelse>
						<cfset result.Message = "Unknown Reason">
						<cfset result.ErrorCode = "NotApprovedUnknownReason">
					</cfif>

			</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="isCountryApproved" hint=""  output="false">

		<cfset var checkIsCountryApproved = '' />
		<cfset var getCountries = '' />

		<cfset var result = {isOK = true}>
		<CFIF not attributes.Test and attributes.personids is not "">
		<!--- WAB 2005-12-13 need to check that communication is approved for the countries that these people are in before they are added to commdetail
			--->
		<CFQUERY NAME="getCountries" DATASOURCE="#application.SiteDataSource#">
		select distinct countryid
		from 	location l
					inner join
				person  p on l.locationid = p.locationid
					left join
				adminCompany v on p.organisationid = v.organisationid
		where
			personid  in ( <cf_queryparam value="#attributes.personids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement=true> )
			and v.organisationid is null
		</CFQUERY>

		<!--- TBD --->
		<cfif getCountries.recordCount is not 0>
			<cfset checkIsCountryApproved = communications.isCommApprovedToSendToTheseCountries (approvaldetails = communicationStructure.approval, countryids = valuelist(getCountries.countryid))>
			<cfif not checkIsCountryApproved.isApproved>
				<cfset result.isOK = false>
				<cfset result.errorMessage = "Not approved for this country">
			</cfif>
		</cfif>
	 </cfif>

	</cffunction>

<!--- TBD - is this used?
	<cffunction access="public" name="isApprovedToSendToTheseCountries" hint=""  output="false">
			<cfargument name="countryIDs" type="string" required="true">

			<cfset var result = structNew()>
			<cfset var checkCountries = "">
			<cfset var approvalSettings = application.com.settings.getSetting ("communications.approvals")>

			<cfset var approvalDetails = getApprovalDetails()>

			<cfif not approvalDetails.requiresApproval>

				<cfset result.isApproved = true>
				<cfset result.reason= "doesn't require approval">

			<cfelseif approvalDetails.requiresApproval and not approvalDetails.ISAPPROVED >
				<cfset result.isApproved = false>
				<cfset result.reason= "not approved to send">

			<cfelseif approvalDetails.requiresApproval and not approvalDetails.ISAPPROVEDTOSENDNOW >
				<cfset result.isApproved = false>
				<cfset result.reason= "not approved to send now">

			<cfelse>
				<cfquery name="checkCountries" datasource="#application.siteDataSource#">
				select countryid from country
				where countryid  in ( <cf_queryparam value="#countryids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				and (countryid  in ( <cf_queryparam value="#approvalDetails.approvedCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						<cfif approvalSettings.useApprovalProcess>
							<cfif approvalSettings.approvalProcessType is "Gatekeeper">
								<cfif approvalSettings.Gatekeeper.LiveCountryIDList is not "" >
									or countryid  not in ( <cf_queryparam value="#approvalSettings.Gatekeeper.LiveCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
								</cfif>
							</cfif>
						</cfif>
					)
				</cfquery>

				<cfif checkCountries.recordcount is listlen(countryids)>
					<cfset result.isApproved = true>
					<cfset result.reason= "approved for these countries">
				<cfelse>
					<cfset result.isApproved = false>
					<cfset result.reason= "not approved for these countries">
				</cfif>
			</cfif>

			<cfreturn result>

	</cffunction>
--->


	<cffunction access="public" name="prepareAndValidateContent" hint=""  output="false">
		<cfargument name="test" type="boolean" default="false">
		<cfargument name="showLinkBackToCommunication" type="boolean" default="false">
		<cfargument name="footerText" type="string" default="">
		<cfargument NAME="subjectPrefix" default = "">
		<cfargument name="doEmail" default = "true">
		<cfargument name="doMessage" default = "#this.sendMessage#">


		<cfset var result = {isOK = true}>

 		<!--- TBD this should call getPrepared content - which needs to be adjusted to return correct isOK, currently we are not validating the notification merge --->
		<cfset var prepareEmailForMergeResult = prepareEmailForMerge(test=arguments.test,showLinkBackToCommunication = arguments.showLinkBackToCommunication,footerText=arguments.footertext,subjectPrefix = subjectPrefix)>
		<cfset var prepareMessageForMergeResult = prepareMessageForMerge(argumentCollection = arguments )>

		<cfif not prepareEmailForMergeResult.isOK>
				<cfset result.isOK = false>
				<cfset result.errorMessage = prepareEmailForMergeResult.message>
		<cfelse>
			<cfset result = validatePreparedContent (doEmail = doEmail,doMessage=doMessage)>
		</cfif>


		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="validatePreparedContent" hint=""  output="false">
		<cfargument name="test" type="boolean" default="false">
		<cfargument name="doEmail" default = "true">
		<cfargument name="doMessage" default = "#this.sendMessage#">

		<cfset var result = {isOK = true}>
		<cfset var errorItem = "">
		<cfset var Item = "">
		<cfset var itemResult = "">
		<cfset var itemsToCheck = "">
		<cfset var key = "">

				<cfset result = getMergedContentForSinglePerson (evaluateAllOptions = true,test=arguments.test,doemail = doemail, doMessage = doMessage)>
				<cfset result.errorMessage = "">
				<!---
				<cfoutput>
					This is the Preview, with all IF and ELSE options evaluated:<br />
					<PRE>#htmleditformat(testMergeResult.html.phraseText)#</PRE>
				</cfoutput>
				--->

				<!---
				check that a P number hasn't been embedded in the text,
				WAB 2013-10-11 moved this code from prepareEmailForMerge() where it was causing problems (because I didn't have a nice way of returning the error message)
				This is a check on the unmerged content, but I am doing it after the test merge because I need to append stuff to the error array which is created by getMergedContentForSinglePerson()
				Slightly nasty, if it turns out that there are more things like this to check then could work out a better place to fit it it
				--->
				<cfset preparedEmailContent = getPreparedContent().email>
				<cfloop list="html,text" index="key">
					<cfif reFindNoCase("p=[0-9]+-[0-9]+",preparedEmailContent[key]) is not 0 >
						<cfset errorItem = {message = "There is a Password (P) Number embedded in this email"}>
						<cfset arrayAppend(result.email[key].errorArray,errorItem)>
					</cfif>
				</cfloop>

				<cfif doEmail>
					<cfset itemsToCheck = listappend(itemsToCheck,"Email")>
				</cfif>
				<cfif doMessage>
					<cfset itemsToCheck = listappend(itemsToCheck,"Message")>
				</cfif>

				<cfloop list="#itemsToCheck#" index="item">
					<cfloop collection="#result[item]#" item="key">
						<cfset itemResult = result[item][key]>
						<cfif isStruct(itemResult) AND structKeyExists (itemResult,"errorArray") and arrayLen(itemResult.errorArray)>
							<cfset Result.isOK = false>
							<cfloop array = #itemResult.errorArray# index = "errorItem">
								<cfset Result.errorMessage = Result.errorMessage & "#errorItem.message#<BR>">
							</cfloop>
						</cfif>
					</cfloop>

				</cfloop>

				<cfif not result.isOK>
						<cfset Result.errorMessage = "Merge Field Error <BR>" & Result.errorMessage >
				</cfif>

				<cfif not StructKeyExists(this.metadata,"contentValidated") or (this.metadata.contentValidated NEQ  result.isOK)>
					<cfset setMetadata("contentValidated",result.isOK)>
				</cfif>

		<cfreturn result>

	</cffunction>


<cffunction name="prepareMessageForMerge" output="false">
	<cfset var messageContent = getContent().message>
	<cfset var result = {isOK = true}>

	<cfparam name ="variables.data.preparedContent" default="#structNew()#">

	<cfif not structKeyExists (variables.data.preparedContent,"message")>
		<cfset variables.data.preparedContent.message = messageContent>
	</cfif>

	<cfreturn result>

</cffunction>

	<!---
2011-08-26 NYB LHID7595 added concept of containsBodyTag and containsHTMLTag - and the code around them
 --->
<cffunction name="prepareEmailForMerge" output="false">
	<cfargument name="test" type="boolean" required="true">
	<cfargument name="showLinkBackToCommunication" type="boolean" default="false">
	<cfargument name="footerText" type="string" default="">
	<cfargument NAME="subjectPrefix" default = "">
	<cfargument name="evaluateAllOptions" type="boolean" default="false">

	<cfset var emailContent = getContent().email>
	<cfset var result = {isOK = true}>
	<cfset var baseURL = '' />
	<cfset var regExp = '' />
	<cfset var find = '' />
	<cfset var newhref = '' />
	<cfset var newAnchor = '' />
	<cfset var prepareHTMLresult = '' />
	<cfset var prepareTextResult = '' />
	<cfset var anchor = '' />
	<cfset var containsBodyTag = "true" />
	<cfset var containsHTMLTag = "true" />
	<cfset var regExpArguments = {} />
	<cfset var preparedEmailContent = "">
	<cfset var mergeObject = '' />
	<cfset var styleSheetInfo = '' />
	<cfset var styleBlock = '' />
	<cfset var linksArray = '' />
	<cfset var hrefsToIgnoreRegExp = '' />
	<cfset var newLink = '' />
	<cfset var link = '' />
	<cfset var type = '' />

	<cfparam name ="variables.data.preparedContent" default="#structNew()#">

	<cfif emailContent.contentExists>
		<cfif not structKeyExists (variables.data.preparedContent,"email")>
			<cfset preparedEmailContent = structCopy (emailContent)>

			<CFIF emailcontent.MultiPartMessage or emailcontent.SinglePartMessageFormat is "HTML" >
				<!--- WAB 2005-05-19
				BASE tag caused problems when trying to do links within a page.
				Therefore we need to get rid of the base tag and hard code the #request.currentSite.httpprotocol#etc. into all the relative pathed src tags
				--->
				<CFIF findnocase('SRC="/',preparedEmailContent.html) is not 0 >
					<!--- WAB 2012-01-30  CRM426370 'LinksToSiteURL' not being used to work out the base URL--->
					<cfset preparedEmailContent.html = replaceNoCase(preparedEmailContent.html,'SRC="/','SRC="#this.linktoSiteURLWithProtocol#/', "ALL")>
				</CFIF>

				<!--- CASE 431757: PJP 11/6/2012:  Added in site url for href's --->
				<CFIF findnocase('HREF="/',preparedEmailContent.html) is not 0 >
					<cfset preparedEmailContent.html = replaceNoCase(preparedEmailContent.html,'HREF="/','HREF="#this.linktoSiteURLWithProtocol#/', "ALL")>
				</CFIF>

				<cfset containsHTMLTag = REFindNoCase("(</HTML.*?>)",preparedEmailContent.html)>
				<cfset containsBodyTag = REFindNoCase("(</BODY.*?>)",preparedEmailContent.html)>


				<!--- if email to be tracked and the field is not already there, then add it
				TBD - need to add some way of not doing this if we are doing a preview
				 --->
				<cfif this.trackEmail is 1 and findnocase("[[TrackThisEmail]]",preparedEmailContent.html) is 0>
					<cfif containsBodyTag>
						<cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</BODY.*?>)","#application.emailMergeFields.trackThisEmail.insertCode#\1", "ONE")>	<!--- this should add the track this email link after the body tag and can handle anything in the body tag (except chevrons)  the ? makes the search non-greedy--->
					<cfelse>
						<cfif containsHTMLTag>
							<cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</HTML.*?>)","#application.emailMergeFields.trackThisEmail.insertCode#\1", "ONE")>	<!--- this should add the track this email link after the body tag and can handle anything in the body tag (except chevrons)  the ? makes the search non-greedy--->
						<cfelse>
							<cfset preparedEmailContent.html = preparedEmailContent.html&"#application.emailMergeFields.trackThisEmail.insertCode#">
						</cfif>
					</cfif>
					<!---  this seems to have been changed to put the code after then end of the body, it used to be after the opening body tag  --->
				</cfif>
				<!--- if it a test email (strictly an approval email) then add a link back to commcheckdetail--->
				<cfif showLinkBackToCommunication >
					<!--- WAB 2011/05/02 startingtemplate code changed <cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</BODY.*?>)","<BR><A href='[[linktoCommCheckDetail]]'>Click Here To See Details of This Communication</A>\0", "ONE")>	--->
					<cfif containsBodyTag>
						<cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</BODY.*?>)","<BR /><br /><A href='#communications.createStartingTemplateCommLink(startingTemplate='commdetailsHome',queryString='frmcommid=' & this.commid)#'>Click Here To See Details of This Communication</A>\0", "ONE")>
					<cfelse>
						<cfif containsHTMLTag>
							<cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</html.*?>)","<BR /><br /><A href='#communications.createStartingTemplateCommLink(startingTemplate='commdetailsHome',queryString='frmcommid=' & this.commid)#'>Click Here To See Details of This Communication</A>\0", "ONE")>
						<cfelse>
							<cfset preparedEmailContent.html = preparedEmailContent.html&"<BR /><br /><A href='#communications.createStartingTemplateCommLink(startingTemplate='commdetailsHome',queryString='frmcommid=' & this.commid)#'>Click Here To See Details of This Communication</A>">
						</cfif>
					</cfif>
					<!--- this should add the track this email link after the body tag and can handle anything in the body tag (except chevrons)  the ? makes the search non-greedy--->
				</cfif>
				<cfif footerText is not "" >
					<cfif containsBodyTag>
						<cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</BODY.*?>)","<BR />#arguments.footerText#\0", "ONE")>	<!--- this should add the track this email link after the body tag and can handle anything in the body tag (except chevrons)  the ? makes the search non-greedy--->
					<cfelse>
						<cfif containsHTMLTag>
							<cfset preparedEmailContent.html = REreplaceNoCase(preparedEmailContent.html,"(</html.*?>)","<BR />#arguments.footerText#\0", "ONE")>	<!--- this should add the track this email link after the body tag and can handle anything in the body tag (except chevrons)  the ? makes the search non-greedy--->
						<cfelse>
							<cfset preparedEmailContent.html = preparedEmailContent.html&"<BR />#arguments.footerText#">
						</cfif>
					</cfif>
				</cfif>

				<!---
				Stylesheets now inserted into content during editing so that a) we can use fullPage editing in ckeditor and b) a change to the style sheet will not affect the content after approval/sending
				--->

				<cfif isDefined("request.CommunicationSettings.dropHTMLTagInEMails") and  request.CommunicationSettings.dropHTMLTagInEMails>
					 <cfset preparedEmailContent.html = reReplaceNoCAse (preparedEmailContent.html,"<html>","", "ALL")>
					 <cfset preparedEmailContent.html = reReplaceNoCAse (preparedEmailContent.html,"</html>","", "ALL")>
				</cfif>

				<!--- replace all links where track = true with a [[trackedlink()]] merge field
					WAB 2013-04-30 changed to support href in area tags.  Uses new getArrayOfLinksInContent function rather than my previous rather complicated custom regExp
					WAN 2013-07-04 added a call to add omniture tracking string to the URL
				--->

				<cfset linksArray = getArrayOfLinksInContent(preparedEmailContent.html)>

				<cfset hrefsToIgnoreRegExp = "\A\W*\Z|\A##|\A\[\[">
				<cfloop array="#linksArray#" index="link">
					<cfif structKeyExists (link.attributes,"track")>
						<cfset newLink = link.string>
						<!--- WAB 2012-03-08 Lenovo, problems in this area,  don't add tracking if href is #, or if it is a relayLink of some sort (starting with [[ ) --->
						<cfif link.attributes.track and (structKeyExists (link.attributes,"href") and link.attributes.href IS NOT "" AND refindnocase (hrefsToIgnoreRegExp,link.attributes.href) is 0 )> <!--- LID 8103 WAB added anchor.href is not "", otherwise replace below falls over.  WAB 2012-03-03 don't add tracking to lonks which are already generated by merge fields since will already be tracked --->
							<cfset newHref = link.attributes.href>
							<cfif useOmnitureTracking()>
								<cfset newHref = addOmnitureMergeStringToURL_NoEvaluation (newHref)>
							</cfif>
							<!--- if track is true then replace href with a [[trackedLink]] --->
							<cfset newhref = "[[TrackedLink('#newHref#')]]">
							<cfset newLink = replacenocase (newLink,link.attributes.href,newHref)>
						</cfif>
						<!--- get rid of track attribute whether true or false --->
						<cfset newLink = reReplacenocase (newLink,'track=".*?"','')>
						<cfset preparedEmailContent.html = replace(preparedEmailContent.html,link.string,newLink,"ONCE")>
					</cfif>
				</cfloop>


			</CFIF>

			<cfset preparedEmailContent.suppressDuplicateEmails = true>

			<cfloop list="Text,HTML" index="type">
				<!--- These are specifically to deal with an issue where these merge fields are put into an email in the wrong form - stems from templates created a long time ago when we released some bad code--->
				<cfset preparedEmailContent[type] = reReplaceNoCase(preparedEmailContent[type],"UNSUBSCRIBE(THISTYPE)?\(\)","UNSUBSCRIBE\1","ALL")>

				<!--- does content contain merge fields, if so we suppressDuplicateEmails --->
				<cfif FindNoCase("[[",preparedEmailContent[type]) is not 0 >
					<cfset preparedEmailContent.suppressDuplicateEmails = false>
				</cfif>

			</cfloop>

			<cfset emailcontent.Size = (len(preparedEmailContent.text) + len (preparedEmailContent.html))/1000>  <!--- KB --->

			<!--- WAB 2012-03-14 save as temporary files for speedy merging!  Code to convert create is in the mergeObject file--->
			<cfset mergeObject = application.com.relayTranslations.getMergeObject()>

			<cfset preparedEmailContent.Text_MergeFile = mergeObject.createMergeFile(phraseText = preparedEmailContent.text,fileNameRoot="Comm_#this.commid#_Text",charset=this.CharacterSet)>
			<cfset preparedEmailContent.HTML_MergeFile = mergeObject.createMergeFile(phraseText = preparedEmailContent.html,fileNameRoot="Comm_#this.commid#_HTML",charset=this.CharacterSet)>

			<cfset variables.data.preparedContent.email = preparedEmailContent>

			<!--- WAB TBD --->
			<cfset getMergeFields()>

			<!--- Prepend the subject prefix if required --->
			<cfset preparedEmailContent.subject = application.com.relaytranslations.translateString(arguments.SubjectPrefix) & this.Title>

			<cfif this.flagID is not "">
				<cfset preparedEmailContent.subject = preparedEmailContent.subject & " - Registration ID [[eventRegistrationID]]">
			</cfif>

		</cfif>
	<cfelse><!--- content doesn't exist --->
		<cfset result.isOK= false>
		<cfset result.Message= "Email Content does not exist">
	</cfif>

	<cfreturn result>

</cffunction>



	<!--- NJH 2013/09/20 RW2013Roadmap2 - Sprint 1 - get path from fileType --->
	<cffunction access="public" name="getStyleSheetInfo" hint=""  output="false">

		<cfset var result = {isOK = true,name=this.styleSheet}>
		<cfset var getDir = "">
		<cfset var fileTypePath = application.com.filemanager.getFileTypes(type="Communications",fileTypeGroup="stylesheets").path[1]>
		<cfset var path = "#application.userfilesabsolutepath#\Content\#fileTypePath#\">

		<cfif this.styleSheet is not "">
			<cfdirectory directory="#path#" filter="#this.styleSheet#" action="list" name = "getDir">

			<cfif getDir.recordcount>
				<cfset result.dateLastModified = lsparsedatetime(getDir.datelastmodified,"English (UK)")>
				<cffile action="read" file = "#path##this.styleSheet#" variable="result.code">
			<cfelse>
				<cfset result.isok = false>
			</cfif>
		<cfelse>
			<cfset result.isok = false>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="makeStyleBlock" output="false">

		<cfset var styleSheetInfo = getStyleSheetInfo()>
		<cfset var result = "">

		<cfif styleSheetInfo.isOK>
			<cfset result = '<style type="text/css" data-relayStyle="#this.styleSheet#" data-relayStyleDate="#styleSheetInfo.dateLastModified#">#chr(10)##StyleSheetInfo.code##chr(10)#</style>'>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="getRelayStyleTagFromContent" hint="" output="false">
		<cfargument name="content">

		<cfset var result= {hasRelayStyle = false}>
		<cfset var styleTags = application.com.regExp.findHTMLTagsInString(inputString=content,htmlTag="style",hasEndTag="true")>
		<cfset var style = "">

		<cfloop array = #styleTags# index="style">
			<cfif structKeyExists (style.attributes,"data-relaystyle")>
				<cfset result.hasRelayStyle = true>
				<cfset result.Name = style.attributes["data-relaystyle"]>
				<!--- just incase the data-relaystyledate attribute has been removed, I test for it --->
				<cfif structKeyExists (style.attributes,"data-relaystyledate")>
				<cfset result.Date = style.attributes["data-relaystyledate"]>
				<cfelse>
					<cfset result.Date = 0 >
				</cfif>
				<cfset result.tag = style>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="hasStyleSheetBeenUpdated" hint="" output="false">

		<!--- get html
			look for style block with data-relaystyle attribute
			check name and modified date
		 --->
		<cfset var styleSheetInfo = getStyleSheetInfo()>
		<cfset var relayStyleTag = getRelayStyleTagFromContent (getContent().email.html)>
		<cfset var result = false>

		<cfif styleSheetInfo.isOK>

			<cfif relayStyleTag.hasRelayStyle>
				<cfif styleSheetInfo.datelastmodified gt relayStyleTag.Date  OR styleSheetInfo.name is not relayStyleTag.Name>
					<cfset result = true>
				</cfif>

			<cfelse>
				<cfset result = true>
			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="updateStyleBlock" hint="" output="false">
		<cfargument name="content">

		<cfset var result = content>
		<cfset var relayStyleTag = getRelayStyleTagFromContent (content)>

		<cfset var styleBlock = makeStyleBlock(this.StyleSheet)>

		<cfif relayStyleTag.hasRelayStyle>
			<cfset result = ReplaceNoCase (result,relayStyleTag.tag.string,styleblock)>
		<cfelse>
			<cfset result = reReplaceNoCase (result,"(</head>)","#styleblock##chr(10)#\1")>
		</cfif>


		<cfreturn result>

	</cffunction>


	<!--- This function is under development, it tries to get details of all merge fields in the content.
		It should allow us to get all the correct fields in the commdetail query, at the moment we get lots of fields which are not used
		Ideally we would detect flags as well
	 --->
	<cffunction access="public" name="getMergeFields" hint=""  output="false">

		<cfset var temp = '' />
		<cfset var table = '' />
		<cfset var column = '' />
		<cfset var field = '' />

		<cfif not structKeyExists(variables.data,"mergeFields")>
			<cfset variables.data.mergeFields = getStandardMergeFields()>
			<cfset temp = getAllMergeFieldsFromContent()>

<!---
			<cfloop collection="#extraMergeFields#" item="field">
				<cfif listLen(Field,".") is 2>
					<cfset table = listfirst(field,".")>
					<cfset column = listlast(field,".")>

					<cfif structKeyExists (variables.data.mergeFields.Lists,table)>
						<cfif not listfindNoCase(variables.data.mergeFields.Lists[table],column)>
							<cfset variables.data.mergeFields.Lists[table] = listAppend(variables.data.mergeFields.Lists[table],column)>
							<cfset queryaddRow(variables.data.mergeFields.Queries[table])>
							<cfset querySetCell(variables.data.mergeFields.Queries[table],"insertname",column,variables.data.mergeFields.Queries[table].recordcount)>
							<cfset querySetCell(variables.data.mergeFields.Queries[table],"derivedField",false,variables.data.mergeFields.Queries[table].recordcount)>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
 --->

		</cfif>

		<cfreturn variables.data.mergeFields >
	</cffunction>


	<cffunction access="public" name="cleanUpMergeFiles" hint=""  output="false">
		<!--- WAB TBD --->
	</cffunction>



	<cffunction access="public" name="getCommLimits" hint=""  output="false">
		<cfif not structKeyExists (variables,"commLimits")>
			<cfset variables.commLimits = application.com.settings.getSetting("communications.limits")>
		</cfif>
		<cfreturn variables.commLimits>
	</cffunction>

	<cffunction access="public" name="initialiseSendResultObject" output="false">

		<cfset variables.sendResultObject = createObject ("component","communicate.commSendResult")>
		<cfset variables.sendResultObject.init(commid = this.commid, title = this.title, commformLID = this.CommFormLID, creatorEmail= this.creatorEmail, creatorID = this.creatorID, creatorName = this.creatorName, description = this.description, argumentCollection = arguments)>

		<cfif isDebug()>
			<cfset variables.SendResultObject.setMetaData(debugFile = variables.debug.webpath)>
		</cfif>

		<cfreturn variables.sendResultObject>
	</cffunction>




<cffunction name="getStandardMergeFields" output="false">
	<!--- WAB 2011/11/16
		going to create a query and the person, location and organisation keys of the merge structure dynamically
		so get details of the fields we need from the mergeFields XML
	--->
	<cfset var fieldQuery = ''>
	<cfset var entityType = ''>
	<cfset var mergeFields = {queries = structNew(),lists = structNew()}>

	<cfif not structKeyExists (this,"mergeFields")>

		<cfloop index="entityType" list="person,location,organisation">
			<cfset fieldQuery = application.com.relayTags.getMergeFields(entityTypeID = application.entityTypeID[entityType])>
			<cfset mergeFields.Queries[entityType] = fieldQuery>
			<cfset mergeFields.lists[entityType] = valueList (fieldQuery.insertname)>
		</cfloop>

		<cfset variables.data.mergeFields = mergeFields>
	</cfif>

	<cfreturn variables.data.mergeFields>

</cffunction>


<cffunction name="getTskCommSendCommDetailQuery" output="false">
	<cfargument name="test" >
	<cfargument name="commformLID" default="2" > <!--- this means it is an email - bit sperfulous since we no longer support fax --->
	<cfargument name="PersonID" default="" >
	<cfargument name="preview" default="false" >
	<cfargument name="top" default="" >


	<cfset var result  = {}>
	<cfset var remoteStub ="">
	<cfset var remoteStubNoSession ="">
	<cfset var linkedSiteStructure ="">
	<cfset var MagicNumberCode = "convert(varchar,(p.personid)) + '-' + convert(varchar,(p.personid & 1023) * (l.locationid & 1023))">
	<cfset var mergeFields = getMergeFields()>
	<cfset var commTypeLID = this.commtypelid>
	<cfset var dummyTableName = "##dummyCommDetail">
	<cfset var commDetailQuery = "">
	<cfset var useDummyTable = iif(preview and PersonID is not "",true,false) >
	<cfset var internalremoteStub = '' />
	<cfset var tableAlias = '' />
	<cfset var theQuery = '' />
	<cfset var entityType = '' />
	<cfset var linkToSiteURL = '' />
	<cfset var count = '' />
	<cfset var commDetailQueryresult = '' />

						<!--- get list of people in communication assume people added to table only if adder had appropriate
						     country permissions so no need to check permissions now --->

						<cfset remoteStub =  "#this.linktoSiteURLWithProtocol#/remote.cfm?#iif(arguments.test,de("t=1&"),de(""))#" >
						<cfset remoteStubNoSession =  "#this.linktoSiteURLWithProtocol#/webservices/nosession/remote.cfm?#iif(arguments.test,de("t=1&"),de(""))#" >
						<cfset internalremoteStub =  "#application.com.relayCurrentSite.getReciprocalInternalProtocolAndDomain()#/remote.cfm?" >

						<CFQUERY NAME="commDetailQuery" DATASOURCE="#application.SiteDataSource#" result="commDetailQueryresult">
						 	<cfif useDummyTable>
								if exists (select 1 from sysobjects where id = object_ID('#dummyTableName#'))
									DROP table #dummyTableName#

						 		select #PersonID# as personid,
							 		#this.commid#	as commid,
									0 as commSenderID,
									0 as commDetailID,
									'' as feedback,
									#iif(test,1,0)# as test,
									#commTypeLID# as commTypeLID,
									#commFormLID# as CommTypeId
						 		into #dummyTableName#
						 	</cfif>

							SELECT
							DISTINCT
							<cfif top is not "">
							TOP #top#
							</cfif>

							<!--- WAB 2011/11/16
							Build parts of query dynamically from the mergeFields XML
							ToDo - Some fields still done by hand, could be added to XML with suitable metadata

							--->
							<cfloop collection="#mergeFields.Queries#" item="entityType">
								<cfset tableAlias = left(entityType,1)>
								<cfset theQuery = mergeFields.Queries[entityType]>
								<cfloop query = "theQuery">
									<cfif not derivedField>
									 #tableAlias#.#theQuery.insertname#,
									</cfif>
								</cfloop>
							</cfloop>
							ltrim(rtrim(p.Email)) as email,
							countrydescription as countryname,
							l.Sitename AS orgname,
							country.CountryDescription,
							country.routingcode,             <!---  not actually needed for email--->
							country.isocode,
							#preservesinglequotes(MagicNumberCode)#   as magicNumber   ,  <!--- checksum/hash type arrangement just to --->
							'#this.linktoSiteURLWithProtocol#/index.cfm?c=#this.commid#&p=' + #preservesinglequotes(MagicNumberCode)# as externalAutoLogin,  <!--- checksum/hash type arrangement just to --->
							'#remotestub#c=#this.commid#&a=u&p=' + #preservesinglequotes(MagicNumberCode)#  +'&fid=' + isnull((select convert(varchar,flagid) from flag f where f.flagtextid = 'nocommstypeAll'),'') as unsubscribe,    <!--- WAB 25/04/2007 added a fid for this item.  changes to unsubscribeHandler and unsubscribeHandlerScreen at same time--->
							'#remotestub#c=#this.commid#&a=u&p=' + #preservesinglequotes(MagicNumberCode)#  +'&fid=' + isnull((select convert(varchar,flagid) from flag f, lookuplist ll where f.flagtextid = 'nocommstype' + convert(varchar,ll.lookupid) and ll.lookupid = #commtypelid#),'') as unsubscribethistype,
							'#remotestub#c=#this.commid#&a=d&p=' + #preservesinglequotes(MagicNumberCode)#  as download,
							'#remotestub#c=#this.commid#&a=g&p=' + #preservesinglequotes(MagicNumberCode)#  as goToURL,
							'#remotestub#c=#this.commid#&a=e&p=' + #preservesinglequotes(MagicNumberCode)#  as goToElementID,
							'#remoteStubNoSession#c=#this.commid#&a=t&cd=' + convert(varchar,cd.commdetailid) + '&p=' + #preservesinglequotes(MagicNumberCode)#  as trackThisEmail,
							'#internalremotestub#frmCommID=#this.commid#&a=i&st=#application.startingTemplateLookupStr['commcheckdetails']#&p=' + #preservesinglequotes(MagicNumberCode)#  as linktoCommCheckDetail,
								'#remotestub#c=#this.commid#&p=' + #preservesinglequotes(MagicNumberCode)# as linkToRemoteCFM,
							#this.commtypeLID# as commType,
					 		<cfif this.flagID is not "">
							efd.PartnerRegID as eventRegistrationID,
							</cfif>
							cd.PersonID,
							cd.commID, <!--- ChangeID:25 added by SWJ 05 August 2001 as additional merge field --->
							cd.commSenderID, <!--- added by CPS 05 Nov 2001 --->
							cd.commDetailID,
							Country.EmailFrom
							 FROM
							 	<cfif useDummyTable>
									#dummyTableName# AS cd
							 	<cfelse>
							 		CommDetail AS cd with (nolock)
										Inner Join
									#tempTableName# t on t.commdetailid = cd.commdetailid
							 	</cfif>


							 		inner join
							 		Person AS p with (nolock) ON cd.PersonID = p.PersonID
							 		inner join
							 		 Location AS l with (nolock) ON p.LocationID = l.LocationID
							 		 inner join
							 		  organisation as o with (nolock) ON l.organisationID = o.organisationid
									inner join
							 		  Country with (nolock) ON l.CountryID = Country.CountryID

							 		<cfif this.flagID is not "">
									left join eventFlagData efd on efd.flagid = #this.flagID# and efd.entityid = cd.personid
									</cfif>

							WHERE cd.CommID = #this.commid#
								<cfif not preview>
									AND isNull(cd.Feedback,'') = ''
									 AND p.Active <> 0
								</cfif>

								AND cd.CommTypeId = #CommFormLID# <!--- not the communicationLID but CommFileLID --->




							and cd.test = <cfif arguments.test>1<cfelse> 0 </cfif>	<!--- WAB 12/4/05 populate commdetail will always set test = 1 for tests, this allows for a person to receive both a test email and a live one  --->

							<CFIF CommFormLID IS 2>
								ORDER BY ltrim(rtrim(p.Email))		<!--- WAB  for removing duplicates--->
							<CFELSE>
								ORDER BY l.Fax
							</CFIF>

						 	<cfif useDummyTable>
								drop table
						 		#dummyTableName#
						 	</cfif>

						</CFQUERY>

<cfset debugLog ("commDetailQueryResult",commDetailQueryResult)>

						<cfset result.query = commDetailQuery>

						<cfif result.query.recordCount EQ top>
							<!--- The TOP has limited the number of items returned, work out the number of items left to send --->
							<CFQUERY NAME="count" DATASOURCE="#application.SiteDataSource#">
									SELECT
										 count(1) as theCount
									FROM
								 		CommDetail AS cd with (nolock)
											Inner Join
										#tempTableName# t on t.commdetailid = cd.commdetailid
									 		inner join
								 		Person AS p with (nolock) ON cd.PersonID = p.PersonID
									 		inner join
							 			Location AS l with (nolock) ON p.LocationID = l.LocationID
							 				 inner join
								 	 	organisation as o with (nolock) ON l.organisationID = o.organisationid
											inner join
							 		  	Country with (nolock) ON l.CountryID = Country.CountryID
									WHERE
											cd.CommID = #this.commid#
										AND isNull(cd.Feedback,'') = ''
									 	AND p.Active <> 0
										AND cd.CommTypeId = #CommFormLID#
										and cd.test = <cfif arguments.test>1<cfelse> 0 </cfif>
							</CFQUERY>
							<cfset result.totalRecipients = count.theCount>
						<cfelse>
							<cfset result.totalRecipients = result.query.recordCount>
						</cfif>

		<cfreturn result>

</cffunction>

<cffunction name="getAllMergeFieldsFromContent" output="false">
			<!--- TBD only dealing with html - perhaps do something different --->
			<cfset var preparedEmailContent = getPreparedContent().email>
			<cfset var mergeObject = '' />
			<cfset var mergeStruct = '' />
			<cfset var tempArgs = '' />
			<cfset var testResult = '' />
			<cfset var result = '' />
			<cfset var i = '' />


			<cfset mergeObject = application.com.relayTranslations.getMergeObject()>
			<cfset mergeStruct = {person = structnew(), location = structNew(), organisation = structNew(),user = structNew(),personid=0,locationid=0,organisationid=0,context="comm"}>

			<cfset mergeObject.initialiseMergeStruct(mergeStruct = mergeStruct)>

			<cfset tempArgs = {phraseText = preparedEmailContent.html,logmergeerrors=false}>
			<cfset testResult = mergeObject.identifyMergeFields(argumentCollection = tempArgs)>



			<cfreturn result>

</cffunction>

<cffunction name="getMergedContentForSinglePerson" output="false">
		<cfargument name="personid" default="#request.relayCurrentUser.personid#">
		<cfargument name="evaluateAllOptions" default="false">
		<cfargument name="mergeByFile" default="true">
		<cfargument name="test" type="boolean" required="true">
		<cfargument name="showLinkBackToCommunication" type="boolean" default="false">
		<cfargument name="footerText" type="string" default="">
		<cfargument NAME="subjectPrefix" default = "">
		<cfargument name="doEmail" default = "true">
		<cfargument name="doMessage" default = "true">

			<cfset var dummyCommDetailRecord = "">
			<cfset var result = "">

			<cfset getPreparedContent(argumentCollection = arguments)>
			<!--- Need to test sending the comm --->
			<cfset dummyCommDetailRecord = getTskCommSendCommDetailQuery(test = true,PersonID = personid,preview=true).query>


			<cfset result = doContentMerge(
											commdetailQuery = dummyCommDetailRecord,
						 					rowNumber= 1,
											test = true,
											evaluateAllOptions = evaluateAllOptions,
											mergeByFile = 	mergeByFile,
											doemail = doemail,
											domessage = domessage
															)>
		<cfreturn result>

</cffunction>

<cffunction name="getPreparedContent" output="false">

	<cfif not structKeyExists (variables.data,"preparedContent")>
		<cfset prepareEmailForMerge(argumentCollection = arguments )>
		<cfset prepareMessageForMerge(argumentCollection = arguments )>
	</cfif>
	<cfreturn variables.data.preparedContent>

</cffunction>

<cffunction name="resetPreparedContent" output="false">

	<cfset structDelete (variables.data, "preparedContent")>

</cffunction>

<cffunction name="doContentMerge" output="false">
	<cfargument name="commDetailQuery" >
	<cfargument name="rowNumber" >
	<cfargument name="test" default = "false">
	<cfargument name="mergeByFile" default = "true">   <!--- In general we want to merge by file, which is quicker.  However if there is an error in the merge fields then mergeByFile gives us no result, whereas merging each field individually gives us a partial result - which may be preferable when for example doing a preview --->
	<cfargument name="evaluateAllOptions" default = "false">
	<cfargument name="doEmail" default = "true">
	<cfargument name="doMessage" default = "true">


			<cfset var column = "">
			<cfset var mergeStruct = {context = "communication",communication = this }>
			<!--- Need to create a merge object on each loop because some values can get cached	--->
			<cfset var mergeObject = application.com.relayTranslations.getMergeObject()>
			<cfset var mergeFieldLists = getMergeFields().lists>
			<cfset var preparedContent = getPreparedContent(test = test)>
			<cfset var tempArgs = "">
			<cfset var htmlForMerging = "">
			<cfset var result = {isOK = true,email=structNew(),message=structNew()}>
			<cfset var entityType = ''>


			<cfset mergeStruct.communication.test = TEST>
			<!--- for new style merging we copy the details of the recipient into the mergeStruct
				everything goes into the top level of the merge struct for backwards compatibility
				but we also create merge.person, merge.location etc for our newer entity.fieldname notation
			--->
			<cfloop index="column" list = "#commDetailQuery.columnlist#">
				<cfset mergeStruct[column] = commDetailQuery[column][rowNumber]>
			</cfloop>
			<!--- create merge.person, merge.location etc --->
			<cfloop collection="#mergeFieldLists#" item="entityType">
				<cfset mergeStruct[entityType] = application.com.structureFunctions.queryRowToStruct(query = commdetailQuery,row = rowNumber, columns = mergeFieldLists[entityType])>
				<cfset mergeStruct[entityType].partialStructure = true>   <!--- see comment in merge.cfc for explanation of partialStructure - but basically tells us that this is not the whole of the entity structure --->
			</cfloop>

				<cfif useOmnitureTracking()>
				<cfset mergeStruct.AddCustomTrackingToURL = addOmnitureMergeStringToURL>  <!--- addOmnitureTrackingToURL --->
				<cfset mergeStruct.omnitureData = getOmnitureData()>
				</cfif>

			<cfset mergeObject.initialiseMergeStruct(mergeStruct = mergeStruct)>

			<cfif doEmail>

				<cfset mergeObject.evaluateAllOptions(evaluateAllOptions)>

				<cfif mergeByFile and not  evaluateAllOptions>
					<cfset result.email.html = mergeObject.mergeByFile(filePath = preparedContent.email.HTML_MergeFile.path)>
					<!--- TBD might improve performance if we don't do a text merge if there is no text to merge! --->
					<cfset result.email.text = mergeObject.mergeByFile(filePath = preparedContent.email.Text_MergeFile.path)>

				<cfelse>
					<cfset tempArgs = {phraseText = preparedContent.email.HTML,logMergeErrors = false}>
					<cfset result.email.html = mergeObject.checkForAndEvaluateCFVariables_ReturningStructure(argumentCollection = tempArgs)>
					<cfset tempArgs = {phraseText = preparedContent.email.text,logMergeErrors = false}>
					<cfset result.email.text = mergeObject.checkForAndEvaluateCFVariables_ReturningStructure(argumentCollection = tempArgs)>
				</cfif>

				<cfset tempArgs = {phraseText = preparedContent.email.subject,logMergeErrors = false}>
				<cfset result.email.subject = mergeObject.checkForAndEvaluateCFVariables_ReturningStructure(argumentCollection = tempArgs)>

				<cfif arrayLen(result.email.html.errorArray) or arrayLen(result.email.text.errorArray)>
					<cfset result.isOK = false>
				</cfif>

			</cfif>

			<cfif doMessage>
				<cfset result.message.image = preparedContent.message.image>
				<cfset result.message.url = preparedContent.message.url>
				<cfset result.message.text = mergeObject.checkForAndEvaluateCFVariables_ReturningStructure(phraseText = preparedContent.message.text,logMergeErrors = false)>
				<cfset result.message.title = mergeObject.checkForAndEvaluateCFVariables_ReturningStructure(phraseText = preparedContent.message.title,logMergeErrors = false)>

				<cfif arrayLen(result.message.text.errorArray) or arrayLen(result.message.title.errorArray)>
					<cfset result.isOK = false>
				</cfif>

			</cfif>

	<cfreturn result>

</cffunction>


<cffunction name="send" output="false">
	<cfargument NAME="asynchronous" default = "false">
	<cfargument NAME="test" default = "false">
	<cfargument NAME="scheduledCommunication" default = "false">
	<!--- and lots of other arguments supported (see send_()) --->

	<cfset var sendResult = "">
	<cfset var theFeed = "">
	<cfset var resultObj = "">
	<cfset var Lockname_CommID = '' />
	<cfset var asynchSendResult = '' />
	<cfset var hoursBetweenWarnings = '' />
	<cfset var scheduledDate = '' />

	<!---  Although there is a lock in the send_() function, I am going to create it here as well, so that if there is a process already running we can return an immediate result, whether called asynchrnous or not--->

	<cfset Lockname_CommID  = getLockName(test=test)>
	<cfset resultObj = initialiseSendResultObject(argumentCollection = arguments)>

	<!--- Use this for recording debug information during a send --->
	<cfset setdebug(application.com.relaycurrentsite.isEnvironment('dev'))  >

	<cfif not application.com.globalFunctions.doesLockExist(lockname = Lockname_CommID)>


		<cfif not asynchronous>

			<cfset sendResult = send_ (argumentCollection = arguments)>

			<cfif not sendResult.isOK and sendResult.scheduledCommunication is true>
				<cfset hoursBetweenWarnings = 24>
				<cfif (not structKeyExists(this.metadata,"failureWarningEmailSent") or datediff("h",this.metadata.failureWarningEmailSent,now()) gte hoursBetweenWarnings or this.contentLastUpdated gt this.metadata.failureWarningEmailSent)>
					<cfset scheduledDate = getSchedule().sendDate>
					<cf_mail from="#request.currentSite.systemEmail#" to="#sendResult.creatorEmail#" bcc="william.bibby@relayware.com" subject="Scheduled Communication #sendResult.commid# **FAILED**: #sendResult.title#" type="html">
						<cfoutput>
						Your communication #sendResult.title#.<br />
						<cfif isDate(scheduleddate)>
							Which was scheduled to be sent #application.com.datefunctions.DayDiffFromTodayInWords(scheduledDate)#<br />
						</cfif>
						Has <FONT color="RED">FAILED</FONT><br />
						The following error message was generated
						<br />
						#sendResult.errormessage#
						<br />
						Please contact Relay Support
						<cfif sendResult.errorID is not 0>
						quoting Error ID #sendResult.errorID#
						</cfif>

						</cfoutput>
					</cf_mail>

					<cfset setMetaData ("failureWarningEmailSent",now())>
				</cfif>
			</cfif>


		<cfelse>

			<cfset sendResult = {isOK = true,commSendResultID = resultObj.commSendResultID}>
			<cfset theFeed= arguments>
			<cfset request.dontRunHousekeeping = true>
				<cfthread name="sendThread#resultObj.commSendResultID#" feed=#theFeed#>
					<!--- WAB 2013-10-08 When the thread starts it seems to lose all knowledge of the output=false attributes.  So add an enablecfoutputonly --->
					<cfsetting enablecfoutputonly="true">

					<cftry>
					<cfset asynchSendResult = send_ (argumentCollection = attributes.feed)>

						<cfcatch>
							<cfset application.com.errorHandler.handleError(exception=cfcatch)>
						</cfcatch>
					</cftry>
				</cfthread>
				<cfset sleep (1000)> <!--- rather a hack to make sure that lock is created before an ajax request comes in to find the status --->
		</cfif>

	<cfelse>
		<cfset resultObj.setError ("Cannot be sent - Locked")>
		<cfset resultObj.delete ()>  <!--- Not interested in keeping db records recording a lock--->
		<cfset sendResult = resultObj>
 	</cfif >

	<cfreturn sendResult>
</cffunction>



<cffunction name="send_" access="private" output="false">
	<cfargument NAME="PersonIDs" default = "">
	<cfargument NAME="EmailAddresses" default = "">
	<cfargument NAME="CommSelectLID" default = "#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.internalSelectLID#">
	<cfargument NAME="CommFormLID" default = "">
	<cfargument NAME="selectionID" default = "">
	<cfargument NAME="test" default = "false">
	<cfargument NAME="resend" default = "false">
	<cfargument NAME="subjectPrefix" default = "">
	<cfargument NAME="showLinkBackToCommunication" default = "false">
	<cfargument NAME="footerText" default = "">
	<cfargument NAME="replyto" default = "">
	<cfargument NAME="sendConfirmation">
	<cfargument NAME="scheduledCommunication" default="false">
	<cfargument NAME="showDebugOutput" default="false">
	<cfargument NAME="debugSaveSentEmailContent" default="#application.com.settings.getSetting ('communications.debugSaveSentEmailContent')#">


			<cfset var mailServerAddress = "">
			<cfset var mailFailToDomain = '' />
			<cfset var mailFromDomain = '' />
			<cfset var Lockname_CommID = '' />
			<cfset var isApprovedToSendNowResult = '' />
			<cfset var checkContentResult = '' />
			<cfset var recipientQuery = '' />
			<cfset var content = '' />
			<cfset var emailContent = '' />
			<cfset var thistitle = '' />
			<cfset var thisItemResult = {} />
			<cfset var checkcommSizeOK = '' />
			<cfset var lockName_Inner = '' />
			<cfset var currentItem = '' />
			<cfset var previousItem = '' />
			<cfset var PMatch = '' />
			<cfset var CMatch = '' />
			<cfset var maxRowsAllowed = '' />
			<cfset var LockName_commitem = '' />
			<cfset var failToAddress = '' />
			<cfset var thisDetailReplyTo = '' />
			<cfset var thisreplyto = '' />
			<cfset var thisccto = '' />
			<cfset var thisfromaddress = '' />
			<cfset var thissubject = '' />
			<cfset var MergeResult = '' />
			<cfset var cfMailAttributes = '' />
			<cfset var c_emaildist = '' />
			<cfset var warningStructure = '' />
			<cfset var type = '' />
			<cfset var severity = '' />
			<cfset var errorMessage = '' />
			<cfset var resultStruct = '' />
			<cfset var updateCommSelection = '' />
			<cfset var check = '' />
			<cfset var getSender = '' />
			<cfset var updateCommunication = '' />
			<cfset var updateCommSelectionSent = '' />
			<cfset var missingEmails = '' />
			<cfset var createCommDetailTemporaryTableResult = '' />
			<cfset var populateCommDetailFromTemporaryTableResult = '' />
			<cfset var Recipients = '' />
			<cfset var totalRecipients = '' />
			<cfset var message = '' />
			<cfset var email = '' />
			<cfset var checkPerson = '' />
			<cfset var checkItemUnsent = '' />
			<cfset var commSelectionFilter = '' />
			<cfset var suppressDuplicateEmails = '' />
			<cfset var tickCountPer100Items = [] />
			<cfset var cancel = false />
			<cfset var errorID = 0>
			<cfset var row = 0>
			<cfset var performanceCheckEveryXRows_default = 100>
			<cfset var performance = {overallTime = 0, overallItemsPerSecond = 0, thisTime = 0, thisItemsPerSecond = 0, CheckEveryXRows = performanceCheckEveryXRows_default, lastRow = 0, lastCheckOK = true, rows=0}>

			<cfset Lockname_CommID  = getLockName(test=test)>

<cfset debugLog("Main Lock")>


				<cf_lock name = "#Lockname_CommID#" throwOnTimeout=false message="Processing Started" commSendResultID="#variables.sendResultObject.commSendResultID#" test="#test#">
					<!--- Note that with CF_LOCK it is best not to have cftry's around the outside, because then locks do not get released on an error --->
					<cfif isDebug()>
						<cf_lockupdate debugFile = #variables.sendResultObject.debugFile#>
					</cfif>

					<cftry>

						<!--- record what arguments were passed into this function - really for debugging purposes--->
						<cfset variables.sendResultObject.setMetaData(sendArguments = arguments)>

						<!--- by default send a confirmation but not for tests or when sending to lists of people (usually a single person)--->
						<cfif not StructKeyExists (arguments,"sendConfirmation")>
							<cfif arguments.test or arguments.personids is not "">
								<cfset arguments.sendConfirmation = false>
							<cfelse>
								<cfset arguments.sendConfirmation = true>
							</cfif>
						</cfif>

						<cfset mailFailToDomain = application.com.settings.getSetting("communications.mailFailToDomain")>
						<cfset mailFromDomain = application.com.settings.getSetting("communications.mailFromDomain")>
						<cfif application.com.settings.isSettingDefault("communications.mailFromDomain")>
							<cfset mailFromDomain  = getDatabaseID() & "." & mailFromDomain> <!--- if the mailFromDomain is still the default of partner-management.com then we pop the database number infront - this means that any returned mail will go into the correct mail box --->
						</cfif>

						<!--- WAB 2010/10/11 decide what mail server to use.
							Optionally scheduled emails can be diverted to a different mail server to prevent them getting in the way of regular emails
							Don't do this for tests - which need to go out immediately
						--->
						<cfif not arguments.test>
							<cfset mailServerAddress = application.com.settings.getSetting("communications.mailServerAddress")>
						</cfif>
<cfset debugLog("Check Approval")>
						<!--- Do some checks on the communication and its files
							set thisCommResult.isOK false if necessary--->
						<cfif not arguments.test>
							<cf_lockUpdate message="Checking Approval">

							<cfset isApprovedToSendNowResult = isApprovedToSendNow()>

							<cfif NOT isApprovedToSendNowResult.isOK>
								<cfset variables.sendResultObject.setError (isApprovedToSendNowResult.Message)>
							</cfif>

							<!--- <br />TBD isCountryApproved()<br /> --->

						</cfif>

<cfset debugLog("Check Content Start")>
						<cfif variables.sendResultObject.isOK>
							<cf_lockUpdate message="Checking Content">

							<cfset checkContentResult = prepareAndValidateContent(test=arguments.test,showLinkBackToCommunication = arguments.showLinkBackToCommunication,footerText=arguments.footertext,subjectPrefix = arguments.subjectPrefix)>

							<cfif NOT checkContentResult.isOK>
								<cfset variables.sendResultObject.setError (checkContentResult.errorMessage)>
							</cfif>

							<cfset suppressDuplicateEmails = getPreparedContent().email.suppressDuplicateEmails>
						</cfif>
<cfset debugLog("Check Content End")>
					<cfif emailAddresses is not "">

						<cfquery name="checkPerson" datasource="#application.siteDataSource#">
							select personid,email from person where email  in ( <cf_queryparam value="#EmailAddresses#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) order by email
						</cfquery>

						<cfset missingEmails = application.com.globalfunctions.listMinusList(emailAddresses,valuelist(checkPerson.email))>

						<cfloop list="#missingEmails#" index="email">
							<cfset thisItemResult = {name = "", personid = 0, address = Email,OK = false,feedback="Email Address does not exist"}>
							<cfset variables.sendResultObject.addItem (thisItemResult)>
						</cfloop>

						<cfif checkPerson.recordCount neq 0>
							<cfset arguments.PersonIDs = "#valuelist(checkPerson.personid)#">
						<cfelse>
							<cfset arguments.PersonIDs = 0>
						</cfif>


					</cfif>


					<CFIF variables.sendResultObject.isOK>

<cfset debugLog("Populate CommDetail")>
							<cf_lockUpdate message="Preparing Recipient List">
						<cfset createCommDetailTemporaryTableResult =
								createCommDetailTemporaryTable(
								personids = arguments.PersonIDs,
								CommSelectLID = arguments.commSelectLID,
								selectionID = arguments.selectionID,
								test = arguments.test)
						>

						<cfif structKeyExists (createCommDetailTemporaryTableResult,"selectionIDs")>
							<!--- Keep a record of which selections are being sent to - useful for feedback to other processes --->
							<cf_lockupdate selectionIDs = #createCommDetailTemporaryTableResult.selectionIDs#>
						</cfif>

						<cfset populateCommDetailFromTemporaryTableResult =
								populateCommDetailFromTemporaryTable(
								resend = #arguments.resend#)
						>

						<cfset variables.sendResultObject.setPeopleCounts(argumentsCollection = populateCommDetailFromTemporaryTableResult, totalPeople = createCommDetailTemporaryTableResult.totalPeople)>



<cfset debugLog("Set Preferences")>

						<!--- resolve preferences (fax <-> email) and unsubscribe details
						for tests we send to everyone		--->
						<CFIF not arguments.Test><!--- 2 --->
							<cf_lockUpdate message="Applying Unsubscribe, Opt-In etc.">
							<cfset setPreferencesEtc()>
						</cfif>


<cfset debugLog("Preparing Final Recipient List")>
						<cf_lockUpdate message="Preparing Final Recipient List">

						<!--- This code allows us to limit the number of emails sent at one go--->
						<cfset maxRowsAllowed = getCommLimits().maxNumberEmailsPerSend  >


						<cfset Recipients = getTskCommSendCommDetailQuery(test = arguments.test, top = maxRowsAllowed)>
						<cfset recipientQuery = Recipients.query>
						<cfset totalRecipients = Recipients.totalRecipients>

<cfset debugLog("Preparing Final Recipient List End")>

						<cfset content = getContent()>
						<cfset emailContent = content.email>

						<!--- WAB: can add a prefix to the subject line (usually Test:) --->
						<cfset thistitle = arguments.subjectPrefix & " " & this.Title>

								<!--- TBD CheckCommSize --->
						<cfset checkcommSizeOK = checkCommunicationSizeOkToSend (numberOfRecipients = recipientQuery.recordcount,test=arguments.test)>

						<cfif not checkcommSizeOK.isOK><!--- 2 --->
							<cfset variables.sendResultObject.setError ("Too large to be sent at this time of day")>
						</cfif>


							<!---  At this point, I want to check whether this is a very large manual communication
								and if so I want to check that there isn't already a scheduled communication running
TBD decide if necessary

							<cfif not ScheduledCommunication and recipientQuery.recordcount gt commLimits.maxSizeForConcurrentSending	>
								<cfset lockName_Inner = scheduledCommLockName>
							<cfelse>
								<cfset lockName_Inner= "tskCommSend#replaceNoCAse(createUUID(),'-','','ALL')#">
							</cfif>

							<cf_lock name = "#lockName_Inner#" throwOnTimeout=false  >
							--->

								<CFIF not variables.sendResultObject.isOK><!--- 2 --->

								<CFELSE>
									<!--- all OK --->

									<!--- WAB/NJH 2008/10/21 Bug Fix Trend Nabu Issue 1076- extend timeout if required.  Say each person needs .25 of a second
									WAB 2008/11/05 think we reduced times a bit so 	each comm given 5 minutes to initialise and have a minimum of 1 hour anyway
									--->
									<cfset application.com.request.extendTimeoutIfNecessary (recipientQuery.RecordCount/4)>


									<cfset currentItem = structNew()>
									<cfset currentItem.Email = "">
									<cfset currentItem.person = "">
									<cfset currentItem.org = "">
									<cfset currentItem.personID = "">

									<cfset previousItem = structCopy(currentItem)>

									<cfset PMatch = 0>
									<cfset CMatch = 0>

									<cfif totalRecipients GT recipientQuery.recordCount><!--- 3 --->
										<cfset variables.sendResultObject.partialSend = true>
										<cfset variables.sendResultObject.itemsLeft = totalRecipients - recipientQuery.recordCount >
									</cfif>
<cfset debugLog("Start Recipients")>

									<cfset performance.TickCount = getTickCount()>

									<CFLOOP QUERY="recipientQuery" endrow="#maxRowsAllowed#">

											<!---
												Every 100th row
													Update the lock status/message
													Check for a cancellation request
													Check performance
														If performance deteriorates then we break out

													WAB 2013-10-08 Moved this code to the beginning of the loop so could check for cancellation before anything is sent
											--->

											<cfif currentRow mod performance.checkEveryXRows is 1>
												<cfset rowsDone = currentRow - 1>

												<cfset debugLog("#rowsDone# rows done")>

												<cfif currentRow is not 1>
													<!--- record time since did last check --->
													<cfset performance.thisTime = getTickCount() - performance.TickCount>
													<cfset rowsSinceLastCheck = rowsDone - performance.lastRow>
													<cfset performance.thisItemsPerSecond = int(rowsSinceLastCheck/(performance.thisTime/1000))>

													<cfset debugLog("Av:#performance.overallItemsPerSecond#. This: #performance.thisItemsPerSecond#  items/second")>
													<!--- check if sending rate is less then 1/3 of the average --->
													<cfif performance.thisItemsPerSecond LT performance.overallItemsPerSecond / 3 >
														<cfif performance.lastCheckOK >
															<!--- Maybe a one off slowness (garbage collection perhaps), do a few more items and see what happens --->
															<cfset performance.checkEveryXRows = 10>
															<cfset debugLog("System Slowing Down, try #performance.checkEveryXRows# more items ...")>
															<cfset performance.lastCheckOK = false>
														<cfelse>
															<!--- already had a slow request, so time to break out --->
															<cfset debugLog("System Slowing Down, break out")>
															<cfset cancel = true>
														</cfif>
													<cfelse>
														<!--- sending rate OK --->
														<cfset performance.lastCheckOK = true>
														<!--- set checkEveryXRows back to its default value - might have been changed if we have had a slow request --->
														<cfset performance.checkEveryXRows = performanceCheckEveryXRows_default>
														<!--- record overall processing time and items per second
															(note we don't count time inside the checking code)
															Note that I don't include a very slow request in this average
														--->

														<cfset performance.rows += rowsSinceLastCheck>
														<cfset performance.overallTime += performance.thisTime>
														<cfset performance.overallItemsPerSecond = int(performance.rows/(performance.overallTime/1000))>
													</cfif>
												</cfif>

												<!--- Create a message to store in the ProcessLock --->
												<cfset message = "Processed #rowsDone#/#totalRecipients#">
												<cfif variables.sendResultObject.partialSend>
													<cfset message &= "<BR>Batch Size #recordCount#">
												</cfif>

												<cfif performance.overallItemsPerSecond is not 0>
													<cfset itemsPerMinute = performance.overallItemsPerSecond * 60>
													<cfset message &= "<BR>#itemsPerMinute# Items per Minute">
												</cfif>

												<!--- Update the processLock message and check for a cancellation request --->
												<cf_lockUpdate message=#message#>

												<cfif structKeyExists(cf_lockupdate.metadata,"cancel")>
													<cfset variables.sendResultObject.partialSend = true>
													<cfset variables.sendResultObject.itemsLeft = totalRecipients - currentRow >
													<cfset cancel = true>
												</cfif>

												<cfset performance.TickCount = getTickCount()>
												<cfset performance.lastRow = rowsDone>

											</cfif>

											<!--- Process a cancel, might be a result of manual intervention or a slow request --->
											<cfif cancel is true>
												<cfset variables.sendResultObject.partialSend = true>
												<cfset variables.sendResultObject.itemsLeft = totalRecipients - currentRow - 1>
												<cfset debugLog("Cancel Detected")>
												<cfbreak>
											</cfif>


											<cfset thisItemResult = {name = "#Trim(FirstName)# #Trim(LastName)#", personid = personid, address = recipientQuery.Email}>

										<cftry>

											<cfset LockName_commitem = "#application.applicationName#-#this.commid#-#PersonID#-#test#">
											<cflock name = "#LockName_commitem#" Type="EXCLUSIVE" timeout="0" throwOnTimeout=false >
													<!--- WAB 2004-12-29
														failto address contains the ids required to process bouncebacks
														it used to used as the from address as well, but clients didn't like the all the numbers inthe email address.
														Most bouncebacks come back to the failto address, but we don't get all the "out of office/read receipts" which can go to the  reply to address

														application.emailfrom is supposed to be the bit before the @ sign to be combined with application.mailfromdomain, but sometimes (Triathlon) it has been set to an actual email address which causes some problems!
														Therefore I am going to strip out any @ signs which have got in there, before creating the failToAddress address
													 --->

													<cfset failToAddress = "#replace(application.emailFrom,"@",".","ALL")#-BB#getDatabaseID()#-#this.commid#-#PersonID#@#mailFailToDomain#">

														<CFIF Trim(recipientQuery.Email) IS NOT "" AND ListLen(recipientQuery.Email,"@") IS 2 AND reFindNoCase("^[A-Za-z0-9\-_\.@\+##']+$",recipientQuery.Email) is not 0>
															<!--- email not blank, has an @ sign and has only alphanumeric plus -_.@+#
															WAB 2005-10-05 added apostophe which appears to be allowed
															doesn't check for  form a@ghi.xyz
															--->
															<CFIF previousItem.email IS NOT recipientQuery.Email OR TRIM(this.FlagID) NEQ "" or suppressDuplicateEmails is false>     <!--- new email address --->
																<!--- email not same as last message --->

																<!--- If the commdetail record is being sent via the 'Send Previous Communication' to checked
															      people then the attribute commSenderId will be set. Use this value to work out the reply
																  address of the email

																  TBD This can be moved into the base query (if even used)
																  --->

																<cfset thisDetailReplyTo = ''>
																<!--- TBD this can be moved to base query if it is necessary
																<CFIF recipientQuery.commSenderId NEQ '' and recipientQuery.commSenderID neQ 0>
																	<CFQUERY NAME="getSender" DATASOURCE="#application.SiteDataSource#" debug="#attributes.showdebugoutput#">
																	SELECT email
																	<!--- NYB 2009-08-06 NABU LHID 2456 - added 'with (nolock)' to query --->
																	  FROM person with (nolock)
																	 WHERE personId =  <cf_queryparam value="#recipientQuery.commSenderId#" CFSQLTYPE="CF_SQL_INTEGER" >
																	</CFQUERY>
																	<cfset this.RequestReceipt = 2>
																	<cfset thisDetailReplyTo = getSender.Email>
																</CFIF>
																--->
																<!--- if receipts are requested then reply to address has to be our servers so that they can be processed, otherwise it can be an individuals name --->
																<cfset thisreplyto = iif (arguments.replyTo is not "",de(arguments.replyTo),de(IIF(this.RequestReceipt IS 1, DE(failToAddress), DE(IIF(this.RequestReceipt IS 2, DE(thisDetailReplyTo), DE(this.replyto)))))) >
																<cfset thisccto =  IIF(this.CCto IS NOT "", DE(" -cc:#this.CCto# "), DE(""))>

																<cfset thisfromaddress = this.FromDisplayName & " <"& this.FromAddress&">" >
																	<cfset MergeResult = doContentMerge(
																				commdetailQuery = recipientQuery,
																				rowNumber= currentRow,
																				doMessage = false
																				)>
																	<cfif arrayLen(MergeResult.email.HTML.errorArray) or arrayLen (MergeResult.email.Text.errorArray)>
																		<!--- TBD better message for merge errors --->
																		<cfset variables.sendResultObject.setError ("Merge Field Error: #FormatContentErrorArray_HTML(MergeResult.email.HTML.errorArray)# #FormatContentErrorArray_HTML(MergeResult.email.Text.errorArray)#")>
																		<cfbreak>
																	</cfif>

																	<cfif debugSaveSentEmailContent>
																	<!--- code can be activated for debugging --->
																			<CFIF MergeResult.email.Text.phraseText is not "" >
																					<cffile action="WRITE" file="#application.userfilesAbsolutePath#/commfiles/temp/#this.commid#-#personid#-#this.CommFormLID#-txt.txt" output="#MergeResult.email.Text.phraseText#" 		charset = "UTF-8">
																			</CFIF>
																			<CFIF MergeResult.email.HTML.phraseText is not "" >
																					<cffile action="WRITE" file="#application.userfilesAbsolutePath#/commfiles/temp/#this.commid#-#personid#-#this.CommFormLID#-html.txt" output="#MergeResult.email.HTML.phraseText#" 		charset = "UTF-8">
																			</CFIF>
																	</cfif>

																	<!--- WAB 2009/01/14 Changes to use CF_MAIL, required adding a CFOUTPUT tag--->
																	<!---WAB 2010/10/11 added an arguments collection so that serverAddress could be passed in optionally --->
																	<cfset cfMailAttributes = {to=recipientQuery.Email,from=thisfromaddress,subject=mergeResult.email.subject.phraseText,replyto=thisreplyto,type=emailContent.singlepartmessageformat,failto=failToAddress,cc=thisccto}>
																	<cfif mailServerAddress is not ""><!--- 7 --->
																		<cfset cfMailAttributes.server =  mailServerAddress>
																	</cfif>

																	<!---
																	check that feedback is still blank
																	this is a belt and braces approach to make sure that two processes don't send the same comm concurrently (remembering that CFLOCK is not across a cluster)
																	TBD This could be moved even later (just before the send)
																	 --->
																	<CFQUERY NAME="checkItemUnsent" DATASOURCE="#application.SiteDataSource#"  debug="#arguments.showdebugoutput#">
																		SELECT 1
																		FROM commdetail
																		WHERE commdetailid =  <cf_queryparam value="#recipientQuery.commDetailID#" CFSQLTYPE="CF_SQL_INTEGER" >
																	   	AND isNull(Feedback,'') = ''
																	</CFQUERY>

																	<cfif checkItemUnsent.recordCount is not 0><!--- 3 --->

																		<cftry>  <!--- to catch invalid email addresses that have slipped through --->

																			<cf_mail attributeCollection = #cfMailAttributes#><cfsilent>
																				<cf_mailparam name="Message-ID" value="<#createUUID()#.#failToAddress#>"><!--- not sure whether this is a good idea or not (WAB), actually it doesn't seem to work - java creates its own message id --->
																				<cf_mailparam name="Return-Path" value="#failToAddress#">
																				<cfif this.RequestReceipt IS 1><!--- 7 --->
																					<CF_MAILPARAM name="notify" value = "1">  <!--- need to find name and syntax --->
																					 <cf_mailparam name="Disposition-Notification-To" value="#failToAddress#">
																					<cf_mailparam name="Read-Receipt-To" value="#failToAddress#">
																				</cfif>
																				</cfsilent><cfoutput><cfif emailContent.multipartmessage><CFIF MergeResult.email.Text.phraseText is not "" ><cf_mailpart type="text">#MergeResult.email.Text.phraseText#</cf_mailpart></CFIF><CFIF MergeResult.email.html.phraseText is not "" ><cf_mailpart type="html">#MergeResult.email.html.phraseText#</cf_mailpart></CFIF><cfelseif emailContent.singlepartmessageformat is "html">#MergeResult.email.html.phraseText#<cfelse>#MergeResult.email.text.phraseText#</cfif></cfoutput>
																				<CFLOOP query="content.attachments"><CF_MAILPARAM file="#urlEncodedFormat(attachment)#"></CFLOOP>	<!--- WAB 2013-06-11 URLEncodedFormat deals with problems with + and % in filenames --->
																			</cf_mail>

																			<cfset thisItemResult.dbFeedback = "Sent">
																			<cfset thisItemResult.commStatusId = 2>
																			<cfset thisItemResult.OK = true>
																			<cfset thisItemResult.feedback = "">


																				<cfcatch>
																					<!--- catches invalid email addresses that have slipped through net --->
																					<cfset thisItemResult.OK = false>

																					<cfif cfcatch.message contains "Attribute validation"><!--- 7 --->
																						<cfset thisItemResult.dbFeedback = "Not Sent: Invalid Email _">
																						<cfset thisItemResult.commStatusId = -2>
																						<cfset thisItemResult.feedback = "Invalid EMail Address">
																					<cfelse>
																						<!--- WAB 2011/01/17 Record a warning rather than sending an error
																						TBD This Looks a bit of a mess
																						--->
																						<cfset warningStructure= { commid = this.commid,message = cfcatch.message, personid = personid }>
																						<cfif scheduledCommunication>
																							<cfset type = "Scheduled Communication Error">
																							<cfset severity = "Error">
																						<cfelse>
																							<cfset type = "Communication Error">
																							<cfset severity = "Warning">
																						</cfif>

																						<cfset application.com.errorHandler.recordRelayError_Warning (severity=severity,Type=type,Message="Communication #this.commid#. Sender #recipientQuery.Email#",warningStructure=warningStructure,catch=cfcatch )>

																						<cfset thisItemResult.dbFeedback = "">
																						<cfset thisItemResult.commStatusId = 'null'>
																						<cfset thisItemResult.feedback = "Not Sent.  Error during processing.  #getMessageFromCFCATCH(cfcatch)#">

																						<cfset variables.sendResultObject.setError (getMessageFromCFCATCH(cfcatch))>

																						<cfbreak>

																					</cfif>

																				</cfcatch>
																		</cftry>

																	<cfelse>

																		<cfset thisItemResult.OK = false>
																		<cfset thisItemResult.feedback = "Item already sent">
																		<!---  Note no dbFeedback since do not want to update the db--->

																	</cfif> <!--- end of if which checks that item not already sent --->



													  		<CFELSE>  <!--- same email as previous --->

																<cfset thisItemResult.dbFeedback = "Duplicate address">
																<cfset thisItemResult.commStatusId = -15>	  <!--- WAB 2004-07-19: was being set to 0 which made reports look as if it hadn't been sent  --->
																<cfset thisItemResult.OK = true>
																<cfset thisItemResult.feedback = "Duplicate Address">

															 </CFIF>

														<CFELSE><!--- Invalid email address --->

															<cfset thisItemResult.dbFeedback = "Not Sent: Invalid Email">
															<cfset thisItemResult.commStatusId = -2>
															<cfset thisItemResult.OK = false>
															<cfset thisItemResult.feedback = "Invalid EMail Address">

														</CFIF>

														<cfset previousItem.email=recipientQuery.Email>
													<cfif structKeyExists (thisItemResult,"dbFeedback")>
														<!--- here update just the specific person being processed --->
														<CFQUERY DATASOURCE="#application.SiteDataSource#" debug="#arguments.showdebugoutput#">
															UPDATE CommDetail
															   SET LocationID =  <cfqueryparam value="#recipientQuery.locationID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
																   Feedback =  <cfqueryparam value="#Left(thisItemResult.dbFeedback,30)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
																   Fax =  <cfqueryparam value="#recipientQuery.email#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
																   DateSent = getdate(),
																   commStatusId =  <cfqueryparam value="#thisItemResult.commStatusId#" CFSQLTYPE="CF_SQL_INTEGER" >
															 WHERE commdetailid =  <cfqueryparam value="#commDetailID#" CFSQLTYPE="CF_SQL_INTEGER" >
														</CFQUERY>
													</cfif>

											</cflock>	<!--- end of commItemLock --->

											<cfcatch>
													<cfset debugLog("Error (1) #cfcatch.message#")>

													<cfif structKeyExists(cfcatch,"lockname") and cfcatch.lockname is LockName_commitem>
														<cfset thisItemResult.OK = false>
														<cfset thisItemResult.feedback = "Item was locked when tried to send">
														<cfset variables.sendResultObject.setError ("Failure During Sending")>
													<cfelse>
														<cfset thisItemResult.OK = false>
														<cfset thisItemResult.feedback = "Error Message: #getMessageFromCFCATCH(cfcatch)#">
														<cfset variables.sendResultObject.setError ("Failure During Sending: #getMessageFromCFCATCH(cfcatch)#")>
														<cfset application.com.errorHandler.recordRelayError_Warning (severity="Error",Type="comm",Message=getMessageFromCFCATCH(cfcatch),catch=cfcatch )>

													</cfif>
													<cfbreak>

											</cfcatch>


										</cftry>

		<cftry>
										<cfset variables.sendResultObject.addItem (thisItemResult)>

			<cfcatch>
													<cfset debugLog("Error (2) #cfcatch.message#")>

												<cfmail from="william.bibby@relayware.com" to="william.bibby@relayware.com" subject="error saving result" type="html">
					<cfdump var="#thisItemResult#">
					<cfdump var="#cfcatch#">
				</cfmail>
			</cfcatch>
		</cftry>


									</CFLOOP>
<cfset debugLog("End Recipients")>


								</CFIF><!--- end thisCommResult.isOK (I think)--->



								<!---
								*****************************************
											Have now sent all the items in this comm (or selection)
											Time to update a few tables
								*****************************************
								 --->
								<cfset variables.sendResultObject.endTime = now()>

								<cfif variables.sendResultObject.isOK><!--- 2 --->
									<!--- update the commselection table to show that selection has been sent
									won't apply if personids have been passed  or if it is a test
									--->
									<CFIF arguments.PersonIDs is "" and not arguments.test><!--- 3 --->

										<CFQUERY NAME="updateCommSelection" DATASOURCE="#application.SiteDataSource#">
											update commselection
												set
													<cfif variables.sendResultObject.partialSend >
														sent = 3, -- sent set to 3 (partially sent).  for a scheduled communication will be picked up on next run of schedule, for a non scheduled communication will show as partially sent
													<cfelse>	<!--- WAB 2009/06/08 if a partial send then leave sent set to 3 (lets see what happens, maybe needs to be 2) will see what happens --->
														sent = 1,   <!--- 3= being sent, 2 = queued, 1 = sent --->
													</cfif >
											sentDate = getDate()
												where commid =  <cf_queryparam value="#this.commID#" CFSQLTYPE="CF_SQL_INTEGER" >
												and selectionid  in ( <cf_queryparam value="#createCommDetailTemporaryTableResult.selectionIDs#" CFSQLTYPE="cf_sql_integer"  list="true"> )
										</cfquery>
									</cfif>


								<cfif not arguments.Test>
									<!--- unlock the communication table --->
									<CFQUERY NAME="updateCommunication" DATASOURCE="#application.SiteDataSource#">
										UPDATE Communication
										   SET <!--- LastUpdated = #Now()#, WAB removed this because need to be able to tell when communication has actually been edited --->
														<cfif scheduledCommunication and arguments.selectionID is "" and not variables.sendResultObject.partialSend>  <!--- ie this was a scheduled communication which was sending the whole lot, just not a single selection , set sent to 1 to show that it has run --->
															sent = 1
														<cfelseif scheduledCommunication and arguments.selectionID is "" and variables.sendResultObject.partialSend>
															<!--- partial sending of a scheduled whole communication, not sure exactly what to do --->
															sent = sent
														<cfelse>
															<!--- if a communication had been scheduled and then the user has clicked on send now,
																we need to check whether there are any other items waiting to go out.
																If there aren't then set sent = 1
																--->
															sent = case when
																		senddate is not null
																			and not exists (select * from commdetail cd where cd.commid = communication.commid and feedback = '' and test = 0)
																	and not exists (select 1 from commselection cs where cs.commid = communication.commid and sent = 0 and commSelectLID in (0,#communications.constants.approverSelectLID#,#communications.constants.internalSelectLID#) AND selectionid <> 0)
															then 1 else 0 end
														</cfif>

											WHERE CommID =  <cf_queryparam value="#this.commID#" CFSQLTYPE="CF_SQL_INTEGER" >
									</CFQUERY>
								</cfif>

										<!--- Only send a confirmation if attributes.sendConfirmation --->
										<CFIF arguments.sendConfirmation >
											<cfset variables.sendResultObject.sendConfirmation()>
										</CFIF>

								<cfelse><!--- ie NOT thisCommResult.isOK --->

									<!--- if an error has happened half way through sending then there may be a need to send a confirmation --->
									<cfif arguments.sendConfirmation and arrayLen(variables.sendResultObject.ItemArray) gt 0>
											<cfset variables.sendResultObject.sendConfirmation()>
									</cfif>
								</cfif> <!--- END thisCommResult.isOK --->


					</CFIF><!--- END 1 - end of CFIF thisCommResult.isOK --->



					<cfcatch>
							<cfset warningStructure= { commid = this.commid,message = cfcatch.message}>
							<cfif scheduledCommunication>
								<cfset type = "Scheduled Communication Error">
								<cfset severity = "Error">
							<cfelse>
								<cfset type = "Communication Error">
								<cfset severity = "Warning">
							</cfif>

						<cfset errorID = application.com.errorHandler.recordRelayError_Warning (severity=severity,Type=type,Message="Communication #this.commid#. #cfcatch.message#",warningStructure=warningStructure,catch=cfcatch )>

						<cfset debugLog("Error (3) #errorID# #cfcatch.message#")>

						<!--- check error --->
						<cfset variables.sendResultObject.setError ("Communication could not be sent. Error Message: #getMessageFromCFCATCH(cfcatch)#<BR>",errorid)>

					</cfcatch>

				</cftry>

				<cftry>
					<cfset variables.sendResultObject.save()>
					<cfcatch>
						<cfset debugLog("Error (4) #cfcatch.message#")>
					</cfcatch>
				</cftry>

					<!--- WAB 2009/11/02  LID 2816
					delete the temporary communication files
					decided to do this after implementing unique file names for each send
					--->
					<cfif not debugSaveSentEmailContent>
						<cfset cleanUpMergeFiles()>
					</cfif>

				</cf_lock> <!--- end of lock for individual communication --->

				<cfif cf_lock.timedOut>
					<cfset message = "Communication could not be sent.<BR>Being sent by another process<BR>">
					<cfif arrayLen(cf_lock.lockDetail)>
						<cfset message = message & "(#cf_lock.lockDetail[1].metadata.message#)"	>
					</cfif>
					<cfset variables.sendResultObject.setError (message)>
					<cfset variables.sendResultObject.save()>
				</cfif>

		<cfset resetPreparedContent	()>


	<cfreturn variables.sendResultObject>

</cffunction>

	<cffunction name="getLockName" output="false">
		<cfargument name="test" default="false">
		<cfreturn communications.getCommunicationLockName(commid = this.commid, test = test)>
	</cffunction>

	<cffunction name="checkIfCommunicationIsBeingSent" output="false">

		<cfset var result = application.com.globalFunctions.getProcessLock (getLockName())>
		<cfset result.isBeingSent = result.lockExists>

		<cfreturn result>
	</cffunction>


	<!---
	Omniture Tracking
	Re-written for 2013

	Some background.
	Originally written for Lenovo, but could be used by other clients.  Not sure whether tracking string would need to be customised.  It could be made a setting
	Omniture tracking strings are added to urls whose domains match those in the setting Communications.Tracking.OmnitureTrackableUrls
	In Lenovo's case the tracking string contains some Items which are constant for a given communication (commid, campaignname) and some items which vary by recipient (ISO Code)

	Within the mergeObject there are a number of functions which create URLs (such as relayPageLink())and also the trackedLink() function which does our own tracking
	All these functions include a call to the AddCustomTrackingToURL() function.
	Normally AddCustomTrackingToURL() is just a placeholder which returns the URL which it has been given.
	However when I need to do omniture tracking I can override it with and 'injected' function which adds the omniture tracking.

	I was worried that this approach might be a little slow on documents with large numbers of links (there is an evaluate in the function!),
	Therefore I have implemented an additional technique for links where we know the URl at the time when the content is being prepared.
	In this case I can add the tracking string (with [[ISOCODE]] in it) to the URl before the merging is done.


	To simplify (?) the code I use the same function [addOmnitureMergeStringToURL] to do both these operations, but have an evaluateMergeString argument to indicate which method is being used

	 --->

	<cffunction name="useOmnitureTracking" output="false">

		<cfif not structKeyExists (variables.data,"useOmnitureTracking")>
			<cfset variables.data.useOmnitureTracking = false>
			<cfif this.trackemailWithOmniture and application.com.settings.getSetting('Communications.Tracking.AllowOmnitureTracking')>
				<cfset variables.data.useOmnitureTracking = true>
			</cfif>
		</cfif>

		<cfreturn variables.data.useOmnitureTracking >
	</cffunction>

	<cffunction name="getOmnitureData" output="false">
		<cfif not structKeyExists (variables,"omnitureData")>
			<cfset variables.omnitureData = {}>
			<cfset variables.omnitureData.TrackableURLsRegExp = application.com.communications.getOmnitureTrackableURLsRegExp()>
			<cfset variables.omnitureData.mergeString = "cid=[[isocode]]|edm|em|relay|#this.campaignName#|#this.CommID#">
			<cfset variables.omnitureData.evaluateString = "'" & reReplace(variables.omnitureData.mergeString,"\[\[|\]\]","##","ALL") & "'">
			<cfset variables.omnitureData.testRegExp = "cid=">
		</cfif>

		<cfreturn variables.omnitureData>

	</cffunction>

	<cffunction name="addOmnitureMergeStringToURL_NoEvaluation" access="public" output="false">
		<cfargument name="theURL" required="true">
		<cfreturn addOmnitureMergeStringToURL(theURL = theURL, evaluateMergeString = false)>
	</cffunction>

	<cffunction name="addOmnitureMergeStringToURL" access="public" output="false">
		<cfargument name="theURL" required="true">
		<cfargument name="evaluateMergeString" default="true">

		<cfset var result = theURL>

		<cfif not structKeyExists (variables,"omnitureData")>
			<cfset getOmnitureData()>
							</cfif>

		<!--- If this URL already has a tracking string, or the domain is not one which is being tracked, just retuun --->
		<cfif omnitureData.TrackableUrlsRegExp is "" or refindNocase(omnitureData.TrackableUrlsRegExp,theURL) is 0 or refindNocase(omnitureData.testRegExp,theURL) is not 0>
			<cfreturn result>
						</cfif>

		<!--- parse the URL into it constituent parts --->
		<cfset parseURLResult = application.com.regExp.parseURL(theURL)>

				<cfif arrayLen(parseURLResult)>

						<cfset urlStruct = parseURLResult[1]>
								<cfset conjunction = "&">
						<cfif urlStruct.queryStringWithQuestionMark is "">
									<cfset conjunction = "?">
								</cfif>
						<cfif evaluateMergeString>
							<cfset mergeString = evaluate (omnitureData.evaluateString)> <!--- this is the version used when called withint he merge object--->
						<cfelse>
							<cfset mergeString = omnitureData.mergeString> <!--- this is the version used when preparing the merge text --->
							</cfif>

						<cfset newString = urlStruct.protocolDomainAndPath & urlStruct.queryStringWithQuestionMark & conjunction & mergeString & urlStruct.fragmentWithHash>
						<!---  in case the regexp has not picked up the beginning of the URL, I'll actually do a replace--->
						<cfset result = replace(result,urlStruct.string,newString)>

						</cfif>

		<cfreturn result>

	</cffunction>





	<cffunction name="getDatabaseID" output="false">

		<cfset var getDatabaseIDqry = '' />

		<cfif not StructKeyExists(variables,"databaseid")>
			<!--- 	Set databaseID for mail FROM address--->
			<CFQUERY name="getDatabaseIDqry" datasource="#application.sitedatasource#">
				SELECT databaseid FROM coldfusioninstance_autopopulated  where databaseid is not null
			</CFQUERY>
			<cfset variables.databaseid = getDatabaseIDqry.databaseid>
		</cfif>

		<cfreturn variables.databaseid>
	</cffunction>



	<cffunction name="getMessageFromCFCATCH" output="false">
		<cfargument name="catch">

		<!--- WAB 2013-09-02 fixed bug I was referring to cfcatch not arguments.catch--->
		<cfset var result = arguments.catch.message>
		<cfset var fileAndLine= "">

		<cfif structKeyexists (arguments.catch,"tagContext")>
			<cfset fileAndLine = application.com.errorHandler.getErrorFileAndLineFromTagContext(arguments.catch.tagContext)>
			<cfset result = result & " Line #fileAndLine.line# of #fileAndLine.filename#">
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="FormatAllContentErrorArrays_HTML" output="false">
		<cfargument name="mergeResult">

		<cfset var result = "">
		<cfset var errorFound = false>
		<cfset var allErrorArrays = '' />
		<cfset var errorArray = '' />
		<cfset var errorArrayPointer = '' />

		<cfset allErrorArrays = structFindKey (mergeResult,"errorArray","ALL")>

		<cfloop array="#allErrorArrays#" index="errorArrayPointer">
			<cfset errorArray = errorArrayPointer.value>
			<cfif arrayLen(errorArray)><cfset errorFound = true></cfif>
			<cfset result &= FormatContentErrorArray_HTML(errorArray)>
		</cfloop>

		<cfif errorFound>
			<cfset result = "The following Merge Field errors were found: <UL>#result#</UL>">
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="FormatContentErrorArray_HTML" output="false">
		<cfargument name="errorArray">

		<cfset var item = '' />
		<cfset var result = '' />

		<cfsaveContent variable="result">
			<cfloop array="#errorArray#" index="item">
			<cfoutput><LI>#item.Message#</LI></cfoutput>
			</cfloop>
		</cfsaveContent>

		<cfreturn result>

	</cffunction>

	<cffunction name="debugLog" output="false">
		<cfargument name="Label" default="">
		<cfargument name="struct">

		<cfset var content = '' />
		<cfset var totalDiff = '' />
		<cfset var thisDiff = '' />
		<cfset var memorypercent = '' />

		<cfif isDebug()>
			<cfif not structKeyExists (variables.debug,"initialised")>
				<cfset variables.debug.initialised = true>
				<cfset content = "<TABLE><tr><td>Elapsed Time</td><td>Time Diff</td><td>Mem %</td><td>Buffer</td><td>Label</td><td>Data</td></tr>">
				<cffile action="append" file="#variables.debug.filepath#" output="#content#" addnewline="true">
			</cfif>

			<cfset variables.debug.now = getTickCount()>
			<cfset totalDiff = variables.debug.now - variables.debug.start>
			<cfset thisDiff = variables.debug.now - variables.debug.previous>
			<cfset memorypercent = application.com.globalfunctions.getmemory().percentUsedAllo>

			<cffile action="append" file="#variables.debug.filePath#" output="<tr><td>#totaldiff#</td><td>#thisdiff#</td><td>#memorypercent#</td><td>#len(getPageContext().getCFOutput().getBuffer().tostring())#</td><td>#label#</td><td>" addnewline="true">
			<cfif structKeyExists (arguments,"struct")>
				<cfdump var="#struct#" output=#variables.debug.filePath# format="html">
			</cfif>
			<cffile action="append" file="#variables.debug.filePath#" output="</td></tr>" addnewline="true">

			<cfset variables.debug.previous = variables.debug.now>

		</cfif>

	</cffunction>


<!--- update CommunicationCountryIDList
	updates the field which stores a list of countryids (so that can be accessed quicker than running a whole query)
	needs to be run when
		selections added
		selections removed
		by populatecommdetail
--->

	<cffunction access="public" name="updateCountryIDList" hint=""  output="false">

			<cfset var newCountryIDList = "">
			<cfset var getCommunication = "">
			<cfset var updateCommunication = "">
			<cfset var currentCountryIDList = '' />

				<cfset currentCountryIDList = this.countryIDList>
				<cfset newCountryIDList = getCountryIDListFromDB()>

				<cfquery name="updateCommunication" datasource="#application.siteDataSource#">
				update communication
				set CountryIDList =  <cf_queryparam value="#newCountryIDList#" CFSQLTYPE="CF_SQL_VARCHAR" >
				where commid =  <cf_queryparam value="#this.commid#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>

				<cfif currentCountryIDList is not newCountryIDList>
					<cfset CountryIDListChanged (originalCountryIDList = currentCountryIDList , newCountryIDList = newCountryIDList)>
				</cfif>


	</cffunction>


	<!--- this function run when the list of countries in a communication changes --->
	<cffunction access="private" name="CountryIDListChanged" hint=""  output="false">
			<cfargument name="originalCountryIDList" type="string" required="true">
			<cfargument name="newCountryIDList" type="string" required="true">

			<cfset var getNewCountries = "">
			<!---  if this is the first country to be added to the communication then add all the default selections
			--->

			<cfif arguments.originalCountryIDList is "">
				<!--- actually still being done when communication created --->

			</cfif>

			<!--- get new countries --->
			<cfquery name="getNewCountries" datasource="#application.siteDataSource#">
			select countryid from country where
				<cfif newCountryIDList is not "">
					countryid  in ( <cf_queryparam value="#newCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<cfelse>
					1 = 0
				</cfif>
				<cfif arguments.originalCountryIDList is not "">
				and countryid  not in ( <cf_queryparam value="#arguments.originalCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>

			</cfquery>

			<!--- add any selections specific to the new countries --->
			<cfloop query= "getNewCountries">
				<cfset addDefaultCountrySelections (countryid = countryid)>
			</cfloop>


	</cffunction>


	<cffunction access="public" name="SaveCommContent" returnType="struct"
		hint="Saves Body Text/HTML of a comm" output="false">
		<cfargument name="HTMLBody" type="string" >
		<cfargument name="TextBody" type="string" >

		<cfset var result = {isOK=true,contentUpdated=false}>
		<cfset var tempresult = "">
		<cfset var argumentCollection = "">

			<cfif structKeyExists (arguments,"HTMLBODY")>
				<cfset argumentCollection = {entityTypeID=application.entityTypeID.communication,entityID = this.CommID,languageid = 0 , countryid = 0,phrasetext = communications.doctorHTMLbeforeSave(HTMLBODY),phraseTextID='HTMLBody'} >
				<cfset tempResult = application.com.relayTranslations.addNewPhraseAndTranslationV2(argumentCollection = argumentCollection)>
				<cfset result.contentUpdated = result.contentUpdated OR tempResult.details.TRANSLATIONADDEDUPDATED>
			</cfif>
			<cfif structKeyExists (arguments,"TEXTBODY")>
				<cfset argumentCollection = {entityTypeID=application.entityTypeID.communication,entityID = this.CommID,languageid = 0 , countryid = 0,phrasetext = TextBody,phraseTextID='TextBody' } >
				<cfset tempResult = application.com.relayTranslations.addNewPhraseAndTranslationV2(argumentCollection = argumentCollection)>
				<cfset result.contentUpdated = result.contentUpdated OR tempResult.details.TRANSLATIONADDEDUPDATED>
			</cfif>

			<cfif result.contentUpdated>
				<cfset clearContent()>
				<cfset updateContentLastUpdated()>
				<cfset isContentOK(refresh =true)>
			</cfif>


		<cfreturn result>


	</cffunction>

	<cffunction access="public" name="updateContentLastUpdated" returnType="boolean"
		hint="updates contentLastUpdated - to be called whenever a content change is made" output="false">

		<CFQUERY DATASOURCE="#application.SiteDataSource#">
				UPDATE Communication
			   SET 	ContentLastUpdated=getdate(),
					ContentLastUpdatedBy=#request.relayCurrentUser.userGroupID#,
					PreviewReviewed=0 <!--- WAB 2011/11/14 added this.  Note that will be updated if attachments are added/deleted, not sure whether this is strictly correct --->
				 WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >
		</CFQUERY>

		<cfset deleteMetaData("contentValidated")>

		<cfreturn true>
	</cffunction>




	<!--- Add Default Selections --->
	<cffunction access="public" name="addDefaultSelections" hint=""  output="false">
			<cfargument name="personid" type="numeric" default= #request.relayCurrentUser.personid#>

			<cfset var commSelectLID = "">
			<cfset var selectionList = "">
			<cfset var defaultSelections = application.com.settings.getSetting(variablename = "communications.defaultSelections", reloadOnNull = false)>
				<cfloop list = #structKeyList(defaultSelections)# index="commSelectLID" >
					<cfset selectionList = defaultSelections[commSelectLID]>
					<cfif selectionList is not "">
						<cfset addselection(selectionid = selectionList , commSelectLID = commSelectLID)>
					</cfif>
				</cfloop>

	</cffunction>



	<!--- Add Default Country  Selections --->
	<cffunction access="public" name="addDefaultCountrySelections" hint=""  output="false">
			<cfargument name="countryid" type="numeric" required="true">
			<cfargument name="personid" type="numeric" default= #request.relayCurrentUser.personid#>

			<cfset var commSelectLID = "">
			<cfset var selectionList = "">
			<cfset var defaultCountrySelections = application.com.settings.getSetting(variablename = "communications.DefaultCountrySelections", reloadOnNull = false)>

				<cfif structKeyExists(defaultCountrySelections,countryid)>
					<cfloop list = #structKeyList(defaultCountrySelections[countryid])# index="commSelectLID" >
						<cfset selectionList = defaultCountrySelections[countryid][commSelectLID]>
						<cfif selectionList is not "">
							<cfset addselection(selectionid = selectionList , commSelectLID = commSelectLID)>
						</cfif>
					</cfloop>
				</cfif>

	</cffunction>




	<cffunction access="public" name="updateSelections" hint="Update selections in a communication" output="false">
		<cfargument name="selectionid" type="string" required="true" > <!--- can be value list --->
		<cfargument name="CommSelectLID" type="numeric" default="0">
		<cfargument name="delayCountryRefresh" type="boolean" default="false">

		<cfset var deleteSelections = '' />
		<cfset var deleteSelectionsResult = '' />

		<cfparam name="variables.countryRefreshRequired" default="false">

		<CFQUERY NAME="deleteSelections" DATASOURCE="#application.SiteDataSource#" result="deleteSelectionsResult">
		DELETE
		FROM CommSelection
		WHERE
				CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND CommSelectLID =  <cf_queryparam value="#arguments.CommSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfif arguments.selectionID is not "">
				AND selectionid  not in ( <cf_queryparam value="#arguments.selectionid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			<!--- WAB 2013-06-10 You should not be able to remove selections which have already been sent --->
			and sent not in (select sent from commselectionsentstatus where sentTextID in ('sent','partsent'))
		</cfquery>

		<cfset setUpdateCountryIDList(deleteSelectionsResult.recordcount)>

		<cfif selectionid is not "">
			<cfset addSelection (selectionID = arguments.selectionid, commSelectLID = arguments.commSelectLID, delayCountryRefresh = true,personid = request.relaycurrentuser.personid)>
		</cfif>

		<cfset UpdateCountryIDListIfRequired(delayCountryRefresh)>

	</cffunction>


	<cffunction name="setUpdateCountryIDList" output="false">
		<cfargument name="recordCount" required="true">

		<cfif recordCount is not 0 >
			<cfset variables.countryRefreshRequired = true>
		</cfif>

	</cffunction>

	<cffunction name="UpdateCountryIDListIfRequired" output="false">
		<cfargument name="delay" default="false">

		<cfif not Delay and structKeyExists(variables,"countryRefreshRequired") and variables.countryRefreshRequired>
			<cfset updateCountryIDList()>
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Add a selection to a communication
			 WAB 2005-04-14
			 turned out there was another one with the same name which didn't check whether the user
			 had rights to the selection.  For compatability I will leave a version which doesn't check for rights
			 And actually there may beinstances when it isn't necessary to check rights (eg selections added automatically)
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="addSelection" hint="Add a selection to a communication" output="false">
		<cfargument name="selectionid" type="string" required="true" > <!--- can be value list --->
		<cfargument name="CommSelectLID" type="numeric" default="0">
		<cfargument name="personID" type="numeric" default="0">
		<cfargument name="delayCountryRefresh" type="boolean" default="false">

		<cfset var saveSelection = "">
		<cfset var saveSelectionResult = '' />

		<cfif personID is not 0>
			<CFQUERY NAME="saveSelection" DATASOURCE="#application.SiteDataSource#" result="saveSelectionResult">
				INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID,created,createdbyPersonID)
				SELECT distinct <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >, s.SelectionID,

				<cfif CommSelectLID is 0>
								<!--- WAB 2005-12-13
								Added a check to prevent selections containing only "internal" people being given commselectLID of an external selection
								could be a problem if other people are then added to the selection but ....
								started off as a separate query run first, but discovered that that didn't work if more than one selection being added at a time, so now it is in the form of a subquery
								 --->
						 case when not exists (
											 select 1 from
												 selectiontag st
												 	inner join
												person p on st.entityID = p.personid and st.status > 0
													left join
												vadminCompany v on v.organisationid = p.organisationid
												 where st.selectionid = s.selectionid and v.organisationid is null)
									then <cf_queryparam value="#this.constants.internalSelectLID#" CFSQLTYPE="cf_sql_integer" > else <cf_queryparam value="#arguments.CommSelectLID#" CFSQLTYPE="cf_sql_integer" > end,
				<cfelse>
					 <cf_queryparam value="#arguments.CommSelectLID#" CFSQLTYPE="cf_sql_integer" >,
				</cfif>

				 getDate(),
				 <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >
			  	FROM Selection AS s
						inner join
					SelectionGroup AS sg on sg.SelectionID = s.SelectionID
						inner join
					RightsGroup rg on 	sg.usergroupid = rg.usergroupid
			  	WHERE s.SelectionID  IN ( <cf_queryparam value="#arguments.SelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND rg.personID = <cf_queryparam value="#arguments.personid#" CFSQLTYPE="cf_sql_integer" >
					<!--- WAB added check for selection not already being attached to the comm --->
					and s.selectionid not in (select selectionid from commselection where commid =  <cf_queryparam value="#this.commID#" CFSQLTYPE="cf_sql_integer" > )
			</CFQUERY>


		<cfelse>
			<CFQUERY NAME="saveSelection" DATASOURCE="#application.SiteDataSource#" result="saveSelectionResult">
				INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID,created,createdbyPersonID)
				SELECT <cf_queryparam value="#this.CommID#" CFSQLTYPE="CF_SQL_INTEGER" >, s.SelectionID, <cf_queryparam value="#arguments.CommSelectLID#" CFSQLTYPE="CF_SQL_INTEGER" >, getDate(), #request.relayCurrentUser.personid#
				FROM Selection AS s
				WHERE s.SelectionID  IN ( <cf_queryparam value="#arguments.SelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				and s.selectionid not in (select selectionid from commselection where commid =  <cf_queryparam value="#this.commID#" CFSQLTYPE="cf_sql_integer" > )
			</CFQUERY>
		</cfif>

		<!--- updates the field which caches a list of countries being communicated to --->
		<cfset setUpdateCountryIDList(saveSelectionResult.recordcount)>

	</cffunction>





<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Remove selection from a communication
			 WAB 2005-04-14
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="removeSelection" hint="Remove a selection from a communication" output="false">
		<cfargument name="selectionid" type="string" required="true" > <!--- can be value list --->

		<!--- Will only remove a selection that hasn't already been sent to --->
			<CFQUERY DATASOURCE="#application.SiteDataSource#">
				delete from CommSelection
				where
					commid =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >
					and selectionid =  <cf_queryparam value="#arguments.SelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and isNull(Sent,0) = 0
			</CFQUERY>

			<!--- updates the field which caches a list of countries being communicated to --->
			<cfset updateCountryIDList()>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             GetCommFileList
			 Returns a query with list of files attached to a communication
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="GETFILELIST" returnType="query" hint="Returns query with of commfiles of particular type" output="false">
		<cfargument name="filetype" type="string" required="true"><!--- emailcontent,emailattachment,faxcontent --->
		<cfargument name="commFileID" type="numeric" required="false">

		<cfset var CommFileDetails = '' />

		<CFQUERY name="CommFileDetails" datasource="#application.sitedatasource#">
		SELECT * ,
		'#application.paths.userFiles#\'+directory +'\' + filename as absolutepath,
		'#request.currentSite.protocolAndDomain#'+directory +'/' + filename as url
		FROM CommFile
		WHERE CommFile.CommID =  <cf_queryparam value="#this.commid#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfif filetype is "emailattachment">
		and commformlid = 2 and commfileid <> isparent
		<cfelseif filetype is "emailcontent" >
		and commformlid = 2 and commfileid = isparent
		<cfelseif filetype is "faxcontent" >
		and commformlid = 3 and commfileid = isparent
		</cfif>
		<cfif structKeyExists(arguments,"commFileID")>
			and commFileID = #arguments.commFileID#
		</cfif>
		</cfquery>

		<cfreturn commfiledetails>




	</cffunction>


	<!--- NJH 2010/09/24 - delete a comm file --->
	<cffunction name="deleteFile" access="public" returnType="struct" output="false">
		<cfargument name="filetype" type="string" required="true"><!--- emailcontent,emailattachment,faxcontent --->
		<cfargument name="commFileID" type="numeric" required="false">

		<cfset var result = structNew()>
		<cfset var deleteCommFileQry = "">
		<cfset var fileDetails = GetFileList(argumentCollection=arguments)>

		<cfset result.isOK = true>
		<cfset result.message = "Successfully deleted file">

		<cfif fileDetails.recordCount eq 1>
			<cftry>
				<cffile action="delete" file="#fileDetails.absolutePath#">

				<cfcatch>
					<cfset result.isOk = false>
					<cfset result.message = cfcatch.message>
				</cfcatch>
			</cftry>
		</cfif>

		<cfif not fileExists(fileDetails.absolutePath) and fileDetails.recordCount eq 1>
			<cfquery name="deleteCommFileQry" datasource="#application.siteDataSource#">
				delete from commFile where commFileID =  <cf_queryparam value="#fileDetails.commFileID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset updateContentLastUpdated()>

		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  		 WAB 2005-04-14
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="schedule" hint="schedule a communication (or part of it) to be sent" output="false">
		<cfargument name="date" type="date" required="true">
		<cfargument name="selectionid" type="string" default = "" > <!--- can be value list --->
		<cfargument name="selectLID" type="string" default = "" > <!--- can be value list --->

		<cfset var scheduleCommunication = '' />
		<cfset var scheduleCommunicationSelection = '' />

		<cfif selectionid is "" and selectLID is "">
			<!--- scheduling whole communication --->
				<CFQUERY NAME="scheduleCommunication" DATASOURCE="#application.SiteDataSource#">
				UPDATE Communication
				   SET 	 senddate = #CreateODBCDateTime(Date)#,
						sent = 0,
				       CommStatusID = 0
				WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >
				</CFQUERY>


		<cfelse>
			<!--- scheduling part of a communication --->
				<CFQUERY NAME="scheduleCommunicationSelection" DATASOURCE="#application.SiteDataSource#">
				UPDATE CommSelection
				   SET 	 senddate = #CreateODBCDateTime(Date)#,
						sent = 2   <!--- queued --->
				WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >
					and
					<cfif selectionID is not "">
					selectionid  in ( <cf_queryparam value="#selectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					<cfelse>
					commselectLID  in ( <cf_queryparam value="#selectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</cfif>
					and sent in (0,2)  <!--- only reschedule if not already sent --->
				</CFQUERY>

		</cfif>


	</cffunction>

	<cffunction access="public" name="unschedulePart" hint="removed a communication (or part of it) from schedule " output="false"				>
		<cfargument name="selectionid" type="string" default = "" > <!--- can be value list --->
		<cfargument name="selectLID" type="string" default = "" > <!--- can be value list --->



		<cfif selectionid is "" and selectLID is "">
				<!--- unscheduling whole communication --->
				<CFQUERY DATASOURCE="#application.SiteDataSource#">
				UPDATE Communication
				   SET 	 senddate = null
				WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" > and sent = 0  <!--- only unschedule unsent comms --->
				</CFQUERY>

		<cfelse>
			<!--- un scheduling part of a communication --->
				<CFQUERY DATASOURCE="#application.SiteDataSource#">
				UPDATE CommSelection
				   SET 	 senddate = null,
						sent = 0   <!--- queued --->
				WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >
					and
					<cfif selectionID is not "">
					selectionid  in ( <cf_queryparam value="#selectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					<cfelse>
					commselectLID  in ( <cf_queryparam value="#selectLID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</cfif>
					and sent in (2)  <!--- only unreschedule if not already sent --->
				</CFQUERY>

		</cfif>


	</cffunction>

	<cffunction access="public" name="unschedule" hint="unschedules communication (selections and whole communication)" output="false">

				<!--- unscheduling whole communication --->
				<CFQUERY DATASOURCE="#application.SiteDataSource#">
				UPDATE Communication
				   SET 	 senddate = null
				WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" > and sent = 0  <!--- only unschedule unsent comms --->
				</CFQUERY>

				<!--- unschedule selections whole communication --->
				<CFQUERY DATASOURCE="#application.SiteDataSource#">
				UPDATE CommSelection
				   SET 	 senddate = null,
						sent = 0
				WHERE CommID =  <cf_queryparam value="#this.CommID#" CFSQLTYPE="cf_sql_integer" >
					and sent in (2)  <!--- only unschedule if not already sent --->
				</CFQUERY>

	</cffunction>

	<cffunction name="isScheduled" output="false">

			<cfset var result = false>
			<cfset var checkSelectionScheduled = "">
			<cfset var selectionDetails = getSelectionDetails()>

			<cfquery name="checkSelectionScheduled" dbtype="query">
			select 1 from selectionDetails where scheduled = 1
			</cfquery>

			<cfset result = ((this.sendDate is not "" AND this.sent is 0) OR checkSelectionScheduled.recordCount)>

		<cfreturn result>

	</cffunction>

	<cffunction name="getSchedule" output="false">

			<cfset var result = queryNew("senddate,x","date,varchar")>
			<cfset var selectionDetails = getSelectionDetails()>

			<cfif this.sent is 0 and this.sendDate is not "">
				<cfset queryAddRow (result)>
				<cfset querySetCell (result,"senddate",createodbcdatetime(this.sendDate))>
				<cfset querySetCell (result,"x","Whole Communication")>
			</cfif>

			<cfquery name="result" dbtype="query">
			select sendDate, x from result
			union
			select sendDate, 'Selection ' + cast (selectionID as varchar) from SelectionDetails where scheduled = 1
			</cfquery>


		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="SaveCommMessage" returnType="struct" hint="Saves the message text of an activity stream" output="false">
		<cfargument name="text" type="string" required="true">
		<cfargument name="image" type="string" required="true">
		<cfargument name="url" type="string" required="true">
		<cfargument name="title" type="string" required="true">
		<cfargument name="sendMessage" type="Boolean" required="true">

		<cfset var result = {isOK=true,contentUpdated=false}>
		<cfset var tempresult = "">
		<cfset var argumentCollection = {entityTypeID=application.entityTypeID.communication,entityID = this.CommID,languageid = 0 , countryid = 0,phrasetext = "",phraseTextID=""}>
		<cfset var argStruct = structNew()>
		<cfset var arg = "">
		<cfset var setSendNotification = "">

		<cfloop collection="#arguments#" item="arg">
			<cfset argStruct = argumentCollection>
			<cfset argStruct.phraseText=arguments[arg]>
			<cfset argStruct.phraseTextID="message"&arg>
			<cfset tempResult = application.com.relayTranslations.addNewPhraseAndTranslationV2(argumentCollection = argumentCollection)>
			<cfset result.contentUpdated = result.contentUpdated OR tempResult.details.TRANSLATIONADDEDUPDATED>
		</cfloop>

		<cfquery result="setSendNotification" datasource="#application.siteDataSource#">
			update communication set sendMessage=#sendMessage#, lastUpdated=getdate(),lastUpdatedBy=#request.relayCurrentUser.userGroupId# where commID=#frmCommID# and sendMessage <> #sendMessage#
		</cfquery>
		<cfset result.contentUpdated = result.contentUpdated OR setSendNotification.recordcount>

		<cfset this.sendMessage = sendMessage>  <!--- Need to do this in case the value has changed.  Need to make sure that when we check isContentOK, we know whether to check the message or not  --->

		<cfif result.contentUpdated>
			<cfset clearContent()>
			<cfset updateContentLastUpdated()>
			<cfset isContentOK(refresh =true)>
		</cfif>


		<cfreturn result>
	</cffunction>


	<cffunction access="public" name="getApprovalSettings" returnType="struct" hint="" output="false">

		<cfif not structKeyExists(variables.data,"approvalSettings")>

			<cfset variables.data.approvalSettings = application.com.settings.getSetting("communications.approvals")>

		</cfif>

			<cfreturn variables.data.approvalSettings>

	</cffunction>

	<cffunction name="isApprovedOrPartlyApproved" output="false">

		<cfset var approvaldetails = getApprovalDetails()>

		<cfset var result = hasEMailContent() and approvaldetails.requiresApproval and (approvaldetails.approvalProcessType is "Gatekeeper" and (approvaldetails.isApprovedByGatekeeper OR approvaldetails.isPartlyApprovedByGatekeeper))>

		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="getEditMessageBlock" returnType="string" hint="A block of text which is output if Editing will cause commto be unapproved" output="false">

		<cfset var approvaldetails = getApprovalDetails()>
		<cfset var result = "">

		<cfif isApprovedOrPartlyApproved()>
			<cfsavecontent variable="result">
			<div class="warningblock">
				This Communication is Approved/Partly Approved.<br />
				Editing the content will cancel the Approval.
			</div>
			</cfsavecontent>
		<cfelseif approvaldetails.editedSinceApproval >
			<cfsavecontent variable="result">
			<div class="warningblock">
			This Communication has been edited since it was Approved.<br />
			Please repeat Approval Process.
			</div>
			</cfsavecontent>

		<!---
		<cfelseif isScheduled()>
			<cfsavecontent variable="result">
			<div class="infoblock">
			This Communication is Scheduled.<br />
			Editing the content now is not recommended.
			</div>
			</cfsavecontent>
		--->

		</cfif>

		<cfreturn result>

	</cffunction>

	<!---
	WAB 2013-05

	Moved logic for working out which communication tab to show and what message to display from commDetailsHome.cfm
	I have reduced the number of messages which get shown, so that it is only important stuff.
	Not really worth saying "Content Needs Creating" if user is going to be navigated to the content tab anyway

	TBD  Note still a bit of work which could be done here refining which messages are shown - there are a few phr_s floating around


	 --->
	<cffunction access="public" name="getStatusMessageAndNextPage" returnType="struct" hint="Decision function and message for commDetailsHome" output="false">
		<cfargument name="nextPage" default="">

		<cfset result = {message ="",type="info", status=""}>
		<cfif arguments.nextpage is not "">
			<cfset result.nextpage = nextpage>
		</cfif>

			<cfset mergeStruct = {}>
					<cfif checkIfCommunicationIsBeingSent().isBeingSent and hasUnsentCommDetailRecords() is 1>
						<cfset result.message = "phr_Sys_Comm_CommBeingSent">

					<cfelseif structKeyExists(this.metaData,"contentValidated") and NOT this.metaData.contentValidated>
						<cfset result.message = "Merge Field Error">
						<cfset result.type="error">
						<cfparam name="result.nextpage" default="content">

					<cfelseif this.sendDate is not "" and (this.hasUnsentCommDetailRecords() is 1 OR this.hasExternalSelectionsWaitingToSend IS 1) and this.sent is not 1>
						<cfset mergeStruct.ScheduledTime = application.com.datefunctions.datetimeformat(date = this.sendDate, showAsLocalTime = true)>
						<cfset result.message = "phr_Sys_Comm_CommScheduledToBeSentOn">
						<!---
						<cfif this.canBeEdited() and not userIsAllowedToSendNow>
							phr_Sys_Comm_CanBeEditedNotSent
						<cfelseif this.canBeEdited() and userIsAllowedToSendNow and this.isOKToSendNow()>
							phr_Sys_Comm_CanBeEditedAndSent
						<cfelseif not this.canBeEdited() and userIsAllowedToSendNow and this.isOKToSendNow()>
						</cfif>
						--->
					<cfelseif this.commformlid is 2 and (this.hasEmailContent() eq 0 or (structkeyexists(form,"FRMEMAILTEMPLATE") eq "true" and (form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")))>
						<cfparam name="result.nextpage" default="content">
						<!--- phr_sys_comm_CommAwaitingContents --->
					<CFELSEIF this.hasLink() is 1 and this.LinksReviewed is not 1 and (structkeyexists(form,"FRMEMAILTEMPLATE") eq "false" or form.FRMEMAILTEMPLATE neq "" or form.FRMEMAILTEMPLATE neq "0")>
						<cfparam name="result.nextpage" default="Links">
						<!--- phr_sys_comm_CommAwaitingLinksReview --->
					<!--- NJH 2013/02/14 Sprint 3 - go to message tab if not filled in and we're sending a message--->
					<cfelseif (not this.hasMessageContent() and this.sendMessage) and useMessage()>
						<cfparam name="result.nextpage" default="Message">
					<CFELSEIF this.PreviewReviewed is not 1>
						<cfif not structKeyExists (result,"nextpage") OR result.nextpage is "Send">
							<cfset result.NextPage = "Preview">
						 </cfif>
					<CFELSEIF this.hasExternalSelection() is not 1>
						<cfparam name="result.nextpage" default="Selections">
					<CFELSEIF this.canBeEdited()>
						<cfif not this.getApprovalDetails().requiresApproval>
							phr_Sys_Comm_IsBeingPrepared  phr_Sys_Comm_CanBeEditedAndSent
						<cfelseif this.getApprovalDetails().isApproved > <!--- using an approval process and approval is required --->
							phr_Sys_Comm_IsReadyToSend
						<cfelseif this.getApprovalDetails().sentForApproval>
							<cfset result.message="Sent for Approval">
							<cfset result.type="warning">
						<cfelse>
							<cfset result.message="Awaiting Approval">
							<cfset result.type="warning">

							phr_Sys_Comm_CanBeEditedNotSent
						</cfif>
					<cfelseif this.hasSelectionsWaitingToSend() is 1 and this.PreviewReviewed >
						<cfparam name="result.nextpage" default="send">
						phr_Sys_Comm_IsReadyToSend  phr_Sys_Comm_HasPendingSelection
						<!---
						<cfif isUserAllowedToSendNow()>

						<cfelseif useCommunicationQueue>

						</cfif>
						--->
					<cfelse>
						<cfset readonly="true">
						phr_Sys_Comm_CommSentNotification
					</cfif>


			<cfset result.message = application.com.relayTranslations.translateString(string = result.message, mergestruct = mergestruct)>

			<cfreturn result>


		</cffunction>

		<cffunction access="public" name="isUserAllowedToSendNow" returnType="boolean" output="false">

			<cfif not structKeyExists(variables.data,"isUserAllowedToSendNow")>
				<cfset variables.data.isUserAllowedToSendNow = application.com.communications.isUserAllowedToSendNow()>
			</cfif>

			<cfreturn variables.data.isUserAllowedToSendNow >

		</cffunction>


</cfcomponent>