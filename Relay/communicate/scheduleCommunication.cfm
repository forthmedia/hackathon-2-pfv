<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

scheduleCommunication.cfm

Author: WAB 2005-05-03

Purpose:  Used to allow the use to (re)schedule a communication or parts of a communication


Mods:
2005-09-20   WAB added some help text
2006-04-10		WAB javascript error if a selection with negative id (ie individually selected people) appeared in list - have made it so that these can't be scheduled
2009/12/08	WAB Internationalisation, added showAsLocalTime attribute to date and time controls
2010/06/07  WAB Move form outside table so that calendar js works with FF
2011/07/27	NYB		P-LEN024 changed comm checkDetails.cfm to commDetailsHome.cfm
2012/04/13	IH	Case 426070 add confirmation message
 --->

<cfset communication = application.com.communications.getCommunication (commid = frmcommid)>

<!--- form posting back to itself, need to update --->
<cfif isDefined("frmSelectionIDList")>
	<CFLOOP index = thisSelectionID  list = "#frmSelectionIDList#">
		<cfif form["frmScheduledDate_#thisselectionID#"] is not "">
			<!--- NJH 2013/02/19 - now handled in applicationMain.cfc
			<cfset scheddate = application.com.datefunctions.combineDateAndTimeFormFields("frmScheduledDate_#thisselectionID#")> --->
			<cfset communication.schedule	(date= form["frmScheduledDate_#thisselectionID#"], selectionID = thisselectionID)>			
		<cfelseif form["frmScheduledDate_#thisselectionID#"] is "" and form["frmScheduledDate_#thisselectionID#_orig"] is not "">
			<cfset communication.unschedulePart	(selectionID = thisselectionID)>			
		</cfif>
	</CFLOOP>
</cfif>
<cfif isDefined("frmScheduledDate")>
	<cfif frmScheduledDate is not "">
		<!--- NJH 2013/02/19 - now handled in applicationMain.cfc
		<cfset scheddate = application.com.datefunctions.combineDateAndTimeFormFields("frmScheduledDate")> --->
		<cfset communication.schedule	(date= frmScheduledDate)>			
	<cfelseif frmScheduledDate is "" and frmScheduledDate_Orig is not "">
		<cfset communication.unschedulePart	()>			
	</cfif>	
</cfif>



 



<cfset CommSelectionDetails = communication.getSelectionDetails()>

<cfif isDefined("frmSelectionIDList")>
	<p class="successblock">Communication has been scheduled.</p>	
<cfelse>
	<cfoutput>
	<cfform name = "scheduleCommForm" method=post>
	
	<table>
	
	<tr><td colspan=3>
		Schedule Communication #htmleditformat(frmCommID)# #htmleditformat(communication.title)#	
	</td></tr>
	<tr><td colspan=3>&nbsp;</td></tr>
	
		<CF_INPUT type=hidden name ="frmCommID" value = "#frmCommID#">
	
	
	<tr><td colspan=3>To schedule the whole communication to be sent, enter the date and time below <BR>
	</td></tr>
	
	
	<tr><td colspan=2></td><td>	
					<cf_relaydatefield		
								currentValue=#communication.sendDate#
								thisFormName="scheduleCommForm"
								fieldName="frmScheduledDate"
								anchorname = "D"
								showClearLink = true
								showAsLocalTime = true
							>
							<CF_INPUT type="hidden" name ="frmScheduledDate_Orig" value="#communication.sendDate#">
	
							<cf_relaytimefield		
								currentValue=#communication.sendDate#
								thisFormName="scheduleCommForm"
								fieldName="frmScheduledDate"
								anchorname = "T"
								showAsLocalTime = true
							> 
	
	</td>
	
	<tr><td colspan=3>&nbsp;</td></tr>						
	<tr><td colspan=3>To schedule selections to be sent individually, enter the dates and times below<BR>
					
	</td></tr>
	
	
	<cfLOOP query="CommSelectionDetails" >
		<cfif listfindnocase("#application.com.communications.constants.externalSelectLID#,#application.com.communications.constants.approverSelectLID#,#application.com.communications.constants.internalSelectLID#",commSelectLID) is not 0 and selectionid gt 0>
			<!---  can only schedule external, internal or approver selections, can't schedule 'individually selected people' (selectionid <= 0>--->
			<tr>
			
				<td>&nbsp;&nbsp;#htmleditformat(title)#</td>
				<td><cfif sent is 2>Queued<cfelseif sent is 1>Sent<cfelseif sent is 0>Not sent </cfif>
				</td>
				<td>
					<cfif sent is 2 or sent is 0 >
						<CF_INPUT type=hidden name ="frmSelectionIDList" value = "#SelectionID#">					
							<cf_relaydatefield		
								currentValue="#sendDate#"
								thisFormName="scheduleCommForm"
								fieldName="frmScheduledDate_#selectionID#"
								anchorname = "D#selectionID#"
								showClearLink = true
								showAsLocalTime = true
							>
							<CF_INPUT type="hidden" name ="frmScheduledDate_#selectionID#_Orig" value="#sendDate#">
	
							<cf_relaytimefield		
								currentValue="#sendDate#"
								thisFormName="scheduleCommForm"
								fieldName="frmScheduledDate_#selectionID#"
								anchorname = "T#selectionID#"
								showAsLocalTime = true
							> 
		
					
					
						<cfif senddate is not "" and communication.senddate is not "" and communication.sent is 0 and senddate gt communication.senddate>
						This item will be sent on #application.com.datefunctions.datetimeformat(date = communication.senddate, showAsLocalTime = true)# when the whole communication is sent
						</cfif>
				
					</cfif>
				</td>
	
		
			</tr>
	
	
		</cfif>
	</CFLOOP>
	<tr><td colspan=3>Note: If the whole communication is scheduled for an earlier time than an individual selection, <BR>
	 then the individual selection will be sent at the earlier time	</TD></tr>
	
	
		<tr><TD COLSPAN = "3"><input type=submit value="Update">	</TD></tr>
	</table>
	
	</cfform>
	</cfoutput>
</cfif>
<!--- <cfoutput><a Href="commDetailsHome.cfm?frmCommid=#frmCommID#">Return to communication</a></cfoutput> --->
<cfoutput><p><A HREF="JavaScript: window.close();">Return to communication</A></p></cfoutput>
