<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Included in commdetailstatistics and other templates to allow filtering of statisitics by test / internal / external

sets a variable Filter.TestInternalExternal to be included in queries 
based on variable filtercode

 --->

<CFPARAM name="filtercode" >
<cfparam name="frmCountryID" default = "">

<cfset filter = application.com.communications.getCommunicationFilter(filtercode = filtercode, countryid = frmCountryid)>

