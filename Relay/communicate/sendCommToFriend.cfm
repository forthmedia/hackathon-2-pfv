<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

sendTestCommToFriend.cfm

Purpose 
	To send a queued communication to an individual email address

Amendments

WAB 2004-09-27

Used to be sendTESTcommToFriend.cfm but actually needs to be used both with and without the Test parameter

is called by sendTESTcommToFriend.cfm with frmTest set to true

gets a personid of a person to send to and then passes control to sendCommunication.cfm

WAB 2005-11-15 added check fro frmPersonID being passed
NJH 2008/07/15 if the friend doesn't exist in relayware, show a link that opens a new window or tab with Quick Card Add
NYB 2011-06-13	changed to accept multiple email addresses
--->

<!-- NJH 2008/07/15 Bug Fix Issue 680 -->
<cf_includeJavascriptOnce template="/javascript/openWin.js">
<cf_includeJavascriptOnce template="/javascript/viewentity.js">
<cf_includeJavascriptOnce template="/javascript/extExtension.js">


<CFPARAM name = "frmTest" default = "false">   <!--- can be called with this set to true and will be sent as a Test --->
<CFPARAM name = "frmNextPage" default = "Send">   <!--- can be called with this set to true and will be sent as a Test --->
<!--- START: 2011-11-14 NYB added: --->
<cfparam name = "includeNextPage" default= "false">
<cfparam name = "includeMessage" default= "true">
<!--- END --->

<cfif (isDefined("btnSend") and btnSend eq "yes") >
	<!--- NJH 2012/11/14 CASE 431953 - remove any spaces found in frmEmail --->
	<cfset form.frmEmail = rereplace(form.frmEmail,'\s','','ALL')>
	
	<cfquery name="checkPerson" datasource="#application.siteDataSource#">
		select personid,email from person where email  in ( <cf_queryparam value="#form.frmEmail#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) order by email
	</cfquery>
	<cfif checkPerson.recordCount neq 0>
		<cfset frmPersonIDs = "#valuelist(checkPerson.personid)#">
	<cfelse>
		<cfset frmPersonIDs = 0>
	</cfif>
	<cfinclude template="sendCommunication.cfm">
	
<cfelseif isDefined("frmPersonID")>
	<cfset frmPersonIDs = frmPersonID>
	<cfinclude template="sendCommunication.cfm">
<cfelse> 
	<cf_title>Send Communication</cf_title>
	<!--- <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder"> --->
		<cfform method="post" name="FormX">
		<cf_relayFormDisplay align="center">
			<cfoutput>
			<input type="hidden" name="btnSend" value="yes">
			<CF_INPUT type="hidden" name="frmCommID" value="#frmCommID#">
			<CF_INPUT type="hidden" name="frmTest" value="#frmTest#">
			<CF_INPUT type="hidden" name="frmNextPage" value="#frmNextPage#">
			<CF_INPUT type="hidden" name="includeNextPage" value="#includeNextPage#">
			<CF_INPUT type="hidden" name="includeMessage" value="#includeMessage#">
			<!--- PJP 20/11/2012 CASE: 429546 Make sure it is possible to resend to already send to address --->
			<CF_INPUT type="hidden" name="frmResend" value="true">
			</cfoutput>
			<tr>
				<td align="center" colspan=2>
					phr_sendCommToTheFollowingEmails
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<img src="/images/misc/rule.gif" width=230 height=3 alt="" border="0">
				</td>
			</tr>
			
			<!--- NJH 2008/07/15 Bug Fix Issue 680
			If the person doesn't exist, then let the user know. Open Quick Card Add in a new window/tab --->
			<cfif isDefined("checkPerson") and checkPerson.recordCount eq 0>
				<tr><td colspan="2" align="center"><div class="infoBlock">The individual with this email address does not exist in the system. <a href="##" onClick="javascript:viewOrganisation (0,'QuickCardAdd');return false;">Click here to add them.</a>
						When they have been added, return to this screen to continue sending the communication.</div></td></tr>
			</cfif>
			
		 	<tr>
				<td align="center" colspan=2>			
					phr_emailaddresses<br>(i.e. johndoe@acme.com)			
				</td>
			</tr>
			
			<tr>
				<td align="center" colspan=2>
					<cfset emailAddress = "">
					<cfif isDefined("frmEmail")>
						<cfset emailAddress = frmEmail>
					</cfif>
					<cf_relayFormElement relayFormElementType="textarea" rows=3 cols=40 fieldname="frmEmail" currentvalue="#emailAddress#" label="" required="true" maxCharsPosition="bottom" maxLength="250">
					<!---<textarea name="frmEmail" rows="3" cols="40"><cfif isDefined("frmEmail")>#frmEmail#</cfif></textarea>
					
					<input name="frmEmail" value="<cfif isDefined("frmEmail")>#frmEmail#</cfif>" size="35" maxlength="80">
					--->
				</td>
			</tr>
	
			<tr>
				<td colspan="2" align="center">
					<cf_relayFormElement relayFormElementType="submit" fieldname="frmSubmit" currentvalue="phr_continue" label="">
					<!--- <a href="javascript:customSubmitFunction()">phr_continue</a> --->
			   	</td>
			</tr>
		</cf_relayFormDisplay>
		</cfform>
</cfif> 
