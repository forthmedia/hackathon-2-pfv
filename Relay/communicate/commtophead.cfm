<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	
	2001-10-28	CPS		Changes following new attributes on commdetail record, namely commtypeid, commreasonid and commstatusid
	1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
	1999-02-21  SWJ Modified javascript to call report/wabcommcheck.cfm
--->
<CFPARAM NAME="frmruninpopup" DEFAULT="false">
<CFPARAM NAME="current" TYPE="string" DEFAULT="campaignFrame.cfm">
<cfparam name="newCommEditor" default = "commDetailsHome"><!--- formerly "commheader" --->
<cfparam name="pageTitle" default="">
<cfset newCommEditor="commDetailsHome">
<cfset thisDir="/communicate">

<CF_RelayNavMenu pageTitle="#pageTitle#" moduleTitle="" thisDir="communicate">
	<CFIF frmruninpopup IS NOT "True">
 		<CFIF findNoCase("commViewPend.cfm",script_name,1) or findNoCase("commCheck",script_name,1) or findNoCase("commHold.cfm",script_name,1) or findNoCase("commSend.cfm",script_name,1) or findNoCase("commDetailsHome.cfm",script_name,1)>
			<CFIF checkPermission.Level2 >
				<CF_RelayNavMenuItem MenuItemText="New Communication" runInNewtab="true" tabName="tab_New_comm" tabTitle="New Comm"  tabOptions="{iconClass:'communicate'}" CFTemplate="/communicate/commDetailsHome.cfm" target="mainSub">
			</CFIF> 
		</CFIF>
		
		<cfif not findNoCase("broadcasts.cfm",SCRIPT_NAME,1) and not findNoCase("broadcastAccounts.cfm",SCRIPT_NAME,1) and not findNoCase("messages.cfm",SCRIPT_NAME,1)>
			<CF_RelayNavMenuItem MenuItemText="Sent Communications" CFTemplate="#thisDir#/commcheck.cfm?frmShowStatus=Sent&frmShowOwn=0" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Unsent Communications" CFTemplate="#thisDir#/commcheck.cfm?frmShowStatus=unSent&frmShowOwn=0" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Scheduled Communications" CFTemplate="#thisDir#/commviewpend.cfm" target="mainSub">
		</cfif>

 		<CFIF (findNoCase("commViewPend.cfm",script_name,1) or findNoCase("commCheck.cfm",script_name,1) or findNoCase("commHold.cfm",script_name,1) or findNoCase("commSend.cfm",script_name,1))>	
			<CFIF checkPermission.level3>
				<CF_RelayNavMenuItem MenuItemText="Process Communications" CFTemplate="#thisDir#/checkToSend.cfm" target="mainSub">
			</CFIF>
		</CFIF>
 		<CFIF findNoCase("sendTestCommunication.cfm",script_name,1) >	
			<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="javascript:history.back()" target="mainSub">
		</CFIF>
		<cfif findNoCase("broadcasts.cfm",SCRIPT_NAME,1)>
			<cfif listFindNoCase(application.com.settings.getSetting('socialMedia.services'),'facebook')>
				<CF_RelayNavMenuItem MenuItemText="Facebook Pages" CFTemplate="#application.com.security.encryptURL(url='broadcastAccounts.cfm?service=Facebook')#">
			</cfif>
			<cfif listFindNoCase(application.com.settings.getSetting('socialMedia.services'),'twitter')>
				<CF_RelayNavMenuItem MenuItemText="Twitter Accounts" CFTemplate="#application.com.security.encryptURL(url='broadcastAccounts.cfm?service=Twitter')#">
			</cfif>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='broadcasts.cfm?editor=yes&add=yes&hideBackButton=false&sent=false')#">
		</cfif>
		<cfif findNoCase("broadcastAccounts.cfm",SCRIPT_NAME,1)>
			<CF_RelayNavMenuItem MenuItemText="Messages" CFTemplate="#application.com.security.encryptURL(url='broadcasts.cfm')#">
			<cfif not structKeyExists(url,"service") or (structKeyExists(url,"service") and url.service neq "facebook") and listFindNoCase(application.com.settings.getSetting('socialMedia.services'),'facebook')>
			<CF_RelayNavMenuItem MenuItemText="Facebook Pages" CFTemplate="#application.com.security.encryptURL(url='broadcastAccounts.cfm?service=Facebook')#">
			</cfif>
			<cfif not structKeyExists(url,"service") or (structKeyExists(url,"service") and url.service neq "twitter") and listFindNoCase(application.com.settings.getSetting('socialMedia.services'),'twitter')>
			<CF_RelayNavMenuItem MenuItemText="Twitter Accounts" CFTemplate="#application.com.security.encryptURL(url='broadcastAccounts.cfm?service=Twitter')#">
			</cfif>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='broadcastAccounts.cfm?editor=yes&add=yes&hideBackButton=false&service=#service#')#">
		</cfif>
		<cfif findNoCase("messages.cfm",SCRIPT_NAME,1)>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='messages.cfm?editor=yes&add=yes&hideBackButton=false')#">
		</cfif>
	</CFIF> 
</CF_RelayNavMenu>


<!--- 
<CFPARAM NAME="frmruninpopup" DEFAULT="">  <!--- wab --->

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Communications <cfif isDefined("subsection") and Len(subsection)> - <cfoutput>#subsection#</cfoutput></cfif></DIV>
		</TD>
	</TR>
	<cfoutput>
	<CFIF frmruninpopup IS NOT "True">
		<TR>
			<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
				<CFIF checkPermission.AddOkay GT 0>
					<A HREF="#thisdir#/commheader.cfm" CLASS="Submenu">New communication</A><font color=white> |</font>
				</CFIF>
				<CFIF checkPermission.level4 GT 0>
					<A HREF="/scheduled/checkToSend.cfm" CLASS="Submenu">Process Queued Comms</A><font color=white> |</font>
				</CFIF>
				<A HREF="/report/communications/commcheck.cfm" CLASS="Submenu">List communications</A><font color=white> |</font>
				<A HREF="#thisdir#/commviewpend.cfm" CLASS="Submenu">View pending</A><font color=white> |</font>
				<A HREF="JavaScript:newWindow = openWin( '../help/communication.htm','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'); newWindow.focus() ;" CLASS="Submenu">Help</A>
			</TD>
		</TR>
	</cfif>	
	</cfoutput>
</table> --->