<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

resendcommitems.cfm


used to resend a communication to a list of people

requires frmCommID

can be passed frmPersonID (or frmPersonISs)
frmSelectID
frmCommFormLID

often called from commResultsByStatus.cfm which passes a list of personids

2012/06/14	IH	Case 427512 add simple confirmation message
2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object and use ajax feedback
 --->

<cfif isDefined("frmPersonID")>
	<!--- different parameter name used by next template --->
	<cfset frmPersonIDs = frmPersonID>
</cfif>

<cfset communication = application.com.communications.getCommunication (#frmCommid#,request.relayCurrentUser.personid)>

<cfif not communication.hasRightsToEdit()>
	<CFSET message="You are not authorised to use this function">
	<CF_ABORT>
<cfelse>


	<cfset sendResult = communication.send (
		CommID = frmCommID,
		personids = frmpersonids,
		resend = true,
		asynchronous = true	
	)>
		

	
	
	<cf_includeJavascriptOnce template = "/javascript/prototypeSpinner.js">
	<cf_includeJavascriptOnce template = "/communicate/communication.js">


	<div id="resultsDiv" style="width:400px">
	
	</div>
	
	<script>
		<cfoutput>
			var commID_enc = '#application.com.security.encryptVariableValue("commid",frmCommID)#'
			updateCommMessageDiv (#sendResult.commSendResultID#,true,'Sending',true)
			updateCommStatus (commID_enc ,#sendResult.commSendResultID#)</script>
		</cfoutput>	
	</script>


</cfif>
