<!--- �Relayware. All Rights Reserved 2014

2016/02/09	NJH	Changed check file to simply trigger form submit. Validation now down through RW form validation.
--->
<cfif isDefined("communication")>

	<CFQUERY NAME="getCommForm" DATASOURCE="#application.SiteDataSource#">
		SELECT IsDefault,
			  LookupID,
			  ItemText
		 FROM LookupList
		 WHERE lookupID in (2,3,5,6,7)
		 AND IsLive = 1
		 ORDER BY lookupID
	</CFQUERY>

	<cfsavecontent variable="commJavascript_js">
	<CFOUTPUT>
	<SCRIPT type="text/javascript">

			var editor = '';

			loadEditorForCommunications = function() {

				var CKInstanceName = 'frmMsg_HTML';

				var instance = CKEDITOR.instances[CKInstanceName];
				if(instance)
			    {
			        CKEDITOR.remove(instance);
			    }

				editor = CKEDITOR.replace( CKInstanceName,
					 {
						 	height : 500,
						 	customConfig : '/javascript/ckeditor_config.js',
						 	filebrowserLinkUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Files&relayContext=communication&currentFolder=/personal (#request.relayCurrentUser.personID#)/',
							filebrowserImageUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Images&relayContext=communication&currentFolder=/personal (#request.relayCurrentUser.personID#)/',
							filebrowserFlashUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Flash&relayContext=communication&currentFolder=/personal (#request.relayCurrentUser.personID#)/'
					 }
				 );
				CKFinder.setupCKEditor( editor, {basePath:'/ckfinder/'});
			}

			checkFormcommfile = function(btn) {
				/*var msg = "";
				var begtxt = "";
				var radiochecked = "no";
				var bothindices = "5,6,7";
				var faxindices = "3," + bothindices;
				var emailindices = "2," + bothindices;
	</CFOUTPUT>
				<CFOUTPUT QUERY="getCommForm">
					<CFIF lookupID eq communication.CommFormLID>
						begtxt = "\n\nSince you selected '#ItemText#' you must:\n";
					</CFIF>
				</CFOUTPUT>
	<CFOUTPUT>
				<CFIF listFind("3,5,6,7",communication.CommFormLID) neq 0>
					if (faxindices.indexOf($('frmCommFormLID').value) != -1) {
						if ($('frmFaxFile').value == "") {
							//fax
							msg += "\n     * Select a fax file to send."
						} else if ($('frmFaxFile').value.lastIndexOf(".") == -1) {
							msg += "\n     * Select a valid fax file to send (.prn).";
						} else if ($('frmFaxFile').value.substring($('frmFaxFile').value.lastIndexOf("."), $('frmFaxFile').value.length) != ".prn"){
							msg += "\n     * Select a valid fax file to send (.prn).";
						}
					}
				</cfif>
				<CFIF listFind("2,5,6,7",communication.CommFormLID) neq 0>
					if ((emailindices.indexOf($('frmCommFormLID').value) != -1) && (typeof editor.getData == 'function' && editor.getData() == "")) {
						//email
						msg += "\n     * Enter text to send as the email message.";
					}
				</CFIF>

			if (msg != "") {
				alert(begtxt + msg);
			} else {*/
				$('frmRequestedAction').value = btn;
				//$('commFileForm').submit();
				triggerFormSubmit($('commFileForm'))
		}


		</SCRIPT>
	</CFOUTPUT>


	</cfsavecontent>

	<CFOUTPUT>
	<cfhtmlhead text="#commJavascript_js#">
	</CFOUTPUT>

</cfif>
