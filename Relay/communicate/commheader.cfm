<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---
1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid
				and to run in a popup window
1999-10-04  KT		Removed references to download - this is now in separate menu and flow

2000-09-06	DJH		Amendments to allow certain information to be included at the start of the email and bypass the setting up stages
				(being used for sending event invitations...)

2001-07-20  WAB 	Allow users to link to an event without going through the event tools
/6/04	WAB		Lots of work - add track email checkbox, add load template drop down, change templates query to work proper
2005-10-13 GCC	Added preview to email template selection
2009/11/04 NJH 	LID 2778  - added a check on selection/selectSearch.cfm before showing the link.
2009/11/30 NJH	LHID 2879 - sort the siteList alphabetically
WAB 2010/10/12 Removed references to application.externalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalExternalDomain()
				Slightly worried that this might occasionally have strange consequences because the function could bring back different answers if scheduled email is run on a different URL to a test email.
				May need to remove the concept of a default
2010-01-20 NYB	LHID 4918 - added support for frmCommEmailFormat being db driven
2011-07-04	NYB	P-LEN024
2011/09/19	WAB	LID 7234 add htmlEditFormat to input fields
2011/10/11	PPB	NET005 moved function getCommType to communications.cfc getCommTypesLID() to allow Netgear to override
2011-11-04 	NYB LHID8007 removed the 'do you have a Selection' condition - this can and should be dealt with at the Add Selections/Send stage - not here
2012/03/06		RMB		P-LEN044 - CR-069 - Added 'CharacterSet' to form
	2013-01		WAB	Sprint 15&42 Comms 	Converted to use communication.cfc object
2012-02-26	WAB 	changed showNumber variable to stepName - so can handle extra tabs automatically
2013-06-03 WAB	CASE 435381   Removed the section for linking to an event, since was not being saved in the back end.  Apparently might be reinstated at some point
2013-08-15	YMA	Case 436313 fromDisplayName now stored in commFromDisplayName.
2013-09-11 NYB Case 434787: replaced SHOWCURRENTVALUE with ALLOWCURRENTVALUE
2014-04-01	PPB	Case 439250 changed <SCRIPT> TO <script> because CF ajax likes lower case script tags
2014-08-15	RPW	Improve Comms Template description
2016-01-27  SB  Removed tables added Cf_relayFormElementDisplay with help from Nathaniel.
2016/02/09	NJH	Changed check file to simply trigger form submit. Validation now down through RW form validation.
2016/02/10	NJH	JIRA PROD2015-566: pass campaigns through as function rather than query so that we can get the selected value.
--->
<!---
ANY changes to the form element frmEmailTemplate should be duplicated in emailTemplatePreview.cfm
We should factorise it...
 --->
<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->

<CFQUERY NAME="GetUsersEmail" DATASOURCE="#application.SiteDataSource#">
	SELECT Email FROM Person WHERE PersonID = #request.relayCurrentUser.personid#
</CFQUERY>

<CFPARAM NAME="frmProjectRef" DEFAULT="">
<CFPARAM NAME="frmTitle" DEFAULT="">
<CFPARAM NAME="frmNotes" DEFAULT="">
<CFPARAM NAME="frmDescription" DEFAULT="">
<CFPARAM Name="frmreplyto" Default=#GetUsersEmail.Email#>
<CFPARAM NAME="frmCommID" DEFAULT="0">
<CFPARAM NAME="frmLastUpdated" DEFAULT="">
<CFPARAM NAME="frmCommFormLID" DEFAULT="0">
<CFPARAM NAME="frmCommTypeLID" DEFAULT="0">
<CFPARAM NAME="frmEmailTemplate" DEFAULT="">   <!--- wab --->
<CFPARAM NAME="frmTrackEmail" DEFAULT="1">   <!--- wab --->
<CFPARAM NAME="frmpersonids" DEFAULT="">   <!--- wab --->
<CFPARAM NAME="frmruninpopup" DEFAULT="false">  <!--- wab --->
<CFPARAM NAME="frmnextpage" DEFAULT="">  <!--- wab --->
<CFPARAM NAME="frmFlagID" DEFAULT="">  <!--- KT 1999-11-01 from the events section --->
<CFPARAM name="frmselectid" default="">
<CFPARAM name="frmLinkToSiteURL" default="">
<CFPARAM name="frmCampaignID" default="">
<CFPARAM name="frmFromAddress" default="">
<CFPARAM name="frmcommFromDisplayNameID" default="">
<CFPARAM name="frmCommEmailFormatID" default="1">
<CFPARAM name="frmTrackEmailWithOmniture" default="0">
<CFPARAM name="stepName" default="">
<CFPARAM name="frmCallingTemplate" default="#GetFileFromPath(GetCurrentTemplatePath())#">
<CFPARAM name="frmCharacterSet" default="UTF-8">
<CFPARAM name="frmStyleSheet" default="">

<CFPARAM name="readonly" default="false">

<!--- 2013-08-15	YMA	Case 436313 From Display Name is now stored in a seperate table and can be added so needs to save seperately if its new --->
<cf_includeJavascriptOnce template="/javascript/newSelectValue.js">
<cf_includeJavascriptOnce template="/communicate/js/communicate.js">

<cfscript>
	getEmailTemplates = application.com.communications.getValidTemplates();
</cfscript>

<CFSET task = "add">
<CFIF IsDefined("frmCommID") and isnumeric(frmCommID) and frmCommID gt 0>
	<CFSET task = "edit">
</CFIF>

<!--- check for eventmanagers --->
<!--- need to store the comms checkpermission query because it is used later on--->


<!--- 2013-06-03 WAB	CASE 435381   remove link to events
<cfset isEventManager = application.com.login.checkInternalPermissions("EventTask","Manage")>

<cfif isEventManager >
	<!--- get events you have rights to --->
	<CFQUERY NAME="GetEvents" DATASOURCE="#application.SiteDataSource#">
		SELECT ed.title, ed.flagid
		FROM eventdetail ed
			inner join RecordRights AS rr on ed.flagid = rr.recordid and rr.Entity = 'Flag'
			inner join RightsGroup rg on rg.usergroupid = rr.usergroupid
		WHERE rg.PersonID =  <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		order by ed.eventgroupid, ed.title
	</CFQUERY>
</cfif>
--->

<!--- 2011-11-04 NYB LHID8007 removed
<cfset getSelections = application.com.selections.getSelections()>
--->

<cfset getCommTitle = application.com.communications.getCommunication (#listlast(frmCommid)#,request.relayCurrentUser.personid)>

<CFIF Variables.task  IS "edit" AND checkPermission.Level2>
	<!--- user wants to edit and has permissions to do so --->

	<!--- returns query with all the details of the communication--->

	<CFQUERY NAME="getthisCommForm" DATASOURCE="#application.SiteDataSource#">
		SELECT c.CommID AS CommID,
			   c.Title AS Title,
			  ll.ItemText AS ItemText
	      FROM Communication AS c, LookupList AS ll
	     WHERE  c.CommFormLID = ll.LookupID
		  AND c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFIF not getCommTitle.canBeEdited() or not getCommTitle.hasRIGHTSTOEDIT()>
		<cfset readonly = "true">
	</CFIF>

<CFELSEIF Variables.task IS "edit" AND NOT checkPermission.Level2>
		<!--- want to edit and don't have permissions --->
		<cfset readonly = "true">
		<CFSET message="Phr_Sys_YouAreNotAuthorised">
</CFIF>

<CFIF getCommTitle.RecordCount IS NOT 0>
		<CFSET frmProjectRef=#getCommTitle.ProjectRef#>
		<CFSET frmTitle=#getCommTitle.Title#>
		<CFSET frmNotes=#getCommTitle.Notes#>
		<CFSET frmCommFormLID=#getCommTitle.CommFormLID#>
		<CFSET frmDescription=#getCommTitle.Description#>
		<CFSET frmReplyTo=#getCommTitle.replyto#>
		<CFSET frmCommID=#getCommTitle.CommID#>
		<CFSET frmLastUpdated=#getCommTitle.LastUpdated#>
		<CFSET frmCommTypeLID=#getCommTitle.CommTypeLID#>
		<CFSET frmFromAddress=#getCommTitle.FromAddress#>
		<CFSET frmCommFromDisplayNameID=#getCommTitle.commFromDisplayNameID#>
		<CFSET frmFromDisplayName=#getCommTitle.FromDisplayName#>
		<CFSET frmLinkToSiteURL=#getCommTitle.LinkToSiteURL#>
		<CFSET frmCampaignID=#getCommTitle.CampaignID#>
		<CFSET frmCommEmailFormatID=#getCommTitle.CommEmailFormatID#>
		<CFSET frmTrackEmail=#getCommTitle.TrackEmail#>
		<CFSET frmTrackEmailWithOmniture=#getCommTitle.TrackEmailWithOmniture#>
		<CFSET frmCharacterSet=#getCommTitle.CharacterSet#>
		<CFSET frmStyleSheet=#getCommTitle.StyleSheet#>
<CFELSE>
	<cfif frmCommID neq 0>
		<CFSET message="Phr_Sys_SorryRecordCouldNotFound.">
	</cfif>
</CFIF>

<cfoutput>
	<h2 class="communicationsh1">
		Phr_Sys_comm_Step_#htmleditformat(stepName)#_Header
	</h2>
</cfoutput>

<cfif isdefined("message") and len(message) gt 0>
	<CFOUTPUT><h2>#application.com.security.sanitiseHTML(message)#</h2></CFOUTPUT>
</cfif>


<cfif frmCommID eq 0 or getCommTitle.RecordCount IS NOT 0>

	<CFIF isDefined("qryCommFormLID")><CFSET frmCommFormLID = qryCommFormLID></CFIF>  <!--- WAB: I changed some variable names and this give somebackward compatability if this template is being called from somewhere else --->
	<cfif frmFromAddress is ""><cfset frmFromAddress = "#application.emailFrom#@#application.mailfromdomain#"></cfif>
	<cfif not isDefined("frmFromDisplayName") or frmFromDisplayName is ""><CFSet frmFromDisplayName="#application.EmailFromDisplayName#"></cfif>

	<cfparam name="frmHideTopHead" default="false">
	<cfif not frmHideTopHead>
		<cfinclude template="commtophead.cfm">
	</cfif>
	<!--- <cf_includejavascriptonce template = "/javascript/verifyEmail V2.js"> --->
	<cf_includeJavascriptOnce template="/javascript/extextension.js">

	<cfset siteQuery = application.com.communications.getSiteValidValues()>
	<cfif frmCommID eq 0>
		<cfset frmLinkToSiteURL = ListFirst(ValueList(siteQuery.dataValue))>
	</cfif>
	<!--- START: 2013-09-11 NYB Case 434787: replaced SHOWCURRENTVALUE with ALLOWCURRENTVALUE --->
	<cfset frmLinkToSiteURLAttributes = {formFieldID="frmLinkToSiteURL",validValues="#siteQuery#",currentvalue="#frmLinkToSiteURL#",shownull="false",allownewvalue="false",ALLOWCURRENTVALUE="true",disabled="#readonly#" }>
	<cfset frmCharacterSetAttributes = {formFieldID="frmCharacterSet",validValues="Shift_JIS,UTF-8",currentvalue="#frmCharacterSet#",shownull="false",allownewvalue="false",ALLOWCURRENTVALUE="true",disabled="#readonly#" }>
	<!--- END: 2013-09-11 NYB Case 434787 --->

	<cfoutput>
	<script type="text/javascript">
		openWin = function( windowURL,  windowName, windowFeatures ) {
			return window.open( windowURL, windowName, windowFeatures ) ;
		}

		checkFormcommheader = function(btn) {
			var form = document.getElementById("CommHeaderForm");

			document.getElementById("frmRequestedAction").value = btn;
			triggerFormSubmit(form);
		}

		submitPreview = function() {
			var form = document.CommHeaderForm;
			var templateForPreview = form.frmEmailTemplate.options[form.frmEmailTemplate.selectedIndex].value;
			var URLstring = "#thisDir#/emailTemplatePreview.cfm?frmEmailTemplate=" + templateForPreview;
			newWindow = openWin(URLstring,'EmailTemplatePreviewer','width=780,height=580,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0');
			newWindow.focus();
		}
		toggleStyleSheetSelector = function(thisID){
			if(thisID == 2){
				document.getElementById('styleSheetSelector').style.display = 'none';
			}
			else {
				document.getElementById('styleSheetSelector').style.display = 'inline';
			}
		}
		// 2014-01-02 IH Case 437325 Put focus on a text field to work around apparent Ext/IE bug.
		var tempField = document.getElementById("frmTitle");
		tempField.focus();
		tempField.blur();

	</script>
	</cfoutput>

	<!--- 2011-11-04 NYB LHID8007 removed this condition - this can and should be dealt with at the Add Selections/Send stage - not here
	<cfif getSelections.recordCount gt 0>
	--->

			<!--- start Edit Comm form --->

			<cfset getCommType = application.com.communications.getCommTypesLID()>		<!--- 2011/10/11 PPB NET005 --->

			<CFQUERY NAME="getCommForm" DATASOURCE="#application.SiteDataSource#">
				SELECT IsDefault,
					  LookupID,
					  ItemText
				 FROM LookupList
				 WHERE lookupID in (2,3,5,6,7)
				 AND IsLive = 1
				 ORDER BY lookupID
			</CFQUERY>

			<!--- NYB 2011-01-20 LHID 4918 - added: --->
			<cfset getCommEmailFormats = application.com.communications.getCommEmailFormats()>
				<FORM NAME="CommHeaderForm" ID="CommHeaderForm" ACTION="commDetailsHome.cfm" ENCTYPE="multipart/form-data" METHOD="post" novalidate="true">
					<cf_relayformdisplay autoRefreshRequiredClass=true applyValidationErrorClass=true showValidationErrorsInline= true autoValidate=true>
					<CFOUTPUT>
						<CF_INPUT TYPE="HIDDEN" NAME="frmProjectRef" VALUE="#frmProjectRef#">
						<!--- <INPUT TYPE="HIDDEN" NAME="frmTitle_required" VALUE="Subject">
						<INPUT TYPE="HIDDEN" NAME="frmDescription_required" VALUE="Objective">
						<input type="hidden" name="frmCommFromDisplayNameID_required" value=""> --->
						<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#frmCommID#">
						<INPUT TYPE="HIDDEN" NAME="frmNotes" VALUE="">
						<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
						<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
						<CF_INPUT TYPE="HIDDEN" NAME="frmCallingTemplate" VALUE="#frmCallingTemplate#">
						<CF_INPUT TYPE="HIDDEN" NAME="frmnextpage" VALUE="#frmnextpage#">
						<cfif isdefined("frmFailTo")>
							<CF_INPUT TYPE="HIDDEN" NAME="frmFailTo" VALUE="#frmFailTo#">
						</cfif>
						<cfif isDefined("frmMsg")>
							<CF_INPUT type="hidden" name="frmMsg" value="#frmMsg#">
						</cfif>
						<cfif isDefined("relfileFileCategory")>
							<CF_INPUT type="hidden" name="relfileFileCategory" value="#relfileFileCategory#">
						</cfif>
						<cfif isDefined("relfileEntityType")>
							<CF_INPUT type="hidden" name="relfileEntityType" value="#relfileEntityType#">
						</cfif>
						<INPUT TYPE="HIDDEN" NAME="frmRequestedAction" ID="frmRequestedAction" VALUE="">
						<CFIF task IS "edit" and not readonly>
							<!--- <INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="<CFOUTPUT>#frmCommID#</CFOUTPUT>"> --->
							<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(frmLastUpdated)#">
						</CFIF>
						</CFOUTPUT>
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<Cf_relayFormElementDisplay relayFormElementType="select" currentValue="#frmCommTypeLID#" query="#getCommType#" value="LookupID" display="ItemText" label="Phr_Sys_TypeOfComm" fieldname="frmCommTypeLID" disabled="#readonly#" required="true" showNull="true">
							<Cf_relayFormElementDisplay relayFormElementType="select" required="true" label="Associated Site" fieldname="frmLinkToSiteURL" query="#siteQuery#" currentvalue="#frmLinkToSiteURL#" disabled="#readonly#">

							<cfoutput>

							<Cf_relayFormElementDisplay relayFormElementType="text" currentValue="#frmTitle#"  SIZE="80" MAXLENGTH="80" fieldname="frmTitle" label="Phr_Sys_SubjectLine" required="true" disabled="#readonly#">
							<Cf_relayFormElementDisplay relayFormElementType="text" currentValue="#frmDescription#"  SIZE="80" MAXLENGTH="80" fieldname="frmDescription" label="Phr_Sys_ShortDescription" required="true" disabled="#readonly#">
							<Cf_relayFormElementDisplay relayFormElementType="text" currentValue="#frmFromAddress#" validate="email" SIZE="80" MAXLENGTH="80" fieldname="frmFromAddress" label="Phr_Sys_Comm_FromEmailAddress:<BR>(Phr_Sys_Comm_FromEmailAddressNote)" disabled="#readonly#">

							<cfquery name="displayName" datasource="#application.siteDataSource#">
									select distinct displayName as display, cast(commFromDisplayNameID as varchar) as value from commFromDisplayName where active = 1
									union
									select '#application.EmailFromDisplayName#' as display,'#application.EmailFromDisplayName#' as value where not exists(select 1 from commFromDisplayName where displayName  =  <cf_queryparam value="#application.EmailFromDisplayName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
							</cfquery>

							<Cf_relayFormElementDisplay relayFormElementType="select" label="Phr_Sys_Comm_FromDisplayName:<BR>(Phr_Sys_Comm_FromDisplayNameNote)" fieldname="frmCommFromDisplayNameID" query="#displayName#" display="display" value="value" currentvalue="#frmCommFromDisplayNameID#" disabled="#readonly#">
							<a title="Add From Name" onclick="newSelectValue($('frmCommFromDisplayNameID'),'Add From Name');return false;"><span class="fa fa-plus-circle"></span></a>

							<Cf_relayFormElementDisplay relayFormElementType="text" currentValue="#frmreplyto#"  SIZE="30" MAXLENGTH="50" fieldname="frmreplyto" label="Phr_Sys_SendRepliesTo" validate="email" disabled="#readonly#">

						</div>
						<div class="col-xs-12 col-sm-6">
							<cfif application.com.settings.getSetting("campaigns.displayCampaigns")>
								<!--- <cfset getCampaigns = application.com.relayCampaigns.getCampaigns()> --->
								<cf_relayFormElementDisplay type="select" name="frmCampaignID" label="Phr_Sys_AssociateCampaign" query="func:com.relayCampaigns.GetCampaigns()" value="campaignID" display="campaignName" disabled="#readonly#" currentValue="#frmCampaignID#" nullText="Phr_Ext_SelectAValue">
							</cfif>

							</cfoutput>
							<cfif task is not "edit" and not readonly>
								<!--- loading from a template only makes sense when creating a communication --->
								<cf_relayFormElementDisplay type="html" label="Phr_Sys_LoadATemplate">
									<SELECT class="form-control" NAME="frmEmailTemplate" id="frmEmailTemplate" size="1">
										<CFOUTPUT query="getemailtemplates" group="priority">
											<OPTION VALUE="0">--------------------</OPTION>
											<cfoutput>
												<OPTION VALUE="#filename#" <CFIF frmEmailTemplate is filename>SELECTED</CFIF>>#htmleditformat(description)#</OPTION>
											</cfoutput>
										</CFOUTPUT>
									</SELECT>
									<A HREF="JavaScript:submitPreview();">phr_Preview</a>
								</cf_relayFormElementDisplay>
							</cfif>

							<cfif not readonly>
								<cf_relayFormElementDisplay type="file" accept="zip" label="Phr_Sys_UploadEmailZip" fieldname="frmUploadEmailZip" currentValue="">
							</cfif>
							<!--- 2014-08-15	RPW	Improve Comms Template description --->
							<cf_relayFormElementDisplay type="html" label="Phr_Sys_Media">
								<cfif Variables.task eq "edit">
									<cfoutput>
									<CF_INPUT TYPE="hidden" NAME="frmCommFormLID" ID="frmCommFormLID" Value="#frmCommFormLID#">
									</cfoutput>
									<SELECT class="form-control" NAME="frmCommFormLID_display" <cfif Variables.task eq "edit" or readonly>disabled</cfif>>
								<cfelse>
									<SELECT class="form-control" NAME="frmCommFormLID" ID="frmCommFormLID" <cfif Variables.task eq "edit" or readonly>disabled</cfif>>
								</cfif>
										<CFOUTPUT QUERY="getCommForm">
											<OPTION VALUE="#LookupID#" <cfif LookupID is frmCommFormLID>selected</cfif>> #htmleditformat(ItemText)#
										</CFOUTPUT>
									</SELECT>
							</cf_relayFormElementDisplay>
							<cf_relayFormElementDisplay type="html" label="Phr_Sys_EmailFormat">
							    <!--- NYB 2011-01-20 LHID 4918 - changed to be generated from a query: --->
								<SELECT class="form-control" NAME="frmCommEmailFormat" ONCHANGE="toggleStyleSheetSelector(this.value)" ID="frmCommEmailFormat" <cfif readonly>disabled</cfif>>
								<cfoutput QUERY="getCommEmailFormats">
									<OPTION VALUE="#CommEmailFormatID#" <cfif frmCommEmailFormatID is CommEmailFormatID>SELECTED</cfif>>Phr_Sys_#htmleditformat(CommEmailFormat)#</OPTION>
								</CFoutput>
								</SELECT>
								<cfset qStyleSheets = application.com.email.getEmailStyles()>
								<cfif qStyleSheets.recordCount>
									<SPAN ID="styleSheetSelector"<cfif frmCommEmailFormatID is 2> STYLE="display:none"</cfif>>
									phr_comm_use_this_stylesheet:
									<SELECT class="form-control" NAME="frmStyleSheet"<cfif readonly> disabled</cfif>>
										<OPTION value="">phr_ext_selectAValue</OPTION><cfloop query="qStyleSheets"><cfoutput><OPTION VALUE="#fileName#"<cfif frmStyleSheet is fileName> SELECTED</cfif>>#name#</OPTION></cfoutput></cfloop>
									</SELECT>
									</SPAN>
								</cfif>
							</cf_relayFormElementDisplay>
							<cf_relayFormElementDisplay type="html" label="Phr_Sys_CharacterSet">
							    <cf_displayvalidValues formfieldname = "frmCharacterSet" attributecollection = #frmCharacterSetAttributes#>
							</cf_relayFormElementDisplay>
							<div class="checkbox">
								<label>
									Phr_Sys_TrackThisEmail:
									<input type="checkbox" name="frmTrackEmail" id="frmTrackEmail" value = "1" <CFIF frmTrackEMail is 1> checked="yes"</CFIF> <cfif readonly>disabled</cfif>>
								</label>
							</div>
							<!--- <Cf_relayFormElementDisplay relayFormElementType="checkbox" fieldname="frmTrackEmail" label="Phr_Sys_TrackThisEmail" currentValue="#frmTrackEMail#" disabled="#readonly#"> --->
							<cfif application.com.settings.getSetting("communications.tracking.allowOmnitureTracking") is "True">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="frmTrackEmailWithOmniture" id="frmTrackEmailWithOmniture" value = "1" <CFIF frmTrackEmailWithOmniture is 1> checked="yes"</CFIF> <cfif readonly>disabled</cfif>>
										Phr_Sys_TrackThisEmailWithOmniture
									</label>
								</div>
								<!--- <Cf_relayFormElementDisplay relayFormElementType="checkbox" fieldname="frmTrackEmailWithOmniture" label="Phr_Sys_TrackThisEmailWithOmniture" currentValue="#frmTrackEmailWithOmniture#" disabled="#readonly#"> --->
							</cfif>
							<cfif not readonly>
									<CFINCLUDE TEMPLATE="saveAndContinueButtons.cfm">
							</cfif>
						</div>

					</cf_relayFormDisplay>

				</FORM>

	<!--- 2011-11-04 NYB LHID8007 removed this condition
	<cfif getSelections.recordCount gt 0>
	<cfelse><!--- if they have no selection records show this --->
			<tr>
				<td>
					<!--- NJH 2009/11/04 LID 2778 - check if user has rights to create a selection before showing the link.. Also changed some of the text
						to fit with the results --->
					<cfset hasPermissions = application.com.security.testFileSecurity(scriptName="/selection/selectsearch.cfm")>
					<p>Before you can send a communication to anyone using the system you must first create a selection
					using the selection tool. <cfif hasPermissions.isOk><cfoutput><a href="javascript:openTabByName('Selections')"<!--- href="/selection/selectsearch.cfm" --->>Click here</a></cfoutput> to create a selection.</p></cfif>
					<cfif not hasPermissions.isOk>
					<p>However, you do not have rights to create selections. Please contact an administrator who has rights
					to grant you access to a selection.</p></cfif>
				</td>
			</tr>
	</cfif>
	--->
</cfif>