<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
   1999-02-21  SWJ Modified javascript to call report/wabcommcheck.cfm
--->
<CFIF findNoCase("phoneStats.cfm",SCRIPT_NAME,1) or findNoCase("phoneStats2.cfm",SCRIPT_NAME,1) or findNoCase("phoneStats3.cfm",SCRIPT_NAME,1) or findNoCase("phoneStatsDrill.cfm",SCRIPT_NAME,1) or findNoCase("phoningHome.cfm",SCRIPT_NAME,1)>
<CFPARAM name="frmruninpopup" default=""><!--- wab --->
<CFOUTPUT>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" CLASS="withBorder">
	<CFIF frmruninpopup IS NOT "True">
		<TR>	
			<TD ALIGN="RIGHT" VALIGN="top" CLASS="Submenu">
 				<cfif isDefined("commid")>
					<A href="#thisdir#/PhoneStats.cfm?commid=#commid#" Class="SubMenu">Stats</A>  |
				</CFIF>
				<CFIF #checkPermission.AddOkay# GT 0>
					<A href="#thisdir#/PhoneCommEdit.cfm" Class="SubMenu">New</A>
				</CFIF> 
			</TD>
		</TR>
	</CFIF>
</TABLE>
</CFOUTPUT>
</CFIF>