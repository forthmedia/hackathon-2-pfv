<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Ammendments
2001-07-17  	SWJ		Changed the listing screen to add features
2001-07-18	SWJ		Added function to set the phoneAction.phoneActionCompleted row to 1.
					This has the result of removing the record from the campaign.
2001-08-18	SWJ		Added additional options for analysis
2001-08-20	SWJ		Modified UpdatePhoneAction to set lastCallUser = #request.relayCurrentUser.usergroupid#
2001-11-07	SWJ		Modified screen layout to work with new look and feel.

Stuff to do:
1.  main query needs to have the location stuff removed

 --->



<CFIF isDefined('frmRemovePhoneActionID') and frmRemovePhoneActionID neq 0>

	<CFQUERY NAME="getFlagIDforRemoveFlag" datasource="#application.siteDataSource#">
		select flagID from flag where flagTextID ='RemoveFromCampaign'
	</CFQUERY>
	<!--- set the phone action record so that it shows the record as removed.  We do not 
		delete this from phone actions which would be an option as we may want to supress 
		records from phoning campaigns in the future --->
	<CFQUERY NAME="UpdatePhoneAction" datasource="#application.siteDataSource#">
		update phoneAction 
			set phoneActionCompleted = 1, 
				lastCallResult =  <cf_queryparam value="#getFlagIDforRemoveFlag.flagID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				phoneActionLock = null,
				lastCallUser = #request.relayCurrentUser.usergroupid#
			where phoneActionID =  <cf_queryparam value="#frmRemovePhoneActionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>	
</CFIF>

<CFIF isDefined('frmDeletePhoneActionID') and frmDeletePhoneActionID neq 0>
	<!--- set the phone action record so that it shows the record as removed.  We do not 
		delete this from phone actions which would be an option as we may want to supress 
		records from phoning campaigns in the future --->
	<CFQUERY NAME="DeletePhoneAction" datasource="#application.siteDataSource#">
		delete from phoneAction 
			where phoneActionID =  <cf_queryparam value="#frmDeletePhoneActionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>	
</CFIF>


<CFQUERY NAME="getCurrentPhoningCampaigns" datasource="#application.siteDataSource#">
	SELECT DISTINCT 
		    PhoneCommRecords.CommTitle,
		    PhoneCommRecords.CommID
	FROM PhoneCommRecords LEFT
	     OUTER JOIN
	    PhoneAction ON 
	    PhoneCommRecords.CommID = PhoneAction.CommID
	WHERE complete = 0      
		order by commTitle
</CFQUERY>


<!---Drill-Down Display--->
<CFIF not IsDefined('CommID')>
	<CFOUTPUT QUERY="getCurrentPhoningCampaigns" STARTROW=1 MAXROWS=1>
		<CFSET commID = commID>
		<cfset campaign = commTitle>
	</CFOUTPUT>
</CFIF>

<CFSET 	campaignCommID = CommID>
<!--- get campaignDetails, takes campaignCommID --->
<CFINCLUDE template="qryCampaignDetails.cfm">
<CFPARAM NAME="sort" DEFAULT="2">
<!--- this is the query we should be using below as the subquery
	select max(commDetailID) from commdetail inner join communication
	on communication.CommID = commdetail.CommID
		WHERE LocationID=2221245 
		and commTypeLID in (1,2,3,5,6,7,8)
	 --->
<CFPARAM NAME="UserGroupID" DEFAULT="-1">
<CFPARAM NAME="lastCallDate" DEFAULT="0">
<CFPARAM NAME="flagID" DEFAULT="-1">
<CFPARAM NAME="campaign" TYPE="string" DEFAULT=" ">

<CFQUERY NAME="GetDetails" datasource="#application.siteDataSource#">
<!--- select pa.PhoneActionID, pa.LastCallDate, p.FIrstName, p.LastName, o.OrganisationID, 
cd.feedback, o.Organisationname , cd.CommDetailID, cd.DateSent
from PhoneAction pa
left join Person p on pa.PersonID = p.PersonID
join Organisation o on pa.OrganisationID = o.OrganisationID
left join 
	(select p2.organisationid, cd2.*
	from person p2
	join commdetail cd2 on p2.personid = cd2.personid
	where cd2.commdetailid = (
	select top 1 CommdetailID 
			from CommDetail 
			where DateSent = (select max(cdi.DateSent) 
					  from CommDetail cdi
					  join Person pi on cdi.PersonID = pi.PersonID
					  where pi.OrganisationID = p2.OrganisationID))) as cd
on 	o.organisationID = cd.OrganisationID
where pa.commid =137 --->

<!--- 	SELECT PhoneAction.LastCallDate, Flag.Name AS Outcome, UserGroup.Name AS Agent, phoneAction.organisationID,
		(SELECT OrganisationName FROM organisation o 
	        	WHERE o.organisationID = phoneAction.organisationID) AS orgName,
		(select top 1 feedback, dateSent from commDetail c2 
			inner join person p2 on p2.personID = c2.personid
			where p2.organisationID = phoneAction.organisationID order by dateSent desc),
		Location.SiteName, Location.Address4, location.address5, Person.FirstName, 
		Person.LastName, Location.LocationID, PhoneAction.CommID, location.telephone,
		PhoneAction.PhoneActionLock, phoneActionID, UserGroup.UserGroupID,
		(select max(commDetailID) from commdetail 
		WHERE LocationID = location.LocationID) as maxCommDetailID,
		CASE WHEN PhoneAction.PhoneActionLock = 1 THEN (SELECT Name FROM UserGroup Where UserGroupid = PhoneActionLockBy) ELSE '' END AS LockedBy
	FROM Person RIGHT JOIN (Flag INNER JOIN ((Location INNER JOIN Country 
		ON Location.CountryID = Country.CountryID) INNER JOIN (UserGroup INNER JOIN PhoneAction 
		ON UserGroup.UserGroupID = PhoneAction.LastCallUser) 
		ON Location.LocationID = PhoneAction.LocationID) ON Flag.FlagID = PhoneAction.LastCallResult) 
		ON Person.PersonID = PhoneAction.PersonID 
		
		DAM 10 July 2002
		Introduced this new query...
		
		--->
		
		SELECT 
			PhoneAction.LastCallDate, 
			Flag.Name AS Outcome, 
			UserGroup.Name AS Agent, 
			phoneAction.organisationID, 
			(SELECT OrganisationName 
				FROM organisation o 
				WHERE o.organisationID = phoneAction.organisationID) AS orgName, 
			lastcomm.lastcomm as DateSent, 
			lastcomm.feedback,
		 	Location.SiteName, 
			Location.Address4, 
			location.address5, 
			Person.FirstName, 
			Person.LastName, 
			Location.LocationID, 
			PhoneAction.CommID, 
			location.telephone, 
			PhoneAction.PhoneActionLock, 
			phoneActionID, 
			UserGroup.UserGroupID, 
			lastcomm.commDID as maxCommDetailID, 
			CASE 
				WHEN PhoneAction.PhoneActionLock = 1 
				THEN (SELECT Name FROM UserGroup Where UserGroupid = PhoneActionLockBy) 
			ELSE 
				'' END AS LockedBy 
		FROM Person 
		INNER JOIN PhoneAction
		ON Person.PersonID = PhoneAction.PersonID 
		left outer JOIN 
		Flag 
		ON Flag.FlagID = PhoneAction.LastCallResult
		INNER JOIN Location 
		ON Location.LocationID = PhoneAction.LocationID 
		INNER JOIN 
		Country ON Location.CountryID = Country.CountryID
		INNER JOIN 
		UserGroup  ON UserGroup.UserGroupID = PhoneAction.LastCallUser
		left outer JOIN 
		(SELECT commDID, OrganisationID, Feedback, LastComm FROM
		(select max(commDetailID) commDID, organisationID, max(dateSent) lastComm from commDetail c
		INNER JOIN person p ON p.personid = c.personid
		group by p.organisationID) a
		INNER JOIN commDetail ON a.commDID = commDetail.commDetailID) LastComm
		ON LastComm.organisationID = phoneAction.organisationID
		WHERE PhoneAction.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	<CFIF IsDefined('FlagID') and flagid neq -1>
		AND PhoneAction.LastCallResult =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
			
	<!---If Date Range was specified during the Search Phase--->
	<CFIF IsDefined('DateFrom') IS NOT "" AND IsDefined("DateTo")>
		AND PhoneAction.LastCallDate >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND PhoneAction.LastCallDate <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	</cfif>
	
	<!---If a Country was specified during the Search Phase--->
	<CFIF IsDefined("CountryID") AND #CountryID# NEQ 0>
		AND Location.CountryID =  <cf_queryparam value="#CountryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
	
	<!---If a userGroupID was specified--->
	<CFIF IsDefined("userGroupID") AND userGroupID NEQ -1>
		AND UserGroup.UserGroupID =  <cf_queryparam value="#userGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>

	<!---If a Country was specified during the Search Phase--->
	<CFIF IsDefined("LastCallDate") AND LastCallDate NEQ 0>
		AND PhoneAction.LastCallDate >=  <cf_queryparam value="#CreateODBCDate(lastCallDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and PhoneAction.LastCallDate <=  <cf_queryparam value="#CreateODBCDate(lastCallDate+1)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	</CFIF>

	ORDER BY 
	<CFIF IsDefined("Sort")>
			<CFSWITCH EXPRESSION="#Sort#">
				<CFCASE VALUE=1>LastCallDate DESC</CFCASE>
				<CFCASE VALUE=2>SiteName ASC</CFCASE>
				<CFCASE VALUE=3>LastName</CFCASE>
				<CFCASE VALUE=4>Agent</CFCASE>
				<CFCASE VALUE=5>Outcome</CFCASE>
			</CFSWITCH>
	</CFIF>
</CFQUERY>

<!--- DEBUG 
<CFIF lastcalldate neq 0>
	<CFOUTPUT>lastcalldate: #lastCallDate# date1: #CreateODBCDate(lastCallDate)# date2: #CreateODBCDate(lastCallDate+1)#</CFOUTPUT>
</CFIF>
--->

<CFQUERY NAME="getAgents" datasource="#application.siteDataSource#">
	SELECT DISTINCT userGroupID, name from phoneAction pa
		inner join userGroup ug on pa.LastCallUser = ug.userGroupID
	where commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" >  order by name
</CFQUERY>

<CFQUERY NAME="getDates" datasource="#application.siteDataSource#">
	SELECT DISTINCT convert(dateTime,convert(char(11),lastCallDate)) as lcDate
	from phoneAction
	where commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by convert(dateTime,convert(char(11),lastCallDate)) desc
</CFQUERY>

<CFQUERY NAME="getCallResults" datasource="#application.siteDataSource#">
	select distinct status, flagid from phoneFlagRecords p
		inner Join phoneAction pa on p.flagID = pa.LastCallResult
		where pa.commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>



<cf_head>
<SCRIPT type="text/javascript">
<!--
	function formSubmit(){
		document.forms.PhoneStatsDrill.submit()
	}
	
	function viewOrg(orgID){
		form = document.dataFrame;
		form.frmsrchOrgID.value = orgID;
		form.submit();
	}
	
	function removeFromPhoningList(phoneActionID){
		form = document.PhoneStatsDrill;
		form.frmRemovePhoneActionID.value = phoneActionID;
		form.submit();
	}
	
	function deleteFromPhoningList(phoneActionID){
		form = document.PhoneStatsDrill;
		form.frmDeletePhoneActionID.value = phoneActionID;
		form.submit();
	}

	<CFINCLUDE TEMPLATE="../screen/jsViewEntity.cfm">
//-->
</SCRIPT>

</cf_head>

<cfset subsection = "Call Statistics for Campaign:" & campaign>
<!---Display as above--->


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="2" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<FORM NAME="PhoneStatsDrill" ACTION="PhoneStatsDrill.cfm" METHOD="post">
<!---Pass variables to the next phase--->
<CFOUTPUT>
	<CFIF IsDefined('CountryID')>
		<CF_INPUT TYPE="hidden" NAME="CountryID" VALUE="#CountryID#">
	</cfif>		
	<CF_INPUT TYPE="hidden" NAME="Campaign" VALUE="#Campaign#">
	<INPUT TYPE="hidden" NAME="frmRemovePhoneActionID" VALUE="0">	
	<INPUT TYPE="hidden" NAME="frmDeletePhoneActionID" VALUE="0">
</CFOUTPUT>	

<tr>
	<TD colspan="10">
		<TABLE>
			<TR>
			<TD NOWRAP>
				Sort by: <BR>
				<SELECT NAME="Sort" onChange="formSubmit()" >
					<OPTION VALUE=1 <CFIF IsDefined("Sort")><CFIF #Sort# EQ 1>SELECTED</CFIF></CFIF>>Date 
					<OPTION VALUE=2 <CFIF IsDefined("Sort")><CFIF #Sort# EQ 2>SELECTED</CFIF></CFIF>>Company
					<OPTION VALUE=3 <CFIF IsDefined("Sort")><CFIF #Sort# EQ 3>SELECTED</CFIF></CFIF>>Contact
					<OPTION VALUE=4 <CFIF IsDefined("Sort")><CFIF #Sort# EQ 4>SELECTED</CFIF></CFIF>>Agent
					<OPTION VALUE=5 <CFIF IsDefined("Sort")><CFIF #Sort# EQ 4>SELECTED</CFIF></CFIF>>Situation
				</SELECT>		
			</td>
			<TD NOWRAP><cfset commIDvar = commID>
				Campaign: <BR>
				<SELECT NAME="CommID">
					<CFOUTPUT QUERY="getCurrentPhoningCampaigns">
						<OPTION VALUE="#commID#" #IIF(commid IS commIDvar,DE('SELECTED'),DE(''))#>#htmleditformat(commTitle)#</OPTION>
					</CFOUTPUT>
				</SELECT>
			</TD>
			<td><cfset lastCallDatevar = lastCallDate>
				Last Call Dates:<BR> 
				<SELECT NAME="lastCallDate">
						<OPTION VALUE="0"> </OPTION>
					<CFOUTPUT QUERY="getDates">
						<OPTION VALUE="#lcDate#" #IIF(dateFormat(lcDate,'dd-mmm-yy') IS lastCallDatevar,DE('SELECTED'),DE(''))#>#dateFormat(lcDate,"dd-mmm-yy")#</OPTION>
					</CFOUTPUT>
				</SELECT>
			</td>
			<td><cfset userGroupIDvar = userGroupID>
				Who made contact:<BR> 
				<SELECT NAME="userGroupID">
					<OPTION VALUE="-1"> </OPTION>
					<CFOUTPUT QUERY="getAgents">
						<OPTION VALUE="#userGroupID#" #IIF(userGroupID IS userGroupIDvar,DE('SELECTED'),DE(''))#>#htmleditformat(name)#</OPTION>
					</CFOUTPUT>
				</SELECT>
			</td>
			<td><cfset FlagIDvar = FlagID>
				Call Results:<BR> 
				<SELECT NAME="FlagID">
					<OPTION VALUE="-1"> </OPTION>
					<CFOUTPUT QUERY="getCallResults">
						<OPTION VALUE="#FlagID#" #IIF(FlagID IS FlagIDvar,DE('SELECTED'),DE(''))#>#htmleditformat(status)#</OPTION>
					</CFOUTPUT>
				</SELECT>
			</td>

			<TD VALIGN="bottom"><INPUT TYPE="submit" NAME="Go" VALUE="Go"></TD>
			</TR>
		</TABLE>
	</TD>
</tr>
<TR class="label">
	<th>Last Call Date</th>
	<th>Company</th>
	<TH>Phone number</TH>
	<th>Contact</th>
	<th>Situation</th>
	<th>Who made contact</th>
	<th>View Call History</th>
	<th>Edit</th>
	<TH>Mark as Do Not Phone</TH>
	<TH>Delete from<BR> Campiagn</TH>
</TR>
	
<CFoutput query="GetDetails">
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD><CFIF trim(LastCallDate) neq "">#DateFormat(LastCallDate, "dd/mm/yyyy")#<CFELSE>&nbsp;</CFIF> </TD>
		<TD><B><A HREF="javaScript:viewOrg(#organisationID#)" CLASS="smallLink">#htmleditformat(OrgName)#</A></B> <BR>(#htmleditformat(address4)#, #htmleditformat(address5)#)</TD>
		<TD>#htmleditformat(telephone)#</TD>
		<TD><CFIF trim(FirstName) neq "">#htmleditformat(FirstName)# #htmleditformat(LastName)#<CFELSE>&nbsp;</CFIF></TD>  
		<TD>#htmleditformat(Outcome)# <CFIF PhoneActionLock is 1>(Locked by #htmleditformat(LockedBy)#)</cfif></TD> 
		<TD><CFIF trim(agent) neq "">#htmleditformat(Agent)#<CFELSE>&nbsp;</CFIF></TD>
		<td align="center">
			<a href="javascript:viewEntity('Location',#locationid#, #locationid#, 'callhistory')"><IMG SRC="/images/MISC/IconContact.gif" BORDER=0></a>
		</td>
		<td align="center"><a href="javascript:viewEntity('Location',#locationid#, #locationid#, '#preferredStartScreen#')"><IMG SRC="/images/MISC/IconEditPencil.gif" BORDER=0></a>
		</TD>
		<td align="center"><a href="javascript:removeFromPhoningList(#phoneActionID#)"><IMG SRC="/images/MISC/iconDeleteCross.gif" BORDER="0" ALT="Remove this record from this campaign"></TD>
		</TD>
		<td align="center"><a href="javascript:deleteFromPhoningList(#phoneActionID#)"><IMG SRC="/images/MISC/icon_Delete.gif" BORDER="0" ALT="Delete this record permenently from this campaign"></TD>
	</TR>
	<TR>
		<cfif maxCommDetailID neq ""> 
			<TD VALIGN="top"><B>Last Contact Date:<BR> #dateFormat(GetDetails.dateSent, "dd-mmm-yy")#</B></TD>
			<TD COLSPAN="9">Last Contact Notes: <BR>#htmleditformat(GetDetails.feedback)#</TD>
		<CFELSE>
			<TD COLSPAN="10">No contact history with anyone at this site</TD>
		</CFIF>
	</TR>
	<TR><TD COLSPAN="10" ALIGN="center"><HR WIDTH="100%" SIZE="1"></TD></TR>
</CFoutput>
</form>
	
</TABLE>



<FORM ACTION="../data/dataFrame.cfm" METHOD="post" NAME="dataFrame" ID="dataFrame">
	<INPUT TYPE="hidden" NAME="frmsrchOrgID" VALUE="0">
	<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmNext" VALUE="..#thisDir#/phoneStatsDrill.cfm?#request.query_string#"></CFOUTPUT>
</FORM>




