<!--- �Relayware. All Rights Reserved 2014 --->
<!---			NAVIGATION BUTTONS (view page)			--->

<CFIF 
	ParameterExists(Form.btnView_Previous) or 
	ParameterExists(Form.btnView_Next) or
	ParameterExists(Form.btnView_First) or
	ParameterExists(Form.btnView_Last)>

	<CFQUERY name="GetRecord" dataSource="#application.SiteDataSource#" maxRows=1>

		SELECT Communication.CommID AS ID_Field
		FROM Communication

		<CFIF ParameterExists(Form.btnView_Previous)>
			WHERE Communication.CommID <  <cf_queryparam value="#Form.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			ORDER BY Communication.CommID DESC

		<CFELSEIF ParameterExists(Form.btnView_Next)>
			WHERE Communication.CommID >  <cf_queryparam value="#Form.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			ORDER BY Communication.CommID

		<CFELSEIF ParameterExists(Form.btnView_First)>
			ORDER BY Communication.CommID

		<CFELSEIF ParameterExists(Form.btnView_Last)>
			WHERE Communication.CommID >  <cf_queryparam value="#Form.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			ORDER BY Communication.CommID DESC

		</CFIF>

	</CFQUERY>

	<CFIF GetRecord.RecordCount is 1>
		<CFLOCATION url="PhoneComm_RecordView.cfm?RecordID=#GetRecord.ID_Field#"addToken="false">
	<CFELSE>
		<CFLOCATION url="PhoneComm_RecordView.cfm?RecordID=#Form.RecordID#"addToken="false">
	</CFIF>


<!---			EDIT BUTTON (view page)			--->

<CFELSEIF ParameterExists(Form.btnView_Edit)>

	<CFLOCATION url="PhoneComm_RecordEdit.cfm?RecordID=#Form.RecordID#"addToken="false">


<!---			ADD BUTTON (view page)			--->

<CFELSEIF ParameterExists(Form.btnView_Add)>

	<CFLOCATION url="PhoneComm_RecordEdit.cfm"addToken="false">


<!---			DELETE BUTTON (view page)			--->

<CFELSEIF ParameterExists(Form.btnView_Delete)>

	<CFQUERY name="DeleteRecord" dataSource="#application.SiteDataSource#" maxRows=1>
		DELETE
		FROM Communication
		WHERE Communication.CommID =  <cf_queryparam value="#Form.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<CFLOCATION url="PhoneComm_RecordView.cfm"addToken="false">


<!---			OK BUTTON (edit page)				--->

<CFELSEIF ParameterExists(Form.btnEdit_OK)>

	<CFIF ParameterExists(Form.RecordID)>
		<CFUPDATE dataSource="#application.SiteDataSource#" tableName="Communication" formFields="#Form.FieldList#">
		<CFLOCATION url="PhoneComm_RecordView.cfm?RecordID=#Form.RecordID#"addToken="false">

	<CFELSE>
		<CFINSERT dataSource="#application.SiteDataSource#" tableName="Communication" formFields="#Form.FieldList#">
		<CFQUERY name="GetNewRecord" dataSource="#application.SiteDataSource#" maxRows=1>
			SELECT Communication.CommID AS ID_Field
			FROM Communication
			ORDER BY Communication.CommID DESC
		</CFQUERY>
		<CFLOCATION url="PhoneComm_RecordView.cfm?RecordID=#GetNewRecord.ID_Field#"addToken="false">

	</CFIF>


<!---			CANCEL BUTTON (edit page)			--->

<CFELSEIF ParameterExists(Form.btnEdit_Cancel)>

	<CFIF ParameterExists(Form.RecordID)>
		<CFLOCATION url="PhoneComm_RecordView.cfm?RecordID=#Form.RecordID#"addToken="false">
	<CFELSE>
		<CFLOCATION url="PhoneComm_RecordView.cfm"addToken="false">
	</CFIF>

</CFIF>>