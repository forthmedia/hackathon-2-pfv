<!--- �Relayware. All Rights Reserved 2014 --->
<CFQUERY name="GetRecord" dataSource="#application.SiteDataSource#" maxRows=1>
	SELECT Communication.Title AS ViewField1, Communication.Description AS ViewField2, Communication.SendDate AS ViewField3, Communication.CommID AS ID_Field
	FROM Communication
	<CFIF ParameterExists(URL.RecordID)>
	WHERE Communication.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
</CFQUERY>


<cf_head>
	<cf_title>PhoneComm - View Record</cf_title>
</cf_head>

<FONT size="+1">PhoneComm</FONT> <BR>
<FONT size="+2"><B>View Record</B></FONT>

<CFOUTPUT query="GetRecord">

<FORM action="PhoneComm_RecordAction.cfm" method="post">
	<CF_INPUT type="hidden" name="RecordID" value="#ID_Field#">

	<!--- form buttons --->
	<INPUT type="submit" name="btnView_First" value=" << ">
	<INPUT type="submit" name="btnView_Previous" value="  <  ">
	<INPUT type="submit" name="btnView_Next" value="  >  ">
	<INPUT type="submit" name="btnView_Last" value=" >> ">
	<INPUT type="submit" name="btnView_Add" value="   Add    ">
	<INPUT type="submit" name="btnView_Edit" value="  Edit  ">
	<INPUT type="submit" name="btnView_Delete" value="Delete">

</FORM>


<TABLE>

	<TR>
	<TD valign="top"> Title: </TD>
	<TD> #htmleditformat(ViewField1)# </TD>
	</TR>
	<TR>
	<TD valign="top"> Description: </TD>
	<TD> #htmleditformat(ViewField2)# </TD>
	</TR>
	<TR>
	<TD valign="top"> SendDate: </TD>
	<TD> #htmleditformat(ViewField3)# </TD>
	</TR>
</TABLE>

</CFOUTPUT>

>