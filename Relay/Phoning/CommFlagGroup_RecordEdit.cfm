<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET FormFieldList = "CommID,FlagGroupTextID">

<CFIF ParameterExists(URL.RecordID)>
	<CFQUERY name="GetRecord" dataSource="#application.SiteDataSource#" maxRows=1>
		SELECT CommFlagGroup.CommID, CommFlagGroup.FlagGroupTextID, CommFlagGroup.CommID AS ID_Field
		FROM CommFlagGroup
		<CFIF ParameterExists(URL.RecordID)>
		WHERE CommFlagGroup.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
	</CFQUERY>

	<CFIF not ListFind( FormFieldList, "CommID" )>
		<CFSET FormFieldList = ListAppend( FormFieldList, "CommID" )>
	</CFIF>

			
			<CFSET CommID_Value = #GetRecord.CommID#>
				
			<CFSET FlagGroupTextID_Value = '#GetRecord.FlagGroupTextID#'>
		

<CFELSE>

			
			<CFSET CommID_Value = ''>
				
			<CFSET FlagGroupTextID_Value = ''>
		

</CFIF>


<cf_head>
	<cf_title>CommFlagGroup - Edit Record</cf_title>
</cf_head>

<FONT size="+1">CommFlagGroup</FONT> <BR>
<FONT size="+2"><B>Edit Record</B></FONT>

<CFOUTPUT>
<FORM action="CommFlagGroup_RecordAction.cfm" method="post">
<CF_INPUT type="hidden" name="FieldList" value="#FormFieldList#">
<CFIF ParameterExists(URL.RecordID)>
	<CF_INPUT type="hidden" name="RecordID" value="#URL.RecordID#">
	<CF_INPUT type="hidden" name="CommID" value="#URL.RecordID#">
</CFIF>


<TABLE>

	
	<CFIF not ParameterExists(URL.RecordID)>
	
	<TR>
	<TD valign="top"> CommID: </TD>
    <TD>
	
		<CF_INPUT type="text" name="CommID" value="#CommID_Value#" maxLength="4">
		
	</TD>
	<!--- field validation --->
	<INPUT type="hidden" name="CommID_integer">
	</TR>
	
	</CFIF>
	
	
	<TR>
	<TD valign="top"> FlagGroupTextID: </TD>
    <TD>
	
		<CF_INPUT type="text" name="FlagGroupTextID" value="#FlagGroupTextID_Value#" maxLength="50">
		
	</TD>
	<!--- field validation --->
	</TR>
		
</TABLE>
	
<!--- form buttons --->
<INPUT type="submit" name="btnEdit_OK" value="    OK    ">
<INPUT type="submit" name="btnEdit_Cancel" value="Cancel">

</FORM>
</CFOUTPUT>



>