<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		CommDetailHistory.cfm
Author:			CPS
Date created:	2001-10-18

	Objective - this lists the history records for a given entity commdetail
		
	Syntax	  -	it can be used in the viewer or it requires frmCommDetailID
				to be passed for it to work.  
					
	Parameters -frmCommDetailID - the current commDetail record.
					
	Return Codes - none

Amendment History:

Date 		Initials 	What was changed
2009-06-30      NJH             P-FNL069 - removed call to setParameters.cfm					
--->

<CFIF not isdefined("frmcurrentEntityID")>
	<TABLE BORDER="2" CELLPADDING="3" BGCOLOR="White">
	<TR>
		<TD>CommHistory.cfm requires frmcurrentEntityID.  Please contact
			technical support for assistance telling them you have got this 
			message and have come from <CFOUTPUT>#htmleditformat(HTTP_REFERER)#</CFOUTPUT>
			<CFLOG TEXT="CommHistory.cfm missing frmcurrentEntityID. User came from #HTTP_REFERER#" 
			LOG="APPLICATION" FILE="RelayError" 
			TYPE="Error" DATE="yes" TIME="yes" APPLICATION="yes">
		</TD>
	</TR>
	</TABLE>
	<CF_ABORT>
</cfif>

<CFIF not isdefined("frmEntityType")>
	<TABLE BORDER="2" CELLPADDING="3" BGCOLOR="White">
	<TR>
		<TD>CommHistory.cfm requires frmentityType.  Please contact
			technical support for assistance telling them you have got this 
			message and have come from <CFOUTPUT>#htmleditformat(HTTP_REFERER)#</CFOUTPUT>
			<CFLOG TEXT="CommHistory.cfm missing frmentityType. User came from #HTTP_REFERER#" 
			LOG="APPLICATION" FILE="RelayError" 
			TYPE="Error" DATE="yes" TIME="yes" APPLICATION="yes">
		</TD>
	</TR>
	</TABLE>
	<CF_ABORT>
</cfif>

<CFIF not isdefined("frmCommDetailID")>
	<TABLE BORDER="2" CELLPADDING="3" BGCOLOR="White">
	<TR>
		<TD>CommDetailHistory.cfm requires frmCommDetailID.  Please contact
			technical support for assistance telling them you have got this 
			message and have come from <CFOUTPUT>#htmleditformat(HTTP_REFERER)#</CFOUTPUT>
			<CFLOG TEXT="CommDetailHistory.cfm missing frmCommDetailID. User came from #HTTP_REFERER#" 
			LOG="APPLICATION" FILE="RelayError" 
			TYPE="Error" DATE="yes" TIME="yes" APPLICATION="yes">
		</TD>
	</TR>
	</TABLE>
	<CF_ABORT>
</cfif>

<CFIF isdefined("frmentityType")>

	<CFQUERY NAME="getEntityData" datasource="#application.siteDataSource#">
		<CFIF frmentityType eq "Location">
			Select SiteName AS Name FROM Location 
				WHERE locationId =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<CFELSEIF frmEntityType eq "organisation">
			Select organisationName as Name from organisation 
				where organisationID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelseif frmentityType eq "Person">
			Select firstName +' '+ lastname as name from person 
					where personid =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
		</cfif>
	</CFQUERY>

</cfif>

<!--- decipher any parameters passed in frmGlobalParameters --->
<!--- NJH 2009-06-30 P-FNL069 - removed setParameters.cfm
<CFINCLUDE template="..\screen\setParameters.cfm"> --->


<!--- Query returning search results --->

<CFQUERY NAME="GetCommDetailHistory" DATASOURCE="#application.SiteDataSource#" MAXROWS=10 DBTYPE="ODBC">
	SELECT cdt.Name AS Type
	      ,cdr.Name AS Reason
		  ,cds.Name AS Status
	      ,Com.Title
		  ,Per.FirstName + ' ' + Per.LastName AS PersonName
		  ,Loc.SiteName
		  ,cdh.DateSent
		  ,cdh.Feedback
		  ,Com.CommID
		  ,cdh.LocationID
		  ,cdh.LastUpdatedBy
		  ,(SELECT name FROM usergroup WHERE usergroupid = cdh.lastupdatedby) AS agent
      FROM CommDetailHistory cdh RIGHT OUTER JOIN 
	       Person per ON Per.PersonID = cdh.PersonID RIGHT OUTER JOIN 
	       Location loc ON Loc.LocationID = cdh.LocationID INNER JOIN
           CommDetailType cdt ON cdt.commTypeID = cdh.CommTypeId INNER JOIN 
           CommDetailReason cdr ON cdr.commReasonID = cdh.CommReasonId INNER JOIN 
           CommDetailStatus cds ON cds.commStatusID = cdh.CommStatusId INNER JOIN 
           commDetail cod ON cod.CommDetailID = cdh.CommDetailID INNER JOIN 
           Communication com ON com.CommId = cod.CommId
	 WHERE cdh.commDetailID = #frmCommDetailID#
  ORDER BY cdh.DateSent ASC
</CFQUERY>

<cf_head>
    <cf_title>CommHistory - Search Result</cf_title>

<SCRIPT type="text/javascript">
<!--

	function doForm(var1,var2) {
		var form = document.mainForm;
		form.frmCurrentEntityID.value = var1;
		form.frmentityType.value = var2;
		form.submit();
	}
//-->
</SCRIPT>

</cf_head>

<FORM ACTION="../phoning/CommHistory_DetailEdit.cfm" METHOD="post" NAME="mainForm"><noBR>
	<INPUT TYPE="hidden" NAME="frmCurrentEntityID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmentityType" VALUE="">
	<INPUT TYPE="hidden" NAME="frmCommDetailID" VALUE="0">

	<CFIF #GetCommDetailHistory.RecordCount# GT 0>
	
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" BGCOLOR="White">
			<TR>
				<CFOUTPUT>
				<TD COLSPAN="8" CLASS="subMenu">Contact Detail History for #htmleditformat(getEntityData.Name)#</TD>
				<TD ALIGN="RIGHT" VALIGN="top" CLASS="subMenu">
<!--- 					<A HREF="javaScript:doForm(#frmcurrentEntityID#,'#frmentityType#')" TITLE="Add new contact history record" CLASS="subMenu">New</a>
 --->					<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
				</TD>
				</CFOUTPUT>
			</TR>
			<TR><TD COLSPAN="9">&nbsp;</TD></TR>
			<TR>
			    <TH>Campaign</TH>
				<TH>Who with</TH>
				<TH>Where</TH>				
				<TH>Contacted by</TH>
			    <TH>Feedback/Notes</TH>
			    <TH>Contact date</TH>
			    <TH>Type</TH>
			    <TH>Reason</TH>
			    <TH>Status</TH>	
			</TR>

			<CFOUTPUT query="GetCommDetailHistory">

				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
					<TD VALIGN="top"><cfif trim(Title) neq "">#htmleditformat(Title)#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top"><cfif trim(personName) neq "">#htmleditformat(PersonName)#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top"><cfif trim(SiteName) neq "">#htmleditformat(SiteName)#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top"><cfif trim(agent) neq "">#htmleditformat(agent)#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top">#left(Feedback,80)# <nobr><CFIF mid(feedback, 80, 50) neq ""><A href="../Phoning/CommHistory_Detail.cfm?ID=#URLEncodedFormat(frmCommDetailID)#" CLASS="smallLink"> ... more</A></CFIF></TD>
					<TD VALIGN="top"><cfif trim(DateSent) neq "">#dateFormat(DateSent,"dd-mmm-yy")#<BR>#timeFormat(DateSent,"HH:mm")#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top"><cfif trim(Type) neq "">#htmleditformat(Type)#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top"><cfif trim(Reason) neq "">#htmleditformat(Reason)#<CFELSE>&nbsp;</CFIF></TD>
					<TD VALIGN="top"><cfif trim(Status) neq "">#htmleditformat(Status)#<CFELSE>&nbsp;</CFIF></TD>
				</TR>
			</CFOUTPUT>

			<TR><TDcolspan="9">&nbsp;</td></tr>
			<CFIF #GetCommDetailHistory.RecordCount# GT 5>
				<TR><TD colspan="9"><CFOUTPUT><A HREF="javaScript:doForm(#frmcurrentEntityID#,'#frmentityType#')">Add a New Contact History Record</a></cfoutput></td></tr>
			</CFIF>
		</TABLE>
	
	<CFELSE>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" BGCOLOR="White">
			<TR><TD COLSPAN="8" CLASS="subMenu">Contact History for <CFOUTPUT>#htmleditformat(getEntityData.Name)#</CFOUTPUT></TD></TR>
			<TR ALIGN="center"><TH>No Contact History with <CFOUTPUT>#htmleditformat(getEntityData.Name)#</CFOUTPUT></TH></tr>
			<TR ALIGN="center"><TD><CFOUTPUT><A HREF="javaScript:doForm(#frmcurrentEntityID#,'#frmentityType#')">Add a New Contact History Record</a></cfoutput></td></tr>
		</TABLE>		
	
	</CFIF>

</FORM>	






