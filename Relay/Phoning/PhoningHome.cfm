<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
Ammendments
2001-07-17  SWJ	Changed the listing screen to add features
 --->

<CFPARAM NAME="message" DEFAULT=" ">
<CFPARAM NAME="frmOrderBy" DEFAULT="CommType">
<CFPARAM NAME="frmShowComplete" DEFAULT="0">

<CFQUERY NAME="GetOutboundCommIDs" datasource="#application.siteDataSource#">
	SELECT DISTINCT 
	    PhoneCommRecords.CommTitle, SendDate,
	    PhoneCommRecords.CommID,
	        (SELECT COUNT(*)
	     		 FROM PhoneAction
	      		WHERE PhoneAction.CommID = PhoneCommRecords.CommID) AS NumRecs, 
		 PhoneCommRecords.CallType, 
		 (SELECT COUNT(*) 
			 	FROM PhoneAction
		      	WHERE PhoneAction.CommID = PhoneCommRecords.CommID
				and PhoneAction.totalCalls=0) AS Untried,
		(SELECT COUNT(*) 
			 	FROM PhoneAction
		      	WHERE PhoneAction.CommID = PhoneCommRecords.CommID
			  	AND PhoneAction.PhoneActionLock=0
				AND PhoneAction.PhoneActionCompleted=0) AS NumRecsLeft,
		(SELECT COUNT(*) 
			 	FROM PhoneAction
		      	WHERE PhoneAction.CommID = PhoneCommRecords.CommID
			  	AND PhoneAction.PhoneActionLock=1
				AND PhoneAction.PhoneActionCompleted=0) AS NumRecsLocked,
	    LookupList.ItemText AS CommType,
		PhoneCommRecords.CallType CommTypeID
	FROM PhoneCommRecords INNER JOIN
	    LookupList ON 
	    PhoneCommRecords.CallType = LookupList.LookupID LEFT
	     OUTER JOIN
	    PhoneAction ON 
	    PhoneCommRecords.CommID = PhoneAction.CommID
	WHERE complete =  <cf_queryparam value="#frmShowComplete#" CFSQLTYPE="CF_SQL_bit" >       <!--- wab: added as a hack to just get rid of some old campaigns - needs to be formalised --->
	Order by <cf_queryObjectName value="#frmOrderBy#">
</CFQUERY>



<cf_head>
	
	<SCRIPT type="text/javascript">
		<CFINCLUDE TEMPLATE="../screen/jsViewEntity.cfm">
		
		function nextLead(commid) {
			entityType = 'Location'
			viewerURL='../phoning/getnextlead.cfm?campaigncommid=' + commid
			viewerName= entityType + "Viewer"
			<CFINCLUDE template="..\screen\jsOpenViewer.cfm">
			viewerWin.focus()
		}
		
		function getNextLead(commid) {
			var form = document.getNextLeadForm;
			form.campaignCommID.value = commid;
			form.submit();
		}

		function wabnextLead(commid) {	
			viewEntity__('Location','..\\phoning\\getnextleadwab.cfm?campaigncommid=' + commid,'','','','CampaignCommid=' + commid,'')
		}

	</SCRIPT>

</cf_head>

<CFIF Len(TRIM(message))>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			<CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>	
		</td>
	</tr>
	</table>
</cfif>

<CFIF #GetOutboundCommIDs.RecordCount# GT 0>
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<FORM ACTION="PhoningHome.cfm?OrderBy=#frmOrderBy#">
	<TR>
		<TD COLSPAN="2">
			<SELECT NAME="frmOrderBy" ONCHANGE="javascript:submit()">
				<OPTION VALUE="1">Select a sort criteria</OPTION>
				<OPTION VALUE="SendDate Desc, CommTitle ASC">Campaign date (descending)</OPTION>
				<OPTION VALUE="SendDate ASC, CommTitle ASC">Campaign date (ascending)</OPTION>
				<OPTION VALUE="CommTitle">Title</OPTION>
				<OPTION VALUE="CommType">Campaign type</OPTION>
			</SELECT>	
		</TD>
		<TD COLSPAN="6">Show completed campaigns
			<INPUT TYPE="radio" NAME="frmShowComplete" VALUE="0" onClick="this.form.submit()" <CFIF frmShowComplete is 0>Checked</CFIF>>No
			<INPUT TYPE="radio" NAME="frmShowComplete" VALUE="1" onClick="this.form.submit()" <CFIF frmShowComplete is 1>Checked</CFIF>>Yes
		</TD>
	</TR>
	<TR>
		<TH>Campaign Name<BR></TH>
		<TH>Date</TH>
		<TH>Type</TH>
		<TH>Organisations<BR>in campaign</TH>
		<TH>Number untried / left<BR>to phone/locked * </TH>
		<TH>List</TH>
		<TH>View <BR>Call Stats</TH>
		<TH>Next Lead</TH>
	</TR>
	<CFOUTPUT QUERY="GetOutboundCommIDs">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD><A HREF="PhoneCommEdit.cfm?RecordID=#CommID#" class="smallLink"><IMG SRC="../images/MISC/iconEditPencil.gif" WIDTH="19" HEIGHT="17" BORDER="0" ALT="Click to edit #CommTitle#"> #htmleditformat(CommTitle)#</A></TD>
			<TD><CFIF SendDate NEQ "">#DateFormat(SendDate , "dd-mmm-yy")#<CFELSE>&nbsp;</CFIF></TD>
			<TD>#htmleditformat(CommType)#</TD>
			<TD ALIGN="CENTER">#htmleditformat(NumRecs)#</TD>
			<TD ALIGN="CENTER">
				<CFIF CommTypeID is 61>
					#htmleditformat(untried)# / #htmleditformat(NumRecsLeft)# 
					<CFIF NumRecsLocked is not 0>
						/ #htmleditformat(NumRecsLocked)#<CFELSE>/ 0
					</cfif>
				<CFELSE>
					&nbsp;
				</CFIF>
			</TD>
			
			<TD><CFIF NumRecs NEQ 0>
					<A HREF="PhoneStatsDrill.cfm?CommID=#CommID#&Campaign=#urlencodedformat(commTitle)#"><IMG SRC="../images/MISC/iconPerson.gif" WIDTH="16" HEIGHT="16" BORDER="0" ALT="Click to view list"></A>
				<CFELSE>&nbsp;
				</CFIF>
			</TD>
				
			<TD ALIGN="CENTER">
				<CFIF NumRecs NEQ 0>
					<A HREF="PhoneStats.cfm?CommID=#CommID#"><IMG SRC="../images/MISC/Icon_Graph.gif" WIDTH="16" HEIGHT="16" BORDER="0" ALT="View stats"></A>
				<CFELSE>&nbsp;
				</CFIF>
			</TD>
			<TD ALIGN="CENTER">
				<CFIF (#NumRecsLeft# GT 0 OR #NumRecsLocked# GT 0) and CommTypeID is 61>
					<A HREF="Javascript:nextLead(#commID#)"><IMG SRC="../images/MISC/iconPhone.gif" WIDTH="32" HEIGHT="25" BORDER="0" ALT="Click here to get next lead in this campaign"></A>
				<CFELSE>&nbsp;
				</CFIF>
				<CFIF (#NumRecsLeft# GT 0 OR #NumRecsLocked# GT 0) and CommTypeID is 61>
					<A HREF="Javascript:getNextLead(#commID#)"><IMG SRC="../images/MISC/iconPhone.gif" WIDTH="32" HEIGHT="25" BORDER="0" ALT="Click here to get next lead in this campaign"></A>
				<CFELSE>&nbsp;
				</CFIF>

			</td>
				<!--- 
				<TD ALIGN="CENTER"><CFIF (#NumRecsLeft# GT 0 OR #NumRecsLocked# GT 0) and CommTypeID is 61>
						<A HREF="Javascript:wabnextLead(#commID#)">
						<IMG SRC="../images/misc/reddot.gif" BORDER=0 ALT="Get next lead for #CommTitle#"></A>
					</CFIF></td> --->
		</TR>	
	</CFOUTPUT>
		<TR><TD COLSPAN="8">* = Records currently locked by another user</TD></TR>
	</FORM>
	
	</TABLE>
	
<CFELSE>
	<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			There are currently no records set up for you to phone on.  Please click New above define a campaign first.
		</td>
	</tr>
	</table>
</CFIF>



<FORM ACTION="getNextLeadv2.cfm" METHOD="post" NAME="getNextLeadForm">
	<INPUT TYPE="hidden" NAME="campaignCommID" VALUE="0">
</FORM>



