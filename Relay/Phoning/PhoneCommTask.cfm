<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		PhoneCommTask.cfm	
Author:			UNKNOWN soldier  
Date started:	UNKNOWN
	
Description:	Tasks to perform when Phone Communication is submitted		

Amendment History:

Date (DD/MM/YYYY)	Initials 	What was changed
2010-05-19 			AJC 		LID 3324 - sent variable need scoping

Possible enhancements:


 --->

<!--- START: 2010-05-19 AJC LID 3324 - sent variable need scoping --->
<CFPARAM name="form.sent" default = "0" />   <!--- this is a checkbox --->
<!--- END: 2010-05-19 AJC LID 3324 - variable need scoping --->
<CFIF PARAMETEREXISTS(FORM.RECORDID)><!--- ie. we are in update mode --->
	<CFUPDATE DATASOURCE="#application.SiteDataSource#" TABLENAME="Communication" FORMFIELDS="#FieldList#">
<CFELSE>
	<!--- We are in insert mode --->
	<CFINSERT DATASOURCE="#application.SiteDataSource#" TABLENAME="Communication" FORMFIELDS="#FieldList#">
	<CFQUERY NAME="GetNewRecord" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
		SELECT Communication.CommID
		FROM Communication
		ORDER BY Communication.CommID DESC
	</CFQUERY>
	<CFSET COMMID = GETNEWRECORD.COMMID>

	<!--- Insert record into PhoneCampaignDetail --->
	<CFQUERY NAME="InsertRecord" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
		Insert into PhoneCampaignDetail  
		(Commid) values (<cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" >)
		
	</CFQUERY>

	
	

</CFIF>

<!--- If we are saving an oubound record, then we should:
	 1.  insert new selection records into CommSelection table
	 2.  Populate phoneAction table  --->
<CFIF CommTypeLID IS 61>
	<CFIF ISDEFINED("frmSelectID")>
		<CFTRANSACTION>
			<!--- Insert new selection records --->
			<CFLOOP INDEX="ListElement" LIST="#frmSelectID#">
				<CFQUERY NAME="InsertIntoCommSelection" datasource="#application.siteDataSource#">
					INSERT INTO dbo.CommSelection ( CommID, SelectionID)
					SELECT <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#ListElement#" CFSQLTYPE="CF_SQL_INTEGER" >
					FROM DUAL	
					WHERE NOT EXISTS (SELECT 1 FROM CommSelection where commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" >  and selectionid =  <cf_queryparam value="#listelement#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</CFQUERY>
			</CFLOOP>
			<!--- Add any new phoneAction records --->
			<CFINCLUDE TEMPLATE="PopulatePhoneActions.cfm">
		</CFTRANSACTION>
	</CFIF>
</CFIF>		

<!--- We should always populate CommFlags at this point as these are required when 
setting call results --->
<CFIF ISDEFINED("frmFlagGroupID")>
	<CFTRANSACTION>
		<!--- Insert new selection records --->
		<CFLOOP INDEX="ListElement" LIST="#frmFlagGroupID#">
			<CFQUERY NAME="InsertIntoCommFlagGroup" datasource="#application.siteDataSource#">
				INSERT INTO dbo.CommFlagGroup (CommID, FlagGroupTextID)
				VALUES (<cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#ListElement#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</CFQUERY>
		</CFLOOP>
	</CFTRANSACTION>
</CFIF>

	
<CFSET MESSAGE = "Record saved">

<CFINCLUDE template="PhoningHome.cfm">
<!--- <CFLOCATION URL="PhoningHome.cfm" ADDTOKEN="No"> --->







