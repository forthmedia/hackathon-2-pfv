<!--- �Relayware. All Rights Reserved 2014 --->

<cf_head>
<cf_title>Alter Phoning system</cf_title>

</cf_head>

<FORM ACTION="alterPhoningSystem.cfm">
	<TABLE>
		<TR>
			<TD>This mini app will alter the phoning system to have it 
				work with organisations</TD>
		</TR>
		<TR>
			<TD>The following things will be changed in the database:
			
			<UL>
				<LI>organistaionID will be added to phone action</LI>
				<LI>significantResult will be added to phoneAction</LI>
				<LI>organisationID will be populated</LI>
				<LI>orgID's need to be made unique</LI>
			</UL>
			
			The following things need to be changed in the code:
			
			<UL>
				<LI>organistaionID will be added to phone action</LI>
				<LI>significantResult will be added to phoneAction</LI>
				<LI></LI>
			</UL>
			</TD>
		</TR>
	</TABLE>
	<INPUT TYPE="submit" NAME="alterPhoningSystem">
</FORM>

<CFIF isdefined("alterPhoningSystem")>

	alter table phoneAction add organisationID int null
	
	update phoneAction 
set phoneAction.organisationid = l.organisationID from location l 
inner join phoneaction p
on p.locationID = l.locationID
<!--- orgID needs to be unique per commid or even across the entire phoning 
		spectrum of campaigns - this needs some thought  --->
select distinct commid, organisationID, count(*) from phoneAction 
group by commid, organisationID having count(*) > 1
<CFELSE>
	Message.
	<CF_ABORT>
</CFIF>




