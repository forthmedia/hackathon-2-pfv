<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 


Amendment History
Version	Date		By		Description
2.0	2001-09-17		CPS		New template to edit comm history detail
2.1	2001-10-12		CPS		Altered template to pick up new attributes on commdetail record, namely commtypeid, commreasonid and commstatusid
2.2 2001-10-25		CPS		Incorporate call to eWebEditPro if this is the default editor
	2002-02-03		SWJ		Modified layout slightly
	2003-12-06		SWJ		Fixed date setting, made two related links populate
	2005-02-22		WAB		Changes to deal with long feedback - query moved to communications.cfc and now deals with updating multiple rows of feedback
2008-06-30     SSS      Added Nvarchar Support to feedback field
2010-10-05			WAB			removed references to eWebEditPro
2011-07-04	NYB		P-LEN024 - moved splitStringIntoArray function from communications to globalFunctions
--->

<!--- Query returning detail information for selected item --->
<CFIF IsDefined("frmMode")>

	<!--- WAB 2005-02-28 date changes
		txtDate should now be converted to a date field (using the hidden field _date), so need to add hours and minutes to it 
		<cfset frmDateSent = CreateDateTime(mid(FORM.txtDate,7,4), mid(FORM.txtDate,4,2), left(FORM.txtDate,2),  
		 FORM.frmHour, FORM.frmMinute, 00)>
	--->		 
		<cfset frmDateSent = dateadd("n",form.frmMinute,dateadd("h",FORM.frmHour,txtDate))> 
	
	<!--- Do the update to the database --->	 
	<!--- need to check for long feedback spread out over more than one row, so see how many records exist --->
	<CFQUERY NAME="currentDetails" datasource="#application.siteDataSource#">
	select 
		cd.commid,cd.personid,cd.locationid,cd.commformlid,cd2.commdetailid 
	from 
		commdetail 
			cd inner join commdetail cd2 on  cd.commid = cd2.commid 
											and cd.personid = cd2.personid 
											and cd.lastupdated = cd2.lastupdated 
	where cd.commdetailid =  <cf_queryparam value="#frmCommDetailID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by cd2.commdetailid asc
	</cfquery>

	<!--- create an array of feedbacks --->
	<cfset FeedbackArray = application.com.globalFunctions.splitStringIntoArray (frmfeedback,4000<!--- application. MaxLenCommDetailFeedback --->)> <!--- NJH 2010-08-23 - replaced the app var with the actual number --->
	
	<cfif currentDetails.recordcount gt arrayLen (FeedbackArray)>		
			<!--- more database records than feedback records  
				so add some blank feedback records to make the same length
			--->
			<cfloop index = "I" from="#evaluate("arrayLen (FeedbackArray) + 1")#" to = "#currentDetails.recordcount#">
				<cfset feedbackArray[I] ="">		
			</cfloop>
	</cfif>

	<CFSET FreezeDate = request.requestTimeODBC>	 
	<cfset counter = #arrayLen (FeedbackArray)#>

	<!--- note that for some reason we work backwards through the feedback array so that the end of the feedback is put in the database first - not sure why this is  --->
	<cfloop query = currentDetails>
		<cfset tmpFeedback = feedbackArray[counter]>

		<CFQUERY NAME="UpdateDetail" datasource="#application.siteDataSource#">
		UPDATE commDetail
		   SET commTypeId =  <cf_queryparam value="#form.frmCommTypeId#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		      commReasonId =  <cf_queryparam value="#val(form.frmCommReasonId)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			  commStatusId =  <cf_queryparam value="#val(form.frmCommStatusId)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			  feedback =  <cf_queryparam value="#tmpFeedback#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
              dateSent =  <cf_queryparam value="#frmDateSent#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			  lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			  lastupdatedby = #request.relayCurrentUser.usergroupid#
		 WHERE commDetailId =  <cf_queryparam value="#CommDetailID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		<cfset counter = counter-1>
	</cfloop>			 
		 
	<!--- Update the flag  --->
	<cfparam name="FlagList" default="">
	<cfloop list="#FlagList#" index="FlagIdx">
		<cfif structKeyExists(form,"#FlagIdx#_#frmCommDetailID#") AND FORM["#FlagIdx#_#frmCommDetailID#"] NEQ FORM["#FlagIdx#_#frmCommDetailID#_ORIG"] >
			<cfif val(FORM["#FlagIdx#_#frmCommDetailID#"])>
				<cfset tmp = application.com.flag.setBooleanFlag(flagTextID=FORM["#FlagIdx#_#frmCommDetailID#"],entityID = frmCommDetailID,deleteOtherRadiosInGroup=true)>
			<cfelse>
				<cfset tmp = application.com.flag.unsetBooleanFlag(flagID=FORM["#FlagIdx#_#frmCommDetailID#"],entityID = frmCommDetailID)>
			</cfif>		
		</cfif>
	</cfloop>
	
	<cfif currentDetails.recordcount lt arrayLen (FeedbackArray)>
		<!--- if there are more feedback records than records in the database then we need to do an insert 
			ie. if the feedback has got longer during the edit and now needs an extra record
		--->
		<cfloop index="counter" from= "#counter#" to = "1" step ="-1">
			<cfset tmpFeedback = feedbackArray[counter]>
				<CFQUERY NAME="InsertDetail" datasource="#application.siteDataSource#">
				insert into commDetail (commid, personid,locationid,commformlid,commtypeid, commreasonid, commstatusid,feedback,datesent,lastupdated,lastupdatedby)
				   values (<cf_queryparam value="#currentDetails.commid#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#currentDetails.personid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#currentDetails.locationid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#currentDetails.commformlid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#form.frmCommTypeId#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#val(form.frmCommReasonId)#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#val(form.frmCommStatusId)#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#tmpFeedback#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#frmDateSent#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >)
				</CFQUERY>
		</cfloop>
	</CFIF>		


		
		
	<cfif isDefined('frmNext')>
		<CFLOCATION URL="#frmNext#"addToken="false">
	<CFELSE>
		<CFLOCATION URL="commHistory_Detail.cfm?cdid=#frmCommDetailID#"addToken="false">
	</CFIF>
</CFIF>


<cfset detail = application.com.communications.getCommDetailRecord(#frmCommDetailID#)>

<!--- <CFQUERY name="Detail" dataSource="#application.siteDataSource#">
	SELECT p.firstName+' '+p.lastname AS fullName, p.personID, 
	 (SELECT OrganisationName
        FROM organisation o INNER JOIN location l ON l.organisationid = o.organisationid
        	WHERE l.locationid = cd.locationid) AS orgName, 
		comm.Title,
	   CD.Feedback,
	   CD.DateSent,
	   CD.PersonID,
	   CD.CommDetailID,
	   CD.CommID,
	   cd.commTypeId,
	   cd.commReasonId,
	   cd.commStatusId,
	   cdt.name as Type,
	   cdr.name as Reason,
	   cds.name as Status
	FROM commdetail cd 
	LEFT OUTER JOIN Person p ON CD.PersonID = p.PersonID 
	LEFT OUTER JOIN Communication comm ON CD.CommID = comm.CommID 
	INNER JOIN commDetailType cdt ON cdt.commTypeId = cd.commTypeId
	INNER JOIN commDetailReason cdr ON cdr.commReasonId = cd.commReasonId
	INNER JOIN commDetailStatus cds ON cds.commStatusId = cd.commStatusId
	where CD.CommDetailID = #frmCommDetailID#
</CFQUERY>
 --->
<!--- <CFQUERY NAME="getTypeReasonStatus" datasource="#application.siteDataSource#">
	SELECT cdtrs.commTypeId, cdt.name as Type, cdtrs.commReasonId, cdr.name as Reason, cdtrs.commStatusId, cds.name as Status
	  FROM commDetailTypeReasonStatus cdtrs
INNER JOIN commDetailType cdt ON cdt.commTypeId = cdtrs.commTypeId 
INNER JOIN commDetailReason cdr ON cdr.commReasonId = cdtrs.commReasonId
INNER JOIN commDetailStatus cds ON cds.commStatusId = cdtrs.commStatusId
where 
</CFQUERY> 
 --->
<CFQUERY NAME="getReasonStatus" datasource="#application.siteDataSource#">
	SELECT cdtrs.commTypeId, cdt.name as Type, cdtrs.commReasonId, cdr.name as Reason, cdtrs.commStatusId, cds.name as Status
		  FROM commDetailTypeReasonStatus cdtrs
	INNER JOIN commDetailType cdt ON cdt.commTypeId = cdtrs.commTypeId 
	INNER JOIN commDetailReason cdr ON cdr.commReasonId = cdtrs.commReasonId
	INNER JOIN commDetailStatus cds ON cds.commStatusId = cdtrs.commStatusId
	where cdt.showInAddComm = 1
</CFQUERY> 


<cf_head>
    <cf_title>CommHistory - Detail</cf_title>
</cf_head>

<SCRIPT type="text/javascript">
<!--
	function saveCommDetail() {
		var form = document.mainEditForm;
		//result = require_TypeAndReasonAndStatus();
		//if (result)
		//{	
			form.submit();
		//}
	}
	
//-->
</SCRIPT>

<FORM METHOD="POST" ACTION="CommHistory_DetailEdit.cfm" NAME="mainEditForm">
	<cfif isDefined('frmNext')>
		<CFOUTPUT><CF_INPUT TYPE="Hidden" NAME="frmNext" VALUE="#frmNext#"></CFOUTPUT>
	</CFIF>
<CF_RelayNavMenu pageTitle="">
	<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:saveCommDetail();">
	<CF_RelayNavMenuItem MenuItemText="Cancel" CFTemplate="javascript:history.go(-1);">
</CF_RelayNavMenu>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<CFIF Detail.RecordCount GT 0>
	<CFOUTPUT query="Detail">
	
		<TR>
		    <TD valign=top class="label">Contact:</TD>
		    <TD valign=top>#htmleditformat(fullName)#</TD>
		    </TR>
		<TR>
		    <TD valign=top class="label">Company:</TD>
		    <TD valign=top>#htmleditformat(orgName)#</TD>
		    </TR>
		<TR>
		    <TD valign=top class="label">Title:</TD>
		    <TD valign=top>#htmleditformat(title)#</TD>
		    </TR>
		<TR>
		    <TD valign=top class="label">Current Type:</TD>
		    <TD valign=top>#htmleditformat(CommType)#</TD>
	    </TR>
	     <!--- AWJR 2011-03-30 P-LEX051 : Switch if a commdetail entity type has profiles then hide type (commReasonID, commStatusID) --->
	    <cfif len(CommReason)>
		<TR>
		    <TD valign=top class="label">Current Reason:</TD>
		    <TD valign=top>#htmleditformat(CommReason)#</TD>
	    </TR>
	    </cfif>
	    <cfif len(commStatus)>
		<TR>
		    <TD valign=top class="label">Current Status:</TD>
		    <TD valign=top>#htmleditformat(CommStatus)#</TD>
	    </TR>
	    </cfif>
		<TR>
		    <TD valign=top class="label">Feedback:</TD>
				    <TD valign=top>
					<TEXTAREA COLS="51"
				        ROWS="5"
				        NAME="frmFeedBack">#feedBack#</TEXTAREA>
					</TD>
		    </TR>
		<TR>
		    <TD valign=top class="label">Contact Date:</TD>
		    <TD valign=top><!--- <INPUT NAME="frmDateSent" VALUE="#dateSent#"> --->

				<cf_relaydatefield currentvalue = "#datesent#" fieldname = "txtDate" thisformname="mainEditForm" helpText="" anchorname="x">

				<cfset thisHour = TimeFormat(dateSent,"HH")>
				HH:<select name="frmHour">
						<option value="00" <CFIF thisHour eq 00>selected</CFIF>>00</option>
						<option value="01" <CFIF thisHour eq 01>selected</CFIF>>01</option>
						<option value="02" <CFIF thisHour eq 02>selected</CFIF>>02</option>
						<option value="03" <CFIF thisHour eq 03>selected</CFIF>>03</option>
						<option value="04" <CFIF thisHour eq 04>selected</CFIF>>04</option>
						<option value="05" <CFIF thisHour eq 05>selected</CFIF>>05</option>
						<option value="06" <CFIF thisHour eq 06>selected</CFIF>>06</option>
						<option value="07" <CFIF thisHour eq 07>selected</CFIF>>07</option>
						<option value="08" <CFIF thisHour eq 08>selected</CFIF>>08</option>
						<option value="09" <CFIF thisHour eq 09>selected</CFIF>>09</option>
						<option value="10" <CFIF thisHour eq 10>selected</CFIF>>10</option>
						<option value="11" <CFIF thisHour eq 11>selected</CFIF>>11</option>
						<option value="12" <CFIF thisHour eq 12>selected</CFIF>>12</option>
						<option value="13" <CFIF thisHour eq 13>selected</CFIF>>13</option>
						<option value="14" <CFIF thisHour eq 14>selected</CFIF>>14</option>
						<option value="15" <CFIF thisHour eq 15>selected</CFIF>>15</option>
						<option value="16" <CFIF thisHour eq 16>selected</CFIF>>16</option>
						<option value="17" <CFIF thisHour eq 17>selected</CFIF>>17</option>
						<option value="18" <CFIF thisHour eq 18>selected</CFIF>>18</option>
						<option value="19" <CFIF thisHour eq 19>selected</CFIF>>19</option>
						<option value="20" <CFIF thisHour eq 20>selected</CFIF>>20</option>
						<option value="21" <CFIF thisHour eq 21>selected</CFIF>>21</option>
						<option value="22" <CFIF thisHour eq 22>selected</CFIF>>22</option>
						<option value="23" <CFIF thisHour eq 23>selected</CFIF>>23</option>
					</select>
					<cfset thisMinute = TimeFormat(dateSent,"MM")>
				MM:<select name="frmMinute">
						<option value="00" <CFIF thisMinute eq 00>selected</CFIF>>00</option>
						<option value="01" <CFIF thisMinute eq 01>selected</CFIF>>01</option>
						<option value="02" <CFIF thisMinute eq 02>selected</CFIF>>02</option>
						<option value="03" <CFIF thisMinute eq 03>selected</CFIF>>03</option>
						<option value="04" <CFIF thisMinute eq 04>selected</CFIF>>04</option>
						<option value="05" <CFIF thisMinute eq 05>selected</CFIF>>05</option>
						<option value="06" <CFIF thisMinute eq 06>selected</CFIF>>06</option>
						<option value="07" <CFIF thisMinute eq 07>selected</CFIF>>07</option>
						<option value="08" <CFIF thisMinute eq 08>selected</CFIF>>08</option>
						<option value="09" <CFIF thisMinute eq 09>selected</CFIF>>09</option>
						<cfset m = "10">
						<cfloop from="10" to="58" index="m">
							<cfset m = m + 1>
							<option value="#m#" <CFIF thisMinute eq m>selected</CFIF>>#m#</option>
						</cfloop>
					</select>			
			</TD>
		    </TR>
		 <!--- AWJR 2011-03-30 P-LEX051 : Switch if a commdetail entity type has profiles then hide type (commReasonID, commStatusID) --->
		<cfsavecontent variable="flagContent_html">
			<!--- AWJR 2011-03-04 Addition of Flags --->
			<tr>
				<td colspan="2" valign="top">
					<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
						<tr>
							<td class="label">
								<CFSET flagMethod = "edit">
								<CFSET entityType = 60>
								<CFSET thisEntity = frmCommDetailID>
								<CFSET frmCountryID = request.relaycurrentuser.countryID>
								<cfinclude template="/flags/allFlag.cfm">
								<input type="hidden" name="frmcommReasonID" value="">
								<input type="hidden" name="frmcommStatusID" value="">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
		</cfsavecontent>
		<!--- AWJR 2011-03-30 P-LEX051 : Switch if a commdetail entity type has profiles then hide type (commReasonID, commStatusID) --->
		<cfif getTopLevelFlags.recordcount>
			#flagContent_html#
			<!--- CASE 428929: PJP: 22/10/2012:  Added in frmCommTypeID as is required when saving --->
			<CF_INPUT type="hidden" name="frmCommTypeID" value="#detail.commTypeID#">
		<cfelse>
		<TR>
		    <TD valign=top class="label">Type of contact</TD>
			<TD><CF_TwoSelectsRelated
					QUERY="getReasonStatus"
					HTMLAFTER1="Status:"
					NAME1="frmcommReasonID"
					NAME2="frmcommStatusID"
					VALUE1="commReasonID"
					VALUE2="commStatusID"
					DISPLAY1="Reason"
					DISPLAY2="Status"
					Selected1="#detail.commReasonID#"
					Selected2="#detail.commStatusID#"
					DEFAULT1="1"
					SIZE1="1"
					SIZE2="1"
					WIDTH1="175"
					WIDTH2="175"
					FORCEWIDTH1="60"
					FORCEWIDTH2="60"
					EMPTYTEXT1="Please select a type"					
					AUTOSELECTFIRST="No"
					MULTIPLE3="No">
					<CF_INPUT type="hidden" name="frmCommTypeID" value="#detail.commTypeID#">
 				<!--- <CF_ThreeSelectsRelated
					QUERY="GetTypeReasonStatus"
					HTMLAFTER1="Reason:"
					HTMLAFTER2="Status:"
					NAME1="Type"
					NAME2="Reason"
					NAME3="Status"
					VALUE1="commTypeID"
					VALUE2="commReasonID"
					VALUE3="commStatusID"
					DISPLAY1="Type"
					DISPLAY2="Reason"
					DISPLAY3="Status"
					DEFAULT1="1"
					SIZE1="1"
					SIZE2="1"
					SIZE3="1"
					WIDTH1="150"
					WIDTH2="150"
					WIDTH3="300"
					FORCEWIDTH1="40"
					FORCEWIDTH2="40"
					FORCEWIDTH3="90"
					EMPTYTEXT1="Please select a type"					
					AUTOSELECTFIRST="No"
					MULTIPLE3="No"> --->
			</TD>
		</TR>
		
		</cfif>
		
	</CFOUTPUT>
<CFELSE>
	<TR><TD COLSPAN="2"><CFOUTPUT>Cannot find a record with id #htmleditformat(frmCommDetailID)#</CFOUTPUT></TD></TR>
</CFIF>
</TABLE>
<CFOUTPUT>
<CF_INPUT TYPE="Hidden" NAME="frmCommDetailID" VALUE="#frmCommDetailId#">
<INPUT TYPE="Hidden" NAME="frmMode" VALUE="Save">
</CFOUTPUT>
</FORM>
<!--- <CFIF Detail.RecordCount GT 0>
	<CFSET frmEntityType ="person">
	<CFSET frmcurrentEntityID ="#detail.personID#">
	<CFINCLUDE TEMPLATE="CommHistory.cfm">
</CFIF> --->




