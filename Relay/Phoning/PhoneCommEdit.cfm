<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods:
4/00  WAB did some mods.  Made sure that result sets which have been selected already are not included in the drop down
2005-09-26		WAb 	altered query on selectiontag.status to handle status 1 or 2  [2 being manually added people]

 --->

<CFSET FormFieldList = "campaignID,Title,Description,Notes,SendDate,Sent,CommStatusID, CommTypeLID, CommFormLID, ProjectRef">
<CFPARAM NAME="CurrentCommSelections" DEFAULT="There are no selections related to this record">

<CFIF isDefined('URL.RecordID')>
	<CFQUERY name="GetRecord" dataSource="#application.SiteDataSource#" maxRows=1>
		SELECT Title,
			Description,
			Notes,
			campaignID,
			SendDate,
			Sent,
			CommStatusID,
			CommTypeLID,
			CommID AS ID_Field
		FROM Communication
		<CFIF isDefined('URL.RecordID')>
		WHERE CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFIF>
	</CFQUERY>

	<CFIF not ListFind( FormFieldList, "CommID" )>
		<CFSET FormFieldList = ListAppend( FormFieldList, "CommID" )>
	</CFIF>

	<CFQUERY NAME="GetCommFlagGroups" datasource="#application.siteDataSource#">
		SELECT      CommFlagGroup.CommID,
					CommFlagGroup.FlagGroupTextID,
					FlagGroup.Name
		FROM        CommFlagGroup, FlagGroup
		WHERE       CommFlagGroup.FlagGroupTextID = FlagGroup.FlagGroupTextID
		AND         CommFlagGroup.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>


	<CFSET Title_Value = '#GetRecord.Title#'>
	<CFSET campaignID_Value = '#GetRecord.campaignID#'>
	<CFSET Description_Value = '#GetRecord.Description#'>
	<CFSET Notes_Value = '#GetRecord.Notes#'>
	<CFSET SendDate_Value = #GetRecord.SendDate#>
	<CFSET Sent_Value = '#GetRecord.Sent#'>
	<CFSET CommStatusID_Value = #GetRecord.CommStatusID#>
	<CFSET CommTypeLIDValue = #GetRecord.CommTypeLID#>


	<!--- Get the selections related to this communication --->
	<CFQUERY NAME="GetCommSelectionsbyCommID" datasource="#application.siteDataSource#">
		SELECT      s.Title, cs.CommID, s.SelectionID,
					(select count(distinct locationID)
						from person p, selectionTag st
						where PersonID = st.entityID
						and st.selectionID = s.SelectionID and st.status <>0) AS NumLocations
		FROM        CommSelection as cs, Selection as s
		WHERE       cs.SelectionID = s.SelectionID
		AND    		cs.CommID =  <cf_queryparam value="#GetRecord.ID_Field#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY  	s.Title
	</CFQUERY>

	<!--- Get Selections for this user --->
	<CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
		SELECT 	s.Title, s.SelectionID, s.Created AS datecreated,
			(SELECT COUNT(DISTINCT p.organisationID)
	            FROM person p INNER join selectionTag st on PersonID = st.entityID
				LEFT OUTER JOIN PhoneAction AS PA ON PA.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND P.organisationID = PA.organisationID
	            WHERE st.selectionID = s.SelectionID and pa.organisationID is null and st.status <> 0) AS NewOrgs,
			(SELECT COUNT(DISTINCT organisationID)
	            FROM person p INNER join selectionTag st on PersonID = st.entityID
	            WHERE st.selectionID = s.SelectionID and st.status <> 0) AS NumOrgs
	      FROM Selection AS s, SelectionGroup AS sg
	     WHERE sg.SelectionID = s.SelectionID
		   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
	  ORDER BY s.Title
	</CFQUERY>


<CFELSE>


		<CFSET Title_Value = ''>
		<CFSET campaignID_Value = 0>
		<CFSET Description_Value = ''>
		<CFSET Notes_Value = ''>
		<CFSET SendDate_Value = ''>
		<CFSET Sent_Value = ''>
		<CFSET CommStatusID_Value = ''>
		<CFSET CommTypeLIDValue = ''>


		<!--- Get Selections for this user --->
		<CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
			SELECT 	s.Title, s.SelectionID, s.Created AS datecreated,
				(SELECT COUNT(DISTINCT organisationID)
		            FROM person p INNER join selectionTag st on PersonID = st.entityID
		            WHERE st.selectionID = s.SelectionID) AS NewOrgs,
				(SELECT COUNT(DISTINCT organisationID)
		            FROM person p INNER join selectionTag st on PersonID = st.entityID
		            WHERE st.selectionID = s.SelectionID) AS NumOrgs
		      FROM Selection AS s, SelectionGroup AS sg
		     WHERE sg.SelectionID = s.SelectionID
			   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
		  ORDER BY s.Title
		</CFQUERY>


		<!--- Get Selections for this user
		<CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
			SELECT 	s.Title, s.SelectionID, s.Created AS datecreated,
							(select count(distinct locationID)
								from person p, selectionTag st
								where PersonID = st.entityID
								and st.selectionID = s.SelectionID) AS NumLocations
		      FROM Selection AS s, SelectionGroup AS sg
		     WHERE sg.SelectionID = s.SelectionID
			   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
		  ORDER BY s.Title
		</CFQUERY> --->


</CFIF>

<CFQUERY NAME="GetPhoningFlagGroups" datasource="#application.siteDataSource#">
	SELECT      FlagGroupTextID, FlagGroupID, ParentFlagGroupID, name
	FROM         dbo.FlagGroup
	WHERE       ParentFlagGroupID = (select FlagGroupID from flagGroup where FlagGroupTextID = 'PhonSys')
	<CFIF isDefined("URL.RecordID") and getCommFlagGroups.recordCount neq 0>
	<!--- if this is an existing record, then leave out all the ones already selected --->
	AND 		flaggroupTextID  not in ( <cf_queryparam value="#getCommFlagGroups.FlagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	</cfif>
</CFQUERY>

<cf_head>
	<cf_title>PhoneComm - Edit Record</cf_title>

<SCRIPT type="text/javascript">
<!--
	function checkForm(){
		var form=window.document.ThisForm;
		var msg = "";
		if (form.CommTypeLID.value==61) {
			form.CommFormLID.value=8;}
		if (form.CommTypeLID.value==62) {
			form.CommFormLID.value=9;}
		form.submit();
	}
//-->
</SCRIPT>


</cf_head>


<cfset subsection = "Edit Outbound Phone Campaign">
<CFINCLUDE TEMPLATE="PhoneCommtophead.cfm">

<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="White">
<CFOUTPUT>
<FORM ACTION="PhoneCommTask.cfm" METHOD="POST" NAME="ThisForm">
<CF_INPUT type="hidden" name="FieldList" value="#FormFieldList#">
<INPUT TYPE="Hidden" NAME="CommFormLID" VALUE="8">
<INPUT TYPE="Hidden" NAME="ProjectRef" VALUE=" ">

<CFIF isDefined('URL.RecordID')>
	<CF_INPUT type="hidden" name="RecordID" value="#URL.RecordID#">
	<CF_INPUT type="hidden" name="CommID" value="#URL.RecordID#">
</CFIF>

	<cfset getCampaigns = application.com.relayCampaigns.getCampaigns(currentValue=campaignID_value)>
	<TR>	<!--- Campaign field --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Campaign:</TD>
	    <TD>
			<SELECT NAME="campaignID">
				<cfloop QUERY="getCampaigns">
					<OPTION VALUE="#campaignID#"#IIF(campaignID IS campaignID_value,DE('SELECTED'),DE(''))#>#htmleditformat(campaignName)#</OPTION>
				</CFloop>
			</SELECT>
		</TD>
	</TR>

	<TR>	<!--- Title field --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Title:</TD>
	    <TD>
			<CF_INPUT TYPE="Text" NAME="Title" VALUE="#Title_Value#" SIZE="30" MAXLENGTH="30">
			<INPUT TYPE="Hidden" NAME="Title_required">
		</TD>
	</TR>

	<TR>	<!--- Name field --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Description:</TD>
	    <TD>
			<CF_INPUT TYPE="Text" NAME="Description" VALUE="#Description_Value#" SIZE="70" MAXLENGTH="80">
			<INPUT TYPE="Hidden" NAME="Description_required">
		</TD>

	</TR>


	<TR>	<!--- Notes field --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label"> Notes: </TD>
	    <TD>
			<TEXTAREA NAME="Notes" COLS="70" ROWS="5" WRAP="VIRTUAL" CLASS="memoText">#Notes_Value#</TEXTAREA>
		</TD>
	</TR>


	<TR>	<!--- SendDate --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label"> Campaign Date: </TD>
	    <TD>
			<CF_INPUT type="text" name="SendDate" value="#SendDate_Value#" maxLength="16">
			(mm/dd/yyyy)
		</TD>
	<INPUT type="hidden" name="SendDate_eurodate">
	</TR>


	<TR>	<!--- Sent field --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label"> Completed: </TD>
	    <TD>

			<INPUT type="checkBox" name="Sent" value = "1" <CFIF Sent_Value is 1>checked</cfif> >
		</TD>
	</TR>

	<TR> <!--- CommType --->
		<!--- Outbound calls are CommTypeLID 61, Inbound calls are CommTypeLID 62 --->
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Call Type</TD>
		<TD>
			<SELECT NAME="CommTypeLID" SIZE="1">
				<OPTION VALUE="61" #IIf(CommTypeLIDValue IS 61, DE(" selected"),DE( ""))#>Outbound call</OPTION>
	   			<OPTION VALUE="62" #IIf(CommTypeLIDValue IS 62, DE(" selected"),DE( ""))#>Inbound call</OPTION>
			</SELECT>
		</TD>
	</TR>

<!--- 	<TR>
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label"> CommStatusID: </TD>
	    <TD>
			<INPUT type="text" name="CommStatusID" value="#CommStatusID_Value#" maxLength="4">
		</TD>
		<INPUT type="hidden" name="CommStatusID_integer">
	</TR>
 ---></CFOUTPUT>


	<CFIF isDefined("URL.RecordID")>
		<CFIF #GetCommFlagGroups.RecordCount# GT 0>
		<!--- The selections currently linked to this  --->
			<TR>
				<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Current call results used by this campaign: </TD>
				<TD>
					<CFOUTPUT QUERY="GetCommFlagGroups">
						#htmleditformat(Name)#<BR>
					</CFOUTPUT>
				</TD>
			</TR>
		</CFIF>
	</CFIF>

	<!--- The Results this campaign could add to this  --->
	<TR>
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Results this campaign could use:
		</TD>
		<TD>
			 <P><SELECT NAME="frmFlagGroupID" SIZE="<CFIF GetPhoningFlagGroups.RecordCount GT 10>10<CFELSE><CFOUTPUT>#GetPhoningFlagGroups.RecordCount#</CFOUTPUT></CFIF>" MULTIPLE>
				<CFOUTPUT QUERY="GetPhoningFlagGroups">
					<OPTION VALUE="#FlagGroupTextID#">#htmleditformat(Name)#
				</CFOUTPUT>
			</SELECT>
		<BR>(Ctrl click to select multiple selections)
		</TD>
	</TR>



	<CFIF isDefined("GetCommSelectionsbyCommID")>
		<CFIF #GetCommSelectionsbyCommID.RecordCount# GT 0>
		<!--- The selections currently linked to this  --->
			<TR>
				<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Selections currently linked to this campaign: </TD>
				<TD>
					<CFOUTPUT QUERY="GetCommSelectionsbyCommID">
						#htmleditformat(Title)# - #htmleditformat(NumLocations)# organisations<BR>
					</CFOUTPUT>
				</TD>
			</TR>
		</CFIF>
	</CFIF>

	<!--- The selections this user could add to this  --->
	<TR>
		<TD WIDTH="150" VALIGN="TOP" CLASS="Label">Highlight Selection(s) to add to the campaign:
		</TD>
		<TD>
			 <P><SELECT NAME="frmSelectID" SIZE="<CFIF getSelections.RecordCount GT 10>10<CFELSE><CFOUTPUT>#getSelections.RecordCount#</CFOUTPUT></CFIF>" MULTIPLE>
				<CFOUTPUT QUERY="getSelections">
					<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #htmleditformat(Title)# - #htmleditformat(NumOrgs)# Organisations - #htmleditformat(NewOrgs)# New Organisations
				</CFOUTPUT>
			</SELECT>
		<BR>(Ctrl click to select multiple selections)
		</TD>
	</TR>
	<tr>
		<td colspan="2" align="center">
			<!--- form buttons --->
			<INPUT onClick="checkForm();" TYPE="Button" NAME="btnEdit_OK" VALUE="    Save    ">
			<!--- <INPUT type="submit" name="btnEdit_Cancel" value="Cancel"> --->
		</td>
	</tr>
</TABLE>
</FORM>


<CFIF isDefined("URL.RecordID")>
	<!--- Count the phone action records related to this communication  --->
	<CFQUERY NAME="CountLastCallResultsbyCommID" datasource="#application.siteDataSource#">
		SELECT      dbo.Flag.Name AS Status, count(*) AS Number, dbo.Flag.OrderingIndex
			FROM         {oj dbo.Flag RIGHT OUTER JOIN dbo.PhoneAction ON dbo.Flag.FlagID = dbo.PhoneAction.LastCallResult }
			WHERE       (dbo.PhoneAction.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			GROUP BY  dbo.Flag.Name, dbo.Flag.OrderingIndex
			ORDER BY  dbo.Flag.OrderingIndex
	</CFQUERY>

	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td class="label" colspan="2">
			Analysis of Current Phone Action Records for this campaign
		</td>
	</tr>
	<tr>
		<Th>Status</Th>
		<Th>Number</Th>
	</tr>

	<CFIF #CountLastCallResultsbyCommID.RecordCount# GT 0>
		<CFOUTPUT QUERY="CountLastCallResultsbyCommID">
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<TD>#htmleditformat(Status)#</TD>
				<TD>#htmleditformat(Number)#</TD>
			</TR>
		</CFOUTPUT>
	<CFELSE>
		<TR>
			<TD colspan="2">
				No Phone action Records currently linked to this record
			</TD>
		</TR>
	</CFIF>
	</TABLE>
</CFIF>


