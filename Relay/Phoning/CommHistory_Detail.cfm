<!--- �Relayware. All Rights Reserved 2014 --->
<!---


Amendment History
Version	Date		By		Description
2.0	2001-09-17		CPS		altered look and feel and added images to make it more user friendly as well as adding edit functionality
2.1	2001-10-17		CPS		Modified queries to make use of new attributes commtypeid, commreasonid and commstatusid
	2002-02-03		SWJ		Modified layout slightly
	2002-03-25		DAM		Modified to display either a commdetail or a comm history see note dated below
	2002-06-11		SWJ		Modified back button to go back
	2005-02-22		WAB		Changes to deal with long feedback - query moved to communication.cfc
	2011-01-13		MS		LID:4314 displaying acive campaign recorded against actions and contacts Line 61 & 68 & 142
	2013-12-03 		WAB 	2013RoadMap2 Item 25 Contact History.  Now store info in commDetailContent.subject and .body rather than in commdetail.feedback
							Record what entity system emails are related to.
--->


<!---
		DAM 2002-03-25

	Supports CommDetail and ComDetailHistory depending on whether url parameters are recieved for either
	CDID = CommDetail or CDHID = ComDetailHistory
--->
<CFIF structKeyExists(url,"CDID")>
	<CFSET Type = "Current">
	<CFSET ID = URL.CDID>
<CFELSEIF structKeyExists(url,"CDHID")>
	<CFSET Type = "History">
	<CFSET ID = URL.CDHID>
<CFELSE>
	This won't work without one of the following URL parameters CDID = CommDetail or CDHID = ComDetailHistory
</CFIF>


<cfparam name="messageID" default=0> <!--- NJH 2013/02/28 - Case 433940 - provide support for messages --->
<cfparam name="personID" default=0>

	<CFIF structKeyExists(URL,"CDID")>
		<!--- gets a commdetail record with correct feedback --->
		<cfif url.cdid neq 0>
			<cfset getMessage = false>
			<cfif messageID eq url.cdid>
				<cfset getMessage = true>
			</cfif>
			<cfset detail = application.com.communications.getCommDetailRecord(commdetailid=id,getMessage=getMessage)>
		<cfelseif messageID neq 0>

			<!--- if a messageID has been passed through, then get the message details --->
			<cfquery name="detail" datasource="#application.siteDataSource#">
				SELECT m.title_defaultTranslation as title,
					p.firstName+' '+p.lastname AS fullName,
				      p.personID,
					  o.OrganisationName as orgName,
				      m.title_defaultTranslation,
					  'Sent' as Feedback,
					  isNull(m.sendDate,m.created) as DateSent,
					  'Sent' as commStatus,
					  'Message' as commType,
					  p.PersonID,
					  0 as CommDetailID,
					  0 as showInAddComm,
					  0 as CommID,
 				      'Phr_Campaign_NoCampaign' as commCampaign,
					l.countryID
					from message m
						left join usergroup u ON u.usergroupid = m.lastupdatedby
						left join (selection s inner join selectionTag st
									on s.selectionID = st.selectionID) on m.sendToEntityID = s.selectionID and m.sendToEntityType = 'selection'
						left join person p on (p.personID = m.sendToEntityID and m.sendToEntityType = 'selection') or (st.entityID = p.personID and m.sendToEntityType = 'selection')
						left join location l on l.locationID = p.locationID
						left join organisation o on o.organisationID = l.organisationID
					where st.status != 0
						and isNull(m.sendDate,m.created) < getDate()
						and p.personID = <cf_queryParam value="#personID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<Cfset frmCountryID = detail.countryID> <!--- had to be set for allFlags.cfm below... --->
		</cfif>
 	<CFELSE>
			<CFQUERY name="Detail" datasource="#application.siteDataSource#">
					SELECT p.firstName+' '+p.lastname AS fullName,
				      p.personID,
					  (SELECT OrganisationName
					      FROM organisation o
			        INNER JOIN location l ON l.organisationid = o.organisationid
			        	 WHERE l.locationid = cd.locationid) AS orgName,
				      comm.Title,
					  CD.Feedback,
					  CD.DateSent,
					  cds.name as commStatus,
					  cdt.name as commType,
					  CD.PersonID,
					  CD.CommDetailID,
					  cdt.showInAddComm,
					  CDd.CommID,
 				      dbo.Campaign.campaignName as commCampaign
				  FROM commdetailhistory cd
				INNER JOIN commdetail cdd ON cdd.commdetailid = cd.commdetailid
			LEFT OUTER JOIN Person p ON CD.PersonID = p.PersonID
			LEFT OUTER JOIN Communication comm ON CDd.CommID = comm.CommID
			     INNER JOIN commDetailStatus cds ON cds.commStatusId = cd.commStatusId
			     INNER JOIN commDetailType cdt ON cdt.commTypeId = cd.commTypeId
				 LEFT OUTER JOIN dbo.Campaign ON cdd.CommCampaignID = dbo.Campaign.campaignID
				WHERE CD.commdetailhistoryID =  <cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>


	</cfif>



<cfset relatedEntities = application.com.relayEntity.getRelatedEntities(entityType = "commdetail", entityID = Detail.commdetailid)>

<cf_title>CommHistory - Detail</cf_title>

<SCRIPT type="text/javascript">
<!--

	function viewContactHistory(){
		var form = document.contactHistoryForm;
		form.submit();

	}

	function editCommDetail(cid) {
		var form = document.mainForm;
		form.frmCommDetailID.value = cid;
		form.submit();

	}

//-->
</SCRIPT>

<CF_RelayNavMenu pageTitle="Contact details for #htmleditformat(Trim(detail.fullname))#">
	<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="javascript:history.go(-1);">
	<cfif Type eq "Current" and detail.showInAddComm eq "1">
		<CF_RelayNavMenuItem MenuItemText="Edit" CFTemplate="javascript:editCommDetail(#Detail.CommDetailID#);">
	</cfif>
</CF_RelayNavMenu>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<CFIF Detail.RecordCount GT 0>
	<CFOUTPUT query="Detail">

		<TR>
		    <TD valign=top class="label">Contact:</TD>
		    <TD valign=top>#htmleditformat(fullName)#</TD>
	    </TR>

		<TR>
		    <TD valign=top class="label">Company:</TD>
		    <TD valign=top>#htmleditformat(orgName)#</TD>
	    </TR>

		<TR>
		    <TD valign=top class="label">Subject:</TD>
		    <TD valign=top>#htmleditformat(title)#</TD>
	    </TR>

		<cfif body is not "">

			<TR>
			    <TD valign=top class="label">Body:</TD>
				<!--- WAB 2013-12-17 A bit of a hack to allow style attributes. While still sanitising.  Not exactly sure why <style is not allowed, are they dangerous  --->
			    <TD valign=top>
					<!---
					WAB 2013-12-17
					If it is an HTML document (like and email, then put in an IFrame)
					Note the special hack to allow the style tag to get through the sanitizer
					--->
				 	<CFIF refindNoCase("(<HTML|<STYLE)",body)>


						<iframe id="htmlcontent"  style="border:0"></iframe>
						<script>
							populateIframe = function(frameID,content) {
								iFrame = $(frameID)
								doc = iFrame.contentDocument
						        if (doc == undefined || doc == null)
						            doc = iFrame.contentWindow.document;

								doc.open()
								doc.write (content)
								doc.close()

								iFrame.style.width = Math.max (doc.body["scrollWidth"], doc.documentElement["scrollWidth"], doc.body["offsetWidth"], doc.documentElement["offsetWidth"]) + 16 + 'px'
								iFrame.style.height = Math.max (doc.body["scrollHeight"], doc.documentElement["scrollHeight"], doc.body["offsetHeight"], doc.documentElement["offsetHeight"]) + 16 + 'px'

							}

								populateIframe ('htmlcontent','#jsstringformat(replaceNoCase(application.com.security.sanitiseHTML(replacenocase(body,"<style","{{style","ALL")),"{{style","<style","ALL"))#')
						</script>

					<CFELSE>

					    #application.com.security.sanitiseHTML(body)#

					</CFIF>
				 </TD>
		    </TR>
	    </cfif>

		<TR>
		    <TD valign=top class="label">Type:</TD>
		    <TD valign=top>#htmleditformat(commType)#</TD>
	    </TR>

		<TR>
		<!--- LID:4314 MS 13JAN2011 Displaying campaigns registered against contact --->
		<CFIF application.com.settings.getSetting("campaigns.displayCampaigns")>
		<TR>
		    <TD valign=top class="label">Campaign:</TD>
		    <TD valign=top>#htmleditformat(commCampaign)#</TD>
		    </TR>
		<TR>
		</CFIF>
		    <TD valign=top class="label">Feedback:</TD>
		    <TD valign=top>#paragraphFormat(htmleditformat(feedBack))#</TD>
		    </TR>
		<TR>
		    <TD valign=top class="label">Contact Date:</TD>
		    <TD valign=top>#dateFormat(dateSent,"dd-mmm-yy")#</TD>
		    </TR>
		<TR>
		    <TD valign=top class="label">Outcome:</TD>
		    <TD valign=top>#htmleditformat(commStatus)#</TD>
	    </TR>

		<cfif relatedEntities.recordCount>
		<TR>
		    <TD valign=top class="label">Related To</TD>
		    <TD valign=top>
			    <cfloop query="relatedEntities">
				#entityName# #entityID# - #name#<BR>
				</cfloop>
			</TD>
	    </TR>
		</cfif>

		<!--- AWJR 2011-03-04 Addition of Flags --->
		<tr>
			<td colspan="9" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
					<tr>
						<td class="label">
							<CFSET flagMethod = "view">
							<CFSET entityType = 60>
							<CFSET thisEntity = #ID#>
							<cfinclude template="/flags/allFlag.cfm">
						</td>
					</tr>
				</table>
			</td>
		</tr>



<!--- 		<TR>
			<TD>
				<A href="javascript:history.go(-1);">
					<IMG src="/images/buttons/c_back_e.gif" alt="" border="0">
				</A>
			</TD>
		</TR> --->
	</CFOUTPUT>
<CFELSE>
	<TR><TD COLSPAN="2"><CFOUTPUT>Cannot find a record with id #htmleditformat(ID)#</CFOUTPUT></TD></TR>
</CFIF>
</TABLE>

<FORM METHOD="POST" ACTION="CommHistory_DetailEdit.cfm" NAME="mainForm">
<INPUT TYPE="Hidden" NAME="frmCommDetailID">
</FORM>

<CFOUTPUT>
<FORM ACTION="commHistory.cfm" METHOD="post" NAME="contactHistoryForm" TARGET="Detail">
	<CF_INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="#detail.personId#">
	<INPUT TYPE="hidden" NAME="frmEntityType" VALUE="person">
</FORM>
</CFOUTPUT>