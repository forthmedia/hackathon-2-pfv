<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		AddCallRecord.cfm
Author:			SWJ
Date created:	1999

	Objective - To create a contact history record

	Rationale - This was originally built simply to record call outcomes however it has developed
				to record all of the various contact history and next action records

	Syntax	  -	it can be used in the viewer or it requires frmentityType AND frmcurrentEntityID
				to be passed for it to work.  This combination is then used to work out
				how to get the current commHistory and what to show.

	Parameters 	frmentityType the current entity type.  Supports person, location
				and organisation entity types
				frmcurrentEntityID the current entityID


Date Tested:
Tested by:

Amendment History:

(DD-MMM-YYYY)	Initials 	What was changed
2001-08-21		SWJ			I changed it to work with frmentityType and frmcurrentEntityID
2001-10-22		CPS			Modified queries to make use of new attributes commtypeid, commreasonid and commstatusid
2002-02-03		SWJ			Changed form so that it is simpler to use and split out
							add next action to another form
2002-09-10		SWJ			Modified getReasonStatus query to use showInAddComm
2005-02-22		WAB			Changes to deal with long feedback - code moved to cf_addCommDetail.cfm
2005-02-28			WAB			Dates changed to use relaydatefield and the update altered to expect this
2006-03-03 		SWJ 		added the ability to update perProfileSummary screen from here
2006-06-12		WAB			fixed a bug introduced by above change - perProfileSummary screen shoudl only load if entityType is person
2008-05-06		SWJ			modified the order by in getPeople
2008-07-10		GCC			Converted to cfform for use with cfinput scrren text flags. Dealt with some bad cfoutput stacking at the same time.
2009/06/30		NJH			P-FNL069 Removed include of setparameters.cfm
2011-01-13		MS			LID:4314 Dropdown to record active campaigns

2011/05/03		PPB			populate the WhoMadeContact dropdown using the new way of populating validvalues with a cfc func
2011/10/05		WAB			LID 7936 Move form outside of table so that calendar popup works
2012/02/01		RMB			Changes and Fixs to flag(S) display

Enhancement still to do:

1.  work generically with organisations and locations

--->

<CFIF not isdefined("frmcurrentEntityID") and frmcurrentEntityID gt 0>
	<TABLE BORDER="2" CELLPADDING="3" BGCOLOR="White">
	<TR>
		<TD>AddCallRecord.cfm requires frmcurrentEntityID.  Please contact
			technical support for assistance telling them you have got this
			message and have come from <CFOUTPUT>#htmleditformat(HTTP_REFERER)#</CFOUTPUT>
			<CFLOG TEXT="AddCallRecord.cfm missing frmcurrentEntityID. User came from #HTTP_REFERER#"
			LOG="APPLICATION" FILE="RelayError"
			TYPE="Error" DATE="yes" TIME="yes" APPLICATION="yes">
		</TD>
	</TR>
	</TABLE>
	<CF_ABORT>
</cfif>

<CFIF not isdefined("frmentityType") and listfindnocase("organisation,person,location",frmentityType) neq 0>
	<TABLE BORDER="2" CELLPADDING="3" BGCOLOR="White">
	<TR>
		<TD>AddCallRecord.cfm requires frmentityType.  Please contact
			technical support for assistance telling them you have got this
			message and have come from <CFOUTPUT>#htmleditformat(HTTP_REFERER)#</CFOUTPUT>
			<CFLOG TEXT="AddCallRecord.cfm missing frmentityType. User came from #HTTP_REFERER#"
			LOG="APPLICATION" FILE="RelayError"
			TYPE="Error" DATE="yes" TIME="yes" APPLICATION="yes">
		</TD>
	</TR>
	</TABLE>
	<CF_ABORT>
</cfif>


<CFIF isdefined("frmcurrentEntityID") and frmEntityType eq "organisation">
	<CFSET frmOrganisationID = frmcurrentEntityID>
</cfif>
<CFIF isdefined("frmcurrentEntityID") and frmEntityType eq "location">
	<CFSET frmLocationID = frmcurrentEntityID>
</cfif>

<CFIF isdefined("frmentityType")>

	<CFQUERY NAME="getEntityData" datasource="#application.siteDataSource#">
		<CFIF frmentityType IS "Location">
		Select SiteName AS Name, organisationID FROM Location
			WHERE locationId =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<CFELSEIF frmEntityType eq "organisation">
		Select organisationName as Name, organisationID from organisation
				where organisationID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelseif frmentityType IS "Person">
			Select organisationName as Name, organisationID from organisation
			WHERE organisationID = (Select organisationID from person
				where personid =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfif>
	</CFQUERY>

	<cfset entityName = getEntityData.name>
</cfif>

<!--- decipher any parameters passed in frmGlobalParameters --->
<!--- NJH 2009/06/30 P-FNL069 - remove setParameters.cfm
<CFINCLUDE template="..\screen\setParameters.cfm"> --->

<CFQUERY NAME="GetPeople" datasource="#application.siteDataSource#">
	Select firstname+' '+Lastname AS Name, PersonID
	FROM Person
	<CFIF frmentityType IS "Location">
		WHERE locationId =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	<CFELSEIF frmEntityType eq "organisation">
		WHERE organisationID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	<cfelseif frmentityType IS "Person">
		WHERE organisationID = (select organisationID from person WHERE personid =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfif>
	order by firstName
</CFQUERY>

<cfif frmentityType is "Person">
<!--- 2006-03-03 SWJ added the ability to update perProfileSummary screen from here --->
<CFQUERY NAME="GetThisPerson" datasource="#application.siteDataSource#">
	Select * FROM Person WHERE personid =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>
<cfset entityName = "#GetThisPerson.firstName# #GetThisPerson.lastName#">

<cf_getrecord entitytype="person" entityid = "#GetThisPerson.PersonID#" locationid = "#GetThisPerson.LocationID#" getparents>
</cfif>


<CFIF isdefined("campaignCommID")>
	<CFQUERY NAME="GetDefaultComm" datasource="#application.siteDataSource#">
	SELECT      CommTitle, commid
	FROM         PhoneFlagRecords
	Where commID =  <cf_queryparam value="#campaignCommID#" CFSQLTYPE="CF_SQL_INTEGER" >

	</CFQUERY>
</cfif>

<CFQUERY NAME="getActionTypes" datasource="#application.siteDataSource#">
	select actionTypeID, actionType from actionType
</CFQUERY>

<CFQUERY NAME="getReasonStatus" datasource="#application.siteDataSource#">
	SELECT cdtrs.commTypeId, cdt.name as Type, cdtrs.commReasonId, cdr.name as Reason, cdtrs.commStatusId, cds.name as Status
		  FROM commDetailTypeReasonStatus cdtrs
	INNER JOIN commDetailType cdt ON cdt.commTypeId = cdtrs.commTypeId
	INNER JOIN commDetailReason cdr ON cdr.commReasonId = cdtrs.commReasonId
	INNER JOIN commDetailStatus cds ON cds.commStatusId = cdtrs.commStatusId
	where cdt.showInAddComm = 1
</CFQUERY>

<!--- 2011/05/03 PPB START - done for Lexmark to change the persons in the Whomadethecontact list in Valid Values --->
<CFIF IsDefined("frmentityType") and frmentityType eq "person" and IsDefined("frmCurrentEntityID") and frmCurrentEntityID neq 0>
	<CFSET countryIdOfPersonToContact = application.com.relayPLO.getPersonDetails(frmCurrentEntityID).countryID>
<cfelse>
	<CFSET countryIdOfPersonToContact = 0>
</CFIF>

<cfset frmAccountmngrID_validValueQuery = application.com.relayForms.getValidValues (fieldname = 'callrecord.accountmngrID',countryID = countryIdOfPersonToContact) >
<!--- 2011/05/03 PPB END --->

<cf_title>Add Contact Record</cf_title>

<cf_RelayNavMenu pageTitle="Phr_Sys_AddContactRecordfor <I>#entityName#</I>" thisDir="phoning">
	<cf_RelayNavMenuItem MenuItemText="Phr_Sys_Save" CFTemplate="javaScript:jsSubmit()">
</cf_RelayNavMenu>

<CFFORM action="/phoning/AddCallRecordTask.cfm" method="post" NAME="mainForm">

<!--- AWJR 2011-03-04 Addition of Flags --->
<!--- RMB 2012/02/01 Moved down to be inside cfform as flags may generate a cfinput ect... --->
<cfsavecontent variable="flagContent_html">
<tr>
	<td colspan="2" valign="top">
		<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
			<tr>
				<td class="label">
					<CFSET flagMethod = "edit">
					<CFSET countryid = 0>
					<CFSET entityType = 60>
					<CFSET THISFORMNAME  = "mainForm">
					<CFSET thisEntity = "New">
					<CFSET frmCountryID = request.relaycurrentuser.countryID>
					<cfinclude template="/flags/allFlag.cfm">
				</td>
			</tr>
		</table>
	</td>
</tr>
<cfoutput>
<input type="hidden" name="frmTableList" value="CommDetail">
<input type="hidden" name="frmCommDetailFieldList" value="">
<CF_INPUT type="hidden" name="frmCommDetailFlagList" value="#flagList#">
<input type="hidden" name="frmCommDetailIdList" value="New">
</cfoutput>
</cfsavecontent>
								<!--- AWJR 04/03/11 Addition of Flags --->
								<!--- CASE: 430444: PJP 18/09/2012: Moved some code to here --->
                                <cfset showTypeOfContact = application.com.settings.getsetting('contactHistory.showTypeOfContactWithFlags')>

                                <cfif showTypeOfContact EQ ''>
                                                <cfset showTypeOfContact = 0>
                                </cfif>
<!--- RMB 2012/02/01 Moved down as the above cfsavecontent needs to be set first... --->
	<SCRIPT type="text/javascript">
	<!--

	function jsSubmit() {
	//-----------------
	var form = document.mainForm;

		msg = ''
		//alert(form.PersonID.value);
		//alert(form.PersonID[form.PersonID.selectedIndex].value);
		if (form.PersonID.value == '' || form.PersonID[form.PersonID.selectedIndex].value == '') {
			msg = msg + 'You must specify the person or people the contact was with before you can save.\n'
		}
		<!--- CASE:430444: PJP 18/09/2012: Added in Check to make sure popup error also works for added in two selects related --->
       <cfif (getTopLevelFlags.recordcount eq 0) or (getTopLevelFlags.recordcount GT 0 AND showTypeOfContact)>
		if (form.frmcommReasonID[form.frmcommReasonID.selectedIndex].value == '') {
			msg = msg + 'You must specify the type of contact before you can save.\n'
		}
		if (form.frmcommStatusID[form.frmcommStatusID.selectedIndex].value == '') {
			msg = msg + 'You must specify the type of contact before you can save.\n'
		}
		</cfif>
		if (msg != '') {
			alert (msg)
		}else {
			form.submit();

		}


	}


	// -->
	</SCRIPT>

<div CLASS="phoningForm">

	<cfif isDefined("message") and message neq "">
		<!--- 2011-03-18 AWJR Ticket 4939 Added message handler. --->
    <div class="row">
      <div class="col-sm-3 col-sx-4">
        <p class="message">
          <cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput>
        </p>        
      </div>
    </div>
	</cfif>
	
  <CFOUTPUT>
		<CF_INPUT TYPE="hidden" NAME="frmCurrentEntityID" VALUE="#frmCurrentEntityID#">
		<CF_INPUT TYPE="hidden" NAME="frmentityType" VALUE="#frmentityType#">
		<CFIF isdefined("campaignCommID")>
			<CF_INPUT TYPE="HIDDEN" NAME="campaignCommID" VALUE="#campaignCommID#">
		</cfif>
	</CFOUTPUT>

  <div class="row">
    <div class="col-sm-3 col-sx-4">
      <p class="label">Phr_Sys_Personcontactwaswith</p>
    </div>
    <div class="col-sm-9 col-sx-8">
      <select name="PersonID" size="5" class="form-control" multiple>
        <option value="">Unspecified</option>
        <CFOUTPUT QUERY="GetPeople">
          <OPTION VALUE="#PersonID#" <CFIF IsDefined("frmentityType") and frmentityType eq "person" and IsDefined("frmCurrentEntityID") and frmCurrentEntityID neq 0><CFIF frmCurrentEntityID EQ personid>SELECTED</CFIF></CFIF>>#htmleditformat(Name)#</OPTION>
        </CFOUTPUT>
      </select>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3 col-sx-4">
      <p class="label">Phr_Sys_Dateandtimeofcontact</p>
    </div>
    <div class="col-sm-9 col-sx-8">
      <cfoutput>
        <cf_relaydatefield currentvalue = "#NOW()#" fieldname = "txtDate" thisformname="mainForm" helpText="" anchorname="x">
        <div class="input-group">
          <cfset thisHour = TimeFormat(now(),"HH")>
          <cfset thisMinute = TimeFormat(now(),"MM")>
          <cf_relaytimefield  hourfieldname = "frmHour" minutefieldname = "frmMinute" currenthour = "#thisHour#" currentminute = "#thisMinute#" class="form-control">
          <!--- <input type="Text" name="frmDateSent_time" value="#TimeFormat(now(),"HH:MM")#" message="The value of Next Action Time must be in the form HH:MM" validate="time" required="No" size="6"> --->
        </div>
      </cfoutput>
      <!-- <CFOUTPUT><INPUT TYPE="text" NAME="DateSent" VALUE="#LSDateFormat(now())# #LSTimeFormat(now())#"></CFOUTPUT> -->
    </div>
    <!--- <INPUT type="hidden" name="DateSent_time"> --->
  </div>

  <div class="row">
    <div class="col-sm-3 col-sx-4">
      <p class="label">Phr_Sys_Whomadethecontact</p>
    </div>
    <div class="col-sm-9 col-sx-8">
      <!-- <cf_displayValidValues validFieldName = "callrecord.accountmngrID" formFieldName =  "frmAccountmngrID"
          keepOrig = "false" currentValue = "#request.relayCurrentUser.PersonID#"> -->
      <!--- 2011/05/03 PPB START --->
      <cfoutput>#application.com.relayForms.validValueDisplay (query = frmAccountmngrID_validValueQuery,name="frmAccountmngrID",selected=request.relayCurrentUser.PersonID, showNull=true)#</cfoutput>
      <!--- 2011/05/03 PPB END --->
      <cfoutput>
        <CF_INPUT TYPE="Hidden" NAME="LastUpdatedBy" VALUE="#request.relaycurrentuser.PersonID#">
      </cfoutput>
    </div>
    <!--- CASE: 430444: PJP 18/09/2012: Moved some code to above --->
  </div>

  <cfif getTopLevelFlags.recordcount GT 0 AND showTypeOfContact>
    <cfoutput>#flagContent_html#</cfoutput>
    <div class="row">
      <div class="col-sm-3 col-sx-4">
        <p class="label">Phr_Sys_Typeofcontact</p>
      </div>
      <div class="col-sm-9 col-sx-8">
        <CF_TwoSelectsRelated QUERY="GetReasonStatus" HTMLAFTER1="Status:" NAME1="frmcommReasonID"
          NAME2="frmcommStatusID" VALUE1="commReasonID" VALUE2="commStatusID" DISPLAY1="Reason"
          DISPLAY2="Status" DEFAULT1="1" SIZE1="1" SIZE2="1" WIDTH1="175" WIDTH2="175" FORCEWIDTH1="60"
          FORCEWIDTH2="60" EMPTYTEXT1="Please select a type" AUTOSELECTFIRST="No" MULTIPLE3="No">
      </div>
    </div>
  <cfelseif getTopLevelFlags.recordcount GT 0>
    <cfoutput>#flagContent_html#</cfoutput>
    <input type="hidden" name="frmcommReasonID" value="">
    <input type="hidden" name="frmcommStatusID" value="">
  <cfelse>
    <div class="row">
      <div class="col-sm-3 col-sx-4">
        <p class="label">Phr_Sys_Typeofcontact</p>
      </div>
      <div class="col-sm-9 col-sx-8">
        <CF_TwoSelectsRelated QUERY="GetReasonStatus" HTMLAFTER1="Status:" NAME1="frmcommReasonID"
          NAME2="frmcommStatusID" VALUE1="commReasonID" VALUE2="commStatusID" DISPLAY1="Reason"
          DISPLAY2="Status" DEFAULT1="1" SIZE1="1" SIZE2="1" WIDTH1="175" WIDTH2="175" FORCEWIDTH1="60"
          FORCEWIDTH2="60" EMPTYTEXT1="Please select a type" AUTOSELECTFIRST="No" MULTIPLE3="No">        
      </div>
    </div>
  </cfif>

	<!--- LID:4314 MS 13JAN2011 Dropdown to record active campaigns --->
	<CFIF application.com.settings.getSetting("campaigns.displayCampaigns")>
		<cfset getActiveCampaigns = application.com.relayCampaigns.GetCampaigns(currentValue=structKeyExists(session,"campaignID")?session.campaignID:'')>
    <div class="row">
      <div class="col-sm-3 col-sx-4">
        <p class="label">Campaign</p>
      </div>
      <div class="col-sm-9 col-sx-8">
       <select name="activeCampaigns" class="form-control">
          <OPTION VALUE=0 <cfif not structKeyExists(session,"campaignID")>selected="true"</cfif>>Please select a campaign</OPTION>
          <cfoutput query="getActiveCampaigns">
            <OPTION VALUE=#campaignId# <cfif structKeyExists(session,"campaignID") and session.campaignID eq getActiveCampaigns.campaignId>selected="true"</cfif>>#htmleditformat(campaignName)#</OPTION>
          </cfoutput>
        </select>        
      </div>
    </div>
	<cfelse>
	  <input  type="hidden" name="activeCampaigns" value=0>
	</cfif>

  <div class="row">
    <div class="col-sm-3 col-sx-4">
      <cf_ascreen formName = "mainForm">
        <cfif NOT ISDefined("CommDetailID")>
          <cfset CommDetailID = "NEW">
        </cfif>
        <cf_ascreenitem screenid="RelatedProducts" method="edit" CommDetailID="#CommDetailID#">
      </cf_ascreen>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3 col-sx-4">
      <p class="label">Phr_Sys_Notes</p>
    </div>
    <div class="col-sm-9 col-sx-8">
      <textarea class="form-control" NAME="Feedback" WRAP="VIRTUAL" rows="5"></textarea>
    </div>
  </div>

  <!--- 2006-03-03 SWJ added the ability to update perProfileSummary screen from here --->
  <!-- GCC 2011/10/04 removed as deemed an annoyance by customers -->
  <!--- <cfif frmentityType is "Person">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-2">
        <cf_ascreen formName = "mainForm">
          <cf_ascreenitem screenid="perProfileSummary" method="edit" person="#GetThisPerson#"
           location="#location#" organisation="#organisation#">
        </cf_ascreen>
      </div>
    </div>
  </cfif> --->

</div> <!-- END OF PHONING DIV -->

</CFFORM>
