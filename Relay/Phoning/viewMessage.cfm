<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		viewMessage.cfm
Author:			NJH
Date started:	10-01-2014

Description:	View message content

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<div id="viewMessageContainer">
<cfquery name="content">
	select text_defaultTranslation from message where messageID=<cf_queryparam value="#url.messageID#" cfsqltype="cf_sql_integer">
</cfquery>

<cfoutput>
	#content.text_defaultTranslation#
</cfoutput>
</div>