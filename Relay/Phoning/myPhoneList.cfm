<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		myAccounts.cfm	
Author:			SWJ
Date created:	7 Aug 2000

Description:	This code is a wrapper which calls the orglist.cfm screen 
				and passes one variable frmAccount

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-04-04		WAB				Changed so that doesn't CFABORT if no permissions.  This allows it to be included in the homepage without problems
09-Jan-2004			KAP			Correction to paging and alphabetical filter
2012-10-04	WAB		Case 430963 Remove Excel Header 

--->

<cfif isDefined("FORM.frmSelectionID")>
	<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
	<cfparam name="openAsExcel" type="boolean" default="false">
	
		<cf_head>
			<cf_title>My Accounts</cf_title>
		</cf_head>
		
		
	<cfparam name="sortOrder" default="account">
	<cfparam name="numRowsPerPage" default="200">
	<cfparam name="startRow" default="1">
	
	<cfparam name="checkBoxFilterName" type="string" default="">
	<cfparam name="checkBoxFilterLabel" type="string" default="">
	
	<cfparam name="alphabeticalIndexColumn" type="string" default="account">
	<cfparam name="alphabeticalIndexColumnUnaliased" type="string" default="o.organisationName">
	<!--- <cfparam name="myAccountFilterSelectFieldList" type="string" default="Account_Type">
	<cfparam name="myAccountflagGroupTextIDToShow" type="string" default="mainOrgType"> --->
	
	<!--- this switches on role based scoping of MyAccounts records only and stops users with 
		leadManagerTask(1) see anything but what they own.  It is controlled by adding
		roleScopeOpportunityRecords to the opportunityINI.cfm file --->
	<!--- <cfparam name="roleScopeMyAccounts" default="yes"> --->
	<cfset roleScopeMyAccounts = application.com.settings.getSetting("accounts.roleScopeMyAccounts")>
	<cfparam name="frmAccountMngrID" default="0">
	<cfif roleScopeMyAccounts 
		and application.com.login.checkInternalPermissions("dataManagerTask","Level2")>
			<cfset frmAccountMngrID = request.relayCurrentUser.personid>
	</cfif>
	
	<CFQUERY NAME="getAccounts" DATASOURCE="#application.siteDataSource#">
		SELECT DISTINCT o.organisationName as account, 
				left(organisationName,12) as shortName,
				o.OrganisationID, 
				LEFT( UPPER( o.organisationName ), 1 ) as alphabeticalIndex,
			   o.LastUpdated as last_updated,
			   c.ISOCode as country, 
			   (select max(dateSent) from commDetail cd
				   INNER JOIN location l ON cd.LocationID = l.locationID
				   where l.OrganisationID = o.OrganisationID) as last_contact_date
		  FROM organisation o 
		  	INNER JOIN Country c ON o.CountryID = c.CountryID
			INNER JOIN person p ON p.organisationID = o.OrganisationID
			INNER JOIN selectionTag st ON st.entityID = p.personID
		WHERE o.CountryID IN (select countryID 
			from rights where usergroupID = #request.relayCurrentUser.userGroupID#)
		AND st.SelectionID =  <cf_queryparam value="#form.frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		AND st.status=1
	
	</cfquery>
	<!--- 
	<cfdump var="#myObject.qUserAccounts#">
	 --->
	 
	 <!--- <cfinclude template="MyAccountFunctions.cfm"> --->
	 
	<CF_tableFromQueryObject 
		queryObject="#getAccounts#"
		sortOrder = "#sortOrder#"
		startRow = "#startRow#"
		numRowsPerPage="100"
		
		keyColumnList="account"
		keyColumnURLList="/data/dataFrame.cfm?frmSiteName="
		keyColumnKeyList="shortName"
		<!--- keyColumnOpenInWindowList="true" --->
		
		dateFormat="last_contact_date,last_updated"
		<!--- FilterSelectFieldList="#myAccountFilterSelectFieldList#"
		alphabeticalIndexColumn="#alphabeticalIndexColumn#" --->
		hideTheseColumns="OrganisationID,shortName"
		allowColumnSorting="no"
		
		useInclude="false"
		
		<!--- rowIdentityColumnName="OrganisationID"
		functionListQuery="#comTableFunction.qFunctionList#" --->
	>
	
	<!--- <cfoutput>
	<FORM ACTION="/data/dataFrame.cfm" METHOD="post" NAME="searchForm" TARGET="main">
		<INPUT type="hidden" NAME="frmSiteName" value="none">
	</FORM>
	</cfoutput> --->	
<cfelse>

<CFQUERY NAME="getSelectionsowned" datasource="#application.siteDataSource#">
	SELECT s.SelectionID, s.Title, 'current' AS owner, s.created AS datecreated
	FROM Selection AS s, SelectionGroup AS sg
	WHERE s.SelectionID = sg.SelectionID
	AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
	AND s.CreatedBy = #request.relayCurrentUser.usergroupid#
	
		UNION 
	
	SELECT s.SelectionID, s.Title AS title, p.FirstName + ' ' + p.LastName AS owner, s.created AS datecreated
	FROM Selection AS s, SelectionGroup AS sg, UserGroup AS ug, Person AS p
	WHERE s.SelectionID = sg.SelectionID
	AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
	AND s.CreatedBy <> #request.relayCurrentUser.usergroupid#
	AND ug.UserGroupID = s.CreatedBy
	AND ug.PersonID = p.PersonID
  		ORDER BY Title
</CFQUERY>

	<p>Please choose the selection to view:</p>
	<form action="" method="post" name="selectionForm" id="selectionForm">
		<SELECT NAME="frmSelectionID">
			<OPTION VALUE="0"> 
			<CFOUTPUT QUERY="getSelectionsowned">
				<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #HTMLEditFormat(Title)#
			</CFOUTPUT>
		</SELECT>
		<input type="submit" value="Go">
	</form>
</cfif>




