<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods
		WAB  070200    Modified insert query
		SWJ	 2001-10-02  Added organisationID to the populate query
		2005-09-26		WAb 	altered query on selectiontag.status to handle status 1 or 2  [2 being manually added people]

 --->

<CFPARAM NAME="CommiD" TYPE="numeric" DEFAULT="3390">

<!--- First check that this CommID is a valid one --->
<CFQUERY NAME="CheckCommIDIsPhoneType" datasource="#application.siteDataSource#">
	SELECT      dbo.Communication.Title, dbo.Communication.CommID
	FROM         dbo.Communication, dbo.CommTypePhone 
	WHERE       dbo.Communication.CommTypeLID = dbo.CommTypePhone.LookupID AND
	                   (dbo.Communication.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</CFQUERY>
<!--- <CFOUTPUT>CommID id is phoneType #CheckCommIDIsPhoneType.RecordCount#</CFOUTPUT> --->

<CFIF CheckCommIDIsPhoneType.RecordCount IS 0>
	<SCRIPT type="text/javascript">
	alert ("<CFOUTPUT>#jsStringFormat(CheckCommIDIsPhoneType.Title)#</CFOUTPUT> is not an outbound campaign")
	</SCRIPT>
	<CF_ABORT>
</CFIF>

<!--- Next get the selections linked to this commid --->
<CFQUERY NAME="GetCommSelections" datasource="#application.siteDataSource#">
SELECT      dbo.CommSelection.SelectionID, dbo.Communication.CommID, dbo.Communication.Title
FROM         dbo.Communication, dbo.CommSelection 
WHERE       dbo.CommSelection.CommID = dbo.Communication.CommID AND
                   (dbo.Communication.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</CFQUERY>

<!--- <CFOUTPUT>There are #GetCommSelections.RecordCount# selection(s)</CFOUTPUT> --->
<!--- If we found some selections for this record --->
<CFIF GetCommSelections.RecordCount GT 0>
	<CFQUERY NAME="GetNotPhonedFlagID" datasource="#application.siteDataSource#">
	SELECT      FlagTextID, FlagID
	FROM         dbo.Flag 
	WHERE       (FlagTextID = 'Untried')
	</CFQUERY>
<!--- Doing it --->
	<!--- nEED TO REMOVE DUPLICATES IN THE STATEMENT BELOW --->
	
	<!--- Insert the records into phone actions for these selections --->
	<!--- was a problem with this query, single location can be in multiple selections and this breaks the constraints
		use a loop so that only one selection done at a time. --->	
	<CFLOOP index="thisSelection" list="#ValueList(GetCommSelections.SelectionID)#" >
	
		 <CFQUERY NAME="InsertPhoneActions" datasource="#application.siteDataSource#">
			INSERT INTO PhoneAction (SelectionID, organisationID, LocationID, CommID, LastCallResult )
				SELECT SelectionTag.SelectionID, 
				 Person.organisationID, max(Person.LocationID), <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" >, 0 
				 FROM SelectionTag INNER JOIN
				 Person ON SelectionTag.EntityID = Person.PersonID 
				 LEFT OUTER JOIN phoneaction ON phoneaction.organisationid = Person.organisationID
				 WHERE SelectionTag.selectionID  in ( <cf_queryparam value="#thisSelection#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				 AND Person.Active = 1 
				 AND selectionTag.status <> 0
				 AND phoneaction.organisationid is null
				 group by SelectionTag.SelectionID, Person.organisationID</CFQUERY>
	
	</cfloop>

	<CFQUERY NAME="updateOrgIDs" datasource="#application.siteDataSource#">
	update phoneAction 
	set organisationID = l.organisationID from
	location l inner join phoneAction pa
	on pa.locationID=l.locationID
	where pa.organisationID is null
	</CFQUERY>
</CFIF>



