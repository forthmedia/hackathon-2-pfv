<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET FormFieldList = "Title,Description,Notes,SendDate,Sent,CommStatusID">

<CFIF ParameterExists(URL.RecordID)>
	<CFQUERY name="GetRecord" dataSource="#application.SiteDataSource#" maxRows=1>
		SELECT Communication.Title, Communication.Description, Communication.Notes, Communication.SendDate, Communication.Sent, Communication.CommStatusID, Communication.CommID AS ID_Field
		FROM Communication
		<CFIF ParameterExists(URL.RecordID)>
		WHERE Communication.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
	</CFQUERY>

	<CFIF not ListFind( FormFieldList, "CommID" )>
		<CFSET FormFieldList = ListAppend( FormFieldList, "CommID" )>
	</CFIF>

			
		<CFSET Title_Value = '#GetRecord.Title#'>			
		<CFSET Description_Value = '#GetRecord.Description#'>			
		<CFSET Notes_Value = '#GetRecord.Notes#'>			
		<CFSET SendDate_Value = #GetRecord.SendDate#>			
		<CFSET Sent_Value = '#GetRecord.Sent#'>			
		<CFSET CommStatusID_Value = #GetRecord.CommStatusID#>
		

<CFELSE>

			
		<CFSET Title_Value = ''>				
		<CFSET Description_Value = ''>				
		<CFSET Notes_Value = ''>
		<CFSET SendDate_Value = ''>
		<CFSET Sent_Value = ''>
		<CFSET CommStatusID_Value = ''>

</CFIF>


<cf_head>
	<cf_title>PhoneComm - Edit Record</cf_title>
</cf_head>

<h2>Edit Outbound Phone Campaign</h2>

<CFOUTPUT>
<FORM action="PhoneCommTask.cfm" method="post">
<CF_INPUT type="hidden" name="FieldList" value="#FormFieldList#">
<CFIF ParameterExists(URL.RecordID)>
	<CF_INPUT type="hidden" name="RecordID" value="#URL.RecordID#">
	<CF_INPUT type="hidden" name="CommID" value="#URL.RecordID#">
</CFIF>


<TABLE>
	<TR>
	<TD valign="top"> Title: </TD>
    <TD>
		<CF_INPUT TYPE="Text" NAME="Title" VALUE="#Title_Value#" SIZE="30" MAXLENGTH="30">
	</TD>
	<!--- field validation --->
	</TR>
	
	<TR>
	<TD valign="top"> Description: </TD>
    <TD>
	
		<CF_INPUT TYPE="Text" NAME="Description" VALUE="#Description_Value#" SIZE="80" MAXLENGTH="80">
		
	</TD>
	<!--- field validation --->
	</TR>
	
	
	<TR>
	<TD valign="top"> Notes: </TD>
    <TD>
		<TEXTAREA NAME="Notes">
			#Notes_Value#
		</TEXTAREA>
	</TD>
	<!--- field validation --->
	</TR>
	
	
	<TR>
	<TD valign="top"> SendDate: </TD>
    <TD>
	
		<CF_INPUT type="text" name="SendDate" value="#SendDate_Value#" maxLength="16">
		(i.e. 12/31/97)
	</TD>
	<!--- field validation --->
	<INPUT type="hidden" name="SendDate_date">
	</TR>
	
	
	<TR>
	<TD valign="top"> Sent: </TD>
    <TD>
	
		<CF_INPUT type="text" name="Sent" value="#Sent_Value#" maxLength="1">
		
	</TD>
	<!--- field validation --->
	</TR>
	
	
	<TR>
	<TD valign="top"> CommStatusID: </TD>
    <TD>
	
		<CF_INPUT type="text" name="CommStatusID" value="#CommStatusID_Value#" maxLength="4">
		
	</TD>
	<!--- field validation --->
	<INPUT type="hidden" name="CommStatusID_integer">
	</TR>
		
</TABLE>
	
<!--- form buttons --->
<INPUT type="submit" name="btnEdit_OK" value="    OK    ">
<INPUT type="submit" name="btnEdit_Cancel" value="Cancel">

</FORM>
</CFOUTPUT>




>