<!--- �Relayware. All Rights Reserved 2014 --->
<CFQUERY name="GetRecord" dataSource="#application.SiteDataSource#" maxRows=1>
	SELECT      dbo.Communication.CommID, dbo.Communication.Title, dbo.FlagGroup.FlagGroupTextID, dbo.FlagGroup.Name, dbo.Communication.CommTypeLID
FROM         dbo.Communication, dbo.CommFlagGroup, dbo.FlagGroup 
WHERE       dbo.Communication.CommID = dbo.CommFlagGroup.CommID AND
                   dbo.CommFlagGroup.FlagGroupTextID = dbo.FlagGroup.FlagGroupTextID
ORDER BY  dbo.Communication.Title
	
	SELECT CommFlagGroup.CommID AS ViewField1, 
	CommFlagGroup.FlagGroupTextID AS ViewField2, 
	CommFlagGroup.CommID AS ID_Field
	FROM CommFlagGroup
	<CFIF ParameterExists(URL.RecordID)>
	WHERE CommFlagGroup.CommID =  <cf_queryparam value="#URL.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
</CFQUERY>


<cf_head>
	<cf_title>CommFlagGroup - View Record</cf_title>
</cf_head>

<H2>Associate Communications To Flags</H2>


<CFOUTPUT query="GetRecord">

<FORM action="CommFlagGroup_RecordAction.cfm" method="post">
	<CF_INPUT type="hidden" name="RecordID" value="#ID_Field#">

	<!--- form buttons --->
	<INPUT type="submit" name="btnView_First" value=" << ">
	<INPUT type="submit" name="btnView_Previous" value="  <  ">
	<INPUT type="submit" name="btnView_Next" value="  >  ">
	<INPUT type="submit" name="btnView_Last" value=" >> ">
	<INPUT type="submit" name="btnView_Add" value="   Add    ">
	<INPUT type="submit" name="btnView_Edit" value="  Edit  ">
	<INPUT type="submit" name="btnView_Delete" value="Delete">

</FORM>


<TABLE>

	<TR>
	<TD valign="top"> CommID: </TD>
	<TD> #htmleditformat(ViewField1)# </TD>
	</TR>

	<TR>
	<TD valign="top"> FlagGroupTextID: </TD>
	<TD> #htmleditformat(ViewField2)# </TD>
	</TR>

</TABLE>

</CFOUTPUT>

>>>