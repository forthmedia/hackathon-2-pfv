<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		AddCallRecordTask.cfm
Author:			SWJ & WAB
Date created:	1999


Amendment History:

(DD-MMM-YYYY)	Initials 	What was changed

2001-28-06 		SWJ 		Added variable #application. MaxLenCommDetailFeedback# 
2001-08-17		SWJ			Added the code to support CF_AScreenUpdate
2001-08-21		SWJ			Added the code to support perList.cfm - still not working
							it has some CFInclude problems
2001-10-22		CPS			Modified queries to make use of new attributes commtypeid, commreasonid and commstatusid
2001-11-20		SWJ			Modified to support unique orgs in open phoneComms
2001-11-24		SWJ			Changed significantly to support new commDetail structure
							new campaigns and actions
2001-11-20		SWJ			Removed call to add Action
2004-12-07		WAB 		Problem with very long text getting truncated - changed the way that  long items are split in two and corrected another bug
2005-02-28			WAB			Dates changed to use relaydatefield and the update altered to expect this
2006-03-02		SWJ			Added the cf_aScreenUpdate function.
2007-06-12		WAB			added cflocation
2011-01-13		MS			LID:4314 Dropdown to record active campaigns
2013-08-16 		PPB 		Case 436510 show commdetail.commSenderId as contactedBy	if it's available
							
Enhancement still to do:

1.  Add the section to show the list of people at the end of the screen
2.  Add the method for inserting a next action record.  This needs to work with frmEntityType a
3.  Needs error checking and logging
4.  Need to modify the code to work with organisation rather than location

--->

<!--- <cfabort showerror="callRecord task">
 --->
<cf_head>
	<cf_title>Add Contact Record</cf_title>
</cf_head>

<!--- <cfif not isDefined('locationID')>
	<cfif isDefined('personid') and personid neq 0>
		<CFQUERY NAME="GetPersonDets" datasource="#application.siteDataSource#">
			Select locationID,organisationID from person where personID = #personID#
		</CFQUERY>
		<CFSET locationID = GetPersonDets.locationID>
		<CFSET organisationID = GetPersonDets.organisationID>
	<CFELSEIF isdefined("frmcurrentEntityID") and frmEntityType eq "organisation">
		<CFQUERY NAME="GetLocDets" datasource="#application.siteDataSource#">
			Select top 1 locationID from location where organisationID = #frmcurrentEntityID#
		</CFQUERY>
		<CFSET locationID = GetLocDets.locationID>
		<CFSET organisationID = frmcurrentEntityID>
	<CFELSEIF isdefined("frmcurrentEntityID") and frmEntityType eq "location">
		<CFSET LocationID = frmcurrentEntityID>
		<CFQUERY NAME="GetLocDets" datasource="#application.siteDataSource#">
			Select organisationID from location where locationID = #frmcurrentEntityID#
		</CFQUERY>
		<CFSET locationID = GetLocDets.organisationID>
	</cfif>
</CFIF> --->





<CFQUERY NAME="getCommType" datasource="#application.siteDataSource#">
	SELECT distinct cdtrs.commTypeId
			  FROM commDetailTypeReasonStatus cdtrs
		where cdtrs.commreasonID =  <cf_queryparam value="#val(form.frmcommReasonID)#" CFSQLTYPE="CF_SQL_INTEGER" >  
			and commStatusID =  <cf_queryparam value="#val(form.frmcommStatusID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


	<!--- WAB 2005-02-28 date changes
		txtDate should now be converted to a date field (using the hidden field _date), so need to add hours and minutes to it 

		<cfif isnumeric(left(txtDate,4)) eq "true">
			<cfset myDate = txtDate>
		<cfelse>
			<cfset myDate = CreateDateTime(mid(FORM.txtDate,7,4), mid(FORM.txtDate,4,2), left(FORM.txtDate,2),  
			 FORM.frmHour, FORM.frmMinute, 00)>
			</cfif>
	--->		 

		<cfset myDate = dateadd("n",form.frmMinute,dateadd("h",FORM.frmHour,txtDate))> 

	<!--- 	WAB 	2005-02-22 removed code for dealing with very long feedback and put it into the cf_addcommdetail 
		 	AWJR 	2011-03-18 Ticket 4939 PersonID can be multiple. Make the insert into a loop 
			WAB 	2013-12-03 2013RoadMap2 Item 25 Contact History.  CF_addCommDetail now takes Subject/Body rather than feedback
	--->
	<cfloop list="#personID#" index="persIdx">
		<cfif val(persIdx)>
		<CF_addCommDetail
				personID 	 	= #persIdx#
		commTypeID	 	= #val(getCommType.commTypeId)#
		commReasonID = #val(form.frmcommReasonID)#
		commStatusID = #val(form.frmcommStatusID)#
		dateSent	 	= #createodbcdatetime(myDate)#
		commCampaignID 	= #form.activeCampaigns# <!--- MS  2011-01-13 LID4314 --->
		body	 	= "#Feedback#"
		freezedate		= freezedate		
		commSenderId	= #form.frmAccountmngrID#		<!--- 2013-08-16 PPB Case 436510 --->
		>


	<cfset form["new"] = 	addedCommDetailID>
	<cf_updatedata>

	<!--- 2011-03-04 AWJR Added the flag update. addedCommDetailID is the entry returned from the above call. 
	<cfparam name="FlagList" default="">
	<cfloop list="#FlagList#" index="FlagIdx">
		<cfif structKeyExists(form,"#FlagIdx#_0") AND FORM["#FlagIdx#_0"] NEQ FORM["#FlagIdx#_0_ORIG"] >
			<cfif val(FORM["#FlagIdx#_0"])>
				<cfset tmp = application.com.flag.setBooleanFlagByID(flagID=FORM["#FlagIdx#_0"],entityID = addedCommDetailID)>
			<cfelse>
				<cfset tmp = application.com.flag.unsetBooleanFlag(flagID=FORM["#FlagIdx#_0"],entityID = addedCommDetailID)>
			</cfif>		
		</cfif>
	</cfloop>
---> 
   
		</cfif>
	</cfloop>
	<CFSET session.campaignID="#form.activeCampaigns#"><!--- MS  2011-01-13 LID4314 storing the selected active campaign as sessions variable for later use--->

<!--- 2006-03-03 SWJ added the ability to update perProfileSummary screen from here --->
<cf_aScreenUpdate>

<cfset queryString="">
<CFIF isdefined("frmOrganisationID")>
	<CFSET frmOrganisationID = frmOrganisationID>
	<cfset queryString=listappend(queryString,"frmOrganisationID=#frmOrganisationID#","&") >
</cfif>

<CFIF isdefined("frmcurrentEntityID")>
	<CFSET frmcurrentEntityID = frmcurrentEntityID>
	<cfset queryString=listappend(queryString,"frmcurrentEntityID=#frmcurrentEntityID#","&") >
	<cfset queryString=listappend(queryString,"frmEntityType=#frmEntityType#","&") >
</cfif>

<CFSET FRMPERSONID ="#PersonID#">
<cfset message="Call history record was successfully inserted">
<cfset frmRefresh = true>  <!--- NJH 2007-01-26 this forces a query to run again--->
<cfset frmMaxRows="20"><!--- this sets the maxrows string to suppress the query --->

<!--- 
	WAB 2007-06-12 replaced with a cflocation.  Problems in commhistory.cfm when sorting report, tries to repost to this template
	added querystring variable above as well
<CFINCLUDE TEMPLATE="CommHistory.cfm">
--->

<!--- AWJR 2011-03-18 Ticket 4939 PersonID can be multiple. Redirect to the addCallRecord template. --->
<cfif listlen(personID) GT 1>
	<cfset message="Call history records were successfully inserted.">
	<cflocation url="/Phoning/addCallRecord.cfm?message=#message#&#querystring#" addtoken="No"> 
<cfelse>
<cflocation url="/Phoning/commhistory.cfm?frmPersonid=#personid#&frmRefresh=true&frmMAxRows=20&message=#message#&#querystring#" addtoken="No"> 
</cfif>





<!--- <A HREF="/actions/quickAdd.cfm">Add Action</A> --->
<!--- need to include a screen here to show other contacts within the org --->
<!--- <CFINCLUDE TEMPLATE="../data/persList.cfm"> --->







