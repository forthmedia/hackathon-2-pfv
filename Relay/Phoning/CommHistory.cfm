<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		CommHistory.cfm
Author:			Simon WJ
Date created:

	Objective - this lists the contact history for a given entity

	Syntax	  -	it can be used in the viewer or it requires frmentityType AND frmcurrentEntityID
				to be passed for it to work.  This combination is then used to work out
				how to get the current commHistory and what to show.

	Parameters -frmentityType the current entity type.  Supports person, location
				and organisation entity types
				frmcurrentEntityID the current entityID

	Return Codes - none

Amendment History:

Date 		Initials 	What was changed

2001-08-17	SWJ			I modified it to support organnisation contact records
2001-08-21	SWJ			Fixed the bug with frmOrganisationID
2001-10-02	SWJ			Modified the new command to show as a subMenu item
2001-10-17	CPS			Modified queries to make use of new attributes commtypeid, commreasonid and commstatusid
2002-10-25    DAM         Modified query to bring back history and details in one dataset. This was necessary#
						because they are in two separate tables and a detail only has a history if its been
						changed. Also grouped by CommDetail showing history for each.
2002-09-10	SWJ			Modified getCommDetailType query to use showInCommHistory
2005-02-22	WAB			Changes to deal with long feedback only show first record in sequence
2006-05-21	SWJ			Modified widths of tables and tds as a) they were not showing within an i.e. window
						and b) they text was not wraping well
2007/01/19	NJH			Added column sorting
2007/03/01  NJH			Forced the 'GetContactHistoryRecordIDs' query to run if a user hit the refresh link.
2008/01/22 	WAB			Made sure that can handle frmentityType either text or numeric for new screen code
2010/10/15	NAS			LID4313: Missing "Type of Contact " in the view contact history by organization
27-Jan-2011 MS			LID:5453 Modified queries GetContactHistoryRecordIDs and GetContactHistoryRecords to bring back campain name to be displayed on contact history report
2011/04/01  AWJR		P-LEX051 Added in profiles for the commdetail.
2011/07/27	NYB			P-LEN024 changed commcheckDetails.cfm to commDetailsHome.cfm
2011/11/16	WAB			Changes link to view communication to show in a popup (saves dealing with back! and better size for viewing a comm)
2011-12-14 	NYB 		LHID8289 changed join on Campaign Name in GetContactHistoryRecords query
2013-02-13	WAB			Sprint 15&42 Comms. Change reference to displayEmailContent to viewCommunication
2013/02/28	NJH			case 433940 and 433941. Add support for message. show the message when clicking on the message title rather than the comm. We differentiate between a comm message and comm email by
						passing through the commTypeID followed by "|message" if we are wanting to filter by messages. If commmessage is defined, we know we are filtered on a type of message.
2013-11-21	WAB			2013RoadMap2 Item 25 Major Changes
							Created a view to union various tables.  Web Visits now come from activities
							Communications, Ad Hoc emails and System Emails now have different commdetail.CommTypeID    (2,50,51)
							Altered drilldowns
							Text of systemEmails, adhoc emails and miscellaneous contacts is now saved in commDetailContent
2015/02/26	NJH			Jira FIFTEEN-204 - changed sortOrder variable from frmSortorder to sortOrder as TFQO submits sortOrder.
2016/01/29	NJH			BF-395 - check that frmScreenEntityID is not an empty string before setting entityID. Then check form scope and finally url scope to find entityId.
Enhancement still to do:

1.  Need to make it so that it can be used by an organisation screen
	- no this is achieved via
2.  Need to manage the various states better
3.  Need to allow for meeting data types
4.  Need to get the drill down to detail screen working.

CommTypes
2	Email
3	Fax
4	Download
5	Always by both email and fax
6	Either fax or email, preference email
7	Either fax or email, preference fax
8	Outbound Call
9	Inbound call
10	Meeting	--->

<cfparam name="PageBegin" default="1">
<CFPARAM NAME="includeHTMLTopBottom" DEFAULT="true">
<CFPARAM NAME="showheader" DEFAULT="true">
<!--- 2006-05-12 AJC CR_ATI033 - Moved from filter section --->
<cfparam name="frmMaxRows" type="numeric" default="1000">

<cfparam name="numRowsPerPage" type="numeric" default="50">
<cfparam name="startRow" type="numeric" default="1">

<!--- NJH 2007/01/19 --->
<cfparam name="sortOrder" type="string" default="Date DESC,TableIdentity desc">
<cfparam name="frmLastColumnSorted" type="string" default="Date">
<cfparam name="frmLastSort" type="string" default="DESC">

<!--- NJH 2016/01/29 BF-395 --->
<cfif isDefined("frmScreenEntityID") and frmScreenEntityID neq "" and isDefined("frmScreenEntityType")>
	<cfset entityID = frmScreenEntityID>
	<cfset entityType = frmScreenEntityType>
<cfelseif structKeyExists(form,"frmCurrentEntityID") and structKeyExists(form,"frmEntityType")>
	<cfset entityID = form.frmCurrentEntityID>
	<cfset entityType = form.frmEntityType>
<cfelseif structKeyExists(url,"frmCurrentEntityID") and structKeyExists(url,"frmEntityType")>
	<cfset entityID = url.frmCurrentEntityID>
	<cfset entityType = url.frmEntityType>
</cfif>

<CFIF isdefined("entityType")>

	<CFQUERY NAME="getEntityData" datasource="#application.siteDataSource#">
		<CFIF entityType eq "Location" or entityType eq "1">
			Select SiteName AS Name FROM Location
				WHERE locationId =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<CFELSEIF entityType eq "organisation" or entityType eq "2">
			Select organisationName as Name from organisation
				where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelseif entityType eq "Person" or entityType eq "0">
			Select firstName +' '+ lastname as name from person
					where personid =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</CFQUERY>

</cfif>

<CFQUERY NAME="getCommDetailType" datasource="#application.siteDataSource#">
	SELECT cdt.name, cdt.commTypeId
		  FROM commDetailType cdt
	where cdt.showInCommHistory = 1
	order by name
</CFQUERY>


<cfquery name="getGroupFlagsQuery" datasource="#application.siteDataSource#">
	SELECT
		flaggroupID
	FROM
		[FlagGroup]
	WHERE
		[EntityTypeID] =  <cf_queryparam value="#application.entityTypeID["commDetail"]#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY
		[OrderingIndex]
</cfquery>

<cfset flagGroupArray = application.com.flag.getJoinInfoForListOfFlagGroups(flagGroupList = valueList(getGroupFlagsQuery.flaggroupID), entityTableAlias = "ch")>

<CFPARAM NAME="CommType" DEFAULT="">


<cfset dateFilter = "> getdate() - 120">

<CFQUERY NAME="GetContactHistoryRecords" datasource="#application.siteDataSource#">
	select
		* ,
			<cfloop from="1" to="#arrayLen(flagGroupArray)#" index="arrIdx">
			#flagGroupArray[arrIdx].SELECTFIELD# AS #flagGroupArray[arrIdx].alias#,
			</cfloop>
		case when isNull(metadata,'') like '%NotSentToMergedPerson%' then 1 else 0 end as SentToSomeOneElse,
		--case when tablename = 'message' /*OR commTypeID = 50*/ then 'Text' else 'Link' end as showTitleAs
		'Link' as showTitleAs


	FROM
		vContactHistory ch
			<cfloop from="1" to="#arrayLen(flagGroupArray)#" index="arrIdx">
			#flagGroupArray[arrIdx].JOIN#
		</cfloop>
	WHERE
		1=1
		<cfif commType is not "">
		AND commtypeid  IN ( <cf_queryparam value="#CommType#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
		AND #entityType#ID = <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >


		and date #datefilter#

	order by <cf_queryObjectName value="#sortOrder#">    <!--- NJH 2007/01/19 --->
</CFQUERY>

<cfloop query="GetContactHistoryRecords">
	<cfif title contains "[[">
		<cfset mergeStruct = {commid=commid,personid = personid}>
		<cfset mergeResult = application.com.relayTranslations.checkForAndEvaluateCFVariables(phraseText = title,mergeStruct = mergeStruct)>
		<cfset querySetCell (GetContactHistoryRecords,"Title",mergeresult,currentRow)>
	</cfif>
	<cfif SentToSomeOneElse and entityType eq "Person">
		<cfset querySetCell (GetContactHistoryRecords,"Title",title & "<BR>Sent to #personName#",currentRow)>
	</cfif>
</cfloop>

<cfif includeHTMLTopBottom is true>

	<cf_head>
	    <cf_title>CommHistory - Search Result</cf_title>

	 <cf_includeCssOnce template="/javascript/lib/jquery/css/themes/smoothness/jquery-ui.css">
	<cf_includejavascriptonce template = "/javascript/openwin.js">
	<cf_includejavascriptonce template = "/javascript/extExtension.js">
	<cf_includejavascriptonce template = "/javascript/addShowActions.js">
	<SCRIPT type="text/javascript">
		function contactHistoryDrillDown(args) {
			//console.log(args)
			var tabOptions = {reuseTab:true,iconClass:'accounts'};

			var url = '/webservices/callWebService.cfc?method=callWebService&webServiceName=communicationsWS&methodName=getContactHistoryContent&entityType='+args.tableName
			switch (args.tableName.toLowerCase()) {

				case 'message':
					openNewTab('ContactHistoryDrill','Message','/phoning/viewMessage.cfm?messageID='+args.tableIdentity,tabOptions);
					/*url = url+'&entityID='+args.tableIdentity
					openDialog(url);*/
					break;

				case 'commdetail':

					switch (args.commTypeID) {
						case '2' : // communication
							openNewTab('ContactHistoryDrill','Comm Preview','/communicate/viewCommunication.cfm?CommID='+args.commid,tabOptions);
							/*url = url+'&entityID='+args.commid+'&commTypeID='+args.commTypeID;
							openDialog(url);*/
							break;
						case '50': // system email
							openNewTab('ContactHistoryDrill','System Email','/phoning/CommHistory_Detail.cfm?CDID='+args.tableIdentity,tabOptions);
							/*url = url+'&entityID='+args.tableIdentity+'&commTypeID='+args.commTypeID;
							openDialog(url);*/
							break;
						case '51': // add Hoc Email
							openNewTab('ContactHistoryDrill','Ad Hoc Email','/phoning/CommHistory_Detail.cfm?CDID='+args.tableIdentity,tabOptions);
							/*url = url+'&entityID='+args.tableIdentity+'&commTypeID='+args.commTypeID;
							openDialog(url);*/
							break;
						default:
							openNewTab('ContactHistoryDrill','Contact','/phoning/CommHistory_Detail.cfm?CDID='+args.tableIdentity,tabOptions);
							/*url = url+'&entityID='+args.tableIdentity;
							openDialog(url);*/
							break;
					}

					break

				case 'activity':

					openNewTab('PagesVisited','PagesVisited','/report/entityUsageList.cfm?reportMode=NotStandalone&userToView='+args.personID + '&visitid=' + args.visitID,tabOptions);
					/*url = url+'&userToView='+args.personID+'&visitID='+args.visitID;
					openDialog(url);*/
					break

			}

		}

		function addContact(entityID,entityType) {
			var qryStr = "../data/actionEntryFrame.cfm?frmEntityType="+entityType+"&frmcurrentEntityID="+entityID
			openWin(qryStr,'AddContact','width=550,height=900,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
		}
	</SCRIPT>
	</cf_head>
</cfif>

<cfparam name="pagesBack" type="numeric" default="1">

<cfif showheader is true>

	<CFOUTPUT>
		<CF_RelayNavMenu pageTitle="Phr_Sys_ContactHistoryfor #getEntityData.Name#" thisDir="phoning">
			<!--- <CF_RelayNavMenuItem MenuItemText="phr_Sys_Back" CFTemplate="javascript:history.go(-#pagesBack#);"> --->
			<CFIF findNoCase("commhistory.cfm",SCRIPT_NAME,1)>
				<CF_RelayNavMenuItem MenuItemText="phr_Sys_Refresh" CFTemplate="javaScript:refreshForm();">
			</cfif>
			<!---
			<cfif entityType eq "Person">
				<CF_RelayNavMenuItem MenuItemText="phr_sys_nav_Files" CFTemplate="/data/entityFiles.cfm?entityType=Contact&frmEntityID=#entityID#">
			</cfif>
			--->
			<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_AddContactRecord" CFTemplate="javaScript:addContact(#entityID#,'#entityType#')">
			<cfif isDefined("entityID") and isNumeric(entityID)>
				<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_AddAction" CFTemplate="javaScript:addActionRecord('#entityType#',#entityID#)">
			</cfif>
		</CF_RelayNavMenu>
	</CFOUTPUT>

	<CFIF getCommDetailType.recordCount gt 9 or getCommDetailType.recordCount lte 5>
		<CFSET modVal=5>
	<CFELSE>
		<CFSET modVal=Ceiling(getCommDetailType.recordCount/2)>
	</CFIF>

	<FORM ACTION="" METHOD="post" NAME="mainForm" style="float:right;margin:5px 5px 0 0;">
		<CF_INPUT TYPE="hidden" NAME="frmCurrentEntityID" VALUE="#entityID#">
		<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#entityType#">
		<!--- 2007/01/19 sort commHistory by different fields/columns --->
		<CF_INPUT TYPE="hidden" NAME="sortOrder" VALUE="#sortOrder#">
		<CF_INPUT TYPE="hidden" NAME="frmLastColumnSorted" VALUE="#frmLastColumnSorted#">
		<CF_INPUT TYPE="hidden" NAME="frmLastSort" VALUE="#frmLastSort#">
		<cfset pagesBack = pagesBack + 1>
		<CF_INPUT TYPE="hidden" NAME="pagesBack" VALUE="#pagesBack#">
		<CF_INPUT type="hidden" name="pageBegin" value="#pageBegin#">
		<INPUT TYPE="hidden" NAME="frmRefresh" VALUE="false">
		<select name="CommType">
			<option value="">Show All
		<CFOUTPUT QUERY="getCommDetailType">
			<option value="#commTypeID#"<cfif isDefined("CommType") AND CommType eq commTypeID and not isDefined("commMessage")> selected</cfif>>#htmleditformat(name)#
		</CFOUTPUT>
		</select>
		<input type="submit" value="phr_Sys_go">
	</FORM>
</cfif>


<cfset showTheseColumns = "commform,agent,personname,title,status,date">
<cfif entityType eq "Person">
	<cfset showTheseColumns = application.com.globalFunctions.listMinusList(showTheseColumns,"personName")>
</cfif>
<cfloop array="#flagGroupArray#" index="flag">
	<cfset showTheseColumns = application.com.globalFunctions.ListInsertBefore(showTheseColumns,flag.alias,"date")>
</cfloop>


<cf_tableFromQueryObject
	showTheseColumns="#showTheseColumns#"
	keyColumnList="title"
	keyColumnKeyList=" "
	keyColumnURLList="##"
	keyColumnOnClickList="contactHistoryDrillDown({tableName:'##Tablename##' *comma tableIdentity:##TableIdentity## *comma commid:'##commid##' *comma commTypeID:'##commTypeID##' *comma personID:'##personID##' *comma visitID:'##visitID##'})"
<!---
	keyColumnURLList=" ,/Phoning/CommHistory_Detail.cfm?#iif(GetContactHistoryRecords.RowType is 'Current',de('CDID'),de('CDHID'))#=#URLEncodedFormat(GetContactHistoryRecords.Detail)#&messageID=#URLEncodedFormat(GetContactHistoryRecords.messageID)#&personID=#urlEncodedFormat(GetContactHistoryRecords.personID)#,/report/EntityUsageList.cfm?userToView=#GetContactHistoryRecords.personid#"
	keyColumnOnClickList="#iif(GetContactHistoryRecords.commtypeLID neq 98,de('viewCommunication(#GetContactHistoryRecords.commID#*comma#GetContactHistoryRecords.messageID#); return false;'),de(' '))#, , "
 --->
	useInclude="false"
	sortOrder="#sortOrder#"
	queryObject="#GetContactHistoryRecords#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	dateTimeFormat="date"
	showSaveAndAddNew="true"
	columnTranslation="true"
	columnTranslationPrefix="phr_commhistory_"
	showCellColumnHeadings="false"
	allowColumnSorting="true"
	>