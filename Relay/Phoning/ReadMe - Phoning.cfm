<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
<CFINCLUDE TEMPLATE="../styles/DefaultStyles.cfm">
	
</cf_head>


<H1>  What  is the Phoning system?</H1>
<P>             The Phoning System in Channel Foundation is a set of 
tools for managing telephone campaigns. </P>
<H2>Key business functions:</H2>
<P>The tools support the following features: </P>
<UL>
  <LI>          
  Create a new campaign 
  <LI>   Link a set of selections to a 
  campaign 
  <LI>Define which flag groups the campaign should use as 
  call results</LI></UL>  
<H2>Security</H2>
<P>           
               
         Access to the phoning tools is 
switched on in the user profiles.</P>
<H2>Tables, views, flag groups and lookup lists used</H2>
<P>There are a number of tables, views and lookup lists that support the phoning system.</P>
<H3>Tables</H3>
<UL>
  <LI>Communication - the phoning system shares the 
  communication record where it defines the type of&nbsp; phone project based on 
  CommTypeLID.&nbsp; A commTypeLID of 61 = 'Outbound' and 62 = 'Inbound'
  <LI>   PhoneAction - this contains records linked to 
  Communication that are the records that drive an outbound campaign or record 
  the outcome of an inbound campaign.&nbsp; The table contains data like number 
  of call attempts, person spoken to the locationID, last call date and callback times.</LI></UL>
<H3>Views</H3>
<UL>
  <LI>PhoneCommRecords</LI>
  <LI>PhoneFlagRecords</LI>
  <LI>CommTypePhone - lists the lookupID for the phone type campaigns</LI></UL>
<H3>Flag Groups</H3>
<UL>
  <LI>PhonSys - this a parent location flag group which contains all of the 
  other phoning flag groups</LI>
  <LI>Standard Inbound results - a list of standard inbound results</LI>
  <LI>Standard outbound call results - a list of standard outbound call 
  results</LI>
  <LI>Additional flag groups - other flag groups can be created to deal with 
  campaign specific call results.</LI>
  <LI>As many flag groups as wanted can be linked to a campaign</LI></UL>
<H3>Lookup Lists</H3>
<UL>
  <LI></LI></UL>
<H2>Setting up a Locator for a country</H2>
<OL>
  <LI>            Click here to create a LocatorDef table entry.</LI></OL>
<P>&nbsp;</P>

<h2>Process of creating a phoning project<span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span>     </h2>
<OL>
 <li class=MsoNormal style="mso-list: l6 level1 lfo8; tab-stops: list .5in" 
 ><span
     lang=EN-GB>Create a communication with one of the following types:</span><![if !supportEmptyParas]><![endif]>
 <ul style="MARGIN-TOP: 0in" type=disc>
  <li class=MsoNormal style="mso-list: l6 level2 lfo8; tab-stops: list 1.0in" 
   ><span
      lang=EN-GB>Outbound call</span>
  <li class=MsoNormal style="mso-list: l6 level2 lfo8; tab-stops: list 1.0in" 
   ><span
      lang=EN-GB>Inbound call<![if !supportEmptyParas]><![endif]></span></li>
 </ul>
 <li class=MsoNormal style="mso-list: l6 level1 lfo8; tab-stops: list .5in" 
 ><span
     lang=EN-GB>Specify which flag group you want associated with this for
     recording results (This is recorded in the Commflag table using
     CommflagGroupAdd.cmf.)</span>
 <li class=MsoNormal style="mso-list: l6 level1 lfo8; tab-stops: list .5in" 
 ><span
     lang=EN-GB>Use <i>Call result</i> screen (AddCallResult.cfm.) to record
     call results.</span><![if !supportEmptyParas]><![endif]><span
     lang=EN-GB> </span>
  <LI class=MsoNormal 
  style="mso-list: l6 level1 lfo8; tab-stops: list .5in"><SPAN lang=EN-GB>If 
  outbound:</SPAN><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span></LI></OL>
<UL>
  <LI>
  <DIV class=MsoNormal 
  style="MARGIN-LEFT: 1in; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo12; tab-stops: list 1.0in"><![if !supportLists]><![endif]><span lang=EN-GB>Associate a communication to a
selection(s)</span></DIV>
  <LI>
  <DIV class=MsoNormal 
  style="MARGIN-LEFT: 1in; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo12; tab-stops: list 1.0in"><![if !supportLists]><span lang=EN-GB style="FONT-FAMILY:
 Symbol"><span style="FONT: 7pt 'Times New Roman'">&nbsp;
</span></span><![endif]><span lang=EN-GB>Generate phone actions records for
each record in the selection</span></DIV><![if !supportLists]><![endif]>
  <LI>
  <DIV class=MsoNormal 
  style="MARGIN-LEFT: 1in; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo12; tab-stops: list 1.0in"><span lang=EN-GB>Carry out the phoning</span><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span></DIV></LI></UL>

<h2>Phoning Reports<span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span> </h2>

<ol style="MARGIN-TOP: 0in" type=1>
 <li class=MsoNormal style="mso-list: l8 level1 lfo13; tab-stops: list .5in" 
 ><span
     lang=EN-GB>For a communication<u><o:p></O:P></u></span>
 <ul style="MARGIN-TOP: 0in" type=disc>
  <li class=MsoNormal style="mso-list: l8 level2 lfo13; tab-stops: list 1.0in" 
   ><span
      lang=EN-GB>Number of Not Answered, Engaged, Calls etc.</span>
  <li class=MsoNormal style="mso-list: l8 level2 lfo13; tab-stops: list 1.0in" 
   ><span
      lang=EN-GB>Call Status flag counts</span>
  <li class=MsoNormal style="mso-list: l8 level2 lfo13; tab-stops: list 1.0in" 
   ><span
      lang=EN-GB>First call date, last call date.</span>
  <li class=MsoNormal style="mso-list: l8 level2 lfo13; tab-stops: list 1.0in" 
   ><span
      lang=EN-GB>Lead status (records phoned, records left to phone etc.)</span></li>
 </ul></li>
</ol>

<p class=MsoNormal><span lang=EN-GB><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></O:P></span></p>

<h2>Making an Outbound Call<span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span>   </h2>
<OL>
  <LI>
  <DIV class=MsoNormal 
  style="mso-list: l11 level1 lfo14; tab-stops: list .5in"><span
     lang=EN-GB>Go to list of phoning projects<u><o:p></O:P></u></span><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span><SPAN lang=EN-GB></SPAN></DIV>
  <LI>
  <DIV class=MsoNormal 
  style="mso-list: l11 level1 lfo14; tab-stops: list .5in"><SPAN 
  lang=EN-GB>Choose <i>open </i>viewer next to the one you want (This should
     set a Cookie(?) or variable of the command.<span style="mso-spacerun:
     yes">&nbsp; </span>This will be used to get the next lead, locking the phone
     action record.)<u><o:p></O:P></u></SPAN><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span></DIV>
  <LI>
  <DIV class=MsoNormal 
  style="mso-list: l11 level1 lfo14; tab-stops: list .5in"><span
     lang=EN-GB>Select get next load - Load lead into the viewer.<u><o:p></O:P></u></span><![if !supportEmptyParas]><![endif]></DIV>
  <LI>
  <DIV class=MsoNormal 
  style="mso-list: l11 level1 lfo14; tab-stops: list .5in"><span
     lang=EN-GB>Make call<u><o:p></O:P></u></span><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span></DIV>
  <LI>
  <DIV class=MsoNormal 
  style="mso-list: l11 level1 lfo14; tab-stops: list .5in"><span
     lang=EN-GB>At the end of the call set call outcome
     (AddCallRecord.cfm).<span style="mso-spacerun: yes">&nbsp; </span>Update phone
     action record and unlock if necessary.<u><o:p></O:P></u></span><![if !supportEmptyParas]><![endif]><![if !supportEmptyParas]><![endif]></DIV></LI></OL>

<h2>Recording a complaint</h2>

<ol style="MARGIN-TOP: 0in" type=1>
 <li class=MsoNormal style="mso-list: l17 level1 lfo15; tab-stops: list .5in" 
 ><span
     style="mso-ansi-language: EN-US" 
 >Complaints are recorded using a pair of
     flags:<o:p></O:P></span></li>
</ol>
<UL>
  <LI>
  <DIV class=MsoNormal 
  style="MARGIN-LEFT: 0.75in; TEXT-INDENT: -0.25in; mso-list: l15 level1 lfo19; tab-stops: list .75in"><![if !supportLists]><![endif]><span style="mso-ansi-language: EN-US">One for the
complaint code <o:p></O:P></span></DIV>
  <LI>
  <DIV class=MsoNormal 
  style="MARGIN-LEFT: 0.75in; TEXT-INDENT: -0.25in; mso-list: l15 level1 lfo19; tab-stops: list .75in"><![if !supportLists]><![endif]><span style="mso-ansi-language: EN-US">One for
additional memo text<o:p></O:P></span><![if !supportLists]></DIV><![endif]>
  <LI>
  <DIV class=MsoNormal 
  style="MARGIN-LEFT: 0.75in; TEXT-INDENT: -0.25in; mso-list: l15 level1 lfo19; tab-stops: list .75in"><span style="mso-ansi-language: EN-US">One for whether they have 
  been resolved</span><SPAN style="mso-ansi-language: EN-US">  
   <o:p></O:P></SPAN><span style="mso-ansi-language: EN-US" 
 ><![if !supportEmptyParas]><![endif]><o:p></O:P></span></DIV></LI></UL>
<OL>
 <li class=MsoNormal style="mso-list: l17 level1 lfo15; tab-stops: list .5in" 
 ><span
     style="mso-ansi-language: EN-US" 
 >A screen is set up for recording
     complaints.<o:p></O:P></span>
 <li class=MsoNormal style="mso-list: l17 level1 lfo15; tab-stops: list .5in" 
 ><span
     style="mso-ansi-language: EN-US" 
 >A report is set up to give an analysis of
     complaints.<o:p></O:P></span></li></OL>

<p class=MsoNormal><span style="mso-ansi-language: EN-US"><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></O:P></span></p>

<h2>Setting a call result</h2>

<ol style="MARGIN-TOP: 0in" type=1>
 <li class=MsoNormal style="mso-list: l10 level1 lfo20; tab-stops: list .5in" 
 ><span
     style="mso-ansi-language: EN-US" 
 >Last call results are stored as part of a
     flag group linked to the locationID.<span style="mso-spacerun: yes">&nbsp;
     </span>The same flagID is used to record the last call result against the
     phoneAction record.<o:p></O:P></span>
 <li class=MsoNormal style="mso-list: l10 level1 lfo20; tab-stops: list .5in" 
 ><span
     style="mso-ansi-language: EN-US" 
 >The screen that sets the result is
     accessed via the viewer menu under Set Call Result.<o:p></O:P></span></li>
</ol>

<p class=MsoNormal><span style="mso-ansi-language: EN-US"><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></O:P></span></p>

<h2>Getting the next lead to phone</h2>

<p class=MsoNormal style="MARGIN-LEFT: 0.5in"><span lang=EN-GB>This process
automatically locates leads which are due for calling in the current
campaign.<span style="mso-spacerun: yes">&nbsp; </span>This is done in several
passes looking at leads in order of priority.<span style="mso-spacerun: yes">&nbsp;
</span>Once a lead is found, the Organisation, Location and Person ID's are
passed to the 'Display Lead Details' process.</span><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span></p>

<p class=MsoNormal style="MARGIN-LEFT: 0.5in"><span lang=EN-GB>If this lead is
returned and accepted by the next process, 'Get Next Lead' repeats.<span
 style="mso-spacerun: yes">&nbsp; </span>This continues until such a time that the
record found is either rejected by cancelling the record or when there are no
longer any leads left for phoning in the default campaign.</span><span lang=EN-GB><![if !supportEmptyParas]><![endif]><o:p></O:P></span></p>

<p class=MsoNormal style="MARGIN-LEFT: 0.5in"><span lang=EN-GB>The following
table shows the search parameters for each pass:</span><![if !supportEmptyParas]><![endif]><o:p></O:P></p>

<table border=0 cellspacing=0 cellpadding=0 style="BORDER-COLLAPSE:
 collapse; mso-padding-alt: 0in 4.0pt 0in 4.0pt">
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT: medium none; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in" 
   >
  <p class=MsoNormal><span lang=EN-GB style="FONT-SIZE: 10pt">Pass<o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext 0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt" 
   >
  <p class=MsoNormal align=right style="MARGIN-RIGHT: 1pt; TEXT-ALIGN: right"><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext
  0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >1<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext
  0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >2<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext
  0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >3<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext
  0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >4<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext
  0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >5<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: medium none; BORDER-RIGHT: windowtext
  0.75pt solid; BORDER-TOP: windowtext 0.75pt solid; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >6<o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT: medium
  none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal><span lang=EN-GB style="FONT-SIZE: 10pt">Lead Type<o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: medium none; BORDER-RIGHT: windowtext 0.75pt
  solid; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">DM Callback<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT: medium none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in" 
   >
  <p class=MsoNormal align=right style="TEXT-ALIGN: right" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: medium none; BORDER-RIGHT: windowtext 0.75pt solid; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">Non DM Callback<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT:
  medium none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in" 
   >
  <p class=MsoNormal align=right style="TEXT-ALIGN: right" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">Untried<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT: medium
  none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal><span lang=EN-GB style="FONT-SIZE: 10pt">Callback Date<o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: medium none; BORDER-RIGHT: windowtext 0.75pt
  solid; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">For today before current time<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT: medium none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in" 
   >
  <p class=MsoNormal align=right style="TEXT-ALIGN: right" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM: medium none;
  BORDER-LEFT: medium none; BORDER-RIGHT: windowtext 0.75pt solid; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">Missed from previous days<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: windowtext 0.75pt solid; BORDER-RIGHT:
  medium none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in" 
   >
  <p class=MsoNormal align=right style="TEXT-ALIGN: right" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">For today not before 15 mins from last call<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
 </tr>
 <tr style="HEIGHT: 14pt">
  <td width=96 valign=top style="BORDER-BOTTOM: windowtext 0.75pt
  solid; BORDER-LEFT: windowtext
  0.75pt solid; BORDER-RIGHT: medium none; BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 1in; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal><span lang=EN-GB style="FONT-SIZE: 10pt">Last called by<o:p></O:P></span></p>
  </td>
  <td width=297 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid; BORDER-TOP:
  medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 223pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal style="MARGIN-RIGHT: 1pt"><span lang=EN-GB
  style="FONT-SIZE: 10pt">Current user<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><span lang=EN-GB
  style="FONT-SIZE: 10pt" 
     >&#8730;<o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
  <td width=35 valign=top style="BORDER-BOTTOM:
  windowtext 0.75pt solid; BORDER-LEFT: medium
  none; BORDER-RIGHT: windowtext 0.75pt solid;
  BORDER-TOP: medium none; HEIGHT: 14pt; PADDING-BOTTOM: 0in; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; PADDING-TOP: 0in; WIDTH: 26pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid windowtext .75pt" 
   >
  <p class=MsoNormal align=center style="TEXT-ALIGN: center" 
     ><![if !supportEmptyParas]><![endif]>&nbsp;<span
   
      lang=EN-GB style="FONT-SIZE: 10pt"><o:p></O:P></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span style="mso-ansi-language: EN-US"><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></O:P></span></p>
<DIV></DIV>










