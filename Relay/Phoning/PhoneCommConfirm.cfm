<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid
						and to run in a popup window
--->


<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<CFIF NOT #checkPermission.AddOkay# GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="commmenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- the application .cfm checks for communication privleges but
		we also need to check for select privledges
		we can rerun the qrycheckperms but NOTE we have now lost
		the application .cfm query variables.
--->
<CFSET securitylist = "SelectTask:read">
<CFINCLUDE TEMPLATE="../templates/qrycheckperms.cfm">

<CFQUERY NAME="checkSelection" datasource="#application.siteDataSource#">
	SELECT CommID FROM Communication
	 WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 AND LastUpdated =  <cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
</CFQUERY>

<CFIF #checkSelection.Recordcount# IS 0>
	<CFSET message = "The communication record has been changed by another user since you last downloaded it.  Please begin again.">
	<CFINCLUDE TEMPLATE="commmenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- check to see if there are already selections for this communciation --->

<CFQUERY NAME="anySelections" datasource="#application.siteDataSource#">
	SELECT cs.CommID from CommSelection AS cs
		WHERE cs.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFIF #anySelections.RecordCount# IS NOT 0>
	<CFSET message = "There are already selections for this communication.  Please create a new communication.">
	<CFINCLUDE TEMPLATE="commmenu.cfm">
	<CF_ABORT>
</CFIF>

<CFIF IsDefined("frmSelectID") or frmpersonids is not "">
	<CFIF frmpersonids is "">
<!--- 		<CFQUERY NAME="saveSelection" datasource="#application.siteDataSource#">
			INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
			SELECT #frmCommID#, s.SelectionID, 0
		  	FROM Selection AS s
		  	WHERE s.SelectionID IN (#frmSelectID#)
		    AND s.CreatedBy = #request.relayCurrentUser.usergroupid#
		</CFQUERY> --->
		<!--- can communciation to any selections that are shared as well as owned --->
		<CFQUERY NAME="saveSelection" datasource="#application.siteDataSource#">
			INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
			SELECT <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, s.SelectionID, 0
		  	FROM Selection AS s, SelectionGroup AS sg
		  	WHERE s.SelectionID  IN ( <cf_queryparam value="#frmSelectID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			AND sg.SelectionID = s.SelectionID
			AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
		</CFQUERY>

	<CFELSE>
 		<!--- This communication is to a list of people, not a selection - add a dummy record (needed in get comminfo query - not sure whether this is best way to do this) --->
		<CFQUERY NAME="saveSelection" datasource="#application.siteDataSource#">
			INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
			Values( <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0, 11)
		</CFQUERY>
		<CFPARAM name="frmselectid" default="">   <!--- needed later in template by regular communication --->
	</CFIF>

<CFELSE>
	<CFSET message = "You must choose a selection.">
	<CFINCLUDE TEMPLATE="commmenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- Use the communication to store the last updated info --->
<CFQUERY NAME="checkSelection" datasource="#application.siteDataSource#">
	UPDATE Communication
	 SET LastUpdated =  <cf_queryparam value="#Now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFQUERY NAME="getCommInfo" datasource="#application.siteDataSource#">
		SELECT c.Title,
			   c.Description,
			   cs.SelectionID,
			   c.CommFormLID,
			   c.SendDate,
			   c.LastUpdated
		  FROM Communication AS c, CommSelection AS cs
		 WHERE c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		   AND c.CommID = cs.CommID
	       AND c.sent = 0 <!--- false --->
</CFQUERY>


<CFSET downloadname = "address">
<CFIF getCommInfo.RecordCount IS NOT 0>
	<CFIF Trim(getCommInfo.Title) IS NOT "">
		<!--- remove all non-alpha and non-numeric characters--->
		<CFSET downloadname = REReplace(getCommInfo.Title,"[^a-z,A-Z,0-9]","","ALL")>
	</CFIF>
</CFIF>

<!--- we assume that LookupID 4 is download
		later in this template
		but if this is a download, then
		there will not be any records
		in CommFile
--->
<CFIF getCommInfo.CommFormLID IS NOT 4>

<!--- 	<CFQUERY NAME="getCommForm" datasource="#application.siteDataSource#">
		SELECT DISTINCT ll.ItemText, ll.LookupID
		  FROM LookupList AS ll, CommFile AS cf
		 WHERE ll.LookupID = cf.CommFormLID
		   AND cf.CommID = #frmCommID#
		ORDER BY ll.ItemText
	</CFQUERY> --->
	<CFQUERY NAME="getCommForm" datasource="#application.siteDataSource#">
		SELECT DISTINCT ll.ItemText, ll.LookupID
		  FROM LookupList AS ll, Communication AS c
		 WHERE ll.LookupID = c.CommFormLID
		   AND c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY ll.ItemText
	</CFQUERY>

<CFELSE>

	<!--- New Flags --->

	<!--- Get USER countries --->
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

	<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

	<!--- Find all Country Groups for this Country --->
	<!--- Note: no User Country rights implemented for site currently --->
	<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
	ORDER BY b.CountryID
	</CFQUERY>

	<CFSET UserCountryList = "0,#Variables.CountryList#">
	<CFIF getCountryGroups.CountryID IS NOT "">
		<!--- top record (or only record) is not null --->
		<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
	</CFIF>

<!--- NJH 2016/09/23 PROD2016-2383 - show flaggroups even if expiry not set --->
	<CFQUERY NAME="getFlagGroups" datasource="#application.sitedatasource#">
	SELECT
	FlagGroup.FlagGroupID AS Grouping,
	FlagGroup.FlagGroupID,
	FlagGroup.EntityTypeID,
	FlagGroup.Name,
	FlagGroup.Description,
	FlagGroup.FlagTypeID,
	FlagGroup.ParentFlagGroupID,
	FlagGroup.OrderingIndex AS parent_order,
	0 AS child_order,
	(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID) As flag_count
	FROM FlagGroup
	WHERE FlagGroup.Active <> 0
	AND (FlagGroup.expiry is null or FlagGroup.Expiry > #CreateODBCDateTime(Now())#)
	AND ParentFlagGroupID = 0
	AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
	AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
		OR (FlagGroup.DownloadAccessRights = 0 AND FlagGroup.Download <> 0)
		OR (FlagGroup.DownloadAccessRights <> 0
			AND EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID=#request.relayCurrentUser.personid#
				AND FlagGroupRights.Download <> 0)))
	UNION
	SELECT
	FlagGroup.ParentFlagGroupID,
	FlagGroup.FlagGroupID,
	FlagGroup.EntityTypeID,
	FlagGroup.Name,
	FlagGroup.Description,
	FlagGroup.FlagTypeID,
	FlagGroup.ParentFlagGroupID,
	p.OrderingIndex,
	FlagGroup.OrderingIndex,
	(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID)
	FROM FlagGroup, FlagGroup as p
	WHERE FlagGroup.Active <> 0
	AND (FlagGroup.expiry is null or FlagGroup.Expiry > #CreateODBCDateTime(Now())#)
	AND FlagGroup.ParentFlagGroupID <> 0
	AND p.ParentFlagGroupID = 0
	AND p.FlagGroupID = FlagGroup.ParentFlagGroupID
	AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
	AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
		OR (FlagGroup.DownloadAccessRights = 0 AND FlagGroup.Download <> 0)
		OR (FlagGroup.DownloadAccessRights <> 0
			AND EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID=#request.relayCurrentUser.personid#
				AND FlagGroupRights.Download <> 0)))
	ORDER BY EntityTypeID, parent_order, grouping, child_order, Name
</CFQUERY>


</CFIF>



<CFIF getCommInfo.RecordCount IS NOT 0>

	<!--- the below is also in commsend.cfm --->
	<CFIF #ListFind("5,6,7",getCommInfo.CommFormLID)# IS NOT 0>
		<!--- can't be reached by fax or email --->
		<CFSET TagName = " - invalid fax & email">
	<CFELSEIF getCommInfo.CommFormLID IS 2>
		<!--- can't be reached by fax --->
		<CFSET TagName = " - invalid email">
	<CFELSEIF getCommInfo.CommFormLID IS 3>
		<!--- can't be reached by fax --->
		<CFSET TagName = " - invalid fax">
	</CFIF>


<!--- =====================
	 qryCommCounts.cfm
	 ======================
	required inputs:
		a list of selectionIDs: Variables.selectIDlist
		the CommFormLID of the communication: Variables.thisCommFormLID
	 outputs (if record count is > 0):
	 	SelectTitleList (a comma separated list of all comm titles)
		TagPeople
		FeedbackCounts
--->
	<CFSET selectIDlist = ValueList(getCommInfo.SelectionID)>
	<CFSET thisCommFormLID = getCommInfo.CommFormLID>

	<CFINCLUDE TEMPLATE="qryCommCounts.cfm">


<!--- 	<CFQUERY NAME="getSelectionInfo" datasource="#application.siteDataSource#">
		SELECT s.Title,
			(SELECT count(p.personID) FROM Person AS p, SelectionTag AS st1 WHERE st1.SelectionID IN (#ValueList(getCommInfo.SelectionID)#) AND st1.EntityID = p.PersonID AND st1.Status <> 0 AND p.active <> 0) AS TagPeople,
			(SELECT count(p.personID) FROM Person AS p, SelectionTag AS st1 WHERE st1.SelectionID IN (#ValueList(getCommInfo.SelectionID)#) AND st1.EntityID = p.PersonID AND st1.Status <> 0 AND p.active <> 0 AND p.Email > '') AS emailpeople,
			(SELECT count(l.LocationID) FROM Location AS l, Person AS p, SelectionTag AS st
                    WHERE st.SelectionID IN (#ValueList(getCommInfo.SelectionID)#)
                      AND st.EntityID = p.PersonID
                         AND st.Status > 0
                         AND p.LocationID = l.LocationID
                         AND p.Active <> 0
                         AND l.Fax > '') AS faxpeople,
			(SELECT count(l.LocationID) FROM Location AS l, Person AS p, SelectionTag AS st
					WHERE st.SelectionID IN (#ValueList(getCommInfo.SelectionID)#)
					  AND st.EntityID = p.PersonID
					  AND st.Status > 0
					  AND p.LocationID = l.LocationID
					  AND p.Email > ''
					  AND p.Active <> 0
					  AND l.Fax > '') AS bothpeople
		  FROM Selection AS s
	     WHERE s.CreatedBy = #request.relayCurrentUser.usergroupid#
		   AND s.SelectionID IN (#ValueList(getCommInfo.SelectionID)#)
	  ORDER BY s.Title
	</CFQUERY> --->

</CFIF>





<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">
function setDate(form) {
	var day = form.frmDay.options[form.frmDay.selectedIndex].value;
	var month = form.frmMon.options[form.frmMon.selectedIndex].value;
	var year = form.frmYear.options[form.frmYear.selectedIndex].value;
	var datestring = day + "-" + month + "-" + year;
//	document.FormCommSend.frmSendDate.value= datestring;
//	document.FormDownload.frmSendDate.value= datestring;
	form.frmSendDate.value= datestring;

}
function checkForm() {
	var form = document.ThisForm;
	if (form.frmDay) {
		setDate(form);
	} else {
		// assume downloading
		var msg = "\n\nThe system needs to prepare a text file for you which may take a few moments.  \n\nPlease be patient.";
		alert(msg);
	}
	form.submit();

}
</SCRIPT>
</cf_head>

<FONT FACE="Arial, Chicago, Helvetica" SIZE="2">

<CFINCLUDE TEMPLATE="commtophead.cfm">

<CENTER>
<P><BR><BR>


<CFIF getCommInfo.RecordCount IS NOT 0>

	<CFSET DefaultSend = DateAdd("d",0,Now())>

	<CFIF getCommInfo.CommFormLID IS NOT 4>
		<FORM NAME="ThisForm" ACTION="commsend.cfm?RequestTimeout=500" METHOD="POST">
			<CF_INPUT TYPE="HIDDEN" NAME="frmSendDate" VALUE="#DateFormat(DefaultSend,"dd-mmm-yy")#">
	<CFELSE>
		<FORM NAME="ThisForm" ACTION="commdownload.cfm/<CFOUTPUT>#htmleditformat(downloadname)#</CFOUTPUT>.txt?RequestTimeout=900&ID=<CFOUTPUT>#RandRange(5,500)#</CFOUTPUT>" METHOD="POST">
	</CFIF>

	<CF_INPUT TYPE="HIDDEN" NAME="frmSelectID" VALUE="#frmSelectID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#frmCommID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(getCommInfo.LastUpdated)#">

	<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="3" CELLSPACING="0" WIDTH="320">

		<TR><TD COLSPAN="3" ALIGN="CENTER" BGCOLOR="#000099"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B>COMMUNICATION SUMMARY</B></FONT></TD></TR>



		<CFIF getCommInfo.CommFormLID IS NOT 4>
	 		<TR>
			<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<B>Date to send:</B>
				</FONT></TD>
				</TR>
			<TR><TD WIDTH=15><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					&nbsp;
				</FONT></TD>
				<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
								<SELECT NAME="frmMon" onChange="setDate(this.form);">
						<CFLOOP INDEX="cnt" FROM="1" TO="12">
						<CFOUTPUT><OPTION VALUE="#Left(MonthAsString(cnt),3)#"#IIF(Month(Variables.DefaultSend) IS cnt,DE(" SELECTED"),DE(""))#>#Left(MonthAsString(cnt),3)#</CFOUTPUT>
						</CFLOOP>
					</SELECT>
					<SELECT NAME="frmDay" onChange="setDate(this.form);">
						<CFLOOP INDEX="cnt" FROM="1" TO="31">
						<CFOUTPUT><OPTION VALUE="#cnt#"#IIF(Day(Variables.DefaultSend) IS cnt,DE(" SELECTED"),DE(""))#>#htmleditformat(cnt)#</CFOUTPUT>
						</CFLOOP>
					</SELECT>
					<SELECT NAME="frmYear" onChange="setDate(this.form);">
						<CFLOOP INDEX="cnt" FROM="1998" TO="2005">
						<CFOUTPUT><OPTION VALUE="#cnt#"#IIF(Year(Variables.DefaultSend) IS cnt,DE(" SELECTED"),DE(""))#>#htmleditformat(cnt)#</CFOUTPUT>
						</CFLOOP>
					</SELECT>
				</FONT></TD>
			</TR>
			<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=280 HEIGHT=3 ALT="" BORDER="0"><BR></CFOUTPUT></TD></TR>
		</CFIF>

		<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<B>Communication:</B>
			</FONT></TD>
		</TR>

		<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				Subject:
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<CFOUTPUT>#htmleditformat(getCommInfo.Title)#
			</FONT></TD>
		</TR>
		<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				Objective:
			</FONT></TD>
			<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				#htmleditformat(getCommInfo.Description)#
			</FONT></TD>
		</TR>
		<CFIF getCommInfo.CommFormLID IS NOT 4>
			<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					&nbsp;
				</FONT></TD>
				<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					Via:
				</FONT></TD>
			<!--- not "Download"
				add a space after each comma in the list of
				how the communication will be sent --->

				<TD VALIGN="TOP" ALIGN="LEFT"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					#htmleditformat(getCommForm.ItemText)#
<!--- 					#Replace(ValueList(getCommForm.ItemText),",",", ","ALL")#				 --->
				</FONT></TD>
			</TR>

		</CFIF>
		</CFOUTPUT>
		<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=280 HEIGHT=3 ALT="" BORDER="0"><BR></CFOUTPUT></TD></TR>
		<CFIF #Variables.SelectTitleList# IS NOT "">
			<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				<B>Selection(s):</B>
				</FONT></TD>
			</TR>
			<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
				&nbsp;
			</FONT></TD>
				<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					<CFOUTPUT>#htmleditformat(Variables.SelectTitleList)#
					<!--- tagPeople always exists --->

					<P>A total of #htmleditformat(Variables.TagPeopleCount)# individual(s) are tagged
					<BR><!--- feedback counts depends on type of communciation being sent --->
					#htmleditformat(Variables.FeedbackCounts)#

<!--- 					#Replace(ValueList(getSelectionInfo.Title), ",", ", ", "all")#
					<BR>#getSelectionInfo.tagPeople# Individual(s) Tagged
					<BR>
						<BR>#getSelectionInfo.emailPeople# Individual(s) with email addresses
						<BR>#getSelectionInfo.faxPeople# Individual(s) with fax numbers
						<BR>#getSelectionInfo.bothPeople# Individual(s) with both --->
					</UL>
					</CFOUTPUT>


				</FONT></TD>
			</TR>
			<CFIF getCommInfo.CommFormLID IS 4>
				<TR><TD COLSPAN="3" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=280 HEIGHT=3 ALT="" BORDER="0"><BR></CFOUTPUT></TD></TR>

				<TR><TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					<B>Flag(s):</B>
					</FONT></TD>
				</TR>
				<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					&nbsp;
					</FONT></TD>
				<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
					Use checkboxes below to select flag data required (if any).  The selection may
					not include any individuals with flag data.
					<BR><BR></FONT></TD>
				</TR>

			</CFIF>
		<CFELSE>
			<TR><TD COLSPAN="3" ALIGN="CENTER"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2><P>A related selection could not be found.<BR></FONT></TD></TR>
		</CFIF>

	</TABLE>

<CFIF getCommInfo.CommFormLID IS 4>   <!--- added by WAB 1998-12-28  --->
	<!--- New Flags --->
	<!--- Display choice of Flag Groups to download with selection --->
	<CFIF getFlagGroups.Recordcount IS 0>

		<P><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">There are no Flag Groups.</FONT>

	<CFELSE>

		<CFSET flagListPerson = "Group_0"> <!--- List of person flag groups displayed with checkboxes = with flags --->
		<CFSET flagListLocation = "Group_0"> <!--- List of location flag groups displayed with checkboxes = with flags --->

		<CFSET cnt = 0>
		<P><TABLE BORDER="0" CELLPADDING="5" CELLSPACING="0" WIDTH="90%">
		<TR><TD ALIGN="CENTER" VALIGN="BOTTOM"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><B>Type</B></FONT></TD>
			<TD ALIGN="CENTER" VALIGN="BOTTOM"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2">&nbsp;</FONT></TD>
			<TD VALIGN="BOTTOM"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><B>Name</B></FONT></TD>
			<TD VALIGN="BOTTOM"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><B>Description</B></FONT></TD>
		</TR>

		<CFOUTPUT QUERY="getFlagGroups" GROUP="grouping">

			<CFSET thisFlagGroup = FlagGroupID>
			<CFSET typeName = #ListGetAt(typeList,FlagTypeID)#>
			<CFSET typeData = #ListGetAt(typeDataList,FlagTypeID)#>

			<CFSET #cnt# = #cnt# + 1>

			<TR><TD ALIGN="CENTER" VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #EntityTypeID# IS 0><CFSET flagListPerson = flagListPerson & ",#typeName#_#thisFlagGroup#">P<CFELSE><CFSET flagListLocation = flagListLocation & ",#typeName#_#thisFlagGroup#">L</CFIF></FONT></TD>
				<TD ALIGN="CENTER" VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #flag_count# IS NOT 0><CF_INPUT TYPE="CHECKBOX" NAME="#typeName#_#thisFlagGroup#" VALUE="yes"><CFELSE>-</CFIF></FONT></TD>
				<TD VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF Name IS NOT "">#HTMLEditFormat(Name)#<CFELSE>No Name</CFIF></FONT></TD>
				<TD VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF Description IS NOT "">#HTMLEditFormat(Description)#<CFELSE>-</CFIF></FONT></TD>
			</TR>

			<CFOUTPUT>
				<CFIF getFlagGroups.ParentFlagGroupID IS NOT 0>

				<CFSET thisFlagGroup = FlagGroupID>
				<CFSET typeName = #ListGetAt(typeList,FlagTypeID)#>
				<CFSET typeData = #ListGetAt(typeDataList,FlagTypeID)#>
				<CFIF #EntityTypeID# IS 0>
					<CFSET flagListPerson = flagListPerson & ",#typeName#_#thisFlagGroup#">
				<CFELSE>
					<CFSET flagListLocation = flagListLocation & ",#typeName#_#thisFlagGroup#">
				</CFIF>

				<TR><TD ALIGN="CENTER" VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2">&nbsp;</FONT></TD>
					<TD ALIGN="CENTER" VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF #flag_count# IS NOT 0><CF_INPUT TYPE="CHECKBOX" NAME="#typeName#_#thisFlagGroup#" VALUE="yes"><CFELSE>-</CFIF></FONT></TD>
					<TD VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF Name IS NOT ""><LI>#HTMLEditFormat(Name)#<CFELSE><LI>No Name</CFIF></FONT></TD>
					<TD VALIGN="MIDDLE"<CFIF #cnt# MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>><FONT FACE="Arial,Chicago" SIZE="2"><CFIF Description IS NOT "">#HTMLEditFormat(Description)#<CFELSE>-</CFIF></FONT></TD>
					</TR>
				</CFIF>
			</CFOUTPUT>

		</CFOUTPUT>

		</TABLE>

		<CF_INPUT TYPE="HIDDEN" NAME="flagListPerson" VALUE="#flagListPerson#">
		<CF_INPUT TYPE="HIDDEN" NAME="flagListLocation" VALUE="#flagListLocation#">

	</CFIF>
</CFIF>  <!--- added by WAB 1998-12-28 --->

	<P><TABLE BORDER=0 WIDTH="90%">
	<CFIF getCommInfo.CommFormLID IS NOT 4>
	<TR><TD VALIGN="BASELINE"><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
		When you select &quot;send&quot; the list of tagged individuals will
		be frozen and the the communication will be queued to be sent.

		<P>Once a communication is queued it is not possible to change the (tagged) individuals who are to receive the communication.  However, until it is sent, you
		may update/add fax and email details to ensure that a greater percentage of the tagged individuals will be reached.
		Selecting &quot;send&quot; will also create a selection containing all the individuals with an invalid email address &/or and invalid fax number so that you may easily update their details.
		This selection will have the name &quot;<CFOUTPUT>#htmleditformat(getcommInfo.Title)##HTMLEditFormat(Variables.TagName)#</CFOUTPUT>&quot;.
		</FONT></TD></TR>
	</CFIF>
	<TR><TD VALIGN="BASELINE" ALIGN="CENTER">
		<CFIF getCommInfo.CommFormLID IS 4>
			<!--- download --->
				<A HREF="JavaScript:checkForm();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_down_e.gif" WIDTH=105 HEIGHT=21 BORDER=0 ALT="btnDownload"></A>
		<CFELSE>
			<!--- not download --->
				<A HREF="JavaScript:checkForm();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_send_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Send Communication"></A>
		</CFIF>
		</TD>
	</TR>
	</TABLE>

<CFELSE>
	<CENTER>
	<P>The requested communication could not be found.
	</CENTER>
</CFIF>

</FORM>

</CENTER>

</FONT>




