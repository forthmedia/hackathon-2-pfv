<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid  (effectively bypasses this screen)
						and to run in a popup window
		2005-05-25 WAB get List of selections from cfc

--->



<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<CFIF NOT #checkPermission.AddOkay# GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="commmenu.cfm">
	<CF_ABORT>
</CFIF>


<!--- the application .cfm checks for communication privleges but
		we also need to check for select privledges
		we can rerun the qrycheckperms but NOTE we have now lost
		the application .cfm query variables.
--->
<CFSET securitylist = "SelectTask:read">
<CFINCLUDE TEMPLATE="../templates/qrycheckperms.cfm">

<CFQUERY NAME="getCommInfo"  datasource="#application.siteDataSource#">
	SELECT CommID, LastUpdated
	FROM Communication 
	WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<CFPARAM NAME="frmpersonids" DEFAULT="">   <!--- if list of people already defined, don't need to ask for a selection--->

<CFIF frmpersonids is not "">

	<CFSET frmLastUpdated = #CreateODBCDateTime(getCommInfo.LastUpdated)#>

	<CFINCLUDE Template="commconfirm.cfm">
	<CF_ABORT>
</CFIF>


<!--- <CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
	SELECT s.Title, s.SelectionID, s.Description, s.Created AS datecreated
      FROM Selection AS s
     WHERE s.CreatedBy = #request.relayCurrentUser.usergroupid#
  ORDER BY s.Title
</CFQUERY> --->
<!--- allow any selection to which one has sharing rights --->
<!--- 
WAB 2005-05-25 replaced with call to cfc

<CFQUERY NAME="getSelections" datasource="#application.siteDataSource#">
	SELECT s.Title, s.SelectionID, s.Description, s.Created AS datecreated
      FROM Selection AS s, SelectionGroup AS sg
     WHERE sg.SelectionID = s.SelectionID
	   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#	 
  ORDER BY s.Title
</CFQUERY> --->

<cfset getSelections = application.com.selections.getSelections()>




<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">
<!-- 
	function checkForm(){
		var form=document.ThisForm;
		var msg = "";
		if (form.frmSelectID.selectedIndex != -1) {
			if (form.frmSelectID.options[form.frmSelectID.selectedIndex].value == 0) {
				msg += "\n\nYou must choose a selection.";
			}
		} else {
			msg += "\n\nYou must choose a selection.";
		}
		if (msg != "") {
			alert(msg);
		} else {
			form.submit();
		}
	}
//-->
</SCRIPT>
</cf_head>


<CFINCLUDE TEMPLATE="commtophead.cfm">


<P><CENTER><TABLE BORDER="0"><TR><TD ALIGN="CENTER"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">


<TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0">
<TR>

<TD VALIGN="TOP" ALIGN="CENTER"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">
<CFIF getSelections.RecordCount GT 0 AND getCommInfo.RecordCount GT 0>
	<FORM NAME="ThisForm" ACTION="commconfirm.cfm?mode=debug" METHOD="POST">
	
	<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#getCommInfo.CommID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(getCommInfo.LastUpdated)#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
	
		Highlight Selections to send to:
			 <P><SELECT NAME="frmSelectID" SIZE="<CFIF getSelections.RecordCount GT 10>10<CFELSE><CFOUTPUT>#getSelections.RecordCount#</CFOUTPUT></CFIF>" MULTIPLE>
				<CFOUTPUT QUERY="getSelections">
					<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #htmleditformat(Title)#
				</CFOUTPUT>
			</SELECT>
		<P><A HREF="JavaScript:checkForm();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_continue_e.gif" WIDTH=105 HEIGHT=21 BORDER=0 ALT="Continue"></A>
	
 		<FORM ACTION="JavaScript:alert('\n\nYou can currently only add selections via the Selections menu on the left.')" METHOD="POST">
<!--- 		<TD VALIGN="TOP">&nbsp;
		<CFIF #checkPermission.AddOkay# GT 0>
		<!--- NOTE this iteration of checkPermissions is not
				the one in application .cfm but the one at the top of the screen --->
		<P><A HREF="JavaScript:checkForm();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_createnew_e.gif" WIDTH=122 HEIGHT=22 BORDER=0 ALT="New"></A>
		</FORM>
	<CFELSE>
		&nbsp;
	</CFIF>	 
		</TD>---><TABLE WIDTH="70%" BORDER="0"><TR><TD>
		<P><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">
		If a person is tagged in any of the selections highlighted,
		they will receive a communication, whether or not they are untagged in any other selection.
		</FONT></TD></TR></TABLE></TD>
	</TR>
	</TABLE>

	</FORM>
<CFELSE>
	
	<P>Sorry, you have no selections.
				
</CFIF>

</FONT></TD></TR>
</TABLE>
</CENTER>

</FONT></TD></TR>
</TABLE></CENTER>


</FONT>


