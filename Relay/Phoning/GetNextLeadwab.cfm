<!--- �Relayware. All Rights Reserved 2014 --->
<CFHEADER NAME="Expires" VALUE="Wed, 26 Feb 1997 08:21:57 GMT">
<CFHEADER NAME="Pragma" VALUE="no-cache">
<CFHEADER NAME="Cache-Control" VALUE="no-cache">

<CFSET minTimeBetweenCalls = 2000 >
<CFSET daysCallBacksStayPrivate = 4 >    <!--- in other words, if you don't do a callback within this time, it goes back into the pot --->

<!--- decipher any parameters passed in frmGlobalParameters --->
<!--- namely campaigncommid --->
<CFINCLUDE template="..\screen\setParameters.cfm">

<!--- get campaignDetails, takes campaignCommID --->
<CFINCLUDE template="qryCampaignDetails.cfm">

<!--- 2012-07-26 PPB P-SMA001 commented out
<CFINCLUDE template="..\templates\qrygetCountries.cfm">
--->

<!--- check for records locked for too long --->




<!--- check for records already locked by this user but with last call date < lockdate--->
<!--- ie record locked by no call result set --->
<CFQUERY NAME="getThisUserLocks" DATASOURCE="#application.SiteDataSource#" >
Select PhoneActionID, sitename
From PhoneAction, location
Where PhoneActionLock = 1
AND PhoneActionCompleted = 0
AND PhoneActionLockBy = #request.relayCurrentUser.usergroupid#
AND (PhoneActionLockDate > LastCallDate OR LastCallDate is null)
AND CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" >
AND location.locationid = PhoneAction.locationid

</cfquery>

<CFIF getThisuserLocks.recordcount IS NOT 0>

	<CFQUERY NAME="deleteThisUserLocks" DATASOURCE="#application.SiteDataSource#" >
	UPDATE PhoneAction
	SET  PhoneActionLock = 0
	Where PhoneActionLock = 1
	AND PhoneActionLockBy = #request.relayCurrentUser.usergroupid#
	AND (PhoneActionLockDate > LastCallDate OR LastCallDate is null)
	AND CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND PhoneActionCompleted = 0
	</cfquery>

	<CFSET message = "Your previous call to #getThisUserLocks.sitename# was not completed">
	<CFSET message = urlencodedformat(message)>

</cfif>


<!--- deletes any records locked by other users for more than x hrs --->
<CFQUERY NAME="deleteAnyUserLocks" DATASOURCE="#application.SiteDataSource#" >
UPDATE PhoneAction
SET  PhoneActionLock = 0
Where PhoneActionLock = 1
AND PhoneActionCompleted = 0
AND (PhoneActionLockDate > LastCallDate OR LastCallDate is null)
AND PhoneActionLockDate <  #createODBCdatetime(now() - (4/24))#
AND CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>


<!--- wab trying out a new structure

<CFQUERY NAME="Leads" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
SELECT  LastCallUser, LocationID, PhoneActionLock, CommID, PhoneActionID,
CASE WHEN 		LastCallUser = #request.relayCurrentUser.usergroupid#
			AND PersonID > 0
			AND NextCallDate < #now()#
			AND NextCallDate >= #createdate(year(now()), month(now()), day(now()))#
	THEN    1
	WHEN  LastCallUser = #request.relayCurrentUser.usergroupid#
			AND NextCallDate < #now()#
			AND NextCallDate > #createdate(year(now()), month(now()), day(now()))#
	THEN   2
	WHEN	LastCallUser = #request.relayCurrentUser.usergroupid#
				AND PersonID > 0
				AND NextCallDate < #createdate(year(now()), month(now()), day(now()))#
	THEN 3
	ELSE 4 END as sortorder


FROM         PhoneAction
WHERE 		CommID = #campaigncommid#
		AND	PhoneActionCompleted = 0
		AND PhoneActionLock = 0

	ORDER BY  sortorder, NextCallDate
</CFQUERY>

--->

<!--- First pass:
	Find records where
	This user has set a previous call back with a specific person for today --->
<CFQUERY NAME="FirstGetNextLead" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
SELECT  LastCallUser, phoneaction.LocationID, PhoneActionLock, CommID, PhoneActionID
FROM         PhoneAction, location
WHERE       (LastCallUser = #request.relayCurrentUser.usergroupid#)
	AND (PersonID > 0)
	AND (PhoneActionLock = 0)
	AND PhoneActionCompleted = 0
	AND (CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" > )
	AND (NextCallDate <  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
	AND (NextCallDate >=  <cf_queryparam value="#createdate(year(now()), month(now()), day(now()))#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
	AND location.locationid = phoneaction.locationid
	<!--- 2012-07-26 PPB P-SMA001 commented out
	AND location.countryid  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 --->
	#application.com.rights.getRightsFilterWhereClause(entityType="PhoneAction", alias="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
	ORDER BY  NextCallDate
</CFQUERY>
<CFIF #FirstGetNextLead.RecordCount# IS 1> <!--- First IF --->
	<CFSET frmLocationID = #FirstGetNextLead.LocationID#>
	<CFSET frmPhoneActionID = #FirstGetNextLead.PhoneActionID#>
	<CFSET GetNextLeadPass = 1>
<CFELSE>
	<!--- Second Pass:
	We did not find any records in the query above so we're doing a second pass
	to find records where:
	This user has previously phoned and set a general call back for today
	but without speaking to a specific person --->
	<CFQUERY NAME="SecondGetNextLead" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
	SELECT  LastCallUser, phoneaction.LocationID, PhoneActionLock, CommID, PhoneActionID
	FROM         PhoneAction, location
	WHERE       (LastCallUser = #request.relayCurrentUser.usergroupid#)
		AND (PhoneActionLock = 0)
		AND PhoneActionCompleted = 0
		AND (CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" > )
		AND (NextCallDate <  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
		AND (NextCallDate >  <cf_queryparam value="#createdate(year(now()), month(now()), day(now()))#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
		AND location.locationid = phoneaction.locationid
		<!--- 2012-07-26 PPB P-SMA001 commented out
		AND location.countryid  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		 --->
		#application.com.rights.getRightsFilterWhereClause(entityType="PhoneAction", alias="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
		ORDER BY NextCallDate Asc
	</CFQUERY>
		<CFIF #SecondGetNextLead.RecordCount# IS 1> <!--- Second IF --->
			<CFSET frmLocationID = #SecondGetNextLead.LocationID#>
			<CFSET frmPhoneActionID = #SecondGetNextLead.PhoneActionID#>
			<CFSET GetNextLeadPass = 2>
		<CFELSE>
			<!--- Third pass find records where:
			The lead has not been phoned by this user but there is a missed call back date
			on a previous day --->
			<CFQUERY NAME="ThirdGetNextLead" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
			SELECT   LastCallUser, phoneaction.LocationID, PhoneActionLock, CommID, PhoneActionID
			FROM         PhoneAction, location
			WHERE (LastCallUser = #request.relayCurrentUser.usergroupid#)
				AND (PersonID > 0)
				AND (PhoneActionLock = 0)
				AND PhoneActionCompleted = 0
				AND (CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" > )
				AND (NextCallDate <  <cf_queryparam value="#createdate(year(now()), month(now()), day(now()))#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
				AND location.locationid = phoneaction.locationid
				<!--- 2012-07-26 PPB P-SMA001 commented out
				AND location.countryid  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				 --->
				#application.com.rights.getRightsFilterWhereClause(entityType="PhoneAction", alias="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
				ORDER BY  NextCallDate Asc
			</CFQUERY>
			<CFIF #ThirdGetNextLead.RecordCount# IS 1> <!--- Third IF --->
					<CFSET frmLocationID = #ThirdGetNextLead.LocationID#>
					<CFSET frmPhoneActionID = #ThirdGetNextLead.PhoneActionID#>
					<CFSET GetNextLeadPass = 3>
			<CFELSE>
								<!--- Fourth pass find records where:
			phoneAction.LastCallerID = #request.relayCurrentUser.usergroupid#
			phoneAction.CallBackTime LT now()--->
				<CFQUERY NAME="FourthGetNextLead" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
				SELECT      LastCallUser, phoneaction.LocationID, PhoneActionLock, CommID, PhoneActionID
				FROM         PhoneAction, location
				WHERE       (LastCallUser = #request.relayCurrentUser.usergroupid#)
					AND (PhoneActionLock = 0)
					AND PhoneActionCompleted = 0
					AND (CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" > )
					AND (NextCallDate <  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
					AND location.locationid = phoneaction.locationid
					<!--- 2012-07-26 PPB P-SMA001 commented out
					AND location.countryid  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					 --->
					#application.com.rights.getRightsFilterWhereClause(entityType="PhoneAction", alias="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
					ORDER BY  NextCallDate
				</CFQUERY>
					<CFIF #FourthGetNextLead.RecordCount# IS 1> <!--- Fourth IF --->
						<CFSET frmLocationID = #FourthGetNextLead.LocationID#>
						<CFSET frmPhoneActionID = #FourthGetNextLead.PhoneActionID#>
						<CFSET GetNextLeadPass = 4>
					<CFELSE>
						<!--- 5th pass find records where:
						 WAB altered this so that other people's call backs are only brought up if they are overdue by x
							--->
						<CFQUERY NAME="FifthGetNextLead" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
						SELECT      LastCallUser, phoneaction.LocationID, PhoneActionLock, CommID, PhoneActionID
						FROM         PhoneAction, location
						WHERE (PhoneActionLock = 0)
							AND PhoneActionCompleted = 0
							AND (CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" > )
							AND (NextCallDate <  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  - #daysCallBacksStayPrivate#)
							AND location.locationid = phoneaction.locationid
							<!--- 2012-07-26 PPB P-SMA001 commented out
							AND location.countryid  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
							 --->
							#application.com.rights.getRightsFilterWhereClause(entityType="PhoneAction", alias="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
							ORDER BY  NextCallDate
						</CFQUERY>
						<CFIF #FifthGetNextLead.RecordCount# IS 1> <!--- Fifth IF --->
							<CFSET frmLocationID = #FifthGetNextLead.LocationID#>
							<CFSET frmPhoneActionID = #FifthGetNextLead.PhoneActionID#>
							<CFSET GetNextLeadPass = 5>
						<CFELSE>
							<!--- 6th pass find records where:
						completely untried lead --->
							<CFQUERY NAME="SixthGetNextLead" DATASOURCE="#application.SiteDataSource#" MAXROWS=1>
							SELECT      LastCallUser, phoneaction.LocationID, PhoneActionLock, CommID, PhoneActionID, datediff(minute,lastcalldate,#now()#) as diff, nextcalldate, lastcalldate,
							CASE WHEN 	ISNULL(lastcalldate, '') = '' THEN 1
								ELSE  2 END as sortOrder						<!--- this should bring completely untried records back before engaged etc. --->

							FROM         PhoneAction, location
							WHERE       (PhoneActionLock = 0)
								AND PhoneActionCompleted = 0
								AND (CommID =  <cf_queryparam value="#campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" > )
								AND ISNULL(NextCallDate, '') = ''
								AND (ISNULL(lastcalldate, '') = '' or datediff(minute,lastcalldate,#now()#) >  <cf_queryparam value="#minTimeBetweenCalls#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  )   <!--- this 30 needs to be a variable --->
								AND location.locationid = phoneaction.locationid
								<!--- 2012-07-26 PPB P-SMA001 commented out
								AND location.countryid  in ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
								 --->
								#application.com.rights.getRightsFilterWhereClause(entityType="PhoneAction", alias="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
								Order by sortOrder
							</CFQUERY>
<!--- 							<CFOUTPUT>diff: #SixthGetNextLead.diff#  next: #SixthGetNextLead.nextcalldate# last: #SixthGetNextLead.lastcalldate#</cfoutput> --->
							<CFSET frmLocationID = #SixthGetNextLead.LocationID#>
							<CFSET frmPhoneActionID = #SixthGetNextLead.PhoneActionID#>
							<CFSET GetNextLeadPass = 6>

						</CFIF> <!--- Close Fifth IF --->

					</CFIF> <!--- Close Fourth IF --->

			</CFIF> <!--- Close Third IF --->

		</CFIF> <!--- Close Second IF --->

</CFIF> <!--- Close First IF --->


<!--- for debugging
<CFIF request.relayCurrentUser.usergroupid IS 84>
	<CF_ABORT>
</cfif>
 --->


<CFIF frmPhoneActionID IS NOT ''>
<!--- Lock the PhoneAction record so nobody else can grab it --->
	<CFQUERY NAME="LockPhoneAction" datasource="#application.siteDataSource#">
		UPDATE     dbo.PhoneAction
		SET             PhoneActionLock=1,
						PhoneActionLockDate =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
						PhoneActionLockBy = #request.relayCurrentUser.usergroupid#
		WHERE       (PhoneActionID =  <cf_queryparam value="#frmPhoneActionID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</CFQUERY>
	<!--- Set the cookies for Outbound phoning --->
	<cfset application.com.globalFunctions.cfcookie(NAME="PhoneActionID", VALUE="#frmPhoneActionID#")>
<!--- 	<cfset application.com.globalFunctions.cfcookie(NAME="CommID" VALUE="#CommID#")> --->
	<!--- Set variables for the viewer to work --->
<!--- 	<cfset frmCurrentEntityId="#frmLocationID#"> --->
	<cfset frmEntityId="#frmLocationID#">
	<cfset frmSelectedId="#frmLocationID#">
<!--- 	<CFSET frmEntityType= "Location"> --->
	<CFSET frmCurrentScreenID = preferredStartScreen>
<!--- 	<CFSET frmGlobalParameters="CampaignCommid=#campaigncommid#"> --->


<CFELSE>
	<H2>There are no records left to phone for this campaign.</H2>
	<CF_ABORT>
</CFIF>

















