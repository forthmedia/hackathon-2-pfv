<!--- �Relayware. All Rights Reserved 2014 --->

<CFQUERY NAME="GetOutboundCommIDs" datasource="#application.siteDataSource#">
	SELECT DISTINCT 
	    PhoneCommRecords.CommTitle,
	    PhoneCommRecords.CommID,
	        (SELECT COUNT(*) FROM dbo.PhoneAction
		      WHERE dbo.PhoneAction.CommID = dbo.PhoneCommRecords.CommID
			  AND PhoneAction.PhoneActionLock=0
			  AND PhoneAction.PhoneActionCompleted=0)
	     AS NumRecsLeft, 
			 (SELECT COUNT(*) FROM dbo.PhoneAction
		      WHERE dbo.PhoneAction.CommID = dbo.PhoneCommRecords.CommID)
	     AS NumRecsTotal, 
		PhoneCommRecords.CallType, 
	    LookupList.ItemText AS CommType
	FROM PhoneCommRecords INNER JOIN LookupList ON 
	    PhoneCommRecords.CommFormLID = LookupList.LookupID LEFT
	     OUTER JOIN PhoneAction ON 
	    PhoneCommRecords.CommID = PhoneAction.CommID
	WHERE dbo.PhoneCommRecords.CallType = 61
</CFQUERY>






<DIV ALIGN="center">
<CFIF #GetOutboundCommIDs.RecordCount# GT 0>
	<H2>Please select an outbound campaign to phone on from the list below</H2>
	<TABLE>
		<TR>
			<TD></TD>
			<TH>Campaign</TH>
			<TH>Records left <BR> to phone</TH>
			<TH>Total Records <BR>in List</TH>
		</TR>
		<CFOUTPUT QUERY="GetOutboundCommIDs">
			<TR>
				<TD><CFIF #NumRecsLeft# GT 0>
						<A HREF="/Phoning/GetNextLead.cfm?campaignCommID=#CommID#" TARGET="_parent">
						<IMG SRC="../images/BUTTONS/c_continue_e.gif" BORDER=0 ALT="Phone records in #CommTitle#"></A>
					</CFIF>
				</TD>
				<TD>#htmleditformat(CommTitle)#</TD>
				<TD>#htmleditformat(NumRecsLeft)#</TD>
				<TD>#htmleditformat(NumRecsTotal)#</TD>
			</TR>	
		</CFOUTPUT>
	</TABLE>
<CFELSE>
	<H2>There are currently no records set up for you to phone on.  Please define a campaign first.</H2>
</CFIF>

</DIV>










