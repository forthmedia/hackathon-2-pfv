<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Ammendments
2001-07-17  SWJ	Changed the listing screen to add features
2001-10-25	CPS	Modified views to make use of new attributes commtypeid, commreasonid and commstatusid
--->

<CFPARAM name = "statisticsCriteria" default = "3">
<CFPARAM name = "frmCountryId" default = "0,All Countries">
<CFPARAM name = "datefrom" default = "">
<CFPARAM name = "dateto" default = "">

<!---Screen One--->


<!---Get authorized countries--->
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
	
<!---Get the country names for the combo box display (only countries for which there are records)--->
	<CFQUERY NAME="getPhoneCountry" datasource="#application.siteDataSource#">
		SELECT DISTINCT Country.CountryDescription, Country.CountryID
		FROM Country INNER JOIN 
		(commdetail INNER JOIN Location ON commdetail.LocationID = Location.LocationID) 
		ON Country.CountryID = Location.CountryID
		WHERE Country.CountryID  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		AND CommDetail.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		ORDER BY CountryDescription
	</CFQUERY>
	
	
<cfset subsection = "Call Statistics">

<FORM NAME="PhoneStats" ACTION="PhoneStats.cfm" METHOD="post">
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td colspan="5">
	Please choose a Statistics Criteria, Country and the Required Date Range if you want to filter records
	</td>
</tr>
<tr>
	<td>
		<!---Pass CommID to the next phase--->
		<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="CommID" VALUE="#CommID#"></CFOUTPUT>
		<SELECT NAME="StatisticsCriteria">
		<OPTION VALUE=2 <CFIF #StatisticsCriteria# EQ 2>SELECTED</CFIF>>Analyze Contact History
		<OPTION VALUE=3 <CFIF #StatisticsCriteria# EQ 3>SELECTED</CFIF>>Analyze Call List
		</SELECT>
	</td>
	<td>
		<SELECT NAME="frmCountryID">
		<OPTION VALUE="0,All Countries" <CFIF ListFirst(frmCountryID) EQ 0>SELECTED</CFIF>>All Countries
		<CFOUTPUT QUERY="getPhoneCountry"><OPTION VALUE="#CountryID#,#CountryDescription#" <CFIF ListFirst(frmCountryID) EQ CountryID>SELECTED</CFIF>>#htmleditformat(CountryDescription)#</CFOUTPUT>
		</SELECT>
	</td>
	<td>
		<CFOUTPUT>From:<INPUT TYPE="text" NAME="DateFrom" SIZE="10" <CFIF IsDefined("DateFrom")><CFIF #DateFrom# IS NOT ''>value=#htmleditformat(form.DateFrom)#</CFIF></CFIF>>
	</td>
	<td>
		To:<INPUT TYPE="text" NAME="DateTo" SIZE="10" <CFIF IsDefined("DateTo")><CFIF #DateTo# IS NOT ''>value=#htmleditformat(form.DateTo)#</CFIF></CFIF>></CFOUTPUT>
	</td>
	<td>
		<INPUT TYPE="submit" NAME="StatSubmit" VALUE="  Go  ">
	</td>
</tr>
</table>
</FORM>


<!---Catch the New User for the Pie Copying--->
<CFSET Usr = #request.relayCurrentUser.usergroupid#>
<!---On the First Hit Delete the previously formed gif file by the same user after checking for it's existence--->
<!--- 	<CFIF NOT IsDefined("StatisticsCriteria")>
 --->		
<CFDIRECTORY ACTION="LIST" DIRECTORY="#Path#\TempGraphics" FILTER="#Usr#_*.gif" NAME="MyFiles">
<CFIF MyFiles.RecordCount NEQ 0>
	<CFLOOP QUERY="MyFiles">
		<CFIF fileExists("#Path#\TempGraphics\#Name#")>
			<CFFILE ACTION="DELETE" FILE="#Path#\TempGraphics\#Name#">
		</cfif>
	</CFLOOP>
</CFIF>
<!--- </CFIF> --->
<!---Results Page--->


<!---Contact History--->
<CFIF #StatisticsCriteria# IS 2>
	<CFQUERY NAME="TotalCalls" datasource="#application.siteDataSource#">
		SELECT COUNT(PhoneActionID) AS Total, CallType 
		FROM PhoneAction, PhoneCommRecords
	    WHERE PhoneAction.CommID = PhoneCommRecords.CommID
		AND PhoneAction.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		GROUP BY CallType
	</CFQUERY>
						
	<CFQUERY NAME="TotalDone" datasource="#application.siteDataSource#">
		Select Count(*) AS TotalCalls, 
		(select count(distinct locationid) from commdetail as o where commdetail.commid = o.commid ) as locationscount
		FROM CommDetail, Location, Country
		WHERE CommDetail.LocationID = Location.LocationID
		AND Location.CountryID = Country.CountryID
	
		AND CommDetail.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		
		<CFIF ListGetAt(frmCountryID, 1) NEQ 0>
			AND Location.CountryID =  <cf_queryparam value="#ListGetAt(form.frmCountryID, 1)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		
		<CFIF #DateFrom# IS NOT "" AND #DateTo# IS NOT "">
			AND Commdetail.DateSent >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND Commdetail.DateSent <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfif>
		
		GROUP BY CommID
	</CFQUERY>
			
	<CFIF TotalDone.RecordCount IS NOT 0>
		<CFSET TotalPhoning = #TotalDone.TotalCalls#>
	<CFELSE>
		<CFSET TotalPhoning = 0>
	</CFIF>
			
	<CFQUERY NAME="GetPhoneAnalysisData" datasource="#application.siteDataSource#">
		SELECT Communication.Title AS Campaign, Count(CommDetail.CommStatusId) AS CountOutcome, 
		cds.Name, Communication.CommID, Fraction=(Convert(Real, Count(CommDetail.CommStatusId))/#TotalPhoning#)*100

		FROM (commDetailStatus cds INNER JOIN (commdetail INNER JOIN Communication ON commdetail.CommID = Communication.CommID) 
		ON cds.commStatusID = commdetail.CommStatusId) LEFT JOIN Location ON commdetail.LocationID = Location.LocationID

<!--- 		SELECT Communication.Title AS Campaign, Count(CommDetail.CommStatus) AS CountOutcome, 
		Flag.Name, Communication.CommID, Fraction=(Convert(Real, Count(CommDetail.CommStatus))/#TotalPhoning#)*100

		FROM (Flag INNER JOIN (commdetail INNER JOIN Communication ON commdetail.CommID = Communication.CommID) 
		ON Flag.FlagID = commdetail.CommStatus) LEFT JOIN Location ON commdetail.LocationID = Location.LocationID
 --->	
		WHERE Communication.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		
<!--- 		<CFIF ListGetAt(frmCountryID, 1) NEQ 0>
			AND Location.CountryID = #ListGetAt(form.frmCountryID, 1)#
		</CFIF>
 --->		
		<CFIF #DateFrom# IS NOT "" AND #DateTo# IS NOT "">
		AND CommDetail.DateSent >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND CommDetail.DateSent <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfif>
		
		GROUP BY Communication.Title, Flag.Name, Communication.CommID
		ORDER BY Communication.Title
	</CFQUERY>
		
	<CFIF GetPhoneAnalysisData.RecordCount IS 0>
		No data present for this analysis. Please try changing Search Criteria.
	<CFELSE>
		<!---Define the pie--->	
		<CFOUTPUT>
		<CFSET TimeStamp = TimeFormat(Now(), "hhmmss")>
		
		<CFX_GIFGD ACTION="DRAW_PIE"
			OUTPUT="#path#\TempGraphics\#Usr#_#TimeStamp#.gif"
			LEGENDS="#ValueList(GetPhoneAnalysisData.Name)#"
			DATA="#ValueList(GetPhoneAnalysisData.Fraction)#"
			SIZE="275"
			MAXROWS="15"
			HEADER="Contact History Analysis for #ListGetAt(frmCountryID, 2)#"
			FOOTER= "Campaign: #getPhoneAnalysisData.Campaign# (#DateFormat(Now(),'dd-mmm-yyyy')#, #TimeFormat(Now(),'HH:mm')#)"> <!--- NJH 2006-10-02 hh:mm tt --->
		</CFOUTPUT>
		
		
		<!---Display the analysis--->
		<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
		<tr>
			<td class="label">
				Analysis of Phone Records for <CFOUTPUT>#ListGetAt(frmCountryID, 2)#</CFOUTPUT>
			</td>
		</tr>
			<!---<CFIF ManagerOK>
				<TR>
					<TD COLSPAN="3"><A HREF="JavaScript:emailStats();">E-mail these stats to a 3Com user</A></TD>
				</TR>
			</CFIF>--->
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<TR>
			<TD COLSPAN="3">Campaign: <CFOUTPUT><B>#htmleditformat(getPhoneAnalysisData.Campaign)#</B></CFOUTPUT></TD>
		</TR>
		<CFIF #TotalCalls.CallType# EQ 61>
			<TR>
				<TD COLSPAN="3">Total Locations: <CFOUTPUT><B>#htmleditformat(TotalCalls.Total)#</B></CFOUTPUT></TD>
			</TR>
		</cfif>
		<TR>
			<TD COLSPAN="3">Total Phone Calls <CFIF #TotalCalls.CallType# EQ 61>Made<CFELSE>Recieved</CFIF>: <CFOUTPUT><B>#htmleditformat(TotalDone.TotalCalls)#</B> to <B>#htmleditformat(TotalDone.LocationsCount)#</B></CFOUTPUT> Locations</TD>
		</TR>
		
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<TR>
			<Th WIDTH="200">Call Outcome</Th>
			<Th WIDTH="100">Numbers</Th>
			<!---If Outbound Calls--->		
			<CFIF #TotalCalls.CallType# EQ 61>
				<Th WIDTH="100">%</Th>
			</CFIF>
		</TR>
	
		<CFOUTPUT QUERY="getPhoneAnalysisData">
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<!---<CFIF ManagerOK AND ListFind(Allowed, FlagID) GT 0>
					<TD WIDTH="200"><A HREF="eventLists.cfm?FlagID=#FlagID#&countryid=#ListFirst(frmCountryID)#&filter=#URLEncodedFormat(frmAnalysis)#&criteria=#URLEncodedFormat(Analysis)#">#Analysis#</A></TD>
					<CFELSE>
					</CFIF>--->
				
				<TD WIDTH="100">#htmleditformat(Name)#</TD>
				<TD WIDTH="20" align="right">#htmleditformat(CountOutcome)#</TD>
				
				<!---If Outbound Calls--->	
				<CFIF #TotalCalls.CallType# EQ 61>
					<TD WIDTH="20" align="right">#DecimalFormat(Fraction)#</TD>
				</CFIF>
			</TR>
		</CFOUTPUT>
			
			
		<!---Display the pie--->
		<TR>
			<td colspan="3" align="center"><BR><cfoutput><IMG SRC="..\TempGraphics\#Usr#_#TimeStamp#.gif" BORDER="0"></cfoutput></td>
		</TR>
		</TABLE>
	</CFIF>
<!---Call List--->
<CFELSE>	
	<CFQUERY NAME="TotalCalls" datasource="#application.siteDataSource#">
		SELECT COUNT(PhoneActionID) AS Total, CallType 
		FROM PhoneAction, PhoneCommRecords
	    WHERE PhoneAction.CommID = PhoneCommRecords.CommID
		AND PhoneAction.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		GROUP BY CallType
	</CFQUERY>
	
	<CFQUERY NAME="TotalLeft" datasource="#application.siteDataSource#">
		SELECT COUNT(PhoneActionID) AS CallsLeftToPhone 
		FROM PhoneAction, PhoneCommRecords 
           WHERE PhoneAction.CommID = PhoneCommRecords.CommID
           AND PhoneAction.PhoneActionCompleted=0
           AND PhoneAction.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	<CFQUERY NAME="TotalDone" datasource="#application.siteDataSource#">
		Select Count(*) AS TotalCalls
		FROM PhoneAction, Location, Country
		WHERE PhoneAction.LocationID = Location.LocationID
		AND Location.CountryID = Country.CountryID
		
		AND PhoneAction.PhoneActionCompleted = 1
		AND PhoneAction.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		
		<CFIF ListGetAt(frmCountryID, 1) NEQ 0>
			AND Location.CountryID =  <cf_queryparam value="#ListGetAt(form.frmCountryID, 1)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		
		<CFIF #DateFrom# IS NOT "" AND #DateTo# IS NOT "">
			AND PhoneAction.LastCallDate >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND PhoneAction.LastCallDate <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfif>
	</CFQUERY>
		
	<CFSET TotalPhoning = #TotalCalls.Total#>
		
		
	<CFQUERY NAME="GetPhoneAnalysisData" datasource="#application.siteDataSource#">
		SELECT cds.Name, cds.commStatusId as flagid, Count(PhoneAction.LastCallResult) AS CountOutcome, 
		Communication.Title AS Campaign, Fraction=(Convert(Real, Count(PhoneAction.LastCallResult))/#TotalPhoning#)*100
		FROM ((commDetailStatus cds INNER JOIN PhoneAction ON cds.commStatusId = PhoneAction.LastCallResult) 
		INNER JOIN Communication ON PhoneAction.CommID = Communication.CommID) 
		INNER JOIN Location ON PhoneAction.LocationID = Location.LocationID
	
<!--- 		SELECT Flag.Name, Flag.FlagID, Count(PhoneAction.LastCallResult) AS CountOutcome, 
		Communication.Title AS Campaign, Fraction=(Convert(Real, Count(PhoneAction.LastCallResult))/#TotalPhoning#)*100
		FROM ((Flag INNER JOIN PhoneAction ON Flag.FlagID = PhoneAction.LastCallResult) 
		INNER JOIN Communication ON PhoneAction.CommID = Communication.CommID) 
		INNER JOIN Location ON PhoneAction.LocationID = Location.LocationID
 --->		
		WHERE Communication.CommID =  <cf_queryparam value="#CommID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		
		<CFIF ListGetAt(frmCountryID, 1) NEQ 0>
			AND Location.CountryID =  <cf_queryparam value="#ListGetAt(form.frmCountryID, 1)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		
		<CFIF #DateFrom# IS NOT "" AND #DateTo# IS NOT "">
			AND PhoneAction.LastCallDate >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND PhoneAction.LastCallDate <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfif>

		GROUP BY cds.Name, cds.commStatusID, PhoneAction.CommID, Communication.Title	
<!--- 		GROUP BY Flag.Name, Flag.FlagID, PhoneAction.CommID, Communication.Title
 --->	ORDER BY Communication.Title
	</CFQUERY>
		
	<CFSET Campaign = #GetPhoneAnalysisData.Campaign#>
				
	<!---Display the analysis--->
	<table width="700" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<th COLSPAN="3">
			Analysis of Phone Records for <CFOUTPUT><B>#ListGetAt(frmCountryID, 2)#</B> Campaign: <B>#htmleditformat(getPhoneAnalysisData.Campaign)#</B></CFOUTPUT>
		</th>	
	</tr>
	<TR>
		<TD COLSPAN="3">Total Locations <CFIF #TotalCalls.CallType# EQ 61>To Call<CFELSE>Calls Recieved From</CFIF>: <CFOUTPUT><B>#htmleditformat(TotalCalls.Total)#</B></CFOUTPUT></TD>
	</TR>
			
	<!---If Outbound Calls--->
	<CFIF #TotalCalls.CallType# EQ 61>
		<CFOUTPUT>
		<TR>
			<TD COLSPAN="3">Total Locations still to call: <B>#htmleditformat(TotalLeft.CallsLeftToPhone)#</B></TD>
		</TR>
		</CFOUTPUT>
	</CFIF>
	<TR>
		<TD COLSPAN="3">&nbsp;</TD>
	</TR>
	<TR>
		<Th WIDTH="200">Last Call Outcome</Th>
		<Th WIDTH="100">Number of calls</Th>
		<!---If Outbound Calls--->		
		<CFIF #TotalCalls.CallType# EQ 61>
			<Th WIDTH="100">%</Th>
		</CFIF>
	</TR>
	
	<CFOUTPUT QUERY="getPhoneAnalysisData">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<!---<CFIF ManagerOK AND ListFind(Allowed, FlagID) GT 0>
				<TD WIDTH="200"><A HREF="eventLists.cfm?FlagID=#FlagID#&countryid=#ListFirst(frmCountryID)#&filter=#URLEncodedFormat(frmAnalysis)#&criteria=#URLEncodedFormat(Analysis)#">#Analysis#</A></TD>
				<CFELSE>
				</CFIF>--->
			
 			<TD WIDTH="100"><A HREF="PhoneStatsDrill.cfm?CommID=#CommID#&FlagID=#FlagID#&CountryID=#ListGetAt(frmCountryID, 1)#&Campaign=#urlencodedformat(Campaign)#<CFIF #DateFrom# IS NOT '' AND #DateTo# IS NOT ''>&DateFrom=#htmleditformat(DateFrom)#&DateTo=#htmleditformat(DateTo)#</CFIF>">#htmleditformat(Name)#</A></TD>
			<TD WIDTH="20" align="right">#htmleditformat(CountOutcome)#</TD>
			<!---If Outbound Calls--->	
			<CFIF #TotalCalls.CallType# EQ 61>
				<TD WIDTH="20" align="right">#DecimalFormat(Fraction)#</TD>
			</CFIF>			
		</TR>		
	</CFOUTPUT>
		
	<CFOUTPUT>
	<TR>
		<TD WIDTH="20" align="right"><A HREF="PhoneStatsDrill.cfm?CommID=#CommID#&Campaign=#URLEncodedFormat(Campaign)#">Total</A></TD>
		<TD WIDTH="20" align="right">#htmleditformat(TotalCalls.Total)#</TD>
		<CFIF #TotalCalls.CallType# EQ 61>
			<TD WIDTH="20" align="right">100%</TD>
		</cfif>
	</TR>
	<tr>
		<th COLSPAN="3">
			Analysis of Phone Records for <B>#ListGetAt(frmCountryID, 2)#</B> Campaign: <B>#htmleditformat(getPhoneAnalysisData.Campaign)#</B>
		</th>	
	</tr>
	
	<!--- Display the pie --->
	
	<TR>
		<TD COLSPAN="3"><CFGRAPH TYPE="PIE"
           QUERY="GetPhoneAnalysisData"
           VALUECOLUMN="Fraction"
           ITEMCOLUMN="Name"
           GRAPHHEIGHT="400"
           GRAPHWIDTH="700"
           DEPTH="15"></CFGRAPH>
		</TD>
	</TR> 
	
	</TABLE>
	</CFOUTPUT>
</CFIF>


