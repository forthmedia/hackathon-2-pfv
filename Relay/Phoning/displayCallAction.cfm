<!--- �Relayware. All Rights Reserved 2014 --->
	<!--- displays the call action details for the given commid, and frmcurrententityid --->
<CFIF isdefined("Campaigncommid") and isdefined("frmEntityType")>

<CFQUERY NAME="GetPhoneAction" datasource="#application.siteDataSource#">
SELECT c.title,
	pa.PhoneActionID, 
	pa.LastCallDate, 
	pa.NDMCalls,
	pa.EngagedCalls, 
	pa.NoAnswerCalls, 
	pa.TotalCalls,
	pa.LastCallResult AS CommStatusFlagID,
	f.Name AS LastCallResult, 
	CASE WHEN pa.LastCallUser <> 0 THEN (select name from usergroup as ug where ug.usergroupid = pa.LastCallUser) ELSE 'Not Called' END as LastCallUsername,
	pa.PhoneActionLock,
	pa.PhoneActionCompleted
FROM         PhoneAction as pa, Flag as f, communication as c
WHERE       c.commid = pa.commid AND
			pa.LastCallResult = f.FlagID AND
            pa.CommID =  <cf_queryparam value="#Campaigncommid#" CFSQLTYPE="CF_SQL_INTEGER" >  AND 
			<CFIF frmEntitytype IS "location">
			pa.locationid =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFELSEIF frmEntitytype IS "person">
			pa.locationid = (select locationid from person where personid =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfif>
				   
</CFQUERY>



<CFOUTPUT>#htmleditformat(getPhoneAction.title)#<BR></CFOUTPUT>
<CFTABLE QUERY="GetPhoneAction" COLSPACING="1" COLHEADERS HTMLTABLE>
	<CFCOL TEXT="#LastCallResult#" HEADER="Last Call Result" WIDTH="14">
	<CFCOL TEXT="#DateFormat(LastCallDate, 'DD-MMM-YYYY')# #TimeFormat(LastCallDate,'HH:MM')#" HEADER="Last Call Date" WIDTH="14">
	<CFCOL TEXT="#LastCallUserName#" HEADER="Last Called by" WIDTH="14">
	<CFCOL TEXT="#NDMCalls#" HEADER="NDM Calls" WIDTH="7">
	<CFCOL TEXT="#EngagedCalls#" HEADER="Engaged" WIDTH="7">
	<CFCOL TEXT="#NoAnswerCalls#" HEADER="No Ans" WIDTH="7">
	<CFCOL TEXT="#TotalCalls#" HEADER="Total calls" WIDTH="7">
	<CFCOL TEXT="#YesNoFormat(PhoneActionCompleted)#" HEADER="Closed" WIDTH="7">
</CFTABLE>

<CFOUTPUT><P></CFOUTPUT>






















</cfif>
