<!--- �Relayware. All Rights Reserved 2014 --->
<CFQUERY NAME="GetEngagedFlagID" datasource="#application.siteDataSource#">
SELECT      dbo.Flag.FlagTextID, dbo.Flag.FlagID, dbo.FlagGroup.FlagGroupTextID
FROM         dbo.Flag, dbo.FlagGroup 
WHERE       dbo.Flag.FlagGroupID = dbo.FlagGroup.FlagGroupID AND
           (dbo.Flag.FlagTextID = 'Engaged') AND (dbo.FlagGroup.FlagGroupTextID = 'OutboundCallResults')
</CFQUERY>

<CFQUERY NAME="GetNoAnswerFlagID" datasource="#application.siteDataSource#">
SELECT      dbo.Flag.Name, dbo.Flag.FlagID, dbo.FlagGroup.FlagGroupID, dbo.FlagGroup.FlagGroupTextID, dbo.Flag.FlagTextID
FROM         dbo.Flag, dbo.FlagGroup 
WHERE       dbo.Flag.FlagGroupID = dbo.FlagGroup.FlagGroupID AND
                   (dbo.FlagGroup.FlagGroupTextID = 'OutboundCallResults') AND (dbo.Flag.FlagTextID = 'NoAnswer')
</CFQUERY>

<CFQUERY NAME="GetNDMFlagID" datasource="#application.siteDataSource#">
SELECT      dbo.Flag.Name, dbo.Flag.FlagID, dbo.FlagGroup.FlagGroupID, dbo.FlagGroup.FlagGroupTextID, dbo.Flag.FlagTextID
FROM         dbo.Flag, dbo.FlagGroup 
WHERE       dbo.Flag.FlagGroupID = dbo.FlagGroup.FlagGroupID AND
                   (dbo.FlagGroup.FlagGroupTextID = 'OutboundCallResults') AND (dbo.Flag.FlagTextID = 'NDM')
</CFQUERY>


<CFQUERY NAME="GetPhoneAction" datasource="#application.siteDataSource#">
	SELECT      PhoneActionID, LastCallDate, LastCallResult, EngagedCalls, 
			NoAnswerCalls, NDMCalls, TotalCalls
	FROM         dbo.PhoneAction 
	WHERE       (PhoneActionID =  <cf_queryparam value="#Cookie.PHONEACTIONID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</CFQUERY>

<!--- Find out what value the CommStatus Flagid is
This will be used to determin whether we have to leave the PhoneAction Record locked or not
 --->
<CFQUERY NAME="CheckFlagValue" datasource="#application.siteDataSource#">
	SELECT  Flag.Score from Flag where Flag.FlagID =  <cf_queryparam value="#CommStatus#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFSET NewEngagedCalls = #GetPhoneAction.EngagedCalls#+1>
<CFSET NewNoAnswerCalls = #GetPhoneAction.NoAnswerCalls#+1>
<CFSET NewNDMCalls = #GetPhoneAction.NDMCalls#+1>
<CFSET NewTotalCalls = #GetPhoneAction.TotalCalls#+1>

<!--- Update PhoneAction Record 
	This is where the PhoneAction record is updated
--->
<!--- 	<CFOUTPUT>NewEngagedCalls: #NewEngagedCalls# 
MaxEngagedCalls: #MaxEngagedCalls#
NewNoAnswerCalls:#NewNoAnswerCalls#
MaxNoAnswerCalls: #MaxNoAnswerCalls#
</cfoutput>
 --->
 
 
<CFQUERY NAME="UpdatePhoneAction" datasource="#application.siteDataSource#">
	UPDATE     dbo.PhoneAction
	SET     LastCallDate =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			LastCallUser=#request.relayCurrentUser.usergroupid#,
			LastCallResult =  <cf_queryparam value="#CommStatus#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			TotalCalls =  <cf_queryparam value="#NewTotalCalls#" CFSQLTYPE="CF_SQL_smallint" > ,
			PersonID =  <cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	<CFIF #frmNextCallDate# IS NOT "">
		<CFSET NextCallDate = #frmNextCallDate# + #frmnextcalltime#>
		, NextCallDate= #createodbcdatetime(NextCallDate)# 
	</CFIF>
			
	<!--- If CommStatus = Engaged increment engaged --->
	<CFIF #CommStatus# IS #GetEngagedFlagID.FlagID#>
			, EngagedCalls =  <cf_queryparam value="#NewEngagedCalls#" CFSQLTYPE="CF_SQL_smallint" > 
	</CFIF>
	<!--- If CommStatus = No Answer increment No Answer --->
	<CFIF #CommStatus# IS #GetNoAnswerFlagID.FlagId#>
			, NoAnswerCalls =  <cf_queryparam value="#NewNoAnswerCalls#" CFSQLTYPE="CF_SQL_smallint" > 
	</CFIF>
	<!--- If CommStatus = NDM increment NDMCalls --->
	<CFIF #CommStatus# IS #GetNDMFlagID.FlagId#>
			, NDMCalls =  <cf_queryparam value="#NewNDMCalls#" CFSQLTYPE="CF_SQL_smallint" > 
	</CFIF>

	<!--- Unlock the record if MaxEngaged/NoAnswer constants not reached; CallBack and 
	CommStatus FlagID is 0 --->

	<CFIF CheckFlagValue.Score IS 0 or CheckFlagValue.Score IS "">  <!--- Otherwise just leave the PhoneAction Record locked --->
		<CFIF (#NewEngagedCalls# LT #MaxEngagedCalls#) OR 
		(#NewNoAnswerCalls# LT #MaxNoAnswerCalls#) >
		    ,PhoneActionLock=0
		<CFELSE>   <!--- too many engaged etc.--->
			,PhoneActionLock=1
			,PhoneActionCompleted = 1

		</CFIF>
	<CFELSE>		<!--- call completed --->
			,PhoneActionlock=1
			,PhoneActionCompleted = 1
	</CFIF>
	WHERE       (PhoneActionID =  <cf_queryparam value="#Cookie.PhoneActionID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</CFQUERY>



