<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB 2009/03/03--->
<!--- 2013-12-19  		AXA		Case 438122 removed code.  copied changes from case 438312.  This change does not need to be merged in with 2013 Release 2, since this fix is already in there. --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="index.cfm">
<cfparam name="editor" default="no">

<cfif isdefined("tableName") and tableName neq "">
	<cfset thisTableName = "?tablename=#tableName#">
<cfelse>
	<cfset thisTableName = "">
</cfif>

<cfif isdefined("datasourceid")>
	<cfquery name="GetAction" datasource="#application.sitedatasource#">
		select top 1 action
		from dataSourceHistory
		where datasourceID =  <cf_queryparam value="#datasourceid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		order by datasourcehistoryID desc
	</cfquery>
</cfif>

<CF_RelayNavMenu pageTitle="" thisDir="dataloadsV2">

	<cfif isDefined("editor") and editor eq "yes">
		<CF_RelayNavMenuItem MenuItemText="Data Loads" CFTemplate="index.cfm" target="mainSub">
	</cfif> 
	
<!--- 	<CF_RelayNavMenuItem MenuItemText="Manage Data Load" CFTemplate="manageDataLoad.cfm#thisTableName#" target="mainSub"> --->
	<cfif not isDefined("editor") or editor eq "no">
		<CF_RelayNavMenuItem MenuItemText="Start New Data Load" CFTemplate="index.cfm?editor=yes&datasourceid=0" target="mainSub">
	</cfif>
	
	<cfif isdefined("datasourceid") and GetAction.action NEQ "startnew">
		<cfif thisTableName neq "">
			<CF_RelayNavMenuItem MenuItemText="List Data Sources" CFTemplate="index.cfm" target="mainSub">
	<!--- 		<CF_RelayNavMenuItem MenuItemText="Editor" CFTemplate="index.cfm?editor=yes&datasourceid=#datasourceID#" target="mainSub"> --->
			<CF_RelayNavMenuItem MenuItemText="Functions" CFTemplate="manageDataLoad.cfm#thisTableName#" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Field Counts" CFTemplate="columnCounts.cfm#thisTableName#" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Data Matching" CFTemplate="dataMatching.cfm#thisTableName#" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Profile Matching" CFTemplate="profileMatching.cfm#thisTableName#" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="View Columns" CFTemplate="dataloadList.cfm#thisTableName#&action=viewCols" target="mainSub">
		</cfif>
	</cfif>
	<!--- <cfif editor eq "no">
		<CF_RelayNavMenuItem MenuItemText="List Data Sources" CFTemplate="index.cfm" target="mainSub">
	</cfif> --->
	
<!--- 	<CF_RelayNavMenuItem MenuItemText="Data Loads" CFTemplate="listDataLoads.cfm" target="mainSub">	 --->
	<cfif findnocase("manageMismatchedCols.cfm",SCRIPT_NAME) gt 0 or findnocase("dataLoadList.cfm",SCRIPT_NAME) gt 0>
		<CF_RelayNavMenuItem MenuItemText="View 100 records" CFTemplate="dataLoadList.cfm#thisTableName#&action=viewData&numRowsToReturn=100" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="View All Data" CFTemplate="dataLoadList.cfm#thisTableName#&action=viewData" target="mainSub">
	</cfif>
	
	<cfif editor eq "yes">
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:doForm('saveRecord')" target="mainSub">
	</cfif>
	
	<cfif thisTableName is not "">
		<CF_RelayNavMenuItem MenuItemText="Home" CFTemplate="manageDataLoad.cfm#thisTableName#" target="mainSub"> 
	</cfif>
	
	<cfif findnocase("mapColToFlag.cfm",SCRIPT_NAME) gt 0 >
		<CF_RelayNavMenuItem MenuItemText="Profile Matching" CFTemplate="profileMatching.cfm#thisTableName#" target="mainSub">	
	</cfif>

</CF_RelayNavMenu>
