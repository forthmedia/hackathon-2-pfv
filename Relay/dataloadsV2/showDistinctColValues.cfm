<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			showDistinctColValues.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-11-12          ACPK        Replaced table with bootstrap

Possible enhancements:


 --->

<cf_title>Show Distinct Column Values</cf_title>


<cfinvoke component="relay.com.dbInterface" method="getDistinctColumnValues" returnvariable="distinctColValues">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="tableName" value="#tableName#"/>
	<cfinvokeargument name="columnName" value="#columnName#"/>
</cfinvoke>

<cfparam name="sortOrder" default="Column_Value">
<cfquery name="getDistinctColValues" dbtype="query">
	select *
		from distinctColValues
				where 1=1
		<cfset inQoQ = true>
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	 order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!--- 2015-11-12    ACPK    Replaced table with bootstrap --->
<p>Values for column: <cfoutput>#htmleditformat(columnName)#</cfoutput>
<p>The table below shows the current distinct values for column <cfoutput>#htmleditformat(columnName)#</cfoutput> and
the number of instances for each distinct value.</p>

<CF_tableFromQueryObject 
	queryObject="#getDistinctColValues#"
	sortOrder="#sortOrder#"
	useInclude="false"
	numRowsPerPage="100">