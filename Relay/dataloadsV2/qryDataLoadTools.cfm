<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			qryDataLoadTools.cfm	
Author:				SWJ
Date started:		2003-01-19
	
Description:		This builds a query object that contains the content 
					for the dataloads tools pages

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfscript>
qDataLoadTools = QueryNew("name, URL, description");
// add some rows in the query and set the cells in the query 
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Add control fields");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=alterLoadTable");
	temp = QuerySetCell(qDataLoadTools, "description", "This adds fields to the incoming data table that are used to control the data load.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Clean column names");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=cleanColNames");
	temp = QuerySetCell(qDataLoadTools, "description", "This cleans the column names in the data load table.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Resize columns");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=resizeCols");
	temp = QuerySetCell(qDataLoadTools, "description", "This resizes the columns in the data load table.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Map columns for loading");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageMisMatchedCols.cfm?1=1");
	temp = QuerySetCell(qDataLoadTools, "description", "Sort out mismatched columns.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Trim the data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=trimData");
	temp = QuerySetCell(qDataLoadTools, "description", "This trims the data and removing preceding and trailing spaces.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Delete records with blank fields");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=deleteNULLData");
	temp = QuerySetCell(qDataLoadTools, "description", "This will delete any rows where the organisation name, person name and email fields are ALL blank.");

newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Prepare Country Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=validateCountryData");
	temp = QuerySetCell(qDataLoadTools, "description", "This relay CountryIDs to the data.");
// prepare the data
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Prepare Org Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=prepareOrgData");
	temp = QuerySetCell(qDataLoadTools, "description", "This adds unique numbers and removes dupes from organisation Data.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Prepare Location Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=prepareLocData");
	temp = QuerySetCell(qDataLoadTools, "description", "This adds unique numbers and removes dupes from location Data.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Prepare Person Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=preparePerData");
	temp = QuerySetCell(qDataLoadTools, "description", "This adds unique numbers and removes dupes from person Data.");
// check the data
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Check for Bad Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=checkBadRows");
	temp = QuerySetCell(qDataLoadTools, "description", "Org data that did not load.");

// insert the data
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Load data into Relay Database");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=insertData");
	temp = QuerySetCell(qDataLoadTools, "description", "This will append a row for each good row in the incoming data table into the person, location and organisation tables.");

// check the data after the insert
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Check Org Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=checkOrgData");
	temp = QuerySetCell(qDataLoadTools, "description", "Org data that did not load.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Check Location Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=checkLocData");
	temp = QuerySetCell(qDataLoadTools, "description", "Location data that did not load.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "Check Person Data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/manageDataLoad.cfm?action=checkPerData");
	temp = QuerySetCell(qDataLoadTools, "description", "Person data that did not load.");

// various views of the dat aand columns
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "View Columns");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/dataLoadList.cfm?action=viewCols");
	temp = QuerySetCell(qDataLoadTools, "description", "View the columns in the table being loaded.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "View sample of data (first 100 rows)");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/dataLoadList.cfm?action=viewData&numRowsToReturn=100");
	temp = QuerySetCell(qDataLoadTools, "description", "View a sample of the data in the table being loaded.");
newRow  = QueryAddRow(qDataLoadTools);
	temp = QuerySetCell(qDataLoadTools, "name", "View all the data");
	temp = QuerySetCell(qDataLoadTools, "URL", "dataloadsV2/dataLoadList.cfm?action=viewData");
	temp = QuerySetCell(qDataLoadTools, "description", "View a sample all of the data in the table being loaded.");

</cfscript>