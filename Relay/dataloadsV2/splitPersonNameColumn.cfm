<!--- �Relayware. All Rights Reserved 2014 --->

<cf_title>Split Person Name Column</cf_title>

<!--- <cfinclude template="dataloadsTopHead.cfm"> --->
<cfparam name="message" default=" ">

<cfif isDefined("tableName") and tableName neq "">
	<cfscript>
	// create an instance of the object
	myObject = createObject("component","relay.com.dbInterface");
	myObject.dataSource = application.siteDataSource;
	myObject.tableName = tableName;
	myObject.getAllCols();
	dataLoadObject = createObject("component","relayDataloadV2");
	dataLoadObject.initTable (datasource = application.siteDataSource, tablename = tablename);
	</cfscript>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" style="margin: 0 auto">
		<tr><td>
		<p><cfoutput>Choose a column from the list below to split into the firstname and lastName columns in #htmleditformat(tableName)#.
		This will update #htmleditformat(tableName)# firstname and lastname wherever they have a null row.</cfoutput></p>
		</td></tr>
		<tr>
			<TD VALIGN="Top">
			<form method="post" name="selectForm">
				<SELECT NAME="columnName" onchange="document.selectForm.submit()">
					<OPTION VALUE="">Choose column to split</OPTION>
					<CFOUTPUT QUERY="myObject.qGetAllCols">
						<OPTION VALUE="#column_name#" <cfif isDefined("columnName") and columnName eq column_name> SELECTED</cfif>>#htmleditformat(column_name)#</OPTION>
					</CFOUTPUT>
				</SELECT>
			</form>
			</TD>
		</tr>
	</table>
	<cfif isDefined("columnName") and columnName neq "">
		<cfinvoke component="#dataLoadObject#" method="splitPersonNameColumns"
			returnvariable="columnData">
			<cfinvokeargument name="columnToSplit" value="#columnName#"/>
		</cfinvoke>
		<cfoutput>#application.com.relayUI.message(message="Column #htmleditformat(columnName)# has been split into firstName, lastname columns.",type="info")#</cfoutput>
		<CF_tableFromQueryObject queryObject="#columnData#" sortOrder="" useinclude=false>
	</cfif>

</cfif>