<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			dataMatching.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-11-12          ACPK        Replaced table from top of page with bootstrap

Possible enhancements:

 WAB 2008/10/29  Mod see comment

 --->

<cfset application.com.request.setDocumentH1(text="Load Data")>

	<cfscript>
	// create an instance of the object
	dataLoadObject = createObject("component","relayDataloadV2");
	dataLoadObject.initTable (datasource = application.siteDataSource, tablename = tablename);
	</cfscript>


<cfif isDefined("action")>
	<cfinvoke component="#dataLoadObject#" method="#action#" returnvariable="message">
		<cfinvokeargument name="tablename" value="#tablename#"/>
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	</cfinvoke>
	<cfinvoke component="#dataLoadObject#" method="addDataSourceHistory" returnvariable="message2">
		<cfinvokeargument name="action" value="#action#"/>
	</cfinvoke>
</cfif>


	<cfset qCountryData = dataLoadObject.viewCountryData(tableName,application.siteDataSource,"countryID")>


<cfparam name="message" default=" ">

<!--- <cfinclude template="dataloadsTopHead.cfm"> --->
<cfoutput>
<!--- 2015-11-12    ACPK    Replaced table with bootstrap --->
<p>Use this screen to manage the matching of data in #htmleditformat(tablename)# to the relevant tables in Relay.</p>
<cfif IsSimpleValue(message) and message neq "">#application.com.relayUI.message(message=message)#</cfif>

</cfoutput>
<!--- Country matches --->
<cfoutput><p><a href="#SCRIPT_NAME#?action=resetDataMatching&tablename=#tablename#">Click here</a> to reset and start again.</p></cfoutput>

<h2>1. Country matching</h2>
<p>First you must match the dataload country to the Relay table.
<cfoutput><a href="#SCRIPT_NAME#?action=validateCountryData&tablename=#tablename#">Click here</a> to do this.</p></cfoutput>

<CF_tableFromQueryObject queryObject="#qCountryData#"
	HidePageControls="yes"
	numRowsPerPage="200"
	sortOrder=""
	useInclude="false"
	>

<!--- org matches --->
<cfscript>
	// WAB 2008/10/29 altered to use view rather than base table
	distinctOrgActionValues = application.com.dbInterface.getDistinctColumnValues(datasource = application.siteDataSource,tablename = "v"&tableName,columnName = "orgAction",cacheforxmins = 0, distinctCountColumnName = "orgid");
</cfscript>

<h2>2. Organisation matching</h2>
<p>Second you must match the dataload organisation data to the Relay table.
<cfoutput><a href="#SCRIPT_NAME#?action=prepareOrgData&tablename=#tablename#">Click here</a> to do this.</p></cfoutput>

<CF_tableFromQueryObject queryObject="#distinctOrgActionValues#"
	keyColumnList="column_value"
	keyColumnURLList="checkEntityMatchesInDataLoadTable.cfm?checkTable=org&tablename=#tablename#&action="
	keyColumnKeyList="column_value"
	HidePageControls="yes"
	useInclude = "false"
	sortOrder=""
	>

<!--- Location matches --->
<h2>3. Location matching</h2>
<p>Third you must match the dataload location data to the Relay table.
<cfoutput><a href="#SCRIPT_NAME#?action=prepareLocData&tablename=#tablename#">Click here</a> to do this.</p></cfoutput>

<cfinvoke component="relay.com.dbInterface" method="getDistinctColumnValues"
	returnvariable="distinctLocActionValues">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="tableName" value="#tableName#"/>
	<cfinvokeargument name="columnName" value="locationAction"/>
	<cfinvokeargument name="cacheForXMins" value="0"/>
	<cfinvokeargument name="distinctCountColumnName" value="Locationid"/>
</cfinvoke>

<CF_tableFromQueryObject queryObject="#distinctLocActionValues#"
	keyColumnList="column_value"
	keyColumnURLList="checkEntityMatchesInDataLoadTable.cfm?checkTable=loc&tablename=#tablename#&action="
	keyColumnKeyList="column_value"
	HidePageControls="yes"
	useInclude = "false"
	sortOrder=""
	>

<!--- Person matches --->
<h2>4. Person matching</h2>
<p>Fourth you must match the dataload location data to the Relay table.
<cfoutput><a href="#SCRIPT_NAME#?action=preparePerData&tablename=#tablename#">Click here</a> to do this.</p></cfoutput>

<cfinvoke component="relay.com.dbInterface" method="getDistinctColumnValues"
	returnvariable="distinctPerActionValues">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="tableName" value="#tableName#"/>
	<cfinvokeargument name="columnName" value="personAction"/>
	<cfinvokeargument name="cacheForXMins" value="0"/>
	<cfinvokeargument name="distinctCountColumnName" value="Personid"/>
</cfinvoke>

<CF_tableFromQueryObject queryObject="#distinctPerActionValues#"
	keyColumnList="column_value"
	keyColumnURLList="checkEntityMatchesInDataLoadTable.cfm?checkTable=per&tablename=#tablename#&action="
	keyColumnKeyList="column_value"

	HidePageControls="yes"
	useInclude = "false"

	sortOrder=""
	>