<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			dataTransfer.cfm	
Author:				NJH/AC
Date started:		2006-11-29
Description:		
Amendment History:
Date (DD-MMM-YYYY)	Initials 	What was changed
26 FEB 2007 		AJC			made dirPath a parameter as lenovo data gets saved to different dir.
9 Aug   2007        SSS  		Added fileType checksum option
24 OCT  2007		NJH			used java classes for string manipulation and file writing. Much faster, especially for large files.
20 Dec  2007		AJC			Created webservice export definitiontype (4)
29 Oct	2008		NJH			CR-LEX578 - set rowCount to be the number of records returned in the query if the data transfer is a webservice.
11 Dec	2008		NYB			Fixed up 
2009-01-22			SSS			changed includeHeadings to false as default
2012-11-22			PPB			Case 432209 remove the cf_head which caused an error 

Possible enhancements:
 --->


<!--- 2012-11-22 PPB Case 432209 remove the cf_head
<cf_head>
	<cf_title>Data Transfer</cf_title>
</cf_head>
 --->


<cfparam name="dtdID" type="numeric" default="-1">
<cfparam name="dirPath" default="">
<cfparam name="contentDelimiter" type="string" default="|">
<cfparam name="tableName" type="string" default="">
<cfparam name="appendData" type="boolean" default="false">
<cfparam name="maxErrors" type="numeric" default="0">
<!--- NYB 2008-12-11 P-SNY055 trying to make the file work -> --->
<cfparam name="useFileWriter" type="boolean" default="false"> <!--- added because FileWriter.cfc is throwing an error --->
<!--- lenovo bug 1602 changed this to false for orginal behavoiur some clients do not want columns in the exports --->
<cfparam name="includeHeadings" type="boolean" default="false">
<!--- <- P-SNY055 --->

<cfif dtdID eq -1>
	<cfoutput>DtdID must be passed as a parameter</cfoutput>
	<CF_ABORT>
</cfif>

<cfset dtStartTime = createOdbcDatetime(now())>
<cfset fileContent=ArrayNew(1)>
<cfset rowCount=0>
<cfset fileGenerated = 0>

<cfscript>
	// get data transfer definition information
	dtd=application.com.DataTransfers.fn_get_DataTransferDefinition(dtdID=dtdID);
	// get the max modregister id
	maxModID=application.com.dataTransfers.fn_get_MaxModRegisterID();
	// get the last data transfer that has a modregister ID
	lastDataTransfer=application.com.dataTransfers.fn_get_LastDataTransfer(directionTypeID=dtd.directionTypeID,dtdID=dtdid);
</cfscript>

<cfif dirPath is "">
	<cfset dirPath = "#application.paths.userFiles#\dataTransfers\#dtd.direction#\">
</cfif>

<!--- NYB 2008-12-11 P-SNY055 trying to make the file work --->
<cfif not isdefined("dtd.GenerateFileType")>
	<cfquery name="dtd" dbtype="query">
		select *, 'utf-8' as GenerateFileType from dtd
	</cfquery>
</cfif>

<!--- NJH 2007-10-17 adding import code... this code currently deals with exporting only. --->

<cfif dtd.directionTypeID eq 2>
	<cfif lastDataTransfer.recordCount eq 0>
		<cfset lastTransferMaxModID = 0>
	<cfelse>
		<cfset lastTransferMaxModID = lastDataTransfer.maxModRegisterID>
	</cfif>
	
	<!--- NJH - doing an incremental export --->
	<!--- AJC - doing an incremental webservice export --->
	<cfif lastTransferMaxModID lt maxModID.ID and (dtd.dataTransferType eq 1 or dtd.dataTransferType eq 4)>
		<cfset transferDataQuery = evaluate("application.com.dataTransfers.#dtd.functionname#(maxModRegisterID=lastTransferMaxModID)")>
	<!--- do a full export --->
	<cfelseif dtd.dataTransferType eq 2>
		<cfset transferDataQuery = evaluate("application.com.dataTransfers.#dtd.functionname#()")>
	</cfif>
	
	<!--- AJC dataTransferType 4 is a webservice export and doesn't create a file --->
	<cfif dtd.dataTransferType neq 4>	
	<cfif isDefined("transferDataQuery")>
		
			<!--- NYB 2008-12-11 P-SNY055 - added - trying to make the file work -> --->
			<cfscript>
				LOCAL = StructNew();
				LOCAL.ColumnArray = transferDataQuery.GetColumnNames();
				LOCAL.ColumnList = ArrayToList(LOCAL.ColumnArray,contentDelimiter);
			</cfscript>
			<!--- <- P-SNY055 --->
		
		<cfif transferDataQuery.RecordCount gt 0>
			<cfif dtd.RecordsPerFile is 0>
				<cfset RecordsPerFile = transferDataQuery.RecordCount>
			<cfelse>
				<cfset RecordsPerFile = dtd.RecordsPerFile>
			</cfif>
			<cfset rowCount=transferDataQuery.recordCount>
			<!--- number of files the export is to be split up into --->
			<cfset numberOfFiles=Ceiling(rowcount/RecordsPerFile)>
			<!--- counter for current files --->
			<cfset currentFileCount = 0>
			<!--- start & end row of the loop --->
			<cfset startrow = 1>
				<cfset endrow = RecordsPerFile>
			<!--- sets the number of elements in the array (1 per file) --->
			<cfset Arrayset(fileContent,  1,  numberOfFiles, "")>

			<cfloop index="xfileID" from="1" to="#numberOfFiles#">
				<cfset currentFileCount=currentFileCount+1>
				
					<!--- NYB 2008-12-11 P-SNY055 - moved to outside this loop
					GCC 2008-12-17 put back because this is a real object - otherwise we would continually append content to object 1 instead of the correct behaviour
					--->
				<cfscript>
					myFileContent = createObject('component', '/relay/com/StringBuffer').init();
						if (includeHeadings) {
							myFileContent.append(LOCAL.ColumnList&chr(13)&chr(10));
						}
				</cfscript>


					<cfset loopcounter = 0>
				<cfloop query="transferDataQuery" startrow="#startrow#" endrow="#endrow#">
					<cfset loopcounter = loopcounter + 1>
						<!--- NYB 2008-12-11 P-SNY055 - added - trying to make the file work -> --->
						<cfset modifiedEntity = "">
						<cfloop index="i" list="#LOCAL.ColumnList#" delimiters="#contentDelimiter#">
							<cfset modifiedEntity = ListAppend(modifiedEntity,evaluate("#i#"),contentDelimiter)>
						</cfloop>
						<!--- <- P-SNY055 --->
					<!--- NJH 2007-10-24 - using java string functions. They increase the speed dramatically --->
						<cfif loopcounter NEQ endrow>
					<cfscript>
						myFileContent.append(modifiedEntity&chr(13)&chr(10));
					</cfscript>
						<cfelse>
							<cfscript>
								myFileContent.append(modifiedEntity);
							</cfscript>
						</cfif>
					<!--- <cfset fileContent[#currentFileCount#]=fileContent[#currentFileCount#]&modifiedEntity>
					<cfif currentRow neq endrow>
						<cfoutput><cfset fileContent[#currentFileCount#] = fileContent[#currentFileCount#] & "#CHR(13)##CHR(10)#"></cfoutput>
					</cfif> --->
				</cfloop>
				<cfset startrow = startrow + RecordsPerFile>
					<cfset endrow = endrow + RecordsPerFile>
				<cfscript>
					fileContent[currentFileCount] = myFileContent.getString();
				</cfscript>
			</cfloop>
			<!--- NYB 2008-12-11 P-SNY055 - added - trying to make the file work -> --->
			<cfelse>
				<cfif includeHeadings>
					<!--- NYB 2008-01-16 P-SNY055 -> added (was originally moved outside if [above] put got put back causing error) --->
					<cfscript>
						myFileContent = createObject('component', '/relay/com/StringBuffer').init();
						if (includeHeadings) {
							myFileContent.append(LOCAL.ColumnList&chr(13)&chr(10));
						}
					</cfscript>
					<!--- <- NYB 2008-01-16 P-SNY055 --->
					<cfset fileContent[1] = myFileContent.getString()>
				</cfif>
			<!--- <- P-SNY055 --->
		</cfif>
		<cfelse>
			<cfoutput>transferDataQuery not defined</cfoutput>
	</cfif>
	
	<cfswitch expression="#dtd.filenameSuffix#">
		<cfcase value="DTDID">
			<cfset filesuffix=dtdID>
		</cfcase>
		<cfcase value="Date">
			<cfif dtd.dateMask neq "">
				<cfset filesuffix=DateFormat(dtStartTime,"#dtd.dateMask#")>
				<cfif FindNoCase("nn", dtd.dateMask) is not 0>
					<cfset filesuffix=ReplaceNoCase(filesuffix, "nn", timeformat(dtStartTime, "mm"))>
				</cfif>
			<cfelse>
				<cfset filesuffix=DateFormat(dtStartTime)>
			</cfif>
		</cfcase>
		<!--- NJH adding a case for no suffix --->
		<cfcase value="">
			<cfset filesuffix="">
		</cfcase>
		<cfdefaultcase>
			<cfset filesuffix=dtdID>
		</cfdefaultcase>
	</cfswitch>
	
	<cfset numberoffiles=arraylen(fileContent)>
	<cfif numberoffiles is 0>
		<cfset numberoffiles=1>
		<cfset Arrayset(fileContent,  1,  1, "")>
	</cfif>
	
	<cfloop index="fileID" from="1" to="#numberoffiles#">
		<!--- HACK: NJH don't want the fileID when exporting just 1 file... probably not the best way of doing this
			 --->
		<cfif numberOfFiles eq 1 and dtd.RecordsPerFile eq 0>
			<cfif fileSuffix neq "">
				<cfset filePath = dirPath&"#dtd.filename#_#filesuffix#.#dtd.Fileextension#">
			<cfelse>
				<cfset filePath = dirPath&"#dtd.filename#.#dtd.Fileextension#">
			</cfif>
		<cfelse>
			<cfif fileID eq 1>
				<cfset filePath = dirPath&"#dtd.filename#_#filesuffix##fileID#.#dtd.Fileextension#">
			<cfelse>
				<cfset filePath = dirPath&"pending\#dtd.filename#_#filesuffix##fileID#.#dtd.Fileextension#">
			</cfif>
		</cfif>
		<cfif dtd.GenerateOkFile>
			<cfset okFilePath = dirPath&"#dtd.filename#_#filesuffix##fileID#.ok">
		</cfif>
		<cftry>
			<!--- Create File --->
			<cfset exportContent=evaluate("fileContent[#fileID#]")>
			
			<!--- NJH 2007-10-24 using fileWriter compenant as it much faster than cffile, especially for large files --->
				<cfif useFileWriter and listContainsNoCase("utf-8#application.delim1#UTF-16LE#application.delim1#UTF-16BE", #dtd.GenerateFileType#) GT 0>
			<cfscript>
						myOutFile = createObject('component', '/relay/com/FileWriter').init(filePath, #dtd.GenerateFileType#, 131072);
				myOutFile.writeLine(exportContent);
				myOutFile.close();
			</cfscript>
				<cfelse>
					<cftry>
						<cffile action="write" addnewline="no" charset="#dtd.GenerateFileType#" file="#filePath#" output="#exportContent#">
						<cfcatch type="any">
							<cfdump var="#cfcatch#">
						</cfcatch>
					</cftry>
				</cfif>
			
			<cfif dtd.GenerateOkFile>
				<cfif fileExists(filePath)>
					<cfset fileGenerated = 1>
					<cfif dtd.okFileType EQ "checksum">
							<cffile action="read" charset="#dtd.GenerateFileType#" file="#filePath#" variable="exportFile">
							<cfscript>
								//gets the first 1024 byte characters of the file
								First1024Bytes = exportFile.getbytes();
								digesterBase = createObject("Java","java.security.MessageDigest");
								digester = digesterBase.getInstance("MD5");
								digester.reset();
								digester.update(First1024Bytes);
								value = digester.digest();
								CheckSumValue = BinaryEncode(value,"HEX");
							</cfscript>
							<cfset CheckSumValueLowerCase = LCase(CheckSumValue)>
							<cffile action="write" file="#okFilePath#" output="#CheckSumValueLowerCase#">
					<cfelse>
						<cfif filesuffix EQ "">
							<cfset FileNameForBytes = '#dtd.filename#.#dtd.Fileextension#'>
						<cfelse>
							<cfset FileNameForBytes = '#dtd.filename#_#filesuffix#.#dtd.Fileextension#'>
						</cfif>
			 				<cfdirectory action="list" filter="#FileNameForBytes#" directory="#dirPath#" sort="dateLastModified DESC" name="fileDir">
						<cfset fileSize = fileDir.size[1] & " bytes">
						<cffile action="write" file="#okFilePath#" output="#fileSize#">
					</cfif>
				</cfif>
			</cfif>
			<cfcatch>
				<cfoutput>#htmleditformat(cfcatch.message)# #htmleditformat(cfcatch.type)#</cfoutput> <CF_ABORT>
			</cfcatch>
		</cftry>
	</cfloop>
	
	<!--- NJH 2008-10-29 CR-LEX578 if a data transfer type of 4, then set rowCount as the number of records returned in the query --->
	<cfelse>
		<cfif isDefined("transferDataQuery")>
			<cfset rowCount = transferDataQuery.recordCount>
		</cfif>
	</cfif>
	
	<cfset dtEndTime = createOdbcDatetime(now())>
	
	<cfscript>
		// insert new record into the data transfer history table
		application.com.dataTransfers.fn_ins_DataTransferHistory(dtdID=dtdID,dtStartTime=dtStartTime,dtEndTime=dtEndTime,rowCount=rowCount,maxModID=maxModID.ID,fileGenerated=fileGenerated);
	</cfscript>

<!--- doing an import --->
<cfelseif dtd.directionTypeID eq 1>

	<!--- NJH 2008-03-05 if the directory path doesn't have a \ at the end, add one --->
	<cfif right(dirPath,1) neq "\">
		<cfset dirPath = "#dirPath#\">
	</cfif>
	<!--- NYB 2008-12-11 P-SNY055 - added .#dtd.fileextension# to filename -> --->
	<cfset filename = "#dirPath##dtd.filename#.#dtd.fileextension#">

	 <!--- running a bulk insert  - used for large file imports. If any errors occurs, the import is rejected--->
	<cfif dtd.dataTransferType eq 3>
		
		<cfif tableName neq "">

			<cftry>
			<cfquery name="loadFileData" datasource="#application.siteDataSource#">
				<cfif not appendData>
					truncate table #tableName#
				</cfif>
				
				--sp_bulkInsert @tableName='#tableName#', @filename='#filename#', @FieldTerminator='#contentDelimiter#',@MaxErrors=#maxErrors#
				
				BULK INSERT #tableName# FROM '#filename#'
                	WITH (dataFileType = 'char',
							FieldTerminator =  <cf_queryparam value="#contentDelimiter#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							RowTerminator = '\n',
							Codepage = 'ACP',
							MaxErrors =  <cf_queryparam value="#maxErrors#" CFSQLTYPE="cf_sql_integer" > )
				
				--select @@rowCount as rowsInserted
				--select @@ERROR as insertError
			</cfquery>
			
			<cfcatch>
				There were errors running the bulk insert.<CF_ABORT>
			</cfcatch>
			
			</cftry>

			<cfquery name="getRecordCount" datasource="#application.siteDataSource#">
				select count(*) as numRows from #tableName#
			</cfquery>
			
			<cfif getRecordCount.numRows gt 0>
				<cfscript>
					transferDataQuery = evaluate("application.com.dataTransfers.#dtd.functionname#()");
				</cfscript>
			<cfelse>
				<cfoutput>Errors were encountered running the bulk insert.</cfoutput><CF_ABORT>
			</cfif>
			
		<cfelse>
			<cfoutput>Parameter tablename must be set to do a bulk insert.</cfoutput>
			<CF_ABORT>
		</cfif>
		
	<cfelse>
		<cftry>
			<cfif fileExists("#filename#")>
				<cffile action="read" file="#filename#" variable="fileContents">
				<cfscript>
					transferDataQuery = evaluate("application.com.dataTransfers.#dtd.functionname#(fileContents=fileContents)");
				</cfscript>
			<cfelse>
				<cfoutput>#htmleditformat(filename)# does not exist.</cfoutput>
			</cfif>
		
			<cfcatch>
				<cfoutput>#htmleditformat(cfcatch.message)# #htmleditformat(cfcatch.type)#</cfoutput>
				<CF_ABORT>
			</cfcatch>
		</cftry>
	</cfif>
		
		
</cfif>


