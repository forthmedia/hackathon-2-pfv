<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name: dataTransferTrace.cfm

Amendment History:

2013-03-06	PPB		Case 434056 add timeout
2013-03-15 	PPB 	Case 434056 optional maxRows 
2013-04-04 	PPB 	Case 434056 may as well make maxrows a param to allow user to control
 --->

<cfsetting requesttimeout="1800">					<!--- 2013-03-06 PPB Case 434056 --->

<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="sortOrder" default="dthID desc">
<cfparam name="maxRows" default="500">				<!--- 2013-04-04 PPB Case 434056 --->


<cf_title>Data Transfer Trace</cf_title>

<cfif not structKeyExists(url,"dthID")>

	<!--- NJH 2012/12/12 SalesForce issues - this file can be included from dataTranferTrace in the salesForce directory which looks specifically at salesForce dataTransfer definitions --->
	<cfif not isDefined("dataTransferDefs")>
		<cfset dataTransferDefs = application.com.dataTransfers.fn_get_DataTransferDefinition()>
	</cfif>
	
	<cfif not structKeyExists(form,"frmDtdID")>
		<cfset frmDtdID = dataTransferDefs.dtdID[1]>
	</cfif>
	
	<cfset dataTransferHistory = application.com.dataTransfers.getDataTransferHistory(dtdID=frmDtdID,maxRows=maxRows)>			<!--- 2013-03-15 PPB Case 434056 send maxRows to avoid java heap space issue --->
	
	<cfquery name="dataTransferHistory" dbType="query" maxrows="#maxRows#">				<!--- 2013-03-15 PPB Case 434056 --->
		select * from dataTransferHistory 
		where 1=1
		<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">	
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
	
	<cfform name="traceForm"  method="post">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmDtdID" currentValue="#frmDtdID#" display="functionName" value="dtdID" query="#dataTransferDefs#" label="Data Transfer Definitions" onChange="javascript:traceForm.submit()">
		</cf_relayFormDisplay>
	</cfform>
	
	<cfscript>
		// create a structure containing the fields below which we want to be reported when the form is filtered
		passThruVars = StructNew();
		StructInsert(passthruVars, "frmDtdID", frmDtdID);
	</cfscript>
	
	<cf_tableFromQueryObject 
		queryObject="#dataTransferHistory#"
		queryName="dataTransferHistory"
		sortOrder = "#sortOrder#"
		startRow = "#startRow#"
		numRowsPerPage="#numRowsPerPage#"
		useInclude="true"
		booleanFormat="isOK"
		keyColumnList="dthID"
		passThroughVariablesStructure = "#passThruVars#"
		keyColumnURLList="/dataloadsV2/dataTransfers/dataTransferTrace.cfm?dthID="
		keyColumnKeyList="dthID"
		keyColumnOpenInWindowList="yes"
		showTheseColumns="dthID,StartTime,EndTime,IsOK,MaxModRegisterID,Rows,ScheduledTaskLogID"
	>
					
<cfelse>

	<cfset dataTransferHistory = application.com.dataTransfers.getDataTransferHistory(dthID=dthID)>
	<cfif isJson(dataTransferHistory.trace[1])>
		<cfset traceStruct = deserializeJSON(dataTransferHistory.trace[1],"false")>
	<cfelse>
		<cfwddx action="wddx2cfml" input="#dataTransferHistory.trace[1]#" output="traceStruct">
	</cfif>

	<cfset maxRecCount = 10000>
	<cfif structKeyExists(traceStruct,"importObjects")>
		<cfset maxRecCount = traceStruct.importObjects.recordCount>
	</cfif>
	<cfif structKeyExists(traceStruct,"exportEntities") and traceStruct.exportEntities.recordCount gt maxRecCount>
		<cfset maxRecCount = traceStruct.exportEntities.recordCount>
	</cfif>
	
	<cfdump var="#traceStruct#" top="#maxRecCount#">
</cfif>