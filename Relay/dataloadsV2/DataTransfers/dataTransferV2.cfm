<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			dataTransfer.cfm	
Author:				NJH/AC
Date started:		2006-11-29
Description:		

dataTransferTypes
	1 - incremental export
	2 - full export
	3 - bulk insert
	4 - webservice export
	5 - file to DB

Amendment History:
Date (DD-MMM-YYYY)	Initials 	What was changed
26 FEB  2007 		AJC			made dirPath a parameter as lenovo data gets saved to different dir.
9 Aug   2007        SSS  		Added fileType checksum option
24 OCT  2007		NJH			used java classes for string manipulation and file writing. Much faster, especially for large files.
20 Dec  2007		AJC			Created webservice export definitiontype (4)
29 Oct	2008		NJH			CR-LEX578 - set rowCount to be the number of records returned in the query if the data transfer is a webservice.
11 Dec	2008		NYB			Fixed up 
2009-01-22			SSS			changed includeHeadings to false as default
2009-05-21			NJH			P-SNY063 - text qualify the columns if they contain the delimiters. Changed default of fileWriter param to be true.
2009-11-16			NJH			P-FNL079 - passed in functionArguments to the function, so that more arguments could be passed in.
2010-05-19 			NAS 		P-CLS002 - Added a param to be able to populate "Empty" fields with something meaningful
2010-11-02			NAS			LID4165: Onyx Synchronisation
2011-10-21			PPB			LEX060 hook to allow export to xml    
2012-01-27			NYB			Case#424794 added message to dataTransferStruct returned by datatransfers/filetodb, assigned message to errorMsg 

2012-01-27			NYB			Case#424794 added message to dataTransferStruct returned by datatransfers/filetodb, assigned message to errorMsg 

Possible enhancements:
 --->

<cfparam name="dtdID" type="numeric" default="-1">
<cfparam name="dirPath" default="">
<cfparam name="contentDelimiter" type="string" default="|">
<cfparam name="tableName" type="string" default="">
<cfparam name="appendData" type="boolean" default="false">
<cfparam name="maxErrors" type="numeric" default="0">
<cfparam name="useFileWriter" type="boolean" default="true"> <!--- NJH 2009-05-21 P-SNY063 - we should be using fileWriter --->  <!--- added because FileWriter.cfc is throwing an error --->
<!--- lenovo bug 1602 changed this to false for orginal behavoiur some clients do not want columns in the exports --->
<cfparam name="includeHeadings" type="boolean" default="false">
<cfparam name="writeBOM" type="boolean" default="true">
<cfparam name="functionArguments" type="struct" default="#structNew()#"> <!--- structure to hold arguments to be passed to the function --->
<!--- 2010-05-19 NAS P-CLS002 Added a param to be able to populate "Empty" fields with something meaningful --->
<cfparam name="nullDefault" type="string" default="">
<!--- 2010-11-02			NAS			LID4165: Onyx Synchronisation --->
<cfparam name="generateEmptyFile" type="boolean" default="True">

<cfif dtdID eq -1>
	<cfoutput>DtdID must be passed as a parameter</cfoutput>
	<CF_ABORT>
</cfif>

<cfset dtStartTime = createOdbcDatetime(application.com.dateFunctions.getDBDateTime())>
<cfset fileContent=ArrayNew(1)>
<cfset rowCount=0>
<cfset fileGenerated = 0>

<cfset functionArguments.dtdID = dtdID>
<cfset functionArguments.dtStartTime = dtStartTime>

<cfscript>
	// get data transfer definition information
	dtd=application.com.DataTransfers.fn_get_DataTransferDefinition(dtdID=dtdID);
	// get the max modregister id
	maxModID=application.com.dataTransfers.fn_get_MaxModRegisterID();
	maximumModRegisterID = maxModID.ID; // NJH 2009-06-04 P-SNY063
	// get the last data transfer that has a modregister ID
	lastDataTransfer=application.com.dataTransfers.fn_get_LastDataTransfer(directionTypeID=dtd.directionTypeID,dtdID=dtdid);
</cfscript>

<cfif dirPath is "">
	<cfset dirPath = "#application.paths.userFiles#\dataTransfers\#dtd.direction#\">
<cfelse>
	<!--- NJH 2008-03-05 if the directory path doesn't have a \ at the end, add one --->
	<!--- NJH 2009-06-02 P-SNY063 moved this up here - was originally in the import code --->
	<cfif right(dirPath,1) neq "\">
		<cfset dirPath = "#dirPath#\">
	</cfif>
</cfif>

<cfparam name="processedDir" type="string" default="#dirPath#processed">

<!--- NJH 2009-06-02 P-SNY063 - create directory if it doesn't exist. --->
<cfif not directoryExists(dirPath)>
	<cfset application.com.globalFunctions.CreateDirectoryRecursive(fullPath=dirPath)>
</cfif>

<cfif lastDataTransfer.recordCount eq 0>
	<cfset lastTransferMaxModID = 0>
<cfelse>
	<cfset lastTransferMaxModID = lastDataTransfer.maxModRegisterID>
</cfif>

<!--- NYB 2008-12-11 P-SNY055 trying to make the file work --->
<cfif not isdefined("dtd.GenerateFileType")>
	<cfquery name="dtd" dbtype="query">
		select *, 'utf-8' as GenerateFileType from dtd
	</cfquery>
</cfif>

<!--- export --->
<cfif dtd.directionTypeID eq 2>

	<!--- NJH - doing an incremental export --->
	<!--- AJC - doing an incremental webservice export --->
	<cfif lastTransferMaxModID lt maxModID.ID and (dtd.dataTransferType eq 1 or dtd.dataTransferType eq 4)>
		<cfscript>
			functionArguments.maxModRegisterID = lastTransferMaxModID;
			transferDataStruct = evaluate("application.com.dataTransfers.#dtd.functionname#(argumentCollection=functionArguments)");
		</cfscript>
				
	<!--- do a full export --->
	<cfelseif dtd.dataTransferType eq 2>
		<cfscript>
			transferDataStruct = evaluate("application.com.dataTransfers.#dtd.functionname#(argumentCollection=functionArguments)");
		</cfscript>
	</cfif>

	<!--- ANY FUNCTIONS RUN THAT RETURN A QUERY WILL NOW HAVE TO RETURN A STRUCTURE WITH .DATAQUERY CONTAINING THE QUERY
			OTHERWISE THE FILE WILL BE EMPTY --->
	<cfif isDefined("transferDataStruct") and isStruct(transferDataStruct) and structKeyExists(transferDataStruct,"dataQuery")>
		<cfset transferDataQuery = transferDataStruct.dataQuery>
	</cfif>
	
	<!--- AJC dataTransferType 4 is a webservice export and doesn't create a file --->
	<cfif dtd.dataTransferType neq 4>	
		<cfif isDefined("transferDataQuery")>
			<!--- 2011-10-21 PPB LEX060 hook to allow export to xml 
			 		requires a 2nd fn in dataTransfers.cfc named as the function specified in the DataTransferDefinition table with a _buildXML suffix eg fn_LifeRayExport_buildXML() --->
			<cfif dtd.fileextension eq "xml">	
				<cfif isdefined("application.com.dataTransfers.#dtd.functionname#_buildXML")>
					<cfset fileContent[1] = evaluate("application.com.dataTransfers.#dtd.functionname#_buildXML(transferDataQuery=transferDataQuery)")>
				<cfelse>
					<cfoutput>To export to an xml file you must define a #dtd.functionname#_buildXML() function in dataTransfers</cfoutput><br />
				</cfif>
			<cfelse>
				<!--- NYB 2008-12-11 P-SNY055 - added - trying to make the file work -> --->
				<cfscript>
					LOCAL = StructNew();
					LOCAL.ColumnArray = transferDataQuery.GetColumnNames();
					LOCAL.ColumnList = ArrayToList(LOCAL.ColumnArray,contentDelimiter);
				</cfscript>
	
				<!--- <- P-SNY055 --->
			
				<cfif transferDataQuery.RecordCount gt 0>
					<cfif dtd.RecordsPerFile is 0>
						<cfset RecordsPerFile = transferDataQuery.RecordCount>
					<cfelse>
						<cfset RecordsPerFile = dtd.RecordsPerFile>
					</cfif>
					<cfset rowCount=transferDataQuery.recordCount>
					<!--- number of files the export is to be split up into --->
					<cfset numberOfFiles=Ceiling(rowcount/RecordsPerFile)>
					<!--- counter for current files --->
					<cfset currentFileCount = 0>
					<!--- start & end row of the loop --->
					<cfset startrow = 1>
					<cfset endrow = RecordsPerFile>
					<!--- sets the number of elements in the array (1 per file) --->
					<cfset Arrayset(fileContent,  1,  numberOfFiles, "")>
	
					<cfloop index="xfileID" from="1" to="#numberOfFiles#">
						<cfset currentFileCount=currentFileCount+1>
						
						<!--- NYB 2008-12-11 P-SNY055 - moved to outside this loop
						GCC 2008-12-17 put back because this is a real object - otherwise we would continually append content to object 1 instead of the correct behaviour
						--->
						<cfscript>
							myFileContent = createObject('component', '/relay/com/StringBuffer').init();
							if (includeHeadings) {
								myFileContent.append(LOCAL.ColumnList&chr(13)&chr(10));
							}
						</cfscript>
	
	
						<cfset loopcounter = 0>
						<cfloop query="transferDataQuery" startrow="#startrow#" endrow="#endrow#">
							<cfset loopcounter = loopcounter + 1>
							<!--- NYB 2008-12-11 P-SNY055 - added - trying to make the file work -> --->
							<cfset modifiedEntity = "">
							<cfloop index="i" list="#LOCAL.ColumnList#" delimiters="#contentDelimiter#">
								<!--- 2009-05-21 NJH P-SNY063 text qualify the column if it contains the delimiter, control/line feed characters or a double quote--->
								<cfset currentColumnText = evaluate(i)>
								<!--- 2010-05-19 NAS P-CLS002 Added a param to be able to populate "Empty" fields with something meaningful --->
								<cfif nullDefault neq "" and currentColumnText EQ "">	
									<cfset currentColumnText = nullDefault>
								</cfif>
								<cfif find(contentDelimiter,currentColumnText) or find("#chr(13)##chr(10)#",currentColumnText) or find(chr(34),currentColumnText)>
									<cfset currentColumnText = replace(currentColumnText,chr(34),"#chr(34)##chr(34)#","ALL")>
									<cfset currentColumnText = chr(34)&currentColumnText&chr(34)>
								</cfif>
								<cfset modifiedEntity = ListAppend(modifiedEntity,currentColumnText,contentDelimiter)>
							</cfloop>
							<!--- <- P-SNY055 --->
							<!--- NJH 2007-10-24 - using java string functions. They increase the speed dramatically --->
							<cfif loopcounter NEQ endrow>
								<cfscript>
									myFileContent.append(modifiedEntity&chr(13)&chr(10));
								</cfscript>
							<cfelse>
								<cfscript>
									myFileContent.append(modifiedEntity);
								</cfscript>
							</cfif>
							<!--- <cfset fileContent[#currentFileCount#]=fileContent[#currentFileCount#]&modifiedEntity>
							<cfif currentRow neq endrow>
								<cfoutput><cfset fileContent[#currentFileCount#] = fileContent[#currentFileCount#] & "#CHR(13)##CHR(10)#"></cfoutput>
							</cfif> --->
						</cfloop>
						<cfset startrow = startrow + RecordsPerFile>
						<cfset endrow = endrow + RecordsPerFile>
						<cfscript>
							fileContent[currentFileCount] = myFileContent.getString();
						</cfscript>
					</cfloop>
				<!--- NYB 2008-12-11 P-SNY055 - added - trying to make the file work -> --->
				<cfelse>
					<cfif includeHeadings>
						<!--- NYB 2008-01-16 P-SNY055 -> added (was originally moved outside if [above] put got put back causing error) --->
						<cfscript>
							myFileContent = createObject('component', '/relay/com/StringBuffer').init();
							if (includeHeadings) {
								myFileContent.append(LOCAL.ColumnList&chr(13)&chr(10));
							}
						</cfscript>
						<!--- <- NYB 2008-01-16 P-SNY055 --->
						<cfset fileContent[1] = myFileContent.getString()>
					</cfif>
				<!--- <- P-SNY055 --->
				</cfif>
			</cfif>
		<cfelse>
			<!--- <cfoutput>transferDataQuery not defined</cfoutput> --->	
			<cfoutput>There is no data to export</cfoutput><br />							<!--- 2011-10-21 PPB LEX060 changed message to be more meaningful --->
			<!--- 2010-11-02			NAS			LID4165: Onyx Synchronisation --->
			<cfif not generateEmptyFile>
				<CF_ABORT>
			</cfif>
		</cfif>
	
		<cfswitch expression="#dtd.filenameSuffix#">
			<cfcase value="DTDID">
				<cfset filesuffix=dtdID>
			</cfcase>
			<cfcase value="Date">
				<cfif dtd.dateMask neq "">
					<cfset filesuffix=DateFormat(dtStartTime,"#dtd.dateMask#")>
					<cfif FindNoCase("nn", dtd.dateMask) is not 0>
						<cfset filesuffix=ReplaceNoCase(filesuffix, "nn", timeformat(dtStartTime, "mm"))>
					</cfif>
				<cfelse>
					<cfset filesuffix=DateFormat(dtStartTime)>
				</cfif>
			</cfcase>
			<!--- NJH adding a case for no suffix --->
			<cfcase value="">
				<cfset filesuffix="">
			</cfcase>
			<cfdefaultcase>
				<cfset filesuffix=dtdID>
			</cfdefaultcase>
		</cfswitch>
	
		<cfset numberoffiles=arraylen(fileContent)>
		<cfif numberoffiles is 0>
			<cfset numberoffiles=1>
			<cfset Arrayset(fileContent,  1,  1, "")>
		</cfif>
		
		<cfloop index="fileID" from="1" to="#numberoffiles#">
			<!--- HACK: NJH don't want the fileID when exporting just 1 file... probably not the best way of doing this
				 --->
			<cfset filename="">
			
			<cfif numberOfFiles eq 1 and dtd.RecordsPerFile eq 0>
				<cfif fileSuffix neq "">
					<cfset filename = "#dtd.filename#_#filesuffix#.#dtd.Fileextension#">
				<cfelse>
					<cfset filename = "#dtd.filename#.#dtd.Fileextension#">
				</cfif>
			<cfelse>
				<cfif fileID eq 1>
					<cfset filename = "#dtd.filename#_#filesuffix##fileID#.#dtd.Fileextension#">
				<cfelse>
					<cfset filename = "pending\#dtd.filename#_#filesuffix##fileID#.#dtd.Fileextension#">
				</cfif>
			</cfif>
			
			<cfset filePath = dirPath&filename>
			<cfdump var="filePath=#filePath#"><br />		<!--- useful for investigations --->
			
			<cfif dtd.GenerateOkFile>
				<cfset okFilePath = dirPath&"#dtd.filename#_#filesuffix##fileID#.ok">
			</cfif>
			<cftry>
				<!--- Create File --->
				<cfset exportContent=evaluate("fileContent[#fileID#]")>
				
				<!--- NJH 2007-10-24 using fileWriter component as it much faster than cffile, especially for large files --->
				<cfif useFileWriter and listContainsNoCase("utf-8#application.delim1#UTF-16LE#application.delim1#UTF-16BE", dtd.GenerateFileType) GT 0>
					<cfscript>
						myOutFile = createObject('component', '/relay/com/FileWriter').init(filePath, dtd.GenerateFileType, 131072, writeBOM);
						myOutFile.writeLine(exportContent);
						myOutFile.close();
					</cfscript>
				<cfelse>
					<cftry>
						<cffile action="write" addnewline="no" charset="#dtd.GenerateFileType#" file="#filePath#" output="#exportContent#">
						<cfcatch type="any">
							<cfdump var="#cfcatch#">
						</cfcatch>
					</cftry>
				</cfif>
				
				<cfif dtd.GenerateOkFile>
					<cfif fileExists(filePath)>
						<cfset fileGenerated = 1>
						<cfif dtd.okFileType EQ "checksum">
							<cffile action="read" charset="#dtd.GenerateFileType#" file="#filePath#" variable="exportFile">
							<cfscript>
								//gets the first 1024 byte characters of the file
								First1024Bytes = exportFile.getbytes();
								digesterBase = createObject("Java","java.security.MessageDigest");
								digester = digesterBase.getInstance("MD5");
								digester.reset();
								digester.update(First1024Bytes);
								value = digester.digest();
								CheckSumValue = BinaryEncode(value,"HEX");
							</cfscript>
							<cfset CheckSumValueLowerCase = LCase(CheckSumValue)>
							<cffile action="write" file="#okFilePath#" output="#CheckSumValueLowerCase#">
						<cfelse>
						<cfif filesuffix EQ "">
							<cfset FileNameForBytes = '#dtd.filename#.#dtd.Fileextension#'>
						<cfelse>
							<cfset FileNameForBytes = '#dtd.filename#_#filesuffix#.#dtd.Fileextension#'>
						</cfif>
			 				<cfdirectory action="list" filter="#FileNameForBytes#" directory="#dirPath#" sort="dateLastModified DESC" name="fileDir">
							<cfset fileSize = fileDir.size[1] & " bytes">
							<cffile action="write" file="#okFilePath#" output="#fileSize#">
						</cfif>						
					</cfif>
				</cfif>
				<cfcatch>
					<cfoutput>#htmleditformat(cfcatch.message)# #htmleditformat(cfcatch.type)#</cfoutput> <CF_ABORT>
				</cfcatch>
			</cftry>
		</cfloop>
		
	<!--- NJH 2008-10-29 CR-LEX578 if a data transfer type of 4, then set rowCount as the number of records returned in the query --->
	<cfelse>
		<cfif isDefined("transferDataQuery")>
			<cfset rowCount = transferDataQuery.recordCount>
		</cfif>
		<cfset fileName = "">
	</cfif>


<!--- doing an import --->
<cfelseif dtd.directionTypeID eq 1>

	<cfset fileContents = "">

	<cfparam name="filename" type="string" default="#dtd.filename#.#dtd.fileextension#">
	<cfset fullFileName = "#dirPath##filename#">
	
	<cfscript>
		transferDataStruct = structNew();
		transferDataStruct.isOK = true;
		transferDataStruct.message = "";
		transferDataStruct.rowCount = 0;
		transferDataStruct.recordsProcessed = 0;
	</cfscript>

	 <!--- running a bulk insert  - used for large file imports. If any errors occurs, the import is rejected--->
	<cfif dtd.dataTransferType eq 3>
		
		<cfif tableName neq "">

			<cftry>
				<cfquery name="loadFileData" datasource="#application.siteDataSource#">
					<cfif not appendData>
						truncate table #tableName#
					</cfif>
					
					--sp_bulkInsert @tableName='#tableName#', @filename='#fullFileName#', @FieldTerminator='#contentDelimiter#',@MaxErrors=#maxErrors#
					
					BULK INSERT #tableName# FROM '#fullFileName#'
	                	WITH (dataFileType = 'char',
								FieldTerminator =  <cf_queryparam value="#contentDelimiter#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
								RowTerminator = '\n',
								Codepage = 'ACP',
								MaxErrors =  <cf_queryparam value="#maxErrors#" CFSQLTYPE="cf_sql_integer" > )
					
					--select @@rowCount as rowsInserted
					--select @@ERROR as insertError
				</cfquery>
			
				<cfcatch>
					<cfset errorMsg="There were errors running the bulk insert.">
				</cfcatch>
			
			</cftry>

			<cfquery name="getRecords" datasource="#application.siteDataSource#">
				select * from #tableName#
			</cfquery>
			
			<cfset rowCount = getRecords.recordCount>
			<cfset fileContents = getRecords>

			<cfif rowCount eq 0>
				<cfset errorMsg = "Errors were encountered running the bulk insert.">
			</cfif>
			
		<cfelse>
			<cfset errorMsg = "Parameter tablename must be set to do a bulk insert.">
		</cfif>
	
	<!--- NJH 2009-06-05 P-SNY063 - added dataTransferType of 5 which uses the fileToDB function --->	
	<cfelseif dtd.dataTransferType eq 5>
	
		<cfparam name="endRowDelim" type="string" default="#chr(13)##chr(10)#">
		<cfparam name="tableOptions" type="string" default="">
		<cfparam name="columnNames" type="boolean" default="true">
		<cfparam name="validateFile" type="boolean" default="false">
		<cfparam name="textQualifier" type="string" default="#chr(34)#"> <!--- double quote --->
		<cfparam name="deleteQryWhereClause" type="string" default="">
		<cfparam name="setCreated" type="boolean" default="false">
		<cfparam name="sourceCollectionMethod" type="string" default="absolutePath">
		<cfparam name="noCheckLoadData" type="boolean" default="true">
		<cfparam name="defaultPrecisionSize" type="numeric" default="255">
		<cfparam name="increment" type="numeric" default="99">
		<cfparam name="httpDirPath" type="string" default="">
	
		<cfscript>
			argumentsCollection = structNew();
			if (sourceCollectionMethod eq "http") {
				argumentsCollection.dir=httpDirPath;
			} else {
				argumentsCollection.dir=dirPath;
			}
			argumentsCollection.filename=filename;
			argumentsCollection.endRowDelim=endRowDelim;
			argumentsCollection.delim=contentDelimiter;
			argumentsCollection.tablename=tablename;
			argumentsCollection.tableOptions=tableOptions;
			argumentsCollection.columnNames=columnNames;
			argumentsCollection.validateFile=validateFile;
			argumentsCollection.textQualifier=textQualifier;
			argumentsCollection.deleteQryWhereClause=deleteQryWhereClause;
			argumentsCollection.setCreated=setCreated;
			argumentsCollection.sourceCollectionMethod=sourceCollectionMethod;
			argumentsCollection.noCheckLoadData=noCheckLoadData;
			argumentsCollection.defaultPrecisionSize=defaultPrecisionSize;
			argumentsCollection.increment=increment;
			
			if (isDefined("fieldnames")) {
				argumentsCollection.fieldnames=fieldnames;
			}
			if (isDefined("numberElementsExpected")) {
				argumentsCollection.numberElementsExpected=numberElementsExpected;
			}
			if (isDefined("charset")) {
				argumentsCollection.charset=charset;
			}
		
			dataTransferStruct = application.com.dataTransfers.fileToDB(argumentCollection=argumentsCollection);
				
			fileContents = dataTransferStruct.importQuery;
			rowCount = dataTransferStruct.numRowsInserted;
		</cfscript>

		<!--- START: 2012-01-27	NYB	Case#424794 - added --->
		<cfif (not dataTransferStruct.isOK) and dataTransferStruct.message neq "">
			<cfset errorMsg = dataTransferStruct.message>
		</cfif>
		<!--- END: 2012-01-27	NYB	Case#424794 --->
		
		<!--- START: 2012-01-27	NYB	Case#424794 - added --->
		<cfif (not dataTransferStruct.isOK) and dataTransferStruct.message neq "">
			<cfset errorMsg = dataTransferStruct.message>
		</cfif>
		<!--- END: 2012-01-27	NYB	Case#424794 --->
		
	<!--- NJH 2009/10/21 P-FNL079 - dataTransferType 4 is webservice import, so just run function --->
	<cfelseif dtd.dataTransferType eq 4>
	<cfelse>
	
		<cftry>
			<cfif fileExists("#fullFileName#")>
				<cffile action="read" file="#fullFileName#" variable="fileContents">
			<cfelse>
				<cfset errorMsg="#fullFileName# does not exist.">
			</cfif>
		
			<cfcatch>
				<cfset errorMsg="#cfcatch.message# #cfcatch.type#">
			</cfcatch>
		</cftry>
	</cfif>

	
	<!--- pass in either the file contents as a query or the tablename where the data is stored. 
		NJH 2014/09/12 - append functionArguments to importFunctionArgs.
	--->
	<cfif not isDefined("errorMsg") or (isDefined("errorMsg") and errorMsg eq "")>
		<cfscript>
			/* NJH 2009-10-21 P-FNL079 - added dtdID to function */
			importFunctionArgs = structNew();
			importFunctionArgs.dtStartTime = dtStartTime;
			importFunctionArgs.fileContents=fileContents;
			importFunctionArgs.tablename=tablename;
			importFunctionArgs.dtdID=dtdID;
			importFunctionArgs.maxModRegisterID=lastTransferMaxModID;
			structAppend(importFunctionArgs,functionArguments);
			transferDataFuncStruct = evaluate("application.com.dataTransfers.#dtd.functionname#(argumentCollection=importFunctionArgs)");
			structAppend(transferDataStruct,transferDataFuncStruct);
			
			// if we didn't process the full load for whatever reason.
			/*if (transferDataStruct.recordsProcessed neq transferDataStruct.rowCount) {
				transferDataStruct.message = "Error in processing file #fullFileName#. Only processed #transferDataStruct.recordsProcessed# out of #rowCount#.";
				transferDataStruct.isOK = false;
			}*/
		</cfscript>
		
		<cfif dtd.moveProcessedFile and transferDataStruct.isOK>
			<cftry>
				<cfif not directoryExists(processedDir)>
					<cfset application.com.globalFunctions.CreateDirectoryRecursive(fullpath=processedDir)>
				</cfif>
				
				<cffile action="move" destination="#processedDir#" source="#fullFileName#">

				<cfcatch>
					<cfset transferDataStruct.message = "Error in moving file #fullFileName# to #processedDir#. #cfcatch.detail# #cfcatch.message#">
					<cfset transferDataStruct.isOK = false>
				</cfcatch>
			</cftry>
		</cfif>
	<cfelse>
		<cfscript>
			transferDataStruct.isOK = false;
			transferDataStruct.message = errorMsg;
		</cfscript>
	</cfif>
	
	<cfset fileGenerated = 0>
	<cfset maximumModRegisterID = 0> <!--- set this to 0 as we're not using modRegister for importing data --->
	
</cfif>

<cfset dtEndTime = createOdbcDatetime(application.com.dateFunctions.getDBDateTime())>
	
<cfscript>
	// insert new record into the data transfer history table
	argumentsCollection = structNew();
	argumentsCollection.dtdID = dtdID;
	argumentsCollection.dtStartTime = dtStartTime;
	argumentsCollection.dtEndTime = dtEndTime;
	argumentsCollection.rowCount = rowCount;
	argumentsCollection.maxModID = maximumModRegisterID;
	argumentsCollection.fileGenerated = fileGenerated;
	argumentsCollection.filename = filename;
	if (isDefined("transferDataStruct") and structKeyExists(transferDataStruct,"isOK")) {
		argumentsCollection.isOK = transferDataStruct.isOK;
	}
	if (isDefined("transferDataStruct") and structKeyExists(transferDataStruct,"traceStruct")) {
		argumentsCollection.traceStruct = transferDataStruct.traceStruct;
	}

	/* WAB 2015-06-22 changed name of scheduledTaskID request variable {became structure request.scheduledTask}*/
	if (structKeyExists(request,"scheduledTask")) {
		argumentsCollection.scheduledTaskLogID = request.scheduledTask.scheduledTaskLogID;
	}
	
	application.com.dataTransfers.fn_ins_DataTransferHistory(argumentCollection=argumentsCollection);
</cfscript>