<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2007-10-02   WAB changes to add dataLoadTypeTable

 --->

<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="Received desc">

<cfif isDefined("editor") and editor eq "yes">
	<cfinclude template="dataLoadEditor.cfm">
<cfelse>
	<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
	<cfset editor = "no">
	<cfparam name="showComplete" default="0">
	<!--- <cfinclude template="dataloadsTopHead.cfm"> --->

	<cfif isDefined("setComplete")
		and isDefined("datasourceID") and datasourceID gt 0>
		<cfquery name="getloadtable" datasource="#application.siteDataSource#">
			Select loadtable from datasource where datasourceID =  <cf_queryparam value="#datasourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfif setComplete eq "OK">
			<cfquery name="setComplete" datasource="#application.siteDataSource#">
				update dataSource set dateLoaded = getdate()
				where datasourceID =  <cf_queryparam value="#datasourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
				exec sp_archiveTable @tableName =  <cf_queryparam value="#getloadtable.loadtable#" CFSQLTYPE="CF_SQL_VARCHAR" > , @method = 'put'
			</cfquery>
		<cfelseif setComplete eq "undo">
			<cfquery name="setUnComplete" datasource="#application.siteDataSource#">
				update dataSource set dateLoaded = null
				where datasourceID =  <cf_queryparam value="#datasourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
				exec sp_archiveTable @tableName =  <cf_queryparam value="#getloadtable.loadtable#" CFSQLTYPE="CF_SQL_VARCHAR" > , @method = 'get'
			</cfquery>
		</cfif>
	</cfif>

	<cfquery name="checkDataLoadView" datasource="#application.siteDataSource#">
		if exists (select TABLE_NAME from INFORMATION_SCHEMA.VIEWS
				where TABLE_NAME = 'vDataLoads' and table_schema='dbo')
			DROP VIEW vDataLoads
	</cfquery>

	<cfquery name="creatDataLoadView" datasource="#application.siteDataSource#">
		CREATE VIEW dbo.vDataLoads AS
		select description, countryDescription as default_country,
			loadtable as load_table, datasourceID, dateReceived as Received,
			dateLoaded as loaded, dlt.name as data_type, dlt.loadTypeGroup,  ISOCode, TypeTextID as organisation_Type
			from
			dataSource ds
				Left join dataLoadType dlt on ds.dataloadTypeID = dlt.dataloadTypeID
			left outer join organisationtype on ds.organisationtypeID = organisationtype.organisationtypeID
			left outer join country on defaultCountryID = countryID
	</cfquery>


	<CFTRY>
		<!--- LID 4690 NJH 2011/01/21 don't show 'Done' link if underlying dataload table doesn't exist (ie. set it to ''). Not entirely sure if this is the best
			method as I don't know dataloads.. There may be a better way. --->
		<cfquery name="getDataSources" datasource="#application.siteDataSource#">
			select description, default_country, load_table, datasourceID, Received,
			case when not exists(select 1 from sysObjects where name=load_table and type='u') THEN '' when loaded is null then 'OK' else 'Undo' end as Done, loaded, data_type, Organisation_Type,
			case when exists(select 1 from sysObjects where name=load_table and type='u') then 'Columns' else '' end as Columns,
			case when exists(select 1 from sysObjects where name=load_table and type='u') then 'Mappings' else '' end as Mappings,
			case when exists(select 1 from sysobjects where name=load_table and type='u') then 'Functions' else '' end as Functions,
			case when exists(select 1 from sysobjects where name=load_table and type='u') then 'Analyse' else '' end as Field_Counts
			from vDataLoads
			where 1=1
			<cfif isDefined("filterSelect") and FilterSelect neq ""
				and isDefined("FilterSelectValues") and FilterSelectValues neq "">
				AND #FilterSelect# =  <cf_queryparam value="#FilterSelectValues#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		 	<cfif isdefined("showComplete") and showComplete eq "0">
				AND loaded is null
			</cfif>
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortorder#">
		</cfquery>
		<cfcatch type="Database"><p><cfoutput>#htmleditformat(cfcatch.Message)# #htmleditformat(cfcatch.detail)# <br>#htmleditformat(cfcatch.NativeErrorCode)#    </cfoutput></p></cfcatch>
	</CFTRY>

	<CF_tableFromQueryObject
		queryObject="#getDataSources#"

		keyColumnList="description,done,Columns,Mappings,Functions,Field_Counts"
		keyColumnURLList="index.cfm?editor=yes&datasourceid=,index.cfm?setComplete=#getDataSources.done#&datasourceid=,dataloadList.cfm?action=viewCols&tableName=,manageMisMatchedCols.cfm?tableName=,manageDataLoad.cfm?tableName=,columnCounts.cfm?tablename="
		keyColumnKeyList="datasourceID,datasourceID,Load_Table,Load_Table,Load_Table,Load_Table"

		hideTheseColumns="datasourceID"
		showTheseColumns="Default_Country,Description,Load_Table,Data_Type,Organisation_Type,Done,Received,Loaded,Columns,Mappings,Functions,Field_Counts"

		checkBoxFilterName="showComplete"
		checkBoxFilterLabel="Show Complete"

		FilterSelectFieldList="Data_Type,Default_Country"
		dateFormat="Received,Loaded"
		sortOrder="#sortorder#">
	</cfif>

