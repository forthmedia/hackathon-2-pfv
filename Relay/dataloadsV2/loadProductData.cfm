<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			productDataload.cfm	
Author:				SWJ
Date started:		2004-01-22
	
Description:		Used to automate the loading of product data

How it works
A view called vSnapshotRelayProducts is used to expose Updates, Inserts and Deletes.  

The incoming data is overlaid to expose differences between it and the data already in Relay.  
The Inserts derive their records from a full outer join.  This join on its own exposes all 
the differences against the key fields in each snapshot.
The join is assisted by the where clause in separating out specific types of difference; 
Insert, Update or Delete.
For example the filter may be run to expose only records which occur in the now snapshot 
but do not occur in the past snapshot.  These represent records, which have been added 
into the database in the period between past and now, hence the new records. 


1.	Check the product staging table.
2.	Copy the incoming product data into a load table which has a date appended to the table name.
3.	Add control columns which will be used for managing and reporting on that data load.
4.	Validate and prepare the incoming data (see Input Validation below).
5.	Apply country logic to the incoming data (see setting country id below).
6.	Run comparison against the product data in Relay to determine inserts, updates and deletes.
7.	Insert new rows
8.	Update changed rows
9.	And mark for deletion rows that are missing from the incoming data but present in the view.
10.	Truncate product staging table and archive data load table into Admin db


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cf_title>Load Product Data</cf_title>

<cfquery name="checkProductLoadView" datasource="#application.siteDataSource#">
	if exists (select TABLE_NAME from INFORMATION_SCHEMA.VIEWS 
			where TABLE_NAME = 'vSnapshotRelayProducts') 
		DROP VIEW vSnapshotRelayProducts 
</cfquery>

<cfquery name="createSnapshotRelayProductsView" datasource="#application.siteDataSource#">
	CREATE VIEW vSnapshotRelayProducts AS
	select SKU, campaignID, productID, Description, deleted, remoteProductID, countryid
		from product
</cfquery>

<cfquery name="checkProductLoadTable" datasource="#application.siteDataSource#">
	select productID from productLoadTable 
</cfquery>

<cfif checkProductLoadTable.recordCount eq 0>
	No products to load.
	<CF_ABORT>
</cfif>

<cfquery name="checkProductLoadTable" datasource="#application.siteDataSource#">
	select productID from productLoadTable 
</cfquery>
