<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		movePersonToDiffOrg.cfm
Author:			SWJ
Date created:	2001-11-17

	Objective - to move one or mor people from one to a new organisation record.
				Steps involved are:
				a) receive a list of personID's
				b) 
				
Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-11-12          ACPK        Replaced tables with bootstrap

Enhancement still to do:



--->

<CFPARAM NAME="frmOrgId" TYPE="string" DEFAULT="0">
<cfparam name="frmSearchString" type="string" default="">


<!--- *********************************************************************
	  we're searching for the organisation to link them to --->
<cfif isDefined('form.frmSearch') eq "yes">
	<CFQUERY NAME="getOrganisation" datasource="#application.siteDataSource#">
		select organisationName+' ('+ISOCode+', '+postalCode+', '+l.sitename+')' as displayValue, 
		convert(varchar(15),o.organisationID)+'-'+convert(varchar(15),l.locationID) as IDValue 
		from organisation o inner join location l on
		o.organisationID = l.organisationID
		inner join country c on
		l.countryID = c.countryID
		where organisationName  like  <cf_queryparam value="#form.frmSearchString#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>
</CFIF>

<!--- *********************************************************************
	  we're doing the update --->
<cfif isDefined('form.frmMove') and form.frmMove eq "move">
	<CFQUERY NAME="updateOrganisation" datasource="#application.siteDataSource#">
		<!--- update person 
		set organisationID = #ListFirst(form.frmIDs,"-")#,
		locationID = #ListLast(form.frmIDs,"-")#
		where personid in (#form.frmPersonIDs#) --->
	</CFQUERY>
	<CFSET message = "#form.frmPersonIDs# moved to organisationID #ListFirst(form.frmIDs,'-')# and locationID #ListLast(form.frmIDs,'-')#">
</CFIF>

<cf_title>Move Person</cf_title>

<p>Find Existing Organisation</p>

<!---	<TR>
		<TD>People with the following personIDs will be moved:<BR>
		<CFOUTPUT>#frmSearchString#</CFOUTPUT>
		</TD>
	</TR>
 	<!--- *********************************************************************
	  we don't have any personIDs so we first need to search for them --->
	<CFIF frmSearchString eq "">
		<CFFORM ACTION="findRelayOrgToLinkTo.cfm" METHOD="POST" NAME="searchOrgForm">
			<TR>
				<TD VALIGN="top" CLASS="label">PersonID</TD>
				<TD><CFINPUT TYPE="Text" NAME="frmPersonIDs" MESSAGE="You must put a valid personID" REQUIRED="Yes" SIZE="15"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="submit" VALUE="Search"></TD>
			</TR>
		</CFFORM> --->
		
<!--- *********************************************************************
	  we've got personID's but the getOrganisations has not been executed 
	  so we need the user to put in an orgName --->
	<CFIF frmSearchString neq "">
		<CFFORM ACTION="findRelayOrgToLinkTo.cfm" METHOD="POST" CLASS="form-horizontal" NAME="orgSearchForm">
			<div class="form-group">
			     <div class="col-xs-12">
				    <label for="frmSearchString">Search for existing Organisations:</label>
				    <cfinput type="Text" id="frmSearchString" class="form-control" name="frmSearchString" value="#frmSearchString#" message="You must put a valid personID" required="Yes" size="15">
				 </div>
		    </div>
		    <div class="form-group">
                 <div class="col-xs-12">
				    <INPUT class="btn-primary btn" TYPE="submit" NAME="frmSearch" VALUE="Search">
				</div>
			</div>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="dataLoadRowIDToUpdate" VALUE="#dataLoadRowIDToUpdate#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
<!--- *********************************************************************
	  we've got personID's but the getOrganisations has not been executed 
	  so we need the user to put in an orgName --->
	<CFIF frmSearchString neq "" and isDefined('getOrganisation.recordCount') 
			and getOrganisation.recordCount gt 0 and not isDefined('form.frmMove')>
		<CFFORM ACTION="findRelayOrgToLinkTo.cfm" METHOD="POST" CLASS="form-horizontal" NAME="orgSearchForm">
			<div class="form-group">
                 <div class="col-xs-12">
					<label for="frmIDs">Select which org to link them to:</label>
					<CFSELECT NAME="frmIDs"
					      ID="frmIDs"
					      CLASS="form-control"
				          SIZE="15"
				          MESSAGE="You must select an organisation"
				          QUERY="getOrganisation"
				          VALUE="IDValue"
				          DISPLAY="displayValue"
				          REQUIRED="Yes"></CFSELECT>
			     </div>
            </div>
			<div class="form-group">
                 <div class="col-xs-12">
				    <INPUT CLASS="btn-primary btn" TYPE="submit" NAME="frmMove" VALUE="move">
				 </div>
            </div>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="dataLoadRowIDToUpdate" VALUE="#dataLoadRowIDToUpdate#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
	
	<CFIF isDefined('message')>
	   <cfif message neq ""><CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT></cfif>
	</CFIF>
	
