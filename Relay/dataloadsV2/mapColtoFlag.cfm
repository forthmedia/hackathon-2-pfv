<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			showDistinctColValues.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008/09/17    WAB			Altered to deal with columns containing comma delimited lists of flags (checkboxes)
2009/03/02   WAB			Major changes (old version saved as separate file).  Attempt to make UI easier
2015-11-13          ACPK        Added Boostrap and removed unneeded tables

Possible enhancements:


 --->

<!--- <cfinclude template="dataloadsTopHead.cfm"> --->
<cfparam name="pagesBack" default="1">
<cfparam name="message" default="">
<cfparam name="entityTypeID" default="">
<cfparam name="frmFlagGroupID" default="">
<cfparam name="frmFlagID" default="">
<cfparam name="frmAnyTrue" default="0">


<cf_querySim >
entityQuery
entityTypeID,entityType
0|Person
1|Location
2|Organisation
</cf_querysim>


<cffunction name="outputFlagGroupMappingForm">
	<cfargument name="MappingQuery" type="query" required="true">
	<cfargument name="flagGroupID" type="numeric" required="true">
	<cfargument name="columnName" type="string" required="true">
	<cfargument name="TableName" type="string" required="true">

	<cfset profileAttributeList = application.com.flag.getFlagGroupFlags(FlagGroupId =  FlagGroupID)>

    <cfoutput>
		<form class="form-horizontal" action="" method="post">
			<CF_INPUT type="hidden" name="addDistinctFlagMappings" value="#MappingQuery.recordcount#">
			<CF_INPUT type="hidden" name="frmFlagGroupID" value="#FlagGroupID#">
			<CF_INPUT type="hidden" name="columnName" value="#columnName#">
			<CF_INPUT type="hidden" name="TableName" value="#tableName#">
			<table class="table table-hover">
				<tr>
					<th>Value</th>
					<th>## of <BR>Instances</th>
					<th>Map To<BR>Attribute</th>
					<th><p>Click to Create New Attribute</p><p>(Enter name if not same as value)</p></th>
				</tr>
				<cfloop query="MappingQuery">
					<tr>
						<td>#htmleditformat(DataVALUE)#</td>
						<td>#htmleditformat(number_of_rows)#</td>
						<cfif dataValue is not "">
							<td>
								<div class="form-group">
									<div class="col-xs-12">
										<CF_INPUT type="hidden" name="flagMappingData_#currentRow#" value="#DataVALUE#">
										<select class="form-control" name="flagMappingFlagID_#currentRow#">
											<option value="">Select Flag</option>
											<CFSET CURRENTvaluefound = false>
											<cfset suggestedFlagID =flagID>
											<cfloop query="profileAttributeList">
												<option value="#flagID#" <cfif suggestedFlagID is flagid><cfset currentvaluefound = true>selected </cfif> >#name#</option>
											</cfloop>
										</select>
								     </div>
								</div>
							</td>
							<cfif suggestedFlagID is "">
								<td>
									<div class="form-group">
										<div class="col-xs-12">
											<div class="checkbox">
											  <CF_INPUT type="checkbox" name="addNewFlag" value="#CURRENTROW#">
											</div>
										</div>
	                                    <div class="col-xs-12">
										   <CF_INPUT TYPE="TEXT" CLASS="form-control" NAME ="NewFlagName_#currentRow#">
										</div>
									</div>
								</td>
							</cfif>
						</cfif>
					</tr>
				</cfloop>
			</table>
		    <input class="btn-primary btn" type="submit" value = "Create Mappings">
		</form>
	</cfoutput>
</cffunction>

<cfscript>
// create an instance of the object
myObject = createObject("component","relayDataloadFlagsV2");
myObject.initTable (datasource = application.siteDataSource, tablename = tablename);

hasExistingMapping = false ;
hasSuggestedMapping = false ;
startedManualMapping = false ;


if (isDefined("removeColumnMapping")) {
		myObject.deleteDataLoadMapping(loadTableColumn = removeColumnMapping);
		columnName = removeColumnMapping ;
}

if (isDefined("AddFlagMapping")) {
			myObject.addDataLoadSingleFlagMapping(loadTableColumn = columnName, flagID = AddFlagMapping, automapped = 0 , anyTrue = #iif(frmAnyTrue is 1,1,0)#);
			message = myObject.message;
}

if (isDefined("confirmColumnAutoMapping")) {
			myObject.confirmAutoMapping(loadTableColumn = confirmColumnAutoMapping);
			columnName = confirmColumnAutoMapping;
}


if (isDefined("addDistinctFlagMappings")) {

// i.e. we've submitted the form with a flaggroup id

		message = "" ;
		if(isDefined("addNewFlag")) {
			for (i =1; i lte listlen(addnewFlag);i = i+1) {
				item= listGetAt(addNewFlag,i);
				if (form["newFlagname_#item#"] is not "") {name = form["newFlagname_#item#"] ;} else { name = form["flagMappingData_#item#"]; }
				newFlagID =  application.com.flag.createFlag	(
									flagGroup  = frmFlagGroupID,
									flagType  = application.com.flag.getflaggroupstructure(frmFlagGroupID).flagTypeID,
									name = name,
									createDefaultTranslationName = true	);

			}

		}


		for(i = 1; i lte addDistinctFlagMappings; i = i+1){
			if (structKeyExists(form,"flagMappingData_#i#")) {
				myObject.loadTableColumn = columnName;
				if (form["flagMappingFlagID_#i#"] is not "") {
					myObject.addDataLoadFlagMapping(loadTableColumn = columnName,loaddata = form["flagMappingData_#i#"],flagid = form["flagMappingFlagID_#i#"],singleflag = false) ;
					message =+ myObject.message;
				}
			//	myObject.insertLogData(tablename,Columnname,thisdistinctValue, thisFlagID);


			}
		}


}

if (isDefined("addFlagGroupMapping")) {
	myObject.addDataLoadFlagGroupMapping(loadTableColumn = columnName, flagGroupID = ADDFLAGGROUPMAPPING, automapped = 0 );
}


	thisColumnFlagMapping = myObject.listDataloadFlagMapping(loadTableColumn = columnName);

	if (thisColumnFlagMapping.recordCount is not 0)  {
		hasExistingMapping = true ;
	}



if (hasExistingMapping)  {
	entityTypeID = thisColumnFlagMapping.entityTypeID ;
	if(thisColumnFlagMapping.mappingType is "Flag" and listfindnocase("radio,checkbox",thisColumnFlagMapping.data_type)){
		actualFlagMappingResult = myObject.predictFlagMappingResult(load_column = columnname);
	}
	if (thisColumnFlagMapping.mappingType is "FlagGroup") {
		actualFlagMappingResult = myObject.predictFlagMappingResult(load_column = columnname);
	}

 }	else if (frmFlagID is not "") {
		entityTypeID = application.com.flag.getFlagStructure(frmFlagID).entityTypeID;
		distinctQry = application.com.dbinterface.getDistinctColumnValues(dataSource = application.siteDataSource,tablename=tablename, columnName=columnName);

 }	else if (frmFlagGroupID is not "") {
		flagGroupStructure = application.com.flag.getFlagGroupStructure(frmFlagGroupID);
		entityTypeID = flagGroupStructure.entityTypeID;
		distinctQry = application.com.dbinterface.getDistinctColumnValues(dataSource = application.siteDataSource,tablename=tablename, columnName=columnName, listDelimiter = iif(flagGroupStructure.flagtype.name is "checkbox",de(","),de("")));
		SuggestedFlagGroupMappings = myObject.getPotentialFlagGroupMappings (loadTableColumn = columnname,flaggroupid = frmFlagGroupID);

 } else {

		distinctQry = application.com.dbinterface.getDistinctColumnValues(dataSource = application.siteDataSource,tablename=tablename, columnName=columnName, cacheForXMins = 1);

 }

if (entityTypeID is not "") {
	startedManualMapping = true;
}



 if (not hasExistingMapping and not startedManualMapping)
  {
 	// unmapped item, see what it might be
	getLikelyMapping = myObject.matchDataLoadColumnNameToPOLorFlagorFlagGroup (loadtablecolumn = columnname, flaggroupid = frmflagGroupID, noPOL = true, entityTypeID = entityTypeID );

	if (getLikelyMapping.recordCount is not 0) {
		hasSuggestedMapping = true ;
	}

	PotentialFlagGroupMappings =arrayNew(1);
	for (i=1;i<=getLikelyMapping.recordCount;i++) {
		// get distinct column values.  For checkbox FlagGrops we assume that values might be separated by commas
		if (getLikelyMapping.columntype[i] is 'FlagGroup' and application.com.flag.getFlagGroupStructure(getLikelyMapping.mapsTo[i]).flagtype.name is "checkbox") {
			listdelimiter = "," ;
		} else {
			listdelimiter = ""	;
		}
			distinctQry = application.com.dbinterface.getDistinctColumnValues(dataSource = application.siteDataSource,tablename=tablename, columnName=columnName, listDelimiter = listdelimiter);


		if (getLikelyMapping.columntype[i] is 'Flag') {
			 //frmFlagID = getLikelyMapping.mapsTo ;
			// entityTypeID = getLikelyMapping.entityTypeID ;
		} else if (getLikelyMapping.columntype[i] is 'FlagGroup') {
			 //frmFlagGroupID = getLikelyMapping.mapsTo ;
			// entityTypeID = getLikelyMapping.entityTypeID;
			PotentialFlagGroupMappings[i] = myObject.getPotentialFlagGroupMappings (loadTableColumn = columnname,flaggroupid = getLikelyMapping.mapsTo[i]);
		}

	}

 }



if (entityTypeID is not "") {
	myObject.getFlagList(entityTypeID);
	myObject.getFlagGroupCheckboxList();
}


if (isDefined("distinctQry")) {
	if(distinctQry.column_Value is "" and distinctQry.recordCOunt lte 1) {
		noDataInThisColumn = true;
	} else {
		noDataInThisColumn = false;
	}
}

</cfscript>

<script>
	<!--- messy function for my buttons to same page with different parameters, rather than submitting forms and things--->
	function goto(parameters) {
		location.href=location.href.split('?')[0] +  '?' + parameters

	}
</script>


<cfoutput>

<cfset application.com.request.setDocumentH1(text="Load Data")>

		<CF_relayFormElementDisplay relayFormElementType="html" Label="Column Name" currentValue = "<B>#htmleditformat(columnName)#</B>" >
		<cfif message is not "">
			<CF_relayFormElementDisplay relayFormElementType="html" Label="#columnName#" currentValue = "" >
		</cfif>

		<cfif isDefined("distinctQry")> <!--- may not be defined for a confirmed mapping to a non boolean flag --->
			<CF_relayFormElementDisplay relayFormElementType="html" Label='Data'>
				<CF_tableFromQueryObject
					queryObject="#distinctQry#"
					sortOrder=""
					hidepagecontrols = true
					numRowsPerPage="40"
						tablealign="left"
				        useInclude="false">
			</CF_relayFormElementDisplay>
		</cfif>

	 	<cfif hasExistingMapping>

			<!---
			This section displays information about an existing mapping
			 --->


			<cfif thisColumnFlagMapping.mappingType is "FlagGroup" or (thisColumnFlagMapping.mappingType is "Flag" and listfindnocase("radio,checkbox",thisColumnFlagMapping.data_type) )>

				<cfif thisColumnFlagMapping.mappingType is "FlagGroup">
					<CF_relayFormElementDisplay relayFormElementType="html" Label='Maps To Profile' currentValue = "<B>#htmleditformat(thisColumnFlagMapping.flagGroup)#</B>" >
				<cfelseif thisColumnFlagMapping.loadData is "*AnyTrue">
					<CF_relayFormElementDisplay relayFormElementType="html" Label='Maps To' currentValue = "Attribute <b>#htmleditformat(thisColumnFlagMapping.PROFILE_ATTRIBUTE)#</b><BR>(Any True Value)" >
				<cfelseif thisColumnFlagMapping.loadData is "">
					<CF_relayFormElementDisplay relayFormElementType="html" Label='Maps To' currentValue = "Attribute <b>#htmleditformat(thisColumnFlagMapping.PROFILE_ATTRIBUTE)#</b><BR>(Any non-Blank Value)" >
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="html" Label='Maps To' currentValue = "Attributes within Profile <b>#htmleditformat(thisColumnFlagMapping.flagGroup)#</b>" >
				</cfif>

				<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = "" >

					<p>Data values are mapped as follows</p>
					<CF_tableFromQueryObject
						queryObject=#actualFlagMappingResult.mappedData#
						columnHeadingList = "Column Value,Number of Rows,Attribute Name,ID"
						showthesecolumns = "datavalue,number_of_Rows,FlagName,flagID"
						sortOrder=""
						numRowsPerPage="100"
						hidepagecontrols = true
				        useInclude="false"
						tableAlign="left"
				        	>


				</CF_relayFormElementDisplay >

				<cfif actualFlagMappingResult.blankRows is not 0>
					<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = "" >
							#htmleditformat(actualFlagMappingResult.blankRows)# rows contain no data
					</CF_relayFormElementDisplay >
				</cfif>


				<!--- if it is flag mapping then and there is unmapped data then we can display the mapping form again --->
				<cfif thisColumnFlagMapping.mappingType is "Flag" and thisColumnFlagMapping.loaddata is not "*AnyTrue"  and thisColumnFlagMapping.loaddata is not "" and actualFlagMappingResult.unmappeddata.recordcount is not 0>
					<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = "" >
						<p>#actualFlagMappingResult.unmappeddata.recordcount# data values are not mapped</p>
						<p>Use the form below to update the mappings</p>
					</CF_relayFormElementDisplay >
					<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = "" >
						#outputFlagGroupMappingForm(mappingquery = actualFlagMappingResult.allData, flagGroupID = frmFlagGroupID, columnname = columnname, tablename = tablename)#
					</CF_relayFormElementDisplay >

				<cfelseif actualFlagMappingResult.unmappeddata.recordcount is not 0>
					<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = "" >
						<p>These data values are not mapped</p>
						<CF_tableFromQueryObject
							queryObject=#actualFlagMappingResult.unmappedData#
							columnHeadingList = "Column Value,Number of Rows"
							showthesecolumns = "datavalue,number_of_Rows"
							sortOrder=""
							numRowsPerPage="100"
							hidepagecontrols = true
							tableAlign="left"
					        useInclude="false">
					</CF_relayFormElementDisplay >

				</cfif>


			<cfelseif thisColumnFlagMapping.mappingType is "Flag"  >
				<CF_relayFormElementDisplay relayFormElementType="html" Label='Maps To' currentValue = "<B>#htmleditformat(thisColumnFlagMapping.profile_attribute)#</B> (#htmleditformat(thisColumnFlagMapping.data_Type)#) (#htmleditformat(thisColumnFlagMapping.flagID)#)" >

			<cfelse>
				<CF_relayFormElementDisplay relayFormElementType="html" Label='Maps To' currentValue = "" >
					<p>Unknown Mapping</p>
					<cfdump var="#thisColumnFlagMapping#">
				</CF_relayFormElementDisplay>

			</cfif>


			<cfif thisColumnFlagMapping.automapped>
				<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = '' >
					<p>This column has been mapped automatically</p>
					<CF_INPUT type="button" value = "Confirm Mapping" onclick="goto('tablename=#tablename#&confirmColumnAutoMapping=#columnName#')">
				</CF_relayFormElementDisplay >
			</cfif>

			<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = '' >
				<CF_INPUT type="button" value = "Remove Mapping" onclick = "goto('tablename=#tablename#&removeColumnMapping=#columnName#')">
			</CF_relayFormElementDisplay >

			<cfelse>
				<!--- <div class="internalBodyPadding"> --->
			<!---
			This section allows the user to set up a mapping - in a number of stages

			 --->
			<cfif hasSuggestedMapping and not startedManualMapping>
				<CF_relayFormElementDisplay relayFormElementType="html" Label='Might map to' currentValue = "" align = "LEFT" >
						<cfloop query = getLikelyMapping>
							<div class="useThisMapping">
								<p>#htmleditformat(name)# (#htmleditformat(mapsTo)#)</p>
							    <p><A href="?tableName=#tableName#&columnName=#columnName#&entityTypeID=#entityTypeID#&frm#columnType#ID=#mapsTo#" class="btn btn-primary">Use this Mapping</A></p>
							</div>
						</cfloop>
					</CF_relayFormElementDisplay>

				<!--- This is the manual selection process --->
				<!--- <CF_relayFormElementDisplay relayFormElementType="html" Label='Or Map Manually ' currentValue = "" > --->

			</cfif>


			<!--- Form to manually choose EntityType and Flag/Group--->
			<form>
				<Cf_relayFormDisplay>
				<cf_input type="hidden" name="tablename" id="tablename" value="#tablename#">
				<cf_input type="hidden" name="columnName" id="columnName" value="#columnName#">

				<!--- Choose Entity Type --->
	           	<CF_relayFormElementDisplay relayFormElementType="Select" FieldName = "entityTypeID" class="form-control" query = #entityQuery# display=entityType value=entityTypeID Label='Map Manually' currentValue = "#entityTypeID#" onchange="this.form.submit()"  nullText="Choose An Entity" disabled = "#iif(entityTypeID is '',false,true)#">

				<!--- only display flag/group selectors once the entity has been chosen --->
				<cfif startedManualMapping>
					<cfset disabled = false>
					<cfif 	frmFlagGroupID is not "" or frmFlagID is not "" >
						<cfset disabled = true>
					</cfif>
					<cfif frmFlagGroupID is "">
						<cfset label = "This column contains data which maps to a single attribute">
						<cfif not disabled>
								<cfset label = "EITHER: <BR>" & label>
						</cfif>
						<CF_relayFormElementDisplay relayFormElementType="Select" FieldName = "frmFlagID" class="form-control" query = #myObject.qFlagList# display=thisFlag value=flagID Label='#label#' currentValue = "#frmFlagID#" onchange="this.form.submit()"  nullText="Choose A Flag" disabled = "#disabled#">
					</cfif>

					<cfif frmFlagID is "">
						<cfset label = "This column contains lists of Radio Button or Checkbox data">
						<cfif not disabled>
								<cfset label = "OR: <BR> " & label>
						</cfif>
        				<CF_relayFormElementDisplay relayFormElementType="Select" FieldName = "frmFlagGroupID" class="form-control" query = #myObject.qFlagGroupList# display=thisFlag value=flagGroupID Label='#label#'  currentValue = "#frmFlagGroupID#" onchange="this.form.submit()"  nullText="Choose A Flag Group" disabled = "#disabled#">
					</cfif>


					<CF_relayFormElementDisplay relayFormElementType="html" label="" currentvalue=''  >
						<CF_INPUT Type="button" value="Clear" onclick="goto('tablename=#tablename#&columnName=#columnName#')">
					</CF_relayFormElementDisplay >

				</cfif>
				</Cf_relayFormDisplay>
			</form>


			<!--- We have now chosen a flag or Group --->
			<cfif frmFlagGroupID is not "" or frmFlagID is not "">

				<!--- Have chosen a flagGroup to map to
					(will only ever be Boolean)

				--->
				<cfif isNumeric(frmFlagGroupID)>
					<!---
					FLAG GROUP MAPPING
					--->

						<!---
						Table showing data values
						 --->
							<CF_relayFormElementDisplay relayFormElementType="html" label = "Data and Mappings" >
								<CF_tableFromQueryObject
									queryObject="#SuggestedFlagGroupMappings.mappings#"
									columnHeadingList = "Data Value,Maps to Attribute,Number of Rows"
									showthesecolumns = "DataValue,Flagname,Number_of_Rows"
									sortOrder=""
									numRowsPerPage="100"
									hidepagecontrols = true
									tableAlign="left"
							        useInclude="false">
							</CF_relayFormElementDisplay >

					<cfif SuggestedFlagGroupMappings.unmappedData.recordCount is 0 >
						<CF_relayFormElementDisplay relayFormElementType="html" Label = "" >
							<cfif not noDataInThisColumn>All data items map successfully<cfelse>There is no data in this column </cfif> <BR>
						<a href="?tablename=#tablename#&columnName=#columnName#&addFlagGroupMapping=#frmFlagGroupID#">Accept Mapping</a>'
						</CF_relayFormElementDisplay >

					<!--- ie no columns mapped --->
					<cfelse>
						<CF_relayFormElementDisplay relayFormElementType="html" Label = "" >
							<cfif SuggestedFlagGroupMappings.unmappedData.recordCount is not  SuggestedFlagGroupMappings.mappings.recordCount>
							<!--- some columns map --->
								<p>Not all data items map automatically.</p>
								<p>EITHER: You may accept the automatic mapping of all columns <a href="?tablename=#tablename#&columnName=#columnName#&addFlagGroupMapping=#frmFlagGroupID#">Accept Automatic Mapping</a></p>
								<p>OR:</p>
							<cfelse>
								<p>None of the data items map automatically </p>
							</cfif>
								<p>Use the form below to create individual mappings</p>
						</CF_relayFormElementDisplay>
					</cfif>


					<cfif SuggestedFlagGroupMappings.unmappedData.recordCount is not 0>

						<CF_relayFormElementDisplay relayFormElementType="html" Label='' currentValue = "" >

						#outputFlagGroupMappingForm(mappingquery = SuggestedFlagGroupMappings.mappings, flagGroupID = frmFlagGroupID, columnname = columnname, tablename = tablename)#

						</CF_relayFormElementDisplay >

					</cfif>

			     <cfelseif frmFlagID is not "">
					<!---
					FLAG MAPPING
					--->
					<cfset flagStructure = application.com.flag.getFlagStructure(frmFlagID)>
					<cfif flagStructure.flagtype.name is "radio" or flagStructure.flagtype.name is "checkbox">
						<!---
						BOOLEAN FLAGS
						decide whether to do Yes/No Mapping, or any non blank value
						--->
							<CF_relayFormElementDisplay relayFormElementType="html" Label = "" currentvalue=''  >
								<p>Set this attribute if data value is not blank</p>
								<CF_INPUT type="button" value="Map Non Blank" onclick="goto('tablename=#tablename#&columnName=#columnName#&addFlagMapping=#frmFlagID#')">
							</CF_relayFormElementDisplay >

						<cfif myObject.doesColumnHaveYesNoData(columnName)>
							<CF_relayFormElementDisplay relayFormElementType="html" Label = "" currentvalue=''  >
								<p>This looks like a field with containing True/False Data </p>
								<p>Set this attribute if data value is True, 1, or Yes </p> <CF_INPUT type="button" value="Map Any True" onclick="goto('tablename=#tablename#&columnName=#columnName#&addFlagMapping=#frmFlagID#&frmAnyTrue=1')">
							</CF_relayFormElementDisplay >
						</cfif>

					<cfelse>
						<!---
						OTHER FLAGS
						just confirm
						 --->

						<CF_relayFormElementDisplay relayFormElementType="html" Label = "" currentvalue='<a href="?tablename=#tablename#&columnName=#columnName#&addFlagMapping=#frmFlagID#">Confirm Mapping</a>'  >

					</cfif>

		          </cfif>

		      </cfif>

		</cfif>

 		<CF_relayFormElementDisplay relayFormElementType="html" Label = "" >
			<A HREF="profileMatching.cfm?tableName=#tableName#" class="btn btn-primary">Back to Profile Matching</A>
		</CF_relayFormElementDisplay >
		<!--- </div> --->
</cfoutput>