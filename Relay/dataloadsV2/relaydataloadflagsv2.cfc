<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2007/11/07 lots of work on the flag inserts - mainly dealing with problem data
WAB 2008/03/25  problem when a flag stored in a text or ntext field, added some conversions
WAB 2008/09/17 change to auto-matching of flags to import a download (replace spaces ,( ) ? in names when trying to match columns
WAB	2008/09/17 Altered flag load code to deal with columns containing comma delimited lists of flags (checkboxes)
			Subsequently lots of changes to allow for mapping of whole profiles in a single go (rather than flag by flag)
WAB	2009/03/02 modified the flagLoading code so that SQL snippet in separate function which can be used to see what is going to happen
				and added a function predictFlagMappingResult which does that
WAB 2009/04/15 LID 2097  Code to check for bad radio data in a dataload (ie conflicting settings) didn't work when a whole flagGroup was mapped, small adjustment to make it work
WAB 2014-01-08	Make insert code aware of decimalFlagData - in particular to check for numeric type and conflicting data
NJH 2016/01/15 issue found on panduit... alter the join to the flaggroup in function listDataloadFlagMapping
 --->

<cfcomponent displayname="relayDataLoadFlags" hint="The methods used for loading flag/profile data" extends="relaydataloadV2">
<cfset THIS.datasource = application.sitedataSource>
<cfset THIS.TableName = "">
<!--- <cfset THIS.loadTablecOLUMN = ""> --->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="addDataLoadSingleFlagMapping" hint="Inserts a mapping between a dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfargument name="flagid" required="yes" type="string">
		<cfargument name="automapped" default="0">
		<cfargument name="anyTrue" default="false">  <!--- only true values get mapped --->

		<cfif anyTrue>
			<cfset addDataLoadFlagMapping(loadTableColumn = loadTableColumn, flagid = flagid, loadData = '*ANYTRUE', automapped = automapped)>
		<cfelse>
			<cfset addDataLoadFlagMapping(loadTableColumn = loadTableColumn, flagid = flagid, loadData = '', automapped = automapped)>
		</cfif>


	</cffunction>



	<cffunction name="deleteDataLoadMapping" hint="Deletes a mapping between a dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfquery name="deletevDataLoadMapping" datasource="#THIS.dataSource#">
			delete DataLoadMapping from
			vdataLoads dl inner join DataLoadMapping dlm on dl.datasourceid = dlm.dataloadid
				where load_Table =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and loadTableColumn =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfset updateDataLoadView()>
		<cfset THIS.message = "#THIS.loadTableColumn# unmapped.">
	</cffunction>


	<cffunction name="deleteDataLoadMappingItem" hint="Deletes a mapping between an indivual  dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfargument name="loadData" required="yes" type="string">

		<cfquery name="deleteDataLoadMapping" datasource="#THIS.dataSource#">
			delete DataLoadMappingDetail from
			vdataLoadmapping dl inner join DataLoadMappingDetail dlm on dl.DataLoadMappingid = dlm.DataLoadMappingid
				where loadTable =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and loadTableColumn =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and loadData =  <cf_queryparam value="#loadData#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfset updateDataLoadView()>
		<cfset THIS.message = "#THIS.loadTableColumn# umapped.">
	</cffunction>




<!--- 	<cffunction name="addDataLoadMultiFlagMapping" hint="Inserts a mapping between a dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfargument name="loadData" required="yes" type="string">
		<cfargument name="flagid" required="yes" type="string">


		<cfquery name="addDataLoadMultiFlagMapping" datasource="#THIS.dataSource#">
			if not exists (select * from vDataLoadMapping
				where loadTable = '#THIS.tableName#'
				and loadTableColumn = '#loadTableColumn#')
				BEGIN
					INSERT INTO [vDataLoadMapping](dataloadid, [loadTableColumn], mappingType, MappedTo)
					select datasourceid, '#loadTableColumn#', 'MultiFlag', ''
					from vdataloads where load_Table = '#THIS.tableName#'
				END
			ELSE
				BEGIN
					update [vDataLoadMapping]
					set mappedTo = '', mappingType = 'MultiFlag'
					where
						loadTable = '#THIS.tableName#'
						and  [loadTableColumn] = '#loadTableColumn#'

				END

			if not exists (select * from vDataLoadMappingDetail
				where loadTable = '#THIS.tableName#'
				and loadTableColumn = '#loadTableColumn#'
				and loadData = '#loadData#')
				BEGIN
					INSERT INTO [dataloadMappingDetail](dataloadmappingid, loadData,mappedTo)
					select
						dataloadmappingid, '#loadData#',#flagID#
						from vDataLoadMapping
						where
						loadTable = '#THIS.tableName#'
						and  [loadTableColumn] = '#loadTableColumn#'
				END
			ELSE
				BEGIN
					update vdataloadMappingDetail
					set mappedTo= #flagID#
					from vDataLoadMappingDetail
					where
						loadTable = '#THIS.tableName#'
						and  loadTableColumn = '#loadTableColumn#'
						and  loadData = '#loadData#'
						and mappedTo  <>#flagID#
				END

		</cfquery>

		<cfset THIS.message = "#loadTableColumn# #loadData# mapped.">
	</cffunction>

	<cffunction name="deleteDataLoadMultiFlagMapping" hint="Inserts a mapping between a dataload column and a flag">
		<cfquery name="deleteDataLoadMultiFlagMapping" datasource="#THIS.dataSource#">
			delete from DataloadTextDataToFlagMapping
				where loadTable = '#THIS.tableName#'
				and loadTableColumn = '#THIS.loadTableColumn#'
				and loadColumnTextData = '#this.loadColumnTextData#'
		</cfquery>

		<cfset THIS.message = "#THIS.loadTableColumn# #this.loadColumnTextData# mapping deleted.">
	</cffunction>


 --->

	<cffunction name="addDataLoadFlagMapping" hint="Inserts a mapping between a dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfargument name="loadData" required="yes" type="string">
		<cfargument name="flagid" required="yes" type="string">
		<cfargument name="automapped" default="0">
		<cfargument name="singleFlag" default="true">


			<cfset addDataLoadMapping (
				loadTableColumn = loadTableColumn,
				loadData = loadData,
				mappedTo = flagid,
				mappingType = "flag",
				automapped = automapped,
				singleFlag = singleflag
				)>


	</cffunction>


	<cffunction name="addDataLoadFlagGroupMapping" hint="Inserts a mapping between a dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfargument name="flagGroupid" required="yes" type="string">
		<cfargument name="automapped" default="0">


			<cfset addDataLoadMapping (
				loadTableColumn = loadTableColumn,
				loadData = "",
				mappedTo = flaggroupid,
				mappingType = "flaggroup",
				automapped = automapped,
				singleflag = false
				)>


	</cffunction>

	<cffunction name="confirmAutoMapping" hint="Confirms mapping is correct">
			<cfargument name="loadTableColumn" required="yes" type="string">

		<cfquery name="qaddDataLoadFlagMapping" datasource="#THIS.dataSource#">
		update [vDataLoadMapping]
					set
						automapped = 0
					where
						loadTable =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and  [loadTableColumn] =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

	</cffunction>

	<cffunction name="addDataLoadMapping" hint="Inserts a mapping between a dataload column and a flag">
		<cfargument name="loadTablecOLUMN" required="yes" type="string">
		<cfargument name="loadData" required="yes" type="string">
		<cfargument name="mappingType" required="yes" type="string">
		<cfargument name="mappedTo" required="yes" type="string">
		<cfargument name="automapped" default="0">
		<cfargument name="singleFlag" default="true">


		<cfquery name="qaddDataLoadFlagMapping" datasource="#THIS.dataSource#">
			if not exists (select * from vDataLoadMapping
				where loadTable =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and loadTableColumn =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				BEGIN
					INSERT INTO [vDataLoadMapping](dataloadid, [loadTableColumn], mappingType, automapped)
					select datasourceid, <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#mappingType#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#automapped#" CFSQLTYPE="CF_SQL_bit" >
					from vdataloads where load_Table =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				END
			ELSE
				BEGIN
					update [vDataLoadMapping]
					set
						mappingType =  <cf_queryparam value="#mappingType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,  automapped =  <cf_queryparam value="#automapped#" CFSQLTYPE="cf_sql_integer" >
					where
						loadTable =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and  [loadTableColumn] =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >

				END

			if not exists (select * from vDataLoadMappingDetail
				where loadTable =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and loadTableColumn =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and DataLoadMappingDetailID is not null
				<cfif not singleFlag>and loadData =  <cf_queryparam value="#loadData#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
				)
				BEGIN
					INSERT INTO [dataloadMappingDetail](dataloadmappingid, loadData,mappedTo)
					select
						dataloadmappingid, <cf_queryparam value="#loadData#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#mappedTo#" CFSQLTYPE="CF_SQL_VARCHAR" >
						from vDataLoadMapping
						where
						[loadTable] =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and  [loadTableColumn] =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
				END
			ELSE
				BEGIN
					update vdataloadMappingDetail
					set mappedTo =  <cf_queryparam value="#mappedTo#" CFSQLTYPE="CF_SQL_VARCHAR" > , loadData =  <cf_queryparam value="#loadData#" CFSQLTYPE="CF_SQL_VARCHAR" >
					from vDataLoadMappingDetail
					where
						[loadTable] =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and  [loadTableColumn] =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
						<cfif not singleFlag>
						and  loadData =  <cf_queryparam value="#loadData#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
				END

		</cfquery>

		<cfset updateDataLoadView()>

		<cfset THIS.message = "#loadTableColumn# #loadData# mapped.">
	</cffunction>

	<cffunction name="copyDataLoadMappingsToThisDataload" access="public" hint="Copies mappings from one dataload to this one">
		<cfargument name="fromDataLoadID" required="yes" type="numeric">

		<cfset copyDataLoadMappings(fromDataLoadID = fromDataLoadID, toDataLoadID = this.dataLoadID)>

	</cffunction>

	<cffunction name="copyDataLoadMappings" access="public" hint="Copies mappings from one dataload to another">

		<cfargument name="fromDataLoadID" required="yes" type="numeric">
		<cfargument name="toDataLoadID" required="yes" type="numeric">

		<cfquery name="getMappingIds" datasource="#THIS.dataSource#">
			select dlm1.dataloadMappingID
			from
			dataloadMapping dlm1
			left join
			dataloadMapping dlm2 on dlm1.loadTableColumn = dlm2.loadTableColumn and dlm2.dataloadid = #toDataLoadID#
			where dlm1.dataloadid = #fromDataLoadID#
			and dlm2.dataloadMappingID is null
		</cfquery>

		<cfif getMappingIds.recordcount is not 0>
			<cfquery name="insertMappingIds" datasource="#THIS.dataSource#">
				insert
				into dataloadMapping
				(dataloadid,LoadTableColumn,MappingType)
				select <cf_queryparam value="#toDataLoadID#" CFSQLTYPE="CF_SQL_INTEGER" >, LoadTableColumn,MappingType
				from dataloadmapping where dataLoadMappingID  in ( <cf_queryparam value="#valuelist(getMappingIDs.dataloadMappingId)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

				insert
				into dataloadMappingdetail
				(dataloadmappingid,loadData,MappedTo)

				select dlm2.dataloadmappingid, detail1.loadData,detail1.MappedTo
				from
					dataloadMapping dlm1
						inner join
					dataloadMapping dlm2 on dlm1.loadTableColumn = dlm2.loadTableColumn and dlm2.dataloadid = #toDataLoadID#
						inner join
					dataloadMappingDetail detail1 on dlm1.dataloadmappingid = detail1.dataloadmappingid
						left join
					dataloadMappingDetail detail2 on dlm2.dataloadmappingid = detail2.dataloadmappingid and detail2.loaddata = detail1.loaddata

				where dlm1.dataLoadMappingID  in ( <cf_queryparam value="#valuelist(getMappingIDs.dataloadMappingId)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and detail2.dataloadmappingdetailid is null
			</cfquery>
		</cfif>

		<cfset updateDataLoadView()>   <!--- wab 2008/02/04 had forgotten to do this --->

	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getTableColumns" access="public" hint="Lists columns for a given table">

		<cfargument name="tableName" required="yes" type="string">

		<cfscript>
			var qGetDBMaster = "";
		</cfscript>
		<cfif listLen(arguments.tableName,".") eq 3>
			<cfquery name="qGetDBMaster" datasource="#THIS.dataSource#">
				SELECT
						#ListFirst(arguments.tableName,".")#.dbo.syscolumns.Name AS ColumnName

				FROM 		#ListFirst(arguments.tableName,".")#.dbo.sysobjects
				INNER JOIN 	#ListFirst(arguments.tableName,".")#.dbo.syscolumns
				ON 		(#ListFirst(arguments.tableName,".")#.dbo.sysobjects.id = #ListFirst(arguments.tableName,".")#.dbo.syscolumns.id)
				INNER JOIN 	#ListFirst(arguments.tableName,".")#.dbo.systypes
				ON		(#ListFirst(arguments.tableName,".")#.dbo.syscolumns.xtype = #ListFirst(arguments.tableName,".")#.dbo.systypes.xtype)
				LEFT OUTER JOIN #ListFirst(arguments.tableName,".")#.dbo.syscomments
				ON (#ListFirst(arguments.tableName,".")#.dbo.syscolumns.cdefault = #ListFirst(arguments.tableName,".")#.dbo.syscomments.id)
				WHERE 	#ListFirst(arguments.tableName,".")#.dbo.sysobjects.xtype = 'U'
				AND 		#ListFirst(arguments.tableName,".")#.dbo.sysobjects.name <> 'dtproperties'
				AND #ListFirst(arguments.tableName,".")#.dbo.systypes.name <> 'sysname'

				AND #ListFirst(arguments.tableName,".")#.dbo.sysobjects.name =  <cf_queryparam value="#ListLast(arguments.tableName,".")#" CFSQLTYPE="CF_SQL_VARCHAR" >

				ORDER BY ColumnName
			</cfquery>
		<cfelse>
			<cfquery name="qGetDBMaster" datasource="#THIS.dataSource#">
				SELECT
						dbo.syscolumns.Name AS ColumnName

				FROM 		dbo.sysobjects
				INNER JOIN 	dbo.syscolumns
				ON 		(dbo.sysobjects.id = dbo.syscolumns.id)
				INNER JOIN 	dbo.systypes
				ON		(dbo.syscolumns.xtype = dbo.systypes.xtype)
				LEFT OUTER JOIN dbo.syscomments
				ON (dbo.syscolumns.cdefault = dbo.syscomments.id)
				WHERE 	dbo.sysobjects.xtype = 'U'
				AND 		dbo.sysobjects.name <> 'dtproperties'
				AND dbo.systypes.name <> 'sysname'

				AND dbo.sysobjects.name =  <cf_queryparam value="#arguments.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >

				ORDER BY ColumnName
			</cfquery>
		</cfif>
		<cfreturn qGetDBMaster>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFlagList" hint="Returns a list of flags">
		<cfargument name="entityTypeIDList" default = "0,1,2">

		<cfquery name="getFlagList" datasource="#THIS.dataSource#" cachedwithin="#CreateTimeSpan(0, 0, 0, 0)#">
			SELECT  TableName + ' profile attributes' as profileEntity,flag.flagGroupID,
				left('(' + convert(varchar(10),flagGroup.flagGroupID) + ') ' + flagGroup.name + ' - (' + convert(varchar(10),Flag.flagID) + ') ' + Flag.Name ,200) as thisFlag,   <!--- WAB 2007-10-22 added left - drop downs were going off page --->
				FlagID
			From Flag
			INNER JOIN FlagGroup ON flag.flaggroupid = flaggroup.flaggroupid
			INNER JOIN flagEntityType ON flagGroup.entityTypeID = flagEntityType.entityTypeID
			WHERE
				flaggroup.entitytypeid  in ( <cf_queryparam value="#entityTypeIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				and flagGroup.active = 1 and flag.active=1
			order by tablename, flagGroup.name +'.'+ Flag.Name
		</cfquery>
		<cfset THIS.qFlagList = getFlagList>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFlagGroupCheckboxList" hint="Returns a list of flagGroups">
		<cfargument name="entityTypeIDList" default = "0,1,2">

		<cfquery name="getFlagGroupCheckboxList" datasource="#THIS.dataSource#">
			SELECT  FlagGroup.EntityTypeID, TableName + ' profile attributes' as profileEntity,flagGroupID,
				flagGroup.name +' - ' as thisFlag
			From FlagGroup
			INNER JOIN flagEntityType ON flagGroup.entityTypeID = flagEntityType.entityTypeID
			WHERE
				flaggroup.entitytypeid  in ( <cf_queryparam value="#entityTypeIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				and flagGroup.active = 1
				and flagGroup.flagTypeID IN (2,3)    <!--- WAB - added radio	 --->
				order by tablename, flagGroup.name
		</cfquery>
		<cfset THIS.qFlagGroupList = getFlagGroupCheckboxList>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFlagCheckboxList" hint="Returns a list of flags">
		<cfquery name="getFlagCheckboxList" datasource="#THIS.dataSource#">
			SELECT  TableName + ' profile attributes' as profileEntity,flag.flagGroupID,
				left('(' + convert(varchar(10),flagGroup.flagGroupID) + ') ' + flagGroup.name + ' - (' + convert(varchar(10),Flag.flagID) + ') ' + Flag.Name ,200) as thisFlag,   <!--- WAB 2007-10-22 added left - drop downs were going off page --->
				FlagID
			From Flag
			INNER JOIN FlagGroup ON flag.flaggroupid = flaggroup.flaggroupid
			INNER JOIN flagEntityType ON flagGroup.entityTypeID = flagEntityType.entityTypeID
			WHERE ((flaggroup.entitytypeid <=2 and flaggroup.entitytypeid >= 0)
			or (flaggroup.entitytypeid = 0))
			and flagGroup.active = 1 and flag.active=1
			and flagGroup.flagTypeID = 2
			order by tablename, flagGroup.name +'.'+ Flag.Name
		</cfquery>
		<cfset THIS.qFlagList = getFlagCheckboxList>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->


	<cffunction name="listDataloadFlagMapping" access="public" hint="Returns Data Source History records for tablename">
		<cfargument name="loadTableColumn" default = "">

		<!--- NJH 2016/01/15 issue found on panduit... alter the join to the flaggroup.. put bracets around the or statement and cast flagGroupId as varchar..
			case when there was a null row... not sure which one... --->
		<cfquery name="listDataloadFlagMapping" datasource="#THIS.dataSource#">
			SELECT dlm.[DataloadMappingID], [DataloadMappingDetailID], [loadTableColumn] as load_column, f.[flagID] as flagid, mappingType,
			case when mappingType =  'flagGroup' then 'ALL' else f.[name] end as profile_attribute, ft.[name] as data_type , fg.entityTypeID, fg.name as flagGroup, LoadData, fg.flagGroupId, ft.datatablefullname,loadeddate,
			dateAdd(n,-(#request.relaycurrentuser.localeInfo.clientServerTimeOffsetMinutes#), loadeddate) as loadedDate_LocalTime,
			automapped
			FROM vdataloadmappingDetail dlm
			left JOIN flag f on f.flagID = dlm.mappedto and dlm.mappingtype = 'flag'
			left  JOIN flagGroup fg ON fg.flagGroupID = f.flagGroupID or (convert(varchar,fg.flaggroupID) = dlm.mappedto and dlm.mappingtype = 'flaggroup')
			left JOIN flagType ft ON ft.flagTypeID = fg.flagTypeID
			WHERE loadTable =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			-- and mappingType = 'MultiFlag'
			and mappingType in  ('MultiFlag','SingleFlag','Flag','FlagGroup')
			and dataLoadMappingDetailid is not null
			<cfif loadTableColumn is not "">and loadtableColumn =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif><!--- WAB added filter on loadTableColumn --->
			order by loadTableColumn
		</cfquery>
		<cfset THIS.qDataloadFlagMapping = listDataloadFlagMapping>

		<cfreturn THIS.qDataloadFlagMapping>
	</cffunction>






<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Tries to automatically map columns to flags etc.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="autoMap" output="true" hint="Inserts distinct values from the outside datasource against flagID ">

		<cfset message = "">
		<!--- get list of mismatched columns --->
		<cfset qMismatchedCols = mismatchedCols()>

		<!--- loop through trying to match to alternative column names, flaggroup and flags --->
		<cfloop query = "qMismatchedCols">
			<cfset variables.column_Name = column_Name>

			<cfset columnMatching = matchDataLoadColumnNameToPOLorFlagorFlagGroup(loadTableColumn = column_name)>

 			<cfif columnMatching.columnType is 'POL'>
				<cfset message = message & "Found POL match for #column_name#<BR>">
				<cfset mapColumn(loadTableColumn = column_name,mappedColumn = columnMatching.MapsTo,automapped =1 )>
			<cfelseif columnMatching.columnType is 'Flag'>

					<cfset message = message & "Found Flag match for #column_name# #columnMatching.mapsTo#<BR>">

						<cfif doesColumnOnlyHaveYesNoData(column_name)>
							<cfset loadDataValue = "*ANYTRUE">
						<cfelse>
							<cfset loadDataValue = "">
						</cfif>

						<cfset x = addDataLoadFlagMapping(loadTableColumn = column_name, loadData = loadDataValue, flagid = columnMatching.mapsTo ,automapped = 1)>

			<cfelseif columnMatching.columnType is 'FlagGroup'>

						<cfset flagGroupMappings = getPotentialFlagGroupMappings (loadTableColumn = column_name, flagGroupid = columnMatching.mapsTo)>

						<cfif flagGroupMappings.unmappedData.recordcount is 0>
							<!--- this means that all the items in the dataload column can be mapped to flags by either name, flagtextid or id
							therefore just need to add a flagGroup mapping --->
								<cfset x = addDataLoadFlagGroupMapping(loadTableColumn = #column_name#, flaggroupid =  #columnMatching.mapsTo# ,automapped = 1)>
								<cfset message = message & "Found Flag Group match for #column_name# #columnMatching.mapsTo#<BR>">
						<cfelse>
<!--- 							<!--- In this case we have to map the flags individually and the remaining ones have to be mapped manually --->
							<cfloop query="checkflags">
								<cfset x = addDataLoadFlagMapping(loadTableColumn = #column_name#, loadData = DataValue, flagid = checkFlags.flagid ,automapped = 1,singleFlag = false)>
							</cfloop>
 --->
								<cfset message = message & "Found Flag Group match for #column_name# #columnMatching.mapsTo#, but data value #flagGroupMappings.unmappedData.datavalue# could not be matched<BR>">

						</cfif>

						<!--- If all the values in the dataload can be matched directly to flag names/ids or textids then we can just to a single flagGroup mapping
						otherwise we would need to map flags individually
						 --->




			</cfif>





		</cfloop>


		<cfreturn Message>


	</cffunction>


	<!--- This function takes a column name (from the dataload table) and tries to see if the name matches the name of a POL Column, a Flag or a FlagGroup --->
	<cffunction access="public" name="matchDataLoadColumnNameToPOLorFlagorFlagGroup" output="true" hint="">
			<cfargument name="loadTableColumn" required=true>
			<cfargument name="entityTypeID" default = "">
			<cfargument name="noPOL" default = false>

			<cfquery name = "matchColumnName" datasource="#THIS.dataSource#">
				<cfif not noPOL>
				<!--- These are columns defined in dataloadcol  --->
				select 'POL' as columnType, dataloadcolumnname as MapsTo, dataloadcolumnname as Name, null as entityTypeID, 1 as orderIndex from dataloadcol
				where dbo.listfind(alternativecolumnname,'#loadTableColumn#') <> 0

				union

<!---
WAB 2009/01/12 this was an attempt at automatically mapping additional POL columns not in the dataLoadCol table,
but having written it I realised that the import code would only deal with a limited number of fields anyway
and also some of the other code relied on the fields being in the dataLoadCol table
So I have commented it out, but could be resurrected
			<!---
					These are columns which can be guessed at
					This was added as part of the Trend Support Instance to deal with all the unusual columns which were exported from the db and needed to be reimported but were not necessarily welcome in the dataloadcol table
					Uses the syscolumns, excludes any already dealt with in dataloadcol
					Any cols with the same name in any of the POL tables have to be referred to as #tablename#_#columnname#
				 --->
				select distinct 'POL' as columnType, cols.column_name as MapsTo, null as entityTypeID, 2 as orderIndex
				from
						information_schema.columns cols
							left join
						information_schema.columns duplicateCols on  duplicateCols.table_name in ('person','location','organisation') and cols.column_name = duplicateCols.column_name and duplicateCols.table_name <> cols.table_name and cols.column_name not in ('personid','locationid','organisationid')
							left join
						dataloadcol dataloadcol on relayColumn = cols.table_name + '.' +cols.column_name

				where
					cols.table_name in ('person','location','organisation')
					and
					<!--- deal with columns with same name in different tables --->
					case when duplicateCols.column_name is null then cols.column_name else cols.table_name + '_' + cols.column_name end = '#loadTableColumn#'
					<!--- exluded items in dataloadcol --->
					and
					dataloadcol.dataLoadColId is null


				union
 --->
				</cfif>

				select 'Flag' as columnType, convert(varchar,flagid) as MapsTo , f.name as name, fg.entityTypeID as entityTypeID, case when flagTextID =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 11 when  f.name =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 12 else 13 end as orderIndex
				from flag f inner join flaggroup fg on f.flagGroupid = fg.flagGroupid
				where
					<cfif entityTypeID is not "">
					fg.entityTypeID = #entityTypeID# AND
					</cfif>
					(
						flagTextID =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
						or f.name =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
						<!--- WAB 2008/09/17 deal with names with odd characters in them which have to be removed when used a column names
						cropped up when tried to reimport a download
								Rather nasty code to deal with 'ID'  which is added to some columns by the download code (however this will mean that we might get more than one row being returned)
						 --->
						or replace(replace(replace(replace(replace(replace(replace(f.name,'-',''),'''',''),'?',''),')',''),'(',''),' ',''),',','')  IN  ('#loadTableColumn#' <CFIF RIGHT(loadTableColumn,2) is "ID">,'#left(loadtablecolumn,len(loadtablecolumn) -2)#'</CFIF> )
					)
				union

				select 'FlagGroup' , convert(varchar,fg.flagGroupID) , fg.name as name, fg.entityTypeID as entityTypeID,
				case when fg.flagGroupTextID =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 21 when  fg.name =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >  then 22 else 23 end as orderIndex
					from flagGroup fg inner join flag f on  f.flaggroupid = fg.flaggroupid
					where
						<cfif entityTypeID is not "">
						fg.entityTypeID = #entityTypeID# AND
						</cfif>
						fg.flagTypeID in (2,3)  <!--- checkbox/radio --->
						and (
							fg.flagGroupTextID =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
							or fg.name =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
							<!--- WAB 2008/09/17 deal with names with odd characters in them which have to be removed when used a column names
								cropped up when tried to reimport a download
								Rather nasty code to deal with 'ID'  which is added to some columns by the download code (however this will mean that we might get more than one row being returned)
								 --->
							or replace(replace(replace(replace(replace(replace(replace(fg.name,'-',''),'''',''),'?',''),')',''),'(',''),' ',''),',','') IN  ('#loadTableColumn#' <CFIF RIGHT(loadTableColumn,2) is "ID">,'#left(loadtablecolumn,len(loadtablecolumn) -2)#'</CFIF> )
						)

				order by  orderIndex
			</cfquery>

		<cfreturn matchColumnName>

	</cffunction>


	<cffunction access="public" name="getPotentialFlagGroupMappings" output="true" hint="runs query showing what results will come from a particular mapping">
			<cfargument name="loadTableColumn" default = "">
			<cfargument name="flagGroupID" default = "">

		<cfset result = structnew()>

<!--- 				<cfquery name = "qGetPotentialFlagMappingsFlagGroup" datasource="#THIS.dataSource#">
					select
						dl.[#loadTableColumn#],
						f.name,
						count(1)
					 from
						#this.tablename# dl
					left join
						flag f on f.flagGroupid = #flagGroupID# and
							(	charindex (','+f.name + ',', ',' + convert(varchar(250),dl.[#loadTableColumn#]) + ',' ) <> 0
							 or charindex (','+f.flagtextid + ',', ',' + convert(varchar(250),dl.[#loadTableColumn#]) + ',' ) <> 0
							 or charindex (','+convert(varchar,f.flagid) + ',', ',' + convert(varchar(250),dl.[#loadTableColumn#]) + ',' ) <> 0
							)
					group by
						dl.[#loadTableColumn#],
						f.name
					order by dl.[#loadTableColumn#]
				</cfquery>

				<cfset result["qGetPotentialFlagMappingsFlagGroup"] = qGetPotentialFlagMappingsFlagGroup>

 --->
				<!--- get distinct column values for this column - including dealing with comma separated lists --->
				<cfset distinctQry = application.com.dbinterface.getDistinctColumnValues(dataSource =this.DataSource,tablename=this.tablename, columnName=loadTableColumn, listDelimiter = ",")>

				<!--- check which of the distinct values can be mapped to flags (and which cannot)
					has to create a temporary table of all the column values
				 --->
						<cfquery name = "checkFlags" datasource="#THIS.dataSource#">
						declare @dataValueTable  table (dataValue varchar(100), number_Of_Rows integer)
						<cfloop query="distinctQry">
						insert into @dataValueTable (dataValue, number_Of_Rows) values ('#column_value#', #number_Of_Rows#)</cfloop>

						select datavalue, flagid, name as flagname, number_Of_Rows from
						@dataValueTable  dl
						left join
						flag f on datavalue <> '' and
								(		f.name = datavalue
									or f.flagTextID = datavalue
									or convert(varchar, f.flagid) = datavalue
								)
								and f.flaggroupid = #flaggroupid#
						</cfquery>

						<cfquery name = "unmappedFlags" dbtype="query">
						select * from checkFlags where flagid is null and datavalue <> ''
						</cfquery>

 						<cfif unmappedFlags.recordcount gt 0>
							<!--- unmapped flags are probably comma delimited lists so lets deal with them
								could have done a dbo.listfind on the above query but that would not guarantee that all the values in the list were found
							--->
							<cfquery name = "checkFlags2" datasource="#THIS.dataSource#">
							declare @dataValueTable  table (dataValue varchar(100), number_Of_Rows integer)
							<cfloop query="unmappedFlags">
								<cfloop index="thisFlag" list = "#dataValue#">
								insert into @dataValueTable (dataValue, number_Of_Rows) values ('#thisFlag#', #number_Of_Rows#)</cfloop>
								</cfloop>

							select datavalue, flagid, name as flagname, number_Of_Rows from
							@dataValueTable  dl
							left join
							flag f on datavalue <> '' and (f.name = datavalue or f.flagTextID = datavalue or convert(varchar, f.flagid) = datavalue) and f.flaggroupid = #flaggroupid#
							</cfquery>

							<cfquery name = "unmappedFlags" dbtype="query">
							select * from checkFlags2 where flagid is null and datavalue <> ''
							</cfquery>


						</cfif>

				<cfset result.mappings = checkFlags>
				<cfset result.unmappedData = unmappedFlags>


		<cfreturn result>
	</cffunction>


	<cffunction access="public" name="columnYesNoDataQuery" output="true" hint="works out whether a column just had Yes No True False values in it">

		<cfargument name="loadTableColumn" required = "yes">

						<cfquery name = "checkForTrueYes" datasource="#THIS.dataSource#">
						select
							count(case when convert(varchar,[#loadTableColumn#]) in ('1-','1','Yes','True','Y','Ye') then 1 else null end) as CountOfYes,
							count(case when convert(varchar,[#loadTableColumn#]) in ('0','No','False','N') then 1 else null end) as CountOfNo,
							count(case when isNull(convert(varchar,[#loadTableColumn#]),'') = '' then 1 else null end) as CountOfBlank,
							count(case when isNull(convert(varchar(250),[#loadTableColumn#]),'') not in ('-1','1','Yes','True','0','No','False','Y','N','Ye','') then 1 else null end ) as CountOfOthers
						from
							#THIS.TableName#
						</cfquery>

			<cfreturn checkForTrueYes>
	</cffunction>

	<cffunction access="public" name="doesColumnHaveYesNoData" output="true" hint="works out whether a column just had Yes No True False values in it">
			<cfargument name="loadTableColumn" required = "yes">
					<cfset checkForTrueYes = columnYesNoDataQuery (loadTableColumn)>
						<cfif checkForTrueYes.countOfYes is not 0 and checkForTrueYes.countOfNo is not 0>
							<cfreturn true>
						<cfelse>
							<cfreturn false>
						</cfif>


	</cffunction>

	<cffunction access="public" name="doesColumnOnlyHaveYesNoData" output="true" hint="works out whether a column just had Yes No True False values in it">

		<cfargument name="loadTableColumn" required = "yes">

					<cfset checkForTrueYes = columnYesNoDataQuery (loadTableColumn)>
						<cfif checkForTrueYes.countOfYes is not 0 and checkForTrueYes.countOfOthers is 0>
							<cfreturn true>
						<cfelse>
							<cfreturn false>
						</cfif>

	</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertAllFlagData" output="true" hint="">
		<cfargument name="reload" default = "false">

		<cfreturn insertFlagDataForThisCol (loadTableColumn = "", argumentcollection = arguments)>
	</cffunction>

	<cffunction access="public" name="insertFlagDataForThisCol" output="true" hint="Inserts distinct values from the outside datasource against flagID ">

		<cfargument name="loadTableColumn" default = "">
		<cfargument name="reload" default = "false">
		<cfargument name="ignoreBadData" default = "false">	<!--- no UI yet --->
		<cfargument name="updateExistingData" default = "false">   <!--- no UI yet --->

		<cfset var insertLogData = "">
		<cfset message = "">

		<cfquery name="getColumnsToMap" datasource="#THIS.dataSource#">
			SELECT distinct [loadTableColumnNAme] as load_column, ft.[name] as flagType, fg.entityTypeID, ft.dataTableFullName, Mappingtype_ as mappingType, dataloadmappingid, automapped,data_type
			From
			vdataloadmappingAll dlm
			INNER JOIN flag f ON isNumeric(dlm.MappedTo) = 1 and ((dlm.mappingType_ = 'Flag' and f.flagID = dlm.MappedTo) or (dlm.mappingType_ = 'FlagGroup' and f.flagGroupID = dlm.MappedTo) )
			INNER JOIN flagGroup fg ON fg.flagGroupID = f.flagGroupID
			INNER JOIN flagType ft ON ft.flagTypeID = fg.flagTypeID

 			WHERE
				mappingType_ like '%Flag%'
				AND load_Table =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfif loadTableColumn is not "">
				<!--- this is in here if you want to do one at a time --->
				AND loadTableColumnName =  <cf_queryparam value="#loadTableColumn#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			<cfif not reload>
				and loadedDate is null
			</cfif>

		</cfquery>

		<cfset rowsAdded = 0>
		<cfif getColumnsToMap.recordCount is not 0>

			<!--- WAB hack in a requesttimeout based on number of flags, number of items in table and say 1 second per insert --->
			<cfquery name="getCount" datasource="#THIS.dataSource#">
			select count(*) as number from #THIS.tableName#
			</cfquery>

			<cfset timeout = max(60, getCount.number * getColumnsToMap.recordCount) >
			<cfsettinG requesttimeout="#timeout#">

			<cfloop query="getColumnsToMap">

				<cfif automapped is 1>
					<cfset message = message & "#load_column# cannot be loaded because the mapping has not been confirmed<BR>">
				<cfelse>
						<cfset entityIDColumn = application.entityType[entityTypeID].uniquekey>
						<cfset entityIDColumnOnEntityTable = entityIDColumn>
	 					<cfset entityActionColumn = application.entityType[entityTypeID].tablename & "Action">
						<cfif getColumnsToMap.entityTypeID eq "2">
							<cfset entityIDColumn = "orgid">
							<cfset entityActionColumn = "orgAction">
						</cfif>

		<!--- WAB 2008/12   altered query to deal with whole flaggroups, needed to reorder a bit
			WAb 2009/03/02 removed this code to a separate function
		--->

					<cfset basicQuery = getFlagMappingSnippet(load_column = load_column)>

<!--- 						<cfsavecontent variable="basicQuery">
							<!--- FROM --->
								V#THIS.tableName# dl   <!--- wab 2009/01/28 added the  v --->
							INNER JOIN
								#application.entityType[entityTypeID].tablename# e WITH (NOLOCK) ON dl.#entityIDColumn# = e.#entityIDColumnOnEntityTable#							<!--- WAB added this inner join to ensure that only load if the entity has been loaded --->
							INNER JOIN
								( vdataLoadMappingDetail FM WITH (NOLOCK)
										inner join
									flag f1 WITH (NOLOCK) on
											(fm.mappingType = 'flag' and f1.flagid = fm.mappedTo)
											or (fm.mappingType = 'flaggroup' and f1.flaggroupid = fm.mappedTo)
										inner join
									flaggroup fg WITH (NOLOCK) on fg.flaggroupid = f1.flaggroupid
								)

								ON
									<!---  any value in the dataload column sets the flag (boolean) --->
									(fm.mappingType = 'flag' and isNull(fm.loadData,'') in ('*Any','') and isnull(convert(varchar,dl.[#getColumnsToMap.load_column#]),'') <> '')
									<!--- any true value in the dataload column sets the flag --->
									 or (fm.mappingType = 'flag' and  isNull(fm.loadData,'') = '*AnyTrue' and isNull(convert(varchar,dl.[#getColumnsToMap.load_column#]),'') in ('1','-1','true','yes','Y','Ye') )    <!--- WAB 2007-10-22 added -1 --->
									 <!--- alue in the dataload column matches the value in the mapping table --->
									 or (fm.mappingType = 'flag' and isNull(fm.loadData,'') not in ('*Any','*AnyTrue','') and convert(varchar(250),dl.[#getColumnsToMap.load_column#]) = fm.loadData)
									<cfif getColumnsToMap.flagType eq "checkbox" or getColumnsToMap.flagType eq "radio">
									<!--- WAB 2008/09/17 wasn't picking up checkboxes from comma separated lists --->
									 or (fm.mappingType = 'flag'  and isNull(fm.loadData,'') not in ('*Any','*AnyTrue','') and charindex (','+fm.loadData + ',', ',' + convert(varchar(250),dl.[#getColumnsToMap.load_column#]) + ',' ) <> 0 )

									 <!--- WAB 2008/12/?  automatic mapping of a column to a whole flaggroup on name, textid or flagid--->
									or (fm.mappingType = 'flaggroup' and
											(   charindex (','+f1.name + ',', ',' + convert(varchar(250),dl.[#getColumnsToMap.load_column#]) + ',' ) <> 0
											 or (f1.flagtextID <> '' and charindex (','+f1.flagtextID + ',', ',' + convert(varchar(250),dl.[#getColumnsToMap.load_column#]) + ',' ) <> 0 )
 											 or charindex (','+convert(varchar(10),f1.flagID) + ',', ',' + convert(varchar(250),dl.[#getColumnsToMap.load_column#]) + ',' ) <> 0
											)
										)
									 </cfif>



							LEFT JOIN   <!--- this join joins up to any existing data in the flag tables --->
								#dataTableFullName# fd WITH (NOLOCK) on
									fd.entityid  = dl.#entityIDColumn#
									and
									fd.flagid = f1.flagid
							<cfif getColumnsToMap.flagType eq "radio">
							left join
								flag f2 WITH (NOLOCK) on fg.flaggroupid = f2.flaggroupid  and f2.flagid <> f1.flagid
							left join
								#dataTableFullName# fd2 WITH (NOLOCK) on fd2.flagid = f2.flagid and fd2.entityid = dl.#entityIDColumn#
							</cfif>
							<cfif getColumnsToMap.flagType eq "integerMultiple">
								and fd.data = dl.[#getColumnsToMap.load_column#]
							</cfif>

						WHERE fm.dataloadmappingid = #dataloadmappingid#

							and dl.#entityIDColumn# is not null


							<!---
							This line can be used to prevent flags for existing data being overwritten/updated
							commented out until we devise a way of using it
							AND dl.#entityIDColumn# is not null and (dl.#entityActionColumn# like '%for Inserting%' or dl.#entityActionColumn# like '%to be inserted%')

							--->
						</cfsavecontent>
 --->

					<cfif getColumnsToMap.flagType eq "radio">
						<!--- as its a radio first delete duplicate flags
						WAB: err this is a but radical - deletes rather more flags than it should!!!

						--->
						<cfquery name="deleteDuplicateRadioFlags" datasource="#THIS.dataSource#">
						delete fd2
						FROM
						#preserveSingleQuotes(basicQuery)#
						<!--- this line filters out any rows where there is existing data  --->
						AND fd.flagid is null

						and fd2.entityid is not null

						select @@rowcount as rowsdeleted
						</cfquery>

						<cfif deleteDuplicateRadioFlags.rowsdeleted is not 0>
							<cfset message = message & "#load_column#: #deleteDuplicateRadioFlags.rowsdeleted# radios deleted<BR>">
						</cfif>


					</cfif>


					<cfif getColumnsToMap.flagType eq "radio">
						<!--- check for multiple values in the dataload!
						WAB 2009/04/15 LID 2097  didn't work for whole flaggroups being mapped, altered count (distinct fm.mappedTo) to count (distinct f1.flagid)
						 --->
						<cfquery name="checkBadData" datasource="#THIS.dataSource#">
						select dl.#entityIDColumn# as entityid, count (distinct f1.flagid) countOfValues
						FROM
							#preserveSingleQuotes(basicQuery)#
							<!--- this line filters out any rows where there is existing data  --->
							AND fd.flagid is null

							and fd2.entityid is null
						group by dl.#entityIDColumn#
						having count (distinct f1.flagid) > 1
						</cfquery>

					<cfelseif listfindNoCase ("text,integer,date,integerMultiple,decimal",getColumnsToMap.flagType) >

						<!--- check for multiple values in the dataload! --->
						<cfquery name="checkBadData" datasource="#THIS.dataSource#">
						select
							dl.#entityIDColumn# as entityid
							<cfif listfindNoCase ("text,integer,date",getColumnsToMap.flagType) >
							, count (distinct convert(nvarchar(4000),dl.#getColumnsToMap.load_column#)) countOfValues
							</cfif>
						FROM
							#preserveSingleQuotes(basicQuery)#
							<cfif not updateExistingData>
								<!--- this line filters out any rows where there is existing data  --->
								AND fd.flagid is null
							</cfif>

						group by
							dl.#entityIDColumn#
								<cfif getColumnsToMap.flagType eq "date">
									, isDate (dl.#getColumnsToMap.load_column#)
								</cfif>
								<cfif listfindNoCase ("integer,integerMultiple,decimal",getColumnsToMap.flagType)>
									, isNumeric (dl.#getColumnsToMap.load_column#)
								</cfif>

						having
							1=0
							<cfif getColumnsToMap.flagType neq "integerMultiple">   <!--- integer multiple can have multiple values --->
							or count(distinct convert(nvarchar(4000),dl.#getColumnsToMap.load_column#)) > 1 <!--- checks for multiple values --->
												<!--- WAB 2008/03/25 added convert to nvarchar because fell over when encountering a text field.  Could test for this since I do have getColumnsToMap.data_type, but infact shouldn't matter doing the convert all the time (except performance) --->
							</cfif>
							<cfif getColumnsToMap.flagType eq "date">
							or isDate (dl.#getColumnsToMap.load_column#) =0
							</cfif>
							<cfif getColumnsToMap.flagType eq "integerMultiple">
							or isNumeric (dl.#getColumnsToMap.load_column#) =0
							</cfif>


						</cfquery>

					<cfelse>

						<cfset checkBadData	=queryNew("entityID","Integer")>   <!--- dummy query which will have recordcount = 0 --->

					</cfif>

					<cfif checkBadData.recordcount is not 0 and not ignoreBadData>
						<cfquery name="getBadData" datasource="#THIS.dataSource#">
						select distinct
							dl.#entityIDColumn#, dl.#getColumnsToMap.load_column#
						from
							#THIS.tableName# dl
						where
							dl.#entityIDColumn# in (#valuelist(checkBadData.entityid)#)
						order by
							dl.#entityIDColumn#
						</cfquery>
						<cfoutput><BR>This entity has multiple records in the dataload table with different values for the #entityIDColumn# data, or bad data values
							<cfdump var="#getBadData#">
						</cfoutput><cfbreak>
					</cfif>




			<cftry>

				<cfquery result ="insertFlagdataResult" name="insertFlagdata" datasource="#THIS.dataSource#">
						Insert into #getColumnsToMap.dataTableFullName#
							(flagid,
							entityID
							<cfif getColumnsToMap.flagType neq "radio" and getColumnsToMap.flagType neq "checkbox">
							,data
							</cfif>
							,created,
							createdBy,
							lastUpdated,
							lastUpdatedBy,
							lastUpdatedByPerson
							)
						Select f1.flagid,
							dl.#entityIDColumn#
							<cfif getColumnsToMap.flagType neq "radio" and getColumnsToMap.flagType neq "checkbox">
								<cfif getColumnsToMap.flagType eq "date">
								, convert(datetime,dl.#getColumnsToMap.load_column#)
								<cfelseif listfindnocase("text,ntext",getColumnsToMap.data_type)>
								, convert(nvarchar(4000),dl.#getColumnsToMap.load_column#)
								<cfelse>
								, dl.#getColumnsToMap.load_column#
								</cfif>
							</cfif>
							,getDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,getDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,#request.relayCurrentUser.personID#
						FROM
							#preserveSingleQuotes(basicQuery)#
							<!--- this line filters out any rows where there is existing data  --->
							and fd.flagid is null
							<cfif getColumnsToMap.flagType eq "radio">
							and fd2.entityid is null
							</cfif>
						<cfif ignoreBadData and checkBadData.recordcount is not 0>
							AND dl.#entityIDColumn# not  in ( <cf_queryparam value="#valuelist(checkBadData.entityid)#" CFSQLTYPE="cf_sql_integer"  list="true"> )

						</cfif>

						GROUP BY
							f1.flagid,
							dl.#entityIDColumn#
							<cfif getColumnsToMap.flagType neq "radio" and getColumnsToMap.flagType neq "checkbox">
								<cfif getColumnsToMap.flagType eq "date">
								, convert(datetime,dl.#getColumnsToMap.load_column#)
								<cfelseif listfindnocase("text,ntext",getColumnsToMap.data_type)>
								, convert(nvarchar(4000),dl.#getColumnsToMap.load_column#)
								<cfelse>
								, dl.#getColumnsToMap.load_column#
								</cfif>
							</cfif>


					</cfquery>

						<cfset message = message & "#load_column#:inserted  #insertFlagdataResult.recordcount# <BR>">


				<!--- WAB
					2007/11/07 update flag data
					if updateExistingData switch is on (nb no user interface yet and defaults to false)
				 --->

				<cfif updateExistingData and (getColumnsToMap.flagType neq "radio" and getColumnsToMap.flagType neq "checkbox")>
					<cfquery result ="updateFlagDataResult" name="updateFlagdata" datasource="#THIS.dataSource#">
						update
							#getColumnsToMap.dataTableFullName#
							set data =
								<cfif getColumnsToMap.flagType eq "date">
								 convert(datetime,dl.#getColumnsToMap.load_column#)
								<cfelse>
								 dl.#getColumnsToMap.load_column#
								</cfif>
								,lastUpdated = getDate(),
								lastUpdatedBy = #request.relayCurrentUser.userGroupID#,
								lastUpdatedByPerson = #request.relayCurrentUser.personID#
						FROM
							#preserveSingleQuotes(basicQuery)#
							<!--- this line filters out any rows where there is existing data  --->
							AND fd.flagid is not null
							<cfif getColumnsToMap.flagType eq "radio">
							and fd2.entityid is null
							</cfif>
							and data <>
								<cfif getColumnsToMap.flagType eq "date">
								 convert(datetime,dl.#getColumnsToMap.load_column#)
								<cfelse>
								 dl.#getColumnsToMap.load_column#
								</cfif>


						<cfif ignoreBadData and checkBadData.recordcount is not 0 >
							AND dl.#entityIDColumn# not  in ( <cf_queryparam value="#valuelist(checkBadData.entityid)#" CFSQLTYPE="cf_sql_integer"  list="true"> )

						</cfif>


					</cfquery>

					<cfset message = message & "#load_column#:updated #updateFlagdataResult.recordcount# <BR>">

				</cfif>




					<cfcatch>
						<cfoutput>
							Error occurred<BR>
							#cfcatch.message# <BR><BR>
							#cfcatch.detail# <BR><BR>
<cfif structkeyexists(cfcatch,"sql")>
#cfcatch.sql#<BR><BR>
</cfif>

							<CF_ABORT>
						</cfoutput>

					</cfcatch>

				</cftry>
					<cfquery name="updateLoadedDate" datasource="#THIS.dataSource#">
					update vDataLoadMapping
					set LoadedDate = getdate()
					where dataloadmappingid =  <cf_queryparam value="#dataloadmappingid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>


				</cfif>

			</cfloop>

		<cfelse>
			<cfset message = "No items">
		</cfif>


		<cfreturn message>

	</cffunction>


	<!---
	WAB 2009/02/03  Took this code into a separate function so that it could be used for pre-loading tests
	 --->
	<cffunction access="public" name="getFlagMappingSnippet" output="true" hint="returns query snippet for use in flag loading.  Snippet can be used to predict changes as well as do the job">
			<cfargument name="load_column" required = true>
			<cfargument name="checkForEntityExisting" default = true>	<!--- set to false when doing predictions of what is going to happen --->
			<cfargument name="checkForFlagsAlreadyExisting" default = true>
			<cfargument name="returnUnmappedRows" default = false>

			<cfset var basicquery_sql = "">

				<cfquery name="getColumnToMap" datasource="#THIS.dataSource#">
				SELECT distinct [loadTableColumnNAme] as load_column, ft.[name] as flagType, fg.entityTypeID, ft.dataTableFullName, Mappingtype_ as mappingType, dataloadmappingid, automapped,data_type
				From
				vdataloadmappingAll dlm
				INNER JOIN flag f ON isNumeric(dlm.MappedTo) = 1 and ((dlm.mappingType_ = 'Flag' and f.flagID = dlm.MappedTo) or (dlm.mappingType_ = 'FlagGroup' and f.flagGroupID = dlm.MappedTo) )
				INNER JOIN flagGroup fg ON fg.flagGroupID = f.flagGroupID
				INNER JOIN flagType ft ON ft.flagTypeID = fg.flagTypeID

	 			WHERE
					mappingType_ like '%Flag%'
					AND load_Table =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
					AND loadTableColumnName =  <cf_queryparam value="#load_column#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfset entityIDColumn = application.entityType[getColumnToMap.entityTypeID].uniquekey>
				<cfset entityIDColumnOnEntityTable = entityIDColumn>
				<cfif getColumnToMap.entityTypeID eq "2">
					<cfset entityIDColumn = "orgid">
				</cfif>

						<cfsavecontent variable="basicQuery_sql">
							<!--- FROM --->
								V#THIS.tableName# dl   <!--- wab 2009/01/28 added the  v --->
							<cfif checkForEntityExisting>
							INNER JOIN
								#application.entityType[getColumnToMap.entityTypeID].tablename# e WITH (NOLOCK) ON dl.#entityIDColumn# = e.#entityIDColumnOnEntityTable#							<!--- WAB added this inner join to ensure that only load if the entity has been loaded --->
							</cfif>
							<cfif returnUnmappedRows >
								LEFT JOIN
							<CFELSE>
								INNER JOIN
							</CFIF>

								( vdataLoadMappingDetail FM WITH (NOLOCK)
										inner join
									flag f1 WITH (NOLOCK) on
											(fm.mappingType = 'flag' and f1.flagid = fm.mappedTo)
											or (fm.mappingType = 'flaggroup' and f1.flaggroupid = fm.mappedTo)
										inner join
									flaggroup fg WITH (NOLOCK) on fg.flaggroupid = f1.flaggroupid
								)

								ON

									fm.dataloadmappingid = #getColumnToMap.dataloadmappingid#
									and (
										<!---  any value in the dataload column sets the flag (boolean) --->
											(fm.mappingType = 'flag' and isNull(fm.loadData,'') in ('*Any','') and isnull(convert(varchar,dl.[#getColumnToMap.load_column#]),'') <> '')
										<!--- any true value in the dataload column sets the flag --->
										 or (fm.mappingType = 'flag' and  isNull(fm.loadData,'') = '*AnyTrue' and isNull(convert(varchar,dl.[#getColumnToMap.load_column#]),'') in ('1','-1','true','yes','Y','Ye') )    <!--- WAB 2007-10-22 added -1 --->
										 <!--- alue in the dataload column matches the value in the mapping table --->
										 or (fm.mappingType = 'flag' and isNull(fm.loadData,'') not in ('*Any','*AnyTrue','') and convert(varchar(250),dl.[#getColumnToMap.load_column#]) = fm.loadData)
										<cfif getColumnToMap.flagType eq "checkbox" or getColumnToMap.flagType eq "radio">
										<!--- WAB 2008/09/17 wasn't picking up checkboxes from comma separated lists --->
										 or (fm.mappingType = 'flag'  and isNull(fm.loadData,'') not in ('*Any','*AnyTrue','') and charindex (','+fm.loadData + ',', ',' + convert(varchar(250),dl.[#getColumnToMap.load_column#]) + ',' ) <> 0 )

										 <!--- WAB 2008/12/?  automatic mapping of a column to a whole flaggroup on name, textid or flagid--->
										or (fm.mappingType = 'flaggroup' and
												(   charindex (','+f1.name + ',', ',' + convert(varchar(250),dl.[#getColumnToMap.load_column#]) + ',' ) <> 0
												 or (f1.flagtextID <> '' and charindex (','+f1.flagtextID + ',', ',' + convert(varchar(250),dl.[#getColumnToMap.load_column#]) + ',' ) <> 0 )
	 											 or charindex (','+convert(varchar(10),f1.flagID) + ',', ',' + convert(varchar(250),dl.[#getColumnToMap.load_column#]) + ',' ) <> 0
												)
											)
										 </cfif>
										)


							<cfif checkForFlagsAlreadyExisting>
								LEFT JOIN   <!--- this join joins up to any existing data in the flag tables --->
									#getColumnToMap.dataTableFullName# fd WITH (NOLOCK) on
										fd.entityid  = dl.#entityIDColumn#
										and
										fd.flagid = f1.flagid
								<cfif getColumnToMap.flagType eq "radio">
								left join
									flag f2 WITH (NOLOCK) on fg.flaggroupid = f2.flaggroupid  and f2.flagid <> f1.flagid
								left join
									#getColumnToMap.dataTableFullName# fd2 WITH (NOLOCK) on fd2.flagid = f2.flagid and fd2.entityid = dl.#entityIDColumn#
								</cfif>
								<cfif getColumnToMap.flagType eq "integerMultiple">
									and fd.data = dl.[#load_column#]
								</cfif>
							</cfif>

						WHERE
							1=1
							<cfif not returnUnmappedRows >
								and fm.dataloadmappingid is not null
							</cfif>
							<cfif checkForEntityExisting>
								and dl.#entityIDColumn# is not null
							</cfif>

							<!---
							This line can be used to prevent flags for existing data being overwritten/updated
							commented out until we devise a way of using it
							AND dl.#entityIDColumn# is not null and (dl.#entityActionColumn# like '%for Inserting%' or dl.#entityActionColumn# like '%to be inserted%')

							--->
						</cfsavecontent>

		<CFRETURN basicquery_sql>
	</CFFUNCTION>

	<cffunction access="public" name="predictFlagMappingResult" output="true" hint="">
			<cfargument name="load_column" required=true>

			<cfset var result = structNew()>
			<cfset snippet = getFlagMappingSnippet(load_column = load_column,checkForEntityExisting= false,checkForFlagsAlreadyExisting=false,returnUnmappedRows=true)>

			<cfquery name="result.AllData" datasource="#THIS.dataSource#">
				select
					dl.[#load_column#] as dataValue, f1.name as FlagName,f1.flagid,
					count(*) as  number_of_Rows
					from
					#preserveSingleQuotes(snippet)#

					group by
						dl.[#load_column#],f1.name,f1.flagid

			</cfquery>

			<cfquery name= "result.mappedData" dbtype="query">
			select * from result.AllData where flagid is not null
			</cfquery>
			<cfquery name= "result.unmappedData" dbtype="query">
			select * from result.AllData where flagid is null and dataValue <> '' and dataValue is not null
			</cfquery>
			<cfquery name= "blank" dbtype="query">
			select * from result.AllData where dataValue = '' or dataValue is null
			</cfquery>
			<cfif  blank.recordCount is 0>
				<cfset result.blankRows = 0>
			<cfelse>
				<cfset result.blankRows = blank.number_of_rows>
			</cfif>






		<cfreturn result>
	</cffunction>


</cfcomponent>

