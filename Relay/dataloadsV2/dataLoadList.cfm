<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			dataloadList.cfm	
Author:				SWJ
Date started:		07/May/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

1.  Change this screen so that it uses Table to object and pulls back
just significant columns e.g. company name, firstname, lastname, some address fields country and 
relay countryID and some of the action columns and badrow

2.  create filters by country

3.  Allow for searching by compnay name, firstname last name and country
 --->

<cf_title>Sample Data</cf_title>

<cfparam name="hideheader" default="no">
<cfparam name="tableName" default="dataloads">
<cfparam name="colList" default="">
<cfparam name="action" default="viewcols">
<cfparam name="numRowsToReturn" default="20">
<cfparam name="keyColumnList" default="">
<cfparam name="keyColumnURLList" default="">

<cfmodule template="../templates/viewTableColsandData.cfm"
						hideHeader="#hideHeader#"
						tableName="#tableName#"
						colList="#colList#"
						action="#action#"
						numRowsToReturn="#numRowsToReturn#"
						keyColumnList="#keyColumnList#"
						keyColumnURLList="#keyColumnURLList#"
					>