<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2007-10-02   WAB changes to add dataLoadTypeTable
2008/06/25	 SSS Took the database check out so the database table should not exist before this goes ahead
2008-07-02	 SSS Added a switch that makes the dataloads work the old way and the new way switch name dataloadfile
				 It set in relay.ini and paramed in this file to false
2008/11/04	SSS	 Changes made to the way dtatloads work and wording
2009-01-29  SSS  File to db contains a <cfrethrow statement so I have added cftry/catch to the function calls in order to display a error message
2009/02/25 	WAB	 name of dataload table had ben restricted to alpha - which wasn't much use, changed to variablename  ie alphanumeric  plus _
2009/04/14  SSS  I have added a request time out that will help with larger files
2009/04/14  SSS  I have increased how big the file can be before you are warned seems the real life dataloads can be quite big
2009/04/27 SSS Added some functionality that will help have mulitiple dataloadtypes in the future
2009/05/05 SSS P-Len008 Added some code to allow dataload types to work
2009/05/11 SSS Chaged the sample data to show 10 sample rows using tfqo
2009/05/15 SSS LH-2215 If the directory does not exsist it will be created before the file is uploaded
2009/06/05	NJH	P-SNY063 - changed the return type for fileToDB as a struct.. get the query from the structure
2010/04/26 AJC LID3239 Change dataload upload location
2010/08/02	NJH	8.3 - changed request.clusteredAbsolutePath to session.clusteredAbsolutePath
2010-01-11 	NYB  LHID5291 removed session.clusteredAbsolutePath - cfset and usage in filetodb call
2011/05/04  WAB LID 6454 Slow speed of loading file into loadtable - altered code in dataTransfers.cfc and reduced the number of rows to insert in each query
			Also Added an Ajax function which gives feedback on status of loading file
2011/06/01  WAB 2011/06/01  LID 6769  Security Scan - Link to editFileContents.cfm
2011-07-25 	NYB  LHID6768 - issue with system not finding uploaded file - because it's uploaded to a different instance then the system is looking for
2015-11-11  ACPK    Added Bootstrap

 --->

<cf_includeonce template="/templates/relayFormJavaScripts.cfm">
<cf_includejavascriptonce template="/javascript/extextension.js">
<cf_includejavascriptonce template="/javascript/openwin.js">


<cfscript>
	// create an instance of the object
	dataLoadObject = createObject("component","relayDataloadFlagsV2");
	dataLoadObject.dataSource = application.siteDataSource;
</cfscript>

<cfif isDefined("form.loadtable")>
	<cfset dataLoadObject.tableName = form.loadtable>
</cfif>

<cfif isDefined("form.saveAction") and form.saveAction eq "createLoadTable">
	<!--- ==============================================================================
	    Create the load table based on the format file
	=============================================================================== --->
	<cfinvoke component="#dataloadObject#" method="createLoadTable" returnvariable="message">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="debugMode" value="no"/>
		<cfinvokeargument name="dataLoadTable" value="#form.loadTable#"/>
	</cfinvoke>
</cfif>

<cfif isDefined("form.saveAction") and form.saveAction eq "insertData">
	<!--- ==============================================================================
	    Insert or update a record in datasource for this load table
	=============================================================================== --->
	<cfinvoke component="#dataloadObject#" method="insertDataIntoLoadTable" returnvariable="message">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="debugMode" value="no"/>
		<cfinvokeargument name="dataLoadTable" value="#form.loadTable#"/>
	</cfinvoke>

</cfif>

<cfsetting requesttimeout="1800" showdebugoutput="no">
<cfparam name="message" type="string" default="">
<cfparam name="dataSourceID" type="numeric" default="0">
<cfparam name="request.dataloadfile" type="boolean" default="true">
<!--- START: 2010/04/26 AJC LID3239 Change dataload upload location --->
<cfparam name="session.clusteredAbsolutePath" default="#application.paths.userfiles#"> <!--- NJH 2010/08/02 8.3 0 changed from request to session --->
<!--- END: 2010/04/26 AJC LID3239 Change dataload upload location --->

<cfif isDefined("form.saveAction") and form.saveAction eq "saveRecord">
<!--- 	<cfset myDate = CreateDateTime(mid(FORM.txtDate,7,4), mid(FORM.txtDate,4,2), left(FORM.txtDate,2),
		 FORM.frmHour, FORM.frmMinute, 00)>
 --->
<!--- ==============================================================================
    Insert or update a record in datasource for this load table
=============================================================================== --->

		<!--- SSS 24-06-2008 check to see if the dataload already exists --->
		<!--- SSS 02-07-2008 Added switch to turn of if dataloads need to be used the old way --->
	<cfif request.dataloadfile>
		<cfquery name="checkDataLoadTable" datasource="#application.siteDataSource#">
			select *
			from DataSource
			where loadtable =  <cf_queryparam value="dataload#form.loadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfif checkDataLoadTable.recordcount EQ 1>
		Dataload Table already exists. Retry with another name.
		<CF_ABORT>
		</cfif>
	</cfif>
	<cfif form.datasourceID gt 0>
		<cfquery name="updateDataSource" datasource="#application.siteDataSource#">
			UPDATE [DataSource]
				SET [Description] =  <cf_queryparam value="#form.description#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					<!--- [EmailChanges]=<EmailChanges,varchar(50),>,  --->
					[LastUpdated]=getDate(),
					[LastUpdatedBy]=#request.relayCurrentUser.usergroupid#,
					<!--- [DateReceived]=<DateReceived,datetime,>,
					[DateRequired]=<DateRequired,datetime,>,
					[DateLoaded]=<DateLoaded,datetime,>,
					[CurrentStatus]=<CurrentStatus,varchar(50),>,
					[DataSourceIdLoaded]=<DataSourceIdLoaded,bit,>, --->
					<!--- [loadType]='#loadType#',  --->
					dataloadTypeID =  <cf_queryparam value="#dataloadTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					[defaultCountryID] =  <cf_queryparam value="#defaultCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					[organisationTypeID] =  <cf_queryparam value="#OrganisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			WHERE datasourceID =  <cf_queryparam value="#form.datasourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfset datasourceID = form.datasourceID>
	<cfelse>
		<cfset insertTime = now()>
		<cfquery name="addDataSource" datasource="#application.siteDataSource#">
			INSERT INTO [DataSource]([Description], [loadtable],
			[CreatedBy], [Created],
			[LastUpdatedBy], [LastUpdated],
			[DateReceived], <!--- [DateRequired], [DateLoaded], --->
			<!--- [CurrentStatus], --->
			<!--- [loadType], ---> dataloadTypeID ,[defaultCountryID], [organisationTypeID])
			VALUES(<cf_queryparam value="#form.Description#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="dataload#form.loadTable#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#insertTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
			<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#insertTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
			<cf_queryparam value="#insertTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
			<!--- #DateRequired#, #DateLoaded#, #CurrentStatus#, --->
			<!--- '#form.loadType#', ---> <cf_queryparam value="#form.dataloadTypeID#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#form.defaultCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#form.OrganisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >)

			select scope_identity() as dataSourceID
		</cfquery>
<!--- 		<cfquery name="getDataSource" datasource="#application.siteDataSource#">
			select dataSourceID from datasource
			where loadtable = '#form.loadtable#' and created = #insertTime#
		</cfquery>
 --->		<cfset datasourceID = addDataSource.datasourceID>

	 		<cfset dataLoadObject.tableName = "dataload"&form.loadtable>  <!--- WAb 2008/03/25 put this line in here.  Sometimes form.loadtable has the dataload prefix and sometimes not, here is does not so need to get right valueinto the cfc--->
	</cfif>

<!--- ==============================================================================
    UPLOAD the data files if the loadTable name is unique
=============================================================================== --->
	<!--- START: 2010/04/26 AJC LID3239 Change dataload upload location --->
	<cfset curDirectory = "#application.paths.userfiles#\dataloads\">
	<!--- END: 2010/04/26 AJC LID3239 Change dataload upload location --->

	<!--- SSS 2009/05/15 Added this before we upload the file so if dir not there it is created --->
	<cfif not directoryExists("#curDirectory#")>
		<cfset application.com.globalFunctions.CreateDirectoryRecursive(FullPath=curDirectory)>
	</cfif>
	<cfif isDefined("form.dataForLoading") and form.dataForLoading neq "">
		<cfset dataFileName = loadTable & "_data.csv">
		<!--- NJH 2011/01/10 LID 4963 - replace the call to the custom file with a function call --->
		<cfset fileDetails = application.com.fileManager.uploadFile(fileField="dataForLoading",destination=curDirectory,renameFileTo=dataFileName,nameConflict="overwrite",accept="text/plain,application/octet-stream,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",weight=6000)>
		<cfset message = fileDetails.message>
		<!--- <cf_FileUpload
			directory="#curDirectory#"
			weight="6000"
			nameOfFiles="dataForLoading"
			renameFileTo="#dataFileName#"
			nameConflict="overwrite"
			accept="text/*,application/octet-stream,application/vnd.ms-excel"
			default="na"> --->
	</cfif>
	<cfif isDefined("form.dataFormatFile") and form.dataFormatFile neq "">
		<cfset dataFormatFileName = loadTable & "_format.csv">
		<cfset fileDetails = application.com.fileManager.uploadFile(fileField="dataFormatFile",destination=curDirectory,renameTo=dataFormatFileName,nameConflict="overwrite",accept="text/plain,application/octet-stream",weight=500)>
		<cfif message eq ""> <!--- don't overwrite the message from the first file in case there was a problem --->
			<cfset message = fileDetails.message>
		</cfif>
		<!--- <cf_FileUpload
			directory="#curDirectory#"
			weight="500"
			nameOfFiles="dataFormatFile"
			renameFileTo="#dataFormatFileName#"
			nameConflict="overwrite"
			accept="text/*,application/octet-stream"
			default="na"> --->
	</cfif>

	<cfif isdefined("DataLoadMappingID") and DataLoadMappingID is not "">
		<cfset x = dataLoadObject.copyDataLoadMappings(fromDataLoadID = DataLoadMappingID, toDataLoadID =datasourceID )>

	</cfif>


</cfif>

<cfif datasourceID gt 0>
<!--- SSS 2009/04/27 changed the query so that the dataloadtypetextID is brought back in the function --->
	<cfquery name="getDataSource" datasource="#application.siteDataSource#">
		select * from datasource ds inner join
			[dataLoadtype] dt on ds.dataloadtypeID = dt.dataloadtypeID
		where dataSourceID =  <cf_queryparam value="#dataSourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset dataLoadTable = getDataSource.LoadTable>
	<!--- 2008-06-24 SSS got rid as need datalaods to create the table if it is not there --->
	<!--- 2008-07-02 SSS added a switch just incase dataloads need to be used the old way --->
	<cfif not request.dataloadfile>
	 	<cfquery name="checkDataLoadTable" datasource="#application.siteDataSource#">
			if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME =  <cf_queryparam value="#dataLoadTable#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				select count(*) as numRecords from #dataLoadTable#
			else
			Begin
				exec sp_archiveTable @tablename =  <cf_queryparam value="#dataLoadTable#" CFSQLTYPE="CF_SQL_VARCHAR" > , @method = 'get'
				if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME =  <cf_queryparam value="#dataLoadTable#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					select count(*) as numRecords from #dataLoadTable#
			End
	</cfquery>
	</cfif>
	<cfif isDefined("checkDataLoadTable.numRecords") and checkDataLoadTable.numRecords gt 0>
		<cfset loadTableRecordCount = checkDataLoadTable.numRecords>
		<cfquery name="setUnComplete" datasource="#application.siteDataSource#">
			update dataSource set dateLoaded = null
			where datasourceID =  <cf_queryparam value="#datasourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	<cfelse>
		<cfset loadTableRecordCount = "0">
	</cfif>

<cfelse>
<cfscript>
	getDataSource = QueryNew("dataSourceID, loadTable, description, dateReceived, defaultCountryID,  dataloadTypeID, OrganisationTypeID");
	// add some rows in the query and set the cells in the query
	newRow  = QueryAddRow(getDataSource);
		temp = QuerySetCell(getDataSource, "dataSourceID", "0");
		temp = QuerySetCell(getDataSource, "loadTable", "");
		temp = QuerySetCell(getDataSource, "description", "");
		temp = QuerySetCell(getDataSource, "dateReceived", "#dateFormat(now(),'dd-mmm-yyyy')#");
		temp = QuerySetCell(getDataSource, "defaultCountryID", "9");
//		temp = QuerySetCell(getDataSource, "loadType", "");
		temp = QuerySetCell(getDataSource, "dataloadTypeID", "");
		temp = QuerySetCell(getDataSource, "OrganisationTypeID", "");
		//temp = QuerySetCell(getDataSource, "description", "");
	</cfscript>
</cfif>
<!--- SSS 2008/06/25 Load the file to the database --->
<cfif isdefined("loadfile")>
	<!--- SSS 2008/11/04 loads all the subfunctions that can be run once the data file is loaded into the database --->
	<cfinvoke component="#dataLoadObject#" method="listDataSourceFunctions" returnvariable="qDataSourceSubFunctions">
		<cfinvokeargument name="functionType" value="SubFunction"/>
	</cfinvoke>

	<cftry>
	<!--- NYB 2010-01-11 LHID5291: in filetodb: 1. replaced dir="/content/dataloads" with dir="/dataloads"
	NJH 2011/11/10 LID 8099: dataload failure removed the destination directory as making is asscessible is done in filetodb.
	 --->
	<cfset first20check = application.com.datatransfers.filetodb(dir="/dataloads",
															filename=url.Filename,
															Delim=",",
															textqualifier="""",
															Tablename=Url.tablename,
															ColumnNames= "true",
															sourceCollectionMethod="http",
															sourceDirectory="#application.paths.userFiles#\dataloads")>

	<cfcatch type="any">
		<cfif cfcatch.detail eq "Column names must be valid variable names. They must start with a letter and can only include letters, numbers, and underscores.">
			<cfoutput>#htmleditformat(cfcatch.detail)#</cfoutput> Please modify the column names in your csv file and upload again.
		<cfelse>
			<cfset application.com.errorHandler.recordRelayError_Warning (caughtError=cfcatch,type="Dataload Error")>
			<cfoutput>cfcatch.detail1=  #cfcatch.detail#<cfdump var="#cfcatch#"></cfoutput>
		</cfif>
		<CF_ABORT>
	</cfcatch>
	</cftry>
	<!--- SSS 2008/11/04 load all subfunctions once the data is in the table --->
	<cfoutput>
	<cfinvoke component="#dataLoadObject#" method="initTable" returnvariable="message2">
		<cfinvokeargument name="tablename" value="#Url.tablename#"/>
	</cfinvoke>
	<cfloop query="qDataSourceSubFunctions">
		<cfinvoke component="#dataloadObject#" method="#FunctionTextID#" returnvariable="message">
		</cfinvoke>

		<cfinvoke component="#dataloadObject#" method="addDataSourceHistory" returnvariable="message">
			<cfinvokeargument name="action" value="#FunctionTextID#"/>
		</cfinvoke>
	</cfloop>
	</cfoutput>
</cfif>
<cfif isdefined("startfresh")>
	<cfquery name="resetdataloadView" datasource="#application.sitedatasource#">
		if exists (SELECT * FROM sysobjects WHERE type='v' and name =  <cf_queryparam value="v#url.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			drop view v#url.tablename#
	</cfquery>
	<cfquery name="resetdataloadtable" datasource="#application.sitedatasource#">
		if exists (SELECT * FROM sysobjects WHERE type='u' and name =  <cf_queryparam value="#url.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			drop table #url.tablename#
	</cfquery>
	<cfquery name="addSartNewHistory" datasource="#application.sitedatasource#">
		insert into dataSourceHistory(action, DataSourceID)
		values ('startnew', <cf_queryparam value="#datasourceid#" CFSQLTYPE="CF_SQL_INTEGER" >)
	</cfquery>
</cfif>
<!--- getDataLoadTypes --->
<!--- 2008/11/05 SSS restricted to POL data only for now - other loads are a future posssibility--->
<!--- SSS 2009/04/27 p-len008 changed the query so that it brings back geodata functions also --->
	<cfquery name="dataLoadTypes" datasource="#application.siteDataSource#">
	select  dataLoadTypeid as dataValue, name as displayValue
		from dataLoadType
		where loadTypeGroup = 'POL'
		or loadTypeGroup = 'GEO'
	</cfquery>

<!--- get mappings used for other dataloads --->
	<cfquery name="dataLoadsWithMappings" datasource="#application.siteDataSource#">
	select  distinct dataSourceid as dataValue, description + ' ('   + loadtable + ')' as displayValue from datasource ds  inner join dataloadmapping dlm on ds.dataSourceid = dlm.dataLoadid
	order by description + ' ('   + loadtable + ')'
	</cfquery>



<SCRIPT type="text/javascript">
function doForm(action) {
	// we want the load table function to fire
	//var agree=confirm("Are you sure you wish to continue?");
	//if (agree)
	//	return true ;
	//else
	//	return false ;
	if (action == "saveRecord") {
	// 2008/11/04 added javascript to make sure the correct fields are filled in before you try to save
			var form = document.dataLoadEditorForm;
			var errormessage ="";
			if(form.loadtable.value == ""){
				errormessage = errormessage + 'You must enter a name for the loadtable name\n\n';
			}
			if(form.Description.value == ""){
				errormessage = errormessage +'You must add a description\n';
			}
			if(errormessage == ""){
				form.saveAction.value = "saveRecord";
				form.submit();
			}
			else{
				alert(errormessage);
			}
		}
	else if (action == "createLoadTable") {
		document.dataLoadEditorForm.saveAction.value = "createLoadTable";
		document.dataLoadEditorForm.submit();
		}
	else if (action == "insertData") {
		document.dataLoadEditorForm.saveAction.value = "insertData";
		document.dataLoadEditorForm.submit();
		}
}
</script>
<!--- <SCRIPT type="text/javascript" src="../javascript/date Picker.js"></script> --->

<cfquery name="getCountryList" datasource="#application.siteDataSource#">
	SELECT countryID, countryDescription as display
	from country order by countryDescription
</cfquery>

<cfset editor = "yes">
<!--- <cfinclude template="dataloadsTopHead.cfm"> --->

<cfoutput>
	<form action="" method="post" class="form-horizontal" enctype="multipart/form-data" name="dataLoadEditorForm" id="dataLoadEditorForm">
		<input type="hidden" name="saveAction" value="">
		<CF_INPUT type="hidden" name="DatasourceID" value="#getDataSource.DatasourceID#">
		<div class="form-group">
			<div class="col-xs-12">
				<label></label>
				<cfif message neq "">#application.com.relayUI.message(message=message)#</cfif>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label>DataSource ID:</label>
				#htmleditformat(getDataSource.DatasourceID)#
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label class="required" for="loadtable">Load table name in the database:</label>
				<cfif getDataSource.DatasourceID gt 0 and isDefined("getDataSource.loadTable") and getDataSource.loadTable neq "">
					#htmleditformat(getDataSource.loadtable)#
					<CF_INPUT type="hidden" name="loadtable" value="#getDataSource.loadtable#">
				<cfelse>
				<!--- 2008/11/04 new wording added --->
					DataLoad<CF_relayValidatedField
					    id="loadtable"
						fieldName="loadtable"
						currentValue="#getDataSource.loadtable#"
						charType="variableName"
						size="50"
						maxLength="50"
						helpText="Please only enter alpha characters"
					>
					<!--- SWJ 26-Jun-08 added validation above
					<input type="text" name="loadtable" value="#getDataSource.loadtable#" size="50"> --->
				</cfif>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label class="required" for="Description">Description of the data load:</label>
				<!--- 2008/11/04 new wording added --->
				<CF_INPUT type="text" name="Description" class="form-control" value="#getDataSource.Description#" size="50">
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label for="defaultCountryID">The country records will be linked to if not specified</label>
				<select id="defaultCountryID" name="defaultCountryID" class="form-control">
					<cfloop query="getCountryList"><option value="#getCountryList.countryID#" #IIF(getDataSource.defaultCountryID IS getCountryList.countryID,DE('SELECTED'),DE(''))#>#htmleditformat(getCountryList.display)#</option></cfloop>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label for="dateReceived">Date data received:</label>
				<!--- <CF_INPUT type="text" name="dateReceived" value="#getDataSource.dateReceived#" size="20" title="MM/DD/YYYY" onfocus="showDP(this);"> --->
				<CF_relayDateField currentValue="#getDataSource.dateReceived#" id="dateReceived" fieldName="dateReceived" disableFromDate=#request.requestTime# enableRange="true">
			</div>
		</div>

		<!--- ==============================================================================
		SWJ  26-06-2008  Check to see if the load table exists
		=============================================================================== --->
		<cfset loadTableExists = application.com.dbtools.TableExists(tablename=getDataSource.loadTable)>
		<div class="form-group">
                  <div class="col-xs-12">
				<label for="dataloadTypeID">The type of data being loaded:</label>
				<!--- ==============================================================================
				SWJ  26-06-2008  If the load table exists use it's type as the default otherwise use reseller
				=============================================================================== --->
				<!--- SSS 2009/04/27 changed this code so that part of it works on the textID so that we can return the right functions for the dataload type --->
				<cfif datasourceID GT 0>
					<cfset currentLoadTypeTextID = getDataSource.dataloadTypeTextID>
					<cfset currentLoadTypeID = getDataSource.dataloadTypeID>
				<cfelse>
					<cfset currentLoadTypeTextID = 'Reseller'>
					<cfset currentLoadTypeID = 1>
				</cfif>
				<cf_displayValidValues id="dataloadTypeID" class="form-control" formfieldname = "dataloadTypeID"
						validValues = #dataLoadTypes# currentValue = #currentLoadTypeID#>
				<!--- SSS 2009/04/27 This is legacy so a check is being made so that stuff already in the database works as normal --->
				<cfif currentLoadTypeTextID EQ "Reseller" or currentLoadTypeTextID EQ "EndUser" or currentLoadTypeTextID EQ "InternalUser">
					<cfset dataloadTypeFunctions = 'general'>
				<cfelse>
					<cfquery name="qry_get_dataloadtypetextID" datasource="#application.siteDataSource#">
						select *
						from [dataloadtype]
						where dataloadtypetextID =  <cf_queryparam value="#currentLoadTypeTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfquery>
					<cfset dataloadTypeFunctions = qry_get_dataloadtypetextID.dataloadtypetextID>
				</cfif>
		<!--- 		<cfoutput>
				<select name="loadType">
					<cfloop list="Reseller Contacts,End User Contacts,Opportunity Data,Products,Profile Data,Sales Data,Sales Out Data,Internal Users" index="loopLoadType">
						<option value="#loopLoadType#" #IIF(getDataSource.loadType IS loopLoadType,DE('SELECTED'),DE(''))#>#loopLoadType#</option>
					</cfloop>
				</select>
				</cfoutput>
		 --->
			</div>
		</div>
		<!--- SSS 2008/06/25 Only load this if there is a table --->
		<cfif loadTableExists>
		<div class="form-group">
              <div class="col-xs-12">
				<label for="DataLoadMappingID">Use Mappings from Previous Dataload</label>
				<cf_displayValidValues id="DataLoadMappingID" class="form-control" formfieldname = "DataLoadMappingID" validValues = #dataLoadsWithMappings# >
			</div>
		</div>
		</cfif>
		<!--- AJC 2007-04-27 Query to populate organisation type drop down --->
		<cfquery name="qry_get_organisationTypes" datasource="#application.siteDataSource#">
			SELECT organisationTypeID,TypeTextID
			FROM organisationType
			WHERE Active=1
			ORDER BY sortIndex
		</cfquery>
		<cfif qry_get_organisationTypes.RecordCount gt 1>
			<div class="form-group">
                   <div class="col-xs-12">
					<label for="OrganisationTypeID">The type of organisations in the data being loaded:</label>
					<select id="OrganisationTypeID" name="OrganisationTypeID" class="form-control">
						<!--- ==============================================================================
						SWJ  26-06-2008  Org type should always be set so lets default it to the first in the list.  If that's wrong the user can chang it later.'
						=============================================================================== --->
						<!--- <option value="0" <cfif getDataSource.OrganisationTypeID is 0>selected</cfif>>n/a</option> --->
						<cfloop query="qry_get_organisationTypes">
							<option value="#organisationTypeID#" <cfif getDataSource.OrganisationTypeID is organisationTypeID>selected</cfif>>#htmleditformat(typeTextID)#</option>
						</cfloop>
					</select>
				</div>
			</div>
			<!--- If 1 Org Type set hidden value to it's ID --->
		<cfelseif qry_get_organisationTypes.RecordCount eq 1>
			<CF_INPUT Type="hidden" name="OrganisationTypeID" value="#qry_get_organisationTypes.organisationTypeID#">
			<!--- If no org types then set to a default of 1 --->
		<cfelse>
			<input Type="hidden" name="OrganisationTypeID" value="1">
		</cfif>

	<!--- ==============================================================================
	        Here is the file upload and edit section
	=============================================================================== --->
		<cfif getDataSource.DatasourceID gt 0>
		<!--- <tr>
			<td class="labelBG"><div class="label">Data format file</div></td>
			<td><input type="file" name="dataFormatFile" size="50">
				<cfif getDataSource.DatasourceID gt 0 and isDefined("getDataSource.loadTable") and getDataSource.loadTable neq "">
					<cfset formatFileName = getDataSource.loadTable & "_format.csv">
					<cfset thisFile = "#application.paths.content#\dataloads\#formatFileName#">
					<cfif fileExists(thisfile)>
						<br><A HREF="/fileManagement/editFileContents.cfm?filename=#formatFileName#&subdir=dataloads">Edit Content</a>
						<br><input type="button" name="CreateLoadTable" value="Create Load Table" onClick="javascript:doForm('createLoadTable')">
					<CFELSE>
						<br>Format file #formatFileName# not present in dataloads directory.
					</cfif>
				</cfif>
			</td>
		</tr> --->
			<!--- SSS 2008-07-02 added a switch so that the dataloads can work the old way --->
			<cfif request.dataloadfile>
				<cfif loadTableExists>
					<cfquery name="gethistorystatus" datasource="#application.siteDataSource#">
						select *
						from dataSourceHistory
						where datasourceID =  <cf_queryparam value="#getDataSource.DatasourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and action = 'insertData'
					</cfquery>
					<cfif gethistorystatus.recordcount EQ 0>
						<div class="form-group">
	                              <div class="col-xs-12">
							    <label>Reload csv file </label> Doing this will delete any work you have done to prepare </br>your data for loading and delete the load table.
							</div>
						</div>
						<div class="form-group">
	                              <div class="col-xs-12">
							    <a href="index.cfm?startfresh=1&editor=yes&datasourceid=#getDataSource.DatasourceID#&tablename=#getDataSource.loadTable#">Click here to re-load csv file and delete the load table</a>
						    </div>
					    </div>
					</cfif>
				<cfelse>
					<div class="form-group">
	                          <div class="col-xs-12">
						     <label>Data for loading </label> Please generate the file using Excel 2008 or ensure each row has a comma delimiter for every single column.
						</div>
					</div>
					<div class="form-group">
	                          <div class="col-xs-12">
							<input type="file" name="dataForLoading" size="50">
							<cfif getDataSource.DatasourceID gt 0 and isDefined("getDataSource.loadTable") and getDataSource.loadTable neq "">
								<cfset dataFileName = getDataSource.loadTable & "_data.csv">
								<!--- START: 2010/04/26 AJC LID3239 Change dataload upload location --->
								<cfset thisFile = "#application.paths.userfiles#\dataloads\#dataFileName#">
								<!--- END: 2010/04/26 AJC LID3239 Change dataload upload location --->
								<cfif fileExists(thisfile)>
									<!--- START: 2010/04/26 AJC LID3239 Change dataload upload location --->
									<br><A HREF="#application.com.security.encryptURL('/fileManagement/editFileContents.cfm?filename=#dataFileName#&subdir=dataloads&userFilesSection=userfiles')#">Edit Content</a>
									<!--- END: 2010/04/26 AJC LID3239 Change dataload upload location --->
									<!--- <br><input type="button" name="InsertData" value="Insert Data" onClick="javascript:doForm('insertData')"> --->
								<CFELSE>
									<br>Data file #htmleditformat(dataFileName)# not present in dataloads directory.
								</cfif>
							</cfif>
						</div>
					</div>
				</cfif>
			</cfif>
		</cfif>
	</form>
</cfoutput>
<!--- ==============================================================================
        Here is the functions section
=============================================================================== --->

<cfif getDataSource.DatasourceID gt 0 and isDefined("getDataSource.loadTable") and getDataSource.loadTable neq "">
	<cfset dataLoadObject.tableName = getDataSource.loadTable>

	<cfif isDefined("URL.action")>
		<cfinvoke component="#dataLoadObject#" method="#URL.action#" returnvariable="message">
		</cfinvoke>
		<cfinvoke component="#dataLoadObject#" method="addDataSourceHistory" returnvariable="message2">
			<cfinvokeargument name="action" value="#URL.action#"/>
		</cfinvoke>
	</cfif>
	<!--- 2008/11/04 changed the function call to only return Major functions --->
	<!--- SSS 2009/04/27 I also now send in a param that will give me functions for a specific dataload --->
	<cfinvoke component="#dataLoadObject#" method="listDataSourceFunctions" returnvariable="qDataSourceFunctions">
		<cfinvokeargument name="functionType" value="MajorFunction"/>
		<cfinvokeargument name="dataloadTypeFunctions" value="#dataloadTypeFunctions#"/>
	</cfinvoke>
	<cfinvoke component="#dataLoadObject#" method="listDataSourceHistory" returnvariable="qDataSourceHistory">
	</cfinvoke>
	<cfoutput>
	<!--- START: 2010/04/26 AJC LID3239 Change dataload upload location --->
	<cfif fileexists("#application.paths.UserFiles#\dataloads\#getDataSource.loadTable#_data.csv") and application.com.dbtools.TableExists(tablename=getDataSource.loadTable) EQ 0>
	<!--- END: 2010/04/26 AJC LID3239 Change dataload upload location --->
		<cftry>
		<!--- NJH 2009/06/05 P-SNY063 - function now returns a struct.. get the query from the struct --->
		<!--- NYB 2010-01-11 LHID5291: in filetodb: 1. replaced dir="/content/dataloads" with dir="/dataloads"
		NJH 2011/11/10 LID 8099: dataload failure removed the destination directory as making is asscessible is done in filetodb.
		--->
		<cfset first10checkStruct = application.com.datatransfers.filetodb(dir="/dataloads",
															filename=dataFileName,
															Delim=",",
															textqualifier="""",
															Tablename=getDataSource.loadTable,
															ColumnNames= "true",
															sourceCollectionMethod="http",
															nocheckloaddata="false",
															sourceDirectory="#application.paths.userFiles#\dataloads"
																)>

		<cfset first10check = first10checkStruct.importQuery>
		<cfcatch type="any">
			<cfif cfcatch.detail eq "Column names must be valid variable names. They must start with a letter and can only include letters, numbers, and underscores.">
				<cfoutput>#htmleditformat(cfcatch.detail)#</cfoutput> Please modify the column names in your csv file and upload again.
			<cfelse>
				<cfoutput>cfcatch.detail2=  #htmleditformat(cfcatch.detail)#<cfdump var="#cfcatch#"></cfoutput>
			</cfif>
			<CF_ABORT>
		</cfcatch>
		</cftry>

		<ol>
			<li>Check columns are correct.</li>
			<li>Make sure your Column headings do not have (spaces or other invalid chars).</li>
			<li>Once you have checked this on the sample data below, click the link below to copy the csv file data into the database load table.</li>
		</ol>
		<p><A HREF="index.cfm?editor=yes&datasourceid=#getDataSource.DatasourceID#&tablename=#getDataSource.loadTable#&filename=#dataFileName#&LoadFile='true'" onclick="doLoadStatus()">Copy data into the load table</A> <span id="loadStatus"></span></p>
			<!--- WAb 2011/05/04 added code to display number of rows processed (really for debugging) could be nicer with eggtimer and stuff, but does its job --->

				<script>
					var dataloadTableName = '<cfoutput>#jsStringFormat(getDataSource.loadTable)#</cfoutput>';
					function doLoadStatus() {
						window.setInterval(updateLoadStatus,5000)
					}

					function updateLoadStatus () {
						 var page = '/webservices/relayDataloadsWS.cfc?wsdl&returnformat=json&_cf_nodebug=true'
							  page = page + '&method=getRowCount'
						 var parameters = 'tablename=' + dataloadTableName
						var myAjax = new Ajax.Request(
	                        page,
	                        {
	                            method: 'post',
	                            parameters: parameters,
	                            onComplete:  function (requestObject,JSON) {
	                                json = requestObject.responseText.evalJSON(true)
	                  				$('loadStatus').innerHTML = json.COUNT + ' rows loaded'
	                                return true
	                            }
	                        });

					}


				</script>

		<!--- SSS 2009/05/11 This will display to rows of data as a test --->
		<CF_tableFromQueryObject queryObject="#first10check#"
		showTheseColumns="#first10check.Columnlist#"
		HidePageControls="yes"
		useInclude = "false"
		numRowsPerPage="10"
		>
	</cfif>
	</cfoutput>
	<cfif application.com.dbtools.TableExists(tablename=getDataSource.loadTable)>
	<div class="row">
		<cfoutput>
		<div><cfif IsSimpleValue(message) and message neq "">#application.com.relayUI.message(message=message)#</cfif></div>
		</cfoutput>
		<div class="col-xs-8">
			<TABLE CLASS="table table-hover">
				<tr>
					<td colspan="3" class="heading">Data Load functions.</td></td>
				</tr>
				<tr>
					<th>Function</th><th>Status</th><th>Description</th>
				</tr>
				<cfoutput query="qDataSourceFunctions">
				<TR>
					<TD HEIGHT="20" VALIGN="TOP">
						<cfif left(URL,6) eq "action">
						<A HREF="index.cfm?editor=yes&datasourceid=#getDataSource.DatasourceID#&#URL#&tablename=#getDataSource.loadTable#">#htmleditformat(currentRow)#. #htmleditformat(functionName)#</A>
						<cfelseif FindNoCase("javascript",url) NEQ 0>
						<A HREF="#URL#">#htmleditformat(currentRow)#. #htmleditformat(functionName)# </A>
						<cfelse>
							<A HREF="#URL#&tablename=#getDataSource.loadTable#">#htmleditformat(currentRow)#. #htmleditformat(functionName)# </A>
						</cfif>
						<!--- SSS 2009-05-06 some things open in a window or a new tab now P-len008 Begin--->
						<script>
						function openNewTabX(tabName,tabTitle,href,options) {
							openNewTab(tabName,tabTitle,iDomainAndRoot + href + '?tablename=#jsStringFormat(getDataSource.loadTable)#',options) ;
						}
						</script>
						<!--- P-len008 END --->
					</TD>
					<td><cfif qDataSourceFunctions.action neq "">Done</cfif></td>
					<TD VALIGN="Top">#htmleditformat(description)#</TD>
				</TR>
				</cfoutput>
			</TABLE>
		</div>
		<div class="col-xs-4">
			<TABLE CLASS="table table-hover">
				<tr>
					<td colspan="2" class="heading">What's been done.</td></td>
				</tr>
				<tr>
					<th>Action</th><th>Last done</th><th>Times</th>
				</tr>
				<cfoutput query="qDataSourceHistory">
				<TR>
					<TD VALIGN="TOP">
						#htmleditformat(functionResultText)#
					</TD>
					<TD VALIGN="Top">#dateformat(lastDone,"dd-mmm-yyy")#</TD>
					<td valign="top">#htmleditformat(numberofTimes)#</td>
				</TR>
				</cfoutput>
			</TABLE>
		</div>
	</div>
	</cfif>
</cfif>

<cfif IsQuery(message)>
	<CF_tableFromQueryObject queryObject="#message#" sortOrder="">
</cfif>



