<!--- �Relayware. All Rights Reserved 2014 --->


<!--- <cfinclude template="dataloadsTopHead.cfm"> --->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Load Action Status</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="javascript:history.back()" class="Submenu">Back</a>&nbsp;&nbsp;
		</TD>
	</TR>
	<tr>
		<td align="center">
			<p class="heading">
			This shows data marked as <cfoutput>"<strong><em>#htmleditformat(URLDecode(action))#</em></strong>"</cfoutput> in <cfoutput>#htmleditformat(tablename)#</cfoutput>.<br>
			</p>
		</td>
	</tr>
</TABLE>

<cfquery name="checkOrgMatches" datasource="#application.SiteDataSource#">
	<cfif checkTable eq "org">
		--check the org records are matched up correctly
		select distinct left(o.organisationName,30) as Org_Name_in_Relay,dl.companyname as Org_name_in_Dataload,
		o.organisationID as orgID_in_Relay, dl.orgID as Org_ID_in_Dataload,
		o.countryID as Country_in_Relay, dl.countryID as Country_in_Dataload
		from #tablename# dl
		LEFT OUTER organisation o ON o.organisationID = dl.orgID
		and organisationName <> companyname
		where dl.orgaction =  <cf_queryparam value="#URLDecode(action)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		order by left(o.organisationName,30)

	<cfelseif checkTable eq "loc">
		-- check location records match up correctly
		select distinct left(o.organisationName,30) as Org_Name_in_Relay,dl.companyname as Org_name_in_Dataload,
		l.address1, dl.address1, l.address2, dl.address2, l.address3, dl.address3,dl.postalcode, l.postalcode,
		o.organisationID, dl.orgID
		from #tablename# dl
		LEFT OUTER JOIN organisation o ON o.organisationID = dl.orgID
		LEFT OUTER JOIN location l ON l.locationid = dl.locationid
		<!--- and organisationName <> companyname --->
		where locationAction =  <cf_queryparam value="#URLDecode(action)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		order by left(o.organisationName,30)
	<cfelseif checkTable eq "per">
		select p.firstname+' '+p.lastname as person_in_relay,
			dl.firstname+' '+dl.lastname as person_in_dataload,
			left(o.organisationName,30) as Org_Name_in_Relay,dl.companyname as Org_name_in_Dataload
		from person p
		LEFT OUTER JOIN #tablename# dl on p.personid=dl.personID
		LEFT OUTER JOIN organisation o on p.organisationID = o.organisationID
		<!--- 	and organisationName <> companyname --->
		where personAction =  <cf_queryparam value="#URLDecode(action)#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfif>
</cfquery>

<CF_tableFromQueryObject
	useInclude="false"
	queryObject="#checkOrgMatches#"
	numRowsPerPage="200"
	sortOrder="">
