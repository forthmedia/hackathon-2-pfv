<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Amendment History:

Date (DD-MMM-YYYY)  Initials    What was changed
2007/09/06          WAB
2015-11-12          ACPK        Replaced table at top of page with bootstrap

--->

<!--- <cfinclude template="dataloadsTopHead.cfm"> --->
<!--- 2015-11-12    ACPK    Replaced table with bootstrap --->
<p>Load Action Status</p>
<p class="heading">
    This shows data marked as <cfoutput>"<strong><em>#htmleditformat(URLDecode(action))#</em></strong>"</cfoutput> in <cfoutput>#htmleditformat(tablename)#</cfoutput>.
</p>

<!--- WAB 2007/09/06
	Perform these queries on a view on the dataload table which aliases all the columns correctly
 --->
<cfset dataLoadObject = createObject("component","relayDataloadV2")>
<cfset dataLoadObject.initTable (datasource = application.siteDataSource, tablename = tablename)>
<cfset tablename = "v" & tablename>

<cfsetting enablecfoutputonly="No">

<cfif URLDecode(action) eq "Organisations to be inserted">

<cfquery name="checkOrgMatches" datasource="#application.SiteDataSource#">
	<cfif checkTable eq "org">
		--check the org records are matched up correctly
		select distinct dl.companyname as Org_name_in_Dataload, rowID, dl.orgID as Org_ID_in_Dataload,
		orgMatchName, left(companyname,10) as search_String, address1+' '+ postalcode +' '+ city as location_info,
		dl.countryID as Country_in_Dataload
		from #tablename# dl
		where dl.orgaction =  <cf_queryparam value="#URLDecode(action)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		order by dl.companyname

	<cfelseif checkTable eq "loc">
		-- check location records match up correctly
		select distinct dl.companyname as Org_name_in_Dataload,
		dl.address1, dl.address2, dl.address3,dl.postalcode,
		dl.orgID
		from #tablename# dl
		where locationAction =  <cf_queryparam value="#URLDecode(action)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		order by dl.companyname
	<cfelseif checkTable eq "per">
		select
			dl.firstname+' '+dl.lastname as person_in_dataload,
			dl.companyname as Org_name_in_Dataload
		from #tablename# dl
			and organisationName <> companyname
		where personAction =  <cf_queryparam value="#URLDecode(action)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		order by dl.companyname
	</cfif>
</cfquery>

<cfelse>

	<cfquery name="checkOrgMatches" datasource="#application.SiteDataSource#">
		<cfif checkTable eq "org">
			--check the org records are matched up correctly
			select distinct left(o.organisationName,30) as Org_Name_in_Relay,dl.companyname as Org_name_in_Dataload,
			o.organisationID as orgID_in_Relay, dl.orgID as Org_ID_in_Dataload,
			o.countryID as Country_in_Relay, dl.countryID as Country_in_Dataload
			from #tablename# dl
			LEFT OUTER JOIN organisation o ON o.organisationID = dl.orgID
			<!--- and organisationName <> companyname --->
			where dl.orgaction =  <cf_queryparam value="#action#" CFSQLTYPE="CF_SQL_VARCHAR" >
			order by left(o.organisationName,30)

		<cfelseif checkTable eq "loc">
			-- check location records match up correctly
			select distinct left(o.organisationName,30) as Org_Name_in_Relay,dl.companyname as Org_name_in_Dataload,
			l.address1, dl.address1, l.address2, dl.address2, l.address3, dl.address3,dl.postalcode, l.postalcode,
			o.organisationID, dl.orgID
			from #tablename# dl
			LEFT OUTER JOIN organisation o ON o.organisationID = dl.orgID
			LEFT OUTER JOIN location l ON l.locationid = dl.locationid
			<!--- and organisationName <> companyname --->
			where locationAction =  <cf_queryparam value="#action#" CFSQLTYPE="CF_SQL_VARCHAR" >
			order by left(o.organisationName,30)
		<cfelseif checkTable eq "per">
			select p.firstname+' '+p.lastname as person_in_relay,
				dl.firstname+' '+dl.lastname as person_in_dataload,
				left(o.organisationName,30) as Org_Name_in_Relay,dl.companyname as Org_name_in_Dataload
			from  #tablename# dl
			LEFT OUTER JOIN person p on p.personid=dl.personID
			LEFT OUTER JOIN organisation o on p.organisationID = o.organisationID
			<!--- 	and organisationName <> companyname --->
			where personAction =  <cf_queryparam value="#action#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
	</cfquery>

</cfif>

<cfif checkTable eq "org" and URLDecode(action) eq "Organisations to be inserted">
	<CF_tableFromQueryObject queryObject="#checkOrgMatches#"
		numRowsPerPage="200"

		keyColumnList="Org_Name_In_dataLoad,search_string"
		keyColumnURLList="findRelayOrgToLinkTo.cfm?dataLoadRowIDToUpdate=#checkOrgMatches.RowID#&frmSearchString=,../data/dataFrame.cfm?frmSiteName="
		keyColumnKeyList="search_String,search_String"

		HidePageControls="yes"
		useInclude = "false"

		sortOrder="">
<cfelse>
	<CF_tableFromQueryObject queryObject="#checkOrgMatches#"
		numRowsPerPage="200"

		useInclude = "false"
		sortOrder="">
</cfif>