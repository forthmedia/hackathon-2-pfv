<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
2007/10/02   WAB changes to add dataLoadTypeTable
WAB 2007-10-10   changed the temporary table matchname fields to be NVARChar
WAB 2007/11/07  mods so that  dataload can supply the orgids and/or the locationids
WAB 2008/06/02  mod to allow firstnames to be null (for say Japan)
WAB 2008/06/24	mods so that dataload file can supply the personid (which overrides any organisation or locationid given)
				however still some issues to deal with if a person has moved from the organisation
WAB 2008/09/22   problem with the created view when dealing with core columns which don't exist (such as address2) - I had been using eg. select null as address2, but this lead to zeros appearing in the data, have replaced with eg. select convert(varchar, null) as address2
WAB 2009/01     CR-TND 556 work to do with Trend Support Instance, re-importing Org/loc/people who have been deleted
				MAde view creation more robust.  Check for duplicate columns, check for failure
WAB ???		Added a try catch around the inserts to give an error message
WAB 2009/03/01  In the insert queries took out all the Max() code an instead did a subquery to bring back all the distinct rowids to be inserted
WAb 2009/03/02  Added primary key to rowid
SSS 2009/04/27  Added some extra functions to load and match geo data
WAB 2009/04/28   LID: 2112 Altered the country matching for alternative country names so does not update the underlying data
NYB 2009/04/28 - LID: 2112 in validateCountryData function, throughout query changed dl.country to dl.#countryColumnName# - so the process doesn't fall over when the country is mapped to a column called CountryCode
SSS 2009-05-07	Added The functions prepareGeodata and uploadgeodata in order to load geodata dataloads
NJH 2009/07/13	P-FNL069 - check that file has valid extensions. Give error message if it is not valid.
SSS 2009-11-17  P-lex039 - Added debug show no to a query so it would not bring down the site if debug was on
WAB/NAS 2010/02/15 LID3095
WAB 2010/04/07  LID 3190 Mods to prepareLocData and preparePerData to overcome bugs which occur when data has duplicate people at different locs or duplicate addresses for different orgs
WAB 2010/05/05   LID 3281    Mods to validateCountryData - went wrong if country was blank (as opposed to null)
PPB 2010/07/14  LID3652 optionally insert crmOrgID, crmLocID and crmPerID (for Panda)
WAB 2011/05/18  LID 6628.  OrganisationTypeID not being inserted correctly
2011-07-25 	NYB  LHID6768 - added limitToExistingTables argument to limit dataloads that have corresponding tables - to stop it falling over
WAB	2013-10-28	Renamed Functions getLocationMatchName() and getOrgMatchName() to getLocationMatchNameReplaceStrings() and getOrgMatchNameReplaceStrings()
				In preparation for functions with the same names which actually do return a matchname
2014-09-11	RPW	Dataloading End Customers is not setting the location account type
2014-11-13	NJH When adding people, disable the unique email trigger to keep existing functionality.
2015-02-12	DXC	Case 443675 - Mapping fails when column name starts with numeric. Added [] around alias...
2016/09/01	NJH	JIRA PROD2016-1190/125 - Removed old matching functionality with new matching functions. Also removed the assignedEntityIds code as the POL ids are now of type IDENTITY
--->

<cfcomponent displayname="RelayDataLoad" hint="Relay data load functions">

		<cfset THIS.datasource = application.sitedataSource>
		<cfset THIS.TableName = "">
		<cfset THIS.loadTablecOLUMN = "">



	 	<cffunction access="public" name="initTable" hint="initialises cfc with datasource and tablename">
			<cfargument name="datasource" type="string" default="#application.sitedataSource#">
			<cfargument name="tablename" type="string" required="true">

			<cfset this.datasource = datasource>
			<cfset this.tablename = tablename>

			<cfquery name="getDataLoad" datasource="#this.datasource#">
			select datasourceid from vdataloads where load_Table =  <cf_queryparam value="#THIS.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset this.dataLoadID = getDataLoad.dataSourceID>

			<cfset checkForAndCreateDataLoadView()>

		</cffunction>



<!--- +++++++++++++++++++++++++++++++++	+++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 	<cffunction access="public" name="newDataLoad" hint="Given an MDB file it will upload the file and begin the process of data transfer.">
		<cfargument name="filefield" type="string" required="true">
		<cfargument name="datasheetName" type="string" required="true">
		<cfargument name="dataloadDirectory" type="string" default="#application.paths.content#\dataloads\">
		<cfargument name="SQLServerMappedDrive" type="string" default="s:\#this.datasource#\">
		<cfargument name="SQLServerLocalDirectory" type="string" default="d:\relayDataLoads\#this.datasource#\">

		<cfset var dataFileName = "">
		<cfset var dataLoadTableName = "dataload#dataloadTable#">
		<cfset var insertTime = now()>
		<cfset var uniqueID = "#dateformat(insertTime,'ddmmyy')##timeformat(insertTime,'HHmmss')#">
		<cfset var dataSourceDetail = structNew()>
		<cfset var dataLoadServer = "#dataLoadTable#_#uniqueID#">

		<cfif not directoryExists(dataloadDirectory)>
			<cfdirectory action="create" directory="#dataloadDirectory#">
		</cfif>

		<cfif not directoryExists(SQLServerMappedDrive)>
			 <cfexecute name = "C:\WinNT\System32\cmd.exe"
			   arguments = "/C MD #SQLServerMappedDrive#"
			   timeout = "60"></cfexecute>

			<!--- <cfdirectory action="create" directory="#SQLServerMappedDrive#"> --->
		</cfif>

		<cfquery name="tableExists" datasource="#this.datasource#">
			select TABLE_NAME from INFORMATION_SCHEMA.TABLES
			where TABLE_NAME =  <cf_queryparam value="#dataLoadTableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif tableExists.recordCount gt 0>
			<cfset dataSourceDetail["message"] = "Dataload name: <b>#dataLoadTable#</b> already exists. Your dataload was renamed to: <b>#dataLoadTable#_#uniqueID#</b>">
			<cfset dataLoadTable = "#dataLoadTable#_#uniqueID#">
		</cfif>

		<cfset dataFileName = dataLoadTable & "_data.mdb">
		<cfset dataLoadTableName = "dataload#dataloadTable#">

		<cfif structKeyExists(form,fileField) and form[fileField] neq "">
			<!--- NJH 2009/07/13 P-FNL069
			<cffile action="upload" filefield="#fileField#" destination="#dataloadDirectory#" nameconflict="makeunique"> --->
			<cfset cffile=application.com.fileManager.uploadFile(fileField="#fileField#", destination="#dataloadDirectory#")>

			<cfif cffile.isOK>
			<cffile action="rename" source="#dataloadDirectory##cffile.serverFile#" destination="#dataloadDirectory##dataFileName#">

			<!--- SWJ: we have to compy the file to the SQL server box via a mapped drive SQLServerMappedDrive
			<cfexecute name = "C:\\WinNT\\System32\\copy.exe"
			   arguments = "-e"
			   timeout = "1"> --->

			  <cfexecute name = "C:\WinNT\System32\cmd.exe"
			   arguments = "/C COPY #dataloadDirectory##dataFileName# #SQLServerMappedDrive#"
			   timeout = "60"></cfexecute>

				<!--- <cfquery name="addDataSource" datasource="#this.datasource#">
				INSERT INTO [DataSource]([Description], [loadtable],
				[CreatedBy], [Created],
				[LastUpdatedBy], [LastUpdated],
				[DateReceived], <!--- [DateRequired], [DateLoaded], --->
				<!--- [CurrentStatus], --->
					[dataloadTypeID], [defaultCountryID])
				VALUES('#form.Description#', '#dataLoadTable#',
				#request.relayCurrentUser.usergroupid#, #insertTime#,
				#request.relayCurrentUser.usergroupid#, #insertTime#,
				#insertTime#,
				<!--- #DateRequired#, #DateLoaded#, #CurrentStatus#, --->
					'#form.dataloadTypeID#', #form.defaultCountryID#)

				select scope_identity() as dataSourceID
				</cfquery> --->

				<!--- NJh 2008/09/30 CR-LEN503 SPMDataload - converted query to a function --->
				<cfset datasourceStruct = insertDataSource(dataLoadTable=dataLoadTable,Description=form.Description,dataLoadTypeID=form.dataloadTypeID,insertTime=insertTime,defaultCountryID=form.defaultCountryID)>

<!--- 			<cfquery name="getDataSource" datasource="#this.datasource#">
				select dataSourceID from datasource
				where loadtable = '#dataLoadTable#' and created = #insertTime#
			</cfquery>
 --->
			<cfset dataSourceDetail["datasourceQRY"] = addDataSource>
			<cfset dataSourceDetail["datasourceID"] = addDataSource.dataSourceID>

			<!--- <cfset dataSourceDetail["message"] = createLoadTableFromMDB(datasheetName,dataLoadTable,dataLoadDirectory)> --->

			<!--- SWJ 2006-08-18 Nikki I have commented this out.  We have experienced problems using cfstoredProc so
			I have created a query below.  NB the query must run against the master db which is where the SP is
			The error in sql is "Could not locate registry entry for OLE DB provider 'OLE DB Provider for Jet'."
			<cfstoredproc procedure="sp_addlinkedserver" dataSource="#this.datasource#" returnCode="Yes" debug="Yes">
				<cfprocparam type="IN" cfsqltype="cf_sql_varchar" value="#dataLoadServer#MDB" dbVarName=@server>
				<cfprocparam type="IN" cfsqltype="cf_sql_varchar" value="Microsoft.Jet.OLEDB.4.0" dbVarName=@provider>
				<cfprocparam type="IN" cfsqltype="cf_sql_varchar" value="OLE DB Provider for Jet" dbVarName=@srvproduct>
				<cfprocparam type="IN" cfsqltype="cf_sql_varchar" value="#dataloadDirectory##dataFileName#" dbVarName=@datasrc>
			</cfstoredproc> --->

			<cfquery name = "addLinkedServer" datasource = "#this.datasource#">
				EXEC master.dbo.sp_addlinkedserver
				   @server =  <cf_queryparam value="#dataLoadServer#MDB" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				   @provider = 'Microsoft.Jet.OLEDB.4.0',
				   @srvproduct = 'OLE DB Provider for Jet',
				   @datasrc =  <cf_queryparam value="#SQLServerLocalDirectory##dataFileName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfquery name="getData" datasource="#this.datasource#">
				select *
				into #dataLoadTable#
				from #dataLoadServer#MDB...[#dataLoadTableName#]
			</cfquery>

			<!--- WAB 2007/09/06 create view onto dataloadtable --->
			<cfset this.TableName = dataLoadTable>
			<cfset updateDataLoadView()>

			<!--- file has an extension that is not allowed --->
			<cfelse>
				<cfset dataSourceDetail["message"] = cffile.message>
			</cfif>
		<cfelse>
			<cfset dataSourceDetail["message"] = "There was a problem with your file. Please try again.">
		</cfif>

		<cfreturn dataSourceDetail>

	</cffunction>
	<cffunction access="private" name="getAdminPassword" hint="Gets the administrator password.">
		<cfreturn "87sAtuRN26">
	</cffunction>
 	<cffunction access="public" name="createLoadTableFromMDB" hint="Given an MDB file it will create a new table with same or similar column names as the file.">
		<cfargument name="datasheetName" type="string" required="true">
		<cfargument name="dataLoadTable" type="string" required="true">
		<cfargument name="dataloadDirectory" type="string" default="#application.paths.content#\dataloads\">

		<cfset var dataFileName = dataLoadTable & "_data.mdb">
		<cfset var dataLoadTableName = "dataload#dataloadTable#">
		<cfset var finalColList = "">
		<cfset var adminObj = createObject("component","cfide.adminapi.administrator")>
		<cfset var setDSN = "">
		<cfset var datasourceName = "DS#dateformat(now(),'ddmmyy')##timeformat(now(),'HHmmss')#">

		<cfscript>
			adminObj.login(getAdminPassword());
			// Instantiate the data source object.
			setDSN = createObject("component","cfide.adminapi.datasource");
			// Create a DSN.
			setDSN.setMSAccess(
				name="#datasourceName#",
				databaseFile = "#dataloadDirectory##dataFileName#"
			);
		</cfscript>

		<cfquery datasource="#datasourceName#" name="dataFileData">
			select *
			from #datasheetName#
		</cfquery>

		<!--- replace all spaces and punctuation in column names --->
		<cfset checkedCols = checkColumns(dataFileData)>

		<cfloop list="#checkedCols.columnList#" index="colToCheck">
			<cfset finalColList = listAppend(finalColList,getColumnTypes(colToCheck,checkedCols))>
		</cfloop>

		<cfquery name="tableExists" datasource="#this.datasource#">
			select TABLE_NAME from INFORMATION_SCHEMA.TABLES
			where TABLE_NAME =  <cf_queryparam value="#dataLoadTableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif tableExists.recordCount gt 0>
			<!--- TABLE ALREADY EXISTS! DO SOMETHING! --->
			<cfquery name="dropLoadTable" datasource="#this.datasource#">
				if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES
							where TABLE_NAME =  <cf_queryparam value="#dataLoadTableName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				drop table [#dataLoadTableName#]
			</cfquery>
			<cfquery name="createLoadTable" datasource="#this.datasource#">
				create table [#dataLoadTableName#]
				(#finalColList#)
			</cfquery>
			<cfoutput>Table #dataLoadTableName# already exists. Please choose another name.</cfoutput>
			<!--- <cfreturn false> --->
		<cfelse>
			<cfquery name="createLoadTable" datasource="#this.datasource#">
				create table [#dataLoadTableName#]
				(#finalColList#)
			</cfquery>
			<cfoutput>table #dataLoadTableName# created.</cfoutput>
		</cfif>
		<cfoutput><br>Copying Data...</cfoutput>
		<cfreturn copyDataToLoadTable(checkedCols,dataLoadTableName)>
	</cffunction>
	<cffunction access="public" name="copyDataToLoadTable" hint="Given a query and table name, will copy the data from the query to the table specified.">
		<cfargument name="qryData" type="query" required="true">

		<cfset var sqlStatementBegin = "insert into [#this.tablename#]">
		<cfset var sqlStatement = "">
		<cfset var colList = "">
		<cfset var colValues = "">
		<cfset var columnTypes = structNew()>
		<!--- get column types --->
		<cfquery datasource="#this.datasource#" name="colTypes">
			select	column_name, data_type
			from	INFORMATION_SCHEMA.COLUMNS
			where	table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfdump var="#colTypes#">

		<cfloop query="colTypes">
			<cfset columnTypes[column_name] = data_type>
		</cfloop>

		<cfdump var="#columnTypes#">

		<cfloop query="qryData">
			<cfset sqlStatement = "">
			<cfset colList = "">
			<cfset colValues = "">

			<cfloop list="#columnList#" index="col">
				<cfif evaluate(col) neq "">
					<cfset colList = listAppend(colList,col)>
					<cfset thisColValue = replaceNoCase(evaluate(col),"'","&acute;","all")>
					<cfif columnTypes[col] contains "varchar" or columnTypes[col] eq "datetime">
						<cfset colValues = listAppend(colValues,"'#thisColValue#'")>
					<cfelse>
						<cfset colValues = listAppend(colValues,"#thisColValue#")>
					</cfif>
				</cfif>
			</cfloop>
			<cfset sqlStatement = "#sqlStatementBegin# (#colList#) values (#colValues#)">
			<cftry>
				<cfquery name="writeRow" datasource="#this.datasource#">
					#preserveSingleQuotes(sqlStatement)#
				</cfquery>
				<cfcatch>
					<cfset errorMessage = true>
				</cfcatch>
			</cftry>
		</cfloop>


		<cfif isDefined("errorMessage") and errorMessage eq true>
			<cfreturn "There were problems importing and some data may be missing.">
		</cfif>
	</cffunction>
	<cffunction access="public" name="createLoadTableFromXLS" hint="Given an xls file it will create a new table with same or similar column names as the file.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="datasheetName" type="string" required="true">
		<cfargument name="debugMode" type="boolean" default="no">
		<cfargument name="dataLoadTable" type="string" required="true">
		<cfargument name="dataloadDirectory" type="string" default="#application.paths.content#\dataloads\">

		<cfset var dataFileName = dataLoadTable & "_data.xls">
		<cfset var dataLoadTableName = "dataload#dataloadTable#">
		<cfset var finalColList = "">

		<cfquery datasource="#this.datasource#" name="dataFileData">
			select select TOP(1) *
			from [#datasheetName#$]
			in '#dataloadDirectory##dataFileName#' 'EXCEL 5.0;'
		</cfquery>

		<!--- replace all spaces and punctuation in column names --->
		<cfset checkedCols = checkColumns(dataFileData)>

		<cfloop list="#checkedCols.columnList#" index="colToCheck">
			<cfset finalColList = listAppend(finalColList,getColumnType(colToCheck,checkedCols))>
		</cfloop>

		<cfquery name="tableExists" datasource="#this.datasource#">
			select TABLE_NAME from INFORMATION_SCHEMA.TABLES
			where TABLE_NAME =  <cf_queryparam value="#dataLoadTableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif tableExists.recordCount gt 0>
			<!--- TABLE ALREADY EXISTS! DO SOMETHING! --->
			<cfquery name="dropLoadTable" datasource="#this.datasource#">
				if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES
							where TABLE_NAME =  <cf_queryparam value="#dataLoadTableName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				drop table [#dataLoadTableName#]
			</cfquery>
			<cfquery name="createLoadTable" datasource="#this.datasource#">
				create table [#dataLoadTableName#]
				(#newColumns#)
			</cfquery>
			<!--- <cfreturn false> --->
		<cfelse>
			<cfquery name="createLoadTable" datasource="#this.datasource#">
				create table [#dataLoadTableName#]
				(#newColumns#)
			</cfquery>
			<cfoutput>table #dataLoadTableName# created.</cfoutput>
		</cfif>




		<!---
		<cffile action="READ" file="#dataloadDirectory##formatFileName#" variable="thisFormatFile">

		<!--- work out the full number of lines in the incoming file this way so that we can check for duff rows--->
		<cfset fileLineCount = "0">
		<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
			<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
				<cfset fileLineCount = incrementValue(fileLineCount)>
			</cfif>
		</CFLOOP>

		<cfif debugMode eq "yes">
			<cfset currentLineNumber = "0">
			<cfoutput>
				fileLineCount = #fileLineCount#
				create table [#dataLoadTable#]
				(
				<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
					<cfset currentLineNumber = incrementValue(currentLineNumber)>
					<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
						[#rereplace(listfirst(line),"([[:space:]])|([[:punct:]])","","ALL")#] <cfswitch expression="#listlast(line)#"><cfcase value="varchar">[nvarchar]</cfcase><cfcase value="text">[text]</cfcase><cfcase value="date">[datetime]</cfcase><cfcase value="int">[int]</cfcase><cfdefaultcase>[nvarchar]</cfdefaultcase></cfswitch>  <cfif listlast(line) eq "varchar">(255) </cfif>NULL<cfif currentLineNumber lt fileLineCount>,</cfif><br>
					</cfif>
				</CFLOOP>
				)
			</cfoutput>
		<cfelse>

		<!--- ==============================================================================
          Main create functions
		=============================================================================== --->
			<cfset currentLineNumber = "0">
			<cfquery name="createLoadTable" datasource="#this.datasource#">
				if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES
							where TABLE_NAME = '#dataLoadTable#')
				drop table [#dataLoadTable#]
			</cfquery>

			<cfquery name="createLoadTable" datasource="#this.datasource#">
			create table [#dataLoadTable#]
				(
				<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
					<cfset currentLineNumber = incrementValue(currentLineNumber)>
					<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
						[#rereplace(listfirst(line),"([[:space:]])|([[:punct:]])","","ALL")#] <cfswitch expression=#rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL")#><cfcase value="varchar">[nvarchar]</cfcase><cfcase value="text">[text]</cfcase><cfcase value="date">[datetime]</cfcase><cfcase value="int">[int]</cfcase><cfdefaultcase>[nvarchar]</cfdefaultcase></cfswitch>  <cfif listlast(line) eq "varchar">(255) </cfif>NULL<cfif currentLineNumber lt fileLineCount>,</cfif>
					</cfif>
				</CFLOOP>
				)
			</cfquery>
			<cfreturn "Load table #dataLoadTable# created based on format specified.">
		</cfif>
		--->
	</cffunction>
	<cffunction name="getColumnTypes" hint="Given the columnList from the excel spreadsheet, this method will check each col name and replace any odd characters.">
		<cfargument name="columnToCheck" type="string" required="true">
		<cfargument name="dataQuery" type="query" required="true">

		<cfset var listMainColTypes = "">
		<cfset var colLength = 0>
		<cfset var colValue = "">
		<cfset var tmpColType = "">
		<cfset var mainColType = "">
		<cfset var listExactColTypes = "">
		<cfset var maxColLength = 0>
		<cfset var finalColType = "">
		<cfset var columnAndType = structNew()>

		<cfloop query="dataQuery">
			<!--- <cfset newColName = replace(columnToCheck,"##","###","all")> --->
				<!--- <cfset colValue = rereplace(evaluate(columnToCheck),"([[:punct:]])"," ","all")> --->
				<cfset colValue = evaluate(columnToCheck)>
				<cfif LSisDate(colValue)>

					<cfset mainColType = "date">
					<cfset colLength = 0>
					<cfset tmpColType = "datetime">
				<cfelseif isNumeric(colValue)>
					<!--- check numeric for double --->
					<cfset mainColType = "numeric">
					<cfif listLen(toString(colValue),".") gt 1>
						<cfset colLength = "18,0">
						<cfset tmpColType = "decimal">
					<cfelseif colValue eq 1 or colValue eq 0>
						<cfset colLength = 0>
						<cfset tmpColType = "boolean">
					<cfelseif len(colValue) lte 4>
						<cfset colLength = 0>
						<cfset tmpColType = "int">
					<cfelse>
						<cfset colLength = 0>
						<cfset tmpColType = "bigint">
					</cfif>
				<cfelseif colValue neq "">
					<cfset mainColType = "string">
					<cfif len(colValue) lte 255>
						<cfset colLength = 255>
						<cfset tmpColType = "varchar">
					<cfelseif len(colValue) lte 4000>
						<cfset colLength = len(colValue)>
						<cfset tmpColType = "nvarchar">
					<cfelse>
						<cfset colLength = len(colValue)>
						<cfset tmpColType = "text">
					</cfif>
				</cfif>

				<cfif not listContains(listMainColTypes,mainColType)>
					<cfset listMainColTypes = listAppend(listMainColTypes,mainColType)>
				</cfif>

				<cfif not listContains(listExactColTypes,tmpColType)>
					<cfset listExactColTypes = listAppend(listExactColTypes,tmpColType)>
				</cfif>

				<cfif colLength gt maxColLength>
					<cfset maxColLength = colLength>
				</cfif>
		</cfloop>

		<!--- use types colected to setup exact types --->
		<cfset finalColType = "[#columnToCheck#]">
		<cfif listLen(listMainColTypes) gt 1>
			<cfif maxColLength lte 4000>
				<cfset finalColType = "#finalColType# [nvarchar](#maxColLength#) NULL">
			<cfelse>
				<cfset finalColType = "#finalColType# [ntext] NULL">
			</cfif>
		<cfelse>
			<cfif listLen(listExactColTypes) eq 1>
				<cfset finalColType = "#finalColType# [#listExactColTypes#]">
				<cfif maxColLength gt 0>
					<cfset finalColType = "#finalColType#(#maxColLength#)">
				</cfif>
				<cfset finalColType = "#finalColType# NULL">
			<cfelse>
				<cfif lcase(listMainColTypes) eq "string">
					<cfif maxColLength lte 4000>
						<cfset finalColType = "#finalColType# [nvarchar](#maxColLength#) NULL">
					<cfelse>
						<cfset finalColType = "#finalColType# [ntext] NULL">
					</cfif>
				<cfelseif lcase(listMainColTypes) eq "numeric">
					<cfif listContainsNoCase(listExactColTypes,"decimal")>
						<cfset finalColType = "#finalColType# [decimal](18,0) NULL">
					<cfelseif listContainsNoCase(listExactColTypes,"bigint")>
						<cfset finalColType = "#finalColType# [bigint] NULL">
					<cfelse>
						<cfset finalColType = "#finalColType# [int] NULL">
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<!--- <cfoutput>
			FINAL COL TYPE: #finalColType#<br>
			column: #columnToCheck#<br>
			main types: #listMainColTypes#<br>
			exact types: #listExactColTypes#<br>
			max length: #maxColLength#<br><br>
		</cfoutput> --->
		<cfreturn finalColType>
	</cffunction>

	<cffunction name="checkColumns" hint="Given the query from the excel spreadsheet, this method will check each col name and replace any odd characters.">
		<cfargument name="data" type="query" required="true">

		<cfset var newColList = "">
		<cfset var newColName = "">
		<cfset var tableName = "nikkitmptable">
		<cfset var newColStruct = cleanColumns(data.columnList)>
		<cfset var incr = 1>

		<cfquery name="newData" datasource="#this.datasource#">
			<CFLOOP QUERY="data">
				<cfif currentrow is not 1>union</cfif>
				select
				<cfset incr = 1>
				<CFLOOP list="#data.columnList#" index="col">
					'#data[col][currentRow]#' as [#newColStruct[col]#]<cfif incr lt listLen(data.columnList)>,</cfif>
					<cfset incr = incr + 1>
				</CFLOOP>
			</cfloop>
		</cfquery>

		<cfreturn newData>
	</cffunction>
	<cffunction name="cleanColumns" hint="Given the columnList from the excel spreadsheet, this method will check each col name and replace any odd characters.">
		<cfargument name="columns" type="string" required="true">

		<cfset var returnColumns = "">
		<cfset var tmpColNames = "">
		<cfset var newColName = "">
		<cfset var nameExists = false>
		<cfset var nameIncr = 1>
		<cfset var returnStruct = structNew()>

		<cfloop index="col" list="#columns#">
			<cfset newColName = '#trim(rereplace(col,"([[:space:]])|([[:punct:]])","","ALL"))#'>

			<!--- <cfoutput>#col# - #newColName# : [#listFindNoCase(returnColumns,newColName)#]</cfoutput> --->
			<cfif newColName eq "">
				<cfset newColName = "col">
			</cfif>
			<!--- if the new column name exists already, append something --->
			<cfscript>
				do {
					if (listFindNoCase(tmpColNames,newColName) eq 0) {
						nameExists = false;
					} else {
						nameExists = true;
						newColName = "#newColName#_#nameIncr#";
						nameIncr = nameIncr + 1;
					}
				}
				while (nameExists);
			</cfscript>
			<cfset tmpColNames = newColName>
			<!--- <cfoutput> :: #newColName#<br></cfoutput> --->
			<cfset returnStruct[col] = newColName>
			<cfset returnColumns = listAppend(returnColumns,newColName)>
		</cfloop>

		<cfreturn returnStruct>
	</cffunction>
	<cffunction access="public" name="createLoadTable" hint="Given a format file based on the load table name it will create a new table with the format files specification.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="debugMode" type="boolean" default="no">
		<cfargument name="dataLoadTable" type="string" required="true">
		<cfargument name="dataloadDirectory" type="string" default="#application.paths.content#\dataloads\">

		<cfset formatFileName = dataLoadTable & "_format.csv">

		<cffile action="READ" file="#dataloadDirectory##formatFileName#" variable="thisFormatFile">

		<!--- work out the full number of lines in the incoming file this way so that we can check for duff rows--->
		<cfset fileLineCount = "0">
		<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
			<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
				<cfset fileLineCount = incrementValue(fileLineCount)>
			</cfif>
		</CFLOOP>

		<cfif debugMode eq "yes">
			<cfset currentLineNumber = "0">
			<cfoutput>
				fileLineCount = #fileLineCount#
				create table [#dataLoadTable#]
				(
				<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
					<cfset currentLineNumber = incrementValue(currentLineNumber)>
					<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
						[#rereplace(listfirst(line),"([[:space:]])|([[:punct:]])","","ALL")#] <cfswitch expression="#listlast(line)#"><cfcase value="varchar">[nvarchar]</cfcase><cfcase value="text">[text]</cfcase><cfcase value="date">[datetime]</cfcase><cfcase value="int">[int]</cfcase><cfdefaultcase>[nvarchar]</cfdefaultcase></cfswitch>  <cfif listlast(line) eq "varchar">(255) </cfif>NULL<cfif currentLineNumber lt fileLineCount>,</cfif><br>
					</cfif>
				</CFLOOP>
				)
			</cfoutput>
		<cfelse>

		<!--- ==============================================================================
          Main create functions
		=============================================================================== --->
			<cfset currentLineNumber = "0">
			<cfquery name="createLoadTable" datasource="#this.datasource#">
				if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES
							where TABLE_NAME =  <cf_queryparam value="#dataLoadTable#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				drop table [#dataLoadTable#]
			</cfquery>

			<cfquery name="createLoadTable" datasource="#this.datasource#">
			create table [#dataLoadTable#]
				(
				<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
					<cfset currentLineNumber = incrementValue(currentLineNumber)>
					<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
						[#rereplace(listfirst(line),"([[:space:]])|([[:punct:]])","","ALL")#] <cfswitch expression=#rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL")#><cfcase value="varchar">[nvarchar]</cfcase><cfcase value="text">[text]</cfcase><cfcase value="date">[datetime]</cfcase><cfcase value="int">[int]</cfcase><cfdefaultcase>[nvarchar]</cfdefaultcase></cfswitch>  <cfif listlast(line) eq "varchar">(255) </cfif>NULL<cfif currentLineNumber lt fileLineCount>,</cfif>
					</cfif>
				</CFLOOP>
				)
			</cfquery>
			<cfreturn "Load table #dataLoadTable# created based on format specified.">
		</cfif>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertDataIntoLoadTable" hint="This function inserts the data into the load table.">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="dataLoadTable" type="string" required="true">
<!--- 		<cfset formatFileName = dataLoadTable & "_format.csv">
		<cfset dataFileName = dataLoadTable & "_data.csv"> --->

		<cfargument name="columnDelimiter" type="string" default=",">
		<cfargument name="debugMode" type="boolean" default="no">
		<cfargument name="dataloadDirectory" type="string" default="#application.paths.content#\dataloads\">
		<!--- ==============================================================================
		       Set up some parameters
		=============================================================================== --->
		<cfset formatFileName = dataLoadTable & "_format.csv">
		<cfset dataFileName = dataLoadTable & "_data.csv">

		<cffile action="READ" file="#dataloadDirectory##formatFileName#" variable="thisFormatFile">
		<cffile action="READ" file="#dataloadDirectory##dataFileName#" variable="thisDataFile">

		<!--- ==============================================================================
		       work out the full number of lines in the incoming file
		this way so that we can check for duff rows
		=============================================================================== --->
		<cfset fileLineCount = "0">
		<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
			<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
				<cfset fileLineCount = incrementValue(fileLineCount)>
			</cfif>
		</CFLOOP>

		<!--- ==============================================================================
		       Show this if debug is on
		=============================================================================== --->
		<cfif debugMode eq "yes">
			<cfset currentLineNumber = "0">
			<cfoutput>
			fileLineCount = #fileLineCount#
				create table [#dataLoadTable#]
				(
				<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
					<cfset currentLineNumber = incrementValue(currentLineNumber)>
					<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
						[#rereplace(listfirst(line),"([[:space:]])|([[:punct:]])","","ALL")#] <cfswitch expression="#listlast(line)#"><cfcase value="varchar">[nvarchar]</cfcase><cfcase value="date">[datetime]</cfcase><cfcase value="int">[int]</cfcase><cfdefaultcase>[nvarchar]</cfdefaultcase></cfswitch>  <cfif listlast(line) eq "varchar">(255) </cfif>NULL<cfif currentLineNumber lt fileLineCount>,</cfif><br>
						<cfif currentlineNumber eq "1"><cfset currentLineNumber = incrementValue(currentLineNumber)></cfif>
					</cfif>
				</CFLOOP>
				)
			</cfoutput>
		</cfif>

		<!--- ==============================================================================
		       create the column list and columnDataTypeList
		=============================================================================== --->
		<cfset currentLineNumber = "0">
		<cfset columnList = "">
		<cfset columnDataTypeList = "">
		<CFLOOP index="line" list="#thisFormatFile#" delimiters="#Chr(13)#">
			<cfset currentLineNumber = incrementValue(currentLineNumber)>
			<cfif rereplace(listlast(line),"([[:space:]])|([[:punct:]])","","ALL") neq "">
				<cfset columnList = columnList & '[' & rereplace(listfirst(line),"([[:space:]])|([[:punct:]])","","ALL")& ']' & iif(currentLineNumber lt fileLineCount,DE(","),DE(""))>
				<cfset columnDataTypeList = columnDataTypeList & listLast(line) & iif(currentLineNumber lt fileLineCount,DE(","),DE(""))>
			</cfif>
		</CFLOOP>
		<cfset numberOfColumns = listlen(columnList)>

		<!--- ==============================================================================
		       Show this if debug is on
		=============================================================================== --->
		<cfif debugMode eq "yes">
			<cfoutput><br><br>columnList = #columnList# <br>
			columnDataTypeList = #columnDataTypeList#<br><br>
			</cfoutput>
		</cfif>

		<!--- ==============================================================================
		       Show this if debug is on
		=============================================================================== --->
		<cfif debugMode eq "yes">
			<cfset currentLineNumber = "0">
			<cfoutput>
			format file Line Count (should equal number of columns in load table) = #fileLineCount#
			<CFLOOP index="row" list="#thisDataFile#" delimiters="#Chr(13)#">
				<br><br>INSERT INTO [#dataLoadTable#]
								(#columnList#)
							VALUES (<cfset currentColNumber = "0">
				<CFLOOP index="column" list="#row#" delimiters="#columnDelimiter#">
					<cfset currentColNumber = incrementValue(currentColNumber)>
					<!--- #listGetAt(columnDataTypeList,currentColNumber)# --->
					<cfswitch expression="#trim(listGetAt(columnDataTypeList,currentColNumber))#">
						<cfcase value="varchar">'#trim(column)#'</cfcase>
						<cfcase value="text">'#trim(column)#'</cfcase>
						<cfcase value="date">
							<cfif isDate(trim(column))>'#dateformat(trim(column),"dd-mmm-yyyy")#'<cfelse>'#trim(column)#'</cfif></cfcase>
						<cfcase value="int">#trim(column)#</cfcase>
						<cfdefaultcase>'#trim(column)#'</cfdefaultcase>
					</cfswitch>
					<cfif currentColNumber lt numberOfColumns>,</cfif>
				</CFLOOP>
				)	</CFLOOP>
			</cfoutput>
		</cfif>


		<!--- ==============================================================================
		      Do the actual insert
		=============================================================================== --->
		<cfif debugMode eq "no">
			<cfset currentRowNumber = "0">
			<cfquery name="insertDataIntoLoadTable" datasource="#this.datasource#">
				<CFLOOP index="row" list="#thisDataFile#" delimiters="#Chr(13)#">
					<!--- increment the current row number --->
					<cfset currentRowNumber = incrementValue(currentRowNumber)>

					<cfif rereplace(listlast(row),"([[:space:]])|([[:punct:]])","","ALL") neq "" and currentRowNumber gt 1>
						INSERT INTO [#dataLoadTable#]
							(#columnList#)
						VALUES (<cfset currentColNumber = "0">
						<CFLOOP index="column" list="#row#" delimiters="#columnDelimiter#">
							<!--- increment the current row --->
							<cfset currentColNumber = incrementValue(currentColNumber)>
							<!--- #listGetAt(columnDataTypeList,currentColNumber)# --->
							<cfswitch expression="#trim(listGetAt(columnDataTypeList,currentColNumber))#">
								<cfcase value="varchar">'#trim(column)#'</cfcase>
								<cfcase value="date">
									<cfif isDate(trim(column))>'#dateformat(trim(column),"dd-mmm-yyyy")#'<cfelse>'#trim(column)#'</cfif></cfcase>
								<cfcase value="int"><cfif trim(column) eq "">NULL<cfelse>#trim(column)#</cfif></cfcase>
								<cfdefaultcase>'#trim(column)#'</cfdefaultcase>
							</cfswitch>
							<cfif currentColNumber lt numberOfColumns>,</cfif>
						</CFLOOP>)
					</cfif>
				</CFLOOP>
			</cfquery>
		</cfif>
		<cfreturn "Data inserted into #dataLoadTable#.">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Add control columns
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction access="public" name="addControlFields" hint="Adds special columns to this.tableName">

		<cfquery name="getLoadType" datasource="#this.datasource#">
			select dlt.dataloadTypeID, loadTypeGroup
			from dataSource ds inner join dataLoadType dlt on ds.dataloadTypeID = dlt.dataloadTypeID
			WHERE loadTable =  <cf_queryparam value="#this.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfquery name="getCols" datasource="#this.datasource#">
			select column_name
				from information_SCHEMA.columns
					where table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfquery name="getMissingCols" datasource="#this.datasource#">
			select dataloadColumnName, columndataType, column_Name
			from (select column_name
							from information_SCHEMA.columns
								where table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > ) c
			RIGHT OUTER JOIN (select dataloadColumnName, columndataType
								from dataloadcol where
									columndatatype is not null
									and isNull(controlCOlumn,0) = 1  -- WAB added this, don't want to add every single possible column
								) dlc
			ON ltrim(rtrim(dlc.dataloadColumnName)) = ltrim(rtrim(c.column_name))
			<!---
 			WAB 2008/10/29 this join allows us to exclude any control columns which are in the dataload table but with different names and have been mapped
			 --->
			left join vdataloadmappingdetail dlm on dlm.loadtable =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >  and dlc.dataloadColumnName = dlm.mappedto

			WHERE
			c.column_name IS NULL
				and
			dlm.loadtable is null


<!--- 			<cfif getLoadType.recordCount eq 1 and len(getLoadType.loadType)>
				AND c.loadType = '#getLoadType.loadType#'
			</cfif> --->
		</cfquery>

		<cfinvoke method="getDataLoadDetails" returnvariable="DataLoadDetails">
		</cfinvoke>

		<cfset columnList = valueList(getCols.column_name)>

		<!--- check the columns because if we find any of these below
		it means the control columns are already added so we should not do it again--->
		<cfquery name="alterTable" datasource="#this.datasource#">
			declare @tableName nvarchar(50)
			select @tableName =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			declare @sqlcmd nvarchar(4000)
			select @sqlcmd	= ''
			<cfif listFindNoCase(columnList,"RowID") eq 0>
				+ ' ALTER TABLE #this.tablename# ADD [RowID] [int] IDENTITY (1, 1) NOT NULL primary key '  <!--- WAb 2009/03/02 added primary key to make it easier when developing and debugging --->
			</cfif>
			<cfloop query="getMissingCols">
				<!--- Add any rows that are missing from columnList --->
				+ ' ALTER TABLE #this.tablename# ADD [#getMissingCols.dataloadColumnName#] #getMissingCols.columnDataType# NULL'
			</cfloop>

			exec sp_executesql @sqlcmd
		</cfquery>

		<!--- NJH 2016/09/01 JIRA PROD2016-1190 - add the required fields for the matching --->
		<cfset application.com.matching.createMatchFieldColumns(tablename=this.tablename,entityType="organisation")>
		<cfset application.com.matching.createMatchFieldColumns(tablename=this.tablename,entityType="location")>
		<cfset application.com.matching.createMatchFieldColumns(tablename=this.tablename,entityType="person")>
		
		<cfset updateMatchingControlFields(entityType="organisation")>
		<cfset updateMatchingControlFields(entityType="location")>
		<cfset updateMatchingControlFields(entityType="person")>

		<cfset updateDataLoadView()>

		<cfset returnMessage = getMissingCols>
		<!--- <cfset returnMessage = "Control columns added"> --->
		<!--- <cfelse>
			<cfset returnMessage = "Control columns already in place">
		</cfif> --->
		<cfreturn returnMessage>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             RESIZE COLS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="resizeCols" hint="This process resizes the columns in a table">

			<!---
			WAB 2007/09/06
				don't change size of control columns
			 --->
			<cfquery name="getControlCols" datasource="#this.datasource#">
				select dataloadColumnName
				from
					dataloadcol
				where
					isNull(controlColumn,0) = 1
			</cfquery>

		<cfquery name="getCols" datasource="#this.datasource#">
			select column_name, data_Type, character_maximum_length
				from information_SCHEMA.columns
					where table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and character_maximum_length > 150
					and (data_Type = 'nvarchar' or data_Type = 'varchar' or data_Type = 'char')
					and column_name not in ('badRow','PersonMatchName','OrgMatchName','LocationMatchName')
					and column_name  not in ( <cf_queryparam value="#getControlCols.dataloadColumnName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		</cfquery>
		<cfloop query="getCols">
			<cfquery name="getColLen" datasource="#this.datasource#">
				select max(isnull(len([#column_name#]),1)) as colLen from #this.tablename#
			</cfquery>
			<cfset newLen = getColLen.colLen+5>
			<cfquery name="alterTable" datasource="#this.datasource#">
				alter table #this.tablename# alter column [#column_name#] nvarchar(#newLen#)
			</cfquery>
		</cfloop>
		<cfreturn "Columns resized">
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             REMOVES Spaces form column names
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="cleanColNames" hint="This process removes illegal characters from column names">

		<cfquery name="getCols" datasource="#this.datasource#">
			select column_name, data_Type, character_maximum_length
				from information_SCHEMA.columns
					where table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfset errorText = "">
		<cfloop query="getCols">
			<!--- the regex ([[:space:]])|([[:punct:]])|([[:digit:]]) matches all chars not a-z
			these are put into an array in the variable st --->
			<cfset st = REFindNoCase("([[:space:]])|([[:punct:]])",column_Name,1,"True")>
			<cfif ArrayLen(st.pos) gt 1>
				<cfset newColName = REreplacenocase(column_Name,"([[:space:]])|([[:punct:]])","","ALL")>
				<cftry>
					<cfquery name="renameCol" datasource="#this.datasource#">
						EXEC sp_rename '#this.tablename#.[#column_name#]', '#newColName#', 'COLUMN'
					</cfquery>
					<cfcatch type="Database">
						<cfset errorText = listappend(errorText,getCols.column_name)>
					</cfcatch>
				</cftry>
			</cfif>
			<cfset st = "">
		</cfloop>
		<cfif errorText eq "">
			<cfreturn "Columns renamed">
		<cfelse>
			<cfreturn "Columns renamed excluding #errorText#">
		</cfif>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames that don't match the relayLoad cols
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="mismatchedCols" hint="Returns columnNames that don't match the relayLoad cols">
		<cfquery name="getMismatchedCols" datasource="#this.datasource#">
<!--- 			select rc.dataLoadColumnName, lt.column_name
				from [dataLoadCol] rc right outer join
				(select column_name from information_SCHEMA.columns
						where table_name = '#this.tablename#') lt
				on lt.column_name = rc.dataLoadColumnName
				where rc.dataLoadColumnName is null --->
				<!--- altered WAB 2006-11-21 to take account of flag mappings --->
		select LoadTableColumnName as column_name, mappedTo
		from vDataLoadMappingAll
		where
		load_table =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and mappingType_ = 'unmapped'
 		order by LoadTableColumnName

		 </cfquery>
		<cfreturn getMismatchedCols>
	</cffunction>

	<!--- WAB new function to remember mappings to replace renaming the columns of the dataload --->
	<cffunction access="public" name="mapColumn" hint="maps a dataload column">
		<cfargument name="loadTableColumn" type="string" required="true">
		<cfargument name="MappedColumn" type="string" required="true">
		<cfargument name="automapped" default="0">

		<cfset addDataLoadMapping (
			loadTableColumn = loadTableColumn ,
			mappingType = "POL",
			loaddata = '',
			mappedTo = mappedColumn,
			automapped = automapped
		)>

		<!--- <cfquery name="addDataLoadMapping" datasource="#THIS.dataSource#">
			if not exists (select * from vDataloadMappingDetail
				where loadTable = '#THIS.tableName#'
				and loadTableColumn = '#loadTableColumn#')
				BEGIN
					INSERT INTO [DataloadMapping](dataloadID, [loadTableColumn], [mappedto],mappingtype,automapped)
					select datasourceid, '#loadTableColumn#', '#MappedColumn#', 'POL',#automapped#
					from vDataloads
					where load_Table = '#THIS.tableName#'
				END
			ELSE
				BEGIN

					update vdataloadMapping
					set mappedTo = '#MappedColumn#', automapped = #automapped#
					where
						[loadTable] = '#THIS.tableName#'
						and  [loadTableColumn] = '#loadTableColumn#'

				END
		</cfquery> --->

	</cffunction>


	<cffunction access="public" name="mappedColumns" hint="Returns mapped POL Columns">
		<cfquery name="getMappedCols" datasource="#this.datasource#">
		select LoadTableColumnName, mappedTo, relaycolumn, automapped, 'Remove' as remove
		from vDataLoadMappingAll
		where
		load_table =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and (mappingType_ = 'POL' or mappingType_ = 'POL-Default')
 		order by mappingType_

		</cfquery>
		<cfreturn getMappedCols>
	</cffunction>
	<cffunction access="public" name="checkForAndCreateDataLoadView" hint="Check for dataloadview and create if doesn't exist">

				<cfquery name="doesViewExist" datasource="#this.datasource#">
				select 1 from sysobjects where name =  <cf_queryparam value="v#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfif doesViewExist.recordcount is 0>
					<cfset updateDataLoadView()>
				</cfif>
	</cffunction>
	<cffunction access="public" name="updateDataLoadView" hint="Creates/Update a view with all the dataload columns mapped to relay columns">

			<cfset var additionalColumnsNeeded = "">
			<cfset loadPOLColumnMappingStructure()>

			<cfquery name="getDataLoadColumns" datasource="#this.datasource#">
				select column_name from
					information_SCHEMA.columns lt
								where table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset DataLoadDetails = getDataloadDetails ()>

			<!--- WAB 2008/10/29 add the default countryid to the view--->
			<cfif DataLoadDetails.defaultCountryID neq "">
				<cfset defaultCountryID = DataLoadDetails.defaultCountryID>
			<cfelse>
				<cfset defaultCountryID = "9">
			</cfif>

			<!--- these are columns which may be required in the queries but aren't in the dataload table, make dummy null columns --->
			<cfset additionalColumnsNeeded = getAdditonalLoadCols()>

			<cfquery name="doesViewExist" datasource="#this.datasource#">
				select 1 from sysobjects where name =  <cf_queryparam value="v#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset columnsDone = "">
			<cfset duplicateColumns = "">
			<!--- 2009/01/22 WAB problems with views not being created if there are column alias duplications
				should try and deal with at source, but for expediencies sake try and pick up here
				also
				sql does not seem to throw an error back to CF when view creation fails,
				so I will copy the existing view, try creating a new one, see if it is there and rollback if necessary
			--->
				<cfif doesViewExist.recordcount is not 0>
					<cfquery name="renameExistingView"  datasource="#this.datasource#">
					exec sp_rename  'v#this.tablename#' ,  @newname =  <cf_queryparam value="v#this.tablename#_tmp" CFSQLTYPE="CF_SQL_VARCHAR" > , @objtype = 'object'
					</cfquery>
				</cfif>


			<cfquery name="createView"  datasource="#this.datasource#">
		create
				View
				dbo.v#this.tablename#
				as
				select
				<cfloop query = "getDataLoadColumns">
					<cfset thisColAlias = column_name>
					<cfif structKeyExists (this.polDataloadToRelayColumnMappings,column_name)>
						<cfset thisColAlias =  this.polDataloadToRelayColumnMappings[column_name].dataloadcolumnname>
					</cfif>

					<cfif not listFindNoCase(columnsDone,thisColAlias)>
						<cfset columnsDone = listAppend(columnsDone,thisColAlias)>
						<cfif column_name is "countryID">
						isNull(countryID,#defaultCountryID#) as countryid
						<cfelse>
						[#column_name#]
						as [#thisColAlias#] <!--- DXC Case 443675 --->
						</cfif>
					,
					<cfelse>
						<cfset duplicateColumns = listAppend(duplicateColumns,thisColAlias)>
					</cfif>
				</cfloop>

				<cfloop query = "additionalColumnsNeeded">
					<cfif not listFindNoCase(columnsDone,dataloadcolumnname)>
						<cfset columnsDone = listAppend(columnsDone,dataloadcolumnname)>
						convert(varchar,null)  <!--- WAB 2008/09/22 added the odd convert(varchar ) to this. We were getting a problem when doing loads of these  'dummy' columns because max(isnull(thisfield),'')) [used to insert into the POL tables] was bringing back 0 rather than '' - of course sql doesn't know whether my dummy null is a number or not, so put in the convert which seems to give it the hint, I could have just done  ''  but using null just seemed better --->
						as #dataloadcolumnname#
						,
					<cfelse>
						<cfset duplicateColumns = listAppend(duplicateColumns,dataloadcolumnname)>
					</cfif>
				</cfloop>
				1 as dummy
				from #this.tablename#
</cfquery>

		<cfquery name="testForView"  datasource="#this.datasource#">
			<!---  check view created OK --->
		IF exists (select 1 from sysobjects where xtype = 'v' and name =  <cf_queryparam value="v#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			BEGIN
				<cfif doesViewExist.recordcount is not 0>
					drop view dbo.v#this.tablename#_tmp
				</cfif>
				select 1 as isOK
			END
		ELSE
			BEGIN
				<cfif doesViewExist.recordcount is not 0>
				exec sp_rename  'v#this.tablename#_tmp' ,  @newname =  <cf_queryparam value="v#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > , @objtype = 'object'
				</cfif>
				select 0 as isOK
			END
		</cfquery>

		<!--- Nasty Messages in middle of function but currently only way!--->
		<cfif duplicateColumns is not "">
			<cfoutput><FONT color="RED">WARNING: The following columns have duplicated mappings: <BR>#replace(duplicateColumns,",","<BR>","ALL")#<BR></FONT></cfoutput>
		</cfif>
		<cfif not testForView.isOK>
			<cfoutput><FONT color="RED">ERROR: Dataload View Has Not Been Updated Correctly<BR></FONT></cfoutput>
		</cfif>
		<cfreturn testForView.isOK>

	</cffunction>

	<cffunction access="public" name="ignoreColumn" hint="ignores dataload column">
		<cfargument name="loadTableColumn" type="string" required="true">

		<cfset addDataLoadMapping (
			loadTableColumn = loadTableColumn ,
			mappingType = "IGNORE",
			mappedTo = "",
			loaddata = ""

		)>
	</cffunction>
	<cffunction access="public" name="ignoredColumns" hint="Returns ignored Columns">
		<cfquery name="getIgnoredCols" datasource="#this.datasource#">
		select LoadTableColumnName, 'remove' as remove
		from vDataLoadMappingAll
		where
		load_table =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and (mappingType_ = 'IGNORE' )
		</cfquery>
		<cfreturn getIgnoredCols>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames that don't match the relayLoad cols
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getAdditonalLoadCols" hint="Returns relayLoadcolumnNames that aren't in the table being loaded" returns="query">
		<cfset var getAdditonalLoadColsQry = "">

		<cfquery name="getAdditonalLoadColsQry" datasource="#this.datasource#">
				select rc.dataLoadColumnName, rc.relayColumn
				from
					vDataLoads dl
					inner join
					[dataLoadCol] rc on dl.loadTypeGroup = rc.loadTypeGroup
							left outer join
					information_SCHEMA.columns lt
								on lt.column_name = rc.dataLoadColumnName and table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
							left outer join
					vdataloadmappingdetail dlm
							on mappingtype = 'pol' and dlm.mappedto = rc.dataLoadColumnName and loadtable =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			where
				dl.load_Table =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and lt.column_name is null
				and dlm.mappedto is null
				and loadThisColumn = 1
				order by rc.dataLoadColumnName
		</cfquery>
		<cfreturn getAdditonalLoadColsQry>
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Trims all of the columns in the data table
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="trimData" hint="Trims data and removes leading and trailing spaces.">
		<cfquery name="getCols" datasource="#this.datasource#">
			select column_name, data_Type, character_maximum_length
				from information_SCHEMA.columns
					where table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and data_type like '%char'
		</cfquery>
		<cfquery name="trimData" datasource="#this.datasource#">
			Update #this.tablename#
				Set
				<cfloop query="getCols">
					#column_name# = Rtrim(Ltrim(#column_name#))<cfif getCols.recordCount neq currentRow>,</cfif>
				</cfloop>
		</cfquery>

		<cfreturn "Leading and trailing spaces removed from these columns: <BR>#valuelist(getCols.column_name,'<BR>')#">  <!--- 2006-11-29 WAB added BRs - looked better on screen --->
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Trims all of the columns in the data table
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="deleteNULLData" hint="This will delete any rows where the organisation name, person name and email fieldsare ALL blank.">

		<cfquery name="deleteNULLData" datasource="#this.datasource#">
			delete from V#this.tablename#
				where CompanyName is null
				and firstname is null
				and lastname is null
				and email is null
		</cfquery>
		<cfreturn "Blank records deleted.">
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames that will be loaded
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLoadColNames" hint="Returns columnNames that will be loaded">
		<cfargument name="dataSource" type="string" required="true">
		<cfquery name="getLoadColNames" datasource="#this.datasource#">
			select dataLoadColumnName from [dataLoadCol]
			where loadThisColumn = 1 order by dataLoadColumnName
		</cfquery>
		<cfreturn getLoadColNames>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="validateCountryData"
		hint="Prepares the location data for loading">

		<cfinvoke method="getDataLoadDetails" returnvariable="DataLoadDetails">

		</cfinvoke>

		<cfif DataLoadDetails.defaultCountryID neq "">
			<cfset defaultCountryID = DataLoadDetails.defaultCountryID>
		<cfelse>
			<cfset defaultCountryID = "9">
		</cfif>

		<cfset loadPOLColumnMappingStructure()>


		<!--- Wab 2008/09/17 had to replace v#this.tablename# with #this.tablename# - wouldn't do an update on a view with derived fields
		not sure of knock on effects

		--->

		<!---
		WAB 2008/10/29
		If there is no country column then we should just use the countryid column (which is either supplied or added as a control column)
		--->
		<cfif structKeyExists(this.polColumnMappings,"COUNTRY")>

			<CFSET countryColumnName = this.polColumnMappings.Country.LoadTableColumnName>

			<!--- NYB 2009/04/28 - throughout query - changed dl.country to dl.#countryColumnName# --->
			<cfquery name="validateCountryData" datasource="#this.datasource#">
			alter table #this.tablename# alter column #countryColumnName# varchar(160) null

			<!---
			WAB 2009/04/28 replaced, don't update the original data, go straight to updating the countryid column
			<!--- this updates the country column to the description that we use in Relay --->
			UPDATE dl
			SET dl.country = ca.relayCountryDescription
			FROM #this.tablename# dl INNER JOIN countryAlternatives ca
				ON dl.country = ca.alternativeCountryDescription
			WHERE dl.country IS NOT NULL
			---->

			<!--- this updates the countryid column matching the country with the countryAlternatives --->
			UPDATE dl
			SET dl.countryid = c.countryid
			FROM
				#this.tablename# dl
					INNER JOIN
				countryAlternatives ca	ON dl.#countryColumnName# = ca.alternativeCountryDescription
					inner join
				country c on c.countrydescription = ca.relayCountryDescription
			<!--- WAB 2010/05/05 LID3281 changed from 'is not Null' to deal with country column being either null or blank --->
			WHERE isNull(dl.#countryColumnName#,'') <> ''

			UPDATE dl
			SET dl.countryID = c.countryID
			FROM #this.tablename# dl INNER JOIN Country c
				ON ( dl.#countryColumnName# = c.countrydescription and c.isocode is not null)   <!--- WAB 2008/02/04 added is not null, otherwise might match on a countryGroup with the same name --->
					or (dl.#countryColumnName# = c.isocode and c.isocode is not null)
					or (dl.#countryColumnName# = convert(varchar,c.countryid) and c.isocode is not null)   <!--- WAB 2007-10-22 can handle countryids in country column --->
			WHERE dl.CountryID IS NULL
			<!--- WAB 2010/05/05 dealt with country column being either null or blank --->
			and isNull(dl.#countryColumnName#,'') <> ''

			Update v#this.tablename#
			Set Badrow = badrow + 'NULL or Unknown Country'
			where
				CountryID IS NULL
				and badrow is null
				AND isNull(COUNTRY,'') <> ''   <!--- WAB 2008/10/29 surely only a bad row if there was a value in the country column, otherwise will just get the default country--->   <!--- WAB 2009/01 removed a spurious cp. alias  --->


		   <!--- WAB 2008/10/29 LID3281 now done in view so that underlying data isn't updated
			-- Update all countries to a default value if no country field in incoming data
			Update #this.tablename#
			Set countryID = #defaultCountryID#
			where Country IS NULL and CountryID IS NULL
			--->
			</cfquery>

			<cfquery name="getBadCountries" datasource="#this.datasource#">
			-- List countries needing manual attention
			SELECT distinct cp.Country
			FROM v#this.tablename# cp
			WHERE cp.CountryID IS NULL AND isNull(cp.COUNTRY,'') <> ''
			</cfquery>
			<cfif getBadCountries.recordCount eq 0>
				<cfset message = "Country data prepared succesfully.  Null countryIDs set to #defaultCountryID#.">
			<cfelse>
				<cfset message = "Problem with data. The following countries were not matched:" & valuelist(getBadCountries.country)>
			</cfif>

		<cfelse>
			<cfquery name="getNullCountries" datasource="#this.datasource#">
			SELECT count(*) as theCount
			FROM v#this.tablename#
			WHERE CountryID IS NULL
			</cfquery>

			<cfif getNullCountries.theCount gt 0>
				<cfset message = "#getNullCountries.theCount# records will use the Default CountryID (#defaultCountryID#).">
			<cfelse>
				<cfset message = "Country data prepared succesfully.">
			</cfif>

		</cfif>

		<cfreturn message>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function checkPerData
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="viewCountryData" returnType="query"
		hint="Returns a query object containing country data compared with relay countrys.">

		<cfquery name="viewCountryData" datasource="#this.datasource#">
			select distinct dl.country as dataload_country,
				c.CountryDescription as relay_country, count(*) as Total
				FROM v#this.tablename# dl
					LEFT OUTER JOIN country c ON dl.countryID = c.countryID
				Group BY dl.country, c.CountryDescription
				order by dl.country
		</cfquery>
		<cfreturn viewCountryData>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function createOrgMatchname
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createOrgMatchname" hint="Creates the org match name">
		<!--- This function is a bastardised version of relay.com.dbtools.updateOrgMatchName - BEWARE! --->
		<cfargument name="updateTargetCountryOrgMatchNames" type="boolean" default="true">

		<!--- NJH 2016/09/01 JIRA PROD2016-1190 - removed old matching code and replace with calls to new
		<cfset var replaceValueStatement = application.com.dbtools.getOrgMatchNameReplaceStrings(this.dataSource).replaceValueStatement>
		<cfset var replaceStatement = application.com.dbtools.getOrgMatchNameReplaceStrings(this.dataSource).replaceStatement> --->


			<cfset loadPOLColumnMappingStructure(requiredColumns="CompanyName")>
			<cfset dlCompanyNameColumn = this.POLcolumnMappings.CompanyName.LoadTableColumnName>
			<cfset vatNumberRequired = getVatNumberRequired().isrequired>
			<cfif vatNumberRequired>
				<cfif not structKeyExists (this.POLcolumnMappings,"VATNumber")>
					<cfoutput>No VAT Number Field Set Up - required for #vatNumberRequired.countryList#</cfoutput><CF_ABORT>
				<cfelse>
					<cfset dlVATNumberColumn = this.POLcolumnMappings.VATNumber.LoadTableColumnName>
				</cfif>
			</cfif>

<!--- 		<cfquery name="updateDataloadMatchnameToRemoveReservedWords" datasource="#this.datasource#" timeout="3000">
			UPDATE #this.tablename#
			SET orgMatchName = substring(RTRIM(#replaceStatement#REPLACE(N' ' + REPLACE(#dlCompanyNameColumn#  <cfif vatNumberRequired > + CASE WHEN VATrequired = 1 THEN   '#application.delim1#' + isnull(cast(#dlVATNumberColumn# as nvarchar(50)),' ') ELSE '' END </cfif>  ,'.','') + ' ','','')#replace(replaceValueStatement,"**","'","ALL")#),1,50)
			FROM #this.tableName# inner join country on #this.tableName#.countryID = country.countryID
			WHERE isnull(#dlCompanyNameColumn# ,'1J5£c REPLACE ME 1981') + ' ' <> substring(RTRIM(#replaceStatement#REPLACE(N' ' + REPLACE(CASE WHEN VATrequired = 1 THEN #dlCompanyNameColumn#  + '#application.delim1#' + isnull(cast(VATnumber as nvarchar(50)),' ') ELSE #dlCompanyNameColumn# END,'.','') + ' ','','')#replace(replaceValueStatement,"**","'","ALL")#),1,50)
		</cfquery>

		<cfquery name="updateThoseWithNoSpecialsInDataload" datasource="#this.datasource#">
			UPDATE #this.tablename#
			SET orgMatchName = #dlCompanyNameColumn#
			WHERE
				orgMatchName is null
		</cfquery> --->

		<!--- NJH 2016/09/01 - create a view that contains the fields that we expect. The dataload table contains orgMatchName which is not a colum
			we are expecting (as well as orgID) and so we replace those with matchname and organisationID --->
		<cfset var matchingView = createEntityMatchingDataLoadView(entityType="organisation",tablename="v#tableName#")>
		<cfset application.com.matching.updateMatchFields(tablename = matchingView,entityType="organisation")>
		<cfset dropEntityMatchingDataLoadView(tablename="v#tableName#")>
		<cfset var whereClause="">

		<!--- update organisation table for those countries being imported --->
		<cfif updateTargetCountryOrgMatchNames>
			<!--- NJH 2016/08/18 JIRA PROD2016-1190
			No need to target orgs in particular countries, especially if country is not part of matching rules. Matches should be fairly quick to update these days anyways, as they are updated constantly.--->

			<cfif listFindNocase(application.com.matching.getRequiredMatchFields(entityType="organisation"),"countryID")>
				<cfquery name ="getDataloadCountryIDs" datasource="#this.datasource#">
				select distinct countryID from v#this.tablename#
				</cfquery>

			 <!---<cfif getDataloadCountryIDs.recordcount gt 0>
				<cfset countryIDlist = "">
				<cfloop query="getDataloadCountryIDs">
					<cfset countryIDlist = listappend(countryIDlist,getDataloadCountryIDs.countryID,",")>
				</cfloop>
				<cfscript>
					x = application.com.dbtools.updateOrgMatchname(countryIDlist = "#countryIDlist#");
				</cfscript> --->

			<cfset whereClause= getDataloadCountryIDs.recordCount?"countryID in (#valueList(getDataloadCountryIDs.countryID)#)":"">
			</cfif>

			<!--- update matchnames in organisation table --->
			<cfset application.com.matching.updateMatchFields(tablename = "organisation",entityType="organisation",whereClause=whereClause)>
			<cfreturn "Organization match names updated in load table and Relay Organisation table">
		<cfelse>
			<cfreturn "Organization match names updated in load table only">
		</cfif>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Prepare the org data for loading
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="prepareOrgData"
		hint="Prepares the organisation data for loading">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="matchMethodID" type="numeric" default=1> <!--- NJH 2006-12-06 introducing various match methods --->

		<cfquery name="checkCountryNulls" datasource="#this.datasource#">
			select * from v#tablename#
				where countryid is null
		</cfquery>

		<cfif checkCountryNulls.recordcount gt 0 >
			<cfreturn "Please check the country data as there are some unmatched countries.">
		<cfelse>


			<cfquery name="runOrgMatchRules" datasource="#this.datasource#">
				<!--- start matching rules --->
	 				-- Set Action for organisations where we have been given the orgid (and it is valid) - doesn't handle ones which might have been deduped
					<!--- WAB 2007/11/07 --->
					<!--- note that the locationid one has to be done first --->
					UPDATE v#tableName# SET OrgAction = 'PersonID given in Source File', LocationAction = 'PersonID given in Source File',  PersonAction = 'PersonID given in Source File', orgid = p.organisationid, Locationid  = p.Locationid
						FROM v#tableName# dl INNER JOIN Person p
						ON dl.Personid = p.Personid
						WHERE OrgAction IS NULL
						and (dl.orgid is Null or dl.orgid = p.organisationid)
						and (dl.locationid is Null or dl.locationid = p.locationid)

					UPDATE v#tableName# SET OrgAction = 'LocationID given in Source File', LocationAction = 'LocationID given in Source File', orgid = l.organisationid
						FROM v#tableName# dl INNER JOIN location l
						ON dl.locationid = l.LocationID
						WHERE OrgAction IS NULL
						and (dl.orgid is Null or dl.orgid = l.organisationid)


					UPDATE v#tableName# SET OrgAction = 'OrganisationID given in Source File'
						FROM v#tableName# dl INNER JOIN Organisation o
						ON dl.orgid = o.OrganisationID and dl.orgid is NOT Null
						WHERE OrgAction IS NULL



			</cfquery>


			<!--- NJH 2016/09/01 remove below code with new matching code

			<cfswitch expression="#arguments.matchMethodID#">
				<cfcase value="1">

					<cfquery name="checkOrgMatchNameNulls" datasource="#this.datasource#">
						select top 1 * from v#tablename#
							where orgMatchName is null
							and [companyName] Is not Null
					</cfquery>
		 			<cfif checkOrgMatchNameNulls.recordcount gt 0 >
						<!--- if there are null OrgMatchName then first fill them --->
						<cfinvoke method="createOrgMatchname" returnvariable="qDataSourceHistory">
							<cfinvokeargument name="dataSource" value="#this.datasource#"/>
							<cfinvokeargument name="tableName" value="v#tableName#"/>
						</cfinvoke>
					</cfif>

					<!--- NJH 2007/10/02 added an additional org matchname based on the url if the request.orgMatchNameMethodID is 2 - no longer want match on exact name in this case (set in relayINI.cfm). --->
					<cfif not structKeyExists(request,"orgMatchNameMethodID")>
						<cfset request.orgMatchNameMethodID = 1>
					</cfif>

					<cfquery name="runOrgMatchRules" datasource="#this.datasource#">
						<!--- start matching rules --->
							<cfif request.orgMatchNameMethodID neq 2>
				 				-- Set OrgIDs for exact match on Company Names and CountryID first
								UPDATE v#tableName# SET OrgAction = 'Organisations matched on company name and country', OrgID = o.OrganisationID
									FROM v#tableName# dl INNER JOIN Organisation o
									ON ltrim(rtrim(dl.[companyName])) = ltrim(rtrim(o.OrganisationName))
									<!--- <cfif (isDefined("request .country ScopeOrganisationRecords") and request .country ScopeOrganisationRecords eq 1)> --->
									<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
										and dl.countryID = o.countryID
									</cfif>
									WHERE OrgAction IS NULL
							</cfif>

			 				-- Now try and match the match name and set OrgIDs for exact match on MatchNames and countryID Next
							UPDATE v#tableName# SET OrgAction = 'Organisations matched on MatchName and country', OrgID = o.OrganisationID
								FROM v#tableName# dl INNER JOIN Organisation o
								ON ltrim(rtrim(dl.OrgMatchName)) = ltrim(rtrim(o.MatchName))
								<!--- <cfif (isDefined("request .country ScopeOrganisationRecords") and request .country ScopeOrganisationRecords eq 1)> --->
								<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
									and dl.countryID = o.countryID
								</cfif>
								WHERE OrgAction IS NULL
						<!--- end matching rules --->
					</cfquery>
				</cfcase>

				<!--- match Org on Remote Organisation ID --->
				<cfcase value="2">
					<cfquery name="runOrgMatchRules" datasource="#this.datasource#">
						-- Create Org Match Name based on R1OrgID
						UPDATE v#tableName#
						SET OrgMatchName = R1OrgID

						<!--- start matching rules --->
			 				-- Set OrgIDs for exact match on R1OrgID (RemoteOrgID)
							UPDATE v#tableName# SET OrgAction = 'Organisations matched on remote organisation ID(R1OrgID)', OrgID = o.OrganisationID
								FROM v#tableName# dl INNER JOIN Organisation o
								ON dl.[R1OrgID] = o.R1OrgID
								WHERE OrgAction IS NULL

							-- ID Bad Orgs
							UPDATE v#tableName#
								SET BadRow = isnull(BadRow,' ') + 'Null R1OrgID', orgAction = 'Null R1OrgID'
								WHERE R1OrgID is NULL
								and orgAction is NULL
						<!--- end matching rules --->
					</cfquery>
				</cfcase>
			</cfswitch> --->

			<cfinvoke method="createOrgMatchname" returnvariable="qDataSourceHistory">
				<cfinvokeargument name="dataSource" value="#this.datasource#"/>
				<cfinvokeargument name="tableName" value="#tableName#"/>
			</cfinvoke>

			<!--- create an org view so that we can use expected matchname fields --->
			<cfset var matchingView = createEntityMatchingDataLoadView(entityType="organisation",tablename="v#tableName#")>
			<!--- match incoming records with those in organisation table --->
			<cfset application.com.matching.matchRecords(tablename=matchingView,entityType="organisation",uniqueIDColumnName="rowId",allowDuplicateMatchedRecords=true)>
			<cfset dropEntityMatchingDataLoadView(tablename="v#tableName#")>

			<cfquery name="updateOrgAction">
				update v#tableName# set orgAction = 'Organisations matched' where orgAction is null and orgID is not null
			</cfquery>

			<cfquery name="prepareOrgData" datasource="#this.datasource#">
				-- Find any null company names
				UPDATE v#tableName#
				SET BadRow = N'Null company Name',
					orgAction = N'Null company Name',
					locationAction = N'Null company Name',
					personAction = N'Null company Name'
					WHERE ([companyName] Is Null or len(ltrim(rtrim(companyName))) = 0)
					AND OrgAction is null

				-- Set the rest for Inserting
				UPDATE v#tableName# Set OrgAction = 'Organisations to be inserted'
					WHERE OrgAction IS NULL

				IF OBJECT_ID('tempdb..##Org') IS NOT NULL DROP TABLE ##Org

				--Temp table to hold org data
				CREATE TABLE ##Org (
					RowID int IDENTITY)
			</cfquery>

			<cfset application.com.matching.createMatchFieldColumns(tablename="##Org",entityType="organisation")>
			<cfset var orgMatchFields = application.com.matching.getEntityMatchFields(entityType="organisation").standard>
			<!--- if matchname is part of the matching, then substitute this with orgMatchName as that exists in the view --->
			<cfif listFindNoCase(orgMatchFields,"matchname")>
				<cfset orgMatchFields = listAppend(listDeleteAt(orgMatchFields,listFindNoCase(orgMatchFields,"matchname")),"orgMatchName")>

				<cfquery name="prepareOrgData">
					exec tempdb..sp_rename 'dbo.##org.matchname','orgMatchname','column'
				</cfquery>

			</cfif>
			<cfset var firstRun = true>
			<cfset var orgMatchField = "">

			<cfquery name="prepareOrgData">
				-- Insert Distinct Rows into temp table based on match fields
				INSERT INTO ##Org (#orgMatchFields#)
					SELECT DISTINCT #orgMatchFields#
					FROM v#tableName#
					WHERE OrgAction = 'Organisations to be inserted'

				declare @organisationSeed int
				declare @rowsToBeInsertedCount int
				declare @reseedValue int

				select @organisationSeed = IDENT_CURRENT('organisation')
				select @rowsToBeInsertedCount = count(1) from ##Org
				set @reseedValue = @organisationSeed+@rowsToBeInsertedCount

				DBCC CHECKIDENT ('organisation',RESEED,@reseedValue)

				UPDATE ##Org SET organisationID = RowID + @organisationSeed

				-- Insert new orgid in to base table
				UPDATE cp
					SET cp.orgID = o.OrganisationID
					FROM
					v#tableName# cp INNER JOIN ##Org o
					ON
						<cfloop list="#orgMatchFields#" index="orgMatchField"><cfif not firstRun> AND </cfif>cp.#orgMatchField# = o.#orgMatchField#<cfset firstRun = false></cfloop>
					WHERE OrgAction = 'Organisations to be inserted'

				-- Clean up temp table
				DROP TABLE ##Org

				UPDATE v#tableName#
					SET OrganisationID = orgID

				<!--- -- Temp table to hold org data
				CREATE TABLE ##Org (
					RowID int IDENTITY,
					company NVarchar(2000),    <!--- WAB 2007-10-09 added Nvarchar, this code failed (silently) on companies with non ascii characters --->
					OrgID int)

				-- Insert Distinct Rows into temp table based on match name + countryID
				INSERT INTO ##Org (company)
					SELECT DISTINCT OrgMatchName + CONVERT(varchar (20),countryid)
					FROM v#tableName#
					WHERE OrgAction = 'Organisations to be inserted'
					AND ([OrgMatchName] Is not Null
						or len(ltrim(rtrim(OrgMatchName))) > 0)

				-- Get a seed for the new orgids
				-- NM - 20-10-06 - adds call to new table AssignedEntityIDs to get IDs for reserve.
				-- Backup of old method (DECLARE @Seed_ORGID int SELECT @Seed_ORGID = max(OrganisationID) FROM Organisation)
				DECLARE @EntityTypeID int SELECT @EntityTypeID = entityTypeID from schemaTable where entityName = 'organisation'
				DECLARE @Seed_ORGID int SELECT @Seed_ORGID = (newSeedValue + entityCount + 1) FROM AssignedEntityIDs WHERE newSeedValue IN (SELECT MAX(newseedvalue) FROM assignedEntityIDs WHERE EntityTypeID = @EntityTypeID) AND (EntityTypeID = @EntityTypeID)
				DECLARE @DataSourceID int SELECT @DataSourceID = datasourceID from datasource where loadtable =  <cf_queryparam value="v#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				DECLARE @NewEntityAmount int SELECT @NewEntityAmount = count(RowID) from ##Org


				-- Insert this org's assigned ids into AssignedEntityIDs table
				INSERT INTO AssignedEntityIDs (dataloadID,entityCount,EntityTypeID,newSeedValue) VALUES (@DataSourceID,@NewEntityAmount,@EntityTypeID,@Seed_ORGID)

				-- Insert new org id into temp table
				UPDATE ##Org SET OrgID = RowID + @Seed_ORGID

				-- Insert new orgid in to base table
				UPDATE cp
					SET cp.OrgID = o.OrgID
					FROM
					v#tableName# cp INNER JOIN ##Org o
					ON cp.OrgMatchName + CONVERT(varchar (20),countryid) = o.company
					WHERE OrgAction = 'Organisations to be inserted'

				-- Clean up temp table
				DROP TABLE ##Org --->
			</cfquery>
			<cfquery name="checkMissingOrgIDsForInserts" datasource="#this.datasource#">
				select orgID
				from v#tableName#
				where orgaction = 'I' and orgid is null
			</cfquery>
			<!--- 2006/0623 P_SNY039 GCC Crude mechanism to remind dataloaders of VATnumber importance--->
			<cfquery name="checkVATnumberWarning" datasource="#this.datasource#">
				select countryDescription
				from country
				where countryID in (select distinct countryID from v#tableName#)
				and VATrequired = 1
			</cfquery>

			<cfif checkMissingOrgIDsForInserts.recordCount eq 0>
				<cfif checkVATnumberWarning.recordCount gt 0>
					<cfset countryList = valuelist(checkVATnumberWarning.countryDescription)>
					<cfreturn "WARNING: VATnumber is an important field for #countryList#. Please ensure it is supplied without preceeding country codes. Organisation has been matched to data already in Relay and records marked as below.">
				<cfelse>
					<cfreturn "Organisation has been matched to data already in Relay and records marked as below.">
				</cfif>
			<cfelse>
				<cfreturn "There are organisation records with null ids marked for inserting. This is a problem.  Please contact your database adminitrator.">
			</cfif>

		</cfif>

	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Prepare the location data for loading
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="prepareLocData" hint="Prepares the location data for loading">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<!--- 2006/06/14 - GCC - temp code for SOD proof of concept --->
		<cfargument name="ignoreCity" type="string" default="false">
		<cfargument name="matchMethodID" type="numeric" default=1> <!--- NJH 2006-12-06 introducing various match methods --->

		<cfset var returnMessageAdditional = "">

		<cfquery name="checkCountryIDNulls" datasource="#this.datasource#">
			select countryID from v#tablename#
				where countryid is null
		</cfquery>

		<cfquery name="checkMissingOrgIDsForInserts" datasource="#this.datasource#">
			select orgid from v#tablename#
				WHERE orgID is null and orgAction = 'I'
		</cfquery>
		<cfif checkMissingOrgIDsForInserts.recordcount gt 0>
			<cfreturn "There are organisation records with null ids marked for inserting. This is a problem.  Please contact your database adminitrator.">
			<cfexit method="EXITTEMPLATE">
		</cfif>


		<cfif checkCountryIDNulls.recordcount eq 0 >
		<!--- <cftransaction> --->
			<cfif fileexists("#application.paths.code#\cftemplates\dataloadLocationMatchRules.cfm")>
				<!--- NJH 2008/04/19 P-TND065 1.1.2
				Trend needed a client specific matching rule, based on navisionID. We couldn't extend the relayDataLoadV2 object
				because it isn't called from memory in the dataload process. Therefore, we had to go with this solution.
				It is a hacky way of including client specific location match rules
				especially ugly if we decide that in this instance we are happy with the defualt match rules in the cfm... --->
				<cfinclude template="/code/cftemplates/dataloadLocationMatchRules.cfm">
				<cfif not isdefined("runClientMatchMethod")>
					<cfreturn "No location runClientMatchMethod has been specified in /code/cftemplates/dataloadLocationMatchRules.cfm">
					<cfexit method="EXITTEMPLATE">
				</cfif>
			</cfif>

			<cfparam name="runClientMatchMethod" default="false">

			<cfif not runClientMatchMethod>
				<!--- START:  NYB 2009-05-12 Sophos -  --->
				<cfset var whereClause= "">
				<!--- NJH 2016/09/01 only filter by country if countryID is a field used for matching --->
				<cfif listFindNocase(application.com.matching.getRequiredMatchFields(entityType="location"),"countryID")>
					<cfquery name="getCountryIDs" datasource="#this.datasource#">
						select distinct(countryID) as countryID from v#tablename#
					</cfquery>

					<cfif getCountryIDs.recordCount>
						<cfset whereClause= "countryID in (#valueList(getCountryIDs.countryID)#)">
					</cfif>
				</cfif>
				<cfset application.com.matching.updateMatchFields(tablename = "location",entityType="location",whereClause=whereClause)>

				<!--- <cfset updateExistingLocs = application.com.dbTools.updateLocationMatchname(countryIDlist=ValueList(getCountryIDs.countryID))> --->
				<!--- START:  NYB 2009-05-12 Sophos - added to work when address1,address4 & postalcode are different names in the dataload table --->

				<!--- NJH 2016/08/23 JIRA PROD2016-1190 - removed below for standard matching
				<cfset POLColumnMappings = getPOLColumnMappings(POLTable="location")>
				<cfset LocationMatchnameEntries = application.com.dbTools.getLocationMatchnameEntries(stringDelimiter=",")>
				<cfset dataloadColumnNames = "">
				<cfloop list="#LocationMatchnameEntries#" index="i">
					<!---
					WAB/NAS 2010/02/15 LID3095
					Had to change  loadTableColumnName to dataloadColumnName
					This is the column name used in the dataload view, and it is the view which is used for the update
					 --->
					<cfquery name="getColumnNames" dbtype="query">
						select dataloadColumnName from POLColumnMappings where lower(relayColumn)='location.#lcase(i)#'
					</cfquery>
					<cfset dataloadColumnNames = ListAppend(dataloadColumnNames, getColumnNames.dataloadColumnName,"#application.delim1#")>
				</cfloop> --->
				<!--- END:  NYB 2009-05-12 Sophos - added to work when address1,address4 & postalcode are different names in the dataload table --->

				<!--- NJH 2016/08/23 JIRA PROD2016-1190 --->
				<cfset var matchingView = createEntityMatchingDataLoadView(entityType="location",tablename="v#tableName#")>
				<cfset application.com.matching.updateMatchFields(tablename = matchingView,entityType="location",whereClause=whereClause)>
				<cfset application.com.matching.matchRecords(tablename=matchingView,entityType="location",uniqueIDColumnName="rowId",allowDuplicateMatchedRecords=true)>
				<cfset dropEntityMatchingDataLoadView(tablename="v#tableName#")>

				<cfset var updateLocationAction = "">
				<cfquery name="updateLocationAction">
					update v#tableName# set locationAction = 'Locations matched' where locationAction is null and locationID is not null
				</cfquery>

				<!--- <cfset updateExistingLocs = application.com.dbTools.updateLocationMatchname(LocationMatchnameEntries=dataloadColumnNames,tableName="v#tableName#",matchNameColumnName="LocationMatchName",countryIDlist=ValueList(getCountryIDs.countryID))> --->
				<!--- END:  2009-05-12 Sophos - --->
			<!--- <cfswitch expression="#arguments.matchMethodID#">
				<cfcase value="1">
						<cfquery name="runLocMatchRules" datasource="#this.datasource#">
							<!--- start matching rules --->
							<!--- START:  NYB 2009-05-12 Sophos - removed - replaced with above: ---
							-- Create locMatch Name based on orgmatchname, countryID and postalCode
							UPDATE v#tableName#
							SET LocationMatchName = OrgMatchName + convert(varchar(10),countryID) + convert(varchar(10),orgID) + cast(isnull(REPLACE(address1,' ',''),'') as nvarchar(255)) + isnull(REPLACE(postalcode,' ',''),'')
							WHERE (companyName Is not Null or len(ltrim(rtrim(companyName))) > 0)
							and (orgMatchName is not null or len(ltrim(rtrim(orgMatchName))) > 0)
							!--- END:  2009-05-12 Sophos - --->
							-- ID UPDATES
							UPDATE v#tableName#
							SET LocationAction = 'Locations matched on matchname, organisationID and country', --city, country, orgid and postal code
							LocationID = l.Locationid
							FROM v#tableName# dl INNER JOIN Location l
							ON dl.orgID = l.organisationID
								<cfif not ignoreCity>
									--AND (dl.[city] = l.address3 or dl.[city] = l.address4 or dl.[city] = l.address5)
									--2007/10/24 GCC Nulls on both sides caused mismatches - this should resolve
									AND (
											isnull(REPLACE(dl.[city],' ',''),'') = isnull(REPLACE(l.address3,' ',''),'') or
											isnull(REPLACE(dl.[city],' ',''),'') = isnull(REPLACE(l.address4,' ',''),'') or
											isnull(REPLACE(dl.[city],' ',''),'') = isnull(REPLACE(l.address5,' ',''),'')
										)
								</cfif>
								And	dl.countryid = l.countryid
								and isnull(REPLACE(dl.postalcode,' ',''),'') = isnull(REPLACE(l.postalcode,' ',''),'')
							WHERE
								(companyName Is not Null or len(ltrim(rtrim(companyName))) > 0)
								and dl.LocationAction IS NULL   <!--- WAB 2007-11-7 when added matching to locationids supplied in dataload --->
							<!--- end matching rules --->
						</cfquery>
					</cfcase>

					<!--- Match on R1LocID --->
					<cfcase value="2">
						<cfquery name="runLocMatchRules" datasource="#this.datasource#">
							<!--- start matching rules --->
							-- Create locMatch Name based on R1LocID
							UPDATE v#tableName#
							SET LocationMatchName = R1LocID

							-- ID UPDATES
							UPDATE v#tableName#
							SET LocationAction = 'Locations matched on remote location ID(R1LocID)',
							locationID = l.LocationID
							FROM v#tableName# dl INNER JOIN Location l
							ON dl.R1LocID = l.R1LocID
							WHERE dl.R1LocID is not NULL
							and dl.LocationAction IS NULL

							-- ID Bad Locs
							UPDATE v#tableName#
							SET BadRow = isnull(BadRow,' ') + 'Null R1LocID', locationAction = 'Null R1LocID'
							WHERE R1LocID is NULL
							and locationAction is NULL
							<!--- end matching rules --->
						</cfquery>
					</cfcase>
				</cfswitch> --->
			</cfif>

			<!--- WAB 2010/04/07 LID 3190 made modifications to match on orgid as well as location match name
					problem occurs if there are multiple organisations with the same address
					[for some inexplicable reason the location match name is just the address and does not include the sitename]
					added orgID to the ##loc table

			--->
			<cfquery name="prepareLocData" datasource="#this.datasource#">
				<!--- WAB 2007/11/07 --->
				UPDATE v#tableName#
				Set LocationAction = 'LocationID Given'
				from v#tableName# dl inner join location l on dl.locationid = l.locationid
				WHERE LocationAction IS NULL


				-- ID Bad Locs
				UPDATE v#tableName# SET BadRow = N'Null company Name', orgAction = N'Null company Name',
				locationAction = N'Null company Name', personAction = N'Null company Name'
					WHERE ([companyName] Is Null or len(ltrim(rtrim(companyName))) = 0)
					and LocationAction IS NULL    <!--- WAB 2007/11/07 --->

				-- ID INSERTS
				UPDATE v#tableName#
				Set LocationAction = 'Locations for inserting where organisations are new'
				WHERE LocationAction IS NULL
					AND (companyName Is not Null or len(ltrim(rtrim(companyName))) > 0)
					AND OrgAction = 'Organisations to be inserted'

				UPDATE v#tableName#
				Set LocationAction = 'Locations for inserting where organisations exist'
				WHERE LocationAction IS NULL
					AND (companyName Is not Null or len(ltrim(rtrim(companyName))) > 0)
				--AND OrgAction in ('Organisations matched on company name and country','Organisations matched on MatchName and country','Organisations matched on remote organisation ID(R1OrgID)','OrganisationID given in Source File','LocationID given in Source File')
				AND orgAction = 'Organisations matched'

				IF OBJECT_ID('tempdb..##Loc') is not null
					drop table ##Loc

				-- Temp table to hold loc data
				CREATE TABLE ##Loc (
				RowID int IDENTITY
				)
		</cfquery>

		<cfset application.com.matching.createMatchFieldColumns(tablename="##Loc",entityType="location")>
		<cfset var locMatchFields = application.com.matching.getEntityMatchFields(entityType="location").standard>
		<cfif listFindNoCase(locMatchFields,"matchname")>
			<cfset locMatchFields = listAppend(listDeleteAt(locMatchFields,listFindNoCase(locMatchFields,"matchname")),"locationMatchName")>

			<cfquery name="prepareLocData">
				exec tempdb..sp_rename 'dbo.##loc.matchname','locationMatchname','column'
			</cfquery>
		</cfif>
		<cfset var firstRun = true>
		<cfset var locMatchField = "">

		<cfquery name="prepareLocData">
				-- Insert Distinct Rows into temp table based on match fields
				INSERT INTO ##Loc (#locMatchFields#)
					SELECT DISTINCT #locMatchFields#
					FROM v#tableName#
					WHERE LocationAction like 'Locations for inserting%'

				declare @locationSeed int
				declare @rowsToBeInsertedCount int
				declare @reseedValue int

				select @locationSeed = IDENT_CURRENT('location')
				select @rowsToBeInsertedCount = count(1) from ##Loc
				set @reseedValue = @locationSeed+@rowsToBeInsertedCount

				DBCC CHECKIDENT ('location',RESEED,@reseedValue)

				UPDATE ##Loc SET locationID = RowID + @locationSeed

				-- Insert new locationID in to base table
				UPDATE cp
					SET cp.locationID = l.locationID
					FROM
					v#tableName# cp INNER JOIN ##Loc l
					ON
						<cfloop list="#locMatchFields#" index="locMatchField"><cfif not firstRun> AND </cfif>isNull(cp.#locMatchField#,'') = isNull(l.#locMatchField#,'')<cfset firstRun = false></cfloop>
					WHERE locationAction like 'Locations for inserting%'

				-- Clean up temp table
				DROP TABLE ##Loc

				<!--- -- Insert Distinct Rows into temp table based on match name
				INSERT INTO ##Loc (LocationMatchName, orgid)
				SELECT DISTINCT LocationMatchName, orgid
				FROM v#tableName#
				WHERE LocationAction in ('Locations for inserting where organisations are new','Locations for inserting where organisations exist')
				and (LocationMatchName is not null and len(ltrim(rtrim(LocationMatchName))) > 0)

				-- Get a seed for the new locids
				-- NM - 20-10-06 - adds call to new table AssignedEntityIDs to get IDs for reserve.
				-- Backup of old method (DECLARE @Seed_LOCID int SELECT @Seed_LOCID = max(LocationID) FROM Location)
				DECLARE @EntityTypeID int SELECT @EntityTypeID = entityTypeID from schemaTable where entityName = 'location'
				DECLARE @Seed_LOCID int SELECT @Seed_LOCID = (newSeedValue + entityCount + 1) FROM AssignedEntityIDs WHERE newSeedValue IN (SELECT MAX(newseedvalue) FROM assignedEntityIDs WHERE EntityTypeID = @EntityTypeID) AND (EntityTypeID = @EntityTypeID)
				DECLARE @DataSourceID int SELECT @DataSourceID = datasourceID from datasource where loadtable =  <cf_queryparam value="v#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				DECLARE @NewEntityAmount int SELECT @NewEntityAmount = count(RowID) from ##Loc

				-- Insert this loc's assigned ids into AssignedEntityIDs table
				INSERT INTO AssignedEntityIDs (dataloadID,entityCount,EntityTypeID,newSeedValue) VALUES (@DataSourceID,@NewEntityAmount,@EntityTypeID,@Seed_LOCID)

				-- Insert new loc id into temp table
				UPDATE ##Loc
				SET LocID = RowID + @Seed_LOCID

				-- Insert new locid in to base table
				UPDATE dl
				SET dl.LocationID = l.LocID
				FROM v#tableName# dl INNER JOIN ##Loc l
				ON dl.LocationMatchName = l.LocationMatchName and dl.orgid = l.orgid
				WHERE LocationID IS NULL
					AND companyName Is not Null
				-- Clean up temp table
				DROP TABLE ##Loc --->
			</cfquery>

			<!--- </cftransaction> --->
			<cfreturn "Location data prepared#returnMessageAdditional#">
		<cfelse>
			<cfreturn "Location data cannot be prepared with null countryIDs">
		</cfif>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Prepare the person data for loading
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="preparePerData"
		hint="Prepares the person data for loading">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="dataSource" type="string" required="true" default="#application.siteDataSource#">
		<cfargument name="matchMethodID" type="numeric" default=1> <!--- NJH 2006-12-06 introducing various match methods --->

		<cfquery name="checkCountryIDNulls" datasource="#this.datasource#">
			select * from v#tablename#
				where countryid is null
					--or orgID is null
					--or locationID is null
		</cfquery>

		<cfif checkCountryIDNulls.recordcount eq 0 >

			<cfset var matchingView = createEntityMatchingDataLoadView(entityType="person",tablename="v#tableName#")>
			<cfset application.com.matching.updateMatchFields(tablename = matchingView,entityType="person")>
			<cfset application.com.matching.updateMatchFields(tablename = "person",entityType="person")>
			<cfset application.com.matching.matchRecords(tablename=matchingView,entityType="person",uniqueIDColumnName="rowId",allowDuplicateMatchedRecords=true)>
			<cfset dropEntityMatchingDataLoadView(tablename="v#tableName#")>

			<cfset var updatePersonAction = "">
			<cfquery name="updatePersonAction">
				update v#tableName# set personAction = 'Persons matched' where personAction is null and personID is not null
			</cfquery>


			<!--- <cfswitch expression="#arguments.matchMethodID#">

				<cfcase value="1">
					<cfquery name="runPerMatchRules" datasource="#this.datasource#">
						<!--- start matching rules --->
						-- Create Match Names
						UPDATE v#tableName#
						SET PersonMatchName = ISNULL(REPLACE([FirstName],' ',''),'') + ISNULL(REPLACE([lastName],' ',''),'') + ISNULL(REPLACE([email],' ',''),'') + convert(varchar(10), LocationID)    <!--- WAB 2007/07/04 added email to match name --->
						WHERE ([FirstName] Is not Null or [lastName] Is not Null)
						or (
						len(ltrim(rtrim([FirstName]))) > 0
						AND len(ltrim(rtrim([lastName]))) > 0
						)
						-- set the person UPDATES where they match a row in the person table
						UPDATE v#tableName#
						SET PersonAction = 'Person exists matched on firstname, lastname, organisationID and locationID',
						PersonID = p.Personid
						FROM v#tableName# cp INNER JOIN Person p
						ON isnull(cp.[firstName],'') = p.[firstname]   <!--- WAB 2008/06/02 added the isNull because firstname not used in say japan, and comes in as null --->
							AND cp.[lastName] = p.lastname
							AND isnull(cp.[email],'') = p.email   <!--- WAB 2007/07/04 added email to match ---><!--- WAB 2008/06/02 added the isNull --->
							AND cp.locationid = p.locationid
							AND cp.orgID = p.organisationID
						WHERE (cp.[FirstName] Is not Null or cp.[lastName] Is not Null)
							and personAction IS NULL
						<!--- end matching rules --->
					</cfquery>
				</cfcase>

				<!--- match on remote Person ID --->
				<cfcase value="2">
					<cfquery name="runPerMatchRules" datasource="#this.datasource#">
						<!--- start matching rules --->
						-- Create Match Names based on R1PerID
						UPDATE v#tableName#
						SET PersonMatchName = R1PerID

						-- set the person UPDATES where they match a row in the person table
						UPDATE v#tableName#
						SET PersonAction = 'Person exists matched on remote person ID(R1perID)',
						PersonID = p.Personid
						FROM v#tableName# cp INNER JOIN Person p
						ON cp.[R1PerID] = p.[R1perID]
						WHERE cp.[R1PerID] Is not Null
							and personAction IS NULL

						-- ID Bad Persons
						UPDATE v#tableName#
						SET BadRow = isnull(badrow,' ') + 'Null R1perID', personAction = 'Null R1perID'
						WHERE R1perID is null
							and personAction IS NULL
						<!--- end matching rules --->
					</cfquery>
				</cfcase>
			</cfswitch> --->

		<!--- WAB 2010/04/07 made modifications to match on locid as well as person match name
					problem occurs if there are multiple people with the same name at different locations
					added locID to the ##per table

			--->
			<cfquery name="preparePerData" datasource="#this.datasource#">
				-- ID Bad Persons
				UPDATE v#tableName#
				SET BadRow = badrow +'Blank firstname and lastname', personAction = 'Blank firstname and lastname'
				WHERE ([FirstName] Is Null and [lastName] Is Null)
				or (
				len(ltrim(rtrim([FirstName]))) = 0
				AND len(ltrim(rtrim([lastName]))) = 0
				) and personAction IS NULL

				-- set person INSERTS
				UPDATE v#tableName#
				Set PersonAction = 'People for inserting where organisations are new'
				WHERE PersonAction IS NULL
					AND (companyName Is not Null or len(ltrim(rtrim(companyName))) > 0)
					AND OrgAction = 'Organisations to be inserted'

				UPDATE v#tableName#
				Set PersonAction = 'People for inserting where organisations exist'
				WHERE PersonAction IS NULL
				AND (
						(companyName Is not Null or len(ltrim(rtrim(companyName))) > 0)
						--AND OrgAction in ('Organisations matched on company name and country','Organisations matched on MatchName and country','Organisations matched on remote organisation ID(R1OrgID)','OrganisationID given in Source File'))
						AND orgAction = 'Organisations matched')
								or
						OrgAction in ('LocationID given in Source File'   <!--- if locationid given then company name is not an issue --->
					)

			IF OBJECT_ID('tempdb..##Per') is not null
					drop table ##Per

				-- Temp table to hold person data
				CREATE TABLE ##Per (
				RowID int IDENTITY
				)
		</cfquery>

		<cfset application.com.matching.createMatchFieldColumns(tablename="##Per",entityType="person")>
		<cfset var perMatchFields = application.com.matching.getEntityMatchFields(entityType="person").standard>
		<cfif listFIndNoCase(perMatchFields,"matchname")>
			<cfset perMatchFields = listAppend(listDeleteAt(perMatchFields,listFindNoCase(perMatchFields,"matchname")),"personMatchName")>

			<cfquery name="preparePerData">
				exec tempdb..sp_rename 'dbo.##per.matchname','personMatchname','column'
			</cfquery>
		</cfif>
		<cfset var firstRun = true>
		<cfset var perMatchField = "">

		<cfquery name="preparePerData">
			-- Insert Distinct Rows into temp table based on match fields
				INSERT INTO ##Per (#perMatchFields#)
					SELECT DISTINCT #perMatchFields#
					FROM v#tableName#
					WHERE PersonAction like 'People for inserting%'

				declare @personSeed int
				declare @rowsToBeInsertedCount int
				declare @reseedValue int

				select @personSeed = IDENT_CURRENT('person')
				select @rowsToBeInsertedCount = count(1) from ##Per
				set @reseedValue = @personSeed+@rowsToBeInsertedCount

				DBCC CHECKIDENT ('person',RESEED,@reseedValue)

				UPDATE ##Per SET personID = RowID + @personSeed

				-- Insert new personID in to base table
				UPDATE cp
					SET cp.personID = p.personID
					FROM
					v#tableName# cp INNER JOIN ##Per p
					ON
						<cfloop list="#perMatchFields#" index="perMatchField"><cfif not firstRun> AND </cfif>isNull(cp.#perMatchField#,'') = isNull(p.#perMatchField#,'')<cfset firstRun = false></cfloop>
					WHERE personAction like 'People for inserting%'


				<!--- -- Temp table to hold person data
				CREATE TABLE ##Per (
					RowID int IDENTITY,
					PersonMatchName NVarchar(2000),
					PerID int,
					LocID int)


-- Insert Distinct Rows into temp table based on match name
				INSERT INTO ##Per (PersonMatchName, LocID)
					SELECT DISTINCT PersonMatchName, LocationID
					FROM v#tableName# cp
					WHERE PersonAction in ('People for inserting where organisations are new','People for inserting where organisations exist')

				-- Get a seed for the new personid
				-- NM - 20-10-06 - adds call to new table AssignedEntityIDs to get IDs for reserve.
				-- Backup of old method (DECLARE @Seed_PERID int SELECT @Seed_PERID = max(PersonID) FROM Person)
				DECLARE @EntityTypeID int SELECT @EntityTypeID = entityTypeID from schemaTable where entityName = 'person'
				DECLARE @Seed_PERID int SELECT @Seed_PERID = (newSeedValue + entityCount + 1) FROM AssignedEntityIDs WHERE newSeedValue IN (SELECT MAX(newseedvalue) FROM assignedEntityIDs WHERE EntityTypeID = @EntityTypeID) AND (EntityTypeID = @EntityTypeID)
				DECLARE @DataSourceID int SELECT @DataSourceID = datasourceID from datasource where loadtable =  <cf_queryparam value="v#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				DECLARE @NewEntityAmount int SELECT @NewEntityAmount = count(RowID) from ##Per

				-- Insert this loc's assigned ids into AssignedEntityIDs table
				INSERT INTO AssignedEntityIDs (dataloadID,entityCount,EntityTypeID,newSeedValue) VALUES (@DataSourceID,@NewEntityAmount,@EntityTypeID,@Seed_PERID)

				-- Insert new personid into temp table
				UPDATE ##Per
				SET PerID = RowID + @Seed_PERID

				-- Insert new orgid in to base table
				UPDATE cp
				SET cp.PersonID = p.PerID
				FROM v#tableName# cp INNER JOIN ##Per p
				ON cp.PersonMatchName = p.PersonMatchName and cp.LocationID = p.LocID
				WHERE personID IS NULL
				AND PersonAction in ('People for inserting where organisations are new','People for inserting where organisations exist')
 --->

				-- Clean up temp table
				DROP TABLE ##Per


			</cfquery>

			<cfinvoke method="addDataSourceHistory" returnvariable="message2">
				<cfinvokeargument name="tableName" value="#tablename#"/>
				<cfinvokeargument name="dataSource" value="#this.datasource#"/>
				<cfinvokeargument name="action" value="matchDataLoadData"/>
			</cfinvoke>
			<cfreturn "Person data prepared">

		<cfelse>
			<cfreturn "Person data cannot be prepared with null countryIDs">
		</cfif>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Prepare opportunity data for loading
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="prepareOppData"
		hint="Prepares the opportunity data for loading">

		<cfquery name="prepareOppData" datasource="#this.datasource#">
			-- ID Bad Opps
			UPDATE #this.tablename#
			SET BadRow = 'No opportunty name'
			WHERE [opportunityName] Is Null

			-- ID UPDATES
			UPDATE #this.tablename#
			SET opportunityAction = 'U'
			FROM #this.tablename# dl INNER JOIN opportunity o
			ON dl.[opportunityName] = o.detail
			WHERE dl.BadRow IS NULL

			-- ID INSERTS
			UPDATE #this.tablename#
			Set opportunityAction = 'I'
			WHERE opportunityAction IS NULL
				AND BadRow IS NULL
		</cfquery>

		<cfinvoke method="createDistinctRowIDs" returnvariable="qDataSourceHistory">

			<cfinvokeargument name="targetTable" value="opportunity"/>
		</cfinvoke>


		<cfreturn "Organisation data prepared">
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             set rowids for inserts
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createDistinctIDs"
		hint="Creates distinct ids for a given targetTable in tablename">

		<cfargument name="targetTable" type="string" required="true">

		<cfquery name="createDistinctIDs" datasource="#this.datasource#">
			-- Create Match Names
			UPDATE #this.tablename#
			SET #targetTable#MatchName = substring(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(#targetTable#MatchName,'_',''),'+',''),')',''),'(',''),'&',''),'/',''),'-',''),'.',''),' ',''),1,50)

			-- Temp table to hold opp data
			CREATE TABLE ##newIDs (
			RowID int IDENTITY,
			matchname Varchar(100),
			newRowID int)

			-- Insert Distinct Rows into temp table based on match name
			INSERT INTO ##newIDs(matchname)
			SELECT DISTINCT <cf_queryparam value="#targetTable#MatchName" CFSQLTYPE="cf_sql_varchar" >
			FROM #this.tablename#
			WHERE #targetTable#Action = 'I'

			-- Get a seed for the new oppids
			DECLARE @Seed_newID int
			SELECT @Seed_newID = max(#targetTable#ID) FROM #targetTable#

			-- Insert new opp id into temp table
			UPDATE ##newIDs
			SET newRowID = RowID + @Seed_newID + 10
			select * from ##newIDs

			-- Insert newid in to base table
			UPDATE dl
			SET dl.OpportunityID = o.newRowID
			FROM #this.tablename# dl INNER JOIN ##newID o
			ON dl.#targetTable#Matchname = o.matchname
			WHERE BadRow IS NULL

			-- Clean up temp table
			DROP TABLE ##newID
		</cfquery>
		<cfreturn "ID's created">
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkOrgData" returnType="query"
		hint="Org data that did not load">

		<cfquery name="checkOrgData" datasource="#this.datasource#">
			select o.organisationName, d.companyName,d.badrow,d.orgAction, o.organisationID, d.orgid
			from organisation o full outer join v#this.tablename# d
			on o.organisationName=d.companyName
			where o.organisationName is null
		</cfquery>
		<cfreturn checkOrgData>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function checkLocData
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkLocData" returnType="query"
		hint="Returns a query object containing location data that did not load.">

		<cfquery name="checkLocData" datasource="#this.datasource#">
			select l.siteName, d.companyName,d.badrow,d.locationAction, d.locationID
			from location l full outer join v#this.tablename# d
			on l.siteName=d.companyName
			where l.siteName is null
		</cfquery>
		<cfreturn checkLocData>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function checkPerData
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkPerData" returnType="query"
		hint="Returns a query object containing person data that did not load.">


		<cfquery name="checkPerData" datasource="#this.datasource#">
			select p.firstname,p.lastname,
			d.companyName, d.firstname as dataLoad_Firstname,
			d.lastname as dataLoad_Lastname,d.personAction, d.personid ,d.badrow
			from person p
			full outer join #this.tablename# d
			on p.firstname=d.firstname and p.lastname = d.lastname
			where p.firstname is null
		</cfquery>
		<cfreturn checkPerData>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function checkBadRows
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="checkBadRows" returnType="query"
		hint="Returns a query object containing data marked as bad.">

		<cfquery name="checkBadRows" datasource="#this.datasource#">
			select companyName, badrow, firstname,lastname, personid,
			orgID, locationID, personAction, orgAction, locationAction
			from #this.tablename#
			where badRow is not null
		</cfquery>
		<cfreturn checkBadRows>
	</cffunction>


 	<cffunction access="public" name="getPOLColumnMappings" returnType="query">
		<cfargument name="POLTable" type="string" required="true">

			<cfquery name="getMappings" datasource="#this.dataSource#">
			select data_type,loadTableColumnName, replace(relaycolumn,'#POLTable#.','') as relayTableColumnName, dataLoadColumnName,  mappedto, relaycolumn, loadthiscolumn
				from
				vdataloadMappingAll
				where load_table = '#this.tableName#'
				and relaycolumn like '#POLTable#.%'
				and loadThisColumn = 1
			</cfquery >
			<cfreturn getMappings>
		</cffunction>

	<!--- loads  a structure this.polColumnMappings, if requiredColumns is passed, then checks that these particular columns have been mapped--->
	<cffunction access="public" name="loadPOLColumnMappingStructure" >
			<cfargument name="requiredColumns" default = "">

			<cfquery name="getMappings" datasource="#this.dataSource#">
			select data_type,loadTableColumnName, dataLoadColumnName,  mappedto, relaycolumn, loadthiscolumn
				from
				vdataloadMappingAll
				where
					load_table = '#this.tableName#'
					and mappingtype_ like '%pol%'
			</cfquery >

			<cfset this.polColumnMappings = application.com.structureFunctions.queryToStruct(query = getMappings, key="dataloadColumnName")>
			<cfset this.polDataloadToRelayColumnMappings = application.com.structureFunctions.queryToStruct(query = getMappings, key="loadTableColumnName")>

			<cfif requiredColumns is not "">
				<cfset missingColumns = "">
				<cfloop index = "column" list = "#requiredColumns#">
					<cfif not structKeyExists (this.polColumnMappings,column)>
						<cfset missingColumns = listappend (missingColumns, column)>
					</cfif>
				</cfloop>
					<cfif missingColumns is not "">
						<cfoutput>Please <A HREF="profilematching.cfm?tablename=#this.tablename#">map</A> these columns before proceeding:
						<BR>#missingColumns#
						<BR></cfoutput>
						<CF_ABORT> <!--- Sorry - nasty cfabort --->
					</cfif>

			</cfif>
			<cfreturn >
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function insertData
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertData"
		hint="This runs the data insert actions">

		<cfargument name="UpdateExistingRecords" type="boolean" default="false">

		<cfquery name="qry_get_datasource" datasource="#this.datasource#">
			select organisationTypeID from datasource
			where loadtable =  <cf_queryparam value="#this.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >    <!--- LID 6628. removed v prefix--->
		</cfquery>

		<cfif qry_get_datasource.recordcount is 0>
			<cfset organisationTypeID = 1>
		<cfelse>
			<cfset organisationTypeID = qry_get_datasource.organisationTypeID>
		</cfif>

		<cfquery name="getRelayColumns" datasource="#this.datasource#">
		select table_Name + '.' + column_Name as col from information_Schema.columns where table_Name in ('person','location','organisation')
		</cfquery>
		<cfset RelayColumns = application.com.structureFunctions.queryToStruct(query = getRelayColumns, key="col")>

		<cfquery name="getDataLoadColumns" datasource="#this.datasource#">
		select column_name as col from	information_SCHEMA.columns where table_name =  <cf_queryparam value="v#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfset dlColumns = application.com.structureFunctions.queryToStruct(query = getDataLoadColumns, key="col")>

		<!--- PPB 2010-07-15 having  added code for crmOrgID,crmLocID and crmPerID (and made some other fixes) to the UPDATE code, i discovered the UpdateExistingRecords is currently not used so have not completed this section  --->
		<cfif UpdateExistingRecords>
			<!--- 2014-09-11	RPW	Dataloading End Customers is not setting the location account type --->
			<cfquery name="updateData" datasource="#this.datasource#">
			begin tran
				UPDATE organisation
				set R1OrgID = dl.R1OrgID,
					<cfif structKeyExists(RelayColumns,"organisation.crmOrgID") and structKeyExists(dlColumns,"crmOrgID")>crmOrgID = dl.crmOrgID,</cfif>
					countryID = dl.countryID,
				    notes = ltrim(rtrim(dl.notes)),
					orgurl = ltrim(rtrim(dl.MainWebsite)),
					matchname = ltrim(rtrim(dl.orgMatchName)),
					LastUpdated = getdate(),
					LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
					organisationTypeID =  <cf_queryparam value="#organisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
				from v#tablename# dl
				where organisation.organisationID = dl.OrgID
				and((dl.R1OrgID <> dl.R1OrgID and dl.R1OrgID is not null and dl.R1OrgID <> ' ')
					or (dl.countryID <> organisation.countryID and dl.countryID is not null and dl.countryID <> ' ')
					or (ltrim(rtrim(dl.notes)) <> ltrim(rtrim(organisation.notes)) and dl.notes is not null and dl.notes <> ' ')
					or (ltrim(rtrim(orgurl)) <> ltrim(rtrim(dl.MainWebsite)) and dl.MainWebsite is not null and dl.MainWebsite <> ' ')
					or (ltrim(rtrim(matchname)) <> ltrim(rtrim(dl.orgMatchName)) and dl.orgMatchName is not null and dl.orgMatchName <> ' ')
					)

				UPDATE Location
				set R1LocID = dl.R1LocID,
					<cfif structKeyExists(RelayColumns,"location.crmLocID") and structKeyExists(dlColumns,"crmLocID") >crmLocID = dl.crmLocID, </cfif>
					OrganisationID = dl.OrgID,
					Sitename = left(dl.[CompanyName],80),
					Address1 = left(isnull(dl.[Address1],''),80),
					Address2 = left(isnull(dl.[Address2],''),80),
					Address3 = left(isnull(dl.[Address3],''),80),
					Address4 = left(isnull(dl.[City],''),80),
					Address5 = left(isnull(dl.[region],''),80),
					PostalCode = left(isnull(dl.PostalCode,''),30),
					Telephone = left(isnull(dl.[Switchboard],''),30),
					Fax = left(isnull(dl.[Fax],''),30),
					LastUpdated = getdate(),
					LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
					accountTypeID =  <cf_queryparam value="#organisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
				from v#tablename# dl
				where location.locationID = dl.locationID
				and (
					(location.R1LocID <> dl.R1LocID and dl.R1LocID is not null and dl.R1LocID <> ' ')
					or (OrganisationID <> dl.OrgID and dl.OrgID is not null and dl.OrgID <> ' ')
					or (left(location.Sitename,80) <> left(dl.[CompanyName],80) and dl.[CompanyName] is not null and dl.[CompanyName] <> ' ')
					or (left(isnull(dl.Address1,''),80) <> left(isnull(location.[Address1],''),80) and dl.Address1 is not null and dl.Address1 <> ' ')
					or (left(isnull(dl.Address2,''),80) <> left(isnull(location.[Address2],''),80) and dl.Address2 is not null and dl.Address2 <> ' ')
					or (left(isnull(dl.Address3,''),80) <> left(isnull(location.[Address3],''),80) and dl.Address3 is not null and dl.Address3 <> ' ')
					or (left(isnull(dl.City,''),80) <> left(isnull(location.[Address3],''),80) and dl.City is not null and dl.City <> ' ')
					or (left(isnull(dl.region,''),80) <> left(isnull(location.[Address3],''),80) and dl.region is not null and dl.region <> ' ')
					or (left(isnull(dl.PostalCode,''),80) <> left(isnull(location.[PostalCode],''),80) and dl.PostalCode is not null and dl.PostalCode <> ' ')
				    or (left(isnull(dl.Switchboard,''),30) <> left(isnull(location.[Telephone],''),30) and dl.Switchboard is not null and dl.Switchboard <> ' ')
				    <!--- NYB 2009-05-12 Sophos - added line: --->
					or (MatchName=dl.LocationMatchName)
					)

				UPDATE Person
				set R1PerID = dl.R1PerID,
					<cfif structKeyExists(RelayColumns,"person.crmPerID") and structKeyExists(dlColumns,"crmPerID") >crmPerID = dl.crmPerID, </cfif>
					LocationID = dl.LocationID,
					salutation = left(isnull(dl.[salutation],''),100),
					FirstName = left(isnull(dl.[FirstName],''),100),
					LastName = left(isnull(dl.[lastname],''),100),
					JobDesc = left(isnull(dl.[jobDesc],''),100),
					email = left(isNull(dl.[email],''),50),
					mobilePhone = left(isNull(dl.[mobilePhone],''),30),
					officePhone = left(isNull(dl.[directLine],''),30),
					assistantName = isNull(dl.[AssistantName],''),
					language = left(isnull(dl.[language],''),15),
					LastUpdated = getdate(),
					LastUpdatedBy = #request.relayCurrentUser.usergroupid#
				from v#tablename# dl
				where Person.personID = dl.personID
				and ((dl.R1PerID <> person.R1PerID and dl.R1PerID is not null and dl.R1PerID <> ' ')
					or (dl.LocationID <> person.locationID and dl.LocationID is not null and dl.LocationID <> ' ')
					or (left(isnull(dl.[salutation],''),100) <> left(isnull(person.[salutation],''),100) and dl.[salutation] is not null and dl.[salutation] <> ' ')
					or (left(isnull(dl.[FirstName],''),100) <> left(isnull(person.[FirstName],''),100) and dl.[FirstName] is not null and dl.[FirstName] <> ' ')
					or (left(isnull(dl.[LastName],''),100) <> left(isnull(person.[lastname],''),100) and dl.[LastName] is not null and dl.[LastName] <> ' ')
					or (left(isnull(dl.[JobDesc],''),100) <> left(isnull(person.[jobDesc],''),100) and dl.[JobDesc] is not null and dl.[JobDesc] <> ' ')
					or (left(isNull(dl.[email],''),50) <> left(isNull(person.[email],''),50) and dl.[email] is not null and dl.[email] <> ' ')
					or (left(isNull(dl.[mobilePhone],''),30) <> left(isNull(person.[mobilePhone],''),30) and dl.[mobilePhone] is not null and dl.[mobilePhone] <> ' ')
					or (left(isNull(person.officePhone,''),30) <> left(isNull(dl.[directLine],''),30) and dl.[directLine] is not null and dl.[directLine] <> ' ')
					or (isNull(dl.[AssistantName],'') <> isNull(person.[AssistantName],'') and dl.[AssistantName] is not null and dl.[AssistantName] <> ' ')
					or (left(isnull(dl.[language],''),15) <> left(isnull(person.[language],''),15) and dl.[language] is not null and dl.[language] <> ' ')
					)

				commit tran
			</cfquery>
		<cfelse>



			<!--- WAB 2007-12-05 replaced sql transaction with cftransaction, using sql transactions seemed to hide errors
				added a try/catch
				WAB 2009/01/13 	added 'PersonID given in Source File' to the action lists, for TrendSupportInstance where personid given but doesn't exist'
				WAB 2009/03/01 removed all the max() and group by stuff because gave problems and was a bit nasty (could end up with data coming from different rows)
			--->

			<cftry>
			<cftransaction>

				<cftry>
					<!--- 2014-09-11	RPW	Dataloading End Customers is not setting the location account type --->
					<cfquery name="insertData" result="insertDataresult" datasource="#this.datasource#">
			--		begin tran
					set identity_insert dbo.organisation on
					INSERT INTO organisation
						(OrganisationID,
						<cfif structKeyExists(RelayColumns,"organisation.r1orgid") >R1OrgID,</cfif>
						<cfif structKeyExists(RelayColumns,"organisation.crmOrgID") and structKeyExists(dlColumns,"crmOrgID")>crmOrgID,</cfif>
						OrganisationName, CountryID, notes, orgURL,
						matchname,LastUpdated,LastUpdatedBy,
						Created,CreatedBy,organisationTypeID)
						SELECT OrgID,
						<cfif structKeyExists(RelayColumns,"organisation.r1orgid")>R1OrgID,</cfif>
						<cfif structKeyExists(RelayColumns,"organisation.crmOrgID") and structKeyExists(dlColumns,"crmOrgID")>crmOrgID,</cfif>
						left(ltrim(rtrim(companyname)),80),
						CountryID,
						ltrim(rtrim(notes)),
						ltrim(rtrim(mainWebSite)),
						ltrim(rtrim(orgMatchName)),
						getdate(), <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						Getdate(), <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#organisationTypeID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						FROM v#this.tableName#
						WHERE rowid in (
							select min(rowid)
							FROM [v#this.tableName#]
							WHERE OrgAction in ('Organisations to be inserted', 'OrganisationID given in Source File', 'PersonID given in Source File') <!--- TND CR556 add 'PersonID given in Source File' --->
							and orgid not in (select OrganisationID from organisation)
							GROUP BY OrgID
						)

					set identity_insert dbo.organisation off
					set identity_insert dbo.location on

					INSERT INTO Location
						(LocationID,
						<cfif structKeyExists(RelayColumns,"location.r1locid") and structKeyExists(dlColumns,"r1locid") >R1LocID, </cfif>
						<cfif structKeyExists(RelayColumns,"location.crmLocID") and structKeyExists(dlColumns,"crmLocID") >crmLocID, </cfif>
						OrganisationID, CountryID,	Sitename,
						Address1, Address2, Address3, Address4,	Address5,
						PostalCode,	Telephone, Fax,
						LastUpdated,LastUpdatedBy,
						Created,CreatedBy,accountTypeID)
						SELECT LocationID,
						<cfif structKeyExists(RelayColumns,"location.r1locid") and structKeyExists(dlColumns,"r1locid") >R1LocID, </cfif>
						<cfif structKeyExists(RelayColumns,"location.crmLocID") and structKeyExists(dlColumns,"crmLocID") >crmLocID, </cfif>
						OrganisationID, CountryID,
						left([companyname],80),
						left(isnull([Address1],''),80),
						left(isnull([Address2],''),80),
						left(isnull([Address3],''),80),
						left(isnull([city],''),80	),
						left(isnull([region],''),80),
						left(isnull(PostalCode,''),30),
						left(isnull([Switchboard],''),30),
						left(isnull([Fax],''),30),
							getdate(), <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
							Getdate(), <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#organisationTypeID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						FROM [v#this.tableName#]
						WHERE rowid in (
							select min(rowid)
							FROM [v#this.tableName#]
							WHERE LocationAction in ('Locations for inserting where organisations are new','Locations for inserting where organisations exist','PersonID given in Source File')  <!--- TND CR556 add 'PersonID given in Source File' --->
							and LocationID not in (select LocationID from location)
							GROUP BY [LocationID]
							)

					INSERT INTO LocationDatasource
						([LocationID], [DataSourceID], [RemoteDataSourceID])
						SELECT Distinct LocationID, (select top 1 dataSourceID from dataSource
								where loadTable =  <cf_queryparam value="#this.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" > ), 0
						FROM [v#this.tableName#]
						WHERE LocationAction in ('Locations for inserting where organisations are new','Locations for inserting where organisations exist','PersonID given in Source File')
						and LocationID not in (select LocationID from LocationDatasource)

					set identity_insert dbo.location off
					set identity_insert dbo.person on

					declare @sql varchar(250)
					set @sql = 'DISABLE TRIGGER person_uniqueEmailTrig ON person'
					exec(@sql)


					INSERT INTO Person
						(PersonID,
						<cfif structKeyExists(RelayColumns,"person.R1PerID") and structKeyExists(dlColumns,"R1PerID") >R1PerID, </cfif>
						<cfif structKeyExists(RelayColumns,"person.crmPerID") and structKeyExists(dlColumns,"crmPerID") >crmPerID, </cfif>
						LocationID,OrganisationID,
						salutation,FirstName,LastName,
						JobDesc,email,mobilePhone,
						officePhone,
						assistantName,
						FirstTimeUser,
						language,
						Active,passwordDate,
						LastUpdated,LastUpdatedBy,
						Created,CreatedBy)
						SELECT	[PersonID],
						<cfif structKeyExists(RelayColumns,"person.R1PerID") and structKeyExists(dlColumns,"R1PerID") >R1PerID, </cfif>
						<cfif structKeyExists(RelayColumns,"person.crmPerID") and structKeyExists(dlColumns,"crmPerID") >crmPerID, </cfif>
						 [LocationID], [OrgID],
							left(isnull([salutation],''),100),
							left(isnull([FirstName],''),100),
							left(isnull([lastname],''),100),
							left(isnull([jobDesc],''),100),
							left(isNull([email],''),50),
							left(isNull([mobilePhone],''),30),
							left(isNull([directLine],''),30),
							isNull([AssistantName],''),
							1,
							left(isnull([language],''),15),
							1,dateadd(y,-10,getdate()),
							getdate(), <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
							Getdate(), #request.relayCurrentUser.usergroupid#
						FROM [v#this.tableName#]
						WHERE rowid in (
							select min(rowid)
							FROM [v#this.tableName#]
							WHERE PersonAction in ('People for inserting where organisations are new','People for inserting where organisations exist','PersonID given in Source File')
							and personid not in (select personid from person)
							group by [PersonID], [LocationID], [OrgID]
						)

						set identity_insert dbo.person off

						set @sql = 'ENABLE TRIGGER person_uniqueEmailTrig ON person'
						exec(@sql)

				--		commit tran
					</cfquery>

					<!--- ensure that this trigger is enabled again. --->

					<cfcatch>
						<cfset var resetPersonEmailTrigger = "">

						<!--- ensure that this trigger is enabled again. --->
						<cfquery name="resetPersonEmailTrigger">
							ENABLE TRIGGER person_uniqueEmailTrig on person
						</cfquery>

						<cfoutput>Error Inserting Data</cfoutput>
						<cfdump var="#cfcatch#">
						<cftransaction action = "rollback" />
						<CF_ABORT>

					</cfcatch>

				</cftry>

			</cftransaction>


					<cfcatch>
						Error
						<cfdump var="#cfcatch#">
						<CF_ABORT>
					</cfcatch>

				</cftry>


		</cfif>

		<cfinvoke method="addDataSourceHistory" returnvariable="message2">
			<cfinvokeargument name="tableName" value="#this.tablename#"/>
			<cfinvokeargument name="dataSource" value="#this.datasource#"/>
			<cfinvokeargument name="action" value="insertData"/>
		</cfinvoke>

		<cfreturn "Data Loaded">
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="listDataLoads" access="remote" hint="Returns records from the dataSource table">
		<cfargument name="dataSource" type="string" default = "#application.sitedatasource#">
		<cfargument name="limitToExistingTables" type="boolean" default = "false">
		<cfquery name="listDataLoads" datasource="#this.datasource#">
			select datasourceID, loadtable, dlt.dataloadTypeID, dlt.name as LoadType , description
			from dataSource ds
			inner join dataLoadType dlt on ds.dataloadTypeID = dlt.dataloadTypeID
			<cfif arguments.limitToExistingTables>
				inner join INFORMATION_SCHEMA.tables ts on ds.loadTable = ts.table_name
			</cfif>
			where loadtable is not null
			order by description
		</cfquery>
		<cfreturn listDataLoads>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getDataLoadDetails" access="public" hint="Returns details about a dataload">

		<cfset var qGetDataLoadDetails = "">

		<cfquery name="qGetDataLoadDetails" datasource="#this.datasource#">
			select datasourceID, loadtable, dlt.dataloadTypeID, dlt.name as LoadType , description, defaultCountryID
				from dataSource ds inner join dataLoadType dlt on ds.dataloadTypeID = dlt.dataloadTypeID
				where loadtable =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfreturn qGetDataLoadDetails>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="listDataSourceFunctions" access="remote" hint="Returns all the fuctions of the function type passed in">
		<cfargument name="functionType" default="">
		<cfargument name="dataloadTypeFunctions" default="">

		<!--- this should be modified to show general functions and those specific to the type of datasource --->
		<cfquery name="listDataSourceFunctions" datasource="#this.datasource#">
			SELECT distinct [dataLoadFunctionID], [functionName], [functionTextID], dh.[action],sortOrder,
				[URL], [description], [dataLoadType], functionType
				FROM [dataLoadFunctions]
				LEFT OUTER JOIN (select [action]
					FROM dataSourceHistory dh
					INNER JOIN dataLoadFunctions dlf on dlf.functionTextID = dh.action
					where datasourceID = (select datasourceID from datasource WHERE loadTable =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					) dh ON functionTextID = [action]
				where 1=1
				<cfif len(arguments.functionType) gt 0>
					<!--- ==============================================================================
					SWJ  26-06-2008  Added this to allow us to just choose major Functions
					=============================================================================== --->
					and functionType =  <cf_queryparam value="#functionType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif len(arguments.dataloadTypeFunctions) GT 0>
					and dataLoadType =  <cf_queryparam value="#arguments.dataloadTypeFunctions#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				ORDER BY sortOrder
		</cfquery>
		<cfreturn listDataSourceFunctions>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="addDataSourceHistory" access="remote" hint="Inserts a history Record for a given table">

		<cfargument name="action" type="string" required="true">
		<cfset var qaddDataSourceHistory = "">

		<cfquery name="qaddDataSourceHistory" datasource="#this.datasource#">
			INSERT INTO [dataSourceHistory]([action], [DataSourceID])
				select <cf_queryparam value="#action#" CFSQLTYPE="CF_SQL_VARCHAR" >, datasourceID
					FROM datasource
				WHERE loadTable =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<cfreturn "Action recorded">
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="listDataSourceHistory" access="remote" hint="Returns Data Source History records for tablename">

		<cfquery name="listDataSourceHistory" datasource="#this.datasource#">
			select dlf.functionResultText, action, max(dh.created) as lastDone, count(dh.created) as numberofTimes
			FROM dataSourceHistory dh
			INNER JOIN dataLoadFunctions dlf on dlf.functionTextID = dh.action
			where datasourceID = (select datasourceID from datasource WHERE loadTable =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			group by dlf.functionResultText, action
			ORDER by lastDone
		</cfquery>
		<cfreturn listDataSourceHistory>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="splitPersonNameColumns" returnType="query"
		hint="Splits columnToSplit into firstname and lastName columns">

		<cfargument name="columnToSplit" type="string" required="true">

		<cfquery name="splitPersonNameColumns" datasource="#this.datasource#">
			update #this.tablename#
			set firstName = ltrim(rtrim(left(#columnToSplit#,patindex('% %',#columnToSplit#)))),
			lastName =
				case
					when patindex('% %',#columnToSplit#) < len(#columnToSplit#)
				then
					ltrim(rtrim(right(#columnToSplit#,(len(#columnToSplit#)-patindex('% %',#columnToSplit#)))))
				else
					ltrim(rtrim(#columnToSplit#))
				end
			where isNull(firstname,'') ='' and isNull(lastName,'')  = ''
				and (#columnToSplit# is not null or (#columnToSplit#) > 0)
		</cfquery>

		<!--- return 150 rows for the user to check --->
		<cfquery name="columnToSplit" datasource="#this.datasource#" maxrows=150>
			select #columnToSplit#, firstname, lastName from #this.tablename#
		</cfquery>

		<cfset addDataSourceHistory ("splitPersonNameColumns")>


		<cfreturn columnToSplit>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                setup the AssignedEntityIDs table
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="initAssignedEntityIDs" access="remote" hint="Sets up first rows of data for person, location and organisation id's in AssignedEntityIDs table based on relative table's max values.">
		<cfargument name="dataSource" type="string" default="#this.datasource#">

		<cfquery name="initAssignEntityIDs" datasource="#this.datasource#">
			-- Setup organisation ids
			DECLARE @OrgEntityTypeID int SELECT @OrgEntityTypeID = entityTypeID from schemaTable where entityName = 'organisation'
			DECLARE @OrgMaxID int SELECT @OrgMaxID = max(OrganisationID) FROM Organisation
			INSERT INTO AssignedEntityIDs (dataloadID,entityCount,EntityTypeID,newSeedValue) VALUES (0,10,@OrgEntityTypeID,@OrgMaxID)
			-- Setup location ids
			DECLARE @LocEntityTypeID int SELECT @LocEntityTypeID = entityTypeID from schemaTable where entityName = 'location'
			DECLARE @LocMaxID int SELECT @LocMaxID = max(LocationID) FROM Location
			INSERT INTO AssignedEntityIDs (dataloadID,entityCount,EntityTypeID,newSeedValue) VALUES (0,10,@LocEntityTypeID,@LocMaxID)
			-- Setup person ids
			DECLARE @PersonEntityTypeID int SELECT @PersonEntityTypeID = entityTypeID from schemaTable where entityName = 'person'
			DECLARE @PersonMaxID int SELECT @PersonMaxID = max(PersonID) FROM Person
			INSERT INTO AssignedEntityIDs (dataloadID,entityCount,EntityTypeID,newSeedValue) VALUES (0,10,@PersonEntityTypeID,@PersonMaxID)
		</cfquery>
	</cffunction>
<!--- WAB 2009/01/12 believed not used, used version is in com\relaydataload.cfc
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function insertOrganisation
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertOrganisation" hint="This inserts organisation data.">
		<cfargument name="dataSource" type="string" default="#this.datasource#">
		<cfargument name="OrganisationID">
		<cfargument name="OrganisationName">
		<cfargument name="AKA">
		<cfargument name="DefaultDomainName">
		<cfargument name="OrgURL">
		<cfargument name="Notes">
		<cfargument name="MatchName">
		<cfargument name="Confidence">
		<cfargument name="Active">
		<cfargument name="CreatedBy" default="#request.relaycurrentuser.usergroupID#">
		<cfargument name="Created" default="#CreateODBCDate(now())#">
		<cfargument name="LastUpdatedBy" default="#request.relaycurrentuser.usergroupID#">
		<cfargument name="LastUpdated" default="#Created#">
		<cfargument name="Dupe">
		<cfargument name="countryID">
		<cfargument name="AccountMngrID">
		<cfargument name="AccountNumber">
		<cfargument name="ParentOrgID">
		<cfargument name="R2Orgid">
		<cfargument name="LastMeeting">
		<cfargument name="LastCall">
		<cfargument name="LastAttempt">
		<cfargument name="VATnumber">

		<cfset var newID = 0>
		<cfset var matchReplace = application.com.dbTools.getOrgMatchNameReplaceStrings()>

		<cfif not isDefined("OrganisationID")>
			<cfset newID = getAndSetMaxIDForEntity("organisation")>
		<cfelse>
			<cfset newID = OrganisationID>
		</cfif>

		<cfset colList = "OrganisationID">
		<cfset valList = "#newID#">

		<cfloop collection="#arguments#" item="arg">
			<cfif isDefined(arg) and arg is not "DATASOURCE" and arg is not "MATCHNAME">
				<cfset colList = listAppend(colList,arg)>
				<cfset findSingleQuotes = findNoCase("'",arguments[arg])>
				<cfset findNSingleQuotes = findNoCase("N'",arguments[arg])>
				<cfif findSingleQuotes eq 1>
					<cfset tmpVal = replace(arguments[arg],"'","''","ALL")>
					<cfset tmpVal = mid(tmpVal,2,len(tmpVal)-2)>
					<cfset valList = listAppend(valList,tmpVal)>
				<cfelseif  findNSingleQuotes eq 1>
					<cfset tmpVal = replace(arguments[arg],"'","''","ALL")>
					<cfset tmpVal = mid(tmpVal,3,len(tmpVal)-3)>
					<cfset valList = listAppend(valList,tmpVal)>
				<cfelse>
					<cfset valList = listAppend(valList,arguments[arg])>
				</cfif>
			</cfif>
		</cfloop>

		<cfquery name="insertData" datasource="#this.datasource#">
			INSERT INTO Organisation (#colList#<cfif isDefined("MatchName")>,MatchName</cfif>)
			VALUES (#preserveSingleQuotes(valList)#<cfif isDefined("MatchName")>,(substring(#matchReplace.replaceStatement#REPLACE(N' #MatchName# ','','')#replace(matchReplace.replaceValueStatement,"**","'","ALL")#,1,50))</cfif>)
		</cfquery>

		<cfreturn newID>
	</cffunction>
--->

<!--- WAB 2009/01/12 believed not used, used version is in com\relaydataload.cfc
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function insertLocation
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="insertLocation" hint="This inserts location data.">
		<cfargument name="dataSource" type="string" default="#this.datasource#">
		<cfargument name="LocationID">
		<cfargument name="Address1">
		<cfargument name="Address2">
		<cfargument name="Address3">
		<cfargument name="Address4">
		<cfargument name="Address5">
		<cfargument name="PostalCode">
		<cfargument name="CountryID">
		<cfargument name="OrganisationID">
		<cfargument name="SiteName">
		<cfargument name="Telephone">
		<cfargument name="Fax">
		<cfargument name="FaxStatus">
		<cfargument name="SpecificURL">
		<cfargument name="Direct">
		<cfargument name="Active">
		<cfargument name="MatchName">
		<cfargument name="MatchTel">
		<cfargument name="Note">
		<cfargument name="CreatedBy" default="#request.relaycurrentuser.usergroupID#">
		<cfargument name="Created" default="#CreateODBCDate(now())#">
		<cfargument name="LastUpdatedBy" default="#request.relaycurrentuser.usergroupID#">
		<cfargument name="LastUpdated" default="#Created#">
		<cfargument name="DupeChuck">
		<cfargument name="DupeGroup">
		<cfargument name="ActualCountry">
		<cfargument name="ssgCompanyID">
		<cfargument name="EmailDomain">
		<cfargument name="R1LocID">
		<cfargument name="R1ParentID">
		<cfargument name="address6" required="no" default="">  <!--- NJH 2006-12-05 --->
		<cfargument name="address7">
		<cfargument name="address8">
		<cfargument name="address9">
		<cfargument name="nSiteName">
		<cfargument name="nAddress1">
		<cfargument name="nAddress2">
		<cfargument name="nAddress3">
		<cfargument name="nAddress4">
		<cfargument name="nAddress5">
		<cfargument name="CompanyLevel">
		<cfargument name="SiteType">
		<cfargument name="R2LocID">
		<cfargument name="R2OrgID">
		<cfargument name="Latitude">
		<cfargument name="Longitude">

		<cfset var newID = 0>
		<cfset var matchReplace = application.com.dbTools.getOrgMatchNameReplaceStrings()>

		<cfif not isDefined("LocationID")>
			<cfset newID = getAndSetMaxIDForEntity("location")>
		<cfelse>
			<cfset newID = LocationID>
		</cfif>

		<cfset colList = "LocationID">
		<cfset valList = "#newID#">

		<cfloop collection="#arguments#" item="arg">
			<cfif isDefined(arg) and arg is not "DATASOURCE" and arg is not "MATCHNAME">
				<cfset colList = listAppend(colList,arg)>
				<cfset findSingleQuotes = findNoCase("'",arguments[arg])>
				<cfset findNSingleQuotes = findNoCase("N'",arguments[arg])>
				<cfif findSingleQuotes eq 1>
					<cfset tmpVal = replace(arguments[arg],"'","''","ALL")>
					<cfset tmpVal = mid(tmpVal,2,len(tmpVal)-2)>
					<cfset valList = listAppend(valList,tmpVal)>
				<cfelseif  findNSingleQuotes eq 1>
					<cfset tmpVal = replace(arguments[arg],"'","''","ALL")>
					<cfset tmpVal = mid(tmpVal,3,len(tmpVal)-3)>
					<cfset valList = listAppend(valList,tmpVal)>
				<cfelse>
					<cfset valList = listAppend(valList,arguments[arg])>
				</cfif>
			</cfif>
		</cfloop>

		<cfquery name="insertData" datasource="#this.datasource#">
			INSERT INTO Location (#colList#<cfif isDefined("MatchName")>,MatchName</cfif>)
			VALUES (#preserveSingleQuotes(valList)#<cfif isDefined("MatchName")>,(substring(#matchReplace.replaceStatement#REPLACE(N' #MatchName# ','','')#replace(matchReplace.replaceValueStatement,"**","'","ALL")#,1,50))</cfif>)
		</cfquery>

		<cfreturn newID>
	</cffunction>
--->

<!--- WAB 2009/01/12 believed not used, used version is in com\relaydataload.cfc
	<cffunction access="public" name="insertPerson" hint="This inserts person data.">
		<cfargument name="dataSource" type="string" default="#this.datasource#">
		<cfargument name="PersonID">
		<cfargument name="Salutation">
		<cfargument name="FirstName">
		<cfargument name="LastName">
		<cfargument name="Username">
		<cfargument name="Password">
		<cfargument name="PasswordDate">
		<cfargument name="LoginExpires">
		<cfargument name="FirstTimeUser">
		<cfargument name="HomePhone">
		<cfargument name="MobilePhone">
		<cfargument name="FaxPhone">
		<cfargument name="Email">
		<cfargument name="JobDesc">
		<cfargument name="Language">
		<cfargument name="Notes">
		<cfargument name="LocationID">
		<cfargument name="OrganisationID">
		<cfargument name="Active">
		<cfargument name="CreatedBy" default="#request.relaycurrentuser.usergroupID#">
		<cfargument name="Created" default="#CreateODBCDate(now())#">
		<cfargument name="LastUpdatedBy" default="#request.relaycurrentuser.usergroupID#">
		<cfargument name="LastUpdated" default="#Created#">
		<cfargument name="AssistantName">
		<cfargument name="AssistantPhone">
		<cfargument name="DupeChuck">
		<cfargument name="DupeGroup">
		<cfargument name="ActualLocation">
		<cfargument name="Title">
		<cfargument name="Department">
		<cfargument name="R1PerID">
		<cfargument name="R1LocID">
		<cfargument name="emailstatus">
		<cfargument name="sex">
		<cfargument name="initials">
		<cfargument name="SecondName">
		<cfargument name="nFirstName">
		<cfargument name="nLastName">
		<cfargument name="nTitle">
		<cfargument name="R2PerId">
		<cfargument name="R2LocId">
		<cfargument name="LastMeeting">
		<cfargument name="LastCall">
		<cfargument name="LastAttempt">
		<cfargument name="dataScore">
		<cfargument name="DateOfBirth">
		<cfargument name="EmailStatusOld">

		<cfset var newID = 0>
		<cfset var matchReplace = application.com.dbTools.getOrgMatchNameReplaceStrings()>

		<cfif not isDefined("PersonID")>
			<cfset newID = getAndSetMaxIDForEntity("person")>
		<cfelse>
			<cfset newID = PersonID>
		</cfif>

		<cfset colList = "PersonID">
		<cfset valList = "#newID#">

		<cfloop collection="#arguments#" item="arg">
			<!--- 2006/11/15 GCC skip arguments that evaluate to NULL--->
			<cfif isDefined(arg) and arg is not "DATASOURCE" and arg is not "MATCHNAME" and evaluate(arg) neq "">
				<cfset colList = listAppend(colList,arg)>
				<cfset findSingleQuotes = findNoCase("'",arguments[arg])>
				<cfset findNSingleQuotes = findNoCase("N'",arguments[arg])>
				<cfif findSingleQuotes eq 1>
					<cfset tmpVal = replace(arguments[arg],"'","''","ALL")>
					<cfset tmpVal = mid(tmpVal,2,len(tmpVal)-2)>
					<cfset valList = listAppend(valList,tmpVal)>
				<cfelseif  findNSingleQuotes eq 1>
					<cfset tmpVal = replace(arguments[arg],"'","''","ALL")>
					<cfset tmpVal = mid(tmpVal,3,len(tmpVal)-3)>
					<cfset valList = listAppend(valList,tmpVal)>
				<cfelse>
					<cfset valList = listAppend(valList,arguments[arg])>
				</cfif>
			</cfif>
		</cfloop>

		<cfquery name="insertData" datasource="#this.datasource#">
			INSERT INTO Person (#colList#<cfif isDefined("MatchName")>,MatchName</cfif>)
			VALUES (#preserveSingleQuotes(valList)#<cfif isDefined("MatchName")>,(substring(#matchReplace.replaceStatement#REPLACE(N' #MatchName# ','','')#replace(matchReplace.replaceValueStatement,"**","'","ALL")#,1,50))</cfif>)
		</cfquery>

		<cfreturn newID>
	</cffunction>

--->

	<cffunction access="public" name="getAndSetMaxIDForEntity" hint="Retrieves the max id fr an entity and increments by one to set. Returns new ID.">
		<cfargument name="entityName" type="string" required="yes">
		<cfargument name="dataSource" type="string" default="#this.datasource#">

		<cfquery name="getSetMaxID" datasource="#arguments.datasource#">
			DECLARE @EntityTypeID int SELECT @EntityTypeID = entityTypeID from schemaTable where entityName =  <cf_queryparam value="#arguments.entityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			DECLARE @Seed_ID int SELECT @Seed_ID = (newSeedValue + entityCount + 1) FROM AssignedEntityIDs WHERE newSeedValue IN (SELECT MAX(newseedvalue) FROM assignedEntityIDs WHERE EntityTypeID = @EntityTypeID) AND (EntityTypeID = @EntityTypeID)

			-- Insert this entity's assigned id into AssignedEntityIDs table
			INSERT INTO AssignedEntityIDs (entityCount,EntityTypeID,newSeedValue) VALUES (1,@EntityTypeID,@Seed_ID)

			SELECT @Seed_ID as NewID
		</cfquery>

		<cfreturn getSetMaxID.NewID>
	</cffunction>

	<cffunction access="public" name="updateDataloadTableName" hint="This function updates a datasource loadtable column and the dataload table name. Returns new ID.">
		<cfargument name="oldValue" type="string" required="yes">
		<cfargument name="newValue" type="string" required="yes">

		<cfscript>
			var updateNames = "";
		</cfscript>

		<cfquery name="updateNames" datasource="#arguments.datasource#">
			update datasource set loadTable =  <cf_queryparam value="#arguments.newValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
			where loadtable = <cf_queryparam value="#arguments.oldValue#" CFSQLTYPE="CF_SQL_VARCHAR" >

			EXEC sp_rename <cf_queryparam value="#arguments.oldValue#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#arguments.newValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn "Datasource renamed in the DB and the data source table">
	</cffunction>


	<cffunction access="public" name="moveFromTransferToLoadTable" hint="This function moves a table from the transfers DB to the dataload table name. Returns text saying complete.">
		<cfargument name="transferDBtablename" type="string" required="yes">
		<cfargument name="dataloadTable" type="string" required="yes">

		<cfscript>
			var moveData = "";
		</cfscript>

		<cfquery name="moveData" datasource="#arguments.datasource#">
			select *
			into #arguments.dataloadTable#
			from #arguments.transferDBtablename#
		</cfquery>

		<cfreturn "#transferDBtablename# copied to #dataloadTable#">
	</cffunction>


	<cffunction access="public" name="resetDataMatching" hint="Updates the dataload table and sets to NULL orgAction, PersonAction, locationAction and orgMatchName. Returns a text result message.">
		<cfargument name="tableName" type="string" required="yes">

		<cfscript>
			var resetData = "";
		</cfscript>

		<cfquery name="resetData" datasource="#arguments.datasource#">
			update #arguments.tableName#
				set
					orgAction = null,
					locationAction = null,
					personAction= null,

					orgMatchName = null,
					locationMatchName = null,

					<!--- don't reset ids if they were given in the orginal file, in which case the xxxAction will either say 'given in source file' or will still be null  --->
					orgID = case when orgAction = 'OrganisationID given in Source File' or orgAction is null then orgID else null end,
					locationID = case when orgAction = 'LocationID given in Source File' or orgAction is null then  locationid else null end,
					personID = case when orgAction = 'PersonID given in Source File' or orgAction is null then  personid else null end,
					badRow = null
		</cfquery>

		<cfreturn "#arguments.tableName# data matching results reset.">
	</cffunction>


	<!--- 2007/03/2007 WAB made a function --->
	<cffunction access="private" name="getVatNumberRequired" hint="" return="struct">
		<cfset var result= structNew()>
			<!--- 2006/06/23 P_SNY039 GCC Crude mechanism to remind dataloaders of VATnumber importance--->
			<cfquery name="checkVATnumberWarning" datasource="#this.datasource#">
				select countryDescription
				from country
				where countryID in (select distinct countryID from #this.tablename#)
				and VATrequired = 1
			</cfquery>
				<cfif checkVATnumberWarning.recordCount gt 0>
					<cfset result.isRequired = true>
					<cfset result.countryList = valuelist(checkVATnumberWarning.countryDescription)>
				<cfelse>
					<cfset result.isRequired = false>
				</cfif>
		<cfreturn result>
	</cffunction>

	<!--- ==============================================================================
	SWJ  26-06-2008  This function calls each subfunction for preparing the data
	SSS  2008-11-04  Hijacked this function to call functions in a step movement
	=============================================================================== --->
	<cffunction access="public" name="prepareDataForLoading" hint="Runs 2 dataload functions to create control fields and delete null data" return="struct">
		 <cfinvoke method="addControlFields" returnvariable="message2">
		</cfinvoke>
		<cfinvoke method="addDataSourceHistory" returnvariable="message2">
			<cfinvokeargument name="tableName" value="#this.tablename#"/>
			<cfinvokeargument name="dataSource" value="#this.datasource#"/>
			<cfinvokeargument name="action" value="addControlFields"/>
		</cfinvoke>
		<cfinvoke method="deleteNULLData" returnvariable="message2">
		</cfinvoke>
		<cfinvoke method="addDataSourceHistory" returnvariable="message2">
			<cfinvokeargument name="tableName" value="#this.tablename#"/>
			<cfinvokeargument name="dataSource" value="#this.datasource#"/>
			<cfinvokeargument name="action" value="deleteNULLData"/>
		</cfinvoke>
		<cfreturn "#this.tablename# control fields added null data deleted">
	</cffunction>
	<!--- ==============================================================================
	SSS  2008-11-04  This function adds relay data and adds org match names
	=============================================================================== --->
	<cffunction access="public" name="LoadDataRelay" hint="" return="struct">
		 <cfinvoke method="insertData" returnvariable="message2">
		</cfinvoke>
		<cfinvoke method="addDataSourceHistory" returnvariable="message2">
			<cfinvokeargument name="tableName" value="#this.tablename#"/>
			<cfinvokeargument name="dataSource" value="#this.datasource#"/>
			<cfinvokeargument name="action" value="insertData"/>
		</cfinvoke>
		<cfinvoke method="createOrgMatchname" returnvariable="message2">
		</cfinvoke>
		<cfinvoke method="addDataSourceHistory" returnvariable="message2">
			<cfinvokeargument name="tableName" value="#this.tablename#"/>
			<cfinvokeargument name="dataSource" value="#this.datasource#"/>
			<cfinvokeargument name="action" value="createOrgMatchname"/>
		</cfinvoke>
		<cfreturn "#this.tablename# data inserted and org match names created">
	</cffunction>
	<cffunction access="public" name="prepareGeodata" hint="prepares geodata to be inserted" return="struct">
		<cfquery name="addAControlField" datasource="#THIS.datasource#">
			IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name =  <cf_queryparam value="#this.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND column_name='GeoDataStatus')
			begin
				ALTER TABLE #this.tablename# ADD GeoDataStatus nvarchar(50);
			end
		</cfquery>
		<cfquery name="matchGeodata" datasource="#THIS.datasource#">
			UPDATE #this.tablename# set GeoDataStatus = 'matched or incorrect'
				from #this.tablename# dl inner join geodata gd
				on dl.Isocode = gd.isocode
				and dl.place = gd.place
				and isnull(dl.postalcode, '') = isnull(gd.postalcode, '')
				and isnull(dl.province, '') = isnull(gd.province, '')
				where dl.Isocode is not null
				and dl.place is not null
		</cfquery>
	</cffunction>
	<cffunction access="public" name="uploadGeodata" hint="prepares geodata to be inserted" return="struct">

		<cfset var replaceStatement = application.com.relayLocator.replaceSpecialCharactersForDBQuery().replaceStatement>
		<cfset var replaceValueStatement = application.com.relayLocator.replaceSpecialCharactersForDBQuery().replaceValueStatement>

		<cfquery name="insertGeoData" datasource="#THIS.datasource#" debug="No"> <!---SSS 2009-11-17  P-lex039 added debug=no--->
			insert into geodata (GeoKey, DecimalLatitude, DecimalLongitude, IsoCode, Place, PlainPlace, PlainProvince, PostalCode, Province)
			(select 1 as GeoKey, DecimalLatitude, DecimalLongitude, IsoCode, Place, #replaceStatement#Place#replace(replaceValueStatement,"**","'","ALL")# as plainplace, #replaceStatement#Province#replace(replaceValueStatement,"**","'","ALL")# as PlainProvince, PostalCode, Province
			from #this.tablename#
			where GeoDataStatus is null)
		</cfquery>
	</cffunction>


	<!--- NJH 2016/09/01 - create a view on top of the dataload view that gives us the standard RW fields so that we can use if for matching --->
	<cffunction name="createEntityMatchingDataLoadView" access="private" output="false" returnType="string" hint="Creates a view to be passed in to matching functions">
		<cfargument name="tablename" type="string" required="true" hint="Name of table to create the view from. The dataload view.">
		<cfargument name="entityType" type="string" required="true">

		<cfset dropEntityMatchingDataLoadView(tablename=arguments.tablename)>

		<cfset var matchingViewName = "#arguments.tablename#_matching">
		<cfset var matchFields = getMatchFieldsFromDataloadTable(entityType=arguments.entityType)>
		<cfset var createView = "">

		<cfquery name="createView">
			create view dbo.#matchingViewName# as
			select rowid,<cfloop query="#matchFields#">#dataloadColumnName# as #relaywareCol#,</cfloop>
			<cfif arguments.entitytype eq "organisation">orgID as organisationID,orgMatchName as matchname,orgAction<cfelse>#arguments.entityType#ID,#arguments.entityType#matchname as matchname,#arguments.entityType#Action</cfif>
			from #arguments.tablename#
			where 1=1
			<!--- <cfif arguments.entitytype eq "organisation">
				and orgmatchname is null
			<cfelse>
				and #arguments.entityType#matchname is null
			</cfif> --->
		</cfquery>

		<cfreturn matchingViewName>
	</cffunction>


	<cffunction name="dropEntityMatchingDataLoadView" access="private" output="false">
		<cfargument name="tablename" type="string" required="true" hint="The dataload view.">

		<cfset var dropView = "">
		<cfquery name="dropView">
			if exists (select 1 from sys.views where name=<cf_queryparam value="#arguments.tablename#_matching" cfsqltype="cf_sql_varchar">)
				drop view <cf_queryObjectName value="dbo.#arguments.tablename#_matching">
		</cfquery>
	</cffunction>


	<cffunction name="updateMatchingControlFields" access="private" output="false" hint="Updates the newly created match fields with their mapped equivalent">
		<cfargument name="entityType" type="string" required="true">

		<cfset var updateBaseTable = "">
		<cfset var matchFields = getMatchFieldsFromDataloadTable(entityType=arguments.entityType)>
		<cfset var firstRun = true>

		<cfquery name="updateBaseTable">
			update #this.tablename#
				set
				<cfloop query="matchFields"><cfif not firstRun>,</cfif>#relaywareCol# = #dataloadColumnName#<cfset firstRun=false></cfloop>
		</cfquery>

	</cffunction>

	<cffunction name="getMatchFieldsFromDataloadTable" access="private" output="false" returnType="query" hint="Returns a query with the matchfields and their equivalent in the dataload view">
		<cfargument name="entityType" type="string" required="true">

		<cfset var requiredMatchFields = application.com.matching.getRequiredMatchFields(entityType=arguments.entityType)>
		<cfset requiredMatchFields = application.com.globalFunctions.ListMinusList(list1=requiredMatchFields,list2="matchname,#arguments.entityType#ID")>
		<cfset var getMatchFieldsFromDataloadTableQry = "">

		<cfquery name="getMatchFieldsFromDataloadTableQry">
			select dataloadColumnName,replace(relayColumn,'#arguments.entityType#.','') as relaywareCol
			from vdataloadMappingAll
			where load_table='#this.tablename#'
				and replace(relayColumn,'#arguments.entityType#.','') in (<cf_queryparam value="#requiredMatchFields#" cfsqltype="varchar" list="true">)
			union
			select column_name as dataloadColumnName, column_name as relaywareCol
			from information_schema.columns where column_name in (<cf_queryparam value="#requiredMatchFields#" cfsqltype="varchar" list="true">)
				and table_name='#this.tablename#'
				and column_name not in (
					select replace(relayColumn,'#arguments.entityType#.','') from vdataloadMappingAll where load_table='#this.tablename#' and relayColumn is not null)
			<cfif arguments.entityType eq "organisation">
				and column_name != 'organisationName'
			union
			select 'companyName' as dataloadColumnName, 'organisationName' as relaywareCol
			</cfif>
		</cfquery>

		<cfreturn getMatchFieldsFromDataloadTableQry>

	</cffunction>
</cfcomponent>

