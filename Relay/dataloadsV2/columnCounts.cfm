<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History:

Date (DD-MMM-YYYY)  Initials    What was changed
2015-11-12          ACPK        Added Bootstrap class to table
 --->
<cfparam name="tableName" default="">

<cfif tableName neq "">

<cfsetting requesttimeout="7200">
<!--- <cfinclude template="dataloadsTopHead.cfm"> --->
<cfparam name="message" default=" ">
<cfparam name="sortOrder" default="column_name">


	<cfscript>
	// create an instance of the object
	myObject = createObject("component","relay.com.dbInterface");
	//2008/07/15 GCC initTable no longer exists but this does the job
	//myObject.initTable (datasource = application.siteDataSource, tablename = tablename);
	myObject.datasource = application.siteDataSource;
	myObject.tablename = tablename;
	myObject.getAllCols();
	</cfscript>
    <!--- 2015-11-12    ACPK    Added Bootstrap class to table --->
	<TABLE CLASS="table table-hover">
		<tr>
			<cfoutput>
			<th colspan="100">
				Counts of values in all columns in #htmleditformat(tablename)#
			</th>
			<th colspan="150">&nbsp;
			</th>
			</cfoutput>
		</tr>
		<tr>
			<td colspan="100" valign="top">
				<cfinvoke component="relay.com.dbInterface" method="getDistinctColumnCounts"
					returnvariable="columnCounts">
					<cfinvokeArgument name="datasource" value="#application.siteDataSource#">
					<cfinvokeArgument name="tablename" value="#tableName#">
				</cfinvoke>
				<CF_tableFromQueryObject
					queryObject="#columnCounts#"
					keyColumnList="column_name"
					keyColumnURLList="showdistinctColValues.cfm?tablename=#tablename#&columnName="
					keyColumnKeyList="column_name"
					keyColumnTargetList="distinctValFrame"
					useInclude="false"
					sortOrder="#sortOrder#" numRowsPerPage="100">
			</td>
			<td colspan="150" valign="top">
				<iframe name="distinctValFrame" id="distinctValFrame" width="100%" height="650"></iframe>
			</td>
		</tr>
	</TABLE>
</cfif>