<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2009-04-27   SSS added extra code so that the correct functions will show for the dataloadtype.
2011-07-25 	NYB  LHID6768 - added limitToExistingTables argument to limit dataloads that have corresponding tables - to stop it falling over
2015-11-11  ACPK    Added Bootstrap

 --->

<cfset application.com.request.setDocumentH1(text="Load Data")>

 <cf_includeonce template="/templates/relayFormJavaScripts.cfm">
 <cf_includejavascriptonce template="/javascript/extextension.js">
 <cf_includejavascriptonce template="/javascript/openwin.js">
<cfsetting requesttimeout="7200">
<cfinclude template="dataloadsTopHead.cfm">

<cf_querySim>
	qDataSourceFunctions
	Metric,AssociatedWith,TimeFrame,Objective,Actual,PercentComplete
</cf_querySim>


<!--- WAB 2009/03/04 altered this bit setting tablename.
Didn't work when I called this page with url.tablename and then used the drop down to change select boxes
Don't know why some places use table name and some use datasourcetable!'
--->
<cfif isdefined("FORM.dataSourceTable") and FORM.dataSourceTable neq "">
	<cfset tableName = FORM.dataSourceTable>
<cfelse>
	<cfparam name="tableName" default="">
</cfif>

<cfset session.tableName = tablename>



<cfparam name="message" default=" ">

	<cfscript>
	// create an instance of the object
	dataLoadObject = createObject("component","relayDataloadV2");
	dataLoadObject.initTable (datasource = application.siteDataSource, tablename = tablename);
	</cfscript>

	<cfinvoke component="#dataLoadObject#" method="listDataLoads" returnvariable="dataLoads">
		<cfinvokeargument name="limitToExistingTables" value="true">
	</cfinvoke>

	<cfif isDefined("action")>

		<cfinvoke component="#dataloadObject#" method="#action#" returnvariable="message">
		</cfinvoke>

		<cfinvoke component="#dataloadObject#" method="addDataSourceHistory" returnvariable="message">
			<cfinvokeargument name="action" value="#action#"/>
		</cfinvoke>

	</cfif>

	<cfif isDefined("tableName") and tableName neq "">
	<!--- check to see if we have exactly 1 table matching variables.tableName --->
	<cfinvoke component="relay.com.dbTools" method="getTableDetails" returnvariable="tableList">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="tableName" value="#tableName#"/>
	</cfinvoke>
	<!--- SSS 2009-04-27 added the stuff below to make usre the correct functions are showing for the right dataloads --->
	<cfquery name="qry_get_dataloadtypetextID" datasource="#application.siteDataSource#">
		select * from datasource ds inner join
			[dataLoadtype] dt on ds.dataloadtypeID = dt.dataloadtypeID
		where loadtable =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfquery>
	<cfset currentLoadTypeTextID = qry_get_dataloadtypetextID.dataloadTypeTextID>

	<cfif currentLoadTypeTextID EQ "Reseller" or currentLoadTypeTextID EQ "EndUser" or currentLoadTypeTextID EQ "InternalUser">
		<cfset dataloadTypeFunctions = 'general'>
	<cfelse>
		<cfset dataloadTypeFunctions = currentLoadTypeTextID>
	</cfif>

	<cfinvoke component="#dataLoadObject#" method="listDataSourceFunctions" returnvariable="qDataSourceFunctions">
			 <cfinvokeargument name="functionType" value="MajorFunction"/>
			 <cfinvokeargument name="dataloadTypeFunctions" value="#dataloadTypeFunctions#"/>
	</cfinvoke>

	<form method="post" class="form-horizontal" name="selectForm">
		<SELECT class="form-control" NAME="dataSourceTable" onchange="document.selectForm.submit()">
			<OPTION VALUE="">Choose data load to work on</OPTION>
			<CFOUTPUT QUERY="dataLoads">
				<OPTION VALUE="#loadtable#" <cfif tableName eq description> SELECTED</cfif>>#htmleditformat(description)#</OPTION>
			</CFOUTPUT>
		</SELECT>
	</form>

	<!--- If we have a matching table then carry on --->
	<cfif tableList.recordCount eq 1>
		<cfinvoke component="#dataLoadObject#" method="listDataSourceHistory" returnvariable="qDataSourceHistory"></cfinvoke>
		<cfoutput>
			<h4>Manage dataload for #htmleditformat(tableName)#</h4>
			<p><cfif IsSimpleValue(message) and message neq "">#application.com.relayUI.message(message=message)#</cfif></p>
		</cfoutput>

		<div class="col-xs-8">
			<TABLE CLASS="table table-hover">
				<tr>
					<td colspan="3" class="heading">Data Load functions.</td></td>
				</tr>
				<tr>
					<th>Function</th><th>Status</th><th>Description</th>
				</tr>

				<cfoutput query="qDataSourceFunctions">
				<TR>
					<TD HEIGHT="20" VALIGN="TOP">
					<cfif FindNoCase("javascript",url) NEQ 0>
						<A HREF="#URL#">#htmleditformat(currentRow)#. #htmleditformat(functionName)# </A>
					<cfelse>
						<A HREF="#URL#&tablename=#tablename#">#htmleditformat(currentRow)#. #htmleditformat(functionName)#</A>
					</cfif>
					<!--- SSS 2009-05-06 some things open in a window or a new tab now P-len008 Begin--->
				<script>
				function openNewTabX(tabName,tabTitle,href,options) {
					openNewTab(tabName,tabTitle,iDomainAndRoot + href + '?tablename=#jsStringFormat(tablename)#',options) ;
						}
				</script>
				<!--- P-len008 END --->
					</TD>
					<td><cfif qDataSourceFunctions.action neq "">Done</cfif></td>
					<TD VALIGN="Top">#htmleditformat(description)#</TD>
				</TR>
				</cfoutput>
			</TABLE>
		</div>
		<div class="col-xs-4">
			<TABLE CLASS="table table-hover">
				<tr>
					<td colspan="2" class="heading">What's been done.</td></td>
				</tr>
				<tr>
					<th>Action</th><th>Last done</th><th>Times</th>
				</tr>
				<cfoutput query="qDataSourceHistory">
				<TR>
					<TD VALIGN="TOP">
						#htmleditformat(functionResultText)#
					</TD>
					<TD VALIGN="Top">#dateformat(lastDone,"dd-mmm-yyy")#</TD>
					<td valign="top">#htmleditformat(numberofTimes)#</td>
				</TR>
				</cfoutput>
			</TABLE>
		</div>
	<cfelse>
		<cfoutput>
			<p class="messageText">There is no table matching #htmleditformat(variables.tableName)#.</p>
			<p class="messageText">You need to upload your data into a table called #htmleditformat(variables.tableName)#
			or rename the table you have loaded your data into.</p>
		</cfoutput>
	</cfif>
</cfif>

<cfif IsQuery(message)>
	<CF_tableFromQueryObject queryObject="#message#" sortOrder="">
</cfif>