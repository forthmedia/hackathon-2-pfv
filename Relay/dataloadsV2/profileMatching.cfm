<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			dataMatching.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-03-02   WAB Changes to implement AJAX for removing,accepting and ignoring columns
2011-05-04		WAB changed ajax so that uses returnFormat=plain
2015-11-11          ACPK        Added Bootstrap

Possible enhancements:


 --->



<cfscript>
// create an instance of the object
dataLoadObject = createObject("component","relayDataloadFlagsV2");
dataLoadObject.initTable (datasource = application.siteDataSource, tablename = tablename);
dataLoads = dataLoadObject.listDataLoads() ;


if (isDefined("URL.flagRecords") and URL.flagRecords eq 1
	and URL.columnName neq "") {
// i.e. we' want to insert flagData
	message =dataLoadObject.insertFlagDataForThisCol(loadtablecolumn = columnName, reload = true);   // since user has clicked a specific link, allow data to be reloaded

//	dataLoadObject.insertTextDataToFlagDataForThisCol(columnName) ;
}




</cfscript>

<cfif isDefined("action")>
	<cfinvoke component="#dataLoadObject#" method="#action#" returnvariable="message" argumentcollection="#form#">
	</cfinvoke>
	<cfinvoke component="#dataLoadObject#" method="addDataSourceHistory">
		<cfinvokeargument name="action" value="#action#"/>
	</cfinvoke>
</cfif>

<cfif isDefined("FORM.oldColumnName")
	and isDefined("FORM.newColumnName")>

		<cfset dataLoadObject.mapColumn(loadTableColumn = fORM.oldColumnName,MappedColumn =#FORM.newColumnName#)>

</cfif>

<cfif isDefined("ignoreColumnName") and ignoreColumnName neq "">
		<cfset dataLoadObject.ignoreColumn(loadTableColumn = ignoreColumnName)>
</cfif>

<cfif isDefined("removeColumnMapping") and removeColumnMapping neq "">
		<cfset dataLoadObject.deleteDataLoadMapping(loadTableColumn = removeColumnMapping)>
</cfif>


<cfif isDefined("addNewColumn") and addNewColumn eq "Add Column" and newColumnName neq "">
	<cfinvoke component="relay.com.dbtools" method="addColumn" returnvariable="message">
		<cfinvokeargument name="tableName" value="#tablename#"/>
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="newColumnName" value="#newColumnName#"/>
		<cfinvokeargument name="columnDataType" value="#columnDataType#"/>
		<cfinvokeargument name="columnDataLength" value="#columnDataLength#"/>
		<cfinvokeargument name="initialValue" value="#initialValue#"/>
		<cfinvokeargument name="whereClause" value="#whereClause#"/>
	</cfinvoke>
</cfif>

<cfscript>
dataLoadObject.listDataloadFlagMapping();
qMappedColumns = dataLoadObject.mappedColumns();
qIgnoredColumns = dataLoadObject.ignoredColumns();
qMismatchedCols = dataLoadObject.mismatchedCols();
qgetAdditonalLoadCols = dataLoadObject.getAdditonalLoadCols() ;


</cfscript>




<cf_head>
	<cf_title>Column Mapping to Relay</cf_title>

	<script>


	function acceptColumn (obj,tableName,columnName){
		acceptOrRemoveColumn ('confirmAutoMappingremote',obj,tableName,columnName)
	}
	function removeColumn (obj,tableName,columnName) {
		acceptOrRemoveColumn ('deleteDataLoadMappingRemote',obj,tableName,columnName)
	}
	function ignoreColumn (obj,tableName,columnName) {
		acceptOrRemoveColumn ('ignoreColumnRemote',obj,tableName,columnName)
	}

	function acceptOrRemoveColumn (method, obj,tableName,columnName) {

			page = '/webservices/relayDataloadsWS.cfc?wsdl&returnFormat=Plain&_cf_nodebug=true&method=runMethod&methodname='+ method
			parameters = 'tablename=' + tableName + '&loadTableColumn='+columnName
			div = obj

			var myAjax = new Ajax.Updater(
				div,
				page,
				{
					method: 'get',
					parameters: parameters ,
					evalScripts: true,
					debug : false

				});




	}
	</script>
</cf_head>

<cfset application.com.request.setDocumentH1(text="Load Data")>

<cfparam name="message" default=" ">

<cfinclude template="dataloadsTopHead.cfm">
<cfoutput>
	<h2>Column Mapping</h2>
	<p>Use this screen to manage the matching of data in #htmleditformat(tablename)# to the relevant tables and profiles in Relay.</p>
    <cfif IsSimpleValue(message) and message neq "">#application.com.relayUI.message(message=message)#</cfif>

<h2>Unmapped columns</h2>
<!--- <a href="#SCRIPT_NAME#?action=validateCountryData&tablename=#tablename#">Click here</a> to do this.</p> --->
	<p>Review the field names below and map the field in the left hand column to a Relay
		field by selecting it from the associated drop down.</p>
	<p>As you map each field the remaining ones will reduce in number.  You can look at the data
		in the oncoming fields to help you decide.</p>

		<form action="?" class="form-horizontal" method="post">
			<div class="form-group"><div class="col-xs-12">
				<CF_INPUT type="hidden" name="tablename" value="#tableName#">
				<input type="hidden" name="action" value = "autoMap">
				<label>Get System to try and Match Column Names Automatically</label>
				<Input type="submit" class="btn-primary btn" value = "Automatically Map Columns">
			</div></div>
		</form>
		<cfform action="?" class="form-horizontal" method="post" name="ImportMappings">
			<div class="form-group"><div class="col-xs-12">
				<input type="hidden" name="action" value = "copyDataLoadMappingsToThisDataload">
				<CF_INPUT type="hidden" name="tablename" value="#tableName#">
					<label for="fromDataLoadID">Import Column Mappings from another Dataload:</label>
					<cfselect id="fromDataLoadID" class="form-control" name="fromDataLoadID" queryposition="below" query="dataLoads" value="datasourceid" display="loadtable" visible="Yes" enabled="Yes" onchange="this.form.submit()">
						<option value = "">Select Data Load To Import Mappings From
					</cfselect>
			</div></div>
		</cfform>

		<cfif qGetAdditonalLoadCols.recordCount gt 0>
			<h5>This is the list of fields left to map to :</h5>
			<p>#htmleditformat(valueList(qGetAdditonalLoadCols.dataLoadColumnName))#</p>
		<cfelse>
			<p>Well done.  You have mapped all of the fields.</p>
		</cfif>

<table class="table table-hover">
<!--- 	<tr>
		<td colspan="5" class="messageText">
			#message#
		</td>
	</tr>
 --->	<tr>
	<tr>
		<th>Incoming data </th>
		<th nowrap>Review Data</th>
		<th>Set this to</th>
		<th>Map to Profile</th>
		<th>Ignore column</th>
	</tr>
</cfoutput>

<cfoutput query="qMismatchedCols">
	<form action="" method="post" name="#column_name#">
	<CF_INPUT type="hidden" name="oldColumnName" value="#column_name#">
	<CF_INPUT type="hidden" name="tablename" value="#tableName#">
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<td>#htmleditformat(column_name)# </td>
		<td align="center"><a href="showDistinctColValues.cfm?tablename=#tablename#&columnName=#column_name#">View</a></td>
		<td>
			<select name="newColumnName" onChange="#column_name#.submit()">
				<OPTION VALUE="0">Select new column name</OPTION>
				<CFLoop QUERY="qGetAdditonalLoadCols">
					<OPTION VALUE="#dataLoadColumnName#">#htmleditformat(dataLoadColumnName)# (#htmleditformat(relayColumn)#)</OPTION>
				</CFLoop>
			</select>

		</td>
		<td><a href="mapColToFlag.cfm?tablename=#tablename#&columnName=#column_name#">Map to Profile</a></td>
		<td><a href="" onclick = "javascript:ignoreColumn(this,'#tablename#','#column_name#');return false">Ignore</a></td>
<!--- 		<td><a href="manageMismatchedCols.cfm?tablename=#tablename#&IgnoreColumnName=#column_name#">Ignore</a></td> --->
	</tr>
	</form>
</cfoutput>

</table>

<!--- <hr width="100%" size="1">

<p>These columns need mapping.

<CF_tableFromQueryObject queryObject="#qMismatchedCols#"
	keyColumnList="column_name"
	keyColumnURLList="mapColtoFlag.cfm?tablename=#tablename#&columnName="
	keyColumnKeyList="column_name"
	sortOrder="column_name"
	useInclude="false"> --->


<hr width="100%" size="1">
<h2>Mapped columns</h2>

<cfloop index = "autoMappedOrNot" list = "1,0">
	<cfquery name="FlagColumns" dbtype="query">
	select '#tableName#' as tablename ,* , 'Remove' as remove, 'Accept Mapping' as AcceptMapping
	from dataLoadObject.qDataloadFlagMapping where automapped = #autoMappedOrNot#
	</cfquery>


	<cfif FlagColumns.recordCount is not 0>
		<cfif autoMappedOrNot is 1>
		<p>These columns have been automatically mapped to attributes and should be reviewed
		<cfelse>
		<p>These columns have been mapped to attributes
		</cfif>

		<CF_tableFromQueryObject
			queryObject="#FlagColumns#"
			hidepagecontrols = true
			showTheseColumns="load_column,mappingType,flagGroup,LoadData,Profile_attribute,FlagID,loadedDate_LocalTime,Remove,#iif(automappedOrNot,de('AcceptMapping'),de(''))#"
			ColumnHeadingList="LoadTable Column Name<BR>Click To Load,Mapping Type,Flag Group,Load Data Value, Maps to Attribute<BR>Click to Modify,ID,Load Date (Local Time),Remove,#iif(automappedOrNot,de('Accept Mapping'),de(''))#"
			sortOrder="load_column"
			keyColumnList="load_column,profile_attribute,acceptMapping,Remove"
			keyColumnURLList="profilematching.cfm?tablename=#htmleditformat(tablename)#&flagRecords=1&columnName=,mapColtoFlag.cfm?tablename=#htmleditformat(tablename)#&columnName=, , "
			keyColumnOnclickList=" , ,javascript:acceptColumn(this*comma'##htmleditformat(tablename)##'*comma'##htmleditformat(load_column)##');return false,javascript:removeColumn(this*comma'##htmleditformat(tablename)##'*comma'##htmleditformat(load_column)##');return false"
			keyColumnKeyList="load_column,load_column,load_column,load_column"
			numRowsPerPage="200"
			useInclude="false">


			<cfif autoMappedOrNot is 0>
			<!--- have a link to load all flags --->
			<cfoutput>
			<form action="?" method = "post" style="display:inline">
				<CF_INPUT type="hidden" name="tablename" value="#tableName#">
				<input type="hidden" name="action" value = "insertAllFlagData">
				<Input type="submit"  value = "Load all Unloaded Attribute Columns">
			</form>
			</cfoutput>

			</cfif>

	</cfif>

</cfloop>

<!--- GCC 2009-02-03 Sophos issue - can't remove POL mappings but there is a remove button - split into two reports--->
<cfquery name="qUnremovable" dbtype="query">
select * from qMappedColumns where automapped <> 0
</cfquery>
<cfquery name="qRemovable" dbtype="query">
select * from qMappedColumns where automapped = 0
</cfquery>

<cfif qRemovable.recordcount gt 0>
	These POL Columns have been mapped
	<CF_tableFromQueryObject
		queryObject="#qRemovable#"
		showTheseColumns="loadtablecolumnname,mappedTo,relaycolumn,automapped,Remove"
		ColumnHeadingList="Load Table Column Name,Mapped To,Relay Column,Auto Mapped,Remove"
		hidepagecontrols = true
		sortOrder="mappedTo"
		keyColumnList="remove"
		keyColumnURLList=" "
		keyColumnOnclickList="javascript:removeColumn(this*comma'##tablename##'*comma'##loadtablecolumnname##');return false"
		keyColumnKeyList="loadtablecolumnname"
		numRowsPerPage="200"
		useInclude="false">
</cfif>

These POL Columns have been automatically mapped
<CF_tableFromQueryObject
	queryObject="#qUnremovable#"
	hidepagecontrols = true
	showTheseColumns="loadtablecolumnname,relaycolumn"
	ColumnHeadingList="Load Table Column Name,Relay Column"
	sortOrder="mappedTo"
	numRowsPerPage="200"
	useInclude="false">

	<cfoutput>
		<!---
			WAB 2007-09-06
			A form and script for doing the remove mapping
			- was being done as a url link, but had problems with the url variables persisting --->

		<script  type="text/javascript">
		function removeMapping(column) {
			form = document.removeColumnMappingForm
			form.removeColumnMapping.value = column
			form.submit()
		}
		</script>


			<form action="?" method = "post" style="display:inline" name="removeColumnMappingForm">
				<CF_INPUT type="hidden" name="tablename" value="#tableName#">
				<input type="hidden" name="removeColumnMapping" value = "">
			</form>
	</cfoutput>


<cfif qIgnoredColumns.recordcount is not 0 >
These columns are being ignored
<CF_tableFromQueryObject
	queryObject="#qIgnoredColumns#"
	hidepagecontrols = true
	keyColumnList="remove"
	keyColumnURLList="javascript:removeMapping('thisurlkey');"
	keyColumnKeyList="loadtablecolumnname"
	numRowsPerPage="200"
	useInclude="false">
</cfif>


<script>
	function validateForm() {
		form = document.newColumnForm;
		if (form.columnDataType.value == 'varchar') {
			if ((form.columnDataLength.value.replace(/^\s+|\s+$/, '') == '') || (isNaN(form.columnDataLength.value))) {
				alert('Please enter a column length.')
				return false;
			}
		}
		return true;
	}
</script>

<!--- Country matches --->
<h2>Add a new column to map to a profile in Relay</h2>
<cfoutput>
<form id="newColumnForm" class="form-horizontal" name="newColumnForm" action="#SCRIPT_NAME#?#request.query_string#" method="post" onSubmit="return validateForm();">
	<div class="form-group"><div class="col-xs-12">
	   <CF_INPUT type="hidden" name="tablename" value="#tablename#">
	   <label for="newColumnName">New Column name</label>
	   <input type="text" id="newColumnName" class="form-control" name="newColumnName">
	</div></div>
	<div class="form-group"><div class="col-xs-12">
		<label for="columnDataType">Column data type</label>
		<select id="columnDataType" class="form-control" name="columnDataType">
			<option value="varchar" SELECTED>varchar</option>
			<option value="int">integer</option>
		</select>
	</div></div>
	<div class="form-group"><div class="col-xs-12">
		  <label for="columnDataLength">Column Length (only required for varchar)</label>
		  <input type="text" id="columnDataLength" class="form-control" name="columnDataLength">
    </div></div>
	<div class="form-group"><div class="col-xs-12">
		  <label for="initialValue">Initialise value to</label>
		  <input type="text" id="initialValue" class="form-control" name="initialValue">
	</div></div>
	<div class="form-group"><div class="col-xs-12">
		  <label for="whereClause">Where clause (SQL after the where)</label>
		  <input type="text" id="whereClause" class="form-control" name="whereClause">
	</div></div>
	<div class="form-group"><div class="col-xs-12">
		  <label for="addNewColumn">
		  <input type="submit" id="addNewColumn" class="btn-primary btn" name="addNewColumn" value="Add Column">
    </div></div>
</form>
</cfoutput>