<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="form.dataSource" default="#application.siteDataSource#">
<cfparam name="form.loadTable" default="">
<cfparam name="form.loadTableColumn" default="">
<cfparam name="form.tablename" default="">
<cfparam name="form.entityIDcolumnName" default="">
<cfparam name="form.submitMapping" default="0">
<cfparam name="form.createFlag" default="0">
<cfparam name="form.submitFlag" default="0">
<cfparam name="form.frmProfileEntity" default="">



<cfscript>
	myObject = createObject("component","relayDataloadFlagsV2");
	myObject.dataSource = application.siteDataSource;
	
	if(len(form.loadTable)){
		columnsListQry = myObject.getTableColumns(form.loadTable);
		columnNames = valueList(columnsListQry.columnName);
	}
	
	if(len(form.loadTable) and len(form.loadTableColumn) and columnsListQry.recordCount and listFindNoCase(columnNames,form.loadTableColumn)){
		myObject.tableName = form.loadTable;
		myObject.loadTableColumn = form.loadTableColumn;
		myObject.listDataloadFlagMapping();
		myObject.getFlagCheckboxList();
		textbetween = '</td></tr><tr><td colspan="2">';
		distinctQry = application.com.dbinterface.getDistinctColumnValues(form.dataSource,form.loadTable, form.loadTableColumn);
	}
	
	
	if(form.createFlag){
		flagGroupQry = application.com.profileManagement.getFlagGroups(form.frmProfileEntity);
	}
	
	if(form.submitFlag){
		application.com.profileManagement.createNewFlag(form.flagGroupForNewFlag,form.flagTextID,form.flagName);
		
	}
	
	
	if(form.submitMapping and structKeyExists(form, "distinctValues") and len(form.loadTable) and len(form.loadTableColumn)){
		for(i = 1; i lte listLen(form.distinctValues); i = i+1){
			myObject.insertLogData(form.loadTable,form.loadTableColumn,listGetAt(form.distinctValues, i), form.frmFlagID);
		}
		
	}
</cfscript>


<cfif isDefined("columnsListQry") and columnsListQry.recordCount eq 0>
	No such table inside this datasource.
	<CF_ABORT>
</cfif>

<cfif isDefined("columnsListQry") and len(form.loadTableColumn) and not listFindNoCase(columnNames,form.loadTableColumn)>
	No such column inside this table. Try sybmitting only the table name and coming up with the list of column names.
	<CF_ABORT>
</cfif>

<SCRIPT type="text/javascript">
	function mapValues(){
		form = document.flagForm;
		var checkedYes = 0;
		
		for(i=0;i<=form.distinctValues.length-1;i++){
			
			if(form.distinctValues[i].checked == true){ 
				checkedYes = 1;
			}
		}
		
		if(!checkedYes) alert("Please check at least one distinct value");
		//menuform.menu3.options[menuform.menu3.selectedIndex].value
		else if(document.flagForm.frmFlagID.options[flagForm.frmFlagID.selectedIndex].value == '') alert("Please select a flag to map to");
		else{
			document.flagForm.submitMapping.value=1;
			document.flagForm.submit()
		}
	}
</script>

<cfoutput>
<form name="flagForm" method="post" action="#cgi.SCRIPT_NAME#">
<table border="1">
	<tr>
		<td width="350px">
			<TABLE >
				<tr>
					<td>
						Table
					</td>
					<td>
						Column
					</td>
					
				</tr>
				
				<tr>
						<CF_INPUT type="hidden" name="dataSource" value="#form.datasource#">
					<td>
						<cfset loadTableQry = myObject.listDataLoads()>
						<select name="loadTable">
							<cfloop query="loadTableQry">
								<option value="#loadTable#" <cfif structKeyExists(form, "loadTable") and form.loadTable eq loadTable>selected</cfif> >#htmleditformat(loadTable)#</option>
							</cfloop>
						</select>
					</td>
					<cfif not len(form.loadTable) or columnsListQry.recordCount eq 0>
					<td>
						<input type="text" name="loadTableColumn" <cfif len(form.loadTableColumn)>value="#htmleditformat(form.loadTableColumn)#"</cfif> >
					</td>
					<cfelse>
					<td>
						<select name="loadTableColumn">
							<cfloop query="columnsListQry">
								<option value="#columnName#" <cfif structKeyExists(form, "loadTableColumn") and form.loadTableColumn eq columnName>selected</cfif> >#htmleditformat(columnName)#</option>
							</cfloop>
						</select>
					</td>
					</cfif>
					
				</tr>
				<tr>
					<td align="right" colspan="3">
						<a href="javascript:document.flagForm.submit()">Submit</a>
					</td>
					
				</tr>
				<cfif isDefined("distinctQry")>
				<tr>
					<td colspan="3">
						<table>
						<cfloop query="distinctQry">
							<tr>
								<td>
									#htmleditformat(COLUMN_VALUE)# 
								</td>
								<td>
									<CF_INPUT type="checkbox" name="distinctValues" value="#COLUMN_VALUE#">
								</td>
								
							</tr>
						</cfloop>
						</table>
					</td>
				</tr>	
				</cfif>
			</table>
		</td>
		
		
		<cfif len(form.loadTable) and len(form.loadTableColumn)>
			<td valign="top" width="350px">
				<table border="1">
					<tr>
						<td colspan="2">						
							<cf_twoselectsrelated
								query="myObject.qFlagList"
								name1="frmProfileEntity"
								name2="frmFlagID"
								Selected1 ="#form.frmProfileEntity#"
								display1="profileEntity"
								display2="thisFlag"
								value1="flagGroupID"
								value2="FlagID"
								forcewidth1="30"
								forcewidth2="60"
								size1="1"
								size2="auto"
								autoselectfirst="Yes"
								emptytext1="(Select Profile Table)"
								emptytext2="(Select Profile Attribute)"
								forcelength2 = "6"
								HTMLBetween = "#textBetween#"
								formname = "flagForm">
						</td>
					</tr>
					<tr>
						<td align="right" colspan="2">
							<a href="javascript:if(document.flagForm.frmProfileEntity.value!=''){document.flagForm.createFlag.value=1;document.flagForm.submit();}else alert('Please select Profile Table')">Create a new flag</a>
						&nbsp;&nbsp;&nbsp;
							<a href="javascript:mapValues()">Map values to flag</a>
						</td>
					</tr>
				
					<cfif form.createFlag>
						<tr>
							<td >
								Flag group:
							</td>
							<td >
								<select name="flagGroupForNewFlag">
									<cfloop query="flagGroupQry">
										<option value="#flagGroupID#">#htmleditformat(name)#</option>
									</cfloop>
								</select>
							</td>
						</tr>
						<tr>
							<td >
								Flag text ID:
							</td>
							<td >
								<input name="flagTextID" type="text">
							</td>
						</tr>
						<tr>
							<td >
								Flag name:
							</td>
							<td >
								<input name="flagName" type="text">
							</td>
						</tr>
						
						<tr>
						<td align="right" colspan="2">
							<a href="javascript:if(document.flagForm.flagTextID.value!='' && document.flagForm.flagName.value!=''){document.flagForm.submitFlag.value=1;document.flagForm.submit()}else alert('Either Flag Text ID or Flag name or both are missing')">Submit the new flag</a>
						</td>
					</tr>
				</cfif>
				</table>
			</td>
		</cfif>
		</tr>
</TABLE>

<input type="hidden" name="submitMapping" value="0">
<input type="hidden" name="createFlag" value="0">
<input type="hidden" name="submitFlag" value="0">

</form>

</cfoutput>
