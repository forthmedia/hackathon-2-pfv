<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent >

	<cffunction name="getDetails" access=remote returnType="struct">

		<cfif isTrusted()>
			<cfset result =  {isOK=true,path = getTemplatePath()}>
		<cfelse>
			<cfset result =  {isOK=false}>
		</cfif>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="isTrusted" access=private returnType="boolean">
		<!--- had problems with this because if a machine has two network cards we sometimes find that a request to itself comes from one card and arrives at the other, so my isTrusted test fails 
		also problems with requests going out through switches and things
		--->
		<cfset var isTrustedResult = false>
		<cfset var IpaddressesAndSubnetArray = getIpaddressesAndSubnets()>

			<cfloop index="AddressAndSubnet" array = "#IpaddressesAndSubnetArray#">
				<cfif areIpAddressesOnSameSubnet (AddressAndSubnet.address,cgi.remote_host, AddressAndSubnet.mask )>
					<cfset isTrustedResult = true>
					<cfbreak>
				</cfif>
			</cfloop>

			<cfif not isTrustedResult>
				<!--- is the IPaddress the same as the IPAddress of the domain --->
				<cfif CreateObject("java", "java.net.InetAddress").getByName(HTTP_HOST).getHostAddress() is cgi.remote_host>
					<cfset isTrustedResult = true>
				</cfif>
			</cfif>
			
		<cfreturn isTrustedResult>
		
	</cffunction>



	<cffunction name="getIpaddressesAndSubnets" access=private returnType="array">
	
		<cfset var result = arrayNew(1)>
		<cfset var regExpResultArray = "">
		<cfset var IPConfigString = "">
		<cfset var regExpObj = createObject("component","com.regExp")>
	
		<cfexecute
			name = "C:\WINDOWS\system32\ipconfig.exe"
			variable ="IPConfigString"
			timeout = "5"
		/>

		<cfset regExpResultArray = regExpObj.refindAllOccurrences ("Address.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}).*?Mask.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})",IPConfigString)>

			<cfloop array="#regExpResultArray#" index="i">
				<cfset tempStruct = {address=i[2],mask=i[3]}>
				<cfset arrayAppend (result,tempStruct)>
			</cfloop>
			
		<cfreturn result>
		
	</cffunction>
		
		
	<cffunction name="areIpAddressesOnSameSubnet" access=private returnType="boolean">
		<cfargument name = "IPAddress1" required="true">	
		<cfargument name = "IPAddress2" required="true">
		<cfargument name = "subnetMask" default="255.255.255.0">			
			
		<cfset var result = true>

			<cfloop index="i" from ="1" to = "4">
				<cfif bitAND (listgetat(ipaddress1,i,"."), listgetat(subNetMask,i,".")) is not bitAND (listgetat(ipaddress2,i,"."), listgetat(subNetMask,i,".")) >
					<cfset result = false>
					<cfbreak>
				</cfif>
			</cfloop>

		<cfreturn result>
		
	</cffunction>



</cfcomponent>


