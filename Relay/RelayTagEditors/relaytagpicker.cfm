<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Code for selecting relaytags and other things to be inserted into documents (specifically KTML at moment)

Developed by NM to insert relaytags
Modified by WAB to deal with inserting links and merge fields into communications

SB	05-02-16	Reworked layout for Bootstrap

requires  relayInsertType    commLink - <A> links in communications
							 commerge  - plain merge fields in communications
							 relayTag - original rlayTags

		returnFunction   - the javascript function to be executed to do the insert.  can contain the word *TAGTEXT* which is replaced with the code to be added to the document

 --->


<!---

	commlinks and commmerge come from application.emailMergeFields and need to be separated out
	still slightly nasty
 --->

<cfparam name="allInOneEditor" default="true">
<cfparam name="form.groupTagsBy" default="moduleName">
<cfparam name="form.orderTagsBy" default="name">
<cfparam name="pickerHeight" default="200">
<cfparam name="editorHeight" default="200">
<cfparam name="relayInsertType" default="relayTag">
<cfparam name="context" default="element">

<cfif relayInsertType is "commmerge">
	This needs updating to use a query<CF_ABORT>
	<cfset tagStructure = duplicate(application.emailMergeFields)>
	<cfset Title = "Choose a Relay Merge Field to Insert">
	<cfloop collection="#tagStructure#" item="tag">
		<cfif tagStructure[tag].showInLinkManager>
			<cfset x = structdelete(tagstructure,tag)>
		</cfif>
	</cfloop>
	<cfset keyList = ArrayToList( StructSort( tagstructure, "numeric", "ASC", "sortIndex" ) )>

<cfelseif relayInsertType is "commlink">
	This needs updating to use a query<CF_ABORT>
	<cfset tagStructure = duplicate(application.emailMergeFields)>
	<cfset Title = "Choose a Relay Link to Insert">
	<cfloop collection="#tagStructure#" item="tag">
		<cfif not tagStructure[tag].showInLinkManager>
			<cfset x = structdelete(tagstructure,tag)>
		</cfif>
	</cfloop>
	<cfset keyList = ArrayToList( StructSort( tagstructure, "numeric", "ASC", "sortIndex" ) )>

<cfelseif relayInsertType is "mergeFields">

	<cfset moduleName = "">
	<cfset moduleID = 0>

	<cfset tagQuery = application.com.relayTags.getMergeFieldsAndFunctions(context=context)>

	<cfquery name="tagQuery" dbtype="query">
		select *, insertName as tag, lower(insertName) as lowerInsertName  from tagQuery
		<cfif structKeyExists(form,"searchString")>where (lower(insertName) like '%#lcase(form.searchString)#%' or lower(insertName) like '%#lcase(form.searchString)#%')</cfif>
		order by #form.groupTagsBy#, entityTypeID, lowerInsertName
	</cfquery>
	<cfset Title = "Choose a Relay Merge Field to Insert">

<cfelse>
	<cfset detailedTagQuery = application.com.relayTags.addModuleNameToQuery(application.activeRelayTagQuery)>
	<cfquery name="tagQuery" dbtype="query">
		select	distinct lower(name) as lowerCaseName,'' as entityTypeID,*
		from		detailedTagQuery
		<cfif structKeyExists(form,"searchString")>where (lower(tag) like '%#lcase(form.searchString)#%' or lower(description) like '%#lcase(form.searchString)#%')</cfif>
		order by tag	<!--- <cfif form.orderTagsBy eq "name">lowercaseName<cfelse>#form.orderTagsBy#</cfif> --->
	</cfquery>
	<!--- <cfset tagQuery = application.activeRelayTagQuery> --->
	<cfset Title = "Choose a Relay Tag to Insert">

</cfif>


<cf_title><cfoutput>#Title#</cfoutput></cf_title>

<cfsavecontent variable="htmlHeadScript">

<script>
var returnFunction = "<cfoutput>#jsStringFormat(returnFunction)#</cfoutput>"

function insertTag() {
	var relayTagSelect = document.getElementById("relayTags");
	var tagName = relayTagSelect.options[relayTagSelect.selectedIndex].value;
	var tagDetails = tags[tagName]

	if (tagDetails.editorExists !=1) {
		insertAndClose(tagDetails.insertCode);
	} else {
		winToOpen = iDomainAndRoot + tagDetails.editorPath;
		conjunction = (winToOpen.indexOf('?') == -1)? '?':'&' ;
		winToOpen += conjunction + 'relayTag='+tagDetails.insertCode+'&formname=attributes.formname&textAreaName=attributes.textAreaName&isEdit=false';
		winToOpen += '&returnFunction='+returnFunction

		window.location.href = 	winToOpen
	}

}

function insertTagTableVersion() {
	var relayTagItem = document.getElementById("selectedTag");
	var tagName = relayTagItem.value;
	var tagDetails = tags[tagName]
	//alert(tagDetails.toString());
	if (tagDetails.editorExists !=1) {
		insertAndClose(tagDetails.insertCode);
	} else {
		winToOpen = iDomainAndRoot + tagDetails.editorPath;
		conjunction = (winToOpen.indexOf('?') == -1)? '?':'&' ;
		<cfif relayInsertType neq "mergeFields">
			relayInsertType='relayTag';
		<cfelse>
			relayInsertType='relayFunction';
		</cfif>
		winToOpen += conjunction + relayInsertType+'='+tagDetails.insertCode+'&formname=attributes.formname&textAreaName=attributes.textAreaName&isEdit=false';
		winToOpen += '&returnFunction='+returnFunction
		<cfif allInOneEditor eq "true">
			winToOpen += "&calledFromIFrame=true";
			openCloseEditorWindow("show",winToOpen);
		<cfelse>
			window.location.href = 	winToOpen
		</cfif>
	}

}

function insertAndClose(str) {


	str = str.replace('<','{{')
	str = str.replace('>','}}')

	returnFunction = returnFunction.replace("*TAGTEXT*",str);
	returnFunction = returnFunction.replace("*TAGTEXTARRAY*",'[\''+str+'\']');

	success = eval(returnFunction);

//	ktml.relayTagInsert (str)


//	ktml.insertHTML('<span class=relayTag>' + str.replace(/</,"{{").replace(/>/,"}}") + '</span>');
	// WAB 2006-06-20
	// for some reason when we do these inserts the hndlr_onblur does not fire the next time focus leaves ktml
	// this means that the underlying form field is not updated.  This code below does this for us
	// ktml.formElement.value = window.opener.HandleOutgoingText(ktml)
//	ktml.hndlr_onblur();
	if (success) {
	//	window.opener.isKTMLEdit = false;
		window.close();
	}
}

<!--- WAB 2006-09-18 added a javascript array with the information on each tag.
	used to display "help text" and to control insert behaviour (whether it displays an editor or not)
	 --->

<!--- javascript structure of information on the individual tags --->
<cfoutput>
	var tags = new Array();
		<cfloop query = "tagQuery" >
			var thisTag = new Array();
				thisTag.editorExists = #jsStringFormat(editorExists)#
				thisTag.id = '#jsStringFormat(entityTypeID)#_#replaceNoCase(tag,"'","\'","ALL")#'
				thisTag.name = '#replaceNoCase(insertNAme,"'","\'","ALL")#'
				thisTag.insertCode = '#replaceNoCase(CleanHTMLCode,"'","\'","ALL")#'
				thisTag.description = '#replaceNoCase(replaceNoCase(description,"'","\'","ALL"),chr(13)&chr(10),"\n","ALL")#'
				thisTag.editorPath = '#jsStringFormat(editorPath)#'
				tags[thisTag.id] = thisTag;
		</cfloop>
</cfoutput>

	function changeTag(tagName) {

		var helpTextObj = document.getElementById('helpText')
		var HasEditorObj = document.getElementById('HasEditor')
		helpTextObj.innerHTML = tags[tagName].description
		var tagDetails = tags[tagName]
	//alert(tagDetails.toString());
		if (tagDetails.editorExists !=1) {
			HasEditorObj.innerHTML = '' ;
		} else {
			HasEditorObj.innerHTML = 'Editor Available' ;
		}

		return true

	}

	function changeTagFromTableVersion(tagName) {
		var relayTagItem = document.getElementById("selectedTag");
		previousrelayTagItem = relayTagItem.value
		relayTagItem.value = tagName; ;
		if (previousrelayTagItem != '') {
			// un-highlight any existing selected
			highlightRow(previousrelayTagItem,document.getElementById('row_'+previousrelayTagItem),'off')
		}
			highlightRow(tagName,document.getElementById('row_'+tagName),'on')


		var helpTextObj = document.getElementById('helpText')
		helpTextObj.innerHTML = tags[tagName].description
		//if (tags[objValue].editorExists) {helpTextObj.innerHTML += '<BR>Has editor' }

		openCloseEditorWindow("hide");

		var relayTagItem = document.getElementById("selectedTag");
		var tagDetails = tags[tagName]
		//alert(tagDetails.toString());
		if (tagDetails.editorExists == 1) {
			insertTagTableVersion();
		} else {
			insertTagTableVersion();		// wab added - click on item without an editor inserts it
		}
		return true

	}

	function openCloseEditorWindow(doWhat,srcFile) {
		if (doWhat == "show") {
			var editorWindow = document.getElementById("tagEditor");
			editorWindow.src = srcFile;
			editorWindow.style.display = "block";
			var editorWindowWrapper = document.getElementById("tagEditorWrapper");
			editorWindowWrapper.style.display = "block";
			document.getElementById("row_insertButton").style.display = "block";

		} else if (doWhat == "hide") {
			var editorWindow = document.getElementById("tagEditor");
			editorWindow.style.display = "none";
			editorWindow.src = "loadingEditor.cfm";
			var editorWindowWrapper = document.getElementById("tagEditorWrapper");
			editorWindowWrapper.style.display = "none";
			document.getElementById("row_insertButton").style.display = "none";

		}
	}

	function submitTagFromIFrame() {
		//var editorWindow = document.getElementById('tagEditor');
		if (window.frames.tagEditor.doSubmit){
			window.frames.tagEditor.doSubmit(window.frames.tagEditor.document.forms[0]);    // relaytag editor
		} else 	{
			window.frames.tagEditor.FormSubmit()    // xml editor
		}
		//editorWindow.doSubmit(editorWindow.document.forms[0]);
	}

	function onInsertFunction() {
		if (document.getElementById('tagEditor').style.display=='block'){
			submitTagFromIFrame();
			document.getElementById('btnReplace').enabled=false;
		} else {
			insertTagTableVersion();
		}
	}

	function highlightRow(rowID,obj,doWhat,theEvent) {

		var relayTagItem = document.getElementById("selectedTag");

		if (doWhat == 'on' || relayTagItem.value == rowID) {
			jQuery(obj).addClass('tagSelected');
		} else {
			jQuery(obj).removeClass('tagSelected');
		}

		if (doWhat == "off") {
			if (theEvent){
				if (theEvent.relatedTarget) {
					nextObject = theEvent.relatedTarget
				} else {
					nextObject = theEvent.toElement
				}

				if (nextObject)
				{
					// test to see if moving to another tag row.
					re = /.*rowTD.*/
					if (! re.test(nextObject.id)) {
						// if not moving to another tag row and a tag has been selected then put its help text up
						if (relayTagItem.value != '') {
							changeTag (relayTagItem.value)
						}
					}

				}

			}

		}
	}


	<!--- <cfoutput>
	//window.resizeTo(#jsStringFormat(tableWidth)# + 40,#jsStringFormat(editorheight)# + #jsStringFormat(pickerHeight)# + 250)  // arbitrary pluses
	</cfoutput> --->
	</script>

</cfsavecontent>

<cfhtmlhead text="#htmlHeadScript#">

<cfform name="relayTagSorting"  method="post">
<cfoutput>
<input type="hidden" id="selectedTag" name="selectedTag" value="">
	<div class="form-group">
		<!--- <th style="text-align: right;">
				phr_groupBy:
				<select name="groupTagsBy" onchange="document.relayTagSorting.submit()">
					<option value="moduleName" selected>Module Name</option>
					<option value="moduleID" selected>Module ID</option>
				</select>
			</th>
			<th style="text-align: right;">
				phr_orderBy:
				<select name="orderTagsBy" onchange="document.relayTagSorting.submit()">
					<option value="tag" <cfif form.orderTagsBy eq "tag">selected</cfif>>Tag Name</option>
					<option value="name" <cfif form.orderTagsBy eq "name">selected</cfif>>Tag Descriptive Name</option>
				</select>
			</td>--->
		<label>#htmleditformat(Title)#</label>
		<div class="input-group">
			<input type="text" onblur="this.value=(this.value=='') ? 'Search' : this.value;" onfocus="this.value=(this.value=='Search') ? '' : this.value;" placeholder="Search" name="searchString" class="searchBoxAlt form-control" maxlength="50">
			<span id="searchButtonRelaytag" class="input-group-addon">
				<input type="image" src="/images/searchHeaderIcon.png" alt="Search" />
			</span>
		</div>
	</div>

	<div id="relayTagPicker" style="height:#pickerHeight#px;">
		<table id="relayTagTable" class="table table-hover table-striped">
			<tr class="header">
				<th>Descriptive Name</th>
				<th>Tag Name</th>
			</tr>
			</cfoutput>
			<cfset currentEntityTypeID = -1>
			<cfloop query="tagQuery">
			<!--- <cfset moduleDetail = XmlSearch(application.menuXMLdoc, "//MenuItem[@id=#ModuleID#]/@ItemName")>
			<cfif arrayLen(moduleDetail) gt 0>
				<cfset moduleName = application.com.relayTranslations.translatePhrase("phr_#replace(LTrim(moduleDetail[1].xmlValue),' ','_','ALL')#")>
			<cfelse>
				<cfset moduleName = application.com.relayTranslations.translatePhrase("phr_general")>
			</cfif> --->
			<!--- <tr>
				<td colspan="2"><b><u>#htmleditformat(moduleName)#</u></b></td>
			</tr> --->

				<cfoutput <!--- group="entityTypeID" --->>
				<cfif relayInsertType eq "mergeFields" and entityTypeID neq "" and entityTypeID neq currentEntityTypeID>
					<tr><td colspan="2"><b>#application.entityType[entityTypeID].tablename#</b></td></tr>
					<cfset currentEntityTypeID = entityTypeID>
				</cfif>

				<tr id="row_#tagQuery.entityTypeID#_#tag#" class="trNormal" onclick="changeTagFromTableVersion('#tagQuery.entityTypeID#_#htmleditformat(tag)#');" onmouseover="highlightRow('#tagQuery.entityTypeID#_#tag#',this,'on');changeTag('#tagQuery.entityTypeID#_#htmleditformat(tag)#')" onmouseout="highlightRow('#tagQuery.entityTypeID#_#tag#',this,'off',event);">
					<td id="rowTD1_#tagQuery.entityTypeID#_#tag#">
						#htmleditformat(Description)#
					</td>
					<td id="rowTD2_#tagQuery.entityTypeID#_#tag#">
						#htmleditformat(Tag)#
					</td>
				</tr>
				</cfoutput>
				<!--- </cfoutput> --->
			</cfloop>
			<cfoutput>
		</table>
	</div>

			<!--- <select name="relayTags" id="relayTags" size="10" style="width:100%;" onChange="javascript:changeTag(this)">
				<cfloop query="tagQuery">
					<option value="#htmleditformat(tag)#">#InsertName#
				</cfloop>
			</select> --->


		<div id="helpText" class="pull-left"></div>
		<div id="HasEditor" class="pull-right"></div>



	<div id="tagEditorWrapper" style="display:none;">
		<iframe id="tagEditor" id="tagEditor" name="tagEditor" src="loadingEditor.cfm" style="display:none; height: #htmleditformat(editorHeight)#px;" frameborder="0"></iframe>
	</div>


	<div id="row_insertButton" style="display:none">
		<input type="button" name="btnReplace" id="btnReplace" onClick="onInsertFunction()" value="Insert" class="btn btn-primary" />
	</div>
</cfoutput>
</cfform>