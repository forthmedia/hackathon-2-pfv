
jQuery(document).ready(
	function() {
		toggleInfinityScrollFields();
		toggleListView();
		maximumDataShowListItems();
});	

function toggleInfinityScrollFields() {
	
	var showInfinityScrollFields = true;
	if (jQuery('#PagingType option:selected').val() == 'Normal') {
		showInfinityScrollFields = false;
	}
	
	jQuery('#scrollFor_formgroup').toggle(showInfinityScrollFields);
}

function toggleListView() {

	var showListFields = true;
	if (!jQuery('#disableListView_No').is(":checked")) {
		showListFields = false;
	}

	jQuery('#defaultView_formgroup, #DataShowList_formgroup').toggle(showListFields);
}

function maximumDataShowListItems() {
	var currentLabel = jQuery('#DataShowList_formgroup button span').text();
	jQuery('#DataShowList_formgroup').click(function(event) {
	jQuery('#DataShowList_formgroup .ms-select-all').remove();
		if(jQuery('#DataShowList :selected').length > 12 && (jQuery(event.target).is("input") && jQuery(event.target).prop("checked"))){
			event.preventDefault();
			alert('You cannot choose more than 12 data items to show in List View.');
			jQuery('#DataShowList option[value='+ jQuery(event.target).prop("value") +']').prop("selected", false);
			jQuery('#DataShowList_formgroup button span').text(currentLabel);
		}
		currentLabel = jQuery('#DataShowList_formgroup button span').text();
	});
}
