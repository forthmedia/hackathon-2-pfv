
jQuery(document).ready(function() {toggleDomainMatchFields();});	

function toggleDomainMatchFields() {

	var showDomainFields = true;
	if (jQuery('#domainMatch_No').is(":checked")) {
		showDomainFields = false;
	}

	jQuery('#restrictDomainMatchByCountry_formgroup, #allowUnknownDomain_formgroup').toggle(showDomainFields);
	toggleAllowNewAddressField();
}

function toggleAllowNewAddressField() {
	var showAllowNewAddress = true;
	if (jQuery('#allowUnknownDomain_Yes').is(":hidden") || jQuery('#allowUnknownDomain_No').is(":checked")) {
		showAllowNewAddress = false;
	}
	jQuery('#allowNewAddress_formgroup').toggle(showAllowNewAddress);
}