jQuery(document).ready(function(){

  // ---------------------------------------------------------
  //    Organisation Scheme Entity ID and stuff     ¯\_(ツ)_/¯
  //    
  //    PERSON == 0 / LOC == 1 / ORG == 2  
  // ---------------------------------------------------------

  // Organisation Scheme detector
  var $schemeDropdown = jQuery('#SchemeID');
  var $rankBox = jQuery('#showRank_formgroup');
  var baseUrl = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=activityScoreWS&methodName=getScoringEntityTypeIDForScheme&returnFormat=json&schemeID=';
  const personId = 0;
  const locationId = 1;  

  // Removing 'Select a Value'
  if ($schemeDropdown.children().length){
    $schemeDropdown.children()[0].remove();}

  // Initial setup
  jQuery.ajax({
    url: baseUrl+$schemeDropdown.val(),
      success: function(data){        
        var mydata = JSON.parse(data);        
        if (mydata.ENTITYTYPEID == personId || mydata.ENTITYTYPEID == locationId) {$rankBox.css('display','block');}               
      }
    }); 

  // On Dropdoan change      
  $schemeDropdown.change(function(){ 
    jQuery.ajax({
      url: baseUrl+$schemeDropdown.val(),
      success: function(data){
        var mydata = JSON.parse(data);        
        if (mydata.ENTITYTYPEID == personId || mydata.ENTITYTYPEID == locationId) {$rankBox.css('display','block');         
        } else {$rankBox.css('display','none');}      
      }
    });     
  });

  // ---------------------------------------------------------
  //    Custom Colour input field handler              m(_ _)m   
  // ---------------------------------------------------------
  var $widgetStyleDropDown =  jQuery('#widgetstyle'); 
  var $widgetThemeDropDown = jQuery('#widgettheme');
  var $themeColourGroup = jQuery('#themecolour_formgroup');
  var $themeColourGroup = jQuery('#themecolour_formgroup');
  // Custom colour field handler    
  if ($widgetThemeDropDown.val() == 'Custom') {$themeColourGroup.css('display','block');}    
  $widgetThemeDropDown.change(function(){      
    if (jQuery(this).val() == 'Custom') {$themeColourGroup.css('display','block');
    } else {$themeColourGroup.css('display','none');}});
  // Removing 'Select a value' fomr the dropdowns
  if ($widgetStyleDropDown.children().length){
    $widgetStyleDropDown.children()[0].remove();}
  if ($widgetThemeDropDown.children().length){
    $widgetThemeDropDown.children()[0].remove();}

});

