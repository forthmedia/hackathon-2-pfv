<!--- �Relayware. All Rights Reserved 2014 --->
<cfif not isDefined("fieldsToUse")>
	System does not know which fields to use.
<cfelse>

	<!--- decide what the separator is for the list of parameters --->
	<cfif tagType is "relayTag">
		<cfset separator = " ">
	<cfelseif tagType is "relayFunction">
		<cfset separator = ",">
	</cfif>
	
	<cfif listcontainsnocase(form.fieldnames,"relay_if")>
		<cfset updatedRelaytag = "">
		<cfif isDefined("relay_if")>
			<cfset updatedRelaytag = "{{relay_if #relay_if#}}">
			<cfif isDefined("relay_if_elseif1") and relay_if_elseif1 is not "">
				<cfset updatedRelaytag = listappend(updatedRelaytag,"{{relay_elseif #relay_if_elseif1#}}",application.delim1)>
			</cfif>
			<cfif isDefined("relay_if_elseif2") and relay_if_elseif2 is not "">
				<cfset updatedRelaytag = listappend(updatedRelaytag,"{{relay_elseif #relay_if_elseif2#}}",application.delim1)>
			</cfif>
			<cfif isDefined("relay_if_else") >
				<cfset updatedRelaytag = listappend(updatedRelaytag,"{{relay_else}}",application.delim1)>
			</cfif>
			<cfif isDefined("relay_if_endif") >
				<cfset updatedRelaytag = listappend(updatedRelaytag,"{{/relay_if}}",application.delim1)>
			</cfif>
		</cfif>

		<cfset updatedRelayTagJS = jsstringFormat(updatedRelayTag)>	
		<!--- special case for Relay_If where the parameters are not named, just have the condition --->	
	<cfelse>
		<!--- loop through all the fields putting name value pairs into a string 
			makes variable tagParams
		--->
		<cfif not isDefined("tagParams")><!--- special case when editing a relayif the parameter is unnamed and instead we pass through the whole tagParams value--->
			<cfset tagParams = "">
			<cfloop list = "#fieldsToUse#" index="formElement">
				<cfif 	structKeyExists(form,formElement) and form[formElement] neq "" 
						and (not structKeyExists (form,"#formElement#_default") OR form[formElement] NEQ form["#formElement#_default"])>
		<!--- 			<cfset tagParams = tagParams & ' ' & formElement & '=' & replaceNoCase(replaceNoCase(replaceNoCase(form[formElement],"'","\'","all"),'"','""','all')," ","_","all")> --->
					<!--- WAB 2006-05-03 rather than converting spaces to _ we can now quote all the variables - may be possible to chose the most appropriate quotes but for the moment just use "  --->
					<cfset value = form[formElement]>
<!--- 	done later so that cut and paste version is still correct				<cfset value = replaceNoCase(value,"'","\'","all") > --->
					<!--- 			<cfset value = replaceNoCase(value,'"','""',"all") > don't think this is needed with proper qualifiers--->
					<cfif tagType is "relayTag">
						<cfif value contains '"'>
							<cfset qualifier="'">
						<cfelseif value contains " " or value contains "'" >				
							<cfset qualifier='"'>
						<cfelse>
							<cfset qualifier=''>
						</cfif> 
					<cfelseif tagType is "relayFunction">
						<cfif isNumeric(value)>
							<cfset qualifier=''>
						<cfelse>
							<cfset qualifier="'">
						</cfif>
					</cfif>
					
					<cfset tagParams = listappend(tagParams, formElement & '=' & qualifier & value & qualifier, separator )> 
						
				</cfif>
			</cfloop>
			
		</cfif>

		<!--- put tag and parameters together to create variable updatedRelayTag--->
		<cfif tagType is "relayTag">
			<cfset updatedRelayTag = tagName & " " & tagParams>
			<cfset updatedRelayTag = "{{" & updatedRelayTag & "}}">
			<cfif isdefined("requiresEndTag")>
				<!--- slightly nasty here, we add on the end tag separated with ','  the opening and closing ' are already supplied; the function expects an array --->
				<cfset updatedRelayTag = updatedRelayTag & "','" & "{{/" &  tagName & "  }}">
			</cfif>
		<cfelseif tagtype is "relayFunction">
			<cfset updatedRelayTag = "[[" & tagName & "(" & tagParams & ")]]">
		</cfif>
		<cfset updatedRelayTagJS = jsstringFormat(updatedRelayTag)>

	</cfif>


	<cfif frmAction is "update">

		<cfif isdefined("returnFunction") and returnFunction is not "">
			<cfset updatedRelayTagAsArray = "['" & replace(updatedRelayTagJS,application.delim1,"','","ALL") & "']">
			<cfset returnFunction = replaceNoCAse(returnFunction,"*TAGTEXT*",updatedRelayTagJS)>
			<cfset returnFunction = replaceNoCAse(returnFunction,"*TAGTEXTARRAY*",updatedRelayTagAsArray)>
			<cfif isDefined("LinkText")>
				<cfset returnFunction = replaceNoCAse(returnFunction,"*LINKTEXT*",linkText)>
			</cfif>
	
			<cfoutput>
			<!--- output the updated tag incase return function fails --->
			Insert has failed, you can either copy the tag and paste it inside the textarea:<br>
			#htmleditformat(updatedRelayTag)#<BR>
			or<br>
			<a href="javascript:document.location.reload();">Retry</a>
			<SCRIPT type="text/javascript">
			if (parent.document.getElementById('tagEditor') != null) {
				x = parent.#replaceNoCase(replaceNoCase(replaceNoCase(returnFunction,"*TAGTEXT*",updatedRelayTagJS),"opener.","parent.opener.","all"),"window.","parent.","all")#;
				if (x==true) {
					parent.close();
					parent.opener.focus();
				}
			} else {
				x = #replaceNoCase(returnFunction,"*TAGTEXT*",updatedRelayTagJS)#;
				if (x==true) {
					window.close();
					opener.focus();
				}
			}
			
			
			</script>
	
		
			</cfoutput>
			<CF_ABORT>
		<cfelse>
		
			<cfoutput>
			No return function.  <BR>
			#htmleditformat(updatedRelayTag)#
			</cfoutput>
		</cfif>
	
	
		<cfoutput>
	 		<a href="javascript:document.location.reload(false);">retry</a>
		</cfoutput>
		<CF_ABORT>
	<cfelse>

		<cfif tagType is "relayTag">
			<cfset relayTag = updatedRelayTag> 
		<cfelseif tagtype is "relayFunction">
			<cfset relayFunction = updatedRelayTag>
		</cfif>
	
	</cfif>

</cfif>
