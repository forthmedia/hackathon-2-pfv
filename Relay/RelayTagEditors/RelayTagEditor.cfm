<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="true">

<!---
File name:			relayTagEditor.cfm
Author:				WAB from work by SWJ
Date started:		2005-09-14

Description:		File called to edit relay tags.
					It creates the headers, footers and forms and includes the relevant editor file

Amendment History:
					Date (DD-MMM-YYYY)	Initials 	What was changed

					20-SEP-2005	NM	Added "isEdit" to form action url params.
					2006-05-03  WAB changed code so that all the tag parameters are passed as a single string which is parsed here, rather than in the javascript.
								Doesn't lose parameters not handled by the editor
					2007-03-14	NM Added checks for iframe call and adjusted layout etc. accordingly.

					2011/11/28  WAB Wholesale change of editor technology to use CF_PARAM and relayFormElement directly
					2012-08-23	IH	Add focus on load
					2014-10-14	WAB	Core-856  Yes/No Radio buttons are not correctly selected with default value
								Store Booleans as Yes/No rather than 0/1 in the Tag
					2016/02/26	Couple of small changes. Look for js file for the tag editor and include it. Also output the field description as the help text to provide online help.
					2016/03/31	RJT	BF-614 where this is a merge field with an editor tag.cfm may not exist, don't include the js either in this case
Possible enhancements:


 --->

<cf_body onload="window.focus()"/>

<cfparam name="url.calledFromIFrame" default="false">
<cfset actionInsert = "Insert">
<cfif isDefined('isEdit') and (#URL.isEdit#)>
	<cfset actionInsert = "Update">
</cfif>

<cfif isDefined("frmAction")>
	<cfinclude template = "relayTagEditorSubmit.cfm">
</cfif>


<!---
This first section deals with
i) parsing the incoming relayTag/relayfunction
ii) looking up the relay tag in the relaytag structure and getting editor information
	or
	looking up the relay function in the merge functions cfc and getting editor information

Ends up with
	tag.name
	tag.description
	tag.type  (relayFunction|relayTag)
	tag.editorExists
	tag.editorInsertPath (points to a custom editor if used)
	tag.cf_params - define the editor
	parameterCollection - incoming parameters in the tag
 --->


<cfif isDefined("relayFunction")>
	<!--- parse out the parameters and function name passed in on the URL --->
	<cfset parseResult = application.com.regexp.parseRelayFunction(relayFunction)>
	<cfset ParameterCollection = parseResult.params>
	<cfset tag.Name = parseResult.name>
	<cfset tag.Type = "relayFunction">


	<cfif parseResult.name is "">
		No Function Found
		<CF_ABORT>
	<cfelse>


		<cfset testEditorName = "function_#parseResult.name#_editor.cfm">

		<cfif (fileExists(application.paths.relay & "\relayTagEditors/" & testEditorName))>

				<cfset tag.editorExists = "1">
				<cfset tag.editorPath = "/relayTagEditors/relayTagEditor.cfm">
				<cfset tag.editorInsertPath = testEditorName>
				<cfset tag.Description = "">
		<cfelse>

				<cfset mergeFunctionsCFC = application.com.relayTranslations.getMergeObject()>
				<cfset functionMetaData=application.com.relayTags.getEditorXMLFromCFCFunction(cfc=mergeFunctionsCFC,functionname=parseResult.name)>

				<!--- WAB 2013-07-04 Deal with what happens if function has unnamed arguments.  Get the names of the arguments from the metadata and substitute them in the correct order --->
				<cfif structKeyExists(parseResult,"unnamedArguments") and parseResult.unnamedArguments>
					<cfset loopTo = min (listLen(parseResult.paramListOrdered),arraylen(functionMetaData.parametersArray))>
					<cfloop index="i" from="1" to="#loopTo#">
						<cfset realParamName = functionMetaData.parametersArray[i].name>
						<cfset tempParamName = listGetAt(parseResult.paramListOrdered,i)>
						<cfset parseResult.params[realParamName] = parseResult.params[tempParamName] >
						<cfset structDelete(parseResult.params,tempParamName )>
					</cfloop>
				</cfif>

				<cfset tag.Description = functionMetaData.description>

				<cfif not functionMetaData.functionExists>
					This function does not exist<CF_ABORT>
				<cfelse>
					<cfset tag.editorXML = functionMetaData.editorXML>
					<cfif tag.editorXML is "">
						<!--- sorry this is a bit of a hack, might need somethin better in the long term --->
						<cfset tag.editorExists = 0>
					<cfelse>
						<cfset tag.editorExists = "1">
					</cfif>
				</cfif>
		</cfif>

	</cfif>

<cfelseif isDefined("relaytag")>

		<cfset relaytag = replaceNoCase (replaceNoCase (relaytag,"{{","<"),"}}",">") >  <!--- WAB added while developing KTML --->
		<!--- generate the name of the editor filefrom the name of the tag file (variable relaytag isin form   <RELAY_xxxxxx> in it --->
		<cfset parseResult = application.com.regExp.parseRelayTag (relayTag)>
		<cfset tagName = parseResult.name>
		<cfif structKeyExists (application.RelayTagKeyedByTagName,tagName)>
			<cfset tag = application.RelayTagKeyedByTagName[tagName]>
		<cfelse>
			<cfoutput>#tagName# does not exist in Tag Structure</cfoutput><CF_ABORT>
		</cfif>

		<cfset tag.Type = "relayTag">
		<cfset tag.Name = parseResult.name>

		<cfset parameterCollection = parseResult.params>

</cfif>

<cfif not tag.EditorExists>
	<cfoutput>No Editor Exists</cfoutput><CF_ABORT>
</cfif>

<!--- this loop creates local variables for all the parameters already in the tag --->
<cfloop item="variablename" collection = "#parameterCollection#">
		<cfset variables[variablename] = parameterCollection[variablename]>
</cfloop>

<!---
This section outputs the editor
Either
	Built from attributes of the tag/function attributes/arguments
Or
	By including a custom editor

Code could probably be rationalised into one
 --->

<cfif structKeyExists(tag,"EditorXML") and tag.EditorXML is not "">


	<!---
		This Section Builds an Editor from definition in the tag itself
		It uses the bleeding edge (unfinished?) rwFormValidation.js

	 --->
			<cf_includeJavascriptOnce template = "/javascript/checkObject.js">
			<cf_includeJavascriptOnce template = "/javascript/verifyObjectV2.js">
			<cf_includeJavascriptOnce template = "/javascript/rwFormValidation.js" translate="true">

			<cf_includeJavascriptOnce template = "/javascript/cf/cfform.js">
			<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">

			<!---BF-614 where this is a merge field with an editor tag.cfm may not exist, don't include the js either in this case --->
			<cfif structKeyExists(tag,"cfm")>
				<cf_includeJavascriptOnce template= "/relayTagEditors/js/#replace(tag.cfm,'.cfm','.js')#">
			</cfif>


			<cfoutput>
				<cfset application.com.request.setDocumentH1(text="#tag.Name# Editor")>

				<CF_RelayNavMenu pageTitle="#tag.Name#" thisDir="">
					<cfif url.calledFromIFrame eq "true">
						<cf_RelayNavMenuItem MenuItemText="Close Editor" CFTemplate="javascript:parent.openCloseEditorWindow('hide');">
					<cfelse>
						<cf_RelayNavMenuItem MenuItemText="#actionInsert#" CFTemplate="javascript:doSubmit(document.forms[0]);">
						<cf_RelayNavMenuItem MenuItemText="Close Editor" CFTemplate="javascript:window.close();">
					</cfif>
				</CF_RelayNavMenu>

				<form noValidate="true" action="?" method="post">
					<cf_relayFormDisplay applyValidationErrorClass=true showValidationErrorsInline=true autoRefreshRequiredClass=true>

					<cfif tag.Description is not "">
						<div><cfoutput>#application.com.security.sanitiseHTML(tag.Description)#</cfoutput><div>
					</cfif>
					<!--- Now use the editorXML to output all the form fields 	--->
					<cftry>
						<cfset xmldoc = xmlparse(tag.editorXML)>
						<cfcatch>
							<cfif application.com.relayCurrentSite.isEnvironment("Dev")>
								<cfoutput>#htmleditFormat(tag.editorXML)#</cfoutput>
								<cfdump var="#cfcatch#">
								<CF_ABORT>
							<cfelse>
								<cfoutput>#application.com.relayUI.message(message="Problem with Relay Tag Editor. Please contact support.",type="warning")#</cfoutput>
								<cfset application.com.errorHandler.recordRelayError_Warning(type="Relay Tag Editor",Severity="error",caughtError=cfcatch,WarningStructure=tag)>
							</cfif>
						</cfcatch>
					</cftry>
					<!---
						We are going to use the xmlfunctions.runFunctionAgainstNodes to traverse the XML
						This will run a function (outputField) against each node
						We need to get a few extra bits of information into and out of dthis function and we do this by using a structure called additionalArgs
						fieldsDone will record all the fields output
						Currentvalues is the value of each field, a the moment we have all the current values which have been passed in on the tag being edited
					 --->
					<cfset additionalArgs = {currentValues = parameterCollection,defaults=structNew(),fieldsDone = ""}>

					<!--- when we come to evaluate Condition attributes, these may refer to current values of fields
						we are going to use a merge struct for this
					 --->
					<cfset mergeStruct = structCopy(additionalArgs.currentValues) >
					<!--- we also may want to test settings using [[getSetting]] --->

					<!--- now create the merge object and intialise it, and pass it in on the additionalArgs structure --->
					<cfset mergeObject = application.com.relayTranslations.getMergeObject().initialiseMergeStruct(mergeStruct)>
					<cfset additionalArgs.mergeObject = mergeObject>

					<!--- WAB 2013-09-24 evaluate any default values (which might come from settings) - we were doing this later, but a new CF10 behaviour in QoQ means that we need to do them earlier --->
					<cfset application.com.xmlfunctions.runFunctionAgainstNodes (XMLNodeArray =xmldoc.editor.xmlchildren,function = evaluateDefaultValues, additionalArgs = additionalArgs)>

					<!--- make sure that each parameter has an entry in the currentValues collection  - default to "" or use the default defined in the XML--->
					<cfset application.com.xmlfunctions.runFunctionAgainstNodes (XMLNodeArray =xmldoc.editor.xmlchildren,function = defaultCurrentValuesToDefaultValue, additionalArgs = additionalArgs)>
					<!--- WAB 2014-10-14 Core-856 Need to add the current values to the merge struct again (now contains the defaults as well).  This is particularly important for evaluating conditions which might depend on the default value of a parameter --->
					<cfset mergeObject.initialiseMergeStruct(structCopy(additionalArgs.currentValues))>

					<!--- Now run the function outputField against every node --->
					<cfset application.com.xmlfunctions.runFunctionAgainstNodes (XMLNodeArray =xmldoc.editor.xmlchildren,function =outputField, additionalArgs = additionalArgs,runtwiceonParents = true)>

					<!--- look for params which were in the tag but were not output in editor --->
					<cfset fieldsDone = additionalArgs.fieldsdone>
					<cfset foundMissingParam = false>
					<cfloop collection = #parameterCollection# item = "parameter">
						<cfif listfindnocase(fieldsDone,parameter) is 0 and parameterCollection[parameter] is not "">
							<cfif not foundMissingParam>
								<cf_relayFormElementDisplay label= "Additional Parameters"  relayFormElementType="HTML" currentvalue="">
								<cfset foundMissingParam = true>
							</cfif>
							<cf_relayFormElementDisplay relayFormElementType="text" label= #parameter# fieldname="#parameter#" currentvalue="#parameterCollection[parameter]#">
							<cfset fieldsDone = listappend (fieldsDone,parameter)>
						</cfif>
					</cfloop>



					<input type="submit" name="submitButton" value="" style="display:none"> <!--- this field is hidden, but needed because I need something to click to fire the onSubmit listener (must be a better way) --->
					<CF_INPUT type="hidden" name="fieldsToUse" value="#application.com.globalFunctions.RemoveListDuplicates(list=fieldsDone)#"> <!--- NJH 2014/10/20 - remove duplicates as when we have a relaytag with conditions, we have two of the same variable, one of which shows depending on the condition. See conditionPhrase as an example. ConditionParameters was appearing twice --->
					<CF_INPUT type="hidden" name="tagType" value="#tag.Type#">
					<CF_INPUT type="hidden" name="tagname" value="#tag.Name#"> <!--- note tagname cannot be written as tagName - DOM conflict --->
					<input type="hidden" name="frmAction" value="update">
					<cfif isDefined("returnFunction")>
						<CF_INPUT type="hidden" name="returnFunction" value="#returnFunction#">
					</cfif>
					<!--- CASE 427013 NJH 2012/03/07 --->
					<cfif isDefined("relayfunction")>
						<CF_INPUT type="hidden" name="relayfunction" value="#relayfunction#">
					</cfif>
					<cfif isDefined("relayTag")>
						<CF_INPUT type="hidden" name="relayTag" value="#relayTag#">
					</cfif>


					</cf_relayFormDisplay>

				</form>
				<SCRIPT type="text/javascript">
					function doSubmit(formObj) {
						formObj.submitButton.click()
					}
				</script>


				<SCRIPT type="text/javascript">
					function doReload(formObj) {
						<!--- need to set action to refresh, and submit without validation, need to remove all the CF _required fields to prevent sever side validation error --->
						formObj.frmAction.value='refresh';
						requiredFields =formObj.select('[name $= "_required"]')
						requiredFields.each(function(item) {item.remove()});
						formObj.submit();
					}
				</script>

				</cfoutput>


			<!---
			These are the functions which are run against the XML using xmlfunctions.runFunctionAgainstNodes
			 --->
					<cffunction name="evaluateDefaultValues">
						<cfargument name="xmlnode">
						<cfargument name="additionalArgs">

						<cfscript>
							var convertBooleanToYesNo = function (value) {
								var result = iif (value,DE("Yes"),DE("No"));
								return result;
							} ;
						</cfscript>

						<cfif structKeyExists(xmlnode.xmlattributes,"default") and xmlnode.xmlattributes.default contains "##">
							<cfset tempDefault = rereplacenocase(xmlnode.xmlattributes.default,"##application.com.settings.getSetting\((.*?)\)##","[[getSetting(\1)]]","ALL")>
							<cfset xmlnode.xmlattributes.default =additionalArgs.mergeObject.checkForAndEvaluateCFVariables(tempDefault)>
						</cfif>

						<!--- WAB	2014-10-14	Core-856 Default values may be set as Yes/No or True/False, we need to standardise to Yes/No
								Also the currentValue may be saved in 0/1 notation so need to convert to Yes/No  - slightly odd place to do it in evaluateDefaultValues, but seemed convenient
						 --->
						<cfif structKeyExists (xmlnode.xmlattributes,"Type") and xmlnode.xmlattributes.Type is "boolean">
								<cfset xmlnode.xmlattributes.default = convertBooleanToYesNo (xmlnode.xmlattributes.default)>
								<cfif structKeyExists (additionalArgs.currentValues,xmlnode.xmlAttributes.name)>
									<cfset additionalArgs.currentValues[xmlnode.xmlAttributes.name] = convertBooleanToYesNo(additionalArgs.currentValues[xmlnode.xmlAttributes.name])>
								</cfif>
						</cfif>




						<cfreturn true>

					</cffunction>

					<!--- This one just sets blank values for all variables
						Could have done it as I do the main traverse, except that would mean that Condition could not refer to values further down the page (perhaps a bit odd to anyway) or which were hidden.
						Actually doing it first was easier for my merge structure
					 --->
					<cffunction name="defaultCurrentValuesToDefaultValue">
						<cfargument name="xmlnode">
						<cfargument name="additionalArgs">

						<cfset var fieldname = replacenocase(xmlnode.xmlattributes.name,"attributes.","","all")>
						<cfif structKeyExists(xmlnode.xmlattributes,"default")>
							<cfset arguments.additionalArgs.defaults[fieldname] = xmlnode.xmlattributes.default >
						<cfelse>
							<cfset arguments.additionalArgs.defaults[fieldname] = "">
						</cfif>

						<cfparam name='arguments.additionalArgs.currentValues[fieldname]' default = "#arguments.additionalArgs.defaults[fieldname]#">

						<cfreturn true>

					</cffunction>


					<!---
					Does the actual output of a field or group
					--->
					<cffunction name="outputField">
						<cfargument name="xmlnode">
						<cfargument name="additionalArgs">
						<cfargument name="mode">

							<cfset var evaluateSquareBrackets = "">
							<cfset var evaluateExpressionResult = "">
							<cfset var show = true>
							<cfset var attributeCollection = duplicate(xmlnode.xmlattributes)>

							<!--- evaluate defaults, a special case for getSetting --->

							<!--- Take the XMLattributes and standardise them, eg make sure there is a label, run validvalue queries, add validation based on datatype ... --->
							<cfset attributeCollection = application.com.relayTags.createStandardRelayFormElementAttributeStructureFromTagAttributes (attributeStruct = attributeCollection)>

							<!--- add the currentValue of the field into the attribute collection
									the currentvalues of each field are in the additionalArgs.currentValues structure
							--->
							<cfparam name="attributeCollection.currentValue" default = "#arguments.additionalArgs.currentValues[attributeCollection.fieldname]#">

							<!--- these are some extra ones specific to relay tags/functions
								NJH 2013/03/21 - thought it would be nicer to always see the default, as it's not always obvious, particularly if no (radio) value has been set
								WAB 2014-10-14 - reversed Nat's changes, radios should now be better
							--->
							<cfif additionalArgs.defaults[attributeCollection.fieldname] is not  additionalArgs.currentValues[attributeCollection.fieldname] and additionalArgs.defaults[attributeCollection.fieldname] is not "">
								<cfset attributeCollection.noteText = "Default: " & additionalArgs.defaults[attributeCollection.fieldname] >
								<cfset attributeCollection.noteTextPosition = "bottom">
							<cfelseif not structKeyExists(attributeCollection,"default")>
								<cfset attributeCollection.required = true>
							</cfif>

							<!--- decide whether this field/group is to be shown by evaluating Condition --->
							<cfif structKeyExists(attributeCollection,"Condition")>
								<!---
									We are going to use a merge object to evaluate the expression
									Firstly we are going to deal with square brackets and the we are going to evaluate the whole thing
									(I could make a function which did both of these in one go!)
								 --->
								<cfset evaluateSquareBrackets = additionalArgs.mergeObject.checkForAndEvaluateCFVariables(attributeCollection.Condition)>
								<cfset evaluateExpressionResult = additionalArgs.mergeObject.evaluateAString(evaluateSquareBrackets)>
								<cfif evaluateExpressionResult.isOK>
									<cfset show = evaluateExpressionResult.result>
								<cfelse>
									<cfset show = false>
								</cfif>
							</cfif>

							<!--- NJH output description as not text --->
							<cfif structKeyExists(attributeCollection,"description")>
								<cfset attributeCollection.noteText = attributeCollection.description>
							</cfif>

							<cfif show is false>
								<!--- WAB 2014-10-14 set field as done, otherwise current value will appear in the 'additional parameters' section (won't work when condition is on a group) --->
								<cfset additionalArgs.fieldsdone = listappend(additionalArgs.fieldsdone,attributeCollection.fieldname) >
								<!--- return false, this will prevent any children of this item being shown (only relevant for a group) --->
								<cfreturn false><!--- maybe would want non displayed items to be hidden fields (what if there is a value in there, does it get lost) --->
							</cfif>

							<cfif xmlnode.xmlname is "fieldgroup">
								<!--- If we are on a field group --->
								<cfoutput><div><cfif mode eq "start"><STRONG>#attributeCollection.label#</STRONG></cfif><HR></div></cfoutput>
							<cfelse>
								<!--- Actually output the field --->
								<cf_relayFormElementDisplay attributeCollection = #attributeCollection#>
								<cfoutput><CF_INPUT type="hidden" name="#attributeCollection.fieldname#_default" value="#additionalArgs.defaults[attributeCollection.fieldname]#"></cfoutput>
								<!--- keep track of which fields have been displayed
									this is stored in the additionalArgs.fieldsdone variable
									since this is a structure passed by reference, we will be able to get at it later
								 --->
								<cfset additionalArgs.fieldsdone = listappend(additionalArgs.fieldsdone,attributeCollection.fieldname) >
							</cfif>

						<cfreturn true>
					</cffunction>

					<!--- this is an alias for com.settings.getSetting which is used in merging --->
					<cffunction name="getSetting">
						<cfreturn application.com.settings.getSetting(argumentcollection = arguments)>
					</cffunction>




<cfelseif tag.EDITORINSERTPATH is not "">
	<!--- This section outputs an editor from a custom editor file --->
	<!--- save the contents of the cfform, it is outputted later after we have got the header details from the included editor file --->
	<!--- ToDo  - should all be converted to run on relayFormElements --->

		<cfsaveContent variable = "formContent_html">
					<cfif structKeyExists(application.RelayTagKeyedByTagName,relayTag) and application.RelayTagKeyedByTagName[relayTag].description is not "">
						<tr>
							<td colspan="2"><cfoutput>#application.RelayTagKeyedByTagName[relayTag].description#</cfoutput></td>
						</tr>
					</cfif>

					<cfinclude template = "#tag.EDITORINSERTPATH#">
 --->
				<!--- WAB: 2006-03-05 check if all the parameters passed have been used in the editor
					if not then output them anyway
				--->
				<cfset foundMissingParam = false>
				<cfparam name="fieldsToUse" default = "">  <!--- --->
				<cfparam name="relayTagEditorTitle" default = "#tagName#">
				<cfloop collection = #parameterCollection# item = "parameter">
					<cfif listfindnocase(fieldsToUse,parameter) is 0>
						<cfif not foundMissingParam>

							<cfoutput><tr><td colspan="2">Additional Parameters</td></tr></cfoutput>
							<cfset foundMissingParam = true>
						</cfif>
						<cfoutput><tr><td class = "label">#parameter#</td><td><CF_INPUT type="text" name="#parameter#" value="#parameterCollection[parameter]#"></td></tr></cfoutput>
						<cfset fieldsToUse = listappend (fieldsToUse,parameter)>
					</cfif>
				</cfloop>
				<CF_INPUT type="hidden" name="fieldsToUse" value="#fieldsToUse#">
				<CF_INPUT type="hidden" name="tagType" value="#tag.Type#">
				<CF_INPUT type="hidden" name="tagName" value="#tag.Name#">
				<cfoutput><input type="hidden" name="frmAction" value="update"></cfoutput>
				<cfif isDefined("returnFunction")>
					<CF_INPUT type="hidden" name="returnFunction" value="#returnFunction#">
				</cfif>
				<!--- CASE 427013 NJH 2012/03/07 --->
				<cfif isDefined("relayTag")>
					<CF_input type="hidden" name="relayTag" value="#relayTag#">
				</cfif>
				<cfif isDefined("relayFunction")>
					<CF_input type="hidden" name="relayFunction" value="#relayFunction#">
				</cfif>

		</cfsavecontent>


		<!--- Now output the whole page
		variables
		relayTagEditorTitle and fieldsToUse have been passed back by the included editor
		--->
		<cfoutput>

		<cf_head>
			<cf_title>#htmleditformat(relayTagEditorTitle)#</cf_title>
			<SCRIPT type="text/javascript">
			function doSubmit(formObj) {
				var editorValid = true;
				if (typeof editorValidation != 'undefined') {
					var isValid = editorValidation();
					if (isValid != true) {
						editorValid = false;
					}
				}

				if (editorValid) {
					triggerFormSubmit(formObj);
				}
			}			
			</script>
		</cf_head>






		<CF_RelayNavMenu pageTitle="#relayTagEditorTitle#" thisDir="data">
			<cfif url.calledFromIFrame eq "true">
		    	<CF_RelayNavMenuItem MenuItemText="Close Editor" CFTemplate="javascript:parent.openCloseEditorWindow('hide');">
			<cfelse>
				<CF_RelayNavMenuItem MenuItemText="#actionInsert#" CFTemplate="javascript:doSubmit(document.forms[0]);">
				<CF_RelayNavMenuItem MenuItemText="Close Editor" CFTemplate="javascript:window.close();">
		    </cfif>
		</CF_RelayNavMenu>


		<!--- NJH 2016/06/15 PROD2016-1205 added relayFormDisplay so the rwFormValidation is available --->
		<form action="relayTagEditorSubmit.cfm" method="POST" name="relayTagEditorForm" id="relayTagEditorForm" >
		<cf_relayFormDisplay>
			<cfset request.relayForm.useRelayValidation = true>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">

		<!--- now output the content generated earlier --->
		#formcontent_html#

		</table>
		</cf_relayFormDisplay>
		</FORM>

	</cfoutput>

<cfelse>
	<cfoutput>No editor available</cfoutput>
</cfif>


<!--- ASarabi MiniColours --->
<cf_includeJavascriptOnce template = "/javascript/jquery.minicolors.js">
<cf_includeJavascriptOnce template = "/javascript/activityScoreWidget.js">
<cf_includecssonce template = "/styles/minicolors.css">
<cf_includecssonce template = "/styles/activityScoreWidget.css">
