<!--- �Relayware. All Rights Reserved 2014 --->
<cfset fieldsToUse = "URLToGoTo,LinkText">
<cf_relayTagEditorTag relayTagEditorTitle = "Link Tracker" fieldsToUse="#fieldsToUse#">

<cfoutput>

	<tr>
		<td colspan="2">
		<p>Link Tracker can be added to any element and it will 
		add a link which when clicked will track the fact it was clicked.  
		When the user clicks it, the page they are linking to will pop 
		up in a new window.</p>
		<p>To review which links were clicked go to the Reports/Content Analysis
		/#request.CurrentSite.Title# Page visits by page and look for the element you have added
		the link tracker tag to.</p>
		</td>
	</tr>
	<tr>
		<td class="label">Enter the URL e.g. www.google.com</td>
		<td><cfinput type="Text" name="URLToGoTo" required="yes" size="40" 
			message="You must provide the URL to link to" value="#URLToGoTo#"></td>
	</tr>
	<tr>
		<td class="label">Enter the text you want to appear e.g. click here.  You can also add a phrase e.g. phr_phraseText</td>
		<td><cfinput type="Text" name="LinkText" required="yes" 
			message="You must provide a second phrase" value="#LinkText#"></td>
	</tr>
</cfoutput>	
