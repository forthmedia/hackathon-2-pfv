<!--- �Relayware. All Rights Reserved 2014 --->
<!--- GenericEditor.cfm 

WAB 

Generic RelayTag Editor

2005-09-14
this was a quick attempt at template which would automatically generate a relaytag editor for a relaytag

--->


<!--- Read file and find any cfparams  --->
<cffile action="READ" file="#application.path#\relaytagtemplates\#tagStruct.cfm#" variable="TagFile" >


<cfset re = "<CFPARAM([^>]*)>">
<cfset CFParamStructure = structNew()>
<cfset fieldlist = "">

<cfset x = refindnocase(re,TagFile,0,true)>

<cfloop condition = "x.pos[1] is not 0">

	<cfset nameValuePairs = mid(TagFile,x.pos[2],x.len[2])>
	<cfset tempStructure = application.com.regExp.convertNameValuePairStringToStructure(nameValuePairs)>
 	<cfset name = replaceNoCase(tempStructure.name,"attributes.","")>
	<cfset CFParamStructure[name] = structNew()>
	<cfset CFParamStructure[name].required = false>
	<cfif structKeyExists (CFParamStructure[name],"default")>
		<cfset CFParamStructure[name].default = tempStructure.default>
	<cfelse>
		<cfset CFParamStructure[name].required = true> 
	</cfif>
	<cfset fieldlist = listappend(fieldlist,name)>
	<cfset x = refindnocase(re,TagFile,x.pos[1]+x.len[1],true)>

</cfloop>


<cfset re = "<RELAYCOMMENT([^>]*)>">

<cfset x = refindnocase(re,TagFile,0,true)>

<cfloop condition = "x.pos[1] is not 0">

	<cfset nameValuePairs = mid(TagFile,x.pos[2],x.len[2])>
	<cfset tempStructure = application.com.regExp.convertNameValuePairStringToStructure(nameValuePairs)>
	<cfif structKeyExists(tempStructure,"name") and listfindNocase(fieldlist,tempStructure.name)>
		<cfset CFParamStructure[tempStructure.name].description = tempStructure.description>
	</cfif>
	<cfset x = refindnocase(re,TagFile,x.pos[1]+x.len[1],true)>

</cfloop>


<cf_relayTagEditorTag relayTagEditorTitle = "#HTMLEDITFORMAT(RELAYtAG)#" fieldsToUse="#fieldList#">


<cfoutput>

	<tr>
		<td colspan="2">
		Description
		
		</td>
	</tr>
	<cfloop index = "thisKey" list="#fieldList#">
		<cfif structKeyExists(url,#thisKey#)>
			<cfset value = url[#thisKey#]>
		<cfelse>
			<cfset value = "">
		</cfif>
	<tr>
		<td class="label">
			#htmleditformat(thisKey)# 
			
			<cfif structKeyExists(CFParamStructure[thisKey],"description")>
				<BR>	#CFParamStructure[thisKey].description# <BR>
			</cfif>

		</td>
		<td><cfinput type="Text" name="#thisKey#"  size="40" 
			message="" value="#value#">
			<cfif CFParamStructure[name].required>
				<BR>Required
			<cfelseif CFParamStructure[thisKey].default is not "">
				<BR>default : #CFParamStructure[thisKey].default#<BR>
			</cfif>

			 </td>
	</tr>
	</cfloop>
	
</cfoutput>	

</cf_relayTagEditorTag>


