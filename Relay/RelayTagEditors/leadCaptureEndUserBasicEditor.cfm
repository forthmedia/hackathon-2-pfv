<!--- �Relayware. All Rights Reserved 2014 --->
<!--- created by NJH 2006/06/27 --->
<cfparam name="LeadTypeID" default="-1">
<cfparam name="LeadCaptureIntroText" default="">
<cfparam name="LeadCaptureThankYouText" default="">
<cfparam name="leadCaptureScreenID" default="">
<cfparam name="leadCaptureEmailSubject" default="">
<cfparam name="LeadCaptureEmailIntro" default="">
<cfparam name="HideCompanyField" default="no">

<cfset fieldsToUse = "LeadTypeID,LeadCaptureIntroText,LeadCaptureThankYouText,leadCaptureScreenID,leadCaptureEmailSubject,LeadCaptureEmailIntro,HideCompanyField">
<cf_relayTagEditorTag relayTagEditorTitle = "Basic Lead Capture Editor" fieldsToUse="#fieldsToUse#">

<cfquery name="LeadTypes" datasource="#application.SiteDataSource#">
	select leadTypeID, name, fieldsToShow from leadType order by name
</cfquery>

<cfif isDefined('URL.isEdit') and (#URL.isEdit#)>
	<cfquery name="getLeadType" datasource="#application.SiteDataSource#">
		select leadTypeID, name, fieldsToShow from leadType where leadTypeID =  <cf_queryparam value="#LeadTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
<cfelse>
	<cfquery name="getLeadType" datasource="#application.SiteDataSource#">
		select leadTypeID, name, fieldsToShow from leadType where leadTypeID =  <cf_queryparam value="#LeadTypes.leadTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
</cfif>

<cfquery name="Screens" datasource="#application.SiteDataSource#">
	select '' as screenID, ' None' as screenName
	UNION
	select screenID, screenname+' ('+ CAST(screenID as varchar(10)) + ')' as screenName from screens
	order by screenName
</cfquery>

<cfoutput>
	<SCRIPT type="text/javascript">
		var clearEmailFields = false;
		
		function editorValidation() {

			var thisForm = document.relayTagEditorForm;
			
			if (thisForm.HideCompanyField.checked) {
				thisForm.HideCompanyField.value = "yes";
			} else {
				thisForm.HideCompanyField.value = "";
			}
			
			if (thisForm.leadCaptureScreenID.value == "") {
				thisForm.leadCaptureScreenID.value = "";
			}
			
			if (clearEmailFields) {
				thisForm.leadCaptureEmailSubject.value = "";
				thisForm.leadCaptureEmailIntro.value = "";
			}
			
			return true;
		}
		
		<!--- find the leadTypes that will be sending emails. For those that are, show the email fields. For those that aren't, hide the fields --->
		
		function ToggleEmailRowsVisibility() {

			var thisForm = document.relayTagEditorForm;
			var EmailLeadType = new Array;
			
			<!--- for every leadtype, find out whether it contains a displayType of 'SelectEmail'. If so, add it to the array --->
			<cfset counter=0>
			<cfloop query="LeadTypes">
				<cfloop from="1" to="#listlen(LeadTypes.fieldsToShow,",")#" step="1" index="xCount">
				<cfset customDetail = listgetat(LeadTypes.fieldsToShow,xCount,",")>
				<cfif listlen(customDetail,"-") gt 1>
					<cfset fieldType=listgetat(customDetail,2,"-")>
				<cfelse>
					<cfset fieldType="default">
				</cfif>
				<cfif #fieldType# eq "selectEmail">
					EmailLeadType[#jsStringFormat(counter)#]="#jsStringFormat(LeadTypeID)#";
					<cfset counter=#counter#+1>
					<cfbreak>
				</cfif>
				</cfloop>
			</cfloop>
			
			<!--- If the new LeadTypeID value is in the array, it sends an email upon form submission. Show the email fields --->
			for (i=0;i<EmailLeadType.length;i++) {
				if (thisForm.LeadTypeID.value == EmailLeadType[i]){
					if (document.getElementsByName) {
						var tableRows = document.getElementsByName('sendEmailTr');
						for (j=0;j<tableRows.length;j++){
							tableRows[j].style.display = "";
						}
					}
				} else {
					if (document.getElementsByName) {
						var tableRows = document.getElementsByName('sendEmailTr');
						for (j=0;j<tableRows.length;j++){
							tableRows[j].style.display = "none";
						}
					} 
				clearEmailFields=true;
				}
			}
			return true;
		}
	</script>
	
	<cf_translate phrases="phr_DescriptiveTextHere,phr_LeadType,phr_IncludedScreen,phr_Optional,phr_HideCompanyField,phr_OnScreenIntroductionText,phr_OnScreenThankYouText,phr_EmailSubject,phr_EmailIntroductionText"/>

	<cf_relayFormElement relayFormElementType="hidden" fieldname="fieldsToUse" label="" currentValue="#fieldsToUse#">
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cf_relayFormDisplay>
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="<p>#phr_DescriptiveTextHere#</p>" label="" spanCols="yes">
		<cf_relayFormElementDisplay relayFormElementType="select" fieldname="LeadTypeID" label="#phr_LeadType#" currentValue="#LeadTypeID#" query="#LeadTypes#" display="name" value="leadTypeID" onChange="ToggleEmailRowsVisibility();">
		<cf_relayFormElementDisplay relayFormElementType="select" fieldname="leadCaptureScreenID" label="#phr_IncludedScreen# (#phr_Optional#)" currentValue="#leadCaptureScreenID#" query="#Screens#" display="screenName" value="screenID">
		<cf_relayFormElementDisplay relayFormElementType="checkbox" fieldname="HideCompanyField" label="#phr_HideCompanyField#" currentValue="#HideCompanyField#" value="yes">
		<cf_relayFormElementDisplay relayFormElementType="text" fieldname="LeadCaptureIntroText" label="#phr_OnScreenIntroductionText#" currentValue="#LeadCaptureIntroText#">
		<cf_relayFormElementDisplay relayFormElementType="text" fieldname="LeadCaptureThankYouText" label="#phr_OnScreenThankYouText#" currentValue="#LeadCaptureThankYouText#">
		
		<cfset sendEmail="false">
		<cfloop from="1" to="#listlen(getLeadType.fieldsToShow,",")#" step="1" index="xCount">
			<cfset customDetail = listgetat(getLeadType.fieldsToShow,xCount,",")>
			<cfif listlen(customDetail,"-") gt 1>
				<cfset fieldType=listgetat(customDetail,2,"-")>
			<cfelse>
				<cfset fieldType="default">
			</cfif>
			<cfif #fieldType# eq "selectEmail">
				<cfset sendEmail="true">
				<cfbreak>
			</cfif>	
		</cfloop>
		<cfset trID = "sendEmailTr">
		<cfif #sendEmail# neq "false">
			<cfset trStyle="display:">
		<cfelse>
			<cfset trStyle="display:none">	
		</cfif>
	
		<cf_relayFormElementDisplay relayFormElementType="text" trID="#trID#" trStyle="#trStyle#" fieldname="leadCaptureEmailSubject" label="#phr_EmailSubject#" currentValue="#leadCaptureEmailSubject#">
		<cf_relayFormElementDisplay relayFormElementType="text" trID="#trID#" trStyle="#trStyle#" fieldname="leadCaptureEmailIntro" label="#phr_EmailIntroductionText#" currentValue="#leadCaptureEmailIntro#">
	</cf_relayFormDisplay>
</cfoutput>	




