<!--- �Relayware. All Rights Reserved 2014
	NJH 2016/06/15 PROD2016-1205  - changed relay_else to relay_if_else as the editorSubmit was looking for that; changed cfinput to cf_input.
--->
<cf_relayTagEditorTag relayTagEditorTitle = "#tagName#" fieldsToUse="">

<cfif isDefined("isEdit") and not isEdit>
	<cfoutput>
	<tr>
		<td class="label">IF Condition</td>
		<td><cf_input type="Text" name="relay_if" required="yes" size="40" message="Enter a Condition" value=""></td>
	</tr>
	<tr>
		<td class="label">ELSEIF Condition (optional)</td>
		<td><cf_input type="Text" name="relay_if_elseif1" size="40"
			message="" ></td>
	</tr>
	<tr>
		<td class="label">ELSEIF Condition (optional)</td>
		<td><cf_input type="Text" name="relay_if_elseif2"  size="40" message="" ></td>
	</tr>
	<tr>
		<td class="label">ELSE</td>
		<td><cf_input type="Checkbox" name="relay_if_else"  size="40" message="" ></td>
	</tr>

		<input type="hidden" name="relay_if_endif" value=1>
	</cfoutput>
<cfelse>
	<!--- Rather a special case here for relay_if and relay_elseif, the condition parameter is not named,
	the condition is the whole parameter string and we use a field name called tagParams which is processed specially --->
	<cfset condition = trim(parseResult.parametersString)>
	<cfset structClear (parameterCollection)>  <!--- Case 436295 NJH 2013/11/12 - we are deleting the parameter collection here because the condition is actually not held in a condition parameter
														and so the editor thinks that it is an undocumented parameter. When then saving, it fails because it's not a valid variable name --->

	<cfoutput>

		<tr>
			<td colspan="2">
			Control Flow
			</td>
		</tr>
		<tr>
			<td class="label">Condition</td>
			<td><cf_input type="Text" name="tagParams" required="yes" size="40"
				message="" value="#Condition#"></td>
		</tr>
	</cfoutput>

</cfif>
