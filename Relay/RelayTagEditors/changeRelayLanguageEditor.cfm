<!--- �Relayware. All Rights Reserved 2014 --->
<cfset fieldsToUse = "showToThesePersonIDs,showTheseLanguageIDs">
<cf_relayTagEditorTag relayTagEditorTitle = "Change Relay Language Editor" fieldsToUse="#fieldsToUse#">


<cfoutput>
	<SCRIPT type="text/javascript">
		function editorValidation() {
			var thisForm = document.relayTagEditorForm;
			if (thisForm.showToThesePersonIDs.value.toLowerCase() != "all") {
				var validPIDs = true;
				var pIDSplit = thisForm.showToThesePersonIDs.value.split("-");
				for (var i=0;i<pIDSplit.length;i++) {
					if (isNaN(pIDSplit[i])) {
						validPIDs = false;
					}
				}
				if (!validPIDs) {
					alert("Please ensure that your list of person ID's only contains the word 'ALL' or numeric values seperated by hyphens.\ne.g. 1-2-3");
					return false;
				}
			}
			
			var lIDSplit = thisForm.showTheseLanguageIDs.value.split("-");
			var validLIDs = true;
			for (var i=0;i<lIDSplit.length;i++) {
				if (isNaN(lIDSplit[i])) {
					validLIDs = false;
				}
			}
			if (!validLIDs) {
				alert("Please ensure that your list of language ID's only contains numeric values seperated by hyphens.\ne.g. 1-2-3");
				return false;
			}

			
			return true;
		}
	</script>


	<tr>
		<td colspan="2">
		<p>Some of the more frequently used language IDs are Dutch=7, English=1, French=2, German=3, Greek=25, Italian=4, Polish=19, Portuguese=6, Russian=20, Spanish=5, Swedish=21</p>
		</td>
	</tr>
	<tr>
		<td class="label">Show to all users who visit this page or specify personids as hyphen delimited list (e.g. 1-2-3-4)</td>
		<td>
			<cfif showToThesePersonIDs eq ""><cfset useValue = "ALL"><cfelse><cfset useValue = showToThesePersonIDs></cfif>
			<cfinput type="Text" name="showToThesePersonIDs" required="Yes" Message="You are required to either fill in 'All' or a list of numeric ID values, seperated by hyphens." value="#useValue#">
		</td>
	</tr>
	<tr>
		<td class="label">Specific a hyphen delimited list of language IDs if you want to limit the languages.</td>
		<td>
			<cfinput type="Text" name="showTheseLanguageIDs" required="No" value="#showTheseLanguageIDs#">
		</td>
	</tr>
</cfoutput>	



