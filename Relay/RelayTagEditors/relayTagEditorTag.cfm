<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayTagEditorTag.cfm
Author:				WAB from work by SWJ
Date started:		2005-09-14
	
Description:		custom tag which must called from within an individual relaytageditor
					1) sets some variables back into the caller scope for use by relayTagEditor.cfm
					2) params all the possible parameters which might be being passed in 
					

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name = "attributes.relayTagEditorTitle">
<cfparam name = "attributes.fieldsToUse">
 
 
<cfset caller.relayTagEditorTitle = attributes.relayTagEditorTitle>
<cfset caller.fieldsToUse = attributes.fieldsToUse>


<cfloop index="i" list="#attributes.fieldsToUse#">
	<cfparam name="caller.url.#i#" default="">
</cfloop>
