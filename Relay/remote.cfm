<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			remote.cfm
Author:				SWJ
Date started:		22 November 2002

Description:		Handles calls from emails:

Parameters:			a = action (t=track CommID, u=unsubscribe, d=download, g=go to URL, e=goToElementID)
					p = person's magic checksum number
					c = this commid
					e = elementID
					u = URL to go to
					f = fileID
					s = send this commid to a friend
					i = auto login to internal app
					ct = click track.  Adds a record to the entityTracking table.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-05-25		WAB 		Modified to handle p number belonging to a person who has been deduped.  MagicChecksum returns variable actualPersonID
2005-06-08		WAB			Added a few cfdumps in the mail sent if there is no url.a set
2005-09-13		SWJ			Added a new option called ct which is used by the clickTracker relay tag.
2005-12-19		WAB			Removed setting of user cookies and replaced with a call to com.login.initialiseAnExternalUser
2008/07/14 		WAB 		removed references to relayCurrentVersion.relaycurrentUser Version
2009/01/14		WAB			Mods to speed up.  For email tracking this template is no longer called directly but rather via another template which does not create sessions.
2009/03/10		WAB			altered warning email when remote.cfm is called without params
2010/11/11 		AJC			P-LEN022 CR011 Add view online link
Enhancements still to do:
make xxxxMessage into a phrase and work out the users default language.

 --->
<cfif isdefined("a")>
	<cfswitch expression="#a#">
		<!--- t=track commid --->
		<cfcase value="t">
			<CFIF isDefined("p")>
				<!--- check ID is valid.  MagicCheckSum requires person. --->
			 	<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
				<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
					<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">
				</CFIF>
			</CFIF>

				 <!--- WAB 2009/01/27 sped things up by tracking link not creating a session at all, so removed this code (actually crashes because session not enabled
				<!--- WAB 200605  kill this session after a minute - clears up memory and doesn't clog up reports of sessions currently live --->
					<cfset application.com.sessionManagement.killCurrentSession(60)>
				--->
		</cfcase>

		<!--- u=unsubscribe from commid --->
		<cfcase value="u">
			<CFIF isDefined("p")>
				<!--- check ID is valid.  MagicCheckSum requires person. --->
				<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
				<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
					<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">

						<!---		WAB 2005-12-19
						check whether this is a link from the relaycurrentuser if not then initialise them (note that this is not a login but will set a user cookie and make sure they are logged out) --->
						<cfif actualPersonID is not request.relayCurrentUser.personid>
							<cfset application.com.login.initialiseAnExternalUser (actualPersonID)>
						</cfif>

						<CFSET Unsubscribe_request = "unsubscribe">
						<CFIF isDefined("c") and isNumeric(c)>
							<CFSET xxxxCommID = c>
							<CFSET xxxxPersonID = actualPersonID>
							<CFSET p = actualPersonID>
							<CFSET c = c>
							<CFSET pid = actualPersonID>
							<CFSET cid = c>
							<cfif isDefined("debug") and debug eq "yes"><cfoutput>DEBUG: in remote with c defined. unsubscribeHandler = #request.currentsite.unsubscribeHandler# unsubscribeHandlerID = #request.currentsite.unsubscribeHandlerID#</cfoutput></cfif>
							<CFIF isDefined("fid") and isNumeric(fid)>
								<!--- fid is for unsubscribing from specific flags --->
								<CFSET fid = fid>
							</CFIF>
						</CFIF>


						<cfswitch expression=#request.currentsite.unsubscribeHandler#>
							<cfcase value="Element">
								<CFSET url.eid = request.currentsite.unsubscribeHandlerID> <!--- defaults to "unsubscribeHandler" --->
								<cfinclude template="index.cfm">
								<!--- might be better to do a javascript redirect here - gets a better url on the browser --->
							</cfcase>
							<cfcase value="Screen">
								<cfinclude template="/relay/relayTagTemplates/unsubscribeHandler_Screen_WithBorder.cfm">
							</cfcase>
							<cfdefaultCase >  <!--- includes singleClick --->
								<cfinclude template="/code/cftemplates/unsubscribeTemplate.cfm">
							</cfdefaultcase>
						</cfswitch>

				</cfif>
			<cfelse>
				<CFSET message = "To unsubscribe from future emails please log into the portal and edit your personal profile.">
				<cfinclude template="elementTemplate.cfm">
			</CFIF>
			<cfif isDefined("debug") and debug eq "yes"><cfoutput>DEBUG remote.cfm: etid = #htmleditformat(etid)#</cfoutput></cfif>
		</cfcase>

		<!--- download file from commid --->
		<cfcase value="d">
			<!--- check ID is valid.  MagicCheckSum requires person. --->
			<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
			<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
				<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">

				<!---	WAB 2005-12-19
				check whether this is a link from the relaycurrentuser if not then initialise them (note that this is not a login but will set a user cookie ) --->
				<cfif actualPersonID is not request.relayCurrentUser.personid>
					<cfset application.com.login.initialiseAnExternalUser (actualPersonID)>
				</cfif>

				<CFIF isDefined("C") and isNumeric(C)>
					<CFSET CID = c>
					<cfset pid=actualPersonID>
					<cfif isDefined("f") and f neq 0>
						<CFSET FID = F>
						<CFQUERY NAME="getFile" DATASOURCE="#application.SiteDataSource#">
							SELECT FileTypeID
							FROM Files
							WHERE FileID =  <cf_queryparam value="#fid#" CFSQLTYPE="CF_SQL_INTEGER" >
						</CFQUERY>
						<cfif getFile.recordcount eq 1>
							<cfset eid = fid>
							<cfset etype = "files">
							<cfinclude template="/relayTagTemplates/externalLink.cfm">
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfcase>

		<!--- goto remote URL --->
		<cfcase value="g">
			<!--- check ID is valid.  MagicCheckSum requires person. --->
			<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
			<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
				<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">

				<!---
					WAB 2005-12-19
				check whether this is a link from the relaycurrentuser if not then initialise them (note that this is not a login but will set a user cookie and )

				WAB 2011/06/22 request.relaycurrentuser not defined if we use the nosession version
				--->
				<cfif structKeyExists (request, "relayCurrentUser") and actualPersonID is not request.relayCurrentUser.personid>
					<cfset application.com.login.initialiseAnExternalUser (actualPersonID)>
				</cfif>

				<!--- WAB 2009/02/02 updateCommDetail no longer creates variable CommDetailID, and it isn't used anyway so remove this test
				<CFIF isDefined("CommDetailID") and isDefined("u")><!--- this is set in updateCommDetail above --->
				--->

					<!--- WAB: 2004-07-21 I think that the tracking ought to be on communication and commid
							WAB: 2004-07-26 actually the commid is recorded against the commid column so not so sure now!
					<CFSET eid = CommDetailID>
					<cfset eType = "commDetail">
					--->

					<CFSET eid = C>
					<cfset eType = "communication">
					<cfset pid=actualPersonID>
					<cfset goToURL=u>
					<cfset cid = c>  <!--- this ensures that the commid column of the entitytracking table get populated--->
					<cfinclude template="/relayTagTemplates/externalLink.cfm">
				<!--- WAb 2009/02/02 </cfif> --->
			</cfif>
		</cfcase>

		<!--- go to Relay ElementID --->
		<cfcase value="e">
			<!--- check ID is valid.  MagicCheckSum requires person. --->
			<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
			<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
				<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">

				<!---
					WAB 2005-12-19
				check whether this is a link from the relaycurrentuser if not then initialise them (note that this is not a login) --->
				<cfif actualPersonID is not request.relayCurrentUser.personid>
					<cfset application.com.login.initialiseAnExternalUser (actualPersonID)>
				</cfif>
<!--- 				<cfset application.com.globalFunctions.cfcookie(NAME="USER" VALUE="#actualPersonID#-#application.WebUserID#-0" EXPIRES="400")>
 --->
				<CFIF isDefined("e") <!--- and isNumeric(e) WAB - not need to be numeric, can handle etids--->><!--- this is set in updateCommDetail above --->
					<cfset url.cid = c> <!--- setting this should ensure that elementtemplate creates an entity tracking record against this communication --->
					<cfset pid = actualPersonID>	<!--- ditto --->

					<!---  December 2005 William trying to get this to work with Sony1
						 - it has to go through index.cfm, othewise the frameset is never loaded
					--->

					<!---
					This is what it was originally
					<CFSET eid = e>
 					<CFINCLUDE TEMPLATE="elementTemplate.cfm">
					--->

					<!---
					this is a version that I tried and it just about works except that the cid doesn't get passed through the sony index.cfm (because it is not in the query_ string)
					<CFSET url.eid = e>
					<CFINCLUDE TEMPLATE="index.cfm">
					--->

					<!--- so I went for a redirecting version
							cid required to get tracking against correct communication
					--->
						<!--- pass on any unused bits of query string --->
						<cfset remainingVariables = application.com.regExp.removeItemsFromNameValuePairString(request.query_string,"c,a,e,p,t","&")>

						<cfoutput>
						<SCRIPT type="text/javascript">
							window.location.href = '/?eid=#jsStringFormat(e)#&cid=#jsStringFormat(c)#<cfif remainingVariables is not "">&#jsStringFormat(remainingVariables)#</cfif>';
						</script>
						</cfoutput>
				</cfif>
			</cfif>
		</cfcase>

		<!--- 2005-06-02 SWJ Started this not finished
		update a flag --->
		<cfcase value="fl">
			<!--- check ID is valid.  MagicCheckSum requires person. --->
			<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
			<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
				<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">
				<!---
					WAB 2005-12-19
				check whether this is a link from the relaycurrentuser if not then initialise them (note that this is not a login) --->
				<cfif actualPersonID is not request.relayCurrentUser.personid>
					<cfset application.com.login.initialiseAnExternalUser (actualPersonID)>
				</cfif>
<!--- 				<cfset application.com.globalFunctions.cfcookie(NAME="USER" VALUE="#actualPersonID#-#application.WebUserID#-0" EXPIRES="400")>
 --->
				<CFIF isDefined("e") and isNumeric(e)><!--- this is set in updateCommDetail above --->
					<cfset cid = c> <!--- setting this should ensure that elementtemplate creates an entity tracking record against this communication --->
					<cfset pid = actualPersonID>	<!--- ditto --->
					<CFSET eid = e>
					<CFINCLUDE TEMPLATE="elementTemplate.cfm">
				</cfif>
			</cfif>
		</cfcase>

		<!--- sendToFriend --->
		<cfcase value="s">
			<!--- check ID is valid.  MagicCheckSum requires person. --->
			<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
			<CFIF isDefined ("validCheckSum")and validChecksum EQ "True">
				<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#actualPersonID#" cid="#c#" action="#a#">
				<!---
					WAB 2005-12-19
				check whether this is a link from the relaycurrentuser if not then initialise them (note that this is not a login) --->
				<cfif actualPersonID is not request.relayCurrentUser.personid>
					<cfset application.com.login.initialiseAnExternalUser (actualPersonID)>
				</cfif>
<!--- 				<cfset application.com.globalFunctions.cfcookie(NAME="USER" VALUE="#actualPersonID#-#application.WebUserID#-0" EXPIRES="400")>
 --->
				<cfset frmCommID = c>
				<cfset pid=actualPersonID>
				<CFSET etid = "sendToFriend">
			</cfif>
			<cfif isDefined("debug") and debug eq "yes">
				<cfoutput>DEBUG remote.cfm: etid = #htmleditformat(etid)#</cfoutput>
			</cfif>
			<cfinclude template="elementTemplate.cfm">
		</cfcase>

		<!--- go to INTERNAL PAGE --->
		<cfcase value="i">
			<!--- Examples of  URL link required to login automatically:
					siteDomain/remote.cfm?p=1031-17896&a=i&st=001 --->
					<cfif not request.relayCurrentUser.isLoggedIn>

						<!--- check ID is valid.  MagicCheckSum requires person. --->
						<cfscript>
							checkUsername = application.com.login.autoAuthenticateInternalUser(listFirst(p,"-"),listLast(p,"-"));
						</cfscript>

						<!--- if the user is not matched as an internal user or they have not logged in from this machine before --->
						<CFIF checkUsername.recordCount EQ 1 and structKeyExists(cookie,"user") and request.relayCurrentUser.personid eq listFirst(p,"-")>
							<CFINCLUDE TEMPLATE="resetblind.cfm">

							<cfset application.com.RelayCurrentUser.setLoggedIn(checkUsername.personID,2)> >

							<cfoutput>
							<SCRIPT type="text/javascript">
								location.href = "/remote.cfm?#jsStringFormat(request.query_string)#"
							</script>
							</cfoutput>
						<cfelse>
							<cfset message = "You must login manually first">
							<cfinclude template="index.cfm">
						</cfif>
					<cfelse>
						<cfinclude template="index.cfm">
					</cfif>

		</cfcase>

		<!--- 2005-09-12 SWJ ct=track click through tracking.  Adds a record to the entityTracking table.  This can be reviewed
				via the entityTracking report.
			2005-10-11 WAB can now accept parameter t (for type) which defines the entityType  - so not just elements, could be elementLink	etc.
				--->
		<cfcase value="ct">
			<CFIF isDefined("p") and isDefined("e") and isDefined("g")>
				<cfif isDefined("t")>
					<cfset entityTypeID = t>
				<cfelse>
					<cfset entityTypeID = "element">
				</cfif>
				<!--- check ID is valid.  MagicCheckSum requires person. --->
				<!--- NJH 2013/11/04 Case 437772 - add the clickThru --->
			 	<cfset clickThru = application.com.entityFunctions.addEntityTracking(entityID=e,entityTypeID=entityTypeID,personid=p,goToURL=g,clickThru=true,visitID=0)>
			</CFIF>
		</cfcase>

		<cfcase value="ctg">
			<CFIF isDefined("e") and isDefined("g")>
				<cfif isDefined("t")>
					<cfset entityTypeID = t>
				<cfelse>
					<cfset entityTypeID = "element">
				</cfif>
				<cfif not isDefined("p")>
					<cfset p = request.relaycurrentuser.personid>
				</cfif>
				<!--- check ID is valid.  MagicCheckSum requires person. --->
			 	<cfset clickThru = application.com.entityFunctions.addEntityTracking(entityID=e,entityTypeID=entityTypeID,personid=p,goToURL=g,visitID=0)>
				<cflocation url="#g#" addtoken="No">
			</CFIF>
		</cfcase>
		<!--- START: 2010/11/11 AJC P-LEN022 CR011 Add view online link --->
		<cfcase value="c">
			<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#p#">
			<cfif isDefined ("validCheckSum")and validChecksum EQ "True">
				<cfset commid = c>
				<cfset personID = actualpersonID>
				<cfinclude template="/communicate/viewCommunication.cfm">
			</cfif>
		</cfcase>
		<!--- END: 2010/11/11 AJC P-LEN022 CR011 Add view online link --->
	</cfswitch>
<cfelse>
	<!--- WAB 2005-06-08 added a few cfdumps to find out what is going on
	WAB 2009/03/10 added dump of cgi and changed email address so that don't go to help
	--->
	<cfmail to="errors@foundation-network.com" from="#application.emailfrom#" subject="remote.cfm error" type="html">
		<cfif structKeyExists (request,"currentsite")>#request.CurrentSite.Title#<cfelse>#application.applicationname#</cfif> remote.cfm called without url.a defined.<br>
		<cfdump var="#cgi#">

		<cfif isDefined("form")>
			Form Variables
			<cfdump var="#form#">
		</cfif>

		<cfif isDefined("url")>
			URL Variables
			<cfdump var="#url#">
		</cfif>

		<cfif isDefined("request.relayCurrentUser")>
			CurrentUser Structure
			<cfdump var="#request.relayCurrentUser#">
		</cfif>


	</cfmail>
	<!---
		WAB 2011/07/05 LID 7086 added an if around this include.
		If this template has been called via /webservices/nosession then there is no relaycurrentuser so we can't include index.cfm - since it will fall over!
		(Problem only seems to occur on Sony where we get lots of links without url.a defined)
	--->
	<cfif isDefined("request.relayCurrentUser")>
	<cfinclude template="index.cfm">
</cfif>
</cfif>

