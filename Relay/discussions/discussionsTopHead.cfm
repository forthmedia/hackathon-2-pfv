<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="edit">
<cfparam name="pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<cfif not structKeyExists(url,"editor")>
	<cf_RelayNavMenu pageTitle="#pageTitle#" thisDir="#attributes.thisDir#">
		<cf_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='groups.cfm?editor=yes&add=yes')#">	
	</cf_RelayNavMenu>
</cfif>
