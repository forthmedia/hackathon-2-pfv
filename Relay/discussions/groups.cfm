<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			groups.cfm	
Author:				IH
Date started:		2013/02/11
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:


 --->

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

<cfset application.com.request.setTopHead(pageTitle="Discussion Groups")>

<cfparam name="campaignID" type="numeric" default=0>
<cfparam name="qGroups" type="string" default="">
<cfparam name="sortOrder" default="ProductCategory,Title">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="233" name="thisEditor" entity="discussionGroup" title="Discussion Group Editor">
			<field name="discussionGroupID" label="phr_discussionGroup_discussionGroupID" description="This records unique ID." control="html"></field>			
			<field name="title" label="phr_discussionGroup_title" description="The discussioin group title" />				
			<field name="description" label="phr_discussionGroup_description" maxlength="255" description="The discussion group description" control="textArea" />
			<field name="" control="file" acceptType="image" label="phr_discussionGroup_icon" parameters="filePrefix=icon_,displayHeight=80,height=80,width=80" description=""></field>			
			<field name="active" label="phr_sys_Active" description="The display of Active Discussion Groups." control="YorN"></field>				
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(url,"editor")>	
	<cfquery name="qGroups" datasource="#application.siteDataSource#">
		select * from (
			select discussionGroupID,Title,Description,active,LastUpdated
			from discussionGroup
			
			) base
			where 1=1			
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by title
	</cfquery>
</cfif>

<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="groups"	
	keyColumnList="Title"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="openNewTab('##jsStringFormat(Title)##'*comma'##jsStringFormat(Title)##'*comma'/discussions/groups.cfm?editor=yes&hideBackButton=true&discussionGroupID=##discussionGroupID##'*comma{reuseTab:true*commaiconClass:'cjm'});return false;"
	showTheseColumns="Title,Description,active,LastUpdated"	
	FilterSelectFieldList="Title,Active"
	useInclude="false"
	sortOrder="#sortOrder#"
	hideBackButton="true"
	queryData="#qGroups#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"	
	hideTheseColumns=""
	booleanFormat="active"
	dateFormat="lastUpdated"
	showSaveAndAddNew="true"	
>






