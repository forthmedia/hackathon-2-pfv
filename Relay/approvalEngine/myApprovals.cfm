<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			myApprovals.cfm
Author:				YMA
Date started:		18/09/2014

Description:		List of approvals relating to current user

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:
2015-05-12			AXA			Added ability to list approvals on portal and portal-friendly link 	for actions

 --->
<cfparam name="numRowsPerPage" default="20">
<cfparam name="sortOrder" default="APPROVEDSTATUSCREATED DESC">

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

<cfset approvalRequestsByApprover = application.com.approvalEngine.getApprovalRequestsForPerson(personID = request.relaycurrentuser.personID)>

<cfset approveNowTranslation = application.com.relayTranslations.translateListOfPhrases ('ApprovalEngine_ApproveNow')>

<cfset buildJSLink = "">

<cfquery name="approvalRequestsByApprover" dbtype="query">
	select *, '#approveNowTranslation.phrases.ApprovalEngine_ApproveNow.phraseText#' as click from approvalRequestsByApprover
	where APPROVALSTATUSTYPETEXTID = 'approvalRequested'
	union
	select *, '' as click from approvalRequestsByApprover
	where APPROVALSTATUSTYPETEXTID != 'approvalRequested'
</cfquery>

<cfquery name="approvalRequestsByApprover" dbType="query">
	select * from approvalRequestsByApprover
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!--- START: 2015-05-12 AXA Added ability to list approvals on portal and portal-friendly link 	for actions --->
<!--- Removed link text from call to TFQO.  Separate links required for portal vs internal site.  Using savecontent to ensure that link is built appropriately and that the encryption and jsformat functions run prior to including the link --->
<cfif request.relayCurrentUser.isInternal GT 0>
	<cfsavecontent variable="buildJSLink">
		<cfoutput>
		javascript:openNewTab('phr_approvalEngine_approvalDecision ##jsStringFormat(application.entitytype[APPROVALENGINEENTITYTYPEID].uniqueKey)##: ##jsStringFormat(APPROVALSTATUSRELATEDENTITYID)##'*comma'phr_approvalEngine_approvalDecision ##jsStringFormat(application.entitytype[APPROVALENGINEENTITYTYPEID].uniqueKey)##: ##jsStringFormat(APPROVALSTATUSRELATEDENTITYID)##'*comma'/approvalEngine/approverResponse.cfm?##jsStringFormat(application.com.security.encryptQueryString(queryString='entityTypeID='&APPROVALENGINEENTITYTYPEID&'&approvalStatusRelatedEntityID='&APPROVALSTATUSRELATEDENTITYID&'&approverID='&APPROVERID&'&approvalEngineID='&APPROVALENGINEID))##'*comma{reuseTab:true*commaiconClass:'myfavorites'});return false;
		</cfoutput>
	</cfsavecontent>
<cfelse>
	<cfsavecontent variable="buildJSLink">
		<cfoutput>
		javascript:openNewTab('phr_approvalEngine_approvalDecision ##jsStringFormat(application.entitytype[APPROVALENGINEENTITYTYPEID].uniqueKey)##: ##jsStringFormat(APPROVALSTATUSRELATEDENTITYID)##'*comma'phr_approvalEngine_approvalDecision ##jsStringFormat(application.entitytype[APPROVALENGINEENTITYTYPEID].uniqueKey)##: ##jsStringFormat(APPROVALSTATUSRELATEDENTITYID)##'*comma'/?##jsStringFormat(application.com.security.encryptQueryString(queryString='eid=approvalProcess&entityTypeID='&APPROVALENGINEENTITYTYPEID&'&approvalStatusRelatedEntityID='&APPROVALSTATUSRELATEDENTITYID&'&approverID='&APPROVERID&'&approvalEngineID='&APPROVALENGINEID))##'*comma{reuseTab:true*commaiconClass:'myfavorites'});return false;
		</cfoutput>
	</cfsavecontent>
</cfif>
<!--- END: 2015-05-12 AXA Added ability to list approvals on portal and portal-friendly link 	for actions --->

<!--- 2015-05-12 AXA  replaced keyColumnOnClickList with link generated above --->
<CF_tableFromQueryObject
	queryObject="#approvalRequestsByApprover#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	dateformat="APPROVEDSTATUSCREATED"
	showTheseColumns="ENTITYTYPENAME,ENTITYNAME,approvalstatusTypeText,CURRENTLEVEL,APPROVEDSTATUSCREATED,click"
	useInclude="false"
	columnTranslation="true"
	FilterSelectFieldList="ENTITYTYPENAME"
	FilterSelectFieldList2="approvalstatusTypeText"
	keyColumnList="click"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="#buildJSLink#"
	ColumnTranslationPrefix="phr_myApprovals_"
	allowColumnSorting="yes"
>