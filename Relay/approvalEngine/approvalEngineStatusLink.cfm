<!--- initialising the modal dialog framework --->

<cfparam name="phrasePrefix" default="">

<cfif not isDefined('entityTypeID') and isDefined('frmEntityTypeID')>
	<cfset entityTypeID = frmEntityTypeID>
<cfelseif structKeyExists(caller,'entitydetails') and structKeyExists(caller.entityDetails,'entitytypeID') and isNumeric(caller.entityDetails.entitytypeID)>
	<cfset entityTypeID = caller.entityDetails.entityTypeID>
</cfif>

<cfif isDefined('entityType') and isNumeric(entityType)>
	<cfset entityUniqueID = #evaluate(application.entityType[entityType].uniqueKey)#>
	<cfset entityUniqueKey = application.entityType[entityType].uniqueKey>
<cfset entityTypeID = entityType>
<cfelse>
<cfset entityUniqueID = #evaluate(application.entityType[entityTypeID].uniqueKey)#>
<cfset entityUniqueKey = application.entityType[entityTypeID].uniqueKey>
</cfif>


<cfif request.relaycurrentuser.isinternal>
	<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
	<!--- 2014-10-20	RPW	CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View - Changed internal display to be a submit button --->
	<cf_relayFormElement relayFormElementType="button" fieldName="viewApprovalBtn" currentValue="phr_View" label="" onclick="openNewTab('phr_ApprovalStatus #jsStringFormat(entityUniqueKey)#: #jsStringFormat(entityUniqueID)#','phr_ApprovalStatus #jsStringFormat(entityUniqueKey)#: #jsStringFormat(entityUniqueID)#','/approvalEngine/approvalEngineStatusListing.cfm?entityID=#jsStringFormat(entityUniqueID)#&entityTypeID=#jsStringFormat(entityTypeID)#',{reuseTab:true,iconClass:'approval'});">
<cfelse>
	<cf_includeCssOnce template="/code/styles/approvalModal.css">
	<cf_modalDialog size="large" type="iframe" identifier="##viewApprovalBtn">

	<div class="form-group row" id="tr_approval">
		<div class="col-xs-12 col-sm-3 control-label" id="approval_label">
			<label for="">phr_ApprovalStatus</label>
		</div>
		<div class="col-xs-12 col-sm-9">
			<cfoutput><a href="/approvalEngine/approvalEngineStatusListing.cfm?entityID=#entityUniqueID#&entityTypeID=#entityTypeID#" id="viewApprovalBtn" class="btn btn-primary">phr_#phrasePrefix#View</a></cfoutput>
		</div>
	</div>
</cfif>