<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approverResponse.cfm
Author:				YMA
Date started:		17/07/2014

Description:		tag to register approvers response and trigger next action in the approval engine.

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/01/07	NJH	JIRA BF-202 - changed friendlyName to label...
2015/11/27	YMA	(2015/11/27 by Yan, P-PAN001 CR001 Custom to Core. Bring details of the related object into the approval review screen.
Possible enhancements:


 --->
<cfparam name="entityTypeID">
<cfparam name="approvalStatusRelatedEntityID">
<cfparam name="approverID">
<cfparam name="approvalEngineID">

<!--- <cfif approverID NEQ request.relayCurrentUser.personID>
	<cfoutput>You are not authorised to do approvals.</cfoutput><cfabort>
</cfif> --->
<cfif not isDefined("entityTypeID") or not isDefined("approvalStatusRelatedEntityID") or not isDefined("approverID")>
	<cfoutput>You must pass in entityTypeID and approvalStatusRelatedEntityID for this tag.</cfoutput><cfabort>
</cfif>

<cf_checkFieldEncryption fieldNames= "entityTypeID,approvalStatusRelatedEntityID,approverID">

<cfif structKeyExists(form,"submit")>
	<cfset canApprove = application.com.approvalEngine.checkDupeApprovalDecision(entityID=approvalStatusRelatedEntityID,approverID=approverID,entityTypeID=entityTypeID,approvalEngineID=approvalEngineID)>

	<cfif canApprove>
		<cf_updatedata>
		<!--- submit response --->
		<cfset result = application.com.approvalEngine.approvalRuleHandler(mode="respond",entityTypeID=entityTypeID,approvalStatusRelatedEntityID=approvalStatusRelatedEntityID,responseForm=form,approvalEngineID=approvalEngineID)>

		<!--- return completion message --->
		<cfif listFind('approved,rejected,revisionNeeded',result.approvalStatus)>
			<cfif result.approvalStatus eq 'approved'>
				<cfoutput>phr_approvalComplete</cfoutput>
			<cfelseif result.approvalStatus eq 'rejected'>
				<cfoutput>phr_approvalRejected</cfoutput>
			<cfelse>
				<cfoutput>phr_approvalRevisionNeeded</cfoutput>
			</cfif>

			<cfif structKeyExists(result,"workflowID") and result.workflowID neq "">
				<CFSET frmPersonID = #request.relaycurrentuser.personID#>
				<CFSET frmProcessID = #result.workflowID#>
				<CFSET entityID = #approvalStatusRelatedEntityID#>
				<CFSET entityTypeID = #entityTypeID#>
				<CFSET frmstepid=0>
				<cfinclude template="\screen\redirector.cfm">
			<cfelse>
				<cfoutput>#application.com.relayUI.message(message="Workflow ID not found",type="error")#</cfoutput>
			</cfif>
		<cfelseif result.approvalStatus eq 'partialyApproved'>
			<cfoutput>phr_PartyApprovedFeedback #result.level+1# phr_approvers</cfoutput>
		</cfif>
	<cfelse>
			<cfoutput>phr_approvalEngine_This #application.entitytype[entityTypeID].label# phr_approvalEngine_alreadyApproved</cfoutput>
	</cfif>
<cfelse>
	<!--- display form to register response --->
	<cfset canApprove = application.com.approvalEngine.checkDupeApprovalDecision(entityID=approvalStatusRelatedEntityID,approverID=approverID,entityTypeID=entityTypeID,approvalEngineID=approvalEngineID)>
	<cfset approvalEngineApproverDetails = application.com.approvalEngine.getApprovalEngineApproverDetails(approvalEngineID=approvalEngineID, approvalStatusRelatedEntityID=approvalStatusRelatedEntityID)>

	<cfif approvalEngineApproverDetails.approvalCriteriaMet eq true>

		<cfif canApprove>
			<cfform action="#CGI.SCRIPT_NAME#?#request.query_string#" method="POST" enctype="multipart/form-data" name="approverResponse">

				<cf_relayFormDisplay>

					<cfset recordBeingApproved = application.com.relayentity.getEntityByForeignKey(
													entity="#application.entitytype[entityTypeID].TABLENAME#",
													ColumnName="#application.entitytype[entityTypeID].uniqueKey#",
													ColumnData=approvalStatusRelatedEntityID
													)>
					<cfset approvalEngineDetails=application.com.approvalengine.getApprovalEngineSummary(approvalEngineID = approvalEngineID)>

					<cfif isDefined("approvalEngineDetails.approvalReviewScreenID") and approvalEngineDetails.approvalReviewScreenID neq "">
						<cfset params["#application.entitytype[entityTypeID].uniqueKey#"] = approvalStatusRelatedEntityID>
						<cfset params["#application.entitytype[entityTypeID].tablename#"] = recordBeingApproved>
						<cfset params["#application.entitytype[entityTypeID].uniqueKey#list"] = approvalStatusRelatedEntityID>
						<cfif isDefined("approvalEngineDetails.countryID")>
							<cfset params["countryID"] = approvalEngineDetails.countryID>
						<cfelseif listFindNoCase("organisation,person,location",application.entitytype[entityTypeID].tablename )>
							<cfset entityStructure = application.com.relayplo.getEntityStructure(entityTypeID=entityTypeID,entityID=approvalStatusRelatedEntityID)>

							<cfset params["countryID"] = entityStructure.countryID>

						<cfelse>
							<cfset params["countryID"] = request.relaycurrentuser.countryID>
						</cfif>
						<cfset params["screenid"] = approvalEngineDetails.approvalReviewScreenID >

						<!--- 2015/11/27 YMA 	P-PAN001 CR001 Custom to Core.  Previousely was necessary to include a custom file to bring details of the related object 
												into the approval review screen.  This code is designed to do this flexibly for any 'custom entity'. --->
						<!--- get custom entity related entity details entity --->
						<!--- first check this is a custom entity --->
						<cfquery name="enityIsCustomEntity">
							select customEntity from schemaTableBase where entityTypeID = #entityTypeID#
						</cfquery>
						<!--- if it is a custom entity... --->
						<cfif entityTypeID gte 1000 and enityIsCustomEntity.recordcount gt 0>
							<!--- get relationship to its foreign Key --->
							<cfquery name="foreignKeyRelationship">
								SELECT
									OBJECT_NAME(fk.referenced_object_id) 'entityTable',
									c2.name 'uniqueKey'
								FROM
									sys.foreign_keys fk
								INNER JOIN
									sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
								INNER JOIN
									sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
								WHERE OBJECT_NAME(fk.parent_object_id) = '#application.entitytype[entityTypeID].tablename#'
							</cfquery>
							<!--- get foreign key entity --->
							<cfquery name="foreignKeyEntity" datasource="#application.sitedatasource#">
								select top 1 #foreignKeyRelationship.entityTable#.#foreignKeyRelationship.uniqueKey# as entityID from #foreignKeyRelationship.entityTable# 
									join #application.entitytype[entityTypeID].tablename# 
										on #foreignKeyRelationship.entityTable#.#foreignKeyRelationship.uniqueKey# = #application.entitytype[entityTypeID].tablename#.#foreignKeyRelationship.uniqueKey#
									where #application.entitytype[entityTypeID].tablename#.#application.entitytype[entityTypeID].uniqueKey# = #approvalStatusRelatedEntityID#
							</cfquery>
							<!--- set foreign key entity so it is available in screen --->
							<cfset params["#foreignKeyRelationship.uniqueKey#"] = foreignKeyEntity.entityID>
							<!--- get entity details --->
							<cfset entityDetails = application.com.relayEntity.getEntityStructure(entityID=foreignKeyEntity.entityID,entityTypeID=application.entityTypeID[foreignKeyRelationship.entityTable],getEntityQueries=true)>
							<!--- set entity details so they are available in screen --->
							<cfloop collection=#entityDetails.Queries# item="structure">
								<cfset params[structure] = entityDetails.Queries[structure]>
							</cfloop>
						</cfif>
						
						<!--- show screen --->
						<cf_aScreen formname="approverResponse">
							<cf_aScreenItem attributecollection="#params#" method="edit">
						</cf_aScreen>
					<cfelse>
						<!--- Need to have an iframe or something to show the entity that is being approved in view only mode. --->
						<cfoutput>
							<h3>phr_approvalHasBeenRequestedForThis #application.entitytype[entityTypeID].label#</h3>
							<table>
								<cfloop list="#recordBeingApproved.columnList#" index="col">
									<cfif recordBeingApproved[col][1] neq "">
										<tr>
											<cfloop query="recordBeingApproved">
												<td>#col#</td>
												<td>#recordBeingApproved[col][currentRow]#</td>
											</cfloop>
										</tr>
									</cfif>
								</cfloop>
							</table>
						</cfoutput>
					</cfif>

					<cf_querySim>
						decision
						display,value
						Phr_approvalDecision_Approve|1
						Phr_approvalDecision_Reject|0
						Phr_approvalDecision_revisionNeeded|-1
					</cf_querySim>
					<cf_relayFormElementDisplay label="phr_approvalEngine_approvalDecision" currentValue="" fieldName="approvalDecision" relayFormElementType="radio" query="#decision#" display="display" value="value">
					<cf_relayFormElementDisplay label="phr_approvalEngine_reason" currentValue="" fieldName="reason" relayFormElementType="textArea" maxLength="500">

					<cf_relayFormElementDisplay label=" " currentValue="#entityTypeID#" fieldName="entityTypeID" relayFormElementType="hidden">
					<cf_relayFormElementDisplay label=" " currentValue="#approvalStatusRelatedEntityID#" fieldName="approvalStatusRelatedEntityID" relayFormElementType="hidden">
					<cf_relayFormElementDisplay label=" " currentValue="#approverID#" fieldName="approverID" relayFormElementType="hidden">
					<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="submit" currentValue="phr_approvalDecision_submit" label="" class="button">
				</cf_relayFormDisplay>
			</cfform>
		<cfelse>
			<cfoutput>
				phr_approvalEngine_This #application.entitytype[entityTypeID].label# phr_approvalEngine_alreadyApproved
			</cfoutput>
		</cfif>
	<cfelse>
		<cfoutput>
			phr_approvalEngine_This #application.entitytype[entityTypeID].label# phr_approvalEngine_doesNotMeetApprovalCriteria
		</cfoutput>
	</cfif>
</cfif>