<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approvalEngineSetup.cfm
Author:				YMA
Date started:		10/07/2014

Description:		Setup approval engines.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:

2016/01/07	NJH	JIRA BF-202 - changed friendlyName to label...

Possible enhancements:


 --->
<cf_title>Approval Engine Setup</cf_title>
<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="approvalEngineEntityType">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="variables.showTheseColumns" default="title,approvalEngineEntityType,maxLevel,numApprovers,numRequestsMade"/>

<cfif not  structKeyExists(url,"editor")>
	<CF_RelayNavMenu pageTitle="Approval Engine Setup" thisDir="" navInclude="">
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="approvalEngineSetup.cfm?editor=yes&add=yes&entitytypeID=#application.entityTypeID['approvalEngine']#">
			<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
	</CF_RelayNavMenu>
</cfif>

<cfif structKeyExists(url,"approvalEngineID")>
	<cfset form.entityTypeID = application.com.approvalEngine.getApprovalEngineSummary(url.approvalEngineID).APPROVALENGINEENTITYTYPEID>
</cfif>

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="505" name="thisEditor" entity="approvalEngine" title="Approval Engine Editor">
			<field name="approvalEngineID" label="phr_approvalEngine_ID" description="This records unique ID." control="html"></field>
			<field name="title" label="phr_approvalEngine_title" description="The approval engine title." control="text"></field>
			<cfif structKeyExists(form,"entityTypeID") and form.entityTypeID neq "">
				<field name="displayEntityTypeID" label="phr_approvalEngine_relatedEntity" description="The entity this approval engine is for." control="html">
					<cfsavecontent variable="displayEntityTypeID">
						<cfoutput>
							<cfquery name="displayEntityTypeIDQuery" datasource="#application.sitedatasource#">
								select label as display from schematable
								where flagsexist = 1 and uniqueKey is not null and entityTypeID =  <cf_queryparam value="#form.entityTypeID#" CFSQLTYPE="cf_sql_integer" >
							</cfquery>
							#displayEntityTypeIDQuery.display#
						</cfoutput>
					</cfsavecontent>
					#htmlEditFormat(displayEntityTypeID)#
				</field>
				<field control="hidden" name="entityTypeID" label="" description="The entity this approval engine is for."></field>
			<cfelse>
				<field nullText="phr_ext_selectavalue" shownullvalue="true" name="entityTypeID" label="phr_approvalEngine_relatedEntity" description="The entity this approval engine is for." control="select"
								query="select entityTypeID as value, label as display from schematable
										where flagsexist = 1 and uniqueKey is not null
										order by label">
				</field>
			</cfif>
			<field nullText="phr_ext_selectavalue" shownullvalue="true" name="approvedWorkflowID" label="phr_approvalEngine_approvedWorkflow" description="A workfflow to process when an approval request is approved." control="select"
							query="select processID as value, processDescription as display from processheader order by processDescription asc">
			</field>
			<field nullText="phr_ext_selectavalue" shownullvalue="true" name="rejectedWorkflowID" label="phr_approvalEngine_rejectionWorkflow" description="A workfflow to process when an approval request is rejected." control="select"
							query="select processID as value, processDescription as display from processheader order by processDescription asc">
			</field>
			<field nullText="phr_ext_selectavalue" shownullvalue="true" name="revisionWorkflowID" label="phr_approvalEngine_revisionWorkflow" description="A workfflow to process when an approval request needs revision." control="select"
							query="select processID as value, processDescription as display from processheader order by processDescription asc">
			</field>
			<field nullText="phr_ext_selectavalue" shownullvalue="true" name="approvalRequestEmailID" label="phr_approvalEngine_approvalRequestEmail" description="The email sent to approvers when they need to make an approval decision." control="select"
							query="select emailDefID as value, emailTextID as display from emaildef order by emailTextID asc">
			</field>
			<field nullText="phr_ext_selectavalue" shownullvalue="true" name="approvalCopyRequestEmailID" label="phr_approvalEngine_approvalCopyEmail" description="The email copy sent to users to notify them without the ability to make an approval decision." control="select"
							query="select emailDefID as value, emailTextID as display from emaildef order by emailTextID asc">
			</field>
			<field name="approvalReviewScreenID" label="phr_approvalEngine_approvalReviewScreen" description="" control="select" bindfunction="cfc:relay.webservices.relayscreens.entityScreens(entityTypeID={entityTypeID})" display="displayvalue" value="screenID" nullText="phr_ext_selectAvalue"></field>

			<cfif (structKeyExists(form,"entityTypeID") and form.entityTypeID neq "") and (isDefined("approvalEngineID") or (not structKeyExists(url,"add")))>
				<field name="" control="includeTemplate" template="/approvalEngine/approvalEngineCriteria.cfm" spanCols="true" valueAlign="middle"/>
			</cfif>
			<cfif not structKeyExists(url,"add") or structKeyExists(form,"added")>
				<field name="" control="includeTemplate" template="/approvalEngine/approvalEngineApproverSetupInclude.cfm" spanCols="true" valueAlign="middle"/>
			</cfif>

			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not isDefined("URL.editor")>
	<cf_includeJavascriptOnce template = "/javascript/fileDownload.js">

	<cfset getData = application.com.approvalEngine.getApprovalEngineSummary()>

	<cfquery name="getData" dbType="query">
		select * from getData
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
<cfelse>

</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="approvalEngineSetup"
	keyColumnList="title"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('phr_ApprovalEngine ##jsStringFormat(approvalEngineID)##'*comma'phr_ApprovalEngine ##jsStringFormat(approvalEngineID)##'*comma'/approvalEngine/approvalEngineSetup.cfm?editor=yes&hideBackButton=true&approvalEngineID=##jsStringFormat(approvalEngineID)##'*comma{reuseTab:true*commaiconClass:'approval'});return false;"
	showTheseColumns="#variables.showTheseColumns#"
	FilterSelectFieldList="approvalEngineEntityType"
	useInclude="false"
	sortOrder="#sortOrder#"
	hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	columnTranslation="true"
	columnTranslationPrefix="phr_approvalEngine_"
>