<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approvalEngineApproverCriteria.cfm
Author:				YMA
Date started:		31/07/2014

Description:		Define criteria filters for the current approver.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:

 --->

	<cfset getCriteria = "">
	
	<cfif isDefined("filterCriteriaID")>
		<cfset getCriteria = application.com.filterSelectAnyEntity.getFilterCriteria(filterCriteriaID=filterCriteriaID)>
	</cfif>
	
	<cfset approvalEngine = application.com.approvalEngine.getApprovalEngineSummary(approvalEngineID=approvalEngineID)>
	
	<h3>Approver Criteria</h3>
	<p>Add criteria for the approval request to goto this approver. Leaving this empty will mean all requests goto this approver.</p>
	<cf_filterSelectAnyEntity entityTypeID = #approvalEngine.APPROVALENGINEENTITYTYPEID# relatedEntityID=#form.approvalEngineApproverID# relatedEntityTypeID=#application.entitytypeid["approvalEngineApprover"]#>