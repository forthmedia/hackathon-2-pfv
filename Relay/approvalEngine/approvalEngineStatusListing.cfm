<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approverEngineStatusListing.cfm
Author:				YMA
Date started:		17/07/2014

Description:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/01/07	NJH	JIRA BF-202 - changed friendlyName to label...

Possible enhancements:


 --->
<cfset request.document.addHTMLDocumentTags = true>
<cf_displayborder noborders = true classString="lowerlevel">

<cf_includeCssOnce template="/code/styles/approvalModal.css">

<cfparam name="variables.entityID" default="101">
<cfparam name="variables.entityTypeID" default="101">
<cfset variables.entityID = val(trim(url.entityID))>
<cfset variables.entityTypeID = val(trim(url.entityTypeID))>
<cfset bShowApproveBtn = true>

<cfset recordBeingApproved = application.com.relayentity.getEntityByForeignKey(
									entity="#application.entitytype[variables.entityTypeID].TABLENAME#",
									ColumnName="#application.entitytype[variables.entityTypeID].uniqueKey#",
									ColumnData=variables.entityID
									)>
<cfquery dbtype="query" name="nameExpression">
	select
			<cfif application.entitytype[variables.entityTypeID].nameExpression neq "">
					#REReplaceNoCase(application.entitytype[variables.entityTypeID].nameExpression,"''","'","all")#
			<cfelse>
					#application.entitytype[variables.entityTypeID].uniqueKey#
			</cfif> as nameExpression
	from recordBeingApproved
</cfquery>

<cfset local.approvalEngineID = application.com.approvalEngine.getApprovalEngineIDByEntityType(entityTypeID=variables.entityTypeID,entityID=variables.entityID)>

<cfloop list="#local.approvalEngineID#" index="thisApprovalEngineID">

	<cfset approvalEngineApproverDetails = application.com.approvalEngine.getApprovalEngineApproverDetails(approvalEngineID=thisApprovalEngineID, approvalStatusRelatedEntityID=variables.entityID)>
	<cfset levelClass="">
	<cfoutput>
		<h1>#approvalEngineApproverDetails.title# phr_approvalListing_ApprovalStatus</h1>
		<p>#application.entitytype[variables.entityTypeID].label#: #nameExpression.nameExpression#</p>
		<table class="responsiveTable table table-striped table-bordered table-condensed withBorder" data-mode="reflow" data-role="table">
			<thead>
				<tr>
					<th data-colstart="1">phr_approvalListing_Level</th>
					<th data-colstart="2">phr_approvalListing_Approver</th>
					<th data-colstart="3">phr_approvalListing_Status</th>
					<th data-colstart="4">phr_approvalListing_Date</th>
					<th data-colstart="5">phr_approvalListing_Comments</th>
				</tr>
			</thead>
			<tbody>
				<cfif structKeyExists(approvalEngineApproverDetails,'previouslevelapprovers')>
					<cfset prevApprovalKeyList = ListSort(structKeyList(approvalEngineApproverDetails.previouslevelapprovers),'numeric')>

					<cfloop list="#prevApprovalKeyList#" index="thisKey">
						<cfif thisKey lte approvalEngineApproverDetails.currentLevel>
							<cfloop collection="#approvalEngineApproverDetails.previouslevelapprovers[thisKey]#" item="approver">
								<cfset levelClass="levelBorder">
								<tr class="">
									<td>
										<div id="approveLevel" class="#levelClass#">#thiskey#</div>
									</td>
									<td>
										#approvalEngineApproverDetails.previouslevelapprovers[thisKey][approver]['approverEntity']#
									</td>
									<td>
										phr_approvalListing_#approvalEngineApproverDetails.previouslevelapprovers[thisKey][approver]['approvalStatusTypeTextID']#
									</td>
									<td>
										#dateFormat(approvalEngineApproverDetails.previouslevelapprovers[thisKey][approver]['approvedStatusCreated'],'dd-mmm-yyyy')# #timeFormat(approvalEngineApproverDetails.previouslevelapprovers[thisKey][approver]['approvedStatusCreated'],'HH:mm')#
									</td>
									<td>#approvalEngineApproverDetails.previouslevelapprovers[thisKey][approver]['reason']#</td>
								</tr>
							</cfloop>
						</cfif>
					</cfloop>
				</cfif>

				<cfif approvalEngineApproverDetails.approvalCriteriaMet eq true>
					<cfif structKeyExists(approvalEngineApproverDetails,'nextlevelapprovers')>
						<cfset approvalKeyList = ListSort(structKeyList(approvalEngineApproverDetails.nextlevelapprovers),'numeric')>
						<cfloop list="#approvalKeyList#" index="thisKey">
							<cfset levelClass="levelBorder">
							<cfloop from="1" to="#ArrayLen(approvalEngineApproverDetails.nextlevelapprovers[thisKey])#" index="j">
								<cfif application.com.filterSelectAnyEntity.runFilterAgainstEntity(
																filterEntityTypeID= application.entityTypeID["approvalEngineApprover"],
																filterEntityID=approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverID'],
																relatedEntityID=variables.entityID,
																relatedEntityTypeID=variables.entityTypeID
																).recordcount gt 0 and not listFindNoCase("approved,rejected",approvalEngineApproverDetails.CURRENTSTATUSTEXTID)>
									<tr class="">
										<td>
												<div id="approveLevel" class="#levelClass#">#thiskey#</div>
										</td>
										<td>
											#approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntity']#
										</td>
										<td>
											phr_approvalListing_PendingApproval
										</td>
										<td>
											<cfif approvalEngineApproverDetails.CURRENTSTATUSTEXTID eq "revisionNeeded"
											and structKeyExists(approvalEngineApproverDetails.previouslevelapprovers[approvalEngineApproverDetails.currentLevel+1],approvalEngineApproverDetails.nextlevelapprovers[thisKey][j].APPROVERID)>
												<cfset previouseApproverAtCurrentLevel = approvalEngineApproverDetails.previouslevelapprovers[approvalEngineApproverDetails.currentLevel+1][approvalEngineApproverDetails.nextlevelapprovers[thisKey][j].APPROVERID]>
												#dateFormat(previouseApproverAtCurrentLevel.APPROVEDSTATUSCREATED,'dd-mmm-yyyy')# #timeFormat(previouseApproverAtCurrentLevel.APPROVEDSTATUSCREATED,'HH:mm')#
											</cfif>
										</td>
										<td>
											<cfif approvalEngineApproverDetails.CURRENTSTATUSTEXTID eq "revisionNeeded"
											and structKeyExists(approvalEngineApproverDetails.previouslevelapprovers[approvalEngineApproverDetails.currentLevel+1],approvalEngineApproverDetails.nextlevelapprovers[thisKey][j].APPROVERID)>
												#previouseApproverAtCurrentLevel.reason#
											<cfelse>
												<cfif (
														(approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntityType'] eq "person"
															and request.relaycurrentuser.personid EQ approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntityID'])
														OR
														(approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntityType'] eq "usergroup"
															and listFind('#request.relaycurrentuser.usergroups#',approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntityID']))
														OR
														(approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntityType'] eq "flag"
															and application.com.flag.isFlagSetForCurrentUser(flagID=approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverEntityID']))
														)
														AND (thisKey EQ approvalEngineApproverDetails.currentLevel + 1)>
													<cfset approverID = approvalEngineApproverDetails.nextlevelapprovers[thisKey][j]['approverID']>
													<Cfset encryptedURL = application.com.security.encryptQueryString(queryString="entityTypeID=#variables.entityTypeID#&approvalStatusRelatedEntityID=#variables.entityID#&approverID=#approverID#&approvalEngineID=#thisApprovalEngineID#")>
													<Cfset onclick = "parent.window.location.href='/?eid=approvalEngine&#encryptedURL#'; parent.jQuery.fancybox.close();">
													<cfif request.relaycurrentuser.isInternal>
														<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
														<Cfset onclick = "openNewTab('phr_approvalEngine_approvalDecision #jsStringFormat(application.entitytype[variables.entityTypeID].uniqueKey)#: #jsStringFormat(variables.entityID)#','phr_approvalEngine_approvalDecision #jsStringFormat(application.entitytype[variables.entityTypeID].uniqueKey)#: #jsStringFormat(variables.entityID)#','/approvalEngine/approverResponse.cfm?#jsStringFormat(encryptedURL)#',{reuseTab:true,iconClass:'approval'});">
														<!--- <Cfset onclick = "window.open('#application.com.relaycurrentsite.getExternalSiteURL()#/?eid=approvalEngine&#encryptedURL#','_blank');"> --->
													</cfif>
													<cf_input type="button" value="phr_ApprovalEngine_ApproveNow" name="frmClose" onclick="#onclick#">
												</cfif>
											</cfif>
										</td>
									</tr>
								</cfif>
							</cfloop>
						</cfloop>
					</cfif>
				</cfif>
			</tbody>
		</table>
	</cfoutput>
</cfloop>
<cfif local.approvalEngineID eq ''>
	<cfoutput>No approval engine setup for #application.entitytype[variables.entityTypeID].TABLENAME#</cfoutput>
</cfif>

</cf_displayborder>