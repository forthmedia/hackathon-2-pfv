<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approvalEngineCriteria.cfm
Author:				YMA
Date started:		31/07/2014

Description:		Define criteria filters for the triggering of the approval process.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:

 --->

	<cfset getCriteria = "">
	
	<cfif isDefined("filterCriteriaID")>
		<cfset getCriteria = application.com.filterSelectAnyEntity.getFilterCriteria(filterCriteriaID=filterCriteriaID)>
	</cfif>
	
	<cfset approvalEngine = application.com.approvalEngine.getApprovalEngineSummary(approvalEngineID=approvalEngineID)>
	
	<h3>Triggering Criteria</h3>
	<p>Specify any criteria that must be met for the approval process to trigger.  Leave blank to trigger on any record being added.</p>
	<cf_filterSelectAnyEntity entityTypeID = #form.entityTypeID# relatedEntityID=#form.approvalEngineID# relatedEntityTypeID=#application.entitytypeid["approvalEngine"]#>