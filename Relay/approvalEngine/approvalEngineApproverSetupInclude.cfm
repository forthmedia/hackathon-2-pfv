<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			approvalEngineApproverSetupInclude.cfm	
Author:				YMA
Date started:		11/07/2014
	
Description:		Include Setup cfm for approval engines.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:


 --->

<cfif not Isdefined("AddNew")>
	<cfoutput>
		<iframe src="/approvalEngine/approvalEngineApproverSetup.cfm?approvalEngineID=#htmleditformat(form.approvalEngineID)#" width="100%" height="400px" scrolling="auto" frameborder=0></iframe>
	</cfoutput>
</cfif>