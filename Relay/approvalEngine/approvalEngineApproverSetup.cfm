<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			approvalEngineApproverSetup.cfm
Author:				YMA
Date started:		11/07/2014

Description:		Include within approvalEngineSetup.cfm to setup approvers related to the approvalEngine.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2016/01/07	NJH	JIRA BF-202 - changed friendlyName to label...
2016-02-18 	YMA 	(2015-11-27 by Yan, committed by RJT) P-PAN001 CR001 Updated approval engine to handle approver identified
Possible enhancements:

 --->

<cf_title>Approval Engine Approver Setup</cf_title>

<cfparam name="approvalEngineID" type="numeric">
<cfif (structKeyExists(form,"Update") or structKeyExists(form,"Added") or (isDefined("approverDelete"))) and (structKeyExistS(form,"approvalEngineApproverID") or structKeyExistS(url,"approvalEngineApproverID"))>
	<cfif isDefined("approverDelete")>
		<cftransaction>
			<cftry>
				<cfquery name="deleteApproverFilterCriteria" datasource="#application.siteDataSource#">
					delete filterCriteria
					where entityTypeID = <cfqueryparam cfsqltype="cf_sql_integer" value="#application.entityTypeID['approvalEngineApprover']#">
					and entityID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.approvalEngineApproverID#">
				</cfquery>
				<cfquery name="deleteApprover" datasource="#application.siteDataSource#">
					delete approvalEngineApprover
					where approvalEngineApproverID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.approvalEngineApproverID#">
				</cfquery>
				<cftransaction action="commit"/>
				<cfcatch type="Database">
					<cftransaction action="rollback"/>
					<cfoutput>There was an error deleting the Approver</cfoutput>
				</cfcatch>
			</cftry>
		</cftransaction>
	</cfif>
</cfif>

<cfif approvalEngineID neq 0>

	<cfif structKeyExists(url,"approverEditor") and url.approverEditor eq "yes">
		<cfsavecontent variable="xmlSource">
			<cfoutput>

			<editors>
				<editor id="505" name="thisEditor" entity="approvalEngineApprover" title="Approval Engine Editor">
					<field name="approvalEngineApproverID" label="phr_approvalEngineApprover_ID" description="This records unique ID." control="html"></field>
					<field name="approvalEngineID" label="phr_approvalEngine_ID" default="#approvalEngineID#" control="hidden"></field>
					<field name="approverLevel" label="phr_approvalEngineApprover_approverLevel" description="The level of the approver" control="numeric"></field>
					<!--- 2015-11-27 YMA P-PAN001 CR001 Updated approval engine to handle approver identified by integerflagdata --->
					<field nullText="phr_ext_selectavalue" name="entityTypeID" label="phr_approvalEngine_approvalEntityTypeID" description="Identify the type of approver" control="select"
							query="select entityTypeID as value, CASE WHEN label = 'flag' THEN 'Profile attribute' WHEN label = 'IntegerFlagData' THEN 'Key person' WHEN label = 'person' THEN 'Specific person'  WHEN label = 'UserGroup' THEN 'User group' ELSE label END as display from schematable where entityname in ('person','usergroup','flag','integerFlagData')">
						</field>
					<!--- 2015-11-27 YMA P-PAN001 CR001 Necessary to pass approvalEngineID webservice to handle approver identified by integerflagdatae --->
					<field nullText="phr_ext_selectavalue" bindonload="true" shownullvalue="true" name="approverEntityID" label="phr_approvalEngine_approverEntityID" description="The Approver." control="select" display="display" value="value"
							bindFunction="cfc:webservices.approvalEngineWS.callMethod(methodName='getEntitiesForEntityType',entityTypeID={entityTypeID},approvalEngineID={approvalEngineID},returnType='query')"/>
					<cfif isDefined("approvalEngineApproverID") and (not structKeyExists(url,"add"))>
						<field name="" control="includeTemplate" template="/approvalEngine/approvalEngineApproverCriteria.cfm" spanCols="true" valueAlign="middle"/>
					</cfif>
				</editor>
			</editors>
			</cfoutput>
		</cfsavecontent>

		<cfif not isDefined("approverAdd")>
			<cfset approverAdd = "no">
		</cfif>

		<CF_RelayXMLEditor
			xmlSourceVar = "#xmlSource#"
			editorName = "thisEditor"
			showSaveAndReturn = "no"
			showSave="yes"
			add="#approverAdd#"
		>
	<cfelse>
		<cfset getData = application.com.approvalEngine.getApprovalEngineApprovers(approvalEngineID=approvalEngineID)>
		<cf_relayFormDisplay>

		<cfif getData.recordCount gt 0>
			<table class="table table-hover table-striped table-bordered">
				<tr>
					<th>phr_approvalEngine_approverEntity</th>
					<th>phr_approvalEngine_approverType</th>
					<th>phr_approvalEngine_approverLevel</th>
					<th>&nbsp;</th>
				</tr>
				<cfloop query="getData">
					<tr valign="top">
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="<a href='/approvalEngine/approvalEngineApproverSetup.cfm?approverEditor=yes&approvalEngineApproverID=#approvalEngineApproverID#&approvalEngineID=#approvalEngineID#'>#htmleditformat(approverEntity)#</a>" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#approverType#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#approverLevel#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="<a  class='btn btn-primary' href='/approvalEngine/approvalEngineApproverSetup.cfm?approverDelete=yes&approvalEngineApproverID=#approvalEngineApproverID#&approvalEngineID=#approvalEngineID#'>phr_delete</a>" fieldname="" label=""></td>
					</tr>
				</cfloop>
			</cfif>
				<tr>
					<td colspan="99" align="right" >
						<a class="submenu btn btn-primary" href='/approvalEngine/approvalEngineApproverSetup.cfm?approverEditor=yes&approverAdd=yes&approverEntityID=0&approvalEngineID=<cfoutput>#htmleditformat(approvalEngineID)#</cfoutput>'><b>Add New Approver</b></a>
					</td>
				</tr>
			</table>
		</cf_relayFormDisplay>
	</cfif>
</cfif>