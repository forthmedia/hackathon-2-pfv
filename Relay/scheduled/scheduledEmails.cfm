<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scheduledEmail.cfm	
Author:				AJC
Date started:		2010-06-18
	
Description:		Sends scheduled emails

Amendment History:

Date 		Initials 	What was changed
2011/07/10 	PPB			REL106 re-vamped buildSQL AND removed buildEntitySQL (which was buildOpportunitySQL) in cfc to avoid joins for each recipient
2012/09/10	IH			Case 430187 if one scheduled email fails just notify support and process the rest

Possible enhancements:
TODO: create a setting for emailBatchAmount

 --->

<cf_scheduledTask name="Triggered Emails">
<!--- get the current date time --->
<cfparam name="emailBatchAmount" default="200" />

<cfset currentdatetime = CreateDateTime(year(now()),month(now()),day(now()),hour(now()),minute(now()),second(now()))>

<!--- get the schedules that need to run at this time --->
<cfset getScheduledEmailList = application.com.scheduledEmails.getEmailSchedule(currentdatetime=currentdatetime) />

<!--- loop through the scheduled that need to be run --->
<cfloop query="getScheduledEmailList">
	
	<cftry>
		<cfset entityIdString = "#getScheduledEmailList.tablename#.#application.entityType[application.entityTypeID[getScheduledEmailList.tablename]].UniqueKey#">
		
		<!--- get the recipient columns list for the schedule --->
		<cfset qry_get_Recipients = application.com.scheduledEmails.getRecipients(scheduleID) />
		<!--- run the SQL stored in the schedule table --->
		<cfquery name="qry_schedule_query" datasource="#application.sitedatasource#">
			#preservesinglequotes(scheduleQuery)#
		</cfquery>
	
		<!--- loop through the schedules results --->
		<cfloop query="qry_schedule_query">
			<!--- loop through the recipients of the schedule --->
			<cfloop query="qry_get_Recipients">
				<!--- check to see if the recipient has already receive the email --->
				<!--- <cfif evaluate("qry_schedule_query.sent#RecipientValue#") eq 0> --->
	
					<cfif left(qry_get_Recipients.recipientValue,5) eq "flag_">
						<!--- <cfset personId = qry_schedule_query["#MID(RecipientValue,6,99)#"]>  previously we returned recipeints in qry_schedule_query --->
						<cfset personId =  application.com.flag.getFlagData(flagId='#MID(qry_get_Recipients.recipientValue,6,99)#',entityId=qry_schedule_query.entityID).data> 
					<cfelse>
						<cfquery name="qryRecipient" datasource="#application.sitedatasource#">
							SELECT #RecipientValue# FROM #getScheduledEmailList.tableName# WHERE #entityIdString# =  <cf_queryparam value="#qry_schedule_query.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
						
						<cfset personId = qryRecipient["#RecipientValue#"]>
						<!--- <cfset personId = qry_schedule_query["#RecipientValue#"]>  previously we returned recipeints in qry_schedule_query --->
					</cfif>	
					
					<!--- add the person to the send queue in the scheduleLog --->
					<cfif personId neq "" and personId gt 0>
						<cfset application.com.scheduledEmails.addEmailScheduleLog(scheduleID=getScheduledEmailList.scheduleID,entityID=qry_schedule_query.entityID,personID=personId,RecipientValue=RecipientValue) />
					</cfif>
				<!--- </cfif> --->
			</cfloop>
		</cfloop>
	<cfcatch>
			<cfmail to="#application.SupportEmailAddress#" from="errors@foundation-network.com" type="html" subject="Error: #SCHEDULENAME# Scheduled Email">
			<h2>Error on #CGI.SCRIPT_NAME#</h2>
			<cfdump var="#cfcatch#" label="cfcatch">
			</cfmail>			
		</cfcatch>
	</cftry>
	
</cfloop>



<!--- Send the next batch of emails from the schedule log (nnn) --->

<cfset qry_get_getScheduleEmailToSend = application.com.scheduledEmails.getScheduleEmailToSend(emailBatchAmount=emailBatchAmount) />
<cfoutput query="qry_get_getScheduleEmailToSend">
	<cfset mergeStruct = structNew()>
	<cfset variables.sent = 0 />
	<cfif personID gt 0>
		
		<cfset mergeStruct["#tablename#ID"] = entityID>

        <!--- PROD2016-794 - additional custom merge fields specific to this data object --->
        <cfif isNumeric(entityID) and tableName eq "trngPersonCertification">
            <cfquery name="qryCert" datasource="#application.sitedatasource#">
                SELECT distinct certificationID, certificationCode, certificationTitle, certificationDescription, personCertificationStatus,
                personCertificationRegistrationDate, personCertificationPassDate, personCertificationExpiredDate, personCertificationExpiryDate
                FROM vTrngPersonCertifications where personCertificationID=<cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER">
            </cfquery>
            <cfif qryCert.recordCount EQ 1>
                <cfset mergeStruct.certificationTitle = qryCert.certificationTitle>
                <cfset mergeStruct.certificationCode = qryCert.certificationCode>
                <cfset mergeStruct.certificationDescription = qryCert.certificationDescription>
                <cfset mergeStruct.personCertificationStatus = qryCert.personCertificationStatus>
                <cfset mergeStruct.personCertificationRegistrationDate = lsdateFormat(qryCert.personCertificationRegistrationDate, "medium")>
                <cfset mergeStruct.personCertificationPassDate = lsdateFormat(qryCert.personCertificationPassDate, "medium")>
                <cfset mergeStruct.personCertificationExpiredDate = lsdateFormat(qryCert.personCertificationExpiredDate, "medium")>
                <cfset mergeStruct.personCertificationExpiryDate = lsdateFormat(qryCert.personCertificationExpiryDate, "medium")>
            </cfif>
        </cfif>

		<!---<cfswitch expression="#tablename#">
			<!--- ? perhaps we don't need to set the POL tables to mergeStruct cos the info is derived from the person sent in, however if the recipient personID is at a different org/loc it may be confusing ? --->			
			
			<!--- add any merge fields that may be useful for all emails for an entity type --->
			
			<cfcase value="organisation">
				<cfset mergeStruct.organisation = application.com.relayPLO.getOrgDetails(organisationID=entityID) />
			</cfcase>

			<cfcase value="location">
				<cfset mergeStruct.location = application.com.relayPLO.getLocDetails(locationID=entityID) />
			</cfcase>

			<cfcase value="person">
				<cfset mergeStruct.person = application.com.relayPLO.getPersonDetails(personID=entityID) />
			</cfcase>
			
			<cfcase value="opportunity">
				<cfset mergeStruct.opportunity = application.com.opportunity.get(entityID) />
			
				<cfset mergeStruct.accountManager = application.com.commonQueries.getFullPersonDetails(mergeStruct.opportunity.vendorAccountManagerPersonID) />
	
				<cfset mergeStruct.EndCustomer = application.com.commonQueries.getFullPersonDetails(mergeStruct.opportunity.contactPersonID) />
	
				<cfset mergeStruct.partner = application.com.commonQueries.getFullPersonDetails(mergeStruct.opportunity.partnerSalesPersonID) />
	
				<cfset emailStage = createObject("component","relay.com.opportunity") />
				<cfset emailStage.dataSource = application.siteDataSource />
				<cfset emailStage.stageIDfilter = mergeStruct.opportunity.stageID />
				<cfset mergeStruct.Stage = emailStage.getstageID() />
			</cfcase>
			
		</cfswitch> --->
		
		<!--- add any merge fields that are specific to a particular email def --->
		<cfswitch expression="#eMailTextID#">
			<cfcase value="exampleEmailDefTextId">
				<cfset mergeStruct.ExamplePerson = application.com.commonQueries.getFullPersonDetails(mergeStruct.organisation.examplePersonID) />
			</cfcase>
		</cfswitch>
		
		<cfset application.com.email.sendEmail(emailtextid=eMailTextID,personid=personID,mergeStruct=mergeStruct)>
		<cfset variables.sent = 1 />
	<cfelse>
		<cfset variables.sent = 2 />
	</cfif>
	
	<cfset application.com.scheduledEmails.setScheduledEmailLogAsSent(scheduleEmailLogID=scheduleEmailLogID,scheduleEmailLogStatusID=variables.sent)>
</cfoutput>

</cf_scheduledTask>
