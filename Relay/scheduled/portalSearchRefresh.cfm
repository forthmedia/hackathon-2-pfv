<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting requesttimeout="6000" enablecfoutputonly="No">

<cfparam name="url.portalSearchID" default="">
<cfparam name="url.fileID" default="">

<cf_scheduledTask>

<cfset qPortalSearch = application.com.search.getPortalSearchSources(portalSearchID=url.portalSearchID)>

<cfloop query="qPortalSearch">
	
	<cfif collectionName is not "" and indexMethodName is not "">
		
		<cfset collectionName = "#collectionName#">		
		<cfset collectionPath = "#application.paths.userfiles#\portalSearchCollections">
		
		<cfif not directoryExists("#collectionPath#")>		
			<cfdirectory action="create" directory="#collectionPath#">
		</cfif>
		
		<cfif not directoryExists("#collectionPath#\#collectionName#")>	
			<cftry>		
				<cfcollection action="create" path="#collectionPath#" collection="#collectionName#" language="#language#">
				<cfcatch><cfoutput><p>#cfcatch.message# #cfcatch.detail#</p></cfoutput><cfabort></cfcatch>
			</cftry>
		</cfif>
		
		<!--- <cfset refreshMethod = "application.com.search.#indexMethodName#(fileID=url.fileID,collectionName=collectionName)"> --->
		
		<cfset functionArgs = {fileID=url.fileID,collectionName=collectionName}>
		
		<cftry>
			<!--- <cfset evaluate(refreshMethod)> --->
			
			<cfinvoke component=#application.com.search# method="#indexMethodName#" argumentCollection=#functionArgs# returnvariable="result">
			
			<cfcatch type="SearchEngine">
				<!--- if collection has been deleted try to recreate it --->
				<cftry>		
					<cfcollection action="create" path="#collectionPath#" collection="#collectionName#" language="#language#">
					<cfcatch><cfoutput><p>#cfcatch.message# #cfcatch.detail#</p></cfoutput><cfabort></cfcatch>
				</cftry>
				<cfinvoke component=#application.com.search# method="#indexMethodName#" argumentCollection=#functionArgs# returnvariable="result">
				<!--- <cfset evaluate(refreshMethod)> --->
			</cfcatch>	
			<cfcatch><cfdump var="#cfcatch#" label="Error Updating Collection"></cfcatch>
		</cftry>			
		<cfoutput><p>Collection #collectionName# updated.</p></cfoutput>			
		
	</cfif>
	
</cfloop>

</cf_scheduledTask>