<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scheduled/scheduledUpdateScorecards.cfm
Author:				NJH
Date started:		2007/03/09
	
Description:		Updates section and scorecard totals

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed
2009-04-27 			NYB			Trend Support 2141 - added errorCatch function, added cftry around all included tempates that call the new function on error
										added param scorecardTimeoutLimit and based RequestTimeout on this
Possible enhancements:
 --->

<cfparam name="scorecardTimeoutLimit" type="numeric" default="4000">
<cfparam name="numScorecards" type="numeric" default="750">

<cfoutput>
<cfsetting RequestTimeout = "#scorecardTimeoutLimit#">
</cfoutput>

<!--- update any flags that are required for scorecarding --->
<cfif fileExists("#application.paths.code#\cftemplates\updateScorecardFlags.cfm")>
	<cftry>
		<cfinclude template="/code/cftemplates/updateScoreCardFlags.cfm">
		<cfcatch type="any">
			<cfset errorCatch(errorMsg="File through error while running: content/CFTemplates/updateScoreCardFlags.cfm.",catchStruct=cfcatch)>
		</cfcatch>
	</cftry>
</cfif>

<cftry>
	<cfinclude template="/scorecard/updateScorecardTotal.cfm">
	<cfcatch type="any">
		<cfset errorCatch(errorMsg="File through error while running: scorecard/updateScorecardTotal.cfm.",catchStruct=cfcatch)>
	</cfcatch>
</cftry>

<!--- update scorecard tier mappings --->
<cftry>
	<cfinclude template="/scorecard/updateScoreCardTierMapping.cfm">
	<cfcatch type="any">
		<cfset errorCatch(errorMsg="File through error while running: scorecard/updateScoreCardTierMapping.cfm.",catchStruct=cfcatch)>
	</cfcatch>
</cftry>

<!--- update scorecard variance table --->
<cftry>
	<cfinclude template="/scorecard/updateScoreCardVarianceTable.cfm">
	<cfcatch type="any">
		<cfset errorCatch(errorMsg="File through error while running: scorecard/updateScoreCardVarianceTable.cfm.",catchStruct=cfcatch)>
	</cfcatch>
</cftry>

<cffunction name="errorCatch">
	<cfargument name="errorMsg" required="yes">
	<cfargument name="catchStruct" required="yes">
	<cfoutput><p>#htmleditformat(arguments.errorMsg)#<br/></p><cfdump var="#catchStruct#"></cfoutput>
	<cf_mail to="errors@foundation-network.com,relayhelp@foundation-network.com" 
        from="relayhelp@foundation-network.com"
        subject="Error in schedule process:  scheduledUpdateScorecards"
		type = "HTML"
	>
	<cfoutput><p>#arguments.errorMsg#</p><p>&nbsp;</p><cfdump var="#catchStruct#"></cfoutput>
	</cf_mail>	
</cffunction>

<p>Done</p>

