<!--- �Relayware. All Rights Reserved 2014

File name:		connectorSynch.cfm
Author:			NJH
Date started:		01/01/14

Description:		This provides.

Usage:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2015-07-01	WAB		Removed TRY/CATCH outside of cf_scheduledTask.  Was leaving orphaned processLocks.  Added onErrorEmailTo attribute to cf_scheduledTask
2015/07/02	NJH		changed time from 4 hours to 1 before sending the 'connector not running' message
2015/09/17	NJH		Don't allow the reset to kick the process again if a process lock still exists. Want to get rid of the reset option, but will do that slightly later
2015/11/09	NJH		Moved connector settings out of salesforce specific settings
2016/09/19	NJH		JIRA PROD2016-2354 - Removed the reset option. Not really needed anymore in current day connector. Having it on mean that intrastructure have to get involved when connector hasn't run a while
2017/02/08	RMP		PROD2016-2769 Connector: Retry single record synch. Added attributes direction and queueidList. In case they are provided, synch will be done based on directions specified or for queue ids.
Enhancements still to do:


 --->

<cfsetting requesttimeout="3600" enablecfoutputonly="true">

<cfparam name="debugLevel" default="0">
<cfparam name="connectorType" default="#application.com.settings.getSetting("connector.type")#">
<!--- <cfparam name="reset" default="false"> --->

	<cfif debugLevel eq 0>
		<cfset debugLevel = application.com.settings.getSetting("connector.debugLevel")>
	</cfif>

	<cfset scheduledTaskName = "#connectorType# Connector">
	<cfset remoteServerWasOffline = false>
	<cfset offlineMessage = "#connectorType# Server Offline">
	<!--- <cfset timeDifferenceInHoursToCheckBeforeSendingEmail = 1> --->

	<cfset fromEmailAddress = application.com.settings.getSetting("emailAddress.adminEmail")>
	<cfif fromEmailAddress eq "">
		<cfset fromEmailAddress = "relayHelp@relayware.com">
	</cfif>

	<cfset toAddress = application.testSite eq 0?"infrastructure@relayware.com":application.com.settings.getSetting("connector.adminEmail")>

	<!--- <cfif not reset and dateDiff("h",application.getObject("connector.com.connector").getLastSuccessFulSynch(connectorType=connectorType).lastSuccessfulSynch,request.requestTime) gt timeDifferenceInHoursToCheckBeforeSendingEmail>

		<!--- get last scheduled task log --->
		<cfquery name="getLastScheduledTaskLog">
			select top 1 result from scheduledTaskLog l
				inner join scheduledTaskDefinition d on d.scheduledTaskDefID = l.scheduledTaskDefID
			where d.name = <cf_queryparam value="#scheduledTaskName#" cfsqltype="cf_sql_varchar">
			order by scheduledTaskLogID desc
		</cfquery>

		<cfif getLastScheduledTaskLog.recordCount>
			<cfset scheduledTaskResult = {}>
			<cfif getLastScheduledTaskLog.result neq "">
				<cfset scheduledTaskResult = deserializeJSON(getLastScheduledTaskLog.result)>
			</cfif>

			<!--- if the last scheduled task log was 'Remote Server Down', ignore emailing--->
			<cfif not structKeyExists(scheduledTaskResult,"message") or (structKeyExists(scheduledTaskResult,"message") and scheduledTaskResult.message neq offlineMessage)>

				<cfmail to="#toAddress#" from="#fromEmailAddress#" subject="The last #scheduledTaskName# Synch running on #request.currentSite.domainAndRoot# has been more than #timeDifferenceInHoursToCheckBeforeSendingEmail# hours ago." type="html" priority="urgent">
					The scheduled task "#scheduledTaskName#" has last run successfully on #request.currentSite.domainAndRoot# more than #timeDifferenceInHoursToCheckBeforeSendingEmail# hours ago.<br><br>
					This is likely to be an indication that there is a problem with the scheduled task and that it is erroring.
					<br>
					<br>
					Please investigate!<br>
					<br>
					Regards,<br>
					<br>
					Relayware/#connectorType# Synch
				</cfmail>

				<cfthrow message="The last #connectorType# connector synch successfully ran more than #timeDifferenceInHoursToCheckBeforeSendingEmail# hours ago.">

			<!--- if the last logged run was the remote server being down, then try to run the synch again... --->
			<cfelse>
				<cfset remoteServerWasOffline = true>
			</cfif>
		</cfif>
	</cfif> --->


	<cf_scheduledTask name="#scheduledTaskName#" lock="true" onErrorEmailTo = #toAddress# maximumExpectedLockMinutes="60">

		<cftry>
			<cfset connectorObject = application.getObject("connector.com.connectorUtilities").instantiateConnectorObject(connectorType=connectorType)>

			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",caughtError=cfcatch)>
				<cfset scheduleResult.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfif not structKeyExists(scheduleResult,"message") or scheduleResult.message eq "">

			<!--- if the scheduled task hasn't been run for more than 2 hours due to the remote server being down, send out email saying that we're now running again due to server being back up. --->
			<cfif remoteServerWasOffline>
				<cfmail to="#toAddress#" from="#fromEmailAddress#" subject="The '#scheduledTaskName#' task is now running again on #request.currentSite.domainAndRoot#." type="html" priority="urgent">
					The scheduled task "#scheduledTaskName#" has resumed on #request.currentSite.domainAndRoot# as the #connectorType# Server is now back online.
					<br>
					Regards,<br>
					<br>
					RelayWare/#connectorType# Synch
				</cfmail>
			</cfif>

			<cfparam name="entityList" default="">
			<cfparam name="direction" default="">
			<cfparam name="queueidList" default="">
			<!--- process the objects according to where we left off. If the connector were to ever time out, we want to pick up where we left off rather than starting from the beginning again --->
			<cfset args = {sortOrder="case when object.synch = 1 and object.active = 1 then object.lastSuccessfulSynch else rwObject.lastSuccessfulSynch end, isNull(rwObject.sortorder,100)"}>

			<cfif entityList neq "">
				<cfset args.object=entityList>
			</cfif>

			<cfset synchObjects = application.getObject("connector.com.connector").getPrimaryConnectorObjects(argumentCollection=args)>

			<cfset itemsDone = {}>
			<cfloop query="#synchObjects#">
				<!--- this will give us another 5 minutes per loop if we need it --->
				<cfset application.com.request.extendTimeOutIfNecessary(300)>
				<cfset startTickCount = getTickCount()>
				<cfset itemsDone[object_relayware] = {}>

				<cftry>
					<cfif export and (direction eq "export" or not Len(direction))>
						<cfset application.com.globalFunctions.updateProcessLock (lockName = scheduledTaskName, metadata = {object = object_relayware & " Export"})>
						<cfset connectorObject.runExport(object_relayware=object_relayware,debugLevel=debugLevel,queueidList=queueidList)>
						<cfset itemsDone[object_relayware].export = getTickCount() - startTickCount>
					</cfif>

					<cfcatch>
						<cfset application.com.errorHandler.recordRelayError_Warning(type="#ConnectorType# Connector Export",Severity="error",caughtError=cfcatch)>
						<cfif cfcatch.message eq "Could not connect to remote site.">
							<cfrethrow>
						</cfif>
					</cfcatch>
				</cftry>

				<cftry>
					<cfif import and (direction eq "import" or not Len(direction))>
						<cfset application.com.globalFunctions.updateProcessLock (lockName = scheduledTaskName, metadata = {object = object_relayware  & " Import"})>
						<cfset connectorObject.runImport(object_relayware=object_relayware,debugLevel=debugLevel,queueidList=queueidList)>
						<cfset itemsDone[object_relayware].import = getTickCount() - startTickCount>
					</cfif>

					<cfcatch>
						<cfset application.com.errorHandler.recordRelayError_Warning(type="#ConnectorType# Connector Import",Severity="error",caughtError=cfcatch)>
						<cfif cfcatch.message eq "Could not connect to remote site.">
							<cfrethrow>
						</cfif>
					</cfcatch>
				</cftry>

				<cfset application.com.globalFunctions.updateProcessLock (lockName = scheduledTaskName, metadata = {itemsDone = itemsDone})>

			</cfloop>

			<cfset scheduleResult = {isOK = true, server = cgi.server_name, itemsDone = itemsDone}>

		</cfif>

		<cfif debugLevel gt 0>
			<cfloop list="#request.logFiles#" index="filePath">
				<cfset webPath = replace(filePath,application.paths.content,"")>
				<cfset webPath = replace(webPath,"\","/","ALL")>
				<cfoutput><a href="/content/#webPath#" target="_blank">#listLast(filePath,"\")#</a><br></cfoutput>
			</cfloop>
		</cfif>

	</cf_scheduledTask>

	<!--- commented out for now and expect the scheduled task to be set up in cf admin to run every minute as we currently have no way of know which box the scheduled task is running on
		to set it up if it is not running.....  --->
	<cfif (not structKeyExists(url,"reschedule") or url.reschedule) and not application.com.settings.getSetting("connector.scheduledTask.pause") and (not structKeyExists(scheduleResult,"scheduledTaskLocked") or not scheduleResult.scheduledTaskLocked)>
		<cfset rescheduleTime = dateAdd("n",application.com.settings.getSetting("connector.scheduledTask.waitInMinutesBeforeNextRun"),now())>
		<cfschedule action="update" task="#connectorType# Connector" url="#request.currentSite.protocolAndDomain#/scheduled/connectorSynch.cfm?connectorType=#connectorType#" interval="once" operation="HTTPRequest" startDate="#dateFormat(rescheduleTime,'mm/dd/yy')#" startTime="#rescheduleTime#">
	</cfif>

