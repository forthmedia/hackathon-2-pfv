<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

this is a stub which loads the code in communication/send 
it can be used by a scheduled process without logging in 

NJH	14-Oct-2008	 CR-TND562 Added the scheduleTask custom tag to log comm sending processing on a basic level. (ie. whether it ran or not).
 --->
 
<cf_scheduledTask>

<!--- <cfset application.com.communications.setCommsParameters()> --->
<cfmodule template = "/communicate/send/tskcommsend.cfm"
	userid = 0
	>
	<cfset scheduleResult.result = tskCommSend.resultStruct>

</cf_scheduledTask>
 
<!---  <cfinclude template="/communicate/send/tskcommsend.cfm"> --->