<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			updateRobots-txt.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:		This file re-creates robots.txt which is in the root of 
					the relay directory so that bots do not index the files in 
					any of the directories in the relay directory
					
					see http://www.robotstxt.org/wc/exclusion-admin.html

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->







<cfset encString = "User-agent: *#chr(10)#">

<!--- first we need to get a list of all of the directories undder d:\web\relay --->
<cfdirectory action="LIST" directory="d:\web\relay" name="relayDirectory">

<cfoutput query="relayDirectory">
	<cfif type eq "dir">
		<cfset encString = encString & "Disallow: /#name#/#chr(10)#">
	</cfif>
</cfoutput>

<!--- next get a list of the usefiles directories --->
<cfdirectory action="LIST" directory="d:\web\" name="userfilesDirectories">

<cfoutput query="userfilesDirectories">
	<cfif findNoCase("userfiles",name) gt 0>
		<!--- <cfset encString = encString & "Disallow: /#name#/commfiles/#chr(10)#">
		<cfset encString = encString & "Disallow: /#name#/logfiles/#chr(10)#"> --->
		<cfset encString = encString & "Disallow: /#name#/#chr(10)#">
	</cfif>
</cfoutput>

<cffile action="write" addnewline="yes" file="d:\\web\\relay\\robots.txt" output="#encString#" fixnewline="yes">

<cfdump var="#encString#">


