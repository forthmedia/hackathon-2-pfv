<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			refreshSonyProductTreeImages.cfm	
Author:				GCC
Date started:		2005-06-08
	
Description:		Deletes all locally stored sony product tree images to enable refresh to take place next time the product is viewed. Recognised by containing the strings "tn.png" or "-large.png" to reduce chance of deleting other images accidentally

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

09-06-2005			GCC			Added a method to delete BV product information to enable the refresh of products, not just categories, when BV imports are run.
29-06-2005			GCC			Added a method to bring a product image local to be used by the category rather than collecting in realtime after a bulk deletion.
26-07-2005			GCC			CR_SNY535 extended weekly image collection to get ALL Live USER tree images, not just category images.					
14-09-2006			GCC			P-LEX003 converted to a 'generic' Relay page parameterised by PCMini.cfm
Possible enhancements:


 --->
<cfsetting requesttimeout="500000" showdebugoutput="No">

<cf_include template="\code\cftemplates\pcmINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\pcmINI.cfm")>
	<!--- pcmINI is used to over-ride default global application variables and params --->
	<cfinclude template="/code/cftemplates/pcmINI.cfm">
</cfif> --->
<cfset productImagesDirectory = request.productImagesDirectory>

<cfif isdefined('url.ImageRefresh') and url.ImageRefresh eq "yes">
	<cfdirectory 
	    action = "list"
	    directory = "#productImagesDirectory#"
		name="fileSet">	
		
	<cfoutput query="fileSet">
		<cfif find("tn.png",fileset.name) neq 0 or find("-large.png",fileset.name)>
			<cffile action="DELETE" file="#productImagesDirectory#\#fileset.name#">
	#htmleditformat(fileset.name)# DELETED<br>
		<cfelse>
	#htmleditformat(fileset.name)# KEPT<BR>
		</cfif>
	</cfoutput>	 
	
	<!--- CR_SNY535 GCC Load ALL live product Images now we need them for the category image picker  --->
	<cfquery name="getAllLiveProducts" datasource="#application.siteDataSource#">
		SELECT DISTINCT PCMproduct.productID, PCMproduct.thumbNailUrl
		FROM         PCMproduct INNER JOIN
                      PCMproductCategoryProduct ON PCMproduct.productID = PCMproductCategoryProduct.productID INNER JOIN
                      PCMproductTree ON PCMproductCategoryProduct.treeID = PCMproductTree.TreeID INNER JOIN
					  PCMproductTreeSource on PCMproductTree.SourceID = PCMproductTreeSource.SourceID
		WHERE     (PCMproductCategoryProduct.suppressed = 0) 
		AND (LEN(PCMproduct.thumbNailUrl) > 3) 
		AND (PCMproductTreeSource.sourceGroupID = 2)
	</cfquery>
	
	<cfset imagesDone = 0>
	<cfloop query="getAllLiveProducts">
		<cftry>
		<cfif not fileexists("#productImagesDirectory#\#getAllLiveProducts.productID#tn.png")>
			<cfset fullThumbnailPath = request.remoteImagePath & "/" & getAllLiveProducts.thumbNailUrl>
			<cfhttp url="#fullThumbnailPath#" method="get" timeout="5"></cfhttp>
			<cfif cfhttp.statusCode eq "200 OK">						
				<cfset nImage = application.com.relayImage.getImageComponent()>
				<cfscript>
					nImage.readimageFromURL(#fullThumbnailPath#);
					if (nImage.getWidth() gt nImage.GetHeight()){nImage.scaleWidth(request.productThumbsize);}
					else {nImage.scaleHeight(request.productThumbsize);}							
					nImage.writeImage("#productImagesDirectory#\#getAllLiveProducts.productID#tn.png","png");
				</cfscript>
			<cfelse>
				No Image
			</cfif>
			<cfset imagesDone = imagesDone + 1>
			<cfoutput>#imagesDone# done out of #getAllLiveProducts.recordcount# #getAllLiveProducts.productID#tn.png<br></cfoutput>
			
		<cfelse>
			<cfset imagesDone = imagesDone + 1>
			<cfoutput>#imagesDone# done out of #getAllLiveProducts.recordcount# EXISTED<br></cfoutput>
			
		</cfif>
		<cfcatch type="Any">
		No Image
		</cfcatch>
		</cftry>
	</cfloop>	
</cfif>

<!--- 2006/09/14 GCC - bin BV for now as Sony specific --->
<!--- <cfif isdefined('url.BVProductRefresh') and url.BVProductRefresh eq "yes">
	<cfdirectory 
	    action = "list"
	    directory = "d:\web\sonyuserfiles\content\sony1\productData\BV\productData"
		name="fileSet">
	
	<cfoutput query="fileSet">
			<cffile action="DELETE" file="d:\web\sonyuserfiles\content\sony1\productData\BV\productData\#fileset.name#">
	#fileset.name# DELETED<br>
	</cfoutput>
</cfif> --->
