<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		scheduledPopulatePerCertProgressRptTable.cfm	
Author:			NJH  
Date started:	24-09-2008
	
Description:	Populates the Person Certification report table. This should be run by a schedule that runs once/twice a day as some reports
				are running off this table such as the certificationSummary report.		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfsetting requesttimeout="900">

e.dtd">


<cf_head>
	<cf_title>Populate PerCertProgressRpt Table</cf_title>
</cf_head>


	
	<cfquery name="populatePerCertProgressRptTable" datasource="#application.siteDataSource#">
		-- truncate the flip flop table
		truncate table trngPerCertProgressRptFlipFlop
		
		insert into trngPerCertProgressRptFlipFlop
		(
			personID,
			certificationID,
			certificationCode,
			certificationActive,
			certificationTypeID,
			personCertificationRegistrationDate,
			personCertificationPassDate,
			personCertificationExpiredDate,
			personCertificationCancellationDate,
			sysAccessDate,
			certificationDescription,
			certificationDuration,
			expiryDate,
			certificationCountryID,
			certificationRuleID,
			numModulesToPass,
			ruleActive,
			moduleSetID,
			moduleSetActive,
			moduleSetDescription,
			moduleID,
			AICCTitle,
			ModuleCode,
			AICCDescription,
			Credits,
			personCertificationStatusTextID,
			personCertificationStatus,
			personCertificationID,
			fileID,
			certificationRuleType,
			studyPeriod,
			activationDate,
			AICCType,
			userModuleFulfilled,
			userModuleStatus,
			userModuleProgressID
		)
		select 
			personID,
			certificationID,
			certificationCode,
			certificationActive,
			certificationTypeID,
			personCertificationRegistrationDate,
			personCertificationPassDate,
			personCertificationExpiredDate,
			personCertificationCancellationDate,
			sysAccessDate,
			certificationDescription,
			certificationDuration,
			expiryDate,
			certificationCountryID,
			certificationRuleID,
			numModulesToPass,
			ruleActive,
			moduleSetID,
			moduleSetActive,
			moduleSetDescription,
			moduleID,
			AICCTitle,
			ModuleCode,
			AICCDescription,
			Credits,
			personCertificationStatusTextID,
			personCertificationStatus,
			personCertificationID,
			fileID,
			certificationRuleType,
			studyPeriod,
			activationDate,
			AICCType,
			userModuleFulfilled,
			userModuleStatus,
			userModuleProgressID
		from vTrngPersonCertifications
	</cfquery>
	
	<cfscript>
		application.com.dbTools.flipflop(sourceTable='trngPerCertProgressRptFlipFlop',liveTable='trngPerCertProgressRpt');
	</cfscript>
	


