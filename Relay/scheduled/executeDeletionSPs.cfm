<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 


run deletion stored procedures as scheduled processes

WAB December 2004

Designed to run the deletion stored procedures once per day so that any feedback can be emailed to support

2008/10/14	NJH		CR-TND562 Added the scheduleTask custom tag to log deletion processing on a basic level. (ie. whether it ran or not)

 --->
<cf_scheduledTask name="Delete Flagged People">
 
<cfset scheduledProcess = true> 
 <cfinclude template = "..\utilities\deleteFlaggedPeople.cfm">
<cfif isDefined("mailBody")>
	<cfset scheduleResult.result = mailBody>
</cfif>

</cf_scheduledTask>
