<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scheduledSODLoad.cfm	
Author:				GCC
Date started:		2006-11-24
	
Description:		Checks Load data from Birch and loads it if it has changed

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed


Possible enhancements:
 --->


<cfinclude template="/code/cftemplates/SODload.cfm">

