<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			OppProductEmail.cfm	
Author:			SSS
Date started:		2009/10/02

Description:		At the end of the day send a email for every opportunity with products that have been created.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed



 --->

<CFQUERY name="getopportunities" datasource="#application.siteDataSource#">
	select opp.opportunityID, p.firstname + '' + p.lastname as pertnerSalesname,
	p2.firstname + '' +p2.lastname as vendorname, p2.email, ot.opptype,
	opp.detail, o.organisationname, opp.created
	from opportunity opp inner join
		person p on opp.partnerSalesPersonID = p.personID inner join
		person p2 on opp.vendoraccountmanagerPersonID = p2.personID inner join
		opptype ot on opp.opptypeID = ot.opptypeID inner join
		location l on l.locationID = opp.partnerlocationID inner join
		organisation o on o.organisationID = l.organisationID
	where CONVERT(DATETIME, CONVERT(INT, opp.created)) = CONVERT(DATETIME, CONVERT(INT, GETDATE())) - 8
</CFQUERY>

<cfloop query="getopportunities">
	<CFQUERY name="getopportunitiesproducts" datasource="#application.siteDataSource#">
		select 	p.description, p.sku, oppp.quantity, oppp.forecastshipdate, p.listprice, oppp.specialprice,
				oppp.approvedPrice, oppp.subtotal, demo = case demo when 1 then 'yes' else 'no' end
		from oppproducts oppp inner join
			product p on oppp.productorGroupID = p.productID and oppp.productOrGroup = 'P'
		where opportunityID =  <cf_queryparam value="#getopportunities.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and productorgroup = 'p'
	</CFQUERY>
	
	<cfset subjectmail = "New #opptype# for #organisationname#">

	<cfmail to = "sachs@foundation-network.com" bcc ="alistair.barclay@foundation-network.com" from = "donotreply@lexmark.com" subject = "#subjectmail#" type = "html">
       Dear #vendorname#, <br><br>
		The following #opptype# #opportunityID# has been submitted today #dateformat(created, "yyyy-mmm-dd")# <br><br>
		#organisationname#<br>
		#pertnerSalesname#<br><br>
		#detail#<br><br>
		<table>
			<tr>
				<th>Product Name</th>
				<th>sku</th>
				<th>Quantity</th>
				<th>forcast shipped date</th>
				<th>List Price</th>
				<th>Special Price</th>
				<th>Approved Price</th>
				<th>Sub Total</th>
				<th>Demo</th>
			</tr>
		<cfloop query="getopportunitiesproducts">
			<tr>
			     <td>#description#</td>
			     <td>#sku#</td>
			     <td>#quantity#</td>
			     <td>#dateformat(forecastshipdate, "yyy-mmm-dd")#</td>
			     <td>#Listprice#</td>
			     <td>#SpecialPrice#</td>
			     <td>#ApprovedPrice#</td>
			     <td>#SubTotal#</td>
			     <td>#demo#</td>
			</tr>
		</cfloop>
		</table>
    </cfmail>    

</cfloop>
