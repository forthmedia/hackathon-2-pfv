<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		scheduledSFDCPricebookImport.cfm	
Author:			NJH  
Date started:	25-02-2010
	
Description:	File to import pricebooks,products and pricebookEntries		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/06/05			NJH			Had to make changes due to product groups now having translations. Add default translations for product groups when adding a new product group.
2014/04/29			NJH			Case 439537 - added cflock around the xmlsearch
2014-10-28			AHL			Case 44216 Pricebooks import error: "''" is not boolean

Possible enhancements:

Import Rules
1. Only one pricebook per country can exist.
2. A pricebook must be named with the countryISO in the beginning surrounded by parenthesis and must end with -RW-. For example, "(UK) English Pricebook -RW-"
3. The country currencies must be set up in the country table


 --->

<cfparam name="singleCurrencyCountry" default="US">
<cfparam name="reset" default="false">

<cfif structKeyExists(url,"reset")>
	<cfset reset = true>
</cfif>

<!--- check the salesforce version that we are using --->
<cfif application.com.settings.getSetting("versions.salesforce") neq 2>

<cf_scheduledTask name="SalesForce Pricebook Import" lock="true" ignoreLock="#reset#">
		
<cfsetting requesttimeout="3600"  enablecfoutputonly="true">

<cfset pricebookSettings = application.com.settings.getSetting("salesForce.pricebooks")>
<cfset pricebookWhereClause = application.com.settings.getSetting("salesForce.whereClause.object.pricebook")>
<cfset defaultProductGroupID = pricebookSettings.defaultProductGroup>
<cfset request.synchLevel = "Record">

<cfif pricebookWhereClause neq "">
	<cfset pricebookWhereClause = "and "&pricebookWhereClause>
</cfif>

<!--- setting up some initial default values --->
<!--- the pricebook organisation --->
<cfset vendorOrganisationID = application.com.settings.getSetting("theClient.clientOrganisationID")>
<cflock name="SalesforceXMLSearch" timeout="3">
<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='pricebook2'][translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='pricebook'][translate(@column,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='organisationid']")>
</cflock>
<cfif arrayLen(mappingArray)>
	<cfset vendorOrganisationID = mappingArray[1].xmlAttributes.value>
</cfif>

<!--- which country to log the products against --->
<cfset productCountryID = 0>
<cflock name="SalesforceXMLSearch" timeout="3">
<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='product2'][translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='product'][translate(@column,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='countryid']")>
</cflock>
<cfif arrayLen(mappingArray)>
	<cfset productCountryID = mappingArray[1].xmlAttributes.value>
</cfif>

<!--- the product catalogue --->
<cfset pricebookCampaignID = 4>
<cflock name="SalesforceXMLSearch" timeout="3">
<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='product2'][translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='product'][translate(@column,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='campaignid']")>
</cflock>
<cfif arrayLen(mappingArray)>
	<cfset pricebookCampaignID = mappingArray[1].xmlAttributes.value>
</cfif>

<!--- the name of the column that the product groupId maps to --->
<cfset productGroupMapping = "">
<cflock name="SalesforceXMLSearch" timeout="3">
<cfset mappingArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='product2'][translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='product'][translate(@column,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='productgroupid']")>
</cflock>
<cfif arrayLen(mappingArray)>
	<cfset productGroupMapping = mappingArray[1].xmlAttributes.objectColumn>
</cfif>

<cfset application.com.salesForce.setSalesForceUserAsCurrentUser()>

<cfif fileExists("#application.paths.code#\cftemplates\scheduled\scheduledSFDCPricebookImport.cfm")>
	<cfinclude template="/code/cftemplates/scheduled\scheduledSFDCPricebookImport.cfm">
<cfelse>
	
	<cfset pricebook2ColumnList = application.com.salesForce.getMappedObject(object="pricebook2",direction="SF2RW").objectColumnList>
	<cfset pricebookEntryColumnList = application.com.salesForce.getMappedObject(object="pricebookEntry",direction="SF2RW").objectColumnList>
	<cfset product2ColumnList = application.com.salesForce.getMappedObject(object="product2",direction="SF2RW").objectColumnList>
	<cfset product2ColumnListForQuery = "">
	<cfset pricebook2ColumnListForQuery = "">
	<cfset singleCurrency = false>
	
	<cfif not listFindNoCase(pricebookEntryColumnList,"CurrencyIsoCode")>
		<cfset singleCurrency = true>
	</cfif>
	
	<cfloop list="#product2ColumnList#" index="productField">
		<cfset product2ColumnListForQuery = listAppend(product2ColumnListForQuery,"product2."&productField)>
	</cfloop>
	
	<cfloop list="#pricebook2ColumnList#" index="pricebookField">
		<cfset pricebook2ColumnListForQuery = listAppend(pricebook2ColumnListForQuery,"pricebook2.#pricebookField#")>
	</cfloop>
	
	<!--- get all active pricebooks --->
	<cfset pricebookResponse = application.com.salesforce.Send(object="pricebook2",method="query",ignoreBulkProtection=true,queryString="select #pricebook2ColumnList# from pricebook2 where isActive = TRUE #pricebookWhereClause#")>

	<cfset pricebookQry = pricebookResponse.response>
	
	<cfset countryList = "">
	<cfset countryCurrencyStruct = structNew()>
	<cfset multipleCountryCurrency = structNew()>
	
	<cfif listFindNoCase(pricebookQry.columnList,"ID")>
		<cfloop query="pricebookQry">
			<!--- get all the countries represented --->
			<cfset countryISOList = "">
			<cfset groups={1="CountryISO"}>
			<cfset countryArray = application.com.regExp.refindAllOccurrences("\((.+?)\)",name,groups)> <!--- countryISO will be in parenthesis.. --->
			<cfif arrayLen(countryArray) gt 0>
				<cfset countryISOList = countryArray[1].countryISO>
			</cfif>
			
			<cfloop list="#countryISOList#" index="country">
				<cfif not listFindNoCase(countryList,country)>
					<cfset countryList = listAppend(countryList,country)>
				</cfif>
			</cfloop>
		</cfloop>
		
		<!--- create temp tables to hold ID's the we're processing --->
		<cfquery name="createTempTables" datasource="#application.siteDataSource#">
			if object_id('tempdb..##sfPricebookImportedObjectID') is not null
			begin
			   drop table ##sfPricebookImportedObjectID
			end
			create table ##sfPricebookImportedObjectID (crmID varchar(50), sfObject varchar(50))
		</cfquery>
		
		
		<cfset countryStruct = structNew()> <!--- a country structure to hold valid currencies and the products/pricebook Entries for that country --->
		<cfset invalidCountryStruct = structNew()> <!--- a country struct that holds the currencies that have appeared more than once for that country --->
		
		<cfloop list="#countryList#" index="countryISO">
			<cfquery name="getPricebooksForCountry" dbType="query">
				select ID, name from pricebookQry
				<cfif not singleCurrency>
				where lower(name) like '%(%#lcase(countryISO)#%)%'
				<cfelse>
				where lower(name) like '%(%#lcase(singleCurrencyCountry)#%)%'
				</cfif>
			</cfquery>
			
			<cfset countryStruct[countryISO] = structNew()>
			<cfset countryStruct[countryISO].currency = "">
			<cfset countryStruct[countryISO].productsPricebookQuery = "">
			<cfset invalidCountryStruct[countryISO] = "">
			
			<cfloop query="getPricebooksForCountry">
				<cfset pricebookEntryResponse = application.com.salesforce.Send(object="pricebookEntry",method="query",ignoreBulkProtection=true,queryString="SELECT #pricebook2ColumnListForQuery#,#pricebookEntryColumnList#,#product2ColumnListForQuery# from pricebookentry where product2.isActive=TRUE and isActive=TRUE and pricebook2.isActive = TRUE and pricebook2.name = '#name#'")>

				<cfif pricebookEntryResponse.success>
					<cfset pricebookEntryQry = pricebookEntryResponse.response>
			
					<cfif not singleCurrency>
					<cfquery name="getPricebookCurrency" dbType="query">
						select distinct CurrencyIsoCode from pricebookEntryQry where CurrencyIsoCode is not null
					</cfquery>
					<cfelse>
						<cfset getPricebookCurrency = queryNew("CurrencyIsoCode","varchar")>
						<cfset queryAddRow(getPricebookCurrency,1)>
						<cfset querySetCell(getPricebookCurrency,"CurrencyIsoCode","USD")>
					</cfif>
					
					<!--- build a structure that holds the currency for the given country and the pricebook entries --->
					<cfloop query="getPricebookCurrency">
						
						<cfquery name="isCurrencyValidForCountry" datasource="#application.siteDatasource#">
							select countryCurrency from country where isoCode =  <cf_queryparam value="#countryISO#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfquery>
						
						<cfif not listFindNoCase(countryStruct[countryISO].currency,currencyISOCode) and listFindNoCase(isCurrencyValidForCountry.countryCurrency,currencyISOCode)>
							<cfset countryStruct[countryISO].currency = listAppend(countryStruct[countryISO].currency,currencyISOCode)>
							
							<cfif isQuery(countryStruct[countryISO].productsPricebookQuery)>
								<cfset productsPricebookQuery = countryStruct[countryISO].productsPricebookQuery>
								<cfquery name="allProductPricebooksForCountryCurrency" dbType="query">
									select #pricebookEntryQry.columnList# from pricebookEntryQry
									union
									select #pricebookEntryQry.columnList# from productsPricebookQuery
								</cfquery>
								
								<cfset countryStruct[countryISO].productsPricebookQuery = allProductPricebooksForCountryCurrency>
							<cfelse>
								<cfset countryStruct[countryISO].productsPricebookQuery = pricebookEntryQry>
							</cfif>
							
						<cfelse>
							<cfif not listFindNoCase(invalidCountryStruct[countryISO],currencyISOCode)>
								<cfset invalidCountryStruct[countryISO] = listAppend(invalidCountryStruct[countryISO],currencyISOCode)>
								<cfoutput>Currency #currencyISOCode# is not valid for country #countryISO#<br></cfoutput>
								
								<cfif listFindNoCase(countryStruct[countryISO].currency,currencyISOCode)>
									<cfif not structKeyExists(multipleCountryCurrency,countryISO)>
										<cfset multipleCountryCurrency[countryISO] = "">
									</cfif>
									<cfset multipleCountryCurrency[countryISO] = listAppend(multipleCountryCurrency[countryISO],currencyISOCode)>
									<cfset mergeStruct = structNew()>
									<cfset mergeStruct.countryISO = countryISO>
									<cfset mergeStruct.currencyISO = currencyISOCode>
									<cfset application.com.email.sendEmail(emailTextID="PricebookMultipleCurrencyCountryCombo",personID=2,mergeStruct=mergeStruct,recipientAlternativeEmailAddress=application.com.settings.getSetting("salesForce.adminEmail"))>
								</cfif>
							</cfif>
						</cfif>
					</cfloop>
				</cfif>
			</cfloop>
		</cfloop>
		
		<!--- building the column lists --->
		<cfset product2ColumnList = "">
		<cfset pricebook2ColumnList = "">
		<cfset pricebookEntryColumnList = "">
	
		<cfloop list="#pricebookEntryQry.columnlist#" index="columnName">
			<cfif findNoCase("product2_",columnName)>
				<cfset product2ColumnList = listAppend(product2ColumnList,columnName)>
			<cfelseif findNoCase("pricebook2_",columnName)>
				<cfset pricebook2ColumnList = listAppend(pricebook2ColumnList,columnName)>
			<cfelse>
				<cfset pricebookEntryColumnList = listAppend(pricebookEntryColumnList,columnName)>
			</cfif>
		</cfloop>
		
		<cfset product2ColumnListForQuery = "">
		<cfloop list="#product2ColumnList#" index="columnName">
			<cfset column = "#columnName# as #replaceNoCase(columnName,'product2_','')#">
			<cfset product2ColumnListForQuery = listAppend(product2ColumnListForQuery,column)>
		</cfloop>

		<cfloop collection="#countryStruct#" item="countryISO">
			<cfloop list="#countryStruct[countryISO].currency#" index="currencyISO">
			
				<!--- if the country/currency is valid - process the pricebook products and pricebookEntries --->
				<cfif structKeyExists(invalidCountryStruct,countryISO) and not listFindNoCase(invalidCountryStruct[countryISO],currencyISO)>
				
					<cfset thisCountryISO = countryISO>
				
					<cfif structKeyExists(application.countryIDLookupFromISOcodeStr,thisCountryISO)>
					
						<cfset productsPricebookQuery = countryStruct[countryISO].productsPricebookQuery>
						
						<cfquery name="getPricebookForCountryCurrency" dbType="query">
							select distinct #pricebook2ColumnList# from productsPricebookQuery
							where lower(pricebook2_name) like '%(%#lcase(countryISO)#%)%'
								<cfif not singleCurrency>and currencyISOCode = '#currencyISO#'</cfif>
						</cfquery>
						
<!--- 						<cfset runtime = CreateObject("java","java.lang.Runtime").getRuntime()>
						<cfset freeMemory = runtime.freeMemory() / 1024 / 1024>
						<cfset totalMemory = runtime.totalMemory() / 1024 / 1024>
						<cfset maxMemory = runtime.maxMemory() / 1024 / 1024>
						
						<cfoutput>
						    Free Allocated Memory: #Round(freeMemory)#mb<br>
						    Total Memory Allocated: #Round(totalMemory)#mb<br>
						    Max Memory Available to JVM: #Round(maxMemory)#mb<br>
						</cfoutput>
						 --->
				
						<cfloop query="getPricebookForCountryCurrency">
		
							<cfset sfPricebookID = pricebook2_ID&"_"&currencyISO&"_"&thisCountryISO>
							<cfset thisPricebookName = "#pricebook2_name# (#currencyISO#)">
					
							<cfset pricebookStruct = {ID=sfPricebookID}>
							<cfset additionalArgs = {startDate=request.requestTime,organisationID=vendorOrganisationID,countryID=application.countryIDLookupFromISOcodeStr[thisCountryISO],campaignID=pricebookCampaignID,currency=currencyISO,name=thisPricebookName,deleted=0}>
							<cfset rwPricebookID = application.com.salesForce.doesEntityExist(object="pricebook2",objectID=sfPricebookID).entityID>

							<!--- don't update the start date if the pricebook exists --->
							<cfif rwPricebookID neq 0>
								<cfset structDelete(additionalArgs,"startDate")>
							</cfif>
							<cfset pricebookResult = application.com.salesForce.import(objectID=sfPricebookID,object="pricebook2",objectDetails=pricebookStruct,additionalArgs=additionalArgs)>
						
							<cfset rwPricebookID = pricebookResult.entityID>
							
							<cfquery name="insertProcessedPricebook" datasource="#application.siteDataSource#">
								if not exists (select 1 from ##sfPricebookImportedObjectID where crmID =  <cf_queryparam value="#sfPricebookID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and sfObject='Pricebook')
								insert into ##sfPricebookImportedObjectID (crmID,sfObject) values (<cf_queryparam value="#sfPricebookID#" CFSQLTYPE="CF_SQL_VARCHAR" >,'Pricebook')
							</cfquery>
							<cfoutput><br>Processing Pricebook '#sfPricebookID#'<br></cfoutput>
		
							<!--- only run the product import once for the country pricebook --->
							<cfif currentRow eq 1>
								<!--- get all products for this pricebook --->
								<cfquery name="getProductQry" dbType="query">
									select #product2ColumnListForQuery# from productsPricebookQuery 
									where pricebook2_ID = '#pricebook2_ID#'
									<cfif not singleCurrency>and currencyIsoCode = '#currencyISO#'</cfif>
								</cfquery>
								
								<cfset productObjectStruct = application.com.structureFunctions.queryToStruct(query=getProductQry,key="ID")>
								<cfset productCount = 0>
								<cfset phraseTextIDList = "">
								
								<cfloop collection="#productObjectStruct#" item="product2ID">

									<cfset productGroupID = defaultProductGroupID>
									<cfif productGroupMapping neq "" and productObjectStruct[product2ID][productGroupMapping] neq "">
										<cfset productGroupValue = productObjectStruct[product2ID][productGroupMapping]>
										
										<cfquery name="doesProductGroupExist" datasource="#application.siteDataSource#">
											declare @productGroupExisted int
											set @productGroupExisted = 1
											
											if not exists (select 1 from productGroup where description_defaultTranslation =  <cf_queryparam value="#productGroupValue#" CFSQLTYPE="CF_SQL_VARCHAR" > )
											begin
												insert into productGroup (description_defaultTranslation,title_defaultTranslation,createdBy,lastUpdatedBy,lastUpdatedByPerson) values (<cf_queryparam value="#productGroupValue#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#productGroupValue#" CFSQLTYPE="CF_SQL_VARCHAR" >,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personID#)
												set @productGroupExisted = 0
											end
											
											select productGroupID, @productGroupExisted as productGroupExisted from productGroup where description_defaultTranslation =  <cf_queryparam value="#productGroupValue#" CFSQLTYPE="CF_SQL_VARCHAR" > <!--- 2014-10-28 AHL Case 44216 Pricebooks import error: "''" is not boolean --->
										</cfquery>
										
										<cfset productGroupId = doesProductGroupExist.productGroupID>
										
										<cfif not doesProductGroupExist.productGroupExisted>
											<cfset addResult = application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID="title",entityTypeID=application.entityTypeID.productGroup,entityid=productGroupId,phraseText=trim(productGroupValue),updateCluster=false)>
							    			<cfset phraseTextIDList = listAppend(phraseTextIDList,addResult.phraseTextID)>
							    			<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID="description",entityTypeID=application.entityTypeID.productGroup,entityid=productGroupId,phraseText=trim(productGroupValue),updateCluster=false)>
							    			<cfset phraseTextIDList = listAppend(phraseTextIDList,addResult.phraseTextID)>
										</cfif>
										
									</cfif>
									
									<!--- because we already have all the product info, we just pass through what we have --->
									<cfset additionalArgs = {countryID=productCountryID,campaignID=pricebookCampaignID,productGroupID=productGroupId}>
									<cfset application.com.salesForce.import(objectID=product2ID,object="product2",objectDetails=productObjectStruct[product2ID],additionalArgs=additionalArgs)>
									
									<cfquery name="insertProcessedProduct" datasource="#application.siteDataSource#">
										if not exists (select 1 from ##sfPricebookImportedObjectID where crmID='#product2ID#' and sfObject='Product')
										insert into ##sfPricebookImportedObjectID (crmID,sfObject) values ('#product2ID#','Product')
									</cfquery>
									
									<cfset productCount = productCount+1>
								</cfloop>
								
								<cfif phraseTextIDList neq "">
									<cfset application.com.relayTranslations.resetPhraseStructureKey(phraseTextID=phraseTextIDList,updateCluster=true)>
								</cfif>
								
								<cfquery name="getProductCount" datasource="#application.siteDataSource#">
									select count(1) as productCount from ##sfPricebookImportedObjectID where sfObject='Product'
								</cfquery>
								
								<cfoutput>Processed #productCount# product records for '#thisPricebookName#' pricebook.<br></cfoutput>
								
								<!--- <cfset runtime = CreateObject("java","java.lang.Runtime").getRuntime()>
								<cfset freeMemory = runtime.freeMemory() / 1024 / 1024>
								<cfset totalMemory = runtime.totalMemory() / 1024 / 1024>
								<cfset maxMemory = runtime.maxMemory() / 1024 / 1024>
								
								<cfoutput>
								    Free Allocated Memory: #Round(freeMemory)#mb<br>
								    Total Memory Allocated: #Round(totalMemory)#mb<br>
								    Max Memory Available to JVM: #Round(maxMemory)#mb<br>
								</cfoutput>
								 --->
							</cfif>
						
							<cfquery name="getPricebookEntryQry" dbType="query">
								select #pricebookEntryColumnList# from productsPricebookQuery where pricebook2_ID = '#pricebook2_ID#' <cfif not singleCurrency>and currencyIsoCode = '#currencyISO#'</cfif>
							</cfquery>
						
							<cfset pricebookEntryObjectStruct = application.com.structureFunctions.queryToStruct(query=getPricebookEntryQry,key="ID")>
							<cfset pricebookEntryCount = 0>
							
							<cfloop collection="#pricebookEntryObjectStruct#" item="pricebookEntryID">
								<cfset productID=application.com.salesForce.doesEntityExist(object="product2",objectID=pricebookEntryObjectStruct[pricebookEntryID].product2ID).entityID>
								
								<cfif productID neq 0>
									<cfset additionalArgs = {productID=productID,pricebookID=rwPricebookID}>
									<cfset pricebookEntryObjectStruct[pricebookEntryID].pricebook2ID = sfPricebookID>
									<cfset application.com.salesForce.import(objectID=pricebookEntryID,object="pricebookEntry",objectDetails=pricebookEntryObjectStruct[pricebookEntryID],additionalArgs=additionalArgs)>
									
									<cfquery name="insertProcessedPricebookEntry" datasource="#application.siteDataSource#">
										if not exists (select 1 from ##sfPricebookImportedObjectID where crmID='#pricebookEntryID#' and sfObject='PricebookEntry')
										insert into ##sfPricebookImportedObjectID (crmID,sfObject) values ('#pricebookEntryID#','PricebookEntry')
									</cfquery>
									
									<cfset pricebookEntryCount = pricebookEntryCount+1>
								</cfif>
							</cfloop>
							
							<cfoutput>Processed #pricebookEntryCount# pricebookEntry records for '#thisPricebookName#' pricebook.<br></cfoutput>
							
							<!--- <cfset runtime = CreateObject("java","java.lang.Runtime").getRuntime()>
							<cfset freeMemory = runtime.freeMemory() / 1024 / 1024>
							<cfset totalMemory = runtime.totalMemory() / 1024 / 1024>
							<cfset maxMemory = runtime.maxMemory() / 1024 / 1024>
							
							<cfoutput>
							    Free Allocated Memory: #Round(freeMemory)#mb<br>
							    Total Memory Allocated: #Round(totalMemory)#mb<br>
							    Max Memory Available to JVM: #Round(maxMemory)#mb<br>
							</cfoutput>
							 --->
							
						</cfloop>
					</cfif>
					
				</cfif>
			</cfloop>
		</cfloop>
		
		<!--- delete pricebooks that have not been imported and that aren't in the list of pricebooks that couldn't be imported
			due to multiple country/currency issues. This will stop the current one getting deleted if panda fail to remove 
			the old pricebook while instating a new one --->
		<cfquery name="deletePricebooks" datasource="#application.siteDataSource#">
			update pricebook set deleted=1,lastUpdated =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,lastUpdatedBy=#request.relayCurrentUser.userGroupID#
			where crmPricebookID is not null
					and deleted <> 1
				and (crmPricebookID not in (select crmID from ##sfPricebookImportedObjectID where sfObject='Pricebook')
				<cfif structCount(multipleCountryCurrency) gt 0>
					and 
					<cfset countryCount=1>
					<cfloop collection="#multipleCountryCurrency#" item="countryISO">
						<cfset currencyCount=1>
						<cfloop list="#multipleCountryCurrency[countryISO]#" index="currencyISO">
							not (countryID =  <cf_queryparam value="#application.countryIDLookupFromISOcodeStr[countryISO]#" CFSQLTYPE="CF_SQL_INTEGER" >  and currency =  <cf_queryparam value="#currencyISO#" CFSQLTYPE="CF_SQL_VARCHAR" > )
							<cfif currencyCount lt listLen(multipleCountryCurrency[countryISO])>
								and
								<cfset currencyCount = currencyCount+1>
							</cfif>
						</cfloop>
						<cfif countryCount lt structCount(multipleCountryCurrency)>
							and
							<cfset countryCount=countryCount+1>
						</cfif>
					</cfloop>
				</cfif>
				)
		</cfquery>

			
		<cfquery name="deletePricebookEntries" datasource="#application.siteDataSource#">
			update pricebookEntry set deleted=1,lastUpdated =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,lastUpdatedBy=#request.relayCurrentUser.userGroupID#
			where crmPricebookEntryID is not null
				and deleted <> 1
				and crmPricebookEntryID not in (select crmID from ##sfPricebookImportedObjectID where sfObject='PricebookEntry')
		</cfquery>
			
		<cfquery name="deleteProducts" datasource="#application.siteDataSource#">
			update product set deleted=1,lastUpdated =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,lastUpdatedBy=#request.relayCurrentUser.userGroupID#
			where crmProductID is not null
				and crmProductID not in (select crmID from ##sfPricebookImportedObjectID where sfObject='Product')
				and deleted <> 1
		</cfquery>
		
		<!--- <cfloop list="pricebook,product" index="entityType">
			<cfset flagID = application.com.flag.getFlagStructure(entityType&"SalesForceSynched").flagID>
			
			<cfquery name="flagEntityTypeAsSynched" datasource="#application.siteDataSource#">
				update booleanFlagData set lastUpdatedBy=#request.relayCurrentUser.userGroupId#,lastUpdated='#request.requestTime#'
					where flagID = #flagID#
					
				delete from booleanFlagData where flagID = #flagID#
				
				insert into booleanFlagData (flagID,entityID,createdBy,created,lastUpdatedBy,lastUpdated)
				select #flagID#,p.#entityType#ID,#request.relayCurrentUser.userGroupId#,lastUpdated=GetDate(),#request.relayCurrentUser.userGroupId#,lastUpdated='#request.requestTime#'
					from #entityType# p
				where crm#entityType#ID is not null and p.deleted <> 1
			</cfquery>
		</cfloop> --->
		
	<cfelse>
		<cfset scheduleResult.message = "No Pricebooks to import">
	</cfif>
</cfif>
</cf_scheduledTask><cfelse>
	<cfthrow message="This version of salesforce connector is no longer in user">
</cfif>