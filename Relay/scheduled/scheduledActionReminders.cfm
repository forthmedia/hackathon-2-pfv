<!--- �Relayware. All Rights Reserved 2014 --->






<CFSET today = "#DateFormat(now(),'mm/dd/yyyy')#">

<cfquery name="getTasks" datasource="#application.siteDataSource#">
SELECT a.Description,
	a.Deadline,
	a.ActionID,
	a.timespent AS Estimate,
	p.firstName+' '+p.lastName AS Fullname,
	ast.Status,
	at.actionType,
	at.actionTypeID,
	ast.Status_series,			
	a.priority,
	a.entityTypeID,
	a.entityID,
	a.RequestedBy,
	SUM((rr.Permission/1) % 2) AS allow_read,
	SUM((rr.Permission/2) % 2) AS allow_edit
FROM actions a 
   	INNER JOIN actionStatus ast ON ast.actionStatusID = a.actionStatusID 
	LEFT OUTER JOIN actionType at ON at.actionTypeID = a.actionTypeID 
	LEFT OUTER JOIN Person p ON a.ownerPersonID = p.PersonID 
	INNER JOIN recordrights rr ON a.actionID = rr.recordid
WHERE rr.UserGroupID = 100 
 	AND a.actionStatusID not in (SELECT actionStatusID from actionStatus where assigned = 0)
and a.dEADLINE > {ts '#today# 00:00:00'} 
GROUP BY a.Description, a.RequestedBy, a.Deadline, a.ActionID, p.firstName, 
	p.lastName,ast.Status,at.actionType,at.actionTypeID,a.priority,a.entityTypeID,
		a.entityID, ast.Status_series, a.timespent 
HAVING SUM((rr.Permission/1) % 2) > 0
ORDER BY a.Deadline, ast.Status_series DESC
</cfquery>


<cfloop query="mailing">

   <CFMAIL
            to="whoever@youwant.com"
            from="valid@from-address.com"
            Subject="Important">

   Message here regarding today's important event.

   </CFMAIL>

</cfloop>




