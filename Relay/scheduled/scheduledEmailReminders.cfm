<!--- �Relayware. All Rights Reserved 2014 --->

<cf_title>Scheduled Email Reminders</cf_title>

<cfset scheduledEmailsToRun=application.com.scheduledEmails.getEmailRemindersToRun()>
<cfif scheduledEmailsToRun eq "">
	There are no scheduled emails to run.
<cfelse>
	scheduledEmailsToRun= <cfoutput>#scheduledEmailsToRun#</cfoutput>
</cfif>

<cfloop index="i" list="#scheduledEmailsToRun#" delimiters=",">
	<cfset scheduledEmailsToRun = application.com.scheduledEmails.populateReminders(eMailReminderScheduleID=i)>
	<cfset eMailsSentVar = application.com.scheduledEmails.sendGenericReminderEmail(eMailReminderScheduleID=i)>
</cfloop>