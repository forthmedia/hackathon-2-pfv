<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			remoteLoadRelayPhraseStructure.cfc	
Author:				NJH
Date started:		2006-01-03
	
Purpose:	Webservice cfc To reload cfcs into memeory in.

Usage: place messages between <<<< and >>>>

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
	FTP files from dev server to servers where files don't exist

--->

<cfsetting showdebugoutput="No" enablecfoutputonly="No">

<cfif structkeyExists(url,"phrases")>
	<cfif FileExists("#application.path#\translation\loadPhrases\#url.phrases#")>
		<cfset frmPhraseFile = #url.phrases#>
		<cfset frmAddPhrases = "true">
		<cfset frmUpdatePhrases = "ignore">
		<cftry>
			<cfinclude template="\translation\AddUpdatePhrases.cfm">
		<cfcatch type="any">
			<cfoutput><<<<#htmleditformat(cfcatch.message)# #htmleditformat(cfcatch.detail)#>>>></cfoutput>
		</cfcatch>
		</cftry>
	<cfelse>
		<cfoutput><<<<#htmleditformat(application.path)#\translation\loadPhrases\#htmleditformat(url.phrases)# does not exist.<br>Phrase load unsuccessful.>>>></cfoutput>
	</cfif>
<cfelse>
	<<<<url.phrases must be passed with the filename containing the phrases to load.>>>>
</cfif>