<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	oppAssetRenewal.cfm 
	
	This template will add a Renewal Opportunity when a Portal User clicks on 'Renew' on the 'Sales History' Tab

	Parameters:

	Author: Neal Sparks 2010-04-20

Amendment History:


--->

<cfsetting requesttimeout="86400">	<!--- have set timout to 1 day cos the first time it could take a while --->

<!--- Get the Assets --->
<!--- If the Url.AssetID exsists then the asset is being renewed manually --->
<cfif structKeyExists(url."assetID")>
	<cfset Assets = application.com.asset.getAssets(assetID=URL.assetID)>
<cfelse>
	<!--- Otherwise it is being renewed as part of a scheduled task --->
	<cfset Assets = application.com.asset.getAssets()>
</cfif>

<!---  loop through the assets identified for renewal --->
<cfoutput query="Assets">
	<cfset AssetInfo = application.com.structureFunctions.queryRowToStruct(query=Assets,row=currentrow)>
	
	<cfset RenewAsset = application.com.asset.RenewAsset(asset=AssetInfo)>
</cfoutput>

