<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relay\scheduled\ArchiveProcess.cfm	
Author:				NYB
Date started:		2009-11-28
	
Purpose:	

Usage: 
	Parameter: ArchiveDef - accepts the ArchiveDefTextID of the ArchiveDef you wish to run.  
	If not passed, or doesn't exist, will run for all.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


--->

<cfsetting RequestTimeout = "300">  <!--- 7200 = 3hrs ---> 
<cfparam name="ArchiveDef" default="">
<cfparam name="RunTime" default="30">  <!--- 900=15mins ---> 

<cftry>
	<cfoutput>
	<cfif ArchiveDef neq "">
		<cfquery name="GetArchiveData" datasource="#application.sitedatasource#">
			SELECT * from ArchiveDef where active=1 and ArchiveDefTextID =  <cf_queryparam value="#ArchiveDef#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			order by ExecOrder
		</cfquery>
		<cfif GetArchiveData.recordcount eq 0>
			<p>There is no ArchiveDefTextID of <b>#htmleditformat(ArchiveDef)#</b> in the system.  Please retry</p>
			<CF_ABORT>
		</cfif>
	<cfelse>
		<cfquery name="GetArchiveData" datasource="#application.sitedatasource#">
			SELECT * from ArchiveDef where active=1 order by ExecOrder
		</cfquery>
	</cfif>
	
	<cfset DataStruct = StructNew()>
	<cfset ParametersArray = ArrayNew(1)>
	<cfset ParametersSubArray = ArrayNew(1)>
	<cfset ArgStruct = StructNew()>
	<cfloop query="GetArchiveData">
		<p>
		<cfif Status eq "Ready">
			<cfscript>
				StructInsert(DataStruct,"ArchiveDefID",ArchiveDefID);
				StructInsert(DataStruct,"AgeValue",AgeValue);
				StructInsert(DataStruct,"AgePart",AgePart);
				StructInsert(DataStruct,"PrimaryTable",PrimaryTable);
				StructInsert(DataStruct,"PriKeyCol",PriKeyCol);
				StructInsert(DataStruct,"DateCol",DateCol);
				StructInsert(DataStruct,"MovePerRun",MovePerRun);
				StructInsert(DataStruct,"CFFunction",CFFunction);
				/* replaced: 
				if (LEN(Parameters) GT 0) {
					ParametersArray = ListToArray(Parameters, "|");
						for (i=1;i LTE ArrayLen(ParametersArray);i=i+1) {
							ParametersSubArray = ListToArray(ParametersArray[i], "=");
							StructInsert(DataStruct, ParametersSubArray[1], ParametersSubArray[2]);
							ArrayClear(ParametersSubArray);
						}
					ArrayClear(ParametersArray);
				} 
				with: */
				parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(inputString=Parameters,delimiter=",");
				StructAppend(DataStruct,parametersStruct);				
				StructInsert(ArgStruct,"DataStruct",DataStruct);
				StructInsert(ArgStruct,"RunTime",RunTime);
			</cfscript>
			Begin Archive Process for: <b>#htmleditformat(GetArchiveData.Name)#</b>.<!--- XXXX-temp --->
			<cfinvoke component = "com/dbTools" method = "#CFFunction#" returnVariable = "runStatus" argumentcollection="#ArgStruct#">
			<cfscript>
				StructClear(parametersStruct);
				StructClear(DataStruct);
				StructClear(ArgStruct);
			</cfscript>
			<br/><b>#htmleditformat(GetArchiveData.Name)#</b> process #htmleditformat(runStatus)#.
		<cfelse>
			<br/><b>#htmleditformat(GetArchiveData.Name)#</b> process cannot be run as it is already running.
		</cfif>
		</p>
		
	</cfloop>
	</cfoutput>
	<cfcatch type="any">
		<cfset x = application.com.dbTools.ErrorMsgNotification(source="ArchiveProcess", errorCatch=cfcatch)>
		<CF_ABORT>
	</cfcatch>
</cftry>
