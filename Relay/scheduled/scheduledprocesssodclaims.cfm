<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scheduledProcessSODclaims.cfm	
Author:				NJH
Date started:		2007/09/24
	
Description:		Processes any claims that are in the SOD claim table for automated points accrual

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed


2009/12/03			NJH			P-LEX041	Added expirePointsForDeletedAccounts parameter that, if set to true, will expire the points if the account that has just
											been given points is deleted.
Possible enhancements:
 --->

<cfparam name="rwPromotionID" type="numeric" default="4">
<cfparam name="numOfClaimsToProcess" type="numeric" default="100">
<cfparam name="expirePointsForDeletedAccounts" type="boolean" default="false"> <!--- NJH 2009/12/03 P-LEX041 --->

<cfsetting requesttimeout="6000" enablecfoutputonly="No">

<cfscript>
	sodSourceQry = application.com.relaySOD.getSODSources();
</cfscript>

<cfloop query="sodSourceQry">
	<!--- NJH 2009/12/03 P-LEX041 - added expirePointsForDeletedAccounts --->
	<cfscript>
		application.com.relaySOD.processSODclaims(numOfClaimsToProcess=numOfClaimsToProcess,sourceID=sourceID,rwPromotionID=rwPromotionID,expirePointsForDeletedAccounts=expirePointsForDeletedAccounts);
	</cfscript>
	
	<!--- get the count of records from SODmaster that have just been processed --->
	<cfquery name="getNumOfClaimsProcessed" datasource="#application.siteDataSource#">
		SELECT COUNT(*) AS processedClaimsCount
		FROM SODMaster
		WHERE
			processDate =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			AND sourceID =  <cf_queryparam value="#sourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfoutput>#htmleditformat(getNumOfClaimsProcessed.processedClaimsCount)# #htmleditformat(sodSourceQry.source)# claims processed.<br></cfoutput>
		
</cfloop>

