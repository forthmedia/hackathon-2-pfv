<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:		scheduledAutoDedupe.cfm	
Author:			GCC
Date started:		2005-09-14
	
Description:		This page can be scheduled to merge the next five entities in a match result set - defined by frmMatchData.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
15-Oct-2008			NJH			CR-TND562 Wrapped the template with <cf_scheduledTask> which logs whether the process has run
								successfully or not. At the moment, it's basic reporting. Could be enhanced to report more specifically on errors that occur.
2009/12/21 WAB  LID 2941  removed a cfflush
2011/03/09 WAB LID 5840 Added something so that when debugging (or trying to dedupe thousands of records) you can leave the process refreshing itself in your browser
2013-11-26	WAB		Case 438202 Removed some old unnecessary code.  Altered the 'keepRunning' so that it refreshes immediately if no matches processed

Enhancements still to do:

1. Create a wrapper to manage all this.
2. Click the totals to view a list of the Orgs for excel / selection etc
 --->

<cfparam name="keepRunning" default="false">
<cfparam name="slave" default="true">
<cf_scheduledTask>
	
<cfset runAsScheduleTask=true> <!--- NJH 2008/10/15 CR-TND562 --->

		<cfinclude template="../datatools/autoDeDupe.cfm">

			<cfif keepRunning>
				<!--- If no items deduped then reload immediately, otherwise wait a few seconds --->
				<cfif isdefined("scheduleResult") and structKeyExists(scheduleResult,"numberdeduped") and scheduleResult.numberDeduped EQ 0>
					<cfset interval = 0>
				<cfelse>
					<cfset interval = 4>
				</cfif>
				
				<!--- WAB when run in a UI it can be set to keep running and running (I found that I needed this while debugging and clearing backlogs)--->
				<cfoutput>
				<script>
				function reload () {
				window.location.href = window.location.href
				}
				
				window.setTimeout (reload,#jsStringFormat(interval)# * 1000)
				</script>
				This page will refresh in #interval# seconds
				</cfoutput>
			<cfelse>
				For debugging purposes you can use this link and the process will page will refresh after every request.<BR>
				You can add a maxNumberToProcess parameter to process more or fewer rows.
				<BR><a href="?keepRunning=true">Click Here For Continuous Running</A><BR>
			</cfif>



</cf_scheduledTask>



auto