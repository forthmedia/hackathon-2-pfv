<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scheduledGeoCodeLocations.cfm	 
Author:				?
Date started:		?
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-12-08	GCC	Changed to use sp_getLatLongForLocation which is the latest and greatest geocoding algorithm. Also changed to only attempt to geocode locations in countries we have geodata for!
Possible enhancements:
because there is no log of attempts it will still attempt to geocode each time.
editing a location will bring it back to the top and there is always the manual 'regeo' button for specfic locations in the front end.


2010/11/? 	WAB LID 4155 - was causing CPU to max out on Lexmark
			Made some changes so that only does x items at a time 
			and works backwards through the db doing any items where lat long is null
			Once reaches the end it starts again
			Also does any items updated since process was last run
2012/07/18	IH	Case 428953 use table aliases to avoid ambiguity
 --->




<cf_head>
	<cf_title>Scheduled Geo Code Locations</cf_title>
</cf_head>


	
<cfparam name="rowsToProcess" default="100">
<cfif not structKeyExists (application,"scheduledGeoCodeLocations")>
	<cfset application.scheduledGeoCodeLocations = {lastrundate = 0,dateReached = 0}>
</cfif>

<cf_scheduledTask name="Geocode Locations">

<cfsetting requesttimeout="595"> <!---runs every 10 minutes --->

<cfquery name="getLocationsNeedingGeoCoding" datasource="#application.siteDataSource#">
	select distinct top #rowsToProcess# l.locationid as locationID,l.lastupdated from location l 
	inner join country c on l.countryid = c.countryid 
	inner join geocountry gc on c.isocode = gc.isocode2
	inner join geodata gd on gc.isocode3 = gd.isocode
	where l.latitude is null and l.longitude is null
	<cfif application.scheduledGeoCodeLocations.dateReached is not 0>	
	and (l.lastupdated >  <cf_queryparam value="#application.scheduledGeoCodeLocations.lastrundate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  or l.lastupdated <  <cf_queryparam value="#application.scheduledGeoCodeLocations.datereached#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  )
	</cfif>
	order by l.lastupdated desc
</cfquery>

<cfif getLocationsNeedingGeoCoding.recordcount gt 0>
	<cfoutput query="getLocationsNeedingGeoCoding">
	<cfquery name="GeoCodeLocations" datasource="#application.siteDataSource#">
			exec sp_getLatLongForLocation @locationID =  <cf_queryparam value="#locationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	</cfoutput>
</cfif>

	<cfset application.scheduledGeoCodeLocations.lastrundate = now()>

	<cfif getLocationsNeedingGeoCoding.recordcount is rowsToProcess> <!--- if we get the maximum number of rows then we can assume that we need to continue working backwards, if we get less that the max number we must have done all that are left to do and need to go back to the beginning --->
	    <cfset application.scheduledGeoCodeLocations.datereached = createodbcdatetime(getLocationsNeedingGeoCoding.lastupdated[getLocationsNeedingGeoCoding.recordcount])>
	<cfelse>
		<cfset application.scheduledGeoCodeLocations.datereached = 0>
	</cfif>

	<cfoutput>#getLocationsNeedingGeoCoding.recordcount# Locations Processed</cfoutput>

</cf_scheduledTask>



