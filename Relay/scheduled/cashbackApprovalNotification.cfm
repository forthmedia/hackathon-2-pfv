<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		cashbackApprovalNotification.cfm	
Author:			NJH
Date started:	2009/03/09
	
Description:	This sends out emails to all approvers with the count of people in the countries to which they have rights to
			who still are awaiting approval

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


 --->

<cf_scheduledTask>

<cf_translate>

<cfparam name="approvalUserGroupIDList" type="string" default="">

<cfif approvalUserGroupIDList eq "">
	
	<!--- if no id has been passed through, find a usergroup with the name of 'Cashback Approver' --->
	<cfquery name="getDefaultCashbackApproverID" datasource="#application.siteDataSource#">
		select userGroupID from userGroup where name='Cashback Approver'
	</cfquery>
	
	<cfif getDefaultCashbackApproverID.recordCount neq 0>
		<cfset approvalUserGroupIDList = getDefaultCashbackApproverID.userGroupID>
	</cfif>
</cfif>

<cfif approvalUserGroupIDList eq "">
	<cfset message = "You must either pass in a list of userGroupIDs that the cashback approvers belong to or create a cashback approver usergroup with the name of 'Cashback Approver'.">
	<cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput>
	<cfset scheduleResult.message = message>
		
<cfelse>

	<cf_translate phrases="phr_cashback_numPeopleWithClaimsAwaitingApproval"/>
	
	<cfquery name="getApprovers" datasource="#application.siteDataSource#">
		SELECT DISTINCT p.FirstName + ' ' + p.LastName AS fullname, p.Email, p.PersonID
			FROM Person p with (noLock) INNER JOIN
			     UserGroup u with (noLock) ON u.PersonID = p.PersonID INNER JOIN
			     Rights r with (noLock) on r.UserGroupID = u.UserGroupID INNER JOIN
			     Country c with (noLock) ON c.CountryID = r.CountryID INNER JOIN
			     RightsGroup rg with (noLock) ON rg.personID = p.personID
		WHERE (p.LoginExpires > GETDATE()) AND (p.Password <> N' ') 
			AND (p.Active <> 0) 
			and (p.Username <> ' ')
			and rg.userGroupID  in ( <cf_queryparam value="#approvalUserGroupIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfquery>
		
	<cfset scheduleResult.message = "The cashback notification email has been sent to the following approvers:<br>">

	<cfloop query="getApprovers">
	
		<cfset countryList = application.com.rights.getCountryRightsIDList(personID)>
		
		<cfquery name="getCountOfPeopleWithSubmittedClaims" datasource="#application.siteDataSource#">
			select count(distinct v.personID) as countPeople, c.CountryDescription
			from vCashbackClaimList v inner join
			     country c with (noLock) ON v.orgCountryID = c.CountryID
			where (v.orgCountryID  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
				and v.status = 'Submitted'
			group by c.CountryDescription
			order by c.CountryDescription
		</cfquery>
	
		<cfif getCountOfPeopleWithSubmittedClaims.recordCount gt 0>
			
			<cfset toEmail = "#FullName# <#email#>">
			
			<cfsavecontent variable="emailBody_html">
				<cf_translate>
				<cfoutput>
					
						
						
							<p>phr_cashback_PeopleWithSubmittedClaimsByCountry</p>
							<table class="withBorder" cellpadding=0 cellspacing=0 border=0 width="100%">
								<tr>
									<th>phr_Country</th>
									<th>phr_cashback_numPeopleWithClaimsAwaitingApproval</th>
								</tr>
								<cfloop query="getCountOfPeopleWithSubmittedClaims">
									<tr>
										<td align="center">#htmleditformat(CountryDescription)#</td>
										<td align="center">#htmleditformat(countPeople)#</td>
									</tr>
								</cfloop>
							</table>
						
					
				</cfoutput>
				</cf_translate>
			</cfsavecontent>
			
			<cfif application.testSite neq 0>
				<cfset toEmail = "nathanielh@foundation-network.com">
			</cfif>
		
			<!--- only send email if there are people with claims awaiting approval. --->
			<cfset scheduleResult.message = scheduleResult.message & "#FullName# (#toEmail#)<br>">
			
			<cf_mail to="#toEmail#" from="#application.adminPostMaster#" subject="#phr_cashback_numPeopleWithClaimsAwaitingApproval#" type="html" bcc="#application.bccEmail#">
				#emailBody_html#
			</cf_mail>
		</cfif>	
	</cfloop>
</cfif>

</cf_translate>

</cf_scheduledTask>
