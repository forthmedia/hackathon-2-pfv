<!--- 2014-07-30 PPB Case 441083 this is a utility to fire off the updates so they can be done immediately during a demo (for expediency... and to hide the issue that they may not be triggering automatically in a timely fashion) --->

<cfset resultCertifications = application.com.housekeeping.updateTrainingCertifications()>

Certifications updated successfully: <cfoutput>#resultCertifications.isOK#</cfoutput><br /><br />

<cfset resultSpecialisations = application.com.housekeeping.updateTrainingSpecialisations()>

Specialisations updated successfully: <cfoutput>#resultCertifications.isOK#</cfoutput><br /><br />


