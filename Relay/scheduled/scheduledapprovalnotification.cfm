<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			ScheduledApprovalNotification.cfm	
Author:				NJH
Date started:		2006-09-22
	
Purpose:	

Usage: 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->

<cfsetting showdebugoutput="No" enablecfoutputonly="No">

<cfparam name="notificationType" type="string" default="internal">

<cfif notificationType eq "internal">
	<cf_include template="\code\cftemplates\scheduledInternalApprovalNotification.cfm" checkIfExists="true">
	<!--- <cfif fileExists("#application.paths.code#\cftemplates\scheduledInternalApprovalNotification.cfm")>
		<cfinclude template="\code\cftemplates\scheduledInternalApprovalNotification.cfm">
	</cfif> --->
<cfelse>
	<cf_include template="\code\cftemplates\scheduledExternalApprovalNotification.cfm" checkIfExists="true">
	<!--- <cfif fileExists("#application.paths.code#\cftemplates\scheduledExternalApprovalNotification.cfm")>
		<cfinclude template="\code\cftemplates\scheduledExternalApprovalNotification.cfm">
	</cfif> --->
</cfif>