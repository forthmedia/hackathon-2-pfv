<!--- 
scheduled\Housekeeping.cfm

WAB 2016-03-17	PROD2016-118  Housekeeping Improvements

This template is run on a self setting schedule by the housekeeping system
It can also be used to 
	i)  Run all outstanding jobs manually with the ?run option
	ii) Force an individual job to run with the ?name= option
	
It displays the status of all housekeeping jobs by including the report in /admin/housekeepingReport.cfm

WAB 2016-10-17	PROD2016-2542 Problems when dbTime and CFTime are wildly different.  Variable scheduledToRunAt becomes scheduledToRunAtCFTime
--->

<cfset houseKeepingObject = application.com.houseKeeping>


<cfif isdefined("Name")>
	<!--- 	Run a specific Task 
			Note, this code is copied in \admin\housekeepingReport.cfm
	--->
	<cfif structKeyExists(url,"instanceID") and url.InstanceId is not application.instance.coldfusionInstanceId>
		<cfset result = houseKeepingObject.callHouseKeepingOnAnotherInstance(houseKeepingMethodName = name, force = true, coldfusionInstanceId = url.instanceid, timeout =30 )>
	<cfelse>
		<cfset result = houseKeepingObject.runHousekeeping(houseKeepingMethodName = name, force = true)>
	</cfif>
	
	<cfoutput>Task #htmlEditFormat(name)# result<br /></cfoutput>
	<cfdump var="#result.results[name]#">

<cfelse>
	
	<cfset TimeNextNeedsRunning = houseKeepingObject.getTimeNextNeedsRunning()>
	<cfoutput>
		<cfif NOT houseKeepingObject.isScheduledTaskSetUp() and not houseKeepingObject.IsHouseKeepingRunning()>
			The housekeeping schedule does not appear to be set up<br />
		<cfelseif houseKeepingObject.IsHouseKeepingRunning()>
			Housekeeping is running at the moment<br />
		<cfelse>		
			<cfset timeToNextRun = dateDiff("n",now(), houseKeepingObject.scheduledToRunAtCFTime)>
			Next Run <cfif timeToNextRun gte 0>is due to run in #timeToNextRun# minute<cfif timeToNextRun gt 1>s</cfif>.<cfelse>was due to run #timeToNextRun# minutes ago.</cfif>  At #TimeFormat(houseKeepingObject.scheduledToRunAtCFTime)# <br /><br />
		</cfif>
	</cfoutput>

	<!--- When called by cfschedule, run housekeeping --->
	<cfif cgi.HTTP_USER_AGENT is "CFSCHEDULE" or structKeyExists (url,"run") >
		<cfset runResult  = houseKeepingObject.runHouseKeepingIfRequired()>

		<cfif structKeyExists (url,"run")>
			Result of this run<br />
			<cfdump var="#runResult#">
		</cfif> 
		
	</cfif>

</cfif>


<!--- for mere mortals visiting this page, give them a report --->
<cfif cgi.HTTP_USER_AGENT is not "CFSCHEDULE">
	<!--- 	The report also has code for running individual tasks.  
			To prevent tasks running twice unnecessarily, delete the url.name parameter --->
	<cfset structDelete (url,"name")>
	<cfinclude template = "/admin/housekeepingReport.cfm">
	<a href="?run">Click this link to run all outstanding jobs manually</a>
</cfif>	



