<!--- 
File name:		CertificationRuleActivationNotification.cfm	
Author:			PPB
Date started:	2013-11-29
		
Description:	This sends out emails to all students when an Certification Rule becomes Activated

Usage:	It should be run on a schedule once (and only once) per day		
		
Parameters : 	runDate	(optional) format:YYYY-MM-DD - to run the schedule for a date other than today (typically if there are rules that have activation date set during a period that it hasn't been run) 

Amendment History:

Date 		Initials 	What was changed
2013-11-29	PPB			Case 438029 New template 	

Enhancements still to do:

 --->

<cfparam name="runDate" type="string" default="#DateFormat(request.requestTime,'yyyy-mm-dd')#">

<cf_scheduledTask>

	<cfquery name="qryCertificationsWithRuleJustActivated" datasource="#application.siteDataSource#">
		SELECT DISTINCT tc.CertificationId
			,'phr_title_trngcertification_' + convert(varchar,tc.certificationid) AS Title
		FROM trngCertificationRule tcr
		INNER JOIN trngCertification tc ON tc.CertificationId=tcr.CertificationId 
		WHERE dbo.dateAtMidnight(tcr.activationDate)  =  <cf_queryparam value="#runDate#" CFSQLTYPE="CF_SQL_VARCHAR" > 		
	</cfquery>

	<!--- form variables retained for compatibility with certificationRules.cfm (for use by sendmail) --->
	<cfset form.CertificationID = qryCertificationsWithRuleJustActivated.CertificationID>
	<cfset form.Certification = application.com.relayTranslations.translatePhrase(phrase=qryCertificationsWithRuleJustActivated.Title)/>		

	<cfset scheduleResult.message = "">

	<cfloop query="qryCertificationsWithRuleJustActivated">

		<cfset scheduleResult.message = scheduleResult.message & "<br>TrngCertificationUpdateRequired email has been sent to the following people for CertificationID=#CertificationID#:<br>">

		<cfset getPeopleWithThisPassedCertification=application.com.relayElearning.getPersonCertificationDetails(certificationID=certificationID,statusTextId='Passed,Pending')>		<!--- misnomer "getPeopleWithThisPassedCertification" retained for compatibility with certificationRules.cfm (query includes Pending too)  --->
		<cfloop query="getPeopleWithThisPassedCertification">
			<cfset form.RegistrationDate = getPeopleWithThisPassedCertification.RegistrationDate>
			<cfset form.PassDate = getPeopleWithThisPassedCertification.PassDate>

			<cfset application.com.email.sendEmail(personID=getPeopleWithThisPassedCertification.personID,emailTextID='TrngCertificationUpdateRequired')>

			<cfset scheduleResult.message = scheduleResult.message & "#getPeopleWithThisPassedCertification.personID#,">
		</cfloop>

		<cfset scheduleResult.message = scheduleResult.message & "<br>TrngNewCertificationRuleAdded email has been sent to the following people for CertificationID=#CertificationID#:<br>">

		<cfset getPeopleWithThisRegisteredCertification=application.com.relayElearning.getPersonCertificationDetails(certificationID=certificationID,statusTextId='Registered')>
		<cfloop query="getPeopleWithThisRegisteredCertification">
			<cfset form.RegistrationDate = getPeopleWithThisRegisteredCertification.RegistrationDate>

			<cfset application.com.email.sendEmail(personID=getPeopleWithThisRegisteredCertification.personID,emailTextID='TrngNewCertificationRuleAdded')>
			
			<cfset scheduleResult.message = scheduleResult.message & "#getPeopleWithThisRegisteredCertification.personID#,">
		</cfloop>
		
	</cfloop>

</cf_scheduledTask>
