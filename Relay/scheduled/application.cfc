<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="applicationMain" output="false">

	<cffunction name="onRequest" returnType="void">
		<cfargument name="thePage" type="string" required="true"> 
		
		<cfif not isDefined("HTTP_Host")>
			<cfset HTTP_Host = "">
		</cfif>
        <cfinclude template="#thePage#">
	</cffunction>
	
</cfcomponent>
