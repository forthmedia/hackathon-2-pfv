<!--- �Relayware. All Rights Reserved 2014 --->
<cfif fileexists("#application.paths.code#\CFTemplates\scheduled\CustomAkamaiExcludeFiles.cfm")>

	<cfinclude template="\code\CFTemplates\scheduled\CustomAkamaiExcludeFiles.cfm">

	<cfif PublicActive>

		    <cfif NOT DirectoryExists("#LocalPublicContentPath#")>
		    	Public Content Directory Not Found<CF_ABORT>
		    </cfif>

		    <cfquery name="queryAllPublicFileType" datasource="#application.siteDataSource#">
			SELECT
			'#UseMethod#' +
			CONVERT(VARCHAR(100), ROW_NUMBER() OVER (ORDER BY listfile) - 1) +
			'=#LocalPublicContentPath#' + listfile AS 'listfile' FROM
			(
				SELECT REPLACE(LTRIM(RTRIM(ft1.[path])),  '/', '\')  + '\' + LTRIM(RTRIM(f1.[filename])) as 'listfile', fileID
				FROM files f1,
				filetype ft1,
				filetypegroup fg1
				WHERE
				  LTRIM(RTRIM(f1.DeliveryMethod)) #useCondition# <cfqueryparam value = "#ServiceName#" CFSQLType = "CF_SQL_VARCHAR"> AND
				  ft1.filetypeid = f1.filetypeid AND
				  fg1.filetypegroupid = ft1.filetypegroupid AND
				  ft1.secure = <cfqueryparam value = "0" CFSQLType = "CF_SQL_INTEGER">
			      
				UNION
			      
				SELECT REPLACE(LTRIM(RTRIM(ft1.[path])),  '/', '\')  + '\' + LTRIM(RTRIM(f1.fileID))  as 'listfile', fileID
				FROM files f1,
				filetype ft1,
				filetypegroup fg1
				WHERE
				  LTRIM(RTRIM(f1.DeliveryMethod)) #useCondition# <cfqueryparam value = "#ServiceName#" CFSQLType = "CF_SQL_VARCHAR"> AND
				  LTRIM(RTRIM(f1.filename)) LIKE <cfqueryparam value = "%.zip" CFSQLType = "CF_SQL_VARCHAR"> AND
				  ft1.filetypeid = f1.filetypeid AND
				  fg1.filetypegroupid = ft1.filetypegroupid AND
				  ft1.secure = <cfqueryparam value = "0" CFSQLType = "CF_SQL_INTEGER">
			) AS x
		    </cfquery>
		    
		    <cfset TextLoop = "">
		    
		    <cfif DirectoryExists("#conFigPath#")>
		    
			    <cfif FileExists("#conFigPath##conFigPublicPart1#")>
			    
			    	<cffile 
					    action = "read"
	    				file = "#conFigPath##conFigPublicPart1#"
	    				variable="pubPart1">
	
				<cfelse>
					
					Public Config File Part 1 Not Found<CF_ABORT>
	
				</cfif>
			
			<cfelse>
				
				Config Directory Not Found<CF_ABORT>

			</cfif>
			
		    <cfif DirectoryExists("#conFigPath#")>
		
			    <cfif FileExists("#conFigPath##conFigPublicPart2#")>
			
			    	<cffile 
					    action = "read" 
	    				file = "#conFigPath##conFigPublicPart2#"
	    				variable="pubPart2">
	
				<cfelse>
					
					Public Config File Part 2 Not Found<CF_ABORT>
	
				</cfif>	
			
			<cfelse>
				
				Config Directory Not Found<CF_ABORT>

			</cfif>	
		    
		    <cfif DirectoryExists("#deployPath#")>

		    	<cffile 
				    action = "write" 
	   				file = "#deployPath##deployPublicFile#"
	   				output = "#pubPart1#">	
			
			<cfelse>
				
				Deploy Directory Not Found<CF_ABORT>

			</cfif>	
		    

		    <cfoutput query="queryAllPublicFileType">

			    <cfif DirectoryExists("#deployPath#")>
	
			    	<cffile 
					    action = "append"
		 				file = "#deployPath##deployPublicFile#"
					    output = "#listfile#"
						addNewLine = "yes"
					    fixnewline = "yes">	
				
				<cfelse>
					
					Deploy Directory Not Found<CF_ABORT>
	
				</cfif>	

			</cfoutput>
			
		    <cfif DirectoryExists("#deployPath#")>
			
			    	<cffile 
					    action = "append" 
	    				file = "#deployPath##deployPublicFile#"
	    				output = "#pubPart2#"
						addNewLine = "yes"
				    	fixnewline = "yes">	
			
			<cfelse>
				
				Deploy Directory Not Found<CF_ABORT>

			</cfif>
			
	</cfif>
			
	<cfif SecureActive>
	
		    <cfif NOT DirectoryExists("#LocalSecureContentPath#")>
		    	Secure Content Directory Not Found<CF_ABORT>
		    </cfif>

		    <cfquery name="queryAllSecureFileType" datasource="#application.siteDataSource#">
			SELECT
			'#UseSecureMethod#' +
			CONVERT(VARCHAR(100), ROW_NUMBER() OVER (ORDER BY listfile) - 1) +
			'=#LocalSecureContentPath#' + listfile AS 'listfile' FROM
			(
				SELECT REPLACE(LTRIM(RTRIM(ft1.[path])),  '/', '\')  + '\' + LTRIM(RTRIM(f1.[filename])) as 'listfile', fileID
				FROM files f1,
				filetype ft1,
				filetypegroup fg1
				WHERE
				  LTRIM(RTRIM(f1.DeliveryMethod)) #useSecureCondition# <cfqueryparam value = "#ServiceName#" CFSQLType = "CF_SQL_VARCHAR"> AND
				  ft1.filetypeid = f1.filetypeid AND
				  fg1.filetypegroupid = ft1.filetypegroupid AND
				  ft1.secure = <cfqueryparam value = "1" CFSQLType = "CF_SQL_INTEGER">
			      
				UNION
			      
				SELECT REPLACE(LTRIM(RTRIM(ft1.[path])),  '/', '\') + '\' + LTRIM(RTRIM(f1.fileID))  as 'listfile', fileID
				FROM files f1,
				filetype ft1,
				filetypegroup fg1
				WHERE
				  LTRIM(RTRIM(f1.DeliveryMethod)) #useSecureCondition# <cfqueryparam value = "#ServiceName#" CFSQLType = "CF_SQL_VARCHAR"> AND
				  LTRIM(RTRIM(f1.filename)) LIKE <cfqueryparam value = "%.zip" CFSQLType = "CF_SQL_VARCHAR"> AND
				  ft1.filetypeid = f1.filetypeid AND
				  fg1.filetypegroupid = ft1.filetypegroupid AND
				  ft1.secure = <cfqueryparam value = "1" CFSQLType = "CF_SQL_INTEGER">
			) AS x
		    </cfquery>
		    
		    <cfset TextLoop = "">
		    
		    <cfif DirectoryExists("#conFigPath#")>
		    
			    <cfif FileExists("#conFigPath##conFigSecurePart1#")>
			    
			    	<cffile 
					    action = "read"
	    				file = "#conFigPath##conFigSecurePart1#"
	    				variable="pubPart1">
	
				<cfelse>
					
					Secure Config File Part 1 Not Found<CF_ABORT>
	
				</cfif>
			
			<cfelse>
				
				Config Directory Not Found<CF_ABORT>

			</cfif>
			
		    <cfif DirectoryExists("#conFigPath#")>
		
			    <cfif FileExists("#conFigPath##conFigSecurePart2#")>
			
			    	<cffile 
					    action = "read" 
	    				file = "#conFigPath##conFigSecurePart2#"
	    				variable="pubPart2">
	
				<cfelse>
					
					Secure Config File Part 2 Not Found<CF_ABORT>
	
				</cfif>	
			
			<cfelse>
				
				Config Directory Not Found<CF_ABORT>

			</cfif>	
		    
		    <cfif DirectoryExists("#deployPath#")>

		    	<cffile 
				    action = "write" 
	   				file = "#deployPath##deploySecureFile#"
	   				output = "#pubPart1#" charset="UTF-8">	
			
			<cfelse>
				
				Deploy Directory Not Found<CF_ABORT>

			</cfif>	
		    

		    <cfoutput query="queryAllSecureFileType">

			    <cfif DirectoryExists("#deployPath#")>
	
			    	<cffile 
					    action = "append"
		 				file = "#deployPath##deploySecureFile#"
					    output = "#listfile#"
						addNewLine = "yes"
					    fixnewline = "yes">	
				
				<cfelse>
					
					Deploy Directory Not Found<CF_ABORT>
	
				</cfif>	

			</cfoutput>
			
		    <cfif DirectoryExists("#deployPath#")>
			
			    	<cffile 
					    action = "append" 
	    				file = "#deployPath##deploySecureFile#"
	    				output = "#pubPart2#"
						addNewLine = "yes"
				    	fixnewline = "yes">	
			
			<cfelse>
				
				Deploy Directory Not Found<CF_ABORT>

			</cfif>
			
	</cfif>
		
	<cfif PublicActive>
		Public Files Complete
	<cfelse>
		Public Not Active
	</cfif>
	<br />			
	<cfif SecureActive>
		Secure Files Complete
	<cfelse>
		Secure Not Active
	</cfif>
	
<cfelse>

	Akamai Exclude Files from FTP has not been setup
	<CF_ABORT>
	
</cfif>

