<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		scheduledTaskMonitor.cfm	
Author:			NJH  
Date started:	14-10-2008
	
Description:	Monitors scheduled tasks and sends out emails where a scheduled task has produced an error. An email will go out:
				a) if it's the first time an error has occurred or
				b) if an error occurred after the last email notification
				
				If the last run was a successful run but an error occurred recently, let them know that it's run successfully last, but that errors did occur.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-09-10			IH			Case 430187 check if wddxScheduleResult is a struct before structKeyExists

Possible enhancements:


 --->

<!--- forces the errorHandler_exception.cfm file to run --->
<cfset request.relayCurrentUser.showErrors = false>

<cftry>

	<cfquery name="getScheduledTasks" datasource="#application.siteDataSource#">
		select * from scheduledTaskDefinition
	</cfquery>
	
	<cfloop query="getScheduledTasks">
		
		<!--- the query lets us know whether an email has already been sent for this error. If so, it gets the date that
			the last email was sent. Otherwise, we get the first date that the error was reported. --->
		<cfquery name="HasEmailBeenSentForThisError" datasource="#application.siteDataSource#">
			select count(1) as countEmailNotifications, max(emailSent) as lastEmailSentDate, min(created) as firstErrorLogEntry
			from scheduledTaskLog 
			where emailSent is not null 
				and actioned <> 1
				and errorID is not null
				and scheduledTaskDefID =  <cf_queryparam value="#scheduledTaskDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<!--- get all entries that occurred either after the last email notification (if an email ever went out) or
			since we started logging the scheduled task information --->
		<cfquery name="getLogEntriesSinceLastNotification" datasource="#application.siteDataSource#">
			select stl.result, stl.startTime, stl.endTime, stl.parameters, re.diagnostics, stl.errorID
			from scheduledTaskLog stl with (noLock) left join relayError re with (noLock)
				on stl.errorID = re.relayErrorID
			where scheduledTaskDefID =  <cf_queryparam value="#scheduledTaskDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and emailSent is null
				and actioned <> 1
				and endTime > 
				<cfif HasEmailBeenSentForThisError.lastEmailSentDate neq "">
					'#HasEmailBeenSentForThisError.lastEmailSentDate#'
				<cfelse>
					'#HasEmailBeenSentForThisError.firstErrorLogEntry#'
				</cfif>
				and endTime is not null <!--- get only those tasks that have completed. --->
			order by startTime desc
		</cfquery>
		
		<!--- get the errors that have happened since the last email went out --->
		<cfquery name="getErrorsSinceLastNotification" dbType="query">
			select * from getLogEntriesSinceLastNotification where errorID is not null
		</cfquery>
		
		<!--- set supportEmailAddress for each task --->
		<cfset supportEmailAddress = "nathanielh@foundation-network.com">
		<cfif application.testSite eq 0>
			<cfset supportEmailAddress = "relayHelp@foundation-network.com">
		</cfif>
		
		<cfset clientSupportEmailAddress = application.com.settings.getSetting("emailaddresses.scheduledTaskSupportEmail")>
		
		<!--- if the client wants to be notified, send them the email as well --->
		<cfif emailClient>
			<!--- the clientSupportEmail field can be used as an override for different tasks--->
			<cfif clientSupportEmail neq "">
				<cfset clientSupportEmailAddress = clientSupportEmail>
			</cfif>
			<cfif clientSupportEmailAddress neq "">
				<cfset supportEmailAddress = "#supportEmailAddress#;#clientSupportEmailAddress#">
			</cfif>
		</cfif>
		
		<!--- if errors have occured since the last notification went out, inform relayHelp and/or the client --->	
		<cfif getErrorsSinceLastNotification.recordCount gt 0>
		
			<cfmail to="#supportEmailAddress#" from="errors@foundation-network.com" type="html" subject="#name# Scheduled Task Failure Notification">
				The '#name#' scheduled task has failed #getErrorsSinceLastNotification.recordCount# 
				<cfif getErrorsSinceLastNotification.recordCount eq 1>time<cfelse>times</cfif>
				<cfif HasEmailBeenSentForThisError.countEmailNotifications gt 0>since the last notification which was sent #LSDateFormat(HasEmailBeenSentForThisError.lastEmailSentDate,"dd-mmm-yyyy")# #timeFormat(HasEmailBeenSentForThisError.lastEmailSentDate,"short")#</cfif>
				<!--- if the last entry was not an error, then we let them know that it appears to be running again --->
				<cfif getLogEntriesSinceLastNotification.errorID[1] eq ""> although it appears to be running successfully again</cfif>.
				<br><br>	
				The url that was run: #url# <br>
				<br>
				The task was run without success the following times:<br><br>
				<table>
					<tr>
						<th>Start Time</th><th>End Time</th><th>Parameters</th><th>Result</th>
					</tr>
					<cfloop query="getErrorsSinceLastNotification">
						
						<cfset scheduledTaskResult = diagnostics>
						<cfif result neq "">
							<cfif isJSON(result)>
								<cfset scheduledTaskResult = deserializeJSON(result)>
							<cfelse>
								<cfwddx action="wddx2cfml" input="#result#" output="scheduledTaskResult">
							</cfif>
						</cfif>
						
						<tr valign="top">
							<td>#startTime#</td><td>#endTime#</td><td>#parameters#</td>
							<td>
								<cfif isStruct(scheduledTaskResult) and structKeyExists(scheduledTaskResult,"message")>
									#scheduledTaskResult.message#<br>
								<cfelseif isSimpleValue(scheduledTaskResult)>
									#scheduledTaskResult#<br>
								<cfelse>
									<cfdump var="#scheduledTaskResult#">
								</cfif>
								
								<cfif errorID neq "">
									<br><br><a href="/errorHandler/errorDetails.cfm?errorID=#errorID#" target="_blank">View Details</a> (FNL only)
								</cfif>
							</td>
						</tr>
					</cfloop>
				</table>
			</cfmail>
			
			<!--- update the email sent for all error entries that happened since the previous email notification and that have not been actioned --->
			<cfquery name="updateScheduledTaskLog" datasource="#application.siteDataSource#">
				update scheduledTaskLog
				set emailSent = GetDate(),
					lastUpdated = GetDate(),
					lastUpdatedBy = #request.relayCurrentUser.userGroupId#
				where scheduledTaskDefID =  <cf_queryparam value="#scheduledTaskDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and emailSent is null
					and actioned <> 1
					and errorID is not null
					and endTime >=
					<cfif HasEmailBeenSentForThisError.lastEmailSentDate neq "">
						'#HasEmailBeenSentForThisError.lastEmailSentDate#'
					<cfelse>
						'#HasEmailBeenSentForThisError.firstErrorLogEntry#'
					</cfif>
			</cfquery>
			
		</cfif>
	
	</cfloop>
	
	<cfcatch type="any">
		<cfrethrow>
	</cfcatch>
</cftry>