<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			runErrorSummary.cfm	
Author:				SWJ
Date started:		2006-09-03
	
Description:		This calls /utilities/errorSummary.cfm

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
14-Oct-2008			NJH			CR-TND562 Added the scheduleTask custom tag to log deletion processing on a basic level. (ie. whether it ran or not)



Possible enhancements:


 --->
<cf_scheduledTask>

<cfinclude template="../Utilities/updateErrorTypes.cfm">
<cfset URL.SendEmail = true>
<cfinclude template="../Utilities/errorSummary.cfm">
</cf_scheduledTask>