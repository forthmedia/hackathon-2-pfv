<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scheduledRWPointsExpiryGlobal.cfm
Author:				NJH
Date started:		2008/04/15
	
Description:		Expires rewards points on the day that it's run.

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:
 --->
 
<cfparam name="expiryDate" type="date" default="#now()#">
<cfparam name="runExpirePoints" type="boolean" default="false"> <!--- added this parameter so that you couldn't accidentally expire points --->

<cfset expiryDate = createODBCdateTime(expiryDate)>

<cfif runExpirePoints>
<cfquery name="expirePoints" datasource="#application.siteDataSource#">
	exec RWExpirePointsGlobal @ExpiredDate =  <cf_queryparam value="#expiryDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
</cfquery>
Done
<cfelse>
	Expire Points not run. Must set runExpirePoints to true.
</cfif>

