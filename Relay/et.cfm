<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes" >

<!---
File name:		et.cfm
Author:			unknown
Date created:	unknown

	Provides the entity handler for the relay software package.

	Syntax	  -	None

	Parameters - None required

	Return Codes - None

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
17-MAR-2005 AR Implemented the securityProfile to enable the switching on and off of flags.
2007/02/22 GCC attempt to set country based on url.lang to get country translations
2008/07/14 		WAB 		removed references to relayCurrentVersion.relaycurrentUser Version
2009/06/15 WAB   mods so that seid links are tracked properly
2009-07-14 NYB 	P-FNL069 - changed logout section, if request.remove UserCookieOnLogout is defined & true then User & DeJaVu cookies are deleted
2010/06/29		NJH			P-FNL069 - check if password is valid. If password is expired or is about to expire, then send them to the 'Change password' page.
							If password is about to expire, then the user will be logged in; otherwise not.
2011-01-26 NYB 	added Message and IsOK from com.login.authenticateUserV2 to request.loginResult
2011-10-12 NYB LHID7848 fixing usage of magic numbers (tepid) - was broken in a previous (post 8.3) version of RW
2012-09-06 PPB Case 430058 when a portal user clicks on a link to a secure file in an email but they are not logged in they are redirected to login and then the file loads in a pop-up
2013-01-29	WAB P-LEN103 2013-01-29 replace code which calls applySecurityProfile() with a call to isPersonAValidUserOnTheCurrentSite() - so that this function can be extended
Enhancement still to do:

--->

<!--- WAB 2010/11/09 added a special check to prevent this file being called on an internal site, redirect --->
<cfif request.currentSite.isInternal>
	<cfoutput>
	<script>
		window.location.href = window.location.protocol + '//' + window.location.hostname +  '/index.cfm'
	</script>
	<cfexit>
	</cfoutput>
</cfif>

<cfparam name="request.defaultExternalElementETID" default="#application.defaultExternalElementETID#">

<cfscript>
	//set (hardcode) legacy level
	level = 2;

	//if user is logging in with their username and password
	if (structKeyExists(form, "partnerusername") and structKeyExists(form, "partnerpassword")) {
		checkLoginStruct = application.com.login.authenticateUserV2(form.partnerusername,form.partnerpassword);
		//NYB 2011-01-26 start
		request.loginResult.IsOK = checkLoginStruct.IsOK;
		request.loginResult.message = checkLoginStruct.message;
		//NYB 2011-01-26 end
		checkLogin = checkLoginStruct.userQuery;
	}

	// SWJ 2005-05-30 added for CR_SNY517 Rewards Rebrand but this is a useful method of autolgin between Relay sites
	// if trusted encrypted magic number (tepid) was passed for automatic login
	else if(structKeyExists(URL, "tepid")){
		decryptedMagicNumber = application.com.encryption.decryptMagicNumber(tepid);
		checkLogin = application.com.login.autoAuthenticateUser(listFirstP=listFirst(decryptedMagicNumber,"-"),listLastP=listLast(decryptedMagicNumber,"-"));
		// GCC 2007/04/19 if using a tepid and wanting to go anywhere else than homepage logged in:
		if(structKeyExists(URL, "eid")){
			form.urlrequested = "etid=#url.EID#";
		} else if(structKeyExists(URL, "etid")){
			form.urlrequested = "etid=#url.EtID#";
		}
	}

	//if magic number was passed for automatic login
//  2005-12-14  WAB removed  - I don't think that it should be allowed to log on just with a p-number
//	else if(structKeyExists(URL, "p") and listlen(P,"-") IS 2){
//		checkLogin = application.com.login.autoAuthenticateUser(listFirst(URL.P,"-"),listLast(URL.P,"-"));
//	}

	//if user is logging in with their email and dateOfBirth
	// WAB 2005-01-02 added an 'else' just here - surely it was required!
	else if(structKeyExists(form, "login_email") and len(login_email) gt 0 and structKeyExists(form, "login_DOBDay") and len(login_DOBDay) gt 0
		and structKeyExists(form, "login_DOBMonth") and len(login_DOBMonth) gt 0 and structKeyExists(form, "login_DOBYear") and len(login_DOBYear) gt 0){
			loginDOB = CreateDateTime(login_DOBYear,login_DOBMonth,login_DOBDay,00,00,00);
			if (isDate(loginDOB)) {
				checkLogin = application.com.login.authenticateUser(form.login_email,loginDOB,"emailDoB");
			}
			else
			{
			request.fault = true;
			}
	}

// WAB wanted to add this, but it didn't work because on login failure the person was taken to a default login page and
// not back the page they had come from - could be sorted with an appropriate variable
	// user using a login form with p-number and lastname
// 	else if(structKeyExists(form,"pNumber") and structKeyExists(form, "lastname")){
//		if (listLen(form.pNumber,"-") is 2 and len(form.lastname) gt 0) {
//			checkLogin = application.com.login.autoAuthenticateUser(listFirst(pNumber,"-"),listLast(pNumber,"-"),application.siteDataSource,form.lastname);
//		} else {
//			request.fault = true;
//		}
//	}

	//if none of the above happened
	else{
		// WAB 2006-05-30 CR_SNY584
		// not sure why frmPersonid should be set to 0 here
		// surely if we know who they are we could set it correctly! so changed to test for unknown user
			//frmpersonid = 0;
		if (request.relaycurrentuser.isUnknownUser)	{
			frmpersonid = 0;
		} else {
			frmpersonid = request.relaycurrentuser.personid;
		}
	}
</cfscript>

<!--- url variable lang can be used to set the language variables for the
		pages being viewed. If lang is detected the set up the variables for that language
		by looking up in language Structure--->
<!--- 2007/02/22 GCC attempt to set country based on url.lang to get country translations --->
<cfscript>
	if (structKeyExists(URL,"lang")) {
		if (listlen(url.lang,"-") eq 2) {
			theLang = listFirst(URL.lang,"-");
				theCountry = listLast(URL.lang,"-");
			x = application.com.relaycurrentuser.setShowContentForCountryID(countryID=application.countryIDLookUpFromIsocodeStr[theCountry]);
		} else {
			theLang = URL.Lang;
		}
		langReturnVar = application.com.relayTranslations.setLanguageVarsFromISOCode(theLang);
	}

	if (isDefined("FORM.frmShowLanguageID") and isNumeric(FORM.frmShowLanguageID) AND FORM.frmShowLanguageID is not "") {
		langReturnVar = application.com.relayTranslations.setLanguageVarsFromLanguageID(FORM.frmShowLanguageID);
	}

</cfscript>

<!--- GCC 2005/08/11 - Set manual override session param --->
<cfif isDefined("FORM.frmShowLanguageID") and isNumeric(FORM.frmShowLanguageID) AND FORM.frmShowLanguageID is not "">
	<cflock name = "userChosenLanguage" timeout="1" throwontimeout="Yes" type="EXCLUSIVE" >
		<cfset session.userChosenLanguage = 1>
	</cflock>
</cfif>

<cfif isDefined("pBLFID")><!---  and isNumeric(pBLFID) --->
	<!--- if pBLFID is detected in any scope then the session variable
	below is set up.  This is then detected later when the person is logged in.
	So this can be used when a user comes to the site, we don't necessarily know then
	and we need to set a flag when they have passed through checkLogin below.  --->

	<cfif not isdefined("session.setThisPersonFlagOnLogin")>
		<cfset session.setThisPersonFlagOnLogin = "">
	</cfif>
	<cfset session.setThisPersonFlagOnLogin = listappend(session.setThisPersonFlagOnLogin,pBLFID)>
</cfif>

<!--- now do actions depending on whether login is good --->
<cfif isdefined("checkLogin")>
	<cfset failWithRelocate = False>
	<!--- 2011-10-12 NYB LHID7848 added isdefined("checkLoginStruct") as this isn't created when using a magic number (tepid) --->
	<cfif isdefined("checkLoginStruct") and not checkLoginStruct.isOK and checkLoginStruct.reason is "passwordExpired">
		<cfset failWithRelocate = true>
	</cfif>
	<!---If a straight match--->
	<cfif CheckLogin.RecordCount EQ 1 or failWithRelocate>

		<cfscript>
			if (failWithRelocate) {
				form.urlRequested = "etid=resend_Password&spl=#application.com.emailmerge.generatePasswordLink(personID=checkLoginStruct.personID)#&message=phr_login_passwordExpired&pwe=1";
			} else {

				/*
				WAB P-LEN103 2013-01-29 replace this block of code and call to applySecurityProfile() with call to to isPersonAValidUserOnTheCurrentSite() which encapsulates the same code (and can be extended more easily)
			    function still returns .failETID, but .continueLogin becomes .isValidUser

				profileSuccess = structNew();
				profileSuccess.continueLogin = true;
				profileSuccess.failETID = "";
				if (request.currentsite.securityProfile is not ""){
					securityProfileListlength = listlen(request.currentsite.securityProfile);    // WAB 2005-09-19 error here - didn't have the listlen function - amazing that it ever worked!!

					for(i=1; i lte securityProfileListlength; i=i+1) {
					profileSuccess = application.com.login.applySecurityProfile (personID = CheckLogin.personID,securityProfileID = ListGetAt(request.currentsite.securityProfile,i));
					if (not profileSuccess.continueLogin) {
						break;
						}
					}
				}
				*/

				checkValidUser = application.com.login.isPersonAValidUserOnTheCurrentSite(personID = CheckLogin.personID) ;
				if (checkValidUser.isValidUser){
					 application.com.login.externalUserLoginProcess (CheckLogin.personid);
					// NJH 2010/06/28 P-FNL069 if a user's password is about to expiry, then redirect them to the 'Change Password' page after having logged them in.
					// 2011-10-12 NYB LHID7848 added structkeyexists(CheckLogin,"passwordExpiryWarning") as this doesn't exist when using a magic number (tepid)
					if (structkeyexists(CheckLogin,"passwordExpiryWarning") and CheckLogin.passwordExpiryWarning) {
						form.urlRequested = "etid=resend_Password&spl=#application.com.emailmerge.generatePasswordLink(personID=request.relayCurrentUser.personID)#&pwe=1";
					}
				}
				else {
					// WAB 2005-09-19 although we haven't let this person into the site, we do know exactly who they are, so should update the relaycurrentuser etc. to reflect this
					application.com.login.initialiseAnExternalUser (CheckLogin.personid) ;
					if (len(checkValidUser.failETID)){
						form.urlRequested = "etid=#checkValidUser.failETID#";
					}
				}
			}
		</cfscript>

		<cfscript>
			// this sets up a cookie for tracking purposes.  The flagID is set up above if pBLFID is detected in any scope.
			if (isDefined("session.setThisPersonFlagOnLogin")) {
				listlength = listlen(session.setThisPersonFlagOnLogin);    // WAB 2005-09-19 error here - didn't have the listlen function - amazing that it ever worked!!
				for(i=1; i lte listlength; i=i+1) {
					application.com.flag.setBooleanFlagByID(entityID=checkLogin.personID,flagID=ListGetAt(session.setThisPersonFlagOnLogin,i));
				}
			}
		</cfscript>


		<!--- add entity tracking if a commid is specified --->
		<cfif isDefined("c")>
			<cfset application.com.entityFunctions.addEntityTracking(entityID=0,entityTypeID=0,personID=checkLogin.personId,goToUrl=request.currentSite.domainAndRoot,commID=c)>
			<!--- <CFQUERY NAME="addEntityTracking" datasource="#application.sitedatasource#">
			insert into entityTracking  (entityID, entityTypeID, Personid,toLoc, created, commid)
			select 0,0, <cf_queryparam value="#checkLogin.PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="Partner Portal: #request.currentSite.domainAndRoot#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#c#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY> --->
		</cfif>

		<!--- request.portalLoginMethod is used in relay/et.cfm. Options are process or EID
		The process called by default is process id 100.  To simply go to an eid after login --->
		<cfparam name="request.portalLoginMethod" default="etid">

		<cfscript>
			if(not structKeyExists(form, "urlRequested") or not len(form.urlRequested)){
				if(not compareNoCase(request.portalLoginMethod, "process")){
					urlString = 'proc.cfm?prid=100&stid=10';
				}
				else{
					urlString = '';
				}
			} else {
				/* 2012-09-06 PPB Case 430058 if not logged in and user hits a link to a secure file then we redirect to login and then load the file in a popup
				(used a popup otherwise it would fill the window making it natural for the user to close it and remain logged in unwittingly; we could easily pass a parameter in to override this and redirect to FileGet instead) */
				parametersStruct = application.com.structureFunctions.convertNameValuePairStringToStructure(inputstring=form.urlRequested,delimiter="&");
				if (StructKeyExists(parametersStruct,"displayfile") and parametersStruct.displayfile and StructKeyExists(parametersStruct,"fileId")) {
					urlString = "#application.com.security.encryptURL('fileManagement/FileGetInPopUp.cfm?fileID=#parametersStruct.fileId#&openInPopUp=true')#";
				}else if (form.urlRequested.startsWith("/")){
					//support form.urlRequested containing a direct path to a CFM rather than an eid page. Note that the autoredirect
					//doesn't happen on direct paths so this is to support when the redirect to the login page is explicitly triggered -RJT
					
					//because the remaining code applys the / itself we need to make sure we don't have one here as well -RJT
					
					urlString=RemoveChars(form.urlRequested, 1, 1);
				} else {
					urlString = "?" & form.urlRequested;
				}
			}

			if (structKeyExists(form, "partnerEntryPoint") and len(form.partnerEntryPoint)){
				urlString = urlString & '&' & form.partnerEntryPoint;
			}
		</cfscript>

		<cfoutput>

 		<!--- WAB 2008/03/25 changed this from an href relative to the root to a full http domain etc.  This allows us to switch back to http if the login was Https--->
	<!--- 2009/02/04 GCC - resolves issue for secure login not flicking back - https only sites should not have useSecureLogin = true - they should only have https access enabled at an IIS level  --->
	<cfif request.currentsite.useSecureLogin>
		<cfset redirectProtocol = "http://">
	<cfelse>
		<cfset redirectProtocol = request.currentSite.httpProtocol>
	</cfif>
		<SCRIPT type="text/javascript">
			window.location.href = '#jsStringFormat(redirectProtocol)##request.currentSite.domainAndRoot#/#jsStringFormat(variables.urlString)#';
		</script>
		</cfoutput>
		<CF_ABORT>  <!--- have to stop here because the page is being redirected --->

	<!--- if they were trying to authenticate after being remember me'd --->
	<cfelseif CheckLogin.RecordCount EQ 0 and request.relaycurrentuser.isLoggedIn eq 1>
		<cfset request.fault = true>
		<cfset messagePhrase = "loginscreen_InvalidUserNamePAsswordaa">
		<cfif structkeyexists(form,"urlRequested")>
			<cfif listlen(form.urlRequested,"=") eq 2>
				<cfset url.epl=LISTLAST(FORM.urlRequested,"=")>
			</cfif>
		</cfif>
		<cfset request.displayloginScreen =true>  <!--- wab just trying a method for getting a login screen --->
		<cfset eid = "System_HigherLevelLoginRequired"> <!--- request.currentsite.elementTreeDef.defaultPageLoggedOut --->

	<cfelse>
		<cfset request.fault = true>
		<cfset messagePhrase = "loginscreen_InvalidUserNamePAsswordbb">
		<cfset eid = request.currentsite.elementTreeDef.LoginPage>

	</cfif>
</cfif>

<!---Logout procedures--->
<CFIF structKeyExists(url, "logout")> <!--- WAB 2008/06/25 this condition no longer important AND ( isdefined("cookie.partnersession") or request. relayCurrentVersion.relaycurrentUser Version is 2) --->
	<!--- START: NYB 2009-07-14 P-FNL069 - replaced:
		<cfset application.com.relaycurrentuser.setLoggedOut()>
	with:
	WAB 2010/10/06 reinstated, removing user cookie now dealt within the logout function
	<CFIF isDefined("request.remove UserCookieOnLogout") and request.remove UserCookieOnLogout>
		<cfset application.com.relaycurrentuser.setLoggedOut(removeUserCookie="true")>
	<CFELSE>
		<cfset application.com.relaycurrentuser.setLoggedOut()>
	</CFIF>
	--->
	<!--- END: NYB 2009-07-14 P-FNL069 - --->

	<cfset application.com.relaycurrentuser.setLoggedOut()>



		<cfoutput>
		<SCRIPT type="text/javascript">
			window.location.href = '/';
		</script>
		</cfoutput>
	<CF_ABORT>  <!--- have to stop here because the page is being redirected --->
</cfif>

<!--- WAB 2006-06-13 trying to phase out etid so that I don't have to test for it and eid everywhere --->
<cfif isDefined("url.etid")>
	<cfset url.eid  = url.etid>
	<cfset structDelete (url,"etid")>
</cfif>

<!--- code for dealing with an unrestricted link to secure page (aka freebie hop) --->
<cfif isDefined("url.seid")>
	<cfset seidResult = application.com.relayelementTree.processSEIDValue(url.seid)>
	<cfif seidResult.linkIsValid>
		<cfif seidResult.personid is not request.relayCurrentUser.personid and seidResult.personid is not 0 and seidResult.personid is not application.unknownpersonid>
			<!--- if the user has changed and we now have their personid, we can use it, but not log them in --->
			<cfset x = application.com.login.initialiseAnExternalUser (seidResult.personid)><!--- initialises an external user but does not log them in --->
			<!--- wab 2006-05-23 - noticed bug
				unfortunately having initialised a new person, we lose the content setting which have just been done, so need to process the seid again
				probably some better way to do this!
			 --->
			<cfset application.com.relayelementTree.processSEIDValue(url.seid)>
		</cfif>
		<cfset url.eid = seidResult.elementid>
		<cfif seidResult.commid is not 0>
			<cfset cid = seidResult.commid>  <!--- for tracking against commid --->
			<!--- WAB 2009/06/15 these seid links were not being tracked in the commdetail table, note change to relayElementTree.cfc required as well --->
			<cfset url.t = seidResult.test>
			<CFModule TEMPLATE="/communicate/updateCommDetail.cfm" pid="#seidResult.personid#" cid="#seidResult.commid#" action="e">

		</cfif>
	</cfif>
</cfif>

	<!--- this bit taken from elementTemplate, by this stage I want to know what page we are going for so I can do the nagging --->
		<cfif isdefined("etid") and len(etid) gt 0>
 			<cfset EID = etid>
		<cfelseif isdefined("eid") and eid neq 0 and eid neq ''>
 			<cfset EID = eid>

		<!--- WAB 2007-01-15 test for an epl - if the user is already logged in the go straight to the epl --->
		<cfelseif isdefined("epl") and request.relaycurrentuser.isloggedin>
			<cfset EID = epl>
		<cfelseif isdefined("epl") and not request.relaycurrentuser.isloggedin>
			<cfset EID = request.currentsite.elementTreeDef.LOGINPAGE><!--- note use of LOGINPAGE which might be different to defaultPageLoggedOut--->

		<cfelseif request.relaycurrentuser.isloggedin>
			<cfset EID = request.currentsite.elementTreeDef.defaultPageLoggedIn>
		<cfelseif not request.relaycurrentuser.isloggedin>
			<cfset EID = request.currentsite.elementTreeDef.defaultPageLoggedOut>
		</cfif>

<cfsetting enablecfoutputonly="no" >

<!--- WAB testing nagging functions
<CFPARAM NAME="REQUEST.NAGGINGDONE" DEFAULT= FALSE>
<!--- <cfoutput>#eid#  <BR>
</cfoutput> --->--->
<cfif (	request.relaycurrentUser.isLoggedIn
		and request.currentsite.nagging.isOn
		and (not structKeyExists(session,"naggingDone") <!--- OR NOT REQUEST.NAGGINGDONE --->)
		and listfindnocase(request.currentsite.nagging.PageList,eid) is not 0)
		 >
	<!--- <cfset REQUEST.naggingdone = true> --->

	<cfset session.naggingdone = true>
	<cfset frmprocessid = request.currentsite.nagging.processid>
	<cfinclude template= "proc.cfm">
	<cfif not request.processAction.ContinueCallingTemplate>
		<CF_ABORT>
	</cfif>
<cfelseif false and isDefined("frmProcessid")>
	<cfinclude template= "proc.cfm">
	<cfif not request.processAction.ContinueCallingTemplate>
		<CF_ABORT>
	</cfif>
</cfif>

<CFINCLUDE TEMPLATE="elementTemplate.cfm">

