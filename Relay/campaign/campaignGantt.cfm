<!--- �Relayware. All Rights Reserved 2014 --->
<cfoutput>#layout.head(title="Project Gantt Chart")#</cfoutput>
<cfoutput>#htmleditformat(layout.body())#</cfoutput>
<!--- 
File name:			
Author:			SWJ  
Date started:	15-09-2007
	
Description:	Displays a campaign based on dates of the requirements		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfif structKeyExists(URL,"projectID")>
	<cfset form.projectID = URL.projectID>
	<!--- set session.projetID so that it is used in requirements screen --->
	<cfset session.projectID = URL.projectID>
	<cfset projectDetails = application.com.relayTimeSheets.getProjectData(projectID=session.projectID)>
</cfif>

<cfif isDefined("projectDetails")>
	<cfset pageTitleText = projectDetails.projectName>
<cfelse>
	<cfset pageTitleText = "Campaign Diary">
</cfif>

<cfoutput>
	#layout.topNavBar(showBackLink=true)#
	#layout.pageTitle(titleText=pageTitleText)#
</cfoutput>

<cfset getData = application.com.relayCampaigns.GetCampaigns()>

<!--- <cfdump var="#qryRequirementsList#"> --->

<cf_gantt showAssignedTo="yes" showDuration="yes" width="1000">
	<cfoutput query="getData">
		<!--- <cf_ganttgroup name="#requirementGroup#">  --->
		<!--- <cfoutput> --->
			<cf_ganttitem name="#campaignName#" href="/campaign/campaignHome.cfm?editor=yes&campaignID=#campaignID#" startdate="#startDate#" enddate="#endDate#">
		<!--- </cfoutput> --->
		<!--- </cf_ganttgroup> --->
	</cfoutput>
</cf_gantt>

<cfoutput>#htmleditformat(layout.endBody())#</cfoutput>

