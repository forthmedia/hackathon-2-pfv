<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			campaignHome.cfm
Author:				SWJ
Date started:		23-Jun-08

Description:		List and edit screen for campaigns

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-04-17   		SSS			Bug 2095 Added required to the end and start date or gantt chart generator falls over
2012-12-01			IH			Case 432353 pass numRowsPerPage to cf_listAndEditor
2016/10/14			NJH			JIRA PROD2016-2506 - ensure that the end date is greater than the start date.

Possible enhancements:


 --->

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="campaignName">
<cfparam name="numRowsPerPage" default="100">

<!--- save the content for the xml to define the editor --->
<!--- SSS 2009-04-17 added required to the end and start date Bug 2095 --->
<cf_includeJavascriptOnce template="/javascript/date.js">
<cf_includeJavascriptOnce template="/campaign/js/campaign.js" translate="true">

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="campaign" title="Campaign Editor">
			<field name="campaignID" label="phr_Campaign_ID" description="This records unique ID." control="html"></field>
			<field name="CampaignName" label="phr_Campaign_Name" description="This records name."></field>
			<field name="Objective" label="phr_Campaign_Objective" description=""></field>
			<field name="roi" label="phr_Campaign_Target_ROI" description=""></field>
			<field name="cost" label="phr_Campaign_Cost" description="" control="numeric">></field>
			<field name="startDate" label="phr_Campaign_Start_Date" description="" control="date" required="yes"></field>
			<field name="endDate" label="phr_Campaign_End_Date" description="" control="date" required="yes" submitFunction="isStartDateBeforeEndDate"></field>
			<field name="complete" label="phr_Campaign_Complete" description="" control="YorN"/>
			<field name="active" label="phr_Campaign_Active" description="" control="YorN"/>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(URL,"editor")>
	<cfset getData = application.com.relayCampaigns.GetCampaignListing(sortOrder=sortOrder)>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="campaignHome"
	keyColumnList="campaignName"
	keyColumnKeyList="campaignID"
	showTheseColumns="campaignName,StartDate,EndDate,Communications,Total_Opps,Total_Opp_Value,Total_Business,Complete,Cost,Active"

	dateFormat="StartDate,EndDate"
	booleanFormat="active"
	currencyFormat="Total_Opp_Value,Total_Business,Cost"

	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	numRowsPerPage="#numRowsPerPage#"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	columnTranslation="true"
>