function isStartDateBeforeEndDate() {
	var startDate = getTimeFromDateField(document.getElementById('startDate'));
	var endDate = getTimeFromDateField(document.getElementById('endDate'));

	if (startDate > endDate) {
		return phr.campaign_endDateMustBeGreaterThanStart;
	}

	return '';
}