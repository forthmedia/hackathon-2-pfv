<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes" >

<!--- this version of element template must use displayborder version 8 --->
<!--- <cfif request. relayCurrentVersion.displayBorderVersion lt 8>
	<cfset request. relayCurrentVersion.displayBorderVersion = 8>
</cfif> --->

<!---
File:			  Element Template version 2.01
Recent Authors:	  David McLean, SWJ, WAB
Original Author : SWJ
Date created:	  7th June 2002

Description:

Version history:
2001-03-08		WAB		Added some security. Checks for level of authentication required and validity of session
								Dependency - loginrequired field on the elements table
2001-03-12		WAB		Corrected error if lislen(cookie.user) not 3
2002-06-07		DAM		Added new security check. Procedure is fired to bring back list of
								elements that the user has rights to. If the user attempts to access
								an element that they have no rights to and they are not logged on, i.e. if they
								book mark a url with an element ID in it, they are bounced over to the login page.
								If however they have logged in, it checks to see whether an http_referer exists;
								if it does the assumption is that there is a link in the application pointing to
								this element that should not be visible to this user, if it does not then the
								assumption is that the user typed the ID directly into the url and may be malicious.
								In both cases an email is sent to the support desk.
			2002-07-13	SWJ		I copied code from FNLMarketing that:
								a) getsHomeSiblings
								b) getsElementTree
								c) sets personid
			2002-07-12	SWJ		I modified getElement so that you can pass the screen etid
			2002-09-27	SWJ		I added a few lines to check if cid is defined and if so set cookie.commid
			2003-01-17	SWJ		Added content margin control
			2003-02-04	SWJ		Added the params for eid and etid.  Modified the display border call to
								pass whatever eid and etid is set to displayBorder.  Modified
								the position of isContentEditor query to appear be after the
								frmPersonID logic.
2003-06-06	SWJ		Added logic to execute a method
2004-07-07	WAB 	Altered to use CF_translate and also altered the way the that relay tags are processed
			2004-11-16	WAB 	made frmpersonid into a form scoped variable so that it filters down into cf_execute relay tag
			2005-01-12		WAB 	found bug where in line <CFIF isDefined("request.relayCurrentUser.personid") and listLen(cookie.user,"-") gt 1 and isNumeric(request.relayCurrentUser.personid)>
								the list len was missing the delimter "-".  Didn't cause problem usually because usually a user was only a member of more than one user group so the listlen (with comma delimiter) was more than 1, but problem for a brand new user with no user groups
			2005-02-13	SWJ		Added method to display summary, longSummary and additional stylesheet for structured page layout
			2005-07-07	AJC		Added styling and positioning to subnav
			2005/09/14   WAB 	Change to how elementLangs is used
			2006		WAB  rewrote as elementTemplatev2  (Some of it never really finished, but went into production anyway!)
			2007-09-20  WAB   corrected bug when showing "you need to login" message
			2008-04-21  JvdW  added 404 Page Not Found server header for search engine spiders
2009/02/09  WAB added parameterStructure to request.CurrentElement
			2009/04/24	NJH		Bugs All Sites Issue 2118 - removed closing div in the relayHeadArea div with class of "relayHeaderArea-#relayHeaderImageID#"
2009/06/08 WAB 		P-TNDEMEA 090 If user needs to login then we need to propogate the querystring variables (other than things like eid which is already there and seid which is no longer needed) through the login process
			2010-10-18 PPB 		LEN022 LID4167 when login expires return to login page don't crash
			2012/03/07	NJH		CASE 426986: Moved the translating of the element detail after the display of the border, as it was getting the borders messed up if it included a border itself. Also removed a lot of commented out code in an effort to clean up the file.
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2014-02-19	WAB		Add textIDList to the request.currentElement structure (textIDlist recently added to the elementTree query)
2015-01-07	AHL		Case 442723 Email link redirects
2015/07/28	NJH			Add secure option for embedly
2016-01-19	WAB	Remove cfsetting showDebugOutput - now dealt with on a session level

	--->

<cfparam name="ContentMargin" default="0">
<cfparam name="ContentTopMargin" default="0">


<CFSET HRefClass="MenuHeading">

<!---
================================================================================
		This section
		1) Gets the element tree for the current site and user

		2) Looks to see what element is being requested in eid,etid etc.
			If no element being requested then choses a default page

		3) checks that that page is available for the current user . If not it does a loop to try again
================================================================================
--->

	<cfset getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID )>
	<cfset request.CurrentElementTree = getElements>
	<cfset request.CurrentElementTreeCacheID = application.com.relayElementTree.getElementTreeCacheIDForCurrentUser(topElementID= request.currentsite.elementTreeDef.TopID)>


	<cfif getElements.recordCount eq 0>
		<cfoutput>There are no elements being returned for
		Tree #request.currentsite.elementTreeDef.TopID#
		and frmPersonid = #request.relaycurrentuser.Content.showForPersonID# </cfoutput>

		<!--- WAB 2005-07-13 problems with code when lots of users accessing it - this in for debugging --->
		<cfset application.com.relayelementtree.incrementCachedContentVersionNumber()>
		<cfmail to="william@foundation-network.com" from="william@foundation-network.com" subject="URGENT PROBLEM ON SITE" type="html">
			No items returned for
			#request.currentsite.elementTreeDef.TopID#
			<cfdump var = "#request.currentsite#">
			<cfdump var = "#request.relayCurrentUser#">
		</cfmail>


		<cfexit method="EXITTAG">
	</cfif>

	<cfif isDefined("debug") and debug eq "yes" and isDefined("attributes.etid")>
		<cfoutput>ELEMENT TEMPLATE: etid = #htmleditformat(attributes.etid)#</cfoutput>
	</cfif>

	<!---
================================================================================
       3. Get the content for this element
================================================================================
--->

	<!--- what element are we searching for? - various different ways of passing it in --->
		<cfif isdefined("etid") and len(etid) gt 0>
 			<cfset thisEID = etid>
		<cfelseif isdefined("eid") and eid neq 0 and eid neq ''>
 			<cfset thisEID = eid>
		<cfelseif request.relaycurrentuser.isloggedin>
			<cfset thisEID = request.currentsite.elementTreeDef.defaultPageLoggedIn>
		<cfelseif not request.relaycurrentuser.isloggedin>
			<cfset thisEID = request.currentsite.elementTreeDef.defaultPageLoggedOut>
		</cfif>

		<!--- loop around until we find a element which we can display. max 3 times (actually lp gt2 is caught within the loop and cfaborted--->
		<cfset lp = 0>
		<cfloop condition = "lp lte 3">
				<cfset lp = lp + 1>
				<cfquery name="getElement" dbtype="query">
					SELECT
						*,
						node as id,
						parent as parentid
					FROM
						getElements
					WHERE
					<cfif isNumeric(thisEID) >
						node = #thisEID#
					<cfelse>
						upper(elementTextID) = '#ucase(thisEID)#'
					</cfif>
				</cfquery>

				<cfif getElement.recordcount neq 0
						and
					(request.relaycurrentuser.isLoggedIn gte getElement.loginrequired or getElement.loginrequired is not 2 )
				>   <!--- strange if statement has to handle a freebie hop to this page  - user might not be logged in, but is allowed to see a loginrequired page , so we can't just do a test request.relaycurrentuser.isLoggedIn gte getElement.loginrequired. Might be a better way of doing this - because this won't allow previewing of a page with loginrequired set to 2 --->

						<!--- this element is OK --->
						<cfbreak>

				<cfelse>

					<cfset failureInfo = application.com.relayElementTree.whyCantCurrentUserSeeThisElement(topElementID=request.currentsite.elementTreeDef.TopID,elementid=thisEID)>

					<!--- <cfoutput>#thisEID#</cfoutput>
					<cfdump var = "#failureInfo#"> --->
					<cfset redirectNow = false>
					<cfif failureInfo.reason is "LoginRequired" >
						<!--- need to login so bring up the default login page with set epl (elementPostLogin).  Note that for this to work, the login page has to look for epl and pass it on (usually as urlrequested) --->--->
						<!--- how about some sort of message? --->
						<cfif request.currentsite.hasLoginInBorder>
								<!--- WAB 2006-09-11 going to try creating a dummy request.currentelement structure to fool any code in the borders which references it
									has most of the fields of the real version!
								--->
									<cfset request.CurrentElement = application.com.structureFunctions.queryRowToStruct(getElement,1)>
									<cfset request.CurrentElement.parentidlist = "0">
									<cfset request.CurrentElement.textIDList = "">
									<cfset request.CurrentElement.parentid = "0">
									<cfset request.CurrentElement.node = "0">
									<cfset request.CurrentElement.translations = structNew()>
									<cfset request.CurrentElement.translations.headline = "">
									<cfset request.CurrentElement.translations.summary = "">
							<cfset url.epl = thisEID>
							<cfset request.displayloginScreen =true>  <!--- wab just trying a method for getting a login screen --->
									<cf_translate>
									<CF_DisplayBorder elementtree = #request.CurrentElementTree#>
										<cfoutput>phr_sys_PleaseLoginToViewPage
										<!-- Please login to view this page -->
										</cfoutput>
									</CF_DisplayBorder>
									</cf_translate>
							<CF_ABORT>

						<cfelse>
							<cfset redirectNow = true>
							<cfset redirectToMessage = "phr_sys_Redirect_MustBeLoggedIn">
							<cfset redirectToQueryString = "eid=" & request.currentsite.elementTreeDef.loginPage>	<!--- 2015-01-07 AHL Case 442723 Email link redirects --->
							<!--- WAB 2009-06-08 LID   P-TNDEMEA 090 need to propagate URL parameters in to the next page. I get rid of ones we know aren't wanted. Added to both the urlrequested variable and to the end of the query string as well.  It actually seems to be the later that does the job--->
							<cfset partQueryString = application.com.regexp.removeItemsFromNameValuePairString(inputString=request.query_string,itemstodelete="eid,etid,seid,urlrequested",delimiter="&")>
							<cfset urlRequested="eid=#thisEID#&#partQueryString#">
							<cfset redirectToQueryString = redirectToQueryString & "&urlrequested=#urlencodedFormat(urlRequested)#">
							<!--- WAB 2010-11-16, LID4473? adding everything to the end of the query string causes problems in some cases, especially if there is a formScope variable, because values in this (esp. eid) override the eid on the query string.
								I am going to try removing the formState variable before tacking the extra params on to the end of the url
							--->
							<cfset partQueryString = application.com.regexp.removeItemsFromNameValuePairString(inputString=partQueryString,itemstodelete="formState",delimiter="&")>
							<cfset redirectToQueryString = redirectToQueryString & "&epl=#thisEID#&#partQueryString#">
							
							<!--- The SSO may want to override the redirect behaviour --->
							
							<cfset loginBehaviour=application.com.singleSignOnSiteControl.getSiteSSOBehaviour(request.currentSite.siteDefID)>
							
							<cfif loginBehaviour.defaultAllowSingleSignOn and loginBehaviour.autostartSingleSignOn>
								<!---Probably autostarting the SSO, but only if there is only 1 IDP--->
								<cfset allSamlIDPs = application.com.singleSignOnSiteControl.getValidIdentityProvidersForASite()>
								<cfif allSamlIDPs.recordCount EQ 1>
									<!---We're autostarting the SSO--->
									<cfsavecontent variable = "ssoAutoStartHTML"> 
										<CF_SAMLAuthnRequestLaunchPost SAMLIdentityProviderID="#allSamlIDPs.SAMLIdentityProviderID#" launchButtonText="Sign in with #htmleditFormat(allSamlIDPs.identityProviderName)#" autolaunch="true" relayState="#urlRequested#" autoLaunchDelay="#application.com.settings.getSetting('portal.redirectWaitTime')#">
									</cfsavecontent>
									
								</cfif>
								
							</cfif>
							
						</cfif>

					<cfelseif failureInfo.reason is "LoginRequiredHigherLevel" >
							<cfset url.urlrequested="eid=#thisEID#">
							<cfset url.epl=thisEID>
							<cfset request.displayloginScreen =true>  <!--- wab just trying a method for getting a login screen --->
							<cfset thisEID = "System_HigherLevelLoginRequired"> <!--- request.currentsite.elementTreeDef.defaultPageLoggedOut --->

					<cfelseif request.relayCurrentUser.isLoggedIn and thisEID is not request.currentsite.elementTreeDef.defaultPageLoggedIn>  <!--- ie they are logged in and we haven't already tried the logged in home page --->
						<cfset redirectNow = true>
						<cfset redirectToQueryString = "eid=" & request.currentsite.elementTreeDef.defaultPageLoggedIn>
						<cfset redirectToMessage = "phr_sys_ThisPageisnotavailable">

					<cfelse>
						<cfset redirectNow = true>
						<cfset redirectToMessage = "phr_sys_ThisPageisnotavailable">
						<cfset redirectToQueryString = "eid=" & request.currentsite.elementTreeDef.defaultPageLoggedOut>
						<!--- WAB 2009-06-08 LID   P-TNDEMEA 090 need to propagate URL parameters in to the next page. I get rid of ones we know aren't wanted. Added to both the urlrequested variable and to the end of the query string as well.  It actually seems to be the later that does the job--->
						<cfset partQueryString = application.com.regexp.removeItemsFromNameValuePairString(inputString=request.query_string,itemstodelete="eid,etid,seid,urlrequested",delimiter="&")>
						<cfset redirectToQueryString = redirectToQueryString & "&urlrequested=#urlencodedFormat("eid=#thisEID#&#partQueryString#")#">
						<cfset redirectToQueryString = redirectToQueryString & "&epl=#thisEID#&#partQueryString#">

					</cfif>

							<!---
							<cftry>
								 <cfquery name = "recordError"  datasource = "#application.sitedatasource#">
								 Insert into WABElementError
									 (elementID,elementTextID,topelementID,personid,created,error,queryString,countryid,referrer)
								 values (<cfif isNumeric(thisEID)>#thisEID#<cfelse>null</cfif>,<cfif not isNumeric(thisEID)>'#left(thisEID,100)#'<cfelse>null</cfif>,#request.currentsite.elementTreeDef.topID#,#request.relaycurrentuser.personid#,#now()#,'#failureInfo.reason#','<cfif isdefined("request.query_string")>#left(request.query_string,100)#</cfif>',#request.relaycurrentuser.countryid#,'#left(cgi.HTTP_REFERER,100)#')
								 </cfquery>
								<cfcatch>

									<cfif cfcatch.detail contains "Invalid object Name">
									<cfelse>
										<cfrethrow>
									</cfif>
								</cfcatch>
							</cftry>
							--->

							<cfif redirectNow >
								<!--- NJH 2011-10-06 LID 7927 - moved into inside if statement, as only defined if we are redirecting --->
								<cfset redirectToQueryString = application.com.security.makeQueryStringSafe(redirectToQueryString)>

								<!--- WAB 2006-09-11 going to try creating a dummy request.currentelement structure to fool any code in the borders which references it
									has most of the fields of the real version!
								--->
									<cfset request.CurrentElement = application.com.structureFunctions.queryRowToStruct(getElement,1)>
									<cfset request.CurrentElement.parentidlist = "0">
									<cfset request.CurrentElement.textIDList = "">
									<cfset request.CurrentElement.parentid = "0">
									<cfset request.CurrentElement.node = "0">
									<cfset request.CurrentElement.translations = structNew()>
									<cfset request.CurrentElement.translations.headline = "">
									<cfset request.CurrentElement.translations.summary = "">
									<cfset request.currentElement.embedly = 0>

					                <!--- WAB, thought it would be good idea to return a status code
					                but turns out not to be.  We end up in a bit of a loop with IIS trying to access our missing template handler - which isn't what I was wanting
					                <cfheader statuscode="404" statustext="Not Found">
					                --->
									<!--- 2008-01-02 GCC skip redirect for Sony web service content calls for  'bad' eids--->
									<cfparam name="request.elementNotFoundRedirect" default="true">
									<cfif request.elementNotFoundRedirect>

										<cfset redirectWaitTime=application.com.settings.getSetting("portal.redirectWaitTime")>

										<cf_Translate>
											<CF_DisplayBorder elementtree = #request.CurrentElementTree#>
												<cfoutput>
													<cfif isDefined("ssoAutoStartHTML")>
														#htmleditformat(redirectToMessage)#<BR> phr_sys_Youarebeing phr_sys_redirected
														#ssoAutoStartHTML#
													<cfelse>
														<cfif redirectWaitTime EQ 0>
															<cflocation
															    url = "#request.currentSite.protocolAndDomain#/?#redirectToQueryString#"
															    addToken = "no"
															>
														<cfelse>
															<META HTTP-EQUIV="Refresh" content="#redirectWaitTime#; URL=#request.currentSite.protocolAndDomain#/?#htmleditformat(redirectToQueryString)#">
															<p>#htmleditformat(redirectToMessage)#<BR> phr_sys_Youarebeing <A HREF="/?#redirectToQueryString#">phr_sys_redirected </a></p>
														</cfif>
													</cfif>
												</cfoutput>
											</CF_DisplayBorder>
										</cf_Translate>
									<cfelse>

										<cfoutput><cf_Translate>phr_sys_elementNotFoundRedirect</cf_Translate></cfoutput>
									</cfif>
									<CF_ABORT>


							</cfif>
							<cfif lp gte 2>

								<cfmail to="william@foundation-network.com" from="william@foundation-network.com" subject="#request.relaycurrentUser.sitedomain# - User can't see home page" type="html">
									<cfif thisEID is request.currentsite.elementTreeDef.defaultPageLoggedIn>
										The following person has logged on to #request.relaycurrentUser.sitedomain# but does not appear to have rights to the the default internal page
										<cfif request.relaycurrentUser.sitedomain contains "sonyrewards">
										<BR>On the Sony Rewards site this means that the user doesn't have an account/ isn't flagged as an incentive user
										</cfif>

									<cfelseif thisEID is request.currentsite.elementTreeDef.defaultPageLoggedOut>
										The following person has arrived at #request.relaycurrentUser.sitedomain# but does not appear to have rights to the the default external page
									<cfelse>
										The following person has arrived at #request.relaycurrentUser.sitedomain# but does not appear to have rights to the the default external page
									</cfif>

									script: #cgi.script_name#  <BR>
									CGI.queryString = #request.query_string#<BR>
									#request.relaycurrentuser.personid# 	<BR>
									#request.relaycurrentuser.fullname#, #request.relaycurrentuser.location.sitename#<BR>
									ThisEID: #thisEID# <BR>
									TreeTop: #request.currentsite.elementTreeDef.topID# <BR>
 									Referrer: #cgi.HTTP_REFERER# <BR>
									Language: #request.relaycurrentuser.languageid# #request.relaycurrentuser.languageisocode#
									<cfdump var="#request.relaycurrentuser.person#">
									<cfdump var="#request.relaycurrentuser.content#">

								</cfmail>

									<cfoutput>
									<font face="Arial">
									<p>This page is not available</p>
									</font>
									<!--  #htmleditformat(failureInfo.reason)# -->
									</cfoutput>
									<CF_ABORT>

							</cfif>


				</cfif>

		</cfloop>

		<!--- WAB 2006-03-13
			although we have got the element details from the cache (and so proved that the user has rights to it)
			we are now going to get the element details direct from the database
			this allows us to reduce the frequency of blowing the cache, safe in the knowledge that the latest parameters etc will be loaded for the current page
		 --->

		<cfquery name="getElementFromDB" datasource="#application.sitedatasource#" >
		SELECT
			id as node,
			case when isNUll(getContentFromID,0) = 0 then id else getContentFromID end  as contentID,
			#getElement.parentid# as parentid, 		<!---  in case the parent within the tree isn't the same as parentid on the table (only if field getChildrenfromparentid is being used --->
			'#getElement.parentidlist#'  as parentIDList,
			'#getElement.sectioneid#'  as sectioneid,
			'#getElement.textIDList#' as textIDList,
			#GetElement.generation# as generation,
			'#getElement.inheritedborderset#' as inheritedborderset,
			'#getElement.resolvedHref#' as resolvedHref,
			'#getElement.resolvedTarget#' as resolvedTarget,
			*    <!--- note that * is put at the end - this means that parentID aliased above overrides the parentid which comes from * --->
		from Element
		where id =  <cf_queryparam value="#getElement.id#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>


			<cfset caller.getelement = getElementFromDB>
			<cfset caller.eid = getElementFromDB.id>
			<cfset eid = getElementFromDB.id>
			<cfset request.CurrentElement = application.com.structureFunctions.queryRowToStruct(getElementFromDB,1)>
			<cfset request.CurrentElement.parameterStructure = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = request.CurrentElement.parameters,delimiter = ",") >

			<CFSET personElementList = valuelist(getElements.node)>





<CFSET form.frmPersonid = request.relaycurrentuser.personid> <!--- WAB added to make accessible to customtags in particular templates below cf_execute relaytag --->

<!--- 	*************************************************************
		5. Check to see if user is a contentEditor
		*********************************************************--->

<!--- 2014-08-22	RPW		Edit This Page link does nothing & no Control Content --->
<cfscript>
	variables.isContentEditor = application.com.login.checkInternalPermissions(securityTask="ElementTask", securityLevel="LEVEL3");
</cfscript>

<cfif variables.isContentEditor and application.com.rights.Element(getelement.id,2).hasrights>
	<cfset OKtoEdit = true>
<cfelse>
	<cfset OKtoEdit = false>
</cfif>

<CFSET elementid = request.CurrentElement.id>
<CFSET parameterStructure = request.CurrentElement.parameterStructure>
<CF_EvaluateNameValuePair
	NameValuePairs = #request.CurrentElement.parameters#
>
			<!--- WAB 2006-03-08 added translation of all the phrases associated with the currentElement
				also made request.CurrentElement a structure
				note that the translations (except the headline) can come from a different element
			--->
			<cfset parameterStructure.ElementID= ElementID>
			<cfset parameterStructure.relayTag.ElementID= ElementID>
			<!--- CASE 426986 NJH 2012/03/07 - exclude the translation of detail here.. translate below.  These are translated here as they are referenced in the border sets --->
			<cfset translations = application.com.relayTranslations.getAllEntityPhrases(defaultPhraseList="summary,longSummary",entityType="element",entityID=getElementFromDB.contentid,evaluateVariables=true,mergeStruct=parameterStructure,excludePhraseList="detail")>
			<cfset headlinetranslation = application.com.relayTranslations.getEntityPhrases(PhraseList="headline",entityType="element",entityID=getElementFromDB.id,evaluateVariables=true,mergeStruct=parameterStructure)>
			<cfset request.CurrentElement.Translations = translations[getElementFromDB.contentid]>
			<cfset request.CurrentElement.Translations.headline = headlinetranslation[getElementFromDB.id].headline>



	<!--- WAB 2006-03-21
		we don't really handle what happens if someone choses an eid which is actually linked to an external file, or some other file
		This shouldn't happen often because the navigation links should all be correct, but I'm going to see what can be done
		will need more development, but may not be important
	 --->
 	<cfif request.CurrentElement.isExternalFile >
			<cfif left(request.CurrentElement.resolvedhref,4) is "http" and request.CurrentElement.resolvedTarget is not "" and request.CurrentElement.resolvedTarget is not "_self" >
				<cfoutput>
						<cf_includejavascriptonce template="/javascript/openwin.js">
						<script>
						openWin ('#jsStringFormat(request.CurrentElement.resolvedhref)#','#jsStringFormat(request.CurrentElement.id)#')
						</script>
				</cfoutput>
				<CF_ABORT>
			</cfif>

	</cfif>


<cfscript>

	if (isdefined("elementLangs") and isdefined("cgi.HTTP_ACCEPT_LANGUAGE") and cgi.HTTP_ACCEPT_LANGUAGE neq ""){
		if (listLen(elementLangs,"-") gt 0) {
		elementLangList = "";
		elementLangList = replace(elementLangs,"-",",","ALL");
		// WAB 2005-09-14 this code was overriding settings which had already been made
		// made a change so that code only runs if the current language setting isn't in the elementlanglist

		if (not listfindnocase(elementLangList,request.relaycurrentUser.languageisocode)) {

			// GCC 2005-08-11 User chosen language - don't set to best fit browser language
				if ((structkeyexists(session,"userChosenLanguage") and session.userChosenLanguage eq 1) eq "false") {

					browserLanguage = application.com.relayTranslations.getUsersPreferredLangFromBrowser(elementLangList);
					langReturnVar = application.com.relayTranslations.setLanguageVarsFromISOCode(browserLanguage);
				}

		}

		}
		else
		{
			WriteOutput("ElementLangs is not defined correctly.   <hr>");
			WriteOutput("It should take the form elementLangs=1-2-3-4 with a minus sign delimiter");
			application.com.globalFunctions.abortIt();
		}
	}
</cfscript>

<!--- set a language cookie when this template is called directly  --->
<CFIF isDefined("frmShowLanguagElementID")>
	<cfset application.com.globalFunctions.cfcookie(name="DefaultLanguagElementID", value="#frmShowLanguagElementID#")>
</CFIF>

<cfif getElement.recordCount is not 0>

	<!--- note sure whether this is required, but left in WAB April 2006 --->
	<CFSET frmpersonid=request.relaycurrentuser.personid>



<CFSET elementHeadline = "phr_headline_element_#GetElement.node#">

<CFSET elementSummary = request.currentElement.translations.summary>
<CFSET elementLongSummary = request.currentElement.translations.LongSummary>
<CFSET elementMenuText = "phr_headline_element_#GetElement.contentid#">
<CFSET elementParentMenuText = "phr_headline_element_#GetElement.parentid#">


<CFIF OKtoEdit>
	<cf_includejavascriptonce template = "/javascript/doTranslation.js">
	<cf_includejavascriptonce template = "/javascript/externalElementEditing.js">
</cfif>

<cfset embedlyAPI = application.com.settings.getSetting("plugins.embedly")>

<!--- NJH 2013/01/21 - don't apply embedly to any links with a class of 'noembedly' - it is case sensitive
	set links that are inside elements with a class of 'noembedly' to have a class of noembedly...
	--->
<cf_includejavascriptonce template="/javascript/linkTracker.js"> <!--- NJH 2013/11/04 Case 437772 --->
<cfif embedlyAPI.enable>
	<cf_includejavascriptonce template="/javascript/lib/embedly/jquery.embedly.min.js">
</cfif>

<cf_head>
	<cfoutput>
		<script>
		jQuery('document').ready(function(){

			<cfif embedlyAPI.enable>
			jQuery('.noembedly').find('a:not(.embedly)').addClass('noembedly');
			jQuery("a[track='true']").addClass('noembedly'); <!--- if the links that we want to track are 'embedlied', then we lose the 'track' attribute --->

			jQuery('##embedContent a:not(.noembedly)').embedly({
				maxWidth: #embedlyAPI.maxWidth#,
				method: '#embedlyAPI.method#',
				key: '#embedlyAPI.Key#',
				secure: #request.currentSite.isSecure?true:false#
			});
			</cfif>

			<!--- NJH 2013/11/04 Case 437772 - add a listener on all links that have a track=true attribute --->
			jQuery("a[track='true']").click(function() {
				onClickLink('element',#request.currentElement.id#,#request.relayCurrentUser.personID#,this.href);
			});
		});
		</script>
	</cfoutput>
</cf_head>



<cf_translate >

<CF_DisplayBorder Element = #request.CurrentElement# elementtree = #request.CurrentElementTree#>

	<!--- 2009/02/20 NJH LID 1855 moved after the displayborder call to ensure these styles are loaded after the default site styles--->
			<!--- SWJ: 2005-02-13 Added method to display summary, longSummary and additional stylesheet for structured page layout --->
			<cfif len(getElement.stylesheets) gt 0>
				<!--- SWJ: 2005-06-20 Added to allow us to optionaly add a version number to the css --->
				<cfif isDefined("request.pageTemplateStyleSheetVersion")>
					<cfset pageTemplateStyleSheetVersion = "V" & request.pageTemplateStyleSheetVersion>
				<cfelse>
					<cfset pageTemplateStyleSheetVersion = "">
				</cfif>
				<cfhtmlhead text = "
				<style type='text/css' >
				<!--
				@import url(/code/styles/#getElement.stylesheets##pageTemplateStyleSheetVersion#.css);
				-->
				</style>
				">
			</cfif>

	<cfset DisplayContentTableBorder ="0">

	<!--- CASE 426986 - NJH 2012/03/07 moved the translation of element detail after the display of the border set... --->
	<!--- 2016-05-03 WAB During BF-673 The option to not translate phrases does not work anywhere within the detail of a page.  Fixed by explicitly setting TranslatePhrasesWithinPhrases to !request.relayCurrentUser.content.doNotTranslate --->
	<cfset elementDetailStruct = application.com.relayTranslations.getAllEntityPhrases(defaultPhraseList="detail",entityType="element",entityID=getElementFromDB.contentid,evaluateVariables=true,mergeStruct=parameterStructure, TranslatePhrasesWithinPhrases = !request.relayCurrentUser.content.doNotTranslate)>
	<cfset elementDetail = elementDetailStruct[getElementFromDB.contentid].detail> <!---2012-05-09 GCC changed to contentid to preserve show content from id functionality --->

		<cfloop query="getElement" startrow="1" endrow="1"> <!--- WAB 2006-02-21 added rows bit - prevents crashing if for some reason getElement has two records (if there are two elements with same elementtextid --->

			<CFSET elementTitle = elementSummary>
			<CFSET elementRighthandSide = #image#>

			<CFIF elementDetail NEQ "">


							<!--- GCC 2005/11/16 - request.showBreadcrumbLoggedIn = true in relayini defaults the beadcrumb on - can override with showbreadcrumb = "no" --->
							<cfif not isDefined("showbreadcrumb")>
								<cfif request.currentsite.elementTreeDef.showBreadcrumbLoggedIn is true
									and request.relaycurrentuser.isloggedin
									and showOnMenu >
									<cfset showbreadcrumb = "yes">
								</cfif>
							</cfif>


							<!--- this will display a breadcrumb based on the element tree --->
							<cfparam name="showbreadcrumb" default="no">

			<cfif showbreadcrumb >
				<cfmodule template="/relayTagTemplates/elementBreadcrumb.cfm">
			</cfif>
			<!--- end of breadcrumb --->

							<cfif getElement.showSummary eq 1>
								<cfoutput>
								<cfparam name="request.relayHeaderAreaTemplateID" default="1">
								<cfparam name="relayHeaderImageID" default="1">
								<cfif request.relayHeaderAreaTemplateID eq 2>
									<div id="relayHeaderArea">
										<div class="relayHeaderArea-#relayHeaderImageID#">
											<!--- <div class="relayHeaderText"/></div> --->
										</div>
										<div id="relayHeaderAreaHeader">
											<h1>#htmleditformat(elementHeadline)#</h1>
										</div>
										<div id="relayHeaderAreaContent">
											<p>#htmleditformat(elementSummary)#</p>
										</div>
									</div>
								<cfelse>
									<div id="relayHeaderArea">
										<div class="relayHeaderArea-#relayHeaderImageID#">
											<!--- <div class="relayHeaderText"/></div> --->
											<h1>#htmleditformat(elementHeadline)#</h1>
											<p>#htmleditformat(elementSummary)#</p>
										</div>
									</div>
								</cfif>
								</cfoutput>
							</cfif>

							<cfparam name="showInPageSubNav" type="boolean" default="false">

							<cfif showInPageSubNav>


							<!---  this code gets the navigation to the current element and 1 generation of children--->
									<!--- work out where to start the navigation from - basically from item which is on the left nav
											this always seems to be generation 3 - but may need to be parameterised
									--->
									<cfset getTopOfTree = application.com.relayElementTree.getElementOfParticularGenerationFromCurrentBranch(
													elementTree = request.currentElementTree,
													currentelementid = elementid,
													generation = 3)>


										<!--- now get the branch from this point to the current element and its children --->
										<cfset navigationbranch = application.com.relayElementTree.convertTReeToMenuBranch(
													elementTree = request.currentElementTree,
													topOfBranchID = getTopOfTree,
													currentelementid = elementid,
													showCurrentElementChildren = true ,
													includeTopElement = true,
													numberofgenerations = 1,
													numberofgenerationsofchildren =1)>

										<cfoutput>
										<!--- output, this could be done as a function --->
										<div id="wabnav">
											<div id="wabnavheader">phr_ElementTemplate_NavHeader</div>
											<cfset currentgeneration = 1><!---  style="float:left;display:block;position:absolute;left:200px;top:400px;padding:1px;" --->
											<ul id="wabnav_gen1">
											<cfloop query = "navigationbranch">
												<cfif generation is not currentgeneration>
												</ul><ul id="wabnav_gen#htmleditformat(generation)#">
												</cfif>
													<li <cfif node is Elementid >class="wabnavselected"</cfif> ><A HREF="/?eid=#node#">#htmleditformat(headline)#</A>  </li>
												<cfset currentgeneration =generation >
											</cfloop>
											</ul>
										</div>

										</cfoutput>
							</cfif>
							<cfif getElement.showLongSummary eq 1>
								<cfoutput>
								<div class="relayLongSummary"/>
									<p>#elementLongSummary#</p>
								</div>
								</Cfoutput>
							</cfif>


		<!--- note that no "page under construction at the moment --->
								<cfoutput>#elementDetail#</cfoutput>

			<cfelse><!--- there's no text yet --->

			</CFIF>

		</CFLOOP>

	<!-- End: Main content Table --->

	<cfinclude template="/elements/addTrackingRecord.cfm">
	</CF_DisplayBorder>
	</cf_translate>
<cfelse>
	<cfinclude template="/templates/underConstruction.cfm">
</cfif>

<cfsetting enablecfoutputonly="no" >


