<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			wsRelaywsLink.cfm	
Author:				AJC
Date started:		2007-10-14
	
Description:		Relay webservices V2
					- Stripped out Sony Specific Functions and Variables
					- Added function to create the receivedParameters array from the passed arguments
					- Added the isPersonAValidUserOnTheCurrentSite to the login 
					- Added Arguments - headeronly and trackUsage	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

 --->
<!--- initialise locale variables --->
<cfparam name="variables.personID" default="">
<cfparam name="url.loadURL" default="">
<cfparam name="variables.locale" default="">
<cfparam name="variables.etid" default="">
<cfparam name="variables.datetime" default="">

<!--- Include INI file or set defaults --->
<cfif fileexists("#application.paths.code#\cftemplates\relaywsINI.cfm")>
	<cfinclude template="/code/cftemplates/relaywsINI.cfm">
<cfelse>
	<cfscript>
		request.ws_URL = "#CGI.SERVER_NAME#";
		request.wsPassPhrase = application.com.settings.getsetting('Security.Encryptionmethods.Rwsso.Passkey');
		request.ws_Protocal = "http://";
		request.ws_URLExpiry = "10";
		request.ws_link_checkValidUser = "false";
	</cfscript>
</cfif>

<!--- If the encrypted string has been passed --->
<cfif isdefined("url.estring")>
	<!--- decrypted the string --->
	<cfset decryptedString = application.com.encryption.DecryptStringAES(StringToDecrypt=replace(URLDecode(url.estring),"||","+","ALL"),PassPhrase=request.wsPassPhrase)>
	<cftry>
		<!--- Get and set all of the url variables from the encrypted string --->
		<cfloop index="i" list="#decryptedString#" delimiters="&">
			<cfset "variables.#listfirst(i,"=")#" = "#listlast(i,"=")#">
		</cfloop>
		<cfcatch></cfcatch>
	</cftry>
</cfif>

<!--- Check all the required variables have been passed in the encrypted string --->
<cfif variables.personID neq "" and url.loadURL neq "" and variables.datetime neq "">
	<!--- Check the encrypted string has not expired (10 seconds as default) --->
	<cfif datetime gt createdatetime(year(now()),month(now()),day(now()),hour(now()),minute(now()),second(now()))>
		<cfif variables.locale neq "">
			<cftry>
				<cfset countryCode=listgetat(locale,2,"_")>
				<cfset languageCode=listgetat(locale,1,"_")>
				<cfcatch>
					<cfset countryCode="xxxxx">
					<cfset languageCode="xxxxx">
				</cfcatch>
			</cftry>
	 	
			<cfquery name="qry_get_country" datasource="#application.sitedatasource#">
				select countryID from country where isocode =  <cf_queryparam value="#countrycode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
		
			<cfquery name="qry_get_language" datasource="#application.sitedatasource#">
				select languageID from language where isocode =  <cf_queryparam value="#languagecode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
  		</cfif>
	
	
		<cfquery name="userDetail" datasource="#application.sitedatasource#">
			select *
			from person
			where personID =  <cf_queryparam value="#variables.personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
  
		<cfscript>
			application.com.relayCurrentUser.refresh();
			if ((variables.locale eq "") or (qry_get_country.recordcount neq 0 and qry_get_language.recordcount neq 0))
			{
				application.com.relaycurrentuser.setshowcontentforcountryID(countryID=qry_get_country.countryID);
				application.com.relaycurrentuser.setlanguage(language=languageCode);
			}
		</cfscript>
		
		<cfif request.ws_link_checkValidUser>
		
			<cfif userDetail.recordCount gt 0>
				<cfset userCheck = application.com.login.isPersonAValidUserOnTheCurrentSite(personID=userDetail.personID)>
				<cfif isdefined("userCheck.failETID") and userCheck.failETID neq "">
					<cflocation url="#url.loadURL#?etid=#userCheck.failETID#"addToken="false">
				</cfif>
			</cfif>
		</cfif>
		<cfscript>
			application.com.login.externalUserLoginProcess(userDetail.PersonID);
			
			getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID );
			debugInfo.languageid = request.relaycurrentuser.languageid ;
			debugInfo.countryid = request.relaycurrentuser.Content.ShowForCountryID ;
			debugInfo.elements = valuelist(getelements.node);
		</cfscript>
		<cfif isDefined("tagArguments") and listLen(tagArguments) gt 0>
			<cfloop list="#request.query_string#" index="valuePair" delimiters="&">
 				<cfif listLen(valuePair,"=") eq 2>
 					<cfset "variables.#listFirst(valuePair,'=')#" = "#listLast(valuePair,'=')#">
 				</cfif>
		 	</cfloop>
		</cfif>
		
		<cflocation url="#url.loadURL#?etid=#variables.etid#"addToken="false">
	<cfelse>
		<!--- If the encrypted string has expired the send to default home --->
		<cflocation url="/"addToken="false">
	</cfif>
<cfelse>
	You may not enter this zone!
</cfif>
