<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Alex Connell
Date created:	2006-09-14

	This provides the extended mxAjax functionality for Claims

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
--->


<cfcomponent extends="relay.com.mxajax">

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the products.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSelectProducts" returntype="array" hint="Returns a set of data for the claims form">
		<cfargument name="productgroupID" type="numeric" default="0">
	
		<cfset var qry_get_SelectProducts = "">
		<cfset productsArray = ArrayNew(1)>
	
		<CFQUERY NAME="qry_get_SelectProducts" DATASOURCE="#application.sitedatasource#">
			SELECT productid,description from product 
			WHERE productgroupID=#productgroupID# and deleted=0
		</CFQUERY>
		<cfset ArrayAppend(productsArray, " , ")>
		<cfloop query="qry_get_SelectProducts">
			<cfset ArrayAppend(productsArray, "#productid#,#description#")>
		</cfloop>
	
		<cfreturn productsArray>
	
	</cffunction>
	
	<cffunction name="getSelectproductgroups" returntype="array" hint="Returns a set of data for the claims form">
		<cfargument name="productserialid" type="string" default="">
		
		<cfset var qry_get_ProductGroups = "">
		<cfset productgroupArray = ArrayNew(1)>
		
		<CFQUERY NAME="qry_get_ProductGroups" DATASOURCE="#application.sitedatasource#">
				SELECT productgroup.productgroupid,productgroup.description, productgroup.productgroupname from productgroup
				INNER JOIN productgroupcategory on productgroup.productgroupid=productgroupcategory.productgroupid
				INNER JOIN productcategory on productgroupcategory.productcategoryid=productcategory.productcategoryid
				where productcategoryname IN 
				(select PC.productcategoryname from 
				person P inner join booleanflagdata bfd1 on p.personid = bfd1.entityid
				inner join flag f on f.flagid  = bfd1.flagid
				inner join productcategory PC on 'ServCert_' + productcategoryname = f.flagtextid
				where 
				personid = #request.relayCurrentUser.personid# and 
				flaggroupid =  <cf_queryparam value="#application.com.flag.getFlagGroupStructure("ServiceCertification").flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > )
				and
				productgroup.productgroupname =  <cf_queryparam value="#productserialid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				and productgroup.productgroupid IN (SELECT distinct productgroupid from product 
				WHERE deleted=0)
		</CFQUERY>
		<cfset ArrayAppend(productgroupArray, " , ")>
		<cfloop query="qry_get_ProductGroups">
			<cfset ArrayAppend(productgroupArray, "#productgroupid#,#description#")>
		</cfloop>
	
		<cfreturn productgroupArray>
		
	</cffunction>
	
	<cffunction name="getCSVList" returntype="array" hint="Returns a set of csv files for the claims import">
		<cfargument name="CSVType" type="string" default="">
		<cfinclude template="/code/cftemplates/pcmINI.cfm">
		
		<cfset filesArray = ArrayNew(1)>
	
		<cfdirectory action="LIST" directory="#request.csvfilepathprefix#\#CSVType#\" name="qry_csv_files" filter="*.csv">
		
		<cfloop query="qry_csv_files">
			<cfset ArrayAppend(filesArray, "#name#,#name# - #DATELASTMODIFIED#")>
		</cfloop>
	
		<cfreturn filesArray>
	
	</cffunction>
	
</cfcomponent>