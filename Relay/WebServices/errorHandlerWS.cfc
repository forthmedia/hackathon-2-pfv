<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>
	<cffunction name="jsErrorHandler" access="remote">
		<cfargument name = "line" type="numeric"> <!--- WAB 2014-11-03 CASE 442481 Add type, SQL injection problem --->  
		<cfset application.com.errorHandler.recordRelayError_Javascript (pageurl = arguments.pageurl, fileURL = arguments.fileURL,msg = arguments.msg, line= arguments.line,dataStruct = arguments.dataStruct)>
	</cffunction>
</cfcomponent>