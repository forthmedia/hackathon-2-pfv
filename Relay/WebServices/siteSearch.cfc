<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2014-08-15	RPW	Relayware Internal Search - revert to displaying Dashboard first
2016/06/09	NJH	JIRA PROD2016-342 - add locations to search results. Did some refactoring of existing code. Use entityType variables rather than the friendly name in the condition statements, so that we have a constant.
--->

<cfcomponent output="false">

	<cffunction name="search" output="true" access="public" >
		<cfargument name="searchTextString" default="">
		<cfset var maxDisplayRows = 5>
		<cfset var maxReturnedRows = 100>
		<cfset var returnVar = "">
		<cfset var qSearch=application.com.search.getSiteSearchResults(searchTextString=arguments.searchTextString,tablesToSearch="Person,Location,Organisation",maxReturnedRows=maxReturnedRows)>

		<cfsavecontent variable="returnVar">
			<cfif qSearch.recordCount>
				<cfset application.com.commonqueries.inserttracksearch (searchtype="D", searchstring=arguments.searchTextString, personid=request.relayCurrentUser.personID )>

				<cfoutput query="qSearch" group="entityType">
					<cfquery name="qCount" dbtype="query">
						SELECT count(1) as numRecords
						FROM qSearch
						WHERE entityType = '#qSearch.entityType#'
					</cfquery>
					<cfset var linkVariable = "">
					<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=entityType)>

					<cfswitch expression ="#entityType#">
						<cfcase value="Organisation">
							<cfset linkVariable = "frmSrchOrgID">
						</cfcase>
						<cfcase value="location,person">
							<cfset linkVariable = "frmSrch#entityType#ID">
						</cfcase>
					</cfswitch>

					<cfset var thisCount=1>
					<h4><a href="" onclick="manageClick('searchResults','Search Results','/data/entityList.cfm?tablesToSearch=#entityType#&searchTextString=#URLEncodedFormat(arguments.searchTextString)#',null,{iconClass:'search'}); jQuery('##siteSearchResults').hide(); return false">#entityTypeStruct.label_plural# (#qCount.numRecords# found)</a></h4>
					<ul class="searchResult">
					<cfoutput>
						<cfif thisCount++ lte maxDisplayRows>
							<cfset var title="My Accounts">
							<cfset var iconClass = "accounts">
							<cfset var link = "">
							<!--- 2014-08-15	RPW	Relayware Internal Search - revert to displaying Dashboard first --->

							<cfswitch expression="#entityType#">
								<cfcase value="organisation,location,person">
									<cfset link = "/data/dataFrame.cfm?#linkVariable#=#entityID#">
								</cfcase>
								<cfcase value="files">
									<cfset link = "/filemanagement/files.cfm?fileID=#entityID#&editor=yes&showSaveAndAddNew=false">
									<cfset iconClass = "files">
									<cfset title="My Files">
								</cfcase>
								<cfdefaultcase>
									<cfset link = "/data/entityFrame.cfm?frmEntityTypeID=#entityTypeStruct.entityTypeID#&frmEntityId=#entityID#">
								</cfdefaultcase>
							</cfswitch>

							<li>
								<a href="" title="<cfif entityLink neq "">#entityLink#</cfif>" onclick="manageClick('#title#','#JSStringFormat(entityName)#','#link#',null,{iconClass:'#iconClass#'}); jQuery('##siteSearchResults').hide(); return false">#entityName#</a>
							</li>
						</cfif>
					</cfoutput>
					</ul>
				</cfoutput>
			<cfelse>
				<cfoutput><div>No results match your search criteria.</div></cfoutput>
			</cfif>
		</cfsavecontent>
		<cfreturn returnVar>
	</cffunction>

	<!--- 2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
	<cfscript>
		remote struct function getRecentSearches
		(
			required numeric top
		)
		{
			var rtnStruct = {};
			var resultArray = [];

			var qGetRecentSearches = application.com.search.getRecentSearches(arguments.top);

			for (var c=1;c <= qGetRecentSearches.recordCount;c++) {

				var tmpStruct = {};
				tmpStruct['searchString']['HTMLEditFormat'] = HTMLEditFormat(qGetRecentSearches.searchString[c]);
				tmpStruct['searchString']['JSStringFormat'] = JSStringFormat(qGetRecentSearches.searchString[c]);
				ArrayAppend(resultArray,tmpStruct);

			}

			rtnStruct.results = resultArray;

			return rtnStruct;

		}
	</cfscript>

</cfcomponent>
