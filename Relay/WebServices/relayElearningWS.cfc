<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		WebServices\relayElearningWS.cfc
Author:			???
Date started:	???

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-11			NYB 		changed the column names returned for getSpecialisationItems
2009-05-06			NYB			Sophos - Classroom modules were being listed twice in certificationProgress
2009/07/06			NJH			P-FNL069 - check if user has access to the particular elearning entities. If not, abort the function.
2009-10-02 & 05		NYB			LHID2716
2009/11/05			NJH			LID 2738 - show specialisations on portal for all of organisation if person is the training contact
2010/01/05			NAS			LID 2945 - Add 'Passed' so that Modules can still be completed even though the Certification has been 'Passed'
2010-03-02      	SSS     	Sophos bonanza
2010-08-05 			PPB 		SOP024 - specialisation rule compulsory modules
2010/10/14			NAS			LID4315: E-Learning - Certification - Quizzes - First question randomizes
2010/11/19			NAS			P-LEN022/LID4871: External Portal - My Certification Page - Table titles not showing translations
2011-01-07			NYB 		Changed getCertificationItems to only display active modulesets
2011-03-07			NYB 		removed isTrainingContact.  This is covered by the 'if no access then abort it' code
								improved formatting
2011-09-16 NYB P-SNY106
2012-01-31			NYB			Case#426399 have added the ability to pass through module information as arguments to getCourseLauncherUrl
								instead of calling getModuleDetailsQry everysingle time, because this being called for every module returned
								by getCertificationItems was causing it to return the result so slowly it was raised as a bug
2012-08-14 			PPB 		Case 427049 introduced PhrasePointerQuerySnippets because the query was very slow in some scenarios
2012-08-24 			PPB 		Case 430296 launch courseware from Akamai
2012-09-27			IH			Don't show study now link if module has pre-requisites that hasn't been completed
2012-11-14 			WAB/NJH 	CASE 431977 make getQuiz SCORM aware
2013-01-31 			PPB 		Case 433337 added a preservesinglequotes
2013-03-12 			NYB 		Case 434008 changed subquery in getPersonCertificationData query in getCertificationItems function
2013-04-09			STCR 		CASE 432194 Translation of ModuleSet Title
2013-04-19			STCR		CASE 432328 respect Question OrderMethod
2013-04-23			STCR		CASE 432193 Respect Certification Rule Activation Date
2013-05-22			STCR		CASE 435401 Check Module publishedDate
2013-05-14 			NYB 		Case 434909 removed floating td separator
2013-05-22 			PPB 		Case 435262 added moduleSortOrder
2013-06-18 			PPB 		Case 435765 added top 1 back to SQL in getCertificationItems
2013-07-19			PPB			Case 436072 add reset quiz link to studentprogress screen
2013-11-19 			NYB 		Case 436793 added various lines
2014-02-20 			NYB 		Case 438849 translated Quiz Passed column
								NB:  THIS WILL REQUIRE RUNNING UPDATED VERSION OF:  StoredProcedures updatePhrasePointerCacheForEntityType.sql AND Trigger trigger_phrase_updatePhrasePointerCache.sql
2014-02-26 			NYB 		Case 438849 - added prefix to translation on Wills request/suggestion
2014-06-24 			SB 		    Responsive / Bootstrap
2014-09-22 			DCC 		Case 441140 added 'arguments.'
2015-01-12 			WAB 		CASE 443315 getPersonCertificationData bringing back links to event groups in the past
2015/08/12			NJH			JIRA PROD2015-19 - brought in some training path work done by Yan for Lenovo
2016/01/06			NJH			Jira BF-169 - filter out courses where modules are inactive
2015-11-24			IH			P-TAT006 BRD31. Add showExpired argument for getCourseModulesWithProgress
2015-12-22			PYW 		P-TAT006 BRD 31. Add certificationRuleType column in getCertificationItems
2016-05-18			WAB 		BF-712. getCourseModulesWithProgress(). Query was not dealing with countryGroups
2016-05-18			WAB 		PROD2016-876/PROD2016-1279 XSRF Form Tampering QuizSetup.cfm.  Require some arguments to be encrypted
2016-08-08          DAN         Case 450727 - fix for validating if the certification rule has been fulfilled. Effectively check if the amount of fulfilled modules matches the specified number of modules to pass.

Possible enhancements:

 --->

<cfcomponent output="false">

	<cffunction name="getCertificationItems" access="remote">
		<cfargument name="personCertificationID" type="numeric" required="true">
		<cfargument name="eid" type="numeric" required="false">
		<cfargument name="action" type="string" default="view">
		<cfargument name="Allowupdate" type="string" default="false" required="false">
		<cfargument name="OrderMethod" type="string" default="Random" required="false">
		<cfargument NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">
		<cfargument NAME="showIncorrectQuestionsOnCompletion" DEFAULT="false">

		<cfscript>
			var certificationItemsDisplay_html = "";
			var getPersonCertificationData = "";
			var getFileDetails="";
			var filePath="";
			var courseFileURL="";
			var urlAdditionalParams = "";
			var fromActivationDate = "";
			var ModuleSetPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModuleSet", phraseTextID = "description", baseTableAlias="v"); // 2013-04-09 STCR CASE 432194 Translation of ModuleSet Title
			var access = application.com.rights.doesUserHaveRightsForEntity(entityType="trngPersonCertification",entityID=arguments.personCertificationID,permission="view");
			var encryptedQuizTakenId = ""; //Case 435326
			var encryptedUserModuleProgressId = ""; //Case 435326

			if (not access) {
				application.com.globalFunctions.abortIt();
			}
		</cfscript>
		<!--- 2013-04-30 STCR CASE 432193 - ignore Modules before their Activation Date, unless previewing content for a specific date --->
		<cfif isDefined("request.relaycurrentuser.content.date") and isDate(request.relaycurrentuser.content.date)>
			<cfset fromActivationDate = request.relaycurrentuser.content.date />
		<cfelse>
			<cfset fromActivationDate = request.requestTime />
		</cfif>

		<!--- NJH 2013/05/24 - a precaution... --->
		<cfif not request.relayCurrentUser.isInternal and arguments.allowUpdate>
			<cfset arguments.allowUpdate = false>
		</cfif>

		<cfset titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngMODULE", phraseTextID = "title", baseTableAlias="v")>

		<!--- 2012-01-31 NYB Case#426399 added a 5 minute cache to the query, and rewrote --->
		<!--- 2012/11/22 Case 431933 - removed the cache, as the module progress was taking 5 minutes to update... if done for performance reasons, we will probably need to look
			at setting/tweaking some indexes on the tables involved --->
		<cfquery name="getPersonCertificationData" datasource="#application.siteDataSource#">
			select distinct v.moduleID, isNull(v.userModuleProgressID,0) as userModuleProgressID, moduleCode,credits, v.userModuleFulfilled as userModuleFulfilled,
				<!--- 2015-12-22 PYW P-TAT006 BRD 31. Add certificationRuleType --->
				isNull(v.userModuleStatus,'NotStarted') as userModuleStatus, AICCTitle,v.moduleID,v.certificationRuleType,
				#preserveSingleQuotes(ModuleSetPhraseQuerySnippets.select)# as moduleSetDescription, <!--- 2013-04-09 STCR CASE 432194 Translation of ModuleSet Title --->
				v.personID, personCertificationStatusTextID,v.certificationRuleID,v.certificationRuleOrder,
				qd.active as quizActive,
				tump.startedDateTime as started, tump.lastVisitedDateTime as lastVisited,
				v.quizDetailID,fileID = case when f.fileID is null then 0 else f.fileID end,AICCType,AICCFilename,
				(select top 1 quizTakenID from quizTaken with (noLock) where userModuleProgressID = tump.userModuleProgressID order by lastUpdated desc) as lastQuizTakenID, <!--- CASE 435326 - NJH 2013/05/23 grab the last quizAttempt for a quiz--->	<!--- 2013-07-19 PPB Case 436072 removed duplicate line --->
				(select top 1 eventGroupID from eventDetail with (noLock)
					where moduleID = v.moduleID and (
						(eventstatus = 'Agreed')
							or
						(flagid in (select distinct flagid from eventflagdata where regstatus in ('RegistrationStarted','RegistrationSubmitted','confirmed')
							and
						 eventstatus <> 'initial' and entityid = v.personid)
						)
					)
					<!--- WAB 2015-01-12 CASE 443315, to make sure that does not bring back events in the past --->
					and dateDiff(day, getDate(), isnull(AllocatedStartDate, PreferredStartDate)) >= 0

				) as eventGroupID,
				<!--- 2013-03-12 NYB Case 434008 - replaced:  ---
				(select top 1 regStatus from eventDetail with (noLock) inner join eventFlagData with (noLock) on eventFlagData.flagID = eventDetail.flagID where eventFlagData.entityID = v.personID) as regStatus,
				!--- 2013-03-12 NYB Case 434008 - with:  --->
				(select top 1 regStatus from eventDetail with (noLock) inner join eventFlagData with (noLock) on eventFlagData.flagID = eventDetail.flagID where eventFlagData.entityID = v.personID and eventDetail.moduleID=v.moduleID) as regStatus,	<!--- 2013-06-18 PPB Case 435765 added top 1 back --->
				#preservesinglequotes(titlePhraseQuerySnippets.select)# as moduleTitle,
				secure = case when ft.secure is null then 0 else ft.secure end,
				case when ft.secure = 1 then '#application.paths.securecontent#' else '#application.paths.content#' end + '\'+ isNull(ft.path,'') as absolutePath,
				'/content/' + isNull(ft.path,'') as relativePath, moduleSortOrder		/* 2013-05-22 PPB Case 435262 added moduleSortOrder (its reqd in select cos of distinct) */
				<!--- START: 2013-11-19 NYB Case 436793 added: --->
				,isnull(qd.NumberOfAttemptsAllowed,0) as NumberOfAttemptsAllowed
				,isnull(tump.QuizAttempts,0) as QuizAttempts
				<!--- END: 2013-11-19 NYB Case 436793 --->
			from vTrngPersonCertifications v
				left join trngUserModuleProgress tump with (noLock)	on v.userModuleProgressID = tump.userModuleProgressID
				left join quizDetail qd with (noLock) on qd.quizDetailID = v.quizDetailID
				left outer join (files f with (noLock)
					inner join fileType ft with (noLock) on f.fileTypeID=ft.fileTypeID) on v.fileID = f.fileID
				#preservesinglequotes(titlePhraseQuerySnippets.join)#  <!--- 2012-08-14 PPB Case 427049 introduced PhrasePointerQuerySnippets --->
				#preservesinglequotes(ModuleSetPhraseQuerySnippets.join)# <!--- 2013-04-09 STCR CASE 432194 Translation of ModuleSet Title --->

			where personCertificationID = <cf_queryparam value="#arguments.personCertificationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and v.ruleActive=1 and v.moduleSetActive=1 /*<!--- NYB 2011-01-07 should only be displaying active modulesets --->*/
				and dbo.dateAtMidnight(v.activationDate) <= #createODBCDateTime(fromActivationDate)# /* this checks that Rule is Active but does not check whether Module is active */ <!--- 2013-04-23 STCR CASE 432193 Respect Certification Rule Activation Date --->
				and dbo.dateAtMidnight(v.modulePublishedDate) <= #createODBCDateTime(fromActivationDate)# /* 2013-05-22 STCR CASE 435401 check that the Module is also active */
			order by certificationRuleOrder, moduleSetDescription, moduleSortOrder 		/* 2013-05-22 PPB Case 435262 added moduleSortOrder removed started desc */
		</cfquery>

		<cfsavecontent variable="certificationItemsDisplay_html">
				<div id="personCertificationDataDiv">
				<table width="100%" data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-condensed withBorder" id="personCertificationDataTable">
					<cfif (getPersonCertificationData.recordCount gt 1) or (getPersonCertificationData.recordCount eq 1 and getPersonCertificationData.moduleID neq "")>
						<thead>
							<tr>
								<!--- 2013-03-12 NYB Case 434008 - added class:  --->
								<th><cf_translate>phr_elearning_ModuleSet</cf_translate></th>
								<th><cf_translate>phr_elearning_moduleCode</cf_translate></th>
								<th><cf_translate>phr_elearning_moduleTitle</cf_translate></th>
								<th><cf_translate>phr_elearning_moduleType</cf_translate></th>
								<th><cf_translate>phr_elearning_CertificationRuleType</cf_translate></th>
								<th>&nbsp;</th>
								<cfif arguments.allowUpdate><th class="6"  valign="top">&nbsp;</th></cfif> <!--- CASE 435326 - NJH 2013/05/23  --->
								<cfif arguments.allowUpdate><th class="6"  valign="top">&nbsp;</th></cfif> <!--- CASE 435326 - NJH 2013/05/23  --->
							</tr>
						</thead>
						<cfset qryModuleStatuses = application.com.relayElearning.getUserModuleStatus()>	<!--- 2010-02-18 PPB B-01696 get statuses for dropdowns --->
						<cfset updatableModuleFound = false>

						<cfoutput query="getPersonCertificationData" group="moduleSetDescription">

                        <cfset var prevRuleFullfilled = isPreviousCertificationRuleFulfilled(arguments.personCertificationID, certificationRuleOrder)> <!--- Case 450727 --->

						<cfset colCount = 0>
						<cfset rowspan = 0>

						<cfoutput><cfset rowspan = rowspan + 1></cfoutput>

						<tr>
							<cf_translate>
							<!--- 2013-03-12 NYB Case 434008 - added class:  --->

									<cfoutput>
										<cfset colCount = colCount + 1>
										<cfif colCount gt 1>
										<tr>
										</cfif>
											<!--- 2013-03-12 NYB Case 434008 - added class:  --->
											<td class="1">#moduleSetDescription#</td>
											<td class="2">#moduleCode#</td>
											<td class="3">#moduleTitle#</td>
											<td class="4">#AICCType#</td>
											<td class="5">#certificationRuleType#</td>
											<td class="6">

												<!--- NYB 2012-01-30 Case#426399 replaced cfscript.  Don't know why but breaking out into cfscript seemed to slow down the process
												     created arg struct, passed this to getCourseLauncherURL --->
												<cfif moduleID neq "" and moduleID neq 0>
													<cfset courseFileURL = "">
													<!--- 2012-08-24 PPB Case 430296 replace line to launch file from Akamai as appropriate
													<cfset argumentsStruct = {moduleID=moduleID,userModuleProgressID=userModuleProgressID,personID=personID,fileID=fileID,filename=AICCFilename,deliveryMethod="local",absolutePath=absolutePath,relativePath=relativePath,secure=secure}>
													--->
													<cfset argumentsStruct = {moduleID=moduleID,userModuleProgressID=userModuleProgressID,personID=personID,fileID=fileID,filename=AICCFilename,absolutePath=absolutePath,relativePath=relativePath,secure=secure}>
													<cfset courseFileURL = application.com.relayElearning.getCourseLauncherUrl(argumentCollection=argumentsStruct)>
												</cfif>
												<cfif userModuleProgressID neq "" and userModuleProgressID neq 0>
													<cfset urlAdditionalParams='&userModuleProgressID=#userModuleProgressID#'>
													<cfif eventGroupID neq "" and eventGroupId neq 0>
														<cfset urlAdditionalParams = urlAdditionalParams & '&moduleID=#moduleID#'>
													</cfif>
												<cfelse>
													<cfset urlAdditionalParams='&moduleID=#moduleID#&personID=#personID#'>
												</cfif>

												<cfif arguments.allowUpdate eq "false">	<!--- 2010-02-18 PPB B-01696 add condition --->
													<cfif personID neq request.relayCurrentUser.personID or not request.relayCurrentUser.isLoggedIn>
														phr_elearning_#userModuleStatus#
													<!--- 2010/01/05			NAS			LID 2945 - Add 'Passed' so that Modules can still be completed even though the Certification has been 'Passed' --->
													<!--- 2013-11-19 NYB Case 436793 added exception for Failed quizzes that still have attempts available --->
													<cfelseif (listFindNoCase("Registered,Pending,Passed",personCertificationStatusTextID) and listFindNoCase("InProgress,NotStarted",userModuleStatus)) or (userModuleStatus eq "Failed" and NumberOfAttemptsAllowed gt QuizAttempts)>
														<cfset addBR = false>
														<cfif listFindNoCase("OnLine,On-Line",AICCType) and courseFileURL neq "">
															<!--- 2012-08-24 PPB Case 430296 made window resizable while i was at it --->
															<cfif not prevRuleFullfilled> <!--- Case 450727 --->
																phr_elearning_studyLater
															<cfelse>
																<div class="pull-left">
																<a href="javascript:void(openWin('#courseFileURL#','Training#personCertificationID##moduleID#','width=#application.com.settings.getSetting('files.maxPopupWidth')#,height=#application.com.settings.getSetting('files.maxPopupHeight')#,scrollbars=no,status=no,address=no,resizable=1'));" align="left" class="btn btn-primary studyNowRL">phr_elearning_studyNow</a>
																</div>
															</cfif>
															<cfset addBR = true>
														<cfelseif listFindNoCase("Classroom,Webinar",AICCType) and eventGroupID neq "">
															<cfif regStatus eq "">
																<div class="pull-left">
																	<a href="javascript:void(openWin('/relayTagTemplates/eventList.cfm?eventGroupID=#eventGroupID##urlAdditionalParams#','TrainingEvent','width=600,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=0'));" class="btn btn-primary registerNowRL">phr_elearning_registerNow</a>
																</div>
															<cfelse>
																phr_elearning_#regStatus#
															</cfif>
															<cfset addBR = true>
														</cfif>
														<cfif QuizDetailID neq "" and QuizDetailID neq 0 and quizActive>
															<cfif prevRuleFullfilled> <!--- Case 450727 --->
																<cfset encryptedLink = application.com.security.encryptURL('/Quiz/Start.cfm?QuizDetailID=#QuizDetailID#&stringencyMode=#arguments.stringencyMode#&OrderMethod=#arguments.OrderMethod#&showIncorrectQuestionsOnCompletion=#arguments.showIncorrectQuestionsOnCompletion##urlAdditionalParams#')><!--- 2013-04-19 STCR CASE 432328 respect Question OrderMethod --->
																	<div class="pull-left">
																<a href="#encryptedLink#" class="btn btn-primary takeQuizRL">phr_elearning_takeQuiz</a>
																	</div>
															<cfelse>
																phr_elearning_takeQuizLater
															</cfif>
														</cfif>
													<cfelseif userModuleStatus eq "Passed" and listFindNoCase("Classroom,Webinar",AICCType)>
														<cfif userModuleStatus eq "NotStarted">
															phr_elearning_#regStatus#
														<cfelse>
															phr_elearning_#userModuleStatus#
														</cfif>
													<cfelse>
														phr_elearning_#userModuleStatus#
													</cfif>
												<cfelse>
													<!--- 2010-02-18 PPB B-01696 show dropdowns to allow user to edit statuses if allowUpdate eq "true" (ie user chose menu option Update Person Certifications not Person Certifications --->
													<cfif userModuleStatus eq "NotStarted">		<!--- there is no trngUserModuleProgress row for this module, so don't offer the dropdown to update it --->
														phr_elearning_#userModuleStatus#
													<cfelse>
														<cfset updatableModuleFound = true>
														<select id="moduleResult_#getPersonCertificationData.userModuleProgressID#" class="control-label">
															<cfloop query="qryModuleStatuses">
																<option value="#qryModuleStatuses.statusId#" <cfif getPersonCertificationData.userModuleStatus eq qryModuleStatuses.statusTextId>selected</cfif>>#description#</option>
															</cfloop>
														</select>
													</cfif>
												</cfif>
											</td>
											<!--- CASE 435326 - NJH 2013/05/23 - output the link to reset a quiz attempt if it exists --->
											<cfif arguments.allowUpdate>
											<td class="7">
												<cfif lastQuizTakenID neq "">
													<cfset encryptedUserModuleProgressId = application.com.security.encryptVariableValue(name="userModuleProgressId",value=userModuleProgressId)>
													<cfset encryptedQuizTakenId = application.com.security.encryptVariableValue(name="quizTakenId",value=lastQuizTakenID)>
													<a href="" onclick="resetQuiz('#encryptedUserModuleProgressId#','#encryptedQuizTakenId#');return false;">Reset Quiz</a>
												</cfif>
											</td>
											</cfif>
										<cfif colCount gt 1>
										</tr>
										</cfif>

									</cfoutput>
							</cf_translate>
						</tr>
						<!--- START: 2013-05-16 NYB Case 434909 - removed, is distroying layout ---
						<cfif currentRow neq getPersonCertificationData.recordCount>
						<tr>
							<td>&nbsp;</td><!--- space between module sets --->
						</tr>
						</cfif>
						!--- END: 2013-05-16 NYB Case 434909 --->
						</cfoutput>
						<cfif updatableModuleFound>
						<tr>
							<td colspan="99"><span id="savedMessage" class="messageText"></span>&nbsp;&nbsp;<CF_INPUT type="button" value="save" onclick="saveModules(#personCertificationID#)"> </td>
						</tr>
						</cfif>
					<cfelse>
						<tr>
							<td><cf_translate>phr_elearning_NoModuleHaveBeenStartedForCertification.</cf_translate></td>
						</tr>
					</cfif>
				</table>
				</div>
		</cfsavecontent>

		<cfif arguments.action eq "update">
			<cfoutput><cf_translate>#certificationItemsDisplay_html#</cf_translate></cfoutput>
			<cfreturn>
		</cfif>

		<cfset result = structNew()>
		<cfset result.content = certificationItemsDisplay_html>


		<cfreturn result>
<!--- 		<cfoutput>
				#serializeJSON(result)#
		</cfoutput>
		<CF_ABORT>
 --->
	</cffunction>

	<!--- 2010-02-18 PPB B-01696 new function --->
	<cffunction name="updateCertificationItems" access="remote" returntype="Struct" >
		<cfargument name="personCertificationID" type="numeric" required="true">
		<cfargument name="arrResults" type="string"required="true">

		<cfset var passedStatusID = application.com.relayElearning.getUserModuleStatus(statusTextID='Passed').statusID>
		<cfset var updateCertificationItems = ''>
		<cfset var arrModule = ''>
		<cfset var getCertificationSummary = ''>
		<cfset var result = structNew()>

		<cfset var arrayResults = deserializeJSON(#arguments.arrResults#)>		<!--- 2014-09-22 DCC CASE 441140 added 'arguments.' --->

		<!--- arrResults is a 2-dimensional array storing userModuleProgressId and pass/fail/in progress for each module; --->
		<!--- loop around arrayResults for each module (each of which is an array (arrModule) inside the arrResults array) --->
		<cfloop array="#arrayResults#" index="arrModule">						<!--- 2014-09-22 DCC CASE 441140 added 'arguments.' --->

			<cfif arrModule[2] gt 0>		<!--- if the status=0 in the dropdown the user doesn't have a trngUserModuleProgress row for this module so don't attempt to update it --->
				<cfquery name="updateCertificationItems" datasource="#application.siteDataSource#">
					UPDATE trngUserModuleProgress

					<cfif arrModule[2] eq passedStatusID>
						SET moduleStatus =  <cf_queryparam value="#passedStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > , userModuleFulfilled = ISNULL(userModuleFulfilled,getdate()),			<!--- don't stomp over an existing date --->
					<cfelse>
						SET moduleStatus =  <cf_queryparam value="#arrModule[2]#" CFSQLTYPE="CF_SQL_INTEGER" > , userModuleFulfilled = NULL,
					</cfif>

					lastUpdated = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
					lastUpdatedBy = <cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">

					WHERE userModuleProgressId =  <cf_queryparam value="#arrModule[1]#" CFSQLTYPE="CF_SQL_INTEGER" >
					<!--- AND	moduleStatus <> #arrModule[2]#			don't do update if status hasn't changed so lastupdated and lastupdatedby aren't updated --->
				</cfquery>
			</cfif>
		</cfloop>

		<!--- now get the number of modules passed to update the certificate row in the 'parent' TFQO --->
		<cfquery name="getCertificationSummary" datasource="#application.siteDataSource#">
			SELECT * FROM vTrngCertificationSummary WHERE personCertificationID = #personCertificationID#
		</cfquery>

		<cfset result.ISOK = true>
		<cfset result.cellUpdates = structNew()>  <!--- keys in this structure are names of columns which will be updated with the value--->
		<cfset result.cellUpdates["Modules_Passed"] = getCertificationSummary.modules_passed>

		<cfreturn result>
	</cffunction>

	<cffunction name="updateModuleStatus" access="remote">
		<cfargument name="userModuleProgressId" type="numeric" required="true">
		<cfargument name="moduleid" type="numeric" required="true">

		<!--- NJH 2012/11/16 CASE 432161 - this function was calling updateUserModuleProgress with a completed date, but it should be calling setModuleComplete which then calls updateUserModuleProgress. SetModuleComplete
			does some other stuff that needs to be happening such as passing certifications, etc. --->
		<cfreturn application.com.relayElearning.setModuleComplete(userModuleProgressId=arguments.userModuleProgressId)>
	</cffunction>

	<cffunction name="insertUserModuleProgress" access="remote">
		<cfargument name="moduleID" type="numeric" required="true">
		<cfargument name="personID" type="numeric" required="true">

		<!--- NJH 2009/07/01 P-FNL069 - only insert user module progress if we're an internal user or if we are the person on the portal  --->
		<cfscript>
			var userModuleProgressID = -1;
			var msg = "";

			if ((request.relayCurrentUser.isInternal or request.relayCurrentUser.personID eq arguments.personID) and request.relayCurrentUser.isLoggedIn) {
				userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=arguments.moduleID,personID=arguments.personID);
			}
		</cfscript>

		<!--- NJH 2009/07/01 - don't think that userModuleProgressId is ever 0!! as one gets created or an existing one is returned --->
		<cfif userModuleProgressID eq 0>
			<cfset msg= "phr_elearning_userModuleInProgress">
		<cfelseif userModuleProgressID gt 0>
			<cfset msg= "phr_elearning_userModuleStarted">
		</cfif>

		<cf_translate phrases= "#msg#"/>

		<cfset result = structNew()>
		<cfset result.content = evaluate(msg)>
		<cfoutput>
				#serializeJSON(result)#
		</cfoutput>
		<CF_ABORT>

	</cffunction>


	<cffunction name="getSpecialisationItems" access="remote">
		<cfargument name="entitySpecialisationID" type="numeric" required="true">

		<cfscript>
			var getEntitySpecialisationData = "";
			var isTrainingContact = false; // NJH 2009/11/06 LID 2738
			var access = application.com.rights.doesUserHaveRightsForEntity(entityType="entitySpecialisation",entityID=arguments.entitySpecialisationID,permission="view");
			var specialisationItemsDisplay_html = "";

			if (not access) {
				application.com.globalFunctions.abortIt();
			}
		</cfscript>

		<!--- NJH 2009/11/06 LID 2738 --->
		<cfif request.relayCurrentUser.personID eq application.com.relayElearning.getTrainingContact(organisationID=request.relayCurrentUser.organisationID)>
			<cfset isTrainingContact = true>
		</cfif>

		<!--- 2011/11/23 WAB LID 8218 Convert to entityTranslations --->
		<cfquery name="getEntitySpecialisationData" datasource="#application.siteDataSource#">
				select distinct
				tc.SortOrder,
				'phr_title_trngcertification_' + convert(varchar,tc.certificationid) as Title,
				tpc.registrationDate,tpc.passDate,
				p.firstname + ' ' +p.lastname as fullname
				from entitySpecialisation es with (noLock)
				inner join specialisation s with (noLock) on es.specialisationID = s.specialisationID
				inner join specialisationRule sr with (noLock) on sr.specialisationID = s.specialisationID
				inner join trngCertification tc with (noLock) on sr.certificationID = tc.certificationID
				inner join trngPersonCertification tpc with (noLock) on tpc.certificationID = tc.certificationID
				inner join person p with (noLock) on tpc.personID = p.personID and p.organisationID = es.entityID and es.entityTypeID=2
				inner join trngPersonCertificationStatus tpcs with (noLock) on tpc.statusID = tpcs.statusID and statusTextID in ('Passed','Pending')
			where es.entitySpecialisationID = #arguments.entitySpecialisationID#
				<!--- NJH 2009/07/01 P-FNL069 if we're on the portal or not logged in, get only user specialisation items --->
				<!--- NJH 2009/11/06 LID 2738 - if we're the training contact on the portal, then show all certifications  --->
				<cfif (not request.relayCurrentUser.isInternal and not isTrainingContact) or not request.relayCurrentUser.isLoggedIn>
					and p.personID = #request.relayCurrentUser.personID#
				</cfif>
			order by tc.SortOrder,
				<!--- WAB 2011/11/23 No sense ordering on a phraseText ID, if essential convert to use phrasePointerCache code 'phr_' + tc.TitlePhraseTextID, --->
				p.firstname + ' ' +p.lastname, tpc.registrationDate desc,tpc.passdate desc
		</cfquery>

		<!--- 2010-08-06 PPB P-SOP024 --->
		<cfquery name="getEntitySpecialisationModuleData" datasource="#application.siteDataSource#">
			SELECT DISTINCT tm.SortOrder, tm.modulecode, 'phr_title_TrngModule_' + CONVERT(varchar, tm.moduleID) AS Title, tump.userModuleFulfilled as passdate, p.FirstName + ' ' + p.LastName AS fullname
			FROM         dbo.entitySpecialisation AS es WITH (noLock) INNER JOIN
			                      dbo.Specialisation AS s WITH (noLock) ON es.specialisationID = s.SpecialisationID INNER JOIN
			                      dbo.SpecialisationRule AS sr WITH (noLock) ON sr.specialisationID = s.SpecialisationID INNER JOIN
			                      dbo.trngCertification AS tc WITH (noLock) ON sr.certificationID = tc.certificationID INNER JOIN
			                      dbo.trngCertificationRule AS tcr WITH (noLock) ON tcr.certificationID = tc.certificationID INNER JOIN
			                      dbo.trngModuleSetModule AS tmsm WITH (noLock) ON tmsm.moduleSetID = tcr.moduleSetID INNER JOIN
			                      dbo.trngModule AS tm WITH (noLock) ON tm.moduleID = tmsm.moduleID INNER JOIN
			                      dbo.trngUserModuleProgress AS tump WITH (noLock) ON tump.moduleID = tm.moduleID INNER JOIN
								  dbo.Person AS p WITH (noLock) ON tump.personID = p.PersonID AND p.OrganisationID = es.entityID AND es.entityTypeID = 2 INNER JOIN
			                      dbo.trngUserModuleStatus AS tums WITH (noLock) ON tums.statusID = tump.moduleStatus AND tums.statusTextID IN ('Passed')
			WHERE     (es.entitySpecialisationID = #arguments.entitySpecialisationID#)
				<cfif (not request.relayCurrentUser.isInternal and not isTrainingContact) or not request.relayCurrentUser.isLoggedIn>
					and p.personID = #request.relayCurrentUser.personID#
				</cfif>
			ORDER BY tm.SortOrder, Title, fullname, tump.userModuleFulfilled
		</cfquery>



		<cfsavecontent variable="specialisationItemsDisplay_html">
			<cf_translate>
				<div id="entitySpecialisationDataDiv">
					<cfif getEntitySpecialisationData.recordCount gt 0>
						<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-condensed"  id="entitySpecialisationDataTable">
								<thead>
						<tr>
							<!--- 2008-11-11 NYB P-SOP010 - replaced code with Title --->
							<th>phr_Certification_Title</th>
							<th>phr_elearning_fullname</th>
							<th>phr_elearning_registrationDate</th>
							<th>phr_elearning_passDate</th>
						</tr>
								</thead>
								<tbody>
						<cfoutput query="getEntitySpecialisationData">
						<tr>
							<!--- 2008-11-11 NYB P-SOP010 - replaced code with Title --->
							<td>#title#</td>
							<td>#fullname#</td>
							<td>#dateFormat(registrationDate,"dd-mmm-yyyy")#</td>
							<td>#dateFormat(passDate,"dd-mmm-yyyy")#</td>
						</tr>
						</cfoutput>
								</tbody>
						</table>
					<cfelse>
						phr_elearning_NoCertificationsHaveBeenCompleted.
					</cfif>
				</div>

				<div id="entitySpecialisationModuleDataDiv">
					<cfif getEntitySpecialisationModuleData.recordCount gt 0>
						<table width="100%" data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder"  id="entitySpecialisationModuleDataTable">
							<thead>
						<tr>
							<!--- 2008-11-11 NYB P-SOP010 - replaced code with Title --->
							<th>phr_elearning_ModulesPassed</th>
							<th>phr_elearning_fullname</th>
							<th>phr_elearning_moduleCode</th>
							<th>phr_elearning_passDate</th>
						</tr>
							</thead>
							<tbody>
						<cfoutput query="getEntitySpecialisationModuleData">
						<tr>
							<!--- 2008-11-11 NYB P-SOP010 - replaced code with Title --->
							<td>#title#</td>
							<td>#fullname#</td>
							<td>#modulecode#</td>
							<td>#dateFormat(passdate,"dd-mmm-yyyy")#</td>
						</tr>
						</cfoutput>
							</tbody>
						</table>
					<cfelse>
						phr_elearning_NoModulesHaveBeenCompleted.
					</cfif>
				</div>
			</cf_translate>
		</cfsavecontent>

		<cfset result = structNew()>
		<cfset result.content = specialisationItemsDisplay_html>
		<cfoutput>
				#serializeJSON(result)#
		</cfoutput>
		<CF_ABORT>

	</cffunction>


	<cffunction name="getQuiz" access="remote">
		<cfargument name="userModuleProgressID" type="numeric" required="true">
		<cfargument name="allowUpdate" type="string" default="false" required="false">   <!--- 2013-07-19 PPB Case 436072 --->

		<cfscript>
			var getEntityQuizData = "";
			var passDecidedByQuiz = "";
			var access = application.com.rights.doesUserHaveRightsForEntity(entityType="trngUserModuleProgress",entityID=arguments.userModuleProgressID,permission="view");
			var namePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "quizDetail", phraseTextID = "title",baseTableAlias="qd");
			var quizDisplay_html = "";

			if (not access) {
				application.com.globalFunctions.abortIt();
			}
		</cfscript>

		<cfquery name="passDecidedByQuiz" datasource="#application.siteDataSource#">
			select tm.passDecidedBy, tm.isScorm
			from
				trngmodule tm join
				trngusermoduleprogress tump on tm.moduleID = tump.moduleID
			where tump.userModuleProgressID=#arguments.userModuleProgressID#
			<cfif not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn>
				and tump.personID = #request.relayCurrentUser.personID#
			</cfif>
		</cfquery>

		<!--- WAB/NJH 2012-11-14 CASE 431977
			This query was not aware of scorm quizzes which do not have a quizdetail, so make a LEFT join and get scorm name from files table
			NJH ordered by score rather than quizTakenID, as we want to display the highest scored quiz
		--->
		<cfquery name="getEntityQuizData" datasource="#application.siteDataSource#">
			select top 1
				qt.quizTakenId,		<!--- 2013-07-19 PPB Case 436072 --->
				case
					when qd.quizdetailid is null then
					(select name from files f inner join trngmodule m on m.fileID = f.fileID inner join trngUserModuleProgress tump on tump.moduleID = m.moduleID and tump.usermoduleProgressID=#arguments.userModuleProgressID#)
					else #namePhraseQuerySnippets.select#
				end as Quizname,
				qt.passmark,
				score,
				completed,
				<!--- START: 2014-02-26 NYB Case 438849 changed to default to statusphrasetextid first if null --->
				case
					when statustranslated like 'Quiz_%' then
						isnull(statusphrasetextid,'')
					else statustranslated
				end as result,
				<!--- END: 2014-02-26 NYB Case 438849 --->
				<cfif passDecidedByQuiz.isScorm eq '0'>
				Case
					When qd.quizdetailid is not null AND qt.completed iS NULL or qt.completed is not null and qt.score < qt.passmark THEN
						qd.numberOfAttemptsAllowed - (select count(*) from quizTaken where userModuleProgressID=#arguments.userModuleProgressID# and quizDetailID = qd.QuizDetailID)
					else 0
				End
				<cfelse>
				'N/A'
				</cfif>
				as AttemptsRemaining
			from
				QuizTaken qt
				LEFT join QuizDetail qd on qd.quizdetailID = qt.quizdetailID

				<!--- START: 2014-02-20 NYB Case 438849 - added: --->
				left join (
				<!--- START: 2014-02-26 NYB Case 438849 - added prefix to translation on Wills request/suggestion --->
					select 'Pass' as statusphrasetextid,'#application.com.relayTranslations.translatePhrase("Quiz_Pass")#' as statustranslated union
					select 'Fail' as statusphrasetextid,'#application.com.relayTranslations.translatePhrase("Quiz_Fail")#' as statustranslated
				<!--- END: 2014-02-26 NYB Case 438849 --->
					) as statusTranslations on qt.QuizStatus=statusTranslations.statusphrasetextid
				<!--- END: 2014-02-20 NYB Case 438849 --->

				#preserveSingleQuotes(namePhraseQuerySnippets.join)#
			where userModuleProgressID=#arguments.userModuleProgressID#
				<!--- NJH 2009/07/01 P-FNL069 if we're on the portal or not logged in, get only user's quiz --->
				<cfif not request.relayCurrentUser.isInternal or not request.relayCurrentUser.isLoggedIn>
					and qt.personID = #request.relayCurrentUser.personID#
				</cfif>
			order by qt.completed desc,score desc
		</cfquery>

		<cfsavecontent variable="quizDisplay_html">
		<cf_translate>
			<div id="entitySpecialisationDataDiv">
					<cfif getEntityQuizData.recordCount gt 0 and passDecidedByQuiz.passDecidedBy eq 'Quiz'>
					<table width="100%" data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-condensed" id="entitySpecialisationDataTable">
						<thead>
							<tr>
							<!--- 2008-11-11 NYB P-SOP010 - replaced code with Title --->
							<th>phr_elearning_QuizName</th>
							<th>phr_elearning_passmark</th>
							<th>phr_elearning_score</th>
							<th>phr_elearning_quizCompleted</th>
							<th>phr_elearning_QuizPassed</th>
							<th>phr_elearning_attemptsRemaining</th>
							<!--- START 2013-07-19 PPB Case 436072 --->
							<cfif arguments.allowUpdate and getEntityQuizData.quizTakenID neq "">
								<th>&nbsp;</th>
							</cfif>
							<!--- END 2013-07-19 PPB Case 436072 --->
							</tr>
						</thead>
						<tbody>
							<cfoutput query="getEntityQuizData">
								<tr>
									<!--- 2008-11-11 NYB P-SOP010 - replaced code with Title --->
									<td>#QuizName#</td>
									<td>#passmark#</td>
									<!--- PJP 03/12/2012: CASE 431389: Replaced any unnecessary trailing zeros from results screen --->
									<td>#REReplace(REReplace(score, "0+$", "", "ALL"), "\.+$", "")#</td>
									<td>#dateFormat(completed,"dd-mmm-yyyy")#</td>
									<td>#result#</td>
									<td>#attemptsRemaining#</td>
								</tr>
							</cfoutput>
						</tbody>
					</table>
					<cfelse>
					phr_elearning_NoQuizzesHaveBeenTaken.
					</cfif>
				</div>
			</cf_translate>
		</cfsavecontent>

		<cfset result = structNew()>
		<cfset result.content = quizDisplay_html>
		<cfoutput>
				#serializeJSON(result)#
		</cfoutput>
		<CF_ABORT>

	</cffunction>


	<cffunction name="getCertificationModules" access="remote">
		<cfargument name="specialisationRuleID" type="numeric" required="true">
		<cfargument name="certificationID" type="numeric" required="true">
		<cfargument name="originalCertificationID" type="numeric" required="true" >

		<cfset qryCertificationModules = application.com.relayElearning.getCertificationModules(specialisationRuleID=specialisationRuleID,certificationID=certificationID,originalCertificationID=originalCertificationID)>

		<cfreturn qryCertificationModules>
	</cffunction>

	<cffunction name="getSpecialisationRuleCompulsoryModules" access="remote">
		<cfargument name="specialisationRuleID" type="numeric" required="true">

		<cfset qrySpecialisationRuleCompulsoryModules = application.com.relayElearning.getSpecialisationRuleCompulsoryModules(specialisationRuleID=specialisationRuleID)>

		<cfreturn qrySpecialisationRuleCompulsoryModules>
	</cffunction>

	<cffunction name="getRatingsDiv" access="remote">
		<cfargument name="moduleID" type="numeric" required="true">

		<cfreturn application.com.request.postprocesscontentstring(application.com.relayElearning.getRatingsDiv(moduleID=arguments.moduleID))>
	</cffunction>

	<!--- NJH 2012/11/22 Case 431933  - used to refresh the module status --->
	<cffunction name="getModuleStatus" access="remote">
		<cfargument name="userModuleProgressID" type="numeric" required="true">

		<cfset var getModuleAndPersonId = "">

		<cfquery name="getModuleAndPersonId" datasource="#application.siteDataSource#">
			select moduleId,personID from trngUserModuleProgress where userModuleProgressID=<cf_queryparam value="#arguments.userModuleProgressID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn application.com.relayElearning.GetTrainingModulesData(moduleID=getModuleAndPersonId.moduleID,personID=getModuleAndPersonId.personID).module_completion_status>
	</cffunction>

	<!--- NJH 2012/11/22 Case 431933  - used to refresh the certification status --->
	<cffunction name="getPersonCertificationSummary" access="remote">
		<cfargument name="personCertificationID" type="numeric" required="true">

		<cfset var certification = application.com.structureFunctions.queryRowToStruct(query=application.com.relayElearning.GetCertificationSummaryData(personCertificationID=arguments.personCertificationID),row=1)>

		<cfloop collection="#certification#" item="key">
			<cfif isDate(certification[key])>
				<cfset certification[key] = lsdateFormat(certification[key],"medium")>
			</cfif>
		</cfloop>

		<cfreturn certification>
	</cffunction>

	<!--- CASE 435326 - NJH 2013/05/23 - add ability to reset a quiz taken record. --->
	<cffunction name="resetQuizAttempt" access="remote" returnType="struct">
		<cfargument name="quizTakenID" type="numeric" required="true" encrypted="true">
		<cfargument name="userModuleProgressID" type="numeric" required="true" encrypted="true">

		<cfset var result = {isOK=true,message="Quiz reset successfully"}>
		<cfset var inProgressStatusID = application.com.relayElearning.getUserModuleStatus(statusTextID="InProgress").statusID> <!--- set the status of the module to be in progress... --->
		<cfset var argumentsStruct = {frmStatusID=inProgressStatusID}>

		<cf_transaction action="begin">
			<cfif arguments.quizTakenID neq 0> 	<!--- 2013-07-25 PPB Case 436072 --->
				<cfset result = application.com.relayQuiz.deleteQuizTaken(quizTakenId=arguments.quizTakenID)>
			</cfif>

			<cfif result.isOK>
				<cftry>
					<!--- set the user module progress record back to in progress --->
					<cfset application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=arguments.userModuleProgressID,argumentsStruct=argumentsStruct)>

					<cf_transaction action="commit">

					<cfcatch>
						<cfset application.com.errorHandler.recordRelayError_Warning(type="Elearning",Severity="error",catch=cfcatch,WarningStructure=arguments)>

						<cf_transaction action="rollback">
						<cfset result.isOK = false>
						<cfset result.message = "Error resetting quiz.">
						<cfrethrow>
					</cfcatch>
				</cftry>
			</cfif>
		</cf_transaction>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2013/11/05 - Case 437459 - function to get the modules that start with a certain letter. Used for the modules select box that is filtered (course edit page)
	2013/11/13 GCC changed to include modulecode as well for begins with if it exists --->
	<!--- NJH 2013/11/21 Case 437964 - filter to show only available modules --->
	<cffunction access="remote" name="getModulesForSelect" hint="Retrieves a list of usergroups for a select." returntype="query">
		<cfargument name="startsWith" type="string" default="">
		<cfargument name="excludeModules" type="string" default="">
		<cfargument name="trainingProgramID" type="numeric" default="0">

		<cfset var moduleQuery = "">

		<cfquery name="moduleQuery" datasource="#application.siteDataSource#">
			select isnull(moduleCode,'')+': '+ CASE WHEN len(title_defaultTranslation) > 100
			THEN left(title_defaultTranslation,30) +  ' ... ' + right(title_defaultTranslation,30)
			ELSE title_defaultTranslation END AS Title, moduleId from trngModule
			where isnull(moduleCode,title_defaultTranslation)  like  <cf_queryparam value="#arguments.startsWith#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				and availability = 1
				<cfif arguments.excludeModules neq "">
				and moduleID  not in ( <cf_queryparam value="#arguments.excludeModules#" CFSQLTYPE="cf_sql_integer"  list="true"> )
				</cfif>
				<cfif arguments.trainingProgramID neq 0>
				and trainingProgramID = <cf_queryparam value="#arguments.trainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
				</cfif>
			order by moduleCode,title_defaultTranslation
		</cfquery>

 		<cfreturn moduleQuery>
	</cffunction>

	<!--- 2014/10/02	YMA	CORE-677 add missing web sercice hooks. perhaps forgotton from original commit? --->
	<cffunction name="insertQuizDetailQuestionPool" access="remote" returnType="struct">
		<cfargument name="quizDetailID" type="numeric" required="true" encrypted="true">
		<cfargument name="questionPoolID" type="numeric" required="true" encrypted="true">
		<cfargument name="numOfQuestions" type="numeric" required="true">

		<cfreturn application.com.relayQuiz.insertQuizDetailQuestionPool(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getQuizDetailQuestionPoolDisplay" access="remote" returnType="struct">
		<cfreturn application.com.relayQuiz.getQuizDetailQuestionPoolDisplay(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="deleteQuizDetailQuestionPool" access="public" returnType="struct" >
		<cfargument name="quizDetailQuestionPoolId" type="numeric" required="true" encrypted="true">
		
		<cfreturn application.com.relayQuiz.deleteQuizDetailQuestionPool(argumentCollection=arguments)>
	</cffunction>


	<!--- NJH 2015/08/12 Jira PROD2015-19 Suggest a TrainingPath path for a user to follow.
		NJH	2016/01/06	Jira BF-169 - filter out courses where modules are inactive
	 --->
	<cffunction access="remote" name="getCoursesForSelect" hint="Retrieves a list of courses for a select." returntype="query">
		<cfargument name="startsWith" type="string" default="">
		<cfargument name="excludeCourses" type="string" default="">
		<cfset var courseQuery = "">

		<cfquery name="courseQuery" datasource="#application.siteDataSource#">
			select distinct isnull(AICCCourseID,'')+': '+ CASE WHEN len(c.title_defaultTranslation) > 100
			THEN left(c.title_defaultTranslation,30) +  ' ... ' + right(c.title_defaultTranslation,30)
			ELSE c.title_defaultTranslation END AS Title, c.courseID,c.AICCCourseID,c.title_defaultTranslation
			from trngCourse c
				inner join trngModuleCourse tmc on tmc.courseID = c.courseID
				inner join trngModule m on tmc.moduleID = m.moduleID
			where isnull(AICCCourseID,c.title_defaultTranslation)  like  <cf_queryparam value="#arguments.startsWith#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				and m.availability = 1
				<cfif arguments.excludeCourses neq "">
				and c.courseID not in (<cf_queryparam value="#arguments.excludeCourses#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
				</cfif>
				and c.active=1
			order by AICCCourseID,c.title_defaultTranslation
		</cfquery>

 		<cfreturn courseQuery>
	</cffunction>

	<!--- NJH 2015/08/12 Jira PROD2015-19 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="addRemoveUsersToTrainingPath" access="public" returnType="string">
		<cfargument name="addPersonIDs" required="true">
		<cfargument name="removePersonIDs" required="true">
		<cfargument name="TrainingPathID" type="numeric" required="true">

		<cfset var result = application.com.trainingPath.addRemoveUsersToTrainingPath(argumentCollection=arguments)>
		<cfset var addRemoveUsersToTrainingPath = "">
		<cfset var type = result.isOk?"successblock":"errorblock">

		<cfsavecontent variable="addRemoveUsersToTrainingPath">
			<cfoutput>
				#application.com.relayui.message(message=result.message,type=type)#
			</cfoutput>
		</cfsavecontent>

		<cfreturn addRemoveUsersToTrainingPath>
	</cffunction>

	<!--- NJH 2015/08/12 Jira PROD2015-19 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="getTrainingPathInfo" access="public" returnType="string">
		<cfargument name="TrainingPathID" type="numeric" required="true">

		<cfset var getTrainingPathInfo = application.com.trainingPath.getTrainingPathList(TrainingPathID = arguments.TrainingPathID)>
		<cfset var TrainingPathInfo = "">
		<cfset var getModulesInfo = "">

		<cfsavecontent variable="TrainingPathInfo">
			<cfoutput>
				<div id="getTrainingPathInfo#arguments.TrainingPathID#" class="modalWindow">
					<h2 class="TrainingPath">#htmlEditFormat(getTrainingPathInfo.TrainingPath[1])#</h2>
					<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed" id="trainingPathTable">
					<cfloop query="getTrainingPathInfo">
						<thead>
							<tr>
								<th class="courseTh" colspan="2">phr_elearning_TrainingPath_courseTitle #htmlEditFormat(courseTitle)#</th>
							</tr>
						</thead>
							<cfif isDefined("courseID") and courseID NEQ "">
								<cfset getModulesInfo = application.com.relayelearning.GetTrainingModulesData(courseID=courseID)>
									<tr>
										<td class="TrainingPathModulesTitle courseSubTH">phr_elearning_TrainingPath_moduletitle</td>
										<td class="TrainingPathModulesType courseSubTH">phr_elearning_TrainingPath_moduleType</td>
									</tr>
									<cfloop query="getModulesInfo">
										<tr>
											<td class="TrainingPathModulesTitle">#htmlEditFormat(Title_Of_Module)#</td>
											<td class="TrainingPathModulesType">#htmlEditFormat(module_type)#</td>
										</tr>
									</cfloop>
							</cfif>
					</cfloop>
					</table>
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn TrainingPathInfo>
	</cffunction>


	<!--- 24/04/2014 YMA Lenovo CR 434305 Suggest a TrainingPath path for a user to follow.
		NJH 2015/08/11 - This looks very similiar to other functions that we have. It needs to be factorised!!!! Don't have time now to do it but it should be done!
		WAB 2016-05-18	BF-712. This query was appying country visibility incorrectly.  Not dealing with countryGroups
		 --->
	<cffunction name="getCourseModulesWithProgress" access="public" returnType="struct">
		<cfargument name="personID" type="numeric" required="true" encrypted="true">
		<cfargument name="courseID" type="numeric" required="true">
		<cfargument name="OrderMethod" type="string" default="Random" required="false">
		<cfargument NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">
		<cfargument NAME="showIncorrectQuestionsOnCompletion" DEFAULT="false">
		<cfargument NAME="reviewAnswersLink" DEFAULT="false">
		<cfargument NAME="moduleSortOrder" DEFAULT="moduleTitle">
		<cfargument name="showExpired" type="boolean" default="false" /><!--- IH 2015-11-24 BRD31 --->

		<cfscript>
			var certificationItemsDisplay_html = "";
			var getPersonCertificationData = "";
			var courseFileURL="";
			var urlAdditionalParams = "";
			var result = {content=""};
			var argumentsStruct = {};
			var addBR = true;
		</cfscript>

		<cfset var fromActivationDate = isDefined("request.relaycurrentuser.content.date") and isDate(request.relaycurrentuser.content.date)?request.relaycurrentuser.content.date:request.requestTime>
		<cfset var titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngMODULE", phraseTextID = "title", baseTableAlias="v")>

		<cfquery name="getPersonCertificationData" datasource="#application.siteDataSource#">
			select distinct v.moduleID, isNull(tump.userModuleProgressID,0) as userModuleProgressID, v.module_Code, tump.userModuleFulfilled as userModuleFulfilled,
				isNull(tums.statusTextID,'NotStarted') as userModuleStatus, title_of_module,v.moduleID,
				tump.personID, qd.active as quizActive,tump.startedDateTime as started, tump.lastVisitedDateTime as lastVisited,
				v.quizDetailID,fileID = case when f.fileID is null then 0 else f.fileID end,v.module_type,v.fileName,
				(select top 1 quizTakenID from quizTaken with (noLock) where userModuleProgressID = tump.userModuleProgressID order by lastUpdated desc) as lastQuizTakenID,
				(select top 1 eventGroupID from eventDetail with (noLock)
					where moduleID = v.moduleID and (
						(eventstatus = 'Agreed')
							or
						(flagid in (select distinct flagid from eventflagdata where regstatus in ('RegistrationStarted','RegistrationSubmitted','confirmed')
							and
						 eventstatus <> 'initial' and entityid = tump.personid)
						)
					)
				) as eventGroupID,
				(select top 1 regStatus from eventDetail with (noLock) inner join eventFlagData with (noLock) on eventFlagData.flagID = eventDetail.flagID where eventFlagData.entityID = tump.personID and eventDetail.moduleID=v.moduleID) as regStatus,
				#preservesinglequotes(titlePhraseQuerySnippets.select)# as moduleTitle,
				secure = case when ft.secure is null then 0 else ft.secure end,
				case when ft.secure = 1 then '#application.paths.securecontent#' else '#application.paths.content#' end + '\'+ isNull(ft.path,'') as absolutePath,
				'/content/' + isNull(ft.path,'') as relativePath, v.SortOrder
				,isnull(qd.NumberOfAttemptsAllowed,0) as NumberOfAttemptsAllowed
				,isnull(tump.QuizAttempts,0) as QuizAttempts
			from vTrngModules v
				left join trngmodulesetmodule tms on v.moduleID = tms.moduleID
				left join trngUserModuleProgress tump with (noLock)	on v.moduleID = tump.moduleID and tump.personID = <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				left join trngUserModuleStatus tums with (noLock) on tump.modulestatus = tums.statusID
				left join quizDetail qd with (noLock) on qd.quizDetailID = v.quizDetailID
				left outer join (files f with (noLock)
					inner join fileType ft with (noLock) on f.fileTypeID=ft.fileTypeID) on v.fileID = f.fileID
				#preservesinglequotes(titlePhraseQuerySnippets.join)#
			where v.courseID = <cf_queryparam value="#arguments.courseID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and dbo.dateAtMidnight(v.publishedDate) <= #createODBCDateTime(fromActivationDate)#
				<cfif not arguments.showExpired>
					and (v.expiryDate is null or v.expiryDate > GETDATE())
				</cfif>
				and (
						v.countryID is null 
							or 
						v.countryID = 0
							or
						#request.relayCurrentUser.countryId# in (select CountryMemberID from CountryGroup where CountryGroupID = v.countryId) 
					)
			order by v.SortOrder
		</cfquery>

		<cfsavecontent variable="certificationItemsDisplay_html">
			<cf_translate>
				<div id="personCertificationDataDiv">
				<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder" id="personCertificationDataTable">
					<cfif (getPersonCertificationData.recordCount gt 1) or (getPersonCertificationData.recordCount eq 1 and getPersonCertificationData.moduleID neq "")>
						<thead>
							<tr>
								<th class="2"  valign="top">phr_elearning_moduleCode</th>
								<th class="3" valign="top">phr_elearning_moduleTitle</th>
								<th class="4"  valign="top">phr_elearning_moduleType</th>
								<th class="5" valign="top">phr_elearning_actions</th>
							</tr>
						</thead>
						<cfset var qryModuleStatuses = application.com.relayElearning.getUserModuleStatus()>
						<cfset var updatableModuleFound = false>

						<cfoutput query="getPersonCertificationData">

							<cfset var colCount = 0>
							<cfset var rowspan = 0>

							<cfset rowspan = rowspan + 1>
							<cfset colCount = colCount + 1>
							<cfif colCount gt 1>
								<tbody>
								<tr>
							</cfif>
								<td class="2" valign="top">#htmlEditFormat(module_Code)#</td>
								<td class="3" valign="top">#htmlEditFormat(moduleTitle)#</td>
								<td class="4" valign="top">#htmlEditFormat(module_type)#</td>
								<td class="5" valign="top">

									<cfif moduleID neq "" and moduleID neq 0>
										<cfset courseFileURL = "">
										<cfset argumentsStruct = {moduleID=moduleID,userModuleProgressID=userModuleProgressID,personID=personID,fileID=fileID,filename=fileName,absolutePath=absolutePath,relativePath=relativePath,secure=secure}>
										<cfset courseFileURL = application.com.relayElearning.getCourseLauncherUrl(argumentCollection=argumentsStruct)>
									</cfif>
									<cfif userModuleProgressID neq "" and userModuleProgressID neq 0>
										<cfset urlAdditionalParams='&userModuleProgressID=#userModuleProgressID#'>
										<cfif eventGroupID neq "" and eventGroupId neq 0>
											<cfset urlAdditionalParams = urlAdditionalParams & '&moduleID=#moduleID#'>
										</cfif>
									<cfelse>
										<cfset urlAdditionalParams='&moduleID=#moduleID#&personID=#personID#'>
									</cfif>

									<cfif personID neq request.relayCurrentUser.personID or not request.relayCurrentUser.isLoggedIn>
										phr_elearning_#userModuleStatus#
									<cfelseif listFindNoCase("InProgress,NotStarted",userModuleStatus) or (userModuleStatus eq "Failed" and NumberOfAttemptsAllowed gt QuizAttempts)>
										<cfset addBR = false>
										<cfif listFindNoCase("OnLine,On-Line",module_type) and courseFileURL neq "">
											<p><a href="javascript:void(openWin('#courseFileURL#','Training#courseID##moduleID#','width=#application.com.settings.getSetting('files.maxPopupWidth')#,height=#application.com.settings.getSetting('files.maxPopupHeight')#,scrollbars=no,status=no,address=no,resizable=1'));" class="btn btn-primary">phr_elearning_studyNow</a></p>
											<cfset addBR = true>
										<cfelseif listFindNoCase("Classroom,Webinar",module_type) and eventGroupID neq "">
											<cfif regStatus eq "">
												<cfif addBR></cfif><p><a href="javascript:void(openWin('/relayTagTemplates/eventList.cfm?eventGroupID=#eventGroupID##urlAdditionalParams#','TrainingEvent','width=600,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=0'));" class="btn btn-primary">phr_elearning_registerNow</a></p>
											<cfelse>
												<cfif addBR></cfif><p>phr_elearning_#regStatus#</p>
											</cfif>
											<cfset addBR = true>
										</cfif>
										<cfif QuizDetailID neq "" and QuizDetailID neq 0 and quizActive>
											<cfif addBR></cfif>
											<cfset var encryptedLink = application.com.security.encryptURL('/Quiz/Start.cfm?QuizDetailID=#QuizDetailID#&stringencyMode=#arguments.stringencyMode#&OrderMethod=#arguments.OrderMethod#&showIncorrectQuestionsOnCompletion=#arguments.showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#arguments.reviewAnswersLink##urlAdditionalParams#')>
											<p><a href="#encryptedLink#" class="takeQuizURL btn btn-primary">phr_elearning_takeQuiz</a></p>
										</cfif>
									<cfelseif userModuleStatus eq "Passed" and listFindNoCase("Classroom,Webinar",module_type)>
										<cfif userModuleStatus eq "NotStarted">
											<p>phr_elearning_#regStatus#</p>
										<cfelse>
											<p>phr_elearning_#userModuleStatus#</p>
										</cfif>
									<cfelse>
										<p>phr_elearning_#userModuleStatus#</p>
									</cfif>
								</td>
							<cfif colCount gt 1>
							</tr>
							</tbody>
							</cfif>
							</tr>
							</tbody>
						</cfoutput>
					<cfelse>
						<tr>
							<td>phr_elearning_NoModuleHaveBeenStartedForCertification.</td>
						</tr>
					</cfif>
				</table>
				</div>
			</cf_translate>
		</cfsavecontent>

		<cfset result.content = certificationItemsDisplay_html>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2015/08/12 Jira PROD2015-19 Suggest a TrainingPath path for a user to follow. --->
	<cffunction name="getPersonCourseSummary" access="public" returnType="struct">
		<cfargument name="personID" type="numeric" required="true" encrypted="true">
		<cfargument name="courseID" type="numeric" required="true">

		<cfset var courseDataStruct = application.com.structureFunctions.queryRowToStruct(query=application.com.relayElearning.GetTrainingCoursesDataV2(courseID=arguments.courseID,personid=arguments.personID,sortorder="Title_Of_Course",active=true),row=1)>

		<cfloop collection="#courseDataStruct#" item="key">
			<cfif isDate(courseDataStruct[key])>
				<cfset courseDataStruct[key] = lsdateFormat(courseDataStruct[key],"medium")>
			</cfif>
		</cfloop>

		<cfreturn courseDataStruct>
	</cffunction>

    <!--- Case 450727 --->
    <cffunction name="isPreviousCertificationRuleFulfilled" access="remote" returntype="boolean" hint="Check if the previous certification rule is fulfilled for the person certification">
        <cfargument name="personCertificationID" type="numeric" required="true">
        <cfargument name="certificationRuleOrder" type="numeric" required="true">

        <cfset var qryRuleModulesFulfilled = "">
        <cfquery name="qryRuleModulesFulfilled" datasource="#application.siteDataSource#">
            select numModulesToPass, userModuleFulfilled from vTrngPersonCertifications v where 
                personCertificationID = <cf_queryparam value="#arguments.personCertificationID#" CFSQLTYPE="CF_SQL_INTEGER">
                    and certificationRuleOrder = (select max(certificationRuleOrder) from vTrngPersonCertifications 
                        where personCertificationID = <cf_queryparam value="#arguments.personCertificationID#" CFSQLTYPE="CF_SQL_INTEGER">
                        and certificationRuleOrder < <cf_queryparam value="#arguments.certificationRuleOrder#" CFSQLTYPE="CF_SQL_INTEGER">
                    )
        </cfquery>

        <cfif qryRuleModulesFulfilled.recordCount EQ 0>
            <cfreturn true> <!--- no previous rule --->
        </cfif>

        <cfset var numModulesToPass = qryRuleModulesFulfilled.numModulesToPass[1]>
        <cfset var moduleCount = 0>

        <cfloop query="qryRuleModulesFulfilled">
            <cfif qryRuleModulesFulfilled.userModuleFulfilled NEQ "">
                <cfset moduleCount += 1>
            </cfif>
        </cfloop>

        <cfreturn moduleCount GTE numModulesToPass>
    </cffunction>

</cfcomponent>

