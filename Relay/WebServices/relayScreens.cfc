<!--- �Relayware. All Rights Reserved 2014 --->

<!---

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2014-08-11			RPW Create new profile type: "Financial"


 --->

<cfcomponent>

	<cffunction name="callMethod" access="remote">
		<cfargument name="methodName" type="string" required="true">

		<cfset var result = "">

		<cfif request.relayCurrentUser.isLoggedIn>
			<cfset result = evaluate("#arguments.methodName#(argumentCollection=arguments)")>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="entityListAndEditScreenFields" access="remote">
		<cfargument name="entityTypeID">
		<cfargument name="forDateFormat">
		<cfargument name="unSelected">
		<cfargument name="selected">

		<cfset var result = queryNew("groupvalue,displayvalue,datavalue")>

		<CFQUERY NAME="result" DATASOURCE="#application.SiteDataSource#">
			SELECT st.entityName as groupvalue, st.entityName + '.' + column_name as displayvalue, st.entityName + '.' + column_name as datavalue
			FROM INFORMATION_SCHEMA.COLUMNS isc
			JOIN schematable st on isc.table_name = st.entityName
			WHERE st.entitytypeID = <cf_queryparam value="#arguments.entityTypeID#"  CFSQLTYPE="CF_SQL_INTEGER">
			<cfif structKeyExists(arguments,"forDateFormat") and arguments.forDateFormat>and isc.data_type = 'datetime'</cfif>

			UNION

			select 'Flags' as groupvalue, 'flag.' + fg.name + '.' + f.name as displayvalue, 'flag_' + cast(f.flagID as varchar) as datavalue from flag f
			join flaggroup fg on f.flaggroupID = fg.flaggroupID and fg.entityTypeID = <cf_queryparam value="#arguments.entityTypeID#"  CFSQLTYPE="CF_SQL_INTEGER">
			where f.active = 1 and fg.active = 1
			<cfif structKeyExists(arguments,"forDateFormat") and arguments.forDateFormat>and fg.flagtypeID = 4</cfif>
		</CFQUERY>

		<!--- 2013-08-20	YMA	Case 436441 Improvements to webservice to call entity fields to avoid browsers hanging --->
		<cfif not structKeyExists(arguments,"list")>
			<cfif structKeyExists(arguments,"selected")>
				<cfquery name="result" dbtype="query">
					select * from result
					where datavalue in (<cf_queryparam value="#arguments.selected#" CFSQLTYPE="CF_SQL_VARCHAR" LIST="true" isQoQ="true">)
				</cfquery>
			</cfif>

			<cfif structKeyExists(arguments,"unSelected")>
				<cfquery name="result" dbtype="query">
					select * from result
					where datavalue not in (<cf_queryparam value="#arguments.unSelected#" CFSQLTYPE="CF_SQL_VARCHAR" LIST="true" isQoQ="true">)
				</cfquery>
			</cfif>
		<cfelse>
			<cfif structKeyExists(arguments,"selected")>
				<cfquery name="result" dbtype="query">
					select * from result
					where datavalue not in (<cf_queryparam value="#arguments.selected#" CFSQLTYPE="CF_SQL_VARCHAR" LIST="true" isQoQ="true">)
				</cfquery>
			</cfif>

			<cfif structKeyExists(arguments,"unSelected")>
				<cfquery name="result" dbtype="query">
					select * from result
					where datavalue in (<cf_queryparam value="#arguments.unSelected#" CFSQLTYPE="CF_SQL_VARCHAR" LIST="true" isQoQ="true">)
				</cfquery>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="entityScreens" access="remote">
		<cfargument name="entityTypeID">

		<cfset var result = queryNew("displayvalue,datavalue,screenID")>

		<cfif arguments.entityTypeID neq "">
			<CFQUERY NAME="result" DATASOURCE="#application.SiteDataSource#">
				SELECT ScreenName + ' (' + cast(screenID as varchar) + ')' as displayvalue, screenTextID as datavalue, screenID
				FROM screens s
				WHERE s.entityTypeID = <cf_queryparam value="#arguments.entityTypeID#"  CFSQLTYPE="CF_SQL_INTEGER">
			</CFQUERY>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="deleteEntityRow" access="remote" returnType="struct">
		<cfargument name="entityName" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="uniqueKey" type="string" required="true">

		<cfset var getFlagsForEntityType="">
		<cfset var flags="">
		<cfset var result=structNew()>

		<cfset result.isOk = true>

		<cfquery name="getFlagsForEntityType" DATASOURCE="#application.SiteDataSource#">
			select flagID from flag f
			join flaggroup fg on f.flaggroupID = fg.flaggroupID
			where fg.entityTypeID =  <cf_queryparam value="#arguments.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>


			<cf_transaction action="begin">

			<cfloop query="getFlagsForEntityType">
				<cfset application.com.flag.deleteFlagData(entityID=#entityID#,flagID=#flagID#)>
			</cfloop>

			<cftry>
			<cfquery name="deleteRow" DATASOURCE="#application.SiteDataSource#">
				delete from #arguments.entityName# where #arguments.uniqueKey# =  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfcatch type="any">
				<cfset result.isOK = false>
				<CFSET result.message = cfcatch.message>
				<cf_transaction action="rollback">

				<cfset var errorID = application.com.errorHandler.recordRelayError_Warning(type="RelayScreens",Severity="error",catch=cfcatch,WarningStructure=arguments)>

				<cfif structKeyExists(cfcatch.cause,"message") and find("REFERENCE constraint",cfcatch.cause.message)>
					<CFSET result.message = application.com.relaytranslations.translatephrase(phrase="phr_deleteChildRecordsFirst")>
				</cfif>
			</CFCATCH>
			</cftry>
			<cfif result.isOk eq true>
				<cfset result.isOk = true>
				<cfset result.message = "Row deleted.">
				<cf_transaction action="commit">
				<cfset result.isOk = true>
				<cfset result.message = "Row deleted.">
			</cfif>
			</cf_transaction>

		<cfreturn result>
	</cffunction>

	<cffunction name="searchBindFunction" access="remote" returntype="array" output="false" loginRequired=2 internalUser="true">
		<cfargument name="function" type="string" default="">
		<cfargument name="display" type="string" required="true" default="">
		<cfargument name="value" type="string" required="true" default="">
		<cfargument name="term" type="string" required="true">

		<cfset var getSearchResults = queryNew("#display#,#value#")>
		<cfset var entityArray = arrayNew(1)>
		<cfset var jsonString = "">
		<cfset var rowStruct = "">
		<cfset var functionStruct = application.com.relayForms.getFunctionPointerFromString(function=arguments.function)>
		<cfset var termExistsInFunction = false>

		<cfscript>
			arrayEach(functionStruct.metadata.parameters, function(paramStruct){
				WriteDump(var="#paramStruct#" output="c:\dump.html" format="html")
				if (structKeyExists(paramStruct,"name") and paramStruct["name"] eq "term"){
					termExistsInFunction = true;
					break;
				}
			});
		</cfscript>

		<cfif termExistsInFunction eq true>
			<cfset getEntityNames = application.com.relayForms.runValidValueFunction (function = arguments.function, term = arguments.term).query>

			<cfscript>
				for(row in getEntityNames){

					arrayAppend(entityArray,{"value" = row[arguments.value],"label"= row[arguments.display]});
				}
			</cfscript>

			<cfreturn entityArray>
		<cfelse>
			<cfset application.com.errorHandler.recordRelayError_Warning(severity="Error",Type="Function Arguments Error",Message="Your function arguments must include 'Term'  use this to filter the query results.")>
			<cfreturn arrayNew(1)>
		</cfif>

	</cffunction>

	<cfscript>
		// 2014-08-11			RPW Create new profile type: "Financial"
		remote struct function GetCurrencies
		(

		)
		{

			var rtnStruct = {};
			var currencyArray = [];

			var getCurrencies = application.com.screens.getCurrencies();

			for (var c=1;c <= getCurrencies.recordCount;c++) {

				var tmpStruct = {};
				tmpStruct['currencyISOCode'] = getCurrencies.currencyISOCode[c];
				tmpStruct['currencyName'] = getCurrencies.currencyName[c];
				tmpStruct['currencySign'] = getCurrencies.currencySign[c];
				ArrayAppend(currencyArray,tmpStruct);

			}

			rtnStruct.Currencies = currencyArray;

			return rtnStruct;

		}

	</cfscript>

</cfcomponent>