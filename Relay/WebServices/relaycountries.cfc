<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayCountries.cfc
Author:
Date started:	17-04-2009

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/04/17			NJH			Sophos Stargate 2 Added function getLanguageForCountry
2011/09/09			PPB			LID7366 allowed specification of dataValueColumn/displayValueColumn from getProvinceList for use by dropdown
2012-10-22 			PPB 		Case 431058 pass useLocalLanguage thro getCountryLanguages
2015-02-10			RPW			Leads - Added ability to customize leads form for customers.

Possible enhancements:

 --->

<cfcomponent output="false">
<!--- ALL FUNCTIONS IN THIS CFC ARE AVAILABLE GLOBALLY - ENSURE SECURITY RISKS OF NEW FUNCTIONS ARE CONSIDERED --->
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Retrieves a list of provinces for the countryID
			<!--- SWJ: 30-Apr-08 Added to provide for the dynamic bind list in oppEdit --->
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="remote" name="getProvinceList" hint="Retrieves a list of provinces for the countryID." returntype="query">
		<cfargument name="CountryID" required="no" type="numeric" hint="provide this to get the Provinces or States in a Country" >
		<cfargument name="dataValueColumn" required="no" type="string" hint="" default="provinceID" >
		<cfargument name="displayValueColumn" required="no" type="string" hint="" default="state" >

		<cfset var result = application.com.relaycountries.getProvinceList(countryid=CountryID,dataValueColumn=dataValueColumn,displayValueColumn=displayValueColumn)>

 		<cfreturn result/>
	</cffunction>

	<!--- 2015-02-10	RPW	Leads - Added ability to customize leads form for customers. Added funtion getProvinceListJSON --->
	<cfscript>
		remote struct function getProvinceListJSON
		(
			required numeric countryID
		)
		returnFormat = "JSON"
		{
			var response = {};

			var qGetProvinces = application.com.relaycountries.getProvinceList(countryid=arguments.CountryID);

			var items = [];
			for (var i=1;i <= qGetProvinces.recordCount;i++) {
				var province = {};
				province["state"] = qGetProvinces.state[i];
				province["stateAbbreviation"] = qGetProvinces.stateAbbreviation[i];

				ArrayAppend(items,province);
			}

			response["provinces"] = items;

			return response;

		}
	</cfscript>

	<!--- NJH 2009/04/17 - Sophos Stargate 2 function to return the language of a country --->
	<cffunction name="getLanguageForCountry" access="remote">
		<cfargument name="countryID" type="numeric" required="yes">

		<cfscript>
			var result = structNew();
			var languageIDs = "";
		</cfscript>

		<cfset languageIDs = application.com.relayTranslations.getCountryLanguages(arguments.countryID)>
		<cfset result.languageIDs=languageIDs>
		<cfset result.noOfLanguages = listLen(languageIDs)>
		<cfreturn result>

	</cffunction>

	<cffunction name="getCountryLanguages" access="remote">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="useLocalLanguage" type="Boolean" required="false" default="false">		<!--- 2012-10-22 PPB Case 431058 pass useLocalLanguage --->

		<cfreturn application.com.relayCountries.getCountryLanguages(arguments.countryID,arguments.useLocalLanguage)>		<!--- 2012-10-22 PPB Case 431058 pass useLocalLanguage --->
	</cffunction>

	<cffunction name="getCurrenciesForCountry" access="remote" returntype="query">
		<cfargument name="countryID" type="string" required="yes">

		<cfif not isNumeric(arguments.countryID)>
			<cfset arguments.countryId = 0>
		</cfif>
		<cfreturn application.com.relayCountries.getCountryCurrencies(countryID=arguments.countryID)>
	</cffunction>

</cfcomponent>
