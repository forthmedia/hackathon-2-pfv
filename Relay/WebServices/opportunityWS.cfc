/*
2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes
*/

component {

	remote string function getRelatedFiles
	(
		required numeric opportunityID
	)
	{
		var rtnStr = "";
		SaveContent variable="rtnStr" {include "/LeadManager/opportunityEdit_RelatedFiles.cfm";}
		
		return rtnStr;
	}


}