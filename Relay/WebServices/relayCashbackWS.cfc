<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayCashbackWS.cfc
Author:			NJH
Date started:	05-03-2009

Description:	Cashback webservices

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
12-05-2009			NJH			CR-SNY675-1 - changed some column/variable names
19-05-2009			NJH			P-SNY069 - added phrase prefix's to the claims display page
15-07-2009			NJH			P-FNL069 check user's rights to access this webservice/cashbackClaim and encrypt url variables to entityRelatedFilePopUp call
06-11-2009			NJH			P-SNY086 - added additionalReference column to cashback items display
Possible enhancements:


 --->

<cfcomponent>

	<cfif not request.relayCurrentUser.isLoggedIn>
		<script>alert('You must be logged in')</script>
		<CF_ABORT>
	</cfif>

	<!--- NJH 2009/07/02 P-FNL069 - check if user has rights to access this cashbackClaim --->
	<cfif isDefined("cashbackClaimID")>
		<cfif not application.com.rights.doesUserHaveRightsForEntity(entityType="cashbackClaim",entityID=cashbackClaimID,permission="view")>
			<CF_ABORT>
		</cfif>
	</cfif>

	<!--- NJH 2009/03/05 CR-SNY675 --->
	<!--- NJH 2009/05/12 CR-SNY675-1 changed serialNo and invoiceNo to serialNumber and invoiceNumber --->
	<!--- NJH 2009/11/06 P-SNY086 - added additionalReference field --->
	<cffunction name="getCashbackClaimItemsDisplay" access="remote">
		<cfargument name="CashbackClaimID" required=true>
		<cfargument name="showCols" type="string" default="ProductGroup,Product,SerialNumber,InvoiceNumber,EndUserCompanyName,UnitValue,Quantity,Value,AdditionalReference">
		<cfargument name="showInvoiceLink" type="boolean" default="true">

		<cfscript>
			var invDocumentQry = "";
			var cashbackItemsDisplay_html = "";
			var cashbackItems = "";
			var result = structNew();
			var showColsStruct = structNew();

			cashbackItems = application.com.relayCashback.getCashbackClaimItems(CashbackClaimID = arguments.CashbackClaimID);
			showColsStruct = {productGroup=false,product=false,serialNumber=false,invoiceNumber=false,endUserCompanyName=false,unitValue=false,quantity=false,value=false,additionalReference=false};

			// NJH 2009/03/02 CR-SNY671
			if (arguments.showInvoiceLink) {
				invDocumentQry = application.com.relayIncentive.getInvoiceDocumentDetails(entityID=arguments.CashbackClaimID,entityType="CashbackClaim");
			}
		</cfscript>

		<cfloop list="#arguments.showCols#" index="column">
			<cfset showColsStruct[column] = true>
		</cfloop>

		<cfsavecontent variable="cashbackItemsDisplay_html">
			<cf_translate>

				<table id="relayCashbackTable2" class=" table table-striped table-hover" data-mode="reflow" data-role="table">
					<thead>
						<tr>
							<cfif showColsStruct.ProductGroup>
								<th align="left">phr_cashback_report_ProductGroup</th>
							</cfif>
							<cfif showColsStruct.Product>
								<th align="left">phr_cashback_report_Product</th>
							</cfif>
							<cfif showColsStruct.SerialNumber>
								<th align="left">phr_cashback_report_serialNo</th>
							</cfif>
							<cfif showColsStruct.additionalReference>
								<th align="left">phr_cashback_report_additionalReference</th>
							</cfif>
							<cfif showColsStruct.InvoiceNumber>
								<th align="left">phr_cashback_report_Invoice</th>
							</cfif>
							<cfif showColsStruct.EndUserCompanyName>
								<th align="left">phr_cashback_report_EndUser</th>
							</cfif>
							<cfif showColsStruct.UnitValue>
								<th align="left">phr_cashback_report_UnitValue</th>
							</cfif>
							<cfif showColsStruct.Quantity>
								<th align="left">phr_cashback_report_Quantity</th>
							</cfif>
							<cfif showColsStruct.Value>
								<th align="left">phr_cashback_report_Value</th>
							</cfif>
						</tr>
					</thead>
					<cfoutput>
					<cfif cashbackItems.recordCount>
						<cfloop query="cashbackItems">
							<tr>
								<cfif showColsStruct.ProductGroup>
									<td>#htmleditformat(productGroupDescription)#</td>
								</cfif>
								<cfif showColsStruct.Product>
									<td>#htmleditformat(SKU)#<br>(#htmleditformat(Description)#)</td>
								</cfif>
								<cfif showColsStruct.SerialNumber>
									<td>#htmleditformat(serialNumber)#</td>
								</cfif>
								<!--- NJH 2009/11/06 P-SNY086 - add custom field --->
								<cfif showColsStruct.additionalReference>
									<td>#htmleditformat(additionalReference)#</td>
								</cfif>
								<cfif showColsStruct.InvoiceNumber>
									<td>
										<cfif arguments.showInvoiceLink and invDocumentQry.recordCount neq 0>
											<a href="#invDocumentQry.webhandle#" target="_blank">#htmleditformat(invoiceNumber)#</A>
										<cfelse>
											#htmleditformat(invoiceNumber)#
										</cfif>
									</td>
								</cfif>
								<cfif showColsStruct.EndUserCompanyName>
									<td>#htmleditformat(endUserCompanyName)#</td>
								</cfif>
								<cfif showColsStruct.UnitValue>
									<td>#htmleditformat(discountPrice)#</td>
								</cfif>
								<cfif showColsStruct.Quantity>
									<td>#htmleditformat(quantity)#</td>
								</cfif>
								<cfif showColsStruct.Value>
									<td>#htmleditformat(value)#</td>
								</cfif>
							</tr>
						</cfloop>
					<cfelse>
						<tr><td colspan="6">phr_cashback_ThereAreNoItemsForThisCashbackClaim.</td></tr>
					</cfif>
				</cfoutput>
				</table>
			</cf_translate>
		</cfsavecontent>

		<cfset result.content = cashbackItemsDisplay_html>

		<cfreturn result>

	</cffunction>



	<cffunction name="addCashbackClaimItem" access="remote">
		<cfargument name="CashbackClaimID" required=true>
		<cfargument name="ProductID" type="numeric" required="true">
		<cfargument name="quantity" type="numeric" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="distiID" default="0">
		<cfargument name="serialNo" type="string" default="">
		<cfargument name="invoiceNo" type="string" default="">
		<cfargument name="invoiceDate" type="string" default="">
		<cfargument name="endUserCompanyName" type="string" default="">
		<cfargument name="additionalReference" type="string" default=""> <!--- NJH 2009/11/06 P-SNY086 --->

		<cfscript>
			if (not isNumeric(distiID)) {
				arguments.distiID = 0;
			}
			application.com.relayCashback.AddCashbackClaimItem(argumentCollection=arguments);
		</cfscript>

		<CF_ABORT>

	</cffunction>


	<cffunction name="removeCashbackClaimItem" access="remote">
		<cfargument name="CashbackClaimID" required=true>
		<cfargument name="CashbackClaimItemID" required=true>

		<cfscript>
			application.com.relayCashback.removeCashbackClaimItems(CashbackClaimID=arguments.CashbackClaimID,CashbackClaimItemIDList=arguments.CashbackClaimItemID);
		</cfscript>

		<CF_ABORT>

	</cffunction>

	<cffunction name="getCashbackClaimItems" access="remote">
		<cfargument name="CashbackClaimID" required=true>
		<cfargument name="productID" type="numeric" required="false">
		<cfargument name="serialNo" type="string" required="false">

		<cfscript>
			var cashbackItems = "";
			var result = structNew();

			cashbackItems = application.com.relayCashback.getCashbackClaimItems(argumentCollection=arguments);
		</cfscript>

		<cfreturn application.com.structureFunctions.QueryToArrayOfStructures(query=cashbackItems)>
		<!--- <cfoutput>#serializeJSON(application.com.structureFunctions.QueryToArrayOfStructures(query=cashbackItems))#</cfoutput>

		<CF_ABORT> --->

	</cffunction>


	<!--- NJH 2009/03/05 CR-SNY675 --->
	<!--- NJH 2009/05/19 P-SNY069 - added phrasePrefix to the phrases--->
	<cffunction name="getCashbackClaimItemsDisplayForClaimPage" access="remote">
		<cfargument name="CashbackClaimID" required=true>
		<cfargument name="readOnly" type="boolean" default="false">
		<!--- <cfargument name="uploadClaimInvoice" type="boolean" default="true"> --->
		<cfargument name="action" type="string" default="view">
		<cfargument name="phrasePrefix" type="string" default=""> <!--- NJH 2009/05/19 P-SNY069 - added phrasePrefix --->

		<cfscript>
			var invDocumentQry = "";
			var cashbackItemsDisplay_html = "";
			var cashbackItems = "";
			var result = structNew();

			cashbackClaimItems = application.com.relayCashback.getCashbackClaimItems(CashbackClaimID = arguments.CashbackClaimID,orderBy="cashbackClaimItemID desc");
		</cfscript>

		<cfsavecontent variable="cashbackItemsDisplay_html">
			<cf_translate>
				<cfif cashbackClaimItems.recordCount>
					<table id="relayCashbackTable1" class="responsiveTable table table-striped table-hover table-condensed" data-mode="reflow" data-role="table">
						<thead>
							<tr>
								<cfoutput>
								<th>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#ProductGroup</th>
								<th>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#Product</th>
								<th>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#unitValue</th>
								<th>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#quantity</th>
								<th>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#TotalValue</th>
								<cfif not arguments.readOnly>
									<th>phr_cashback_claim_#htmleditformat(arguments.phrasePrefix)#deleteItem</th>
								</cfif>
								</cfoutput>
							</tr>
						</thead>
					<cfoutput>
					<tbody>
						<cfset totalValueLocal = 0>
						<cfset localClass="evenrow">
						<cfloop query="cashbackClaimItems">
							<cfset totalValueLocal = totalValueLocal+value>
							<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
							<TR class="#localClass#" id="claimItemID#cashbackClaimItemID#">
								<td>#htmleditformat(productGroupDescription)#</td>
								<td>
								#htmleditformat(sku)# <cfif sku is not ""><br></cfif>
								(<cfif distiOrganisationID gt 0>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#Distributor: #htmleditformat(application.com.relayIncentive.getDistiName(distiOrganisationID))#<br></cfif>
									<cfif isdefined('serialNumber') and len(serialNumber)>phr_cashback_claim_#htmleditformat(arguments.phrasePrefix)#SerialNumber: #htmleditformat(serialNumber)#<br></cfif>
									<!--- NJH 2009/11/06 - P-SNY086 added additionalReference if it exists  --->
									<cfif isdefined("additionalReference") and len(additionalReference)>phr_cashback_claim_#htmleditformat(arguments.phrasePrefix)#AdditionalReference: #htmleditformat(additionalReference)#<br></cfif>
									<cfif isdefined('invoiceNumber') and len(invoiceNumber)>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#InvoiceNumber: #htmleditformat(invoiceNumber)#<br></cfif>
									<cfif isdefined('invoiceDate') and len(invoiceDate)>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#InvoiceDate: #DateFormat(invoiceDate, "dd-mm-yyyy")#</cfif>)
								</td>
								<td>#htmleditformat(valuePerUnit)#</td>
								<td>#htmleditformat(quantity)#</td>
								<td>#htmleditformat(value)#</td>
								<cfif not arguments.readOnly>
									<td><CF_INPUT type="button" value="phr_cashback_Claim_#arguments.phrasePrefix#DeleteItem" onClick="eventDeleteOnClick(#arguments.CashbackClaimID#,#CashbackClaimItemID#);"></td>
								</cfif>
							</tr>
						</cfloop>

						<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>

						<TR class="#localClass#">
							<td colspan="4"><strong>phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#totalValueThisClaim</strong></td>
							<td><strong>#htmleditformat(variables.totalValueLocal)#</strong></td>
							<cfif not arguments.readOnly>
								<td>&nbsp;</td>
							</cfif>
						</tr>

						<cfif not arguments.readOnly>
							<tr>
								<td colspan="2">
									<input id="confirmVeracity" name="confirmVeracity" type="checkbox" value="1"> phr_cashback_Claim_#htmleditformat(arguments.phrasePrefix)#ConfirmTheClaim
								</td>

							</tr>

							<cfif application.com.settings.getSetting("cashback.uploadClaimInvoice")>
								<tr>
									<td colspan="6" valign="top" >
										phr_cashback_claim_#htmleditformat(arguments.phrasePrefix)#invoiceInstructions<br>
										<!--- NJH 2009/05/19 P-SNY069 added phrasePrefix --->
										<!--- NJH 2009/07/17 P-FNL069 encrypt url variables --->
										<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityType=CashbackClaim&entityID=#arguments.CashbackClaimID#&phrasePrefix=#arguments.phrasePrefix#")>
										<CF_INPUT type="button" value="phr_cashback_#arguments.phrasePrefix#uploadInvoice" onClick="javascript:void(openWin('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','InvoiceUpload','width=600,height=550,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0'));">
									</td>
								</tr>
							</cfif>

							<tr>
								<td colspan="6" valign="top" ><CF_INPUT type="button" value="phr_cashback_#arguments.phrasePrefix#SubmitTheClaim" onClick="javascript:submitClaim()"><CF_INPUT type="button" value="phr_cashback_#arguments.phrasePrefix#SubmitAndStartNewClaim" onClick="javascript:submitClaim('submitAndStartNewClaim')"></td>
							</tr>
						</cfif>
					</cfoutput>
					</table>
				</cfif>
			</cf_translate>
		</cfsavecontent>

		<cfif arguments.action eq "update">
			<cfoutput>#cashbackItemsDisplay_html#</cfoutput>
			<cfreturn>
		</cfif>

		<cfset result.content = cashbackItemsDisplay_html>
		<cfreturn result>
		<!--- <cfoutput>
				#serializeJSON(result)#
		</cfoutput>
		<CF_ABORT> --->

	</cffunction>



</cfcomponent>

