<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction access="remote" name="getUserGroupsForSelect" hint="Retrieves a list of usergroups for a select." returntype="query">
		<cfargument name="userGroupTypes" type="string" default="">
		<cfargument name="userGroupIDsToExclude" type="string" default="">
		<cfargument name="suppressGroups" type="boolean" default="false">

 		<cfreturn application.com.relayUserGroup.getUserGroupsForSelect(userGroupTypes=arguments.userGroupTypes,userGroupIDsToExclude=arguments.userGroupIDsToExclude,suppressGroups=arguments.suppressGroups)/>
	</cffunction>

	<cffunction name="NewUserGroupName" returntype="string">
		<cfargument name="NewGroupName" type="string" required="true">
		<cfargument name="frmUserGroupType" type="string" required="true">
				
		<!--- Need to cast as string for call web service's string handler that uses java regex.  Returns Type error otherwise. --->
		<cfreturn javacast ("string", application.com.relayUserGroup.createUserGroup(name=arguments.NewGroupName,type=arguments.frmUserGroupType))>
	
	</cffunction>

</cfcomponent>