<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
AsynchronousGateway.cfc
Author: WAB
2010

WAB 2011/11/02 added request.requesttime. This means that functions to extend timeouts can be used in asynchronous processes

--->


<cfcomponent displayname="RelayAsynchronousGateway" output="false" >


    <cffunction name="onIncomingMessage" output="false">
        <cfargument name="CFEvent" type="struct" required="true" />
	

		<cfset setApplication = request.applicationCFC.setApplication(cfevent.data.AppName)>	 
		<cfset applicationScope = setApplication.applicationScope>

		<!--- WAB 2011/11/02 added request.requesttime to asynchronous requests - allows us to use functions which extend requestTimeout --->
		<CFQUERY NAME="TheTime" DATASOURCE="#applicationScope.SiteDataSource#">
			SELECT GETDATE() AS Now
		</CFQUERY>

		<cfset request.requestTime = TheTime.Now> 

		<CFTRY>
			
			<!--- TODO 
				It may be that we need some sort of security here to prevent any old function being called 
				Maybe it could be a property of the method  say ASYNCH = "true"
			--->

			<CFSET theObject = applicationScope.com[cfevent.data.componentname]>
			
			<cfinvoke
				component = #theObject#
				method = #cfevent.data.methodname#
				applicationScope = #applicationScope#
				argumentCollection = #CFEvent.data#	
				returnVariable = "result"
			>

			<CFCATCH>
				<cfset warningStructure = {cfevent = cfevent}>
				<cfset structDelete (warningStructure.cfevent.data,"applicationScope")> <!--- applicationScope manages to get added to this structure and needs removed, since it is enormous --->
				<cfset fileAndLine = applicationScope.com.errorHandler.extractTemplateInfoFromErrorStructure(cfcatch)>
				<cfset applicationScope.com.errorHandler.recordRelayError_Warning(severity="Error",Type="Asynchronous Process Error",Message="Asynchronous Process Error: #cfevent.data.componentName#.#cfevent.data.methodName# #cfcatch.message# #fileAndLine.relativefilename# #fileAndLine.line#",warningStructure=warningStructure,catch=cfcatch)>
			</CFCATCH>
		
		</CFTRY>
		
	</cffunction>	
</cfcomponent>
	

