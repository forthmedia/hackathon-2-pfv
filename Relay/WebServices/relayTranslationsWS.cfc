<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent >


	<cfif not request.relayCurrentUser.isLoggedIn or not request.relayCurrentUser.isInternal>
		<cfif structKeyExists (url,"returnFormat") and url.returnFormat   IS "JSON">
			<cfset result = structNew()>
			<cfset result.isok = false>
			<cfset result.errorMessage = "You must be logged in">
			<cfoutput>#htmleditformat(serializeJSON(result))#</cfoutput>
		<cfelse>
			<script>alert('You must be logged in')</script> 
		</cfif>
		<CF_ABORT>
	</cfif>

	<cffunction name="saveTranslationsReturnNewLanguage" access="remote" returns="string">
		<cfargument name="phraseTextIDs" type="string" required="yes">		
		<cfargument name="entityTypeID" type="numeric" required="yes">		
		<cfargument name="entityID" type="numeric" required="yes">		
		<cfargument name="CountryID" type="numeric" required="yes">		
		<cfargument name="LanguageID" type="numeric" required="yes">		
		<!--- Also lots of form variables for each phrase --->

		<!--- NJH/WAB 2012/10/26 Social CR - add ability to translate non-entity phrases with the edit widget --->
		<cfset result = structNew()>
		<cfset phraseTextIDs = uCase(phraseTextIDs)>
		<cfset result.x = application.com.relayTranslations.ProcessPhraseUpdateForm()>
		<cfset result.translations = structNew()>
		<cfif entityTypeID is not 0>
			<cfset entityTranslations = application.com.relayTranslations.getAllEntityPhrases(defaultPhraseList = phraseTextIDs,entityType = entityTypeID, entityID = entityid, countryid =CountryID, languageid =LanguageID,evaluateVariables=false,exactMatch=true )>
			<cfset translatedLanguages = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase (phrasetextID = "",entityType = entityTypeID, entityID = entityid,defaultphraseTextIDList=phraseTextIDs)>	
			<cfset result.translations = entityTranslations[entityID]>
			<cfset result.translatedLanguagesStruct = application.com.structureFunctions.queryToTwoDimensionalStruct(query = translatedLanguages,key1="languageid",key2 ="countryid",valueColumn="countryid" )>
			<cfset result.otherTranslationsHTML = application.com.relayTranslations.generateTranslatedLanguagesTable (translatedLanguagesQuery = translatedLanguages, currentCountryid =CountryID, currentlanguageID =LanguageID)>
			<cfset getlanguageAndCountryName = application.com.relayTranslations.getlanguageAndCountryName(countryid  = CountryID, languageid = LanguageID,defaultForThisCountry = result.translations.defaultForThisCountry[listfirst(phraseTextIDs)])	>							

		<cfelse>
			<cfset x = application.com.relayTranslations.translateListOfPhrases(ListOfPhrases = phraseTextIDs,countryid =CountryID, languageid =LanguageID,evaluateVariables=false,exactMatch=true,onNullShowPhraseTextID=false )>
			<cfset result.translations = {updateIdentifier = structNew(),phraseIdentifier = structNew()}>
			<cfloop collection =#x.phrases# item="phraseTextID">
				<cfset result.translations[phraseTextID] = x.phrases[phraseTextID].PhraseText>
				<cfset result.translations.updateIdentifier[phraseTextID] = x.phrases[phraseTextID].updateIdentifier>
				<cfset result.translations.phraseIdentifier[phraseTextID] = phraseTextID & "_0_0">
				<cfset result.translations.defaultForThisCountry[phraseTextID] = x.phrases[phraseTextID].defaultForThisCountry>
			</cfloop>
			
			<cfset translatedLanguages = queryNew("languageid,countryid")>
			<cfset translatedLanguages = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase (phrasetextID = phraseTextIDs,entityType = 0, entityID = 0)>	
			<cfset result.translatedLanguagesStruct = application.com.structureFunctions.queryToTwoDimensionalStruct(query = translatedLanguages,key1="languageid",key2 ="countryid",valueColumn="countryid" )>
			<cfset result.otherTranslationsHTML = application.com.relayTranslations.generateTranslatedLanguagesTable (translatedLanguagesQuery = translatedLanguages, currentCountryid =CountryID, currentlanguageID =LanguageID)>
			<cfset getlanguageAndCountryName = application.com.relayTranslations.getlanguageAndCountryName(countryid  = CountryID, languageid = LanguageID,defaultForThisCountry = result.translations.defaultForThisCountry[listfirst(phraseTextIDs)])	>							

		</cfif>

		<cfset result.languageAndCountryString = getlanguageAndCountryName.fullstring>
		<cfset result.languageAndCountryName = getlanguageAndCountryName.shortstring>
		<cfset result.countryID = CountryID>
		<cfset result.languageID = LanguageID>
		<cfset result.iseditable = application.com.relayTranslations.doesUserHaveRightsToCountryLanguageCombination (countryid = CountryID, languageid =LanguageID )>
		<cfset result.isok = true>
		
		<cfreturn result>
			
	</cffunction>

	<cffunction name="deleteTranslationReturnLanguageBox" access="remote" returns="string">
		<cfargument name="phraseTextIDs" type="string" required="yes">		
		<cfargument name="entityTypeID" type="numeric" required="yes">		
		<cfargument name="entityID" type="numeric" required="yes">		
		<cfargument name="CountryID" type="numeric" required="yes">		
		<cfargument name="LanguageID" type="numeric" required="yes">		
		<cfargument name="CurrentCountryID" type="numeric" required="yes">		
		<cfargument name="CurrentLanguageID" type="numeric" required="yes">		

		
		<cfset result = structNew()>
		<cfset 	application.com.relayTranslations.deleteEntityTranslation (entityType = entityTypeID, entityID = entityID, countryid = countryid, languageid = languageid)>
		<cfset translatedLanguages = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase (phrasetextID = "",entityType = entityTypeID, entityID = entityid,defaultphraseTextIDList=phraseTextIDs)>	
		<cfset result.otherTranslationsHTML = application.com.relayTranslations.generateTranslatedLanguagesTable (translatedLanguagesQuery = translatedLanguages, currentCountryid =currentCountryID, currentlanguageID =currentLanguageID)>
	

		<cfset result.isok = true>
		
		<cfreturn result>
			
	</cffunction>
	
	
	<!--- WAB 2009/09/23 LID 2572 used in relay/translations\addNewPhraseInsert.cfm to validate form--->
	<cffunction name="validateNewPhrasetextID" access="remote" returns="string">
			<cfargument name="phraseTextID" type="string" required="yes">				
			
			<!--- check for valid phrase textid format  returns isOK and a message --->
			<cfset var result = application.com.relayTranslations.isPhraseTextIDValid(phraseTextID)>  

			<!--- if format OK then check that it does not already exist--->
			<cfif result.isok and application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID)>			
		
					<cfset result.isok = false>
					<cfset result.message = listappend(result.message,"Phrase Identifier '#phraseTextID#' already exists","|")>

			</cfif>
			
		<cfreturn result>
		
	</cffunction>
	
		


	
</cfcomponent>	

