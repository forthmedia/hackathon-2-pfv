<!--- �Relayware. All Rights Reserved 2014 --->


<cfcomponent>

	<cffunction name="saveLeftNavOrder" access="remote" returntype="string">
		<cfargument name="leftNavOrder" type="string" default="">
		<cfargument name="variablename" type="string" default="ui.menuorder">
		<cfreturn application.com.settings.InsertUpdateDeleteSetting(variablename="#arguments.variablename#",variablevalue="#arguments.leftNavOrder#",personid="#request.relaycurrentuser.personid#")>
	</cffunction>


	<cffunction name="message" access="public" returntype="string">
		<cfargument name="message" type="string" required="true">

		<cfreturn application.com.relayUI.message(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="displayMessage" access="public" returnType="string">
		<cfreturn application.com.relayUI.getMessage()>
	</cffunction>

</cfcomponent>