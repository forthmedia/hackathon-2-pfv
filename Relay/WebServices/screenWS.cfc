<!---
  --- activityScoreWS
  --- ---------------
  --- 
  --- author: Richard.Tingle
  --- date:   10/03/16
  --->
<cfcomponent accessors="true" output="false" persistent="false">


	<!---Returns (a) if there is a special UI and (b) )if there is also includes its keys --->
	<cffunction name="getSpecialEvalStringUI" access="public" output="false" returntype="struct">
		<cfargument name="thisFieldSource" type="string" required="true" hint="The type of screen item (e.g. screenWidget)">
		<cfargument name="fieldTextID" type="string" required="true" hint="The specific screen item (e.g. the specific screenWidget)">
		
		<cfscript>
			if (thisFieldSource EQ "screenWidgets"){
				return {"hasSpecialUI"="true","html"=application.com.screenWidgets.getWidgetsOptionHTML(fieldTextID)};
			}else{
				return {"hasSpecialUI"="false","html"=""};
			}
		
		</cfscript>

	</cffunction>
	
	
	
</cfcomponent>