/**
 * datesWS
 * 
 * @author Richard.Tingle
 * @date 22/03/16
 **/
component accessors=true output=false persistent=false {


	public query function getDaysInNonLeapYearMonth(required numeric monthIndex hint="1 = january etc") hint="returns a query containing every day in a non leap year month of the requested type" output="false" {
		/* TODO: Implement Method */
		return application.com.dateFunctions.getAllDaysInMonth(monthIndex);
	}
}