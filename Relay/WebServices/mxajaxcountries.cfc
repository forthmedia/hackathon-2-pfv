<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Alex Connell
Date created:	2007-04-25

	This provides the extended mxAjax functionality for Claims

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
--->


<cfcomponent extends="relay.com.mxajax">

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the products.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getcountrylanguages" access="remote" returntype="array" hint="Returns a set of languages for a passed countryID">
		<!---CountryID is numeric but to handle the nulls comming in I have declared it has a string SSS--->
		<cfargument name="countryID" type="string" default="0">
		
		<cfset var qry_get_SelectLanguages = "">
		
		<cfif not isnumeric(countryID)>
			<cfset countryID = 0>
		</cfif>
		
		<cfset langIDs = application.com.relayTranslations.getCountryLanguages(countryID)>
		
		<cfset LanguagesArray = ArrayNew(1)>
	
		<CFQUERY NAME="qry_get_SelectLanguages" DATASOURCE="#application.sitedatasource#">
			SELECT languageid,language = CASE WHEN locallanguagename IS NULL 
                      THEN language ELSE locallanguagename END from language
			WHERE languageid  in ( <cf_queryparam value="#langIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFQUERY>
		<!--- <cfset ArrayAppend(LanguagesArray, " , ")> --->
		<cfloop query="qry_get_SelectLanguages">
			<cfset ArrayAppend(LanguagesArray, "#languageid#,#language#")>
		</cfloop>
	
		<cfreturn LanguagesArray> 
	</cffunction>
	
</cfcomponent>
