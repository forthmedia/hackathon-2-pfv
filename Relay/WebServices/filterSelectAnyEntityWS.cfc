<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			filterSelectAnyEntityWS.cfc
Author:				YMA
Date started:		01/08/2014

Description:		web services to populate filterSelectAnyEntity custom tag.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:

 --->

<cfcomponent>

	<cffunction name="loadOperator" access="remote">
		<cfargument name="field" required="true" type="string">
		<cfargument name="entityTypeID" required="true">
		
		<cfreturn application.com.filterSelectAnyentity.loadOperator(argumentCollection = arguments)>
	</cffunction>

	<cffunction name="loadComparison" access="remote">
		<cfargument name="field" required="true" type="string">
		<cfargument name="operator" required="true" type="string">
		<cfargument name="entityTypeID" required="true">
		
		<cfreturn application.com.filterSelectAnyentity.loadComparison(argumentCollection = arguments)>
	</cffunction>

	<cffunction name="saveFilter" access="remote">
		<cfargument name="filterField" required="true" type="string">
		<cfargument name="operator" required="true" type="string">
		<cfargument name="comparisonB" required="true" type="string">
		<cfargument name="comparisonC" required="false" type="string">
		<cfargument name="RELATEDENTITYID" required="true">
		<cfargument name="RELATEDENTITYTYPEID" required="true">
		
		<cfreturn application.com.filterSelectAnyentity.saveFilter(argumentCollection = arguments).html>
	</cffunction>

	<!--- Used by the local version of the filter --->
	<cffunction name="getHtmlForNewFilter" access="remote">
		<cfargument name="filterField" required="true" type="string">
		<cfargument name="operator" required="true" type="string">
		<cfargument name="comparisonB" required="true" type="string">
		<cfargument name="comparisonC" required="false" type="string">
		<cfargument name="RELATEDENTITYID" required="true">
		<cfargument name="RELATEDENTITYTYPEID" required="true">
		
		<cfreturn application.com.filterSelectAnyentity.saveFilter(argumentCollection = arguments).html>
	</cffunction>


	<cffunction name="deleteFilter" access="remote">
		<cfargument name="filterCriteriaID" required="true">
		
		<cfreturn application.com.filterSelectAnyentity.deleteFilter(argumentCollection = arguments)>
	</cffunction>
	
</cfcomponent>	