<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			myRelay.cfc	
Author:				
Date started:			2012/07/16
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/04/26			NJH			2013 Roadmap - removed the need to pass in an attribute when deleting a link as the link is really based on the location.
Possible enhancements:


 --->

 <cfcomponent>
	<cfsetting enablecfoutputonly="true">
 
 	<cffunction name="addMyLink" access="remote" output="false">
		<cfargument name="type" type="string" required="true">
		<cfargument name="location" type="string" required="true">
		<cfargument name="name" type="string" required="true">
		<cfargument name="group" type="string" required="true">
		<cfargument name="module" type="string" default="">
		
		<cfset application.com.myRelay.addLink(linkType=arguments.type,location=arguments.location,description=arguments.name,attribute1=arguments.group,moduleTextID=arguments.module)>
	</cffunction>
	
	<cffunction name="removeMyLink" access="remote" output="false">
		<cfargument name="type" type="string" required="true">
		<cfargument name="location" type="string" required="true">
		<!--- <cfargument name="group" type="string" required="true"> --->
		
		<cfset application.com.myRelay.deleteLink(linkType=arguments.type,location=arguments.location<!--- ,attribute1=arguments.group --->)>
	</cffunction>

 </cfcomponent>

