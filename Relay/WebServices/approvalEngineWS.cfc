<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			approvalEngineWS.cfc	
Author:				YMA
Date started:		2014/07/11
	
Description:			

approvalEngine web service calls e.g. ajax portal content, select field bind functions.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/02/18		YMA		(2015/11/27 by Yan, committed by RJT) P-PAN001 CR001 Updated approval engine to handle approver identified by integerflagdata

 --->
<cfcomponent>

	<cffunction name="callMethod" access="remote">
		<cfargument name="methodName" type="string" required="true">

		<cfset var result = "">
		
		<cfif request.relayCurrentUser.isLoggedIn>
			<cfset result = evaluate("#arguments.methodName#(argumentCollection=arguments)")>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getEntitiesForEntityType" access="remote">
		<cfargument name="entityTypeID" required="false">
		<cfargument name="approvalEngineID" required="false"><!--- 2015/11/27 YMA P-PAN001 CR001 following updates this function requires approvalEngineID. --->
		
		<cfset var getEntitiesForEntityType = application.com.approvalEngine.getEntitiesForEntityType(arguments.entityTypeID,arguments.approvalEngineID)>
			
		<cfreturn getEntitiesForEntityType>
		
	</cffunction>
		
</cfcomponent>	