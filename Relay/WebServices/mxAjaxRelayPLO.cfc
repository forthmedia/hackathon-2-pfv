<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Peter Barron
Date created:	2008-09-04

	This provides a store for POL-related functions called  using Ajax 
created during CR-TND561 Req8 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/09/21			NJH			LID 3956 Check the org type when doing unique email validation. Skip the check for end customers
--->

<cfcomponent extends="relay.com.mxajax" output="true">
	


	<cffunction name="isEmailAddressUnique" access="private" >
		<cfargument name="emailAddress" required="yes">
		<cfargument name="personid" required="yes" type="numeric"><!--- pass in 0 for a new record --->
		<cfargument name="orgType" required="no" default="reseller"> <!--- NJH 2010/09/21 --->

		<!--- CASE 426744: NJH 2012/02/23 moved this to relayPLO to allow server side validation --->
		<cfreturn application.com.relayPLO.isEmailAddressUnique(argumentCollection=arguments)>

	</cffunction>

	<!--- NJH 2008/11/14 T-10 Bug Fix Issue 888 - added a new function to get the number of locations at the organisation --->
	<cffunction access="remote" name="countLocsInOrg" hint="Returns a the number of locations in org with organisationID">
		<cfargument name="OrganisationID" type="numeric" required="yes">
		
		<cfscript>
			var countLocsInOrg = "";
		</cfscript>
		
		<cfquery name="countLocsInOrg" datasource="#application.siteDataSource#">
			select count(*) as numLocs from location where organisationID = #arguments.OrganisationID#
		</cfquery>
		
		<cfreturn countLocsInOrg.numLocs>
	</cffunction>
	
</cfcomponent>

