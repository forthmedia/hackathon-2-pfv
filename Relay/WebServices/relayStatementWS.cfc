<!--- ?Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayStatementWS.cfc	
Author:			
Date started:	19-08-2014
	
Description:			
This webservice was setup for Incentive statement

--->

<cfcomponent>

	<cffunction access="public" name="deleteTransaction" hint="delete an open transaction" returntype="any">
		<cfargument name="RWTransactionID" type="numeric" required="yes">

		<cfset var deleteStatus = application.com.relayIncentive.deleteTransaction(argumentCollection=arguments)>
		<cfreturn #deleteStatus#>
	</cffunction>

</cfcomponent>