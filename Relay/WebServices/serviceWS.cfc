<!--- �Relayware. All Rights Reserved 2014

2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).

--->
<cfcomponent>

	<cffunction name="unlinkEntityFromService" access="remote" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default="0">

		<cfif request.relayCurrentUser.isInternal or (not request.relayCurrentUser.isInternal and request.relayCurrentUser.personID eq arguments.entityID and arguments.entityTypeID eq 0)>
			<cfset application.com.service.unlinkEntityFromService(argumentCollection=arguments)>
		</cfif>
	</cffunction>


	<cffunction name="optIn" access="public" output="false">
		<cfargument name="flagID" type="string" required="true" encrypted="true">

		<cfreturn setUnSetServiceFlag(method="unset",flagID=arguments.flagID)>
	</cffunction>


	<cffunction name="optOut" access="public" output="false">
		<cfargument name="flagID" type="string" required="true" encrypted="true">

		<cfreturn setUnSetServiceFlag(method="set",flagID=arguments.flagID)>
	</cffunction>


	<cffunction name="setUnSetServiceFlag" access="private" output="false">
		<cfargument name="method" type="string" default="set">
		<cfargument name="flagID" type="string" required="true" encrypted="true">

		<cfset var content_html = "">
		<cfset var flagTextID=application.com.flag.getFlagStructure(flagId=ListFirst(arguments.flagID)).flagTextID>
		<cfset var service = replace(listFirst(flagtextID,"_"),"optOutShare","")>
		<cfset var socialFlagID = "">

		<cfif listFindNoCase(application.com.settings.getSetting("socialMedia.services"),service)>
			<cfloop list="#arguments.flagID#" index="socialFlagID">
				<cfif arguments.method eq "set">
					<cfset application.com.flag.setBooleanFlag(flagTextID=socialFlagID,entityID=request.relayCurrentUser.personID)>
				<cfelse>
					<cfset application.com.flag.unsetBooleanFlag(flagID=socialFlagID,entityID=request.relayCurrentUser.personID)>
				</cfif>
			</cfloop>
		</cfif>

		<cfset content_html = getProfileDisplay(serviceID=serviceID)>

		<cfreturn content_html>
	</cffunction>


	<cffunction name="getProfileDisplay" access="public" output="false">
		<cfargument name="serviceId" type="string" required="true">

		<cfset var content_html = "">
		<cfset var flagGroupData = application.com.flag.getFlagGroupDataForCurrentUser(flagGroupId="optOutShare#trim(arguments.serviceID)#",returnNulls=true)>
		<cfset var action = "optIn">

		<cfsavecontent variable="content_html">
			<cfoutput>
			<div class="manageSocialProfiles">
				<cfloop query="flagGroupData">
					<cfset action = "optIn">
					<cfif not isSet>
						<cfset action = "optOut">
					</cfif>
					<div class="form-group row">
						<div id="label" class="col-xs-9 col-sm-6 col-md-4 col-lg-4"><label>#htmlEditFormat(translatedFlagName)#</label></div>
						<div id="action" class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
							<div class="#action#"><a href="javascript:#action#('#application.com.security.encryptVariableValue(name="flagID",value=flagGroupData.flagID)#','#arguments.serviceID#');">phr_social_#action#</a></div>
						</div>
					</div>
				</cfloop>
			</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content_html>

	</cffunction>

</cfcomponent>
