<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

2006-03-28			WAB			sorted out problems with running application .cfm before these services so no longer need to pass in an appname variable
WAB 2009/09/07 LID 2515 Removed #application. userfilespath#  

Possible enhancements:


 --->
<cfcomponent name="flashRemotingFunctions" displayname="Flash Remoting" hint="Various functions for Flash Remoting components">
	
	<cffunction name="getQueryAsStruct" access="remote" hint="Calls a method with return type query and builds a structure out of the results" returntype="struct">
		<cfargument name="methodToCall" required="yes">
		<cfargument name="methodArguments" default="">
		
		<cfset var queryReturn = "">
		<cfset var rowStruct = structNew()>
		<cfset var returnStruct = structNew()>
		<cfset var mArgs = structNew()>

		<cfif arguments.methodArguments is not "" and listLen(arguments.methodArguments,"|") gt 0>
			<cfloop list="#arguments.methodArguments#" index="nvpair">
				<cfif listLen(nvpair,"~") eq 2>
					<cfset mArgs[listFirst(nvpair,"~")] = listLast(nvpair,"~")>
				</cfif>
			</cfloop>
		</cfif>
		
		<cfinvoke
			method = "#methodToCall#"
			returnVariable = "queryReturn"
			argumentCollection = "#mArgs#"
		>
		
		<cfset returnStruct.recordCount = queryReturn.recordCount>
		<cfset returnStruct.columnList = queryReturn.columnList>
		<cfset returnStruct.records = arrayNew(1)>
		
		<cfloop query="queryReturn">
			<cfset rowStruct = structNew()>
			<cfloop list="#columnList#" index="col">
				<cfset rowStruct[col] = evaluate(col)>
			</cfloop>
			<cfscript>
				arrayAppend(returnStruct.records,rowStruct);
			</cfscript>
		</cfloop>
		
		<cfreturn returnStruct>
		
	</cffunction>

	<cffunction name="getBannerDetail" access="remote" hint="Retrieves banner data based on bannerID">
		<cfargument name="bannerID" required="yes" type="numeric">
		
		<cfquery name="bannerDetail" datasource="#application.sitedatasource#">
			select	b.*, bt.imageItemAmount, bt.textItemAmount, '#request.currentSite.httpProtocol##cgi.SERVER_NAME#/content/bannerImages/' as imagePath
			from	Banner b, BannerType bt
			where	b.BannerID = #arguments.bannerID# and
					bt.bannerTypeID = b.bannerTypeID
		</cfquery>
		<cfset picExistListArr = arrayNew(1)>
		<cfset picExistListArr[1] = "">
		<cfloop from="1" to="#bannerDetail.imageItemAmount#" index="picIndex">
			<cfif fileExists("#application.paths.content#\bannerImages\#bannerDetail.bannerID#_image_#picIndex#.jpg")>
				<cfset picExistListArr[1] = listAppend(picExistListArr[1],1,"|")>
			<cfelse>
				<cfset picExistListArr[1] = listAppend(picExistListArr[1],0,"|")>
			</cfif>
		</cfloop>
		
		<cfscript>
			queryAddColumn(bannerDetail,"picExistList",picExistListArr);
		</cfscript>
		
		<cfreturn bannerDetail>
	</cffunction>
	
	<cffunction name="getDummyBannerDetail" access="remote" hint="Returns dummy banner info based on bannerTypeID for example purposes.">
		<cfargument name="bannerTypeID" required="yes" type="numeric">
		
		<cfquery name="dummyBannerDetail" datasource="#application.sitedatasource#">
			select	0 as bannerID, 'Banner Text 1' as bannerText1, 'Banner Text 2' as bannerText2, 'Banner Text 3' as bannerText3, 'Banner Text 4' as bannerText4, 'Banner Text 5' as bannerText5, imageItemAmount, textItemAmount
			from	BannerType
			where	bannerTypeID = #arguments.bannerTypeID#
		</cfquery>
		
		<cfset picExistListArr = arrayNew(1)>
		<cfset picExistListArr[1] = "">
		<cfloop from="1" to="#dummyBannerDetail.imageItemAmount#" index="picIndex">
			<cfif fileExists("#application.paths.content#\bannerImages\#dummyBannerDetail.bannerID#_image_#picIndex#.jpg")>
				<cfset picExistListArr[1] = listAppend(picExistListArr[1],1,"|")>
			<cfelse>
				<cfset picExistListArr[1] = listAppend(picExistListArr[1],0,"|")>
			</cfif>
		</cfloop>
		
		<cfscript>
			queryAddColumn(dummyBannerDetail,"picExistList",picExistListArr);
		</cfscript>
		
		<cfreturn dummyBannerDetail>
	</cffunction>

 	<cffunction name="getIncentiveTeaserDetails" access="remote" returntype="query" hint="This function returns certain specific details for the incentive teaser button">
		<cfargument name="siteDomain" required="yes" type="string">
	  	<cfargument name="balance" required="yes" type="numeric">	 
		<cfargument name="currentCountry" required="yes" type="numeric" hint="countryID">
		<cfargument name="teaserProductGroupIDs" required="no" type="string"> 
		<cfargument name="callType" required="no" type="string" default="remote">
	  
	  	<cfscript>
			var qGetIncentiveTeaserDetails = "";
			var promoid = "IncentivePrizesCatalogue";
		</cfscript>

		<CFQUERY NAME="qGetIncentiveTeaserDetails" datasource="#application.sitedatasource#">
			SELECT 	top 5
				'#request.currentSite.httpProtocol##arguments.siteDomain#/content/productThumbs/CountryID' + cast(countryID as 

nvarchar(255)) + '/#promoID#-' + replace(replace(replace(replace(SKU,'&','-'),'*','-'),'/','-'),' ','-') + '.jpg' as image,Description as caption, DiscountPrice as points
			FROM 
				vProductList
			WHERE 
						campaignid = (select campaignid from promotion where promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			and (
				CountryID In (#arguments.currentcountry#<cfif application.countryGroupsBelongedTo[arguments.currentcountry] is not 

"">,#application.countryGroupsBelongedTo[arguments.currentcountry]#</cfif>)
					or 
				(countryid = 0
					and not exists(select 
						* 
					from  
						Product P1 
					where 
						((isNull(vProductList.SKUGroup,'') <> ''	and vProductList.SKUGroup = P1.SKUGroup) 
							OR
							vProductList.SKU = P1.sku)	
						and vProductList.campaignid = P1.campaignid
						and deleted = 0						
					     AND P1.CountryID In (#arguments.currentcountry#<cfif application.countryGroupsBelongedTo[arguments.currentcountry] is not 

"">,#application.countryGroupsBelongedTo[arguments.currentcountry]#</cfif>) 
					)
				 )
			)
			and discountprice <= #balance#
			<cfif isdefined('arguments.teaserProductGroupIDs')>
				and productgroupid  in ( <cf_queryparam value="#arguments.teaserProductGroupIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>		
			ORDER BY DiscountPrice desc,sortorder, productgroupid
		</CFQUERY>
		<cfif qGetIncentiveTeaserDetails.recordcount lt 3>
			<!--- Not enough current balance - get the closest 5 above the balance --->
			<CFQUERY NAME="qGetIncentiveTeaserDetails" datasource="#application.sitedatasource#">
				SELECT 	top 5
					'#request.currentSite.httpProtocol##arguments.siteDomain#/content/productThumbs/CountryID' + cast(countryID as 

nvarchar(255)) + '/#promoID#-' + replace(replace(replace(replace(SKU,'&','-'),'*','-'),'/','-'),' ','-') + '.jpg' as image, ' ' as caption, DiscountPrice as points
				FROM 
					vProductList
				WHERE 
							campaignid = (select campaignid from promotion where promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				and (
					CountryID In (#arguments.currentcountry#<cfif application.countryGroupsBelongedTo[arguments.currentcountry] is not 

"">,#application.countryGroupsBelongedTo[arguments.currentcountry]#</cfif>)
						or 
					(countryid = 0
						and not exists(select 
							* 
						from  
							Product P1 
						where 
							((isNull(vProductList.SKUGroup,'') <> ''	and vProductList.SKUGroup = P1.SKUGroup) 
								OR
								vProductList.SKU = P1.sku)	
							and vProductList.campaignid = P1.campaignid
							and deleted = 0						
						     AND P1.CountryID In (#arguments.currentcountry#<cfif application.countryGroupsBelongedTo[arguments.currentcountry] is not 

"">,#application.countryGroupsBelongedTo[arguments.currentcountry]#</cfif>) 
						)
					 )
				)
				and discountprice >= #balance#
				<cfif isdefined('arguments.teaserProductGroupIDs')>
					and productgroupid  in ( <cf_queryparam value="#arguments.teaserProductGroupIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>		
				ORDER BY DiscountPrice asc,sortorder, productgroupid
			</CFQUERY>
		</cfif>
		<cfreturn qGetIncentiveTeaserDetails>
  	</cffunction>
	
</cfcomponent>

