﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name: relayFundManagerWS.cfc

2012-09-19 PPB Case 430743 avoid error when fund account is set to Please Select
2013-02-18 PPB Case 433111 changed the format of dates returned because IE doesn't support YYYY-MM-DD by javascript new Date()
2016/01/19	NJH JIRA 164 - few minor changes to deal with this ticket, primarily changing argument from numeric to string for getBudgetPeriods_Dropdown function
 --->
<cfcomponent output="false">

	<cffunction name="getSelectBudgetGroups" access="remote" returntype="query" hint="Returns available BudgetGroups for an account">
		<cfargument name="organisationID" type="string" default="0">
		<!--- START: 2011/05/09 - AJC - Issue 6431: Fund Manager - I can no longer select any dates as they are all grayed out in the request form --->
		<cfset var orgID = isNumeric(arguments.organisationID)?arguments.organisationID:0 />
		<cfreturn application.com.relayFundManager.getBudgetGroupsForOrganisation(organisationID=orgID)>
		<!--- END: 2011/05/09 - AJC - Issue 6431: Fund Manager - I can no longer select any dates as they are all grayed out in the request form --->
	</cffunction>

	<cffunction name="getCoFunders" access="remote" returntype="query" hint="Returns available CoFunders for an account">
		<cfargument name="organisationID" type="string" default="0">
		<cfreturn application.com.relayFundManager.getCoFunders(organisationID=val(arguments.organisationID))>	<!--- 2012-09-19 PPB Case 430743 added val() because it will be empty string when form loads on new request and fund account is set to Please Select --->
	</cffunction>


	<cffunction name="getBudgetGroup" access="remote" hint="Returns a BudgetGroup">
		<cfargument name="budgetGroupID" type="numeric" required="true">
		<!--- START: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfargument name="organisationID" type="numeric" required="true">
		<!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

		<!--- START: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfset var qryBudgets = "" />
		<cfset var aBudgetGroup = "" />
		<cfset var budgetIDs = "" />

		<cfset var qryFundAccounts = application.com.relayFundManager.getAccountFromBudgetGroupID(budgetGroupID=arguments.budgetGroupID,organisationID=arguments.organisationID) />

		<!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

		<cfset var qryBudgetGroup=application.com.relayFundManager.getBudgetGroup(budgetGroupID=arguments.budgetGroupID)>

		<!--- START: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->
		<cfif qryBudgetGroup.recordcount neq 0>
			<cfset aBudgetGroup = application.com.structureFunctions.QueryToArrayOfStructures(query=qryBudgetGroup) />
		<cfelse>
			<cfset aBudgetGroup = arrayNew(1) />
			<cfset arrayappend(aBudgetGroup,structnew()) />
		</cfif>

		<cfquery name="qry_get_budgetperiodDates" datasource="#application.sitedatasource#">
			select min(bp.startDate) as startDate, max(bp.endDate) as endDate from budgetPeriod bp
			inner join BudgetPeriodAllocation bpa on bp.budgetPeriodID = bpa.BudgetPeriodID and bpa.authorised = 1
			inner join Budget b on bpa.budgetID = b.budgetID
			<cfif qryFundAccounts.recordcount neq 0>
				and bpa.budgetID =  <cf_queryparam value="#qryFundAccounts.budgetID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				and bpa.budgetID = -1
			</cfif>
			<!--- START: 2012/02/08 PPB Case 426434 on the portal the user cannot enter a request earlier than the current quarter --->
			<cfif not request.relaycurrentuser.isInternal>
				where bp.endDate > GETDATE()		<!--- we could use dbo.dateAtMidnight() db function but the user can specify 11:59 PM when setting the period enddate and it is documented accordingly --->
			</cfif>
			<!--- END: 2012/02/08 PPB Case 426434 on the portal the user cannot enter a request earlier than the current quarter --->
		</cfquery>

		<!--- START 2013-02-18 PPB Case 433111 changed the format of dates returned because IE doesn't support YYYY-MM-DD by javascript new Date() --->
		<cfif qry_get_budgetperiodDates.endDate eq "">
			<cfset aBudgetGroup[1].budgetperiodEndDate = "01/01/2000" />
			<cfset aBudgetGroup[1].budgetperiodStartDate = "01/01/2000" />
		<cfelse>
			<cfset aBudgetGroup[1].budgetperiodEndDate = dateformat(qry_get_budgetperiodDates.endDate,"MM/DD/YYYY") />
			<cfset aBudgetGroup[1].budgetperiodStartDate = dateformat(qry_get_budgetperiodDates.startDate,"MM/DD/YYYY") />
		</cfif>
		<!--- END 2013-02-18 PPB Case 433111  --->

		<!--- END: 2011/04/07 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

		<cfreturn aBudgetGroup>

	</cffunction>

	<!--- 2011/04/18 PPB LEX052 --->
	<cffunction name="getBudgets_Dropdown" access="public" returntype="query" hint="Returns Budgets for a BudgetGroup">
		<cfargument name="budgetGroupID" type="numeric" default="0">

		<cfreturn application.com.relayFundManager.getBudgets_Dropdown(budgetGroupID=arguments.budgetGroupID)>
	</cffunction>

	<!--- 2011/04/18 PPB LEX052 --->
	<cffunction name="getBudgetPeriods_Dropdown" access="public" returntype="query" hint="Returns Budget Periods">
		<cfargument name="budgetGroupID" type="numeric" required="false">
		<cfargument name="budgetID" type="string" required="false">

		<cfif structKeyExists(arguments,"budgetID") and not isNumeric(arguments.budgetID)>
			<cfset arguments.budgetID = 0>
		</cfif>
		<cfreturn application.com.relayFundManager.getBudgetPeriods_Dropdown(argumentCollection=arguments)>
	</cffunction>

	<!--- 2011/04/18 PPB LEX052 --->
	<cffunction name="getBudgetGroupPeriodAllocations" access="remote" hint="Returns Budget Group Period Allocations" output="false">
		<cfargument name="budgetGroupID" type="numeric" required="true">
		<cfargument name="budgetPeriodID" type="numeric" required="true">

		<cfset var qryBudgetGroupPeriodAllocations = application.com.relayFundManager.getBudgetGroupPeriodAllocations(budgetGroupID=arguments.budgetGroupID, budgetPeriodID=arguments.budgetPeriodID)>
		<cfset var result = {amount = qryBudgetGroupPeriodAllocations.amount,amountAuthorised = qryBudgetGroupPeriodAllocations.amountAuthorised,amountNotAuthorised = qryBudgetGroupPeriodAllocations.amountNotAuthorised,amountRemaining = qryBudgetGroupPeriodAllocations.amountRemaining}>
		<cfreturn result>
	</cffunction>

</cfcomponent>
