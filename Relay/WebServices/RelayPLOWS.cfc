<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>

<cffunction name="getLocations" access="remote">
	<CFARGUMENT name="actionOrgs" required="true" type="string">
	<CFARGUMENT name="LocationID" required="false" type="string" hint="location id">
	
	<cfreturn application.com.relayPLO.getLocations(argumentCollection=arguments)>
</cffunction>

<cffunction name="getEntitiesSearch" access="remote" returntype="string" output="false" loginRequired=2 internalUser="true">
	<cfargument name="entityType" type="string" required="true" default="person">
	<cfargument name="term" type="string" required="true">
	<cfargument name="searchApprovedPOLOnly" type="boolean" required="false" default="false">
	<cfargument name="extraColumns" default="" type="string" required="false">
	<cfargument name="countryID" required="false">
	
	<cfset var getEntityNames = queryNew("entityname,entityid")>
	<cfset var entityArray = arrayNew(1)>
	<cfset var jsonString = "">
	<cfset var rowStruct = "">
	
	<cfset arguments.term = URLEncodedFormat(arguments.term)>
	<cfset arguments.term = URLDecode(replace(arguments.term,'%23','%23%23','all'))>
	
	<cfif arguments.countryID eq 0>
		<cfset arguments.countryID = "">
	</cfif>
	
	<cfset getEntityNames = application.com.search.getSiteSearchResults(tablesToSearch=arguments.entityType,searchTextString=arguments.term,searchApprovedPOLOnly=arguments.searchApprovedPOLOnly,extraColumns=arguments.extraColumns,countryID=arguments.countryID)>
	
	<cfloop query="getEntityNames">
		<cfset rowStruct = application.com.structureFunctions.queryRowToStruct(query=getEntityNames,row=currentRow)>
		<cfset arrayAppend(entityArray,rowStruct)>
	</cfloop>
	
	<!--- jQuery autocomplete is expecting id,label and value to be in lower case.. CF structs are always uppercase.. so, convert json to string.. seems to work well. --->
	<cfset jsonString = serializeJson(entityArray)>
	<cfset jsonString = replaceList(jsonString,'"ENTITYNAME","ENTITYID"','"label","value"')>
	
	<cfreturn jsonString>	
</cffunction>

</cfcomponent>