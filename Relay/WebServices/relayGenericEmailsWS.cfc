<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		WebServices\relayElearningWS.cfc
Author:			AJC
Date started:	2010-06-01

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/05/02			NJH			2013 Roadmap - return the modified date column for flags.
2015/08/18          DAN         add support for is null/is not null in the conditional e-mails
2015/10/13          DAN         PROD2015-160 - helper function to get emailDefID by given emailTextID

Possible enhancements:


 --->

<cfcomponent>

	<cfsetting enablecfoutputonly="true">

	<cffunction access="remote" name="getTableColumns" returntype="query" hint="">
		<cfargument name="tablename" type="String" required="true" hint="Table from which you want the column list">
		<cfargument name="returnColumns" type="boolean" required="false" hint="" default="false">

		<cfscript>
				var qry_get_tablecolumns = "";
		</cfscript>

		<!--- return another set of rows for flags that are for the lastModified date for the flag. Have a name as  flagID_lastUpdated --->
		<cfquery name="qry_get_tablecolumns" datasource="#application.siteDataSource#">
			select fieldname, 'phr_'+tablename+'_'+fieldname as friendlyFieldName, 1 AS sortOrder
			from modEntityDef
				where showInConditionalEmails=1 and tablename =  <cf_queryparam value="#arguments.tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			UNION
			select cast(flagID as varchar) as fieldname, case when len(f.namePhraseTextID) > 0 then 'phr_'+f.namePhraseTextID else f.name end as friendlyFieldName, 2 AS sortOrder
			from flag f inner join flagGroup fg
				on f.flagGroupID = fg.flagGroupID
			where showInConditionalEmails=1 and fg.entityTypeID =  <cf_queryparam value="#application.entityTypeID[arguments.tablename]#" CFSQLTYPE="CF_SQL_INTEGER" >
			UNION
			select cast(flagID as varchar)+'_lastUpdated' as fieldname, case when len(f.namePhraseTextID) > 0 then 'phr_'+f.namePhraseTextID else f.name end as friendlyFieldName, 2 AS sortOrder
			from flag f inner join flagGroup fg
				on f.flagGroupID = fg.flagGroupID
			where showInConditionalEmails=1 and fg.entityTypeID =  <cf_queryparam value="#application.entityTypeID[arguments.tablename]#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cf_translateQueryColumn query="#qry_get_tablecolumns#" columnName="friendlyFieldName">

		<!--- have to set the name after the translate..  --->
		<cfloop query="qry_get_tablecolumns">
			<cfif right(fieldname, 12) eq "_lastUpdated">
				<cfset querySetCell(qry_get_tablecolumns,"friendlyFieldName",friendlyFieldName & " Last Modified",currentRow)>
			</cfif>
		</cfloop>

		<cfquery name="qry_get_tablecolumns" dbtype="query">
			select * from qry_get_tablecolumns
			order by friendlyFieldName 					<!--- to list table fields followed by flags: sortOrder, friendlyFieldName --->
		</cfquery>

		<cfreturn qry_get_tablecolumns />
	</cffunction>


	<cffunction name="getColumnType" returntype="struct">
		<cfargument name="tablename" type="String" required="true" hint="Table from which you want the column list">
		<cfargument name="columnname" type="string" required="true" hint="">

		<cfscript>
			var qry_get_tablecolumns = "";
			var entityTypeID = 0;
			var groups ={1="entity"};
			var regExpResult = "";
			var type="";
			var result = structNew();
			var flagStruct = structNew();
			var theColumnName = listFirst(arguments.columnName,"_");
		</cfscript>

		<cfif not isNumeric(theColumnName)>
			<cfset qry_get_tablecolumns = application.com.dbTools.getTableDetails(tablename=arguments.tablename,columnName=theColumnName,excludeColumn="")>
			<cfif listFindNoCase("int,float",qry_get_tablecolumns.data_type)>
				<cfset regExpResult = application.com.regExp.reFindAlloccurrences("(location|person|organisation|country)ID",theColumnName,groups)>
				<cfif arrayLen(regExpResult)>
					<cfset entityTypeID = application.entityTypeID[regExpResult[1].entity]>
					<cfset type = "entity">
				</cfif>
			<cfelseif listFindNoCase("text,nvarchar,varchar,ntext",qry_get_tablecolumns.data_type)>
				<cfset type = "string">
			<cfelseif qry_get_tablecolumns.data_type eq "bit">
				<cfset type="boolean">
			<cfelseif qry_get_tablecolumns.data_type eq "datetime">
				<cfset type="date">
			</cfif>
		<!--- otherwise it's a flagID --->
		<cfelse>
			<!--- if it's a lastUpdated flag, then it's of type date --->
			<cfif listLast(arguments.columnName,"_") eq "lastUpdated">
				<cfset type="date">
			<cfelse>
				<cfset flagStruct = application.com.flag.getFlagStructure(flagID=theColumnName)>

				<cfif flagStruct.flagGroup.flagType.datatable eq "boolean">
					<cfset type="boolean">
				<cfelseif flagStruct.flagGroup.flagType.datatable eq "integer" and flagStruct.linksToEntityTypeID neq "">
					<cfset entityTypeID = flagStruct.linksToEntityTypeID>
					<cfset type = "entity">
				</cfif>
			</cfif>
		</cfif>

		<cfset result.type = type>
		<cfset result.entityTypeID = entityTypeID>

		<cfreturn result>
	</cffunction>


	<cffunction access="remote" name="getColumnOperators" returntype="query" hint="">
		<cfargument name="tablename" type="String" required="true" hint="Table from which you want the column list">
		<cfargument name="columnname" type="string" required="true" hint="">

		<cfscript>
				var qry_operators = "";
				var columnType = "";
		</cfscript>

		<cfset qry_operators = querynew("operator_value,operator_name") />

		<cfset columnType = getColumnType(tablename=arguments.tablename,columnName=arguments.columnName)>

		<cfswitch expression="#columnType.type#">
			<cfcase value="date">						<!--- 2011/06/27 PPB REL106 changed from "datetime" cos the filter options (eg "Exact days before") list wasn't showing --->
				<cfset QueryAddRow(qry_operators) />
   				<cfset QuerySetCell(qry_operators, "operator_value", "NumDaysBeforeRun") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NumDaysBeforeRun") />
				<cfset QueryAddRow(qry_operators) />
   				<cfset QuerySetCell(qry_operators, "operator_value", "NumDaysBeforeRunOrMore") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NumDaysBeforeRunOrMore") />

				<!--- if using a profile last modified date, then don't return filters that expect days to be in the future, as it deals with past dates --->
				<cfif right(arguments.columnName,12) neq "_lastUpdated">
					<cfset QueryAddRow(qry_operators)>
					<cfset QuerySetCell(qry_operators, "operator_value", "NumDaysAfterRun") />
					<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NumDaysAfterRun") />
					<cfset QueryAddRow(qry_operators)>
					<cfset QuerySetCell(qry_operators, "operator_value", "NumDaysAfterRunOrMore") />
					<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NumDaysAfterRunOrMore") />
				</cfif>
			</cfcase>
			<cfcase value="boolean">
				<cfset QueryAddRow(qry_operators) />
   				<cfset QuerySetCell(qry_operators, "operator_value", "Equals") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_Equals") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "NotEquals") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NotEquals") />
			</cfcase>
			<cfcase value="entity,string">
				<cfset QueryAddRow(qry_operators)>
   				<cfset QuerySetCell(qry_operators, "operator_value", "Equals") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_Equals") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "NotEquals") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NotEquals") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "In") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_In") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "NotIn") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NotIn") />
			</cfcase>
			<cfdefaultcase>
				<cfset QueryAddRow(qry_operators)>
   				<cfset QuerySetCell(qry_operators, "operator_value", "Equals") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_Equals") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "NotEquals") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NotEquals") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "GreaterThan") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_GreaterThan") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "LessThan") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_LessThan") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "In") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_In") />
				<cfset QueryAddRow(qry_operators)>
				<cfset QuerySetCell(qry_operators, "operator_value", "NotIn") />
				<cfset QuerySetCell(qry_operators, "operator_name", "phr_email_NotIn") />
			</cfdefaultcase>
		</cfswitch>

        <!--- 2015/08/18 DAN - add support for is null/is not null in the conditional e-mails --->
        <cfset QueryAddRow(qry_operators)>
        <cfset QuerySetCell(qry_operators, "operator_value", "Is") />
        <cfset QuerySetCell(qry_operators, "operator_name", "phr_email_Is") />

		<cf_translateQueryColumn query = "#qry_operators#" columnname = "operator_name" nullText= "">

		<cfreturn qry_operators />
	</cffunction>


	<cffunction access="remote" name="getTableRecipients" returntype="query" hint="gets columns with a personID in the name and also the accountmanager">
		<cfargument name="tablename" type="String" required="true" hint="Table from which you want the column list">

		<cfscript>
				//var qry_get_recipients = getTableColumns(tablename=arguments.tablename);
				var qry_get_recipients = application.com.dbTools.getTableDetails(tablename=arguments.tablename,returnColumns=true);
 		</cfscript>

		<cfset recipientFieldList = application.com.settings.getSetting("conditionalEmails.recipientFieldList_#arguments.tablename#")>
		<cfset recipientFlagList = application.com.settings.getSetting("conditionalEmails.recipientFlagList_#arguments.tablename#")>

		<cfquery name="qry_get_recipients" dbtype="query">
			select column_name as fieldName, 'phr_#arguments.tableName#_'+column_name as friendlyFieldName
			from qry_get_recipients
			where lower(column_name) like '%personid'
			<cfif recipientFieldList neq "">
				or column_name IN (#ListQualify(recipientFieldList,"'")#)
			</cfif>
		</cfquery>

		<cfif recipientFlagList neq "">
			<cfquery name="qry_get_recipient_flags" datasource="#application.siteDataSource#">
				<!--- select 'flag_'+CAST(flagID as varchar) as fieldName, 'phr_flag_'+flagTextID as friendlyFieldName  --->
				select 'flag_'+flagTextID as fieldName, 'phr_flag_'+flagTextID as friendlyFieldName
				from flag
				where flagTextID  IN ( <cf_queryparam value="#recipientFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			</cfquery>

			<cfquery name="qry_get_recipients" dbtype="query">
				select * from qry_get_recipients
				union
				select * from qry_get_recipient_flags
			</cfquery>
		</cfif>

		<cf_translateQueryColumn query=#qry_get_recipients# columnName="friendlyFieldName">

		<!--- if we're a person or location entity, then grab recipients from the organisation level as well, as a location doesn't have any recipients as a table --->
		<cfif application.entityTypeID[arguments.tablename] lt 2>
			<cfset polQuery = getTableRecipients(tablename=application.entityType[application.entityTypeID[arguments.tablename]+1].tablename)>

			<cfquery name="qry_get_recipients" dbtype="query">
				select * from polQuery
				union
				select * from qry_get_recipients
			</cfquery>
		</cfif>

		<cfreturn qry_get_recipients />
	</cffunction>


	<cffunction name="getFieldInputHTML" access="remote" returntype="string">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="columnName" type="string" required="true">
		<!--- 2015/08/18 DAN - optional arg which lets you return different html based on actual filter type --->
        <cfargument name="filterType" type="string" required="false" default="">

		<cfscript>
			var columnType = "";
			var name="filterValue";
			var id="emailFilterValue";
			var inputHTML = "";
			var entityStruct = structNew();
			var getEntities = "";
		</cfscript>

		<cfset columnType = getColumnType(tablename=arguments.tablename,columnName=arguments.columnName)>

		<cfsavecontent variable="inputHTML">
			<cfoutput>
                <!--- 2015/08/18 DAN - add support for is null/is not null in the conditional e-mails --->
                <cfif StructKeyExists(arguments,'filterType') and arguments.filterType eq "Is">
                    <select name="#name#" id="#id#">
                        <option value="NULL">Null</option>
                        <option value="NOT NULL">Not Null</option>
                    </select>
				<cfelseif columnType.type eq "boolean">
					<select name="#name#" id="#id#">
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>

				<cfelseif columnType.type eq "entity">
					<cfset entityStruct = application.entityType[columnType.entityTypeID]>

					<cfquery name="getEntities" datasource="#application.siteDataSource#">
						select #entityStruct.uniqueKey# as ID, #preserveSingleQuotes(entityStruct.nameExpression)# as name
						from #entityStruct.tablename#
						order by #preserveSingleQuotes(entityStruct.nameExpression)#
					</cfquery>

					<cf_relayFormElement relayFormElementType="select" label="" fieldname="#name#" id="#id#" currentValue="" query="#getEntities#" display="name" value="ID" useCFForm=false multiple="true" size="5">
<!---
				PPB: I considered that datefilters ought to use a date-picker but then realised that they operate on the basis that we send a mail if the filter date is +- num days from today so we enter an integer as the filter value not a date

				<cfelseif columnType.type eq "date">
					<cf_relayFormElement relayFormElementType="date" label="" fieldname="#name#" id="#id#" currentValue="" formName="editScheduledEmailForm" useCFForm="false">
 --->
				<cfelse>
					<CF_INPUT type="text" name="#name#" id="#id#" value="">
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn inputHTML>
	</cffunction>

	<!--- 2013-06-21	YMA	Format email content to output as message. --->
	<cffunction name="testEmail" access="remote" returnType="string">
		<cfargument name="emaildefid" type="numeric" required=true>
		<cfargument name="frmCountryID" type="numeric" default=#request.relaycurrentuser.languageid#>
		<cfargument name="frmLanguageID" type="numeric" default=#request.relaycurrentuser.countryid#>
		<cfargument name="testSendMessage" default="0"  required=true>

		<cfset var message = "">
		<cfset var onload = "">
		<cfset var thisLabel = "">
		<cfset var emailList = "">
		<cfset var i = "">

		<cfset details = application.com.email.testEmail(argumentcollection=arguments)>

		<cfsavecontent variable="result">
			<cfset emailList = "toAddress,ccaddress,bccaddress,fromAddress,subject,body,messageTitle,messageText,messageURL,messageImage">
			<cfloop list="#emailList#" index="i">
				<cfif structKeyExists(details,i)>
					<cfset thisLabel="<b>#i#</b>">
					<cfswitch expression="#i#">
						<cfcase value="messageImage">
							<cfset message = message & '<br/>' & thisLabel & ': <br/><image src="' & details[i] & '">'>
						</cfcase>
						<cfcase value="toAddress,ccaddress,bccaddress,fromAddress,subject,MessageTitle,messageURL">
							<cfset message = message & '<br/>' & thisLabel & ': ' & details[i]>
						</cfcase>
						<cfcase value="body,MessageText">
							<cfif details.cfmailtype eq "text">
								<cfset message = message & '<br/>' & thisLabel & ': <pre id="#i#">' & htmlEditFormat(details[i]) & '</pre>'>
							<cfelse>
								<cfset message = message & '<br/>' & thisLabel & ': <br/><div style="display:none" id="#i#">' & details[i] & '</div><iframe width="100%" id="#i#Iframe"></iframe>'>
								<!--- NJH 2016/08/10 JIRA PROD2016-380 - append onload to onload --->
								<cfset onload= onload & "<script>
										populateIframe#i# = function() {
											content = jQuery('div###i#').html();
											jQuery('<iframe id=""#i#Iframe""/>').append('body');
											jQuery('###i#Iframe').contents().find('html').append(content);
											jQuery('div###i#').html('');
										}
										setTimeout (populateIframe#i#,500)
									</script>">
							</cfif>
						</cfcase>
					</cfswitch>
				</cfif>
			</cfloop>

			<cfif testSendMessage is 1 >
				<cfset message = application.com.relayUI.message(message=message,type="info",doNotSanatiseHTML=1)>
				<cfoutput>#message# #onload#</cfoutput>
			<cfelse>
<!---ESZ 16/09/2016 PROD2016-379 Standard Emails - The 'preview' button is not working.--->
				<cfset message = message & '<p style="color: Red;">' & application.com.email.sendEmail(emailTextID=emaildefID,personid=request.relaycurrentuser.personid, languageid = frmLanguageID, countryid = frmCountryid,test=true, mergeStruct = application.com.email.getEntityMergeStruct(emaildefid)) & '</p>'>

				<cfset message = application.com.relayUI.message(message=message,type="info",doNotSanatiseHTML=1)>
				<cfoutput>#message# #onload#</cfoutput>
			</cfif>
		</cfsavecontent>

		<cfreturn result>

	</cffunction>

    <!--- 2015-10-13  DAN PROD2015-160 --->
    <cffunction access="remote" name="getEmailDefIDByTextID" returntype="numeric">
        <cfargument name="emailTextID" type="string" required="true">

        <cfreturn application.com.email.getEmailDefIDByTextID(emailTextID = arguments.emailTextID)>
    </cffunction>

</cfcomponent>
