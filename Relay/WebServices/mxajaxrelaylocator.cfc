<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Alex Connell
Date created:	2007-12-07

	This provides the extended mxAjax functionality for Locator

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
--->


<cfcomponent extends="relay.com.mxajax">

	<cffunction name="getAutoCompleteTown" access="remote" returntype="query" hint="Returns a set of towns for a passed town name part">
		<cfargument name="town" type="string" default="">
		<cfargument name="countryID" type="numeric" default="">

		<cfset var qry_get_SelectTowns = "">
		<cfset var qry_locatorTowns = "">

		<!--- <cfquery datasource="#application.sitedatasource#" name="qry_get_SelectTowns">
			SELECT Code AS [KEY], Name as [VALUE] from state WHERE [search] like '#lcase(urlDecode(searchCharacter))#%'
		</cfquery> --->
		<!--- <cfset application.com.relayLocator.getLocatorTowns(countryID=session.locatorCountryID) --->


		<!--- <cfquery datasource="#application.sitedatasource#" name="qry_get_SelectTowns">
			SELECT Code AS [KEY], Name as [VALUE] from state WHERE [search] like '#lcase(urlDecode(searchCharacter))#%'
		</cfquery> --->

		<cf_include template="\code\cftemplates\locatorINI.cfm" checkIfExists="true">
		<!--- <cfif fileexists("#application.paths.code#\cftemplates\locatorINI.cfm")>
			<!--- locator INI is used to over-ride default global application variables and params --->
			<cf_includeonce template="/code/cftemplates/locatorINI.cfm">
		</cfif> --->

		<cfset qry_locatorTowns = application.com.relayLocator.getLocatorTowns(countryID=arguments.countryID)>
		<cfset stripedTown = application.com.relayLocator.replaceSpecialCharacters(arguments.town)>

		<cfquery name="qry_get_SelectTowns" dbtype="query">
			select place as [KEY], place as [VALUE], place from qry_locatorTowns
				where ajaxplace like '#lcase(stripedTown)#%'
		</cfquery>

						<!--- where #replaceStatement#lower(place)#replace(replaceValueStatement,"**","'","ALL")# like '#lcase(arguments.town)#%' --->
		<!--- <cfset qry_get_SelectTowns = querynew("KEY,VALUE")>
		<cfset QueryAddRow(qry_get_SelectTowns)>
		<cfset QuerySetCell(qry_get_SelectTowns, "KEY", "Alex#qry_locatorTowns.recordcount#")>
		<cfset QuerySetCell(qry_get_SelectTowns, "VALUE", "Alex_#arguments.countryID#_#qry_locatorTowns.recordcount#")>
		<cfset QueryAddRow(qry_get_SelectTowns)>
		<cfset QuerySetCell(qry_get_SelectTowns, "KEY", "Nat12")>
		<cfset QuerySetCell(qry_get_SelectTowns, "VALUE", "nat12")>
		 --->
		<cfreturn qry_get_SelectTowns>

	</cffunction>

</cfcomponent>
