<!--- �Relayware. All Rights Reserved 2014 

2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).
2015-10-07 WAB 		PROD2015-165 Security Scan.  Prevent SQL injection into filterTypeVariable

--->
<cfcomponent output="false">
	<!--- 	
	<cffunction name="follow" access="remote" output="false" returnType="boolean">
		<cfargument name="personID" type="numeric" required="true" encrypted="true">
		<cfargument name="action" type="string" default="start">
		<cfargument name="entityTypeID" type="numeric" default="0">

		<cftry>
			<cfquery name="follow" datasource="#application.siteDataSource#">
				<cfif arguments.action eq "stop">
				delete from socialFollow where personID=#request.relayCurrentUser.personID#
					and followEntityTypeID=#application.entityTypeID.person# and followEntityID=#arguments.personID#
				<cfelse>
					if not exists(select 1 from socialFollow where personID=#request.relayCurrentUser.personID# and followEntityTypeID=#application.entityTypeID.person# and followEntityID='#arguments.personID#')
						insert into socialFollow (personID,followEntityTypeID,followEntityID,lastUpdatedBy,lastUpdatedByPerson,createdBy) 
						values (#request.relayCurrentUser.personID#,#application.entityTypeID.person#,#arguments.personID#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.personID#,#request.relayCurrentUser.userGroupID#)
				</cfif>
			</cfquery>
		
			<cfcatch>
				<cfreturn false>
			</cfcatch>
		</cftry>
		
		<cfreturn true>
	</cffunction> 
	--->

	<cffunction name="getActivityStream" access="remote" output="true" returnType="string">
		<cfargument name="pageNumber" type="numeric" default="2">
		<cfargument name="filterType" type="variableName" default="All"> <!--- WAB 2015-10-07 PROD2015-165 Security Scan.  This variable is open to SQL injection, can protect by forcing it to be a variableName (ie only alphaNumeric)--->

		<cfreturn application.com.social.getActivityStream(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="getNewActivityFeeds" access="remote" returnType="struct">
		<cfargument name="postedSince" type="date" default="#request.requestTime#">
		<cfargument name="filterType" type="string" default="Organisation">
		<cfargument name="cacheQuery" type="boolean" default="false">		
		
		<cfset var result = {content="",lastPost=arguments.postedSince}>		
		<cfset var activities = application.com.social.getActivitiesForConnections(argumentCollection=arguments)>
		
		<cfset result.content = application.com.social.displayActivityRows(activityQuery=activities.recordSet)>
		<cfif activities.recordSet.recordCount>
			<cfset result.lastPost = activities.recordSet.created[1]>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="saveLike" access="remote" returnformat="plain" returntype="any">	
		<cfargument name="entityID" type="numeric" required="true">	
		<cfargument name="entityTypeID" type="numeric" required="true">
		
		<cfset var qSaveLike = "">
		<cfset var qGetLike = "">
		
		<cfquery name="qSaveLike" datasource="#application.siteDataSource#">
			IF EXISTS (select * from entityLike where entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER"> and entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER"> and personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">)
				BEGIN
				    DELETE FROM entityLike
					WHERE entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
					AND entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
					AND personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">					
				END	
			ELSE
				BEGIN
				    INSERT INTO entityLike
						(entityTypeID,entityID,personID)
					VALUES
						(<cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">)
				END					
		</cfquery>
		
		<cfquery name="qGetLike" datasource="#application.siteDataSource#">
			SELECT count(personID) as likeCount
			FROM entityLike 
			WHERE entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
			AND entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		
		<cfreturn "#qGetLike.likeCount#">
	</cffunction>


	<cffunction name="getPersonSocialProfile" access="remote" returntype="string" requiresXSRFToken="false">
		<cfargument name="personID" type="numeric" required="true" encrypted="true">
		<cfargument name="readonly" type="boolean" required="true" default="true" encrypted="true">
		
		<cfset var content = "">
		<cfset var frmpersonID = arguments.personID>
		
		<cfsavecontent variable="content">
			<cfoutput>
				<cfinclude template="/social/personPublicProfile.cfm">
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn content>
	</cffunction>


	<cffunction name="saveEntityComment" access="remote">
		<cfargument name="entityCommentID" required="false" default="">
		<cfargument name="entityTypeID" required="true">	 
		<cfargument name="entityID" required="true">	
		<cfargument name="comment" type="string" required="false">
		
		<cfreturn application.com.entityFunctions.saveEntityComment(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="getEntityComment" access="remote">
		<cfargument name="entityTypeID" required="true">
		<cfargument name="entityID" required="true">
		<cfargument name="cacheQuery" required="false" default="false">
				
		<cfreturn application.com.entityFunctions.getEntityComment(argumentCollection=arguments).recordSet>
	</cffunction>
	
	
	<cffunction name="countEntityComment" access="remote">
		<cfargument name="entityTypeID" required="true">
		<cfargument name="entityID" required="true">
		
		<cfreturn application.com.entityFunctions.countEntityComment(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="deleteEntityComment" access="remote">
		<cfargument name="entityCommentID" type="numeric" required="true" default="">
		<cfreturn application.com.entityFunctions.deleteEntityComment(argumentCollection=arguments)>
	</cffunction>	

	<cffunction name="shortenURL" access="remote" returntype="struct">
		<cfargument name="url" type="string" required="true">
		
		<cfreturn application.com.social.shortenURL(url=arguments.url)>
	</cffunction>

	<cffunction name="checkAccountUnique" access="remote" returntype="struct">
		<cfargument name="account" type="string" required="true">
		<cfargument name="twitterAccountID" type="string" required="true">
		
		<cfset var result = {accountExists=false}>
		<cfset var doesAccountExist = "">
		
		<cfquery name="doesAccountExist" datasource="#application.siteDataSource#">
			select 1 from twitterAccount where account=<cf_queryparam value="#trim(arguments.account)#" cfsqltype="cf_sql_varchar">
				and twitterAccountID != <cf_queryparam value="#arguments.twitterAccountID#" cfsqltype="cf_sql_integer">
		</cfquery>
		
		<cfset result.accountExists = doesAccountExist.recordCount>
		
		<cfreturn result>
	</cffunction>

	<cffunction name="saveFollow" access="remote" returntype="struct" output="false" returnFormat="json">	
		<cfargument name="entityID" type="numeric" required="true" encrypted="true">	
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="personid" type="numeric" default="#request.relayCurrentUser.personId#">	
			
		<cfset var selectionTitle = "">
		<cfset var selectionID = "">
		<cfset var qEntity = queryNew("")>
		<cfset var qUserGroup = "">
		<cfset var result = {isOK = true,message=""}>

		<cfif structKeyExists(application.entityType,arguments.entityTypeID)>
				
			<cfif application.entityTypeID.commFromDisplayName is arguments.entityTypeID>
				<cfquery name="qEntity" datasource="#application.siteDataSource#">
					SELECT isNull(cfdn.displayName,'#application.EmailFromDisplayName#') as name
					FROM commFromDisplayName cfdn
					WHERE cfdn.commFromDisplayNameID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
				</cfquery>
				
			<cfelseif application.entityTypeID.usergroup is arguments.entityTypeID or application.entityTypeID.entityComment is arguments.entityTypeID>
				<cfquery name="qEntity" datasource="#application.siteDataSource#">
					SELECT firstname + ' ' + lastname as name
					FROM person
					WHERE personid = <cf_queryParam value="#arguments.personid#" cfsqltype="CF_SQL_INTEGER">
				</cfquery>
			</cfif>			

			<cfif qEntity.recordCount>
				<cfset selectionTitle = qEntity.name & " Followers">
				<cfset qSelection = application.com.selections.getSelections(followEntityId=arguments.entityID,followEntityTypeID=arguments.entityTypeID,personID=arguments.personID)>

				<cfif not qSelection.recordCount>
					<cfset selectionID = application.com.selections.createSelection(selectionTitle=selectionTitle,selectionDescription="Portal Users following #qEntity.name#",RefreshRate="N",selectionCreatedBy=arguments.personID,followEntityTypeID=arguments.entityTypeID,followEntityID=arguments.entityID)>	
					<cfif selectionID eq 0>
						<cfset result.message = "Error: Failed to create selection">
						<cfset result.isOK = false>
					</cfif>			
				<cfelse>
					<cfset selectionID = qSelection.SelectionID>
				</cfif>
				
				<cfif application.com.selections.isPersonInSelection(selectionID=selectionID,personID=request.relayCurrentUser.personid)>		
					<cfset application.com.selections.deletePersonFromSelection(selectionID=selectionID,personid=request.relayCurrentUser.personid)>
					<cfset result.message = "Deleted">
				<cfelse>				
					<cfset application.com.selections.insertSelectionPeople(selectionID=selectionID,personIDsForSelection=request.relayCurrentUser.personid)>
					<cfset result.message = "Inserted">
				</cfif>
				
			<cfelse>
				<cfset result.message = "Error: Invalid entityID">
				<cfset result.isOK = false>
			</cfif>	
					
		<cfelse>
			<cfset result.message = "Error: Invalid entityTypeID">
			<cfset result.isOK = false>
		</cfif>

		<cfreturn result>
	</cffunction>	

	
	<cffunction name="saveFlag" access="remote" returnformat="plain" returntype="any" output="false">	
		<cfargument name="entityID" type="numeric" required="true">	
		<cfargument name="entityTypeID" type="numeric" required="true">
		
		<cfset var qSaveFlag = "">		
		
		<cfquery name="qSaveFlag" datasource="#application.siteDataSource#">
			IF EXISTS (select * from entityTag where entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER"> and entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER"> and personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">)
				BEGIN
				    DELETE FROM entityTag
					WHERE entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
					AND entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">
					AND personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">					
				END	
			ELSE
				BEGIN
				    INSERT INTO entityTag
						(entityTypeID,entityID,personID)
					VALUES
						(<cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">,
						<cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">)
				END
								
			SELECT count(personID) as flagCount
			FROM entityTag 
			WHERE entityTypeID = <cf_queryParam value="#arguments.entityTypeID#" cfsqltype="CF_SQL_INTEGER">
			AND entityID = <cf_queryParam value="#arguments.entityID#" cfsqltype="CF_SQL_INTEGER">				
		</cfquery>		
		
		<cfreturn "#qSaveFlag.flagCount#">
	</cffunction>
	
	
	<cffunction name="addElementShare" access="remote" output="false" returnType="struct">
		<cfargument name="elementID" type="numeric" required="true">
		<cfargument name="url" type="string" required="true">
		<cfargument name="service" type="string" required="true">
		
		<cfset var addElementShared = "">
		<cfset var result = {isOK=true}>
		<cfset var serviceID = application.com.service.getService(serviceID=arguments.service).serviceID>
		
		<cfquery name="addElementShared" datasource="#application.siteDataSource#">
			insert into elementShare (personID,elementID,entityTrackingUrlID,serviceID) values (<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_numeric">,<cf_queryparam value="#arguments.elementID#" cfsqltype="cf_sql_numeric">,<cf_queryparam value="#application.com.entityFunctions.addEntityTrackingUrl(pageURL=arguments.url).ID#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#serviceID#" cfsqltype="cf_sql_numeric">)
		</cfquery>
		
		<cfreturn result>
	</cffunction>

	<cffunction name="orgPublicProfileEmails" access="remote" returnType="query">
		<cfargument name="contactType" required="false">
		
		<cfset var orgPublicProfileEmails = QueryNew("value,display")>
		
		<cfif structKeyExists(arguments,"contactType") and arguments.contactType neq 'noContact'>
			<cfquery datasource="#application.siteDataSource#" name="orgPublicProfileEmails">
				select emailtextID as value, name as display 
				from emaildef 
				<cfif structKeyExists(arguments,"contactType") and arguments.contactType eq 'emailDontCreatePerson'>
					where emailTextID = 'LocatorPartnerLead' 
				</cfif>			
				order by name
			</cfquery>
		</cfif>
		
		<cfreturn orgPublicProfileEmails/>
	</cffunction>
	
</cfcomponent>
