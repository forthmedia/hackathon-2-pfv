<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

webservices\nosession\remote.cfm

WAB 2009/01/27

This file is called by the tracking link in our email communications

By being in the nosession directory we don't have to spend time creating the relaycurrentuser structure.  This makes everything much quicker

 --->
<!--- PJP 2012-09-04: Case 430418: Added in request.noSession test so redirect works on no sessions --->
<cfset request.noSession = true>

<cfinclude template = "\relay\remote.cfm">

