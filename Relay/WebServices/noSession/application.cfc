<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="applicationMain">

	<cfset this.sessionManagement = false>

	<!--- Don't want relaycurrentuser to be started up so we need a blank onrequeststart --->	
	<cffunction name="onRequestStart" returnType="void">
	
	</cffunction>

	<!--- don't want the special onRequestEnd processing to run (among other things, requires relayCurrentUser) --->
	<cffunction name="onRequestEnd" returnType="void">
	
	</cffunction>

</cfcomponent>