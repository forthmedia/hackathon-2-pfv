<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		akamai.cfm
Author:			RMB
Date created:	2012-05-021

	Handles redirects to Akamai from portals / internal sites. Needed to ensure that the cookie is writen from a *.relayware domain. Without this it won't be passed to the akamai *.relayware.com domain
	This won't work for secure content where the domain is not *.relayware.com

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:

--->

<CF_decryptformstatestring>
<CF_checkFieldEncryption fieldNames="cookieName,accessPathList,accessPath,akamaiDomain">

<cffunction name="stripAccessPaths" output="false">
	<cfargument name="stripPath" default = "" type="string">

	<cfset var CheckLen = "#LEN(stripPath)#">
	<cfset var CheckFileInList = "#FINDNOCASE("~", stripPath)#">
	<cfset var EndTheMid = "">
	<cfset var ReturnThis = "#stripPath#">

	<cfif CheckFileInList GT 0>
		<cfset EndTheMid = CheckLen - CheckFileInList>
		<cfset CheckFileInList = CheckFileInList + 1>
		<cfset ReturnThis = MID(stripPath, CheckFileInList, EndTheMid)>
		<cfif LEN(ReturnThis) GTE 3800>
			<cfset ReturnThis = stripAccessPaths(stripPath = ReturnThis)>
		</cfif>
	</cfif>

	<cfreturn ReturnThis>

</cffunction>

	<cfset sSalt = application.com.settings.getsetting('files.Akamai.AkamaiSaltKey')>
	<cfset javaEpoch = request.requestTime>
	<cfset nWindow = application.com.settings.getsetting('files.Akamai.AkamaiWindow')>
	<cfset expires="">

	<cfif StructKeyExists(cookie, url.cookieName)>

	<cfset GetCookie = "#cookie[url.cookiename]#">
	<cfset FindURLCKStart = "#FINDNOCASE("!access=", GetCookie)#">
	<cfset FindURLCKEnd = "#FINDNOCASE("!md5=", GetCookie)#">

		<cfif FindURLCKStart GT 0>
			<cfset FindURLCKStart = FindURLCKStart + 8>
			<cfset FindURLCKEnd = FindURLCKEnd - FindURLCKStart>
			<cfset CheckFile1 = "#MID(GetCookie, FindURLCKStart, FindURLCKEnd)#">
			<cfset CheckFileInList = "#FINDNOCASE(accessPathList, CheckFile1)#">
			<cfif CheckFileInList LTE 0>
				<cfset CheckFile1 = "#CheckFile1#~#accessPathList#">
				<cfset accessPathList = "#CheckFile1#">
			</cfif>
		<cfelse>
			<cfset accessPathList = "#accessPathList#">
		</cfif>
	<cfelse>
		<cfset accessPathList = "#accessPathList#">
	</cfif>

	<cfif LEN(accessPathList) GTE 3800>
		<cfset accessPathList = stripAccessPaths(stripPath = accessPathList)>
	</cfif>

	<cfif ((expires LTE 0) || (NOT IsNumeric(expires)))>
		<cfset expires = #int(javaEpoch.getTime()/1000)#>
	</cfif>

	<cfset expires = nWindow + expires>
	<cfset mac = LCASE(Hash('#expires##accessPathList##sSalt#'))>
	<cfset cookiestring = "expires=#expires#!access=#accessPathList#!md5=#mac#">

<cfset application.com.globalFunctions.cfcookie(name="#url.cookieName#", value="#cookiestring#", domain=".relayware.com")>

<cfoutput>
<meta http-equiv="refresh" content="0;URL='http://#url.akamaiDomain##url.accessPath#'" />
</cfoutput>