<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
2011/06/11		NJH
2011/11/21		RMB		P-REL101 - changed drop down of RelayFile link to show group heading with the type in brackets
2012-05-31		STCR	P-REL109 CASE 428533 duplicate file check.
2014-04-30 		NYB 		Case 439373
2014-08-13		RPW		Create ability to choose template for Standard Emails
2015-02-26		DXC		Case 443875 - issue with testFileURL & https. Treating all http status codes < 400 as success.
										and same checks for http in case it server side redirects to https
2015-08-05      DAN     K-1314 - allow filtering products by multiple media groups
2016-10-11		WAB		PROD 2016-2496 Insert File Link dialog failing with XSRF error because XSRF token not being added to webservice call.  Does not need token since these are genuine GETs
2016-11-28 atrunov	add getFileKeyWords function that  returns list of available keyWords for select box for RELAY_FILE_LIST tag functionality PROD2016-2812
--->
<cfcomponent>

	<cffunction name="callMethod" access="remote">
		<cfargument name="methodName" type="string" required="true">

		<cfset var result = "">

		<cfif request.relayCurrentUser.isLoggedIn>
			<cfset result = evaluate("#arguments.methodName#(argumentCollection=arguments)")>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- 2012-05-30 STCR P-REL109 CASE 428533 For file upload duplicate filename checking --->
	<cffunction name="checkFileUnique" hint="return struct to indicate whether matching filename exists" access="remote">
		<cfargument name="filetitle" type="string" default="" required="false" />
		<cfargument name="filetypeid" type="numeric" default="0" required="false" />
		<cfargument name="currentfileid" type="numeric" default="0" required="false" />

		<cfreturn application.com.fileManager.checkNameConflict(filetitle=arguments.filetitle,filetypeid=arguments.filetypeid,currentfileid=currentfileid) />
	</cffunction>


	<cffunction name="getFileGroups" access="remote" requiresXSRFToken="false">
		<cfargument name="fileTypeGroup" type="string" required="false">
		<cfargument name="fileTypeGroupID" type="string" required="false">	<!--- WAB CASE 426874/427357, added support for fileTypeGroupID.  The editor for fileList relayTag is passing in an ID --->
		<cfargument name="returnType" type="string" default="array">

		<cfset var getFileGroupsQry = application.com.fileManager.getFileTypes(argumentCollection=arguments,sortOrder="heading")>
		<cfset var result = "">

		<cfquery name="getFileGroupsQry" dbtype="query">
			select distinct heading,fileTypeGroupId from getFileGroupsQry
		</cfquery>

		<cfset result = getFileGroupsQry>

		<!--- use an array as ckeditor is expecting an array --->
		<cfif arguments.returnType eq "array">
			<cfset result = arrayNew(2)>
			<cfset result[1][1] = "Please select a Media Group">
			<cfset result[1][2] = 0>

			<cfloop query="getFileGroupsQry">
				<cfset result[currentRow+1][1] = heading>
				<cfset result[currentRow+1][2] = fileTypeGroupID>
			</cfloop>
		</cfif>

		<cfreturn result>

	</cffunction>

	<!--- 2013-07-02	YMA	Case 435912 prevent params from fileList.cfm erroring due to calling this function without specifying a fileTypeGroup --->
	<cffunction name="getFileTypes" access="remote" requiresXSRFToken="false">
		<cfargument name="fileTypeGroup" type="string" required="false">
		<!--- 2015-08-05 DAN K-1314 --->
		<cfargument name="fileTypeGroupID" type="ANY" required="false">	<!--- WAB CASE 426874/427357, added support for fileTypeGroupID.  The editor for fileList relayTag is passing in an ID --->
		<cfargument name="returnType" type="string" default="array">

		<cfset var getFileTypesQry = queryNew("type,fileTypeID")>
		<cfset var result = arrayNew(2)>

		<!--- 2015-08-05 DAN K-1314 --->
		<cfif structKeyExists(arguments,"fileTypeGroup") or (structKeyExists(arguments,"fileTypeGroupID") and arguments.fileTypeGroupID is not 0 and arguments.fileTypeGroupID is not "")>
			<cfset getFileTypesQry = application.com.fileManager.getFileTypes(argumentCollection=arguments)>
			<cfset result = getFileTypesQry>
		</cfif>

		<!--- use an array as ckeditor is expecting an array --->
		<cfif arguments.returnType eq "array">
			<cfset result = arrayNew(2)>
			<cfset result[1][1] = "Please select a Resource Type">
			<cfset result[1][2] = 0>

			<cfif getFileTypesQry.recordcount gt 0>
				<cfloop query="getFileTypesQry">
					<cfset result[currentRow+1][1] = type>
					<cfset result[currentRow+1][2] = fileTypeID>
				</cfloop>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

	<!--- 2016-11-28 atrunov	Function returns list of available keyWords for select box for RELAY_FILE_LIST tag functionality PROD2016-2812--->
	<cffunction name="getFileKeyWords" access="remote" requiresXSRFToken="false">
		<cfset var result = "">
		<cfset var qryGetFileKeyWords = "">

		<cfquery name="qryGetFileKeyWords">
			SELECT DISTINCT [DATA] from textmultipleflagdata
			WHERE flagID in (SELECT FlagID
								FROM flag f
				 				INNER JOIN FlagGroup fg on f.FlagGroupID = fg.FlagGroupID
				 				WHERE fg.FlagGroupTextID = 'fileskeywords')
		</cfquery>

		<cfset result = qryGetFileKeyWords>

		<cfreturn result>
	</cffunction>


	<cffunction name="getFiles" access="remote" requiresXSRFToken="false" >
		<cfargument name="fileTypeID" type="numeric" required="false">
		<cfargument name="fileTypeGroupID" type="numeric" required="false">

		<cfset var result = arrayNew(2)>
		<cfset var getFiles = "">

		<cfquery name="getFiles" datasource="#application.siteDataSource#">
			select distinct name, fileId from files f with (noLock)
				inner join fileType t with (noLock) on f.fileTypeID = t.fileTypeId
			where 1=1
				<cfif structKeyExists(arguments,"fileTypeID")>and f.fileTypeID=#arguments.fileTypeID#</cfif>
				<cfif structKeyExists(arguments,"fileTypeGroupID")>and t.fileTypeGroupID=#arguments.fileTypeGroupID#</cfif>
			order by name
		</cfquery>

		<cfloop query="getFiles">
			<cfset result[currentRow][1] = name & " ("&fileId&")">
			<cfset result[currentRow][2] = fileId>
		</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="getFileType" access="remote" requiresXSRFToken="false">
		<cfargument name="fileID" type="numeric" required="true">

		<cfset var getFileTypeQry = "">
		<cfset var result = 0>

		<cfquery name="getFileTypeQry" datasource="#application.siteDataSource#">
			select fileTypeID from files where fileID =  <cf_queryparam value="#arguments.fileID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif getFileTypeQry.recordCount>
			<cfset result = getFileTypeQry.fileTypeID[1]>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="getFileGroup" access="remote">
		<cfargument name="fileTypeID" type="numeric" required="true">

		<cfset var getFileGroupQry = "">
		<cfset var result = 0>

		<cfquery name="getFileGroupQry" datasource="#application.siteDataSource#">
			select fileTypeGroupID from fileType where fileTypeID =  <cf_queryparam value="#arguments.fileTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif getFileGroupQry.recordCount>
			<cfset result = getFileGroupQry.fileTypeGroupID[1]>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- NJH 2013/01/10 function to test whether a given url is valid. Used in file edit screen --->
	<cffunction name="testFileURL" access="remote" returnType="string">
		<cfargument name="fileURL" type="string" required="true">
		<cfhttp url="#arguments.fileURL#" result="urlResult" >
		<!--- start DXC Case 443875 - issue with testFileURL & https.  --->
		<cfif left(urlResult.statusCode,1) lt 4>
			<cfreturn true>
		</cfif>
		<!--- if it fails because of https check for http version and a 301 (moved) or 200 status --->
		<cfset tempURL=replacenocase(arguments.fileURL,"https:","http:","all")>
		<cfhttp url="#tempURL#" method="head" redirect="false">
		<Cfif left(cfhttp.statusCode,1) lt 4>
			<!--- allow all http status codes except errors (i.e. 100 - 308 - see http://en.wikipedia.org/wiki/List_of_HTTP_status_codes) --->
			<Cfreturn true>
		</Cfif>
		<cfreturn false>
		<!--- end DXC Case 443875 --->
	</cffunction>

	<!--- WAB 2016-03-02 CASE 448308 Add mandatory encryption of fileID --->
	<cffunction name="deleteFile" access="public" returnType="struct">
		<cfargument name="fileID" type="numeric" required="true" encrypted="true">

		<cfreturn application.com.filemanager.deleteFileFromFileLibrary(fileID=arguments.fileID)>
	</cffunction>

	<cffunction name="getFileFamilies" access="remote" returnType="query">
		<cfargument name="fileTypeID" required="false">

		<cfset var getFileFamiliesQry = queryNew("value,display")>

		<cfif structKeyExists(arguments,"fileTypeID") and isNumeric(arguments.fileTypeID)>
			<cfquery name="getFileFamiliesQry" datasource="#application.siteDataSource#">
				select distinct ff.fileFamilyID as value,ff.title as display
				from fileFamily ff
					inner join files f on ff.fileFamilyID = f.fileFamilyID
				where f.fileTypeID = <cf_queryparam value="#arguments.fileTypeID#" cfsqltype="cf_sql_integer">
				order by title
			</cfquery>
		</cfif>

		<cfreturn getFileFamiliesQry>
	</cffunction>

	<!--- 2013-07-01	YMA	Case 435986 After moving a file we don't show the correct file path.  It is necessary to call a webservice to get the new filepath. --->
	<cffunction name="getFileNameAndPath" access="public">
		<cfargument name="fileID" type="numeric" required="true">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">

		<cfset var hasRightsToFile = "">
		<cfset var fileNameAndPath = "">

		<cfif isNumeric(fileID) and fileID gt 0>
			<cfset hasRightsToFile = application.com.fileManager.getFilesAPersonHasEditRightsTo(personID=arguments.personID,fileID=arguments.fileID)>

			<cfif hasRightsToFile.recordCount>
				<cfset fileNameAndPath = hasRightsToFile.fileNameAndPath[1]>
			</cfif>
		</cfif>

		<cfreturn fileNameAndPath>
	</cffunction>

	<!--- START: 2014-04-30 NYB Case 439373 - added --->
	<cffunction name="getFileTypeDetails" access="remote" returnType="string">
		<cfargument name="fileTypeID" type="numeric" required="true">
		<cfargument name="detail" type="string" default="autounzip">

		<cfset var getFileTypeQry = "">
		<cfset var result = "">

		<cfquery name="getFileTypeQry" datasource="#application.siteDataSource#">
			select #detail# from filetype where filetypeID =  <cf_queryparam value="#arguments.filetypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif getFileTypeQry.recordCount>
			<Cfset result = getFileTypeQry[detail]>
			<cfset result = javacast ("string", result)>
		</cfif>

		<cfreturn result>
	</cffunction>
	<!--- END: 2014-04-30 NYB Case 439373 --->


	<!--- NJH 2013/09/20 - Sprint 1 case 435830 --->
	<cffunction name="checkFileExtension" access="remote" returnType="struct">
		<cfargument name="fileTypeID" type="numeric" required="false">
		<cfargument name="filename" type="string" required="true">

		<cfreturn application.com.filemanager.checkFileExtension(argumentCollection=arguments)>
	</cffunction>


	<!--- 2014-08-13		RPW		Create ability to choose template for Standard Emails --->
	<cfscript>
		remote struct function readFile
		(
			required numeric fileID
		)
		{

			var rtnStruct = {};

			var fileData = application.com.fileManager.readFile(arguments.fileID);

			rtnStruct["fileData"] = fileData;

			return rtnStruct;

		}

	</cfscript>


</cfcomponent>