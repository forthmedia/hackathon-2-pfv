<!--- �Relayware. All Rights Reserved 2014 

2016-04-05	WAB BF-246 Get UI related to setting variants to work again in the DIV world
--->
<cfcomponent output="false">

	<cffunction name="getAddVariantRows" access="remote" returnType="string">
		<cfargument name="variableName" type="string" required="true">
		
		<cfset XMLNodeArray = application.com.settings.getVariableNodeArray_Master(variablename)>
		<cfset XMLNode = XMLNodeArray [1]>

		<cfset result = application.com.settings.getAddVariantRowForNode(xmlnode)>
		<cfreturn result>
	</cffunction>

	<cffunction name="getVariantRows" access="remote" returnType="string">
		<cfargument name="variableName" type="string" required="true">
		
		<cfset XMLNodeArray = application.com.settings.getVariableNodeArray_Master(variableName)>
		<cfset XMLNode = XMLNodeArray [1]>

		<cfset result = application.com.settings.getVariantRowsForNode(XMLNode=xmlnode,level=2) >
		<cfreturn result>
	</cffunction>

	<!--- 2013-03-05	YMA	START: Case 433813 Use ajax to populate report options based on homepage view --->
	<cffunction name="DashboardNumberOptions" access="remote" hint="" output="false" validValues="true">
		<cfargument name="DashType" type="string" required="true">
		<cfreturn application.com.relayHomePage.DashboardNumberOptions(DashType=arguments.DashType)>
	</cffunction>

</cfcomponent>