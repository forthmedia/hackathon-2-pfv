<!--- 
WAB 2014-01-07

In CF10 all debug output is disabled when calling a webservice, so we can't debug easily.
For debugging purposes we can call this file instead.  Just change webservices.cfc to webservices.cfm
Theoretically it could always be used, but I don't think that I would recommend it.

See 
http://www.raymondcamden.com/index.cfm/2011/6/30/Changing-in-debugging-with-CFCs-in-ColdFusion-9

WAB 2014-05-21  Updated to reflect a bug fix in callWebService.cfc
--->

<cfparam name="webservicename" >
<cfparam name="methodname" >


<!--- call the method in callWebservice --->
<cfset obj = createObject("component","callWebservice")>
<cfset result = obj.callWebservice (argumentCollection = url)>

<!--- Look for a returnFormat on the URL and then at function level --->
<cfif structKeyExists (url,"returnFormat")>
	<cfset _returnFormat = url.returnFormat>
<cfelse>
	<!--- In case the returnFormat is defined at function level we will get the metadata and look --->
	<cfset method = obj.getComponentAndMethodPointer(argumentCollection = url, webservicesFolder="webservices").methodPointer>
	<cfset metaData = getMetaData (method)>

	<cfif structKeyExists (metaData,"returnFormat")>
		<cfset _returnFormat = metaData.returnFormat>
	<cfelse>
		<cfset _returnFormat = "">
	</cfif>
</cfif>

<!--- Now output the result in the required format, defaults to WDDX --->
<cfif _returnFormat is "JSON">
	<cfoutput>#serializeJSON(result)#</cfoutput>
<cfelseif _returnFormat is "Plain" and isSimpleValue(result) >
	<cfoutput>#result#</cfoutput>
<cfelse>
	<cfwddx action="cfml2wddx" input="#result#" output="wddx">
	<cfoutput>#wddx#</cfoutput>
</cfif>
