<cfcomponent output="false">
	
	<cffunction name="getNewMappingRow" access="remote" returnType="string">
		<cfset var rowHTML="">
		<cfscript>
			var samlAttributeManager=new  singleSignOn.common.AttributeManager();
			var personFieldsArray=samlAttributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.person);
			var personFields=ArrayToList(personFieldsArray);
			var personFieldsWithBlank=" ,"&personFields;
		</cfscript>

		
		<cfsavecontent variable="rowHTML">
			<cf_SAMLMappingTableRow
					currentField=""
					availableFields="#personFieldsWithBlank#"
					suppressScript="true"
				>
		</cfsavecontent>
	
		<cfreturn rowHTML>
	</cffunction>


	<cfscript>
		remote any function getLocationsAtOrganisation(required numeric organisationID) output="false" {
			
			var locQuery=new Query();
			locQuery.setDatasource(application.siteDatasource);
			locQuery.setName("locQuery"); 
			locQuery.addParam(name="organisationID",value=organisationID,cfsqltype="cf_sql_integer"); 
			var queryReturn=locQuery.execute(sql="select locationID as datavalue,SiteName as displayValue from location where organisationID = :organisationID" );
			
			
			return queryReturn.getResult();
		}
		
		remote string function processIDPMetadata(required string xmlString) output="false" {
	
			var returnStruct={isOk="true", message="", identityProviderEntityID="", certificate="", authnRequestURL="" };
	
			
			
			//we grab a java object to parse the string
			var idpMetadataProcessor = application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPMetadataProcessor");
			
			var parseResult=idpMetadataProcessor.readIDPMetadata(xmlString);
	
			returnStruct.isOk=parseResult.isValid();
			if (returnStruct.isOk){
				returnStruct.identityProviderEntityID=parseResult.getEntityID();
				returnStruct.authnRequestURL=parseResult.getSingleSignOnServiceURL_POSTBinding();
			}else{
				returnStruct.message=parseResult.getInvalidReason();
			}
	
			return SerializeJSON(returnStruct);
		}
	</cfscript>

	
</cfcomponent>