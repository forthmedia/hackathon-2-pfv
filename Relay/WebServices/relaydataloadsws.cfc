<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2011/05/04 added a new function to give progress of loading data into load table
		At same time converted all functions to make use of returnFormat=plain (removed <cfoutputs> and aborts and returned values instead)
		Most functions should be called through runMethod function bacause this does the iniTable for you
--->

<cfcomponent extends="relay.dataloadsV2.relayDataLoadFlagsV2">

	<cfif not request.relayCurrentUser.isLoggedIn or not request.relayCurrentUser.isInternal>
		<script>alert('You must be logged in')</script>
		<CF_ABORT>
	</cfif>

	<cffunction name="confirmAutoMappingRemote" access="remote" returnType="string">
		<cfargument name="loadTableColumn" required = "true">
		
		 <cfset confirmAutoMapping (loadTableColumn = loadTableColumn)>   
		
		<cfset getPageContext().getOut().clearBuffer()> <!--- otherwise some warnings might appear --->

		<cfreturn "Accepted">
	</cffunction>
	

	<cffunction name="deleteDataLoadMappingRemote" access="remote" returnType="string">
		<cfargument name="loadTableColumn" required = "true">
		
		 <cfset deleteDataLoadMapping (loadTableColumn = loadTableColumn)>   

		<cfset getPageContext().getOut().clearBuffer()> <!--- otherwise some warnings might appear --->

		<cfreturn "Removed">
	</cffunction>
	
	<cffunction name="ignoreColumnRemote" access="remote" returnType="string">
		<cfargument name="loadTableColumn" required = "true">
		
		 <cfset ignoreColumn (loadTableColumn = loadTableColumn)>   

		<cfset getPageContext().getOut().clearBuffer()> <!--- otherwise some warnings might appear --->

		<cfreturn "Ignored">
	</cffunction>
	
	

	<cffunction name="getRowCount" access="remote" returnType="struct">
		<cfargument name="tablename" required = "true">		
		<cfset result = {}>
		<cfquery name="get" datasource = "#application.sitedatasource#">
		if exists (select 1 from sysobjects where name =  <cf_queryparam value="#tableName#" CFSQLTYPE="CF_SQL_VARCHAR" > ) 
		BEGIN
			select count(*) as count from #tableName#
		END	
		ELSE
		BEGIN
			select 0 as count 
		END	
		</cfquery>
		<cfset result.count = get.count>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="runMethod" access="remote" >
		<cfargument name="tablename" required = "true">		
		<cfargument name="methodname" required = "true">		

		<cfset var thisFunction = this[methodname]>

		<cfset initTable(tablename = tablename)>
		<cfset structDelete (arguments,"tablename")>
		<cfset structDelete (arguments,"methodname")>
		<cfreturn thisFunction(argumentCollection = arguments)>

	</cffunction>
	
	


</cfcomponent>	

