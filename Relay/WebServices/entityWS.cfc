<!---
  --- entityWS
  --- --------
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   07/11/16
  --->
<cfcomponent accessors="true" output="false" persistent="false">


	<cffunction name="save" access="public" output="false" returnType="struct">
		<cfargument name="entityType" type="string" required="true">

		<cfset var saveresult =  application.com.entities.doUpsert (entityType = arguments.EntityType, entityID = arguments[application.com.relayEntity.getEntityType(arguments.entityType).uniqueKey], scope = arguments )>

		<!--- <cfset application.com.relayUI.setMessage(message = (saveresult.fieldsUpdated)?"Record #saveresult.entityID# saved":"Nothing to save", closeafter = 5)> --->
		<cfset saveResult.message = saveresult.fieldsUpdated?"Record #saveresult.entityID# saved":"Nothing to save">
		<cfreturn saveResult>
	</cffunction>


	<cffunction name="getEntityLookup" access="public" output="false" returnType="query">

		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="formElementID" type="numeric" required="true">

		<!--- <cfset var entityType = application.com.entities.getObjectAndFieldFromFieldName(fieldname=arguments.fieldname).objectName>
		<cfset var entity = application.com.entities.getEntityByID(entityType=entityType,entityID=arguments.entityID)>
		<cfset var formElementObj = application.com.formRenderer.getFormElementByID(formElementID=arguments.formElementID)>
		<cfreturn formElementObj.getAttribute("name")>

		<cfreturn >




		<cfquery name="local.getSQLForFieldname">
			select foreignKeySql from vFieldMetaData
			where tablename=<cf_queryparam value="#listFirst(arguments.fieldname,'_')#" cfsqltype="cf_sql_varchar">
				and fieldname=<cf_queryparam value="#replaceNoCase(arguments.fieldname,'_')#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfif local.getSQLForFieldname.recordCount and local.getSQLForFieldname.foreignKeySql neq "">
			<cfif arguments.filter neq "">
				<cfset foreignKeySql = local.getSQLForFieldname.foreignKeySql & arguments.filter>
			</cfif>
		</cfif>

		<cfif foreignKeySql neq "">
			<cfquery name="getForeignKeyData">
				#preserveSingleQuotes(foreignKeySql)#
			</cfquery>
		</cfif>

		<cfreturn getForeignKeyData> --->

	</cffunction>

</cfcomponent>