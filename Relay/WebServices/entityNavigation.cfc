<!--- �Relayware. All Rights Reserved 2014 --->
<!---

entityNavigation.cfc



	Webservice functions to bring back bits for ajax calls from data\entityNavigation

	WAB 2007-11-22
WAB 2008/02/20 Added function to do the recent Search Box.  Not sure this is logical palce for it!
WAB 2008/03/25 Added parameters so that getScreenList knows the country of the organisation/location
WAB 2008/12/05 BUG-1440	Problems with searching for accented characters etc.  Search Parameters now passed as an array and encoded by Javascript
WAB 2009/01/22  changes to display recent searches
NYB 2009/02/02 - Trend Support bug 1714 - added 'with (nolock)' to query
WAB 2012-02-09	Changed both functions to return values and leave CF to do the encoding based on returnFormat - had to update javascript as well
WAB 2016-05-04	BF-676 Knock on effect of PROD2016-878. Change calls to getScreenList to use argument entityTypeID not entityType.  Prevents any issues with organiSation and organiZation.  
--->

<cfcomponent output="false">

	<cffunction name="getEntityDetails" access="remote" returnType="struct" output="false">
		<cfargument name="entityTypeID">
		<cfargument name="entityID">
		<cfargument name="screenid">

		<cfset var result = structNew()>
		<cfset result.entity = structNew()>

		<cfset result.entity.defaultScreenID = "">

			<cfquery name="getEntity" datasource = "#application.sitedatasource#">
				select
					o.organisationname as organisation ,
					o.organisationid ,
					o.countryid as organisationCountryID ,
					<cfif entityTypeID lte 1>
						l.sitename  as location,
						l.locationid,
						l.countryid as locationCountryID ,
						<cfif entityTypeID is 0>
							p.firstname + ' ' + p.lastname  as person,
							p.personid,
						</cfif>
					<cfelse>
						'' as locationCountryID,
					</cfif>
				o.organisationTypeID
				from organisation o
					<cfif entityTypeID lte 1>
						inner join location l on l.organisationid = o.organisationid
						<cfif entityTypeID is 0>
						inner join person p on p.locationid = l.locationid
						</cfif>
					</cfif>
				where #left(application.entitytype[EntityTypeID].tablename,1)#.#application.entitytype[EntityTypeID].uniquekey# = <cf_queryparam value="#EntityID#"  CFSQLTYPE="CF_SQL_INTEGER">
			</cfquery>

			<cfset result.entity.name =  structNew()>
			<cfset result.entity.id =  structNew()>
			<cfloop index="i" from="2" to = #entityTypeID# step = -1>
				<cfset result.entity.name[i] = getEntity[application.entitytype[i].tablename][1]>
				<cfset result.entity.id[i] = getEntity[application.entitytype[i].uniquekey][1]>
			</cfloop>
			<cfset result.entity.organisationTypeID = getEntity.organisationTypeID>
			<cfset result.entity.entityid = entityID>
			<cfset result.entity.entitytypeid = entitytypeID>
			<cfset result.entity.entitytype = application.entitytype[EntityTypeID].tablename>

		 <!--- get a list of active screens  --->
			<cfset ScreenList = application.com.screens.getScreenList(entityTypeID = EntityTypeID, organisationTypeId = getEntity.organisationTypeID, CountryId = getEntity.LocationCountryId, organisationCountryId = getEntity.organisationCountryId, includeParentTypes = true)>
			<cfset result.ScreenStruct = application.com.structureFunctions.queryToStruct(query = screenlist, key="screenid",excludeColumns="groupingsort,sortorder,entityType")>
			<!--- also put in screentextids as keys in the strucure, means we don't have to come back here to look up a screentextid --->
			<cfquery name="getScreenTextIDs" dbtype="query">
			select * from screenlist where screentextid <> ''
			</cfquery>
 			<cfset structAppend(result.ScreenStruct,application.com.structureFunctions.queryToStruct(query = getScreenTextIDs, key="screentextid",excludeColumns="groupingsort,sortorder,entityType"))>

			<cfset result.requestedScreenid = "">
			<!--- get details of requested screen - may or may not be in the list so am doing a full query --->
			<cfif screenid is not "">
				<cfquery name="getRequestedScreen" datasource = "#application.sitedatasource#">
				select screenid, rtrim(screenname) as screenname, entityType, entityTypeID,  0 as nes,  '' as grp
				from screens
				where
					<cfif isNumeric(screenid)>
						screenid = <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfelse>
						screentextid = <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>

				</cfquery>
				<cfset result.requestedscreenid = getRequestedScreen.screenid>
				<cfif not structKeyExists(result.ScreenStruct,result.requestedscreenid)>
					<cfset structAppend(result.ScreenStruct, application.com.structureFunctions.queryToStruct(query = getRequestedScreen, key="screenid"))>
				</cfif>
			</cfif>


<!--- 			decided to return a default screen in all cases
				<cfif result.requestedScreenid is ""> --->
				<cfset result.entity.defaultScreenID = application.com.screens.getdefaultScreen(entityType = result.entity.entitytype, organisationTypeID = result.entity.organisationTypeID).screenid>  <!--- functionreturns a query (may have zero records), we just need the screenid --->
				<!--- need to be sure that the user has rights to this screen - ie that this id appears in the query of screens --->
				<cfif listfind (valuelist(screenlist.screenid), result.entity.defaultScreenID) is 0>
					<!--- if not in list then can't use it, so blank out the default - will be set to first in list later --->
					<cfset result.entity.defaultScreenID = "">
				</cfif>

<!--- 			</cfif> --->

			<!--- if no default screen then take the first item in the list --->
			<cfif result.entity.defaultScreenID is "">
				<cfset result.entity.defaultScreenID = screenlist.screenid>
			</cfif>

			<cfset result.ScreenArray = listtoarray(valuelist(screenlist.screenid))>

		<cfreturn result>
	</cffunction>

	<!--- 2014/11/07	YMA	Return a html list of entity screens for fancybox popup over 3 pane view.  This is a replacement of standard grouped dropdown. 
		WAB 2016-05-04	BF-676 Knock on effect of PROD2016-878. Change class names to lower case (and change css at same time)
	--->
	<cffunction name="getEntityScreenList" output="false">
		<cfargument name="entityTypeID">
		<cfargument name="entityID">
		<cfargument name="screenid">

		<cfset var getEntity=application.com.relayentity.getEntityStructure(entityTypeID=arguments.entityTypeID,entityID=arguments.entityID)>
		<!--- get list of screens available --->
		<cfset var screensList=application.com.screens.getScreenList(
									entityTypeID = entityTypeID,
									organisationTypeId = getEntity.organisationTypeID,
									countryId = getEntity.countryId,
									organisationCountryId = getEntity.countryIds.organisation,
									includeParentTypes = true
									)><!--- call to function to get list of screens available --->
									<!--- NJH 2016/04/06 sugar 448818 - pass in sortorder so that the 'More' link in 3-pane view shows screen in alphabetical order.--->
		<cfset var entityScreenList="">

		<cfset var entityTypeList = application.com.globalFunctions.RemoveListDuplicates(list=valueList(screensList.ENTITYTYPE))>

		<!--- output list of screens to HTML --->
		<cfsavecontent variable="entityScreenList">
			<cfoutput>
				<div id="entityScreenList" class="entityScreenListCol#listLen(entityTypeList)#">
					<cfloop query="screensList" group="ENTITYTYPEID">
						<div id="#lcase(screensList.ENTITYTYPE)#">
							<h4>#GRP#</h4>
							<ul>
								<cfloop>
									<li id="#screensList.SCREENTEXTID#">
										<a href="##" onClick="jQuery().fancybox.close;changeView(#screensList.screenID#);">#screenName#</a>
									</li>
								</cfloop>
							</ul>
						</div>
					</cfloop>
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn entityScreenList>
	</cffunction>


	<!--- recent search box for left menu

		WAB 2008/12/05 BUG-1440	problems when there are accented characters, &s, %s etc in the search
			Now pass a JS array to the search function
			Also displays all the search parameters on the list (eg if search was on lastname and organisation name)

		WAB 2009/01/22  Displays criteria nicer - uses relayplo function to parse the string. Displays a cut down version and full version in title
	--->
	<cffunction name="getRecentSearches" access="remote" returnType="string" output="false">
		<cfset var getPreviousSearches="">
		<cfset var item="">
		<cfset var searchName="">
		<cfset var searchNameJS="">
		<cfset var searchStringStructure ="">
		<cfset var SearchStringJSArray	="">
		<cfset var parseSearchString  ="">
		<cfset var result = "">

		<cfparam name="request.menuEntitySearchList" type="string" default="frmSiteName,frmFullName,frmFirstName,frmLastName,frmEmail,frmsrchOrgID">

		<!--- NYB 2009/02/02 - bug 1714 - added with (nolock) --->
		<cfquery name="getPreviousSearches" datasource = "#application.sitedatasource#">
			select top 10 searchString, max(searchdate)
			from tracksearch  with (nolock)
			where searchType = 'D' and personid = #request.relaycurrentuser.personid#
			and searchDate > DATEADD(year,-1,GETDATE())
			group by searchString
			order by max(searchdate) desc
		</cfquery>

		<cfsaveContent variable="result">
			<cfoutput>
				<h4>Recent Searches</h4>
				<ul>
				<cfloop query = "getPreviousSearches">
					<li><a href="" onclick="return loadRecentSearch('#JSStringFormat(searchString)#');" title="">#htmleditformat(searchString)#</a></li>
				</cfloop>
				</ul>
			</cfoutput>
		</cfsaveContent>
		<cfreturn application.com.regExp.removeWhiteSpace(result)>
	</cffunction>

</cfcomponent>

