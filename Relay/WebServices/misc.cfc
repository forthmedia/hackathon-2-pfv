<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>
	<!---

	WAB 2007-06-11 changed isFlagtextIDUnique to use function in flag.cfc
	 --->




	<cffunction name="incrementCachedContentVersionNumber" access="remote" returnType="string">
		<!--- user must be logged in to an internal site (probably ought also to be a content editor --->
		<cfif not request.relaycurrentuser.isLoggedIn or not request.relaycurrentuser.isInternal>
				<script>
				alert ('Failed')
				</script>
<cfreturn "">

		<cfelse>
			<cfset x = application.com.relayElementTree.incrementCachedContentVersionNumber(updateCluster = true)>
			<cfoutput>
				<script>
				alert ('Done (#jsStringFormat(x)#)')
				</script>
			</cfoutput>


			<cfreturn "">
		</cfif>

	</cffunction>


	<!---
	This function works out whether a flagTextID is unique
	It return the value in the X-JSON header so that the result can be accessed in the second parameter of the ajax return function
	 --->
	<cffunction access="remote" name="isFlagTextIDUnique" returntype="boolean">
		<cfargument name="FlagTextID" type="string" required="true">
		<cfargument name="entityType" type="string" required="true"> <!--- NJH 2016/12/14 - check against the entity columns as well --->
		<cfargument name="flagTypeID" type="numeric" required="true">

		<cfreturn application.com.flag.isFlagTextIDUnique(argumentCollection=arguments)>
	</cffunction>


	<!---
	This function works out whether a flagTextID is unique
	It return the value in the X-JSON header so that the result can be accessed in the second parameter of the ajax return function
	 --->
	<cffunction access="remote" name="isFlagGroupTextIDUnique"  returntype="boolean">
		<cfargument name="FlagGroupTextID" type="string" required="true">
		<cfargument name="entityType" type="string" required="true"> <!--- NJH 2015/09/24 - check against the entity columns as well --->

		<!--- Also need to check against the POL fields --->
		<cfreturn application.com.flag.isFlagGroupTextIDUnique(FlagGroupTextID=arguments.FlagGroupTextID,entityType=arguments.entityType)>
	</cffunction>

	<!---
	 --->
	<cffunction access="remote" name="getClientIPAddress"  returntype="string">

	<cfheader    name = "X-JSON"   value = "#application.com.mxAjax.jsonencode(cgi.remote_Addr)#"   >

<CF_ABORT>
<cfreturn "">

	</cffunction>




</cfcomponent>
