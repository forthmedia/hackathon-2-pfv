<!--- �Relayware. All Rights Reserved 2014 

2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).

--->
<cfcomponent output="false">

	<!--- 2013 Release 2 Item 37 - function to show related records for the SF error report --->
	<cffunction name="getRelatedErrorRecords" access="remote" returnType="string">
		<cfargument name="relatedRecordID" type="string" required="true">
		<cfargument name="relatedObject" type="string" default="opportunity">
		
		<cfset var result = "">
		<cfset var relatedRecords = application.com.salesForce.getSalesForceErrorRecords(argumentCollection=arguments)>
		<cfset var thStyle = "background-color:##E8E9ED;">
		
		<cfsavecontent variable="result">
			<cfoutput>
				<tr id="relatedRecords"><td colspan="10">
				<div id="relatedRecordDiv" style="display:none;width:100%;padding-left:10px;padding-right:10px;">
					<table style="width:95%;" class="withBorder">
						<tr>
							<th style="#thStyle#"></th>
							<th style="#thStyle#">Object</th>
							<th style="#thStyle#">Name</th>
							<th style="#thStyle#">RelaywareID</th>
							<th style="#thStyle#">SalesForceID</th>
							<th style="#thStyle#">Message</th>
							<th style="#thStyle#">Attempts</th>
							<th style="#thStyle#">Current Status</th>
							<th style="#thStyle#">Last Updated</th>
							<th style="#thStyle#">View Details</th>
						</tr>
					<cfloop query="relatedRecords">
						<tr id="relatedRecords_#currentRow#">
							<!--- <cfif currentRow eq 1>
								<cfset tdStyle="border-top:1pt solid ##99BBE8;">
							</cfif>
							<cfif currentRow eq relatedRecords.recordCount>
								<cfset tdStyle="border-bottom:1pt solid ##99BBE8;">
							</cfif> --->
							<td><cf_input type="checkbox" id="frmRowIdentity" name="frmRowIdentity" value="#rowIdentity#"></td>
							<td>#htmlEditFormat(object)#</td>
							<td><cfif relaywareID neq ""><a href="" onclick="javascript:viewEntityDetails('#object#',#relaywareID#);return false;" class="smallLink">#htmlEditFormat(name)#</a><cfelse>#htmlEditFormat(name)#</cfif></td>
							<td>#htmlEditFormat(RelaywareID)#</td>
							<td>#htmlEditFormat(salesForceID)#</td>
							<td>#htmlEditFormat(message)#</td>
							<td>#htmlEditFormat(attempts)#</td>
							<td>#htmlEditFormat(current_status)#</td>
							<td>#htmlEditFormat(last_Updated)#</td>
							<td><a href="" onclick="javascript:void(viewErrors(#errorID#));return false;">#htmlEditFormat(View_Details)#</a></td>
						</tr>
					</cfloop>
					</table>
				</div>
				</td></tr>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn result>
	</cffunction>

</cfcomponent>