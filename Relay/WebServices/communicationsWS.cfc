<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			communicationsWS.cfc
Author:
Date started:			/xx/07

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011-07-04	NYB		P-LEN024 - added createCommContentFile function
2013-01		WAB		Sprint 15&42 Comms.  Added lots of functions for sending comms via Ajax
2013-05-29 	WAB		Fix sendCommForApproval() use new GatekeepersDistinct query which doesn't have blank personids
2013-06-19	WAB		change names of sendTestComm and sendTestCommToFriend functions.  Test removed and becomes an argument.
2013-06-20	WAB		centralise getCommunication function (and store result in THIS scope).  Only send Test emails when has not sent Live
2013-10		WAB		During CASE 437315.  Added function for cancelling a communication during sending
2013-11-01	WAB		CASE 437307 - Function for getting status on an asynchronous download
2013-12-17	WAB	 	CASE 438411 Send to individual person (for gatekeeper test) was not working (actually no method at all)
2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).
Possible enhancements:


 --->

 <cfcomponent output="false">
	<cfsetting enablecfoutputonly="true">


	<cffunction name="getCommunication" returnType="struct" access="private" output="false">
		<cfargument name="CommID" required="Yes">

		<cfif not structKeyExists (this,"communication")>
			<cfset this.communication = application.com.communications.getCommunication(commid = arguments.commid, personid = request.relayCurrentUser.personid)>
		</cfif>

		<cfreturn this.communication>

	</cffunction>


 	<cffunction name="loadStatisticsTable" access="public" output="false">
		<cfargument name="frmCommID" required="Yes">
		<cfargument name="filterCode" required="Yes">   <!--- E,I,T --->
		<cfargument name="frmcountryID" default="">
		<cfargument name="refresh" default = "false">

			<cfset var result_html = "">
			<cfset var stats_html = "">

			<cfif not request.relayCurrentUser.isLoggedIn or not application.com.login.checkInternalPermissions("CommTask","Level1")>
				<cfsaveContent variable="result_html">
					<cfoutput><script>document.getElementById('emailStatsStatusDiv').innerHTML = 'Please login';</script></cfoutput>
				</cfsavecontent>
			<cfelse>

				<cfset stats_html = application.com.communications.formatCommunicationStatisticsQuery(commid = frmCommID, filtercode = filtercode, countryid = frmCountryid , refresh=refresh)>
				<cfsaveContent variable="result_html">
					<cfoutput><script>document.getElementById('emailStatsStatusDiv').innerHTML = '';</script></cfoutput>
					<cf_translate><cfoutput>#stats_html#</cfoutput></cf_translate>
				</cfsavecontent>

			</cfif>
		<cfreturn result_html>

	</cffunction>

	<!---
	2011-07-04 NYB P-LEN024 - created
	 --->
 	<cffunction name="createCommContentFile" access="public" >
		<cfargument name="CommID" required="Yes">
		<cfset var pathStruct = application.com.filemanager.getTemporaryFolderDetails()>
		<cfset var subPath = "commfiles\zipped\#arguments.CommID#\">
		<cfset var result = StructNew()>
		<cfset var fileName = "">
		<cfset var communication = getCommunication(commid)>
		<cfset var commcontent = communication.getContent().email>
		<cfset var imgFile = "">

		<cfset fileName = "Comm#arguments.CommID#.zip">
		<cfset pathStruct.path = replace(pathStruct.path,'/','\','all')>

		<cfset application.com.globalFunctions.CreateDirectoryRecursive("#pathStruct.path##subPath#")>

		<cfif len(commcontent.html) gt 0>
			<cffile action="write" file="#pathStruct.path##subPath#/Comm#arguments.CommID#.htm" output="#commcontent.html#" charset="utf-8">
			<cfset imgArray = application.com.regExp.findHTMLTagsInString(inputString=commcontent.html,htmlTag="img")>
			<cfloop index="i" from="1" to="#ArrayLen(imgArray)#">
				<cfset imgFile = ListLast(imgArray[i].ATTRIBUTES.src,"/")>
				<cfif fileExists("#application.paths.content#\emailImages\#imgFile#")>
				<cffile action = "copy" destination = "#pathStruct.path##subPath#" source = "#application.paths.content#\emailImages\#imgFile#">
				</cfif>
			</cfloop>
		</cfif>
		<cfif len(commcontent.text) gt 0>
			<cffile action="write" file="#pathStruct.path##subPath#/Comm#arguments.CommID#.txt" output="#commcontent.text#" charset="utf-8">
		</cfif>

		<cfzip action="zip" file="#pathStruct.path##subPath##fileName#" source="#pathStruct.path##subPath#" overwrite="yes" recurse="yes"></cfzip>

		<cfset result.filePath = "#pathStruct.Path##subPath##fileName#">
		<cfset result.fileWebPath = "#pathStruct.WebPath##replace(subPath,'\','/','all')##fileName#">

		<cfreturn result>
	</cffunction>


	<!--- WAB 2011/10/19 added functions to do ajax attachments
			WAB 2013/03/25	CASE 433419 added XSS/SQL safeVariables because other form fields (in particular the content fields) are submitted as well
	--->
	<cffunction name="uploadAttachment" returnType="struct" access="public" output="false" XSSSafeVariables="FRMMSG_HTML|FRMMSG_TXT" SQLSafeVariables="FRMMSG_HTML|FRMMSG_TXT" >
		<cfargument name="frmCommID" required="Yes">

		<cfset var communication = getCommunication(commid = frmCommID)>
		<cfset var result = {isOK=true,message=""}>
		<CFSET var dirpath = "\commfiles\emails\attach">
		<CFSET var destination = application.paths.userFiles & dirpath>
		<cfset var commLimits = application.com.settings.getSetting("communications.limits")>

		<cfif not communication.hasRightsToEdit()>
			<cfset result = {isoK = false,message="You do not have rights to this communication"}>
		<cfelse>

				<!--- JIRA PROD2016-1271 - create directory if doesn't exist --->
				<cfif not directoryExists(destination)>
					<cfdirectory action="create" directory="#destination#">
				</cfif>

				<CFFILE ACTION="UPLOAD"
					FILEFIELD="uploadedFile"
					DESTINATION=#destination#
					NAMECONFLICT="MAKEUNIQUE">

				<CFSET upfilesize = Round(File.FileSize/1024)>
				<cfset upfilename= file.clientFile>

			<CFIF upfilesize GT commLimits.eattmaxsize_kb>
				<cfset result.isOK = false>
				<cfset result.message = "File too big">
				<!--- Deletes file just uploaded --->
				<CFSET delfile = destination & "\" & File.ServerFile>
				<CFIF FileExists(#delfile#)>
					<CFFILE ACTION="DELETE" FILE="#delfile#">
				</CFIF>


			<cfelse>

				<CFQUERY DATASOURCE="#application.SiteDataSource#">
				INSERT INTO Commfile (CommID, CommFormLID, Directory, FileName, OriginalFileName, IsParent, FileSize, CreatedBy, Created, LastUpdatedBy, LastUpdated)
				VALUES (<cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, 2, <cf_queryparam value="#dirpath#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#file.serverFile#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#file.clientfile#" CFSQLTYPE="CF_SQL_VARCHAR" >, 0, <cf_queryparam value="#upfilesize#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,getdate(), <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >, getDate())
				</CFQUERY>

				<cfset communication.updateContentLastUpdated(frmCommID)>
			</CFIF>

		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="getAttachmentsTable" returnType="string" access="public" output="false">
		<cfargument name="frmCommID" required="Yes">

		<cfset var communication = getCommunication(commid = frmCommID)>
		<cfset var table_html  = application.com.communications.createAttachmentsTable(attachmentsQuery=communication.getAttachmentsQuery(),showdeleteLink = bitand(communication.canBeEdited() , communication.hasRightsToEdit())) >
		<cfset table_html = application.com.relayTranslations.translateString(table_html)>

		<cfreturn table_html>
	</cffunction>


	<cffunction name="deleteAttachment" returnType="struct" access="public" output="false">
		<cfargument name="frmCommID" required="Yes">
		<cfargument name="frmCommFileID" required="Yes">

		<cfset var result = {isoK = true,message=""}>
		<cfset var communication = getCommunication(commid = frmCommID)>

		<cfif communication.hasRightsToEdit()>
			<cfset communication.deleteFile(commID=frmCommID,filetype="emailattachment",commFileId=frmcommFileID)>
		<cfelse>
			<cfset result = {isoK = false,message="You do not have rights to this communication"}>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="sendCommToFriend" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">
		<cfargument name="emailAddresses" required="Yes">
		<cfargument name="Test" required="Yes" type="boolean">

		<cfset communication = getCommunication(commid = arguments.commid)>

		<cfif arguments.test and communication.hasBeenSentLive()>
			<cfset arguments.test = false>
		</cfif>

		<cfset args = {commid = commid,test = test, emailAddresses = emailAddresses,asynchronous = false,resend = true}>
		<cfif arguments.test>
			<cfset args.SubjectPrefix="Test: ">
		</cfif>

		<cfreturn sendComm_Private(argumentCollection = args)>

	</cffunction>



	<cffunction name="sendCommToPerson" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">
		<cfargument name="PersonIDs" required="Yes" encrypted="true">
		<cfargument name="Test" required="Yes" type="boolean">

		<cfset communication = getCommunication(commid = arguments.commid)>

		<cfif arguments.test and communication.hasBeenSentLive()>
			<cfset arguments.test = false>
		</cfif>

		<cfset args = {commid = commid,test = test, personids = arguments.personids,asynchronous = false,resend = true}>
		<cfif test>
			<cfset args.SubjectPrefix="Test: ">
		</cfif>


		<cfreturn sendComm_Private(argumentCollection = args)>

	</cffunction>


	<cffunction name="sendCommToSelf" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">
		<cfargument name="Test" required="Yes" type="boolean">


		<cfreturn sendCommToPerson(argumentCollection = arguments, personids = request.relaycurrentuser.personid)>

	</cffunction>


	<cffunction name="sendCommForApproval" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">


		<cfset communication = getCommunication(commid = arguments.commid)>

		<cfset approvalDetails = communication.getApprovalDetails()>
		<cfif structkeyexists(approvalDetails,"GatekeepersDistinct") and approvalDetails.GatekeepersDistinct.recordcount gt 0>
			<cfset Personids = ValueList(approvalDetails.GatekeepersDistinct.PERSONID)>
		<cfelse>
			<cfset Personids = "">
		</cfif>

		<cfset args = {	commid = commid,
						test = true,
						personids = request.relayCurrentUser.personid,
						SubjectPrefix="phr_sys_comm_GatekeeperApprovalEmailSubject",
						FooterText= "phr_sys_comm_GatekeeperApprovalEmailFooter",
						showLinkBackToCommunication = true,
						ReplyTo = request.relayCurrentUser.person.email,
						Personids = Personids,
						asynchronous = false,resend = true}>



		<cfset sendResult = communication.send
						(argumentCollection = args
						)>


		<cfset application.com.entityApproval.setApprovalRequested(entityid = commid, entityType = 'Communication',PersonIDList = sendResult.getSuccessfulPersonIDList() ) >

		<cfset result.isOK = sendResult.isOK>
		<cfset result.commSendResultID = application.com.security.encryptVariableValue("commSendResultID",sendResult.commSendResultID)>
		<cfset result.message  = sendResult.getAjaxMessage(10)>
		<cfset result.continue = false>

		<cfreturn result>

	</cffunction>


	<!--- TBD secure this function for admin or owner --->
	<cffunction name="reSendComm" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true" >
		<cfargument name="CommID" required="Yes" encrypted="true">
		<cfargument name="selectionid" required="false" default = "" encrypted="true">
		<cfargument name="asynchronous" default="Yes" >

		<cfset result = sendComm_Private(commid= commid, selectionid = selectionid, test = false, asynchronous = asynchronous,resend = true)>

		<cfreturn result>

	</cffunction>



	<cffunction name="sendComm" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">
		<cfargument name="selectionid" required="false" default = "" encrypted="true">
		<cfargument name="asynchronous" default="Yes" >
		<cfargument name="test" default="false" >

		<cfset result = sendComm_Private(commid= commid, selectionid = selectionid, test = test, asynchronous = asynchronous)>

		<cfreturn result>

	</cffunction>


	<cffunction name="cancelCommSend" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">

		<cfset result = application.com.communications.cancelCommunicationSend(commid= commid)>

		<cfreturn result>

	</cffunction>


	<cffunction name="sendComm_Private" returnType="struct" access="private" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="true">
		<cfargument name="asynchronous" default="Yes" >

		<cfset communication = getCommunication(commid = arguments.commid)>

		<cfset sendResult = communication.send
						(argumentCollection = arguments
						)>


		<cfset result.isOK = sendResult.isOK>
		<cfset result.commSendResultID = application.com.security.encryptVariableValue("commSendResultID",sendResult.commSendResultID)>

		<cfif asynchronous and sendResult.isOK>
			<cfset result.message = "Sending Started (#sendResult.commSendResultID#)">
			<cfset result.continue = true>
		<cfelse>
			<cfset result.message  = sendResult.getAjaxMessage(10)>
			<cfset result.continue = false>
		</cfif>


		<cfreturn result>

	</cffunction>


	<cffunction name="getCommunicationSendStatus" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="false">
		<cfargument name="commSendResultID" required="false" >

		<cfset var result = {isOK = true, continue = true}>
			<cfset var checkForResultObject = "">
		<cfset var checkForLock = "">

		<cfset checkForLock = application.com.communications.getCommunicationLockDetails(commid = commid,commSendResultID = commSendResultID)>

		<cfif arrayLen(checkForLock)>
			<cfset result.message = checkForLock[1].metadata.message >

			<cfset result.message &= '<BR><A href="javascript:cancelCommSend(commID_enc)" >Cancel</A>'>

			<!--- Add on the debug link if key is there --->
			<cfif structKeyExists (checkForLock[1].metadata,"debugFile")>
				<cfset result.message &= '<BR><A href="#checkForLock[1].metadata.debugFile#" target="Debug_#commid#">Debug</A>'>
			</cfif>
		<cfelse>
			<cfset checkForResultObject  = application.com.communications.getCommSendResultObject(commSendResultID)>
			<cfif checkForResultObject.isOK>
				<cfset result.message  = checkForResultObject.object.getAjaxMessage(3)>
				<cfset result.continue = false>
			<cfelse>
				<cfset result.isok = false>
				<cfset result.message = "Failed (#commSendResultID#)">
				<cfset result.continue = false>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getMessageEntities" access="public" returntype="query" output="false" loginRequired=2 internalUser="true">
		<cfargument name="sendToEntityType" type="string" required="true">
		<cfargument name="messageID" type="numeric" default=0 encrypted="true"> <!--- NJH 2016/07/19 - JIRA PROD2016-1444 set this to encrypted for tidiness sake --->

		<cfset var getEntityNames = queryNew("entityID,entityName")>

		<cfquery name="getEntityNames" datasource="#application.siteDataSource#">
			select entityID, entityName from (
			<!--- <cfif arguments.sendToEntityType eq "person">
			select distinct isNull(firstname,'')+' '+isNull(lastname,'') as entityName,	p.personID as entityID from person p
				inner join booleanFlagData bfd on bfd.entityID = p.personID and bfd.flagID = #application.com.flag.getFlagStructure(flagID='perApproved').flagID#
				inner join location l on l.locationID = p.locationID
				#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l").join#
				left join vPersonSearch vp on vp.entityID = p.personID
			<cfif structKeyExists(arguments,"searchString") and arguments.searchString neq "">
			where <!--- (isNull(firstname,'')+' '+isNull(lastname,'') like '#arguments.searchString#%')  --->
				<!--- (CONTAINS(vp.searchableString, <cf_queryparam value="#searchStringWithQuotes#" CFSQLTYPE="CF_SQL_VARCHAR" >)) --->
				1=0
				<cfif isNumeric(arguments.searchString)> or (p.personID = #arguments.searchString#)</cfif>
			<cfelse>
				where 1=0
			</cfif> --->
			<cfif arguments.sendToEntityType eq "usergroup">
			select distinct name as entityName, usergroupID as entityID from usergroup
				where userGroupType = 'External'
			<cfelseif arguments.sendToEntityType eq "selection">
			select distinct s.title as entityName, s.selectionID as entityID from selection s
				inner join SelectionGroup AS sg on sg.SelectionID = s.SelectionID
				inner join RightsGroup rg on sg.usergroupid = rg.usergroupid
				left join usergroup ug on s.createdby = ug.usergroupid
			 where rg.personID = #request.relayCurrentUser.personID#
			<cfelse>
			<!--- if it' a person, only return the person tied to the message. If it's a new message, then there will be no results, which is fine --->
				select p.firstname + ' '+p.lastname as entityName, personId as entityID
				from person p inner join message m on p.personId = m.sendToEntityID and m.sendToEntityType = 'Person'
				where messageId = #arguments.messageID#
			</cfif>
			) base
			order by entityName,entityID
		</cfquery>

		<cfreturn getEntityNames>
	</cffunction>


	<cffunction name="getMessageEntitiesSearch" access="remote" returntype="string" output="false" loginRequired=2 internalUser="true">
		<cfargument name="sendToEntityType" type="string" required="true">
		<cfargument name="term" type="string" required="true">

		<cfset var getEntityNames = queryNew("value,ID,label")>
		<cfset var entityArray = arrayNew(1)>
		<cfset var jsonString = "">

		<!--- <cfset getEntityNames = getMessageEntities(sendToEntityType=arguments.sendToEntityType,searchString=arguments.term)> --->
		<cfset getEntityNames = application.com.search.getSiteSearchResults(tablesToSearch="person",searchTextString=arguments.term,searchApprovedPOLOnly=true,extraColumns="p.firstname+' '+p.lastname as fullname",maxReturnedRows=25)>
		<cfquery name="getEntityNames" dbtype="query">
			select entityID as ID, fullname as [value], entityName as label from getEntityNames
		</cfquery>

		<cfloop query="getEntityNames">
			<cfset rowStruct = application.com.structureFunctions.queryRowToStruct(query=getEntityNames,row=currentRow)>
			<cfset arrayAppend(entityArray,rowStruct)>
		</cfloop>

		<!--- jQuery autocomplete is expecting id,label and value to be in lower case.. CF structs are always uppercase.. so, convert json to string.. seems to work well. --->
		<cfset jsonString = serializeJson(entityArray)>
		<cfset jsonString = replaceList(jsonString,'"ID","LABEL","VALUE"','"id","label","value"')>

		<cfreturn jsonString>
	</cffunction>


	<cffunction name="disableCommFromNameID" access="remote" returnType="struct">
		<cfargument name="optionToRemove" type="numeric" required="true">

		<cfreturn application.com.communications.disableCommFromNameID(arguments.optionToRemove)>
	</cffunction>


	<!--- 2013-11-01 WAB CASE 437307 - Function for getting status on an asynchronous download --->
	<cffunction name="getDownloadStatus" returnType="struct" access="public" output="false" loginRequired=2 internalUser="true">
		<cfargument name="CommID" required="Yes" encrypted="false">

		<cfset var result = {isOK = true, complete = false, message=""}>

		<cfset checkForLock = application.com.globalFunctions.getProcessLock(lockname= "download_#commid#")>

		<cfif checkForLock.lockExists>
			<cfset result.message = checkForLock.metadata.message >
		<cfelse>
			<!--- No lock, check for file --->
			<CFQUERY name="getFile" DATASOURCE="#application.SiteDataSource#">
			select
				directory + '\' + filename as filename
			from
				commfile
			where
				commid = <cf_queryparam value="#commid#" cfsqltype="cf_sql_integer">
			</CFQUERY>

			<cfif getFile.RecordCOunt>
				<cfset result.complete = true>
				<cfset result.message = "Complete">
			<cfelse>
				<!--- check for an error id --->
				<cfset result.isOK = false>
				<cfset metaData = application.com.communications.getCommMetaData(commid)>
				<cfif structKeyExists (metaData,"errorid")>
					<cfset result.message = "An Error has occurred.<BR>Please contact Relay Support Quoting ID #metaData.ErrorID#  ">
				<cfelse>
					<cfset result.message = "An unspecified Error has Occurred">
				</cfif>

			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="getContactHistoryContent" access="remote" returnType="struct" output="false" loginRequired=2 internalUser="true">
		<cfargument name="entityID" type="numeric" default=0>
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="commTypeID" type="numeric" default=0>
		<cfargument name="personID" type="numeric" default=0>
		<cfargument name="visitID" type="numeric" default=0>

		<cfset var content = "">
		<cfset var commID = 0>
		<cfset var CDID = 0>
		<cfset var includeTemplate = "/phoning/CommHistory_Detail.cfm">
		<cfset var result = {isOK:true,content=""}>

		<cfif arguments.entityType eq "message">
			<cfquery name="content">
				select text_defaultTranslation from message where messageID=<cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfset content = content.text_defaultTranslation>

		<cfelseif arguments.entityType eq "commDetail">

			<cfif arguments.commTypeID eq 2>
				<cfset CommID = arguments.entityID>

				<cfset includeTemplate = "/communicate/viewCommunication.cfm">
			<cfelse>
				<cfset url.CDID = arguments.entityID>

			</cfif>

			<cfsavecontent variable="content">
				<cfoutput>
					<cfinclude template="#includeTemplate#">
				</cfoutput>
			</cfsavecontent>

		<cfelseif arguments.entityType eq "activity">
			<cfset reportMode = "NotStandalone">
			<cfset userToView = arguments.personID>
			<cfset visitID = arguments.visitID>

			<cfsavecontent variable="content">
				<cfoutput>
					<cf_include template="/report/entityUsageList.cfm">
				</cfoutput>
			</cfsavecontent>
		</cfif>

		<cfset result.content = content>

		<cfreturn result>

	</cffunction>
 </cfcomponent>