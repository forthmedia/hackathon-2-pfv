<!--- �Relayware. All Rights Reserved 2016 

--->
<cfcomponent output="false">
	
	<cffunction name="getMediaLibrary" access="remote" output="true" returnType="string">
		<cfargument name="pageNumber" type="numeric" default="1">

		<cfreturn application.com.mediaSearch.getMediaLibrary(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="getProductFamilyQuery" access="remote" output="true" returnType="query">
		<cfset var productFamilyQuery = "">
		
		<cfquery name="productFamilyQuery">
			select name as displayvalue,
			productFamilyID as datavalue
			from productFamily
			<cfif structKeyExists(arguments,"productCategoryID") or structKeyExists(arguments,"productGroupID") or structKeyExists(arguments,"productID")>
				inner join productCategory on productFamily.productFamilyID = productCategory.productFamilyID
			</cfif>
			<cfif structKeyExists(arguments,"productGroupID") or structKeyExists(arguments,"productID")>
				inner join productGroup on productCategory.productCategoryID = productGroup.productCategoryID
			</cfif>
			<cfif structKeyExists(arguments,"productID")>
				inner join product on productGroup.productGroupID = product.productGroupID
			</cfif>
			where 1 = 1
			<cfif structKeyExists(arguments,"productFamilyID")>
				and productFamily.productFamilyID = <cf_queryparam value="#arguments.productFamilyID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productCategoryID")>
				and productCategory.productCategoryID = <cf_queryparam value="#arguments.productCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productGroupID")>
				and productGroup.productGroupID = <cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productID")>
				and product.productID = <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>
		
		<cfreturn productFamilyQuery>
	</cffunction>
	
	<cffunction name="getProductCategoryQuery" access="remote" output="true" returnType="query">
		<cfset var productCategoryQuery = "">
		<cfset var productCategorySnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productCategory", phraseTextID = "title")>
		
		<cfquery name="productCategoryQuery">
			select #productCategorySnippet.select# as displayvalue,
			productCategoryID as datavalue 
			from productCategory
			 #preserveSingleQuotes(productCategorySnippet.join)#
			<cfif structKeyExists(arguments,"productGroupID") or structKeyExists(arguments,"productID")>
				inner join productGroup on productCategory.productCategoryID = productGroup.productCategoryID
			</cfif>
			<cfif structKeyExists(arguments,"productID")>
				inner join product on productGroup.productGroupID = product.productGroupID
			</cfif>
			<cfif structKeyExists(arguments,"productFamily")>
				inner join productFamily on productFamily.productFamilyID = productCategory.productFamilyID
			</cfif>
			where 1 = 1
			<cfif structKeyExists(arguments,"productFamilyID")>
				and productFamily.productFamilyID = <cf_queryparam value="#arguments.productFamilyID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productCategoryID")>
				and productCategory.productCategoryID = <cf_queryparam value="#arguments.productCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productGroupID")>
				and productGroup.productGroupID = <cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productID")>
				and product.productID = <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>
		
		<cfreturn productCategoryQuery>
	</cffunction>
	
	<cffunction name="getProductGroupQuery" access="remote" output="true" returnType="query">
		<cfset var productGroupQuery = "">
		<cfset var productGroupSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productGroup", phraseTextID = "title")>
						
		<cfquery name="productGroupQuery">
			select #productGroupSnippet.select# as displayvalue,
			productGroupID as datavalue 
			from productGroup
			 #preserveSingleQuotes(productGroupSnippet.join)#
			<cfif structKeyExists(arguments,"productID")>
				inner join product on productGroup.productGroupID = product.productGroupID
			</cfif>
			<cfif structKeyExists(arguments,"productCategoryID") or structKeyExists(arguments,"productFamilyID")>
				inner join productGroup on productCategory.productCategoryID = productGroup.productCategoryID
			</cfif>
			<cfif structKeyExists(arguments,"productFamilyID")>
				inner join productFamily on productFamily.productFamilyID = productCategory.productFamilyID
			</cfif>
			where 1 = 1
			<cfif structKeyExists(arguments,"productFamilyID")>
				and productFamily.productFamilyID = <cf_queryparam value="#arguments.productFamilyID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productCategoryID")>
				and productCategory.productCategoryID = <cf_queryparam value="#arguments.productCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productGroupID")>
				and productGroup.productGroupID = <cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productID")>
				and product.productID = <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>
		
		<cfreturn productGroupQuery>
	</cffunction>
	
	<cffunction name="getProductQuery" access="remote" output="true" returnType="query">
		<cfset var productQuery = "">
		<cfset var productTitleSnippet = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "product", phraseTextID = "title")>
		
		<cfquery name="productQuery">
			select #productTitleSnippet.select# as displayvalue,
			product.SKU as datavalue 
			from product
			 #preserveSingleQuotes(productTitleSnippet.join)#
			<cfif structKeyExists(arguments,"productGroupID") or structKeyExists(arguments,"productCategoryID") or structKeyExists(arguments,"productFamilyID")>
				inner join productGroup on productCategory.productCategoryID = productGroup.productCategoryID
			</cfif>
			<cfif structKeyExists(arguments,"productCategoryID") or structKeyExists(arguments,"productFamilyID")>
				inner join productGroup on productCategory.productCategoryID = productGroup.productCategoryID
			</cfif>
			<cfif structKeyExists(arguments,"productFamilyID")>
				inner join productFamily on productFamily.productFamilyID = productCategory.productFamilyID
			</cfif>
			where 1 = 1
			<cfif structKeyExists(arguments,"productFamilyID")>
				and productFamily.productFamilyID = <cf_queryparam value="#arguments.productFamilyID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productCategoryID")>
				and productCategory.productCategoryID = <cf_queryparam value="#arguments.productCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productGroupID")>
				and productGroup.productGroupID = <cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif structKeyExists(arguments,"productID")>
				and product.productID = <cf_queryparam value="#arguments.productID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>
		
		<cfreturn productQuery>
	</cffunction>
	
</cfcomponent>
