<!--- �Relayware. All Rights Reserved 2014 
File name : relayRatingWS.cfc

2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).
2014-02-04 PPB Case 438147 add VisitId to rating
 --->

<cfcomponent>

	<cffunction name="rateEntity" access="remote" returntype="string">
		<cfargument name="entityID" required="true">
		<cfargument name="rating" required="true">
		<cfargument name="entityTypeID" required="true">
		
		<cfset var saveRatings = "">
		<cfset var result = "Saved">
		
		<cfif request.relaycurrentuser.VisitId neq 0>			<!--- 2014-02-04 PPB Case 438147 don't save rating if visitId=0 ie done by a bot --->
			<cftry>
				<CFQUERY NAME="saveRatings" dataSource="#application.siteDataSource#">
					delete from Rating
					where personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.personID#"> 
					and entityID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID# ">
					and entitytypeID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entitytypeID#">
					<!--- 2013-07-17	YMA	Case 436216 added to support cancellation of rating --->
					<cfif structKeyExists(arguments,"rating") and isNumeric(arguments.rating) and arguments.rating gt 0>
						INSERT INTO 
						Rating (PersonID, entityID,entitytypeID, Rating, VisitId, created,lastupdated)							<!--- 2014-02-04 PPB Case 438147 save visitid --->
						VALUES (<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.personID#">,
								<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entityID#">,
								<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.entitytypeID#">,
								<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.rating#">,
								<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.VisitId#">,			<!--- 2014-02-04 PPB Case 438147 save visitid --->
								getdate(),getdate())
					</cfif>
				</CFQUERY>
				
				<cfcatch type = "Database">
					<cfset result="Problems please contact admin">
				</cfcatch>
			</cftry>
		</cfif>													<!--- 2014-02-04 PPB Case 438147 --->
		
		<cfreturn result />
	</cffunction>

	<cffunction name="getRatingsDiv" access="remote">
		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="entitytypeID" required="yes" type="numeric">
		<cfargument name="dontAllowRatings" required="no" type="boolean">
		
		<cfreturn application.com.relayRating.getRatingsDiv(argumentCollection=arguments)>
	</cffunction>

</cfcomponent>