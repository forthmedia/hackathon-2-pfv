<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayLocatorWS.cfc	
Author:			SSS
Date started:	05-05-2009
	
Description:			
This webservice was setup for the locator for ajex purposes
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="showChildRecords" access="remote">
		<cfargument name="parentEntityType" type="string" required="true">
		<cfargument name="parentEntityID" type="numeric" required="true">
		<cfargument name="childEntityType" type="string" required="true">
		<cfargument name="columnsToShow" type="string" required="true" encrypt="true">
		<cfargument name="editorCfm" type="string" required="true">
		<cfargument name="showRecordDelete" type="string" default="true">
		
		<cfset var childUniqueKey="">
		<cfset var thisValue = "">
		<cfset var getChildren = "">
		<cfset var resultDisplay = "">
		<cfset var childEntityTypeDisplayFunction = "">
		<cfset var displayColumns = "">
		<cfset var showDeleteColumn = true>
		<cfset var addNewTextID = "">
		<cfset var addNewText = "">
		<cfset var childXMLEditor = "">
		<cfset var childXmlSource = "">
		<cfset var col = "">
		<cfset var fieldStruct = structNew()>
		
		<cfif isBoolean(arguments.showRecordDelete) and not arguments.showRecordDelete>
			<cfset showDeleteColumn = false>
		</cfif>
		
		<cfsavecontent variable="childXmlSource">
			<cfoutput>
				<cfinclude template="/#listFirst(arguments.editorCfm,"/")#/#arguments.childEntityType#Editor.cfm">
			</cfoutput>
		</cfsavecontent>
		<cfset childXMLEditor = xmlParse(childXmlSource)>

		<cfset childUniqueKey=application.com.relayEntity.getEntityType(entityTypeID=application.entityTypeID[arguments.childEntityType]).uniqueKey>
		
		<cfquery name="getChildren" datasource="#application.siteDataSource#">
			select #preserveSingleQuotes(arguments.columnsToShow)#,#childUniqueKey#
			from #arguments.childEntityType# e
			where e.#application.com.relayEntity.getEntityType(entityTypeID=application.entityTypeID[arguments.parentEntityType]).uniqueKey# = #arguments.parentEntityID#
		</cfquery>
		
		<cfset displayColumns = application.com.dbtools.getColumnListInCorrectOrder(getChildren)>
		<cfset displayColumns = listDeleteAt(displayColumns,listFindNoCase(displayColumns,childUniqueKey))>
		
		<cfsavecontent variable="resultDisplay">
			<cfoutput>
			<table>
				<tr>
				<cfloop list="#displayColumns#" index="col">
					<th>phr_#arguments.childEntityType#_#col#</th>
				</cfloop>
				<cfif showDeleteColumn><th>&nbsp;</th></cfif>
				</tr>
				<cfloop query="getChildren">
					<tr>
						<cfset count = 0>
						<cfloop list="#displayColumns#" index="col">
							<cfset count++>
							<td>
							<cfif count eq 1>
								<a class="smallLink" href="#arguments.editorCfm#?entityType=#arguments.childEntityType#&editor=yes&showSave=false&showSaveAndAddNew=false&hideBackButton=false&showSaveAndReturn=true&#childUniqueKey#=#getChildren[childUniqueKey][currentRow]#">#htmlEditFormat(getChildren[col][currentRow])#</a>
							<cfelse>
								<cfset thisValue = getChildren[col][currentRow]>
								<cfset fieldStruct = application.com.relayEntity.getTableFieldStructure(tablename=arguments.childEntityType,fieldname=col)>
								<cfif fieldStruct.isOK and fieldStruct.data_type eq "bit">
									<cfif thisValue>
										<cfset thisValue  = "phr_yes">
									<cfelse>
										<cfset thisValue = "phr_no">
									</cfif>
								</cfif>
								#htmlEditFormat(thisValue)#
							</cfif>
							</td>
						</cfloop>
						<cfif showDeleteColumn>
							<td>
								<!--- do an evaluate because it can be an expression --->
								<cfif evaluate(arguments.showRecordDelete)>
									<a href="" onclick="deleteChildRecord('#arguments.childEntityType#','#getChildren[childUniqueKey][currentRow]#');return false;" title="Delete #arguments.childEntityType# #getChildren[childUniqueKey][currentRow]#"><img src="/images/misc/iconDeleteCross.gif"></a>
								</cfif>
							</td>
						</cfif>
					</tr>
				</cfloop>
				
				<cfset addNewText = "Add New #arguments.childEntityType#">
				<cfset addNewTextID = "addNew_#arguments.childEntityType#">
				<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID=addNewTextID)>
					<cfset addNewText = "phr_"&addNewTextID>
				</cfif>
				
				<tr>
					<th colspan="#listLen(arguments.columnsToShow)+1#">#addNewText#</th>
				</tr>
				<tr id="add#arguments.childEntityType#Record">
					<cfloop list="#displayColumns#" index="col">
					<td>
						<cfif application.com.relayEntity.getTableFieldStructure(tablename=arguments.childEntityType,fieldname=col).isOK>
							<cfset fieldXML = xmlSearch(childXMLEditor,"//editor//field[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(col)#']")>
							<!--- #application.com.relayDisplay.displayInputForTableField(entityType=arguments.childEntityType,field=col,fieldAttributes=fieldXML[1].xmlAttributes)# --->

							#application.com.relayDisplay.displayXMLEditorFields(fieldNodes=fieldXML,entityType=arguments.childEntityType,entityID=0,newLine=false,quickAdd=true)#

							<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID="#arguments.childEntityType#_help_#col#")>
								<span title="phr_#arguments.childEntityType#_help_#col#"><img src="/images/misc/iconhelpsmall.gif"></span>
							</cfif>
						</cfif>
					</td>
					</cfloop>
					<td><cf_input type="button" value="Add" name="addRecord" onclick="addChildRecord('#arguments.childEntityType#');"></td>
				</tr>
			</table>
			
			<!--- turn off validation for these particular form elements that we've just output, so that the main form submit doesn't pick them up --->
			<script>
				var parentObj = jQuery('##add#arguments.childEntityType#Record').get(0);
				var inputElems = getInputElements(parentObj);
				setValidateForInputElements(inputElems,false);
			</script>
			</cfoutput>
		</cfsavecontent>
		
		<cfreturn resultDisplay>	
	</cffunction>


	<cffunction name="AddChildRecord" access="remote">
		<cfargument name="childEntityType" type="string" required="true">
		
		<cfreturn application.com.relayEntity.insertEntity(entityDetails=arguments,table=arguments.childEntityType,insertIfExists=true)>
	</cffunction>
	
	
	<cffunction name="deleteChildRecord" access="remote">
		<cfargument name="childEntityType" type="string" required="true">
		<cfargument name="childEntityID" type="numeric" required="true">
		
		<cfreturn application.com.relayEntity.deleteEntity(entityID=arguments.childEntityID,entityTypeID=arguments.childEntityType)>
	</cffunction>

</cfcomponent>